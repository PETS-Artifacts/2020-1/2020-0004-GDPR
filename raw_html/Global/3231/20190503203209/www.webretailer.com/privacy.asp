
<!DOCTYPE html><html lang="en"><!-- InstanceBegin template="/Templates/blank.dwt.asp" codeOutsideHTMLIsLocked="false" -->
<head>
	<meta charset="utf-8" />
	<!-- InstanceBeginEditable name="doctitle" --> 
<title>Privacy Policy | Web Retailer</title>
<!-- InstanceEndEditable -->
	<!--ZOOMSTOP-->
	
	<meta name="description" content="Find the best software for your needs, discuss issues and trends, build industry knowledge, and learn to become a more effective seller." />
	<meta property="og:image" content="https://www.webretailer.com/images/logo/600x315px.png" />
	<meta property="og:description" content="Find the best software for your needs, discuss issues and trends, build industry knowledge, and learn to become a more effective seller." />
	
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link href="/favicon.ico" rel="shortcut icon" />
	<link rel="stylesheet" href="/css/reset.css?v=253">
	<link rel="stylesheet" href="/css/unsemantic-grid-responsive-tablet-no-ie7.css?v=253">
	<link rel="stylesheet" href="/css/webretailer.css?v=253" />
	<link rel="stylesheet" media="print" href="/css/print.css?v=253" />
	
	
	
	
	<script src="//code.jquery.com/jquery-3.3.1.js"></script>
	<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<script src="/js/js.cookie.min.js"></script>
	<script src="/js/functions.js?v=253"></script>
	<script src="/js/pageload.js?v=253"></script>
	<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-5877090-4', {'siteSpeedSampleRate': 20});
	
	ga('send', 'pageview', { 'hitCallback': removeUtms });
</script>
	
	<!-- InstanceParam name="final_error_check" type="boolean" value="true" -->
	<!-- InstanceParam name="robots_index" type="boolean" value="true" -->
	<!-- InstanceParam name="ad_bar_tall" type="boolean" value="true" -->
	<!-- InstanceParam name="ad_top_right" type="boolean" value="true" -->
	<!-- InstanceParam name="member_controls" type="boolean" value="true" -->
	<!-- InstanceParam name="breadcrumb" type="boolean" value="true" -->
	<!-- InstanceParam name="shareable" type="boolean" value="false" -->
	<!-- InstanceParam name="admin_page" type="boolean" value="false" -->
	<!-- InstanceParam name="section" type="text" value="" -->
	<!-- InstanceParam name="pagename" type="text" value="" -->
</head>
<body>

	<!-- COOKIE CONTROL -->
	<script src="https://cc.cdn.civiccomputing.com/8/cookieControl-8.x.min.js"></script>
<script>
    var config = {
        apiKey: '4500fb74b833a40329098271b51de52018edf411',
        product: 'PRO',
        necessaryCookies: [
			'hideAnnouncement',
			'ASPSESSIONID*',
			'SessionToken'
		],
		optionalCookies: [
            {
                    name: 'functional',
                    label: 'Functional',
                    description: 'Functional cookies enhance the website\'s functionality, including remembering ' +
								 'your login information, your country and the last tab visited on directory pages.',
                    cookies: [
						'CountryCode',
						'last-profile', 
						'last-profile-tab',
						'LoginToken', 
						'memberName'
					],
                    onAccept : function(){},
                    onRevoke: function(){},
					recommendedState: true,
					lawfulBasis: 'legitimate interest'
                },{
                    name: 'analytical',
                    label: 'Analytical',
                    description: 'Analytical cookies help us to improve our website by collecting and ' +
								 'reporting information on its usage.',
                    cookies: ['_g*', '__ut*'],
                    onAccept : function(){},
                    onRevoke: function(){},
					recommendedState: true,
					lawfulBasis: 'legitimate interest'
                },{
                    name: 'targeting',
                    label: 'Targeting',
                    description: 'Targeting (or advertising) cookies record information about your visit ' +
								 'to and use of our website, for advertising purposes.',
                    cookies: [],
					thirdPartyCookies: [{
						name: 'Google AdSense',
						optOutLink: 'https://adssettings.google.com/'
					}],
                    onAccept : function(){},
                    onRevoke: function(){},
					recommendedState: true,
					lawfulBasis: 'legitimate interest'
                }
        ],

        consentCookieExpiry: 90,
		statement: {
                    description: 'For more detailed information on the cookies we use, please check our ',
					name: 'Cookies Policy',
					url: 'https://www.webretailer.com/cookies-policy.asp',
					updated: '03/01/2019'
                	},
		logConsent: true,
		initialState: 'open',
		branding: 	{
					removeAbout: true
					},
		excludedCountries: ['all'],
		position: 'RIGHT',
        theme: 'DARK'
    };
    
    CookieControl.load( config );
</script>
	<!-- END COOKIE CONTROL -->


<div id="pagecontainer" class="grid-container" title="">

	
	<header id="pageheader">
		<div id="logo" class="grid-25 tablet-grid-35 tablet-prefix-15">
			
			
			<a href="/"><img src="/images/logo/500px.png?v=253" width="500" height="103" alt="Web Retailer Logo" /></a>
			
		</div>
		<div id="member_controls" class="grid-35 tablet-grid-35 tablet-suffix-15">
			
			
				<ul>
					<li>
						<a rel="nofollow" href="/register.asp?source=header&amp;fromURL=%2Fprivacy%2Easp">Register Now (It's Free!)</a>
						
					or
						<a rel="nofollow" href="/login.asp?fromURL=%2Fprivacy%2Easp">Login</a>
						
					</li>
				</ul>
				
			
		</div>
		
		<div class="grid-40 tablet-grid-100" id="top_right_banner">
			
	<a target="_blank" href="https://www.zynergybooks.com/" rel="nofollow" onclick="return(trackOutboundLink(this.href, 'Ad', 'Top Right Banner', 'Zynergy Books'));">
		<img src="/images/das/topright/ZynergyBooks-2019-02-23.gif" width="468" height="60" alt="Zynergy Books" />
	</a>
	<script type="text/javascript">
		ga('send', 'event', 'Impression', 'TopRight', 'Zynergy Books Impression', {'nonInteraction': 1});
	</script>

<!-- ASP served by VeloxServ Dedicated -->
		</div>
		
	</header>
	
	
		<nav id="nav_bar">
			<div class="grid-65 tablet-grid-65" id="nav_sections">
				<ul>
					<li class="nav_section">
						<a class="nav_section_link" href="/directory.asp">Directory</a>
						
						<div id="nav_directory">
	<div>
		<ul>
			<li>
				<a href="/categories/multichannel-management.asp">
					<div>Multichannel Management</div>
					<p>Manage inventory and sales channels</p>
				</a>
			</li>
			<li>
				<a href="/categories/amazon-selling.asp">
					<div>Amazon Selling</div>
					<p>Tools and services for Amazon sellers</p>
				</a>
			</li>
			<li>
				<a href="/categories/ebay-selling.asp">
					<div>eBay Selling</div>
					<p>Tools and services for eBay sellers</p>
				</a>
			</li>
			<li>
				<a href="/categories/product-sourcing.asp">
					<div>Product Sourcing</div>
					<p>Find and buy profitable inventory</p>
				</a>
			</li>
		</ul>
	</div>
	<div>
		<ul>
			<li>
				<a href="/categories/marketplaces-channels.asp">
					<div>Marketplaces &amp; Channels</div>
					<p>Marketplaces, carts &amp; connectors</p>
				</a>
			</li>
			<li>
				<a href="/categories/international-ecommerce.asp">
					<div>International Ecommerce</div>
					<p>Handle challenges of cross-border trade</p>
				</a>
			</li>
			<li>
				<a href="/categories/shipping-fulfillment.asp">
					<div>Shipping &amp; Fulfillment</div>
					<p>Manage orders and shipments</p>
				</a>
			</li>
			<li>
				<a href="/categories/financial-management.asp">
					<div>Financial Management</div>
					<p>Accounting, taxes and loans</p>
				</a>
			</li>
		</ul>
	</div>
	<div>
		<ul>
			<li>
				<a href="/categories/pricing-repricing.asp">
					<div>Pricing &amp; Repricing</div>
					<p>Automate pricing and track competitors</p>
				</a>
			</li>
			<li>
				<a href="/categories/feedback-reviews-support.asp">
					<div>Feedback, Reviews &amp; Support</div>
					<p>Manage customer support &amp; feedback</p>
				</a>
			</li>
			<li>
				<a href="/categories/outsourcing-consultants.asp">
					<div>Outsourcing &amp; Consultants</div>
					<p>Get outside help to grow</p>
				</a>
			</li>
			<!--
			<li class="spacer"></li>
			-->
			<li>
				<a href="/special-offers.asp">
					<img style="float: right;" class="dir_icon special-offers_icon" src="/images/design/shim-transparent.png" alt="This Week Only" width="35" height="35" />
					<div>Special Offers</div>
					<p>Exclusive deals for our readers</p>
				</a>
			</li>
		</ul>
	</div>
</div>
					</li>
					<!-- THIS WEEK ONLY START -->
					<li class="nav_section">
                        <a class="nav_section_link" href="/special-offers.asp">This Week Only</a>
                        
                    </li>
					<!-- THIS WEEK ONLY END -->
					<li class="nav_section">
						<a class="nav_section_link" href="/forum.asp">Forum</a>
						
					</li>
					<span id="dynamicnav">
	
</span>
					<li class="nav_section">
						<a class="nav_section_link" href="/webinars.asp">Webinars</a>
						
					</li>
					<li class="nav_section" id="nav_section_last">
						<a class="nav_section_link" href="/lean-commerce/">In Depth</a>
					</li>
					<li class="nav_section hide-on-tablet hide-on-desktop" id="nav_more"><a href="#/">&equiv;</a></li>
				</ul>
			</div>
			<div class="grid-35 tablet-grid-35 hide-on-mobile" id="nav_search">
				<div id="search">
					<form action="/search-prefilter.asp" method="get" name="search" onsubmit="if($('#search-text').val().length &lt; 2)return false;">
						<input id="search-text" name="query" type="text" size="35" maxlength="35" value="" />
						<input id="search-button" name="action" type="submit" class="button mainbutton" value="Search" />
					</form>
				</div>
			</div>
		</nav>
		
		<nav id="breadcrumb_bar">
			<div class="grid-75 tablet-grid-65 mobile-grid-65">
				<ul id="breadcrumb">
					<li id="homecrumb"><a href="/"></a></li>
					<!-- InstanceBeginEditable name="Breadcrumb" --><li>Privacy Policy</li><!-- InstanceEndEditable -->
				</ul>
			</div>	
			<div class="grid-25 tablet-grid-35 mobile-grid-35">
				<div id="followbuttons">
					<span id="wereonfb"><a href="https://www.facebook.com/WebRetailer">We're on Facebook</a></span>
				</div>
			</div>
		</nav>
			
	<!-- if not blogASPBlocks page -->
<!-- if not admin page -->
<div id="body" class="grid-100">
	<div id="maincontent">
		<!--ZOOMRESTART-->
		<!-- InstanceBeginEditable name="Content" -->
<h1 class="noparaabove">Privacy Policy</h1>
<p>This Privacy Policy sets out how we, Angel Internet Ltd, collect, store and use information about you when you use or interact with our website, www.webretailer.com (our <b>website</b>) and where we otherwise obtain or collect information about you. This Privacy Policy is effective from 3rd January 2019.</p>
<h2>Contents</h2>
<ol>
<li><a class="mfp-prevent-close" href="#section1">Summary</a></li>
<li><a class="mfp-prevent-close" href="#section2">Our details</a></li>
<li><a class="mfp-prevent-close" href="#section3">Information we collect when you visit our website</a></li>
<li><a class="mfp-prevent-close" href="#section4">Information we collect when you contact us</a></li>
<li><a class="mfp-prevent-close" href="#section5">Information we collect when you register on our website</a></li>
<li><a class="mfp-prevent-close" href="#section6">Disclosure and additional uses of your information</a></li>
<li><a class="mfp-prevent-close" href="#section7">How long we retain your information</a></li>
<li><a class="mfp-prevent-close" href="#section8">How we secure your information</a></li>
<li><a class="mfp-prevent-close" href="#section9">Transfers of your information outside the European Economic Area</a></li>
<li><a class="mfp-prevent-close" href="#section10">Your rights in relation to your information</a></li>
<li><a class="mfp-prevent-close" href="#section11">Your right to object to the processing of your information for certain purposes</a></li>
<li><a class="mfp-prevent-close" href="#section12">Sensitive Personal Information</a></li>
<li><a class="mfp-prevent-close" href="#section13">Changes to our Privacy Policy</a></li>
<li><a class="mfp-prevent-close" href="#section14">Children's Privacy</a></li>
<li><a class="mfp-prevent-close" href="#section15">California Do Not Track Disclosures</a></li>
<li><a class="mfp-prevent-close" href="#section16">Copyright</a></li>
</ol>
<p><a name="section1"></a></p>
<h2>Summary</h2>
<p>This section summarizes how we obtain, store and use information about you. It is intended to provide a very general overview only. <b>It is not complete in and of itself and it must be read in conjunction with the corresponding full sections of this Privacy Policy.</b></p>
<ul>
<li style="list-style-type: none;">
<ul>
<li><b>Data controller</b>: Angel Internet Ltd</li>
</ul>
</li>
</ul>
<ul>
<li style="list-style-type: none;">
<ul>
<li><b>How we collect or obtain information about you</b>:
<ul>
<li>When you provide it to us (e.g. by contacting us, signing up for our e-newsletters, registering on the site, or commenting on blog posts)</li>
<li>From your use of our website, using cookies and similar technologies</li>
</ul>
</li>
</ul>
</li>
</ul>
<ul>
<li style="list-style-type: none;">
<ul>
<li><b>Information we collect</b>: name, email address, IP address, information from cookies, information about your computer or device (e.g. device and browser type), information about how you use our website (e.g. which pages you have viewed, the time when you view them and what you clicked on), and the geographical location from which you accessed our website (based on your IP address).</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>How we use your information</b>: for administrative and business purposes, to improve our business and website, to advertise goods and services, to analyze your use of our website, and in connection with our legal rights and obligations.</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>Disclosure of your information to third parties</b>: only to the extent necessary to run our business, where required by law or to enforce our legal rights.</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>Do we sell your information to third parties (other than in the course of a business sale or purchase or similar event)</b>: No</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>How long we retain your information</b>: for no longer than necessary, taking into account any legal obligations we have (e.g. to maintain records for tax purposes), any other legal basis we have for using your information (e.g. your consent, performance of a contract with you or our legitimate interests as a business). For specific retention periods in relation to certain information which we collect from you, please see the main section below entitled <a class="mfp-prevent-close" href="#section7"><i class="mfp-prevent-close">How long we retain your information</i></a>.</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>How we secure your information</b>: using appropriate technical and organizational measures such as storing your information on secure servers, encrypting transfers of data to or from our servers using Secure Sockets Layer (SSL) technology, and only granting access to your information where necessary.</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>Use of cookies and similar technologies</b>: we use cookies and similar information-gathering technologies such as web beacons on our website including essential, functional, analytical and targeting cookies. For more information, please see our <a class="mfp-prevent-close" href="/cookies-policy.asp">cookies policy</a>.</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>Transfers of your information outside the European Economic Area</b>: In certain circumstances we transfer your information outside of the European Economic Area, including to the United States of America. Where we do so, we will ensure appropriate safeguards are in place. The third parties we use who transfer your information outside the European Economic Area have self-certified themselves as compliant with the EU-U.S. Privacy Shield.</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>Use of automated decision making and profiling</b>: we do not use automated decision making or profiling.</li>
</ul>
</li>
<li style="list-style-type: none;">
<ul>
<li><b>Your rights in relation to your information</b>
<ul>
<li>To access your information and to receive information about its use</li>
<li>To have your information corrected and/or completed</li>
<li>To have your information deleted</li>
<li>To restrict the use of your information</li>
<li>To receive your information in a portable format</li>
<li>To object to the use of your information</li>
<li>To withdraw your consent to the use of your information</li>
<li>To complain to a supervisory authority</li>
</ul>
</li>
</ul>
</li>
</ul>
<ul>
<li><b>Sensitive personal information</b>: we do not knowingly or intentionally collect what is commonly referred to as 'sensitive personal information'. Please do not submit sensitive personal information about you to us. For more information, please see the main section below entitled <a class="mfp-prevent-close" href="#section12"><i class="mfp-prevent-close">Sensitive Personal Information</i></a>.</li>
</ul>
<p><a name="section2"></a></p>
<h2>Our details</h2>
<p>The data controller in respect of our website is Angel Internet Ltd, a limited liability company incorporated in England and Wales (company number 5585633). Our registered address is Regal Chambers, 49/51 Bancroft, Hitchin, SG5 1LL, United Kingdom. You can contact the data controller by writing to our registered address or you can email us <a class="mfp-prevent-close" href="/contact-us.asp">here</a>.</p>
<p>If you have any questions about this Privacy Policy, please contact the data controller.<br />
<a name="section3"></a></p>
<h2>Information we collect when you visit our website</h2>
<p>We collect and use information from website visitors in accordance with this section and the section entitled <a class="mfp-prevent-close" href="#section6"><i class="mfp-prevent-close">Disclosure and additional uses of your information</i></a>.</p>
<h3>Web server log information</h3>
<p>We use third party servers to host our website, administered by VeloxServ and eUKhost. Their privacy policies are available <a class="mfp-prevent-close" href="https://www.veloxserv.co.uk/privacy-policy/" target="_blank" rel="noopener">here</a> and <a class="mfp-prevent-close" href="https://www.eukhost.com/privacy-policy.php" target="_blank" rel="noopener">here</a> respectively. Our website server automatically logs the IP address you use to access our website as well as other information about your visit such as the pages accessed, information requested, the date and time of the request, the source of your access to our website (e.g. the website or URL (link) which referred you to our website), and your browser version and operating system.</p>
<p>Our servers are located within the European Economic Area (EEA).</p>
<h3>Use of website server log information for IT security purposes</h3>
<p>We and our third party hosting provider collect and store server logs to ensure network and IT security and so that the server and website remain uncompromised. This includes analyzing log files to help identify and prevent unauthorized access to our network, the distribution of malicious code, denial of services attacks and other cyber attacks, by detecting unusual or suspicious activity.</p>
<p>Unless we are investigating suspicious or potential criminal activity, we do not make, nor do we allow our hosting provider to make, any attempt to identify you from the information collected via server logs.</p>
<p><b>Legal basis for processing</b>: compliance with a legal obligation to which we are subject (Article 6(1)(c) of the General Data Protection Regulation).</p>
<p><b>Legal obligation</b>: we have a legal obligation to implement appropriate technical and organizational measures to ensure a level of security appropriate to the risk of our processing of information about individuals. Recording access to our website using server log files is such a measure.</p>
<p><b>Legal basis for processing</b>: our and a third party's legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interests</b>: we and our third party hosting provider have a legitimate interest in using your information for the purposes of ensuring network and information security.</p>
<h3>Use of website server log information to analyze website use and improve our website</h3>
<p>We use the information collected by our website server logs to analyze how our website users interact with our website and its features. For example, we analyze the number of visits and unique visitors we receive, the time and date of the visit, the location of the visit and the operating system and browser used.</p>
<p>We use the information gathered from the analysis of this information to improve our website. For example, we use the information gathered to change the information, content and structure of our website and individual pages based according to what users are engaging most with and the duration of time spent on particular pages on our website.</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest</b>: improving our website for our website users and getting to know our website users' preferences so our website can better meet their needs and desires.</p>
<h3>Cookies</h3>
<p>Cookies are data files which are sent from a website to a browser to record information about users for various purposes.</p>
<p>We use cookies and similar technologies on our website, including essential, functional, analytical and targeting cookies and web beacons. For further information on how we use cookies, please see our <a class="mfp-prevent-close" href="/cookies-policy.asp">cookies policy</a>.</p>
<p>You can reject some or all of the cookies we use on or via our website by changing your browser settings or by using our cookie control tool, but doing so can impair your ability to use our website or some or all of its features. For further information about cookies, including how to change your browser settings, please visit <a class="mfp-prevent-close" href="http://www.allaboutcookies.org" target="_blank" rel="noopener">All About Cookies</a> or see our cookies policy.<br />
<a name="section4"></a></p>
<h2>Information we collect when you contact us</h2>
<p>We collect and use information from individuals who contact us in accordance with this section and the section entitled <a class="mfp-prevent-close" href="#section6"><i class="mfp-prevent-close">Disclosure and additional uses of your information</i></a>.</p>
<h3>Email</h3>
<p>When you send an email to the email address displayed on our website we collect your email address and any other information you provide in that email (such as your name, telephone number and the information contained in any signature block in your email).</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest(s)</b>: responding to inquiries and messages we receive and keeping records of correspondence.</p>
<p><b>Legal basis for processing</b>: necessary to perform a contract or to take steps at your request to enter into a contract (Article 6(1)(b) of the General Data Protection Regulation).</p>
<p><b>Reason why necessary to perform a contract</b>: where your message relates to us providing you with services or taking steps at your request prior to providing you with our services (for example, providing you with information about such services), we will process your information in order to do so.</p>
<h4>Transfer and storage of your information</h4>
<p>We use a third party email provider to store emails you send us. Our third party email provider is Google Inc., located in the United States of America. Their privacy policy is available <a class="mfp-prevent-close" href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noopener">here</a>.</p>
<p>Emails you send us may be stored outside the European Economic Area on our third party email provider's servers in the United States of America. For further information please see the section of this privacy policy entitled <a class="mfp-prevent-close" href="#section9"><i class="mfp-prevent-close">Transfers of your information outside the European Economic Area</i></a>.</p>
<h3>Contact form</h3>
<p>When you contact us using our contact form, we collect your IP address (and user name, if you are logged in to the website). We also collect any other information you provide to us when you complete the contact form.</p>
<p>If you do not provide the mandatory information required by our contact form, you will not be able to submit the contact form and we will not receive your inquiry.</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest(s)</b>: responding to inquiries and messages we receive and keeping records of correspondence.</p>
<p><b>Legal basis for processing</b>: necessary to perform a contract or to take steps at your request to enter into a contract (Article 6(1)(b) of the General Data Protection Regulation).</p>
<p><b>Reason why necessary to perform a contract</b>: where your message relates to us providing you with services or taking steps at your request prior to providing you with our services (for example, providing you with information about such services), we will process your information in order to do so.</p>
<h4>Transfer and storage of your information</h4>
<p>Messages you send to us via our contact form will be transmitted by email.</p>
<p>We use a third party email provider to store emails. Our third party email provider is Google Inc., located in the United States of America. Their privacy policy is available <a class="mfp-prevent-close" href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noopener">here</a>.</p>
<p>Emails you send us may be stored outside the European Economic Area on our third party email provider's servers in the United States of America. For further information please see the section of this privacy policy entitled <a class="mfp-prevent-close" href="#section9"><i class="mfp-prevent-close">Transfers of your information outside the European Economic Area</i></a>.</p>
<h3>Phone</h3>
<p>When you contact us by phone, we collect your phone number and any information provided to us during your conversation with us.</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation)</p>
<p><b>Legitimate interest(s)</b>: responding to inquiries and messages we receive and keeping records of correspondence.</p>
<p><b>Legal basis for processing</b>: necessary to perform a contract or to take steps at your request to enter into a contract (Article 6(1)(b) of the General Data Protection Regulation).</p>
<p><b>Reason why necessary to perform a contract</b>: where your message relates to us providing you with services or taking steps at your request prior to providing you with our services (for example, providing you with information about such services), we will process your information in order to do so.</p>
<h4>Transfer and storage of your information</h4>
<p>Information about your call, such as your phone number and the date and time of your call, is processed by our third party telephone service provider, Microsoft Inc., located in the United States of America. Their privacy policy is available <a class="mfp-prevent-close" href="https://privacy.microsoft.com/en-gb/privacystatement" target="_blank" rel="noopener">here</a>.</p>
<p>Information about your phone call will be stored by our third party telephone service provider outside the European Economic Area. For further information about the safeguards used when your information is transferred outside the European Economic Area, see the section of this privacy policy below entitled <a class="mfp-prevent-close" href="#section9"><i class="mfp-prevent-close">Transfers of your information outside the European Economic Area</i></a>.</p>
<h3>Post</h3>
<p>If you contact us by post, we will collect any information you provide to us in any postal communications you send us.</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation)</p>
<p><b>Legitimate interest(s)</b>: responding to inquiries and messages we receive and keeping records of correspondence.</p>
<p><b>Legal basis for processing</b>: necessary to perform a contract or to take steps at your request to enter into a contract (Article 6(1)(b) of the General Data Protection Regulation).</p>
<p><b>Reason why necessary to perform a contract</b>: where your message relates to us providing you with services or taking steps at your request prior to providing you with our services (for example, providing you with information about such services), we will process your information in order to do so).<br />
<a name="section5"></a></p>
<h2>Information we collect when you register on our website</h2>
<p>We collect and use information from individuals who register on our website in accordance with this section and the section entitled <a class="mfp-prevent-close" href="#section6"><i class="mfp-prevent-close">Disclosure and additional uses of your information</i></a>.</p>
<p>When you register on our website, including by posting in the forum or reviewing a product or service in the directory, we collect your name, email address, IP address, information about your computer or device (e.g. device and browser type), and the geographical location from which you accessed our website (based on your IP address).</p>
<p><b>Legal basis for processing</b>: your consent (Article 6(1)(a) of the General Data Protection Regulation).</p>
<p><b>Consent</b>: you give your consent to us collecting your information, and sending you our emails, including advertiser emails, by registering on our website.</p>
<h4>Transfer and storage of your information</h4>
<p>Information we collect when you register on our website will be stored within the European Economic Area on our third party hosting providers' servers. Our third party hosting providers are VeloxServ and eUKhost. Their privacy policies are available <a class="mfp-prevent-close" href="https://www.veloxserv.co.uk/privacy-policy/" target="_blank" rel="noopener">here</a> and <a class="mfp-prevent-close" href="https://www.eukhost.com/privacy-policy.php" target="_blank" rel="noopener">here</a> respectively.</p>
<h4>Use of web beacons in emails</h4>
<p>We use technologies such as web beacons (small graphic files) in the emails we send to allow us to assess the level of engagement our emails receive by measuring information such as open rates and click through rates.</p>
<p>For more information on how we use web beacons in our emails, see our <a class="mfp-prevent-close" href="/cookies-policy.asp">cookies policy</a>.<br />
<a name="section6"></a></p>
<h2>Disclosure and additional uses of your information</h2>
<p>This section sets out the circumstances in which will disclose information about you to third parties and any additional purposes for which we use your information.</p>
<h3>Disclosure of your information to service providers</h3>
<p>We use a number of third parties to provide us with services which are necessary to run our business or to assist us with running our business. These include the following:</p>
<ul>
<li>Telephone provider(s), including Microsoft Inc. Their privacy policy is available <a class="mfp-prevent-close" href="https://privacy.microsoft.com/en-gb/privacystatement" target="_blank" rel="noopener">here</a>.</li>
<li>Email provider(s), including Google Inc., located in the United States of America. Their privacy policy is available <a class="mfp-prevent-close" href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noopener">here</a>.</li>
<li>IT service provider(s), including VeloxServ and eUKhost. Their privacy policies are available <a class="mfp-prevent-close" href="https://www.veloxserv.co.uk/privacy-policy/" target="_blank" rel="noopener">here</a> and <a class="mfp-prevent-close" href="https://www.eukhost.com/privacy-policy.php" target="_blank" rel="noopener">here</a> respectively.</li>
</ul>
<p>Your information will be shared with these service providers where necessary to provide you with the service you have requested, whether that is accessing our website or ordering services from us.</p>
<p><b>Legal basis for processing</b>: legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest relied on</b>: where we share your information with these third parties in a context other than where is necessary to perform a contract (or take steps at your request to do so), we will share your information with such third parties in order to allow us to run and manage our business efficiently.</p>
<p><b>Legal basis for processing</b>: necessary to perform a contract and/or to take steps at your request prior to entering into a contract (Article 6(1)(b) of the General Data Protection Regulation).</p>
<p><b>Reason why necessary to perform a contract</b>: we may need to share information with our service providers to enable us to perform our obligations under that contract or to take the steps you have requested before we enter into a contract with you.</p>
<h3>Disclosure of your information to other third parties</h3>
<p>We disclose your information to other third parties in specific circumstances, as set out below.</p>
<h4>Analytics provider</h4>
<p>Google Inc. collects information through our use of Google Analytics on our website. Google uses this information, including IP addresses and information from cookies, for a number of purposes, such as improving its Google Analytics service. Information is shared with Google on an aggregated and anonymized basis. To find out more about what information Google collects, how it uses this information and how to control the information sent to Google, see <a class="mfp-prevent-close" href="https://policies.google.com/technologies/partner-sites?hl=en-GB&amp;gl=uk" target="_blank" rel="noopener">here</a>.</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest(s)</b>: meeting our contractual obligations to Google under our <a class="mfp-prevent-close" href="https://www.google.com/analytics/terms/us.html" target="_blank" rel="noopener">Google Analytics Terms of Service</a>.</p>
<p>You can opt out of Google Analytics by installing the <a class="mfp-prevent-close" href="https://tools.google.com/dlpage/gaoptout" target="_blank" rel="noopener">Google Analytics Opt-out Browser Add-on</a>.</p>
<h5>Transfer and storage of your information</h5>
<p>Information collected by Google Analytics is stored outside the European Economic Area on Google's servers in the United States of America.</p>
<p>For further information about the safeguards used when your information is transferred outside the European Economic Area, see the section of this privacy policy below entitled Transfers of your information outside the European Economic Area.</p>
<h4>Business operations</h4>
<p>Sharing your information with third parties, which are either related to or associated with the running of our business, where it is necessary for us to do so. These third parties include our accountants, advisors, business partners and insurers. Further information on each of these third parties is set out below.</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest</b>: running and managing our business efficiently.</p>
<h5>Accountants</h5>
<p>We share information with our accountants for tax purposes. For example, we share invoices we issue and receive with our accountants for the purpose of completing tax returns and our end of year accounts.</p>
<p>Our accountants are located in the United Kingdom.</p>
<h5>Advisors</h5>
<p>Occasionally, we obtain advice from advisors, such as accountants, financial advisors, lawyers and public relations professionals. We will share your information with these third parties only where it is necessary to enable these third parties to be able to provide us with the relevant advice.</p>
<p>Our advisors are located in the United Kingdom and the United States of America.</p>
<h5>Business partners</h5>
<p>Business partners are businesses we work with which provide goods and services which are complementary to our own or which allow us to provide goods or services which we could not provide on our own. We share information with our business partners where you have requested services which they provide whether independently from, or in connection with or own services.</p>
<p>Our business partners are located in the United Kingdom and the United States of America.</p>
<h5>Insurers</h5>
<p>We will share your information with our insurers where it is necessary to do so, for example in relation to a claim or potential claim we receive or make or under our general disclosure obligations under our insurance contract with them.</p>
<p>Our insurers are located in the United Kingdom.</p>
<h4>Business or asset sale</h4>
<p>We may share your information with a prospective or actual purchaser or seller in the context of a business or asset sale or acquisition by us, a merger or similar business combination event, whether actual or potential.</p>
<p><b>Legal basis for processing</b>: legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest(s)</b>: sharing your information with a prospective purchaser, seller or similar person in order to allow such a transaction to take place.</p>
<h3>Disclosure and use of your information for legal reasons</h3>
<h4>Indicating possible criminal acts or threats to public security to a competent authority</h4>
<p>If we suspect that criminal or potential criminal conduct has been occurred, we will in certain circumstances need to contact an appropriate authority, such as the police. This could be the case, for instance, if we suspect that we fraud or a cyber crime has been committed or if we receive threats or malicious communications towards us or third parties.</p>
<p>We will generally only need to process your information for this purpose if you were involved or affected by such an incident in some way.</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interests</b>: preventing crime or suspected criminal activity (such as fraud).</p>
<h4>In connection with the enforcement or potential enforcement our legal rights</h4>
<p>We will use your information in connection with the enforcement or potential enforcement of our legal rights, including, for example, sharing information with debt collection agencies if you do not pay amounts owed to us when you are contractually obliged to do so. Our legal rights may be contractual (where we have entered into a contract with you) or non-contractual (such as legal rights that we have under copyright law or tort law).</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest:</b> enforcing our legal rights and taking steps to enforce our legal rights.</p>
<h4>In connection with a legal or potential legal dispute or proceedings</h4>
<p>We may need to use your information if we are involved in a dispute with you or a third party for example, either to resolve the dispute or as part of any mediation, arbitration or court resolution or similar process.</p>
<p><b>Legal basis for processing: </b>our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest(s):</b> resolving disputes and potential disputes.</p>
<h4>For ongoing compliance with laws, regulations and other legal requirements</h4>
<p>We will use and process your information in order to comply with legal obligations to which we are subject. For example, we may need to disclose your information pursuant to a court order or subpoena if we receive one.</p>
<p><b>Legal basis for processing</b>: compliance with a legal obligation (Article 6(1)(c) of the General Data Protection Regulation).</p>
<p><b>Legal obligation(s)</b>: legal obligations to disclose information which are part of the laws of England and Wales or if they have been integrated into the United Kingdom's legal framework (for example in the form of an international agreement which the United Kingdom has signed).</p>
<p><b>Legal basis for processing</b>: our legitimate interests (Article 6(1)(f) of the General Data Protection Regulation).</p>
<p><b>Legitimate interest</b>: where the legal obligations are part of the laws of another country and have not been integrated into the United Kingdom's legal framework, we have a legitimate interest in complying with these obligations.<br />
<a name="section7"></a></p>
<h2>How long we retain your information</h2>
<p>This section sets out how long we retain your information. We have set out specific retention periods where possible. Where that has not been possible, we have set out the criteria we use to determine the retention period.</p>
<h3>Retention periods</h3>
<p><b>Server log information</b>: we retain information on our server logs for 24 months.</p>
<p><b>Registration information</b>: when you register on our website, we retain information related to your account for as long as you remain a member of the website.</p>
<p><b>Order information</b>: when you place an order for services, we retain any paper records for six years following the end of the financial year in which you placed your order, in accordance with our legal obligation to keep records for tax purposes.</p>
<h3>Criteria for determining retention periods</h3>
<p>In any other circumstances, we will retain your information for no longer than necessary, taking into account the following:</p>
<ul>
<li>The purpose(s) and use of your information both now and in the future (such as whether it is necessary to continue to store that information in order to continue to perform our obligations under a contract with you or to contact you in the future);</li>
<li>Whether we have any legal obligation to continue to process your information (such as any record-keeping obligations imposed by relevant law or regulation);</li>
<li>Whether we have any legal basis to continue to process your information (such as your consent);</li>
<li>How valuable your information is (both now and in the future);</li>
<li>Any relevant agreed industry practices on how long information should be retained;</li>
<li>The levels of risk, cost and liability involved with us continuing to hold the information;</li>
<li>How hard it is to ensure that the information can be kept up to date and accurate; and</li>
<li>Any relevant surrounding circumstances (such as the nature and status of our relationship with you).</li>
</ul>
<p><a name="section8"></a></p>
<h2>How we secure your information</h2>
<p>We take appropriate technical and organizational measures to secure your information and to protect it against unauthorized or unlawful use and accidental loss or destruction, including:</p>
<ul>
<li>Only sharing and providing access to your information to the minimum extent necessary, subject to confidentiality restrictions where appropriate, and on an anonymized basis wherever possible;</li>
<li>Using secure servers to store your information;</li>
<li>Verifying the identity of any individual who requests access to information prior to granting them access to information;</li>
<li>Using Secure Sockets Layer (SSL) software to encrypt any information you submit to us via our website and any payment transactions you make on or via our website.</li>
</ul>
<h3>Transmission of information to us by email</h3>
<p>Transmission of information over the internet is not entirely secure, and if you submit any information to us over the internet (whether by email, via our website or any other means), you do so entirely at your own risk.</p>
<p>We cannot be responsible for any costs, expenses, loss of profits, harm to reputation, damages, liabilities or any other form of loss or damage suffered by you as a result of your decision to transmit information to us by such means.<br />
<a name="section9"></a></p>
<h2>Transfers of your information outside the European Economic Area</h2>
<p>Your information will be transferred and stored outside the European Economic Area (EEA) in the circumstances set out below. We will also transfer your information outside the EEA or to an international organization in order to comply with legal obligations to which we are subject (compliance with a court order, for example). Where we are required to do so, we will ensure appropriate safeguards and protections are in place.</p>
<h3>Email</h3>
<p>Information you submit to us by email is transferred outside the EEA and stored on our third party email provider's servers. Our third party email provider is Google Inc., located in the United States of America. Their privacy policy is available <a class="mfp-prevent-close" href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noopener">here</a>.</p>
<p><b>Country of storage</b>: United States of America. This country is not subject to an adequacy decision by the European Commission.</p>
<p><b>Safeguard(s) used</b>: Google has self-certified its compliance with the EU-U.S. Privacy Shield which is available <a class="mfp-prevent-close" href="https://www.privacyshield.gov/welcome" target="_blank" rel="noopener">here</a>. The EU-U.S. Privacy Shield is an approved certification mechanism under Article 42 of the General Data Protection Regulation, which is permitted under Article 46(2)(f) of the General Data Protection Regulation. You can access the European Commission page on the EU-U.S. Privacy Shield <a class="mfp-prevent-close" href="https://ec.europa.eu/info/law/law-topic/data-protection/data-transfers-outside-eu/eu-us-privacy-shield_en" target="_blank" rel="noopener">here</a>.</p>
<h3>Google Analytics</h3>
<p>Information collected by Google Analytics (your IP address and actions you take in relation to our website) is transferred outside the EEA and stored on Google's servers. Their privacy policy is available <a class="mfp-prevent-close" href="https://policies.google.com/privacy?hl=en" target="_blank" rel="noopener">here</a>.</p>
<p><b>Country of storage</b>: United States of America. This country is not subject to an adequacy decision by the European Commission.</p>
<p><b>Safeguard(s) used</b>: Google has self-certified its compliance with the EU-U.S. Privacy Shield which is available <a class="mfp-prevent-close" href="https://www.privacyshield.gov/welcome" target="_blank" rel="noopener">here</a>. The EU-U.S. Privacy Shield is an approved certification mechanism under Article 42 of the General Data Protection Regulation, which is permitted under Article 46(2)(f) of the General Data Protection Regulation. You can access the European Commission decision on the adequacy of the EU-U.S. Privacy Shield <a class="mfp-prevent-close" href="http://ec.europa.eu/justice/data-protection/international-transfers/adequacy/index_en.htm" target="_blank" rel="noopener">here</a>.<br />
<a name="section10"></a></p>
<h2>Your rights in relation to your information</h2>
<p>Subject to certain limitations on certain rights, you have the following rights in relation to your information, which you can exercise by writing to us at Regal Chambers, 49/51 Bancroft, Hitchin, SG5 1LL, United Kingdom or by emailing us <a class="mfp-prevent-close" href="/contact-us.asp">here</a>.</p>
<ul>
<li><b>To request access to your information</b> and information related to our use and processing of your information;</li>
<li><b>To request the correction or deletion</b> of your information;</li>
<li><b>To request that we restrict our use</b> of your information;</li>
<li><b>To receive information which you have provided to us in a structured, commonly used and machine-readable format</b> (e.g. a CSV file) and the right to have that information transferred to another data controller (including a third party data controller);</li>
<li><b>To object to the processing of your information for certain purposes</b> (for further information, see the section below entitled <em><a class="mfp-prevent-close" href="#section11">Your right to object to the processing of your information for certain purposes</a></em>); and</li>
<li><b>To withdraw your consent to our use of your information </b>at any time where we rely on your consent to use or process that information. Please note that if you withdraw your consent, this will not affect the lawfulness of our use and processing of your information on the basis of your consent before the point in time when you withdraw your consent.</li>
</ul>
<p>In accordance with Article 77 of the General Data Protection Regulation, you also have the right to lodge a complaint with a supervisory authority, in particular in the Member State of your habitual residence, place of work or of an alleged infringement of the General Data Protection Regulation.</p>
<p>For the purposes of the UK, the supervisory authority is the Information Commissioner's Office (ICO), the contact details of which are available <a class="mfp-prevent-close" href="https://ico.org.uk/global/contact-us/" target="_blank" rel="noopener">here</a>.</p>
<h3>Further information on your rights in relation to your personal data as an individual</h3>
<p>The above rights are provided in summary form only and certain limitations apply to many of these rights. For further information about your rights in relation to your information, including any limitations which apply, please visit the following pages on the ICO's website:</p>
<ul>
<li><a class="mfp-prevent-close" href="https://ico.org.uk/for-organisations/guide-to-the-general-data-protection-regulation-gdpr/individual-rights/" target="_blank" rel="noopener">GDPR Individual Rights</a>; and</li>
<li><a class="mfp-prevent-close" href="https://ico.org.uk/your-data-matters/" target="_blank" rel="noopener">Your data matters</a></li>
</ul>
<p>You can also find out further information about your rights, as well as information on any limitations which apply to those rights, by reading the underlying legislation contained in Articles 12 to 22 and 34 of the General Data Protection Regulation, which is available <a class="mfp-prevent-close" href="https://ec.europa.eu/info/law/law-topic/data-protection_en" target="_blank" rel="noopener">here</a>.</p>
<h3>Verifying your identity where you request access to your information</h3>
<p>Where you request access to your information, we are required by law to use all reasonable measures to verify your identity before doing so.</p>
<p>These measures are designed to protect your information and to reduce the risk of identity fraud, identity theft or general unauthorized access to your information.</p>
<h4>How we verify your identity</h4>
<p>Where we possess appropriate information about you on file, we will attempt to verify your identity using that information.</p>
<p>If it is not possible to identity you from such information, or if we have insufficient information about you, we may require original or certified copies of certain documentation in order to be able to verify your identity before we are able to provide you with access to your information.</p>
<p>We will be able to confirm the precise information we require to verify your identity in your specific circumstances if and when you make such a request.<br />
<a name="section11"></a></p>
<h2>Your right to object to the processing of your information for certain purposes</h2>
<p>You have the following rights in relation to your information, which you may exercise by writing to us at Regal Chambers, 49/51 Bancroft, Hitchin, SG5 1LL, United Kingdom or by emailing us <a class="mfp-prevent-close" href="/contact-us.asp">here</a>.</p>
<ul>
<li>To object to us using or processing your information where we use or process it in order to <b>carry out a task in the public interest</b> <b>or for our legitimate interests</b>, including 'profiling' (i.e. analyzing or predicting your behavior based on your information) based on any of these purposes; and</li>
<li>To object to us using or processing your information for <b>direct marketing purposes</b> (including any profiling we engage in that is related to such direct marketing).</li>
</ul>
<p>You may also exercise your right to object to us using or processing your information for direct marketing purposes by:</p>
<ul>
<li><b>Clicking the unsubscribe link</b> contained at the bottom of any marketing email we send to you and following the instructions which appear in your browser following your clicking on that link;</li>
<li><b>Sending an email</b> to us <a class="mfp-prevent-close" href="/contact-us.asp">here</a>, asking that we stop sending you marketing communications or by including the words &quot;UNSUBSCRIBE&quot;.</li>
</ul>
<p>For more information on how to object to our use of information collected from cookies and similar technologies, please see the section entitled <i>How to accept or reject cookies</i> in our <a class="mfp-prevent-close" href="/cookies-policy.asp">cookies policy</a>.<br />
<a name="section12"></a></p>
<h2>Sensitive Personal Information</h2>
<p>'Sensitive personal information' is information about an individual that reveals their racial or ethnic origin, political opinions, religious or philosophical beliefs, or trade union membership, genetic information, biometric information for the purpose of uniquely identifying an individual, information concerning health or information concerning a natural person's sex life or sexual orientation.</p>
<p>We do not knowingly or intentionally collect sensitive personal information from individuals, and you must not submit sensitive personal information to us.</p>
<p>If, however, you inadvertently or intentionally transmit sensitive personal information to us, you will be considered to have explicitly consented to us processing that sensitive personal information under Article 9(2)(a) of the General Data Protection Regulation. We will use and process your sensitive personal information for the purposes of deleting it.<br />
<a name="section13"></a></p>
<h2>Changes to our Privacy Policy</h2>
<p>We update and amend our Privacy Policy from time to time.</p>
<h3>Minor changes to our Privacy Policy</h3>
<p>Where we make minor changes to our Privacy Policy, we will update our Privacy Policy with a new effective date stated at the beginning of it. Our processing of your information will be governed by the practices set out in that new version of the Privacy Policy from its effective date onward.</p>
<h3>Major changes to our Privacy Policy or the purposes for which we process your information</h3>
<p>Where we make major changes to our Privacy Policy or intend to use your information for a new purpose or a different purpose than the purposes for which we originally collected it, we will notify you by email (where possible) or by posting a notice on our website.</p>
<p>We will provide you with the information about the change in question and the purpose and any other relevant information before we use your information for that new purpose.</p>
<p>Wherever required, we will obtain your prior consent before using your information for a purpose that is different from the purposes for which we originally collected it.<br />
<a name="section14"></a></p>
<h2>Children's Privacy</h2>
<p>Because we care about the safety and privacy of children online, we comply with the Children's Online Privacy Protection Act of 1998 (COPPA). COPPA and its accompanying regulations protect the privacy of children using the internet. We do not knowingly contact or collect information from persons under the age of 18. The website is not intended to solicit information of any kind from persons under the age of 18.</p>
<p>It is possible that we could receive information pertaining to persons under the age of 18 by the fraud or deception of a third party. If we are notified of this, as soon as we verify the information, we will, where required by law to do so, immediately obtain the appropriate parental consent to use that information or, if we are unable to obtain such parental consent, we will delete the information from our servers. If you would like to notify us of our receipt of information about persons under the age of 18, please do so by sending an email to us <a class="mfp-prevent-close" href="/contact-us.asp">here</a>.<br />
<a name="section15"></a></p>
<h2>California Do Not Track Disclosures</h2>
<p>&quot;Do Not Track&quot; is a privacy preference that users can set in their web browsers. When a user turns on a Do Not Track signal in their browser, the browser sends a message to websites requesting that they do not track the user. For information about Do Not Track, please visit <a class="mfp-prevent-close" href="http://www.allaboutdnt.org" target="_blank" rel="noopener">All About Do Not Track</a>.</p>
<p>At this time, we do not respond to Do Not Track browser settings or signals. In addition, we use other technology that is standard to the internet, such as web beacons, and other similar technologies, to track visitors to the website. Those tools may be used by us and by third parties to collect information about you and your internet activity, even if you have turned on the Do Not Track signal. For information on how to opt out from tracking technologies used on our website, see our <a class="mfp-prevent-close" href="/cookies-policy.asp">cookies policy</a>.<br />
<a name="section16"></a></p>
<h2>Copyright</h2>
<p>The copyright in this Privacy Policy is either owned by, or licensed to, us and is protected by copyright laws around the world and copyright protection software. All intellectual property rights in this document are reserved.</p>
      <!-- InstanceEndEditable -->
		<!--ZOOMSTOP-->
	</div>
	
	<div id="ad_bar_div">
		<div id="ad_bar_tall">
			
		<div>
			<a target="_blank" href="http://www.storefeeder.com/" rel="nofollow" onclick="return(trackOutboundLink(this.href, 'Ad', 'Sidebar Banner', 'StoreFeeder'));">
				<img src="/images/das/sidebar/storefeeder-2017-07-20.gif" alt="StoreFeeder" width="180" height="150" />
			</a>
		</div>
		
		<div>
			<a target="_blank" href="https://algopix.com/?utm_campaign=WebRetailerSideBanner&utm_source=webretailerSideBanner" rel="nofollow" onclick="return(trackOutboundLink(this.href, 'Ad', 'Sidebar Banner', 'Algopix'));">
				<img src="/images/das/sidebar/Algopix-2019-04-01.gif" alt="Algopix" width="180" height="150" />
			</a>
		</div>
		
		<div>
			<a target="_blank" href="https://www.sellware.com/features-wr/?utm_medium=banner-ad&utm_source=webretailer&utm_campaign=sidebar-banner" rel="nofollow" onclick="return(trackOutboundLink(this.href, 'Ad', 'Sidebar Banner', 'Sellware'));">
				<img src="/images/das/sidebar/sellware-20160630.gif" alt="Sellware" width="180" height="150" />
			</a>
		</div>
		
		<div>
			<a target="_blank" href="https://streetpricer.com/prices/free_WebRetailer?utm_source=WebRetailer&utm_medium=SidebarBanner&utm_campaign=201903" rel="nofollow" onclick="return(trackOutboundLink(this.href, 'Ad', 'Sidebar Banner', 'StreetPricer'));">
				<img src="/images/das/sidebar/StreetPricer-2019-02-28.png" alt="StreetPricer" width="180" height="150" />
			</a>
		</div>
		
		<div class="omega">
			<a target="_blank" href="https://www.thompsonandholt.com/amazon-monitor-and-protect" rel="nofollow" onclick="return(trackOutboundLink(this.href, 'Ad', 'Sidebar Banner', 'Thompson & Holt'));">
				<img src="/images/das/sidebar/ThompsonHolt-2018-08-16.gif" alt="Thompson & Holt" width="180" height="150" />
			</a>
		</div>
		
	<script type="text/javascript">
		ga('send', 'event', 'Impression', 'Sidebar', 'StoreFeeder,Algopix,Sellware,StreetPricer,Thompson & Holt', {'nonInteraction': 1});
	</script>
	
		</div>
	</div>
	
</div>

	<footer id="pagefooter">
		<div class="grid-70 tablet-grid-70">
			<ul class="pipedlist">
				<li><a href="/about.asp">About Us</a></li>
				<li><a href="/advertising.asp">Advertising</a></li>
				<li><a href="/directory-listings.asp">Vendors</a></li>
				<li><a href="/contributors.asp">Contributors</a></li>
				<li><a href="/media.asp">Press</a></li>
				<li><a rel="nofollow" href="/contact-us.asp">Contact Us</a></li>
				
					
				
			</ul>
		</div>
		<div class="grid-30 tablet-grid-30" id="social">
			<ul>
				<li>Follow: </li>
				<li id="twitter"><a href="https://twitter.com/webretailer"><img src="/images/design/shim-transparent.png" alt="Twitter" /></a></li>
				<li id="facebook"><a href="https://www.facebook.com/WebRetailer"><img src="/images/design/shim-transparent.png" alt="Facebook" /></a></li>
				<li id="linkedin"><a href="https://www.linkedin.com/company/web-retailer"><img src="/images/design/shim-transparent.png" alt="LinkedIn" /></a></li>
				<li id="youtube"><a href="https://www.youtube.com/c/Webretailer" rel="publisher"><img src="/images/design/shim-transparent.png" alt="YouTube" /></a></li>
				<li id="rss"><a href="https://feeds.feedburner.com/WebRetailerBlog"><img src="/images/design/shim-transparent.png" alt="RSS" /></a></li>
			</ul>
		</div>
		<div class="clear"></div>
		<div class="grid-100 hr"><div></div></div>
		<div class="grid-100">
			<ul class="pipedlist">
				<li><a href="/terms.asp">Terms of Use</a></li>
				<li><a href="/user-content-agreement.asp">User Content Agreement</a></li>
				<li><a href="/privacy.asp">Privacy Policy</a></li>
				<li><a href="/cookies-policy.asp">Cookies Policy</a></li>
				<li>&copy; 2019 Angel Internet Ltd</li>
			</ul>
		</div>
		<div class="grid-100 hr"><div></div></div>
		<div class="grid-100">
			When you click on links and make a purchase, this can result in Web Retailer earning a commission. 
			Affiliate programs we take part in include, but are not limited to, the eBay Partner Network
			and Amazon Associates Program.
		</div>
	</footer>
	
<!-- if not admin page -->
</div>

	

</body>

<!-- InstanceEnd --></html>