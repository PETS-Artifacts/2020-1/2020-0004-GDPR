
<html>
<head>
  
  <title>Visitor Prioritization Waiting Room page</title>
  
  
  
  
  <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  

    
    <META HTTP-EQUIV="Refresh" CONTENT="20">
    
    

    </head><body>
  


    
    
  



<style type="text/css">
body {background-color:#7f7f7f;margin:0;padding:0;font-size:14px;color:#585858;font-family:Helvetica,Arial,sans-serif;}
h1 { font-size:2em; padding:20px; color:#ffffff;}
h2 { font-size:1.5em;font-weight:bold;}
h3 { font-size:1.125em;font-weight:bold;}
.promoheader {background:#cc0000;width:100%;height:70px;}
.header {background:url(/vp-assets/repeat-top.jpg) repeat-x;text-align:center;height:150px;}
.content {background:#ffffff;text-align:center;width:940px;margin:0 auto;padding:30px 0 16px 0;}
.content img { display: block; border: 0; max-width: 100%;margin-left:auto;margin-right:auto;text-align:center;padding-left:10px;}
a { text-decoration:underline; color: #585858; cursor:pointer; }
a:hover { color: #C90000; text-decoration:underline; }
li {list-style:none;line-height:20px;align:center;margin-left:-46px;padding:0;}
</style>

<div class="header">
<img src="/vp-assets/header.jpg" alt="CVS.com&reg;">
</div>

<div class="content">
   <div class="promoheader">
      <h1>We're sorry but the site is unavailable at the moment.</h1>
   </div>
  <h2>We apologize.  Please check back in a few minutes.</h2>
  <p>If you have questions or require assistance, call <strong>1-888-607-4287</strong>.</p>
  <p>If you'd like to refill a prescription for in-store pick up, call <strong>1-800-SHOP CVS</strong> and choose option 1.</p>
  <p style="padding-top:20px;">You can still visit one of these CVS/pharmacy<sup>&reg;</sup> sites:</p>
  <ul>
    <li><a href="http://www.cvs.com/photo">CVS Photo</a></li>
    <li><a href="http://www.cvs.com/minuteclinic">MinuteClinic</a></li>
    <li><a href="http://www.cvs.com/extracare/home">ExtraCare</a></li>
    <li><a href="http://www.cvs.com/pharmacy">Pharmacy</a></li>
  </ul>
  <p style="padding-top:20px;">Or one of these CVS Health sites:</p>
  <ul>
    <li><a href="http://www.caremark.com/">Caremark.com</a></li>
    <li><a href="http://www.cvshealth.com/">CVSHealth.com</a></li>
  </ul>
  <p style="height:28px;"></p>
  <p style="font-size:.825em;">&copy; Copyright 1999 - 2014 CVS.com</p>	
</div>


</body></html>
