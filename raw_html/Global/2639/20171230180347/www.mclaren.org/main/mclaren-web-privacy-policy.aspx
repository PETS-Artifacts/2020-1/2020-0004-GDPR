

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html id="html" xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head id="head"><meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link id="lnkSiteType" rel="stylesheet" type="text/css" href="/SiteTypes/Default.master.css.aspx?aud=Main&amp;rol=Public" /><title>
	Website Privacy Policy | McLaren Health Care
</title>
<!--Admin > Properties: HeaderHtml-->
<link type="text/css" href="/Uploads/Public/Documents/dropdownlinks.css" rel="stylesheet" />
<link rel="shortcut icon" href="/Resource.ashx?sn=favicon" />
<link href="/Resource.ashx?sn=slider" rel="stylesheet" type="text/css" />
<!--End of Admin > Properties: HeaderHtml-->
<!--Design > Styles (FY18 Site Design): HeaderHtml-->
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400,500,700,800,900,500italic,400italic,100italic,900italic,300italic,700italic,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">
<link rel="icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->

<!--[if IE 7]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE7-STYLES.css?v=2">
<![endif]-->

<!--[if IE 8]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE8-STYLES.css?v=2">
<![endif]-->

<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<!--End of Design > Styles: HeaderHtml-->
<!-- Site Architecture > Audiences (McLaren Health Care): HeaderHtml-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6396952-25', 'auto');
  ga('send', 'pageview');

</script>
  <script type="text/javascript">
    var _monsido = _monsido || [];
    _monsido.push(['_setDomainToken', 'VW4sxwq_5Jph8JUIFq9uyQ']);
  </script>
  <script src="//cdn.monsido.com/tool/javascripts/monsido.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link type="text/css" href="/Uploads/Public/Documents/mhc_custom.css" rel="stylesheet" />
<!--End of Site Architecture > Audiences-->
<meta name="robots" content="ALL, FOLLOW, INDEX" />
<meta name="rating" content="GENERAL" />
<meta name="revisit-after" content="30 days" />
<link href="/Integrations/JQuery/Themes/Stable/Root/jquery-ui.css?v8.6.6" rel="stylesheet" type="text/css" />
<style type="text/css">
/* Site Architecture > Audiences (McLaren Health Care): HeaderStyles */ 
.mobile-navigation-link-visitor{display:none;}
/* End of Site Architecture > Audiences: HeaderStyles */

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
/* Admin > Properties: HeaderStyles */ 
a.dropdownlinks:link{font-size:12px ! important;}
a.dropdownlinks:visited{font-size:12px ! important;}
a.dropdownlinks:hover{font-size:12px ! important;}
a.dropdownlinks:active{font-size:12px ! important;}
/* End of Admin > Properties: HeaderStyles */
.mobile-navigation-accordion { cursor:pointer; }
.mobile-navigation-accordion span.expanded { padding-left:17px; }
.mobile-navigation-accordion span.collapsed { padding-left:17px; }
.mobile-navigation-accordion span.expanded { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-expand.png') no-repeat center left; }
.mobile-navigation-accordion span.collapsed { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-closed.png') no-repeat center left; }
.cpsty_Left {display:none;}
    @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}
    

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#4c4c4c; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#4c4c4c; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#0065a4; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#4c4c4c; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
.CpButton { cursor:pointer; border:outset 1px #CCCCCC; background:#999999; color:#463E3F; font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 10px; font-weight:bold; padding: 1px 2px; background:url(/Integrations/Centralpoint/Resources/Controls/CpButtonBackground.gif) repeat-x left top; }
.CpButtonHover { border:outset 1px #000000; }
</style></head>
<body id="body" style="font-size:100%;">
    <form method="post" action="/main/mclaren-web-privacy-policy.aspx" id="frmMaster">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="G3h8G58VpQLVGfDEy3JIqc9v8Mck/y9Ug/Au0boBDH5qTma3uCIdoSpKrWZAJAU1ugnbB8JLBXCiDBbmP6zwEfpowMqxskyvu7uX4hH2X70BR83u0OFc4eI7JxvW64TRyJUbuT5HDU+Ihir/iXSDDyknBRVjbxOUwI9C/XC9Qfv3hZCaKxtPb8SMoPkcqzDdxmehQi+3jQUneK6lQ8htlON17vCezKZSw/ycpvOUxZsqJ0dN2MGWfmLJQ/xL67ayJCyIMYKGTCl3m4FiW8bw+MNaHP2oNYu7AFxf5qNqv70Uh/ikjetrjwHLLhJ+nrVwXy5uByvDGmYvR6uo/5dEoYm7VdvOIfjnuwWR1kIZA+5iA6CjRw6OA1xMXTzqKRgoCFt2SLTZUmfmMCkZiR136E72fE8MhexmN4GHBfyoo9DPCV98Zx8Wx/IE8n1XIT4cLE1gmjjNs7EZscUPBlLzM40JEBjpM6o/RE4j3FnFqDEYIMo4YfrmcNjEJj1b4fQg8sqTxzZSPn96o57j7S7NIKpOkMUP1BbJJcD7ZYVqVssBEAnkjccTcD3UI324sMjtuR+XOAwZgRX3WSiW7H1FkfEkGKHafhO6btfH4ofc8PIz34fS3ZywDQbbvnN1eCSQfnk443JLa9sJPmOs1JgJefryX0/cmDx1OdYAc1OCOiSuvEgwmxVSYvnCLbvNhGcmt9AIiBS6pBDOpVgmOalp8JNjUR7THmaCMx+X33gNKKaOp9ovunQRF4/tPhqwiL+PR8UGhsFRxM2057Tpaljx3g3vFIcz8nDpTXUs7hfXXrUaHW4cd/kV+nuY8OKdzXajtHLpa9SniH6n/kJfXGvVBkZuTNeIOqXFFLBa8feuBRRUysyvMP61zDvhI97q4/ygTzmuhGqWp+byV2LnwELOZAN9EpJVRMug/AdZA0fKf4/cXhyw593MBcMyerhsY8XrqJ38HDwxCKqTBut3ru8QBUfBL0D5pKmMtM5Ihmmo0sRBCtSifH8waX+FpBXQZQKnkQ3AvullxEbOM7g5ZxNncgivHhAj8lm58kCbH/eC1NuYxD7ubwqhPNcqXYFjKeKKE9uzVyJ5DQWlf30NO5ql9BPsJIIRTmPHFQ8nRRHg2f3VYvzP35UBKg19n7aBn5D/hAmZLeQJLppeoxFq7UvjW1UjyHcGKo0Ykf2tFd7fodC1D2xT0vb4LdrWQqUWEnrvfPW6NHoI+YSEyRk7MK4SR9ivV2x2sF5QNam3KM9GEpjGY+CUaVHPPTL6O+b/Lor+mpVQszqEER3+2gNFbHFCpZuQTvWHIYC20Wihc068iQae8zRnLoueiTitcqDJe7l0yA6a2n03ne8EArfPSyxpIp1bkawNSSVMuVXC1iTWwJ1tcNK9V6gUzPZJ/WO9T99BxJw5nbbSJzEGmqPjQtY/ulaAAWTXNTW138yAtgY1xiABP90FLPUqJr5RSWirmu+BV3htmEQ/qGk1FEejm9FdGTLW3NwwItVSmmF3PnO2rkQDBtEraKmNmhbZyetg2shKowVgQdCNq96GnnosfX/McPFOeuyI83SWPa67EEM8+AMw7btuXP1L4O2SBtm9yfD2ksMsevGQP3heFre410ZyGFZZuej4xACFypqlkHvZACRTaEbIjCYpN19qHyrC91Cie+fknZZNNryhOb64PtMoJM7sIAIn/I88+al9HhTYW8uLbN9BkJ56l1JONtwc8/P+OaVGeFPBeb0niRsfWWjqKdrUy4PB+FIJVXqgrG5CaSRBQzdiStAleV8wipOAIBWiAvc7vlsHILv64tq51t/RG4noVKMJqCP9ppANyNTFZOqgks3He96jsASGszYrqAC/NrW/Poc7FLK15uMl5a4sskNW/AToOVCgzux9FR4Sz+hOe5DIRs/OC2q6oOKnxygg2mmdQiL6AWqtK58Pgz9A6LU7Oh+/PGLzreXA43LVvIw0xQlGqwf+xi9w9ubee6Qt+l2sf9iQpluzNdFIUktqW+jsuM76H1XQx8nTE70+B/ZEDCDOtNQ4BU9mic0AhLkcUOhLrpT+T0L2UwJwitl92F2EQvKbIo6eRi1xsnAp8tE8icyUZepPBRyrLqHmYC3cNHYvQv7fFQEOJ48cKcrUC8ZhulQCNJLqMs9nzP0l21qHdbbqrh7NUbFs6pf7sBCmyzczilfvQgUfUqna5sBosov0zMDTBk1QcAKejK3OwX56eozNFEOsFIlXdlHfI+i9IRJR53d+RBX4GiGvoE0LGu68QMRZs50TUe+9yWJ4zwulrfKnExNtsYtlzL8WqoNdpjf224A5XRwFxrlBu6rHm+gttgL8aKvCalqN5a231uCn7WruBEjT8VDXeiOZd0TPKrCi2wZs3Ht1bUl69gVnyzDZSJH0QWZuu6oIew7Xv8KBeaTUcs1ZBMQaAKu4g5tw0Rs75sP+Nh62nCQRfcLPHrpBdaIqiiQ2VrpccWSF/yu4Z8w/4CRGeU6w25XQ922Q+/tpadr7aL0CE55s4SKaVumKf7iNkPW4zVuUo/snbUjwFNiUuBX56cLhsxF2158xhk7CABVhxg8PuVZTn3V5+Rr6mg3+cfYd6qjlzK39M8RajUg2K2P/zCQvqcxyE/93o2F5DrgxsOj+/geUcUbg+fIuNepSzECjXVuf2NR4eGbruqW3/4HZ4I2kqhBobTxELfTJ+N/+lw3A/Wdr6/te8QhJUMKSfNfpKqRjrok7JfNjiN6Dpnr3bussND9oL+udRz5t3lajSewbp2rxdDC85V5zmGNJcZ8n8PQPs+mw56xOMGTn1nj6Dx8txYP23a2I4IntNtkD1PvHnsiuhc69F7qzYVxb6/lYuv1ra8H2mbEjiFGbUl/ChAd1jduQq+bZCXkeNWlWXIPT+g56x1ZXqZ7Wk1d+qThOzXR1zv8YCUlW4UytOKAuY2jQqoYwG4LNB29JQU6z/L8z2CZK/jFEZVYczC6irMESJEknrUETJD49NdLf8rg+N4T4MbEwEY61q5FSXrEC6pERjecDK0BY0GUweh5Q3vwY5ycuexoUv7Pvg1RJ15FlTkZSWqsnqqK81bOgi2aKdGt4Pc82UtpKRo0Mr67Kv6qB5S6eY0SHcmlkCxbRWTG5YRp9KgFrxkZUZeBXP/gbrHSlOzKz81aTpQv3btsXAB+NtbiPfqJCnXvXHhyM/PF93gGcsHQQNQjePdm1gHNw73eEN8dpMlcuAVDu19SDqxPOYs6BUI7CoPDhlnkb9aTl9M2pddSQ41XgLdJZvTsopWWL80RXlPS0HF4X8lcHTEh3CAi1X42eOtcrND4HnS6Ja643dLpJn2gGVMIbR+T5ONrkG9//ore5p1xvDqZdtLX2fTjZS3GqNRVWyReov+4pthVDUoLrZKG2JnR6G7OVU5NfOcEMIT/66H0yqU32OJgxI5+7a7v4Yg3sr7ooR5gcOHvE9FP2Tg8OASqvosagvy6lH3vr3gIVtZ9ENJQUxv6lNvNj+HOcTQLx+ZCaI+M6vzNYsMddEdivoGORm3pt69e+TGZOL2/Dktej5ddLLTRlLh6g9oxKH9qQfSJzSqBYqOj0lGOOETMMQIAJy0u8ci+9Bih9RFV48muD2PhP8Kt5EIMXJETzJv1edMMz4h0l9V8eb3FGWSVMHWZPhL+mPmDYkguYDWivAmnuVuSMWR5kVNxZwBzx3lZI/agNwG5gszZuju9v2x5WCS9DH+RrVxV0erI4Urdi9UZFjtLXxbpaCMRC7EVQuaAe/aPsh3lJcvL8i0AVbiltiVVce5MkxvTLGqLINp4aTmExKgp04hqkLubKVv6vEb9pNX1ZRTsIGxMBFYp90h9jW537fVmxMocwO3NEE6HXjCmYczdJl7qg22ONzNmHdMSj2QbkZ7kYTFpwp7z0Eu0U0pJbbZpHSk1n74muX6wDIpSbYTuFLIctl8dUe0Qs5EZsc2B8Dnl0VvwUgqRixK/L/tWGT/EvwtN2wROlhcfMNPxds7BX0cuXDREy04YPNBAEIfHL45Aji3dFtaTEf1hyMnBypTpmhFga+8VlpkPJxIP6HpVFwqZFQPxp9RhlgzHCQSbikahFShuMRQnnIEm6Tu07YxgCJ816qY9bbdMX5gpwYFx8dOIrqKgPUu7ndwfiRkSfmv2ANgoDXAvIecr/UYvk0/vOED21bWQj0juzgDwKhhiVSxgibajUA+TsobzzZevvjouC5q/KcDKW/WUrSmmRAfFwznFRFOO2l1v5bojQ1sa/bC9yqF+3SQ7RVeGbXA6l0zXYTspZLiGK6x0pOhcctxBjjEzlUVYREPF5Q7IzJILZ3tbBiAdhn1qjrSydeBalxemB327WB495aFvY8o2oxTJAXHZgin9gZKcKidPbg+gTBn+NnVLFq9wA+wQoONMElSVNeGf+refqaP0toELF5yqnqAubNdVC2cTb9lig6SC8krH5Mmntkz03vXZHaK05qnfyvikbFT+VtdZ0/tXsLldpn8mJvJQ0WB2Zedw2yt/JJPutEVt7kz1ntw2I+5MOzYzBCROEmd2xw4KspRVu9ykFfD11gwZ7saVgPj90EUYKKYAzPmSGDjA1PHi38xY9PmNnH4/5/xAbsvpLO7it+vfchcFA33nqWh0Ex8VyicaS4yRhTAFbz0BHADd+K7/ksiWggPHk9Q3XDJtG/CPOspHS6/+jbnjW/7MA8hgYligjp4+zO6mfNR4s8gmgq8W1JL2iN/1KmWCIMUADf6ZlaUN2pp4/2oWxvnuPydIORvT6erwkbn+BUw4+mt3hBK3iRWnvCwjGPVF3rcm8/Oy3U3NKO4HJ8/IiXXHaYupWLzo5iHvzJN3GRt6k35mpmZShxh/Q8nLX5OyBiuolQ6PePmkmY0DBFCSLAt7TM7w6dZgX9Xjp31C3fI9WwFSaTOwkvJ4X3UEVmbg2NFIJ0fe4fc+bt2oQ9zYdIK1PnqZYYdegg4RKAHt0J6X65h+Aqh53QOXKhSByPojp/70t0puRRtTtR8LKNrOzzTptdAKb2SeQnAPOezwwwi76BkcKPKPqXbU0h5EXqNda/Iw2RQ8Axy116uF0047JF4+Ye+4Q5GGeZZosYof9gLhwFpHnd9Ml5cpdF2sqTcA26VUvCfrHVCd3vIl/MYwq8tTwlKpsOVxrrq7vo+nCOuMFQ3sDEYusv6r4IIqk9DQMz0qhgukX6jY4Eh7VnaJTjK+WIyqAd9q5DvFuPvFPRK/Er24/ZLH3E6v8P1Dvv8xR5i1as7cF3dA4YZ2LUr7Psl9TT9NsvVkR14+ws7LLQJV5/nFSaS4z7UMqi34+q5AEWO0lA6A8cPy9ZhW14yxMj0Vnq9AEJwIyTzNs/rfmbbj5mPZ4Idi7pwnBpSB4UichAjXEnj7eXK2jqYgRNlDMxbXX7wWTNKmkZAYhQBetZwuZnQkZaZ3PaEJhjng0c6oW1YeJjBLTqwYa9rIz7qVblFzHRC9o1kvXyudg5JqMRgSJ1hFCgP+CUhqutaTuqqBbL6mm9PJeTXE5CYbY3sd317iJbK1s+vh6qD4mqHxs2XVV+PoxqdOVeQ3EMyouAHaee5WSGWbWP9tlU9PZ3r/eqLIo4Rrh/SEO4no1yrXIlSTBQ/EtDg2j+vYlATj93Sh9xUcqm+fMD/bafveJ5DC0yJt9bavi7GeEuT+jHlgxa/HKZzgksXs5qAoCYdo//3Ijr7++ZjDaeP3Gn5SFIkqkznBU0AgexnhISkOD9HvJmgnH8Ez4byx+ptp0tBlI+X+lezngC7CjNk7moGgjYzQ7nwajhr5ImCRlegsWR246+st/NEB+LhZ/daegyU3ikCwcbVWMm0n1BirEnJG/IC1S6clTeQhtx7tGQbVNMK51DYl3HTdtjWpTJgAd0zA3eKpjcHSAZFTbUoc7m2lUMHlnWFJvcrfPneHIdn4qRWJ1wK3bBKJmkDysxe8jaM/t23tn1ypz4X8cGqJ/snFsgCVpwbYF0k5dwVIjH5lddtp0s2exF+lDMl2v7mNIh+6DBA218qxTtvlQv2HKxd6beHVckY7K/g9uHZqYRkVFFyfIjp/BctDMRXFC6j8BxjSD0A0zdB5xK1E7Og2TzOLPRlhVd6uYZIO3yGsrOizWv2SZ9jgXAheoTiKjEPYc9NzoVXVDKFXeJjeRUBBfP0zP9YjB+S360sMkN1TGtSEIjWx7Tv9CU1U4j9N0WiQXlqf6CrGfSQqY7BoKqOpyiRXlzmtPIniJVnISgnWjbRmTk4J2UE05cfhNMiIPbdtxttGkPv5QU/Y2pQT7SMwg8kZ4tilaOuAVwMwhFAJFILldIf2BRRB9fj8dyAy9dEe4SWekHEC5QI8NW/5Dj4Lcgrxs8B50vy2h7ZBWaosjZ+mhUhN49hqs8WMFLCW6KYwgGLr1FchV6tcaC36yM3eMbvunQQoGs1o49/5EbDhvqIthze+zumpCcvCeIpDKyXWU9lUlYCr37psIMTBv+1enVb/vspQNqie7qTSU92ZMFL+AizZ9KE2X+MJnu9M3ZLZ3IsA1hs002dk0j7vU8AXxkYWmA8PC3yAVUhhlcL9xfRIeZcEGJtqTe67Og2joQjrqMjHJFPxCfWbNXEJ/NghQlJEzovSMBUimmuLC/u4T1/z4KTM41I8lHug0ilF+7nJaxl3PdQLAITi3lSYWVf3MVK+nWZYi8YWNNM8zC5kf/mOxUL/eOkhC915k6fVXqh8kymw4oLvPTlQ7JjD628DRhHhC43GaoDxnj34gVaRvlzx6iVx+4t2Hu+RifmwUD6oSb+uwcbJ6AHYAULOjo8tU4AoFBoGXqoTcmtHTcR2S/dbkPWUAVRnck/MeUzQszrPxJVV3f5QNMSSnShD7+sDtDDLJz0LwLfEIxZGG+ng3wqUmEVGUs397pmh/cYoThhNoy1DcNotjt5EvT6KkISdYbv1Xm7+0fPa3NCvwwO7gHGdfRs2AG/McxdltAEL+exrhTJpoSjm4zTC2+Qt56rbcMpzKaLyWJ1cIw5q3+IJp4DR3Gjgnqw/eYbc75ldvBwnIrKftyTqi8V+mUnYOmUW7eOkuQQOsoAT2tV3dyrGiGoR+yh0QYafvTrmNePzbBdEyKC8f3xPC/LyOeUMebEv/07LU2u38l1MEnWDjUbxlwFVLVt++fbgGeYU1LhEgg4USp+cecqx4bzFMsnzOI/SoKNukDXC+HWpxdg2Kcodtw8+NoVgqxE7yJj7PJ0P7+3zTQ/0DBqHBlBnzndNA41NkjozbMwGw8fphsmS/3BqaOEnixsiqHI1KF9qG1nMDFZbWUQ9Z7bBLvdpOlDiuBFdw7FP6mu7KqNLaW+br7l4IxE2xTGN1NTXUhLytOctebqiSoqBQuKDneAhnzpMMzAnEDwsnMa9/xvWrUL6tJ0vTPi1alLGhs63I2q5pYr3vUhxl2zBEX8TDAT7P9OPbcNtpS4HfuiOW/yRJTgCjcbL2lPJVDuE0nfnPuPpCPdQryM3nRVueuNQBpR3rV2VTmtpjGewF9qK0JseWJWh50UsNKkax40j3thqNHY00UeBCyEf+wtbU5VgTlRTr1E5cNrglzlbKV8VNoGqXwWRj8CZaByJ+rsWqynshGV/IXy+SQH7sAw9aktk3f72MoJPBR6fDqv2Es9YWBdouotrVH1ybEamTgDEcOYOGhUzwyKSh6MF+KDmTIwXWxyWXc8XhKgG9QmZ3NzcxRRN2uDK3mLn8YP5EQsJpJrvbl0JI4WUf4k4rRdfUJ1hRFLcf3cOf+fYLtH/FmEoD7Fgv36RTZ3bohRnHWHiwxnNvSuhj6OJy20HshCAMCVvEyB/j0XUbDYsNL+PeUZts029k5TRF824bUpCmgTUALvmLHUksMzr1cuxh0UxnpZ7TGI4KHH04i04J+xd5pBp4K4SeazK5yCIwvhPXECIAmb7ebGGnmP71OSBmQiGtpCbMT1Z60jbNLPFSvgtzmxcfErvvUdHsCrPo930vqwgP2PeTyXjCq+dhQOxC6I14IMRLdqSDsVl3doMIhB9Hj8guH5QU132+T7nsNAqtu06wPJfKN2RpGLbv4yjg3+1/prRQeQtMP6gDQQs7TVDiMoGiPnd3M+ULuGfTlAG7jxs/n9rHL0gQYEKcO2mTSkdVwrHQp8+A0uaQXqJQbuJ/0F1tOXooYgZlrI8tvgeuvbUldhckHn5BcGWev2lRlmh5+A9StHp6aTaxixD8OQLvhoPT/+lUqtwITXb3oh/4DdZkmupd3+V9izSB/OMUk8XNG7Mu/3YfMDKvJCRxRn/V+n8d3VsSlqMXr8zKhzDkxZ8EXu36jyXRsUoXOCEyU8YShV3i721inNqoc0RY4akl9UxkbsUxUzHgKhNFZeToPGAX4Jf8IN3rjFHzoGNxAbxiyoA5nCqJ6skhshESA3992Y3rpDJ2bQLnrTym8DfyZShNBatSyj4Rj6OJgAzDGLOmyYbPk10a8ANfuhvYbmBFig5gVz/79Uao//A76/Eru/j6bLJHVeBHITYsm97vzDMRCudH1WFt1Lr3TVVd5ifyCp9fHBgNS4aYVqBiC/cGuST+yehLgWCUo4sbkPaKcR4YMzg9UsfvT/OU3QbzVB/KNMhDJ5t6OhflxaTVf3w3H74EIVYuqPz6BkVKOBah//nelE75IjF88NcFn4j7rv0z8HPS2UoF6KE1H6ZVJ6y/rimBGoMyNiE8HO+2daNSqtiD3gSMnqBwcsSu3NrFUBMQmgHZp7EMsADeHYXuZMkHFhnhy418wDmFVwS68rdbrK0MMv5dgLy/UTArKOMtvREgnB5CfxmBQ7OpC//wN8JxBo3v4gxnhyqEqdhT4IVxVxHR5GsxBN6yr0SJQspGSxleNY/yKD/GSKuGLYAONgK4t9wmBMtVhk9f3bVIRcl73MzempJPzHpKD3NEefrf2lGA8TFeYn8HJx2Kox/Mmmdl55dgzSy8qwBzfrjACrYvFHEBZy5ZN1mtog0ZMMAS0BlwqbPdGtOuHozNZRZAkS13SoziptB5Ad1ipuvO5gtbpUTjfv6z/4h+/H4EBoyZLAHu7mvXkMH9aDsUDW0pWxIJU3RuTLdeM360y77TvbCD3nyxb6+IOrg8YJ0COO4bSAlG+iA9dmUFv5tJC4D5P4q6wZ3vkSCVfozIDEaP0yiR8V8HS1Xp/ZhgQJ2YdkYPkuTaheZbDDF8IVJW8Mec8kODdh//myd9kK8hZO1T2L4EI3hEUfJQWHgUpxbgKABQrMJ1TBQOVYCJXR8lG2OW4Q3zLrh/+D4uLy+xVucn6QNNSAmV0AF6NooGA/EFmObXMEyW5S8vFPt3MXxO0p/1fGJlLP/xE+0mFUN4JBL5Yn+q4gx8fjG1k9BbwxSieUMsV4VWGNdUvPDPSpFdALS5+BhRQbPHt3jQItXV8tp63L9FMY3ooioYizKYfC1QD7F/OMv/us7ay+WLeWNiitTYtDN1R9I/6BbGpoB76OO2ayXKx5QTBZdkVPQpz4mD2csoVq/gOyfduQYg7gBYO8Ks2w1HQn/kgmD2UBWVmqeCfpbadDzepbyt/TVHIghiSTg2Krq3zWAm9+8t9vzJ2NmUdzE30K0YPda/RAuGLzi3VHqet5FgLJaSSlPGF8O/b5Z0pp0rZxHK3e4OzE0Vsv3uWEUUOdRa5kuVygwkQ+oiXZZwr9U2sjKmGREQu72ojER1YyxhlLDkrxqWWA7CqYMGlAfw896zj3YmQnCHGqWXX1gj748n4oFnFxn55avtvXwGYUMj0thX182U3+oD0QaQaNhrovtI2fQ9Wze3s9DzmjsPIxLVbKtYgTpGH/5YSgxJEGTP/YTBYH6S6BsJmybJrvvUnjrfiiqDEicSsg5cMTRsI2kQJJsJpDp0SLsBNz9gEzrBgIlRzBcJYuw1T2ZEoXtoI7YIPb1/9s3v9KWxeQZ8QIB8xCocE8155nOAp3VSBl0wQcUD+yI+YPUdCilFOUqVbuRso9CERdtYK/5TT1kwEEVHitTS9bkGVDJ7XCUY4L0T/o9JI0/asBOqtU7vLAmUs3klNzwhdA+qo4gnrbcn/WPu8JTlwCYBNBNjn2umAjA06cyjJzK72AowfXywYYJVsSYaLFPB7Sa6Q2cmuD3BaTys/qTKMy/A1EJnAF4QJZXnNuZjOTXJe+tPl77YlxzqUMLs+mcKrKDwhKXfTmW8ZIqGDZSutBreN2uEwT6z1tT5A18KiGLriSPqhSzUUl+sa5Kt8ZBQAEc8us7r9xLrZftK/PH2crB7KVSZw4i1W0JC/V5f0j6dakEuMDPxSX2FJq108/5VdL+WxlxORC/JecFHGmIgQSZEUv792n+SnFvC6Z4/oGhhboO63VLKW/KXe3tuChF43B9kwMWKMju36CfaFSGQHVxHwG6K5w66/wh0U6go0DFQCXUb/jn3DEjJxW/itnCE+48fqcK29UZeRaKOcNHjbJDCNSE0SWru/CjXc4rnXKL7FLcwCl3RIsoPsWDkrze75Zddu7nYKxPTsAHLE8MoyA5qDqUPX/rVqbvHrc5ea/+ZR76xPjaiETWJzWoNfHY/NOa9CFdnDKH2zGvg3MtTcTwO4n2xfScU7MwbMb17Kix65RiXRGBr638mMgKMaVPxlAJuk0jmWVwp3Hgw7uZfo/UYGiCr8cbX9PLqMDITdDkn5WyJ5FCQ185acTB0E+Y8gODpqt+34qGkl53Idpd2joQ+q8P7wk4qJcAqErEHoBjc2YenKxC2HXr8t22T6q6ZHwXHKc7SOOSBmPQRoS99jPE085cYz3ZxAN+ZFTe+xjck5cpuGJQ6y713KMXEuc6qQUBqUOVxS+y0CnjR1bQ70qD5KAiN97VMEZ9/g8fQKUosqPzb6e2oBDqs78DcR/0aEDSVK25R9dTre9I17tRk9MskxW4Q3N123TlYe6JX1vYCDVTp9ju20vEumGva5kSzCeSA/Uk8hFFW/lfUUaL27VgSsiiFuICrEmo38I8N/zt/JQG8EFtOYZGPYWPIH2R/h42FGjjaCQyrIqoxsIqNVBjLdeIIwYuG3VvJhv9X9M+CBNKq5AakbBMe0wpYBG/xBdY/j2lHhkgqEaA5s9gVX2RlMMCMQayQRka4S18YIvdcI+xcs8jtKi2uGJ5IggpKamL3dI2tz3MQyotJtpHMRJftXbJx9+WYe0lpBU10LXaZyGsZYbynw+AkdO8M0dGs0iUCLnELV8fa5Vr5eCmAftrBPBPwX2vanynAyxsfTVSJjZxbaut94aD8qaxBHu5uBMXeAtBTKvaMmOIXHUxvi5NxCtfRijFDpRQzquucKKzRTAkDIsUFUpCxgC6AFFSDh5XvPX62R4bGRwTFHBMjfNRgdZ5xvJyzBGMxb8e7Kyyu5BFrGC0CQVeDxt0wBj/u2FvBXW+6IlKGqHp9s5RBiQtlDp0DUdr3dgKpfJ04SF2IoXxSJq6JcFUyPgX6l1ToY9w696+dTrbc62UtMvi5Awpain2w0chRz7rPBjgLb3Om2LTZoLAQkKwTD4hOocwRcIAZ9d6+QCTXHM9liM3ubDKLZFYjNr6ipljnNxLCua9323fYOKItkk3MdQHyDmLy8kYYVwideoTq2W19Fti3fcWzBdRPBQYxF5mqhZK8mGmHR1XLghMaSgdIEMJfWpjQQ7UNlwrzYTEIoM4jL7/fby4duA0CfchVOlbPvhA0EgJABav23GaCEiuA3Y20cXsuV9OCQt5yMOorZL/apsEsDJXkWXUxa64VN62BhpKv5mhKSBq+BzMC2Z249XcJk/xrmR5hYmCBP0Em81zkRR9yhyv9SMjGg49C7vyyHTQ6VUJMI3Qj/UWFXrk21sr+tzgdgtlL/J82+s69JZ5OhBjJk8cizsE3kFUQgZ04mOcyk4UBkRwU1pmMdQizbngNnztj5ELvAsI0QF4+gQIXiOPfM8n55kow3z+w8LRhB+gki30duynUf9+ebqD0IpYZDtueTzyUkV1QOjD40MxZQVGWNekxq13vDIKeg6NrNEdGhKQDxmCrbC56/hBccs3O/DpVNMDuhCuGCs+PxUqsBVenvj2Vmfbhw2F7DgTh605jZyIdkul7eXyN+Vyabl+hWIT2LjDsCHo+98M7Lm8C2qn3lynqooAMIXQpNpqpHomdgtX9AbR7phJ+6m5hn+QgzhqDBIqnX5RHpNT9jpmdbx4iQiODrmpu5t2Jh+GQUIbb6C20jbUJY4ZB0Oez1Gpa7IWAB6E5awJpc6vwUCsd5v/wkWxw607rzJy+rOdtNLqLbSocSl8/WJOWGLbmfuWuYZSM8Z3k9UW45qWCM7UrdXRb5HWuAw0To0H5cQqOSTppsyHXUEBUVaceMHVDsuTJCFC08KfWsOGHviFnZR9+x2NedxW5L1YVwD3Ypibj/kVHhwX6274LSJ+fxB0xBHDY8SNFIwTV4z2KLqvmr5mNO4YnaN+NHQZOaIPVeR4CWPufLe9rj9+hU5zsEe84qMhn0Jf4t0Jj37webh/WKxtX03DeFOGkXCs6fbaEK3N5WBTZHItkDXRrl/Y2oSBdzZrHKcThoKiYB5FgIxfvD/CkKYck2XfFje52lYMD9Ocs1HxjYXFu7VeFVNRskg4mOvEFFtecjK90zktJ0e0ssjhbyj1ZLKaFh9asqh1MjZWOYYKRotUuzA7EeLhQHnu+7miI24l2SQMZu7FWoH/VFl5v0o08As1fgWIQyY75gik/tWUkDEuP9ruKcGhOuOG+3fLk8otGdK+4MdsmOFYeketC8hBpXVl2LNK61eFojQKeQzslZ3Bf9j+uIaw+S5tflhNnRl9aammeCrBebkrblmbVzM4o0hD4CyIx7DhKKl5yNq/aLwfLF4j1c5qEyMS6Avv5CkPWL/iqxJ/qa8odlClOZVJAcperZgLhERnr5dF6pau6MUkf+cVfS6aS9zq2lRE4zOTy+4aTBw5jSUbzAFXoULTTxKLL/NWfNvv58qH8ZjlUOSDB7P+JtF4/TYsFnSMo9Zs68gH5O+aky+5QYDByTfQoaJPOTq29zoF2a2xSj8dxDF9T4OF3igk3hE/QHp+W59pRJ3yTwZ/kCvt+uvRsfGbureY2pWr3MFag/vKd7wtzwJiO7/Uji5iOLZLNZrxRGYHq6PHiNZgqJV5uJmKe0YKO4TqCW2kGLtb81K5ya5sHpeDp3okFfU/zkWszhoQAC9hxu+zBbVqfErXCXEbZ6ZePOKw6uv9WDJwJliSWATuE3pJ/ls0FJeUDArRqAzXYIkwPKhEUIcOEK26jiifmEf81IhjpEg4DPI6z+jT4rzQ/0iYoeUIfMhM/Cs4VgQ68vRoBZLouWNuUFFyn7DmzMfhr6mUsXezCg40toVyFXAQwwkrXkKGuIgD+iDRUDLsa5b9vuo819sTfvmIz9appFvx18S0BLIIGSl0c0QqDw+RzQwneyWpWRJDbSS5Rx6jLpr/cBMv4n7R9QmE6UmLSDNpmK7JJUthtjYj8+nyefN0Ux818Rj7BYUH0Sngj5BLdmL++fuuubbcQC4coOn/mDvXJZslnnPoyoLG2o6J9fR8QUaws3crSfsgCY2vF32IeGiA/yHrONM5SQLEx6tVDt171VOWsZH4L3mknxmjtJhBpqCYdW4/OeZvEUPF2jlUVELcwBLhZMcHWeEcIaTphfbhT+fQ7Li3JJegEqTA9PRZmruhvgPppKVt4iBVZZKQ2vuDJQu1c1B8NTtDZJxphDnSwXXhs4GaY3PawCzBigYc6UlqnhRCCbQ2m70EJd/b/DbOOb/OTxIWNCCRfBU956xqUE2tPzlmKp8m+izUxjczi4EUdZMMRrdDqAW6YuCiKgwFZvTIyQ6yLWIVX6Ibt1YOYcGtj7scx3bRx7YwTPMYyLQta81asxqH/1GqfHk0IWMc5oJdreeA/AHVFAPn0wsVoJvXfU9OHTCz+5I+EdJXY/TyKcnLllravxOFMThdd8Y5D6wnuz7vvgdK2+X5+iqfoAnThLdSfdJqxlFjt8Ob8MoiOIDlH8gm8nTrf+pajhpP48NcPTy2j8CUAAA/06ZEa4J6nx3mm5QPpWgLjJZtWSeMhOrEFA0aegUb5Oirxw7Hid1hZfk1RWQ6/DSn32xUUgr73g2iAdvCjXL6dXFC9xcBI4AefZjDb14osY4m83UVH7Zi8CWeJvbI+MqCoQYlfwGfuIGv5vDaVkMH4EZm6nYoLiZgGT3ydK6c+mzY2Opj1NA+vn5qiQX2v20jEnkdSrzIxgeGp+0XYgU1Bv+k12PVoShed0SvgMXfaOemenJSzbsQ5QYgrdTEzqs/CX/ziPBykexaNLhjaktUrt4ikV5ILZMqGPNMrasInfky+kd3UVtb+YeEI+PbkdwJTgN0i6+D4ZnSHCUDDsXMqaciZeCpAeyiSzhj6uy75xgIY+PajaOSmgslyFr95eXlKzu193oscRrMi35IAAJnZvCJt3+BbCJaPz8eAzMiMWKAFhemo+amtnFzu4A8CderMjTuPi583Wl96W8OXVraJ6Ceb5mSAfpybgqOBfCGh+zCUKDP/9gstF+BXjS569ZBuXtjh3uJfpQTvSjrF3358vqMn29k4Ls0pe0I9ihbbR+YsJyyhFDbtobuKu1iimdCQDAFaD7abY6P093ygaWuqgWJJlgQBKiStEQaJHSWmWsLOKOCkNYcs3Hyz8h3evA9yUK+xEvixikfi62mcd+iYoO8hdmXSRgGa9UmpgqeRtuP8BoeZur4p5/uEXbTOvNITffAIjDTmJS3TLy02Li2m0cXkgfHWh/VfAyV+YpUaKOz+fvKCAcmsE/lVmvpSKrMhKEIqKzkPx7rv2JLpM6A/5VEZ5hPuYzw+uJsnolSweCiQxEbnhwkldzeUiX4O6brYdnf9P1DED0BME7PmZM/DRxqRS8MEETTq+cN1X80h1BRns2s5rFVU10nyZ8lmDgI8i9T9emCXEPQQCAjKKwhEKM3vXiFjfGucM2drHR1UC+ZzErJhEX7kTNziz/m/13LoDn/2ySk/TrAG8njNs51UDf/UNJ99dV2S0UjwS4k75KW1JntLsAxUj2ibqJQyu9IHOllCE18mat27yJpEGWE1Z903qsay/OjzVbhJ/qJydoEpYVCehXjaUZTI1VuviIiJOhEjQvFfb9ITpqlcfzgKwrFP55P6fVelyzHytyaDhX283/j6EwRUqCQHvHVx0Ae2dWuelLgc895PK8s7TGCPCgQu5cP1ZxN4okE2fnaWN6yh5G+JRSKbVVcmaDngMwOH5SFDl+IO8adHK4uCgCype5weslmv9D3QKScVW2P8OtOZBSNsOcNdOl2djYuvY574rrj28IU8jCo0pA4XYHrsOqk6gqO4elhNzU0nJKjfmqoFOWFkspJomEP4FUMzh2Sen1j759yGHvAkpZlzKmltsGAmASp3n14shEtx3vKTRw9cMSOW8fNorbur1F7jDFlRz8U1m+Als2bhIepZ6ObnuNtnL2c6WqaPBjY4iU4WPWUJDQyBxiEuP938HTNK3yNi66HoSdBFQIssVSdJUGDfGXrTIBfI7Au3rT5DJjQ67bnkir/4Evjgoud6DWdOmU/GDwbmCy+2vIqse4hzNTzRiPoj4cD1Ht53YgzO0HpS7x3iU0UF7P9FUfNI0mx/3R4O7SaZKa0isw9UURIz6G+L5n12QfVAxQdYiDsrw/g9gobSsg8qSnAT8cqPtfRJ8VTR8oTy8KFrMtTYOzPx2GLhX6K2TkJe9/cIo/0aUb374dhXF4uZWjJya1zwWyz4/DFvMiW3QWoD1TdmBmtT6O3hyOmyotCdB1iouWgSKarxMjkA7k+lPpWdu3t4AOBvWbvGRTynAGrpHgLp6eKoXA8s+hQIWylRNpaD3SlW63Z3UdEgGesM26FFsFUiM+7ovm1nKrVhIHCPkZNXP8ZySl8c5jm2NmjRPZSEI301R3R4oP/0UlceLO6TwuCEHrZS9maGlwqmeqSD0aYAX07FKX/Eo415M7ZMt/g9JobrY9lnKpE918URhU6EwuPpZAwd8aiegKxaFwHFqpI3QdKz2ysUTU+Kdv5MFS7FfU0Mrx2tzG4+r3bElxS14aLZE658MYNaTqbai9SR+LXk+9jK3CF+wVGBi7GA84Zs7/gp9ky/bawkVIg4DZP1JOnfgdRTGfhBI6GtZaplPcJSBLFU6DVnmMdqgawEL87XvfnS6ngTjYNar6reqXgHfarrQ6gPMgsDH4rzpbMBpNargX4fCfg59EFSm6lo1jIoGUwHltvqiYUZYR2Y7udW2fbamKAEgqBsxtbjv/fKJAjpQfpjOOt0PyfS9b5FSpETRXqRJuZ6mTnrizjITTPoalSxG8+kIU5l7cVu/JFjh4tzeDUfkKdZHXBkJaudHY8KP3R66Bh+tsSCMz5ipeq9JkKSXt5uQ77KkxHGmUjpIOryWAvfehEei9f0ZnS1A++UVxjLQ9zBeDstEeKECbw3VhHQdIA5AkQqRpjyzFKQfRILQL0cm/h6QougLUMRSgFkamAZyIqCQjKQVHZSRLnOh+B3VJ58yfalJVofMrPsE77XbJDMK8ZgqnnN2tUdTUysfUnfSG+czphFbCmJKhNEqUw6mUh0IdBB7NOlNbKW2trk1+OfTR6OnleU3CwruKklBla9uLpL5epdENotYs5WaJ8kaWBV23qDD6TenQj5h81BKzm7GPRAED4armzQJNqOOYXTQcplXpjxhtCsmrj7RgrsOlCxS2ILV3NYcKVehpbn4UVQ6Cj+us/ByPVtyf07E7YzCsaXfctyuAoQyL3wpPpF9H6MuwcvjueOAL0mpykx7OCVgrV5M0e9UhYBf7k0sHJbN8MJdVBwwWi5eutqc356pw5O47XPy/kG/QqVVZ/YGsHKODJ1yduAWkB3vEZEYOs/D1P1QNveq7XeLdl752OlFZKTT1CNrvSNbnl/v5cYVSllFuTy0b7PIITC1L36CrhUEBVls1iTKxVXUcRKiwyX/IMwOpu9sDd7gYrd7ZHMfP/opOYrvLfSL+/NyVwqBu/r9WJqAFeV7pIumfWjjkJ9exZCHR2+Z81of0DVi1Zhqs/rHCP1h+jglTEmGEBsgbwpI0dj7y4K212pACYiwbOILDVSiL5EZ9S0fFVGXd2UjEvIao/Ihv49ILBbmDGn613xNo3CYOOkBgzJ85elqkC2srSRTCVrA58C3bTk6m4yELxNWtvCIg9uDV89xzAevJKwLCqAAhgdTv0jB46+/xCfvEpFWloGExmhK8wWWh30Eno7CrsATyk9N3i+9ZidTe9fizC1LtnpuryhynJg96pWq0gZg2Jzjz/asLKAkhwIUPp1f1Kw7kKeYCrKK+gJ27cb9qa37JUuQd1DVjj7WQz8SyKc5aaMS+/BQjRbIPsn3yBN+JapqY7N7u5RJ1xYZNHrvSBZeFLMogjq4vhr+xtpMLnmXJ45epAc3Bb91dqyV372fhuTaxiMECblpv0R/jOY8dzap2pGoZFEeKtm32nenshbeGqPsGykj9KkWNb+6sWN48/xpEUK8jDUKrGYgNl/KQGDG8+1fhSigg9pVr8mSQ1lcMCHKjA0qzgZNZUU253lHDfLjAklDF52i/WrSDkEkx6qSmjX7APbnmI7ysKhVcsWykZJarkB+u08mpMq7RtseVJDgx4iPUxKUvkh1JSKvl5M925NL6UYwFDXkWd+F5QvaYWHRcUeplBSGQa+7PfmqvSaohmVwj+S44rFv82s1284L801LVIS+DidtZ3T1Z4s8JlaMoBKdKyj2kPA+VknmzA0XTSMNbvu8rVkpM187SHPyigics+H1TGEpf6SIHS68mFH90erWfR6TpbhySaXlGuWFOlruqf5GD6+okNW85FncSfxFZnZAHYPUTKCK3/9D4YrVerE4AWc7cIS0twk58h15APHley4B9xPTmu8CMrv4zebHKdHIvUNuWbbtA7rWc70eDoogUqii3Td8YuGcKW5Ll6Cu2B+YFDLSJ9cMhFDKaB4CflAPL4JOf+LSkKl7aeZdHJ/t0jP3Unc4bh4ceVJa1xAEWU13O2QtvuxUMaTUwfGBbme0C+qX+3cw2A/rq3o070nzIZFxzRFn8gvPE/NfCxiTARNmmlQ8cjKVTspvUXSiLZA3GR3H3JG+HJv8IAdhgU6woTXmrmsUfgMfUdfcI80pb/dS5UC4yzshRpqMgKzTrkqZAlDoyXnMpwXVZ0tUcpm3VAHdZ6I6yl/ppyrzlpEbhvL1FzeEY/72OWa/W2sZefYXDP0NilHD4XS5JFD+m4kdwFs13X3ECg1AzPpbWvofqzTh94oX2HdyETaF1vlGXQMyrlpv4OvYcc0j0EdlKgZ3KDs3m6w1V1RG2FxKgJjIkquOHBtAqvvNOqD/wQKO+1791PduajvLy/pz7LVZlAbH552/QqbQa+xiChvv0GVrx074MqjDxzRgdM99uIFQ5LgGrRyjRqvODv95HHqhnk7/qlCbn9+YbX5Ar1n1c4B7OtyMj/c9XKmNGVahTGVrvYxQYcdnkb1SLfdGHBxXs/2cpOsK/Xn/NkNsZhMzC2HCWmN4hpMYp3x61TNru4HlOqHnJHn1s0CAJ6rAURO6bg1CIAdQGVmeJzwKTnlNe8rEp9FERhqxJO4VO2pVIQ45sLBpG7alHU7wuXwZqExapM8RGEhVJHQNCPccWc4TNbYw+As5zUUNzlUE9Ok/1ajpRXfFNq2+EQTa5Mjd0wm5DRCV06Z8yqd/fO4XjUKmu+K7+6Povmub33JQ9lwq2s4u6L8XUXNuYttSp+mWdEj+8pZdjg3xZIltLsA2Z45BpseNKdPGEK8FjDzkPDPxeQmCtDiiEY8DwE3s0uGGVAPXEe5rAr7ooVRlDJEu7GpObeubkm5i6qLQcF0a9I25tNYLexfTMdP3ORnu3xQzfM57WArnSvkOGiJCOTWhvBz5f9AZw1XV552ioQ09g47vaTJjheCNtIHixY9fb228ZBnSX6MwXdqMv7vFQerayG/qxF7fRQTjfJu5qnII66kgNvCwCVwhs8tjpWIl9b1auyeWBfw5gV4gqQV8HurFOwKB5ieXmLMvKQanKLYJ+SFWKG0VYgJNOM1ZWKSggmF9dNcRrtTOwIyZDRqYLwAiBOiAccB+B1XoHKHtkQMuplKi0NrnW4n0qbJxnqJLRBzn4ewFrlMGPvAKJ1lUXdL0UD4oiQiNvp83EVn/6VI5+ywXu2HIYaFRq0BUzPBZSqqTnhe28AXPoSm3XBNjaiR6X/hVEHSKN2UorkIg74+U8QamRZldVfg5ZWkvTF+0f3DclBU+D6BuOlG00PVCXqxtKrCmX3uxlvS2UoC8t6eFPaB1CJifUWDU34tndh/vJzIujiEI/5AZyfXUiSAGOQ78V7vEnNHQ88sIrTysEmV8ndCRe8MU0ReNtTOs5uSFEQlSiywiNtN+yDZSzdiEdfnE3/U9qJ3fSjH/Q9LFf0RqczbCgRmSz+x+5DRCZwRGon1ghqvJ5czqeptxScuAk98UxoZ2Ixmec6QwV3Cs/2ozTqXziygkEDJGfuMWsyZeixwAoA7331RqNj2EMlUBPeweU+RXiyUmOfytXJyOlUR5mmM1vtWK+iWrU0bipH8m09qKgroiFNm11GIAXJKRfBwQ1qM8e2tVaMqVwsgzURy0WlPtyGgQXX9VRnihvSQX6AyfTcCoMSIwPFrEfFM22vDJhK6YBU2+OdFlt0mjXLBB88Q2GG0nr9bBepOLK+35/dKJ6odAjZkdq1QRq9TGfLY6+EsLD+AQNgPrcgW30IRJJd3cp5EJmHyYrR6sYifLFu+pFJ/+UP0IcnLMl9/WX2evmlcnJ9/HtkQtY2JHzuuODcJnOLB5njHcs+pHHgpSLw1l6Rcj/8UbJ+IjlIf+Ugb1bRt2vdoRUnr6oSQ79CBTTK/RZjheESKsDwVyHGXHZr9hg7493dkJ+9P3XNgQgxitcAfTJxjH8DO7c13ka9yorGCtHU8ZPQ9CIB7//V/E+XY09+nq8q/GK/RXGDyVv9jt1T+jP3JQDAIYKerz5jt4CdMafchuTD/KCSCNqI6+sMsEuBo7gltyRMRW/dCgB9wac+zaY7Rb2CysCujFR8J8QzTWvx0vRFxsE4Xprydtq9kYoFHAlKo/4GgeWdyVmaRw4qpwZ0ntpToP12Cdb540NctdYr8hrz9rExNopvweYWEVpir65bPAgXGfjMMVwC6MEKQGCR1DgRsDEER4+haBCRECcIfnejUjIYmQQ8zdmcI71DnFfMeQ6KBZiTJyFh8x4BHtN/MrwhBHmcS7vNCwQQr4fnKl0INQ1FArgcTKe+V7IB16BHtjmZWipWSzc1NRhKZ/NhWycslCPuQs8MfNPYuJYoyuwaXH5YVuuJCCuPQRHwC/0YfzNvkONhLlCR9SQkHgyiqIyLHj2t+QowGoLZKuCNmBmzKY/PAZzWM8fDkzivo4TkiJ0+iYdDwK/Kay4zP5qEhE2D2x1UD5Ip7I10GmpmzgvPFEZVWydAKDjLj0GEyE1IysTl4HEP77XMafiMAJQTHMiKJ+wqlUHhm0H/TLqSICABwvHRGIuiGWjpsadmCJXfm+KTcEKP1uEF9rKoRnC1IoCV6I8RiWs0A+K9T+q5kLEycjkEVFssZVIQAlu2hONn/S6hsH+4bNtUnxflosHL9GEO1E3aNf5QkKeRGDZBmj9yaeFlpQYDhml224PPa1FkCR3uT+cjb+8AqIHrT/mMHVuTHaoqZwaD0wqLJfSLZGa9/jckxV6Skz4nkQjXIjLko1pkym3n5Wjs0rIUvKKZ2plLzPBxMhgDC2NIgKPngqBSZ0O0Ei+1+UinrIpzVaAevBrxS5htWrvLblym8P5NgeWvTXmIj0VId0mjhnvcQ3P1cCWM6PCg85F3h8DUlsUpy0PBuNNLDzaIEkVVY0LCMIymiFYgUfNRrj8LhOtyqCnDFcL3MARievJYkJeH9ErX0Ns3Ba+8Dk8xI//jAt7mUyVYQBVa4vjY7KoRRGygYf9Cuo13Fd9vxEiqEAaDArC6mj+8U3BO4P0aTkN0rIRICZkrBVw/bfJDXveSuaN54RiU4qZX/APXTv1uQm1wUSAxu5cl3hGTf0WtBy4IYA5qQQXkgguX5tyGQOoMYo583RZ+ieuUtRuKWwi2bX09GDmaKWdEMQjocKY8amAf+vsCxtZVeGayAys+P5/xWOq4B/2rPmseWGmv6ctQX7aKFLsxi7Sj1VaIgoaH1W/hNA92EImzkLzOhVtZ3NYZvvydCgjTCZd/7uTj1gW9mODuoMGNa87wOTiywj857sMiDlsu5c3vsK7OufBr5m6qLHXMXqr0txbUVCHeFxQ+F38ZupI8x4OjQfDqScw3b+rRhd2zZgfYF6e2w2spkqrMQVju/LMVX3sgB9oW4gsxIyVZqljgP6PyVNoeK4z0+Ldlfdxv1pHB85pS1H8U5SHfdx90NCyGEQqvhPcU2EhNTLluQ2/CmrNBc7R/54R0yZ5v22M9s30k8a+nMnYwM4pmQhkKxhCebOWzURZVg77GQHw4TAEhUUIZ9r9okbJE/JM6KI/Zq6/6dWRrd9KTW3wGBDj7T9lCFOmJjgj4Tqev1iL/PnybZh+gSrhpTJxIWtwqShmXo1QNY6bZihjbhE4cU8veXsZ163PvXWP/P/rgKacd3T9tE7tc6kO9K6ehdcq31JjYWk1LrZPJePf+S5i50zfmTOHWVr3sdWJnBSDB0VrsagyoLUqtY1eYVQT4XgiFqPBayTYWYnnLQxYX7KftAUA/dTpzuXHfGhx3ZMP/Z9PoBbk9H8pLpcWhE+YPXfDy0JQfycSYmTPK/NkqK4nzMKlG3fhdtLB1UWgP0iTroEzRuSXgmTo42fHW5Lzuhe+Ysnk5m/esYuLkNcGvziwqbarpNeaDgGaAC5Iwz3iNz0OUC/IjugTk+gLIAocxymZw+bbe6TYz//RF1+t9BJJNIMrLgrVnIPBnWEF9diWqtqAC1BZ48y4TpNvUgLSEHBAhddSPvxf2wkQ+p4w+1f3We+5ugM+sCwE5Qagg6LoFQtMQXq22WfO6b8c0CmBYSnJuIus0iLBCL1XXverBonsa2jYkrLSjuMIoWktAk7Of4XKq3MnTBZJHNk+v/plid7csQPora2OFthgN+ZeFRbuVT7JMk5iPs2nUTiTiir357kfWs4FOAE7VFuwID2/cqIoW4T4l+OBhzEMmIsYXXO2h8TMHUhVs6FTj+drewtlH+aEaafCheXOc9yyot9gUDTqu/aBgvx6N6ZnIFDZxjc5/sPM2GSS8Oagd0gqwN2GFFTNDsdfxLMmziSPHImJw5tDMsPrn25tXZtK8jtAW5nRF33wGDviaWiJmFdq7qau9oWNpXq606x9Jl5hvgXBMinT2E0d6KdfAvqrCofXxPrxfJ2UgaTw8wJ0SO0fSCForl3hNTxuUKlcH1UfX4uVF2YqjuhJKt3lWHPht3S2iJr2B+f9JNMRFz4ZoFtyVrAC5EzmtuJHDsPj8r31c19bYWiZE6sZN6aV6N6aAk0ax8dm6uZfdzIEvlsUKD1ymMNh6OWDEMVqwYXQ94aISZRrSyv9mxA7FiQytM9wuQsvVwBgf5ELf4r3ssle1amaT93IDhylCY06h1JPgg/TKP1L5gW9r9Mss11JH1xkijJYoBil3kraK8VOWQYqgz/CSqXZSuI1sSYq/X0ca6jw1TharbaxWpCo8ImqGAHjXAB+1KYw8ExnIE6pSV210NUtcse89iE4GPaVCJTgYG8MGDg6HD6/L0unR0XrClWXh/A457H0Z03Vd9rI2kx8Ot7cJTSl5DbjUJBfGxeo5QaPOBE76o6ho0BO8OaUFfdNRZhSeJYW+sAtMcT9mRutQKJISUhHaN2TFrKpDho1MDYWMEKMME5tW0ofViOVncijlYp2YAUrGb1YF2TtODV0gqY8hCYMZOFk+gXVMKCJxg2MaRqIcqvzuIycwU4PABlEEkmgvH79g1G5bUBK3WqDwfcuJvsrgj/JBE08T93KxmB/LrWihuWdKRcE1Lx8eeT56JRILv+NSm0D9Oj8HVvz0BBoNxhArz3l5QIx+9RrMf+7ovtnwo6YnKfdLx2vXbcNm5c6bkNmoXNjYDFN2buwco23Fb61cpHYQ4f49rs+RuFUQgDdXU6rEgvl3qzMFdHA9axe2G6IdnrepCRP1cxKTNcnqzKaBujZPwByqYrtqJkwsvj7mDd6ArGqwTMv154CTkpt1Rhnx+BxntYaosG/3exZwf2kExZQvV3mEJrkToSBm5HZMUWHAgWOmhyEjjcVYJKAcspzd/ZwIektZic2uadBFdOiDHf8kBmwxtxAGCz6pERfGCFfWsVppP0+Lk0UgSjNMlQIidrf1+9UUHtuDtDYkmHoIzmeFm2nZJUAtCeecxmvC89L7RuEZHUG97OzW8B21WDZRHBMbVdSGeVyMLkyYtZw+MDKf/o5irk2OP4N0hFGuQvpcI9TESTCrL2jeHLoMoLk20G5wW2/41XN0GM0B5A2badH/f51vZK+4juGzWOnwuYTNFjdm4Amm6YY45B4uLLXS66IpQawgwIMxRaiD88UzKRt1tGB3ykGXSTNc9NrpSX0oYn2+FkIDbSvnxoOiPz9O9BqLVT2/xEv/ZFqWu/9tZVw3AO+f2VG4KeuvcBDRo+EnXgLEm5y/bGbSpqeRsaD8wnVvlSnPrtGzFJGfecl2Axe7GCbrUt+uMpLyZ25ZD92wIeqpyxFTs03gSKuYnQthhwmhCqGtP+8ZtyhGPc/8SnhJ7bTRx6aZTtSGBcg5yuxD4XcgAMCAyu04aqN5kooF1iEQPzE2J/V8g4oYBoJQb60zn1NIN9GWL2RldLjz8YAi/TeVaMGkrmgYdyEzvsT7+amYgRIDvgeKlBt1TOerCbKZXK+LXg/kx/qA1SzFiYw3v1j1PCNVLsbCip7W776OKU6FbwxJZx51em0PbFkNgGKAgT9z1KcouPfux53/2hmQWVrxKGyXZP/5x6UOw7kgD1broTgs3SBtBNCRA0B4BtjUWcjR92xyGdYkHJM3DceUpcBxEVTGapfUB1qgMgjXx6znLSUOqOVk6oVE5bw4PKhZWbFzuJe3qk4peshGq6z07+N1k8Y0KbhjJ+LnPdNAKb888ljxjgH9b5ze9o+Jqe6zN01EsgHPTGJ69z5Xx5D0aoHvjejtVxocBjeoDI9w9VpB2QQpruHanBtwuhBMz7vQAvVoYER1uVwgTO/trx9nWBOqY2swHBwTWFIWodcKSFz9FJEVwXAiXug22WTxtjYPJjxtbFWgeGq7LkNxZz5qspNBIs9ppbY22jz1Da3hYc9N6JdhAyDQJ+16fsHJVJ4AAMEBmIn6+4kmP9ia5e0+gQgt8GAN0k1QhHBf6rpMwsIVY2I33KYRgMnumsSWdzBMagVNaULg1bQcgGovzmRTi6DwcUHwduWfyMxJv151ZBOVKHilMUQaB5sh3xYNQ0fdx9z3KRpvYtmYi8thCm6/e2XvhPxSaLyK1eM0AYmBmIfJqkkdOVhPFc0plqMIu/d84VDCrLt+1Yxd05DFwKj4g91f5LAeMty7q2pal+3Mlo8CoooHpTP7zAs/Jp/8aveEpS2dOHGHoWVbXibEVtm68UfskJnNcEwO32gS3kgRuZ5Epza8QXziOOKNW2w2EaQyg+YfSpSUpYdfZ5GkDPZIveS6maTpGCl/h3vZbsF56ZvcGbbKcmM2SEf+3uylquCniq6sn5OyrMLtfxFPYg/x6kS3vWQLQQzC0iri5YEWemlp4qhTKymzWdoFQudnkB2X0ZQ5GUQHn3Onf9QT74bjqEZOS5BlhXbgnzzNby6O31qwWBrRtk0zwFBne+iJVFFuSUFg4rEVuKR1xlzmB3cfuC8y13epABQV/6c0ekGnulKys32gBsvBdNnoaSTWKN4bNwwaLpr780kPoy8ozCLSgVeJJkN/9g62x7e2D90K2mowHKhehkV/HRGFt4H5jDkeyuQqyzH2AZeoREn148rGhINJ/2gqvw53pDkmBNAnJ+DOt9WZp8vjwEr0O3Ne/rKKKrNLAvq2Mn3Cj+d+2n0D11GKmHjoVqN/T/kLZNpJg/8krr90+4vihzhTc+FSuDUs+GEspaBZjEn79Zuaed9EdiUBtgoR0GPZsYOO+EXfQI+Mu03ioNvehNLy4u8oWw2p2c7AIc0vwd70LiPtoOX0ZQKJmGaq3NMdvYEuPBCVwXkQmB753o4FNF+cWx0rOCSrr8N9MxN0j+TxyCy+hOf6AeI4CDK1uCuASSpxkBnE+yc4ybyi4yKSOYs+KbmFKxrbR7O3EXTaceJfUn3sUuBD9R1cZmRatN1tRKb4InncvmN5E640yyV8zo/Lsh1fLgRkvvtjoWTbIY7UJ4YjBCAlqcyRgyXvTV0lj/CNLDGE6jfadaA97ifYL58UoP4esm42RM5ol/+/NKlDjGHmysb9/Ysm9itUFZo0De7VlWpIyB3NrUJclVD7IqPJANMPVHodiW6D/ecHt+rONuBj0+olFTNjBbY+zz+73mruYFpnpLuhEtXrVZHN7EdxUI/WYRn0PJ/AJ51Ydhjjy+SCuaMkWMsVwvYBaO2K0N6yygcAKR5j+9QofbUNi6XteWBGPWWpWa4bptFRbwO2mfcSjqLcvTwNqgHMTHaIKHYL7evGk8B0gHE5Eqp5pDSqmatgq5lk+B5zcYyWjpFAspU9eWQAE1HLnZd+DPOflVd6aVNGmEjFVebQVy7bNnDqmllTfyjcmbkbLJ7wkND1ArvFFPBKlzJ11r54B1Mo0WQIq+2IMiYfnX45vI7DOjQZzQGUPvgLI76PBNYFbLCB/j4zcaPl4mua/onXiTpSrQb1dnqGn8MNFOA+4wt3yQ68NgDALnuKbTENHkXBwdFuIWReucdM04b8vFoIgTHqTpMI0CBFY7FlsKH9Eds+qJFISTYtGYc7cYkfKJXQc+KMbrXPp5I5qoYxYY5w2DIS4TGFwAswlp3GzHU+gi2rnD6sqan0dLt8TM0U7wWkE4gitmhlwU2H+zi9NxQ2xTM9UEwwBqbHP4EdY9l79NU3q6uvZH1PGvp/UQZ2u8e0y4IU5wsI/nivkWfE+hExwM1KDUfW7pOBrQZQy/DMVoN/oMIdfrj8EElF4Iy9gOXTKaLGYjqENTp7LneOWetXMXrEtwfbtYgLAn1ly0KyFGerbl49o+KSejXvzfb20et58D6jzykfiC8/7ZGakzHgnSR7uDbKVN2/Jgym7Tnqvnbde/6qFWaoAsrQtq2SFaaou1Xuxh75WfjmBe+zHMPEl6DcBUdnl9AI1YXJU4gZHjh99wTc7mDB+JMtOLDdrnuIpZIPt5AB5HRxT3htpXXt3T5AZBRKtJiFYDUJOYqI3A6IorDf53Y6iBYkHUY1Uh8qoXVDTUloMpq1njDSRHBCaDKAGaz/oleYzdSSuBdCWEiNu/6UpP0O2f4MwIiOrQvgup49fpenVZH5rdR36zKyocQVPfN4PztR6rFcjp8BvOliZqN6itya5b7JD33LY39UGev0JtI0vVRbvylgON1D4lt/18Dsy9nMtAi96D7EYOT+qj0bUqPA4IZVavacv+COOhEo0QXidGfgIXWkxwBdard9NQ4q/QW5UkrOsaUa5qw1ar7ZwJoME/BRpfiSdl3fNeXO4KDj6LU/QQsVZmmsGpkFjS2Mc8BGjhFhPNGgDzc8SQY9PufAQOMwa8zdy0UNBGmKvRF3i4nSPO/J4m06Osr9FPy+pc94hcIUrM/H9J5p+St/w7VxPGPQGWRVslRIZXEovuMkDchmYiTmBbqJz/Ld51qwEO1kCY5xXEtMRq5R5xh3NWjB2QVFmxgpbIbZ3lk/+3OjTtU64a2fbR5U1xZMqeTZjMe94zkd/aEiMHCvn6K2SxJTOapuogdkwAfWgin2kX5aEIqqb4lqRd77D3qehtqkSGnGcCHZShgXS9GR+mypNEFLrSuSoaTBxaKJ7riVGjQ+bF4BrDK66rgtLnm3r+mLWgw3B5CGt4aOL6lXWsCNJ6UoteNQb7PZyc55JeiTakA5Kp87ehwWGxXYC7hIbfuk4rSZtxnf/AiVFeznVNzQo8VTaq4cL2gFZJUiwxq4KxugutxteiMHcLdLJ9pb3xl70ffUaMVPwffvgHE1Ut9b/Mssnq2jkwutYja8Sz4Q6zKXRXa6qnqMkrgGTscZdcMPqktvWnTTmdGdkNn3bIS7az87Frnd1WGK3UiOSZ84skEn4KzxQNcNvaHAksPhZrb+cjt+4eYPEyHwIvPMq3iO4PmeyYfBkcdajwoiPdq3f1WQleal0lkICk8rfo/hBqTkbYuGYvPNESlXDu8pY7oFP0Mc7kqN1gyrLRitrlIx5hnNKBHuzfV7kNrpR8kWcE+4ssVSDM5P2XLN+HU2o1gW8K5A/U9XWshjj8IqpKiAZF8oDolKyPiQuoH0sZBT3IN4saR+lTyxX+B8Mmh2/QqPQlrEGKt6qOBWuSnviCtk30MXORsjlCVhDVgd20Cro/niSoXcfYSl9yrTkunjR6zCRiBOyVdEQ6d54O23UEyhdpE+1sKmWAOEt25pVmGSsF/1mfd5onhY1PgoYCmJsR53F1dfPU5N5SEOFuUj22SlDXEjvUXn8Iwv6Ll1SgKEO6LxNH2+K+uPGxW320AIDzygjN8v9tD8cJ1M0yGd6cv22495Bht73zVKOEEQ27omLW3DZmiSEilCUWdobcobm1YbG7nnH1y561jEsWrMaNE6+MF6a3Q+OFzZ9DHYFTfkSz7ei60CxfjGlGWY2Sd7wxS95gxWYHCdGpn25AvxQQxs9MpO7XP/OKpGWqfnQtSBITe3nzChiIHfesZFFq7qKINO7RJ689PPDEnDj4eZ8eYSawC0BNVv26/6z2iJOMkpXHz3JzPpBhVqHgtFnEn4hAsa8ICgK1eRtt+FqptZ/nQZPJfdeSzkprcD0/Z/NRTCPV3oVQmCoTp/sDj/ziisrE9C4pjZJR2qQQQKk5KtIX2/ChsSWcFEVBRgVVq0pcsaMMZcF9hV85hvwsDf21QbHN+0cLyoUDpxhzvI2ZwHl8opZie79PbFL2dPu1600nMhpjsV90vIwrTNJlvrp+KY+Wg+k+GIzvTaWe0Q5DiCsvxYdeLzloutImviVg5dGcMzUI/x7YRBhURNNZmJo0lN1DAhoZZAK7nOAVFlPtobB6S0Ozi3B+Lx2vxcUr0ZQ7PRZxcw/ifVMYx7XebyAUjvBJAdmiydwTqAvVsbSMGF4uTPJ6ppUDKsboNlLK2bEZv5gR4RSdOkyNoLN09wOHyM2IQL8mYhPbRHGi56Unl9we7NbR9O0kfRzzz9GlqMEPuAea8YySDRmp5ihE6cRbcF4Q+HBxMMnLseWUx7R0GoQewDYyNZJFFyDD5JZ0ot/a2XUPYnpe0JCLbZcTU6MAuJ8KNnvCNyLJHP3lQekakMLl+jRO0DOLTA6n5uSZAVHQx7VgqwE4dPLYyqVqzosWO+isam567H8ve921l7MgVQaOnsKhr0H+yG+96XgNoUKnLpNPYCAkWErsr0a/jiznK4GbYn9dHrKd7ChxPcpido3IvgO5n6VTFP/Fe7u24zOkZwnngtcdBP1b+Dvt2xo5VuLaTuhmzL71JrXzI0AMmTMaNvif//qG3foHLp5Xiu+YCaPp8F2RBuFWO6pkh01bd+CjIXx7NkiR2pwYFJYOg/wdNXu/j/ZorG/6TZKv6aMDAsGhraQsyamkOjBU5PCJp5jkqrgVG69J9l0CxNx9W0EE/qtsbOwsVUn/Sfvs8igQAyB9Iuf4Myi0PoSHQAKE5QgCTDHOhBTgE7bw8JaNBHchPb9VYyieQ/R9YMKiy4bF8WJc/jV8jCviBKLw5e2vtBRaUg/prGYq3OFvZ9hyB1QPT1/Pp/iU4qom/KooIgN07hh35Li9mnW2TRmUbe6UjEwEaZrHgQESU2jhk5gsb4o94cd5468y4nTPa2H4h+Unbi3Ahw1CWlmKRQmuPvj5JEDrHtZ9VIpybTBmqw76s3u36h4PN0c9nAvKAz5Z0Uo8Mfa6Ds0erRhBEcvug6QQsULhdrY4rYsMUifENBmmfdGPTOY19EgLh15C6+9Os5x0910N2s+bbziU+u3WKY56UcZPVQaRtDjNVlY+ndA/pP/FwHicdoS6CTVGdB9Sv4LPuyEYQsebgo95n86CwiIUJWOXsvGsO8bvJ5tXDJ/7zbpqv6MnHIV6/0uLo8+tTN7eo/jZto9Md0Ctjxi+b8ceFp7y71S0PXTSvEF0+V16nnmyNaJZVAzjrfCKouxTLxrYtvVdTg51H/D0LGxU+6vDcCanrQ7dkXleO2DtNM4Z/aaS7+x//2jYE4gS9zoFfXJmXAUNKdQOi+5KEMWvtAqn0ttUgNmBaZ6qpM1im+QVsITaqsHyQlBADzrWHe/+ExUMURpmrm1XWN9c58QJyUtXRwCGi67+BwdlGtn6T0XFiYN7h8BZ3RiHRPA6utqgaNzHL8QuTll/2y9hbTZoenoiiLvC+bAKVO2K54ZRThqwRIATRVc/4Kbovw+JgcVDM3rv/gV349+xoUQNPo8Vee1vnX5NgsO09p6mGIrLoBucWauDXh2REMDuhrM3sq+nfePFRTcz8UmP/IigivZeJCNMHtoMPdQUdu7il1KlRiAMhG8aq7ZtXfpL/Ltf3Ml2s+U5ftAOwN3QhQydIPLMtJ7srsmnHXSG0j1oZvxSxN466L/FfTu7HEkAGojQCCAFT33JpAIQbBdB3QKxbwezS1xYzmhB2+Z3yhBbWCeMQ3QMziVblPNEdU907HvKcap2jCgGgIUU7hAVG1KXnT8QTAu97SmbUNaUirzYEc7cJthkKYbucRUuMD7h5/iBZgPRCIp5cprhYLs1IaF3Mobm/C7M1sz7oKGBzcLgmd5bzZJjeycDqBZ3D+jVLM4P55shz6zq1gfmK21fVAg7khwMsrPf/NzF3DbeJpG5AnAZHEPwCiZh7LSfZ3eC8IsJwdxAlRgfz/7PZ77ygaHpTjcrKN+xSYZ1kEsG339nAG+PiCN72Yji7aZ9soQW/sNkt3RXLocpvCJZqX9zzql+ZiAepyMxdJ8h9w/NmR027oYJaldGW5n3Fd9nuEdcVamlQ2URWei3jc/cCt7GXEhuWFD86krpVKlhTf5ftXwas+3Uje0nUBUnUYaxRYkvnf5ujwOx3CVv6YNTTTPkOIoIR6HshWZxSj99GIdfLRan6atvavfL3pM83NJ3Tzuh19EUVGDXnZ1zVmLmStpOZhwml2/KL7Ju88rmdPvZnURQTyDilx5RXYtBI67gB4Leqw9HRIiwSI6EtrM7KzDaZ6RhMFCCUulamsSxnF5nFV1vhlg8YqBLq0VX5k7IafvrzWOCd9x7N1aacsk1EF0t6CA5bBGFKyb04atxijKdmfnkqBsq/u6eKGwwKa34vgeHNY/MngrchxKQnNPGK1g+um77AdYwzlPrp85bJc3FErbzXxFsdGZv0Mzc2T0Pwv0+2aXqEmJALsqIPZQ8BdpBsYgZujwPLRPaJbrZpaOlkldSVa/n2xxFUC0LMPDCKcwz4ravIYo17rhPE+ZyRPmP/QX1c7CoDhrgIjsG+ePvk8WFL5H7hxZSE8jcjZf8veIke2qOeb+gCRieuM9D7G+4w5B31ztDFrXWOHLHnt4Ckh+hliVrf4A7Dad7kX5uDX82pOdKJiG90CzPfYRPhUwD9m2DdHfyABl6yrF0+zUja0W+DSIAnnoNPylazNcn9//x95b1sonzTYcin0LvZ96mfIIk/ObXkEovjBc2sPXCxGcdM1iH99RHyTKSGF2gxHPiQn/gykyHG9a0fcit/Wvo25iAZluDMXut0pAaDyHkx/PtIVV3mpi3xpERJb2q4qcWIFF1xlHkJq5EJ553JWjrJsNwbVXcpVc+XDFYR//sbSGhOpm4m+k0j+CoSeoeAPPoMzk/TF2igqkVr+8GCnJ5EUa/T7cFO1WCZXdpsbhLtfZXdX3D3JHeQDhhvEMRQEhOd8FV62WxIoyF3HNqJSosyOsWBK6iD+7w6Zjzvnc4WW5w58le63HGjh1nAw2mZA36niTSqHRkJFHyr+gZ2hJRA2lIXmweW7KDsqCC25+7f8ApG1VGpcvaGn+w9xlQCcU/18AUoNBjZDrq4v3L7uS8ODqOwGUcUfIYi9eLQUe4g/bwomWhQfF5v9OaBVWn2b4wr0eWfa6A6tIsnapF7vFwfTHL23kNfJWuyuMudw8emYDujC2KlPUTNTaTksNjDN+oRTtrWAcT0fFJ5A/YJlGwJ10az3kYO4GorHuEoiguZIaYYyDlAgO153/cgeHm8lktuOxTssx+N6zky/xvVHTXdYatdWALOQizh5MJENtt+ULByV6nqok5Na2octuZVvMWkcEeGMw407h++o2ylmQiBCpGYY5EZbCHTcVaIX5dlUPfYj8kt3fxrM4M0VdfNps4I3+bKFxwBJDDFHapFLP/nHTctFZC7uM421f9HPjk7tXW3odYJ5SRM8p/F/M3hkRAXROEUmP3vSot2V61sqKgjWL14XlvIQjlhYHbi2uPRBSFVzUfCTyH6GxX28t8ZQB+0XV+XRxxZ4lwwkqDvx8dUUeG+agEa09bN4KKYODNcAlILw02wME4Q+XhzNnIf8G7UF2EiZ4gRAlpASTjH2RMB9X26vyoU13OdTtLPpdYFE6b2QXx1+0h005734QbuPDB8/mHcO+2MLyb8Wi3AO2VJUV/8otf2wWem+4zE7zrXDSmo8cADpL19Jp6YFsfjRvCuME1ZJc1cV+NePhx40NCye6l4btmcvkmFqFlQoTBvNnsjHRYJKVd/MvwnBtiDdLite2ommZTwkRqs9oEJnZ9ChN1xZVUePZGNdgGwwZNtIrKVY1bqA1da/qAfeJdK723UWG0vzlKg/hF345WD/UJiS7SJLZFh220vJhya72A4mxq4sfORjNr6XHqVwT17zBiv7ScO+6/jh+UYP7/T6siwJ5eEEFd03dYWqd5q8CZVd7uNr4s9U3x0t6p/C5/Y8uVh+BkIddAOlVG8KSCXPauw1rLztY4wB4Uvt6+KY57KPLkAnIFSzQ/3Us2CqT/95ByH3dNjC7Dm4rn6uIRzUC4boxgDLigC2tpMSkGqzPJD5jALPv1EnGDqUPjhduHi6FqP8lvP5UPOYPx7k29tw5bOd60jBp+t3YiQrCw/6lISvoQ9ge0TPc077GOZDvHuSIoyoFpIrVON4xe4VZyC3jsGL8PfR6CiAoSmeN73uxlP66ApjIyQ7nt9DF5r+h7F+S2KTb4WI114YoBDH1jNGTcaHkBnaJ6C1Gv9xZqp4UvViz31zgM3wE3KkPd2n27xRiD+Gcy/S9ItWfwOc8BHcF7UOMq+12VSe8JfX/3ll6+p42XZe/hdYkOxhs6MAfTMsuKaTEglgksMQ/VBtSgPaJXB+cS2uzTplOlvmRPNZ7L3LtMJUUJXsOvPhnFQS0ld8XUo8O+OXhukYxdVy04amd49hKid6Y1yW7Sj4vqYyIbt7ZaN+GUKGdujMvJcsnYGbtNlmM6wbCChpNRKjz1opVQsRPnOn4MXGU6PD++dmfXLUln4TTvQuscBsCUaQ6oLk/LDgefTRR7f1UcSV2jBYMYX1xkHXZx6c7tEiaG5hzZcVrvbqNIQcptCOVlwd+N3qnoqDAFcw8farNiREUZHvkD+dadVh+LA7HldMa1sBfKabZnZfYvEbmOFa+rMUMjzbesh78nN63hIm0BDwcLzoPw9Rs1m6uiBMHrcY2WeHwy1l3qJQjbpBs3uU4SSAbw1uT92W2QdluHxiY3pvh2heBwjixWMKqiWuFxgQPIREJ0ip5Ymnv5oZKZjhs8Lg34o3LESRHFOn4sg0JXREZ+31zixx1ut5rgeiBMHS1neHxHaBNGVy9WxECUGt6RmRwfhQRMBXP5SmaJP+OqszUVhkAtqpdwJerBImVpQDmEG5RCW5ePRhotU7XpnOW6+6z3sM8ztP3TzuoQgnClnWCqpeTRj6DCfQqj/ezf68NvZY/7xQ9sUrEzBPguu7dFGQv6irPA+V2OCNp9ZRMSx48TwL90wH4tuN7Z+aS2BtCaRr9v1qI4XsWAC9H/mZxNoUk5L6IK9OsnOwpmIansfT6Qa9kkzofPVPQTxWEMie0ivMn1NjwxkAujzuUUI0xXt1k9JY1FUGFt1XnElD/XzLlBFToNL9MBb+kS46f825kKiyjR8nFgtyC/hMueZ9aLEIKVIQ4kXRxJ6xrBMlaQ+FivRR8fm1ZAcnEpqUnOkwHKBQKR0u0jbsjc+K73mYB09VKf9QD5sdHA4uTlmz/0g4AUge6XVyU5sox27y+D07rWtLatMPL9WVgU73k7Oy5LS+E09lpbRUd5YrVgpA5/5yNDjD1giHlrfbhhj27AJ90lU3JrQbYfBoW3s8roEvswMfPBqf67JNA3dQ4HZea2V7y5oeiUJ11rNLjoMgT14iSVU5TgWtumbPZQMkUW5Erz1+SNfmiVQHFBNmn1hDzrF5YP8e9fEetuugDldlmrOpe0M+Ssh3T4Gk5j9R+CrODdSj4YhixXiyO2aC448YzVMtK+edK+W9N/85oS1FsOy3BumrNdBPZSUPoUrTW2csvxonIdN2sQHn0Uaziyydid70AZ2GdG18j/f777v9cEP9sVEWXtcGZmlAbXtYxilMM+jE3sr5BfaJs30eFWoAK8U/t5ILaBdHL1/muDUab4yDjw1D+CTeRre1Tr9iBlNzovMvcK5uNOvnPOvUFcWUnoZ0jIwg6i5T3frpfSdNMRUfs+OpMZslQa6L/7XVQUj2bVReH2yTIsgxvbyaW1tYu/X8Xx6ZgayrMMP4qKlcMwfkGdvqgRqKwk4NF1eiHaIP/r+MGDgEzqKA1Vc+obCaacXhozvr9kfbh38vY77jF96nQfJ69VZUYqOyPXtDxqEjNa2zJVNxIDRtFER7DLUCpJERgDUWLBuNGYvVUO8bC9zmtLEv8afOqkgwOZgZKs+HvXxwNnKlPwI9sMESwbBMOLl+mR/DHDHZxhxBbUC4fLzYAts8J2QMeCcOAup3Im29qSTCvKpmlJSw1s7DCkqaE1/VgddAwIh27Mgv1A+qbyqReTCu0PDIVC8LY7xICnweu2x6QVQSagnh5enRShMTwuIfbFCNkm0RQTAPCZaaeZAfxqELAh4/UOKb0E2h8OX6rmFI3odTk/yYXA4cmtwTeco1jEeOda9yqov9gOi4svmSUMy4YcXYK3IVb9jUliVEUC2VkedPnf95GZuRp4KakaMt7ZL622MmxM+oa7/3VQm1p4ToqCYvwIPYWYKSGmX+kBBxn63joar7H8EhGj32cGfsSvY0/QXn+GwaBc3JufCL8hDJcdSPaq1GsGVaL+QBkd59+Y4IKOIRYFY0jPqi0V+tEGpxY4lqT2W8UtrhWCfnKSvRnrT4FjYzPQSWix1a9CCyxXfBwOPzfqXZUgRpESj6JpVZpbV1Hi8+OePmXV+6ImxG4PWr7RxyqjrfBVRG3xOrNZhrEAOcnlH65TqX/4a5fdZ+C9FD1GPw25jnsTcpxlcQ8311qvzU97HbBrLDJPmzjfAECXKpiaL85e9mS/2fUhJSqaxBm2XSF+pyY0PjGJebqJ9iQLvsPm64bZguZQ9HIBW4t/gKXYHH4bGaEOJXRoF/mbxlBpobYBcekhwwgHroHdwOyhrhQha12A4J1zReWqS/Gu/lwQ2tW4R5Dap7rKGX4/6XOh6+RRNL9r9EjBKK4hc6XRGmB0BlYNueRWKYgxyeJ0bI1Gn+EBbnN9XRyiQk/jlE6E7g7Pd8oqc/ouWBQT+Ak0xazdakFVckeTUqauTRuie/bu1XLNmh7dj18g8lmbKemYfuZY4Hm99aDAz+dEj0WhV7Ss0LdvE8jPlEXot8h5669KCLQHxkL2XEonRp6mRcVpcd0BAblofgFyC6bdjhS1ixORruwJ9fnuT66Uq78NcYtRBJNWQqSPfgDZGrkbGoVR1IyoYYu9HGts2f3thpmcAZf0Pwfhum8Dc5gTSnk6GiGRgE8Ot9z3HqZTglDdRUz/7c/CGuQ8dwWH//WoPVxG9O19ihCvJTvRhmMNS8SIDDY2/TvnpYF2UUGtWQ/9dfvd6zjK1oSBqFk/Y5jkEUpTpGIA5A4NQoyUDumOG2YSPmXJmMoxyN9lG271x/HTtoA0/HAI5IjaM5OtLBRusGvGHXizD3CsWVIbkWLRNDiS0Kekf66HC6xG2VqzozCifHsqQgCsb9yhDYVBbjIFgoIYQZra/ehFQCmtTOcfT4uu9lHFM0J1SETtPT01HCdLIvkPxU87IWH781M/DJ8eIu+wifxXSV1LPYhj7Iye9FV+R9HMDdIQiGnfwz9erEYDMlTgywl18NGySxZTtFHisyOJPOio7UujKT5lHLRaD59SvP91xAtZEHgynwB/nRN3yMflte5QeEvgZSFmx3y9n7nZ7wj3G44BEVL4M7v90i1tWmFk+BTAp6h2p0EmImLht4eU363tO/pWYUh2jNc1wRJEHI/inOn6aen4BdGoHXpHLd29HeqHC8FdnfojyFE2JlWLJjqkJi4rmA81+QSKQ3sMNIIYNYR9scrbCEFGytNsJdTKOCaDzKQylyUvkFemUSwcrabMn3uWWnab6LlR9MxdgRysYbuzGZ/0Y51UXAW6rbnMQxcVWrF/9GIMnWNHDbXMlbbqj4elXT/qQzQcZdvFisWLkn3reW6f61WN+x6tOTD4JPEMeiS4GAAIsZLDnXM22RzVh/sxBzwTu3GktmIE3B3nO5RnpE7F6CAPfCBUx/y8aK1hvI/T4cK8ycGLWtS8F+cBIwNAYxdtLOXjmDPJRwwADtPTqqwhE/NPNDnv/tfcNjP6yTCN7ar0aDcFMD8yE2Z5+VXLU25yXYsjn0b27Kfznc1jHeqHgdRIUGbWArfMPIQNasNIo+G7Jftysey7AOOnRC5DJNuMdMLSmhokT7TpMI0WpyRAenuGw2eGmkmlhw1ox9jXqDG1ndq9gaDBvDhmd9YKeonWo4y36dOBjpirIo20SslNFYOBkuHprohQ8OwnBVl1IAWvesX18HgJ99v9iKMLlK+H9d9CUVcihVxwiYkgZVuPEiS/FW3QI9s/X54+kVCPXuMAqTCyp4ke1ZXGoJvJXzGh4BQXLO5Qf3qJBBLWKJ+hB0TZOFqOr9pOqkQMV4hk5nQ1mKnlsQnirZ4U1aHF0qZe3PU3wOdiVGkdSda0vIIhH+kp8qf5/kttokmeDaYcY4nUNLN2JPzlR0r6jMsT4M474XSZO5k6tCexO5Lpx0JTNODdysHpKTfyGozcBHhgfG6ho6W1k/WKRRIq9SQ6PKaQcRIbBNv6W+yPxyte0fE1Yxq/RQQ5RRRsS2ZrFb1I6Xjg7V7hsdgFVf2DS75XkLuP7S4luaIsAuoCD8hmmAE6Pm15buM2UKihuee+1VUP2BZSeSt/pK7aoLu4hTq2btOqxoiLRAtMhT4K40SgTrOtbqvFY8+ToK5Z3wxEO73jnx2ldZ/EwRLJ0PEJ7LqOWo05EIhWBJvyfnih2kvACBKkwGS+1DDqVzLIwn7P2BUjuFYe+huS0n7tfVdNvkBpudAcy0qOUOyE21x/Hd/g9oAsAqKPQhWAXpZG5R3PbzTJdCfEgGm3CXSk7WPbwCtU1pCv30UPmU36iC4zBRIm7JmaDMGtj4SWA5PssODf36pc5aA2a6aA4iqIdT3WFigWA/eucUHQjorJbHhHZb3KkUfh8aJdHrMd9p9P82MPZmYiDTkNfK4N/TQHCFEdrzvWIAC3VvqVoCG/xu5K6s/PbcduHM4EBAw8S5qDVgUHIWHNFMlsDBWg0Mju+mIbMyESIIIEr7IDvQMFv3ZpQU/M/jC6lv9dfHGmB3vidX9ZXXbnYniKdTQZheOjKmycTFWwR7viMv6eniU2eVBVBrgH7xmxJbbp47CuNe0cTmX4WiOwAP6zD8CAWNVolORJ8g1uVFCkCxOXzEYLIx1eNS35QY+fQc5M2JCbvxsc0LpqpfBYokU8mByWJ7deF3wuSvb8y5903eVH0HNcikTSe/tmT6GsAARC7jCjQBP4vVTtU8pPTlb2ZWuTOG+xJmRFWXcnoG2aKYDlzyyhjpO4/8ITlRmWEi68asNeiConZLRB6klYGqkZLvyxtnU+7sLHbfdh/xtd4w9rQ9ZvaPvJwahlrJiJSuqxbnIQy5+X8k6vI8Ib6l1mUCxbs0RqqheomI+5O5DCshpDnRxACjALLDk7AXMW5Gfs/JmakrrbW0/GChJZIZ7CS9QaFp+MGEWM+uiE1dogvy4SjGjp2OlrdfYEeTM7LRGAKSv101aVI3zN5P/jG4tnAB5GdcLVYOBkFJoKT8YMyYZbaJLdPhqOlx5SqkrTjrSatKqhPWaiwpt63IL1aop6xiCpjiL8QDif0I53/9KwrPfi2W7WKYzLknhRegHdE2t+r13F0XPwFyJ+vszAqcsS5/n+XCmd/ZpuLTO5Ska8eMu5yYA+H1E+mhf1bZV0HWPm0dgIWrQHjb08fdUlNmlzwkink6Q2pKIulj/r1nb/r9QLYqKRxBzzGjMxiLG7NSiys3o3boPIMcDD7WtElV4iIDz1J+pZ5VCp1y4h+cCOy0wYcxrZQ5fmMd5TF0e3EHqeu6BbOWcrIoHBFQMGj1cf6aYecRIiL+nCoQc6Nm/Vfqk3WHBBRt4tE1W0Df4DvquymCSzYU1vTtPCYKCLmTJVykP8jRReHCimhZbIekxvTDYJnNTOGgQ4RtsvQeSq8PUXzi6z2Q18AlTEzKhfH+zIq+EpjLIl0oGm58LvYhI6pnJhidnQ9xk3J0JMxiWzxo4f4j+er+UtsrRbLejwZVKEROexXG6vkRj9kCrRpi60PE5qC/mFlnOKVjnijtxndTJffvkWydOcbOqQG+SWN5r4W1uk/D2mROtYJ9mOILZh1jY55yDAlUx3vjriSL0tkLyzgsfsv7nBwhe5z/aCOBirQL+U3UOICJg5fwnrn5mqmdQSg0ccmlaBHFp0qZWRrgQGv94oE/vgzUTufnTzsoXD+g847JSXxAM/H9d7fq26l1xBxmNmEQexs2KQyfhpPeKSNAXlRT45i2GzwHE63s/ZuERAe6sTHCccSMiP1kKUe1Hk+D1tz8pcjjj+M2No2V9xIKV48msTZ0A8B2zcr/6r7d/xLSyxBun9BBbsG/4ABfBNiyhrylx3Fjcld4oKWgLzcrzxgws6yo12N3s6mD+3ywcFqUFX7Dvka9JzwJgiGbpcxbYZYYvfsmWQBGA7886UwRygO63HCFFcv+sVaqlUJoIZ4KkRwoz0j5NguraaF4RaTRv9g6Lv06yuRW6IH5dvs4+Zu5nsiMRYZu2ViSr5rIrFBPlTkBC7m4kVESTKjZDjCrtGhpIKkrRI69KN5jhp8tmT9d98WQXDH9mILkKfRlzRnLFwY4yd0FDtZpUwgrxJTgn3kbpFS+qYrG9nIocW8v53VveAvOz614Ht1Hxo/BtrYFpuDKxsAG49c6XQTqO/cJBVuMa5tkum4DgGpBL4vAAhVEYSLGte3gTxzUlKfxGIT1EQat+zSufAyhGsS6UXp18VmmDygao0YakJdv2FL3YDG52MJw+P/jd+IR2ZWWziJDU49fUVUmeRf50xv/i7ZW0MpfI+1w6n9oAgOct0aPNgV6xqlay/Kjs5UchEkPwqFpu2p3b/KinZVxRdT0TVGdUBfUHv1kHK6vpR9mXWcTd/oCs1i0XoS+R8ODNy8XURXOP4/fGoyqPOHmN2sQHBFR5f9qNZJZY04h0szymWWoi3rQ9T++cwO+vAB94yoiofpqzS2xf3ODNY0KV8mWDQsyPug2s8AVkCD6HSJdOtoF6j6UvBX3y0jK8fcX5QCxJMfuq0Q4+lwznLVbPH+SZavKj2zKl4aIow3mWQJbJiK6DVYG+w1Q6PzENfI2pCgnbucmQMkWv3q3vqcwcbhGCU9awgd6BnPu748Sbd8YgY9StXtkU9lnON/x6xPh6ymLYnsc2+hRnRi9KwXQdsfTrjSpM7JBTzKw2ziKAJOsRnJ5mJjHOrqPifm88UK0nAKmbiCzmM8P5PCKoE1ivmtTrolmD+Et2+kY5HkXTLOlfq6NeJs38E4GS4qzxBR0459Xkbfkh8Lbl+QxzWhCVjJvCQFBhKQ+K+Xcc7Vvrgrj6s/D4tXWIrvsp6J4aShPPiZagYul5BPnfEZNSelZ3cEY/8LH42Scc2uxr5J68njOsCmFj7RS9yIeAsxp1C+XPp+VXocWaZ87zMSLTEBKkwfRmA6YIEhKLFUOrbXGXfjbcZdohGEMWMd1NvBQy+rTul/t5CWPdlEPbTOObc0MwsaPD1ubcYVmKGBBaQYyuk2t6wOFiXLIYdL1YwBT4Uabte21af0iyZdQZ85og6AcxrXA86OswTjDfuuMoSeuAGGM5rWXOEOhDgkFhMiq60cbw42qnbfA/un6Xfuzo/Bw51rpMzdD7yWmMon0ECJFnpsVYW0tHLF7H38QJbV6PYHb/18LFaHJgyjjBqk+q2ZT4XZilkeyjCEJqsh0oElZrwIitpU7ajOEzKpa1mlvP2IWHAP6aC8RPKJd3fvzUy2DTV4ZKP5Doaod+9q1AymaoV0SnSYSf8ZOi3JQC+WfOpbXheV04Jbt8GNJBMi1vaHnI47bsNMHPicNybXrnzELuGH+cThcwNlfWKvIckJR28qTtpzLRqMIleegqEGSZQOh+dmM17XPIGKSGW8KgnbOfGeOf0wH9/tC5rX69bqhU+T4O5WVN/94sI9OkrkfM1ixqscrnjypOo9k9k2nYh6MW5nkP9kg88U6VpCDrD+inM6GH1oFn0PityLStUNSzKW75Sg6OLhfUifxwmRTk7yCH9YATvjuzbc4D0DEgUROp2CYVs2PAscZcylERtmNsMHVuhh+Ffz3vNXNLqOHHRqUzxlZgjBRsxPVh5JDN/aTXMuQ8mVnygBLZnzV09SqCu1/6TCl8I/ppcTW1kAbP88tzKRjAllxFabrO1T/oOu947cxy6/xpiQWc7m7z4htCy8TkugKE8VqHyBfjHKhqBfGtDZsCN02Kbc30e4ObABrU6sTpBJwZ2hdtRKJBX7y+yxzhc07swRg1iFiEzivFVub3vHsG2Tvabw7L6AtW6w9C3knK78poAuD5eHT4XVZIHuEdxu5KjofUT9VJuOHYTEaP9bdEZiZVyqgVSxxRoFbh+UT3batf2nrW0Rukv0DfZQvruUri5xJjTimXg3KxMUONnrtPUaPvEywyEjO2DSLedJ684f8L/kN3ucz3L9ojqM1hU+9a7QqL/yMzo8Ty+mK68ieTtN1xNc4KvxNVNzqk+FhRKRKiGlTyZxGabhoYuyFl77mW9q7epISl7hA+ZQgF6tc4BI6tP5xaJ9jYvEqa57lmq8p0cFp3sH8Gl2peyRYxIzpduxGdPvfR/An0HZwWAQ4khahFo5+yVsZFjo7mM+MTvgv25Uscwt+lB3Espq94UQA3qxl1ifTBUbaElw5fN3y/C6nes04TpCrM4yLlZ/zwUjmg4I4TQPhb3z6ZDenD9xxumjOsCsHagHopJEIg8JECO/Ls0aSIRwAxeaPLzWNgmeRpSdLCIzjgvyosUWM4M5xtEvj0hD5ZyA5aIBHQRf4K11OVDRh0Yz6h+A8pD1qNFpj2yQ1TaKm9e/uHGsEYM3339s45+3Bqu3dzrhXHKsUCOffSxO8PyJBlVDKMsMqaX0K8Cj2sY3yQTM/btdjzq/jB7fqgVRKc9AEmDfCfhjbhn5HM3yogu+XN3lLqC66pcI7SXtdiaXky+3w/u7KiIuyELEjNrN/cshzoYqIUCe8xZ4MUmGfIHfe2P0tNUtcPG4focvZ0v460DGXwKf7UCVGHsq+f7NkRZSx8CuBnT4UjteGrIEJEWYQw5ci8pGPAO84opWUPDYyGgKZrO1uQhS2XawjGiWML9QmrD1E48pFXkw+g96iHzdGkgef7HaG99cHTstZvpp9g4vohtZK/VaDNI2rq99vcbuHNwuVcapy93ePjHIVXgTue+OgUVL30tWzW4Zi3Ctp1JE3MTAmIMMRvBpZOB4lvx6ZXs1jjOB4VGB0cPMkKcHjmvIfu/XWcIO27Xp822Tc9yj1eL4aiF6LmCTKgJIJidS7VYkkwxrXQKs6CCC/hUKit1A1cAFrUblMRO6qVNLRq2+e/GXgRTQgnyj7J10pCLm6NohohvNILe2m7h3Zxm+q9yq04qYnSkcH7z6MArBKoBw0rfNW/8DQJMe/28SGo7ThyS0T/q9l0kkOS649oMug5Q07amE28/n01fnGbiZbN3frPSi+1uZ4IGY0FDM88uCndINei454feGFrd8n1TifwdL1IN0JBSt1P+ftVg2LExqo9XU3AbvVs/MN8DE+OyalPa/7027J+Wft1HaSp5amuECxM524uAQ34idKt9CHspX8s8YaiKePjqVxOghPzfUcmtS+tIEYnqPTEqkDMLZHBe2wi2hFl4qeqLRXnORBRlct3RW/P8VNWXMH+bK69U6jCm/uaVCbM89GHpmzsyZ9uSUDqdz6/XqCPkAKvLlYrQnl1n043g/PSHe5i5Ogo/+wMG3LJI2vhgNfOvNS/1cuxgHdiPQhba8ErT4dO/qGM3yxpFlCAoCXGZl3owlJI/ptN72VEbgWWXd+jREtRKv7dUmZEqUPT3jWQPN1nqMore5FO50va8urF5yc9eR49SsiQVc4hoXafhUn77A1QxJDlUUSe8Qkn81pTpDK9vDjVvfgSooB2T5mNpaSt8W6kyzp6pUFBw1JGPJSNmStcWwuugOzKsTmgTJCLt3n9xtcpOrqFJeAS4Yf/bq/FUZis9C3kvqAQjR/68qMBSYlFSN5EmMDCoeJ9UxWHaddU9IV1rIu1JExBgXx2atXNH3PyaBWM+T/aKKhp5p5L04USIlp3Ulu48Ubxws0jjpDeuxZ2RmC4/vPR7LYGw4xSIIUvXSKPL7DO8PkQ5mrHIQoOV1adyoHs0JDtOL+Jj5+p1/KUyKYwbXvG26JT3NlRb73gChn4gsZJH0y8uxcMzrL0KUOFwtVj7SGIxgEmOOsRlUdAGdmwUj2V3OJs+rHIvnpSGCgX0dVDSMpcxwtnuvbRVad2U8634QZ7W9TXsg9nmDJkWTJQPLrZsrrqAq4wf/ukZfbRmys2IlMTvIY9E1tSCKyFCuUXNdUk29I7bFdb1I0MEhh7nqv9D02glwRK0RRDly+G8WOAB4mXwuHAmXU0sthjFs/DRUM6RoI+l7oBtrpmN0Vo+l7z0j6fdOLOgtrVE/bl2rvgOE7D422zXxR1cLVnX1rI5rnMjcanFpJBIGl9AAYo5sadJwsfN+KX1gLa/9HdHan8SPgLDfPSVKZj0LThMzi4SNUmLV6TPfjaTwlLLSEi7DmHk1tagavjzZs/q7K5MtcIX+LM1y0PSQyfM4M4bEN9LC3FiFsBSU3JvVYckFbOkZdrkDRpXy7oihehhXClCOPLmOo7XlT5TjhYjC6wTPeLTLqTXGbl9ngn/OTUvY9WKNPmtUKn44lsXrBGPfco124ByvlOeR02p0fzXOplNA18poFik70LeZVF+rANo491tEAuKO5c746mFJ1FySWYq18FLc2s03SPr+bQ2bTxziF+l86Sw8PleF+I+nHrK8XKZbSDxTS0qIkPBDiyWEZLPrcFXbzVSCemN+VrTMpuHqtsKSbpOJ/fByYbIKNx4vxqvb/Rq34XKAKvekEEs31EmC8IQp/i+WBApmqYPBJOdfSwkff2vSfsvin1Sphj34aOru6zeZ6MInFVYa3iwazmqfkPbJ+iPSIT/bwDheoARHpor9rc5AV0IGNoMt43Y7TORJ7AOhiKPHMv8B70+hGfYJmW+PWWgF6slhq91ZId6RfKcwoGSogup2hSYdnd0yQ4DMBbkMR5QQ0oBHnsCi66Mw4Jer/dVwAqZFzEcvCMIZorOM3AumxJZdjeUsn+xO1S7R2Pf61fe70HFUM6ORbWFPmmzJe7p9eu7p4kERjxZnqCscPEZohyLDfNQHBgACYJPGXi5ejkn5Kokh9z03TAqs2MAgjCW+dggPzncHZfA/X9OaNIt9Sdnvr1zX1xGT5I+uNg6maZgEc1cgiTysvXzY4M4MhZbSEzl9ta1mc6TrRdMb/oJCUJF79bXd/3YOW7ruEm1+GOw5llC+jggB2y5kTJ7QKd9RzOAEA3VDfLsd6aU0fwg/4smhgk5J+XQuVa2e84UVX4qg7FKQr/LD75djG0ZrjEX27TcuJZFwtRjDpRKSHi0AhqThFWi8KsdPa2AokBbRY9v2/gir84AiLZih+NVcyJ4LA5XULK6RyBT13TBBbE45JsrowVEPGfKPgjN9aQhmhTRZxG2zY+rQkPcShjSNg3NSy6RPGr7Wc5xKKOZQFOlwIQkH1QA93z6HqS+l7mke+zRqKJytvOMvVByrgKmq1l7zTpHb/h6ynKBYTTDSw/FXlKWrmIjvAylSyI/NR9LOqqlCuvXcCvaMReosi135amnlWODu5+qvp0uNacPrZDof4gBwggqqCRWITVHu3qNm9upWgSKhlqyAYTKPQXpEvgXhmN3h3KOndQtjlWzPDH/DvvHZRNv522qlRyVz/9f9l2h22PBMY9yjK8rD4SD67BoMt8U/A4llUe7Lljr7ghYFeCM96khW1ezVC179dqARo7kJ6Ocu8gZWD34Y7tiKejV4AwNX7pl/ATxxL/bwhcI4oo8ZUblNORzBQJITMQTwQk9lMrr9ZOJoK2xiLA6em2zPDHP6Eemi18nrp5NvXPsXHQr56RwbApVqeu3+sP9k2qFBjFq3h/EUv178oQdiZ8liE9s+PYKRpxUu8n3dQzvEAw9e7Szf4SX6ON5+xvKDSZNOD/fDQjMSxzKnI9/L2epDLP/EdsGumWQ4bQHrHz8/x1W3ixnXxpM6SOpi5f/EYPyWLvKn2PDy1Dk/MWpGXXlkmAWVZAqUOGiyk674R9SYzqLo26nmEctObXy+ZpSEp4f40uG1DgsQT802Mcaj8GEZajxzxeEl/VG86vDorDmsEfM6EnONrtPxlkgMBIM/M865w4hocxOkx+gu9bsN+0NBHhOasApOlE+5cICqf1RmeN3dwkOHaTZw9wSWVxeg8vDxwjBk+zMb1py05Jo9Y9ihGjGsLIJNIwtGX4vmMRWiMHPVXsNlMeHL+77zRYtUkiEeCB90ZhwVDZ8NXuOnIEZ8IaSSt5aATaDhPHy4pTtwHBn2qiUiPqsq5G1ggE0oM8WI7WTpu9qBZPoD06Db6Vo3Nzkp2sdZEl3qPNq+9+Rsh0/yshaZ4LQeEvtnCPuBaEFGlRemcAm086AvKEwW4pYB1juehJEqUThBHDpzhrs7jJj5e5VEbqNoFNnxKBPXpPXnzKx8VZCsfBVs3IOmE11IRlhvYHHf1GJyF6HwWT1y50uF8+Fyalg0p3V3tSrNEM8gohh8cgitmmwXZ6pPHKA13rdFhicWq5zLPT8TcnYELkcYFTDnGmH7+ng6uUKtj+yqqXZR8rISVIN2dHMiqjjH2fEdPEyGRuSgKOdRsCNLdKYUlWAC1Jcj4B6IBNakPLNPqw1S/ARYzvDoOH0TEWLmxa+2GN/7ZaQfBQADSSichRfHSPde+DAlac5d2uuabpSyBiaplDaXZWR+6aXA8oi8Rxnf99NL10ObLddlKnGjdKdyIi89hTKx4saOD3wezod3/42LWLIiuJT+XmQctwtNvatBYuFzjs4iK775KTF7VkLK4WaIsYNtyiQ92lSPw9t9PLg1TltI1DLRUbOKbC03d295eTEH4KAozNrRRt4ezfs6Yf4oNWHtaDK69khWLaf4jFlhCsHqtc+Mg2sGJMapjXAWFwZ1as4zoWM4FBidgxObKapN+DQToL3x7Tr8eGT8Pyjhp9mp8M2Txkakma9SJjLqIwjGwMqlQzENOfS321aEr8oCnukkSp3mXazKUPTJnzDescz6CZVcMnn2hFSbRvgxgWtREbDGCDR6zJMyK7D4g5gCiBr3+ZP2RYq7a34pwZ5lv1mvLXz5gfwBjycq95NSWud59WwPcLLzjRZ4xis9GYNrtjpZCvUC/hOOXTLbqZCDS0mFyvAL1b1wUshDEpGTH77n0wLtTzLsFRoHceJlgiRTqIHrOyWhoAs8KZ9PnFg6Js1SZaqMBDpTvGmRx9Uz7jW4RTyotUzxF/bbIH74LVx3mY0Om5Affbq/CeVBeypP7hB0Rgh1a2WGVyKkeBuCYUZ8t1BXB9j4Km2hIVwg3rA6lnFeOKe4obTPvaKp7BSMnO1YGCoKI7kyB4kdJQ8XFX872uBKyRnpVUEmTz2s4VJJ5iYPGKYnmUfUxCrv5cQwJhl9ItdmkEiDedqWzEN7sW/EnUBy8opn38TqOhahl7nTsyflmCLDdvO1phMlPmy73JzfcJT/l3rcaLwRi/EWHVS5jzJnG+bKkSNS/Pb7h+/c1EZVDUHAiNMvFeFeiGLW7Q8hJiXgMAqhBxleDVufXuOvgQmJBzH95YrvFVFAN9NDolHvCuHVZTO+wdjihoNnNbZre33/VKp/ZWlxgQWXUekIO/Lg+//ibbZ6GmCp3V2+KFx2pxhc+AFKKF2rWXVPfOER8GIHgk3ZOulVa/t/axd91g0BE3laoSFaewgwJPb3pkmIgkhas9Wt7ljLig5hgq2VP/sO28LB6W8QKfDOahcLvX59Z31JpUlpDcaWGhk37VMSABRPfyh9cnIWF2SzuoKCHLZQroTAgQsbqaPjJEbKs/yS9DDUd1C9S3aV4mcFlZ/+k59cX1kkJW5nsfax8uuq+ijsbn1GAv0iQ/remEN5OhdQpSR1XEhR7ZdTxJRq+OXX1fGbqCE8cPkZ7EpGqGqBTFDTnjp7gBvR0lnq/8mEvMrGyG0Qsseu7CkFAEEoWf75DdinZLD7Lbw7eHnogq6WWCPoXruNIIqcwry4agFulGYXIycm/1zMlH/q92xhGqiHB0SbPIQ0dxl1b0zaa7EgBD5vIyPsvsjQ5E3nTd08ElDrWzatJSzUmGThBwBQG7GJBOTXYXynksiNWuWSptnOg+sqzP9zrmpN4mAJNBIsES9Xh1qmGzzgsHpjCVfv2q+LleeDS4pDrOC9ClCzMLtYPtByGZ/a6tFhtl+LKqpXBWInQxdMzRB5kGmw4f8mDwraZtAZ+60o7aVKzbIZ8eMswD7Qr3uuThk7wrimeEZVCKhCx+B3ms4X4jSmR2AtVVXXOFACpDMBJPr44pGVO3SwR9AYwJbk53I3fSUhtGTHwkmIcoyNFwYKQ29GQ4O4mIFv4QVhXA66TTMxp0nXg7yesDnOk50QOJ9OIwC8hd3XZcjZrwCJa/2f0Sao2TspBqqusMu8s1eexoHX7CUcBzqcF694IBBx3MQcb1oVrKdalY9mWK8p4YLw9EbiukiIBN1kwphr44BM/xku+x/npc46Vk9cOfTZkdR8P9crEy5fIjSrdi6hhU5YpiMDwDOJxs2t17rXdp7qID+0JUltUEYp1NkUACno1sgDyQFMtt6ohutFTi2AksCwhAsV94T0qxpZ4dRvWd9or4zazyskj29BfCLmqxxzAQJGZi3vpLcVSzpa1qs+y200k1ZmxZ9foBWNeSPtGX/T/tImVD+1Hc5qqUpxwEYxUDPq21iUNE+sI4EvizGr3rqdBGgwdcBprwP6ngRjbtPzhLAg017oNq6yB2+7qo+hhdjDvJa4CbX1ZDdM2GF3SOu+SESEDSAzIwIzJzAlb3mxTgyuSGN0+SsOV6OXTgrreYRSvxf/1KJs0Ih7LY1jmxHPJMD4jw/Dc16hTCeQDTuIpv6HD46S97Rg0ecCQXWOieNwBNRIYed3TLhYSz09lAOkqVlQszrsKIr4Lq94OCv8mxbTQMF2Cvv3pzqc/0iFrUbD8ZKZLTQ4wa4Zua+ABCiyT9UFivgZew4ufA0XR1pHfVyBMCY59FGd8gFJdkHOt5/49qZqCFqDABGS0KkdL0LYFILRqCNCZoqtZSn7VIQi8dYCCWPrB9nZZfTcckPjK6Fy3fMeCuHx1y60+XpO0kWYoFcIwYAeTtNt0SSmbddOPIFec6IJ+aAKIGDE30240JtDJ071NWgf8pFZEWW91AGUCWlXLGocxQKg7ZkqnrKTUSb3uuFfK05B+PLgabnq31iFhBa/zDg2kZqa4VZcc7Y/bqKkBgDgrupeDQu5JGK1ZuAetQEBfy/Oq6cti1RNNswALdb5DxyH5iACPXuGELN3K56hoXnmofX77Hqnf6uKn6O6JWjAAj3g7D3yxtThMdKIhQZdZXYekd39HFHrpbadzepHAx+zdHKKW2vGsfGKp7iT/4SlAFrWy+6IZQ9JM+Z9ZZ0GZig7+z7xnwnG3fz1Tq2g4OYJfcmAVSwPHJhTJ64qHhynGzrEpZK4gXGepHa/c27zRefo8m7nnz2tHG1fc0i/v+Z1FJAHh/XZDu9lYE+XzK27TzhKjzm9aJe5mGcGAWmZpsbGSmIeq3vyVzpkF6njbFoO/RRv0aVdcM4A9/L/SAJh+Z15XkZ4BJCN91T9wczel2GOD+IA6eK2tiO6NEt2ZaXKHVuV+bC9gLGuvKWITKGxpRLqoY/bTWxputR1uavWeDxXoVmHUyBtpDEKj0q7+ZhOuf9nya7lMvrQGXjiEiZXZIAmKjIIAenBcItB8In6w9D0LCn3WVZXDIrIeZUs2HWdiFkLgvpLUW25WZnbha/WISsafccdyYv5h3Cz6hnF3R7xaWm7QGiY/RD4AFF4BsY8/Y08tpk1EcdvnkqfZhep2gJ6WJL3kmNkqti8G8UWA+7qqAag3uMt6k2xj1RKf46nzjFF8eGltZwOmRAR8fqQgmtqXO59g8oe8R+STfAjyRUvdRuLlVtwXhCgNT2xbrMx+okxCgumofvbz2uHyjfR5/tslE4itjia4Pf9nBZbbH80Xnm1WDATLX5Z8436Ngl/+VXkG1opc2+ckzkGRsLTAGAd83DWBHhK36R917Sxjx2NJ1QX5ejjFlMgzEs4Zw0nR8itGBud4k0s22rM/d+qsBQf4QHBY0D1CVBsbAKLYn5I8H5vrxlfdCNBvl7MSjOIJJ96ZHH+HwEHOcdDfVn7vp+RhIFOo9BY7788r44nUu900Wazgcab/4m3plJ7PoplaFNNQ4eVUpbKztDz/P1HNmU1I0l625ROi8gitTtNQ/b1VpJm5bSBlB76Hn0pVpXBhESyMmuKOo2UuKOTcmhAmNkQ6ahlQ+V0EUPdMwtvhsFbuNPu3HO+8sxwssat5pUQRul1pwwLddpjna5YwIRnJZPE0la+RKsIK+Y9tPUeltIDyc8P2/y5dPYK2xtd9E1OSh4imd+5VOtY2f6xXIzJOkBdQ+xZFUYMSOjk43zSgHN6y3sOSBT8JVENzLnMaV3ANqVcyRHM1HMQxJy7Z4Zy/x+HfVmOpgYjRX+vdHezNpdloeJzxYh/ZSyfrZlKsQMW5L64VRj2HUHzIvY+LZztRP5gLpTsGotFte/J0gLxqY5VsuSDsK+QvTTjQcAnCzkV3oX2EbWVE0XjReoF4iP1qnU0y/WRxaRngYw1UjSjAZ0ZzsdqY7hioKMh0Xr883489qjXOrcIytnCf4Fpr/sInx+Y0br1T+urEzWVCNcn06tV/1pxD1l4Hx25cp+c3QRAtWAgJWYRwTUfqKB7vHbAeoknA7WcDgjUnxu69aD+1JBpc99o5opMdcgcX1o3bTnR8RByAvMTEV+G/VX411SPQLYm4+dJ6PdKGNERbYMhDw8rUd+LtkX6XJc1fG7cOXuLYzicbNwtlCmMz+AbKJzCcn0Nf9/cqtJ5CgZ7AnNW3+OsgcKgdOVc7rlUePFY9w5xk2iqWHP+OhO/p2j0FulUWXHbGZ+v2NalxCh42z9n2omz3YflGpMkFi7Tl1IrD1D6d7UFdXKLyoYQ2Yt+8MXZjXNkEgnubT3XcO6/T96+WpbjCwjP28xa7Tr+i3g51D3WrE4jj9w6tXGpv7CsWnxY/lYwhmtwl5CPDNSGlqewQzhUnSLAErDHw+ZrL8isTVxFRtcZvRvSrfZpV+I/b6TCer7MSTQFUgOR/jnhzyywws7w8yRj1RRadKqhqYuMM1E5chmo9Zx7fQW8lbFVOIdos+I5FaOpfZKQtVfU1WZDK0GDAV0Gw0j9Ag6kwd6fb3nhpayIMUyYgwDa29jlh8TgRkMqQ9aHfrVo63q5ENkgiwtkqbZiNIh0qTwXe1oRqdt3jYVFmzmCrk4ejcbTXh+Wg7+jY9qmNDxRHlvCO0iyehw0XRcyWfPyfoqYA/YMVpE9s+MpisdiUPweVvqHyRFaxF58xbWJejunje7YU8FF6jQqO525vlP4UXFrSwWagn5yqO9Wu5kG9MYuRXv38Q4FJD/q2AOBz/MXKW2PMsbL7txZNwdKBYNIO3h3rWPsv9uuUtoT+xhWatYmp9/uYSNL9ISbxaNYDGR5RlIJlgPhds4bLjbmvO+2HzeuC8viuBwfY/jZB8EOwYe4g2WFkf7WJQhSvT98nxPZ2mf5Ee8M9ppBK3x8RE/xAs8G2HLxodPOlthQ3amC16WrtmATDy/nN5eN9p2txAgKwaHYZG9lK3BE0ML+i9b0rXYP+5JMUp5JChdL2rWhVPAKJUvoAjx7ZFzFr+SC+ZdI62YP+n1Al06DlzJhe7+N/uuFks/428zkqXa+Z2zRdQ64DHlYyBpMt0FY+aQy7eykUSOWA475kbqCc/9LNyPezibX/HWmJcvdD9h0o5cHHZYzVXpF/PEcU0mJlejtVKcIV66O7bFtmAIHsW7vtdNfHfBUyWXqFjd94BfMS2DYGxNysW2X+lfvfyk7oC0C410t5V3d53e1GeZRG2Ks0ataLj71gMPrPzkh52gMD+6SUD+EzIi/ZBUdumlSfK1sK36ERTW0SVwsQUs3Nt0o8XQyFOB0/oXbYEv32jm6EdtwnqagVZMFGqy3ddGaBmiAFp9F3Baomx+2Uqxi7630qnujn+s0MrtX/T3xMV4Wl+MURXOmyOlbjsWBBhNNb0Q8MnivmLQPq4PVOZtgc8ZzXBuhAJE4bwGnss04eI2LbpqoOFiiHsFZrvVy3FJGn3ntPuWhotyBpEXTKHlrdZmFKFLWPjL0rqja1fmhGRc6u8LDl/SfxcB+ZqC+sourzCK3jhqeb5phUQmQqHNL7eu7u1tRH474C1X+KDR3FcY/o5DM503fftmepdyltpaU1MTV69Y3LrehyBtuA5mIaDoTi5CCNc+LTYJV0ylq3q7f4M+p/CJW2FLp1UIzpwa1/jrQQ3cY2cz7/YIVq5Kz4G0XCXKYoCNbUHvpp7XXd+QlIUMyll6fTSdRDk25YzPm1ed58Z9S3vO5UAKl1TMFIZRxX2Mgdi7Oy/VP0QreA1krDd1w2SLSZdGqMW0bwL19BTj3KGsM8GbncG/epQ8QwbadgKgsnG5kJlmz1poLon5L4iyAel4lfUIE5LlZA3B8JrZDPpXbcdGm17F5McDConuoXPoiWSCpq3qz0Gzf+RvZoWf4CCOnDgkJRS8cKQcjmQ2e84xVIfCl+NEG67JfPXhjf4kBK2ZwgVQAC56BSdvAyB+V9LlseKkRyAGmEVk743M8It4xmHawZiV7SqVR3VzscLOu3QGiLwd7/sTwtkmnqRulMSg52ivu/dovzqGe3l9o4XzyF09gBJsfA3yAJE//1LAb/pt5mPyZxVh+4+oXJKY30cfSUWln9XbOnleUT+X4LK+4uUWsh5HIekq4N3ozB+GL24xKXprUYcRoaauVeqw4IXPuF7iEX69/3ln1y55hGeT1vYQOMKRsRQyMAOHD2I3IUreoljL6LXgM87opa9L3foSraNVyHWZOVYS0Ddgu6j3X0uBrpFqejG8egMs2O/Lrui2Acig5PMsFiHm0OqHHewU3eEZUJFAC3fmJLs+nSdKmFz4DQjlQQAiHIra0b6gte8U5VLy+Wsbgek/NwxQRh8udmiQaRSqIh06Lz7dIn9S+Xe5xRCfT6zs0IopxieGSGR038tPhICKscwqRHRixWfuf/5ZkSrV0g7hQtEC9TUS+U3K/lfKuGiDjTdsgc5I3C/cUMdxA9tR5WH98mIW7Lwpd6hsB65K9cAL/y/MmmKNuGdW5VzZmeZMPrYDTGdGkPLrOHm5yhMuX1MGjA3c3iG7DOQz/BC2p7MzpNQYtKlSdSNWlkgjIM0DkHtjF5po7BgYaJCPlAf/ByUbH22UPVvUKVWT9wapG1djgybldfn1iBsg/uDcZ3FrUb+HiX4RZkafIglQmBeV11ePUfm7txhsj2VikZgzhdvdT7wvnh8AX8XAwkBfVKK/8PdHDvofQe+mRewxKVtvhrc1bfFh7Sq3jEWZNLUlJb1WPfRr42/vsv0P8dfNu1VtXW9poW/ieBfD0mPxYlbwNoaN8iYKVXyM/BJPvevIOSMjcnUlhItzlP4AMRHV5hKcOpYgIwSyYCHuU10C8S3ZT6i+Ms3Vd5AVG9UwGuT66lWBsE0InDJQVupbA9ofUuLaOe3jB9po3YdpHnXpP04pGpPm7TCBfGd/b5vNL7FVaBVWY8IDju2qnDNTwK4ZhON08+RmAl/vHb4hZ627fQpG4HWjxNGhQr1LrvA5QSvr6SDyv/chpSgLkdsGyiWr097ghF7u05zUAdpRT4KxOGanS3YxFe00km2RznNDRIqdOB++TMegb/uwn0ghndDeV2NcSQOdp44uQf2foYkWfSgNoiOeAwxOIWeIUqvqs1nxpoEBJZ24T+AbCrbmod5afsv9vHgQoGy0C85PFh3CQkQQ2kI3S1yI11nU+aEVrphFkvVqEM2Tc+Xur+QEWCOn42x/LzKAiDP/DsKlBEY6zy1EUCkphAr83oVJXLqdOxBVs2m1CVnE5sbgDZRhs6T+S7S8jsPbtf9Sh69FZpOCrdqWkp9KeVagCYcTXa4pAGcJfr2/6g0X4H9/u7kIVIWOBkAFYP4+XtODCDCbRxjjapN8Aio/rBzaoCDX5ohdvf7agKEDwuWJ5EAWsyrL8GGaLXDJT/LE4wNdI8eHzz8IuFpIVc/NoJ7zKz2lCSE2qun65Agr3J7vi+OjKwv+j9gEmMBWMuGJ+bnFsFeX2tIqb6l+pXacP1uvvZmY6alUhai+8knaZ/8cCvJAv5Zx2YFQC0ePWc0xwMFOzBYj4OUkY/Qy8ELc/DQzfwXOKXFKu9JVRAmC/rGptw/mIeSmydqL+TBAFQU2dOM7dmwmdzRKYdG74escJQ6m3bsw4i9CAP03UhEsUTln9ApF+fZ7ApJiA+MSIBY80S8sSkeZXBbEU+1EtT0vZksJm2GIbs+3spp0FuD/YjU/qBoA/eCs6b5X/2YrXpO7TvWuQ6BS9JyIeVPiHAZAz1Bx1d8hb6C0CIut9f4xzLBJaRnTIhtSqfEleWNgepGGHBehfUvu+dr98e9Y6daJwjuMIGEct6RZo2dUNOB3NFQHR2z60vgVxBtjYrsIWNbtFgIMl4v1wih1HycEaRHReXaZdtXUfD3n9oqFzhBPOuxVpxODatYt/PJ8T+vwqMa16twj8ToTPJSewi9GwElkJb7RP4cIUysedzneP0zf9p4TgbKZSLru+plGIpv0r0HEHZy3ddq86w90EFA5r9bV6e1IPpgg/lFNxgJtUpc/QuDLycmuc5tuO+slnSPpiBVe1yOpO/KAEJFUY3Dy6SVDz8Ublrb+9qKlSIBwdILXtvCAs+mU/qOLfmMCIbwOX8YchQZNLTZRtz32fwU+L6ye0H8hxKVxN/sUoiSL7dpbdPC2WP0KGZ36bi/1RSCTpSy1jVTYzwqPzenwCRHQKQG9AOGpQyuYHGXzfHaVJOArrFeG0gpcMn/JKR6DvDxziWIwzRdSKP3I9uPMyc6ilztCiO5KcpqKbs0bXm0WTbb1MqTmC52gYjQiPvXIMjGz6CuGPJP+TUUKgYv4L8YceS4rnY6IZdtJ4gWbzimAbzRmEVZ281fGcGxS0J6GrGfwMfYLCcHnJsg12bJGdmgdEGMnO7UqGBhW7LlE0vrx5mMG76uMvYAFuYonhj2WZCP7+g+4As43JHAgvPExnh9wQm6GcRzd9p1T/afKtCxOPfYfbtIOcxNdMMVwVn4n9VykH0KB+ha4ba64DTqNKmka/M9aG06ECUaCsnnj1ZtnqIXVLVIvu41IUbxqt5rwP37M5HbbrjGBnQud7prrWcHXD3z5GTIidPer8VBOSiQAfYGU7IPacB2FDfG5fbfTrDz7nKzW8EK1ht2DfquNNs7U2dkMXwpYGe67BCFCC7RSYASMRP+GpE9DNuBATgzish6YAH4ay6bZzakh7Smo/K1f5L49mq9EI6wdS58gw/FKAYfVkY4JSA4/fnZfeLVa8Pnt+K6dyS88bwluPY9CsZUm6F928BktvMtavGtPNkMtSk4Ih2uqXfKQcCayD7u+ef2YoBuU6Xb5HVabqfyLEtNQDjfwWc4R2nW6Ke3ozHlpCM62fqdDfri5pYczHGqxx71jH7ucihanlcv9oYPmz0RYrjUfmFZ1EYQMWCjAt5pYDRiV/BSDYo4lqw+WQxc6TR0ER0Hn3umDtP4QYgzICvvZyHxxa5aYdKxF+p/bgP7F7Vk/p3bE4oLUmcydKpPjEcZ3u2A6DDM0k2HbZbVDIBggDdh4AGvxFXRol74CEKkPb1JIs0xZHUh9qNvIPpkEJFmI8h8ML+md6YBff7O781qzi4yaB6fR+AYfBM/5gBha3FTbvOuLrCBi3T9nncYQaOS69fZ36rmqzHHQQTVMvOLZIfisHTTQgDLutpWspCWQ4bc5tm6URpk+86b7j18khRUpqyKDRffizK14mswqnsXj00+YSER+WMupZ0gd/LEEcZHzAIExQfZLJg3dbWlNLffLb7b7vEmfvJJ7lK9i4jHeV6r/oqD7Juvcf1W2yNgQ3xasbynX9yTg/aSNtySrvF8qz11XHiolykDEkKqCiiTms7AcaCf03Ef2bMEgqCNBOUOoyZWe8vbq6ONdyIHqe+1gLDOkusPCcXi7lhJAzFP+UwCwIxB3GNXUYKilXjtsj7QKh/ti9QOr7YCxCCHwI+Jc/JZCfnFvSx1OQ/qIVgKAL0He/9/XS+tq2yI3msuK/zf2FkjiR5v0JZ1Nzz6rwwlDiwa8q9f5Xb2f/v5sSNDZua+EcM9+vDr/b56L7W1Q7Dzvx9CYfiGn+M6UZS75YHuhOqid/EWhIrHqy1cPi3YfP8qmRz1p5VUq2kPjd7y+kE43mTEjlzfhE0jrXRsNWGSu8kucOwO/eqJGB4drT/iEV64uDLZa5tytJnWJofwYIghIe4rLSWXEJsusin7F0+hLTX2lc8lBYqfEYXoadEJmfxTRaxk2jvQRLyZutm6RWPdlBv2XT8f6Tw8CsgPmb2Zn/a9ek//qE2j2OwJNatbTaJV3u+7BsbZ74Wj9Ya8i0VTvqjhInDZmh3RBIleUxHBt66ycCUMMVzUG/yE+p/OFZrrSSPkKLQOF0H/WNlDysudT8tGskJcPLahvqdjk8s3AUPuvimGEh04e1eA/IytT+UPxFb5tw6MFiChmQRKQ7bEOgHc5McZRUIwE+HxLFzTMwEnC0EQII1qj2Cl5CmwuiQcWR4prjzc2/fibnvP5cCT9Vz8XE6cFYHQI2amPfXk55FjPpfticN2RIJcRMEY9VN8Umv3Dh1Ge43twLBqmdnOOeIu3qC3zS66J+bgAM2zSmyBul9POvahU6mxyUXISpss5Ecssm8qfaVYK7WmbMbqLyyj1GcM7qaaiAlj3ZdYfXkhq2QQBgmu63CucTEQF5M24fuSEYZ1GJ1SIEItxyOtBwPd7xp14cUVExZlTBAa5GsAN7Ki4sPjh5teuu/YgKPZ66xgUZVshSvlQAXJDRmj1BGzgbYIJn8MwS95JD9O8giNamisO0Z8NERsmPbspKQL5AZMdzrvCf4RDcHgAOnuMQk/0zwu76KT5pwM7IfqCiCA2iEP1HTMkvokqqlj1r4Sq7dnp/LChe9GOtFwZZFlltRa977dWrbssHkBvETqirkkBCV5CzBbk7Qib9yYoZUNx6VYgXLi1X3C4cZDVzhn4KrCydJsNL0xyE2teYI5bSxC5JCKW+f5MStdjB/yrYtjufhxtlgaraY12cD6FcoaOL+r8z6Pl0C4P6oj1IKtSDKSHuGDIIlHv9OW6ylq8V3TyEJ844xVGhEkXZA4UmV/YKaFxXOuKT5ooLE+CLYFXbi+NcTq96zQ29fWQ2qlxGAT77Z387IA9F/u2O/BDjdXa4zxTVEQ3bDyayn5gZCyO8la+b3KDXrramNR95d+WywfR0Nww679TLjVmCzlL323hs7mCUchBTKyRCDad3W7s/NJNTQfjwnAZc1Iq+HjyF97FZLns8fQaEapJ/IE52/x9lWaLSF/83nvLKuGzYvYMNpjTgLyUT5bJRdHj80ZcGpkCfKLiII6+9kv3XDTgdCbCc66pHbkQ6bkBvrjnNw5FiM9Q6TlD6ixqX1KXDdFZgoricyeZKc6jJu6qIBJZVelNMH6EIcbFdqRM//24FGUtFBgVzKM2TVxmMU8Rw2K7xx3B0XkIrVodhPMRkwVwe6b142g57ikMJXuNKSLO+Wldx+ktEl/kLYPsUR0LQuq+5XvUjrEK+Bz2bP83iHwrBiFH/rXYBpZGcsOuUMJ9nT+MJIa/565HxVE9yAa50Nk79efsmOsRJl1eUx8iaKyyA+z2xGKvDmTAk+P4xup2Lt5jLlAD7oLim9PC+moPOVm4ExMiv6lMqh1TSYhCXlxB6a8FiSYPAS01ZehPkJ+R34f0XbzIjwwTDnuNMjQ/Pqx24fxNysR5FPp5CWhrS0rIfAyVQ4zjP9h6FxV8FHgnyPuivPEgCrdx9cJrENfHSZO2vfoM76LPFvBXgS0zOR5qRexs1oASazADT5fiDh6sCttjKjTZ9SijWmjC3o8s0oPQ3+HlHzyKdS/9oqbUDfBu8q6gEmUgV6GmYcBNoEDuDCmTo4guuR+L16fNnLw55YWjpi+GZyY+mtuDfLyy+4A6Sij4OQBSWvOZmOuDn95xpp8EWKrJMm/GsxSr2clsI3+ydbYCsahLXooOjvm80o2O7XquP/pyhbn3aNDBY6LanHV1TeS/HLOu3LWkMLXulWtnZ+SNPj4HeSu+mf4teVc3unABMkBrRAQr9zHgtWoEIktjAaAFNj4hKPSzIkkSkFBWQ2t0fq3/LR3vlCDRya9MZCDEsjPB4ZGTEjGkbdBLysXcqk7rRI0iezMXM9D0UVNh+ZQKarPj21tf1zemZwwtcQIGONvxZ+aBA+7K3BZJpleoTdEffg3gTk/FdNOLDkNCvS22XT74gjCOeykF6Ek7Aa2JPYBSVuHIFclXjndOmfKYW7o3iRe3Z9e6JSC7o1Vh9NepQM4qamlBaGKSnNkTGIATOgC8yvWFEfFG2I1khfVopBJlXKK/UPJPqYyfy+mZV3U8PjHw27lVzhh9zxe0Eo9csqyfEkEfz5eS5G7FAYYnZ5TFW5hvX2N1vBqRqDsxkfJSvZsx2gD4Do7YnihoLi5daAAmR2kxWC2g8zQUPH46A9wpiwTpzhuFX2X/DouF3YbR+sEBwMejVVvSzzPUzplngh57aKyD3k+hkJFObAve0Byvus+oWQyD23m5DsnWj+IZFrYAHElwPC6OdMknaHFUGireFuPrR/v6nY8ObfMbHbtrMxmQjjTZ7DGNHn4LPChkFrcu3stUG5qWm1C7whhACf1HVdfEUm/fsWyoaCb+DuSFQAeTRAoGmPQKoAK3/hhl1iBnc3ITNCYXeHmzT4JBThyISBbADT2SH+D5iXR3bCI4WxlZmOnZ709K732wkySdYrIIEDrGEw3/GEmvUKDZnDZvVDkrX3uzkEohjrvUlU1J1hIak+BZ/ddjZ6ZJostX9Ibmnjc3IAVLw4BvbUao6j93G0QsSKgSjNKE4BJZf/Ky7cyUqz4o4G8bqg7ow8YZt02Oz93gUy4Bt7TCBdeLcZaV7IVAfLWV0Bm/PSL4JkbnqPo2kmoWH4IauCTYly4DdLhBMqOONDaSB9OLYEI3Hb6otpAj6pwLNGcAPasz6EOPxZuXl3GIm6S4Qzn6/kle2l1DVaoMXJDzhRD+1pXlDXVrK45CQteAPFXuwGdqmdCFTEgcCsNKE6oClmhjOor5JhflefU1nEwlVmU9DZmJiKU1w106pAhbGd2+VrBtCmzFra2kg6q1e5viCJM01RMbEo9hasO1b9KRYuGsjoYRvAODSncAUV3/SvaVDyYui2Su7tcoSAhwAoveI6rig0oAsu83QGfyituR4qDMKheuaIbT+nVJ/CILeL9sfrI59qnxc57yeFXvBk/0v2eNxCeHMN2wHL1M0XYC/c5s5u1dF8P4tYrxy5TWxHn/vZW1fMX+OxO2pakh2GGRKWUFbCA+yRYVzmrZRxNK434XXiBY93IfzxBS1vWYzNEMhEmCJpa7/w91DNWLzCAq+LE2mpTVGd0uzvcTnVManUTseVaj8kGYx6tvgiuMJFNEZQUGizQDaTiLbrAkgwNPLbsPS7nbktgmepOfBaOAg94FoxYVKF0uy93UTaWFC0E4YOkaKDZiuUTI6qyETBIaMOrk6SVMihB8MGKBRptAd5OQkOG9qicmAwXZ6jR8wqhhbrU7/PT4twUor4f1HoRjgTXzsxlVa+vAk2CL+U7TXZTMGXMXdw3ciodTZy++ZoZ5JIGxlRuhAI2rklPWuNaNTIcK6L7kmusBTOOepYF4L8uYY+iJBsBvM4WfElEmIIQ7+6T324ospJ/4RSsLRIRtdKKVfngdUzYQaB2CUNhQ7M7l/4n/X+lmQix4nlwkIXNMDloUV3t0V3oGzCJf62uTCxpVwKXDqin83OgwkiK+XaHvoTVM3y6iB4cEpN6JpJf9B3ENdUQT41BL+0AzENh7sH8X2gmu1CCdYUsAKb1glM4HGcz0tBsvdHkopUgxPIME6gksxXfIRgeu69eUL11eYldop4v4Fp8PSTjeaChr+MUQ4AWStg/7i/0CeKJk3cjPi51qfar3jSSwqejOvr0d4tCV1xS1o6eEj2n0WpZxO0JlCiiObIsV8YnQLTIqHl6LeG7S5c1e1rdTZzMwHYLqMKjTkQg1k/l6XoWyLiDc6aRl3dIViZUmDiwt/sRl1obhEJSBeoX9EnI/B9uF95QJwDcu+XasrjUBa9xc24CRA9motUtI+ac8QmVf1eH4QEYc59wCMxoqTrwNsstKpf48qZZ2Xx7Zwsp8MXwlvBK45KrWbjl2gNp4mde4VLN79HfIhsOHMLd2wYroE261MGOEvo9lUc341ZcrCHY++pt2ooYBPS3PBlDR3XsVsHMQ0eBKZhEJCzwDGyd/vfrz6q24qybS1rQZ6LfN10+245EDUHZ1ECJmYL1syAXxi5h3Et5l89R+6koVxkF0eRtEeHHkYIUtWQAuiP1Gd2OfNpCbBgbC9wIqbOfl/INORKq6KtnGgyG3mXNou4xmcYpBsZWFvEWcSw+8cl11O4puo5Keixu6AAHEuJxjyjgTMjsTeu9DDGwJjaoixvRH4uXutHKo4OzS/3hzyVcZT9FzcUJtqxRR0di2mZbyS1/WU/8iRGFwEjZvBlDwmncZU9bn4DWhoLj2zSaR6fOwheyo9IdjHPqyFs+Kers5XLeIz/O6i3DcZWCo/XcEfhBzXbOplppezsgx6geLcDyojPRQv8wNs5E4WUm6bt8Qdb3xF91rOSM1Ey0dQrvEcMZeE669NTwBNsgZqNvFXpYsQj/w/pEm4aohY2dXPx9R0SDwPuR98X2ykxYLeSrYAHOEcXqovNqmw2ocJYbe/bA3KPOSaOotsdF7di3EKBQm1s8KsSh/sykwet/TyfUDjL4F9OvNsVWNiERSRh8edlnAl3yrluKsl/Mzg1CjM8VsWr61r2aBzxuDyzV4CwJNxVJ7SxbKGEzNFn2oQ8W8+EP50n3VVdz/2ExNYUT9tyVOF4kJ4jrTEdxBXZUTNswx286t8QMYtk1gHufzl5x8Q3OXaQmrhd0ym99/Iwdqe+c/QUAA51UAuUPcFLXck3YO4/bQSdyabgoyn+eOVE7SH3OjfKO0AvcCYjXxwuxwV3Jptdmqser2le86BqF7LnDA2I6thOWgLhCJfy2GyLYk0ie2gSy08tR3zMabuLVUxEecWetIbBd+urq7PwFR7gWCYI9vYHvotXLTQoiPFMF2dN9BF9c+OIvO/zfHdnKLIe08GzFDoocrUOLO7KOj0kbKtlCi1sGtRd9ixl6iaKKF4x0xaS8piNJ137jlzKrW/sHt8tn1j9XTq0DzDLirVJee0qdrLr3n19VSwzCXoCh5uNKVbpgMTDM46P8D8q9WRfrGEj5IeTv7TTdzdrcr8kl1BBUyRDY6jis3lMQSWlGmCcqd8zhLpngyxulZbumLBRK2ym1WvLZYxkWwezEX789yjQlpdF1p9gpu6XTfNXlK47MYXtc5EvoI1W2mj9EFpP3ytQTDjZ3BlYMKhIJoul4IhPEG1llbKAenYhgfN4KXQnM/OYpKnQRBlqaxXsFmibqeS02/ZbU+kkP23eRwDoPkUVEaQBhZLTBCoDudnqHC0vNH8GjKfCjUkgqWgwLOZ81BY2X64nJSqfNTxq0AhtSMOopC3vCY7ITFLcVaXOMbnHeCyvKysBkJPUKf8fYFs0Dc58VFfU/Ib1qnrD0ABC8CyT5+Xn6sMZOKo056x8l5JRfPFcoSdfIpfbiA+1t9QP35Rb7gdYuK2P0B5TJtlsTHKeuJC550t1GfLzFiiu3696+bKXX6CUn2uQ0TzkVvOlv8d0XGwH7eZyLUuVQ2mOOCJWQWxqF90L8u4kZjMmqqfhhme7iPbhqhICbO9XI8/Xciz2D7DZXbv050m3Z/XRNqKwH/xnSVafVh7SP7gTGkcLymKJ6IcDSE4zE1g26e+EpbKrYEEFW06XP61tBnZwCBqAPk6+lCV+qJHGs/Zw4csKLTrP96itkkrDZdUhUAUmjkNN/fvoHoyWl9RJaxUP6HK6YvIIYXWY5fvMPC/Rb9UFuLLuG7SoNL8E3AcV4s+pDZNLP4emrkEyHl314E+XIQ0XxFbTbKqlG4hz5wtl5dUlA5+ecCeFjpCT1Lo/6AwsPo9Bef6uXkFBkz8CacHeYjvCFQWZNxrEQXy42AOPXicCTg2r6PUCpgmGfH01dOPqLS7yH9Mo7oIH83ur9xgZOywhr8f8k95nPn2XNZ9f4oScCRr/pE+EGnL2GbybQhXVuWTA9Jzt02D2a26X3RviAofuOY391caKAT4OLQ7RoHzOuBxxbEwBaecFygw04qk4RHU6/D6HNOMcSDcsuqvUMD9dbExeC+qYd9cOb+UEdC5GGhE0i+NL8E8xdtdWUzryuFf/xDTBn6xBadnLiLqOx2kT1aX2d6j3bM5pXuc613db2XkEryS8NUGwJrNgzLk7NQTPZRr7nmyCg2S/He6tPZ58g6ubWDtpp1x/DUoRnAUYvyLcCDxMzl60mAdSwpQjLyejvvk5RaMbmjV1ttPmVp5tzz/m7LbuOGW+xkRWyuolbCfOFdyByfdnqvBSYyhmBlpMFkhjZupJLpmN8gQEfSwODHrIxypnz/EEZDhkvndZOBmJoCbJaJ1MoGY4ogM04QANIDTEbBfEaq7FkdGuB77JICGPbAOPDhiAInLlZKc235smTuJgdREBBbd0SV7QblfgGM0LLjNW6+T4iZmiOjUMGXeQD2CA944TY+X43TTWWDoL2K1HFTNpqWxAIQbZFCCh23FufspyNxYXE9mZ8e/QLisG/viPQO8cnKxWt7xV2sZzmbhIIkHNj/Jc1U487oU+pD3W58zYOa5DxcPa/pRz8C1F6Bjoc+vvf0ZooBGkx8w0PzvJQ16tWjnc31L4MSkCcZ5B/G1tBZBWFiPveJWprBGI/S2OGUWwwNXHNs1WIyPhrp7Ro1LrR5KdJ/ceyb2VF0vi70RrLCcnOhcV/cszDbUF6X66/v2Ul/063qP6Kwo8qyt626mcClFMJTm0sPJ0+OGoV/lWNISEPO32CUxpA3l44YdajuDkBPy/zPGlJfu4LQAIRSlkXQrFgh9oRredAfJO6cHjAOBhuTr21nc4anoGDwhafNA5js1LEG/K7A/wWqaZlXKYEotPYzFw0PDVSWFoCy+EN0lf9N1Pd4ibmbIIubCGcxx9hfRYHOigeSUzgzwso7h6J62jy7sNcVt3LEdQ6k9YcPOlHANKkQOAHiX+H0aOh9W6qTu3Qh6F4cQ+ethLx70UG7M6oYP4EakFi6n5A73Xino7NKu8qXvG+VQXIzRVxy+ClrueOMkikPEfZL+gSXAjzV0lqSXsaB1uvRu/TjuuO50s/Yswk1I+1/o5/QtnY57lz0rDq6EVsEvkTVQ/beam1XPK80YINsgmqDB7OetRZylEjMoVlIyCU7Dkgq3gYPp2+y/4nVNDsElgvzz/FcU6r+2vgixd66l7CON9WeznjaHueEbdRs7wevK+2UjnmSP80fRe0S2zoMnZ5zB7uSk/LzJLkgTPiYNjb0lnuiZl02ShSK1hvp1dWPSfLncr8oxwlX0Qr+StBZjNp2fW0lZwFBV02tTtlOSTNvY+NkDAOrbdMZY3mG3eP8qHU8+7e5bxBONbYrSZhSPT6rhykZF4mRPEm8HmlkjrSXcX6bkwzX0SqcfSCD1Y93o3+HcetY+vgkBRa75AMbh8U+T8XCoS+oj0/eY57yUVDMwuTJR2WDw0Q7HAOT99Hkm/LfExDip130IYcWZIdVZQzuNKZ2wffcFs9Qoag5WejHyk1R9YVd2PgszmZe4PYBeSXw/lLPXnQOZTbSExURvv27CFnAUg10htMkGs/tGY/qtCAYRvd1v0qjU3K1LLlRHUXnX9bSEBCHlIiPr7Xp5q0IazU7p9Ezx1ExI+IORDIHTd+NNS0R+hzgwnW90m1GeMQn5o7Fv1o5l4Wzf4Uo4W6qdigOzzNxKmSPD/xJDVi8BXEeQoBY4P5dGx8x73nJ7M+e3wTFiMH0wHVsJPrAWNS35zOK5BDFhbTImlDiJ+4wAbKjcHRyYBgyqZ+hTVa7Lg4VjiAGBgVBsIyRsto/A0hmk82IXOrMyAKHnWauVvGcB3T31XnzkpGvR5aZJd0l3uNrBUNZkfcN/tBNkuqBV7Dnk8TQYcu48wImhdkMvubgnKTzSyDiFEcLRvcTHZe/YXkly5c0LK29wjXDRTeTAd8+YxSSrkhKbLo7fu3Yu0C0ZWKrQEkdyM19v9wHXzQG9OKzTGKFoV84yyDmoTdEBArDrNQoEf1FlUBomww5m5dD/aE5I0J1/3nsnfNIDWjMndo7BJ1leu1scbdo6vzsM5phw3TJsbXWp0WtBcGwXCVKL7CHk6/cGlUCO32FkMCDsV1D8oftO+urhhQNUizSGBGRnfBfW+Bh9wvjpQ1boJ0lkFGZxUZKST3SU63ClUTN05mJtfJbZBTJSjidR7kjMUjihTU9Yc/SR+0WGwHNx/rhvfIW4Yg5GSq/PqyKDspIAzSRJ6s5B0rn7Yf2FxuTaRMLe/AhegRT6AFsi09DfX2giEKDMrvb+/n8dPRGI6OJdSzxrNUxf4UC3GltP56oDgJ7LMViHKwahko6SR5ihPvyswHx1XfT7HDiKoCX030zPQ7upS+8kKud4hi+cDQZ5tZr6UokrN7Cnoukzhh39q/9D8Iik/GvE5ALm+a+cPKMQDA72lsA/mjC0+dSO3Gz/x3pcExfLSGynCEqbBrBQ4Aw+hxE7GWijy2xv77ukCWk6LAmgFF1s+yyMMsmL6zO8Q/Xph738irPWkEgJLq+zIW83Ia/xbCq1JxWLl5tDdpkdZgnxthMs5XF4cue7mhA1R5xXBbr/9kJkPSciTNZthCc/kiDsd4kcmPdL+YSTv/BhKK5yVQBRd1u5MZlkzonMh7KoMJQgU7knXjoUlaUthxsdsKOjyPcBC5ip86BHGFgJBuu/aO3JbWTDaq3SUS26nO58G1dHFs7rZbHwNVtrgPeE1MUQjPSmagj6fHuDzkZDCwwNDZobUEYF5U8Q+lt8ML+1iXErAi0S81KWo3IQhC3lqjWpP5cxYHHp0z8gtZt4SqaLfFxp+FkozVzhlk8hbvVpmM+wui4DldF1SxcK18rRK35Mg19T85UFhc8/6jpE9K9pLFct0OA1CQad5bRLyWY6WiIZUZA6joumVKfLzBqt0odzeHlu6Tn8FWz5nLKe/g51rXnsnQdpVhp3eEncCU3RGiljdv2Lz+XIoT7STSA473+NOiPJbKb6jJEYoc4l9BflKUTSmbF5MysXAaD7YW6NJinxSCpIu9hX37aucVfcdqIwfczVGid5g7abwC3LcHTsgd0zbrNeARZfOKZVXYiY1zqd2ZdSpK/v82Bx4f5XPJPcj2HLIpF4yTlcNOz4UcpbX1sjviRuTHBgRBqOkNPhv0kgwKNS+h/IVfPGCNhgo8dQYdGnboUMWPxyJ3djsXG1QRHkicHjI3FXKurrAJQ7ZohXNPIceZXlfPTmioIerYJ7a52xJApR/VtOV7HvRUtWe5UMwekLcZOVdYA0jTdOcS98lrdB3g7XsVOoLe1c8AUrF89/LU9hKMnUNTWk6L+tjmWS0IaGO61AlKWNxpONjMokHN3ab5RZ5KV6g0nrnwuT7EmQS/98SpITRR8wwFuWrUutQi5tsSAQei1rPlT3snWob9c2N9EQHz3roJWXPns1Ms+yXta/SzxiiuUSfrVdhOkq8ei2K0ihaKqrCn2UTlyYMK7aTwYw7L24n3g2oHwSxXzT6PVdRDFpodfTO6n0Ma01yZHwtj0YrXmZ5uSvtPo8vK5Ek7OgKodqfOZTtm1ifd+NmgerOjohiBbM3a184Smt1wxCznLvV2fbPoGCgJdVpQqLl/qZF/bBVDgrh+WAj/vq/lhzpf5ioJqKW9X6GG1UregwEhCuWsVunMDPpwhCaMUe1dPZdhN23ojb9QiQoEW+N5Iukt8dv8jETFszYUZr3P10y0rIkimWerdnD/eT/mm7uPZ2RISoVhswoZZSV0y5dzQaMVKxPRy+z+NlIJMqTMAZFWAWW5oJQOULOA70e4ouX9r4FFDYn/C+6kp4cnWNWBtU31KqTu3pfrR5vFcCc4/W/0PcsPeaonaq1p45k8wNcv825FZTrkWtvrKEJEBPUwEmNDW/Br4kDngDfek2WhxK+QRmsnPxK7MSL+ll+lCR4u/r1nnMpMz4UN4EJSkK5CwAw1js2350jOov/TCvLBBcyA2rUh6QYTbTdR/FXhyd67YpGCeTvdVmFkc9fNvLRdopIUM2gqF51g03hqlBQbvAoTomx8d0pRA/uw/kPLvYbZPIHe4MPH/fE8vaScIP7ISuEecLeMpMwDH3Ryg9E2Br5Js4xU8WltAliXHNk4HejMoR5i7wHVoRYzSuFixtuRw9V9wzby+tS6OCiJWA9vXvuVwB6wpdSFXxvk+yuvqwFIzug3QGERzf/dQCEQYo+PVlvIad4dpcY2fJQ07510sO49XzlYsrBb4cXsJPpnnesrPVfQMVBzYqRf9XJPTQOu9XnbdwAWwzURt+Mw0w9seSSJFbNbMkAk8D7hctopYf/lCmRcdHiKkJHW9Q4lMmQdvq9zJXOw+5qS5T+U6ItEE8VYvmfUtvp2igV5v1JhBfZn/7JBEqy5VV5tSPPDUJklmNqsmajnjDsyJ8KyydWOZLR5dkeqR75Q9HzhAbiwEKjCbE2+YEezB7Np/8T93xmmh7inTXA472QxwmiS4QM3XKCekdz+/vNRFRAD7rnLd0xYqu/X6DzrvrqrQw3R5t3mTEupnKMe77nEp+VI3wxY6LonIV0FK7bLbGOfNkVfgO3eLz4KozP//sVhBm4HRqjyt/FH+NVvU2L80PBIFNDuGZfwged9GyOwOHZjVIiChjwq+b0Su7LrRjf3XI0nYcAU6L2dGPHBUUQP3RMJsUc5H6kGH2s9TU78dX7+s581UXp/KXCpak9wgXynul8Qg8XW0kQSD2pMf85cHB6RBSpj42okm/fx/cnEiy/rY+SRghBgLKpnVVfUU5C4diAX+vUIPwUNcYODNxEOXrU5bsWEnWOTrxByqHn7hZhke4xn/x+wjn9zK0C6CIVMBQvg30op87STWWgveWUwt/Juum44IRcmXKzA3ji8d5qhLcRF/jdtkqrjTkNJk1Q3MYB38c7dc5aJy3+vwHZ43jwkPQiTxEUmp9aL0J7xRfATgEJM27yNvMPV73KlgtjGzaof6qcHhnoEGvYpbNsyMSuYAojnkbaG2+RgNAuggBBkGlfbPD771mBiQqsex4VPt5NX9XTtPunTUEW/LjM2kQ9UpeVdwY8y0qjQXA/l+PMWXBilfQ1uvh3AvytwDa3VcUolSM2OlruAh5ZCLUyt0V7H3yvGJkwFUKcwHEfX9v2Lwe71cvTTOXB8sMkCrWsIXZXFfeVRHV0rqdiE9J0VjkcpMST+cMG41Qk8/F6/ud7+giDjsXjVrVaq/oX3zUyucCTZqhjF4KAVR4GXqlIDO4tMTWqkhK7Q+m7Rbeha7Rx/ThBnJJd0TTZCg3ma4C2gd0k9XKdtZB0BqkC7p4LYkPeZGbDur7isocE4u+/Gfr/zgrUMw6CgQCEO/0zo9eIDmFzglcmn6g44hOp2qN9li73EW8K1YqF/LHGIpP/j/UADrRimEaBKGt74wbBhHwlarr9E/5me6fFxqkamJTunbdoS2YIgHNTnfbmbmYPeGFGpzn47Mwf4/+n+0onENQldfdPJh8hh1eWH2/UG9gITmMRCp1LvWwLSjM4jB73vOKeSuqIq1I5qELvjdPRKAomU8Z5wreptK8AseQTSL+tafhe3b3EwAB1z2xR6hIG5IWAfOkEkJKwXyQ8Kr1qHHei/kVCJRQxkjupzdEdKf/u/IA0CnFXAbDRRfBZP6rJqbJHGwTbcMerzojRMERBM7HHd5scD6nHVRcJwKGMJV4l5NeuYbVyMFU+SNggOH4bx778YT11Ddd7FHJiEr4Vt374BKGCgUWBzaQAPpwntYwOJaPfUr8Nc8T+7y84kidO/vRvV0HO0tOwr+yMGVMTf9kIfv5XMIMKpkpj4Qg+mhRUwt/evozPkdUIK9uPE0GzM2G5eWQ+ZFUdDyFkS/FtnL8Iwa32BAo4BLIZ+K4Tq0ke144jdjNVBK1CGbxIPnyBreGNVf/T0nDz/2zsIj7JYcafOm8gOlzObVPkU8TudMKnhzgD1TjCS5SxMgvXnRuz2YXUYMl+wNJ24JyQvrdNIxiz8PLLKc/Z8YUHd5KI4MINae5U7Cix4ZqFeucO7MKKTiFeHTv6xn1Sgi/n51bTv3EVoAOzpsnGj202adYTjtRDDhJhFWyxAwbTKsQM66COBwq2yKvNZZuJpHwHsIIT43zyU6GtNzDxdrwAnDcu4CnBWzP3BVcdhCaqnvl4lOlFqr5eS8871svkNCb+835aR9IvsEkWpvjEt9kOdjx7lWHn/BHYawWjLYuK4KgFntaw/ROcE5VY0lXDNOpQqItDr+wDhKEk3XFBOlhYipjMAYN5QFMjSsUof3wvhC1QvTguc8OPd5ih+yo/XskXeV8IQL47IHM/dILHTbE3Z7diDxp0a+OWBDR8QPqUCoCZEHt2jNUKbZYQYCppRSYN4zmX1tXOdn/tbbkySoPE44D/gzxBUOMjWFWGR71DHXlnF1SynvHfjgxapgd51R6wAQGoS57WM6mODxC99RhV21P4XFbQHPofZPYZKEyyKc8625UaqLyZu9jvLhRwuasv1/ZJQeAdfd14WTpzFGrMbkrbeTOha4Q4NTJGAm/GOqJyRHIKHDKD/hrNv8YDvMC1KeFQfdmCOpflSUU0zXieavhW9pu2bkbW5xfrUlsa9ApO1/fqAABVwxZjAolvjQqCJ2/qjFatGIzvXylFfsFhJ9fW/rduFKrlsGjDCs07+NK3Ev+6yOQE9BGgYFd2Espjk8I3Fq/oJubHKLFMTk3qKN9JUB4bf9pOz53DOk5m24SibSjtkMjkho2doCQNIoyTZa4qR/bKRUv1l3OAxAqD3lE8zDGdQzYHk8XRB6NsXs5wsVeudnQWwgXkv80zLF2l/8RXwc5tK8enPYlbgh0II14KKw8iWewhnGOJhwYppLm8axhrpLcc6TwmdIlXiSCssusIbxHkYMco6mN+B01rgPQldRQ5slk+4Nt8eNrq7W9SxKdguXTjqZ8hP7iuUZUxliAIJKohTZOLabyN3csrJZv22N4nTC277erFBaYsJT9wYxrSyUwxFezdDc5O8i+bT09Ifsqp4SyV7GLPhY7q7GMV6SozQclGfsIyCdysuxcHtz5NVGRVHH9saQHrFiHFoPVzn44nfJVd8n/PVHM09yf/5qq7I0lfgqyVrncSgX9f9k2f7S+UwHpkln2g3K3Q4IYlj4wXVVJnwylWSR98XoJI2gDRBpEe6BEmeQlZu/NdwKHC3NPhPHdLdiiiD+if4J3af9GlJ8NppbFk5e77x1o+jPiV6B70uUJlDB67ErRSYaWuPSALC15I3tviInM07I6tbwfIRm183G17hW13P0zVAOQytTKLzxmIvwYWaWdORDUA5lNWOdiOziGHvz+u8DwqUG/SolafC8zBMSQG3Q4KvOWJHG+ZuWgBdZVjtuPaBAJejcrdZOHI2+7AlNxsKTSebSEpJUfk/SAAqgi2ehe40T/CuYZHkmLpyhQqpbis61tyfqu3l3kWjriE23/NYiP8j2XiV3sWnKZBl+OmK8wD4WFlVoJ3V5KmZI8f4K2eK6epFfGvmNUaIyQUtuCPxflKNq/Q684QtqYUzDjGXGJ8w3ZBx8c+aoZbtmNGjE7RtqAAyJfjPibIFxjeuEW21VADUw6cTqBtaFaVhy4560GiR6Bylv9Jwj0chvts3lFYbvoa+RZS1izJgwLgxeTGAu0n553Inuh/aQpDAr/iGKTs0S+DaEB9x8CffH0BMX+SaOn9iqwr2If8zTcS+n5JkYnbbUWkXkUBcwXHOE5T5HyanZpIwkOA5xyBC4JbzpdSo4Cm8yQN9pvHkjriwnNbDAfM5E2it30kNT2Dv2M1jT1jG2DQNeIOx3oZIMY5cMvVxGNUbc5UwjYZVBqz3VTqBgG8jH8UdgdJhh7ZB+5uLodppMEa6L8kgQoi/THx74bNCSenXlouT0Sg29Jdb4Om6cLw1aSakvA2uQbTRzXcvb6LS4iUdvZzZtaxlig+lpFpz615IxvLd46iDe3yhxAJShFh7p760ZsSaWJipuJC4OOv4y+/TGKkVbwecKueWzLEpFPShc/CI9CU7wj6lhpwbO1mQgYRCyIViYMSpQcpw5i0r7v/ROHB98ikfxOMPmUybC4mg2XvtaN4hkwHu/SkwJ85cqztQ2wSDLfGJcGhZAMl2c+5Zn509IQ8k/1/tE5tS29WbB5iGNq9aVD1g8+mToSRVvKDXN7cljrCp1/Gbgo9MRcnku4zWCzcTKw8sGA4cgqr1K2IiOBZVvNjJektz5UbKaZoWDLsASH5P/TIe6m5/I7Mkji71ousq5yAF78ckSBWc1bbRVFurhTv14lpnTJjAvW4doAhbaaK06srsFT6+kdcdbS97KzZyDCAzS5zVusdiqX/cAfkswgntPtAEL5bw+qOQk8sko7PDM3TUgzkv5cvJcXo+PePvelNTviWnwhcaHKusH5gBIED7Qg1Ssjm0dC26CubtAaAjYMOaoK149rOoFH1de4+JJbIyFArkyGVYa3pWW/Oyd7EwWwOGs/3Nw5MMiPwCy7K5EGahh5bSgT/bAhGq6cbwvZQq5DnQcd5+zLoVgcjvDaRd8PODYVJCHKrIER/w8EpDR98s693gV35oLgNdlrSfUOTG1iTX/zA7pHH4SDLOGOYsuDYtbfypfIp21Sq/JqZC+SaZZLGHMEv5OfboSbbbhedAIwyRwwS8TUOyyo/Cg50xaic5MArLmMV+tVSKiuh5oJYrM2Lt2PiV1dKl8nSrXzqpDnzwK1UGltjuNHT/tMxfsc4kbeX5RRI0gsaYN70pIYo0G6zwhNdKkPYsC18Xy7dB2HV2z7QTFCCFhbSdQIuzDmecbow6wz1ZSwv87bjsQgR+rY6p3up7WqAW2juS6FC5wlRVKAfD5/u8c+ipUxXUKem49E8YU6qc0diNR0erleQ5QhSC6tufU/68gM+lY8EUOj5jEyjfDHMBi17UzhbPMJjL1fCYADTtdGthnFzRhEoMmj79HSwnwphjQr65TUod8WBZSvs7rUMzQ1rRpusLuGmv77KtHfYnk4/h0TysTplerXMdDjtIshwcIaolb02DIVCJ3fF4n5FO4bjtm7exhSmB/nY7ZwzMFFGKWhIA94KIAqofn1HSuNTprLdVF7TwUfW39NgxezW0DXf8/+GkBmCigAc8c7QB33F5J8AJuq2LWr1iCZzPJ3xdAHBzsY7WQbA51FxwnNcskHa9VX77sQpDsOkxS/6fCvRcY/UxOtFt6bCNcG53MFJNissslHuVBR53x5XrFn1KFf1GcpIAgSd5+Gps7Nj0gt72qnQM+NEDb7xdrrFs7nJuLEsCoos1u4oJJqiL8Q/lWb1gv+5tAubP+IgIvZBn5fq9ZO7QBI3kglBH7L4auKfrlaEVUnQ4YK33cFvChIUfd1/vEdgqrtYF+Fuo04FSPMEv7625KCcoBQ8c56dZp4MIGhfPYgJ5lbIFOM9amA9ZsJj872PrtU/52v3iNwpLTzh2ywKlPAjQYyWmYbkFLON6uHn1lI2gK3SmVMN5LmidiFcncnEGqMImdwrqPuc35y5WwQjp+wXW45k7fBum9in6OQuNk3Bp80wRtYNpumN9XZvC6nYoO74g8z0UTNQE23Dt4f4Xz5OcFWcUxfnB8Zy6Z33UgZmyNDJRH05wJMVn54vegITSdkEoBoBJ1gODRMn019TL0IuAhkK25laAQo99wkCAo5Pxw8lOWN62gEc4Yg7dXdObVIVL+ivIDqEF3NIegp4KJg2OHFcqVmFu47s4XgROgZMlOESNivXeU0QOVc1747LN4YjqRnFKw3tvS/iFSQwdRVcUSH+i6yVtelvcAd3htfqki5KLStbGayRT5ZFiQWVBoP+9Azc87CiEzpFLnDDvgm/+esTgkm7Y52+fc2Fdulx4SVIoc141jymO45EET27gSxwWadXJgLkqIA0BkckTciZI37NllZtkc8KDXocSccdwJ6CM0r41oIzg7AJBPbRQvT47PPxdTw29e0UkSfwRWpdyeHeQKBNBUv7bLJHh2xWe/+cW6IM80UDGhOcUHZM7B+YOKd/icsYo1bQ4/OzAMD3jh8RozroENZqmg5LVLaJEIIhnzZVomS2uWN9QT9qB8lsr6a8d9lpnQ/DxccjBDOVIKTpYX/SiV6Zvu5mxcneyrRZyQHYeawalhW+cC18DAlw95EUgNTHfcsK8XJ2z2R8/Wm62b5k1cRp2H8cT4iaXKRkZP2CDtcLFFxQzU199OjqGp/CUvLshgYfhaXw8t8LO2O1mv3037j4rpHneQabp3ZPV9YeNSJfVbumDi6LMk+jQgSsovQbZ7S6WQ8kudp3HAgVY33qXwqnIeukRRm+yKS5l6TjuCl2IoJRZ/3BcAHxbJr7+h3kCDWAPcXLKZQS2omxH8qaDvnqFeMdRzEpOq1awHDDg1yoMar/h7Rz4TE1O09lkCVWsnyU1SJco1YBXM7dYIAlSVSekOVX7TRzpyxTCYInWpViQuUVQs5hWXgHnQxe8c7HU/UCW+V3OdYL2ZdU1VTmizNjPlrmjCBBnEOAHIvj10f4AXpC9mDmz5f46OTsiF3OKB+yFvasmkosNYOFSs6fC86rziHc2x8wuYIooIS7MeJ/vhyLvxRYnh9QVCQn2mEdIXr0DadheMF1Bpfkiv7K5C0E5Emx6kbQ2k7CLeOoytV/vOSGMKYuYTNIr9RdvPTLy0t01x0gDkrbmYoih1uFUTZSU4q/h7LKDzPtuSz4dgTnRFZ9j9IZTXawBxjkrw9rv7ph/qjCr/QZmhgpxpBvKmvptzCUkHm1OwtLqngtcdhwXiPwjAdRDEEaK72/X8DpAWnX/k4OY+T7kTCj3THulOpBLHHGx63FeKkTXw1mHqTyNEBha4P/R60mfP+/K4l5NbOFHTWQAqCPFYAaSA2fZjK9aEmZIbk/CwpniBKuuuEeRwlz2GOnP6aDVVM4S3BQ0+Bhh3vT9009GhM+h/FVbdFgK/uJPiBbZd9KVnnkyP/sbDJNSW4FORlkjt9qilXDq8E++YHkYJ/sxMGLpHlyCDL6xMMIx2nxIijqnQ9cjbYTTPYaE5kninf/fYuBBGZGL8UfO6OMt31k8J+yc01sx83mJyK+7kDO1i7hmUn3NzAwGSn1Oe0V9QCFLEn68CFyIEiO/elFEf+LzEUkjWYouwGriYpH25uN7QVRUBNWGw/tEz6A1SzDEcSyA2WMKTXgA0iuX3WiOTwJGm+L+XOAaL5vJRmyX4kCPOVUMU7gZ1MHGouPdrYDG3hjBi5PU4Ky3g2oXRmqX90ZBizdkq2TgMMN5++JT2jbyJ+C28CeQ2rmjoaECpO+S2M2nFbkPgvnYLE+WBGp1H0WovIw++b0ICVJb+Bodf/wPdYmo+2qTpCQUpRxXfBE39lfYPrm2XlZsjRreBicLGTIpbqXxcKpQFQxaOQhihOPUrNcXWLRZBmB5AJIOVhYW2bYQjyAKbubQ7DbxnQKkN/uEzRzS6eol3FfHmPpmFuzswTIUqkndAKyyj9YyuE16RyFnPJ9dKfZEuC6r75WeQND1vbvYRiB2ctIdvZbLSeOVq72t5/JDLz0ilEvNq25n4B92m6bz+XytnyCRcmKnAr5K2iQYEWrinnranQsq4AVbDeGvPltSapI/GBQhBEmhBoULUguST/Ytq0W+qHPoXgiicJt85p4N2H0/F1EQiYRpnB70hNYkfdECAwYJEr63lTgGf9OBQ2e1fJM246V5XDAZOOC37HfPDitIBZ4L41zPR03ottEhkFIqB8AYtPnkFZanJx9UUTjnymO9tp+x5jSoq9WpD6M+qCE7ts+H9p8lzEbzlFN7ejnNT3DV3Y75BO8QVDsRzY0mqIOMKmH1sbm48s9mFQV6lBqIX3efXA+vaPylV/DrbgZm1NbNzBU67MUhqB6l0nNABUozb517bLj5+6JcJPS6m6nHfphotV/vPF2qAi1fAnmV8ExOmx3MnwMotLiGSejQpmv/ChoXfAPM7TQamV5r3Q36bzfdjbTbbrjj9qj6OrriF+d3Ck96HVIeCsgovf/MN4ufe9TktHAoTh6P8XOe2PdCXkMRHmOgmEHFmVrGp2UBZ3ElN2CViij3Ez/GNnQtYv4H/JhsLbpmZ75gQ1QM7VYu2JM/MRolpdth85qZN6bHOoH2VrCRRGRY4yNb1NQqV+Hc+QZXGIOG4/4QHQouPsoNe26yyopz6OvcLn7KTbY3QX5yOzCTkedtGNjEI5pUJikrD9rIm9P+3p+h29FkagGYCm+jz+lJZJpnGwqku7dO9UcDP1G+ojegVRpFGu4+C0CQk8G9tkowxgTYxskvy5l2WpsxO/K1bW8egx3ctQs3AAv2BeUJP9LR8fxieLlerA7YVeh/uPYr5zoU7Rx8cVfJxxrwJUM6Kn6G7gsXN+TT3sLLqj/w9Dux6Wj7a9qxlhQ1ekOROVo4fFqC2fWlbjjRn0x8l6Ka6tawXV8DrEGo4Kq3RxsDMPoPKeZxBDjaGU5zr3QuOQ1kuMPYIzrA2kNDt0sDDHms+vm1cnM54iXnsymJloW5mRVQgADwE4KSc3uNomDNIRY60QdRA8yytE6FYvlwyle3ZD/dOBsIu8YVrKj5da5NjnYlblSIFzJnJGoiXPVWug2R09d1R0gGSCkWTcQdFyNE5528lGKEcaHR0Sffz2C8fA+6O4fXB2JQ5MzLg4lUZImTrHUO2U1tClzitEd/+6DS9C06kiCV23xMELSmXFc7q6D2TGfYMaD7PwUcYREmYogI10DtKIokdT0WePyA9PyFQdVz1Sb8kKOY8LlaRaFrQKVaeaLOAmL4rcuJmm0KfYcZmKankHS5Oj3W+4bxHK3zbSTEeNTPgsbQRJ2mksXMmbiMCjI3dWllcaSl2OIRxP8Ae5y5ccSXC0nanibGcbzXqpRNbf0PbD4juy/xVzMqWm4bi3SP/XOFX1QPPR3OkbMj7bg8/vJciDdMSD+xsvVdOLc9kP7G/WWhMLIMlYqVrTmD9ZJN68yMtRgml50B6o58LaDmYZqWO/nheFT96/EzEw7ayFzEBCGFYMFM21WPzj7dDh5dtiJjq5ptB3wLo+PblctOnQzV72OuXWw64DIZZX80M4x6nAuo6dEXHbnzOfr3dBd8qkrBZJIQ/OFTMmryxvNtH8u7BD2EB8uypfX6H+7yPbGh/XOBgOnIffYRdxyWl9ZZYyRAC77L0d8Q257h8jvqGMfLmYccJ06OFwua/1JGr8dEYHjM2t8ibxqbLsb1zeOv4vDuECC+0x8deqwqBI+hxZaYO7G/Lvy9hQgspnE1bxNy7e0tEhGBpEd7qfCgHjV0NSrHIk7vG01ToB7u/+jdqhDSvt/qoZ66aeW7Yc6I3KY0r9SJbBYlQWHx9ymvMdu/eEju58wPkivu3UHPlIf7D/WsT5KabESDzAgOjmHoIkAUVMVwQkZP1G6srFLLE1avJWU1NrNfk8JLnza3pD+e3GHprKfSZxr9AmiBO+LLjArJ1SgUbrIA6aFmawP/bNsEGKpwMmYdDYyGoltUknULISvhfFxQClbGVJsecTZQ4+0n2zpFrmOszom9f0AXMB48LlbLiggNOLvi6O6VUa/Q1fgjhDqT4g1Q7Mo9NCRW4jUrxwFLJd8VWS3I1INZWokGkyVkWOub/O+4ykynCyBw8i6CpKe6X7SpD2sGHDzJZiA6hIQ4JkHVP2i/HYkE42zktuucE3UbIXpYeEVwNbdi90fS66Jhw/z88EZtxETM3h7aaH3TV2H5vo7ZIZV6RZxq2txFonPLpkBP99hjM5ZdQcV5Xh8LStuatU8GPcS4C2kCX6wXFAcjeYWitYN8w39BD1z99PFDB11+WyYzwr7A8b6uI2itdFk00R+Sqxc2uXPTV5xFJ5LiqgbFT6EHbF229dOMbvEn64Ep2E4lf6HEfiOpiU89aql9T4KCFUd+0fUsaI1UYZpkEe8keVW+t9e/uwo0pmbicFZX5fSyrV5t8bV0brMLK+E+2otTmuA85dD7CHo336nIGM8q9tqDPBil0GMPOwzMbTdiBY81/2GQCJ0x3i26UoSI3nqpY0qOcNtO2UUEGTtriPavITpw4x3NRzLN8l4ouYRPeC4YMb4UFbi8+HvLjeIAaK73KFZay0kjZky0dKZ+iOcdZgJYwgq8puxHSM8FIva9+xrPdbFJKDgqV2xTS2AN2DSzCtUn7h9ffCytPkHf4+3anmmfYJEeQDSv8MCvGvtirJ1kbB6rxi9YQrFD79ee1NbK47hCwok37L41suFp3ik7TDwQ/q4302bn4DjC9rrswGkYC9FlKi1riOtERx2+CpGPI/EddVav13mQHszh85Fz9W/EyvHR1XIk/KtTu64SVr+9nUCPT3dtN4gW8SP2e09sVajnYVAJ+4VAUSyZyxkU59bmJaDBDfCvDahbR6ldj/I2dwsYpwMhw0LJXnjD7WjPSyg5BX6Z8gc/7TKEHCOsfRE940Vo7AooXz/QciOy3hkcCiBMx1S8i0Art7qPB2btUVLhN42VCdBioBqufNa+H3Saps8JDoSUxguHzfIYoq+3FV27WZNP6WBFj6+YTrYxA9arZ4ybEVm+8NXUbAu1UvLh4e4o33THABW2wKWgGZetoM7O5RHr7h7P7Do/XF3r4CZ4TAQ//ngt8W1+QLSVPuJU0aPl1Py+y5vZTWGG8BpKsrK+3nOqVdkC73pmI7JCWLN0TZP+HKq5yzuAdPjcLH9G2MLJjJoR4OYnoHvu3G89oCMtby7AtauZXhd4VhpDECaLRKeJ3nyoSRGUFUXIejXW4pb1fTLHFnImEREN/4lWHutFq7sRk1m/G0T+Qy3YGp2v6PHmU9cObzypWtjxtxpeBaDkhLdqWdYIUNZSwcLYCwgyAvFfwVar7JB/Uz+h46vjrP2reWuoZ9y/xfgbEdLUaVpSgzhINERVlvBqAcNNuy31s+I4SQF3td60sOUZEkWKXPAwUe/W0vAvXT8et+tz6bvJKj02+gtIJ2lAjVGYWM9RgNZhzZkphQLFSQV8Sus2LaibQv9D/auA16ReoX64Keb0iFkz3ZKjuC53DufXvIvcdqRbzjQnXAeEcvuwjA1VkMPrhSzUxD1i6LBkhyEhgHkdTBKrMoeG5IjVKn51jBhh4lKB/bpsrPP643GdybeS7Hj8TPd/b9AcrBSPD62rVoheXg3W01Wg46lw5KIMTv+kzX6+1XkcjKZAG1wJZc2ZxxmM8ZkY+5oSvtkANiRWqtXxv6c8BGhlt0H+sxKbI81EkA3sNqSBHPH33b/koiBTKz8FOOQXeawc/XS2ynzP4ga3tu0MED76JND0n89pgCf2x/7QmEoKoi1mqf1sVyYQlzqqdjvBJKTaHDd5HmAwm2sv4L7CGdOhCnKWV/AxzlSC2eGZSuvdN4KDPRfXjP0cTIYbjrx0LvVoQeP6FWCJt2Ve/7byP25Kvh+Fbk/2PQ/Z2v9Qp5Hgl7Vo0J4QPPCLTQV280XTwsYRzFN0WvwN0KWS5WcPqB4ul0COtjVtGVvKxdtsm4MydizZr0CqpuY36ni+eQGiEol6w8pk7JLfREPpA4uZ1W+T0GD9mfIXCY0/s7EiyIa/as1hjcs/80Q78Ht7WLNIy5XfOp26jP4uqV5hb7QR3uQ866N7ibAw50nx+1LPAajCPtM4Xjdtjyh1HfIxtWjFsmFMxQtN/hEjcx61dcoCLU/kOWIlVAC3jU+8u47s6hWeuFTgVfdi1aGYF3BdZ5MUz3yS+kJxwm47gmM4MmDzXn3kBZ+zhm/UaFzz74MubavEi+iokEec9etURrauHAtUjiyn3F5qZXXqnO3uyMs1C4wzgzYWKupULMWYqOG17Mm64x2d3za4TFTWRI+Yvee0EXmDqVxJyzXxvKLoJsGbjiuKaPeWXpQH7skuvvkC3PXBp2MqObnCc8lh2G2HirIVLd83V74T2WuPpLtFc5KWIzVaOVjDRGXwJ0awgak38kw4Jmm6XKEbkrN+Fe56x2Wdn+CbeXg2bZ+dq5CKjopd/6whv+9Rpgn9+obhauPzxnWaddW+kuoSiNheiLUGDoecO508+eHSdgchXAayi07ubpZZr0hbJptse7LSvnpS1rmlm9GStWt0/XY9+Ni54RFjhYuegDwwOJrQKwEMTuqYybFPRJpP+YWEXN+L4IYvtp5bw4+m1l3IarlZyErHV2DBpTyJZYUr2MLKrNykoFs+0HUaTKaI/hulC2N3FhvokuoX8bLcUFRDw73FXuqsbh8T2KG+L+a68g3z/gd546w8NWYT1abIcEWEqpKQEFsbvhFw69OYe85ihwFzFm+2Z9/F0slDpJim9VP3Qrti5V4XVJZi88SSfR/qPK5SZV0mY+gLQN6E66WuCDmBk8KrRp75sruwos4bhrdS2nLrHQFB0622oE5i8Zg295Y6M/ubsEmeZ891Vpb25ZNmtfife9tIz7Dyr4m8K/CAZNmn103I+JIly92yxk/DIHV18hoI4I8WX8M8+2ZH2+n6Nbl2joqUT7Z3TDRJWFX7tl6OEGbIEJ0hXpH+aECRur1Ae54kiFGeURL9M19Z0ayqC+Nvo02d/J/3bsNmed+8Rlkv/lJJOhGkGjaQK8bv0zbIpYmVouHSFrvO5EF223SzLp6Ww//lj9kyh4k/yaGqF+oW1gbT5wAjtT5sMKjTuYy57/IdzA0L0buITyYjaadh3V1UdY4Fvy1Hp1O7ci9Fs5P1MKFXjgBOeD2mZCFUOcGn9lMcJFAWQmU0Huq320H0rTfH6gcSiC9rlytNwG9T487ACryFUATlebamXyNrwTbzjvh3oODGIYVQ+4x9ddB+W8Ez5vwHfI4PDr7gN9w9V7Ns0EF9VrK5GKZU8Wcjyqiak0/1vJ1LdHSb++HHtRVXPLTfvJ6h4LVArEzY9Qo/rSMdgCHUHt40wa4TKE022pNVC1sYLVC6JVQZAgyHyWSVefH4QgdaN1Hlr+Fe3sAH6fgqJ/KIv90ItAyLEhyw9LBhp7Wl2R2Hr6MwutcFF4QhnjAr6ZlOwgpl3FnelXc0CO5zXWKxVS0Cht0R4EHBqQJ6kzAO+mfahGo8abJmezMSxcXvQaDXdfjStkGaAirMPAo2fhVSiOWLFSv7nExxtHUCJLVICoGlW29cbZIZOGfUUegVEaqZeS+MSrN1o9arrUq2xGce4vaFYTTlnVWdSK+3SZueBDvvsceJ60317QRh5MYvvKUUFd+AlXBr2sJklOEqRktzoWOiSz3DYhAoOc/tRsUm0hzfME8/Tvr0kplWUjUjD5mw1G4Kl/5VmdU9r4Zjz/gmrwzRUFLpajgYBXZNNAOG68CQxeQ0lYq6iujq49ROvhU/gDFAVtmiX4JNQdXqlzIwfKDM91nEn0vuV5RlUqgfEwb0hQ6CmkPBp6Q92flMUXvuMQUnTKgLoN56fSzNrSlVXvF9njyGBb/GKuWA2qk2NAspyeI5BnLdj4pO5qrDY6stxp99ombKYm5vTm2TplHr7MbyfjCtUiYo8DFm7HO/SYOiS5C3zznFEcI+16TjvwCGWOuOp3zMBklhY+9/bKHWgoY/O7NjZHzj++AS8HmicgN1kls2myuOmHJROACq5g2/vbr8HBDn4SQQyWH4jOhmKgQ4TKddiZesyoUEtnAiZTK/WI0AbytojASet70h+65s9oj5hzgyzp41Igu35f3bpYPy9RGsRbHTPlWhZ2WjVXazzVu5pmSNER/wW/3e09rzWpxLpbN7A2CayUiqkD3pSblK1Ut9/Jjwp7rMKwh9w/2ugjNmR5/ujDMweEi5siM65GNR+5m0KdOwMCel60xLuzMiqOJwncYFFXQFHiI6DxJhS9/PXD2rvXdcTYMgMjJiU6OOrHSJwOnbYYzieipXk4bC1wV1TjHJfBvND231yxVrfu21zVd33h393K1vzLEVmprnuR1oyRWt7SRRvrsiRasNchva2QyVfCzJJAn0YJPo7nD7XTd0IbxiOMjEM2P2qJ3ja2wSaKw9NpZoB9kR3kJ/YRaKghZxzxXcjvjumL4uqil6LPAJniJRgnncSIDPYzSuMn8XEmTgEBV6rZXOU2Aih9o3/yT6BPxXkFgXOkoRTS8bbeaVKTXL5cD8/aht35vuXdWHU4/rjoKL0APCi316zZg6zsUl6GL3ZXXGN0ex+RNnEVhjb+u+2hKHiVLcEiF9/KOdVsNF3U7/Fv9z67/wukJd2NMpd7cuxtqsxyse2fwg5u/XrlGt0mzTCvtyY9LjBiQkttbR4c2nZlsVXBGqXCcHsv/t5qARb/AKd45jukcJ+Cfuyn7vB9gPZ4GcqHs5RfGkmgiUXQeNq4yb9GCbZtfEiAHGlYN7o6ZFWS1jNFWwtI+irOLj755B2eYj1KgulrQtObU+CKC6Oy86+I4PSqNU4I3DTgKP9qJy6dW4RQHLogMbilfTQn0Xh+sq4zpGrsZwx8Ah6ZmcQFdaAeD1jbFrNhutdAdd8xJ9F3o6uNmMpKm0lpnahnTJVfeGmqhjv4+LphKyRSyFAMNcDm7K/C9m8X8uWXqRMmSSpZOfR4U9p6yAkb8manxvRugzH0Jpslr3xURfs31ENqJAneh26HA4DNyBA3NlUzXIBg8UdUv8EkoTO5Gu2hxW5FEJjCM4vJq3XpOIM0k4UwJ5qtp2OpAHqmUH8AKMFAdrqrUcLUtObHB9XGGc72zHJaJVoZqVnFOFJ/LLUHjL41snJhfIuVuX/5+c3WvcwYERPxZBd8nPZUWIzHqWk965tYINH51sWQzFp+s1eU1fyYiMwnZ/FFN5ka14R+/R/0j98S02fgGMoPA6LP/D1IwsZdn9p4fWJ7xftilsYwoHRMwmeNb4pD7xnwHn26iWgsS5cAqEHHLtOIa30axhF2z4me9HRg/WOSBLDLLMwJ/mwzspW3VKDNVoWaKRArBBIBvv+0rbTDteIf4nbasc4fDM+4W0sk4AdlhmxY/ZkNlJv791X8HZ/Ez08yLMAvWdoGIE9jq2Eoouvfqxe00hRt91znUL6oEELyomRy7/wBG7vPpDGD8153YWF0CTi/AjGOMcOocsejY3yxUoaXVF0YpsJI4kJBSL82RK5qu+GJtm0yx5DBdzCs4wToE/gK3BJxjq+krOBV0aXMVbFr20z07wyVgPiFBNb2zKLTTcx1X/mDRRExLnKgtEtaYkjXqtyVa6XBcokE7fJj8ARe9643plbGZRdZhNMX5Ph5N0f4ZKupcRF+0sIqOn8vZH266xsSEEqEtH22G1v7WAQsxF1VoX+A2qWhunqiGmZI4xnK/P0c0W0ZqjKecHiAsWvZECVsZNim7E9L/O39fzOOOk4LYGAR6wZeWtR6lhn6COyZ+Bg9/XnfyEFTaS7i22FPUPOO4O0ZPehkU678ExqP/3Zx1hqWX9mEtxu81b4+uwE1HDcVN5yHV4BtCviNGzqYThOaEdj744jP7WtXXi40nrPuuaF+deqa8vm2VKyxRQyo906K+Y/uhzrJPx69mLDcMofIlGGEosIBHLebQvXdcgdq9F5LVbuf0Ea7ieuD28OCwhDXnpt1cHQDah5p9PIOlh6dtPg+lv+fn5BDpUaZzaVB3xGdtbtymJsqRakdDJM+o8HekHOdzDDgX0/mmBZ3LXkzSv7bO79+7QlbNioWrRTvC3bH/ECC2SuW+1CrndSFHJ6NIb17btQQKGGhxAb3NjtNYEAzWQ4Y6xjVqRZQcZhoZrCjBvebgo49C4D3MfkVphYMWF/gQWhgR2E1jOzHQ20/jy3Hz5KtWxdEJ03dS/pCM8PX7zmVwqUKiIk76pJc0pytq9bCJZZd2Wyi/ZIfuQ5upFsCxfvjlYcyyjStkVPvlORuz915whyacUrhOnsTjFtlUR+RvnPwprjDNADPqsHGGrt96xIrBLCnx6x7esSW423z2ope+xGJqst3bgPs0RoLBS//G91R75FO5nTET15EyGy1igBVoT8wfTI9AgThw/wgSxL58ifPeRc2tcirOoDMIji1H38NqyoHlLr2czA27rSXJdaLFzElCMdLykSeum/Nen0pwUJN9zNq5INl7y1XHZteQbMt2CObceZicn6P5EKey03uO/CyEGq51s/LEbOCv5SLNcbAayvpjK36hckhq8pn1+O/Gq78VGY52006iV/k+8QLxIdIyv0f5EJcOo1w9ECplU2aDyHS/L+bq+RYYoW8NJgu9N5QvIxsUlCE3lUIkWN1G7R61HQwmIeOJ3lpfzsZd3NjfhQChIyTYebtyFKqc4+XFTxbP7sc+uKcPmXfwMbdwBSwH8kymvpYKrh+Z9yt61bZ8dbblNQAUEg8UvJc5hQt0ORjh+qUs1uRiAoFr5OBBmZ8UjZDUa+KMTZlXinU2kGKp4qKzJDCVbmuyGCSHV/ewk4AghTThOQ08SGT30Xfbt2GMgFcHeR/fIFho5ej766jDmOuca96JSLcFLPMSb86uRd8qIdwUcc+Dwl92Do5r4B5aVlFnJZDhz6GhoHPh97VSbpOLKlpHC9N16C7FugsTUNZIktzqHxYSkc3547RChSIvU7yCsR7lvf9oPDY8p8YdSOZIKTVwGDaIUNh9GNq5OfYt9qiCylRpxPKNriE9RqiTXHMWl7RCFDbRTVUqPwaRxzLc6D6ulOw4xGPfvalpChuaWGaBSwS1cUYczHmc8I9f8Vzrf6Awecdwdq8uaTqls1fJ4hKdcCQxsW2t8lcwURtp+j0JtR5T0d6kJ5Uh05GXC1JBtS1EJBxpobYFn6vqQaNfOv2yapqs0/kRY2sSVlBCpvb7b4uCmGlFf9eavRZq8g5rnNYbfRDD8R6TXS9JPrDxVCy5tScd1gALXELTeRI++7pzedpx6s6Oup0IKeS9PS7t0isWj2v1zO+tRBxb/qraJKTX1QKzgIpvBI9hfjGPsICJ2a7CibMTUgrHUyj3Uzm9FaeWJ60Jr224yY/EsPfqwQWRXKQi0WggZV6F7jj1B5fQhqIrfBxVPiglTLN6QVYa3okl/WMhCA4H5zJN72zBdAuamrlvPXAeAbH0tTG6VYsXOE8so4hiKbhvkqfKUxR+xGfpVbiTx6TTJQKHh304SWQo1fiGlp3DGKlNdUz3JU5e9X76mwpO0DgOOXn4ODP/JZ/KErXpBePCp2iap+lnlJDIP18Kz0IDV7C0QZhgVqGs81SJAyO53SOvykYRMHRz7HYYi8+CxLYdC7tw4kQdIl2xanOz+6mmXRPscbr45yA1hS8CSs5AdwFtL7HRaD/eiYuexnBvsWktLp9Vj2JqcbBNGeBPUgQwWWSrAHkomf06H7twv9HthHPTYAHvAuovdcF+taNgo6Zl1oz9zTDDGezme0iVa/1QZohslPo5wumlBVK/MWVzw1ekly5H0C8tntMT1FXFvxAQhXTCAOuWLunTC4P+aaXIOtuh/WzIECfxwxyMTPJWzTX0pTAOHGx8bUOtDNMDR3/C3sZif2E11eFAZBnVkUAYBs24vGiRfvRBvscCQldXw4wXKTht9YuTUTiOSC37vfVDilyQpNoR+2d7staJRNT18grbgXpl+VyzhgA+mzcSHwmVHf8t0QR+l5mBisMu0nNH/jkDsjHQXII8t10F7rdP31NzpEqdMknDv3S82wBZrg4MmH1eAxqoc16FHIFUWZChH6+WV2Zkhopju6gu951BzKeVCwMYSw8NjiP0lizFRwYUQutSi6MuL5pkuDegGoZ1uO5QuwjRR4eWFmuGi10KnviQn9kROr6Y7P5f2AC+QXqG/MwXDhqD0zg9/fTQGfTrFgptiK5be1ZIcBhIy23krOievG0Yjw2zSTaHdWZZzd0MvgJ3/oBWb5iTwomFkxe6cuivmPt7MkQGR3Go1rnZ3Afnjxdq/ATXWwr0Y7V8SAh2gS1ELTsDxtxeP0DZp5lm3xisypVWZRYTTSTJAh8UiTliVaPqn2eHSxZ5+1IJg5YknhTLPXfj2luVSRoCHMaI3mz6OlP8eGmhhWJdDokqlbYchjgLpC1Uv41Uvk9Q64rnkAeoN3hWpuaWn45IMwyD234SkYLTRhtgePad3LeaKbMehVpR2SYUa00dbFJmkiSBg2tbqQy2fLVJV6H5aBu8ARhzCo3dTVU467WMvyC2LFaOgdxbMY3ZFlKjxrIjHE6JPZSh5pDt5osU8u+utynSl0zZZiT35i3uu36h/76oCwLNt1Fwm5IA5gLGxajd11oGRU6AODkAknRpXbMcOXYQN6+RjjUNtu5VvupF2iDrDHBV8YIT4Ynyo7DKWLLQLX9LtN73qFCnh5XEAp1d+YVxvIu63kpqhIjtRlrxKVxMme3UJ1KbUNHwlAasMFTEKd86uoPjH3Ywh7ekHEzRttSqY/uoLrC1M1BBDqsXQoPok1nwCaxPkF6/86cnjsKQ6kbqD/f+rVOoo5GkzbShk6sPwNIZjfDAGW23hZH6TJW6+ODvO73gWyLF9aJBnlgNhx38K+I8ed9GBA65YBy79T4W60ARtDTZ/Q4taC1j/Nugd5HySs+g/UmxlVDJ/MdX20e4lR8N50n6Ipgiua4oW7D3Yh/gTbKb5/IRI36G96Qs8jHaro5G0lXwMnDKNawnEOHlMlO+RoyhT2i+8ptlvwHC14lWhwUX6gG1nYLtiv6nNgqEWzLpRRYKueyJf6Ena3dD+kEfv2Lkdz7eY9Zv78Ci8j1v9aLknAXl7EF8FW37rjnTFbpnpLB/HAr5BqA61G+2kTLay/9KIGFluRg1DDoXdO6rqsrDR8zFR1sfJKMUu1OnxiXD7lqzGtYsKdnh8bNZWe+kaTQz1igppgtxszUZ7TtNiWSnMhZ9Jj9eIxgD0D6CWTbB5r09c6rMiwtSArHHIMIGmd2Am9pGo/pfxVm1AyT9HT74wnZMqqzie9/AoEqx9IBKUPpo587NuDIrsf8fF10V2UKscJ6ab3or7jNyJbMQaRxeLcIlNomDzN0ArYC1mjct8zALp1NL7LdqZVKhwBcfUZzGG6xSA5WWdl6MjzgRJxdIrvP4/P0G5fO/Q2A7PetZX3IVS1CIOLE12eCAFAqM36gbA01uc1Nwi2vFj0skKQbxURKSF+tNovK6qmqTvCoXeTaXfcAnuCI0VCzSst8jqMA5xfmlXuYGLLVLwq9empbkgCHuUYTk3RxDoQXbRiIWrK9cPZSgP6nDIYFc0mp5ROnoNOx6HZxdMKz5ss6/vXFySSokxawG5C+69eZegygF00XUSNisS73lr5emvkvYKv/q+oSNOZWTeb0nOiS7F9T68dW2dhlHdcA7MekPIjQNLxJIYjxY8YQFvYSWNXZmUc80edVlU6fEbRaX6oQmaJX9a5CCoB8v5m48e4dIUTWLhoTs5OtTqBO78Y13FrrQFjM357RMUcQpztEWgYNeu4cziF645+59ecHDpGMIraKqJvT/e0Bezs01b1XUisgWdJiZx0qcafyxWIKDZeieeDvobIimI+/xJ2+lTWya57e1YkT7K57opvFo8iki7R2VCzt91cLSVkD0XgwQNnVrz1aBh+lOdtdIVWXVZiB35bpjgyCk+71AOiV/g1lAVGocfY9qLitA0yAUgy2sXn07DGiEI9qt6VSA9SWChNYH3DOSHCWUjv6JC0qMWlIMDFDIRtsmx3e+Bi8uVa//G3x6eUHKqaE7t7wJ0iVZgW5YXMgdUdJwrMwwexlpd4rSk8nNmUdt04t10nwcD81pkgJ3Z4kn4ibv1K5U4pFsz3sRAZdVSO/0eLM6sNP5rLGyxaxTfpHtaTI8AmaDAHJlnbJsTa3wmvF00hRintibVRh22xscRVrQ25FiSiRUKsGjfGn6o7JDRR8zdHfhvToEdN/IA7jSPcdEF7bgyw+7i5zO90XZTdqpsKjVILjI8c0IN63ZFpCbKr4u1DTkSd631SbDiH9IP2H855ytS7Ca86xm6C0v765ByNNQm/+7JKAWO+Gz05iZkIzD9VXLY9o+efaGhlIMAYsmbxN8NvxaN+skZF3h1jzw7gT6KScfAWoYju+zPQIdAWoboDZLzZdblDlQMHKDg3F42/LdEvCVA4qhuBsz0CkcnIhS/IuaGBxGGD83v/QPtPyVej0Q/+QobC5iVsFAF6J2aNd7iFWch2Ga5h5f8BT+5qfR2oixIiH5AwrLB1KwSQwvr2hyUKRl7j2E3s12KZgXqevBH3Dhu97bcmx2qja/6Z9WxKDq0GttdmwbYEOqvjWN1JsoF8I2qFGTvvgb8z6y2dmP5swP+voW9n5NL24E9Q1NGNB0nYdNULa/0ebCC+pBI6Zihm7WjH9PKZHRJZUZ89XkB8QXa1lYEki865aYqjrHKZcTrjpzVm7cgC3WY0j0LPRimP8vVU53QMVaM0whFwzjwrOQLSFcdKLXR32GS6DhGMfMoK5HfEmLrDbWAGyg0XXqwC+9F/d2q08kNszDuleHEcMhtSh9urwYusUQyBkDqLWvHMlCqj3lrUTs/dWsE6FHm5zgEvL2aS/0B2/U/zKDD2a6hYNDQnKcF2u+sZJq++ibQ/wiztHytFg4YfIaIeV+Nm7G9DKO1w/vaZFvjsupLhHYKOhW2J5G4qkSnKUEsXTAKu2hcH3BUv+d48zWEGp+vx3KZnHWysQUWp4g21V1iM93jizrDN+sg9S1QGSgUXguAuxeQpwKueteZGhNbyE75Kbtlid/KABBo6CTZbAhSw1maPohCtU5S8OML4gcSspvnOo7MImNokmE1+URYXwOv0jtkCW73CqQx3XYY62PoTzb9BVDjHDN5pV6PJ9wA2CG57rbknGvptXStjI+R8XB2vwti6ymrBbse7F9bfj7PbYlbHac7OuvdoiEX3JEk/luBVvegdbNdhB6kWS9q3EWmL6z8j5HOYHDICjhVTJoNb/ctkEyaNaIoHelO0lcZmvGv5XWLvSNlnCwBLtFx8BzlJuH4eSovGBhijaNpbI8zwDg8HF2WeF8vEwg9VVa1T9lYA9hM70JTrVkrI6eRdnHOFj7KPjDTrqFi8K3hf64C1Z71Syjko3h7L2KC2EZR7TasOyVURHBLV3RLb4FaO3sZkXP9VcPc4SVrio2LRlVH8ZQeR0IchkcXRqPyotfKAjqu6Pc3K7vqyTBJ9oWcrYJWIdkVfFBURaSWvN0GMPU2XRL0jPUcDL/JPspLFPz7xpf7YYu/kwaBCLgXfmajNuRxcdao1iZbOXniSgvql480Kpu9yEKJg8ciFq62CcmZIfpUTYpFXronctDw0burGJEQiNcjIOJLSHwS9wuhBaoqQeAwZwAD4Q0awt2s/Rff7VnEE3B6+eQQMOGcZJhqS9zByGKrIRUebODGaBTcsJTe8kt03Zefbl7SBrjQ//yuyPAzpiZDVXx9EnF6uV3e4CLtaTZ8WhnBP8bZ6CCPb3zVcHxRXy9P0rT7YQdKEt82RUvs/vFMQImwkUuTQCHDzI1YgKwEHTUuoSSDdH5pHRZaDWYaB+caPzfTjbuDBGd2wkY8uEjoaBORnHI90k70IIBJy9+U6CZrxk89/1qjNvlI24+JCVRFeODoEWKa9CLJuo8FgKVCBtLQKgutIFxLtW9+Wt8BHLr/hBPj4WyQ5X0MS+WtTUV2m/Wlm/dGHuaUi5A4FnIrHCTXpAOaV8o4yXVPjPUWMpKyMaQjjcc4nam4fef+6W5+kQjsjl/7IF/uwWDM+UXOTxKzzoJv/x0RaKhcfYSqmdQakQp8TE9Vv1t3iwwB3G99LlRpaFkjTHf8k/ITjWHj/UV7sTtBtGnFIdUWYog89k8K2i58GlUnRNSvUrJ9xfL2Rakq+3poMJMQO5idoQOWnOMa2TfZP3G9/EDoTS0T+LJjYv/P4hhMban20VK+GLrKSOGKAi/5I9XSna8jfylQMEQyAP6isOAmxNg+fVWgdNvgSjaQSPBclo5I2FT7BvkntbVgh2qCMRT4JDT472S6VjU1JWQ4EbCSEvzlX8oX7eaBE3zdZrLq/EPdjH9khfuTFGWB7UQ2dEnZQS/aBXevDKHCLnbwP1l3CeDO5eRG0m6r2kCCTVb/0WthAtqRKswpPdadUNWfEaeqIGqCvo3pLeBpEw7V5bsQPWg80rvIKm7W8wzzhW3tVUTJ4WV2zTy9YJwA7YqcuaZpIODrS+toTkH5sKONpc1s81/HbKlkqxl6Xfe7f1/AF53U964AK5ZaLbQ4Y7QTW5YKuNc6IbyjGyD8egInkspb7f8A9T1QjbgqZm/KCTbdW06kdWuO2UdnZUmoCpXiJ/SC4GOpmlHOHzn/1rNSLfhXoEVVnOAFVv42skO8TlHW07J27aZZlm1HkP3arcPuZqOqvjDaPE4EBPj1SCdfrWX9aXxZTEz0OGf/IWHgSfIH/jMLgJKDfoT6c4+G/HlIeptIQBXUIdAplEPcektjP3FeisqzzUPfM2bwHwYRhF0p4NpVwU/GJa4+gO3QHC9S18pA3xrH2vCZubQxmu5/a6H8ZtEW7qBOekCwT/C1Di2aGoIWRXRBfNmLoumHhzcku73VsrZcz2E7xhjV3uuz80Fm5yWFyQdbrZ8WSpUpaOpY5HcGWxpVoYzAfI/fC4NNor7czjFyJvbhCOmJKjMUIxQX43nnnFFm1MsSe5f4d7p5MHg0TevaYBB7gQtDyQ8AH86+hN80Bv2a6F8x+q8MCHzqG+Twzh+dRPnuDm+rahoX+5oDdn0CLxJfX0dHqCaeh8eRvvJuXIbHqD/jU7wGlmP/PFFGu5l+sloTu7J2Lak7DuBUmyjdLcsM9zdTqD++cO6MAG02JUGWLva3+jMN0bF4xmPROZuKh5iAQdxDdgX36bJQE0yYspzEOzMLHR/nPK/71S13jcbhT3NadvJM1FiPHQSuW8ODlNLk8rKQRp2jn6Ct54pwri/wMlsPnhbRXXLPPEYNe3BHoptMMc1AQPZSVgfjwR0q24UYWtbggS/BVyEzXxyuh//saZvfDnb6Wn7Jx/ArhVUWikfHVB09hZAFvJ5mtl1MidY9/PH5ZACOxAwPJ7dZx81ac+0c5j/Umc8uYJAIN5DMVTfM0Tul/wwqK6ngLMriz68n41Xb3EOYWKfUYZfu2iaQvne1WRtYLKl/M3MtHf/cKd4ilssQPEfs6THTFuFPX8uWdz3+UI0+2qgL/TdU6UJIZ6GNjEHIS0dWbAeGxp9duRT0dczQ3cHGhE+IKQX08o6Itcd7TUM4ZJ45B2uL/WrBQ7QQglVqfCW8jzzql2b9/z+qvrQlaj0TSo9RETdCBiPxMsaSTnkdgRJblorOk4NYCt1h5Gms1jVIZlCTFPKY2m5vlXAJFGAttbN9672BaD9t1SQP6HZzAOf4k6KONQ5R2eBVZDv3xEOoq1xCgLZYfbtoF76bpP+9LHBevRArI0Zpodw0wHw+Qt+dLpvZUfaZIc7h6BUMEaHGFNdBNOpmFi0pHO9DqdCdeq19vNM9lnbgEhXIG41bPDiFu+7hgeGdlEJQjD3XwkQRn1wJhU1qjDbpOsm7g2dd5rAxhj/rzOnftKKIVvrgugemmGCqvkxOPpTW8ifXktXF8iTHAnNQ1dOcG9WRlpCfdy/ua6+ur5MFIZa2NRPJ2mybwmujhvgMjYRIm2XmUtMviPZfiZmtNvjcHRUyRnJS4ZEDxQfuHWETqrAykyq1Na7jqGwF2Mwtys+zA4E2mTn3YaWLiHw+ZMqP0eCEF32HUjXmE/EA230M2tqfTmcgE7aVYBxtAg4ZjXH2x1DB1yCDxZgf7etifO0nznGmVFT8OMmDn3yomzs9QHrfOCmDB79EhSy6L+N/DcP6yIlpW5Beym1AWWIf+sr9xsGDMTbwBtxM4wUL1/e7D7o1Re5zLZ0zI4naxql6LbMAr97rySEDKhlhwl6nMl/qV90Vscr1MzmggdC0Kmb04IliCKNnuQqEXH0VVKBfQj7tBBC9dgl71JofsBIbDnAH72/kjQFJZZgkML74kIAxcu4N0j4TEkVOZLtFpumH/b4fnLQOlqQhtCGs/Wjdl/Cv2wi1VtN4699y8wFokFx5Z+CPdcMuVYcXaOSWo6iS1c712goDnmxfYZMiJrYvFw4Wtt4B9KvNSQKLJubm4ZFerrdVUgLV+oar80DrldOkpzZYT9QnrZttIQnFbYo6VWhCkQ/LtFGfFGd0eamCPztza77ujyYPtCsHtNqGbGL1dosFGi2tj1H0fdArmqvLwp9Wh94/1ZDw9L2VNdO9+aIJMFn8A4E9qpXTaDAg0CcB+suh7JtPfOBehU1BMAqtL18xBqGcpmSpOH5rKuHYgjUDr7kROu9RAoaygqoTOC0FZAtHgn07GorNBv8Eb/iwbg8NY+IqUkjnyncSZgrGmrUSd4myaBqStu8uVHrSCFbdJiS6YwYbkssircB2A5BLq+gylhr00ICSx3wLdIzMIEKq+iBA7joQ+jTvh0flVdbicaqF3lB8hH2IMU9sN4i8Sj+21TiCfK0eRpCecGkmKM8Rgrr18NfwCHJBAU2F0/6RxWwK3Mhis2CbyH5w/UIl7+UMJfYzp1HSeyMuVKd15hbCZoyjZBY3VHCGG2ZUSfe4IAIaPtT8A5Bqo+UTPmc3SfYW88A5N4bNxmWWKuBnyL0GFZ5BZ6oMH1kXvYutMtteHEP77WGxJOXtgrBLhe5ophkO0jw1/EGM/aGCBBp7Mz3bYS012ElzxyUAXs+ltzoWmeGlA64w9fkFKyzK9kH1//yyoEZp3Yxw4ERx+w9crcnm3lrmGnUyE8brZnwm7b9RZ3eC2Zg3trG1Gyiz+ONYJrEULAEVONdblDCbHpisDLb1v7fSprINOUM59t8Ph8BayncG0JNgz33UGtOrWyaalu2mrBV20iLvEgAt1cbXUezsoIRfp7G8drxcvBgjwC7nMb0FiAVAHhq/XysHiKc3R6cwU9irziBubwa029Lj0G8/2GTSjaSRJkwELND1TJBssVo2YJBo5F6ImzdPiMfvT3WZnGsvPIFmKLHONlqxGxcZvndPjsX3KOTxu1/ftrgB0RTyOJGy1EYaqCpwl5kFMH1zdhSerquSVLeNz+ZQXW1pjUhrH5NsnvLnuDxmfUFuOzI62y6VNW6+L4MEIdeDsfsqF0IShUeVz8KBcciJKB8Z/H3i5MbTo7lxM9PlFkwVBclL9rizIa3hrjHjNq7EhSqjjjzovuFkZ1H9MIH8EUL8pKPUxngCvG0r0pr4k4RB1hBI/6YL1XKx1C1z2DOTXVOUZQun3Ee4llHxvji1TxkinFuLIjBkHGvYJrvjbsfUia3Th4F12dyx1MDJ2XCEAV8MhKFrb5GV099zJJoa7UeZNZo8rcXpHn9pxeYng1qhSmn9leIkzUm//UENJbzff+bdDGX43QXFjaHTlDn/56oHh3g9ZGxY/cWKYKFVQ6g7Tz5j80pUaOGt0LeXch7qOtcHtqvpSL5839togDPx+egVJc41+qD9v0DLX7cwklJRqfYX0s7rbDlgVcziUARYsGaQPiBiT1H3Y35LI7M5FtuEm5vL3lWBjJvp0r1cCt+5DvLhX1Z53z/PKCzVOzV/G7LlP/A0bHBZsNaqO5U6MdVWSdHDCh4osIQntWxHH5Js0tT2o0ijvom4icMZc5UYi1HbjHMqjEq29+RaWpxVD4LR0X21uM7a4008E8sodaJ0GIwCp47vEF6/i7rsDv2JcKmMJypz5zz7sLeEzPxdSO5zDZsK7Gfi2TDymRqGVCg8ptz8OErtPZZayvbL6qk+fTZvBFV8R1Cq1reB9wh7dje0jQYdr8/i3s33rRWbNaQqAcGKLfAwY8ExCMjirBN9+XteUF1P0Et2RuplT4LjCE6IPUex/UPX7sqIh8ViN2MxcLKJOdDM6Y2oHXjbMlG3ZkBUtAuIm+iDee8nbnlO4tCS7qUvpvzEuaW2EawQAVsk9RTvPWbWgayz0F8BT3h6XI+yp7MzJgwcVou100RyQQJe/4x2Za5jF/PNDGN4SjRWuG4Z6fzeorxRSJmgVWWvzNNYFxVQHR1Clex78gCcgugxMrCun/zYoOweeRz40jHjywBaZLLpVvd483C4ZbPxRkFyazOSAuz8BU6d/Rz6besW7GFrHB6oN7GfCRitEmAnSOSYhW+LJWolLaiS3Ag+vy1gypLLj7UhFKjtH7VPt+xyqXNbqa5Rhy9jAlTyGhZG9BAsD7TEFNjRV+7/VW09JA9kRbhcobfB/S1A3mYsQEjEIRhfYIKub5hUZGy6TYwkX7H2kjO4XHHv5jZtCYgYWhNuVBfr7Miy0oG88VyjI5mBRqXuyhG9ssl4YW8zUPOL6BhFHq5rcOb4ocBixCILvAHPt09jj/7bxGCQYm8tsuboVDyG2RSdb+sAKrXCJRi+a73XKSy8PZPEQMhyf5ZqBcafm2PPtV42zd3oka+wzjD9QYaIjoUOeVkq7AY3mZ7nW1ideFHIn2ZVb7uCfM5XqRUTzB0YhVg16tch8x3uWmmAAIBA+Im5yFhx2BpVM9+r/ls7c2ys2f09X7yOjnf6kL38kBMHFhR5rIZEFrIeYkeCYFVZ3egTkaNhJTr9QJzppnvhg3nxhp/WTq/OfulzC51xaRK9b163Ax/Lp3ppYyLNPwBg4Uy3zecQEAQiqteS1ZXvlgjHMoC71Rj3p7179PPNcsIxc3MG/jI9jehX3f1/j87rwBBp4easedJnZvb35m4Dk8yrpViC5JTUI0fwpy7d5qfd1VbR7cOiRGqG0BGWIp0avGAZRmTTFuf4xHYc0vj5kBGZWE3v/l/JHXnKAp7WopviWbdRmU5HAjxZAsDp3/Sf7fOvhLVVIv5k8BckKyaY/t2r2jFeNClkp4m88BX21W1vkJQrs/LLvWGXY8l0aXQLHJt8pHcYnMU1x+9rsOPD2zDVH9+MQnwIYQGciarBhicretUfPYDwfirdrYMmqvEifoDs1qMi/Apg5ELSmNLRwQhJejtVgds9Gsf0MqnvwkIxBesgzXqXAsib5KydRzjO8U77w9Zpu0RlqJQrEcysJXhILa2XnZyTl5d+mhIKNrtNc4D6F+ZQ8AGDqtKiQ9+1g2bCkryNRG3irBLnI5XdUcr/KU1x4IztdyBuH+DGckw+YgvMuNw0gAzXgG3FB2y+OrcoA5uA5u6n8p2aZASm64NEfkdiDT8lmX7jVLVtsx/JrcJWQ/cLYjEnhMFJbC4VHgceUGElK9gv84cjFM6bSR5NtkXp/0tYnsnesLkap37B2tU2Je6a0nm8VfU6N18AuAlbE4t8Wf51QmUdCtcJhfmDaFj2W3MlAPD4z7VPPvM3+9eakmgM707t99yIsGa+b+AJKD3HUsleUky7SoF4q11Myj7wNTt1yul8iyMhpkexevCBzmXwBmLFYgPmXR41VMCYp7+ve/j0VEnPUb/Lt+3fmql9TcVKogpu9QbuKep/Z7bZwBOywHzyJdeV5OWCTmmRO+89vLygzUPkzxJS27V5GTRkDpyCaftb5WPZzTdd9w5c4PFj+tZEyQ+J0GdcVlKwJFSYwurbHir7fLUd37BbO0ceDsRT/BUDd5ariOBlpp4bTUQG3vIDJFmkGzPZU8t+qB7KGZWYO/5P0wUwQ5IgvV/jUrzbCGGGpFJa4DjCsCat6y54L2TjGIk+qjykJ82HOWsWToResn/HUFVZBfRufQ0IfoLgiPSOLryGJUDZ1LTadPHGaGoC4LZllXVDdPvn/1hwSaV63ez06YXWNwtJbEo54Zf6W4H5NNTVO4GnIBS+ihC4zdEx1/xuk7KwmTcuMs0gWCCBx/GTLrmZ3Y1K6ckz1+zNZhxFOHi2xfiSExA9q3pwbr+tSntPT9UjyRnYGMIkpKReZdB8pf/ogA2fXOAxkSQzXf8jhs++iQcdVri6A8Nsr8sI6hatDdhCTak4gJ+li4X9SMUyGPEsigPbGEWkamF5yaN0+t8sQcaUvYz+ndZWkkL3k6Q7VbHaY4AiHTPvsWA9ZCZdWjmWLu2Mv8aicnFXqG0MQwaZ/NGeJTn6SUao2Mx+fveTaSmoUx/9WcTUoUBcg3bobAqs7T3QEjMpGnnzLRm2wsp1EQyVAUZ0BU7egSOS+rRw29LcKf6ugzBSc7w9pxB4v/tm8DqLYvJkj3wQtaHiyEtjn5u7WnM3q2qibPQkdBEJ7dXoudQxNNjMqbfHzSTGZpeqXs7DoX7z+CB9M8PsBx9+m2UjjIhhrq4MLrq2X97O66Hhk/4XMHk9S80dRHF4tI+WerxYuhRa/ekqC9BurDUja8hO065q9ks6XQXMzBsfZ6hYaUkUtIYSzEBZslvR1qX+N/S/pJROhqZ/A1BbYiL5vmMk98+bAwAhB90n2G6M5Hhe3lpamiBotwd6qCLMDFMKtljrVs+yNi0BW+JtoLkWHPC8CIfpg4k4cTgySUch1NAiztXe6m0YB/7897LaMTJZv5pkYcoAs5TsHGXVbbpwBZvF2PGiWDvlMTFkXL/zsAQ0WNpJoSAmrw0fQ0Nb0PTLREpTfDerna2Y2JWM7b5Rr/CqJ4+NbZ/0fkwwND5ErzvZCUdM5V66jmnL0iGRHMGVSmaPzzrg8RUNJw+gHBOXQ9THee+t2oeqU/G9dtok6+TI/mvXPOwCl7vf1HtFdv1uZpQ76OEoLSxlTH819SjJG2nEo9KcRc7Jaiip2lFD7Nk5iky2aWd9ph0Ab43pZPdyyboND/TTrunssD68jVHSs2ful+KLMO3DL6jFOlzMOBiGwTch8wvplas2xjbryk4C1FvaVx3yTHecCyj3R4hi0xhOfU+l9mbJlY3WkF/TkiLI6YQeYWMCmVualXG4Lsi07ISCn0FA4ef63XKlcr/VeW2rfO+iq9ZuihnE5fzQATPH6e1p8dFRbj2/alt0EEx8Q7rD75h88/UBZLQ1KXbHvqWBIwccI4AQ4vTauzJ+2JlJjLxwx8nE/oGjcWY7dx5bfs+L+9mY/89Wvw546VpOhPkYbzVVRxkk8W+EnpW4csyc+P0r0ErlwgD+ZFlahxSZ26r0qwaUrs0DRY5LXiQ4ajvEwQ+aQdyGRQnVv79XPY7H1OXz0Qkf7MeZnlCTHbQ/acuob8PSwTHC82lv2v5ra8m2dSwbXD1ZA3bQK/w88D8rAaPPC3O+FU8UqJCUsALtW2mZAtHgwJheWOXM+UhRG9lxQ+f+wpiKpSO3B62Q5S9ip59/tNiFe4Nxl576GRie0sajfEC5N8YVypaqYp0RDT5ZbYoPvUMGl6WMPGlAy5mvhsfdmBumknX83GTw/j1VDqSPYLeBOZ50WJoOjW/deBHPWSVdH7Bt9Ile04AkqI+EICKqgVfw4aOT/CWXOoFpnPoozlC5dYifcn4kHdWHWUX82Fq4PsgAcJrENhJ6Vkl5lChO1WmGrVoBGCqgmPCon75wC6oP276gRkrWn5yMtk2AwRKzC8k3chul8yQ5B2vR0GABU80FYpxVlbo/E9mQW6wLU4MoXN07uIvjyY00sARx0872ePMuAVhGutwojV57CxhZWM8cawCf1Xchx5aw0+2xU/g7FtugVL4Y6aHspXp3qEOXw4ygl9FZhbW7Qnxm/5B6JgVxspd9gTqLIRdjvUmcALiSpxXn7WYQBGzv9hZo5WFU3e5J2xCxwfflxgllmmxRkpxrifjKU+gma/QeMsLRLcdD49i2herh4DuM7hyqaY2vkmimJz7NR3UVHX8GinDw2ycxSJo27bzm5tGlRvZ0lKcleeMHOg/hbrhpG7K0MztuJvxQaVaTH5uWtmG+Ij4HQaeJ1OIGihhTyB6AKlJv0OqDExykpxjdSLXLHewUV/lc3gAk/gKB5JDtuPTVI286bc2/28r7McYglbhOOH+wXA94ypT55fAehdtoVHfYG6E=" />


<script src="/Integrations/JQuery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/jquery-ui-1.11.4.min.js" type="text/javascript"></script>
<script src="/Integrations/Centralpoint/Resources/Controls/Page.js?v8.5.1" type="text/javascript"></script>
<script src="/Integrations/JQuery/Plugins/jquery.cp_Accordion.js?update=8.4.38" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yEk_YMl82vYnFaa5FD7acAOVmHN1OVAHW0odKyQQu9Q1bz3qnQXM6wMMgHgoP86fVuoAAaSWCN1in80BXnYa73tm4GfJTRouHqe9VVz8IN8t--lDrBHPNEDRfOXwvvzil1ZZgTlodaqVfkaxC99bGRQ1&amp;t=ffffffffd5bd3df4" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="43343198" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="5dJMiwx01HbF5+JgxGPn0OB/zXducA0jM3KxfkaIzP1fZnMOZZJwNcvujkXLFDmmL6GsdPTOCeo6GDhhjeX9jWaQCBGrBU0bdXr8TEMlB2KBfH9m2vTpqJZ0W3zvlTp47m8qQU6RuVNWdf7Ptv+KmBEd59J1tLgvJJWxYvOGG+M=" />
	
	<script> 
$(document).ready(function(){
    $("#discover-expand").click(function(){
        $("#discover-expanded-container").slideToggle("slow");
    });
    $("#search-expand").click(function(){
        $("#search-expanded-container").slideToggle("slow");
    });
    $("#mobile-navigation-expand").click(function(){
        $("#mobile-navigation-container").slideToggle("slow");
    });
    $("#mobile-search-expand").click(function(){
       $("#search-expanded-container" ).insertAfter( "#mobile-navigation-container" );  
        $("#search-expanded-container").slideToggle("slow");
    });

});
</script>

<style>
</style>
<header>
<!-------Top Nav------>
<nav class="top">
<div class="inner-container" id="blue-navigaiton-bar">
<ul>
    <li><a href="/Main/mclaren.aspx">About Us</a></li>
    <li id="health-care-professionals"><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li id="appointment-button"><a href="/main/patient-appointment.aspx" target="_blank" style="background: #0066a4;">Book an Appointment</a></li>
    
    
    <li><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a id="discover-expand" class="discover-mclaren-health-button" href="#">Discover McLaren</a></li>
    <li><img style="margin-left:13px; margin-top:13px;" src="/Uploads/Public/Images/Designs/nav-search-button.png" id="search-expand" alt="Search our website"></li>
</ul>
<div class="clear"></div>
</div>
<!------------Search Box-------------->
<div id="search-expanded-container" style="display:none;">
<div class="inner-container">
<div class="Search-Input">
<label for="HtmlSearchCriteria" hidden="hidden">Search Criteria</label>
<input style="" onkeypress="if (event.keyCode == '13') { document.location.href='/Main/Search.aspx?search=' + this.value; return false; }" placeholder="I'm looking for..." name="HtmlSearchCriteria" id="HtmlSearchCriteria" group="" type="text"></div>
<div class="Search-Button">
<label for="HtmlSearchGo" hidden="hidden">Search Now Button</label>
<input style="vertical-align: middle;" onkeypress="document.location.href='/Main/Search.aspx?search=' + document.forms[0].HtmlSearchCriteria.value;return false;" onclick="document.location.href='/Main/Search.aspx?search=' + document.forms[0].HtmlSearchCriteria.value;return false;" name="HtmlSearchGo" id="HtmlSearchGo" value="Go" src="/Uploads/Public/Images/Designs/nav-search-button.png" alt="Search Our Site" type="image"></div>
</div>
</div>
<!------------Search Box-------------->
<!------------Discover Box-------------->
<div id="discover-expanded-container" style="display:none;">
<div class="inner-container">
<ul class="discover-list">
    <li class="header">McLaren Services</li>
    <li><a href="/Main/bariatric-services.aspx">Bariatrics</a></li>
    <li><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li><a href="/Main/imaging-services.aspx">Diagnostic Imaging</a></li>
    <li><a href="/Main/birth-center-services.aspx">Family Birth Place</a></li>
    <li><a href="/mclaren-homecare-group/mclarenhomecaregroup.aspx">Home Care</a></li>
    <li><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></li>
    <li><a href="/mclaren-homecare-group/laboratory-services.aspx">Lab</a></li>
    <li><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li>
    <li><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></li>
    <li><a href="/Main/robotic-services.aspx">Robotic Surgery</a></li>
    <li><a href="/Main/surgery-services.aspx">Surgical Services</a></li>
    <li><a href="/Main/womens-services.aspx">Women’s Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">Primary Care &amp; Specialists</li>
    <li><a href="/Main/locations.aspx?taxonomy=Cardiology7">Cardiology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=FamilyMedicine4">Family Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=InternalMedicine8">Internal Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=MedicalCenters">Medical Centers</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Pediatrics11">Pediatrics</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Surgeons">Surgeons</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=UrgentCare9">Urgent Care</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Urology10">Urology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=WomensServices2">Women's Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">McLaren Hospitals</li>
    <li><a href="/BayRegion/BayRegion.aspx">McLaren Bay Region</a></li>
    <li><a href="/bayspecialcare/BaySpecialCare.aspx">McLaren Bay Special Care</a></li>
    <li><a href="/CentralMichigan/CentralMichigan.aspx">McLaren Central Michigan</a></li>
    <li><a href="/Flint/Flint.aspx">McLaren Flint</a></li>
    <li><a href="/Lansing/Lansing.aspx">McLaren Greater Lansing</a></li>
    <li><a href="/LapeerRegion/LapeerRegion.aspx">McLaren Lapeer Region</a></li>
    <li><a href="/macomb/macomb.aspx">McLaren Macomb</a></li>
    <li><a href="/northernmichigan/northernmichigan.aspx">McLaren Northern Michigan</a></li>
    <li><a href="/Oakland/Oakland.aspx">McLaren Oakland</a></li>
    <li><a href="/lansing/orthopedic-services.aspx">McLaren Orthopedic Hospital</a></li>
    <li><a href="/porthuron/porthuron.aspx">McLaren Port Huron</a></li>
</ul>
<ul class="discover-list" style="margin-right:0px;">
    <li class="header">McLaren Difference</li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a href="/mhp/mhp.aspx">Health Advantage</a></li>
    <li><a href="http://www.karmanos.org" target="_blank">Karmanos Cancer Institute</a></li>
    <li><a href="/Clarkston/Clarkston.aspx">McLaren Clarkston</a></li>
    <li><a href="/Main/Home.aspx">McLaren Health Care</a></li>
    <li><a href="/mhp/mhp.aspx">McLaren Health Plan</a></li>
    <li><a href="/aco/homepage.aspx">McLaren High Performance Network, LLC</a></li>
    <li><a href="/mclaren-homecare-group/mclarenhomecaregroup.aspx">McLaren Homecare Group</a></li>
    <li><a href="/mclaren-homecare-group/laboratory-services.aspx">McLaren Medical Laboratory</a></li>
    <li><a href="/mclaren-homecare-group/pharmacy-services.aspx">McLaren Pharmacy</a></li>
    <li><a href="/mclaren-physician-partners/mpp.aspx">McLaren Physician Partners</a></li>
    <li><a href="/Main/foundation.aspx">Our Foundations</a></li>
</ul>
<br style="clear:both;">
</div>
</div>
<!------------Discover Box-------------->
</nav>
<!-------Top Nav------>
<!-------Logo Row------>
<div class="inner-container">
<div class="logo">
<a href="/main/home.aspx"><img src="/Uploads/Public/Images/header.jpg" alt="company logo links to website"/></a>
</div>
<div class="mobile-accordion-icons"><img style="margin-right:8px;" src="/Uploads/Public/Images/Designs/mobile-search-button.jpg" id="mobile-search-expand" alt="search button"><img style="" src="/Uploads/Public/Images/Designs/mobile-nav-button.jpg" id="mobile-navigation-expand" alt="mobile navigation expand button"></div>
<div class="second-nav">
<ul>
    <table cellpadding="0" cellspacing="0" border="0" role="presentation">
	<tr>
		<td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation73&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation73&#39;).hide();" onfocus="$(&#39;#cpPortableNavigation73&#39;).show();" onblur="$(&#39;#cpPortableNavigation73&#39;).hide();" id="cpPortableNavigation76a224fb-2841-4c3b-b9bc-237b847fccbe">
			<li id="services-nav-item" class="second-nav-services"><a href="#" onclick="return false;">Services</a></li>
		</div><div id="cpPortableNavigation73" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;width:600px;overflow:hidden;">
			<style>
    .featured-service-alpha .grey {color: #cccccc !important; font-weight:normal !important;}
    .featured-service-links{height:250px; width:750px;}
    .featured-service-links a{font-size:13px; color:#5f6062; padding:0px; margin:0px; font-family:'Alegreya Sans', sans-serif; text-decoration:none;}
    .featured-service-links a:hover{text-decoration:underline;}
    .featured-service-links p{margin:0px; padding:0px; line-height:20px;}
    .featured-service-alpha a:hover{text-decoration:underline;}
    .all-services-p{display:block !important;}
</style>
<div class="services-drop-down">
<div class="services-left-column">
<h5 style="padding-left:31px;">Featured Services</h5>
<!----Services of Excellence DataSource----->
<ul>
    <li class="services-of-excellence"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></li>
    <li class="services-of-excellence"><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li class="services-of-excellence"><a href="/Main/cardiac-care.aspx">Cardiology Services</a></li>
    <li class="services-of-excellence"><a href="/Main/emergency-services.aspx">Emergency Services</a></li>
    <li class="services-of-excellence"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></li>
    <li class="services-of-excellence"><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></li>
    <li class="services-of-excellence"><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></li>
    <li class="services-of-excellence"><a href="/Main/stroke-services.aspx">Stroke Services</a></li>
    <li class="services-of-excellence"><a href="/Main/trauma-services.aspx">Trauma Services</a></li>
    <li class="services-of-excellence"><a href="/Main/womens-services.aspx">Women's Care</a></li></ul>
<!---img style="" src="/Uploads/Public/Images/Designs/Main/services-drop-down-image.png"--->
</div>
<div class="services-right-column">
<h5>Our Services</h5>
<!----------------------->
<div class="featured-service-links">
<div class="featured-services-all">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/athletic-medicine.aspx">Athletic Medicine Institute</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/heart-failure-clinic.aspx">Heart Failure Clinic</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/locations.aspx?taxonomy=podiatry6">Podiatry Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/home-care.aspx">Home Care</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/locations.aspx?taxonomy=westbranch2">Primary Care West Branch</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/pulmonary-services.aspx">Pulmonary Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/birth-center-services.aspx">Birthing Center</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/imaging-services.aspx">Imaging Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/rehabilitation-services.aspx">Rehabilitation & Physical Therapy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/blood-services.aspx">Blood Conservation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infection-prevention.aspx">Infectious Prevention </a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/breast-care.aspx">Breast Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/infusion-services.aspx">Infusion</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/senior-services.aspx">Senior Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cancer-care.aspx">Cancer Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/laboratory-services.aspx">Laboratory</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/sleep-services.aspx">Sleep Center</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cardiac-care.aspx">Cardiology Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/oakland/lake-orion-campus.aspx">Lake Orion Nursing and Rehabilitation</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/spine-services.aspx">Spine, Neck and Back</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/northernmichigan/cheboygan-campus.aspx">Cheboygan Outpatient Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/marwood/homepage.aspx">Marwood Nursing and Rehabilitation</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/stroke-services.aspx">Stroke Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/locations.aspx?taxonomy=dermatology7">Dermatology</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/bayspecialcare/bayspecialcare.aspx">McLaren Bay Special Care</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/surgery-services.aspx">Surgery Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/home-care-medical-supplies-mhg.aspx">Medical Supplies & Equipment</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/tavr.aspx">TAVR</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/dialysis-services.aspx">Dialysis</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/trauma-services.aspx">Trauma Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/emergency-services.aspx">Emergency Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urgent-care.aspx">Urgent Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ems-services.aspx">EMS</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urology-services.aspx">Urology</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/locations.aspx?taxonomy=endoscopy2">Endoscopy</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/oakland/oxford-campus.aspx">Oxford Campus</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/vascular-services.aspx">Vascular Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ear-nose-throat-services.aspx">ENT: Ear Nose and Throat</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pain-center-services.aspx">Pain Center</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/vein.aspx">Vein Center</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/eye-care.aspx">Eye Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pediatric-services.aspx">Pediatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/womens-services.aspx">Women's Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/main/foundation.aspx">Foundation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/plastic.aspx">Plastic Surgery</a></p></div></td>
		<td style="vertical-align:top;" width="34%"></td>
	</tr>
</table>
</div>
<p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/mhg-home-care.aspx">Home Care</a></p><p><a href="/Main/mhg-home-infusion.aspx">Home Infusion</a></p><p><a href="/Main/mhg-hospice.aspx">Hospice</a></p><p><a href="/Main/mhg-lab.aspx">Lab</a></p><p><a href="/Main/mclaren-bay-special-care.aspx">McLaren Bay Special Care</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/mhg-pharmacy.aspx">Pharmacy</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p>
</div>
<!------------------------------>
</div>
<br style="clear:both;">
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigationc1fdecc8-73a4-478e-b7f9-da0884005b89">
			<li class="find-a-physician"><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation76&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation76&#39;).hide();" onfocus="$(&#39;#cpPortableNavigation76&#39;).show();" onblur="$(&#39;#cpPortableNavigation76&#39;).hide();" id="cpPortableNavigation4f92167d-700e-4912-a762-f01a0b4e052d">
			<li class="patient-and-visitor-info"><a href="/Main/patient-and-visitor.aspx">Patient &amp; Visitor Info</a></li>
		</div><div id="cpPortableNavigation76" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/patient.aspx" target="_self">Patient Information</a><ul><li class="featured-service-style"><a href="/main/patient-opt-out.aspx" target="_self">Fundraising Opt-Out</a></li><li class="featured-service-style"><a href="/main/patient-mymclaren.aspx" target="_self">MyMcLaren Chart</a></li><li class="featured-service-style"><a href="/main/patient-notice-privacy.aspx" target="_self">Notice of Privacy Practices</a></li><li class="featured-service-style"><a href="/main/patient-donation.aspx" target="_blank">Online Donation</a></li><li class="featured-service-style"><a href="/main/patient-pre-registration.aspx" target="_self">Patient Pre-Registration</a></li><li class="featured-service-style"><a href="/main/patient-appointment.aspx" target="_self">Schedule Appointments</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/financial.aspx" target="_self">Financial Information</a><ul><li class="featured-service-style"><a href="/main/financial-billing1.aspx" target="_self"> Pay Your Bill Online</a></li><li class="featured-service-style"><a href="/main/financial-services.aspx" target="_self">Financial Assistance</a></li><li class="featured-service-style"><a href="/main/financial-insurance.aspx" target="_self">Insurance Information</a></li><li class="featured-service-style"><a href="/main/financial-participating-plans.aspx" target="_self">Participating Health Plans</a></li><li class="featured-service-style"><a href="/main/financial-insurance-veterans.aspx" target="_self">Veterans Choice Insurance</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/visitor.aspx" target="_self">Visitor Information </a><ul></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation77&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation77&#39;).hide();" onfocus="$(&#39;#cpPortableNavigation77&#39;).show();" onblur="$(&#39;#cpPortableNavigation77&#39;).hide();" id="cpPortableNavigationbf4fa1a8-3c75-4ea8-97c5-67f9504e6f30">
			<li class="health-and-wellness"><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
		</div><div id="cpPortableNavigation77" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-your-health.aspx" target="_self">Your Health</a><ul><li class="featured-service-style"><a href="/main/healht-clinical-trials.aspx" target="_self">Clinical Trials</a></li><li class="featured-service-style"><a href="/main/health-community-needs.aspx" target="_self">Community Health Needs Assessment</a></li><li class="featured-service-style"><a href="/main/health-information.aspx" target="_self">Health Information</a></li><li class="featured-service-style"><a href="/main/health-navigator.aspx" target="_self">Health Navigator</a></li><li class="featured-service-style"><a href="/main/health-infection-prevention.aspx" target="_self">Infection Prevention</a></li><li class="featured-service-style"><a href="/main/health-my-checkups.aspx" target="_self">My Checkups Health Tool</a></li><li class="featured-service-style"><a href="/main/health-mymclaren-chart.aspx" target="_self">My McLaren Chart</a></li><li class="featured-service-style"><a href="/main/health-patient-education.aspx" target="_self">Patient Education</a></li><li class="featured-service-style"><a href="/main/health-wellness-tools.aspx" target="_self">Wellness Tools</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-classes-programs.aspx" target="_self">Classes & Programs</a><ul><li class="featured-service-style"><a href="/main/health-classes.aspx" target="_self">Classes and Events</a></li><li class="featured-service-style"><a href="/main/health-support-groups.aspx" target="_self">Support Groups</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-mclarenhealthplan.aspx" target="_self">McLaren Health Plan</a><ul></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigation52587aa1-b090-4537-9c98-40261a490ab3">
			<li class="our-facilities"><a href="/Main/locations.aspx">Our Facilities</a></li>
		</div></td>
	</tr>
</table>
</ul>
</div>
<div style="clear:both;"></div>
</div>
<!-------Logo Row------>
</header>
<div id="mobile-navigation-container" style="display:none;">
<ul class="mobile-menu">
    <li>
    <div class="mobile-navigation-accordion">Services</div>
    <div class="mobile-navigation-content">
    <p><a href="/Main/athletic-medicine.aspx">Athletic Medicine Institute</a></p><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/birth-center-services.aspx">Birthing Center</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology Services</a></p><p><a href="/northernmichigan/cheboygan-campus.aspx">Cheboygan Outpatient Services</a></p><p><a href="/Main/locations.aspx?taxonomy=dermatology7">Dermatology</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Services</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/locations.aspx?taxonomy=endoscopy2">Endoscopy</a></p><p><a href="/Main/ear-nose-throat-services.aspx">ENT: Ear Nose and Throat</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/heart-failure-clinic.aspx">Heart Failure Clinic</a></p><p><a href="/mclaren-homecare-group/home-care.aspx">Home Care</a></p><p><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging Services</a></p><p><a href="/Main/infection-prevention.aspx">Infectious Prevention </a></p><p><a href="/mclaren-homecare-group/infusion-services.aspx">Infusion</a></p><p><a href="/mclaren-homecare-group/laboratory-services.aspx">Laboratory</a></p><p><a href="/oakland/lake-orion-campus.aspx">Lake Orion Nursing and Rehabilitation</a></p><p><a href="/marwood/homepage.aspx">Marwood Nursing and Rehabilitation</a></p><p><a href="/bayspecialcare/bayspecialcare.aspx">McLaren Bay Special Care</a></p><p><a href="/mclaren-homecare-group/home-care-medical-supplies-mhg.aspx">Medical Supplies & Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></p><p><a href="/oakland/oxford-campus.aspx">Oxford Campus</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatric Services</a></p><p><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/plastic.aspx">Plastic Surgery</a></p><p><a href="/Main/locations.aspx?taxonomy=podiatry6">Podiatry Services</a></p><p><a href="/Main/locations.aspx?taxonomy=westbranch2">Primary Care West Branch</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary Services</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehabilitation & Physical Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep Center</a></p><p><a href="/Main/spine-services.aspx">Spine, Neck and Back</a></p><p><a href="/Main/stroke-services.aspx">Stroke Services</a></p><p><a href="/Main/surgery-services.aspx">Surgery Services</a></p><p><a href="/Main/tavr.aspx">TAVR</a></p><p><a href="/Main/trauma-services.aspx">Trauma Services</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/vascular-services.aspx">Vascular Services</a></p><p><a href="/Main/vein.aspx">Vein Center</a></p><p><a href="/Main/womens-services.aspx">Women's Care</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p>
    </div>
    </li>
    <li><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
    <li>
    <div class="mobile-navigation-accordion">Patient &amp; Visitor Info</div>
    <div class="mobile-navigation-content">
    <p><a href="/Main/patient.aspx">Patient Information</a></p>
    <p><a href="/Main/Financial.aspx">Financial Information</a></p>
    <p class="mobile-navigation-link-visitor"><a href="/Main/Visitor.aspx">Visitor Information</a></p>
    </div>
    </li>
    <li><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
    <li><a href="/Main/locations.aspx">Our Facilities</a></li>
    <li><a href="/Main/foundation.aspx">Ways To Give</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren.aspx">About Us</a></li>
    <li id="health-care-professionals-mobile" class="top-nav-mobile"><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li class="top-nav-mobile" ><a href="/main/patient-appointment.aspx">Book an Appointment</a></li>
    
    <li class="top-nav-mobile"><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li class="top-nav-mobile"><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li class="top-nav-mobile"><a href="/Main/career.aspx">Careers</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
</ul>
</div>
	
	<div class="cpweb_PerimeterMiddle">
		<table id="blPerimiter" class="cpsys_Block" width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
	<tr>
		
			
			<td id="tdPerimeterCenter" class="cpsys_BlockColumn"><div id="divWrapper" class="cpweb_Wrapper">
	<div id="cphBody_divTop" class="cpsty_Top">
		
		
		<div id="cphBody_divTopAc2" class="cpsty_SiteTypes_Default_TopAc2"><div id="cpsys_Advertisers_af343aee-a245-4e1d-9da4-6d08bbbd30c5" style="text-align:left;">
	<div class="breadcrumb-trail"><div class="Breadcrumb"><span><a href="/main/home.aspx" target="_self">Home</a> &gt; </span><span><span>Website Privacy Policy</span></span></div></div>
</div></div>
	</div>
	<div style="clear:both;">
		<table id="cphBody_blSiteType" class="cpsys_Block cpsty_blSiteType" width="100%" border="0" cellpadding="0" cellspacing="0" role="presentation">
			<tr>
				
			
			
			<td id="cphBody_tdCenter" class="cpsys_BlockColumn cpsty_CenterTd" width="99%">
				<div id="cphBody_divCenter" class="cpsty_Center">
					<div id="cphBody_divCenterAc1" class="cpsty_SiteTypes_Default_CenterAc1"><div id="cpsys_Advertisers_bf3115e5-4271-4bad-91ed-a4aa140e412a" style="text-align:left;">
	<style>
    #data-source-created{display:none;}
    #service-line-datasource{display:none;}
</style>
<div class="left-rail-nav-container">
<ul parentsystemtype="FirstTierList" id="site-nav"><li class="liParent"><a href="/main/mclaren.aspx" target="_self" class="d173dbee-de0f-4132-ba5e-76a09f076348">About Us</a><ul><li class="liParent"><a href="/main/mclaren-awards.aspx" target="_self" class="64b1e7ee-84b2-4587-afe5-5d74ca900868">Award Winning Care</a></li><li class="liParent"><a href="/main/mclaren-community-assessment.aspx" target="_self" class="f0856316-de0f-442c-8207-78ea6ee5b5ce">Community Health Needs Assessment</a></li><li class="liParent"><a href="/main/mclaren-contact-us.aspx" target="_blank" class="25423667-c62c-4a08-89f5-0296d2ca22f8">Contact Us</a></li><li class="liParent"><a href="/main/mclaren-history.aspx" target="_self" class="6c6781bf-645e-4190-9b15-b1df37f62521">History</a><ul><li class="liParent"><a href="/main/mclaren-br-history.aspx" target="_self" class="bff09b68-e652-41dd-8b45-a963dda7c15d">McLaren Bay Region</a></li><li class="liParent"><a href="/main/mclaren-cm-history.aspx" target="_self" class="023cada0-e4d5-4d9b-9da1-81c3b5ca94b6">McLaren Central Michigan</a></li><li class="liParent"><a href="/main/mclaren-clarkston.aspx" target="_self" class="eb43ae8e-0657-4790-80c0-4f97ab08bd54">McLaren Clarkston</a></li><li class="liParent"><a href="/main/mclaren-flint-history.aspx" target="_self" class="d8639099-ce6a-4ead-9139-a59962d5d3db">McLaren Flint History</a></li><li class="liParent"><a href="/main/mclaren-gl-history.aspx" target="_self" class="1a368f0f-f80d-41d5-9f49-d8227cb5b09d">McLaren Greater Lansing</a></li><li class="liParent"><a href="/main/mclaren-hg.aspx" target="_self" class="9177539c-3814-4aa1-b948-718e538bed6b">McLaren Homecare Group</a></li><li class="liParent"><a href="/main/mclaren-lr-hisotry.aspx" target="_self" class="592f9725-eeb8-403c-b5cd-40d24c6ff7fa">McLaren Lapeer Region</a></li><li class="liParent"><a href="/main/mclaren-macomb-history.aspx" target="_self" class="787adc33-1be5-4654-a1e1-b00f7748edb4">McLaren Macomb</a></li><li class="liParent"><a href="/main/mclaren-nm-history.aspx" target="_self" class="f8821150-597e-48dc-b092-7853def059fb">McLaren Northern Michigan</a></li><li class="liParent"><a href="/main/mclaren-oakland.aspx" target="_self" class="1f97c3e8-205c-45f2-899f-cdd5dcabed17">McLaren Oakland</a></li></ul></li><li class="liParent"><a href="/main/mclaren-blog.aspx" target="_self" class="79a09704-9964-497f-b21c-29a720645b6f">Blog</a></li><li class="liParent"><a href="/main/mclaren-executives.aspx" target="_self" class="84efe6f0-cf94-4b18-9e0b-9ce8374387c1">Meet the Executive Team</a></li><li class="liParent"><a href="/main/mclaren-message.aspx" target="_self" class="d97677db-b368-4c2d-b08a-a3c7a2a3a306">Message to Our Community</a></li><li class="liParent"><a href="/main/mclaren-mission.aspx" target="_self" class="212a2d08-6f76-49f5-b5f0-9b2ad17f88d8">Mission Statement</a></li><li class="liParent"><a href="/main/mclaren-publications.aspx" target="_self" class="a424848b-5c16-49fe-ba74-05c565299104">Publications and Videos</a></li><li class="liParent"><a href="/main/mclaren-research.aspx" target="_self" class="8c52cd2f-a2bd-40fe-bff6-a7fd50955fff">Research Studies</a></li><li class="liParent"><a href="/main/mclaren-service-area.aspx" target="_self" class="df08959d-e863-4e26-a7b3-e13754ca9049">Service Area  </a></li><li class="liParent"><a href="/main/mclaren-community-health-centers.aspx" target="_self" class="a0e1b844-af54-40f5-9e8d-9bcecc9eefec">Community Health Centers</a></li></ul></li></ul>
</div>
</div></div>
					
					<div id="cphBody_divContent" class="cpsty_SiteTypes_Default_Content">
	

	
			
<!--cpsys_Template:cpsys_Register-->
<!--cpsys_Template:cpsys_Register-->


<!--cpsys_Template:DetailsHeaderContent-->
<main id="inside-page"><div class="page-content mod-details" id="top">
<h1 class="nav-display-name">
Website Privacy Policy
</h1>
<!--cpsys_Template:DetailsHeaderContent-->


		<div class="left-rail-nav-container" id="service-line-datasource">

</div>
<div class="main-content">
<!---Shows Location Info--->

<style> 
/*----Styles For SubPages-----*/
.cpsty_CenterTd{width:100% !important;}.cpsty_Center h1{margin-top:0px;}.site-banner .inner-container{min-height:115px;}.site-banner .inner-container h1{line-height:115px; font-size:34px;}.site-banner .inner-container h1.multi-line{line-height:normal !important; font-size:34px !important; height:100px !important;}.site-banner{height:173px; background-color:#2f82b6; background-image:none;} .site-banner .inner-container .site-banner-logo{display:none} .service-location-on-landing-page-top{display:none;} .service-location-on-landing-page{display:none;} .left-rail-nav-container{display:block; width:24.5% !important; margin-right:2%; float:left; border:solid 1px #cccccc;} .main-content{width:72.5%; float:left;} @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}
.cpsty_LeftTd {display: none;}
    h1.nav-display-name {display: none;}
/*----Styles For SubPages-----*/
</style>

<h1 class="pageheader">Website Privacy Policy</h1>
<div class="service-location-on-landing-page-top"></div>
<p>McLaren Health Care Corporation cares about privacy issues and wants you to be familiar with how we collect, use and disclose Personally Identifiable Information you provide to us.</p>
<p>This Privacy Policy and Terms (the “Policy”) describes our practices in connection with Personally Identifiable Information that we collect through our website located at www.mclaren.org, the websites of our subsidiaries that display or directly link to this Policy, and the e-mail messages that we send to you that directly link to this Policy (collectively, the “Services”).</p>
<p>There are a number of circumstances where this Policy does not apply and we want to identify a few of them for you:</p>
<ul>
    <li>This Policy does not apply to the information you share when receiving treatment from your medical provider, including when you are receiving medical services from us. For example, this Policy would not apply to the information you share with us when admitted to the emergency room. The use and disclosure of the information you provide in such circumstances is governed by the Federal Health Insurance Portability and Accountability Act of 1996, more commonly known as HIPAA, as well as Michigan law. You can learn more about your HIPAA rights and protections and our obligations in our Notice of Privacy Practices.</li>
    <li>This Policy does not apply to the information you share and your use of the <em>My McLaren Chart</em>, which is the patient portal we offer. The use and disclosure of the information you provide when using the <em>My McLaren Chart</em> is governed by HIPAA, Michigan state law, as well as the terms and conditions and other policies you agreed to when registering to access the <em>My McLaren Chart</em> service.</li>
    <li>This Policy does not apply to the practices of companies that we do not own or control. Other services and organizations that we link to may have their own privacy policies governing the use of information you provide to them. For example, organizations such as Facebook, Twitter and LinkedIn each have their own privacy policy and practices.</li>
    <li>This Privacy Policy does not apply to individuals whom we do not employ, including any of the third parties to whom we may disclose user information as set forth in this Privacy Policy.</li>
</ul>
<p>If you have any questions or comments about this Policy or our privacy practices, please contact us by using one of the means listed on our <a href="/main/mclaren-contact-us.aspx">Contact page</a></p>
<ol>
    <li><strong>Personally Identifiable Information We May Collect</strong>
    <p>“Personally Identifiable Information” is information that identifies you as an individual, such as name, birth date, telephone number, e-mail address, or unique device identifiers that was gathered in connection with your use of the Services.</p>
    <p>If we combine Personally Identifiable Information with protected health information subject to protection under HIPAA, the combined information will be treated as protected health information for as long as it remains combined.</p>
    </li>
    <li><strong>Other Information we may collect</strong>
    <p>“Other Information” is any information that does not reveal your specific identity or does not directly relate to an individual, including, for example: (1) computer or device connection information, such as browser type and version, operating system type and version, device information, and other technical identifiers; (2) information collected through cookies and other technologies; or (3) aggregated information, such as usage history and search history.</p>
    <p>If we combine Other Information with Personally Identifiable Information, the combined information will be treated as Personally Identifiable Information for as long as it remains combined.</p>
    </li>
    <li><strong>Security Measures</strong>
    <p><strong>Virus Detection and E-mail Security:</strong>&nbsp;For website security, we use software programs to monitor traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage.&nbsp; </p>
    <p><strong>General Practices: </strong>Although we seek to use reasonable measures to protect Personally Identifiable Information, please be aware that no security measures are perfect or impenetrable. Therefore, we cannot and do not guarantee that the information you provide to us through the Services&nbsp;will not be viewed by unauthorized persons. We are not responsible for circumvention of any privacy settings or security measures contained on the Services.</p>
    <p><strong>Security of Service Communications</strong>: Some communication through the Services may be sent through the standard HTTP protocol and may be delivered using regular e-mail. Information sent over HTTP is not encrypted. E-mail, while convenient, also poses several risks (e.g., e-mail is not a secure form of communication, is unreliable, can be forwarded, etc.). We cannot guarantee the security of the information sent through such means, nor can we guarantee that information you supply to us will not be intercepted while being transmitted to us. It is important for you to protect against unauthorized access to your computer and to take appropriate security measures to protect your information. We use encryption technology, such as Secure Sockets Layer (SSL), to protect your protected health information during data transport with <em>My McLaren Chart</em>.</p>
    <p><strong>Your Obligations to Safeguard your Information</strong>: Security is not a one person job. You must also take reasonable measures to protect your information, including, for example, securing your computer and mobile device, using an antivirus software, using a firewall, and other similar safeguards.</p>
    </li>
    <li><strong>Disclaimer</strong>
    <p>McLaren Health Care Corporation and its subsidiaries offer information on the Services for general educational purposes only. This information should not be used for diagnosis and treatment, nor should it be considered a replacement for counsel with your physician or other health care professional. If you have questions or concerns about your health, contact your health care provider. We encourage you to use the Physician Finder to locate a physician by specialty and area.</p>
    <p>While we and our subsidiaries make reasonable effort to ensure accuracy of the information on the Services, we do not guarantee the accuracy, and the information is provided with no warranty or guarantee of any kind.</p>
    </li>
    <li><strong>Links</strong>
    <p>We and our subsidiaries provide links to other websites. These links are provided as a convenience to you and as an additional avenue of access to the information contained on such third-party websites. Different terms and conditions may apply to your use of any linked sites. We encourage you to read the privacy policy of each website.&nbsp; We have no control over third party websites and make no claim or representation regarding such websites. We accept no responsibility for, the quality, content, nature, or reliability of any websites accessible by hyperlink from the Services, or websites linking to the Services.&nbsp; We are not responsible for any losses, damages or other liabilities incurred as a result of your use of any linked sites.</p>
    </li>
    <li><strong>Corrections, unsubscribe and data retention</strong>
    <p>You may update the information you provide to us through the Services. To change your information, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>. If you are subscribed to one of our newsletter, you may unsubscribe from receiving the newsletters by clicking on the appropriate link at the bottom of the e-mail. We may, from time to time, send you administrative messages. You cannot opt-out from receiving administrative messages.</p>
    <p>We will retain your information for as long as needed to provide you services, comply with our legal obligations, resolve disputes, and enforce our agreements. Except for authorized law enforcement investigations, other valid legal processes or as described in this Policy (or another policy in place between you and us), we will not share any Personally Identifiable Information we receive from you with any parties outside of McLaren and its subsidiaries.</p>
    </li>
    <li><strong>Changes to the Privacy Policy and Terms</strong>
    <p>We may change this Policy from time to time. Please take a look at the “Last Modified” legend at the top of this page to see when this Policy was last revised. Any material changes to this Policy will become effective 7 days from when we post the revised Policy on the Services and all other changes to this Policy will become effective when we post the revised Policy on the Services. Your use of the Services following the effective date means that you accept the revised Policy. If you have questions or comments about this Policy, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>.</p>
    <p><a href="/main/foundation.aspx"><strong>McLaren Foundations:</strong></a>&nbsp; Some subsidiaries uses Internet donors' technology provided by Blackbaud, Inc. This is encrypted technology and allows financial donations to be forwarded electronically using a secure server.</p>
    <p>McLaren Health Care Corporation and its subsidiaries use oxcyon.com as a host server and vendor, the content management system is Central Point.</p>
    </li>
    <li><strong>Use of Services by Minors</strong>
    <p>We do not knowingly collect Personally Identifiable Information from individuals under the age of 13 and the Services are not directed to individuals under the age of 13. We request that these individuals not to use the Services.</p>
    </li>
    <li><strong>International Visitors</strong>
    <p>The Services are controlled and operated from the United States, and are not intended to subject us to the laws or jurisdiction of any state, country or territory other than that of the United States. If any material on the Services, is contrary to the laws of the place where you are when you access them, then we ask you not to use the Services. You are responsible for informing yourself of the laws of your jurisdiction and complying with them. By using the Services, you consent to the transfer of information to the United States, which may have different data protection rules than those of your country.</p>
    </li>
    <li><strong>Use of Materials on the Services, Trademarks and Copyrights </strong>
    <p>You acknowledge and agree that all content on the Services (including, without limitation, text, images, user interfaces, visual interfaces, graphics, trademarks, logos, sounds, source code and computer code, including but not limited to the design, structure, selection, coordination, expression, ‘look and feel’ and arrangement thereof) is the exclusive property of and owned by us or our licensors and is protected by copyright, trademark, trade dress and various other intellectual property rights and unfair competition laws. These marks and copyrights may not be copied, imitated, or used, in whole or in part, without the express prior written permission from their respective owners, and then with the proper acknowledgments. Nothing on the Services shall be construed as granting, by implication, estoppel, or otherwise, any license or right to use any trademark, logo or service mark displayed on the Services without the owner’s prior written permission, except as otherwise described herein.</p>
    <p>You may access, copy, download, and print the material (such as, for example, educational materials, service descriptions, and similar materials) purposely made available by us for downloading from the non-secured component of the Website (“General Website”) for your personal, non-commercial use and for your business use in connection with evaluating McLaren healthcare provider services for offer by your health plan or use by your employees, provided you do not (i) modify or delete (including through selectively copying or printing material) any copyright, trademark, or other proprietary notice that appears on the material, and (ii) make any additional representations or warranties relating to such materials.</p>
    <p>You may access, copy, and print the material contained in the secured component of the Website in accordance with the terms and conditions governing the secure section.</p>
    <p>Any other use of content or material on the Services, including but not limited to the modification, distribution, transmission, performance, broadcast, publication, uploading, licensing, reverse engineering, encoding, transfer or sale of, or the creation of derivative works from, any material, information, software, documentation, products or services obtained from the Services, or use of the Services is expressly prohibited.</p>
    <p>We, our licensors, or content providers, retain full and complete title to any and all materials provided on the Services, including any and all associated intellectual property rights. </p>
    <p>As long as you comply with this Policy, we grant you a personal, non-exclusive, non-transferable, revocable, limited privilege to enter and use the Services. We reserve the right, without notice and in our sole discretion, to terminate your license to use the Services and to block or prevent future access to and use of the Services.</p>
    </li>
    <li><strong>Submissions and Postings</strong>
    <p>To the extent that we allow submissions on the Services, you acknowledge that you are responsible for any material you may submit via the Services, including the copyright, legality, reliability, appropriateness, and originality of any such material.</p>
    <p>You represent and warrant (and we rely on your representation and warranty) that you (i) own or otherwise control all the rights or have sufficient rights to the content you post or that such items are known to you to be in the public domain; (ii) that the content is accurate; (iii) that use of the content you supply does not violate any provision in this Policy or terms you may have agreed to with a third party; (iv) that the content is not defamatory or otherwise trade libelous; (v) does not violate any law, statute, ordinance or regulation; and (vi) that you will indemnify us for all claims resulting from content you supply, including arising from an action alleging infringement of copyright or other proprietary rights in such work.</p>
    <p>We undertake no duty to determine the validity of any claim of copyright or trademark infringement. Upon receiving written notice that any item posted on the Services is believed to infringe a copyright or other proprietary right, we will remove said work.</p>
    <p>If you do submit material, you grant us and our affiliates an unrestricted, nonexclusive, royalty-free, perpetual, irrevocable, transferable and fully sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute and display any and all material not subject to protections under HIPAA throughout the world in any media. You further agree that we are free to use without limitation and without any compensation to you any ideas, concepts, or know-how that you or individuals acting on your behalf provide to us. You grant us the right to use the name you submit in connection with such material.&nbsp; We retain any and all rights granted in this Policy in and to any user submitted content or non-HIPAA materials after termination, notwithstanding the reason for any such termination.</p>
    <p>We have an absolute right to remove any material from the Services in our sole discretion at any time.</p>
    </li>
    <li><strong>Usage Rules</strong>
    <p>You hereby agree to not upload, distribute, or otherwise publish through the Services any content that (i) is unlawful, libelous, defamatory, obscene, pornographic, harassing, threatening, invasive of privacy or publicity rights, fraudulent, defamatory, abusive, inflammatory, or otherwise objectionable; (ii) is confidential, proprietary, incorrect, or infringing on intellectual property rights; (iii) may constitute or encourage a criminal offense, violate the rights of any party or otherwise give rise to liability or violate any law; or (iv) may contain software viruses, chain letters, mass mailings, or any form of “spam.”&nbsp; You may not use a false email address or other identifying information, impersonate any person or entity or otherwise mislead as to the origin of any content.&nbsp; You may not upload commercial content onto our Services.</p>
    <p>You expressly agree to refrain from doing either personally or through an agent, any of the following:&nbsp; (1) use any device or other means to harvest information; (2) transmit, install, upload or otherwise transfer any virus or other item or process to the Services that in any way affects the use, enjoyment or service of the Services, or any visitor’s computer or other medium used to access the Services; (3) engage in any action which we determine in our sole discretion is detrimental to the use and enjoyment of the Services; or (4) transmit, install, upload, post or otherwise transfer any information in violation of the laws of the United States. You may further not use any hardware or software intended to damage or interfere with the proper working of the Services or to surreptitiously intercept any system, data, or personal information from the Services. You agree not to interrupt or attempt to interrupt the operation of the Services in any way.</p>
    </li>
    <li><strong>Infringement Notice</strong>
    <p>We respect the intellectual property rights of others and request that you do the same. If you believe your copyright or the copyright of a person on whose behalf you are authorized to act has been infringed, you may notify us in writing to the e-mail address or mailing address provided in the “How to Contact the Web Team” section below with attention to Copyright Agent. To be effective, your notification must be in writing, include your contact information, provided to our copyright agent, and include:&nbsp; (i) signature of a person authorized to act; (ii) identification of the copyrighted work claimed to have been infringed; and (ii) identification of the material that is claimed to be infringing including references to the location of the material on the Services.</p>
    <p>If you believe other intellectual property rights were violated, you may notify us in writing to the mailing address provided in the “How to Contact the Web Team” section below with attention to General Counsel.</p>
    </li>
    <li><strong>Jurisdiction and Applicable Law</strong>
    <p><strong>The laws of the State of Michigan, </strong>without regard to any conflicts of laws principles thereof,<strong> will govern the construction and interpretation of this Policy and the rights of the parties hereunder. By accessing, using, or registering for the Services, you acknowledge that you have read, understood, and agreed to be bound by this Policy and by all applicable laws and regulations. </strong>The Parties agree on behalf of themselves and any person claiming by or through them that the exclusive jurisdiction and venue for any action or proceeding arising out of or relating to this Agreement will be an appropriate state or federal court located in Michigan and each party irrevocably waives, to the fullest extent allowed by applicable law, the defense of an inconvenient forum.<strong></strong></p>
    </li>
    <li><strong>Severability and Waiver</strong>
    <p>Our failure to exercise or enforce any right or provision of this Policy will not constitute a waiver of such right or provision. If any provision of this Policy is unlawful, void, or unenforceable, for any reason, the remaining provisions will remain in full force and effect to the fullest extent of the law.<strong></strong></p>
    </li>
    <li><strong>How to Contact the Web Team</strong> E-mail:&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong> Fax:&nbsp;(989) 891-8185</li>
</ol>
<ul>
    <li><strong>We respect your privacy and do not collect Personally Identifiable Information through the Services unless you choose to provide it. We, and our service providers, may collect Personally Identifiable Information in a variety of ways, including when</strong>:
    <ul>
        <li><a>You contact us to request information by sending us an </a><a href="mailto:Webmaster@mclaren.org">e-mail</a>&nbsp;; </li>
        <li>You participate in one of our training or education events, such as a childbirth education, nutrition education or exercise &nbsp;training seminars;</li>
        <li>You request one of our publications or newsletters;</li>
        <li>You participate in a Service related survey, contest, or other promotion; or</li>
        <li>You complete a questionnaire(s) on the Services (<em>e.g.</em>, did you find our website helpful?)</li>
    </ul>
    </li>
    <li><strong>We may use personally identifiable information</strong>:
    <ul>
        <li>To send administrative information, such as information regarding the Services and changes to our terms, conditions, or policies;</li>
        <li>To respond to your inquiries and fulfill your requests;</li>
        <li>If you enter into a contest or similar promotion we may use the information you provide to administer those programs;</li>
        <li>We may use survey information for research and quality improvement purposes, including helping us to improve information and services offered through the Services;</li>
        <li>For our business purposes, such as improving or modifying our Services, identifying usage trends, and operating and expanding our service and information offerings;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>Your Personally Identifiable Information may be disclosed</strong>:
    <ul>
        <li>To identify you to anyone to whom you send messages through the Services;</li>
        <li>To our third-party service providers that provide services such as website hosting, information technology and related infrastructure, customer service, and other similar services;</li>
        <li>To a third-party in the event of any reorganization, merger, sale, joint venture, assignment, transfer or other disposition of all or any portion of our business or assets;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>California Do Not Track Notice</strong>:
    <ul>
        <li>We do not track you over time and across third party websites to provide targeted advertising and therefore do not respond to Do Not Track (DNT) signals;</li>
        <li>Third parties that have content embedded on our website, such as a social networking connectors (<em>e.g.</em>, Facebook, Twitter) set cookies in your browser as well as obtain information about the fact that a web browser visited the Services from a certain IP address.</li>
    </ul>
    </li>
</ul>
<ul>
    <li><strong>How We May Collect Other Information</strong>: We, and our third-party service providers, may collect Other Information in a variety of ways, including:
    <ul>
        <li><em>Through your browser or mobile device</em>: Certain information is automatically collected by most browsers or through your mobile device, such as your computer type, screen resolution, operating system name and version, device manufacturer and model, language, and Internet browser type and version. We may also collect information on the search terms you used to find our website, the search engine you used, or the address of the web site from which you came to visit.</li>
        <li><em>Using cookies</em>: Our Services may use cookies and other technologies such as pixel tags and web beacons. These technologies help us provide better services to you, tell us which parts of our Services people have visited, and allow us to better measure the usability of our Services. We treat information collected by cookies and other technologies as non-personal information. You have a variety of tools to control cookies and similar technologies, including controls in your browser to block and delete cookies.</li>
        <li><em>IP Address</em>:&nbsp; Your “IP Address” is a number that is automatically assigned to the computer that you are using by your Internet service provider (ISP). An IP Address may be identified and logged automatically in our server log files, or those of our website hosting vendor, whenever a user accesses the Services, along with the time of the visit and the page(s) that were visited.</li>
        <li><em>By aggregating information</em>:&nbsp; Aggregated Personally Identifiable Information does not personally identify you or any other user of the Services. We may aggregate information for a variety of reasons, for example, to calculate the percentage of our users who visit a particular page or clicked on a particular item from a newsletter.</li>
        <li><em>Google Analytics</em>: We use a service from Google called “Google Analytics” to collect information about use of the Services. Google Analytics collects information such as how often users visit this site, what pages they visit when they do so, and what other sites they used prior to coming to this site. Google Analytics collects only the IP address assigned to you on the date you visit this site, rather than your name or other identifying information. Google’s ability to use and share information collected by Google Analytics about your visits to our Services is restricted by the Google Analytics Terms of Use and the Google Privacy Policy. We may use the information from Google Analytics for a variety of reasons including trend analysis and to make our Services more useful to the communities we serve.</li>
    </ul>
    </li>
    <li><strong>How We May Use and Disclose Other Information</strong>
    <ul>
        <li>We may use and disclose Other Information for any purpose, except where we are required to do otherwise under applicable law.</li>
        <li>If we are required to treat Other Information, such as IP addresses or other similar identifiers, as Personally Identifiable Information under applicable law, then we may use it as described in “How We May Collect Other Information” section above, as well as for all the purposes for which we use and disclose Personally Identifiable Information, but we will treat these identifiers as Personally Identifiable Information. If we are require to treat Other Information as protected health information as defined by HIPAA, then we will treat the identifiers in accordance with our Notice of Privacy Practices.</li>
    </ul>
    </li>
</ul>
<p><strong>Mail:</strong><strong><br>
</strong>McLaren Bay Region<br>
Corporate WebMaster<br>
1900 Columbus Avenue<br>
Bay City, MI 48708</p>
Last modified:  5/2/16
<!-----Related Links------>


<div class="subpage-info">
<!----Related Photo Gallery---->
<!----Related Documents---->
<!----Related Videos------->
<!----Related Links-------->
<!----Related Blogs-------->
</div>
</div>
	

<!--cpsys_Template:DetailsFooterContent-->

<!--cpsys_Template:DetailsFooterContent-->


		
	



</div>
					
				</div>
			</td>
			<td id="cphBody_tdRight" class="cpsys_BlockColumn cpsty_RightTd">
				<div class="cpsty_Right">
					
					
					
				</div>
			</td>
		
			</tr>
		</table>
	</div>
	<div id="cphBody_divBottom" class="cpsty_Bottom">
		
		
		
	</div>
</div></td>
			
		
	</tr>
</table>
    </div>
    
	<footer>
<div class="footer-social-outer-container">
<div class="footer-social-inner-container">
<ul>
    <li><a href="https://www.facebook.com/McLarenHealth"><img style="" alt="facebook icon" src="/Uploads/Public/Images/Designs/footer-facebook.gif"></a></li>
    <li><a href="https://twitter.com/mclarenhealth"><img style="" alt="twitter icon" src="/Uploads/Public/Images/Designs/footer-twitter.gif"></a></li>
    <li><a href="https://www.linkedin.com/company/mclaren-health-care"><img style="" alt="linkedin icon" src="/Uploads/Public/Images/Designs/footer-linkedin.gif"></a></li>
    <li><a href="https://www.youtube.com/user/mclarenhealth"><img style="" alt="youtube icon" src="/Uploads/Public/Images/Designs/footer-social-red.gif"></a></li>
    
    
</ul>
</div>
</div>
<div class="outer-container">
<div class="inner-container mobile-hide">
<nav class="btm">
<div class="col-1">
<ul>
    <li><a href="/Main/Career.aspx">Careers</a></li>
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a href="/Main/mclaren-web-privacy-policy.aspx">Website Privacy Policy</a></li>
    <li><a href="/Main/health-community-needs.aspx">Community Health Needs Assessment</a></li>
    <li><a href="/Main/Sitemap.aspx">Site Map</a></li>
</ul>
</div>
<div class="col-2"><img src="/Uploads/Public/Images/Designs/FooterLogos/footerlogowhite_McLarenHealthCare.png" alt="Company logo links to website"><br>
<br>
McLaren Health Care
<br>

<br>
Grand Blanc
,
MI
48439
<br>
<span style="font-size: 30px;">

</span></div>
<div class="col-3">
<ul>
    <li><a href="/Main/employee.aspx">For Employees</a></li>
    <li><a href="/Main/media1.aspx">For Media</a></li>
    <li><a href="/Main/physician.aspx">For Physicians</a></li>
    <li><a href="/Main/vendor-registration.aspx">For Vendors</a></li>
    <li><a href="/Main/volunteer.aspx">For Volunteers</a></li>
    <li><a href="/Main/gme.aspx">For Medical Education</a></li>
    <!------------- main, bayregion, macomb, oakland, flint, lansing ---------GME Link-------->
</ul>
</div>
</nav>
</div>
<div class="btm-ribbon">
<div class="inner-container">
<p>McLaren Health Care, through its subsidiaries, will be the best value in health care as defined by quality outcomes and cost.</p>
<p>©All rights reserved.  McLaren Health Care and/or its related entity</p>
<div style="clear:both;"></div>
</div>
</div>
</div>
</footer>
<script>
$(document).ready(function() {
	// Browser Less Than 1000px
	if ($(window).width() < 1000) {
		$('.cpsty_SiteTypes_Default_LeftNav').insertAfter('.cpsty_Center');
                $('.cpsty_SiteTypes_Default_LeftAc2').insertAfter('.cpsty_SiteTypes_Default_LeftNav');
		$('.module-results-intro-paragraph').insertBefore('.details-view-left-column');
		$('.inner-container ul:first-of-type').click(function(){
			$('.inner-container > ul:first-child > li > ul').slideToggle('slow');
			$('#search-expanded-container').insertAfter('#mobile-navigation-container');
		});
	}
});
</script>
    
	<div class="dv-bottom-edit-liks">
	
	
	
	
	
	</div>
	<div id="uprgUpdateProgress" style="display:none;">
					<table border="0" cellpadding="0" cellspacing="0" class="updateProgress" style="position:fixed; top:0px; right:0px; border: solid 1px #CCCCCC; background-color:#F2F2F2;">
				<tr>
				<th style="vertical-align: middle; padding: 2px;"><img src="/Integrations/Centralpoint/Resources/ProgressIcon.gif" alt="Loading..." /></th>
				<th style="vertical-align: middle; padding: 2px;">Loading...</th>
				</tr>
				</table>
			
</div>
    <input type="hidden" name="ctl00$ctl00$FormAction" id="FormAction" value="0" /><input type="hidden" name="ctl00$ctl00$FormGroup" id="FormGroup" /><input type="hidden" name="ctl00$ctl00$FormButton" id="FormButton" />

<script type="text/javascript">
//<![CDATA[

 //Admin > Properties: HeaderStartupScripts 
$('table#DynamicNavigation1 img[src="/Uploads/Public/Images/arrow.png"]').attr('alt', 'arrow');

 //End of Admin > Properties: HeaderStartupScripts 
$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show().siblings().show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('#cpPortableNavigation73').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation73').unbind('mouseleave');
});
$('#cpPortableNavigation73 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation76').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation76').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation76').unbind('mouseleave');
});
$('#cpPortableNavigation76 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation76').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation77').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation77').unbind('mouseleave');
});
$('#cpPortableNavigation77 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
});

                    $(document).ready(function() {
                    $('.mobile-navigation-accordion').cp_Accordion({collapsable : true, active : -1, headerClass : 'mobile-navigation-accordion', contentClass : 'mobile-navigation-content', event : 'click', expandedSpanText : '', collapsedSpanText : '', expandCollapsePosition : 'left', expandedHeaderText : '', collapsedHeaderText : '' });
                    });
					if ($('input[name="HtmlSearchCriteria"]').length > 0) {
						$('input[name="HtmlSearchCriteria"]').autocomplete({
							source: function(request, response) {
								$.ajax({
									type: "POST",
									url: "/WebServices/ClientMethods.asmx/SiteSearchAutoComplete_v2",
									data: "{ \"term\": \"" + request.term + "\", \"lookupType\": \"StartsWith\"}",
									dataType: "json",
									contentType: "application/json; charset=utf-8",
									success: function(data) { response(data.d) },
									error: function(XMLHttpRequest, textStatus, errorThrown) { /*alert(textStatus + '\n' + errorThrown);*/ }
								});
							},
							select: function( event, ui ) {
								$('input[name="HtmlSearchCriteria"]').val(ui.item.value);
								$('input[name="HtmlSearchCriteria"]').siblings('input[type="button"], input[type="submit"]').click();
							},
							autoFocus: true,
							delay: 10,
							minLength: 2
						});
					}
					//]]>
</script>
</form>
</body>
</html>
