

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html id="html" xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head id="head"><meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link id="lnkSiteType" rel="stylesheet" type="text/css" href="/SiteTypes/Default.master.css.aspx?aud=Main&amp;rol=Public" /><title>
	Website Privacy Policy | McLaren Health Care
</title>
<!--Admin > Properties: HeaderHtml-->
<link type="text/css" href="/Uploads/Public/Documents/dropdownlinks.css" rel="stylesheet" />
<link rel="shortcut icon" href="/Resource.ashx?sn=favicon" />
<link href="/Resource.ashx?sn=slider" rel="stylesheet" type="text/css" />
<!--End of Admin > Properties: HeaderHtml-->
<!--Design > Styles (#ReDesign): HeaderHtml-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400,500,700,800,900,500italic,400italic,100italic,900italic,300italic,700italic,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">
<link rel="icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->

<!--[if IE 7]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE7-STYLES.css?v=2">
<![endif]-->

<!--[if IE 8]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE8-STYLES.css?v=2">
<![endif]-->

<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<!--End of Design > Styles: HeaderHtml-->
<!-- Site Architecture > Audiences (McLaren Health Care): HeaderHtml-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6396952-25', 'auto');
  ga('send', 'pageview');

</script>
  <script type="text/javascript">
    var _monsido = _monsido || [];
    _monsido.push(['_setDomainToken', 'VW4sxwq_5Jph8JUIFq9uyQ']);
  </script>
  <script src="//cdn.monsido.com/tool/javascripts/monsido.js"></script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link type="text/css" href="/Uploads/Public/Documents/mhc_custom.css" rel="stylesheet" />
<!--End of Site Architecture > Audiences-->
<meta name="robots" content="NOARCHIVE, NOFOLLOW, NOINDEX, NOODP, NOSNIPPET, NOYDIR, NONE" />
<meta name="rating" content="GENERAL" />
<meta name="revisit-after" content="30 days" />
<link rel="canonical" href="http://www.mclaren.org/main/mclaren-web-privacy-policy.aspx" />
<link href="/Integrations/JQuery/Themes/Stable/Root/jquery-ui.css?v8.6.6" rel="stylesheet" type="text/css" />
<style type="text/css">
/* Site Architecture > Audiences (McLaren Health Care): HeaderStyles */ 
.mobile-navigation-link-visitor{display:none;}
/* End of Site Architecture > Audiences: HeaderStyles */

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
/* Admin > Properties: HeaderStyles */ 
a.dropdownlinks:link{font-size:12px ! important;}
a.dropdownlinks:visited{font-size:12px ! important;}
a.dropdownlinks:hover{font-size:12px ! important;}
a.dropdownlinks:active{font-size:12px ! important;}
/* End of Admin > Properties: HeaderStyles */
.mobile-navigation-accordion { cursor:pointer; }
.mobile-navigation-accordion span.expanded { padding-left:17px; }
.mobile-navigation-accordion span.collapsed { padding-left:17px; }
.mobile-navigation-accordion span.expanded { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-expand.png') no-repeat center left; }
.mobile-navigation-accordion span.collapsed { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-closed.png') no-repeat center left; }
    @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
.CpButton { cursor:pointer; border:outset 1px #CCCCCC; background:#999999; color:#463E3F; font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 10px; font-weight:bold; padding: 1px 2px; background:url(/Integrations/Centralpoint/Resources/Controls/CpButtonBackground.gif) repeat-x left top; }
.CpButtonHover { border:outset 1px #000000; }
</style></head>
<body id="body" style="font-size:100%;">
    <form method="post" action="/Main/mclaren-web-privacy-policy.aspx" id="frmMaster">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="04pOfjESIC0l58b8fcphG5Wgk3ME96Bo8KbGHWcQmUtmS/DdV0fx+Pc1DssVA5jGohWVsr1vvEbm/YXusbS/N6cKK4+UaNKEbJavET4MP5hCI2B3i3JUjE0fFqzwauiCMRbi+BboKafCrR31xhEvCeRMnyU/hKq2b9/EjnnOnKwfzqtaCMJ61xlbw4A61XCPnuV4H/Q1gpahecJnlfUc8QoV/MRVFqFOyoG6jCrn6fpDNW3an9nZ132qawjYl7ZhHCC1Ec+dhMKtjRf9GH8KwmNbf67UKi+/rPERRwDmSm3xEqxm17P9OiCi41HMxScbsauOvV3D23Mmot0sFkXCQitAAmdzBprHCjU8+qnzNK1yqfkk+P7SPQ1qihM2XnJm8+Ck76mtUlITGfiX7+82w8v7JyESAiBQnMHYm6o39bvMbjpfCbklbJLLgoLYtPVJhCVbydGX2aoGy20bhwvf4izMZJefY/WfheNjtNjIDaL5QjCcz0ZuDJNyhghHPMFeGeKCJ8Hn9PCIekw3BMZY9QBZ+gRJibi47QviFGeuZtFmKNYrGJV5foEMFDkZxrq6/B9ML4kkfNDz7rnK52TTVA2YoKTEY5mEDiJ4/s0gmlCwgLgFlA0JHtAibOaRR4e2p1JqXO5dvfX8M3nuItcjXmAmmAEONfSoeGefy0OZVCWkFjv3nhKWlyRv8G725N014a7/4X+KtLwpjSEPFN/T53DoGcJEEmmLnfAiSBIGr18kno3VBwyIsKKf+pB/Fr6M0pxR8JAr01152qkUuQEi9gz3iWiUTLEqBCeWriGayxdrgCEtOwuUF2NTt0b08+mBBh+ZJzLE/Ax+xZ8o8WCjpq5BB4X4qxV+nC0yvdGhUr/1MzcVovm0JNN+GpQ6W6dAAq7brgyV07zI8ZM2TVQFoPFg5+gXMmRg8JWCzrPMmlZ1JuZCd/Gko37eIKIIvSnxOb2q32GKdAEPI612ns3TGTjnZ9mAx9h9XPwKhJE7slSmpjYM06xvJ9jFLLoZjkgXdVHHZxAuu/gxCqMN38FCFjVdH/hySUYZganHHDNBB/iYrmQujFjJqOhpKtneyTEhAVItWMIIhsI9eN3n5NVUdlh+bRfNailT6fv1aBizmVS2FNy5lQcM48Tk2yn4+dOjbJGB+ypTQXkG6OMVrqVZKvJoaVNB0CHmx7skblSiPjmW46lH0zXRC7ZKtuxPeW0mRVEGTN0o/n42/FUqUGKdR1J0YwZxKy7BdCpWEE+KznJLH+5+6Pr3drebjzF/Y+9lJJeH78GkniEcdppbcGZV0Y+cP3e+ygjC8f2+lUV3u4mp+OjwpqWjy/fjPkDIhr4qULU5ndkma238ts5a2bvfeIW3u284CrkGrPQetzAOKx8ZyG5uqQ1xQayzobUESD7Di6XdkNHHkwycqmhqUPsUFtwVOh0YYdkONw5/RYat7symxejjlXM5LwaL52FjrsxgMDhiCR4srp2ijSIW7dHA+awjaOffT6rNqTN2S29A/BUSlkm8+2ejHuwo5oNuN7MzhttuEfhPyeIBxSP9oX5hCVoqTD/0Hr3AKQZTdr279tXAwSfus70fUFOo0vnuxW47zKFOfMeYVB6zdSi84RWN+oANJlrMqsIjKg2VgqNZBkeGr/vmAV/hwnYwFSK9XgTF7XnepRPvc5NpeOr5+eqNVwooBKl0JY/xpxyInx69a2RvP6/TEC253t0XdT/XCgGK+bDKscBv2Y6ZJ26cypXOwC4QPmoxTJ2EOIKhrLxmJv9UuO15RkkR3ZWh7O/jNXS84+gMhqIm7PffvDqDwWVHnnLGzGT5Ln4++N7fpQabErn61J98aKCxI3kiocwBSg5N73pbniRgkUQgZ5HLU1EzloRpbt2CgG3E9xC5g2mYCWVFrKB5A4hZvqG0RddukSWnMQa7vpyUcUg0x++VKo2JIzCIAKNF/0kHHyPmwCGT4JaqTK3gOv3buYn554S5C3tzeSr+Vi7t1FYW+4HXATIRqz+cIWhMzaqo0Ou4YhnKDcrQ3rmT86YyY38NuU3WiBHRJJI9TvW+q2oO1RhFTUcoAQlqtrJbYZgVrVORkc/5N+/4TeGAPL9qY3HHCMacsmU5k+Cy4R+YcgkYXaVUiCy6L44UuoNhHAA73t6nrIsAi+nbASw7WNwwOdbQ7qbUg3KrPiBT3OKc8wDpNlcllyf7xUKq0RTzZtPQ8lieEoeLwhKOZobVdX5Sw/RCHDNivt9C5NZxSXp9d7i9tGYkJPIyom/sP37Qb3S0x79CbPieRK+lfvtWdfoe1FjP+stDBYR+48O0Op1Lr6tJhjcYpa6ub1qRsKa9L+qgnVZbALkz+8k2Oniqd2dxgtjm/YEUzwX6mIU2KNDNMEZto8FprdaVD2ctn3bfEm+PEZoEWWky6BSo79tVGjUzfAt1qBGN5rqpy64QWPuKtTjabOfHSXilkrc7YS84opURa1FUNT9jxFHLrv4fYWR7xY5XFRTkhF4tFoxJYdmhuhpDtGe+RRfrszIRoUkJtZIpxZjD37oNzSmxBS96JVxDUDT6aMvu7ItaUW6dyWYTPwtz1aJbUaTASRYEuJbA2yRnQSjPG2vVMj9y0Oy4kfxOrsqTh3nmp44zTmicYTnUfsUMx7fNut9EPKqsaWZbMvLHZC/ibebIPPivCrzVD/wRhRnTa7T9TEJc0w/oLHC3fq2MnqeKo0QqGwglFNMTE1ES+6V4o5J0QSlwqWD7fZtxjJWDc+KEPHHRlqd33QXWXRrboVuUi5CZN0REZXCZeCfBboc8Fe7d+pMBqkCclvIiUvR1vfZlkZtzQM97nJMYSAWLMh6GCXm1UgYWKCdr+i89r9ovH7q5kgEv5AutK9KlsX/MBaHZeHLr4snilQt9AOelHDJCaAY24rEMP6FGbKfkdNoa1BbxGFMQOuHfm5Bm1v/5dKteoUKIEf+0yY4vbLlV6MpxGhFoo5OyIHHl+/NLeNoTJGrkd/tkqusEdIO2WD9GXUggKgMXT+TozeSkkFMoJ+rTh2FMdz07kubI1oOmegKfAD/CLNUkGBKFvtXC+mn3/s3WPADX9jzfuRNvghmgZT0jisc6nsFhmmIclTEoEOiln0aeg8vsFUKFmMB0lMC87u/KSxzt1stctR6uhOq5rBl2PI9UQ0twPOTXB9SKsfzQNEGwJurebm4QFMRz4IH8xVSlhxVZ5Wp00hQjOQSiJyShkXgM26jf4751TE6SpAuP0OqbrUXWBwPvC3E322zAirQDCQnyCweGRxgE8ozGP1GYJi5aUft3Of4ngGmvex58dCRQIJExtqBXO2LrH7H3GK8ml+4fsF3EPHmzm5zqxzGcXop6WWozrtWiS49w5PQVU6QbVZseL2dGZCxdQBws30dMYCDmZQFoEPjdetJjdHWMULOj0ZVFElW+vyfn3WnZUKP16jLqfeST/jcXzhh0sqQq27qM2EUZfNyTaWo2dgltHujp7Fn2uA1C+4jtIg7rEZ15xzdiYDKDXx1ZdgHjwv8qwvz/57D+r5qaLXR2JieVVyrqBwf79tG6QVQToagLgbFGujKod0bRnbGSQt0jllKrzI4YyRFof4kJqrDXdhiK5DUljrXTIYh3az1EWIkCgs1BIjjJYIDrgdHCpM6MecBaal8hFmapXMtrp3ewkjX2YsX1q/r7B8deygeG/j5xDDSEkee5BKuWIs9RNgxRrsm6XJ/1N2ZGfHKaV5cJWWlMwIMrRGPfhWjnqCR0ty9WVqCivJxrsWU73Z+0acuJHbS1u2BpVb8xHC/it3imWFbGeobWOxIoEBPj58jKnvUpmbOhp/kmz7u9juumu0zSeR61+4o+HiQwSHstWleRC8qgy3n6/hgv29dIPoOcdOTNkJhlJxZ4TPj5uUCegiLF/nLU4dQ1jo+nrIvgpl85rzy3lbklTBAWPrQAh9ZPwRZuyn18bFV2pA9cuxdRKLYxKD/2YROt568xmeF+jkoT6gwNIPzqn6eadI1cSAp3ZLgpDTDHQwZd+ENwb3LIYUAsSzWj9nwAd8HH9eipJRgHN8AXSaaKgXxsDTLv8jRhKCiVBRyXQcVXZySs3u7+PsvNmEVhXEA4f+0tvMBydJekPg+E9gSHNH9HHQsKvaTMkmdJm9Vtk0M0yAsqhFTk/7fvkE42WDVTaFbh9Ov+j3aaN51tiEUMyqN4dpl8iQ2U6JSMlmiKEM1lrc8oxy+hd5I7VfCHrt87GPADwJ78VW0IsWsXd//UrfkhwLno2h06+4NDHXtHqGMnMbByDMkwDQyRDbxC609275RdWiQXBz1cCQj3/RJHaLiZNRVMQ0LpAAsv5Fbu+dy0KKQHsadDZGVWRzmIk76mE7ecrpl7vXnwGWiMGB3wO6jo5SKmhvF5klFtcx49c9exYLDpv38RFB5OEHvwwjFUbmjU6lJ2C8etPbZBP009XQQB5r5aqiPY0beLddil1WGF2DTFecl8mpCwnz+8AD0d3k5txelirpDJXqXf1hGY98ql5x/PHa00qgro9V2UWvqhwnQ+WfKxbDtWflOpX/ZTrTQFUQUTGHVAWRnpLslunIq0ZvMayf4+emG5r017CfZTHJgEjH2vo0uPzK/5SopA/pj+WxFr0FaBSG0MzHCwg6rHOaVITeVGbUI6ZWQqkV0AgFyFEwnC3ynT9r0+kKSVVp7efqCP3OxJ9HOXvbCQJVoVSVzIyX+Q/4GKeQ4AhEYZz/H9CBbSB/v0k1ZisRLHy00kxSmtcFsuWWiV7ZeNBeNmi9afZVSKwq1znZGuwITOKRsY9nlg9DUcb8dXgp5xOD0hWmRFc8uE1dQ0Rt0abpKdcA881hOJWByAjoGuJOJHdMbPzB9HQ39udQaKdmO8YtIk6fceRzLxumCHqJdl6ul3GBceYAwEuecPqXRSOY+amMfns34YR5VlVstNbNpsNHF7MbN/yPtMau0EJQeIwp3OhYv20SaTOhSnZjQwSNrwZCWTkezmTQw3s8DERE4ASTWqnsc/MdHjdnUSRlPDd9iyfCipJ8kq8vxhuOX8jd4F7/yG9f+rKzVutewK0oCzzscQ4S4xWHCImNYfqd4zJ1Kmr50a67Ly47uYK3Vq5dldJsglZ6kO4CY4e4Rf9wOpdTcwZRzNsAUp3ysX3EVsKSn2Gff+lP2ns7RYg8Grirm2ogWrejTueciW5Po1T4Zv0XhG/FRC9E72GGCywcDJyDU5kWq+Pb1LB0xEoDyHQMz38HnRIJA5yFu8bsYecajWED27kWLAo1hhPHR7yCQ6B2ZOFrIR9Wk+5g2qwQk9B4hEWA3iL7P7YNXWWyjY/kZIJlkkYw5kVaGfCdUyAv3S9n5RXwkuKVkCDuTbq+/B1LXeK8ZNwpxRp53rTTu4xVn0kFuCuTVZSQBC6ZYUVKm0BYbpfZq0nPRTG5tAY17+qGS18M2uTzicLEcbS99CTZ1NkvhNfEZTIg3w1yAXW3A17t4NGG5e2A+1nDueiII613UJMs7uxlOQp/POzOGCndCc83v1rfFZnJ0wb8oEAvFvBdlmh3sH8NYfMZkeZmJL891EKlok874SVhhVIwW0tj9w0G2cEptSFgIH/agIxd9sj5cRtNEopszSWEsrttWnzd+BbrL/Vp8rah9Fc142Qfq17b876AvaTBA39lZVPgLmFJ5cNabmbewJlGjupQibZB0n8me7IylmVNpTH7RW91l7K/tunRoF5saV+VZVh+wipxiD8noLa/RbT8Y/aHaqQLH4LfocVCDueDIRVBIdAVB2RN6OSuyExJ5wZAyuuW29dRZbOl9ZhjzS21N71PqykiluGECNfaiLxINqtDzZntMQX2hauUKRw4Yc9k75Mg+ZQwYMfn71vvYa7OObAz5Rjiv5BE6jJ6BPgxorDd93HJkEOPiRHzSZ8Uf+gOE+F7btziGvpL//gfJs8UHLhSWsO/tw4x+cnaRT8tTm4OqYEM7AfCA9seT40hDamINWK6NrD4kmZzgRrjrL0mpW1UGrtNXQdORrd4TJdyHAtQLk98eMVeGXLmyqHl+3onN1f3b/dnY3xsxLtX9cGYvSitdg8N2JsJEabzU81uBeQGQg5Ag2PLHZtS8VINTmkG+mVOfK0nj3PUITO65xiH5PAwicDyZcRH5ib73oxIFtgZoCkmBWHOr+1pK6k2YbohYpRtjM2DY/6rfOdaeZ1mAxkJNIgSwkuAEPgiCp/9g+s3JhMBY5MR2e567vBa0ZP2WCMNsHeEs5O3Q7/35182yXfbPlKiDbch20UtdimQQJagF51v03xiCYNYB5C7hVkm/lqR20gmiElK0G4MJi7MnDkQQkz4p/N70RMoSQfZdYuzCdJqt4Zat/QxTNTNt+DHrKdaYsJzIRma7hW9fp3kprMptl+i1GsXxpSNmHVEyXcrInIRsr08wVes9Eev8Izp51kAt+Kgyl3qbuDq/ccN2zL3MxVXlcegb0V1/ObwuhM+XDt5nKRZIVsmC+ZkobzwhhOh3cXTVDkSljt6OVfW3UoBoHy//tJ1/eMSD6DeMHql3dpB5o0pBCFMFKG6ZK2RWTpG9kEv0yczSh6lDx/Zm6Rg1gd57eg6HavFJ/qzbd/vjjQnrRdg4XDjqT712oayYMEYiuVUHBGxfM6/AGzb3MkMLA1zmbmcPnrSZtTqWzrbJed1TpA3Rbm3WaC/SnqEsGUbmEyYHRpFV8TwPXb3/OiGp1r618yT8CCfdVssDasAAEtxnl2MTHZWY4yWROvIpO6krsr0DvgHWhWKSN3epX1Q/V+tyLeSemidVYVDHftJkUmiffvoNpuQDnCJC8qoFlAe9NjJ1bsJ1TaHnP9h4CxsA3U3/h3Lr/K9azBpwBDzGHFOXKBRhC7I67OqKpWWlmp3EN4xel6WrrKjJowlJMFJUQBbCmrFhs+7eBvk4t2qb5oXdqdBcgj0ZMpjqtuVUxqZDuYEgIRcCvefSxpVh2zCjWLiGjP1rD0/lpDamHY5oz6FuWmt3/igaPl0dyE1Kg0KDSET/JDgkE1PLE+j7X4wacl8MhuWwZzkfI/DifWlnmlt+Ld8257QY24jh6uV1KUAKDY4ZA1SN3WW39PqrrH0iEksIlEIgrhAV2YtdRx6PzXhtHa9MI1Apm6a2QB5fnGyWcn1MtG/to0wbMSc5SFqPCIAGSFSb8zaXxp+hcDdEz6YrgbUdsf3RK6s6mKa3twE3uv1TQ5iSJRA19Hb5JDSoCcb2xd7kC3UOzQqLnAQbofCzgW1jRDgscRdif9+ZvfJoJ5M2djwJldP0e5QDlN2vQM5xg2mfBF1k6hpSy3GHwNWFcC6Pwy/TvmW0hNKt2NVPUs528MIQ8wz6hQAiBSr0l1s9ohYqAFVhBE2Xk/k7UiCKFLKuZsVFRRAXY1E1Jx08Ml/lsmTO7Vmfo/6jtnrTUPpXjrgjhHhTLp+6OazBr5ZzYhgsmMz+7hYyIlI7ArDXtxMD0hcmTF+Q/cG+RNQPUA0qPucYgBc4aD688kfRATjxOUd9F230zhRxrfcA91+n9e6qpC98FGPnu86Mtysg3rvna89N9/su7l6Mo4uTPgVK3u7t8tqrMZW8Zqy2tdJbG37z9cSGD5qNeX0bb3YCqW93QWKuPMbShwamfDsWmCttuN6y6EHTrVFdZf/vosh40HVGXQAl2emhr/5E0s6XMc4pU+VlFvGLtTG+1LTXSzsco3SK1WXtACJ2OFtGDJDHJs6eb1dr9UMDiYLOZ6zRbNIOF68popqczVIkAMtd0IxTcOx8wLKARVujNaxXEGQ+wXJC3vWlBp2vZm60drbgDhpaSPvV68jlQl1YU7Cs1lmma4OflRIIIa037dpM9OIoblYegoIO/9IG8YYkzQCcHKm1CS1nuODX29PPnuUvw1mIAMUSQH9ekTOTInAw8/1knmPd/XhurTMTFofbe67UqiDi5S+1HIWn5Fv68uuj9ICMdVZfyEgnG0VSGXlpaaH9YVKZ5rpKId2EMGBEIqxu9ODGJRfvj0XLCHDQVrRblhPBqqCuCScFQsHB1hFFb+ijUGtGGKBsinoGaTqZ3M0cvKU6UAL56C2ztG88OnNKbGCmLt2VYgRNznOkwV3jGLZk5usWP6SoVoc9MxEgUOAoWdqAWuoZ9NkWvPtg0/biH7v7X3vitAJx+HONj/vB+puQTfdqwR5QYurm6npmjWRk1xvRxhZOyg7jyx1faaKudVoSo35W131gnfDfdXN0kEmqlXBaSmCWUTFwOYHLSA2WjSvlBhmfxg4srdnlw/iEUKZdjHWWFHIEI24dilt83vwtpj9Y+6AnNr+h3Yb9GVq+sxQ8ds6MnJhCG3AP+XUrN5Xphkw9imO7KiF3boleWCgGBzcP6YUN9HBHv4E72XfZLFl3yTwB8gu9iiJo8vEhxMeIrO2o9EakgT99seuJ7sT4J/1iMp9LJMY+bKY5Q7V64ptBpNRVhGUAei3LBb7a7VWZYcQ/Uw94d91jWqs2zxEyX8HloB/z52eslM/SOeb/uEi9Cauvl7WJsPbShjcMPU5RryLQYj+Ed5i+hNJzqOu8WMIwrf55I8Yolw6FvcQaPhEUi9n3x2gkZtDyx6AUIHT0SGPIJ5JF3+x3sWko+iXreOXA8AzmbviPWK84k1LLI8Nu0JuDZjVInSrt22xOTY8bC071bEngdxgWK+P4vlrwjlea/pYS8seYsRDoJtSWJB89Qxy2iHeShhLvUG7HdLEaaG7ERVJUdvuC3e6t/qrD+IUbHvt62GL/0Vhen86LGyYspQJlLIit8DgforRU4IYVT5ZzlQGdzqGHV0bAs1rw7BJz4ZhZSWw/N4Ck6FGXUk5ktlU/an0Gc0i5MLFHuy5Hs2Qb/d45RVQ5FEsJuHI6SFMhnyC3DdmK3jjvUUZkvEBsfU0J+NLdwq1Ynlg4dGBMqX0VFabqSkxsfdY+f1aZZKlglKHZGyF/jpiz012aq+HawHWz/4PHj2e1dqMLX2gue7mLX9aaRXMft3T4N91VFLGpxX2Q9e0QEfA6UM3Z7TlzeszTts8BQ3EHgA1GnPDYIvGtjPU5RwDKjlyNYjn3V0TRcQSdbyDfaTmtGvtDsoeQhUnhu2AJ+dNOx8Q/vZafa1kmbgjBrhdgScqSGmnyTTG4LqkTDkObA0TjHahSBha0udPnV+5JnJJ9DIl3Yld74yI5pmzH+xIgJOWuyj64uHbJJRtXPA5gqVsKTWtz9YmyhyrwZh/3LSIL4K/4dX/Mk/RWYXKne+3+T1jDhhbSnEpj1MOOLULzXbFgPw2yFdRrhyHMV7+FbX0HIkRJGl9T3F+EA0LYCTVw5pZYugAauKj1S1V44T781PdNu/kGvNkM0D1LKn5HgBZHQlU3anX0jEUgFFkgl/SYw4rAcVCe2KhhmXnymAAIGtmiGRj/Y0HPXmtrn+8ZbHHf/i7unVb4gWhtR5qzL+4FqCU9owm4AUXT3K2nlZCudBy/ze87qZ0Q1oSIifXRAnBOTzPczxbEw+CcWYjlp0Ns6RQbexEHldFhVDljZ7AGYtJMkd+YAZVbGa8ptAcnxhKvQIrySxL2NUfNDq8KQ5Dj1X88+t8wW/Ydm5Cdkosp3UuE1ijA8K5ryDrcPAQGL+dkqEMNTmW6EKocWIggaIxHMSyLrDVpCXEJWlS55kJa8LFWjWyzQLRgirtl66dOA39sK/wzDYxVpnAlzmfJfbZrRV1Wss27fK7V498XesjMvVSpMSxYv4GW7ioC8opmQ7jRmnb2hrojmt6LXglgO/NZjcan99EedodAMX1i4wlyGRUje65jRj9Pd045IFm4bml4ebFjKw0Y/+1P+7Cr5+iP+TMjFsRY5joLoeaChdEZ5O7Cfh6p9bIqCtxtuow3uIsR2LeTxmiFz4hArgl8kucIHPVMzpK58Xvd5AzbuiNlECTdqYVMmHmjjImsqx/g00eMbs/aaRpQ6VkkYqJCazU65DeoqFKizp2+rEiH28IsbO50HzBho2QkU2DCItZm5t5/o9qb/TN0uSjL/UnidX9ylgmjGpExc+k1CQtunJja2UCyhKz5kmTl15HzlcTLfF4tD5yYv6Vum5AvoIIgqlT904TJYyQEzUlb1EygYcsKu/O+YXsxQclTCEpWB2Jk5K5fhhVGRZ0dIKXtWUeqiGDY14M6uJtuqlD0g8+jwMDrBNRhqxcswU6Zfo8PBUTSrq9S9H3bfxwRa6mbpK4/utAFRFC63yclXGGA2NIYogGUHSg1c68FTMH1Dk5b+zas1DcB/Glh4TQ40JqCgPMJ8B8Pkmo4IxiwsWZazRZxdLq0YoAFFeOezmg3MVvBbdqMZFTQOegUC0HskIXtwbESocWAykDWPLzd+/xvfufUM16taj9HDr8o3HhlevkvBVRum26oO+DzQizxJVNrtGEB1h1xvpaad8uAKI2YF3sd41TMXTZ6sefPaZseJNqtQRNof+Uq7jNXly/9eg41CFyQZuDs+khSZ++pQjXzDD3hCu2wt5/dsuqcdnottp0HQjKSYK2b9gJfLVCLxkeIx922qhzbSVavZ3yUxP2ONVhavPNK93liliIXDhR1djRRMkpZbqXWOvgDuYxHudurpP402aB9S65b0UtbDlls5L68K1VZ5DMxWWVhyQ2sRiBU6TfaxZBOGUFN6+xDwX5e67vO4XwaTy5TjtHe80v6YdcM4SGtuyJ+Hs63g3lg6D1s7lzOQuAYxKPkJuoteFcd2GkTYgXyJtaXgIs9qTDqEI4jDS0MDEgMafqokjIfQo6uU/To88SMXwgH8w5qFkwan8QEm8gd8KjBmLxkATXgDTFTrSP/a04PqN0p5YqM+POTXiux3EpxMPSJzhLaZTc411P0QxBkpBJCCjKaVRv4Yj1+BAYArUNViVFiT9T60TCVx9h3v4FujjM1CvD12dh/n+KV4z9fhanFjUaCoxebhTeEwpYzLEMLtdEAkyB9N0D4mUVpL6N0RKcVLFKilLE6EbIb/kq8qhy47Nvm7cQu/GiQASg9eKcgKzTJ6msuPk0px0/vGrdCgmiXyCm/3JvMO1o30z/XjLowS3/XCJJ4JMxleKq+ulKTGa2imw2XzsYEI+dRtE1BrOrtPuXXjmGJbsKpvCnpg5kYJbUxhv7y7PdrdmhT0uP2fXTpecWPQW97Y+daUf1Xn7xD19qcBohMUau9Z+XNceeRMcJj7p6RucS2cSvgyBFtLgmvTu2LF/W9vA2Uv7/Bvy5rkQSF0RJH2WVI/iNAmxB71rnqse0pDjD3m78rqPbH7VS0uYszDiviB9wwYUvSc3erwNUo4FS6xgFZmsh6FWAlAgAG2bRO8AVH5LlJ3Wz0pEV+RPed4/GVrEnqaxjucoF2LU0Uyo5gvGhnyn7Zsglh2BAphemCY81MRzw+WFDepM/SRSk3bXcmIm6iMh/T8z/yP2ktIGkgvaGW997ZIDGLBZl2xESrQopCgAia0PRlP8AZyGwD3ftL1K6pv65hrJ4EvhD8MYSL4Hq752TU/delu8idKXiJL+XiQ1T5zq++liKV1jHE59axiTUfzwiwXqjexKne3v1TRoDlp6LiCE6/nZAMm8ul2ZWMxscP1hE9/wcaGcdyMr1Zr7AUkmjaJdIboUsn4S0v4N4T9EZr/NU9zYxAqtTiKydVe0pp/AIrdubBiZcMeS37nYL0Gj4xh4kc3TJf2KVXEyF+90hIc3exzjy3D/2F75hDQ+63p72PmNpORnbAUuP6lO+yOebrS/JhPhjbB9s5hKofrb/FXytfrMR9ipy11Kr0jalLqfqL5z4hs1OvI87acxVgyhu7CfC1My/f0CyJA3SZOQLetOJjbJ2ZUM/G9uiwfUZuEjLFl8gITE415qpBn5erOSE1wnAtdqQ8HeBy7QxKNSheJFLXkub1/hzQa0YBqrvdUFau7/5SZJLfpCSYfRSgRsnP/r8I9msL510vHlSkbF0munM0593NQNvy7wT0nEO+Byf3O5mZYgoIrKa46iN7kZdhPctK0p8qAgbhp14PBpIAYdtDJU9IIbodhkmR7DEESfyPvjA1eBPbQ2kW3mzuoFCgPuUUO1Br9wGvIxFMBhulp5TD+PXtI9QLUrYhFGEgLgAwnmy3VSMwWWk0eZIhgb8N5hmuL3paMBLIV7qAHhEU8kTcuw4aB1l5DhPB/cB4rfQk9nzbSCNMm7qF6sArSuNlTw9n671s0wXSrk1Oyj9uHkguGhEKHe/k+p8IA5WhfEyFrHslRAc0+qX9VLrkQYpRpOw4QR9Q6VT00lAlMaoP84CjrmoOM3kxvoYgYW+V/5lBX5Y6zaHVG1eo9qVUTY0oayY4KRRzVGRcTXsD5w9/mcw1rbIQv1kfRhDVxlPG+CNxU8lPifobm/4JjmiQHgBCYW5u7JIY23wK270w+3uDpU069COvt7yg9KVrMfwXkSRXnAZmuAfdF3clvtKbyuqQm03pBj8GkmVqRokPF/uhrW/b/wvJiN1kAhhNwtkYMZYYNK0kCFJycL6TcO9Yqde9qBr1khRHqXxHSMNHdk6p1+cLYT7u4JrNZGJPv1Jz/hZZT75D2xGDrFewYawf5jmU8D8kXIRJ+Bm3346uC+J5w0PIbnvwGeq2yBeTDP0LU0epimb2CXmvA/P+f2OXuwN1KBzJPCLWw+j16SGJO9527i5GVy3912YRymykXnLcpGE+rSTm9H5rMFUOODGmt0QBrx/2hjmSqXu4ksDmOnNpuIamBAh03meBVYcMQX/L6WA7nVOwvhw+kMw9SVGgcwB+AkiePKyEjryKFqd+SD98BsXSkmy1NRg3mLVrZjOSudEtVXeoJhdtHJHoiPz5C52+d9Fa9bgl5rtJ3PBWjxmh8OMvTISV5Ig8PGcsz2VNmfG0UCfX+rmkRCCNDOU1PqrdOnxxc4ZKCXtgIe8BDq8PvKeeOSZS2attK8gqaRdv4q+sjeEWafOW0uohblqqY8qk29GCNjMSLO9+rG8GSqYMOAKGNoucPv+9U6ZXjfn2m/RXQcawFbkJ92w0Iww+90WQqjyHfWtY+I4bIoxWpm/FB6bklEraHv+tkba0v6mt7w0C7Ju9aKBueJFCEUzjIr3p5P4wHLxN19fZacQnaKeGs2xJNhLLcTKIgUxhF4F4op0oaGsEUPwEYJ8kzf9p8TcmZ/jL2vacUwYIg6iW07qPKZB05uB6onq8Rz7scHpyaBWJM6LylbKcL0DYsyqDOgvOtXLSoIQrS2V12TlixICmGE9nUI83Xg3Hs+JzYvGMDAm341ApAS4Ak1DnzbhzYI5prXBAO2KkMEq4MfWdk8eRmHqJnXmdx063H6S/xvbPNiXjbritcyc+UtbEDjblJ6Eem6pWvQY7ukCO3yOMFW2wA87SCTl6YhiSnRyarbP4YgtHvSTO3+v00eIItDeXz9kqYxMX9kZ+430hdo5yT9qVz/tGScNy1OKwbK+7fgST97EMAiIa33osi1wxvmiJRt8yDzBDO207kRcHdN/I+ULlhB6tCHVuU2p1HTemmxODP+2NwpSvuitCi7NQvuoYcV+Ti04QbLvRzGo4gHEqgrLiFX3RDP+krkzHgpYQX2mTXGnLJxyAP+XTdioQxrYotV34xM2XWsNU9zQ9ykQQrDfi5WF4+AfEw2J0zBnilkK7S36kmXoEg26MhQg18lQvW3FvASmkgi3tbrdvxk4Nqaa7tNvHH02nUe+6M6O54iYvchV9RcFxs68QNFXmItsz2gnrQbkYE72zPrXKXbqBxPC8rSBTnbM4ig+G/mt6WjcorZqlyYrKiWW3gw8O4ODTRIF+UOTJbEcsUNZLMRZDP0n6/8GhiW8U2LRay6BRjLDB6vlXJqTVNgYKyf/zPiCXDceoQgoauE2hhLQ1HvxHfrVpyoyebShLkBqH132efriHEEDYcfXVSIC1V5hWvIgF74ivU6E5i+RJL5bEcHMPdlkyeDL6BYauPCohYEuV5HsNqJUNICmQRtQoiHxknFyO3zC1gsTsBFpAD0bxsIX3VQ0cqo+apOJUxYXMK1ptHIJEZ1qbCelc/BtIlEyS4O1FsO86X+uOGwgfBFRt76zixDyEF0rBXQ6G0FIjqvyuojFGny8JplvOKWxwFqXqb9mdq3MI7S/uQqx/rOViR0RQrXimVpJKGWsv8jwjnmL/gSjngzx0CoP07LVQgHMMfhZqqyxnPL38MH2VIkzwcmt73iQXtlySLHZy3BvSP08poHFUFChx48xG1R2vFh1EcwmV55sJXQCvvonKGtebNZURN+FDjpGcT21GfDq8I57Bx73F0igdoAIF4P6B9hK9S1peBKlHAKVP67l8WmJwFLNQxrvNZr9teO2e8X2d3mRhS/tD4lAJnicnMq8g+z+aUWT/kkB2ShAvHy+Q/DFcif4Qf/O29Ld6tdndRU6JDWZ9wG+6a2zM58QsWesaL0RECVgJLjsnwRRhJP5P2UCMiblfv2bRi75AGVwCnyexIu93nMR+6/5AKDdKr5LkeAC9wmCy5fnJuRiSITSPPGFTkfaV6AchaBt94NPnahgAxqRA+sTWYVip5v1eE3CbevDOl/5iuW1x8USjqXNOLVA9Noygel6oOifTUe9MpzPzt47FQnoXmfg6Zjexip+r+xjIYz4K2BCYdjuhCpJ4Gu4FFOsx5vaHZwwxV6Z3fUC9jscfhp71TnDschO/vo6l2qK0UNUSQRbZEw/wVh/zAdTRWX93qglxzF0o+/YZJ0HBIptrU6xW252y4f3XTv/Boi76VPlwJGz+yWa5268bs650wDx4dbgml8vxSQHtf6PiAkQyw5vXEstvfknryejsKIIsq8gIjgbmQhl8vn8jUYvpFtZMx16e3e2JRM1itcGyl0AL4Re0cTFOK81pwDxBd0AHMTUysgzWLgqD26/nLw5aB4okvZgxIirUYVoBGXnTQ8QTwNovwLOlw1AqJzUyUO4R/8yk3pGCBnvWXgEs0j80eahd8JQcdipWzZfhbDW3E8P8MjBcZ17SD5IGFNrEmB0TcpEhsUFbWJe/tgFPqjdxLJCL3fiVd0Y42UHxHUui7DnBdQDSQL6EYEkUM+p6Fw06f3j2eDhfytKk/6Q8UlxXuaRS/MZkIuSR8vfyNQH6JXCmphwVW03gDR2XvHFtTriSUN540ZYy1VwlkANnBgMENoOdQVdyYqZoLT50iNB1zs7CgW+LukOFg2slaL/DnSovdskv6gIKUCQm7K0wYJxd78zIZ42IK437tTWDRaLGfufr2emaxLNvZPXSNwqt5GTT81TVx51IgtYjpyIsZNTViiOfrKrLeysUTHsPqn8cYRmzEfE3ntgZeRj8MRRSKci4iLAP/KFGvkyALviGz1FJOv06CMUkaXz/yhlDO3DeciPKJnizL/8c4mCN+qs5dNpIRK6/TRMtsI2KlfZslIwOfwPD4eqbenT/UbkuDFFUPLJPSoGyq3qKmAAx+7p839s1YdNkna98+4a0I2HZpiIMZ12MmU2dayzbjPEssiC6Z2uspySwXd5y7/CwLKSrGNvHdZp1bMRAWz9ieVXAgHTxws5p1YroRrCOEwVe3wXBd0x+okbggbzZZsqOrNRtsWGJsPLzLexWbRfw+786opkp+fjNE52mekv22CKsjFI5YHhi/BpqmN2LBZc6yrFow7ja0b3yVT2WIMu+7lfLCglISfaRY+dUIUltaYT39Pn8n73LFmxtKE370TdAkpKCX4AKtFm5G8Jws6q6sGQYTd+hRP7OymTor6IhLWddpl6XlNRvnYYiCKwHsx6S8LArLPSMHORDHS7h8DePjCDrh44ZiU/YPEfz2sz4FB2D83mjgveNEO4Do9OF/zpjqmUMnUMTPXcnifk0sVT8g+T9HAIBE8iXzsiQesxovLvCwvL/CYqtXbr/MXwPPwLgs7oySAK3dqk7KZeex4xx3nEBEcCye+lix1YTr51E/UTjIW6Ln+5FaGs81dpZpee3rGo3IhDNMCf27/CmM3kAr6sMKaxq0h3QToS1oU63JSKlcmZArRmTVJrMjG7YMQzfWwryQQefLEAUnrCeeRDSNNrQTScS2mb4IVF9IdKW9a2n5+IovKIFazeeVUQOWoV3Qgbb8h17tqLMuQBlSCKo6LVjXg4scbKykUtdzAyi4J1BVayi+g0HNWCdvv+aEh1jbm4KKS20381zgKwgSwPxsM1T6vtlx7klvtdA3KQvOMonB+s6svXLsFm1upZY9sBgtg2tOQlwzbWLIQ9jFfKxxSNn16Hta1lkQui/cv5rwpCXDJ8QKGTTtGlpAd4Q3lznPG86MlPJ5uhAapjHepU//YteGJwnuD64TtouaDYNOUqA/OHViikpp8dvM+QjF9CL7dTfuKOa5STa2DmvFg+g+pWV7tMk2VnzHsxrDK6MLv4iYMYi33vQau0igbs/R611l+ozg+iQtM9aDN2pdw+YDe/REDrodaoGipaZEeiDpJ0JZjCOKG57/msbdNWCFLQ8dFcN6vUMJhJiv6W+RcgkXdNI7K1GZ0DupxPos4Ahz7sGfQgpwDvgvN8q3PCWSX1TkzDCc8UUYrPtCRmHIdRt8lRq8CfqUBchLXVGgcP5nNmwL6pLuDqgpGwTJeQt8oHBt28KZSzk32Vu9nfSLFgdJVlmmLUFJ37lCiivPIcjni1UbrQk9ZaUWX0DlrtyBqgVQCSTCC6yVOwq+l80yeygsjZqj2/FBtsxXII8K+fx9jk3Krs+LkUx68ZAaX23LqveRODqeZVGd1O7b4eal7oKHkv6lQcRgHOa1qd/NXoPzN/JLaMOkxVeLwGhovkIfd8vbibpJWKDuLCophwoQcd6D3ib/4bJtee0y8pmwR28gXRJbK2SkA3x3bjLPH3J7E2YUrJeK2yNzg5S30ABFwcHBdjWSezLYtsOc/lA3ZtZuX9MHBhbj8Ox1lP7uhn4OuZcvEhFIwhGZr9HCIoosxqPwzM9Awaf1y3x6dwbh1lIdXmIWrp+uMR3f58696ITctcyk47XX7i7boR1/VgucFFYVSQ5VbalaLrUT3+P58E4xZIOTTXY2/dki/KfpyPB6vbSP4KgffICev+c81cgapC31MH7BUmHoW6rAcRtzKrQJHDoCJWBEAecLFiMuk22dSQHlejPg6wcIvB7XAtJ4P7CL6zRJkeLszemPtNO5E8kGmS63UxzEDFml6iJDm8PAGUXeOk4VqcGYqVwggykppPLR5W8kRWR7bm8+H6LIlsdeg8OYOII5bZGCN2DHJi3WaRkh7BCvaf2p3nIRBxLxE6/nhy6t8LsXcgz1G8nvMsxgoGZM8TfGgg+ycmtMG2yEKqRchqhsGLm2GVtGWkUMSmvH7vVqz892fEh4QRVSoocVp1Pa7v07/DsWnJaWYSf+RDy0jD8CeZeiT/bdQPzwVmhpmeYQm/M8tRtKKZOUHr3iYFYpsIdMx/CuDpocUIykpc+ya6AGK9bdQV9vkQitdl1d6dkVpC7qfc2Gfqob3/bIAyM20+hguiLTzJvMCB56PMi6k8A14qMyoZqs61oEGltFTStBCh9FV8pKQ3aUmS3pQ3KxaY/0VoyUNQUbBnZbNoY69FAvsawtW5gf/Iy9BXC9ZxiWSG0uGBKw9ZYnFCtugeBbpXEqlFrA5s9a7XlzirCH07Mzj2FvsQEiG5MFQ3o6OYWZsXF6EePt53zhqjdCThwsk4aRtNu0Irm4uxdBSzFNka6HSP3VW8wIDoIdr7FnrDlVQuO5eG64jXb9CIi3TR+1L/1w4pzh+s9fD0DM1ENJMKFkL4gJmkiyTda/WgbPoWcfPB8G8vP/z/S96F0kFpp+DPSIf5M/rTJpEPh4Gs+UA7WzIG62r9hQygxBVtHGkkiiqKZF+TR8c8f2ZTyWy87PUBhAI36CS3FlBCmNAJbsdOpSesKjSrOa7II9NnBLLhNhiU5W5ohC06wDBycOjCMiYW1dtshzCrI3S4Au7RJNg81KcE+wETy8rbYwuG8Cd+1VFBD+vXtCpFH9prlLd9tW8z2W9HEHtnlYUENH6Ivz6/SSDl5eoPKlYigaGGk90kE3La2vJcZoI+YUi9LlXXI4B0r9G2LaxFGTn1NqbiQTiAVJJf6ZMZ4TuGp+4qOrIsEpjyrWVYbXjhJcEZGkxYVxWvCXo6d4Z38pb4+ryk0yACVpVPp31Tl8tU7UnYBFRKlPWfUhKPs3Gn661zDMFgMJ1MqWIIcInn91Xcvm5WssXdMdFTCyv+QPLi9FSAjD0B/0gSXdXrGWDMwnD1rfI+OeeB2vVuYdbfFiGRMnj2J44Ya1e9IoKEd3vxst/aWr9MXwOWS8HXOQ/DTgKKhGJEEJFpCT1xoHdU6CB3kTvU0jBsK6AtjR1w3Z4wPkpVEQiJQJGnjCDaAnRq/O6Q41MEEIx+nzEIMJ+y6een+FoCat2OJwIGP8Za4/YO6cuUe/p7U/zeuGw3Ib61SWOLC0ykYBPyRgLfkMeQS6hTiLM4ECQglkG/uHfET4bbmpnvuWuozZ8xOGE+fmcnmNZL2DcGy+g96E6lpxJw0XMyJWa571kaP4FxCzsOP0JwSAkxAyyejAD9riWai1qGoe652LT5Y9ggdpxKlaC6H2/d0JsXNgAmSuu9zoQBXTdom9oHQIzawAEHp2EZoEQKj1FumRgdhf8gHQ1toFiOVrbNF/JsDoN62j5ZGc9pEdDKFRwjz/IFWf3fQlA6QnNwc6cswHe0L+4FpIIO0O8BCmpc+vJcWatJ+vxA4NHAYHrVjN13hX5fDPzz9lYAcbAtNi2do9/FtnN6xyMEqDMWy97pCUHYZMz4X09FL4zyqg942RnA6yDFM33kJMJ9Sb0iKCjZ5sV+t57DS5hlfRbN9MXNrAt8qOzMxFCKGqygcki3RxbexZfF5BIDVrZYww5N9EbpDYNaMfetCJg+cigdL28lQhuuf0dNrYrExStq4fGhL4vcX2Lcnb/2M3beuEvYlARQCmNm7LITCz+DqFHIGls6oT2WqQWvE8SZRJfXbh9d8l81aSWLOtYf0d638KWabZHB415RLZsx7cqc7psOEWMUDuQpu48YuOeMjwesYprZsusHgnvx+I35xV5KPVcr2dp3kIQFW2Z4iV32639CfwiBOiMcv5ELKA4aE5aHUFbksM1+ysdvnjYmqb5brKaON/KeA/c+TTqNt9ClgxC6wGzHZFxKGhBbE7jpc2TpCnPPuqNz+IbLkrfrLBauLOOza5jaSCr2ny+PYxPw+ive5hG9GNA1dGg6Igyqaz7sk2NMiT1SSI5vQ3hr6kufKYhk9BcQkmPL0gMeA6RP+ofT2IN5LqCi5ZaFOm+5NnDhdo79daKGABboBkFzAJZlJzmLoPjx/loyopYOZihqCEd7grwRFIFYPueDD+R7kuUBINQwBy6oTZ/CL10pFDMDulZ5VyFoFgIJXXt9TzzwrdtVdRHztlGI6WJpWXVUV0P8SjQh7u/8lJJZ3m0+TrnnjtbYhIpRc8aB/0REUTFjUbc2MvzDYy5fBC2ReiJ4R+hDroz1lBWZUOeMY+MzWTG+oM+zsOcGgdP2Cptj4oxU/o1XdjNsokSybeyMFbJ6rIYOec1NTjEG38QotfG4hvwwRJoQNARxcDwkdCaTDDs1N1QzrWP1VvuzX+az2EggTfNI85vqQb8vJtn25YUZLs0WEttWbLpVgszc1FJHLXnAkTAJFCQlM8WqbmoUyCWuVgQnCaoYP3ec9sINANU1cChQR4yNEhiiAwD2vcxWnNMLTLLh+ydfK1Uero2v7R6WZzKk59GxjlVilzuTnS5N7urMAQlIFeA2CIRKYdiQ8pDTiguzilFXa99cLgFkMUdM1uQQn7VAUiKd3smgd/2zVuvTGaZzmk8kvxBuzP3VvJXq4cNRXWKZ+014ToolsWe+useGM9gFgGEwmPFJHWWrESoxHbAIQwxkjHNo36/p+0aF/DqRj2DHcyi8Wx0LZ+u7jJcovX4fNBnKzRq7lBxLS9D3OJ4Tytr5vmV5hRECBkcKdCSKM4RxRcBj34QSK3XC14dk5lgnV+g54bC+OYDqJZo/Ulyhb4ZAOVfowUvyraf8rhpz5QkQU2ro2jgmZC9qbMzu81fLaZtkznrUT/0BVlyvu8QLUh11zbfrqrTdNRNRMdYHZHGISctvDKRO7SOOEc6MBSp1LXaO1YWYSJXz5XWbwcBjEMjiFY3JOzE0AkPXxJfRV98VzIYb5HNA1mZoFN+fP9aF9rLD71Fk+H5TKooQPicSReX+XMGJ6yuevH4ROXY46QPKQhXFqDSvbO2bgqxtoEUc7X0f9lkObwHZgsfBx35sbBlCF7Iy3NDZse58zLWTOjw6YrqcejJ+xy69OD8LRqFo4YtN7JqowP3DCRUfU+HrpH2IIAraY9qMaxcKNWmtDpsYntwcRU2zkcA9TgfHjP1av2v1PvlcPtQMS73zDS+0hj0IG4X0m+FUsp3lM++lTIA/geL2jYpgvzdGzFmojzHZ9hzCMdkCftzXjR139RqR3ixQxlcLxUuRkH5/JXv2ovK79z/JI2JILvGmmKOPqNDQ1ya7j5alcAZ9tYkBTFAL8jNt831VM0Pp6BVlzvqeFDW0rqr0cAGwxLTcm2XNvKbCD2NfzgBuKHGaPie+DjZSguu42WzatSlF3I4b9zjuVfyYymJJ1QGAcYf9NxGHCQUtXTqMr+BMyVsuyRqmweNJkQNC1gWfnb76fNJB2QlF2kPEBRRRX6FQ/lX91wb2xWGuv0LH4scBWVSVQnrnFB/vb1SUV0WJ6LD2ex6bxAEsq4rUsYbNvDe3BRinxy5sakLUI4C+VMszK50nibns6+oGiAXTyYTHVlUzOzXyP1c0vOpMmlVGApP90bqgiLj9o1eEm/EVTuEA0iTP3XF/0z74mWU3VdpNocN1n7+7hlM+mjQjAGpj/9z8FdSg45ZFEBvPKrkO9lAcFszHDIFb38CIhWuN2XbI8v591JzhghTnoMgHWoAkb+KoFLDgURs5xFzA6CKjBAL8ta+PaIQBGIVwPUgWNMBew/N7lxoGtxATc2SVMcRhRpDqFJQVJ2eqhD9hRalfUiL5MkU0fph1YM+h/LShoZSp3qw6tUhfiSRJRno6ctK3M0jquGmBr1FkXJI1OzlqkGw5VBSs0lvam1qt2Rdd2Pc9i6JLkSknXrV0W5Y//fEaCZsGVVun7VrZb14OywtkURU4aZ6Ey4Qfp6h/yi8zRkjUSgjn5jqFm+4v9XRFo7xOEDQ+RR/UZjcVVRH8xcjLFJk+tyztFOhtj7dAL1YqBeUDYCf0hLqGhCsYxEUvc/iAgckUmDWiaPDGe/YFZLHQ+Q0a2Ix3qoVhgOXYzFq39ujb81/tdOBLyt/1QvL/J+t6gpzdvMkLt/hu7mfFjt+A/y/AFThEswZHsViLPKuPrq5fF7BihY8H63Cuh0A4HnASOiwvFnk9BbSbzT0f5kTcDxB1OZhSzzW80nSkYHBMlcQ/EHFBdywD9lV+oOe3TwDhnrowjCFsagy0rru0aYTfu4GTOFbq5RRaKdgRdtrAIfyBOyxYtlfBl317ZzQxxe+a6XL2IYRzCW8k9BTtWJNCdHfa0vb/MzP/rji/F8lbDn3DDyZ7BKO+Ddl7ZVDquSscs6JDmyuZfwi9me1ftw/zW86921QX2usExn+hHghyAHHU6rG6SlccED9MzWXDf/pXylriuC+Fe1Ru4oLSdDRk51Ra0X6rd8ouGfCGoO6YZCVsbUtNbAcz3xn4TFXOLZLoIJmGICNnO06oijj/wqjNoB6arUm8btgHKAAUEJYJxN2aD3X5wigQfvv7sC5ctNG6dLWzWuTkYQ660Iu6GqTdN9g+/XC4V23VOzUgUcvh0UPjw00gM8J4YF6qi0X5GmviOVUHPvRIyfq9l1jkscwsjiJk7aADvsnJMQ6WevZm+FtpgNw7PjizuUAbFkG7Ow9C5rR3TeSBiTgNf0cGKzcgm6PpCkRDewgdc0iUX2j/yxpXpX5JFgbwlwuq6JQfKKKizv7hEEre81SodUVFBcKUrXWHFSqBvCIAe5cj/POm7q87H3nVT8/MeRqPVL8wf7KDEc4bjuLUDX79e3/gfhAXrmZjeqksr70EgQaJfPiMEbdvQ6LrTpjjZeZItpM2YiQk2syHZ/JA63xMQ9O5I+0WY5Ay/xft3JNUcJr8gblG0u3Q1idXVU8g8LsAAVbUBTgLNp1lz+E/6VZR3JCdTSkE13hRftDWTbBhRMv48pJ8z/zKFVK8QPjeOa/5ZrJnZkYby9EXtUzRA2yTb/wD5+IwaCvJs4XCzzg7Cybjm8ItZWOBQyV6++MJ8jW/xsB2zxK7wT4f15xU4J85KpVk0leeKUIlk6KYMJtt/dQmsDqmlT7KlZ8p2JfGQzDg9TC5SaDtngZMERS8iKVdcw+sGf4KyiL3uguz4OBtfLwM+AyYr0B8DIvo9ewl8PFCCjcq9nXX2IpMV7XB/JkaJRFQPSOIBZIJ1YogynzPV8JT2TJYAWFJ0a2Xf0Zb4LgRZnxPZ/mfMRe460UVeR+fIyjg4PdfnmvlQRCsWXxS7dAvv01p/LhX6XstsqAdIEa+v6WcnfwtZAgap8fCMhjTSWKb4YkN+2TjyhIa5nJGy2o2ceWNmz+TeW0c15Pg4CVEYKi8fy5VGzZWEjEkpRYPw0kSUB+LwvXbXnBqiY8fKq4AdVtVX7oFwb54V9UqF39GKsahHDBu8YfnLRyjYOEeggaEfSs/dE2D4ZzLU9rIExv8nZUKGtqmqbV15EKoBDnLY0uDXfbrA6Y/Ztwf1JlSZJS9HqXWapOysuk35e+4xd9IobC3OixntumR9W/+CxJgLyZJv8teYuEaa7WOfFO10AFyPaeYF5iOE8G1AujwPyAk5TavJQ3Pjt1hSXu//Z20Q4AAAi4ErBVKybt7Y/Rz9K8Jt7pCAoQHjcx68bhXbUbG5+38w6yRWWWP+4wgsTdjdJcs4K+TWMJbTnAnlOUNc6ifBbNdQtvKL4XvHMFfGnlhFwPdAcuX+QNWrtLOhCsmfISADdcTgMJ8xIaekk5GuxjFCFWoHoNT0nt0ch3nXg2PRX9ypFnNHWsomyynG77thbxT2Jv3ZQdeP0E2mXnjEpHKi1UBOgrE6OhmZB0/6z6FQifGXzQUqzE7TEgITSIrQwNGH8w+sZ5E1rLPs0b5uSdmSVYo8ZVMxn0poKrXfjoTv90oOL0dce7oFj8P/VVm19UWr3bAkrq4fHJ9M1iRfMm2TEcr/N1UXY//tmxqFtZ+DMjDGnE1l1PjGmU8J1F9qcdympc6pw/kTIB/pvR7FMdgu8GdRoIGCe1d/n5TDAS5K19Z8WtGBlyeJSS3NMaxlSI8yg7rvG8a1OcpMEg/NruiETaYB1EGn0tB1c1mfDQU+DKCFA8bGnodfWKr8rNPaLVunt2an32fKJ5z8Ovt+irQs0DjefG3oN2cvGvkexIjqjy0CKtwudaoFB83YIzQi4qSsUjPOm990O6mFCIw1CwcTLhgmUICAk0/jSxnbldTeGEjZ8zxzc+92aW6+PTPQIm/kVtjIx8pRse257VYreJlkkEe1OF/v4r2AqADWPE3ZlfYS73Kz2YrxHvSuGfl0FEAPeKUrmh6FVngyGA6sG2FlEIUCcl45yDIzQF6vIBeDfiHJ1blzLea1doztJik7l/yOFdXNAu4TSm7/VG6lovDs1PVMUcGCS600izgAWm3mFl97qHkZEuUbVkAmItyu4i1+oZqAwO8ivuZWYBoBrmxd76ePmoufvvvGbQGjRk+Tndd8tfcBIEv/qILwAAKSHgOCNyyaquBSyAezoz9/WP2EjvIEifHw+00mV0I69bw5JaoOLAbsc2zuCBWShNj6lXJUWhW8VSu77li1NfhZtoGozGWj0n0QhF/GtG6rLgYhnf42VMQu8Ug2mnwiFINzD+1cJ52os4wMLrjdtnlAyCAYUDM0WG2jRw1hXTISyAfB18YRHhuevBu86+RuQyzZuIjSzpnjjtaFKzx6jGn0/qY2aRZZJGKjl5G37RU5aKOsNBQJSJK8fqdY0I6JIFlgBwffBlcw1/pAxRq31uKQT96uWCwuC2+D/DJUpTpTAErNG3os6VJo1zsW/uLlDbdL0CXOvaPCKBhiIEifckCda9RwGZ68JQ0si3MbnfN8Z798dF8Rkn0PYkwi+fwS1bZn2gsah42RPqj7SIdrN7dR6FYZEYy9WMceGwiLUl/ovZggUr4M2lHnjv5V/1vqTG1OA66xx+TmvEBG5qHFqmep6PA8EijjV91XAT0KbOFv0snDbQ4OPe5W5l9uC3s1a9sJ8gARlCjMcRQPpuRPKyfgrkmMKgYwJn/GgILnRi6udWVn+6HibtHfUEKLLUYxHSu6WQFeyZrAKfMBQboASpqpVYqUe7SiTk15D8McSqaFZJRoBZy/8QdfVirM7fAbuAsmRPZx8CtDDno+gXQqzdi5oumYA53jDe/Dx9wX+x/09Yw4Eg2PPDV+W7BwubeLj4GxnaG46+5dPop5qHgG/Mw4D3A/Soe2PZg+znwByeMvVIWmTmNzNidCiyXQtHIlBfulyJ+pUOV3xKkuF820IceYV6BZjvM5ttkJhKG9oxu1urX84S39yEW3WtsT1ddUHxrntZL8y9rhqTwgeNCuQwZqkfvEH6NtZiEI708wvKEEVA/s7BWs+ODSAdhtlRTLAMyzrRXoJLzpoTJUi7vnYz8S7khY00luv3lLgmYNvdKIkaYWHkmkP4Otkk9jAqXsLv2Agol+EjLipIvru2lI6tU+I3rUTqDIQIveIDJG3Qq5FyYCdzk2lnDo2Z1WkPYefeL5SEKfrDiLofkt+YT/HjIN4eCtx3NNtt2B2yDeHmMot5TlMn9bU0Mb8B732nY7qkrCp4kvhTwYLPPYaWKv7Uv9dcU0iXw82hKZDQJDIYcMBWCMnbFpkWIeHXRvihXHB2hiQ1t9Xl818hrmXJRg7X8c1q+3kOS8VyfL0BErITRNsLCMursFEsu/sjguaTDH8XYCZnEile9jrglwXmIhRqpqNu+P0ocOeDK2CtqpwUnNCyRu06LL1NQwPZXENIIdYQ64zAIAAlG9VMIPxQqMu2oU7Pqidhox5+F5h3D0tV1esBGJKeiJ2yf0xJD1grENTciuk/SwldBazw8YZniIcXlDUc/BcGeKsiwOTlzRNps1rCqyUlcOOwH0biWyhFVpIrZ8LCimau4lKh4wvi7sBpIWJWxi8oKfX3TqZYhi5AtSAeXXYFysTvVwIOjBkOxRtPJtauTMrCQkDd9z3kKiNhtZPnXMQTYnhnbQq2kMlsp2LhO1OTUdRtdsRUA3aIvnmYokg5TMCHJjzHRGJeNNZsTBQj0b4yoquQzAeHJONcpzN65C6CdVQuFhcErynYYRueVM+7D8OO6F581M4zNKOrWnucAFgT1ZrjMofQvhg3GkMVO7WNJmdnaj23UpdObB3VjANZZ5c+uzIx0aXFuu7toUjtgoKGaxL2tS6frnzT6myXYi8NvYV4kWev+QiRnApc6jLW6iw2RJse7oexw0iRj6hcYeqy7VAduANnyH08qM5TXfW0grS+390cHrMdLDj9DggDCM2WfdUTY/BNC5X8NHZTJSPSIAmNkj9uf+dIRGpaLj+6Jdnsu/EY3ZQJ1Ugm/rnxzcllVv5Vugr2OI5A4+xVOEwna8+/IwYB/h4vRiNRMCL1xTGPZhRlP7epzoMEQFLhIIDgerQ4cylrXouNlA9n/FYFdFGyG/JJsAPEcPcQC4sHPdKeATgYU/bAVhWGatVXH9LnXvNOkffPDwbP3zRzvqwyJHlnRPZ69s4neKr5I5H/Zyp6QO9mp/x8FS9rZ1M7tD/uEv6pCCxzYSm5q/QDiD9jqHoLFyLlFS64MyILyaR9yk2a6EbLARNS/QpyetkS6rPvfCnUmqWSnUUdZXIPx705QguBdrWQMlMryW7JfjfXBKcFMpGfHoP9+ZGvsMXVm1XespNpIApWHJdJsx+ZUnuexbbNi5kL8VSB1h0X4AdIjFy8wBXtvp0SCaNWUABiZ3pvS/PFU7wIML8v0EoLVoqkdcgOwD0XNlbKZGGt7yrzOhOk3jp4szvbw5HiYyyminPps5t0NcfCHna0XZX8yxUjL7H0T/T6/xFdIMmqa/CTVxoIcQBEO1UUqe6VlSPhMwzZkoENI1qlD+Q5Ku+ttoGVS8uGzq6Mr2xXc6/Hy/Q6mSWZs/tAV2+jZa4vBXRCSyGfxPeGZcJgV9AoOZH6Jh8S0BMGg6mXUl/0wgDhHIum/ZwzSmJ3fbsy0xHMBrCLOqHumP4yVUEcG3sLO/p9QPehm4+tkV4JtX+GCblxGcqKaRuzqgB3cfn6t0Qd6aO6vYFUynGAEH9dEnS5Dnqid9Pm4uJu/1UTh4Ik7+9lyIu5c08FtDz2949zo7oshFxZdoVhFUcGP6vZexW1J1mFoUje9hVCPFQGvpIXTy1BwJXfYy4ekm7JNbb/V/bD404ipp1Nc6OXzmYg7HWNMzfGI93yh9bF9/0iGpZWihOnArIYbJGahLVJy9E8UHOdXTYXvDz8xYPqQ45Q0lzwi6w72fMkfkBoDoP6oYk0IRgXDZU0tjkgvcx35K3sU6mJx7lzk7zYNc8x/jnurKKCGiDZXUNaKLKxJHTXDUxSSEGx+eNvLxIpJc1nONLJkccFK9QLgPW2B4YJopn3gKAecnt6a3C64758yF4AcJROqJ5d6e4/nPsi1c0x1q7ynapO+wrHl9VBelPHgW7HWoWXDfDxOO80r+6ZJQc/LpIrWYdZlpcvVVIy0lte0WP9gDqj3fFfOQpaBILP9bcQ7m8RIE+dP7gdxBunoiuB7zLXMXnZpMfBG5UZPJf4qELtilbBEda5QcHPmxOlPJKCdomJbQwj1TnrFiZlZzbxE6a49UbtjmJCCWX+ecdm95i5nY5xqzuGkn+xm9Augn6GIBOtPUyoRqpa4VlVFzkQQhgfY5p6Ux31sfUH9T0wZoRUOU+jv3INEMmwGHCmGUAsTdekVeyDGG7LAUHhl9/JHJqMmi0VVaUFmiRwo2+IbP3EuqElLwVLxCGTXjd3wf+t9YresZl1GElQBGK2uspBqh3+JT762GM4acTfa2SAtuS02aAjuVNCNdFnRo3U6ycBQhKhT4iwLky2XajLD800qrsur7y6+B93Uz5HGQKMpD5EOGYLES8ePizJx/tAX8bNyzVbjioOKqCbuLZ6kR1zp+KMrR8OWAMt+pmVdDD93oocSsR3ZCKBwcYfV/Nlubn5HtMcAIQ9/TjAQ8njYidALw8psFnLoc0PNJ6plGVthImrztEcsVJQvhGeIR7z9yNMPsdpLujctj4dB0Px5wICz+DAou19FajuCOax8dKiGBZzU6gusOtksP5o4B24JHnUCVtXz0vSGFsIZsSR7Km02DB+50LNdoZXDsHhZLu5Q8hNOcuL13dMa9AE8d5uqcL32OscxDy3wDkPmxDC2rwO18wUol6m0DhxHCvn8C2grqv2BJfbkoh7+u4zoOfNn8hsNB/2reYdiT53aiyIqQdJWNzO7O44jdjFzlzDCmFOBAxsSlxjkQXsBWBRYbQWUFPxsBDwwlX1pCMAQBsyBcAS7gGdpv6vRVmTZyYEp3Ydy+w7EK/ztbZfPa7fXGJ07nmUZk9r92Si1EXwdUycCLgnEH811b+5dEl5tltRVxVSrHE+wq2r9TKWADVIjO6RFQNgPtjBWaz/o6VQ+q2kCjj3MB0XBv6dH4ij9pwHt1FY5BPLqiP5KG1ERf3yzu2nkK6gRIw+juJoDlGMa2mPlQaInbGdit6jupNHo3WL0eFSKGEk4BP6fZZL/lUjvj6TVCrCIm1kWm/VtO2LsrQTy2OIOp8vf6q+Da8tuFFHe0GyAhnv19O8rZsGOEB74dqcxzn6XRYhFuzTDLll6xKC2N7AqEn308hu45zEsQA3YcsIn/70kBxGIYetK3QKVW/lyrwup1uB66cZOqPVV7yUtvh13pjKa8tlSxCxHiVdsz+UZGvTu/n1qejkVxh4cyPVmqX3umWlQ2MLKsI4vQSaMh6WhbN/Fc/iWj6vZ+Xsque75IapuV+84TuDe1XRWkIzl+n8hrEfRefX6qQBZau9ss2GYLNA1GfJVj6iuR2HnxBZDCS2d3RJnCPGsyaE9CLNvsFHegvBVQVG8NQfpD8ihwx1ui4lkrMSXSwtuVeyFwQCf2FMF381+263UY8rjSQWNW0zeKYDeuMr0q0ouiHZJstgUHoutOJuPTRZ0XnLZht22CHgYr7Yb7JGIYSdBN5hX7f7hfREVS9u/AjRZxckXWDma9YymeciYw299K0wKeoHQGzj4KOWhZVtGB4rE6tq0OB6uIHPydziY7VWa8OP4+dYUPG7V6/+eLiw698I5tZ72RphyTvOzFjBhEM51slowCbuyVHoYUJaXCLyj6jhMYekJ9LP4NGWppIs6azUVEYduKjViWLmyHigulS3BZbyMl/0I5130xJWG+LWNQkMQKF7BJbc67vaVfPPAgD2D3+LxmHg5H7u7fg9oWL5UnAYaN4FtRWgN6OqNjclB7CM7Js4+GssAWGgstVH34wzsNJ+thgke4Ypr8ppZrnY6Xkxn1oImMHzg9iy0vghBE+1Kfjf5aad0RIyBiLOAsjL+IjxCttQR+EOwAvZO9uZAcl1mfDWSDfcPzLIpbSIwJ9a3XVTPn+QedEPhoPHzPY9YmcrfoHNm3qZ8+kJPjfX2lboiv6pTvPBqINf+le6jUr0yUVX+1aoqLhmDZEECrrGD3qLxXltzmKO/cEfh9v0J6emz1ZzLShvLZWOPXCxg0UiaI5J4PxW1NyouebM2e4yEoE6a6ExUVThKf4xl5It0YDnlbnmoJBkUQCEJQEkS0HxbUThnU3xO0ZRFAq3KE5FvGOkxT/U86A1ClTZNHs9TtX2vO+brnTbLGttGX1oorZJ/pVrX8YhC6OjE5N1/nXX7EnrOB6CE3zF0/qBJ3YdRqAwp0NgoY9t/e8ifQXaxRmd7GK69ZO9eLd7+abU0aQzTDnblwKWFz6NMJv0YIUw7O6a9rXCjJ8hPIWOOaaU+T2XNSt9xWmcbzpinm3lp5ERzRHZzXkFwfPZ1HuekTciluw8feW36VoEcZVmajSE/tmuPKGk/bnnkc9Cc/2ABoQ2vOJDmH4GIBf9zy29a7gMo/7qCtAnlcqyfvAchsB8nY9ikbczzB+f5UieOAyaSN3Idhf4ao6A5kUbQWeOy4mtxIEEFJam806Uug8fJev1xWKAYMM9Ak6KDeH5hpgcthGy8xrY9FLnmYWS6VOaPfTI3yeHei28Co8Jb5BEDm6xT/bIivaJqnvI4nMadyQwBDCOQwEYyWqKYkWE8C4KSqI2IlvnzdO8VFsWrbNz7Vk5koxLS9G0eliNVMPnvE0TXDxf4Ptr+X4wcTo93V87pQp6IOKP3OmXBVHtR5Hg4tdr4HRVsfw2prpmEwsMtdk1stwQBzRD8NeakZGdPnxdTdACs/b0nb3LLGN9GAiuZmKIxSKv5bp5M2+R7+qKgpkaWZOFzhQ5JcFi5t+GllcrZkKZEssQSZQ1aZN54ljhIELXEotNaPpKBsIi40guiNefmkfWDjdgIKPMvYOIuC3J8H1wdMi5USihXP8zTHyX3pPrEa6TRYR4O1I1qUr5DrbdGtmub25/wYm7c2Jj30cmjDO+QcGhwnC8+09i2casYTzAVXA8GxGR59Byk8PJOObAsnoGZghoiuEvYX1oHkwm5+QZVed4/wBh08SQctuNR5jzZvqmPMQdpDVnfspB4SkhROadMPdcgZNVDZ1f6BgSW4zqvVD3b4SRmglI7PC5GnwA+IxuoiWTXu2MBvce2yB0qvWengy75QKUz0tEc+MFfs4m5M2YEui1A2VSrpwdTlqXSj3QjnKQC5HafZ2fF3rdI2YX2pCENt8WpoELrPojWpLysBOOWkoxpLrt1IQDYjJYkAjsU+Jr7En1M5GCIkGLUmWpG/ThisNEjg9vxj7VsfvC1PqxGSUBWfKsjab4IWpswWxF1NvI7JvDENXBvAIyKYkH4Yk4nzGlCMEmQ5Gl8CAGr4bNhgV5N5cIB2i4pPKfLtOAray941eJyH0cI/tmI08MS2KUi/xzwkYCfk7+f9xIrzZJIjWaJaUJ5rBIhfIIt46IgC5IPXxjf0miKJw04gUHZZ3+8yHCd06yg9dx7KdfQXHKP8sXrVciuE1HCJ9J7+GVJaWV3tLnnIYg1l1wckszQZ6m1/ssydZ7/O67SlsLP3h1Z4iuat0GOYUU2gEFwPpKOhaiXAvP6LMurbfcfzVDBfiPKShjoZi4mePzI9U8JOEbfOpnl77EMr2YgqKBGCsfYdt507dxGfPeL6JdNfF8vt4Em3cQvR+VoJ8zu36BTz2FJNaUW/eBP95SujGkjGLkPfGh3fBcMW0Jy7nD8xD4KOOPVZZFfQCfDVaxFciraapjp5Xo+VS7c0CfOktQ5TndCkZ+su1p5SwWFcPGuokuvmVkKMow30PehWW4UpDoAW2sfNS+2aD4CA0qT5ETVQXqo449scd46cYx1BPZpuscbkGW33R+fztKm8i9T6f381O3QUYp30GwtexbVTL+1Wxc/i7zrLrsDa5/dGSkCYGD85jdH0w5r/iyorZcoKiuBM6doCNvIeHg9awi1Du6pexN2GGQPeQ1zLNGtn7w0yh8nhnN5QqG8l4OMuKpoFn4sbW/eElEQXeFK01/R53Zh9CxJMH71irAQIPyog9Z121O2ViYc456ecGytUfi2YYjX0+q0y8vSVo03jnGVvQpPfgwNAMGKfHwr4hXsoHPiH+ujcva0vFZ6jFpqevZmUbVLOYK23kF4Ajj0k9Hw+sexqhI2m2z3icd3dUpHuaF8vrYx9QUeV4wPjMltNECiuZHg0rsplkQpwcpAVPmSNmBI5Y4XQzlpgWau1uWjLaE05EecnG3Syr8Q0ejGLVtNlibRqW57dbg1aXPj2qcsSrRmfuqwD/e0DpmzM/7i2HlpUOzgITcBxTBrT3sbecEPoIYPy4mgjJ2IqYzjziaAv5U6UaVdU+Elj3EJ9tnMkgACFSdn6nh0wiTA52I/pdO5qH38duSMWunZvNrfQQoqsdls9cj+2o8uair7zvYpUzsikwafuT1oLaHr1k/jUN6QA2YK3P/PHuuap873RMMzH0bXi7+QtCaXqT1HHWKff20BqMKRVNqK+Peeov6mkwZeemUvXzfVNg7Qg/cSD1UT6Jkd7iG051kPlelXRHdbqwQqLjRlfQA+m4Wv2E6/avjVexWXpxb7hHSLqnqlFsNZm9RqQtNTDb8FlijwDs8JmZyAwQ2gqjIu9nXpyJT4JFGGw8nKEGkPjpzH3Md0mXl0osrWH1RtBvrgGCAS4V5FC/zFH90v3+0rJUjcrGrvEPx8/VX3OZqcWsdZrWsLewJzdeug6NRWlyx0yboxb1HAJ34RyrXFEl+LpH1sqf6+g7K2aMr06DWU/KCKhMm9fjXY6LB3ANNqTuUtE8yFYAsMCk2n3baWdAkn0SxMLdm32qIz6QGIaE5oVM8qSnfPwO06rw7KpM++qVX9DwyEFyi7/NFvOCqNealQPi+oStRVwcBetD3zzpyjcrZY8MhGINtuZnHwIr3G1fOb8L+ygoAM+GtqZDnXYgy5QJPFCKa3LoySxi1wJY6yhrK/ZO5P/zHsyf4n8A5mFs5NSzKYks2XoC6veiBDYxiZNiSdtQEWk0nGSG90eSqRByiNG45K0P6WXKxdHfr3eAXQsQvKrRD78TREMNo2cgO9a6MR2sTBYeCTKz8S/ynZ7Ltq/3Vkuc28pxdlAKz1pcKXE4c9FhjVTL8Z0Qq2xBo3xZ2J5BCH0d/hjZIlM4uAHErHk67q76Fvu7XAM6SW+L88ZgINJOGHoLZ2H54zp/HG2eOfceotwCo47dWqQd9qZdJZjRrqBop5kXduCMBpxTpF8E5Y2wMk3HESXIK+MkfdgFaZkmLu0BY3MiP38QovFAUM24FW1ZikGubMzWrym9bz3F4wCE2ake7O11xiYM/lBcXYapLOl/ei6yuPddm/VFupn7peJDBqk0r+eK/jf+NaXxiLSixKp4J1FTvhnmB49tOhvBQJzYAybUMl42O8yQ3Z65MJgQvib9ZPlUuXS9yR2Bp5F0aO87dZ7nGNXJxYfwj8gYFB2tbhNB9FoGajyrYwv7HFd6VXo2xBqUV5Pq2CLHrr8T9Ay4su1DwkmYzeG++9qjc9QAR7y+qDH8s6jcDX58NVQGhexvL2Pml2/hN/LyNU+KTLb8JrhczvkCc0JQeRcQ+Q5g9DqW+CHfYdMXOu6LGDoL8wh2LC7ThIRSEd4n5Zlk1P1iZ6aSlUF2pbKWlFN/jipfCBxLofyaXbwm9Y6hrpCAh7JZ4dv3LDwuwmZ6JuCqbJ8sso+fhwk/qDFuHW5ZrapTAvola1S1Ca4XHq7huticijA1sLydXlRKwLV/i/BJ8GphKkwq1rDAYaO6iTECQV6zJZ9FRkx1Zp3jUEWH5dGVsb0uJTjOEEzHf7QOmWqh+o/4t8mQHPvpTMsVLdDJ3pNDH1BZGkXTH8yBaeLwZYej8YVOvdB1+PVnBFoPse/4Y9YLmlLUU20FjxCMqOm/pfWVZvNthaofsJZB26uF4/buCmtQk1rDOpNhX2Vd6mqZ7lHRlpuA6T2dUmjXWsvC84TQnCU+BHPD9qorHxGllKE3k0gyU7Kz+yJTD/BcwTg4r5IzOcV8nEPDHW36u+nU4x7ZFhKb1tJKehGw72Lcva0iUrc4/8ncw1nKz2e0T531HFXNDPQ4igMrk4rXbCALN2foKCKGuvp2RQvjUgd3VGBdg5qbjQTx2lBELkluovcEYoZA0g+AXIlay60C5pbiCRsddSEjaVaVZKl1HWkOkTN0w4Hy4W4oSnNMjEFiVdZzCgWCPivkuJF2dKiO4UF2llzlf3K4nEeI36ooS2zYw0PFS65NVvF9J0pbhB9t1tj8mUtJcKkLmo7v174rOArXUdSy2UXpHWz+9mBChfGY9JFTzCGhSMDMgjWmx/ULHogzLs9zdtgKyuugf6XhjlFxi3RcsF14yJthwHmGZBXC0SXGWxhxqzfXgWkDIW+jEqnYh2Gfz43CH6MqN1Q6HZOkbl5wa86kAzYfGz1U0ZGw31ew25kg+efxny67bRov5vdY3p0jbOBQ/tuNat/Zs4mvhNQokUH2tufwShcGGvkhSkmE3fz3lJrFgH+XWmMXP4vS/qQUcpCqHtrjvCC0oDuzZzdLyJ1+k5pegjxSL/fnGVtZG2BlMXzZqS+hc3UL6L7q/6sHpkd2cCPJ1Bia36wKrnewxdI/0wuqiLv/afguaPn2HzSf9Tt3QZFvK7fmURYVv8SuKlwt0kHFV8ESaeRf7oF6Ymk/Nej76bHAJZO4q13HMUPoFqIVx90Mo2g6GOsGWoQd9B5LTOy3RNraYIHwILAZunkrhAcacD6oyKXHo2fFABkhgPXpkm/6MoVDWsoCOCswlHpjhlu07nSTypqg3HNx1MP1SnxGO1WDrWnMQrKpDsPiQI6iSf0vk4XUaQpur/LRMA1JA/hGkMBXzaJOPe2tBLX1mGeFnkw5fULQs15HGxy879HOVK/cdj+e0f+FgEbzPpC3Rm8KeElB25WpAJm00nQjSWiHzrgvodo6g3V/qoJoNRjTylMeKQ44YWscYY/ZpwjcX3EMl6v0W9+fwiognRzcrzXdzznYsNAgFHXvclKUOuShAfACDr/X3Bi2MbpWlxcHJw/srpnQSJWwVIXaqOW8JZGpuP3n+eNOdXmvpmikWypjk1HgyHyQLk/czR6fJAenTFGF0Wa79ynAWcCMq8OjeFA4izPd9T4mkaFXrvos+OafE4PnWatqUnRGqXEAkZIRwASm9la3wg4nGMzIXI87LhzEcWIXAoWQ8p1y6wqmN/7gMBYpyQajPd8Ew/f0BqTQtAkTONdwKIjg7FxelPFTrd4zJJiOWSzoGxiyuQJ/W6V5UNSTpym39EEG+4jgPXW4Rf3c6jFCS0m/LABRUvLJXCN/wWVxPC/rZPouz8vjjQ8e6QaEZRkEmJ+qmSZyMbU/oscLk3oNI0aXl2d6SVQv/22qkI6zbzv8EqBjOEipUlMB0sUnLYqT74VjmFyADChBqQFxLTINKM9eeBbFvBCc6aDM+LY5YQd9rAiJvrO6piA1XrNrgvn/423TXfT0tdXCs3MmpPuoYqtX1G5nfxx3NL8qWZvuuEGXbIZ6OD1VuuKzUnYPYzZ4HpO/jnlJbjBbK0LGcz0oBSLaFlshZ1BhaUnPSDWr4AESRUgfXnt6kYXdtQoDf7eAWm+MRk/7ocjSF7coZ8gG+Z3pNpEOw86/AjR68OxiRwPWTCNNcRSct+E3mVZHSv9tkzl176oDVs2kmk4XkOdNpdTckS+FM9XjoTGi9TFBeXLb/Son14+5JzzdLoh84Mza05KmKx7o41up3ZMrAt+aFl5p/HY47fQfYvyFIkr7ByIgQ40h+OJvUIljz9OXrOWOqidutWaNTWRReLBDzrlQKmEZmFYR9LSVlwN+fhl57BsGFLiDYJhQZV4UqF0/HNGf8pEtl3dF39eJzOklfbMGeSZLq6/5x+PNLjbKI8UL4a+OxGhmL4ZH9ZpM2YPwtZ2JkjpjlMDlf4eVCkBQOPSeoQOav/P4qbQ4FL/LNQgGJ3bWbASvUKaWQ8J8cfLirf2TLCS8s3EuoF7C4/gDYN45CeZYRjmK4HUiiCnr0DIVOFEJ+eMPL3QrB6/n/0QXUcVyui8PPSWbrUdDBgaBSYu+xVbFOE+hXe5Us1Z4UiDQ0Hd3A/6noGfCsguqtMQdUXpeGjdK6gHO+Hp9DgT4VWBAR1tsfEHoCskyUNaoU0zuO43AME2qWye8JyqomrxO/Xm9LROV13Djy9KblNznv2miOTYCtF8KZy15EqfpGvj8i84kJswpSOvdRA5HiSRxfO+xoKpOQavL11CU+5A66E07oxWEUV868Nf1T6Wvu+UZaFTOLCk+oFGSlYVBVtA2yzjinxviroeowI+Snd8WKnsfHVkHs6mnwulevemrNaLR+VqFZ/KMfwIyDwDJ0u5g6feqADhtCFAhmpXh/72AcFWHLwcd3stqDBbAaLYHRhrPdj2EJOkJyU5ykbPMzVHC2e+HXKJkj87MieZu7eYGPOWSB7LkTlDXGlpjiP6/eZ7f0D8cohZ0oPLb9AGThgv3mXpLFZd4uQ9TUnqnHdzTUdK1+rXsl6fHdOAXQe3gm2qrRo4nTg24RzNIVZIJwBr5kkKVfbpsYl2LIoJrw6UH3SgAr0TUVlFXiXGKJyWLPZNvALBJIp4p8tt6c+SF3YBWD0W2vczQzzV6v23EMSkOOoieG/n4XXiaNWY8XKjFZj/AFAMz2aHD4jUlMWO/Y32bse6cFOTieO4hMCcq9LbMzLrBswcy/tf50ErBwoJAaTbJVg1OVVZ844WvxM43yvYnCUuoS8jGFIQlUtOjJlt2NjPhr9iglcYCgdu+FvCXQz/UWfeUipRYp0nDXV1tddJR44vB5KJBrM1Ybt2Edg51K7g0K5e+ne+F0JwN4uIW0ghNlPwxc3jdbCxPeM4+wpHACGJLqvIzhosHEV5kq8PT9EysXFYAMiAy9cnetvwbQdSQJpJtoAdmzgqz3q2bf9y8s0CgagZOZj1Gh/5XzfZUsrdXOEiUvenkohU82bqT6OSlU9GnhPoqZFGDGtZFX9WskakloPzMFhFMNUOnC7G07JjapfdTsxEaZQs1bCT6wt1PT38U+voysH6h2j5/ssz8Z1B0DNag/pFx+hIFmUzTwPmHX4u26zyx0XGYJYPbvZoVrG0cfclXjVGSADVt29WV9+K//sNWODcEURkQQi8CPz2lHGlTZFBIn455F97DmDIBiZMNchzvkW6Ek4SSu9GU4vIpHFeh64Hitq33RkYa2b8Q+7+4bvJlAYc2C+lUnX6rzgsMc9Q+VrcXFfO1klKF/wolk+rT3xGjVzDkeoVKXXmBmH4bsZy96wer/WpqGfgIWh6vq6s/xzajePoW3tyAU3dZ7YpBsbjztrJBmMLp+wc08fr5akJ+fSqwehghNy64Gwk8U9KVMQoaGzs5T+jYqjkkJ7OtotDHCrZUF3clRRryuY2NP1qdNy550E7dacBi3CnuRTEh1EIVvbls1gB60++jXtGNbAs7Oisbu5G+00xb94gOTlMQBHk3gxBiUx2MpniAl0RXCjE7HaRLEwXlqvysOgrUW+0q/ZyQ5WrWMeHcXWQYrhneslWUC7iOKJ0tailitsVJJDEbKweZ1Imeg3jU/YvI+w1E6hRYUll8tUqzNHTcvCc/2kumWvPnx4jzwLEQRElYJkDN379sqoab8DCxAul8NiJyF8gqrIWAU/PqULReZYw51ezTDJQNJobpkDlcltojqjjTNV/UKSeb9tuYuLEDDJ2Tqrx1HUuLP/pB+QOXE6v3rFaY6D0Kc/aVcNogcb1QAL4eb6/k2fyLBINiyxF5ilQIVAynS77v8hm5yLYCc3S6golgDyBx9waKtnzkCeIvn1RzAvY2l/OBfceJSRKBUfYWbLcQiyRXKspHcvwnmAn5a4fEuWA4V1QWK4EiJBjrd5Toy8U9/Ewrzgv5ZaG9Ej9UUzfcXjdB/mLUin5OOFCd6ZaExgxkEmIZMyvdHfQw/G/DvoGzuzE/1DFg1jYytlEO4pIWNyzsWQWiDXjSQxyeNe+tk54HbP91OGz4WEQB2Epl49XgvGJb+0g4bpF8cx3MpfIf3tH42VYFqeBE1hbal08OVuD9iRADLL4dmCB3soeWswFtlDSPfzZIA40Iu9i7KLf4Mrc3UNY9uZLRNXzX7ZcMvkwIusiW0y+bfuj+lcNpBsZboEi38BUH5WCLSIHKBEmWGYimIynfd/U/U12FBTplSRTWxwkJg1EKCNZo43XRSM1Zh1qyrdRbShhxtA/fh08sP2hxmc6XqvmuYvHFS2IrwwbIHqL/UYluB80y1azU9yGEE1b9g1B/uslt9XJA2OMVjhxKxuzOGKS7JW2VOc0wa5C3BHh+hw9vxdWzCYj8a2chf+7CbbcHLnViog+sgSG5ShcLfokaxOqNZ/nk75xxutNF1NJwAmwQDYndifvFEuTS5rsSnIo8SAICd3SM4HaDQbfFtmSSdNya4h8LFu5T715owZMr7brTZoODgAm/x6uO8M23/yAymHOaaAWLoODjskbrxYX9ypL4D85QczGEpH/Y5bJFtN7nTHt82X4nBaNbu+ISla1xipfWNtyf+E19ifCbubzs1Omb582f64Vbk46/whko1yzdU8U1iHHbOZPCMz5+yEdoni5OWhaT97nX8pIGvr+zjTR6ogufPUEbrBxbpbpHS3sWFaXuW+cAs1qT3wtYH0nDWShbVqKBkVzuy5/wQSJpLmJWqTeAQFWKCbPcd2y969TxaNt5HakB4uaMWKt+v9ZImtWZ3dAfvs41hohIjvvzVBQitrnvRE7k9S7ceneQ0AfevY9lr2+BbZTy/gqhb3qDLc+n3AKqvgQjraFsLJm9V9aKYf44U2L9dCm7Tb0uq6MbYPdvcORdJZroqOLjrfPEeLDeTNVZHPII5IvG1pxSIsTwaL6csnxJl2/pS8JvTKTNDwZ0OrPuo5w4Mkm+hFdtJu1rXQEE0tKr7JORD/3Cd5WnskB4CoDO5oyasCRSuGBruPeQ4/bQ8ixt0wgfHmfkN95o6+UQUkAKP5jGjSTL5hL8gQEEUHltbGakLjrMs1o4nAbbPYHQvpwysIOqu2OqOoi5a8vI1GbUKyK6M0DDw29khrp3wojM3uAOGwZtkiW1NMzAD54a+beHWRS/ys2VeHgmP2d6hJteGCinhJAsDoakb8QtdGRySfXeZIm0nW1iUfombG8vblo5tX7nR4002SYFj88p3WnX43kXeh4IWvfN5lHPZEkJHAEe+NfTMbSZHRqjDcUWF057E1/AqfhNSvjA/mwfVA2ceEVmbD7PHTZ+iEuaDUU7iIPjkuq1PbMQrldxzMKfjj889QiYu6fhjMIZw5sM6sfwHy0eptZsYCmVHt21jo5nNrQNLWUMO3S1E0y4IvSfSDLaxzb4qRWqi+oJbs9MOfelNtuKjVrdH79lZZXggiClKzO+DK1kfBbJeCX6LHR/rp8+w+Vv8ureoO8odWWs0Fu4v/5ly5snbXzssuuFL0Szw8qpj4LOX+N+h/RLMZ9S85JWiVTI5nNkIXDYk1ABGy9OHTsw2eVEMHbj48gO8WqF5dRJgBoEwoCU6GanghUdS49R+rpPAbl+5G9lXpLrB7DaSIQzH6JyNU99QJbzk9UwiZ0OAA8BmfmCkrz5sRYrKmvwSn5rFnOHVDcyLGd9fE3J9uT18hEmO9KUz0qrLyPe+EcN8Rttt1ZgNPDZd3d2av70/dptCv+NUNSOUrQgngdDpBAvO5tC6Iepn0nLBof5sV/VmOVs0R5paU5FDFgpvh/pwgQYNH/6sB8/k31AiGBMQ8CtuF9cA19+nNiB+zlEsoAoOGczCOf5YjhPytOftTatjUHJZROVHodEQpiM7H5W/7kYAgeEw+ZA8eFsM3r75jeg2WUPHScn1BhF62+5hVnNF6+k0vmlXtKyfTdgaKHcwEFW5iUsSq4N3BeeuMnxFCAPewitPb4rWlD5xHbBZsp8cH0rtfqYPAz137pPKg25Ayz24keHKNj28h1qLVUB8Pfk72NecRtfJCcEmalLegJ4Ep1zrOfaauK/R9ahzI17CykXmRvdc5Gwvd9AkdEkMA4tqPUsUAlp+zE7ywv3iZXuyeGBQN48jGglIKbiFLp0Kruv3/z0PcCRcZQOZpiQk0J7bjFg3BJI7QtLuK2u32+IThVbVheEnZeqxaU0EeLJZAs9NGTIwEq4IhbmFvv6TuPMP6rt7YcHC9gFTO58qHRYGFHuq4WpWJVXuhYbYH+VWTFixw5huMTQZQuLTFg1MRmgs21WA6hNk0uVNWoQfzBzQQ8Rouz//oNMOmPJIl74p6RY3TGS/5u8T7mg2bq2m11evmHg+jn+9ENKzOHKtBOfF7JhWV6FvEOXdCAzHa7IWry6tH7U/vzbvdm6o/y4/sGOU7Z9+1qBmUPjB5o9IdLdfKA5CIPFRHDfqc6Xdg46V5brhxuIlhu7DYwvDqqsgdXqPE8hknD/W9VAEGDLliPAPNM1BRnasJxOCIYyaV537ki4+qi/GjfCxMpIFVQH7W22x21OOjKzdqTZy0XYiVWIuUijKMHM0ikBo2WjCK5/pbWDySTMuC91Qs5n8bYv2ilz+M0Du5R/iXgYWdE495NUlO3B3F2eVgdOFVDN+FcRFHGZGF4xJ8QfXBHS4GzHQgQP5MpFNWDoF1ZdOq07t2JPm1XggDDEao9+yruemQjAje+ANhip3jhsgskp6SAtxybYd/Ak6Di+NrhtFYG82enKU+PRc+a454YVsNcx+LUuu+HOHsR+w35zrOJiehbbpMZHoG8tHdJlXRR7WFEBsgYNW/wJ0P0W/92sTywPwTyxFnblogPYcP/tj9z1mXntfZDg+PKkOPUZkGgTlP9UPQsjIp3f7zizYJiJvffqcNr56s43SQc02nXDl8f7FjhJ13YycJ3gcrBp0mIUy25eVFfvyqB+gD8OLy7EaNaJCiS2P3k3i2w6HvG047MZlzL0M/wYuzN8XG4ZJiao1N5EcizZYwTLxAJUBNdzC1BnkfD1yWv0Yz2mMnQy9AjPbu7CbCNmXWUM5cU9O4w3WCeDgZMCN0o4//XK7GOrVHkfq+uSeuVVj/ETrYwpEcyqgnsVSWqnhP4W47aIVFlm9KNN1gtlc+1iMV6lTsKRe+rJ+RyInzNqnrpNDt9xrYGuBxTHWIE/RyqjymXLNRcA6GZ/6ZeJlLw4iRcd/lBxvkwpU1zhfN7JnU0UakAAJOqTo3xNVm8Onx6Go6Ye0R+GuNh/DBORlhcovHCdvHUNo0wLSXBGVhGLJBTjZb3injRjkKOJS9MKpHohj/laj+HeLURxk5dTC8GvLRc41dLBgnAGkfTeWrKdKRtxPCXcgL53xBI7i8nB8izFhX310Hxw7asaMrzt26811+jFf+1qBn3d8XNZQdFhWAjEpKefxDs7YA2b3aRTdW8LkShZx56eWsEIRcoga2/bP30Qfq1r/OyVTXK+vEUxSUcPqIEknoLClKdHWekMkBur2p88W5e5d3PrPlrpzCo7a+iS+26J0BvEjwZeUtM5JvLP3SxYPUaAu5cJpATFoZFwJnXChYM4hRuT9z59Zlb25jXYLwfNoXeN30KBkCaW6ZuuqikcE3kuRanR111zOFpvsqmPJJi61H8WOnNJmC8gAN3vT6+HJ7nPVEcq5JjJgdr5/Hx0w/lsZqV8Z+2+ShfGbFAeGzDoQ4SQIpiIFNBSQZNkVCNf2PwG6eXoUfRuAcNZNSLV9TjqNH6ORvzPN6b2x49bcTlfOlKev9PkQyLZwEMvA5bGtzz336Lq7wASKz7Xeo298xluS/EWdJLl2ZuL9u9ml6etlfRRukdO60Zixh8qXf56CRcIRyzDtD3W25+L7oA91VcFSJCNy+jRiepIH3t0bsNqZZiDNy+MKOw+Jr+6ijfYRli+dOqYZXzf4w2BAdV1Qe+axuJ+oCmRhcoy3XuxL5BeXRls0SQRrBrPeYY8YhkAsOMxobrzX/WGGICLmaZWhIt1zeofCr2t7BLQFTmMx+IpnLQ5UnEkH/qGjRh/Rskzwrr+loxA9zgQvJPjAkthWmJbVgWUYTBRsFyrtbiYp7I3WNIH1ohPh4czFZqg/KUVRhd0ndbauRX2YctC5JJ/K9OvZlXSl3X8P/g71tYM4xP8jFKH9DRfzcUbP3yJv2ztuSeIOH1nk/qgxsH2aHWKXqCo15QBk3xVYZ249XHPcLoAZqEpTa+kU8vX7huh+HY9HrRG6aQ0o6VNYXoo6qo1Vwb+lagQ5PFC6PA9t5gTL1UeG3aMfFT8n5V/H2yf3nC0oChg2D+tFOzH/CjxM1bouYgSPjrzf17YY15qahBUIAozrosyAhQhxIpfrE15qurJ0Ey+gmIQSMxKirqomIhSyJ1pKLFiZ+VBFBKU+6q2zhHdrkP0mph1i21S9+YFAqsEu60In3xL48WG0RK+Rj5VLO0mPSp62cp8adyTeH3PI4o0JUrVwp2h+AmDJkliBstRzE8+n+nx5lzyLOsd9EeCwpKOy5XDBYCgdMYsUEFMu0EpLcs8wrG7GYcvQGbcF/sXRupNLhd/3Os7tLoIM4pFuSgET50EaQOgXDllNcqZNW/0y5vK5YAet5BfEp3Yi+xyiDFpO6nqtHsUVwRBgj1hhuocYMWaYDvdXu8vptw37qNu/pKn58IFKUOSc1U1pRUTS+e0bzy4oyUnHxpB+zAn5Kl4gfzGplJ+3YJQmfua0f5ZfTiAHZ0MbtU5To2J5Xr0oDvnh1unEJ/a1WtH6KaWS7nekhL1ymIYfXK6PWt5ChKk6OY9cGE2pg3K1occruQ9hmLrbKDiC40TTL3NcQPW8uqi7e1aW/eKwnp1NdjgJxfT9OY3rkJoQ+RQjuacV8DBnEd83wBeyCmNe5CDK6brVGrbLWOqVytUydo3ZlUyQ1Tvq86vvF2T7wZCMik6cuDuIcQgClIKLxjss4+wbzF/88+JD76w5RTCG5kxcPpnpqUTFW5/n61QrfxQlDicCql2OpXej7L7KWPUGBgyuuleJB+KjR1H3ANjX7k7GVHqN10S8yGol5Hp6d5+dwiaQEunCeKkwE3fC2kw0JcgmawulfFqnO+1cWGu0bZ/Ix/satrn3IwCjW3gdb0+BfhbiLSHB0gcIKsekAqfWwaqp5bn256W1jYsRphS3NRjUD3Kw5VrAdBMUZRVb4QZ8QcKoZME/WUF6JLlX0/JIDoa0wwMIYVZjhAwSCJ06I1xzUZ7NrMtrTS3BBeUl33+btQxMnjtbGOiyTc56bW2YPw6phrZ3nab3HpQGJrb5EHjQpsIL29Ny4H/9AI7W+ocfzPug++vlRgy95VAatxc80ojbtpFgauunAOgCBxSMAhZ89sJRSY5aAn1L4Ik6hlc4s7QO9dWNZSbJMIdkX1TJLwVtH6J3PZ4vu9A1eajiVqhSdXUGf27Fc51vQV35YOnC4elWZJ1Hu25YOPPaHMIimyux3ZK1PQHbpUgqTk3Wh7AvXBU44dNzO4NaOnN8bTmnMTR/JXnhTGWsgmabiFjnyWaYiELgppS9ry6C/7FtkbjVcLKhzaC2D/3qhvFqJQc2EV+B1eYhVS9DXI7GgylJXRd9BPtF+ERk9Pp8zFrVJYfzEog4t0dkne9/+h9cTeS9HkUOP0P+iUOXR9wUO4OtFScGdpEs563P6rT+ogY5osl65RsZtasRcNB7B9qU5j1MtoPQRadQ/2QetoWGaJHpZLIVHqKOa0mycvemMyBoqSlqq+ErxymZCUhkLum8AizcWfIK1xsmytLCizRTOlqjoiBpDle9EXszHYcK6gxwUkd9nXJkc//Iihb+BdoFAblTEI3rnJU9Zva9vW0YTB2fy94I3iCEObTIbSgZhWdC1Gzt98I3NimZQH2l3Wle5/Qo7abcmJ90CjnW0mruybOhrKAwOtTDZbsS3Wu/z39Gc7v66fs4nePJwLCSZNdUnimormhQj+bp/uF6p00xuIBFgZIJEj9ulREHCCOOds81LCQN/5QuJkBYvmbeUyBVTJv1JCZ8ndY8N/tZLFhSPC3lXjxmc78pJY4XcQRRBHIk4u/1pewWKIaIXstbvb7VNtyfFJNbsvSNSPF6Ffy140xA0zyihGRxpy4inXN7HDYJ+x9CH+D0t0tbTM/anFBAgUpNOgGHH5lrMomsuKOtcDF0fEXevcmnkjKsaaNNfXda1C6KeMc+NMs7X7GwPVknrAzjpQO0o7ak2YV0gAWClF7VQ+FuTSskh5g10T3jkxDOLK5OF3nCDqSCBXlKGayL7qOeQtsCpaCDMsCOajnIdallQa2jWvbP4Hcx2BlfbX1TZNma+vjb3by+uBuo2J2ncG0n0zwCsPH/H1C8PNe5m9YWEzBHHoWfeiiTes+sj2wrpffrHJ+sbQMbSbq6nWGQ5Z8+9whJluuabHk6ri0v6u/AU5e9UTDrwYN+uSQTDR4ed5MGIWIfUfDS8OBQ1JJZ8drm7wddFTIVEYS9HwSuSJhg9D0acVGQi3PN7vz+d3fVQ/5ds6X8PdTfwYi5JOQ5YryQtqGeASR4+8D6d0N8V3QfZznbXO89QC245EYM2RdQzbJ9mX72hbmUcorHKCpA/VRS/krcBxB12OLRSXZWZ1GixZRZLcuC/8wboTPDL5nIa2Afs1dIi81gbpT4E87WbckuCW84yrkZeqbdw7oKtths89tlXJwn/Tl9iJyEntuMeYg4NLa/FQ+nu6kq7B2ol2d/oBm+0Y74z2FbOccotQ5Hb48ZoRRbxZ9z7a8J/fBi10ApKVcteYLNd8LqOEiXbEHUjT0ut7pR+eLBai1Z4eoDREaXu7znoiQ6G6bl4NnpRxp0DE/WAkd182bBHHNGU05auFIXd892uM1rq1pUnARAgib5OL2XDfZtidH59rPoK6cIZaK465ZExyzSPBJBInyuyeBfOkC13BLRB1t9+Oz4lvcoPkhbMuRaWAM3+Wf12Fh+5CfuZrxAufXCkWgXWLFjJGPLuek+3cCH6CMW7ZzWNvsuJKDOfDr1t0k2a6a+qUjibayUwJannr96c+8bLEtVP9/Q0588nmDN52vgPOv7s2Tb7ZGqKzyLK664fhAgKigiVlozsi+ieLA0dR/7MP1g6HW/GxZoBBkTO5M/fFOkHBC/Cu2sxeXWkjfauK6xoopyFQQlZzGOPgR1zfHVJif5YiGUS6TlhJ7At0QRS941c2WynBfD42Buug9te1mfmrotGNLORm9fX9oEl+j0phzbhxXODFykit1ZV3VsN6ukQLT3p2AhAjIBQXhmk9U2mtlxP4PHQbOtrqinNFI0uOcKUcNZMCUUgSAxQGbAFv5+ekaVY4W4TaQMT+pYHnwbcFdrpGWLV6ge9EyIiCmV6yO0OMES1DhWk2HjFd4uS7dqagbBjwv68gf/3kc7oTqkeZGzuEAQ7VOsPixoVg7m6aeFHhu/AHEbsAZWgkhT2l5kMCGEeTPXkMPIBV9ttVcLjVyfbgv8D/6g6FfL8+I5TQ5p4oO6yjAx7cGzUr/MiUt9L/Lw0EG0OVe+F1KfzVVe0mu5V2kqrZFoUENxgZ4ICY880lr9FPSnqnP0fW1VrBPeR6MTp9cHKu4xlHE7LAjDOszrOjQhFj2PfMn1S5bJ8tWrcwrC6BaEVasQypbc9xvepFTlNeziAZGgxNgUpeayWlMheV5WfEBVvmEN/crdHNHltwQePUHv8ROewdeZ+X90+sfgfjBarCCrvY5+zKQ230Hevrp8azgbJU8WxjptLusCyfNIol35gNWgCd1/NaM2HMj8KOOlrdvO0imSjF7p9Zab4uNauqQhyTDQ3JGKwNOCGamVtD4ohv5DnhUsLEjRVV4Q2LMyNCiJehuOio9lAmN2YeDlyAznh3ee6HzBhbuNqMVOblGfgBpUtWrLFToAUjPiTmo0zPsnsgnUyLgt1ujb8pBEY21RRR2FhzapJArg8Mcc87i8hIstikgxRFZL851jiJEL7r8hkwnrT8OmrlqL4WWbl9MCAUJ8BMnoAy3S7FvXloXmmDErGGAnLzzoN1ZlWemu6ve1uYPh3+JEXIIaRMlAkxbzUKJ/qnvNQOwktF+EbZ6Et702Af9BzWkCDm2U0o2UWkIFqEjGri7Ao9D/zr5WIKYuTaFSRmU3C46LzDBKTJrjs88LDTphGW1Xvr9Vziqtgtwv0ayvMzdm7cAUqaSZcNRkT9PdVnzJhi2iZ+ABcOMoFbjpLmVtchAaZFsrIE4PpLFr4KSTwvJFS9K14iAUSyXbdMobll+QyRmj6Np91sPjYDbY3isZTtM0JCGMnwc3SrpcJ1bb1VFsfOxgGyzEDyJScMVb4lxXOMmFR2N0HyU66b8N8E7FmZ4xE1C9sBuLH4AkZ+OGe7kLScVbeQ+HPCp2BMQmVXGktELJG+1YUr8mY14AkEya5nm/nHq8Ur/mowh0CG+ZCP1ymwPrsyFIelk9iqZAElNXiLEeraPPi81Qg3ZKnUiAw69MXbH06NZqjFiWtfWqV/ahqUZ0abhbi+Z+pT/Uwhg3+s0jEy36Rh5se3jWLY49IDWCRW/hgsDqDqrCbEpgjbHByglbBo+ONWZMIv7YAXZV2dKYTx+Ba1ck9eCJW+ZdpVHNz2jXJ9ofhIGetZdUlw7IB+cKaBx3dZth0boc3kTksXILFnfuflOX/EGGb31VoSCvH0M3bvsnUb0ex60PzNXzJgXvFbIcFN17ZNE9kMPOjoT7NkuNqwXD/hqladrU/fFHwRnP3MQbggSlTiXKBUHdGogLVXTuTJLrMZYvPrwEe5kuDjaduQ8PfARZwnG92qj1XAbZ/NV/IQOcGV6FBN3EludHife/EPyEEgfnhH+IJfHRtqm25qLUTXqWatdZ58GG+w0572frPGrR9RyOPUw6VYJZ1XVv9cqOou6E+0YGIJWPkkXAm9ImkrcvE+rfIAo3jsSB6HId92KD74dKj4zwhPtgR3R30UjbDTLtH5xioJZv4b0tyxUDpIu0XuROZ5TNbczlhGV43jWrveWIH2cYQfwVT6qrgB5TL1Dt6rjbhTV/CgLxT5WV34UQIvGObBmcw/FmQQIdNHncVaKsZ7Zf59uLjeyPcYz2GTnPJbWXBQL6/HYoEWaIeIppiSe0G6WMHyk6q6o4rUMvFenSS3HD1Ve1qyL7dboW11wDjbMy7Gu7keMNWvKpqEqNgsLTK8vPx5cL4ssI3u2boW4dxphZ+jT9Hjh827bcSLymZlY4C38at+P4VMO+emY3l0KoDjvQ9EgwGLTt8zcbnRWFCzxu9+x1xeTq/zMTptmnlwbYQd1uiK95T/Qst4WEMYiSetlqx0svQQhdPmitvTNw6QaH5rtx7dZdAOGPOEtQWjTN0JjI3wQciR8TKJr9ph6GlWBpYr+tR7c2Kp9loYEMM6CjLLmlML/vHIyRMvfxYLiRTLdw9JoGUEurllexfpedMiIyUkPmZ8Nfl+k7Wm9C5Zgb+mAtMNm0qcQzKERvGbZp9rUVUmUKHlPQW0K1eV7Oc77O9kM68bydFpPLOEMvwMc01VfzqzdiweZ924yHAmOZ+D4a1/AB14dzsgpcUGqXjEih3nqEh5WPQpeokXIMaknOsIpIibvJWisokEHZ03plreDHBtBVEv+qHTyf3Y2OGZ5td+g7mHq2YLWfWJvxrWdB5RQly1fy42bCje4vtUZDXZBiqsITsTIkuifaCUdQBZhn/e6prsMG8HlMdZsvC6fTXIDmJSDo+Y10H179NE8tN8tufduETz4uHaQNIUMJOheK2r4aAcQNhuZB+y1Kp6FY1MGNIVqRQW0do4owNTr4A5NYH7Z1IVO943OUGhhP9HcGKBT8ctXiZ3bgKI0LBpO0vtw2Kah7t6SkcLrqleijNUDB0GfAz73fu0q82WAKsM6IBMa+NEmacJMQKf261TQnLQN/VsTqQtJ7HbpK85La+LP4hA3jaNg1wa6adi4/N3gf9/kT4+0aRh2HfXEZ9KSkH621LrRFbEDFTebQnATMWk9qfZmcGZWd1gjFrSWZiCYGroyUaozF4wHbpoIkwQOlugNk1wlPd496J1geZBLkwWDBYT1Z7rmWYzQugEPTkU40QFMCGTjbEeiDjO4WvySJ6Qb+cpoNKOdzj558D/RlptmSPhX4fHEwU5NsBnIwPkVianTxXw9GdtHQzK895EiXNplf4tpeisoRSZCrUmLaSX8OlwJW0SJjHmDgTRP5mHnpcC43LVzEUfkimhaPLZBQ19zmcfvvVaaf3YoEa6QaqPeza5/jSeW1sMr6+ldIHEy2mFTLbvQMmXyoM+QryyC7Tvix9qB3o3yiYZpUIUrNqKv3uM9FKRKrf9DxieQ8wzUqbB6eo//e+YvwpHoFgXpOZM4sT+GOL4Bi7+4bbdewtZoKHj5KZ4KwRY6G/aSgO4yrlq6RYy8KfV9/uxJfkO5JZfk8cbGjYqGjSKQQG90psM6L+JzGSnDj6Y46ueuPVSucp9XiUhfKlLVCyYltbyAPTzhWXvsOQ8OMPkLA9REMmZRsnWq4iwv5Ebn+BEexHO855hpb0zae2WYebNflPyWBoJCJwWiDR28QtAiJFnjF1BPt6iHmLBHweDYT2YQOVldXYeXZ/XXfR0Q7OKf5aS5lpsUkinKi1ikM3UQWJ8nBmdrW/uxsjQpQMEn4Eb0hV2Ooms9NzAb7oAO+4+VhvWSEPrQNMZ/Gp1tqbLmZc6iiN2ZzwxLdaUmr4lAGpJiMDv3+kV4bNK221CG/amhTx/DqjhEbJjhMp9AdpDIUOiGFVZaYLWbM6LTlDlejMqeW38IC8kRq6YA62HcTOuo7/EjTEB1GhTnXCOjWYBLqBKI4/6PgUaHPMSUZAI7YmfxGziKj68UGhnKihI8OrEVIXNtTSXGZbDQn32GHl9HjUAdnpvtdxOFe9mVArO+OzhiyFVRkU+vENaB9DozPDQdR32AR0msojXtuq235VP6tZIB6xhPN0NmI6jZdUtIX27XrnB706dctJRt3hwiewkkYoR+bwhH4RrP7WGXRPx7/z4BXUI/Z+w0ia5Ha9TAVtiU7JQDOuuqE9DoY7oOnqWsty5/IX/qpMYpf5T+nhsQFpJU/fStGExUecSEkClo1devW/+uayi8C566S2kYvxi+6wpb6LjobBCf+SzHHYZo3Vob/EPbw6qZDkNW9ZG1iVUMe6ZLa5dikQTF0RrWQCSfLyJhZWKKelkPzr+usIdFVhyfnqPWCsRFo8E1r4Wi/VKhSi4YtGSaLHYJ8yrml/LIvGe9vlrIIuM7QVXhIkdL4PBlwemIoyzeZpv6ZaZpvo0FMmtHH9wTMsI9BY0bw3SKcgRj45ukd+Lu1o5pmtr7QQn4aKExpOwuBefgO8+g6CX/CdLlXt6Wj5Q86/DjGGimJi00wB4E9W/x5bsjrcFOEHLoxPR4bC3k7DDXobQGud+ekEQwWFbMxR09ZG0mYT6knjDVWVfYmGjdNoAvpnu1CwYarTJYCp18O8rtw1DZB/OQczCDeNv0dyeIBPbWudx3eloVIwjdCl9he4gmHTGN0U1gZxXEMk1qUnqJszD/OvpofrjBzUgsyMlYiRvVhgeLWeEc4T1vY+wZwYfCgKUM3x8mJ+m1IiLbrXmFWOKPeiAWKgn6bW5zzbxSKlAXVIuD+VJWHJ/rnBxeZU6m/K1Pap+2/+rFIoDxw0wOkmiZwZ4keedsLHcEWgAvzyoD/+SzZwjgdqBZ8R92XOtavo2inZ404xAZk765oUVh7eBzDJtD3XvBmVV74GT+dpCfdgekYFvpWYkSOcsxU9+er1mb4apBk6/bDgIm8DQR+8zI4iDBbFoBiyFY7I7U4Hrthikprp3NJqKHfB9H/ItAF+zoGqYCQWxNtkKoisJ1o2tfcWJUCbrhq5ERxbevGU1BeQaE+airNBAHxewql/NOUFl6Pxap/8FBOV3OSjkw8XT/fKgWfQdwZTzfnazS8Tuij+wfdR72DgflxpHAf5ZmvoZEAia/uABrpGAULOSxUmnT0abQ0POGvJwj8VTf+ZTKyfXqDdVROOxPvOVjd1j/m8eDNdavGkw/B8lVJjF9oX7HbM6N6zkQnxUTx/eGqKv5CR8iJUMvfw+scBHfphoH13rWnqtVOzEhE2aeNAzJ2LP8ZAwiN6j7hs7q4oU5R/NomHfAGAFHlVuKYSe4Oc3MP5KP/C71/4X8bRsAxXwTAeWwvJ4WhQaYwd2kswdNteAF3Aqo3/yzFoOGALckQ3lv2QRMHPyp6XEProl14ql8CujiOysFdXliFRNWSPiY/kvmFnoZFZZ7pA3hS6mxeFl1GQqW0xKjp81c0/ajPIlgyWwg8THfG7IrUBbYMSoWVTWETlx1L/EJX5wdPe6HJPQBsVU1vUW3PXDllmi/ggiY++/tutbSCdlbkigkko70CPg74pXInTaOhwbUJPXt5J+PIgypxRkbJf4eNkGVR7U3HqHNST29vO5P51cNoXzY5v+hctskGh4rBMruZ17y+KiwolcAKqpxWvf0FiU5uL7ifXXxgKJL3VdCu32+k0M6bhYjsrYGfL0K6StoXA/9m0Oj+tYV1bxPw7mr71B47TDkvwP4MA0FyOUIyn0tOsMn4TE9sq8kWTpcTuUmpwrdfBVBeBT9cr1pfYdYHgNl3lKAytHsNi0ccu/tCm5UnJwKex8TvttaXFglC6WPQnbLuFPfYjR1oUKI0hJ/MKuumzarGSHJOht8y/F+AK7KFe81SjT4UlM/ZnVXIc2gwuioY7fXQjYEUUeGDi+1HdW73zmwUaiH9XT8voBX5v7lzvAKybHASSQmuKGKJneWWd37rxtwAAwvPviE/iEONqS5JzEx0rT+NFo2qRxum/lCS8w9BzZLGVE6RfoLukpL9eSOLH7H2LmGGn6j6jnvZajzZ/Wz9eCYxSW7BbVuaCJahQXWj+PBaBqwDHVIajndhFbrAtv/6hPDHsb3X2PpFKO4PEYaxAMptIqkjzpU9LpDf9UfXDNHnJWDC6qKZiBYLiUvutMjrSLmLY0fUHX3ACYT03tuixPBPOEKHrJxXLFhz2a587UMINdRGl8OnimPAVZJZ3ERBcios4BbGgs7JQpopGltNNRssMDqwgve2Uciauy63ccE9vwz6cFjHbI12Z2Ah3bCJQlp6YuBoOJC9JUCPzEZsKDLwNfMq552FUg33jxSKVfun+D+FSwMMVyLndFikpnzRGgYApLb4R5s7dkHRomt56vREY53UG+bnNvTFNyzCHHWWMLxJQt8eQfGrVmA5/RquyROERqtfdIXvv/tBoLhJrWgc7tPT2Jl9yJPcY4RjYSKYCtm8hkVSdVB0J6oCY/Ywy/cjGzQ5p13P4FJCpojQeI3dVjLYEwX37Byr0riFP9hdPpdzkBwP6CUTKBYh/UKNzuPQmWadURWTgsR7IRudvH4/qmJ0HWicQtGxulrNyvFu9jYUA7CQMnPgUCuczGupbUtMTqgfDcCFO+jqwxtpQ5o+enYOfK1WeYVojoG+y7raBN0ah+9az2A6eENfK22LUoiA+b0oZSc7sdnHvwqXWTF6SEsbmcGn4GGQNWbJSyJonY7eqfiZQ/NQr6K+lYpXJhG8kIx9zfGPWfjl0igHqrDX2lZnQUKtujnw2siRfralOgdvVPKiKGf0zJ0sqzAWVnnj0LrJIy2LuLmL8916EVXOBlbIdQrwcEVw5r5MnswMX0WDibvMMXyPyDIoGTTJymBqmMCzaUwYfA14b/dwmIuPcY1icjXx1FSM3V34Fci6um46TWg+9XIUAq5TYc9jKLYMz/4u9Aa02+Ko0knFF0bIz5caau2AMYtljCKR8K/6Kr4KB1G8P9CWX8i3p8d/PinmVgs/QucDKPU+usMz/RSbrubBA+U1VJki0RKOHClNzlC6ZcHLCRIOYFDyWpwVlTCpac7h38HLgz7fqsw+dSeEV2ULiyDUkHgwFdOL+39i4GbrhbQQmn2fpIrky1jDnAuqEPAj4bBRKWkLe2xcnqjyu8SaagCCU4RjCk5DX2J/whgNXhJjIA6uPfS+QM1xrxmRWL42b8QkJ/YFQbtO7CBY5Zz5GpsDVH2qWXRd8nsSEbxxFnBUszuaaFAYbfD42+w+9rHrINQux6pFtPShX9bkv2LygijhhzmnJgfGHyIvAtVr/zXXzkp+n+2s6fvPAQejRbAiJwpfAdrk8rN4z5kvU/wzqM3q0Zy5K8A1YM7JTShma/I9d0aFXO33LwXf7sigJXz//ZDfBFPhwDChlE+/vzB2WBRsC1JdeGHjWuPOPcQakxwbyb6LLMCGCeKXAwB8K7hDdQSWOu0YFosHrPJYzlfSJ6Tl7twembpiVPH1rAeRWM7b8YpgbCvjSwMFp4ubxvidC+4uYCtVJVouvX5c4NO6l85nhs8yl4p2yxoMDcTqUnu64UhiiREBVQ/hZGHaFl0ajXYYHss16Dn2KGNQHwIIRCjJBNPm/+Fq07OYO6IdgNre7KrlE268wHA7u7kLCBoJnaPKo+VyC+/SVi6Jsnclbmd4VdhZqAjRKJQ+WdY/RIX3I9EP5CaR6BA/3DNij5tKL7C/tpXeNTSev2uE0JtGSUs1fEHKHcdtu/0cQ+E/EdOH3itY59jehlZAsmA45rpo0ZLysCsdb+65vAYy0oQpXWzH/Az7ND5hrhJ3dPoj90oirYrQqfaU5v3rc3TRQ3/bdCEb5wggyz3/HTJoRGwd0Z/+0lmKsY56MFR3QY/xDEbxXQJwJczOVujAZHoR3zoH5kYvMXu7jVTgWuhR4d4XOXjMznR/a/fZcb5iQRSWe1M+e/z4z2Gjq2TXdtnuHmW9J7VKeVE3skPIxaDi6QemtMMctaEAuUb3yhUesbVcc1kMDPQ5FYAFmskE54nmVcXzyP8MWHGPQeuytr+rAbrrATg12xdocUvGLR/5FSKZuGWSfD/Rdxy7iztmCc/g7kP5E2pVWf4ckR+OnIQhmUQSNj5Md7AL7LB68YCogFoHuQhV7ImxzA0Ulylk0UFs2aAf/mKpMRjft8hyXWwpjW7OO5B0ilnTdLzofqiX7QRoZ+4tjM8rAQP+PCaAVmyg6x/zZftlaPZk0olF4fZ7KFsbZsw16MNDEJGLTniRql7bXKJ05R1CH2k2HSKUL572RI90hd8OjdmYnm9N4oVpf/NDPVbdsiSJNSBEkr5gPW0z+a0klY0CPVNFZIuYRyovMt/dunWnAc1KIE6Uz9y5dqHG2whOvDRXn4KCe3O+rZfRbN2bPNHazqqeXQGX/dW0DID9lUPXcZGh3fwwLfESEsDVu/Jrpt1GnkdV7MBOKgCLqx+A2kVf7go1NgHzHQ5JQajpltcMOcPWxJIcAvhuWcjQyXS+CMLEqx0jgUOFK55XxkV036uBfh7GLWyvdWgD7mgoaJD/3lwEgPjIeUi00mx1i0bXdohZArHxuiuRFM+m2pYCKWQB5zOAlrdtmGvXquEqzvW2uZ7krL/bQgcqC7ZYQu0MHL6S36488oeah/lqI4Oqzeis6MSRSMJe/nvGC8AsUOEY9V0mdUo2lJe0XcJqA8EwyhqPwokeXomz2A8CUzVNu5wF2W7y7TvIrPrYioS+p53B7xkiRFz9EzAAQ7z6yuOzigU0LTjDW6EEKjI3h5s/qhKoZDZSZRqd9s3b2DIqk89ATLTJUwyvBrRo4B00zYokn3eLRBJWdRIQ07T1K1h8TEQZ/9LerjyfspxxmlP0gw08LAjizNdQQ30lOkUsiW+Awlu0pONpWgoNxy0tK5ShJHQj2QSYyvhJA4LyZDMNbSwpSCFp8wuqX15rOSlZNldJ47Fsd/cZ6DZHGSzYbhF7JwukdopmgdZ6p9ByzdM+gdPcbsLfPzcnf/PxvFlbNlw60zkDBsWvSdx9erCZ6DNkML4/xzcc4EceDja6gvkR+viUW/Wl9OrAuXYCSpdpgERdSODq90cM15kEml1xaLcqSWSXHxpiLH40jCJdnAKd46EkiSCB5+eZNiG2TV35eLG3eohgOu8odfYP+SrMSzP8oYYUgizIzx+kchE4XdLXE/HgaZTa7lhPkuP7UfJvMcqc+imbqbaeVc8XKrjboCP0x//lYo54pMR8Xs/Q6Az8o9ZyUZJ9PBtHzjwXeXm/uwuesvKKMsdKkAgAjbQATCkGKJcE/yIXRatFDyMNIQ2Za8jwM1qWPUNJzbFmXesr5rQ5IBPR+tVh/QJvExeXS7hQ+o3Jj17iDjSS28luCOIJsVJa+/iSDAM8QByA86+eFpWZKU2Ix4spWvKH/0rqem/dYN+t2WmYanRokxE4+SEC6IbzBQW67wsepmCl4wCp0l9MuwF1ze3gbW5QZrkm+87WIiFnvm3qP+fZdJcxXfyaCEZtAf+4FkvonNP5hsrQqM0u6a23GzC5J4Jx+xL24gj5Tp1lgYF45CoDrW1Jde1AGavzWec9Dcmq5c68YxHTYSZoSV0025A4D8qY9WzTqjyQyO5lauJfmerxG9SVRsIlJbvFzX87KIHx+KBKQPReCSSIOceNaWSAK2ljWZmV+XoxpeLlwe0OFQ1hxEttf5a0+jzYvp3X2JMjyXHqoKXlD2RgU/h+V4Jp/kyIAkCo1EL6+m5L6Dndu+R0TQeJoHUhBk24asPam+94EY2VPDI5bFEAOAZW5uEimWIgh4jYM/O9B6bsR14/U2ToYDGPZrBCYwfi7qEtodf8FLBXjDGLUjdjOvld+pHq0XQZSpRrNiCqDUB4+MTdC268GrKc99L39ZmDY1B1w684VXvqmybnJQ5MyaRg1h8OiXxrbQh6VtEVP/ipMq6Ipv/F1oTXkiSeW2IBbAxpWeJbt2UYwo/QAMYmJOWGKn9dXQc4KTJtRH6E4CGh52UkYjNudgHZgPGAb+rRZCVjaFndPvFBavAE5SYznuhmNqYxsDFyx0Ogn8LJMnyGFvVIMajl74SzA7EuscmEgDdY+pZOSyFRsAQqdoodP2aidavc9hU4Rb5TgBhHPr/IugHaAJ4Z42MTmYBoadvJmnfjveoTBcbecxMXBDzsw5q/00y2NGpgCwe49J6PIbdkFQHkPWOq4XJWTSmE3NEt5GoKB12DIdh5fyp/yM9MwdGMaGrBK8qNlqNfRLjxXhicY9mkJSoWg8z42AU7pkJGRAcA3qTPpZGf3YIb37gXzi/EiJmY7mWLkTdi5lsO/9tCuzomRbKNu+SX1ryYKqWgWGw201O3pfsVJVE0lzQ5g3tblOhD2nije02xAD2HG5Ofb5RUiH9hjMd4O9//yYuNUmsx7tpMhfftyi4ocLcCpttuclogd+c8BTmgk7iMvBNQA/rfmKkRDVnkcMim6U1Pve1l8LeudNselkeS0PC/WNJl6QeZANWj98zKmC19p/kxma5UhCGdbSuOlF/nUURfr5RuSYESczeGPJ2VAGSFgPlMxL0AEk4WI8axOLi+D7St7WViKWPGrX3oOzshnO6dHVsNzjsTf7w+ZdA56CwgUwdKdq+aLKhCqtlF829JNy0bidl88b44k4aOVVHcn4eVgM9MSF4uKsrYD2o84GBICRl009gaTMRj5LmU2gayWchto7jn3OOZXlgQSAGo1nsCJ+S3JclhO2zOFG2s196A1O5ChUXOJY/JLf7pVEmcLGcB/5QKH2dZk5r0aPKa1mKqWh4dwWOaAhQKGuLVQW9a4VTgPH/tiqap7ofs4LfY6r0O86rmkSO5qawaehHjcNBtDEFPKniQhJpsdDHL9l3HojmBayqsDAD14bP8+SnHM/pH/v72OKSHpqhHSNeBRIoX1/+RaTcFs7EGkicZghCvtC40idvWoDdAqUTVmC7AmZ3wzGWNmK78VDf1x+bhMuQBN/m9zv5zasshsVnc0h4GYa3ryPFQoB3oV6Xp/YzQOliLmGI0KEs4lfMXzW2OqGh33mUiTmgDdsSgpb9vBvJiZTvbsj8AG+ge82QDA6ZPOiLZiU8W4G3u/e9dq3ZodLah0Z4wGYw1td+Ho+1pbC4MDgaWB6BAhywrgQk2rFd8dlPrPHtgOFZvYv3G6yt1wnUyHCqOJbwpYlPjCWP0W9P7vXoJpbkRCsVgPOp6+SwKOai7MwIwAO6vAQV5+Fk/noD9ZT/driWvoS8R4TwV4kP9qSQvMrBaoa1v5VjrWCBaxUJQVsbbcGb6QpdnzijFjPo3VquVvHGO7k0iVoynY0W6Ab0v1402OmAuYaIAVPMKqLM5ql22jNstPoM8FfNbk0NOd76sIdjaV+ggU5m2DuvQk1smWZ5wFd14HP33+qfSNORGdB3C70jhStlsmSjsS8OlKyXCycadnerxk1Fp3fXbLdkyd51kepwcM9Axg/E3gwvRvDtFrRQsgWoHz1VlKPF0FpY/2W3jVuxtD5AUAPBaT5LvyQOGGDVCkPlqu4BWkiU8NsOoZGREptCXWmK/y/Dk435SwcKU+Pcf9ihkXJK9q8IBZq2AfpRXW+EPXhjiPhUYZBfU9uTa3mivKdiGDL2vxtzXhwCUL6WDDRj2PMRJSApsBVra/UXPlA23VlYFmahIcHEA1YhLZRWoiuzBh5kpQXQWNdBImqFqCG32oUmrzan1co64XMK95LgHCqHyic2rxEHv7kZq7nOnRYnN4uPKKDt6s2n5PXWtIk7UpubIqpyUZQKWw4k53v2VVOXuSD1O8sh+s7tmLWLSz3lTIUWJoqQ2RN0s3L4IIEcR3bqwqZpQpIuE6WhnjwuQFgV0bsYuoGXfrbwJf3Mh7bUM5LcyOQZQhO7YRb27rrqQR15NG00BVH37KFMXGYztNkMdj0ZYZ0FhI1Oxn+6VuJqqBzCyktpUVUfNnsPvmnP3KeedGxGWCMZsSklDgxBdyOthqlf214JrubeEP6M3AtEDYCmUcan8yuuXa0GljN2jB5KXi875J6p1x/NgZo+YEixjiaG+AL25wR2rBkDwMhFqUZ6MmrKQ0ZE+N9dXPPiIXaxdZ2ON4oO9kSM+t+VzxoNkfM6En2mOzvPWPoJLyP1Ay4yzbXtvq+U4FrOoypApezUdKagGGY0PFehrAUL+u3FT/eLwiGO40nPm1VnuUNTQcHDxmbVcSihRR611xy+NRlqomPRotz/YuRTqbaiomAgwOhxrc7KnlOP/cjdXa0i3DWqmulD6k0XKaVbq2ei9z37cMn7c6CYwsHaSam4wFbheL7yZ8WFEihm/q2CkN5ro7GXinXhEHpxMEHgVPpYkBtLr+VWrC6uh945N1z420X1rLZdUSvgjns3yEZ07VNeptY0bdh+pkZFnPU+uVtxu5o5TaCIKVpcDz8R+jWiPieI2axol2mBzVuZXH00XM6ju3ghTawG7X+Wz1rSVE0eqq2p0RBGByiEeQQq7ZJhGeNjEKf1Ki+JtKQEGPiMiQVV6ItKpzLMTZMEEZoSYwtVVcZjMNSZaUclypx8DP+qUUI+FdpL5gETrmjVqf7kGcEKI1ncrFgrTcK97R4RC79hHh4zoTdduje52cwmDFKY/D0N+shGOzJImx1dqWoG9FdvAvlXndx5IJOkayPdVbviIHgrbkQNXDfVbpxFjt9xjMURgKrxFe62ymzSKIeSHvFJhjNbZRtXg8CdOADSeux9lN1m+t+bxbrLJ/BsRKuqPC8Za+97A4Q0rkY8ASIaztXKvh7E0VFFAUlhkxGatKXsv5jMEIm6oa24dyhQ4sTqblgoxGxVOh51vIz+XTCV0U79jjp7WWF7zitXygT2pTr0GElhMTxAcljKn7g2U5cbjjGLEJyn+o5DQvULjKhd+hVXOQlhGzkhcitAn8N3Sx9pcpwDlavBfn6xD2JZUwNIlVi7hmssEsL5jBTO4SRv7y+KygAI+Y7dS7bHu7K0IL55FMrDez/kfcVYZtEuAMKTjG5ksQtKiwSCpckH++oOnim7BsWFvez8Ga6eyMmCQyxYkker2cqHli1V9SadHHZ+eTpJet/6HWWt/TOgPUkjQpPmHhhJwxwTIv+L53OXJwnEebnL/sCgA644XptLW3Mr7CIkxRIZ2aDR9Iap2bjaJAsKoWvHvacztO1wdEupfWfg6KVrLhBjTqrt9WyLbIP24amRJVxxi6IdSHeYyxNzCfVb1UEhBeH9EvlO1FqhTDqytqj9u3gk0SvZWbvOjWspkR2fDvA+Ri2kps6Zn2HVV+GrmXTOg//hvNwGM+Fkp9FfHuyISReVY5gTphOlRMrITBeliVAjidzyyaHw7SNzl2jFqhx4HvYJx63J/0oxJ4/P2CKCm8orKPutkUId7FJ6McXomU6cLFHGmhwWs6/lhh7liOWyiGXVImwkmohpBSlRZDY36OwwEtSouJev0Ro9F1ii2FA0ERyzhBQNdZobtQo9ETyCiSBINV3MZCEXFHz6C92Wd5j90G0DQwAFF51tcaKVChLe6VbD3wwdlUILROqM4IEeg2jbOTRs5mS3niwR+j+Y7tngNYcbmsccOIUObzpZDkHyrMKw0MU1NdPS7Cip7w/hbL2cfpQOIL9ZAUlh2uxTIf5eBypnrv29L5qO05vIpCKBctZqQeb/AVGab83Y+70DzN1Mq9NWls60FfUW9BIbW6g+bXhFGk1nnvJ0i6hcAd8rohcINLXe9iDdMt/0UOCYXGDayN5wlgD++KJL2Fg+L5hwNAXAFywSpwYwoJukgJTi5d1oA6vLN9IOQTcs1Ejivisk/h4xq3ERjJGJ99bYGvzYWIvlXpnQM6pYpGqkYlBsRx3tLBd5cMT0ZNpIBOVKrJ4Svp/+NB8VFdhkXPAG5Xln4vJKGAFboeCNlbReEZtzBwDXhxBMkI8U1Nu3ny/GLFMAN7ZyezLxCRYEGPfncrt+75nBIpdzSHqeqYgCE0OrhVXl4e2Smy6jN96KpJkMUJMYVIfAc5P++H9U7fokaGGBXRQrhgGtZSvM7FJ+dK9S4Gra03Zu92UfdT4Vz6GJi7U7pJSQaUAsZmwd5bqlLF1nr6aedcq7zSLLXYfP1q9bzEMapamJqH0s+JZsXr3ID0GOCxSlDqdhpVPgkaBQezNPFX9NSm/F7CuOIX5A7iuBTiSMRB0Hk/gK4s/YlXQJIw1uCjUDd+jrEYTEQexHa15C3QMpL3GZt1K8i/hpTUILXhhmE4rimuEpIimCOjkmsFRCRG3HvgQLHW2T7O/1cKglZUIJlCyeA8Q6o/dL7kvyRlyTiOPCCl3FtWgPRnQR3T2QtNFJzBakBZ8rIeOLvG4RZwIP2Q0avhANYmam75lJi8TjfO2wAM9nZXK9nGvSLyymQrAHlGEsHh2stVfZFOlBztxn1hbF6cnjPt9CAPffC3NAD3CB94nirOrwFSoNAqugIWRih7OWVartCGL8pjK3Jvtkpb7hiocULb7ZzamFQmVPgVQysJ7tRsh8cEIhub2vULg5ZzNTH2slboiFoOoTgqc51iOfWZJ/ExpXCx5XDkRuPWViXftd+wYvIVRGBX8CawA8Aj0FYWhceeSo2sB2YM0jptD2zVRavTsr1JmIibTpmy0VmzvHVSwpsgUqsxiD30zKt0eOGZVQ1cRHIxrGst5Gfvh8qFfwu0IG84NUoHePk+83Y9/Eh3ly48CBgNy9fbo4a1xUdzzisnFzYeQPdckhYV+VBDbC8+rQgGzar1sNMiM03VUhXUZ32rJ5oZFMz5Xp2HzpCy0lCm9rswkMUlmaXEuN8FXV0iULKv1/sx0c7narlGTBQqyFnNLr8FNx3KpoKXcEbbutMbkZ3V8a8VneiWd9NApIHl/rpUOyllD4ClEPIjfHrrYlsWNR+7wIq+vpQdUWDniU7YXf449IdhN/iLOLzvcwrT89CjfBa5MjxmXJag5ZJKHnv47rPafC6cVuC6ORsfgnnPwnawxuILkne4W5kfGxCObfzi3D/nGMWWnMdjhXzmmvIJaVFFFXpNti7L20ro8YQN7ZGNCbKdy3+GaxdR7M2YAjjopIFuHOwJsRJFRvQRL5SDhOoABN8xRSZiSjNzaH9saKu6VA17ihxv1lX20ciICAOUtYNIrSojiX6AWpRvFbDDZOaJI6MGZMLmwygEkITCp9fqWUfRr2otxfBi/NOETORqyG5sTixUazeDchPhn1mQIKqXuYZuE58KkU0PDLetaZM7ZqhySpd4OUXQ3kKVFye9eE//9Z4yxUaaLHFbEIwMZsE+vnEcCyKReyUpyb3rx9fEujpRREADZqpC/C5eirdeI1xFv9/uwqdWpEzt5ziB0yVklDnZUKwdy9scmdYgtTYTF31Ma+WK5QsoF6PNIZH5P6t6oLpC2G/GyHp9qbwWVj4fCxHVD7GPSlGX2XwuVgcVXCEL6ptSb/0qfpjlFwhqX+NDJcKeZYxP0dfDQar8VVqW4zxsB6TcNWR8HAQ2F+MofBxB+VRj3h6UUBt8hwpJzrBHKMmSdrcNMCVWeO/AY15QkcxElH1eyrTe7/JAaqASX33gFKGXpNZvzkbbilyxaRxY18ez2oUlHek/7iGX89lvneRbev+bh5via9RgzIMuQA6SgIEW3L7r7qnPD35zkrrMZY2wiBA1Gj94smlveyyBa2yhhmbDoewRmJogzlHU21Af5n3qyfe9IT3uxQvRn0vh14nSaADMgncZ9fcm15EEUKXEvYLKc6Vv4RErCsG2l7gmgJ8+90dwjJ/4n1wzbx9iZFMhRRmRGUyF38cgYcLDxaAmr64FpfpufFQVp7I1I8rVqdSiXnFWNAnu33Pv2r4XNvcUJsxbI9CDz7r8xcyTKcDuiw2FM70i7Nk44grYlY5JPNoRIbx3Z5X4TeUQOLk4PfKWEssLxZS/yDfcZ1qgBE3IHaC4zLF/U3psQ1PKLRc9us69k8JjiBTzZ8qBoFPMZaVp+Bz6TJoxvfRm6knyJwuUnqHE11cIGnaqd/kjuw3pnduXaJNiSdCUPcPMiTXaTcm5sZeWR+KQaPrNSiaNpC/+6x2VD48ZC1bQdzoGNN3aU7HiU2u0Q5MIlFvdv7uMTmPzrGsdS70swfa3WSkUmsc1ylIbv41SpR8qa4MIEaLqAyp27nPWql22DSkPjfXJ85hCGAHjMxUz0Y7zzz0aad2/XJ7rRAllmaae3nAWoqNS4KSle2nVtbYU/vrgdG3hMI8WGpawNTb/7tWPc37UfPkD7+cT8wMXPsXoNovnyc1OVLw3SH49fujBXI4tXyGFndrR4HeDa9T9G7aFjG8ioJhKSKCZukwrgJiiTvRfyur9IYdGLrfAUrwZSR7M2RPvFFlST4JL2vQllCe4AkIB/jJxLopTYbupEo2Z1c1kUHGW0VDnmPy1RxUPhsHZtkAB4WSrDVp2pOqByRUpxeD5dWXWKyntF8Zs2Yc4TNSakeqLi0syg7Db4IfIjotjyl8/hIbW0sUXxqBBnzJeJIykoPN2rncfU1nOuRdIfb5b2wDzeIrL9eH4eHJMWnm/xsA6s13wOEFSFu7SSchq+T1mAyLaW6IEU9pwR+FtXd/aVvEXAZSwxVlMplVzAY4N4mTm1IL9zD1Gcjj0dX7KhIUYcnyC5zYcIXUf1VhCx/sfCSiqb0P4aOa+ihK5AvBA8J6eFPG671fveVNZy6jT13pwf3YhnAMX6ALHmQ5c9yysyl1uCW/lIVXhgdDAqsvbXj2usJM8zz7n2ACDuK0DYbVUe0HXfTgM6vIZhPbG2khtW4cm6/mSDuBGCshjeG3MX+35R4iUg738wJTY11UbC95mF/D2YAsTtWDzaSH+qlR/KRnsMwwxLQ4bxJYsW4SfV7CYJt3JUsRWojpxk4empad9qy90/Q5rxQvkiNNOIewv/IUWR2ejc58gy/rjDpyL2rhGLMNSP11NieJKbefcr4513NVMrtj1JmMMULZIlobk9B0u5bxQLKtavNVTmh4Dps3ZJVul7/fbElUS2HqnaliwLrUWLYocpx8K9g9KszSOGXOsuSfVncIhjQU9deMbM6t1+NekrMiJHByWST+9PM++wdttDKjYGGcphOe6DUGl0WqgB7dHZHXLTXKVk4EwCtS3na3aWqIs5YRHFzsMQsvYgabFdTWYyWG22+96WavO7Tqy9PDwRpESSYeL6SrB8fXdO4zeVpHb5ddkHIG+hfmuajM0DliflbiXu/Om+GWnHojuZ6fagsRjmcSfw8Lc978wW2+/JfVWu6wB369cqtPMnJFK2EfFAetQ823aTTV/5VNlxkBHMsIS648O9Z8Cn+vJisEKkQ/J868cb29JKBTozduGKsnBIV6M9J2cHIOe2e3dSaUQF1gV+d87Ev1qtjfoxJbuU/zDJIi0VdS+NUt/X7DtnwT9WFKTgCBIBKkm/GgWqPZlsKV7odnr1tskfyb5TH6GKegAn+EmSNoN/A+1nzeIG/0osHzTEK0Lh8dhY8vmx1ttjteSE6btZukV8vPsYUwzNbh4xyPGZaiG9E5zzwEh7En9vW3D6fjl0QV/wzmKGEGaXoeVWemv3kBHZhlYLeislcZsWjJtQcj/ExYH5NBnoI+jh5DddtE4+EaecpVbPTj5z2bVfWoI91Xb/Kc94aYKqmZHG4redw/fk93CihHKn1vT9HMqBL+uuX5OrHqPNny96KTZM2Laj4cE+vWeJxqL3GmgYvkPSRg278mBEkKdCdYDtf4W57SlDXadpUsZFrPGOkqUj+YItlnkUA3dixlUsze9NQBiAUL1uneXTFCkphL42JTmYW7WV9VXf8qMit505SNAqOM0prWRhEpreNQjCDZSsPmiXGhQTjF1S7+5zaV9b7eT1aOmwZOtBGBEHxnkjqoJoDuiGhhaVylzk6S286L7F/POogyXzZTtdxX/7+JL50kegJfiQM8IWcGLf2KI4w3t+NMz/Y2dzh5QbYO51Db8A0LGdCv4OrcNQvfY4+6R7E5hN+aQob5pH9Dc/29Tem0Kq2290BiMMqPWsVLgoakObNPrfeH8rxevqVIPQf9X3hPatVZyM+APi5hlgH1XsLQ0DRrePEMTqg1663X+MrPyVZna8Ze9UODKQ2DT0AjW22dmf3Bf4mb6nK+HUmB1dv/HUduh4nCuF6wmKq/4ox0tCt4xAagSc4j9A5q4O2KQ2qESpX+1fijLmjqSZ7juVBqdpFcrHqiUpE8nZgFGXzM++/ONc/spdGCTrZxTFJ8iYnHUD88Bbz0KQyJPTswVnruj7xA3CtNX+tMhzSPZ0/bo6qpBPtc02OD6DCpV0bQF412Febhn7gaBA0gmS0l5ruH/fO5vjEakU8ztXLxpa2nMxM1/0WKzvFZkAqhzgZMzeFOG3Vbi0ovlAGHcp52RuKL/loHWiAIJ7RPRYxcBnpLJQHM+lvKVXBIcDKfjtikJ4nptjzUDvRaPQhIdAsHgkqlMgJh4YBhCgBcsaGLiq8B0CHi0QjeY0Dan+IG0eKbXy5rfIhqbqBLnv4vX8u3XveexSQSePZXuaFV1GI1uFP5h0kVmhq4VkE+9dfBeoLiNJIVOxKd1tpsxhVE+QU9UQX0V+0GZLTKrIz0n7lsCYnGl/SydQzvbPpWTu616PjYUc3EqowngqWyRuUGOPtGHaMChwzDtKeIGciqkaa0Z/ZGS3qpO01tSUnZ6UhYXr4h2+q18M+LBRaTNw37h2vApR0pZvqlL07LEBJPVKOtjVUN6I3ee3R3ryzeTqJJHBXUimiWh0q0LYzzpFcNAq2JKOFogaMFZgcN1Q3yTVT8FLJcoIUXkvJOEiA7/eykPtLB1wi5DLv9YGKtJmz3KDtHs9QKZZd09xc2MxCd1IHB/g1gNLIqmED8SwaeYq2HzX9KBcXcGmAuLxdWgJm989qwMJQPpPA7joHXHQ/qNAoLT5kMouBLyfrZZOVm2+jqaS2uctXuaexyL+uH3fxOEA08LyQZLBwoDQWr3LtkYEB1357raoT+pOol/zoQmTPyYOPZMbCb0T+5sdpY/UlAWlslthss64rd1o/hRbIK6p0RyOcTvH4Z4+Oxh0rCHiOvf6aGDt77ZefnWF2iVHdc3zuxN2ZTgR/pNc1kB1wZEZzUAspAle1rQqcuJD7Xp8SfJWqPrMn0EbCmBAoKf8MYjwks7fOmhVvYjUndWQ4OH7JJ/gFscd71gL3GffaMBygsMZhYSqbzHKxfvJhs/0gxYTgBNJjpNA7sgUnWlutfeJmGBuTW0ZDGIS0VswOVK5cy7e34QotKxVP+p8CfekykqsDJRJmBMjpe4ZXYVST3eGKK8hsM769pXhwn8NUwa0AzC/SEuHtYp9syXB+nRDRCdY664Po4BNVgl4esL8RszqxpVN8+zCg5hjYOfHKRUdxyz+GGdNm5gZLb1bRPtQujWEXuZtUvaNdEv1uhof93fKaBfJ9WOygAxv8ypNQwetyVpsSJVkl+bOpebHc/mRJ+xQAKXOWanVw/UZdIO4QaN0wrhlCEOgele9/LQpqJPKGxwEe3GNXYnCrUDSXGl8acsJNpKs44yl4beBRAvkI9uF+lN0JUwV5Oqrkgq3ACqVqH99Z9fvLnLE+7c7FKFYWh57pLVUwdQ5+gnCf1QtOMDCew6dUqokfJ0wertXufP44GRVhxLku2Bd16VisT2DixkaIfP6j6CckHQABEnPZyfe4v+yivdJb8uSSqDyPqdLfEh9RPax/mLF6fMmkG8wmlSkLCi+gzYV4UfNRcLsrsRzOZ/0bIqQdTI2lPSBVE/R12BT/BZh26PN+tixnCxKoNEva1w4LdlUIDuOf/p0OXEPX6qp+ldGD3UsutkqRudLo7fqaKsEiRZ4ThQYW/oa6ZlKpPjZz5vCboRN82G5JXuN7BpHDVd+EFAdM+HOU1cL7YPgGyo4LFrgp9RjVjagB1sB1od921+z1/V0NQxI2LzPtdKKfR377KGhl0imFp7OgPJ99Asd9nm32MWmNuJrzKEXpQi8YSvY9oyBD0FQDc0A6auH+fZDgHVQ8iuVf0fMY3QccCuoHMllCwAmsY8GiGP8kw6gJLztGbqNNVD55vu6VCd5Hxh80952g85hQq/BxxBf1w5usSviYuU4lv02RQZai6YeMjrbi6FBwZ4qoTC/Fe+HSOkAkA3WpP7gaxIPDcPtNybj4YiiuqGGkUY023q26VOjWHwH1nhH1w0JlGyqn6jtXs3bDQyyPaGMkxvTPUw7F4ck9yejDUXq74Efpl/pVKLvIuilkSMRAliQSQ9GEDtKzeQ14PQbM1SIvgKxfISBzDQPzSflulFkRloGY/JnkKd59Yfnbj6aMUxBXvTG1r+0sh8blYdy41ZjtFzGQzbitWlmPemj/xEWo+I5GEMlfcUjXLy39AaDe0o9PfdJ+AR2E8K3bHRkCjTcQ/KEkD3NIAQkwLNzpNK5euh3BbyMx9DdOg1TWuF/u8erpD+O5tQCrVk+wztszynYRkLii29VYRq+BVAVuP+hLi352oGclPc9pDqG3PQnWzx7QbprZHJR5h2gD+8iRIOeQFzfVQb/Hab4H5r5reD+66tHGgPa8olqc24Cjjx/fG59Xae+eQZf7/smCn9x+ueIsbVK6gInETiBXV8c+xCysbjHOn+0Gas5supX3GuLivN2qZ2JFwwN5zPEefXlEdkmNYMAiAYbI+vx6kfyJlhKGPe3lICwalDTrq3jSn0RdBHRkomQ3IwPLkEN2wEd7NpgM5C044BfYF9BaqxoYMOc5g34AcvvfUrJPSUHdr7PDrROWYYnO1/FcsUMP4ICcCSBjyZr+teNLpcKXNJ0DPjpA4WdGJcBJkT5eidgdCwj4xOWjToaAbUNZ3yYF+gi7GgVnDeoY5O+bRYeZooGs7CmUGI4rEG/rzh7N/Fi9ptRanW/aSGGgIq6rZDvChc0WK181w5xTcD0WTQyooidciyq6QZ2aEDHXxc1FIymSkXriNXuZe9at4NPZwxUDT3pX2Frqj3pn2TJeAaAFw02/+ns30D325y0LEF9pIZhkvOXOjnioY1Cl9YNSUxc6K9a9MRh5NFt0iXvDeL6MYd9oQ03BWNYfLvo7pf+B2rwLR2W4dg1FnAVtynpiBm0Zw7vp2DRYeibLRBuRY7hfMNSMWMviC+mdJuZG5TU2PtrcdlHCli8hXfd2QktAvnehmYI3VkFToUMN31sjyDpgsbVU7JRv5cV4Q8445lZyzZlGEeXlzRygDvChkQoE/MIcqKaWoERnR5+N+o521/PM8+Nbt6nxmIsZLSswO9rws96G764nY4fpab+tALFylf7FApmSx7c1y37JW6K4vT6rLIgh6S1FKmJTWQSQDuC81la+0sNYkIXyVser9tHayHBKxY/myjp/ChQuliL997w5JWgkjnUBHyHJ2IT8NZo5+8Za8NITLC/w0atDGnehw9R99mfFriiDCPHWsAQWkWrLd3Dudn8rIE/t0Teuqiq1c2m5P/+PlH9t35jafYlYISqhAHyvticRluyatOa1GX/Q5nLgdGDRwPloakEZGqee2WvIVVX+H72AZfbxTEmoKaNTYglB2ym7bP+3IWm5BeGJj6yDS5NaKB0LdzUlCFxr4oWVKB+wzJCjDbUiZXGcX4QGCGeYh13lxL1LyEj9naxqngFSfXVrh2QJtmUlu7Qd1o3isGacqSBOz9G/cMYM/cwLvlg69kw6HmPCnAOKtL6qHJUzVYyc2B9R79wOg5MI/unN8AW6TKlfrqTcM0h+pvLjH6DnNZq+y1Ci3DMMyZFstEwILyo4QQNHHj+wU6aImmo/EwZ2xwKgtqm86lYNHvx3lAK6uFHGiO/T4+4EZ4mtqkchlRdlbJ9W2dW6ke72/6lqZSDbniiCjOyW7/6zlxKyvrJjeCgXoCNKJxu2pLKwix4NbcAWImsb/h8NQZr39OKrJASaTBdNDthgL4LbunB2KLljaLyVzoZJ2vw6ZvE+QA62eHpd1hY9D0y/+mDWJbjI/lvVlcZxvaiJXt0a2yAQIxh+dsBBe94038c+YGVDguo13y66VzwiCtQ+zrhkfX0/WKHdbgXwO2wYk6S0b6MiG4AoHNWzxkDnIx0dEOb2Cei/o0jEzcw3M/WsF3juAu3Efjiq+L2BU79Bvkfo2xKL2ZVJAmuImGGIUy8O1B9Pbvcj/VCHrTytsiq6XstHH8FbIZ4nfzk9ucEgPFKPmTzRoG3L/PeHQFG+o8ZOOZ/bxvquhmnONeyTEoHNog6y6/80hgdM7nyLzUW+M27F3Y/I+V+lQoTsu1FlsVKJ4KDVHu00HdVzCo2CnZkbjH2NHtwY2nUflQFs06Qq0TKsRAAPIkxiHFOdKCz78EXddtL4clAoK1Erzzd6yvRAnkmLuZ3bYLs6oR+foORVxSbTJeq9xu7IoHXNdxi3Vu/j6l4izUEAniWXlI+EB8f5B8RUVM6Uj4HpQnQnXH25oURrCdhir5cZ+0tJVhwoE3VGeoUle67tu0HMpyfO+uqb3JzOnlickPceS7uCq0811VhMqyiWqa3lvGrlzkBeIBvzEZoEtnYrBMr1mjuOoGET87ycR8yxu9QXSMCA7/jZTdZN7n/y1tsDwHJeuzUzJvYUUn/vQAWtaoZmgytZ448/F6a4ZfKeZ4U3uQ/gkFtBWIbu9vWFPtFtQWuJN4g34/p5pQkILScCHTSlNwwNki+okBdZga4Ej1oyEMYiK/0tT++FaLU79O9FeTZTFLS74X4jhwRrfKf4tNbCGDifrDZvQ8PTTfeJzGiRL9o5lxKm0Xqz9KwlWq+92MiAoxSYjhJBZX1EOixHziT/2HTwsHTBxd6HG34W4PeaSeiMlV668UYY3MqsgM0niXLap6zuYM0VSxfYDd781vQk0c1wI1QZVwQzFbn1Iob048AtQ9TqGD0RfZQvDMQEghYS/1LcHETz9Ul2RBgcx4caPPdZ1ijY4kv8UXXkZpaGIVXoUaOqdqLz678GjU+cglZnDzRnlJeol9CdPFz7bmpzxHYXKDVs6CMsDGEfdOyyvNVyyU19GOto+m/S4NdXm30tLbwJAHob9qPNK9eieCp2jNShSOeHnTcBnYLm2Id9AHSGcQy0R0HEqHm3uAUrknXOEzQdwIEF47T8zaM6ph/Cmi/IxwWcN7y2QVU2ubFxlnzYsSd7e1AaVa6RiAUsZNCK8d6Jg6n0r9C5qdOioWrwB1ktFPK+BIGxMvXzaMYdG/WcCp8/La6CCO870pDq/y+seZc5ZZZPPhZeFswi1Trlo8mkL7Xec8T37PgvOf3/nx5hXNpPhm7gEjqc1JwgLRXsaox28HOrp6Fs7xVFTXkBE23Sg+yuP08bTrY61qe6QG6frg5JmqKB3cjfSpn89OfGOA94xuZ4d3i3l6Wp2Z121Frf4gYRLJEt5TSAGY1gRAxw8jeaMNaz0NysAfGXvtgQiCrS+IrCim7O++E/DseGJRO7iHUMqM2IFAw05ovW33RRfmU3o7TvH/Rf3KjjPYzGllnl5Kl4q/a0eOcyc9Xrwr2YXWtLWyFmTGdodZTmi3ZJVQ4BCTNiOYOqSr6QQrOhWJW59g5MhbMJ5AuN1Upm/236Y5rOM6MzlgzkyyBb1InSBMbkmDVmeiz+gEYCzJYnNQ6cHNC+1jriN9ERdwiLM9juZ7/WNz6G/UDGYPH0tv5e6frAXKca4+P/sp9SPd/qV3OJzgcXAjoHYc0Nc/tWYjMTMspwonhAVas8+E5W4rMECyowSAHYFtfS0sIgvYU6lG2Dd2jSYnAyu3amz3ZVmAiYRhqg+twb86i3nqAuRXHAy5yo6jr3HTI1uTiSDrBwSQT6JilAK5z/mU2GJ9P7RZkL+9sP7XVDT+pJFJCLb1sIseXwvk0gNZv0WzVwlq7m0re5fGZOiPMr9hm2SVkKo1P0mlTUaAX4yJ254uRloanJ2oJ11VSzgXWuB0zeWu86QY+brEeeq3TIrj92rNsnKeLzcuXLuNKf6Yp8hvR8IGoUkip81bEXB8k7i+wcPeIap0TXDoJGmpkdVl2OdCQyRBTP6YLg0e8vgnrRS1Zm6xPCS+Sj5hLirBp8k1lV1nETTYLXxLEwR4FeLrHn7Ecu0uqNQsjZyOLmOXSMuCU0D9mn1c/Z+SMdmxJEVAkBAKcH/j6MxAQODmAnC1nxG7pgiOwiBHIqU3p6Lrm1mC1LC2IgKiNa6vau6B4q6SUZG4WRrsD+wRrEFHbB9APKTzT+zaUB2rpoaFKTxNcvHEHnVTK+Fee71keH/ZgaQ3GN8/yu8/ZFB4xj9d4bONykpMG0qbVZ/ybLVJ3k0q+MeXcDv8o/Qds5H60wmxqb3+2IRphSrf0xIaotuf96uqqLVbPYvBjmRD/bn8lz9mqgIVLMwtgOn7RKPROnRxiQfMwZDcchfr8qHdWXNVf9w+3u3lDyOof2hyFrxJ2qb1z+lbQgbqUzMXw7xCkl1ZQQ5vFo0wyPoDxu4Tfq1DMSGekEZbLx7TBgo+ZSul3DssEYkBwjMgojvntYcXSIW0gXVeG+1Vjggkymgfj7GcQVJIptOdDjoidem5Zl2B1hkbFp+eIFBgVon5OUgNcy588VlvJSOtbxiv1q6Nos1hgfKLo2N0W5EB06a3SQpl6KioVXK0QbRFK+a7cv2l3DhyAEeE23ir+HG4wPsfNGAv9IcN/tEKMe7JQw9pHfLAmcwng8zKmBABBcAr/RhGi6lOWIsyC1Tp57fGC1kppIcgMAX7Tyk4hI/T8e7vZavA412wEC3lRwMzzkgga9V6VII0IBo4FeV3azgnAQNME69Djq8zeCbvM+tObyOK1gpwdSITMCZimFcO6Psp990GU5pwdLj5xV/mnCti5Y5hGP7ynP7JMD1umx0UM1h3+hYxcoX4MoZID73zOHqRuaqycFXQ9eID/L5XObCLbhzZ+raHKtO/fFPkWVUz9jdFgC8QQOzW61VAYDvele9oyXT/DCP8L+GNss6OCgYs7LdBnEh/cNnQl6c1sSd96Psi1nfeu33tyUDppSSKj/3NErsJw5Dl37+iyYNiWGcz1sk7ZzXgnUzjwY99aUDFqEbVg2A87614ecYjzzVMPViZlmIikVDVHTGSm3Zi7wgk39PsaczloV9hWLTB0p9Wp+/iMUH3vkHgRqJVw2T9rt8kRSikrxlvTg8H7WVtIu9yyYFPJkTms4TgnypoHKM2eRnZpmANm5LejDPmhbnibO9EWrbyOySs4PdgOdhHSKk5f7xrJtrXKJ1VQVvOZS/fiAL2YtnQEf09uj3ntEtQfz1WqwPANI8IPEQjB9yH2XckGD2YniHrdiUrra6GclxX+Hx3T+MaEBxr5XkTVRD+2p3OS33ENc4AqLDwcw1FmuGVxq2Mf9zT2P6MLvN1EG5hn33T9Dic0ajKG4ScgF1Qn2wMoGEDAERgZ/J35x4F0NPR2+2VhEtFxAWsRz9+6p/UPpLO6Hw0GRv/3xndwrnoZv8LjxBrk/SmCyRUpMRE4+LtulvgLaVAIksK2RdZqJ7wLHjb1wlXPdQUPd8wXqe/Rt9T+rUZmG6gUIl4De2JZNZRv4GZ/ekDxZlVt7vOnB5V/3MtSRpdaT/e3CnsAQ2s/nct8DM0XJvZTCRFiOU9JYWueYbTSE5W19jtyWekibed57cR3agH/5e5HC1pi8FvhFbBL3PS/CjTdYA+MS0a1Qbabq2gNw7XsY1fVvLhhnVmg1DTXEneoF+107j4J2U6OxHgXJ2mNg5wXX5LFVp6E0eti8kGzENqdpmE+Q71ya/eQbtLTjAUSSvRqqry9l0lJikrH2ph4UfhTLMYkkJZ1YMvGOMAB0F7injD3xXWyPww2u1Lo1bIUtPNFzznz6ohSATPShA/lpwWs2k/zkM37/ijGcsL7f4kbDqInjHtU7kK6Bzq/tLHoSpDSHF+z62Xs7CpfbEcJtbhdTwg8dFu0FedecAdBgL10p6RyilxiM13qyVOMeFNR/qwfDS4Rt1da0dI3iH+AINWRgOsaUXMwWZgVTZfXltIecVnyt9qonBOOeILbdiaLfPn2xC9t/mli38J+6MEc82O8ojvTZwrV3CPCF3yPGDXFvYz/rc2UJPuOzpZjxtQ2f9JzVAJHWuCw9sfmj1Fln3mruPu4fTj5x6rWRXAdqIlPEuTCyZK5hkFovCPXFKuL3penQPvTU/y2vKnmrtXsfUcaKwEB2qbQKtvaZkJyLv+OLv/PvlhEqsV5Q+7JMWQ2nJ1yUFJott4GE8VaYsbBSaTiBBcnu19DY8qnP/FTImNP4OrICSlsrKMYXdMD7T5S68ed9zizO9+wW8Pp53mD5yj/PrsyBOBNpundF01nudgfzoANa3LyqHdNR13mivxWgyJaRT1N18xgFN+de/xITeTJzdvTddQ//bXffSvvwpn7h8bqn6W/ic+WvakBwG7vzn/dHtrIZOHhT9t5QJ+80Uyf2VplPWN80uF/j0VdeIO65J+HD+ShkyN0RHUQ1S1DKVFhsCbKqWtezSqjXdBtM+0gNuHDathxQY7w7LaNKOztoe9IgkIoxBsKmJ0CiyjuxSrpKeUZcvgjOxtzwQ7vhUqfVt/ZL56OJsX7TG640EgkTBKdZHfh5KdKGEQG4r0csJQ38H9PZyFOEUyZpuvfV6rx0k5UfTnPs0a2x018+Il1QG6ZIQuwIbao/J2ybvAdphhTPRVZ465+6oprDOmjHdD1em8K0QUBQ5VAFBYYfX9ouz1RmhuzFwNxBluI2OW/Sx00j0OAayunAKALXawG+VHAxGOv4qqzOfs/929DZZS/TbK6DxfK5p6VJiH5XsqxbaZb3/6OlszM/PEXHqLekuW/dCAzOn5iqLgzAsDfWyJJH4bLTxV895LXQ6RwtXzatCdouv7WvZFIjuqr4fKdZ3wuH8twh4W4HEumUzTMb3aLL9gucceZjJplxe7jJEJUQfRZeWrcitTTvOu/sTRHyIFmO5g1FLg9f72ggZc++qDtVKSuW/GfKTIRGMQhEePwN5cWEO/rjW7AS6oh6VJCgaKqarARjmNhqPapYHLk6APnRGTldPcSYqoknIEX3WtElJ78yembwd+1wnBDXeyQ7Ce7cFtr/a5wsqgqTa1FZK9yx+ZSt0/BD0Bga2zPkLxVKcHj1Cu98Ggv7GKmSqrslVngAQ9akRWkm1Cw36Od5fN3fjR0sglQ+eXHQUed+CaWx5tXZYHqv/VYhLBQQ/ESQgjOS5m6sfWRaas3uGL+Jj855E9UdpXincad7tEguDch9mslH8XxtlzqzsBvsY31f+71Qbxv71O4IuWkRF/zVag/0AuCsglcQVpFmhTV9ygIyQ8VLJjU1COQmWuVjjXK8rYv8lWKDFfyOMj0e9J6LnY6rjhUJPjwnr/XsTpPFAUndlHNc+xFDG6kjRHHCv2aahtFnx7Ui0PAlJn3O/qcEfuS4S+sxyvk0cz0+y+3u6tJruPhJO3rF5rJxr9X9KsFbYYVMxxi8mcuGTj6JDVAb/J/K7jczPxnTIufNrTQ3ButN3v/CE0JBfOioO8gkZ9U+JHlViRk0QbFZ8xYm+hukhYLAJ9Y7GMVz1ZgIV/Dke576cNpbktJDVZNBhvoeClVUF1YdIDsTVfbViXQLXj7IHVrTgz007pSBp6S981/Sah7RLmwC42+HkpqAmxr/z8JegRHlpZS0sNykjbrFQtoLQK2D8HbkRvtVFmevKd4my2u/1yZxWvaLHsLkAYookmGzpAaFuzgLre+9x4iSVPlhFXdEk7yYetAHqLo51dy3tD0AuIzrFS86iIHDWOT069C8rLYh/5QiN8SH3+omhbimoad9ETkwa3rqo4t1FTnS4qMkhnQ5xJeUUPyPhOOtA9gcC3BfN71Q/OFnFV78oJrPaUUQzvc9SB4WXsYvbUFILOMesiU8q+qCXtIM5TbOEW4fTsouxhLxb97w+D/LIZDgHCegtocz25ngRuW4Za0aWa69UgaKrdRaRkeyeHbtIXEmM0+/hDyy5/ZTMFAfqHieEvOxVA0RS1BnEzcB9CpnDkpvzBGGI+EF0lBwbWlihqFQfG+pczvNIZQfEuighcnJE5gEVlct8blNUDfa1JG3Elfk0A0C6d+MtgOWuGrkBxfQalpGrYcLo98fJahgyEV82UNAqgUWTzLTZ5FnaPeEfeJK2/yM0KZPaFVqtYVtoi2G4CITfES6fXdFRepTMBTEpSogppWlmKNgSHBLqsQiJl0HVK4SM7P5bUrCk7IulY1rPSUFxiKlCUsfXD767Urb4yhZ7jpngiTZLsmO15OOXweihvMh036hvbK2exHjyR8I/IanoBSXer5EWMPjvM/VfIzwPMhAxDScC1n0YW1Acs+1I2l0s7vWL0+rMDPUjYfQmraGS/ad1Li5TL6BACRTMMa0qMDB3AocqPfHCMadkvd8HzR+mXvtklvQ3lm+9NQ+1wyX3QshGSBaSEGdAm5/AU83LDB036wNnetO9edT7j6u+PU8L+ZXt9cDtneA2g8eE+ShA/y7OP+jUAtuOKZ4qDKqSNuxS114RSo5q3G2A/KKuM9zU606wLuGtTG3yT4uSdvPBHXAfDfH+zcGwPLy+UvKgFTAzkEPoNvsBd0GRSqxlzBMfI+kLDPR38/XcYSpLncIq3bSX3bVXjjdJw6Nk9P4aaRMofymM9sxRuUJNGk0U0dz9ifUFkRzKfvpR3IBCGIhBvXdk5xYUiKszg7ve0R+qMSkMtOvnqPafxTrnL1x1xoHukfnRdTv+U7RsPw2YkZdgFLuMqr38jUcXgynLktkWIr6VEOSsuPTjJKTE66CjY9lfqDlLKIeAWl+x+c598EX92gt+9Kg8U9Gx0QtW63z/Bsp3Vz6gnL7uM+ynCmvS/+GJiPi1Klw0jo5dXCkE6vhJnUtA5B7vsLxtFWs/BEPd7h/W4jdD4mVgSHVrVcAEWgYG9iFam4YB0rE10vXLkTi+cr3xa2m9tVjrjdramuYmsj7HsxBWqNwL7cUzIN7v6be0j1qoZ/4xgiCmLASpAeH9VWGpkaNW0BgGZmApKjkmjFPsXHGNrfcLhnK79npBUUAhVlLo5DYCjXE0nye57glBJJrQX7K6w4YYjOWFa6A2awnW3W8LpSxToCid8/3EU6pW9P568edo1w7LjHzoloDNadMVg4FImoId9P8Im2IImWT5FgJ67uQ45HSwZRtMXAJqL6uPpBLfxXoNmB0nsVG42fXf/wG2cygzfTZt8RUcMoR5CdrBqpOjDNVBltccwC3xDN2RwTCjj7pkmTUAcfm5crCw6IJTVd62HOoSRIDKYlwZc4X+o0j72p7qMqdeoNOZXoYZH9EsPOdtjlDyG8KWx5A1GCxf1xMzR1iJekbUWR1w/vwSrfPpWQfDZNJKnwaXb50Bhg9FUnJTBpVAThsku7+qmKSfSkK599JkTBRpcLFNF1iO+KtvNWKIlimvp+0rmVTUa+vGrCkGt07IwH9RWw0ZJTpnIl8EuXbB4wPj53FJ/9FYA+aQqOnGkb9lP1VQAMyvOef55WHKSU5MkZRibdDvCMLkwedz81RnTqKo3Fqxlx26TjDwMvIzerozK93IRKYPdyvJIk3NU6OPIrUX1KW7oKZGuOOJb921VPmbF1U/6VFOej++nZBH6MZtFE4uIyrm1JRnpp1qPuHHXERCMdX3GvUdre7OPrC+6nqmSNVn/TocqegQtud6bYuDxBba3NwAUBpuHpc1RVvpRK4D0+USPmGeMUTuVmo6GzmuEDhiwF1zbWloFnPk4X6mCHxXuFG19Kz97AQFOxPQN/EbbX+8aKpvTk3dt29zLUASIR59j/KUpKuvyNS7Gb+P5cVZbB/HVPxP07nhvQzc0lYoQ4kuu/lPuQkgdBTbhLMFtiNl7E7wUkxdcovcOFzsfL1OiRb8iOO7LavwUzjlVOQ7AIS3ukhlYU0MoD3ia0lrHYWrX3im0lVX+x7MbeapHN2nQg5uPxMdFcwu40V4zTBEk3CJOOXsZhURjeEJ1hkEbCOpTXrNkOXwdlCmDuFi8G9rC0tk0mxGpTHEsqyLIHFR+MeXrfskLIrHcrR5S0XwmRIywBbsEaQaLAVpCvYeDsrmx9HQBQCdlJrQy9hyFpdEbJg6Q+q8nnDO4CgPnhwX+YH9Rt3LhaSxNpyCT8Q5OvOI2YgxWW18a/IgOe+4hIw/djgBc9vqqp+ckSGH6iMHWkZZK+l1TNk1e/mdC7c3nk6hxQIUfmQKqTTi/Tr6ot4EFluq1B9kM2IJbrS03eb6NIw2gr0z5//yFoyNWn8VxjvI3SJ8U/zEXGxA2n/FcEnXxzKyVbgPSQSIuFHzdcr3aiUdfGJGhOg1MsrNJ2a76fIGOfzU1LWfJGqZLWZyC2UL1LIRAqXxhASaR9wJR2Xte96RNom/ygf98mxIqsB96g7JCdte6gibTW6UQGPdZBliwgR8sLjpEaGd90tAvQizUVUfoXxPKZAe0CNQxXolm23nm0Foyd+hK2FydmNgs8yeYc0fJnmGYShfo4I/K/ErJLklkPYf3BVB4n7ohtAcKt5K0jOuSXb3H6++q7AicyVu5O36VN4Olrq3ijd8ZutD58amK/VkNOXaU7kejgVh31LfWDaw7p6RWBx3+4Z0JyHIiYt39qe+kxiB0pnxgBmlbVAdzEkb5kFqyY/WcvTiqSd1hEC11Q8fMPIoAx/OonRapQcfVFqG9F15dvkdxZqioZulACsx7zV5qJqA4puknqX/XscJtmhiiILgKEe+IjOyDEG+UgOiPFCi9ni23dCrocdHOqgS6SOV9PazT3OmwC/YM3uIU57BzT5YbepzE3r/8f9jyFAvwkfKOVs/jhVMbVYMlXEcaa30TTJNbiKXsMkT7pz8xppAurxp616kuBdecnZpmxt/DkrTWHTDjqcjFyrYtkdGsqVilZcBp0Q8ID2DUD/Eu7CcLewfR074OWBalujNr8bs010SJjN78n+eKNCf6InkcdF7v8ccEvM/Sn1o97ChcRH9lgZwVwn9kKIiHURelvLOQsp+J2DdZM4lbUZFmRemWy/Xo9EGGAg4ijhEW4AUWrh2oIxa9LzIrehrgP57RTEaEx8qEH57Qp7e5d3lysfo1ts4e0WqBiFOABr9XUqozY8cHgi0OVwLNNxMISyoI391/iES0UtMwcsSoqHoHr4jM4VxB87npzszRltILw/z4MsdY2KKpUwWtmi7pnNk5Nh++XRas72lbxoJYWaqIUL0Gau+cT0REK3obeuVv/RKtbm6Vx47J/Frj+vOiMAmYOF7V54bI5mmhXVabAcoXm/sgSz5TymhptYt0nvYysZFr6LvO7cNqkgmrqJEF31UWPZsyNhxJtdBf+lbwH+sfvvg+qpO5ZQk1SUBb8ZsgqvzWFJmfmdQUzamdO03B/1TYlqUGsI54YbyQCC6S8e2d9LxwFIwwN8aLSc1KPX9NsXvF2NGfusz5pNHKc3qH5kAPFoEX4pBhaeiMYJxChG6XOM30gWcpt1qAFvDwvKpca6YzmZvNilVadbzguKLlcJ/Ii7+8N5Lqx2ZzkTaHfLKZpPK2p/+TB+DmRRYJeNFmnF0E82R7Qw3lG3fwHrJevp0x/qFC2A5hAKAEO069LTEi9qO+sucj+snNsHceOASbsv1XAMqrsWj2F/kT9k/cpFvCf9ujtytDNFBaCahVZaYKViMgcHkbKPSUESP1zIzrv9kQuWIaaRJ8W9GIgvio9rmaedTuRJe6kmWBl+aqECsdRZfm+gtB5TMyl2EknDZQ+2fdpeEwj1kPvGhalLl4U9nOi018gJmijcRu73yS98wny2RvFUP/rx07P1pNmJ9bJtPCnsCbf3ZZ+OUVmlp31z0PARZZGLpIsTkDM9EGHl5rv3H+KlN21Qx0oevrTWdZZdrasNOcwJz1bzmY3XyOAAUUFD7h96Pd5R79lCOY8MAoivZ4q0w7KA6vZ41sV6a+tTNeQBwNgaDZODlYI7bxvfP1O6GhOys64C0trFKGn+hu5J2RwO9Lst67b5e/6pZIu6lAQyeGt3aPiStDvEUpUfcI8Od39nkCqnq49pZcTRpRkCl2L5eUp4aqcuCYJpxuHifVbinhVQFG6aJMQBpXHVxhzb2EoNXVYcbmf4VDEPK7zXUpUPMKNEovy8YLZmGwKKZdpXjPg13IL0ikc5X0cpgcMWYiBk93x39YkfGTz4H81oC8v6N9ee4sTGh475Hx6JvMHYw/yl9QKWoVbNQ84lNiqS4RnXMNUIONM4oKkE/E+mkaxDSR28lkQSHims8BAe23kYW24/vQy0MUEnu8PuWD5pIg3zh7rSviFf+x18+Bmey48ShMS5sOELKOEmslXa4K/c5jQdglMIk0Oj1YsPEcaXfz7WLJudZm9g5iD2mRq0ERbTCUN07F9PJBw+q+WkNYI66Ba53rY1bgQg4lXvBxChkGLGtnea+OSdVky0F6Iv7c8YTQqFiLm79Im0qNyHS7P94p2UHkaJ+H+vbrHRcCHXJjutrT29KSQnnw60TU9NFxLymqgQwVKGa9KHMp1JCf8mS4o1DUSd28Lb/8o9MteO/8zUjTmVEG3iWpR5RjaBrsK7U+hDdR51hErmvQDBchVj0vW6nUj0lGtXQBwsxVZaPt6TSdrWHUQs8myuU24X5S2E/il5ZWBWec/9+VJShG4T4XYWBrJcZaSDh6tYQA2GPC4XzEwiHuKyPZTiwxssw5LXDEnxegOGYsKu61V8Ky7F9Ove8WP7SxF7MgqLRVUyxf+bJSmlFKy5wi8PeBviG5LryyprwaCLrlL5KvwrZKMs3YW+nr4fGjGM5tGB86UyQvD1aW3LbBWjC9/jJIyTButB5WgF2wb651Iw4wwrVMGSd+ATc/DXawH6VwAqEpLqrs9kFOgCL+hRkHn48m6Pmxjeu1c9W+tZL8bCf2+4KBoQ/nZaZN5z2VwLOx/ogKoLzU8e1tG1n+32yQK0wizN4OdXcrTy2MtXWGA6cstdQlQLMqx1tiUK6zuFSyiVPqOZ2+NgU2Fj7no/ZnnNBVggIkVjRLFnM6OGhwvtPlaLIwSLysDAKxGyeLUpph2dHb8pqisB9kh9OFbusvcX9eIDzYOsh+YKmGa85xjGxlIzr0ZpiHdv7DLwtUX4F4HDbzC36nuFp2h93wHRiQFwYAaExz+6CWlmBRGGuQHsQXSodX0NOKARSjsB7VgQdhgg0cHMpyEW8HFuSPbbOhz6R/e62OFNdEqgiMgwRYiw/PYxsvbO9fdiFxcT5ERm+j95iDTwkElk1ViOXsea4hOqHd4G0cntSAlJZVtorZ6xpvKvLyGq03Ff2utlDFakiHiIOSl2b8uwDwUhn51A81znIq3OTmnRaHjE3SflssE/TMw8q57JV8RhJu7DMlMQ0G8fqgRQ62ot0qPEAg2hgFm7fh0m/1d7Gdnr4iu2SoS380tyoA8CkumohA/aswoD6REAf57WQqmb7AYCyt+8eNnB5tGwcLBtYonHX73YRMpsutALY+gwaK0TN6y62hTMEf2hCcft23E+q0+jmO4TTq9pIGUK24Ov6gKK7C3ED5muTI/4Crgb7Y15RqE9WlVRYwWarzPuvES5Zn+orW19qVAlV7ex3mO6AWTvqwdwdW6dELBjKQ0Ztrp8+JoXpqvCJpUcX9bfeco0c8xGesJiDvnciEjWa3Fot8avQwa69/icUxhVkTGBE24DBK9j7jgf/Rc3ear8+XOEXzh3UIClN1C/EVOrj7Xe6lYy30LfJOPpYVrCgS21x0hBdZHhIWAp61nCmeCevoIZMK3zOD9V4e4rCYn5oCdzzctch7lGt9biS7B/WAni0bUcqsc+YdvEmsLZ+QoEX/lF93NY7NcNFBtgXXCtjcyJrWi51GBCWFBJWD0OZpY1dk6A3upL4CvTV5a5cteoe7XkD4+dj32CExPLz83bVhP7M/0GcFsZnGqF/j7h3YvOg7Rc2pkwcbaLtTrXOXi924hITu8ZDpdykm2VxmM4us0/XPwHsYrBfpfXztDPTmsTCOGWseTvOnawayGltKG/Il0TqAl44IT3EU4g8ULaJcfSnfg4+JDmWeC358EZVpodqUTfTvLLVwL7MGzwXyq1vVFUhGYEFXk0MYS0ET39wEpyBtMSbLWF2ZXOTDjgScVn/jsi0XLGVZk0hV44DslS3Ex0YnRfV4lLVTY2KGOE1pllWaoAbXgwitlOUPrCnwWGRAKywp0HOt/PbJSq9Y4s+c62EhE0XYnK00nPkQHZvQ823gsOgu18l1OFTYmzwNcthXu1Lc/eMcwwb16sJfHinotmWDcz++Jcw4gYiK0tn/628PXi0riV5aAUlSZEEMhNsPnk7Txy8RWQ+e5iqSa0ByJJFUvVLWkx/a6rcNkcdljicqrDXdCJ6YOqIjq2APKMq5VtUbKxvMzeOOuKr/C0JQX4S2QitTik+YDhXCvx/ngZwHhYFYNDyuFrN3lDqB1sY7YgK3zvq90sYEOKqEZgoc2Nvq45ahrtJk0TLMFA6o5DjI88DN2ESIJkJdetXwg9FuPJNIFqd4MEAhQiXSXQJvHXGEBgNgPaXLKTDaLGeVCzI8AIZdNgo1Ne2UyXUgFXJStsO3viGXueewrbKfSmArSmx+tzQ9o39AbN/pYxBCXZ8agzESafp/Lb/7AAZgljhi3WVFyPtm3jt2Uy1ZgNRk3Xwl0CdI15IqB1yqfVFs7GjeBhGwXw5J1wV8iS3/Dp69yPCYPTjK7gkNnfOyKzHwGk8SgDbh280PqHksvqGZZq4oLEC5jDU0Kqt73MazaX4wFY6ciFWleeIwhOE0KkoYaJDr1JFCWSyCFRLzWVXyNjl+hQbAlfZAyo7c8mLqwVTIcV6FKd4KM65sqGp9P+QNu+FfatOrbJ+1oxQ0+mL2Sb3j2KxrcY0hnKTRPujL2ZIdhn4Nhv8VNlGG5LVOr/WXiQ2hQggjoGvdiV7CnLfous4uhfit6x5RFXjFXkqXa/L52hg7r5tyJicVL5ZsjpN8y4Mzl5frp2gx85nnLLSL91tn9P8QgustDGoUaeQkgqMu4FbZVnDTv8zDlER1q9xOomy6W7ZhpcIKfJeaCwFd6aHVbykR1hETKVo6ALSwmcEsolYEjMFgSShuGoCr5CEkr+l6MmAG+mVeRm1vDZS6GSlMSHbl3QUj0QsA2N+UMIzSIHxl+l8sJlFM6acYYFHNu1d9Qxs7icHEOTUz7Ps9mabmEGz+Jb5DI6ShTndns5qOz8apcJr1HPzZm17D0SCr+1jMphlLrhfasM/ak/IeHVKMOWyInnODvlYItAqmwM3uixBmq+cUtADz0CM/inG58E5PzFOvmUniPugq6covvea5e5lFfoJqv8HD7IMlg6ECBQF08yt9fKIyVBuzsw9VwAe9f73C5Hs3JNVpNkyXgslIKleom7YTLZ/8jl8T6TPX6Rc8WK2PlzNV0X59XbIzCuMOOGElxPYlygueT9Pb9/HWz65vxFAScnHvy50OjfyueE6zJVka3siE0owij9RijVIGPpU+gsMHeFvQ0kwKmlo5mzGFBfzhsGa0O0PE9/CTnhl0CAnqZ5rUMLetkI+BbvD9eiLzCtShQe3hXpZL/BFOHbH7w+RY13LlWILrV0GcqIb6gdSEycLm6Tw+SQT/bA8cccyyvNHHNcJKtoUSGTshWIIJaLet50q2GSzwrTVXnK06xssq/819K0/9Oeh9I4/5I8VIXITW5KFbj6KexwOTeVksEZ5qz49S2q9lfIGKKmr4dpXHKbc5wgFV5+rhd7vpCegk/xaVlu33Dt1nsTjkWTUuYNNSIzzTdYbw5ESJ8KqQ+Gy2LhMIRex5bERsL5cioLMZdyK0/+pfVcP+N2ce/gdeK9Td27QGI0FyDqNF67bRQQ5k5gnM9uQxwfbRJ+GRZuYCbBFi1Sn8CBbaHGSgiSYI5IHzazVJCOwqwGfkSkZ7/7JP6TEEStipRQcPkRs7X/TQEbH8qTYKVCEtBUP/Qdzzlc0YT2V+qM2H8TlY6wbsYmSYKId7uR7zOxuLhz4a8KTSOVnXaYSP8Buemd5n2TwWSOwmbjqqJqPahmtrYxeS+iE9ffWV2BBLqGdE3xgltGAD1yZYUu1pXrOB/rIuSAsdScpgbfLEMRB0pnf6ifpdg2tiskP15MDa53ka4/kd9gEMkI1BFO/tCiYwnYmnpCDZBOSpporzhb4LBAyOozcS+4nyyg73U3gbqHQp2D3P10xh+RgySNiZakm+yQhnM2O3foFhKUBBqTZRdnm7E4Jx/p1ByTE1fK9KNVmajlQ6XoShiO3wKIe+Jfpo+lcUPfCTJrVcDlGvD1AqX3kaXlfVGVjlGKtFoz3wS1snfL1WWSLAcZrPa975cUijSCMlI3StC9Ah+UKRkMeRYOYqsdI1GORb7MrHwgwZTMVwJaKGA6RP9QAhj6zCp+X9kexpMbcaD6xTDGOeRq9UODqqy/iL0h8L1irlGSN7/yLenNwBDPpcb4HzZwuNPQEwV+/0kOo5/dxHAEkwvbNgkzVG4obmeaTcPg1gMrlPoKunUyjbFRzJVyxUQIEiMXnkwWE9r3s+2/cDsEiJ68/nB+0o2YoN90Uj53YBHwygxeTIuyFnbkLAOIwC0zVEQi6l+IF5JUY33I+/qb4+YQhVWzHRrUqGBHoVwciltOt1aPDHV411nofQ2JVbSi7oCvAzj5ljomZ0GSgEmM0+FkFpH2PPb/3mb3/pFa6Ku5O/ZNQRn+bk/P7l8UAL3NoP73QHsMagO82s/uCi8UMvjaxvvblyXpfzJ18OurZYy1OD1Wyt0OgDWwkjzZ4naxLeyAj0EaqpILryQP8fMIAqFy9cMK2/ixpr75HqjJPgC/ycNm/SLH6mC1nHiUWkMsJtycQ54B0OV1h7puVLWpFmlmW9v0jp/Dd8ZOUMMLeqHI+Qwbi5RVXph9KHhgcZueZP/U4SJ5NY+TMvINHPrYrLcGLt9LQIsLVgoJ/o5K4El9bH1bC+ccQxgq5vt3ZZDWE5Tcuig2jruzejgkG9s1nPr/jXXCC1prUnYZm/IWdDWsHGPAKEeGHDz5cTFepcCp/jnHaYeTtiEozhA/e7wYszhtEFY2zA0gdrs2o3UtnkgZxXioW5iWNVx/j4PFiDXtzqdOpmJWRtAAlVfRAWiGnIcbUkf2Y1vix7d+ZP6FAbbC94BWmtkbncNyJ1uNWPFpTviTQkRTVhzBVOwwOoErNsvWUAM1SsaXSbtRyrA8MIXYdr6qq4wIq1R9EUcpU43AnR3pRUNtslCcB1rpvelL2n5PZX30XHfX+FDjPFp0leQMtPvzckaM8C3DCuVrCe+2128Vp2Ff93LWqsI6EoQDVrx8WOywNShtrbY7t8Yg4IS2+L8eyXBoWnMiltyVVFkui2h/LgEUwH0qMNSPmbPVHhUIzmusF0YCB7gmgAYtSmEFfNiKI2DPiWGWsIVY4HnZvCNJA7Pgda7P0//QkDOnKEH56vn1S8vobX4WeoRZ0anYEZTyjCO0r1F4J2t40bUEdYo+9wQwi3Fk6Ji9jga+SUR+rzNuxh7D/vplfaTEsg4eQLtAjydfZVpzED9qgqSK+F296ew5pMebElW7cvE24DfNjDimDrzf+3akZPDulmz+EzuDe/INQpB0qDfi9jPrwvjm27Bq9ai/B8PJbFG+vu/w9K9/V5cdgwXzvthVa16/WhZZv3pC0xP3CT75xJxU6k749AN35+/prj0sPuL6+0jFBV6lDvFxbdbtVU/h/jYpHKR61fU7zopWV613NpnsSGvmqFfTJ0u+1yLlKzSb2bElGxrUKfH6k7uJJCJw0Ro8Dyl4IDA/H/8yDJ+2lI6261gczzOzHeFiSYMimNHMXWqHQel6PRcjknXAQll3M+vgpjXUnJH4XEyRn4QnMza5RRNPXStVIx9dAsgYUH9wVftuC8goCWUshPewrRkAanDoGYSiycz950RInZ/oPdKWcLC3bES+rdxjcDlmd3iougwsAwbxg5QIb8P9SlFaK/XQLJUAXprExuyKVwEE4UUgV6VNpUBTw4/WzKRRy7Ch55R1z0zWuPMA4t0xaVsgcANIOFPzybfwE16dFasGrgS5Df/QMM3luQyDVS1IaKSJzq5TPEVt3Qt4M7RYVHGz+f/0kbbiKwfmnGp6SXY6L30gqdwYGmTd76rRzaCZghV+Z3JWdl+tSVCQmUzFhtTMuo+HXdDd0BopyxKJY+bL0HOqK6jP2yOUrV4d/iXfy8hByEJ0ak24OH4yxDWWdMQJ4dMZy268bc6j92B08lcE0IU2aVHvcCBGvYxYNJ7LpZzo7V4ijHMRfAKhDsACFCja3aV3ie7ae6jb+Sr16JDSy5338fNONJx1//9CESD44u9XEI0CouuPhPwmjkvbXTl1jx0LthF4dmT1cQEdQkwXEjY+IlyoVaRkYqu2XfwyhVIiRZtoXZcb2Nk66VQ33rLN9im0zNhpAEFbs6budueLiVOxBC5Fqz70/iXCro7QvbaMpJwv1LlyRsQUcLygSo/g/NRfIyuzs+U6qukWpen1XDHvxsHMPiIBcqOePDOJk8Ub0Gvv14TvoF86Nvrnp9qQA60NvnSNLCf3qggkXlLHKI4jdQFa/R9k/UJOQyj8PWUg1Or/Teh8Bc2dsk0WlT7ZwfR2XxEzSYmhmfHp2phHhj+EJOhsc9aMeK+HXc5VIdpogOQSlyYw0u/tEENbTrtifHtRjMCutz/2ZCoYcLuIA7SfBmlV5fKOuzxcHBNSQBYQsY2k86mlCM5+PVYq+XP7CQnBbZsW69F0wQasaVuSMbJtPQsBLAY1QvhGQx/OJeC0BolQYi72ZbM1IG475SOe0XONdeDNIufRURq6eHfXrUXpCvp5CoquU7qQvszuvst8qxmDWfWthFzRl6Frka0mtLSbujNM2GobkbqT96xXA+oKtQFAznXwagfz4Wx+wlzzugJVt8vVUJu9v26Q6fKSxcR3tJP9NL61Vv+/SsEnkMKhp+/CMxccn7ASJ86VRPtb2ST9z7WznzKKtX43Y5s8B2Bd9WY5nqSDhFLCi3cqmrSwVnnLjfy+GKuUy2eX4E+0QBWlujWxgkWbQ4QbIWoprF+t525dL0UFKZJaQ1ZQlBbyqDAdVA0vhNgdqcUA/pX1QhQjMZFsaGscFMeKFYioKtMf3KJGo7/ZzzVS96B/hR9WTp4Vh11IAhE1LEd3HW1t9vFmFqnxSgGk8dDd90qVYEf70MEpV99zymtgGytN9Ab7ijnjDvM8YsylsyA9w82T14XA8T8xGMIfpOGctN5oUTBDKeTUrYZ44wiCYnmA/DF3hhcGkhR9xERQk4SFP8s40bnDNMe+VIB2pmeWFH7kKl9QHBDVrZlyTqcH1Ig/jlpDgOh0AAutKMLlOtiQ/Wdyc03c9QZ4GecxFHjkokoEE0PIdg5pcr2s30Z5HOb20s7qp6c82l1p+NxRtBp4nMN80+k08XRcl9SWqpYunSFFcKCNQS75fg0JZfL/FQ4DU7Jr45/GkQZ8r8jXRDdTAk1qo+Twjd2sUeKErZb1FSNhrdPQWv0xOC3YAL1fZZB0wm5m1qS0VFUumD7qVWYxW0IC5f755+5wD6MKcQEzE7NukFHUQIAttMZOuAJEHPf8cWC0i11o6dY3DQOqqtMG7FG7fcm9r1kQgVf7Yg2+i2mhaXsRUoEv32A3hgQaJqQUNmUwe0pd4bXmxeN/Rhbxij0RRroOVGiVEcmTPDkUNFp4SOh7UHmQcIzl5APrbIsAkPGa8wCJ+zjZEosIH6b+r4eQOcvtFEY5kiX0AhCntruZqvbocmQfHO6R9CPVTgzbzqOcxB4N7yLPAoGRPgda7m/sfFTqzi7INyH4HIAWgsDZii5ng6AQHtUi/GV5InqevbE30DLZe9STzRi6tZG3Z49P6lNHgn2B5B+aeFEFKKm/403F6zBZPZyfRs5GdUhDJvRbg5SrYE1KtGLc5GnEQzEN/VU58wCX6xGY+n6yb1ALvO07+OqP4h7HlcdVb4M1WluYADHMpRGguXgc9l+8Wo5nwmTxUbSEc5c0+T3O+S9koXm9V1UXvSxwGYO/9sFbE7nQNCWJjHFs/o7xH1AaL9nTs0gXZN+L2hDbcxCIfZ2oZLmAxmb/3Hi6EJIZaZMHb75ce5+z1OJum6A2M09vH67HMEoOmt4LvY2MBzn3MzVn6ZOmgMlCZKS/nGBILEprj3q23ZRrKehf2w0Lwh94/6EzOyDlr0aWh6scIYBOm533vimByRga7OKf0UZ6mBTsXcS4ADbUjlzraQLac77i4BFM2DzIXt7mTy+P+ld4TZc4t5gmyYiPqBdsvyFide/B0CJxzyqwZ6txCkKgYdIkRnu/8ki5ZB53ROkDcppU6pXwe3P3H0tCnw988+YpLqZrAIhw2Yo154AKm30Gt512QK//YQbWysdQXG6hSNOYtX+v1oyg7QU7iWzm9KmEgi+3/5UllT+Mo7T2xuzxvrODaQuf768fOz1XdLETJRT2H3+iYx3y4eLkKgS/C5u/4BhIxogoeaXeN+1LXrXrH97Z5SZtoi/q98l1rS1jKmbKlcvatoX9qlSmtK0VYCytfC8AP749FC+ZWUQVRRPbSn0QcKqoI8Rl/V0jK88KRCJX1v+XnEWrmdvV4ijk26J5b0YC9gO8WJ4MTkjNfu9tIJtANZ16v0BcZsyX5bR4JYZi+PPnoncMBzuazvqjBfkOIF2GPbVlV1r0hJCMU4FLHKe6D3+WyM5wHxe4PToCdkvcDSZvVUAB/FpBtTtBcA0bOX2VQrsY6ts7t9FQEd2ukDZXnV0i+posrmKkGPg+fcWO41O3Re8szzsGxyBPLKnUxOpTINu2Vz7MNuttOsBqncAg8sp2rnibSO6Cl6FIPaPqcd7P2NUZo9/y79onmuJDmSpPZ5eNq9yoGgkjNt2kk5M7Z16oXZsuCfBlxm52voksTdtBuSIGsrB2IBd7Ewlp7qwmojwzDxQeuWuoJiUvMioQXulkkAiih+igel6zN1Zt2fp27l+RC2idoY/SK+pwHj+fGaU1cb3tMQP47fVitBffP0D+xB8I2u7CRxWAC1gq9ioyhI2Dihl9A5I6W82AMZDCNvATIPhBK7Vxtrnywz6AOe2Q8ozI9N2Q2WuUTV/B+mVh0Q0/rI4Yf3AyoWbjA/FfN1klkiYNdpCVeNjuDU0YKb2hmiQDoTDjbg+jhyEpC7jSlb+gSgZe1Gt9EQssYs4Prm4zuYsUIFZPg3IYHUKYANglyJ+BE1/FBcP5+XX0aaqiQk+UvF4NfX4ss8pwQfiPEPGR15JCwY/PmL4MRZWZXRiLLUEVTHSddW0FsaWcQi2Wv/gHX14sPi5JPaOl3KUyrqJNPO5OFgPeU6G93iVW2WEf6+NozLF1G3VfepV3qTEwJ92FjV7tgt859zH6eg1o6EyYZq3FuwNLsztUxPciUMAhbTDeOOr14MIa+8v6CClQ0CqM4glDsaVgPwYNZPPEEkrfGb8ftja9fsGpOAutfP+Ew5L5U02onS3njcAd8trMooi3iaJ8ZHHVagelullUSDzvifbpKukNqRMT50h+yARJMNlZlkJHCf+PJrbSze+6VtGKBzv9Vo95l1sHNU3qzt9BH83H493Q9dhUiDYIiXE2UHfyh1h/niwIEeJo6AlgSNsj6V51WBOP+pOH3NVes5iefY0nCZq3Jq1nICjrfwjUZSLsi23ogtZ0BTduEdQpxsfpDPTPTfmxsOODHNX+D/ZKSINHshs4ENisAPOpX1tFotp+8lWrJSd+NJtnLUK1M3v+qoEB5eAfjsQjSk7odwkuu6TsFuWGySBmgzKLf//N14WloIeEV63eGtudbuQwnkz1nZvalDjdckfkWZIUIeYfAOyACYxk87BfaiMmOAO4rqBxSYBQfXm8WMwK0OdURuXtO8mrC65pc2LNcCNqtwjiA8HRDy+QAPE4QJt4nO+GDLa5WgV1eW+wXYhWE2SCaZ97x4M7GZqrVp+5xvQcoIUS4CJFnbaTWhmgBWmk65t89jqB0Wy1N0KuPpkMh/4vrUQRKAoXFYgc62AzX6gIAAWRRI23DHZUi7ND/BGOy2fHotwGB6qVEIN1gP73Lmce90S2klqLDTp7ys9a5pZbkpOpiiktcuBVfc24Wko0rhsFNvYr2DNKTmHAiZuGG88e158Z4IxlMs7DRh3TJ6boxRMhigXtbdyffUv7I/Deret8zvcNwqG2pYpijS4cT/tlvKCQCdslvtHSzicn77rrdSPors7wxgSUk2M4pVNb/NbA4uHqmDboEUkWRs/EoTOzjp8iZOxkhf8Uvt976KeY6YQRhfafFO+hTSgMSU5ilDDtnRs4zlHTuPYyRixoVEO+YcpJ2pP3bjcm/DdZ/MsdU8oN6TeZQVNksuNbFRCCLDpHKRuDxneqTtVlsaBfgdnh91EE/HtHGTjW25oyg8OwUm8NC9wZm6VCAC2B2kK6vVnAPktUs1EgyvmGZel/AcQDq74Iz8odepYEAIVaP6eqgm6BvdN2EogxtmuZ+ms069Y3f1ZGM0XzcF8uNOPC/xLp8wIp7Gz/UZLUClpQwdsg1j5yTVdAAeLuRdvMB2mMwx3FE40ddWKMdnM0uD/Yx+fn2Ips6L3Kt4l44VXj0Z9u1N9IdAfVspXizHJHMlWLt9TXBkIo0kbRlYd8nEqNmcTembkITWV1vP4VewmHsO3hkyNDihcew1Io/hnxx+cs1vUb+bvA0oM70IenEcZLvXWhP0DywiVBnIQPAVfLpuX5AIe/QLria+ZzC+kMKku9arQ10fCpHPWt5YayQfSBmiZHjTwkBKGD8xlIFQitETsvaQX3wDgSqyTy7c+fAn5aXN5ahbg35SYinPOxujdDvCIhTh/ATcIA3Zwi+pQIQucCVLxU7ptRCwYQ7jEDOa7Q5upeUIJs2ZreercuNhR3Kgqg6lRJsVKufHuKSiGC14A/kJMuB0Nh/7DJKPa/H1H9PyqKE4RDCPrWlkcxTBgwbXHnhHuc/Di0PuO658SMB6iMpajuh0JRukap1RE0PDg/bicNvCiuKfOhxuS8CWDLAIjr5ZD451IA5U4xJwpWTRqpRIqE/ZSMry3HK+4xGGSocrrPgDPT/rW9iZm0GaupQ4YveLQ+3FXH6od4lZVNSVclp9bVpIuKIASuY477tPzwxRq8KZyXUPOIJ7DQ8zeoPmr90wouTYMzkoHblQMMPgpYn3O/kWTEFzXwsdCRWQ3wqWSdY6xMPvii8EcwMQB97Pd8BwCDH7aFzzhYP0AE7eKcYkopDqJwxewj9c1BGHVsJdL7EMTmv5NGizlRndp75//Xk/xX3kwDYf/GGLdxFvUXgv9fR5w0KgjaBGqrhof3cmcVoICwCtDg0OGPvS0xQn3okP7RSMeK1rAgkya+YEflQ68wHgMSdz38JIGQG21uidHBxJFTUY6147A3+Jhe4abkX0jmjBTgIvEHBAQku0H/43hsaAw9eAXEwN+YNgpOwEvo/rUd3Jxp5BEtfHDy+C780aYngsxW+t00BdZjjxNPCOzHK8QwCtaVadDUf/t3fnrjuynnNgTmHz8XTCXQH0KYt7Uz+Lmj2f4G7Umx0wCfXuRcYEOjstlntjTCLgDR52RihJ2gtYfhFi6pHk5FwlTiYlvi/kYHug4x+AUWkHHQWgFehHeDsov5CHKi8vMqpxUnl2qGjt54WQq4lK6q567VVa1Td6kSlWDpfi/wxCBO7OHUeb4VPJKoM1VTuhfIFEJvsPvNgk44bIA/aaXUhJEJO4uplLVftTrv7fxbwTZ/FL0y9o/n23BdaVgg6Z7wgstmyrVGADDiXDkONqnFPaoVXNNlosTWtvau8NcaAjAXtKTiYl5un2j+0HVpQR2KrnaV7dfep2zntWxIbhVd8k1nSOaT4e9pvXkrawCpW/Bwk16Vi2slHtRGVF5KzpO9SgVImWrpJmituZN+zjnAVez2dWa9W8hP0HorDs5X7gCJHR5Sxrr0S2f4BzsX8RqY6WGik0XI6lD0uy+qFQJvwG2WYo+9Aa6aV5UvDdWq96nikDSHiKLq39OpvJRZvBjMxKsQhGqvd1tX+SlHnAA6ITvoljYKkRnVimOLObfhpgTh+3gVaCs0iAOLAbIzH9LJQYfYVJB7MBazCydtnx/qfRc/4geMzPBZl39nj7pozqOeHtdn3z2E60Kc5HQtMpvBnQ8f2goFuHY9vC5r4CqOSBmN94eiVh8YD+a9VMcxdXbMZ758hwGQ9VfKBGIpqU75wudxRwMdNndFkHkDdWfqDovWLFV9E4ohMO4PwXfn/N97bfdVGC2+Lb31ASrD74EVmVHRCg45E+K5/yrl1KjP+FKfEicOLxbEYmZKQApX1JPE4qo1I51/2hQDtmbHjAxL8fvvgWyzGCYSRU+6FRCPt96SfaTM8GbpRBguvdFGXr12UetYWwIdhXLC5gvesctNcZ83Z5vOks9jSbPAbw9FG94ocJUav0mqFQu3S6UOPuDIqJH+cenOm9CLNhGizOWwt4xnrJdtEdPFAF/+8NmNyYoo2HnFzUuAbvpWVpDapkRrhRkYIKprTFHiPAgslAkVGC9HXIhWQBXx0L18AlAcd1xrkHlbd2OFlsEEQ8gqSUHx/w/mboANCfBhKOX9Az1WR5sUO5optmfCUwxkIz0YL9TnBAIvsVvrlN61nhgjhyW1dtlC3ewuDQPhdk6tyn2rPQKFmjjpvvEOXrAIb6J2ScyWz2VniX4uE03+dWRScZLW/TkHsYPYNjlteD9DrqaX718c+E/fu6CpKtzCKB1dkFfYN3tq5YT0Jl6+uvTwpwvnh8vAdTGufV0VwNfZZe+1DToG4yZPnFKA8vfAIOx5f8ATKVxIvt2GZEuH3iFWmX0Dcj4MN+7RMheZQ/P6wNwHf5qxYiLVwZjrbh2uEjW0cEW/zTr7eFIbn5/tK/jUSHjvwB6rZ+Izz8tsZtgJ6rLI7F0gh3r9f2y+5OyufnQoV088JBJBAHBL31pI2unieUKdnfSYC9Kmyum6rE8QOcWcyxPMPnpzdbDUaeF460ARtACwefNEtRTjOAYNyvheDYDSP6AolbmLUy8JcXm4kvDXcTp+jGHkHvtN1BLZWgAkRFmg7jwZbsn2NGQDXJ/mV2dQy8gjT5lPZUo6taSxbMgXwiXZy8oyy1MYMmriOMCJp/OpxpwIbY+BZwkNsAZ59P08DszMgHkO9XhNVOjm4Nt1m/K5vwTk9SkJNn1t4Z1W6PdunfQFGpBXTdMcqmJlwtb7/v9OtX+BDu4TDCUSfA/dxUy9VkQmy7TbKjICwcs0koYxlvbAMkXYGzaMmbOJDcdXHxE3KjD5YYVIknji87rwRg4CuVxpSWxUVH8TCCgcjhO2wKeoQypnWEEJD0Mqgfe8hViUr7C0fRHFB+hebObI/YI8yzrvvQKurokQV9BmZm/JOMBzR2RLqIuYIa5Q+ckjjNgLObOwXSVkPsdBYcrWztV2/85LdukQTuLNayICe2vJcHORxU0ohmFWKUgpEijZhrw6GDa3fPCd2c4D0fKSIjSTQpS4tJrwp1Qsm1uEbwNUqLTmPNEgkv0vHzY9EiwaJIz0gLPo6XjemVEgzHGekq/pJD2oTd99Xow/VBw9x01/fHgjD0v1j+x+kN8+A0yDksof3f1iIzxbIgSBFv9b6fDEcklXlnSmSwAGN6svRxZ+HE3UwGMuQbLbRvEK+T6CYugw3Rm6IGzyiSfStsvpYEd4eCgBydWBLN1zI1QbezUJqwOb0M09EcTIvnFdH0q7NMFRMwyMhNeVrPG6NjAgUG5nN6uPsy5B4ca+IB7UsOpFfIXcxp9uuVOsVBSkOp1CNNsq0/hzrbG2ONekwPT9A57khDVK7UeA5KiToPq3YsiQ7nYnzRDB/xyVPWu1Ktn10pEfKYUp+FSmeHsYy57DON2bEd9Dy/ZTE3IJ+Q9CqBLkhBj1Xyt1ERMZCLKmGLCsFrUOLMMOiKjPvN5q2//pFX4WwFlEGRM/YGFXyZdOdnX60hlZLzbm/m5Uijs8zCDlH50n4NOitmK1yI+T9YDFZ7qkWXv8zgJ4DyqQBALE6blsXwXRidDPsBDCzOG9vEeJ4kQ0xx3M0j6Ja/XmEkDYy56Z/5QKMKpqO/D+tFMbN02XugBI+eYd4ncMtxbnZq7PMipKT0dy21XD2Z3+Lw58GU3mNwUU2S9rJSkx7nG6eruWttetoGz3ASg1dGTFI2W5IoLfdDe2cqP3nJ4OOBxbYpy25psruxmfu47dMtOi0FK+dvwBorA6eI/tQYtmIabzvPxDKxHhRfYQ4uHcXmgp3AZd6kKpxPiil0eoVsfw71qGeBzUwBMTdeeVb+ca6kxw7DQ9dVJOdrGR6U+uXw7NzAwoDaJETEuIPa64QuPFv0ecqj4fQWWwIGy1npnsquWr0OCzYmcEn7Ti4ih576c3kCv6C6Tvx7ZSrxe+bDtJr5ux9lqSnLVzq3fAfNftpoiz7rg7Mk+5+uqT7YJ1JRHK5jl9SJaUWJtTtz30H2N1IgD+8r54j+7p0bUEn1h0BwGVw1RLIi6mTDRIDOkjEW/9ObkZzJNVmPqXp12oUezwJNwzwXIPErRX0ug+yJgLcQ7yXMmWhu077dKjppMKrw3LEHGyxkLxzfOqQUQFLQr5AYpKyX6dedmHfALlg90u+PgmSsYY5fF93KWaG+44Wje9MrHSjmSe4dowcy5Ra3gkzf4yy9Y1nuJkJ4nvzf4cCyvO3uICJV0mNEfDJNQ468XvQJpIR/+txbaeia8nWFTujB4DRxLXkX4R5b1d4mCgqq6d60YV3u6u3nCQ+q/ftia82Wam2zwAcx2fMdS45l+JIEHpb6+3vHIZzzbzFrrINHjfBjOzZwn5oz5L+PEWcjxJciOK5ZigIJEJUOmEqMuQ1Bk/dwk5Jz7esA9u+Ue37ke9zaapT9E2GhJbiAnSy/I67Mgwm8P2sWU=" />


<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script type="text/javascript">(window.jQuery)||document.write('<script src="/Integrations/JQuery/jquery-1.11.3.min.js" type="text/javascript"><\/script>');</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.migrate/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script type="text/javascript">(!!window.jQuery.migrateWarnings)||document.write('<script src="/Integrations/JQuery/jquery-migrate-1.2.1.min.js" type="text/javascript"><\/script>');</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">(!!window.jQuery.ui && !!window.jQuery.ui.version)||document.write('<script src="/Integrations/JQuery/jquery-ui-1.11.4.min.js" type="text/javascript"><\/script>');</script>
<script src="/Integrations/Centralpoint/Resources/Controls/Page.js?v8.5.1" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

 //Design > Styles:  (#ReDesign) Scripts 
$(document).ready(function() {
	// Browser Less Than 1000px
	if ($(window).width() < 1000) {
		$('.left-rail-nav-container').insertAfter('.main-content');
		$('.module-results-intro-paragraph').insertBefore('.details-view-left-column');
		$('.inner-container ul:first-of-type').click(function(){
			$('.inner-container > ul:first-child > li > ul').slideToggle('slow');
			$('#search-expanded-container').insertAfter('#mobile-navigation-container');
		});
	}

	// Lowercase Services Menu Links
	$('nav.services-nav ul.cpsys_BlockList a').each(function() {
		var uRL = $(this).attr('href').toLowerCase();;
		$(this).attr('href', uRL);
	});

	// Find "Cancer Health Info" links & append Querystring
	$('a[href*="/cancer-health-information.aspx"]').not('.event-content a').attr('href', '/main/cancer-health-information.aspx?iid=1_001289');
	$('a[href*="/womens-health-information.aspx"]').not('.event-content a').attr('href', '/main/womens-health-information.aspx?iid=1_007458');
	$('a[href*="/birth-center-health-information.aspx"]').not('.event-content a').attr('href', '/main/birth-center-health-information.aspx?iid=1_007214');
	$('a[href*="/cardiac-health-information.aspx"]').not('.event-content a').attr('href', '/main/cardiac-health-information.aspx?iid=1_007459');
	$('a[href*="/stroke-health-information.aspx"]').not('.event-content a').attr('href', '/main/stroke-health-information.aspx?iid=10_000045');
	$('a[href*="/sleep-health-information.aspx"]').not('.event-content a').attr('href', '/main/sleep-health-information.aspx?iid=1_000800');
	$('a[href*="/wound-care-health-information.aspx"]').not('.event-content a').attr('href', '/main/wound-care-health-information.aspx?iid=33_000175');
	$('a[href*="/neuroscience-health-information.aspx"]').not('.event-content a').attr('href', '/main/neuroscience-health-information.aspx?iid=1_007456');
	$('a[href*="/bariatrics-health-information.aspx"]').not('.event-content a').attr('href', '/main/bariatrics-health-information.aspx?iid=10_000053');
	$('a[href*="/ortho-health-information.aspx"]').not('.event-content a').attr('href', '/main/ortho-health-information.aspx?iid=1_007455');
	$('a[href*="/dialysis-health-information.aspx"]').not('.event-content a').attr('href', '/main/dialysis-health-information.aspx?iid=1_000706');
        $('a[href*="/diabetes-health-information.aspx"]').not('.event-content a').attr('href', '/main/diabetes-health-information.aspx?iid=1_001214');
});
 //End of Design > Styles: Scripts 
//]]>
</script>

<script src="/Integrations/Centralpoint/Resources/Page/SwfObject.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/Plugins/jquery.cp_Accordion.js?update=8.4.38" type="text/javascript"></script>
<script src="http://ajax.aspnetcdn.com/ajax/4.5.1/1/MicrosoftAjax.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(window.Sys && Sys._Application && Sys.Observer)||document.write('<script type="text/javascript" src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yKL3veK4_1o6aC-AgxFwMcMVIIRV9FSpZJuOVk-LAIAVsBBLL3O32w_IrtC9GX2Hh4_Jux5cIno3a_NfWjNJey3pHIYsP0dX-mgG1YhQAMEmmQ4K4v3GbkuLsq21ZvMc7MLyHc4nUSN_UV69kEqz7hA1&t=7b689585"><\/script>');//]]>
</script>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="43343198" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="AOr/snWYko60ZamvhdxaYnSQsIddn4lvuROsWEzcSKcnnBwL1Cu3gplhzQWOHiq+gwD3Ek+A6by+Yxk8yddfd77xy4EJE8OIqZ3RqljsrkUlM2TD3r670fxQsm3a4IxUmifn+VJ5flj3V9nqOH6Q1FcPA/lGJWgvdr29I/cMPWs=" />
	
	<script> 
$(document).ready(function(){
    $("#discover-expand").click(function(){
        $("#discover-expanded-container").slideToggle("slow");
    });
    $("#search-expand").click(function(){
        $("#search-expanded-container").slideToggle("slow");
    });
    $("#mobile-navigation-expand").click(function(){
        $("#mobile-navigation-container").slideToggle("slow");
    });
    $("#mobile-search-expand").click(function(){
       $("#search-expanded-container" ).insertAfter( "#mobile-navigation-container" );  
        $("#search-expanded-container").slideToggle("slow");
    });

});
</script>

<style>
</style>
<header>
<!-------Top Nav------>
<nav class="top">
<div class="inner-container" id="blue-navigaiton-bar">
<ul>
    <li><a href="/Main/mclaren.aspx">About Us</a></li>
    <li><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li><a href="/main/patient-appointment.aspx">Book an Appointment</a></li>
    <li><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a id="discover-expand" class="discover-mclaren-health-button" href="#">Discover McLaren Health</a></li>
    <li><img style="margin-left:13px; margin-top:13px;" src="/Uploads/Public/Images/Designs/nav-search-button.png" id="search-expand"></li>
</ul>
<div class="clear"></div>
</div>
<!------------Search Box-------------->
<div id="search-expanded-container" style="display:none;">
<div class="inner-container">
<div class="Search-Input"><input style="" onkeypress="if (event.keyCode == '13') { document.location.href='/Main/Search.aspx?search=' + this.value; return false; }" placeholder="I'm looking for..." name="HtmlSearchCriteria" type="text"></div>
<div class="Search-Button"><input style="vertical-align: middle;" onclick="document.location.href='/Main/Search.aspx?search=' + document.forms[0].HtmlSearchCriteria.value;return false;" name="HtmlSearchGo" value="Go" src="/Uploads/Public/Images/Designs/nav-search-button.png" type="image"></div>
</div>
</div>
<!------------Search Box-------------->
<!------------Discover Box-------------->
<div id="discover-expanded-container" style="display:none;">
<div class="inner-container">
<ul class="discover-list">
    <li class="header">McLaren Services</li>
    <li><a href="/Main/bariatric-services.aspx">Bariatrics</a></li>
    <li><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li><a href="/Main/imaging-services.aspx">Diagnostic Imaging</a></li>
    <li><a href="/Main/birth-center-services.aspx">Family Birth Place</a></li>
    <li><a href="/Main/home-care.aspx">Home Care</a></li>
    <li><a href="/Main/hospice-services.aspx">Hospice</a></li>
    <li><a href="/Main/laboratory-services.aspx">Lab</a></li>
    <li><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li>
    <li><a href="/Main/pharmacy-services.aspx">Pharmacy</a></li>
    <li><a href="/Main/proton-therapy.aspx">Proton Therapy</a></li>
    <li><a href="/Main/robotic-services.aspx">Robotic Surgery</a></li>
    <li><a href="/Main/surgery-services.aspx">Surgical Services</a></li>
    <li><a href="/Main/womens-services.aspx">Women’s Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">Primary Care &amp; Specialists</li>
    <li><a href="/Main/locations.aspx?taxonomy=Cardiology7">Cardiology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=FamilyMedicine4">Family Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=InternalMedicine8">Internal Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=MedicalCenters">Medical Centers</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Pediatrics11">Pediatrics</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Surgeons">Surgeons</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=UrgentCare9">Urgent Care</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Urology10">Urology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=WomensServices2">Women's Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">McLaren Hospitals</li>
    <li><a href="/BayRegion/BayRegion.aspx">McLaren Bay Region</a></li>
    <li><a href="/bayspecialcare/BaySpecialCare.aspx">McLaren Bay Special Care</a></li>
    <li><a href="/CentralMichigan/CentralMichigan.aspx">McLaren Central Michigan</a></li>
    <li><a href="/Flint/Flint.aspx">McLaren Flint</a></li>
    <li><a href="/Lansing/Lansing.aspx">McLaren Greater Lansing</a></li>
    <li><a href="/LapeerRegion/LapeerRegion.aspx">McLaren Lapeer Region</a></li>
    <li><a href="/macomb/macomb.aspx">McLaren Macomb</a></li>
    <li><a href="/northernmichigan/northernmichigan.aspx">McLaren Northern Michigan</a></li>
    <li><a href="/Oakland/Oakland.aspx">McLaren Oakland</a></li>
    <li><a href="/lansing/orthopedic-services.aspx">McLaren Orthopedic Hospital</a></li>
    <li><a href="/porthuron/porthuron.aspx">McLaren Port Huron</a></li>
</ul>
<ul class="discover-list" style="margin-right:0px;">
    <li class="header">McLaren Difference</li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a href="/mhp/mhp.aspx">Health Advantage</a></li>
    <li><a href="http://www.karmanos.org" target="_blank">Karmanos Cancer Institute</a></li>
    <li><a href="/Clarkston/Clarkston.aspx">McLaren Clarkston</a></li>
    <li><a href="/Main/Home.aspx">McLaren Health Care</a></li>
    <li><a href="/mhp/mhp.aspx">McLaren Health Plan</a></li>
    <li><a href="/Main/homecare-group.aspx">McLaren Homecare Group</a></li>
    <li><a href="/Main/laboratory-services.aspx">McLaren Medical Laboratory</a></li>
    <li><a href="/Main/pharmacy-services.aspx">McLaren Pharmacy</a></li>
    <li><a href="/mclaren-physician-partners/mpp.aspx">McLaren Physician Partners</a></li>
    <li><a href="/Main/foundation.aspx">Our Foundations</a></li>
</ul>
<br style="clear:both;">
</div>
</div>
<!------------Discover Box-------------->
</nav>
<!-------Top Nav------>
<!-------Logo Row------>
<div class="inner-container">
<div class="logo"><a href="/main/home.aspx"><img src="/Uploads/Public/Images/header.jpg" alt=""/></a></div>
<div class="mobile-accordion-icons"><img style="margin-right:8px;" src="/Uploads/Public/Images/Designs/mobile-search-button.jpg" id="mobile-search-expand"><img style="" src="/Uploads/Public/Images/Designs/mobile-nav-button.jpg" id="mobile-navigation-expand"></div>
<div class="second-nav">
<ul>
    <table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation73&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation73&#39;).hide();" id="cpPortableNavigation76a224fb-2841-4c3b-b9bc-237b847fccbe">
			<li id="services-nav-item" class="second-nav-services"><a href="#" onclick="return false;">Services</a></li>
		</div><div id="cpPortableNavigation73" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;width:600px;overflow:hidden;">
			<style>
    .featured-service-alpha .grey {color: #cccccc !important; font-weight:normal !important;}
    .featured-service-links{height:250px; width:750px;}
    .featured-service-links a{font-size:13px; color:#5f6062; padding:0px; margin:0px; font-family:'Alegreya Sans', sans-serif; text-decoration:none;}
    .featured-service-links a:hover{text-decoration:underline;}
    .featured-service-links p{margin:0px; padding:0px; line-height:20px;}
    .featured-service-alpha a:hover{text-decoration:underline;}
    .all-services-p{display:block !important;}
</style>
<div class="services-drop-down">
<div class="services-left-column">
<h5 style="padding-left:31px;">Featured Services</h5>
<!----Services of Excellence DataSource-----><ul>
    <li class="services-of-excellence"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></li>
    <li class="services-of-excellence"><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li class="services-of-excellence"><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li class="services-of-excellence"><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li></ul>
<!---img style="" src="/Uploads/Public/Images/Designs/Main/services-drop-down-image.png"--->
</div>
<div class="services-right-column">
<h5>Our Services</h5>
<!----------------------->
<div class="featured-service-links">
<div class="featured-services-all">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/acute-care.aspx">Acute Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/proton-therapy.aspx">Proton Therapy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/home-care.aspx">Home Care</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/hospice-services.aspx">Hospice</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/blood-services.aspx">Blood Conservation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/imaging-services.aspx">Imaging</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/robotic-services.aspx">Robotics</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/breast-care.aspx">Breast Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/senior-services.aspx">Senior Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cancer-care.aspx">Cancer Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infusion-services.aspx">Infusion</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/sleep-services.aspx">Sleep</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cardiac-care.aspx">Cardiology</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/laboratory-services.aspx">Lab</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/spine-services.aspx">Spine</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/stroke-services.aspx">Stroke</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/dialysis-services.aspx">Dialysis</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/surgery-services.aspx">Surgery </a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/emergency-services.aspx">Emergency Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/trauma-services.aspx">Trauma</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ems-services.aspx">EMS</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urgent-care.aspx">Urgent Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/eye-care.aspx">Eye Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pain-center-services.aspx">Pain Center</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urology-services.aspx">Urology</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pediatric-services.aspx">Pediatrics</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/womens-services.aspx">Women's Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/fitness-and-health.aspx">Fitness</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/foundation.aspx">Foundation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/podiatry-services.aspx">Podiatry</a></p></div></td>
		<td style="vertical-align:top;" width="34%"></td>
	</tr>
</table>
</div>
<p><a href="/Main/acute-care.aspx">Acute Care</a></p><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p><p><a href="/Main/fitness-and-health.aspx">Fitness</a></p><p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/home-care.aspx">Home Care</a></p><p><a href="/Main/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging</a></p><p><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p><p><a href="/Main/infusion-services.aspx">Infusion</a></p><p><a href="/Main/laboratory-services.aspx">Lab</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatrics</a></p><p><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/podiatry-services.aspx">Podiatry</a></p><p><a href="/Main/proton-therapy.aspx">Proton Therapy</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotics</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep</a></p><p><a href="/Main/spine-services.aspx">Spine</a></p><p><a href="/Main/stroke-services.aspx">Stroke</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p><p><a href="/Main/surgery-services.aspx">Surgery </a></p><p><a href="/Main/trauma-services.aspx">Trauma</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/womens-services.aspx">Women's Services</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p>
</div>
<div class="featured-service-alpha">
<a class="service-alpha-all" style="color:#0066a4 !important;" href="#">ALL</a>
<a class="service-alpha" href="#">A</a>
<a class="service-alpha" href="#">B</a>
<a class="service-alpha" href="#">C</a>
<a class="service-alpha" href="#">D</a>
<a class="service-alpha" href="#">E</a>
<a class="service-alpha" href="#">F</a>
<a class="service-alpha" href="#">G</a>
<a class="service-alpha" href="#">H</a>
<a class="service-alpha" href="#">I</a>
<a class="service-alpha" href="#">J</a>
<a class="service-alpha" href="#">K</a>
<a class="service-alpha" href="#">L</a>
<a class="service-alpha" href="#">M</a>
<a class="service-alpha" href="#">N</a>
<a class="service-alpha" href="#">O</a>
<a class="service-alpha" href="#">P</a>
<a class="service-alpha" href="#">Q</a>
<a class="service-alpha" href="#">R</a>
<a class="service-alpha" href="#">S</a>
<a class="service-alpha" href="#">T</a>
<a class="service-alpha" href="#">U</a>
<a class="service-alpha" href="#">V</a>
<a class="service-alpha" href="#">W</a>
<a class="service-alpha" href="#">X</a>
<a class="service-alpha" href="#">Y</a>
<a class="service-alpha" href="#">Z</a>
</div>
<!------------------------------>
</div>
<br style="clear:both;">
</div>
<script>

$(".service-alpha").click(function(){
    $(".featured-services-all").hide();
});
$(".service-alpha-all").click(function(){
    $(".featured-services-all").show();
});

$(document).ready(function() {
    function filterResults(letter){
        $('.featured-service-links p').hide();
        $('.featured-service-links p').filter(function() {
            return $(this).text().charAt(0).toUpperCase() === letter;
        }).show();
    };
    filterResults('B');
    $('a').on('click',function(){
        var letter = $(this).text();            
        filterResults(letter);        
    });
});

var $p =  $('.featured-service-links p')
$('a').addClass(function(){
    var s = this.textContent;
    return $p.filter(function(){
       return this.textContent.charAt(0) === s
           }).length ? '' : 'grey';
})

</script>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigationc1fdecc8-73a4-478e-b7f9-da0884005b89">
			<li class="find-a-physician"><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation85&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation85&#39;).hide();" id="cpPortableNavigation5bdbf3e4-64dc-413f-ab0c-dc4179e9a3e4">
			<li class="patient-and-visitor-info" id="patient-and-financial-info"><a href="#">Patient &amp; Financial Info</a></li>
<style>
    #patient-and-financial-info{width:120px !important;}
</style>
		</div><div id="cpPortableNavigation85" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/patient.aspx" target="_self">Patient Information</a><ul><li class="featured-service-style"><a href="/main/patient-opt-out.aspx" target="_self">Fundraising Opt-Out</a></li><li class="featured-service-style"><a href="/main/patient-mymclaren.aspx" target="_self">MyMcLaren Chart</a></li><li class="featured-service-style"><a href="/main/patient-notice-privacy.aspx" target="_self">Notice of Privacy Practices</a></li><li class="featured-service-style"><a href="/main/patient-donation.aspx" target="_blank">Online Donation</a></li><li class="featured-service-style"><a href="/main/patient-pre-registration.aspx" target="_self">Patient Pre-Registration</a></li><li class="featured-service-style"><a href="/main/patient-appointment.aspx" target="_self">Schedule Appointments</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/financial.aspx" target="_self">Financial Information</a><ul><li class="featured-service-style"><a href="/main/financial-billing1.aspx" target="_self"> Pay Your Bill Online</a></li><li class="featured-service-style"><a href="/main/financial-insurance.aspx" target="_self">Insurance Information</a></li><li class="featured-service-style"><a href="/main/financial-insurance-veterans.aspx" target="_self">Veterans Choice Insurance</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/visitor.aspx" target="_self">Visitor Information </a><ul></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation77&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation77&#39;).hide();" id="cpPortableNavigationbf4fa1a8-3c75-4ea8-97c5-67f9504e6f30">
			<li class="health-and-wellness"><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
		</div><div id="cpPortableNavigation77" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-your-health.aspx" target="_self">Your Health</a><ul><li class="featured-service-style"><a href="/main/healht-clinical-trials.aspx" target="_self">Clinical Trials</a></li><li class="featured-service-style"><a href="/main/health-community-needs.aspx" target="_self">Community Health Needs Assessment</a></li><li class="featured-service-style"><a href="/main/health-information.aspx" target="_self">Health Information</a></li><li class="featured-service-style"><a href="/main/health-mymclaren-chart.aspx" target="_self">My McLaren Chart</a></li><li class="featured-service-style"><a href="/main/health-patient-education.aspx" target="_self">Patient Education</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-classes-programs.aspx" target="_self">Classes & Programs</a><ul><li class="featured-service-style"><a href="/main/health-classes.aspx" target="_self">Classes and Events</a></li><li class="featured-service-style"><a href="/main/health-support-groups.aspx" target="_self">Support Groups</a></li></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigation52587aa1-b090-4537-9c98-40261a490ab3">
			<li class="our-facilities"><a href="/Main/locations.aspx">Our Facilities</a></li>
		</div></td>
	</tr>
</table>
</ul>
</div>
<div style="clear:both;"></div>
</div>
<!-------Logo Row------>
</header>
<div id="mobile-navigation-container" style="display:none;">
<ul class="mobile-menu">
    <li>
    <div class="mobile-navigation-accordion">Services</div>
    <div class="mobile-navigation-content"><p><a href="/Main/acute-care.aspx">Acute Care</a></p><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p><p><a href="/Main/fitness-and-health.aspx">Fitness</a></p><p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/home-care.aspx">Home Care</a></p><p><a href="/Main/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging</a></p><p><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p><p><a href="/Main/infusion-services.aspx">Infusion</a></p><p><a href="/Main/laboratory-services.aspx">Lab</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatrics</a></p><p><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/podiatry-services.aspx">Podiatry</a></p><p><a href="/Main/proton-therapy.aspx">Proton Therapy</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotics</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep</a></p><p><a href="/Main/spine-services.aspx">Spine</a></p><p><a href="/Main/stroke-services.aspx">Stroke</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p><p><a href="/Main/surgery-services.aspx">Surgery </a></p><p><a href="/Main/trauma-services.aspx">Trauma</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/womens-services.aspx">Women's Services</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div>
    </li>
    <li><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
    <li>
    <div class="mobile-navigation-accordion">Patient &amp; Visitor Info</div>
    <div class="mobile-navigation-content">
    <p><a href="/Main/patient.aspx">Patient Information</a></p>
    <p><a href="/Main/Financial.aspx">Financial Information</a></p>
    <p class="mobile-navigation-link-visitor"><a href="/Main/Visitor.aspx">Visitor Information</a></p>
    </div>
    </li>
    <li><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
    <li><a href="/Main/locations.aspx">Our Facilities</a></li>
    <li><a href="/Main/foundation.aspx">Ways To Give</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren.aspx">About Us</a></li>
    <li class="top-nav-mobile"><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li class="top-nav-mobile" ><a href="/main/patient-appointment.aspx">Book an Appointment</a></li>
    <li class="top-nav-mobile"><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li class="top-nav-mobile"><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li class="top-nav-mobile"><a href="/Main/career.aspx">Careers</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
</ul>
</div>
	
	<div class="cpweb_PerimeterMiddle">
		<div id="blPerimiter" class="cpsys_Block">
	
			
			<div id="tdPerimeterCenter" class="cpsys_BlockColumn">
		<div id="divWrapper" class="cpweb_Wrapper">
	<div id="cphBody_divTop" class="cpsty_Top">
		
		
		<div id="cphBody_divTopAc2" class="cpsty_SiteTypes_Default_TopAc2"><div id="cpsys_Advertisers_af343aee-a245-4e1d-9da4-6d08bbbd30c5" style="text-align:left;">
	<div class="breadcrumb-trail"><div class="Breadcrumb"><span><a href="/main/home.aspx" target="_self">Home</a> &gt; </span><span><span>Website Privacy Policy</span></span></div></div>
</div></div>
	</div>
	<div style="clear:both;">
		<div id="cphBody_blSiteType" class="cpsys_Block cpsty_blSiteType">
			
			<div id="cphBody_tdLeft" class="cpsys_BlockColumn cpsty_LeftTd">
				
				<div class="cpsty_Left">
					
					<div id="cphBody_divLeftNav" class="cpsty_SiteTypes_Default_LeftNav"></div>
					
					
				</div>
			
			</div>
			
			<div id="cphBody_tdCenter" class="cpsys_BlockColumn cpsty_CenterTd" style="width: 98%;">
				
				<div id="cphBody_divCenter" class="cpsty_Center">
					<div id="cphBody_divCenterAc1" class="cpsty_SiteTypes_Default_CenterAc1"><div id="cpsys_Advertisers_bf3115e5-4271-4bad-91ed-a4aa140e412a" style="text-align:left;">
	<style>
    #data-source-created{display:none;}
    #service-line-datasource{display:none;}
</style>
<div class="left-rail-nav-container">
<ul parentsystemtype="FirstTierList" id="site-nav"><li class="liParent"><a href="/main/mclaren.aspx" target="_self" class="d173dbee-de0f-4132-ba5e-76a09f076348">About Us</a><ul><li class="liParent"><a href="/main/mclaren-awards.aspx" target="_self" class="64b1e7ee-84b2-4587-afe5-5d74ca900868">Award Winning Care</a></li><li class="liParent"><a href="/main/mclaren-community-assessment.aspx" target="_self" class="f0856316-de0f-442c-8207-78ea6ee5b5ce">Community Health Needs Assessment</a></li><li class="liParent"><a href="/main/mclaren-contact-us.aspx" target="_blank" class="25423667-c62c-4a08-89f5-0296d2ca22f8">Contact Us</a></li><li class="liParent"><a href="/main/mclaren-history.aspx" target="_self" class="6c6781bf-645e-4190-9b15-b1df37f62521">History</a><ul><li class="liParent"><a href="/main/mclaren-br-history.aspx" target="_self" class="bff09b68-e652-41dd-8b45-a963dda7c15d">McLaren Bay Region</a></li><li class="liParent"><a href="/main/mclaren-cm-history.aspx" target="_self" class="023cada0-e4d5-4d9b-9da1-81c3b5ca94b6">McLaren Central Michigan</a></li><li class="liParent"><a href="/main/mclaren-flint-history.aspx" target="_self" class="d8639099-ce6a-4ead-9139-a59962d5d3db">McLaren Flint History</a></li><li class="liParent"><a href="/main/mclaren-gl-history.aspx" target="_self" class="1a368f0f-f80d-41d5-9f49-d8227cb5b09d">McLaren Greater Lansing</a></li><li class="liParent"><a href="/main/mclaren-hg.aspx" target="_self" class="9177539c-3814-4aa1-b948-718e538bed6b">McLaren Homecare Group</a></li><li class="liParent"><a href="/main/mclaren-macomb-history.aspx" target="_self" class="787adc33-1be5-4654-a1e1-b00f7748edb4">McLaren Macomb</a></li><li class="liParent"><a href="/main/mclaren-nm-history.aspx" target="_self" class="f8821150-597e-48dc-b092-7853def059fb">McLaren Northern Michigan</a></li><li class="liParent"><a href="/main/mclaren-oakland.aspx" target="_self" class="1f97c3e8-205c-45f2-899f-cdd5dcabed17">McLaren Oakland</a></li></ul></li><li class="liParent"><a href="/main/mclaren-blog.aspx" target="_self" class="79a09704-9964-497f-b21c-29a720645b6f">Blog</a></li><li class="liParent"><a href="/main/mclaren-executives.aspx" target="_self" class="84efe6f0-cf94-4b18-9e0b-9ce8374387c1">Meet the Executive Team</a></li><li class="liParent"><a href="/main/mclaren-message.aspx" target="_self" class="d97677db-b368-4c2d-b08a-a3c7a2a3a306">Message to Our Community</a></li><li class="liParent"><a href="/main/mclaren-mission.aspx" target="_self" class="212a2d08-6f76-49f5-b5f0-9b2ad17f88d8">Mission Statement</a></li><li class="liParent"><a href="/main/mclaren-publications.aspx" target="_self" class="a424848b-5c16-49fe-ba74-05c565299104">Publications</a></li><li class="liParent"><a href="/main/mclaren-research.aspx" target="_self" class="8c52cd2f-a2bd-40fe-bff6-a7fd50955fff">Research Studies</a></li><li class="liParent"><a href="/main/mclaren-service-area.aspx" target="_self" class="df08959d-e863-4e26-a7b3-e13754ca9049">Service Area  </a></li></ul></li></ul>
</div>
</div></div>
					
					<div id="cphBody_divContent" class="cpsty_SiteTypes_Default_Content">
	
	
			
<!--cpsys_Template:cpsys_Register-->
<!--cpsys_Template:cpsys_Register-->


<!--cpsys_Template:DetailsHeaderContent-->
<div class="left-rail-nav-container" id="service-line-datasource">

</div>
<div class="main-content">
<!---Shows Location Info--->

<style> 
/*----Styles For SubPages-----*/
.cpsty_CenterTd{width:100% !important;}.cpsty_Center h1{margin-top:0px;}.site-banner .inner-container{min-height:115px;}.site-banner .inner-container h1{line-height:115px; font-size:34px;}.site-banner .inner-container h1.multi-line{line-height:normal !important; font-size:34px !important; height:100px !important;}.site-banner{height:173px; background-color:#2f82b6; background-image:none;} .site-banner .inner-container .site-banner-logo{display:none} .service-location-on-landing-page-top{display:none;} .service-location-on-landing-page{display:none;} .left-rail-nav-container{display:block; width:24.5% !important; margin-right:2%; float:left; border:solid 1px #cccccc;} .main-content{width:72.5%; float:left;} @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}
/*----Styles For SubPages-----*/
</style>

<h1 class="pageheader">Website Privacy Policy</h1>
<div class="service-location-on-landing-page-top"></div>
<p>McLaren Health Care Corporation cares about privacy issues and wants you to be familiar with how we collect, use and disclose Personally Identifiable Information you provide to us.</p>
<p>This Privacy Policy and Terms (the “Policy”) describes our practices in connection with Personally Identifiable Information that we collect through our website located at www.mclaren.org, the websites of our subsidiaries that display or directly link to this Policy, and the e-mail messages that we send to you that directly link to this Policy (collectively, the “Services”).</p>
<p>There are a number of circumstances where this Policy does not apply and we want to identify a few of them for you:</p>
<ul>
    <li>This Policy does not apply to the information you share when receiving treatment from your medical provider, including when you are receiving medical services from us. For example, this Policy would not apply to the information you share with us when admitted to the emergency room. The use and disclosure of the information you provide in such circumstances is governed by the Federal Health Insurance Portability and Accountability Act of 1996, more commonly known as HIPAA, as well as Michigan law. You can learn more about your HIPAA rights and protections and our obligations in our Notice of Privacy Practices.</li>
    <li>This Policy does not apply to the information you share and your use of the <em>My McLaren Chart</em>, which is the patient portal we offer. The use and disclosure of the information you provide when using the <em>My McLaren Chart</em> is governed by HIPAA, Michigan state law, as well as the terms and conditions and other policies you agreed to when registering to access the <em>My McLaren Chart</em> service.</li>
    <li>This Policy does not apply to the practices of companies that we do not own or control. Other services and organizations that we link to may have their own privacy policies governing the use of information you provide to them. For example, organizations such as Facebook, Twitter and LinkedIn each have their own privacy policy and practices.</li>
    <li>This Privacy Policy does not apply to individuals whom we do not employ, including any of the third parties to whom we may disclose user information as set forth in this Privacy Policy.</li>
</ul>
<p>If you have any questions or comments about this Policy or our privacy practices, please contact us by using one of the means listed on our <a href="/main/mclaren-contact-us.aspx">Contact page</a></p>
<ol>
    <li><a><strong>Personally Identifiable Information We May Collect</strong></a>
    <p>“Personally Identifiable Information” is information that identifies you as an individual, such as name, birth date, telephone number, e-mail address, or unique device identifiers that was gathered in connection with your use of the Services.</p>
    <p>If we combine Personally Identifiable Information with protected health information subject to protection under HIPAA, the combined information will be treated as protected health information for as long as it remains combined.</p>
    </li>
    <li><strong>Other Information we may collect</strong>
    <p>“Other Information” is any information that does not reveal your specific identity or does not directly relate to an individual, including, for example: (1) computer or device connection information, such as browser type and version, operating system type and version, device information, and other technical identifiers; (2) information collected through cookies and other technologies; or (3) aggregated information, such as usage history and search history.</p>
    <p>If we combine Other Information with Personally Identifiable Information, the combined information will be treated as Personally Identifiable Information for as long as it remains combined.</p>
    </li>
    <li><strong>Security Measures</strong>
    <p><strong>Virus Detection and E-mail Security:</strong>&nbsp;For website security, we use software programs to monitor traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage.&nbsp; </p>
    <p><strong>General Practices: </strong>Although we seek to use reasonable measures to protect Personally Identifiable Information, please be aware that no security measures are perfect or impenetrable. Therefore, <a>we cannot and do not guarantee that the information you provide to us through the Services&nbsp;</a>will not be viewed by unauthorized persons. We are not responsible for circumvention of any privacy settings or security measures contained on the Services.</p>
    <p><strong>Security of Service Communications</strong>: Some communication through the Services may be sent through the standard HTTP protocol and may be delivered using regular e-mail. Information sent over HTTP is not encrypted. E-mail, while convenient, also poses several risks (e.g., e-mail is not a secure form of communication, is unreliable, can be forwarded, etc.). We cannot guarantee the security of the information sent through such means, nor can we guarantee that information you supply to us will not be intercepted while being transmitted to us. It is important for you to protect against unauthorized access to your computer and to take appropriate security measures to protect your information. We use encryption technology, such as Secure Sockets Layer (SSL), to protect your protected health information during data transport with <em>My McLaren Chart</em>.</p>
    <p><strong>Your Obligations to Safeguard your Information</strong>: Security is not a one person job. You must also take reasonable measures to protect your information, including, for example, securing your computer and mobile device, using an antivirus software, using a firewall, and other similar safeguards.</p>
    </li>
    <li><strong>Disclaimer</strong>
    <p>McLaren Health Care Corporation and its subsidiaries offer information on the Services for general educational purposes only. This information should not be used for diagnosis and treatment, nor should it be considered a replacement for counsel with your physician or other health care professional. If you have questions or concerns about your health, contact your health care provider. We encourage you to use the Physician Finder to locate a physician by specialty and area.</p>
    <p>While we and our subsidiaries make reasonable effort to ensure accuracy of the information on the Services, we do not guarantee the accuracy, and the information is provided with no warranty or guarantee of any kind.</p>
    </li>
    <li><strong>Links</strong>
    <p>We and our subsidiaries provide links to other websites. These links are provided as a convenience to you and as an additional avenue of access to the information contained on such third-party websites. Different terms and conditions may apply to your use of any linked sites. We encourage you to read the privacy policy of each website.&nbsp; We have no control over third party websites and make no claim or representation regarding such websites. We accept no responsibility for, the quality, content, nature, or reliability of any websites accessible by hyperlink from the Services, or websites linking to the Services.&nbsp; We are not responsible for any losses, damages or other liabilities incurred as a result of your use of any linked sites.</p>
    </li>
    <li><strong>Corrections, unsubscribe and data retention</strong>
    <p>You may update the information you provide to us through the Services. To change your information, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>. If you are subscribed to one of our newsletter, you may unsubscribe from receiving the newsletters by clicking on the appropriate link at the bottom of the e-mail. We may, from time to time, send you administrative messages. You cannot opt-out from receiving administrative messages.</p>
    <p>We will retain your information for as long as needed to provide you services, comply with our legal obligations, resolve disputes, and enforce our agreements. Except for authorized law enforcement investigations, other valid legal processes or as described in this Policy (or another policy in place between you and us), we will not share any Personally Identifiable Information we receive from you with any parties outside of McLaren and its subsidiaries.</p>
    </li>
    <li><strong>Changes to the Privacy Policy and Terms</strong>
    <p>We may change this Policy from time to time. Please take a look at the “Last Modified” legend at the top of this page to see when this Policy was last revised. Any material changes to this Policy will become effective 7 days from when we post the revised Policy on the Services and all other changes to this Policy will become effective when we post the revised Policy on the Services. Your use of the Services following the effective date means that you accept the revised Policy. If you have questions or comments about this Policy, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>.</p>
    <p><a><strong>McLaren Foundations:</strong></a>&nbsp; Some subsidiaries uses Internet donors' technology provided by Blackbaud, Inc. This is encrypted technology and allows financial donations to be forwarded electronically using a secure server.</p>
    <p>McLaren Health Care Corporation and its subsidiaries use oxcyon.com as a host server and vendor, the content management system is Central Point.</p>
    </li>
    <li><strong>Use of Services by Minors</strong>
    <p>We do not knowingly collect Personally Identifiable Information from individuals under the age of 13 and the Services are not directed to individuals under the age of 13. We request that these individuals not to use the Services.</p>
    </li>
    <li><strong>International Visitors</strong>
    <p>The Services are controlled and operated from the United States, and are not intended to subject us to the laws or jurisdiction of any state, country or territory other than that of the United States. If any material on the Services, is contrary to the laws of the place where you are when you access them, then we ask you not to use the Services. You are responsible for informing yourself of the laws of your jurisdiction and complying with them. By using the Services, you consent to the transfer of information to the United States, which may have different data protection rules than those of your country.</p>
    </li>
    <li><strong>Use of Materials on the Services, Trademarks and Copyrights </strong>
    <p>You acknowledge and agree that all content on the Services (including, without limitation, text, images, user interfaces, visual interfaces, graphics, trademarks, logos, sounds, source code and computer code, including but not limited to the design, structure, selection, coordination, expression, ‘look and feel’ and arrangement thereof) is the exclusive property of and owned by us or our licensors and is protected by copyright, trademark, trade dress and various other intellectual property rights and unfair competition laws. These marks and copyrights may not be copied, imitated, or used, in whole or in part, without the express prior written permission from their respective owners, and then with the proper acknowledgments. Nothing on the Services shall be construed as granting, by implication, estoppel, or otherwise, any license or right to use any trademark, logo or service mark displayed on the Services without the owner’s prior written permission, except as otherwise described herein.</p>
    <p>You may access, copy, download, and print the material (such as, for example, educational materials, service descriptions, and similar materials) purposely made available by us for downloading from the non-secured component of the Website (“General Website”) for your personal, non-commercial use and for your business use in connection with evaluating McLaren healthcare provider services for offer by your health plan or use by your employees, provided you do not (i) modify or delete (including through selectively copying or printing material) any copyright, trademark, or other proprietary notice that appears on the material, and (ii) make any additional representations or warranties relating to such materials.</p>
    <p>You may access, copy, and print the material contained in the secured component of the Website in accordance with the terms and conditions governing the secure section.</p>
    <p>Any other use of content or material on the Services, including but not limited to the modification, distribution, transmission, performance, broadcast, publication, uploading, licensing, reverse engineering, encoding, transfer or sale of, or the creation of derivative works from, any material, information, software, documentation, products or services obtained from the Services, or use of the Services is expressly prohibited.</p>
    <p>We, our licensors, or content providers, retain full and complete title to any and all materials provided on the Services, including any and all associated intellectual property rights. </p>
    <p>As long as you comply with this Policy, we grant you a personal, non-exclusive, non-transferable, revocable, limited privilege to enter and use the Services. We reserve the right, without notice and in our sole discretion, to terminate your license to use the Services and to block or prevent future access to and use of the Services.</p>
    </li>
    <li><strong>Submissions and Postings</strong>
    <p>To the extent that we allow submissions on the Services, you acknowledge that you are responsible for any material you may submit via the Services, including the copyright, legality, reliability, appropriateness, and originality of any such material.</p>
    <p>You represent and warrant (and we rely on your representation and warranty) that you (i) own or otherwise control all the rights or have sufficient rights to the content you post or that such items are known to you to be in the public domain; (ii) that the content is accurate; (iii) that use of the content you supply does not violate any provision in this Policy or terms you may have agreed to with a third party; (iv) that the content is not defamatory or otherwise trade libelous; (v) does not violate any law, statute, ordinance or regulation; and (vi) that you will indemnify us for all claims resulting from content you supply, including arising from an action alleging infringement of copyright or other proprietary rights in such work.</p>
    <p>We undertake no duty to determine the validity of any claim of copyright or trademark infringement. Upon receiving written notice that any item posted on the Services is believed to infringe a copyright or other proprietary right, we will remove said work.</p>
    <p>If you do submit material, you grant us and our affiliates an unrestricted, nonexclusive, royalty-free, perpetual, irrevocable, transferable and fully sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute and display any and all material not subject to protections under HIPAA throughout the world in any media. You further agree that we are free to use without limitation and without any compensation to you any ideas, concepts, or know-how that you or individuals acting on your behalf provide to us. You grant us the right to use the name you submit in connection with such material.&nbsp; We retain any and all rights granted in this Policy in and to any user submitted content or non-HIPAA materials after termination, notwithstanding the reason for any such termination.</p>
    <p>We have an absolute right to remove any material from the Services in our sole discretion at any time.</p>
    </li>
    <li><strong>Usage Rules</strong>
    <p>You hereby agree to not upload, distribute, or otherwise publish through the Services any content that (i) is unlawful, libelous, defamatory, obscene, pornographic, harassing, threatening, invasive of privacy or publicity rights, fraudulent, defamatory, abusive, inflammatory, or otherwise objectionable; (ii) is confidential, proprietary, incorrect, or infringing on intellectual property rights; (iii) may constitute or encourage a criminal offense, violate the rights of any party or otherwise give rise to liability or violate any law; or (iv) may contain software viruses, chain letters, mass mailings, or any form of “spam.”&nbsp; You may not use a false email address or other identifying information, impersonate any person or entity or otherwise mislead as to the origin of any content.&nbsp; You may not upload commercial content onto our Services.</p>
    <p>You expressly agree to refrain from doing either personally or through an agent, any of the following:&nbsp; (1) use any device or other means to harvest information; (2) transmit, install, upload or otherwise transfer any virus or other item or process to the Services that in any way affects the use, enjoyment or service of the Services, or any visitor’s computer or other medium used to access the Services; (3) engage in any action which we determine in our sole discretion is detrimental to the use and enjoyment of the Services; or (4) transmit, install, upload, post or otherwise transfer any information in violation of the laws of the United States. You may further not use any hardware or software intended to damage or interfere with the proper working of the Services or to surreptitiously intercept any system, data, or personal information from the Services. You agree not to interrupt or attempt to interrupt the operation of the Services in any way.</p>
    </li>
    <li><strong>Infringement Notice</strong>
    <p>We respect the intellectual property rights of others and request that you do the same. If you believe your copyright or the copyright of a person on whose behalf you are authorized to act has been infringed, you may notify us in writing to the e-mail address or mailing address provided in the “How to Contact the Web Team” section below with attention to Copyright Agent. To be effective, your notification must be in writing, include your contact information, provided to our copyright agent, and include:&nbsp; (i) signature of a person authorized to act; (ii) identification of the copyrighted work claimed to have been infringed; and (ii) identification of the material that is claimed to be infringing including references to the location of the material on the Services.</p>
    <p>If you believe other intellectual property rights were violated, you may notify us in writing to the mailing address provided in the “How to Contact the Web Team” section below with attention to General Counsel.</p>
    </li>
    <li><strong>Jurisdiction and Applicable Law</strong>
    <p><strong>The laws of the State of Michigan, </strong>without regard to any conflicts of laws principles thereof,<strong> will govern the construction and interpretation of this Policy and the rights of the parties hereunder. By accessing, using, or registering for the Services, you acknowledge that you have read, understood, and agreed to be bound by this Policy and by all applicable laws and regulations. </strong>The Parties agree on behalf of themselves and any person claiming by or through them that the exclusive jurisdiction and venue for any action or proceeding arising out of or relating to this Agreement will be an appropriate state or federal court located in Michigan and each party irrevocably waives, to the fullest extent allowed by applicable law, the defense of an inconvenient forum.<strong></strong></p>
    </li>
    <li><strong>Severability and Waiver</strong>
    <p>Our failure to exercise or enforce any right or provision of this Policy will not constitute a waiver of such right or provision. If any provision of this Policy is unlawful, void, or unenforceable, for any reason, the remaining provisions will remain in full force and effect to the fullest extent of the law.<strong></strong></p>
    </li>
    <li><strong>How to Contact the Web Team</strong> E-mail:&nbsp;<strong><a href="http://mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong> Fax:&nbsp;(989) 891-8185</li>
</ol>
<ul>
    <li><strong>We respect your privacy and do not collect Personally Identifiable Information through the Services unless you choose to provide it. We, and our service providers, may collect Personally Identifiable Information in a variety of ways, including when</strong>:
    <ul>
        <li><a>You contact us to request information by sending us an e-mail</a>&nbsp;; </li>
        <li>You participate in one of our training or education events, such as a childbirth education, nutrition education or exercise &nbsp;training seminars;</li>
        <li>You request one of our publications or newsletters;</li>
        <li>You participate in a Service related survey, contest, or other promotion; or</li>
        <li>You complete a questionnaire(s) on the Services (<em>e.g.</em>, did you find our website helpful?)</li>
    </ul>
    </li>
    <li><strong>We may use personally identifiable information</strong>:
    <ul>
        <li>To send administrative information, such as information regarding the Services and changes to our terms, conditions, or policies;</li>
        <li>To respond to your inquiries and fulfill your requests;</li>
        <li>If you enter into a contest or similar promotion we may use the information you provide to administer those programs;</li>
        <li>We may use survey information for research and quality improvement purposes, including helping us to improve information and services offered through the Services;</li>
        <li>For our business purposes, such as improving or modifying our Services, identifying usage trends, and operating and expanding our service and information offerings;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>Your Personally Identifiable Information may be disclosed</strong>:
    <ul>
        <li>To identify you to anyone to whom you send messages through the Services;</li>
        <li>To our third-party service providers that provide services such as website hosting, information technology and related infrastructure, customer service, and other similar services;</li>
        <li>To a third-party in the event of any reorganization, merger, sale, joint venture, assignment, transfer or other disposition of all or any portion of our business or assets;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>California Do Not Track Notice</strong>:
    <ul>
        <li>We do not track you over time and across third party websites to provide targeted advertising and therefore do not respond to Do Not Track (DNT) signals;</li>
        <li>Third parties that have content embedded on our website, such as a social networking connectors (<em>e.g.</em>, Facebook, Twitter) set cookies in your browser as well as obtain information about the fact that a web browser visited the Services from a certain IP address.</li>
    </ul>
    </li>
</ul>
<ul>
    <li><strong>How We May Collect Other Information</strong>: We, and our third-party service providers, may collect Other Information in a variety of ways, including:
    <ul>
        <li><em>Through your browser or mobile device</em>: Certain information is automatically collected by most browsers or through your mobile device, such as your computer type, screen resolution, operating system name and version, device manufacturer and model, language, and Internet browser type and version. We may also collect information on the search terms you used to find our website, the search engine you used, or the address of the web site from which you came to visit.</li>
        <li><em>Using cookies</em>: Our Services may use cookies and other technologies such as pixel tags and web beacons. These technologies help us provide better services to you, tell us which parts of our Services people have visited, and allow us to better measure the usability of our Services. We treat information collected by cookies and other technologies as non-personal information. You have a variety of tools to control cookies and similar technologies, including controls in your browser to block and delete cookies.</li>
        <li><em>IP Address</em>:&nbsp; Your “IP Address” is a number that is automatically assigned to the computer that you are using by your Internet service provider (ISP). An IP Address may be identified and logged automatically in our server log files, or those of our website hosting vendor, whenever a user accesses the Services, along with the time of the visit and the page(s) that were visited.</li>
        <li><em>By aggregating information</em>:&nbsp; Aggregated Personally Identifiable Information does not personally identify you or any other user of the Services. We may aggregate information for a variety of reasons, for example, to calculate the percentage of our users who visit a particular page or clicked on a particular item from a newsletter.</li>
        <li><em>Google Analytics</em>: We use a service from Google called “Google Analytics” to collect information about use of the Services. Google Analytics collects information such as how often users visit this site, what pages they visit when they do so, and what other sites they used prior to coming to this site. Google Analytics collects only the IP address assigned to you on the date you visit this site, rather than your name or other identifying information. Google’s ability to use and share information collected by Google Analytics about your visits to our Services is restricted by the Google Analytics Terms of Use and the Google Privacy Policy. We may use the information from Google Analytics for a variety of reasons including trend analysis and to make our Services more useful to the communities we serve.</li>
    </ul>
    </li>
    <li><strong>How We May Use and Disclose Other Information</strong>
    <ul>
        <li>We may use and disclose Other Information for any purpose, except where we are required to do otherwise under applicable law.</li>
        <li>If we are required to treat Other Information, such as IP addresses or other similar identifiers, as Personally Identifiable Information under applicable law, then we may use it as described in “How We May Collect Other Information” section above, as well as for all the purposes for which we use and disclose Personally Identifiable Information, but we will treat these identifiers as Personally Identifiable Information. If we are require to treat Other Information as protected health information as defined by HIPAA, then we will treat the identifiers in accordance with our Notice of Privacy Practices.</li>
    </ul>
    </li>
</ul>
<p><strong>Mail:</strong><strong><br>
</strong>McLaren Bay Region<br>
Corporate WebMaster<br>
1900 Columbus Avenue<br>
Bay City, MI 48708</p>
Last modified:  5/2/16
<!-----Related Links------>


<div class="subpage-info">
<!----Related Photo Gallery---->
<!----Related Documents---->
<!----Related Videos------->
<!----Related Links-------->
<!----Related Blogs-------->
</div>
</div>
<!-----------------Find Here----------------->
<!--cpsys_Template:DetailsHeaderContent-->


		
	

<!--cpsys_Template:DetailsFooterContent-->

<!--cpsys_Template:DetailsFooterContent-->


		
	


</div>
					
				</div>
			
			</div>
			<div id="cphBody_tdRight" class="cpsys_BlockColumn cpsty_RightTd">
				
				<div class="cpsty_Right">
					
					
					<div id="cphBody_divRightAc2" class="cpsty_SiteTypes_Default_RightAc2"><div id="cpsys_Advertisers_f3226af2-3ced-4f64-a3ab-844e6284e705" style="text-align:left;">
	<table cellspacing="0" cellpadding="0" class="cpsys_Table" style="height: 400px;">
    <tbody>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
</div></div>
				</div>
			
			</div>
		
		</div>
	</div>
	
</div>
	</div>
			
		
</div>
    </div>
    
	<footer>
<div class="footer-social-outer-container">
<div class="footer-social-inner-container">
<ul>
    <li><a href="https://www.facebook.com/McLarenHealth"><img style="" src="/Uploads/Public/Images/Designs/footer-facebook.png"></a></li>
    <li><a href="https://twitter.com/mclarenhealth"><img style="" src="/Uploads/Public/Images/Designs/footer-twitter.png"></a></li>
    <li><a href="https://www.linkedin.com/company/mclaren-health-care"><img style="" src="/Uploads/Public/Images/Designs/footer-linkedin.png"></a></li>
    <li><a href="https://www.youtube.com/user/mclarenhealth"><img style="" src="/Uploads/Public/Images/Designs/footer-social-red.png"></a></li>
    
    
</ul>
</div>
</div>
<div class="outer-container">
<div class="inner-container mobile-hide">
<nav class="btm">
<div class="col-1">
<ul>
    <li><a href="/Main/Career.aspx">Careers</a></li>
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a href="/Main/mclaren-web-privacy-policy.aspx">Website Privacy Policy</a></li>
    <li><a href="/Main/health-community-needs.aspx">Community Health Needs Assessment</a></li>
    <li><a href="/Main/Sitemap.aspx">Site Map</a></li>
</ul>
</div>
<div class="col-2"><img src="/Uploads/Public/Images/Designs/FooterLogos/footerlogowhite_McLarenHealthCare.png"><br>
<br>
McLaren Health Care<br>
<br>
Flint, MI 48532<br>
<span style="font-size: 30px;"></span></div>
<div class="col-3">
<ul>
    <li><a href="/Main/employee.aspx">For Employees</a></li>
    <li><a href="/Main/media1.aspx">For Media</a></li>
    <li><a href="/Main/physician.aspx">For Physicians</a></li>
    <li><a href="/Main/vendor-registration.aspx">For Vendors</a></li>
    <li><a href="/Main/volunteer.aspx">For Volunteers</a></li>
    <li><a href="/Main/gme.aspx">For Medical Education</a></li>
    <!------------- main, bayregion, macomb, oakland, flint, lansing ---------GME Link-------->
</ul>
</div>
</nav>
</div>
<div class="btm-ribbon">
<div class="inner-container">
<p>McLaren Health Care, through its subsidiaries, will be the best value in health care as defined by quality outcomes and cost.</p>
<p>©All rights reserved.  McLaren Health Care and/or its related entity</p>
<div style="clear:both;"></div>
</div>
</div>
</div>
</footer>
    
	<div class="dv-bottom-edit-liks">
	
	
	
	
	
	</div>
	<div id="uprgUpdateProgress" style="display:none;">
					<table border="0" cellpadding="0" cellspacing="0" class="updateProgress" style="position:fixed; top:0px; right:0px; border: solid 1px #CCCCCC; background-color:#F2F2F2;">
				<tr>
				<td style="vertical-align: middle; padding: 2px;"><img src="/Integrations/Centralpoint/Resources/ProgressIcon.gif" alt="Loading..." /></td>
				<td style="vertical-align: middle; padding: 2px;">Loading...</td>
				</tr>
				</table>
			
</div>
    <input type="hidden" name="ctl00$ctl00$FormAction" id="FormAction" value="0" /><input type="hidden" name="ctl00$ctl00$FormGroup" id="FormGroup" /><input type="hidden" name="ctl00$ctl00$FormButton" id="FormButton" />

<script type="text/javascript">
//<![CDATA[

 //Admin > Properties: HeaderStartupScripts 
$('table#DynamicNavigation1 img[src="/Uploads/Public/Images/arrow.png"]').attr('alt', 'arrow');

 //End of Admin > Properties: HeaderStartupScripts 
$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show().siblings().show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('#cpPortableNavigation73').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation73').unbind('mouseleave');
});
$('#cpPortableNavigation73 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation85').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation85').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation85').unbind('mouseleave');
});
$('#cpPortableNavigation85 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation85').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation77').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation77').unbind('mouseleave');
});
$('#cpPortableNavigation77 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
});

                    $(document).ready(function() {
                    $('.mobile-navigation-accordion').cp_Accordion({collapsable : true, active : -1, headerClass : 'mobile-navigation-accordion', contentClass : 'mobile-navigation-content', event : 'click', expandedSpanText : '', collapsedSpanText : '', expandCollapsePosition : 'left', expandedHeaderText : '', collapsedHeaderText : '' });
                    });
					if ($('input[name="HtmlSearchCriteria"]').length > 0) {
						$('input[name="HtmlSearchCriteria"]').autocomplete({
							source: function(request, response) {
								$.ajax({
									type: "POST",
									url: "/WebServices/ClientMethods.asmx/SiteSearchAutoComplete",
									data: "{ \"term\": \"" + request.term + "\" }",								
									dataType: "json",
									contentType: "application/json; charset=utf-8",
									success: function(data) { response(data.d) },
									error: function(XMLHttpRequest, textStatus, errorThrown) { /*alert(textStatus + '\n' + errorThrown);*/ }
								});
							},
							select: function( event, ui ) {
								$('input[name="HtmlSearchCriteria"]').val(ui.item.value);
								$('input[name="HtmlSearchCriteria"]').siblings('input[type="button"], input[type="submit"]').click();
							},
							autoFocus: true,
							delay: 10,
							minLength: 2
						});
					}
					//]]>
</script>
</form>
</body>
</html>
