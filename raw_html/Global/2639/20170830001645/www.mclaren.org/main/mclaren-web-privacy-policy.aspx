

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html id="html" xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head id="head"><meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link id="lnkSiteType" rel="stylesheet" type="text/css" href="/SiteTypes/Default.master.css.aspx?aud=Main&amp;rol=Public" /><title>
	Website Privacy Policy | McLaren Health Care
</title>
<!--Admin > Properties: HeaderHtml-->
<link type="text/css" href="/Uploads/Public/Documents/dropdownlinks.css" rel="stylesheet" />
<link rel="shortcut icon" href="/Resource.ashx?sn=favicon" />
<link href="/Resource.ashx?sn=slider" rel="stylesheet" type="text/css" />
<!--End of Admin > Properties: HeaderHtml-->
<!--Design > Styles (#ReDesign): HeaderHtml-->
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400,500,700,800,900,500italic,400italic,100italic,900italic,300italic,700italic,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">
<link rel="icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->

<!--[if IE 7]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE7-STYLES.css?v=2">
<![endif]-->

<!--[if IE 8]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE8-STYLES.css?v=2">
<![endif]-->

<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<!--End of Design > Styles: HeaderHtml-->
<!-- Site Architecture > Audiences (McLaren Health Care): HeaderHtml-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6396952-25', 'auto');
  ga('send', 'pageview');

</script>
  <script type="text/javascript">
    var _monsido = _monsido || [];
    _monsido.push(['_setDomainToken', 'VW4sxwq_5Jph8JUIFq9uyQ']);
  </script>
  <script src="//cdn.monsido.com/tool/javascripts/monsido.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link type="text/css" href="/Uploads/Public/Documents/mhc_custom.css" rel="stylesheet" />
<!--End of Site Architecture > Audiences-->
<meta name="robots" content="ALL, FOLLOW, INDEX" />
<meta name="rating" content="GENERAL" />
<meta name="revisit-after" content="30 days" />
<link href="/Integrations/JQuery/Themes/Stable/Root/jquery-ui.css?v8.6.6" rel="stylesheet" type="text/css" />
<style type="text/css">
/* Site Architecture > Audiences (McLaren Health Care): HeaderStyles */ 
.mobile-navigation-link-visitor{display:none;}
/* End of Site Architecture > Audiences: HeaderStyles */

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
/* Admin > Properties: HeaderStyles */ 
a.dropdownlinks:link{font-size:12px ! important;}
a.dropdownlinks:visited{font-size:12px ! important;}
a.dropdownlinks:hover{font-size:12px ! important;}
a.dropdownlinks:active{font-size:12px ! important;}
/* End of Admin > Properties: HeaderStyles */
.mobile-navigation-accordion { cursor:pointer; }
.mobile-navigation-accordion span.expanded { padding-left:17px; }
.mobile-navigation-accordion span.collapsed { padding-left:17px; }
.mobile-navigation-accordion span.expanded { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-expand.png') no-repeat center left; }
.mobile-navigation-accordion span.collapsed { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-closed.png') no-repeat center left; }
    @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
.CpButton { cursor:pointer; border:outset 1px #CCCCCC; background:#999999; color:#463E3F; font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 10px; font-weight:bold; padding: 1px 2px; background:url(/Integrations/Centralpoint/Resources/Controls/CpButtonBackground.gif) repeat-x left top; }
.CpButtonHover { border:outset 1px #000000; }
</style></head>
<body id="body" style="font-size:100%;">
    <form method="post" action="/main/mclaren-web-privacy-policy.aspx" id="frmMaster">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="HQTbcSvAWbPriCUIp0Nso34pcK7p4Rx5fzy9Eoup8KjgVeAMzCbOpVrxQenHrHA+5PXxoj0Q9Tv7QtVRwjpZ0DWqI5beyZ/l64BzMEHH5TuZYRTt/rqdyGwhfwN4PFOla6/QeygzghwNLSNFRuP8Oj2lS1QtkvNRir92CysGBcG9ni1f51LkmYRIyLHHZUJ3JS0qxKKYjpbOSyzipmw4zb/MUJ/hCiUjGP3L860/G/ed0SgyH1t0cxaonaq1Bg6irhvent6bBDf0CGfEYfES4I0V/KuK8u162P0TdMZGylHtE+KovLhRdbBEhdOP4mi3qjb812FFOMrz3CDwUPX6W9GLcg3Xj/lP8kjy53DNWprwXL2bw6s73h5igMoHGddaG/ZUZweEHtdlQXqsMXv0IPrZkicRU9GNQm2X589PiBO0R3LraYL0vx8rm9xuwrSpFsRg0zJmkgJBjrFnOWcmPtWJmbAmlklC6v1AxXrY4lzrs9wUah/Ic/xNy1V63GJhDC2yMer57mEg5klCKjNzFVaEKkykXMrirRPKui3G6rbm/dHHjsfTwUm0YiRTFrmtvRP24DzVCgUhWJ2FBXy0812hq3vbaxjvVzpDUr7Of9t/89TXNMSHnPdgPh3Bi8mH6oXw4iN7dVbu+BrwDbB4vnXUjzZjUrsEzjv/1diq/1IsIKGLh29tfoGhVbNFYb7omMMNdqwMcD3nBM7BkMVC9+9hgMd1yC6ePSKgHUCTMM5wMD7eYs5qSQcQiPTqnGawjvS3ru3tRwgq/WctNY90Z+6Dop5ZJnZcqup+C7ozH41q2kYU1e5znm3Fw6vTjQdsANcZEIa523hy9SuPAAdx7NVbZtc1I2y1ccdC00HTPa/6/i3dLGnz5TDslVjZhpkGMu7MVckG05EmXKoI+ONNDQgfSdyZHBZZ2Lv+ZjQ3PvbukuN08cbQA3ysb+ca+Qk51Q6nlna/zD7IKVXqg63qkNfAfnO8Byi0pck8GGJUWNxpv4FYHEbmQ04yntTc8OSqCPNRcE7rmUsHiDsXydgYL/zEce5WmdKyEaQdXlWsfyjdZ7fTNnjuh37yAG5DagOd0OhSuUPgf12AUDwbB43hvhWM6qGEbhSeOLFqePqaJUSI0fVVfOZTEhBlHugXVrP+PshWv3Q752h0uckpWeSWZgLAnOGjKw+38q29mwuscp1jXGdkI2EXYVRG8sSbhNr13TEY0d41Isc/W3LwwUKJY7ka56Iu+bONBcPZtH9Jo4Psp42R4+M2WIFK2mPae1LP9K+Jbfd++8hV+Xb2Lgsc7lVEY0wap0W3PB/r61+tI6wFMQv2JTYlAxlN6Hz2k/wIWQuh64c9XI4H4y3WjLp8q19EJ6nLmVdIPAyF9iziH4DHK+AuZYWTvUVDLeC6aNg4fA5UKYV6GzjEfQcv8dNazyZyvuNVpdXXxo/QSp/CAZA4DEfZztbui1UUATxoKT2Ice8tjkutsNNRQOrIW6kWyoot9nAA53nzYeyf1iH9aQdOi7/0IaTNbX0i73ScxynAhidBEm29s6GOpRV5JOGL0sXmKn0B3D0UCV6qOVjN0hpqULGR4KDMDtpvILqxhSzYnocxKTX/3Yw2KwJgdw1Nh4rrEdP7++mxuawrw/aBwGMN3E9DJNBPTHhBCrqokqppkC4qxKHTVQ3VvNdls4tpznzf8ug+9Ykcd4UpjppVYV+oCfoJyLT941Y5Tg5sj8lIwGrbENstuKID/sX6SWWKJtT36xI4sGC+au4ffLEk3/WwsX4f8tT+yMyKNqZsY5W9xrv/1dewbEONcrMyAbGZyxCtVy+7ZR/jiYQ1VEpQLYZOAoP0rmjdwhCoyD36saw8AKJZmq4rMlpodS6SS4iI1UitMaujtRhj6zB1x+AtNeXY6c+QwmgbqClDZZ+Sc0usRyaJ9cA5zK4HBwR+MQ46rGU6pb4CYPM7Ff1CFY+iYE5sua30KF+72Bd3CNd0ZB4PZWrMAfgHmpbIT2nO1Floc4WA4wjGC2OnxIm5o1hDXyANSfWPAQnTyfNSnFoKCHJo94Y/7qGalqfggqWyjKepBOyl5lx92WySfmlGKdEKnhGm6HIic/uVmbg9X7GcPMbhbfVjhVpRhQhSXN3SRsn8F6IGQ0npQOcF1coDigQOzteNpR4patvhnF+XHKU+LP6xHTyZc7kRYx4er4b3KmRUUvIEEc3K2vqAJ0URUpUVYzD8W4vO4WzgEdQb+hQuC4/FxThJoJ2U4DUaPrxgtv3EOjx2Bk4JQ4D0rndkWaHVpNtvCj+LxXkPODIQQP+GJcOA3H9iSYeaJNIFx5AjI3vMq8f0ygEYrbowsb/wAzVUGft/ABpRtzPGTGm1g14nXMrfAGFcYKIVbMEPKuhy+PY8FDRL8UugyHh2U3qvFyE7Id4/h1eOVptevHd6xfgitd4EJBTW/NgJv3/qSJDdHNDaR8bJFgLCWRFd6pbOaJaIZF88EGnrEvJr9ilOK/DZYDosJ/XfU7BXNy9gLSy2r8SXrHO9yk/AZ/9vH6Xw9z8Ung/h4/AsfV0IEgG3vkng3NyW0b71nj3wtvXS7rqEVN/ls9gW5t/PgB/zWt+/2iHcN0JMgHf8/aW2fUaHOSLcy3G/CIF2ppkXYkAZdXhyqjoc8xaDNTSvc4YCPEtSr7gVkr/ZQ1tpnM2ODYw9CLaTD7XwxPqudQPYHutEc0DWbC4iaKtpMftES0f5xw63Qot6ZXhMVRVg+T1LyIMlp7O6XCSm7EP4EltTCAE5FV5EFZSuc5JCPiiKmE6AESyX5imX1K7CC2BzuJHYjCf1k+c7f46KpROj47BluZ1RLX+X9P5VFOSkYdivEOWLiDs5vA3SQ5JcdlymhfwJr6rG7PlzBFa1Vam/DfdsP5OicWnAbh8uJ4/T3wJ4G6BUzWskLUdgjNQ6WVrgpU4X2ZitseV/xR0crpPJsdix0GDHn5+c37wYSHeX4pmuF9wZjUzPf4Js5mEgTcTa6CuAmbRg9Q8n0iAHs1wHuWzNiMiR2+lLuv5YtL19Ccd2iwauGnRnRcTp75IRaYdn3HZ8DVrgQzCwA7W1g2v10PJ94WnZHlWBRIgWY9aZQ3Y+NSCikHViUpmxpWhXPnvQn8CLek52swutwmO7VbMMvx72Gs7VTu20vVku0W9sNjmpMxjBaHHgkCclY3UUzvdXQGIyqimvbc8vrUlw/oCtq8W79oxgRo6kdPOqC15Lpa9QyN+nUFngt3x0e0w0BXiAFTfk21weLW21uNmKmAKDQTMZfMxlpCpAZPTiVtv8rmjRr/ZnLOrZDKhyB1Jlw6qDD90EKE4GAMPbiMmVN2EvxjUXjk5LiePF8FTXdtgcC4b40yZWR57kvJ+SBdoe7ZgQAVHRX2ohNr2FDIMhALRqFJdqPfJjxQDPcRMgA2qsoEyYHYKgFb6De6ZvIa7zSHTr0/+/tIKfNPXyu+GbEuu+SS4UU9cwEQdEJMuglkFsfcE/lNwDrSzlmkmtlTbkTdE9J7FHuXm/OxidRTCcF3wp0NghBa4J+nFV+SFIQKax+LlMPYLwRA27zbZDx7HjTseccPh0djD0FmxM3yb6SU0N4NuigzKB3PMbwnW7zFW6OtKeBShmsJ6x9DhWo0fVbKqNeL+Juud9IhaAaQfn+Lgn5dofLXfrlXFtLrzEPQcXb906urIeWvDRJ2D48b0bcS8a4h9IrraVizzldmbBNk7joxfRCj2QzN+nW5g+ikMYsTa/GhDvIXTFQj4MM4IAVqr6Y+jDAfW85+SYku/UGiZ5FBl0eIwCxM/5BdIeek2ChZRUZS/9WWgzE3dyM8pfwtHilRI6COL9/3XccvshUv2mL6dWCUuskwZCibq+w3lDzElv+fvkO55WUo8QKt+PKYqGxuNxy+yWK0k/+sjxx17esrz4GulAXe+kDI5N4Sjym+M67uKSzNKNzszaBsKAWuUI0drO1EzP2qGtXxerpR3SQrpDRW/p5tTN20o2c3wUC2K6QFNazEMtIVmR9j67+shX8ORDVB9PFAgtk0NpVJR4oWTose5VHNprnzW2zdeDcayjmYMqRxS8W4wvARDyYU+UNu3bvz9GjVhKFHrjlj1qOHPIPyKNYtxIVoAxa9RKO5Uz0zRJTU2F+dTyknNKueYWeg9h138UeAuraY2j/Rq8gfV21LcUIlg5gmzOfutogeKj0iahsySjXoxmFuD80r9/wHiB+55noGqjboYsNdkDmAc8dVo8ZHR31Rc7Zwe7kMex1J59KLfbsOfkYF25/yNODwrscRtQjsef4lMuKkRDsqCYjQpuJe5cg/YLnnay/sKqPlig7xorFBeU0pqwZSdbuSnbca6rxZbmETJ9IGohfDp74obvPZ8a/xqVeCWnyDF8AwImRsLRN/OAbe7uzVB1LTJ3b8z1899R41TxpoJjKDtPiDWURpnYT3fr9MLvGT1OM23v3ZV05M+PVEScfqoVOGRYnQUSO0SDA1la4Ax8mVIpv1bCFtAO1Od33j3yB1oPCnt8uUyki8s/V/3SY38IEWC40MbkLcHXvV+dfNFEd/UMZnjoh/TTLHGG9wcUG+N8yIHUWOOlPnm3t1cRTHjwtXqgtU0fHMmhbSG8qFnrZU6fr1x1a73KJEukJZJRDZ4GBSiEfTitKH5TSTePwYEh67ph/TWhD4tCFIrnvQdFatYGwCbtk8yMiczX1Og4XF5dnbGWtoFSr5FShN7cElSlWwsET3atcC137SK24faFKkeKaA1Abd+S8pSBCJAjJLVynxF9M6VEBBKpWtckzbI60R0EIlqoSFicz8/umumKmJdf25rn+Ehm6FvWi1i3Spf91RZCPuC2ZDws2OOF18sse2RMSpd8hG6YqG7uOsmJhe657ve6p8aL0aQ+WiV+3noYDeVjbttqZti2/3QYzqgD9Jk9dpwg8HKJfD/D061CnC6PYIJmWRErLcd1/VmvM+LOyr2bBzwJNnLeu9StWGQB7/faZGfpceIfc+sEc+jGGRUO1c2Sridnehkvnun4gLBSb6ZtBKMqV2Zbx2Lseo2oCp1mjfsVgI8LgbVLNqByjgHq6X8g1iSyxjz6MXRE3da8YvtmdBGwnP2vN6/9MZS0/bolZ5mC7xgqR9XIzqViE57qFIYAcwREs4itOOoAOWyXaID5exemsssq63tWtmIFEIMNei+xulfHcMpRGDjuCgKfOPua4XkjvcZP7SrktKTtGvMK6psv0ei4XihZwLB5Im4qgVU7cn4f+irq3ND6x8MuLofq4JhzSr1cgyIS0DWspF5Tw/sbyoxlQS2xhmErzlGxOOUkNSCgSObWEC8mJsBD4oYwqwwEqDoB4T93rMoyZG5XIjcn/B1GnJg/tyZMlCTkv9+42e8Ex9cn3PqoVU0j4ounxhdLmFEtJJRNBsrgkyZ/QB3q/5iu7NV1t+4v7/AY9Yu05eRxBZCYZ37QVmnMrv/pW32PbabFarU4okmwbkBeW7CBIrlTQ3iK0XtNwELB7T+3Dr0XSqJN+O1vuYOiU564t1BgwJfqs++m9MWguDNPjOOCX4AGw8niNxP2lVYmw5fE9dvuGaCGMxMPOL0cn1yhZ2UiBpwTQuQl/jGF05ZF381Z12OexDzl6i3us3nNgcB5Gd9x5bU0to/Vsu8ACyzRVFfx95bFc/ZwK7dPlSvRMNsdH2B3s0E/pCbG9NKwpS50lw341PVUX8H9WtBzW3ZEce1a2OVvKVhzK8B3NLEzM6rXTV+8wzhkuccrj43uYQE5wf60CWzb7k0T+mz/hWB6GXA/+MzrdmGIzSg7xRk2uwqmapt5S4Nuln2Z1+a3jFR3cTj+x0QgelL+tdMaOvUaaklYJH3fTdjbo/m8cYufHcgW53n6Jwjo6aOVtUI3g+DVNbldt97s2IfcvhNyDkKBxbWM/7173Htr4kliUSWlZpiBbenN2hy6GwhRultYEOXzjFUxhXqCAdCfGBEWAz5YCkxjJW1MCzAtjwUC7FQKnii/QNcN8KGgBvU8B3w8RVIPcKCCnVdG3xFVMxqWeK+u3S/Kp5YfX5eQLinExmd07cdaOtCH/vmcb/t5uzRpO1Pf1txOhyln7nw/zz2AdSoPbQuf2lOqc1fvWFwbRcMZalVSPlemqbSVd5ragvCY++PriOqZPcUHMJwlPFPFli5D4pFWImJUD8rnydNmsFsrMnEAQ8LcullGS81MAhgMEPaJm1EwMFkst/DEXcttpKAEwEGRHbjTlgD1nfYZ/pEp9Q/qIAdZBy1y+C/eUilspm60QDyLN9xEpAl/e4bPvMzC6MJyyu/UTRlL+/O+pnZIWg6yZfjanonC/WMzIEQ2/piFln3DA577VQLqGSFNBy2RuDvbYNhMLCJZlpJRYhk9676kIzgpWQthITT+97qkAkKr1obv+T+qcM5rm3OTCbinj+0sb7Iw/r2LrgepSBREGjdxjn9XzB818ij/qFElsdbmcDRpldaI2znFhNYnnWktMFT7t6A0Q3C80kQO/NC+rgGXRqhLx+05aRXB1UfqEH8PqdvjSATIHNE8w5xCDoiM9LcJKKeGHB1zQFlfA1EzIG2aGDrezQyfKBAII5HTuHYJPRU7gMTI32QTlelR6Q6qFXfDJUwfcbQsnkq7c7Vt2CCi1yhhnJQesqb4OlcaSpgL/FKhABHMMaZ7wCn4GoulHKkqubhVUt1pZDOYO3JKzUjYPMbBAXko4IDSapJhTlMIA/3kxwqLRNS5C2ojgAeWJdoQdTiOye2mBqFWAoQFT+RD9CLCH83/5AYSEOX6oCCXBbVmv2yV1AbtwqoXYAvM3hqx9HM3ofRla1jEBCwKWAWM/ejOeLDy/qKDoRRAuTSD2hANMuCgTdw5MyBjF7GsCK6kSuuJK0K+7zZWPMdo6zoDF0fQpp8BP26/AMZ3fDcgihNfbFZ6bxxuVyinm/HYIBbs2WM994vfyCMke9NZwPTqNheiVcMf6QwjPQB37vB3d/B3V80gQ8Nz+HVytVcelsNS0UyeQxMSNner26V5hOUtajt+pEE4JvUKNf0d6KaYXtY8gZXUBbEo21RyaEqhZI0rcVkyPGFZF4wNMPU4pJe1ZOWwMNGoGYCjYuq2ngxx6cIHn5jVvGtPnmwzIG/aAPXMoO8+mqVpU3og6i3iUCfRVbzTM4gjBNlJo/FfpRlM6Zl5qWcCYsngQIyDATXfaiv/EA0jtmLKAArRo4uPGtlpZfnme+wxwa7Iem57Y8pv+8aVfbTPtmCpofBStGhw2kITdPC4Nu+DcYSMb7YUl/qdNE19OEsuQ7SFMF0aONueF2tCtnCTAZXi0w4XCcaqIalH6cMeabv5njZB4DaDaavl68phccCEEZ07QKKhqjh+KxoNubIcKkZdPGvsfYMGJDexABfzhHPLa5VmhCMyK7kop4RAw4/zoOHkQ4F8CFVZb4dr89tjEYCdVQbOUEN4RK7a6JFdaZ5g2jrMYKjKxT99rwq+j3sVs7tqCmNkUZla6flqaVw+ZFnD3UUfOcu9DoyDu8LzOIy6e+AuBYmAl4NIjpKVtDHtlg4TeRIU7NuEzwgIbtNdzNPawTNtv64shfuGwnzfZNj9lAvHODtIDr82Kt5gQfROq11P5pOZBKlJqdkHBEXJ0qECDXnDExWjP5FRxrqX2v43isy1nelSx8983fxCGL5lW9ObC+irW0NvmmqVJ+qouF9velNETQw/idp/qhb+ddJAFTrv1FM8oG4nBcTDxw+J/0/fpnMzzcoKmTW3IDnEemLFppx+wKaEby2UYn7CyEaVmvBkFgWd3L3XKiRywBPsRli+ffYJ/mIhGQvFEkUW3hX1nOL4g6G5q76NJ7SSUUNRxvPESjuH3bT+fT0FOx7mAljmH4Ant70ZPUIfFjukqDRvDxNzmvnzn7eOlY/Shyfl6dX9iRmBsokfik8sNwf22vf210kiw17/+ttT1miZSMAfrOEDs5J8dsIpgI+JZ9NRWCiC9V5/fPy212XsmlHL7oy8I07XAItoCSwP75rEjMFVEVDovI7WfhNczVjkznKn2t4Oe73Wi98joY+UShVCUhnN1aSXmYc0W7wmsbf43GtHO6/qsgi4d/8YlHwvy7QuRMVo2DjvvnoMg8TXc/XOfLDUo9Rf/a/hjc6SyLByhjNPBM19YRnzCda7JvGsyv74hQ/CWv8xYtkDr9zppiLA12IDUOBhqXEKETXLQTvEwj5/dQSlS1zionvY1nnaEp9pG4k4kDqseeR0bqHsaGUfEMZnpM5tR4KVlcepkMuVMYvPEL0d66hHve5i3KmmAkXyd7nycUt3gqLlROwuYZuoqB8XuYP9529Y6hJW5CXdFiadaIWslGQq2Y7f8q/84UbbUsNqbHJFTQyy7utNvrsSHTGQ4RQeakHkCoZvL2POKZ5gJAh0quJto7mrpkJvLRM5FhlNROl35DJSX33RWIgc1js2tm8XeYD8B5vMHDn2EnsPzEiwcLBfkTRs0P3iS/3IBnZvIEiN5jgOQ8iq+a+nnhWjVySof7WqhHj75pzV5sRF6qaON+X/VwzkQZcsJtgAOXxOzri7EoMe6PQxAuaR2p8PQe21JYV95QwCbpfEfc8unT+5Le1wZETaQM+k2iUFlfEz/FCKQ1AFvC6n0AMFOw8e+2XxT6Von2gI5Lvj3khumnN610HrStnvU0OSq53Fcl+94RyzYMC48TdzHDP7KFkxHjEuSqpIvAVDBtseRtci66azUP8jJy3COaOpN91/LAHoBUm5Bo7hNwDHd6s5vc6jsRTTJUdv778p8LIH9KNqTyGzsTh1iheyoOFrN0yiLiT9B+N/sQ/eAtC2jDw1pqflnaVYenVg0rCmcMFu3RohLqgwwY3fWGwuFvryWfj4OKG5R6fCax6yuB+YWWMo2xlpOKVxJNiqDIIOlVBeMDp5TUB+FDWCzfLaMtizv0qHEmCVWKoNU7xqN4l8093WUhU8BNZiYnyt7MLtb2BT1EHoJcNeIU7XbgFxl+++V5quVu+fs5l0j+ud2OZbPFOVmJbHSfian3L/oBSjixw3RD7ohtF/0wDAg2GIIGXGwKvidMp7tzy4c669RCriAJZseJR1uW+7RsLQiFCMNWTB7w93IqAzpcQ4ZWOGDiLEfZx0fmcpPPDYbwraFoGH3BKwNlu6eAxsTW3Cpv/vREbeFPLXNS4yw6ON+Mkvb6Oc3x1BUXfModgG6oYpJuqXb70z4xK4vCv6iWANDzksYeE2Us7tfaPC6BXBB1AlMx0ZS2Ye/AuDPPXLfdNORW0aiJQytZ9h57sxLhQTxRTtNXjRCLSgbX0w9k5C20yCdHERLzRrHQyUeSl6Ab0WKhXf7t2zRYww46GfT9gDgd2cupsa5RtkkNNIKcXfrkcc1N2J9BEa7p22D8ocnzKGYr7YEsMHEy9M70gLtpV30Ql+PQ+w4kkkt8cNopu2ez3NMjBfYvVi+/5oWHMMzNyBzOCsbBwPYQSnpmzW1K9WfvaFwmbsEaqC5ed5ckiM1MY1KPZL6EQrhBbUUK0PGCO12AMJ/E3aH2lRpRdF1TSEL680fqv7tVnzLpg9iXvo0mRGY8XGSIhGZSViSlbhJkOl9lvgO9HJfe/r5x5TjWU4vrExJ+2+FdGVguZb2k3z9cOoMmd2nZyRCAbv6dPjYHAWItUY2blhTjuqbu/vRSJS1cT3pasS9wk5/lAktT4Yhyoge3mCHzKw5lmX+V+sppiirP2AiyI8oO6StdOpuBgDG0E7NlzUQGMatbMUK7QWAOUZbHX0/e8CLFjfztYuq/tOQiK+VK3qai1ZirzqKm9bXjbrukQSYiPH33+ZWmjk5uaTFVyk9wcumjKfx8v59heKIREgCSvymmzJJsP+pjtq5rL0owM+tpUQ8tMjKVU4jVc8vM/Qd6Gl/kiK5MR7sZ3sHycZl3DP3y7a9BKgbkMbo/VfIeL+xtDXhOo9nuazAfSWPi3UeN59sv24UFuXeNhbKnon6vlg9gm2PJs6Fs3hL4l1QpSVHDqulRdMsRhDqCOtTOAh7iWtjeWKTe9//t43vFmEsILV+q3OusIGfbM7Qrq8Vkod6B1tEo1OG13N/o1eagFoyLDa+8JcThJeNaRbno6QJYlg67BEWBvaRov7FrbW74BZ5s67ZMYO3m9aFk4kmRG4kGV0x/HJI+ESMCkrphQgZO4wJCGpAKk9YE5h2tspVbeGWe2j6QvivL0DTWhuocmp30qF8pDfmuXF4ir0O2k3/2E3zdqd18DwKHWxJ/SZu4SFjTcytgR4R2yznGhTHFSVog4ELiTTRolufuDA9shgp2JxR3wgIZwJfqEbPFG1NYluC/2mWbXDkD6mCfCS3dXehiWB4jgA+ziIEwU1/pIxvPOoAAW6ahynGdnE4AvauC+2hnIdBrF8XsZvkBG1bQ4L2/96FBOibqtj+hm47MQBuJpMJaIMN1uhZZUs2Tq4Fk5Owjbrelnuo6xDAXsca4YqGJkVUvPaz6yJlfFnRHnmqyh4klP6mc6EkbxU41BBPwxm6ivhdvdW0mQSbG5pDXoQkDB175i8yRbPsbRW4R+Qtek9DSoKFpJB8l5hkaaPU1oSycWNtf2Ok5cXlL0JsWgGaa533Q+Vz2ciKstHbmTGT4NEi/+6MA1l0LmKeTuc5+8GgXh3T6LHbLoD3BR/cSyGlbsBM4MQEy7zbzt0yXx/RB77W4PpINpunLDnI9tQmA/a29ms0MZIbky3rs34prV8fKGdEhK7qgjL26KB/x8syTxWeyv85pKGOqpKIhg8J7QIMGwPVPZUKs2LTUIiCZ8n/gPhlb4alS1/RdPUAWiNFjf2IjBI5mPdomesLfM2eK+yaecDYUj2H/P+md4QL3CNBRl9U+K/8+BR9EkXVBVY3dRUFkxBJjcPxlhPs9p0gZ0Bp13dIn7vydS3KdaYcWzD0yKBczqW7mNaQeFyn3nM3u1ioSaljXUGcNJPpL2l4zTNeLH/aEkCiCncIfRke3E/StCQL1OAlQAlPxCsQW539kNL+oU4XAh2ztN8x40gIHi3qyTQi3tC1BtbJlINTYENlzdIeaaFWDaW3rA2YSa9iUmYxaNsoLa/kxvjp6YBz4NJUof16Ir7UY53tqz9hPWzQnoWfIBTcL2zkrsLNHoMm2KAToLtQJeIwhVBi1BBkDXLKpIMWWJTI3JDVLbVuJhVWZEzfSPNhSfrWxQqL0xqriVBoj3Gq4tO2VZolN63hY6c5yHt5ZBAzWhLlG+L0pRa81In3jaXsvm/vhnZJ0BYvCFpIv3DsE3Acap9L1eZqCg/2fElqOwfLZ985vVZY39AueTDyuu5AqtFTsqNGIlsmRSrWvEGDXMmi54iFAxct/Y7duMIuhRUQ43byozFidfLx7SHub9cdKVmtcd/XWQhg603KG0xk2fomVCWTZVFBulJMSG9qPO67BN+BF+oqtKV2cudIe4gVNtt+XSxDeOShOosJF3d0PgbxArq/oWWpNOlK7W++0EGQ3DUiMws2bB7uFA0LOaaeMM1zYoX3VgOeBijKbf+r8AvQFrNFx+p6I48KEM2grj885w4scb48dHe8sW3AsYBG+XcUASHRDS8SUqPd1hlSSkh00fQqLpn3m6FaYjCQXu7Tx6kmtRovzvw/rrJyo37Dz4GUak5nqJYHj2het3QE3iq/xEBM9OAF96ReaGMazTDWJs35PzdNIxQvc7G+0Yzv0bHVLpkEdBY6osUQcHupAMElcJDMKwK/ounlsvmpBzw+JSUzmJt1mDBACNotjseAbL7KyP4kIgUNF+DdBgjxi80P1yqFLzVV5RZwUqH7U32EaU1JV1WMpLf2ZPkhEHGqwNe9eFwrsq5SUrPRlqYAIkpU8Lvrm75EmK7WZzkUKfqIRFzYIf/mpOtP+uOxZu3KP3sIjLVe420ok7OnHYhG6b6gRz0asPti0KZ0lCFSSGtWyJUN9WdJgLLjoVLchCwPLT8g7P9UARfUZ+/hn9f7BWwwuYkcqex7ZIuP4+qcxLO3n4MAPMbpMP3yXK93ye2G3mkWbGTRX2tByyMYYZc740/xC9c20DPoiP13X2Y40gRaOinOFjrnSJ7/D+iFNW0UGEvKplNo6Ctn6CNKxXf0xgLDpF1yC0eRO1vTjmGl52WZA4ZC9XATZ0tRfep7Q8R+IELzhgQurv+Tjo3LVzMFeGpYw7WxJtGJ+iyNgZqd8OMnVLOrgO2I6zUVDvVl9CPwf3OrO5aNH4s/E3D85l77uW8pU/ngNthYcGj1Hkzp+KhKNXZLtnetcoOEYpfWWOuw45QCl/lW4WpZ5O019iwIImekITSi3UJx/HDBQXY51a9/5GHiP63khu/gruR4GUSdlmoN9ct+f9BGtg9T43FT/ykgxtcKUK8i1jgeOKEeVZtgB4BI2bQSuZFGEFf0u6ziP2PN+ULtH8JZA0h5L7TTBuBHUid2KSsukPk22Y+LzMe4eDJlAlrKiphX6xJlg8OcFvS+MFFr7D4eQvXB3/Fg9t8bKAGB8UB2Ez7vjQ5C15Mx+072NcdyZcH2mm0m5JPT37JS6g0hP3s57lJfSJdSWqnBXBPgw2u5JiBpwXlRo/wDP7bVf4oEYGB/fQhL0PFbgWv3IFZ6ab781zG1nORaaNzO2M989RbS29O8FvTE/gqCGUw8Ppo7umAp2Vd/ZfJuoJ4ZInAnNdK8Y3Z1z2Qcv7XQXj0bgj/XZQLrVTfPY4rTgA0t87XR7ftGv6bOTBDpii2zVcWOkR0WQNKJZF6DoJvBAn0HeFFy5EdVxeSQRqRkNZmkmgmYajIC2fWskgkEW+CwtaNmlVT+sbJQUCZ6rvkaqG1w2So1iYnq2L/MmYDNDQtLg672xBn8AWVmYW7Cnb+87d/ldH/sYdWn/2HOJZAi626bJIhIEgx9pJJyXF21+QaOPl5l6FQ1y1h1GHaLI8nlh4PtT7q/ZZg62ozalDusM1Crw27HYmpMpM+sIGNzSx5fmLmOZw9mcw56Vy25O45FCGOSWS488iLOMh3T3SVnVdqkptH0EF3xjAWG/PEFmQo/22THGwXuXqoC51V5S+GbK9pJXZyI8Ho59AJVLn1N5m8bEPtw2xUlwH1dXTXIrvzfbSuw3dBL2ddxOqPMLCP1ohBVPuvzvM6aX+7bs6m5ofue6X+dV/ljGAqW/dRyneGjjuugPHfUbTwg9NMj/Pr+9jk/gfhlvVimTtDN+QwCR0BZ22O24SYwBNPlg7L7HO65mnXxW8sRVgQnzNOMd6EWZ9FF5fdXJdCqKFjwBRhYLboqaku3YrJMJ/SkqWmhDLDgGJhqEPbbU1cbqw/xUnQO0BTxGmtPIuK2Tl+SFDM5Z8wWsbTi7iJTHIue710w28GSCHlZObfuJYfakeH5BRuchG0/0rHikSDNSSRNPbxgq2n0svMcsxQFYa8t7K+z5cQEOPALJIT/1jKCZpC8gpP22sd6KUly9J4mtvBzl3HdxKdUMlBCIuMOwOcs8Xm6Ca6qWJAaFCKxjeFD5WqOmBDYSxH5wq1kyxIOpoB7ch8YbTzxNnlHon3ykq9WO4NZehmduHx9eYIudxT6kURpsTF9+a5/PB3Lj7w3TvmjQ+ikPVEt/NNZxgpuJ13oQWbMkxru1Grvv0dtg4EPvVc29yOD1GOyYHyfYlnSGUv6Mty+Rhxv6VarswQGp9ROBpeJZybhg4t7BWAt6HTQVCpPh87q4sHoluJ2MQnWvo1EGLjj0YPboVUhV6NwDR9vHPf3z4PCEIt+JGNUCqKVII+qQTIHeIk1xeOgkh8OMtDjbbRN5pH1PfzMutiEMZzi4kVmoxddpej1+fBiqt3paH+0qqFiOGPw9kZVOvYyFIHJeBYlsZuXb+RD1eGo2Ko8LUlWuHyRnuUGu48Cor1OrusR4Zk1cZHbalYKrQy0ChpAMRyUAiPjneGxNQ29VlM4ono0b++5Ww34I+Ge2ctQUOMdAmsyEwadoIUDe/6qB+TnQQnQZXZ5nL5hNJhRZtR+WeOl5wxOCVZgVLYwAwCfR6NzxUbIj6Hh+T4/E3sVEtdBbqWj/RmQWAdca/DgIrqkcOBp6NxO2CAvQ82EbCVRcWAP9Fgv7CWx1QS6SErVlqALAUioYMLMCKgqlwO5EYu2Rzy+lndC/HU+EpGcHPK7Cdjdu/fZNM5QMUokUwg4epFE73DBS+jYLzP69/1fJ6QmO19aeJb4dZAz7z7AYF5cYUcrtu0L2atjOmGMQngukebJLozLKj5uU3taW9alGmfX19MX1UtKwzq5GFEQ+BihseVdatuTXMWdrDgWJ/vuaLAxo30eZtJPyoOU6Tmp7df8VHHUopAC5zS/fqN9gmPiDa4LCAL9jhHnGLmsuQOZsseLKqSVkLVOqzGf2A5VSQTTJuyg6msA6qkXtCf7/JqyDGcGiW5z+YRZBHtUMXyyLX0RTNbnNvRTaQIxH3eQdkpe5NgqRlVShiluRry9PStZiUWO4fqtz7wWq4exIVP1N2RnN4YwMizoRSp5eDILBeL1FLu0b/BkC3ra2BVSW2zq/AWNbL6Wkg4UVQzVKv7TdkFU0y4mkC/FdeeX1DttJEHS/i7kSoCI2DerEE9Cbqw9hATQrW+aOCFnw3CXfuF8xQitSGkKIEl339eNmqHJTebcuqNb6q3bSslZncUnuqlsn/iYHqUtEcPO8yihUEw/Jr1TPJZ6zBNdbX0tD+NDo3+/TzJapbRnhSSuM4bt3OsDRODAaG8QuQ7shVWw4e9KCZ963eL6x1DPHRhuXLCossnZPhSZ0BUaowknOl7HcRiugSPpdUqZOF2w5T2WfiDWlP7vKMSY+5mmaLPUSSJXJ1sMQs49LkYyv1NhHB2c06WV9tBD4aMrqTdm4GotUXfIbV/LlYzgiBTrMjyeB4B1+CBDT1xVXAResw6CA6ZuD6EnlRTDULeOFjMwHFH9sS7DzK3g7EcH+x57rxFnCr1smKwDRraqct/Tc3nTwj2cmSIJ9m7H/I6L8yYNNw3f+5xN50yu0YBXQA0og56N7dcqGM7yJLYoiT9xr0WahifsAkrRERv3XuJc2aZS8z6YuXAvn59AEC/J260sseRgiKjNd+nu2Il9l4z6zcVf9tL6+Uq3qNor0psSBRXv7FuCcV4QR2/Cf+UxdyZU7a2OB5RgGnuSFEuCg10uwqclDC3iEpBGOlMqiNPqNZDb27wjUPAcdgVvLp8YoTfn9vfSmp3DNXgJmL1PZRHaek1m3q9gcTlzlI8CpS7TcW/qmyD+J7hkda9eKmKmYHoLZH/4tFPdoZ6oJLHY/RwG6vzuwZw6b2LuX4JokXTuE47VpTvlKZj0n9UPWFy2877ZEQuO6J/OqovF2SMTnRhUzRpiGAtQ1V6vdX6xyaZ0AsdAMhuyNl51615YOvSvjsHVpS8iD2tsKs59ZjLYpg4YV0VQd00IV7DYTbagXQ/VWDXfjbVUBNJHyk7TxwV+bkNOX/eGIYyvfirPoh1C/KEhIXgFpMMRvsPvl68SaAtmyeBZnQHR5AD38KElKFJqv1N1m9RME9rtPO8zjXGalzrGOoPUkaM0dm5osptkaiD5H+SXkI44IcsyyJv4nc27Iipg+vB8rZlYtr1YaB7gch+IEcGNrz/zClt44KXa+9zOViwpdtq1DFHHeuqmZg9ne7SA8GKDkkVpXO16bcCbaa5IhdZwSuJsEdNOUQU7czkPhIlccA2ltSz1CC4ANN5SdAQLMcZi/gfgUPxWvA8Xjz2EwRywTTLPX0pAv+q6AAyvF5K00xgH7bmFZzTD6+/c0WHYa8dwP83RsHJd8Sj7IXNauzvOr8++QUWJUYf9O26QJNOGtZI18oPv5RU96qgKQ+vqAxMNsRj97CV2OhC9JvuTBXYc59outQOsqSaSKoUHe3zO35aSJy+tzPKuwZb0GlPkv7I/1doP8eckkx/DSaOC1HgQtNakcsiI8PnXCcefjAbFn5FwFjwGaL7CkqMKyvlNtL1yJzJfnljWKmRQUzUVsF1DLs0SxmThG6L5HE5uDmoJAUOtqwq1aRK+mlaDSJ9LDBb9LlAlT64CF69NJi0ODLIsYVCzR2r005G4o156yof1J4Ffs+uBc4JGLeH6x4TyNnY1BUxusfbO+ogDJFsztbhAVoBwzc9mAErgCyO3GqkVeJeVP+ceUmDZMsaokBitj1XTtjfEPh22up3kwAGsrYrWa/1aaQWEykhRJMRR9BJRu0iDgpnBWA/RO06T7QW9T5Xtwpktdhwo1J1wDjmCcpRRBL90UF/wq3iL0rMZP5TQE2+Es0rLQVVFa6/EopizSYSOn6Hxbya6WAQVTtMtjLmNJN7J4eycFML2kjOXAfYb+9906swEDvvjvm1Y3At7dlKAUjbGXRc+pr96+VZ749lvgM4sZGsZdvlrTUQfLRppo3Q9ADYvHglsKvb1Yb4jVZRfYnLw0VbP/O5OnHxJUbKKX5CRBykZk42+SminxCYGea3Y96kKPW/Eeur8dN/5I+lIwvCxsg68251tc3Pic1Nr4E5NFW4OLw4Yiwnti8yHdZC9o6fGEGhoM2TbiV0rKh0tFLplNn+cI0CbtaVeiF1lw68Ysdx1OjUN5TpQJAJ34hVRAiJW9Fe510t6fPCyLwKL2O8a098D682gjdiWtSodwhVJVr+solQxdCk4mgl9z5sKAcj7WcJMtykubkbSlbZw+aKU3L3WQxQyUqx8no2K4U+qrP/K6TxQlQBOYygggM6ybac9QUFZkiSC3hpMSSqPnSmSJzyHzblACRa6ZgfNyPe/dYH/WIok/fBhJY0CtlnNV3oW+NblTIycks4C99NZGz7PI6MmOsxyzK4hnzQ1f3C+UxXhYCDxUBmWAR/fpUh0EDZeKhDFbIim/MtMfeMFHIYhDGlAvqQ1gjkCaUwsYEirIXuKtELu1bWj0il7qijdMG+PvY2ToZs8tjMYmWmIYGbjO3VyHODg2231heukmgLajcHZYOMNFXL47iynOsm4ilAYE4yljWZJ3u5qG0Jva5pE4AnxG6PUEazH/o1sZKc/IySdhJmnioDU4DnJ+dwUoSkcv7RXpTiAk7vinYlc9/ywK/4b6zL1wKOrkaTHFgOeTMZWi6Prr3CKxizFL2c4XGVYkiO5I0kCV15DY4Qg3gK8VYHTfRbn6NxjP4AX+PLj2cUQd1ZXRn5IPAPEClOi1tvb2dexUx7OSY6V6bS29FVQ5/L23yhp7x8izSart8psyPYRBhQVLhXs/LFSUzoS7V6knSqUcAn3pOCfYwmP/czctPlfkFXifKTCAeUCnzY6auxEDWp/YH+NTN6ttzlpZ6KJ5194s1fPAXO3UDf28XGeWVkB5kIQjDQFDLZhGGj8M+CvfJhL0ov+DqZGvD8tUnpL5uvrrIAKjy939KsBFIt7zA6HMX0MOFkCtL+xubNzcpI6Ionr7/UW+HXUxwmh8TesXW8xzdEvKtA1Bf90S+kbvFwRhnKK/Ahrv6JUIwlf3XpHeAHR1bn5886o+XwQlcPf0vJXlj82N18ZryMVuOwreDCuFNZcvT9Laol6nqwQjqqrO2V17OOODOOQcRr4uIPirx9ZujDNOGNTQXmtMxLrBJTBFGWfuDHZDfJcv06ukac0XCpTnA0MFF//JrwmfN9hKTFbgeGp9Im5e0Fs/VgGxLW2k+HxJ0TUaNcefot9PHLMwcHeqmGgBGoX29B7kDGXCDuQnTY2X3pLZMR6Iug3eaOMY8rNdSvAiYBwxItxaYwdkdNOn5j6o0OizsNyH/IDNkwGJz4cTipFRmoInZ03IMnYYVOyVqXNZ1lXtbgvtppTsYKrV+1jcv2PHn33Z5dLK5Kz86s094JOxJgQgOSQRCL/iRREnfpqSC/LKTWoV6n8+o4DEKpcKOASxdq6n/cUfHRBBKQy1MRwCx+69XU1U6nra8oj70TZyThp6v00KOgAtL+NYfrWrC+IQLp6ysAdfNf8E5IUSdgXz3OVqjCgB6IcBpD7EoAgvtD+VWR6Qy8jfQtJq2raLGYxFDstvSptV7YH2TwfEbxqNlPBBjc3kfaI/BS7uL+0OSNtp2JKjGXPnzPHcitGBRONQo5frQmVNUEUB9i/+jMQ+DHHxiiAeCceBis6ZDdTh9L+Pr9OgvMPCT7Ib0Ftg+glStom+qr5cIx7IPpoEqOnVgjOoHkEQ5TAvqjuh6/OAqYpA3aTnBlGUVaQ1tfvUG5OSi8BmNbxbyw9WTinPO50vzolNzV5tmUkcQ8IPjiCYkgkUzfe0OP7ZPnkBuJkgpsnCOL/3Wmcj5wriwu+5S1C9cmAC+6ymt/nflQ7KlSqv13WbK179/nKQgrL8dV8bEgu8WAL2apDDL7MymF3XqvEUKY8CLFmy2mFpnUaqml0RQC6nUhO1thrOTaS8XUoO5+iixs2n3CU7YSfXRcRHF4ssgzpuPSPEtAqbGj3OkCGe/ZbGsxl6RKo3HHmWs4yCBjow4cLbs2y80J9EtdhGDUlpXSFsb42jUZQusgQi/ZQ+4i0BNLaXTLvRtFX1V2n5rbkJOXHpT2dMQINwa/6BQCqzyZaRT5pZq+RaQEoxEMVK72N1/0haVckglE4jFzbu8/nhsK8GGWE/H+HkBvWPKZrLco28qVBa2ojYmZ1zeUdqZdFyVUSU0wcPb30ZVhWZcGa27Tu4Y4ilDOHYs3wIDJMy6LxcYK2O81SqVr+70BPz6rejGmT9tTAUPo5WlN4L4RjkVDcuK87lx/fmHeacrbJ5Ba4Qemr1tc9hK98KLuCjZgepHOL60Wi7S7jiXQTGiHfT3ta7vXAVAuAUGJiZ205L/IUGse5SVM1H8YJ7kINl30P3rU8TcDWTJTqu/4omzHIgojOWf+MHyXt1Lr4O2IlhLKA+ay6uBpbAdvYaPN3Ynh1lsqpklDEWSKvTTU6WX6tXL2WOoOxNncVV8Orx3aL0nCxr3G5HQ+7HIXF4NFcSv86EO6f2BNhX2vsgiIwZu84FooZC2bCId2IjNAx79yY7xwVlYz+BjC2pvWUxQg4F7uPGTiCsm/LrLPYsoc+6AxiQN6W5N9p14WafTpsEYGorq1PZv1SfkqMdZ58BknI4/jpf8ocGaL7AopS3k9+Jmq5V6RFlazTm9px5WzYjGRqaN1sC0YkoFD72XpH4i6iVqgLSO0j6stuS8db/0zY2GmIRD9RuQn5ot+dwZLtC2jlUrVHMdROR0N987j9+yEB79KmrVLsoW5I5b9uPd/hdNTQH6cpggThmoUgOPdFBgMHU64vGzaycn51tVBRr+ccsjb6LAXZ8vrMHhL1ch/6bm1nxdlslZP3gglBdHI97nHiCEn5sSW6+s8eFO2WlggW3VOVkGLnf8/IHZxGSZOD+0NtB8VmVw4ifNjCi1sjz4242g0mWRIrmtTTCiXr9i6akbrnfH4fwncsQCSj7GwPPlbMaWuQz37Dajcw33k3p3/RfkarxPu22pnfkE4HxI7r2w33wBzgaVHMSziiub6/CZkmTOVFFBqFQ4e0ifTJE2n8Pdyh9OTL+NYCwpaT5FRXxQOek5CFjdIKpjIqFjnYfQrL1ZUIDI67212rUBGHGCSf1CyoFjKsde97k+dQr3GOMlSegLZ+8jf7HpK1YPRlxmS1TUxIPoqUAvJ08GYFwD9OY8yYrlCPGE1Z7wxDxALpwVwH5aMMBXhKaNBH2RaN5nuSjknQ7KWLGLVgxEBIskzAQlj5Zo2CPOj9c5sBwS4jCqz3GQDum6a6wTgT7M+D1qrSWNev4kn9aEbDKlFzVyD/34+UvmqoGm/FpXD3qj7iUbMi7j4JqhDnAp2EbP/s1NTmOkRoB+bHV6HrSitpksKi5hWF8uF8GplDUGs7HmX7E00N67sRX9JTPj15d0QSprp6eVLkbfJffqmqI+6kSxPDK4vyDU6RvH3AIkWU3gJXeJREBxX/oP7X9oYkrut928jTcE7+ovvbXJHpmw0nj+zGkHwBIU93iXbWmvoIuBpkGfzLSLU/asvvvCHvDVRBag93RCyvhMPHprucOO50mgGp3tOWsGY5jN3M/lPDOQo4Cqo6xC2yVFbZVUwgWZTkXvtVtcI62YBndhe6n7u2GCwrV+Ht8oqKKwZSRN+J8h2TJVFn3IDNNBwbym8CxWUs0L5A/S5JOBobFO4NogIFKTpPHHRbXHsv7xwWUTzPNBsmrNnLiP6SH2TTlkSsZI2076SPy2wdDJ0AyiPDaXhTYoYn27BlkvdGWkAOv1rw54WJdDJEeYm4Ayg0qt5y2Ks+Mf74TeH1p1HZHx4EI2gIe9dE6YpKnu+GOObOcqC/uqN7pJ89GmxqIpny1cLU84PWMx0TdW+6lcECka8ZrB2AtV4Ag5l3GrhR6V/qHEKl0lTCrKU1Ts4Y6CbkQlAS/PT25BaG+Ks0SQdEReVxOjl1AzB0RVE4KMORB9xMMmS97jOy4qFYrj/SdQ5ZZjs9gN5jTrShwJVUSx9NnBNnp2W3NYUDK3WTAPnmsA0oxKPGLzYrKeA/jewsmJ4QoC6FNT8iaPmFd+VV6C0qqewGiZCqXaqxIRZbve3mbve/8gmqCe54jAEWp2OXQb9qcu0rzCZ6Xb0ezASitEdmjBVXuvFMwp8XpUzm7h6QlAlOd+KInAbNnRxAb79WHCGUsnb58sPMGhnnPTXhPbr1zNNpeggA/HJwK0YUOfFDsaY083BtdiDlAW2fE51gfT0iivLMsVNupL0ewUCs5I3fkeOvjwTQGyoSPTdiOUt9ZU6OGY5d6P/SKJI3Q0gjM8qk2vw/mZ73HnekiAAW7ds67ccLWYXTzfuK/l/TZRgAVqCiSNHsMyPRVvCECrQJaZHQ9T9XA+oBd0WqGK2sUP/iuqF8j71LmKeZbvWi3lw60ZP3Df9fJhluTOSH7pzwdG4Oh1mEf0HbBudJPsQqPbfOLg9l5t+8YE9lN7QIZl3GJucirgW3vVLGcXNdOQXfrGTw2iq3gKt9OFhbDc6B4Rc464cpCXpv8eWBGsSaS5b/IRp/dUEA7/uqjSg35BGEru4hf3W7rrJoWT8mWVw0GjpDkZ3MoXht5OMeQzFU9kxFBPKr4xlgHahsWHFLu0KnqPqyXY1GSk+2AyCWzIb2XKO2fDerrFXI1R1dziq6YAmMFGCkZfcrfBOwnjoGAc9X6dRtynw6RWo2Xxs6PuKNQ94FEBiBSxYxCEteatUPHgFOKH4fWvgjl1XCdit5AulsSwMPolSsgGj4SdvVCc4DwGRzDrNid50thh/hAYMrbn4sccBgexoh2FHqlzODttMBerP+MfsWTqXluKwAptlmyxHt8RPBZjNOgt7/7dREC7Iq7vmXXFZyyZ2u8dg9uE8dAJ20Jj6AiJWJmcJTq9yeLQnQudj+OCxbP98nOVUdg4MRjnuegMCu9uVDkrp2GafFOs1tSmleSEvy/c9V6aUzi5sqKqa4Vr9hqMkrBufLQbJLdIs12D85ZfxSXZ97FTfC1UJ/uLGl+hljaUpd+aHPyx2TPZeyoPhOdxl+eij9tBXNtHiD+F/xsczN6l8+0yIVlcpWht7r/c4R7rtopjN8reSCqwwKrFRZWwVDgRTputjWC8Z3Ippk/Jf2AL4rVafFVOvaVKMd0Hz6TbMr41LSSjlz/QakOzKxZh8yzK5QDDE69gHU35AYIbqNg3VdkHziDyjG7g8bxoIPUGbyodsLy57MEn9dqXI9ymNj/7SjUqJCWtBqxkHsx9dZHF+Hl9eEIoaKIQE92RVqOSqWYM862KVuIFzaXOSvMdk3wYgIv0mH9FJnoFSa2pHZtUYXlvxZY//LOCj6Gs+eG7HKcTroRnodGD4e1Ix8QHOA6AqQAr9kguPRJ76z1s8uXQqtbnb8BIrZmo0eNY3wGJBR0N756WQnhA/QSWOZJHVR8Yw4CHKJN0Cjf0MaRrIbY8b++nz+v/pWkUmlbWhU4rdcwNHrzLnDTv1c4Jz2UEm30AxEvRwnNIGrKtdQMCDAzHOev7kivAoz6406NHhxiq9s7G96OZJ24q89rQeHeIBQ0mMznQIq0RYB75vFLV+XvlMPfZsIvHJZP90AlCE3Ez91v+h0+FHvk0R4hcJxCD/qQEpksZlRX8MmDLUyf/dUCE8CuoM6wliNFJKf/I0O8+JKZOPTm0Dv3fj8nV25Fyh0XwOPo3PV5g2qXeeVI7RqNJw5EJoW/qE1Ja6WZTgHQ19HKQxJ8bfXOAAvX5Epab8g/ustjwUjkBYdTFslZKuUElje8By5rW+f04gHEp9Mf3SBJ9yKHXB3mzVCQQsKQge+M72IJGGjxrD/Ry/nY6l6/i16edynjI6At9zYtlbY9Ur2bCBAXMNmCGMq8Z0MtN8+7h2oSW+UYVeRPWMzBqWP+rNBhyOGQdO2NipJjByty0coJqUUy88/hCp1hgPfXfdk27WEArj2a+D5Updu1fFt2PJS+82QbvLD5mYNTrjeHM0nnAhjRQXYgbHEXkWBEMKVqUZNnCBwJUFeM8ImcpYNHmLzcMbVA4uWzuyhEKnSb5O8xN3sjOlTivyF/gOQnKGuAlKuzb9Lck10NMiP0973KWpyioxqke68stUfi6J4aLqeK0yLnK7cmhUxNFaH6olwSn2+WwRPGAEtQBlS7oC2yy0mc7zJqih/KcN4cku2atNkCgAQpYj2rW5ViWqrVfKQdC9+ziJ+fTosk6f/NB/+/SsOjl2kqdLIXm5bY706NdfElYzQW6YjhK/K2rgC9VhpISxxUlNQ92mOxdNYH0Vf1+zhQ12N/UIi//NzRL4qRHrR946YGyReoVMmlMvp10iU2wDjOfYfNdI+8RJYimO+QnGDAX3M5QXNMuWjhNo3aE0+1j6jiC1pp7YisjKhLJXHo37FuRiDykj6732gVlzgHQQ/fVXgYZ89Dh9bauE40i0lBpj4ScwI1vQPXcr9OQzgBnF+/YXGeyVn9LO67tF0eTRO8GGGXqGMafoALAlpTdWYWy5eZ3NegwHB8lwU/TjKW4DGP/pbXCpcGlHXE4tUBOhAacr3QA5ZTHftn4VLrTr58weG74xFetsHo9MEgUF1iLRXivsLZBhojpN5/RTYXR7bQOL2MHGLAfWzvOUKAw/41n1G7BI413yoa9/Qe6VIIuoNqFrf/5U351NvwRkQ+9aKqgv/1tPFLA6PbOzNg3kJ4TN5ChAfSNMRvjX90qwqzNe7FjKfrGLidHaQj0qPFB6np8hU2TvD69gs2gNfP8DC68/j0maxcNCvDuyKQuYwy5ko6971mL9QQ6yTcTc0gx7OFlDrqoCW4e2F3nOLn4FtFFWpev5e2AqtVCq5j/i7qZUv5DBUZz2KWM4vuwbn6h58HS6902yAb4pxlhOtRpGeY1+TIod28iC2R7yV5ug1dhwDmz0pOk4tacSoIUJ6DYdmQlfYmHl2wKnQo5P3H3JfGpZY4nUEZRbnOWG/vBIOixnk6TnNMH7DY5UfyicO9EHDtz5se0O9NkYi/FawwZzFw99N1TjDPsKm2oRPT+M+dhPg/jXb9ETnhTMAimp+hSSeAMJhpEb8ULYDlaxSOiGqqTuK+sFfrpP/clFLhZQtNUjsX0E08ElEZzVjgZicDeU6lQGfwAEES+/zOJuwQoeuoiK/BivZLTcBZUFoA3lG7UuhjVS5WTQ1n1WeeBf5v2IznSt1FUMJ7VkikP4ZyLZvGfUX4sHdytoTkqSJ3LNWJB6uHmo1HSlMnupFE37YSn2df5tTvycUKZfFuBL6I0hsU4JLTgSewq+OdKDzHtBzPyKZSgV9NHLhinIKm+EhFkMzRQZXOOaDhnTAzLu56RaKqaIMk4cMRw7uasHlVNu2IDaAAIq8p8A3LoLNOO8vgEZDcXMOSLlX6MfU2kyWaSI03QdXzmFTX6kZyqeUL+WmTcOHHB8kbRJElyvaM0NDLHa95H5Hhlab4/lI5M0dBj6uT/RFQbAOP3+aDdvgCijyIM9+AWwVyNFkhmooelFyQXAki+nwz3LmwP9yLvztHBLQa5/MtZW8rFKXSgEeaDAzqVrSh9uzypBTOlIckTovrjQNONrMM3c5rKRU+IYmK96Jt6reEjTLB9POy/DfaAg1/8mcV3Ttr/CSLqDPbpC2WHpraC9qlkIYo5vxvejkOsW6mI+3dCBHq2RaU3K9nWSQfOyA0FyQ32g0LsQIHrz7zyLEKV9mYSFBflojxTRkoM16OF/F0FyzBz2NgHnsuEg/bkwUIj1He/KIGOlBsyol2PRdsHPKo6dPNG5zchOHst5uBlzdpCQyA+qeIKDvKTYTEPzIpQCtzZpT74R1ifS8hVR/d79I+P/XoZN8MvTJYH3ZrnyaXCWXARLWnFjJgYsysZUGrAcJRk6TuACWe6BwDzn/8sucqqjTC1SZV1p0HQ9i+RVBMK6y6uJyHpX3+yVxs9Faap1FG1x9tFZs6IWhHWLSQcewwNH69DX4IWWU4wGDM5C98S6eENSz974RcHBI7L8gb97EClfMuCYJvHieLj1waSJp2tRqiiY/k5m3nIxELXQH+uFsITcZMcciWOVehsnuDNQ9EGcBvT2FSdRE472UJ4DxioGkjuAxn2U6q5CB4DT9a9L6bdURVtq+OErVnBXoBQrQ4yGqOnZY32IxHNFzpHaxW1dArmVw0yhz3DqJVN0aEXoAbwqZo2KIqCjIvHYnNw/K0Y/78qBQm0Wwlvblq0UYSHS3CslocM89rg6eCGr3mhW9lt+yJvCMtuzAC5H1Enu/G4m6tLdryhrZle/0fGQmr++UTuBv0MxEKoetn96ap4o8BzkVfJJvShlOP2YTTt2y8W4JUgT206aTvC6KQQEMXQ5QIwuhXasQbfkgDT7/mCCpcdgRIOecAsBW2yvzgjbi7dkmHuGx10VuADLdO2Bvbe93bmp8yhCUjh7SkK20NXQjH8M345dMplj8JR4wZR9TfmCFxXvwOjMv8ro5jJQpaXxyqoIz2qJk/0OMAf6lHtFlknggQaZ8jA9dQ4wzxsyxoqQXfrFHCKJqxPJ+Y64YCKojvrA/KsgmYFbznYEvccVbX1WZeowTpqfFJSUvqkr+ZLmb47uMJ2+N5tOc40jqlZfP5AX4hbAzhDRCRp17VvcMQderObbcT7C8N4Jg7gHTwS6bTeDWAGBQsdV4vyLhSTaHVdvgfuyY1n/JfGMGqrVN8ALO6Iupbuk3i7qo4J1IF8Ice0rUhtTbVJeMu3EwhzzLwfVwWLrutnBzEH9CLAollvB1cxY2VO+RDeN11uH2H5DpMYqKflw0TCl2et6dnBSJTnV6TxLPn2n7yD5XdZW9HApn15XKzacV/DMs9kMnML7PAplOUDR+p68zq2rWNMxBBx2uc+4eIJoM2Xu1BS/ai7wF+H/jZQjuhzehEPbctIcC+4uDSD4OXHORQR1jW/l4i3EIuhA29bcq+V5vAc9n2og+lvX/rz36+P7XCKHHhrUXM/yislgicmyNf+5BD5YqlK6BjTHXLBmY+otgNcsJED+W2vQYBxU7Un7H2fCnIyi5lhCsBr5+3XxoqBCHim8XsDqmWG/hFH7MoktbylWiGqzQQySZrOVt+3BrmCWEolgH+AQGx577ZwWxHD/dr8V6yeco/Ft6aeqHD+XGWnlTO5VD1GTKamM3rpG6dDo3SW4O9YoRKtczu3tybRu06BqQF1RgLrxpSb2LJ8m+efa9f0CYMpq93x8f7Wi9cxPCZUE3jrza9xkkpSNC3mxrOoaPB9ilSld7xB9i4fFW2ouAwG+tsKxAJ+XUy1/oMS1TE9rS1dATvjnKmvHSyzxQMLsFo2CitaRtBgRwilEWybem5vZHndFzlhFWcYW04hVk3mIegKPgLmMBh6fPeLf6UvwzSYRamuaZQrZRHuvme6IP43v6cuCntmbpU7jmBmkyJNbyssaHqPW3ozGqGxB+uAGPfzdhdMM2L1Bpce4S4vnbl0VRq0jQtZ0X6tjOeiEV5qIN+tOaZ54wbcXs9cU3feQgdDpd50q9/1k7r8ijnfPAzrC7afspkefTqf4WZ84H5rPOOmYoV3bd/yNvBv+VuuGotq6WaiyPNIAn8kJdSqH1G4hbbVRDLO4EuCAPEk1TsyxZFQReTyzV/AxhoFa0uWBmSiQbIi5vO16mTgKocMgeXnlu0cXwlrm6t5idKRrR7fKZ5HPr6si3vNWKlVZNnK2MEiXHGAEsF2jI5FEk4WdWSCddN23CjcLSg3EDjGNOOdqmjaRirugq0AEco5Dqge0Qb4c63L5H++4ACh8ZZbodlb8NYfozl6EYTqU7zh/6vgyB+ICaIbz3LiiFnqMwFnkxjrBl78EGLbj7aFulQe0hMRG3EoffEhAvW7RboeNpvRdvq5DPg0unBr3+qrKA/v+coDQDbLpPBl/hB3EUlKUEr0YlBCL6UoWVMJRvNWWX8CcWYPnI0R6nRPBzkcHEH0lVCq2sDYlLZZovgRB+oIIdNi6ygXOldkWcIQbpIgjIuJiH5zlHGRDvGWmWajuw8vl7cp1d44wEBPFFETyhAfslJHYfabDjwFWijRytvpOREXK/rAc5F2id3o9odzSwAztsv7vJ4d4aUr4oq21nY0o7InaUdYf1mk6xuBgdyoJKO286KA/mzW9rOdg/CkrTqRTv3NYHRegbL5jxHvdkIN2prgH/cYiRvU8nEBL66vHWiGUDW3Fpvv0zCURnBdTtBPSFHQKZr6ZeKhO6InaV13Vl2nMQlJBFcadGnpJbZ40FW+FDF12sy4bO8BDnQCSIrVwAj9MR0eqJA0fzjWm9hQnbXo7BtNKYjaaNtmNjz7EL7DhN2j23A0lIuf5A+QH9+V/t/NjEWaTFGXnuwvAwmn2/w2J71zEXiAhkpP3w6Uz7sRX/kZHSWiesLwKiw9AHCJHnjbEaXoHtSokvRdZXPvKm3tCdQ2ovKwl42yaFpXh4fR/LyYWpns4OfRpMV2ZKEjpOM6kiSPi3Z36ujiO6/rpudjLspovz7HuAE12t7eF8ONsU7hAOnG+4QpYHrZ3WV/uwRglEeNhiLWyephkXTGHYoqtIDgAJVHuS8gMF8TA2WT/gc/mVJXYaUw9Kia2uKqlPomGGdD+nATtcA4J+UEkKCXT0gCMO0JrLqC7w64qAIEkvoaORhnmbKIxzWdS0egGetzfYO8JS1jH/demy9b77qKUZ0atRTeWG+3ThW3QzXs8VPVW4aZUlm04YW8dx01/Et9seYVtIjTZ9wOzCDWJJy5xaHAuTswFydIiOt8RNJvZ3JHMxVUqVlMVUnmHMsYDx2pgjkzlLmrPbQ1yt+nVGGaGyaAVVIzP01pk4qW5sG5coAQZCKrAaPCyRet8dpuzE5/2MpkkP/kot2e92vunVwgrM/v/hye3fqIs9h5+FNoB1xRtzN4rq1uMt4y//sPTDllUsdc5ozGlACNQEghgxI0ieXc/EHPyHqnF/yWWqz2CIO7BEVyqnOgfbYGOhpSpyuVxznWW+ZMA/YiWlNH9AFddhIFFjPVAhlD2aI4LbAw/jDuM/B3qeFldmzGKNmMPd7dkQknxHqb3EdqR3FUqy25w6wZ++0usiPbLtOkZXm8KufIQnULcrD6Mh36Now8FrFtBvtKcW6poEQxX7uvby08bBi7UN9XMqIuxg3/CiMkuAsLX3PMhOjVkJukN24Ec6sNdXxfNo9wdGRMn4mGJHo6znPqFh+F7kS/6KPg0dn0Xl+sXO+0mcyAD15JGp3/vigFcOIaUgxU+Yjksmeq7WN/e0K/6FBhCYXQ+GH2WL3QK62CBpr1rc/64aEk24o8slhxkiZgoj1bIVQ3jhQRErcmt03n9TSa3nLky53bppQ2K8G1JWoLnjo7o1bUNHvqPVcS4Epicz0vUz92cSwho9BKCVGPRzq/emVxV3arcQ4Hdxrb7+ouEDoxKmj5pAH79amPV9YbixE9cmhnzjG3n3qx0UoYszMw33QuNDDE56gfRDpeuJhCpS3xb5aLdecw8Gqx7md8aYYYS6gbyRPKJI12V4d3nOFmMR/efoJ9thxxVJcQgJQ7ypAau3HKQdULMwEXQB4hyTEZXDbyt5REoTW4P4RjOqnz6hh2oLhEnrhovzydmUyCOCsqeOcJRv5zfMuZaB45uGSwG9DYLExrS13UH8uS0c24sfdgCyEjHhhNZL4ihhQBBYbFl3a9COIPQ8Nq+um7EDFYEBL7qsG2B1gzMEIbPk0TE8I7yB5vFd+wGv5qPaGlj9tPIw7xuEFYed02adhxUlWjNAFOq8RO0XH0nVEY2dlvOYj/egtfCbzDhVAYnK+nXzE7l25ToFwpPb+Rdmf7oprLPDWUV1xzJIFvsIaYe3bp5J9o1ycOc/0BYkGc1ZT6HChNlnSR53qERRg3sHWrDqw7eQruHvnYlL2+bfNNqpbM2L9nvtVrHuAzVldh/EWR9g09je7/iwD333MW/R9qtS/GovjuB/oBDNNj8HopfdJLsQX3sTJ7jU1X7iCwrbrasW69mfQ2WK9lwT1655atGAsoSTbqUPYOjHEChsL7x6rFMqJ8LDgVoLXZy25C7x1U33Pk+MeEPnmmaRE+7ZgMW7LH0syhhaGuKPqKmwh/q1gRarwERZ0CEHsDHQ0vuDm8HEmtpm71dWUylaQc31n/BtebELgFUbx4nLwu79X6t3SYh41iTfAUJL/8qDohgxjFlvEkB9+m7KnUFHjdzVaUN4tAKaQgYHsq1QaPDPSXIfgop40jgu/uCkV8mJFxHWRyhDlAQkYtd2JZyHnaidrO+pXTCqPDVmHkFTqYlzyJAwJbgxCYfvckFdEeOqJIqpfsNLDVfko2P2jJwIh1FD1wN7mpk4ln05yuCW+sa0ClrcgNvHZRyhfiGr6XHuis0GfzRx3plnvSqmHKqSwzvwxjCDCuoivhuua+8BR2QYbtXsR1fb+DPabqUSux+AQwcMinEip3Te/DMie8w8mFLIDqeYnjpWXiHhijiETuPxBPxhuqYnghvF37Un41L+wqjm+ZtpSOr2CD2Nkm1rlHFuwhxoF3sIj8G2f9hMkV8JLqn2B9Znu1kQ0SjsUoWHfVcH4mK7K/TX0xDz6ZCleENyc/MlwsvpLc3lZ5Q81AQvTv6nF+ggv7l26DgVCoaFs6cpEfXaPKWYPsf92fuHERfQ3SGncXUgzH/uzmnb1ccnUh3UzEBvffZKtMEZpvuvz1GJ1Ng/jMNx3OuE+JIjJNmB9iGOaonbH1XJDzRD7PKYZj0+Xt+eLlaavuhCrtAnfrQcgLQlDo3EDVjv2mfx8SwZh7DtLNLiJUyMhUzSazc31TZFBVvNlWTYtU3ireFIiaDUaBgtr4iEnIW+8ubdOKo+gVpLluV8rVqgq6hM37s0+TY/cbURvVliRugIOoAqUK4LArXiIoRF5tRYhAVzrkUe4xVHhFfIeW7NeaPTuduLwDpdLBTg7jFmXfVixlB+9Qy2d9OikD+eHIQ1ZJ8zY//A8f9cTscyOkk/xnJWsBDmOoC/NrzhpUscb0EqINVenmNhU9zjgxhNgMfjkPsClSy5Gn5YijFfll+JgOb8HKXk4wCQEScBskqtULySkBwg22Xp7qkvHYEUEpaZlSIZGFpgp2/DPTW4KjiRVXwkFrKJFBu5sByRVNm69YC6dTB43UIlEN9YEUm0aQIWRMjSvdaQR7glDkIkWs7Q6x2ex3HnuVKQ1no+I9TsR1AfOQayYq3nftQlOEeYfSYocwyfbuD22QV4USxMpUZOiaggFzOGK5bhPY7iIwpvOEzLR89rc8sE5/hNKyy1qQ0xEG6u6i7V4nEv0mk3ZzF11ppA8kYGFQMDipkGpefqM3gXRlQoSCZcNByTAYbZkdr+fUo7wfDMAi9+eAG8NC7F0ZrgU68uqzKGJk3kJ6QQQZjfDTUsdxuUAVngfvsYQP3R5Nmu65zRWTcqsVIXNuBouIxKlq36u6BSNre6nugoSr0lJcD9Dr6ShtZk98CjTfesCs4NWqOKXZxpKWRvZ1LEFqOyX2J3B/57GAgYf04J7hMjjRXTyZd9tmgcmJSZ58bF9NEUW+aELiEqqVonHy0OelkocjewIcPSdH1rwWGoEWClsJdmBBZPbiMUH50OjPeXL+3ZyO7oSLAaqZZmPb2UbPnc+mKfZ09c4YpohuQnH3KGjTbNNQsCFTh+NiyQbpRHzuLvrHl7qZOuVXSoFphRRzTaHnpvzcK31cVmd4w9ek75LDBKwfNKH38Q/kf8D37R66bXbmSRXVcxMlRVkLuTo+eY9/Dy2wSq+3LBEqp8b4ccvsdpabKUQ0qpRmYJo4j9GL0fOyDopL4pcl2+0LRjqPhKo+SYMUhVIoA00z1tHrgVOgs2KtUc47wIqLWAzaYc+RiKxUIMU1LQochbvej4mocd/ZdGu6fwy64nmOGMoLEnCB0yxPoukn/LwCxkwAY+3AjQYkL0cviSVgS1ixyK47GZc5ZQ2gBKBXpOts2S/nwLvA8dzGctVSJgGQ9Se6Lh0mUi9IP6bG8r0G1D7BtF8flrkKgYD4l+msPATTjslcCBaIDMN6+fEoYDJbl2hdBjurbdEV0B0feFI8cmtfWzLoRhAzfwF3a6IwY4p3VOdjdJ16514Nk+t9Aez3u4mru8X5XjUkQEVU/QfHJ/bQj5mDogIOexeuZHleOFRejoEc298RxUSRSvoRCWesnKrxp3dD1DeKxeoQCDekDkWm7BmYXjuzYfXh/KS76xONCzhXpgEBlSNhoRbIW/ehglvtiYkJc5qph7NNhZCrp6iMfpW7Kt4ZuUrr96VAAF+TQDvblxxhJ8WtcK38WNmuzV5tN/YGIQIq6WiwYdQhYF90zaS3BDGvf4XN/yqABphgA6eIUEFsQzLEw5/jUWdpTSyFSR0KdopbofSu0kVc07hR7XViDS19b3Pe7lRzpPCP82D+adRTTmWy/CNgMa+hH3Nmo2bvrMjes8+mX8m4rb9ZOBuvuZPeauXS/iCXMUMqs5ZBmlGHjlidWzDmYcY6VYnF8Zo3tAGjEFz48S2STQR+2BEn9WZ9rcSeX+hxMk6eqBYcUE/bXWTDtlfdJDr0PGMDaGNB3HChVEEvGWUad9uJLFULZLH459HwC80lfXIWQe3Zf5di/36eh9/FrLfg7WA9/3sO9giMg/iYC1Kk2SW7kNqtAekazgfU1IZUd/+1MvbZRlwLE40ZmGstHxe5KK8+bN+iZTRwhR46bRT8u+sUJacFmkFGgHaQldBYzd/qcxX0aZ7v0uMFoIrYbyMlOO9DMjz2ehWiJ2SLCcyuZ7jnUIcFmAPvul632YhBcj2gkqAKQtXbaSGR3OvaKjfjKOfNY0OQnsIEUCV/O3d5jttyCL+RpmDq0ypk+5Y0l1mp/K7NWnSAmVHg27MUDTewjVctjFmWlkeqIcWoIA51K+Bimgc5h+N66aO1KoOlsGyod7JMBM4tAKQ1/2n8MhHNJTymEZP5ykj7/NH8YMEAu7GjkYoI1cxaB7erBNeBozzHdSVQ3epfcu5AOLpBvfsF7ESLRp9ZcmsBf9KZd0gzuVJecWN0n9lW/1Jj47ed9k/PVgK9HqYeKht9Zis4sQaCS60lcDNM2VQpnNhbASiL2eQUQmjiJL7QDYLUB18cT7QXG65WWCBOkZQYVQskzVvaddf8xZ9bmV2uDu4OUn9/3RCl570ZhEY5cf1/HEp4GZYpvv5+60xCz/dwsnGOD/okyvPZSON2cOwjxYmelWhdfIpZnRSdQYRsbUdKkvJJ/S9UZzr7U52YW1ETrGj0c8a1q45l/Z1CBVb3/9HWQQO+m7oZvVqO49MjoF5Gqve3mywzhKmXhfvWYG3xWPNu05w9tpsLLd1qA2A0+e0su6eSAwaJ1K3ol7uCAboQG2KO8osblzaOUZC8Pp21oHBGWMsa7KkWym7zue0COSgok1S2yRuup++UNMMgBEFpentr04uiDKb9r7Sgj/rvUq85z7YHUPFgwMoRwGI/tK4O2h0XKNXRDhHC4m9OkdlvbqKZ+ZOEXVNd1u4EEYGB5sm7Hh+f0XgU3zWRGTVWVUozS5y2wVIF08fAbTxtCS2pcnVFWK2vZQOVRM/Eeclhx4EsoGcv60f7T6/nkbV00dlben5aW5P7NbrQQRYRBie/rujTccEYJ3xB8m69iFd52aexNYVGoClWH0ge/sUjKijvTlc+sc4fuzudBShMaX/sJiDJrkvq8FdF4SuLUu6gVozH1RiL5/f7cqLUrnU6V/BFfgbqTJ/xMMw0A9tZCOYLuZ9EXdU+jhHWczSjt1Jv5yxs448yMcy6WI5nKPC2HLuJRN4KN9cqVOW2Z0yqo9qqidhE61L/+EFsR5s/f5RKKd6fHJRwfUMWpTpBEhOv7zVEmIzDEkL6x2QLGwwp3XBiaoY3niONOhtMGOXxH3csK03AFAGtZL1TVB54uyoJs2MUbVgBEFqAAGxJPY4RdZELTtiVCgeoD/Vi5Qai07BunPfuEJ79lkHvlRsT61gEpNK1ScTJbyKlGxf0asCqDt4C9d0Az6ngdQgWauWo7aCyeevwg0kdFQAcUj2qJwXYRBjzLKt1TAQcCaP69uQydARwnV8YsTePS7h+mV++aywM9BR4YuK3jHNKr2PKhdc0N+xdf1coYJ/129UsRNsTDvRnJz/QUNykncJ5LRpvLuAjlIPAJb0aPfgxXNXziTp6iUHV6RUVkPDRdq+sDu/jc2yjRAQbz7ft2lrSApgXeh9sfcwHr7CzBceEuwujWt5ExpwQtO/MwtxNx/M3RJslaqaKEnaBXXaN+JH45ZCUFizdlN5/izEKpaE3yvVGFMACMBwuH2JB/wW6cvdULMFM+feHFHAedYQmcljSExociLzvA0nibVdNYWvILOlU/xPCWk/VZeIvQQIW3DFFHdu/iGhzLWeaWVUdL/59Kxw8C9/7PtqhRQoxZgM2ROoUdj59rZSLaPMusQOYsPgqNd2t67UjjFBE0seQnDXZgbgC9QO587X1BoinFju11zP1t2bBqUSeMh2+sKgGt8t8y5rFj0go++drh48h6UGPL+79GGeHYq9c/KMkAtwTYQSJRwqQwJ8f3Db3I9rcGqmWqCyVvtCSacLcdrt2F42Ei63kIxQNT0jW73lEDwb2KHDE36PGsGq3g0lTkQLfCOmAaxx50g2lEt+IlkUobPIzoSurVfcJKosxiTaizQuXVkipzwpJoQfO+V8JPySMdj88tm6/Ta0L6i8nwWlcZ6s/9Wv7lwaCVQuWgUIKsgavzl4mwdqnv1jiBb0Fypt6Jmu6YljgsW34sovQyIEyCBSMdZ6S8RYHQkqtXAkGFbxvXfaZim+habwAAbQkv1wLkBo/sOagH7d2Xlq2VxwGFt1yeE8bgvuc+5Ru1BcnDTFYOig7e+s0oPTLolHncjiqrGfT6h3YGGo+pLMP3bpJby9fjvODoJsmAAKR9R6tPmOQiVBun4xtVnvqMNA78ibkxHI6kpNadO5Gq4WnHf7mqsmXi2OOS/PU4WLFIPiWIdxdMs8QMUjtlpK2Hhd76fmmgLxNlMNsCmwLC4GHInIxDvDr/kYk2sO1mEimtnRbIlC6/oeGgGpR4w3LEYwGrkK2aZHnFVKuAXIc2T76XcFU2uGB+Eako3/M2Umt64e/bCGRPMYYnDb/DWesUWBZ59LYk8QgoxayWulRrpOhAJxe8UczFfsgFidVI7iFLvtPpHzOh+mEKYlwIxBXtk9k83K9DruHA1uhlcPndGshdPqwqltK/d8mikIJboK8VcUJUnoylOva4WJ/AQQ0ibqvRa8HrNIhHjC6fbcpqvYwsLCpNtYG1qmWqcKtukEpbIyFvc/J1apTx//21d7kt7+ygHTysYaIAJkbpSIsxpFiP0UcHL8yBcMiKsbB3hPPuZUr0C/G1CxbPc9boracPwNhqrwM0zlniaqFWQnHEpx9nIPAtQVkVkHFTlzAQdEUXBB8B2O1ItRW3GuoqR4Jd00uCB/lCHSmPaqLLftMGL6sSYmWblCtLtGsCveU6k22WzQKRrmxP+GiXkDj8X2oFuUiPbDl7AEiyV7HRLFWKAn9xLabJ3yIuwXuX7FT7FPZb8OfCv9B9vpYJfGDPo7q44yEaWBablUfNAyByqn/Eq42a2r5xHhkLZVgfwFyTbDVfL44rJ0k0Ntr/3nRPv2+InNkrtlOivx0vH1iFQYIIm7H3nGQsghY+MXxNQw2Vnwkgds90pT8+tGsbiym6m20oqxMvt2A8g067o4TFToym/DBYMD3TGR+3qntZNJSz202bW0wUypg1kOg6bYo8jgx4Q23agTh1tVZQg/wVtqEvqu5/IjQgesidE8iA3I6BRqolIR63Ycks4AQSuZ2SOX2oEgGSvEgdDmSJp3J+bWg5einsral3hvyBo/qpfU+obqespwxotT8EUjyKshKVkGMN6zHo/KNVAdEKhZ2ycU5JiRR4HXXs39Chh3x4ribwQs7DIqEqPVQ/+CNtl/Pd/NPRMFLRvnV50CEQR/+we7q4dUTss5i7Fd0ivnPMhEBxVsE7zYXJvhoLKIiQL9nCO7Iv/prz39SZ3GY5dXU3bhnwg6ezdL5+YWkasZmRmAs3f5pLGgt+m3JVlfJSuv8MJrCRYw62YMc0SQ5MciTMBHHPbQxT39g7Bx101QTv5hx6U2jaAE7pWh8IoBeEITIZhAI9PtIRjCFoXo/wRpcHa2xN+UtJUYtwBrA1EPS75BZOrPeK4DDYyzm2U/+xqnQ3OWSoSAXIBSGAs92Y7zq6y2VmQIDhqfVCYm5zktdg83Aoqr4KFg5DxLbAUpxk4B0bRMUQf2u5/lxo1qFSYPAMCJfIt5ecJIF8oWN4tNiKN7MRvHIeQ5gFfxFvwgHItEVg8MawQXUHD/XbPq1PQDKpq7eiE3eEO9kbhHCGhJi5cajXoOult7wJsnrgE7LYnGAcpFAV72njUQmkqD2tx2oMcBRghhCcwwlpSrno5nrQo2HLOmukcAM84CzrPZ31XPsuDtUZoMHD/byalNSykonvNxZ7YaO3LRpBidrsMZit9qcD+ocrXUSLdtTNMu0X7PXumGZUlPSKJVeFR0U1hDdUH+zagKpXchbVEBFWbvsluNxDCVptg6nL2NrAPhK4DvI+wk1GlSOE9al3h4xEVElTpGzsWo/93lQMTruPcyYsKWATDPJplhU5W1q6HzEpnuLJmJwXeTA1LtxOtimb0WMfgFebx9JuVvHRKCoqGB3ILnXFgP0ViTgq/JIaqDPQ81NJhmUKV6j8RTau9j5YHil/HX4/4aUrDPuOzrE10LsVjGHBszo/T2lmG82q1Io1Ys2V+sA7lDoTr57sAzIENrYDuNfgn/mGjwM7AsF14dVht8rwtr4a9YvdkwsT4ovQ9VNBEIskVDEcRxk34Ica60NnjsAxRH4pm2jBBkqm7SbkkzlGBzy4s/DPkLRoHtmaXZwWioICVnfKbR2gYMC5X59EKBftlyh8+K3NGEq558mV718O4hkPmrJb2RputdCAQ+Wyuh/TPANTx6TZVYSqBXaWwzpoTW9KTCH/T6zsnJ0+AulCEhd0LZxQObt32EW8Os0LHUHT0Q85e04OVAuKUDxou9+SKbUjK32EE6UxeTba644Oigtg8F7UmL/WZToWL1lvmODCQxOenUJDHIVdh8pYoxU8lWqQbhhkHk073N+aemHyTA5/eocOeT6wpKrAG0UmF8LpQpi8XNf6+d/oEE7Ad7bPwLoC+fFCkpLbID3X4si0hSZNchfhhq3nUD0Bq1o/wpgfWOEY92kIoXe4VaQesjH4MI0s7gjYEoiwEZq63/VSJXBYmIvQ/fVN2tUTY9mWA9FxR0yHMOuy2F4Z7O6eJ6g56Qtx7gEhXNv+NCnlEU0GnBFRW6z9Nt5ZNu/vhqzI9HBeDntE2X0+DBsGla1tII8NqvlUL3RRRMMsl4Qxlkds9eDZEZW058wsAewi4kobCK48CDfrcmBanBJgvnfAlNWoAzBJrOyGYixSfgEt6+M2RAclAvi7uvTUK3dVamDzhZA3eceHbDDWUWpswNEejjNfultCaPuHqYc3zuPn4+8YoEIBEE3Z6e0KACJzgY8pBx4hxvwyXW9BJoOq3SEwSgFplCPL5CB0dSGSbm8meH4/agTX+c2cBmlQpCdSn/w1PsLrYYlPHBwJq+4ne7525rU5e9WDywMLyMrF4PRWo4jgPPuk0iynGowZAXfa97apFqp5QGIFpXRpNLtctJhHZKwIf4XvLDlc+xzUamJNTTxzCFi4OUhY3vG/QKk79mefAKcYGUKmKc3h4g2ZwnbcTzKETkMlgOWioWCxdDcC2lMrmNMwIfRdhr9cEdRYvyaexIz4GaNTYU7NcpyAfN0nXQ+d/a6KuMSTWFS2xtHvrqZ5TQJu4rJ9vfuXJnpBthyy/P2ZsdF84DFgqiL65JutoYWxlXSqTMaSe1dsELvJWJE1h7y/ZD2mQf87TfCa6I2MNe0yTPR6CvjYzoYmndj5U44Tq4te5EQu2Mtm2PvVNFH4Rmte4eNpr5yNvzRBIEyXDkgOVrgZH4sYsshSkCwxFI7fmShhTA9fIaDbtZw+qQKa7Zem7IWdyIwBC8lVVS37oKJZn6L/gabiup0k8D5nWuHU+ts5ioqb/Y3i+mpbdXQnDCwbCfYyAhCLFbi9PmdtcpT69pO1LRAfGbipFMTIvOIt09PmyahTRwRq9KZAzuhywo7IO84gYM0Bz50sv4pGhgu4hqW8DtwEItTiYbjCI+gA9hoW2gf2Ud2dczK8PQRwu2vByhwy0ReQqHsI1OAGbRkK25FzWwC/tLRGlyf1shUs4eYSiDu5dPk8iaFgbwcfH6/2HslOL027KBzYKPcbjhHB1N3mnvAHLrpvrGLztMiaBnZeAqK8KR0drOUh1x7loCshdyu9M9sS5ww/ADgM/IxHT/PGQf/AA3JQzfeA6OQc554b3F3dTBYWKulMjNVFnV9QBpWWhHsZE8UpzX1pyGQcYP6s12tdR2YPQp7P2PuiBZYJTbltiufQ057vkGA7dPsr0FCFJMv5Byhx1LQ1U1Kv1MxHNCd5e9zNS6x1pDGXzZ5CExYID47+077FowDZ+CnksVjuUE8XW4GVPuSy1lWqzN6wBtPy979LlAeH/gNECylH0XxXwvyV6FajcYJAkLDb9WRpQPh7nI3vk+ONZTgZ07LdJ56Lch9MHvd/TDQuARhm0hOuMbOftz5rZ8rpfBVCXzFQo/1nQeKs9wKGJpRmbSSQWUCFZ5HyjeDKd1QnaD0gx2NeMX0Y9accRCQlwMlPbCe520aJmBWm8SsTZPHteNz4ah7yxKLlR0Nl4RO/SZaWD15w9PeWwRspnMVZoHA6ymCaRpedJ9AL4prInNj6pOlsfvg2qnj28aQohxJkEekOdPnLU4EebMKyyunDQme2jprO3VLKmObY/5w86Ufl2WAlL18250o5Ef/0wqJFgNn45F+/BLweEg39mQxiyCwZBpA1DDi7GTJLDXDbEzuWEFDnhv9MOQMjGBtXXzX1+wQahzNEaqhqDX9sNfeSUAfGqJCYiPLVAGxEH3NnQba18HS+glXBXfDWoQBEOBEf7nulxCZsjRIDzevaMEiLA4O0sY7S4EpBZzbFhisA2P0haea2G8F1uZx7BBJDC70/y56OXdRQ/ixyVSE2y4Hv9JfTtI7nZ5xyFwAuPMKFIDDOxWmyFa05/cMpNanNH3R2kXZ05yg+WmVhf1iZRW2jm4WDsSepfBRh3gsjQoVsesaKGHzq7HJyBakCwjHW3lf/nIDIEiJwL3fQaABVomp0S7g+LMhx5RhErPA9eYfgKI+Ivf9dhAj4qBgXdR+46si1lJzInZw7S5h6LAOeZcpRzOxnMlSHx50wfay2+3U6J2PU09RCIBAfgjtFnXFXq8G30lm9+TKxphL5fC/WJaLFNoLwOxUg/U3YWswCXKfF4OIuiZSO9AlKabqVrwsoBWnf+YmHj1FQXW66t7GouhnCYEs9Mc8kpid6tfJLsE+sguAHzmNaxobFIUvY5WQmf5h8EJ29objHg2D4B6KlVPEj5A4eaw3fOszcrgLuIdnobu7EuBxMpZIyKuBgPQ3jzJc0oj1SHWN3e8pdeICf6A4tnBdGF4JcnRUk8X4mhqya5MTmgsQzcLc+FVsyGYbo46tcOoFMr2Yt4XxbE0uUxz4LJxaLCjUPcf/Zjpg2LRINijzmjQb8dDn8CaiKy/SvgVtiqSLIfzoHvyoXvQN+jNMGU7VaFSaDJXIUkj4jPgV2CpLWA05kclINFu8YlqzD9DAOhucKMMnGr3AulkmBv17ItbBz7+mtjRN8mt70dqplFle1cvPyrqjw/59F92DHImUA9+rInBOw4qCpAy4myZ2lgy+PKB2MuHHbYiD+rBQmaZHFgPNGRnr1gxgeK9Cjv/un9trV0VYLAlp4AD0F/ywYNCQW5t7ROZR+hnh5MM1H9Bfmf6tBv3dPLmtPa0wFyPLQXOQxeUkI1NB/6PLKvnSPY/4jzQS0MJn3pUaTVbi+xLMOs9DKq8IZwA+quraGk9ZTTsdwMttkDz3ShWkfylSid/fzjmis1KJ8TBR/pQI0uHILWnlVzYnEueWkQOPIPHfXBtwDM96H6ioeBjRWVeu1lZ8YpVcBtflpE45Yu582y7KY4Opj3X9j+1noUoxN+Sygh91h8p2iXKfzW2MAQfBKnWD/7fp6XXQof/Q60ZGsAZ6y60N+XnKrJ54HQbirKuPQSe55/55ZVC7EoFN8o7sCPXGfhEiuLBARndxew1K97BYphs0CAqk9u4guOt6LQv13yktdbMuuPN7ghyjVAbX04EbkbSMniZdJxPDhRmrFeEiy4P168NB117LopcMf0DClQZ3w8Phpn/8tTqlMs2NAAK8FTQOEJmL6zn2KqbJ0/ZOemPkEolFY4Ww82cuRctQjRHhHKW9y1xGKVy34oCY+yBms9NoH829C0AJenIzMFd6ogv42+lnyVE4M/VwbAvxxz2+NqD1LZidSSM1whV5kBFUvfBW6zjbUR5R1M6RUG2Y3eJna8pqZKzrDN82lrWUf7MmZ1wPVDjIUYu/d/zZFnye+ACjle2aLPGuibUGJVzTYJiqt0iQODaym0Go6ACUcgLx1brOJ4hRaZa5jwcxVuYU0CktgA8AEd3FQIMK84CoBwliatzm7pxjoBwV0LqR10oRN0ODyTDhCjz1oQzbLcQogZma3CF8cW0lnTVTkvfiqcd2qRR60afY05vE7J9BL6iPWtqZXZY8YeZVZHQLI59NdSX5O2tiuW7HETjInFoG+5kJ97bJLfoiRxP1xaLhjggfZla9GIs+BJWugUxMQxdR4obavqgBmf6Xo58Ntoy3Xem2D6F/e4HK701OCv48I9/CxBaf62gK+SJk73wuH1tkCklACtLfW6JqKChG6YXL06ZyjltgUZMd21yEJ9GWUVAjj2A2kk5z8loS7kfD37N7FZ4kaNfio76/gx3Nc+bF5BFPwGE0YvKix4Cxjlt0f47U//8dCC7Lo0SHzfQLqMAc5b1slHlKmJNkw5sMaCe4q2g5aywC1UjkesHzpqKTWawp9ZjFXZP/YYiq9kmKq9w7NLuvKReAREPFrZ16SZdY8Jm4kusBM6UQhTsJmbxWlMGJ5PeVO5TOZXTo+P1wlMq1KVzZBNodpp+PqJQNoGThQhCPqRgATyLxd9YuCvmen4Zrp7rXkgqn+N9LQFrgqauSQF7ZqdPAV0AE37Om7CksNIfdwger2saB3SHqHA7er9Z8ye03CaSOvWgHsOg4iRjRG3AJYyYiE8lcGyAPWBuMLt3xL33ek7BYs8iQkH3yyymPzNzoFs//GtaF3I4/wLPSAWbVm9NnyeykJ47YIlPrI3wlNw4V/bUpldHjvSPWnw4LlhDH0+dwIwYZAgH/PvIIB5VofpeX00DCxyvnX2yAz/eNMJNlneGGLSQHcOxz2uB1MNXOr2Yyf2L/TzhqMFcsIce7N//PZKmOpAgppFHE3id+c8e+QcQ/y+77FZeXVAMfIpMqfnCjneEGf5HHs8X3aJkgRkWBJlr72FGHND/wrTrEzcRCxp52vUfpGkeHZJWxsg5oWFxLusDAIRFBneFjve4x6JUyk3Rn2v0RBUX+z9vjNI4qn1hzC9VtJHflAF/INquKJtp7KZrbe7LHi6wOGINgOtm6x7DZv2c33m3nIU2C9R86z/wc9oiFtJAeFswBLSbWZSwULyHuxOoMFXpMkubGdVd13wYz/BGNtNM157M7bN+f5GSYNoLt5rxuTCBNWCMpMALk1zk/zmjhs9TdKUYTv8iEh5PVapcQnHMyLQ7JghU+gFkb6/oR5Z5DEx4SEpYHHrdMSHnV9e4CBiRYLlsMmblcGwxQTYE+33sY9tGh5YcHIHjbQfPa3bq4iJP+s30xRZXS9o0rAmqNypidc6rEMwfdOu9XevBBU9E9JouPYk0Xetf8NCv3oe7S/eoKet7Zu1Lm6Xv5RrNodHb+WaTiUDF4KRQevszaUFDs31oAiI+TRDLCWH7zUREOWB6pN1ENhX6hm9t2W0UJl/0H5alt0izDXdZsi7BNlZRPY+V0zIv9Ies36Txt31MKh+VsAWCZS0OO9Ecm3VnpdJu8Fr0e/rhBpVLROwYYKYJQ16FEDiyUsFNJ7r1gmkV52iGmiSwHKwnErdAD3TdLPiFjBxhDgShuFD4UZ4vbTMSqBbhFDjclZq7yc9SEXzsJaxewJsiasz47lfz16YyGbxgoE4q+fTDX5g+YN20CDNisQOpflDlbv1edA6j3fS5uec0SKSpgj0G7QxF54WdVSe7u/TAORb89lxe8RpdhPylLjeIPxJ16YEkDUwz16Pk68LzD+9WFcdUt7QFcZ24zCRLCcy1N2Z2J5aBAo01OxaMcsLaf11447mnXNhJCdVGxTkr1qPQkRBTTHWpYh2cifIasYquc5sVelQAz/a7LkN8DSQXG2EloaxjuoC57tilHCXUAj1pKb+wNXkE1okZgA1XS9qEdtZmffikJfl0aK88ViCt/dyVWiz0eeJqGA8fIEzT6VWFooKPaTpxpXuukB6lyyQsJ6P6AIqU/aeYpErl/8Qbl4hykOsWJY7VeC7Z7m/U4sOV/Q7QUFGjVeVZaPhGLYUYHGKihg/bnvcUVOrUq+0VyScALZAI0RxkgQLe3iksZ3heziB2CCIIpC9Uac1Ud7hTct3eSTSmmop7Lea1j/pa3qhg5c92zcOMxaFvAcJZhyKlGlVIseYI8XcpfB5m3rvWODCh3CBbDgFGOJznoBpHtbMLzWCCdsXXI469V46RlIEinUjrUDzkL8CqKuC1ubYjZIwB5BsZBaankfZ1lOGLyl1/t0AT+4Lg65620ltcsJw5pthdvCl2aOMsG8U7EBsx0W0rsN2aTaEe0VrtwaD3QqZ4cL3VddGqLin662RZZmRDkU9t+YuEjOeIucRbDC63baHFur4tNVOev+Qp1/s1YuzlkN66JpD1ZffsU2pa5hQ6ZL4foSXGxFSUDVYSYS3OsPFyWKpOd5xFQm47Nw2P+RqZorOjZbPlhAIn9BbEsFMAuGIL/MoTqsla4LvMq78RCwU9qM4+pimY0N1FiuOQffZsYNm9RPnEXgoL0AAsSaqGgIkzmfcGKjMBRyAckFybherZjuUKHiVdA0NvdWPSSQE7zgVCITUNQCzMhhPTS+0BBzx3TDGvkZhJDkP87634Rfs8gVL93wBCrMVxfNx5d2I5yTquOb0BuVALg9XL31GWwzBA5rHHaA+J/6/g+7T5QALPEvL7s7v3o8qnOKBAO6RoaUSsTdGH0zdyLlWmeAbLzGxrYZB5evVBhMNK8L3tQqe5O/VODEqm95ZwN4GF/hnaIjGqiqtTXvUxTXPW/uTiCNIIrlBGh+R6lu5X2GTDjoqwzpy6npAvXmsp7zTo5SXTAUjMapnHhtV6/uv2KVSYUfzN1L3rYW/j4K2D2IqoQV5RtTm00tiHnXGmF/H9TmobGrMbirYJaJQ9jX6qzPykqViUPUXi/ha4cOfvcmiIh/zH5lYLcsSeF0n4fXipS0ESkcKP527RF7JnaxcJkDf59CMRjJPO+aPU5tzGaMchQdvDnuyXibfyCJR//H+YCRSn2i/qegqqYmFANoqRQ8fLlJYNcb9HffDkiZSHM0WUovUWd8mMt4TypshE4pW5T6VIqidd52dColQ4S8/zu56p2C0jz5mhJBh7frUWZnPp/QQPdvHeO3PY+5ALPeXgBlHet+Py1xIAfinuss0gfshtf6b6Y/Qy8L0F5Z4X3sUhophWclMbecXCwUaj+1UrCSLOieSbpK1+Yle35BvzADJbJ84yobI9YDbX4d0m1vVBtXqa8kt8ZAYhyQWp6mfXQrlqLblGGUArdp4aVR6MwikYDR9mU7KcTTDe683OCtOo9GPWwrJFBztupUzt+Mn4Lqb4mH7FFOCINZhGYwJZN55meNpbGA9yQ9+xtUe1PVWDBTPZlgETaCRxdHSC32F8MYeyxEi24lqmTOC+L2ZOFUCJ4emgaig0TufGETzDPF0URbk17nagnw5vtKCYaQksE/8ze8umdTCDAVU8Ll+izXQhgZhvpsEXUuC6pcviLkI4cA2OLIpFXy7jbO93SeulI7le4iTFgk46bGTvdVqjyLCvm4YovWIa3MfvUXo2Ql16jpMYtvloUo3xyO8b13GWquV/kkWSyrrHmXs0WQE+Db+E5o879ZLDRh/ELTMQyRY3Glae3Udw4cICE/Xy/dpa5D6oij9huwodMLJa0EFeqgVMMg4PlkuUbjN7XeXQSts0PI5D3yybTm2fHNudkLGMfBZJwNC0vrrStbIA3uI30RrwZsZ5HQPMf4FEMuwGAIL0qZKI70RjRJ7Hm3aokf+i6MhyyOlR1jIPPdTOKOZnN5ZmEMTvt/5wRiLsBDEmi4+T7EeELNrqIoTKjdvZFPNtPIZcAYE8z+Y7SJ+g3mPNDdG1iV7u4YhDS2TAvlC8wSCaguLKruvgEK80RBJNSLtebMc+a3NIzavb2mW8okmy7P9ERuwxbMZnrCl6blJanDKxDJUdMnrG0rPzQxaw1D6tXl6lmtCixcQve20xEBSsZY1SoFAzS183Ae0Vp//FCqptvG4PS7MArjGPXtg36jpmNO1D277m10Tf5ymAtxG+Zr8VecDnlN9ocT3Dn0oJNjhUZYSIqCz57KgySkJ9xPN0uqyulZYwHxLP4JRLy/YRZFRZAaZFkOFvW+cFAos7qfZjVV7a5q75piYGasSjUCBfnGwgTX48abg9bZsQEE+OOPzSfsgAaPfgeK1EGKhtyJ8ViNCY+w5pzJE+7jcmvlWkI2hofDSAo6RB1XhZwP271+IlOeIyCs0q87z4PJtfQPX//p0QNgOTOwwhIEqdGcBRcrKVidOP+ZFIu7+l0gaociVgQkHx8tRdODpTIAQT0P+Cg3fl13Qtxlnpt2tTK+S9QNDsZQFrbt2IX6i6DnJgZjuBhabthWmvqn9vdej6c64KMWnA2j9mxdfg7zocwlPObRzz1fdVerJvBLI1rkR+YLkVgIXyiTZ5blyJhYiK1VB1IPUmyKIqq6KR8f6KeyaORXFDPDgerZHZoJKPfT+3i12/M4vKbNbhANSguqDFbrPRXft3V40awfmy79lX2No0VgOKpr6NE+FG5IYKUHkqihXLODk1vJkNTMyiByDr9OGGC9I5BBfSDH46GT5d73Gz6Ipsjnv1Z0US2HMsbO+/wzxtE4m0wEKM4XYqwPASA/SC23M87+vKtqSMURTfjfBC5RRPtsFoNd1TZ97CsS8/TZfv92wr6pkh2X2QzrgMipAWDihfJS7O8aZ135gFObqvJPrEfLYOirSlXAllc1ET42ccxU8CCM8cJ8lbv62B2MbHeDp89BEYNvlBgeE4Sd1uFDBOJPJ0qhOeXrgJN1Ez1sX7tZa6Tfzt7v4oSNm8Sf24pC9aCQ4mcCLJNBKmjmUC+R8cobFZQ+3MC2tGL3zDVrku724jYn3KZHBqpom1RSwJ1qK3YEJAaf0tz4GQCANuoXXfLFpWmKUF52a2TIvBmdlsGcMinRvEUrxtRqD+31lqeVRGl47wX1uO5/tyK+wBGouq7KP5EowDV+Hoyb3InYMuiq6fiaLBNtiPZuXJC9wTJC6U8DRaKSwHj1/25AC+zl74XHqJ4Kgm7IPGWadReVa0cwieSxw8DSi6m70gjEu6b9GhpJc/yu6enpINdteo0rOV/L+/4vNUQ92bsw/ToOJjy10ji3qp3TzVyoyryYWvn5SGy26/I8HrH4+Fy9r2CFk7QPLapb+5drXD2TiOe+zQyv/l5N56DPaw08SJ5wsABnUuA8DRln6vyCVgqS4nF2XaoQnCf3nwQB9wXERLPjWmmEQg2R8t5Hv31956Uw0a1NUrUpCUJNt/028JvtcrxdpqLnMUMoDBiwnk5dbeJR2ofR5i2il0tYtWNtPsBfCAYZM18SPG4bPKFm0zG5DA4fvOQiiv0cvoBp0eR90NgQKkcPKns0A7B7t8KKfpH9qzbHmcxlsVzhZIeoREdhZEhuBSfW7DRNkrysk7EzM4B1rjKhAbvSdEDDXRf8G3FEUpu6bzUE70pYPUG65UfxwhdjvedbqWz6teYdgBt1ctyG28WUwi7V144+Uj5VhE/uJo3JN0ALVsNShUadAWgg6v0kQz+vxCqE8sMXhIabNGTTS4EjXwdsqRxRllfGPGod6KVXxKHonbkXlMRTJO5sSkT62TkuGYu9bWHnNCC+5eqOuymneFMH1PQilpRJn+Dz7z0BgzNVKA/0UlkGHgrJxGGbQFZIE7A9da/aoNMJ3jY3wZIMRkcIYyKXOfylIrAY0YI7d4lhIIDY5lqklJ/tSO/xy5op3D5mhhsY5Cn5MzqvnW8Uz6gTGyh99iT8yt+G2KTI/YXcy0TAguKxC37YH/QEtdUq4DhL3DNgwlLWvK4JKc4kaj6uvqRmLyvfZ0hsSTwVCnjKMcu9yDnKoJTjfXUPsWYelMNqh7661sy9vdMZMQT3+OOqe53vKO27t+hWA3Grr2ip36fEc4at8WTI2/kkIXD3/dMyYt8safBIlerAOHvHp/ipArV3pgpu1xaNy8hhk1eGWhlU4mRgfEOnKnPEYBFHH0dA3JEd9y4tpQhTp4mT5KEQotuJvtA/HxbuVEk2U+OyZwzrusVk0p/Z/+KUR4mF9nte3vC8PWz511xh8fu36PRxF+dc06egGmh70UCXPajKuqH8ANB2OaclazI03SafxscUxuSCojU6VYMPUCST4zHR7PEMjTjfIcpS8FBJE5pQv3i3vB28gVROmQG7Cw14t873WPw+J13sdSKSnfIKogjtusb5VywhHYBcQB80Qtq8/6gaXnVRjWnveNmQw+lxncaTJc351cTufAwdyE6xnRA89JFY4iPSdTJjpwYIQSCI6TG5KHkRhT3XFvh/tPhNj1A4QQhIos+3vkb9Xx6BwMxbs/bWilRdwacR0s0bWhiVNGSFN62aFCqdSVXVpgV+/lxrxy5seCFDgMSRuOQtePiA1HAcwgxCEI3g0GnaK0CwBVN8KcviSFFVA1nQKI4wO4a1TPPQteA/jmpnmB6t0VaptCkA2gIBdDTUTYWZB0wDWD9N3qyVpWwB9ucT+pgRoNaaM8aZDrZUTnqzDKeEvFPo4l7ibrJC6APBi4ZBQXzvkFD058Ms6Ljr1DCBBSLnyIbAS+iKiGe9Xor9QRnnXWzpAFydB7Lhe/UjWT1kYLAaz3CZau7G7g6sGHnEBPm0+DImFl1RNRSx1uQrWp9sss11o7gVebDRZHHKzaDckd3yUYj3+nqMb9oSaiqE1Myj9yJsKGwjpB4FKet7XSEHklEhzfoNqL18kQfmoRZmGlKevb86Lo7tSlQKOWZQP6uaGalv74nYXXtaCAWQzUy3phSce8evA+K5usmayyR7nvvooxOpPGprpEABb4rwuzEKCGeLqCDoLhEvZ9ra8MD5JLT85L0zSqkHArvqZupUeK5VJJ5MsD/CdYAlDAPvpS/lzKiorSOIg5E6UUY7Y1BMU7gy9cPMomIdP9pC4v0KH/3zJNzwXrUfL8k03KBHPdPAgj/TLSvPcSDWEwD4Y/4YOHXzbAZxciA2olDsK4RKT0Teiz0fDxuxlVQURnncFGARbYOs5Km17+L8kON/SR5hCgD0Puri0cIiRdQi65hgKg9zQLrDXTVkFKmbTaS0+OVmImplh7wOAGSbCgoDKT8jIdBncV2sIIGXm2xomto8bn03Jt4Et91LZRVkPmhbAFab6i0+Ur1VkGQBMZxqOe8zJXTKuYmYFJJwUYM8/PM6sS7XWji9k2u8mVMfHoNxLA2GDZUpn8zFJ/cLdF/XUqdALnaXPGmAVQpaKVzzCDbvUouKkB9y4dSMytvcpzwUptcW/HOWYuHZxX9NJNYFSzZw2y7ZFD/NK10lwEmKmpWM6zr9LKf1Dj6BzFs9o/xHpv+PvqvQaYlA3m/Lhi/m9zcT/ekYekSgSMXMA+d84PRKEiC/9N0vCMKp5xcWT6g4K7sR4Dz6VzBNDlKd5rB4T9S7METxDWsSmMjMIP4tR1c0yh9np5OzELyJKyZqgzelIf/lChFBUFqGjWL8ppqzGaUElIVpRbr+J3SbE083sJikcvkrzOIjucn7AUh9DycrSEIRKzX9eM1/I3x2cAAuZUyJbjf8qscE8SLEE8m0Cdg5+jL/9s+Mp0mVTnLcU64Ed8CD1AXCxboQ14AsFyJuMXyOnhUNZzDTDswOOPjpqq+DU20pVgP7qe+7s0XslIDTVSN3eGVi4tNGUIMYQF7k2gLWKaGOIcmqVX5peIvVBChkMoz/wjQKOqhLwkdUbmKvl3tV+Hob9EluBwDbsNOpqK3n3porAMhbMOjJU3wzgQuS+1K0zJFcDTFFYjPbhw5OcdZEAvx5gIALXFX93AD6VLMv4inf7N0Vi1sknhkpfK/iDIw88Su7UJ+K7+mUtY/ba7GO+GHqklkQKouJY6Yr7w7eOOrCFAxaf+oaUVFIEMXcw9PwJ2cChj9uUi2p3x2r35V+4q3h8/nZ9KEFCJ9lIlOM1X5KWdxFPovMc0QpdHif1X3jd9s3/dOh4wY0akb1cDuRDjq092/SbwX9UH7sijDbhq+Xy74Nf2GkUEkQDIRJdbTBL6M6dygZaLhx9NgkCc9HBp+SylFZFmIdeYzSB3NxFLPGDmHRzqZQpSkREbg1c2NSzImAxW95HcOPk6yRndgNe+21AqOVcXTxz8iwV4qzUwZRpBbHsB3O8u7EegogRkR5UXpwSeCa7gmxiCxWi16EYK144vBr6vtW/QH6l30At4QWGQ5w5l+43b6+26OzUiJuhVsewWTHJo81CcsNXu4NgCowLRxiHpNhxnZ/+iFdr6qr+cNgmFTOWYaMfR6ZrY+MIwNiMuJ+Dww7UMoP2a2ZmzOscFvcCosL0ZRGLGPAlAf/8ufL9WhmC0QHF165UIPEAMriCHLK3MaL6F5JI9PL546RIn8ZHjhWAnF81Vu817Pk3VU8/jm6UktSjGIE0y6FBWn36Knv4fK96/yZz/2xdePFQ0zUkfYiJG0oVNhGkfSKqapldC3tlgI6peVvMttBpG1H37M+OwozXBTgE4u8X0HCzWF44MoKnxPvFCPfS1FKXAFGTAmIpO2Cr+DsiPcfrCE61meaQpmaWqbMFRYGiBZy1ycTCWZOYvyTY5xSgggAeldGrJEX/A2jhQn1Nx+MPesi1fA2ms2ItidBqMIBbxmHFjjkN/ca/hqq7sNeZLcki2bj9+oO7UXqt1TsODWdyh06KYYnyX7ltXiopmmsuyof473mjQ3WFtmCQCGGq5uMmOA9LnPqKeMI+nQxLkJmSA4kGpEM4oN7bz8Ur/nV2zk9yfcvWsr6BTA1cJmnxdX2ZDb+HYccl6QstYxEOzcZLYlcRmZ/6rd9uNY0JFJVDSI0NEvDP3Htz47o4FDCm90XD0qPMh0WL78VVC+7ws8hXe/3JHoxBfpU1K4au8hRXJV8zCh6AuH9dXXWjboQ5BVtrOHYOm6tpc8Qjp3r5eYM2aJweVRcmyIlyZvpwDUu4rJ4JdFuD5NyXibixYeUr0Y9XQG2feUslQGwKvDqDMKqDQGT0ptXKbsRXX36xEZGzojKZmfqpZoVo0t3Upm51feayO7elGYCAbCAZkEX/HXlmxd/fdPtNjXjGODsMQdmHAPtPk7NRL4oKXXZquQHIDhGWSkUmHxQ+PSgFgU+E0vCv9K1kHbDAY2bgDjg1jXlRoD0NYmtwqW9BJHPf1ICPf67OzS5zWyO8iarCn19EHFUGr45du3xuk0+jngsFUlmSoa4sKgoKWqdGvWBX6OzvcACHGSdRUw6f1aBazLvYDGhbRlb/PnQj+Js++5rVL7WIdFa72iHTROjxcuo5LzWHgM/xX+uMIJrV6HDSTFNRq8tc08josa3J9LL2yZaOym58Heo7qtJVPwO0F7Y0+tNCBZtfqMA5MjWttphoi7lqoaQ7CwEkvOKdXfetlxMd5iJhzuEOTYCU0cGydbxWsHqbzA6QRAVl+Fff4R4hNHiF90y1xoBOT0sttrINqGkmlV7ZdBIVl80AgPqiYFZYNy9/d4Jnp0U3ApdPx2yw7M3mrhULBIGAh1HhxsYwMJXGFSWa+Tb/LIw7tDbfsl2lVPZ7GbsHThggRmeuVkfK8UfF1W9clOWJ31siTNvoDKUN6rVcVZe7JMhAtsMR+27ifA6GaSsI2nC7+4eYFi9em++dALwWiSbtexVb9vpy01AGHeyEDwEJEd/2jSezyehPbqyDwK0yn7e+z3GD1glOoFN6QDLfhmtUh4n1xXhQu1POjbROZCPFalwFSEhakLXMEFZVRDThk8YhCVYQdkyESO3JZ2RD7F6GD3thNbye5fByl5EVM1ZhKUJUXUooDYdm0ZEiQRkCFeNpxwRcdSwTqLWeV1CLB4qvrCicZ2vFua+6sSjUGd53NSFOC9HsNLH7+tW9WORrwBnGP/Jt5ItGtteHwDE7lHNSIryRC354rIiWof7LWWjMnz6owhO5bFD1QYICWCGHov667uPrATYg2Ouo3auJM5PA/2YI31TTrAhCxJNAEqR8fWv3326ftPeGqzvFRquKd3chAFnqrt1OuGc0VvtHNwsZL9o2+F2abd9TTbkm9h/fHLVHQpFAc2RmnkDqluTlOqOQkt3Bho8dlArL2M/w3WQfqQkdXwFZz60ISSYV/wTJ9mwNdfcxjphqUpu2PWmWObHaVEoTIB0lDvU/am87IEtYM1kVQ6Od3V00C4XoxfrTdiLIh0XwH7taFroehx9ZtqsUyq/WmjFb0XWcmhHEcAHEn8gFAmkw5teEWFdjfBw4nUqrenL35s5coSQ/qvcx7+IAEu8dBsTV6sBbrU/W7LYR81mIMvDNOTOvkvG5Q1xHK9ruqXlEyhZ+X8T5lJPKSYdMQk2WGlXKHWs09/DQ0CVm7Ck+TsUq4DdHpp8YQgOjB8Z/dItMlhaEP8p8cUnYYBIjZovhaHqQHVjo9YjXQHbcLby/a7R6uH/No5RQ7A8x2PisQz/DaGPmxGfNMn+hg/CRxiUwGB7lNeObHEDHwFJnNpZ7nkW6oDrzHYlMnYT6ru4e/PfIPX+3MixjEwOoZNOmu5aQJECNh9rDE861YJ85Ft+8uQd+4ZZMaSlnxRekp959cQIOnDvPKYoo6LqeQ8KBSE0km97TgXuMekDO/oTMHiwIrN1ddI/sWKFYjeLpigZxywrCfNiEe6i382mX8UL/csQHsZ5bH9JEvaKw9XokHSsVHWc4MCiU7mYPODHG0V1Uuus2YBmxkU0oyfPqtOdanlWbhn5wUiy+O4o6bs9RrTiGE4t7tN5dNWwfF1XQpeSzXJISDzt6hZex/QL2Jx7oFVUkj2F4+tVDjEhaOoU7uHVaKPfX7uhNXNvYhVe6ZxTgskXUYHVHtt3foYPuq/zosFx2WzONc1+gpYIDzA9yNPr4cI5Se9sQBli0O061JTBADcjJalnrid2DK9mU5jC/cQ1eTVJWv20y/O1EqFIr140pOGyQ8mxCqcP3/sDGWQU2QJZmhfKneFLWNrU5pphRo+K5eEfi77/lcM2dFaJdRcbGf4vxnxE6QHKnSvs1fKdsUn2YSJn9x5XpoVmAvOQQveFUXA+MGnM9WbKUnmvF5bJWTLEoWQtILb13VxQxkvOmeAihW1hzZd4Rvd9mOOWDqhRPfCo2uXaBaKGLrigBB4KEqVeHyU5W5wD3Ua0XISGW59APMZ93Ho0E0Hl0wp3080/OuI/E16oB0+7bZQBXwOFlRtgwVrRbjZPzBk1e836F5cJ8kacWaVYSUaKwshZlWB1ZNSPdMw0xOTwm9rf4qh62WFankhleVOCHTs6/dYd1+9e+SfnbKOT0DHC10s2ipxBDv7wyo76XtRtJxJqUfXhB0eEEqZyL/ju/rC/ooXMwXrJI+dowrRcGkAgcFdtyVnngmpbSIekCRIgx1COnybdVegll+3Yc3bO5CQ+NRYnhASaK3ZzBp4jMFyD9tPMeUMWfsT7TdEB88t22xhirhT//gFUT/hveuawk3QWXjKRBTzMaTGLDcMFob5aNJQEiv4bOPn/c6zXXpZ09dpTmNWaK4vubSJH5HIM0fftcpr6KgKljWYWu1kiFA+hlqP5zsExlhL3FIpb+SHXcBhQicFBmr9v73GkxvZxlV8ds1lwB43kMdRMbfDv8RWYPYJXnxalJsysuih6y/POu6UDtAdmLn6fGsuWEK5KzpJEEe+9QM9lz5hpmUT9EFyWOu3/srKQ/6fsnV5tU0VeYrX/6TBNLHuPbU317m3QwNiMeMQtF9gruhYN6sDgQtuOOPfPizvDc7DXd9atfol8UMzfnXhDWah8ywwKBWgPaATpPjHINmpbXwtiEkXYj2tbgYiO2LZN9T/6HDOo/q1YA7TjAmQwiGZiIE4N4kUMSOYZIoVQ8ckMehIL3zjbaUobwdOeTLjgt1St+bwgc6Vf821K7dzkcon002cFko3ZgjtNqd7CyI5uLX5l47G/dbSsV1q372oKdczdSXJXNfWqhEey0mLDqkO/PfRXVToDbh2cP0/FADR2Kl1TXrbMK4lU0nSMU0b+purVjvTfY6rGyz3ARJQRWM7uP+BA7bQ3V6D0Qb+lVZXVJ3SUkyqzgUEX5td4F+DP9mreuIPNxuEg7kkIkODp23QI0HBjrpQNTVN6Go7a68alGQFOZ1f/RSss8h+g9C5VVs8PPtbqNdzMJWoGc0ouBgFov19qK7bIOikfet1tuFm75apbVUd1MTRabUIZLfHCo7xoL8KdK8pB7A1EDymtKQePTMwPq1kVyE6ysqWoUDzZHxeOkLWticRHpJlglJ6LtOl9dE8o3YIRs2VVERyPH8T8X/DNavgvNznputpiiOVKAS8h8kyYgEfKugY6ImE7mB0D5TxHrNxKooVK8qYlOognQS35C9AQnx6xWbSwtOmqVpGdTGwdD3nJ2+qrQ+JMlqJO5QiM6dX6+CFVMwDPcVODXLcK0XLbIOp4ZLhoXCRMUbowGrv7Ve4V/XHOWGfD0fk+YRdhVKLhqRVB6x0EzK+vzcroTQy22lTHnJ9/BnYtzjN/5u6uKW8P5uGdHUk6iTPSzqmcIbW2TlAOpCbNcDpjZu/Wmi5CPJUmtlF+WkcTW1LRhrX2I5PMEVgbTNm1k77RjJj3OSG2tCk5jq/PMFvj1HRtRgJSBBlxOn2o8czJaTfR0WYObNxDdN9bzjtD/qbBlW0qZgguC7rZwLUoihG/34px4v+GQ8rnuqcgrFxr9QfFe1PdTSLGdLZxw/MoEBPTZCHwVg6jxFtJ3vp21VNeWA2aGyrJMQnAHliMolfVPlXd6C2VvGOQGujRwujxOluN3amXJ8dncbAge1Q7SF10tNLqk1wK/Tcgz72ls74KFqs1fyHINveEYTZOYXFlcpM/zfshdXVwkrB+JSvk8kc5frT4HGS90iRceh2+edSt1+nbDvFWuRzz937QS0vAl0FhMUJCSXg4IiGOaTr4amy/b243AwhchsC13Xhkzfr8/8a4aF5iPJpAI5nz6F8WSxOQCqCGUHrktlhKcwZAlXw7gRAMOkJsOYH/YLZeQIj9mgcuD8Ya3Zr09ePE8oBClmIJj7H1ZyLGAEC/niaB+R2mwkv6lEQUMFX++2Z0uDr5CB68uxtuEEoKU22YSHmAGZCeeR9sZ5JLiNbiaIr0vusgtpRE7X4U1BjLA8P1I+BJcaav8bNWQpi2CucITkw2tBiD9PIc0LnXY2oG6qXL1As1J620jmcaJNX++GFlB2KK46WjSG3S8/A7+fd8u2i2PNZH4xrE6ecWx14HE6dTxbXjm3FBpA3C/XTQEkppGAYumSzpwPOhlnb72l9w7SHPMyO2FcoxUs86t7aMD9qC9GHsxQ4oX8tW+7PINeK3Kvla3X5Wb4sx3fmE4zwZ53/2WMPtGHX7569/oSTYG/Cu7pvZ0nM8c/SeulWZyCz7vd8wH0z/Wi0G+7LWl7ffpexoy52YazfyHAHHqH1/fnBPQ6iDPOMJefgvtQcWbm9RY3x6rveyl/mNkbLMwqJ5+dTwlGvLZv75Rh1KKHDjmvspfF0DUEGQw2gm2FPrMTjz4DO97hU0StmX3kbjadKVeXcATRO3Y6L9h0fc+Qe9PSy7h2Ard7MDIzyq90ZyZZbKyt6/xhTkAgHDVDXnVTj4D7kRF8zeSKIRaADhKcVHU5mHXcOKX/qNqkchMvaoYHKdFCXRwOguYOm6IPdlmQ9VQiIpM/MXLgDB/xv/JiTeTbd9rEvQUKz3vn0r+9QsDORMceCocBsJse1z8oKZC2I/iC73QIpcB2QTh68Faj4y+3c4XeiytQ7qOJpKVIcLU+qSuOb59eN3tMXXan4lskToHG63zZ5o6oblvs/LM8UdSyIBpc99f9/z525H+K18yANUuqhQS7PBnYGlnwPtlxw8hXpWsjl9Pd7hCF4XWgUTM1iVzoBOW4/2C/hAZImX7z+J63rEBWXhGfFmoQiRRxwUZ4AeAY9y78mY31NMgZiy6+U5B2NxCntWlyCTZrazT9X4dnDIQle7aC0sSyoXmorKZN363bSkGOJIQuY5M+VNXQVPuyRaWPBfris/5Nen+HiBo+8waiE9sOk05OGK3i20KvfSTc8ZZKYz16rlHk4cJv5CQbocBXQGYP7m+usSRmPbypy+108Qs+BalGsTEYU1iMkYZ2K96Ykd9c7iS/9vf7Y0zz+79JfHpNghQ5aunE7YGnJYwgt9elbslOj5E1y80bdhhQAZk/0FC0l5ykjGak9dzUlHWllz/HqM8t4QVkE2XTXLQegrPeQDMGTxe1qvBBQThpvlxTJw0fBNl8+g/+gI5ZX1Kc932QOmy0LcjI1CbpJ4wy8HPingOGxHVoNG+GYNaZCOq1htz60axla7fsuBGV/4tgA8/LR/wHszAm8bTY2Iw1aAqrfgacjNYe7IBRwLigGnuezc8Y7nnOcaWU7BsejmbpHpy8pV8cg3nPC7yFLvtang1gc7t5eG6aM2vDTQ0HfC/b2hOmJRu9sx5Qt/P2EVaYJekI7gjEB3dpW3JwcKc8CGPULSrl2fBEtltScUElwvn+NrLK4vulZtuSVWm9o/iUEj1TT7SaRyopQfVTbCa8famsX4v5czT0sZjkdjWPFzaLJcIM5w29lF+QMKKCSsPGD5ro203mqVHGfqP5qz0MyEjXY5ZyE/+1m2O5wcOF/2uEoJ3tdwmik7riGbbk8KlMKUYuhiUeu52xO1Gg0KN5AFoaQu4R3u6uUrh9AJsEO0YrwTrp72giJ+u8O34Ltm5cNQemnURyAYMCvJIiFsGCaFFdTMdzmnXpk3LT9jH5d0TjW66UWels1dxYZJIjBBbQTXaJYrcG77ppthPurMEMPdxuZimRCcaX/4wCVh+pAhLg7EXo+v3XFWuTcbF8mxhzcRckWIEhO+6gYgwtBwuVvfOR/KFkm0fcT/dyA+vLi4+9f9R2L1FwpCSm6F66uFisV5CFKt8VNOOtmTVrClZtTxQlv86HnkvGQaJz0Nuc6Ldjs9YRIVdIMz6mUv3t/m1Z1F9J4pBNNmcu6S93kHmignZ70lBiauhjxU5kwb/Iq+soytG1xoHbWQ9A0QGLOce3aN03lSluyDDgvhA9vTxEzc/obz9pm0oex4ZVeGmvleetDPjTKno1QyS1tvE92WR3ClzpUrMKUM66OPqNMxl/d/gVqP2fFO6MHhXsggMJv6isy3q/x2eTC52QMwfGQgOW6A4ymsBD8RDiZha8G2M3wDy/e+IelzmBQ6svhUKvvHh7dYaC2uKuKH0In8dt48xVIDnCzamQTvOJPuoscnKJRsaElNVTmB9eYZltmlrL3NgtTi2JpzXhczs249536yQecf0pdkS5IQrmkvCTv9K45KbwRLndvq4XD1TUACibLJksBsNA6S2OnerSn1jmumn5BqJ+tMjvDZN7g/Dow7us2l7VQ8vP7+LnTJutkwkG9PoZolLCkIw1490rAyfG4M5KaYMEsV3UdnRkAvr32V21NsYwf9ikcs93s+j/HoIr4zS5J5eHlTFCb+VubQbMfSN0kG8HRZV/eQgc+MF91bvQaQP+kfkuFnuHdf41iaSDeo30i04X9wqJ6F6uYjuErtsr+W9h6FLh0Bb4QCoW2IO+KAycGWP/Tew8QYf7oHVtBCXP0cyPVKrjufwAaycgTZincBr9485A5sC6muWzEKeKZIlsV8JkM7yAutqTVLitdRhVgG5TPo975hIWFJEnqFVaCaKm/jtQn3Z8PEp2SE2OBHd3hOBEyULTYjrNk+Yv0nggvd9XCxLymnSmBRD/BFhyiKhZ5Lo8p8lY02/fFFi2yqxDsyJEBDDufECtcmCC2VF23Kj2xHE5aKM86tL7pxmJ6FXKyBRHBKRRHCOyOKaQeSNNkDDb5yvx3dnMQiidYROaW5TdArV7lWR3tzWiTf76a1BV9DhrKD1Oc8QcVTMKP+KARYrsChmMLhxQ0wYTZcwx4RCp3G+LPh3O3/GWj4Wg7g9gPVhP7Ux0oUkcEGRHfMjMok98rOoQIK6BfoF+ijzSopPpt7OwWqpWnedutPp3AmpGn3XnvE190piaWni4yfO/ZDY/0d8J5glLQiPkGrsGWVXZGE1xjSteSMtScW7LKEWQZz43mo/jGhekc/ftv6z6341MSZuy897YDd6ARAh9XgJeCMQ7xXcDLK/woSLCfN1e5dyxqUUWD8ctuQXbbxyFSZZ7FByOWecTR273ykLz0gPwYpLqLKCPerhSXQB3vOkx4Rtp3/4xaGbxrT115sLoKXRyfZDgaB9qHxQhNgD+n8z7ktpDkeJKoOk2mWYjJMjXtOr8TfTzsYZMsjM61LI8+EyBvIAxWTSMpJ6EgfkzQOLLGmGQ/5t7fr2BUMuv+nFLmuaBZiXninBFPdDZwOtzxuxhf86zq0y0KO1UjmzP8SI/61XzNAqC7f4WuxaWLiRVT/kOXl/y87diFyAGcot+osxPYicE1GcUfdwyH3p4efPMTqh7rGfkxXtaNuESlcapMCRIu7njsqrDUq0vjhHWqDocbTQ0w6QeNDlEzMk62WtmkVuGRQsVGlADhfszlf4SHjZQ8PzjNwVeKNa24UcSEbwAUbxHdyOtUdnGgYo9eKJ6R0M9DJWbJbuqixCF3cqmeLqNmCh1U3sGl++AuLg61wsAWTcEF5MqK0j8V2i1a6XW1f78IcO9ripFc2WeAmSc4SwbILJM3zOzOSa+IwJkywDSyNL5WtX8c91o9jcqozeBPD2ZWLT3amdtiLBbmiI9gpMLK5LdSVQ9uMWVRLAnu/AAG6Pgv5pm/er+omqjocVxOKvmbeb6GRD2Ddk9+OrmSSmtUPyjne1t+xjSu4mVEvw1IonvIX+kv4kmrwEBD6JVBaMfvqTjuT20xYQUESotwU2Rs/2pjiow3Z1HTmG3Ht0wPCFi5Ef42yRp8BnI7AAIxPEwK0cQ/io7BAohtHb6hkEyrltbYmy8Q+vRWTR2akKwsC7vc5EVW7wsFjTINblKKCNfzSNvH6ObE6LItPLM/mV70Bai9Es3zZ80tpu7Wlcwr5eZzRuTUFkN81gVJp7SaLrRikYS0n0jzQY4BchK7YvcJyCpR0U4pjKSHCYQ28dfMldvWZqdrJGkqW/OQR6JZu3WoWMeA9X05ellgANWcmMWNl8Bg56F9ZBvjY7cHPnwr6YOAySC94mFc4f0/tihc5KYvsHO1JzZvczCLh1UWIw5j8wCmlK3ZK7R9gm/daI9SeHPq8I6Hec25/UwiM+mnR/y71Cmw3XOUdNTiKd6bHYUcAsqNsILhX0iiiVQLnHruUm5ur/O+pU1ZrQFSm2k5tZFCgvB9ycMqIRE3+HEEYo8qPR035hWG/W1mnt1u+yXqKbISZhmNnxFoHlHwHpgEitUuVRk+AnE+3mto3Nb1oR1a2EFuWW4uc9N3hoilCSqaIqaWBpyEDTO5VSSIAvCcXNB2v1g3g2N9sUUI7O5YhYJZroUxase4/4z4/JhxIf1VH8hrdDMCCRvv4sjuoPBB9nk2orYcQhu9WuXY/LUvHqotpqJSVIxJxo8ZfjDwVHV+6Xn0Vm/1IhryuZrQRNUVlUtjTv8/9yl5IVXHGzZTd/3YI/KEfEr6nDG9ZpgNyYii54LBH+sABx97vEBHxq0Ppnxv9zgyLOYZcSchMssmlMFMudh5F9jX73P2NJeojT5c5l33FjhpP25GagJGjWXkv3Fg9LY73ckwxMbdbXhHztwKCsslLWhVlJvqH8jg0rUxgDqoLCcIgc7+i4nAZ81yXodsvW9s2C2EVg8bFI7R6ZoftCccqJDQmCkYeQD+iqj7kKwc+XyRu+tmFPtWlU3wbEeBxsQHIH6XEFhFYmKUW8s6eQxiSok8hTYNbfrR/qWGM6Qw7cSfcY9QG6U+xozHUKlkArwxufTEqvucIjceFX8CKhi+PT0aW1gnKAWz/CcpY8LXOfehbh1WQXbtqGlGVYvXbjoG6qMXrtVnNhdG3rxkYTAlQIH4ua9NJFxnQwTgyw5mg3FRf7Oo45cABxGuXdNdSI3QI6mV3APbNs3fn6K7mo7xNLUv/F/pVTwf28zpFoxEERtiNzgteoJKzHgrX4+Xq8I5Q1F69WsabFwNT6XZqqlxv6oemK9Pg5sER+Oq6z6KCIb5aqOX1NdV/y0JIRwdWlzPTO+Msi3VSQhGrvsVdGoYAmuQh/JWUdNk37B6Bi8BTWBTid+3Sm8Y4U6XJn1XHtZmPOdySUP3jkEV57HPrINsAYN0bgwTNERE4LKM7hyVs0R8tSQwE2OWuOeAxzm09YeYw6i224tdpsjMzv8jUkT1KzA8LChjLMxY3w0V1qkLpxQ1VRkgHd84KniHv66jqDJVNaT0g5wSBzLZklieh2ichfThwfluCtfKpFAyOFUkUNGlfiolFUwOofz7ALtAjIMedJiqESPh3Ptk0t5IosGfVefUcF0GV/ACagm9jzJit4SFI5knK80G2pfq0J5cxZB/4/MJMApoekbkPAOS/dY2m1uEWvMyqr/7so9o5PsKI7F1v2py4NrRUMXUVMEheerpm9NSdK0nA1nEMMreyji6n3gRMLiC5P3lrbwsIkiBjNV6C1YVg+t0/mDi8DLrd+KUs093/50Jr4+dHl/x/EHY6qEdMLc+J4+Ug1E1/Geu5jbGdkN4ES7f1JQZxksQMATmAETQNht4SjssqLvd8HEkc2RlKuyG9V+tO2zVCmkqH+oCMYx2+KuyJor0iDpfIaU1KcnyEgCzKIPm4Z4+qrIIPDGk2SXmuc5v4b+ucmTz2hfgLHkwl2YEIghiVNoT+a3zK0Go7Ks98MBt54aGsktHdMQcMk3TlO84NdUNfF6A2U61ewfIJfOx93LGoi7QkVcT1jXNbzmePxwD9ofpgugYyIxnd1ZXm40iuN0bNdy+XbfdKXTuY01wUR2eOhhumGlUdztaGwfDnbV3VmePVLcEX/29g3JkmwAm2LyCV+13VrVBX0+aXubdfI/2lIo36QH69+tuKxUylwG0BSe78cz4T1fvW97fQSp7BmAV3XXc+yOQByqvgtGmotAJ1klsi/+CPkscKqFs1Q9wswyMsSC4CKSlWvKXWumogzaEHqZKBDfiRJ41oJgWWASUn1t4qAH73sNJcISI/+y9syp0YH6t2CKQWgISxDLWIBFaJiQl3j+QX+E4GjiqOVZxq1TO7kAcqQ7/tORmfqEzrVmD8OeISR+aiO20VurBEb1Z4c+ePikHK6EuTNVTJ0d/ZReFVO0Y5YLgWJzad4abW4E6vrINf0QfDO0zFvncTN0ZmVepr7nLniGCpJG9dCOojWtzCa2NjDg3UYu26fVntLJ7K1dxUJx5MvtYdjcwIPcDKy5V/t9UUThos+bd7jNUmwMh8q3tmjTdrJRNOy4rGpQq/bWqQAT1k3j/8lyQn+TU7Xc6NDMnso8wms5qte7IfzZU8XA1XR4o4iIIEcvbQnGMM2yCnjbn6TCtMezWJPE1m1zWEzUZJJZzH936vP9d0r8xmo5NzIMqN4eNs8x5G2T/uzwWeBwgpVElDJI/1fzSdUmNqqXzmIGXatdj8R/ahOrW4uu8clgdUi4dgjqK2zrov1WfKSJhCyb1z1cxi+8x61SDlrShB3cmdVRpYvvKu33+miU3AuvHy8pSQZsVOet7xZFxJeY47rt7M6oChBbDyLdK760ZeMPweXtCLNlV3RmLy7vku+P0B/xJgePYH85NE/MvtTHnFDSSPdbPl/FrB9oEllUOs51/jVR215iHAQW1z9o28KmJjmba4nVKiJBFOQ/V8l7URdnI6UuethLXCq2z3ZuacCPCW1qy6dLZ/Bp01Ei30HQGmGD2v8wzfejuGAnulerM5zHG+HeqpUafXB4j46qq6GUV5m4AVnTzjLx1rO67RvFNnim0+sFXChFMJE6OpudF6v3stWkBFKWekGSX0Wxf5Axm3QLZ6M8gANnGWBaC+gvPpi0QkaaZD5CrZquGvfn8h9Y6syglLaHvJW4U9xRgeUdsHDLhOf+anjz9+PDuU/kYcV6TAQD62BAouflxxWfW9dFNhb0hS4Y61j92q1n/kpEaK5rfz2YTl5ruWEDeR59gYrk+ym1biw4kLyKcZsFmgxMFGUbq5HRdw0htp/w8erlbHnmRXukMUDFmuBgm740xeb7LS6W2KiowgmifBCNzfZ+mjljNxeUcCFSw4er5ePtzkKF2jiCmhaqQ0p5/VtoDK7RdFrm6u0KKjRDf8p6u5OrKX07c8afte6yDlw3Ha5rMapf0scUhxgHXNtNIVX89JEOg149guLoBXmWFAZOlbxDDezozeca7qezu3kmVsSCD30pEW2NcVQfd/KDSJl9kPBlKRsEADOPXC5juZBeSpDvTLyVtt204QtXVYV/1G35P83X2OMoEthhGeGtL6QzHG4kgParjPKli1kuF+rhX7Aszmzlaks31seg+TCZrueb4VbQwg06OcMTYOtrz44OgcriUax4Tde+dF8kiNDNOReZhJdfj2EMc4FaZZEqjupSZs/aIyiWBVNF4SHqwu7XTV7feS7vGKu+OO463Iu7ZepDZrDjR+GQT3pCVRlI266cOJFgq67pv9OCBE0nW9aF4kqi0jkj3b4ZjHCe454sRcWq2034Gw1vtQQ32Wajpzp6D7KPW6jHS4VFuQFDAx4ImBbS75FtE4Uv4puGvYmhQePsOSNU/MlV/YD68VF8tW+ED3AdFx2i4JVggFr5xgM194UmkRZET2bdroLKjsTie2oI5smdhLucR9D6lLLsNPmSYjFqrn8hQxIn66uO/b3g5C/sv91iBBjvF10lfu4Qhd+F4F3/nWynOzp3i7yjejv4kBzxsktMFD+VXAJLqgAeA8JdUvzUlizMoD68lE3tGuhCVRggL4N8W8d3JUSHLfNxXE72YmHPvXnT6XwDRsO4NKurN0ukKmc18eS3TuVr93XQx+7zM7pFW6noxhclo6FVnnBu39oCYMZnOXQC8f1w8CIcxkSiQCyU8HPBPznhXjAbVd5mQhd1emmXFsBVEc2DkRb1LJFhZU1jQRag+gXgD2W8jM0mQ/Zt4WmN8zHTYt+G4k+5Xbi7kVtd3UxZJiC4RJYuWMBftcQGrFNFiiCz3VehTAZWbDB+evmqdqbhEVyoFOXu5Prq/qiimH4unjbJwwBOLqdNoiOauSg8vrm5flujrxYGSzRcqBcHT4mxWEFcrVQbwqRdrMgOjuTpOJmRV5tZu1pRLQbhzlXeWSmcWxQOQQUl+5k8eUdY3YAUju6+7sBazwewHP8Atnojolt4IBHwbeG/KCplEJtcoB8grjMsehY1XMXfK3E1HEbKJNOwnhHimEp6D2ZI/DbU+pAF2bCXeAnCIphdh4tDtRxXwRoGur1+FRys79UHPDPIRGcxr/bLjq0AewnUx3mXwslDzzJ7XXObZBihExRWRZl3jJlWHNhf0mGd5SvX4lrZulpfXvQVDgIk6Do7wVlHiCEWC9EzgDcOEFS/hsA6Cxz/fN6aOuZevpjqzrlV5jypJiKoInLXhnFdx8UOqnxRdPQVXvkPe2qZjR17+ZyqmcDoZP8Oo1Y4Lo1Cqvml5Ku8AfPpbqrV/+X4DVUETFAJhHtmdTG1V7wP4WW423c5pxW2/t4qv73WJDN/JpT3waQ5HBacmE8abdpLfIUMZp3krAKRUnpfXE/RB3pEW0u4ClQmA1vvEUy8pCHrwjY6JyyUjJsmC/g4vqUuuc3WvSsF8LfVVD2y5M0u343/noPlbLvPFHnPWxroOGSgqkyxjpTkOJEA73aXDx4NrpLer+BzDcnOX30i0aPffOROp6U8/8wBKrzFXrmfflXZLtZuzRXDkiq2RLz5LvlHA2R4IBCZLf6RlvcowXv5K0LNJ3BhVO+7KIkJ1kEvWJyFeD3cYRp+E8W46nX1uA/yQb4iwuIydlhwN2a8QJZPbfRj1uciz8Gw6p+ddc1NjbStilPS/IMifDJfO2Aohf3dQw5VgpuDClQebVQtY+AkqoOgjO/FzGYwHYdnHmkhFLzx0Wr0+XCOanuOne0CleWKagLtrDjo2la4tjOH5+vLKrDZgzZTUjtyxhOsaOwJaAoqUJ9tmvLeNiyxcgmsfzo2NIwZYI+u+VgwLAajNrnrshfYKe8EXKjMajl9/ZAv1MDnTVKEIneh+3AfuEhnlya2dHPY3rI9KWnVqsab9T92ioNDPR/RgphNkY/wwixL4naMUV1nGbwJRK8vZNhBZcei+BQG0Qgy0ERF6L3H8Cul5s7ZdNrggg9qSjmNvhnKISvq+sVJeeIQcM+JbQcnvMHWm2sGe1QWIEFgd8NJZSVbjefvdJBjB+n+KSV1SuBYBrH4vUzUgu4OHdPU8mT3U7jXMvMvCDAXG4TNdEm6XdDPTK4DACwusmT53hHMZxAz/Gl2S9bBbcarURg0Nlbup3VrpJWZVC9GQ+z763gVijkf8uLGxOMB/n+PA3eQ0hZz6P1FAdotxJqyKasYfb+L8J3xbAOZbwfuBoCDxrA3tBpjBspnOcCH3B1jglhnioXQpsPCGm877fGMWiPLUgkP9bKjYrMxLzilJNLIW4O3VaGJ2oe6LKVoOG1ff2B4DVtIlqIBIEMrzH4XoFA0DEPwGG6WdF5Ab8sGPyP9V7vdZmdbItcSms9hwLyl3S5kJJZi6JY8GCLJB8hhTWM3RUTy/y36xFW6q0Uq65AOj9VsfdJVEKGRBLDb1umla7DcDYpxdMegctOC95n2+FEmbXqTDshSdZP021TbnUNB2R+qkt0m9FWb+acGO7ZJqV9/MlCAiuAx4pnbg7WdG2z96yxdfihVnJAL59xVXd513g3DdtcEyXUr0ags2sJmdn1swGldrccRUsTF7hQqLUS5sKfHclC6URAu/Va06p1zYT1GmTRD0kvCv6c0VoxvdsyhmyNvMxvCCAJb+Eqfi4m85xDK8KZgNiwVK3JegYdmFDxzQAvOOvcZWVCA1UnGiAtLJgGycgLm6TpNIh3ZrLWXSuYwftiSDdFSyOTyKPlKrm/x/kPqgqQEsWWquptVbCBpnEPTrjk/b6QhnlnJkSQtua/DbWHXhhuJ3wG7BUTAlJ5/lnrAWpuLGb05dwD4ZswFJ6a3ZzQkP5iixKeqgbG6AkuKtI81pM2c56QN52uVOP+XsDpiu4kdNb97LnZf9xN/EzgrdP0B0+hjopgDtoechiBokI2jRAFxqcPmG9csWysC1QTXf4wPGoE2IIwodIF2gSxbcnIV1Xbu0Yuz26BLCTfmtWoMVU1QaYGZUILnF6C2//0ZeKvRLjnOm2Ccn/aDseDFUazbsL5B2UWXFM4N5rJvhWJL9IymOvVlvMAKyIUoKOFmebqPd0kyQ9c5UTGNucbt9Tqosd6zAjfA8EDgYdJH4d+n2wOyXRtL1NuH0rqnMKSwVp0/FAUkiIOv+YJ13esZuonoQZD020oUJ8gELCMwLzmySjnuPhsfPukDjLnWBbiLRxbgG0lY53JYF+5cUEBV/XufL5qoHPL/x35If3rILag4rKcXN0u4n+aUuZ+zslWwTX24SSAsViGKIB+vL6Ew9d7uLr7m/60+egmzORji+rDYCfHSzexamFN3Yy2PrTGZXqXwMN8NFgHUiivbuModt4KwIacpgBsuX2Zm8KRATbRyYP7z+dfrqLh/10wbbGs4YRpo3jbOjn3QyY0rfI7L2DcUo99dvQlM/SQFrNyP9utGoHK+u6K0GKjYxBIR064XXcpRd2VodmV143UF/zolrFlxUe96kvkFHPlR+IABP1wKEg/GuEgQZ5m5rC+/nEHnYfxCsIyxro0OhpPIn0HTte5R0tW8Odgmcb/mNyKkb007e9CRCl55UzoUshWvbuehJRfl9ziKvoL/cCbBlxQ6T+Q2s/4hYEtceEXtDiatpdTq1l0+WJywX+Hc8nyh9DhivM/XqsIxSWvi5fk9swBIgDVKR79UOGHX9XO0IdD7vWrsZ2j4UOP7Ck9BfBGiFjybX4lx3CWYMcAKc4xmm543cBIAGuRkC418SVrpqBVFHNd0IEB8dPyNazDU4BTmEXWaa1+56/GqC528zGfR5GMCbL99Q5yTT2cwYzuKE2SF8Pnise1PlD4EkKdd2dVvuvFmVlesCQc3UH+iTQLfPek+AXx3+XP6spb6NCah1BUWpKYrVhejzqw2Xe8b6O8ORY+nRVyqIdtlT6RWI8QKoM3hNd46It7MtHVlE42iGbMPS7owwXIftQIExXt5wTWhuoUWm0S8mvkiS+YGZF7Du9OvKP8h5UScDMVjfAl6H01DzveeUZnhdYnEfdpBg6MBuhI6fHZr2iNxO3+MvnCCl/F4lkn10cKFi44ViDy2cnai0MtpUpk+cukjCY3fEnRV6wwA7Q9xa/mpS7mY2w30jDeQCul/qa9yOZJPyZsLkOM7Ncbj+kDSieGqN9WzsiljRA3Lnt5l+J1OIg3lmqCYDjfiWHUHKJupqbXLkjq28VVYjXlufzP+s9iHPATI3071zgXH0nqCC9TvfxnaC4ciUOYJ/67WNlCjt7lrLBTVH2lKOFSgPh5A+kiDIy42encMtXfTtvaH8zpEjSPWdhN7JJKqJHtWhBTK05fxKJr42KS7gvVS2wbanZ072p7nnFCm+/fELPGd9+NkAwxGGfhEWDYxHEP92cyBAE/nGDT4vElyF33WzkPR5/1gfs4bNnIpyioTohueDalV7LZ7XdruiTNJTKG9Dih6o65JYxhpj6P81QIv2hwqifEc2w67Ir5SZ9ybGuETH4yTxmaC/CrCBonKKnqpzuZme/eUbNkqbKsHjljxMksSJoBnN/MFIa+10wgwsodMjQWfw7q5e8ZuRxkRGk5U/va3AopRNtxTtDviYna4ed3gYf+eEpudgRcfknWp6en2UCVqWXTRTEOzWYslxhtL3WnwbGsAdo2O3NwMzq4mtCNhDzz8Z1KbVrHXEYV2SWXvwXtTAcFHCUIpPjvHImUC27z4tj2ryPqd4RDbyskUR/c9nHtiP+r/EQH64C85gi4kTtcGyCTuX1HAPl2DI/nNtTFHyvyZxTA+2TYYTbD7fuY4NU/eEz1zea+QSJU//egTrgq2vP1UYvP7hZy5xI3SedHxuevra5UI26VUHHsES2S4QEy+skGaWlYfq7sLmKbXfFGU1veruGUBkBTFRARt2qb6NNrv0OFrQv3fFHPgkRVNUX34dVUavO5QBfD240/xu5IJjSHMwmQdeST8cgoUMdRAz/FucSCthDsaaj6vhFFSrA61sxIR4+Ta3RnB9zsQP2VeXKrSMfjR8eafgr5tHMyg2xbMVnA/Hy51G4vS5doV1dUSsYi0YgMEj6QXF7i+tSnSm3gmIiwHgO3SxLtn8tDTJDGG/UT8Oqs3dfTiJTsU4fzbCUTjRoY/yfrhm4WKupyhBmLPreMr+INLQVIvPa/stHa+MDY7pAv8Y7jvfdfbZ14lOepB3P9MomMFGFSlFUKXw8Ug4jGp9yZnYp1AR0AuSmw9sdVVL+JWgbfvNJP2zNyKIyu2pe5YI7gY4HkORpwrj09K7+AFKmS2yx2NOPcDEXJzgth3RJq4RQeNfgzEe79q7ehk2I30Ir/PgZEISqDWajC9TdVZKZwLpNa9xIwwhRG+4OGHXxPo4rUTujXxJezs0f4JsaVLSNdoi3H36XqXFJBPX8ye0ezLEVv679KwFOBE+J5QBCR3l4lrSHN1M+C83nIe+9AyfY+hDQJzPepynojgtWsEX5XvrMxS0pVilYcrfHGm9EvKo0Px1HMcxCyzM6NYI0pCVy+eIgFpt8P6ntc0J4/7jvX9ZvKO0xfVzHLsZr8t8twmgraRiiix3jg1AdKrjKgmTE8/7ZC0OUtAv62RPPTHTbbK4wP2+SOElTdKw1Hatv57bgcRpX45eTxXOit8IDuNeusGz8NHf5yED3JTFt+9qF5xqC1vbNJnY5CB+wBDzddUBaaRvCzswysHrrZlkBe/fYmtVbFEDDiAVP0mZyWAF6oIaOC+wyr/oE3ZcfqkcjsUar5Pna8H1Do8f9E3h7YxtT/kEdep6XLeY2jC+uKAl+1s0brRUoNNAVYrCnTcRojTN142Juow/93jdLCo0iWFEXBHAu07swcqXEPeeGtJGs2SjSncwa1TmKqTKvT6iRqCD9WpCJrxuwQAIgoq2dDq9GaFUr5/WpBVn66LzwhMOOReUE2+GTAZZAatWRllU1gFgfwbqMHFG4Ybcv7N/rnA001nf5s0NHOtfp0SnV09e65/6OKahbutYr3Kpzch3aB0DS8wSJuWwzxfFwfNhVihjV/33etJ36L2FhmZhOP3dSf5FNH7YheqlNxEmhRE7OSBGZc8dbDZECJZjRwhsv8aVIz23fqIgW7uThV6XpjVcF0f1HUM8pORXEC0hTpbjGumP/PJNm0QBHy6xfDAUNYD30S64JcUdCXjIFcPAp9GrlMvZCzQ3FxHHCGMYcSQ/Ttrbzz8woOMYpxM/H/QzZ/aOYBMSiQHJF2Mis45mNI7VW+6qbSvM/996A6ZKYkfPA5uvU7b6pt7hthUKAYQ/YkeqTswew5cIaP5xQ8fBwsYRIOheJ22mWf0qKLLwdQt6V2Gid5+BiRrnlBfuqm7MB15hvRfStYk0Z1d6BftVsRz+clb6JAMJPYH5dnTAPnppHrMZyIzRGFJFOZSN3iRqaHYaS2nhkLbp0jrZudaHJoUAQwNrsPx9mxAAaMa2t7npbz774W+DyH/aGlkqSM7dobgMTxV4KNn7GdB9UA06X/WO3CwA/OO+XBjS0TcHGLsNBZBzNn1Ja6tZd0Bdpgm3IN5dOtxFHoJlVKPTGkloztXjI5dFQkvrU9umtrznVrqpwu2UPhdqQxLl5dkMjWxEi6S6+Xyaav9YBo3U486bZ8e+051MlPDFCIjQqu3ylA6e2Kg6LDPO9dbq5D5CDNJ8RUoudMJQf3H/jahdHku7HZ9CacoFneE9qqTRHhN/HrviBuhvCbn1+gZ35AZYWkP3U1zvJvgli1SThh8++nvS4Nc0yMV64IhbVbXQdQRwfNdJ5DpyZZhAELIEbn23vXAohzijMBdPgAuhHRa2VwkYQ+Zi9AaT/eFml3nLwjqFvq46K5a8jzvPjBeIsq2PesYCJhJqsriLi/mcaWNReoTKwG30/kiR34Z5USn5N5r0H0Y7iCPardUY5of5zylH+xEVqcATu6rA61ky6d4q8S7uKByUSDMaBAI5mdjmNtVrOfie/Apq+YuSakZISExpSnDRrH5Js4AopS/N0OpmbvUOIebQgIgmo+dsc0jwR9lqeaKH5DNqEQTl3N85ElVOBPUuaBOfPVSTj6V6xLoBzj4X5xZL4QSuJ4NHc4gxBeW2PFEb1H+3/cxbWVnW8Bb+pmkPxqEpoW38vO/5v1yH4UPZn9WXFp6lRhoIyFbSkhzatCDm8+fWIBgeukNrjCzKnEzNQU/p4gQBWCPYkawAPzHI06UMhzR8j0a+Vl5XpZV7tLqVcGoMWWG5fP5lTjcPRr8+mLwKejtc1msnu/w5cDIGxgdggeL9n4sL5IDO63Ej4Il50cKBfoSRcvThx7ObAQPD3w0SCWbbvB80PeAKdvc0gB5hVqNgrAUbP4k8LH2CkgTGWGB11MefuYy5dJk2jPcmUr/jDQP+Q1qHo+DIvVrOD0+GB/NAak1jtcmBp2NEKZ9PQEO0PKjxOUVWBpTVBKo4PX7dLWH2gnPDhK0S5EC90BZDgQYar/oLr74dzk8eaZoTFKJEkRUE0FqGvJaVqqMUHpy/UedWhdUJIwqt93vQaYyDeV1dDzLCMakMRohhk3eahVg4p1dpG650cf0ZZV2DIVBvEVN5NcNLB9Qrryq7eQVMAYTy9ThdUcB614JGoSslEnQVo7QsZ9Il17Ung/ohQOCfFgv3DsF1XF9AGlmyDnET01bLJnhdR/e63MDOZyzoCe4pBNAbJp5/UsPO4Da+TR1wPSP8bOQO2bE6qiHNydko2/NuaPKoLt94YcKA/AjMq1eEo1ovCAcoc0L248eePQgYYceTRdI0UXSP3/lP8CCpHDSebheXMF4pVI/zdDPCz31f1VwqxlfGr4nq6V2B5bXC84etDoouQkR8Z8KtPZl0Oxpu67XpSZ1HxtBYmNvkn4sT1btEALt4FnyMWdMF8eqG/0rnSFiYLPOgurXAxx3iT05YeTccB3Zz//vbIbPY6fm1BRyEQg8OZlKwTQ83vl29Pf0w72H8UZVvTYsIxhwKO3+llERTvhxahxEppujgRQdavL5IKSSBI5QSqOUPmBk1xePOtzu6Ej0jDHDTvRAHzBYWaHazKzvVofOnPCUC5B1m/PoGYE2nryUzznzxzBp3EUvOkw5UABVJdaLiRXzPwlLab/8dWmzda17Q6gxX+VbZKcFexeHOZ6EOJV9PHXHdP+oVEbD7lKLN6TQn4WvJ6VwU8cmFJBsvdIB4qTTeOBd6fymfawkdFRvxHo+md3GM4l4DO0EXPOxKGA5If0CKXgS5Yw5ilh/5NcAnXWJkILdr5iY4w8oJuQYuxQFyMNDMglXKKWsvmxzH23bycXUG9k4GiDRlqX6IkrP+ZZOHW2dJvxH/vxV3pU8e+DI1lL5WVElEEk6yJ3rq6rpbX84ouGcYWexACYubH4pmZiRTytryHNmFbViGFQY8d1yJKYv8nDlQVXlvSqdlQps93bNnTP+q2WX0wuhr/TTZ3MIeUAVwT//MigeTbhhN0EXNf9aWlC3JB6m7/QtLY4OKLgeTGbk+foAj0D5010/e0RZ+yHPjkKpFCsRlYilwAqh4o9HM1zbZhlbWNjcgp7vOCvru6+B0OQ/c0Nd5nRqax1czlRybbe7iCt+t5vcJLyecR8YXlhRTyiUI6LTIYtnRwmH+nWLJC2zvQVJJBRaK3moAi7aH6zENh8XnHpafrU0gpchW477cKYlMgZ3HG0PNO4DOir7HK+cMQpMS2+ZYCuGKul1Ydw2xzBU1XVOMAgwt4F49xOGCvCLOz2o2pIoeZVwoKDsAOLhgiJvzBo1jGf2vI4vrq/6BIYwIXMPE2Hk6P9ReDOpoE/itRtZhkrPqgn8w9eTR26iaUxpoD4R8tatkB6YZXPrzHddwgQewGsB4Ih73DdEqVzFf22K6cTIhMPaiLzBwHOBCwmXJ2biN4tUT6QotqsuVWdQi3CT8LL/3cxWnQbJhicywRqfkhOZG2ygsQx6i+niZetVjKYCkaJ8FeNHdOm3S6uWcuJK4PRTYXlFAiPOUSSxlYIcX5fVxhOMqDIPfFmPJpt0s38QMGNyEf7WNvWrlMQsNhSMf1cTKnnHmlpGXPLCUFTJrIaSJIv9wYRd1hOfXhRx0T7owt/2KotdtzsWq89R/Vyag49JFGhVs1GR6jKgLOV5IxqrrKscPvjh3R3TZyU79Dojy2rexNN3VOwdZbRCc871sMLDO25HvXtZKqZNG86jXux9oz4uZvAKCNRFnIFxW9pwFxIPJt+unZbNXaKM+q1Qdsgl+tYUiI7HOWOWIAj3TOm5j136ofYQwwrwoniVu1lny1wJT/6Bly8tlTMKVqmR4fbaV5+B3xHFQXqJTGyWRlHHZzmbGZcJpa3OmzKViL9t+lqoOOVKsHvBWVH1gCR3DxE2OWaLVPoqiUMFPo7YOGeVS5HXRv6goD8239Rfbcc9MDitHB2XH38m9fmMMmonHAmofZBN14Tzx8TZpRh1GGb/+UbHHnxxRZm+usW0EVyPN8A4p2nA3nIZVKYCfSJZcp1kny6wDGpE2u11RvxAg2wufxD8FFo+EWwtruk2Xclhjm0rljijLbIpKoJ9m37ThGR00pR7Jmwyfc/erD3R2DBZBo1RXuAZRA/M7dikdt7WjXiuW3LcPEwjNIhu9q9+XHJ3tTrL2Fbf68I4Tf5Xp3rDBy3Rw4/FL8gUnz8IF6i4vectWh47vDEbL8hqQewkPZHBy6fDdv9o+ITjIQ2tfcRI/7TkugtRsyeGs/wMzLVVglXlErR8GHsVEZ/vpWVblBdTcwKmcSeGNPduToemavBsNxOHzi+uuUjx0eldeA4JOpSLpvfcIbA4sRYsg/b9rLlpsOPdy8yq1LbcPHJ/9KrUzbK65gQEZHXfbtcndSIbX5rRcu/JKU7knYMO/qfrJEox37fqnMg6RjWIADlBky3UZTMtZLSMnKK9jwOxAy9VkMcNhKg8t+uszI5CDtXWFbXoDjA0sx0OM/tyEAnWbXl89UBmvPM4Gj9/98/ao8dIuDzg1UsqLtpqHw/KUHJzHd/R0l7M3Ndg7KXDvFCdWiaKpQ8fGSJvBwCGsreTa97D9dejuhQtrYQtxjiq4ImjaDZt43CX2Xa7PDvXTrXKdsm//k4heaqeCRbI2Szf73sz5gS1YKOGQ0zd5d5FAxxoggNiVrmo91hgd1bQiYPVCYMRntVos1FlxVEDBoakVYNj6iHCUFACcNBjAdIOLHdhzEQdc5OITOA1FteQGQiOHLq2fpZxmpZshv+41X59rtvSnVETzbOmIUAloOx+WIdwyIOXKwCC74PnU/jLcFa4hup7QeUV3oARluWpfAiSzfZeTUIPpes7yF8MPd7fKNgVEKClJffDfEAXFrsJkVbhDlDj/vjQqhA8NJst/99lQTlTzvpWjCzdGsTKoYRsxzdMbem19MDGbGqIgKiiVbTDyL/HDSuUnuyKwvtVT9/CDG5iGYZ6s8o0YjjAWSlSwUnKCH/zmOnnfn/Hzw2ZwEFZpelHo0UERgZ+x2ls+6VF5u7leHjHIRjPEZeYzsusce8vXN+9gKsAKJY4crsOYuXeSABCwzEUSRzKA2un4Num81lIhjptbzx/Kcz279IwIyUJbRLdLL+zrZpHcEiSbtbF5dSJrAoR+0lziPkzIbpdpxgDYg7ekfeII4kVzgSwtzkPakmLMs6L/hilrJz+GTWurAyDLw/DOR9zqLMFZsjwUbpCnKOPZP9pQ7ZCW46F0SzN7bh610YqLwQ00wywwmUBVIu/N1gozeb0ttiRW+isxbl4xhtmhxGhJFfg311dcc5ncXl3b9vpWpocTIqOaA1TsGCmjkhIrf3GP6PKizfxA/dIGwyD/NzCgNcDkQf+qtlnSJXvMrw0ZKlOK/YhOpSK0KoTyEjznx3/B8BU+OxkTcYxGOL0mEIB7IeXkFLeeWwFR4A3Avzt7toVvuhF8/EcD6/16esnENG8+9XUmgfFK1sPs/RKZu2WTbfKYLSdtXoeP1Fp5s7jeaZ0YEOA9LihWK45CnXy10u1yDH1AtiMhGwMNsflLAoAThDr4gnQTW70tXwJTOvVaUW4YcUuibP2UrqT/ZqbAPnbeUy9StkLBrpC4ii7bOmTFBR5yjqjR658l3i9e4wwBAPksaOpDDym4zY77byPjTkbSQowUyDLb27DL34LrX4Ds4fGgfHKmFH6gPSc4fj3/QmRUarpSj6DoSW+lesxc3KkKOa0/9doWeTG8SSAr0Cs5H/5cxfeh38MlpUKsrxu+LUppynurlzR9hlmiI3Esd6oUgvdIPM8L+7JmbAwd8XjiygKOvHoMtEFQPhOwRP5ZK0d1iF2fpsXRpE17PN27UDRWJEqmvA6aQgDvIXFjDJA2rmIMXS8/47lN/S2Jdh5hO8NvoVJK/mL7NAKJns0O77L/KANR/NrijWESBrCHaZuX6goelhZT1vyA5VcBczq87sXH2XIPwkR1epbHR+WvjzitBohbO8zFVJBEW25uXxCqeuVmVcyQUU7vMPGF0KlQqoJN/6kY6HNtkQrvhLUmxvuuI81VXzluitZlAJuZfgwyfy1LVJCihHaYVBztNBGq7DakLGtN93mqrc3va1MOtt7p76b814DIL+5EkZW/EPKOJzmBpgxL3cutPV4w4eQs52chicanxUQ62p1CSaXUo5tbbc/LOMuvKYh2R0PtTjXi1maL27ZdZAbYTD9xwKm81uQ3q++Dzg8Mod/6m8MvNqRleXG6aVPiVal6W/WwKnPCqFXgtqoGg4gLQMe6E+6Ed9oU5uCwn39kF0gpBazn/k2ahrdSXG1or7mCkbvXk6/e3SitFngWT1g+jf3qJ+4MLWMdfLU1qqN3DMnB2bZvfAuQqJrKCLPy6XjjpPmMCkeNIw26lSZHrJZ1ZMmj2k6U/EqHYAa4nSFOGS99LTPhGYt9HK1k5FW8R63S3EqC3+p1YC7oxixrSOhckKmQUJd8BYOfYP2sB2ISSXhRlgMkpQJgBWbXUlEVjxrFmCJQTEU9P3BaCcSkpvUpxqtlpdPSyjEFadvX8FA4stKcGlGGqd5yhdQ81252iX5YC+cP+f8OP1pH+JRYN70oICGZACizB9cxsS5rb+nZMTnZ7c0QK41OFAeaY7aq8/EB0vnkoaJ+zCcmmky8Xr4Ya3TLPZOuC76GwS8xkR4gKgnqUoWMvBM/jy9SUfePXSHCjcLIlVRfpXABSYVYaDW/GeK0Aori4YfGbQT4Lc/pV74kLhrSdlSS1ZpdUeQIxUs1n489p78hOZgDfhwuFHskppGrH4YKlm4XvIHOf6WtPLLsevdSvYqaA2AmDHwAPP7boi44TvRpmDnYSeAJZwVlbG3iVzFQOFh6+GDZ4BYeP/a6aAOMgKoAy4t7pw8hnmmJFqDdyKr9uznRegJ0MzQo6sdfByzyIJ4tktTmnR+R7/LWRv+ABqCoorXcT1Rud6W+5AA6lhje1auE/ryYMKJaVQX3ycLG7NTClhKmM61vJtz9nGdx4abr9IJmVUvItdvNH6FC5WO5y1VsE0IUz14lOcMkwRM0gIxNO9Bu5apG6Rxekb0kx25oISdVIcVrbAtEZx0Ca1JTmAVD0FdppUIP7kDeEufwTg4fp8b0cLt9SNXW9AkIAr/lWcJ6LPrQiB6uQsaGBPLlZoOxmuZXgkQ6Sl0yV+DDlLcwDz2/9DeQ+Lb0lWs2LKVyezSIT+p4gpT5+b3fWlNdngiY1ANjYVAiS4cygktXWdF9leh/74VV1+zvZC985mNBV5ReG6fDXeAMO7czp/p0I2Zk8FuHOnugj3w5YwHjp5ibXvvdF3BjX8GC2wwL8/McBlxwn+eAoZoTFutiX7hcTaf/bnDw1NuM+E+uayRqERbp5FvUm4LiMqC03IYznYKLTJ9G2yl6PmCTpFF9rmjJ70M3cjB0vEHAFSGu+MDaf1hqwRopr0F6meAtuDWYfrZ1LKxiOnRv26QKIWWKdsq40C9pbPsW2RyH9tMvqkWBVOauUmNMmLv0asZcWRXBh3Ck5rSabyTAP+oapT8RLkqWwr20QSWAXcQo72xB8pWOyYHYEwHp8hLZwDv5wm3vJmdQhMDHZbOaCQ5IDbUsVFlBMB4/GMjatiU1f9hahj4mdGfiPYofjcidjPdqDufIsgOUaTYT+B7fxHisuq/+dLaidbf52TvGPJn2DyUK9HhOnzWB014mmWQc4Rv8RZKuILaqgX9O/g4j5l41PgxB7OfBqXsSBgwCxUVKgBptGR7i2HSdRKjCLaEZWnOBPsGhHyf/bfIJWp/u2onXK9nvjaPbFoa7chMo4ts4ATCROmod2f+4ARmo9zXEwsNeUjO5ClF4eq5f0U/wlKsOnTbr6TCbBbSKVwv4En1XpVriZkU7c3ti9Vr3uS6wI2LQCDJea3aZIS9YCRVKibdKM7akxQ5hT041nzBWTKAYfZvkL2T0SEOVT4F9oeQkamWDN2SvuQiKE1RlYYtN2SITW5NolM1gxfbqqPlOZLtKhAv57fNMpJMRHxYZlDKkyDynzB+Q5v44mGsQCv+EOqZh26fDV3ugB+yx3VQHIF37ZD6NtSS3UXWofCTVa+khgILUs/rPzdpuaD8gSzzaqp05Qwz45AJq9U/bEXB14WPr5oPfJ2UKPWDqYmuCa6drEt1GXXUvWJLE4f2HR3GiekWC0tJ+TZgc0QWE7hiIn98CINeGinYPULYPzST9bifi3U2eNbZaZ7U9+TnZp/FyR1IQxvgl2Ofn0xi0pLePgzx50Am6LZ9GLEwsCAw5K+sPOhjrGsbufiCrr751S8jhzcOzDEuAg9+VbI9fhdt2sDFE3zWAyuZOCYcHSfICprHQK0RZUv+I39Z/He62wuY3G7f6Vu0d8XH9LtjDewnQaRGcSDtR5wpWDd28WPJ2Q629MjM5Ct9/PA1Kop28kS2kEIZldh7n348XTpvY7AxUEKmyvHZPIEXQCUpF0+8Hmx3K+LCrMm2PX7yrreynXDQ6D12A3yXRs1Xikp7tg4ocDhJIdxAxfxgYnxBHM1vKBAL03iRO2fT0hyXMJAF2nvQjjzbo11p0CVW7L0+NfvuS9dCJrHLShNMdBJ1sByEImzO+sXAfyk6EJztOWHm1YrN2VifzW0a5R5uCY1TEYmmXPvPDUqLKmFLAysg91UtpvLVdFrkPNhy2b7htZM0GDkJGUcu5a5itB9UllkmyxEn2Vq1zq47DWxPfSDDRNOhXVkYkzbT+36nazmXiqwl8X0sg6H2O2Xb7X/wBNaXOA+K/Op46erP3k57TpRu82g1rh5KCltZsYSyNYu0WA01pjz42WO4mXb3m6cLn8rnzoSxrJhqWH6vZDdbXN6+XKK0EP/dwiiyXb64fxwJHnwfxUwZTDGKK69wRhhAypWmcYt76JVLNFF+nyUUiwaejHgGtR50oMC1UPLgCAy8OlxdI+ncoSqKlutewuKfBl+xPwSiob9qN+/zDGdVR+Zk0Vwv+5uQplnIsCVk1B1eZ5yNvvfBo/A3xn+I4Zdb7m544do79fU1e+myJ2C0e+UiiCFep56vGAwZWg0J2fkX13BJjGO4HhdHOKecWuMejWWcp8fD5cwd0yluCbRQZgkDtA7UXwSU62vNCUQpEIPULuyoPUo9AsjEkBGoMAyHC6KlnnPbUmrGtmk45JFQeURIycdrmkH73DAWq3j6/3I+iNFV0dSOkCo8wWkwakW/OatCZ/kjoXyonvGOGkOZ/1RbEhT5E6fIPZdo178Q1JtIpHYwaPgNjRzxlZ6A68nBqXpS1ekSBVgXWeE+K0AX/CesLhqVyQCSk2aRvLIOOstRDtooDySOLigqlBJ5FCnWDNp0v48Tcwy9o85yWlYLEe5dfNc/hCpF2n/IWhLrrCnHZlHQsiDM3sZLIHlRtmuTX3zkAnqPaQx3qJi1O1Oap8dYt9BYex63yGrn8X9RVrXmmcSOFEWP19Ivan/vlHvq50vyVvKXIpDix9qCOxdqTISTAVaMVuAY87IcffJadV4eeaMUxykyyZ28n2THWQ6trl1MwPXyioK3jZdWdDYezBvxtd8wJrNmmYD+PtfUpG0C3FuKi0UhxFAt5p2dd3jkF//zdyorXfDdDwxzRp2cCnBYF1WtLrvU11Zh6dxyT4cRMP9UJVs6lYu3rpX0XYQBf5W/EZKgtxyvhhXmYBYu3aMeYL5aq5YATmR7Q3qjv/cC75tzVwfFRWBonpvS+qhrrnTnNksegazfG1w/Qo3bz1mczt35NA7kcNZRToNYgdOvDSkyFo+kvSOUfwSNu+aggTkzzlQ2PvKb8lepNp5ICiA7yu07c9YJ6/IVjObMbwkgUiWbdMxqVdGL6rpKWii9P3dBgLkl0VJeqY6OpqaqOecjyKgAetY8T6T2muFdKP62SIBPJpujtHe9o7jc/dBWj+Ql4+qHM9iFnmsFz0nwUehnyqslaZTJhrk0Z4HcAM3a8bohfaXZZA33l7a1wc1nVtH3g9QjtOcXGGMGic+1hqC32Wb/XqHqQZyx74J5Sg5c19XBaBN3htHXZzdaE3RlTDNxWz7D5Yq5M+jhPjHVpBBaXGFeCF/6vc7qXKSGJuZhB5pkSDZsmWXMwhL9KOOR2IIL5m76gNy0Iynv2CHH4qJxBGjw9IN60FupbvN8g5dXgagNG4osxIRC7lIp8cPpYEyu/V5VaioXl9Yf0fhVS4IevOJJ6WFHZCMqq/xz5q9tFdZosOTjJRroCXHUrnrUO/ATVGo2wnL+q+WKlHDU/UGsgPsBqtjCATZaPkn3FCayk0TJgTKt1khJcSK8ubEn5aqzfu1z42EY9SqwRRV3nnkPl42teosxc3Z/PfEKQVuo5fWLVsY9sRUyhoN/UmtuxJRESR4Ay3NBhx2z4rTLAEHZ08/ZpDUaB7G6BbYHimpFgn1dIywl8Ke7Ye6z9eG7kuIj5TXXvSEDxnFsNxn9hVkZg7KN6uo1i4S4mRPxCJiNSLxwzY9laOsvCChQUTPL2mHjVP0SKrNoOWnX6zB+EW+bvqX85bBS+QmxlWiv4ixL71xTx3fJn2pEn1zLQ1jQ9huhgUpNJMYEEBULclkjNfX7pwkaqx0c/WiqrOAxvVN4PjvEYgP8XIl+gsSSJpPNN1bcPQGXZ9qxyHu73pcZ4mQM0SK7wR++FRGMqvt8o+RGYam6HvHIYyZbcqLZfga1fxJBwPynAohMIROWofyz/XaAURQ7gUYry801SdIPtsVJIyor3bnJT5vBY5hQVbG/9o5O70TVg2BHF6pYg/zM6gdUIDTCsPtz6v7djOgjtma+xYiDZqkOPCg4Kn2Yg0XiPSsSnn3/6FIf2Jj2jEDKJA2A5KZUzlD7aMdx6NbX/fzzbgpy5NZ6YdkLkm2/WVATncET+SxTw+k3YGr4O2PbkdyZ2bNjDsEJ1erJd3288Nv5hb3wKugHx0XCcEKPBKS+bTuUKAnw9YBCytRNpXt3wrVWmY7Wa8uogyMV27Dydx/aSDqdKSoPw52FZP6uglDXnxJXJbek2S0ZMyDf+JwNv6Qv8w3yRkVOywK24nZ6MQuseT74HoTWJEb/AHSfyzTIl8cYTlK590wDmSEVACvV002GskUevXd0xHB7qzwtdbvU6qBjzgBU4RQMP83HL/1LYYHY/CTwrlOLoOnrIwJhk24CdZXAxnjAcq8vT9bqLY52VbI5y9E0cAZx9xfGptpOuqYvOhKB2iUewcvU7+DI124erhKBf5HQLVCA+yhwEH8xu3vvqyaq9MSwVUlisG51+ZnmmwQovB0b/Fy9OaK05VuRdP4LqQok2ZSC8EvqxTM2qO9wfxyFqcCoXhjXfQNuHkQyMRwm5/+/b0kZsCxqRSs45Zr4ROHUL2zFQrT+Ao4k/P93K/g5B0y6IcyN84WmFF9LmD6h2pzVzYC+CIaKRYNbOWXnPp0ASkL2vqBAhYfyGlk9msZ50HqiQtbLbkV53/8BdwNZuvSdfWS6bNBAI7SEiMB+Ob6oC3c+5gQIQ/WrTHjgCukOb91nmTqODgMIvQS3WCDpa8KUBUgHCq/QrrC+gfBDOwF4IWGBSy33zDQgKOdpvqDYiasdriZIAr128OLW/CpoU/b0H8jpC3hodUGhyqjP33ByhEvEXtV+VjnbjTDciMi3pfLs+q3pwLxx+N6H/UJSSiR9/cbz0nOQupvsWMO2H39goFgfSX7TYQv4pcXJSRYjDm4AA9KXjRevyFh0co+6OUaqIV6Qil3+y6q+RmXETOXciWk+2ftAlwvHkA+XJSZYHbo3dCD15HyOfj/y9vWQVugCDU01ngo0qUaC61fx1jdaW6ainmbLwQEMEKR3ilq8ChzoIZ5ODG1eWZ64a6nXlgmjNNSDZ31wD52kLWUNCnALHUOuqg19lVKrxqdBtKqp9TmC/QBLOhEuvoiziLGKwVU/8SNUPAMrHMF3IM1IrGujdjoN7+iBjye4uKYHRBZ7O5x5sB0qWG9J8wLQfdcAGhokltpFnXE1/EKw5hJss8r2REfKDTHMUJbOySOnBMNWGzQzpY1mtEjyAvl5p+di5V0ebfCEYoyI9AYWbdM8fcimt3GeYQz8aWBBl4MB0YnMgB3TPbfuCLrEVZAAal/xcenNHBuqEfQptAGj6PBRkDFuY6wPZ8wCycMAsL/zfbLMOOe0kebUpxgOA9KE5Vico+crpR71PGEWiWXjdc4iw013YnRD73OoKHW8u7WrklDnP6G+NASm1UEn9d2rtjS48V8MdVif50B5Y7o/WqDo7/ltdjtHGb7b2SL8YXtrQrjfA3sHekApxpfVr/LM1DWQGutnXDtgg9Q8MnkIp4kjViBnYr1R34n+Sz3bdG6fZvbwIUIS8A75VrSbvQr97JJG3JY1Q3bzXW/Uhdf8pzJ6XE+VEpSS2XXSa0vOqiIR8NuyaYehYnWeVfMPpjBAjnCYdpLqYbacDesOzuajpgXxl4In+PcAIQDzCNINkfumxgIINHmvAhG3sB1GYgYUlUnBUG5FU0n+7E23pXVv3KUcfE/ZfFbAxaDA3p+SW6hqWAoyHSxRO0RiT0UN5KdM5twr8lh0Wy/IPZCXsjSA/nDeomqESXdKUiWgqmGV7oP1cE70ljZgffsKxbIVvLZo4OX5LEHTt4Q9GDvapuLoE2/FofCAqEoZTdQgSIbZhZXKY1TyqugZ1IwjnppBCIOKn5YxU4qyWRQh82g5JhX1MeIQf2x9ZtbqBxhr5VEFM7G/kaplpOEimtXc5iTJ5BodXl+VWUQSpurc8CuVGnH8zEQDCsg+epLQq58l02PxQQIhJW+qUjIp8lnlc++UztZajBPNGUUaTa8QJeelLJIfTme1gaUjjD596MBdLrFPnbbTnxrt5Kzu81aOGlXCjRix/UdhD7b/2wF1h315Mc0/pH0IVqzoY+ScaPJ0Z+ITh4X6GDc8trDkTLziMSQz1yVvWE0rhlROW6UkWiBPa5j/ty3KEEAtI0adrODcb5ZLJiItVuoXh52/YrwKcH6RK0tdVDeLlmDxbG9YD7In9LzuJVlV8+kCY8vNudqep8HKXTqStrCylkcGx9HeYSdPfwHUfLKdMQ2ObUjmKAKiq+C033HGQNxaxhgvt2dwDqc28aVOpbTDT/YmccrdHJKigrBDgrA6P6GzzNC5QMNbvNydDCukkiw/Lp1Ht5/Bfg+WyYd90AXbvTJHvPfuRiX9dSz+dm8AKYISulRGt2t5STahOg3sn7WCRziPFy8Pk7F4A0m6xGDtIMWAcC2Ks5X0CrJAvL1Aq3wS3KUk5Kb9pj++M02DZ3JMBjZGICxaK1VS+lw9r/sQhY2uAknwo5HJ3IJ27oOzELSAoeHT18ahQFXuUgPpcZKDJBo0jtG7T8TM0G9sQ0tViKuszMmoqCjxColPu4KbNsfob7hpLxao6YP9OB7EYMKf5iyv67/z9ZrXcVde4ns0tYZxpnjtb3jYKak56hrcIgVqSxt6tYP/4Hgu/mV3cn0lnDs8wD0OMj4or1B84hWrfNekRYKQKkzLmyy+b1CQqVjC/LdqgvaFcB2CtluSjl6bKa8jLt+70OWPjRTNWneM1IbI73oQaIyScDFR+RVAviTEBIs5/4+f42xPdd+P6qkv1J/dXjLTXvSoBnlqHixN5hXM3mfkFevDrEMB3CKSlE3kt5NzHPg/x4e1O7TQiUp38dqEYDmZjtxEDFRD+KhGM2LKQV0qMmX+XmpVA4CrjV9aNONn1Reh/KzZMrNCLHSe+MsYHIMdU70Ip2qh8cKna4jy8vrClPDfXZerU/zehuVlGmc5VIBfLNNe7/P2N8AUgLJEd7Qyu64TiX3QYfN9WrBhwbXMafJlOGbWp80LrTDk53WfMtPLZqNeNgBICDqIws8pjvRfgZ5ZDzKImr5vRQ0BBNLzhVY5fF2novYMdDOudnzt0groUE9d2birYPSQkkMbnDVP6p1C21tRZ59xBebMSrwvuLxH1zRBc6A9M5qj/05B6jDegi+GDc8WFcxNpdWEhUu2faAz7JzusgXqB8Pc16PsTgM+U+5XZzVLOKVn9jZr5TNx00NHjSaGIOsvww79sqMTt0a8sOGSRox7vHRGeXUaDOkHVs4e+k7Zl0IxJxQuGXk8M8bMVeZw+nVYizfkeZ549f4QduxiKfFzkq5GIW+2/iPRtLmiHLsqCqio0z/AR+SFpEBjanKiJ4AiztoutXRW8eEt7SjxmAIp9H/Ibb7iwyEZa2wDVQHqiz96aueW7+WVRPngMmtAuwLOXYTn4w5c8CNhpPUkyxNB3wOqKjb6oUGAVp7kuIomdoFz/+2CFfqw89ONCPhvs+LU0a1LYVsxsDLPvEmy5Q709JKeUBd+dwiCi07RtRkQZM4UGYn811Tlu60XZG5/8IhXIrxSzvDwhP9h3iZ5Xvzrq9FutaqWdm2mD3Pcz5mdSZ4fMFC0btgl+hDklPdk6gw/0qskkMzSElXsgmEVEEcLxL9kkbgYnkfoHzYR+JdVVRiayr71vb/MPGCgmzKUhyAlqWP4VC8HrII1mupYR/50O9YiQ9UPMNIev5suVXBihC5/nWARffdO8A1UuOGd/nR2Atn/x04m/rmno8ZO0R3BQmhxpa6wUbKOk7jkIfL9GM5c9FExW8EU0gs2I6onssZI8AhmXBXR7MsHrmu0Qexjo3ds63VTGqVsDwHd1pcVZS/Ve3V0g911zoAuZ1sFcoFjyxmlYGE1Dre53G7fJgX5vBzEpmrPbws0wE6hnW5T0Ifth2w2gpMrl+LslzcyHqdiZ7bIQNNlNo1mhnkLUqGYj8+TXNM0yiAYli3BgoX3/CtkuFlOSnafp2Hz78/EKj+CE1KLv7m7FdKpFYs8mSslfiL+gwNlYmSpGEcKNp565OjSONE44fSYmB4kH3NlMpDfewrYtYHpBxITh8ORNI1Afs5w4xv73knpxuJZCCEzLMqRoM9RMdv1F2kHC0w2jlbGe9Knts4RBN/emRMjNsMx3WorpisFtxKUKlJnyS0RxTDWYNg1cURYR+NUtbz8kfd9+cgZxYoa4KOrdtuoGLfbG8Lx0/kUQhaAbqkAIEj29BP0X9l0a76HVUy0kFT3LY5Su5wp9RMze+Ljk3orU/FYy0tEmMLs5EEebOtZcPPKr7wUuhGjW+qJts1ryes6SGw2mjbrfMFURkpI+PXoELHdj0JpnwIhZHiFMhpyVREjxfbn2JndTUCvKspWzRIk3cKEW/IbmFpexi3Wjavidf/dal3OhDMu/jlc28qyyrEC8NOz7ldBgbhkEL1+UkZUSMBOgfswXF9ygCo1QDhv9SnuL8Zeh9opbQjg2XzWM3eHKbBQGBTaFeF5jzWDhrisk+ioeUK4Zm4WQSB6o7mTjC78oIjVSak98p4aL8URiQ1MtcAyaUWz+FmAw01g+Q+49qBhjMSfC1GHllfuNLSlthH+2l0hKEU1D/xBpBXHS5tXOKex5D7jbqYZXgMhaSnhhaqjHgLSWr0Qo2oGomEENI/TwH0gBkKCljAWfNA95RSWoB2tCQifCGWTzwqw1VNypqVjMISJiiXIHrWkSALWt1anKpRw84aZbnaHFwseDbJSIDqj+Jtqo9bBz2FmwFN0/48oE4r9LDOR7JpMrlQRMzxgL1QAp4YZuGe4ITpo8vI+FpfTiOiSno1PHeqeVMi5xWnVibKCrgxXPIoMaZZ8huARoVlV5vpKoYAQt0D/7NQYDrsYhVRieqKe2v7KjF2/5gTsPGKXHLLAWir6C8Ycl3ujU9UuBS7NkJS+FA213urAWndxrzAVNpfHTcxN6OYJWX3KI4BMDbdeRmJyoxFjb7sCb/GBxpVW1i1C4k3rrXM0ZoXG09KPevSmTC7qVRO3yfJiunzmWWD5rnRVD08rXKgkUrbIIjb2VgypoUG6vmRSseB3fD83XCdwzw0dEQ+tjXl4CTtj8AF4PDXC8n4CHyDo6iIrsm3anpusxmyHS/Kjo8Qm/OhZY2qHdVEXwCWOjS4Y1jXo/ncXBeihut4NiA1+5iXPGp0USBHX0QgtMuYV39E+lHEoPJAkPj0XfiNTMphXmpryd6BXZPcIGoKq05u+7UdTqJ+/10IJgDAFoz3dCIFXrUmd5Frlqr8z3V0KHZmjbF2Rf3Rn8ta/rEdtwIE10H1KSfqst/tup1R89ln9SNg7OQSbZ4aoLe++r2SlrL9lHLM0C07hlGMdystkEajR6AfQfMZzD3wLexUHzRF1vbx8nh7tyZgTBFpkzZilHjkJs9WPfLFP3wKGg5GxEcZc87ISx6fPR1T6EpykrLZzohylpbvDBcZ0YVRSWE1SVxX+QQeJHkFGQrjCtIBReq9wp9lWXHBuCqdQ6ShywT5XuWPkl6N7Mj1UXyV4Z4gU/Y4ClO5ckgJL86ZFZHNwgGpe/z9KYs0AhMAMAshp8LML9Dc1zT55Hb9zzFPuCUtc2Wo/FXNswQOP1BF8LQk8t7uec6j7UGc+2prOWEVOlZwuYKnJVd+/frSt+1QWxt9ZBK1S5uhBApt8VeWWEn9sqlSXTuId90cqwkITeoryltzktxFgyD2PkimRSy/F5dqK/5idu4RxBN1K/hrHLCfueoto2//P5IACLXAzMO2STMSOPi+jTsVT42QxyJaCAkoZT/FCfNK9SeilN4u3Uqg5H7AhwGuki9x7WauvmmXUE768ZbWCuJFoRZnksjH1JOHX7pVHy7zYciPoChquVtka7r3vyOOuFFnlWirEadzY64bLLFn2su6OcFcu7qg/FsT7InB8lz5B7d3VtLUB8AXF9hC9H4IzxgwuL/3185h4+GCMb3vuOcO8F5KS6sOyvGj+z9DNAiQ1L1XPdFvsm0ZGiV/JgA0wMAWnKksECs4T6FgssaSzclLva7zqx8fJisySupjhONepvW0iwybql9MiiZHc3EqfrkvAeq1Rc9yY+dtsVi4j9U35YBgyL+7e/L/NMD3pJuXv6J0wVyoQlqde80E6wUMeMyQz7tgbMvIFEqOcJkLZFBFZWv0HaAmLKQpQcH3ZqIfj7Q+npQ+N//dlzeUoBPc0QAhiKRCBiOPT1xyRUNatJ3wvICHRD/wKIMyegUpdBSU7+joYQCwsyj2Z25HxCFfVsGEbnlf9o/P0rTIDqxYUxEnpZ+A5MZBewInw5IQi4IAd4HKvrI1wPPCULQH78H51AOkT48/GnkmHbuFOqhQmnD40PYD186YoQMyFIbZlMIyZcTg68nj0wc6ON+1i8Ernr3gSJRfCj0BsejooaKyLZmdwJSMcWh9HfRXPe3JXBTL2vvOG/oCjNzA3AZtUR8p5aEC2T7IvPMc+L12TxLoqer5kagizQNbF7dl//5Pk3XphXXG0ixQvdwhKebIzaDVdf5v7IK/GWno/8jEx0eNQcDsBzh3dTVTJ3RTvouWl/X336oJko0/fr5+W9Z5Flx43zzVgJ+BQDwdqbFaZ1811RIE9CCLHsej5JpVgMxbDFVpjfiQsHf5HedEYv6Lmc3u6CQYV5VV9RSPUkXTLcbRdLr1hj3eopUeTN/KZK2fChZZACvdWJzuDLN9PcZ2jvk7dN/9nKJYq4EVy7rU7wPcHStMiI4zFTDcoPDF5trScjgECw6E3Ge/XTp6FU6XA7Ts8D3pKL7jmZcuGRyT/sSheJfAuiDU4g1zgI/z1uPp5wykMUEypZyOCzT7amxf6Xa5ZEipYYSfhZnpzGJu77J/I4ac8Vppwlbgdix++wxBf7MpxbIeySy1svdrg29a4LFo3YgYaQewN6Jsh/qgl0Z80WiEUJCcu7y6mA8H+bmK5w4+JMT9oFcPtI+7gfukaDC4dq/7jJpyc8BqeQOLslhcFysc88oiFhrwpik5j82/i3RkE8xmSdesZBhUvRXb3oV4jbqy9r6OcBh0m/4F2OQwNwnPT/MmIwnS3j1jt+eMm2KZauhfotb0BIlDIsY1Sessk5hFpriE+W+fG1RyMpfodYUXiWweDJDY2lU77dXJs2JZplarIiRyppss21WlfvZa6F9Qo1XuPa31wTi8RY+4t7I6EQPYHjAhMAVjGwpHZOIqT5vejvuWMAyOQipokMpiqfmB+wYlWMZmSzoye1lkxqsuRJ6S6xDSWVodbz+CwoNCMV80ZBtdEyNpO38mySyOzBN9xYxvGXv0SRFpTEMXZ/GoDbtU/KPinLVTf/PfB+J/Q2MlXbgGK8twf/uvxGT2YG3dm8F25knz08YV3a3vBnlWxkPCw4UhATPLljos4QVoH+AhE2Z9JTcpfu8ESUP1AjSOqNlm4MpG+AHERdpm6VLVrS5uKKMO0bV5oMQw307LZcbYOINNH2+GgopRWGTyVlj9u+o12/fwGiGXU5SW7l/lmYdYsYbQARZg/w7xW+d3SOYfI+TjYEQKoxUUFJ1A8P1xs2SlEp1cunrFDJA5M7EY3OXAzGwCroZ7nF4KS1zNfdIT7PKWbiRMH2wZjw4r9b8ywmCZVH841eonq8HsvtTpW+UGyTfCujNafyj2Wvuj3Piap1/eLROLqLusai1sadp+xxsO8RGL+VrRjf8JThEpJcbgzKie3lWbqwwptZI6+nA7MA+0uFiiPVKfwh4YeBKPrVH8bxTctzGqWwz3m+wCgPpHS1MW/hqxkIL3uzHwN7aq1v+e22Oc8xU8+ZnFNkJOqC7Bbs3oR52T2cbmRjIWMUAIhz9H8txvGY8SaMA4QVlovNd7557a5TT34cNbU0aGLeozWw6QgGpiDYHms5xhJW5FkZPicn+yl8sK3/XkKd+3nThD5zxdyk9UqeNTB9WtaewYbB8HvPDVEMXHYGlrbcsR7IjmDuPLwtFv0zh7H3IN2r/jZtJjvWTwwFARsOiGFQrSajEDDo8wjPuWkXDqIFuvq7Zr9/FqrQ3dBwlK9w87XzjWTprW2O2bvU/Mg9A9Nnd/wMQqSMEp5iO9m9lDrYjA2XKx12iJJjZXDjNLoWfGHgK+2osgyPjqt3w4OifluId/u7OspdBYalFFwnUSRY0XZrs8K7qctBHRKeZdvBi2ruuj31inrWE8xcNcfz1PGYm9gr9Uc7FJkpPAJhJAsxxGi4yaHEgc9AWlKMph5sRTMafV745669juC5d/ezOlh/kFfhN3R1gZLOephhAzZJQFEJ+ALr/4wdHGSIY/wnNnoEmsiGrmaejL1MEQjS+pP/2jBn9Nq9Ef96I+RlDD1fjpRn9fq/klpWKeWBQpKFwCNfThr/qDWuQR0FoTfRc27PsdMb6WhcQ7N/SnipP++FkMKARJlyQOczZ+ZLWFYvpv5+TB4iPPd1cI+O3y6oqXqYd03DlKUnQbe+fqPHmrlrGDQjEZXCeqPlKP9ihRN0ut5sh6mAjTJ9KaqoqIP4O/hmgWQYfpsX00YdHkzuZmG8lltQP3CRqbJ2Ss96WRQGqVfoDYEYvgeEwb2vQmLg+OdjnDaRvG7B0rFfatxEmA9HQpStP1FMXngD8XYPsfh2Ni5uJzLuQPln6ls9RHw0F25H5aLZRjJfWeVnP268Q88SBM3Uya2UNDRedTCkbkN+gfvN8yKWguhGqPmJENYo7xmfwaI4J+lJhab8ZIlPa4IwaMbiEa4s4st5QaCb9nTtxBtPmqcukh9yhBtTOpQL+GyJX5eCU97dE26oeCRDKNLs9UVzYj6JJIsVUo3tNQz8ajgFKPnVhi2bw9whyT82I+zfWPivoXj/tzKYax3u4dZzfoYfAfBjUV95BnJlyHtG9odcj9R1LY7OadR2NyrRgEYpehiglbn54QPExAwacdHN6Up0Kr59xtsmyqJ8crxrCTBQurT0cAIIVo/WQv2O0Lo93gK1tFnaPcIUN1oVjT8Wblf8/vgXfqva6v5k5Vcit+P0MUZTWviwtqtQlt44ripDl78G0FWQ5lB3QI4cRFxgFf2Da8cswQszu7y6CeH3CzZu8+rsNjMpE6wLLhcakNNG3AihoQkUdmnO24vgzkuLOxXjCNbQqkiuJHofg8OxlZNx8SiIun8Izz7JBW2rvzfwmwaBIkBvfBC4K+ZGneT8rsrpJzYuFKgO1lewy0VSwCfXdLgNfaeIzyqyAfmp9SDAJD6kFjzcnyUYR66poFjwO7iQivSO4Ok4//Woexz2uGlxsZtSthlKfxOpQMCpcnwBEMrhjIMqEd+SkXSQ9UH+W5lJSge2igFqmS/hOBMq0CpOgtelaEjIWCCRqRggfnCOsc44e1RxozKkDZZF1b+nt7YuiRmXGX5BBL6bQAIs+VkYOWxvmi+5E/YQb29NJMTviXzeLE2gy5HK7QEsDxwR2XRf4hdOnL21lx3z9WNg6ezuo3DgLUtzBaSBj4PCIA3GxTzxRa7Mp2Vj7thA0nNU6VWtO2gHgNBFfJynPj2IFzY62qmmOhA+JiSutAKtC0/+9iSZ17vB6mu324s+0XmOdspbab7HWO3NvFq9ndZ2p7uZpLMp4tboOH/1RxsHFOWibm1JPneo7YUguRoT710seEGihi/taBJaEefOblCuvHzpYG15q0//LripMiPU6VDdqSjBiy5f7QQp2QhMuJy5Hvkj0vpGVs8g6TkBm3bs+kUlW11hZwiUEOMrs+ctASSEDX23ny0rcFGsEawK1Sv/cbO55yu3Jm+89x1ImG48yW3Ai8GSWNYMmZBu5p/dJYkChOWFkfqeUAITF3fBpjac6bwDCsOYYXYGqRXhZXXKz0TJ1cJrVC/Q/iGxNEo0gIdGi1U8+c4t+NOOtlYbWw6XDQ92R48AF1BmQ/XwbLu6oSf37aoejzARAmEwMYfiT5nmar8sG1GqF2ioiOEeG10F86un/TTI/TdkPlxqey2os9Mj/KnV9qisiyn+pCdrabo0zDhxnOonlmwwwH5rjbnshRIv+qyK+xCjxNM59QIDnneffsV1otTHQM5bJatqoZPvF/goqyp7cl3Ko0PDF8tOWtNsinikvbibLXxDQyp6PiVGeNA4hrRBdEwm/NcgHrgs6GEfVoQ5cIouHmtGbIIr2xUKDM6zHDp50VPmiQsEnZm+y5LWohHMOHVuFw4fG2nRm4g20lGeU7mviyZtZ8Z6RyXOfACJgJMGi8ouEG/jjD4T9aXzNRnQqUrE0IlSIcAMcKGT+gKdYYZEThLXfQD8S3q/+svmcm6WUFhZT32Cxo+qUWVEy+sYSer2GNNV6oD7xTI3Er5DQ7Xc1Zqn1Azi7tmG9iEQEw7M31/zhDFg38wXVgR+jGN4VMRA3QO6oyGalC+OVg/YK+L4pWwfEDJoyfinbGg50IQbmv8/NNqxdu65dLFVuhKWrspo34/fUmr4tj5ZK2/WUyuKlsldY/Kr5APSYEmiZNS0fHsGoZzrOPbBsCAlozFJWG71q45rDKYXONUjV8J6OCJfmvhE8McP5pfP69C0MOiExBGgiUtAY1r55VvlQZ6WEXwMaD5EM6FoMp4LdvUjMq+OVoCqkxXesJb0kwz2cvjJlJ+OhQELwhjPJPeuNPd018GJPh+Dj2CX+Muh4gK9zvIB9GiXqC7ypyczJmGbGx36yLCgo+MBTFt70YOEQbvT7i1KMWMWscA8cQHdNE1eaFnwY+2itSf9a3MERzX7b50wBXoBEWDqe5wL/jTCCfVEvvXyMRH7NLhaknxawP6J5aX3STVms57PQ3RbWHFHzQvr59Xao0SXirFk9QQfTLVltrUqdxpfB6VyqjB9055Ft7YGtWQyX4pzlfqTJf1edQxOuYOCQY5U+XcJMbDmR4Da7HRqYpzNfVECu/h+SNrODfK9FDlPQkxuodN1WLHYvdC/i4rbMS1PvabKp5K+RBO9laUfVUsmvQ7T/7t3gNLDk/o7YOD67/kPrxHIVGPw1OhkV5E1egD7wRNd/dvoHtyjSv1xnSrG5KJH52zsc/VZon1QMPTYQz1EVP5QNQLseWUHQ3XLx2u734mbJLXtuzWymNKWFiQhXqsqwv5QskyH+xkmmIRCWoJjEayTZkNaxo6cjiAxf8WOTU2QyDGFiPlN8OOcWJcNwna/XttKFeGiL6Tl7Op1bmqUjCAphXGFL/C8l3a5GppTHCYt/eGXlXZjCkpXX8gQcbMhGKMx9zQj7WpY4BJBZNus80O2Uyt7OeX7kG2peFD6+fZjlVQ4GGcLAbAnRyL2JjmH/Wr46HuhRe48ZatXZTsNyqGGOfyZmV4wwVQjcRNE7CIrTy9BxBucvSXh/3xfgr7B9SlqvBi+h/fx8bYp1JYhGkPbNTVymFEzrXVv35v6f8KJxYw78jRPobcAML1jaj2SZ4SOBKMlNhrefSE1nv+mk9TxZHLsTKseP6g+X4pTGaaf7DSIjHmlmjz1Z9Qkuuehq5Rcyti5DSgBfjViNV3mDVoYi2Jd5Iww5Cjx4Av1CuU1Rz+QBfi4bGO0emA5SKH00H0oZWAMGGXLF5a2qjZ3mnP8R2VC2RHKVaWH3z+F9t81M57WXlUb8K/Yy0EMBAOAbSAHfSUQOSa2Dm869KayTvx0hWZOhU4bCR7aVR6b6q4820aUIuXn7kRMkzOyezJ3l3cyiCCxsV0jIHswfQxEx8tvekE+1mW52Q16xrUQmtvR9m4P0yru+vh9hiiKFRp8exQRLhSfVR11i/gtP3rOzC6h/JpLMsunHsyC/zgniILfT8A6w+k3qdFRszfEY00Zsz5FJwMfJvp4ZGrVs3O7+k6Pni9MsoipXQy0zOVZkFV7T98ldIbTy0y9IXTUnsBmh+Ea4+XXQMt4Gia8Ml8cZBQ1aIdSLca86jDuF6/Zo3XEbd6hPVNSvSim6RabyNnKRRohQ7G0jemb8PQSAuE7sGs9NK8SDPP3W/UdA8lBVixNI7MZRKUs0V/xO0iZ9/Xh7zaVi8LITpRVCmYLuALrNBFFKL84leNv335kIkRj1RIHKfd5kj5YxYC+YLUfQp5bICwN4Dit7VjJ8jD+fcbLcfdtcKZhQXqrFYy9ZNxB0LmAblw0zYg7cON1XkgSarjHSXt9aaGM4HHz2w1NDdZU5XRBu/OjxZpiMRXg6bw/PeaCVStA26mgGA0DeBivYLPZXne4UQ6ntaJ/31M2Byzl97ubio1dQTVC16uFIUfCmzsyDPgG/XXR8qx6WOcB/wq9NYvKmFIz9vsm0Sq1QjAjIuTIJCpmat6DKX3QRVaz7N6X/WTOqgbedC2X5Ja/BkLyr9eGzvpdSql8Vq4jjDVBgFhqLlEi7OnlWJJIDL0668H6BFfyXVzE2DhAN3Uh07Omw1WNPMv6RdllqE7FE6me9kzg+F3W/DqHUUQhwxOrnJ0hZPQg1EtSaFlEgde39Y0COCSH8WywptB3HWOAg5fhRbj+O22o8SMx4dJRM39DpzEnRTYQbNDQs8n1/pacC0XEZacjhnZxZ5IG8gsKXnLlSDfLlrFtBYMxcoT3nSrGhdLMMB0Rtm9848jfKn1r8NRwCZZqlQdAOjqGCSIfiUs6fpfuSf7Rn9vocPbZwFsIBM6lYmGbQQ5pzJETYiCoax5eKgcBS07CJRFy9LV7/yqblBX4KiilUBKZXYw4gX+PL+pz2QFVeHKcFPXhuNzo47owi0CTCnILMguc+ns/T382WsHrXnqqMMYqMOsTcfq1og9fZZgxPOw63/uG1QS5OKqlt2UYc1crlblTDZTcVhy2XZATyy2LiypyXpjQHk86NL3ywnjpj9f6btMHF8JpJkRL7M1jHFuk7/l7Pz4kwnLOAEoeiWOtO+5ZwN/SwUfi9ehGFwAKOanvwTsSNaCfnCAchwmO9YdZLiB7jydQv3EEtt4NL8kDjDx7XvPu6JzN/w/+0+FGmvMTq4DXx8xo03R5HBdhP3ByPl9OrBdZpimasoLcw8Td5fpeoxPfFx90Gy/xph7szFKQoXI82nX3j82ZUjtrEj5N52Z1+hvp5tFripCTiQfgGNWCbC7y45OVgusMDY7QAwRBmbHc82XFlwVMWNPuZcWkTPuij6+ia+Og79QErsoghR3RP8BcZMN7E+EkRU7snNS+PUvLJyC71soRGDT2MGWw+2qTMt7jXN/C6hqelkah2SYqbSb8U6EvMStX75ReRfw2jokmYWCaPsRoU7Le0/Z1CZQWws5v4Qef8rk07d/Fpv1pgCPH+viVQP274idecOUBlwY5Diku0DudDCa+rH4WRSbecrYdHibeSb4MGyCK5k0a3zgNvhUA8AZwRWLwHqPR28RgL/0h4vwi83GMB3PUMS4aa2l49SC1MHiOHBgoLu3mLNgWq4KzYZ9g63Qbbc1od55AMgR8Kp9uMpN7AR1XixWjdhU0IpqHgEBNPSYPW8lg1T9kCF3Z78jEOHGinzSc1Ho4Ts18AqD0e5dHKPjbiuSPH/H6jVWtabOZRwtK8U6/PUyK6G8mEDp9raDaKNx1SkYcyLzPlix99HPUEhRVbY319JpBuV3F9YbWhZJIaZ/7GuGj85ArmkKndW0U9rgbwN/qU+EMgp8QAQ5YW1Zd1GQ6TyYpFsvbkvBo7yEsEsdHknL3UiGuStrzAJPqwM/6XmZ/ao7dJSop1tMxhIfY/l6xtxjucwrYjbmzQGe8Wcgfn65HP3yls02/reqbfKkYYUvS7x+6z/Yr/le6Ug22SWiU34FR9kattbdaxjU/SuyT9WlMS1jh0Hesl+kpzX0Tltt2jPl6xIqlArNIGXLEFngStAD9Q/GpAkd9ZrlFUuSfnuzt+Z/2/K1CAoDM1RGdyL1ItpA6u/X4ecFYjfaGW3l4SJF3+wQc8Ex4RIbVItgaq3BeCwtQGn/avkktpl9gUdG1/lp6Ch70mhE3J7kSPofqHYFI966ePKO+A3/suYstJxTc5iTKJ2ArYUxb9UAGlpvR5cNPKgoOgOQ8jt45muSz0ockw9sHLmvB08EORuEzkebCb0HrU+jBax1ZVSIuNaECqMj/rKYUD9XEaQY/aNVvCgk5IK8HNJ6Hc+6Ke8d9DEs9nNJEQ2x3YN/dc983jcizO9plqZ3vHVsYCEjR0BFzqDxgz2kEjy8lMMbfvc3WsTUbEl8awlLblHakXI0VAOMzMKUJWC2mLlz1ithQ53reowhNL2kEYzGnRM0smgBKvJMjHmRH2xQL82OfXcXRfGUJOpZd9d3+jwReRqntKYdVCtDel1KLG8RXLWF6QMybMWCBcZOLVDlfpfqvz2aFcaSoyTULXQjrXVtOAOtIrvth1lYSq01qrNixlGTmkuDEdCz+9lbNvCbfW465+oK52tfoyaWehI8LGxEAobf+zJKkQ74Bt437679GtVY7nq7UDoyEuC7VkSdF5xOdl/uSTWnSxbcppKHlrvHfv21UPUVIGPtNVG9x+lbglJNVm/LBe9PJx8/UPtGRBE3LP8bu8S3l3yPwDCufbmC8HmzgxMf/USjO9nuKpaEIyX9X6slfYJ9ksqfebyifV+VVaKiD5l5Ed0JuvVFS7IcNr59wsAuPRhCN5KG6ByhmErYzvmFAhydgA3Crht8VQZqx4pYbsnVJjtbidf5U/r2GHKNYpqNcLEfVJNG3eT/cICnzKo4wYHQCXqwIkQiYKSKA57zRZVqhgYb04z20sKgRCriI/K6I14GRrSm6tNvc3HE7syiT/RChlSXXoYcwWcSFeZR0al5MDSUhFei+Hf3clHg0MZeLXJ/HMIX5r2pxlHmlkVydzTm5KeoUqBh4PgbwLif6vQVCZPxvVxCPFLx8HFSznGkltT8KQsLEYVeBKU6kjUzfEbr8bsJDZXzdRfben7RCt/M6zMcOomGHJNViDhk+nOVXti4+6GafWCfWLZYq+3vRxWzMnFI3bMj+PmQ8NU9hzQDdm/PbG86fzuQ5eOEXu0OuFifVOBKO+FsQQd/uy5mASiO2awYfdl4TGI2M0r/Y5iPrnU7sfedfobeEkCUa8hlk7estcBAYzmUqQMg/+Q+t9oMr5t54oZqoOVonponSI4yn25I/CHbSjKazRmox+mVyjT2/a+xrpqVfTGgCFvo/h5AkTmHOAqU3zG/gdCXbI/LgRLVe4EbQfA5+ki1fK15+Hei+dyMs/pv1hUVLA3HS8N12Zu98DXTRQ7idxdzkS9TJUlSdJl4Hm41OAao1LLoFs3uELcJoV7RBoT5x/ScQ9KpncfwqHr/2zj7n3zhyXF5dbimxrxWHlYNLTryWiCgVo54Wg6rgk6rIzvgujv8X1DfoNrAcu3d4C8kDOCXo5hFxw2DYgn4ecsboZfY2kPQBjYNibuBUr8pU9S3QajGxeloobEkX5BqcnS3T/3mW2OBaPUkm0YXVbr077YtAn6KYCGSk9bVDBwewIIyruYwkHvJ047UljwXaD/jRKzudYaMDrL1V9kXRRwU3wPqdSDPjOLdpXtMLa/tw2daUGhrQJ73ZeHc8dEHIM/dfOSAlTSimZS7DXOaJo+7iRIfpDwregey+XwHoasYDqX0cUt0VN7kpC3cRUXxNu3nL2RTfyhf0MeBoeza+Xsg7G56TkhCQr7+FLSN/8zxuXqEH5vj6vvBBX0QKur7sKq8eebS3OtR6OefaztcBxhXWkiGNE/mQGg4UyBkRx9MmjZXUFdbJ1LkBMjgRmLjx+q6ANJVQeuUCKui/AEtaTtXBHnMwzgA5sZIPG0Pppacr6jGvKAkF3BJv4shw2gYZEIf/kkQN9/2BDevNdpCEXjJ3ZSINh8A+I/LbWTfrJKVi8xLz5S+sCKnsYVGZ4aF1FE0Ppq/7wPwq6VIHwGKQXGvpUZKoZPdc2ClqvBqWWv9jsMxlm2YLKnCIWGz6m7OCmir3g8LW79p0r8od4Aut1tI9rK6EwNXY60Fv1Hn9NlnHSD7VmD79z4tFf6n8lmPlIeNu84yK2Lk3ETthwwft0T2kBAnKZEG/ADAJB1UzkF0mxVIjCi/B/xo+e3LV321UHqI7n98+aP2N0pmW8nfeDQqQo6zyFlfVXo4rZeENqT8MewlZ/a3/9ma/FwNoFA+su5xfeFJpv3il0d2znzIWA3EBHOievevgCn9A21Yz8CLrr9twlXV/Gi1+1IGIpuZJeqOeY/65LKigy3yw2elBoIn3LmRuP6iqh2DxRefcYl91yLWkNW9TRH5NeApcD4jAdpFwnwT8mycjhSQ5cK/+gbDc7t6/EglmMeOG1wNW2AVTfa8ldrvPLKxqvdReQ+GaUzSktGfSSk6ByhUuWbCyZo2QIuL67O9x7aiv54Dq7VDqRomAxiaPcwJLzla0AhT5rOQmBf5AUq/lqOevvIFP9WXT4Ft8TgynSYd8N6ymv6KPF5WQr1KgGSz/9D2Kw1BuRrfzv0ADsqcDUGqzxUoOoYoUrCq4zTr65zgSsQfaz+GKicExHRofHuJZYr4RIfiptvtRXl09Z9+ZlWGUUtO8Hmsh+N31V6wTFNTxuFAbk+lAifj5n2VlAj6sOGuLPhF+jLugsgEBgi17H7OH9Z1p9F7sHgYLk91Oun1fI4LqP1k2SpJXdosO59wzOnF8QjhDbPcYlglcGbSjw4lXeeClFYLK9R6KxteFSG6tEv7z3FFhqaPpRz+ttwMNI1kPhvFYzcD4pWUKSws9a4elSAXOJq5K7ZNWa/KzibQNaXl/IHdvwkEWkps4BS5qNUbPu5jNCYEEQGi59bVvPoCDwZZLQN5ZlcPqp3xxbbsNutFTtqvQ/9RRWojRw3qBnnlj1EgekI/mMFwmj818OK+aXUb6DWSCKXVAfIYd2Vs5ph4Z8ZvoPUqKAY+LiK5Z7LZM0P1vUSx+JPiGsEhw5Adpo+buLCE6l9+TYRwqBU/HGd7pKrOduK9qqdn7pN5xEMV4MDRkheKR/eDNzKhA4npFqfdVa8vDPrknVUdl/D43K9vazT4afWBwD4bgSVKgJ9LEtN8EnwKmIrSIB8bJGo0VR27oMlnCKKJjbkOGhCQO0rkOTNAmp1WLfypV/YuduuzGVkqrf7I5irfv7pQeFb60hWD/Zx6BHNQ6v/1j8zmkW9A2ynHRQY5AoU472x1tXY2d7ycLePYkWADB60Hc7elZnXZT3FwH7Gq5AFqATNUpjVoDhZFubNw7hESdccEkbfy3sHx5jnBBWGWgFQ3AYdiKeW1sh04iUL2qeLT44HFNXh5TbzuW6SD/6JbeuMZ/jrfiq1hYEU9/jweel3Ik0aryYgYf+meH+L+jJnET2zCZwoJnpBIQC+GjqABzkF/qZEjb1IS8oWJkzFzWQO5WSTTGoMFTizrp8wN231Ya+wYtYUjLFMgCYFCTqQmbFyvJRp55lDkwgd2MGWkSShBdpCAa26DYWUDIH24ynsLFvG/MbSDj17+jgjAEwnNXUsR/aVUoyn7Xeag5XnBygfFwGGAxxsEKd/CRs9BtXbQk4ciM3aFolYoVHEbzT8daoHNuLffXLID2yHxBaD2it05grRD5abmH3ATjVA8h0IdLNh5R4i3bFyqm8Q84PYBmqy2Lu7TBRXVx2RZvAWX5Ql/v02VdityhfNwR17POcmX6KLP+AEGB1fNUehr0eKcgWdWRqzsaJ9c3GPtz6Bq0hLCOJ0e2i4mIcIF6hMosbxQs6zPdoAkHnAOEU4uB6iFqv0WEsO2dXqn8PnZmqT6WbeSnbP36MUJ+PJn/FydJTsOdEbfZhOHE7cnlALRKNPHnhLca8oYmWHs1/sJ/DqBCALNlwTA3Mpjqm5kGOIlX8h290NBdmPswlCF4k/q1AVoqKkUpiW84fw2EI41qGLrUTn+UbyXBRFYPn1K8/iJcmHuBD1ljGhtAVOQ3hkXcJAKIrNYFba8nRohnsrOMs4HujLMf/euTB8mT8v5dcta9HCUVfNxcxx+H03591HczD9jL/UX4LGpxfmzWwwaTjRA74u41hSvIqZeAuzPtxBlhYZWEemfqldudeJhQATdnYhobJaU7y5kanbajT0nPb90c=" />


<script src="/Integrations/JQuery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/jquery-ui-1.11.4.min.js" type="text/javascript"></script>
<script src="/Integrations/Centralpoint/Resources/Controls/Page.js?v8.5.1" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

 //Design > Styles:  (#ReDesign) Scripts 
$(document).ready(function() {
	// Browser Less Than 1000px
	if ($(window).width() < 1000) {
		$('.left-rail-nav-container').insertAfter('.main-content');
		$('.module-results-intro-paragraph').insertBefore('.details-view-left-column');
		$('.inner-container ul:first-of-type').click(function(){
			$('.inner-container > ul:first-child > li > ul').slideToggle('slow');
			$('#search-expanded-container').insertAfter('#mobile-navigation-container');
		});
	}

	// Lowercase Services Menu Links
	$('nav.services-nav ul.cpsys_BlockList a').each(function() {
		var uRL = $(this).attr('href').toLowerCase();;
		$(this).attr('href', uRL);
	});
});
 //End of Design > Styles: Scripts 
//]]>
</script>

<script src="/Integrations/JQuery/Plugins/jquery.cp_Accordion.js?update=8.4.38" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yEk_YMl82vYnFaa5FD7acAOVmHN1OVAHW0odKyQQu9Q1bz3qnQXM6wMMgHgoP86fVuoAAaSWCN1in80BXnYa73tm4GfJTRouHqe9VVz8IN8t--lDrBHPNEDRfOXwvvzil1ZZgTlodaqVfkaxC99bGRQ1&amp;t=ffffffffd416f7fc" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="43343198" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="qG0c2Owpqnq1YbfYiJgD8mlZerYBXg4Myofc6ZB2ORHdGy46kvWJ/LnvAPd1Q2+6ZDELHfPnjo6jNZ6gDrvqjCMPVja7DVKWJsmyFp2eahKl/A+0XOVsOgYGuI39FQs5iIuMkhY3UeYPucef7WtFSmw5IQ8n2AvYNZWahLvr15E=" />
	
	<script> 
$(document).ready(function(){
    $("#discover-expand").click(function(){
        $("#discover-expanded-container").slideToggle("slow");
    });
    $("#search-expand").click(function(){
        $("#search-expanded-container").slideToggle("slow");
    });
    $("#mobile-navigation-expand").click(function(){
        $("#mobile-navigation-container").slideToggle("slow");
    });
    $("#mobile-search-expand").click(function(){
       $("#search-expanded-container" ).insertAfter( "#mobile-navigation-container" );  
        $("#search-expanded-container").slideToggle("slow");
    });

});
</script>

<style>
</style>
<header>
<!-------Top Nav------>
<nav class="top">
<div class="inner-container" id="blue-navigaiton-bar">
<ul>
    <li><a href="/Main/mclaren.aspx">About Us</a></li>
    <li id="health-care-professionals"><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li><a href="/main/patient-appointment.aspx" target="_blank" style="background: #0066a4;">Book an Appointment</a></li>
    
    
    <li><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a id="discover-expand" class="discover-mclaren-health-button" href="#">Discover McLaren Health</a></li>
    <li><img style="margin-left:13px; margin-top:13px;" src="/Uploads/Public/Images/Designs/nav-search-button.png" id="search-expand" alt="Search Button" title="Search Button"></li>
</ul>
<div class="clear"></div>
</div>
<!------------Search Box-------------->
<div id="search-expanded-container" style="display:none;">
<div class="inner-container">
<div class="Search-Input"><input style="" onkeypress="if (event.keyCode == '13') { document.location.href='/Main/Search.aspx?search=' + this.value; return false; }" placeholder="I'm looking for..." name="HtmlSearchCriteria" type="text"></div>
<div class="Search-Button"><input style="vertical-align: middle;" onclick="document.location.href='/Main/Search.aspx?search=' + document.forms[0].HtmlSearchCriteria.value;return false;" name="HtmlSearchGo" value="Go" src="/Uploads/Public/Images/Designs/nav-search-button.png" alt="Search Button" type="image"></div>
</div>
</div>
<!------------Search Box-------------->
<!------------Discover Box-------------->
<div id="discover-expanded-container" style="display:none;">
<div class="inner-container">
<ul class="discover-list">
    <li class="header">McLaren Services</li>
    <li><a href="/Main/bariatric-services.aspx">Bariatrics</a></li>
    <li><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li><a href="/Main/imaging-services.aspx">Diagnostic Imaging</a></li>
    <li><a href="/Main/birth-center-services.aspx">Family Birth Place</a></li>
    <li><a href="/mclaren-homecare-group/mclarenhomecaregroup.aspx">Home Care</a></li>
    <li><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></li>
    <li><a href="/mclaren-homecare-group/laboratory-services.aspx">Lab</a></li>
    <li><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li>
    <li><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></li>
    <li><a href="/Main/robotic-services.aspx">Robotic Surgery</a></li>
    <li><a href="/Main/surgery-services.aspx">Surgical Services</a></li>
    <li><a href="/Main/womens-services.aspx">Women’s Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">Primary Care &amp; Specialists</li>
    <li><a href="/Main/locations.aspx?taxonomy=Cardiology7">Cardiology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=FamilyMedicine4">Family Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=InternalMedicine8">Internal Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=MedicalCenters">Medical Centers</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Pediatrics11">Pediatrics</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Surgeons">Surgeons</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=UrgentCare9">Urgent Care</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Urology10">Urology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=WomensServices2">Women's Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">McLaren Hospitals</li>
    <li><a href="/BayRegion/BayRegion.aspx">McLaren Bay Region</a></li>
    <li><a href="/bayspecialcare/BaySpecialCare.aspx">McLaren Bay Special Care</a></li>
    <li><a href="/CentralMichigan/CentralMichigan.aspx">McLaren Central Michigan</a></li>
    <li><a href="/Flint/Flint.aspx">McLaren Flint</a></li>
    <li><a href="/Lansing/Lansing.aspx">McLaren Greater Lansing</a></li>
    <li><a href="/LapeerRegion/LapeerRegion.aspx">McLaren Lapeer Region</a></li>
    <li><a href="/macomb/macomb.aspx">McLaren Macomb</a></li>
    <li><a href="/northernmichigan/northernmichigan.aspx">McLaren Northern Michigan</a></li>
    <li><a href="/Oakland/Oakland.aspx">McLaren Oakland</a></li>
    <li><a href="/lansing/orthopedic-services.aspx">McLaren Orthopedic Hospital</a></li>
    <li><a href="/porthuron/porthuron.aspx">McLaren Port Huron</a></li>
</ul>
<ul class="discover-list" style="margin-right:0px;">
    <li class="header">McLaren Difference</li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a href="/mhp/mhp.aspx">Health Advantage</a></li>
    <li><a href="http://www.karmanos.org" target="_blank">Karmanos Cancer Institute</a></li>
    <li><a href="/Clarkston/Clarkston.aspx">McLaren Clarkston</a></li>
    <li><a href="/Main/Home.aspx">McLaren Health Care</a></li>
    <li><a href="/mhp/mhp.aspx">McLaren Health Plan</a></li>
    <li><a href="/aco/homepage.aspx">McLaren High Performance Network, LLC</a></li>
    <li><a href="/mclaren-homecare-group/mclarenhomecaregroup.aspx">McLaren Homecare Group</a></li>
    <li><a href="/mclaren-homecare-group/laboratory-services.aspx">McLaren Medical Laboratory</a></li>
    <li><a href="/mclaren-homecare-group/pharmacy-services.aspx">McLaren Pharmacy</a></li>
    <li><a href="/mclaren-physician-partners/mpp.aspx">McLaren Physician Partners</a></li>
    <li><a href="/Main/foundation.aspx">Our Foundations</a></li>
</ul>
<br style="clear:both;">
</div>
</div>
<!------------Discover Box-------------->
</nav>
<!-------Top Nav------>
<!-------Logo Row------>
<div class="inner-container">
<div class="logo"><a href="/main/home.aspx"><img src="/Uploads/Public/Images/header.jpg" alt="company logo links to website"/></a></div>
<div class="mobile-accordion-icons"><img style="margin-right:8px;" src="/Uploads/Public/Images/Designs/mobile-search-button.jpg" id="mobile-search-expand" alt="search button"><img style="" src="/Uploads/Public/Images/Designs/mobile-nav-button.jpg" id="mobile-navigation-expand" alt="mobile navigation expand button"></div>
<div class="second-nav">
<ul>
    <table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation73&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation73&#39;).hide();" id="cpPortableNavigation76a224fb-2841-4c3b-b9bc-237b847fccbe">
			<li id="services-nav-item" class="second-nav-services"><a href="#" onclick="return false;">Services</a></li>
		</div><div id="cpPortableNavigation73" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;width:600px;overflow:hidden;">
			<style>
    .featured-service-alpha .grey {color: #cccccc !important; font-weight:normal !important;}
    .featured-service-links{height:250px; width:750px;}
    .featured-service-links a{font-size:13px; color:#5f6062; padding:0px; margin:0px; font-family:'Alegreya Sans', sans-serif; text-decoration:none;}
    .featured-service-links a:hover{text-decoration:underline;}
    .featured-service-links p{margin:0px; padding:0px; line-height:20px;}
    .featured-service-alpha a:hover{text-decoration:underline;}
    .all-services-p{display:block !important;}
</style>
<div class="services-drop-down">
<div class="services-left-column">
<h5 style="padding-left:31px;">Featured Services</h5>
<!----Services of Excellence DataSource----->
<ul>
    <li class="services-of-excellence"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></li>
    <li class="services-of-excellence"><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li class="services-of-excellence"><a href="/Main/cardiac-care.aspx">Cardiology Services</a></li>
    <li class="services-of-excellence"><a href="/Main/emergency-services.aspx">Emergency Services</a></li>
    <li class="services-of-excellence"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></li>
    <li class="services-of-excellence"><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></li>
    <li class="services-of-excellence"><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></li>
    <li class="services-of-excellence"><a href="/Main/stroke-services.aspx">Stroke Services</a></li>
    <li class="services-of-excellence"><a href="/Main/trauma-services.aspx">Trauma Services</a></li>
    <li class="services-of-excellence"><a href="/Main/womens-services.aspx">Women's Care</a></li></ul>
<!---img style="" src="/Uploads/Public/Images/Designs/Main/services-drop-down-image.png"--->
</div>
<div class="services-right-column">
<h5>Our Services</h5>
<!----------------------->
<div class="featured-service-links">
<div class="featured-services-all">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/imaging-services.aspx">Imaging Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/podiatry-services.aspx">Podiatry Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/birth-center-services.aspx">Birthing Center</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/pulmonary-services.aspx">Pulmonary Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/blood-services.aspx">Blood Conservation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/infusion-services.aspx">Infusion</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/rehabilitation-services.aspx">Rehabilitation & Physical Therapy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/breast-care.aspx">Breast Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/laboratory-services.aspx">Laboratory</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cancer-care.aspx">Cancer Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/oakland/lake-orion-campus.aspx">Lake Orion Nursing and Rehabilitation</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/senior-services.aspx">Senior Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cardiac-care.aspx">Cardiology Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/marwood/homepage.aspx">Marwood Nursing and Rehabilitation</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/sleep-services.aspx">Sleep Center</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/bayspecialcare/bayspecialcare.aspx">McLaren Bay Special Care</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/spine-services.aspx">Spine, Neck and Back</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/dialysis-services.aspx">Dialysis</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/home-care-medical-supplies-mhg.aspx">Medical Supplies & Equipment</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/stroke-services.aspx">Stroke Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/emergency-services.aspx">Emergency Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/surgery-services.aspx">Surgery Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ems-services.aspx">EMS</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/trauma-services.aspx">Trauma Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ear-nose-throat-services.aspx">ENT: Ear Nose and Throat</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urgent-care.aspx">Urgent Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/eye-care.aspx">Eye Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/oakland/oxford-campus.aspx">Oxford Campus</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urology-services.aspx">Urology</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/main/foundation.aspx">Foundation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pain-center-services.aspx">Pain Center</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/womens-services.aspx">Women's Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/home-care.aspx">Home Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pediatric-services.aspx">Pediatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div></td>
	</tr>
</table>
</div>
<p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p><p><a href="/Main/fitness-and-health.aspx">Fitness</a></p><p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/mhg-home-care.aspx">Home Care</a></p><p><a href="/Main/mhg-home-infusion.aspx">Home Infusion</a></p><p><a href="/Main/mhg-hospice.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging</a></p><p><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p><p><a href="/Main/mhg-lab.aspx">Lab</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p><p><a href="/Main/pediatric-services.aspx">Pediatrics</a></p><p><a href="/Main/mhg-pharmacy.aspx">Pharmacy</a></p><p><a href="/Main/podiatry-services.aspx">Podiatry</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep</a></p><p><a href="/Main/spine-services.aspx">Spine</a></p><p><a href="/Main/stroke-services.aspx">Stroke</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p><p><a href="/Main/surgery-services.aspx">Surgery </a></p><p><a href="/Main/womens-services.aspx">Women's Services</a></p>
</div>
<div class="featured-service-alpha">
<a class="service-alpha-all" style="color:#0066a4 !important;" href="#">ALL</a>
<a class="service-alpha" href="#">A</a>
<a class="service-alpha" href="#">B</a>
<a class="service-alpha" href="#">C</a>
<a class="service-alpha" href="#">D</a>
<a class="service-alpha" href="#">E</a>
<a class="service-alpha" href="#">F</a>
<a class="service-alpha" href="#">G</a>
<a class="service-alpha" href="#">H</a>
<a class="service-alpha" href="#">I</a>
<a class="service-alpha" href="#">J</a>
<a class="service-alpha" href="#">K</a>
<a class="service-alpha" href="#">L</a>
<a class="service-alpha" href="#">M</a>
<a class="service-alpha" href="#">N</a>
<a class="service-alpha" href="#">O</a>
<a class="service-alpha" href="#">P</a>
<a class="service-alpha" href="#">Q</a>
<a class="service-alpha" href="#">R</a>
<a class="service-alpha" href="#">S</a>
<a class="service-alpha" href="#">T</a>
<a class="service-alpha" href="#">U</a>
<a class="service-alpha" href="#">V</a>
<a class="service-alpha" href="#">W</a>
<a class="service-alpha" href="#">X</a>
<a class="service-alpha" href="#">Y</a>
<a class="service-alpha" href="#">Z</a>
</div>
<!------------------------------>
</div>
<br style="clear:both;">
</div>
<script>

$(".service-alpha").click(function(){
    $(".featured-services-all").hide();
});
$(".service-alpha-all").click(function(){
    $(".featured-services-all").show();
});

$(document).ready(function() {
    function filterResults(letter){
        $('.featured-service-links p').hide();
        $('.featured-service-links p').filter(function() {
            return $(this).text().charAt(0).toUpperCase() === letter;
        }).show();
    };
    filterResults('B');
    $('a').on('click',function(){
        var letter = $(this).text();            
        filterResults(letter);        
    });
});

var $p =  $('.featured-service-links p')
$('a').addClass(function(){
    var s = this.textContent;
    return $p.filter(function(){
       return this.textContent.charAt(0) === s
           }).length ? '' : 'grey';
})

</script>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigationc1fdecc8-73a4-478e-b7f9-da0884005b89">
			<li class="find-a-physician"><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation85&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation85&#39;).hide();" id="cpPortableNavigation5bdbf3e4-64dc-413f-ab0c-dc4179e9a3e4">
			<li class="patient-and-visitor-info" id="patient-and-financial-info"><a href="#">Patient &amp; Financial Info</a></li>
<style>
    #patient-and-financial-info{width:120px !important;}
</style>
		</div><div id="cpPortableNavigation85" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/patient.aspx" target="_self">Patient Information</a><ul><li class="featured-service-style"><a href="/main/patient-opt-out.aspx" target="_self">Fundraising Opt-Out</a></li><li class="featured-service-style"><a href="/main/patient-mymclaren.aspx" target="_self">MyMcLaren Chart</a></li><li class="featured-service-style"><a href="/main/patient-notice-privacy.aspx" target="_self">Notice of Privacy Practices</a></li><li class="featured-service-style"><a href="/main/patient-donation.aspx" target="_blank">Online Donation</a></li><li class="featured-service-style"><a href="/main/patient-pre-registration.aspx" target="_self">Patient Pre-Registration</a></li><li class="featured-service-style"><a href="/main/patient-appointment.aspx" target="_self">Schedule Appointments</a></li><li class="featured-service-style"><a href="/main/patient-thank-doctor.aspx" target="_self">Thank Your Doctor</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/financial.aspx" target="_self">Financial Information</a><ul><li class="featured-service-style"><a href="/main/financial-billing1.aspx" target="_self"> Pay Your Bill Online</a></li><li class="featured-service-style"><a href="/main/financial-services.aspx" target="_self">Financial Assistance</a></li><li class="featured-service-style"><a href="/main/financial-insurance.aspx" target="_self">Insurance Information</a></li><li class="featured-service-style"><a href="/main/financial-participating-plans.aspx" target="_self">Participating Health Plans</a></li><li class="featured-service-style"><a href="/main/financial-insurance-veterans.aspx" target="_self">Veterans Choice Insurance</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/visitor.aspx" target="_self">Visitor Information </a><ul></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation77&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation77&#39;).hide();" id="cpPortableNavigationbf4fa1a8-3c75-4ea8-97c5-67f9504e6f30">
			<li class="health-and-wellness"><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
		</div><div id="cpPortableNavigation77" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-your-health.aspx" target="_self">Your Health</a><ul><li class="featured-service-style"><a href="/main/healht-clinical-trials.aspx" target="_self">Clinical Trials</a></li><li class="featured-service-style"><a href="/main/health-community-needs.aspx" target="_self">Community Health Needs Assessment</a></li><li class="featured-service-style"><a href="/main/health-information.aspx" target="_self">Health Information</a></li><li class="featured-service-style"><a href="/main/health-navigator.aspx" target="_self">Health Navigator</a></li><li class="featured-service-style"><a href="/main/health-infection-prevention.aspx" target="_self">Infection Prevention</a></li><li class="featured-service-style"><a href="/main/health-my-checkups.aspx" target="_self">My Checkups Health Tool</a></li><li class="featured-service-style"><a href="/main/health-mymclaren-chart.aspx" target="_self">My McLaren Chart</a></li><li class="featured-service-style"><a href="/main/health-patient-education.aspx" target="_self">Patient Education</a></li><li class="featured-service-style"><a href="/main/health-wellness-tools.aspx" target="_self">Wellness Tools</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-classes-programs.aspx" target="_self">Classes & Programs</a><ul><li class="featured-service-style"><a href="/main/health-classes.aspx" target="_self">Classes and Events</a></li><li class="featured-service-style"><a href="/main/health-support-groups.aspx" target="_self">Support Groups</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-mclarenhealthplan.aspx" target="_self">McLaren Health Plan</a><ul></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigation52587aa1-b090-4537-9c98-40261a490ab3">
			<li class="our-facilities"><a href="/Main/locations.aspx">Our Facilities</a></li>
		</div></td>
	</tr>
</table>
</ul>
</div>
<div style="clear:both;"></div>
</div>
<!-------Logo Row------>
</header>
<div id="mobile-navigation-container" style="display:none;">
<ul class="mobile-menu">
    <li>
    <div class="mobile-navigation-accordion">Services</div>
    <div class="mobile-navigation-content"><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/birth-center-services.aspx">Birthing Center</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology Services</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Services</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/ear-nose-throat-services.aspx">ENT: Ear Nose and Throat</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/main/foundation.aspx">Foundation</a></p><p><a href="/mclaren-homecare-group/home-care.aspx">Home Care</a></p><p><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging Services</a></p><p><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p><p><a href="/mclaren-homecare-group/infusion-services.aspx">Infusion</a></p><p><a href="/mclaren-homecare-group/laboratory-services.aspx">Laboratory</a></p><p><a href="/oakland/lake-orion-campus.aspx">Lake Orion Nursing and Rehabilitation</a></p><p><a href="/marwood/homepage.aspx">Marwood Nursing and Rehabilitation</a></p><p><a href="/bayspecialcare/bayspecialcare.aspx">McLaren Bay Special Care</a></p><p><a href="/mclaren-homecare-group/home-care-medical-supplies-mhg.aspx">Medical Supplies & Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></p><p><a href="/oakland/oxford-campus.aspx">Oxford Campus</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatric Services</a></p><p><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/podiatry-services.aspx">Podiatry Services</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary Services</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehabilitation & Physical Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep Center</a></p><p><a href="/Main/spine-services.aspx">Spine, Neck and Back</a></p><p><a href="/Main/stroke-services.aspx">Stroke Services</a></p><p><a href="/Main/surgery-services.aspx">Surgery Services</a></p><p><a href="/Main/trauma-services.aspx">Trauma Services</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/womens-services.aspx">Women's Care</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div>
    </li>
    <li><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
    <li>
    <div class="mobile-navigation-accordion">Patient &amp; Visitor Info</div>
    <div class="mobile-navigation-content">
    <p><a href="/Main/patient.aspx">Patient Information</a></p>
    <p><a href="/Main/Financial.aspx">Financial Information</a></p>
    <p class="mobile-navigation-link-visitor"><a href="/Main/Visitor.aspx">Visitor Information</a></p>
    </div>
    </li>
    <li><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
    <li><a href="/Main/locations.aspx">Our Facilities</a></li>
    <li><a href="/Main/foundation.aspx">Ways To Give</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren.aspx">About Us</a></li>
    <li id="health-care-professionals" class="top-nav-mobile"><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li class="top-nav-mobile" ><a href="/main/patient-appointment.aspx">Book an Appointment</a></li>
    
    <li class="top-nav-mobile"><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li class="top-nav-mobile"><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li class="top-nav-mobile"><a href="/Main/career.aspx">Careers</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
</ul>
</div>
	
	<div class="cpweb_PerimeterMiddle">
		<div id="blPerimiter" class="cpsys_Block">
	
			
			<div id="tdPerimeterCenter" class="cpsys_BlockColumn">
		<div id="divWrapper" class="cpweb_Wrapper">
	<div id="cphBody_divTop" class="cpsty_Top">
		
		
		<div id="cphBody_divTopAc2" class="cpsty_SiteTypes_Default_TopAc2"><div id="cpsys_Advertisers_af343aee-a245-4e1d-9da4-6d08bbbd30c5" style="text-align:left;">
	<div class="breadcrumb-trail"><div class="Breadcrumb"><span><a href="/main/home.aspx" target="_self">Home</a> &gt; </span><span><span>Website Privacy Policy</span></span></div></div>
</div></div>
	</div>
	<div style="clear:both;">
		<div id="cphBody_blSiteType" class="cpsys_Block cpsty_blSiteType">
			
			<div id="cphBody_tdLeft" class="cpsys_BlockColumn cpsty_LeftTd">
				
				<div class="cpsty_Left">
					
					<div id="cphBody_divLeftNav" class="cpsty_SiteTypes_Default_LeftNav"></div>
					
					
				</div>
			
			</div>
			
			<div id="cphBody_tdCenter" class="cpsys_BlockColumn cpsty_CenterTd" style="width: 98%;">
				
				<div id="cphBody_divCenter" class="cpsty_Center">
					<div id="cphBody_divCenterAc1" class="cpsty_SiteTypes_Default_CenterAc1"><div id="cpsys_Advertisers_bf3115e5-4271-4bad-91ed-a4aa140e412a" style="text-align:left;">
	<style>
    #data-source-created{display:none;}
    #service-line-datasource{display:none;}
</style>
<div class="left-rail-nav-container">
<ul parentsystemtype="FirstTierList" id="site-nav"><li class="liParent"><a href="/main/mclaren.aspx" target="_self" class="d173dbee-de0f-4132-ba5e-76a09f076348">About Us</a><ul><li class="liParent"><a href="/main/mclaren-awards.aspx" target="_self" class="64b1e7ee-84b2-4587-afe5-5d74ca900868">Award Winning Care</a></li><li class="liParent"><a href="/main/mclaren-community-assessment.aspx" target="_self" class="f0856316-de0f-442c-8207-78ea6ee5b5ce">Community Health Needs Assessment</a></li><li class="liParent"><a href="/main/mclaren-contact-us.aspx" target="_blank" class="25423667-c62c-4a08-89f5-0296d2ca22f8">Contact Us</a></li><li class="liParent"><a href="/main/mclaren-history.aspx" target="_self" class="6c6781bf-645e-4190-9b15-b1df37f62521">History</a><ul><li class="liParent"><a href="/main/mclaren-br-history.aspx" target="_self" class="bff09b68-e652-41dd-8b45-a963dda7c15d">McLaren Bay Region</a></li><li class="liParent"><a href="/main/mclaren-cm-history.aspx" target="_self" class="023cada0-e4d5-4d9b-9da1-81c3b5ca94b6">McLaren Central Michigan</a></li><li class="liParent"><a href="/main/mclaren-clarkston.aspx" target="_self" class="eb43ae8e-0657-4790-80c0-4f97ab08bd54">McLaren Clarkston</a></li><li class="liParent"><a href="/main/mclaren-flint-history.aspx" target="_self" class="d8639099-ce6a-4ead-9139-a59962d5d3db">McLaren Flint History</a></li><li class="liParent"><a href="/main/mclaren-gl-history.aspx" target="_self" class="1a368f0f-f80d-41d5-9f49-d8227cb5b09d">McLaren Greater Lansing</a></li><li class="liParent"><a href="/main/mclaren-hg.aspx" target="_self" class="9177539c-3814-4aa1-b948-718e538bed6b">McLaren Homecare Group</a></li><li class="liParent"><a href="/main/mclaren-lr-hisotry.aspx" target="_self" class="592f9725-eeb8-403c-b5cd-40d24c6ff7fa">McLaren Lapeer Region</a></li><li class="liParent"><a href="/main/mclaren-macomb-history.aspx" target="_self" class="787adc33-1be5-4654-a1e1-b00f7748edb4">McLaren Macomb</a></li><li class="liParent"><a href="/main/mclaren-nm-history.aspx" target="_self" class="f8821150-597e-48dc-b092-7853def059fb">McLaren Northern Michigan</a></li><li class="liParent"><a href="/main/mclaren-oakland.aspx" target="_self" class="1f97c3e8-205c-45f2-899f-cdd5dcabed17">McLaren Oakland</a></li></ul></li><li class="liParent"><a href="/main/mclaren-blog.aspx" target="_self" class="79a09704-9964-497f-b21c-29a720645b6f">Blog</a></li><li class="liParent"><a href="/main/mclaren-executives.aspx" target="_self" class="84efe6f0-cf94-4b18-9e0b-9ce8374387c1">Meet the Executive Team</a></li><li class="liParent"><a href="/main/mclaren-message.aspx" target="_self" class="d97677db-b368-4c2d-b08a-a3c7a2a3a306">Message to Our Community</a></li><li class="liParent"><a href="/main/mclaren-mission.aspx" target="_self" class="212a2d08-6f76-49f5-b5f0-9b2ad17f88d8">Mission Statement</a></li><li class="liParent"><a href="/main/mclaren-publications.aspx" target="_self" class="a424848b-5c16-49fe-ba74-05c565299104">Publications and Videos</a></li><li class="liParent"><a href="/main/mclaren-research.aspx" target="_self" class="8c52cd2f-a2bd-40fe-bff6-a7fd50955fff">Research Studies</a></li><li class="liParent"><a href="/main/mclaren-service-area.aspx" target="_self" class="df08959d-e863-4e26-a7b3-e13754ca9049">Service Area  </a></li><li class="liParent"><a href="/main/mclaren-community-health-centers.aspx" target="_self" class="a0e1b844-af54-40f5-9e8d-9bcecc9eefec">Community Health Centers</a></li></ul></li></ul>
</div>
</div></div>
					
					<div id="cphBody_divContent" class="cpsty_SiteTypes_Default_Content">
	
	
			
<!--cpsys_Template:cpsys_Register-->
<!--cpsys_Template:cpsys_Register-->


<!--cpsys_Template:DetailsHeaderContent-->
<div class="left-rail-nav-container" id="service-line-datasource">

</div>
<div class="main-content">
<!---Shows Location Info--->

<style> 
/*----Styles For SubPages-----*/
.cpsty_CenterTd{width:100% !important;}.cpsty_Center h1{margin-top:0px;}.site-banner .inner-container{min-height:115px;}.site-banner .inner-container h1{line-height:115px; font-size:34px;}.site-banner .inner-container h1.multi-line{line-height:normal !important; font-size:34px !important; height:100px !important;}.site-banner{height:173px; background-color:#2f82b6; background-image:none;} .site-banner .inner-container .site-banner-logo{display:none} .service-location-on-landing-page-top{display:none;} .service-location-on-landing-page{display:none;} .left-rail-nav-container{display:block; width:24.5% !important; margin-right:2%; float:left; border:solid 1px #cccccc;} .main-content{width:72.5%; float:left;} @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}
/*----Styles For SubPages-----*/
</style>

<h1 class="pageheader">Website Privacy Policy</h1>
<div class="service-location-on-landing-page-top"></div>
<p>McLaren Health Care Corporation cares about privacy issues and wants you to be familiar with how we collect, use and disclose Personally Identifiable Information you provide to us.</p>
<p>This Privacy Policy and Terms (the “Policy”) describes our practices in connection with Personally Identifiable Information that we collect through our website located at www.mclaren.org, the websites of our subsidiaries that display or directly link to this Policy, and the e-mail messages that we send to you that directly link to this Policy (collectively, the “Services”).</p>
<p>There are a number of circumstances where this Policy does not apply and we want to identify a few of them for you:</p>
<ul>
    <li>This Policy does not apply to the information you share when receiving treatment from your medical provider, including when you are receiving medical services from us. For example, this Policy would not apply to the information you share with us when admitted to the emergency room. The use and disclosure of the information you provide in such circumstances is governed by the Federal Health Insurance Portability and Accountability Act of 1996, more commonly known as HIPAA, as well as Michigan law. You can learn more about your HIPAA rights and protections and our obligations in our Notice of Privacy Practices.</li>
    <li>This Policy does not apply to the information you share and your use of the <em>My McLaren Chart</em>, which is the patient portal we offer. The use and disclosure of the information you provide when using the <em>My McLaren Chart</em> is governed by HIPAA, Michigan state law, as well as the terms and conditions and other policies you agreed to when registering to access the <em>My McLaren Chart</em> service.</li>
    <li>This Policy does not apply to the practices of companies that we do not own or control. Other services and organizations that we link to may have their own privacy policies governing the use of information you provide to them. For example, organizations such as Facebook, Twitter and LinkedIn each have their own privacy policy and practices.</li>
    <li>This Privacy Policy does not apply to individuals whom we do not employ, including any of the third parties to whom we may disclose user information as set forth in this Privacy Policy.</li>
</ul>
<p>If you have any questions or comments about this Policy or our privacy practices, please contact us by using one of the means listed on our <a href="/main/mclaren-contact-us.aspx">Contact page</a></p>
<ol>
    <li><strong>Personally Identifiable Information We May Collect</strong>
    <p>“Personally Identifiable Information” is information that identifies you as an individual, such as name, birth date, telephone number, e-mail address, or unique device identifiers that was gathered in connection with your use of the Services.</p>
    <p>If we combine Personally Identifiable Information with protected health information subject to protection under HIPAA, the combined information will be treated as protected health information for as long as it remains combined.</p>
    </li>
    <li><strong>Other Information we may collect</strong>
    <p>“Other Information” is any information that does not reveal your specific identity or does not directly relate to an individual, including, for example: (1) computer or device connection information, such as browser type and version, operating system type and version, device information, and other technical identifiers; (2) information collected through cookies and other technologies; or (3) aggregated information, such as usage history and search history.</p>
    <p>If we combine Other Information with Personally Identifiable Information, the combined information will be treated as Personally Identifiable Information for as long as it remains combined.</p>
    </li>
    <li><strong>Security Measures</strong>
    <p><strong>Virus Detection and E-mail Security:</strong>&nbsp;For website security, we use software programs to monitor traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage.&nbsp; </p>
    <p><strong>General Practices: </strong>Although we seek to use reasonable measures to protect Personally Identifiable Information, please be aware that no security measures are perfect or impenetrable. Therefore, we cannot and do not guarantee that the information you provide to us through the Services&nbsp;will not be viewed by unauthorized persons. We are not responsible for circumvention of any privacy settings or security measures contained on the Services.</p>
    <p><strong>Security of Service Communications</strong>: Some communication through the Services may be sent through the standard HTTP protocol and may be delivered using regular e-mail. Information sent over HTTP is not encrypted. E-mail, while convenient, also poses several risks (e.g., e-mail is not a secure form of communication, is unreliable, can be forwarded, etc.). We cannot guarantee the security of the information sent through such means, nor can we guarantee that information you supply to us will not be intercepted while being transmitted to us. It is important for you to protect against unauthorized access to your computer and to take appropriate security measures to protect your information. We use encryption technology, such as Secure Sockets Layer (SSL), to protect your protected health information during data transport with <em>My McLaren Chart</em>.</p>
    <p><strong>Your Obligations to Safeguard your Information</strong>: Security is not a one person job. You must also take reasonable measures to protect your information, including, for example, securing your computer and mobile device, using an antivirus software, using a firewall, and other similar safeguards.</p>
    </li>
    <li><strong>Disclaimer</strong>
    <p>McLaren Health Care Corporation and its subsidiaries offer information on the Services for general educational purposes only. This information should not be used for diagnosis and treatment, nor should it be considered a replacement for counsel with your physician or other health care professional. If you have questions or concerns about your health, contact your health care provider. We encourage you to use the Physician Finder to locate a physician by specialty and area.</p>
    <p>While we and our subsidiaries make reasonable effort to ensure accuracy of the information on the Services, we do not guarantee the accuracy, and the information is provided with no warranty or guarantee of any kind.</p>
    </li>
    <li><strong>Links</strong>
    <p>We and our subsidiaries provide links to other websites. These links are provided as a convenience to you and as an additional avenue of access to the information contained on such third-party websites. Different terms and conditions may apply to your use of any linked sites. We encourage you to read the privacy policy of each website.&nbsp; We have no control over third party websites and make no claim or representation regarding such websites. We accept no responsibility for, the quality, content, nature, or reliability of any websites accessible by hyperlink from the Services, or websites linking to the Services.&nbsp; We are not responsible for any losses, damages or other liabilities incurred as a result of your use of any linked sites.</p>
    </li>
    <li><strong>Corrections, unsubscribe and data retention</strong>
    <p>You may update the information you provide to us through the Services. To change your information, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>. If you are subscribed to one of our newsletter, you may unsubscribe from receiving the newsletters by clicking on the appropriate link at the bottom of the e-mail. We may, from time to time, send you administrative messages. You cannot opt-out from receiving administrative messages.</p>
    <p>We will retain your information for as long as needed to provide you services, comply with our legal obligations, resolve disputes, and enforce our agreements. Except for authorized law enforcement investigations, other valid legal processes or as described in this Policy (or another policy in place between you and us), we will not share any Personally Identifiable Information we receive from you with any parties outside of McLaren and its subsidiaries.</p>
    </li>
    <li><strong>Changes to the Privacy Policy and Terms</strong>
    <p>We may change this Policy from time to time. Please take a look at the “Last Modified” legend at the top of this page to see when this Policy was last revised. Any material changes to this Policy will become effective 7 days from when we post the revised Policy on the Services and all other changes to this Policy will become effective when we post the revised Policy on the Services. Your use of the Services following the effective date means that you accept the revised Policy. If you have questions or comments about this Policy, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>.</p>
    <p><a href="/main/foundation.aspx"><strong>McLaren Foundations:</strong></a>&nbsp; Some subsidiaries uses Internet donors' technology provided by Blackbaud, Inc. This is encrypted technology and allows financial donations to be forwarded electronically using a secure server.</p>
    <p>McLaren Health Care Corporation and its subsidiaries use oxcyon.com as a host server and vendor, the content management system is Central Point.</p>
    </li>
    <li><strong>Use of Services by Minors</strong>
    <p>We do not knowingly collect Personally Identifiable Information from individuals under the age of 13 and the Services are not directed to individuals under the age of 13. We request that these individuals not to use the Services.</p>
    </li>
    <li><strong>International Visitors</strong>
    <p>The Services are controlled and operated from the United States, and are not intended to subject us to the laws or jurisdiction of any state, country or territory other than that of the United States. If any material on the Services, is contrary to the laws of the place where you are when you access them, then we ask you not to use the Services. You are responsible for informing yourself of the laws of your jurisdiction and complying with them. By using the Services, you consent to the transfer of information to the United States, which may have different data protection rules than those of your country.</p>
    </li>
    <li><strong>Use of Materials on the Services, Trademarks and Copyrights </strong>
    <p>You acknowledge and agree that all content on the Services (including, without limitation, text, images, user interfaces, visual interfaces, graphics, trademarks, logos, sounds, source code and computer code, including but not limited to the design, structure, selection, coordination, expression, ‘look and feel’ and arrangement thereof) is the exclusive property of and owned by us or our licensors and is protected by copyright, trademark, trade dress and various other intellectual property rights and unfair competition laws. These marks and copyrights may not be copied, imitated, or used, in whole or in part, without the express prior written permission from their respective owners, and then with the proper acknowledgments. Nothing on the Services shall be construed as granting, by implication, estoppel, or otherwise, any license or right to use any trademark, logo or service mark displayed on the Services without the owner’s prior written permission, except as otherwise described herein.</p>
    <p>You may access, copy, download, and print the material (such as, for example, educational materials, service descriptions, and similar materials) purposely made available by us for downloading from the non-secured component of the Website (“General Website”) for your personal, non-commercial use and for your business use in connection with evaluating McLaren healthcare provider services for offer by your health plan or use by your employees, provided you do not (i) modify or delete (including through selectively copying or printing material) any copyright, trademark, or other proprietary notice that appears on the material, and (ii) make any additional representations or warranties relating to such materials.</p>
    <p>You may access, copy, and print the material contained in the secured component of the Website in accordance with the terms and conditions governing the secure section.</p>
    <p>Any other use of content or material on the Services, including but not limited to the modification, distribution, transmission, performance, broadcast, publication, uploading, licensing, reverse engineering, encoding, transfer or sale of, or the creation of derivative works from, any material, information, software, documentation, products or services obtained from the Services, or use of the Services is expressly prohibited.</p>
    <p>We, our licensors, or content providers, retain full and complete title to any and all materials provided on the Services, including any and all associated intellectual property rights. </p>
    <p>As long as you comply with this Policy, we grant you a personal, non-exclusive, non-transferable, revocable, limited privilege to enter and use the Services. We reserve the right, without notice and in our sole discretion, to terminate your license to use the Services and to block or prevent future access to and use of the Services.</p>
    </li>
    <li><strong>Submissions and Postings</strong>
    <p>To the extent that we allow submissions on the Services, you acknowledge that you are responsible for any material you may submit via the Services, including the copyright, legality, reliability, appropriateness, and originality of any such material.</p>
    <p>You represent and warrant (and we rely on your representation and warranty) that you (i) own or otherwise control all the rights or have sufficient rights to the content you post or that such items are known to you to be in the public domain; (ii) that the content is accurate; (iii) that use of the content you supply does not violate any provision in this Policy or terms you may have agreed to with a third party; (iv) that the content is not defamatory or otherwise trade libelous; (v) does not violate any law, statute, ordinance or regulation; and (vi) that you will indemnify us for all claims resulting from content you supply, including arising from an action alleging infringement of copyright or other proprietary rights in such work.</p>
    <p>We undertake no duty to determine the validity of any claim of copyright or trademark infringement. Upon receiving written notice that any item posted on the Services is believed to infringe a copyright or other proprietary right, we will remove said work.</p>
    <p>If you do submit material, you grant us and our affiliates an unrestricted, nonexclusive, royalty-free, perpetual, irrevocable, transferable and fully sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute and display any and all material not subject to protections under HIPAA throughout the world in any media. You further agree that we are free to use without limitation and without any compensation to you any ideas, concepts, or know-how that you or individuals acting on your behalf provide to us. You grant us the right to use the name you submit in connection with such material.&nbsp; We retain any and all rights granted in this Policy in and to any user submitted content or non-HIPAA materials after termination, notwithstanding the reason for any such termination.</p>
    <p>We have an absolute right to remove any material from the Services in our sole discretion at any time.</p>
    </li>
    <li><strong>Usage Rules</strong>
    <p>You hereby agree to not upload, distribute, or otherwise publish through the Services any content that (i) is unlawful, libelous, defamatory, obscene, pornographic, harassing, threatening, invasive of privacy or publicity rights, fraudulent, defamatory, abusive, inflammatory, or otherwise objectionable; (ii) is confidential, proprietary, incorrect, or infringing on intellectual property rights; (iii) may constitute or encourage a criminal offense, violate the rights of any party or otherwise give rise to liability or violate any law; or (iv) may contain software viruses, chain letters, mass mailings, or any form of “spam.”&nbsp; You may not use a false email address or other identifying information, impersonate any person or entity or otherwise mislead as to the origin of any content.&nbsp; You may not upload commercial content onto our Services.</p>
    <p>You expressly agree to refrain from doing either personally or through an agent, any of the following:&nbsp; (1) use any device or other means to harvest information; (2) transmit, install, upload or otherwise transfer any virus or other item or process to the Services that in any way affects the use, enjoyment or service of the Services, or any visitor’s computer or other medium used to access the Services; (3) engage in any action which we determine in our sole discretion is detrimental to the use and enjoyment of the Services; or (4) transmit, install, upload, post or otherwise transfer any information in violation of the laws of the United States. You may further not use any hardware or software intended to damage or interfere with the proper working of the Services or to surreptitiously intercept any system, data, or personal information from the Services. You agree not to interrupt or attempt to interrupt the operation of the Services in any way.</p>
    </li>
    <li><strong>Infringement Notice</strong>
    <p>We respect the intellectual property rights of others and request that you do the same. If you believe your copyright or the copyright of a person on whose behalf you are authorized to act has been infringed, you may notify us in writing to the e-mail address or mailing address provided in the “How to Contact the Web Team” section below with attention to Copyright Agent. To be effective, your notification must be in writing, include your contact information, provided to our copyright agent, and include:&nbsp; (i) signature of a person authorized to act; (ii) identification of the copyrighted work claimed to have been infringed; and (ii) identification of the material that is claimed to be infringing including references to the location of the material on the Services.</p>
    <p>If you believe other intellectual property rights were violated, you may notify us in writing to the mailing address provided in the “How to Contact the Web Team” section below with attention to General Counsel.</p>
    </li>
    <li><strong>Jurisdiction and Applicable Law</strong>
    <p><strong>The laws of the State of Michigan, </strong>without regard to any conflicts of laws principles thereof,<strong> will govern the construction and interpretation of this Policy and the rights of the parties hereunder. By accessing, using, or registering for the Services, you acknowledge that you have read, understood, and agreed to be bound by this Policy and by all applicable laws and regulations. </strong>The Parties agree on behalf of themselves and any person claiming by or through them that the exclusive jurisdiction and venue for any action or proceeding arising out of or relating to this Agreement will be an appropriate state or federal court located in Michigan and each party irrevocably waives, to the fullest extent allowed by applicable law, the defense of an inconvenient forum.<strong></strong></p>
    </li>
    <li><strong>Severability and Waiver</strong>
    <p>Our failure to exercise or enforce any right or provision of this Policy will not constitute a waiver of such right or provision. If any provision of this Policy is unlawful, void, or unenforceable, for any reason, the remaining provisions will remain in full force and effect to the fullest extent of the law.<strong></strong></p>
    </li>
    <li><strong>How to Contact the Web Team</strong> E-mail:&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong> Fax:&nbsp;(989) 891-8185</li>
</ol>
<ul>
    <li><strong>We respect your privacy and do not collect Personally Identifiable Information through the Services unless you choose to provide it. We, and our service providers, may collect Personally Identifiable Information in a variety of ways, including when</strong>:
    <ul>
        <li><a>You contact us to request information by sending us an </a><a href="mailto:Webmaster@mclaren.org">e-mail</a>&nbsp;; </li>
        <li>You participate in one of our training or education events, such as a childbirth education, nutrition education or exercise &nbsp;training seminars;</li>
        <li>You request one of our publications or newsletters;</li>
        <li>You participate in a Service related survey, contest, or other promotion; or</li>
        <li>You complete a questionnaire(s) on the Services (<em>e.g.</em>, did you find our website helpful?)</li>
    </ul>
    </li>
    <li><strong>We may use personally identifiable information</strong>:
    <ul>
        <li>To send administrative information, such as information regarding the Services and changes to our terms, conditions, or policies;</li>
        <li>To respond to your inquiries and fulfill your requests;</li>
        <li>If you enter into a contest or similar promotion we may use the information you provide to administer those programs;</li>
        <li>We may use survey information for research and quality improvement purposes, including helping us to improve information and services offered through the Services;</li>
        <li>For our business purposes, such as improving or modifying our Services, identifying usage trends, and operating and expanding our service and information offerings;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>Your Personally Identifiable Information may be disclosed</strong>:
    <ul>
        <li>To identify you to anyone to whom you send messages through the Services;</li>
        <li>To our third-party service providers that provide services such as website hosting, information technology and related infrastructure, customer service, and other similar services;</li>
        <li>To a third-party in the event of any reorganization, merger, sale, joint venture, assignment, transfer or other disposition of all or any portion of our business or assets;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>California Do Not Track Notice</strong>:
    <ul>
        <li>We do not track you over time and across third party websites to provide targeted advertising and therefore do not respond to Do Not Track (DNT) signals;</li>
        <li>Third parties that have content embedded on our website, such as a social networking connectors (<em>e.g.</em>, Facebook, Twitter) set cookies in your browser as well as obtain information about the fact that a web browser visited the Services from a certain IP address.</li>
    </ul>
    </li>
</ul>
<ul>
    <li><strong>How We May Collect Other Information</strong>: We, and our third-party service providers, may collect Other Information in a variety of ways, including:
    <ul>
        <li><em>Through your browser or mobile device</em>: Certain information is automatically collected by most browsers or through your mobile device, such as your computer type, screen resolution, operating system name and version, device manufacturer and model, language, and Internet browser type and version. We may also collect information on the search terms you used to find our website, the search engine you used, or the address of the web site from which you came to visit.</li>
        <li><em>Using cookies</em>: Our Services may use cookies and other technologies such as pixel tags and web beacons. These technologies help us provide better services to you, tell us which parts of our Services people have visited, and allow us to better measure the usability of our Services. We treat information collected by cookies and other technologies as non-personal information. You have a variety of tools to control cookies and similar technologies, including controls in your browser to block and delete cookies.</li>
        <li><em>IP Address</em>:&nbsp; Your “IP Address” is a number that is automatically assigned to the computer that you are using by your Internet service provider (ISP). An IP Address may be identified and logged automatically in our server log files, or those of our website hosting vendor, whenever a user accesses the Services, along with the time of the visit and the page(s) that were visited.</li>
        <li><em>By aggregating information</em>:&nbsp; Aggregated Personally Identifiable Information does not personally identify you or any other user of the Services. We may aggregate information for a variety of reasons, for example, to calculate the percentage of our users who visit a particular page or clicked on a particular item from a newsletter.</li>
        <li><em>Google Analytics</em>: We use a service from Google called “Google Analytics” to collect information about use of the Services. Google Analytics collects information such as how often users visit this site, what pages they visit when they do so, and what other sites they used prior to coming to this site. Google Analytics collects only the IP address assigned to you on the date you visit this site, rather than your name or other identifying information. Google’s ability to use and share information collected by Google Analytics about your visits to our Services is restricted by the Google Analytics Terms of Use and the Google Privacy Policy. We may use the information from Google Analytics for a variety of reasons including trend analysis and to make our Services more useful to the communities we serve.</li>
    </ul>
    </li>
    <li><strong>How We May Use and Disclose Other Information</strong>
    <ul>
        <li>We may use and disclose Other Information for any purpose, except where we are required to do otherwise under applicable law.</li>
        <li>If we are required to treat Other Information, such as IP addresses or other similar identifiers, as Personally Identifiable Information under applicable law, then we may use it as described in “How We May Collect Other Information” section above, as well as for all the purposes for which we use and disclose Personally Identifiable Information, but we will treat these identifiers as Personally Identifiable Information. If we are require to treat Other Information as protected health information as defined by HIPAA, then we will treat the identifiers in accordance with our Notice of Privacy Practices.</li>
    </ul>
    </li>
</ul>
<p><strong>Mail:</strong><strong><br>
</strong>McLaren Bay Region<br>
Corporate WebMaster<br>
1900 Columbus Avenue<br>
Bay City, MI 48708</p>
Last modified:  5/2/16
<!-----Related Links------>


<div class="subpage-info">
<!----Related Photo Gallery---->
<!----Related Documents---->
<!----Related Videos------->
<!----Related Links-------->
<!----Related Blogs-------->
</div>
</div>
<!-----------------Find Here----------------->
<!--cpsys_Template:DetailsHeaderContent-->


		
	

<!--cpsys_Template:DetailsFooterContent-->

<!--cpsys_Template:DetailsFooterContent-->


		
	


</div>
					
				</div>
			
			</div>
			<div id="cphBody_tdRight" class="cpsys_BlockColumn cpsty_RightTd">
				
				<div class="cpsty_Right">
					
					
					
				</div>
			
			</div>
		
		</div>
	</div>
	
</div>
	</div>
			
		
</div>
    </div>
    
	<footer>
<div class="footer-social-outer-container">
<div class="footer-social-inner-container">
<ul>
    <li><a href="https://www.facebook.com/McLarenHealth"><img style="" alt="facebook icon" src="/Uploads/Public/Images/Designs/footer-facebook.gif"></a></li>
    <li><a href="https://twitter.com/mclarenhealth"><img style="" alt="twitter icon" src="/Uploads/Public/Images/Designs/footer-twitter.gif"></a></li>
    <li><a href="https://www.linkedin.com/company/mclaren-health-care"><img style="" alt="linkedin icon" src="/Uploads/Public/Images/Designs/footer-linkedin.gif"></a></li>
    <li><a href="https://www.youtube.com/user/mclarenhealth"><img style="" alt="youtube icon" src="/Uploads/Public/Images/Designs/footer-social-red.gif"></a></li>
    
    
</ul>
</div>
</div>
<div class="outer-container">
<div class="inner-container mobile-hide">
<nav class="btm">
<div class="col-1">
<ul>
    <li><a href="/Main/Career.aspx">Careers</a></li>
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a href="/Main/mclaren-web-privacy-policy.aspx">Website Privacy Policy</a></li>
    <li><a href="/Main/health-community-needs.aspx">Community Health Needs Assessment</a></li>
    <li><a href="/Main/Sitemap.aspx">Site Map</a></li>
</ul>
</div>
<div class="col-2"><img src="/Uploads/Public/Images/Designs/FooterLogos/footerlogowhite_McLarenHealthCare.png" alt="Company logo links to website"><br>
<br>
McLaren Health Care<br>
<br>
Flint, MI 48532<br>
<span style="font-size: 30px;"></span></div>
<div class="col-3">
<ul>
    <li><a href="/Main/employee.aspx">For Employees</a></li>
    <li><a href="/Main/media1.aspx">For Media</a></li>
    <li><a href="/Main/physician.aspx">For Physicians</a></li>
    <li><a href="/Main/vendor-registration.aspx">For Vendors</a></li>
    <li><a href="/Main/volunteer.aspx">For Volunteers</a></li>
    <li><a href="/Main/gme.aspx">For Medical Education</a></li>
    <!------------- main, bayregion, macomb, oakland, flint, lansing ---------GME Link-------->
</ul>
</div>
</nav>
</div>
<div class="btm-ribbon">
<div class="inner-container">
<p>McLaren Health Care, through its subsidiaries, will be the best value in health care as defined by quality outcomes and cost.</p>
<p>©All rights reserved.  McLaren Health Care and/or its related entity</p>
<div style="clear:both;"></div>
</div>
</div>
</div>
</footer>
    
	<div class="dv-bottom-edit-liks">
	
	
	
	
	
	</div>
	<div id="uprgUpdateProgress" style="display:none;">
					<table border="0" cellpadding="0" cellspacing="0" class="updateProgress" style="position:fixed; top:0px; right:0px; border: solid 1px #CCCCCC; background-color:#F2F2F2;">
				<tr>
				<th style="vertical-align: middle; padding: 2px;">Loading...</td>
<td style="vertical-align: middle; padding: 2px;"><img src="/Integrations/Centralpoint/Resources/ProgressIcon.gif" alt="Loading..." /></td>
				
				</tr>
				</table>
			
</div>
    <input type="hidden" name="ctl00$ctl00$FormAction" id="FormAction" value="0" /><input type="hidden" name="ctl00$ctl00$FormGroup" id="FormGroup" /><input type="hidden" name="ctl00$ctl00$FormButton" id="FormButton" />

<script type="text/javascript">
//<![CDATA[

 //Admin > Properties: HeaderStartupScripts 
$('table#DynamicNavigation1 img[src="/Uploads/Public/Images/arrow.png"]').attr('alt', 'arrow');

 //End of Admin > Properties: HeaderStartupScripts 
$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show().siblings().show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('#cpPortableNavigation73').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation73').unbind('mouseleave');
});
$('#cpPortableNavigation73 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation85').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation85').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation85').unbind('mouseleave');
});
$('#cpPortableNavigation85 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation85').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation77').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation77').unbind('mouseleave');
});
$('#cpPortableNavigation77 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
});

                    $(document).ready(function() {
                    $('.mobile-navigation-accordion').cp_Accordion({collapsable : true, active : -1, headerClass : 'mobile-navigation-accordion', contentClass : 'mobile-navigation-content', event : 'click', expandedSpanText : '', collapsedSpanText : '', expandCollapsePosition : 'left', expandedHeaderText : '', collapsedHeaderText : '' });
                    });
					if ($('input[name="HtmlSearchCriteria"]').length > 0) { 
						$('input[name="HtmlSearchCriteria"]').autocomplete({
							source: function(request, response) {
								$.ajax({
									type: "POST",
									url: "/WebServices/ClientMethods.asmx/SiteSearchAutoComplete_v2",
									data: "{ \"term\": \"" + request.term + "\", \"lookupType\": \"StartsWith\"}",								
									dataType: "json",
									contentType: "application/json; charset=utf-8",
									success: function(data) { response(data.d) },
									error: function(XMLHttpRequest, textStatus, errorThrown) { /*alert(textStatus + '\n' + errorThrown);*/ }
								});
							},
							select: function( event, ui ) {
								$('input[name="HtmlSearchCriteria"]').val(ui.item.value);
								$('input[name="HtmlSearchCriteria"]').siblings('input[type="button"], input[type="submit"]').click();
							},
							autoFocus: true,
							delay: 10,
							minLength: 2
						});
					}
					//]]>
</script>
</form>
</body>
</html>
