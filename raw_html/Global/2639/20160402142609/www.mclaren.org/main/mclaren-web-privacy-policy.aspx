

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html id="html" xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head id="head"><meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link id="lnkSiteType" rel="stylesheet" type="text/css" href="/SiteTypes/Default.master.css.aspx?aud=Main&amp;rol=Public" /><title>
	Website Privacy Policy | McLaren Health Care
</title>
<!--Admin > Properties: HeaderHtml-->
<link type="text/css" href="/Uploads/Public/Documents/dropdownlinks.css" rel="stylesheet" />
<link rel="shortcut icon" href="/Resource.ashx?sn=favicon" />
<link href="/Resource.ashx?sn=slider" rel="stylesheet" type="text/css" />
<!--End of Admin > Properties: HeaderHtml-->
<!--Design > Styles (#ReDesign): HeaderHtml-->
<link href='http://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400,500,700,800,900,500italic,400italic,100italic,900italic,300italic,700italic,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">
<link rel="icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">



<!--[if IE 7]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE7-STYLES.css?v=2">
<![endif]-->

<!--[if IE 8]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE8-STYLES.css?v=2">
<![endif]-->

<!--End of Design > Styles: HeaderHtml-->
<!-- Site Architecture > Audiences (McLaren Health Care): HeaderHtml-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6396952-25', 'auto');
  ga('send', 'pageview');

</script>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link type="text/css" href="/Uploads/Public/Documents/mhc_custom.css" rel="stylesheet" />
<!--End of Site Architecture > Audiences-->
<meta name="robots" content="NOARCHIVE, NOFOLLOW, NOINDEX, NOODP, NOSNIPPET, NOYDIR, NONE" />
<meta name="rating" content="GENERAL" />
<meta name="revisit-after" content="30 days" />
<link href="/Integrations/JQuery/Themes/Stable/Root/jquery-ui.css?v8.6.6" rel="stylesheet" type="text/css" />
<style type="text/css">
/* Site Architecture > Audiences (McLaren Health Care): HeaderStyles */ 
.mobile-navigation-link-visitor{display:none;}
/* End of Site Architecture > Audiences: HeaderStyles */

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
/* Admin > Properties: HeaderStyles */ 
a.dropdownlinks:link{font-size:12px ! important;}
a.dropdownlinks:visited{font-size:12px ! important;}
a.dropdownlinks:hover{font-size:12px ! important;}
a.dropdownlinks:active{font-size:12px ! important;}
/* End of Admin > Properties: HeaderStyles */
.mobile-navigation-accordion { cursor:pointer; }
.mobile-navigation-accordion span.expanded { padding-left:17px; }
.mobile-navigation-accordion span.collapsed { padding-left:17px; }
.mobile-navigation-accordion span.expanded { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-expand.png') no-repeat center left; }
.mobile-navigation-accordion span.collapsed { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-closed.png') no-repeat center left; }
    @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
.CpButton { cursor:pointer; border:outset 1px #CCCCCC; background:#999999; color:#463E3F; font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 10px; font-weight:bold; padding: 1px 2px; background:url(/Integrations/Centralpoint/Resources/Controls/CpButtonBackground.gif) repeat-x left top; }
.CpButtonHover { border:outset 1px #000000; }
</style></head>
<body id="body" style="font-size:100%;">
    <form method="post" action="/Main/mclaren-web-privacy-policy.aspx" id="frmMaster">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="XZjD6PCF3pxLrILUi0FRhU5SzLT2M26fWcie5TudvamURh4IuednTHgjSLm2wsOQMctCFaHMdEFBVLGkH8w/UKGG7gBRfFxoBijrDtxPF3sG4Rwq8FbMJDas7+c1noBybg9+f6BEJjZj3vwgaFHxApKVTb/Tyty9GNJkjxphzwsUKypz6oyRtDZdUZoZ5wqdd6+hRQf1Oru6sHYifpRrI/+xyen5CyqcqzJrYjL8zUMEqcyODH6DqYLbqqudHSN6LEhBMSljx3sfaiZsUdjDNGyfutDnNV/aznUOzeLqcbF+fAF+5wTZRVLGszlhozMCeHZA78WhYdB1wEy7w28Xx1zcozINhz2qEnhLzyjr+79XyzPE3ycL0NdGzTPN5ySiYYa/lpXqhRV9OhoCh5V9YVUdGR4W2Sm7+1ANSjhwILu9ZwQjSG/9W4aJ7W7oXBWk5IdOGY+bXI186O8CRIsUZuneRzbdd6KZUNhgiS/xmNJ68AIOiG5RjTHobDF/8eJ6ZWkYKTBwql5mJggifxGwSzQ058T1AShakdtnALWjeqljFVsw68/F0FoZSR5Nfe4tdxOyYLJOlmOj7UGisWEZ02RtnqM+/Jsj7WNcZ6b71EAiuoMP7zVPZO4ukAUx8xQ4KapZokTNxaQpVeYTmMbM2Fyeph4cO5rzwIdSgxuw4peOuk91FUYNFVd2a4bCBD+/mgf+db+bz4wm4DaWWLk8AEFXRDnaN26qsG3qged9niI3DIErrOmplaVPtUes+r1RRtL+qoaheYSqPzMz/1us4xug/fw6vyQ0rZtVa6JRcm4poVqMZDrFS7iPmqST74u8lwZaG35iXksLqz5/PadkcZe0ITMFpAa+OuEpjWpz4WjJ7GOeNKUt4YLjFAh1skckjT2O9bgMDL2EllJ1wqMfNfPQ+gB9en8TxLkqD5hrZofisf+YMr0kQueQLu/wZogPoUss3H+TXQbNc0/JnylsEafaHQfYOGlqSJXQ/Ftk8odsIrfuXrGScRtpVSRW/0+NzId7GmaQ0SsHLvwikr/TO/ZFqgn3CZWrrQsNmZT0VWSPs7+HACXFfONCJoiy+nH7X+5nZlq/y65iZUuDmbTf+L0fKni/uZ89khx/Jn+n1bgO4IBSm6ySiWCBdHlO532HrSQMZPfoZTvSHIcEKtZSX924rOBei8MaDSQZA3IqZ3l2GlwFABzIuNDXUlSlc5nG++WFFKiXReCCKJLWmPgkg38ZNeLJJ3dQbZRxNzHOSeZvuDZse5BgNp8oiOH0UmsFAqlIwGeSBy8AqiMrsi2hw1wOwHYmqie6h41HJjNJ28+SGYsVxOxilBSsZJVDXdZeDcpC1ogy16Zgdz/TLeRiAwpyZsAisoB3C8oNdbAyj/RR4aezhkQb+18OdlelGrstDAqmF31H0NO6CFor1uLeHuX2ld3R6slunJJSGppEEE+7fS4Y6kPc1VTX2+RFH/VJkvEeyh0GyF17tj9KwpzSKwCnxx1xxMVk9zL/8lXwSa7G71O0YY2GE9dTk+K7xsxGUr2KDSus2nx1IZ2oahlEKeNQ74akzkyEneyaCCpher4LnvnmreIMrLO0MRERQMA5kVQG+WGOEPTmsw78f/isTqyAWiTWYN9LV6eE28kv3hXVTIt6Y0B06tn/QXnj5aonBQJ8Jry1LH2cExbgpD4tOdTq/G3SC/ZFFMQHay0VYdHvm4y6+3aHZFQvk56RYuZgYs6TE9s7WI9DUSaaTOcE5iM6pbhzG5Md2mqC9Gr2jlZTWfxAxPiTb/87AZfGNl6H4MsaABxltx4eCHFX1OZbJcMeaa1yQ6QwgZ+hYyP4Lu2qH7NRFyyPDKMi2WX1C3ITOIxeknw5mW1V6fn2gzTDsWBitBskcGQJPYd3sO/nHoM66RMAyOSRrNso2/VqolD6RdXdPIrRLAMSWwOmOm9yCRcBlO0W93vmW93O21gAAP3LsCD3aIc/lrIMi3ilNr/Ek9qUMNk0ruUjK13qysYK9IAoqq3xKTEWldhRc9YMv9H6wYquFRq0sEhCPg5ZmF1LACv9ds6N5FQUgJacHg2pFEpKk2QUX3XAuFHF9Wtzk6TA2gurn4JXPLMpTvhmei4bIInWNYufiSEm9VN/QcknWzJiC7+6NYc/kuH5YXsmpHpzjeZXL+PVNHDtX2A7Igx/hjmRFLymLXopGqxwqGsZNcSJsAXnMZ8WTWJypcDI8hbrXwBueOP7gnpHk6sgphUI74BseSsYmI8sLRsSRs974pSkO1L11HghQelk1VQ7STtCR2tkgKbbHwe6ybxaKDr2uslBwJg8bnvXxr8mOuwvMP4AJ7xbk55Ze6n77OlkbnJB2++mbj93deAOwAr74wp9G4HuDs9NMovOLL5pMTUfxVsZrFs9yb7k6tKG6Jh8CHb6IOcAFgMBcL/iqoKIGdsCuHPhmbMDd/6C/Q4BL+/zCJE0cX+lV4vqNxvtSdG/sXkKWAJKsrW9OFMY9IQf2DLPEMeE2Ji80fJ4iEAP6oBvJLU2zCcxddBi912rq4ksZXeCF0W+FY0zzTgo5paAuBnQskB91jIUULWbtt/1yRhhJbIwZjMH92p9sI8PhFAb3Gc6IxqVLZtkCp6mUNDL8dwMAjWEXbOsAXq2iLOqLfSlVfGi3gBPPQ+wQzG4k7yfQcqLDbdoX+ogmTI6F/0L5qnhLmSkRJo0gYl3jL+SCdwGyXcLKMOqrFQRIMt4KaXT3QNWhxLNHsUqO22JdrgdsRrXItniX2uM/jZSwznOVAHmlxDhgbAiuERMlSPLY6Roo4e/dWJ3SZFa2Sl1738cvFc/XZodaE9XrLyUKDJSiUHDwG/o6I4vAGeJi0PvxtxO1auWgrf9C/gkq6ibwA7bJWcFqd5PkWB+EnZss3HA1QzcACaw23BqWlnaGZw3E8oVw52zWsS46lWtBc8oMBSa0Sidw3SpWhA3Pw7Rs7J/sd+opmtYFYjtjo0Y3y3+xJx87fm+aLuoj6VsZcw3eU79v8x22K1bCGzYk+MBXXdh6UaF0VoSqQz8VcdUJDsb2AFUGPXlvOJP9FwTK6tOqBT0ZcC9Z+70CpT/lqjmA2kMZcIlv+Sl1rNp9Pc+sutUvW0s/Irss9Heg+mOmvQdUt9BOxXJrGLKG5uQJkl0onk9weAjQme+IyIVV9Uun9EetH/yRewGT9OwKfQ2JoPzlGF1AZqNtu5LAQX8qSG658to3hmPLFTXJErIYk3GTL8vgOj1YJOGDv5+63jnt45Hqp74UyDU2nOkvyqcNx6PvTyjCbr8UQ8V/hjMKlQ0qox7Dh8/RsKESAE/MEuL+HCrBIGR8kPW9wzvOadEELSakuOVaoH94yeKcUDbatvkzn6MaYE0wU84Xlo/QH+o37WeyzYu1XmIFGiA0RfhVafBrvXPiRh6J4IdOYz2ximVyfBolIeoA4PmygMNxpnnRaj3egaUVFMkeiybDgBM3h05cLYBtabNBkG457jrLq0dCUdYc4rahQPDvYrJA4aZaJ+yVi3g/ujqVGQMTX4zOQYQyvOFUG8hlqIKSteXxEDpCcwO8xpunG5ZZJ/jFsvUFpR/Kdcv1HY+T6IWz4cbGk1bWd6e1QnWfiOZR7ZpvknMgOW+fR4L3jxmlRS1K7yayUCHg8huT2NggVVz4uxTt4Il4wi7Q3AlWPJvjXtYz6nXCE4bCeWDRLgdJiz2CxNiWVPCqjkNF8nXT6FMGCd4PuCc8yTyQPRwfshYPvTdQprb4/HrdXInOimAyA8WpIxS5jyh7SdVNw3Xy7dHBB4pK9Qu/fl+Qd+ctenGGCLyzaySQkeplKxPwvd9LH4g/0grp6loJQbK0orB4c/StW27j2EItf1csQr81eHpV33K9HlEE75XlWb/GgaYafaEoKGEz/bhCckKqk4gc25IzIZwXlYhfJK63DSfeqzoOocM82PxL8t0330Suesz/If4l4DeuLaUYEfLAAn1LL39Hpymvq+rGX69/T6bTuPs7+Sd5mh1ChVITqceiYbLCdgbWGCBOh1hZnsBLO3zdwP06Tr2MIwuz6fMU3AqY4wbSB//PZyjGzSPKUV2ziUVxDUdfGUClQl8nyPt9soPSZ3xi82b77dcVX8agAp4zSSS4W4qubiiCM5bLX/PTvmegKC1kYVZZvuQ0hKb11tlnnZtt2UIu1WPE5WSnD+wexDvLE/WbCh3A++IgIsL1GnadBBwgWshLd/E+xcLNOsPpOou65iOYxojPPK2ijZMPbmow+/FjQp3X9zxU8LdW81kdX8CmOjLMAq9G5S6fv4lSus4l7YK0W+u16vojhQ10svvnaa/bWOw7aTTMVVsKrQhDFv4XbNbQLD0x3GbVEhofVE5iYzjzgYP0EiR33aXLguMmFkQvssHxZ0XmssCSIbYVq0e+euOj9puvPTTRCVJoIfv8VRuBoAzd97858wHvFPhbVFjyxaiezuwPAgl4iYtqt5T3wHaPK1ovE3QhaO3oFRiXmV9dlck+cURP4ccglFP3Fj47NQU8lqgOM6qBAZLoCq7KcuDa0yMlR3mBO0ryq5CiVFe8DBvxX2XQuBfE6UUzOvh+3wAPZvRnKAkbqo8AsClcl0LFGlamOZcDDkjh6ATNa5bDHNj/xSezbmHzeJ2P+byDrSKY6er/qapR42GNnSl6zrg02kgSaA4PKUh7pKnSreEi14sT45BGnu7LDb8d7HPRYJad3UpJVLtRvXN/x+4dFl+i1sPT2c2xBPhYHgXLfb7jNMS2kFtJyi6Mqn9ETnq2a5OEfSjoNZ7mISdfKdmgBFHED0/3z2uIZw9JMDhFb74L+UXRsj+rPCfurt3odL0c2QAJFF7Ld8psAj875d1Pi0o8NxMvSDDZYPQN2E7qm/RP8CAJ8PJ+O4mSXuRsjpB1INAJhM8YqFLm829fZ2xAsJrMkpsoyRXlrF15ljx4mjanlwv3tkCUif4njZl1AlI9pFQmc4bjZ6yw68yH1/nueOa1RKXKE40Lj6Oyb7wVhHhw/1iNIRTsnsl35OV3g9EIlGDwEC4hvJp6avNOMOIFoKqQ9z31XGXuegkUUzqwF0jzyNnjWyCT2/5O8StJ704LgUct4DTs+H/MkdK0dQn6f3TZBRjCc/4kfkqndTA8wIJPe7Mjf+xzw+onhQhEccT9Va2HOtzKneodqmnJwPYRW8yYCcdhoQw6Y18ORdmVQla8YMo+77ot94lukJZAvVlqpV39hyk4y7xgcuRVEMf1uqpmhMgLuN9QGDWWZxZUyshSXpWnocCQV3QRURVEGZbGHlMH9rSWKY1x+6NCl/08ceejYOjmf3w8n/gPwteWUTCwKwjFJ78MaPEDjrPXgf+5zbLY4pg0m4GF8AMqHmwA4Kp5FrZTPSteuw4VU4XShIj77W51hPTQHmrcp8IZn5q/RfMYJc0VZW/m3p9Z8hb8carPPec2CWUo/lWSZIJxr9BxMni1H3X47kIxAV7ga9qYspT3qDImopztN/KbJZ6LcyA+99YLlllnBdp6yw9JSSkldmhisw++4SXJi8OrtcStgVoWrtXKsG7Aj/N8etYJcgyT4IvyaqAEFtEG3vJkYDfJ0q/y4Cgd/dQ923q7k+T2vVEfk6Ggu7GLuVrpRouLwM0e0ihU/v4Or5wBaC9MwdJ2TEm/mkwDlHa/D+lKoAYJt5nSsXrp0R3PmqN/qafMdVeQenH8iJZENAHJ3Au/7HnIK8jtU2wveVzLslA7mY8khYlaXKZyUIl4aL9a4o/+9uNZ7IIhI+4LNpMWPMCJAT3Bxrg2vqh2JpLLRMZhQC6esQdJCFWUZ1wOfHr/9wQtqLmZO6fTk7kS0cBdPmionNhR9E0sEM0J3tdg03ekLVLyLRgWjEs8X1e2wEPwrWueiqs/sCIqlrgnwZcIKZdRt9REJPWo+vJy/1+yc1JE35jjGtWvbagOmOWms2Rvp8gCR9ISMhAy1iYfBqp12WXV6xJxBo12fskszYUf2WMEyrHFbCBvWGjJHz3HYG9RLjXBbjSbjIJ9kz4YikaCETpKuoW1s8VSsHXnDQxjUBjUQtqXuhgqy90UyXDuVju7UIhUv0oDxHT9MhDN/oW5nBE+RXnDfCfCQIdwTlGET0iYBqwcZunO21QLdoOxNnfohna8IFCqCUAY1YKUGt8BJQ4DAt+Ir+WYyVBaC6GScoRSgHRH6PjFnShjTVmea3l4KRcwMWyHFBTimOs1hjdKSrwye77eA4vGPcW3wdnM2CuFrcZ4N/rFQjlfb/RxUvIeBfvP2tn4YOxNFsPC3T9iD1j233owj5HXDWYMTmsrEadVm9FxdOKgLpjJ4SxUpZiaUwsic29S43J7OJI+tIijxZ98RrhbDv+AASqh1WW6Ev4jIs8wyBdnMbQr9hgSOL1EmUGLy+9aCEE8fRUQxNewgwdPBIf3rMxZIWlw8tUYTEHaO0UuFOBmq57UeqVSDbyuVIHCn3pRUOPm4UFuuhaKyUpGfHbe5VHKGcEiofCZrwlCUE8iYhVhYGo759UD3wJwG2eaqYH/ogt1F+0s05kDMedjaRN5E45XjNRsrLitFIYccRZ4ICxruVfBySZ2t/E6Kg/OoORz87oj4x77dTMv2RnLp0opRzrFH5VYKeXt+HkZYI+qEan6np5UUBaSDjo2BDgUyeRlU6tIMPCn6YteoEQ7GcV7RuK3k/MHsM1kcSnsT3LWGuplSJqsF6aRTeqk8cd74XpRcDD+U0XSvPF/Ouy+DmcK6MZKe1KMyuGcQMzopKHWPx31FZ44TSEN6nAg38yIIfF0ICrbTTm8TL9PNptkxAem2NqfjTtYwjaNr6xVF0DWVW5PXdevcwT/9EH8W/aMMNUxMm8DroyOD92P8ojZhN11pKj9HIoRNrXzoQSsAlGbf8N/YlORObCI/R5xRs6jzWMqm3c4GAr6nagWNq5rLA6UqNxvNYvU5bvmOXPz4olUsQNn13+/EZ9rwndqu3h3cTJq5fj9xaoQHlAXJGBq+6VeoTBr5Z4cQcOcdB7LtOocf6ArpXTZudVU/8zX1i1bA5SUjDvV2ENO2Q2zfNhRZGG6SGWoKGwnhM1EGrou9tfHYh5j7WqZh4h9ThECFh0tf9jKoCY/SjTQtYKBWKUenHGa6IaiHOxv30qkZWp/VlO6fQnfRk+a1sEYpXnTNlDThORKs4Mj80bvNt9usZtbfyJIolEGzAC1VfdbjIAcNYCigSnArw+v9/OuSeYdOYiy1sD96oX751/jYSztTMQ4+dpwaS6Do88UAX2qsh6JeRVgcOIISpi5MOn0rOyjEWYnvhnDx2udWA33se1+oyIMKjw+Qkl9lTSLtfPjuhNME7T9KpBFbFvRSg/YNRxoID8BPsaSLrwP257Co7A8S633AwWJINUMa1YkSHDopv6LU4hp8MP7hbRW+LFK22heX0531Dj5aWUgu/QHvKkYZuZKk+1rmIlpZhDAGjG9GMHblujQeT2Z/ev2K8oKrrpBPyzAj39sMduRKns8MBsGAe4753BlIeMgjYWa2sN8nP3IfiuJnAQlJYMGuYlSAGryu2RPb17Axqd6vQ6G+QMJPDKmftabOmVSVkeT7FSAJtZiOqmI3uS9j+O2/iJ6NFRMbJnMhWPkD0ZkRps+zFhHiwU3jAo6CncGJ6MKwv+Z7OAi8jsF9HB36i2D1IuSQ1ZsZHCIvk2WIVXXMTNTyS3rKjN45GvlK6UPJnHVJxiB4gLr28OrzmAYpJ94Uqhk+LNwSijkEotBpYVC3yYHBSJ2ASDxfS+/kmC0P+UZ1vwa+XrEAy4Jlp0cf3JapQ8urR+d09xFLcVyugfUZDO9K9Fth01SVEJmFjGq4bcxIcNm5GLEr2vUjjYvOCSCo5RVgfAGWJ8qGwmg9DNVCy9+ryChL97CNd+IxHp3ubEpoJPArL8bFspEHrFncp8Xzw7221RB4fKSQzxn1G9twB17nFOhx6BPzE1rm8FGI6qsJAdRZlt04Bf/nf3UQACyIVWUzyxddbtCs00iReCPPHvnHjv0wmM631LCHdT3blBnxwWoGIgenVEQG64+oGx5XuubwWyaulcESvbc2ellwZyGTqzAvApUrGu28Gwmq5Q3gRbMm7v+hLa/JRHi1K+dN0KUPsd6+lfgtD7Dy2NwULcXJROEvDitCYCSise9WaO/uwQvtkwZTprWkJgqfVYFQAThNXYJzVPZj2Bg+GoyJkLNTXYS9BZ/t+RklEI/upA9KE2kFsnf5Ph+iBJ2mpOq0QHmQo6sknKSHgYqo7pZcO5cOyhZmu5FvBbsjQ5Df+2gYz4JUg+bFxZ1zisox2q0lOxxsI/K1jnaS5ZiJOr9ZENiplO5ZEAOW6UvcXcJ7Yl9grBrztF+9z5T+j80kOzJLlEVuBPoT+Hu520HNkRH4cSun40Z6IVWUHz5SKVUxb5+ViFez/93HM51vCmrcK3jED9XK5Ip4+sXNoMDHDunAxO+64Nxvsa3XDCHLwJ3vkwmfVfWFa0YyBvKs0zcYaTLGQyIOA83xrtKyyd8hHpufm+2MH+JQODYbLTtioVa59/nuZXVOYVpHvIZ+gRjHSXgR79Q+PdQRlqFltuhcN4oHsEgQMPsFSYfIp07rCTUTC3ZZ/D/PzbaMg/uScicBIBe4sGkBuA65EV4R3W54eSwvGgArRazj5iPdPJN5LU07qVANEtALAZP0ZgsPieS8y3lns8ipZGNa4UL47VnQ8t4A3XDk9E4IpFTiI85AUmLf/EhtKs+7MLo/sjHQ/4dWbd2IMdXxcu3Ganfkxss51nLjZitA64TBeQYLuUf/EyE8P9b4bwmNrbhGRsrU0he511knGuCCvoGrbJO0VFrUzgIkNGJVancbzG153ryPVxamJSY2X1DiVdFN4HIIdH6Cg5Yn1Ck0You0kLYeG6VKgwXijWOlqXK21Ob5o6TPWLqlSe99Jo+WTPEMn3RS0jRYaLfXg2PbHwllkPJEJZEf2SDBJ5dML9g/gPvwOjaTKyCM6BbRqisvsGI/GoGi7v3JGVYGPJZ8HcRMQmKpTjqFw29IlZI+1ny7suQ9S61oFUyW7VqU4fNys+FmDtq7klVz0JnELo/BBSP9pAqIj5YSeTfandiY6QYTPz0cV/wa4BrChFGQO6233wMHzWwHMki8iExC476gQM6OqTUIVSPbKDwiP2BIzs9WrSzYGWAXB5IdZoiI66dvYTyqeJ+S2eR9AnN7rGr4pD4aTcWrOHKJGJvjhFZme4oXL2xgQcF1YIwRZQuOm5Ab4NffJ9mHzTjWt4AYtIG+NesbLUqs/0JrapXndkTctFuZV7c7Cmmyg4hgCAL173polLYVSMiwOmdIE3dc3G8Hwma5ckdoyPgJ/UDBGj99z5PYkpI8/+ZLRPdp2Z7uVK+k29euxy60dYdawdxbqoChkFWpHGMRwMV7w+O1Y6wkt+ADiTz/2unOylga10xS3m5YS8gkPTdEXl0mHfeLg1vIF3uWY+EwEc6dk63FSGYCtLbBweHwix4pNmyaYnN3Puyn6gSXpLr3r2vEdN8AjAusgZXoB9zuypfaSg97/fFcDXkbAhU+Cip33moR2T7rc1Zm4CG60F13VKJ7Y0GCMgPILraxapr6KKAptoKDkNkxqVSqWnD5fKnUXUGsMLruf2bwWWxKUp89Do1xgbmeZ8V604QAyOVKRjWwOJR2IDVUD9HW3p4nO38Br+KbRk9jboY92z7B+8cB4RNM1xL6SyxcmMF0whpgznGyz/lC9/ZlOlVtXobMte9V9m11ajrQ6quOPrih7q5YQAbBbJlqO0/zemZv5Ym/4F0LtDDbJj/m2YaNlS/6c8I/VPhsy4zizQ+mp2zMBtGjhWMexUjQRrdQ9ogDP2Q2V7QqAExhCsd6gjuReJ1vPHfMIi52ZkE4G/hNzrCKZ7LldejDchCRJbuRehxBR91k2teedoHR+y+yqsvvOtTiUXnGEMICyoCPthGTTjIJIS2XtSJV3PzowI1wYlFJg1bGZcZ2IVHRvSEhTigbDopjudoqeh2g2R90vaZI1yZrDgDjECWAWmx4vXZPK7nA91vio34kq9SF5GGRAd94au0kLl+fdGHTjxMEYsb6LlwKpeMtbYwyX1qH+ScGyfmk99TUTduCrxFq2LcIj0tW4/6qzkKQ3fhNnLRrSXvf+YMNL6c6fRfv4IYFnHeLfG3FHgegbjzEXtB2RCGLD0NzqDR/Oj8sqyCtGjZumu0p6dCTBZggl2mPZFZpVwkPdFqlOiWKAYJ5HzqsaVu6ZtO6/U1PfotUbEJFX4vIAKnDfya6PbkDuwaW6NbQt//D2H4/sBVBQVlHaehow0hcwXmAngt4afrSsyzdpZJxUWYle5RSi3bYA188XWkrHAo7NhXbHvCQ/mJrk+yutoSn6nwxeiVvgg215FG5eKsW81JAU6JHFWhceamqxEG06LNIclznHRNzG2dVVXJSzSVDhDovWEzlLqnyfQ3XlL0yniLNju9l5KrPwzXEW5vdEZnIx+JiQpgYcrDK8ggsDOwfjCsCfZ3BErNndoswPMD5to7kbcFqvJLnHzGKd9vUXJNPLT6uXZJppx3JAc6sQWK9YxSL2evwboTBjQuLpA3/3w4rGu9eWXrVA9mPOpr305+yEA0oUS4+P2WRQ4hoFGpdil9GJwZR9Y19pK7byqZ4SkoTKcPxTlB8/y0ohn7UYa/UFsish3mcPtS7ur6mFy8o/5n33Yw03RNGO2p8GCJj7mjtPOV4URaQAqrAccVwHMoW0JLpZPuKMU3veRlEwxJF+zoXLFbxuhh8brilD+3ZBsrWbEccfM/3b5X1euXT5+FCoIBYD7wQy3GVazJmvbMXtxsWd4NgeF7p5qhDoDERXzA+0ggPD6LetJqvKdCdybjZte8sIu3r5grrSooH0S1uxaMu+9x4/mOivO/1L/PHyasj/2gfqv2Dst4fm9UrPobA/tdyms3nM0Rn+YXNpVLdWLMqtNItNUv6VigMog93XjrPpLd6mpHtu21X2fxWjDPzGhjvFHNCp7QkVcyYmS6jUpyPu8D3a7DgufLKJWPWzc+ep866rUNfcy3YWsIR7HIh5fuSNJG7xifOg5iXWgLf/Q0uE4OV9x8OpsbinAtzGosO497WRx2P6jRUbl1d35Scg6o35JOHvwKCi5YmfOPrArYPZ9OAdsKxYrRta6IYDMElnLH29VK1KwQzcRXvbpy6me2Tr5MnA1vgzUTAn6ERlzJ1sC4tUEfk20NXUlw0uzsnpp/ZeMZvaUFEu8DBp7803mf0/3yKs58PFPmBhHE/oMnE33vX+vHRQWKf63/80ZKpxOC2zbtyKjfMEYua77MhbaXEM70XBr2TZGS4jX/apEYCTvbCFKT4osriJnXZahPFYU/cZS0e3wEuXjXhSHqMf67kuptacFaXvZfdPbOZJS4ljm9BRSUBWQzew0/JFCKy5KCOIjWsTz3bANaWfxD/mvZGcIpNL0+nqqBkwXYNmea453ghtAKUYYao8wyDzKdldO3MEaShncB6BzL7EYycviEOZJ6PXg+8FFljTihoxCLH2HtZEFk6JAXIhive/J8DEAGEfNEgss3BaWEt++cZUvop1N+KWer91DHTJvotvkm1gUzIjByepifwE5XYnbfBpaMsLQVHw4iEtZNQ4KdLffU8OGW8H9S8Gucpm3cPCyCEUDw/YLZHejJOi3TJ+IW1do2PPamJNkzValxVT3zqgKx1r4MgZ7FXFLqzrq2Y1s2QFjO4vfWEbxK2uCRL/JOTkY17fSfU96v2/oeqPFPMNj/8BDPxN1NaYS48My9hWxrYdDkcc9QdeykqrUlrFuIZFHQNO9ub60vmiS5miXq8e6EYtEZHc+k3Lr6kXBpmaR/atWw3tEVQEJyqxv3rgLZrJ1g+39r5ZeSoxPvgexY+NEq00ox4e99SZlEfsjvkqUTjxHmbfqz7NSiapoKBmANIF7pv/iZjZsDoFCxYhkuYjtYufF14uSL97lbUoOjWPZG/fi2chvkhBwGQdxImhKhojHYxC62y3uhRyBWTOII/7mc8p8kFTFPbYY7oJMAIMKfIiv08ZmyGMx9PZIdp+sU/dE9I8zI02z+qQcdNQtiFMS6tYmve432HdrOqDuG+aMPAQuw/iVf/wexKzUMLMfB+uAoVFYrgAUWez1ASxgFTV4hGqQOvLsgG2NArC3L3427a+AYNNfUnoQHJhpjmK410GpgL01oLl47DkupJ/vWAt758NCWk/ZwsOCJxQ7MiPr3hAVVo2Y7BpZ9+yDqYcYC3G54wVAEBQWYL2trdpp/MzYseZlv4wPuoPiLTJ6Kpuk/+fQHKqzZeke/1EQIq0rWQcBPtxwQhb6S6W8SPbrEPPUsP4sz9ssimT3n0+p42yKa4hqu+FUQGcuJCchKVvKKS6OUgUa0SN91Hasal6i0w894r74WZnYZpZM0G+xynDAGWZRJQFG7Iu4B3ieLfseQaDqvSpFoKsQI1fB1yGhj9d+jnjqDBBnCcwamFPIsNU9tV1FceWTRdKBbp4NNMRYuIgd6UFTJbUL4Oe12MHfgZXtKzPf/fZLBq1UiOnDpYGiIzr30c7cObZpFiXXx6jksfms3sNwK7LiN/Zg/aUxpHZXc7rjzAEd3VsYea0AtGOW9d4i8Mq2xXJSmpFuQxirsZ40tqHZePa5ROqtmuvRlGNqZDk12UZMZi8Ok2+Nq/kpKZvx0hN2/9ivu902UahtwNLhWkpEcX+LvwRjMIlr5ilOHjhvHhm3IuDrCs2u04EbPHIxzc5phEAwprFB42IyAn4FRYONdiI/uFyS6wNxQdJ9a4YxUn+4dq2yruY1GRQ0KZPh8WF1Mao5WYtgKQ2F/FaMPug003ebdA8XXsPoXgurGUGp6V7pP7F0zmZWle2VE0s5acaDBDr0ltcPYuVU0mq2POSizJiSXH+Ui1o6sB/Vs0j/1iikX2M5UunMRPX4ojVm54nv8XiM/dI4/aas09+IXImV2eXAZwtzrI0v+HK0XDh/Dskq53u6kToMY/iNIyp9TG3TsLWKZh2wPni5qR0x2ajqDGyofmIwFPsgZ7FwPKb3lFZsJpudICeCzbs/zcQvoFSFrJNChZg/tDAW8gL3gB5XcRHJLDhfrKVBSPTCVGhRFrPPDigds51QFgcR7I+OiZIx78vKmwxr1Kt0RJ6hEWrpXt5WdlcJfrBV/y5R/yl901moszNXpsWLUykj71u996Qv13+NEy6lojAsW92vc1jaCkor38v93nwlD0tMwXp3rgwHpdgAJS2f1uwqzucfpwu3JPKhiRkSqlQslYo7GBiLyxEcw/MI73zObNUOG7+P/9NBd38oQypqhtMPxgOlr7PvkUu/2tkOerJMg8bhpqcEwLiiD6IZxc5RkvMFueXCuYhCY1NDcDVnBu7NN+Zg8hD6FwfY2QubGD2PSoMgKu7tGj0PGXHMhmDlI2ZMz6sBce5ZryRHIGw3KeID7dEvHKrYEHXgYRc+L6grK10IplDPeRxDCOnvXeF+Hx3ORGLBf6vMAiFzWCGn/Hqwrt/8ExMFXrxu3ODA5ibItGI0LN57Qkm5HI1p80cUFbFV1iUfr2vllKeGsBCzUeGG/RitEhw0TgSNvlz7eSJdTAXx7+pIavpPve58PeHZxxvvpVlkpk+1N4oXyUSbirVwSj8Dj7X5862xnXcgUnQHnmhJfQhWelHKGu9Ox4V5CnfIzFvsBtQUZKZBNxHgI4yUdIz8m6gheXY674aKPaZ2OmBXe1JAQ9orO2JgoREH5te4/mniPC2Ab6JOxCzTv56uUH6r5QYQs9ib3JfKdzr5IFfHeGQnPOdnFotynQ7cM0NJkGgS9P9Xq1qzoDmLOcPRjwflwyoM329qt4/QPl3Zo/p6atM/SJuhWEffrei4iKZDto8owY+g/4l8JrdRvj7A1+C0yVY17GMxsiwQpx1So7mNvkQXFilO0X14irlChRFGTRKhLdbWMNMYxVCvlvbVo9VF2d2VRU/MUalSqP04AeR+FqWBvpfSRJi7bk6KUIXXmaJcgUlvvX7oOUz/yGUWHnXymg5+Udihd7IqT5EEkgvt7HhyfHui3eNrjcseu8pWw+ER81uz212NWubJgagzRCMpEMo/Corf/icIcQXL1n9zdpT0BLxzMgkSMjXnzockPAtj03gTzJwaoE/ljzjMLykCy7/yGYU5VqjPdjuica0E4W5DRGzFSdHG+PIj5m/xyED7vjgosohhoHqUh2sl7mXJgT6rwNxHmdLRBwIuiYINqBkWJrggJ11Np29RwdC1T795QssZxGGhV+yGNU9wfLjYhgdh6rCjXFP5pBdYlbFGg4pbFI2xRaRFqsCee6qGqT4W9DrSwKoP0VC3GzFpjUjiCvxdwilIYUxKu8dnO/2wfEIeIbi2/mlM90G7c2BDyq8tBAH9qenauvl6Q1DnzDfZxs0uIWOqOmM4C6NSNlF3drlg4JVWsxye0bR2ASTmvxdN6It5lpmX6+Kxj2zKSPWIxQUIPbaVChxLU2MwAuqQ9yF9K74iJnkB0R5lFF8LVXxYHPKBFtfUj+G5t6wjK8om45KCS0oaA9lOjfx0j7XgHsJstBZK243sSUTc/C+jnnWtSuos0WQ7L7dpDL6x0q6NCUpAezDTrnpszRlVuCYMWY9ldyND9y0dToCDzKYLurPmrYzIAFxzVkrZwW6v8JZqOBUk6gqT429zwLtOw4MowW1jXu92EeYlDJLDF+9rG3AMIBDd+XpR92/5u0ElMymQ80uxQK9EMp0DFhgdoFLRElw1az9e4yyNLdajdCYCAqCoeLe9r+53uX9lstVPvjfwJRsqZezVuRa/itUZFxKxenWbGBDapLKws8gK6NhRnYi9FjXzH8HHHqVtX5OatlOIHJYHFUp4OE5q7erhVNb3tQK0p0k5IYH055TEfBBtjH67wfrYIEKZ6ALr0WJBVQeqRMYjpTy5DSc+xoPKokzlJ/ke9RHQaDWma8B0s1z7aQFXOhGrN1rYIxDOP/ZZfPGf2ZpHsazTMmRxKhCMUQMjSvtsWOfvb0OKHi9r8hE53Ql68327bu7GzmnQV4U29hR6VoSKGrBEeXkFfaNfa3VKxL2Lw4/+Aoxg9IIM4eTTVXNMiOpZuc/+19v1Uo1fvE1Ks/r+JzMs2xZu6HCr7lBb8CVtRecjk+LQyOtS/eaDtOxAen7G9fYUXdZNjyD/xHKF3eD+TSA4vhhE7ppL61PsK86LsZYGlFCmSROkRFCArcbNoiQsNNXhb6AM5XHXPVSO/hjf0PqefFJquNGR1h2OJTaht5Y7SmRXou9Lis1PL+k9YOHxQwQroDlLRaH5p3U72HRVLp+duyEXuGDwOnNyhv3OSYrJ089RxPSfqLpK6uzVb+xJ6GemhPAn5zXfb1qZNoAXOYzhR3IprYv1ZPOhtg5JYQhz2UrZpcE2wVTh5VP4U/FZ3d1m1L/3T2LOM2hDg45vIS4hEqk3HT4Exuff611IinoUrZxauut1WJMB+ZwbyREwYx2YVbvUyv3jPjZ5SkrJ/kV8HrsT/IRbbbJ0pZmVTnTlk/TfouSV5BQNvkJk+1sasjyOk82s/RtQPMJHT9afx8+wSdhajb1zIdWeYc3G2EH32SEc8ZcovCX4XhNl/OEIGJfS6KbOp5CxmkPWqMnlnkjOmQXgEOzLvIiq8f8kRpd1AWsgTZg2lhGcDbRWhoO28dxKjVcUOFThDGUxtJBRTDi57otgN303lMuEvAiNwMHdGmK50KM5dCc0XZ3+p2MxJrsuSwdilhUnQQhyaqQL9Im40q1RkPlvZ/AeXghmh+qEkz2/ktiup6bo8BOuZpORWRyBRsvKhcEL6B5TDmKy+9vEkSQD1yoVN0sRSP+jSQXXWSO/Ywnnr09Y+tx4QrwkqxXTkygPxNkQ1tABP+GCGmiHqWr8Z2fGSjaUbIl6Jsn9ZU3DzgCZOV2ZrZgNSLr4gRkEylIpxq9PKNUq249onU1YmMaQ/093DjrejVcCJJ+uhRvQEoMAew9hxewfC+oBmxyhFvF78/0QIHhDInKBxXssuac4+f4VRUb1dP/6hsJ9TRPmdgpjhM38G4iFI2Ws0cwZu2cewMNIw44kONK2Os86v+hoHlpNN8ITZfKjpo8CewxUjZ79Vop95lxYXdTF0i8n6zeZCoXd49vRVu8KRIj3FAl02p8CaWNxniKgyHEavOot8PdEsse3g3+d5XeOTP9AbU/YX0AthBYp3PpPK6JXxUfFvo5HczrYzmgABSSsA8CmZsG6B5+pfhfi9NiIuLGQR+SbIUm/fB1lHJcK7+Z5d9NctkSb9z0TVlXppNNzbka90eMR++n2s2EoXlAy0jWVaNIV4x566WW+C4uUj5uRFMFeZV+b1e+bAGI7Hc7cEVYgf7drwFm2mOKpYJXOkQoazBPc6dnIJiLjbEHWxXC7qpFKDg9CRLzwQ14zeXJChIM/U+hqvF3huqrGyyncQJnG+cohhcbGcTBMZvCFTGhSOWPjxfjZLhs6JY+NQqXmpEgCcZShE8HfhHHsx2gA11UPYAMJeFslGMqDgf4qMt/olNKxbHOBrcBHGdllCgZHrQkaJFPy3EG+cZMvMZKm343P/FeUmEqY/r32k9334sCPMFU1hAAXOWXsg6vrLI1YZOf84NtMZ4wTol3WPmw1NTd6eMDQzfTJuhRrjJBs/WWnZkuShI85YlmIaj35H68/MSYVwM7clnDTfyoZAG/UotadlXh5jBT4UuTyV4odN6Joh/x+5u1cfKvtEzk0/+XLS03favUb7MXBJh9l3kOY9eLWrylZTmzdO1h3MvnLsMzDJ1wHBMPT/JcxiBnAFsvWsMyB97SrH7fvNsaf5ABx2USUs4W3zGTUiwc99vbzWOfCWcI22mrD5tYsXErN2CvP9mTAO7FYAFTKoTvCQtabj1gTMP1bGHGF3cdADGIpkoFSX2a/pb9gs304civ4/Ndx+TGTzSlbiZ4seaSDebsZQ6kK2/hpcnJjRZbkNjyFj16/6cZyAUeJYDqZDrI9gVLFoz0MsFACd406P8MnNQbxkBxAwARgUWx8mdDrQNngZatUSN/IXUnmoyXZiuKeQ+jFaQOWL+Wb9j6UvnOeKzShNlxB9UODXzEvqA7bVa+jM4xTeo95ln/Xug+LIUhC8oj9Q6YsKbvyhY+jtdfQyz+2dbjmulGFJiIty9Y5CmlIeOBHis1pXCAD3+Mg/sE+ExjyM846bxss138xN+AvHxtNVi8W4pcSCIHOwi35xPD7dCLzLRSP8Md7shvA6irWkKDf5a/yTOV0T/XHbObhC82QUwB0oJZ/51yblBtGuCp1bWwXBqGHiX1l+3pOK1CFtUFDPlrHTFVNqENNCzEe8jEvNkG3GwD44wr3q3LFO5XHbxJTkaM1Vz48xOhQWj0Ho2GR3yWOsjDQadYZ7LWELFlDbJplmXlt2gSfXG977UdDHaB9R6SjCuLuo6PFKQl/EbhqFZRw2ZaNdr6HdrfwhwC74UHJ/2eo4T65DAptLYdcj7phcTLulRygwGwxNXsD0Xg/dNgN8beBg4Ievp3Nlkt28ovVd6u+aS9RdHv/iSebCq8nktJF4OXIp0drocHWK5/eY0SPwsXx4AWocanIFgo0LiWFQMnLht6OxwVpvcjRQJwmw/bjetBEASX5Od2eTFI+p8jMXl67oNyeAVFGOPDtzd2g5QEIOFTZzcQsrQ6TBIqtYgKlnl+juK67CejRBq1aickGjnjpc4VJ3GeKl6zTXld7HBlLRwh8cwVieetf2fQDOzGUu0xHv44zmSXC9p1y+mP9iUn5n/uXJwCfA7u2s/9/c13RBPiMpJ1SjIgxnV4+hB7QmBx7fjeeiEqicguq04EvSHuImmYavyalJk5gWcKoLKTgi2X9fj8I+EjBWQGwPuiQyCYpIeA/9kVyhdyyNEWJma43C3FPSxlMTs+SyvJhwctF4piuzXznTQcxbBaNsDSubX8zjv4Az3S8XmC3k+RMxXxVztqW5w2872FPiUTDf7FhQNjtCmIi2TFVPPacRqWbh9XKnIxJp51cD9m4lXSaC8PoN7LgoXpzjMye/Bx2WKu7V6J0uKxK0ceMMaA74DUklrEfH1IqxFDdohGak2WOs2ceOf0gl/X+sRWxZhUn4SLF6JkXZUfRHRy8GfhyLw//Cx2TdIJ6qHzSZ2KuxYZnyGbtS/ZIAOjG4A6KFapcXfyRt3GsbeT4jcZB3CxvWTbtGI1B2iV4fxeHATgKyRIir4enEZv/qpVTmOHn0921RiNhPEdx9anHa2Z9KhPlJy5My7PenSuKficDVVmT0qnidu2Nn5IX+CMq27pD7UtOkNO0vTTNDCh9hfog/v7X95DEx0uY4DbLNqI5TWElXRkt9Vw4VcjwivuXijZe4u/66RUqZMKukAvfTT+tDkLh41H6nyMPV2s8Tfj5/3GsA4wiP/zCvSstCvCS0TrD1q6C5Oxr+TyB68Bw9qtFSi3etU52kwUHj22BeRr6e7RKn+iwGad5R1OGmV/GAYd0y01F9Nz0rMvLWyE8rgnnJTiufEUyvjXy2V2zG6v4P+pGgcsPr2SXHJgsG78JLtp2IQEKG7A94ZAkyP8vbROUyh5cfy2S9sgH9HS0DBKcr3yx5SstX4eBRqYiE8WWjWonkw5bOGrscHFTUOs/dD9hgpBdwUeEQNARgxQbTUCXmhEMEy04TPJinFHSkAN5pGRjxf7ZoMqnE5/MZVRyuPyTzzlIl6V0QHD8yRlFtjZmw5OAngceaDZ3TCWw5glMi7GsWOFuDu/sfdEIguPzkT4Qm6SgHm5n2qQ1qVK5efsP29tYBZBeDHVk6pgetp7N+O65QKXC+0ZPRE+u5nw0lG+TeogLR/eefKwH2lrte52ASrif2in23Z467kQy/Zh0iP1VxyDhF4n5IwlC9QhzxMG0dnglFXv5+WhLc3q3q7nd9Ko9EvblLIGocBH66VUOFX6VLB6Zxb7YvmAC6dK47YmEbzhe8HGtP8rVgWIi1WvuUzyg0WyWuyCi/77fNAzZM9DPp7Tf9ZIMj3gSUzpVQd2aeJVnUqu8z/bYbPxiuW9wqcvmDetsgjAZgaPCYGG/iR5C/qjheTTnFzsZedsHIXkTtmR9hqs1S/cspyBOiIA3BbpERhQRNMbPves3MBjc9jF4q0+zOItDNhAqWUH3pBof6lo1i6DUbqOlhiFyEYljlVl/HGHV49KP3aIAa/wgHqxFYGDxYtGbk/giDGz6Ts3zjp2yqoPmcPcBu9uQjLWfK0zZceV45UmcUGMUeJjV1om7syaUskQBnBEiGE2fMrEz1XwXGVNX9dFZdI1zPhZnsj8Ml+12GoirEyOn+7BrmiJfZ3Kx591p8K/oYWxv5fzpsUgBClLXsOsYjYGbNQdq8TO7LlikGqzFIOArBRCKXrRaxybG1X4OjimIjS3tqeyQNED+B2TdenTWcnoB+j0UULd5sXH1oRHyzKBkIfaTT+zFX+Qq6MGRjxIkwCbyh72wG9RhwnG121JJkaletL9aeUuaohAC7Ly9DB33XsNBIQxXKeaqs+PrNQ40IOFBeVkQM6JvQPDN8T7DpSfWmg/6TbsNWLE3+805qWDsGUIe/UdOT1yDdqbkhy7X3xIVz1U9sgQmxHohDXwm2uc2uAkiPCLktfeejBeALIV+qk2RSGxzLARtqXybhVufbDRvru6ILy4ud/ZMEDlhvwCy0ciGX52sVZdX7JEzC2DFxraARzUhJ2ZMvC00cACwBIHFSxq8PW6A+J77dC0wFeyMIFOzrztAWttc33/tXE3Vfis0GHkiPqb2RKSyO/bJkfRZIndsdYJCD0oDol2zxxlNVFbo6qKqUZaEoZzDVEealxzM2iTx6rDRADDI+1tzj1ETqDnGJCfXH1drYQzK/ZWA28ABv0k1t4An4NQlYeqhdCiAbS3wLGm3fmgwT5XwnoEWkE16RnBnwq8CTVLhMJvi5RmNwBWf9mi+wghvQ69Dly+NHFkHXXxEEKlGjkBagpkr7YpzWrj4I2g9iSZhuaADmZ6dkYY0nMZW7p7PPidns+rWSrbC2xo0wNMV8ywtvqnHpYn67nC7dOoAWllgchuJg7csjr0iQz/VhnlPDEK2cLG8GPBaT9cDVPLsgYnfctSoGh0nDeQsxbXuQ9mWGbqYG2GhW9t8wUE+QnHAWO1hEaprUU+nI0ojzW4nlQEAO84+IFEdvNFNSDaDn54p5nf1peLcbCG8iPtf+SsxHs6COXyvbeF1GXop3wjzlxZQ+YZjLOm0qrMbQpidnnkWXyG0DuebVmZIBG9rOit+bOlOzMDXWMAFqSCk93ic8lY5gamC1OdlgpZSkDEl5S83pQR+bWjOferi1sV86amfFSKowkH/7DVgMVWqgM/HRwVfDotBuMoXxjvuFt54Y1wosKGLO+/RAEL9MIPiTTW5DxoYVNQ25R+uKuEX5FRRJXnSi/LrJe2sqFmBirjtngWpTJc1KDp6DDIhSZhpTJzmA0XFGdofwpPOYM8YMvs97iG538doM8KiBOIdxeGVD9K0pIiXD19VfZ5YNDiX7BYyDUVeMDY/LBvW4Oekw4v+Vw/72xLsKKvi8TsSL63niUi6MRSMiFQAdV0EQpcGijFhajOgMtS2ANJBK0BaEKuslQx7kdZMykkBAhRgvBw6Fzc+PSbf4eJ4ZTJQbd5z/s2pQJivoC9OdKMcpsxTIW/AJt6nfzrMELA5yvyyiU3f1AC+G3rhghkoApfvDrwGPefg+iwv9uzmH1vWn7F6xkZ6WNkk31J2fXV6nqDeiY7p7j49W2Mj5UxyoN1BM/cSUW4Hf/5eMfT1Z8QpJZU3OxHdmDHzseqQG7bK0oIAjVR68P/1tmOMTDRm73nvWvCdCtflfSiPpoWRb4U6TmCgB4/WLtuOwG3M3m80yK+dRzUlUI4sKgKr234UUnGtOc1+U/FZEwpwSeTjs6Le5zgAi0s7zzXPSeA5z48Xc8b3mo2JlXcAzNqfU+uQwZMIWXfcvfJ8TeUJG9RYqMrunWv4Vgduas+k2D1c1AfDpSs/UjPqdNx1WhQJ5R59iBzGZroAtb5r+srwrBtkc1YiOwr9rDJcAKp2aKjzh254t6vvhjCVl30+Zc6CnsG4HV/PTDBtZkzV0gZvx6zQl7S+OiIgSdhXS8F0KLBV6WFCWMMZLFdQIAT75FZLBw1+JfAwovvP9hPi8FQlZ0uDNTOYSZCPFQXOeo3GZ7vfjNj+2/NVbP/FGfUMn1EZ96C/uceAPfkUU7O7EvbmS1UkNqQ6aHAvo8ETAexHgPFRwUMDAKR6KFsmNIm9jpJrHenUm7UF1tqz7cjdIpA2qR6RMx3wBtNqyZweZceVKAdQmJDtzzhu3plnDczMPpQ29Vs9LJshm8BxgSwkfxZFibIR0SvHOMPcRM4h0vC9OOv2fa5lY/Vx2qB8KSGrWGSYL5ygS63hWXa5iFqCRO+eeSvS8O1tEBJhd2GUE69SjDt6YMAeWzRqdhcCW/JZuEPwFiMiaLr/GeHQu7wKLiZVVm+VVkhtaz0qEc+0vG+ohnon6ZXMg7Qwqs190vYjhR2k9REEmNpt+VGkp81cRaqsDMoaGkyLxa/F3SuEm6Xmd9Ie2AsvaWLcaOuscd6ZdtdyAK8uRJjad0IETNbMDd0mcbENi0TXBXzEDsICE6+76r+yBA4/aRCMmD7j2LonciA0Vjo7rFZ7YZABmGmXjF5D+HX/ff+W5SoE7Ae8jlOKsCjB2ZMtuUthmZgX04wpbK+L2q6Zp8KrUstAA8HcTvQhaUA8fBteVWmitgnJAPfzHWyfNlLfm2b7EYUiXXinm4ZXD9q3mVApAfIQ38pEgAVH05EQiofEMq2QNT5zLz86GvUAhzf6ddMQMjseC8eybZCWQV+kD7wJRqtuiCYc8ey9SCnf0az9nY+PtTA9WUz4qR0ikVtIAcXyytrsPrzWIndt89hJKr/bA8o7L5+Y1GNnxrR2LDctNfge0YzZwNXjpIIkvvT2c88fYiB3PIZesl7hF9rt4D+kDCYlKTotG1EM7NIkXMaozn31t/GP7IC1C5O3xIDayBpwKgtmMEoDFUURqwpJ+2PtLqGp+lYDMhB5BGGPvDNuuyswKHXLdMqRkL774bRHXheloEMqktK60tkyDuNPd6E+jGERzyMbbk3iN/VFXo9oK9aOawpFwD2hVDrpZ8fS5KYob1NRozde9J7ZnLTI0NlD0SaMm98M2vfN1/S/P0M6EJucb8WAA1y/tY2mpaDs4kww+VcZe2V+P7CYwlZ5PA+cB2qmKk5+OyP9LvGjs8O50hUhb0zMvMhh3eijHPgtO1v1ddJgUSe81ns+/9VVzo+TFX5tnCU26mRC7nzdy1OXRXtN+kXlAlnKunkoUoLiGsR+rwDFYswK3rCihRWqLWNjukJxHrCAgOq07DYWTHECB5MYgMT+OSy7JoNdDgVTxHpGhs0+xgkcvTyDoZaethhPnWkz2RHllGEfOgLxsiAINata96QJsA5UOfLVwsC+nOx8KQIxoNaMWKvg0Ur004A2NhPo0eYeuc8zxudlkcrHnqlCtidCXBAea52MESArNEvk+ZpE2JUXPKznALsC57GD7CIALHLK6aoFYrCJM5vbWJbFwFePH5DyvnSJird0Kq60paXGHKxNjQE/iWDc19s8L4bOKa+tFC5zgZW0QzEiE3h3WmXc9lkHk5Gx3vwQRvIJMnyDx8M6Sm3fisxiFvbqhHNs0slf2wqNS+rIp4+DnmHpavAb6raNnPKKTlslnkG1DFDi0ksJy4s+pzQ7alwGpu58TXaia+G349beP/m9N99ENl01/fAuzfU/I1vCL8E4XbgfxtPRAIEGj7Y2RqE1FcJefcHV38/l9I4LmadFdv7fkOHrzFx9HGwIBYkrzmLqVJaFBo3/GxRbk2UFB4hXH3NXBrHTH0PifIBQDftot35zD0BjYT0mo/5ru7qKTII+KT/WdJMI0ntd7iuLzrkJOooZ28ElCJqO2gPW00VHAb6LH/8pGqF1gRmSDLxgdhE2/+9R7sQjH5qdkRsy4YW0KgRxlryPT02LuH+Goc6uUFipwL05BgfFZdR1MmloGcxvjwYa4gehivRFRyF5YgDBL0afLPk7WTYmgl7B3aq1QINtEh6r0jOPvcyefqwuK/ZrT0C/yc+2CJpXd1R9Z3SCLRfpUEeeD/dWB2bH+mxwMcFZaIgqpbldoMnB+6CNoZcbJ6fI39tseAyjX6OW80C0sPaTfB23xcyuKw37rcWzX8PNxirkqNQOGjdpieVef7kCiTOWdnrYyStKkiWucmtK/cFZeHWoF2BGDd/S4uHB09V8ZiI4Fd3P/7INn/luOUqnk+MvD3Gvr/oUcluke8bbUZcPJX1rK1P4vg45XdslCPC5d7EoAvTKGMvWoyKP4erVOxz7hvYbU+iX3E+lBijv2jLWnOSmj2ISiemQss+CSCHTR+Rnniv9HFC16u8trK3u4Lsu/UA25qXdQjXviNlVQ3l23DCSXIHkiiy0YTiGivg158QN2MgVtWD40pTrqtzaeCmYTqkASc9fvXObVtGDXWs+dvYpR/HpMuVSbOq20g2mYyvmRp8tAZyl13agtuzVZpLad0+ICFQRdlMVkBTKGEANxKTFho/K5KEpB++lfN1mqPC022oL4PJtLgDW0IcZBw5rnzEI633wTz0w7WQaZX5OXjv2f/OT8CwWSQt36si7Ruc6aeLoyTQDdGu42iRetZmQSjnA3EKEnNDlvtcjKI8Ty16WoeGSoJteYwxfY63CKOOpDDdAjGwJ8Hpwil2YcYDcBPMYOMCi9m9Mb1lkO6/VZKY/WKjkKJLk08QJ2cGpfGKwRjXzniZe+6tBXjk8e4DhD4gWsz9hLTXQSfjQJxQIy+vqxWDY554PZUNcvKgxUW2HsOjqLgSxMsJs/XAFxlTOGfEoaE2S7unfKnXZ52i+KK4I3tAkaCRk8MAvvf0G107P/HatXKiIp8zFR2zo2Rpv8fTbnQSGSwzR0V5QL4L20ZOBqA4O6mNCNoPvuwnxkq/iW+jSSrSoWwapa3WKZoHzKDrRBQ4oLja2pJqGv1VdOTMo89l4aHj5N1ckiazRmIWZ7AkVmsCbtiG16aPwQJOn0Y6Co2I/znV6U8O/75GGd1FraGZDEJ2TBbL3acv4FF87tngxLnxQfidoB1sNCGSpg62MPT9IvWWkYZh2I1vg2Faf31aoQJNL7xXsqjI5FM9RVpnMhaIgJgQ8Vgydh3XYv3vfELHxzpfWkbjhnFuXIQjsurouEIECZ81xFh1pjsBxYIxWQABwbyPar6SU82oBouG/GVZmppwI7Hojsn8ngEdeZOR4CF769yG8djUBcA/HADcWI7Yqs7PQNAuByI4uUoaYId/vhjrdtVVDJlGpDPtOMB+VTnsjVFzlZpO9hP1sjMxGF4humt31oG1UgdDtX9YRGjG4zlbiysOH03ZgUU9undxgrQtgDF62vX3spNuTN6gOfwZDdiGldRHzrLkWddWSpUbkN1+QH1DPowUouORcdak6cyyRqk8x0PWxcKJ5GYd/GqT1TskpuXg5B4WlGLik3By0eRrGRkMhZPBBXsthC9voLgbuAOHv1dytbMz47+dQkl464NkMh83yaROj/B+33lu3COvV1HKSXaWYUh8pxM61g2dL6I4YclWUlVQOP/FY1BVWUBbpX1cm98ZkJ+cb+ACmh2NwlQ3+9mCosZO+qQ7zV8lSA+1KlDK/jPg73GZgCgQnk1VMqqD6TbuH8KAeoqTMiEU7S+E5SsjGCBBDJqePf9YEEBcfzlqQ/LVVLuiXbRm65fjp9QN7NbnXLEGJ+DvQuemKjGHH+P1R0hV7vH5kqInSkpsYAOo2OaryFfFkhvDLb0mK4vRc46TdrtGavux5Iyhh0MvgPsY3cFT+U5lNeiHWvooVB2GKcYqNF/z9j5DZGJnb19o/JhNxDEM5YfKIlJC7ijCIPYaCrZPl1kbNYPUpNxbhpZgDfhPvdDTva+8KCzap5By5+rSNyW3b1J69zOwW1X1lMJ+37OwoHAnpFwRpnDX6IxLHVAhtVs7r6niOxnhrQZTERpcv6uKbyplu0sp0aY6+Kke0yj4allrhWzF6M9GI7D7/0yd8ek0WV6EW4ZhmMchByRHaKsZXujsRJZHOrJcaKosHSxNB6tK3N5+jaLjZfUsMQV4sw8PifrYuQcfNU9zthsqGsSErEWiyoQX0fGu3pyOrELSlAsnkkimuMhlLb5x7x2ry0f+DJc/oyFvGYhNVgoxN/sx4t8KEZp0pzc1FfKAJJzmRx37Pj6fb2zOvRTcDMg31V9nNcrB63rR+8AuLtvwNKMpzWVybAAGjRIyof3T2ImdJFBvNGnfG64VXE8w5Uo7uWl7L1FcMsAcYmZmWIhfbYX/x81CTGiVyHd5HwC/dbm6tBjytdOZGelhHVsMJ4to0W3fnj2cV4KVWd/DLYGXV49KWvnQ3IaQVh1/3xV/VuMhmKGTFNyhqrBqKobX1eo6HIH6cnIhOliL2WC/SD/c4uQNYQyHQYR9UJ4DHUaLxrSdi8ENzsf756KKdhATCIhhHWLra18xCPW1S0pf1MBWwXONH/cyjx/o+2R/5e1QLR5jh2U9FQb3l+V8b4E8hki8IJwjgZ/DMph70tP5Il8ULmiUPojPsvrdMzk6bG27hBbsD8eQ3dyhlfaLAcEd36aV092r22wb02binLW0FgI/htKDwYzvTkHCcVZKJ7Ki+0mvUwrXs+9Wjm6cE5mSVpL50tanrYqTJSch57BGUtP/p3VO9nKEd1iP0l36VakX5iFtviUqu751HhU3elcH+hblMXo5jYlhc+VRFzkDF3OVXkhsCQpbr+7HdYCDNe2OnIfYQkTYBI+iub57U3qqtuZlSYPJyDVzLNXGxdMUAGwZIMhPSQcQs2/pfnHp4e3YK4JANa6r96RMZnjHNkJjvUT8SW1yJoVm+fNnQpJ7XSj+lBuI5h6aPKDpeUtKCGdzIrEHFkMvjD2vThHNoApbc+9AvjEvPmxdMadICCtWVZYHai5cgI9hOpo2/SHXdSVm4kDLWgyQ2DwTeQ07YDUzTe4sTcl9c1t1VnUX8xwvRjRbobreXhvoDo+TvhWB5JbUSu4YBDvfmH/WMr70cBy1s7sNv9RvyQHTy3AlGV4MHWhrsTW4ppfUFmraFZ50ov9fZF3pyM5TiuniF30Mvrb1VHCHPs645HG8E7jxrVOR2ZvrqMZAINlE0gNWQ2FO1eXiFmiCo2Zf/R5SATrDV6jUoXujHAOxE2E79BhJoySn3SDBhTCn2y9lp0rYsQ63+V41rwOE6TDYIM23m7qg2vurlq7+bpyQeTTJMpSPWTD5WRbQ/eZwtcOY/Fyy+FTSw6mp8+VFo6o4BdLIYjwcDa9hOA7NcNOK+7sSSl2L2DYpnV6zVOdTSOHx97keiYeVcFW/k+WTVJNicb3OIxcd+JlUSHPakfgOojFste5a0GoKsTtpnCnJ5d8j8yzh0+/7xT46t19a30+c06Zl8uPpmGDNZHUVsWvUz//ule4wBkhmyZVtBZrSRcVTxyJ88PDACbih49I1QWAsjiMAx4AO2GA0R8euud8b1lEWJ7XCJyDH0fTb2GSUL3iT0g4WnBqQBr0tVVDDdpbX18dbJuJnQ6DlPnuNtXEITAop4l0uwL4DRfyc2/gkqNSvXGn96k4OzMyVt+Oh+fLf1ma51Az4vOuXB81s1TKQRmehqD3le5HTlftg6bYGTSn1idzQdVNNfBMwZMs7M+m8rGNhc9yeFGQncgsY3/COvMDcfxVriuoMyZB0sa/Ek5lDdS6hkOo7ojFBwjo9jO6ZdF/DTwHxUJQY+s4uzBAN/zwu3F2dGggCoKngjXPW4lGMDWFaQbBEZHH21TtPv/w93wZlFhapsy20GbNYzjLjkEQfEkl1nlDltEk7CtyVZN6lo1AxL+cyxqA0lAx+PaHXAlYgxP85tL0M1GeA2+DhJTGAbO38MY1aGT1ibnM05GqaCbTpe46wYvPkDZKptxeb0Tpw4L0wmR9XIMotD19feudN1eleFVkTem6D7WRSL8ad+10HQuLA0qn9c6EgteznRACUm4P+3JVcs+RpQEb3bULmcbfHLNeL2nqFIhakoKqIZZb+2rSKGWeqQN/nnYcr5nAEboP///fj7/QdrfF4U+rad0KriqFUNeXGSYrOIaYyKdjQOxGH7WTi3FR5PCKW2qDxBwHGGMG35NQzEg+bg+XStHlzwfjruXF7aZAT8IamGuGqENC8fOIwAoHQA76Kkyhkd/DNwSqSqWh8PzNHkG2SV801s84CpZeIKjypNycPA7wjmojrRep4AH6bHTgFft1q6NC+rk9qPAQ6bpR+QSYyoMVl8KYfZSuKMid3cDZEpOpDE66AbejSCGrL9tWFrqfVUhKo/Wsnsh95XWZpxmRTkiVOyfkYIy+QuRPXiGDOlDPvtAUNnKrDCUlIYB/pBzRDjWQqmvBGZ0LBJndlsS1ubAC2cc6t9dKZbkzvh1GcRpz31iQD3U0MDAUWsIedK4ZbW45zGXvREk0lpBmVGh5me3wyHIgy7R96ZPtIbdCZTZswxvCfIyw4yyU1Xy5VvqnF+xqfd4SxuIORpKHboxo4AsG1m962eMK2I5fr/pocFNGtKhXSQ3Kij6sbbEUIQH2LQDUY1l/9TL45yKxbCq5WwtBZoPIlrpRjbswNK8r7G3C70hsfkjr99PB9YdD/OSEUpzisiozdAblekEAecIEq7K8LIWLmN13ZgbES2q69Bc6BliLbgmJGRe4PDECQc1QhdwbwF3qj6n6+UzsmtBajnkRnCLuOfV86p0JPvd9aSJZ+/maMCIYN2b9YhjhHI3DxyK+snYjbQXyO+SAQtwE3CkUyhMj1tdE3JvPZjpq26LvPG2kCIXk3CVNtdzgwPgJO+sxqhirwbyiNFQSTqqNXoqHe1U675TARdRzOXjYWbxePp4m7tcqFw8DsQnCuoHXk4ZNgSRF019RcDrP9CfLPl99QRs57lb/YboWamFk/62egpvPKRTFlWTsdkzx1f6fr1sw2WcEnvEF46i2T3COWkYiIe0ZXJh+aDXxTcyrZEMTrUO/c13pLVWsnTgKH5h2gPgg0EkYAjBr42eXbPeGzfgBaOEyCaIufyHJECWEmtHtmavLESFXXaRDarGbPvOQ2LiZqJFvJ1r9dcbBOndJVjVhPLGM12UIjloYny11Fzm9uF2Tq21Fhya2V2k2b2ndjrZ4fyakNl8RuKJQOPcChgpfY34CB4FkaIl6B0ZN+yIGa0lg7TDwpg/0zGVy6xX7dzZ6vRe1v0dRjdWNanHLnuAvJz5fF8ldZVyhBQM2bXUWYHZy7VFy2wTYG/I5YxrU5Z29t01s3ta6f47i0xiqdtQmI7lqxVzH6XzsGJ78ZdeeMULXrVqp5lhdp8L4MLZzMTfBC0ueejS7NpdB7Di9eb+vJLWxVXnb2ieXl37sUuy04RmK6X885Y9IJXLvGiSyMJtKLusLZwJu6pefV2jW+bo2iKdIBtidUgku96utiUZCyr9wemB+RwYNjo6GiEqjdv7xolSDILY3XQvy6Tx0j5/PD/tFhsQ6uihNu+fsZL+CA/ag9llaAvaFRIS4tmksU3/iJDXuaC4chg44fs0YQmc41v3SUk9KFcdeAoNMKaZFfJVr63jjOKTbQqeWuHFZMJlztzOYh90LCp7tsQ7XhwotFD1Z0gvDeONJRFNNKWnjaCQ8LqayoptRIO+7p/LBUM1tW9B0t/nkD2RTeA4W9zjFPlX6kweWa2NJv2ui0BkbjPUfQUDVtQb0rF1Z8cCxkP+KpsZomqq7luFmCj+Oosm3zcqpoUGF0HiVBAEdQFJjytIshwnw9sLVaKYUOkb/gjHmICMOkVXMChPcu+QzVDDWcVBcnXLiyZFRbm1prUGGI+D9ZYGZpgyZgZ3/YPbVhFeSr9X/diqzIzWpU7fmE5H7jB43cGPHoO/Q+n03K54/lkjKPyQ5xY8FnHE9Gua5f5LhwdIlVOuLsUIHJrMTfCaxazqmG18/5HijL67XnawdawsjhUqAH2OSxyHg4ep6YjLcp32uqy879hkzpe0sMAB1xg78UwUQeFMBejdREWOxFvuZREHHmO6S6IsaFd2ttELjXQtZw0Ct2Aw0zlqLCn6RMPtJReMki/i5mht1MqYlbNJ/o8s/xNxUzp0b7F/kqoY6JwXXWe+bNPPg9JYFxEm3/0H00AN3wAP2dujy8CL3FOO83azRG3ZVJha9TFfjHh0no1Tsi5HzyjwYn2dEGrDYPRYiRhM0RJ0wtZyhEWXt/lx3DtDgFPsXilw/OZDTCgxBv6tFYzkbCs0QX5xPF1qkejKOh7X4jE7F93Q4PTL0QyH8+3pi4suG1UJi1hrE/4Qrnf7lJ2djWu3+aLJZibqeaOuXLhpnGNwIP6mdzqs+5vXR81hEOuSTTDYjiLfWQg50iI+fdqEfaVFr1/2rvqUM4ubUPoD0KuDlq0taxVx2a3dGxGZ0nBjRuLIAXxNtn6tjjlvXTpoy18KR56GlKzSqhXrOwoonuRd9G+QAPQYjGgU73OQu5EawySGyFv6XTWhazpnN1clLW+JSr+MFO8UuKk8o77YgP0MrR2Tb8FLxU+EOGJ76A1dA67OBVuI9Xd70Sw2aRCyiiWZ8vdDMooxJQh28UkKj5HVv6nRnDFBG92/cq/TSOmt8gQGpMutuusm7d4tKPvrQPewqjFLw/BX2oF8KBUIxWEDA4My9r7QiySNhfcle1unr8gOazde5o52tmT0LLdSmalVPiufCNyaQ/30dVL9X556zFFM+UvL4M9nrmqI01wWWfwO3IlRpvS1F6cg7gONg/5jWcAfucKeq+jzYT3Z1sy0XQD+cLrsrPy95zyIpWI7W7jLHo1lHsMbTwuFzynLzdrUb9FVF7p+unx0hjThpqiu2WGxkNI8Z3yh9FD7Gv5fc1wGqt6tSK06PIwM3FbeER9XJ9EbhdKtttwaKbu8UnDwOo2WVxypxHkZBGO5m6GU1PxQbiIBw3m9bRrMzADtcmE+GcT5sYsWNrzu/Ehcask1kJBsi1pbUF9fbkF6as5Ib9R2xQhkdeGL/QfrMJTz509gXFXLBVqSCRYGLatw5WFkgw0gc/YS7dKqe5sx9/FWgCo8/i2FS6PsMLMTbN15KR8reC4sPCa0Cz/lvHPKA/e3N3extYTjZM0p4UvzWDteKxcsZYluwrSS+TRQxDfpMhjUPhFeJPDcGu9NZCTfypzYIiR6a8u61GU7mrDhoCtVpZBNh8G6spiWg3Nankca1LTAoG3JO2yMkh7Y1s4yVYJQznZmJFJ9hbIYB6Z8fMiOikZcGicE142He0lpGZBy6kMj7e/D3op+UoJrJegoPkcBrsfANRpdMkUTsGJaIyq/bWTJ/bdeMc9gYKC0QScuuvqY+/2Davx4g8IbWGz/9EN5RS8F7edSVqZX3nMtomD/ZdBFCasLCmyuhQMDmjEA29zGe9hgixalofpDWRoaJYIUC3raHrWgtc+nREQoF2vOvL4kN8YLLVLGke56R04SFKQXPt0ZZwnxvFe56ZWjWUwzeYnml5sIV8afgAwPZ0jiKZ+lRJCLChtVdD0PxWq/8Q8SEuTobL4WN/e9qp4Woj675XS6yMYCrP1fCY/c+Fp7mhtBr91CV5O589ohAlvnNl6yWXJIYjlI3RGQy64qHVFVeMimnwQeaY0jtNN0jKAaBYYM2ThI/Boi9+4NpddyRE1VAhne0HcZlnwTls073/haGJjZCD5ZdTJKpm4PMHw2iAMKUnoHErp7wpuKViUNjF/VXUek7oMGLzS3YYCKuXRAlnCA+HTna2VrSmJqSylClj51E/7HIXKyAk7ylIQXB/pd1x9/u0x0XiApVt4H6JGf8DGwA+Uak+PbBFHYTxu6QbszsXlYX2O+SO+hv1y4ZalJID41CICrGWlus04IidGLmFQeoUa7IElSzHtL96chIdqHFpWBUwEhu/XY5Gl0v7KIMPwNi21R9aMn8jrv5C8JAY8BxYZzFR1RcJYTjSYC/Eq8YvHarMMrjE9HM7k8bhVBQAwlW/2tYH/azySw/I2allFmPS8+duPyMC3O0xlpKV7FYCbojcnMUxm9UG0IcoI386pGA2ZjVB82bCXpf7BpC8qAHGisqqEVJl4pDBRky6epmGcCOK9Lx9R2C5hUXLqAAxDW99v+E3lTDFcxzJJtjTYs/S0R0BaCQrCREWowCLqIMcT5z7g8/T59oCNRTJkwshYyuybJmLpAFTX/TmUREWhaqPtH3u6851hxWOxNjtSNlk1VAJgQhPj7JLXHmSu0bsjQjstWO1ZTC/x+1/p90dqKvPtNfM/jOcvnXubRc6U77lsppvCrV4xZdTQFI07oI86yH4Zno8cMrjkGNFXZOYTT++veDUVp91MA/FkKm1qfJYjwvSO5RRYu1VBCpNMq9MlYrlEYM/w92aWkUj5eZgfm10uuO6I4rAvHORiFSwOTcQJJQh0Z1FPvQ22YeJdlii/gQDNFnv7quC6/8NWhCt0wHDrrHVGjt6ECoBlQXdTTxwlfuclJQPAFhaRqTuMA+iE62OLW7ow1pBiudmHAWXjHrgbkV1xDKZxGq3s1Prh+0fcL3yMS1HITDJ8w7oao+YS4y74O+pr6TUrrKvg0EsjV1UmKcM60ahu9BVKRzD+qxaXSp7NpCi3oerzl2TMsp273CwHLQfvaUYcTM7q0qgMAMG8nRfh2Gy+Ia+cZCNJyja7qa2MFSrWZHPr0Ke0+U55Qpx1QkmFP/5mMceZxTS/1P7yek9vYTxJYlbiZIMR2GwbBz2WyseiOZe3bDRm03AG1jTyOgKjgZbqu0fKOmO9YB/Zt/qbTsIYcXeNWmbWMngf5PlFDzR0QJz2PSOTsUpFFDe9XIRuekkAysFE46R5dcltEV4pWamlObTzNu/A0vtGvHvXlf74LGmWtReFhycPo2EX2xu4lolrt/UDYjqurWQnNwX9DIAWvGV8/ZvQGGYidJ8W4nsLMuH/Lid4GopQgSHpoq8rxHJrPde29nitYzm7vOW9wIs90IZdmDcDPD6nqS4Kr/lTImbh2fTGWZ3cD9/Q/NUZ+c6Nah6zxdEGQtcQzeMfUpskfExROF2MdskSu8iFqyK4xGz0uDhMoUfJm3NvVc/gi+HWI60aqG9UCScoTd0NZ0VBaTGhBHWdzAfkpm6tubYXvtghd0H8avcRp5GGrQq9Q2ioitj6DwZAK0ndx7tD0ZNEbTynFvZU+VxklXxcj1T28rG+5vymcq7FyL3d+n/ZpQbadLCFXj49ivqojywHSnk096+TMd3Atm1WHJBLEf6KUBELHKQhmx1NZe/fJIe1QE/xWLtwTNxXKg2pwwynA4VGOGCBch37mN2enup3UVCBbqXjb0B8scrK8X9qUHL5G6c1D7rgGf6jdp8WbLQiwLUqbA9XvRc5lr+weVTJmJw0PGL7dtYM2TetNLYRXnuB4fO2h85xSHcOZejeE+pqjO7VOcmT3mSAy1PGVdWMJmdW/cFWNGs3EhxsD9Y0S7OZeZHTJTc0Wbhwj1IviUashRE5SdAPH482oWDtlpZbBkH74zfWmyhd5/jDmrUc14XmA5TzR7n7xtEVyadVMhPcJoN03wmzOe9mntq1jg3aVwtyhW8/ETJpejkaoxvUF3dwZGe++lC9PiPY4MWllZo6RAe4AtWOuXhx0wEpXMZdvPC1PC8oSCmJk77FLb4B7SY++8CMLEvJeoO9m0TzvV0+QutUczkbj5n4l/wh93DJJBKPHM2BkG8DiY7myn07U8IuT6e+JSgFWbicCFslwqA+vpWgXHFHmC535YWoF/YrFkvxQaQ/A/O+g+i518x16X1u0jipVqSlXTnuwa9OHItpKftxPMqOFOId9jLzrBvtk72OEXUm+yo7z4uktRPQ317S9UU1rTC+Mn0G3QPVXvYVuWiZranrvOtRt8PgPqce4fW5uDiOrqpP0EwAWVFPK8ijuYp6d0LFhnrUr67qRC68OPCrAIpUOSyAFXvcpOaL5Wy9jhcWlBomKujuKbEMMu9CcN0U7rtKKjl0RHo6cZqOiFVUua9RqPoK6rVw4GbWZOgbBAQfVwTPsMKpuA9u5igPvmkk/Ozt7MORjucFaaIb1UJfRGSPhrRGIinOcwvlsTabKezCWIjdnWsVORR0h1VNDCVP+diIn40eoYpz86dJaz7Zqnr8V4f5pJqIOPDFagRoECPed7F3XecZHWZD+rm/lHihGSq6JTd636ZcAlMGlIii/k712hXS/0OCVYZWqB65acTKD0fm1KKWKS7HYwlBpUdoLnvBXUW5s+sJ+zYZl0wahb3q+KX4oN+wTMl6MLNh51atbxpd9MO2GwE4CagL6pW5PfDtxCLtNBdOZSt90M9D58OrfJqrk8POX7O4KXB/Mm2ComFH+SMk0Uq42YfH2yWDy49GClFBbVN80wz96wqVaE9PkQpKnJMGtBMG4yjmxGo4gz8WjPnw5SFDOMoOUJ75co1umTmknHG3gKX36bakzGwECZz4OgpleMb3yLKsYQWUx81Sllu3yX3Nlpz/XmBbPQ6EyWhWnpDWBiBBCYCVocj9FNYoMdKKO5usYWIbkqPhR3x90nYLPWOsX83j9amhZNlLBOjvM+nGtgNtDduqcI7NMkleqxM3hZA7TGMFbOwoJg036R0F2YNk301YRZhlemev2zEDUmzQFiLK0dIR068mtKRV2Nd/XkqNED/b5/Rcp0yGlEkIJvgoKB5UWhxBQ0sr5Qv+HNg4A6vk8PCZUbu41bBjPd7sa2V+8oSclgpiJ8bdLm5qt54HPz0zh1Y+YNtUjH2DCx9kxfuDnMQrjSklLef01PO+dfMUFvi2g0eHmQ8Bit7FHHwFkp4iSN4yVk1EON+I7PRbdXtvhg2z5R7Q/E084f4mZnBb4VJh7PZO4QNHHnHAQbA8mnEay3jWdo4g2aZstAl6hV4A7vFM7skjTgD0iqy4aYE6RUqgUKNvNAUvPkk1vQm4+g8SroUM2elRB+8r5DDDp0vLy16HJv55Ek21+J23ky4rScpMRwUTXrIbGjlQOLzarQYv9qrZPpcP0QjEGyEc43mnSp68bRTunRI9JC8fV2gry8UD1CMUSon+/JKgOPwUEKuglKRD1J7RbXV5cYY9aAh9ICoqyTOJqHapDoej3mvPV/TP+XJMF3AgQOJbn0KLkj/GzWJyo1Os9jVPvsF01IvulhSHHDhH4RLALQkzFJtqOvB0MzlSw63qDBIJQvZXFqU7wo74QzVL2/qDxm+OkHQXgbMheGTbxNuf1c1MQXo33TsJU6xuv6SVCEt86XXUHRxXjlgMQyy5hZoBx6mXR/FFbJPgNRrRoIlQ0m5BaBoOm13wNSGb7W7A+UTMIowuLXBI8OtLF9S1GWHO6Yc9PhPv8l+GNHKyzg6VuurbUif722HcRCjddIUTa3THZmkjA2SZwtqMl7FMZW7XD2JhxZ4ixE9EjIzf2e6i7x4VQpJGqqSkE3Q43sBQhk3mp42RTAYhI5CivgAaS9ygBEAIbaOcTUXht3PiD71BDGq60HnP5YeMJjY0cE0lhycZXIXreTCDOamBy7ZQwYSyBZg/w0n91mAVitmEdG2eqQY3Ti9xkrUmdr8gU+bFwH0qAD3WWjbuiL+Q/IZfTnaE9BbZfs6tbW+7NcHL5RFV9NiQnp9TJlJsjO3D3KCW/PM039a15IPF7RQKLKyHMfsnr+X5e+O/ceuZt1/EX+P3QX0xySlCTEbmrdK00H8s8E7iuZH+qsvixC2kTj6ULqmn11aIMbYgzOXrIcXqis/WmYoc5frQJGCE34o69kTgSoP1jZdwgosOaWjpuNnkcaKTYj4VsF0iJXe2Bc2dv8Zumk4DrSKurf4eIvQqweLLwdExP89Do+bZRVSAkhExfzoRAscz8NprZMYa1oDlCRHIuTOQ2B3Z4w0Ir4nbUkzU9HGIYlpEp4R9/wS8MxKQEEaNqQpq4qoz1VOlM0klerPlk0+8913ImB/YPdDFXfXazxt3Abugvh9M1hXjHGYmbpjIvvVLy0rKuu7EpxxviXvSL+jww6BMx2FGA5HBuxtzjC06GWc5PK2VJXVkDxUTqxIpEvSIkqVn1fbRiFqel0Tlmwhr+4S1C5fmmoOBpaQ5GE01DF/UQaOwEdAhcB1b52yepchyxatXjSj6r9fSzsDTR5dhE9sme+LXYP9gan9IrSPoJKSCwHclFuRTkqVQy2nxoLsH1Ni0fONHlZGtpYnoq7SNAMgI1CgsZ5tJ+tF6ptMDTJSLIeCeEspj5CJTclqGb0mo4l0EOvt2jUTsoy4y6Zh+5DaQjw0OX1ctf0ygxURyuNbtDNCnfkJsaQWOi5Bf7IPm4CDWCO7pZs3xMq/41zdu7RdZg9IzTkdurVJLUvbS05f2NIVZz1j4vw02bg59umXJe8lTWjh320J5tOVyGKEw3nKLca23S1NzkHbti7q23EKbMHzgwW4afnsmc0aV6MnAdzdFjH9dKHtlpAcQFQdgCeotuVVQl4Nr5sqG0IVAAuNDwfhGlC7Jy8HpPafixddDukJYBDCt7WFub37+bhFk59s0IGNFhCLHErNQIyN+OPxxTkCc79TWwzizdTMKxGZnitR9k2zAunu4zCVLKCJS+PcVIXFt5yNQhZgbHHYB4thYUZKG75iPPxxm5g8g+uiGk56y1/oTmLnX8/HmWAGjK8+IN1EcaofUaByFI+PtbtQg09iLV8CaBfaBNGCRAk693UnqUUuCts3efr2v5v5lUXRX4xWxxpryiLhC4npCRgpOyPXzJsNfL9TxYtzoSECd4RKoCsQOANNrhxIg5p235+WhSNAGpcVO3ghiHoZsn9oAy09S5R6FJv8q3H7GAMN7Ua9L2/QxJHd3SJ96CKupn1pVXESRK8h8bGh9sD3OdH5BjVjibAPQfzcV8bOekJXAE9Pc3Sp9rIPdrOfxmV9+5V9WzRZLT35Ekm6scpmxRVFXcPrHBE7CK6FVBeUjwV7Blooh+urOLoiEVK6IBizeiElu+0QWw1w/sv1y8mFL/5/RwdOLY+NWjode4oskXGOnYe20haA5HuV5hHK7QjyJwxVQ4fJtG/zVBxJsl4PnC9q4j5rqHsIpo81CEKhnWAgpNp9hCJljnSLNfS1kK7Wp8RLUgBLfKMOqoZpTG1TNbpzm8Jm+QJ9a9ruQrcYjb5fFTw7kqBNxpD/AaXNI7P7r/nunddjin3uRtWHkXWYz28DeS/nZ9CL8dzoppjDad5nJRkwZi6xtJz4UCWln6iwQrwP5sSpoe1uW+8N0czdjV7+6idYb3b5y5eLUmZvrjKBgTsJ8OE6YBDtOSATEn07h4ZSEWAKCcucS8OHwwS+M0TrxJ/HsGFbIjmW48QIpxbNZlvxSOnQb6r73nSizIUTSW7V8b0lbG1mMvlCOBPlRfswc0yR05X7Rvperv5QXnklXxBbtSO+m+Fx/SORlUxzuLmH08txyXIrZzSRdTyhPcyzDS+aH82EGDL+9gk3/D05yFWW2gKx/pZpxnps4o4jM6pE6QP6g1fC0UKOV1fN9D2o0HNMUn6LbG3K+KJ/6MVZmaJAoS4rPG3aaMcgR+2cmg6f9dozUwOfSLOY7STlYbTPb1vIEo6+olvhBv2JSA1XLREtLMZiU/laE7+z30Ffmv4njETTiMskBHZV0Xw8J4NfLDHsQCaNZQcdMhggV3ssULFtJxj71MUL4XmfjmJOaG/7FBrl7owt07bWp1hM0oonZnTlZhdqB26bXDedlKYp8mWQPi5iAk6l/kvrqDNzcvaAZfaZA4yaVAzzMK+5Gq1VVSgM9jzVIcGAzUSWwTGGwLsEbE9UQNhKcMFgJyzHk1W1Khsa72LOoxZcaE/cJXlO5IqH+Dtyoh0eUBHlob4QW1oibvwD5bXU8LwnP2fmTxhRB02HvwfzMy4iZHu5uelOZBYNEig5mg3vyE7Y7/AnvM8ywcdRDI3ryDs2mIk4Q1poKqBga2IaRgQOQBPc8ivaNW722j54i7TRavgCYaC3wMjOhmfwrQdO79NP5W21wzx6dzWOSo70eS/EBQmaz6PgvKjoxUtWE4CGoQ6shASAy4d5ZDDi3mPyZjYJFRdwr3Ig4z+q+Dt1LRoWC++QTZKkmXQauGl7XmMmG5rr7Ft5D/uJ2gRKgPnjcMEK2zG09NG+qvz2tJR54lnYaxBLGrMrDRJQvNf2iQBHGxH12hjRzM2z1m9/+UPNdyox8EHKH39ACip6EjjIqUCOUMNqC2y6O0nf58R2ZtytY7eFCtuIYe9LO81C9x7KD0bsSh5WsbrPhC+qBDw0irIFB9z1IJh3hHGHJKJ/q7NiniXvPl2ab0tFmzcuUrWLBxgXWdeGdGAb+aTeIKk6V8XABpH+UVR1rbKfubPX6aB5JO/AYIOeX4ygKN3nJ2AIoK9wapdaeDjCVXQ8xevoCOxsfZXkh4OWb2i6YDhlIaBbP5vS5kZhM47KXhJ+SzrWI99pq3rtxcBVQeZ2q6s2/zu6x3oGAPgL59sEPbwj//ArDwD4rsWEObsPJlrt6IgdU6A9RLgLt3Ojf5PA6OK/QOUsEvCyajJDZK6HWin9E0MbzgDdX0jD+grm5fxE35bMnLaaaqJ3PmI7CFzt974t63Tlmlo119WBKEjjBeUBBJkVNnCsocJYA1LVj6cCAV9bX5PGRkLG4yXZHvH+hK9lAciBA1iCLq0oNZNRr1TN6LtXRSNtIoWipjQlKZQ6dsGSYGBgsAI8JAy3RaUjycnId07G4DMN1IaR4XIXrrrqON1AOR9QgCNiQuzyVtKX/mwAKrKfegWmZL7WChPHSH9g+Ro2NSNq7JJm1lFO2jzoOuKW+HRORAMbDVeDntZJprbjeNO6/xEWxPeaYxF9CgbD8eedyRpc3Oy9H/9X0hyFsa/qijvVoepC3ITsCUPfPfaHa3z6WEfvq5827JkzwvDoo8rWwY353cUyvjZoQ/OwI4AdIHWsDKa7oBa/gouC78YNDDsyy0EPocB8Q5uIQXGxezZNH750SK6dFgSLcF/8/5eY/uedaQx312HY63yjPFsQ1XSUlkhPGjYsrmUbgGI68w1a8I2j11vjt5YlXgYXW3Kw9JaHpkRmHMkVrnWHOR+PiSp36TYFomlHvTjmY0URTenZf5QYwRg3bXSV+Cr/L4cJdICufG2wGafqCJdmfM2I8ivV599osCDEL4RjfEfx6nJ7sgycKxiu4TPdGKcYUHggkiESW4HZybGM7VnguSl6gfmaJ2NZPyCUp4UFPawbd3h3owt5ZHizaiHaxYEMRfRFA78J0vmRh5fDK323ugX7EfppqTFwD5Izp2VD6vXgSj2KUxgycFlxRx9220PEfyyiXMcXq3ug2ZyKvPIQk1EI3K1C7fzxo0TuYCaIFKAMW5tadg3D1x8foqr5z+/y9RXjQk0GJIg609IH4LkLU5Q0IB/Llr6J8oWZJbJWlHSVtq6gt3FkSSo/KBX2guwGQ0fxL7yroqYb5zpLR+tNVMRfWta/byJv/khpCXzpIFhYwMAFTafZNI0WKUBR4dYMgMNFKfuxamx8OYzZIX6tj9N7n6M2InZMq1OKqRh4+2/r+y2lHPvEBpThXAwUi2tGSy814q/Wz6NNDz7xvnbp1aInwAbXsH+jhL6oq7GiFbg+m3T6UQxeo/Qwm2d9rtUAKU90mM3ydfSlqmx1sgxZcusHkfxtm954ygNBW4l3rB5HeuVCuqSIuUnjFD+fRWALkdydhO8gp6IkyS3y5nHQCc/r/eS6kynhYVSeuzrD63nqjhQBQswayHE/t3lu0vcF0s3J4SaTfYu/sn6/ikPCDQ7CfsmQjUf+unaYVyCn+xYI2RrYeAQ9X+tdsP4NL14kA89ZZ1oMf/8yj+wTg34hiJnIxZJKGyY6nzs0P673E45TkbG4/Q4Sw//LR9dNMasN3lJEJaisExXvyW+1jY68Qc712l/HsslVepjA5b2zDdYN/3+ZL4GhnrNj4LN3bdxGWYbuWX+Q6R9/JN4TA4VzvT005L8ISHM25g5krqlBsau7MDlEOXCHhj885js9rgk0a1zW4Gruzr6J77tVrJrnUX4pe963fjbszvOYaaDsPr7anFZU/GpO9psnoxpd/eEXai8s2wCSarNHQ22k1cbobFDIPEjELueVlIawXxeIQpRuqdhgf7SELTNlsYRVzkihUt3pDwRPPmrgz+7GGjbnER/ZhuCCJXEW3LBuY2RLeeBI9zKFG6ywXz/ygfTt0Sa6iSTNIUxdpNogSsQp/fqoFoGgr/xED9fONfCBcqzdk/nFW7MpWze81xxXICfqHIF0ek3aH2Q2vHjVq6dYvMRkEjoLblB9M70pXIJ20PUq43g8FJEK8OvU171DyZodJPmB6iUNch6LZCt/yYBo74zpqSXFSh2VcVvO03JW0hKNRQUfae9R3j7xCQuax8DRRPDiGliWDPU1WL1sO/akXvUVA3R3BDwgvDJW18cmTESr5v0h8ZCjHBAqxtGqHElbBINEWc5PUnMLCQNYWjg9ys4i9JZsC6ldOqcTpzkWhuP3BkTiSjg1s/wGF7wrYWcEltpjIPYL0LQJv5fACjJH97+lZvyS/K4nfyWkLY0qux8t/xY2prhbSxtGKEn2mY6XC0ubP3OJKmBM8tValNiNqiiuMWnqOnwF8bC0GIRlZ6TUIZS86HIt+UeqM/4XR+RXrh0149XGmMV4C0eXToevKhn3VGNwspeNAKmqemNlsANauIj7taQESxzYTm7ZdOt1SpRM01t6JuOUioaoYj12OVnZIB0bDC2QM0pAulD8H+X/1uNIkAjHpJDw/AXGuUR5gh0fpw0XwoTMAhQKn0Q+40943yghtMbYgcFslMxfHbzBOq3ylZlRSVVDMsJevdP4suuVYFOtGlWi2WGa9wF+Q8PR+3fD4LimwIupxBUGA15wBOLA6Fll+mtCGY1FCHm0IHub+DCW5qV8OaogWT2+X50kj5EVlmyrpf3lAt2lTvEp6nI5WWgCgzBb2HktWRoiDvB6QQMOsnHujmDtFEaCLmm4HlyY8EzNAouoN80M0y1Sm3HISTlYcWzk9b3cxQy3LYtRG4WrI9MYn7B7/Hr+zoSheKoT5kVH+5KxmKyu9EbKq2Wh1Uobdz4zmOOf9CPTMAzY8CengA1h0EBTFUcwkQsfehPCRyKYIxjwMtrw2hcrA3pnljtBtYLjeUC6bLlSwB0nrKaJWjrslFsdjykLTkxEZSbsjj5XPvu/mFiu8+c4pI9qZksKA2glxdWhQDNKdgwxSZECIxUdNuaJ0CRBcWPg9rqnA5OQWjnUzKjRGYpqJKhOeC0N+A6rLJCBI94kKTqhgsxhOfkJHQTHrViGb1AoGWCzCo0ZjmFzClbs7KIzdInxIa8ilHZuthVF1H6/V3+5mIMWHrH4oo7NNCcmk16ws5esIxeXg0Uo3DTx99gsAcZyII93jGB8jlFbPrb6r1QXHfWBbdwgV1wVtumhrCSiG+6E3YxGAvhTxmcjC1BCejOfxTERh1mps23CCoTAnrhT2LXd0CMX0+v8G1NWIQCGzIDSg+qKefKvpowWEyLW7fqYbJK4izzc6SvWrDaqFM+UTphmwVwU8QAJ9ObsFgMdH/wVh8m2L7wRhGlBNHmFmt0O0O6A77HsFe1ZGlmACFbXbN6N2E5+bTFwfUMpyrfmPzaT00qWlI/JkOQk2/kSAvFdLUZxJnslvcHDZ5z477rp/xpsBWAlAVAw8w9ZBq7ClDDM1IBg72LY6eGLBxQnZCf5mZKOO2F7cbUIy+VimgllHrs5OfOhkfibf1UMuQiNDB/RCrDgSuDIH5MTTi7LMKARYnb/qR+J3yA3+e2th6vMxeGy7+Ut8tFOPLINVqnz0p+2NzLTJzens4V3Yu83KSyvVelWhfw0S+Q2g2+5RbxUBfXjB2nZpXg3VwXWje71qK2KazANc7KLIIRY9YmATV3eNSJBuEPb8chMLqjWLCES8uG6ulzQhrzzBzE4JQA3oFK1cAtZg9yc5uopRO60AEdCP84wCa1OFcAWTaMBIhR+eb3Az3a175GjRXTrn35ECcZWWhYotGqByYyLVX/CSQ+ajrFziYNSp0RCoccpA9jpC9UNcKo+tA4utKkVB50oi909kLRYuTAvUzDJhn9KL62KK57TCd6k7Iap5w3UQrvxobYpQoLKgtBh3ERgmpdZhUZGB5t23XbGVnXT+fmJzkiuVJ1oHT6D7Nk4uve3dqeJoPZ6aY9UnJGH5Ph+o6ZbeutoJIAmPa7rDvaii8D9cZ/MbDPthXrwQfwsQ4shQbReUQjrZ67F6W2UGfPQ93rdTxqqSIzLrLvOslkpu6yx1+Bz4xsGbsOo/GnkVwC+8chkML4U0NivqlrNbRnk9vOuLTl9j+qjh36emM9KUuh+Al6MHBL+j2gzMosLFN5f2gsSU189Ai777wFaZHIr1bVD+k9xArIV4DtJ6udAjUTu+DWgNnPQOC/79uvHFUgG5d0o9SR2QuTTjN0eUqNarukhIxZvJIghKYD853Ol+hNl7a9gLvTRz6X8ZWjV7WVytZf6LYV+0pps6X+dBJgFFedK+wQUKnRSSnEx5GkcpPmcLUz6oIdAFq0bRCXMMbKXNRLW7XPUuWGFifztzx0zUYGFkrLmvxjEb9FlMdU3RX3S9c7Jx4TqYroumZJhADEK8gHig5LQy7ewdDiC7X6OBE5qZG7odW+dxDtNIhbbHgKMAzkNQbU5338xyWkN6d6+CyKS+xbkwx2sNIsG9h674HDGChLwHBE6Yxc2TWM1c4SnKhaYke3uDqijzaKGTnd/Y12QZHXPwan1FreuCyPEl7jzTsaXyzW/utqwJB38wwjU5+IPExnP1cgNWE7F8XVFYKM7HPxz1KaNhtWmC9DaGx+znlV0v2wSHPoyAIePOsDWYn1av0KDV4SftRnEK60GLfgDG8p/qGN7d30gbZNejphWADgaDBGOTEcvcaWULS9ZB7YTibK0q32L3xD/F01REChuwla8cgt5amiRaP+JwlIaUvN9ybYEeiOULdrMtIbj+YyC+eMuEvahb+JWLC8JD68XfIx4kUR98ojog6bdx34wLqmA+hHZnQz9m7gqcqs3d49rCuFbenRsW033HU3gnl4hszu5uK5K5DXCerzwzKWbFlHnpikFQxWF/8mP0UUvr36DbLOhSaSnOcjWlJ1OtQ3sDFpo4uvns3q+X3EpY1GUA7/ZE1Bqq7J8HXsIGEbzZ7ck/VKejZOqTVUhs1USxrXlhSxSPp3aoXRUZKe5KEKFpp9zl9h3JFTdvqf33z8CTBZAsiomAxoXzGLItxe0Egx8Dv1ADl2a8h9oU9IOtTitmWXzgVVq+PpmK68bc4hfX+2DSY2S4V7SAtbQFUx3A8x7pGfxkhHNpq67g585Feg6+OdQKHkXR9FLyZi1MP9lNFEplz712Mi3nLgZqDjIZnmV1jESLV/GvjPhu13u0ML3+J4FEr9JWxv18RbNUasHLsKePgVpFFvg762sutOVB5PGN3IKkEfM73CZ931egq+XCNCj8SZ4Du4uQvJ2Qm9GviqQPdCsd/9lFLRHUnhXA4boc+998O7opI5jmdb6BZN7Npo0HEzzSPSLeIfB4BA61JKzZvC1kxFiA5vlUGOamxMdJ6/dD0nXdEzWFioc5LbqMcp2qUA4KebfHR5B0A72Ns8wUhawxgWYKbUqgBnHkLKPN7FxbJuxvzmgp/NXxKJDlW2KjPGysOekx1fbQJvF03jLxhr4ydY/Rz8gg0de3YAZNYEQH9GDpuCvAu/WULLUF2QiRH/DOY5Y0DAjjISA0jmdxCYqw6aGvCVCwcQhCkk8lqvPinXRX2cyMuvA/YJudw9pEUKLWPbfrW/vlInMO1XTb4yGjHfIydG2WHRGLKnvjNHZfjgfEsRP3IfNYyzpfA1TXa0n441wwzBh1RgFtSmQju4gac8faxPkRYxpKiid0KFqutiXOHcnMhwVleuZc7SO4RlPU6O0skutUHIMas/WzsHGXPUGcnTB8InjcbzhNb1SFiFU8jU+/JrizSj+YgtZw1kABezj0SwOugzOw5uVfP83PdlwqEV67fv7Q8FQNUkTvHQsTD2qCjvdAxvwVuhi49VVx0uUpHjCiDuJDQILw+mfpFfpM1XeMCR+BrObj/07VJrrdJmOrwn3XO8qRVwmnvJYNlbHS4djf8cSkxkNg5HsykrEEFTbvfkYQsXy+imbQWT+r4HMR3ckRb2+ZTbrnNYjIpV6m44mCw/KYtWzu4e7negifsoKQlJsYgD9rIeDw/va5UAdEQ0pLn/edO7+T+Qyho9W/0XjaNhVPgUl7sYVRmnojLRjz9/U/3ouGAclCnY8xp7MlQrcLbi9g1UqZ4cseSB816plmMSP3Adb+vRTn6Q6irSORDefe4RtG1XIirWrVmvdH8iqmnGSzlRlisdcqZ5uhmP/MF+gFq5cVhS7FRIRv9dz3cyZHFdr3BOewseJ14t8wi4Ncxa+OD4NgufoZ9A+rMLBG4rZZ6MionC1hMQ8JGN5bt9IgQRtxdWPdWPMvNDflkcT9FdpDYbV4ZTLOIdDhsac1z4aVnBPshclO65+NKUOjY8eZpwsoZ4684tm1SRmzQ64mYaiZHCw5E/OpUEsV2lzxV0c2eNtZV8cKz/z0VSB1fw5PUxfn51viYOKS6e/ZklvTFI51O4vE3QeiZCXhjsJDRe4ES4enDUJ50gDUYs0x02Om/RIPHdSMQDForavCgnBZeBI1y4CyrdL7Tt+QvnOvTkYGIngPmhg2vEwUqFo5Gp49p26RdCDhVMifNgdn+FRb511C4MRru9+aU9yQED8MoskIFnDH+4DyOMZQsCLT98zQ/GQ69B8BheJ4emG8ogxnYvL0uHQEYYlWzakv4pCMOP8UX1M2YNVsCboVXRCpqXu7VtomBhzFaAHtGvg11s4I+Q0km6bhNQKDH5bInMSyGZH6ssMfT/rrodNhPQHMJkxYEdKM6aLNxKy5m9nolPSX6POK+yNtUnLO1D2QGq4riAlvNf9Z7SGjmxUce8vGFrfKL3Axb/ydnIcn1gQbKuSqp8vL9zlyr6dhVwRAUaM8EbsO8ejeTllLARCofUK/SSi1PwJZpBNNPHnYi0Oh3lMwvDlvLteHPn+M/MKzEBS/5Uc9cSR2H9+uw2vY/7yWVzpt/DeAYwiDT91jdplL4Cvd4A36UmoLbzXZa9DcTjCsWtD9R3zNakg9HfKFc5CrpW1a5ihAspuKRanXI2AUepRTqJwOrrP7it8/XYGEqzsea9poNDfOuD+6qXy3q/+vbt4qyRRt6x9kni/xrIh32o5j8hFdw+AYinQkeNMgMH71aNp2n7Yxfq5nbw7xlMZy3ZcOImQr69qRMvY0YwvMw43zn3Q4aG3kEEJ5eOw6V9X79X8S/GLu50puSFc839JmKR6FxxD1UggChi1Y/X/cFkMkKD080dbTU1EtHaoa+QbB4fDpXyWPrPDM1w0oLw6iT7mYeisXanZR04QCmpqxnnTddDiGX58qI9b4GidqIPyOKfQQZW9m4l+FAcgoRXrvVM/qpPvBttZVRs5UQDUIr/JDy3Zj2G3aji6lw6XH47JNRiwjLYiKMflDMEDq+Pg5UIGOP3kiBWbgkSsxbUDR7Er3PnGnNjYCDzexPrjP7+YWYufAWWYLn+j3ATbcjtU3upXH5izAnG/S57cxdQUEOjLDQw3K7CHBAICvDVMwV+mOwMeLac+Vxji41ptOpKaeTnn/5JF03MSC6bANZbspbh361tuT39b0NfVXTdx3KzuZmM7QKB1ySG93RcXeTZ1F97zFcM2WV64vDlzhOS5yqyyi3S0aGTgBp5Ep5+QYJ1gdzJHU8/XTZQo6cbPfDgaGJrcWiYilpsqTZWi5YvRb7khBb4buANzsM1Ic+R/FBfpveqLhAyKDxHNGKLpj77iWNrzR3Mjp7Xy0e7h4RA2dC4WzDUeRouDhuYSuhFa9T1Vd25FsdPUSVFDeA01wOu76sum97V6nMXVxxzipeEtPCb+6ciRrYc2lV0WvBa2MNm9MACgaRH1bb2IKlqyZOFuCXOaBO+JqKJD3w1eJV1GkvYBr14UTX8eZngn83f60jIGjZ4cxGHVnEB1PXD63+6XljjPzZ0+faAIjFT4Orfee0EqM9nMYxWknbm99fC4i7VqHbHtlYdSqmLLC5kl6M8kV5C6CBBpgKkhwlfcxjnPvc/173IH2NhNV5FdzVpAUxb4VSARnoo4NZz6mCACmWZ730aJoZDA1626DZ2yYVDz5eB1FrjEpPYziV8N52cfizK71JHsGlAVwvpDPZyYPcz1ez3PDpeP2/JWcTy7Ya9bKJqK+iGEBSk45x+SLRqQ+z/Uct8fj38PCyBxfauqZFP/Vjz6hZLivNwyE6TyKPmvIL4ZESNsLUpLA0U7NVEcuhxLOcqvh4zKN6bgLSGWkjC/nsoH7dzt3nx4qw/pG2ZvFsNscUEddCdEF2hy8awmBFae5xh3SIu+iQrhaAjqE9ozQkAI+2+PrGi/A7lphccbGOP6G9Xl8jt7Ow5N0l4npk3brMNIEJ4c4Ia5ooPPA/45/Wq6Jz9l1LESdsH1eaQUZWDlOGu1tktW3++K4+at6Zsa/jEYU0LNS2DWTd1oUjoyJLDD+CzfLlrzQRv1NkVOrKYYhcpyjFPbMs1pGf4G3cevhrahAfV1m/wkmh5eJ5ldFx1+LXlpxDQWV00wv5aZ+B82NQuO8hR+ckAUM2V8N+otVo0jQpbtZHnuaXG63+CxjAcfeY1irZUd9yYzdkc5hYa8tyYz3PPEKVuE8LcVxCt2fnf4nEME/+BLzcd+DuqxLAvBEaQK3vrt4DWOsQGBRagppFEymXv4TcxWLZkLAV0RYGDTdlA1FGieHCiRPmfieJWaldnQ75vXX6hfkj3jVdn55Jg+GWRn6RvB+21ceOiqQTuZGoM6EnTZl4m6p6CS0CiNvVsmGeXXHQCU7H0/SJubeITGRyo+ZVfPr4L+Z3bKPuIyuu1K4m7/vCWz8UnZi427TMgH9bFT8iMWiirtg0Ldbf2JNO0uBaXRxunuvxa32Tt33dbToHAQc8kkXBHHLjhnR3WJ+MFeDbqKlbYDn9mX4LRBj5sANay14Hk5fLnwU524lzyyBaBTq/mYwCYZk5h0L2UfQPIz5oY1BwxLLk6a2dP7DfjDkl8BtZipQeUIRWtHF8vHHGewGeirhYdJWonpw/KXX9r4uYrVbLCSUB0ggdGL5Gm8vgmT7JROWIuZgBrNlFnHlA81t8CGeTeeGQq8asECk0MH97k9hSnJN27Qw38DtoySbsIwKxYuThPG1xqYGBLyS+QfX4upgNIsv0yuc47B9R5gB4pu0uCTDivHYS6aIESybWvVCJx92hL7RDsmZwXIygRrat5653NxBLt/3aG5VIdulOuwfMlIgUsdC+GZKigoNY184ST5krN2md1JBBHAUnQS4gpKQDvwGfAWLjKIq3UGgl48w05jbYgu3odHNlzbCF0oRPek2J79dXCAoAxc3czKWk50g0uAP2dWeoiuf6DvmbIzOzoCmPys/Yca4HAcR5N0dyvtsIs5nCbWbI6y3qebP2yGuSlsxG/QEMzm96fLGgBRNsBF1R7jok5q6x6RagSst1Edole+a6HGWAcM0T3agEFZWeacYqxn5Ek4b2StmArcoTTKTpOI2RiqXyRJhBOLlzpuHlfKhkVP56ISjkhOTGVOjDRmTh8HvtHzlyQC1V9sdvNq8T54+KPIu1cRPD2e03HNovL9dxxhpK+Bjde4Vf8ng440BcKH1/IMqMlX9ni7qvRj9b9/5UanL/wovJD6xVh4alUQlnuA975c4WxgG+L9JjO2gUS2rvwQ3tbYNho9CqoffZJLp66ursy8tMHoXPD0NPmK2/Y54gSaueBgKR44VRsuPBWRGmMIteWSvc74sjs/CTgnVqbD3H2BNKUghrYGd9iVacF4o1B8NtQ+pKn+129HzvVz3o+SeKYGWahWSng752kIZo9tJ5lWreSpjuD6U9XOn9oKk8QY4FwiCxGrCEYbJryxuvWcO5L/IwAtAfIIQk6ctYni+hN/r7IG3/IJ3drugvqpV9cYX84q3a0lugPz9Zo2ZRhKt8YBdCCXoTASA/cDrQyf/jq0VyfzmU3MmAI5J5L87CB44xmW/N5PbIdlB7M2L9D+am8Ah/k1H9REs6o1ezrkTref+GINGYJ1tD+W45vUlLFZE5Qegu0U6W8Y4uINF0ujyXyWwJ0Fu5yEUAk8UW8PLVheusUIeTHjMycJSTLTp0XhIDPAxKA3QIoajTYHF42uuCotAoCweZ+4S0i0JaCZccQ97RB9pJjK40POcYn8zHctpfWumWZX2w53pcIvCaySKxiT1QRAufrMz7g2mYleEHbBtprrbqbFrHg7jL/9Pek+OwzSZ63MEZ+AYU6XvT+VMEiIJrWOumBZ46/s5PS0q7byb62eqVdbm1kXXBCLsUYGKkY8obbTRMLjVJbR6beIrCxs8YJjIj+t9BsNnHgwvhhuTTB9JaGrxwbBbQKkY+OLerVDBiZ7MvbrTpqWOSYlN07kuC6JfcY9KxBDqWk6pvdrUGdaVb1P2ezh9q6+90/waIxK2gI+BW0WUJqK9XCYgLIVaRKFG1cZvZDklUCUwqXLwLSQWXT7rjmgX9IAFs51TuSEspb96NamE1PDLziiUetU447BlGnR/9r4vWWQ8kBEIFjPts4vizFsYUoF5Tkoj2BwmbIX/EORrW++05reZ8+dZfFQ7OLYGNVIcHiPVn7OO6TXJEEVOnhAm8ZbSJiud0EoABxz8IKzFVESv+cdm6Pqp6+tIgSs+737gSWiSnD2GbdrzhnVfq6E09otdM1pyiXEd7J1ypJQVVtJ/K4iQL3Ajzo/ODrv4xparO6Hrze74P2QFvlppASJC+NEtirqa28SKcPhtBxhp4Yu21/2oicbrtOz1BdF3lQ0wcxxahKAkODcbirKufcbu/RvueueP9kA19/zMauQzqybWuKF+6Wbp1ms3zE2kz140UPd4h2SlbijAXRJpbtD084djrn4beoE7qKnwtmWPA8RssvnNTJDYYLf7ApgAbQLMI/m/9Bbf1MPmKeh2sx/jhu9KFuHDlyoB4jD3ozz/Oh38EMAKv0k50hgtscl7knZgBUxM04vJTlK6uW7SEGDAPggtWah0KGx7Q1Ae4glWaxHw41kOjfcdQsCMb62CT9vZtMObFOBwaM+p9J60SkmSc/jY9trLfIj9+z4ZFx6cJNOyWfStK6cpnmTEhGgdHZojclfdMgPy4OOZjkq4IlkZff7wHgYfpmgAEDl/tS5lCEtAfNQVQcFoGIWpdwzUJ9aWaTtK3Wk3RU49Iac0OQe4a45MRPx+onrW51KKgSfgZSkuyn56R5EjKJvn2xxmrDWgo3fdTIYMnGrjgUUhAERf4UL7mJgN4uyMQDdDVd6uJoKnc3GJpp/aNmI/hiUEsRZDZUQIuEfQR1XwxeYEfZ9O4KA90Zng5NcjoWIi0IZPbdf+odXQV7vHcoaNdUi4RqFuNFAHq8QVIE/4qBynk86GUxip5zTHOogZColAn9kUQulNHxpSZJEtXfZHMqQiwf9l2DrKuzpvHm7EsaEp2apP43E7IuIfPSdnTWCUaA5zdNJu164XpE9HCYnr3/9UcFFpBm5M1LaAY71DGqU21FupPHDqPDt7XDZAUj5HcItvGg9QnzzJ+tHpfMZJjhJe1HT2IUdJnis3IcttLa0PuVo981uWg6qgQknEA4OG/eywiNsYkmawcMHMLzjGB0nblFCTyZ8iIbESDkB5bf0OH9t864MFzK6WTmJ2Xn/x/M/nKl6uEPtavF+MihJHPa/zzx6mCb56wXw44dylHaS1TTcT3nJfrUV6hfc/WwThm1fROPmjiQO04Qu36WmWUUEp0BZCKaaHv+2GAUBU/YP4a8J9QSOKqoX1P8UehP6w0WjRpdNLENC+pVQb0pn6maLIvRMqD8SxfVRl/XLvtPuFVfMj/82OKtEzT7rlXdy78MjBxm1BrwM5/UwYjdYEN5wnX3MRnhOvaZm/qnlNfTRt1SBsvtklw1mUGTeYqZEB89QtBmD8hkkF8lOBrYPWipkdfbZu+g817LmpEtB4qND8hwMqVVYD3n3qpSKUISaRIlHVXyMu2eipXAs6J5CgfSOe3CXNIwDoEptNNopfn7efZqtOV+icZE+XvIOWS35IFBmWcfIeRZsO6JaJ2BLXDxS1ZNVLskYBP8Ojn5vkeJVF8fxd39xnfyp5p02ElKhmJ7UwVmmLl4/0vx0neKtGShpUwiOY3TSo0/kF/7Q/zm/bu1bxyW4bh0enlhWz9Y2uF3kXr37Tmh1jBlCiKs01er4fkyMZ+RqHNjfshbGx5/tLq/DjSLaYcm1sF/J0GCoNhi9XF3j8uXkNo5mZJ/HEh+Q++XJMW7eU075n6uuK7S3M+yzWVdYsKnBcJKzo8hbnhMfEN2SLPKLeJkrC6E8fzymj81vAZSALOAhNrRCK4AW5uJHCOLjMso6MKVgCWtHqsx1hm/5HItD11G42RWzVFkM6CpOHon3PfHUkuVG4qXa8lt4Hkn350DnGSZENA5XR61SUIijoDaMALfrA4vw45RfHYoLeRVdnsoKcSkBpBilC7ni7WDRxRFTkrWpDEGSABY/uMVORPjy88uCQS0Ts2fDlvtG9jV66GCotEojS3xbC0vonanEGCs9q3VQFrkikTYDyAuwPzz67NWLCs8k01Fjdur/gR2VGGxOjY00yph8Km1wYtpWV8ZV5VGDXV0vJ5ZlecuC+RCl3jKdRTSsQpx23b0MMShNSNqipy+fligsj4N16W1iU+FJYco1yEsr1RuwozMhER8Ws61xG9KwpONoVF+/77RcaI/MqJYof80g8YM+Y3h61Nz2m4oZMen6hAsQm0quNFpod2wUA7Ur3nMdv5LdrFq40hnNZVaLb8yjEbTccs1YEGHY06KGQFddnQ+4VMecSZ5g7EIzgwbMVQrtmdosMW5EInIwJLcAI2hyjmEB+Id0UxMkTJWJfxDRrgN2Qchkzz6hKTau6C9DnaTD4hHqrE8MeiyYBCg3IYfj4l45U1LbDFmeqGCshtetsYTCd/ZPi6R3Ds5Gtc5YNOOt2ZP63u/KDKFRg0LOO4iAXd8nrK/dRo58GtQd8VNLJbbptF+rEeEBh6hYks62DluEgH+xpS2ahDqDEU9ZsePO96sX2BYa36WcYNeSXTzHHIhCx9xFrk6HI6g3SXEaHlhuNMTinvzrkdIl+dPrUkLkm1deKNgEdQKRm6HZocH4CdsN8h9QMT4mlwYfxWzdWbRmu0EWkAk7AIi3GPPC4wBdLdC5zXYaz9wH7zteI3MNwrhVSawq+KK6OQ5zdCcMjoCBG+g0tD18t+4R+5MsvOBOf0Txvst8IhDCBtrq5KSxUmBo9CaOfK0BH67vndzdbtOPP+jMckfYNQGRF8dnylzFjNuSdI/wxVXQDpMJaD2KdeNtLT2TKI7TwZJOkM0wKm+EvDUTUoSVbeVJZ7DN3uU/Q+7p5yeg+6cYXlWrVxudy6lqFiMLXXSYfRA6x/eyh/0Mp+hXF232K37gA9/BPDpnrAmvpxEgY0+PY1L1VY3YvlpTeYhpFlCEEgn1Ez/ZNSuWDcl1TjDuMnNVOGyIn1I2Qv4YPBsTojRpAwMrzzeY63rztomhj3FGhgZFFHUAMBTI1m20yY5mGhbpXw3JiiKA9NwmVgxRXm4P4JM+nSIpYfJcsNNnbHvXZiG4n+PYO4SoRgK5PJ8j5kd0BcOZ7Y80/ikp3CrCXInQoRr4Rg1U+C7sWMWd6jzoDPMFYGMBX3R4W99M6h61NITlS6Z8anblw7UzKSnCd5hQeHrtSpU2YOI2vN8aG+dTYDqeKFGRZXChmGqG2RuKepyqtiBJ8BAA6gUqOrS4JZlMd9zVv1MKqnTTJxjyMBb17K+ezh1Nml4WxETQbghA2Ot8Fj+O3pVMSRUFmn+2SdybGonzORuCSriQsotIYtxKdv6h9D/BHwKk1WhGnlB5tbocS3kSBd5Gp/JoMukGcH7qi+eyff9r8hbKzAy9Djeahbj4rlynFhMVgeZ4rIOHuu6e+KANjzep7AcZlTXmpn97mBP8BjxAI33mLZ+mhNU/uy/boVOAeFReV4VuCtHBAJx6T/PkM2HpEbD+DkU3q0Aeyk9I8crVeXaiV3ts68QRwEWxbxVYJHqGvWRbQ48qc8q8ONr2HpHGSoOxuY3tL/i151g96/RoBP/PYi4YJwkZaaB4kb+J2q+boDLZXGoC9olOudoRxf5QG6UO7y8CSctK11w6ztfVfhz0lfVidY+mFQZITrQ+guAzoYrdDxcgn+HjXW5IB2nkYfPQT3t8rpd5E7w5At4Bb1kHagmdB3DAyTtWo+5iCSvgEIwuK+jQgJOKM69WHuv0nK74d/6QAkYzfoh4nyHn/C47sc+q35NvjX17j7N5+FxwiFHpiP4N3ypr8j7QLFozw0ZV2oarCUWscuAYtn2W++VIZXEcU5toOL1XRC7al5RrkPYa05PxL/OE9pYUcBTdN6cg453l3Vd197y+xGZGBwb/wZ43gfxvGXewxENfCcjN52Uq2xoDDJp+KgJwGFw1U5RtV7qlgOAQnxO0IMk42uljyMK9UHLt7g5FzL6S9Cyp8Kxd+GMDCWfsKiG428UcuXrWlbkd/l4enacG3UUus0/dNsErxLwEdKLmvdUTSF85zqSnYPQApf8KDivyr2ghdpiaisAtjibrElTReXHj7yQqF8aSMQVM46Q3DCDaHWJtgiX5IPW3t+V7pMVmD9xzEjOiPN8TVjabOq1n+v718PjRu04ZWF5mxPUEW4C1mLUBHgjPOP2mKcJbAx5linkl2wkTzKTOF/W0a8nrciwLVw4IPA8O+qoEESMWBeZ8eoa1/x93y8d7f+AZ1kzhpP6Jpxb18TT7TOOpgvEQq3bzs76LKKbZrPRZcJYi7kOfM11U2VSrrGEpvHq/qoBmMGTpTJ3VOv848MM6t4M0jz62xfuxdfcfPBn0QCAkeqae2XOJdFeWI994YX8QD2dXSzAnWgzHDK62+1zGaYHrrNhEiiTbOrhquH8xL4odnEK9X4TPSxjx1EqiqJGdWvVPuFkq/Xe9KqktnGcIblV+nHP4hNQR/dHKzzI0GiU0wCCr9xae/6SQLIFyDsRGKNg6zWxNsyHN7H20v8nXTKEwdRvZ2hLzSYX4pyiBWvzsyQ9f5gT0Kjqm6QcCsKQ6L5uLpwlTWUWY8WyiI5o5SoQw2WcX6iZkeQolJKr+/8i8r9lBO6yHFKROqeTALdi1Vz4rhl0OxJepPVyaCVl8ymnhBt8AKd3wjLyAZXZKhmP01Rfq+8pkGqkJWIrdZabffIOUu3hIwD6guzw4tKtvtZn8gOa8uLcpQKIQFpsUl5vT6xrAQVRTr9To7Q2QbonPw1IlYoTRzlM1i07UMRJv+1oUY5pL46+UvZCEM/aBfmDH7uPJTEKhbekjEscaUYj130x+hGZNc7D6BkLfe/bt5wKKNynPlt2h9UoL8x5QRWSbMYli3HVgvw3DEM1ga157AKVlNyVCZHu+UqLg+KsNJkhCb1NUYXbANsvbgoU924D4H5PgcGG3zm+3RdeXqTJs3j0ZFcL9yhpB/+HOHW1sd+jfK2mBk5bhWSUAEc9sA2zX1ldDJMDQsWsTvvPs0e7Zc8TybmZ9e2CSQXbU7ZsGEcPjSrTulpoct9QZT2aMVHafkT99ta/1qhEPR0BZmlxe5mrZ7yE1+OfNK3L6yq9oftREfh8YARYX22bj350EOrTgexFcqHkqQlIsm8QSSmfT6GmR6485AL+17PCZmGQtliPgZ9qxrR3Y1GdepyKfUeBXOPkoi8WGwr9ZXvMwutijNad0BIn1bXOJRox53D8ineLpRDCP/3YmypoqfpDLWLns49RrDqHYoWPE3yqBxkTrDfwjlcIjS5dYNgi7ZOWek2IZ3iAh3oEfcW/nlDEuZuhudrsPFo/cT3XY8BjwObUmghpG4VnkJsRX+uZ+oagLhdObSxpKwpMBvnzPeCBH+C1+iJmPKEZ0RC2eG4ud2cw4B5yUeToQPsGb1pG3KkCvl5ST8rvefS9hc0ab3NfmnFLK4BA2Y35YSkZKW47ZR5VC0zyJObhzFtMm9iHaWSjeVRuAnq/tE9FPBoAhisbFMbJSl3gavgx8QpvVDudm1tbNg337uUyq/aeMB0LwQAD3yAFtjBN2pft93tGCyPIBWK8KhLI/W8E66WTyt85xsDHaZCGACljO1blbdnKVAzc51nMjbnKCncKUzMbbmw2o8K8OH5Vy6xloRIDh7dTANpd0QKwvPemDasmQAebLxj1I0pPeJuDV2QOD5OkUUVlSFOD0MK8kYHnidpN9p4egHy0jSeUnOPVZCfcL/C7noSKgXfGa/zGFjmC1pKSzsj89DnzDYLAcKKb5YrX9r+Zv7Dfe9JYc4TTgBKUuAPQcgiTkPoyPWKAS/UHiKBrEuSWNwV5A1jfIg+m/3ZIlMAINUYWwcANZ5ul6mBrvdHAvoEeSpO++5tWNPscnBry6NUWPet/JELwEUCVmeopibHp/WJgCMA8EHtpFd3KgMUr/iSdqmNVoQJoxrU7gQIUqXA2nm5ty0HHXBdES8+F5tL9RGQ62tDHRV17Dx+tGLSxe9zMcpMzDrU3JM03ATAR6fHu2FMvMpvcZdXQ7I6HQi+jhCCfsh17CLXdDx/Q8co0ohMfeUuFB48sz2ultNVk69QPbGVkaZz2ATw+z0oWYvdIfXpclgHCcP2hTVTF/fx/EA5DF6wjHgGSYwa3f5aUfQfhWpugfnH0cGbbkZO2Q68kX5uFLgBSc+7Qdn+uwALIdP7Y6ZyjTrhR4RVPnrdYrpScGt5bi6x/ySFQm4h6vyMkeTpUdXRNfzvrop0jiexrSGLFyen3oFZeI294eP4FWCkWUecIXDcWV9CqoaXQjfvc4imfd00OxnAtTqaBwm1perkZeXZwwrcoLr3rPlBVyewF2I7ZosU68XwQxV+kukeya95WgvJ6x4nbCzM4Ggant+E5tWaJvUNSjgKjCRu2f/LKq3vOqB1T6TY1h2yCi7NBSL2hWzxv+OCJ7r2+q6NffWim0GbDWTktHDGs/h4RtIKYy7KKy8u2sjkp7ryNv0ufJvEDyPaBT6KhkZAkXNZyCTMM7aO0iLnAd3RmoZxWFo6r6Ix3eJwOtxIxT2WWcyWDbnHHD84VPjFweEFzzO6fdcv0v4CS00BtY3SNSyUy1MTvo1Cs2blkVNYnQwJRM+XmWZfeveLs5fvAheQn9g685jP/oxFlZZSdeY+yRByHq+CyL9+PJPdyRLCmDkK0nWBMiZV4MK4Kpvqd6ZbHBZwUKM0FBRozHft5hjX6fGRzXeVxHr5E1+2QyaU8EuMO2WpSEM/tIPssGXAL8/w8v2QH3uhOxB4Qq1sh6P1F1cbdI9ltlPxy1PInQFjTvnh4MRwlvzEmC9AsCoV2v8VflrBGHMw8NwlN3G5m+EKrw+xm6E/TTY9Tb1KmYbQyrelwm3EyLyaiZkSrL/A4bbhpSWFYBzX+JJxd6N4eUIUdmoqmU0eEaSVVNF+aSwciQQEe/UU0HXJpzqnXthhRHE2RL2NLMKiwwwNGZZFUGo5N59Xej1aPVBqAC9vGq31s+XZMGSo9Lz7dZRpJ5b4QCYFv93VuqSDYDTd7gbNN4fXNT98RbKBVApXODJSs7QYRKLr80ovbimnJHp93s7MTD9dVvIAY3DgkFHd4snYI8GSzIpmC6UcvKB0+kvsYxDlM7rkEgo+IVfAMTowtz29e9ww52yE1Ir9u0Q/DGgn4zKTy5zxnj7NrzzeuzbcEMjDwp7vRzC4MnYwk8UkACdJ5/lEilZGUpF5UZIvWbpwxAAkJyQLPbrslHcj3z/EzMp9zKMOdjmaLyaQCtavLksdXE6A4cEILIHMiiGJTaIzOYj8t/XBalKX3KkQz6clWLgktw5vVOJpAmVeVX8kmZLbWNuDTJTVTjYMg+V32cSXkdvPxv81GC7aAhP8D35SEjWFUOgbBY43a1Lt8vxTlJw5H8G0/kKdcCmD2sDIb+7KCpB59NaNG74/5sRdP8XBA2c12RfsNfEum+K3LhWV7pVAIIT94Gb94hDib/ofLR/MOoJDlOeKKcDX/ZaobDDtLAWuupvx/B6458KYewBT+86q4AFDVG1/aoz1ieE4o9Dkjklo78yMuuBeUa7/FO7ZZP9DU14pPBOM0ZZAJOSX5a0BUVDNFOOG2lHeTkA+ii9Ktri4IZFFOuwt/8iu7RlAsq3DAeRlascea77fc/v+F/NkxKwA6AhKhsCa5NzvqSyGIbdxrTOVTUY/fJU9culDKhmyOBzBXsTbDZlaWEEgVfbBv1Qz6GBsKYit4okoeRuBniSBxuwNMWnBYmabZWX3JA2nQ3j40t2ZjHMrvlOwvAxGsX9LjJ9ue7DkstknrB93ye2NskhuJmmf+aHoz+01NR5+dIrW50z6DhhXISp7+Kh2vlos+nLfFqYpIyIMwSCFPlRvv3pqdar/uXt9svw2LKfj3yyRilF6jeaWuk6pOlF8RVDyrrc8vfLkjZHslvIxydzLmhIjqihgmDT60EfIQum1g3iDypOp9ATl48hbXxZVefSFw6GH1NqNBeR5AvE50VY0qL1hd2bUPGx6YpbupoRrqgVZtUi4hksrRCG600y5y6HaUuvV9GRcBJKhH/0q97rf2WVv7f0qTRA9oCs9Sm+w6oahqP2GBuundIDzJ6VMEco0R2k4EC9ExAIBXLuD6m0qBOJUpNtDgA4JYa3EV7LHxtMNkG9UpCkDng8BVPPZ4mxrNecB2/LSDIKvhQAIDLu5kJXCDyJo72Vc2surEZSyLq//vIuuG3m8gV31hzDQQgbRalUxn/8rol1MM1UTlxnv5IsDiIS20bRHz+7yCyqJb6AIt2FM7WjoU/RWLAvFwe2sNxlqvbC2oP5/o2HhNanhVM8eKPx/yBfSoYuOm8ia9BsEKZ42MQNLxaEOLaUQu976i//kxbgcRA826Z8rMa26IV6zCFMtQJMAIaYLnSMFN3NaD4bkPk5/pFJ7H8EoyqxlUORGgtN7gYMNpvvRF2ynlUnO0IP9rZKbFjyJcGcmN2QDA9iMJu6+qBWzD4gnvotzXpLNkv21Rjf4rSNy/mfksqcjtjx3csk2oPBQqFwz5cmxQeniLIjvN/YAwhoeK8HcnHSnbRwktcLxhkHPFeUevsqBjb4DUjp19g3t6Y4NL/WLjFywgdmCqWNE3q/e4ADTyb1sNPvmn6zyoXVY9/rLzBX9Ce1u/xNzvzX21QXBX2treXfS9kAZ6ME6zUm2BG+fVwYgpzWMW3Jw2+66Fl1rl84J+qRGdwolxG4ALSrgMDHOfPVsqB2VAdyF5V8dbIuav19JvhYO27/L5YbTUBRKxPzOqJu0juAkyY5gBMQij4/MezimuSlsDxKl8Tawrr5yC3cbWCKCVyNHZHV1Mt1CdDm8br9FRul83Qz+j0Mu9Eb2qzsX6ir7rfeF6sUY8mHR3Hsec5oh8zKt7i1chuKadgaKDZYQYMMyfXGBXFVcQ8TrN+a0afuzpYfhVWMptgR1E2hyCuYJCYcJMj1NfN86JYQYM0D7gEdDu3zQE9+6rDOjmnCqTKw27pkzyrhOmvQkvf5uHAf/Mr5G8aX4bM6dF+0Suk2+firCNwaowP2y4IDyuG4eTZcDXb6jlAqajaL4Rjd6U03E6aksOeSmndQDmyFF3OyS21TbWrMlFpliJZ55iQHdeY0CAwLGmPv0SQNWbfw8syoTmcCnC7mM4X3FKqyaXqHyBpI6mu4meOu94hpPE4XEMVIbWEFMLNl6apyCOUe06mWfZmf/1eT6IUCUk58hXxHHgQkL82BWM9Op/HtZ+poyeGSAosc+9v+iFZHj7FYQ/unchfoL31vaaBuHLcG0zFx82tkh9yDmuCWgLfMw2eHVw2EgmEAVuVjYXs0z731YVJGGO5VdDry0oQjTfiYWhQ9ZCmiFg0MUZFLt/foUVgriGdLXO8bxBBeqgNtlti7krVWByxVAOxR4bZdPUH507mfovBMnP92jvdZNYQUVk1ZIoO5yjpmTILzm0V3GrqDYJyjslML2+j5TIC37nQpsJsPDXDm2IzKBryTJHQUgi9nJXeGv6CkRto5uEJrwWK2xoSdrTmB5cIh50WRc2sZEuDXsjJcB0WGzqPvZ6BP2mlMcfdDt4cuBqqMaYXjtTkt8ZSSU9eprsIUE9+ncQXA0u3oSlUI9vKX9QqSG+Qq4b0QTp4UuTkJOukSdd4qblZj0eUFOYwn48d+TAril473U7iCW13+G/PB1EozLuRoyo8Ooh8DL2ZirxibyS5jC3aCTQksL34vG6+LMWsUX/AH/vIrG/HTqhkdUg4bR8VRKU9HAOg6PwWgVj1auBpJoJxRGfly8FZ8vkk0o00UlH/FMJfno210+7sItB4jVbbfcgv9j1DAI11RbcYs7pO1huqihZ0afOnNEC+xjGyt5eZ+T/whDV3W31FfEYxa9l6dhg5j9ZEzvFxAwz76jih/AyFNCB78U1L38ANAtQKy+iyskaRDynZzaiBSiJ7a16KQ6EJyjzSILo93IXXSY5pv1c5SarhA83iXo8Yb+/4PPABYvHttz+Z3Tst/yRQKebk0fwjn1wRtGwiuWua0JdDQ+afvwFe7VXuG4omWF3dIV97sgxNBWjxPYJ0k3Evnry86+DqSPmOxBmXpbE2OM5xmfCCrndRPlkTDQPqYyCbmHLBNjvt3OQFXkOLvVZddxXXngFGykhCQ4pb1QCB09I1URL4LBQZjLZUTGLwFxEJBLctCS5x7EUYatq8xR2qCCk1VEh5ix70M/MZBgGejKNV7I15r2myPAWFlOudRwd7rfPtK4Vxs9c7iWEfiFme2KnbUu7S0Z0n+5Je5914gU9XCB3x/cHqvFwG+qu+Irk23V2mpz2p1fcW9EtmEYUYXb4lsIG6jBeU4aew8nG3hZ8SGSXZQP6YrUIPbOXRvnN26dmgcq0y4WhXSMicLIm/eomkNxXB6/+41Ek5SFfwuG2UY9LdCfGH6sX5jotTZ9G1iKKQ3dI0y1T7t48zhjiZQ78W0wmaH0XC8TDFcwTKvGVpEtlyh9gIhC0RGBiW6QoJ8vy3nFQQ5zdPTzUP4V7YKWwth3klEvd2/R5erimkgQq7Pw6U5Z+z4T+V5fcLmpFBA+dgJbNXFenHHw+iwqCKhnc9xtqmSj6hJ7N8XplG8uljTFoo5JyA/xJgKBF1yBm5NzzrW/QpR5ecNJeNyr5x9DpoCNEs2lJDqDvRJaFZtP6KslcV3cPr4/sAj2cyIIabM/NOyh85+t81YnrHGnjnw/Fe1Thi7ma2xCyq4uw+xj0yTK5f/23fMzDOiL4Ak5J2+TxinyEZWsUb/0n7KeVOMw5qHVJRv6UojceK2YnXiyF+vRNb5VLJ8r4JE9Kj2ROrlIaemvqbuTzeWxBqFxtZBfbp3x0qCS2GS7pZnCE8j1PsuBZbPT/uh+KHesDJfpkaIZNC/v3ohJJyQn9FjO1cN6K/bKv+fDPebkMm1niesevB4FUg6DCfN6mfpjXYUUueVZ/glOtRdtRgIFrf1EcsWytrXHMbYQAialJg0f0qvGT/e7Izm4OfTPZZNG2SgCPF1T+PkCdn148O2h7rrEv163o1JZYxbg4oQiTTxYeyZVGJp/DSFcHwV0BWmFYaExPfHdKaR/8sez97f0pfxNYOKxNoSoiUP1kzFRZc9Zg6m1BZMBKAiMCJdWFR6zZCoCXjrru8gkACqW7dGkPhaijYwVe+ShOda9BB88BEBFllcDT0Hi96hyTIpM77ZVcmUbBeLXBqzJJq+wKgW3wnpHknxndFTU21R0HCAmkKvfpcJ09ca5I3C5gWGg0DElLqEFE1jlp/FpSzEm3pAcYZe5uYXDlYIZfAlucf7oDU6XlycPNB7WiGTArt2zJa9OzuDfRxJX0l6BaMC0hwUXyV2r3zxaU3DY2iZLCAUO8+hvHFsaOJXdYe1s9EVHJzu86VRqSj3FW/HkJc4wsQYotnmeq3MywJMUhEs1xTM9mXPzj1WhJ+u++7ydHLJO9/OHEJPit6QiD0Vifo7ZyoywNcsoBDSPzAwZriC5rzjoe5dRFidDxG92zX3qrNXpV/Miq7BqLCGRLP2C29lSVIq8lGbC7fdKg5GIlolyc4h1CEUAeElXSDLlRXcUxaO3KGyRN9gMKvmdSmq5fzVU3gSSS82c/dTZHbwSFLEIh8j/xEYwu70tXUgBSL8xuAJdLhDFcr7FiaJgkMSg9wSBaR5hMUIY6gdOOBDhFtoPr0IB9ZETl0Q7UY2DJsHvnhVq1udSpSr97UYk+jafT6CSAPlaUG5CgMezjnSSk1gZAuKcELqQD3XsyJghzIIzxBgivJt/B6P41FqxFMb8AZ3lAoN6m+4vLR7mhzg6xnVUbazKfi5Y7bhN1LcWG7UUGppXF53oHLH8Yduowz2HCtEZz5gZtPNfZx7em2FF5/DJkoqNRFEhmuNsD6Mj2V32qDIL4tu89HFia1QvCO6pn6cylMqf/5JsxzRPJmr2VS1KB0fY0z/jWAKUFg0kTRBjGloezfaUz9CUT0B21hnFMRvH+iTOKMmJ8i1NH1PMTI3fDS+OqqYrXHPkvbUblualjFxvIQUpr+7h/hSmUocWurGykgM1C7AB0HnYZoOQGlafcyQyR1sDdPWhtJLqUvUxcE6nUp7aDc3ehTMB+FzvmzAsFxySLe2Yl/LO5Xkt4F8xEnKHckq5kS7SgkylxzjnRe++TLTFWoHoAZaJr8E9JbI7XFAuoMnoTLxY1DPStu3L0Ge0KjRwj0yWcxeBxHudxeWfV1SvQiyxpRlYDdU4FEAVSIUhMjjWP06zpcFT4r9elQ72X6jDyKrvgDeODgcgCeIZ76nwyn4xqXtgkoyyEUq/VqON0W7pbCT7rb+uUUmS2dzczj3fTgKIEdOvjqR+P/IXR/BF86LQQUlMwfSTCXvO/v+x19tDUXGztZcq71tZXPGAEtY4ePmbddJLVpz7TcpV10JcsYE7WejM/eji3M1+3hknuQNS/kbwDql/szVwna8uoXvdxDZBGDh9t+TqU9b75axAM7bE5a4Pe4a5BQai7pplTEE4Nsu/f0nM3Lzw2hF0M0VkIx+m/rg59WD6nOwKyvAwE3+gNkNsVWyY5khZKABWjwCAri0m7pyKmOEIuvCHTjtk+9v6TYckec27yheR2WJm670i7PpPlgJlApLVNmSdplihopNiy2YnqbgfivRKyNHU3xHEwtDth7c8PFgkV/63MFdco3V9+Su5xxUfdSXVz2vVK/MGfYK5Y8Hbir8oUDXYg/MP8ANtaBAcy8sWn6kPk0OR7twuY+kzbz2dGpG0mrM91dzl/DW3W4xCvn1h0DztYhEGjf0M6nSx5woD1qUKNmTVzHhh9BjYJNN5dPPxcUzbTscS+B5K1R26tsylojHn3i8SQ2fO5wV3nz4iCmyXgbBymMNfeDuxiwuXKTFcaPQpNluyWhK1/43c8zn4wK6NibiEEwx+8atP/C/SHQXHtjxTdDqML6pdjx0ldl1BYcl1SdwVBP1B3NQBosbmqRbhaLvObF0o6ImHvPjngzk70v2TK6NL4fWK18QeYaIZpRoIj9VxsUnZxW7Na7JLojEKTmSfaiktnBvTxlGwD6Bzsnzk22PUNpDOtxI0niqwoYp4JjWuzXrxBLhpbBL68DY6P/WLQewltI9UsaGuvA0Coi48hKnZrXyXXKJhlPkz1dmZZAFzkzT46GIc9G72HonSHwsyD6Z0z8D47UPqqpZcOBs96qyx7Sqx/nEdCHORUAGaDApN4kgEvZd5fyrnRcTw4A0xvlswJi9vVgZ92v+LJWEGzR0VLjuIZua/OWsqaSB2b62Mfvvj0HQLJKx2I5ffos5cxk/x7UDnfiWQ4ff85NTq9aJAsbkSfHDnsarrnWArNCQ8BhBRoR1cu0mjhaPM7gOmvxnKoPynIUnqwePfPeXxavgFFBCZZ/rJG4NefMGqmwVTtRhof1LqdzHfF2ZltnIA00weQZlBls8i11orvXVGw1Ya/+N1TjrUB4q4hRhRUF2FHLePlkn7xuwarGHkCnLR3OyX+Qmm8UK8oIkQHIV3fxMZ8/M9XqsQoSH4AXrHr5ksKZF5qZjEkuhjSufMeX7BiX29NuiGbFZGbbCtBPvzb3UaCBd9MSJmhW1p8LIegXU5UZJtZLYohcUedhseyYBFo+q/tz6LQu9fq4wz27FKvxPapGjGUQcXNHRgJ1WRuaNGhmsICwGTxxfRhINxGPsoH7kY6fhEVhcjf/KUEaNPcQWhRfQ7ylJLpY544LdsDwhvKN4BEziF/rH7kPI1WbzW0Z5hD8lxuWUiVpvGM1961tapKa9lhpCkQEcDYGr1i5v6xblyeo30t3dL3VQzxQJX0Qh8q6KYHJdeMqnzlMOnqHTzGwGlj4f0dGO+p0Ta4VVSRY+2dcep+0DXS4Y0wrb9LjV+Y6uBoAqRrOgg6JZS8LJywRfgt2Y88efzJJ1lYTT2MPjg7r4baIEKD+SQbml5+9lWInGHsrf/di3hdg54UYUm/C9D9oiUME/eEIAf/GhnI88yCAtXR8z1ZLXZmY14ZtI46D7pjn1a2Xl1UCqQoepwZZyaJVOf11AIkmnuxVyvKnEvLdJNoxxmR9GPJAksk+kzi4QiPQvcybXmzipZCEQsRJVgZka90Ab2/cqWE+EMQZNZcsJCoXhvslrTSP0SkGACtBySgbjohtOhET0qdDH73Cxizb/yPYJMjWid3tbAjY2PkFAyi4JADXQGNtxML7Kzo57w8vnma8NxE/AoOP/s1qAyWshD0wrz1YmhkNQupPIQAwXsiDBajnrrDkdrRjZHSmqD3dgeVxVoJh/BxF7UCW8clYtNTbZIn8y1PSCEB1w4Cgbk5VYXxxrcPNfMhOcOsRTkGQN7XO9SARFKUR2CKtt/gpHTHBrckYmXveFhZEa3juNnIkDR3JMvIjNLN81+btgqJMUP1Cr099YemsbpWisHKTGYoSRiEuzwyXSUD3UwH72QBBrJMdsr+E50+FSAGLPM6S+lJhly7q2M2HZskt9f1U5/lVaNGV8p/VqQwzT2QqfcmF+4w0nmv3hFeAt4i7mjbyVlsqJ/5By26cpWbJUiiclAhvSlD7JMkMw9Aqg58RIOBZMcBHijOWCCoVZP8+v0p+u1JMjDGU3TP4KUOHrOuhGMJweqLfi0ONwNlamWGQnDaBYAT/C4lXGWVd05MdmJILM86jxljBNaDsctZlHqZz5BjADB4NG89VparBxtmdUw+nFUBgNPCSrRbYDjXuOv7kRpVEiW60ajRA/qisuh1vP/BWEDzXwjQMCl2wLcnzSC/lsheqP6VFZVeRxaHB5kkHTZYETLv5TXkVJHT475aoaSr4QPl9wxHs49GDmWw7QxRdclFFh9bdDpYOBQi7Y8cbu0BfcRplJBos0vmUYh6WmHxebRnWhslajO3PBypC+Py7UyOs+wfYmrix02g9dXEbRliK3X/bUGOjBmHAbEKPNVwT0hgCXYrJ1hE+JFVI2UIEG6F8Qtl92toxXSmjMqAx/KQE0G0n13lGuLXzEH7OMi1ZKcLiN56zoge/iHZD73/XRZap9bBdlJZTlTBn5cHeUaJvE288/hx7u2til+qQHJVvtn/s7BmBxN9dBmU4hg7n48AIKOtWjzs6qLzVX85TmNGTazfAo9IfNMC41O4SVlGHkQU0QH/xigfxf1CRyue9VTGt8HwOoupQe4uftYpqwKX8qhIwSQ9wdcgrK4NMzfOKFG0d0Bw+jo9skrNiOFK5cqhRWo70zkj9/dK3mZkk4H7+pC+Mt6kva1q3WQnpKXejJUAksb5NCEyI+Uz1XwQbWg9vYPQBzLNmwE1hqdTGgFu9j2ZakPUsYK5zs38PwspyKr76Ei9w4fzD5EIwlnxu6iqQUE0CJgo3fmxAdLD7Zotoz28H1oqd+bmHLIzV43Gtsd3R5tvUhkr0B1E4euuXyotNlJ/9td7Qa5jsl/sHDD7i8jGi+hPtF8tF289BWsEXs8uf8UhQI3PANp5QRl59M/0b4KTw4HEtWWq6lhXax+rYEGHsT6zvCWjDo8XwSzeBpJMhgHsCqXgGsukfYZI6dAxwKAVF8CbSULzUxYi05XgggLWDmRewRXNFKCLEzOM49BWCZx2oOvTMWFqI95wIg6dJ70cH5jQWHGrEpe0hth7geTqhmQ6h69LnhyVqaQzG+XoqIpQbLDYo74PInKDjYfj0fRUD0tNbnqzF2TgMgk7G9/r9Bfb/JOqqNOGO62VVdf7jiSDk854r5BDL7xXiPLMy/8mxrxf6qLLrem/zm4PM2KCQYmjYu5Eq4olVJPrsj90jBWeRaMv0y0c6oZWmnc260B4bIT9uOcoHEgDQCnmRa+SS8fukAwaCsaLJVgiS+3hRr7ARrdn8mT3/PNfnBp/Wt6BpWmgyA6CI1V360EIgCCC5oY4vy1YiaFh7IcS+LfVR3tZuODCnEP75HIrEqwn741bQol53ZfbJIQUMdhVEHU5Z1n1T/m4zfKI0uBiBqZXWodWNFkUq8FqjtaRpTVeN4TdZv90wVOoGVFJ+w0qt3tny9BkcfhN6jRKtDXi6j75KrZel4GbVZHmaxWQM/VkKy8Ck9JhUb5HlxvrW2X9HC1PlH8cPQGgVwvY7hzZ6d1hzJaOcfyuJor2o0SV2lkPgfGDhy38p3aNOHoZPvQSWNqtW6G/+j9rvDP81DsyTGyOttV5fC5I6TL0qCgj6X8FbvgQr3kCrfLl/u4v+BFJWd+xVw5UFo+k1E0W1uJGsINq9sr1osJZaYWocWlcL0P8nBnSzdVD/zI5g+TzRpdRRJjAm9wKXtGWzs0nxloDBDwvu2BQvystU37n4z0uMB91l04DR7eXp0PeYU76/ckPAtvKvIZZXfr2uj7MjNuAt/muiS1SXle4svcB+gF3vFWBgwZX5eHrKs4SpRFqj6ppOrr34E8kkETFmP19gEQNBS5rLTx6V0OzlWknqwITPOjWSmJnCM7h4iVJi4T5E92/y31lMcrG98qt4fGl2L+CNT9V1zcMKPzojdWgj8fsOwCJnmJesE+p80IrebMpe8BoSIpinwsS6qqTsdGqrPJq2+Ih2iJPV0CXSKY9RICErX4wWx6sVprAxftPITqmaDl5IMc8v2dj95rYAV1nhSCWqx0WtlV04ij98fP7pL6p5m5BdEYmGO45iGke+kffRP0sR9JZyxYAcFebygfm9+300xrT1EocSX0uPoKfZZw9TM9/2AkUc4pgKsojK4A2HloEIvQ9rhJJ05XllsJSB+1Jq271wqNERR16SmmoV0WyMY0rIPhsKYayGjqT51PBgCzKZupLWHhKn0UvirAJPojsNGpgi1DnuWzHVbnHsa2lui2Mx33OapoldSNrXk8Pxe4nW8hSWYRufrT7bVNoy1Be8Op3esgnh4skR1OKRPJQfFvDccazqEnZcm5AJEEVYLTF75rc9mYhNxJaUGuciAEL0bhHH5YKbOsWP2Oln1d4JsEv60DOYQ4ItHKTjlhx8WKUg8tn08EJig9HZ5m+2NZJVNM4ru7sStM6cM1uJczgURYvVBdkCgv1ySfHGc1q/PauaTWc2g8trchR2Y1pynPm1OppdV0rlZTBvvQ8EbQqQ+YcXL2cX7ikfWE2QbknA8V1b5+W+gg1G+l5uazp1hOduAWLXRDdqvGrEtcBG8zWk0R/L8No5hX2JZtbijKF55ysbEMdPCDNWasnT719WWPkK4TTabXgEq/eKidxA9zSH9iBY8nE6Ish96UtixOyMfa+/g2PpL/FNqpZKlOYBCVVwHGwivI1pfTzXSQxpW05GHBsaUX1N0s+WGsT7VUu3Ntam0Y0K7l5oDk1jdT6XBpOsIgyjrLCykvGnOLQcLhTyMnEMlaWngVJ16JwKmI0VHFN6uZWOtXowuRNDWVRS+Q+sA7YdsuEfmDPgiFX3LcO/oznj1x6+mwCzmYF1ACPUgFYRd2SWbWH3oDL4nBIWBDQSBHGomLjFGtDe6+JtqiwpRNZwjUyq9xgtA3DNETJeAwvKPSr0r6o5v2IiNVDeUNlWECVQPvfHAHlT+3SzBjfx4rRo/ulvF0W3tVV8T94fK0WxsCGAbS/HTuGcxpIUerPV4zHTsXkDws6Q2kVhNCnGK36UQm1jzOU8fZqpsOkNPeXcBEIHjXUud40SqAOv5dF38VBqdGe2Uf8GHHuqUmHlE4zIZ7tZuAvO7R7xDaYSqZAi05iLqZjVOQiMYLnrcOoqVsrehVo3hgO6THYV9/ix5UDFZ20evMV+PNxCHZGq9fQkKm/mSdBLpaHG1Gza0NsxNzUB0jdkwk92W5aUf+o+L1B2j4Sb/I3Hanl8VMX2Zgy6TSCTyty3C60DAXO1PKfJ/P3encV71qGqdlUTDTe+x26jC3S6LTOZsRiW/gbFmZkcKrnyEddGvukrrYMT3hQrnE/rse4wdBYK/bX7ZKdIJcs6caFrRS3gSOXSPOi+HjZlrLKz3Ni/5zFodOQK49058JiI8/LELZ8IVgXpA5WaqOFtKrI8HjWVqzFpa4lgNcQpA4srtsO9DvEqlNqfNvwEo7xoZspUA20iGRTV85jc428d47IG5Yq+r+0a6d3OrVm6Dzwe4g2xQpSqReFDsOkGrK7pT9jE0KAjEKAMzhzcx23yGKMjy2cDJYPlodybAzIlNC2sq7vInqHlcE25mtzOnlM46itwUcRIlIObuP1FJQRa65n32edDhaEVkhoqHrQtmeS56DaME7NwyiXK+xDuoL7qL85tWhuQKZV9iInHujmLBUBGdae+gUtnVIkroinunE0c9kjc65iaRL8L2e2fBycpWmWaneZ6DlHJlijjfhxC5CKf3gQxlRzuO18Dg7TExEJcHtxuAEWCP54DS8yP89cnADp49VFxSIZxwURhpUxr9VivfQQ42ogHI+iLvTbFp2sZyicMs8IMwOy3wpBOlBrcE0+Ct4m1ck2vafdG6dVyBTlJe8a87MAgOd1fNFBao9//Yzev+MRO4j/3hYaur52WQKURQ3mtFayqeONGjyg0tCYj/KYDU30Y0BPnFrL3HPBan0KM/mzTCBWtSDinv4nENJvbZqVPugYsMB0Iqhxomp7TSBkStxMVy1G7YaMED4zSIsYzw+TzcgI5h5OkQrDnf77A4gCdvgBkzROoA0d7BPHncJj6Z9Zr02LE41rZWCqoMUbp6tgBsOOYT1RGzZCidjIBvr+38pOb7b4Q+eKo75yd9qfxUSwcWSDCE+AZNLo/kgQ/KZYmY/QbuNuVzPyWxFEEKNBAsCrpVYpO65PGPOa2iSowtza9ShvYiz46a8yiFloQjlhPbcgufYQKx5jQsxkz2X/Fp5S5AHBYlX62/T79Q3q7r/mORJAXhtBktz2rtpzccq8DM/Sms5CQO0H+eBZiwDOkyZkVp6FIJ3GP3Nv10G8A3RyRbd+kuWhHfcGCKxJcl9u4Qg2cozPv5Thg6Ux7w5uuxUV7OsmOA/ks4OXyhiESEZNVT1JEOkzc2TZ42aqiGH7KxAUttyL7C/ZFpHQCH7PqHKG1zNIZaksw3eEoPBdpdflD5O/+HbhfKiRVaE5hmyGb8KyHbuIt8Puhg+h9KU4/Y9r3Ay0XS+fVvhfkDYFBn8DBbKpRhBtagdzyzr/NLXwoq+cfk6Ghmmi74XYEsitz5dMrixEUEsWuC39kHUtXVMPNzRMFgm/9v7X8bdiAt0QBa9GI41F3PJvmklD6lMamCkOA+z+SZhiNikiBa5ICArQDH+tFnHvlO93hzs1IXVfubVzWuOHq0TjmrB9fjWK93szTons+TtlYYhvI6WfLRDNLLBju4GEElWGqzhxhyG4hKvWArDyHOzmmiToiWknT7sE5O91YEjVUoOkhgosATKZZo/HfYQeVs+zSUuW/fxJSDE2UAyaWoYlehNY/7t0PcDZZ6dlLkXXwyD0sfBLX7pqHOkhg3cvu0+OOyse4n3bvSfpOA8yNLAiKTCb7Y6J7fdX3EOD4EjUEKMLcd25jovN8U5/PAywMSqZZmlGnmWPto3OfXdyFPI4PZb2utWzN9meae9XMnigglOg6yNzhYpNFWOyvzo917Cd/yVYcbJ49rHTLRjuidQEY43gW+Dbr1X33w4H6/3RvV5T2sFzHzfGtwjaWme6swaqYSbo1FpS4C/ubwSKqX3r1GpBPd8ip8pa1jgwW7gklMnIqDfp9lB4B48ral5pjCaEsC1HNF7b8DExzdbVt4zymudTsmFz/1ivcjbtVr5IbKeyZ5V6/7wwkYh8zgeocCB6YQAnNDjuxbP+xdT1wJr7iPWr0T1qUxtkTsr6VpT0tdPU3OBBn3zPCGL0MI7fiWVR425Wc01uIgILi4FwfT2ddiE2nZN6l3auyj0cOZZl7t6bOzsKXTJAMp0eZGWbKDUPxBmO5CjT2/pyEsMu0C3cgjGTEaaZwm0yZR7KlNuzBqIDN+q+XtTmsQ/oSrlr9Ks0ycNDTmYZzbzi7ZKsSQzFegNWtWHnvzdImQKUAUvrgYHt3GzTRk+lG/bLODNtqdYeLJLG/8D4qpXucJa26SZ32y7ubUKnMth5w0j2fouIKBZDUPctT1XfDM3mvtyljzKqgvZ5QqFK6vx9jqz8Fmj7DvcbuVkWgo3PuuZK/WugxakB/ytukYW2bijMuaG9sObT5q3VS5343+7px4G7hfzGqrCH3WnsNxcix7bKHdHOSXuzQbSNBWR8hvuGMNl76FDN5l/NKPVIdoz1kq9fLaVc15cg0g+J1oloDhF6VabPKrzxlM//dOoCrGk3v052OxFuqnAi/e+TOPcntPs8u3YxzOlySkMUspwaZYZrQsdh6soVx9krG1rgr3S10bEUKZFdM/PWZI1d5Qj2GBJ+CewS5jpofyf6QXKqeCTfXVp846V7QP75AJfz6maxNS52yeRpfYEgJYBXYYHkpMQX25RxghreR0mV+0MB3qRkuNU/T++5xBCo7f+VbGxf0x8G9jkFoznQl7LbXPUWiR2A+7KVEby3Nywit8lv2XI7olNqArrlKbHyRcx9nO3l57RbtssIoiM8/wDFpvjL01ghRpJOefYQomewRqt3v+ykLUN8IxafhBrtfoyfsTDIQ6wDxV/9zCL1Tr/xp6wFk3uM81Jn6yB1YwVEtut/leBoI76Yrbx72iiNNYMjFp1MHWLuH5432eHQi/B8LhzBCY/b+adml3WS/Dx8mA6cFVMED+6kPZJmJJKvQAxrDIfNLA3BRy3SFBXM9fy8wVtV8oSgLzQUbQtBwpfk9s9Qi7Ecx+w3d0EddNBHdIUbusq8nO2z6h119ZCV4kHlU2KlUy9cYeo4vVlNTchxD4fcN/ArSETAQxCIj/UwIPHe/zCxeZhuM7MMX1dRQpBSCULwNanQXdD2cg2xq22VWR+NIsIgPxYP4Weaq3t+u5buL87gVxyxCF2c2I9M32GjzIzZq82wGltWLxuBOHoe3uKMJEPwPq0jI4ee4d51vZZRLB8arNzYfIza7rNrVwlVL3lsEjIhQAo43bozaf9ODycXRe55qNC6rQnyOocikhv89OkWcAnKxGHwjjrLyn7DetjV3qIS2vWw4pRxyPZegWvTGLeqQXWkn5nFcsxei1DD5KQIO4iASDSAyUnhVLfTXQCMrosNjHhT+DKvx6rWtBKIEA1KvMtShJaDT8dsxHewSdDKG0BhMnHFRfU4nJ+4id1aQeXvJbmpir6oxeTw3SWOXiT2Be9PDcX0ffp6TEazvw3cJvTM0so4NZxCAIQ8ch1rDpSSQ0aTvR9nL2IpsHIm4ddBDgh4jpLeCZZOs3mgOxrIIP/l9ps1ARZCjxS4Jh5pmXv+eu8Bwu5WPukLpKIr7Tbin+9j/MO5dhdRDKFz2DgP+QPExy9Wneu/xqJfP8z+JQ5UrywMTLb45ngMhIi48jevduHFbXUYSSJBXvg4n7k7N9963p3JrQlF0xTeytQNIJ9GotBxjhVO5qRC2KX3+gSzcQoY72LlaI3sAxJGxBF6z0I1uYDFufR64LngcyLJy/aeg+rLOmJrdcbT3ndXPSiQadAaVhhF42acp+OJ0POagkgDj8y8QoYrId/GkFO8IwYiktGIZjAbRPkPZDqeUavvoy02Z3bihbeZT8RSAqofcLVt9LcIuDOgvcoTQFK7DIvPc7KduGrUyQaAajOabc2yThe7EZNqBH/8m6zzji/8sX/lQkkes1uhVUK0+sbpM15Ie37ybm2dO/Y1Tg34BhhRfeYqkzASWiQBdaAo9GgbKTryY/olaEFgDC4nxBxq8Izenki2AXFOHgAEDPjRFtfwnlHFOg6afQ7rNeYnWaCftzw3gPIsNGxPRGy6HzWWN1NGpjx9Aw/YrhqAf039P96mo3mBh7xjULzo5IYJCsaGSyWDgq3s6qDMN3cz3qJ4nfZk2mNNGxoDIpFaUchkocRJxNiU2Bkkl4MIVjZ1m9DpoZ/9yGZVjy2nGkc6y2QY+Ji3bnwrSQHcs4gkG5re6kFZk4ylAvtWQFO6FFFUR/8lczm5mRaB70LVPuclG+IF7ZOQTwdF/8SuSGI3FrlPNeEqtokxUk2QlvP1D4pDpuj9vn6oET1tanWDUctnzw/kUMyRtH5p3IwSAs2bklrkqnxTWDqGzgOTb43QPTEHT7OlJK07QRlePyWTSzPKaIBHM8eiXJGV/lb8BSXRP4cBBfADLhSO8W5W+fe9wMGCGN1W23ZElw6WEsWKPbUcaRzqpNt8cjdITXMq2mXOsuWuQRQfkPhhq+cI/un2fGf1kE2gPyBraAyWdICE5nvsavSeUzSJczHu3/txun9s301Pv+ermwYFL7WtdS/iK/ofMbYenKJJe/GGKBrr6i2xLqAJD9RyRxxk1cpDaf+6nFkagQpvemCRdAcmts8O7Cke2fbLTggeYCWquwcgeGbPUnpaciv7TnYrOYZSVZ0doKuWXYg8+hC3Dwz58YWabqyqzatO1xT+4dPFh6+FuNCwLfw4UjXZZE3EVQLEvq/vYdpakVAz9VjVaG3gCfPkll07vkzmKINoB1BGZBhlKhS7F5TaJ4GXS5Sfj2A3CkkaUJb/dfWlthvyIxy1B6yRcBd2I9kr8kR+H/lzRh8+ylCAjmXq9OpASQgYqo0xcJPt9nhgLoiNAKoaf99aPAaXo0f263d6m8bjYSTvVOVOihuIBhYn/u2nev0lNz03dPERrqHpynW3sv6Rj0QneEvNDAc8klgKbNO5ll4yoIcWw0dyA0E3vGsp2FoADl2u4tFNfsJjV2CrnHVY1zhDNxZMuqy2oH8gU5jE/KcESXqCdXxyQBKhge1P4qn53RxYgOW0uYtgUE2assBTFbPTckawDPLZCwnpYgXJTzuPqnR6iOJMdjHOt7Uxxaevow/HYSS97VzMcTJKtZkhymn8AlJxFaq/Aegalm+63brsMXWpbM39fgStIiWMBBDuB9/h1nv1NiRCGbh+t1MaEM/qjMTk2WgcMnEkaz9qrt1F36UtO/W48B8w8geVaNCDsjXBNaQyQcZgR7lRvyTOT2tmPaaGtVliLnwvXDPr2qPRLdvUp2pV5+lbqcFAdWZL3S7QRlLfjmZD4PXnq201FNn5NfGV6Ij8BUAy3qTbUr/EnzykiXQFwlXeKuewY+OTRkIwM4aB2fdAwftHn1e6qFRo7N9K+fcHqYb8/uCuYwHZk0LAHKRVXBOS7bF3Cb9yfna8feSJZuQHIzC608oHDMc4jKrFko30Vr/wYiMtJaE3YubjdC5yVTcERJ75RiVQ6QBJwkiAEfIqWvbx/GTpDIaiuAx3lcY3W1gypej8qrq6R0lMbWaczeC+u3fUu6PPm/plL3FNdw7esuPrAH7WXLCtR7NN29iVbiE1A0E9gZrIsAtJzV0EQeGS44uqblU4sw2Kc3fwTPf7mPAqUto0t96bQfBlnf1iHxtVSlLhS5V5eL8ZWHEtnG1oVzAyb/ckxMs9vyaayDhHH71dGrmqv8dfiTbQhoXPgUI3SqvFqBavu9tvV18BhFF/3Zs1oMEmOlj/h2OJJH21LyaBQMuOfuKzUwX9kQVmyh2LqUEmeaQ3fRvJOTjqQmguWG41mlUsTCnbqhVn5uNxXszBGfrTkUh4Aqc1AcRFN1xb6r1ldj8EPuH0sChgBEjwSkqkKh7E/dbH1bwrHPbI2rJfie1S1LOJsKMcTTGCNnDwWk87iv+dyIkGaaFB6sSOWelTaqaDyHqFBgjjHkmI3VK42AcPLEStlHSoLwhXgQdPxHzdzIH2awo46UQ0nIqcGJxdEiyQ+oSPM+A1HVNMaB5SQKv6SebjSGZZVHqIrv9jZOyKmK/CXihzCqvnZ3tWHceEcl6P0yQqCjwXLl4hmgqTVXMx6kHIWztKvLDf4g4WAFoIxPcXNKrLLQ4HG9sg1AUCIxx/eD5s+DLrLws22rWNPP2M8cRbTsKI1tL0yNoFNy3KxXjABLL0DuyS6KmRnvGm5NeIEjFqSSaKbqViSqgYdbV5rZZNZsvIPGl503HMaF2WuXt2gxhOAf7/1CI7YwpdQMAvbDvEl/GPsLt+wi7ZCelt7YMa7sS6b22I+M1JBQuwjcQS7MlOH87OFOYr5jJJc1J+8EfvNJNys36fB7WDJOn2oPQ2vsq1nVBWhCuQJp3WxH3UsiBTMcuoY6Reb/FsMg5qjlwH0j4X5LgJzxe5v743j+Z9VhfZFbDTLbZJ5K1nf1xQRnjO2XbJ7uXnRk+u7Pk4JVQ2HqvHrExDKJSUpYUBm6gq8DDdu16/ty8uIiFLq8Ftm/O30uc1ir/JfOO/f9MsV/Vd7xN3TeckTn1YbwTTV9f31LiCSKkUmrrdSjgBY2Fh/vR/jY7mpnVmDpzR5R6SnqgekHtu+/bbKsdCQtahw8cHFD/Uf1Ne7T5WXZxKVUFSDqXnddf6SVnlpEdvso6FiRUpb6fvnFJLIyGHcr9VkjhRJnqMm5b9rq/yrbx8fgt0KHg2ozI5UR2caDVzaJ0RbRMyVNU/ymwGhiR5G2os9ai0HqUDs1st37CgpvUHouf2+JK6VWgQirgu/s6fu7kqWdaHFiQn25m8sh7YHuoUJwa5XlYPsZGaAMTqkOUJKnnGEFfMtFX+c4C8rFEyIKGmGfS4B9W7KJeIIDdLsaKVOoqO9Agb5lN+d0qvX3rmrgUrUxf7nfXrEJJMq9WsfphX8VDn2hjwboa20awC/kCEPYW2X8SnTQd0A51A8x2m5G1SCBZC+4ZHWnp3pMkVrpmbc4VHZT5JLLVlhCW/4RvBgv1vyUjL/l9yhpZOrw39CAFbY07hocWJBydNvtUsXOUupEEwkoNm3vhcUzQyVfqr8dCtprv0joV0X047aGe61OmW8qF6/gdCBGxRG0p54XT4PBt9kUM/z1S+WMOx76UW0NWbW4epCNhCPwLFpR02XrD47lUfqU1D7NDU6zp7ebeGsiUswAJkL3NB9sjfbVRfLe2YsQ3vM3aV9p70frSgWwa9F+tdC2EvInH7Qs9KDIMO6LcY0k7uL1NEuGmyclZ5/4P8LrkMOFYGqtDPuLbMnyvVqSU8G3TCM/nujNI3d0OTCgYSzE6RKAEJl3DKB+SfyxWQgrzQRUtGxe9msStK190jnCCNs578Gd/k28QneEYIdcRkFnopLWrb8GtSl3K2xY4k69BBJc4UEkNErVXrjKqeoP+6kPsfsK5VKJEVwI38fQgdpLk/fWLLbiJEDLFXLDKTW25J5dicvrNq8M+6lmK2C8k2c7LPTRN6Q7TAYUyh+Ukn+x6zFpTzVmIfucANz9TUcavzJg7lMSHswa2tras9dUIlUVaGy/y7HLZ6MRXUXffN11jOte8wPgLNkH90Vrn3oN9PXibx7CUY3VarG76s5p5MK9vYOw4ggWzoZQi5zf9vqDU5J47YEvM8cYHpovIKTr2PrDXnxJLDp3TzAfZNL//yC6LYuia6T9sBDzmqaDnU5rLCfxAmeMdat/zLOvOVR+vPJtzdev9rL4iv5E4Dj7cnsAFo7mUk1sclHfC1uddF056Hp3Z58HnHXaBC0BeqGazaPxGEfOKWZ4t4l1o0xJGWH1l9WwNgtzd/8xKh7xcCPFSL0s3vCs5ysOmuzAfBFBfSpQP6dpGoPhW5wDhmtCGLhtsdVBRUsL/tUb0uOMTF4HpcHjbU3iHnXyZ9NA+T4JaxJm6pOREGdCD4ov97bT5nl8dxBsYC/mzF+0PwXGek7EBtmvHF2Y+c1jAlzoJ7XlCghiEenRTYYqnWfrRPsHCqCyCFXO4MwXG1Sl8PI6LhnKNUgruye+eKNCTwYSZcCOIU36Jl4w7P93/IIugZko14Rd6vgQEJIZsXvfpoJXIA4+ev5tisPE+RsMhZBvWkLHnkqRPdJocVhDmuN9jZa4T3B+ezl0ESEf7yBkb2mzPDZseq+CuTkOKd4euonwFjVQD50adrWxT/zfDbewPtjYqwUvsbnC0wYsVHmbWlykcQSc37f4yuGW3dTgBE/eEjdE3hi2+sDgHBJ8AYXwjY9yPSIEYPsI1XH82l/rPnmfvPC1QpEajYCq3gsifHTOIGGdWcAZDHw2miWgrZo382vSDmWwVlg98oXITFqg1IfU1WSdMGuAxFoxzWKUg5fAqm7j9CsPf9ev0d297HU1EY6ztO5WR6sdvAbOFW3HStJ+yHrBytbfjKtCJz7AsEFLBWoGosNl06Q2iTGQPTumH+FgTpgr/RLESVC7/W6LDHVscUUKLy5PcdtYk5Nzq7vyWWtUjPEY3kc9d3w/ysCWpcYDa647E3/neT/JLhZKkip/Z+kVnEJ/WIy8Sf2XS+kqcIjQiSuGnpl8YlrnmgF9l3VEez0aqGKvb1nw5k+4hZ/s+PZBN8MzPwqy1SH1UoJ+3nttKPNh02ZBdwxS9195/U90SZSum+WCfdmJTS+Ewj+XYucoEPdTgsHIY5OZTaQpvS3dCIhy+GFbPo05cZ+sSjqAvCaejGVY7g1Im+A73HhZafRg01hqZornhbSSeu9GFnv65ZWs2LMSH5Zh32TalOqZ0GDjjQrmKnItQtdSfS1YlLSpI8iruQ0uDG1Db1856x+Vqy+iLjYqV6ptefE+oFODGSagQcf9kUnShR0ZJCZ5LWkUN1ZWbBEv+2iHpvsS7Eb5yDgTg95+pyOnrNnNEs/3hnjf1fPRw+TNKx3BVeFUq7RFcyrlz95GzzTq87XEtFWaCpFaKZ1LHpLGubkWav7gW3uOkADua8xb2at50z6k+rHanZgdKIdBcayNEY9qFvEfoDeyDSkwDMeZzTUw4sDjoBSR04FDkfBU1RtLJ+EJcyF7zVP2UaWtpqgtqWnBuZbun6BOzlpEQvyVjv3FtmyDAJRhjnYKNBx7gd/9EyHqtKFpByO/F0eca6ElVA/10JEsPLYcXEmtEA4CBGmZox7KCm5PLQARF0GaqesASU/C5ssO+FSNiULxNOAs4OkzvjCbRmCdQw6nmuFKpbSLene46OKwvb/Oez8VW07bQYEf6ohDQhXFKrKuMj/YNi0+2900nVPxaqlSW8XB19Rem67Seu50WVxCGmSEgh1L57C9N/vLGiDj65Muo0XF6y9eZG/Ev+IAgoZsutOKHPoN8Ewya7R8MtYekL0rzi2BSEqM0sgjKuR3n1kvCl3sGV+u4gllPoZCAFW9AMYXHCyTwk7Y0SzHbSR14ynFVMvt9wb5rv8e3x6ern0g3kB7y3HnLhs3s+brpjd7mHz2NT3l+MW96cXT+G4kNsTI++/BlvQRFemfljXf+1WFQacE9AMGYJzOVQ9BgTqNtXATVutDAJfdSOOJQX4eBQcbmVqTE8obLRjt9FC4KRyysCddN/0YmF4WyZIJw9y2TGxjpy2/PWvhsbP+1DYHOxmWiGqM5WYchsQJgmaead4yv80eJZ/hHe8OqfOjSJnaXlrsMqveOJsOSCnMEqJBgxhvH6pbiXcfzjKZkRQ8912m75G3/Ak76gf+hVL/LRlFQzn7Kw73HVAu9GAjdxlAolhS+KH+9cUTOmRI76lPpmeIWjObXZ2hDQe/hbBQ/0JR1boeE3cIxUhQjNxrnigAKdGOUSRr5eOV1yojJcSWz1GIvXvGU1taugk0Fp+JdLnPqZ6OcV2Ic2yzsIZJpvZ7+GwOstx0Q6FG7bsoJBOU1zJ1f5teETcdkbdvYYhuEJSguYg6euJ890W0yA/TuAJHWqTc1XdHBsT1tz/aB/5fS0TbzglUSi1OaY0hwFrIoOWk6p1uzXQ6ARaYsti+5xGJR6+TDvlTynJ2O27daUWtID/L9rL9BtHjTlmZT5plo9GSHh8mmXc9GRhsP9+xNyQT5VvhoDGYXNUO7QIiaPVx71DtUeceKFO6Is2TUgmg4CBs2P8KZUiH0SFZsPzVgrSb1A79iwgi1V8/CrYUk8pJ6W9c52PfTQ3YgI59L1UGhYSKWY11NbkqsOSxzvs6gMLpOZUDhWPysMmJGFEu0cxJW00rooGGpu2z4HM/0/uspLO6AQkIr6j93NshCUPR4d4I1gTrvTu7fEal5nv00M/0zXeZ0cYCdqNENzEUHTL226zLjgvRwp1BFRxR9eeXj4jpjrM+zcrdO7LCUv4PLNQZefzDuWERgXfst/ZVqAOFy3AmRiqeKCDhrbnjeU/2UU3KYRhqL6xGR4+9PXff++KY17GISzIDTBMMLdeFYD7B69gwewrIjByJLvhmQYvtahXKLPYLuwYNfOlRHdjJRv+WLrchlpf4pV6ItWYSoumf8eIiW6sWvvLpvlnZ+In2Uj5+7Cfq2pipaZ/SoBe65thDs1dBgpk9CqOTcfkON4NI1GtmDTH0UqVn5P9uzkskCG+aUB3OM2rj8iTscEP4LG+woYLw8qYFVqgfI0+c1AHzAE1u4+8EkGmq0/lDHBy1KPBaiamfLtZzs2dzfzLs7UoF5KaIlTzfsiI/X2rESnY3NmEo7DmA+F+XKymbMw35ikEALo+uCX81b+h34j4VxS4biCwqEMF3vwbdKnOR+cq+mdysKfE9nFRQtYVjPyix4rsDm+bpHDgslPxnFW58gfEfke6cg/Ol128cbIEi4JJ5YZon+i3xQTTJ9LYGexXHW+6nPnnbpP+ZWvHrhlfZAjjQ+XBtUqGV5cN1QIaDFaYqXquYxYTqmHKu5x+MF9fHx6KJMQahnDncqRIi6pOHsmP2dJPkIjuAG8TYobNOz1x6qeTGmQHNSNR8KeSZWLiykgzbDjyd72HHk02SwV/bAI0xhiQqpiNjcSxfXGB/wOhw0TTRiENkySjAom61Wn3ThYkC6v70DVXPYWfXhvb9kHRf3sibLgvlrgiK+a8kkeBouwWpXiIyZbB4g/C1jU7X8QTrxsHdCllt1eSQoaMvyW2j19OsfqsbYj4Jv8xoDrwFuv4bcjqIF7oN5rOIMOkaVF90lt3jc3LvwqS2X5MkF65oLZzhP+9RgGxIP2s4D3olimuREjTRn0QGogwA/FTRomqj5jCMjIHopPr31q8RWvzP9Re726f8k1sOJQMoMsaV8rTWA67UavUKdqp3BdLIRxMqmZaWXMe0HTbjdzUsbxcUTCUDyLCbjr2RJMvN8nvqXOw8yQ0OpDEAPORrVmK3YjFViPm2r3+N1CBtLPDjuX3Um7fNVhC9z2asJIAx1tRFHqlKGoS/a6ogk5CTjAKAec6a7vP8Kf97mCkUTsaoJScOgCc8f0kKb5eQ2crMkTf8JbfePqP1Fs6vmM+jIxOviA05gMINUAeikeeNfnAVdvmAe4NsG5rbUvO2kc31T9v9LYKhGohbtk7iX0VLXjRPxio2ZBtrb4jjHMdDk0jGTDiNhuZofYEKQ0Ckczb4Yu3yunpAYTQzj4Bsi3CLAIMTmqFqP4OBkbq7i3Sbi12WGO89RqujPY2LoEfvrJUdQCZEadbp3StSYTY3JL8EadHKOrLDZ62KEJpsV7+R6hMJMqorkYwTEO+vHaRew0uKoCkWtHyNgutAgDmKEHyBnYCIug/7IoDP/vo+7Wu2PbQXhylL3rFK8rJ55tYqYumFm+DFSdnQPneirnJ29GzRPfyM4478Vw9/CTZqHgp0vGNXOI4uh9jPBQs6VZKbHmtc6hGkTdX4/nqqPbi1ma6qW4wJRhIV7BSLIx9x/+n4Xh8+94tgwENF/NSwIWhPcBvwsq8BGK8VYLtcBMwn112G5dcs3it0wqZt9Ee1K2uJAzLWlJ0QRtTfJZTxm02ithVVPd8B7arRyvMXywo3gm4lVmkG89i8aCpIkc7EFi2E8QNEyCjJ1KoeR+OEwPEMkcORd+KV4cmuDUs86SSxnOzuRffDUp3vLpPeV3gwmbH75T1dbY6Nab8+DGgYDrw7Qt+aG4z4HJtKRn+Ut1tAB15EtJlP4efrpwwyEs41//Lp3cp2mBlyONBVWs3z+i+rnIa8DelOKZA6a7E7CAq7qgVQi6MUC6ZhyeQCfU4wT22c5f3jWjn83KHneU+wx3XpuB21A8DUakP7xZYsVoHRdLP7uu2W1MjzzPmAyLVS/YKL1JaxI9l9gNStYUZ8e6baDiNe6gdhlsI8lTxuEuTQTtZf2wECWbz8QxYsFMn3qJxxiqueq/TO+Ai7LwL92ei0tYmtpLdfK1gnvzSW7Y6zWjvZGplW3YOGZQ0btEJhRCQlv/v7a2UeJT5SyKv08zmaKWzdXhn+1rhY049zB/MmexuaMCxmwtU9JWTM/Li2PVpozqUvyX9sO9ndfQFyfzPBPM2nE6rm5NUp79QWLwRqEUUewPw26CXzL1gfxPH6lf0Ye6cNaZDQvB78ZQC6zmRr/2X0ILgtPtuHW37qji9JfEq2u0szXYOHfihtw0xUJZGbEy/jlvHfQbElkTjdorRFqGDw8pROEan0o2xOM5s/2STMT5OxVJI5tP5xU0eMeJIwMLoXl4InHBFkLtxquUGCtXHh8MbGhzSR8/Qevw5/+80FgLZNpN33pxETDudVdPM1A45R4FjH1F1l3yUULDOe9/52bxOp2RD+V7j1fdpj43XC842T8q0pQQcR3vnkiBmb6hEf7HC3YCwW9lmjWj3KHA5XPTwfoeJB+9qVw4ab/PQ5PlF0W5YbC1BkrnY6ACv6ut9p8Fw+MKs4fXs7Bed1e5K71U7ip3GGLR+/12Adttjr3uyoYj7Y9TlwZNlttUjSEA3/g2K8uKlWV9Jp7xVKhT7hNq9MOG9jlPO+s8kiI3S/GcDZAAet7xB3rnXssBZcwbOqYy0Q3P6NhFutTmp/LOMaIm2UGROMbJMpDbZDdUpxiqSYm7OYzAnX8BXebNZpAwVGUyoqwtQzWuO15uGtXfANdKfoDFfDgZt4A+yWwYJEe5EoHoycU5qndIFUzmh2AZA0VOOWGA5Ihti/x3+KAxxTze61z+51a+P6QRCCOB9MxRsLYPGhUGMDCijhQTXWKjRvp6egIBZS3KPop4F0wK1MBmLg63PsBMtCOWALx9V60cmfXHXJ+88G5lOjRiLyipLsr4dk9O+6I8vhXbiWx5/myvn4yUsA5E+KgVO4xQ/DUvdyFsiSY+QtIzFe9iLMv3DXYD39SFQ4hz2XRq2RldI5aPodmcz6jmhW42FZcVSkgP+uZMZUT3fh3UiYSctqlCCNxUNA3NSSiwKTO0vohRpV/EIeTEz7ukk30aB0n5O0gKg5doNFJ6qZVh/BnPJpxqT9LlIhNzyEz73FEHSRnzeuLw1QHmSyanTRcdSPenvY/49GulkjEfaXvBNxyRxE0Wzrl2An5kf9oDjOwwYmVBRVzcDv5MRbihYceiq1KgYn1Iru468BwBKsZ/JdWeSdzmm1z+64+ouulb5p8UYRE4C0aEOi29zNEiUojHcVwfRKZRZG12fdhia930h88XYx01q7msJOP807wCf8fFsbWt1WoxVD+Cf6H1cOQLg4dcY10Z4wMPvkLhdikzAp4SXlNOzOAxhsNw8x9gU/43X3+EsF5fUnUIy7fCS2B7XzllcPbLYZEyZWq5ODrr+tn/N+TTmmjXtg0NmPVsBebjoNtCnjeZO2xpl2jBTVKk91YT0rRIg3byiEabF27AvwLItrZ8sZ6ZkOlqLOk0eib5XaaC590l641+CTjZKNNF+CpjPmdM4ov48+Aew36fib99M3tzJft8QJp/lxEmZIh8oRZMQ0/5ee5ZTKu86QLvviSo+qvaTaN/heDhTlKl3lA2mwp5x6Tght/IMjQEXwsOMEQft6vPbfxjYnWhpAr4xr7avXir+slAU/CXhwze/Ll2bMZxqo5aNm62mLBKY/FXJgZeux0lIrTwYp81oqggMacXYAf3030wrAI5M1TqAD+lJnvA5o2iGh1Kh2M4qSafxYo5dQifqpOJb4Znk2SPEf/ia0H4HGOr/gmC11wGQa1HJIyrMIYTDM4UXhaFjt31IpbW29f8s87AbWI/HIfCK3aa9CW5EDL+FWEyUYUWm5lCjmkiId0v14I59+k3dlCfCiJdm01oQI96MkjycYbpcLxeg7NvJViQj4Vp/j1xoqcXcdGCg0DMkehLjBSYjB9OWu0tcyNiUqKr6DqpiotxRj0Fo7rTYkQggYzQ3wlT9VtbUOrH3RxxoMxRjLTCx/uQEFoL+VdHxmtASkgArHZvlTBtZrROmNwTAVrs+3qJAO7c9Ps2Ym4zxxmvvxWNfNSAKrrgKlp4DcmTY26rlZmsNjgnLgrB00iff+zS3JXdIyC9FIOMpez43mw2T7O5SbuyGWW7A2FGmc6u6j/4lOGaEDRhkT1HdHEcZQupfH0ljKnQwIsJcrDQudz40kGpB7izWOG/CvMwBB9X4XzJP8X3WH3L39knyOlVipY+dpDbrc/4O9DwQSJaN7DuPGBOQ4TNopLHZ3f6OhWIMk+chXNIOV/yvNKTTS8PMadXj5kNl1/0vLXajZcunJNfgop8rT5Qd6ISQjMO1Rc/uqxCFpLnC3ruGAFPwkPr+SeNhCbQ5vhTXf6OGvrwXtFwpMpyZ/L2mltdMk7RpDKoO/LD4A1H4bYgUrViQd1I5fi7OAaeEQeQZ059x0qiw9Wtox2BFSJcWmxc7pOhzgRLu6TFeo9ps5GPu/7/D6b2Aw6/GqMuNGmcetoaH5DixAWdgjakcO1hD16iO1B0PK4YVuyqsKy8ZsAhcQ7Os2aRMsO/oPJMb0ISwuudffE5u0KRnHDBZxbNcryNykn8gR5bUARgU+HyeE+yCcWaET1H21SVVUW9DK7PwTro4+GT8XS2Lg8W1NqOMB1L5z9Phwlxg/FXyuOX3Nq1jPYqqy3TCNysxNTojVPFEiAoT2hxEc7h2j3cpv+87ETKas8ahvQU7q1cyDe4ZxqwGLZfgo5RhdFj8VrsX0TMEgj86eoGaQGSPIxqhkORvcBJg/tIMMEgGEKSxboRfC+vebH2QuGGs8AOGN7U2y7dh8bwo77LbH1F/8dqe1jIJYlQs4ZQueUH8QEkiygDYmSnszxeAJQgDdn22UO+OK5J2eMhTf3qqQoJEPSx9udLA3ohVc3QT3j8XjQ9Bo2KI5iKPdkDaBdKoqXsjG8Yw5ZjG83ukZfJx9oGB8Xed/89OMGW/1RJNPo0QRpvIcqPVDMlke/RRTYwSdy8YTpgEm/dq60r2+nBirZ36rEd8ITXCvcaQlU46bqS46o3GoFh4e6xihDc3teSIUmv62bK9leYNwy+E8KY8339hchS7Beybt0TIcXemX3uufyYbtfiUpMl4V8cKtBAY+N5L1gf71dGTp2/w59IajA4fZDi+kRcSheV+3/XhYdTItxcFAT2VOaqMb9GqyTj42gccYaMy9yFVgxASW5Zfc/iUmVprvr+IWgrRCrdCQeOinmdQim9sfeIDcOFtTyzhbHrwQ0+Tzuq5ROyELewsKY6k6+fLxBTrAyXsUfh5gSpcokDzdaJcWlRtKEEXWOuihlIIWbRfiz0VXNOTulH4oDINDfuQWgXHq4kZOruFxtRDO0OQGqVWcg0tEeEz0C7LYIzQnSHi9eGuCjN3y7cnor0DQ6mYHu6CyjR+MJd3NM8h/j47/BFYA/KWAnHRGXD0LvHsbdel140FRrwwRRfpRCGDh2bY7tL0iow8sCVS5V1jaUGYo3zIn5tn9pUQ+GI3GFFKaEKndYCVQwPJmfEIf0SNXM7tl7hndaCU11C+3TprvjJKdLzWRj962PPkiovmW2bLhAKPlQkIQAt9cbVdpN2gYzQVHbXGDBVHomZceKPi9zgra0f4JlgMnXmA4yKDOlPzaZod2JZ1NLjya0dyJ/zxQ6AnyiPc2QeaoB3+9fVQjCX5i65zCZ+pwq1UVdmAc2aq3Be6VVbvKnz7iMVF0J7i+Gwjrd4rdxOJ/DSTWGQg4gFXjUoUjhLmhSnI8aVnidtqSGGQB509G7Wn66NAUbtgegNN2DK3OgjiVUZsGPny0tq5KpnUXKA90Kw7U8A6hqBPiXdbsTSnwfe+sYfQ56h45JunLh8hv63LXBWXXfU4jn9nbcLXbaryg5SqlbQDNppH3PLKf7TIPl3js8uRrCNG3Fdw/5IAqAxWHgfKkOm5zyeApEKT8TTKakcwQ7JpP38bBu08JTR5/N9OSf68VM8WMV88Yxq6MOZm9F5VIAIgaxhdZ+W92q1lwxJeQFYCk6EJMFF1fHGbkLh/emloSM6VkbK63bdQ9iHD5BKx0+S0GCGbUDzMoIuaI93UNJDXUoko9jYv2s2Wdi0AwDr04hKB6z4aQlpuh6zJGXitDoThNvJ6FXkQYb8ipHCQ1p9XoCCSsyWtGLCbbzX+rz9Hj4ZL7hqBN3FZje+VWj7S9HuxEdHY/vkE9vsNRTjhLhWOxdRBzjEa5QFbqU7yA0LPp5L9zbFlLeVZgHDqKVQX47DSwXij2It1FJu67wzc3CBYYicdMXtyHSpwPczgQY4GqXIz75dtGFl24qPEGftIX/lVdG9A1qBLPQYtszPkwZBJRhyOK6u6+uyXQ7MXgXu3cQoXILi4WyLuPjeMAdHGdsXYQD/aoZAMZLoKpTuzNAnN17AErMsqnT0vZcG/CkrkuRGGK04olIURF6m8NL5/B1PmVZFy0A/Q9e2J7b5SsqTLb4mikNQj2y0u/rKPVjs6pfQJOsjSIUivpmmh0UmWtmk6F/u2kO74BhruyuDuEj0SWz6JGiSnCPO2FBrV4G5wraqw6mX6R4hDrw28p9O1IRUOsX4EoaX3pKzr9k3geOaqispx80VO7vGU0S2oaxq1tnDc4Vu2IuR3xEINpEMxNuC4I4ZrxWj/xKzfOISq3NnO/Nfyn+2V6Rt+Hn+L5yPDXlgbbHJOCLDCUfSrOFFZdUw/l2p+gUpmbYdnq8TJQ88+8VXq17gLyvFRVNMiNNC0mueHfS2AlAYIE/22ORq4H3C2YrG6g/92X2j5/lWnE+7A4/dCrQ+0V6AFlqb52Tjv/LO2ttYjLizEIRtuMiAc9X0XX/yZMKYXTcR05G+lJ/giwUAwWHkz5oHngZ5vSLKjmyzVRIK0DTqrvMpS+B51Kiaj/rKdKFZb/ECzW9JuOI/H2ENaFTns97Dez9TDElJFs3IvimePbRQYCcG8c0iPieNPdgoUJm4VGtbzqGJpbO1dzHY7yq89EV8t7jQKmaBOhQ5QifvDeqEtJOTRRCnEpDnAt0Ep0o65w3MHVkjZpQXnM++48ADKZ2Lq9K0+PTwhjMhSE/Elzrt6iguLdJZGJrWiTlkh8VmnA8j3AHQQkuaf38T+vW+xblsbzhxhapGOUpaUzZEsT4LrMilWaN6lCy4JY9H+1f+l6WAJPZTCHfk37gwSZFYZ0YDzqnVR3hRPJwjqvqzpe1bedZGSfpneCQtQEqEz77oAwCEk+OkJAoa6HbmaG26rRxObf0ixIDOXeLgmiYNxFHFQ4RrrQZ9CXJRYikeAgZSixmbRyn9c/TH6wmffPkRGjaLsgPVcTTclDbdMQwVNV1lBVwdglIeJJMf6vVxwkW1/weRY2HQwVavG9dFToTe3jPYw4cPwKqAaAqAACvS+Z0nhcGFocrj2KptkYUiFsvj9IajRtEJDWdiB3qg764eMQVtEz+AekGIecGCUZp1iRhkZVT38VZ7DUTAq4gRVNM/2x7mI7cthiqUZzxOO/pja/go7GH6W84jI+gmTsTDAEyBBlP3ahPMarXRo9GUvGnaTgqGboatkynPR1Gr0UUN+YM3JrS5Jp6qblmqrnbmvY+4TmU3rQg1UhFRIIHgOguoJXOJK0KVpZEhFE2BV+4fYAppYtA9etS5Y36Trev7/S3tH5IMnzg01jjGBw4BSYYwhjAgfD133+ZFJKuKcHstV3/tyhzOvJIANKIZ0RT+QN91JfE30Y5TKB33U1h+R+vLZYHhhfjck1VUfQYW3XjDezWfE7hMKyLwWA1AqCkv4qe1R0Gxdt+mBQy0SIsE59uWiGczuwo2Ki3TBM0Ha6mqif2h9FRGr8CVDW0IDkbk2BV74YisREBrNRlasL7YpLQFaTzAmIh/+0bpOERRB8STLbibx+g7jUrsPbQ/2hctG4o5qb/bfvy7/SIDrLf/AG+YZucG6kZlr7ALmYcWKUH3gQuaQhHjw3Scq6K5PSlxG9IYqP7jHxRUHH/CvbpuFCli0n4HkeVTajMyoQXNahGAkuoLEgD/6dffsAfHQwM/zyvOZIAysMZPri7GM8jSk0RBb2d6ENR7lOPXxcQlC6i1/oiO9KZiI6BZ7Izb/61Z/CiNJUBma+hIYAkxXG5qNV29UVl8w96eCLIp4TYUkBmmM07YxaMiyOL2W2HxPFkHnb1gsvcs0n0OvuEMEE4Pjh7qSO9cRcaJnpZPfOAa0o3c9sODUj5zlwL61u8QlDlopXM9aYNPdSmILWj6C2mM+sLOcW8r57azphP0Z039ZuObKyWDXzsTj33MpEhTAXV+56qiI2K9B6s9r2h4mxf9WwaEjIeNSjbVqG2ZMshRnfRbOwwird10jI9aqmRwj86Hdjveen9j2Z62Rkg33vMiWrD3+irN437nKJUlWZiUjFCCQ61vlPxRMJ0HA/tSz/1p8+VN/vsf7TloyGyi3VtoteTj5HeF+ucU36vwDnRpeqxuu+t9NMURo2Swjh0/CdojrOZ8m8nt7pQRv4jndL87K66A+22ll9T1STKzasLTC3bhMMr6nabyd03Kh8XOGVC+muY+IvNWJzg7IsdkNOi/KLcdQHKKIfwc5INH2snx00aIKs4e7T9dcfKmeW3WPxLLMIt7RwcwWZeh36RnvBGO+lM1q0eMnLu/2LfP8vHoyIqJghWloifeI+vxQpiP1QOosEVzJpkiDUOFdjk1EZnJRx7AfQ5fKMRd6DT9xUDMPPBkJ1u2etmNzulDMPvfPu5XCln0HLE1TyZyHgCHG9hDbP3NPf6oONvOjxWCqc2R4ZLVaMZ8JoIEZTllIRTh2m8E1uRrBmHlPcdfqZBh1g2dOOqo2ND36AqBYfKLgdSFbk44yQiBNRfvjMnyq/jPMVVdDDZIiaOQMM1LDsswqenfLAqozWAFC9CyOIc3oQsN4f6RnHzlk49igSEpjHuzdWYAe6hE24ykGwwLabbEZ5PGfwVWkdg2f7qD/Z8YcX1WExG8hF/XZeVcd0qY2JbV2FiRL7Nys6OPUXnMp0aNBedLuu/GKjNLBwtmHwRttK4KS8BUpf06Q+8KntkakUUFakiNpWnA/A8eHTT5vx+rP1cOku6ugay4IcyQxE4CGa26ZMT19YItr1EIKaV06REzzCgu5hCiulyrgPe7TbgVodI45RJTvg8RT+f7VVopmuJF0q6/U5r+y/LfI8smWSQP0EmVach1ZS5XR8M10FxBdOdRX9/6OCKd1IQuxcdv7G0XNtLN1QIqL+SM3yD4qLFhJHDcvgFg53rVKPOj1yKIidfPzwGMn5wmzDZgoSJ62jAf4jwxlLD+5am2v8vjVSNrqOPbHb6z7T7JLPXDvJnFyQoP+M+Yr7rmDSitOTIXQutq/S2/tT0rcwqbQk0mAqnk2ZDlhPITDZrD0XeruWZISiemrk8NafsRH0X9uuerZ1cEq3y5PXK9vZU3+OJflv889M3N4ECy3wOrMGkTx2Dn1vDYzGmSnOtmw/5cespZwbDG5VYpixqA4Vn1qxpvoS5vJIOdd1QcUNKEvNbxASY4Bv0KlYfIEetVA+lmQMEZdbwMkMaBcl3ptVGFBeHr9510ATQB4ezCxrPem1r0JbBBMzYhruJQMI8dz0wYhhSmNMMW8itjKbZRjnz7jjVEOHFqFOZr4zXKJ9DzcmL4RrVpkZng+VzSKin/sh4nYVNMrNdGBr3lAuqERuEtgrOU5MF/O9RenovtoSs1RLEpcbLGvsfxmxXpfgg2QUKt5YC0WNCVZfVGeQkDbO66XtgDmTn6d4xRkv8vjsqQtfJPovG9BhWc2rVOHrRxvyhSS6I5qleHSl0Djdz+IV3Sv0PCRZ15+2p7yPuaLkNRRzYNbtYx8Y/IAFsC30HQbamVN0H1zzm+g8F/OYjf9C0avKK5E9u5a7v3XmDN03VDBFYXv7bDKOSR6S87gUsp2s+GEvCqSItRm6KAw59Fz2mg6dns95fXA3DqmE0VksdOje/HTj5kXBdZLu49aowdcgJkI6mihq9GWo+dHqH9/HduK4BqGvnGST0CRzcDik0d+KHsDTFsAmTHxdkYInYE3TclUlqNiBgoN9cUDLSvDXe3LVxlBXedQY3u7wkf1csIXByRkbgnmhX78s75WKdXWF9lwOYUb1LlhVPhCyKlpBsooROcgjHmPZfdX3tFSSztvxE+XOb39sCENL0uBPvuiqC1ZFpryfdIYMyWViD911IAj1AkLUl27mP9ySGFlOAGZtgq6GbkxQ+1KT3Z1oNPEXfYqYGkKHCDit9AU4Y3lR/M9wGJeT6FkBYTMHsGezeIhhRipvJlRQjSn68g8CG/qL0xdKXTAKV6c/j0EmpQR8h2GhTgz1QZo/MbmP4iYY4g/xGR5Qf6i/88fuRl2mtrIZBPrfIlYiuA9TT0huATB68ad8HCu5EJnXtuRQEm9MxTKbjk+vlalhJe7M945RgR22evEGK9NuXoY4NWz60007EfuTMfQxUGPiZqxvUDX+drwWaWmIPI6HW6cSDL5rJKa9ETPV5Erc0B7iiR534Wpqp4U0Fup1V9i4cpbzlmSa8NuNn9RnndpsZoScpQjva0xERi9vHzs7HT5sqhQ2KozjRbmxhQc+6425pGBztQesNMQg/H2VsjRRr1LlocJgtM99rS45sMJf8DOFmKE7v0dZG2x9CDP5xsaUtseHTQKtDfHdMzjxWfbHRorKYNqe65KCi9tK9VnPv5VaN7i3iypFj7OJjTPpzTiH2UCAtYSe/DxhBGl4EGeALmPmzO2nU4ZyhRqJgXR1N5ZDiGPzb4A1dk5CnayeuvKJScc2u2nBKhl9iij6XHMihvqqucbGOUn6goBTSXDE/mKe4jCQFp+2PBbNy9LH+l4svjIbCwadnXMjk4n5lwDe5229i7U6X6HhW+vWhjgZCP0LtNxLF+fo4/HQGGmJUg3A2VwP4bL9WCy1BnbZKT/dK+3CaJRfyexo0ku+vYkHb4DVyM0r0YLxdhJ34LjrH0CTxzWES1aiEOiuXngW1pxrAl04i2fh39lKL8aZs/mJi9dURfKuPnd+3qhEHrBzJEhLFqlRwHB7DIQDJrLKmmSFhT6l0K1a6qaFCce0qZMMvVI4PdSC2xpqIf4acUS7nogTEF0Khbhv5aHS3bnx2i9T4D46x1CmHDS7QPbVbJ1fRp4/8p9cNYsBOPa7RmZ2QjEdB1jekt2G/o2LUUezFbSEnsBoe3I644oJ3qJFPgcaiehVdgfnUnoePk2JBwQ6nyQVRPyvpz2/JY0wa9BlUoOkDB+srV+0sUh54K4Jx4hQ0VumLu71ebqosF3VEskIVRjZzCTRj74dwhIIxi8Wl5jmeO0PFkDEjiLhymQe8jX+P3+JgfM1Ls2k0Iopz54PKv+f6khC29JJ4sYflZRWfhH8XXgkrbptWEFh8SugmQcvUfpc217x+LeB/088dCS6kcZ6GNuSKcLUZu3d1Fb5DQkjeV84FTt96+6RysgaMgoXxPRDp9ymzfXlT+Ff/DNmXWV/vwFMyCVXgf80MnRPQNBYA3ZfPDRUHTTG6j/XaaEqkVVa6yyQxZUwlxARaPbRESvsMShoYp1EP9s0eA7XFxw00INVKBRz1mfVvJQu/4sHrZlHvH8lIEnOdWijm2U2o6rfLhTuNqWKIAnTb+2RaKQw+wX80fyo0n5B1rVXtwpQ2M7ICk5KumSAiVVbwsXTl1m8cIXRTt+pkV6GkdSLIevgs7dySRb9OyCOC9G7DqCnaKkzdNj6DgClj731dt6SNPrrJGREsUqr/wFo7ayR/umCsrl+QFrhUHurVfElpF7mw9KinVzrS4UNvqMdait1SuqhfjF8FFVomBu99TfylrVpZn+qYrJ3iTSWOPlVziXf94TP4z+pn0EBd1LUR0/wjE5ehOtrsUBGkYXcoVtsqGV41XQwaC76eJ1kgcj88u0uyEwmuPxYwR6LA5f2POdEuUMuntbpYxTBtZq2PDCMow9FrjrwWmXL5MqU9eRUPE9DOd9lIXygcS2fq3zHiiFd9PXzNMaH5mq5x95YtWCKuE8eW+SyosB9euMaxl3Gpc7Wvmuj5d7y5Ugq/QfsjRcsoFgfYvdEg47tG4BYxh9mUqV7GT4jDstErTDTpIXyqzvCFw0J/is0nklKvGOxDN07Iz3bRHrlDBRSDHZ3Dfzc++ReK+eFIK9vnUHfzkb5r2I0rV00C4jV9htW4/9dNut64x9RAV+L06YUoAZWkVhZa466WoFuviO7Koc28nwZEK9p7yJeX6AqKp34ZcXeImd4Pv3FQfbwAWfRGCYknNIGPb1xK5jMLI8h43kS+Gmn+40ss/0LMZfMyYWUd4kgxJXu/IPK7eZliJaqVUwogrjp9S62u5Lrg8iIPsPIYhe2s9kiYS/V6mgE0jPkClM9L3PW+e3nT0wgdBwb60qgw/AbkrLzwi6lqx0uEcgKpshVmHsIMHKWWRrVC/2cndUUBMNpLv+33+umrPt5Ksb5UUShCOU/R5Szqk89W2YofZ/9rVC9NcTvEtPQ8bW9mOrJ7ZtIrR37QFJ4kAi9m+EBxc2RC98/qKDMPusIbOCoT32hbkdqqQK1mwRLdJtXZiHntZw/OTZZHp+oFBwIXMZg0hsTAeKI7f04zo3SHnOuZBleNN05MlGwp6BZuau6MJ8InSEgrQw2tDs2NGkN9iLqmeguz4u5X6UbsKn9QuhA9QMjJMXBKsdR6w41HpAwVm9FuV3ga3zGzdvETags2Bg+G2nW+/LiUSHT9TVueBv1wDdLqcyqs8gbTjIw49wenl1kTWLTJJBW9gGIhjtIuGCIQudqkqVpVyJiLxlmp4jSF8n6VgVMwZ34d5IB/b+qv3X1Edk+MdzmpSyYkJ11ItLTz1Nv/e9qP/v9iQLMcHJrNPSDA4VCDlQPQRoP2oqp18v32k02qZfYTX5CtGn80Xt3B2ftRkKb9YwFIo2zkXReLb3tOB9xSWtC4IpaTZaLnWfl1PlCBvhINqfowoiog5XSaXBqvkTv1fXuQ1jVMbE6BVDDn0YAjkvEf8FctyfYJVtyJL3bC5P23MlbcmPOuJZHP8+vTRgTZjTJhk03ItMG5D1uWe0NUuExRPvu/fNM1xq40lt9XjGWdKROPMxK1D37VvbDLHnBBv7IG3PPnGm5E3SU8zSDBCGw8dPySQWCcVmczVaElLnjoDU/eodaJr++qQqYItpE6jAe42D6Ybd7UnNIBkOZ+IuKLqdX4oC5ylHHQw4h24TOCJSKVxWcg3XZFfly3UMg2wwcCP2kDDximH0Av6Qyz7YPiYhUsPcV3xRI4IrIE9bGBhBjPfo2F2TjsK19HxX/j89oiwp321RQhJZbqNYTKFMQi8/1Q6ljZfhjp3iwdfkexPcUasJOHuAkZm+y6UnxtbZJ3Rim9mBTO7DtewlZu6BtaDPWHaaKMM4RQRjxeNxkq+TtDTyKgV5ACNMkyVNAzbIrkjd/u9woT5WSZntWPIUm0VnZ92efV48T1UkE8kd16keIQRcHbUJMW92NMbLuNhGBhlZUFXN5QsWhSRgOmKvtba6zFzd9cnjAPL5mvyq+RKByrxxICxmQspPGAVU0M7BMfN/ahLDTqSW3Z0oJJ6RvgcrlBo3+5OHxti17xrbgImnBSQ1/C6QHEfcyeGFqdKiqwog/Y/KR3Y1ZcBgcFUfB8X3+1G9ks9TEj2TzzkD84TP41PfDiCH3IwDTJJq5m8ykVs0W0+dvax4OZFocrDkj2BYEaWBp+LNXoqTv6KZje1TcKHQa0yIEs+iwK5JzD7dvYuT7az+L4iYYvPZhGT5qR3UQaRkMcimlU3n1LpMIaMiza6rBL3zbW0Zxe60q5iN7hHREcBOdST0eNEQxSNXcFRg3T8Z8hVzlNN0DvcWIQXR9TZYmBjj+Tt+3Wsp18J1raxdiunWBgdtsjCTxsYHBp3GMBPy7r7/+sEUPsA1zCsZomJDIkBjmzBNtT5w7Rn0jeATo5fWkzzqZ" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['frmMaster'];
if (!theForm) {
    theForm = document.frmMaster;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="http://ajax.aspnetcdn.com/ajax/4.5.1/1/WebForms.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
window.WebForm_PostBackOptions||document.write('<script type="text/javascript" src="/WebResource.axd?d=pynGkmcFUV13He1Qd6_TZEmSB8xlmqwJmuP1tvlCAr5eSizVWdB4I2UwGRKQhzIUJ312Ru2f72W8B3K0dHQZeA2&amp;t=635586452020000000"><\/script>');//]]>
</script>



<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script type="text/javascript">(window.jQuery)||document.write('<script src="/Integrations/JQuery/jquery-1.11.3.min.js" type="text/javascript"><\/script>');</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.migrate/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script type="text/javascript">(!!window.jQuery.migrateWarnings)||document.write('<script src="/Integrations/JQuery/jquery-migrate-1.2.1.min.js" type="text/javascript"><\/script>');</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">(!!window.jQuery.ui && !!window.jQuery.ui.version)||document.write('<script src="/Integrations/JQuery/jquery-ui-1.11.4.min.js" type="text/javascript"><\/script>');</script>
<script src="/Integrations/Centralpoint/Resources/Controls/Page.js?v8.5.1" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

 //Design > Styles:  (#ReDesign) Scripts 
        $(document).ready(function() {


if ($(window).width() < 1000) {
      $( ".left-rail-nav-container" ).insertAfter( ".main-content" );
      $( ".module-results-intro-paragraph" ).insertBefore( ".details-view-left-column" );
      $(".inner-container ul:first-of-type").click(function(){
      $(".inner-container > ul:first-child > li > ul").slideToggle("slow");
      $( "#search-expanded-container" ).insertAfter( "#mobile-navigation-container" );    
    });

}
            
          });






 //End of Design > Styles: Scripts 
//]]>
</script>

<script src="/Integrations/Centralpoint/Resources/Page/SwfObject.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/Plugins/jquery.cp_Accordion.js?update=8.4.38" type="text/javascript"></script>
<script src="http://ajax.aspnetcdn.com/ajax/4.5.1/1/MicrosoftAjax.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(window.Sys && Sys._Application && Sys.Observer)||document.write('<script type="text/javascript" src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yKL3veK4_1o6aC-AgxFwMcMVIIRV9FSpZJuOVk-LAIAVsBBLL3O32w_IrtC9GX2Hh4_Jux5cIno3a_NfWjNJey3pHIYsP0dX-mgG1YhQAMEmmQ4K4v3GbkuLsq21ZvMc7MLyHc4nUSN_UV69kEqz7hA1&t=7b689585"><\/script>');//]]>
</script>

<script src="http://ajax.aspnetcdn.com/ajax/4.5.1/1/MicrosoftAjaxWebForms.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(window.Sys && Sys.WebForms)||document.write('<script type="text/javascript" src="/ScriptResource.axd?d=Jw6tUGWnA15YEa3ai3FadOTe6hcgWgzpfugVUEIQdVvyvVnvBI2PTyj7_7WwNZZeLrPeSiI5xwupnXQDClTKZOByFssxNwH3zyC89KvnDkK9mc0v1RE1H6GLADS8YJelwlfMb6y4XJa4J192U8GeQfRN-m_X-sCPHKOVpUDVCxk1&t=7b689585"><\/script>');//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="43343198" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="yB15p6/JypuTdczEVkNquKzNEcX4do7MUqM7RibVeLiqNoyjwaROG2FBOsDfa4QZcvUeEtGQ5941OT3OqihAyT+GTRD2NgGxp0u9vyBuLs2lJMXh46ltreighbo/WtMYG3HphpLzIU8nqvKUxU25bDabKIQUXTWK7IVHiMAfsF8=" />
</div>
	<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$smScriptManager', 'frmMaster', [], [], [], 90, 'ctl00$ctl00');
//]]>
</script>

	<script> 
$(document).ready(function(){
    $("#discover-expand").click(function(){
        $("#discover-expanded-container").slideToggle("slow");
    });
    $("#search-expand").click(function(){
        $("#search-expanded-container").slideToggle("slow");
    });
    $("#mobile-navigation-expand").click(function(){
        $("#mobile-navigation-container").slideToggle("slow");
    });
    $("#mobile-search-expand").click(function(){
       $("#search-expanded-container" ).insertAfter( "#mobile-navigation-container" );  
        $("#search-expanded-container").slideToggle("slow");
    });

});
</script>

<style>
</style>
<header>
<!-------Top Nav------>
<nav class="top">
<div class="inner-container" id="blue-navigaiton-bar">
<ul>
    <li><a href="/Main/mclaren.aspx">About Us</a></li>
    <li><a href="/Main/healthcare-professionals.aspx">Our Healthcare Professionals</a></li>
    <li><a href="/main/patient-appointment.aspx">Book an Appointment</a></li>
    <li><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a id="discover-expand" class="discover-mclaren-health-button" href="#">Discover McLaren Health</a></li>
    <li><img style="margin-left:13px; margin-top:13px;" src="/Uploads/Public/Images/Designs/nav-search-button.png" id="search-expand"></li>
</ul>
<div class="clear"></div>
</div>
<!------------Search Box-------------->
<div id="search-expanded-container" style="display:none;">
<div class="inner-container">
<div class="Search-Input"><input style="" onkeypress="if (event.keyCode == '13') { document.location.href='/Main/Search.aspx?search=' + this.value; return false; }" placeholder="I'm looking for..." name="HtmlSearchCriteria" type="text"></div>
<div class="Search-Button"><input style="vertical-align: middle;" onclick="document.location.href='/Main/Search.aspx?search=' + document.forms[0].HtmlSearchCriteria.value;return false;" name="HtmlSearchGo" value="Go" src="/Uploads/Public/Images/Designs/nav-search-button.png" type="image"></div>
</div>
</div>
<!------------Search Box-------------->
<!------------Discover Box-------------->
<div id="discover-expanded-container" style="display:none;">
<div class="inner-container">
<ul class="discover-list">
    <li class="header">McLaren Services</li>
    <li><a href="/Main/bariatric-services.aspx">Bariatrics</a></li>
    <li><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li><a href="/Main/imaging-services.aspx">Diagnostic Imaging</a></li>
    <li><a href="/Main/birth-center-services.aspx">Family Birth Place</a></li>
    <li><a href="/Main/home-care.aspx">Home Care</a></li>
    <li><a href="/Main/hospice-services.aspx">Hospice</a></li>
    <li><a href="/Main/laboratory-services.aspx">Lab</a></li>
    <li><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li>
    <li><a href="/Main/pharmacy-services.aspx">Pharmacy</a></li>
    <li><a href="/Main/proton-therapy.aspx">Proton Therapy</a></li>
    <li><a href="/Main/robotic-services.aspx">Robotic Surgery</a></li>
    <li><a href="/Main/surgery-services.aspx">Surgical Services</a></li>
    <li><a href="/Main/womens-services.aspx">Women’s Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">Primary Care &amp; Specialists</li>
    <li><a href="/Main/locations.aspx?taxonomy=Cardiology7">Cardiology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=FamilyMedicine4">Family Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=InternalMedicine8">Internal Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=MedicalCenters">Medical Centers</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Pediatrics11">Pediatrics</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Surgeons">Surgeons</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=UrgentCare9">Urgent Care</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Urology10">Urology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=WomensServices2">Women's Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">McLaren Hospitals</li>
    <li><a href="/BayRegion/BayRegion.aspx">McLaren Bay Region</a></li>
    <li><a href="/bayspecialcare/BaySpecialCare.aspx">McLaren Bay Special Care</a></li>
    <li><a href="/CentralMichigan/CentralMichigan.aspx">McLaren Central Michigan</a></li>
    <li><a href="/Flint/Flint.aspx">McLaren Flint</a></li>
    <li><a href="/Lansing/Lansing.aspx">McLaren Greater Lansing</a></li>
    <li><a href="/LapeerRegion/LapeerRegion.aspx">McLaren Lapeer Region</a></li>
    <li><a href="/macomb/macomb.aspx">McLaren Macomb</a></li>
    <li><a href="/northernmichigan/northernmichigan.aspx">McLaren Northern Michigan</a></li>
    <li><a href="/Oakland/Oakland.aspx">McLaren Oakland</a></li>
    <li><a href="/orthopedichospital/OrthopedicHospital.aspx">McLaren Orthopedic Hospital</a></li>
    <li><a href="/porthuron/porthuron.aspx">McLaren Port Huron</a></li>
</ul>
<ul class="discover-list" style="margin-right:0px;">
    <li class="header">McLaren Difference</li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a href="/mhp/mhp.aspx">Health Advantage</a></li>
    <li><a href="http://www.karmanos.org" target="_blank">Karmanos Cancer Institute</a></li>
    <li><a href="/Clarkston/Clarkston.aspx">McLaren Clarkston</a></li>
    <li><a href="/mhp/mhp.aspx">McLaren Health Plan</a></li>
    <li><a href="/Main/home-care.aspx">McLaren Homecare Group</a></li>
    <li><a href="/Main/laboratory-services.aspx">McLaren Medical Laboratory</a></li>
    <li><a href="/Main/pharmacy-services.aspx">McLaren Pharmacy</a></li>
    <li><a href="/MPHO/MPHO.aspx">McLaren Physician Partners</a></li>
    <li><a href="/Main/foundation.aspx">Our Foundations</a></li>
</ul>
<br style="clear:both;">
</div>
</div>
<!------------Discover Box-------------->
</nav>
<!-------Top Nav------>
<!-------Logo Row------>
<div class="inner-container">
<div class="logo"><a href="/Main/Home.aspx"><img src="/Uploads/Public/Images/header.jpg" alt=""/></a></div>
<div class="mobile-accordion-icons"><img style="margin-right:8px;" src="/Uploads/Public/Images/Designs/mobile-search-button.jpg" id="mobile-search-expand"><img style="" src="/Uploads/Public/Images/Designs/mobile-nav-button.jpg" id="mobile-navigation-expand"></div>
<div class="second-nav">
<ul>
    <table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation73&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation73&#39;).hide();" id="cpPortableNavigation76a224fb-2841-4c3b-b9bc-237b847fccbe">
			<li id="services-nav-item" class="second-nav-services"><a href="#" onclick="return false;">Services</a></li>
		</div><div id="cpPortableNavigation73" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;width:600px;overflow:hidden;">
			<style>
    .featured-service-alpha .grey {color: #cccccc !important; font-weight:normal !important;}
    .featured-service-links{height:250px; width:750px;}
    .featured-service-links a{font-size:13px; color:#5f6062; padding:0px; margin:0px; font-family:'Alegreya Sans', sans-serif; text-decoration:none;}
    .featured-service-links a:hover{text-decoration:underline;}
    .featured-service-links p{margin:0px; padding:0px; line-height:20px;}
    .featured-service-alpha a:hover{text-decoration:underline;}
    .all-services-p{display:block !important;}
</style>
<div class="services-drop-down">
<div class="services-left-column">
<h5 style="padding-left:31px;">Services of Excellence</h5>
<!----Services of Excellence DataSource-----><ul>
    <li class="services-of-excellence"><a href="/Main/wound-care-services.aspx">Wound Care</a></li>
    <li class="services-of-excellence"><a href="/Main/Bariatrics.aspx">Bariatric Services</a></li>
    <li class="services-of-excellence"><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li class="services-of-excellence"><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li class="services-of-excellence"><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li></ul>
<!---img style="" src="/Uploads/Public/Images/Designs/Main/services-drop-down-image.png"--->
</div>
<div class="services-right-column">
<h5>Featured Services</h5>
<!----------------------->
<div class="featured-service-links">
<div class="featured-services-all">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/acute-care.aspx">Acute Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/proton-therapy.aspx">Proton Therapy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/home-care.aspx">Home Care</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/hospice-services.aspx">Hospice</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/blood-services.aspx">Blood Conservation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/imaging-services.aspx">Imaging</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/robotic-services.aspx">Robotics</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/breast-care.aspx">Breast Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/senior-services.aspx">Senior Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cancer-care.aspx">Cancer Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infusion-services.aspx">Infusion</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/sleep-services.aspx">Sleep</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cardiac-care.aspx">Cardiology</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/laboratory-services.aspx">Lab</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/spine-services.aspx">Spine</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/stroke-services.aspx">Stroke</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/dialysis-services.aspx">Dialysis</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/surgery-services.aspx">Surgery </a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/emergency-services.aspx">Emergency Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/trauma-services.aspx">Trauma</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ems-services.aspx">EMS</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urgent-care.aspx">Urgent Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/eye-care.aspx">Eye Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pain-center-services.aspx">Pain Center</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urology-services.aspx">Urology</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pediatric-services.aspx">Pediatrics</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/womens-services.aspx">Women's Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/fitness-and-health.aspx">Fitness</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/foundation.aspx">Foundation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/podiatry-services.aspx">Podiatry</a></p></div></td>
		<td style="vertical-align:top;" width="34%"></td>
	</tr>
</table>
</div>
<p><a href="/Main/acute-care.aspx">Acute Care</a></p><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p><p><a href="/Main/fitness-and-health.aspx">Fitness</a></p><p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/home-care.aspx">Home Care</a></p><p><a href="/Main/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging</a></p><p><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p><p><a href="/Main/infusion-services.aspx">Infusion</a></p><p><a href="/Main/laboratory-services.aspx">Lab</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatrics</a></p><p><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/podiatry-services.aspx">Podiatry</a></p><p><a href="/Main/proton-therapy.aspx">Proton Therapy</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotics</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep</a></p><p><a href="/Main/spine-services.aspx">Spine</a></p><p><a href="/Main/stroke-services.aspx">Stroke</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p><p><a href="/Main/surgery-services.aspx">Surgery </a></p><p><a href="/Main/trauma-services.aspx">Trauma</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/womens-services.aspx">Women's Services</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p>
</div>
<div class="featured-service-alpha">
<a class="service-alpha-all" style="color:#0066a4 !important;" href="#">ALL</a>
<a class="service-alpha" href="#">A</a>
<a class="service-alpha" href="#">B</a>
<a class="service-alpha" href="#">C</a>
<a class="service-alpha" href="#">D</a>
<a class="service-alpha" href="#">E</a>
<a class="service-alpha" href="#">F</a>
<a class="service-alpha" href="#">G</a>
<a class="service-alpha" href="#">H</a>
<a class="service-alpha" href="#">I</a>
<a class="service-alpha" href="#">J</a>
<a class="service-alpha" href="#">K</a>
<a class="service-alpha" href="#">L</a>
<a class="service-alpha" href="#">M</a>
<a class="service-alpha" href="#">N</a>
<a class="service-alpha" href="#">O</a>
<a class="service-alpha" href="#">P</a>
<a class="service-alpha" href="#">Q</a>
<a class="service-alpha" href="#">R</a>
<a class="service-alpha" href="#">S</a>
<a class="service-alpha" href="#">T</a>
<a class="service-alpha" href="#">U</a>
<a class="service-alpha" href="#">V</a>
<a class="service-alpha" href="#">W</a>
<a class="service-alpha" href="#">X</a>
<a class="service-alpha" href="#">Y</a>
<a class="service-alpha" href="#">Z</a>
</div>
<!------------------------------>
</div>
<br style="clear:both;">
</div>
<script>

$(".service-alpha").click(function(){
    $(".featured-services-all").hide();
});
$(".service-alpha-all").click(function(){
    $(".featured-services-all").show();
});

$(document).ready(function() {
    function filterResults(letter){
        $('.featured-service-links p').hide();
        $('.featured-service-links p').filter(function() {
            return $(this).text().charAt(0).toUpperCase() === letter;
        }).show();
    };
    filterResults('B');
    $('a').on('click',function(){
        var letter = $(this).text();            
        filterResults(letter);        
    });
});

var $p =  $('.featured-service-links p')
$('a').addClass(function(){
    var s = this.textContent;
    return $p.filter(function(){
       return this.textContent.charAt(0) === s
           }).length ? '' : 'grey';
})

</script>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigationc1fdecc8-73a4-478e-b7f9-da0884005b89">
			<li class="find-a-physician"><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation85&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation85&#39;).hide();" id="cpPortableNavigation5bdbf3e4-64dc-413f-ab0c-dc4179e9a3e4">
			<li class="patient-and-visitor-info" id="patient-and-financial-info"><a href="#">Patient &amp; Financial Info</a></li>
<style>
    #patient-and-financial-info{width:120px !important;}
</style>
		</div><div id="cpPortableNavigation85" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/Main/Patient.aspx" target="_self">Patient Information</a><ul><li class="featured-service-style"><a href="/Main/Patient-Opt-Out.aspx" target="_self">Fundraising Opt-Out</a></li><li class="featured-service-style"><a href="/Main/Patient-MyMcLaren.aspx" target="_self">MyMcLaren Chart</a></li><li class="featured-service-style"><a href="/Main/Patient-Notice-Privacy.aspx" target="_self">Notice of Privacy Practices</a></li><li class="featured-service-style"><a href="/Main/Patient-Donation.aspx" target="_blank">Online Donation</a></li><li class="featured-service-style"><a href="/Main/Patient-Pre-Registration.aspx" target="_self">Patient Pre-Registration</a></li><li class="featured-service-style"><a href="/Main/patient-appointment.aspx" target="_self">Schedule Appointments</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/Main/Financial.aspx" target="_self">Financial Information</a><ul><li class="featured-service-style"><a href="/Main/Financial-Billing1.aspx" target="_self"> Pay Your Bill Online</a></li><li class="featured-service-style"><a href="/Main/Financial-Insurance.aspx" target="_self">Insurance Information</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">

</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation77&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation77&#39;).hide();" id="cpPortableNavigationbf4fa1a8-3c75-4ea8-97c5-67f9504e6f30">
			<li class="health-and-wellness"><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
		</div><div id="cpPortableNavigation77" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/Main/health-your-health.aspx" target="_self">Your Health</a><ul><li class="featured-service-style"><a href="/Main/healht-clinical-trials.aspx" target="_self">Clinical Trials</a></li><li class="featured-service-style"><a href="/Main/Health-Community-Needs.aspx" target="_self">Community Health Needs</a></li><li class="featured-service-style"><a href="/Main/health-mymclaren-chart.aspx" target="_self">My McLaren Chart</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/Main/health-classes-programs.aspx" target="_self">Classes & Programs</a><ul><li class="featured-service-style"><a href="/Main/Health-Classes.aspx" target="_self">Classes and Events</a></li><li class="featured-service-style"><a href="/Main/Health-Support-Groups.aspx" target="_self">Support Groups</a></li></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigation52587aa1-b090-4537-9c98-40261a490ab3">
			<li class="our-facilities"><a href="/Main/locations.aspx?taxonomy=hospitals">Our Facilities</a></li>
		</div></td>
	</tr>
</table>
</ul>
</div>
<div style="clear:both;"></div>
</div>
<!-------Logo Row------>
</header>
<div id="mobile-navigation-container" style="display:none;">
<ul class="mobile-menu">
    <li>
    <div class="mobile-navigation-accordion">Services</div>
    <div class="mobile-navigation-content"><p><a href="/Main/acute-care.aspx">Acute Care</a></p><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p><p><a href="/Main/fitness-and-health.aspx">Fitness</a></p><p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/home-care.aspx">Home Care</a></p><p><a href="/Main/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging</a></p><p><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p><p><a href="/Main/infusion-services.aspx">Infusion</a></p><p><a href="/Main/laboratory-services.aspx">Lab</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatrics</a></p><p><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/podiatry-services.aspx">Podiatry</a></p><p><a href="/Main/proton-therapy.aspx">Proton Therapy</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotics</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep</a></p><p><a href="/Main/spine-services.aspx">Spine</a></p><p><a href="/Main/stroke-services.aspx">Stroke</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p><p><a href="/Main/surgery-services.aspx">Surgery </a></p><p><a href="/Main/trauma-services.aspx">Trauma</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/womens-services.aspx">Women's Services</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div>
    </li>
    <li><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
    <li>
    <div class="mobile-navigation-accordion">Patient &amp; Visitor Info</div>
    <div class="mobile-navigation-content">
    <p><a href="/Main/patient.aspx">Patient Information</a></p>
    <p><a href="/Main/Financial.aspx">Financial Information</a></p>
    <p class="mobile-navigation-link-visitor"><a href="/Main/Visitor.aspx">Visitor Information</a></p>
    </div>
    </li>
    <li><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
    <li><a href="/Main/locations.aspx?search=executesearch">Our Facilities</a></li>
    <li><a href="/Main/foundation.aspx">Ways To Give</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren.aspx">About Us</a></li>
    <li class="top-nav-mobile"><a href="/Main/healthcare-professionals.aspx">Our Healthcare Professionals</a></li>
    <li class="top-nav-mobile" ><a href="/main/patient-appointment.aspx">Book an Appointment</a></li>
    <li class="top-nav-mobile"><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li class="top-nav-mobile"><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li class="top-nav-mobile"><a href="/Main/career.aspx">Careers</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
</ul>
</div>
	
	<div class="cpweb_PerimeterMiddle">
		<div id="blPerimiter" class="cpsys_Block">
	
			
			<div id="tdPerimeterCenter" class="cpsys_BlockColumn">
		<div id="divWrapper" class="cpweb_Wrapper">
	<div id="cphBody_divTop" class="cpsty_Top">
		
		
		<div id="cphBody_divTopAc2" class="cpsty_SiteTypes_Default_TopAc2"><div id="cpsys_Advertisers_af343aee-a245-4e1d-9da4-6d08bbbd30c5" style="text-align:left;">
	<div class="breadcrumb-trail"><div class="Breadcrumb"><span><a href="/Main/Home.aspx" target="_self">Home</a> &gt; </span><span><span>Website Privacy Policy</span></span></div></div>
</div></div>
	</div>
	<div style="clear:both;">
		<div id="cphBody_blSiteType" class="cpsys_Block cpsty_blSiteType">
			
			<div id="cphBody_tdLeft" class="cpsys_BlockColumn cpsty_LeftTd">
				
				<div class="cpsty_Left">
					
					<div id="cphBody_divLeftNav" class="cpsty_SiteTypes_Default_LeftNav"></div>
					
					
				</div>
			
			</div>
			
			<div id="cphBody_tdCenter" class="cpsys_BlockColumn cpsty_CenterTd" style="width: 98%;">
				
				<div id="cphBody_divCenter" class="cpsty_Center">
					<div id="cphBody_divCenterAc1" class="cpsty_SiteTypes_Default_CenterAc1"><div id="cpsys_Advertisers_bf3115e5-4271-4bad-91ed-a4aa140e412a" style="text-align:left;">
	<style>
    #data-source-created{display:none;}
    #service-line-datasource{display:none;}
</style>
<div class="left-rail-nav-container">
<ul parentsystemtype="FirstTierList" id="site-nav"><li class="liParent"><a href="/Main/McLaren.aspx" target="_self" class="d173dbee-de0f-4132-ba5e-76a09f076348">About Us</a><ul><li class="liParent"><a href="/Main/McLaren-Awards.aspx" target="_self" class="64b1e7ee-84b2-4587-afe5-5d74ca900868">Award Winning Care</a></li><li class="liParent"><a href="/Main/McLaren-Community-Assessment.aspx" target="_self" class="f0856316-de0f-442c-8207-78ea6ee5b5ce">Community Health Assessment</a></li><li class="liParent"><a href="/Main/mclaren-contact-us.aspx" target="_blank" class="25423667-c62c-4a08-89f5-0296d2ca22f8">Contact Us</a></li><li class="liParent"><a href="/Main/McLaren-History.aspx" target="_self" class="6c6781bf-645e-4190-9b15-b1df37f62521">History</a><ul><li class="liParent"><a href="/Main/McLaren-BR-History.aspx" target="_self" class="bff09b68-e652-41dd-8b45-a963dda7c15d">McLaren Bay Region</a></li><li class="liParent"><a href="/Main/Mclaren-CM-History.aspx" target="_self" class="023cada0-e4d5-4d9b-9da1-81c3b5ca94b6">McLaren Central Michigan</a></li><li class="liParent"><a href="/Main/McLaren-Flint-History.aspx" target="_self" class="d8639099-ce6a-4ead-9139-a59962d5d3db">McLaren Flint History</a></li><li class="liParent"><a href="/Main/McLaren-GL-History.aspx" target="_self" class="1a368f0f-f80d-41d5-9f49-d8227cb5b09d">McLaren Greater Lansing</a></li><li class="liParent"><a href="/Main/McLaren-HG.aspx" target="_self" class="9177539c-3814-4aa1-b948-718e538bed6b">McLaren Homecare Group</a></li><li class="liParent"><a href="/Main/mclaren-macomb-history.aspx" target="_self" class="787adc33-1be5-4654-a1e1-b00f7748edb4">McLaren Macomb</a></li><li class="liParent"><a href="/Main/mclaren-nm-history.aspx" target="_self" class="f8821150-597e-48dc-b092-7853def059fb">McLaren Northern Michigan</a></li><li class="liParent"><a href="/Main/McLaren-Oakland.aspx" target="_self" class="1f97c3e8-205c-45f2-899f-cdd5dcabed17">McLaren Oakland</a></li></ul></li><li class="liParent"><a href="/Main/mclaren-blog.aspx" target="_self" class="79a09704-9964-497f-b21c-29a720645b6f">Blog</a></li><li class="liParent"><a href="/Main/McLaren-Executives.aspx" target="_self" class="84efe6f0-cf94-4b18-9e0b-9ce8374387c1">Meet the Executive Team</a></li><li class="liParent"><a href="/Main/McLaren-Message.aspx" target="_self" class="d97677db-b368-4c2d-b08a-a3c7a2a3a306">Message to Our Community</a></li><li class="liParent"><a href="/Main/McLaren-Mission.aspx" target="_self" class="212a2d08-6f76-49f5-b5f0-9b2ad17f88d8">Mission, Values</a></li><li class="liParent"><a href="/Main/McLaren-Oakland-Medical.aspx" target="_self" class="62b40616-81aa-4f8c-8689-7e5a721217fa">Oakland County's Medical Main Street</a></li><li class="liParent"><a href="/Main/McLaren-Publications.aspx" target="_self" class="a424848b-5c16-49fe-ba74-05c565299104">Publications</a></li><li class="liParent"><a href="/Main/McLaren-Research.aspx" target="_self" class="8c52cd2f-a2bd-40fe-bff6-a7fd50955fff">Research Studies</a></li><li class="liParent"><a href="/Main/McLaren-Service-Area.aspx" target="_self" class="df08959d-e863-4e26-a7b3-e13754ca9049">Service Area  </a></li></ul></li></ul>
</div>
</div></div>
					
					<div id="cphBody_divContent" class="cpsty_SiteTypes_Default_Content">
	
	
			
<!--cpsys_Template:cpsys_Register-->
<!--cpsys_Template:cpsys_Register-->


<!--cpsys_Template:DetailsHeaderContent-->
<div class="left-rail-nav-container" id="service-line-datasource">

</div>
<div class="main-content">
<!---Shows Location Info--->

<style> 
/*----Styles For SubPages-----*/
.cpsty_CenterTd{width:100% !important;}.cpsty_Center h1{margin-top:0px;}.site-banner .inner-container{min-height:115px;}.site-banner .inner-container h1{line-height:115px; font-size:34px;}.site-banner .inner-container h1.multi-line{line-height:normal !important; font-size:34px !important; height:100px !important;}.site-banner{height:173px; background-color:#2f82b6; background-image:none;} .service-location-on-landing-page-top{display:none;} .service-location-on-landing-page{display:none;} .left-rail-nav-container{display:block; width:24.5% !important; margin-right:2%; float:left; border:solid 1px #cccccc;} .main-content{width:72.5%; float:left;} @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}
/*----Styles For SubPages-----*/
</style>

<h1 class="pageheader">Website Privacy Policy</h1>
<div class="service-location-on-landing-page-top"></div>
<p>McLaren Health Care Corporation cares about privacy issues and wants you to be familiar with how we collect, use and disclose Personally Identifiable Information you provide to us.</p>
<p>This Privacy Policy and Terms (the “Policy”) describes our practices in connection with Personally Identifiable Information that we collect through our website located at www.mclaren.org, the websites of our subsidiaries that display or directly link to this Policy, and the e-mail messages that we send to you that directly link to this Policy (collectively, the “Services”).</p>
<p>There are a number of circumstances where this Policy does not apply and we want to identify a few of them for you:</p>
<ul>
    <li>This Policy does not apply to the information you share when receiving treatment from your medical provider, including when you are receiving medical services from us. For example, this Policy would not apply to the information you share with us when admitted to the emergency room. The use and disclosure of the information you provide in such circumstances is governed by the Federal Health Insurance Portability and Accountability Act of 1996, more commonly known as HIPAA, as well as Michigan law. You can learn more about your HIPAA rights and protections and our obligations in our Notice of Privacy Practices.</li>
    <li>This Policy does not apply to the information you share and your use of the <em>My McLaren Chart</em>, which is the patient portal we offer. The use and disclosure of the information you provide when using the <em>My McLaren Chart</em> is governed by HIPAA, Michigan state law, as well as the terms and conditions and other policies you agreed to when registering to access the <em>My McLaren Chart</em> service.</li>
    <li>This Policy does not apply to the practices of companies that we do not own or control. Other services and organizations that we link to may have their own privacy policies governing the use of information you provide to them. For example, organizations such as Facebook, Twitter and LinkedIn each have their own privacy policy and practices.</li>
    <li>This Privacy Policy does not apply to individuals whom we do not employ, including any of the third parties to whom we may disclose user information as set forth in this Privacy Policy.</li>
</ul>
<p>If you have any questions or comments about this Policy or our privacy practices, please contact us by using one of the means listed on our <a href="/main/mclaren-contact-us.aspx">Contact page</a></p>
<ol>
    <li><a><strong>Personally Identifiable Information We May Collect</strong></a>
    <p>“Personally Identifiable Information” is information that identifies you as an individual, such as name, birth date, telephone number, e-mail address, or unique device identifiers that was gathered in connection with your use of the Services.</p>
    <p>If we combine Personally Identifiable Information with protected health information subject to protection under HIPAA, the combined information will be treated as protected health information for as long as it remains combined.</p>
    </li>
    <li><strong>Other Information we may collect</strong>
    <p>“Other Information” is any information that does not reveal your specific identity or does not directly relate to an individual, including, for example: (1) computer or device connection information, such as browser type and version, operating system type and version, device information, and other technical identifiers; (2) information collected through cookies and other technologies; or (3) aggregated information, such as usage history and search history.</p>
    <p>If we combine Other Information with Personally Identifiable Information, the combined information will be treated as Personally Identifiable Information for as long as it remains combined.</p>
    </li>
    <li><strong>Security Measures</strong>
    <p><strong>Virus Detection and E-mail Security:</strong>&nbsp;For website security, we use software programs to monitor traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage.&nbsp; </p>
    <p><strong>General Practices: </strong>Although we seek to use reasonable measures to protect Personally Identifiable Information, please be aware that no security measures are perfect or impenetrable. Therefore, <a>we cannot and do not guarantee that the information you provide to us through the Services&nbsp;</a>will not be viewed by unauthorized persons. We are not responsible for circumvention of any privacy settings or security measures contained on the Services.</p>
    <p><strong>Security of Service Communications</strong>: Some communication through the Services may be sent through the standard HTTP protocol and may be delivered using regular e-mail. Information sent over HTTP is not encrypted. E-mail, while convenient, also poses several risks (e.g., e-mail is not a secure form of communication, is unreliable, can be forwarded, etc.). We cannot guarantee the security of the information sent through such means, nor can we guarantee that information you supply to us will not be intercepted while being transmitted to us. It is important for you to protect against unauthorized access to your computer and to take appropriate security measures to protect your information. We use encryption technology, such as Secure Sockets Layer (SSL), to protect your protected health information during data transport with <em>My McLaren Chart</em>.</p>
    <p><strong>Your Obligations to Safeguard your Information</strong>: Security is not a one person job. You must also take reasonable measures to protect your information, including, for example, securing your computer and mobile device, using an antivirus software, using a firewall, and other similar safeguards.</p>
    </li>
    <li><strong>Disclaimer</strong>
    <p>McLaren Health Care Corporation and its subsidiaries offer information on the Services for general educational purposes only. This information should not be used for diagnosis and treatment, nor should it be considered a replacement for counsel with your physician or other health care professional. If you have questions or concerns about your health, contact your health care provider. We encourage you to use the Physician Finder to locate a physician by specialty and area.</p>
    <p>While we and our subsidiaries make reasonable effort to ensure accuracy of the information on the Services, we do not guarantee the accuracy, and the information is provided with no warranty or guarantee of any kind.</p>
    </li>
    <li><strong>Links</strong>
    <p>We and our subsidiaries provide links to other websites. These links are provided as a convenience to you and as an additional avenue of access to the information contained on such third-party websites. Different terms and conditions may apply to your use of any linked sites. We encourage you to read the privacy policy of each website.&nbsp; We have no control over third party websites and make no claim or representation regarding such websites. We accept no responsibility for, the quality, content, nature, or reliability of any websites accessible by hyperlink from the Services, or websites linking to the Services.&nbsp; We are not responsible for any losses, damages or other liabilities incurred as a result of your use of any linked sites.</p>
    </li>
    <li><strong>Corrections, unsubscribe and data retention</strong>
    <p>You may update the information you provide to us through the Services. To change your information, contact&nbsp;<strong><a href="http://mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>. If you are subscribed to one of our newsletter, you may unsubscribe from receiving the newsletters by clicking on the appropriate link at the bottom of the e-mail. We may, from time to time, send you administrative messages. You cannot opt-out from receiving administrative messages.</p>
    <p>We will retain your information for as long as needed to provide you services, comply with our legal obligations, resolve disputes, and enforce our agreements. Except for authorized law enforcement investigations, other valid legal processes or as described in this Policy (or another policy in place between you and us), we will not share any Personally Identifiable Information we receive from you with any parties outside of McLaren and its subsidiaries.</p>
    </li>
    <li><strong>Changes to the Privacy Policy and Terms</strong>
    <p>We may change this Policy from time to time. Please take a look at the “Last Modified” legend at the top of this page to see when this Policy was last revised. Any material changes to this Policy will become effective 7 days from when we post the revised Policy on the Services and all other changes to this Policy will become effective when we post the revised Policy on the Services. Your use of the Services following the effective date means that you accept the revised Policy. If you have questions or comments about this Policy, contact&nbsp;<strong><a href="http://mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>.</p>
    <p><a><strong>McLaren Foundations:</strong></a>&nbsp; Some subsidiaries uses Internet donors' technology provided by Blackbaud, Inc. This is encrypted technology and allows financial donations to be forwarded electronically using a secure server.</p>
    <p>McLaren Health Care Corporation and its subsidiaries use oxcyon.com as a host server and vendor, the content management system is Central Point.</p>
    </li>
    <li><strong>Use of Services by Minors</strong>
    <p>We do not knowingly collect Personally Identifiable Information from individuals under the age of 13 and the Services are not directed to individuals under the age of 13. We request that these individuals not to use the Services.</p>
    </li>
    <li><strong>International Visitors</strong>
    <p>The Services are controlled and operated from the United States, and are not intended to subject us to the laws or jurisdiction of any state, country or territory other than that of the United States. If any material on the Services, is contrary to the laws of the place where you are when you access them, then we ask you not to use the Services. You are responsible for informing yourself of the laws of your jurisdiction and complying with them. By using the Services, you consent to the transfer of information to the United States, which may have different data protection rules than those of your country.</p>
    </li>
    <li><strong>Use of Materials on the Services, Trademarks and Copyrights </strong>
    <p>You acknowledge and agree that all content on the Services (including, without limitation, text, images, user interfaces, visual interfaces, graphics, trademarks, logos, sounds, source code and computer code, including but not limited to the design, structure, selection, coordination, expression, ‘look and feel’ and arrangement thereof) is the exclusive property of and owned by us or our licensors and is protected by copyright, trademark, trade dress and various other intellectual property rights and unfair competition laws. These marks and copyrights may not be copied, imitated, or used, in whole or in part, without the express prior written permission from their respective owners, and then with the proper acknowledgments. Nothing on the Services shall be construed as granting, by implication, estoppel, or otherwise, any license or right to use any trademark, logo or service mark displayed on the Services without the owner’s prior written permission, except as otherwise described herein.</p>
    <p>You may access, copy, download, and print the material (such as, for example, educational materials, service descriptions, and similar materials) purposely made available by us for downloading from the non-secured component of the Website (“General Website”) for your personal, non-commercial use and for your business use in connection with evaluating McLaren healthcare provider services for offer by your health plan or use by your employees, provided you do not (i) modify or delete (including through selectively copying or printing material) any copyright, trademark, or other proprietary notice that appears on the material, and (ii) make any additional representations or warranties relating to such materials.</p>
    <p>You may access, copy, and print the material contained in the secured component of the Website in accordance with the terms and conditions governing the secure section.</p>
    <p>Any other use of content or material on the Services, including but not limited to the modification, distribution, transmission, performance, broadcast, publication, uploading, licensing, reverse engineering, encoding, transfer or sale of, or the creation of derivative works from, any material, information, software, documentation, products or services obtained from the Services, or use of the Services is expressly prohibited.</p>
    <p>We, our licensors, or content providers, retain full and complete title to any and all materials provided on the Services, including any and all associated intellectual property rights. </p>
    <p>As long as you comply with this Policy, we grant you a personal, non-exclusive, non-transferable, revocable, limited privilege to enter and use the Services. We reserve the right, without notice and in our sole discretion, to terminate your license to use the Services and to block or prevent future access to and use of the Services.</p>
    </li>
    <li><strong>Submissions and Postings</strong>
    <p>To the extent that we allow submissions on the Services, you acknowledge that you are responsible for any material you may submit via the Services, including the copyright, legality, reliability, appropriateness, and originality of any such material.</p>
    <p>You represent and warrant (and we rely on your representation and warranty) that you (i) own or otherwise control all the rights or have sufficient rights to the content you post or that such items are known to you to be in the public domain; (ii) that the content is accurate; (iii) that use of the content you supply does not violate any provision in this Policy or terms you may have agreed to with a third party; (iv) that the content is not defamatory or otherwise trade libelous; (v) does not violate any law, statute, ordinance or regulation; and (vi) that you will indemnify us for all claims resulting from content you supply, including arising from an action alleging infringement of copyright or other proprietary rights in such work.</p>
    <p>We undertake no duty to determine the validity of any claim of copyright or trademark infringement. Upon receiving written notice that any item posted on the Services is believed to infringe a copyright or other proprietary right, we will remove said work.</p>
    <p>If you do submit material, you grant us and our affiliates an unrestricted, nonexclusive, royalty-free, perpetual, irrevocable, transferrable and fully sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute and display any and all material not subject to protections under HIPAA throughout the world in any media. You further agree that we are free to use without limitation and without any compensation to you any ideas, concepts, or know-how that you or individuals acting on your behalf provide to us. You grant us the right to use the name you submit in connection with such material.&nbsp; We retain any and all rights granted in this Policy in and to any user submitted content or non-HIPAA materials after termination, notwithstanding the reason for any such termination.</p>
    <p>We have an absolute right to remove any material from the Services in our sole discretion at any time.</p>
    </li>
    <li><strong>Usage Rules</strong>
    <p>You hereby agree to not upload, distribute, or otherwise publish through the Services any content that (i) is unlawful, libelous, defamatory, obscene, pornographic, harassing, threatening, invasive of privacy or publicity rights, fraudulent, defamatory, abusive, inflammatory, or otherwise objectionable; (ii) is confidential, proprietary, incorrect, or infringing on intellectual property rights; (iii) may constitute or encourage a criminal offense, violate the rights of any party or otherwise give rise to liability or violate any law; or (iv) may contain software viruses, chain letters, mass mailings, or any form of “spam.”&nbsp; You may not use a false email address or other identifying information, impersonate any person or entity or otherwise mislead as to the origin of any content.&nbsp; You may not upload commercial content onto our Services.</p>
    <p>You expressly agree to refrain from doing either personally or through an agent, any of the following:&nbsp; (1) use any device or other means to harvest information; (2) transmit, install, upload or otherwise transfer any virus or other item or process to the Services that in any way affects the use, enjoyment or service of the Services, or any visitor’s computer or other medium used to access the Services; (3) engage in any action which we determine in our sole discretion is detrimental to the use and enjoyment of the Services; or (4) transmit, install, upload, post or otherwise transfer any information in violation of the laws of the United States. You may further not use any hardware or software intended to damage or interfere with the proper working of the Services or to surreptitiously intercept any system, data, or personal information from the Services. You agree not to interrupt or attempt to interrupt the operation of the Services in any way.</p>
    </li>
    <li><strong>Infringement Notice</strong>
    <p>We respect the intellectual property rights of others and request that you do the same. If you believe your copyright or the copyright of a person on whose behalf you are authorized to act has been infringed, you may notify us in writing to the e-mail address or mailing address provided in the “How to Contact the Web Team” section below with attention to Copyright Agent. To be effective, your notification must be in writing, include your contact information, provided to our copyright agent, and include:&nbsp; (i) signature of a person authorized to act; (ii) identification of the copyrighted work claimed to have been infringed; and (ii) identification of the material that is claimed to be infringing including references to the location of the material on the Services.</p>
    <p>If you believe other intellectual property rights were violated, you may notify us in writing to the mailing address provided in the “How to Contact the Web Team” section below with attention to General Counsel.</p>
    </li>
    <li><strong>Jurisdiction and Applicable Law</strong>
    <p><strong>The laws of the State of Michigan, </strong>without regard to any conflicts of laws principles thereof,<strong> will govern the construction and interpretation of this Policy and the rights of the parties hereunder. By accessing, using, or registering for the Services, you acknowledge that you have read, understood, and agreed to be bound by this Policy and by all applicable laws and regulations. </strong>The Parties agree on behalf of themselves and any person claiming by or through them that the exclusive jurisdiction and venue for any action or proceeding arising out of or relating to this Agreement will be an appropriate state or federal court located in Michigan and each party irrevocably waives, to the fullest extent allowed by applicable law, the defense of an inconvenient forum.<strong></strong></p>
    </li>
    <li><strong>Severability and Waiver</strong>
    <p>Our failure to exercise or enforce any right or provision of this Policy will not constitute a waiver of such right or provision. If any provision of this Policy is unlawful, void, or unenforceable, for any reason, the remaining provisions will remain in full force and effect to the fullest extent of the law.<strong></strong></p>
    </li>
    <li><strong>How to Contact the Web Team</strong> E-mail:&nbsp;<strong><a href="http://mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong> Fax:&nbsp;(989) 891-8185</li>
</ol>
<ul>
    <li><strong>We respect your privacy and do not collect Personally Identifiable Information through the Services unless you choose to provide it. We, and our service providers, may collect Personally Identifiable Information in a variety of ways, including when</strong>:
    <ul>
        <li><a>You contact us to request information by sending us an e-mail</a>&nbsp;; </li>
        <li>You participate in one of our training or education events, such as a childbirth education, nutrition education or exercise &nbsp;training seminars;</li>
        <li>You request one of our publications or newsletters;</li>
        <li>You participate in a Service related survey, contest, or other promotion; or</li>
        <li>You complete a questionnaire(s) on the Services (<em>e.g.</em>, did you find our website helpful?)</li>
    </ul>
    </li>
    <li><strong>We may use personally identifiable information</strong>:
    <ul>
        <li>To send administrative information, such as information regarding the Services and changes to our terms, conditions, or policies;</li>
        <li>To respond to your inquiries and fulfill your requests;</li>
        <li>If you enter into a contest or similar promotion we may use the information you provide to administer those programs;</li>
        <li>We may use survey information for research and quality improvement purposes, including helping us to improve information and services offered through the Services;</li>
        <li>For our business purposes, such as improving or modifying our Services, identifying usage trends, and operating and expanding our service and information offerings;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>Your Personally Identifiable Information may be disclosed</strong>:
    <ul>
        <li>To identify you to anyone to whom you send messages through the Services;</li>
        <li>To our third-party service providers that provide services such as website hosting, information technology and related infrastructure, customer service, and other similar services;</li>
        <li>To a third-party in the event of any reorganization, merger, sale, joint venture, assignment, transfer or other disposition of all or any portion of our business or assets;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>California Do Not Track Notice</strong>:
    <ul>
        <li>We do not track you over time and across third party websites to provide targeted advertising and therefore do not respond to Do Not Track (DNT) signals;</li>
        <li>Third parties that have content embedded on our website, such as a social networking connectors (<em>e.g.</em>, Facebook, Twitter) set cookies in your browser as well as obtain information about the fact that a web browser visited the Services from a certain IP address.</li>
    </ul>
    </li>
</ul>
<ul>
    <li><strong>How We May Collect Other Information</strong>: We, and our third-party service providers, may collect Other Information in a variety of ways, including:
    <ul>
        <li><em>Through your browser or mobile device</em>: Certain information is automatically collected by most browsers or through your mobile device, such as your computer type, screen resolution, operating system name and version, device manufacturer and model, language, and Internet browser type and version. We may also collect information on the search terms you used to find our website, the search engine you used, or the address of the web site from which you came to visit.</li>
        <li><em>Using cookies</em>: Our Services may use cookies and other technologies such as pixel tags and web beacons. These technologies help us provide better services to you, tell us which parts of our Services people have visited, and allow us to better measure the usability of our Services. We treat information collected by cookies and other technologies as non-personal information. You have a variety of tools to control cookies and similar technologies, including controls in your browser to block and delete cookies.</li>
        <li><em>IP Address</em>:&nbsp; Your “IP Address” is a number that is automatically assigned to the computer that you are using by your Internet service provider (ISP). An IP Address may be identified and logged automatically in our server log files, or those of our website hosting vendor, whenever a user accesses the Services, along with the time of the visit and the page(s) that were visited.</li>
        <li><em>By aggregating information</em>:&nbsp; Aggregated Personally Identifiable Information does not personally identify you or any other user of the Services. We may aggregate information for a variety of reasons, for example, to calculate the percentage of our users who visit a particular page or clicked on a particular item from a newsletter.</li>
        <li><em>Google Analytics</em>: We use a service from Google called “Google Analytics” to collect information about use of the Services. Google Analytics collects information such as how often users visit this site, what pages they visit when they do so, and what other sites they used prior to coming to this site. Google Analytics collects only the IP address assigned to you on the date you visit this site, rather than your name or other identifying information. Google’s ability to use and share information collected by Google Analytics about your visits to our Services is restricted by the Google Analytics Terms of Use and the Google Privacy Policy. We may use the information from Google Analytics for a variety of reasons including trend analysis and to make our Services more useful to the communities we serve.</li>
    </ul>
    </li>
    <li><strong>How We May Use and Disclose Other Information</strong>
    <ul>
        <li>We may use and disclose Other Information for any purpose, except where we are required to do otherwise under applicable law.</li>
        <li>If we are required to treat Other Information, such as IP addresses or other similar identifiers, as Personally Identifiable Information under applicable law, then we may use it as described in “How We May Collect Other Information” section above, as well as for all the purposes for which we use and disclose Personally Identifiable Information, but we will treat these identifiers as Personally Identifiable Information. If we are require to treat Other Information as protected health information as defined by HIPAA, then we will treat the identifiers in accordance with our Notice of Privacy Practices.</li>
    </ul>
    </li>
</ul>
<p><strong>Mail:</strong><strong><br>
</strong>McLaren Bay Region<br>
Marketing &amp; Public Relations<br>
503 Mulholland Ave<br>
Bay City, MI 48708</p>
<!-----Related Links------>


<div class="subpage-info">
<!----Related Photo Gallery---->
<!----Related Documents---->
<!----Related Videos------->
<!----Related Links-------->
<!----Related Blogs-------->
</div>
</div>
<!--cpsys_Template:DetailsHeaderContent-->


		
	

<!--cpsys_Template:DetailsFooterContent-->

<!--cpsys_Template:DetailsFooterContent-->


		
	


</div>
					
				</div>
			
			</div>
			<div id="cphBody_tdRight" class="cpsys_BlockColumn cpsty_RightTd">
				
				<div class="cpsty_Right">
					
					
					<div id="cphBody_divRightAc2" class="cpsty_SiteTypes_Default_RightAc2"><div id="cpsys_Advertisers_f3226af2-3ced-4f64-a3ab-844e6284e705" style="text-align:left;">
	<table cellspacing="0" cellpadding="0" class="cpsys_Table" style="height: 400px;">
    <tbody>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
</div></div>
				</div>
			
			</div>
		
		</div>
	</div>
	
</div>
	</div>
			
		
</div>
    </div>
    
	<footer>
<div class="footer-social-outer-container">
<div class="footer-social-inner-container">
<ul>
    <li><a href="https://www.facebook.com/McLarenHealth"><img style="" src="/Uploads/Public/Images/Designs/footer-facebook.png"></a></li>
    <li><a href="https://twitter.com/mclarenhealth"><img style="" src="/Uploads/Public/Images/Designs/footer-twitter.png"></a></li>
    <li><a href="https://www.linkedin.com/company/mclaren-health-care"><img style="" src="/Uploads/Public/Images/Designs/footer-linkedin.png"></a></li>
    <li><a href="https://www.youtube.com/user/mclarenhealth"><img style="" src="/Uploads/Public/Images/Designs/footer-social-red.png"></a></li>
    
    
</ul>
</div>
</div>
<div class="outer-container">
<div class="inner-container mobile-hide">
<nav class="btm">
<div class="col-1">
<ul>
    <li><a href="/Main/Career.aspx">Careers</a></li>
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a href="/Main/mclaren-web-privacy-policy.aspx">Website Privacy Policy</a></li>
    <li><a href="/Main/Sitemap.aspx">Site Map</a></li>
</ul>
</div>
<div class="col-2"><img src="/Uploads/Public/Images/Designs/FooterLogos/footerlogowhite_McLarenHealthCare.png"><br>
<br>
McLaren Health Care<br>
<br>
Flint, MI 48532<br>
<span style="font-size: 30px;"></span></div>
<div class="col-3">
<ul>
    <li><a href="/Main/employee.aspx">For Employees</a></li>
    <li><a href="/Main/physician.aspx">For Physicians</a></li>
    <li><a href="/Main/vendor-registration.aspx">For Vendors</a></li>
    <li><a href="/Main/volunteer.aspx">For Volunteers</a></li>
    <li><a href="/Main/gme.aspx">For Medical Education</a></li>
    <!------------- main, bayregion, macomb, oakland, flint, lansing ---------GME Link-------->
</ul>
</div>
</nav>
</div>
<div class="btm-ribbon">
<div class="inner-container">
<p>McLaren Health Care, through its subsidiaries, will be the best value in health care as defined by quality outcomes and cost.</p>
<p>©All rights reserved.  McLaren Health Care and/or its related entity</p>
<div style="clear:both;"></div>
</div>
</div>
</div>
</footer>
    
	<div class="dv-bottom-edit-liks">
	
	
	
	
	
	</div>
	<div id="uprgUpdateProgress" style="display:none;">
					<table border="0" cellpadding="0" cellspacing="0" class="updateProgress" style="position:fixed; top:0px; right:0px; border: solid 1px #CCCCCC; background-color:#F2F2F2;">
				<tr>
				<td style="vertical-align: middle; padding: 2px;"><img src="/Integrations/Centralpoint/Resources/ProgressIcon.gif" alt="Loading..." /></td>
				<td style="vertical-align: middle; padding: 2px;">Loading...</td>
				</tr>
				</table>
			
</div>
    <input type="hidden" name="ctl00$ctl00$FormAction" id="FormAction" value="0" /><input type="hidden" name="ctl00$ctl00$FormGroup" id="FormGroup" /><input type="hidden" name="ctl00$ctl00$FormButton" id="FormButton" />

<script type="text/javascript">
//<![CDATA[

 //Admin > Properties: HeaderStartupScripts 
$('table#DynamicNavigation1 img[src="/Uploads/Public/Images/arrow.png"]').attr('alt', 'arrow');

 //End of Admin > Properties: HeaderStartupScripts 
$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show().siblings().show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('#cpPortableNavigation73').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation73').unbind('mouseleave');
});
$('#cpPortableNavigation73 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation85').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation85').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation85').unbind('mouseleave');
});
$('#cpPortableNavigation85 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation85').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation77').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation77').unbind('mouseleave');
});
$('#cpPortableNavigation77 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
});

                    $(document).ready(function() {
                    $('.mobile-navigation-accordion').cp_Accordion({collapsable : true, active : -1, headerClass : 'mobile-navigation-accordion', contentClass : 'mobile-navigation-content', event : 'click', expandedSpanText : '', collapsedSpanText : '', expandCollapsePosition : 'left', expandedHeaderText : '', collapsedHeaderText : '' });
                    });
					if ($('input[name="HtmlSearchCriteria"]').length > 0) {
						$('input[name="HtmlSearchCriteria"]').autocomplete({
							source: function(request, response) {
								$.ajax({
									type: "POST",
									url: "/WebServices/ClientMethods.asmx/SiteSearchAutoComplete",
									data: "{ \"term\": \"" + request.term + "\" }",								
									dataType: "json",
									contentType: "application/json; charset=utf-8",
									success: function(data) { response(data.d) },
									error: function(XMLHttpRequest, textStatus, errorThrown) { /*alert(textStatus + '\n' + errorThrown);*/ }
								});
							},
							select: function( event, ui ) {
								$('input[name="HtmlSearchCriteria"]').val(ui.item.value);
								$('input[name="HtmlSearchCriteria"]').siblings('input[type="button"], input[type="submit"]').click();
							},
							autoFocus: true,
							delay: 10,
							minLength: 2
						});
					}
					Sys.Application.add_init(function() {
    $create(Sys.UI._UpdateProgress, {"associatedUpdatePanelId":null,"displayAfter":500,"dynamicLayout":true}, null, null, $get("uprgUpdateProgress"));
});
//]]>
</script>
</form>
</body>
</html>
