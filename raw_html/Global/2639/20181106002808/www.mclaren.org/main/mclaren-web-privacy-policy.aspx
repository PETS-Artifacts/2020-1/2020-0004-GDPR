

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html id="html" xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head id="head"><meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link id="lnkSiteType" rel="stylesheet" type="text/css" href="/SiteTypes/Default.master.css.aspx?aud=Main&amp;rol=Public" /><title>
	Website Privacy Policy | McLaren Health Care
</title>
<!--Admin > Properties: HeaderHtml-->
<link type="text/css" href="/Uploads/Public/Documents/dropdownlinks.css" rel="stylesheet" />
<link rel="shortcut icon" href="/Resource.ashx?sn=favicon" />
<link href="/Resource.ashx?sn=slider" rel="stylesheet" type="text/css" />
<!--End of Admin > Properties: HeaderHtml-->
<!--Design > Styles (FY18 Site Design): HeaderHtml-->
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400,500,700,800,900,500italic,400italic,100italic,900italic,300italic,700italic,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">
<link rel="icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->

<!--[if IE 7]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE7-STYLES.css?v=2">
<![endif]-->

<!--[if IE 8]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE8-STYLES.css?v=2">
<![endif]-->

<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<!--End of Design > Styles: HeaderHtml-->
<!-- Site Architecture > Audiences (McLaren Health Care): HeaderHtml-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6396952-25', 'auto');
  ga('send', 'pageview');

</script>
  <script type="text/javascript">
    var _monsido = _monsido || [];
    _monsido.push(['_setDomainToken', 'VW4sxwq_5Jph8JUIFq9uyQ']);
  </script>
  <script src="//cdn.monsido.com/tool/javascripts/monsido.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link type="text/css" href="/Uploads/Public/Documents/mhc_custom.css" rel="stylesheet" />

<!-- 
DP Snippet
-->
<!-- Global site tag (gtag.js) - DoubleClick -->
<script async src="https://www.googletagmanager.com/gtag/js?id=DC-8351112"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'DC-8351112');
</script>
<!-- End of global snippet: Please do not remove -->
<!--End of Site Architecture > Audiences-->
<meta name="robots" content="ALL, FOLLOW, INDEX" />
<meta name="rating" content="GENERAL" />
<meta name="revisit-after" content="30 days" />
<link href="/Integrations/JQuery/Themes/Stable/Root/jquery-ui.css?v8.6.6" rel="stylesheet" type="text/css" />
<style type="text/css">
/* Site Architecture > Audiences (McLaren Health Care): HeaderStyles */ 
.mobile-navigation-link-visitor{display:none;}
/* End of Site Architecture > Audiences: HeaderStyles */

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
/* Admin > Properties: HeaderStyles */ 
a.dropdownlinks:link{font-size:12px ! important;}
a.dropdownlinks:visited{font-size:12px ! important;}
a.dropdownlinks:hover{font-size:12px ! important;}
a.dropdownlinks:active{font-size:12px ! important;}
/* End of Admin > Properties: HeaderStyles */
.mobile-navigation-accordion { cursor:pointer; }
.mobile-navigation-accordion span.expanded { padding-left:17px; }
.mobile-navigation-accordion span.collapsed { padding-left:17px; }
.mobile-navigation-accordion span.expanded { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-expand.png') no-repeat center left; }
.mobile-navigation-accordion span.collapsed { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-closed.png') no-repeat center left; }
.cpsty_Left {display:none;}
    @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}
    

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#4c4c4c; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#4c4c4c; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#0065a4; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#4c4c4c; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
.CpButton { cursor:pointer; border:outset 1px #CCCCCC; background:#999999; color:#463E3F; font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 10px; font-weight:bold; padding: 1px 2px; background:url(/Integrations/Centralpoint/Resources/Controls/CpButtonBackground.gif) repeat-x left top; }
.CpButtonHover { border:outset 1px #000000; }
</style></head>
<body id="body" style="font-size:100%;">
    <form method="post" action="/main/mclaren-web-privacy-policy.aspx" id="frmMaster">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="49B5jP0Ucp5cbAKHhjcqJS/wQMJAk2nCsylM3IRAt5t42elVBYZqGS96rLikCuuDlj8MyZmobCKfBE794xME+1PYglDLV+vditlLbJ1jKexx90douQwNa07y/A5xt+TlaL730mPeFOVhCsXi8WAvkAf6NOVB/FIG8KurCK2bXJGiniHailz2bsbMfpG6eMucCqslMh7IjtOBUOwNg/HV/WcWPLTuJPSAGe5qjd3toxwakrFJiqLnroO6EmW8VOODll1q3KkVxT520F99mFivn0ac6K4Z8ZiMXCl8b7tCztmMc9XA/giDkfY9zovidm005Y6N+LG8DSoxUXt05+RHXUJ0+KM1iJVn0e/MC2yftqcaxUGn9Uun4runmYrHLhP1DONchMZSU4xzLqbf1Xje9tYK+79aDRqOexAPi8BQron7itXUx7hzCduaeSty0GHIQVEg6Yr2sIr/orZbhqlklUaeevemnbB6hxTLpbw/9IzXxzFjHWxAWGwsFQtNjBJeWUqZNajXdlbWPXivXXovZGDXc0sHtoLy/VnTt4AFTf7MymZpPJanft2yghR1xClvTgsC8fe7Z1/0T6paZ+z0Vo0UFem0lusNH2TzIEtIb84kZ0VQCUTp0xSIqJuh5fnw8P3bc0g3j7AhUM55tPDXRbud9Zn8WrvWEf5xf6x+m64vIFIM9rdwNgM88Y1yXxlx+0IdGSgZgjvdcLzLCHegdwnF0cV6koku2ORUlNAu8cCGqHVZ9NEq9dSEucFXlj4cLXzHFRVMcEWDJ1qbF2OJ3AM4Z+VDYT2w5xVBxIoTQZ8wLekZ8vg5amVJ3Sq5598L+481W2yWhl0nmvAwchd1s65ycbXojOPyhBUsv/L3Dv1SpoUM/gUZed0IgXpKUkiKHlrB3pYyoTlAGUsB4V09iaq3LAcZZlCpDYh/qaRHR1/pQBq6wStYSoqxYiTHafEu940t9wA/KIziS8w97tuHTTER6A/xgdO2EfyNuhJLUyWs7jb6B2Cl+hUF6F8X+oNCw/dQQRvy+KeXkenWHxmjVK75sigLCzRYoJe6Ay0dvPFl3UB9w52jfFk6rc9vtUVqIEGIHn0qHOcZ2Aah0xqvCD/0e900zAn5gOxNuMuUonoFjDpLUmI8ZKIgIZDtQPCp148G5WzmdFptU2EO1dkA1Tp4OUeFU996fa5662li5EOeotQ7zqccvofCrEe0+H31+xhJUZymS9nhSaqOU0kQ1rAKF01ZuIRAMRjVPSzEYT0FMZnvKuKCUvqslhCvSqKDIyl3d6hwr7xZLM7dTbEXT+HAu8tNRGY7lf34YQlrw/bpBsOny1+nt3mRV1k06vpDgY9mG4SjzezUaZazo+q7PKwKF1AcepHj8ZrPRYss3+smevRspfTxO+kHfRTb+sX+gCn9bTQzLiwwE56z9S381qCie8M2Cz6itqYuKLmEwOrCSwZ9wk7d+jyVtTPM3FnNXwnFVSL5rV9KLVjcHzykmmdbbKr0C8BDFWEmuOxsClUAVF6yv/d4PTnIwM2c1Kdg2mncE3rquuvJtVh68cuky9P741IO8lgplfDHRT8C7bC7/16SnzIVjPQqy6obOZ1JCucX5HPf/kp7D8YZhyyM3ksnoNvltEqvggbwRaw56kx0O/h4Jf8mhRem4mdCiUV+bRzMsD+fSuELNarZgTKyjyz6gedQUq+POjwyBAjWZ0e20oXLwvunPQsbAV0hCCU9SBLzuimJRVHrGNNfBzoCpctYnSJ7neajjlRdXADUOlZld78HICTt8hAGFM9eJ6s3roQ0mnvw3eK5h0NUs2z/90zT3Buzffts6QZCozu8HGfxeglK2nJiDUD/bvViHnL8O8n0NftIT41tSVVNwRVhzzLlh88uqs4Y9zaonuiI9+sx1ceh/6xU5whX7vle9YRF7hA4bEd3rl5Kpf+4yBo8Fn3jXDt19qe4YgYdhJluwlyCBsZHXOIzSbBhqBKzcqhKY/H6N5w1Tc48gjrqrXWGBpzWvmoeO1VsZy4ARbrMiyogdtIOAR74wAugqoptDJII33S2mx+OsY+k9BdEtBGDdMTXormkmPzhra4iucnjgm0EOYrJHdnur72G6rgrmPtZ7Ec6lNcF1ZdBSrQEgFiKcfnqqpzP2dPKn1fW5ikR1cr7kTUnff+VWJnVNpZ3VTuIHrA1bg5iqRAV93RLM555bvttr9chFjRIukpuxbB6CUxjA3DANj04JQ6pO1epBj2mxuhRi7WvawpiBGkCCuZ8W4fjRO4uyaN9XR7Ml0voypdvjLzpQHFSURGQsPNGyspOAlKNXw5U/Vcby31Q+1BdqNUNNHyKxq63UpZkTNJfFijXkXHgB+XMyqOWJujXZ84IYBPaM4UMDAMvY7Nl2wLFG+BXDHFZrsnq6g3Ceeto4rVFm9tTQEo/yfNdySbclxwrKqSuGYahVIZVqo2IQe3tZg8jSSYL4Nj96IRH7UnnlGPyUD2Ode0Hx3iimbW0zwJMyJR22eAAjyfc2IBRqAaJFqfaFj1bYn1AEXK1iEri1ElJr0xmp0MeHB4dZKZXHO99Mzfk0ZPJ+GwutXiS6S7/Klq8H8dNFETAhZ664CU8F2hc0h5mDwB8YzJX3FwQVZ5nbQcFbPj1dBdCB1wECavmafG0qWLfrZufeL+TxEI/Oe0ztydr/n6olJFGUgV1agcl1vkxQ8gJdQFLAVeu2Bu1hHveKtuFN93EBoOI7MEh3G0jacJX/ldFTp1niSffH8/+FabVAcz24WTmSvh6kgoZsyw2s5hs6LX58gIoxDoVeiBpYlRrdt3xe3962raC5pxBWY15FtFiIzGEossozZXneB2uVfgoMK2p9qgGfUlKnQw8eibE+5Tgzk9Tv0/sO5HsOc4Z1fwcQoDHM2n5RD3AEpbZsZc/r6h7M/pIGIKsHtWwgUgYJllqPrvhB4Ls9kMDkw76+XNifAtKcNYr6O1eW3DQc+mG4SNwFFb70YFE0LHySHckhJZGw3nazx7M7jjwR36LfCQdjEbV5gxGAYfk8a/tE4IsZ5iSYmcr9feC8Nzla57LDtJ7a8sHLJxvO4Vzh5R7Hrn5FFCznkuOR+rL9VgWt3SzpCbBbcAgqvWog2Qaclqg+iCxqCccJAFxqGX/S3lDcg3lE2a3fcMlsazOkPuHPI7fqHNZC1UpIAN8K7QPJfXdDFM+SUK17znvzwCzhaoio/4qWLsjfrgfgsbPPGKenro3ddqwUgb/6Vgngr/S7oPfbI4d01MMoxv5Ypino51CdvAM10hWmyJnoRxqlWHxGkDZuzTKsOZS3ScC3eRzggc4iVpfVcA8BDRlBOHj9htPVYnDQS59+D9HU+bqMUNpr/iGHmRKYCQ9rDD8bKD2vCKE8isjNBU8HeRaFvKzmA4sZSyCznWZX8g8m1TsheryLlvTX50POv8uj+QLmJs7ex1/D9dVdkk7GJMraf/i0FaTvtvZdMcgyp/ckoLBkC00V6DGTtakKDCEH5pBXYE8a+XpQ/4oYXylACCTlb6aUKbj5b74gVrd5jRM7oqt/D1ByfEsTGEZR3fF/UEmlOFP+pIM2echpuT43clFw4svAhOUIx+gCm7Yn8dzYgGKNrNEY3A3JjG3I8o/6bsIaN8KbtCqp0Mb5ePHRoHLmV3AdwngZ/OOOwtODydICFtdtdV1Z+DrvbP7hrdDz4lv3vK6Qj39UNT1sMxFLVNk20ZfkqWvG0IA1tnGEygIjFHnyUpGDTRF3s3jYuzaDlC1X3pQHmI6UYZRDpjSEUYcXfKkcCWBQEIKDQq0MzDn4ci0LyKQLnpimR31nvYIlbW0cZJ3EX19Jbk/ZzH+1RIVf7vQva5ijoowIqzxG1nXMh6huEztVSl6sRRKbE/YiAKVx/v6mkHs0uhYL6O5ERM400Yd9lXUQ25mIwuwh8fhML299Z0cwfkPoAQWX99/v6oqwk4Q9ymWbKsmBDQyT/XUpJIGTBB6NescQ52FtZNETK+cTVGOdhpDCY4DUz4OX/+oLUX0Beuz+e5jwyv0zcqaQUHEZzb8Xxx6GaHJlmqpPcOkBth0gZmF3/JdZzr2uyxNMUO6D5sqk0qNBDtbrV1TZV7NYjQFdSH0c/DUjW7hs10wjgMUMWabNVhSVxLe1ZNrx1OZ8SgTQbhUrXS7dInc1qThXNIZ43ekkc+Ox5WHkrsScJG7BstaCCAzx7JyW8D15hTE3QoZG1wqt55Uq75HgayruFvJzY0Mgp03yth6NlPX9CSbmeHfIxsSAKScJTBAEJRoIQJb8ntzLtDblCw9PkRIkaPeATWG18uFAVoKIowEAzBX3Ex7x2I2eVA5Zllsp8GNwu8sgCThl6xzlfc0JqAmMQZqocXr7Tli86FJkolC0JQqfN0sHjJTq/bNjb+zvIAguYnXnoXs3LNLv0dYJt9Ux8bdIX55rbEqdF5xgezyKT1Wmoy1Pr5I2Umr1dBJKY3Q+fEllfGgnASa2vbUnSfI3K74aWYBOhz2/KHdVB7JD/a3KfTQ89sn9XMPNNcdDle5y0eXwdaOvwb6lrWZxbM66qCcZlDuQdVbqsjcUdW3rZTulEND1lEbStoYL0ue5QtW1bfBsTZmJGsa4dPIfxXqqmw5M12U3IsJwnGsTsXPpr1B6+NlMnBtTbMHw8UguC6qUldIyV9hvOksw7fNqg+iX22/3BH+dqSyEyq4HAOKQ3KN8TPGGGBB7VOmRbCBhK4l/FZf9usJNxDUd5LP/WP6yFBwTNxNM16m0cIw/Aqd8YT5M+0bJgrPnLLcUFXKqvvvYg9bzHUv8Ewr9a7StJ15DfKo/5UbxVU3uyDiQ1xpmKxZs4w6qiZO8nHy7mb+haGiBIIoEqOz/Oa6JnS1GOEERIhklXmezSyo+5R9DVuQWGtTJ1Ustdt74ROHMjXkwmASymycKVQH7reDSfRFJytiT6WpwuB7OpVJUDpJtootuPLFuezU07QLyuZVN9mjUwl098B+ku7Oc54n8r0IZio7G6AkZDc+ieNFe02ZN16yoUuS1jYAMl75srnNQCqdLpB+xKgUSPqkSBfMhqyfWldmHraNYZEHPC3xHVCMVF+R9j0yDOJX6YmtiAtYEAkxcNPeTEDLxImc2KRgY4vKlOCw6wX577GFXyLZ/+R7KpBAGbgXBrxdi+w8yYG1kAXS9Ql2SuNe8csp7dwXoQ2iyiq1FTomm98hF5P/5VS9VLx1jT56YIY+bXyzZFokAJy02NG/61Ss677qdFfaQskLgAUJux7R7tD41d56aiFgDC70xxBaalxhYMPtrHWu6fUd7x8tJryw8ikeQFYh/UwdTMzjwz2p5fd3ywBJ39uPHZ6oMvGerTBURS4wcrTUwqEGqEE8tK/XmflLTngv9sX/0pv6hBVMm1TSscgT1NxnMY4TPdbmOhg7lYd/ZRCGTO7MLAyHvRhg5NvhtuGSFARDL7yxFoxuWF3zJg68r6nDdP84vLNtBvfOBEvcnOEBzzkBVHJmDJ/amC2vSQ2bgg07hujZQaz4cLadz9mjd5O8OzHpqwD8zNrEe/8qUUt6iDJ8NrN4dSw+3HF/mWMSIxOJ0UFUsmxYwydSxztLfGvnO+qehyYiyGfox3etbeNmk/6V2i1Us5T9lPfSBDKrxrBIPNaK3sBO4ld5/zG0XsNukY9/3IY4qqlJU9Bidc6Uv4YaYXIJ6sZXD+RQHfiEN0h8pWdCBZWU+C2a9w25HZV1ImBhk3jH/5i7mt4adIrv3T4ccR6cM2WpZFljkWvrKCcMLN38dqGm4Y5Bx0p2yWHZbmB1lbrfvt1fC08k0ZwoqoUXtdYKx+fK/DXc1kleFo9oQv5uBv56z7o4Kv0d06aZ4lOokH/NNIdB4FuTKo0UtfHN6d4lIwSOPv/T/BSHHkGv+bAGIlJE7Qc/S47yo4+a1FAkPvbzYpviEg+QxMlw6ybvpWqEAuA3zjWVgjzuzzLVINTMZRbK4hLoKpk29Ry2JUbcQ8Iizxu67QlHzis7WN8Zhz72r7NlqxRZIqeBjzGUFi/mD968RnEy/F9br1th5OEqbX7qh4eYZT5DJ9gkqZdONhGYUG5EKk/xaVFGw9VpAFDM84zGvZKTaJYYXjGnUZ3jVwqE5xUPKzzAiLozfkhnFRkgMIBjJNAZwFd82qkbROvrysNEdd+bRTIFViqa8KnCKkEgu0RI/PCAcX9GoT0qgHQE+cXwSJX0zZ+bv87CxHldkYt02X/MOGDbC6aZ2MGM0kxcjv35NeLIVqtkJS6lgMMj1YuExre4doh1pfxkQFoPgvsOk2PxtT3JvKCj8GEmqexAfvO2nfZH58xyE6nKCghOKuFMrm3yYHHBl8YXpRvbnZVxMZwzM1/g8CzGhuIJoiLRTrfMm/HR8k55sAcPUbNlezMU1LF+ioIBgvpDG+ixAaEjUEaTFQHpBFoE4nN/sK07dq/0Wmy20nglqVkO0OiA9Pt+/gvHr1ioTQk2Vfy7c8FTlhlpWIrMuTkm5b8ZVV1FvTfPFuHlj0h+6ASYTTkCgPpIe7R52psq86bGz7PHg8FtlbOOPu1hkKB9ySF0drFLhYmFxQRSMrUmzJX1Isprp8FCdE1PQpo6VcPcCzcnRZ7oHk5NmYhea+RJDPttYp/0k4KpJmjkzXWzzvozvhp2umMLANhUh9fZ9lXVHickV1CmlcMJpNQFta8XdLNtbaCTN++2AO6gm7fipPgA4J4fzMDB/2ZUzaTHEVW1TABAkIzNDGSDv+11zaDg8I9rKKnjxOqWpewWk72/gRXnoIo5uPwciNuwVVdQ2hmQpbKhg+y1n3LyxbqxlYeaXH8shSLFcpiVhoNiWOm4I2f7FaenOba31isnFNgAVPumM/DcF4SgtbfBXa+0MUhPJWXVEseXI28a5jPZQ05edS5sEScEvZsTistLJICSSz94jydq9+/qJKXf+/5wmRt6BciK99vqj/spEWC7OgcIvBdwwcTxFxd9JaRyypSBkuFiWAdKMbIhHYlAHYdq3CaFxowZQkzvaribqywmY3EdaDpEfYMfbPPx7m8fUkeHEcNYS4vJa1PGB8uYc3lnJeMvmaGcpZzySmBIzjMPnTuvRBroxjkfSJKp4wOIXMRhTjCprPNZxR9rqRuAzW0HPoqEX+TJc3eKR0a9upR9SGTEtCBuXYD3r3J0OWDEuV/O/lOsNcsoDNv7TBQ/V1kCFiSNRUOnkNYEVHcxhWx1ufbyT6z9LSt82U6CpBCyxyANkYdhzJEog+Tfuo/+NupATO5Hb/8kN9ZxB9d5F9mjl85w6TaUzuyK+K7M6DMVp+41xd5pVLlDW2zvpTByp70dbGwFuOHC7Dnu2Lzn7j2q4zfEC/VTC+wIPh/lDki8Z0peoISlYZjgzdsUlzam2gzsdK+Tk1AdzP4pUsuMV5nvtFzqMdcy6glLwobQ7ev485qSHESmPu5xch0DMzGO2B+oZ6WedMeT13Q230kT6rqrBpRjBLYhJjRWQGIeA//oAWOOdY5DREPpk1ZwiW2sr6ac7sdV9tcgbGMHFSIBxslOBF3MIu+OoiZb3SzzPf3b1BJGv8LIanu4RMhVSzhOdnUlY9yG/gnpWrH3GLHI2by+BRJVutMs924hfxnLXOapkyHsvat0NcVd3e2FSfF4WiqSQwIRbE4eEub+r7LwZ4lbWRhQOV2wVctu9zrf0oRg5IqQyszn5je2y/m75sasWsXgsn8oYaaWPjCFrxqeTAlQsrIbAY054CpctwWulvOs4YFoKrD8ym6L7mZyQ1kNyHQFuDxdTKCsEzhikMx5Q5TRAahKxUFKvxwo/BV+PnUIgyg7P7DJ0Y7LwQqqqLL25TZ25ejhiRMCrEKORpZl6yt9DnuQj7in2x6DirUngF73oj1FA3FX4u/0c9Sb4i3jRKlkMOu26IQ0MKu7PE/UkQBfy0Z0+acbnT46BLwQkqaBEoMMNxiHg4MNcG+WWIE9F80+63n/cywwW9kVatsCy+gxDqrISiFzfS3/Kyy+kQlwEy8QJLUXkEqZXq/6pBTs6tFAC9RGDX2OHRd5A7PMfAf9RLRIB1l/irCaFKJIa+QIPcUAWfYyqgAvEKLTnWWrMLrRqNnyDofvzj18nzpqzZzYbo4GzgZr2+vh+lyB+29TkMW5iiSaiVjbp10e2PIfoZMsmfno+L0GrJ/jy9RVxoknJ1lKR4FMul3fIf6SmwRy4NBhP+AW0dUGtm8NceZDeyiydYy2VyLLGdC9NVEZOMXYV7WTClRBHcyn3Euy4XBJ2L6GiOwr58nm7/a96yP46XTk9X8p12e63BKoY5OfC9o1QBQ2AbXFB6wAblEle+7gxl+U8j1jojW4R5bK5ZkZT2m5QlWKTz8ERL+XqdU98WVS5gCqtTB4/3beEeggUCpcqFtiqO+VkAKoc4Ia0nf9SuDrTlPpMAb0kTpWdMrfMURGZSdlrwfoaihTsm5wabrbSfks0bJSw9dK4iS5hhNYbqznB0ASzc/tjl91J/XlYPI/FE4+wCdaiCRz3pYIDeirjtRzXjf8OsxLHykrayc8KZAiA0rc9iuD0mnaNW6hJlrJB8Me1jwbmKH4yMCvgrVpRlUtJlZZvbrhH2z8MZmU72PQlgQtSJDN4vYfgLvXPjzpjRx20fgwoY4z2cdSbFVj893fwuh/zKgOQrcWW4PXnA7/UY04k/KGc4YpR8TElpXebRKGZBj691735HMoM8N/8I6gpg3/NxlPQNfF7CCclA4jjuDCfwWtpVJUYmdJYWnrCbRbCyKjJfaD83ido3HijRuhcs/FT7eEJwYUA8nVYW+7PFbNeObw9n7A7BGrwnFsfmyHO0EOm4wLIkHtDMQdTpotAKtGSEjixlm2WBDmH+3oB6Tyww9oave1GVl7BpyZTH8x6BU989cWunh9OnCo1+/loMaP+CZOADIeQ++rGs9pSO9yEbPmrYVdoWriQgdq46+6uV1ctWUV61JFYOyc0gU1K00Xc/qWUyweJhOkp71aywionsvIiU7rGRih/gz1hhHCyiLUQI2aTEIY+8xCEZFlsyVns0/4gUBLU2ARqJExVTAQcaNS0PYPSydYBYhusI/ksylU5ARmnJ6K7/X96C5BTbevKo6i73kNShKm9aRvOcRcrZRS/4dR2R8dXq0gSJ8C4aoFdu76XzqFAeA+Gnb/ahGCt8OsGdE6np9AC5u/lE302NO8wClzsJOSUoXkK0t6VhJ6iYT7te91GBat3nUAUR95h2e5bX5FbjgNr/RxkG2Z5yYj2ljIfSBbJSy6ZFVqOdfkzn8H1TX/YXbKC4ETkYo2GNAOGwZh5jHBHlxMOu3b6iZgDiZe94sOH0/14YFcy4vKsbeULM8iyquz7D6yrqswEQyWWNWq8HnVX5gbFWICaM2MmRss98tCte1aNkmrVasgO1rRrZCW1RoOf8SeXih7DW+tNiRM3aKUHiqLHKvre2JDwueGNLzLObEa0dZap9uil+EmrN5pN5raZT2TwTFiMUkM7mcPWpXCybXc2kAWeC+CrsPZWeXZ9x+q1FEAqordSSvCMs77R/KtZodQtcYcdOn4JRrVYE0j+XAZnkZMa36x0BP9uu4UiQvxo7QSAqaDZ4BQq9TbJaZdzBGeORE/d2HBAcqGTbSi9apc69itP8y5j3Bk5noeYylYkVFqgvHX467i3yS/iTnnlxVskZg1fTzbSGasgNT+DY5KHD3OjWlcbXLHHa8ubihqpURMwnI1+in3WRRsAvrCrlH6CaXSW6gDFM6GtfsF5AHhqqn7/ocet0HOx81ZnEaXzLDeIIeGafyeS5lB/EctO1WlO1ewA060OCDmCHesag0WMrQzKsVIK+vC1Lhx+BvwoUpPOvZ2xWN9wiohJeNKnSsvdDtWf8Hy9ni1ckkYnZKm0wc7z9DC4rYY0Gsg1jgkkDSj/pcVGzrQUp/tHuhsxlgj4PJlVqP7RL/W5Jgj6B1Y/MR7OCxbvx35pty7DzTWKXPJUhXJRnTskdAdg2UU42XGcPEk4lR8zLfpITHp49bD3DEkDhCfHEtyn9ahPNL49qKxm5+P596Hp3i4SjBxfdi2vGdM6x1TuZ+gWJ4wcCjHPCjTUt6ETvb0ifZdQPKXlbqNErDPvjm6u10SgEOKKf+CyjDiF5JEHWYLjZL1hP0vCCcBoY+06Gxic9Ud//8mkdxE0TrXao6f7J2L8I9tPspQbS1npIJg0eBVMBZ6BXtfisk4zbScuhCuSU6DBV7awvS2C0K7mEAygEOeYkCFP+0ZorV9bgnBhi+NvL4jAPnEXrSMuWSP91Kl3reUA2pC8I9nlMPbmacryVQWMJ1WYwCADULznk0pnplj/XSHLq6DsSCdmMXkqziQrM3Ah8+yiku1UD4Fq2eZ0WhjCy7ARq2lDAZWatE1m9AONhO7VB65ul9QV/Qqnn6c8FIu+qKGTr9KfcrY4mnZR8VvzK+mRZtgZ7oewiAgEdS/mYiQXRtS14JcBsiBehuK4AzuI0EBVBujL39DG5E28c6swfqBdaxH5ctojpLDTvTxl+bmUgrJbuqjsgKZHRl2C5GJ2ngy4FW++QzQXsP53ukJHjbQwBtYM7MyzF1XoMwPV2BkvyeFtONNh41WN5ysY5nM3pUuyAJckEmb1Cfmo4jGHTq49gd4sXzFUeLSR6QTB0frrpymOzOMK5/OG/Ne74BT4xLM+9h9I35Sw5KD2KLxD2piQHT4RJBGxRhEwH6CoMKrvZVpmBjUaeH4kq9k8Pm9LHzNIJXHLcxdHf0KaHan219dunv1Gerodrsfm98CB7XHwyncSxMPS9UMZyeSJrNnk4rPyKWGl5CSgx2uyKqwEPUlB3a7mZJO088fKp2okRAcpQRgijDcp8+pGi2Cxy+0bWWIkXEithP8xc9300iMeAUP+1KEStw3wHzSfCo+BNHxFxaHybPJCY3B/LCvisqDKJqgdF++KV9JC/5/+vgh5csPlhz/wszQ3D1kGSv1+R4gmdk/q9ObbJ0gtX7bpzy2ptkgxo4Km9oMPy6/JMDltLXjWGZNfMxIBLBLGGUeGmXj72hE4ULgZ+jxsgMc9hDMNRq/MU6i7Bd2qliH7x+qkf/sIGnM8hYSQl5Aar1/GUeaVVduVA8O4O9OkSs7wfWGSZ/EaEehA9M4V4+JGIuIHBPOgBaswLeqfw8SXI/xwnGKYSOTf85aiIGHdg/3XDCBtCiyV7JbcArkbtb9Z0l0fB3n8aGI2XQ9Yui5eT9azzlr6lC2XjOmpOUAOtgt7FOlRNPIrUJD0DYNr5cRWRVsNWI20C2kI4bwoIQfJLuCUvdA7ik3r714KDa56r3v/zIhVr+w9ZzfDprQ4moFTq83MK3pMD9pKFamOK1Q8jDi2VqdpcF7u2/9nSWaZsUx4w59DlIaIIpC3wP3Qt3qWR3y+iEjtoKAol+9Gso19YQj6XiYGZjBjNHPQcoalpK+7ckhmAJtMF1uWFyl8/38uUETQjRMXjcY0uEbAPLk2anyDU2ZA0o2j8mqrdQg5ilJepFjOjU1MWrK4KwqXDcio/UEgn6mZDo5UvJqCYgzdydacRXwXkLCNYfGNNYqj/kbX/qcOTxPgDufr5AYps5QRCQo4TkY9QQbBqV7pfC1hNMbu2gJSSuG1I6mte3fouvv9bR3B7Fflg0QMzH2sAn4dyuq9YStWjQ7vOhjT2fvKEyq7sGXUz3WVUzeeKPQQ8mK2veXP9tVbEDbkmFYcUv4tx8dyN0K8clbFnHZZUpi4n6fTL/v8e3KP8fUcgu5t8EtOs6qzd+eb51cmwGUlMIrKrzNw/cM33hQo4W+4pw1c4XAZme7sa95KN7aKziuJ0WzEKB3Wkwmdrcl5gW327eBr5cPFdo1ECcL7VKqROkYSJV75+iigbj0SiBHmWiMzRrubOaSt2I7kz99CQMPammMfhHJp9HkTsfQ56MZuH1oJu7LEXbyHMpTYvqQGume1EHjZe9kz/bYaRMyiIIGxrOc6oN2cehUadvQlLYenecwxM1uPRyB7jMrP3jaS0ZQgaBjCdrAqmokRg56rhkZ+WETiF0GDDBYDz2eNrvMvVuYee/y7Wk0ru8SlflCN4SSCnV2Gszxq2gAc+5nCxpNYEJYKV+C2H4hcgPgpR9AMZbRrZQy1+6PqgbLewtoCNj44wi+zgyhyFfSfzLRUCwn2L+4MiJHI6qF/oXuURBQzAHW12X7IuqUHzN0BQQuKnBPjjDmfALRbgBWD68gHyH+oTsOuiK0Qoc7pTvU3/Pua/Ss7rhgCC/E+UNdG0VJuaNain5EXKcOLC1mLK7C0p4Sehu5EIwBJ4wsUXWtqMtu5BeTZMtehwphxakARVhL+Ao2fozoCQKQtYTMGFOFLzAJw4kUx3jWW1uTqrkCUnsYkuUAaw4EIuqvQAOrU9NdK7lArMzuMgfganPC9XtlSjXHbaXiApzGAx8BRa35otbLY7EFNoN8fsfQ8SI22RoiWpcMcs2ZM9olC+ko0yVLSpqMn0oWgRj+eOhkJmeZsSNEzjuQytQW0GP4z6kysMk+gwmWHjSf47YXretAG6zWU0VSThx98D/Y4iB47nwLkGBdyGxSRP0Efax5W0XxrgnAnlxYo9BAeG5OACvLIDlyR6snVfWyt8iEL5rTMsR2wGw7TzHv8V/AzE0DikC1rtqrco98Q7goBAI7zvSvD6YuXLrP7gKeGbvU0h3QLLnV2xlcRuYKueVNma9wfZQPupRTfFXphnXwiQeo86JFI0nJE2czfExvoqi+3lsad35F70hCQYhpzqWa69XDOIbWDABsbywa+ioEgp7/NGVmBO8VDv+PGLdbyu9rnWS5cFe8oP6aMH2aWapm9N9avfMCiQdRiq/W8Wa+4A0np714uknmMZUYOLBx4zGEF2YsG6BRC2WTWAAdu5RNto1wYkmj8LbHgww0Ui7e3QJeCRisuNnpQJpUEEsAoIYULz8tNQVcEMywBQavv0CDe+BtqlzGK+6+9XRQfePtPfVjFf3bLY/q2zoDcq7GwMLzZXPiSrteTRpaccEdGlVVryb2VknDhLtH2e9eewVmT0kD5KCOmw4AZ7ziS2TwfbOf19tzyV9o8TDvTlRfZKQ0Tf/APIe4CK8eshzL3uQYjvpnU+WiQ1xO0jOlDEg5kxQ0tK+dw5muZ+Jq25zFxb84XmfoAygZsx6WOPGB4Sc7ZOzPAOSboLpk9NKCR9l6vAXMEbwOWRh2gYyhOAAInMorF/1kdGqwaNJkSGKB36JaCJHl5uqf0GxQo4N8HcopP7QNTtjAY9IWcdXUIoydi8w3uUmLI734ge6l2hdbUjZx9NpWMez9fLG1jPLgKdRDMuaMndtW4AhR6FwEua4NYs/Bqb03UtrsbR79/7f+Tj4p1ljh3z7hvGAszPmxYeA2kOmeQBSYEw9CI/jeI6x12FqcrObQ/Gtl1u+MyviThK9PNge3e6EjdEgW6UnaZbjAU+pvLcpn5Tev1N/PWPH+fU5iHQDdIbmxMmPpKBVeOHmb9Rjdk5lzni6LpOJLC03RP2Wi1phSqyEhZ7EvGyRQGqtobpg4NzquSTbSyZgXUIpLmOQznW4OyPam1ag4mpBpX+4fEiLb+Oo7/5cWbw1hpSoacUNK6pJcmFD2wqWWVb6f+G8LkrVJzI5UIMRbOktqam7SR6fvp4SjjTcHnI9M4wJLFj9ciWBrY1TeqR0xqDRrNcbdIqowGiui1DXLoAIDfRIyGuPv6mg2uMJq4oalHzVNxkRm6wS+DJr3nTStI09smKMqZicRfsuDvSzEiqdxRxT2CK/ILBzQ2SEU5VWhfDiqqsjfbqPEhIR+hv+oCrTgT5pUAZ55NBUvm6zZ1C7IKnHyHlbVC0k3WkxHMjoxsr+NTeoF9kJ9Aw7Jk9I0ZNMrk0/N2OjSQmYVzbo+umFFwU71NWyIwnkuiuHNQ7RmwqSofmk7dgHXTc74O+xPxW/92YVfA56kBDoLlqNQ/VDy0dRUME1qvuXM6M5IdQKa8SVoHuSGO3KQ9NxBIFmIoxZSpMh2xn8Aoru2xtuuMOdPIrlHqZ9Nz2DxsAv+g9RdljGIXIbN5ptC+zKKk7WfkVq+0Z4EG4askMfngDSyXvHJo9IEh/AO6Eiv89cz/dxDrutNR6bnJ3NNCT7Z1Hv6EoQCGwZDo1nf3zSuDFD67OukTUgPgSWaNJsOKfeyxWQ1FQ4WQq7tTG7mY04h63R5RTNbT3AODG/V3sddHIX5dBgxNp77yYQ/KQulnwpEjD2RqYex7HKjLRj2xrqxu37Ut0qXA2UElB8by2BULluxhMpMn8jqoUVX9RHOxp2mJfp6QwdSXcNRfXyncLnQ3EQsJ4LALEELduTVJvEOCassfczYPFGmtaOA4lWMJ3AgtmxGX17DemWQcSNGaTzj/t9UX+wX8wZ/G2US94DQLNQ6o3vhxX0fffQohqeVD/BxUm6VkOTGCemznpOeax3LfnGx0aSXJsCOay3bLQhtIKx8MvRyqs/1SttwX+ynXO3lxlOYbbVvFRoHyAA1dupdPc93xK60ATo++t8jWTYUfY2RCLY5ZRknVuqoPqDFZDnRugawkOo9Divj+MtXa3C6ProBq6aRxoKDfENRs/rPcNV9AuQ59yqI12SLRYEUhM4r5pioZ+vPMMIyJNkvDU7/8xIX3P+YySwtBrnvXHWvRezN+L6us6igLRA1endh3anBWI+Jw4fZml4hx7rn4YpibhCCG+AcwD/hbryf2DabPcnoottwwyrXzla65ccPEfvAsIXrXI9Boo9CyIPs/qR7vVLx0zOooNIRbfTgip/Kl0uL2uPgOloQhKJBK/djyRRFENXoLiy9Y6JLbVmpZIdfVwJCz3s0aCZU+uN0TyTZvwTbueu5Kpe5v2ZkvmMd9JwPvYZ/cmmz4sgjf8j7d7T++P19u40H5PUd211ankXkVqgBw6Gsc7HXyMgCXPY8FgfDgBrSRH3gQ7nVjN0KHhZBDj7Ecv3sGn8k1UyKvC9McBjaTYuQH0MyLzBonNOHO7s/6nSnNCku3bSS5xq+PEM1FWQfiWSF1YTxhz6TrfuGs+TxRSwReb2M6FTIEuGLtzG13NlxiQN8W4zrgHz1cXoo17vvFyhHKvCJupxxc5sN6h1WdEOGGS/fNpfjJMJ7CjB/QWRl0t0KE8lIW7aDBR2hrVIH/6mmmt1gE1D/rQLB8yiMMuQYJ4gIdpJnldM1XBWYeap6A8pnITERbCZufpIeJuXNUgccBqd0pviIDSfF79GRRwJogpu/DsjNC5r00ZQW5n1MgGSuZW2i3puOgWkwbh+sb97/S/0LUhb6mgdvEF/dWL/AmDqxUn3VXh3CjQmmqzuTvtD9SYlosTqKSIwtwwnWryhdVD8odJPwDA8Y8oiB/HX1jYw1E/hVrh7JsIk8vMTqHFM38ONzLynIowI/rkTfUieOLQL3AJ1MV1cTEelEs9XHHR/dwiV1Q7ipkY7cfgnIsqWxPHtBw4ZZ17/8dztJ4w0fwWyJo7aC/3BqWHMgFxfDgOh18XLDQBIdFjc+3ShTrKLxxnypChT4C8P4zJ4xWdk5I9WvkhBz8nqEo0qgzeCSA32ZVkOO9MdO3mjBIGn/2G0Oa14AXNq714xY1Gdc9dcADNHJlFsIdwHMnoWc9vr1SCb0DtK+VQ0SmLMcUuUrXECuC6it9v1JPwv4t5jcqK1E74CpKzmXxQuiiWFHJt1WYAd/QQ9M+NsEXjTz6BS+XSMlAVIefPjjPWTMpIpTOeSdemQY5iVAR0D1lzelZk8dXKcmF7jKesmkKBmIzt2ZvQ9v2WuqLhGM7USMmwqQJM11eTe6plRHTXhfCxLhC6oPNxRSa13OW731vANckBQT+0jiZJa5cBRKo1QXgObQpf6BtmGNnbKRzbfckEq6Q46pvGVbdwdAXKcz6Dfbp07ddt9/VTe9qty8Ss/oL63mFUyP9HHYuflrZbc+Ai4QLlUbkya1YCzFE+HIade11UpQk0YF/rlxK9532AMfHJzXgKxcsdje4CFfigoJfykSANPD2HLNotwR+dzuTJS5uX1RSpC8PvotqYcn+JoAMkatBHbqk5q1ZWsREEe+EQ38I91vNtKu+DlWbt1eTN2/+d+w1UPKVjMs9vlsDcuTs6zpqlTW1E8uUmhADS3uBz/iKvCuPeyLSA00+pvcBbb+MRuzut1yhBGt5TrOXy1VFvrvDixMgNNYrjGKLQMrK4EKf2HDglnLXA77MOGro8NskzVqC5syO0hih1p+Y6VEpYWmEpBAP31n0AUsVD+gwyzPTHb/r6/X4WATX6sr2K+UrYt7ktXndsQxMXe3h1AZ77DWIlsFszKPYXJgmOM8Upr9Co5PiqpiBnGqCH307tNo5AGNVzjsK3/QrW+oA/lvY7mAW1g7HLp0mrrRw8S/bZrALZq1+xDBYcxkItmBGQR+2SxuJj5ctwxbmX9ujMxBHWAqCZln5W41RzfPI5JOgHT8xXz3YFxy+NFMrd6rlent8dlIIDxot9v6IqEVF3ogMkV1Lr1gHXfqwMmF8y6Ahyb9ZVKLCnjvpab1JVPpG4W9Lx7/gLrE0kd6MIxKcFyLWSVtEfcPZVMdSHVSvt3Svw52uXdmh1xKbEfRxKpM+y33udKRlM/eNFGIkuozqMNi2EREmCbqdmRZZuc4VnRTZWKrnEJ6mPpW3Y6PkppKvZZe+RBfSNOpVokaXciceLI68/E0YIqTIPCiugHXc2YXwiqlmFoZpo0n9resNIQfTCNfQLiq+6K7PR9o0UxuN+GghRlIkGXJYuDqQnXL/GjJEah2gmMrTGyJKUQr8sz8ViZ5bn/vfJL3m0QvjaPTdDpE/nyBnkuOK+zHyQCEC3kMwgyuM4Zqn2MeGkO1vh+wEzRzLPBV2aftipPkJxcgr9TwdFPJktNh6R4QxQm5nxpKsD1+fN1ihvKqkr8KB4eP1Dglzlb1LA912926pnH51b97ZGrkFo6dqnjoVzbB5YRvexEcsCBn+f+yLlzlpSjvGE2+EtMPAUaFMcdMTc3mBzsnqP+CMmGtYunDDqOEZJpWCVydAIYX9obGIh+SHSJeVCFpD7jA6k7zvCZHDxBtUxsndcX88as9GaZ2PF1q/+EFn7gCQLPdZZyDeiIxAjbjpRNgkeUk6LVZz1Fiekx/E2VcxNWkIFBU7A2t8oFkRgv3IyOw4yujMumUAqtrStFqnR5vBU0o6V2aV8+rQEIRJyUy2YgMVWhhGHNd65cZMhJZizBOgcL9ebrO1C9Qixgnzz8sUA7GsnnLdHemF8gWCdtVm1D+zKNepKiYksKG6W+VYuU9/89iAs42O2vDBrTfOu5bAQ+NZncEVNsJOjVo08eIF6q1nhgAI0TImRLyxSkw3xgSybesnZVJwwFh8N/BFDmoMEw5GZsjnPiwMNcJwFlTySnsL5YEA6ePAnYBlJYEGcPGUMHv8VGhSBc4tupVKEEVKQVeXCWRRi9GKAIe+2R3P04cnEBN4In/84rFKI1hP0G5pdORNf0phWQgmGG13rF3oAAvhfqWytj4orkGs47PZRchZuzUeIAxLRvh+2qvio23FVUs+1LaFdDiAiakYCwAOUPXVXN5aTEikp12m/2BnUNk/EiFuKLf/8WKmtdy8SPmfp9NQP3vAO5KQnVrp2ZeNg4FDHz7f3tCrFw9j8M9wlXFxYxWrm66RfsWzlO55KrLGvDPwks1JybMUAGd7tQ21Y3vb+SFdLVYVKAsnGKaZgsIKAdOyIYq6TG2ZxcCkGyol+BVVjIL0scgpvh++73L44UxB5Gh57D7Juup79xxjVsdGh0WPFPeine9xYkwrOGnXAk3GNvvWgcIBKvo0xp8JDBpG26Riy6jQNlInmVUMspoorQ1OeLD8jdZoDhOcYD0mK2R80EJXrlsNyOYiuvCuMUaRHnrDbWeGoi5d4RIzf+vcfXqNjzLBFkjiZLoSUoZYNmHVP/6Xxug9q9al0S0OY68vC5uRM1BTWmDpkyZ+Tol28Rx6Sib6BJKupY2pznLAY6sgtrBtffW0IvkXIxMdtP8wyOBPmcs1osUHSQHAJoqheE2egCGZIsU0lIbC08l5zvP7ToGJddIgqpr6IXyjXZGl2s/V5uRfPN2Kgab7p/xUgnI6yfHeSSLWkMeqfg/iJSwuJL7/2OMLlTXgtZTlqyiim54jcZTNOv9Z4lh2QHlIfloDJAp2PgLHo/wv2mIbJhOw+89jqh4rBXNE+noADn9g3lLlfsoWDaVXtOLwjrM57G/i18mlk2OJ3huuN8oRStrqKfcb6KQaocZsfN4BPY8hopNCuOt6sqkHRCwsW2IWxwlSa8SaKeZsQkXAmeWVIqFrstzukbjYcr+swYMwOLXPxZH00uMLSurXT3MMRn6ccci/KsgKKrSqw1XTjE9dqtfHL9bvI+wWNka/09qtz/Ou4ZGHSK1w3GsWXktxNjkMbrOm6Wy3VAXanR32qd3lidUCLCa68fbBbCICdvMM5RKSh9HP81IAZJ1Bg0Yl/oc8Gq6s0vIYTrmvGKurrGyA//wTdek17Fi+vV+fOk7ejod8QL5UNUoFwQsEAKnzVyQl5RcC/gFAw+nTaKX229UvXrv+mBdeWxUUuDygxZwxineZLV9XeBPwQiiR4f2fmqjM0G/zz6+k3cZSebUwN3Op1oPUnc3aBiu0SYJIwhUCxRTwyvuNxBf/p+nhZsjO+gwlcFHlOkZD2EY6zQ6GX1FpuMkqtSojffD8uf5B5O8tMopgUHySH0RH+9EE3AL/LekV2mY7FxR/zjw+orFAAb38ENG39ESNUhuibIyq30ZtzBDyN0Vh1A1hYKfpS5oQl/SmhtnaeTY8KFbUsTFJJezshk00zReZ1I8jxhcICEhcCQsJdRtuIzV22iFcdCq0B6aGN696BhjldnNhNudmCPbiH8AM0HkbnUg0aOQFlwCZM/2hsaUVI7afQ6H+3juufgPzL6ITCCYjkjqOu3QuORhOii0wu4OmLviVmuhsmOm56EjKgtigPXjuKsL9ifXzMhscnRKJh9SBwjasgYvQ6CBAq8VHKSb83ECZIyMuF0Ly+netCmYO/fVmQI3ch/atkbyTPwQ1iV86PC+YPDbK38grlHeWFATNrek5YYKLwmEWwEmQCj+XnYMhPj74HuCpZ7TXURUnjqV0dxeBJnSLQExlvGd+AQZeJMbSH8DWXjL/pPvFrISOJCuTA4wjZ4pAVANrJW154TW/v0D5nKP26Y9DxpuPKz8eKVEXTOetTvhjnraVz0qA2PPA5C9s4XC0l3CwpHPhqnLT3Ed8FJokozmwaqP3x5OUM2fhAelZ+VGzBuzMVoOBYdHvTDciQyniTdlSfXKzvWqEes3Wvc6ZBYlygKdhBR7Uf7zmeEZGi/EHbMlZo98uA76likX5OyTNbb5qpfk2xr6GQhHfMphm+0h7nGQ+PZpRL9yEQOJFxE13sJzO1nPTpTJeKGmDDXi/gyAZ13viMJN3N8pJVvHAfXNIBVF771qpYpWAOsjbNS1DVvbxbiGIc1gB7kRYDT7oyoTNa3W9ETZYtv4cYJAEmel0f0Z4oZhgZxWMfcBdo/sVCP/9EwAJzpssTceD0qMAThyf6NPYbvuRsdeMZ15BVT629hZLlzeDnpWDbym+5ShJZ6ImdQBbV3vOfoGjRGNWfSwQytC2MFQ3/CVBa1s3UReCZLTGpICTKZbPP+tv7VV+Z/KWMZ/OLeVIQqf0hBtIp6PTW18EJQGzvobC2rxr2qQe876BvpyGajjQaOn/1dH6vL9t86LP3oDkT+ttoFAIYYqBXbYZiWEFteCwNWFh+cI2+ktIqobrQGaHREjP69Lzw6l86aN81Tun5PUDUasj6LA8F7IsHQpP1LOLSgRTG6LLFzku7jGq6pv2PBCy190DqbPW5OZ5EbQoumocXqP/RuVLDMc5NXqNN+TXljXIqlPRC1kZF+w4I9avjG28KLWGQskMmFtVFML50+2jrGkpNWt1q/z9Qh/SRhGtecBWtqLCVUlYivsTGEyxshiozH1LQmTxr8RlFMGGqGPezv6Zqfy7Z+lMWeMSCtfwr/Ta9C9HNJkLuLyw51zBT5hyABG7FAcphZ/X9lyoTOD/BnCENiNlY7M/AMqlAGq1r25NettLAdNv1K6JJpOefBT8/hpbwySesVS31ePoR+sq300rrwt/ytJ/3Z1zkhg3hw6rtqqE0C5QQhE9xJiS7o2J3EMCjOld4rPnjUmCwg/PhAsJ+2+l0YIC4gPRU3hdpElM6MCht93FDK6L5P7uRlqFZsfXa9s3cU5AZpLsMrYd2UsSRpdOHOUXgAkD+Gg2JTRhDHVngansf7Kq06hhn3zyYsl5aR4sH75Hpr8Gb6tX+YR0EZQmRZQle7uvPgTmvuTegPZh5hEKp/4PtWH/JotqDJXGFA8t642A3r+9g7S9AAFz4dgxQ4IkaTJPzIQBRz8VPNbkUvHXRiwnjV01zxpLZ329CTd0qIyxTNSOfVcLAsDUujMi7wX8BNIb5LndG1d9dSe8oo3B62xb66B8l18hVp/QyOcsPw0iTvr5hw/7HsWVvzBI8hwv9fIsaulQxdMJfGuLXOxMulCRtWu+zmNZKFtct1s7pwhZQ0EHbk0xOq/M9/JzD4MJ0hY0IrB1JxaHv4NDToHFmke08vExDF3vx6Q3n9Gn9sHUk7T5ba0PvDgJZ9sU465U1FFVWbRmhvlBp5296BMmlwXPHoSFBuEdZqd8pPw4zvuzJ6kbtIWm12usugvY+ZiZNtIBH/lVBx/m3S5UWw/LXAfE4efeuDb9O3AUsTir0lAauwx5l8QI3CUOuUMZqkUv6BXVDXAZXD2QXQcrqQhvMNZIpNDCUBypKAizIzyZKWRnF9GTAQBg81C1ylnsoKIyeoZOUUutpo5gqrGkkOvRmYexm6mpnaEnVRPouvxWHqTVc3vXzWd3ckA+eWVaSuXfSFgK3TYDFNfVp8mAT7SaFiPviikcPeqO4T1jfQ1+1kAH8c4oP9cveUotph9cErqbb1PaRHo1vfncVfJvD/LIzPeKpGVzBfeGErF2BszhDTFCxpCIzhT01h9Ux+tWJCz267tcjSEo8rIm8M04Gp/6WZA8hBdU+p5oIsiGERalGGEawDroyPfrY4cVlk6YQymgFtTxnPQWGz4o/6yjlVhGWqxQI/Bcld/ghmHc/Nhu+v+erJDnGv7+FTNhrw0erSgKp/3NFqNQrpcwq1G+96Ecz6cZkB1xhTPoCJiytHsIlntlZhFe/H32mcd0O8eLr1s1yI2KSkxm2NZlFJ/VqYol3mDs5t1R6g4Onzjj06u0q7HuY5MjhOZ8TOlSFpdM5X/K18kS9vX09Nhtgo3f0AwVE6+mj3U83V3GJ3hgGVQmNuD/cZakTkgsBNtNFCfePSCv/8Str313SaH6hMKIrHPikobWu4q+8OH9a7tYPhYvDlOTrsyFvJp34EJ0mUcGXdPTDD9VdkxllLifiJ6LhF4zc8/0nKC74MUedVtXrucPfTiaJs3kMR/nNaUaK40Bwe9usZbuxRxlPIu6JZkg0+4AUZf9LgVvs1kdNwRCF2ZucDZ9HSCcPTx13P2gfrmWZQy2BxFGNVnbTNoM1smuEA5sPVy+ZrxuDP/A8TRdcjBRdEdPBtSapQZT9VwkPO9+7qd1ZrV/GpJdXtyQpph9bf+nR6cMit3GupDD5eHo32n9VIQV5tqkYN2fDPeurmCh+dnsOsJCIRpYOlEFZYiS4O5Osn31krLmqRh6/BLsSGDkHzJR9HD4NMOqgyMYOn0lXTTQtLEWtIGb3Q4SVSfMMOEMIzAw4+JZkblHxX9AuddIrVej98kjFbvKEcZnUfAIo4/nJG4Q7sZ0N3NVO5oLLtRZYvM34vUrj/7vyEJAvgVWepnhz4UPcWF1xa9PIZNMq1tiG6eLG8zCddHPKIDFEru6DsAnHgyz8CmBTRLUGnsRACGEsjvnsdJ6M4iI+D0KEeMpSXvdAPaOL4w5bfqsN6YPmnm60FVJfUCE46w0WernAU1Yru0Zxr6Sr/XqqUt4rr4O6UNnBRynx05gVNhTISA1OOoJUFLwPFJQZBXk2PwaYZsKSYBDDNUByNeDvMrr/Vzc5siN7GFEGMNEBOUqGv9FeViRtqN4vrOC51pM74gVXx7xhXnXQKDKRAStZqUPsvfGLMsFOoedJkQd8DSRmlLTOZqw/J6rlqe3B09eIZSuxirfHQOWZb+f+cCH2KK1F7wSy0QyP+06CitTEMVG4oF0XHHewWtcbTR4sO8JbpQ5uRv+rp6j9gGxeu4cX/z3Wl6/0r7fBUmPoLn5ErjSCMfnn4dU/W7eepRYQnKmOBCK0aN3LfeIGQlOLOdCTChDNM5tjC9jXN7Vt3mPLXqEX6rH4AlKgw8UhlSZyZgYrsyxDv/07VP5w1mNDxYwGtTFcHhh145MyhOG1oXuQJ0oSbukomW2sacbMqm7nHUJgmNPoqyOOnegn8os418R0ZmVX9lizGo4ekusv4/nOZjucUKzQdl68PZpINuQPxfc/NoIDWOrXHkZTWl3/HGRHbQ0IUGVgkCcIqcyJb1/kBcLeK7ImWy5kvTsyMgVVUecjR9mMjY8dgmo6qz8jD9PVnheIpOs80WTa8jfS5lpMZnbBOrc6X3y8HlDQnGUDSA+c5ksapMHKoM5eCND6ZYA7WVf6WW3Z6cFhByyJBKpCdjXek/3A6nIlVFFukJNvuFJlZ2CmEd8aE3OE1oGNlxvUTFVDjVOe6evwUErPIYttHCc4nrKEEwxm7vR+ltVXWyOdym9By3YOxgoQGnfXRaIIe6g/hrtZcp/zJeQO+zABv+qqXdsxb9XF5TRWyrkfGruUlFsg0UsPnrfTBpOlc1LVFL195BsOI6sdYn6NxycLdUB86JZfTE74SV6i/oQ3xFuECcrg6+4ZSLRTaJcNyoh2fErYpSgw3hDPVBP0dr305tbOesx07Cb4QhG4sKy3AYSRd9UFAdO5cR4FKZ9GydwKBHU4v1WQDjeSmjti63KLzU9kHPhgmC2kCNIUzNDSpyD5DsadEvnJSJTQI1w8ZWWiopjhE5/r24HFl4C3Xx1btwUtqqfpBjpGfnHR97Jcn/7kSnc0jKYo/5LLi3ZFoauUSeVV+lgzEHXZcEln95/rh6pGYVoAS9dEMizBspeOrBrjaWOzd3q8xZGG78JVtPd+4b9GT/xTi0TbAO5PjNLhpFQCopIedK2rj95tGPmerfhnOAMnzHWUp3apXaajBeH+P6t7IVDb3m6+ziydMzPCUHFPrnjAmM+4U9X0B7o3fGfZVKwknzyRQyLFmOH1LTpTPN6xzaD7T/1ANILTmVm5BuXYLuvMFNvwrjb8bqNb22fpIv9aREFfefksArzYeTn77fYSIHT/oD6pXpQ/+z4Kr+lH38jECgOnzSo1kQ/5GsZ5mv/xhJzJ+3BTGGbJfT/QG+B2hVat6LCQatu+wn0tx4VV4uUSfUFKUVutfWfoJIq/jVwrgQ/pCVVMErO+5TF4gGBNh232OU48yr3VrVq8U3dTa0+/2fQNotgxvjNCsaM23jLMDSxOG4PMfA0g1eR6QHGMaC9oClSRa1JpVkKZKDOIlgYLHZsgzvuHu8KnaPrNtvwjeqzWPSvaLAnLHWcPGqvJsWcRxOeoGMAbWdF1l7hoFqq4GOZx4IIHQtMzydVHqZDCnSatM3/n0S85AFY05wHIXnKW6iPNsOU4wqTE62v5ImFf+U1mkCqhTOc+3StIq4uaX4yRSznx9iX15xsOvQfp5FyuGrpXNZG002KKk9377uSldOJfVzQvToosR+zRnKjp8RYbKaXzr9n4kPYBFRvNYdHTvqLAiOBmU7ZqmOwkNL73uT3yQtmTMNlIzlVKEL26+eBeKJWG7pqYmSNTMn7Bh10N7eR6QGtF6PnZLVboKPZRBfmR61R7STFG9cLujRP+Rwp4RF9geB4mtbs2mFQlUXys/uy/VtKLQKoYQUqaHv+lcTZpQnkmL2DjCJBt+LHVTZ1kZ5TVKlZ29cGOGiMpEOndXRvZY8WL6bXn5F1SR/dvz1+jRfiqMir9EiVRyNH4vzj7Rmc8FjB+paWONIt5TqZrueVHntjUu3KrgacTw8ia7Pe9oWVW4KTK21CNcyLaS1JKGo5lbhIjQs6ek11FpAHu/0tvXMToUCrt4SJNcXbzjEWjilVZBQbf/6KuNaKucFPcizieuXHve6sb3mRN/5fP6MDnOlLBDimZnsgfxck3pNEyVIhUkA0SzbbWgNtYMFFYCbaUcCmwKWoAX9hvG/GsRldTtiDAPmoc+eDN3OGjaWcNXfrG84dY6CWKz1eOxJ+/85/dP0kWlY7BXKt8ut7Icl4LMNw2wrNCS4EnonHROqr3t0DRQ4s9SEtAhxhzBn0ODtFAU4niQ2eq3kOPmzHC+vnUPGf5xiih08MRDuQH5hz3kLL7yASoHHrp2YIZF8jpa5helkw9/xdhnF8uBZng0Hf6QRpfXshzOFCsRieTFW6hsk2xR4Q0JLWVewRK2QEZoszNXBWjFwBzegPf2LSfzukJ56HW7wx2Ckhnlkpu5q3wdst2QZEI9w/hy3yNyrjfgphYuGV0AjIuDihiqNE0g7jbvFEbu2GkovOk4I80NC3o9APVCZ4KrPkj/D4Oj/SHy2zULXfpV7J7yj9Ohh6BGxADfCVivPLbcwaMFMeQVJdo1V/Dq0Ll0jsdHbK+Q1U2qwDGK2ksWGyuT8VtxQmj5Xt5mK6oYboSHcJJAD/n3hyEkE/rQpm6iC13G+wDFcxJfvMCg6r6VenkSumzkLbXSQEO0W9r5K73QpsA16nZPRibapCPU7Z8zFFm45lzv05WX4yGC/Tq2JaUvHQ6Zt0nclv3M+085rq2gad4uikOdo4PfD9JK/uizZAQedzRLg5ChqC+LTXj3e0KFCX5nfoZSHbn/MhLGA/x+qkSpYuAeVhzfEZKH9hgXvrTQE4X6TYjjAv+svC3YpTHzdicW536fGOUh2CrzmYtXse1OF3PtgMEWn5zs+vjERTAFpaA7o6MwgvmDCO08H+9n+aP60ZKx/ufdIymqPzLsj2h+vzJgqzX4XuV34grnp3MUtiPx8SfA+RyHHYxMr210k7mVccjkVCt4LQQRgNStzE8xEH0dBOSvOxZ9ujnB0isWioVvAeAzUyR7TFpVok58h/V08AVq2yQAuFN/VJRSan055HQ7sMHX+aFOdar1wuPxEF3AX+hFIZ5SRKKjJ060/eYSqQH/M4ViYTle2tV4QOeVuvZYHWvqlDTRwHYbrq2C56ZUrmigUvbOQJzQElcxnFbWVy4DnbfLqfsQkGSq0TRqoAPTDPryxT4iJDKd1DQ3R/OO16wbd/hgvsbwj2DW53fvChWLJ0zXvvZjridkqwrUZPu5KCcc+J8gKCGAByDGndl0yQmyIgpxkDp4jfh5UX2D0tAyZKBVSAYq1LxWmnc7P5gu6jmvh17sorNleSu9/b2Kbfzx9dZv23CBuRbFa3r/X9E3Sye1TpZzyVvG8Jti4knye7xGsGdq27BnhEEv+R3K6kyqVASuQJ+VZJf2PG2rRXnvDJAfi1hDg8ZnPwESzw6zaB6rX6irhMvzGuCBeXQ4eXJI1jNEjCUz8Rtse8xy30FQaAqVM9gVBYuJ/5DE1BCSArUG9c5B60mqUSKtKpMKphOGCRUbTh/+5o3K9ysZ6GFkxibfYCHUv3Bdf3dw+bfwGmZTIU6niRlEIX2M899ObYDTswUznXRjTEPbsQX8Qn8D9v+ZpMuckKEP6MJQtODaekItjG4TFIGa78Q8a2rAXkp6ardh+d4upMDAvGKPJbz6TYBQDzm+HlZ5nPpzT0QrP1P2IeFQQpAAJ9TmQBogEf5h5jhCWZhKL3O3qMRfEQARRXMCzJOmC46jtOcjtunlnG5xM/85W2+12L1n3fABmsyxvkJl7+ryrBj2O9ZhARPLtKTZQUZSHeEFutI1MQxn79zQgWR/hKgA9j7x3E8rZCkLaK8e+dGJAjYkkBzrvTToO8kQe1SLxpAL53yt0ud9kYNy0s0NH+taCVy83gnr30IVI1aMTul0q7YBRtAYFVNj98IZtZPR+5GtyyxJRUUnHNjQPjcNfK66Lw77Y1f6ZdXfTRnKjFHUbUnrEy5MJUXZjPLJjK/rjH2/SBq6tJ7lLTfGaCKfeeJUn8QQI3Dk4ei/q+plSkYWx3cX9bM0OLQG3zsBy7ZQUlOMyseOtxpEqExYkUsTSBtpFqmAD6zpvtnNd41z/MksIxGpR00CrdcGJ0DJdJ26U9bjThOy9W7azaeUXiSEDksFDrF5FOoeLWxMP9wJbkeABUQN6frHfDWkyivPZBxDAtvfKDN2kBqht0atRZLZGN1PNCdglu/57C3rBJdbM3KZmiRrQAcbvd+sEzC7HCVT6mIaSy7xUHPWrPjX5lYF+NBeG+fLKNF7ETLPR3gm1nhJXpgx+VORdyd+5tY2LCvP+89WAOGyGZLGawdxGbg4rhiQDnX3YIYW7Ujda/ARH38ROciqT48MRIgxr4e8R3mQi2lyIIt5j4MBrFDhg3Mcndf+DrD6WNwCTUaFS9XRnLd0lWgesZgR2YEpfbIbY26qAAEFu8naJ14zSdWhPKRLIFk0ZR1vjF/c1ZMPHpcpMp0mJBKZPkoOmsvDBhl9IW7Tp5IQexJjTlSp+0FKhRhyedzbybmu/ISrmdy3wwCJConPRYYk/zdZZzTtHguTpK7UBmhv1FH9fnqk6mZDX9J0dntEl0mOOUCtKzyjW1J0Jz7+NSmBV/yJr6j2+RkKmuPY76wrydW8kNRquxLgbdy1z/h70Bp83LNy6l6axhxx8Hyfri22YMy4v6SJ7w9E09Z992XEJ1LC4U1J0K1OCpks+EtM/nETv2+ePEZKYXNDa8XbRc02aKtiJNtyvxAjzc/91Gx8YY3S0yZp4lAiIHyrv0ZUwe4bA/TEId5Mubymyf8QcWqgeaAmnIUJHrk5OU0iCXfwzp4GnxDfq22f3IpCzT2ngL5IGL/dVeLTfM+Hk7r0Y8dKTuF43gaK3P7XOhlla05+DKDtMM3yZe8b516r0Npx4YKyOwKoznPx6HgCZEX5ZGL8roqqb64DRv4J11uF5i1xzNbMdPtdwVsJLVUAiF7byySeFjZdFd6Ei4mSjoEgKeLRawpqaBztKI3BZcQtlwUWgXsCGkN5kUXMyToduuWB0fhUXcalvuxFJmv2952oVPiNx8u0aD9HxFdosK+H1dJxTofi328SPJeldfyRjSDpKAQkdSXacx42ZuoGdfGtDr0m6swVpRSqV923t971koQVd/weNQ5F2xRhvGU3+Q0/lkn6K7xtjhcwxtzp9RH8C9KrU4kJaaa8v4vqk4JqZ+6Jtsh/PeaObuGP0v4ePmCX6UVDwngamDsPKsjR4cplajNh4veefr9uUDIT/8TkfxszleZYmC5gjJ+G3+QJ9KBSbsHhe20jkgHZo3ScBn7TidJ/Dd68niz1csIGXoordnOUsSK/q3uDkPsxTGuJSJA6NWjPsAd2JFEgex3xuTYZRB2841xJqo9ieLCU+DO0aDEpfoOsg7kT8J3R/97GhQ3+fBdiIfqrKzYWvH6CJOGBrb7oNy1JsnkupPE4/rj7yKnQQx45IaiiLS2GQm1MHkpz1mqbEo2tPjYk4Lhr4Py87dRwLP3McwIyjoQmq9N8w7BOEaG9ALsraAk7LRZ+j9pJIlN9GZDTwzUt+m4jB0k6oW/A5U1KK2OtbJ9XibAsz59Y7ic873AZtDR9DciZSr50tMxamv9NCK2N4vO0GRQ0jBFWYURkviMp7YPZvTAwE/GlON4XZ6LVJllQhH8lAgfOhGNQtN/8kg+fn5QHbC7OkwXRkalJqrDeEqEk7hhIrymrd9C4Q0e3wz3B/fdG6p8bq4WRcBiYBX7qeeo5KWL3bkK7QHEyt+6zLFjFqiuVd8igyfppZpPYsUzvBwbiZAz2kJc+cx9KSvctS0KryB1oVA7qhSO9ricBYvQJLKjMxcLav9/c6hRrDodv66JR7fNbQ5hLpp8EQbgIB8jBiN6nXt749+K4pqAY30P/6sEKlq8BPHSHa5PE+ROB31BYZpN0354zQAY5xIy7qUqU/cEVRWj5NgPjiRuZSz3nBfUOG4pkLwzN7z2RCagHku0dI1sgfY0/D7KJDacuAINdNNY6kzvIBrA4xjNjBKm3NmqwY8FfAq+kevKauayhtkeAu/D67QbSzrShJCfsQpgWFJ7CzhHwTDXVPl6vKZeyUgdH0PX0i9eoC7tPBe9ZpqJ3SFJCVtaSwgySQRmMOw3D5x834d54BGktDC59s7bnyLkqb5pjxphCVI8MF06XqM4sqw2qU8zhRjpW0TM+pxlBL1jrtAR6/Wa5r7Mus/FY2hS2otkZfUbBvjlRKG2SXOACYW+pOpy0jAoHe+dvDE9jRCUZtaP9cyb8RBDaZOyUhtwyZMh5akItd0CgQPsJKVuekAFPryQ7e8NTPdCee3iKawnh35IeP2KETNIMSGgbdVuGkrTZmnHFCn4UUbiF5cV3tEv6h2b/FrEwz5iCZMXt0n+kNXHLLGE8qUYRLsKTZPFYSNSLPn3bbgck5tpnPNOJlBBetCb7p7TtLHfNoZEwg6KFm50VYE2Qso3v2vPIfuDgyabI5eZhwPKyvgDMqXEwN4e/YYv9XoMWetH9abR80r0tKJYFaSXKE0+RmkQPM+MhK4pFfHnB7nvelSvsHCct9zb5EsQ5Q2gCsSJx8y1fCNqms9ttJ2qRaCk02NG6o9xZ5hOc0Y5l8t973P+H7b2z5aVJH0O/IfP7WCnRxjkUjUrWNa+/dLVQTzBWP5k6Af6CnICUVVvs5akNXCW41xDczAsAzWk5E11sUGbUkCWLdHWU7pcHix+Pa33dWmpIh1WLfRz6ccVrl34yuybJW4heqXYMjBxVtwxddmE9ePsEufQlqyTAM/4YPnoeiYyI8f8EhR+GCNj1yb72Kl7hq81mjtcTyuXV9Yywo5QkxZ4Y4ke29RYYpOzV9f/oBz4/y5RxQsN69Tkl9JoKvIT4C2jpu1cz8LbPaYplCZazJYEHqQodCTznnoY1lK59hDk/sq0IozOmwR9EllQpOxKmxKOH7LuPXOS4rQe/3rH7AlXspIJI10uwRakpO2aCCx1g3BRxVZO2diJUIrk8NlPPYIhS4tNCqhPrMbL2vw6KlsDzXsND2ZfhgHr03rHlenH7DYcXSka8sDch4SUva61FzQKh1bDBxdK1veZWX1PJzBLDLkNWL7D4Sm8iQGGGvIh4Qo31w4KutDJfgEDfO9OPSHjmoztOWCgHs/mp+ouvJNwY6CYqc16I8Tm/gG6rQE7wzNP8i6v0fWfojj7BQWTSoPSKmSJvo8DgTwkgbrkaQM3DZT4MqHrg4OkdfYrhju6CAMZxHdv0U1X1W/TWOJhs+lLun96eIrwWIOWV933Le9nIRhjV4+4z7/PNCWmj3OsG5EL5j8liCod3Me452h2vWN6uYNxeG1S2FTUvJ1ZdeMt//C3SyDVhVP2kf0+vaIdz6VPT8g1OR7vhJJWLoo3RIuBvLoXs/F37iaajffBQAGiAYeFQGjU5Evs1xnxldfrh6HgSMSeOgmcngK+asz/syWTp43DN+JQPiuRwyMXLt+RYHtDalKY5ZN2KqcV6QF9QKgdofLWlLQkN2byXw7lF7vZ7Ovw0GPofJXvfbFFPJVBjE4bxyWsPI5LTUwVbQld+v2VIk0LvEeARB6L6mQ76/VOuM/sD62F897Y3hLf72JDoVypEoVsHhNRCW6xk4w0Wq8SEOcsO0LMA6R/AnrGQHYLMUivegmIm5i7bi2lKnxJHZUF3tNi6EwOTowB6WOuMnfXJMaws66FkI9dMCeG2Ke8aKqmm+FKSofsMlw/PCdDsKt4J3uKR+LcNs16aia7SjHEZFsDUg5skpsMJ+Ir6FMZbVMltpZNdZYQRZXfBSy9hShzd1hGW/VZhizcfeUv+gvRLig6m8lQA2hLdNAHRPcn6XGEvQtMrgMKM2Flznw8ZI/p3dvNiC5Jwg+xp9u7R+eqyK/779f4D5qk58WHeuPlsYeM+zGfTdGtEtCda0EcPIZgspbIzVgQaq3bYB6oZgHi1r6rkXq2I1iY89MQCWPRxcQH4y0eSHjG49XrOhOp0QGNZIsD/raUo6EKp5tyarSJ9MZ9c4xb7sLwEDrsm5tdfAKsqQvl6GwEuL64h1jz7i7PMJv5r9KU8WP6o0QaOO6ix0B4Ey3lj5YjsXw/YvULx10MERFDrxHBst2kGVJjSzwl7Lk+unm7pSUF3J0OWODWcC0hIYtJmYCxe1k6V37U9cB0Km2ydmZGZoqwhnl2ca8X3sddljR6aBR72juqKhL1buPKU2xn8fkXmxNd68gMkCYYKG8US7JTQOE7aq38p5CQBj/CY3e5Dyw348ZyhkneuB0iJzge8cBgKFNucqpV05Lc9SIyyyO3zB95q5anBuqbwj9vup3oUqoISnwc8NMl8rF384z0jm+bQ5PgRxZ47RHiHrVj4HoBrGVOa4b9fMWK4TJqG2jw6hJPtgZQnEldiTYvnfwVZXHui+xreisiaYET9eQ7B+YkS9Yktuz8O+39fqABgxoL2VIljQUqicmsxVEjtVw0UEvmWtYw+zNyYnG59U/ypgCnD9NpuKcKq9Y9Alom4Bq0kbsugTWQ7dzzxfEajf4Vl6SF6vKQHBJYwEOUzCuo7hSYyYMvPUmxBWRDwkIYDSngg3Me3RkLcBMg9AWsdNHciU8tlSmy81DIgasNJZ2T2I/4TmLv+QGwxTrT9AyVjOIAO3uazcI86wwfwIOXXSI3lAMniBnmwiVmf2EJPqtBmSbiv3g9T3l9eoxtkQ+Z461rtJA//VmZMnqtHqjLdz31wcqdxEsxPTG/pfpHBkFUeBLnfYalvCnNNysO46brTLVuaefvg79poygA8D5L2ePtclsbxvYV8o1MZWenQzZM7YDBrYP/3sZBu2x/mlp1qk7SxWbHDjJQGX00r6DtNFVVsjACz59iVKddcflZkSVZjC6Xz2wr3FA0slQbLSuAki0ckzo6b1HaJAt7JzR/oheDRHrjAHFEYI+Xv5zRteW9VfCUxNVU3/jJj5t+DQ4jx7O5m9vUkdpnYBb+1PHLgS5y33fPTNfA7T0tmeAJsObUaiwXo5dhaNNmztY50umkGEBsEdWJGAqCTi02E1Fg7GWqEU/q1wrW7NsKoEgy3/A9tVssY59Doc7Bl90kobz+z6hkQomJLS74+wsVHCfHt+jydSy3O9HuCvGEXS5zj1sYnQt/24yk/rxMhfSxg9NWUXymvPMWcle0daBcm698PjItfEAuBBvOQ7ALmmNYUSaOnNrjuKXp50tRGhT8RutFKeIbDrebQSfr8HKjaOfIxgAlQfGaYtc536jYqraqNRVm7fgzVLyML80ovIg7fOlp9YRXDrh/Rc0mJnUIbRH1P3lGE9YQtwr77+wAPySwjl56SImKeflUzMT0tR263dJ5yoXi1niZi8viHUJmFAujlFh1eFoLiVv47XpiyPp3mZpmXt2yP71xUtx+K51OUBlWwEtiNc1PvqxWy3oDoQ5t9DAZMVejJqkncBpL0fTwWi+/PDIpAV8HDZo7sAanEBCqr+WydHnR+89rl7XsHyRR/bAP2j40HN34QuWASnW9QN0e4NqDPw2TJ5LnXttqfr+Aosc+EMBguyyekeG0UCa7F54y6rfRDtxoJTDsS2Icp45jMwggcUnMHemrszfV6xauYaAgEUAKne9KihNsvb5rm0SqyS4mwrwIQ3fMU0bZLFYY5flVrTu5xE966w0g2zsQsXbL1Ox8NbLcj0Y+GczQjGJCNons6PxGGpmqUMo84P2dkN9pKIn3QduMcwXWkJslfOXEpv/L3sThqqAeraSfbBwObhcDSE56g9DJ76scpbspGKUZV86fG70KqJvS/5HViWj+1r9sgkJBPFq6b3bQpekj3zBchT8Y005TuIim6pYkc2IvlVfVsycMHZ7WRoUXdTj+kh9gCWsiaI42IGr1dr2f8gOkUGK2t7ggySmwQ+3DCn1xxFkQcb57RVVBMSeigDKKk3tuBKc5DekfNgY0IYIgpq2QEkEEr7xjB58dguMMPn+hjwVvZeQ52APsl0T8ugS0/HpMziVuZ5YfgMY1b5coN7Uabeb0+a/8icdNKYULhYLSn/560iTmVR2RVi5C3XMuWaZowa3zEGUH1NxUTQuz+dWN8vA2wVYMgW8TMjpEUGeZK5DiyPvIAYvPASevsg/iyV0jgg9ZsjDuWpY+h4sf4oKlH+WoRoi5YCDZh3/0qg7ib4QDV01sK6myr6qWZkI2wnDdSKXxQbZqYaWH7CnQ6ojctsedoLUTmZqn1EJS0ZInlH9kOqvUeQALqpe70YXtlzLtHGIj+BMG0OJu/FQC/4Qp1RX+7p3t89x3P99dikgTwaPZDWeUZmLKZbhSkviv6aTD5o6SkH3W9jgexj9aaPVj3SnFfiDPBEBd7AY7qvLIhgs5tEiuO7qQutQKb0SJrs+bUwpXjdE3+7sUukvhSYUaTzCk6I1cmDE5vsAn0Vm49Fn7m+VwsFwXxVo2UB7jys90Xxrkxzho5VfJR9e1ii2nFEbw2yiB2DlV/hRKkTlLU+dj5p7yT8JS7twP/9kxkk3qQDar+6RjEUxdnQ7fnrB5AOyUzO9xIwel+bB9KCy43jZ/hM7zjNsDKD2b0HoW7MwHbtxehPWwTz3gHKzGs51XrZZ9xrH/9czfd9gq5MMhD4gUnfVShzsL6Y6OdJxECfb45fIaP0QuFI0y5sIt7qvoKEERVeLlHYt2YumiFi8SEckKn3xJlumihU/pedoI8hN52gVWPsOcCj/qSt+5NLvEo/CD/zTVkr06vlpmU4LBLj7vjN06wnfqPvhkVqD07mwktmulTbBIoL7iVSMkG5wzZLMLUkFqB0KQhTxwmv/cCyUwQFCMrW2yTO1oJsGV/nHN8PhkWJFRbf1A0wXOuYf6tQqxR6PM+pNLrxRL/9PdNzgmOmtFk4F6rjD6eHEAtqpWX9yMvmQgoFp3S9aZnrAMT1ausdcIsMV9dsRmxQVELJmJUzzOu5YGew9G4PJwyXcyY5Z/cujpQec/WmyRqN/N1xVCq9Ml3cBwrQQZLiGwmpBugPwlUcxdZbHUEhpiwiCBuL7KYbFKkEuVAjgWz6opKurlKr0TNnbe9KLH8jaF+FDUXkMD6r0GQG2XQhsZw5wuOfn5Fn742DmxZ54DJsO9rHF2VvXJFS483s4OzUfJKLTFQt6hDLdLKV/Vgg089RMl0VHa8/qXWOgxaxuGgVl98JFxZpNSxFzDlz+03J/iEnDzX4GrdRJC5IJOQYe9/b2UU/34w5wz6mFPO4mVSomrhriGluVxMlaPhWtgWWialBgpBNQLe2xaDyI80Tz8AOFCIEZhlh87aErNRSFKRYxoRl142poyxlQGXYf8Rc1AZU8U1QJxbI0d3YXqQ0rWpdB50ivWUnAEmt16v9Pyy9ymJbaFVgyhCbsVIl1lYVkrdeLVwem+Z4uQOQhwZ1ZlWAeCaJ1ZpRi6/kw59DQ6eJG1/nkXuJeFfUhsS6cjQxfNDksDjQCvUJ6DNcBYhSKj9dKkXF6nQopwArIxs3YBKNiWzwVgmVVaO+4b+zHKwhOZt2TuFawA7JbmI4NSummnyfFF9gmfC1bMxFaTC4tu0qGxraer/ieSvIc7VWthmqovokO6eWK3WqOcg9KnWk5Cp9bHIZQfn6btwEeEdiuYrfCqKyB4AQ1MqCfHr9yvwb8PuOcSt94/oi5TQIz6vbRgOXN4cgRsa+kOQPUcfJ5WW8QTM/hAAph/S6t1FguWwo3lBpXvZ+u4UVqBnYlfnEkcF8+S+rlpSTr033IUYqKOc7zWR1uZjncgKJcuE1aD1NXD+BQeGxSjKLQ8zpyLQDQJi+LnrTuyntFlTdXH9EnqKmfcHWyG8EtyJhHzXzyCJ5fl5ZJ7Ow4JoeAH2iO+avKZpc4AWeftp2lEp75XsAvwJIPj4hkawW334olAdRCXXbsDFZLBpyG/7dI3/XOLjA4g8n8YjyA9kXyFgGwmHz+AUjgf389lIC04WpVKn8Kq1DAdOxfWt0XN+pA4Oe2mQzwYYbzP5twibj7D7UqoNGrxIQRT0otAGdCkqsz4k5P2f9W/vzLDgmfSTXh3Zju0M5ep7eI27wEI3J+Psm/0RBM7G9rcY9RIFlf2MqrIGVdz/4EE7p1F7RgVqlUvTps6JIIWnVFJiUpqsiBvt9wcND7yhdL8Eby567GJYi61aTDFJDKfA9pyQ8NbTF1rmw2lmrFxlguYudWqW+bAWQ4lb0SGYi9Q/bHFYOQnM/MrmLxBB3bJH8f1qy2VyBH3o13ILJgafylpird8gC3y06j9dxjrLvWUMaK4BZDOIHawCGz//4MX/r/JsykOApxBaSn22N4J/Pd46xEePRAsletj8ihoGBpMQ8znpoYj6eb2IGIeDTD/NICxTHuRHSDCDyPVuqNyVaSfbunaBXK5nZe4MdxavKhicnNBGx+04RnUG1+hS1P///ZQQFhXipf70FpIMtZxKkQHBfdM5Ge0wk+YkGxC7yl6+DY/k5rbT+lcq1ANVxUKTSp5IzbeeYrvZZDOqV2XDjBI3XtDyBuhK/DHBE8QWOlFzpediDQZyiVEcDW0VuQHA4/EToOD1z2VY2nBF6F8BAk9KCl4G6HImT9kRyg3g19zX2pehZz2R9t7ohDlur8AriBPlG7d+Gqk+7PMB77BTDIy2jdkShGICQhWSg35BB+xMe0VSsVQHuhRdGFHoAYvXk29YC9nFNWfKBurTLpPmWUR+E8PP+q8XuVicCyksOZn7gBZd9ZZvEwZ9Gx18u0jU0JRIoVVG363WLUBRHRdhu4/Qc3jrTbNADWCMErZ4OQ8zAq1c74uLFCrzRHCfaNXu5IJoYY6QPhko3lg6Mzawp+hdPe9Ge6mFASMDzMI2x/uAy2ZLbR8Guy5NkadvJDC0fIm6gTAdoj3E9Z8v1Ws4o0fmrTUCg+6Z9DlQbJQcYUhfgdvkwZCnuhB0do7kJS1CTAXKphYU/KJVIiwG63Kz1FXm+zhtU2hez+Y5ejv0WtQYqBiD7NnLTSaZmiIG0XDuiLUJzqrPk9DvvMO5c1XcCxfsbnFobPj1zPD92B/dCSle7LRATXMGqGuY9Y9724gStImN8iPHB1PouQtqXp0vtUS3dS9dQc+3IHjm5Mdj8nEz9Jb6aoCrEJKhTaMjng6+Cg58c+NGriTWUWMQnWJQD3vQHF/5QHOFduMo2UDlq9zHJfYWOUWclWoYpSZWeqJrFJiNpubq1gCyp1zt5WQUo849gcQaouzlwYtDvAGu+840CoVaydxWova8mJOJr0bDDj/UYOXbFOyMPSUbvZq+pWpvbdIx5tfbW/8ze5mhQxs2qYFWisDWaA6zTK+cAd+gTqKqZwE6fB2nYA9Pseu5GVuPN2wPvCcRIGILapao6AxD9iAaEASxC1VoECY4eTjbuT3tTItRG/cslihyLLJpMb8peH0dumtRVH9jSanw/2Z7+dZQsIodTm0AVV1yzvgp3mLXqE/NogNYqALv/FhftqnelJNx8LoK4dHMK4ADfsbYNKROrAAkHVq4OkVIAAlBxDKq+iORiBOaHlI23cQ0WShg+HnsQF45QVjK+9NfL1NLZWIM2cO+/0YSU1iKzaMFxKNy1nvveeeJf9oyuaHNccGM1Hgf+/PcPoKg3p/DM0Jb9TJH5B6p4CQ3ml4vqzGguVwj55Kg1jogdJla11V7x6B2nldVimOA9+HmGLJ93SVxcNTb+ClBVX6+lYfivdli3sWsPDczG7HkbQca+B2EmZF53OKTni/5+GtyzRrxUux546YhUgTG4xZdtoA5ysakLfyt0fCrN+UqYiJuJDfWxCJCFlq14R31FztLq56LErz+/SvJ4zpB9qRUNWmFJULHmRzSGBxbG96zxOgOokC7eWkUFnDgefch3ugt9hLrf+f0xbAyDESjYlP32BhGyXM8Rh3UpHX5SMnGrqaaz7NlJeR0P3hS6L8TmvZ4PNA4tBEeHgP3h58Axrw7EkgMxXqwrj7ahKVVqNtpgQ/V+9rZA3rPTAtFRhHUzJbOKEaITKuW2I5x691SQE8ohOdSaak7WgBLsaxaEWR9T0e3z1pCJo4+MksrRc0IyAMGxBK7fZMpWqImV1aFdaQDN26BdsdPTJDBvwR4jIP/a6WZYqD3hVCzhWKsjDksmm67BWdpR/57FYi/9EG/rzugHz0laP/FTKATxobCiVpuXPPvkFw+ngplH7xk3yQnvfdKNcoW/chVQgUih4JqbipZ23we9u0eU5xUI4eKmQbPooDHHtLJiTrw+cFd7HJfm/tLhX5fdup7J20k6X3IIzChC+upNFPcmeX6U361ATIKv9qh3ocRynAmJrPVhEsWpx+dLuTDiX+vZs1HHkrxgxEmNTaJWYqj2PYgU6v1TlwH5+29BfNd2695vEPIglpPBi7mv3tOnZXRKWE0J15cn1Z0UuvvtFIjd9DEDgw8ko/+rSkU2lDY0S7NrN5eLaDULHDdHpdH2FBV9bSKQv+J0PTzBmzLdF/2B+G2UDeqCKTGjIhEzGamFPhDlwhWMI+u0GbY2vKQKbdveksWqTngzDXFUpRpkcBmiePTYBiSVdpACcqG2/5IFOHCTms7cg5nneRhOmPd7ljrBdgwo946/R/TZNBlwNgg8Jy9lwHrEhK4opzt9UJLHzRnsfts1IxM52nG09LgbuKYt1RuqQ+u+j9OUMs1a1wod3tACEcAh7e+PNIG/Q8AB6TQygNU3bJliGnDQtvwKzixqsE+yPEeeEFwjwSy+9nDD3zS3nCa3+MUUCK07lduZk87UEbeRS6BPz7aL0jF0Q+J6NIBmz9IpN2QE9gsW2hkT8ZQgXffAeoSV0uzkDaAmwiKv1ZcjYMVL0gPcPHbQ2TVr61U2QJiTolyUOL7ib/l1sHx25XNzh7+tnDPE+UN6ul5yfgJKnoZm4OZTeBzbOyq2ZXVIAbh1n5j0AQV3EpznZyUW5D+GS8nc1nR/d+RTaaniMO2FDkBhLXHrFJRTGBKbylKcsNcPueci0wBxHCmxmERmJD+nzJ2LUXKjNpaa06kelbXNKSb787dDu/1PeEMclFsPkXK6Hk03oROf+KOW5ljPXOpAvN7+GzfQq54EI0ebl//SKIohTwoG5NRM1Dr2w76UcDGhN8fpSOob9Wz8TJLucvFuKWS3XN8oAeMw3oBjSLD6/AjgYodDuY2UaLKIEQYjK5RaUvBz08y//yL0uzxur0iWja0xNZwLDWGGNrXSY3/s8IGNn2dt2lmz4Gh0ZfWTfCPfrtDiq7H6seakJKZ6klfTRIR1t/L/jzQds68b+HxjNHWPP6prT+sJ1h/YqfqUbH2jcdQXkR9HNgwF19UBxaLLloZKcht2SUclq4/VCagv6B7zU6tgQpmfN1xwkVGDB2+IE5Crz7OPqoN2xzAoj8FdCqbMTwMsVMQgz0xkWbO8PNgSWoP4OaaU8H0IU48/XFC2SVr5FYXCfbK8wXSh5zn6LSDcMJm3rWQmcadfY01zeki5IcPIZqa2mSEv8ZQ4e/mkIaqhJfBcS0Od/RuplXA+OBhTNEamL0YLsPb0rmETCEW3YzCxYXrb0bGLCyl1LXdWPrYSQyLV4mMwMyzjpK1zre3Ez4uA7Z+M2ktv3HqaTFc/RyeTKuym0aylqsqzJdPqzIPc+XT/jEkVitdpt1CtmZh2rM3zMaFTImNpWt4np/iBnjNu9ofTqq1cC2ykYdzPg6/t7929iNvVI4Lp1xiWQsMb05cCFUF1Svl8L2GAKc5vLhq2HsAza4WO/U8pgQPCEZHAiKNPekVxJoSYvck1pSiwf66buQVWF9UDEn1HfnZkXPdQzvOrGu6fE8ZCaM+KPMJQrTtUNEkl62WW1Ir+KnXvebGxJP/P4uEoBITt9kJXWCIqmDjwX5wrCyMyMrS97CQnlUPehU7g7DJfWKWWjvSnHi1yhnmfyBUQF6ai7hWzMHMno1Lla+sCMnKn5wa3gEyjrYPMygiPUIhdlIcyn5czKc55G+P+cHxovnBNCscCJ2UcpTNLLS1opElftzt1cjaoJ5ZUloh6N+IEdWocBfi+XzElV9Eo175skJrG5PXVrl8HP5xddJoDbQpuyTM5R1slPIL4iQanaZztBOs7tWMZ17Q2OWoSSTBiGkOM66I0YoXHe3BmTXb3Vb4CyrRo8IFz4KVAwTk61rjwVSuchve7SE++K+aBKTZslolR8l/YXtnjNpFeW0KbQUUr4YJGtOsRolAykkYWZiM4ZNbbqcgYrRhiAv1uwnpcutzfKsuvuks3kUO8fQCsEg5/O6QE4W6PEsrCspTfKbxmTHBJL1nsZwM5FfQ4gxChk4cOX+faTcxaO1Mybx2VB4J/gSy+9ew3kjempMKikxDbOart17Uk7oruKsMjYz3nDvVXpm10O34FV2MkR6T5/FKGMtVQ8tiWNoXlYR1zASbYV0bQqXhZ15hWei8Y610xSL0xe7v4+Ne3GcqJUtBQ8LyIqU+BILA9EkBoxuOPEm2yWnlHjZT5+k4yiCMZ6ILpy3YeTcQ8R3FFPT6u5fZ7K5INB1MiUn3hgQEFIsVz8LVAvM97ZMsmE67ZrYIycmeYE9voPNOKM+GdiGu7C4aWYqHlocEOBLOiLcwCE08cSuVgRjBtYMKfheAFicS5FtMlOXViXtkgpMVfR1IPFfs9W5oEhWaHoCnmJR9ORz2Gm3MD6nLSDlzV8zcsl5r+MvtH66YFfvYblGYNq8ndKBJpKEvnp6U/8Xd0tifwQ2/3pm6tPI1jMrgJT7g3npoVbrbGVsVy2IWx+oSA2o1Dc5eRO/Mus/fZdIEkbbQ7CE1dXa9UZR4TEJIV4yG6ONWej4BwwLVZKAgUmma86RlAaw+dyEBxaGdxtYlFt+QumIsFvdmlgCf2MZ0ahctfRBBo1E5W3T8bUj0lpx19xDGzdTgIzGeM3a68As3+9vBExrdBJiSK7OECHiN3gqQFkTYRQtB9W5TNAvqlqwTo2We4eOZRydJRAPa1fxM1hewVMzkAwLPmpe38Dm82LgjkC5CAELtS3fUQa/5Mh/Qn3Jl4Vz+o/NRzkaarsxG4G6gE1iDOhTNwZg0Dq6kNtkoBhImSKEij/I13v4zBVgf+gg6rPczh21c7Dt+eqreBFX1HLsHXUa+awtzV4clemK2p+skKqfiUcH1nFXQypHd99X7RTZrobbWvhJDpCYLLUm7GsClbABrP4elx5jrDnHD5Si7OzHv//+a/1XHg0U4lqoqx63d/Kpf7rKYdK9N8/JcL7fUFiS1Sf/we0jdbLhpMIwC7aKjI51HiegMxzwNvUOvqT844A/aE9Axz2a9lWvlovx24XEIk2uj/DzKzyBxXiMwMq42cXobjWjTjLT0Gq9QFXxgLpz98Na33xhoeX9Ty/7myudDsz7H//nJB84dDb+qjAsiTvWpdxuBVGUdLp8EFcYjzCNJqRX7lROyNtDNqaLe0kHDWJ8GCPG95YG7OLlJ/tPwV5uosIkKTqS0p9n3QwKfkThyk9ww+MddEwIJ8L529wjFgLG6tMj7ryRmXumC5cSJUjs31PJe5lfqymcZmQRbXdHrlN7xO77SjL4WPJqMV6K3W4o77RPwRJiRgyxhoEWw2pW088cEDjUVsDLQ1MYXXJzwAifz0Tz98WiEMIbAE9qmQAL+W8oejWdhvj3lDfF7d4qbSm7oNYCieS2qqNkOk76/C5NY0wgZS/WgSl8CS6SoQWoFouFpjPZJy0wCRS5YnK1dQAFaGLMaP1VrbYD06Sh3ZZb5TQqnRDf8fEiXIce4Ii5eKXUVIUd4IXg4rSZAIIlUTzHnLyVBg31fMiGI+pBDWNLrR4ZKa9hDuUgO19ikKFT/2FiKHcPeznj4sKNwWbPIffFNO81lOKa8PrWdjKCzSwsD5nQJXo02cjV0wL+m/4FW/mhImR7Lfyx4/UXGkIubvcHDsDHWzsjJol0y6vproiPcEu4/IAHbs02NOz6dd4BWv3ZVvKaxGVsKaOdzDT6VKmbW2RGjwCdaX+EKCsheFanCk1Md1XqI4uqHoG/0Syy4H//QKR4u/LK39rUsZg//B9XVXWBUi83PUtuS3TN1c5AaPZGIxL6S9OZKb5AFPz+/5aAzGd/t9N8mgEwzPqw+XDRDMZ9kr1Dei2ua4pc9gkZlO5qv7MJk2e38Tn/bDDdnA/W0CwJYprOjxtkU1seg9SM9SK0YxyCLz/hjdub+FusZH3GE4lr3B+BBhO8cB5QRSM43eWO5K5aHZ7cE2ZL4LoUeV3TjdYALOt2RxRrEpTC6PjM/RVrugoYhpbEZe7brjmePuvU9xcNFOUVGjeRKAgcebBmxX8jJfl3d4FcCQIkfajmJ+IO1p6X3n6skecsftwPshA3MazkhGCvrwbhoZbWvVUQFMtfv4TRlBj0lDNE4ggw23wTiUzsB/HSpo9+piMkH5AazEr7GcujygTvRFX6WBPyflx5OzYJJqdS2JJU5nu6rsQOA144QSejGNIb+xVYn3Lz+8N/NhSCkTJw1HmEww4HOBGSgtiiPTukMK00U6zFEziiIlj7WoB3pTRSTFZG+oFBLCnPSL4VSfb5aF2kQ0lEptEOnLFok9E/wXPdKRWAR35RYmIZZiDq1YuvCNVpCDZ9om7r0wcsl3VDjt9Gc9vtEuZtrSN+rJqxjQbmSUbNNsoFLZva4kmeF3zwUj2I1qK5TbOlf2/aayqD92j91UhgOygTGvMpEN4qzYQ6vid/g801YnI4UEQkvHugpHvAn89FCAPXII/42U5M95VRdO82RQvX8Orts89khQQxJaesymLkwvMMkPB3rJOKyA9yCwoBiN7AhT3racur0gPt9yPTUEvC1HMnGtTytUy2q8M+VzVn6rs0CbjIh74Zt2Pmtu2lhEMb+fczdq133pzAUjhXWPVwsr05IqtuwV3bLlUJMFwwDQdQ/D8huLQY2F3qia97jVLXOCUgGMXGl1D661vnv5yIPOvbOa/+zRp6+TUm6iWTejwBgYFEiG93GL4FJ6HcNpPOMNvoPc3CP7lBt1CRYY4a2XagR3QUyj9uw/xpMWFPb2NQok1BfvNW9NYWH5G4Yo0llpThRbirKdyTTNbJ4cmF6UgSHuFdr+zokFxSRsVM5MAmETHSdYAhZ5OaenT+1R8KHWe/9//TNapsHEcnCyu6CaXjsMfewlh8WCMyMcugzFRv7NPqGIStwJV9jFlSJDxYdoovuAWe61NFN1K31gSo3pd8GQJ9jFc8ZBrQSEtjgMsS6qnAUgbSADKK2XOrU/OTZPGP3pLnw/7boKlxv9UeKbkc4q069vY0VbWu1CegxQFZgseNd8r27yQC2g35rd56p1cVLyQroW0MMkAEMyKLS2iDIcbQvox+ftV0wcNO4beypQWFxGOTvk/8qBQWw5a4oFWHJ6Skkrwmc6o7p+oNrL/qYRgUQKAdn2TFeySZ1DgvSImynpkpbe/BXqfZ9iUo7fFiOfovtuJKaIwPyuwGjuH3p+8i2+ui7JsAVCzYQhqEbPEhp4C0AyKZIANIlWrpyXEMSOkKvAzw+RCvvEfi60Y67X1PCPHh0yVynFLJZDNsi/iQlR8KCKmlzprNEWeLa4G0G94nUKDPJV4V58BdmJpaGFiQRwIqUdqrr0TZdIBK8nI8uT6u0cqQPUwNSySwkN4Mre4ZJk4QjvtUULXXvZdo9buWO8HRqY0dwfyJ/nBP7uNtm9449N9VOj1Vjb+k0buLtYc77w29MQQdqbtskq+O43Tuft5rYl+Fhtdao4PFss1pQ332bDBRPmBdf5Xx+9V33uFowboWyce8OKMfCfQAcx6r/D2YE1cYLdEEikEMYpzTN33IjCu59Rq45rh+P9VJqpf2E+BMMpDH+kVMAEM70gV/sdz1dUU/MonE+LP0229PnDCi5xvZOq8EyDC3v5DN6itawkQW3T8VkBjiY5pJd+bzW9NPWGsvm8nLx/0iaxFfVHHSAOcHm8Gc83VMVO4bTzabYTjfYOBeTjlP0hlOE7HlIKg50F84//Hzf89k5mnqylO+kVB2nPKPGtCy8gpC7NoQCBcb2R2eiq27NiO47051Lb5Vt9weRetjnaq0q6cEdVvvoYHN6R21GN1bX/FDbxxYxPQOQn9mDQvcTqWXURZ6K5Y+hKXuBoGXLsC+ulSaWDZexKKWNxM9zWpwxzFE0VAr0kbg6Lh3RGO89WTWI5QiO+P/Saek8DfnfMqWz6IsN8agCyr9M7YNCFLOds7HarYOh70rfxENft9yFcG1Muf3k+Jpw9V97VBKgIHup3EJBUFqKYTPOBL+VGNAFEegar4lvneaIYSQ4smu4Cw1I0BuQEGGhxIHoapFxszoCS9bppcw8g2cspCmuJwtG+u92LrEl6381pi8ZflM1NxoTLAdPrt6zJydWVQ+NrO+LwLsNQJsaHZlm50cF/BpGHITpP6BZZj3IPzbut6YRh6CLeX+E9TxG6jYtyI7q8GzmzHADdYs2u6UFjgHhYw7e27+gt9tIOxqsEtEZXHOgknPFqNLAarr5xNRkZAWitE+6BUtLEG2EcZKsJktKSWI8vlQgScx2+v4P9DyWJszjDhh8LK4P0q0eQnD/sZzTXZJlnCtoU9NEysrgNfmhNSbEzOqV9s3FOEKjilVQi9lJJLefbReIlWQwh3JxjEuRRwbgE6JqVo2LcsbhzASLQ03Au7xvztByRTAWqPuqS+UlyAoUBdXgH3NUPZ9xkLkXKUUCVIBgJFxfPNYitfczl2TuO049yI3z291aW/Qaci0AYOBOUVQZx+Proekd+HML8ZDVjbAbLoOKq6dlQ/Vu2X6VHpAB34qHHtFzZ54KPioqeO16vApfNwiHChVG9qAJpqlVF7G8ThkWYn1UrY7w4TvPg0c2Nu+LuqNoJSEBCy2STt0q8hxXNaGo3mmpjStnQT5NT0U0kpDyUkae9Zrsr8eyRE3+oO0nh1zzt1sJj9UyS151/VN2pkdBU+pTmI7+MUO02aomqUNR/yITXtcjN/esZ7k533EiApnncwlJxZLcWlv7Gzd5ybSFsfP5Or8m9iCUgBvqGjBKCbAV+lvYcAJb36MTQmoqz4gQM4UV9PwGeRXLGHiVfMAoYcN6kAdD7p9DZ8LL2FYfhiQ9mFseRdom2CRvyQtXKz18yu4W0VLCi1Fc6wE/jU+iQGgNnP5EH6c7YYSy9YhfOfPXBxliiZvJulzKTogkBkA+JmqVNQaYK1ujdgUG9sjqBYF3skCDqx4zpX1XDjWVOVHFwocjOrBu/g1Z1S42VEvP4enq2ZobquUp+QDIHiUaqLTJ2ALBeVlH0arICtup+L0xFrUhBJkfRxfV//5hDOhMEBFEltDcWGRoNQwhfgTgHtSruFP7yEGz2YgwzFExalv05vlSf6vhkqGxzTeGX5nHiH7LbGVHjTtgAc8CYr6Yx93ZMACO93yKhb336+9DCNgnd2246MuEuBBV/+SbBk9l+pAMlLyDYYPBhv5fIdiXQ7++F78n7V8FjKFKjllJhRDUyhfFwFcNouOb9nJ0FgHh/FXnQ9hZVmHbdZqJSK5fUFNad2ZVQk6nZdXBNS5aj092l0njHWL1BvP7Jxj8yoKaDg9FZBNrqCAPgDAMFuDJpXQFYQbY9BUvAwd7UYBaqppHQU5nFsDxtj51doEdF9IWlBScaeVb3PkAuoRBGcFzPkp3x476zlYkq6Th+AgEd7aEF3OklMvVQ9lmd/frXw/mbq1PnuepXTivdi6T80Qcjccv+bOFGE0RV1EvYqhbZJVcEXMVT+u2mZEJoOS83pkWIQmAqTSanMqCpSrHIOKKQP7dL6tzKoKB4YS43bAfbsSzYcS/2O7dGHq5ftGVjz0PjCE5nIMKEpQJU7Vswc9flO5KgWjBLCjCSj08m3x9M/3rKNilOzyCt9FKPJY08ZK+aiK2u3HuMiLgBjmOOMiY27LmVHOxTRQlA5KKopyrhDWS22gNUEBXTuGT/II8W3dbdPJ7reSteHqfOhh34vHaBYUPN5Jlhqs1bxPXZG2TSIxYW1pgQpi2AYOuNar3KziMUltaXZXF19n2hXMiJzHo3TZ4My+oZrAMA1LozByKzOXluiK5feKv/krUaH1aa9qwhWOEiviMbdpy9DB2uxFTy6XKvy0OSG4zfsF/QbJL1aUOs3SCb853XdZMMjIo8TjDM11b7FgwaJiMPZGGxZxo5hr3OOcVvY95HkyEr0wsYxut0W4uySmoisa/CBM6JRQ/UsLm2mnvv+Z30vOsFMQz48l4iDAOJ1OdXwXO3vrWOZ4WN8ihX2Z1JApMHC236pZpdkLDZ2EWVY/IqZwqW9nOKH3ogb0/b61kDfxx4MeOVYk6rwCZDS82eSp1Ow3zaR+T7aCXAahFydogJeaOqjuYbojtLSnp7GXQjUG68hX0cmYuS+Od24uoEA42XvC9vjhYeKgdoJ3qe1Ciwcr4DHaES6bQsWIf4yG8Y7Pk+AvNDt18CFSe0OzU8P+McZvtShoLiHhnaIs4xY/t3MjT4EpWt5ilSGIcfy+kP3VjAeCCsJCW6+aYJQKcjH/07EzyNETckyfRTQ+lcjehIxRqcrHzfTlOqTQg2venO/IETvqGQPJuUzmYGW1QuvtLOll+JgVjVCqLw5hDzmW71c5SRrjjmZBTHWqkTATwLpSFJMlapwTTZ3wjXLtxxDs3T7z7fbE0rvgd4/1jgAtVju/ORW42kmJU25RL/sS3shgX9JIo2+e3PPR1iqNTDmro0GfmhK5ij/O/2BgmjhPfMPa9oOiauH+T/qoXqhmDvrpRoirUE/+P5Xse6lbLl7w1lR7FHPJvQfAMP7ii2/7+rChb9V5SFhnHbscnh5y6b6OX8ppT+WzfqgauhR68eO6F+dpamfbdaumM2fv+GgmvcfFCQ7GN54jiRvqo5N5YGhUdXpGPpFDqCrXbOT+BAXqrmzI93ZpONegu4fxafo0nzjpjw1GT7JCWeThxyOWkYW3v4n8JaJADI8pmbqC4gxdU4QXSdH8/5jz+9JrZuZxGf5g+3PNMN5CnBgsuQuVYcRhjvhQLqNXvVdpNKY/EY4KX04mPnn7Ms2NunxAz057nwmmEnvBVelMGWy88PA7+y9BgrBFRC/49+C29RZcNXeVySYVWxhA7j+S3tuWo+OKrqfictYjU7qJPGnxty8P+EK3x6WfrfD+Vw0qYMJryKyhJQSMGZ6JbwmRBe9mDYQxYMUhHDyaGfQJA1T6XY75QXZy/mLjkXL4leg84o7TAlDB43rEUxC8Rh4SNpcmusv7NPrMofy2+EKbOXfLObGcSu/shoRhsymTAyKeyPaU286zh0nbaGugdvjTpRvpAMuXhHQdifdBlZqjv1GimMP6qkTVdD8wZsgtxn1hlHXNy84rAjxz/HAxh8rxSkIuUCTzq4B6l7p/sfFP1YjJR3jFBlNDhB5DqmzTPKO8Z/9xVrM7ESL7pM9ULfjdtKuSMTsuYs4U95WtN/MdubK0WuLoVj6jpR5hjTw7u4guvcfGZMR7ey3Dd2tyl3Qjk5i84GuxAmDBCZbk5wBiUJ2Bp/ei3Bbd8J2bX4VdIgUQMQjStEhsT9G8BcecQVrX4jG9dpHyaV0hvq1nAstIEEuFH6Uuz1wdsEumA5gEDO8+zP5/wc2K90bWFVh6mEffbjXvDGu5PSHZoKzSSj2OvMBJ5OaCmyTlJCO7spRvTlTNZqxfExM2Q+tGwzeBA8Wuf/mU5Ftmzkqphi2oPe8J4d1cUAMti2D6HfdHkJjskA+BdLrhRegdIHUpq8Bd8g9BjT8M3hMtd9IVG4qcSFd/xwi1d0OD+xSPLTkF5CnnvgC5pewFCV+CsiV7RagS89aq+CtQy+Ng4lRqNROKXswcrV+ijFO4XYo5+iGU65iGaTFgGer4DRAlCxADrxtj+ifW2D2o7VQehv5oKcAL6sGzhjlooGPTgh5tyh/dntXU2j8psh5CSIq1KZbNvayuzjt0eqJ+BHVx9JRWFcPMf0POGtD9G6j2Wz6wsaU5uwoY6QGavNEyUhIO1z+hrc9DuL18fYpxiWsmomNwSomFTxzdGioHVZ5vD3PJFiTQgB4NR09uUhVFSDwIWRHnDMvu2aETw/MKaF9XSzA3jv7pi8EAwEWJcMqKVLwMlM2kvc5z7vr0rKFGlqOJtHI6GVj/FxMMAax8GQuao6DBcjC/EGdZLjR2R4I+RRB3HkeBiLWMr4t+0/AOwUdOnWAyf4qUhaFIV+AqgExIvDzcJzcjoId9hcbQMG1azB4QYNe/GNVClIKbrXeiYTX3OGtlNWU38gEkdxWpabHDamncHYkLkxPIptYOv5zMh2IKZ6d8hD/f/hZCt+SeDddPntd3yOnCoMBcc53fHCv8TTuSQs2VwjRb8qpZT2nQt6V5DpIfAbKUaaSLO6ObzVXzUMFeYmJW4ufPcBBzz8xSA3Yi4QtlQdC+1KQDwkp1rOcHWh5Siw590sjyFsXG/bIU8VVwh/deJQN44fqcSrJMVoepJEfDO6tcvjHd4lK/FE0gIhb8pNGf7S6uiWkK0/bjnkRBNLR6QHOV6QkENsNaNFRdJSZ2/kqw9ldyCRIgGlMLOAtT78l5RpM8TZR4YQZqLX2unG6XaKMAfsWqPtefciiiQrPfneQYB36chGPtpB5cbP3paXGCVi0QBIenuw7yiBAEUy0UPF555mH9qP5i0HDnqvBZvQNhn0KrtL64XdbzYry2zdU/UinoGiDw0NsXZ6UqsXihC1+tEjD/axiT7l2j9tLtTTq7bXW+EeQrP4IzJuYVXouOe/qounTAe2DBokFxfpzBNLl3pIMIOmuay0A+VKrbr7Pz3w+I73Fa/d5aHuab/yuUX4FiHvHpfJG59jCVW2F1ghZMVCyV+HJ5XzbzHqt8JCFt8trmMqFEED1yDwblMWoyKK+bVl42/vsnOQWOKsqofHyNbyMq5O3SehF1oW26jBEVKmmawx4g9035q+sCh3teT8rvd98i/NVlCABSwG0kZOenH1EQDPjrnVWw26xXXGxRCJLwO4/X5iiGD93EycEsTphwJmIQEVyO/fe6ioSm7Fcp/u08m3kV3ZFNdKVgH0UYAFIWJ7f30mhz3dj7++uRgRvyiAYVE/Kv7zbT+wVibvUfFLVxfsD0odf2i9Uvu3XlvxsZNZIGdLDLpnn8WKmoVrKcLe9nJ9e591eNI2jZsKJhRVyogc2pVHJCHVsFUINJFnxDbnTCPVkpK1solRZz7sgpJte+Sa6VzCbZX7+MZBawu2/MIVziK7x6QSY7b7BytykY8cHSzr7PcaK/MphvmAfoqrEJJhg4Ybk59wapDuHAfLkeGbRVnp6ghQJS9Avd/b9fX7ms0Y4v5HceVR9cjzswlNwfGSyb9lKWzKvoQlLsYPaSLRcM+gt3OgIuxG+19sOA89r3n/Yg+VEZRWIzeJsDKDEw6OgfHxaEGr7RdSs47D6/QpZUlq2kb7FeYBhIKfAfuUBlibbuxbgTqiA8RA+bkGO14ZKj9SuohKhpe+15O6BOPDwpg+fMjp9KfmCQJGU7TuamZGuoDZ/m6TouZIw+s62SDjg4E/nyAUI9l9jVjeSTaMsik+/X3Yt5YDtijyOZVkWb0cCsY+sAQ9NYXzelEP6LjuEhfqQ1ea/MnjNXyAWHvQrw7JMKWNAr++BTGpvx588/W1/KJ9bVN+DFGACUtL/4A8UCRkEAITt/F9eO2Fx+N5rJXKDCUrx45KTzqsFFCozAdar6AjZ4vWuyuCnAjTrXJTfizTjo4co2uSGYxsep+wKu2x+kAk7Qlwcybcg/wSgB/adgAfIN0zDKSnXsizu+5w2geA47wTaiwwGnXJ7k5e3FFA8cCnvjB05yKV+yV+eSOcVJjaLoBHvrZgP5FUzcklyAkh2XLGcFsU4We2CMm4MtMghweten1T1wIskMzy6O6RAlgI+gdPcCK5QuJTdZR8O67V69DQACU0fj72MSAwbxUpt67/uu7LiddgmsBOWIfAlExsaRKAxzzWbGJ91dRllqXQ8LGNq1bKaYsDBsF/lpKnB3KZDmtUR+TiMe8wGcWavYskOWUu8HFQhKEUyOKke1e5dVCBVFZLXWVy9F7g46A0XxvYXzdktH8c7J69rhUVGGBbYW4cT83Xy0wARDmDskDUI4XNRgQvOkv3biVIQag7VJHx4feO0vwMtHl7Up6HOR+bFmhrdLAuT6tAStgJ7ZaylMeHhjE+5OW8Lz1cFS/S9997WkaTkFgTaP6rbf/Fq9D6eeQHRvIEvNcHOmZYW0tMfe0mEqhw77obe9y7UWRLhMspiWarPhV7cNvt8C0bb/aYN4s6boo5miAf58yT8ocRmzj+k3mOP9upoUwxb0ZrBHe+Eu7aL7DqxU4vupIyGPaXW1XppopCq/SRLxtiDLe0qCcSxKPQrx3yy/RVxp2ZLOJGtglS6aAWqd+9Uswac3ID/5A2p8dp35WpRpadZF8pt8DXLTQpW+r1oxxHp3qVCtf7qG00vuhW2Cj4eueOL4QcfpB4XJJxC+tMhJcZgF+aP4TzQQ9MJqY75rPeL3XBoTMD2cNhLjw76BERlIjXuUfwAgAWSDZA6cPVF4AeZLUW8ul84v5UdMUT6uqg5SkhQ7eIw5A3L2j9Uox+t5K6HN9o5r6bwqgDfuugMfQlFiGLd9hqt1jDq2pE7YA1oQtdjUlERHuYr7US5cGobOAaOP9uUCbtKVvAQUWyka+x4+XzXtBDs+xPqhHFEcR3oTWjNR8so6d2XrlrzWQc0a+xRkQO9aEYGskMoRa0cAcxbz/yQw9p6/9iGGtTQtUg2JHZ0S4HE6WANO/xSm4DZvG2ehQA7wRcTDkU/VQiBbj/xB/19FtXdaU7XDlLI0gGn2xx/8/xxuVtksghqExXwPiNnbfsYJU1MaOZg+INCIpKj853yunzqqeBOgpPu88Q/D+gS2IV2cn48OocEmNGwcepIKXmwF+K+EcAm67IMmE6mf2X3S9sLgfbKKRqqj33Jigwa9hEiTMSR7MxAg3ZDhWaF4SCnczHNfB1OP4CeTUbicZBbk3D35R9eHCEsIwYebD89mhZZLQcRQasQ5Be0lndgkj4+rCVvbMQDcM6ZJvk5TcUT1ci4gbC2/KZKCyn+DyjLTVChnqyIDjxnqLcMUfeF9QzOvx4xO+9h39iNeGrfjT4GiiQmRTu3H3BnRJilL+Gd0PCQqskpNytY5sGUQKLqw/nRgMjXJaLFREasc4Ak7+rRqLfBJDFKbrutI0rvLG0YjdELUcAUYaCfZtk2HVop5zZMFbN48+dyAKkJv7eAO9fcomBdhdFUJVZNkfIL1yavtKKUzZfRzepj9d+qN0+6JFqwmx2GZk9Z6xt9XVxtQ4I1RAHyY5eqWjwh/E7FRLEnHXo43nYXXogmXEPBDHMsUzqyGMypHLdfI+GX3Q+eghvuCEwEPhCU7i6clvMh0A7WagokwPAZNmIVJtasKE/eKuxUFd2DO0nPtLxQycsrxzQqnVkhezwzOV/E/DW9xXRIdI6yyt9/GOXmwr1JsPv8+JhtbW93nok+Za+XO4Jzu4uF+DnVYeSNGLhZOXTbRNE87iqW6Q0tBO25x1gKAJKKAAIvZBQ1Hkla1yTE38/VXsZBSfM6D8IBAGEOvQ6luvDy6otl5vy/r+Fr9LP1o+I6dYwxbQYaqW/ZBineFKO5h4LQjS1GQrwe8tCIWjsY1fASS6ANWW9S9/rc/cs+3VA+U/VEQUYM37S8YXQmabnI96KqDnmZhk7yH2ucIavO0a1TzSkgjx4Fl4MX+4P06yHEEH85HZG+nLqbj3oEvxu58tMibmpFbnvMs+I60kjpqH8nO2blsEUxErWy42qZHJU8de8JGy7Uhw4fL+DKKutErk7gtzx8DPkfd07frc/IblmbroKay4Amt7DNkRa35E9yaZf0Qn9bhzt6CXsGcXfLMro9i9AB/S5FKcnVp5r16k0MCqKRBY2bHhEJzdgFvHGhzPtZ0/oUaAlOZOd+qLrXv+y+1Fwx/l3tmxxYBtI9VqyFRomStHyBKLIA8edWUrpxcO0ZK9KJFO7YrgclX/0EFM44PmLhHiB54KzJ/8lECM41cfl7j2c9zgV2HLdcwtU++xuoTfpbYJq0T+5LCGtvRjLbFYhXLIrvBjCiBilzfXGk8cVITVaf5WHUFhvwFyT6wv2MG2sLC2tyEeANBSmY2JlQBjSEg/ZK4uFlUnO8gCz+pQ2+eP33if7tkSmiW6YsO0QFMNVXbVEvTWuE5MobrWYmGSQiMZrDyoBxhbXre2x1/Tioi9gl/YUTWXi0VoO5rX2oSWWPXnPRx8DPOLCacMEzdsIJjmd5dAeVVmSaf0YjOX+lDxgIKxAqgJEJj6d+wRK3HSKEPcto7ii6sESOFAfMM2brIP8jcoYIxXk8bubVA8Gr8i8Q7Nx8W7lVs05UUXWRCm+2oYtBQAGq+lnr6/6nDWa1aa2CvAGvPxnSRJ8BVe95TN6JvoE0X0pdnTS4tbLz7WMm/PsglOUcn9keP518kW3L991TM9nKLNE3lGRSC23IG8G+SO26qyHDVDdxQzNdBPyILOray5ho0KjfNFK3YzqbUEoetEnkBBPYpp/HvtzRhssowgkiXXBvbOH/FKjAiO3wz/Yd/hSHh0vBa90sdwBUw4eY/kOi0VdwMekLJ/nRhSvQhz6mRIak9c4cgXGmlhZ06bhB3HLlHWwrT2Awoj3ajULSOYY2b8s4qwDwCpGEDGwcdTCiBIur0hwApDnx157gBhYuuxRoskVtHqnuTaGkAL3TqPrVtdHtUU02Kc/AeZj3uqNq74bVynded6+kMUYd/Q308EZx7rPNgslnACrWHwo+GZalL/ujBXNDdwPInwOoQlioetp6BsU5ekjKMAInGsyod7IJcf0SDeDqyHssDWg85C4XbTAH/LOruToKACmeYbe3zp5QkfYKeTCkKtNDcirTnYtWczWPykKNEMsGkerM5xtmgNaEr/gT1P1qtxK1dToWX5tRrrEpoqRMxCObST429136G/zQAdPOtNMBPq0c03rc+0/WiJkNlKH6bTTvQPt5VbHBddcP3BdFB7+goHwmFKhjnwzcIP+pxvysEN/peKev2zHIv/ra6yiAg+uyfFeFd3od6O00QPaAoEk5vpAuMWv0L7QOVDb4xsow+krX/5z20nb2vEjJPOtWvkdJYeg0+YOMSETsCV0aE3ni2FyzhbwKDzW+HYkLBe1yI5p7j/u9j5IdadKAn7NhXBGEy0vWVvAPqEInrtyMsWGopydC3bvCTlQZD74ZTy0E+UlFtmfgt48gkstRGTjWUbEmSr/A0L6yJZCJnbl1LwmpvoW03hl+P0o2nmm8H7aJtOfjiaNdqilhVLcqrdhMRGl5lMdXtoPI1C8RDgeMr5p48nJiTPkPbDLt79Sofg0D2KkQKfmVFFXPVtfO1gg/absun5/umk5ZF+jr8/pxrV5/KT3EqiAbLQhwYxjGP44QK65YADFOlVP42PIKl+Vz8qApNz6HBRsjreymZSr2BrxzvitaU28E2yFZ6ma0oCvVkrKp/po7q4sI8nVpPQBVkTcT8E0jWfrou+xmdAUU9kwT2OcVblcX4PMRzZrGju0mVO/fbDSGqGvednhv6/YO1asOsHMWhni0ZiBSfh3FGt5D5Oduayb7SHOKehbmrck8qO3sdEAzlIxUrWWtvhF8KvlsW64hQ/lRmsw028JLs6+1nHgSQm6XT7kDYgInqijRQV4v2pNF7/EBgWEhmpHm9oR3ecGyBu7VxPEd5RThQqw7loKTZPsaA1b5ghVnJIFBP9Adxy3StWk9mdV2VzoHUWv/iY3D5lY+HeANAqYGfIxJTEkvHLtU1F/oBdBJ7PVthR06C9h2+/nsYKuYnoXuISGmd5q4fnjFCQDsW2PwYpUTmAj62GmpjgRg1VQXevqP+ikhdJEcfCV3Rq+VLiUUeI54Q4RdACgVShbxitwcFwB5pUTwAmxxvyrgKncXOy/bQfs6TDI6zTL1xK7nOycRwFl+PaHgXOoSsXAKgn3tFsBraaKWPdYBYD1rT/8mhRGN29M2Or2+UCWwfQjVy2WQn+A2ImtCheMIhxh6v3Yu5I7Z8LwqVxJ/6BxaPAc2L3A7GIKdRLCJr3Y7AroJ88UOILslXGqVA1b4we/zdmGTUGj5G0AK1NlEhiJlzDE0YUVKNtMge0F/e2gCzRdQft7bhRL8vl/OcQHMkqkvmooSJVzFVPCT6O1u9elTnfC4ibKp+eXDkKxiJeh4qX4GKBQSoRY4VsDiIfjb10jG+GEocMesLGwwZ1hZ5JUtrAQeGB4VlCwQx1DhPjyWSTLMH3FIFzOMpw1DL6M5c37uSTua49mpM7cEIOh6rbWERUI21Bl23zqJo7GuhidCszAOyVGCftM/nrRclo7Msa7hJJ+aOM9b2fBrf2ycEm8FK08trPTf9aGq/r7WDkpT9DrziWcKohr2lhb23K7bYizNp9GxdCg4ij//SIcTHVwto4PDMoVFOdAws9GS3Qb6DjJB7e6L4XgvgPXz7A5KccsnFqlSHvvJKrMP20j/p9/BQ4H9wWo8UZGYgp5DAhveXNyQ9acpTx3QqyqGcRysGa+c4y0R9yt8ItQ3YDr4ryaDgONuDSfj70Jbljs00DgjPbhxIHYe2UA6mNs6iL33TEjPf2+6+U21fl3YjAqVODFkrM/XQd7d+yoFXIXBurQrBs2/V8ZHi+RGrX6y2daWEByUBaDRNgFb9+ZdynJnxYzMlOcu530Ewzky4iMoUvrGQH29Sz7JkpNdhM25pYS+BwyKElyQ8tMXIX34G2fsp3jrT581hyaUwbaMNREwDaLFVX3/UGSJ8Q/+kQqZKcPRFOMbR3FvHzcgNeiVqCunHYVctN0JGFnpz1zzxqQDw3PwOA88tqUo49qKTKXmf+KsfjkmQ0sEsQqvq6+BjL8TVrvAZaAdcg5Y3ySKntUeWpIl8P5UPJFknqhHAJ9e5PL+hO0Rle+C9S8EEzr7jBqY+hVcRn/UOZti5fZxEkjiso4nQLS73EAkBNCqZm7KQnBS8h+DvUzwTTpdlZd3eh4HTqsqY3rfdFJbcd5affILCHWebKV7DB4iPw63vjavaOn1KHmQ8cdjd4tWG/JA1jwmEaABneDpdSInZH3aNsLJ0N08nhT0Reh8GG3O8CR4EZ4IKBFQE7KNmx1JhU4sMGNoUSXss944z5c7O3l5URmhNTDkchtM0H0zWAjiKRdkniyKwQKfH33ZsL9p0+tPtvxiF7M+GGeiGPFr5qoCYBj7ZmFw4iQ1b7zzzUgtZ9wmKb7vmu1JVlnishwstj4Ug2nILVJhdF3cGbeT+2+Iyn4zOttgUVyMDC1j/Im7X/fQgliEi6qMFs8EhllAiUPvK+2JJ7c6HutZ+LTW4zyFVV0g1JkOAilk/mqAtGXyAaGuY21ocFnoXmK9PJzHXrnTLPsoTyJ+yn/UFZAdGxoZBTdxm+sy6K/EKQO4lrNhMtpL57JuY1T90P1B+zATRo+ECbPfrB4Cbm/MlFDGOP7TP8zX7nbh+O9G5Jz839DO867c+3hJ1Fvx9ksDTOHt7VSeZ/MTl4Z4mKcRIzm43v/vo0f2vBsJGgeJwgqbTveEQGF/n5pJ+W/UHD7QOiXdzxzKWwMgL5ftRp9eLTqBa5DGmB8pj2FAp4PTKznY7gtrvQOdVG/TVdg+6zbTh2CbNrDPox2e8Lc5LnCE9pXOgq8NEoOX/w5WzstYPs2y6xzKZsya1LQ1a7KdEvvi9tKyav6QDSyeIxWHyQ/s+UnKjyYTfRIB70EdlWdL5Mxt60Xya/2V/tZSx5BbeH0adUT/S9YN3aj9lFK+UGG2QIofs9JwnyF09tQTfXFOiOohg6oJa6yNEJ+sIhaUGsHQhDonW6z1RRXpTMkNqEz2vMTdNnCZ6VdWjLqEOTBcR7RxJVNfGz8ujzfHpmt27QulnQydQPSD2ynz3bZyi7o/pSNMslZw2q2+0pO2S59eXA+pBzyK2fKOUincnk3lhpSbkhrm08aNHlcxSfHgUYNg8ClNAH2X4okg7RsbVrWgeh8a5+izN7RpzrHf7rn3SFscVtoK5w2fWnCGqDN1Pv6rImaHo2Xy8GclDNt52tv0THmq02jyp+kRNXL/ZPaquMpglEzbzsUoklb07QK1IO+lw731cirFz+P/ZNFuuHospwy/Kiy5/F2RNi/+JMWWfTzAslnx6BjA0WGyACL9nFU8m5ojWZYp4qmc8R928r2gNxNe5NaU0kyhaOcZFummM8NFSOFhixV3FBnaA1A1kfN6t97KEerZKPqtekLlDOGuRMztmxaYAYEd6rtLSu3TuPsWCHurZCJYlL25uNzGjkemCrFH6+TvdecMez6yiwmhT/Xvet3u3GWp757nxCHzy6Pk/TsuBzAqWFjjICTEorv9Rx8BqEu3DhH44iHlYBDXuGOFlJc/DId17lXGAFaAsHqTgQdsnIni2h4g4nL13+GcZDAf9JIji8UEbpi5L41AT9MfL8bRcOAg/2WtRKN8ZIS6Yw73TuV6SCyH6k1aMobbZ1J+IaAPkEisJkG9hGDG0iAvjPS5AxhjMRZTmRb6YdQ1pDKDZZTbeudM3g8MI+PfIbqi+Xol0fybddpjoNzEccdrJBmHPN2zt2E9650Yyv7Hfqry4PS+1QQt0jLHxxD2gonWHgQYoqWmXrhiDi7QJupsqQzUYmZmHcdvnLLYahOBcqLFOKsktnHxnvQE5JzpVA2pO6LfgPjN7A16UAlPSBNamxHaXW0RYim8DyMKMjTqqJq2akkRS2ot/tDcFXnABBu9Ja9VZQQeCmBqxIwEuYfMcAa6bxO+BXLhmVW6cQvDd6jCYXNVs2hCITppCGsdgpAFQ7CfK86zXpd41KLe73jHCb5syg6l13clD0qiHqYGKqlvW9j5m493o8o3aCmKf7lVYp/hmSSrzeAONRfrnmAuY5jGO0oRYKLk0a2EDZw+hyLct3UdpMmV54u98oSDY83frTj7h4FtxD1/J2DX0gXTKAzE8IYr+Kn00f03K1bdx5ykjyaYhD5P8ifxv28m3OW8HC/VM6abdBOk1ZtjN4g+BV4za/p6A/yJZgOmQBGUUMziwA/yW2F2Hmqhjili+UgavFyx0VLKtWAs6teeh0lDGV1e4aF5ci+R9/ri1F0DZRusLmaQutsOygnVKpTtZ0+xvmwPvnqgge3YNq0ol7UMp5JaSGr7yXcDzBdHbRj2xvdaCmx8RJaf5/HMpxY6AUG9nFoT/+SvFK5NvCgsEXIQ42cyx6XsVQfzXH8OyWsFMgLYjRnjwQoiy7n3NX7UUAWPJYWwGARJWYPIBkbGCozhcl4uSdblaZcz6sUYvLwd8s9XrTuBdWm6oMJYbeSnRdE/Dkbryvk1zyDCEJF9bMUreAbyWAtIcJxDYc0sBuVSGDV89pN5wF6TYzx60VG9aC1uIf3j42ZgirUcmr2u9PKjGyQWbjnn8SMhILJARCR9NFp+t99LivK3VBEUUjvsjIdHCEhMmL4fbrbskOKMlxD888AGvq39uLXNa5qWSBDF1/n2VrTDYV8xnQkOfe6Cx7NJlIKzgaB+muhcqh6V0bOmG1WMe0G7MH4VBcbqcqlVCIrpyRZ/nDBDmHiqOeY2ge0q+1x/atf8lBvsH32IlXC8Iq/lPyv0X/vNPUN9CSnE4zFVDZ3UleB8jyvmrhWyh3MCVwK7bGDsODYncBk459ON//OoqgubvOIHP3iELeRnyOFlF3HR7OiygOR1eW92TiAOzLVteujB/dGbM6G93VbybqpVdxnOFdTI6NFw06BGvDcRvmaFWYMex4QXeU5hnWt9rV2i3iCiVOw2Q24Z78enprOnkDGiAiwF5UsB0+ywXti97aY8Hfv33hpGN8ow+q/9sk4Xgx+gPch6H+5U85Q9T+20CxZFAEQHos1bkscLYLknXfaYdY99mwYelTF2QzEwXbDgD2RznGVbH/svC9hFpRQOYiWmzCHBw7jHmHlFb/WV4ed21ILSMsi6a0x9/FPGBm154GnRc4XqGQxn+OLx5WHLopE4tx90stO125OGlwGeWyRRbeDwEabPfoIutiXuV0s8qlMQz+wH250qITKC+HxP7RprbA5aac6g4dogJUTzogXubpEu+JHJVo9Ozoqs6sHoILHPZrljqqqYFJQoaPKyv51dqqnl60kPNikpWl+0mAXdrLmPLz/2uD8FwV7H3MVtIsoRIbH/16gJgPz2oSnA/TZzikQoPSt2h2Rao+WLhEBD75sD/1VWUdg8BwAqqSow6YtO8hVgY/cUjQGKkAHHUZpmj16GN/LDkMlQQX3wFWRaFibkt0GoK7DN5W5m4FJZdqPfG5c7IzXbRFNL9Eu6Rt/3uaB39FoUPXifqTgCsDe32t9P9IQCCpWxSsENpeLAo8ROJRL5MrPpE5HlYliqHJpkkakeu3Rdf89gieIJjLm4vQXSCZhKMqFrdd/5IP64d42q6ZiyFEhypDqNE/lZh5Vupb7nc55LV5BQONbsSuLS/N0K351GZtQOJ4xfVCIffaoF4ue72MAU8pdwGNSjoA4FqulyJWkKLVd/8G2UirmKeGt5fwnkoSrru9i4mFlEcamwNv8EyGSNiw5C6/99y6VOUxpWU2n15KzdG2VL1EhZlVjXSmxDkB1FGWX6qgKotxsSyGLhP8YeBdvibqXoTmqI6wIBXujpqojn5D2IFk7dA/ctVJ87U92YmJJ3MpaejhkGQE1l09QALTyrjRGaubphPaFA4wK8+SbmzuMM8whUcjUyBb2MnC9tyknct1OQEbOwVToSJLSPjtkggsjdE7lzDenkMyMBvCDH5rw9QwwLpl+5IbG86BDBpQEBkBZHXj+TyBrwCOD09qkhej5IVlFRGwutZW6Z6C5gRLEVgoeyDo1cMTPXp+/N0QfGRT657HbjvFqyxuBhiMEH2I0xKaxwSuyO/Wn2qdZvohIsByMiOLHNhFfdtwlza9yOnhv8UXK3XYZZFPfDRICvOcXe0ATXqTzn4o6ghWGicuhDHNuiaJ+Ks/4M/82sjM2hzjxnc83MaqDG5DDlvDL4lcmH84flcuvOOcR981KC/yLlSjaYqEWtJw29bJ9GloxDBsHPV8VM2GRK4Pk+75NXpKRa3pvwBHWzw/OfEWQs2m0C/L+uU0RcBVg4ZElwQvVGINSuNaLt/ACwFpAuOJAad8FrX9h77WPlvp0iJnRSne6UGUc0FlGKKfbjG4Vg3gTsCOu2kOt7Rl7mGV0iuBkr+vrCeGzeQnbKx1pKd39k1v+oZZ8qJ04p9rPKzQ+cjsVCaznCYYSWJJH8q87KRSI6mK/sLZzCNpovdMvwMKVyjo+Z6P2o9cR/CdyEZRQPi8RYQsxcNP/2nVP27nKx+hf9LTea7snl4ouXua4DIugwuRCcha7Q4YmcyluOL0/GSo9//7XoQDrmW7HocpnFsTydYWlkeEuYsAwByYJ/itRvZDcdeDJ+nF/MxaMLGWsJGFcWXc+G1C8c3aCBQF0motXIq6DsQikjkKE9y5v1HzXa3NiZNs64DSVhpHZLehpFQ627k0Ga8YxvVvQayhiJ9EHd6PKY0m9dfmcM80laZ0+MRyp+vAoWgyVHzeAHanF8LlUn90IRnzXZBU80JtqLdvx0v4XaPYhHPqsEz9zEx2DTELhFx1G8KSn8K4ttppN+qKquNkUxRUju/IuEAchzahPb6BcBQygA2tZqPwdUmTLf8/rlDJNlgq9mk8A2lxr+UaUAWpY0B2/AzO3KBMXmQ7CwctCarZAs7GR6/yhlTL6RRM+bbgzApBh0A2xdH6zxdhrZDUJIE8qZ3GkLrV6ZjmLwVNVt1h59RZtMEQe/lAsVbzLmKoWaTvQINhaClQ37wOKzu0ibAbuwI0apvVD8yy7nPANMgqHWlhDR8hxz+4Teo+bRFOPHoX/kLDBNMjIF5/q7icTocuB39S6SeA15Y8mR4MEc5KRxszfIkqiD6lCTA2UN7fru1vintNtqnVvVKs5vSX/IIwIcpoJ0MW3FBcjBFvFjLzQaaD495wg/XfXfgr7xa9C/iwpPrRYv5nkvYruYbig5+q8+8slhjZa11X5rmT4fCL+L5qkt5MH4o5qLyPwTE8qGSHMvxJBEnh/pqjKOzmFCSsxn5PIYelrBkgZcVqiXPBKb+IKX+WDcLdqqWGjjpZkpwTyIFNMkwaRO4u8tLENtnMNdy0LZy2g58AE/iAdYSXDEGoeo/fLSYWO74lEI+yXkWfcaD9SO9au3ECo6NhGCpJ/zCpWsRUsU6kVcyEPuxE8ak10SEQTwCAq27Jsw14SoU5CWuyG88RQ6gxKpvhl68JB6bk/zYKDhvk/qA4iFJcpQVex6VAN5e2fitn+VehfU+Off29saQielcltNjl9YO4B+3Ug8Pw1kLM/0urxbk3WDFE+NdPfsYMuXH2alwpq7/pj+MDAGJOSCCa4S0YrzTFbsDK23/Fkf/iviQBdo45LcGiohRGB/mDJxE0b8Tvug/iPWxmkFRLg4Qvb3UK/52TExGCNRP9Ya2AEkLva5T94n2D30BZzTpetUjGTT0ypAGMEHBvE5DQFyjjpI4p/47MOo39FjUNureYFE4sVza4VEgbmtNa7ddqyJkGsF735vyJkiTxZKrZmTBItXsjg7XsDvbAURTHl/LEQOHpC+zKO3Fmf7XjTw/Eaqjs/ss0kxVMpEmblLDiPu6JXq9NNrZ+ZPw4VeWiSOSMg/SG21TN4bWIk+utl/iYTCZQIR4WTbFmJNA+ofFwoXwRDsPUSm2dxbffU8qsSRtTuOXKedpoInZeiPLW29+9AlR4MczMA7rWM4IHBCf8ZkET/Ao9tIEsaYiRbd6Ov6PoOVtwUcJZyk4EyN70Xg2TQW3ohljoce0j+LL68eDL+3HzkdchdrPRcaTDP6DvwOYmFiHcH11Bzywyn7R2UsLN0MK/LX8N/ZAF2hqVpKZ53yJrSIDLyTgGp0GOnW5Mb2pIolJ+xmUKD69zuAc8D3qT6ntoz3KfDpzxCht4RGf4cD8iZH928nBFcj/nBHZyk+v1jH0Cb3seJRSbJW9iE7L299CtTCJeqsLaAvsCz4KgPaQ1XbFibWiDX0ls9S7+7lvT5oskrgF4cF4iv5aWtYSWohLbLLT/EMpMGjIWUxnLRBqmZxhaoZ2yaHU4ukaS5b/k9UPbfm5rebaatPIIe7K+r7eYVIO8JVgOa369Izf1a89YCfKziKxqmEZ4kTdIUhbgC1Bz5nOhVF8Yf7Lyq1k/D1CX3e+aF1AQKBhfW0MYIFnl+atNKSyS4YVK120tjd+jLyq6IeJHMwt4n2yt+l26uMEPPVUCJQJ023eOhiM7InerZyqBx/5jTHEVe6FJDs//5Jxmax0ryVKSZYOS21wMgESScivWQYWE9WK1oMQZAUJmYjZJXk6UOEO2DWfR9QBoIi9bhI7/PvbOi8ideBRrpswx4DAO+OlYBXwFJHVcCfMvPeh7oF6B8JhoQF+0LCT5KzlFuaH1frSESNQCLZCVlwAjn1Q0Qmj7FbgkgBTP7DMZHSWB9nKsH0VFjlaO7bCMas2YCdUk7dMDkIRRgfYrr14lNANwGepfohWPsDigDyj2IS2gZxGZPLE4T98ThpTe5jVgcisVYAWGPa2A0fYxsFhO1C6nmhZNkDLhiygk+qLgMX+Uag0J7ochZfdtT/a4ZJZXg1Ct0uaETx2v56oMu2Fq/dYgh3uuEgFy3i+2+iPvjzNcdoLjbkLMzbXcddJHjOk00cbEjLkXMknVdgxWk3wvNfow/1QFtk+WBGsDe53C2c6wMMHCICRHJdm+tqymBXN6gs3dNUlbgZYe/UsnlB5RiDsgzhck88I1pbJwylCuvLOtVQT9XrJQen3DypTZ8YWzUK+h3UnF4wJ/XnrggFQWHjcGzFBvtK3tgPEAJey+gQn2K3A9eGEJgyRO54o4scwYjY70C/LgnXvbUszyZSSWSreuwCeb3tHYsf9ROg1YLzoQWSb7ytx20GaWZV+Jgm8oVhTG2fTayxWDvBPYx7bqPTZvFluwfutlZYMNILAlTvcpxcXVX+mM/bjCxNUEJ3XccGwttgpTDmPfsdaHP2PxgRsbBL9Rh3SzEjpbzLyryoDuw/3KFtQipUk4Gc/BAwyaJXRxmwhN77WJek8UIaxgVhIiWzSwam8eW9psNaQlXbPucIlBTCaPy8t79FfVBmD4ZPYuhutisJ2Q09qmXqdUoHaOzaGWS/ZEqA2aFrBRczSmGcfMJ6G4hzxnXUlaUej34TtM7hpUqyJKdwnqtDob/XHHD3XwwCDooTU51N1cLH9NfxLicPB/36Jun+byAdfffQGF8KvC+NYmWkKgAuCwr246kxfWx+3Jtg0IhAGEI2/tYVwtf22ep01mXyRhSBJyQ1oRvCwNrR0MRwTeWxepMgTE83T2YxVIw+zeBcu46EqnWLu9fsxaaC+d8Ki/Cxf1BBGOZvagV2jjcFDEyrooED82ZYeEFMDQfU2RCq7J42fanG/o3anOWWH1p98OHLqW3YOZARV5pGNIyS/HtV/C0keiLVVkWbzyHwppGFuTgt2ftXV8jet5nmFvSR1mV7b617VluBMtmTdWYqqASaZR11umSOsFKGs3TgN+FFIlMCX6q995qEZKAhMU4wUBd5bPAZDlIeGmeD2p0GnW2caMgbumS+6wtgFHd4GHzz7/jfseBgb7jqBm7nW3idWSPfgjIeTLPNsM8DojHhiyPKzhVr+rKrhPcCqqetUlFcqspDzD5w9U4o4+baMNTjjQY4GHSi7sRotnc+sOWrrWvYNP+kx3UMkDGT+JKk4/CmPJyPNCLhmpj3vFGaLoZ/WBgcrNiFLqErEVy47FdqpBSodkW40SwmgCB+t85Frxm2kQPD1FkGfULGBK8aZA3jHjrCIT0RDZiQEYZAnEVreHpoqXQqw6XCNUVw2GPEwrJyiY1oXNgJvQiPzonSRh0an+AhkBZoHvkYSf7DOd/pa9vIjs7tJ9PjOUvpQmCBvR2FbtVNWXKLS7bfOZiuIFnlWsGPLaEzKcX01byVNmmTPcYuUpgqCZnY326eesm1bfvYPqzm0FKGYm4R7dlP8ML06LO+fwr0QgctcYOCTz9dmvRAqNT0MrutB569JXbi0vKu3T86OoAx/rdWCqyO2T+/YwJTn+YmBiHUD4uYnEj8nyKedDKVsYHwd7AP7L+rapKBS6nf7QCsMI9ayHFQuVcNoq06Xyw8kGRxxMW6539hzFL+bAYDXS+MJWDiqy6R3kpwlxxtAg6JQdNPBIiNOFGk8sxZQ+tTD+SxDk1j2a05vK6y+Qnqy6R5KNvhYUEmE8btuqMQ7whIwZXUdPQuTZDkswNFRwRgIQYaenCiVe6LC0YbRVlLmHtAjgHSvBbJsw8B4S1DsYoW+45n8NSNCsA21w8O9OL3N2lP6yRwy0crwZLJHDKYn3lcrGcv8r5VKGKG+iBJk2jSYq6kF2sdPzFvdqMQyfhTHWgyW2Hk5I4uPwN7u8+P46FU3IRQvCTno0oGufY3xK5LaGkyyWPMNS3GgUcPpNL/RG52Ar5fAxlXRLbNY3GGVFSx2bS42u6Ptgp7Pbrp13Zr9yvGdaFjF36CKs80B6cayJCbiUg/hvm4knPo5aJsswEdVioxIOzESPGGXGCC0b2yNDfBIH4S3xqV9KB3bxHBsbbHUYSbNb+2ro9vx9++fIiTbVFk4tgi8unn0lE0nhR9F5pggTtmBe2q1rAbw+u80QIFjtPcbJ6dpd3fNQiahHnl9jx5iHarJJoW0X/AAdEC7zrUpGKhrG/GWPUU+pgdXfL2dVu2Q/d02/1hBQUn6OA63ol17Lv1KahjWPZZzEJ6VAbAfzgczdk+ye+9dGnOt/7+HN+eN0aE3l8r//1pMRUxOjavWPyTGXl1eJE6Rnt80ughMRXl7/4CnnFmAbcQDlShUZXhFY3zclBmyCD0uvnCeEZ1bVHa8xGJROi3RiiHojpD1a3gBB9UcAfWaYoC1HA61uW2v06h+nuWQffEpU6UHQVU+WA60Tq8hfnWj+ROb/3UJooisympQeOjga4tfclTSkItVHEkqwaTj95rFMFr6PWUGnmzih47c7TEIntqBKM8ZOupfqnqUCGS7jtMgWhoW9f5uUOPfJMf5lhLKtUnzFH8K+hNbVAgln3ahOjd87+NHMuRH2Q8LWcWE8///73ZFHa+hjRsrwau8iI7zfGzI6UV0fwaOtIuuPC8MgNteXawnHz6Ze0wJsoiICYzaSnzb7WhaiHKDGkbsQBWUqt4ZXRKM5Hx/tXXx4n3W92PGuuzMom1aKYWhNdn6bBYebg6yruX/3utloz2tcfh2Y9Z3opLoVn9VvItMcXc0xktY9HXDD+J7vDBmu06/u5/N/SsEBBLN0e6YnqFl6GeGB5GPGIJSkHHUfDoVg01jNQWUKA9a1D9VV5vlSn/3nOsfr79gm4B7h0qqSaOg0ORUwyM3hB09cmESTm1qaNaJ+K7jkKLL+K1f/g6nsZoJoB3gP27GSK1UOlU/haW9KqFT1NfeVS3TMV8tiimd17F2nvcXpnEWeZmQT2SdxQQvV5aleIXcnsu1WTCxShAA/l3mbfLxMERkZ6D7VZRU2PQ1lyG3BcLk3SqB91Pv3uz4rCiE9H/HXQIsymOPgJ6hOywHfi7qm1T7o6+FtfV9L4KZGq3IrEul1kw8sUq0njqfuIJDYeY5hQy6Cc6rVmFSewBXgiEB1tvN1CZa3CoQqaYo2HWeHAtsuW0YAHB+3zSb+bzSDUz/euvTeb9zmG0RCWqxFJcwxIs7Fll3fOAOb8D336uQSzYkt52ssdkIjEQyjaE8an8tgzHvUk0Jxo5uiLE3fuA8KWgAV5nbCkj0LdtIKMQ5nyrOZBATd38xONYwmuC5h5ujVBqZDZLyUbZzuRpOAwnM6HKPdrDxHw9qSlpsCzUMf5zLMIuhlGBPdPzI1h3Ih1ocdm3gvYSWwo/1aawv9iC8h+Yra8IqcH32zeZmOPxqb0dFnMFwuhYXjzK0QxRHGZ4XZRhL8255sRx36GknatYpifG/7KKPU1A+iuSZJSbSFi2wZk+x6kQy8qwe9yuNNn65d0vcJXGyFQqfISeJrPf5WDlyQHpJelqfuRgvlDObHRlHaHsrXQ7UvX80VdEx+rN3gS7cOZHYqNdSpxue7UZtz0yIGjZOZrQsV+F/KTI68J+FSmuLC+OxMUe6Tdvo6Jun+rLxNh4CPFwfSRPCMwyl3A1II1vw7Z4Td2eivuC23DNVh3yeNtzyDi/jzmPhC9rkgOMYfOE42zqNaG6B+xQTtqLq1uwtAJOW6LordMANVX3kF/wV9h23PS1VC8VMbf+Sm14HMVrM9ZeXavjVvk2KnlIvQf6t7oK3ID3C8ebakkGZjvVwvpzbVjJ/vXhOWrQZQU1mCn7DcEQiqE45dKLASkRI7kz5WfGLjaSNEfuy87f07lgu+7Tu+WHTR3qaNbsmMq5LBZx/QjK6KS2snzDAF5VrIolxuAn6gDsVrmZCVEkD7Y8jWo4kSZl2E2W82yrBLX8pRS35zrnq7CkFTGrp+IErf2Epar5D/Yr1o27lQTu9hMoL8THLJW6Y3h/1VYjo4ks7ndVZ3PPLVQuHggSjUCoWspppQ7draNtdaZBgXbdIWyQjpEa8GHPKq+oTqYcoKvdaetfEbfw3xKouo60YzOze98rWhNld3EmKGflB9+D7fOtuDilvbCkWC+yv7kND7ivPP0zskCLYqtQ92gRa4R9NRTiA3aXcYuASSDrvQ7O7hx85dxEgV4ADl6/QfGFt6EXiI7Ltz/tcvIMclKoRZA3yTyp1/P8GHHzBoF7IC/wgS/abiRT35BkOYTF8AGBguGr2mcCHV7crHj944EoU8AJ28FihkeVWO8o5AF/NEb4Zu+F1XUgubQi152szglypWhQl0+1cnLgHJaYNyUX8ltMB/OwqXlPu8iRHJ11HN/KAw70EzQSSS+V9/3S67lPLUUv0Xj48xYsb6M9CPWC1scK1BKTmgvCTaquGKa7RMVxahariSBeAauXyqImyHrwKeWpH/HHeOACRX1c/8fvdxV0VKG49OMW1N6uOFYRH8Hp8WwWU8VnU+eVnVGtTtZj/v02Bu+JFANiaz+0VcmRAAyhyoVjHHjGZeX3gF+nl7ikv/+9JqitwGv2pHWtDxUvx6+qFAcnAFiDhPBxhUmJ5mgK26R33h7n8TlAfngZ7jZhxLTa6WLT1JebaHF6SlVwKixdNJcFcscfBvcZBMd9iyn7+f+MTWO/UlRBlPt+ROLH7cso2LktmQVdBmI4E5BzvmpFB5FOinE0ofwKSdg6ns4iHka36AUzna37nRepZonKXWPms5NOL1h5PO7lhBO2EcghdX19GOo09vHaZgPaczMt+CCxhkm8VkUL9ibgRCOwK69dKnL2vH3jw7uhMOLXYMdEXdG2MKZ3xsrVYnENpVQXHgL3HbyQL46lxwrubMzx9RZ+P3YuiMcRWYSAYgKgtisOd4hFv7lgYMHUYtp2Zvses9cqa2G5yMa2RlKnNr2SYe7ngQ7davlyTZkEMwLmTJMDDhXxvrp84LUh9i1eaE/spFQSVSnq8VPqLPAoasarCdF3w52WNnLqyhxh0AYasCxXMGZdqbkFJEoyqVURDY8hwm3DfNgiTqcIIlSjDDlXlCOr2hAvFcP4XfPVIgPUll+8F0aZxgRontwZfFqRZZNqfT4j0KynNaGvp4FBFMcBb/aD7EShf4/j1n4G3ENfQaVpSsqZSKOH5C4nVKgReZ4uLRt2NMajQ14r56249FzYUQirNwZvzrgLxg7GI3/Tbgwk5BwC0WwLZBTjCx1HJrIMToTpqMWtfhtCEj2cCTc11yhGNAqVsv/nPckRMga4li7JXXKGhvsEg62/fLze0nL4FuXRLfiBpynY1JzXxhII6pYso1mngswLUgS1LZy8PmcJ4F1es26MweqX8ehcznkF4hbZ80C5BBKWHABfpxaqkRRbM20jPM9otrxygPdBue0kZHGItOfyUYMcf6Q1Q+JtGgwpGTC6KEgXVAAN4zM/lGLvIR9J9/C2kxwT7ZBhaMahKZ0bltWLqq7XPc1l8ejTp6VpQ710hhJZdDF/nyPXIov0JwEh/XagnIwEP94Futi9ka+iDE6Q8oTjLNnpGXpyCVESF/adkZdDtPGJi9rZvjgbwruTLudpFPoTvaPsGX3dNjgWnm4IXD7zyRkxXhCqiLOHl+gCldVWvxbPmRV4ibajUVKKYhGp2XxxB4V3TeQSe3hOfPY0W+titYVF3zvxw6/zucAZspUAiMXdS8/4T/05eXYFa7Xs44hvx41DdpZTTc99j6lKfs/007sM0GzlLPVim1mS2JIsTD7PF134iYt4eu2Nst1Vslhz1G6hpegfmI/lT7R2pnuQQcszbUPnNLZbkvYG/j8e9y8U1YSn9jTMzQ/L4S6DfM4SBjvVhnvT10xlsc22IHMxuOLJq6g0hPMrzNa47SuebiT8CzSYT3Plj6IFfALAgkQHOnKVdyd18EHn/VgLj4qVo5PBV5Rvcr4TQi9ItivyKhtu5gIGrxBckCb5fKdt4buxekII7mdT0bcbQm5q5FOMizyjv3E44KaLnVjeESR85sxrVO8L3HZ6bZGnluUrGDatsi5/XPisrIhhc2rNoTzOmV6hznE1x9bC2woqk6NFp5W3d1G9TJdTYQ+y+dCv7L6enQAPdOH8JIJ8vHTE3rzBuWNpwOzU4LBr8fL85+UxTVXnxr79LM9FA+W81hi1UHLvNgYV/PZJiD7mDAqCoEt7NrFdeEw1ffk2XrX92KPStlPRTHKGrqMjBZPXIugd/OoRD6rbCAmkxn+/iNLrlDmrBbURm5Tu9q03OwSeoTji+Xkt3L4FGjnyb8aMkfpZfD7ll1Egw0S3K0VfDsvAS/G7oV0ADwfZaBD5vVBw5EmBPAf0o6eF3/tOAakuqjZgvW6SD6jmN0NKzoIcxSmsLEWitAFBqbX5xSV8hMmRVp1iDT9ncZ79U9YXqQDhSkkHlwH0rsPwpnWmAkHIT8ECQngewdE3gywL/y3sGPVUthbBlRx2taSTlO1xrlM1l3YU+YKKPvvOxedMKjLk/OwAmhWS2b2fSCRQ9uwyQfsnsGQFvDHTvcCc1ewI+PLqA0U8Y+AtG8AMsu4lLJGTjl48xrF0uYCqfq8wQnroZMETlcNJXIaze9KK+20aJQ0BWEZo0BcfFgpws+X9tLGX2aX+3JkS2frAAZhO4V01+DOTK98k54d/gkFpdYjpvVe1EUn5VkIXLySJFMND/z01+a6ijsLP1+QAVckFw2ryXL2qbAWLfZXi7Mn3J2tnKP52EbgznSGodEUQlB/Ri2EikH8OHYGb2HkRmoIS9l4pL9YF3aOuO3YttDobPRpz3axni4e39UQkezx9pC4xCkrYdO6xJYZs0s0XXCFQ1hsMlduexnzlCeQuELilE2+45xqkYJiCcgbefbqU4nZ9vh0UXI6gwwkXcc63tYepTl1MHZwFfIJMTmhQgL0B0x/k7PHZHRWPF2eNTsIiNY9ZYI8j9HyxysbxQ1j9ZByBT+MyTY1KPJpNY77IQbuTcM7UZYlgO25ZNNQkBzLT3FdTj49idC1GxgbBoH1oJKbCOIQYnWA4HZ7hh+o6CvFgxzu5EtQ7ies4LOIA4kDqwKD6dSb8NlA5DFoXFhCdxHI5yWL6Cn4msKaHN5riUtJ0C76Fu5IIyKQJlfZ2LpbMBYtmpDdAYuGOMHhgRKrc7uxGBSuOdQMlt3AjIgft7zj08X4+vWED1yxAXNEi8mRqk/goj3xOM1igtfVbgmw2S5qBEXGtqktB0Lw9mXUk78bBcq4fc5qpz7jEcTa3FRWnFoF6fBhJJQc5wJda/yW9Jf8xT5Y0RZIVPr21cM1zbfToCg8LDWrZvQ2TGufM3gYt+IXB9gzLBTtVnrRbYQsxSL5sakpG1iwJ9pRVnGantAHjylvByoMUVaxT8L/9WDVQ1GwGUu59cTH0riPtXDh4wQ4aOXa9Db6RXu5gLGO9fbjuhGsUirSQdrMh0hOqutiuY45Kgji7bryy2MQMPKqJetF/EINsoFogai6F2HWghILcHhpFaL1zPLfXsUPRP8W+iuooden/Q14aBg1nrOIIjh7Zwj12zRuH6+rIq+ctwoXcIR9EuPWlr/aN34qB2tX63Ac9xxNmV+uGAsZ5QDw4WhQrWEQY4I03qV2HA3kHmJlu05w+hWuzvf/JxNJv3eqfTk1u8UIChtxqA1DNOeklqwcUbGZ/6Gz9fgQDh8ncNyT3HzTi4+G20kjV6lqp02xLDABlQGvUfS+7cDjBcHvvLpMmgVY3Q8FJfXXiI9Cod7+2jpSjkftuD520aMLgqt6o7oQ0irmP6a5Bahkj2rB/9InQeAMELMisZtiHZIYd1qxL5wZkmWChSk2NWB6r8ShzKvqNtkg9asQKDAMUcjNeQDsYKNwHNYC1zVwt7IDylMp6hNXCIAlnn7PlmzGdquw6WblCDTdYjUmKn5fQOGpV1Ekev9jBfTz2PZTJKkHlh4IbVJFV9rPuMHeVPH4bUksCTNxxLpUOARxdsBdr/Sex/c2IcN8kQuoK/HHz679wZWjzBcSUTnHCXL5EIPswzX8yQPUP0qdsyx6mkvLtIv4mO0/HPiS3vCyL3o4nWPgi1UtHCq/yplWuTtxm70MEzukr9GjaT+yAXGQt2Ho1YMmkLW6YbB1dDhDTBaN9CAzjUoItE8eW+1RKnBpxBecFZdCv1EqV2f1j5uZFbGEBxyk0Cxua4sVWZzXCIwhuXBouTjektY2J2RL5wai4UxkaPzCvdt9wpEfMpu5X6Op7o3THSLjA/mWWsbcRCsp+0lyYiCXb5W8Zpce0RVXEtJEQKLgczYj4yfghjYwR+Jg1sJx4iszh3XMuSGFm/iNKzheTpzv4VkzlILucLIwi3+3awZEkOW0lWmtm9VaFVobx27eo/j4QenZn6YWhGxm60y3Fc3D32/mQXa8mGTHkYBJsFNrSnCA5Hnn4YKruN4LlZJ/CpIhma8JoM4fvxLS7a3/2ncaq+zYg2qBnm0g/7J2147zzrv9Hd0eXagzLHdsgP/ZN4q3ezXrWMdO1TjuYacmepXkcrHxyGNAizOQO2kpM2k+MG2+barL4icCCLkbwStc8SJpk+Ta6kFDAwNkFKFoJDgQB9sSLLLii8vMKBcAelVM90/jKTeEL4CMAmqXGypLRT6eSkwHvSF5bwAbINKa/6jGCzWL6oVoacKYbyJLfMbzWuFOi/m3YST+5uTv5pDTtW/0jLAHMfJS+SEHbRjMDktbPC2LrAuZ2lTZPsdYHtFgy99CVqmqOrGh4QU9fxKbDTN6nYv07z2gmf6Zh3zucVXxolwEsFG9mtTDpJFvXzPIHruRro26lcY2+jjRQ1+h3zJBiOXJW6GHLuCGrW7dczyEND4DNB5oYqMCpNwz6HL1NkGkm+VPjSrQGkzElm6ST/MTgwRCfCQ3sYtNXLu6ZuwyUOt/16AFYBiBmYq6FP51wc/EBZNXyxjlkJZG/7IBpfhUEWhGr03JGk/DQfk5giiKwZtiInz11DZ6dUP0AWd02DUNRG4oo66TgAZ9iwIgQ+nofPmIuCpZ7FWMilSuNHGcH4whQVRQhmQbbSR6DevTr4TAt8P8POiQ9C30JJklUBUzQeJrJn1zqEk6pg/uNVa/o+PfV6D5SHc6E4REsbDYhj/4CGFbF8fV6GPJtp6Gc/dpOzai7bxvzNEADli+4RUVpW9FJ5+6pfgr90TFj6+1AA48uKs4H4O9tIMfCoQOWgeMZIZjLG/qytsBMvgV42A8S+CLQ3wEca2kRIJcqt9EDVOwaGp3rJw5BnGzJbjssHofyeQ05YejjGw9De3+SneJGcTUMBVk1WwLwj9HaEGfqfUc8ap6Vqco2rtz0msDKYXlwXpCQyPQGiZD/o7yq8igzJ47iVTjHIz1uEnNxyqlOkLKnVcKhDpHUb6ftcSyz5XqRIE7qzrg457kIqHzFtsqiPjMFix8CZGec+LQ+52AIxt4fc4niYS/3A177yOJ7PuNn9UaVZ82zny1uaIzacXaNa9slix+bDe9MNt/sdIpERAL0gdG17CwZET2jbiavAh9tUTYNG3Ze2JQmVX1UA2HifAtnf7BdAOHRDiAvmtMIwCQmltTydNgXS9IuYhuAH4wn/rgwnFtZ2r7riNZYW49AH6+ljptEtiSiMgDaWQt47JtRHEDti7oBI1mGLtgAHuPUx99LXkI9Q/W9BUvg/YUi2VEoO/gTwMjGKqspV8aFSENy7y5c5d8sLYMc5HkEK0Ab3InrMMJf3VeI9IT8S+dsCWcOSKhXpUwsQoonz5UAOygjIgC+pIrPkY+4k3Yz9ha2KoW2kxX27wv3QDGV4XzuX0nzl7Tc+OnHUehbgordknO+DPdfHaH3HcmI+o3jdLLY49ZMWV7YSzfFh5XljHvSi1Vbv12yTEvunZF2lZnXlwVYqNenMjYiowYGwP3iSEXqOubLDLZKPXF5depF7/Ll60295tFH4wxPMt3l8I0huKyzQ/cRD6c5KwwrBaziaiW832O0YzBXgw1itb8PXVTFws3CL9U6g04Vsw08MwbSq0ySvYSQG8Bt0g1/RS3kkra1hUdIRJB0JausQW2W+e2/RpUdvKGRaBTxWJyXv/cQzslPX4QQcGjrlaTS5doBSCa4GDrrldGLAzLlN6ACWsU7JKOgdZlpCPLK58+n8LNkC0v61sTF3g+d5QMo7ist1BcLrWa6sUfjDVTRFXY+gJMm+N3Yn6tDSejsTcG3Ca3XT/hMeru7SDOMh7YUiCdXO6C9Y3mRVMIlaN49/04rPpepDCXGlxAOLQ9Hn+gKIZlgnh6NvUSGIrjxwGhHR7PzPRbXbWDRc7i9frmMshK2K5/iteLpoS4Y2cgtdhTRvnaOSNi/FsLJ+D/0edFnP5iH7DePcVn2BMG72j7PBD/HSDQXayiuHyP+wm57+0LwGdr5ES/bxJ32DLGET1PS4gXtjhWW0HVUwQRE1nU/kv9q0Rj8Hu2w6LYDCH57BLz4dQTa4UqSbHjtMwcC1022G3BXs3qHcAZtIlVuWU+xpTs10GjF9WgJEhe2zXDBy5x9IK5zcpYOGFRUh421zU/FZTBTmJGLbfnulAaFzvgR2cSCGAs2iXfjbS+uubIgiDAnLZ1aQ/Y5TnAURhTpws9mfd5CeOlRnwOnHBULmYU7rsuqdAYG6ORjKa+MJtiw2QBUYyjU/KB+gyOgLj1EcXrd8SvoBQmtP3E+R6ZYRhawDIeG8CRfPODfQlb84COm2vcdOzA81z7Nd72GqFmA7kaKdHOq99NeHMDh4EXmMrpg36mPfMv060JSx9VMvYjTE8NTsRP/fSIY2wJ7uuffF6j/ut0CKfLVu5woaf498jnPEfbaUeHgOs516d9xCXZDEHrXgt51YkFvy2aE+8mqnkpJOKah1B/Aa84tvkRRquWlkh6ylXsYMuFog28oVTgZPjzM8mT9Hb/j1M+3exSf5TzdDRcS4H14+kIUZi2K8gfN+dcdk6LAu0h7Upzex/IK34vbaPM/4JNPYjhjcVlQqB1P0r9RY60W6OMP7Iuos9K9l7+cyCax0epBsB0uJRXtycLUEzjt7U84HAWNSwX4jAmd1UR4Jc0o1XNVTA6tfdG2vkF0Ryh7hmx6lUGYQTLyzOMg72qqs0ubu3dS3gz1//KYiDtLxi7KkwNlk9bfYghrSwAgXzyJXjnBAwMfFBDYXG9q/W+hj4KFbc6zCwTPBI6W3U6g0yeAR6frAQOpRtiVstjNicU2Nl6LomRuhnwWE24ADc5NRy1hIXMFlSFOiZ5GwG3iL5GY1vYMBkKE3hFht2k8p0+F47j5d1fsOC/nA7/hYKILHrO3oAYz5bs+UW1eVIsQDitYu0lXZl8FxkM2dr5J1KV9J/FPgVNA1Tr7hcm4dKWwI9T7HXt2FiPbW9zY+ItxN7p3NJRtNrXAeCN6Lj5oXFwZCAFyZ4QzmUGme9YUFrbg3K99cXLULyRyLskslkeIx/atgSmG4HK162vuTUoxEvBsXp2Zswmg03e6+fIiqGgrQRU9CV/f8aUOoli57GJrOhumMUBLVGyXsUZmCnvsKNT4nXniDmpCiJeLSwM4QIGlyatZXHjCIYXEPN0sh1ZxcBZLbKpGVNRNQOIBPrWfwoK/fBsE3WHMKxJNNBCVOvlBKbdEJ45TdTZ199QkghiI+lIssQ4KkDtCtNSpCfaLygSWEFFlwJDjOatJAMGcdkp6zzR6U/JyV5R5LEsE+Aaej2lVbsDqdSKB1cYMQyQYSql/MC3iyxac1eH4Kt6PYQ3x0PXj94RAVFvGSQR/pZVNny0h3ZgMl0bunlvGLfo19cTrVBRoXkts9tDiXllPE5BwIWmebLB3XmtZEAk4WjfZGDxIRBCe4GMrfPwmC1lWzhKhAPvq2dTXqs5M8Hf2hbrYMz9xKxRf6+fLKnWNQbGEwvMNKPgVOG/HYHS/dV5+sRrlUJzCPp1lI53mWNoZL7sOX5Dtyeg5+79iuYbty178B+d+SThXWD75BO9eymo5F6Sa/OWu/k2lxjdC1b9J/MWA+uY8+rXyHZDOaxCElkIUg9KnBm2BbX+P/vOX0PfW7nuGmaSY2CrZ1jeaG83rMlrNnwbua+gsU72wNPlJ32dmqY5p195rmqWjBYow/yRDL1ZZW28jRYkAgu1+G6uDiz7DGMaUGM64qu3lra4sj0r+5IDSFisYQjO4J4bbp814yJiJbtDZdagii27eHGtnclnB1L97o8YudQMCSO4NZiVUCV1b5fDCSi/R9kQclYkyiu+W+g27MzZX5oNCJV4z4D5SGrVNODlrUdQTtDxzc6G2PuMQt1jhVt0ywU8tCbokTLiuDWhCt44qfRRrNSa61LdhH30c/pf/Wc93/6RNC0vXnL16FiOalOznR644qR4hPG2LEbUxfrrMw6Cro/Kd52a0ueItD9Rjz6tWPaSg0UhB3FbpGB4+hDJ7l6ad7R5HksFbFdhf2z4YCOpKCSyWJ7TXrmV1iDiwtP9xV6uD5V8g5X1vhDnYW7bBJSgcX6zkjAb8tbdpb7TotS/tf9cfXwtlyp5xczig8HfQTKsaQXu7902cmUWpUGIiqIVVOmPAL/UTJshDD6RAyz0O7JNleYwNXe+LOPTNL2QNmzd3vn9HmCGYSmTU0TeqOqQ7YT1quaMmElc1kVuwXAHpKkO9agpLscfBaC3D2pDlf2YmbKqDnbvCgAE7vWH9DZcrxDOU7DcFJSabvhpFTwVjm1uEIhZttElZzODl5yXa7kXn7zVIFAmd3mEAkPO/GhQsswhaezK0PC2kQzbO2g6HA3nE4M/QTrYeU7NNwhCUIwePcSJtDUj9VAzjfr44oxU9n/EcXD/mZIoVHOSc4ZrapLpT8JuzB5O7jO5aX1pDlYrynnkEO7jPiKfw+RAZryMG5BnzZzl/BF3SAapdcy+hx0aI4K8cwz1P4RGR1vV/x5PhxJW4g/CYeolaGA0XQEBRGkNy3MbbAOlXclB7MaOVN7R5NU6pQhlJgCfURoCisLTQy1k0lw3pq7cW1EaUnf0mBzhd9G/jEReb7ijlhdiX4Fzxx1CwGpsBQ8YAooGQqff/Xyg1+ECJ+I5Z7O60MGGK/YTwOU2PutDHAlZQhCVkCRIe+E0dsoJtjr/ZUmWCLLByFFv9L520zMb6T8Z39gIgC++9+3uTo+bjh3RUKxYj9T5OojAXYRgtDjsNuF7ZcVKo/80P9gkZWyzdf9BT8+lLGx1t5OYNIi1WnMdQJYcXXWBhb3lSGBSWyBAajGamiYTOGbCmP92B2Johb537woUeLQsgdQ2HxENLAy/aLT2h/okhMwvbGS6XeAzRxheSFxO0lgD/4dq9BFuFiy3hLLZv/ap0h2XgAO3xYt4NeIMu1IEKZ5jYtf/TJr+6Xkm7w9BwFTok9jGJW0mD/S5Dq0MMwxNLZ6XReeCGHYCBvM149W+0zZKUGrm+QlmfPAdVYt6Y4CCkYh5UgY/SzvDH7zCRsenR5gDY0m5dWUx9dZNjlpSFxNjNCo+zsiDqcCFZTwyXUoDsRIGL3ji/6Cf/4XGDigoDbRnpv8Cf5WpUaUJcUvp/Eu5KytTOPRJaT/o6p0Uqo1IgYZLAYXdGFyJB+EKgCaFERsxEYLZa8nH9iuVdfctHyk+h40f8qIj0Flj2D42ZHWLfa5zZ+bj4VpFJUTP3QDfwJr/JMD86c0x+V7bsMqxZzhbZxo0XkoRWrdMsjlnbmSECR/F7G2yihgBCW74iD+SKSbGmQSU68IkMl7VzJryL8kcxoahcRvdIPcwkCPWL9t6l9n+0XTqyOIoOaWCizXjPW7l/SQhSpNLYUum+Sst18B1xqxUqGDtNY9IJnYYDUqXQ5b33qysJkdsS9Q5xoZujFTfhPpm5Czzae8QbN3/BNISBfin546E9DnhyogZbwEAKeHIyFqRRcSeWiCwA/CQiFVVofKB1er7Z45zwitNQZJ8sexdJBN3IGU3HAFW03ADwg0fu0eXlzA7aU5wcsay9am6hRr+i7W/wwLSBJeh4FO5Znx7JULFnhtbKAHSRx5QbzM1vcweQ5FvHBruNGTOuYZaH6H+Zzc0So4aOgFOstGtpjOfn9kx5H1vRQbaXFCrLCEq01WAwfv4zDU0tqzy7tiCQgNQbwphCkFeIY7OwM91/S9F+e2Ey87yLFrCUp4b9omQPIL+uAIosvuVtLeGtxhSeIoxCfxeCi4Gl9AwJVynqz7IcWLFB1RJtfVMfVDghu+kAbiFsO95N7QJ7vVo///ArU/qtBtiNe3IxMnWibHF0SKIOu+Ap/6lh2QnFONfTwzpJIciGs2bhH1bzP3BAGehYBVdFO/B0ncIjpfwZL1B378PSDZE38EG0zOMSehA/1kmZL5IKs7dk9YnM6fianukSNsNGLTur6U2sq8LAgiKp4zhSQnkhZNrWxgM0JD0CAEKA5W4qf/JvMPBxsarc4tetxGhXWjQ+cZGslj4gNDOb14gQeAYrGy7BuZEycNkm5NNXt2KvesvMv6L54OOY0Gp2P2POL9Joga2nfLyGdYxvIAWL0r41XUAaqnlson515xGnfs9Qaq1mMMC2KF++ou7IQ8hFgNd7aIayS9nn5Z3CeaA5OIUeIExIqyHdaRythPGRSRSurEH2Zk4tf0/DLv/2c5Mex4NVEVW79rhBlptVd2Mn1lGP28R2uRsHweJzWv8QBktDHtrnz6kn32eygxTJtHRnADmWEL0prjjV6Ja/cKaB1GhPYBxme+j3446s3BXpCYrrN6ybo91fnsLuPIpJQtpWfv4Gm12+C/Zmd9meXbiEIy8r57gxUjMToD6Rzmc1nb59VlF3/WNTwWTni1mzuSgJ8tXNo6gxG/lcBpK5LS5TzuasZlSHFQLNoZaoK3vjuU2Dnj9rkIof4Q5a76lQliyvUPPxNMDyABcC6DixUtczm7E7GOQgAxRjMM9uhvlGn60AzoDUQeppNWdmlHNLzar2/hcc/j7ICxN55Ynzn5FQNA9SlEI0A30ZgEimfvIRAZt4NsnDlIIWzb0FMq+lUufKkNhz8EUVGkQJVldNAJenTSHDHrkHWDMTrUvrlh4vsJhG0jQ/VcPyAc5OIp3EOJK22uxLguaUWqaGJ4i/grrj/twqe7fo5eQkFfR/zK+eRB4z6+4CDS9geuTjAj23AyLKkmQb+1EZH/YMgAhvhG0Qp4vJfFL5A8/4yY5hzzD70WyHDvpOyhD6PlgzLfre/6fbQvuhBvcO9oCJr8xw5Ua5XnbfUs2QLrYFqmgEGSoCX8kqIgA3TyTwUsxzcw88ugZb/BV/8A+pZXaQaNpdaHd1kONJmdAuJ6a44fjbeOwCVoNoIrcby3gEVDG8o/qKozh4adeuzz8t0G1+qUpaLzFLtkT+45GI3mavXUb1bKCkyLRgWGsTMPNNZEtKlLgDMCPj2GJ69dJhqG+jzheCvzrHWYSXEBONFZtXg2C3A3p8ta3RSC9GIYDMjeFIBksNXMMt1q58OBYUSl1U5770hlP7F95YV9jFfiZswxlLJrPLwlHyuARgM0iovkh9gQmj3fOPwmjHYm6klkCCTCVGRNY3ELA4o8ftVqSh2A9qNfsBftbHHQK9aM3/oAuOt39DnYLEAsVzV0ZIGWAUehG83qs9QUKwhNcNCdbnyx9EJ5qN12R9fofl7k3dJcV+EqjlsVBtL8aH0Q+Xq7HjN8+/BmpI7d2FwoWiunLEOq9Gu935efJ5UhoX/Hl2lWW/9GYx4hKOt/JCiXNlPpPdpMveZy6WIxQNASB+HTGcVz6oFpgL0S2mCzxmO5ZiTmuYVDCOaaGnRtosh7MsrPgCtbtydTW9jlv2zacuJpkubrZkRKeZGcYO0ou4y2bUXx6H3AzI+cxrAg99p1JHmdPz8K2Nlvns3q5s1xYbUpTRPqGcWzDwPwwEJpL81otjB03mXduoZXPuRRZcZjTeWT1vo1mDjpEKErJQYyMtTTAPmrG9VzqY3yriezk5wx42rGRQ5DdJuVJC54eLZgXufILqjivrZKZM/x6luzt8S9gvBVPUtdD7ziuUHMaME+izMaZUxC9qi7OvGptLB5dy/UzrOQbL2HuYZVPmUmYZNDhLIuk/SnSkQwW246TBZO1IrYH6M//wQCSM0xH237owa3ulb8Ur2EHwgDlXWmjimfu7cQhQmog2mz8xlWO99WuXI695o117zZJS5S6YzFDIcMjWr71QponmVYlGlBs1Sxdhg06TgM6EewQZa/tfpmJpsqMO2Yro0VT/3DibTvMI6GPu4duS7ZD+wQvpcuVvGLkVJ4xYZPqVcm67CNWDkWFQ/eoB3X7rrCaNLIkH246ASSPrGX3N0KC8hTPKlvbscNvO176m/lATk4j/djhW2Ywndigny/6yF7We7rES5Tkl8xRXKvf5NFKY755pG/giYPd8xNNI1YQ+K7cBr+1WWflKQgWGQ1TYx5/IFAvtS4/Zl1P31/17lcYcjDTJVrbB0yHMhYC62GTNnLrdl/SR7o5Ov24U9JedXD1au9s+4OTK1f29UuT8SNmrrmp43xqsB5nVOCuWzA4j0614dmalXBhCcsC8Nm56JU86ES90YJPxBh8JHkxBDRI8BwUw/G4XkAEwigcpHdIHWnWZOJTXA02fzmgxpFvsVBMj4r7LH2vHj9OUa0ZmxSvrv4SyCofpJzq9EnHqQ5B2wYLALPlq0RtBUI1lXHGgEAKmw4Jw4zoGmD5xW+V9GLCxe4GUy3tTxU43H7d8yMhEn+RMAtOQY3liC/4kVnESVlMZYgs36eO+vr6bSd4JFaB2ojPxO4gZJ4sX+NLI5anL7izxitYj3j4lpCTiOPW9OwovOkDgVha5Jcpmsnz8Rl7umjg5uBtnzCV+bvK5GbYH/po7du1zBRETE2ZBMS6yHi3Qpg+FSYYLtCnpp60KYQG5EFGNOcURndMlkKDRGBm0B820Wb1K87xyP7AS1rwJl723uvYuS2zxSlQp67oIZhXxkN2LqjasMzk0H/zoWicO8pKPH0JoOKUU7KywjybdNkUwAre/MP2a0sUpEFLTOI+mAJgZTqHjKu5vcTw9eNjXun452mWxmQyX89Fepj7F5OOq4GfWvKQddDTrAjihEyYSLHo0OhyGZVfF+AueXGQu4ArbS01gJolGftbJUz3wA0XZryxOAQEb5/AWHxc/RR/j/nyaZS5awktQqIVYv1utvGB6lzwHU1f/JjebKMgbCxGrIQqAnZzoa+8vUawEgDpLhwVtujNfdQObDpKuuNjwlNcLtrhw8PINbPsL0MqAQjJUlx9yi6A6DbrYpyFHVDSxnMVhi5nGJSkBXlkxC9P4N2OwRK7kpwIsCizTXwwv1nLOHZjlennJ8n/gSeUoYgX3trmhvXiq9RXY1zQM9Y8Z2vv2h+sBZPi3ueBwf8/s/NqjIOZDxy31Rdnvel/Oa4RuTYYFWGdskLhCnWSlwWNVpec+XpCY5CqDTHdZx+wqT6awGbZSRm/tqIAyWTDZOydTQBOkCDvJPNiGJkwd2RY3Zai8+hCRguC1gA9Wm3SeDIvGJUOAYB7liGZa7xRrmBL3oi5UnImmqjUZZy+KgC17eTO1jigREMu4CkIY+LYR5xPVCn0trVEgyWCZSXpmZyHUHb0hAUnLlZOdKfgWwX5wju+MwzJ2ojeOfu9qXdr/QY7oxE0I7ne/Bp5kgRVgMvLtXule1Mgksq35QHCWorKErN6d2f4jnuDex58Ge4T4UEN7Ii/+4MjIQP2ssSZzWE7KL2vUBPIlfDsMbTUEthktCY1syBE4xtm0RYdOxHakrKEuCgdK76rguig4QvxsFhveAsEAf8BEsO/EYxJwiiRz0Nysa5nIBxxU/ELdEaui+J9aF8C/5McNpOp/Q8s4pGQpcWc6iXD4L2lM4zWik0wmyBS66VYk7ea3BD91ZA5UaoObymQolb/F/N46MP+OfCBXCo0tbyHVqeMPA8HkTJUH6pQkArqr2VuI/GrV0eDs+xLa7COidromfYeAbHzon6KQrwIWCCsXCtoPbOeIyvdfR7KyqBGe/zLKMgpu/oHU9bCGYvuxdLtbNFGNIhEgkar/0OCAR9ZDlpH7zokntaaufbOzUevoklbbSCXwc0qxsIEd7+yXkRUhKgcDCsSua/d+x396AT3uUquW9d5Vy+erqKjzSbKuA/pP7KvU3UNxA33mAV3/Eu9FAJgzeeBaKM6Pt/ZN+vlnha+t8HTxCqqBV9E8PijKS0+DDTWIkwG3jxFNoxYN0Pw2VyRJBaWQM2F25ph1TIYgouAMHimrhDywv8L2bQv+NEzg/40rzYIZp3koIaRbFhUlCwMqk6dCJ8oKzjnnqD8CH1qyP7RYhNUFvDJfwMvL9o6bOd5nYeU3IZtR8cqljMMlORoFiPXr8FeuBIUY0wOg6v+xwJZI+6KTbbGLZYTuCcrsGAHnK2PW5q/3kdc+Ry9ZZ05okeoCWcVvxHFtIC6Uqm2K/hmxpb++znrm7RZAF11uusqjLB5rm/FcrH6P/za/vRJfJP1r2Tz6EtyRWbZvaDXxsC/ZciDmPqiq3oA7IobaDM+e2/LHGYlA1wPUiMm9pnfMrmObHoyt0eCc0elrRBor8l9zJGZAWmf7yMvqnb9B45lWa2WPDiO5HSruQO8E0B1/jE2mv09WCB62HnroUMiIw000SkCfv893EE+bSxYu2kt6cxcidhqQTEgVKd/LexmZdNxLmXRXrbij+6nZjr8DdSXXw/L/M6sRYfPUr+KXrlZBbQrcW27mJ8VCtDFwbyFEeDod6RsK8L8k6RHRZkNAanFseGHbaKBQ+aO+ThBxnkQ19RAvgC1z0/LjIitrhEW+wOp8Lz7W64rr+cW7qNFlTGNuwLfkXxANoL4nfEWEJm5zFqj9dUu3dOvvsgmyqdGU047KnQfmpUJOsqIA7dRu/zkDympADeNbrVax/pAw0Ykvh2G2nKlGLcx1XrOnXXSlOVG5wE3mXvSE2AMQMNeF/OBfGKtKEJk7oYvcSDaBR99NOxr1lFIYfNiHTVhVo6Czlh+uJ4nzpe0OkHarNTHFgJmaGrsD0GOHEayURTeU3fnytCLAE9aKPMrZM1c3DWMmDpEnt7oNZAzr85UTZk6dixZCMVUZO23/G9YziLQy1SLgA8i13ku4NegJPbgCuWvjKof7eHDDDhIcBgH/e4JHNgo71lZePuIESm8SIpFsGjd/YhNcOchllZcJHT12bI9DwIysWH0oe0J1/a8Ui2Km3TyoXPY8d3bbmShH76dx4QYLOgnKQL6dn1lBf+J+iP9zziUMpjWLy6L9OOdTYwPff8CzqXteVyu9gcZFnuzJ/2ZBsboq6O6WkJquSEYL1bOnvEovn3RTTU6aXyRDvAGYOjtAGq4M6f5r1hl3hOwLvTlkxv1f6SzanDuoklh1tj0tyG+uOCn/YMIW2HIp8cCPsAaL+zDwSAQD1D6Sdlzhc8735TfEB9Fe/P2RE8YVCWSTzF+Bzs+e0QXKo010fnumLYhvCR/gTGWzHYP4R8UZO29YOib5HbAJj2DcTzdYB4LfwA85GHKoSs0Db8EUbLV28gzUQUAn2DTME2qh4wr2vb1CEDAtKuM4nzdM6vOYJkFx0PCEZYyOWpfv5Vo8GunjBdsFLWq3kIbyRQWW6rge5cWkLRZWni+yOaSihJgJWpg2vYfLbQDShDAKBtWLwvK+1hmYDzDCmHfo1asvPhqTM2Xr+Buv8cIAEqG4XKGFVWxERDJPo2KuF///JQbHlVuDcYGPmMJtfz8PTtamBCKK5N1+wITctcQMgTvf77jx3UJVi7YL4zdzSG887rYtJUQut/3FbZB/K7r30anMUzi94Wzu8xYGK7x7C9uh1FCAPwTysH6d6Qx5PpeXkWhX/o6+EgwQ4aNYKYuTz6BA2aUXndPE+Ywm+eJ7Bb7vzRALxSAXAKcomvRtTPppxeFbgln4jTnO0U+r+tLzLkYeCl+8vlLMAiFf53dbW4eMCHSP9b/M7BZtRGd+9CML2Uxv2o5urFSFYoWlD9oIiR+iuqkrkVavEqIpSC4KfQ2OoNs+mPnl+9LzqDPz7ajGMAYDcAKpAOViezCT1hZWmjHSsVvf9nSi0Ic6LaPhI8qm0Lnif4/jDeb69cv0kCHz7Xtd9XZ9uUrTa1DKoJ+D+Vzz5GH5ELc4BWgqolV20wqvMD6pxA6H/tzb3DlMdGoppTqQNvhSYfU+G0ckeOZSZJrrdozrb31kXuIYU/w7VP7ZdlvXQrE9NSeW8dtO81OcG7bAk8xvNNxejzWNBlnXg/7iKqejBs2dICvsQAFb9V4KWNCj/dR768ODJb3nuhXjfmZ0qtEbFPESVdCjyXf6qkplvul3RJrt639+O3KELrlTpkmwU34v8jhUV56oZyhSxM2DcEs+1cx/Oftf3NxvBecb2w8l1K93UuNpfEyz2fpv9m4XgY0A6tDvuF7Ek97/tNdoTzgknCIv6hOgfJc2qWkP6ZxgL7CNRurTS0TGj6sWTIRhUsuGaqgdn/rrN1kRXrm3YtjwluN1D+F/ad3p4a8MGu1OCOKmzDRBS0r9SZ5YlTilwXb5lXP2cihifvaZ/XmvkVesIXAvmTiOXgImfni5WjumQmpA+7AYoXwI0mZ96/zSvFJ7NGfP1QA0s/ySbYq4CW09uCx/382aK/LXoTgU9DyDyQrC7J4GlpkxpFxqARxBELQPBTARUWw8nNbZs14TgWgN6hOqZYdbRoHqSkYJuKY4iyF3ePkgeBXmgvnMz6ljqNCynkHObQvzX5dUHeQHJbMc7W/cb78WhLnA95zykY3NsDX51d85nooLDiLYNe8zFAJ0zRVKI9vvkm3qm2AxhA6v1eRxQSxAwguS286xDwpIBmckT9+ZNiVRvPZ7Oq15lOjtLf7MkL9ETR4DAgB7LhPRpWh8a8b9R/f7sFZXULycJ5glFhUzh5L2UofRuUHXfN8YRX+8yvuqdNGSnilmauU2+H5DBOD/peFydTPe2lYAS7S2Ul357/IKE6CsXlejJS24PEUOnjSlua8STA6baJ/+fGLcbS9SRke1ylXnsllTrwOF0kIU6oy/lZ0qp78zU9opOILwvL8tnujw/PJ3tA4lWCeDBum0fKX/wb1sy8g8Exbzd/L3TKQZymvisdlCGrGsD/2qUy+oFV0Q9HkHKEMcwqFSzCYWNZxf5OmhwZ6KGCpPIrSF1RsJcdWMA1EosWgTd4wQNyXGtD9FpSBODmfyoZubKG993VeVfiNabhdih4NNrpE2Q1MK3rrkgWXXur6KpFLQfF0sK4w5IHzhhJxTEj9kYox2OJO3iGBnQPQly9tlzKIy3/qnoojawA57wil3ZQehZHbfulqXR/79rmI2KQx26zfpuFz5H7LNXKnjfwf01izPjyU04GYurmXyPBFY7o9Zm/HBfcapgt5z9rsjvPkhT72bJA6rLBzbQ3fqLDKXWddBkdIiIPDDhijlnS72EOVYaaJlgMLug62hz2c/iBb4oFN87jTOK5BsyzAIxvgn0O6VRlW5hQIzlkRIN8e/GIP7ku17wupHd/qljox455NdL05PSiRIyyLyh1Sq9Mtxxzy0HLdFPtmUEjCQ/yHy4YhiCLG/CYLhw41kqH8bSpn6QLl8cDtRfn3LJr8w6ezxY+DV9k4VPKVtUhfeJ68tpjxSN0UUP9cYGRelCvSvSsEXzAI19befwf+6wr698cSsZtylBKEBgGjEqrgreAlhEhX/5jwS+GAUVA6x3m2IT/1SNO4MlxNQQn1HxNfnkWwwvKOfWLBKb1BkY3qNt8qTbX8IoM6Nb+sl+E7aqHW14A4vxztuoUXSqMvBu/WAlHgt7KLg0V3W+WVv8+BirAIY+QE7a4jmrMCLWhMYvGm5/2jqWpeAi/TXmumOD1FKQOWJ33kp2NXmxUOYTCqSYm7d6oWfC5jr82QcksJdxvJvtSs1Yu1dEg2i+2bR2JHXo4PEabVFIU4xrzRGZOfkNMkJVmq9zFQNmuue1OYsl9G/sKZX/6rdNfeNBn79ljgGDQKhqy38JZ8ZxGER1lsHtMceNJZ7+RgdBAyPxkqRQBEKoFslmkNTWNgReGaNdHofwipWTu5AoUP6JKspBqe/sIxhiXwMsPu+NPf7IL5aY2TdGq6eqgSlMSy2h6QhaV7R6oxFbocXtCx2jc57JHgdNvcfe093zhvsCSh9Xcl2rMmK07WxTccBr9rotqKV+lrlYsY+cimiKRQFSQyOpEuZf0zBwjY1BTqvau7n6gIumSqkatJcvVXSFcFOTT3iJtQYqMtzInrmeYl9xda5XjZres/Xr7qVq5okhL+8jpq0JliIrADdY4576GcRQfnoAnVR72693W0CZ1OgK5sGKv9kIb43weJGGXV49loj91KLPhw8z93sxqugudarVSQYoy8zlu6oTGH5D41m2OB3k5jjdZy4Lz7Hh8ifTLFtgmdEzHZuaBmMN4zq2MfbwM/GSfM7QmCn3m4FIePwX5Ztm9WZJsfWZ4WQmHZ40Li3jDVCpRjP4gMVZtet+9KZxBLIE7dBOATab38/+6A21nuyh/b6aHG++tXu3jWol6ze9UgLr61hqH0Moa2vPla/3ZOgK3pRp6myPH+1xyHFin1gjdKuljqjT5fpLjC9lC4jrokBMl/M7x6q893lEIqzgpBDGLqj9QkVzUZNZmiyXfzXVwIE6qXc2GJfu/wxwMm5hhDhy7nFoFNWEvTP3Nz1tT1fPJqLxA107eiqUTf5W7SYA4CR5ewl3tCl3qYGCUJ5GHHIoi/Cd8Qj6hqDZbD4biGPrreJmlO3eLbQWDeN6aqRoz7QDIp4z8fCbsk+RAeCP9lZkNX+hvXKELaQRCzjXiPySDfWPfWNI/RS31JspLNH51gNeTsCv9vQpzfOGpRHQFfK3GhQxloBS0FuN5IYvCnNxjR7l+IaxarJgEMdSdLrkIQR4kmRk9Mo01+zNQOihs2OdYLRgesnlYzVMybEbpI1pspvVz2TlY7UsBG8a0g63WxPEAsGxak4qnS4OkbcU1lZIs9dqEUmXrfurc79za8wnRXwBbPzfJVZ2w/ssJe7613d10Xmb9XayVba9oa6DaSsjIBJzA/WoRzGbmZnGcSYEZChs0ZDdu8xOBeZgKHa8BUrnoSbxP2Ce6AuS0mTfwTRgJ5UwMhXAJrbe3tKq3n9NqLRpxGS2b0Luc3u3SGVUmkFAMAZeIy+P+phb8N34q6qx4ghMNdyY+wGfaIhRHdzDSFxaTaxvutrGJ7+EMgI8y94e8hC0Alq9ieE1YbBtwM8gi8Ov/cx0b+RVFqrjzhtNyi7NwKAgUKMkKDYLoFds1TpM6l/w2aUzo0NkXDATKMiJ9THeR3rxDI0oDaDCVIhle6rjev7JltPfpt7j+KgE6iv2D6Dx1KudQIxiUrdms/y1w0nybxMCJUP7ySMtCdRRjY2cxN2yF5ss+01K1FMSg3FeC42LfEcxl0YoP2LYKRtrVN/OT86Re8bJjsdASzCg6xR3rfRQKnrrNPvhACOlGRQ+kbGjXo7F1Pbpa/g42w2DiHlRP3+8PKsAgKn+0obhxzJjRRUWgrgBwTbdq6XciBCYnJKCkDP7ZkpPZur6t3lEBwiq0bUpaKTit3oyKvn4W4FHxa1aFz1u+hlbzEeHaGHilZxAlX/fW7Jt57IJO3qEWr34dpHHKX1GEivIoS8Y9vYAPezNcBDnQl0aTDvIxrlkaH3VnE2i03zRyOt3yMAAvchjWP7lS9jzgNGAnvdL14kog3qRhqWEtJGqnDw03gQXt+ddh6q63y7RGK5GdYO4Gw0thHodXgy85u2l1f8y4VHs6ff6ybx6y+4kpFNYUG6h1nBR+SfHih4q7GMzow4i8+rKfgy/ZHDxEEik76QANncAIYwj3e/etV5xmyHN3Alas2ElExhhYB2DP7vN2Hpfx3vZnoXfbGmoBsJQqWbP40GbKSu16DLpNHdAijees99Shc6nnhaiW4pc/HthEtHG3AIoYlyV2lGYLt7yOy5ELajl2yJDcaFPUK0jE4RMH9ZuuZzimzocgN9B+4Vew2QH/VMQx0c/kGzfZ6Rju7oL/vqJ07t0ivkN3GuJHdhxDUMA//xBiPXE902orXSpGohx89zhNFtv4eHiP7fS9KHaRZM48R8lW6pwTOEnC2A4UZrXZCHEi8pproh7GHyfaSCS6WWqSt+voq/bWvLQeTx2gvyI2zR/6wDlMAYUTW/IPOyao4Far4/Ej1+A5Zwf4UiR6SiQ+690On+WW8gTQjh3AXbeX/XHpxi20mFyGnst178XIAzDwHUQRLk+VdJ+Nhechou/D2ljoH2ZZ5xkMngVbqZYZZENvnv5GRwBDNtUxOZPZBcvgxfR99KI8kFvapUUQ7ejrPq/DBevYkW/qTR/fOXIdgIfAUQJuWID0Zxj8r8xVv1r2/cAEY9gI1332TW5HJQH0tIDavpLwwweUqOBLaf85GbiK2yjtI5NrlQiwIqXxR2bhoFpK01f7pI+g7ZSekGQ3ePgFYm2CvYS00V1svDXjL+zkkeKm/cTfvjxlaiulJlqW2fXIsPaa27ElX67uq14OmyXjI6wGhoatvj1uZELt/cFAk3/t/PwNu5Sbls+W8GT3qEpZv4h1kjgsRhaVI/IqH3tKlgmNoHTcQ4c7MdVEmT6xZWbWd98kjg6d4w4v1VZlufpbQEb6zG2Qj4tlNuJ8sZPgls19uTscsADIQNnOEX08vaLgV2IabxxlnaNVKkkb/y+2LsCAurXNYxxOfAkbelwxpL6E5LkxT39gM6w4w4oPaSpEw2sG/+aeDBcxlWTHjdgilb/ja/1rp5QCHHWac/O1wIkVZHts9F4u2tDasPm/eraUdXFHH7IoiZ+17Ou3n94LeJHFimjLyHw2B4tatFuG28DolwvwqXX1XYm9/J2YjgD2Sx0W5WkSnoGXFccgBDyWF3bE4OZJMdT6ZAAN23bq7sRfE8DYSv9ZU8nSD7D1+8E44RJJ+ZGj+A0BVHUkjb7CnGS5Vnrbt2d7sGlXnZSysd+rtVi+b52fZ1oolKKPZcPMsOaGvyT3DFNFSl1Ovz8ONngmPxllU/O0fMljxJpCfpPX8gO2WrnGOJFtE/IIW3LUlWOsKzxQBzCkJolT0INLh3j/8vOm9UhjJoaIkT0LHgOcYpvovm2I20o7dt8/fWAhM0vXw21c4Xm2rN4VwDht8ZgmuihHXtGTbG0rcEvLjH8I5SRhHVN4nFHkhzsQZh2nswQCmFl8lDsRuhoRJ+2w1beZAoKShkmvX0AUYrM7P578kCm66s8OAbDKAnpIcpy1AnvxnuhEObj5p7b0O2ImQJnwINS9ma2r1L9OEdsshzwXaJqOIo7UgX6iX+YkBYHOiyD81KmiFpNE5N2dQIwwz36vdHAHxwOZu50uL+98CUJ7cCrFB+5Z6ktrv7CYWATLEqwAFCkQ5BY/D1lavtw0EMg9s0b+Q+srmL/jWr0qmT70CZTBcd64r9XqsroTyifl8D3ve4kdznDGPxLkyb1KSQmL6MeZmySSfw5S63vs+4LBMDQOPKuuYr0r95NwfI6j1QPCjVVJLzyxWn95A6cQD/3f5zUzc5FYHZrDc/pYjieQ/UAWzfo1NcWvVUbLXvLXWpAtYmDGvn8GBwedZ1bWPUuGv6uHhRpAPzGSVHYARiY2NXySZ9N+UmFIEcmW+hMRQYukOODPwghY+UjpxgPyA4cf0gjSvAQSb5DV3cLtzhXUn8Sv1N0FXOUCmsYb/jf4KsYUeuycyL+/XBG0OTZqQMGRT75cVPXarNFNy45PafVMVetexil4iFHAg5DYuMxCo25hde6SNmauX1KVV7xaCY8dW9PG7Hzbe+2VtkeKJ6xcUYcbv0au3AJNntTJraulqMkOO8VCLY946ohHZkBtOHY/badIJfCNeAaKErZUovKOtULQxxz8wEKNrF0VS640hY545AP4Scmwm7nsXgOpHIrpg9181Pycqb4SClknejEZWZgOmLKbqgfTz54zcCW2b791n1CoaL1Ib/eVBSmBei9rFLdmB0uTC0VLSMa7LIJaumtWAmB+MTwOToKYnFeILCF9BPPVVGFCAMRSaXo5S6pC6XOuoTphb5aSV0n9s7flV8+0XlFt9UHVVKz9BDEHJt8f7tXRtXzZfdaTvN5ASjtE87gIM3/k1Voz3jiFcHRux3rMmmiE0I0xculK3/SVpMwtA3QNMEhkpBNg+6Di8SkF5lJsA7UtFMCm7pzMLnRXmmAiaBGgu8R4Ltg61IuCwZlGm7g8im3e7+HbfK4xfI+TB2KGWuxR9R3/g/cFyGQw1prEvB2Yv3JZ38Jelb5krf2tk179ECZKPz2P62JBDwTso54e3QzFwz8trPitC3+ObgTYTcWuOXLjgAzebgOD0G1LTM8KliDE6mNLd3xO9i5GLNh+EsfUMNXJTpc7CHZQD+l90K0nqLEwC6TfRT03PnAq19kyUmRgbqn7Mbf2ZndwuXVCTOYavUrtDEcebQxbij+v/UBg/scQDudMoIfc7VJmYB2TabGBVKG4M/BIWnIMF6mnsVVYNidyLwxRCGyKyWUZCqqsSFf1+sIZnGjeLt3gj9zT64cvXb+rl0d+xNqyeCN7C4trQ8ee5lQ8lvon3rP3zrOWEb8PAprqgl8jSbzRNuidZMcwFWCeLNlhUHejGTYHW0xPeGLZr25oMq7fkgUwXaP4V9vzjpXuN0NWML7Hb0R8B9s1UNCoG99YGLHGGQtI5a8TT2/3wwamvroED32MFbIoUQm1ho5pD/lJYvA7Dt1ZqqH2dCvDMht6f8+qNe/BNeBs0E7M+GNKaDUAWnIuqCBIVHA2dPzkcSALyAJlNalvdPOIU+pyAFnXyYF09xjJ6pDrD1yIVxjHP3MJHqjin/fJVNzJFxHRWj5MrtMqInA5khM/thUH2MI5btFht5UNW1d0se5zXyTmYe26mokWwhyZPJYH3LmjXdpYMLxcTWCE9wdOpkfkZYDr5rdzKHL7OPlzqXzeUr4oP/aZw3yWp7MfcwVrRc8BV5BFnESzmZQLv6S3yoI72M/oqcBrjs3uLz0LMqLWIVBVQcgkc1l37LKaAb2aqox3UwxtcW2mvOtKzDxGDv5A9qbtn2/IJHhMJAGPB+1audDtQJaN3VjrX229QWP+N3erjzXfZWyM4ygCG4JWHKFuFi1vNoRSVBOVtbzGCLEVZWRvaq+HSZsbzmKno1opW3q1WzsEry0W+u4pBnkVjwILfWZJ0PmyORj6EKi/0vJSq1IiPGEOlomlWdopkVFPdufEwA309p4Y2KLrzjKfqC9AKvfWL4oq1ogcfi7CfZTYe5YOkWvdaubz3p8j6k3cDF4RHieXd6fYxQkRgRR/MUlDtGSL/HX6x2RFk1GoN2D4LTNnonjxUcGGWlnEdxBmGqJk+E1jtG/QbGHNAsO0jw1zB5S7Apx7llZOdF77hf5Nn5Ty4LzVt4nDRKYIBdzb8H3oXU/KnYN8wjq9Vzmbh9RjAi1ZTRPtPrg+9WOio6/mGQziXEz07oO0yKhC8QQ04HoVlqb9VAaBaU/p0vqVn8hqglNYI+qvb7H+2kl+BOSHqqEdoQORFPcT0ZtBEvq8qUkF+UhxbZchlHXz2pFS56PVvtEMW38uhoRuVrD/FtB5jpFgFs2TqA/A1OiEqtWUx75eY/ll6PKR775kde6b+pT06UN4W+X8c+p4EN2qav68vbStXxZGtNFijDdrudZcyUXPA98L4sPLm9BBHVzVoPuhgiSB/lkccEja3AfEW8xXALmigVbcihkTJ8D5kvbIhiXksrnY03IL+3H+f142yWBFLxrc6+jg7eoOq6nm3RdNGdTzoLWoD7QnpziJYTLmADEZy6DQyHUnl0DTQe/nBnaewcJKFB8cPskxPXl28D0jfTlXiRBl3orqAKTo7SwI192BlZgwjb655F4cT2JPm/wPl2A3JSD1WyL3fjjBQNcHk4I48pdvJo2H8LYAazgtCb1WJsZcPdNzEqq28oSOdXs4ywVfYs6+bOAbsxIMCc9r8A42kcZC+Om/pIN0GAsud7hBsmJPCe43RvxOuKymEw2ERR2SgSYMsTNRDGOIRzSagpPNzUnwrKR3CgA9fOAU1OzBY5hoxRcQvL5Huga2RMy2yfJhT0JsKN3Sk08W2zOiCtl5CkENCYzQhtg2H5r5D877y/lUEySDl/rjwxHLrjyflLx1frKMnuvrtDPQlj27WvWgBZzoz0GSRlrNi7WwqprHuaJZXZ5RbVQdWJsiwCYIGpBBmMl6PSonSf0rFv8RB8Xu8XbWOF0UTxBxFt7bgjjINsesH7MIPrRJGZJZPByGMEdKdCvWiUoiou/ScN/TZXlz6u/UXBUJKH8XcZDtJ87c34+UxGnbRi7MmAgOudr8YIgMaVIlg3DLUBqgh26NasUlfEqvz3dh7QmgKE/eNkJRD2PWEQKNCsD9ztGzq+YpEdF0HKHlpCpHAGv+/q0n1hEUTZnp0fU/L8wOKUbL/bYGnhxO31uAIMbKdV73hgwLMUOcVwMeu2FAi3xpdzaGi7AMnmTBmhL24uuG49ELQ3vBSuDfZLFq29nL1rZlKkpyRf4kGjIEOn76atn9AArXtb8j254d++VCvylE0kBiiyypyDU2lBPjXNnwf6w67t4+OP8nlm+prw8zTeog2Mh0MlNsUQ+ZWt1By9u/BN8bNB2yqaegB+T4fqu/Vu/qO2iByDHzoK9UX8hGIazTrIMpqRGvW8S9XvbOov1DXwwlzaKrU2Xkil8BMVV3TdDJtf1EKNEBFAnIj54Hd5tqWC4r99D/TJQ0s+wrEIw+46Urvxh2ZTutAAAnAuxm9v+EpYhAi0gOf4ZPAHDz8aPlaK3VfFbJBGt8Ra8oATaOdkXJbcdljRHZLF/iJbyawD+nmmtOxq1AWk1TRja1rMdMbEbz124W63V71bVt0Gssw0sR4YsrsvylwXNrCnPpAKwqi1ncgM6N3X2pDBcU62V9FtHMRgxRgsA1143beO30aO+a6UsmaSE1R4/1MT+dSn/CzFkFOLYSt7KJrOtuuW/3Yo5O2ovEASfKZg+IMy5jqYf8Cie2uSyzshO06mVv8otw6IOck7LFE3fLDxgPqvz2MkxbCvbHDolPV7jTF+c2Zamt6l9rE/ZNX6zJh5l0ESAxKxDqOwermfkDViD+zbVu6x2U7Gzi/dxW9P21N0UKJySQ+gbpSgmzQr5kOWF1+HQ6iR7pmljW+RWby3zmfyp7W3JhEvqweszQKypJhXXbwvj83BeWrqN9KPvs5+XpVxcapouWY2AtVWKEBZ+LBB4rIzMLSQwgylnhfETnZR9qz5w+ii7sEveJo9o7wtL5S2EGOv+Hft+nA0mBpRARc+kDXpJtRgtLyAcMgJVsaTfI/KZYtcFaPQKzdFbJyu6bJqoHhN7x0ta8dMVWx+cnLPi+2qj6G4IdtHqLLLep2s3kok4IG9x4yYds5R4NYQXHMURX9EFfZVgF20IOS15atYSpXGfAqtGFWkR00K843G/qUwhV8LxdhZs3PbjsGE0PH5zdxIdL4wBF2mhV4WXZDVGADwRMQ7FRV/N2G3oJ5i/CA5eU3fZGgca2ubVFGlQSDTMxSy5p2b/5UjTaX5YAZ5Y7jbADzUhNm5a+/oflVGQaB5Py2Guuvu5Xx212MiqOMFHsutGekvCVQIP5NArPjnkRVJ7DVqVr6lf0EiFdoXOCWixZFBtapOgEVH5dF4gDJ2Ye/hEq3kA60MhPkWsNIYfX7PcPkvPoe3fxTtNeRlkIo26/Y2VFtodtECVGjciRGfcJ1oyHsHj/B9/c+Ay/OyPKeomFkkvTFouvGAZFnCA+FrJenpFHsx6XwK3W7IPh7i9xQAHvzeU313LyNEp3Tb1/wlYgEvZSmhLCsnVdieeDImK4KVoD+h+LiA+R4UWSod/3rNRai1u7XErY5330AqEjrMWgX1SjmmoCB0Jrvt+Gm4nP0gTsBNZfXIjAnACX+I4nsgaOqcz9w1vpl5BIyRoCJrlZD+Is2G0w3C67dTpu3L1RaTj6Hx6NOcrx12KehmiMak4SGxp8z+cjwGEUYB2VG2cJfCVz3/39A5DOfFKKH9pLHqnkdCxL6Dcg9uf2YqWSbeZVACYZlVoWTFNNuBbROSDu2yIN9aT5ICDqMmKrLmsNGoqkfggPJTulKc+VLUfK2MgqDzWeRbjX//PcTvNPwfI4N0Yd9q7rKtaxnEs2pcA+5lEqUUGBHUuWpreIUaMOgvXVsPj1HjTBeuJ7Fp8HlwPVEJoyAWxT+B9Fr32bKeUXCYex4HFmwh9WXs8BVJrwK043PmGuFiorbOvTQPU7lR0u86UVHxDALlf0MPPt7uo29cmngM72Dv7I5EukJw00fcuhmujKNgDLNRpD3/dHG8pUAhuyh7Y2gmxFmYt78nQ6L8hr97l0jOmLFLgl3hu+aibLyEZWyfEdsbftJbAOvuGP4wA+PTXCSpPcUEYeh5ZmTbENdyHoMsByUxipCa4l3W/pBzb0hF5TozWTDXSt4TzMjVBfBBfeZw5GZLnhIUhbbC8J8QPAQQgihg5bkD5bNiXGcgS0oD5MxVGwcNhmsiR3q/RryvDP/GMrJWNHbntvSYgmujzCTaQ6Ae+bISJQnnlp4PSfKjZnFFNoFYRY6sEkUsiZZfeBYjiSIp0CSA+fFf8q0GlBx3gjt7p30CRcNsDujxT8MXTI3zIwGRImnf7h0F7Csb1Vdpu2JPxcx5DoSSZFZBdPOHSCdh/Oe5/0wfeTjgmSd04aU1mNpe9uZZ4LUlFukchb5HkTcKskeud1vxsKaKBcR0xBtq/QfMt71nZMhgZg9t0T+ktcGpOPWIrRG4kCqSsuLZIEHnwmB3KOASeRFb4jMfTmyxp8K7Tsr1q0q49bVgZoraHOct3ec86iE2Oi1Rim1XnNgl1+XwcSNvm+f9dHca+BRtetMFd5x9yMA1EaHSWS8Wl0m4v0Z1f2NlMxxYr5zN/jSWmDxTjLmoL/ILdDIF1tCL9PcuFhdl0LMaA0cmfywdMtU9/MvQdHSm9aPHpify+lLT8GBwUGNA7fyAap2Pq15uM+ozQk/NhcXDY1SVqArgnwijnPZ7WnKnVPovnSAb7J/uQNtR+DuDa//cKT1jDe/EKpvvtFDqO4tnrCgncLmyYmWLMYjCr2MSAjeXUhWibKEJBUGI8rTfasTsXDc1KfY5qa+ehPc7M7lB0mQ+T5RF0s2uVwX9olyVbsO4Krg6dCChximBm1K095rJH3+hZ2MvHpfn6uiuX1nRoMH8ZysPUJ+iIB7R+GMno+9GDxIl1wA8+HMq9+HYLyewg+yE7OC+MBc0nMcLCeASKUtyG6qJYGj2+hNc4tu1N0rFVzsa5p36Lr2UqxMZ3kSitdsbcPgtC61SNYT+nVvwdhTKViPkM4jIRcY/pWWEOxGPr6HPywTopE7Rs3Bvtn+rvYSfl89pABbSnhV23Qrm3Bv7pnc18AftKcsXsoLhVrlG7f9LbNAugL6jk0sU8aNEauFP9sBWjq3hYKEMT6DsX8+zv0T5kEYBxQ74xctmDuY3x0sLYGrIV6SMXFmHbe1mnBaX2K7EHibsgezQ9oY/z11RSE03LzIrYkXGpJRgXwOCjD0Kw2xN3PmiFd3gU7M69olD5lYizjbmOtFy/pGQTSfuqyoIx002xVRw2FNZjf0UEAfdD8XUvjyZmkg/eTri6qyPs/dsyhpWT9BrwwQhdLbbBE+QRPXVV0q8mVPhhAs9qh+62RnblrQRH8+W0jJsnwKCqzg7u1ENLF3PD+FLmVUU5V7/hwzxAA8DdR7vulKYDPMfiFc1zaL4xzHPrv00HfyPor1C8XEMV4mCyWGIjDHaiAyPCnqCHpTxOKdC9prlAp2YjV1n3zNKmmCVPeovgT0skXKUdfwIKnist3v5kb24eKZzxnLJQBZHKgCWEbK4FoKmsNb2VC43GKOVH+k+fiFGiEaOSI9JHzkLKY610rWmxK+1Txl3vNlBhhaD43nXNwzzogC6ClpqQcugfMfdhB03cmZxc3GyBIVcYZ3QWUF996Zmx4NxZL0RyR47n9i1WmcRCd6AqzK3q3RGpH8fkCU99uZRBiF5WdRHS/gUb5geTNNP0fZV/kUIy3MCoAqMkGFlqtE5KI94z5L0/e9iUqjrkXjkE2bJ3jfJRnv5Aq5x+uwQchgVBgx3rukwYTQ/FngcY1L1OTqqzsV1dZKQB+ZxHtXfDqJoakfo6jmU3eV3QFlHZVSemfcq8Eh/smoqYHJwFxocCy7y7jmuy/DIdKUOhup11b3bq7G34JVl0X6mboYOcoGZ5wvzGXIqBdOvI7sKb773DurdzofeAwwNobNyO5p1IhQ3i6MeyAqcUkToLmprRA3JxKn+nOviBG1PONWmJWv/ZcCsRoKE8/JKQFrc7NUnDhimprHfoDgyqN4lkS8DQcpxMKCF/KmjQHbLaVQ9Akv7j1++L16Orex2omGCBYlXvjcOp+SDFqDvAU1WeMJzMqAP07KG1r0+6bztA4AJloIHohIMq8lBaOHLRhw01Tgr97UZZQy1yhEMVOmu2Wa6kvMhqaklswKmmihLF1vW/qkFtwpPcpot4cIBqj5kffIouwXVu2EKaLxoZcvQzqS1twVccbLiRMeFMB3+wywgONu1ziH8CoI42mcj0WFdIbuMAxmEZFX2pUOwT5GdqJDFUqPyhWuvT78IYmxGtfaPhSKM0FmPCMUPsFVSsx/3f5tTIWCxu9Xa3fFl/vkmsYabhCdC1pNhcMUILk8Oz5ZZwebItV0aUY+0AdbIYqS8G8nikoPrbWROS5leVqC4ap+tX79o5slIBkXxtkbfVMU8dkfG2zP3Qi++i64okapXhege+/yUMUkZTpQXLfbwQxWHPQsSQCcCXQE5QGTN/hrW9dQaD+K0suHG6RsyqtL/AXR9fjJ2fKRhRHs2atWpmsVedzkul+kUybAB6qgbDHp3cYh//Z21Bx2EPzwMWlcZ2rYKKRMXOBy3rA9TfyGSj7Umu+dH8EgrjlwuhvlA/21TFFwF2TsaF5bWLeoxb2hI7TA2xem4BErU4e+adJhlTIKB8z6d6lYw6anvtlonbx3aZzwsSQZj0VycKEMTs9jNhtj3G9qcz+ji8nl61AFqalyR3RbelKAqa2kqtkMEZyoiEpo4K/hR+TfhFsoxBq0h6massIfq5/vAH0JRAg4RODnnG8eQsgz7kEUXHNAyXMuCjs82rHJ8gGF9WxlBkT0OFe1NKlsHo2L3lzGUPtrdgAzhcXw251fDj0anm+pSc68AmDI2vR4kurSDPHbt04B6H6MpOjGz1txVExHly2UhpV9EcROb7XU3VsJtygbYVVEse0Hyi9uQYj2c5zbncduxRUFApYY3Cjj/J4obYfPzv0FtB3qRDa0CuOPAFHSMTGzIyPnsNJfVo+rM8AwVJP0C3k8Y7DvMFlmQviuE8OK6h+4zJXIv+EkJr0KVUm/bLGSH+U1suYQ27aHJz3a7b+Vh2Te9msoo7itx2QXr2Y4BcqNLsy1DnTaIiEySWZjq8gW+nxT/Cj2SYslwYfv2wymcWy0RtSyDN9PtBDA3kuQyu7efDuqv4dnO6kErATZ/ZPt50wk3LffiAhKHvr9pH/rtzZpfkIR3dW1nCyNtcYhd/DF+oUyJ0XxblDMORh42vQWNOWnzbTdUs2sQwcpK2rsi8Ayf3ot9QkMGAibnQELHfRrOHgHfQbVuvH139PF5x9YtS/xuCfZS0qKEbiR2vTT3dm77uYB6nTeCKlVXYJgTR+4rLipqBpTa0SbDRZ5b+vocufZxe7BwVtTxCFXJkKXKztad0yKlBaTGoWCgTELD4oc7Pegxa7pso4sNKl9Qu2oKhOIFhFipztSG5W/96oPp2VdW6q7mzVOYxsW7Hlb" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['frmMaster'];
if (!theForm) {
    theForm = document.frmMaster;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV13He1Qd6_TZFymLutAfKRxtvbL_IjVTgaeyoG9JzSHyEv8GsPsVLQKZkUg6f-SxEYfab_AfIC-KQ2&amp;t=636426603120000000" type="text/javascript"></script>


<script src="/Integrations/JQuery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/jquery-ui-1.11.4.min.js" type="text/javascript"></script>
<script src="/Integrations/Centralpoint/Resources/Controls/Page.js?v8.7.23" type="text/javascript"></script>
<script src="/Integrations/JQuery/Plugins/jquery.cp_Accordion.js?update=8.4.39" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yEk_YMl82vYnFaa5FD7acAOVmHN1OVAHW0odKyQQu9Q1bz3qnQXM6wMMgHgoP86fVuoAAaSWCN1in80BXnYa73tm4GfJTRouHqe9VVz8IN8t--lDrBHPNEDRfOXwvvzil1ZZgTlodaqVfkaxC99bGRQ1&amp;t=72fc8ae3" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Jw6tUGWnA15YEa3ai3FadKecVxFyabnEQR4tOwsjSsdRpQbmyjA4bVOgoHrMbcW5811kXDRzVZ-GK6zIS5XthJQUY7KY0Sw67VUrtqoFJzpSglNyrQEPkMJlbKngbD_LMBT5zr9AOCIcz3r91XC_1y3pKnxY2WvdMiz7vQe7E1I1&amp;t=72fc8ae3" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
var cpsys = {
  "StructuredData": {
    "page": [
      {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [
          {
            "type": "ListItem",
            "position": 1,
            "item": {
              "id": "https://www.mclaren.org/main/home.aspx",
              "name": "Home"
            }
          },
          {
            "type": "ListItem",
            "position": 2,
            "item": {
              "id": "https://www.mclaren.org/main/mclaren-web-privacy-policy.aspx",
              "name": "Website Privacy Policy"
            }
          }
        ]
      }
    ]
  }
};//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="43343198" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="v3Sh9ZxzSWHB75TgjQ5X8Z6uOd7cWPrH2B291V+yA3uyfG5z5Dv8ArTFDxu4KKxPfU84eBMBrxL70d6jHTdtO+qbCBLg5qwVVB5NShZXsAEnKEkU//MhdNjpEqjWBn4PqiPelbVI+jqsU2EVgRABlXo6XJWYisNPyp6u+z10z2Q=" />
</div>
	<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$smScriptManager', 'frmMaster', [], [], [], 90, 'ctl00$ctl00');
//]]>
</script>

	<script> 
$(document).ready(function(){
    $("#discover-expand").click(function(){
        $("#discover-expanded-container").slideToggle("slow");
    });
    $("#search-expand").click(function(){
        $("#search-expanded-container").slideToggle("slow");
    });
    $("#mobile-navigation-expand").click(function(){
        $("#mobile-navigation-container").slideToggle("slow");
    });
    $("#mobile-search-expand").click(function(){
       $("#search-expanded-container" ).insertAfter( "#mobile-navigation-container" );  
        $("#search-expanded-container").slideToggle("slow");
    });

});
</script>

<style>
</style>
<header>
<!-------Top Nav------>
<nav class="top">
<div class="inner-container" id="blue-navigaiton-bar">
<a href="#standardo"><img src="/Uploads/Public/Images/HealthPlan/skipnavigation.gif" alt="skip page navigation" width="1" height="1"></a>
<ul>
    <li><a href="/Main/mclaren.aspx">About Us</a></li>
    <li id="appointment-button"><a href="/main/patient-appointment.aspx" target="_blank" style="background: #0066a4;">Book an Appointment</a></li>
    
    
    <li id="mclaren-chart"><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a id="discover-expand" class="discover-mclaren-health-button" href="#">Discover McLaren</a></li>
    <li><img style="margin-left:13px; margin-top:13px;" src="/Uploads/Public/Images/Designs/nav-search-button.png" id="search-expand" alt="Search our website"></li>
</ul>
<div class="clear"></div>
</div>
<!------------Search Box-------------->
<div id="search-expanded-container" style="display:none;">
<div class="inner-container">
<div class="Search-Input">
<label for="HtmlSearchCriteria" hidden="hidden">Search Criteria</label>
<input style="" onkeypress="if (event.keyCode == '13') { document.location.href='/Main/Search.aspx?search=' + this.value; return false; }" placeholder="I'm looking for..." name="HtmlSearchCriteria" id="HtmlSearchCriteria" group="" type="text"></div>
<div class="Search-Button">
<label for="HtmlSearchGo" hidden="hidden">Search Now Button</label>
<input style="vertical-align: middle;" onkeypress="document.location.href='/Main/Search.aspx?search=' + document.forms[0].HtmlSearchCriteria.value;return false;" id="HtmlSearchCriteria" onclick="document.location.href='/Main/Search.aspx?search=' + document.forms[0].HtmlSearchCriteria.value;return false;" name="HtmlSearchGo" value="Go" src="/Uploads/Public/Images/Designs/nav-search-button.png" alt="Search Our Site" type="image"></div>

</div>
</div>
<!------------Search Box-------------->
<!------------Discover Box-------------->
<div id="discover-expanded-container" style="display:none;">
<div class="inner-container">
<ul class="discover-list">
    <li class="header">McLaren Services</li>
    <li><a href="/Main/bariatric-services.aspx">Bariatrics</a></li>
    <li><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li><a href="/Main/imaging-services.aspx">Diagnostic Imaging</a></li>
    <li><a href="/Main/birth-center-services.aspx">Family Birth Place</a></li>
    <li><a href="/mclaren-homecare-group/mclarenhomecaregroup.aspx">Home Care</a></li>
    <li><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></li>
    <li><a href="/mclaren-homecare-group/laboratory-services.aspx">Lab</a></li>
    <li><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li>
    <li><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></li>
    <li><a href="/Main/robotic-services.aspx">Robotic Surgery</a></li>
    <li><a href="/Main/surgery-services.aspx">Surgical Services</a></li>
    <li><a href="/Main/womens-services.aspx">Women’s Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">McLaren Health Plan</li>
    <li><a href="http://www.mclarenhealthplan.org/community-member/mhp.aspx" title="link to medicare advantage information">Community - Large Group, Small Group, Individual</a></li>
    <li><a href="http://www.mclarenhealthplan.org/healthy-michigan-member/mhp.aspx" title="link to healthy michigan information">Healthy Michigan</a></li>
    <li><a href="http://www.mclarenhealthplan.org/mhcc-member/mhp.aspx" title="link to mclaren health advantage information">McLaren Health Advantage</a></li>
    <li><a href="http://www.mclarenhealthplan.org/medicaid-member/mhp.aspx" title="link to medicaid and michild information">Medicaid / MIChild</a></li>
    <li><a href="http://www.mclarenhealthplan.org/medicare-supplement-member/mhp.aspx" title="link to medicare advantage information">Medicare Supplement</a></li>
    <li><a href="http://www.mclarenhealthplan.org/community-member/state-of-michigan-mhp.aspx" title="link to State of Michigan employee information">State of Michigan Employees</a></li>
</ul>
<ul class="discover-list">
    <li class="header">McLaren Hospitals</li>
    <li><a href="/BayRegion/BayRegion.aspx">McLaren Bay Region</a></li>
    <li><a href="/bayspecialcare/BaySpecialCare.aspx">McLaren Bay Special Care</a></li>
    <li><a href="/caroregion/caroregion.aspx">McLaren Caro Region</a></li>
    <li><a href="/CentralMichigan/CentralMichigan.aspx">McLaren Central Michigan</a></li>
    <li><a href="/Flint/Flint.aspx">McLaren Flint</a></li>
    <li><a href="/Lansing/Lansing.aspx">McLaren Greater Lansing</a></li>
    <li><a href="/LapeerRegion/LapeerRegion.aspx">McLaren Lapeer Region</a></li>
    <li><a href="/macomb/macomb.aspx">McLaren Macomb</a></li>
    <li><a href="/northernmichigan/northernmichigan.aspx">McLaren Northern Michigan</a></li>
    <li><a href="/Oakland/Oakland.aspx">McLaren Oakland</a></li>
    <li><a href="/lansing/orthopedic-services.aspx">McLaren Orthopedic Hospital</a></li>
    <li><a href="/porthuron/porthuron.aspx">McLaren Port Huron</a></li>
    <li><a href="/thumbregion/thumbregion.aspx">McLaren Thumb Region</a></li>
</ul>
<ul class="discover-list" style="margin-right:0px;">
    <li class="header">McLaren Difference</li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a href="/mhp/mhp.aspx">Health Advantage</a></li>
    <li><a href="http://www.karmanos.org" target="_blank">Karmanos Cancer Institute</a></li>
    <li><a href="/Clarkston/Clarkston.aspx">McLaren Clarkston</a></li>
    <li><a href="/Main/Home.aspx">McLaren Health Care</a></li>
    <li><a href="/mhp/mhp.aspx">McLaren Health Plan</a></li>
    <li><a href="/aco/homepage.aspx">McLaren High Performance Network, LLC</a></li>
    <li><a href="/mclaren-homecare-group/mclarenhomecaregroup.aspx">McLaren Health Management Group</a></li>
    <li><a href="/mclaren-homecare-group/laboratory-services.aspx">McLaren Medical Laboratory</a></li>
    <li><a href="/mclaren-homecare-group/pharmacy-services.aspx">McLaren Pharmacy</a></li>
    <li><a href="/mclaren-physician-partners/mpp.aspx">McLaren Physician Partners</a></li>
    <li><a href="/Main/foundation.aspx">Our Foundations</a></li>
</ul>
<br style="clear:both;">
</div>
</div>
<!------------Discover Box-------------->
</nav>
<!-------Top Nav------>
<!-------Logo Row------>
<div class="inner-container">
<div class="logo">
<a href="/main/home.aspx"><img src="/Uploads/Public/Images/header.jpg" alt="company logo links to website"/></a>
</div>
<div class="mobile-accordion-icons"><img style="margin-right:8px;" src="/Uploads/Public/Images/Designs/mobile-search-button.jpg" id="mobile-search-expand" alt="search button"><img style="" src="/Uploads/Public/Images/Designs/mobile-nav-button.jpg" id="mobile-navigation-expand" alt="mobile navigation expand button"></div>
<div class="second-nav">
<ul>
    <table cellpadding="0" cellspacing="0" border="0" role="presentation">
	<tr>
		<td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation73&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation73&#39;).hide();" onfocus="$(&#39;#cpPortableNavigation73&#39;).show();" onblur="$(&#39;#cpPortableNavigation73&#39;).hide();" id="cpPortableNavigation76a224fb-2841-4c3b-b9bc-237b847fccbe">
			<li id="services-nav-item" class="second-nav-services"><a href="#" onclick="return false;">Services</a></li>
		</div><div id="cpPortableNavigation73" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;width:600px;overflow:hidden;">
			<style>
    .featured-service-alpha .grey {color: #cccccc !important; font-weight:normal !important;}
    .featured-service-links{height:250px; width:750px;}
    .featured-service-links a{font-size:13px; color:#5f6062; padding:0px; margin:0px; font-family:'Alegreya Sans', sans-serif; text-decoration:none;}
    .featured-service-links a:hover{text-decoration:underline;}
    .featured-service-links p{margin:0px; padding:0px; line-height:20px;}
    .featured-service-alpha a:hover{text-decoration:underline;}
    .all-services-p{display:block !important;}
</style>
<div class="services-drop-down">
<div class="services-left-column">
<h5 style="padding-left:31px;">Featured Services</h5>
<!----Services of Excellence DataSource----->
<ul>
    <li class="services-of-excellence"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></li>
    <li class="services-of-excellence"><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li class="services-of-excellence"><a href="/Main/cardiac-care.aspx">Cardiology Services</a></li>
    <li class="services-of-excellence"><a href="/Main/emergency-services.aspx">Emergency Services</a></li>
    <li class="services-of-excellence"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></li>
    <li class="services-of-excellence"><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></li>
    <li class="services-of-excellence"><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></li>
    <li class="services-of-excellence"><a href="/Main/stroke-services.aspx">Stroke Services</a></li>
    <li class="services-of-excellence"><a href="/Main/trauma-services.aspx">Trauma Services</a></li>
    <li class="services-of-excellence"><a href="/Main/womens-services.aspx">Women's Health</a></li></ul>
<!---img style="" src="/Uploads/Public/Images/Designs/Main/services-drop-down-image.png"--->
</div>
<div class="services-right-column">
<h5>Our Services</h5>
<!----------------------->
<div class="featured-service-links">
<div class="featured-services-all">
<table cellpadding="0" cellspacing="0" border="0" width="100%" role="presentation">
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/athletic-medicine.aspx">Athletic Medicine Institute</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/locations.aspx?taxonomy=podiatry6">Podiatry Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/heart-failure-clinic.aspx">Heart Failure Clinic</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/locations.aspx?taxonomy=westbranch2">Primary Care West Branch</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/home-care.aspx">Home Care</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/pulmonary-services.aspx">Pulmonary Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/birth-center-services.aspx">Birth Center</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/rehabilitation-services.aspx">Rehabilitation & Physical Therapy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/blood-services.aspx">Blood Conservation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/imaging-services.aspx">Imaging Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/breast-care.aspx">Breast Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infection-prevention.aspx">Infectious Prevention </a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/senior-services.aspx">Senior Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cancer-care.aspx">Cancer Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/infusion-services.aspx">Infusion</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/sleep-services.aspx">Sleep Center</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cardiac-care.aspx">Cardiology Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/laboratory-services.aspx">Laboratory</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/spine-services.aspx">Spine, Neck and Back</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/northernmichigan/cheboygan-campus.aspx">Cheboygan Outpatient Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/lakeorion/subsite-home.aspx">Lake Orion Nursing and Rehabilitation</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/stroke-services.aspx">Stroke Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/convenient-care.aspx">Convenient Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/marwood/homepage.aspx">Marwood Nursing and Rehabilitation</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/surgery-services.aspx">Surgery Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/locations.aspx?taxonomy=dermatology7">Dermatology</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/bayspecialcare/bayspecialcare.aspx">McLaren Bay Special Care</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/tavr.aspx">TAVR</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/home-care-medical-supplies-mhg.aspx">Medical Supplies & Equipment</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/trauma-services.aspx">Trauma Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/dialysis-services.aspx">Dialysis</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urgent-care.aspx">Urgent Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/emergency-services.aspx">Emergency Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urology-services.aspx">Urology</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ems-services.aspx">EMS</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/vascular-services.aspx">Vascular Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/locations.aspx?taxonomy=endoscopy2">Endoscopy</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/oakland/oxford-campus.aspx">Oxford Campus</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/vein.aspx">Vein Center</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ear-nose-throat-services.aspx">ENT: Ear Nose and Throat</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pain-center-services.aspx">Pain Center</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/womens-services.aspx">Women's Health</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/epilepsy-services.aspx">Epilepsy Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pediatric-services.aspx">Pediatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/eye-care.aspx">Eye Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></p></div></td>
		<td style="vertical-align:top;" width="34%"></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/main/foundation.aspx">Foundation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/plastic.aspx">Plastic Surgery</a></p></div></td>
		<td style="vertical-align:top;" width="34%"></td>
	</tr>
</table>
</div>
<p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/mhg-home-care.aspx">Home Care</a></p><p><a href="/Main/mhg-home-infusion.aspx">Home Infusion</a></p><p><a href="/Main/mhg-hospice.aspx">Hospice</a></p><p><a href="/Main/mhg-lab.aspx">Lab</a></p><p><a href="/Main/mclaren-bay-special-care.aspx">McLaren Bay Special Care</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/mhg-pharmacy.aspx">Pharmacy</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p>
</div>
<!------------------------------>
</div>
<br style="clear:both;">
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigationc1fdecc8-73a4-478e-b7f9-da0884005b89">
			<li class="find-a-physician"><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation76&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation76&#39;).hide();" onfocus="$(&#39;#cpPortableNavigation76&#39;).show();" onblur="$(&#39;#cpPortableNavigation76&#39;).hide();" id="cpPortableNavigation4f92167d-700e-4912-a762-f01a0b4e052d">
			<li class="patient-and-visitor-info"><a href="/Main/patient-and-visitor.aspx">Patient &amp; Visitor Info</a></li>
		</div><div id="cpPortableNavigation76" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/patient.aspx" target="_self">Patient Information</a><ul><li class="featured-service-style"><a href="/main/patient-file-a-complaint.aspx" target="_self">Filing a Patient Complaint</a></li><li class="featured-service-style"><a href="/main/patient-opt-out.aspx" target="_self">Fundraising Opt-Out</a></li><li class="featured-service-style"><a href="/main/patient-mymclaren.aspx" target="_self">MyMcLaren Chart</a></li><li class="featured-service-style"><a href="/main/patient-notice-privacy.aspx" target="_self">Notice of Privacy Practices</a></li><li class="featured-service-style"><a href="/main/patient-donation.aspx" target="_blank">Online Donation</a></li><li class="featured-service-style"><a href="/main/patient-pre-registration.aspx" target="_self">Patient Pre-Registration</a></li><li class="featured-service-style"><a href="/main/patient-appointment.aspx" target="_self">Schedule Appointments</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/financial.aspx" target="_self">Financial Information</a><ul><li class="featured-service-style"><a href="/main/financial-billing1.aspx" target="_self"> Pay Your Bill Online</a></li><li class="featured-service-style"><a href="/main/financial-services.aspx" target="_self">Financial Assistance</a></li><li class="featured-service-style"><a href="/main/financial-insurance.aspx" target="_self">Insurance Information</a></li><li class="featured-service-style"><a href="/main/financial-participating-plans.aspx" target="_self">Participating Health Plans</a></li><li class="featured-service-style"><a href="/main/financial-insurance-veterans.aspx" target="_self">Veterans Choice Insurance</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/visitor.aspx" target="_self">Visitor Information </a><ul><li class="featured-service-style"><a href="/main/visitor-parking-and-directions.aspx" target="_self">Parking and Directions</a></li></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation77&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation77&#39;).hide();" onfocus="$(&#39;#cpPortableNavigation77&#39;).show();" onblur="$(&#39;#cpPortableNavigation77&#39;).hide();" id="cpPortableNavigationbf4fa1a8-3c75-4ea8-97c5-67f9504e6f30">
			<li class="health-and-wellness"><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
		</div><div id="cpPortableNavigation77" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-your-health.aspx" target="_self">Your Health</a><ul><li class="featured-service-style"><a href="/main/health-clinical-trials-link.aspx" target="_self">Clinical Trials</a></li><li class="featured-service-style"><a href="/main/health-community-needs.aspx" target="_self">Community Health Needs Assessment</a></li><li class="featured-service-style"><a href="/main/health-information.aspx" target="_self">Health Information</a></li><li class="featured-service-style"><a href="/main/health-navigator.aspx" target="_self">Health Navigator</a></li><li class="featured-service-style"><a href="/main/health-infection-prevention.aspx" target="_self">Infection Prevention</a></li><li class="featured-service-style"><a href="/main/health-my-checkups.aspx" target="_self">My Checkups Health Tool</a></li><li class="featured-service-style"><a href="/main/health-mymclaren-chart.aspx" target="_self">My McLaren Chart</a></li><li class="featured-service-style"><a href="/main/health-wellness-tools.aspx" target="_self">Wellness Tools</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-classes-programs.aspx" target="_self">Classes & Programs</a><ul><li class="featured-service-style"><a href="/main/health-classes.aspx" target="_self">Classes and Events</a></li><li class="featured-service-style"><a href="/main/health-support-groups.aspx" target="_self">Support Groups</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-mclarenhealthplan.aspx" target="_self">McLaren Health Plan</a><ul></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigation52587aa1-b090-4537-9c98-40261a490ab3">
			<li class="our-facilities"><a href="/Main/locations.aspx">Our Locations</a></li>
		</div></td>
	</tr>
</table>
</ul>
</div>
<div style="clear:both;"></div>
</div>
<!-------Logo Row------>
</header>
<div id="mobile-navigation-container" style="display:none;">
<ul class="mobile-menu">
    <li>
    <div class="mobile-navigation-accordion">Services</div>
    <div class="mobile-navigation-content">
    <p><a href="/Main/athletic-medicine.aspx">Athletic Medicine Institute</a></p><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/birth-center-services.aspx">Birth Center</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology Services</a></p><p><a href="/northernmichigan/cheboygan-campus.aspx">Cheboygan Outpatient Services</a></p><p><a href="/Main/convenient-care.aspx">Convenient Care</a></p><p><a href="/Main/locations.aspx?taxonomy=dermatology7">Dermatology</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Services</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/locations.aspx?taxonomy=endoscopy2">Endoscopy</a></p><p><a href="/Main/ear-nose-throat-services.aspx">ENT: Ear Nose and Throat</a></p><p><a href="/Main/epilepsy-services.aspx">Epilepsy Services</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/heart-failure-clinic.aspx">Heart Failure Clinic</a></p><p><a href="/mclaren-homecare-group/home-care.aspx">Home Care</a></p><p><a href="/mclaren-homecare-group/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging Services</a></p><p><a href="/Main/infection-prevention.aspx">Infectious Prevention </a></p><p><a href="/mclaren-homecare-group/infusion-services.aspx">Infusion</a></p><p><a href="/mclaren-homecare-group/laboratory-services.aspx">Laboratory</a></p><p><a href="/lakeorion/subsite-home.aspx">Lake Orion Nursing and Rehabilitation</a></p><p><a href="/marwood/homepage.aspx">Marwood Nursing and Rehabilitation</a></p><p><a href="/bayspecialcare/bayspecialcare.aspx">McLaren Bay Special Care</a></p><p><a href="/mclaren-homecare-group/home-care-medical-supplies-mhg.aspx">Medical Supplies & Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedic Services</a></p><p><a href="/oakland/oxford-campus.aspx">Oxford Campus</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatric Services</a></p><p><a href="/mclaren-homecare-group/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/plastic.aspx">Plastic Surgery</a></p><p><a href="/Main/locations.aspx?taxonomy=podiatry6">Podiatry Services</a></p><p><a href="/Main/locations.aspx?taxonomy=westbranch2">Primary Care West Branch</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary Services</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehabilitation & Physical Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotic Assisted Surgery</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep Center</a></p><p><a href="/Main/spine-services.aspx">Spine, Neck and Back</a></p><p><a href="/Main/stroke-services.aspx">Stroke Services</a></p><p><a href="/Main/surgery-services.aspx">Surgery Services</a></p><p><a href="/Main/tavr.aspx">TAVR</a></p><p><a href="/Main/trauma-services.aspx">Trauma Services</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/vascular-services.aspx">Vascular Services</a></p><p><a href="/Main/vein.aspx">Vein Center</a></p><p><a href="/Main/womens-services.aspx">Women's Health</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p>
    </div>
    </li>
    <li><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
    <li>
    <div class="mobile-navigation-accordion">Patient &amp; Visitor Info</div>
    <div class="mobile-navigation-content">
    <p><a href="/Main/patient.aspx">Patient Information</a></p>
    <p><a href="/Main/Financial.aspx">Financial Information</a></p>
    <p class="mobile-navigation-link-visitor"><a href="/Main/Visitor.aspx">Visitor Information</a></p>
    </div>
    </li>
    <li><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
    <li><a href="/Main/locations.aspx">Our Facilities</a></li>
    <li id=""><a href="/main/foundation.aspx">Ways To Give</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren.aspx">About Us</a></li>
    <li id="health-care-professionals-mobile" class="top-nav-mobile"><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li class="top-nav-mobile" ><a href="/main/patient-appointment.aspx">Book an Appointment</a></li>
    
    <li class="top-nav-mobile"><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li class="top-nav-mobile"><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li class="top-nav-mobile"><a href="/Main/career.aspx">Careers</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
</ul>
</div>
	
	<div class="cpweb_PerimeterMiddle">
		<div id="blPerimiter" class="cpsys_Block">
	
			
			<div id="tdPerimeterCenter" class="cpsys_BlockColumn">
		<div id="divWrapper" class="cpweb_Wrapper">
	<div id="cphBody_divTop" class="cpsty_Top">
		
		
		<div id="cphBody_divTopAc2" class="cpsty_SiteTypes_Default_TopAc2"><div id="cpsys_Advertisers_af343aee-a245-4e1d-9da4-6d08bbbd30c5" style="text-align:left;">
	<div class="breadcrumb-trail"><div class="Breadcrumb"><span><a href="/main/home.aspx" target="_self">Home</a> &gt; </span><span><span>Website Privacy Policy</span></span></div></div>
</div></div>
	</div>
	<div style="clear:both;">
		<div id="cphBody_blSiteType" class="cpsys_Block cpsty_blSiteType">
			
			
			
			<div id="cphBody_tdCenter" class="cpsys_BlockColumn cpsty_CenterTd" style="width: 99%;">
				
				<div id="cphBody_divCenter" class="cpsty_Center">
					<div id="cphBody_divCenterAc1" class="cpsty_SiteTypes_Default_CenterAc1"><div id="cpsys_Advertisers_bf3115e5-4271-4bad-91ed-a4aa140e412a" style="text-align:left;">
	<style>
    #data-source-created{display:none;}
    #service-line-datasource{display:none;}
</style>
<div class="left-rail-nav-container">
<ul parentsystemtype="FirstTierList" id="site-nav"><li class="liParent"><a href="/main/mclaren.aspx" target="_self" class="d173dbee-de0f-4132-ba5e-76a09f076348">About Us</a><ul><li class="liParent"><a href="/main/mclaren-awards.aspx" target="_self" class="64b1e7ee-84b2-4587-afe5-5d74ca900868">Award Winning Care</a></li><li class="liParent"><a href="/main/mclaren-community-assessment.aspx" target="_self" class="f0856316-de0f-442c-8207-78ea6ee5b5ce">Community Health Needs Assessment</a></li><li class="liParent"><a href="/main/mclaren-contact-us.aspx" target="_self" class="25423667-c62c-4a08-89f5-0296d2ca22f8">Contact Us</a></li><li class="liParent"><a href="/main/mclaren-doing-whats-best.aspx" target="_self" class="d91d2260-de3f-45ab-9974-6d3d12267efe">Doing What's Best</a></li><li class="liParent"><a href="/main/mclaren-history.aspx" target="_self" class="6c6781bf-645e-4190-9b15-b1df37f62521">History</a></li><li class="liParent"><a href="/main/mclaren-executives.aspx" target="_self" class="84efe6f0-cf94-4b18-9e0b-9ce8374387c1">Meet the Executive Team</a></li><li class="liParent"><a href="/main/mclaren-publications.aspx" target="_self" class="a424848b-5c16-49fe-ba74-05c565299104">Publications and Videos</a></li><li class="liParent"><a href="/main/mclaren-research.aspx" target="_self" class="8c52cd2f-a2bd-40fe-bff6-a7fd50955fff">Research Studies</a></li></ul></li></ul>
</div>
</div></div>
					
					<div id="cphBody_divContent" class="cpsty_SiteTypes_Default_Content">
	

	
			
<!--cpsys_Template:cpsys_Register-->
<!--cpsys_Template:cpsys_Register-->


<!--cpsys_Template:DetailsHeaderContent-->
<main id="inside-page"><div class="page-content mod-details" id="top">
<h1 class="nav-display-name">
Website Privacy Policy
</h1>
<!--cpsys_Template:DetailsHeaderContent-->


		<div class="left-rail-nav-container" id="service-line-datasource">

</div>
<div class="main-content">
<!---Shows Location Info--->

<style> 
/*----Styles For SubPages-----*/
.cpsty_CenterTd{width:100% !important;}.cpsty_Center h1{margin-top:0px;}.site-banner .inner-container{min-height:115px;}.site-banner .inner-container h1{line-height:115px; font-size:34px;}.site-banner .inner-container h1.multi-line{line-height:normal !important; font-size:34px !important; height:100px !important;}.site-banner{height:173px; background-color:#2f82b6; background-image:none;} .site-banner .inner-container .site-banner-logo{display:none} .service-location-on-landing-page-top{display:none;} .service-location-on-landing-page{display:none;} .left-rail-nav-container{display:block; width:24.5% !important; margin-right:2%; float:left; border:solid 1px #cccccc;} .main-content{width:72.5%; float:left;} @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}
.cpsty_LeftTd {display: none;}
    h1.nav-display-name {display: none;}
/*----Styles For SubPages-----*/
</style>

<h1 class="pageheader">Website Privacy Policy</h1>
<div class="service-location-on-landing-page-top"></div>
<p>McLaren Health Care Corporation cares about privacy issues and wants you to be familiar with how we collect, use and disclose Personally Identifiable Information you provide to us.</p>
<p>This Privacy Policy and Terms (the “Policy”) describes our practices in connection with Personally Identifiable Information that we collect through our website located at www.mclaren.org, the websites of our subsidiaries that display or directly link to this Policy, and the e-mail messages that we send to you that directly link to this Policy (collectively, the “Services”).</p>
<p>There are a number of circumstances where this Policy does not apply and we want to identify a few of them for you:</p>
<ul>
    <li>This Policy does not apply to the information you share when receiving treatment from your medical provider, including when you are receiving medical services from us. For example, this Policy would not apply to the information you share with us when admitted to the emergency room. The use and disclosure of the information you provide in such circumstances is governed by the Federal Health Insurance Portability and Accountability Act of 1996, more commonly known as HIPAA, as well as Michigan law. You can learn more about your HIPAA rights and protections and our obligations in our Notice of Privacy Practices.</li>
    <li>This Policy does not apply to the information you share and your use of the <em>My McLaren Chart</em>, which is the patient portal we offer. The use and disclosure of the information you provide when using the <em>My McLaren Chart</em> is governed by HIPAA, Michigan state law, as well as the terms and conditions and other policies you agreed to when registering to access the <em>My McLaren Chart</em> service.</li>
    <li>This Policy does not apply to the practices of companies that we do not own or control. Other services and organizations that we link to may have their own privacy policies governing the use of information you provide to them. For example, organizations such as Facebook, Twitter and LinkedIn each have their own privacy policy and practices.</li>
    <li>This Privacy Policy does not apply to individuals whom we do not employ, including any of the third parties to whom we may disclose user information as set forth in this Privacy Policy.</li>
</ul>
<p>If you have any questions or comments about this Policy or our privacy practices, please contact us by using one of the means listed on our <a href="/main/mclaren-contact-us.aspx">Contact page</a></p>
<ol>
    <li><strong>Personally Identifiable Information We May Collect</strong>
    <p>“Personally Identifiable Information” is information that identifies you as an individual, such as name, birth date, telephone number, e-mail address, or unique device identifiers that was gathered in connection with your use of the Services.</p>
    <p>If we combine Personally Identifiable Information with protected health information subject to protection under HIPAA, the combined information will be treated as protected health information for as long as it remains combined.</p>
    </li>
    <li><strong>Other Information we may collect</strong>
    <p>“Other Information” is any information that does not reveal your specific identity or does not directly relate to an individual, including, for example: (1) computer or device connection information, such as browser type and version, operating system type and version, device information, and other technical identifiers; (2) information collected through cookies and other technologies; or (3) aggregated information, such as usage history and search history.</p>
    <p>If we combine Other Information with Personally Identifiable Information, the combined information will be treated as Personally Identifiable Information for as long as it remains combined.</p>
    </li>
    <li><strong>Security Measures</strong>
    <p><strong>Virus Detection and E-mail Security:</strong>&nbsp;For website security, we use software programs to monitor traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage.&nbsp; </p>
    <p><strong>General Practices: </strong>Although we seek to use reasonable measures to protect Personally Identifiable Information, please be aware that no security measures are perfect or impenetrable. Therefore, we cannot and do not guarantee that the information you provide to us through the Services&nbsp;will not be viewed by unauthorized persons. We are not responsible for circumvention of any privacy settings or security measures contained on the Services.</p>
    <p><strong>Security of Service Communications</strong>: Some communication through the Services may be sent through the standard HTTP protocol and may be delivered using regular e-mail. Information sent over HTTP is not encrypted. E-mail, while convenient, also poses several risks (e.g., e-mail is not a secure form of communication, is unreliable, can be forwarded, etc.). We cannot guarantee the security of the information sent through such means, nor can we guarantee that information you supply to us will not be intercepted while being transmitted to us. It is important for you to protect against unauthorized access to your computer and to take appropriate security measures to protect your information. We use encryption technology, such as Secure Sockets Layer (SSL), to protect your protected health information during data transport with <em>My McLaren Chart</em>.</p>
    <p><strong>Your Obligations to Safeguard your Information</strong>: Security is not a one person job. You must also take reasonable measures to protect your information, including, for example, securing your computer and mobile device, using an antivirus software, using a firewall, and other similar safeguards.</p>
    </li>
    <li><strong>Disclaimer</strong>
    <p>McLaren Health Care Corporation and its subsidiaries offer information on the Services for general educational purposes only. This information should not be used for diagnosis and treatment, nor should it be considered a replacement for counsel with your physician or other health care professional. If you have questions or concerns about your health, contact your health care provider. We encourage you to use the Physician Finder to locate a physician by specialty and area.</p>
    <p>While we and our subsidiaries make reasonable effort to ensure accuracy of the information on the Services, we do not guarantee the accuracy, and the information is provided with no warranty or guarantee of any kind.</p>
    </li>
    <li><strong>Links</strong>
    <p>We and our subsidiaries provide links to other websites. These links are provided as a convenience to you and as an additional avenue of access to the information contained on such third-party websites. Different terms and conditions may apply to your use of any linked sites. We encourage you to read the privacy policy of each website.&nbsp; We have no control over third party websites and make no claim or representation regarding such websites. We accept no responsibility for, the quality, content, nature, or reliability of any websites accessible by hyperlink from the Services, or websites linking to the Services.&nbsp; We are not responsible for any losses, damages or other liabilities incurred as a result of your use of any linked sites.</p>
    </li>
    <li><strong>Corrections, unsubscribe and data retention</strong>
    <p>You may update the information you provide to us through the Services. To change your information, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>. If you are subscribed to one of our newsletter, you may unsubscribe from receiving the newsletters by clicking on the appropriate link at the bottom of the e-mail. We may, from time to time, send you administrative messages. You cannot opt-out from receiving administrative messages.</p>
    <p>We will retain your information for as long as needed to provide you services, comply with our legal obligations, resolve disputes, and enforce our agreements. Except for authorized law enforcement investigations, other valid legal processes or as described in this Policy (or another policy in place between you and us), we will not share any Personally Identifiable Information we receive from you with any parties outside of McLaren and its subsidiaries.</p>
    </li>
    <li><strong>Changes to the Privacy Policy and Terms</strong>
    <p>We may change this Policy from time to time. Please take a look at the “Last Modified” legend at the top of this page to see when this Policy was last revised. Any material changes to this Policy will become effective 7 days from when we post the revised Policy on the Services and all other changes to this Policy will become effective when we post the revised Policy on the Services. Your use of the Services following the effective date means that you accept the revised Policy. If you have questions or comments about this Policy, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>.</p>
    <p><a href="/main/foundation.aspx"><strong>McLaren Foundations:</strong></a>&nbsp; Some subsidiaries uses Internet donors' technology provided by Blackbaud, Inc. This is encrypted technology and allows financial donations to be forwarded electronically using a secure server.</p>
    <p>McLaren Health Care Corporation and its subsidiaries use oxcyon.com as a host server and vendor, the content management system is Central Point.</p>
    </li>
    <li><strong>Use of Services by Minors</strong>
    <p>We do not knowingly collect Personally Identifiable Information from individuals under the age of 13 and the Services are not directed to individuals under the age of 13. We request that these individuals not to use the Services.</p>
    </li>
    <li><strong>International Visitors</strong>
    <p>The Services are controlled and operated from the United States, and are not intended to subject us to the laws or jurisdiction of any state, country or territory other than that of the United States. If any material on the Services, is contrary to the laws of the place where you are when you access them, then we ask you not to use the Services. You are responsible for informing yourself of the laws of your jurisdiction and complying with them. By using the Services, you consent to the transfer of information to the United States, which may have different data protection rules than those of your country.</p>
    </li>
    <li><strong>Use of Materials on the Services, Trademarks and Copyrights </strong>
    <p>You acknowledge and agree that all content on the Services (including, without limitation, text, images, user interfaces, visual interfaces, graphics, trademarks, logos, sounds, source code and computer code, including but not limited to the design, structure, selection, coordination, expression, ‘look and feel’ and arrangement thereof) is the exclusive property of and owned by us or our licensors and is protected by copyright, trademark, trade dress and various other intellectual property rights and unfair competition laws. These marks and copyrights may not be copied, imitated, or used, in whole or in part, without the express prior written permission from their respective owners, and then with the proper acknowledgments. Nothing on the Services shall be construed as granting, by implication, estoppel, or otherwise, any license or right to use any trademark, logo or service mark displayed on the Services without the owner’s prior written permission, except as otherwise described herein.</p>
    <p>You may access, copy, download, and print the material (such as, for example, educational materials, service descriptions, and similar materials) purposely made available by us for downloading from the non-secured component of the Website (“General Website”) for your personal, non-commercial use and for your business use in connection with evaluating McLaren healthcare provider services for offer by your health plan or use by your employees, provided you do not (i) modify or delete (including through selectively copying or printing material) any copyright, trademark, or other proprietary notice that appears on the material, and (ii) make any additional representations or warranties relating to such materials.</p>
    <p>You may access, copy, and print the material contained in the secured component of the Website in accordance with the terms and conditions governing the secure section.</p>
    <p>Any other use of content or material on the Services, including but not limited to the modification, distribution, transmission, performance, broadcast, publication, uploading, licensing, reverse engineering, encoding, transfer or sale of, or the creation of derivative works from, any material, information, software, documentation, products or services obtained from the Services, or use of the Services is expressly prohibited.</p>
    <p>We, our licensors, or content providers, retain full and complete title to any and all materials provided on the Services, including any and all associated intellectual property rights. </p>
    <p>As long as you comply with this Policy, we grant you a personal, non-exclusive, non-transferable, revocable, limited privilege to enter and use the Services. We reserve the right, without notice and in our sole discretion, to terminate your license to use the Services and to block or prevent future access to and use of the Services.</p>
    </li>
    <li><strong>Submissions and Postings</strong>
    <p>To the extent that we allow submissions on the Services, you acknowledge that you are responsible for any material you may submit via the Services, including the copyright, legality, reliability, appropriateness, and originality of any such material.</p>
    <p>You represent and warrant (and we rely on your representation and warranty) that you (i) own or otherwise control all the rights or have sufficient rights to the content you post or that such items are known to you to be in the public domain; (ii) that the content is accurate; (iii) that use of the content you supply does not violate any provision in this Policy or terms you may have agreed to with a third party; (iv) that the content is not defamatory or otherwise trade libelous; (v) does not violate any law, statute, ordinance or regulation; and (vi) that you will indemnify us for all claims resulting from content you supply, including arising from an action alleging infringement of copyright or other proprietary rights in such work.</p>
    <p>We undertake no duty to determine the validity of any claim of copyright or trademark infringement. Upon receiving written notice that any item posted on the Services is believed to infringe a copyright or other proprietary right, we will remove said work.</p>
    <p>If you do submit material, you grant us and our affiliates an unrestricted, nonexclusive, royalty-free, perpetual, irrevocable, transferable and fully sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute and display any and all material not subject to protections under HIPAA throughout the world in any media. You further agree that we are free to use without limitation and without any compensation to you any ideas, concepts, or know-how that you or individuals acting on your behalf provide to us. You grant us the right to use the name you submit in connection with such material.&nbsp; We retain any and all rights granted in this Policy in and to any user submitted content or non-HIPAA materials after termination, notwithstanding the reason for any such termination.</p>
    <p>We have an absolute right to remove any material from the Services in our sole discretion at any time.</p>
    </li>
    <li><strong>Usage Rules</strong>
    <p>You hereby agree to not upload, distribute, or otherwise publish through the Services any content that (i) is unlawful, libelous, defamatory, obscene, pornographic, harassing, threatening, invasive of privacy or publicity rights, fraudulent, defamatory, abusive, inflammatory, or otherwise objectionable; (ii) is confidential, proprietary, incorrect, or infringing on intellectual property rights; (iii) may constitute or encourage a criminal offense, violate the rights of any party or otherwise give rise to liability or violate any law; or (iv) may contain software viruses, chain letters, mass mailings, or any form of “spam.”&nbsp; You may not use a false email address or other identifying information, impersonate any person or entity or otherwise mislead as to the origin of any content.&nbsp; You may not upload commercial content onto our Services.</p>
    <p>You expressly agree to refrain from doing either personally or through an agent, any of the following:&nbsp; (1) use any device or other means to harvest information; (2) transmit, install, upload or otherwise transfer any virus or other item or process to the Services that in any way affects the use, enjoyment or service of the Services, or any visitor’s computer or other medium used to access the Services; (3) engage in any action which we determine in our sole discretion is detrimental to the use and enjoyment of the Services; or (4) transmit, install, upload, post or otherwise transfer any information in violation of the laws of the United States. You may further not use any hardware or software intended to damage or interfere with the proper working of the Services or to surreptitiously intercept any system, data, or personal information from the Services. You agree not to interrupt or attempt to interrupt the operation of the Services in any way.</p>
    </li>
    <li><strong>Infringement Notice</strong>
    <p>We respect the intellectual property rights of others and request that you do the same. If you believe your copyright or the copyright of a person on whose behalf you are authorized to act has been infringed, you may notify us in writing to the e-mail address or mailing address provided in the “How to Contact the Web Team” section below with attention to Copyright Agent. To be effective, your notification must be in writing, include your contact information, provided to our copyright agent, and include:&nbsp; (i) signature of a person authorized to act; (ii) identification of the copyrighted work claimed to have been infringed; and (ii) identification of the material that is claimed to be infringing including references to the location of the material on the Services.</p>
    <p>If you believe other intellectual property rights were violated, you may notify us in writing to the mailing address provided in the “How to Contact the Web Team” section below with attention to General Counsel.</p>
    </li>
    <li><strong>Jurisdiction and Applicable Law</strong>
    <p><strong>The laws of the State of Michigan, </strong>without regard to any conflicts of laws principles thereof,<strong> will govern the construction and interpretation of this Policy and the rights of the parties hereunder. By accessing, using, or registering for the Services, you acknowledge that you have read, understood, and agreed to be bound by this Policy and by all applicable laws and regulations. </strong>The Parties agree on behalf of themselves and any person claiming by or through them that the exclusive jurisdiction and venue for any action or proceeding arising out of or relating to this Agreement will be an appropriate state or federal court located in Michigan and each party irrevocably waives, to the fullest extent allowed by applicable law, the defense of an inconvenient forum.<strong></strong></p>
    </li>
    <li><strong>Severability and Waiver</strong>
    <p>Our failure to exercise or enforce any right or provision of this Policy will not constitute a waiver of such right or provision. If any provision of this Policy is unlawful, void, or unenforceable, for any reason, the remaining provisions will remain in full force and effect to the fullest extent of the law.<strong></strong></p>
    </li>
    <li><strong>How to Contact the Web Team</strong> E-mail:&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong> Fax:&nbsp;(989) 891-8185</li>
</ol>
<ul>
    <li><strong>We respect your privacy and do not collect Personally Identifiable Information through the Services unless you choose to provide it. We, and our service providers, may collect Personally Identifiable Information in a variety of ways, including when</strong>:
    <ul>
        <li><a>You contact us to request information by sending us an </a><a href="mailto:Webmaster@mclaren.org">e-mail</a>&nbsp;; </li>
        <li>You participate in one of our training or education events, such as a childbirth education, nutrition education or exercise &nbsp;training seminars;</li>
        <li>You request one of our publications or newsletters;</li>
        <li>You participate in a Service related survey, contest, or other promotion; or</li>
        <li>You complete a questionnaire(s) on the Services (<em>e.g.</em>, did you find our website helpful?)</li>
    </ul>
    </li>
    <li><strong>We may use personally identifiable information</strong>:
    <ul>
        <li>To send administrative information, such as information regarding the Services and changes to our terms, conditions, or policies;</li>
        <li>To respond to your inquiries and fulfill your requests;</li>
        <li>If you enter into a contest or similar promotion we may use the information you provide to administer those programs;</li>
        <li>We may use survey information for research and quality improvement purposes, including helping us to improve information and services offered through the Services;</li>
        <li>For our business purposes, such as improving or modifying our Services, identifying usage trends, and operating and expanding our service and information offerings;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>Your Personally Identifiable Information may be disclosed</strong>:
    <ul>
        <li>To identify you to anyone to whom you send messages through the Services;</li>
        <li>To our third-party service providers that provide services such as website hosting, information technology and related infrastructure, customer service, and other similar services;</li>
        <li>To a third-party in the event of any reorganization, merger, sale, joint venture, assignment, transfer or other disposition of all or any portion of our business or assets;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>California Do Not Track Notice</strong>:
    <ul>
        <li>We do not track you over time and across third party websites to provide targeted advertising and therefore do not respond to Do Not Track (DNT) signals;</li>
        <li>Third parties that have content embedded on our website, such as a social networking connectors (<em>e.g.</em>, Facebook, Twitter) set cookies in your browser as well as obtain information about the fact that a web browser visited the Services from a certain IP address.</li>
    </ul>
    </li>
</ul>
<ul>
    <li><strong>How We May Collect Other Information</strong>: We, and our third-party service providers, may collect Other Information in a variety of ways, including:
    <ul>
        <li><em>Through your browser or mobile device</em>: Certain information is automatically collected by most browsers or through your mobile device, such as your computer type, screen resolution, operating system name and version, device manufacturer and model, language, and Internet browser type and version. We may also collect information on the search terms you used to find our website, the search engine you used, or the address of the web site from which you came to visit.</li>
        <li><em>Using cookies</em>: Our Services may use cookies and other technologies such as pixel tags and web beacons. These technologies help us provide better services to you, tell us which parts of our Services people have visited, and allow us to better measure the usability of our Services. We treat information collected by cookies and other technologies as non-personal information. You have a variety of tools to control cookies and similar technologies, including controls in your browser to block and delete cookies.</li>
        <li><em>IP Address</em>:&nbsp; Your “IP Address” is a number that is automatically assigned to the computer that you are using by your Internet service provider (ISP). An IP Address may be identified and logged automatically in our server log files, or those of our website hosting vendor, whenever a user accesses the Services, along with the time of the visit and the page(s) that were visited.</li>
        <li><em>By aggregating information</em>:&nbsp; Aggregated Personally Identifiable Information does not personally identify you or any other user of the Services. We may aggregate information for a variety of reasons, for example, to calculate the percentage of our users who visit a particular page or clicked on a particular item from a newsletter.</li>
        <li><em>Google Analytics</em>: We use a service from Google called “Google Analytics” to collect information about use of the Services. Google Analytics collects information such as how often users visit this site, what pages they visit when they do so, and what other sites they used prior to coming to this site. Google Analytics collects only the IP address assigned to you on the date you visit this site, rather than your name or other identifying information. Google’s ability to use and share information collected by Google Analytics about your visits to our Services is restricted by the Google Analytics Terms of Use and the Google Privacy Policy. We may use the information from Google Analytics for a variety of reasons including trend analysis and to make our Services more useful to the communities we serve.</li>
    </ul>
    </li>
    <li><strong>How We May Use and Disclose Other Information</strong>
    <ul>
        <li>We may use and disclose Other Information for any purpose, except where we are required to do otherwise under applicable law.</li>
        <li>If we are required to treat Other Information, such as IP addresses or other similar identifiers, as Personally Identifiable Information under applicable law, then we may use it as described in “How We May Collect Other Information” section above, as well as for all the purposes for which we use and disclose Personally Identifiable Information, but we will treat these identifiers as Personally Identifiable Information. If we are require to treat Other Information as protected health information as defined by HIPAA, then we will treat the identifiers in accordance with our Notice of Privacy Practices.</li>
    </ul>
    </li>
</ul>
<p><strong>Mail:</strong><strong><br>
</strong>McLaren Bay Region<br>
Corporate WebMaster<br>
1900 Columbus Avenue<br>
Bay City, MI 48708</p>
Last modified:  5/2/16
<!-----Related Links------>


<div class="subpage-info">
<!----Related Photo Gallery---->
<!----Related Documents---->
<!----Related Videos------->
<!----Related Links-------->
<!----Related Blogs-------->
</div>
</div>
	

<!--cpsys_Template:DetailsFooterContent-->
<script>
if ( $( "#inside-page" ).length ) {
 
	
$('<a name="standardo" id="standardo"></a>').insertAfter( "h1:last-of-type" );
 
}
</script>
<!--cpsys_Template:DetailsFooterContent-->


		
	



</div>
					
				</div>
			
			</div>
			<div id="cphBody_tdRight" class="cpsys_BlockColumn cpsty_RightTd">
				
				<div class="cpsty_Right">
					
					
					
				</div>
			
			</div>
		
		</div>
	</div>
	<div id="cphBody_divBottom" class="cpsty_Bottom">
		
		
		
	</div>
</div>
	</div>
			
		
</div>
    </div>
    
	<footer>
<div class="footer-social-outer-container">
<div class="footer-social-inner-container">
<ul>
    <li><a href="https://www.facebook.com/McLarenHealth"><img style="" alt="facebook icon" src="/Uploads/Public/Images/Designs/footer-facebook.gif"></a></li>
    <li><a href="https://twitter.com/mclarenhealth"><img style="" alt="twitter icon" src="/Uploads/Public/Images/Designs/footer-twitter.gif"></a></li>
    <li><a href="https://www.linkedin.com/company/mclaren-health-care"><img style="" alt="linkedin icon" src="/Uploads/Public/Images/Designs/footer-linkedin.gif"></a></li>
    <li><a href="https://www.youtube.com/user/mclarenhealth"><img style="" alt="youtube icon" src="/Uploads/Public/Images/Designs/footer-social-red.gif"></a></li>
    
    
</ul>
</div>
</div>
<div class="outer-container">
<div class="inner-container mobile-hide">
<nav class="btm">
<div class="col-1">
<ul>
    <li><a href="/Main/Career.aspx">Careers</a></li>
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a href="/Main/mclaren-web-privacy-policy.aspx">Website Privacy Policy</a></li>
    <li><a href="/Main/health-community-needs.aspx">Community Health Needs Assessment</a></li>
    <li><a href="/Main/Sitemap.aspx">Site Map</a></li>
</ul>
</div>
<div class="col-2" itemtype="https://schema.org/Organization" itemscope="">
<span itemprop="url" content="http://www.mclaren.org"></span>
<img src="/Uploads/Public/Images/Designs/FooterLogos/footerlogowhite_McLarenHealthCare.png" alt="Company logo links to website" itemprop="logo">
<br>
<br>
<span itemprop="legalName">McLaren Health Care</span>
<br>
<span itemscope="" itemtype="http://schema.org/PostalAddress">

<br>
<span itemprop="addressLocality">Grand Blanc</span>
, <span itemprop="addressRegion">MI</span>
<span itemprop="postalCode">48439</span>
</span>
<br>
<span style="font-size: 30px;">

</span></div>
<div class="col-3">
<ul>
    <li><a href="/Main/employee.aspx">For Employees</a></li>
    <li><a href="/Main/media1.aspx">For Media</a></li>
    <li><a href="/Main/physician.aspx">For Physicians</a></li>
    <li><a href="/Main/vendor-registration.aspx">For Vendors</a></li>
    <li><a href="/Main/volunteer.aspx">For Volunteers</a></li>
    <li><a href="/Main/gme.aspx">For Medical Education</a></li>
    <li id="health-care-professionals"><a href="/Main/healthcare-professionals.aspx">For Our Health Care Professionals</a></li>
    <!------------- main, bayregion, macomb, oakland, flint, lansing ---------GME Link-------->
</ul>
</div>
</nav>
</div>
<div class="btm-ribbon">
<div class="inner-container">
<p>McLaren Health Care, through its subsidiaries, will be the best value in health care as defined by quality outcomes and cost.</p>
<p>©All rights reserved.  McLaren Health Care and/or its related entity</p>
<div style="clear:both;"></div>
</div>
</div>
</div>
</footer>
<script>
$(document).ready(function() {
	// Browser Less Than 1000px
	if ($(window).width() < 1000) {
		$('.cpsty_SiteTypes_Default_LeftNav').insertAfter('.cpsty_Center');
                $('.cpsty_SiteTypes_Default_LeftAc2').insertAfter('.cpsty_SiteTypes_Default_LeftNav');
		$('.module-results-intro-paragraph').insertBefore('.details-view-left-column');
		$('.inner-container ul:first-of-type').click(function(){
			$('.inner-container > ul:first-child > li > ul').slideToggle('slow');
			$('#search-expanded-container').insertAfter('#mobile-navigation-container');
		});
	}
});
</script>
    
	<div class="dv-bottom-edit-liks">
	
	
	
	
	
	</div>
	<div id="uprgUpdateProgress" style="display:none;">
					<table border="0" cellpadding="0" cellspacing="0" class="updateProgress" style="position:fixed; top:0px; right:0px; border: solid 1px #CCCCCC; background-color:#F2F2F2;">
				<tr>
				<th style="vertical-align: middle; padding: 2px;">Loading...</th>
				</tr>
				</table>
			
</div>
    <input type="hidden" name="ctl00$ctl00$FormAction" id="FormAction" value="0" /><input type="hidden" name="ctl00$ctl00$FormGroup" id="FormGroup" /><input type="hidden" name="ctl00$ctl00$FormButton" id="FormButton" />

<script type="text/javascript">
//<![CDATA[

 //Admin > Properties: HeaderStartupScripts 
$('table#DynamicNavigation1 img[src="/Uploads/Public/Images/arrow.png"]').attr('alt', 'arrow');

 //End of Admin > Properties: HeaderStartupScripts 
$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show().siblings().show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('#cpPortableNavigation73').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation73').unbind('mouseleave');
});
$('#cpPortableNavigation73 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation76').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation76').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation76').unbind('mouseleave');
});
$('#cpPortableNavigation76 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation76').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation77').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation77').unbind('mouseleave');
});
$('#cpPortableNavigation77 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
});

                    $(document).ready(function() {
                    $('.mobile-navigation-accordion').cp_Accordion({collapsable : true, active : -1, headerClass : 'mobile-navigation-accordion', contentClass : 'mobile-navigation-content', event : 'click', expandedSpanText : '', collapsedSpanText : '', expandCollapsePosition : 'left', expandedHeaderText : '', collapsedHeaderText : '' });
                    });//]]>
</script>
<script type="text/javascript" src="/modules/templateajax/controls/structureddata.js?v8.7.17"></script>
<script type="text/javascript">
//<![CDATA[

					if ($('input[name="HtmlSearchCriteria"]').length > 0) {
						$('input[name="HtmlSearchCriteria"]').autocomplete({
							source: function(request, response) {
								$.ajax({
									type: "POST",
									url: "/WebServices/ClientMethods.asmx/SiteSearchAutoComplete_v2",
									data: "{ \"term\": \"" + request.term + "\", \"lookupType\": \"StartsWith\"}",
									dataType: "json",
									contentType: "application/json; charset=utf-8",
									success: function(data) { response(data.d) },
									error: function(XMLHttpRequest, textStatus, errorThrown) { /*alert(textStatus + '\n' + errorThrown);*/ }
								});
							},
							select: function( event, ui ) {
								var value = ui.item.value;
								if (value.indexOf('&') >= 0)
								    value = value.replace(/&/g, '%26');
								$('input[name="HtmlSearchCriteria"]').val(value);
								$('input[name="HtmlSearchCriteria"]').siblings('input[type="button"], input[type="submit"]').click();
							},
							autoFocus: true,
							delay: 10,
							minLength: 2
						});
					}
					Sys.Application.add_init(function() {
    $create(Sys.UI._UpdateProgress, {"associatedUpdatePanelId":null,"displayAfter":500,"dynamicLayout":true}, null, null, $get("uprgUpdateProgress"));
});
//]]>
</script>
</form>
</body>
</html>
