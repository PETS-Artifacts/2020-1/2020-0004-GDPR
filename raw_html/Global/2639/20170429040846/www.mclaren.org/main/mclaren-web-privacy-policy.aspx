

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html id="html" xmlns="http://www.w3.org/1999/xhtml" lang="en-US" xml:lang="en-US">
<head id="head"><meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link id="lnkSiteType" rel="stylesheet" type="text/css" href="/SiteTypes/Default.master.css.aspx?aud=Main&amp;rol=Public" /><title>
	Website Privacy Policy | McLaren Health Care
</title>
<!--Admin > Properties: HeaderHtml-->
<link type="text/css" href="/Uploads/Public/Documents/dropdownlinks.css" rel="stylesheet" />
<link rel="shortcut icon" href="/Resource.ashx?sn=favicon" />
<link href="/Resource.ashx?sn=slider" rel="stylesheet" type="text/css" />
<!--End of Admin > Properties: HeaderHtml-->
<!--Design > Styles (#ReDesign): HeaderHtml-->
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:100,300,400,500,700,800,900,500italic,400italic,100italic,900italic,300italic,700italic,800italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">
<link rel="icon" href="/Resource.ashx?sn=oxcyon-favicon" type="image/x-icon">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<![endif]-->

<!--[if IE 7]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE7-STYLES.css?v=2">
<![endif]-->

<!--[if IE 8]>
<style>
.REPLACE-ME-WITH-A-STYLESHEET-AFTER-QA {}
</style>
<link rel="stylesheet" type="text/css" href="/Uploads/Public/Documents/Styles/IE8-STYLES.css?v=2">
<![endif]-->

<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
<!--End of Design > Styles: HeaderHtml-->
<!-- Site Architecture > Audiences (McLaren Health Care): HeaderHtml-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6396952-25', 'auto');
  ga('send', 'pageview');

</script>
  <script type="text/javascript">
    var _monsido = _monsido || [];
    _monsido.push(['_setDomainToken', 'VW4sxwq_5Jph8JUIFq9uyQ']);
  </script>
  <script src="//cdn.monsido.com/tool/javascripts/monsido.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<link type="text/css" href="/Uploads/Public/Documents/mhc_custom.css" rel="stylesheet" />
<!--End of Site Architecture > Audiences-->
<meta name="robots" content="ALL, FOLLOW, INDEX" />
<meta name="rating" content="GENERAL" />
<meta name="revisit-after" content="30 days" />
<link rel="canonical" href="http://www.mclaren.org/main/mclaren-web-privacy-policy.aspx" />
<link href="/Integrations/JQuery/Themes/Stable/Root/jquery-ui.css?v8.6.6" rel="stylesheet" type="text/css" />
<style type="text/css">
/* Site Architecture > Audiences (McLaren Health Care): HeaderStyles */ 
.mobile-navigation-link-visitor{display:none;}
/* End of Site Architecture > Audiences: HeaderStyles */

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
/* Admin > Properties: HeaderStyles */ 
a.dropdownlinks:link{font-size:12px ! important;}
a.dropdownlinks:visited{font-size:12px ! important;}
a.dropdownlinks:hover{font-size:12px ! important;}
a.dropdownlinks:active{font-size:12px ! important;}
/* End of Admin > Properties: HeaderStyles */
.mobile-navigation-accordion { cursor:pointer; }
.mobile-navigation-accordion span.expanded { padding-left:17px; }
.mobile-navigation-accordion span.collapsed { padding-left:17px; }
.mobile-navigation-accordion span.expanded { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-expand.png') no-repeat center left; }
.mobile-navigation-accordion span.collapsed { background: url('/Uploads/Public/Images/Designs/Main/mobile-accordion-closed.png') no-repeat center left; }
    @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}

    .left-rail-nav-container{width:284px;border:solid 1px #cccccc;}
    #site-nav > li > a:first-child:link{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:visited{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:hover{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav > li > a:first-child:active{padding-top:17px; padding-left:0px; display:block; width:100%; text-align:center; font-family:'Alegreya Sans', sans-serif; font-size:24px; color:#00446a; text-transform:uppercase; font-weight:bold; border-right:0px;}
    #site-nav{margin:0px; padding:0px; list-style-type:none;}
    #site-nav ul{list-style-type:none; padding:0px;}
    #site-nav  li{list-style-type:none; font-size:14px; color:#919191; font-family:'Alegreya Sans', sans-serif;}
    #site-nav a:link{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:visited{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}
    #site-nav a:hover{display:block; font-size:14px; padding:10px 0px; color:#919191; background-color:#eeeeee; border-right:solid 3px #0065a4; width:88%; padding-left:12%; text-decoration:underline; color:#0065a4;}
    #site-nav a:active{display:block; font-size:14px; padding:10px 0px; color:#919191; border-right:solid 0px #FFFFFF; width:88%; padding-left:12%;}

.liParent {display:none;}
#site-nav {display:block;}
#site-nav > li {display:block;}
#site-nav > li > ul > li {display:block;}
.useClick > ul > li.liParent {display:block; }

.useClick > a:first-child {background-color:#eeeeee !important; text-decoration:underline; color:#0065a4 !important;}

#site-nav > li > ul > li > ul  {}
#site-nav > li > ul > li > ul  > li {}
#site-nav > li > ul > li > ul  > li > a:link{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:visited{width:80%; padding-left:20%;}
#site-nav > li > ul > li > ul  > li > a:hover{width:80%; padding-left:20%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > a:active{width:80%; padding-left:20%; background-color:#FFFFFF;}

#site-nav > li > ul > li > ul  > li > ul > li a:link{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:visited{width:70%; padding-left:30%;}
#site-nav > li > ul > li > ul  > li > ul > li a:hover{width:70%; padding-left:30%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li a:active{width:70%; padding-left:30%; background-color:#FFFFFF;}


#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:link{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:visited{width:60%; padding-left:40%;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:hover{width:60%; padding-left:40%; background-color:#FFFFFF; border-right:0px;}
#site-nav > li > ul > li > ul  > li > ul > li > ul > li a:active{width:60%; padding-left:40%; background-color:#FFFFFF;}

.full{background:url(/Uploads/Public/Images/Designs/Main/navigation-more-arrow.png) 15px 13px no-repeat;}
.full2{background:url(/Uploads/Public/Images/navigation-more-arrow-sub.png) 42px 13px no-repeat;}
.CpButton { cursor:pointer; border:outset 1px #CCCCCC; background:#999999; color:#463E3F; font-family: Verdana, Arial, Helvetica, Sans-Serif; font-size: 10px; font-weight:bold; padding: 1px 2px; background:url(/Integrations/Centralpoint/Resources/Controls/CpButtonBackground.gif) repeat-x left top; }
.CpButtonHover { border:outset 1px #000000; }
</style></head>
<body id="body" style="font-size:100%;">
    <form method="post" action="/Main/mclaren-web-privacy-policy.aspx" id="frmMaster">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="K4M6LCxemnbDg/UFf7Ddcb8kVXiooIPuBQWyVrbxbrvTu0gOAi2E/Kps5HzTHhhAJCZAd9eW6Rot7e4eDpNwwIRP7+PAbH7/K7gnU70Atqt2UyeAvdXz3zmjxKkhiidY6C+2fMTqGVBaLv8ofNd9PP3nbQ1bJCWVbPs+lZTQzGTVIbiry9w50sthNdLXL9QwdOqTOWajenTyHlgDlhAK0Z6ZCpWlb3yj+V/ywYM8KasoeNuTYw2h8mSghCoVCTVnDmeUq3mpJT93N7KAgWMbt+71CiQw16BRXDU5rYkWqn88zHS9a5E8JjwNiQpiwiYkVVKRqEPNE0qWp1UxopxTjcgXSTt03LLN1R1PvJed5QDFTirWFivM2SMeVs/3Kp2bCEXMkubrhgzQQdh+Hi9wCLenIOuK5qqbPGfeMCGe3mUfxDMfsFCzA3DkGH8yzD1MIRbH2hcwdkpYavyNY9G0EwmQMF1WYQC4BnG6Je5bBqzUxFRkyZf7gRq9exMevjTX1RE4QhMO8oGYDzOOm2XmwDlzpi5FB8O8kVvv4ZaOct4wfya/KV4liBY7xgYFDuqiRkOkD9qJXGqilAOjDpey1QQLDE6NG2FALGluvoBbQB+Wz7eWFs5G6B0cH2CcdZ+qjAtBQmmZefEiIZNiAKHfE/xYpRNHonZoM1+ainKFNV3YQvMFlqEqGmE43fFbPVhFIy0PrIek7odMuAa4jeExVCaKIzKee5n1td6V3tiamW0GshXJU9ZQb7hmMhdiZt0arfT30ISYvAGeacKjdR2ymfA/mXET92B1pcgngQH4+WAaej7uhYnI7MNNOUunNd8IbjiQfT+A+hIF1C9F4Hcwgc+wkJ3tiFEj+BqD9PcFqA3VE058aMJUDo+CDkMp4dX3ldepTbZoei2VlKVqz/KxDBiOiD1oXWjvZG2ocfIhds32VmXV90UhC87lgCBkrKUli/MYQsfsv+JwauopgeW7iNhTL3TKEXuF13rTGT4FC6T01PULZapyFzasw5SeImC7FrIMYvRH/sg25br+/1XwksGI/C+lTal7cPqCnS1CyeUw6dv+G5ckDp1lquisLPn+KO1LPitQx+k37GBr3IgmpALuqUSDJRNoyJc0MCBAv3+feKKTqxQxxSkpi05/rLSR15VDExE0SqzwYlK8EwPM1+mP6fRW1ny92iKC6tihtD0gWdk3w7as4xJculfk1zJjJiF97BsyxYq3jU8ceh5DXxjjt3SmF3b0E5Aa0wwxjvmCQhdHXP0HJngZeDKZzGGFkVS6PSWl+7TVmjGPcgNbqfElOt153DHuuBPARiK/VbhEWT7jt+Z9qXIDJEoCMBju8z7K3BoTYP303zIYM2ZZvFRez+sRCoxC2SaA0eR+EarPbeZwR+cJTISweYBesZsKuT3ifZcwYhsoTccFfvKwsnB1p4DXz23iTXKSqS7jRIIgDa2PlQbUjhvAQWthAI3S1y7obBJ0f668DbxUwdxzc25mwzQEzwAwosjrB7YFS5vk5VFhwmTwePRQyQmGEdO4QYZkAZdYw4KUktbFRKtHcC7CdyLVrTbIVG5hAX0KeGVDnAcMDH4y0QIIZ4knSol2K7giOhUGxhrPUT2UG0gAxx/m6GZSw0N3N5CVJFdiqitKAq2wZUIXHtSdzWvGRSUFAfk/ZvwnUESVI2bP/Mp13+WBP06WactsRHbGE9V7sG8TiDTOubI0L/YsgzP8KAzXGDNBvllhrfnH61ZmW//Oo5qsn/zmY1lna3VBxFbKDMm8KMyfFHcCjCxGtCs8UYUfx/4t1qjb+ZRfYiqo5IPdggGYmUacUt4VUkYE0iEzHIA1KswN5NdVfzhkVvc59nfx8p0sgjaRo2P6jf7JJymNvG/8Sfw1b83oyq3ep5gSHWvxO4H4t7ZhkTx5vWsIDyb84TH4lbDbg5NG4OINrADihjBOyVQYbY7rzePSbnSEMOdMduo1ni/af/6mMcR8xyWRI2eew33Ot+923E+0+omY0BJ55hVRiW08Gy2+ktAAJapGOMUXuhPb0URj56raVRuzipaQEHJVkxTKGXrFylfMa3uziltaCnSqJSBb1dSJvx5XjBu8fm/MIwR4/xACEurazZjhdiRLyFafwX4VZ101PQFFk472eJCq+FZDghvtOKJnOWzD3BLub0ekn9sMPK2FKUkTOhMpn+hRJV8gDOwsTIZRWgDJJbOcRELklAh2kKs7Vd4WQfDT1RC73jB4WiRcbp9c14GTfMksnqZXW1ObxaPpF2AsjTGuNTIrbhedy+MXs0lHHUCXqYIszSd0W5VQtk5+MvWLpzH+2HY2OQP62f2+JILJ4bAxaLmDtxLAyQrv/cBnQ+Mqzg0Mlk/0UshHT7YlsrAw0TLdrZrNwWSQkVTC9c4hibbZxP/jyX3o+Ih42l2Ocb2a5TBrqUlRBAv0ktJPdIX7ylQh9Gg1QW7JzcFxSNsevfWlSs4ZE0cS+hiZr2dRKb58VWoyaASSyoMWLr/wabESjd19B7NuDHWbLlm8jFWVd+TsXWkcB3d0t0spHU859roLoyWkmkQSF68wrngcllsb8a4+6LQctwzcsRzGxdjF0vNJf/0uG0KMNwc0O8w5av+lM6sn6tQy/m+OMer5OZ8RK7GnIBWNrfgMMYviW5q1OTc7gPPXn0xVMzg6ETYJOZ9tC6sq/iiOFW0OgTYTaTGgGpBf7SJR6UxVNanvHXzub/FygOTOrQuDnH1SDj3LWlFGlDVI3Zajobug/dlw58aJe3WUWAoTmZ2DvdNRTJdM01la1BghY3oCQvvZmWneNzwqw3Y8kT7ISdLzR+u8asNounpDc6agEHsN+Dos5nHIsbN3zxtafJ14tKq+x1orHYKq0995lRY3r77VQoObil8kx9J7F2zWpo9XxrAP45KXGap1hMrQvGGxUd9IwuzZzuZiVtX6CZvm7AbXD5nwpAECeblHysYtPQc4lqK6dkegmV8N0odwHX25E+T1YT2zj9usEV26mDYzA7qSxyGBT0NhsVSd+abBKguV9CrNYMeNx0eUqVCfYjfwITkQthpuKhpju2saSz7y4xwWbpqsEH+6vHTrzyfZF/ScOOhMmFz2bDHdv7w8OakI/1Sa40F62vWg857Q4m3HOrx8ylMdFIjLVeffTBqF/jA8wZ9OfjCvuLX0FRGU9T0hXhRxOC+OTQwLWErTbQJHKBQtm09VtOIYyG8aJJxi1ocTWYZLa2uLJlbW6h2/0d/fMoGcNQ0KxY6aFdDyTPS2/vmC406wh0NpYJjh0zYy4S72c3n8uF7fr3sBSzMqj+DanvNfEPiduMej6d2QhuuayVNRYgGVLXZtBZ4olDsqP2iDuueIapkA38nli6/zjLDNpIQ5x33taWd1lfobV+AG5jVzzSPXPfuKmclGS9sznRWhaoZC06sadslaqgrYM3RxiTGFFq2RRQp+Ca5YLAEDWtF6yrms4/Zw6r3AK7MkamZVZSkNGXgsr8SJQr5dZSQQxATd6oGBxKiZuW9V6FKuS2jZ80UL5bOHQ+jBpMUXGSwBZOk0l0cD2KwA3aXJjSxuqj3ZaiZQIMXctdknNqy2pqUdCvrt+8SR6EV2etes1SLRBhQgMUmBkSJ00FLbHhVTRw03fX6bFDjw1/tval38sYAs3ngHFU1PtWnr0xnoMbFMtm7MF7COuhmsQh2tkYmK8vsNEiMOQ5pKccVk7be+VYPWFTAHXbZyyr6i5o0QZEKmIm1q/TCdvI6oOwXEokSQb6bazif+VnJPHU0H+NJsD2/YO+x0+3u+rVlo3NvFgTAiFnIoG2TSjuaZ8EK0oDBUuRTlZIf4hhdi3MpmdsKepxQ3PrJTR3idya2SW3Of6bZmdmmCFPofuR5BDEEvCuQS8fFpiYfDXuPcp/G+sKxPSlkKgpRG9uda8fINYofb7roekmmFqVGKbsGcHg4ugSFiqSjIC0y76+K0w5mdMu54C5lRz+svYOP5mxOmru8873nSiQVVKWPYBvu2PnR3xSCybXPo7y8hlJVeOsmxaZ1V/75AK4hyt5fdNsU/bPNnjUwqsGB1BobSr1saOU5Rn6UxPBOCnd6VSHR5RSh4476RyJQMDsDGuw9ICFYAF+1WlFUu1XBLK1doV4gYq+KMftoBw9Qm9wUe/PS37/XVq2nl56t8D+122HF2zCf5WomygHG+rB5bwuvt+FJEAHb/zUhuTpQ/23EirlYdxpzI96Gzdzi7q0cktT2+mUIBNZX1HZPDDP35IdLQHsAU2vLZD22bWTKiMLBgHZH0fHPzXpHbi1nVQc+MMWpHcJptuirzovNeZdLHF5nLyom+l6cgoSQMEMLA3WDopTMbmkEdIkdqA5r3PmqaV+JUi4QB6hezIbpwJ0OKUBgcMssWmLeEIVp/2Q+zeY3Bo75x7VnBam6KwyRsbkIQtT+bNpZbb0tdEIn8JP08qSK3080nhHPyVX71QdiVUboYyEDA2tEqVKPj9R/UXFBEpykrLR2kJ2c1q0LejvWja58IJTPgtkUi1Jzk3TtKr9/1TrWzetlSaCB1L+wfzfA2VCZNyd6ubGyHFpGbkdKoJlGKaqhzOSrYpHQS0CqHfoy9OtpAdKmmbe2jLuOosClF0LmKpgWT/YLWsmahImkQ9JNKiCssGf+LntxZBH4mWcw9rGMMyHeHFgRb6m9hxGJVSz11nU6H24DocfIETckYvYxABR8A1nF/SH3MtKSoMzP3AbiXG85Q1ZmvzFa6h9NJtZwRAPZ06K83BgchBiGidw8ayuqbD76SuDOR5UkSf4713xZ2PnRVZs6GW8QvkQUwIArGCO/BFRCm/jCHRhlZHBAa+zerUygA+W3A+HsBnDdm56Oko9a05LShYyIES7VBQdgMg21THO9Qx0iAbrAlCV6Z2/hhK/7tlmV/UbtztdCDh2aq9S7Nl0NGf+7S92RGvXnCK7T7PqE8OSwjdqKBMFR0c6l4sVjnrme75T7zTE2vxLTJSd85PjqeGvcdczRG31hX6qEdQWmVd2CNRreV2Mr7lFOhIDrWaVd2iSrho8u7t+765zsu1aJQpJkrLSGuNbCjcHKm8j0phdecUGEFsBSycf/gGNrr5AGeLZLqmrPTZX4wNqnQ89TDPrCeOF0XtsZjhxh04IT6HDLuiUInChXHyL/rKsnhXDaBhNKO6pyfTjmTZUX7A6Xjz5NNVI2/d+VwZV9gh4djwUNte3Lq/1CR2ezHlO6N8OgtlVF81c1VaZjsLbFSk0ciYSbP2k0XIuT+yvKg3swkJnF3/SaOdVeulAe3fX7kqZAQNZzhjnHJfP+0b0o3h8JjgqlxoE4J84FyhI+/lFmQnGMjRORY3mO/9vLQt6GvNNwmAQWkD64z3iZs3pT3UPx758Tq3cbCZS3JqGJzTsSdMJWyphOqgAUbehc1u+qvFhDffh58z3kAvs6I5+la2r2awRFL9A8tKe3ZYVkQjxGhjWzFiYYINL3WjzD7gX76rXdBw0XGzMrKrJFjvXFXS3DWQFDom1WlCJm+d2eMP0g5fRPBQBt6y1JgVshauQQ3WgleocJ1adCpDaZvrQYTiL8VydqrnBa8N2ds9na1nIxm5dm2U8KsVFDkkQ31NqKRtg+2lBe77GiQ4RT0zesYtaQJDMZXISTgy3vT+A8LeWoFRoSTql1O+HdMt+nuGhAg8XLtzFF6OP65vGGEPYrfaeIPSvceUHArbA7chXMcYqfNrrXCj/iVEHzqI0bPkyb2YAzHgSK27RWjvKaqaUCxw8zHi/JK2XYuvrRpzXY4i+t/fwvu1csIULtjRJhPlqOuh/CRj5y8h9zRT5c3YCf0kTcOvbLimS9kSrNasNkDRfI08NYBBWOQszT0fWy9BY4f84T0eKGRnJH7FUt+voP7b2Wyhx2BqmXxqVJW+HUyeKu+A0qeZATJyde8WDGRFbEh3hHWs5zGKbicSclzeYrYPnDiLa04PPV9nD8mirsc3D+maQA2hCGaYG3uGCYZQ5c4+FOWtY2+0pLPH1eIt3VOFDbzR9pZvkq0aupCCxT4STAK0FaBZNIzj725j+B77FTxnxiWqpgGK7QFCxb76ev9u23Q5iVRn941Aj7YAiu9Q+8teVBH7jcLgQ7FizvVjCMK2psw808i6qbT7JVhlcJxVEEwO2z2BkidBX4Oijecg1eNlVTRCPEKc2nXnCqPM5fuoHVNg6JLN8TkhGTenvXjmKS2YJspHwpsVVhLVeIXkV6kZDFqw5HMyW6swTrJOmk235bY4DpxIbXHfDyDxHJfnIsg19b7a7F2RHQvcsHgpdPmtyiFZ3yf5oRt8PCpAeF4vnS+TIt5gp8VPGSA7rAijz0Tw3E8H4cIbwJJetqsv3/pNZyLuEygqmsSkjOhSl0kkKnf3kJBKrNt2n4HZ+oc5yDmKqWJopOJwPI0XMSOd+UuDUyzeYcjERN6fN3OhViVnq6g+ZPOdcSQS6/mmowAr8ESeMERQoz2E5lqHxPwNoFiBOcjGV/51ChHcUiOWgGQIr7OzQO6nC6amQOS6wHJDR9MhkfENj9QrVzC0JRZH1hYxvKOAuf0DV9VkeBpUrTIDmW+/jE1sf+vl77WpcJH6Q8YV2YvG650yFynvzpXE7s2NLkCeORnWVzV03BHAOp+I4nRA3VvE/cp8EU5Q/alc+tQn2OqAE0yvPU/YV8nH3iUNsA41VbV7VDwblcKy6A8UL4Gg9wBt0DBccX3AvtdG37ZHlSedXY6qYlZyAz9gA1Qsa7VV1QDxgePswwRP7ZvYl38bz0mBDcy+7ZHOCocrwzHQ7iUBykNMzGykheZSQVMdAJa3ZlyKvXeK3b39O8OJ8kFbfLaiFIEjTLkmQnmsHGMMugcDuLzUpJMGPaT0lY2a9enAtTjq1cRhw//rc4opI6fn1Z3Loldfex9tPgxKb/HWkdCnAer9xatTRsakDQO3//nECwwmgxzargfb2/Hsetw6WVd0gEZgIduETPObOkqIhYSZ/2ZczbTzcNDHCYqg0CN1uByIl5LE4OdFO7ZpWoj5Ygn8G7iCACvAUFoh4crFUqqxEMDcWqxnfedisw+xHQgoVYJHfigVB9Ac7fyGinh/8FPgdzGi2o+qCAt42EIRjcjO+5tiOW/7sfm3HKR+TXKyE5Xm4txi7sNy59T2nA9xjvbttNsTYxeQYvoGTloFXloqhxwO4m+/BoOW9UePiGm8iSXh9s6KC1TUCJS/DOKtazZ8YqxHkPzciGjnHXo0z+T0UXwAN9T9L4TKvLPow9hH5EW1gPLQrzIvf+W/FFYn9ThOW+kVVWM16yPy9CT3nPr2b/x8K2ynrESMldtlSVtNxsaZT6JaBAyMdQtPPf/4wLSmmRyPnMfQAjiH8pPYeOj41XvvW+0IKhO4G6b60TYUKrFxHqP1c8FD8H+MbyraZ0GyJ3RMELtwMLenzuxYU4R5WmvY+BgzCOz8rqLx+4LbJbKJccDddIlQVfaU0R+QwmnOjbmCK36iz3J3w6xRD43p+7h1lA5+Hb5iW561WkD4/y8lTbRHkzZoEkclJyF5KAJBkdaNsblddFE61ZPD7GE5rEf+gda22MlwthQtdgLK2Mu5xFJGzvwyaGu4ktCfc5AmEdBkXt7cvOZ/kNbAKTdiwH+rofIHNwouYo6BV5YyNObLAH8/cTcl2GnQXQRiI4ukmf5BcEXo01Kihq+TCPtq/FcRUMcVBrmvRG/kTm3QcXonQ0PT078SbPVwObSU/YIbZrdooYlk2vyYd8Dv5WJsPBejAo7R/zILeoRsKoA9oh+9sAXezzYMdsQ01hhBa0j8QCdbXrYwisDXD+qeUYLJuLyZxGOgPDZtJQ07pb4fBPWd/5sDV43dHctat4BXz7aCZM1jfZ6UXr/8VHQcA1JBGcjwG9cDgLFypU5t7qWXwLIyuOXhqCaC1ENk7VlSK0j7ipt9a88E2t3xIZdvfXBV+aCXNoSIPaDpnSjNzcQThcruMiOUDBAPsta9ZZG63o9Ey7uoELYOfzM8DzkSD2OeSuaw2ya92jS9TCsR03+pVnY4yOY/ZtpuzqoK2fEEvIwYN4EXyP8u//atIyXf+lA7GKekbP/hgGu3Rl7EZ+CEVVmQSZrEF88uuBniHNYLTWufehl7gssK+FCPzPu2/fezDPkgUZ/jxfywPLr5/lPa8y2VqJvzaCsAE+6v0MoOWtLteSUK1uS0YGywshQhuEr6CBZ/GdOi5Hy/d008xrHOLKlvFw1kSITf7ZnMq/a9KITDGeRMsb5zUtpt5yC/uIcWsmcG5U1mYmO6hYjLT3aAImnVasPAnsk0DpfRfJZcNkKFc/jwuAye4c8DFg0hujkxdiU1RtljaX5NAWIyNYnZ56b/u2+zRggyaj30PVxhMDY+3XeKrphmZwihvzaC/v8kPJmnxVMsw176wz3lSjbejk5XvsTZwYYb3ytnIeXhXDu77wRdRnWpiUPBL13y8XgHltNSTOe+aEf/ZK1Xgk5ivp1yLs/q1nEkMmsRj47I2lCX3uAbpexVhcc2VROHGbuBpI6QDm09NGEXL2Lyijsjms/cHyhGarcKTxPD25lFD6bw59Cn3DwLNjafulwIVCP396Qb4/1B3BVPanfZjUajsRW4B7ndDjscX6r/muTpOx3ad5iMg12kky6kzqp0UugNpdjEbgbt5BW1lpiT3f5mKEOKG/97CFQxRzWypcDbtDmdK82K5cDySgmS3jq7n9UCV4AbF1YmRfDuRZk3TgQTWHMgv2btzLhNp5DKdTGyWqly1VoTMxpGJcwsbDw7EH5KRlwGUl4UB1v8gl2nhCaFyTSoysEeqnkahs9mm/quwfkKdZ15yUTBek6FdDJubNMImYQSAu5AL2YQk/iL1ExO9YN4KkCvyaCHuoa6G6Ng56Eptju6lA+MPdUhIN8w5xvxAhcN20UQnRhRtQ3QmyE1mMhlRdgmZarn6UApSjSkBR0pFDHX5bmWQzgKC6yqy9nlRMkKe+SqX0Zz3bkzmRuQ5VNk2qYIb8+yQOUwRBKb/qWcwpPbjydYnd6KoEZ2ThuBxx81koKPk0dICX8eAifWQKLou55+ljUHH77Xmv3KeukGVPuLLM0dKi4c6PMu078cAq6tCZNPcbLLpkC+wVgKcQ/5CuMoVEf2NWHZpJRZOtECoatcT03/7Sy3qmAWaRxohfDe3b/TNicnn5MK03MEZgNT3JRSVcQHoY3Z4Lh5jrO0r7BjQDa7wQ3n4xvpfBahUxoHXRtBb4BY5whobG4M028gQu+8kH0buVwmrgRFB+V9ouHw19oUsuhGegPD3idDckHNMYTuN7h6FVWVj6ruPH8VDrT59VUiAYtMGUm34pF1nCRT0Ohas+0toCDBLoWSOu8kk1XZlmeozobnK3PgweEWglDbv+7wdKIPprWau8zdkfwkDI/5R76rUdwV7FQ5n6Ze6J71Hw3RDmGKfWFlXcv1IZgJBr5H4C9uIbvuFOykRrdu5fPaKbXDyEOdz4TusJkZ7L/j3XtBpY84hzH4bFjsWqp0lMCW+5Iv7EBeSxgurBLBfZIMK0J3zmCy9BRXzrRyAeEVL9UbxNSC6Yyt5XdLB5jXPAtg2qKQ1cMso++1rOH3ddUKPjiJ8A3w1dudkozwahWmgr0/lW+J60/ouDppIfLv6zosi4Dd1bt44UViG+H8F/vQV491864sl4EVwbICGtbED9lixdEJOwVGry3Y89S68jZqRmrjv0HaHirjHgKrJ+w0ipoikWWDinlygstls0s90i85AYAPmgpbTAz9FH7qbIfuEzYBfUEGE6twixTo7ceKZ6f78Ethy+/A05TsC9PbFV/rxM4Sx3aa1Nf4GaXutJSK/k9w6J9GG2ZAf0hzzI1LncKmOkSeQiALLwlYMpORnYRyy6A3wb0d8put44iiPIFb4yAf4Vg58yoaR3MbCn/jFI2A63peVsDm9gYpU9IXwP2NdESzE9o1ILEn9m1qOB3M5lFFJvqmwRJMZrTJFbJJ3DNOxgpKnjJxNLZLi18YchfOofIRr9hBEAslxw+SWOIw0LwmC0uBJHOHmM/b0vrENUX6pZ7JUSvXLPDeKx/Z7zfX2A4DjdRAfat/QxSX4XTCid9s2YcRh29vcIkju3t4MBcsTfvyG7/SNWR9mdWDMhZm7aMfLgTfPoPRFy/mhxy1NOIViBfz+tQiO9/7zbue2SQfyS86oTq0LGHbyMd+qYd8QiYEN9BVgKp8EXVOjp8wNes82fSgWkvUlDp/fm1Z2CypAl7q4LIwELwQv7ho6IVV48HF2Lx/ozF8uLyjCqpXOymAGj5Wpz4+ZLABOnAPhCMHc4nFsJ2PCFHeSHh56VYBUtIDLc6ZvC4QFANmP74TyEPjXzofcE7aC7Km6+1ITEfQI1uxnUiEJSzoXUuHm6kbRVi7wbA630KHxBXMoj21ddJjB7czqZbekdmzv6XWXXF4oe/u9pfcxT5Kl+KbNCAOHM7uS5xhLSMtMunPuwp86eRt6BFU+mcDETkvdoqDwPTmCJzuvqsZFs9CJmc7u4h8SYBRIdPQbiv/5+LJS98LzLu2RYZfQsRJMkwWs+TKIraFo+pkR6WCyD/3ojJzy3lBdJJd+3MO2XrX7wVfqeK+OxWUcJDb1ljplFDDOhIu38IvDpEQAcGtvpWzYUj64S6cEXxDQuEkVwfiuZhC65L9QMyoFaJpYq8M9n3ABoWvOsN04OED/uaO3xZMBhBs5S+T3YzeyDEJO+Z/jeORLpcx7Rmj6WdVkQV5YJlscSm62s0uHOCclvyrhdXPeRCeH7DVagl/7+/5+KbMNEJj7HuCp9Tm/WCkBL+o1l6bjLqm3PtPcMKY+W6hPaR8PEPRFTErQIdsQrc8FqTv7fDkAS/L0gHDpMJv9dXnqoqSThVEk2MI9/Wo86sohGq7IzF2d1N+XF4pre+XtapFgp8k8VF6GBfq4dBr1//qQRoW73nkCip3HH12ybDqKkFN2z5l3NoV7ehE2EVhlgHGngwhZ5uWPTR+NqHtdB7SnLxJk/4NBztYz92nR7LETgxMOxxGjQjZrPIWPj1YWKtUSF5slOVs7/l8WOGjsUTPTXYWcm1eCX6hwiTzmjdnDItD15flIIYeT/usFsqBlFr1GcWpTq38ChHQCpe236LPLTuRieSW8gAzGyrMNdwieadK6wdduOdobEk8zrYo769AuyB59Ucd75Fm0JOrzhFMyWvtWZEEdJHM4jVWuE+pg/s+keq4xie1EhbpceF2a5AoMNkwjJXDhVfvS3csaridW8WsYav8I/w0jSbndFPa0N628QFVVhq6fgyb2rwUY7vQ8eEfq4E96al5IOidKFxNERiUlIJnM88l1aNpAoKR46LjWrZX+ToQj0t4Rz38E/iabBwd6BVVyaXA8OU/y0V45zjzb9M9C+bM5XXYhfGojKQBXk7k0jeC8U38eZGPTNp9fB9uJfcDHvv6ezW04i6mgEKharjdy0Jn4LhJ+sHKiLhEWdavhHqU3GYRu5A/6aeYGRgTVLiNDaVs6Dnp7zWBbjPTyoB4T4tQHoqv7uTGyBA7LEps7e1cnVAgPECf9KakOEaVBv9qv5wzMqiUTXHDBFyInyr5BGVQ++V3hswSqlnXDD6Ask3PDQMs+zyY+Ynl69ow5+EjfsUGULgFOuwCQ6KlFNRGa2aeLlrijq0v7sG0c19yv0tRRq+kcVJSJYiot1KNRxrZFwEP9OprBBaTdmOmYxhRGxC6xIojxCpluCTMHAT4ROuZVbQaSGzGhAdHY1qjOPNB+KjxKI2ivfrHzM1CWaWEQtHriSvy72YIO80YNqqnZ6O6H9komBRcG2cQcyY4qyYP96CUNok3XyRnJh7sF88TkxNqujfzcEYUKu8VrBM5I8wDzIxnUNLCMZF2CoRt69ymSO4VHVA6vCdvcWsMhczZwduhdLJQw3ynnXTme4+aziBrm+dC451sk9ShUo5sWoaRm9FFSSQCAHoB091g1N1sNmSmz5PHvg63Sbko8xeDkvzVZbCUYq9pSJUKBxTHO8I5HVNL5/rUEB+RSe6RVyNk0bM5gWJN5EE5Yb1Pk+Yc91Q5qzEpySdhdKVN10fALcmWFbrH8Kt4BAHpMA14MfOc8v2hm7f8K4/JsZe6jE5+rv9yTap35UPpc1lGgeH5Rnt2wfCxTLLdLQVGzC2VydMCMkVCLqo8p7tRiQi0Ara7wViQ6cCOVV6b5Vz+5tZEkqfHKkDJ0dCE3EKAPYal+iaWu1SsZgs/dEAVSt/bhlD5pW43KdntRCTYMLLhTAavVvn3Uux2w/IALu9AzulgSNegyncZir2oJGhYYBrM9CwLykcjVXps6w4hQ3v2P+ul4UzfILJsUlmxRd6tqn071ecLJCUHb+MsCNzQdlslm4pKwpdUsDWaN+h0GcwCP977rwovXPqAYw5wx3WJm1R1omaOTUfyuu7gCR/IdEUkUHsAdT54rTpRBX/IXv6yAZ57GvKBJWys3SEVUat7ZQWD/sq/sn0t0veKJUC5HpPE0dARHdy91nVxRQg1A0wWes5GXPzg+YiIFSEIuTV3p83lT3gyDQihL1ye9zOreDflYZx36W8cvy0tV4DodXZ5FyemNC2laeN4/aAzRdyoxYYg63LNC8EBiCc30gm24kynQvZkKP1wEafozjwU8v+zrvhyff52y3+cMC4vaWsLlNcOtWFp7YyNLud/EZy2NJbS37l7hGeP6HBek1nrTmjUCr7iWrtRXDh51RCAV44pz2F2QwrY8RKGJoeWFhRQ0VhiD0T43L5E3AgJC0Ak36Q4gM5s9KnYRsjVlbKimDUE3gdbhFHU3UFGnOIoiTXDW6Ds5shgHhQwkhkpzy4zhWYFT8qRq9vydXLDQ/47ewqfqndQochnYyW71+TgjAp6NBpApVLY5U7O9m+nbKIkm17Sd+eh3oeU6j38HGX/h8ur5JbzFD/YOH99QPVf+5zET04x8Yq8zDrPAI1KxPxo8wMUWokjCwAOeHhvcq7FDFDYBtrQ+JJk15/MJFPk49zsL52KIhuGF2brjBTJdKgKZLdIoDm1UUH2ss2RhWlAJzlWIKvtowkdxITmvgjQgx9XzlT+NOv6pOxOfyVl9xTiiB6KjlscXg1wSv9/yvnBKFh7NkW0vQg0nmPP0CpEEdpcKDASWFnbLvsMlAAhUMOOqcgJXZoD0h9w20AFpEt6mACEFxKVK87rge1aKNCHwCoIXnrIsUmR7LbhkOj5GIKpXaZrcA2IoslK9E4ukFE4/CuBuCUV3QFdl+JtoFRrF3XnUntummuuOZ37jZ80XhBt3iYWd8sakGPfHw6zQeGIV5hRvCIC/FRq4Be4OUOCqipteXnJirwaVYDUQwP468ivCn4NgYcVasodgAIUpKdl8UzaighIxhTpISJrIOUAACB/mcSZd0Fg0hK2yHtqhdBYRa+ZlkuYRq3jDR5RZvFhQb1kW5Uq96+HrekRcZjzoMsXHlMaZbVrJ4zHKxLTrGT7WM/zC/V2hIDWZInBPZRuomoMRrqkJ1vN5/6ToUa/o9CH/9gLPyTmIypXJq3G+8GJWwyGOkLqgvQDEeCre7mf4ySw7J0M7DmGaKXxj45zPA7y/34z4/KVnwSG1OPufKavL7hn2V+ZVUQOh2UfSmUhjCJSNo0iUuqK8uvN2CudxXMpTI2SpNSjIzfkzdo4hYGlw7jxkubN5N+IMBS8LMqvPtnTcHPbfgKAPBOsbPqXoosJf+VBMLPnQ9/CceXiEzoqG7voh1JbwoWC8UttyOc9cMTzlrxD0VyywVYm7O/67wnQfM89EuAUUe79ZCaM/nWyDV48MoT97uSju3Bz5EXkq6FKSAhiM4EC84ajqHdQyX6k6cm7eHAQhp5QOvyJ8tC8DKoPpAngFyqBG+UCevdLWAV6SgVr2cTpBPiDQ2dXetw8yqH5DdgNR79yiLloukQIqe9dIt9+Svq6axaUB/4qSZcZDl8JiwlDuNnU4iqLovufHcigkLpSzT3QlP0gHhzAyiQPBvT1KTcJTHmoXyLloJCf3v6CvPWMewfYoysaUknSz+9Q3P5J3qBvFfCv6KDeQ/0Zn/TmL62cKYBoyNK0lnCEziBjb591zlmOf0br14uB70eP0fUNMdHHoeY3G76TJ+kIP/WywAfz4w3WxFCg2RzmtsyKbo9cNlhmUnLM+DmQvwSUrK0qOut3LuMGgdyTsEsyDVyLMcYYgFfbZaEeUw87UWisBjRddOaZAqrTzUrkXi3swLiGtUKFJY1S4to8xuoiqajts6i5ZLCvAgAuzO9kf2JTjSAtqsbi5KWE8JBKM6iqf5dqhF+180+uaoE8jtSUiSjD8W8E1vmvXN2D399xwVamVT20+pGVfiA5U6PejhzVEOfIWuQpRHDQRuTkvlGrSWfM6Zztx6T4LfBImjG9XklO9nGWvV0P0hE1U2HnGf9BSSfOP3KE4smkbkYdP7tdcJgcIwnZu+W2tLnAXgNcMVJr7687RX5QN/xnTz40DkJUb5WOjVTM01fY4x3ei9eKFwrsAs4WFCBnN/V83uzjPK3ABtMUx45e70jY4Voq7dafD3mEL77d6aGaeIfy0W0fWry4Xn0oR80T8zsG8EC5uX13GyUNjFbpFQMPIZ/FxJU5FgggyNmVywuzmV0nzsPeMoHfU3DMtXfjH6AWqPpeYvSmh5y+jVd9GBm0yGZWXoryvGPd2XW6nGJpxWmtGWtsJm4vtzD5l7p4M6mW3EIpgtLWvjEZzsrD6QYLd+m2/QzDr/6l1l+uMGcBIEtc/u7iZQdhSEgfcljfWzyzMRg3u7PDhGDkfvjHvWPl1hciNFiwJNGF2sgMq1d2mSuIpfDJuivbc35LFlLL3Y9GHq/P16JGyQVf2U6RJiarNvIANVj26Tu2/X4ggukdlgKnZE/8BXTW9MDQCPWgptm3SC9CQflSlxqYknbxwfTm4UpPitMYThJbLF5LwH0K0nPNvtyz/kjdWBZLAlmzlSrKIFTXSU1Xe0sijF9LNCdk+bX7GwiU9V0Qqyr0GzufaYaHi3negZYwpf8wGLaVQSLkL6u5tCstseQfvK9k4dN7rOfxDgfMH2xz0Euw2HSPRM5G1Mk4hCkC6lKYKUKHOucgndft4deOjQAizH1GlqXQToJYzMT7G5yd5lvxwAYgrn3gFTbRCv2axqpKRoW2pJdYZskw6VkKE8+APFEfq3jgYIXH84PN/RwWoadGKihk6D9nhoH7vxvxFu3LNBeMBJk6a+Im69JmPVZj618miaHJLtAMtnaNxO8T0B+xNzC3MTYj9PnQSyH8uRvgsvWzwMCKaTdpdrWW5194t/5RvlC6/f14frNI/URyBvKl8P90AZoI1IOG9h/wvq0Ugs/fa7U6vONCQyMCCxA/yJ2R8ojKo+FCzxcqgWqaDAbofHAgLrEjY/mJMuHqwnc39CpOWS38zoeW2lEjTV3qY2hOCW/cN93kWT9zQynWGzW6INHvmmnJUUP3iVjCT1MIvLaVSeHyMtm9icwSpopY74+MFBMqeE6rVR94dj2Cl56PJlSsjWiDDrykFWBbHYlZQliwOQMIDe4RBgvKX4jJe+ck9hcojqFinNNSAs8Lsm/nwJBoRuhBHD1jfvkWAD3bAMsch0qnKJj+U2wTaVPo6zlTqwptNJXZ8t+9dB3u6FMT7QmlIYRziQ697A6Ej9Bd1iMwB7b9G3g8JTn3qmLZJ6goPudrMhGHf94mxxVJBX2BZmnOF+Tmukqpq6PdbHTzPORkymkBL001f7KgRbh//wFpw46qSUIF1ZKCzhi3bxrgVI8Htjy+g3mDP5Fw1S/zwxfoTsgyi/PAC7LxGWVjE1YJPt0iBglHMw7Pz2zEeOUXwCpSNvYpaQalDFW4Ad8RM3OqjDQjzKPbnPR5iQ4iM94Xn/+HJof6NRYLfaZo9o0UJDbnOkTwcakVoOQksUuFagZBsHivxbP0HYkeLhdNyXeY1Qd0rmtmDpBLMW6b2tJUFLOUZiTYg27KENpE1i3ux2HSi1p+9SxdGwKTkV1ra15kpNwPh2sbc5iiDQTHOJEOwuCyiq/isI0neje3IP8naTO6fYujhxeIorfmBbUeaOQ6WxitfD8xsuFYsD3kqtV+7R46wTbee8IRMVrZEeD9WvJyKDYa7oXoIV/Tgk42Pu0i/9CqoZ02mnOdRxz2ORaBH7b9tMc7VBB6yIEHYxWd9sqGg5aUOmQ3ORnOE1v5e4yRoGroauzMhy5f96q643EAL3OiP/qrnuIT3wXstp29xAbO1wqdFP0IpEfcXKVhqtyK7C2F16HH9bUEVrtdrl6CC7ZCmw2ptcrKib28oUPVM/D6/cTW++meCHRQperqaSyaZ2n4vAgXWba7aaht3/eZ1YJXrUZDzZQ3fkcUEbPnUq/OAF/Z4L4AGxVG0ENF7mKZlfNZQjvbkBq47uFG9ML1vKnYeUyX0KSaROtD/7wYMusWnhIkeQNiby3p5Sp7n++e8drfB587mF5pUaVZXLAXS/RpWkQjeKHjtJhOoGUsvJbSBgAydnQUBcHLy2EHstl3XeQSxLvKUSEXINhc9WT9L68u27WPCEbhze2FLBjepGFKe/E9ByeFmydnpDYm7N3MvEamDYJpZG3Qm3MVy0zhbaLWx6JsvjbqdRrB0JZw2eRQ+PxQhWUN+IcapE81R5io3/GqD/O2R04Nsi+xPiWJJZW8DiExTQrLBvvceHCIpTmTcRXCFrx2WG46oXHAG97q9Li/9ssfaGNrSXC0L5PQE5hj7kSoyg4J/hq57KfwS3wl6e4103YR9pXTlhQEVdsa4RAmG+2PpUfjoxq8FVfdZf0n+JG+QErBneOsYgTc3sUsGMWtebmlbt/R46ECV+m/jNaQVh7k3/G2UfwdOMFq/XfvbCZjsM6R8GSwloMuBxBEqossr6VkwJlv0bDSwihA4/ZFcdyQPYJiym+uYdKAENjSE1sAypzgccdg7ApgDYO0LpfId/lVBccWaLBZ5ponSSsydqk2sPe1CNbklsVkeHeYS7rEBlbk7LkkLPWvov1+jm871xZ0FjKo7PCLV3ymNRG1HTn4sFc3niJAYeTZyVcnQZCiTkilS3DQb6uIVJiRyAt7zDrlHm9Y/WXec8loP0taDr+CiXLa7ysJqcyEmu1r8mqMXjdabcp98aRJ7gjNFHYFLVWhT4bSGHZvWwXH0fG2d3uGzfXfI02KKAd+z0RG40Go+ZgjPL0G74lLwiw2qh/GxWej6orLnns1nuYm4C5VGRPJWhsnEyyGheuF37Ps/97TMPRQevyejQnOB6W8hFAm9P+YfE81ofYz5btPCoQzKHZbyfsapKpwdWkgTEGooAq4HhldAZE1Fx9GVjEBlwyFOLp378CiUBwTW9TH+/oCPfHwaJGZzrVyqFne6FonlrkfdDk1ATnhlb6+8YHZlfz3cDPsJBoOSR4ogybyghOGG9V5cQc/HNOGcG7tJ8+l3JhG28lP+1PmAbwJr3wr0/wH0Nxb4HQJeWESQ7YMSn6sNZn1DVZ0SMbwRfnMTSZGe1tmmdI4Zzx/4ipPakgfWrUR9W9OdErULUZZNKS6MmW9pIRZxLA25YmGdrOJyNyd9fuJrXGmU1tHTfOs1yn6AOU0XlODLH3ZsPyENYy86fxjeHJJ365zAQ3KwVHdqfhCm/v5j6YRurECLGsm9V4Ee5DFNuJQKkN92JG12QSD424LonxaF/F+1oYxPu2KryN1kALgVGmRzrBJcEBhN/3Cq1bMjvdB0BV7xaYsYPTKHyRnOX7EtO+3ybHBJlWSK6vHtza1StlfyCRgln7bezAUcK1gqCHISsMpsuFLWd6jcARI28JhxZ0rczH6WoDjXllqAkfmGCc1OblcqioMU1RIceD59LTMculz2oBxJHMb6tTWEy3U1yXGb479zMmpGNTuIZJok78cbC8MjS1rrUPKKzM9T/mVAya13JeLA6+uEzo1+jRJ34ZHoWPFcaVd7IlEEf4LuUNAjXnrIGKR16nu88pHUACbNIDAzA9nwSLVPXLjtGttjrjPzz/YribC4eAZfZDnx/79RbjW2LpcI6jIhifQq/NN6aEuoE//eeuIslZ8jaLVnMCQOkJGCYFdoInUFEOVfJzpJcKGXgbepwZcR7pdHeBstVS/6+BAP09latDd5grsk49+mGfDX+hSAzCdTBs42k+724z+mx9M7vS8xdnNdLvDXaSB5YKHf3WeG/SKo6bZ/22i99UnJut3dV3CcQNACak4IvlUV7OmPv6ZFWByV8d63rTQfeIAVbxDmdyit31bwgbo9LipUsFZYAF8ubPy5PIT29s6E9rR278sazMkITNrEeQPyGPEsYClFsVGZ0rr8GFEp9SWrzdw4pkJaRLZN9cpsaF91y9AyoHeoyGVUKBrYCOsTSvNqrRUj1Q9WdbqzzGE+YvMHkFZwFOIxZrnQseJMh24mdDwiJp7YPLhzkwTNg7rqAA/EM4HUa+98tRtbl3hNXqQZuzzmFdOnkRSmo/vQoKhX58RxtJchbqfBsPHGrdJQWnkOyiUllxQwqZGE5EaLTGNsgBhQCAKec2nuUIZg2XEFSEpBDzIHEIXaTE92vC4EfAWMoo/ZgQc/cff//bFZ7D/SpBtNeYj4TeJi+HPW/ADF05S3bJKLZHThclbOscmy6FITcJLs2KJwZ1SHcb0kVc5t4jJk2VMfq5bmaQ2MHup5+MWl4U2d3rY81t0Ebt4nOHNKAyrof5MLGCRtaj1QkkhXhF/O1cy9qIVePjXEUvGXwlAuAfwwyaN2/9gVcrToI8XZF4Xe8+/4+U0DCQf51UgPqX8eju6/ffF02JBp8iPsxgv4NGjOwe5uL2y7Kc+Ns1LFtOGfpVR8DdmT8ynghzF3MIbeLzoZpf2PksoJ0SPz257IbLgyBGubPUZ/Kpk/Xlb8aTs+YFedpoM8XmJ08tEYj4UYBzbtQIVnMI7s12LSA1g6nJEPR3oyB1LIZ274HnJU4nCUeDA2DmXYyfBFY6tGgcmC81reLwlVyZdu/3c2GYzYF4DvuvAF1Q3ChhdH1rQrCpzUfJV1PJ1HLqWuWBA6N+hx+ngMaRXv+NWy2wGvKUfePwvvm+3eR3Vslgco+1jRjf3JUxNkAIpR8J5LkbuGvnBt7R+0K9WfIKe0a+fUIhr7uVF86346JzfPuyIjQj0MLmLxWzudzUQ/Da6Y12HyTSi3W5kQzHGlraBjNABK026Ng1QWvyyVTePGcu1OJtyoCCvv2pXYX4QtWAO2uAewV2nuLJ51eqVjvkJOelAQHNrrp5QZcohO0/T27rh10uRKu6rJJso1H4zBQ2dqS976gkBYhO0vsQx1glbwMjFp++O2ejy415hajSzQ6wmvaVpNMbmOnnx0b9KA2tWNuN3Rj3o96R96Cb/IoAJ9xOjdjM0nwrkAfs5sZko9G3XMhyYiawDVf7RCbnc4S8e/HbzaeEvjkyWN03CVnkLjf/aE4X4NoTLQ6A9mbgAE4JlpVHoIzoeW1k9fgxGWvZTSGXHqZycU0AyX/TRUMzNgVaZtn/K1NvWmQKWnNJRLAV/X1S4Sdxw7qiX4iSxYIwQpncp3bsbb1/a2ipuFLTXHhfldAWC0j5wAK1dMexrV0Uc2D8sr0PvFIFjbxcoh5XTKo1NOBBz2hB2ec27mdwRx6BpFrwlPVfK8psUeZxqFnkU9GSUm0z8ULcGI6jsHnagSUo5s3bVtYCvGkcfGkF2O+e5nPd83SQLlAUQYDXDlHAbA6Xm1Jh97ba8eWvDmfwLG1dzfc+xf+y/WGvjcbSYmPrduYW1VUsp21Cvo7oRXf939Jey/NRoB2QKBmjrbaarLGYZKGr3JLP03G0haBrM55+DoU8fZzhX88ndcjuv1+EWPvVJ+6wshAmUjzsS52FVP1VMhqoCq252zeVIHczVoxWmrl3uOVNJel4cMNxMTqyTDfGaG2vwrbQ71iB/C5kxs1bxH94cdHKuSXng3ieJ8JyR6s8W16iELRICR7d2hGROn5XNxJX/uYRlqbe8U6rWuACRY9yeoqtUoliOyy81w8260qHodyBXPIu3yDrgGLZBE8AiyUG0Irr+yJ1Ufpb7RewHHUJqGzQUXefFQcZ8muN4+7inlNw4ECgYKnRIZbEb03mqIcZWOySyRprnEUWqUj+OOPxr/gkWdDkYmSe9oDVgKf7HxcEmSQ9X9RfkyiqeNzmLuznGoIQHhK9nHs9TIiR6fi+V6bMs61Xh/APiuy3cTlqsdZ9hV1uwYT8UdO40GQBeaYwhab3kfGWGSRmAVKXb5R+V+G0E7yKDfXgBrt5N7n33bT8zpdBvHFgyP5lYjTdXGlXtNaLa6yWvMOpTyhfBbFD32lN1NfMwcoBEH1ZBGDv0AwkM6UJue8lMvMbZt3xkRdkoEqmMM7ditZ2pPVkyytj12yJ15vV6wkDpiqJSb/EWcL9fz6blgOUW9vx28LE7xmiVQTGvepfzy5Emo68WTOtOxu92qSlWATt3jGKX3jiEyXQN2B0FAXEypOCb8IHRXvPiAPr99bDVTcTDj/J9suXQDQUDMSR4JStug3nOcCkSLD24rvPo6Y4gqdl2R6np3D/Ym7CZ5QUKKcHJ8C0DxHm/F/r/DUmWIsM/Hzy84FZhey9wF09BfDgaldDSug1s9GPCmrhlwXhrzOzVXemcVJmYKn580wZ5JNQlJCKg1t3wkZJX2Iotjfs/5mIhurZjwzRCRVS1anCtMoc0D9Go5BjHX5glOqEdL2XoJitsCjyN608w5DHX1PN84iwql03eBJoZoL4eHtyBl1sdj0cj4d4hSINQYwICwaJiTGesPWunqnvEX9zY0nsTuo+Q9njdlBIukKODx7ht8FM/J71qjt/X1WdT5c8mtErc/KDvuWuILAdkYXCh2GcNUDMutjtLFjH+7hJTAm9dx6Fe9N3PrJy2a4sAuwJ2zuAjt9rgiBFj9E9UE+5U0pCDmqsrrB+h4g7lcjU9GnmIxBzXtYT562D1zb4VO4xlrRPmbY9ZFETNdXj9Qy+LA6KNB6nOP/k4iz3PxBQkTO19g8vOtXSJ8dGMxHeTB5eX6CXoMCWLJCIZCI1QGKR9wUHOwO+2PbWVKdm3BVDW8T3snDqsXPCVed5ExWwFvl+DydlkOGRyW9oUXbMmU16Eh9C14mxumgXfCy4Cv1CDmQ0woMu6pzYrm/GCMr3hsNN4T4JtGyRtRRF/pNz/TLRoOdrda1lyzUoHsV6iPlaoncfDnQHdkr0ARo98hXaLRNBiFl2FLOAdaFOUSoRJKz7CGYVl6RDNIvl1pABmlzejt9Qn8P2qWJ8sObzmtQ/Lkju0Os65JdN1uAXyVt9RQJtgvujcUYpYoVuuxmrtau1X0HYFlCzNpC5H+TQNSYQE8vIh0dnPY4hCptx0aah3rTvxUi0+Ebo2LgeukyCwYOSvtEIy72BvLT1GcVhhFk0taJLp9/ODkoCZNGI9HhhB9/nYedOlNJ3iC6UePruXoFewdt3tUb7q0OOlPoGjcWVi+kPCEhFpgaWTYRIrF7em1wrgw4xk2S57UQWmbjoZtj+CLbheru+o6We5fo1g5xlHgF2X6eSb6DB+eFHqkU/9cWDq0NK/B726MaaVjf3bzlzsN4kv4gp0MBzDux/+qtHKoT1HApk6hkLcCKs+cWDL87lX8btR9MoFppyytcO9kgcLF720ofWQDJc0l+3lSR4Xy6/AYOOht7wfCvvCTcy/O9yaDCceUebk8fErcw7jT/k1Gznb92bc9LK7/DiX+i2kWijR2rR2sIAkSLLrU053H/mbAl06ciuY5jwnrSmlSiAtsy8VOx1Bn3Ws8qtm8Qs0AKC3R3kleP/CdEOqeQp0OThGiW4P4UbobAG/nsYmJs5YUi5+SR+nHXmVbgeLeiEeOAQezYWftFB3PEcr8A9FWOrkJg7MQCL7hJncXpNKzXr3u8ut92AvbhKe6I3IrpToHtYnpdx9p8v2K3SWBxZ6HLABg/BKEkv4YW8+9eq9xbtgf3XpblIoUAQVx8o8b31X6xq9yrDI5LICO6ERct/6Z+6ZO+mShfNd+JuAqWFL5onuK7zGZVUuoduCAcrIl7suSsOWFQnnNFg0vWSxGSChbrUt0xLef6FQXW3vot/ok07Ts/8/PBeEDYn3SuZM5SNW8hr9TX4aM8EWtGGAM6bQj814Yt7/FHaM02pyHANf9wzuMlXx8cKG/HivKQjBPV3oVUiw+9qHbFaLS4GwsgXf6UbqFEm84MECQdLeHI5aHKq7cM8VJtXcrNwY7xpCmLIl0UOYsbpNpinJh0C1Bfirpaotq5Ls++HzpZERnQAwLr2agpcoppEm7wYzYUO66b5VPGF2J8S29Lk+T6qDoMaNSW6MHTEv+75G/oOk+lYLNV3FN+ZnPVgTN1uBSXpeGZD1eS22v4D90y7y9Y1+ubxquDkNJKOyV1mlONOIBYHp2/iRNQU/lpKIqGbUOODkpd3ZWIP+EUSPBzIwxXhNAF37ZFzNZ7xB+v+5P5gR3JgWVDCBX9Jr813x3BeK1qK6vTYyCYHP/QmczlrRUQ+SDj9IHKXtb1lfK9XPUzDMKoKpkSfbx/9PGMjgI5i2t1ROsVeV4mgQsl/bjpg6r/ZmMExPFjjkGXExVugqFTOumHv/hbmxcnqsRHwam0FENmhMAk3iP4G2NPsrTk4VStWxssRmsEfZPbfI8GUjbuMh/VjhF4XI5S6mWYiwPbdUdrBpfVWXzxycDBuNgkk/vEDLaINVLDVEXS16kZrfnm4jqyngC7IQxxSk7KjaxwVTJMtjMQ+2kvdBojj3CfEts6/vj0WJCUs92SP3uyY3OpNguszaE4nyCpmW6SHhQikyYhO+ygl/iASCdz06zMAZ4tlH2vp+fEW8XMv67wZffWM7Gc3GcttVsaaEncDjAdupnZQVHjWC3P29LHzpXtxYt1VHQIYIC9Eg4ot/RLQ15tg1TUBJursCpvg89SkEGCHhYSWiRpv1m0T2Xqp5HTlyL5ogVDxVRJG0VQvnobh5ketVepwnvmAfNw2MeWUbr4AIeS8KrDAwghP7n57qF1FGRz/IOmpqJhBfJDzumwGa4N50JrmYTTYkKKHJK+V8vT9TKzmc01oQVnRJt05nS/RET/aTeWYbkd+GaYNNNInOuucTFOj6dEcOiLbccPvvc9xDGWcnStqtuZiQKYhXydQOg7tiCD32bHVeojlKxgyOszIspbZmoYXogwnodtp57XbPQFH9/UdbbHFkuNXp3r9PZuS7fz2fa6deWAQqq7n1RrsR9qUHhnbZdYTOE4nld/DlZHnABDre+OzF+UNZzYCJXiosUWLUIdHg259o5nLeopIVdnrAZRlTcEQchvM7mfBjCDJ8pm2t6VhgrRygZNDSNiQ0wRB/wP7jmfKR63iGbhAYDAmmzzMI4V5zwrsKRojdRv+9VMRsqQFBJiYS/p3G8S/46RljMVoVdXhReZrR8d6etpq8yql9L7JrAfFfYMF7d6X9A+Ao1zuv0DHBbgjL98Viwq+o8gFdItaHiUoU9iG9jNh2B9HtQ7mJamxohlr9bbesU/d2qEFxWvpQrZaPyFGEDAzBntfEIvzwehwfrTrdzHFeFAg7kMUpPJdYKY8jKSTUyys2FUnPJXCnSEwMVYN07SgWU/Uvp3kvPaqcwUB5+xish/o5XChs4CeEy1TUac9yY7KWMkGvsN7NgXpYe1naU245/4f5TdKqErO4+yVfHivtaC6SBIxS7Guw7UHyQzZVm9EYdHr2b8gBcfqsA7OGMncXENYiEvGM2qiFoHO0NLx8lzuxpzy7aqvk0PAa++5xc0nlC/1YRBtH0r/RvvL1/SjRm2V0fUB4GMq4cWV/0YureO0NdcTuJE71dM+3T27pkmOhux73kwRrKm5lug9cUStRFh6ofqj9XuE3fWw84VEoxuvovSDMUsK4mQFL2nfMD4mVZ+y2kvUAfhPxw9S2yne/ewLX2UeAltxJs08tRc9tIb81RjNGyGtU1hO6ToiPL85kaTEIrXcgJNl9ctzcYKTgMBGjFD1zdKE1mDfRvKZQh4JrJ7B1c8+DcpuCGM7P+VJHwi/RcRjToe9l9/sLVh9ptSFlTzy0Z0N2Gx6OtwKOkPiLeP0MquM77C1bz8cQA1CjozW6A4MOeXxo3eTQJw4iw6VLYjlIiakoGrGIErcn9+NvmaX+yhs+KwxNlrh/0PsB51Z/kV77reHFRRN4epWKLCQtt8zBHfF8zvYcjYu8hh2gg1epC5PDTO/F8HyTnt9RekJr1YMxDaO3WhEc5J52MwWSrTUjUkBHxvBDcQ5n2SbFuvNbBA/i1t6RnWiW+EcDtPyVhDGOD9Md7ubC2u6iKP/yF9QB7aGmwnPDm2C3VAss2bUBReGbuyLeTCDcTa2QSZvzVvrCh8fsBUugUTwQ25CksmND30i3bCUDpVu3qbUeNymQ+Y90gAEjr1SuR1pZgiNBXsvIX3tadCh2hN+OnvKQw5Xk1ylMrC4XqVyx1KWw+ndmpM+DwOuzbR9Oe3Xi/O19B1TKWfy+J6gxe8jc468eqtUrQG3iexofGG1d3Mm9QUUwnNK2CHwSygBamwd1VsM9T7VaWhbCX3s+Gr9B0eG1hhHZ9gDC2EkjJG61yUWoOMVGlm74JGxN7YyZYnysK5D5kVHrQqBsFiqeBeN42loRjHLoMh/XYLw8fCop3TcN76GZkS60dTffXFuDqvL1hzM7ESJkmbRwo6+Md7Vj+tvoVs3RamA6//UMpDLsi/gyJUUXd9LsQA7SMKU4sX0JbttCjdwraXg9dkNTYgEiyjuZH6XSYKUHefSn1AwlFLJ4ti1RA+WIs24zYxI+yNR3U4DGh7ayOnaFx6o8/dG1XtVOKFFx8x+7qphPu3QdJw5j3ChEXKZE8iKUDw791oPye8gp/yL4PgfgIpOu71S7DvogdltYo8KBoZbI2VvHBjdUChicSwv7WRpzJrDT45dpjgDQwf7BHTwJzLoxkKeokoR3oRpm2RbgZlqSXUob2tfxN9nvLMuaXEUW8XojawOWhxoywnAeyS+kp9OpDIi+EgxEplSJMuMQWIF40kJJhRHLwA0H/Fdwu+f77fZaV6VsUkB1HeA03EzRvr6PNw/ZD0zX/wE+Xvw8R7LQMTohuau2/vAuqP7oyZShmSXDmYrQ2ts/5mWMjCA5koDitZiF6SNwlU+Mqb+JD0bZkLUXsaglPfLrHPuj25m/4d4nBH80LgEXJa6k30mmemVrVzO2ggFZOFPNub+i+cCCUfvMSdLiAZ0eA59eNL7E61Sfur3Cd5RGY5Ky7Erf9ujFRVee/Y4jqrsYzTwLo6RjJSM9nFcq1dlocdx7a4eZUGRqr3KTvFX9hj7G8wtk4ZfVMalHmURvHQ1NmNZAYKMwEplpinZj6UegQKLM9Y2F53bnmFDa8aanhUOAaAbsUD1C1aCVosAnVTsSoWii6a9e6aO6yvs1a4lgdSacGcOTWow3Sf9snxm/9GF/fRh83/ClIRtftGoFkwyPOuyK0wyTBjkKZ+t1pn5EorrmMRzucS9g+kvnlw0CMKJHJM88ADAu/+Jc2D/Z9Ojm1k/dG8Vbc+7D6L/3sXV9uRYLG+drNlvgUbt+SezrJforpyOWHIFrxTE2G7J/phVIPbxoYSEay4jJ0tj9hpZ+N1Z7YMJqDIcXhswlgTrrKZgpFfSmQVHrwJb2EzKNoGIjRKp2SiWDwe4tY9fUh3D1MoleuoHgx6VpiIXg20zZB9VaRB7mZ7CUvJeOsbjStfHCFS+0nRrpgvRTutpT2T2mwheSpRwiDfRx0vAastPM6hOe3lWYYfG0RkksZDY1DTP2rjfdpwH5cYqSw7hIIRU+QAO2jpJ7UknKK7JiuV1gNtw6TdECgUaCjEIv0Xz5ps9/ifx2vEPz6eLs8nOE9VpR8fInZEIMul3DXmsmHJfzb1sPHRmyFtR1pmCwvfxBgw72qjQt6DQyQADjDtzTIgFaMcCt7kbJtZbcVsEawAx/VB2dIo7wDHnKSzfb/fBZvctSJ4TVSo1NT20uXcc3WKZXr8WH0YA8wiWK4KNL+F5BdMsk99CDPYf0xLIK6TGCI8bL0LyGHfxcX2vG+DhSbi/ND2a0ZX9r5nCZL5kM3r5wcFZ2MrOdhOfwW8LhKIU0mgJrATzIUJ5LyAeXg72c4cRnhblJTW4/x8zU5IDFQ2ssQOfTYGz+Ne52NRp7gBgJloemmt/4q0RMyRB2+97E2OOt7NQrTCtJbWxoIXsFO7gr04lOTFnz3ErLe1GlCObezfVPsyF1pbf03ygJacZrPD+NcrqTWjN18rSnicby+wkXkKNAfSElyCfOK2HTT7tE6ZXvRz8ArNVlwFe6C5oKkMyqXFuO6+XnOHm5ce3ILQx4Y3Hom9ZFF42sewPuShab6x4dpY0qPF1cEE+bdWIJGwOgZ8X0RzcWdHDH7Oa+LY57nqXxGXVllrfgyciefkInpx76Latil4vV7bCjBhnx23kf4qYF2Sw6f1a+e7jymOGkluU2c03sMueJCKJ6A/fXj6Cpxtt3QLFljZrikLQKF8DdHthgbDOLdSYp19KJRhsmiT7+D4OSc9Y1BTO7IydWVYaB+5ZKRW2M9f5rGPSxqEg02e1OMC9G6CUICvDMpjxlSJnoaYFQFPouhGUwu9n/zptc7J62YyPLGoYvte4SAe4K/pZczoZ9raPkLx1gSBGt0tQnVM3TNujjJ9hoslDQFYwNHp1Uv+z5amrF5v7kx1wzNQiTDRae1P3HUYwhOg++3QfyI2e/zicKYkLUDd1h/IKXM1AOgGvQRMjQREHjZdhGAvRRg/mKu/d5W6P8Qeor1gQuEQqG1c7qH6tjcFVkx8RzoOP1oE4tW3iVFSYr3H/NLIqBeXbYh4Qv+1IoaB4yE9D2hSQOVinJNnbrRikwd4SscoCjmIXrBEYU+ULp3BXjB2cxt+jxHnEFf2WgyDoGplEQq7cPz53wOKC3Fr4/EfPOP2yuGeKYNf9TlTQWKRxM0fwcpn5KHGEmwis4ZXBLnhvI8QXCY80z3PrZl0DONiFgT8V2lwNWfxGCaqdyxs8YtGEmLCzPCFkhgk/Z6HvchzvfTq++oW/MmnOkVEE0jJ+PTjpFY1T3U3RHQ7nHZEintrNBgUzxRkEdNXukAuwMXqKBUIwqFfMPPcRZ5Ff5O2HHsMghYLda7HLI37SxMTmmu52ttVvwHbdz9vhew1hbOk0o7K7MtB8jtRDyH0m2ieKkZb785uyesTpxyC2ZsuR0vkigkeGJjkZhxmPKL6VZsxG2YshQun7vVZFTEzH/npaYyEnH67hFi+CVS4vKWmEj4+mgjWGttU2MCCilGIsCtSAzmdbgTU2rbiPvhOSpj2rxfTH9IWsfg8svVHAIZz/VLvUSpaYZlcfDplOQc+EkIsFcnG55gr+FKtX8VE4LWvbkPK88tCDICe4H8cFbQjFhdzYLu0hKlqYN58MYNOQTkLqDqks1ufFYDMZmUEL+rKHYEKYwUnQRs0/07yiFUTh4YLOziFXzp+BIo6IaV3d9smT+VrrLcge2gq1EcbYkp6Jp+/HnVb2+xPIEDwSpPg3PDm4V80lYb/9z/TFAbvdwPF4UEoOpscPFhTalilCIhRl3i8gGJMsX5YYNea/Me2lQPSjjkQycyYdH1p9WZRPi6B1+t5FHRTCmcJi4DJkJqpRQdDyHfJQRDf7pUrj3NWsMLCFVfKY6CZCP+1IiimsJAq4L38wTRRR5WK+ncYX+lOoEFiNjz053HCkKAEYQR+g7Oy9dC51bFNgHYv5pElp+PeDRHCjv8tv2UDgppJaIhSTkmUhcA9FUd+lj5p5DkDGGlA2t0AkM1XIx8zctdgoNpPC0zvXRTYBwh7uVKRNK8Vgq5zevsHUH3d7wN8bHXy4pw25uULPDUJV0z5BsGL3ANhhM02AZq9pqiHz9xNOq9m60b4GjUXMlV5a3i45zGCn4v6txEZln0ioh+Fk1X/48eJi9P6TIYZpcdKk1ukqiej80AZ1p5w+QOpz/ihi4SwDTxNy086w3w47uDphe6rGxGGru9DT+rjRohH7Mj76IkETou2ushz4C96Fi2s0m1D0hLSDQw8s/PV4hb5eL6HeRG2jMED0gNxK1maM+Jg8hCFDLK7aLtQJz7PSd1+C+PTrrnGGb2HqoQ6JneH3ipk7bcYTo2xP2cXIHhNnW2Wtih/Vv4LdTdrjJPYoLKR+zS4zfoP07XKOXrGbO92NlAGLdcgTcH0X1iCdAehC3NA8Zya1WvrI2NNeIF63nn5eJvCIHMvS46Rukk7TsPW7wdeHQT4LSxnYV9wSA4RouhELrr+rnoWL9Mq7B4Kkt2cSqOUjkNnDd3pFd4IPqFzDXkVfqi5AxpyYxmOhEFU2LWP1mbM0SEXhiI5u5rnCY6+E5TGm7CNQ5PAixPPvwDWzz8LAI8rRmnl7vJkhuvLbIFI2dggkboe9eI4ApUyFbGIf5aQG/CTansuOKobwE8csmCRs4qsiO9kUBqEO6wACTdZMWUZhN393sWI0VBSERajZU0/xHQwGectUQ/AtfllqukoXY12b/PR95PUwiS96cVKmlydMwxSMOmSefJ3ZSEGgOPy40N0bepQoTytvLLyDlrPX4Wnmfu+ckdVFHLxHcA8oWrdTTTs/ji07W6s5CPzwhFVS2GF+ZOBJxfNd2KOq0WYiehpgBEQqvSWUyJW+Obqh6RTmSu2LY3EPoH9gt6mW4Z3csYsNbui9u0fZk3JnLZriYmRI4+5zGzvpgzDYgvYIZwVPzh/8h03MkibKxdrIbuNeUBVErHJVjW9C6WAs2HJNMJVV+JZP0+Z32EPpIaZk2S35nFeSu90L4k+jlb1fOEBC23/km8iCGAZAXbcxWcKCaQAPt+ElWuSJDpXqR3Mdp8uT+P+ESapdxQdRj+zuS/ext2ZE83xuemvuozJETGaZQillXUnHqsqeQIollv9Mvwb+kJ6ryPr+Zw/ofiFN74RPkJ2kk8FY49kZ9hPFiszH3jN9T83E8urCjSC9LVysir8lJGifKnnXCoYQB+cxayKpqjeXaQY4zcfNx8m7K8T5VDPxPYOt0DAgFXCtxgDFuA906+wmhCXmdztOlpxLwEGQA1e2tiVfL4Tl6B1e2rG3LteoLCddlbukn3Zb4ISRvl8MiGvSWISs6CbTQkT0flzZSSrO7/HIe3Q3syzTpmFXPQkOKC7KzoBkcyzGtWJUYQM+rI6uEmiMOH2kx8slXcDOFvR3pgUDnTtC+Wv3r1UJGq4J6VHJSHUEwtO1HyDDrOBX91hKr5nhTrcVOp/seigPpLuVHXXhNokESQiFgM2HsdeHGk+GRbFTLtQqOQ/jD6uUPmH3+PxS0a5gTM6M5Xl75FXU2q+qjgaNEiGbZvUbf5m79MBXIvwB1x2ZZDRFxVqHpOLPG0KURGlTrJFg443DFKmfTDWqCNQcRmYRca4j1+Vv+X3jpPHEmm/+a5BQqi7KXFme5akpw102hRmxNNEsU3dw0T9HeCwRP3qsm1gapNOUiG2wpHaUKVQ1SRJFYGP9ZY6YGKrM/2RlSC0WrjrjCZ0W3JAd1du+2LKVSN82MT7/bvjfqBvf86eRbQ/8jjY6UrvINr7UYNIaF8Xvntr3cMN60JLF8WaznpaJjrhXdrSmdNHz9vJE33nggr6T8zHczD3ywq/ejGRoXB68tqAVlZW02p1bRlEG73EMHox10VP7TyxG3m7KYtCu6c6bCxjWWa+Mic9xxx37WQ5xWwwX7jWsfhFddYMkAn1W66SX8dXB4XHBIdedB0eSsR8etcm2yAen+ULJBwMUfYMMhr9hy7LwH+iUZdDe/tQPmhjqGyZeANsdcvUIUz4ZjWIO4CWRbNXOxk/93GDzMVCzApw1o4Kru/3qADsBFssjcqPqcZRsChgXT6c2fa/xztlUYJgwQGMlVW1xycz359SBq7uW6tuqVIQ844a3SzJX4570zafdeW24xWpnwq6FXRafzaqo6HTmxXLueVs7T6YfT+KTWHuC889XJoviVUA3H+fCt11NPD3aURWRORo4Ceg5uSwQ50XS1oyIg3eWsSJrDJ68PezNhUVak71EE0Wie+xsReh2akBaoRLSzIpZlm88iTUyl/woZPyQkSdt2qGx9eEUkn2ufC5O/YFa9E2WObUN5rPK0crpm23aCyU6TWvldcjY0KEtxIVzq3AWJJRikikpBERBJ9QUpTV3IOb4Ui9f9eMYZNYYOUnAgyBrVFcZkQjX0JbFM7yCk+UVa0BXOVqyc8ES/AF6AKVAmPjWzkpcvLpa9m2gZ6FzRblXaaoRbhEBZ7U1PZZBi6yJLV4//rDdYKsaY9W+2Y4Oir6N5iOkW2v3MfO2vDQxeYIJCBXmCpTQibVlIH7OtV2AuwTHH5tdjNZExm9QqSXHKYBHMjI1abQImlfSS10RIZAruWLVcrTXaP8l2S7dqkumabFcV3rQhGNKIPrlMwxnSDZ42/LdeVikHYg5Vr7dy24xK3q8IXb4TRv2Sg6wocdS1VNxwNVldZmOKLZUnpcso5/x4BXbSZxLlVeLnxRWaa6g1wSLx/UgFCchYhgC9SCWOex9qU+9CddKv2zEIBf0DD2uPD3zJYc6evlR7S6P7FeSgS6IULUaBiDM+uyAN0KCRVJAB+SXvF80ch1AzwDDwcuAkigqmNazQ93EAxGdNnGMPnmPGsD0OVvx8HEae8eTtCi18Q3DMBgTZgoWtqd02pSJn84ZX+0dU04lDZ3Ca05KpsCb0dG/WEFSgH5rAqF1fRWqU+Mrs0rI8n1zKawGSDIexOhBPVhdGOYXj0mfaEICiln3betE50QL7Qwm/t7LvMgA1BE1E9tBY8UoXRF3EaMW43XaFeKjUJMP1igx3Ca+fabn6PnaxF8LmVcV+fg3YqZuwutmY64urvLzyi2Bhf6jMsm4jciGGFXE1ZL7EaThVMNHnKny0dMtmRn5AD4MkCzv70PpuC4SWqGGC75BrsyEZ261iwZVaY3/+mdeT2hCYetwRqc8tMFkY2TN7K/xNJfltLDesz5YoxNCnD5ULBpdsObUivT5NVKvMu8NC8fCcUGLcA3hDJe3oCn5bSIaP4v+O644YPaycCVF1ikuSxfS1rm/2pAeQ62Fj34K3PZEsnPPWAn0geT7hOoGkPICo62F/M04Xa4IpPyv1KwchB3agNzSoTtUPmx2qKcN1U+tcI4qFnzeT+JdYeRitSOstaGYF0LHH3Y8YBXmGG0Bco93xXf64Y7c2DPGl7MbHpTbdXHvBabCqDvJIGn96wuEoAhKLJDJZGT9V2PseHq0APuprmkY3MfDrzwKHCa9BhsAkCkTHGMQ3qnPwcNlzW4eqKj4XmGqUYoOXhT29MfpDKCLM6CTWBvy7Pkd/BsfMS8FjHJXhkV6w4jq3Lc/9BGErFFOl57gImPCyUNEre85MsI1CgjlCI+yTMRh0L0tQq0BtfEjj3pi7cAr6UQDPcNGXuOh+5Qxuwnlc0j4B0YXbe43IEOIP7Bs4yJOlQ602SV1ue04I8uxNKLuag/mfVuLFsM8Re5JhnUIKYAfNgiHk1JGxEkDFbmsyq+aGd3QPhijg6CCDG+CjBKngG7Z6AENs1CA8r69M4UlMYhxgNDb3YKR8IVfqCoa7JNre7mLjQ+4Z/IyQpyCMCviydbN2oRkRLs6M/9ZAjGRM+vsQXo6oK9u8sU03OfOh4QI6o3xGe7ItgdUUZWqhD/vvndgqaRY8bK4OSHLFE5oy+8ABzHGO6rE33tsyJoapsZq9vaf9m/BJUsWMKj/imUIxZWkfwOo7BrZqsZuiYET7q0pMZwpkk7tyBSzpaAffnp42uUSPEgpjwkNLZA0nOSbf7z19H8V6Hwwp7hFlhOU464Q39Inw9F1tbKA7KfqiV35KzCjYjkPzWzgy5kmXJKdBjcCvBbe7VxGuAe3Qqabb9pWOj8WAXgaZCYFkv/Ur3i9h+4l0GtrGT1vajVpizwfqKYwiDDpeg0XvaMkwdKp79qRQgBydixFrMKA2NCGgShuza2bcm6fdYapGXZsOUvrTPQLZCbwvvGT63+WQoAhzD5OpRZ701SUqPAcKebl1nr4rXua7O8CavPgBVwJjNRMOvcEmewIzGP9kDtlj0Z1d9KIxF9gK8Z3XWR38uPABAWcEscCzn/OpuA1zfwI8PLaPnGlbTvm/vv8Jo3TMNZKrX99ob+Amg75bQ6iWY1m4L46T/8ApDoLyGMG4JzJP4wt43rOfFHOCWU/tXGL2BRrcGU1Gk+iMRKnPOHKQLIJUeyF+7WIV26AWpREdezLYAkLLK3eg9/n2LnY6ffNzpwJoiHWaW6/CYU1bnEQ8/yTs0vQM4oIgaGmgXuxyOOxu4hQEaUSW8toppsCfb6nbS9mD+zS+wwHPbzk+cAsG8yaNnEizuJ9pwYurWDxRr4Ca3Nok3+fBExi2A6Vp0qfXUugObP/3B3RhiGk8++CMRo2N/cKEnUWeeO0+s6gDtxkRBe0OGcAOo7VBWJozIpU8ZLkNNLGz2sMNn+VB+tTRWmcfHuq7QZkKoCX1kUlIsSl7OAu6CkZrijc7lPP1cjDI1WNz522DNn5MaK43Lh2bRC+7+G2/QhGnLeUZqDYL1khWBuAwkUzqVDVTZiELBSBtJ/pvkcupo1XTBObavbxwEKGoAcgQuB3vOYgT4SiSqFteXVns70cOVpBxQzoctX2r0r5+gytDB+EUvOEbCinl7n5YVqGkeu6c7IttxGSeNl5deqdL7z5jCy8XoNQfahVIjb5BTRXhtCnd2pr4d2so8KpaQovAcUZUnEcTiQNGUzhUuRN6EwEuUQ+mqZzcnxsc3mn9Bkh93FoP1jgAyvPij8yix1kpcFLVSMp9HJtfSR5engP/dje2xJgDdZJoZWxUpBXSK/ZLTbD2FphAgnw7SWIFzu2kDUVCufMhgVyHIxObWml8lqEpG/4ZCGpTrtx6m30cd2BiOTOVKmEFaqwwr7+LKZXra9C31L09MLzR+4dnu8A3Gih+repGpcSRThRXA2e3rNV9CIsi7NZVwoPhLeeemEVFV+hqYWm7Af9Un3XpwCyVZFSg/ntiOP7Qgom6Td572/zXWyrJ9/ICTU5J6OiWXLBiEPdc8rssCX5Q/B2T9BrHbNgDJU9NwQudifV7pFXK7MLFdOLy6Y5kkfBpBJZRa9q4wV5sIYTSq9YnDlXgVKAVnlDBHzNZrXxlPLIwu/QPZkRUZYfgMbaDchA3OzK53nNVDY2hyuv0Y8AnRqpv2CiCpWHQyWeVx6jHbTMXjD/sDJn/DnzCSH3tdKjxMqpGB9mOaQsUbn45r/vZAYP1O0oDGJNqeV1P6WYwqA0IythPPox9N8zw//5gj6ZNZkWE1u0jewG9nUuzWiJnVXJLvnBFqf4r4PQiHbE62FcoHbLolnxZ+cbkPQ/xcb9z2m/5AlQtwi8Ap0SZuz4hl2IZUhfTu97xGgOAW1fr4OapfRjXyQ4oTgmtiq/84jZbkh8uEzQ/Vf+n3iqbppycc99NoJPj/6E/MfrgMqiptcEG9YXJuufkmqrf0UwlrHC1bd8j7TkIo2BeHSGBn+/aB+pQv9wvMmkqS1eilvwiqyjXJ1w82ZS1/BdaMjWdL6Tq9p8J5pI7c9AstgvCygpIWTDYpkCzhJBWAcdcJHbPayH26DXxZGSYwWCEvBkC41xnRydxME8BOe13MvWR/tuRTFwFgHYVee2aWPom6yQaq0kAgOZHBBLpqSeIE5fD4cZR+reCk9YIInZSPsQ+4XOf0VKOr4GJO6p6yId5dJevioXAb+LPk3+6SpH4MymVCm8RH5wMoAhbJSN1hW8h1noVG26DG90+aiP6jYD8M8bj+Uggs4bQsAlG1HQak5Qj7VAHAYi8CPKCQsLT+lR9Yi21inLI4gGCHgdPx4okzW8vlEXfYcR9TAmevjX7UnDAG1fC7BteTnuOqKY898hDoo4XFGNO/Ex/zO3KJbOvZZjn3La6RCPJfmyRYbW+OET3YvSZ0Bl5EvCViIJoyI4AyxMQIgSL0rFBWIbhKKebcNgHsDmOc2ZDJpdhgyMN3D2DEdymcNYVJqiEX8b92VFLH60V4NzVls96KcDUZ5R3eWk4+F4sSqU2RbuqGaEMlHesJhY8JSG8WLGNnoB3ACpL7MjEy78KCJf1n5pIARsGXAWa6TbXSybWASlKV/ssyiieSGZ9YOctKHuYoPKQxoGeQj8ONq2QFZqeP2NiqEke0b8ci09gybRbv3gKI8ewwxC5zv9AUgKh4MrLTpRO5cG8vEFAXZHL5/Jx77uiWKMkixQmvwHCtweAb7RO8OXSmjyW/PkjjOFV1VZK55XNowW5MpT81ZdfIf6sQiKd5CvUHbFpmp3CnXHW7dnNfN/Zp5cWzO2W+hv9mPUoiFiNX8RYLlOHzTeVIIUnfGU2XFqXD6xHWtCKf4LBPJDueJG4cY3ZDvp8ZazEoaS6NbCPfWUqaYcv+kK5dtsJ7PrcZ9amAxeqLmA8RdzMQzpC/V77M/Mc/JKngGuvYRM1C5QnDh8e09beRl0EUYVY4WKGAZSvstjNmL3HJ0Fw6Ls48wWlIBrIvqu+tlviA6rNdJaAW1HcuI3uRcP1u+r4p4vJw02NSp6AHDRmgAVQvmnkAIVdWTOBetW+Iicsy96b/7Qvlz0xh7sCF1rtpzzkv8OQ8Xogg7l0C6iX5g5bC3s+8TNX30NBRVg+LQ8/C20CrwoQSYA1do7Yr6PliA6EOc29Q/iYUWZckongNy9ObvwhiS9NoqV3329GXJKiKEw9DQ2L0DdWQnHC6iti/5v0DFjZKCweYobmmxCK5ufW4FknJu/XEWSx4is9buSvxHoayT4ww6E/A8zOptb8Rg5zebm6yT+BTSYI2Il04lBTJ0JVPW9ps4VWvlBx1PkbLQD94U0Gm2MWR93Db/KozaJ8g4A2h7ANd6HRIUPFxV5FFrwnEhAARjFJCVDChUXXVrwc61KZE8T6AouSVIf79Y//wnf8wKvLOKr+1FeSR1bp9OMRWFBa6ATK6KqpVqR9ZKRwF255RLIT1al2X6YGRCxeI6cRT0Pi6aP50/8Fz+MprDjh7kN+a1PqFtsAQptMRaPFoaDJfcqBGxJteWm022C8DXdRIZZIRI7pvUtwlgHxmq8qOrC834KLM73oPsr2kxDnnZQaM42lgXRofKgzbNYV8MlP3TptEInj+E5kJoWS9NpW/OSYIt5Zms0mbXByEf3jClaTg50BVlUk99I4r6I4IUGk64XPCUq/S3HsQU0zXZEdzCewg5QjzlwGhQpE/QaMRbSX7nAoSGk8u7goM93U6C5ni1yfpowKgUSQYC9OgeXQ9d9C4ujfeBZmXh2q6F9d4bt82hN2Gky3scNuESs9hFOcqzssnNNiR3pCjtsiJx3afVQgHXs3eH6tfPbSLkJM2amR4upym7eiIG4OsH4w1pfVuE0uRPE85Hl0dzJIyzuGKmZZI1QFliJ4q2nPBCYAgriMlvQCIf8VwE8N34QHnKGAavDR+M7yhsiWPMmOM3AajUw7ylY0ftl4zbZ1Gokb6M0dGOgP0tt9gdVA0BLxxs+4s4xOiKSWgJrDeBk7ZiNk87qw5ZUmld31vocV3WZJIsNZHvCjiold/UTj9xfLx2efvutHtdS5jpHtlyLMs+FfyPESOROyG4c4xVHr3zK7rVGC0pu9HQsICUvkHAmMn9n4SFQtMBcy+qUYdGB53GG2NLfY4+qY52ZzsyGr27cZlh4wxhBVK6Rud52xTTAkdjOnfw7G4uyKmncQU9ch53IwixxOkOoEzuOodjtJ41lktYFMB2DN4h1ym1oT8j7wDL0CYWudhecCEF7N5oJAvpPrql3+8Kh12MRLUWGbqWP4Fe9KvMOLs8ganWHknnwyz3tUJi6eaNHvJ9HHZrgg4QE3wSLO0oozyZj7Qs0NS+jiFKpAtkac0DA7XZ4im2DPipKZfNP00zRIzvoFBfzvxzdZheqD3f5lkDAqkXhkuoZdmZCzm5d3LnluGiz2bXTzpbcWy9FJBLmJglATou1YKS4Rfihx5x/JD8NrZ2id3YTiVwXw4vkstWG1KYjPUz8uIcgzM9fhSfvXP0tIFW1D9Cj3TGd1d+q+EkMZCbglWNZrMCN82pBus9Pe6H9WZclzVVsbUCASVnb9gHA3uSBxnP3Ew1aUUAlDf/qHagsBArqHr7O4O1jl7HfGpjIaxaKgIzLG8jpNftFwFjstnmSDn6Zfo6s0NwoN4IRWQz8FuOCb+dr6VMtBhbg+4gLdMEp7t9/JpRhTzFTbynHLAT3ReYrGhrlHln4TC6DLAO09oUXPPBVvVaRELJRXFYfS1G+jqf8CGECcI+JK/pP9AhKS2apLlP5MRovqr+0x5ih28EB9Qd7l4AnBL8I3r5gFkOPsGsoAKBpGAXcBCGa4rOPYGOtN+E/mGEjYDH5GCb34i4ULqhKoxsfMw1TEmPwuNTVOohk8mrPU7uZrzGUcSMSl9B2jDYycDl5QHuV7Cez6GUiP2J1cPQwKzi7DFEvl26u9Q+zAKo7p6oSnhFHKCrF/x66+Qy9V4gHmQ8+CxC0N8Ec8gDY8oeRyLOejlcepcOn8cbE+dw/kyDpQ1B1w+A6PdTAMlTHYHPQzLlJZ3kKZVQ/dDpOSQA/L/Y1F1XfzQAu9Zc9PStBbxvKnDDJndynVOYqIBtl9JC8NFVJEaaUvMWGnE8yfGd5VodXFeTtMLJ9kL3xXSAhWCg9OgMBy7ojzzUHp1tSlJSVyQSxKHAn3Tpdh0TXjJE9whu+kTQzXyHVDGVOCj7m5/ctJUJmMIu+/yxdK2NPFeOxiD3QU6ljpDoSLvFGoLfMHFex3vdsJ218URW/3JuUANemPVbsV+WEowvKMSxYomgDKAj+uhe8lAgx05qsPsl4m3dMT2rLw2Pl5lTuxNphkN8EUqs9bm6QJAZJWKmbuWwlYyTsv5MCTs150bKZjcX/9ub86qFwWaUwGqTNRDq1qPobtkB607U+yE5nr5f842hU5pJdnHf9+08bI2X4eEZQSWKscHiZUKWEJYihbvZZJaN9Dm/7UZB3BeFFYpoA9D2he+hGoHsoAKmslZGv/VuuKBal+t9ARX0VqTCuX1YXumNFaxiErnb0cb/tP4qOVE9oDlhQdDNX1lXv99/5YXU1inBeOi5ghu/2Sc4s20ETrikIcOWWMfRxcVTsga0eyw12+B/cT9AijnLNFP1GkAd0S0/JmygfHUYQIjgNMkJnH95q1JDrv20G1919NxdFe1OHFV2L6cEn9g1WawhH85s/DUEcUfmquEwXhe1wWLc+tnTQX6WVGheEwQfOitQAnQTnAYJhoDEgZrCJBkF6QBt3ZKL3JwLEng+wOKc2ttKvsgRWV9fkxRJTY3UUxyMwBnFSSheksmqGih8FoW53ToQeTmbkDP2CbnlF2I2GSYa29lM4MNujUPq3FVJ41XqSN8FUZpwpAErUL2RLe7v72gC+WqOe7V13XxYrmhzXL1RSuzvA9dgqNeFg9j4nPIv1ZCTQlS4UDs+v1uFEYeFUfm5b0izEO7DTlw5ijMWgpFZ31TaFHALSCdqrn8zg/dBtpaILdOo3zZlr4hGoicXh84DiZDfpE2My7HOxTKjgpJaQDB6k8tjWbZaWYDac4z0zgAEYZ6UIrY824e+E26QpD0DhqYV0NmFVs1JEa1siK0tE0E/jvhYuSy3EtcMfDcNM9otdn0L9cVdSzEcDb50pHByBBgdPoFhcJB7Da82Mu2O2bCIt8BBbk0UCRraWCeUGVfLpnjkXnualx5QLFUjF7JzzyZA0bNgMOuqW2AqlcFbmBuNG6ZCqBcjl8JyTLSwkSQ5+9DY2d5DcA7XzzmzzQsyCB1txEJlNnVx9ylv6mm0e5Sg8VaY9F5ghl+xWtBGJYc40Xo/b4I3glqSfZ/6cOUlXaaxLipdxTREYl2KMEs2TjzW17q76UyoZya2Ae3zIxodhCSRM4f1j2/iSh1e85XW5lhyE60/GP/lNiGF3JNiIFp1EprqkNXCgX0Uq1nl3wRzTLW+wW6CFI5xY6ra4ybHWw1OnUTzV0MK9Ymnk4oEisgFk8CZaBtOndIbfdEsXXVFDp3Da+YcWP7XzEtuo4JcedFkM1g6isiUHFy/J7xN5Yb4+M3jQKAG5euO/RXDMIMt6scfsM0uyVxDOAira+ONLeAPUEh51WNS0nGMg0NqFDjP/6V9xCqXy2R+/UZyz8MdnhAG+mdMz73QPfjpFo08eg5v0Yp+BE8WCvhRKckxQqQ20ZL6/d/t+/lp7VLyyuJ9J5GNoIu4rqqcJNr6ug66r57FA360RaeNZt6HVc4f0CVNSGuwj90CiV8FzyWsiO/vyShdt05ucDpsOovps3hEKl3f/+tVfwmr75z7G4Y8o8PyGmfokHtsUCRQMHToFEGAnIvk/QWWW1OpDOydEgf5QyuRQn1qft04MfNglZ/gSU/wjrzoeOoGp5TsEKkJM2dOxsKRgO1fKaU6UFIqmm2z7v6ygjySgGeZT7OkuhdqYpM9WezDsdArs+dbSBCyXbw0ktWFOjiq+NrCQbNrax5eGuh4pBu+E2pTc33SvDZaFzyMxsHSOAoT5V+Ty88O/o02GZWz1uA0YMAABrysPdktMApaPEa5pRc7j9pLQITf/Oh6QmPpLR8Dd7EsKhzKyU6GpCDM+BD2f+Nv5rQJZwvQ0uyE7hGTUskLiUarViOyOer6aTwupJbVD6JWn1O12Mofe/riceis+dPwSyNrjJrx+DeopyJsORrlvikOLwKKQCVItyAbdyiTDDTYkRSqC6sQFm1A5/ZRe0n3R8Q8oADlGeWEoRn+wQo91uH1M0ye4zoYb7cVUmjj/RCDCOs6eYg8gt6lkSfl6l1l2S71Zxu0uRXliNqTLzPiUBTvtttBdcH5SZ/avUvMYuWn0c3aUHDrMbVCw4hlAKmlDlhWZBNPKSHleQjL31vqsWx4rcloLdgQBhnvsvVa7IfhjvfDa+lFrwwH0NZ6/MQ0sJ+Bhix3zxRSYWjnGIT6IhyBfy21KIY0u8qc6yjnpjQ/30SrBiRC0+a/UCt7RODIUOC5m2oOBfIkbos4ayDvUHR8s+QLMV8jzejwzE5aVlTv8P+i9D/xHIHV2pV3xtRi8ZYWHdF40z+jUvV69KC1Mx4PxljEBw/mdlZv60vlrVWAqSZC49yS++jaQh4YDZ+zgzxlmonAm9RaKre6Q18oaSd6nMiGr7LJAZyxXFv9RTaq8tbJ7KtrYsuEUqei1pIHLsYtLPVL0i47XY+dBg6FYNPPVMjNV9bnytmp2jf4PMqaWLEN1SYalr0LVT5zLvrMSUaiAvHGvEZnQf8obGDfR4HTyaEvOuZaflkKSR1BrsfPuaMTYmsLhihtciiTQ0DmuU7o1jZqagg6/mkHYupec5IVDXXlHYjBMlb4qb2YDTdzIRqsnmjjP//Sk3Jne5sDb5lHPTMXS4vxc57ylRPNAobfM81AvWo6/ZjPqdar+r+bh/DcZ7Us3fbiseXh50TyYDQVzulWPYQ4ZS/FqcVIgl+79gyaeCb+Z2rAdPMvGQBEpP6XPDSaYHr4oO9/IIBXITtS8hl1sZxWOE1duAqiZTOJW4Cm9M0HySA0gOgjM4/Ov+QRbw0GGMhTyS4lhjO8+4EO2USToyQuIqD7YFGdnbfR/yv8gF+b6Nyf2J2u19+/w94jEWZdaeO0hKLggunkI5liGB4s/VL2w1rQ1RsgB0mTCXLJv4FwaOu2riipJiU+u41n1JHvaQVj8OTUlqmC9bQNpikrwifiPc5F3YkohY11Kf1gzvVatTi2yoF2mUWaN3NOfwz8tqSwbi8btH2IJ4qvMSkO5JDJxcugAT3WgP1opgWnBxTAyeTlyYwkxMlr65zJ2UsJ30ufbzbZUPzDSOrXiPrkARZsxfImXPnCLEIyZPbq2Ppxx6PPnvqhX0Gg56ewSwwdGbgErtrJJa7nW+gcjz2xVodRorgHN6f781HKelNTWQv39weZQh0yq/3HDUQQMDqdn9vFgc9Uso63gw54StxLRAGks4d7r/MLkunpUsX7btujDbrux/hin3H5KX18/N++G/seVtICmC2QMFvygH8R+7U2x5jaceOvf6v5Lm+hj0RAO0nDMNsjd01tsfcs+6FZafWFN5H5fimL6CiVc/wDcJzDYLu5lvE9Kd8JnVIleE4AvI2k73Df0RT84lgYbPZUYJ7y3+ur3k7pUpEOT9sE9GowfpIbuQ8tIPZ5ZLapgle1xOxPTQlYctHlPIuVQmMy5EqJq7LrrM5YktAbaapLiox1LzxmSxI1FkHt2YNlLASUkSVhpNOTFPZuep2JbXjslFCMD54TSbQ6WnWkII6WSgW/YfoCvWmP+0UngRY9vze8JNdzTPqU6xA/iXfV6/bLcZTrnRyNz0esMKN272IVQASiMGSszoaUHho++lIlKnpBX29CShfGC9OLj95M9/KGahA9NG0j5bgHB9KOIhEISL+cMlUzPnbNjsAsRs6Ov0kw8gEwu2yRYeJC6HHMo7vHXYeWr/on6z6A4xDo/ttlkbxSvPwKbj7OOhfV+TQF6QMmsqc9e/r743XNbCbh3YTVOsUJPqCBBZP2XfTTDu06NILPe8C9wLQ36kjFBibalnNIQ3LXyPMHMEMZldDYOuyfKCKLxx+N45mjjtaCMZjibsWqD5B0jlS9euzpCKVsRIcgg2v6dIEXJ/S/p2b+NwKF14eF9AXeLyRLkTtV23VAXzqgF4cQ+/XYPvc89D9bhO7wbaaZx29Y+AJB2sFKJy2PoZhijrhTLtV+D7td556FGd5rsuEVUH9oH0M51Vs4RlbVyEltLACtaVjqV7v1SM45h0C9UTAgif7wdJcXCXyBiZY2RsZUnDympty6Rh5f9McREFrIu3vCE8M/HvPeeREj/YfX3gQ9LY3MBcUeCbhKmAMYEoSAR26vIubH5XobZiZGBXZTrVukkSvcJkFQfb0M0vaCOLajb7xDL0wPnZQg1rdEEPCdqV8hTOXAN+aSBZLHlJQz8+U8tZfOIyMS4pe4s9EC04ATrN7+gHYIWfT1HBXv5HFcqvMZqwYqdPsLLUjRWxUB+X4EIuFOylwxgXJd+dWia8Fh/9Q1QDNb7+58CsbW1dgrDSn1kn6/q85G4YY1q5glUcJacicP/UV3OQx+YzyRp2UXJsuI2T+jojG4bhOk2ePGGpABNoqUJuUP8K90zlxgyvHTfsGJoij++BeO3778Bgz+gUFatD09Uds6Jc1IMdOYSNmFAwPpUg1dk7nNJCs3ZkXjzHM6dw0p/lUy0xD2+bP1XOfWdRP0PRUPFqCCVbJ+5duNEVfeNmalE6lqhCaco+F4wTgltTDZHNd2zPyFDRMb0+wv3afOx9FW34y80mMCHjQABrvuNPS9OET4lUQ7jufuk3/rJSUaveTuY1eg1+5e+dZXm2y7o4c9/UL5bv+u+xcooq4lW21J33UWxD5HcEoKirmAJvMUMgqkCnY4m8N+CFj1xsnoaCtUxNCBIdKAqPieokMrTooD/9V+ob5NnV2WidFxL15tW1Z8JSXu/MZX2lGMo9nRMxcxPB6eVmpIsIsq9omf5BvebjM4ord/WeoJhQk9nmSXrVxu23qBpt+T/lZxY6s9KGfQGC2gcnTrq1pMiOZE6Y8/5YCn8kZam4DbAJPAk5LNvGMa9NOfeaGwzVujszrS1oATpseD8fPtNQtOpaE/KO6AxAJg/Z7xBVIuMj8PdtKAWi3UErDcfgPgiVaPrSe1f/z0ZEc/jX5DwDG2m0lAxqsp2Fv/Jc+5/p6G1CWM1L3hCaIH8ZWfLf+LuqnGwXfUD1KK4Ev0sbcxIhhLIVNEJzZ7n2FuCKdUuRyEuB6BbilVpc8wU9Qsl1CyngtDr6gwThevusA26oEXTfg7g3TJ53SI0vUjkI9boBoJv7nOJZqg6eoisjuS54R4Mia3R9Wrisl/+1RsyXaxRosnnIoCX40NPT6rbQfSFd2v08PLYFSMINJIe/Q2beyRa6tnQxCfIKGond9U5u8UoCWShtLiwKHN8OEr2hlYE6dNPPQG21JVaF53JVYtxJe+kFpL2rS3l57sdTnvNuByygeC6B5NjokZL1UKp9O/RyV62uyg8gxW3md1F3EPATe8GYVV9u5Vd/q21SpYT43W+yze6Lwr/unMGruiIsIlA6wUlJYKqzZjcvDBvP9PpTyYIDdp+6etClLx7PClREzcS6yuILuGqtEt+Mok+DV1L+pAmEKu2OxmTYODlnUA4iW7yz3Lkcmnvcchni4WbycxrSr638ceRAYYTO0ZsGb31GzU2LjVoVojUqgztiWLd8gFQXHWsiN3Q18VwVskCKB6SHJJqqIPGWMvOIufhWKlZMdoY2Ante/IMNRM8b0XpyaMcbAJTF4mvV5++iW1PmMyx5CAILaMCvA0VxOMiXxld8JrfdMiQm96QlsY3SSRZLzaxIkGsn8WzIWcWawPT9BdfGrq3pnRHRDXmcVYrjAk1njJU+g35qA1KdsYRteONQTg+bSN0j1x+3SkcaRb6BNCb2R9fJkZbpUJzOoJfqqzVrVOQPDSikR/WdEuPU/oAFcVIVuaX3JveMts2nIETs+o9d7ETGPq/un0BBr263DGsos7dLeBIbJpal1o4RC2O/MAj9xYFSVyHUbQIEOBT+/JaohRXn8Nva5FZ+sDKJCsnlFHhURcNbNuDhV0a/M72A7Fs/eTrlcQ9lTc4mHUWg7N6YiK+9RfLc3/Lz+tf2m1hRE7CxIOqqPdR9hvTlbL85K6UefigI7vgj0UXP4kfOEzmo1nceVdj4EoQc4itgGAaRvNHH8nkVitKhbPGg7yQ05aSKrejlEBPXh6aYjYlzDlWPImdshOmBN4GUuCoN1sxPSLJO5zptvI6eUZjdAAzDkQULQt1xH3t5Ss2mVwaxL4hYuATg1cIGGCwuvODmAOIEU2PdOd2Mb7PJWbMoT8VvW23xTsxOZkMUAqVE/jOFqIv3SrRZIceZkBoTFwRpzqGCWsR7GgxdvdlFL0sCVFbziNnySQmYfXTCdgeQhJIBOFVIzjheBgZdQwN+rKMmLYP14BIHrCbeDX7vnHUOc24HywwmF4j+Vjtm7Q79Xx/FVShCK0ZQB0jFKQE3vjy0iO30oENESQw57Jx0++1v7LpPl1M4mByO+kIOB8a2LGg09lMvtAEJM3nK7Y95R70irNMQ6RgEgz8wsHzLP/wBhUH44UIBppmhLA6rPxtNVNg2m6Xvu/s4/g0SvEFqZmmkaT5XIUrXP6SyTWuTk/TfHohf8Y3Dwkn4GMJwE55ZvPT4q1PUEBBl3nW7kOwcPIp9127R7qyD3K+ekEKtD3MrWicpY7aJmH/TyJnD3WK1EmJsh/lB3JsbkrIex4iGOtQotMKccieOHOAT6Smim47FtVVSySKISqleEiW7tcxQCZi16K3SwO0S+ntVo4yeXFPpzoZh3tumhSEe1W7EOD+JKQNEWP2nSY0Sx95MfkXNF4HOPGV7PUC6uHiSIFQZvcx3Y3jQlr7X+FJdrPjarwU5q8zUPNycXHg90M50Mxj7bU0+pmSgfbW7/eD/X2N4gUqKUc5yUVt0rXnDr7aayZXlwsBli10bgsepH88aKfxixe8iRWo4YE6cEvlnSRTAjq8ZA6PYkgSsEdSBxIBD5SD77ORtjq2bVSSW8tPBnLKRsLjHkT8rW+aQR+ngTKyJHXALV1gXWggkfXeoURjABbMy342wcBkrib+5cDP6cjF3i1Tc0vxJO+9PN6a5oesVumD2Hy+kvZxhnRN2FlBgoSvq+3pwHo26exyA3ciZ9SEK61XJFgsl9FntIW3oGOgl2GR3d/48ZLNOokiD4iC4LJ9p6XZ/rNcu6rTlIfb7CO6O//dhRPuZbJO1g44NjKS79tGzDXaiQiCKnNQU7+l3t6NAaEYDrUuUVvqvq6JGRC9+vMOfrmGxY0o0S0/8qBi1JGtKlcB/j39pcU+h13STDzalVIlrmJo21/FpRnkNF48opF6qMWzra/2LWgpxULMKolSRhnpv01NBRlWUmymLs3pJ8nUissBCZlCxWHSnF4Tg8XKR2CK7XC5YxbbbMKXPFgxyJoAF1WsoEMqY6xkqdqQoLfDQg+5uIlIYTa70nihxlYkghNLqDgJposS9AeACOcO+44967ZLygKcHij19OCkb6vIs3NRKVRsSfc1Uf8/oZ3exEfqeu5fNiGk+nIVc4hZ2w+MLNhj08mpBC4/b3+JDTpQVNcqLzTk7Lt0uK7AMv3QDBNrvOP3lLkNkQoTTgoTPl/yim+RUgmNqzXrbS8+l+ijpc8zIFmz//Ocrwnn1OY14ELg5Au/JI0EikE5ifn+W96lxRNq7rlnrfKdT6mRS/V4MQqoSb1wKDVCxh4WMX+YH/iPXs3x6bQ9NthmbpP0ex/L6lLk73imt5G5Zzj8hBSGPtPy4Bp1Q9gR1Z+lz5MtK7sLnW5abUOq8EqRP45w/+Cc1Wrvd8JxHopU5u/6poUQ2njdlVGrtzfbL2EAZmqPAQsc8Hs9kWoSRLoqjAJjvyVCPIIxpJ5R36u/AFyAVq6yrHei/SOy3mveTixZJ4HcL9WuZCKgVVQpq5001KkFlEnT+0YtxSaf5mcKv1jHRJbu73cvbBJ+pJZWzPAoUyxc8eDAtt1wIV42AmK4vOaZGqQhizmU0znXFcqKdsJlYFQ2z4tMe0e3eyXHLUldXu46o+o9+1kStWcBeJytivxDHv86dVw9dFpODh1kUphm04bXodmG8HMkCsIj2j7Jls6KGC93BxFh2xkFDL6TTVJe486oIgiN1NqCbzxYpWX9Wu63iNWxhT9lBKPcnnTZfVurvxaCNXaZk4TUc/19veqDDcM0KsAk74Tsz/JY87T0vFfsAqEbVjmLxyEk0F9joJaPZHlyimGfJSNpXUi25tr59jiuwsVbU8xii4ZuYDE7S6Duf8cINy7D+9BZ96TJq6vzeFOhOc7m8CaMZAni5P68ewxVg7IeXEYG3p3dE5h5W/utOlja+dYNmFK4//dYlQbxokM2O2vIpU91ca05J12z80tIoIf0Q9K7xAfKld7jrvhSSebk0+sY9SVXLW819kVEXWF3oS42gPzb/MInAR/K2IAKr8sFpQHGwLXSRyBZOPUFBeKnghUdosw93vE60nMwOb6ZUSmo6yeZLX3b4MuiRpFQqunLMEKl6NlDp9efTNxelgQDl5lPJ59ADEkHvx8hQRp/GKqaeDDwgfn194wNd29ZWyBYrZeMQ4xfTTxd5uX7NLLcmwHIw6m0PVs/LV10UX8juf+g5PqFt5zRq0ZVTcoVhJI4fKFy7cNq+Jabt9wlV5WyY3G3iSAei3sgbWyACnpHKNWDLPsYKIhhXmIcHjNr4tf+qOmdD7z0sh08XfeE0pj84cG8CKvrZ/8iNXNrttvlWWEUuR9MBbmLJ0ZPM0fW7fP2Yb/Rz9nSGLlbDwTuyHNg28I3vR9ULCBRmApB7FFc8acfgf4CSuftmI8JWDOsXWmlAVkPyKh4OCdNNkELkR8A2o1bgGwg+3R/QBtx2si4r0VY1F3o1T64s9Iu29N5Swo9sR98aCkIGFcKKN21NcaVYCnyZo11vMEhmbIvLNQtxK71MscZiRwX6BlulOITbQGhJ1LK5tuJLuCXHDa5P8HNurAphXPvfWgVR0WWA4R9ddRfg9RChP2X29P706Odhw4cdQgeB9xkrPuW0yAMQyIYf2TeltP1JcbFomMd9BNgFwPQi7TDdbJG9Y0D5DqSd2Db9In2C1vDuPGAyfKg+h7KigZgQX9qjyp9uFjT7t98hZEQbHykcsPR6tQOu+T2Kp5PpiXFevzU71A6WDasX3vqZyf6mRzYNNIWAAEFYoH1JAu9po7UDUFoEdRLUi0At5XIZAbE4+Ji0E+VbjTtXOA0s4Pf3/EL0YmrHPH8ufzRvaslfbA661EOaoEZMmeD75jNx7J4bRiEq/ZcdD833V5Z7wsyAfHbtPTGNf/kVrTKtwlLpqx22GM8PEObgSbJHwcqRwpbldE9LtgS6T5aLqjAJCbEW4qjSB+i10pQTrzZqcNsUyH1oDv/GyuC+BBY/cY7aEB4XqNg6gWNjAs/lZNYCE/g6Rc/bTXkWnagkZbPey8YLEnfbVinhnyPGcxIA1rKP965dC1HblXbXyO3++ucS/32x2QsTqNZW5zC0Ymjvo3Xf7inZ54ctJE/iw1sbEDYnD2kULlU/EyNbQDL/zcRxWbiSk1qxVLvB7q9JKval731wzs9LRJGsya6ND9tlnbvIn9BRvq8eOrwUfENAYxZxRnxKZLV6Mr7jDYjXSYGuQ/ThLzGu+Op64qWmyBYhprqKxUtKkLgMbZodlOrlCChVxCOua1Wo9yBdqQm8nD4Q4vbulvvEXx+JU7QdX35s1iHozO/1KuJi741rFcaKWJtTSnz8qaN/Y92+pYZq3DF5olg68OsYazxCNe4ZNNf7+2542BNDRpMRhtV8fZ3OcjYxOVU2zcJpKJMtd+69AfF25qUSnUUAKwGBVHh1Y3iaR7IfizS2UrqWw06qvyCtE6ELuWJ7XM21bTWwvSclfWnplNfgAoVTh2hj73FAfVkkWqNBzxEEsNSMVV852MGxLBPxs44FDNBROkM4l7A1Sh3tbD8Nd+7MTmFNmZGoy5CakUkSelClyLYAg6cMzk7ZzN9G/To0P4n232uNhVdBkOgHxbjFXHV8r+TJMNaANuZosnv4wL0kPuDoJzu13LjuO3uFAFX6cs73giPTI8z4CeuNHGQDQ5PxUOrQOeL4Av8nrutDb/Dbi6T7XVR39OEs1QxUu5Qexjkrw+pN7cRPuuluvN1m/nhtz00kKeUyQj76LkL9aAJTdjYKjG+2GHceuZViL6FDMq2v91286GnoNfdoYmN9qkD3EiTJ1mcs/zGtOPX7cbTlC5uPW34aGfv0fn8e3rjhfdSiekQtXLYPstQsbjDSCHG8i2MPA6UyGvDvH0SYlbZGeFaUm8y2JBJZaLvW572ekaO9mMAKkC021S51VppVvGi+D74kQ2i3P37eenB9ZCpWe5Nl6btqm8svLmTyMjQKXGjxkbQpTIPKBEXIoF+j4BsWP1PfClxX4aN8ipbKUnC3Op1ehX09vKQVvp1GXSN9za89gGQAp/WBWeabLX/VH7s/EdMrATRwSlnYGXMdgP4YIQ3LP3bD00HqIFoxO2eMu4dpuE7SIG+royU1LL6ezsfVPQ5TyWmJCB2ajyN3hFya+gKvmIpx5HFqNOOFf22LWdj8qGwboWVG9mwNOIUNvFlji3KcBUWzfqgt8LtI/E4TonHXgXAVCG1vsZ3T4MDOpOlBZ2SQ1/MLMpEO4QZItjqRHEcZEwFZfMn0oNKL9qF7UIZGKuZG+gzucDkftl97fGV+h18DTWaUvRPeRblKES8q3gecxiFrkif5ycwYNlazbTir8WgxkDtrjRyofrZXYde+OmK6gubFg4ecNuuzK7NZ+h9RXrKh4uAqdOGJvIflF2gomp74Nou7mCq6VKijmC1xqTajZ/F1y6RU0Q6uJ0mfkq0SGhojg0yB9SGgHzRD1GnZcrPBGYwIBAr4rjv40+PIKgvVLtQUJgnaz2rTr4448dh0/AsPH24h1dAuDnXARj0D1ZTYngjc1eKAPKxOeLjdgwtrHUx2LIOC0zLQg4BSudubhLp2qfJS58Io3ctJdLV+S+iT9w5yV61yD6KSqR3nAqYLEboz/M3DD+TXiQNfvwuG9gFWn5PjKB/yqOA5pJWhNBsa9BhLGyu8hkRMHOxEF5y4ySX/PfNTK2H909qepBh5fdc9wz2HQyrBEVTOEPevl4lkR/ZZY1V2Bbbods76+xSWM2VwAo9vOWjijqUl7GqcN+61R9KcWLEKztLKRw33VBKtcBKtPds/WIirNYznSRdLYMIn8YfzIW3JeZ5yEPoHaX4Xf3DdBg3Iwe5jYBgjm6A5KYFJ8F7kxuQ6QM+rinAWk3ZVpErst90LO+jlXN0lbDoGTdFw/EMqy3afxvER7ug5TOU9JZVOUqY8AHUsn3AfdKthpV3rjYWjqJ3r4ZIohtTUyOvWe2KWd/WrYgKOx0kda+SfkyuUn3vjNL3f8Nk85AJ7meEeYvxLBKbzj0ee/5D5/Gh8xsnYQBnwf5FSuQF5z/1G+yDMh3J2zfbNMaPbcp506z0ojckkMTlel3I0KFXSFnIWCRoAaSrpq0nJIVq+wdbm8hR4i72XSYlhIFduaAowXD9MsjlGQNDC4ID6m07ecqsrxwDPPWcM8k/0kUknRBglmkYfx7zYK1FTor6J2V/DmdwdR98OmuVjjxi+5aiQxD6h2/BEWwN1pwFDahVMZSRoS8DKgjldCjZHx1CWHJHH5lJCUF3FSQM1BPzre+pHUs/Lmmq1WxWpRB3SmUSgINDOn48zXm/vifBS/G2Eh4J2dwCqaE1pVGR9hzquFkxFgoqAUq2+rwSGEa2kPstHdvwMZ/VSDJC+vAax/cIE7f4rniSXP7wrpmLDpBFqlN7P7aQQP67Ion8cxRGRcp5BT71bhaA2ZIliMvMb7/tnQiFmu4Ccz1TNzVbVr4hka7PgeQUUFfPTV9pTJqhgWI82ZBWUZM53k+7g1Gcvs0V+H2BQrreNkbyu93qW81V25d8m16443i3KOM8ny5iRI9aFDWY1iFoBn8FLOYGqtywdEYc/A1mGCqa3a3eNIfacgI/5GF18hiWTsv2sW0yuTu2quwUkpMOubFo8VqJjoLS6qlOtTiro5ZRUH6wmSwFUbsVl6Jx1Kbpm03VhbCBVe6OtK4mVfZn6LvPVcsQeo0jdoFevqRRve5MGsd0q5ffW3H243bhtgI3oKdvXQx3301JzNtv5qgSzp/wbb5fLUql7E69GCCuSlyEwWaVw9Tr+FPIN/ptLOs+wFWi0zs/IT5EiLcyryNT7lxN+2KBhg2syf1fjnxqKASaf/TmLNtKmcDrg6CbdXLQFhv/+0u4N9INx0fQYg1bFJOb6bSf1O38m96aaS0FC24JBaOyQ6rOdYEXmgE5w5ChJngO4Yl9CvnBSwfz0OEFAtFrbS4yJIbW7hJsh0i79NjIc5+3AFemA1SD4Ei23REG8MLOJq06mdKTZqNo7VopTL2yOWFhmfqiMMTGFh69XqByVWVIlwC8ckrZv4LqyS9jAPr0z9bXI8wSkxhJylGRj5ZOlVSGqclKjTwZZjEUmYg+LUgCojOzaXpsPJanJB4YfzxIYwQN0/O1O0OzjMBeICMvnwbODoTE3HGLxsrwNpKHNB8fnzs9dxPuRu3jD7MZcAeYAIBkHtI1GZCTNi0Qf3oiTfJQg0Uc+u2YDTwKddYgfBfaV5Np6oxRToWldzIRbUOCSr/vTxTHAeKdP5Hl31planFaqyfmj9CZ2hqBy/Q8flRNDGgnl6QGNWfuKlJcoJesD0/McDYKN2L+gDsqgVVqdsGkJa4OYelpKBTgVjS/mND68LPb/b1b1ytWYi/bSkhUwOkLoDyXrQxP6sp8ksKYYSVyFLKrWKaBTyXZV8JNoOYXcUtt79SKQQs9dhzysZuuNpxotB4H51B7z3CFBH3dZflzhSR2TD/JHcJGpBWRSK3xtgzJevkuFmNM+n/EfAUixyV0wuQJDH30ZdAXqEB4dPvFJo5FjS7zdX/RjGDvZZJCaiPDSsOAjDtbsAZQWb5Y3RawvMldaC9jtMKI3MWkV01fL7sQCmXqmLoONTMq36hE4H6WyrHEODjUXhS2w6e3Vccvu33PJ3q7gzZQWvrhiQHAEIuTgdB+h47ECUv8bmNJDey2YNKsOXzR52Ps48C+mjwRyDDp5j+AxzXrnOvq/8RNoUCikekBS6r/Aoyteim38AXE5c296pnCumYtf6+4NRsBFNQptof7Fe7wL354tVmTmyGqJ1EIDiyOcpZQdkZyFfAaZqu4DTsk/nKGFLis+HVvW/5AKLUZ8oeRLIy/4hwtdYHcVEP4uT6F8U3LHpWOwRQ3xrfyecfiDAFoCWTXTKwjx7FkcYLTZ57oih2f2vwtbSQ+1zgWK2DMlK+MODFnPIAQe6LvqMBeone/T3Qj8l/PuThxrY5/BbcWBvsNFc3f/FTxVJnebPCYayLTvOARB0OVJTaZqYnLfrvXgT+riA6WKMbEY8JxTx98BboqkX3VAObQtr7+2XCmRxeCzq5SDEMjtiywDcOcCNpM2EoUALz7j+Dcw4sma53MdZUnW2iFdXJ1Lj99k+j0nxNTHP2so4m+zmOmBEyQ3UgCPvy4A1izx2YNElZM6xEIRPByJ+YGLCxfakdDgU2YrjMWX4Vtgfz2xfSsslFCXvcC8toDVcXDIyjhBE5e5eHUXxsx1x/sjGGLc5Sy6Mp53ZGl0Xoo0UcyUIrgj6DDiR5FgfkM+UTFJfEgj/NfrRr5jMyxDjTCnt8ayCB3262DctxIyWoAaCpk1AlqeUjeataZBs1s6thXbC9EeefnjRVJxGiHSQm/mBjXuEo0J5xSKRzjn270HcDByNjlQkiZXeHWEcAs8lS7arwJe9RltZVwkvzZR51FIX3BvfR3SKgHmCTQDQM6pzcSdJfPFMgBRZj9igxjTEQW7IEIY8Loo3tD6z29EqrmIDsEJKW+aY1eu/lj4e3eZY//MLcMt6NNfb9ckVJFovM0CfYINzm80404MGU9vNxhuNTIbUXlBfLLmXHKax8U2XPjAO2kFaehoEmcur00MGJ9ij5UKimWwd9ZAei65gAa6IfblDwWdcATgkPOJdqOhQHlt6sNcODG/xjjxoQnqyAqBC0O1u6lDDr8feduKcdfmwUP693rlte/1jHWpsulf/cEbId249w7VE7Krfn6WYY5AXSY6M69XYwon4Ev6IOQ+dz0o2NO0CwMY+458e3N6grYa9ffKELT6dchRh/YlQ7n4R1g6OWS4ntGBgCcMbm3zkRK4XT9P+LF2BcSEMHbDbOS+cA6oXqxYIvopLBSwHJoIVxoRYjvk0qCgNf7x35VGhbONUbmSbqwOZtHdpMc1VLcnak6ID2TN1EMwwV0WtEeGNTcBGPEpbZ+sUq5TdW3mFjqJqqcaLutn5n5rKcR0Z14XTjCIer0rDgjBZ160XlbMeao64U8i+WJo3fFyKdtWSSBv7pNEajYt37X3YBbR9KHWHAE4FjZLCjiaQkm6ezbXwxeDXEiJd3CaWVsuL+VZEw5vhB5BlISBkfX1QpvgZQZmGI45D5ua3N04eTdGOeuPG4R9af4TFbGQYGYfWq2exFqHFsNee4enWM0g9QI3uOSZvjOFYLNSTs//PG+33cnuoZloT3xI4vWHawVCe/CyY/eo50QZR8sMESonUskyqnIOTC1LZl9rEWbDELab8qBWeazcXX5tdnhQM7qrGplEzFPvfGmXANpOFIQ23COp12CQqM0s+pw+eQDfTM+669uZv5gxVLkzQs7bxbhYrg/Jfg9FDXZpAT+FhQ88UhoUa/NOLe0gQKsaKE3hhtH8h79KqiTAofFn9J74q4/NOTAl/OLJBOmY14rIry3MNLEMFwpZhZbimqQvGLy5y7AAZ7Bx/qwFdfDeGKx291SZOTf3xpC2bM2bVcx3QDKbhtTyXyEHJpVYWTuXMl6Mu8dWCGQEx+BGCyHzPT1slbVhhAB+I9soMADeDB0R/j6zRPiuczmfjSQBfvbj1Z32pes615R8P198GBKWwdltqPzA9uNV+xL1WtlE0exS1zzr7rlawhj3HK5F7bY8UOpilTIfgJXVhybPUxA8TXvUZiTs7lJRe9/p7UWdVzmw5bA2VzgnINuxHIDzc19QcQ3vh6wCQcNBzpnCeiJrRbL6A4wnRFKUTxK3gCLCgfgzrZsKagffZ1x2pz8fIY1h3oF2g25ZkDMHvId1RlKI4vMHrjDAC4QjShbBmU4givwrdYZFmi242gfEkerVovhrdCdIpIZnRRJcavMsJcdii0P0OLFC5hrHq85o/EYOiKQo9/2bnHjlPozfD8KwA/gK6cx9YDtGJnPeh0sDxOqhvc4esMP24VGJ/fjcUvw05A3fIV4UUA29RbYsDslL10w0m9BB3y1hbBxGSzDOGUW491AQkrWVl8t3JVehHEBXpp7Wpbm2ROmyEIBt/sUOYI1jRH5+RGusyiwmt4Sa+4EyOif37+c2/wyWYZIG/8f3xQJV36ntc/MdvzkxkD+9S0t48G2ikvzOzda2xHCVP4RNr1Jl80y0LgxqIv1oWfxUo+DFItW55UjXq5baq5sY3BvEDmGUprSFg+bhTuBuE0IlvX/j3lO8gtigVDPpWTDYJF6gHBeo6ydw/qh+QC+/RqXVPy/CpNs2EGoMpLQpWrD87x21QJgLsTHu39S05qKzHA7FJ+3EFVnxPNawO/Je7oexKFLJUi70Wu16apMlMNonp/jFVupMY4YZoEqcN1oFQNf1XbDEzzj0dWbGE55SfuER+Yk8uxiZjyuhhduWzl0N4Fcnmfbkzjz2y5X7euWypHFjULCTvVOZ0Sxjl+t9M3lt9eT6Ov397DlJgg24/5stdidQBJLQiDHUaqqCHoapgwYj0m6LezvGS1pPEsbtvuBYl+y63Kbfx8cFfWOe6R4/s9vWeVl0F3MFhfVhaH1Y9WK9Pnuarhfs2UaSkcEVoi56QcpyYYwBrECPGkFqApt2Is1nhGgTg3xxjNI5rfxnnbO+HJo0Bxfn+7sBmsmlrbFHmFIIIJfBr6cTUqX1ODHh01A7iJHh/5j9rq7zLU7U3b68GCUTbHqnpYxYnpfEWf4Lp4HmS2xdekLULwyqurTWrgWH6lhPft0OlT193FOE9mNc9nPNPP4rka/pfedaV7JKOocDDiXTXv7bEQqtSG8MCtNU1coaPVaF7U8QM0kYBYMl+SdL17TrXcLZZGVwbAK1OgKZhkOpnorj8I+RAUGixHYylfkI6Y8epZJ862rRFRgpJtZ3ETC921tsIah05DNuAuM2+i+TwpkZyU9HaymjkCKL7oyYeALCg/I/pdna46x0G9DoQq5FKi/IcMZqphgWhOxQ3RJCDvVh5TsR/YAT4yXLfXotBAOIwLo6qTEkgBKQ+Gng7Qk3lr8ljGbjaAwfg8acxsQahBcd/lr/qWtfJnTugMKSx4slIwwZ6MQaxxJn7wAWYE6jrOU8syryA71JRcAs6D9v9u2FlQCAJg5pWwhcjbLWrUQ7pxEpL5eEICYa0PhD+O2tAvUqaSkuXDsmtCuhjaynNekH/wpnqxBdS7fDp0EDWHSU0YBSWgkF0kBrpUiO5Y6sSeOj4ApO9emWuYIdkEQQe7YwXYsFgrS+iEkaeO0w5adrEfYbfcEAQ0DwD3X5iFzrbIKeVSgAXADQn5g+ODnPOjULDIWDVVqnd//+wZvNJWVGTq0YNd9bRzmdqmE/vNDwU3pKc9+a232lYWZWJ2s2PbokhPvnoaP4nd7AAhK/hnp8JhXL33/S8Qn8dgMmWakE+gnUso9tPj6MHwFORwfWTQXKcqAwRNu90VU9tZlVc73O4Ekum/9213gCUFrR5QyWpTdAagAJ4+8yzYUG8RZxKyLgk30lxVokwVvwL/DQN9LJNwas0dqq5p3JTVPZea4RUIqAtAzyuRnQej/YVGVmXTlqh0F0+dZAG4z71Rtuk98/IogbTIZwaqmnQhgIjVfhIZpQEOltGwcPBMVpapvFuKMuq9k+Cist9fZuoC5Pq198+hfds038cL0dntSQwt+rzfuK9YTzJIr9pPalxVsCUNrHNAPs238Kn7VJt0bBK8hXxUYCMfjZpO6JPPg/cFzJVI2/xItSEG7nmAeozS87ixPJFoA9J1ynQs2OtUsE8R2nED+XKUx3ZGn8uJgmiGdnCS0Zx1o1FMC/9r8d+3pGZYstutfiVNzP3q3EjkOUgnNiABmxe/Z9CTph8ZvNR+wOBpUCIQMj9vatYcoRcm7YoAY6NUu1vhkXHqY/3Jy0BxEEcHpypJJuzhRQU8IMvKGIQOsx858Wv5kXoJ17ikGD3S/Ouo2DRhrgL42j0qoZCTWY9PGY7DAE/XYMd/O8mZfNHu2jY3re6tfhO22X8VtKp8SNxOJ2UlIGS3xffwp8lczqvjBxYbCpazYAgz6Pr6emmaYpF8ZBPQQBM3pWCGRVrSpQ212SFWVeI3ZW8Bybf7yDI3eUDMxHmM2hguiqGHmfq9E+O3RcO9hIpFo0Ga2M+2LEqywg64xmCXrg5suBTHqPZp3lEs0GS1uYx1iAYI+L0n5GQDYzfKXDoWu7J2WCOJfRdZYXbPG7/tl/1pWwBI+k1YV34Nz7YZEdyBpBBEXTwPrx2TJteItcA3LXrAOxR66cQ91eQQx6KjeY4D2l36FOd85jppxzPNibmCaGdiMVKSZORcKL1RVpy2yg02+9VRn5hiFePZ2zAzf30lbIUc0MB2IO04UvMtWhT+9CSLISU+hAjiHniLi2ZCclECRF8gMBYv9lcBfPgZO7DSQb+w9yx2HkXeUyXetWy+9kcKD1d8F38BEi0niyI6Nbi7SgWk+jS1q1uY5sM3K8/t5vOwrYT6gkQXdJe2GbYOhjrXcDS68pDDMv0WMEinQVdgk/5rSLfLJLZM7YbSmujQHFWhHeTBpStmhTxwkjSsxxK1w4AeHAQM8r4CSjkN3ZtMhbUxvlCQNvELeOZfm+UweZG61KISPPzIDSRW/ID6B8eu9S8UAacnkZV5tQHTHHdPGmxB3hFx7xw66HLU/vXD5nT1ClSuuMjLXUAv7AmVqlK8rzcpsVZ7Os+pGkYVAzP56OYSXxLkmFQ1DT6jUP7z5g4YyLdF7PYwOlGllbec/HVK+EMxcafE/22kbqFKZDeEchUZXJhegYvhOBZrfNKhMIC6wOj08MZHJ5km4R0fXfZq+oL2EOKiYPQvOG7IjgL+G0GLN20yx65OsjdQiVI6JCHKaeUk12GWEVlVr1ISO4ZDlbmaK2/EEDST0ss2AU+uFn5HL4yw3i1TRntbvnQvlvrCI2BIHaCae5gdKTlLH4v4x88yttXD6yJcPrCfYe0Mrvjn7K/XT8to306P3umwjyvYQUnjXcflZutcQQiMJBAKrcy2NCeCEkfalnzq7iRsi1Y05X6C02t4oR4e+tOOn186Pb0LPqOxOoGwGZ8rNk76YHUTINP6i+sTPeffAzpD+sSw333Kt4pPaCUGmFYgz8nSL+HckpOVlLrVimd294dkpl2jOrzUs8m/Wj+zroKZi6yHLFVkuAPOB1swFpFwSDIVhBmbOrKoN8AiwZAikFMi7jit6ZCuusYzpy74GiCuQ6JFmQknIVBd4ZIwf4iYwiX28G0XXLS6T0J+XHbmsMfEYTaTcqfVu86g+ntCz+jN2iWSlOJJOdQlshh4xi6289ZmhiwXY9XRtptvQs3XZ838+Ux9ioFgiQQ8rhWRlBMCbU6XOcDKofonGGanuUDrhVpOHH1hG3Hbb/ptYUZDgLWLcESabCtphwXJK+oV6XcTXU6AUaDz2G4ecLKY/hVwhijVJ707J4zkhQe7PDINuIFsHI96aMFb+UaIUWMDmcJCQ5QAiyP2tRja4dFPSJx7wAkG35UU7W36whMrFoWh3WTu9HuPMYwKElXpuu9f51f44Pe+D6shoAAbg2298O0OowiBJT0CWAepS3HCEG+dDASLedpbMZd/Tp+P8+pFnTW6hU7mO7LJ2Rl1qgGjN3YZhAqkd9r32JfQmDhClWa47K9gv2Vq/ufxhev2PZeHiLqgIxqUQzGNoIVfdWfPh3oUGPNwK/zqvKLHWypXGkr0799oW+yhfWn2/xBNYzL7H2O4/IRMawgjgj7sIXbX3YgFyBiubcDyL3Gwg6O4LAiAagmLXQeLJCnA/5epE3fdmb0KgGcgQ5SINq0zSlcOZUDBxnW294eOfHLaABWidONimV5P0mZ4iav6TA7Pux2eY+cc9v8pPilluU0XftM979ilpjMqtpDQRVsFTnlWPZ98niaJvznJAd2d82oaW6D0Bqv6mrexmsTaSuViWXjsJq2qN2jLP0zs85GYalrP/t5+4O8wh/MLDjyU2Vs/NBB7RWfLYlyWQJu6ggPW3coSwwfgpgedG16Ht/p03ElaivkCXcyeJJjs3D5IhRFxZwK8/JNYe8gRtBTJu7iStJ9W61NSnDxu0rvWv3UbKJr5/iSlljL71g6fl6/UrBF7SL9sGFbcZ0vyBVL7OMhxRv+G69JXn2vtnl6kLMLMkfOsfzY8AtrZQKz0HUUrsH8uWw17KMQGJu+4pwa2Nkff/nhAhRLmC1DoljlFSgA7iOCqdYkArEj2empYwDxv1zommlXFG+JY626YpxDW+DSWkUBjDjDDBbF91rPZJqgs89i9w4+nAe6a+5hgmaRF4CzNpaSzVcC3En+BeMEWFtFwd2rXihK97YKNQkNjZ/5y4JJT72hpDbTeRzHPwagQ7pBGB8k3eWY4Vxmd+NQRoiASp1xubM+ogUHs6mx8DwRElroHU04pP46H+pVby2vLaP3eN5WMV3pU2srM4vUiBTjHNukhCjurMxudOYSqEL8DmNdaMVUXfgLFQj+mZp/EG9kwKi9CUKLav+0AdaNjQoQZmLmA9tjqpqb5xOJTf+N9bTtP7EyyJKZ+vlMbAHiJJneYfvtLMp6bGXv1vFV41c6LikBNXomHeVl8eB5dtSd/+9GwDDI8povybaZPtQY/btlHEAGkqT6UnzHFrSNujDHQDj/89O1BkYD8Pkp3OFHv/1w3cfp1Rf3ZyXZNF43XtUY5g9iGv3xOzHX0nLJ2qAgYDpO9F4Q6QOVqwiXxyLEj5J72Crom/1p061lVSas2/RamO9IpCNA0ExiJcufMjdHN1DyRzLfxd5LB519f7r6CJKOF68bNOZj7hQRuQeTKaUkrCEo8Nvm91IBhWbeAPphVNUeOKDYMEC2PenY6u6+TVGS9SFm7/l5tRCxqX2eHWtwlrjmfVz82qqKvEjfQICsCl6/kJ5U9LrTTTI2nyPiRCqHF559ILdL7lLlUFa3rVmj8IZDiXtTJkktzNK4HWcTj/NQlY7ibRMgWTEpVgXD35xHDgM/BaGk4affQM6RvoeNumimPoTiP28gcDoOMGOWJ1igEHYua0tneGK47dmXu2Rfxelm1DQugiBdjv4/vG7tLzxmLQa0qyfQVvyo8+8Whn9DbIxYnPyTCoeurLcKstEJVBLo7YCde43c3d2fOSQkzSEH1V9X6odIQ5LP4OOiYUneqAgZIKSDCXpiyvdns7uwrp3qRPPS29nuvQkszGTaXBMHmA4tA7Y6ORC5RzOsUt3JueoAqF2bpbL7q31zt0qXbg/9rdfmKySpXMPto6pNSbcrPqBnYdZcCRHril/8gnseOmdBRBWw/X3m2FS8w90ox2YRim2l9eL64fqNgJu/gM/TUuwBY/LD7ok0smqjX/xUCyJ5eDBs5pksvYTuWO5eVBDkQxz3s3uOwrahbtTc3g+Ei/BGwUIy2YXAhJ19W72p2MX/GY5pK7wZGRX+yfAwNhug9hOIiAamZRrximYHlVXkz+Z4S/9Ww9QLIRwT3oppo0nk+bn94QSV48A+DGDiogiVjW2V02Hn5VDTd+wVfYBJvXXGvaBWacSzmh1TCB+Uoid9MrMklXAWvIXs+yk5WqYAVHjBQkmXv7QspxOLHcotFcJKbBo4cHVeMr0gZPfcW5MmCr9EEaCIYzHaXlfOl8D568/GvPxpf5/laPohwOkqlKFoEZ6KYB5i3FZ3J4zVIDgAc8+j8fZokMqYiXw1/Ga3vyNu6dPF3Ch+cUOM7og64g1Mc9QVPCVh4ITIprlZqSYJB6QgHAIBWZKIiUTP3Y+a8V7QvVNP8kbyR8sBDWFxrLsgCeCOadNs2N8xgXvV1G/f50zshi9rYZOs3fEj/qxYhu64s3hvOR/+qlZbe9RHQgbjwLZCLMPDKsZaXObhxxoG5ShfD++rBnI2FRU2tKNfbqq2HkpH+hw2B0LwAO2jlzXKl2xlPlr6SXWfS3F/mWnYEQ8/4hihMJNXnKoiZi9ZVZiOfpifL/iP22t0SN46ExEV6DZgfW9fO7BFs5iMJe/TKNcbP6R2cI7WyJeVnjRfrPO1VNHiuZoRSi0GZGNy8iKyiSX3478v8PYwTvTfk4YwXqc2YTf/HhWrEj11PChhgMoHUMh497iLuEJfbGxhAZzVShLe5xb+uPcTAGnC9CsJF2k8Lu0rzApylQtkZQvIYsUlKou8718AXGt8ue3fjOSBRvepEddTFlUAIG0RJNDNlHUlQVodFhNl5lf5LvEAYuxg+J46jYGx+JOpt+F6cNtNXhzIu19ZS/u7yCHCbIOXkMdma+/RpVHaYXcfTqjcl89w01G/0NBTc3qw6ebPsnXB2UU6JUrVVuj7qUA8F4oLp8p7IZ4VkLfeABlL3OFvCXVhpUJHwqLApSn1Gd3T1aAm3Gh5Bx3MR8KkoJrJTpm0j5Rslsp94kF6WaJro8eOhvasQS6yWUTcBca2fpAzPMk0oU2sJ9grcFKcdiToM9jjOeXEc2laNwje35cUF6WTS/59k7Oo3IoIATZqre9BfrzbMxN5DvZQpNHfcxsvWzHl1RIj3V3qfaSiQhNxWNF9kC5ijJLk/2CUY3UZ/6UCanVzF5QPSSRD2fi+GUbe47T29FPkaNkm0fsyrlXSr40hEYhecnLESovwu9Poe7PdkDuk0uGR01DlKOlp591HDu7K1Le+r+qUyfYv/4n3O5u7RJM/rN7tu3vMkzgYvA1wKIcpSHv32bSo5rNpjCrzaY+B50fsNbUTbsQ11ha+VzcSBRmbv9WGFysw9bTJXuhlFQGVTuol/WeNF8HvZltjP6g7QN4m07yJNrKAK4+RzBcsrjGrlDXHNOyrAvBOl0EQfuDdWGx+istb+0F6I0wBUKixYM+5ugfIAU2z9bsyZjZKvw4W+wkaAWocuwZHYQjrzL3cy968H8AoDlFRyhSNkAiw2rzmlmkowAXHHyLbHgd+Icou+5sZtyizqTxAQkGM4xOYp4PaM2x9L+KsYpmBQZO730pz/67LVm21+AWTXtxsD/USm0ajSPFno9hOgv13R1adIrjQRVJEIgYli3R8g3J1s93JhWphpQUGvpm3sNTv8Eqm+GZNdeoQMreiUOXKRqxeslEV9i5/6r2l5zWoqoY9QLbFWrj/KSPneyCjyvYiRRlaZMY45JKNXXB1eg3pd2XpmSAsiAtqCdWjJ8+KyYicNS6JfCdmfFiX3WzcEANlk7pQ3EuGtA8y15vkh5Y/iSmxbzIpt/uexpH/hswV5mI50NwkIQkU5Z2praMPpULJmC9syyAyoAFupZ9CkRwWHPS2fiKzwrld2TbTmCG7VjT3xg1Pdv6GmzIR9TvkJXYNDFs2CI7gHOisRGgQUXeycwZo6KAA+6znY70xKgu8zb717QafUxKAQ5dXydl1EsBrjMLeGbjjfrEIB9tFzXwJgnBX7UpDrom2IsCgLfDKI1tgPxTx+tdospbWFKLQeaCZjLqG2dnPNefhvKuLWkUxT5ogA6Zg87AjPdOfuK2kqrd9ySnRAwzsZKB87SeuaGwIP2FeX4/v+t/41LCYMs21lYpC2Edqr1R5zjp/K7wHoIVkOX/lme5leHw6Qgwnj+WfvCVpDxISKfvSaczvKg/IImDRT4DNfvdPzcHItI272xZYBhAPFY1pFwDD5GZfTsFtgcCE3dznjDZ3SpRNENk3NIzfLxpIV6faUaky5wS0jaAOgjHljtkY9IobhNumWeYrR3iwYYRbTmbuZS7+6/QeHsdm+pmnOhu/Cu3CwS/DiZdRglgT/dZx5DiewMZ7qMx0HoT80ueavY5xnM5rjvembqYIyywnXRv/jrl5aS0vYjNyj0K294pKIpU/WJfDlwSUhFIdPFFokuREcw7dJvHN1xaZ+UCQty0Ng7jH8CFLWeEf4Vf5PU806lyara2hUzJkjm5NHNGHQBW42n2OrIIurPhSug29e3JAEXqQh4eRshsa0OqZu004yzmvAsVRyaYCubaWPc90R78AscKwGpSTTUOnmJtbLXxKidjCD+a+LjHEb8ffE6S58oYUfTo5JkGQOHXpD4T8mCiaq9bDc3Hhf+MkWoasypvWNzax0kcH3bRbv9p1Qc+pg526SdWKGVIQ1xqFeUUa6H5OvyGyK4W0nqjwmSZRl/E8wBHjK+xtrTcg6uHfs/Qw5gs/7YAvZqdA0UjzDzUGY+mzsH3P7eJoVfcUNNIMnQYC+uxSXN7PeGA6o4t1qGHXoqSf1aC12zShAGa4JZ3FqHgbLR72z1F/hdvh+8bHdnIoOFSg0IuOTMUkSQ3qprvUBqKCK1A/tFZa0S4mii6locJFHt9dAUz48HZiEClo1Z8WO9/Bm0ImoHAEIwIKTd6flfO0IqCSSPF4zm1PZhgiSdxLWVgT6qO0qsBEYIBn8jNA54V7NWnLAxDY3QghFHZ01kxYaVpA2BY3RxTdiRHHq/F0cbACkNMmY6q+GPU/wRzUSYI5JpkBqvj3bhC+8VhaXlyxdpqYlvpV6JxZEDc+mUY7dF7w8QDDB6/jm3ColYCnagkVEUCoZQs6Ceh1Rp0SBp63h3AVApgStN6K+uxj3R4BUqoZ6fIwFrS8L9uxSxZFAmofpJIXahbYIMaW/Wwuj0Pb4tQLvA4Ksoq/+LLdwQUzUmx93qdInPzU4FZ+a3zXuRXZ05vI4GaEgL3nvvmJRKaEs46sqGmbnW7btZuWdAh8ikgX6DaNRs/v2E3KkK3RnyeIvJL+HCRWM9NMbyWRkAh4LdNqDi1FKqa95xN1CXRBl2Bmv5PkPAP+/WcA4KyP9hthXMgA5CA+QW8gFiirHQPOvOAbXFXPsUk1xF4JguyEbI5Au5zcusS6oqEpic06IzlLCROV6K//CBWwnxVeBbvphsB5rYbGkh6L5TCmZ6nd8sJBBqd/ZaH+irHux3+/JUBmt90fUU/K73NK06tBTEl/5cffPSo29eJQ39Ba0E0OPsxzYfBhgld2G7+5ODUhmXXyNF/9O/W2XHA2oXnxtwSy1njr3jB8wKcIp5D01AijOBh07BKmvxPrgJ/BVOVCUzznOqLYFvY3ncogDFakYhPINW28zc3xEkMG30Vk+O9Hfe4NbVHjihOaT6HyIr03WJuSwRrTqa/v6sn4x3/qYTtxCHCpzxXB7lXu0yh48Hj1DdwfXFjXlgcyKZfwV6rv0MqAj5HDv5FmhIbjOKsT+mY4loMrBPls9c7sNlNkphobEe+9ZbTFj8xW24MYceMFfxdOFQlcSG0rJzhnFsfI3S5BzcZgTKyznhvEQqwTLI2eTfeJlILW+2lydRHNPeMjo+JUMJqRyQ8gcwfIR9sZrNo5OrfZZmWaUDmd8v3rU9bhdmZ9Ub6AOx5NmMyTkSbDSDqoJzlk2dBNRSs9i+1TBEJfBXgyTn/RCUzmd9O6PvkkO78R6HfV0dr89F00gu1y5R/VllUpm88INbQdLhHRqAkHBtm0R1tFfrwpG0lVFkmks2CwJ99b3jbbzyu3DNUwsifqwWjZ0Bw/9swcmmForoQuV1l8v8OvugllishHEpQNLyIsQOGQVNs4gzd5OwH8rceqvPf5yzm0sY6dPbDekRlWTwxPkCWImhgZ9da2zZ+t50ieeMTx9G3YVa13oe/R8VsUsp6LCf77wNd7mwlxQExcgiRb6f7y6Qd6G7G+Z/IXxTXmDzqwtYKrw7y6s1rI1qVZf64nIksi/PlZxqVPoDAdkOnGaEQGZgBaRiXafQ8WFz6jMjTcq+A98TSDYPTZgpHSInzbP6llUJAmoQIsD8WGijcrqomKFvxuw8XRy5d7FfMfbdP6GZgOGnGRub5VyKgnn8ZPgcf9UOsYVJmxUa2yswhnNSAlWR0t3Eed7QW7Ucytb4T1G8kbUSwPqdXWstZQEjkgJj3/+beAbXzOxD4Mh3GkU6VlO0zMPHqQMkVSFrFPD6ghgLP/bKTDIhCq2p0UBx674Qi6fwhlYzAEiIaT5H/4JAYVZ4Nt7v8n6SA9D7SKrysupkrSHxjXu/+GIfRUxVwvHnzrXu0MUxpds2YXj0+49ByxvheOv+hHtACuILt0KC2o/7yRPRm/opBkoHnO0xcedPRW+hOOPVCihcVUKxu5rCcmEKoheJGkAufH9uypnB+F9mki7+Nx4xb5xeUbt5o9LqJpL9JLgEY7b+3oVSDEp+wRrWX8nrBojcxbfcINS7+28gk+rEapChOhFd3ZtngzlqMx16RrSupiS4gG9rJO+IgUJT0WHGmUwgPE0OTcyHpI9ZuN1AVILf8PhT8SIUx1s88lkrqm5tv5ilfqh3GswelvIpL1HH6559/tTowLvSDKGikSB3tx6sgwjsikV87eg3r1GSgMaM9nzjxpnTSKoepYZJftQAnQqJyF+PwUpCQZXhdZyt75jkobmCKluDxjLiqb8Imfk77bWB8AAs3Fu/SxUlk3McvJLHbSuuWXoJvGNgCZYOrL19eu1i0QQCCcPCqhzlxgWUG/0j1pJITrGnv+23HPxq1dTqj2eR84u9qxrxj3ssMY5e3XLTX07AFKhaS0MoCRsucSPxxuw7NFBW59dJuOiuDzBon7jnYBfS1hqYxgwW0KQ591M1a8effo4oNaxoF28X3d6myUVGaQsDW9eyppJeuqFqhEOVa1EVwdXpwxSeYVr3Kgfg7NjuMV6kqpZMJJT23ax9SSQA95ro9+OXfggjNArfYyPsWzITwpznKYm44O+LDo53zKssgx7E5rBH7mRniMxo2O6tb3QvZZZ8Oxr49BPZU9gQJvmVmiTM7BipHqdHVWcrS4TVSDubN1Ftl3g4QFOG9fpfQL0H257MnhfIhAvEcYPRIjlt+vhZJKT6ugrSKri1KaNpKfPgEAEmcwcvK7hSYG2HEcwtk3K12G11tCQQrejxMAvQHzMpgkQvxghdbiBH0BuZOODrLfg3VIK0GgI42ZnlIBrxOGZkvS4coWkn8sMR+lojrv6EfYn9CqBNaKt2/Bl6fs7hemSYE0DmCarYpWMgcSoFLrEsKCh6fHe5YxLhjbnXh/mmyFQrgezRsWqrl6xKYICQhJ7oEbXC7aZMwlTnVmzkDsBytroz9erkxZMerLf7PhHfEfQOWlzMm9We/gZ7j5Uk2/qrr9Y5BlTLYjDIj+083NcbKy1PIwitLZpA28Haz0qOyoZR7Rjy+QEAywKHJ8NUDraeAmOzIEJT+dPRrU/47P2Kd7BAHg/XmX/+Nv+wThdfyKLixs4ApPSYy+yvmK8Sopg2npLG8hhlEfc/paVopOOxEY9ki+8QNX/dqSapBTBjajYcr5wJs2DGJV9k1wbzvyco8U3MfpsDjkKjYCe+JPZ4y8TNB8PxNbYVc6gq2ytFr6ryxPXZVP+MFaIT/mfTdIekYQeQyBGqyfCvtVvWuWoFJoUay3d6/+s7BoblJtpFxfdNEcP+lZM9MDKIAgaO9Ue50kC5Be6JYGqC15qp0Z4vs/KMN9+PhkhCAEa1toxqQsDm6v3YRjaNUOh6ZFZyp2tWEkH/fUEW9gtol0bkn75sMIbv6X8lXxlYuaMpsEPH2jWjFzXPx17BI9zIe3Z+TX0Swq7n1qI2u0QCSse4BkJI33aGJnsickkYbSpKSt+sW83huLIhMBqfOVzCRPfl9+KOZz+eQ19lP5o14rUQXiHni/X/vitMj+5Qq9fSFEt81RnKkE2x1NHWCJpwpBoSS09JK3SHNvbmKwJqXkKUr3ffG3Ws8hia+pvB+H4EHKkwToraNvCxPnAgXHNxnoKrPh9J9r0qDV40DSFnkWQErBm5dHRQUvAfwC0iGJt89/NzCGAOj0KohRIqP5hjevPUGBTKPvOq06MJXd0OP2fDPnUjKlC/rVYvgytnN5CItrSkaPYkUMwdqlK8njczS1knJ1mEuA9hbmeXKmKZBNJYPS6kwUO69utd2gcAtSfUH1JR6Cu65n5B/3A2XNxTHQgL6eOfV95y5+cgOfdZKFbVgQ2uIscEjpzPSDUltk7w5+4PlVsnAJ+6A7u6yKKDhT24Tyr1dlwOwYpMOt8ujGbN8yo/C+Z70oVHn5Q3wgygApugGy26ktAgiNtqV+I3ct+Ak+bojkUFrbl045hnMOAafcv5JWinhmr84yt0FXW1Fg8cK+PY62a6d2uR9LpJMCjmC2fZMbGHo26hWeStl1QfexN4xR4loR8gDSmdTWdJCGPm5oSsSzMQOwKD4GWGKV7RU28BurV10JRQW67v9CZq67Pf2B8ajk79KKNCR7tDZHzMILMm5wwFuPCK5vIq0VVf9ZgWSDV0p+DmLNmD0gOtAniT0KPgKj6mkZwGX4V/59LC0oQLoDAZtlZ2oHEwIr6vEHPanygPuKyk9KrNHa3cq0fw6cKMEpbALeJJKLsYyZPQ8jqcRFx6iwSVxkCBg7hf4/HU3pe6XNjfjcO2lCxrxdTAW0SSmdAyQvPAfZt8hOAXeA2lv2PE5LaHdxuD4P26Tjps0NovRyx2JWe3Olh6u9ZKQuHzfC4XvJ3MFxyv34/cVcZC2Vq4Nmq3NHI3C2yynOurPfFduLe/9tAPDp2SiyhwUDpu0Ed/8PBfZVCr27iDUde5lDUWFxi7YsIAqrwZoiqQo+yKI0A2KYkwON77JNIUVKVDuZ4gFoBJ8K3jifdKo6ZDGyuYcDr4eNg4ZohFD42LTJafQJ8F2wt+1WK/3rZ6JVCRX7aMR78BWzgIsS6324DylLhR+UM1gvBI5VyQb4kdP7QaocN9GEBb6rh8lYt084EFjKWkH4AP0t+84ZUNROu/7WdWhVquntdqdJtlYT+4lCrgGTZXURYdX1NBMW1XygpYizZlaoNdZF1tCgZDUaj7wz1kRfy/mH0E0gkQb0C6nSK/Di/SeCEqeMTBl9euEPYiVtUP6okqdC27frnmqI5cMYiZkhIkKLP3iVyZGEyF+8GRcqP+eLo547SvYXQVwE2BFiLSmB0akVqB7RWhGrc4A5j5R+T2BtU1twn2autHn9eEC18EwT2fy+lTfkVQY8tqM1xhHQQ25BE1wTSkoZ2oq/3eEeRppDcnChqcER4/XGYDML/unlLHEE5qKmJCxRWnTOS7CzJNWcmbynkBTQQJmhL/fFoD8BmYgTWemJJI9L96AFC7Ydg3WR6+3wjMvCZJSnHIj11g3SlUuX16nvHgZ36H8L4YVSMOLlgnwjRTo741Hn5vqGdGDnv7VgcPNQKE4ECulXePP7b3iGJCpwzBbb6CaTeLXqQoTbtavDglew14gArTt+PQLrspa+TIeP4sHk5TPzMLR6SgyzePiM3VAKaOgQAV7jidT7wzxdrTqR5sZVTvqBKfHFAlCpLgLlhGRt6ghZEcfDBM1TmJwRM02+4O80lk512PPbIusls9GwT1jc9H/vk4PmLPxUZFZhKA3mOij2bsSjC+IxY0T2L/YjNN3cWEglUrti5GfggqmqpirPbXf687ODyekDNVNU+6kaJaNLO/8vWSVahYdAgQS1/wtjJiHpQNLx1k4UoUnR4u8S20X6v8pn0vbQUtORJD3swUHOp3g7m7l5OfmRxdJ0sQnRTn3RRCBDbjlHTs352M6sYry4N2XNa7KcAWZU/DRSqZExT9TzAEv5xPmbRVY7aPMwMlyhQLy02rlKWNZiaiK93lbMw+YxsygyaMptI6+HpqBRHUy/TYZFQPg1oXnAS1wXkpAiMvPJpNPGENcMc9xDgHJPl3TBL1TLYTsS3pzLW7+UL4dGDYQgUVfjSn+iCQX2stPQoScBe4rpNRh5GwCBtBuk1yqn1Lq4wPEmaTE2Z+6rcvVm6zoxZl3uBGofOvGXpAdleJxSFlCq9muYSX3gYHE0J82o5WzcI3jvh8CGYHrSIhkVISbE9tXwpEAJ5yy7CkWCi3sv6tbPo2LHhPKEHGigm6QEYvPTADWwf7EdaW4PTu/Y8tH8IwUQsMOe+HsdPcV3+5ZtkmOtVRHAXIKHNGsWM+aeMqnAkUhv38Af5bB+rgqgsT06o6Rcc4tkkW21bv25VO9c/sf/viIOxPuogA2HUBAoGJH+hN6qqWYMBp/WBVjITSSS9E7ygx3eKgYYGQ+nu43x0G+Ixlqn0cZcGt/Dz2Ig6k/BI0ALVEdA3zs9cHgefjIKQIFexxDLz0Emml74ZeR24idzLjde1OZNVQ48fvzZXJUTCQruMMoZNdz3qcAYySSjALICYvuLCuJpF8BnmldyJ14+K29Eoe26DoDPBZVsfhpnKIvSOWob9fmwc/Q12LScixEmhkIuuESCeYTGyhgtfRT6/u92M5oTfVc18bpF5Iq8Jn0WfVjYm9JbzXCAD7mTIfEaiujKULN/F9KnVk39eTKGjf9a6C2QlvYVdCitSafimpByEPyy6uMMt+NWSb2tDYJNeZzotm0x3/615j45ShK2djWrkNdEbIVImNKQW+GHwin17SCx9SzaUk78Bk1kkCEAjTGbHqod3A13ikuGf98IZwF+UD3iHmIC+wQM79qYQtsfM8xTXv+6QUIr7Dy1VXwlXWkebu2qBJmpcHiPP51AirNFD3vz3SCnLaEyFxa77ToLMVyuDlGk1Tvbi1BqJrNvYpZJNFo5UlWlXuV5cymBv8bO8eqEs2A19d8JWFmpNlomF0pNxO73CMgTDH1rQJo3YRHEssAIzh2w+q16IWufsFl9QDrbiSYh1rpEIgBg8I4kUncg1YvRkmH6vNge0/dAZuuBNMAGHNZsoLY6s++UqLf4Of8Clr0xZlQLPNVo0NRFczfHKJbFLgaokoWl8/DiVRA0lrpbWaNwL2iVwi4oPjuLZiHJIqwzrtjv/LpV9M1uzWMZUpJfs3P83OaKOfppGnf4OlkllcmZqsRlAn00+B2XZ4VM6R0o3rGmE3u9Buf8QhnewRlm0gHAqUHT07XZ8g/CmxApIxmPhA3GzBwD8ZSAPNjjpMspNYJU8z5TND2iZ0GbKUiKo8NsmaNdLTE9JDtPe8RB3AQ56cvJD0uzqq3VynK6a9EAxFwFmIXNRhJu/WZfP8oftsDrMHmrs2IyHyjxK0iRtsFnbCwzuAbTkX9EqOuoj/Wc1Oe/Xy1juW4RorsYlpRwkWcTdzr1waohrZCxJq7LRI69m6q7DC5bqBhK7uvUzFry8XS5qJTX2XGVp5WNYUG59f9AKWJkM1TXIrfj1eLEGhPu1hEf2vqPySO/XNzOD3ghHq8WUwbGiX2VKaJ/FyyvWHpdhNhEIddmeuVk4jALLPZcnYa/b4pnqrEad5f7Y2pND/GwFuJlwDSZPhXwAjyqZT+tAD1rohdGWqdRnKkQbMb+VSDqQAr3o4NLtgGEtg3g6QtVO8s06l/6gpZUoGANl5GpU3ywfNUNbVBtPPlMUz0dYYKn7vh4uxKvKFzx2uZie1t4xEgIKwSnRKN0735AOFgVFdM60DtgWqR0DMs5q38gHPlyplxQGOVzgTS3/GTHNI5HX3RjSSHwh39s5LQZX0GW1ftEsr+N/uJ/oxVm4DDdHKRO3jzL5wVdS+6/9t/75VdRPCs+9gmzlP1KzojgiM7mk1caIYLANzkmsbIxba1sELyONs96jYAMKCS1P0gnqWxwkgdzTJ3a9vGj9j5aQZSQgGJhKujGEp8Mis7IYSPtkRX7kEnW8ozQLY10PcS9+SsahpUbqBv14k3v1foCa8FsHH/02v7JNYnSj1aRlRoV0hF4I20pEd52ANDZfOMU0OxnotryJzCKAn21TdLgpwjbO6EduKCW2r92ESGdQx13FnKEMBUUGuQQx0/xNZyupgZ6RVq10dQgyBdUsCplt+Qzk9otQQgYwfiG7+2CfJAxUXPKHug3jf0xCTY/sATbBIWXOCkDDgUbOE27GwtgVn6+zDdNdSQ2SfWSN0jdKvr8W/aBRRWMwTd1vLxWxOEfnSq6AEX3SiYcTBvsT4KU+oPvAhum3+IfZoO/x+njRLRmXq4f48JIm5D76sfKPohA0fl1K0p3PDcBsgud5YirwLabdefgmNKe+53AFAjuD5CAAFMdxUGn0RDzP76o8indeAMA5lHtNYpzXNLXYzWdwHy7lB2TPX3hdGk6d8z+DkQk9d7fcF4e494klojrqYo50/Sg511xb/fynOdsft+3sFPcv0D1XOKN5zmRpQz6y3zVUhFSL6SfoMUe3HYlpHXJdeKtoq0CR7uzpULy8d+fejGvK8y6VS94KbnHjPjxU/jo/Tkkg6bmeOFqXWO6Wqg8f82zdVnMv1wSg4dafpH/3wVUcGRKMm69clNVN5DIA2Ht+9Hpk/Mtj9KaT3RE2cNcxsOKMndl/MfakmhY4a2S0kbhvmVH7315eur72mQ6ty3TSbmfn1RrYyJ/0mNTuOt25EwqhVhWTaLaUZNLh2GXa8N7wgQ3QfbiRoyXY/+0vL69SYzGAFgxUBDYAtM4zt5O/l8iRF/yr0DInul5CsBDjH75yD+HvUMKfSpq8xkmptMUVqrTBW7wUXP/5c8vZFi7Y96PthFRJkCeeIvySqPjc8AD5f+AhDWS6VplM4be8sHbSA7m3YTVtfVSb7zkPTdhPsq0aCzwx2F5SuRLScd22pQ8y3TIenMGgK7EIGK+Paqjefg+ZdcHZp9OAqkXS4coE/Q1unXCKwh26E2xIaY8QwdWXdNqy0rBdY94M7HhLSz9ODvePtq3XfL/MNegVg99wA2U9ldvqSX9JK9L0Op1NEEOIvxH+vANNc9LUTaLnNR9fHdPwiEQXDPAE4bpW81+GWw0J+NrUwQ1t5pZBREMs1iit4sXzBFHp8tsY2sASbIPQnKd5+MTzDiAhZ2GNh1D+zSu5DbSYcWzWzL8BjPIXb1j6fHw/SaKYJlcVrKh5r0LxIFHb2AyNsfDC1m1kARZQ/830mMJWNJy+4CrwOpBbVbX/RAk1rC68vsjb8JNPtv+xp51BrJLRxYMa78/E2T/6TYnYjsk2+Q+m3x9cunQo1l5njznlyHpfmFI0GfyOy/PnNMJhjkZzMIk7s2AxK25Ll/pccxals96fUAmXn08KEXe5plYh9fJKsQ9FTSLRFHGSvQRR2C4WCNXoUMsRPlQr83OHprrSXOMnJou7fm9/u//QHuXKuDkMyOntSU8v37FdfPmg0Frt0lNVZ+1LabDV5mMMjUj8Ye0LHJhogOT5QYH6QymeykwAFBHoyNUEdxHEMz0p4lQCrowaTg/gUrvtLBLVxaFv1hxryLP4vU5h5KGq1+pD7NP6bR6Vg/iCTHgef58blnEtLwn0mwuIVDw1Yz0jDoS2fgxrT9q11eArD9sKk6GZWc1951tSV7m9TP5Wm7A4FgJmT4zWIYSBxeOJeCozF0Rlc6BHrKzPk1yxJvFLnQs8XZLBMKLfhBvYcNui9QdChG9E4B61jpUZW7jIB2d9krtbAPV/TU96gvpFTQdBtKh0CeFe+bbt/a12ksYz8W1uo8a0kJrFIDTTqEeA+kY7RsprzOpf7i9Zp+60+xbo7Uoxlh7HzgiEA2mQ20PCwhhVFMTU8jMiumJW/yuuK50uvDGsBMMt9aLQqvZuDExnw4rn4DRLG9IsuPGmbEFXJYyKm3jreOX7erXhc4c0HCIMfCZMZnJgKNFxFa3psiwF7JYXvQY3hl5MfAPwwgE8N0PMubzUdNX/Jy82pHHIzviVS6Ri8CgRnlBnWAr+H3eEHyGwh4XNeHfgEEZpCRRBHrZ+h+d5vahzVqUdAaW5epNEQkMuAOLK3LDc4MLVjdP4JaO8Mex96D6vvNju7Umh8WoKH9VqNsoljjsiCsnQzTuHw7Iv4aTsqFGtcC2VZBuWrg8d6DXi3QzhCbAvH+FQpG1FvogSYYkv2aIIO8AgTrPgU6eDNwoeo+3cj9OK1rD41ThtQ0+omTTA2C8iPcBEeva9R5EPZGx3gciZ75hWqJpS2TFEgei6RhgBvaZUFh5OMyrfmoJWJ2EGOPSh5RHM1JUSRKDGsFJ2Zq6fqq0RU88hwGSToryeGaQ37l4IxGSZ3IXKj0/p0qJ1N3KAyKgSbGbcS1g7AAR8uEGBC3kIVmC5uqVI25SR8V2xnflKMRwJmelrlvW9Dxyhv0g3g4TDOib6JD1B/gWRvlrdywd5MFUDvEnSvkdGOkfrsPiBWY8a/cDPlhxP7Vxd991Wwtzg/6uhyFx03PeaRgxQVtYv5sCcHT/xZmbLtzCx0kt0aUBYSaF44SALVyVcCt8P8XejPpmHcK7U/XJky5Z6zR18016Lu8cbp9yKxOhpwLwFJnzgdXwpUeBL6+SzJuFxkElWcuzNg4wBgiRBINW9dvh0TwXdqfYbarevk4yzOprzPVC279riQp5NAnfFK5c7GhAceTPMj4pc5SCunkwhXKXk5kHMwkfySt1jLYZUOLoSPQyVdEVP+qcMahhC5mp0b41Wn0f3xkONgyQIn5U3Y/C89mpmloEIFei1DpObyI3uGc/bdiyQX4t9O1CR1lxuRSNEqfgShrtJ+K8t2mZBorkDFPEGrNtjp1Bh5t1RY9pmp1l0Svvda2S6DZbW3nKtLEHAUzwUIt4ubgP4qgdiNaeglK7duLpSvN/PcbLZAb05ripDfhThKawS7HF6r2wGX8hOf8tzvgQRjbhzWLBfZKoSoRVU1J2t5kB4xzlStx6F7OFPFEB/ASQSE0JpLa9hW7dXcKmNGatc9lH14mXdNyzqNp8ookJl6tGBMXL8ZTVKvf3H/X056Wog95SXnEr8MAQZyViXN9/JRwdrr6BnUf+PhctthGLYzcB20nilDRy+jnzUuayYZQAHgmsupEy024WT4pXr4EHZnGeYd3QoABc+bOf8Bv4LFcmFyuMuPrJh9LYc3PmnELtW70RvTTHks7ntJAd9KFws/JbeZ/427LiJz9gSjo17KTo3gSpKHJdL5cfygQK10x7lBXG0P3rFt8Pln91rJsQzSSt5KluJGUnze215yUfbpdgvrK9mzYZus47nO+lhtuNObamLJ7ccJXvGp9VGjnbhA2B/AlwT0Rs3H50oVPWYu0UdjTIC4rbtkAP67iEh9MiKQkhvxtriq1Q/UJxdTPSGGjsbDtCrxJwRKaE+WNXcZRaSJqXFk4BWW6UpE7ogL0WRAC/fkw3X7ACxoCLfC4SSUmGjRxE4PLyqhbCsOunDA5uPIO9VfQ9hoBCwPaHrDIU+papbGeD0cWg01OLze1sLrLPENp6TeHKdrzM3AWgipm4yGNmLBeKc5jpcikiJaqf+ysZbIVAJLuGk1mfNjTywR2hZwjeqsVb3Vn8PnaHMWTKx37ZZmMRXzryPO1gF97kia+Hq/ka1Z6AvMEmkjhWnjeMAQ98Ur8mphxG1Iq/sAl9pVc74AxJEptYuv0SDDA5c/lF3MERyxaTB8VfCT8CoP5gpjg2bxwzSsxyup5SiMkJSR9S7Mz2dAvJPlL8ZabClFj5gpXq9erfuarDHp3QDwX1ZW3y09fgBYofnXK78qcYXjU7oHUf3peWS069jqnlO5sXG3rLP+JYmO5ZMD5DwYaHtbKidBuj1HYZxX7TqYgQ9ksSpdZHrl1gXl+D3wQspxncLOFeovM3JWX6sWXJ+N7dcvVFWkt3Yo13Kbp/RZYFSF0N4TIsGpJNwpNMzJmbeJ8H+i89vixEM5uyIUhWFJ2P2uZkmbP3/7LM5K0cMtQE+9SeplCtJQhlBJvb05uHvFlFd3KrdY28etw+69trYQIVwlNe08bpP1M5o2mcWVHTd4keokbwRgrXLakkJnyiJY6OTv2Rj48ZY2F0cUqS16KaJ6kRH79LAzLqyyx5nVlffrz5IRLoHNUj18LA2aAZIXrcV9naMqDA2SXjEzR5O9RN4imXhy3qbKnyEM2NoB5PZZrQ2xW+EDeu2x70056Uu4kpyu9xhq4WTpOZ4lf3UqbHGI4jusMuJv6uXmM11xVzkr34ovl3wKRWRtEMvOJT6XYY5iIWi527OcI0ABphyG1N8AYbsAC+8TQIHHoLOwTXAkC/boJmGZW4aAnkuBpJK+br6HbtbiRsJIRTpwUuf0MYk49aBF0R+O+hRuPUebJumRxAutb/eh2dSnGGyyXt459KZY59nC8uRhN0jdNFE6LerqB9HPnS0lMeu2u5nbnwAlBOU2bN5y3e1pyRUoznyBOKowCScdgPtKq2/sM1hWzWeYZpogRr82JChF6wpVkTVe+g+t/SE9eKwRLm4j/bakw6IqeUqafeGTz+VsniCG4Ez6WlrRBweHcYAQi4KtY4PxZZ+I5cBRoGiH6pqs8d8rSniBRTpPmV3pn2pU0GfbVObKWDC/SZ/vO2ekJRg9CoCxdVtKQhwmpbig1DUbVNAAtu3I3ORW75n4E5OefNqSC2hRgmEckaH/qqWef/QtxeEZmVA5FhbmPDfL53o520IzHOy3vZ6xqBsmKqpvpJTzw4tJv/R3iBYW7rGWb0WuzbLVkriLIP1G2RCBNiNCXYOTJdCZTBZkcfYJRTtqeCihdsFOidJKdP9DohuLisMpTQQIrexEemT0tpqY/OXK15d/Qf3D8Mm1GspDDLGSntFWg302Hy5xJvQBimps9gpiksLW68dIr+BqM9N7fYF3FrXxxnTMso85Y1ENAKZMwYCz3hT9dDxyIPyLKTb/zgbyf97XasbzqVPb0D5J52nmPyBhd4PlZmO7eaE13jcpcyNQqZWVjix26442CdrBAktwWZLumgBHNd/r7N2QWyiGo7Gb21dkVYF1NFt1kwHMr5czPn0kCvKjpFK4sOlx7x4a+Xc6wo/RiY37KdRlfxWVpxSw+xqkCwmV/k6Os1WJQ9w8wFtlifYizjwsXRp6VQr35HCIXkVvcadH/dxSzjIitPhOJi099/Zis4oz65NzaFXnbzd65lNXvr+mrJFjvKJpn6NllKfG+gR/gtWjn1b6weYMTq7kawAqIwY/Ye7RP/wSTvZN1g68JGkLvoVcj+GxG0cGseSlUePM81eoebHw078kY8KYDeRuaYn/OqFU/XoXH4aEx9JSBuJ5ctB4rKE3srhKQUkKtN5FeJ3QhVl6icszNThZje19fTR5Iz6j2INuI4J2xWkx3mElAjIVJoTpVrqbGCPRySZxK8v70e3GoQMWdldtfFfDCj4Wq7brw+hX/wJaQwd19S6cOTq43lx8Sl9LViJz+TsVZiw9hybON5kdCKltZEGmdwvIIT6aPOo4SqWirIfzWt5Gr/PL+uksKQAd9t0g5vX6ARgKEMj8Povz4VSalNksQmw05CFseBFDTA0W5T7QzHSH2oB3AlAZCZiwKtDB5lJBhf6RicDLM0e7U30pBZtMMI5ualCSzxzmdNysp9qMqEcaYjyA1RsMoUUmJnCOVEnDK30+vO8R8Q2ml4jzxLgeNZerFJS6ZO9cdupdoWOAM4GD2zKt4ex0Clpaj+tPT8iWZLbDjtpdLnoJLy3bPoSz9LyOYPAgTkmn5yNKErSMjcudkOIszScYzQyNrrVECYKOrsQCyLAmvtpVXHpoU/5Lm4/Pf/34fbAaCX0tQ2gRObY9w+0KjSMeI/puvC31lsNOjMic/Q8I4OYYumywxj9zX1BsWna+Nm+fgyKMlAwFFTvJvhgQ6amYv0VF0vlE2hcy7WlWmI2GlXl9UW2YniPML/AvhhagkXu7eKV0pxlj5IEHYAz+/DWH+2Owm31B+9ydz4jArvY3KIN3u9IZcGuXZfAGla2MYGXWN6rxML3qfpBnPJ1jFS7w40KuG3Y1JktPFZgKlsXiNEshFrPsV1Qwf8zogMmVix7hiLxAmieCtLtjWQRPm08eeccIGBeEolarFeb2gjdZgKPTU9tbWsW+pDoZiQoZBZbnlAEsX/B9Ts7/NiWbUbi5UnzFO8gNpwQrSIvpBZDwFd/VxR7i4iRJxwPpoIOt/wAt0t2I1zmIQs1wogMsDxTDEVOrrGswsxxrxJYtuSk9AfeEfKwnjiUjeRpGjgRrbWzVa3WaL9wH8k/s5TBk545srtW7m1rCkKokeD6EOCfklyzfbZfLaMvzyuBznI6F/4xU39vp84FUPRp3hBbO1RrIEelUyIi4RbQ1VRCgixnxFwbjNYKachlI8dtTvIZisdFcFLNadjD3qCM35jPqUXyAFgJ7DckkS6y2MT9GPiccrXUAUx17FUKM3bHVRUhSK/FI29+T2C+v83B/aOPVlILrwyS8oHVubmwGHe6qH4x4rSM6TO/5ae4kCXJgC70gb+4CDULmZU0BTCDrPp695MaaW6Iu7MgyI5Ee2FRDCFZCJhkdlFKbYuCH5xE24rT6oiCUWgKlT8iCN+prhDZVKcZkIqOeyWhNsC8sy2egsnEJraPCcwpHTN2ZdPs7U880MmXrIVJqB2576bW+nfpd9qgd+XphKPQ+gpA5o27d6Wqgn7D8TXH/Mso4mhyFYMowsV4EDrmRlUCAWUQpkBrKZKv3KUAkbSkIuW3UoMbUdVMBCoRkJPpBw56dg9IEEur7jSLA66tkH9Lyv/0XXr17AK+c8cBnxvMRVZSe9m4TOA31FFq9X6Q3hCS1+XupTrlW3+AH4TX/po1Tj8J87mQqV0hAGCNUMEtcS610onEPMO8Q8h5BFFApPflq7CfskbM8Y9pdfgnQC8S4a4ZnFTe5dJ4rGxj5ob8YFpT3ooo3iP9Zm4nwO7oIdaSeM0NNkU3UNGN7q5+MX/vJN7k0av/jmTc2gl7b9LY4lcTIrlGVrE1JJSHhf9dP6aloYJilRK45jsXyVl1GGmiYO6K5Cgk0SpKQapWLsOTck1irh8g1v1JYFNlGm4igwKxInTQ4w8QyDLBooba/kiXPFvlE0uNRbZIjlCXBub1ppUPYQ1fxZOxKuW+GHHYkXu0qRpQdUPjauBAjHvIFRUU3a2D6c1QGC6no5WdJNujHSCLYmL0NyjPQFliFSGnxH8ZvqHm3d6jTo0f5JIqTQf9vSHA5/uqZ+JdN5wv04Rpi+TeILlU+NdKemGFdOItYzTHafv3NHdal/C+E1k3FB5JY2apA9L2P7bvAteYzToLqWvD+gdEzR8iw+D4CaM+I3SD0wQTgm6je+DKZoi6+OUubpxB7GmaAF5y+Zrn+lPUJdNS5T1jcsRuI1bMUwnso8b2lsX/f4b6tQVCPuyJjIje8Zo20Gbzw+pw35+fSkIJEztCj2PS3rGc8X0ZqFV9if+cHIj+fSHUbbZrjqqc/EYEnIDER9Hw7lJ8bkcHiJogIzenpdGddSnWn9PRJ58OhRajlDW/fZXBHUKuGGKhb7n2arAHeyoFhvPRlrAyqtEEbAm6unHYOm0E39VYKV61Arug5ah+xtkj3Z4z0Kk3qwzKn4bSHM6AuOB2hksbRVVNsIC3fNQsYPmX0uRv+iVKv+FmwHLBkxR3LnxfAKaX/9rANmfhqBLmtVIKw1oQu4l5xc6T9w8sCH2xLjGxJIbOws6hT1jR2X1LC8vHhks0UV0ddaIdIkiOkqL+w0Sl86BVk4B3xg3gGk3U1/B/+FrF8QHpd0W0tknqITuJjPxa8kfeAZD935emkYwxE67YS8QhfSVVdN49dZsIuCF4ymnPClnvWVz3DXXkEr1PK6p8mep8dEnewS/Ss1dOte4VK0dUHF9QTMJnCTcx9JI+UbXb6zguSycZhC7xgROdgDGMZOqqaE+UX8dvb2x/POQhuqBBLbDbvAeCXKjn3wn0Z/xwm49ouGVc4qkT68jHQkEoCmfjO8HDQ+5Ag+90Y5hc/NKE2IfsHaKXA23dw/Mb/b0fCNLeQKcyzJJqnStt5B1JCzUXSxU9y2bDXKSjmqMTEepR2lgIw11PXBV0+ZWN3vrfo+y7qSFr8rEfZve5uD/6tfhZ56u0pLcBNseYn8+5zcmpdJC5IkvRMuRno2liRhvqLpmZd0WFniQ8l//0bLxGwSGlaIxHKqD/KY0tMDXx2QThOTXvxOOl1ty2fHVr5vWRSISlyARRQAOx3Xw5RsaY6/AjsoJ4PmHsqR0oOjk/ffeps0Nn8rqqryij4urPVBu5FsilR14FDd8OwU70Pa6yHcOrBnNhHqAoAz2YJMqWKfFFkR1jAhGaB94vAS2xltwjWJuZtaQp4pZhduXdSlt/rZYppBEeVSkajUaMxvQaOBaOJa0cKcxM37FsxGhVAMzaAWozjzzADEsWS9zUGiUaCPYnMA+By1jYtoCd5V0HrlCZCgOdqdL/KMD6HGNooeHTUkpk45bQ+Mybq8Nx1vaaoisgXU+sur3LJ2yHFxFOs/cX0xESI8V8cz9srTKR1ULmkFAbMs9RyiEgthCejF59C3qpDDQs3koefpK5osjhPGZpSAhiTtreBuSmfAa9HlHPyjvK42iYsBeJBQy6WsbzD2InK+zsvMAKx+QWngv9B/Q2E8pTUQ0UD7W87v1mXyHJb0i+OmMW6Rh+uzBGELqboP3k0BabDzytl0lhO3qh3+t7zNei1498UBFUtsvI6fmgOwGVwkL8I267FiBWc8nF6ZEe1FB6vrj7rFyC8SqY3GiZArQlcL9smnRYV1GmsjRKingvi3qzSWM/f5tCIcPGjmU0rnnR1Cfr8vp8C06Ov8KyS4P4JcyjSZKTo7NjC3EhtDcqUvo+gnO18AKf3AisTnhWA65JPYtt9tRIe9oyBnRiEMD+FDPz/CNVgPJ8bZR5vCZlLh1fveD4m7dCKb8v/kQo+iEf8fp5JGN0KyOeyxUFsNIKWJmwusxmQI3zMdhOeD5VlYVJM97Pbzxw3GGaCphm4K42dMli1aoAPrJFLAHSMTD6L0dyH04AH0gUTM4myrm2EXpQFn4kdCvrm+bMe2VurmvE9hgmUGrENSkH2ovjxnWBaN1FAJ/uy7iKQ8hHLZxRazWeCh6xEw8bVoqKGL6gy493IZ9h6fL668cmiL1GjdiBtx0LTd04zjfYoIW+a2kJ9fMxtNDePknFZYOTzB/pllDvmyCAveUZDrfnDPr8/59mm71KZJm+Vh4uVlWTNNj3JeDKq2GeWOSw8wT9uGUD+S7pyZj8hQSVqHCcEJFm/JvG+zZWwQ1AXzPBYOKbcOaxMf3zf02QtEgqEewzkLYXRd19E6WOHU8tBtXfRyMvSdBXAm/hGXvpwFBZ+CQAyd0DFEigw4UQwmH+6bUyOMfPqOQ6zUnt1pJyBTbkWq13qvQiceU2xB1i5nVlbItD3UC24U8FjFYcowgHomHV3XxfB43tMLpxdCqAe/39c9ro0zlme7+QsfqpgeBf/TEfKQ+EjA4nvZISQvLRy3C6htpWtUZjo8DYHkI8G4TPBe4jJWXVHdqpIlUliZhZQwI+d4KUQ+Lurc/0yx8l2O9YPGmY0hybw5xC+47WxtBX5yCE3bdkIQnIAuHo07b1VDMdHtTZmGVDCrWM1X4ACK3S+aX7QaMiwR4v5qNPMPAUWqGVi6XXSHW/YtHXSnQBldlMVv0RoG6Sdfcd/iuL/ITmXlhp3Ar0s5gI5RdrYjL3Ag0Tt8l+sBA1OhU7PP+3be4+RAAG7ZSxiUbWYS7XOgGeUBAPbnI6yMmlnYe/DOAXFltULUQPH0peHd8Cgs2YbkEN/JYCTVIrqmaKqvrzV5kVHUz91XRKTAPcLIysJqoP+f5YEUzw0u87+HLsst9W5V6q5U6lD/NqCUwTRS7B5RmxbIGroyN5klBZpy7h+MCaBugtT+m/rlcWxZDIm3b8rFS/MNJmeuAakWsnVS15GIJ1pfBUTYIkYZUjYNaRYAaHKztqBBsE7i/7KETPnx2rmvGZUIVXn3AzO5yP59Z7GnjA/rslcGP+8ODoZau8Qq/pY70WeiD2gDyuo01UCy+7bQmNYsMBf7Q+xXYJqmf9/JdIb2RZ1CfZLZKFUAbsA+EZsdaQ0bjeK6RZjWmQNSJ9Yw+BXdQU610pLf9HjOBzFkkk7i95SAAwOl/SLRHzgUnXP13c8STCdE4nTnOzi1WEsBHp/JMsLAph3RNSH5/Lm8ool+2ZRVB/b8hDEnBmHL85nTsXEKBKl5eQSprUSZ6PYwMUqHiL2p2otLesSytin4PXVFt+defmGvnEMj6AJXhvGlPdhRr2mXUrU0u+djfx0oIUhvFeWPKNOKa8/CVwLYG1lnSLXpcpvwg2Kc1fltkt6R7t9PUWktmK0Zg1k5fjGtxf1r+UsFepsC5dma9N1ZBbrI1C8i8BEWRo3Ne5DOHHu41NtHqwk/nW85kWSy2E0x9gMkg0XBoVcR7rj6cfUYHR95+bV0FS3A7ot/RO//Ll+cvo/dNSc/OBiZTAstVzXwl1vixhoHfTY0i6ni86WlJ4sK2H7fhqNauijM56+gO9TocJc4COQKYt2pL3T8iMAeLMiwjaBZAThOCW26gj+/9Fr4EdYl0sUv68/ChKJ8NecYTtM117LcELDO4FssmVJFPsDJjndyb8tytjCB9JDqs8ecyk0+5vzcW/Xe7PATnNzABRhr1FildBveQSApjpG/QaUJN8+6cl1e+FSFkzn5O5T/kWzWQ4dwYK96SK2pOeuZCGjGMBodPecxWoXS+Rgyi46JH2FRHfiXdFE7REfN8ryXywFRetzr5Vc/OSUjjOxOCCPeFPv4aZGwjT4Piiie5ua12xcS6ASngaXT9TERUMu6j782msa3n2TsBSkqnNj5/zWuJ8M6fIBrGxZW9BAZSD5RAlhGU6cIrFKvh8T0oRanmwIRWg77jI/TAVf195azsIuSB4fhSsHigIDe3x3GQTNmJdRyYC+Cm33ghcDjgEagTDEa6L9iVFkJRtMFpok0zTDU3Zj6UvdLJO+L2Ro+bCiP7GMpObmvfzmV52ED6Lf2VRN1OYtqNaQwIVFBNFe8yKHG0pdJE87o/0RA8mV3r2M5MSFJfWRhW/x94msbGsxof8eFbVLJXeAJbPmUdqFflAB6re+VW6RDjOHj0WxV7pk38+4+bxVbhdkApqYe2Z0hFe3OQBP/Va7LX44fclmqnU6AB+KknYLxc5X1FReRvgxZm1PJ9MCvh2YjYREEb1CyrTvGqJfwHBI29xQ+2wTJQo3hlCgMRuszBl8FBXKOXTl4BWOmuFJWSPw0RTLRcewXZm4Caxgf7qx7ezxdEHWjieSrfSBeEqNAqkQY/Yeg+HCx5FC5A74GNypZjb1WDniNVPd0m1RkNRTKDYXXgkA1bAzwrJQHAzqsHkiRuu6hEXwGka5fj7xQX8Z1VX8oC2DuhLcngiUKgE2pkVMQx8OGOrzC5M55veMP1Qz9pUKQ4QXq7e/mJo5V9hOSI0GFJiSB4ATrE+Ph2JYXmtgfwGRPt12jyaSULRFqdZWWVoH6XGA2oxeMdByuOkqceAoXw3cwZAtl+QOBEKrxSBAsId8Bjpm5S7cwbR3DMughQK3IPl/wgJZhMOvK2UiYMUzMs2j5ts1mGLPR7qmGOHOcMppje1dF+PujsTCTxN5QyVf5Qq6JZZQV7Hq92lL3qM3g2h/zfp4vrsIev5jFgVRu3ULk7pv8/JqRfkmJLcJXrA5sBqaUtKbrWSs0kQljS10GwSgbhWAkj5ze2cDbO/a41vVBP8wKEmGpCmiwPFNsu3/xa3Ee9jBsU7gQ5qhxPsTsXx8HmxNhMgT0Rrxj0F1oL/ca4ABqJJ4EjilNM4A+n+AkRrscAsCZQVkXhVBffaYsar+CBR4MkbYCi1nJebcTKBCtr4NgGM6b5U21XipsPbo74s5hamYlCXN4YuYJOEY3AvjLPWyuxDE6Aw+cHGJYPtR9m+9PrEVDbxc+AvYBFmFSeAjDKtp+VH+jms65uVB7KOmBnGgu4t6v1V3HHPgcTk2vn9ZlE73oG7FT9sjQti3vp9ubNX30TAmfj7pvY9Y6SWxBKf71nV/zrjITU9FSgABwgLV4jLXyfp7HXvpHRMR3Cp9Rpz0lLn8lAYRTEZKAd4nPi8FUM9MtQMFUkNPjFoNWCMhvkLmnp4Hzvb2DzPCBzu9MwLlQra7w7H/x2M5lCRat956SR2eY4eqv+pCd7hKIZQ3CJAVOCFhKfJRYrRfsVfI0LnkuB/ZbVaz2AtGA8f3gvER+JCVwDGItX/8o9GkE5IaFoBzMuSN4+++knDZ75yp2UoArjsLKi/8Zrq6HtCOt9CJqYXVY1xlnyUsljhZP1jm0VIEy1JymW5FoGmjQyoHpkjhcMxQ3GCm6Gj61wbzeSLIChYRjeIClesbe5VzWmAAovvZMk1/G6+2O+2zo43wMsRf14UCOxxN9lMKWrIyXwwVVdjIy/+Df8eK9pNhpf0bLBdtyYDf+XkdtX/Mksx0yweEdy2f7sj6rfdVhxNFFHJq1w1L3g097Ror04zEFdI3leSSxGq8wh4Nkb8iNVQmmXIyXcZokLt2fWWhOaVjvaQ2dCdczpFI8k8p/Md3YD+T7PyxrYEyRjZhBUdVR7l1dGwsFgkqDATAb2Ug0+J/wpPdf9yCVBp0ooWXbEqnI4DOeWD6nt/EDegEocqfQNVQVj5C+Fh3uZor3pkAuhztMXBrRDEQo8kIz3EH/fn1mS4DertYKVTUFbKuOtTtFUJ0IpbD93k7qMLLf9qjNHoTA6StMfNg7LmcEladjfkq+JgxDCbSl94GUPsIae2d+EE/jkxtKdq9H8SSlpx5kkdOT3DByjxU0/rpapnrxTXK0W9/HaS7nPzyJibhZnIfwAg7IZNWli0pZjnI2L3QBNJBXEUQAS43C/ngj9oYlPp7e0arETu3Q3bJvkq8tvV2I1b5/4+wWYinIG7bRN2BLt+US6YNg1tgxq1QR3w8oVGYjuXIZutkidC6vf6bFolAxoU4swz3TAnwOCZ1jj2HUEVF5di0PuAz7+oNvcwHIF/UQY6uRj2xheVvpL6JTjFezv923Y23GhT4V4i2UN23Dw3yk79WuXvcrB+VIyYNJFxF9QW4jM9EVEwp4Ao2p7cVlNjHH+qKdy1YKFwPW1PbE0FAyh1sxKTz0yacVeK1wGawcGfQC7S66Unc0tpLJrU5B7I8uFfPn8E/JDwMOiRayM1Z3X/yxZLbDSIjr8FwnmRcfZS6Tv+VxeH8M2d/0d15zk5tsEQWvVW3AgQK5NgbUOOykGvEbm6uDeumgu8SVaOgWdAQmdiQDdZ7EHHANsQ90zGU9cE58zjV/5MtgcxwXqINtfyo23lxrTY/lhzE8i+WhsLSDSrMMgl+dTAlPMF8ebfDlmeoAadfiygPgT8DBSqHf7DyFEe7VQ9Z5f9x9zM5W0NzTD6kfbcrjRd0RQq0SAu7oBiOImovz0E+E1R3JDqohr/GfUOCze7nGCW4k5JAE8/14/YpwiZRcdfDSP40GFAABISw+m6+eZTihrMirvRcCJveYq6Dsjgbnch1LI2BApxmuQSKLdjNM94MNlzJBpKffp3HYRrHscmN77E5d7x1aGkqvIdIIotdvyUCdZBx07qVQytzSu0ZVBbJY3XeTyH1Fuy6XEGQrTsP+tEgWwDkoziqtbImD/JJ96LTSyKpmlquBuvWD7TJdV4Y7d2crxk6vf0aX5yOBp1fyLCACL2xVhjX7cChXZkDLGSP7b65TCd2ku7QRMzsJ2NcaEpzycmE+jaEYun44oaiTrfT8uPzi5CiV9wEjgaM9K0LH+PT8QG59OSgWsj+T6Gr/08cXNuCubA6eT5AskNaLaMC3Q9yhTjXiLHB6Zaby/6QjifTkujFTkcZa2EjwAsLNcpOJv8FuclmxkI9rfYh1HHpLyZYTFGbmQdl9CRQhUk6nxNWV8FXvnbSKqkXY/Pf3A4//3XbJNe49Fk2MUJU9SEv1AychNcqxRwXJKxajUejWkuDadiJToV3M33nzdwePlp8AV3eXU+hDkiYj5SVtDGpXlOLDTxC6Hk6x4PmJTrovthrdJzdytJ7tbcpRqPN2Bff87XxLVzEs6UDCrWglaX6cX0x8gNfaHq63oM+MruTrs+BMqY+SeK2EOh4WboW4jjtqSfKHjcs9Jv1WSCOSbxRAgxT5R8ZNNio5y4i08xk2OMhldhtc4RjrpLbVtsOkXUvfzLksrEBczbt2D4+Z3N5lL2HAcy/GerfyyZGDGe2BlYwAKXEfCzriNeLrKgssNqlww1aECltzGL/Ww60ZxabprSa8zZbDr982ybufoCcaY3FT7w4Tpicowzo1feORCSFwkLHgyt9hK9j7RfzoR002nDfa9/VJPgl4ixFfEsUHWbyIVaJidSqLKUX6diZNdfO6qCB76lL5eq7Aqo9+pv+XTmpnV3+ZgMRY8M6gfkzua+sNq1OVXqVmwN9LgiQ43WqKR2fqe6h+UmtMFavHWT7BjdHA501xNgCo964fEurygOYb+zdBfilh4cLuZSXfG1aKpcJ53XCMv6/B/JZgO8Gtr8JyDnBy6aBhGTvmo3Y4DiYdX94t6zHqT1VgWtNTJdOuBzAzDcPFwxUfqS+YFDc2ktNs5Px+3kXFW+VsKvBvcx9r2Shbw8CuF2sCVEAfMeuU+719gAUd305f6mtxMnKXAg93qKxIR2VEz6NJAp/rSEROPbCDW1nZMABW2dNI708NW/YWH9A2kBjyeJIPXYi2/SeXdsVP88zf9g8Vix7pXYUf4wgiDQnZ+3o5/yhfjN9dirsUtot3me6sHkVyVNeZqcruZVos9g9mcwQ5PuH42umYuQ/BI+TSe819kYnWgCPcF5ILx7Q56tI2yrIgxKiqaK3abMOzg01oOe2AM8/Az4IbjOWup+mLxC6mz76HlUVTsCkT+oxV/i79r3VLXu7j38OXZEy4LRht08d5Pj9v7oRmX5PNHkPlrC5DbZYxu5pbZDNwSd+MR61eyd4zOgzVnWfX89P/tyaSIYole5HCv5Kv/HBwoK6oPauYg3nNYnzHWWL+939rr6g6mdYFFnY4yNlxbg/RUmQcB8uO6k9E86Pvv63suN/KGxqdpAEJEe6VPurQju0rnNPAF3uuA8v5ZmEwDOhRHgSqPlD1oUcLBKsPZoJySq4t+7efE65lTSh/E6TO5QHeeL3lgxcsYarteUSdpvSxykEtCccfRCWYTk66O/DcVly8C4XJVl1JV/hYmzGVLnVHa+Vo9f6UcX/f1/rSwRrcD1e2VgEHjMzx/WX9lWrzNY82ncTQPw3nYvF+Om9BHS+UcU+W4in2rIqy2vEg4x5RPb2EnA6oFjWbYopyOso5ITUP7SboyxsPghdb8GyNKtpd1LJrGt89pa09LfcSHGNt6AfJfi0RGXa4D0s14/VsZ0oCDxYLeFVaOJ1gPNV8vSxxiWXtShtIHvmGXclcktZCGM2jSzO39ewNtYpWDNuilZATKSNd8mTNDQyzVXl4rrzI2UqenocpZ9Q9G2pi8ZdvLbDjeXV6gd7eiwhYWxIQDMiHAgCWyaBJrKpjYNTqrav3ul8HvBuYS6QFbufoj9aq24l7c76L/3u5Xtr00md++dfj4H+3i4uB+5vYQztGN/9kXfbcBYc8g0FXX7sWT2iqa6kjZLt5K0GneSC3VueFsKu2tA+6KUlkvM5O8hDmKMzK6EBrVti9r04NEBUZLb+nApuSVT74IRcMPXg4V/GsZlJkpX5nC4EavSsVuYVL0bpL2L9qK6jOM+FlnaI3BWjBsIwQQtvoevbBxrpVnwDJRZfXMtgVQLb7HY6Jpd8x4bMUz3slsRMoUy8W1r/HzGcT7eKrzDZ/+yLxtRVlMDIEPzhC+b1N8NIofZtNq7t1eugAP3ElA5i/LLLTqGL4MV7ERt7tL+64GGZX7JD6b+xGtl2Kf/NvcR6+cMHWnp9aWpiH98dlsxcV3STgsx8sDQNz3e+F5sOnN6jz+QI08KypEUVrhtqHIUwJ2g52O1XLye1lswAsg76TfJ0LocHpTMW14AAwWoa4AFN2DGABPM1zbUU4EEcUo30rmjO23ZxAjjhkHovNyCECaChwifTSwvex6DJLWLrYvajK1li0QzC7f6Wm7mQVvA0tryNRm4twqiol9LwZ8R+CMJBXHWP07Tj+2y4Y0FHNaa1gnpkGmTir8uGA3yNHuYoNRebOzXieFncSkonHwmpgP0MGHJFlUWtNoJrHtPiYiEBxpQ40OAzHNIgOW6o7s0Hf4YHJObbSj32widTPl/cggnp9QOCg76VTjENBl96hfW0GfVx7R5/w+6iDUjqtRAkO1UZ20wgaxpcb52U0NEUKp2j34JF5Zp3MjCXTD7hl57c0aNPCAWHfn5LD7I/qZmRQlb/mIdr59ToHpBKnc6ubJQUHNt/4i02OIhi3QjiezjNlKu2kcTqu83EsVuG+qUq/gggOzGq93UbUCNt4Z4528FMBrxbKeG2+p8hjg2jAp+VOouMe0OpktIDQaevibq/H2GnPa3exaxYmU+DrDNWqTwef9B0MNVNO8EmXl2eLO9U/Kt9qaIa9fANi6sv1ni6ooA8bLqO4ejj1PpTn2Mma6aQqTqARpwfEigGF/cCH2aJ1qYDgIWgJ4gEb8bHU4S7pYxIiI1m869qsv4MfAbLU8VunPFRt/2jwDC4kxTfU8SSjKw1+58SGyDEyJroxdqqJB/b+K1tjX7B1rx372tbeCEemeMK34/xwhMyYQhl/xPqBoTHviVVhkFlg7H0y6Y90dsJGNRP4UVRZiK4ireDe2LIe8+D+acH9zuMlywUYBD3NJS4xilCHgCh0o5JElqGz7s9WC3mpnT2FEuP/bQwysIzX5s6+knlapEBX4aVw9dRp6bB96zkqrC+ohl3YX55UGm8tourRRaopSPXfVBOFyB6iTfDwoqvq8n617kZ5Gb7LJ/+tz2G1oUXf+cd0VkACU52g3ndichjBJIh/VbEBnLWPnGoKYQCWPEvM7Ajq+WNJxCzSyvu/tnhPEZfVUYpnCnY4iGxf2WXwCHwEtdM/HS+bAozHEKaWP9uk7I3+TxIgsloLA+T6SSMgt15I0UEYnhWdrX52CLNQWYPxSMn4Mp3V5cG7iR6jJVRuws10Rl9EEWPXiSGjGvHo1BtSVzxLDa6r13lFX1+b3Rg0BRgWec+cQ7cHklyUZ69HEbEDgPtE2Ke8fXKooR3yLTold9kpHAZxpN4ZAWR9cIznqr/TTYbsgOO/nUuAYYFg3WGbC3FET06s615xZ858faJ/Ooct/iPKphSmiX9xTGq+4XIb13S++kwv+uv/5XxfE3TL4rOh82D4GbET4yMKl+mg1cbz9CBjQDpbbsuXHlOcuhZih1s5OGFAGMacUMbwUVY+PWhdKPB1iGfMHv6XH7bgsfs0RcUnhYqxSBHryJ6hdnKmCSSjzqDKgzuoD4D/MwxycQQHNKY2LDSWHXN8ndouO+XKOCGnVXdwTRPwsGqKnhSl1l4bk8u4VSpq8DGv4hWySFrP9TuFqu3Hb2u37hVptrY1urdE2boYJAk3XMlvFyjxlPcotQaTR59PuOKm08NQcdacuUkyoFTJXjNU+61QAL3cSwlbhiv87obYKu3NUBnTP6MlT1SkBDSaDfOEEhEJD4M1bheP8JrcohbtuhSDmJzcGRBhLowm/TTCrHae5jLk55G4woACWrRNbScuZSiIDsrmF5EeyDVxbrGfcLtkZIwqisTzjQT5BRqEf8ML9WtWEzeaaxjR5cTpoSCFx4pREV4YkN6V8jCslTx+4q6364ki1aEUPoBvODIQt3qfK4PFsu23I97mNRwUV7TXVrLHa+GJmSEVCGwaMCXlKKyCawVlCISNOWk04KZ4fwi5N8SALW+UGRpbuPBtL98J7ax+t3Kr+qAZbEbIEyc9mpJFXFNwbTlN4veJrvSonF6XIWm1tn8elYQNaqTrB136aQe26SOC5SoDM2WCWTT/Rmje8bh7TXlCEfA2ujGfUke2kr+nb/QMOKfY4hoKQHIec23TdrQrQPG4Z0iOEtRCShjptYbL4c2hpFPAptUkO7WiAIdpNp9KZHZiKhRavr17CTrBqsW2TlhbsSmWxVhibtMtKQ4fvXa1ypmlH5v9FIUsXSFzUXCo5O6vG0uuy04gQPO8J0cRHqlcXeKNl169Bg7DFCWGM+BesRm8QDNlea6exxU9lP3ec+tsqzgXByLYaiSJEUs02QlSGLXLm4dpX/NTV28Zwc7iNAFuKAJwch5V4wb6Qmu5XQqoKv47IakcktSQTERFIZKenqK4ahfP6kno+kYKzLzLtjgNu2uMSLAnX0bxRA5huuVHUKQDEjFcOBgmMX1Q4ak93ACEj3f9ekrFv9Lo10SuvM2blBL7cPIqza3iOlazZ/F9EwF9eE0I6IQql2YArXeQdzTcVKCNnrQfv5Ctc5rSqnd7QZIQ/c/pUfXAUAkHrkOPgou4G8wUa3HI33JQ1SWtJiwX0ejBcEczTiTrKxSzyX2wC8eAXH4scT9vQJ+B8NsIA1HiHyqpRnZwnKSoP5XI5lO/S28q/1ygW1RaPqYeme0gNnqyPOAtdGTtI1lZNCRWHNYQ/2zPPuEwwOp6b7WQyFKiOVl5BgW4Qzt15vJz5dLM9F8I9Zf2aKzyJ0coY6yWrOUhbe77E6M3g3g1NPec7UPglYDg256VTf5r0JbyWCPwSZXjcuTqAA3Vsg7KMv7a1m9v42Uim+/p5Gd+CnlRyjnw6v9ctUBcva1mSOJVI3m0Fvzzvybft6TZ3DMYOylFXZGX495gpvAhCazHRKUOC+kTPa+xDTh/kdu8wjXMEdmUoNFE3pUW1VwHnHpN8R+0z8NZA0y7sFssWxlJomOLFbgW7rWB1HxnP+Zhv3q4JxAq0cFHT/Tf8PO1fCMnrKuzbWvoxPM03JSoFIdbxCtNwzYalw04fvzWkj7f0fD5S9ObKQaPkNbGsSXqoaE1jrMlYkC3AplcWGbC5+k/2reIpoUPhj08pySBd+aicHwK/Q8bMAhRDSUo6+OMQnPJgYKp634dK7unE6Sf2zMe3aTQLzmtd7eFPBfMdL5BtmS+Zk6uh3hpN0HUQYX/057c/ZIs5FN3zlhdTf3Zpn07O8tbHEr0aY/RruIwxola4tn0yBMg55vKvnmwOJjITGzchgVL12QwyzmKW4x7kO0pIepQHSWd1OfutXSwfY0WWt8qmAMJcAJBUjnTEt7uIBVN4Sy9c1WdU9Y+4WucJVv6rDda0RjA1W6g/dpQztgUEGJEvPADDBr6La1bdug6Shah2JC7k+q+eOkfH2vYz12YsWO3fAkUcaZP5kDbo9HX9jXGZ1ZFyIRk+ZP3SIT/lMGr9AvymwEmr3h0RhirfCpSyC5s0gJ571Q/hncxO9cK+QJtmOa9uVoOe42m9w3+jX1KQSPcMxWUFOEOApfxWen/JhpkbVNs2sHHrouV1JSsh9BisuAfNkrf02euzjhI2Fe0QKdG0trtUkXhKOzWjl0ZM9tuo7AKaV6xw0C3yMoMNe99N/BascRUIa6JndUUFHJXPEt5SYdHVvA6YnypvXwtV6r7Nre5BuwKGKoQIM9zAUuIYI0pFH2oe59ws/loLVWUsVRdDcYwn71jKAkR89ZtIqfHnhDuZfcWJ8StAgEhP5p/Hicu+P05l0DFo6dAB8OxhHNnnO+UO7yzvixeTb21QfVzryiE7On1S8u1vNnGdQoL9mrb76g39QK9ydsv90Baif4N0tuEtg/DTu39zynVHblhWWSCxQPQyEaVWvyYXCKahWhlyoKMZK2oAsPJBo409iQs0Wt0yywYyaNtTAWjbXPydBmDfUyW/msMzWkgm8mlWPG11fm2M5SMkJAva8gAC6Vs9MAXLAayj6Wy1VEFtPh1nj++vP8XZK829+NzQWdxPHoAsPn4Q5VE+XIeUTnJufNS3gwvPmiEDMCcB0szikxUuUbzxfKw9mCxWlXoQwGyoNMH13b9Bbhu0dPr1RZ43qAz0JpuonDD2HYSyHbJHZrm80HV7p3yZ4aEo4phvGOu87OARy29Zpfld7KBLtYaxq1IppaZ8IPZjqBceL/Vb4IIdByLXwY9QuZSPJawJrCqe3W4FmSKBlKAfRRMUjteitf+v+WxnhVkon8WHjgYD4rrJxkwpWS1tH6bpHvJvZpoXMpjGlMistEka2v+KZvBmmgfZvpQ/xObDUh5JwzZhC+xXojWynEm6D9Q50Zz4ldqywY37XPBIsoX4KwN++CGNMjVqbdoGVIWNXj6xK4MCXyUfd03/atIPOnlbMflNr1N20m1ox+OtomEzc1zJ9I9GY06Y/z+NJywtEDFAfXe0jrGSp3q79BeE3wpxisXc9hpffspoAVXMIXfslho0cApukUge3k+yrg1UHBoo5VYHi/xlmvvqUoyF0ucOrSNvr5P7++0oPKNsB1uNaH35NZGGEOGV57aBVvA/CRef6k7Ce2rcudP1xIEHJZnCCYEoQGESpUxHLZugh1/TmJegKnQJGKHCF/MM937qlKYbzudV2iDqUNwnWZRiuGBuJAE915ijlyvAAmIy1pxdIUSa63vtSjNBEP3ousCaFOp3/y7MSdMkeNb5wh1Lox1vvq3PZaY8eL3ti/2hesjQ+k5FI2SN3tlzQ8IoNRX14UYAHptWOTsqCaWUkLiUpOtSWHQKM8mWPDAQoxTFqBFzNJqmDMkQzxy7hBs8VkAkP/i8ykuCSKIBtEBFcGkEe62juzHyBuHRuDgUX64cZOfehp8kPmV6YvT9G0sdsCAZXex79DcJ6VhfVVc+KpOCCwYWNKB83mXdqH4zLU0Ftj5/LJF214lRzyoa9c2Qn2Yyt0VjKl/xzyjCmykB2JIS+u2DelWtcpGUlIGySv/W+3xuDDUg20t7YBdrTaQLJYpyBuqD8ilnft9tG3vSDpNzqYHAAdXtEnG2eVuig/Fh+fDmgP4CtQrL06iLwCIRh7L6uof7sW83YfWzqxhGv/JcD/lPeSG01quUlSlOlB9V04dwINkhCy0ekA14+fA7tpXeliu6YV00iM8Xvu+JBXMZUiHeAXZQq5nYRh4myBnv8O6fI0WP5IoqTGClUoHQtywTEjlktETXQ7Tm3E72GQPT/Mup3Rs7CDqJ/yb1dZiiT1vXSTVY73cXn2zH8NvbGOe3Yo4/+6jK1oujjkvi4UsNH/nihqc1WjQsi5FfXwoT8IGAl9RRr8QG3P+vhi/PKEqohKAFNS/erklbPXiVYECTPMtFGTk1g97monP9Itt0OeUACpkhaFi5haXMMkq+eyoDsndUD2FNcWp6+SpZxuS7oRlXniC5PEclFvrXn05nJa01Wy1YcQ+v/gZ85Ny6RWzWIuZzgGNk7kmcvo8WTY7PPbU+C71x8RNYaZVThCDozQr9DRIXR6yDnwcnw0hujQiEPYi3MwRCnoWZkKH3NF9gdzmHjahG+cEDbDNU07O36cV/cOpNQk+QOsxZ6AT1qsCp0B+bKFFZsvEMQjLOaKl3F4vbrRjhl7HTeOEsbY9MYcpqzzAzE3q1uvBnL/Lxxn2BmaalZi1myDRKGp4MI55Lk9FdD5n4JsKMR1xP/uj3EiosmKfbY8OGc10e+vgDgYiK3N5MwHE7E4flLQ9K8bBIq8Tm8Dbs6N6aRRvaDh9/HtEAvJGaGUcoomJCFGmo1z5BPUB5U09m7vsNzvZRn4JcO8OEyWROSPLP8fXeWtyG5h/RvE0sGnVOWaYTLa9JpwwikOiz4aKkzx1490Rww/x47PNtSgTZMSEJdVsWLsl3LMkPDmxDmkImHGqVqkfz6eZGbVjjcIYIj42gT+qA+GK2S1ElH4sTSZKRXnilTypWYEnxbJLSq2HHJQKEbjFPuaKH8lTUzcVz3A0NjzrVrzQ7nrrm3UR/Nr2Fi0iGzg3CftZYVJMGjQavtBqmmruY0ee5z30lKxy1ZiCyMmhcapC8JEDqbUWvjwmZhhp3YqQr7fIgGzeTs0U3TOTWY1g8kv8gZ4sRyqPgeplV36TIMxlGubSDtqZQ1VhBskMzLxYue5/9gNk/9I9voaYHBmEp6rjFsYEo87stYqoF719FOrDh2FP99fTUmvzwnbsIIrV9wH+c+URTDRK/JJKHBtefNa5b4sxdlCK9Scfki5UVMy2wbAr9Zyo/87QJgwW/8sjpGziWbbDpEYQ4KGCQ+7x1QBEzvJDcR56IbZtHc6O7aTTFXTAGnNoBF550dqNRwF9rSWgF9Lp63WDHJrhPNQMoKUf8UVeGZUahQtvJ9k8WgFMXxzOg2aFLyVw6DHYTin3X+7lKPzl0K9RhR01fGQ9b+nAnwxZ2QuWyJbPJkg9Cd0tO/16J91MjnKLl7GNfCJU/cOamxJbu0Y69y7innC7QCDwYlg+HGfs8alXtGAnSRZhpM05DIRO5gnzV4Gky0zA672Y5DaDMGoLneYkRjl79VMCF3CMkdw5cgU+UY4aboCeYBmfBy/gdUNxyGG5ZRH1s8RySQnfQG7QT081LDzf1gm69qdk49NORPgQc1GOUFyIr02lh58eizco/qrK7a4zAIl4/h+NX2N531Re0dflZvMGsDR5irR7l1XaciCrSjzK1+2fn1BItn13YkXEJMKppQfZxz1ZGHyH2iwpJTAczgDbaVt+D0yIB00JlnXum37MOaE4OlAxj75b5sYcHU17W3yTzLpvgEsPB88YjTV5dpHazb228wFSkwz9WMKi6Iwt1Tvppg6r4tHRnuFxfzFNPAdJbWQXqgDjufy9WKEn2t3eXxtkKaj/VpjKYbbklsKATrHTdVQgj1DsjBRa3tCm0Pwj4kyZuFGVKpYl0BeAtgFjMMnmlMMqPfX7xLa1JFJfaBk9FBxngzUlgov+3hLI0A2a1CIPKe+VR4fCgIhNEseHXc8AplSbrS7bfRR1RdQmgCJfBkcuszKriJARLBXaPq8oZD2pMQcDu66roNhl/y0LAVXwtKy+3KVFy5q1vY5PI1K/kYUfnYn/9Pxmdzl6RaMN14Z52EAV9Rdmk748dWgmy7PcJUVccyKn8996dc2oqksVzQnPZsdjpQ3UsKPnf/8sxMoABpaxaMM6YW5TdUVRsY8UFT2hqApV5hvBkP0mw1trHg1De8qDScvGV83YeVzzzjdRwZdmZcnf+ez+XqIdNwdH82wqiwrrwodgoUeLsNPJeB5Ssukud1BmCKVCsUQJ7vn2kiOOiO2wE2xpzmZVK1tzLPakgLXsn6ZCOrszhHP89W0vfh5cbVSn3410y5PwNBpIhrawS4kIDHgJx1o7iaZGOHoXOfV3EgsOOIvJazbYHhSURJ+MxEQS4C5IyriFMp1sAClGHydcoJS6dj5m2xFkgEp7U3OtoFUS6b+HA59+1z6YAhqKcIIcGet7SVDB52+ybjLKHn1P1u4/QmoSAOAH96W24NMt6C4uE8J5gdvf6rb+uTeSPAdk0WbNOdPFiLcpE4/2bJT38u511bnxfeVKLtNuYkyleXG8coQoQvxSv5Skt6hXxcheRDIO+7mcHhjQ4wVYJBSURIBfKjRFY9v4Afj0SlWZyoRXs9BOl3f8vSjzj4fbauNhC00ZoB/Xeqv6Kjt8SbU8eXhiT5II4WU2uoiN1qCUxoaABPDcArihRnbfe1E5BIavVMEJzSOz/jQpTN7sJ99sp4sZxiY7J0lEb7fC3kDpCrLw+u9aQmv7mueK/XijlJQ+NrHNDMJgSkqGFJjh9drUqe+RpBjTt56UGdJsvLy0dr2fZ1k90ekYyE6yme7emrYyPNU13mrAMLuEhwEfm3QZdFRihgWTJTPMDqTWU3xRfM4iXDoPJJQu6q0bB/bsLPK5V2S3gjJkn4p640+QwBB1zI25P2Ot4trL4oTqEzZXU016TAeNnCfdQ7QJR2E6NSZOcu/kwdZ0C2ObQBVFgUu9Z/ksiocke9fl8IGQxwEgNe7VNgLkaeCG5GLjrN2jK3dllOFJ2/+GZF6Ivf/tukLcWNmo2M9+xqEdxPHRUqUwZzLJu6UKKz2XRC4x8vOpFbFwys7wLMwGc+Yx4ztFRM8stdyYSWHsPqsiCaP30bEft7OxZGdo4jgUcbAXNc/jS6kDxzAWYLg9c0+9+rbHm9ZhX5oNLm74K5aPobrq7dhyEC1mLVlQq20WbarAa4bkCRxGyWClGN5/1JSguw9ErYPb1QBOej7cQdeC4dKm1Lz6QIuBCrQMW7Ao87zoGJNJYA7/fGMaR0CmhKV2aDovDWLTTgeDEgzljWM/Lf8i8p0T2JCvyUu9rYLSoLGNqM4Pb00BaFfRVpzljk0Lfs00JuugeNcOtFrDdEXcQa9rabU6xmu1xm5FH0GysMFt2QZ+aiN+3ZDoqwMRrtnJWLpGLT4O+APo68U0sAnT9jMeCxI4gB7bckKcBFuOQjYETmVXsY1Tg7mio8itzEy3Fd3RBhhHrl0ukLb4YWuomCkpqnFiagJgkXyg7rpfzAGqL5q8ec6FZMMpDM3kXXNsDYO/n/xphGEObFDwbGkIDHb+PNUYxP+YPesoq43WLZLLETUDyzgONy0hhsXsNPvTqpNtl/bJDnJt0uEY8kONt86Cu6frBxlhnWOB0Cqfjp55I9WivdBC69WzKBGx4CAfZ3/lmvPwOKeNgReIDGWDzUsxtBami+I+MA1dRRCxTmxYIoBDc5dVkciA5j+qczHRrUi1xTeDPeU1lu7/esxWLavotJzcxzE9IiQtKOKZTrBg1uYYqOI7CeCw9ADXc5XwgngVcESV9QPsto/Cq8ff6cbbOsfIt8PgyLokJ7hTONbIjVt5LF5I0+ggoPM3zbx7he/gG0cTxB336N16ZZbc+jIiXVjdkRUbSBvbcleqQ1dWU77kxR9B5DuTrYNiMrsTMPhOG+BI5G3KqjMFII690KeJlbgEgYC/lghDW9DBAylVgnoxbQBmLF/RxCm+cMv5OwBD1dJKex0QU1Mp2a7GJOdJFGrPxgC+3RRYEca9erkTKui2P2f36zQVy7Txc9NQqKbtcl4ufc+YVLZ2TgKk4VymozcSm5bINJOqubM19r3/VneNihevUhNgdsP0gWk2A3b6dt6DANQslaUM2rFltTafGXiLxg5CPqPe+j1N7dFjkm130DEN7iyz5fLHg==" />


<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script type="text/javascript">(window.jQuery)||document.write('<script src="/Integrations/JQuery/jquery-1.11.3.min.js" type="text/javascript"><\/script>');</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.migrate/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script type="text/javascript">(!!window.jQuery.migrateWarnings)||document.write('<script src="/Integrations/JQuery/jquery-migrate-1.2.1.min.js" type="text/javascript"><\/script>');</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">(!!window.jQuery.ui && !!window.jQuery.ui.version)||document.write('<script src="/Integrations/JQuery/jquery-ui-1.11.4.min.js" type="text/javascript"><\/script>');</script>
<script src="/Integrations/Centralpoint/Resources/Controls/Page.js?v8.5.1" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[

 //Design > Styles:  (#ReDesign) Scripts 
$(document).ready(function() {
	// Browser Less Than 1000px
	if ($(window).width() < 1000) {
		$('.left-rail-nav-container').insertAfter('.main-content');
		$('.module-results-intro-paragraph').insertBefore('.details-view-left-column');
		$('.inner-container ul:first-of-type').click(function(){
			$('.inner-container > ul:first-child > li > ul').slideToggle('slow');
			$('#search-expanded-container').insertAfter('#mobile-navigation-container');
		});
	}

	// Lowercase Services Menu Links
	$('nav.services-nav ul.cpsys_BlockList a').each(function() {
		var uRL = $(this).attr('href').toLowerCase();;
		$(this).attr('href', uRL);
	});

	// Find "Cancer Health Info" links & append Querystring
	$('a[href*="/cancer-health-information.aspx"]').not('.event-content a').attr('href', '/main/cancer-health-information.aspx?iid=1_001289');
	$('a[href*="/womens-health-information.aspx"]').not('.event-content a').attr('href', '/main/womens-health-information.aspx?iid=1_007458');
	$('a[href*="/birth-center-health-information.aspx"]').not('.event-content a').attr('href', '/main/birth-center-health-information.aspx?iid=1_007214');
	$('a[href*="/cardiac-health-information.aspx"]').not('.event-content a').attr('href', '/main/cardiac-health-information.aspx?iid=1_007459');
	$('a[href*="/stroke-health-information.aspx"]').not('.event-content a').attr('href', '/main/stroke-health-information.aspx?iid=10_000045');
	$('a[href*="/sleep-health-information.aspx"]').not('.event-content a').attr('href', '/main/sleep-health-information.aspx?iid=1_000800');
	$('a[href*="/wound-care-health-information.aspx"]').not('.event-content a').attr('href', '/main/wound-care-health-information.aspx?iid=33_000175');
	$('a[href*="/neuroscience-health-information.aspx"]').not('.event-content a').attr('href', '/main/neuroscience-health-information.aspx?iid=1_007456');
	$('a[href*="/bariatrics-health-information.aspx"]').not('.event-content a').attr('href', '/main/bariatrics-health-information.aspx?iid=10_000053');
	$('a[href*="/ortho-health-information.aspx"]').not('.event-content a').attr('href', '/main/ortho-health-information.aspx?iid=1_007455');
	$('a[href*="/dialysis-health-information.aspx"]').not('.event-content a').attr('href', '/main/dialysis-health-information.aspx?iid=1_000706');
        $('a[href*="/diabetes-health-information.aspx"]').not('.event-content a').attr('href', '/main/diabetes-health-information.aspx?iid=1_001214');
});
 //End of Design > Styles: Scripts 
//]]>
</script>

<script src="/Integrations/Centralpoint/Resources/Page/SwfObject.js" type="text/javascript"></script>
<script src="/Integrations/JQuery/Plugins/jquery.cp_Accordion.js?update=8.4.38" type="text/javascript"></script>
<script src="http://ajax.aspnetcdn.com/ajax/4.6/1/MicrosoftAjax.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(window.Sys && Sys._Application && Sys.Observer)||document.write('<script type="text/javascript" src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yKL3veK4_1o6aC-AgxFwMcMVIIRV9FSpZJuOVk-LAIAVsBBLL3O32w_IrtC9GX2Hh4_Jux5cIno3a_NfWjNJey3pHIYsP0dX-mgG1YhQAMEmmQ4K4v3GbkuLsq21ZvMc7MLyHc4nUSN_UV69kEqz7hA1&t=ffffffffd416f7fc"><\/script>');//]]>
</script>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="43343198" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="RpilkantW6tD3IPZ1Kn/Bdy9sbpdcl6dQc+s4xT2H5h1ZG2lVZw7EtpyFrmbYz7ZDGe6jv/dBiAFObkGrRLR0a8+xqzqTUC/UbcSwxpsu8vVs5CT8SPMZqzl5MRyPFz3yWOJff3YQZED2K8EfmdUixplfIPpCXDm8oENWCwrxDk=" />
	
	<script> 
$(document).ready(function(){
    $("#discover-expand").click(function(){
        $("#discover-expanded-container").slideToggle("slow");
    });
    $("#search-expand").click(function(){
        $("#search-expanded-container").slideToggle("slow");
    });
    $("#mobile-navigation-expand").click(function(){
        $("#mobile-navigation-container").slideToggle("slow");
    });
    $("#mobile-search-expand").click(function(){
       $("#search-expanded-container" ).insertAfter( "#mobile-navigation-container" );  
        $("#search-expanded-container").slideToggle("slow");
    });

});
</script>

<style>
</style>
<header>
<!-------Top Nav------>
<nav class="top">
<div class="inner-container" id="blue-navigaiton-bar">
<ul>
    <li><a href="/Main/mclaren.aspx">About Us</a></li>
    <li id="health-care-professionals"><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li><a href="/main/patient-appointment.aspx" target="_blank" style="background: #0066a4;">Book an Appointment</a></li>
    
    
    <li><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a id="discover-expand" class="discover-mclaren-health-button" href="#">Discover McLaren Health</a></li>
    <li><img style="margin-left:13px; margin-top:13px;" src="/Uploads/Public/Images/Designs/nav-search-button.png" id="search-expand" alt="Search Button" title="Search Button"></li>
</ul>
<div class="clear"></div>
</div>
<!------------Search Box-------------->
<div id="search-expanded-container" style="display:none;">
<div class="inner-container">
<div class="Search-Input"><input style="" onkeypress="if (event.keyCode == '13') { document.location.href='/Main/Search.aspx?search=' + this.value; return false; }" placeholder="I'm looking for..." name="HtmlSearchCriteria" type="text"></div>
<div class="Search-Button"><input style="vertical-align: middle;" onclick="document.location.href='/Main/Search.aspx?search=' + document.forms[0].HtmlSearchCriteria.value;return false;" name="HtmlSearchGo" value="Go" src="/Uploads/Public/Images/Designs/nav-search-button.png" type="image"></div>
</div>
</div>
<!------------Search Box-------------->
<!------------Discover Box-------------->
<div id="discover-expanded-container" style="display:none;">
<div class="inner-container">
<ul class="discover-list">
    <li class="header">McLaren Services</li>
    <li><a href="/Main/bariatric-services.aspx">Bariatrics</a></li>
    <li><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li><a href="/Main/imaging-services.aspx">Diagnostic Imaging</a></li>
    <li><a href="/Main/birth-center-services.aspx">Family Birth Place</a></li>
    <li><a href="/Main/home-care.aspx">Home Care</a></li>
    <li><a href="/Main/hospice-services.aspx">Hospice</a></li>
    <li><a href="/Main/laboratory-services.aspx">Lab</a></li>
    <li><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li>
    <li><a href="/Main/pharmacy-services.aspx">Pharmacy</a></li>
    <li><a href="/Main/robotic-services.aspx">Robotic Surgery</a></li>
    <li><a href="/Main/surgery-services.aspx">Surgical Services</a></li>
    <li><a href="/Main/womens-services.aspx">Women’s Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">Primary Care &amp; Specialists</li>
    <li><a href="/Main/locations.aspx?taxonomy=Cardiology7">Cardiology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=FamilyMedicine4">Family Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=InternalMedicine8">Internal Medicine</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=MedicalCenters">Medical Centers</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Pediatrics11">Pediatrics</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Surgeons">Surgeons</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=UrgentCare9">Urgent Care</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=Urology10">Urology</a></li>
    <li><a href="/Main/locations.aspx?taxonomy=WomensServices2">Women's Services</a></li>
</ul>
<ul class="discover-list">
    <li class="header">McLaren Hospitals</li>
    <li><a href="/BayRegion/BayRegion.aspx">McLaren Bay Region</a></li>
    <li><a href="/bayspecialcare/BaySpecialCare.aspx">McLaren Bay Special Care</a></li>
    <li><a href="/CentralMichigan/CentralMichigan.aspx">McLaren Central Michigan</a></li>
    <li><a href="/Flint/Flint.aspx">McLaren Flint</a></li>
    <li><a href="/Lansing/Lansing.aspx">McLaren Greater Lansing</a></li>
    <li><a href="/LapeerRegion/LapeerRegion.aspx">McLaren Lapeer Region</a></li>
    <li><a href="/macomb/macomb.aspx">McLaren Macomb</a></li>
    <li><a href="/northernmichigan/northernmichigan.aspx">McLaren Northern Michigan</a></li>
    <li><a href="/Oakland/Oakland.aspx">McLaren Oakland</a></li>
    <li><a href="/lansing/orthopedic-services.aspx">McLaren Orthopedic Hospital</a></li>
    <li><a href="/porthuron/porthuron.aspx">McLaren Port Huron</a></li>
</ul>
<ul class="discover-list" style="margin-right:0px;">
    <li class="header">McLaren Difference</li>
    <li><a href="/Main/career.aspx">Careers</a></li>
    <li><a href="/mhp/mhp.aspx">Health Advantage</a></li>
    <li><a href="http://www.karmanos.org" target="_blank">Karmanos Cancer Institute</a></li>
    <li><a href="/Clarkston/Clarkston.aspx">McLaren Clarkston</a></li>
    <li><a href="/Main/Home.aspx">McLaren Health Care</a></li>
    <li><a href="/mhp/mhp.aspx">McLaren Health Plan</a></li>
    <li><a href="/aco/homepage.aspx">McLaren High Performance Network, LLC</a></li>
    <li><a href="/Main/homecare-group.aspx">McLaren Homecare Group</a></li>
    <li><a href="/Main/laboratory-services.aspx">McLaren Medical Laboratory</a></li>
    <li><a href="/Main/pharmacy-services.aspx">McLaren Pharmacy</a></li>
    <li><a href="/mclaren-physician-partners/mpp.aspx">McLaren Physician Partners</a></li>
    <li><a href="/Main/foundation.aspx">Our Foundations</a></li>
</ul>
<br style="clear:both;">
</div>
</div>
<!------------Discover Box-------------->
</nav>
<!-------Top Nav------>
<!-------Logo Row------>
<div class="inner-container">
<div class="logo"><a href="/main/home.aspx"><img src="/Uploads/Public/Images/header.jpg" alt=""/></a></div>
<div class="mobile-accordion-icons"><img style="margin-right:8px;" src="/Uploads/Public/Images/Designs/mobile-search-button.jpg" id="mobile-search-expand"><img style="" src="/Uploads/Public/Images/Designs/mobile-nav-button.jpg" id="mobile-navigation-expand"></div>
<div class="second-nav">
<ul>
    <table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation73&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation73&#39;).hide();" id="cpPortableNavigation76a224fb-2841-4c3b-b9bc-237b847fccbe">
			<li id="services-nav-item" class="second-nav-services"><a href="#" onclick="return false;">Services</a></li>
		</div><div id="cpPortableNavigation73" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;width:600px;overflow:hidden;">
			<style>
    .featured-service-alpha .grey {color: #cccccc !important; font-weight:normal !important;}
    .featured-service-links{height:250px; width:750px;}
    .featured-service-links a{font-size:13px; color:#5f6062; padding:0px; margin:0px; font-family:'Alegreya Sans', sans-serif; text-decoration:none;}
    .featured-service-links a:hover{text-decoration:underline;}
    .featured-service-links p{margin:0px; padding:0px; line-height:20px;}
    .featured-service-alpha a:hover{text-decoration:underline;}
    .all-services-p{display:block !important;}
</style>
<div class="services-drop-down">
<div class="services-left-column">
<h5 style="padding-left:31px;">Featured Services</h5>
<!----Services of Excellence DataSource----->
<ul>
    <li class="services-of-excellence"><a href="/Main/cancer-care.aspx">Cancer Care</a></li>
    <li class="services-of-excellence"><a href="/Main/cardiac-care.aspx">Cardiology</a></li>
    <li class="services-of-excellence"><a href="/Main/orthopedic-services.aspx">Orthopedics</a></li></ul>
<!---img style="" src="/Uploads/Public/Images/Designs/Main/services-drop-down-image.png"--->
</div>
<div class="services-right-column">
<h5>Our Services</h5>
<!----------------------->
<div class="featured-service-links">
<div class="featured-services-all">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/acute-care.aspx">Acute Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/home-care.aspx">Home Care</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/hospice-services.aspx">Hospice</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/robotic-services.aspx">Robotics</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/blood-services.aspx">Blood Conservation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/imaging-services.aspx">Imaging</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/senior-services.aspx">Senior Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/breast-care.aspx">Breast Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/sleep-services.aspx">Sleep</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cancer-care.aspx">Cancer Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/infusion-services.aspx">Infusion</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/spine-services.aspx">Spine</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/cardiac-care.aspx">Cardiology</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/laboratory-services.aspx">Lab</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/stroke-services.aspx">Stroke</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/surgery-services.aspx">Surgery </a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/dialysis-services.aspx">Dialysis</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/trauma-services.aspx">Trauma</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/emergency-services.aspx">Emergency Services</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urgent-care.aspx">Urgent Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/ems-services.aspx">EMS</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/urology-services.aspx">Urology</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/eye-care.aspx">Eye Care</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pain-center-services.aspx">Pain Center</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/womens-services.aspx">Women's Services</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pediatric-services.aspx">Pediatrics</a></p></div></td>
		<td style="vertical-align:top;" width="34%"><div><p class="all-services-p"><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/fitness-and-health.aspx">Fitness</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p></div></td>
		<td style="vertical-align:top;" width="34%"></td>
	</tr>
	<tr>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/foundation.aspx">Foundation</a></p></div></td>
		<td style="vertical-align:top;" width="33%"><div><p class="all-services-p"><a href="/Main/podiatry-services.aspx">Podiatry</a></p></div></td>
		<td style="vertical-align:top;" width="34%"></td>
	</tr>
</table>
</div>
<p><a href="/Main/acute-care.aspx">Acute Care</a></p><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p><p><a href="/Main/fitness-and-health.aspx">Fitness</a></p><p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/home-care.aspx">Home Care</a></p><p><a href="/Main/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging</a></p><p><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p><p><a href="/Main/infusion-services.aspx">Infusion</a></p><p><a href="/Main/laboratory-services.aspx">Lab</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatrics</a></p><p><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/podiatry-services.aspx">Podiatry</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotics</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep</a></p><p><a href="/Main/spine-services.aspx">Spine</a></p><p><a href="/Main/stroke-services.aspx">Stroke</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p><p><a href="/Main/surgery-services.aspx">Surgery </a></p><p><a href="/Main/trauma-services.aspx">Trauma</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/womens-services.aspx">Women's Services</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p>
</div>
<div class="featured-service-alpha">
<a class="service-alpha-all" style="color:#0066a4 !important;" href="#">ALL</a>
<a class="service-alpha" href="#">A</a>
<a class="service-alpha" href="#">B</a>
<a class="service-alpha" href="#">C</a>
<a class="service-alpha" href="#">D</a>
<a class="service-alpha" href="#">E</a>
<a class="service-alpha" href="#">F</a>
<a class="service-alpha" href="#">G</a>
<a class="service-alpha" href="#">H</a>
<a class="service-alpha" href="#">I</a>
<a class="service-alpha" href="#">J</a>
<a class="service-alpha" href="#">K</a>
<a class="service-alpha" href="#">L</a>
<a class="service-alpha" href="#">M</a>
<a class="service-alpha" href="#">N</a>
<a class="service-alpha" href="#">O</a>
<a class="service-alpha" href="#">P</a>
<a class="service-alpha" href="#">Q</a>
<a class="service-alpha" href="#">R</a>
<a class="service-alpha" href="#">S</a>
<a class="service-alpha" href="#">T</a>
<a class="service-alpha" href="#">U</a>
<a class="service-alpha" href="#">V</a>
<a class="service-alpha" href="#">W</a>
<a class="service-alpha" href="#">X</a>
<a class="service-alpha" href="#">Y</a>
<a class="service-alpha" href="#">Z</a>
</div>
<!------------------------------>
</div>
<br style="clear:both;">
</div>
<script>

$(".service-alpha").click(function(){
    $(".featured-services-all").hide();
});
$(".service-alpha-all").click(function(){
    $(".featured-services-all").show();
});

$(document).ready(function() {
    function filterResults(letter){
        $('.featured-service-links p').hide();
        $('.featured-service-links p').filter(function() {
            return $(this).text().charAt(0).toUpperCase() === letter;
        }).show();
    };
    filterResults('B');
    $('a').on('click',function(){
        var letter = $(this).text();            
        filterResults(letter);        
    });
});

var $p =  $('.featured-service-links p')
$('a').addClass(function(){
    var s = this.textContent;
    return $p.filter(function(){
       return this.textContent.charAt(0) === s
           }).length ? '' : 'grey';
})

</script>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigationc1fdecc8-73a4-478e-b7f9-da0884005b89">
			<li class="find-a-physician"><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation85&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation85&#39;).hide();" id="cpPortableNavigation5bdbf3e4-64dc-413f-ab0c-dc4179e9a3e4">
			<li class="patient-and-visitor-info" id="patient-and-financial-info"><a href="#">Patient &amp; Financial Info</a></li>
<style>
    #patient-and-financial-info{width:120px !important;}
</style>
		</div><div id="cpPortableNavigation85" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/patient.aspx" target="_self">Patient Information</a><ul><li class="featured-service-style"><a href="/main/patient-opt-out.aspx" target="_self">Fundraising Opt-Out</a></li><li class="featured-service-style"><a href="/main/patient-mymclaren.aspx" target="_self">MyMcLaren Chart</a></li><li class="featured-service-style"><a href="/main/patient-notice-privacy.aspx" target="_self">Notice of Privacy Practices</a></li><li class="featured-service-style"><a href="/main/patient-donation.aspx" target="_blank">Online Donation</a></li><li class="featured-service-style"><a href="/main/patient-pre-registration.aspx" target="_self">Patient Pre-Registration</a></li><li class="featured-service-style"><a href="/main/patient-appointment.aspx" target="_self">Schedule Appointments</a></li><li class="featured-service-style"><a href="/main/patient-thank-doctor.aspx" target="_self">Thank Your Doctor</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/financial.aspx" target="_self">Financial Information</a><ul><li class="featured-service-style"><a href="/main/financial-billing1.aspx" target="_self"> Pay Your Bill Online</a></li><li class="featured-service-style"><a href="/main/financial-services.aspx" target="_self">Financial Assistance</a></li><li class="featured-service-style"><a href="/main/financial-insurance.aspx" target="_self">Insurance Information</a></li><li class="featured-service-style"><a href="/main/financial-participating-plans.aspx" target="_self">Participating Health Plans</a></li><li class="featured-service-style"><a href="/main/financial-insurance-veterans.aspx" target="_self">Veterans Choice Insurance</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/visitor.aspx" target="_self">Visitor Information </a><ul></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div onmouseover="$(&#39;#cpPortableNavigation77&#39;).show();" onmouseout="$(&#39;#cpPortableNavigation77&#39;).hide();" id="cpPortableNavigationbf4fa1a8-3c75-4ea8-97c5-67f9504e6f30">
			<li class="health-and-wellness"><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
		</div><div id="cpPortableNavigation77" class="cpsys_MobileNavigation_Menu" style="display:none;position:absolute;z-index:1001;overflow:hidden;">
			<style>
    .main-nav-drop-down h5 {font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    .main-nav-drop-down-column{float:left; width:33%;}
    .main-nav-drop-down-column ul{background-image:none !important;}
    li.ParentDropDownCSS2{display:block; padding:0px !important; width:auto !important; font-family: 'Alegreya Sans', sans-serif; font-weight: bold; color: #0066a4; padding-left: 0px; margin-bottom: 10px; font-size: 26px; text-transform: uppercase;}
    li.featured-service-style{display:block; padding:0px !important; width:auto !important; float:none !important; font-size:13px; color:#5f6062; padding-left:10px;}
    li.featured-service-style a{color:#5f6062 !important; font-weight:normal; text-transform:none;}
</style>
<div class="main-nav-drop-down">
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-your-health.aspx" target="_self">Your Health</a><ul><li class="featured-service-style"><a href="/main/healht-clinical-trials.aspx" target="_self">Clinical Trials</a></li><li class="featured-service-style"><a href="/main/health-community-needs.aspx" target="_self">Community Health Needs Assessment</a></li><li class="featured-service-style"><a href="/main/health-information.aspx" target="_self">Health Information</a></li><li class="featured-service-style"><a href="/main/health-navigator.aspx" target="_self">Health Navigator</a></li><li class="featured-service-style"><a href="/main/health-my-checkups.aspx" target="_self">My Checkups Health Tool</a></li><li class="featured-service-style"><a href="/main/health-mymclaren-chart.aspx" target="_self">My McLaren Chart</a></li><li class="featured-service-style"><a href="/main/health-patient-education.aspx" target="_self">Patient Education</a></li><li class="featured-service-style"><a href="/main/health-wellness-tools.aspx" target="_self">Wellness Tools</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-classes-programs.aspx" target="_self">Classes & Programs</a><ul><li class="featured-service-style"><a href="/main/health-classes.aspx" target="_self">Classes and Events</a></li><li class="featured-service-style"><a href="/main/health-support-groups.aspx" target="_self">Support Groups</a></li></ul></li></ul>
</div>
<div class="main-nav-drop-down-column">
<ul class="cpsys_BlockList"><li class="ParentDropDownCSS2"><a href="/main/health-mclarenhealthplan.aspx" target="_self">McLaren Health Plan</a><ul></ul></li></ul>
</div>
</div>
		</div></td><td style="vertical-align:bottom;text-align:left;"><div id="cpPortableNavigation52587aa1-b090-4537-9c98-40261a490ab3">
			<li class="our-facilities"><a href="/Main/locations.aspx">Our Facilities</a></li>
		</div></td>
	</tr>
</table>
</ul>
</div>
<div style="clear:both;"></div>
</div>
<!-------Logo Row------>
</header>
<div id="mobile-navigation-container" style="display:none;">
<ul class="mobile-menu">
    <li>
    <div class="mobile-navigation-accordion">Services</div>
    <div class="mobile-navigation-content"><p><a href="/Main/acute-care.aspx">Acute Care</a></p><p><a href="/Main/bariatric-services.aspx">Bariatric Services</a></p><p><a href="/Main/psychiatric-services.aspx">Behavioral Health</a></p><p><a href="/Main/blood-services.aspx">Blood Conservation</a></p><p><a href="/Main/breast-care.aspx">Breast Care</a></p><p><a href="/Main/cancer-care.aspx">Cancer Care</a></p><p><a href="/Main/cardiac-care.aspx">Cardiology</a></p><p><a href="/Main/diabetic-nutritional-services.aspx">Diabetes Education</a></p><p><a href="/Main/dialysis-services.aspx">Dialysis</a></p><p><a href="/Main/emergency-services.aspx">Emergency Services</a></p><p><a href="/Main/ems-services.aspx">EMS</a></p><p><a href="/Main/eye-care.aspx">Eye Care</a></p><p><a href="/Main/birth-center-services.aspx">Family BirthPlace</a></p><p><a href="/Main/fitness-and-health.aspx">Fitness</a></p><p><a href="/Main/foundation.aspx">Foundation</a></p><p><a href="/Main/free-clinic-services.aspx">Free Clinic</a></p><p><a href="/Main/home-care.aspx">Home Care</a></p><p><a href="/Main/hospice-services.aspx">Hospice</a></p><p><a href="/Main/imaging-services.aspx">Imaging</a></p><p><a href="/Main/infection-control-services.aspx">Infection Prevention</a></p><p><a href="/Main/infusion-services.aspx">Infusion</a></p><p><a href="/Main/laboratory-services.aspx">Lab</a></p><p><a href="/Main/medical-equipment.aspx">Medical Equipment</a></p><p><a href="/Main/neuroscience-services.aspx">Neuroscience</a></p><p><a href="/Main/occupational-medicine-services.aspx">Occupational Medicine</a></p><p><a href="/Main/orthopedic-services.aspx">Orthopedics</a></p><p><a href="/Main/pain-center-services.aspx">Pain Center</a></p><p><a href="/Main/pediatric-services.aspx">Pediatrics</a></p><p><a href="/Main/pharmacy-services.aspx">Pharmacy</a></p><p><a href="/Main/podiatry-services.aspx">Podiatry</a></p><p><a href="/Main/pulmonary-services.aspx">Pulmonary</a></p><p><a href="/Main/rehabilitation-services.aspx">Rehab & Therapy</a></p><p><a href="/Main/robotic-services.aspx">Robotics</a></p><p><a href="/Main/senior-services.aspx">Senior Services</a></p><p><a href="/Main/sleep-services.aspx">Sleep</a></p><p><a href="/Main/spine-services.aspx">Spine</a></p><p><a href="/Main/stroke-services.aspx">Stroke</a></p><p><a href="/Main/studer.aspx">Studer Group</a></p><p><a href="/Main/surgery-services.aspx">Surgery </a></p><p><a href="/Main/trauma-services.aspx">Trauma</a></p><p><a href="/Main/urgent-care.aspx">Urgent Care</a></p><p><a href="/Main/urology-services.aspx">Urology</a></p><p><a href="/Main/womens-services.aspx">Women's Services</a></p><p><a href="/Main/wound-care-services.aspx">Wound Care</a></p></div>
    </li>
    <li><a href="/Main/physician-directory.aspx?search=executesearch">Find a Physician</a></li>
    <li>
    <div class="mobile-navigation-accordion">Patient &amp; Visitor Info</div>
    <div class="mobile-navigation-content">
    <p><a href="/Main/patient.aspx">Patient Information</a></p>
    <p><a href="/Main/Financial.aspx">Financial Information</a></p>
    <p class="mobile-navigation-link-visitor"><a href="/Main/Visitor.aspx">Visitor Information</a></p>
    </div>
    </li>
    <li><a href="/Main/health.aspx">Health &amp; Wellness</a></li>
    <li><a href="/Main/locations.aspx">Our Facilities</a></li>
    <li><a href="/Main/foundation.aspx">Ways To Give</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren.aspx">About Us</a></li>
    <li id="health-care-professionals" class="top-nav-mobile"><a href="/Main/healthcare-professionals.aspx">Our Health Care Professionals</a></li>
    <li class="top-nav-mobile" ><a href="/main/patient-appointment.aspx">Book an Appointment</a></li>
    
    <li class="top-nav-mobile"><a href="/Main/Patient-MyMcLaren.aspx">My McLaren Chart</a></li>
    <li class="top-nav-mobile"><a href="/Main/Research.aspx">Research &amp; Clinical Trials</a></li>
    <li class="top-nav-mobile"><a href="/Main/career.aspx">Careers</a></li>
    <li class="top-nav-mobile"><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
</ul>
</div>
	
	<div class="cpweb_PerimeterMiddle">
		<div id="blPerimiter" class="cpsys_Block">
	
			
			<div id="tdPerimeterCenter" class="cpsys_BlockColumn">
		<div id="divWrapper" class="cpweb_Wrapper">
	<div id="cphBody_divTop" class="cpsty_Top">
		
		
		<div id="cphBody_divTopAc2" class="cpsty_SiteTypes_Default_TopAc2"><div id="cpsys_Advertisers_af343aee-a245-4e1d-9da4-6d08bbbd30c5" style="text-align:left;">
	<div class="breadcrumb-trail"><div class="Breadcrumb"><span><a href="/main/home.aspx" target="_self">Home</a> &gt; </span><span><span>Website Privacy Policy</span></span></div></div>
</div></div>
	</div>
	<div style="clear:both;">
		<div id="cphBody_blSiteType" class="cpsys_Block cpsty_blSiteType">
			
			<div id="cphBody_tdLeft" class="cpsys_BlockColumn cpsty_LeftTd">
				
				<div class="cpsty_Left">
					
					<div id="cphBody_divLeftNav" class="cpsty_SiteTypes_Default_LeftNav"></div>
					
					
				</div>
			
			</div>
			
			<div id="cphBody_tdCenter" class="cpsys_BlockColumn cpsty_CenterTd" style="width: 98%;">
				
				<div id="cphBody_divCenter" class="cpsty_Center">
					<div id="cphBody_divCenterAc1" class="cpsty_SiteTypes_Default_CenterAc1"><div id="cpsys_Advertisers_bf3115e5-4271-4bad-91ed-a4aa140e412a" style="text-align:left;">
	<style>
    #data-source-created{display:none;}
    #service-line-datasource{display:none;}
</style>
<div class="left-rail-nav-container">
<ul parentsystemtype="FirstTierList" id="site-nav"><li class="liParent"><a href="/main/mclaren.aspx" target="_self" class="d173dbee-de0f-4132-ba5e-76a09f076348">About Us</a><ul><li class="liParent"><a href="/main/mclaren-awards.aspx" target="_self" class="64b1e7ee-84b2-4587-afe5-5d74ca900868">Award Winning Care</a></li><li class="liParent"><a href="/main/mclaren-community-assessment.aspx" target="_self" class="f0856316-de0f-442c-8207-78ea6ee5b5ce">Community Health Needs Assessment</a></li><li class="liParent"><a href="/main/mclaren-contact-us.aspx" target="_blank" class="25423667-c62c-4a08-89f5-0296d2ca22f8">Contact Us</a></li><li class="liParent"><a href="/main/mclaren-history.aspx" target="_self" class="6c6781bf-645e-4190-9b15-b1df37f62521">History</a><ul><li class="liParent"><a href="/main/mclaren-br-history.aspx" target="_self" class="bff09b68-e652-41dd-8b45-a963dda7c15d">McLaren Bay Region</a></li><li class="liParent"><a href="/main/mclaren-cm-history.aspx" target="_self" class="023cada0-e4d5-4d9b-9da1-81c3b5ca94b6">McLaren Central Michigan</a></li><li class="liParent"><a href="/main/mclaren-clarkston.aspx" target="_self" class="eb43ae8e-0657-4790-80c0-4f97ab08bd54">McLaren Clarkston</a></li><li class="liParent"><a href="/main/mclaren-flint-history.aspx" target="_self" class="d8639099-ce6a-4ead-9139-a59962d5d3db">McLaren Flint History</a></li><li class="liParent"><a href="/main/mclaren-gl-history.aspx" target="_self" class="1a368f0f-f80d-41d5-9f49-d8227cb5b09d">McLaren Greater Lansing</a></li><li class="liParent"><a href="/main/mclaren-hg.aspx" target="_self" class="9177539c-3814-4aa1-b948-718e538bed6b">McLaren Homecare Group</a></li><li class="liParent"><a href="/main/mclaren-lr-hisotry.aspx" target="_self" class="592f9725-eeb8-403c-b5cd-40d24c6ff7fa">McLaren Lapeer Region</a></li><li class="liParent"><a href="/main/mclaren-macomb-history.aspx" target="_self" class="787adc33-1be5-4654-a1e1-b00f7748edb4">McLaren Macomb</a></li><li class="liParent"><a href="/main/mclaren-nm-history.aspx" target="_self" class="f8821150-597e-48dc-b092-7853def059fb">McLaren Northern Michigan</a></li><li class="liParent"><a href="/main/mclaren-oakland.aspx" target="_self" class="1f97c3e8-205c-45f2-899f-cdd5dcabed17">McLaren Oakland</a></li></ul></li><li class="liParent"><a href="/main/mclaren-blog.aspx" target="_self" class="79a09704-9964-497f-b21c-29a720645b6f">Blog</a></li><li class="liParent"><a href="/main/mclaren-executives.aspx" target="_self" class="84efe6f0-cf94-4b18-9e0b-9ce8374387c1">Meet the Executive Team</a></li><li class="liParent"><a href="/main/mclaren-message.aspx" target="_self" class="d97677db-b368-4c2d-b08a-a3c7a2a3a306">Message to Our Community</a></li><li class="liParent"><a href="/main/mclaren-mission.aspx" target="_self" class="212a2d08-6f76-49f5-b5f0-9b2ad17f88d8">Mission Statement</a></li><li class="liParent"><a href="/main/mclaren-publications.aspx" target="_self" class="a424848b-5c16-49fe-ba74-05c565299104">Publications and Videos</a></li><li class="liParent"><a href="/main/mclaren-research.aspx" target="_self" class="8c52cd2f-a2bd-40fe-bff6-a7fd50955fff">Research Studies</a></li><li class="liParent"><a href="/main/mclaren-service-area.aspx" target="_self" class="df08959d-e863-4e26-a7b3-e13754ca9049">Service Area  </a></li></ul></li></ul>
</div>
</div></div>
					
					<div id="cphBody_divContent" class="cpsty_SiteTypes_Default_Content">
	
	
			
<!--cpsys_Template:cpsys_Register-->
<!--cpsys_Template:cpsys_Register-->


<!--cpsys_Template:DetailsHeaderContent-->
<div class="left-rail-nav-container" id="service-line-datasource">

</div>
<div class="main-content">
<!---Shows Location Info--->

<style> 
/*----Styles For SubPages-----*/
.cpsty_CenterTd{width:100% !important;}.cpsty_Center h1{margin-top:0px;}.site-banner .inner-container{min-height:115px;}.site-banner .inner-container h1{line-height:115px; font-size:34px;}.site-banner .inner-container h1.multi-line{line-height:normal !important; font-size:34px !important; height:100px !important;}.site-banner{height:173px; background-color:#2f82b6; background-image:none;} .site-banner .inner-container .site-banner-logo{display:none} .service-location-on-landing-page-top{display:none;} .service-location-on-landing-page{display:none;} .left-rail-nav-container{display:block; width:24.5% !important; margin-right:2%; float:left; border:solid 1px #cccccc;} .main-content{width:72.5%; float:left;} @media only screen and (max-width : 1023px){.main-content{width:100% !important; float:none;}.left-rail-nav-container{width:100% !important; margin-right:0%; float:none; border:solid 1px #cccccc;}}
/*----Styles For SubPages-----*/
</style>

<h1 class="pageheader">Website Privacy Policy</h1>
<div class="service-location-on-landing-page-top"></div>
<p>McLaren Health Care Corporation cares about privacy issues and wants you to be familiar with how we collect, use and disclose Personally Identifiable Information you provide to us.</p>
<p>This Privacy Policy and Terms (the “Policy”) describes our practices in connection with Personally Identifiable Information that we collect through our website located at www.mclaren.org, the websites of our subsidiaries that display or directly link to this Policy, and the e-mail messages that we send to you that directly link to this Policy (collectively, the “Services”).</p>
<p>There are a number of circumstances where this Policy does not apply and we want to identify a few of them for you:</p>
<ul>
    <li>This Policy does not apply to the information you share when receiving treatment from your medical provider, including when you are receiving medical services from us. For example, this Policy would not apply to the information you share with us when admitted to the emergency room. The use and disclosure of the information you provide in such circumstances is governed by the Federal Health Insurance Portability and Accountability Act of 1996, more commonly known as HIPAA, as well as Michigan law. You can learn more about your HIPAA rights and protections and our obligations in our Notice of Privacy Practices.</li>
    <li>This Policy does not apply to the information you share and your use of the <em>My McLaren Chart</em>, which is the patient portal we offer. The use and disclosure of the information you provide when using the <em>My McLaren Chart</em> is governed by HIPAA, Michigan state law, as well as the terms and conditions and other policies you agreed to when registering to access the <em>My McLaren Chart</em> service.</li>
    <li>This Policy does not apply to the practices of companies that we do not own or control. Other services and organizations that we link to may have their own privacy policies governing the use of information you provide to them. For example, organizations such as Facebook, Twitter and LinkedIn each have their own privacy policy and practices.</li>
    <li>This Privacy Policy does not apply to individuals whom we do not employ, including any of the third parties to whom we may disclose user information as set forth in this Privacy Policy.</li>
</ul>
<p>If you have any questions or comments about this Policy or our privacy practices, please contact us by using one of the means listed on our <a href="/main/mclaren-contact-us.aspx">Contact page</a></p>
<ol>
    <li><strong>Personally Identifiable Information We May Collect</strong>
    <p>“Personally Identifiable Information” is information that identifies you as an individual, such as name, birth date, telephone number, e-mail address, or unique device identifiers that was gathered in connection with your use of the Services.</p>
    <p>If we combine Personally Identifiable Information with protected health information subject to protection under HIPAA, the combined information will be treated as protected health information for as long as it remains combined.</p>
    </li>
    <li><strong>Other Information we may collect</strong>
    <p>“Other Information” is any information that does not reveal your specific identity or does not directly relate to an individual, including, for example: (1) computer or device connection information, such as browser type and version, operating system type and version, device information, and other technical identifiers; (2) information collected through cookies and other technologies; or (3) aggregated information, such as usage history and search history.</p>
    <p>If we combine Other Information with Personally Identifiable Information, the combined information will be treated as Personally Identifiable Information for as long as it remains combined.</p>
    </li>
    <li><strong>Security Measures</strong>
    <p><strong>Virus Detection and E-mail Security:</strong>&nbsp;For website security, we use software programs to monitor traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage.&nbsp; </p>
    <p><strong>General Practices: </strong>Although we seek to use reasonable measures to protect Personally Identifiable Information, please be aware that no security measures are perfect or impenetrable. Therefore, we cannot and do not guarantee that the information you provide to us through the Services&nbsp;will not be viewed by unauthorized persons. We are not responsible for circumvention of any privacy settings or security measures contained on the Services.</p>
    <p><strong>Security of Service Communications</strong>: Some communication through the Services may be sent through the standard HTTP protocol and may be delivered using regular e-mail. Information sent over HTTP is not encrypted. E-mail, while convenient, also poses several risks (e.g., e-mail is not a secure form of communication, is unreliable, can be forwarded, etc.). We cannot guarantee the security of the information sent through such means, nor can we guarantee that information you supply to us will not be intercepted while being transmitted to us. It is important for you to protect against unauthorized access to your computer and to take appropriate security measures to protect your information. We use encryption technology, such as Secure Sockets Layer (SSL), to protect your protected health information during data transport with <em>My McLaren Chart</em>.</p>
    <p><strong>Your Obligations to Safeguard your Information</strong>: Security is not a one person job. You must also take reasonable measures to protect your information, including, for example, securing your computer and mobile device, using an antivirus software, using a firewall, and other similar safeguards.</p>
    </li>
    <li><strong>Disclaimer</strong>
    <p>McLaren Health Care Corporation and its subsidiaries offer information on the Services for general educational purposes only. This information should not be used for diagnosis and treatment, nor should it be considered a replacement for counsel with your physician or other health care professional. If you have questions or concerns about your health, contact your health care provider. We encourage you to use the Physician Finder to locate a physician by specialty and area.</p>
    <p>While we and our subsidiaries make reasonable effort to ensure accuracy of the information on the Services, we do not guarantee the accuracy, and the information is provided with no warranty or guarantee of any kind.</p>
    </li>
    <li><strong>Links</strong>
    <p>We and our subsidiaries provide links to other websites. These links are provided as a convenience to you and as an additional avenue of access to the information contained on such third-party websites. Different terms and conditions may apply to your use of any linked sites. We encourage you to read the privacy policy of each website.&nbsp; We have no control over third party websites and make no claim or representation regarding such websites. We accept no responsibility for, the quality, content, nature, or reliability of any websites accessible by hyperlink from the Services, or websites linking to the Services.&nbsp; We are not responsible for any losses, damages or other liabilities incurred as a result of your use of any linked sites.</p>
    </li>
    <li><strong>Corrections, unsubscribe and data retention</strong>
    <p>You may update the information you provide to us through the Services. To change your information, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>. If you are subscribed to one of our newsletter, you may unsubscribe from receiving the newsletters by clicking on the appropriate link at the bottom of the e-mail. We may, from time to time, send you administrative messages. You cannot opt-out from receiving administrative messages.</p>
    <p>We will retain your information for as long as needed to provide you services, comply with our legal obligations, resolve disputes, and enforce our agreements. Except for authorized law enforcement investigations, other valid legal processes or as described in this Policy (or another policy in place between you and us), we will not share any Personally Identifiable Information we receive from you with any parties outside of McLaren and its subsidiaries.</p>
    </li>
    <li><strong>Changes to the Privacy Policy and Terms</strong>
    <p>We may change this Policy from time to time. Please take a look at the “Last Modified” legend at the top of this page to see when this Policy was last revised. Any material changes to this Policy will become effective 7 days from when we post the revised Policy on the Services and all other changes to this Policy will become effective when we post the revised Policy on the Services. Your use of the Services following the effective date means that you accept the revised Policy. If you have questions or comments about this Policy, contact&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong>.</p>
    <p><a href="/main/foundation.aspx"><strong>McLaren Foundations:</strong></a>&nbsp; Some subsidiaries uses Internet donors' technology provided by Blackbaud, Inc. This is encrypted technology and allows financial donations to be forwarded electronically using a secure server.</p>
    <p>McLaren Health Care Corporation and its subsidiaries use oxcyon.com as a host server and vendor, the content management system is Central Point.</p>
    </li>
    <li><strong>Use of Services by Minors</strong>
    <p>We do not knowingly collect Personally Identifiable Information from individuals under the age of 13 and the Services are not directed to individuals under the age of 13. We request that these individuals not to use the Services.</p>
    </li>
    <li><strong>International Visitors</strong>
    <p>The Services are controlled and operated from the United States, and are not intended to subject us to the laws or jurisdiction of any state, country or territory other than that of the United States. If any material on the Services, is contrary to the laws of the place where you are when you access them, then we ask you not to use the Services. You are responsible for informing yourself of the laws of your jurisdiction and complying with them. By using the Services, you consent to the transfer of information to the United States, which may have different data protection rules than those of your country.</p>
    </li>
    <li><strong>Use of Materials on the Services, Trademarks and Copyrights </strong>
    <p>You acknowledge and agree that all content on the Services (including, without limitation, text, images, user interfaces, visual interfaces, graphics, trademarks, logos, sounds, source code and computer code, including but not limited to the design, structure, selection, coordination, expression, ‘look and feel’ and arrangement thereof) is the exclusive property of and owned by us or our licensors and is protected by copyright, trademark, trade dress and various other intellectual property rights and unfair competition laws. These marks and copyrights may not be copied, imitated, or used, in whole or in part, without the express prior written permission from their respective owners, and then with the proper acknowledgments. Nothing on the Services shall be construed as granting, by implication, estoppel, or otherwise, any license or right to use any trademark, logo or service mark displayed on the Services without the owner’s prior written permission, except as otherwise described herein.</p>
    <p>You may access, copy, download, and print the material (such as, for example, educational materials, service descriptions, and similar materials) purposely made available by us for downloading from the non-secured component of the Website (“General Website”) for your personal, non-commercial use and for your business use in connection with evaluating McLaren healthcare provider services for offer by your health plan or use by your employees, provided you do not (i) modify or delete (including through selectively copying or printing material) any copyright, trademark, or other proprietary notice that appears on the material, and (ii) make any additional representations or warranties relating to such materials.</p>
    <p>You may access, copy, and print the material contained in the secured component of the Website in accordance with the terms and conditions governing the secure section.</p>
    <p>Any other use of content or material on the Services, including but not limited to the modification, distribution, transmission, performance, broadcast, publication, uploading, licensing, reverse engineering, encoding, transfer or sale of, or the creation of derivative works from, any material, information, software, documentation, products or services obtained from the Services, or use of the Services is expressly prohibited.</p>
    <p>We, our licensors, or content providers, retain full and complete title to any and all materials provided on the Services, including any and all associated intellectual property rights. </p>
    <p>As long as you comply with this Policy, we grant you a personal, non-exclusive, non-transferable, revocable, limited privilege to enter and use the Services. We reserve the right, without notice and in our sole discretion, to terminate your license to use the Services and to block or prevent future access to and use of the Services.</p>
    </li>
    <li><strong>Submissions and Postings</strong>
    <p>To the extent that we allow submissions on the Services, you acknowledge that you are responsible for any material you may submit via the Services, including the copyright, legality, reliability, appropriateness, and originality of any such material.</p>
    <p>You represent and warrant (and we rely on your representation and warranty) that you (i) own or otherwise control all the rights or have sufficient rights to the content you post or that such items are known to you to be in the public domain; (ii) that the content is accurate; (iii) that use of the content you supply does not violate any provision in this Policy or terms you may have agreed to with a third party; (iv) that the content is not defamatory or otherwise trade libelous; (v) does not violate any law, statute, ordinance or regulation; and (vi) that you will indemnify us for all claims resulting from content you supply, including arising from an action alleging infringement of copyright or other proprietary rights in such work.</p>
    <p>We undertake no duty to determine the validity of any claim of copyright or trademark infringement. Upon receiving written notice that any item posted on the Services is believed to infringe a copyright or other proprietary right, we will remove said work.</p>
    <p>If you do submit material, you grant us and our affiliates an unrestricted, nonexclusive, royalty-free, perpetual, irrevocable, transferable and fully sublicensable right to use, reproduce, modify, adapt, publish, translate, create derivative works from, distribute and display any and all material not subject to protections under HIPAA throughout the world in any media. You further agree that we are free to use without limitation and without any compensation to you any ideas, concepts, or know-how that you or individuals acting on your behalf provide to us. You grant us the right to use the name you submit in connection with such material.&nbsp; We retain any and all rights granted in this Policy in and to any user submitted content or non-HIPAA materials after termination, notwithstanding the reason for any such termination.</p>
    <p>We have an absolute right to remove any material from the Services in our sole discretion at any time.</p>
    </li>
    <li><strong>Usage Rules</strong>
    <p>You hereby agree to not upload, distribute, or otherwise publish through the Services any content that (i) is unlawful, libelous, defamatory, obscene, pornographic, harassing, threatening, invasive of privacy or publicity rights, fraudulent, defamatory, abusive, inflammatory, or otherwise objectionable; (ii) is confidential, proprietary, incorrect, or infringing on intellectual property rights; (iii) may constitute or encourage a criminal offense, violate the rights of any party or otherwise give rise to liability or violate any law; or (iv) may contain software viruses, chain letters, mass mailings, or any form of “spam.”&nbsp; You may not use a false email address or other identifying information, impersonate any person or entity or otherwise mislead as to the origin of any content.&nbsp; You may not upload commercial content onto our Services.</p>
    <p>You expressly agree to refrain from doing either personally or through an agent, any of the following:&nbsp; (1) use any device or other means to harvest information; (2) transmit, install, upload or otherwise transfer any virus or other item or process to the Services that in any way affects the use, enjoyment or service of the Services, or any visitor’s computer or other medium used to access the Services; (3) engage in any action which we determine in our sole discretion is detrimental to the use and enjoyment of the Services; or (4) transmit, install, upload, post or otherwise transfer any information in violation of the laws of the United States. You may further not use any hardware or software intended to damage or interfere with the proper working of the Services or to surreptitiously intercept any system, data, or personal information from the Services. You agree not to interrupt or attempt to interrupt the operation of the Services in any way.</p>
    </li>
    <li><strong>Infringement Notice</strong>
    <p>We respect the intellectual property rights of others and request that you do the same. If you believe your copyright or the copyright of a person on whose behalf you are authorized to act has been infringed, you may notify us in writing to the e-mail address or mailing address provided in the “How to Contact the Web Team” section below with attention to Copyright Agent. To be effective, your notification must be in writing, include your contact information, provided to our copyright agent, and include:&nbsp; (i) signature of a person authorized to act; (ii) identification of the copyrighted work claimed to have been infringed; and (ii) identification of the material that is claimed to be infringing including references to the location of the material on the Services.</p>
    <p>If you believe other intellectual property rights were violated, you may notify us in writing to the mailing address provided in the “How to Contact the Web Team” section below with attention to General Counsel.</p>
    </li>
    <li><strong>Jurisdiction and Applicable Law</strong>
    <p><strong>The laws of the State of Michigan, </strong>without regard to any conflicts of laws principles thereof,<strong> will govern the construction and interpretation of this Policy and the rights of the parties hereunder. By accessing, using, or registering for the Services, you acknowledge that you have read, understood, and agreed to be bound by this Policy and by all applicable laws and regulations. </strong>The Parties agree on behalf of themselves and any person claiming by or through them that the exclusive jurisdiction and venue for any action or proceeding arising out of or relating to this Agreement will be an appropriate state or federal court located in Michigan and each party irrevocably waives, to the fullest extent allowed by applicable law, the defense of an inconvenient forum.<strong></strong></p>
    </li>
    <li><strong>Severability and Waiver</strong>
    <p>Our failure to exercise or enforce any right or provision of this Policy will not constitute a waiver of such right or provision. If any provision of this Policy is unlawful, void, or unenforceable, for any reason, the remaining provisions will remain in full force and effect to the fullest extent of the law.<strong></strong></p>
    </li>
    <li><strong>How to Contact the Web Team</strong> E-mail:&nbsp;<strong><a href="mailto:Webmaster@mclaren.org">Webmaster@mclaren.org</a></strong> Fax:&nbsp;(989) 891-8185</li>
</ol>
<ul>
    <li><strong>We respect your privacy and do not collect Personally Identifiable Information through the Services unless you choose to provide it. We, and our service providers, may collect Personally Identifiable Information in a variety of ways, including when</strong>:
    <ul>
        <li><a>You contact us to request information by sending us an </a><a href="mailto:Webmaster@mclaren.org">e-mail</a>&nbsp;; </li>
        <li>You participate in one of our training or education events, such as a childbirth education, nutrition education or exercise &nbsp;training seminars;</li>
        <li>You request one of our publications or newsletters;</li>
        <li>You participate in a Service related survey, contest, or other promotion; or</li>
        <li>You complete a questionnaire(s) on the Services (<em>e.g.</em>, did you find our website helpful?)</li>
    </ul>
    </li>
    <li><strong>We may use personally identifiable information</strong>:
    <ul>
        <li>To send administrative information, such as information regarding the Services and changes to our terms, conditions, or policies;</li>
        <li>To respond to your inquiries and fulfill your requests;</li>
        <li>If you enter into a contest or similar promotion we may use the information you provide to administer those programs;</li>
        <li>We may use survey information for research and quality improvement purposes, including helping us to improve information and services offered through the Services;</li>
        <li>For our business purposes, such as improving or modifying our Services, identifying usage trends, and operating and expanding our service and information offerings;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>Your Personally Identifiable Information may be disclosed</strong>:
    <ul>
        <li>To identify you to anyone to whom you send messages through the Services;</li>
        <li>To our third-party service providers that provide services such as website hosting, information technology and related infrastructure, customer service, and other similar services;</li>
        <li>To a third-party in the event of any reorganization, merger, sale, joint venture, assignment, transfer or other disposition of all or any portion of our business or assets;</li>
        <li>As we believe to be necessary or appropriate: (a) under applicable law; (b) to comply with legal process; (c) to respond to requests from public and government authorities; (d) to enforce our terms and conditions; (e) to protect our operations against, for example, security threats, fraud or other malicious activity; (f) to protect our rights, privacy, safety or property, and/or that of you or others using the Services; and (g) to allow us to pursue available remedies or limit the damages that we may sustain.</li>
    </ul>
    </li>
    <li><strong>California Do Not Track Notice</strong>:
    <ul>
        <li>We do not track you over time and across third party websites to provide targeted advertising and therefore do not respond to Do Not Track (DNT) signals;</li>
        <li>Third parties that have content embedded on our website, such as a social networking connectors (<em>e.g.</em>, Facebook, Twitter) set cookies in your browser as well as obtain information about the fact that a web browser visited the Services from a certain IP address.</li>
    </ul>
    </li>
</ul>
<ul>
    <li><strong>How We May Collect Other Information</strong>: We, and our third-party service providers, may collect Other Information in a variety of ways, including:
    <ul>
        <li><em>Through your browser or mobile device</em>: Certain information is automatically collected by most browsers or through your mobile device, such as your computer type, screen resolution, operating system name and version, device manufacturer and model, language, and Internet browser type and version. We may also collect information on the search terms you used to find our website, the search engine you used, or the address of the web site from which you came to visit.</li>
        <li><em>Using cookies</em>: Our Services may use cookies and other technologies such as pixel tags and web beacons. These technologies help us provide better services to you, tell us which parts of our Services people have visited, and allow us to better measure the usability of our Services. We treat information collected by cookies and other technologies as non-personal information. You have a variety of tools to control cookies and similar technologies, including controls in your browser to block and delete cookies.</li>
        <li><em>IP Address</em>:&nbsp; Your “IP Address” is a number that is automatically assigned to the computer that you are using by your Internet service provider (ISP). An IP Address may be identified and logged automatically in our server log files, or those of our website hosting vendor, whenever a user accesses the Services, along with the time of the visit and the page(s) that were visited.</li>
        <li><em>By aggregating information</em>:&nbsp; Aggregated Personally Identifiable Information does not personally identify you or any other user of the Services. We may aggregate information for a variety of reasons, for example, to calculate the percentage of our users who visit a particular page or clicked on a particular item from a newsletter.</li>
        <li><em>Google Analytics</em>: We use a service from Google called “Google Analytics” to collect information about use of the Services. Google Analytics collects information such as how often users visit this site, what pages they visit when they do so, and what other sites they used prior to coming to this site. Google Analytics collects only the IP address assigned to you on the date you visit this site, rather than your name or other identifying information. Google’s ability to use and share information collected by Google Analytics about your visits to our Services is restricted by the Google Analytics Terms of Use and the Google Privacy Policy. We may use the information from Google Analytics for a variety of reasons including trend analysis and to make our Services more useful to the communities we serve.</li>
    </ul>
    </li>
    <li><strong>How We May Use and Disclose Other Information</strong>
    <ul>
        <li>We may use and disclose Other Information for any purpose, except where we are required to do otherwise under applicable law.</li>
        <li>If we are required to treat Other Information, such as IP addresses or other similar identifiers, as Personally Identifiable Information under applicable law, then we may use it as described in “How We May Collect Other Information” section above, as well as for all the purposes for which we use and disclose Personally Identifiable Information, but we will treat these identifiers as Personally Identifiable Information. If we are require to treat Other Information as protected health information as defined by HIPAA, then we will treat the identifiers in accordance with our Notice of Privacy Practices.</li>
    </ul>
    </li>
</ul>
<p><strong>Mail:</strong><strong><br>
</strong>McLaren Bay Region<br>
Corporate WebMaster<br>
1900 Columbus Avenue<br>
Bay City, MI 48708</p>
Last modified:  5/2/16
<!-----Related Links------>


<div class="subpage-info">
<!----Related Photo Gallery---->
<!----Related Documents---->
<!----Related Videos------->
<!----Related Links-------->
<!----Related Blogs-------->
</div>
</div>
<!-----------------Find Here----------------->
<!--cpsys_Template:DetailsHeaderContent-->


		
	

<!--cpsys_Template:DetailsFooterContent-->

<!--cpsys_Template:DetailsFooterContent-->


		
	


</div>
					
				</div>
			
			</div>
			<div id="cphBody_tdRight" class="cpsys_BlockColumn cpsty_RightTd">
				
				<div class="cpsty_Right">
					
					
					<div id="cphBody_divRightAc2" class="cpsty_SiteTypes_Default_RightAc2"><div id="cpsys_Advertisers_f3226af2-3ced-4f64-a3ab-844e6284e705" style="text-align:left;">
	<table cellspacing="0" cellpadding="0" class="cpsys_Table" style="height: 400px;">
    <tbody>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
</div></div>
				</div>
			
			</div>
		
		</div>
	</div>
	
</div>
	</div>
			
		
</div>
    </div>
    
	<footer>
<div class="footer-social-outer-container">
<div class="footer-social-inner-container">
<ul>
    <li><a href="https://www.facebook.com/McLarenHealth"><img style="" alt="facebook icon" src="/Uploads/Public/Images/Designs/footer-facebook.gif"></a></li>
    <li><a href="https://twitter.com/mclarenhealth"><img style="" alt="twitter icon" src="/Uploads/Public/Images/Designs/footer-twitter.gif"></a></li>
    <li><a href="https://www.linkedin.com/company/mclaren-health-care"><img style="" alt="linkedin icon" src="/Uploads/Public/Images/Designs/footer-linkedin.gif"></a></li>
    <li><a href="https://www.youtube.com/user/mclarenhealth"><img style="" alt="youtube icon" src="/Uploads/Public/Images/Designs/footer-social-red.gif"></a></li>
    
    
</ul>
</div>
</div>
<div class="outer-container">
<div class="inner-container mobile-hide">
<nav class="btm">
<div class="col-1">
<ul>
    <li><a href="/Main/Career.aspx">Careers</a></li>
    <li><a href="/Main/mclaren-contact-us.aspx">Contact Us</a></li>
    <li><a href="/Main/mclaren-web-privacy-policy.aspx">Website Privacy Policy</a></li>
    <li><a href="/Main/health-community-needs.aspx">Community Health Needs Assessment</a></li>
    <li><a href="/Main/Sitemap.aspx">Site Map</a></li>
</ul>
</div>
<div class="col-2"><img src="/Uploads/Public/Images/Designs/FooterLogos/footerlogowhite_McLarenHealthCare.png"><br>
<br>
McLaren Health Care<br>
<br>
Flint, MI 48532<br>
<span style="font-size: 30px;"></span></div>
<div class="col-3">
<ul>
    <li><a href="/Main/employee.aspx">For Employees</a></li>
    <li><a href="/Main/media1.aspx">For Media</a></li>
    <li><a href="/Main/physician.aspx">For Physicians</a></li>
    <li><a href="/Main/vendor-registration.aspx">For Vendors</a></li>
    <li><a href="/Main/volunteer.aspx">For Volunteers</a></li>
    <li><a href="/Main/gme.aspx">For Medical Education</a></li>
    <!------------- main, bayregion, macomb, oakland, flint, lansing ---------GME Link-------->
</ul>
</div>
</nav>
</div>
<div class="btm-ribbon">
<div class="inner-container">
<p>McLaren Health Care, through its subsidiaries, will be the best value in health care as defined by quality outcomes and cost.</p>
<p>©All rights reserved.  McLaren Health Care and/or its related entity</p>
<div style="clear:both;"></div>
</div>
</div>
</div>
</footer>
<script>
// Left Navigation Adjustments
if ($('.cpsty_Navigation').length > 0) {
	$('.cpsty_NavigationSelected:first').prevAll().remove(); // Find 1st selected item & remove everything above it.
	$('.cpsty_SubNavigation1Selected').prevUntil('.cpsty_NavigationSelected').remove(); // Find 2nd Selected Item (Bariatric Services).  Remove Everything Above Until Selected Parent (Services)
	$('.cpsty_SubNavigation1').not(':first').prev('.cpsty_SubNavigation2:visible').nextAll().remove(); // Find Last Item in Section & Remove the rest
}
</script>
    
	<div class="dv-bottom-edit-liks">
	
	
	
	
	
	</div>
	<div id="uprgUpdateProgress" style="display:none;">
					<table border="0" cellpadding="0" cellspacing="0" class="updateProgress" style="position:fixed; top:0px; right:0px; border: solid 1px #CCCCCC; background-color:#F2F2F2;">
				<tr>
				<td style="vertical-align: middle; padding: 2px;"><img src="/Integrations/Centralpoint/Resources/ProgressIcon.gif" alt="Loading..." /></td>
				<td style="vertical-align: middle; padding: 2px;">Loading...</td>
				</tr>
				</table>
			
</div>
    <input type="hidden" name="ctl00$ctl00$FormAction" id="FormAction" value="0" /><input type="hidden" name="ctl00$ctl00$FormGroup" id="FormGroup" /><input type="hidden" name="ctl00$ctl00$FormButton" id="FormButton" />

<script type="text/javascript">
//<![CDATA[

 //Admin > Properties: HeaderStartupScripts 
$('table#DynamicNavigation1 img[src="/Uploads/Public/Images/arrow.png"]').attr('alt', 'arrow');

 //End of Admin > Properties: HeaderStartupScripts 
$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show().siblings().show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('.liParent a[href*="' + window.location.pathname + '"]').parents('.liParent').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().addClass('useClick').show();
$('.liParent a[href*="' + window.location.pathname + '"]').parent().siblings().show();

$( "#site-nav > li > ul > li" ).has( "ul" ).addClass( "full" );
$( "#site-nav > li > ul > li > ul > li" ).has( "ul" ).addClass( "full2" );$('#cpPortableNavigation73').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation73').unbind('mouseleave');
});
$('#cpPortableNavigation73 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation73').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation85').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation85').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation85').unbind('mouseleave');
});
$('#cpPortableNavigation85 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation85').mouseleave(function() {
$(this).hide();
});
});
$('#cpPortableNavigation77').mouseenter(function() {
$(this).show();
});
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
$('div.cpsys_MobileNavigation_Menu select').mouseenter(function() {
$('#cpPortableNavigation77').unbind('mouseleave');
});
$('#cpPortableNavigation77 select').mouseleave(function(event) {
event.stopPropagation();
$('#cpPortableNavigation77').mouseleave(function() {
$(this).hide();
});
});

                    $(document).ready(function() {
                    $('.mobile-navigation-accordion').cp_Accordion({collapsable : true, active : -1, headerClass : 'mobile-navigation-accordion', contentClass : 'mobile-navigation-content', event : 'click', expandedSpanText : '', collapsedSpanText : '', expandCollapsePosition : 'left', expandedHeaderText : '', collapsedHeaderText : '' });
                    });
					if ($('input[name="HtmlSearchCriteria"]').length > 0) { 
						$('input[name="HtmlSearchCriteria"]').autocomplete({
							source: function(request, response) {
								$.ajax({
									type: "POST",
									url: "/WebServices/ClientMethods.asmx/SiteSearchAutoComplete_v2",
									data: "{ \"term\": \"" + request.term + "\", \"lookupType\": \"StartsWith\"}",								
									dataType: "json",
									contentType: "application/json; charset=utf-8",
									success: function(data) { response(data.d) },
									error: function(XMLHttpRequest, textStatus, errorThrown) { /*alert(textStatus + '\n' + errorThrown);*/ }
								});
							},
							select: function( event, ui ) {
								$('input[name="HtmlSearchCriteria"]').val(ui.item.value);
								$('input[name="HtmlSearchCriteria"]').siblings('input[type="button"], input[type="submit"]').click();
							},
							autoFocus: true,
							delay: 10,
							minLength: 2
						});
					}
					//]]>
</script>
</form>
</body>
</html>
