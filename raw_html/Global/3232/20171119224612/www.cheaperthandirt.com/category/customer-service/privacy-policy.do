



<!DOCTYPE html>
<html lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>
            Privacy Policy-Customer Service - Cheaper Than Dirt</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta charset="utf-8" /> 

<meta name="description" content="" />
				<meta name="keywords" content="" />
    <link rel="alternate" hreflang="en-US" href="https://www.cheaperthandirt.com&#47;category&#47;customer-service&#47;privacy-policy.do" />
    <link rel="alternate" hreflang="x-default" href="https://www.cheaperthandirt.com&#47;category&#47;customer-service&#47;privacy-policy.do" />
    <link rel='stylesheet' type='text/css' href='/wro/32b053e35d7d68b405c61a7d97ddf28/HEAD-CATEGORY-cheaperthandirt.css?minimize=true'><link rel='stylesheet' type='text/css' href='/wro/df1cf53a1562863384c1e07daf9aedd5/SITE-cheaperthandirt.css?minimize=true'><link rel='stylesheet' type='text/css' href='/text/content/global/overrides.css'><script src='/wro/7dcd30b529fcafdab40f55072aabc6b/HEAD.js?minimize=true'></script>

<script>

</script>
<!-- Google Analytics Reporting Support -->
<!-- Google Analytics More info available at http://www.google.com/analytics/index.html -->

<script type="text/javascript">
    window.MarketLive = window.MarketLive || {};
    MarketLive.Reporting = MarketLive.Reporting || {};
    MarketLive.Reporting.googleAnalyticsEnabled = true;
    MarketLive.Reporting.delayBeforeLeavingCurrentPage = 200; // in millisecond

    // From 14.2, we support Universal Analytics Enhanced Ecommerce
    MarketLive.Reporting.googleAnalyticsEnhancedEcommerceEnabled = true;

    MarketLive.Reporting.googleAnalyticsEnhancedEcommerceMaxItemsPerHit = 20;

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ml_ga');
    
            ml_ga('create', 'UA-172267-1', {'cookieDomain': '.cheaperthandirt.com'});
        

    // Note that by the time of 5.9.8, Universal Analytics does not support Enhanced Link Attribution.
    // In 14.1, Universal Analytics is out of Beta and Enhanced Link Attribution is now supported.
    // https://support.google.com/analytics/answer/2558867?hl=en
    ml_ga('require', 'linkid', 'linkid.js');
    ml_ga('require', 'displayfeatures');

    
            // From 14.2, we support Universal Analytics Enhanced Ecommerce
            ml_ga('require', 'ec');
        

    ml_ga('send', 'pageview');

</script><script type='text/javascript' src='//3237090-prod.rfksrv.com/rfk/js/11243-3237090/init.js'></script><link href='//fonts.googleapis.com/css?family=Raleway:700,800,900' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href='//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
<!-- CHTD-151 : Social Annex - Facebook not working
<script>
	(function(d){
		var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
		js = d.createElement('script'); js.id = id; js.async = true;
		js.src = "//connect.facebook.net/en_US/all.js";
		d.getElementsByTagName('head')[0].appendChild(js);
	}(document));
</script>
 -->
<!-- SEO tags for CHTD-352 : Start -->

<link rel="canonical" href="https://www.cheaperthandirt.com/category/customer-service/privacy-policy.do"/>
					 <!-- SEO tags for CHTD-352 : End -->

<!-- Begin Navigation CSS Override -->
<link href="/text/navigation/navigation.css" type="text/css" rel="stylesheet">
<!-- End Navigation CSS Override -->

<!-- CDUA-432 -->
<script type="text/javascript">	
	var triggermail = triggermail || [];
	triggermail.load = function(a) { 
		var b = document.createElement("script");
		b.type = "text/javascript";
		b.async = !0;
		b.src = ("https:" === document.location.protocol ? "https://" : "http://") + "triggeredmail.appspot.com/triggermail.js/" + a + ".js";
		a = document.getElementsByTagName("script")[0];
		a.parentNode.insertBefore(b,a)
	};
	triggermail.load("xxx_xxx");
	window.triggermail = triggermail;	
</script>	  


<script>
	!function(f,b,e,v,n,t,s){
		if(f.fbq)
			return;
		n=f.fbq=function() {
			n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)
		};
		if(!f._fbq)f._fbq=n;
		n.push=n;
		n.loaded=!0;
		n.version='2.0';
		n.queue=[];
		t=b.createElement(e);
		t.async=!0;
		t.src=v;
		s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)
	} (window, document,'script','//connect.facebook.net/en_US/fbevents.js');
	fbq('init', '520255291469580');
	fbq('track', "PageView");
</script>
<noscript>
	<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=520255291469580&ev=PageView&noscript=1"/>
</noscript>



<!-- SP-2016-23936 - Add Emarsys Pixel to every page -->
<script type="text/javascript">
var ScarabQueue = ScarabQueue || [];
(function(subdomain, id) {
if (document.getElementById(id)) return;
var js = document.createElement('script'); js.id = id;
js.src = subdomain + '.scarabresearch.com/js/18D192BA54D9FF25/scarab-v2.js';
var fs = document.getElementsByTagName('script')[0];
fs.parentNode.insertBefore(js, fs);
})('https:' == document.location.protocol ? 'https://recommender' : 'http://cdn', 'scarab-js-api');
</script>
<!-- SP-2016-23936 -->

<script type="text/javascript" >
	ScarabQueue.push(['cart', []]);
</script>
<script>
	var showDotNetPopup=false;
</script>
<link rel="shortcut icon" href="/images//cheaperthandirt/en_us/favicon.ico" type="image/vnd.microsoft.icon">
	<link href='//fonts.googleapis.com/css?family=Raleway:400,700,800,900' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <!-- ExactTarget Pixels -->
	<script type="text/javascript" src="https://6271404.collect.igodigital.com/collect.js"> </script>
</head>

<body class="ml-layout-section-category">

<a name="top"></a>
<div id="ml-modal-placement"></div>

<header role="banner" >
    <nav role="navigation">
        <script>MarketLive.Nav.TopNav.initialize({minKeywordLength:3, maxKeywordLength:500, msgSearch:"Search for a product", msgNoSearchTerm:"Please enter a keyword or item number", msgShortSearchTerm:"Your keyword or item number must be at least 3 characters long"});</script><span class="ml-icon-lib ml-icon-home ml-icon-pre-load-fix"></span>

<div id="mlLiteLoginApp">
    <div class="modal fade" data-ng-controller="mlLiteLoginCtrl" id="mlLiteRegistrationDialog" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="ml-lite-registration-dialog">
                <span id="ml-lite-registration-resources">
    <span data-ml-rk="lbl.basket.liteRegistration.login">Login</span>
    <span data-ml-rk="lbl.basket.liteRegistration.guest">New Shopper</span>
    <span data-ml-rk="lbl.basket.liteRegistration.guestCheckout">Checkout as guest</span>
    <span data-ml-rk="btn.literegistration.login">Login</span>
    <span data-ml-rk="btn.literegistration.register">Register</span>
    <span data-ml-rk="lnk.literegistration.saveCart">Save Cart for Any Device</span>
    <span data-ml-rk="btn.globalCart.saveCart">Save Cart</span>
    <span data-ml-rk="btn.altTxt.cancel">Cancel</span>
</span>

<div class="ml-lite-registration">
<div class="ml-lite-registration-underlay"></div>
<div class="ml-lite-registration-wrapper">
    <div class="toggle-options">
        <a href="javascript:void(0);" class="toggle-option active" tabindex="0" onclick="MarketLive.LiteRegistration.showHideSection('login');">
            <span id="login" class="toggle-button" >Login</span>
            <div class="arrow-wrapper"><div class="arrow"></div></div>
        </a>

        <a href="javascript:void(0);" class="toggle-option" data-ng-show="model.show.newShopper" tabindex="0" onclick="MarketLive.LiteRegistration.showHideSection('guest');">
            <span id="guest" class="toggle-button">New Shopper</span>
            <div class="arrow-wrapper"><div class="arrow"></div></div>
        </a>

        <a href="javascript:void(0);" class="toggle-option" data-ng-show="model.show.guestCheckout" tabindex="0" onclick="MarketLive.LiteRegistration.checkoutGuest();">
                        <span id="checkoutSoftLoggedTab" class="toggle-button" >Checkout as guest</span>
                        <div class="arrow-wrapper"><div class="arrow"></div></div>
                    </a>
                </div>

    <div class="section-wrapper">

        <div id="socialLoginVerificationMsgContainer" class="ml-sociallogin" style="display:none">
    <div class="default returningAccountWelcome">Welcome back! We have noticed you already have an account.</div>

    <div class="accountVerificationInfo">
        <div class="col1">
            <img src="/images//cheaperthandirt/en_us/global/globalgraphics/info_icon.png" alt="info" />
            <div></div>
        </div>
        <div class="col2">
            <div id="returningAccountInfo1" class="info">
                We have found a Cheaper Than Dirt account that matches your  email address.</div>
            <div class="passwordRequired info">Enter your Cheaper Than Dirt password to connect.</div>
        </div>
    </div>
</div>
<div id="socialLoginSection" class="ml-sociallogin">
        <div  id="show_provider">    </div>
            <div class="dottedBorder"></div>

        <form method="post" action="/sociallogin.do" id="socialLoginUserDetails" data-ajax="false">
        <input type="hidden" id="slMethod" name="method" value="submit"/>
        <input type="hidden" id="slFromPage" name="fromPage" value="LITEREGISTRATION"/>
        <input type="hidden" id="uid" name="uid" value=""/>
        <input type="hidden" id="siteUid" name="siteUid" value=""/>
        <input type="hidden" id="saToken" name="saToken" value=""/>
        <input type="hidden" id="saKey" name="saKey" value=""/>
        <input type="hidden" id="slFirstName" name="firstName" value=""/>
        <input type="hidden" id="slLastName" name="lastName" value=""/>
        <input type="hidden" id="slEmail" name="email" value=""/>
        <input type="hidden" id="slUserName" name="userName" value=""/>
        <input type="hidden" id="slAddress" name="address" value=""/>
        <input type="hidden" id="slCity" name="city" value=""/>
        <input type="hidden" id="slPhoneNumber" name="phoneNumber" value=""/>

        <input type="hidden" id="slAboutMe" name="aboutMe" value=""/>
        <input type="hidden" id="slBirthDate" name="birthDate" value=""/>
        <input type="hidden" id="slBooks" name="books" value=""/>
        <input type="hidden" id="slFollowersCount" name="followersCount" value=""/>
        <input type="hidden" id="slFriendsCount" name="friendsCount" value=""/>
        <input type="hidden" id="slGender" name="gender" value=""/>
        <input type="hidden" id="slImageUrl" name="imageUrl" value=""/>
        <input type="hidden" id="slInterests" name="interests" value=""/>
        <input type="hidden" id="slLanguages" name="languages" value=""/>
        <input type="hidden" id="slLikes" name="likes" value=""/>
        <input type="hidden" id="slMovies" name="movies" value=""/>
        <input type="hidden" id="slMusic" name="music" value=""/>
        <input type="hidden" id="slOrganization" name="organization" value=""/>
        <input type="hidden" id="slPhotos" name="photos" value=""/>
        <input type="hidden" id="slPoliticalView" name="politicalView" value=""/>
        <input type="hidden" id="slPosition" name="position" value=""/>
        <input type="hidden" id="slProfileUrl" name="profileUrl" value=""/>
        <input type="hidden" id="slProviders" name="providers" value=""/>
        <input type="hidden" id="slRelationStatus" name="relationStatus" value=""/>
        <input type="hidden" id="slReligion" name="religion" value=""/>
        <input type="hidden" id="slStatus" name="status" value=""/>
        <input type="hidden" id="slTimeZone" name="timeZone" value=""/>
        <input type="hidden" id="slTvShows" name="tvShows" value=""/>
        <button type="submit" disabled="disabled" class="ml-invisible" value="Submit">Submit</button>
    </form>

    </div>
<div id="guestSection" class="sub-section-wrapper">
            <script type='text/javascript'>
				fbq('track', 'Lead');
			</script><form id="accountSetupForm" action="/checkout/literegistration.do?method=submitRegister" method="post" onsubmit="return false">
                <input id="socialLoginLinkAccountsRequired" name="socialLoginLinkAccountsRequired" type="hidden" value="false">

                <div id="accountsetupfailerror"></div>

                <div class="section-description">Register with an email address and password.</div>

                <label class="visuallyhidden-with-placeholder" for="newCustomerEmail">Email Address</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="ml-icon-lib ml-icon-envelope ml-icon-fw"></i></span>
                    <input type="email" name="newCustomerEmail" maxlength="100" value="" class="form-control" id="newCustomerEmail" placeholder="Email Address">
                </div>

                <label class="visuallyhidden-with-placeholder" for="loginPassword">Password</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="ml-icon-lib ml-icon-lock ml-icon-fw"></i></span>
                    <input type="password" name="loginPassword" maxlength="50" value="" class="form-control" id="loginPassword"  placeholder="Password"/>
                </div>

                <label class="visuallyhidden-with-placeholder" for="loginPasswordConfirm">Re-enter Password</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="ml-icon-lib ml-icon-lock ml-icon-fw"></i></span>
                    <input type="password" name="loginPasswordConfirm" maxlength="50" value="" class="form-control" id="loginPasswordConfirm"  placeholder="Re-enter Password"/>
                </div>

                <div class="input-group">
                        <span class="input-group-addon"><i class="ml-icon-lib ml-icon-question ml-icon-fw"></i></span>
                        <label for="liteRegistrationHint" class="ml-srOnly-content">Select a Recovery Question</label>
                        <select id="liteRegistrationHint" name="liteRegistrationHint" class="form-select">
                            <option value="">Select a recovery question</option>
                            <option value="1">What is your city of birth?</option>
                            <option value="2">What is your mother's maiden name?</option>
                            <option value="3">What is your father's first name?</option>
                            <option value="4">What is your pet's name?</option>
                            <option value="5">What is the year you were born?</option>
                            <option value="6">Please re-enter your email address:</option>
                            <option value="100001">What is your favorite color?</option>
                            </select>
                    </div>

                    <label class="visuallyhidden-with-placeholder" for="liteRegistrationAnswer">Enter Answer</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="ml-icon-lib ml-icon-comment ml-icon-fw"></i></span>
                        <input type="text" name="liteRegistrationAnswer" maxlength="100" value="" class="form-control" id="liteRegistrationAnswer" placeholder="Enter Answer">
                    </div>
                <div class="ml-lite-registration-signup">
                    <input TYPE="checkbox" NAME="emailSignup" id="emailSignup" >
                    <label for="emailSignup">Sign me up for Cheaper Than Dirt Email Updates about new services and special offers!</label>
                </div>
                <div class="ml-button-set">
                    <div><input name="Cancel" class="ml-cancel-btn" type="button" value="{{model.buttons.submitCancel}}" data-dismiss="modal" aria-label="Cancel"></div>
                    <div><input id="btnContinueAccountSetupForm" name="ContinueAccountSetupForm" class="ml-register-btn" type="submit" tabindex="0" aria-label="submit" value='{{model.buttons.submitRegister}}'></div>
                </div>
            </form>
        </div>
        <div id="loginSection" class="sub-section-wrapper">
            <div id="loginContainer">
                <form id="accountSetupFormLogin" action="/checkout/literegistration.do?method=submitLogin" method="post" onsubmit="return false">
                    <input id="socialLoginLinkAccountsRequiredForSubmitLogin" name="socialLoginLinkAccountsRequired" type="hidden" value="false">
                    <input id="socialLoginFromPage" name="socialLoginFromPage" value="LITEREGISTRATION" type="hidden">
                    <input id="mergeAccount" name="mergeAccount" value="{{model.fields.mergeAccount}}" type="hidden">

                    <div id="accountsetupfailerrorlogin"></div>

                    <div class="section-description" data-ng-hide="model.show.mergeMessages">Login with an email address and password.</div>

                    <label class="visuallyhidden-with-placeholder" for="loginEmail">Email Address</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="ml-icon-lib ml-icon-envelope ml-icon-fw"></i></span>
                        <input ng-readonly="model.show.mergeMessages" type="email" name="loginEmail" maxlength="100" value="" class="form-control" id="loginEmail" placeholder="Email Address">
                    </div>

                    <label class="visuallyhidden-with-placeholder" for="loginPassword2">Password</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="ml-icon-lib ml-icon-lock ml-icon-fw"></i></span>
                        <input type="password" name="loginPassword2" maxlength="50" value="" class="form-control" id="loginPassword2"  placeholder="Password"/>
                    </div>

                    <div>
                        <div class="ml-forgot-password"><a href="#" onclick="MarketLive.LiteRegistration.forgotPassword();">Forgot Password?</a></div>
                        <div class="ml-button-set">
                            <div>
                                        <label for="amazonCancelBtn" class="ml-srOnly-content">Cancel</label>
                                        <input name="Cancel" id="amazonCancelBtn" class="ml-cancel-btn" aria-label="Cancel" type="button" value="{{model.buttons.submitCancel}}" data-dismiss="modal"></div>
                                <div>
                               <label for="btnContinueAccountSetupFormLogin" class="ml-srOnly-content">Continue Account Setup</label> 
                            <input id="btnContinueAccountSetupFormLogin" name="ContinueAccountSetupForm" class="ml-login-btn" type="submit" tabindex="0" aria-label="submit" value='{{model.buttons.submitLogin}}'></div>
                        </div>
                    </div>
                </form>

                </div>

            <div id="socialLoginTerm" class="ml-sociallogin-term" style="display:none">
    <div id="returningAccountTermHeader" class="termHeader">
        Your personal Cheaper Than Dirt data will not be shared with </div>
    <ul>
    <li id="returningAccountTerm1" class="termItem">
        Cheaper Than Dirt <b><i>will not</i></b> share your account information with </li>
    <li id="returningAccountTerm2" class="termItem">
        Cheaper Than Dirt <b><i>will not</i></b> share your purchase history with </li>
    <li id="returningAccountTerm3" class="termItem">
        Cheaper Than Dirt <b><i>will not</i></b> attempt to contact your personal contacts on  </li>
    </ul>
</div>
</div>

        <div>
            <div class="ml-secure">
                <div class="secure-message-wrapper">
                    <span class="ml-icon-lib ml-icon-lock"></span>
                    <span>Your connection to this website is secure.</span>
                </div>
            </div>
        </div>
    </div>
</div>


</div>

</div>
        </div>
    </div>
</div>

<div id="globalHeaderInclude" class="ml-global-include ml-global-include-header collapse">
        <div class="container">
            <div class="ml-header-global-include-wrapper">
                <div class="ml-header-global-include">
                    <div class="ml-header-global-include-close"><button type="button" id="globalHeaderIncludeCloseBtn" class="ml-icon-lib ml-icon-close" data-toggle="collapse" data-target=".ml-global-include-header"><span class="hide">Close</span></button></div>
                    <!--
    Your Image goes here
  -->
        Share your latest AR build or photos from the range with #RangeDayFriday for a chance to win $500! <a href="/category/contest.do" title="#RangeDayFriday Contest from Cheaper Than Dirt">Get the details.</a>
</div>
            </div>
        </div>
    </div>
    <div id="ml-discount-threshold-messaging-container" class="ml-global-include" style="display:none;">
        <div class="container">
            <div class="ml-discount-threshold-messaging-row">
                <div class="ml-discount-threshold-messaging-wrapper">
                    <div class="ml-header-global-include-updown"><a href="javascript:void(0);"><span id="globalHeaderIncludeUpDownBtn" class="ml-icon-lib" data-toggle="collapse" data-target=".ml-global-include-header"></span><span class="hidden">Threshold Messaging</span></a></div>
                    <div class="ml-discount-threshold-messaging" style="display:none;">

    </div></div>
            </div>
        </div>
    </div>
    <div class="ml-header-wrapper">
    <div class="container">
        <div class="ml-header">
            <div class="ml-header-content-wrapper">
                <div class="ml-header-content">
                    <div class="ml-header-logo-wrapper">
                        <div class="ml-header-logo">
                            <a href="/home.do" aria-label="Cheaper Than Dirt">
                                    <span class="ml-dynamic-data" data-type="image" data-alt="Cheaper Than Dirt" data-mq-default="/images//cheaperthandirt/en_us/global/globalnav/logo01.png" data-mq-xs="/images//cheaperthandirt/en_us/global/globalnav/logo_xs.png" data-mq-md="/images//cheaperthandirt/en_us/global/globalnav/logo01.png"><noscript><img src="/images//cheaperthandirt/en_us/global/globalnav/logo01.png" alt="Cheaper Than Dirt"></noscript></span></a>
                            </div>
                    </div>
                    <div class="ml-ctd-custom-header hidden-xs">
                        <div class="ml-ctd-header-tagline hidden-sm">
                            America's Ultimate Shooting Sports Discounter
                        </div>
                        <div class="ml-ctd-custom-navigation">
                            <ul class="nav nav-pills">
                            	<li><a href="/category/free-shipping.do?sku_instock_b=true">Shop Free Shipping</a></li>
								<!-- <li><a href="/">Shop Everything</a></li> -->
                                <li><a href="/category/everything.do">Shop Everything</a></li>
						        <li><a href="/category/new-items.do?c=509903&sku_instock_b=true&pp=30&sortby=ourPicksAscend&cx=0">Shop New Arrivals</a></li>
                                </ul>
                        </div>
                        </div>
                    <div class="ml-header-search-wrapper">
                            <div class="ml-header-search">
                                <form name="searchForm" method="get" action="/search.do"
                                      onsubmit="document.searchForm.pageName.value=MarketLive.Base.getVariableValue('pageName');return MarketLive.Nav.TopNav.checkKeyword(this.keyword);">
                                        <input type="hidden" name="pageName" value="" />
                                    <div id="searchDiv">
                                      <label for="navsearchbox" class="ml-navsearchbox-label">Search</label>
                                    <input type="text" id="navsearchbox" name="keyword" class="ml-header-search-field form-control" placeholder="Search for a product" maxlength="500" data-toggle="dropdown" data-rfkid="rfkid_6">
                                        <!-- here is where autocomplete content is appended to -->
                                    </div>
                                    <div class="ml-header-search-btn-wrapper">
                                <button id="searchBtn" type="submit" class="ml-icon-lib ml-icon-search" aria-label="Search"</button></div>
                                    </form>
                            </div>
                        </div>
                    <div class="ml-header-links-wrapper">
                        <div class="ml-header-links">
                            <div class="ml-header-link ml-header-shop">
                                <div class="ml-header-link-item" data-toggle="collapse" data-target="#ml-navbar-collapse"> 
                                            <span class="ml-icon-lib ml-icon-bars"></span>
											<span>Shop</span>
                                        </div>
                                    </div>
							
                             <div class="liveChat ml-header-link pull-right">
								<script type="text/javascript"> 
									var bccbId = Math.random(); document.write(unescape('%3Cdiv id=' + bccbId + '%3E%3C/div%3E')); 
									window._bcvma = window._bcvma || []; 
									_bcvma.push(["setAccountID", "154870725323790052"]); 
									_bcvma.push(["setParameter", "WebsiteID", "1675787025551198501"]); 
									_bcvma.push(["addText", {type: "chat", department: "4049392923900479432", window: "2669693815149950084", available: "<i class='ml-icon-lib ml-icon-comment fa-flip-horizontal'></i>Live Chat", unavailable: "<i class='ml-icon-lib ml-icon-comment fa-flip-horizontal'></i>Live Chat", id: bccbId}]); 
									var bcLoad = function(){ 
									if(window.bcLoaded) return; window.bcLoaded = true; 
									var vms = document.createElement("script"); vms.type = "text/javascript"; vms.async = true; 
									vms.src = ('https:'==document.location.protocol?'https://':'http://') + "vmss.boldchat.com/aid/154870725323790052/bc.vms4/vms.js"; 
									var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(vms, s); 
									}; 
									if(window.pageViewer && pageViewer.load) pageViewer.load(); 
									else if(document.readyState=="complete") bcLoad(); 
									else if(window.addEventListener) window.addEventListener('load', bcLoad, false); 
									else window.attachEvent('onload', bcLoad); 
								</script>
							</div>
							
                            <div class="ml-header-link ml-header-account">
                                <div class="ml-header-link-item">
                                    <div id="fb-root"></div>
<span class="ml-topnav-identity">
    <span class="ml-topnav-identity-guest" onclick="$('.popover').popover('hide')" tabindex="0">
				<span class="ml-icon-lib ml-icon-user" aria-hidden="true"></span>
				<span class="ml-topnav-identity-link" data-from="IdentityGuest" data-url="/checkout/literegistration.do?liteRegistrationType=identityLogin">
                    Login<span class="ml-guest-dot">...</span>
                </span>
            </span>
        </span></div>
                            </div>

                            </div>
                    </div>
                    <div class="ml-header-global-cart-wrapper">
                        <div id="globalBasket" class="popDownWrapper globalCartWrapper ml-globalcart-container">
    <div class="popDownNav ml-header-global-cart">
        <a href="/basket.do?nav=%2Fcategory%2Fid%2F148594&amp;gc=1">
            <div class="ml-icon ml-icon-global-cart">
                <span class="ml-header-global-cart-count">0</span>
            </div>
            <div class="ml-header-global-cart-text">
                <span class="ml-header-global-cart-label">Cart</span>
                <span class="ml-header-global-cart-price">$0.00</span>
                </div>
        </a>
    </div>
    <div class="popDownLayer globalCartLayer popover bottom">
        <div class="arrow"></div>
        <div class="popover-content"> <div class="ml-discount-threshold-messaging" style="display:none;">

    </div><div class="globalCartEmpty" tabindex="0">
                    Your shopping cart is empty.</div>
            <div class="globalcart-carousel-wrapper" tabindex="0">
            <div class="globalCartLastItemAddedFillSlot ml-slot-item">
                &nbsp</div>
        </div>
        </div> </div>
</div></div>
                </div>
            </div>
        </div>
    </div>
</div></nav>
    <div><!--CatNav Start-->
<nav class="navbar navbar-default ml-navbar" role="navigation">
    <div class="container">
        <div class="collapse navbar-collapse">
            <div class="row">
                <div id="ml-custom-nav" class="col-sm-12">
                    <!-- dhtml menu start -->
<ul class="nav navbar-nav ml-navbar-nav ml-navbar-menu" id="ml-navigation">
	<li  id="catNav1" class="ml-category-nav-item">
			<a href="/category/firearms.do" class="ml-category-nav-name">Firearms</a>
		<ul catcode="firearms" class="ml-custom-child">
			<!-- Firearms Category --> 
		</ul>
	</li>
	<li id="catNav2" class="ml-category-nav-item">
			<a href="/category/ammunition.do" class="ml-category-nav-name">Ammunition</a>
		<ul catcode="ammunition" class="ml-custom-child">
			<!-- Ammunition Category -->
		</ul>
	</li>
	<li id="catNav3"  class="ml-category-nav-item">
			<a   href="/category/parts-and-accessories.do"  class="ml-category-nav-name">Parts &amp; Accessories</a>
		   <ul catcode="parts-and-accessories" class="ml-custom-child">
			<!-- Parts & Accessories -->
		  </ul>
	</li>
	<li id="catNav4" class="ml-category-nav-item">
			<a id="tn_gun-care" href="/category/gun-care.do" class="ml-category-nav-name">Gun Care</a>
	</li>
	<li id="catNav5" class="ml-category-nav-item">
			<a id="tn_hunting-gear" href="/category/hunting-gear.do" class="ml-category-nav-name">Hunting Gear</a>
	</li>
	<li id="catNav6" class="ml-category-nav-item">
			<a id="tn_range-gear" href="/category/range-gear.do" class="ml-category-nav-name">Range Gear</a>
	</li>
	<li id="catNav7" class="ml-category-nav-item">
			<a id="tn_survival-gear" href="/category/survival-gear.do" class="sf-top-level-link">Survival Gear</a>
	</li>
	<li id="catNav8" class="ml-category-nav-item">
			<a   href="/category/parts-and-accessories.do"  class="ml-category-nav-name">More Products</a>
		   <ul catcode="more-products" class="ml-custom-child">
			<!-- More Products -->
		  </ul>
	</li>
</ul><!-- dhtml menu end --></div>
            </div>
        </div>
    </div>
</nav>

<script type="text/javascript">

	// Initializes Alternate Category Menus //
	window.useAlternateDropNav = function() {
		 
		console.log("AltNav initialized");
		if (jQuery('#ml-custom-nav').size() == 0) return;
		var letjQueryCacheAjax = 1;
		var pathToMenus = '/text/navigation_new/';
		console.log("alternateMenu: init");
		try {
			jQuery("ul.ml-custom-child").each(function(i, o) { // these are the divs that will contain the nav dropdown
				/* Collecting the data */
				var categoryData = {};
				jQuery(o).find('li.flatCatLink').each(function(index, obj) {
					  alert('test');
					var dataCatCode = jQuery(obj).attr('catcode');

					if (dataCatCode != null || dataCatCode != "") {
						categoryData[dataCatCode] = obj;
					}
				});
				var subCategoryData = {};
				jQuery(o).find('li.flatSubCatLink').each(function(index, obj) {
					var dataCatCode = jQuery(obj).attr('catcode');

					if (dataCatCode != null || dataCatCode != "") {
						if (subCategoryData[dataCatCode] == undefined) {
							subCategoryData[dataCatCode] = new Array();
						}
						subCategoryData[dataCatCode].push(obj);
					}
				});
				/* Loading the navigation text file */
				var catcode = jQuery(o).attr('catcode');
				jQuery(this).load(pathToMenus + catcode + ".txt" + ((letjQueryCacheAjax) ? "" : "?" + new Date().getTime()), function() {
				//	useSidrNav();
					console.log("Sidr Nav loaded");
				}); // end after-load function
			});
			// clobber this function (assigned to onmouseovers) because not needed
			window.loadCategoryCustomData = function(o) {
				return true;
			}
			console.log("alternateNav complete")
		} catch (e) {
			console.log("alternateNav error: " + e.toString())
		}
		// removed the hrf link for More Product
		$("#catNav8>a").removeAttr("href");
	};
	$(document.body).on('click touchstart', '#responsive-menu-button', function() {
		$('#pageWrap').append('<div id="mobileOverlay"></div>');
	});
	$(document).ready(function(){
		useAlternateDropNav();
		console.log("AltNav loaded");
	});
</script>
</div><div class="ctd-customtag-header-include"><div class="hidden-xs" style="line-height:30px;background-color:#c62128;width:100%;color:#fff;text-align:center">Welcome to Cheaper Than Dirt! Shop 200,000+ deals on firearms, ammunition, gun parts, survival gear and more.<span class="hidden-sm">&nbsp;All in stock and ready to ship.</div>
</div>
	<div class="container hidden-sm hidden-md hidden-lg">
	   <div class="ml-body-wrapper">
	<!--CatNav Start-->
<div class="ml-navbar-secondary" style="display: none" id="ml-navbar-collapse">
    <div class="row">
        <div id="ml-custom-nav" class="col-sm-12">
           <!-- dhtml menu start -->
<ul class="nav navbar-nav ml-navbar-nav ml-navbar-menu" id="ml-navigation">
	<li  id="catNav1" class="ml-category-nav-item">
			<a href="/category/firearms.do" class="ml-category-nav-name">Firearms</a>
		<ul catcode="firearms" class="ml-custom-child">
			<!-- Firearms Category --> 
		</ul>
	</li>
	<li id="catNav2" class="ml-category-nav-item">
			<a href="/category/ammunition.do" class="ml-category-nav-name">Ammunition</a>
		<ul catcode="ammunition" class="ml-custom-child">
			<!-- Ammunition Category -->
		</ul>
	</li>
	<li id="catNav3"  class="ml-category-nav-item">
			<a   href="/category/parts-and-accessories.do"  class="ml-category-nav-name">Parts &amp; Accessories</a>
		   <ul catcode="parts-and-accessories" class="ml-custom-child">
			<!-- Parts & Accessories -->
		  </ul>
	</li>
	<li id="catNav4" class="ml-category-nav-item">
			<a id="tn_gun-care" href="/category/gun-care.do" class="ml-category-nav-name">Gun Care</a>
	</li>
	<li id="catNav5" class="ml-category-nav-item">
			<a id="tn_hunting-gear" href="/category/hunting-gear.do" class="ml-category-nav-name">Hunting Gear</a>
	</li>
	<li id="catNav6" class="ml-category-nav-item">
			<a id="tn_range-gear" href="/category/range-gear.do" class="ml-category-nav-name">Range Gear</a>
	</li>
	<li id="catNav7" class="ml-category-nav-item">
			<a id="tn_survival-gear" href="/category/survival-gear.do" class="sf-top-level-link">Survival Gear</a>
	</li>
	<li id="catNav8" class="ml-category-nav-item">
			<a   href="/category/parts-and-accessories.do"  class="ml-category-nav-name">More Products</a>
		   <ul catcode="more-products" class="ml-custom-child">
			<!-- More Products -->
		  </ul>
	</li>
</ul><!-- dhtml menu end --></div>
    </div>
</div>
</div>
	</div>
</header>

<main role="main">
 
 	<!--Start CDUA-620-Add bing Bing Ads UET tag tracking code-->			
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o=
{ti:"4002727"}
;o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function()
{var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)}
,i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4002727&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
	<!--End-->	
	
    <div class="ml-navleft-bg">
        <div class="container">
            <div class="ml-leftNav-wrapper" id="ml-leftNav-wrapper-block">
                        <div class="ml-navleft-body ml-body-wrapper">
                                <div class="ml-breadcrumb-wrapper"><ol class="breadcrumb" role="region" aria-label="breadcrumbs">
    <li>
        <a href="/home.do">
				     <span class="hide">breadcrumb link</span>
                   <span class="ml-icon-lib ml-icon-home"></span>
                   </a>
             </li>
            <li>
        <a href="/category/customer-service.do">
				     <span class="hide">breadcrumb link</span>
                   Customer Service</a>
             </li>
            <li class="active">
                     Privacy Policy</li>
            </ol></div>



<div class="commonHeader group">
				   <h1>Privacy Policy</h1>
				</div>
			<div class="ml-gateway-leftnav-wrapper">
            <div id ="gateway_leftnav_singleslot-1" class="ml-slot-item ml-slot-1 gateway_leftnav_singleslot-1"><style>
	#privacy{font-family: Arial, "Helvetica Neue", Helvetica, sans-serif; white-space: normal; color:#363636; font-size:14px;}
.homeFill .sectionTitle {
    font-family: 'Raleway', Verdana, Geneva, sans-serif;
    font-size: 2.2rem;
    font-weight: 900;
    color: #333;
    text-transform: uppercase;
}	
.ml-gateway-leftnav-wrapper {   
padding-top: 0px;
}

#privacyBody{color:#333 !important;}
#privacyBody h3{color:#333;}
</style>
<div id="privacy">

<!--<div class="commonHeader group"><h1>Privacy Policy</h1></div> -->
	
</div> 
	<div id="privacyBody">
		<div class="homefill">
		<div class="sectionTitle">This privacy statement is for Cheaper Than Dirt.</div>
		<p> We are committed to protecting your privacy and want you to know what type of information we collect and what we do with it.If you have questions or concerns regarding our privacy statement or would like to be removed from any of our databases, contact us at: </p>
		<p style="text-align: center"> Cheaper Than Dirt<br>
			2524 NE Loop 820<br>
			Fort Worth, TX 76106<br>
		</p>
		<div class="sectionTitle">INFORMATION COLLECTION AND USE</div>
		<h3><strong>Account Registration and Ordering</strong></h3>
		<p> We collect and retain the following information when you create an account to place an order with us: </p>
		<p>
		<ul class="bullets" style="margin-left:20px">
			<li>Name</li>
			<li>Billing address</li>
			<li>Shipping address</li>
			<li>Email address</li>
			<li>Phone numbers</li>
			<li>Credit card information</li>
		</ul>
		</p>
		<p> We use this information to process your orders and to offer you discounted pricing. Your information may also be shared with other companies offering products that be of interest to you. </p>
		<p> If you do not want to us to retain your information in our system for future order processing, call or write us and we will remove your information. If you do not want us to share your information with other companies, contact us and we will not share your name and address with outside firms. </p>
		<h3><strong>Cookies</strong></h3>
		<p> To place an order on our site, your browser must have cookies turned on. A cookie allows us to recognize you the next time you fill out an order on our site. To prevent your personal information from being read by other web sites, our cookie is a unique, encrypted string that contains no personal information. You may purge our cookie from your browser at any time. However, you will have to receive a new cookie if you wish to place an order through our web site. </p>
		<h3><strong>Email Newsletter Registration</strong></h3>
		<p> If you register to receive our email newsletter or place an order with us, we collect and retain the following: </p>
		<p>
		<ul class="bullets" style="margin-left:20px">
			<li>Name</li>
			<li>Email Address</li>
			<li>ZIP Code</li>
			<li>Product categories of interest </li>
		</ul>
		</p>
		<p> All new registrants receive a confirming email that verifies their subscription to this service. </p>
		<p> We do not share your name and email address with anyone. If you do not want to receive our email newsletter, you may opt out by clicking the "unsubscribe" link at the bottom of the e-mail received. Your name and all associated information will be removed immediately from all email lists.  </p>
		<h3><strong>Catalog</strong></h3>
		<p> When requesting a catalog, we collect your name and mailing address and maintain this information in our database so we may continue to send you future catalogs. When you place an order with us, we use your name and shipping address to send you catalogs. We also share your name and mailing address with companies that we feel may have products of interest to you. If you do not want us to share your name and address, please contact us for removal. If you do not remove your name from our catalog mailing list, we may continue to retain your information. </p>
		<h3><strong>Third Parties</strong></h3>
		<p> We use third parties to provide a variety of services and give them the following information: </p>
		<p>
		<ul class="bullets" style="margin-left:20px">
			<li><span style="text-decoration: underline;">Credit card processing company</span>: Name, billing address, and credit card number and expiration date. </li>
			<li><span style="text-decoration: underline;">Outside warehouse</span>: Name shipping address, phone number, product SKUs, and order number and date. </li>
			<li><span style="text-decoration: underline;">Shipping company</span>: Name, shipping address, phone number, package weight and order number(s). </li>
			<li><span style="text-decoration: underline;">List Services</span>: Name and shipping address. </li>
		</ul>
		</p>
		<div class="sectionTitle">ADDITIONAL INFORMATION</div>
		<h3><strong>Links</strong></h3>
		<p> Our web site and email communications and promotions may contain links to other web sites. We are not responsible for the content of these sites or their privacy policies. It is your responsibility to determine what those privacy policies are and what information is being collected. </p>
		<h3><strong>Send Page to a Friend</strong></h3>
		<p> When you use our "Send Page to a Friend" link, the information you provide is used only to transmit your email. </p>
		<h3><strong><em>Legal Disclaimer</em></strong></h3>
		<p> Although we make every effort to preserve user privacy, we may need to disclose personal information when required by law if we have a good-faith belief that such action is necessary to comply with a current judicial proceeding, a court order or a legal process served on Cheaper Than Dirt. </p>
	</div>
</div>



</div></div>
	</div>
                        <div id="leftNavContainer" class="ml-leftNav-container">
                            <div id="facetsArea" data-facets-area="Category" style="display:none;"></div>
  		<div class="ml-navleft">
	    <div class="ml-navleft-body-wrapper">
	        <div class="col-sm-12 col-md-12"><ul class="ml-leftNav-category-wrapper">
                                    
                  
            
              <li class="ml-navleft-topCats-active">
                    <a data-category-type="$singleCatType" href="/category/customer-service.do?sku_instock_b=true"><h2> Customer Service </h2></a>
        
      </li>
        
  


                                    
                  
                   
                    <li class="ml-navleft-cats-inactive">
                        <a data-category-type="$singleCatType" href="/category/customer-service/contact-us.do?sku_instock_b=true">Contact Us</a>
                </li>
        
  


                                    
                  
                   
                    <li class="ml-navleft-cats-inactive">
                        <a data-category-type="$singleCatType" href="/category/customer-service/faqs.do?sku_instock_b=true">FAQs</a>
                </li>
        
  


                                    
                  
                   
                    <li class="ml-navleft-cats-inactive">
                        <a data-category-type="$singleCatType" href="/category/customer-service/our-story.do?sku_instock_b=true">Our Story</a>
                </li>
        
  


                                    
                  
                   
                    <li class="ml-navleft-cats-inactive">
                        <a data-category-type="$singleCatType" href="/category/customer-service/our-guarantee.do?sku_instock_b=true">Our Guarantee</a>
                </li>
        
  


                                    
                  
                   
                    <li class="ml-navleft-cats-inactive">
                        <a data-category-type="$singleCatType" href="/category/customer-service/shipping-info.do?sku_instock_b=true">Shipping Info</a>
                </li>
        
  


                                    
                  
                   
                    <li class="ml-navleft-cats-inactive">
                        <a data-category-type="$singleCatType" href="/category/customer-service/return-policy.do?sku_instock_b=true">Return Policy</a>
                </li>
        
  


                                    
                  
            
              <li class="ml-navleft-cats-active">
                    <a data-category-type="$singleCatType" href="/category/customer-service/privacy-policy.do?sku_instock_b=true">Privacy Policy</a>
        
      </li>
        
  


                                    
                  
                   
                    <li class="ml-navleft-cats-inactive">
                        <a data-category-type="$singleCatType" href="/category/customer-service/terms-of-use.do?sku_instock_b=true">Terms of Use</a>
                </li>
        
  


                                    
                  
                   
                    <li class="ml-navleft-cats-inactive">
                        <a data-category-type="$singleCatType" href="/category/customer-service/careers.do?sku_instock_b=true">Careers</a>
                </li>
        
  



  <div class="ml-navleft-spacer"> </div>




</ul></div>
	    </div>
    </div>
</div>                        
                    </div>
                </div>
    </div>
</main>

<footer role="contentinfo">
    <div class="ml-footer-global-container">
    <div class="container">
	<div id="globalFooterInclude" class="ml-global-include">
            <div class="ml-footer-global-include-wrapper">
                <div class="ml-footer-global-include">
                    <style>
@media only screen and (max-width : 767px){
   .ml-footer-global-container .ml-global-include .ml-custom-seal-badge {
		width: 94px;
		display: table-cell;
		position: relative;
		top: 30px;
	}
	.ml-footer-global-include {
		width: 100%;
		padding: 0 15px;
	}
	.ml-footer-global-container .ml-global-include .ml-custom-include-content {
		text-align: center;
		display: table-cell;
		padding-right: 15px;
	}
}
@media only screen and (min-width : 768px) and (max-width : 991px){
	.ml-footer-global-container .ml-global-include .ml-custom-include-content span p {
			font-size: 15px;
			padding-right: 40px;
            padding-left: 100px;
	}
}
@media only screen and (min-width : 1200px){
     .ml-footer-global-container .ml-global-include .ml-custom-seal-badge {
		padding-left: 58px;
	}
}
@media only screen and (min-width : 992px){
     .ml-footer-global-container .ml-global-include .ml-custom-seal-badge {
		padding-left: 35px;
		padding-top: 5px;
	}
}
</style>
<div class="ml-custom-seal-badge">
<img src="/images/cheaperthandirt/en_us/global/globalgraphics/guarantee.png" alt="guarantee-badge">
</div>
<div class="ml-custom-include-content">
	<span>
		<h1 class="hidden-xs">
           100% Satisfaction Guarantee 
		</h1>
		<h1 class="hidden-sm hidden-md hidden-lg">
		   Our Guarantee  
		</h1>
	</span>
	<span>
		<p>
		   Shopping with us is absolutely safe - you never have to worry about credit card safety when shopping here...&nbsp;
		   <a class="showLink" href="/category/customer-service/return-policy.do">More<i class="ml-icon-lib ml-link-right"></i></a>
		</p>
	</span>
</div></div>
            </div>
        </div>
    </div>
<div class="ml-footer-separator"></div></div>
<div class="container">
    <div class="ml-footer">

  <div class="ml-footer-container">

	  <div class="ml-footer-signup-wrapper">
	      <div class="ml-footer-email-signup">
	          <form id="emailSignUp" method="post"
	            action="/ancillary/emailsignup.do">
	            <div class="ml-footer-email-signup-label">
	              <label for="footerEmailSignup">
					Sign Up & Save</label>
	            </div>

                  

	            <div class="ml-footer-email-signup-field">
	              <input name="emailAddress" type="email" title="Email Address" id="footerEmailSignup" placeholder="Your email address"
	                maxlength="100"
	                class="form-control"/>
	            </div>
                  <div class="ml-footer-email-signup-button-wrapper">
                      <button type="submit" class="ml-footer-email-signup-button">submit</button>
                      </div>

	          </form>
	      </div>
		  <div class="ml-ctd-dealer-container">
					<div id="resourceFinder">
							<div class="ml-ctd-dealer-title">Find Local FFL Dealers</div>
							<form id="eslSearchForm1" name="eslSearchForm1" class="eslSearchForm" action="/store-locator.do">
								<div class="zipEntryField">
									<input type="hidden" name="method" id="method1" class="form-control eslSearchInput" value="viewSearchPage"/>
									<label for="eslSearchInput1" class="hide">Find a Gun Resource near you</label>
									<input type="text" name="eslSearchInput1" id="eslSearchInput1" class="form-control eslSearchInput" value="Enter zip code" placeholder="Enter zip code" onfocusout="if(this.value=='')this.value='Enter zip code';" onfocus="if(this.value=='Enter zip code')this.value='';" />
								</div>
								<div class="zipEntryBtn">
									<button type="submit" name="eslSearchButton1" id="eslSearchButton1" class="redButton"><i class="ml-icon-lib ml-dealer-button"></i>ffl</button>
								</div>
							</form>
					</div>
				</div>
		<div class="ml-footer-social-links-wrapper">
		      <div class="ml-footer-social-links">
        <div class="ml-footer-social-connect">connect with us</div>
        <div class="ml-cust-icon"><a title="Cheaper Than Dirt on Facebook" href="http://facebook.com/cheaperthandirt" target="_blank" class="ml-icon-lib ml-social-icon ml-icon-facebook"><span>Facebook</span></a></div>
        <div class="ml-cust-icon"><a title="Cheaper Than Dirt on Twitter" href="http://twitter.com/CheaperThanDirt" target="_blank" class="ml-icon-lib ml-social-icon ml-icon-twitter"><span>Twitter</span></a></div>
        <div class="ml-cust-icon"><a title="Cheaper Than Dirt on Instagram" href="https://www.instagram.com/cheaperthandirt/" target="_blank" class="ml-icon-lib ml-social-icon ml-icon-instagram"><span>Instagram</span></a></div>
          <div class="ml-cust-icon"><a title="Cheaper Than Dirt on Pinterest" href="http://www.pinterest.com/cheaperthandirt" target="_blank" class="ml-icon-lib ml-social-icon ml-icon-pintrest"><span>Pinterest</span></a></div>
		<div class="ml-cust-icon"><a title="Cheaper Than Dirt on YouTube" href="http://www.youtube.com/user/CheaperThanDirtVideo" target="_blank" class="ml-icon-lib ml-social-icon ml-icon-youtube"><span>YouTube</span></a></div>
</div></div>

		  <div class="clearfix"></div>

		  <div>
			</div>

	  </div>

	  <div class="ml-footer-links-wrapper">
		  <div class="ml-footer-mini-wrapper">
			  <div class="ml-footer-mini">
                  <div class="ml-footer-phone-number"><a href="tel:1-800-421-8047">1-800-421-8047</a></div>
			  </div>
		  </div>
		  <div class="ml-footer-links">

			<div id="footerColumn-1" class="ml-footer-links-section ml-footer-mini-on">
					<div class="contentSectionContainer">
								<span id="footerColumn-1" class="contentSection col1">
									<h5>Shop</h5>
								</span>
						</div>
					   	<div id="footerItem-1-1" class="footerItem dtOnly">
											<a href="/category/firearms/handguns.do">Handguns</a>
												</div>
									<div id="footerItem-1-2" class="footerItem dtOnly">
											<a href="/category/firearms/rifles.do">Rifles</a>
												</div>
									<div id="footerItem-1-3" class="footerItem dtOnly">
											<a href="/category/firearms/shotguns.do">Shotguns</a>
												</div>
									<div id="footerItem-1-4" class="footerItem ">
											<a href="#" onClick="window.open('/category/ammunition.do');;return false;">Ammo</a>
												</div>
									<div id="footerItem-1-5" class="footerItem dtOnly">
											<a href="/category/parts-and-accessories.do">Parts & Accessories</a>
												</div>
									<div id="footerItem-1-6" class="footerItem ">
											<a href="/category/all-brands.do">Shop All Brands</a>
												</div>
									<div id="footerItem-1-7" class="footerItem dtOnly">
											<a href="/gift-cards.do">Gift Card</a>
												</div>
									<div id="footerItem-1-8" class="footerItem ">
											<a href="/category/sale-items.do?sku_instock_b=true">Sale & Clearance</a>
												</div>
									</div>
				   <div id="footerColumn-2" class="ml-footer-links-section ml-footer-mini-on">
					<div class="contentSectionContainer">
								<span id="footerColumn-2" class="contentSection col2">
									<h5>Customer Service</h5>
								</span>
						</div>
					   	<div id="footerItem-2-1" class="footerItem ">
											<a href="/category/customer-service/contact-us.do">Contact Us</a>
												</div>
									<div id="footerItem-2-2" class="footerItem ">
											<a href="/account/orderhistory.do">Track Order</a>
												</div>
									<div id="footerItem-2-3" class="footerItem ">
											<a href="/category/customer-service/return-policy.do">Return Policy</a>
												</div>
									<div id="footerItem-2-4" class="footerItem ">
											<a href="/category/customer-service/shipping-info.do">Shipping Info</a>
												</div>
									<div id="footerItem-2-5" class="footerItem ">
											<a href="/category/customer-service/faqs.do">FAQs</a>
												</div>
									<div id="footerItem-2-6" class="footerItem ">
											<a href="/category/customer-service/rma.do">RMA Request</a>
												</div>
									</div>
				   <div id="footerColumn-3" class="ml-footer-links-section ml-footer-mini-on">
					<div class="contentSectionContainer">
								<span id="footerColumn-3" class="contentSection col3">
									<h5>Stay Connected</h5>
								</span>
						</div>
					   	<div id="footerItem-3-1" class="footerItem ">
											<a href="#" onClick="window.open('http://blog.cheaperthandirt.com');;return false;">The Shooter's Log</a>
												</div>
									<div id="footerItem-3-2" class="footerItem ">
											<a href="#" onClick="window.open('http://www.youtube.com/user/CheaperThanDirtVideo');;return false;">Videos</a>
												</div>
									<div id="footerItem-3-3" class="footerItem dtOnly">
											<a href="/ffl-dealers.do">FFL Dealers</a>
												</div>
									<div id="footerItem-3-4" class="footerItem dtOnly">
											<a href="#" onClick="window.open('/category/knowledge.do ');;return false;">Knowledge Center</a>
												</div>
									<div id="footerItem-3-5" class="footerItem ">
											<a href="/category/rebate+central.do">Rebate Central</a>
												</div>
									<div id="footerItem-3-10" class="footerItem dtOnly">
											<a href="#" onClick="window.open('/ffl-dealers.do');;return false;">FFL Dealers</a>
												</div>
									</div>
				   <div id="footerColumn-4" class="ml-footer-links-section ml-footer-mini-on">
					<div class="contentSectionContainer">
								<span id="footerColumn-4" class="contentSection col4">
									<h5>Company Info</h5>
								</span>
						</div>
					   	<div id="footerItem-4-1" class="footerItem dtOnly">
											<a href="/category/customer-service/our-story.do">Our Story</a>
												</div>
									<div id="footerItem-4-2" class="footerItem dtOnly">
											<a href="/category/customer-service/our-guarantee.do">Our Guarantee</a>
												</div>
									<div id="footerItem-4-3" class="footerItem dtOnly">
											<a href="/category/customer-service/privacy-policy.do">Privacy Policy</a>
												</div>
									<div id="footerItem-4-4" class="footerItem ">
											<a href="/ancillary/sitemap.do">Site Map</a>
												</div>
									<div id="footerItem-4-5" class="footerItem ">
											<a href="/category/customer-service/terms-of-use.do">Terms of Use</a>
												</div>
									</div>
				   </div>
	  </div>

  </div>
  <div class="ml-footer-bottom">
	  <div class="ml-footer-copyright">
	  	<!--<span class="ml-site-map">
	  		<a href="/ancillary/sitemap.do">Site Map</a>
		</span>
		<span class="ml-site-map">
		<a href="/category/customer-service/terms-of-use.do">Terms of Use</a>
		</span> -->
		&copy;2017 &nbsp;Cheaper Than Dirt</div>
	  <div class="ml-footer-security">
		  </div>
  </div>

</div>

</div>

<div style="display:block"
     class="ml-recommendation-page-context"
     data-ml-recommendation-page-context='{"pageType":"HIGH_LEVEL_CATEGORY","currentCategoryCode":"148594","delayBetweenTwoRequests":1000,"addItemsPresentOnPage":[],"categoryid":"148594","globalCartContext":{"amount":"0.00","pageType":"ADD_TO_CART","optin":"","addCartItemsQtySubtotal":[],"addItemsPresentOnPage":[],"email":""}}'>
</div>
<div id="recaptchaErrorContainer" style="display: none;">
    <div>
        The form is unavailable at this time, please try again later.<br />
        <a class="reviewSubmitReloadUrl" href="javascript:void(0);">Reload</a>
    </div>
</div>
</footer>

<!-- Google Analytics Reporting Support -->
<!-- Google Analytics More info available at http://www.google.com/analytics/index.html -->

<!-- End, Google Analytics Reporting Support -->
<script type="text/javascript">
    window.MarketLive = window.MarketLive || {};
    MarketLive.Reporting = MarketLive.Reporting || {};
    MarketLive.Reporting.templateType = 'CONTENT_GATEWAY';
</script>
<script type='text/javascript' src='/wro/d634276e638c8d790f17af89c2bb0c1d/FOOTER.js?minimize=true'></script><script type='text/javascript' src='/wro/c860b62339a984cf23a60cf3d5bfe3ee/P2P.js?minimize=true'></script><link rel='stylesheet' type='text/css' href='/wro/e39b8a7312910b246ac29a192b427ed/P2P.css?minimize=true'><script type='text/javascript'>
    $('body').focusfinder();
</script><script type='text/javascript' src='https://cdn.socialannex.com/s13/signon-all.js?id=9411111'></script><script type='text/javascript'>
    window.MarketLive = window.MarketLive || {};
    MarketLive.Base = MarketLive.Base || {};
    MarketLive.Base.section = 'CATEGORY';
</script><script type='text/javascript'>
	if (typeof(Storage) !== "undefined") {
		if (sessionStorage.getItem("fullSiteOnMobile")=="true") {
			$("#responsiveCSS").attr("disabled", "disabled"); 
		} else {
			$("#responsiveCSS").removeAttr("disabled")
		}
	}
</script><script type='text/javascript'>
    jQuery(document).ready(function(){
        var verificationRequired = "";
        var fromPage = "LITEREGISTRATION";
        if ((fromPage == 'LITEREGISTRATION' || fromPage == 'MOBILESAVEDCART') && verificationRequired == 'true') {
            jQuery(".ml-lite-registration #guest").on("click", function() {
                jQuery('#socialLoginVerificationMsgContainer').css("display", "none");
            });
            jQuery(".ml-lite-registration #login").on("click", function() {
                jQuery('#socialLoginVerificationMsgContainer').css("display", "block");
            });
        }
    });
</script><script type='text/javascript'>
        // Create MarketLive.SocialLogin namespace
        window.MarketLive = window.MarketLive || {};
        MarketLive.SocialLogin = MarketLive.SocialLogin || {};

        MarketLive.SocialLogin.socialAnnexLogin = new MarketLive.SocialLogin.SocialAnnexLogin(
            {
                'siteId':               '9411111',
                'format':               '',
                'signatures':           ['{"key":"::9411111:273202621407299158:1511131572761","token":"FLU15t0AuNHkEFFB2iJtixerQSr0sLXclhrBeUXrb10%3D"}', '{"key":"::9411111:5276711934566769311:1511131572761","token":"mrff2m8jlAIIeiWjwVpBQdts6QHSGU0wtpNL8%2BzmEO8%3D"}', '{"key":"::9411111:8905050069730637098:1511131572761","token":"puDhpzR5Y1W1SCcRomidIVA%2FrCzHbvRC0P3IhZISk68%3D"}'],
                'fromPage':             'LITEREGISTRATION',
                'window':               window,
                'usesCallbackFunction': true,
                'socialReturned':       false,
                'prevUrl':              '',
                'prevFrom':             ''
            }
        );
    </script><script type='text/javascript'>
            MarketLive.SocialLogin.socialAnnexLogin.show();
        </script><script type='text/javascript'>
                window.MarketLive = window.MarketLive || {};
                MarketLive.LiteRegistration = MarketLive.LiteRegistration || {};
                MarketLive.LiteRegistration.originatingSrc = '';
                placeHolderConfig = {autoInit:false};
            </script><script type='text/javascript'>
    (function(){
        
        var invalidEmail ={required: 'Email is a required field.', email: 'Email does not appear to be a valid e-mail address. Please use the format name@domain.xxx.'};

        
        var invalidPassword ={required: 'Password is a required field.', minlength: 'Password can not be less than 7 characters.'};

        
        var invalidUserNameAndPassword = 'The username\/password combination entered was not found. Please try again.';

        
        var invalidHingAnswer ='Please enter an answer to the hint question.';

        
        var invalidHingQuestion ='Hint Question is required.';

        
        var invalidEmailsMatch ='The emails you entered do not match. Please try again.';

        
        var invalidPasswordsMatch ='The passwords you entered do not match. Please try again.';

        
        var invalidEmailExist ='An account has already been created for this email address.';

        
        var workingYourRequest ='Working on your request...';


        var errMessages = {
            "loginEmail":invalidEmail,
            "loginPassword2":invalidPassword,
            "newCustomerEmail":invalidEmail,
            "customerEmailConfirm":invalidEmailsMatch,
            "loginPassword":invalidPassword,
            "loginPasswordConfirm":invalidPasswordsMatch,
            "liteRegistrationHint":invalidHingQuestion,
            "liteRegistrationAnswer":invalidHingAnswer
        }


        var eleErrorMap = {
            
            'loginEmail|:|Email does not appear to be a valid e-mail address. Please use the format name@domain.xxx.':'loginEmail',

            
            'newCustomerEmail|:|Email does not appear to be a valid e-mail address. Please use the format name@domain.xxx.':'newCustomerEmail',

            
            'The passwords you entered do not match. Please try again.':'loginPassword',

            
            'An account has already been created for this email address.':'newCustomerEmail'
        };

        var paramObj = {};
        paramObj.errMessages = errMessages;
        paramObj.workingYourRequest = workingYourRequest;
        paramObj.eleErrorMap = eleErrorMap;
        paramObj.invalidUserNameAndPassword = invalidUserNameAndPassword;

        MarketLive.LiteRegistration.initialize(paramObj);
    }());
</script><script type='text/javascript'>
$('#guest').click(function () {
	fbq('track', 'Lead');
    //return false;
});
</script><script type='text/javascript'>
$('.ml-lite-registration .form-control').keypress(function (e) {
  if (e.which == 13) {
       $('.ml-lite-registration input[aria-label="submit"]').toggle(on);
       fbq('track', 'CompleteRegistration');    
  }
});

$('#accountSetupForm').on('submit', function () {
    fbq('track', 'CompleteRegistration');
    //return false;
});
</script><script>
        MarketLive.Base.initGlobalHeaderInclude();
    </script><script type='text/javascript'>
        MarketLive.Nav.AutoComplete.ready(1, 35);
    </script><script src='//connect.facebook.net/en_US/all.js#xfbml=1'></script><script src='/includes/MarketLive/FacebookApp/Facebook.js'></script><script type='text/javascript'>
    MarketLive.GlobalCart.onGlobalCartReady();
</script><script type='text/javascript'>
MarketLive.GlobalCart.initialize(true, false,
        "/basket.do?nav=%2Fcategory%2Fid%2F148594&amp;gc=1", 300,
        "globalCartNavOver", 115,
        0, true,
        true,true, "cartItemTable");
if ('' !='QVE') jQuery(document).ready(MarketLive.GlobalCart.adjustGlobalCartLayout(true));

$(document).on('focus', '#globalBasket', function (e) {
    MarketLive.GlobalCart.showGlobalBasket('keyTabPress');
});

$(document).on('blur', '#globalBasket', function (e) {
    setTimeout(function(){
        var focus=$(document.activeElement);
        if (!focus.is("#globalBasket .popDownLayer") && $('#globalBasket .popDownLayer').has(focus).length==0) {
            MarketLive.GlobalCart.closeGlobalBasket()
        } 
    },0);
});

$(document).on('click','.ml-header-search-field,body',function(){
	if($('.globalCartLayer').css('display')=='block'){
		MarketLive.GlobalCart.closeGlobalBasket();
	}
});
</script><script type='text/javascript'>
	MarketLive.Nav.Menu.menuReady(12, 27, 450, true);
</script><script type='text/javascript'>
     $(document).ready(function () {
       
        //Binding & Track menu click action events
         if (MarketLive.Events && MarketLive.Events.bindingMenuEvents) {
              MarketLive.Events.bindingMenuEvents();
         }
       
     });
</script><script type='text/javascript'>
	MarketLive.Nav.Menu.menuReady(12, 27, 450, true);
</script><script type='text/javascript'>
    MarketLive.Nav.FooterNav.onFooterReady();
</script><script type='text/javascript'>
            $(window).load(function(){
				var params = {
				"currentCategoryCode" : "148594",
				"productid" : ""
				}
				if (MarketLive.Recommendations.rti && MarketLive.Recommendations.rti.start) {
					MarketLive.Recommendations.rti.start(params);
					BaynoteAPI.init({'server': 'https://ctd-www.baynote.net/', 'customerId': 'ctd' , 'code': 'www'});
					BaynoteAPI.executeAll();
				}
            });
        </script><script>
MarketLive.Base.showFormWithRecaptchaRendering(15000);
</script><script type='text/javascript'></script><script src='//assets.pcrl.co/js/jstracker.min.js'></script><!-- Steelhouse Pixels -->
		
		<!-- Smarter Pixel starts -->
		<script type="text/javascript">
		(function(){"use strict";var e=null,b="4.0.0",
		n="10597",
		additional="",
		t,r,i;try{t=top.document.referer!==""?encodeURIComponent(top.document.referrer.substring(0,2048)):""}catch(o){t=document.referrer!==null?document.referrer.toString().substring(0,2048):""}try{r=window&&window.top&&document.location&&window.top.location===document.location?document.location:window&&window.top&&window.top.location&&""!==window.top.location?window.top.location:document.location}catch(u){r=document.location}try{i=parent.location.href!==""?encodeURIComponent(parent.location.href.toString().substring(0,2048)):""}catch(a){try{i=r!==null?encodeURIComponent(r.toString().substring(0,2048)):""}catch(f){i=""}}var l,c=document.createElement("script"),h=null,p=document.getElementsByTagName("script"),d=Number(p.length)-1,v=document.getElementsByTagName("script")[d];if(typeof l==="undefined"){l=Math.floor(Math.random()*1e17)}h="dx.steelhousemedia.com/spx?"+"dxver="+b+"&shaid="+n+"&tdr="+t+"&plh="+i+"&cb="+l+additional;c.type="text/javascript";c.src=("https:"===document.location.protocol?"https://":"http://")+h;v.parentNode.insertBefore(c,v)})()
		</script>
		<!-- Smarter Pixel ends -->
		
</body>

<!-- SP-2016-23936 - Add Emarsys Pixel to every page -->
<script type="text/javascript">
ScarabQueue.push(['go']);
</script>
<!-- SP-2016-23936 -->
</html>