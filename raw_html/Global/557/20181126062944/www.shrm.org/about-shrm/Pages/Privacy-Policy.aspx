

<!DOCTYPE html>

<html xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml" dir="ltr" lang="en-US">
	<head><meta http-equiv="X-UA-Compatible" content="IE=11" /><meta name="GENERATOR" content="Microsoft SharePoint" /><meta http-equiv="Content-type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Expires" content="0" /><meta http-equiv="Accept-CH" content="DPR, Width" /><meta name="BreadCrumbText" content="About SHRM,Privacy Policy," />
	<meta name="BreadCrumb" content="{{Privacy Policy,/about-shrm/Pages/Privacy-Policy.aspx}{About SHRM,/about-shrm/Pages/default.aspx}}" />
	<meta name="DocumentDate" content="2018-10-02" />
	<meta name="MigratedModifiedDate" content="10/2/2018 12:29:37 PM" />
	<meta name="MetaDescription" content=" 
   
      &amp;#160;Privacy&amp;#160;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource..." />
	<meta name="MetaKeywords" content="" />
	<meta name="PageGuid" content="05f756a6-7090-4d03-976f-a430f545b58e" />
	<meta name="PageName" content="Privacy-Policy" />
	<meta name="SHRMMemberOnly" content="False" />
	<meta name="Abstract" content=" 
   
      &nbsp;Privacy&nbsp;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource profession. &nbsp;In Part I of this Privacy Statement we set forth SHRM's Privacy Policy as to Personal Information which we collect online through&nbsp;www.shrm.org&nbsp;or other SHRM websites which link to this Privacy Statement from their site (collectively, &quot;SHRM Websites&quot;).&nbsp;In Part II of this Privacy Statement we set forth SHRM's Privacy Policy as to Personal Information which we collect other than through SHRM Websites.&nbsp;&quot;Personal Information&quot;, is any information that enables us to identify you, directly or indirectly, by reference to an identifier such as your name, identification number, location data, online identifier or one or more factors specific to you. Personal Information includes &quot;sensitive Personal Information&quot; and &quot;pseudonymised Personal Information&quot; but excludes anon" />
	<meta name="RollupImage" content="" />
	<meta name="ArticleAuthor" content="" />
	<meta name="ArticleIsToolContent" content="False" />
	<meta name="ArticleSocialToolsEnabled" content="False" />
	<meta name="Taxonomy" content="N/A" />
	
<meta name="SHRM" content="v=1.65.8514.453;" />
<meta name="msapplication-TileImage" content="/_layouts/15/images/SharePointMetroAppTile.png" /><meta name="msapplication-TileColor" content="#0072C6" /><title>
	
    Privacy Policy

</title>
		<!-- IE -->
		<link rel="shortcut icon" type="image/x-icon" href="https://cdn.shrm.org/image/upload/favicon.ico" />
		<!-- other browsers -->
		<link rel="icon" type="image/x-icon" href="https://cdn.shrm.org/image/upload/favicon.ico" /><link rel="stylesheet" type="text/css" href="/_layouts/15/1033/styles/Themable/corev15.css?rev=OqAycmyMLoQIDkAlzHdMhQ%3D%3D"/>
<script type="text/javascript" src="/_layouts/15/init.js?rev=Xpo7ARBt8xBROO1h5n3s6g%3D%3D"></script>
<script type="text/javascript" src="/ScriptResource.axd?d=pVhMdx7BaO7vWyDvmfSFy2UmUP0AZeOYCKdSH5vzevhISxxotfv42LBXS-7tTSjJj0TffjY4wbwAF7mP-zNBnhHtYYSSc5oL7iJreKPYAc7Mf4OdX6ca9cmkWj41yNKcdJFqS-i-jnY6SuvgDiG_Qf0vE5Q0fV6cL-k8_maa5NmH4AQM-B-u-Jh0aeu7IgN60&amp;t=ffffffffb0622999"></script>
<script type="text/javascript" src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D"></script>
<script type="text/javascript" src="/ScriptResource.axd?d=7whSi3pO0Sr1q7wtXvGRhCLH5v0Hiv7OvHHHzO4fgUhg34hBa_GU9Tw_QvR8u_PEBFsbnvVVfzUmpgjKcC8n5USDVWEqSz2MwfFIySR2RS6r-Du8U1E1UFib4NdVYImD6Bl9DRe3DJtp6Pt1rOrpLxKWrXyq6EnC1udd8iI6ppclvi73BLQs6J7hCQSkoApT0&amp;t=ffffffffb0622999"></script>
<script type="text/javascript">RegisterSod("initstrings.js", "\u002f_layouts\u002f15\u002f1033\u002finitstrings.js?rev=2N3pk\u00252FwjnG3Tj9o5NrqHcg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("strings.js", "\u002f_layouts\u002f15\u002f1033\u002fstrings.js?rev=y6khbhv1Os2YPb5X0keqRg\u00253D\u00253D");RegisterSodDep("strings.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sp.init.js", "\u002f_layouts\u002f15\u002fsp.init.js?rev=jvJC3Kl5gbORaLtf7kxULQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=yNk\u00252FhRzgBn40LJVP\u00252BqfgdQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002f15\u002fsp.ui.dialog.js?rev=3Oh2QbaaiXSb7ldu2zd6QQ\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.init.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f15\u002fcore.js?rev=GpU7vxyOqzS0F9OfEX3CCw\u00253D\u00253D");RegisterSodDep("core.js", "strings.js");</script>
<script type="text/javascript">RegisterSod("mQuery.js", "\u002f_layouts\u002f15\u002fmquery.js?rev=VYAJYBo5H8I3gVSL3MzD6A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("callout.js", "\u002f_layouts\u002f15\u002fcallout.js?rev=ryx2n4ePkYj1\u00252FALmcsXZfA\u00253D\u00253D");RegisterSodDep("callout.js", "strings.js");RegisterSodDep("callout.js", "mQuery.js");RegisterSodDep("callout.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clienttemplates.js", "\u002f_layouts\u002f15\u002fclienttemplates.js?rev=NjHQZTfQs\u00252BiDIjz4PK\u00252FeWg\u00253D\u00253D");RegisterSodDep("clienttemplates.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sharing.js", "\u002f_layouts\u002f15\u002fsharing.js?rev=XxxHIxIIc8BsW9ikVc6dgA\u00253D\u00253D");RegisterSodDep("sharing.js", "strings.js");RegisterSodDep("sharing.js", "mQuery.js");RegisterSodDep("sharing.js", "clienttemplates.js");RegisterSodDep("sharing.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clientrenderer.js", "\u002f_layouts\u002f15\u002fclientrenderer.js?rev=PWwV4FATEiOxN90BeB5Hzw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("srch.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=Srch\u00252EResources\u0026rev=fPbk\u00252B7qByTbyuNVTMptzRw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("search.clientcontrols.js", "\u002f_layouts\u002f15\u002fsearch.clientcontrols.js?rev=\u00252BN5FhPOhdMDlyCSUMQPtrg\u00253D\u00253D");RegisterSodDep("search.clientcontrols.js", "sp.init.js");RegisterSodDep("search.clientcontrols.js", "clientrenderer.js");RegisterSodDep("search.clientcontrols.js", "srch.resources.resx");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002f15\u002fsp.runtime.js?rev=5f2WkYJoaxlIRdwUeg4WEg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.search.js", "\u002f_layouts\u002f15\u002fsp.search.js?rev=dMkPlEXpdY6iJ\u00252FsY5RsB0g\u00253D\u00253D");RegisterSodDep("sp.search.js", "sp.init.js");RegisterSodDep("sp.search.js", "sp.runtime.js");</script>
<script type="text/javascript">RegisterSod("ajaxtoolkit.js", "\u002f_layouts\u002f15\u002fajaxtoolkit.js?rev=4rOiCbaFgJMmqw9Ojtpa6g\u00253D\u00253D");RegisterSodDep("ajaxtoolkit.js", "search.clientcontrols.js");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002f15\u002fsp.js?rev=lrxLgKOmx0nl2elVy0T07w\u00253D\u00253D");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("cui.js", "\u002f_layouts\u002f15\u002fcui.js?rev=LPKF2\u00252BgWXqwwaFh34pQUlA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002f15\u002fsp.core.js?rev=tZDGLPOvY1bRw\u00252BsgzXpxTg\u00253D\u00253D");RegisterSodDep("sp.core.js", "strings.js");RegisterSodDep("sp.core.js", "sp.init.js");RegisterSodDep("sp.core.js", "core.js");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002f15\u002finplview.js?rev=iMf5THfqukSYut7sl9HwUg\u00253D\u00253D");RegisterSodDep("inplview", "strings.js");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("ribbon", "\u002f_layouts\u002f15\u002fsp.ribbon.js?rev=1F3TSGFB5\u00252FyAaRkjYHJL5w\u00253D\u00253D");RegisterSodDep("ribbon", "strings.js");RegisterSodDep("ribbon", "core.js");RegisterSodDep("ribbon", "sp.core.js");RegisterSodDep("ribbon", "sp.js");RegisterSodDep("ribbon", "cui.js");RegisterSodDep("ribbon", "sp.res.resx");RegisterSodDep("ribbon", "sp.runtime.js");RegisterSodDep("ribbon", "inplview");</script>
<script type="text/javascript">RegisterSod("sp.ui.policy.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EUI\u00252EPolicy\u00252EResources\u0026rev=wtVfoqgvnf2cuN894ZirvA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mdn.js", "\u002f_layouts\u002f15\u002fmdn.js?rev=CZrBt1\u00252Fch9YeLFLJtB0mvg\u00253D\u00253D");RegisterSodDep("mdn.js", "sp.init.js");RegisterSodDep("mdn.js", "core.js");RegisterSodDep("mdn.js", "sp.runtime.js");RegisterSodDep("mdn.js", "sp.js");RegisterSodDep("mdn.js", "cui.js");RegisterSodDep("mdn.js", "ribbon");RegisterSodDep("mdn.js", "sp.ui.policy.resources.resx");</script>
<script type="text/javascript">RegisterSod("userprofile", "\u002f_layouts\u002f15\u002fsp.userprofiles.js?rev=p5tCOm\u00252FlHUwcfll7W3pKNw\u00253D\u00253D");RegisterSodDep("userprofile", "sp.runtime.js");</script>
<script type="text/javascript">RegisterSod("followingcommon.js", "\u002f_layouts\u002f15\u002ffollowingcommon.js?rev=jWqEDmcjCSPmnQw2ZIfItQ\u00253D\u00253D");RegisterSodDep("followingcommon.js", "strings.js");RegisterSodDep("followingcommon.js", "sp.js");RegisterSodDep("followingcommon.js", "userprofile");RegisterSodDep("followingcommon.js", "core.js");RegisterSodDep("followingcommon.js", "mQuery.js");</script>
<script type="text/javascript">RegisterSod("profilebrowserscriptres.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=ProfileBrowserScriptRes\u0026rev=J5HzNnB\u00252FO1Id\u00252FGI18rpRcw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.mysitecommon.js", "\u002f_layouts\u002f15\u002fsp.ui.mysitecommon.js?rev=Ua8qmZSU9nyf53S7PEyJwQ\u00253D\u00253D");RegisterSodDep("sp.ui.mysitecommon.js", "sp.init.js");RegisterSodDep("sp.ui.mysitecommon.js", "sp.runtime.js");RegisterSodDep("sp.ui.mysitecommon.js", "userprofile");RegisterSodDep("sp.ui.mysitecommon.js", "profilebrowserscriptres.resx");</script>
<link type="text/xml" rel="alternate" href="/about-shrm/_vti_bin/spsdisco.aspx" />
			
    
<meta itemprop='name' content='Privacy Policy' />
<meta itemprop='url' content='https://www.shrm.org/about-shrm/pages/privacy-policy.aspx' />
<meta itemprop='image' content='https://www.shrm.org/publishingimages/shrm-sharing-logo-square-v5.png' />
<meta itemprop='author' content='' />
<meta itemprop='description' content=' 
   
      &amp;#160;Privacy&amp;#160;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource...' />
<meta itemprop='datePublished' content='10/2/2018 4:29:37 PM' />
<meta itemprop='dateModified' content='10/2/2018 4:29:37 PM' />

<!-- Twitter Card data -->
<meta name='twitter:card' content='summary'>
<meta name='twitter:site' content='@SHRM'>
<meta name='twitter:title' content='Privacy Policy'>
<meta name='twitter:description' content=' 
   
      &amp;#160;Privacy&amp;#160;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource...'>

<meta name='twitter:image' content='https://www.shrm.org/publishingimages/shrm-sharing-logo-square-v5.png'>
<!-- Open Graph data -->
<meta property='og:title' content='Privacy Policy' />
<meta property='og:type' content='article' />
<meta property='og:url' content='https://www.shrm.org/about-shrm/pages/privacy-policy.aspx' />
<meta property='og:image' content='https://www.shrm.org/publishingimages/shrm-sharing-logo-square-v5.png' />
<meta property='og:description' content=' 
   
      &amp;#160;Privacy&amp;#160;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource...' />
<meta property='og:site_name' content='SHRM' />
<meta property='article:published_time' content='10/2/2018 4:29:37 PM' />
<meta property='article:modified_time' content='10/2/2018 4:29:37 PM' />



<meta property='fb:app_id' content='649819231827420' />















    
    <script type='application/ld+json'>{ "@context": "http://schema.org", "@type": "NewsArticle", "mainEntityOfPage": { "@type": "WebPage", "@id": "https://www.shrm.org/about-shrm/Pages/Privacy-Policy.aspx"}, "headline": "Privacy Policy", "articleBody": "", "image": "https://www.shrm.org/PublishingImages/shrm-sharing-logo-square-v5.png", "datePublished": "10/2/2018 12:29:37 PM", "dateModified": "10/2/2018 12:29:37 PM", "author": { "@type": "Person", "name": "SHRM" }, "publisher": { "name": "SHRM", "@type": "Organization", "logo": { "@type": "ImageObject", "url": "https://www.shrm.org/_layouts/15/SHRM.Core/design/images/SHRMLogo.jpg" } }, "description": "", "isAccessibleForFree": "True"} </script>



			<!-- _lcid="1033" _version="15.0.5075" _dal="1" -->
<!-- _LocalBinding -->

<link rel="canonical" href="https://www.shrm.org:443/about-shrm/Pages/Privacy-Policy.aspx" />
			
		
        <!-- CssRegistration will remain here for now -->
		
		<!-- Start - Your references -->
        

<script>
var SHRMCoreVars =(function () {
	var private = {
		'Member2MemberSubsiteUrl'	: '/ResourcesAndTools/tools-and-samples/member2member',
        'ExpressRequestWebUrl': '/ResourcesAndTools/tools-and-samples/exreq/',
        'SearchDomain': 'https://www.shrm.org',
	    'SearchQueryUrl': '/search/pages/default.aspx',
        'SearchTopics': 'Benefits, Business Acumen, California Resources, Communication, Compensation, Consultation, Critical Evaluation, Employee Relations, Ethical Practice, Global & Cultural Effectiveness,Global HR, Labor Relations,Leadership & Navigation,Organizational & Employee Development,Relationship Management,Risk Management,Talent Acquisition,Technology',
        'SearchSourceUrl': 'https://edit.shrm.org',
        'SearchPublicUrl': 'https://www.shrm.org',
        'CobaltSearchEndpointUrl': 'https://shrmcrmapi.cobaltsaas.com/api/v1.0',
        'CobaltSearchDatabaseEnabled': '1',
        'ListenToScrollerAdSettings': '1',
        'CurrentSiteUrl': 'https://www.shrm.org',
        'CurrentContextUrl':'https://www.shrm.org',
        'UseSPSearch': '1'
	 };

	return {
		get: function(name) { return private[name]; }
	};
})();
</script>




<link rel="stylesheet" type="text/css" href="/v1658514453/core/assets/dist/styles.min.css ">
<script src="/v1658514453/core/assets/dist/scripts.min.js"></script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyDbkBtKmBsBrLXHtfbDxsavW5mgnytrPYY&libraries=places"></script>  


<link rel="stylesheet" type="text/css" href="/Style%20Library/temp.css">

        <style>
            li.ms-core-menu-item[text='SHRM Foundation - Management'] { display: none; }
            li.ms-core-menu-item[text='SHRM Foundation - Settings'] { display: none; }
            li.ms-core-menu-item[text='SHRM HRPS - Management'] { display: none; }
            li.ms-core-menu-item[text='SHRM HRPS - Settings'] { display: none; }
        </style>
        
    <script type="text/javascript">
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function() {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>
    <script type='text/javascript'>$(document).ready(function () {RegisterGoogleTag('AdWordsScript', '');});</script>




    <script type="text/javascript">
        function CloadinarySetCaption() {
            console.log('cloudinarysetcaption: start');
            $(".article-content img").each(function (i) {
                if ($(this).hasClass("16x9")) { $(this).removeClass("16x9").addClass("ratio-16x9"); 
									}else if ($(this).hasClass("3x4")) { $(this).removeClass("3x4").addClass("ratio-3x4"); 
									}else if ($(this).hasClass("1x1")) { $(this).removeClass("1x1").addClass("ratio-1x1"); 
									};            
            });
            $(".article-content img[data-caption]").each(function (i) {
                if ($(this).attr('data-caption') != null || $(this).attr('data-caption') != '' || $(this).attr('data-caption') != 'undefined') {
                    $(this).wrap("<figure class='imagewithcaption content-figure'></figure>")
											.parent().append("<figcaption>" + $(this).attr('data-caption') + "</figcaption>")
											.addClass($(this).attr('class'));
                }
            });
            if ($(".article-cover-figure img").length == 0) {
                $(".article-cover-figure").hide();
            } else {
                $('.article-cover-figure img').one('load', function() {
									$(this).closest('.article-cover-figure').addClass(function() {
										var figClass = 'ratio-';
										if ($(this).height() === $(this).width()) {
												figClass += '1x1';
										} else if ($(this).height() > $(this).width()) {
												figClass += '3x4';
										} else if($(this).width() > $(this).height()) {
												figClass += '16x9';
										}
										return figClass;
									});		
								}).each(function() {
									 if(this.complete || /*for IE 10-*/ $(this).height() > 0)
										 $(this).load();
								});
								
            }
            if ($(".article-cover-figure figcaption div").eq(1).text() == "" || $(".article-cover-figure figcaption div").eq(1).html() == "") {
                $(".article-cover-figure figcaption").hide();
            }
            console.log('cloudinarysetcaption: end');
        }
        $(document).ready(function () {
            CloadinarySetCaption();
        });
    </script>



		
		
		<!-- End - your references -->
		
		<script type="text/javascript">
		    $(document).ready(function () {
		        $("html head link[rel=canonical]").first().attr("href", $("html head link[rel=canonical]").first().attr("href").toLowerCase().replace(":80", "").replace(":443", ""));
		    });
        </script>
		<!--	Disable Firefox Microsoft Office Plugin Prompt (if you're making copy of this master please make sure you include this part as well)	-->
        <script type="text/javascript">
	        function ProcessImn() { }
	        function ProcessImnMarkers() { }
        </script>

        <style>
            li.ms-core-menu-item[text='SHRM HRPS - Management'] { display: none; }
            li.ms-core-menu-item[text='SHRM HRPS - Settings'] { display: none; }
            li.ms-core-menu-item[text='SHRM Foundation - Management'] { display: none; }
            li.ms-core-menu-item[text='SHRM Foundation - Settings'] { display: none; }
        </style>
        
		<!--	GTM CALL (if you're making copy of this master please make sure you include this part as well)	-->
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-N6W5WC');</script>
		<!-- End Google Tag Manager -->
	</head>
	<body id="ctl00_mainbody" class="shrm-role-member shrm-role-member-expire" ng-app="photoAlbumApp">
        <!--googleoff: all-->
		

<script>
  //window.fbAsyncInit = function() { FB.init({ appId: '649819231827420', xfbml: true, version: 'v2.5' }); };

  //(function(d, s, id){
  //   var js, fjs = d.getElementsByTagName(s)[0];
  //   if (d.getElementById(id)) {return;}
  //   js = d.createElement(s); js.id = id;
  //   js.src = "//connect.facebook.net/en_US/sdk.js";
  //   fjs.parentNode.insertBefore(js, fjs);
    //}(document, 'script', 'facebook-jssdk'));
    function shrm_encodeURI(s) { return encodeURIComponent(s); }
    function RightsLinkPopUp() {
        var url = "https://s100.copyright.com/AppDispatchServlet";
        var location = url + "?publisherName=" + shrm_encodeURI("shrm") + "&publication=" + shrm_encodeURI("Legal_Issues") + "&title=" + shrm_encodeURI("Justices Hear ERISA Reimbursement Case") + "&publicationDate=" + shrm_encodeURI("11/11/2015 12:00:00 AM") + "&contentID=" + shrm_encodeURI("6badda72-62e7-49e8-b4bd-ebbd02a72a17") + "&charCnt=" + shrm_encodeURI("7399") + "&orderBeanReset=" + shrm_encodeURI("True");
        window.open(location, "RightsLink", "location=no,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=650,height=550");
    }
</script>

  		
        <noscript><div class='noindex'>You may be trying to access this site from a secured browser on the server. Please enable scripts and reload this page.</div></noscript>
  		
  		<form method="post" action="./Privacy-Policy.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div class="aspNetHidden">
<input type="hidden" name="_wpcmWpid" id="_wpcmWpid" value="" />
<input type="hidden" name="wpcmVal" id="wpcmVal" value="" />
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value="" />
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value="" />
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value="" />
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="0x6E72AB1A409D6F7DF4E3702910971311375D9383A8318D0D80C20BBD92C95D0C51F88D80713129AA62AC9150847014969A87CBC0F512851DB3B2EDFA40778D71,26 Nov 2018 06:29:44 -0000" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMTc4MTEzMjE5D2QWAmYPZBYCAgEPZBYEAgEPZBYOAgYPZBYCZg8WAh4EVGV4dAW1EDxtZXRhIG5hbWU9IkJyZWFkQ3J1bWJUZXh0IiBjb250ZW50PSJBYm91dCBTSFJNLFByaXZhY3kgUG9saWN5LCIgLz4KCTxtZXRhIG5hbWU9IkJyZWFkQ3J1bWIiIGNvbnRlbnQ9Int7UHJpdmFjeSBQb2xpY3ksL2Fib3V0LXNocm0vUGFnZXMvUHJpdmFjeS1Qb2xpY3kuYXNweH17QWJvdXQgU0hSTSwvYWJvdXQtc2hybS9QYWdlcy9kZWZhdWx0LmFzcHh9fSIgLz4KCTxtZXRhIG5hbWU9IkRvY3VtZW50RGF0ZSIgY29udGVudD0iMjAxOC0xMC0wMiIgLz4KCTxtZXRhIG5hbWU9Ik1pZ3JhdGVkTW9kaWZpZWREYXRlIiBjb250ZW50PSIxMC8yLzIwMTggMTI6Mjk6MzcgUE0iIC8+Cgk8bWV0YSBuYW1lPSJNZXRhRGVzY3JpcHRpb24iIGNvbnRlbnQ9IiANCiAgIA0KICAgICAgJmFtcDsjMTYwO1ByaXZhY3kmYW1wOyMxNjA7U3RhdGVtZW50IFVwZGF0ZWQgT2N0b2JlciAxLCAyMDE4LldlbGNvbWUgdG8gdGhlIFNvY2lldHkgZm9yIEh1bWFuIFJlc291cmNlIE1hbmFnZW1lbnQgKFNIUk0pLiBTSFJNIGlzIHRoZSBsZWFkaW5nIG1lbWJlcnNoaXAgYXNzb2NpYXRpb24gZm9yIHRoZSBodW1hbiByZXNvdXJjZS4uLiIgLz4KCTxtZXRhIG5hbWU9Ik1ldGFLZXl3b3JkcyIgY29udGVudD0iIiAvPgoJPG1ldGEgbmFtZT0iUGFnZUd1aWQiIGNvbnRlbnQ9IjA1Zjc1NmE2LTcwOTAtNGQwMy05NzZmLWE0MzBmNTQ1YjU4ZSIgLz4KCTxtZXRhIG5hbWU9IlBhZ2VOYW1lIiBjb250ZW50PSJQcml2YWN5LVBvbGljeSIgLz4KCTxtZXRhIG5hbWU9IlNIUk1NZW1iZXJPbmx5IiBjb250ZW50PSJGYWxzZSIgLz4KCTxtZXRhIG5hbWU9IkFic3RyYWN0IiBjb250ZW50PSIgDQogICANCiAgICAgICZuYnNwO1ByaXZhY3kmbmJzcDtTdGF0ZW1lbnQgVXBkYXRlZCBPY3RvYmVyIDEsIDIwMTguV2VsY29tZSB0byB0aGUgU29jaWV0eSBmb3IgSHVtYW4gUmVzb3VyY2UgTWFuYWdlbWVudCAoU0hSTSkuIFNIUk0gaXMgdGhlIGxlYWRpbmcgbWVtYmVyc2hpcCBhc3NvY2lhdGlvbiBmb3IgdGhlIGh1bWFuIHJlc291cmNlIHByb2Zlc3Npb24uICZuYnNwO0luIFBhcnQgSSBvZiB0aGlzIFByaXZhY3kgU3RhdGVtZW50IHdlIHNldCBmb3J0aCBTSFJNJ3MgUHJpdmFjeSBQb2xpY3kgYXMgdG8gUGVyc29uYWwgSW5mb3JtYXRpb24gd2hpY2ggd2UgY29sbGVjdCBvbmxpbmUgdGhyb3VnaCZuYnNwO3d3dy5zaHJtLm9yZyZuYnNwO29yIG90aGVyIFNIUk0gd2Vic2l0ZXMgd2hpY2ggbGluayB0byB0aGlzIFByaXZhY3kgU3RhdGVtZW50IGZyb20gdGhlaXIgc2l0ZSAoY29sbGVjdGl2ZWx5LCAmcXVvdDtTSFJNIFdlYnNpdGVzJnF1b3Q7KS4mbmJzcDtJbiBQYXJ0IElJIG9mIHRoaXMgUHJpdmFjeSBTdGF0ZW1lbnQgd2Ugc2V0IGZvcnRoIFNIUk0ncyBQcml2YWN5IFBvbGljeSBhcyB0byBQZXJzb25hbCBJbmZvcm1hdGlvbiB3aGljaCB3ZSBjb2xsZWN0IG90aGVyIHRoYW4gdGhyb3VnaCBTSFJNIFdlYnNpdGVzLiZuYnNwOyZxdW90O1BlcnNvbmFsIEluZm9ybWF0aW9uJnF1b3Q7LCBpcyBhbnkgaW5mb3JtYXRpb24gdGhhdCBlbmFibGVzIHVzIHRvIGlkZW50aWZ5IHlvdSwgZGlyZWN0bHkgb3IgaW5kaXJlY3RseSwgYnkgcmVmZXJlbmNlIHRvIGFuIGlkZW50aWZpZXIgc3VjaCBhcyB5b3VyIG5hbWUsIGlkZW50aWZpY2F0aW9uIG51bWJlciwgbG9jYXRpb24gZGF0YSwgb25saW5lIGlkZW50aWZpZXIgb3Igb25lIG9yIG1vcmUgZmFjdG9ycyBzcGVjaWZpYyB0byB5b3UuIFBlcnNvbmFsIEluZm9ybWF0aW9uIGluY2x1ZGVzICZxdW90O3NlbnNpdGl2ZSBQZXJzb25hbCBJbmZvcm1hdGlvbiZxdW90OyBhbmQgJnF1b3Q7cHNldWRvbnltaXNlZCBQZXJzb25hbCBJbmZvcm1hdGlvbiZxdW90OyBidXQgZXhjbHVkZXMgYW5vbiIgLz4KCTxtZXRhIG5hbWU9IlJvbGx1cEltYWdlIiBjb250ZW50PSIiIC8+Cgk8bWV0YSBuYW1lPSJBcnRpY2xlQXV0aG9yIiBjb250ZW50PSIiIC8+Cgk8bWV0YSBuYW1lPSJBcnRpY2xlSXNUb29sQ29udGVudCIgY29udGVudD0iRmFsc2UiIC8+Cgk8bWV0YSBuYW1lPSJBcnRpY2xlU29jaWFsVG9vbHNFbmFibGVkIiBjb250ZW50PSJGYWxzZSIgLz4KCTxtZXRhIG5hbWU9IlRheG9ub215IiBjb250ZW50PSJOL0EiIC8+CglkAgcPZBYCZg8WAh8ABS88bWV0YSBuYW1lPSJTSFJNIiBjb250ZW50PSJ2PTEuNjUuODUxNC40NTM7IiAvPmQCCg9kFgJmD2QWAgIBDxYCHhNQcmV2aW91c0NvbnRyb2xNb2RlCymIAU1pY3Jvc29mdC5TaGFyZVBvaW50LldlYkNvbnRyb2xzLlNQQ29udHJvbE1vZGUsIE1pY3Jvc29mdC5TaGFyZVBvaW50LCBWZXJzaW9uPTE1LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTcxZTliY2UxMTFlOTQyOWMBZAIXD2QWBAIBD2QWBAIBD2QWKgICDxYCHwAFMTxtZXRhIGl0ZW1wcm9wPSduYW1lJyBjb250ZW50PSdQcml2YWN5IFBvbGljeScgLz5kAgQPFgIfAAVbPG1ldGEgaXRlbXByb3A9J3VybCcgY29udGVudD0naHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wYWdlcy9wcml2YWN5LXBvbGljeS5hc3B4JyAvPmQCBg8WAh8ABWk8bWV0YSBpdGVtcHJvcD0naW1hZ2UnIGNvbnRlbnQ9J2h0dHBzOi8vd3d3LnNocm0ub3JnL3B1Ymxpc2hpbmdpbWFnZXMvc2hybS1zaGFyaW5nLWxvZ28tc3F1YXJlLXY1LnBuZycgLz5kAggPFgIfAAUlPG1ldGEgaXRlbXByb3A9J2F1dGhvcicgY29udGVudD0nJyAvPmQCCg8WAh8ABfQBPG1ldGEgaXRlbXByb3A9J2Rlc2NyaXB0aW9uJyBjb250ZW50PScgCiAgIAogICAgICAmYW1wOyMxNjA7UHJpdmFjeSZhbXA7IzE2MDtTdGF0ZW1lbnQgVXBkYXRlZCBPY3RvYmVyIDEsIDIwMTguV2VsY29tZSB0byB0aGUgU29jaWV0eSBmb3IgSHVtYW4gUmVzb3VyY2UgTWFuYWdlbWVudCAoU0hSTSkuIFNIUk0gaXMgdGhlIGxlYWRpbmcgbWVtYmVyc2hpcCBhc3NvY2lhdGlvbiBmb3IgdGhlIGh1bWFuIHJlc291cmNlLi4uJyAvPmQCDA8WAh8ABUA8bWV0YSBpdGVtcHJvcD0nZGF0ZVB1Ymxpc2hlZCcgY29udGVudD0nMTAvMi8yMDE4IDQ6Mjk6MzcgUE0nIC8+ZAIODxYCHwAFPzxtZXRhIGl0ZW1wcm9wPSdkYXRlTW9kaWZpZWQnIGNvbnRlbnQ9JzEwLzIvMjAxOCA0OjI5OjM3IFBNJyAvPmQCEg8WAh8ABSw8bWV0YSBuYW1lPSd0d2l0dGVyOmNhcmQnIGNvbnRlbnQ9J3N1bW1hcnknPmQCFA8WAh8ABSo8bWV0YSBuYW1lPSd0d2l0dGVyOnNpdGUnIGNvbnRlbnQ9J0BTSFJNJz5kAhYPFgIfAAU0PG1ldGEgbmFtZT0ndHdpdHRlcjp0aXRsZScgY29udGVudD0nUHJpdmFjeSBQb2xpY3knPmQCGA8WAh8ABfYBPG1ldGEgbmFtZT0ndHdpdHRlcjpkZXNjcmlwdGlvbicgY29udGVudD0nIAogICAKICAgICAgJmFtcDsjMTYwO1ByaXZhY3kmYW1wOyMxNjA7U3RhdGVtZW50IFVwZGF0ZWQgT2N0b2JlciAxLCAyMDE4LldlbGNvbWUgdG8gdGhlIFNvY2lldHkgZm9yIEh1bWFuIFJlc291cmNlIE1hbmFnZW1lbnQgKFNIUk0pLiBTSFJNIGlzIHRoZSBsZWFkaW5nIG1lbWJlcnNoaXAgYXNzb2NpYXRpb24gZm9yIHRoZSBodW1hbiByZXNvdXJjZS4uLic+ZAIcDxYCHwAFazxtZXRhIG5hbWU9J3R3aXR0ZXI6aW1hZ2UnIGNvbnRlbnQ9J2h0dHBzOi8vd3d3LnNocm0ub3JnL3B1Ymxpc2hpbmdpbWFnZXMvc2hybS1zaGFyaW5nLWxvZ28tc3F1YXJlLXY1LnBuZyc+ZAIeDxYCHwAFNTxtZXRhIHByb3BlcnR5PSdvZzp0aXRsZScgY29udGVudD0nUHJpdmFjeSBQb2xpY3knIC8+ZAIgDxYCHwAFLTxtZXRhIHByb3BlcnR5PSdvZzp0eXBlJyBjb250ZW50PSdhcnRpY2xlJyAvPmQCIg8WAh8ABV48bWV0YSBwcm9wZXJ0eT0nb2c6dXJsJyBjb250ZW50PSdodHRwczovL3d3dy5zaHJtLm9yZy9hYm91dC1zaHJtL3BhZ2VzL3ByaXZhY3ktcG9saWN5LmFzcHgnIC8+ZAIkDxYCHwAFbDxtZXRhIHByb3BlcnR5PSdvZzppbWFnZScgY29udGVudD0naHR0cHM6Ly93d3cuc2hybS5vcmcvcHVibGlzaGluZ2ltYWdlcy9zaHJtLXNoYXJpbmctbG9nby1zcXVhcmUtdjUucG5nJyAvPmQCJg8WAh8ABfcBPG1ldGEgcHJvcGVydHk9J29nOmRlc2NyaXB0aW9uJyBjb250ZW50PScgCiAgIAogICAgICAmYW1wOyMxNjA7UHJpdmFjeSZhbXA7IzE2MDtTdGF0ZW1lbnQgVXBkYXRlZCBPY3RvYmVyIDEsIDIwMTguV2VsY29tZSB0byB0aGUgU29jaWV0eSBmb3IgSHVtYW4gUmVzb3VyY2UgTWFuYWdlbWVudCAoU0hSTSkuIFNIUk0gaXMgdGhlIGxlYWRpbmcgbWVtYmVyc2hpcCBhc3NvY2lhdGlvbiBmb3IgdGhlIGh1bWFuIHJlc291cmNlLi4uJyAvPmQCKA8WAh8ABS88bWV0YSBwcm9wZXJ0eT0nb2c6c2l0ZV9uYW1lJyBjb250ZW50PSdTSFJNJyAvPmQCKg8WAh8ABUk8bWV0YSBwcm9wZXJ0eT0nYXJ0aWNsZTpwdWJsaXNoZWRfdGltZScgY29udGVudD0nMTAvMi8yMDE4IDQ6Mjk6MzcgUE0nIC8+ZAIsDxYCHwAFSDxtZXRhIHByb3BlcnR5PSdhcnRpY2xlOm1vZGlmaWVkX3RpbWUnIGNvbnRlbnQ9JzEwLzIvMjAxOCA0OjI5OjM3IFBNJyAvPmQCMg8WAh8ABTc8bWV0YSBwcm9wZXJ0eT0nZmI6YXBwX2lkJyBjb250ZW50PSc2NDk4MTkyMzE4Mjc0MjAnIC8+ZAIDD2QWAmYPZBYCAgEPFgIfAAW9BTxzY3JpcHQgdHlwZT0nYXBwbGljYXRpb24vbGQranNvbic+eyAiQGNvbnRleHQiOiAiaHR0cDovL3NjaGVtYS5vcmciLCAiQHR5cGUiOiAiTmV3c0FydGljbGUiLCAibWFpbkVudGl0eU9mUGFnZSI6IHsgIkB0eXBlIjogIldlYlBhZ2UiLCAiQGlkIjogImh0dHBzOi8vd3d3LnNocm0ub3JnL2Fib3V0LXNocm0vUGFnZXMvUHJpdmFjeS1Qb2xpY3kuYXNweCJ9LCAiaGVhZGxpbmUiOiAiUHJpdmFjeSBQb2xpY3kiLCAiYXJ0aWNsZUJvZHkiOiAiIiwgImltYWdlIjogImh0dHBzOi8vd3d3LnNocm0ub3JnL1B1Ymxpc2hpbmdJbWFnZXMvc2hybS1zaGFyaW5nLWxvZ28tc3F1YXJlLXY1LnBuZyIsICJkYXRlUHVibGlzaGVkIjogIjEwLzIvMjAxOCAxMjoyOTozNyBQTSIsICJkYXRlTW9kaWZpZWQiOiAiMTAvMi8yMDE4IDEyOjI5OjM3IFBNIiwgImF1dGhvciI6IHsgIkB0eXBlIjogIlBlcnNvbiIsICJuYW1lIjogIlNIUk0iIH0sICJwdWJsaXNoZXIiOiB7ICJuYW1lIjogIlNIUk0iLCAiQHR5cGUiOiAiT3JnYW5pemF0aW9uIiwgImxvZ28iOiB7ICJAdHlwZSI6ICJJbWFnZU9iamVjdCIsICJ1cmwiOiAiaHR0cHM6Ly93d3cuc2hybS5vcmcvX2xheW91dHMvMTUvU0hSTS5Db3JlL2Rlc2lnbi9pbWFnZXMvU0hSTUxvZ28uanBnIiB9IH0sICJkZXNjcmlwdGlvbiI6ICIiLCAiaXNBY2Nlc3NpYmxlRm9yRnJlZSI6ICJUcnVlIn0gPC9zY3JpcHQ+ZAIDD2QWAgIBD2QWAmYPPCsABgBkAhwPZBYCZg8WAh8AZWQCHg9kFgJmD2QWAgIBDxYCHwAFcTxzY3JpcHQgdHlwZT0ndGV4dC9qYXZhc2NyaXB0Jz4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7UmVnaXN0ZXJHb29nbGVUYWcoJ0FkV29yZHNTY3JpcHQnLCAnJyk7fSk7PC9zY3JpcHQ+ZAIfD2QWAgIEDxYCHgdWaXNpYmxlZ2QCAw9kFgQCCQ9kFgoCAQ9kFgICFw9kFgICAw8WAh8CaBYCZg9kFgQCAg9kFgYCAQ8WAh8CaGQCAw8WAh8CaGQCBQ8WAh8CaGQCAw8PFgIeCUFjY2Vzc0tleQUBL2RkAgcPZBYIAggPZBYCZg8PFgYeCENzc0NsYXNzBT9yb3cgcHJvbW90aW9uYWwtYmFyIGhpZGRlbi14cyBoaWRkZW4tcHJpbnQgcHJvbW90aW9uYWwtYmFyLWNvcmUeBF8hU0ICAh8CZ2QWCAIBDxYCHwAFFUN5YmVyIE1vbmRheSBTcGVjaWFsIWQCAw8WAh8ABTpHZXQgJDI1IEFtYXpvbiBHaWZ0IENhcmQgd2hlbiB5b3UgcmVuZXcgd2l0aCBjb2RlIENZQkVSMTguZAIFDw8WBh8ABQlSRU5FVyBOT1ceC05hdmlnYXRlVXJsBVRodHRwczovL21lbWJlcnNoaXAuc2hybS5vcmcvP1BST0RVQ1RfRElTQ09VTlRfSUQ9Q1lCRVIxOCZ1dG1fY2FtcGFpZ249TWVtYmVyc2hpcF9SZXQfAmdkZAIHDw8WBB8AZR8GZWRkAgwPZBYIAgEPFgIeC18hSXRlbUNvdW50AgUWCmYPZBYCAgEPFgIeBWNsYXNzBQdkcm9wb3V0FgQCAQ8WAh8ABbUBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDBfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkhSIFRvZGF5PC9hPmQCAw9kFgYCAQ8WAh8ABbUBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDBfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkhSIFRvZGF5PC9hPmQCAw8WAh8HAgMWBmYPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAX3ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+TmV3czwvYT5kAgUPZBYEAgEPFgIfAAXdATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPk5ld3M8L2E+ZAIDDxYCHwcCBRYKZg9kFgICAQ9kFgJmDw8WBB8GBSkvaHItdG9kYXkvbmV3cy9oci1uZXdzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQdIUiBOZXdzZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBS0vaHItdG9kYXkvbmV3cy9oci1tYWdhemluZS9QYWdlcy9kZWZhdWx0LmFzcHgfAAULSFIgTWFnYXppbmVkZAICD2QWAgIBD2QWAmYPDxYEHwYFFWh0dHA6Ly9ibG9nLnNocm0ub3JnLx8ABQlTSFJNIEJsb2dkZAIDD2QWAgIBD2QWAmYPDxYGHwYFMmh0dHBzOi8vd3d3LnNocm0ub3JnL3Nocm0taW5kaWEvUGFnZXMvZGVmYXVsdC5hc3B4HwAFEFNIUk0gSW5kaWEgVG9kYXkfAmhkZAIED2QWAgIBD2QWAmYPDxYGHwYFG2h0dHBzOi8vYmxvZy5zaHJtLm9yZy9zYXNpYR8ABRRTSFJNIFNvdXRoIEFzaWEgQmxvZx8CaGRkAgcPFgIfAAUGPC9kaXY+ZAIBD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFgAI8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPlB1YmxpYyBQb2xpY3k8L2E+ZAIFD2QWBAIBDxYCHwAF5gE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5QdWJsaWMgUG9saWN5PC9hPmQCAw8WAh8HAgQWCGYPZBYCAgEPZBYCZg8PFgQfBgUhaHR0cDovL3d3dy5hZHZvY2FjeS5zaHJtLm9yZy9ob21lHwAFC1Rha2UgQWN0aW9uZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBUIvaHItdG9kYXkvcHVibGljLXBvbGljeS9oci1wdWJsaWMtcG9saWN5LWlzc3Vlcy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUXSFIgUHVibGljIFBvbGljeSBJc3N1ZXNkZAICD2QWAgIBD2QWAmYPDxYEHwYFImh0dHA6Ly93d3cuYWR2b2NhY3kuc2hybS5vcmcvYWJvdXQfAAUXQS1UZWFtIEFkdm9jYWN5IE5ldHdvcmtkZAIDD2QWAgIBD2QWAmYPDxYEHwYFOC9oci10b2RheS9wdWJsaWMtcG9saWN5L3N0YXRlLWFmZmFpcnMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFDVN0YXRlIEFmZmFpcnNkZAIHDxYCHwAFBjwvZGl2PmQCAg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYcCPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5UcmVuZHMgJiBGb3JlY2FzdGluZzwvYT5kAgUPZBYEAgEPFgIfAAXtATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlRyZW5kcyAmIEZvcmVjYXN0aW5nPC9hPmQCAw8WAh8HAgMWBmYPZBYCAgEPZBYCZg8PFgQfBgVIL2hyLXRvZGF5L3RyZW5kcy1hbmQtZm9yZWNhc3RpbmcvcmVzZWFyY2gtYW5kLXN1cnZleXMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFElJlc2VhcmNoICYgU3VydmV5c2RkAgEPZBYCAgEPZBYCZg8PFgQfBgVSL2hyLXRvZGF5L3RyZW5kcy1hbmQtZm9yZWNhc3RpbmcvbGFib3ItbWFya2V0LWFuZC1lY29ub21pYy1kYXRhL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRxMYWJvciBNYXJrZXQgJiBFY29ub21pYyBEYXRhZGQCAg9kFgICAQ9kFgJmDw8WBB8GBVQvaHItdG9kYXkvdHJlbmRzLWFuZC1mb3JlY2FzdGluZy9zcGVjaWFsLXJlcG9ydHMtYW5kLWV4cGVydC12aWV3cy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUeU3BlY2lhbCBSZXBvcnRzICYgRXhwZXJ0IFZpZXdzZGQCBw8WAh8ABQY8L2Rpdj5kAgUPZBYCZg8PFgIfAmcWAh4XZGF0YS1wZXJzb25hbGl6ZWQtb2ZmZXIFBUZhbHNlFgoCAQ8WAh8AZWQCAw8PFgQfBgVEaHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvUGFnZXMvSFItRmVhdHVyZWQtVG9waWNzLmFzcHgfAmdkFgJmDw8WBh4ISW1hZ2VVcmwFqAFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wLGhfMjUzLHdfNDUwLHhfMCx5XzAvd19hdXRvOjEwMDoyMjgscV9hdXRvLGZfYXV0by92MS9Ub29scyUyMGFuZCUyMFNhbXBsZXMvMThfMTQ4OV9IdXBQYWdlX1J1bGx1cF9Xb3JrcGxhY2VJbnZlc3RpZ2F0aW9uc19qbm4wc2EeB1Rvb2xUaXAFFUhSIFJlc291cmNlIFNwb3RsaWdodB4NQWx0ZXJuYXRlVGV4dAUVSFIgUmVzb3VyY2UgU3BvdGxpZ2h0ZGQCBQ8PFgIfBgVEaHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvUGFnZXMvSFItRmVhdHVyZWQtVG9waWNzLmFzcHhkFgJmDxYCHwAFFUhSIFJlc291cmNlIFNwb3RsaWdodGQCBw8WAh8ABYkB4oCLRmluZCBuZXdzICYgcmVzb3VyY2VzIG9uIHNwZWNpYWxpemVkIHdvcmtwbGFjZSB0b3BpY3MuIFZpZXcga2V5IHRvb2xraXRzLCBwb2xpY2llcywgcmVzZWFyY2ggYW5kIG1vcmUgb24gSFIgdG9waWNzIHRoYXQgbWF0dGVyIHRvIHlvdS5kAgkPDxYEHwYFRGh0dHBzOi8vd3d3LnNocm0ub3JnL1Jlc291cmNlc0FuZFRvb2xzL1BhZ2VzL0hSLUZlYXR1cmVkLVRvcGljcy5hc3B4HwBlZGQCAQ9kFgICAQ8WAh8IBQdkcm9wb3V0FgQCAQ8WAh8ABb4BPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDFfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlJlc291cmNlcyAmIFRvb2xzPC9hPmQCAw9kFgYCAQ8WAh8ABb4BPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDFfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlJlc291cmNlcyAmIFRvb2xzPC9hPmQCAw8WAh8HAgQWCGYPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAX8ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+SFIgVG9waWNzPC9hPmQCBQ9kFgQCAQ8WAh8ABeIBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+SFIgVG9waWNzPC9hPmQCAw8WAh8HAgwWGGYPZBYCAgEPZBYCZg8PFgQfBgVHL1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9iZWhhdmlvcmFsLWNvbXBldGVuY2llcy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUXQmVoYXZpb3JhbCBDb21wZXRlbmNpZXNkZAIBD2QWAgIBD2QWAmYPDxYEHwYFOC9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvYmVuZWZpdHMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFCEJlbmVmaXRzZGQCAg9kFgICAQ9kFgJmDw8WBB8GBTwvUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL1BhZ2VzL2NhbGlmb3JuaWEtcmVzb3VyY2VzLmFzcHgfAAUUQ2FsaWZvcm5pYSBSZXNvdXJjZXNkZAIDD2QWAgIBD2QWAmYPDxYEHwYFPC9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvY29tcGVuc2F0aW9uL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQxDb21wZW5zYXRpb25kZAIED2QWAgIBD2QWAmYPDxYEHwYFPy9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvUGFnZXMvZGl2ZXJzaXR5LWFuZC1pbmNsdXNpb24uYXNweB8ABRVEaXZlcnNpdHkgJiBJbmNsdXNpb25kZAIFD2QWAgIBD2QWAmYPDxYEHwYFQi9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvZW1wbG95ZWUtcmVsYXRpb25zL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRJFbXBsb3llZSBSZWxhdGlvbnNkZAIGD2QWAgIBD2QWAmYPDxYEHwYFOS9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvZ2xvYmFsLWhyL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQlHbG9iYWwgSFJkZAIHD2QWAgIBD2QWAmYPDxYEHwYFPy9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvbGFib3ItcmVsYXRpb25zL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQ9MYWJvciBSZWxhdGlvbnNkZAIID2QWAgIBD2QWAmYPDxYEHwYFVy9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3Mvb3JnYW5pemF0aW9uYWwtYW5kLWVtcGxveWVlLWRldmVsb3BtZW50L1BhZ2VzL2RlZmF1bHQuYXNweB8ABSVPcmdhbml6YXRpb25hbCAmIEVtcGxveWVlIERldmVsb3BtZW50ZGQCCQ9kFgICAQ9kFgJmDw8WBB8GBT8vUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL3Jpc2stbWFuYWdlbWVudC9QYWdlcy9kZWZhdWx0LmFzcHgfAAUPUmlzayBNYW5hZ2VtZW50ZGQCCg9kFgICAQ9kFgJmDw8WBB8GBUIvUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL3RhbGVudC1hY3F1aXNpdGlvbi9QYWdlcy9kZWZhdWx0LmFzcHgfAAUSVGFsZW50IEFjcXVpc2l0aW9uZGQCCw9kFgICAQ9kFgJmDw8WBB8GBTovUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL3RlY2hub2xvZ3kvUGFnZXMvZGVmYXVsdC5hc3B4HwAFClRlY2hub2xvZ3lkZAIHDxYCHwAFBjwvZGl2PmQCAQ9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYUCPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5MZWdhbCAmIENvbXBsaWFuY2U8L2E+ZAIFD2QWBAIBDxYCHwAF6wE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5MZWdhbCAmIENvbXBsaWFuY2U8L2E+ZAIDDxYCHwcCBBYIZg9kFgICAQ9kFgJmDw8WBB8GBUkvUmVzb3VyY2VzQW5kVG9vbHMvbGVnYWwtYW5kLWNvbXBsaWFuY2UvZW1wbG95bWVudC1sYXcvUGFnZXMvZGVmYXVsdC5hc3B4HwAFDkVtcGxveW1lbnQgTGF3ZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBVIvUmVzb3VyY2VzQW5kVG9vbHMvbGVnYWwtYW5kLWNvbXBsaWFuY2Uvc3RhdGUtYW5kLWxvY2FsLXVwZGF0ZXMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFFVN0YXRlICYgTG9jYWwgVXBkYXRlc2RkAgIPZBYCAgEPZBYCZg8PFgYfBgUhL3Nocm0taW5kaWEvUGFnZXMvc2ltcGxpYW5jZS5hc3B4HwAFIEluZGlhIExlZ2FsICYgQ29tcGxpYW5jZSBVcGRhdGVzHwJoZGQCAw9kFgICAQ9kFgJmDw8WBB8GBTMvUmVzb3VyY2VzQW5kVG9vbHMvUGFnZXMvd29ya3BsYWNlLWltbWlncmF0aW9uLmFzcHgfAAUVV29ya3BsYWNlIEltbWlncmF0aW9uZGQCBw8WAh8AZWQCAg9kFggCAQ8WAh8AZWQCAw8WAh8ABYUCPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5CdXNpbmVzcyBTb2x1dGlvbnM8L2E+ZAIFD2QWBAIBDxYCHwAF6wE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5CdXNpbmVzcyBTb2x1dGlvbnM8L2E+ZAIDDxYCHwcCCRYSZg9kFgICAQ9kFgJmDw8WBB8GBT0vUmVzb3VyY2VzQW5kVG9vbHMvYnVzaW5lc3Mtc29sdXRpb25zL1BhZ2VzL2JlbmNobWFya2luZy5hc3B4HwAFFEJlbmNobWFya2luZyBTZXJ2aWNlZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBR5odHRwczovL2Jyb2tlcmZpbmRlci5zaHJtLm9yZy8fAAUZQmVuZWZpdHMgQnJva2VyIERpcmVjdG9yeWRkAgIPZBYCAgEPZBYCZg8PFgQfBgVTL1Jlc291cmNlc0FuZFRvb2xzL2J1c2luZXNzLXNvbHV0aW9ucy9QYWdlcy9FbXBsb3llZS1FbmdhZ2VtZW50LVN1cnZleS1TZXJ2aWNlLmFzcHgfAAUaRW1wbG95ZWUgRW5nYWdlbWVudCBTdXJ2ZXlkZAIDD2QWAgIBD2QWAmYPDxYEHwYFRS9SZXNvdXJjZXNBbmRUb29scy9idXNpbmVzcy1zb2x1dGlvbnMvUGFnZXMvSi0xLVZpc2EtU3BvbnNvcnNoaXAuYXNweB8ABRRKLTEgVmlzYSBTcG9uc29yc2hpcGRkAgQPZBYCAgEPZBYCZg8PFgQfBgVEL1Jlc291cmNlc0FuZFRvb2xzL2J1c2luZXNzLXNvbHV0aW9ucy9QYWdlcy9TYWxhcnktRGF0YS1TZXJ2aWNlLmFzcHgfAAUTU2FsYXJ5IERhdGEgU2VydmljZWRkAgUPZBYCAgEPZBYCZg8PFgQfBgUVaHR0cHM6Ly90YWMuc2hybS5vcmcvHwAFGFRhbGVudCBBc3Nlc3NtZW50IENlbnRlcmRkAgYPZBYCAgEPZBYCZg8PFgQfBgUhaHR0cHM6Ly92ZW5kb3JkaXJlY3Rvcnkuc2hybS5vcmcvHwAFEFZlbmRvciBEaXJlY3RvcnlkZAIHD2QWAgIBD2QWAmYPDxYGHwYFMC9zaHJtLWluZGlhL2Fkdmlzb3J5LXNlcnZpY2VzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRxTSFJNIEluZGlhIEFkdmlzb3J5IFNlcnZpY2VzHwJoZGQCCA9kFgICAQ9kFgJmDw8WBh8GBTovc2hybS1pbmRpYS9hZHZpc29yeS1zZXJ2aWNlcy9QYWdlcy9SZXNlYXJjaF9TZXJ2aWNlcy5hc3B4HwAFHFNIUk0gSW5kaWEgUmVzZWFyY2ggU2VydmljZXMfAmhkZAIHDxYCHwAFBjwvZGl2PmQCAw9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYICPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5Ub29scyAmIFNhbXBsZXM8L2E+ZAIFD2QWBAIBDxYCHwAF6AE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5Ub29scyAmIFNhbXBsZXM8L2E+ZAIDDxYCHwcCDBYYZg9kFgICAQ9kFgJmDw8WBB8GBUIvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvUGFnZXMvZW1wbG95ZWUtaGFuZGJvb2tzLmFzcHgfAAUSRW1wbG95ZWUgSGFuZGJvb2tzZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBUUvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvZXhyZXEvUGFnZXMvVHJlbmRpbmctVG9waWNzLmFzcHgfAAUQRXhwcmVzcyBSZXF1ZXN0c2RkAgIPZBYCAgEPZBYCZg8PFgQfBgVFL1Jlc291cmNlc0FuZFRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL2hvdy10by1ndWlkZXMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFDUhvdy1UbyBHdWlkZXNkZAIDD2QWAgIBD2QWAmYPDxYEHwYFQC9SZXNvdXJjZXNBbmRUb29scy90b29scy1hbmQtc2FtcGxlcy9oci1mb3Jtcy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUISFIgRm9ybXNkZAIED2QWAgIBD2QWAmYPDxYEHwYFPS9SZXNvdXJjZXNBbmRUb29scy90b29scy1hbmQtc2FtcGxlcy9oci1xYS9QYWdlcy9kZWZhdWx0LmFzcHgfAAUHSFIgUSZBc2RkAgUPZBYCAgEPZBYCZg8PFgQfBgVLL1Jlc291cmNlc0FuZFRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL2ludGVydmlldy1xdWVzdGlvbnMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFE0ludGVydmlldyBRdWVzdGlvbnNkZAIGD2QWAgIBD2QWAmYPDxYEHwYFSC9SZXNvdXJjZXNBbmRUb29scy90b29scy1hbmQtc2FtcGxlcy9qb2ItZGVzY3JpcHRpb25zL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRBKb2IgRGVzY3JpcHRpb25zZGQCBw9kFgICAQ9kFgJmDw8WBB8GBUAvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvcG9saWNpZXMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFCFBvbGljaWVzZGQCCA9kFgICAQ9kFgJmDw8WBB8GBUUvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvcHJlc2VudGF0aW9ucy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUNUHJlc2VudGF0aW9uc2RkAgkPZBYCAgEPZBYCZg8PFgQfBgU8L1Jlc291cmNlc0FuZFRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL1BhZ2VzL3NwcmVhZHNoZWV0cy5hc3B4HwAFDFNwcmVhZHNoZWV0c2RkAgoPZBYCAgEPZBYCZg8PFgQfBgVAL1Jlc291cmNlc0FuZFRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL3Rvb2xraXRzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQhUb29sa2l0c2RkAgsPZBYCAgEPZBYCZg8PFgQfBgVFL1Jlc291cmNlc0FuZFRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL21lbWJlcjJtZW1iZXIvUGFnZXMvZGVmYXVsdC5hc3B4HwAFF01lbWJlcjJNZW1iZXIgU29sdXRpb25zZGQCBw8WAh8ABQY8L2Rpdj5kAgUPZBYCZg8PFgIfAmcWAh8JBQVGYWxzZRYKAgEPFgIfAAUXSk9CIERFU0NSSVBUSU9OIE1BTkFHRVJkAgMPDxYEHwYFM2h0dHBzOi8vc3RvcmUuc2hybS5vcmcvam9iLWRlc2NyaXB0aW9uLW1hbmFnZXIuaHRtbB8CZ2QWAmYPDxYGHwoFcmh0dHBzOi8vY2RuLnNocm0ub3JnL2ltYWdlL3VwbG9hZC9jX2Nyb3AsaF84OTksd18xNjAwLHhfMCx5XzAvd19hdXRvOjEwMDoyMjgscV9hdXRvLGZfYXV0by92MS9NYXJrZXRpbmcvSkRNX2RjaXBpch8LBRdKb2IgRGVzY3JpcHRpb24gTWFuYWdlch8MBRdKb2IgRGVzY3JpcHRpb24gTWFuYWdlcmRkAgUPDxYCHwYFM2h0dHBzOi8vc3RvcmUuc2hybS5vcmcvam9iLWRlc2NyaXB0aW9uLW1hbmFnZXIuaHRtbGQWAmYPFgIfAAUXSm9iIERlc2NyaXB0aW9uIE1hbmFnZXJkAgcPFgIfAAVMQ3JlYXRlLCBNYWludGFpbiAmIE9yZ2FuaXplIFlvdXIgSm9iIERlc2NyaXB0aW9ucy4gSXTigJlzIGZhc3QuIEl04oCZcyBlYXN5LmQCCQ8PFgQfBgUzaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9qb2ItZGVzY3JpcHRpb24tbWFuYWdlci5odG1sHwAFCkxFQVJOIE1PUkVkZAICD2QWAgIBDxYCHwgFB2Ryb3BvdXQWBAIBDxYCHwAFvgE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMl9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+TGVhcm5pbmcgJiBDYXJlZXI8L2E+ZAIDD2QWBgIBDxYCHwAFvgE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMl9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+TGVhcm5pbmcgJiBDYXJlZXI8L2E+ZAIDDxYCHwcCBRYKZg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABfkBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5DYXJlZXI8L2E+ZAIFD2QWBAIBDxYCHwAF3wE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5DYXJlZXI8L2E+ZAIDDxYCHwcCBRYKZg9kFgICAQ9kFgJmDw8WBB8GBTsvTGVhcm5pbmdBbmRDYXJlZXIvQ2FyZWVyL1BhZ2VzL0FjY2VsZXJhdGUtWW91ci1DYXJlZXIuYXNweB8ABRZBY2NlbGVyYXRlIFlvdXIgQ2FyZWVyZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBUQvTGVhcm5pbmdBbmRDYXJlZXIvQ2FyZWVyL1BhZ2VzL0NhcmVlci1QcmVwYXJhdGlvbi1hbmQtUGxhbm5pbmcuYXNweB8ABR1DYXJlZXIgUHJlcGFyYXRpb24gJiBQbGFubmluZ2RkAgIPZBYCAgEPZBYCZg8PFgQfBgU6L0xlYXJuaW5nQW5kQ2FyZWVyL0NhcmVlci9QYWdlcy9zaHJtLWNvbXBldGVuY3ktbW9kZWwuYXNweB8ABRVTSFJNIENvbXBldGVuY3kgTW9kZWxkZAIDD2QWAgIBD2QWAmYPDxYEHwYFQi9MZWFybmluZ0FuZENhcmVlci9DYXJlZXIvUGFnZXMvWW91ci1Qcm9mZXNzaW9uYWwtRGV2ZWxvcG1lbnQuYXNweB8ABR1Zb3VyIFByb2Zlc3Npb25hbCBEZXZlbG9wbWVudGRkAgQPZBYCAgEPZBYCZg8PFgQfBgU7L0xlYXJuaW5nQW5kQ2FyZWVyL0NhcmVlci9QYWdlcy9DYXJlZXItRXhwZXJ0LUluc2lnaHRzLmFzcHgfAAUWQ2FyZWVyIEV4cGVydCBJbnNpZ2h0c2RkAgcPFgIfAGVkAgEPZBYIAgEPFgIfAGVkAgMPFgIfAAX6ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+SFIgSm9iczwvYT5kAgUPZBYEAgEPFgIfAAXgATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkhSIEpvYnM8L2E+ZAIDDxYCHwcCBhYMZg9kFgICAQ9kFgJmDw8WBB8GBRNodHRwOi8vd3d3LnNocm0ub3JnHwAFCXVjY0hySm9ic2RkAgEPZBYCAgEPZBYCZg8PFgQfBgUVaHR0cDovL2pvYnMuc2hybS5vcmcvHwAFEkJyb3dzZSBBbGwgSm9icy4uLmRkAgIPZBYCAgEPFgIfAmgWAmYPDxYEHwYFE2h0dHA6Ly93d3cuc2hybS5vcmcfAAUBJGRkAgMPZBYCAgEPFgIfAmgWAmYPDxYEHwYFE2h0dHA6Ly93d3cuc2hybS5vcmcfAAUBJGRkAgQPZBYCAgEPFgIfAmgWAmYPDxYEHwYFE2h0dHA6Ly93d3cuc2hybS5vcmcfAAUBJGRkAgUPZBYCAgEPFgIfAmgWAmYPDxYEHwYFE2h0dHA6Ly93d3cuc2hybS5vcmcfAAUBJGRkAgcPFgIfAAUGPC9kaXY+ZAICD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAF+wE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkxlYXJuaW5nPC9hPmQCBQ9kFgQCAQ8WAh8ABeEBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+TGVhcm5pbmc8L2E+ZAIDDxYCHwcCDBYYZg9kFgICAQ9kFgJmDw8WBB8GBS8vTGVhcm5pbmdBbmRDYXJlZXIvbGVhcm5pbmcvUGFnZXMvU2VtaW5hcnMuYXNweB8ABQhTZW1pbmFyc2RkAgEPZBYCAgEPZBYCZg8PFgQfBgU+L0xlYXJuaW5nQW5kQ2FyZWVyL2xlYXJuaW5nL29uc2l0ZS10cmFpbmluZy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUPT25zaXRlIFRyYWluaW5nZGQCAg9kFgICAQ9kFgJmDw8WBB8GBTUvTGVhcm5pbmdBbmRDYXJlZXIvbGVhcm5pbmcvUGFnZXMvU0hSTS1lTGVhcm5pbmcuYXNweB8ABQllTGVhcm5pbmdkZAIDD2QWAgIBD2QWAmYPDxYEHwYFSS9MZWFybmluZ0FuZENhcmVlci9sZWFybmluZy9QYWdlcy9TSFJNLUVzc2VudGlhbHMtb2YtSHVtYW4tUmVzb3VyY2VzLmFzcHgfAAUiU0hSTSBFc3NlbnRpYWxzIG9mIEh1bWFuIFJlc291cmNlc2RkAgQPZBYCAgEPZBYCZg8PFgQfBgVGL0xlYXJuaW5nQW5kQ2FyZWVyL2xlYXJuaW5nL1BhZ2VzL1NIUk0tU2VuaW9yLUxlYWRlcnNoaXAtUHJvZ3JhbXMuYXNweB8ABRpTZW5pb3IgTGVhZGVyc2hpcCBQcm9ncmFtc2RkAgUPZBYCAgEPZBYCZg8PFgQfBgU9aHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9ldmVudHMvc2hybS1ldmVudHMvdmlydHVhbC1ldmVudHMuaHRtbB8ABQ5WaXJ0dWFsIEV2ZW50c2RkAgYPZBYCAgEPZBYCZg8PFgQfBgU3L0xlYXJuaW5nQW5kQ2FyZWVyL2xlYXJuaW5nL3dlYmNhc3RzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQhXZWJjYXN0c2RkAgcPZBYCAgEPZBYCZg8PFgQfBgU8L0xlYXJuaW5nQW5kQ2FyZWVyL2xlYXJuaW5nL1BhZ2VzL1NwZWNpYWx0eS1DcmVkZW50aWFscy5hc3B4HwAFFVNwZWNpYWx0eSBDcmVkZW50aWFsc2RkAggPZBYCAgEPZBYCZg8PFgYfBgUoL3Nocm0taW5kaWEvUGFnZXMvdHJhaW5pbmctcHJvZ3JhbXMuYXNweB8ABRlTSFJNIEluZGlhIFRyYWluaW5nIChQRFApHwJoZGQCCQ9kFgICAQ8WAh8CaBYCZg8PFgQfBgUTaHR0cDovL3d3dy5zaHJtLm9yZx8ABQEkZGQCCg9kFgICAQ8WAh8CaBYCZg8PFgQfBgUTaHR0cDovL3d3dy5zaHJtLm9yZx8ABQEkZGQCCw9kFgICAQ8WAh8CaBYCZg8PFgQfBgUTaHR0cDovL3d3dy5zaHJtLm9yZx8ABQEkZGQCBw8WAh8ABQY8L2Rpdj5kAgMPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAWAAjxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+Q2VydGlmaWNhdGlvbjwvYT5kAgUPZBYEAgEPFgIfAAXmATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkNlcnRpZmljYXRpb248L2E+ZAIDDxYCHwcCBRYKZg9kFgICAQ9kFgJmDw8WBB8GBTtodHRwczovL3d3dy5zaHJtLm9yZy9jZXJ0aWZpY2F0aW9uL2FwcGx5L3BhZ2VzL2RlZmF1bHQuYXNweB8ABQ5BcHBseSBmb3IgRXhhbWRkAgEPZBYCAgEPZBYCZg8PFgQfBgU+aHR0cHM6Ly93d3cuc2hybS5vcmcvY2VydGlmaWNhdGlvbi9sZWFybmluZy9wYWdlcy9kZWZhdWx0LmFzcHgfAAUZQ2VydGlmaWNhdGlvbiBQcmVwYXJhdGlvbmRkAgIPZBYCAgEPZBYCZg8PFgQfBgU6aHR0cHM6Ly93d3cuc2hybS5vcmcvY2VydGlmaWNhdGlvbi9mYXFzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRdTSFJNIENlcnRpZmljYXRpb24gRkFRc2RkAgMPZBYCAgEPZBYCZg8PFgQfBgVFaHR0cHM6Ly93d3cuc2hybS5vcmcvY2VydGlmaWNhdGlvbi9yZWNlcnRpZmljYXRpb24vcGFnZXMvZGVmYXVsdC5hc3B4HwAFD1JlY2VydGlmaWNhdGlvbmRkAgQPZBYCAgEPZBYCZg8PFgYfBgWCAWh0dHA6Ly94bHJpLWhyLnRhbGVudGVkZ2UuaW4vP3V0bV9zb3VyY2U9c2hybS1iMmMmdXRtX21lZGl1bT1zaHJtLWIyYyZ1dG1fdGVybT1zaHJtLWIyYyZ1dG1fY29udGVudD1zaHJtLWIyYyZ1dG1fY2FtcGFpZ249c2hybS1iMmMfAAUcU0hSTS1YTFJJIENlcnRpZmljYXRlIGluIEhSTR8CaGRkAgcPFgIfAGVkAgQPZBYIAgEPFgIfAGVkAgMPFgIfAAWAAjxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwNF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDA0X3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+Rm9yIEVkdWNhdG9yczwvYT5kAgUPZBYEAgEPFgIfAAXmATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwNF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDA0X3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkZvciBFZHVjYXRvcnM8L2E+ZAIDDxYCHwcCBBYIZg9kFgICAQ9kFgJmDw8WBB8GBTYvYWNhZGVtaWNpbml0aWF0aXZlcy91bml2ZXJzaXRpZXMvcGFnZXMvZ3VpZGVib29rLmFzcHgfAAUiSFIgQ3VycmljdWx1bSBHdWlkZWJvb2sgJiBUZW1wbGF0ZWRkAgEPZBYCAgEPZBYCZg8PFgQfBgU7L2FjYWRlbWljaW5pdGlhdGl2ZXMvc3R1ZGVudHMvcGFnZXMvaHJwcm9ncmFtZGlyZWN0b3J5LmFzcHgfAAUUSFIgUHJvZ3JhbSBEaXJlY3RvcnlkZAICD2QWAgIBD2QWAmYPDxYEHwYFUS9hY2FkZW1pY2luaXRpYXRpdmVzL3VuaXZlcnNpdGllcy90ZWFjaGluZ3Jlc291cmNlcy9wYWdlcy90ZXJtc29mdXNlX2ZhY3VsdHkuYXNweB8ABRJUZWFjaGluZyBSZXNvdXJjZXNkZAIDD2QWAgIBD2QWAmYPDxYGHwYFPi9zaHJtLWluZGlhL2Fkdmlzb3J5LXNlcnZpY2VzL1BhZ2VzL0FjYWRlbWljX1BhcnRuZXJzaGlwcy5hc3B4HwAFIFNIUk0gSW5kaWEgQWNhZGVtaWMgUGFydG5lcnNoaXBzHwJoZGQCBw8WAh8ABQY8L2Rpdj5kAgUPZBYCZg8PFgIfAmcWAh8JBQVGYWxzZRYKAgEPFgIfAAUXSU4tUEVSU09OIFNIUk0gU0VNSU5BUlNkAgMPDxYEHwYFTGh0dHBzOi8vc3RvcmUuc2hybS5vcmcvdHJhaW5pbmcvbGVhcm5pbmctY2VudGVyL3NlbWluYXJzL2ZpbmQtYS1zZW1pbmFyLmh0bWwfAmdkFgJmDw8WBh8KBY8BaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCxoXzE3MjMsd18zMDY1LHhfMCx5XzU0L3dfYXV0bzoxMDA6MjI4LHFfYXV0byxmX2F1dG8vdjEvSG9tZSUyMFBhZ2UlMjBDYXJvdXNlbC8xOC0wODk1X01hcF9pbWFnZV9veWFrZGQfCwUfTG9jYWwgRGV2ZWxvcG1lbnQgT3Bwb3J0dW5pdGllcx8MBR9Mb2NhbCBEZXZlbG9wbWVudCBPcHBvcnR1bml0aWVzZGQCBQ8PFgIfBgVMaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy90cmFpbmluZy9sZWFybmluZy1jZW50ZXIvc2VtaW5hcnMvZmluZC1hLXNlbWluYXIuaHRtbGQWAmYPFgIfAAUfTG9jYWwgRGV2ZWxvcG1lbnQgT3Bwb3J0dW5pdGllc2QCBw8WAh8ABZEBQnVpbGQgY29tcGV0ZW5jaWVzLCBlc3RhYmxpc2ggY3JlZGliaWxpdHkgYW5kIGFkdmFuY2UgeW91ciBjYXJlZXLigJR3aGlsZSBlYXJuaW5nIFBEQ3PigJRhdCBTSFJNIFNlbWluYXJzIGluIDE0IGNpdGllcyBhY3Jvc3MgdGhlIFUuUy4gdGhpcyBmYWxsLmQCCQ8PFgQfBgVMaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy90cmFpbmluZy9sZWFybmluZy1jZW50ZXIvc2VtaW5hcnMvZmluZC1hLXNlbWluYXIuaHRtbB8ABRpTRUUgMjAxOCBTRU1JTkFSIExPQ0FUSU9OU2RkAgMPZBYCAgEPFgIfCAUHZHJvcG91dBYEAgEPFgIfAAWzATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19wYW5fRHJvcG91dE1lbnUnIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5FdmVudHM8L2E+ZAIDD2QWBgIBDxYCHwAFswE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19wYW5fRHJvcG91dE1lbnUnIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDNfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+RXZlbnRzPC9hPmQCAw8WAh8HAgQWCGYPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAX+ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+U0hSTSBFdmVudHM8L2E+ZAIFD2QWBAIBDxYCHwAF5AE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5TSFJNIEV2ZW50czwvYT5kAgMPFgIfBwIJFhJmD2QWAgIBD2QWAmYPDxYEHwYFF2h0dHA6Ly9hbm51YWwuc2hybS5vcmcvHwAFI1NIUk0gQW5udWFsIENvbmZlcmVuY2UgJiBFeHBvc2l0aW9uZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBR0vbWxwL1BhZ2VzL0RpdmVyc2l0eTIwMTguYXNweB8ABS1EaXZlcnNpdHkgJiBJbmNsdXNpb24gQ29uZmVyZW5jZSAmIEV4cG9zaXRpb25kZAICD2QWAgIBD2QWAmYPDxYEHwYFM2h0dHBzOi8vY29uZmVyZW5jZXMuc2hybS5vcmcvbGVnaXNsYXRpdmUtY29uZmVyZW5jZR8ABSdFbXBsb3ltZW50IExhdyAmIExlZ2lzbGF0aXZlIENvbmZlcmVuY2VkZAIDD2QWAgIBD2QWAmYPDxYEHwYFNWh0dHBzOi8vd3d3LnNocm0ub3JnL21scC9QYWdlcy9TeW1wb3NpdW1EZXB0T25lLmFzcHg/HwAFHkhSIERlcGFydG1lbnQgb2YgT25lIFN5bXBvc2l1bWRkAgQPZBYCAgEPZBYCZg8PFgQfBgUtaHR0cDovL2NvbmZlcmVuY2VzLnNocm0ub3JnL3RhbGVudC1jb25mZXJlbmNlHwAFHlRhbGVudCBDb25mZXJlbmNlICYgRXhwb3NpdGlvbmRkAgUPZBYCAgEPZBYCZg8PFgQfBgUvL0V2ZW50cy9QYWdlcy9TdGF0ZS0tQWZmaWxpYXRlLUNvbmZlcmVuY2VzLmFzcHgfAAUdU3RhdGUgJiBBZmZpbGlhdGUgQ29uZmVyZW5jZXNkZAIGD2QWAgIBD2QWAmYPDxYEHwYFMWh0dHBzOi8vd3d3LnNocm0ub3JnL21scC9QYWdlcy9TeW1wb3NpbUNBSFIuYXNweD8fAAUZU3BvdGxpZ2h0IG9uIENBIFN5bXBvc2l1bWRkAgcPZBYCAgEPZBYCZg8PFgQfBgU3L0xlYXJuaW5nQW5kQ2FyZWVyL2xlYXJuaW5nL3dlYmNhc3RzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQhXZWJjYXN0c2RkAggPZBYCAgEPZBYCZg8PFgQfBgUaL0V2ZW50cy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUOU0VFIEFMTCBFVkVOVFNkZAIHDxYCHwAFBjwvZGl2PmQCAQ9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYICPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5FdmVudCBSZXNvdXJjZXM8L2E+ZAIFD2QWBAIBDxYCHwAF6AE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5FdmVudCBSZXNvdXJjZXM8L2E+ZAIDDxYCHwcCBxYOZg9kFgICAQ9kFgJmDw8WBB8GBTRodHRwczovL3Nocm0ub3JnL21scC9QYWdlcy9zcGVha2Vycy1idXJlYXUvSG9tZS5hc3B4HwAFD1NwZWFrZXJzIEJ1cmVhdWRkAgEPZBYCAgEPZBYCZg8PFgQfBgUmL0V2ZW50cy9QYWdlcy9TcGVha2VyLUluZm9ybWF0aW9uLmFzcHgfAAUeQ29uZmVyZW5jZSBTcGVha2VyIEluZm9ybWF0aW9uZGQCAg9kFgICAQ9kFgJmDw8WBB8GBS5odHRwOi8vY29uZmVyZW5jZXMuc2hybS5vcmcvZXhoaWJpdC1vci1zcG9uc29yHwAFI1Nwb25zb3JzaGlwICYgRXhoaWJpdG9yIEluZm9ybWF0aW9uZGQCAw9kFgICAQ9kFgJmDw8WBB8GBShodHRwOi8vbC5zaHJtLm9yZy9yZXF1ZXN0bW9yZWluZm9ybWF0aW9uHwAFElJlcXVlc3QgYSBCcm9jaHVyZWRkAgQPZBYCAgEPFgIfAmgWAmYPDxYEHwYFFWh0dHA6Ly93d3cuZ29vZ2xlLmNvbR8ABQEkZGQCBQ9kFgICAQ8WAh8CaBYCZg8PFgQfBgUVaHR0cDovL3d3dy5nb29nbGUuY29tHwAFASRkZAIGD2QWAgIBDxYCHwJoFgJmDw8WBB8GBRVodHRwOi8vd3d3Lmdvb2dsZS5jb20fAAUBJGRkAgcPFgIfAAUGPC9kaXY+ZAICD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFgAI8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkdsb2JhbCBFdmVudHM8L2E+ZAIFD2QWBAIBDxYCHwAF5gE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5HbG9iYWwgRXZlbnRzPC9hPmQCAw8WAh8HAgIWBGYPZBYCAgEPZBYCZg8PFgQfBgUsL0V2ZW50cy9zaHJtLWluZGlhLWV2ZW50cy9QYWdlcy9kZWZhdWx0LmFzcHgfAAURU0hSTSBJbmRpYSBFdmVudHNkZAIBD2QWAgIBD2QWAmYPDxYEHwYFIy9FdmVudHMvUGFnZXMvc2hybS1hcGFjLWV2ZW50cy5hc3B4HwAFEFNIUk0gQVBBQyBFdmVudHNkZAIHDxYCHwBlZAIDD2QWCAIBDxYCHwBlZAIDDxYCHwAFgwI8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkFmZmlsaWF0ZSBFdmVudHM8L2E+ZAIFD2QWBAIBDxYCHwAF6QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5BZmZpbGlhdGUgRXZlbnRzPC9hPmQCAw8WAh8HAgMWBmYPZBYCAgEPZBYCZg8PFgQfBgUeaHR0cHM6Ly93d3cuY2ZnaS5vcmcvc3ltcG9zaXVtHwAFDkNGR0kgU3ltcG9zaXVtZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBUdodHRwczovL2hycHMub3JnL2V4ZWN1dGl2ZS1ldmVudHMvc3RyYXRlZ2ljLWhyLWZvcnVtL3BhZ2VzL2RlZmF1bHQuYXNweB8ABSdIUiBQZW9wbGUgKyBTdHJhdGVneSBTdHJhdGVnaWMgSFIgRm9ydW1kZAICD2QWAgIBD2QWAmYPDxYEHwYFSmh0dHBzOi8vd3d3LmhycHMub3JnL2V4ZWN1dGl2ZS1ldmVudHMvYW5udWFsLWNvbmZlcmVuY2UvUGFnZXMvZGVmYXVsdC5hc3B4HwAFKzIwMTkgSFIgUGVvcGxlICsgU3RyYXRlZ3kgQW5udWFsIENvbmZlcmVuY2VkZAIHDxYCHwAFBjwvZGl2PmQCBQ9kFgJmDw8WAh8CZxYCHwkFBUZhbHNlFgoCAQ8WAh8ABRBTSFJNIENPTkZFUkVOQ0VTZAIDDw8WBB8GBSxodHRwczovL3Nocm0ub3JnL21scC9QYWdlcy9TeW1wb3NpbUNBSFIuYXNweB8CZ2QWAmYPDxYGHwoFhQFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wLGhfMjM3LHdfNDIyLHhfMCx5XzI2L3dfYXV0bzoxMDA6MjI4LHFfYXV0byxmX2F1dG8vdjEvTWFya2V0aW5nL0NhbGlmb3JuaWFfTmF2X0dyYXBoaWNfbmtyZHlxHwsFH1NIUk0gU3ltcG9zaXVtOiBTcG90bGlnaHQgT24gQ0EfDAUfU0hSTSBTeW1wb3NpdW06IFNwb3RsaWdodCBPbiBDQWRkAgUPDxYCHwYFLGh0dHBzOi8vc2hybS5vcmcvbWxwL1BhZ2VzL1N5bXBvc2ltQ0FIUi5hc3B4ZBYCZg8WAh8ABR9TSFJNIFN5bXBvc2l1bTogU3BvdGxpZ2h0IE9uIENBZAIHDxYCHwAFoAFKb2luIHVzIGluIFNhbiBGcmFuY2lzY28sIENBLCBEZWNlbWJlciA0IHRvIGxlYXJuIGhvdyB0byBzdGF5IGNvbXBsaWFudCB3aXRoIENhbGlmb3JuaWHigJlzIGV2ZXJjaGFuZ2luZyBsZWdpc2xhdGlvbiBhbmQgcmVndWxhdG9yeSBsYW5kc2NhcGUuIFNwYWNlIGlzIGxpbWl0ZWQuZAIJDw8WBB8GBSxodHRwczovL3Nocm0ub3JnL21scC9QYWdlcy9TeW1wb3NpbUNBSFIuYXNweB8ABQxSZWdpc3RlciBOb3dkZAIED2QWAgIBDxYCHwgFB2Ryb3BvdXQWBAIBDxYCHwAFtwE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwNF9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDRfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+TWVtYmVyc2hpcDwvYT5kAgMPZBYEAgEPFgIfAAW3ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwNF9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5NZW1iZXJzaGlwPC9hPmQCAw8WAh8HAgMWBmYPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAX+ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+Q29tbXVuaXRpZXM8L2E+ZAIFD2QWBAIBDxYCHwAF5AE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5Db21tdW5pdGllczwvYT5kAgMPFgIfBwIFFgpmD2QWAgIBD2QWAmYPDxYEHwYFGmh0dHA6Ly9jb21tdW5pdHkuc2hybS5vcmcvHwAFDFNIUk0gQ29ubmVjdGRkAgEPZBYCAgEPZBYCZg8PFgQfBgUzL01lbWJlcnNoaXAvY29tbXVuaXRpZXMvY2hhcHRlcnMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFCENoYXB0ZXJzZGQCAg9kFgICAQ9kFgJmDw8WBB8GBRRodHRwOi8vd3d3LmhycHMub3JnLx8ABRFFeGVjdXRpdmUgTmV0d29ya2RkAgMPZBYCAgEPZBYCZg8PFgQfBgVBL01lbWJlcnNoaXAvY29tbXVuaXRpZXMvaHIteW91bmctcHJvZmVzc2lvbmFscy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUWSFIgWW91bmcgUHJvZmVzc2lvbmFsc2RkAgQPZBYCAgEPZBYCZg8PFgQfBgUiaHR0cDovL3d3dy5hZHZvY2FjeS5zaHJtLm9yZy9hYm91dB8ABSJMZWdpc2xhdGl2ZSBBZHZvY2FjeSBUZWFtIChBLVRlYW0pZGQCBw8WAh8ABQY8L2Rpdj5kAgEPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAWEAjxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+U3R1ZGVudCBSZXNvdXJjZXM8L2E+ZAIFD2QWBAIBDxYCHwAF6gE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc2X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5TdHVkZW50IFJlc291cmNlczwvYT5kAgMPFgIfBwIBFgJmD2QWAgIBD2QWAmYPDxYEHwYFMC9NZW1iZXJzaGlwL3N0dWRlbnQtcmVzb3VyY2VzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRVTdHVkZW50IE1lbWJlciBDZW50ZXJkZAIHDxYCHwAFBjwvZGl2PmQCAg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABf0BPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5Wb2x1bnRlZXJzPC9hPmQCBQ9kFgQCAQ8WAh8ABeMBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzZfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3Nl9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+Vm9sdW50ZWVyczwvYT5kAgMPFgIfBwIEFghmD2QWAgIBD2QWAmYPDxYEHwYFPS9NZW1iZXJzaGlwL3ZvbHVudGVlcnMvbWVtYmVyc2hpcC1jb3VuY2lscy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUTTWVtYmVyc2hpcCBDb3VuY2lsc2RkAgEPZBYCAgEPZBYCZg8PFgQfBgVCL01lbWJlcnNoaXAvdm9sdW50ZWVycy9zcGVjaWFsLWV4cGVydGlzZS1wYW5lbHMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFGFNwZWNpYWwgRXhwZXJ0aXNlIFBhbmVsc2RkAgIPZBYCAgEPZBYCZg8PFgQfBgUfaHR0cDovL2NvbW11bml0eS5zaHJtLm9yZy92bHJjLx8ABSBWb2x1bnRlZXIgTGVhZGVyIFJlc291cmNlIENlbnRlcmRkAgMPZBYCAgEPZBYCZg8PFgQfBgU5L01lbWJlcnNoaXAvdm9sdW50ZWVycy9QYWdlcy9Wb2x1bnRlZXItT3Bwb3J0dW5pdGllcy5hc3B4HwAFF1ZvbHVudGVlciBPcHBvcnR1bml0aWVzZGQCBw8WAh8ABQY8L2Rpdj5kAgMPZBYIZg8PFgIfAmhkZAIBDw8WAh8CaGRkAgMPDxYEHwYFIS9hYm91dC1zaHJtL3BhZ2VzL21lbWJlcnNoaXAuYXNweB8ABQpNZW1iZXJzaGlwZGQCBA8PFgQfBgUhL2NlcnRpZmljYXRpb24vcGFnZXMvZGVmYXVsdC5hc3B4HwAFDUdFVCBDRVJUSUZJRURkZAIFD2QWAmYPZBYEAgEPFgIeB29uY2xpY2sFWHJldHVybiBPcGVuSW5OZXdXaW5kb3coJ2N0bDAwX2N0bDc2X0hySm9ic19zZWFyY2hKb2JzJywnY3RsMDBfY3RsNzZfSHJKb2JzX3NlYXJjaFRlcm0nKTtkAgIPDxYGHwAFClBvc3QgYSBKb2IfBgUkaHR0cDovL2hyam9icy5zaHJtLm9yZy9qb2JzL3Byb2R1Y3RzHwJnZGQCBw9kFgICAQ8PZBYCHw0FcXdpbmRvdy5sb2NhdGlvbj0nL3NlYXJjaC9wYWdlcy9Mb2NhbENoYXB0ZXIuYXNweD9sb2NhdGlvbj0nICsgJCh0aGlzKS5jbG9zZXN0KCcuZmluZGVyLWZvcm0nKS5maW5kKCdpbnB1dCcpLnZhbCgpZAIOD2QWJgIBD2QWAgICDxYGHwgFGHNocm0taGFzLXVybCBzaHJtLUtpY2tlch8NBSB3aW5kb3cubG9jYXRpb24gPSAnL2Fib3V0LXNocm0nOx8CZxYCZg8WAh8ABQpBYm91dCBTSFJNZAIDDxYCHwELKwQBZAIFDxYCHwELKwQBZAIHD2QWBgIBDxYCHwELKwQBZAIDDw8WAh8CaGQWBgIBDxYCHwgFD25vLWF1dGhvci1pbWFnZWQCAw8PFgIfAmhkZAIFDw8WAh8AZWRkAgUPDxYCHwJoZGQCDQ9kFgJmDxYCHwJoFgQCAQ8WAh8BCysEAWQCAw8WAh8CaBYCAgEPFgIfAQsrBAFkAg8PZBYCAgEPZBYCZg8PFgIfAmhkFgICAQ8WAh8HZmQCEQ8WAh8BCysEAWQCFQ9kFgICAw9kFgICDA8PFgQfBAUTc2Nyb2xsZXItYWQgbm9pbmRleB8FAgJkZAIXD2QWAmYPDxYEHwQFJnNocm0tdGFncyBzaHJtLXRhZ3MtZW1wdHkgYXJ0aWNsZS10YWdzHwUCAmQWAgIBDxYCHwdmZAIbD2QWBAINDxYCHwAFsgFIaSw8YnIgLz48YnIgLz4NCg0KSSB0aG91Z2h0IHlvdSdkIGxpa2UgdGhpcyBhcnRpY2xlIEkgZm91bmQgb24gdGhlIFNIUk0gd2Vic2l0ZTogPGJyIC8+DQo8YSBocmVmPSdodHRwczovL3d3dy5zaHJtLm9yZy9hYm91dC1zaHJtL1BhZ2VzL1ByaXZhY3ktUG9saWN5LmFzcHgnPlByaXZhY3kgUG9saWN5PC9hPiAgZAIPDxYCHgxkYXRhLXNpdGVrZXkFKDZMY1kwVDBVQUFBQUFNVm9PQVdSMXc0dVdYQWNkODBlZUlPOG8yOVJkAh0PZBYCZg8PFgIfAmdkFgYCAQ8PFgIfBgWFAWh0dHBzOi8vd3d3LnNocm0ub3JnL3Jlc291cmNlc2FuZHRvb2xzL2hyLXRvcGljcy9iZW5lZml0cy9wYWdlcy9hZ2VuY2llcy1wcmV2aWV3LWZvcm0tNTUwMC1mb3ItMjAxOS1maWxpbmdzLXdoaWxlLXJldmFtcC1kZWxheWVkLmFzcHhkZAIDDw8WAh8CZ2QWAgIBDw8WBh8KBXhodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wLGhfNDM4LHdfNzc5LHhfMCx5XzAvd19hdXRvOjEwMDoxNTAscV9hdXRvLGZfYXV0by92MS9CZW5lZml0cy9mb3JtX2ZpbGluZ192bzBzZmofCwVDQWdlbmNpZXMgUHJldmlldyBGb3JtIDU1MDAgZm9yIDIwMTkgRmlsaW5ncyBXaGlsZSBSZXZhbXAgSXMgRGVsYXllZB8MBUNBZ2VuY2llcyBQcmV2aWV3IEZvcm0gNTUwMCBmb3IgMjAxOSBGaWxpbmdzIFdoaWxlIFJldmFtcCBJcyBEZWxheWVkZGQCBQ8WAh8ABUNBZ2VuY2llcyBQcmV2aWV3IEZvcm0gNTUwMCBmb3IgMjAxOSBGaWxpbmdzIFdoaWxlIFJldmFtcCBJcyBEZWxheWVkZAIfD2QWAgICD2QWAgIBD2QWBAIHD2QWAgIBDxYCHwELKwQBZAIJD2QWAgIBDzwrAAkBAA8WAh4NTmV2ZXJFeHBhbmRlZGdkZAIjD2QWAmYPZBYEAgEPFgIfDQVscmV0dXJuIE9wZW5Jbk5ld1dpbmRvdygnY3RsMDBfUGxhY2VIb2xkZXJNYWluX0hySm9ic19zZWFyY2hKb2JzJywnY3RsMDBfUGxhY2VIb2xkZXJNYWluX0hySm9ic19zZWFyY2hUZXJtJyk7ZAICDw8WBh8ABQpQb3N0IGEgSm9iHwYFJGh0dHA6Ly9ocmpvYnMuc2hybS5vcmcvam9icy9wcm9kdWN0cx8CZ2RkAiUPZBYCAgEPFgIfBwIDFgZmD2QWBgIBDw8WAh8GBYcBaHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL2VtcGxveWVlLXJlbGF0aW9ucy9QYWdlcy9Db252aW5jaW5nLUNFT3MtdG8tTWFrZS1IYXJhc3NtZW50LVByZXZlbnRpb24tYS1Qcmlvcml0eS5hc3B4ZBYCZg8PFgYfCgXqAWh0dHBzOi8vY2RuLnNocm0ub3JnL2ltYWdlL3VwbG9hZC9jX2Nyb3AlMmNoXzQxNDAlMmN3XzczNjAlMmN4XzAlMmN5XzAvY19maXQlMmNmX2F1dG8lMmNxX2F1dG8lMmN3Xzc2Ny92MS9FbXBsb3llZSUyMFJlbGF0aW9ucy9oYXJhc3NtZW50X29tbzdwcj9kYXRhYnRvYT1leUl4Tm5nNUlqcDdJbmdpT2pBc0lua2lPakFzSW5neUlqbzNNell3TENKNU1pSTZOREUwTUN3aWR5STZOek0yTUN3aWFDSTZOREUwTUgxOR8LBThDb252aW5jaW5nIENFT3MgdG8gTWFrZSBIYXJhc3NtZW50IFByZXZlbnRpb24gYSBQcmlvcml0eR8MBThDb252aW5jaW5nIENFT3MgdG8gTWFrZSBIYXJhc3NtZW50IFByZXZlbnRpb24gYSBQcmlvcml0eWRkAgMPDxYEHwYFhwFodHRwczovL3d3dy5zaHJtLm9yZy9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvZW1wbG95ZWUtcmVsYXRpb25zL1BhZ2VzL0NvbnZpbmNpbmctQ0VPcy10by1NYWtlLUhhcmFzc21lbnQtUHJldmVudGlvbi1hLVByaW9yaXR5LmFzcHgfAAU4Q29udmluY2luZyBDRU9zIHRvIE1ha2UgSGFyYXNzbWVudCBQcmV2ZW50aW9uIGEgUHJpb3JpdHlkZAIFDxYCHwdmZAIBD2QWBgIBDw8WAh8GBVdodHRwczovL3d3dy5zaHJtLm9yZy9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvdGVjaG5vbG9neS9QYWdlcy9IUi1kYXRhLXNlY3VyaXR5LmFzcHhkFgJmDw8WBh8KBcQCaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCUyY2hfMTk2NCUyY3dfMzQ5MSUyY3hfMjY3JTJjeV82ODQvY19maXQlMmNmX2F1dG8lMmNxX2F1dG8lMmN3Xzc2Ny92MS9UZWNobm9sb2d5L2lTdG9jay05MTQ4Mzk0NThfbzZvdnd2P2RhdGFidG9hPWV5SXlNWGc1SWpwN0luZ2lPakFzSW5raU9qZzFPU3dpZURJaU9qTTNOVGdzSW5reUlqb3lORGN3TENKM0lqb3pOelU0TENKb0lqb3hOakV4ZlN3aU1UWjRPU0k2ZXlKNElqb3lOamNzSW5raU9qWTROQ3dpZURJaU9qTTNOVGdzSW5reUlqb3lOalE0TENKM0lqb3pORGt4TENKb0lqb3hPVFkwZlgwJTNkHwsFN1dvcmtwbGFjZSBEYXRhIFNlY3VyaXR5OiBJcyBZb3VyIEluZm9ybWF0aW9uIFByb3RlY3RlZD8fDAU3V29ya3BsYWNlIERhdGEgU2VjdXJpdHk6IElzIFlvdXIgSW5mb3JtYXRpb24gUHJvdGVjdGVkP2RkAgMPDxYEHwYFV2h0dHBzOi8vd3d3LnNocm0ub3JnL1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy90ZWNobm9sb2d5L1BhZ2VzL0hSLWRhdGEtc2VjdXJpdHkuYXNweB8ABTdXb3JrcGxhY2UgRGF0YSBTZWN1cml0eTogSXMgWW91ciBJbmZvcm1hdGlvbiBQcm90ZWN0ZWQ/ZGQCBQ8WAh8HZmQCAg9kFgYCAQ8PFgIfBgVvaHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL2JlbmVmaXRzL1BhZ2VzL2NvbW11dGluZy1hbmQtYWRvcHRpb24tYmVuZWZpdC1saW1pdHMtMjAxOS5hc3B4ZBYCZg8PFgYfCgXcAWh0dHBzOi8vY2RuLnNocm0ub3JnL2ltYWdlL3VwbG9hZC9jX2Nyb3AlMmNoXzQwOCUyY3dfNzI0JTJjeF8wJTJjeV8yOC9jX2ZpdCUyY2ZfYXV0byUyY3FfYXV0byUyY3dfNzY3L3YxL0JlbmVmaXRzL2NvbW11dGVfY2hpbGRfaWZ1cXpkP2RhdGFidG9hPWV5SXhObmc1SWpwN0luZ2lPakFzSW5raU9qSTRMQ0o0TWlJNk56STBMQ0o1TWlJNk5ETTJMQ0ozSWpvM01qUXNJbWdpT2pRd09IMTkfCwUzQ29tbXV0aW5nIGFuZCBBZG9wdGlvbiBCZW5lZml0IEFtb3VudHMgUmlzZSBpbiAyMDE5HwwFM0NvbW11dGluZyBhbmQgQWRvcHRpb24gQmVuZWZpdCBBbW91bnRzIFJpc2UgaW4gMjAxOWRkAgMPDxYEHwYFb2h0dHBzOi8vd3d3LnNocm0ub3JnL1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9iZW5lZml0cy9QYWdlcy9jb21tdXRpbmctYW5kLWFkb3B0aW9uLWJlbmVmaXQtbGltaXRzLTIwMTkuYXNweB8ABTNDb21tdXRpbmcgYW5kIEFkb3B0aW9uIEJlbmVmaXQgQW1vdW50cyBSaXNlIGluIDIwMTlkZAIFDxYCHwdmZAIpD2QWBGYPDxYEHwQFOHJvdy1wcm9tby1wYW5lbCBzaHJtLXdpZGdldCBjb2wtc20tMTIgY29sLW1kLTEyIG5vaW5kZXggHwUCAmQWCAIBDxYCHwAFD01FTUJFUiBCRU5FRklUU2QCAw8PFgQfBgU6aHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wYWdlcy9hZmZpbml0eXByb2dyYW0uYXNweB8CZ2QWAmYPDxYGHwoFxQJodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF8xMTkyJTJjd18yMTIxJTJjeF8wJTJjeV8wL2NfZml0JTJjZl9hdXRvJTJjcV9hdXRvJTJjd183NjcvdjEvSG9tZSUyMFBhZ2UlMjBDYXJvdXNlbC9UcmF2ZWxfUmVkUGxhaWRfb2RvbmxjP2RhdGFidG9hPWV5SXlNWGc1SWpwN0luZ2lPakFzSW5raU9qVTJMQ0o0TWlJNk1qRXlNU3dpZVRJaU9qazJOU3dpZHlJNk1qRXlNU3dpYUNJNk9UQTRmU3dpTVRaNE9TSTZleUo0SWpvd0xDSjVJam93TENKNE1pSTZNakV5TVN3aWVUSWlPakV4T1RJc0luY2lPakl4TWpFc0ltZ2lPakV4T1RKOWZRJTNkJTNkHwsFpwFUaGUgU0hSTSBNZW1iZXIgRGlzY291bnRzIHByb2dyYW0gcHJvdmlkZXMgbWVtYmVyLW9ubHkgYWNjZXNzIHRvIGRpc2NvdW50cyBvbiBwcm9kdWN0cyBhbmQgc2VydmljZXMgeW91IGNhbiBhcHBseSB0byB5b3VyIGxpZmUgYW5kIGNhcmVlciwgYW5kIHNoYXJlIHdpdGggeW91ciBjb21wYW55Lh8MBacBVGhlIFNIUk0gTWVtYmVyIERpc2NvdW50cyBwcm9ncmFtIHByb3ZpZGVzIG1lbWJlci1vbmx5IGFjY2VzcyB0byBkaXNjb3VudHMgb24gcHJvZHVjdHMgYW5kIHNlcnZpY2VzIHlvdSBjYW4gYXBwbHkgdG8geW91ciBsaWZlIGFuZCBjYXJlZXIsIGFuZCBzaGFyZSB3aXRoIHlvdXIgY29tcGFueS5kZAIFDxYCHwAFpwFUaGUgU0hSTSBNZW1iZXIgRGlzY291bnRzIHByb2dyYW0gcHJvdmlkZXMgbWVtYmVyLW9ubHkgYWNjZXNzIHRvIGRpc2NvdW50cyBvbiBwcm9kdWN0cyBhbmQgc2VydmljZXMgeW91IGNhbiBhcHBseSB0byB5b3VyIGxpZmUgYW5kIGNhcmVlciwgYW5kIHNoYXJlIHdpdGggeW91ciBjb21wYW55LmQCBw8PFgQfBgU6aHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wYWdlcy9hZmZpbml0eXByb2dyYW0uYXNweB8ABQpMRUFSTiBNT1JFZGQCAg8PFgQfBAU3cm93LXByb21vLXBhbmVsIHNocm0td2lkZ2V0IGNvbC1zbS02IGNvbC1tZC0xMiBub2luZGV4IB8FAgJkZAIvD2QWAmYPDxYCHwJoZGQCMQ9kFgRmDw8WAh8CZ2QWBgIBDw8WBB8ABSdGaW5kIHRoZSBSaWdodCBWZW5kb3IgZm9yIFlvdXIgSFIgTmVlZHMfBgUgaHR0cDovL3ZlbmRvcmRpcmVjdG9yeS5zaHJtLm9yZy9kZAIDDxYCHwAFO1NIUk3igJlzIEhSIFZlbmRvciBEaXJlY3RvcnkgY29udGFpbnMgb3ZlciAxMCwwMDAgY29tcGFuaWVzZAIFDw8WBB8GBSBodHRwOi8vdmVuZG9yZGlyZWN0b3J5LnNocm0ub3JnLx8ABRRTZWFyY2ggJmFtcDsgQ29ubmVjdGRkAgIPFgIfAAVJPHNjcmlwdCBzcmM9Jy8vd3d3LnNocm0ub3JnL0RvY3VtZW50cy9TcG9uc29yT2ZmZXJGZXRjaEZpbGUuanMnPjwvc2NyaXB0PmQCMw8PFgIfAmhkZAI1D2QWAgIED2QWBAIDDxYCHgVWYWx1ZQUkMDVmNzU2YTYtNzA5MC00ZDAzLTk3NmYtYTQzMGY1NDViNThlZAIFDxYCHxAFBUZhbHNlZAIQD2QWEgIBDw8WAh8GBSRodHRwczovL2xwLnNocm0ub3JnL3ByZWZlcmVuY2VzLmh0bWxkZAIDDxYCHwAFFUpPSU4gVEhFIENPTlZFUlNBVElPTmQCBQ8PFgIfBgU5aHR0cDovL3d3dy5mYWNlYm9vay5jb20vc29jaWV0eWZvcmh1bWFucmVzb3VyY2VtYW5hZ2VtZW50ZGQCBw8PFgIfBgU3aHR0cDovL3d3dy5saW5rZWRpbi5jb20vY29tcGFueS8xMTI4Mj90cms9TlVTX0NNUFlfVFdJVGRkAgkPDxYCHwYFI2h0dHBzOi8vaW5zdGFncmFtLmNvbS9zaHJtb2ZmaWNpYWwvZGQCCw8PFgIfBgUjaHR0cDovL3d3dy55b3V0dWJlLmNvbS9zaHJtb2ZmaWNpYWxkZAINDw8WAh8GBRovYWJvdXQtc2hybS9wYWdlcy9yc3MuYXNweGRkAg8PDxYCHwYFF2h0dHA6Ly90d2l0dGVyLmNvbS9TSFJNZGQCEQ8WAh8ABZYoPGRpdiBjbGFzcz0iZm9vdGVyLW5hdiBjbGVhcmZpeCI+DQo8ZGl2IGNsYXNzPSJjb2wtbWQtOSBmb290ZXItY29sIGZvb3Rlci1jb2wtYWJvdXQgY2xlYXJmaXgiPg0KPGRpdiBjbGFzcz0icm93Ij4NCjxkaXYgY2xhc3M9ImNvbC1zbS00IGNvbC14cy0xMiI+DQo8ZGl2IGNsYXNzPSJjbGVhcmZpeCI+DQo8ZGl2IGNsYXNzPSJyb3ciPg0KPGg0IGNsYXNzPSJmb290ZXItY29sLXRpdGxlIGNvbC14cy0xMiI+U0hSTTwvaDQ+DQo8dWwgY2xhc3M9ImNvbC14cy0xMiI+DQo8bGkgY2xhc3M9ImNvbC14cy02IGNvbC1zbS0xMiI+PGEgaHJlZj0iaHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9QYWdlcy9kZWZhdWx0LmFzcHgiPkFib3V0IFNIUk08L2E+PC9saT4NCjxsaSBjbGFzcz0iY29sLXhzLTYgY29sLXNtLTEyIj48YSBocmVmPSJodHRwczovL3d3dy5zaHJtLm9yZy9hYm91dC1zaHJtL1BhZ2VzL01lbWJlcnNoaXAuYXNweCI+TWVtYmVyc2hpcDwvYT48L2xpPg0KPGxpIGNsYXNzPSJjb2wteHMtNiBjb2wtc20tMTIiPjxhIGhyZWY9Imh0dHBzOi8vd3d3LnNocm0ub3JnL2Fib3V0LXNocm0vUGFnZXMvQnlsYXdzLS1Db2RlLW9mLUV0aGljcy5hc3B4Ij5CeWxhd3MgJmFtcDsgQ29kZSBvZiBFdGhpY3M8L2E+PC9saT4NCjxsaSBjbGFzcz0iY29sLXhzLTYgY29sLXNtLTEyIj48YSBocmVmPSJodHRwczovL3d3dy5zaHJtLm9yZy9hYm91dC1zaHJtL3ByZXNzLXJvb20vUGFnZXMvZGVmYXVsdC5hc3B4Ij5QcmVzcyBSb29tPC9hPjwvbGk+DQo8bGkgY2xhc3M9ImNvbC14cy02IGNvbC1zbS0xMiI+PGEgaHJlZj0iaHR0cDovL3d3dy5jZmdpLm9yZy8iPkNvdW5jaWwgZm9yIEdsb2JhbCBJbW1pZ3JhdGlvbjwvYT48L2xpPg0KPGxpIGNsYXNzPSJjb2wteHMtNiBjb2wtc20tMTIiPjxhIGhyZWY9Imh0dHA6Ly93d3cuaHJwcy5vcmcvIj5IUiBQZW9wbGUgKyBTdHJhdGVneTwvYT48L2xpPg0KPGxpIGNsYXNzPSJkcm9wZG93biBjb2wteHMtNiBjb2wtc20tMTIgbWFyZy1oLTAiPjxhIGlkPSJmb290ZXItaW50ZXJuYXRpb25hbCIgY2xhc3M9ImRyb3Bkb3duLXRvZ2dsZSIgaHJlZj0iIyIgZGF0YS10b2dnbGU9ImRyb3Bkb3duIiBhcmlhLWhhc3BvcHVwPSJ0cnVlIiBhcmlhLWV4cGFuZGVkPSJmYWxzZSI+IDxzcGFuIGNsYXNzPSJjdXJyZW50LXNpdGUiPlNIUk0gR2xvYmFsIDwvc3Bhbj48ZW0gY2xhc3M9ImZhIGZhLWFuZ2xlLWRvd24iPiZuYnNwOzwvZW0+IDwvYT4NCjx1bCBjbGFzcz0iZHJvcGRvd24tbWVudSIgYXJpYS1sYWJlbGxlZGJ5PSJmb290ZXItaW50ZXJuYXRpb25hbCI+DQo8bGkgY2xhc3M9ImNvbGxhcHNlIGluIj48YSBocmVmPSIuLi8uLi8uLi8uLi9wYWdlcy9kZWZhdWx0LmFzcHg/bG9jPW51bGwiPlNIUk0gR2xvYmFsPC9hPjwvbGk+DQo8bGkgY2xhc3M9ImNvbGxhcHNlIGluIiBkYXRhLWxvYz0iL3BhZ2VzL2RlZmF1bHQuYXNweD9sb2M9aW5kaWEiPjxhIGhyZWY9Ii4uLy4uLy4uLy4uL3BhZ2VzL2RlZmF1bHQuYXNweD9sb2M9aW5kaWEiPlNIUk0gSW5kaWE8L2E+PC9saT4NCjxsaSBjbGFzcz0iY29sbGFwc2UgaW4iIGRhdGEtbG9jPSIvYWJvdXQtc2hybS9QYWdlcy9TSFJNLU1FTkEuYXNweCI+PGEgaHJlZj0iLi4vLi4vLi4vLi4vYWJvdXQtc2hybS9QYWdlcy9TSFJNLU1FTkEuYXNweCI+U0hSTSBNRU5BPC9hPjwvbGk+DQo8L3VsPg0KPC9saT4NCjwvdWw+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9kaXY+DQo8ZGl2IGNsYXNzPSJjb2wtc20tNCBjb2wteHMtMTIiPg0KPGRpdiBjbGFzcz0icm93Ij4NCjxkaXYgY2xhc3M9ImNsZWFyZml4IGNvbC1zbS0xMiBjb2wteHMtNiI+DQo8aDQgY2xhc3M9ImZvb3Rlci1jb2wtdGl0bGUiPldPUksgRk9SIFNIUk08L2g0Pg0KPHVsIGNsYXNzPSJjb2wtc20tMTIiPg0KPGxpPjxhIGNsYXNzPSJsaW5rLWV4dGVybmFsIiBocmVmPSIvL3d3dy5zaHJtLmpvYnMiIHRhcmdldD0iX2JsYW5rIiByZWw9Im5vb3BlbmVyIj5DYXJlZXIgT3Bwb3J0dW5pdGllczwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg0KPGRpdiBjbGFzcz0iY2xlYXJmaXggY29sLXNtLTEyIGNvbC14cy02Ij4NCjxoNCBjbGFzcz0iZm9vdGVyLWNvbC10aXRsZSI+RUxFVkFURSBIUjwvaDQ+DQo8dWwgY2xhc3M9ImNvbC1zbS0xMiI+DQo8bGk+PGEgaHJlZj0iLi4vLi4vLi4vLi4vZm91bmRhdGlvbi9QYWdlcy9kZWZhdWx0LmFzcHgiPlNIUk0gRm91bmRhdGlvbjwvYT4mbmJzcDsmbmJzcDs8YnIgY2xhc3M9InZpc2libGUteHMiIC8+PGEgY2xhc3M9ImJ0biBidG4tc3VjY2VzcyBidG4tY3RhIiBocmVmPSJodHRwOi8vd3d3LnNocm0ub3JnL2Fib3V0L2ZvdW5kYXRpb24vc3VwcG9ydHRoZWZvdW5kYXRpb24vY29udHJpYnV0aW9ucy9wYWdlcy9kZWZhdWx0LmFzcHgiIHRhcmdldD0iX2JsYW5rIiByZWw9Im5vb3BlbmVyIj5ET05BVEU8L2E+PC9saT4NCjwvdWw+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9kaXY+DQo8ZGl2IGNsYXNzPSJjb2wtc20tNCBjb2wteHMtMTIiPg0KPGRpdiBjbGFzcz0iY2xlYXJmaXgiPg0KPGRpdiBjbGFzcz0icm93Ij4NCjxoNCBjbGFzcz0iZm9vdGVyLWNvbC10aXRsZSBjb2wteHMtMTIiPldPUksgV0lUSCBTSFJNPC9oND4NCjx1bCBjbGFzcz0iY29sLXhzLTEyIj4NCjxsaSBjbGFzcz0iY29sLXNtLTEyIGNvbC14cy02Ij48YSBjbGFzcz0ibGluay1leHRlcm5hbCIgaHJlZj0iLi4vLi4vLi4vLi4vbWxwL1BhZ2VzL3NwZWFrZXJzLWJ1cmVhdS9Ib21lLmFzcHgiIHRhcmdldD0iX2JsYW5rIiByZWw9Im5vb3BlbmVyIj5TcGVha2VycyBCdXJlYXU8L2E+PC9saT4NCjxsaSBjbGFzcz0iY29sLXNtLTEyIGNvbC14cy02Ij48YSBjbGFzcz0ibGluay1leHRlcm5hbCIgaHJlZj0iLi4vLi4vLi4vLi4vYWJvdXQtc2hybS9wYWdlcy9jb3B5cmlnaHQtLXBlcm1pc3Npb25zLmFzcHgiIHRhcmdldD0iX2JsYW5rIiByZWw9Im5vb3BlbmVyIj5Db3B5cmlnaHQgJmFtcDsgUGVybWlzc2lvbnM8L2E+PC9saT4NCjxsaSBjbGFzcz0iY29sLXNtLTEyIGNvbC14cy02Ij48YSBjbGFzcz0ibGluay1leHRlcm5hbCIgaHJlZj0iLi4vLi4vLi4vLi4vbWVkaWFraXQvcGFnZXMvZGVmYXVsdC5hc3B4IiB0YXJnZXQ9Il9ibGFuayIgcmVsPSJub29wZW5lciI+QWR2ZXJ0aXNlIHdpdGggVXM8L2E+PC9saT4NCjxsaSBjbGFzcz0iY29sLXNtLTEyIGNvbC14cy02Ij48YSBjbGFzcz0ibGluay1leHRlcm5hbCIgaHJlZj0iaHR0cDovL2hyam9icy5zaHJtLm9yZy9qb2JzL3Byb2R1Y3RzIiB0YXJnZXQ9Il9ibGFuayIgcmVsPSJub29wZW5lciI+UG9zdCBhIEpvYjwvYT48L2xpPg0KPC91bD4NCjwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjwvZGl2Pg0KPC9kaXY+DQo8L2Rpdj4NCjxkaXYgY2xhc3M9ImZvb3Rlci1ib3R0b20gY2xlYXJmaXggdGV4dC1jZW50ZXIiPg0KPGRpdiBjbGFzcz0iZm9vdGVyLWNvbnRhY3QtdXMiPjxhIGlkPSJjdGwwMF9jdGw3N19obF9Db250YWN0VXNfUGFnZSIgaHJlZj0iLi4vLi4vLi4vLi4vYWJvdXQtc2hybS9QYWdlcy9Db250YWN0LVVzLmFzcHgiPkNvbnRhY3QgVXM8L2E+IDxzcGFuIGNsYXNzPSJsaW5rcy1kaXZpZGVyIj58PC9zcGFuPiA8YSBocmVmPSJ0ZWw6ODAwLjI4My5TSFJNJTIwKDc0NzYpIj44MDAuMjgzLlNIUk0gKDc0NzYpPC9hPjwvZGl2Pg0KPGRpdiBjbGFzcz0ibGVnYWwtbmF2Ij48c3BhbiBjbGFzcz0iY29weXJpZ2h0Ij4mY29weTsNCjxzY3JpcHQ+dmFyIGN1cnJlbnRZZWFyID0gbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpO2RvY3VtZW50LndyaXRlKGN1cnJlbnRZZWFyKTs8L3NjcmlwdD4NClNIUk0uIEFsbCBSaWdodHMgUmVzZXJ2ZWQ8L3NwYW4+DQo8dWwgY2xhc3M9Imxpc3QtdW5zdHlsZWQgbGVnYWwtbGlua3MiIHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7Ij4NCjxsaT48YSBocmVmPSIuLi8uLi8uLi8uLi9hYm91dC1zaHJtL1BhZ2VzL1ByaXZhY3ktUG9saWN5LmFzcHgiPlByaXZhY3kgUG9saWN5PC9hPjwvbGk+DQo8bGk+PHNwYW4gY2xhc3M9ImxpbmtzLWRpdmlkZXIiPnw8L3NwYW4+PC9saT4NCjxsaT48YSBocmVmPSIuLi8uLi8uLi8uLi9hYm91dC1zaHJtL1BhZ2VzL1ByaXZhY3ktUG9saWN5LmFzcHgjQ2FsaWZvcm5pYSI+WW91ciBDYWxpZm9ybmlhIFByaXZhY3kgUmlnaHRzPC9hPjwvbGk+DQo8bGk+PHNwYW4gY2xhc3M9ImxpbmtzLWRpdmlkZXIiPnw8L3NwYW4+PC9saT4NCjxsaT48YSBocmVmPSIuLi8uLi8uLi8uLi9hYm91dC1zaHJtL1BhZ2VzL1Rlcm1zLW9mLVVzZS5hc3B4Ij5UZXJtcyBvZiBVc2U8L2E+PC9saT4NCjxsaT48c3BhbiBjbGFzcz0ibGlua3MtZGl2aWRlciI+fDwvc3Bhbj48L2xpPg0KPGxpPjxhIGhyZWY9Ii4uLy4uLy4uLy4uL2Fib3V0LXNocm0vUGFnZXMvU2l0ZS1NYXAuYXNweCI+U2l0ZSBNYXA8L2E+PC9saT4NCjwvdWw+DQo8L2Rpdj4NCjxzbWFsbCBjbGFzcz0idGV4dC1jZW50ZXIgc21hbGwiPlNIUk0gcHJvdmlkZXMgY29udGVudCBhcyBhIHNlcnZpY2UgdG8gaXRzIHJlYWRlcnMgYW5kIG1lbWJlcnMuIEl0IGRvZXMgbm90IG9mZmVyIGxlZ2FsIGFkdmljZSwgYW5kIGNhbm5vdCBndWFyYW50ZWUgdGhlIGFjY3VyYWN5IG9yIHN1aXRhYmlsaXR5IG9mIGl0cyBjb250ZW50IGZvciBhIHBhcnRpY3VsYXIgcHVycG9zZS4gPGEgaHJlZj0iLi4vLi4vLi4vLi4vYWJvdXQtc2hybS9QYWdlcy9UZXJtcy1vZi1Vc2UuYXNweCNEaXNjbGFpbWVyIj5EaXNjbGFpbWVyPC9hPjwvc21hbGw+PC9kaXY+DQo8cD4NCjxzY3JpcHQ+DQokKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7DQp2YXIgY3VycmVudFVybCA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmLnRvTG93ZXJDYXNlKCk7DQp2YXIgY3VycmVudExvY2F0aW9uID0gZ2V0Q29va2llKCJTSFJNX0NvcmVfQ3VycmVudFVzZXJfTG9jYXRpb25JRCIpOw0KaWYoY3VycmVudFVybC5pbmRleE9mKCIvYWJvdXQtc2hybS9wYWdlcy9zaHJtLWNoaW5hLmFzcHgiKSA+IC0xKSB7DQokKCJzcGFuLmN1cnJlbnQtc2l0ZSIpLmh0bWwoIlNIUk0gQ2hpbmEgIik7DQp9DQplbHNlIGlmKGN1cnJlbnRVcmwuaW5kZXhPZigiL2Fib3V0LXNocm0vcGFnZXMvc2hybS1tZW5hLmFzcHgiKSA+IC0xKSB7DQokKCJzcGFuLmN1cnJlbnQtc2l0ZSIpLmh0bWwoIlNIUk0gTUVOQSAiKTsNCn0NCn0pOw0KPC9zY3JpcHQ+DQo8L3A+ZAILDw8WAh8CaGRkAhEPZBYCAgEPZBYCAgEPPCsABQEADxYCHhVQYXJlbnRMZXZlbHNEaXNwbGF5ZWRmZGQCNQ9kFgICAQ9kFgICDQ8PFgIfAmhkFgICAg9kFgJmD2QWAgIDD2QWAgIFD2QWAgIBDzwrAAkBAA8WBB4NUGF0aFNlcGFyYXRvcgQIHw9nZGQCEw9kFgJmDw8WAh8KBYoBfi9fbGF5b3V0cy8xNS9TSFJNLkNvcmUvdXRpbGl0eS9wYWdldmlld3RyYWNrZXIuYXNweD91c2VyPUFub255bW91cyZ1cmw9JTJmYWJvdXQtc2hybSUyZlBhZ2VzJTJmUHJpdmFjeS1Qb2xpY3kuYXNweCZpc01lbWJlck9ubHlQYWdlPUZhbHNlZGRkXqO+VlESbDU8nBL3usjpljZso0jByK644IlC5K4dBfk=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=4yL1y1QJ0QhVEUCrTTXpDzuEWyMCtTXZY6B6_TJaPJuc9HrvB8a_21p-Bsmy6K_l1LufbwXFRBZL7t0Jz3QBse1ipb8E3o9qWwtZDTbEWQ81&amp;t=636486628482159859" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaQoSEnabled = false;
var g_wsaQoSDataPoints = [];
var g_wsaLCID = 1033;
var g_wsaListTemplateId = 850;
var g_wsaSiteTemplateId = 'CMSPUBLISHING#0';
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fabout-shrm", webAbsoluteUrl: "https:\u002f\u002fwww.shrm.org\u002fabout-shrm", siteAbsoluteUrl: "https:\u002f\u002fwww.shrm.org", serverRequestPath: "\u002fabout-shrm\u002fPages\u002fPrivacy-Policy.aspx", layoutsUrl: "_layouts\u002f15", webTitle: "About SHRM", webTemplate: "39", tenantAppVersion: "0", isAppWeb: false, webLogoUrl: "_layouts\u002f15\u002fimages\u002fsiteicon.png", webLanguage: 1033, currentLanguage: 1033, currentUICultureName: "en-US", currentCultureName: "en-US", clientServerTimeDelta: new Date("2018-11-26T06:29:44.5823489Z") - new Date(), siteClientTag: "1293$$15.0.5047.1000", crossDomainPhotosEnabled:false, webUIVersion:15, webPermMasks:{High:16,Low:196673},pageListId:"{73895ffe-9776-4928-b219-3dc68dcd539a}",pageItemId:6, pagePersonalizationScope:1, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};var dlc_fvsi = {"DefaultViews":[],"ViewUrls":[],"WebUrl":"\/about-shrm"};
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {

    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fabout-shrm\u002fPages\u002fPrivacy-Policy.aspx");

}
//]]>
</script>

<script src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(function(){

        if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
            return;
        }
        _spBodyOnLoadFunctions.push(function() 
        {
          ExecuteOrDelayUntilScriptLoaded(
            function()
            {
              var pairs = SP.ScriptHelpers.getDocumentQueryPairs();
              var followDoc, itemId, listId, docName;
              for (var key in pairs)
              {
                if(key.toLowerCase() == 'followdocument') 
                  followDoc = pairs[key];
                else if(key.toLowerCase() == 'itemid') 
                  itemId = pairs[key];
                else if(key.toLowerCase() == 'listid') 
                  listId = pairs[key];
                else if(key.toLowerCase() == 'docname') 
                  docName = decodeURI(pairs[key]);
              } 

              if(followDoc != null && followDoc == '1' && listId!=null && itemId != null && docName != null)
              {
                SP.SOD.executeFunc('followingcommon.js', 'FollowDocumentFromEmail', function() 
                { 
                  FollowDocumentFromEmail(itemId, listId, docName);
                });
              }

            }, 'SP.init.js');

        });
    })();(function(){

        if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
            return;
        }
        _spBodyOnLoadFunctions.push(function() {

            if (typeof(SPClientTemplates) === 'undefined' || SPClientTemplates === null || (typeof(APD_InAssetPicker) === 'function' && APD_InAssetPicker())) {
                return;
            }

            var renderFollowFooter = function(renderCtx,  calloutActionMenu)
            {
                if (renderCtx.ListTemplateType == 700) 
                    myDocsActionsMenuPopulator(renderCtx, calloutActionMenu);
                else
                    CalloutOnPostRenderTemplate(renderCtx, calloutActionMenu);

                var listItem = renderCtx.CurrentItem;
                if (typeof(listItem) === 'undefined' || listItem === null) {
                    return;
                }
                if (listItem.FSObjType == 0) {
                    calloutActionMenu.addAction(new CalloutAction({
                        text: Strings.STS.L_CalloutFollowAction,
                        tooltip: Strings.STS.L_CalloutFollowAction_Tooltip,
                        onClickCallback: function (calloutActionClickEvent, calloutAction) {
                            var callout = GetCalloutFromRenderCtx(renderCtx);
                            if (!(typeof(callout) === 'undefined' || callout === null))
                                callout.close();
                            SP.SOD.executeFunc('followingcommon.js', 'FollowSelectedDocument', function() { FollowSelectedDocument(renderCtx); });
                        }
                    }));
                }
            };

            var registerOverride = function(id) {
                var followingOverridePostRenderCtx = {};
                followingOverridePostRenderCtx.BaseViewID = 'Callout';
                followingOverridePostRenderCtx.ListTemplateType = id;
                followingOverridePostRenderCtx.Templates = {};
                followingOverridePostRenderCtx.Templates.Footer = function(renderCtx) {
                    var  renderECB;
                    if (typeof(isSharedWithMeView) === 'undefined' || isSharedWithMeView === null) {
                        renderECB = true;
                    } else {
                        var viewCtx = getViewCtxFromCalloutCtx(renderCtx);
                        renderECB = !isSharedWithMeView(viewCtx);
                    }
                    return CalloutRenderFooterTemplate(renderCtx, renderFollowFooter, renderECB);
                };
                SPClientTemplates.TemplateManager.RegisterTemplateOverrides(followingOverridePostRenderCtx);
            }
            registerOverride(101);
            registerOverride(700);
        });
    })();if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();function WebForm_OnSubmit() {
UpdateFormDigest('\u002fabout-shrm', 1440000);if (typeof(_spFormOnSubmitWrapper) != 'undefined') {return _spFormOnSubmitWrapper();} else {return true;};
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DD7C858C" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdADRo/O5nMCzulJYRszw0WPCrDQe9fvP9DoKx2GZjwiNT9+pp2zw3Tgrn8rLgj5rS+4cTdLq8LW599yBLi0c2ZcB3YkJ27LjNOL30+AlZEgGZbS2rAEd8ERHOBHCxdD66PEDuRYNd9RthI+JH9RDWPwg3ne0mjS3W5SZgCBXYpVI1c5mNWL72pc2tfBS+Ieh6skU3Nibwe54jb51Ly/3IDlzK2tFYN8RC/lbig3d4Gemc0Fp+n2tVPbzcsDJ84A4kKdSKT/EgeipEtn7+aAQNO9PL1WDlmmeKVdHQbdEfzU16EwfQIVpENndvhV4EIqM/UK7eg3lcuxK59UzuMAJUeEga7lub4Mzn0M5lsMUbg65LIf9U1Qkbrpf2fwV5Z0ujAVOGm24NmG/E9BJeaUpf+cVN7MFJ84RzJM4JJWPOyRBJZ+0Kw301fpjg/58MITLMs7sQLy8HNQlXaUVgfGa21GJYHTv2ozge4q7OeHadB3woPFDLXG5UAt6JnV9UdZXSRCsfI2MbS84ZZ+qBpeWgcR6l2IoXCc2/On9nJ66GEDPIi0pguoOK4ZEWPnXK1D2XaVPdIFgnVRv4/HigRIE1O5l1Zcww6FM/UpIl5/BVPICEeHVvFj/irGQXcrTJL8qbNT3h+Ja6nmZua4Hdkj4Hn7/GvE1/udu/2T8ng7y5/6ZAqkJSf7aolSABAmyM5Ybm2imYsVGOW8HRQPdJxh91CHSnp1YSVl/gw12fRl3EbpUx/ryr1KjFfaN7YFjGmiY2VVVWGOFSbujXNUn2lAF07+FWfhcZm+W9qkCtakLjITNp1eJNIa4IKdJ0M6pFRVPRZgTojqDbCfNFtf3onJGObh4mbaCTQ0uGPxY/1JfD1OQX1QetHIV0SRr5D4N+l4FE8y9fWYDLCkiXZuLIpYH5M33HdLXJ7OJgnfgjweUtGHQILHvu3IZUc7DOXtyMlLxbTDMV+TRGUoY3tP4zZJ3h9Rixt/e2XyU5imw853pkqeIup9g5scp14/1o2dgWWGEooU3ZnalUlUdd4ECNVYL+DW4v8GgLeSPI4zCsNmS9cGv58AHqT3ijFMiYhuu4+68JWn8HIzdC/wjexth7/Pvvd4pfTkFPxdYHawlImY8I21I+IwSmYUU4U329w7WNg+WRvhQ=" />
</div>
			<script type="text/javascript"> var submitHook = function () { return false; }; theForm._spOldSubmit = theForm.submit; theForm.submit = function () { if (!submitHook()) { this._spOldSubmit(); } }; </script>
			
			<!-- End - Ribbon -->
			<input type="hidden" name="ctl00$ctl71$hf_SearchHost" id="ctl00_ctl71_hf_SearchHost" value="https://shrmsearch.shrm.org" />
<input type="hidden" name="ctl00$ctl71$hf_SearchDomain" id="ctl00_ctl71_hf_SearchDomain" value="https://www.shrm.org" />
<input type="hidden" name="ctl00$ctl71$hf_SiteSearch" id="ctl00_ctl71_hf_SiteSearch" value="https://store.shrm.org" />
<input type="hidden" name="ctl00$ctl71$hf_SiteCollection" id="ctl00_ctl71_hf_SiteCollection" value="prod_kafka" />
<input type="hidden" name="ctl00$ctl71$hf_SearchClient" id="ctl00_ctl71_hf_SearchClient" value="2016_shrm_org" />
<input type="hidden" name="ctl00$ctl71$hf_SearchDisableSectionCounts" id="ctl00_ctl71_hf_SearchDisableSectionCounts" value="1" />
<input type="hidden" name="ctl00$ctl71$hf_RestrictedKickers" id="ctl00_ctl71_hf_RestrictedKickers" value="/hr-today
/hr-today/news
/hr-today/public-policy
/hr-today/trends-and-forecasting
/resourcesandtools
/resourcesandtools/hr-topics
/resourcesandtools/legal-and-compliance
/resourcesandtools/business-solutions
/resourcesandtools/tools-and-samples
/learningandcareer
/learningandcareer/learning
/communities
/communities/communities
/communities/volunteers
/authors
/LearningAndCareer/learning/seminars-materials
/learningandcareer/career" />

            <input type="hidden" id="hf_Cloudinary_SelectedObject" />
            <input type="hidden" id="hf_Cloudinary_SelectedCaret" />
            <input type="hidden" id="hf_Cloudinary_SelectedCaretParent" />
			<!-- Start - Working area (to be use as-is, but you are free to add some classes) -->
			<div id="s4-workspace" class="ms-core-overlay">
				<div id="s4-bodyContainer">
					<div id="contentRow">
						<div id="contentBox" aria-live="polite" aria-relevant="all">
  							<div id="notificationArea" class="ms-notif-box"></div>
							<div id="DeltaPageStatusBar">
	
								<div id="pageStatusBar"></div>
							
</div>
							<div id="DeltaPlaceHolderMain">
	
								<a id="mainContent" name="mainContent" tabindex="-1"></a>
								<!-- -----------------------------------	SHRM SITE START	---------------------------------------------------------->	
								<!-- TEMP SWITCH ROLE BTN :: PRESENTATION ONLY :: TO BE REMOVED	--> 
								<!--a id="switchRole" class="btn btn-default" style="position:fixed; z-index:2" data-toggle="tooltip" data-placement="right" title="Switch membership status"><i class="fa fa-random"></i></a-->
								<!--/ TEMP SWITCH ROLE BTN :: PRESENTATION ONLY :: TO BE REMOVED	--> 
								
								<div class="shrm-site">
									
                                    
								    <!-- --------------------------------- SHRM MEMBER NAVIGATION	CONTAINER STARTS	-->
								    

                                    

<!--	MEMBER CODE	-->
<div id="MembershipPanelPresence" class="container container-dashboard hidden-xs noindex" style="display: none;">
    <div ID="MembershipExpiresTopPanel" class="row alert alert-danger hidden-lg text-center">
        <span class="expire-in"></span> &nbsp; &nbsp;  <a class="btn btn-default btn-cta btn-cta-red" href="https://store.shrm.org/membership">RENEW NOW</a>
    </div>
    <div class="clearfix">
	    <div class="pull-left">
	        <ul class="dashboard-quick-access">
		        <li><a data-toggle="tooltip" title="Manage Your Account" data-placement="bottom" href="/my/dashboard"><i class="fa fa-2x fa-th"></i> mySHRM</a></li>
		        <li><a data-toggle="tooltip" title="My Bookmarks" data-placement="bottom" href="/my/bookmarks"><i class="fa fa-bookmark fa-2x"></i></a></li>
		        <li><a data-toggle="tooltip" title="My Tool Bookmarks" data-placement="bottom" href="/my/bookmarks/tools"><i class="fa fa-wrench fa-2x"></i></a></li>
		        <li>
		            <!-- collapsable .dropdowns trigger - >> aria-expanded="true" << makes it collapsed by default	-->
		            <a data-toggle="collapse" data-target="#shrm-recomended" aria-expanded="false" aria-controls="shrm-recomended"><i class="fa fa-star fa-2x"></i> <i class="fa fa-angle-down"></i></a>
		        </li>
                <li class="li-btn">
                    <a id="hl_AskAnAdvisor" title="Ask an Advisor" class="btn btn-cta btn-ghost">Ask an Advisor</a>
                </li>
	        </ul>
	    </div>
        <div id="MemberDropdownPanel" class="pull-right">
	        <div class="shrm-member-nav dropdown">
		        <a id="member-account-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		            <span>
		                HELLO <span id="preferedNameSpan" class="name"></span><span class="certificate"></span>  &nbsp;<i class="fa fa-angle-down"></i>
		            </span>
		        </a>
		        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="member-account-menu">
		            <li><a href="/my/dashboard">My Dashboard</a></li>
		            <li><a href="/my/profile">My Profile</a></li>
		            <li><a href="/my/account">My Account</a></li>
		            <li role="separator" class="divider"></li>
                    
		            <li><a href="https://store.shrm.org/membership">Renew Membership</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="/_layouts/15/shrm.core/utility/federatedsignout.aspx">Sign out</a></li>
		        </ul><!--	/	.dropdown-menu 	-->
	        </div> <!--/	.shrm-user-nav 	-->
	        <div id="MembershipExpiresRightPanel" class="alert alert-danger visible-lg-inline-block">
				<span class="expire-in"></span> &nbsp;
				<a class="btn btn-default btn-sm btn-cta btn-cta-red" href="https://store.shrm.org/membership">RENEW NOW</a>
			</div>
	    </div>
    </div><!-- .container END	-->       
</div>
<!--	CUSTOMER CODE	-->
<li id="isCustomer" class="dropdown membership-presence-dropdown" style="display: none;">
	<div id="customerDropdownPanel" class="dropdown">
		<a id="customer-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i>&nbsp;<span class="customer-first-name"></span></a> |
		<ul class="dropdown-menu" aria-labelledby="customer-options">
				<li><a href="https://www.shrm.org/my/profile">My Profile</a></li>
				<li><a href="https://www.shrm.org/my/newsletters">My Newsletters</a></li>
				<li><a href="https://www.shrm.org/my/orders">My Orders & Donations</a></li>
				<li><a href="https://www.shrm.org/my/account">My Account</a></li>
				<li class="divider"></li>
				<li><a href="/_layouts/15/shrm.core/utility/federatedsignout.aspx">Sign Out</a></li>
		</ul>
	</div>
</li>

    
<script>
$(function() {
	if(typeof membershipPresence == 'function'){
		membershipPresence();
	}	
});	
	
	
</script>

                                    <div id="ctl00_ctl74_pan_PromotionalBar" class="row promotional-bar hidden-xs hidden-print promotional-bar-core">
		
    <div class="container">
    	<div class="promotional-bar-wrapper">
            <div class="promo-msg">
                <p>
                    <b>Cyber Monday Special!</b> Get $25 Amazon Gift Card when you renew with code CYBER18.
                </p>
            </div>
            <a id="ctl00_ctl74_hl_Link1" class="btn btn-default btn-sm btn-cta btn-cta-red" href="https://membership.shrm.org/?PRODUCT_DISCOUNT_ID=CYBER18&amp;utm_campaign=Membership_Ret">RENEW NOW</a>
            
      </div>
    </div>

	</div>

								    <!-- --------------------------------- SHRM HEADER STARTS	-->
								    <input type="hidden" name="ctl00$SSOUUIDHidden" id="SSOUUIDHidden" />
<input type="hidden" name="ctl00$EmailHidden" id="EmailHidden" />
<input type="hidden" name="ctl00$MemberIdHidden" id="MemberIdHidden" />
<input type="hidden" name="ctl00$MemberStatusHidden" id="MemberStatusHidden" value="0" />
<input type="hidden" name="ctl00$AuthTokenHidden" id="AuthTokenHidden" />
<input type="hidden" name="ctl00$AuthTypeHidden" id="AuthTypeHidden" />
<input type="hidden" name="ctl00$MySHRMAPITokenHidden" id="MySHRMAPITokenHidden" />
<input type="hidden" name="ctl00$GreetingNameHidden" id="GreetingNameHidden" />
<input type="hidden" name="ctl00$CertificatesHidden" id="CertificatesHidden" />
<input type="hidden" name="ctl00$Member2MemberIsActive" id="Member2MemberIsActive" value="True" />
<input type="hidden" name="ctl00$HeartBeatStatus" id="HeartBeatStatus" />
<input type="hidden" name="ctl00$CareerPortalIsActive" id="CareerPortalIsActive" value="True" />
<input type="hidden" name="ctl00$hf_TourButtonVisible" id="hf_TourButtonVisible" value="0" />
<input type="hidden" name="ctl00$SiteTourEnabledHidden" id="SiteTourEnabledHidden" />

<input type="hidden" name="ctl00$TaxonomyHidden" id="TaxonomyHidden" />
<input type="hidden" name="ctl00$GeolocationHidden" id="GeolocationHidden" value="US,California" />
<input type="hidden" name="ctl00$CurrentAnnualConferenceHidden" id="CurrentAnnualConferenceHidden" />
<input type="hidden" name="ctl00$NetSuiteEnabledHidden" id="NetSuiteEnabledHidden" value="False" />

<header class="container container-header hidden-xs noindex">
    <div class="row">
        <div class="top-bar clearfix">
            <div class="header-brand pull-left"></div>
            <span class="header-brand pull-left"><a href="/pages/default.aspx">
                <img class="img-responsive" src="/_layouts/15/SHRM.Core/design/images/SHRMLogo.svg" alt="SHRM Logo" /></a>
            </span>
            <div id="ctl00_nesto" class="header-nav pull-right">
                <ul class="utility-links text-right">
                    <li class="dropdown">
                        
                    </li>
                    <li>
                        <div id="ctl00_SignInPanel">
	
                            <a href="https://www.shrm.org/_layouts/authenticate.aspx?source=/about-shrm/Pages/Privacy-Policy.aspx">Sign In</a> |
                        
</div>
                    </li>
                    <li class="dropdown">
                        <a id="language-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe">&nbsp;</i><i class="fa fa-angle-down"></i>
                            <span id="language-id" class="notranslate">U.S. - EN</span></a> |
                        <ul class="dropdown-menu" aria-labelledby="language-options">
                            <li><a href="/pages/default.aspx?loc=null">SHRM GLOBAL</a></li>
                            
                            <li><a href="/pages/default.aspx?loc=india">SHRM India</a></li>
                            <li><a href="/about-shrm/Pages/SHRM-MENA.aspx">SHRM MENA</a></li>
                            
                        </ul>
                    </li>
                    <li><a id="ctl00_hl_AskAnAdvisor" href="/ResourcesAndTools/tools-and-samples/Pages/HR-Help.aspx">Ask an Advisor</a> | </li>
                    <li><a href="https://store.shrm.org/">SHRM Store</a></li>
                    <li><a href="https://store.shrm.org/checkout/cart/"><span class="shop-items"><span id="cartItemCount" style="display:none;"></span></span></a></li>
                </ul>
                <div class="header-ctas text-right">
                    <div class="btn btn-success btn-lg btn-cta">
                        <a id="ctl00_MembershipCallToActionLink" href="/about-shrm/pages/membership.aspx">Membership</a>
                    </div>
                    <div class="btn btn-success-dark btn-lg btn-cta">
                        <a id="ctl00_CertificationCallToActionLink" href="/certification/pages/default.aspx">GET CERTIFIED</a>                        
                    </div>
                </div>
            </div>
        </div>        <!--/ .top-bar	-->
        <div class="header-bar">
        </div>        <!--/ .header-bar	-->
    </div>
</header><!--/ .row-header	-->

								  
								    <!-- --------------------------------- SHRM NAVIGATION STARTS	-->  
								 	<div class="container-fluid container-nav noindex">
                                        <div ui-view=""></div>
                                        <a id="cloudinary-open" ui-sref="start()"></a>
                                        <a id="cloudinary-open-wideimage" ui-sref="start({ only : ['21x9'] , width : '1920'})"></a>
                                        <a id="cloudinary-open-m2m" ui-sref="start()"></a>
									  	

<nav class="navbar navbar-default row">
    <div class="container">
        <div class="mobile-header">
            <div class="mobile-control">
                <div>
                    <button type="button" class="navbar-toggle navbar-inverse pull-left collapsed" data-toggle="collapse" data-target="#site-main-nav" aria-expanded="false" aria-controls="site-main-nav">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                </div>
                <div class="mobile-logo"><span class="header-brand visible-xs"><a href="/" class="mobile-logo">
                    <img src="/_layouts/15/SHRM.Core/design/images/SHRMLogo.svg" alt="SHRM Logo"></a></span>
                </div>
                <div>
                    <a id="main-search" class="btn btn-danger pull-right visible-xs" role="button" data-toggle="collapse" href="#mobile-search-bar-row" aria-expanded="false" aria-controls="search-bar-row"><i class="fa fa-search"><span class="sr-only">Search</span></i></a>
                    <a id="main-search" class="btn btn-danger pull-right hidden-xs" role="button" data-toggle="collapse" href="#search-bar-row" aria-expanded="false" aria-controls="search-bar-row"><i class="fa fa-search"><span class="sr-only">Search</span></i></a>
                </div>
            </div>
            <div class="site-tagline visible-xs">
                <div>Society For Human Resource Management</div>
            </div>
        </div>
        <!--/ .mobile-header	-->

        <!--	collapsable search bar for mobile affixed navigation	-->
        <div class="row search-bar-row text-center collapse" id="mobile-search-bar-row">
            <div class="container visible-xs">
              <div class="search-bar-group">
                <div class="dropdown hidden-xs">
                  <button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary">
                    <span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
                    <li><a href="#">SHRM Foundation</a></li>
                    <li><a href="#">SHRM India</a></li>
                    <li><a href="#">SHRM China</a></li>
                  </ul>
                </div><!--/ .dropdown	-->
                <div class="search-bar-widget search-bar-xs-container">
                    <!--window.location.href = '/search/Pages/default.aspx'-->
                    <a class="searchSubmitButton" role="button" onclick="window.location.href = '/search/Pages/default.aspx?k=' + $(this).next().val(); "></a>
                    <label for="search-bar-main" class="sr-only">SEARCH</label>
                    <input type="text" class="form-control" id="search-bar-main" onkeydown="if(event.keyCode == 13){window.location.href = '/search/Pages/default.aspx?k=' + $(this).val();}" />          
                    <div class="search-bar-widget-results"></div>
                </div><!-- /.search-bar-lg-container	-->
                
              </div><!--	/.search-bar-group	-->
            </div><!--	/.container	-->
        </div> <!--/ .search-bar-row	-->
        <!--/	collapsable search bar for affixed navigation	-->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="site-main-nav" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl76_rep_Navigation_ctl00_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl00_pan_DropoutMenu' data-toggle='collapse' role='button'>HR Today</a>
                            <div id="ctl00_ctl76_rep_Navigation_ctl00_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl00_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl00_pan_DropoutMenu' data-toggle='collapse' role='button'>HR Today</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>News</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>News</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/hr-today/news/hr-news/Pages/default.aspx">HR News</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/news/hr-magazine/Pages/default.aspx">HR Magazine</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="http://blog.shrm.org/">SHRM Blog</a></li>
                                                        
                                                            <li></li>
                                                        
                                                            <li></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Public Policy</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Public Policy</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="http://www.advocacy.shrm.org/home">Take Action</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/public-policy/hr-public-policy-issues/Pages/default.aspx">HR Public Policy Issues</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl02_hl_Level3_Link" href="http://www.advocacy.shrm.org/about">A-Team Advocacy Network</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl03_hl_Level3_Link" href="/hr-today/public-policy/state-affairs/Pages/default.aspx">State Affairs</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Trends & Forecasting</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Trends & Forecasting</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/hr-today/trends-and-forecasting/research-and-surveys/Pages/default.aspx">Research & Surveys</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/trends-and-forecasting/labor-market-and-economic-data/Pages/default.aspx">Labor Market & Economic Data</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/hr-today/trends-and-forecasting/special-reports-and-expert-views/Pages/default.aspx">Special Reports & Expert Views</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl76_rep_Navigation_ctl00_ucc_FeaturedArticle_pan_Main" class="column col-md-3 col-featured visible-md visible-lg" data-personalized-offer="False">
			
    <div class="siteMenuColHeader"></div>
    <a id="ctl00_ctl76_rep_Navigation_ctl00_ucc_FeaturedArticle_hl_Image" href="https://www.shrm.org/ResourcesAndTools/Pages/HR-Featured-Topics.aspx"><img id="ctl00_ctl76_rep_Navigation_ctl00_ucc_FeaturedArticle_img_Image" title="HR Resource Spotlight" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop,h_253,w_450,x_0,y_0/w_auto:100:228,q_auto,f_auto/v1/Tools%20and%20Samples/18_1489_HupPage_Rullup_WorkplaceInvestigations_jnn0sa" alt="HR Resource Spotlight" /></a>
    <h6>
        <a id="ctl00_ctl76_rep_Navigation_ctl00_ucc_FeaturedArticle_hl_Title" href="https://www.shrm.org/ResourcesAndTools/Pages/HR-Featured-Topics.aspx">HR Resource Spotlight</a>
    </h6>
    <p>​Find news & resources on specialized workplace topics. View key toolkits, policies, research and more on HR topics that matter to you.</p>
    <a id="ctl00_ctl76_rep_Navigation_ctl00_ucc_FeaturedArticle_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://www.shrm.org/ResourcesAndTools/Pages/HR-Featured-Topics.aspx"></a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl01_pan_DropoutMenu' data-toggle='collapse' role='button'>Resources & Tools</a>
                            <div id="ctl00_ctl76_rep_Navigation_ctl01_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl01_pan_DropoutMenu' data-toggle='collapse' role='button'>Resources & Tools</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>HR Topics</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>HR Topics</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/behavioral-competencies/Pages/default.aspx">Behavioral Competencies</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/benefits/Pages/default.aspx">Benefits</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/Pages/california-resources.aspx">California Resources</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/compensation/Pages/default.aspx">Compensation</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/Pages/diversity-and-inclusion.aspx">Diversity & Inclusion</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl05_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/employee-relations/Pages/default.aspx">Employee Relations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl06_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/global-hr/Pages/default.aspx">Global HR</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl07_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/labor-relations/Pages/default.aspx">Labor Relations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl08_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/organizational-and-employee-development/Pages/default.aspx">Organizational & Employee Development</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl09_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/risk-management/Pages/default.aspx">Risk Management</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl10_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/talent-acquisition/Pages/default.aspx">Talent Acquisition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl11_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/technology/Pages/default.aspx">Technology</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Legal & Compliance</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Legal & Compliance</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="/ResourcesAndTools/legal-and-compliance/employment-law/Pages/default.aspx">Employment Law</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/ResourcesAndTools/legal-and-compliance/state-and-local-updates/Pages/default.aspx">State & Local Updates</a></li>
                                                        
                                                            <li></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl03_hl_Level3_Link" href="/ResourcesAndTools/Pages/workplace-immigration.aspx">Workplace Immigration</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Business Solutions</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Business Solutions</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/ResourcesAndTools/business-solutions/Pages/benchmarking.aspx">Benchmarking Service</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="https://brokerfinder.shrm.org/">Benefits Broker Directory</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/ResourcesAndTools/business-solutions/Pages/Employee-Engagement-Survey-Service.aspx">Employee Engagement Survey</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl03_hl_Level3_Link" href="/ResourcesAndTools/business-solutions/Pages/J-1-Visa-Sponsorship.aspx">J-1 Visa Sponsorship</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl04_hl_Level3_Link" href="/ResourcesAndTools/business-solutions/Pages/Salary-Data-Service.aspx">Salary Data Service</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl05_hl_Level3_Link" href="https://tac.shrm.org/">Talent Assessment Center</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl06_hl_Level3_Link" href="https://vendordirectory.shrm.org/">Vendor Directory</a></li>
                                                        
                                                            <li></li>
                                                        
                                                            <li></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Tools & Samples</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Tools & Samples</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/Pages/employee-handbooks.aspx">Employee Handbooks</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/exreq/Pages/Trending-Topics.aspx">Express Requests</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl02_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/how-to-guides/Pages/default.aspx">How-To Guides</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl03_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/hr-forms/Pages/default.aspx">HR Forms</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl04_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/hr-qa/Pages/default.aspx">HR Q&As</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl05_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/interview-questions/Pages/default.aspx">Interview Questions</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl06_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/job-descriptions/Pages/default.aspx">Job Descriptions</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl07_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/policies/Pages/default.aspx">Policies</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl08_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/presentations/Pages/default.aspx">Presentations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl09_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/Pages/spreadsheets.aspx">Spreadsheets</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl10_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/toolkits/Pages/default.aspx">Toolkits</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl11_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/member2member/Pages/default.aspx">Member2Member Solutions</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl76_rep_Navigation_ctl01_ucc_FeaturedArticle_pan_Main" class="column col-md-3 col-featured visible-md visible-lg" data-personalized-offer="False">
			
    <div class="siteMenuColHeader">JOB DESCRIPTION MANAGER</div>
    <a id="ctl00_ctl76_rep_Navigation_ctl01_ucc_FeaturedArticle_hl_Image" href="https://store.shrm.org/job-description-manager.html"><img id="ctl00_ctl76_rep_Navigation_ctl01_ucc_FeaturedArticle_img_Image" title="Job Description Manager" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop,h_899,w_1600,x_0,y_0/w_auto:100:228,q_auto,f_auto/v1/Marketing/JDM_dcipir" alt="Job Description Manager" /></a>
    <h6>
        <a id="ctl00_ctl76_rep_Navigation_ctl01_ucc_FeaturedArticle_hl_Title" href="https://store.shrm.org/job-description-manager.html">Job Description Manager</a>
    </h6>
    <p>Create, Maintain & Organize Your Job Descriptions. It’s fast. It’s easy.</p>
    <a id="ctl00_ctl76_rep_Navigation_ctl01_ucc_FeaturedArticle_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://store.shrm.org/job-description-manager.html">LEARN MORE</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl02_pan_DropoutMenu' data-toggle='collapse' role='button'>Learning & Career</a>
                            <div id="ctl00_ctl76_rep_Navigation_ctl02_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl02_pan_DropoutMenu' data-toggle='collapse' role='button'>Learning & Career</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Career</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>Career</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/Accelerate-Your-Career.aspx">Accelerate Your Career</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/Career-Preparation-and-Planning.aspx">Career Preparation & Planning</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/shrm-competency-model.aspx">SHRM Competency Model</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/Your-Professional-Development.aspx">Your Professional Development</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/Career-Expert-Insights.aspx">Career Expert Insights</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>HR Jobs</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>HR Jobs</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="http://www.shrm.org">uccHrJobs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="http://jobs.shrm.org/">Browse All Jobs...</a></li>
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Learning</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Learning</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/Seminars.aspx">Seminars</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/LearningAndCareer/learning/onsite-training/Pages/default.aspx">Onsite Training</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/SHRM-eLearning.aspx">eLearning</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl03_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/SHRM-Essentials-of-Human-Resources.aspx">SHRM Essentials of Human Resources</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl04_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/SHRM-Senior-Leadership-Programs.aspx">Senior Leadership Programs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl05_hl_Level3_Link" href="https://store.shrm.org/events/shrm-events/virtual-events.html">Virtual Events</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl06_hl_Level3_Link" href="/LearningAndCareer/learning/webcasts/Pages/default.aspx">Webcasts</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl07_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/Specialty-Credentials.aspx">Specialty Credentials</a></li>
                                                        
                                                            <li></li>
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Certification</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Certification</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="https://www.shrm.org/certification/apply/pages/default.aspx">Apply for Exam</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="https://www.shrm.org/certification/learning/pages/default.aspx">Certification Preparation</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl02_hl_Level3_Link" href="https://www.shrm.org/certification/faqs/pages/default.aspx">SHRM Certification FAQs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl03_hl_Level3_Link" href="https://www.shrm.org/certification/recertification/pages/default.aspx">Recertification</a></li>
                                                        
                                                            <li></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>For Educators</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-toggle='collapse' role='button'>For Educators</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl00_hl_Level3_Link" href="/academicinitiatives/universities/pages/guidebook.aspx">HR Curriculum Guidebook & Template</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl01_hl_Level3_Link" href="/academicinitiatives/students/pages/hrprogramdirectory.aspx">HR Program Directory</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl02_hl_Level3_Link" href="/academicinitiatives/universities/teachingresources/pages/termsofuse_faculty.aspx">Teaching Resources</a></li>
                                                        
                                                            <li></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl76_rep_Navigation_ctl02_ucc_FeaturedArticle_pan_Main" class="column col-md-3 col-featured visible-md visible-lg" data-personalized-offer="False">
			
    <div class="siteMenuColHeader">IN-PERSON SHRM SEMINARS</div>
    <a id="ctl00_ctl76_rep_Navigation_ctl02_ucc_FeaturedArticle_hl_Image" href="https://store.shrm.org/training/learning-center/seminars/find-a-seminar.html"><img id="ctl00_ctl76_rep_Navigation_ctl02_ucc_FeaturedArticle_img_Image" title="Local Development Opportunities" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop,h_1723,w_3065,x_0,y_54/w_auto:100:228,q_auto,f_auto/v1/Home%20Page%20Carousel/18-0895_Map_image_oyakdd" alt="Local Development Opportunities" /></a>
    <h6>
        <a id="ctl00_ctl76_rep_Navigation_ctl02_ucc_FeaturedArticle_hl_Title" href="https://store.shrm.org/training/learning-center/seminars/find-a-seminar.html">Local Development Opportunities</a>
    </h6>
    <p>Build competencies, establish credibility and advance your career—while earning PDCs—at SHRM Seminars in 14 cities across the U.S. this fall.</p>
    <a id="ctl00_ctl76_rep_Navigation_ctl02_ucc_FeaturedArticle_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://store.shrm.org/training/learning-center/seminars/find-a-seminar.html">SEE 2018 SEMINAR LOCATIONS</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl03_pan_DropoutMenu' data-toggle='collapse' role='button'>Events</a>
                            <div id="ctl00_ctl76_rep_Navigation_ctl03_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl03_pan_DropoutMenu' data-toggle='collapse' role='button'>Events</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>SHRM Events</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>SHRM Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="http://annual.shrm.org/">SHRM Annual Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/mlp/Pages/Diversity2018.aspx">Diversity & Inclusion Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="https://conferences.shrm.org/legislative-conference">Employment Law & Legislative Conference</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="https://www.shrm.org/mlp/Pages/SymposiumDeptOne.aspx?">HR Department of One Symposium</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="http://conferences.shrm.org/talent-conference">Talent Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl05_hl_Level3_Link" href="/Events/Pages/State--Affiliate-Conferences.aspx">State & Affiliate Conferences</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl06_hl_Level3_Link" href="https://www.shrm.org/mlp/Pages/SymposimCAHR.aspx?">Spotlight on CA Symposium</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl07_hl_Level3_Link" href="/LearningAndCareer/learning/webcasts/Pages/default.aspx">Webcasts</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl08_hl_Level3_Link" href="/Events/Pages/default.aspx">SEE ALL EVENTS</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Event Resources</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Event Resources</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="https://shrm.org/mlp/Pages/speakers-bureau/Home.aspx">Speakers Bureau</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/Events/Pages/Speaker-Information.aspx">Conference Speaker Information</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl02_hl_Level3_Link" href="http://conferences.shrm.org/exhibit-or-sponsor">Sponsorship & Exhibitor Information</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl03_hl_Level3_Link" href="http://l.shrm.org/requestmoreinformation">Request a Brochure</a></li>
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Global Events</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Global Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/Events/shrm-india-events/Pages/default.aspx">SHRM India Events</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/Events/Pages/shrm-apac-events.aspx">SHRM APAC Events</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Affiliate Events</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Affiliate Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="https://www.cfgi.org/symposium">CFGI Symposium</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="https://hrps.org/executive-events/strategic-hr-forum/pages/default.aspx">HR People + Strategy Strategic HR Forum</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl03_rep_Navigation2_ctl03_rep_Navigation3_ctl02_hl_Level3_Link" href="https://www.hrps.org/executive-events/annual-conference/Pages/default.aspx">2019 HR People + Strategy Annual Conference</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl76_rep_Navigation_ctl03_ucc_FeaturedArticle_pan_Main" class="column col-md-3 col-featured visible-md visible-lg" data-personalized-offer="False">
			
    <div class="siteMenuColHeader">SHRM CONFERENCES</div>
    <a id="ctl00_ctl76_rep_Navigation_ctl03_ucc_FeaturedArticle_hl_Image" href="https://shrm.org/mlp/Pages/SymposimCAHR.aspx"><img id="ctl00_ctl76_rep_Navigation_ctl03_ucc_FeaturedArticle_img_Image" title="SHRM Symposium: Spotlight On CA" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop,h_237,w_422,x_0,y_26/w_auto:100:228,q_auto,f_auto/v1/Marketing/California_Nav_Graphic_nkrdyq" alt="SHRM Symposium: Spotlight On CA" /></a>
    <h6>
        <a id="ctl00_ctl76_rep_Navigation_ctl03_ucc_FeaturedArticle_hl_Title" href="https://shrm.org/mlp/Pages/SymposimCAHR.aspx">SHRM Symposium: Spotlight On CA</a>
    </h6>
    <p>Join us in San Francisco, CA, December 4 to learn how to stay compliant with California’s everchanging legislation and regulatory landscape. Space is limited.</p>
    <a id="ctl00_ctl76_rep_Navigation_ctl03_ucc_FeaturedArticle_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://shrm.org/mlp/Pages/SymposimCAHR.aspx">Register Now</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl76_rep_Navigation_ctl04_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl04_pan_DropoutMenu' data-toggle='collapse' role='button'>Membership</a>
                            <div id="ctl00_ctl76_rep_Navigation_ctl04_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl04_pan_DropoutMenu' data-target='#ctl00_ctl76_rep_Navigation_ctl04_pan_DropoutMenu' data-toggle='collapse' role='button'>Membership</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Communities</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>Communities</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="http://community.shrm.org/">SHRM Connect</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/Membership/communities/chapters/Pages/default.aspx">Chapters</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="http://www.hrps.org/">Executive Network</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/Membership/communities/hr-young-professionals/Pages/default.aspx">HR Young Professionals</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="http://www.advocacy.shrm.org/about">Legislative Advocacy Team (A-Team)</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Student Resources</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Student Resources</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="/Membership/student-resources/Pages/default.aspx">Student Member Center</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Volunteers</a>
                                                <ul id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Volunteers</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/Membership/volunteers/membership-councils/Pages/default.aspx">Membership Councils</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/Membership/volunteers/special-expertise-panels/Pages/default.aspx">Special Expertise Panels</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="http://community.shrm.org/vlrc/">Volunteer Leader Resource Center</a></li>
                                                        
                                                            <li><a id="ctl00_ctl76_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl03_hl_Level3_Link" href="/Membership/volunteers/Pages/Volunteer-Opportunities.aspx">Volunteer Opportunities</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
            </ul>
            

<div class="mobile-quick-access visible-xs">
	<div class="mobile-utility-links text-center">    
        <div>
            <a href="/ResourcesAndTools/tools-and-samples/Pages/HR-Help.aspx">HR Help</a>
        </div>
        
        
        <div id="ctl00_ctl76_HeaderMobile_SignInPanel">
		
            <a href="/_layouts/authenticate.aspx">Sign In</a>
        
	</div>
	</div><!--/	.mobile-utility-links	-->
	<div class="mobile-ctas text-center">
        <!-- Role All	-->
        <a href="https://store.shrm.org/" target="_blank" class="btn btn-success btn-lg btn-cta">
		    	<span>SHRM Store</span>
        </a>
        <div class="btn btn-success btn-lg btn-cta">
            <a id="ctl00_ctl76_HeaderMobile_MembershipCallToActionLink" href="/about-shrm/pages/membership.aspx">Membership</a>
        </div>
        <div class="btn btn-success-dark btn-lg btn-cta">
            <a id="ctl00_ctl76_HeaderMobile_CertificationCallToActionLink" href="/certification/pages/default.aspx">GET CERTIFIED</a>
        </div>
	</div><!--/	.mobile-ctas	-->
</div><!--/ .mobile-quick-access	--> 

        </div>
    </div>
    <div id="ucc_HRJobs" style="display: none;">
        <div id="ctl00_ctl76_HrJobs_pan_JobFinder" class="shrm-widget shrm-job-finder noindex">
		
    <h3 class="shrm-Title-Small text-uppercase text-center">Job Finder</h3>
    <h5 class="text-center">Find an HR Job Near You</h5>
    <div class="finder-form">
        <div class="input-holder">
            <label for='ctl00_ctl76_HrJobs_searchTerm' class="sr-only">CITY, STATE, ZIP</label>
            <input name="ctl00$ctl76$HrJobs$searchTerm" type="text" id="ctl00_ctl76_HrJobs_searchTerm" placeholder="CITY, STATE, ZIP" class="form-control" />
        </div>
        <a href="javascript:" id="ctl00_ctl76_HrJobs_searchJobs" class="btn btn-success btn-lg btn-cta" onclick="return OpenInNewWindow(&#39;ctl00_ctl76_HrJobs_searchJobs&#39;,&#39;ctl00_ctl76_HrJobs_searchTerm&#39;);"><span class="status">Search Jobs</span></a>
    </div>
    <a id="ctl00_ctl76_HrJobs_hl_PostAJob" class="shrm-job-finder-post-a-job" href="http://hrjobs.shrm.org/jobs/products">Post a Job</a>
    <script language="javascript" type="text/javascript">
        function OpenInNewWindow(anchorTagId, textBoxTagId) {
            var searchTerm = document.getElementById(textBoxTagId);
            var searchJobs = document.getElementById(anchorTagId);
            searchJobs.setAttribute("href", "javascript:");
            searchJobs.setAttribute("target", "");

            if (searchTerm.value.length > 0) {
                searchJobs.setAttribute("target", "_blank");
                var href = "http://hrjobs.shrm.org/jobs/search/results?radius=0&location=" + searchTerm.value;
                searchJobs.setAttribute("href", href);
            }
        }
		    $(function() {
			    $uccHrJobsHtml = $('#ucc_HRJobs');
			    $uccHrJobsPlaceholder = $('nav.navbar li a').filter(function(){
				     return $(this).text() === "uccHrJobs";
			    });
			
			    if( $uccHrJobsHtml.length > 0 && $uccHrJobsPlaceholder.length > 0){
				    $('.shrm-job-finder',$uccHrJobsHtml).addClass('shrm-mini-job-finder');
				    $($uccHrJobsPlaceholder).replaceWith( $($uccHrJobsHtml).show());
			    }
		    });
    </script>

	</div>


    </div>
    <div id="ucc_LocalChapter" style="display:none;">
        

<div class="shrm-job-finder shrm-widget">
    <h3 class="shrm-Title-Small text-uppercase text-center siteMenuColHeader">LOCAL CHAPTERS</h3>
    <h5 class="text-center">Find chapters in your area</h5>
    <div class="finder-form">
  	    <div class="input-holder">
            <label for="chapterLocate" class="sr-only">CITY/STATE OR ZIP</label>
            <input type="text" id="chapterLocate"  placeholder="CITY/STATE OR ZIP"  class="form-control chapter-form-control" data-onload="chapterLocatorScript(this, new Date().getTime())" />
  	    </div>
        <a id="ctl00_ctl76_ctl00_hl_FindChapters" class="btn btn-success btn-lg btn-cta" onclick="window.location=&#39;/search/pages/LocalChapter.aspx?location=&#39; + $(this).closest(&#39;.finder-form&#39;).find(&#39;input&#39;).val()" href="javascript:">
            <span class="status">Find Chapters</span>
        </a>
    </div>
</div><!--	/.shrm-job-finder	-->

    </div>
</nav>


									    <!--	collapsable search bar for tablet/desktop affixed navigation	-->
									    <div class="row search-bar-row text-center collapse" id="search-bar-row">
									        <div class="container hidden-xs">
														<div class="search-bar-group">
															<div class="dropdown hidden-xs">
																<button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-cta">
																	<span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
																</button>
																<ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
																</ul>
															</div><!--/ .dropdown	-->
															<div class="search-bar-widget search-bar-lg-container">
																<a class="searchSubmitButton" role="button" href="javascript:"><span class="sr-only">search</span></a>
           											            <input type="text" class="form-control" id="search-bar-main" />           
																<div class="search-bar-widget-results"></div>
															</div><!-- /.search-bar-lg-container	-->
														</div>
													</div>
									    </div><!--/ .search-bar-row	-->
									    <!--/	collapsable search bar for affixed navigation	-->
									</div><!--/ .container-nav	-->
									<div class="affixed-nav-space-holder"></div>
									<div class="search-bar-row text-center hidden-xs noindex">
									    <div class="container">
												<div class="search-bar-group">
													<div class="dropdown hidden-xs">
														<button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-cta">
															<span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
														</button>
														<ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
														</ul>
													</div><!--/ .dropdown	-->
													<div class="search-bar-widget search-bar-lg-container">
													<!--window.location.href = '/search/Pages/default.aspx'-->
														<a class="searchSubmitButton" role="button" href="javascript:"><span class="sr-only">search</span></a>
           									            <input type="text" class="form-control" id="search-bar-main" />         
														<div class="search-bar-widget-results"></div>
													</div><!-- /.search-bar-lg-container	-->	
												</div> <!--	/.search-bar-group	-->     
											</div>
									</div><!--/ .search-bar-row	-->								  
									<!-- --------------------------------- SHRM NAVIGATION END - SP NAV	-->
									
								  	<!-- Start - Layout content -->
									
    <div class="container container-content">
        <div class="article-page">
            <div class="col-md-8">
                <div class="text-center">
                    
<button id="ctl00_PlaceHolderMain_ctl00_btn_Kicker" type="button" class="shrm-has-url shrm-Kicker" onclick="window.location = &#39;/about-shrm&#39;;">About SHRM</button>

                    
                <!--googleon: all-->
                    <!--googleoff: snippet-->
                    <h1 class="h2">
                        Privacy Policy
                    </h1>
                    <!--googleon: snippet-->                    
                    <p class="shrm-Dek text-center">
                        
                    </p>                   
                </div>
                <!--googleoff: snippet-->
             		

<div id="hiddenvalue-author" class="hidden author">
    &#160;
</div>



                <!--googleon: snippet-->
                
                <!--googleoff: all-->
                                
                

                


                
                <div class="article-content">
                    
                                     
                    
                        



                    
                    <!--googleon: all-->
                    <div id="ctl00_PlaceHolderMain_ctl07_label" style='display:none'>Page Content</div><div id="ctl00_PlaceHolderMain_ctl07__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_ctl07_label"><p class="shrm-Element-P"> 
   <b>
      <span class="shrm-Style-NoDropCap"></span></b>&#160;</p><div><p><strong>Privacy&#160;Statement Updated October 1, 2018.</strong></p><p>Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource profession. &#160;</p><p>In Part I of this Privacy Statement we set forth SHRM's Privacy Policy as to Personal Information which we collect online through&#160;<a href="/"><span style="text-decoration&#58;underline;">www.shrm.org</span></a>&#160;or other SHRM websites which link to this Privacy Statement from their site (collectively, &quot;SHRM Websites&quot;).&#160;</p><p>In Part II of this Privacy Statement we set forth SHRM's Privacy Policy as to Personal Information which we collect other than through SHRM Websites.&#160;</p><p>&quot;Personal Information&quot;, is any information that enables us to identify you, directly or indirectly, by reference to an identifier such as your name, identification number, location data, online identifier or one or more factors specific to you. Personal Information includes &quot;sensitive Personal Information&quot; and &quot;pseudonymised Personal Information&quot; but excludes anonymous information or information that has had the identity of an individual permanently removed.</p><p><strong>Your use of the SHRM Websites and/or provision of your Personal Information or sensitive Personal Information to SHRM constitutes your consent to the use, storage, processing and transfer of that information in accordance with this Privacy Statement.</strong></p><p>For the purposes of the EU General Data Protection Regulation 2016/679 (the &quot;GDPR&quot;) the data controller is Society of Human Resource Management with an office 1800 Duke Street, Alexandria, Virginia, 22314. SHRM is an organization based in the United States. The SHRM Foundation, a subsidiary of SHRM, has also adopted this Privacy Statement.&#160; Where your data is being collected and used by the SHRM Foundation, all references to SHRM herein shall mean the SHRM Foundation.</p><p><strong>QUESTIONS</strong></p><p>If you have any questions or complaints regarding this Privacy Statement, please contact us at <a href="mailto&#58;gcoffice@shrm.org"><span style="text-decoration&#58;underline;">gcoffice@shrm.org</span></a>.&#160; If you have any questions about the SHRM Websites, please contact us at <a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a>, by phone at 703-548-3440 or 800-283-7476 or send a letter to&#58;</p><p>Society for Human Resource Management<br> 1800 Duke Street&#160;<br> Alexandria, Virginia, 22314&#160;</p><p><strong>CHANGES TO THIS PRIVACY STATEMENT</strong></p><p>We will update this Privacy Statement from time to time, so please check back periodically.</p><p>If at any point we decide to use Personal Information in a manner that is materially different from that stated at the time it was collected, we will endeavor to notify you of such changes (e.g., we will post a revised Privacy Statement with a new effective date, display the word &quot;updated&quot; next to the Privacy Policy link on each page on the SHRM Websites, or otherwise), prior to implementing them.&#160;</p><p><strong>PART I – SHRM WEBSITES PRIVACY POLICY</strong></p><p>Throughout SHRM Websites, there are forms for visitors to request information, products, and services. We use Personal Information from these forms to provide the products, promotional materials, or memberships that you request.</p><p>Forms on the SHRM Websites that request financial information do so in order to bill you for products or services ordered. Unique identifiers (specifically, your SHRM member number) are collected from website visitors to verify the user's identity for access to restricted content or features on the SHRM Websites.</p><p>This Privacy Policy discloses SHRM's privacy practices and contains detailed information about the following&#58;</p><ul style="list-style-type&#58;square;"><li>What information do we collect?</li><li>What are &quot;cookies&quot; and how does SHRM use them?</li><li>Do we share information with third parties?</li><li>How do we use the information we collect?</li><li>How can you review and modify your Personal Information?</li><li>What is the opt-out policy for SHRM Websites?</li><li>Your California privacy rights</li><li>Your European privacy rights</li><li>What types of security procedures are in place to protect against the loss, misuse or alteration of your information?</li><li>How do SHRM Websites use bulletin boards, discussion lists, and moderated chats?</li></ul><p><strong>1.&#160;&#160;&#160;&#160; What information do we collect?</strong><br><strong> </strong>SHRM Websites collect the following Personal Information about site visitors&#58;</p><p><strong>a)&#160;&#160;&#160; Site Use Information</strong></p><p>SHRM collects technical information relating to each time a visitor comes to a SHRM Website, including IP address, browser type and version, time zone settings, browser plug-in types and versions, operating system and platform type (e.g. Internet Explorer browser on a Windows platform).</p><p>We may also collect information about your visit, including pages you viewed or searched for, page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), methods used to browse away from the page, and any phone number used to call our customer service number.</p><p>Gathering this technical information helps us to learn what browsers we need to support and helps us determine what sections of SHRM Websites are most popular and how many visitors come to our site(s). You do not have to register with SHRM Websites before we can collect this information.</p><p><strong>b)&#160;&#160;&#160; Information You Give Us</strong></p><p>You may provide Personal Information to us which may include your name, home and/or work email address, postal address, phone number and/or fax number, your employer and job title, demographic information about you including birth year, certification designation, etc., and if you are making a purchase, your credit card information. You are only required to provide such information if you want to take advantage of optional products and services provided through SHRM Websites.</p><p>SHRM collects Personal Information in the following ways from different parts of the SHRM Websites&#58;</p><ul style="list-style-type&#58;square;"><li><strong>SHRM Membership Applications&#58;</strong>&#160;You are sharing Personal Information with us when you join SHRM or renew your SHRM membership through the SHRM Websites. SHRM members will have SHRM membership log-in credentials which enable members to take advantage of restricted content and features on SHRM Websites.</li><li><strong>Exchange Visitor Program Applications&#58; </strong>Host Organizations (the entity hosting an exchange visitor) and Exchange Visitor Trainees/Interns use SHRM's online application system to provide SHRM with all information required to process the applications.&#160; The information collected is subject to change over time, based on applicable laws and regulations.&#160; In addition to the terms of this Privacy Policy, any information provided pursuant to an application is governed by the terms of the <a href="/about-shrm/Pages/Exchange-Visitor-Program-Privacy-Policy.aspx">Exchange Visitor Program Privacy Policy</a>.</li><li><strong>Other Registration&#58;</strong>&#160;When registering for specific services we may ask for the same type of Personal Information. For example, if you subscribe to an e-mail newsletter, we will ask you to provide your e-mail address.</li><li><strong>Online Purchases&#58;</strong>&#160;When you make a purchase using the SHRMStore or when you subscribe to a SHRM publication through the SHRM websites we may also ask for the same type of Personal Information. The number and variety of useful services on SHRM Websites that may require the collection of Personal Information about you will continue to grow in the future.</li></ul><p><strong>2.&#160;&#160;&#160;&#160; What are &quot;cookies&quot; and how does SHRM use them?</strong></p><p>A cookie is a small text file containing a unique identification number that is transferred from a website to the hard drive of your computer. This unique number identifies your web browser to SHRM computers whenever you visit SHRM Websites.</p><p>The use of cookies is an industry standard, and cookies are currently used on most major websites. Most web browsers are initially set up to accept cookies. If you prefer, you can reset your browser to notify you when you have received a cookie. You can also set your browser to refuse to accept cookies altogether.</p><p>While SHRM does not require you to use cookies, keep in mind that certain services will not function properly if you set your browser to refuse all cookies. To help serve you better, SHRM generally uses cookies to&#58;</p><ul style="list-style-type&#58;square;"><li>Identify return visitors. Cookies let us remember your web browser so we can provide personalized member services such as My SHRM and SHRM search agents. Cookies also allow us to identify SHRM members who are returning to the site.</li><li>Display advertisements. SHRM uses an outside ad company to display SHRM-approved ads on our website and other websites. While we use cookies on other parts of our website(s), the cookies received with banner ads are collected by our ad company. These cookies allow SHRM to manage the delivery of ads.</li></ul><p>For more information about how we use cookies on the SHRM Websites, please see our&#160;<a href="/about-shrm/Pages/Cookie-Policy.aspx?_ga=2.198675503.1884290357.1526907360-1177679999.1484762105"><span style="text-decoration&#58;underline;">Cookie Policy</span></a>.</p><p><strong>3.&#160;&#160;&#160;&#160; Do we share information with third parties?</strong></p><p>Personal Information collected through SHRM Websites is generally collected and maintained solely by SHRM or its contracted vendors.</p><p>We may share Personal Information in the following ways&#58;</p><p><strong>a)&#160;&#160;&#160; Information You Give Us</strong></p><ol style="list-style-type&#58;decimal;"><li>Personal Information provided when you register for services or products. When you provide Personal Information about you on SHRM Websites to register for a service, buy a product, or take advantage of a promotion SHRM reserves the right to sell or otherwise provide to selected third parties, mailing/information lists derived from such registrations.<br> <br><strong>If you wish to opt out of such list sales/distribution at any time, you may do so by following the directions in Item 6 below. We will not share Personal Information relating to individuals based in the EU unless you have provided your opt in consent.&#160;&#160;</strong></li></ol><ol start="2" style="list-style-type&#58;decimal;"><li>Information provided when joining or renewing membership in SHRM. If you join SHRM or renew your membership through the SHRM Websites, you provide Personal Information about you on the membership application. SHRM reserves the right to sell or otherwise provide mailing/information lists concerning members to selected third-parties.<br> <br><strong>If you wish to opt out of such list sales/distributions at any time during your SHRM membership, you may do so by following the directions in Item 6 below. We will not share Personal Information relating to individuals based in the EU unless you have provided your opt in consent.</strong><br><strong> </strong><br><strong>SHRM does not sell to third-parties, e-mail addresses obtained from member applications or renewals</strong>&#160;(except that SHRM will rent a member e-mail address if that member has expressly opted in to allow such rentals).&#160;</li><li>Information provided when you apply for an Exchange Visitor Program. &#160;If you apply for sponsorship through SHRM's Exchange Visitor Program, you provide Personal Information about you and/or any dependent family members on the sponsorship application.&#160; SHRM is required by law to provide your Personal Information to the U.S. Department of State and Department of Homeland Security.&#160; <strong>SHRM does not sell Personal Information related to Exchange Visitor Program sponsorship, and will only share such Personal Information as necessary to process the application and provide sponsorship to you.</strong><br> <br>Sensitive Personal Information.&#160;&#160;<strong>SHRM does not share with third parties, sensitive Personal Information such as passcodes, social security numbers, credit card numbers, felony conviction information, or health information except as necessary to complete transactions</strong>&#160;requested by you and under strict confidentiality and security protections; nor does SHRM publish such sensitive Personal Information.</li></ol><p><strong>b)&#160;&#160;&#160; Site Use Information</strong></p><p>We disclose to third-party sponsors/advertisers aggregate statistics (i.e., impressions and click-throughs on a company's advertisement).</p><p>In addition, we may share aggregate website statistics with the media or other third parties. We do not disclose Personal Information to these sponsors/advertisers or other third parties as part of this process, only information in an aggregate form.</p><p><strong>c)&#160;&#160;&#160; SHRM Subsidiaries</strong></p><p>We also allow our subsidiaries, Strategic Human Resource Management India Pvt. Ltd. (&quot;SHRM India&quot;), SHRM China, SHRM MEA FZ (Dubai), SHRM Foundation, SHRM Corporation, and HR People + Strategy, to use the information which you may provide when you register for services or products or when you join or renew membership in SHRM, to the same extent as SHRM may use such information under this Privacy Policy, and fully subject to the same limits as SHRM is subject to on the use of such information under this Privacy Policy.</p><p><strong>4.&#160;&#160;&#160;&#160; How do we use the information we collect?</strong></p><p>We use your Personal Information according to the terms of the Privacy Policy in effect at the time of our use. We will only process your Personal Information, including sharing it with third parties, where (1) you have provided your consent which can be withdrawn at any time, (2) the processing is necessary for the performance of a contract to which you are a party (including your membership agreement with us), (3) we are required by law, (4) processing is required to protect your vital interests or those of another person, or (5) processing is necessary for the purposes of our legitimate commercial interests, provided your interests and fundamental rights do not override those interests.</p><p>We use Personal Information for the following purposes&#58;</p><p><strong>a)&#160;&#160;&#160; Information You Give Us</strong></p><p>We use the Personal Information you give us&#58;</p><ol style="list-style-type&#58;decimal;"><li>to carry out our obligations arising from your membership, Exchange Visitor sponsorship, and any other agreement entered into between you and us</li><li>to update and renew your membership as required&#160;</li><li>to provide you with the information, products and services you request from us&#160;</li><li>to arrange and deliver conferences, events and programming relevant to your job and subjects of interest</li><li>to respond to your questions and provide related membership or customer services</li><li>to improve the SHRM Websites</li><li>to send you SHRM publications, information about member benefits and special offers, and other information that SHRM believes is relevant and useful to its members, where you have not opted out of receiving such information&#160;</li><li>to send you third party information that SHRM believes is relevant and useful to you, where you have given your consent</li><li>to notify you about changes to your membership or related services or to ask you to provide feedback&#160;</li><li>to enable you to partake in a prize draw, competition or complete a survey</li><li>to administer and protect our business and websites (including troubleshooting, data analysis, testing, system maintenance, support, reporting and hosting of data)</li><li>to comply with all applicable laws or legal processes, including providing information on individual users to the appropriate governmental authorities where required by law enforcement or judicial authorities&#160;</li></ol><p>a.&#160; &#160;in matters involving a danger to personal or public safety, or to protect the rights, property or safety of SHRM, our members, customers, certificants or others SHRM may voluntarily provide information to appropriate governmental authorities</p><p>b.&#160; &#160;in some cases members may receive products or services, which are paid for by their government employer, and as a result of payment by the government employer, the record must be open for public access pursuant to applicable law, such as the Florida Public Records Act, and SHRM will share such information pursuant to those laws</p><p>c.&#160;&#160; in some cases members who are veterans may receive products or services, which are paid for by the Department of Veterans Affairs (VA), and as a result of payment by the VA, the record must be made available to the VA pursuant to VA regulation, and for processing payment, and SHRM will share such information pursuant to such regulations.</p><p>&#160;</p><p><strong>b)&#160;&#160;&#160; Site Use Information</strong></p><p>As mentioned above, SHRM uses the aggregate, anonymous data collected to let our sponsors/advertisers know the number of impressions or views and the number of &quot;click throughs&quot; on their advertisement(s).</p><p>SHRM also uses this aggregate, anonymous data to perform statistical analyses of the collective characteristics and behavior of visitors to SHRM Websites; to measure user interests regarding specific areas of SHRM Websites; and to analyze how and where best to use our resources.</p><p>Without such data, we would not know which parts of SHRM Websites are the most popular, and we would not be able to change and update the content and services appropriately.</p><p><strong>c)&#160;&#160;&#160; Information Collected in Connection with Certification and Recertification</strong></p><p>If you register for certification or recertification services, or for any other SHRM credential or certificate (e.g. SHRM California Specialty Credential), SHRM uses any Personal Information you may provide doing so in the same manner as we do when you register for other SHRM products and services.&#160; Without limitation of the foregoing, SHRM (and/or SHRM's certification or credential-related vendors) may use and disclose Personal Information, certification exam performance information, and/or assessment information related to any other SHRM credential or certificate in connection with (the following is a non-exclusive list of examples)&#58;</p><ol style="list-style-type&#58;decimal;"><li>Listing all individuals who have achieved a SHRM certification in the SHRM Online Certified Directory</li><li>Sending notifications/reminders on the status of certification or other credential or certificate, information on professional development and recertification opportunities, and other news and communications of interest to the certified community or those holding any other SHRM credential or certificate</li><li>The scheduling and administration of exams</li><li>The preparation, review, updating, validation, accreditation and/or administration of the exams and exam preparation materials</li><li>Publishing information regarding a candidate/certificant/credential-holder/certificate-holder against whom disciplinary action has been taken and the reason for that action.</li></ol><p>&#160;</p><p>Examples of how SHRM will not share a candidate's/certificant's/credential-holder/certificate-holder's Personal Information, certification exam performance information, or assessment information related to any other SHRM credential or certificate are as follows&#58;</p><ol style="list-style-type&#58;decimal;"><li>SHRM will not share the names of candidates who do not pass the exam with the general public.&#160;</li><li>SHRM will not share information about an examinee's performance on individual exam items with any person or entity.</li><li>SHRM will not disclose any Personal Information related to requests for reasonable accommodation under the ADA, or under similar Non-US requirements, other than as reasonably necessary to review and/or provide that accommodation.</li></ol><p>However, SHRM may disclose the above information as reasonably necessary in relation to the administration of the exam and exam program (e.g. to communicate ADA accommodations to our exam testing centers as necessary); to comply with the law, regulation, pursuant to court order or other legal process; in connection with the preparation, review, updating, validation, accreditation and/or administration of the exam or exam preparation materials; or to protect the rights, property or safety of SHRM, our members, certificants or others.</p><p><strong>d)&#160;&#160;&#160; Exceptions&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</strong></p><p>On occasion SHRM collects Personal Information through SHRM Websites with the intent to afford a greater degree of privacy for such information than is otherwise set forth in this privacy policy.&#160;</p><p>In those relatively rare situations where SHRM does so it will clearly disclose to you at the time it collects such information, what degree of privacy will be afforded to the Personal Information collected at such time; and SHRM will follow a process to assure that the specifically disclosed degree of privacy is in fact afforded to such information.&#160;</p><p>For example, SHRM may collect survey information through a survey where SHRM's use of any Personal Information from that survey is more limited than the general SHRM Privacy Statement would otherwise allow; in such a case, SHRM will disclose the stricter privacy policy governing that survey information at the time such Personal Information is collected from the survey respondent.</p><p><strong>5.&#160;&#160;&#160;&#160; How can you review and modify your Personal Information?</strong></p><p>You have the following options for modifying or causing to be deleted Personal Information or demographic information previously provided by you to SHRM.</p><ol style="list-style-type&#58;decimal;"><li>E-mail&#58;&#160;<a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a></li><li>Or if you are a SHRM member you may also visit&#58;&#160;<a href="/my"><span style="text-decoration&#58;underline;">Member Dashboard</span></a></li><li>Send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Customer Care Department.</li><li>Call&#58; 703-548-3440 or +1-800-283-7476.&#160;</li></ol><p><strong>6.&#160;&#160;&#160;&#160; What is the opt-out policy for SHRM Websites?</strong></p><p>SHRM provides members and customers with the opportunity to opt-out of receiving communications from us and our partners. If you no longer wish to receive specific communications or services, you have the following options&#58;</p><ol style="list-style-type&#58;decimal;"><li>You can send an e-mail to&#58;&#160;<a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a></li><li>You can send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Customer Care Department.</li><li>You can call the following telephone numbers&#58; +1-800-283-7476 or 703-548-3440.</li></ol><p>Instructions for opting out of any SHRM e-mail newsletter you receive are included with each e-mail.</p><p><strong>7.&#160;&#160;&#160;&#160; Your California Privacy Rights</strong></p><p><strong>For California Residents Only.</strong>&#160; SHRM may disclose your Personal Information to our subsidiaries or other third parties who may use that information to market directly to you.&#160; As a California resident, you have the right to opt-out of having your Personal Information licensed to such third parties.&#160; We will not share your information after we have received your notification that you are opting out.&#160; If you wish to opt-out you have the following options&#58;</p><ol style="list-style-type&#58;decimal;"><li>You can send an e-mail to&#58;&#160;<a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a></li><li>You can send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Customer Care Department.</li><li>You can call the following telephone numbers&#58; +1-800-283-7476 or 703-548-3440.&#160;</li></ol><p><strong>8.&#160;&#160;&#160;&#160; Your European Privacy Rights</strong></p><p><strong>For European Residents Only.</strong>&#160;If you are based in the EU, in certain circumstances, you have rights under the GDPR in relation to your Personal Information.</p><ul style="list-style-type&#58;square;"><li><strong>Request access to your Personal Information.</strong>&#160; You may have the right to request access to any Personal Information we hold about you as well as related information, including the purposes for processing the Personal Information, the recipients or categories of recipients with whom the Personal Information has been shared, where possible, the period for which the Personal Information will be stored, the source of the Personal Information, and the existence of any automated decision making.</li><li><strong>Request correction of your Personal Information.</strong>&#160;You may have the right to obtain without undue delay the rectification of any inaccurate Personal Information we hold about you.</li><li><strong>Request erasure of your Personal Information.</strong>&#160;You may have the right to request that Personal Information held about you is deleted.</li><li><strong>Request restriction of processing your Personal Information.&#160;</strong>You may have the right to prevent or restrict processing of your Personal Information.</li><li><strong>Request transfer of your Personal Information.&#160;</strong>You may have the right to request transfer of Personal Information directly to a third party where this is technically feasible.</li></ul><p>Where you believe that we have not complied with our obligations under this Privacy Policy or European data protection laws, you have the right to make a complaint to an EU Data Protection Authority, such as the UK Information Commissioner's Office.</p><p>If you wish to exercise your European data subject rights, you have the following options&#58;</p><ol style="list-style-type&#58;decimal;"><li>You can send an e-mail to&#58;&#160;<a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a></li><li>You can send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Customer Care Department.</li><li>You can call the following telephone numbers&#58; +1-800-283-7476 or 703-548-3440.&#160;</li></ol><p><strong>9.&#160;&#160;&#160;&#160; What kinds of security procedures are in place to protect against the loss, misuse or alteration of your information?</strong></p><p>SHRM Websites have security measures equal to or better than those reasonably expected in the industry, such as firewalls, in place to protect against the loss, misuse and alteration of your Personal Information under our control. While we cannot guarantee that loss, misuse or alteration to data will not occur, we take reasonable precautions to prevent such unfortunate occurrences. Certain particularly sensitive information, such as your credit card number, collected for a commercial transaction is encrypted prior to transmission.</p><p>You are ultimately responsible for the security of your SHRM login credentials. You may not share your SHRM login credentials with colleagues or friends so they can access content or features that are restricted to SHRM members only. You should log out of your browser at the end of each computer session to ensure that others cannot access your Personal Information and correspondence, especially if you share a computer with someone else or are using a computer in a public place like a library or Internet cafe.</p><p><strong>10.&#160; How do SHRM Websites use bulletin boards, discussion lists, and moderated chats?</strong></p><p>SHRM Websites make bulletin boards, discussion lists, and moderated chats available to its members. Any information that is disclosed in these areas becomes public information, and you should exercise caution when deciding to disclose your Personal Information. Although users may post messages that will appear on the message boards anonymously, SHRM does retain a record of who posts all notes.&#160;</p><p><strong>PART II – SHRM PRIVACY POLICY FOR INFORMATION COLLECTED OTHER THAN THROUGH SHRM WEBSITES</strong></p><p>If you submit Personal Information to SHRM through any channel other than SHRM Websites, the same privacy rules set forth at Part I above for SHRM Websites will be applied to such Personal Information you submit through channels other than SHRM Websites (including without limitation your opt-out rights at Part I, Section 6 above), except as follows&#58;</p><ol style="list-style-type&#58;decimal;"><li>The &quot;cookies&quot; and other tracking devices used by SHRM Websites and described in Part I above do not apply to Personal Information gathered through channels other than SHRM Websites.</li><li>Hardcopy Personal Information provided to SHRM which is not converted to electronic media and hosted by SHRM will be subject to different security procedures than will stored electronic Personal Information. SHRM has security measures equal to or better than those reasonably expected in the industry, in place to protect against the loss, misuse and alteration of your hardcopy Personal Information under our control.</li><li>When SHRM collects Personal Information through channels other than SHRM Websites it may in some instances apply a different privacy policy to such information; but where it does so it shall conspicuously disclose to you at the time of collection what privacy policy will apply to such information.&#160; For example, SHRM may collect survey information through a survey where SHRM's use of any Personal Information from that survey is more limited than the general SHRM Privacy Statement would otherwise allow; in such a case SHRM will disclose the stricter privacy policy governing that survey information at the time such Personal Information is collected from the survey respondent.</li></ol><p><strong>Privacy Policy Effective May, 2002. Updated February 24, 2011, August 7, 2014, October 1, 2015, December 14, 2015, May 25, 2018, and October 1, 2018.</strong></p><p>&#160;</p></div></div>
                    <!--googleoff: all-->
                    

<div class="marketing-promo-blurb noindex"></div>

                </div><!--	/.article-content	-->
                
                
                    <div id="article-paragraph-ad" class="article-paragraph-ad shrm-widget ad-holder visible-sm">
                        <input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Fluid" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Fluid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size1W" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size1H" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size1H" value="250" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size2W" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size2W" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size2H" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size2H" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Section" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Section" value="StandardArticleFlexUnitMobile" />
<div id="ctl00_PlaceHolderMain_ctl09_ctl00_pan_AdWordsWrapper">
		
    <div id="ctl00_PlaceHolderMain_ctl09_ctl00_pan_AdWords">
			
    
		</div>

	</div>

                    </div>
                    <div id="article-paragraph-ad-2" class="article-paragraph-ad shrm-widget ad-holder visible-xs">
                        <input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Fluid" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Fluid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Size1W" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Size1H" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Size1H" value="600" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Size2W" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Size2W" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Size2H" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Size2H" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Section" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Section" value="StandardArticleFlexUnit" />
<div id="ctl00_PlaceHolderMain_ctl09_ctl01_pan_AdWordsWrapper" class="scroller-ad noindex">
		
    <div id="ctl00_PlaceHolderMain_ctl09_ctl01_pan_AdWords">
			
    
		</div>

	</div>

                    </div>
                    <script>
                        if ($("[id$='_ControlWrapper_RichHtmlField']").find("> p").length > 3) {
                            $("*[id$='_ControlWrapper_RichHtmlField'] > p:nth-of-type(3)").after( $("#article-paragraph-ad") );
                        }
                        if ($("[id$='_ControlWrapper_RichHtmlField']").find("> p").length > 3) {
                            $("*[id$='_ControlWrapper_RichHtmlField'] > p:nth-of-type(3)").after($("#article-paragraph-ad-2"));
                        }
                    </script>
                
                <div id="ctl00_PlaceHolderMain_ctl10_pan_Main" class="shrm-tags shrm-tags-empty article-tags">
		
    

	</div>

                


                

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="/v1658514453/core/assets/js/socialbar.js"></script>
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_SiteUrl" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_SiteUrl" value="https://www.shrm.org" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_ArticleGuid" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_ArticleGuid" value="05f756a6-7090-4d03-976f-a430f545b58e" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_ArticleTypeId" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_ArticleTypeId" value="1" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_ArticleLink" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_ArticleLink" value="05f756a6-7090-4d03-976f-a430f545b58e" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_Headline" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_Headline" value="Privacy Policy" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_Subheading" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_Subheading" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_MembershipUrl" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_MembershipUrl" value="https://membership.shrm.org/" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_EmailAFriend_Hash" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_EmailAFriend_Hash" value="501F5108A24CF46DE09DD507CA669BC0" />
<script type="text/javascript">
    bookmarkExists();
    likeExists();
    function EmailAFriend() {
        //document.getElementById('emailAFriendModalContent').classList.add('in');
        var _IsValid = IsVa11d4m();
        if (_IsValid) {
            var _FromName = $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').val();
            var _FromEmail = $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').val();
            var _ToEmail = $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').val();
            var _Subject = $('#ctl00_PlaceHolderMain_ctl12_emailFriendsSubject').val();
            var _Message = $('#ctl00_PlaceHolderMain_ctl12_hf_EmailFriendMessageValue').val();
            var _Hash = $('#ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_EmailAFriend_Hash').val();
            $.ajax({
                type: "POST",
                async: false,
                url: "/_layouts/15/SHRM.Core/ajax/api.aspx/SendAFriend",
                data: JSON.stringify({ '_FromName': _FromName, '_FromEmail': _FromEmail, '_ToEmail': _ToEmail, '_Subject': _Subject, '_Message': _Message, '_Hash': _Hash }),
                //data: "{_FromName: '" + _FromName + "', _FromEmail: '" + _FromEmail + "', _ToEmail: '" + _ToEmail + "', _Subject: '" + _Subject + "', _Message: '" + _Message + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $('#emailAFriendModalBody').hide('slow');
                    $('#emailAFriendModalMessage').show('slow');
                    $('#emailAFriendModalMessageLabel').html('<span>Your message has been sent successfully.<br /><br />SHRM Members please note: Links you share to member- protected content will not be viewable by non- members.</span>');
                    $('#emailFriendsSendBtn').hide('slow');
                    $('#emailFriendsCancelBtn').hide('slow');
                    $('#emailFriendsCloseBtn').show('slow');
                    setTimeout(function () {
                        $('#emailAFriendModal').modal('hide');
                        ResetEMailAFriend();
                    }, 5000);
                },
                error: function (msg) {
                    $('#emailAFriendModalBody').hide('slow');
                    $('#emailAFriendModalMessage').show('slow');
                    $('#emailAFriendModalMessageLabel').html('<span>There was a problem sending your message.<br/>Please try again.</span>');
                }
            });
        }
    }
    function ResetEMailAFriend() {
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').val("");
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').val("");
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').val("");
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsSubject').val($('#ctl00_PlaceHolderMain_ctl12_hf_EmailFriendSubjectValue').val());
        $('#emailAFriendModalBody').show('slow');
        $('#emailAFriendModalMessage').hide('slow');
        $('#emailAFriendModalMessageLabel').html('');
        $('#emailFriendsSendBtn').show('slow');
        $('#emailFriendsCancelBtn').show('slow');
        $('#emailFriendsCloseBtn').hide('slow');
    }
    function CombineSubject() {
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsSubject').val($('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').val() + $('#ctl00_PlaceHolderMain_ctl12_hf_EmailFriendSubjectValue').val());
        IsVa11d4m();
    }
    function isEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function IsVa11d4m() {
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').parent().removeClass('has-error');
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').parent().removeClass('has-error');
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').parent().removeClass('has-error');
        var _IsValid = true;
        if ($('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').val() == "") {
            _IsValid = false;
            $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').parent().addClass('has-error');
        }
        if (!isEmail($('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').val())) {
            _IsValid = false;
            $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').parent().addClass('has-error');
        }
        if (!isEmail($('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').val())) {
            _IsValid = false;
            $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').parent().addClass('has-error');
        }
        return _IsValid;
    }
    function Va11d4m() {
        $('#emailFriendsSendBtn').removeClass('disabled');
    }
    function EmailAFriendFromReset() {
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').val('');
        $('#emailAFriendModalBody').show('slow');
        $('#emailAFriendModalMessage').hide('slow');
        $('#emailAFriendModalMessageLabel').html('<span></span>');
        $('#emailFriendsSendBtn').show('slow');
        $('#emailFriendsCancelBtn').show('slow');
        $('#emailFriendsCloseBtn').hide('slow');
    }
</script>
<div id="modal_bookmark_save" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>You have successfully saved this page as a bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="window.location = window.location.protocol + '//' + window.location.hostname + '/my/bookmarks/';">My Bookmarks</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_delete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please confirm that you want to proceed with deleting bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="deleteBookmark();">Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteBookmarkCanceled();">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_deleted" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>You have successfully removed bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="window.location = window.location.protocol + '//' + window.location.hostname + '/my/bookmarks/';">My Bookmarks</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_canceldelete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Delete canceled</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_loginbeforesave" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please log in as a SHRM member before saving bookmarks.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>                
                <button type="button" class="btn btn-primary" onclick="ProceedToLogin();">Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_expiredsession" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Your session has expired. Please log in as a SHRM member.</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:" class="btn btn-empty" data-dismiss="modal">Cancel</a>
                <button type="button" class="btn btn-primary" onclick="ProceedToLogin();">Sign In</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_membershipbeforesave" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please purchase a SHRM membership before saving bookmarks.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="ProceedToMembership();">Join</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_error" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p id="modal_bookmark_error_msg">An error has occurred</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_limit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p id="modal_bookmark_limit_msg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="window.location = window.location.protocol + '//' + window.location.hostname + '/my/bookmarks/';">My Bookmarks</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="emailAFriendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content shrm-loading" id="emailAFriendModalContent">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Email A Friend</h4>
            </div>-->
            <div class="modal-body padd-v-20" id="emailAFriendModalBody">
                <div class="form-group" style="min-height: 65px;">
                    <label for='ctl00_PlaceHolderMain_ctl12_emailFriendsFrom'>From</label>
                    <input name="ctl00$PlaceHolderMain$ctl12$emailFriendsFrom" type="text" id="ctl00_PlaceHolderMain_ctl12_emailFriendsFrom" class="form-control marg-v-0" onkeyup="CombineSubject();" />
                </div>
                <div class="form-group" style="min-height: 65px;">
                    <label for='ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail'>From Email</label>
                    <input name="ctl00$PlaceHolderMain$ctl12$emailFriendsFromEmail" type="text" id="ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail" class="form-control marg-v-0" onkeyup="IsVa11d4m();" />
                </div>
                <div class="form-group" style="min-height: 65px;">
					<label for='ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail'>To Email</label>
                    <input name="ctl00$PlaceHolderMain$ctl12$emailFriendsToEmail" type="text" id="ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail" class="form-control marg-v-0" placeholder="username@example.com" onkeyup="IsVa11d4m();" />
					<p class="small text-danger" id="emailFriendsToEmailMsg" style="display: none;"></p>
				</div>
                <div class="form-group" style="min-height: 65px;">
                    <label for='ctl00_PlaceHolderMain_ctl12_emailFriendsSubject'>Subject</label>
                    <input name="ctl00$PlaceHolderMain$ctl12$emailFriendsSubject" type="text" id="ctl00_PlaceHolderMain_ctl12_emailFriendsSubject" class="form-control marg-v-0 disabled" value=" sent you this SHRM article: Privacy Policy" />
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_EmailFriendSubjectValue" id="ctl00_PlaceHolderMain_ctl12_hf_EmailFriendSubjectValue" value=" sent you this SHRM article: Privacy Policy" />
                </div>
                <div class="form-group" style="min-height: 65px;">
                    <label for='ctl00_PlaceHolderMain_ctl12_emailFriendsSubject'>Message</label>
                    <span class="form-control marg-v-0">Hi,<br /><br />

I thought you'd like this article I found on the SHRM website: <br />
<a href='https://www.shrm.org/about-shrm/Pages/Privacy-Policy.aspx'>Privacy Policy</a>  </span>
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_EmailFriendMessageValue" id="ctl00_PlaceHolderMain_ctl12_hf_EmailFriendMessageValue" value="Hi,&amp;lt;br /&amp;gt;&amp;lt;br /&amp;gt;

I thought you&amp;#39;d like this article I found on the SHRM website: &amp;lt;br /&amp;gt;
&amp;lt;a href=&amp;#39;https://www.shrm.org/about-shrm/Pages/Privacy-Policy.aspx&amp;#39;&amp;gt;Privacy Policy&amp;lt;/a&amp;gt;  " />
                </div>
                <div id="ctl00_PlaceHolderMain_ctl12_reCaptcha" class="g-recaptcha" data-callback="Va11d4m" data-sitekey="6LcY0T0UAAAAAMVoOAWR1w4uWXAcd80eeIO8o29R"></div>
            </div>
            <div class="modal-body padd-v-20" id="emailAFriendModalMessage" style="display: none;">
                <div id="emailAFriendModalMessageLabel" class="form-group" style="min-height: 65px;">
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:" class="btn btn-primary disabled" onclick="EmailAFriend();" id="emailFriendsSendBtn">Send</a>
                <a href="javascript:" class="btn btn-default" data-dismiss="modal" id="emailFriendsCancelBtn" onclick="EmailAFriendFromReset();">Cancel</a>
                <a href="javascript:" class="btn btn-default" data-dismiss="modal" id="emailFriendsCloseBtn" style="display: none;" onclick="EmailAFriendFromReset();">Close</a>
            </div>
        </div>
    </div>
</div>

                <div id="ctl00_PlaceHolderMain_ctl13_pan_ArticleRecommendedForYou" class="row-shrm-recommended-inline clearfix noindex">
		
    <a id="ctl00_PlaceHolderMain_ctl13_hl_FakeLink" class="fake-link" href="https://www.shrm.org/resourcesandtools/hr-topics/benefits/pages/agencies-preview-form-5500-for-2019-filings-while-revamp-delayed.aspx">&nbsp;<span class="sr-only">Fake link</span></a>
    <div id="ctl00_PlaceHolderMain_ctl13_pan_ImageHolder" class="image-holder pull-left">
			
  	    <img id="ctl00_PlaceHolderMain_ctl13_img_Image" title="Agencies Preview Form 5500 for 2019 Filings While Revamp Is Delayed" class="img-responsive" sizes="100vw" src="https://cdn.shrm.org/image/upload/c_crop,h_438,w_779,x_0,y_0/w_auto:100:150,q_auto,f_auto/v1/Benefits/form_filing_vo0sfj" alt="Agencies Preview Form 5500 for 2019 Filings While Revamp Is Delayed" />
    
		</div>
    <p class="shrm-Label text-uppercase">Recommended for you</p>
    <h5>Agencies Preview Form 5500 for 2019 Filings While Revamp Is Delayed</h5>  

	</div>
<script>
    //console.log($("[id$='_ControlWrapper_RichHtmlField']").find("p").length);
    //if ($("[id$='_ControlWrapper_RichHtmlField']").find("p").length > 5) {
    //    var _Count = $("[id$='_ControlWrapper_RichHtmlField']").find("p").length - 2;
    //    $("[id$='pan_ArticleRecommendedForYou']").appendTo($("[id$='_ControlWrapper_RichHtmlField']").find("p:nth-child(" + _Count + ")"));
    //}
</script>

                


<a name="TaxonomyTreeViewAnchor" id="TaxonomyTreeViewAnchor"></a>
<script type="text/javascript">
    function SelectTerm(_TermId) {
        FindChildNodes(_TermId, false);
        FindParentNodes(_TermId, $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").prev().prop('checked'));
        FindSameTitleTerms(_TermId, $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").prev().prop('checked'));
        FindRootPath();
        SetSelectedTerms();
    }
    function SetSelectedTerms() {
        var _HiddenValues = "";
        var _VisibleValues = "";
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").each(function () {
            _HiddenValues += $(this).next().next().text() + "|" + $(this).next().val() + ";";
            _VisibleValues += "<span class='valid-text' title=''>" + $(this).next().next().text() + "</span>; ";
        });
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val(_HiddenValues);
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children().eq(1).children().eq(0).html(_VisibleValues);
        $("[id$='PreventEmptyTaxonomy']").val(_HiddenValues);
    }
    function LoadSelectedTerms() {
        try { $("#hf_TaxonomyReset_NAID").val($(".article-taxonomy-treeview-node input[value='N/A']").prev().prev().val()); }
        catch (err) { }
        var _Terms = $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val().split(';');
        if ($("#hf_TaxonomyReset_NAID").val() != '') {
            $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").parent().parent().parent().parent().parent().hide();
            var _IsNA = false;
            $.each(_Terms, function (index, value) {
                if ($("#hf_TaxonomyReset_NAID").val() == value.split('|')[1]) { _IsNA = true; }
                else { $(".article-taxonomy-treeview-node input[value='" + value.split('|')[1] + "']").prev().prop('checked', true); }
            });
            if (_IsNA) {
                $("#cb_TaxonomyReset_NA").prop('checked', true);
                $(".article-taxonomy-treeview-node input[type=checkbox]:checked").prop('checked', false);
                $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").prev().prop('checked', true);
                SetSelectedTerms();
                $("[id$='pan_TaxonomyTreeView']").hide();
            }
        }
        else {
            $("#cb_TaxonomyReset_NA").parent().hide();
            $.each(_Terms, function (index, value) {
                $(".article-taxonomy-treeview-node input[value='" + value.split('|')[1] + "']").prev().prop('checked', true);
            });
        }
        SetSelectedTerms();
    }
    function FindParentNodes(_TermId, _Checked) {
        var _ParentCheckbox = $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").parent().parent().parent().parent().parent().parent().prev().find('.article-taxonomy-treeview-node').children().eq(0).children().eq(0);
        if ($(_ParentCheckbox).is(':checkbox')) {
            $(_ParentCheckbox).prop('checked', _Checked);
            FindParentNodes($(_ParentCheckbox).next().val(), _Checked);
        }
    }
    function FindChildNodes(_TermId, _Checked) {
        $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").parent().parent().parent().parent().parent().next().find('.article-taxonomy-treeview-node input[type=checkbox]').each(function () {
            $(this).prop('checked', _Checked);
        });
    }
    function FindSameTitleTerms(_TermId, _Checked) {
        var _Title = $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").next().next().val();
        $(".article-taxonomy-treeview-node input[value='" + _Title + "']").each(function () {
            $(this).prev().prev().prev().prop('checked', _Checked);
            FindParentNodes($(this).prev().prev().val(), _Checked);
        });
    }
    function FindRootPath() {
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").each(function () {
            FindParentNodes($(this).next().val(), true);
        });
    }
    function RemoveSelectedTerms() {
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").prop('checked', false);
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val('');
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children().eq(1).children().eq(0).html('');
        $("[id$='PreventEmptyTaxonomy']").val('');
        console.log('#cb_TaxonomyReset_NA is: ' + $("#cb_TaxonomyReset_NA").prop('checked'));
        if ($("#cb_TaxonomyReset_NA").prop('checked') == true) {
            console.log('#cb_TaxonomyReset_NA is: TRUE');
            $("[id$='pan_TaxonomyTreeView']").hide();
            $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").prev().prop('checked', true);
        }
        else {
            console.log('#cb_TaxonomyReset_NA is: FALSE');
            $("[id$='pan_TaxonomyTreeView']").show();
        }
        SetSelectedTerms();
    }
    function FixAspTreeView() {
        $(".article-taxonomy-treeview td[class!='article-taxonomy-treeview-node'] a").each(function () {
            $(this).attr('onclick', $(this).attr('href')).removeAttr('href');
        });
    }

</script>









            </div>
            <div class="col-md-4">
                <div class="shrm-widget ad-holder hidden-xs hidden-sm">
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Fluid" id="ctl00_PlaceHolderMain_ctl15_hf_Fluid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Size1W" id="ctl00_PlaceHolderMain_ctl15_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Size1H" id="ctl00_PlaceHolderMain_ctl15_hf_Size1H" value="250" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Size2W" id="ctl00_PlaceHolderMain_ctl15_hf_Size2W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Size2H" id="ctl00_PlaceHolderMain_ctl15_hf_Size2H" value="600" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Section" id="ctl00_PlaceHolderMain_ctl15_hf_Section" value="StandardArticleFlexUnit" />
<div id="ctl00_PlaceHolderMain_ctl15_pan_AdWordsWrapper">
		
    <div id="ctl00_PlaceHolderMain_ctl15_pan_AdWords">
			
    
		</div>

	</div>

                </div>
                <div id="ctl00_PlaceHolderMain_HrJobs_pan_JobFinder" class="shrm-widget shrm-job-finder noindex">
		
    <h3 class="shrm-Title-Small text-uppercase text-center">Job Finder</h3>
    <h5 class="text-center">Find an HR Job Near You</h5>
    <div class="finder-form">
        <div class="input-holder">
            <label for='ctl00_PlaceHolderMain_HrJobs_searchTerm' class="sr-only">CITY, STATE, ZIP</label>
            <input name="ctl00$PlaceHolderMain$HrJobs$searchTerm" type="text" id="ctl00_PlaceHolderMain_HrJobs_searchTerm" placeholder="CITY, STATE, ZIP" class="form-control" />
        </div>
        <a href="javascript:" id="ctl00_PlaceHolderMain_HrJobs_searchJobs" class="btn btn-success btn-lg btn-cta" onclick="return OpenInNewWindow(&#39;ctl00_PlaceHolderMain_HrJobs_searchJobs&#39;,&#39;ctl00_PlaceHolderMain_HrJobs_searchTerm&#39;);"><span class="status">Search Jobs</span></a>
    </div>
    <a id="ctl00_PlaceHolderMain_HrJobs_hl_PostAJob" class="shrm-job-finder-post-a-job" href="http://hrjobs.shrm.org/jobs/products">Post a Job</a>
    <script language="javascript" type="text/javascript">
        function OpenInNewWindow(anchorTagId, textBoxTagId) {
            var searchTerm = document.getElementById(textBoxTagId);
            var searchJobs = document.getElementById(anchorTagId);
            searchJobs.setAttribute("href", "javascript:");
            searchJobs.setAttribute("target", "");

            if (searchTerm.value.length > 0) {
                searchJobs.setAttribute("target", "_blank");
                var href = "http://hrjobs.shrm.org/jobs/search/results?radius=0&location=" + searchTerm.value;
                searchJobs.setAttribute("href", href);
            }
        }
		    $(function() {
			    $uccHrJobsHtml = $('#ucc_HRJobs');
			    $uccHrJobsPlaceholder = $('nav.navbar li a').filter(function(){
				     return $(this).text() === "uccHrJobs";
			    });
			
			    if( $uccHrJobsHtml.length > 0 && $uccHrJobsPlaceholder.length > 0){
				    $('.shrm-job-finder',$uccHrJobsHtml).addClass('shrm-mini-job-finder');
				    $($uccHrJobsPlaceholder).replaceWith( $($uccHrJobsHtml).show());
			    }
		    });
    </script>

	</div>


                <div class="shrm-widget-row">
                    <div>

<div class="shrm-most-popular-widget shrm-widget non-tiles col-sm-6 col-md-12 noindex ">
	<h2 class="shrm-Title-Small text-center text-uppercase">Most popular</h2>
    <div class="list-group shrm-list-stream">
        
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="https://www.shrm.org/ResourcesAndTools/hr-topics/employee-relations/Pages/Convincing-CEOs-to-Make-Harassment-Prevention-a-Priority.aspx"><img title="Convincing CEOs to Make Harassment Prevention a Priority" class="img-responsive" sizes="100vw" src="https://cdn.shrm.org/image/upload/c_crop%2ch_4140%2cw_7360%2cx_0%2cy_0/c_fit%2cf_auto%2cq_auto%2cw_767/v1/Employee%20Relations/harassment_omo7pr?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjAsIngyIjo3MzYwLCJ5MiI6NDE0MCwidyI6NzM2MCwiaCI6NDE0MH19" alt="Convincing CEOs to Make Harassment Prevention a Priority" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"><span class="sr-only">anchor</span></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/ResourcesAndTools/hr-topics/employee-relations/Pages/Convincing-CEOs-to-Make-Harassment-Prevention-a-Priority.aspx">Convincing CEOs to Make Harassment Prevention a Priority</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="https://www.shrm.org/ResourcesAndTools/hr-topics/technology/Pages/HR-data-security.aspx"><img title="Workplace Data Security: Is Your Information Protected?" class="img-responsive" sizes="100vw" src="https://cdn.shrm.org/image/upload/c_crop%2ch_1964%2cw_3491%2cx_267%2cy_684/c_fit%2cf_auto%2cq_auto%2cw_767/v1/Technology/iStock-914839458_o6ovwv?databtoa=eyIyMXg5Ijp7IngiOjAsInkiOjg1OSwieDIiOjM3NTgsInkyIjoyNDcwLCJ3IjozNzU4LCJoIjoxNjExfSwiMTZ4OSI6eyJ4IjoyNjcsInkiOjY4NCwieDIiOjM3NTgsInkyIjoyNjQ4LCJ3IjozNDkxLCJoIjoxOTY0fX0%3d" alt="Workplace Data Security: Is Your Information Protected?" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"><span class="sr-only">anchor</span></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/ResourcesAndTools/hr-topics/technology/Pages/HR-data-security.aspx">Workplace Data Security: Is Your Information Protected?</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="https://www.shrm.org/ResourcesAndTools/hr-topics/benefits/Pages/commuting-and-adoption-benefit-limits-2019.aspx"><img title="Commuting and Adoption Benefit Amounts Rise in 2019" class="img-responsive" sizes="100vw" src="https://cdn.shrm.org/image/upload/c_crop%2ch_408%2cw_724%2cx_0%2cy_28/c_fit%2cf_auto%2cq_auto%2cw_767/v1/Benefits/commute_child_ifuqzd?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjI4LCJ4MiI6NzI0LCJ5MiI6NDM2LCJ3Ijo3MjQsImgiOjQwOH19" alt="Commuting and Adoption Benefit Amounts Rise in 2019" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"><span class="sr-only">anchor</span></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/ResourcesAndTools/hr-topics/benefits/Pages/commuting-and-adoption-benefit-limits-2019.aspx">Commuting and Adoption Benefit Amounts Rise in 2019</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
    </div>
    <div class="bottom-button" style="display: none;"> 
        <a class="btn btn-block btn-blank shrm-CTA-Green" onClick="$(this).closest('.shrm-widget').find('.collapse:not(.in)').slice(0,2).collapse()">MORE</a>
    </div>
    <input type="hidden" name="ctl00$PlaceHolderMain$ctl16$hf_Message" id="ctl00_PlaceHolderMain_ctl16_hf_Message" />
</div>
</div>
                    

                    <div><div id="ctl00_PlaceHolderMain_ctl18_pan_Main" class="row-promo-panel shrm-widget col-sm-12 col-md-12 noindex " data-personalized-offer="">
		
    <div class="promo-panel-wrapper outlined text-center">        
  	    <h3 class="shrm-Title-Small text-uppercase text-center">MEMBER BENEFITS</h3>
        <a id="ctl00_PlaceHolderMain_ctl18_hl_ImageWrapper" class="promo-image" href="https://www.shrm.org/about-shrm/pages/affinityprogram.aspx"><img id="ctl00_PlaceHolderMain_ctl18_img_Image" title="The SHRM Member Discounts program provides member-only access to discounts on products and services you can apply to your life and career, and share with your company." class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_1192%2cw_2121%2cx_0%2cy_0/c_fit%2cf_auto%2cq_auto%2cw_767/v1/Home%20Page%20Carousel/Travel_RedPlaid_odonlc?databtoa=eyIyMXg5Ijp7IngiOjAsInkiOjU2LCJ4MiI6MjEyMSwieTIiOjk2NSwidyI6MjEyMSwiaCI6OTA4fSwiMTZ4OSI6eyJ4IjowLCJ5IjowLCJ4MiI6MjEyMSwieTIiOjExOTIsInciOjIxMjEsImgiOjExOTJ9fQ%3d%3d" alt="The SHRM Member Discounts program provides member-only access to discounts on products and services you can apply to your life and career, and share with your company." /></a>
        <p>The SHRM Member Discounts program provides member-only access to discounts on products and services you can apply to your life and career, and share with your company.</p>
    </div>
    <a id="ctl00_PlaceHolderMain_ctl18_hl_More" class="btn btn-success btn-cta text-uppercase btn-customized" href="https://www.shrm.org/about-shrm/pages/affinityprogram.aspx">LEARN MORE</a>

	</div>

<script id="promo-panel-script" async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>     
</div>
                </div><!-- /.shrm-widget-row	-->
                <div class="shrm-widget ad-holder hidden-xs hidden-sm">
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Fluid" id="ctl00_PlaceHolderMain_ctl19_hf_Fluid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Size1W" id="ctl00_PlaceHolderMain_ctl19_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Size1H" id="ctl00_PlaceHolderMain_ctl19_hf_Size1H" value="250" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Size2W" id="ctl00_PlaceHolderMain_ctl19_hf_Size2W" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Size2H" id="ctl00_PlaceHolderMain_ctl19_hf_Size2H" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Section" id="ctl00_PlaceHolderMain_ctl19_hf_Section" value="StandardArticleBox" />
<div id="ctl00_PlaceHolderMain_ctl19_pan_AdWordsWrapper">
		
    <div id="ctl00_PlaceHolderMain_ctl19_pan_AdWords">
			
    
		</div>

	</div>

                </div>
            </div>
        </div>
        

        

        <div id="ctl00_PlaceHolderMain_ctl22_pan_Main" class="row-shrm-sponsors shrm-widget noindex">
		
	<h2 class="shrm-Title text-uppercase">SPONSOR OFFERS</h2>
	<div class="row sponsors-wrapper">
        <ins class="adbladeads" data-cid="18802-1901870943" data-host="web.industrybrains.com" data-tag-type="4" data-protocol="https" style="display:none"></ins>
        <div class="shrm-sponsor col-md-4">
    	    <h4><a id="ctl00_PlaceHolderMain_ctl22_hl_InHouseAdvert_Title" href="http://vendordirectory.shrm.org/" target="_blank">Find the Right Vendor for Your HR Needs</a></h4>
            <p>
      	        SHRM’s HR Vendor Directory contains over 10,000 companies
            </p>
            <a id="ctl00_PlaceHolderMain_ctl22_hl_InHouseAdvert_CTA" class="shrm-CTA-Green text-uppercase link-external" href="http://vendordirectory.shrm.org/" target="_blank">Search &amp; Connect</a>
        </div>
	</div>

	</div>
<script src='//www.shrm.org/Documents/SponsorOfferFetchFile.js'></script>
<input type="hidden" name="ctl00$PlaceHolderMain$ctl22$hf_DataCid" id="ctl00_PlaceHolderMain_ctl22_hf_DataCid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl22$hf_DataHost" id="ctl00_PlaceHolderMain_ctl22_hf_DataHost" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl22$hf_DataTagType" id="ctl00_PlaceHolderMain_ctl22_hf_DataTagType" />

    </div>
    
    



<div style='display:none' id='hidZone'><menu class="ms-hide">
		<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/15/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

		</ie:menuitem>
	</menu></div>
									<!-- End - Layout content -->
								  
								    <!-- --------------------------------- SHRM FOOTER STARTS	--> 
								    

<div class="container-fluid container-footer noindex"> 
      <!--<div class="row footer-newsletter shrm-role-member-hidden">
            <div class="container">
                <div class="text-center newsletter-title"><h6>The best of HR News</h6></div>
                <div class="text-center newsletter form">
                    <div class="footer-subscribe-form" >
                        <input type="email" placeholder="YOUR EMAIL ADDRESS" />
                        <a class="btn btn-cta btn-success btn-lg">Subscribe</a>
                    </div>
                </div>
                <div class="text-center newsletter-all"> 
                    <a href="http://shrm.org/publications/e-mailnewsletters/pages/default.aspx">SEE ALL NEWSLETTERS</a>
                </div>								
            </div>    
        </div>-->
    <div class="row footer-member-newsletter">
        <div class="container text-center">
            <div class="footer-member-newsletter-cta">
                <h6>Stay Informed with SHRM Newsletters</h6>
                <a id="ctl00_ctl77_hl_Newsletter_SignUpToday_Link" class="btn btn-primary btn-cta btn-lg" href="https://lp.shrm.org/preferences.html" target="_blank">SIGN UP TODAY</a>
            </div>
        </div>
    </div>
    <div class="row container-footer-lower">
        <div class="container">
            <div class="footer-socials text-center">
                <h3 class="text-center shrm-Label">
                    JOIN THE CONVERSATION
                </h3>
                <span>
                    <a id="ctl00_ctl77_hl_SocialLink_Facebook" href="http://www.facebook.com/societyforhumanresourcemanagement" target="_blank"><i class="fa fa-facebook"><span class="sr-only">Facebook</span></i></a>
                    <a id="ctl00_ctl77_hl_SocialLink_LinkedIn" href="http://www.linkedin.com/company/11282?trk=NUS_CMPY_TWIT" target="_blank"><i class="fa fa-linkedin"><span class="sr-only">LinkedIn</span></i></a>
                    <a id="ctl00_ctl77_hl_SocialLink_Instagram" href="https://instagram.com/shrmofficial/" target="_blank"><i class="fa fa-instagram"><span class="sr-only">Instagram</span></i></a>
                </span> <span>
                    <a id="ctl00_ctl77_hl_SocialLink_YouTube" href="http://www.youtube.com/shrmofficial" target="_blank"><i class="fa fa-youtube-play"><span class="sr-only">YouTube</span></i></a>
                    <a id="ctl00_ctl77_hl_SocialLink_RSS" href="/about-shrm/pages/rss.aspx" target="_blank"><i class="fa fa-rss"><span class="sr-only">RSS</span></i></a>
                    <a id="ctl00_ctl77_hl_SocialLink_Twitter" href="http://twitter.com/SHRM" target="_blank"><i class="fa fa-twitter"><span class="sr-only">Twitter</span></i></a>
                </span> 
            </div>
            <div class="footer-nav clearfix">
<div class="col-md-9 footer-col footer-col-about clearfix">
<div class="row">
<div class="col-sm-4 col-xs-12">
<div class="clearfix">
<div class="row">
<h4 class="footer-col-title col-xs-12">SHRM</h4>
<ul class="col-xs-12">
<li class="col-xs-6 col-sm-12"><a href="https://www.shrm.org/about-shrm/Pages/default.aspx">About SHRM</a></li>
<li class="col-xs-6 col-sm-12"><a href="https://www.shrm.org/about-shrm/Pages/Membership.aspx">Membership</a></li>
<li class="col-xs-6 col-sm-12"><a href="https://www.shrm.org/about-shrm/Pages/Bylaws--Code-of-Ethics.aspx">Bylaws &amp; Code of Ethics</a></li>
<li class="col-xs-6 col-sm-12"><a href="https://www.shrm.org/about-shrm/press-room/Pages/default.aspx">Press Room</a></li>
<li class="col-xs-6 col-sm-12"><a href="http://www.cfgi.org/">Council for Global Immigration</a></li>
<li class="col-xs-6 col-sm-12"><a href="http://www.hrps.org/">HR People + Strategy</a></li>
<li class="dropdown col-xs-6 col-sm-12 marg-h-0"><a id="footer-international" class="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="current-site">SHRM Global </span><em class="fa fa-angle-down">&nbsp;</em> </a>
<ul class="dropdown-menu" aria-labelledby="footer-international">
<li class="collapse in"><a href="../../../../pages/default.aspx?loc=null">SHRM Global</a></li>
<li class="collapse in" data-loc="/pages/default.aspx?loc=india"><a href="../../../../pages/default.aspx?loc=india">SHRM India</a></li>
<li class="collapse in" data-loc="/about-shrm/Pages/SHRM-MENA.aspx"><a href="../../../../about-shrm/Pages/SHRM-MENA.aspx">SHRM MENA</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
<div class="col-sm-4 col-xs-12">
<div class="row">
<div class="clearfix col-sm-12 col-xs-6">
<h4 class="footer-col-title">WORK FOR SHRM</h4>
<ul class="col-sm-12">
<li><a class="link-external" href="//www.shrm.jobs" target="_blank" rel="noopener">Career Opportunities</a></li>
</ul>
</div>
<div class="clearfix col-sm-12 col-xs-6">
<h4 class="footer-col-title">ELEVATE HR</h4>
<ul class="col-sm-12">
<li><a href="../../../../foundation/Pages/default.aspx">SHRM Foundation</a>&nbsp;&nbsp;<br class="visible-xs" /><a class="btn btn-success btn-cta" href="http://www.shrm.org/about/foundation/supportthefoundation/contributions/pages/default.aspx" target="_blank" rel="noopener">DONATE</a></li>
</ul>
</div>
</div>
</div>
<div class="col-sm-4 col-xs-12">
<div class="clearfix">
<div class="row">
<h4 class="footer-col-title col-xs-12">WORK WITH SHRM</h4>
<ul class="col-xs-12">
<li class="col-sm-12 col-xs-6"><a class="link-external" href="../../../../mlp/Pages/speakers-bureau/Home.aspx" target="_blank" rel="noopener">Speakers Bureau</a></li>
<li class="col-sm-12 col-xs-6"><a class="link-external" href="../../../../about-shrm/pages/copyright--permissions.aspx" target="_blank" rel="noopener">Copyright &amp; Permissions</a></li>
<li class="col-sm-12 col-xs-6"><a class="link-external" href="../../../../mediakit/pages/default.aspx" target="_blank" rel="noopener">Advertise with Us</a></li>
<li class="col-sm-12 col-xs-6"><a class="link-external" href="http://hrjobs.shrm.org/jobs/products" target="_blank" rel="noopener">Post a Job</a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="footer-bottom clearfix text-center">
<div class="footer-contact-us"><a id="ctl00_ctl77_hl_ContactUs_Page" href="../../../../about-shrm/Pages/Contact-Us.aspx">Contact Us</a> <span class="links-divider">|</span> <a href="tel:800.283.SHRM%20(7476)">800.283.SHRM (7476)</a></div>
<div class="legal-nav"><span class="copyright">&copy;
<script>var currentYear = new Date().getFullYear();document.write(currentYear);</script>
SHRM. All Rights Reserved</span>
<ul class="list-unstyled legal-links" style="display: inline-block;">
<li><a href="../../../../about-shrm/Pages/Privacy-Policy.aspx">Privacy Policy</a></li>
<li><span class="links-divider">|</span></li>
<li><a href="../../../../about-shrm/Pages/Privacy-Policy.aspx#California">Your California Privacy Rights</a></li>
<li><span class="links-divider">|</span></li>
<li><a href="../../../../about-shrm/Pages/Terms-of-Use.aspx">Terms of Use</a></li>
<li><span class="links-divider">|</span></li>
<li><a href="../../../../about-shrm/Pages/Site-Map.aspx">Site Map</a></li>
</ul>
</div>
<small class="text-center small">SHRM provides content as a service to its readers and members. It does not offer legal advice, and cannot guarantee the accuracy or suitability of its content for a particular purpose. <a href="../../../../about-shrm/Pages/Terms-of-Use.aspx#Disclaimer">Disclaimer</a></small></div>
<p>
<script>
$(document).ready(function () {
var currentUrl = window.location.href.toLowerCase();
var currentLocation = getCookie("SHRM_Core_CurrentUser_LocationID");
if(currentUrl.indexOf("/about-shrm/pages/shrm-china.aspx") > -1) {
$("span.current-site").html("SHRM China ");
}
else if(currentUrl.indexOf("/about-shrm/pages/shrm-mena.aspx") > -1) {
$("span.current-site").html("SHRM MENA ");
}
});
</script>
</p>
            
                
        </div>
    </div>
</div>

							    </div><!--/ .shrm-site	------------------------------------------------------>
							
</div>
						</div>
						<!-- This should stay here -->
						<div id="DeltaFormDigest">
	
							
								<script type="text/javascript">//<![CDATA[
        var formDigestElement = document.getElementsByName('__REQUESTDIGEST')[0];
        if (!((formDigestElement == null) || (formDigestElement.tagName.toLowerCase() != 'input') || (formDigestElement.type.toLowerCase() != 'hidden') ||
            (formDigestElement.value == null) || (formDigestElement.value.length <= 0)))
        {
            formDigestElement.value = '0xB059E1ED38EE4C44B62F338CABBC05573019ABF29F5074072919B656A6B82ECD463B4BAB779E4680C1DB71D9319F9FCCDAB204F6BCBAF760003B4B24A50E5831,26 Nov 2018 06:29:45 -0000';
            g_updateFormDigestPageLoaded = new Date();
        }
        //]]>
        </script>
							
						
</div>
					</div>
				</div>
			</div>
			<!-- End - Working area -->
            
			<!-- Start - Hidden staff (do not remove this) -->
			
			
			
			
			

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);function loadMDN2() { EnsureScript('MDN.js', typeof(loadFilterFn), null); }
function loadMDN1() { ExecuteOrDelayUntilScriptLoaded(loadMDN2, 'sp.ribbon.js'); }
_spBodyOnLoadFunctionNames.push('loadMDN1');
function _spNavigateHierarchyEx(nodeDiv, dataSourceId, dataPath, url, listInContext, type, additionalQString) {
    CoreInvoke('SetAdditionalNavigateHierarchyQString', additionalQString);
    g_originalSPNavigateFunc(nodeDiv, dataSourceId, dataPath, url, listInContext, type);
}

g_originalSPNavigateFunc = _spNavigateHierarchy;
_spNavigateHierarchy = _spNavigateHierarchyEx;

            ExecuteOrDelayUntilScriptLoaded(
                function() 
                {                    
                    Srch.ScriptApplicationManager.get_current().states = {"webUILanguageName":"en-US","webDefaultLanguageName":"en-US","contextUrl":"https://www.shrm.org/about-shrm","contextTitle":"About SHRM","supportedLanguages":[{"id":1025,"label":"Arabic"},{"id":1093,"label":"Bengali"},{"id":1026,"label":"Bulgarian"},{"id":1027,"label":"Catalan"},{"id":2052,"label":"Chinese (Simplified)"},{"id":1028,"label":"Chinese (Traditional)"},{"id":1050,"label":"Croatian"},{"id":1029,"label":"Czech"},{"id":1030,"label":"Danish"},{"id":1043,"label":"Dutch"},{"id":1033,"label":"English"},{"id":1035,"label":"Finnish"},{"id":1036,"label":"French"},{"id":1031,"label":"German"},{"id":1032,"label":"Greek"},{"id":1095,"label":"Gujarati"},{"id":1037,"label":"Hebrew"},{"id":1081,"label":"Hindi"},{"id":1038,"label":"Hungarian"},{"id":1039,"label":"Icelandic"},{"id":1057,"label":"Indonesian"},{"id":1040,"label":"Italian"},{"id":1041,"label":"Japanese"},{"id":1099,"label":"Kannada"},{"id":1042,"label":"Korean"},{"id":1062,"label":"Latvian"},{"id":1063,"label":"Lithuanian"},{"id":1086,"label":"Malay"},{"id":1100,"label":"Malayalam"},{"id":1102,"label":"Marathi"},{"id":1044,"label":"Norwegian"},{"id":1045,"label":"Polish"},{"id":1046,"label":"Portuguese (Brazil)"},{"id":2070,"label":"Portuguese (Portugal)"},{"id":1094,"label":"Punjabi"},{"id":1048,"label":"Romanian"},{"id":1049,"label":"Russian"},{"id":3098,"label":"Serbian (Cyrillic)"},{"id":2074,"label":"Serbian (Latin)"},{"id":1051,"label":"Slovak"},{"id":1060,"label":"Slovenian"},{"id":3082,"label":"Spanish (Spain)"},{"id":2058,"label":"Spanish (Mexico)"},{"id":1053,"label":"Swedish"},{"id":1097,"label":"Tamil"},{"id":1098,"label":"Telugu"},{"id":1054,"label":"Thai"},{"id":1055,"label":"Turkish"},{"id":1058,"label":"Ukrainian"},{"id":1056,"label":"Urdu"},{"id":1066,"label":"Vietnamese"}],"navigationNodes":[{"id":0,"name":"This Site","url":"~site/_layouts/15/osssearchresults.aspx?u={contexturl}","promptString":"Search this site"}],"showAdminDetails":false,"defaultPagesListName":"Pages","isSPFSKU":false,"userAdvancedLanguageSettingsUrl":"/about-shrm/_layouts/15/regionalsetng.aspx?type=user\u0026Source=https%3A%2F%2Fwww%2Eshrm%2Eorg%2Fabout%2Dshrm%2FPages%2FPrivacy%2DPolicy%2Easpx\u0026ShowAdvLang=1","defaultQueryProperties":{"culture":1033,"uiLanguage":1033,"summaryLength":180,"desiredSnippetLength":90,"enableStemming":true,"enablePhonetic":false,"enableNicknames":false,"trimDuplicates":true,"bypassResultTypes":false,"enableInterleaving":true,"enableQueryRules":true,"processBestBets":true,"enableOrderingHitHighlightedProperty":false,"hitHighlightedMultivaluePropertyLimit":-1,"processPersonalFavorites":true}};
                    Srch.U.trace(null, 'SerializeToClient', 'ScriptApplicationManager state initialized.');
                }, 'Search.ClientControls.js');var g_clientIdDeltaPlaceHolderMain = "DeltaPlaceHolderMain";
var g_clientIdDeltaPlaceHolderPageTitleInTitleArea = "ctl00_DeltaPlaceHolderPageTitleInTitleArea";
var g_clientIdDeltaPlaceHolderUtilityContent = "DeltaPlaceHolderUtilityContent";
//]]>
</script>
</form>
		<span id="DeltaPlaceHolderUtilityContent">
			
		</span>
		<script type="text/javascript">// <![CDATA[ 


            var g_Workspace = "s4-workspace";
		 // ]]>
</script>
		
<div id="ctl00_ctl81_PageinformationContainer" style="display: none">
    <input name="ctl00$ctl81$pageInfoJson" type="hidden" id="ctl00_ctl81_pageInfoJson" />
    <input name="ctl00$ctl81$pagePropertiesJson" type="hidden" id="ctl00_ctl81_pagePropertiesJson" />
    <style>
        .shrmpageinfo-filler {
            width: 100%;
            height: 450px;
            clear: both;
        }

        .shrmpageinfo-panel {
            position: fixed;
            bottom: 0px;
            left: 0px;
            width: 100%;
            height: 450px;
            border-top: 1px solid rgb(204, 204, 204);
            background: white;
            overflow: hidden;
            z-index: 9999998;
            font-size: 14px;
        }

            .shrmpageinfo-panel .nav {
                background-color: #f5f5f5;
                border-bottom: 1px solid #e5e5e5;
                width: 100%;
                padding-top: 5px;
                font-weight: bold;
            }

            .shrmpageinfo-panel .tab-content {
                max-height: 400px;
                overflow: auto;
                width: 100%;
            }

            .shrmpageinfo-panel .tab-pane {
                padding: 15px;
            }

            .shrmpageinfo-panel td:last-child {
                word-break: break-all;
            }

        #shrmpageinfo-resizer {
            cursor: n-resize;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHMAAABkCAMAAACCTv/3AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAA5ubmSUUG+gAAAAJ0Uk5T/wDltzBKAAAAPklEQVR42uzYQQ0AAAgDseHfNC4IyVoD912WAACUm3uampqampqamq+aAAD+IVtTU1NTU1NT0z8EAFBsBRgAX+kR+Qam138AAAAASUVORK5CYII=);
            background-size: 18px;
            background-repeat: no-repeat;
            width: 24px;
            height: 24px;
            display: inline-block;
            padding: 10px 15px;
            background-position: 0 100%;
        }

        table.shrmpageinfo-claim-type-table td {
            width: 33.333%;
            padding: 5px;
            vertical-align: top;
        }
    </style>

    <div class="shrmpageinfo-block">
        <div class="shrmpageinfo-filler" id="shrmpageinfo-filler"></div>
        <div class="shrmpageinfo-panel" id="shrmpageinfo-panel">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="shrmpageinfo-nav">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Page Information</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Page Properties</a></li>
                <li class="pull-right"><span id="shrmpageinfo-resizer"></span></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" id="shrmpageinfo-tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <table class="table" id="shrmpageinfo-table"></table>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <table class="table" id="page-properties-table"></table>
                </div>
            </div>

        </div>
    </div>

    <script>
        function shrmpageinfoUI() {

            var element = document.getElementById('shrmpageinfo-panel'),
                    filler = document.getElementById('shrmpageinfo-filler'),
                    resizer = document.getElementById('shrmpageinfo-resizer'),
                    tabContent = document.getElementById('shrmpageinfo-tab-content'),
                    nav = document.getElementById('shrmpageinfo-nav');

            resizer.addEventListener('mousedown', initResize, false);

            function initResize(e) {
                window.addEventListener('mousemove', Resize, false);
                window.addEventListener('mouseup', stopResize, false);
            }
            function Resize(e) {
                var wHeight = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight,
                        calcHeight = wHeight - e.clientY;
                element.style.height = filler.style.height = calcHeight + 'px';
                tabContent.style.maxHeight = (calcHeight - nav.offsetHeight) + 'px';

            }
            function stopResize(e) {
                window.removeEventListener('mousemove', Resize, false);
                window.removeEventListener('mouseup', stopResize, false);
            }


            function renderPageInfo() {
                var pageInfoTable = document.getElementById("shrmpageinfo-table");
								var theInfoJSON = [];
								var infoEl = document.getElementById('ctl00_ctl81_pageInfoJson');
								try{ theInfoJSON = infoEl && infoEl.value ? JSON.parse(infoEl.value) :theInfoJSON ;}finally {}
							
                var pageInfoJsonObj = theInfoJSON ;
                var pageInfoData = "";
                for (var key in pageInfoJsonObj) {
                    if (pageInfoJsonObj.hasOwnProperty(key)) {
                        var val = pageInfoJsonObj[key];
                        pageInfoData += pageInfoRenderTemplate(key, val);
                    }
                }
                pageInfoTable.innerHTML = pageInfoData;
            }

            function renderPagePropertiesInfo() {
                var pagePropInfoTable = document.getElementById("page-properties-table");
								var thePropJSON = [];
								var thePropEl = document.getElementById('ctl00_ctl81_pagePropertiesJson');
								try{ thePropJSON = thePropEl && thePropEl.value ? JSON.parse(thePropEl.value) : thePropJSON ;}finally {}
                var pagePropInfoJsonObj = thePropJSON;
                var pagePropInfoData = "";
                for (var key in pagePropInfoJsonObj) {
                    if (pagePropInfoJsonObj.hasOwnProperty(key)) {
                        var val = pagePropInfoJsonObj[key];
                        pagePropInfoData += pageInfoRenderTemplate(key, val);
                    }
                }
                pagePropInfoTable.innerHTML = pagePropInfoData;
            }

            function pageInfoRenderTemplate(key, value) {
                return "<tr>" +
                            "<td>" +
                                "<p>" + key + ": </p>" +
                            "</td>" +
                            "<td>" +
                                "<p>" + value + "</p>" +
                            "</td>" +
                        "</tr>";
            }

            renderPageInfo();
            renderPagePropertiesInfo();
        }
        shrmpageinfoUI();

    </script>
</div>

		
		<img id="ctl00_PageViewTracker_imgTemp" title="temp_image" src="../../_layouts/15/SHRM.Core/utility/pageviewtracker.aspx?user=Anonymous&amp;url=%2fabout-shrm%2fPages%2fPrivacy-Policy.aspx&amp;isMemberOnlyPage=False" alt="temp_image" style="height:1px;width:1px;" />
<script type="text/javascript">
    $(document).ready(function () {
        var src = $("#ctl00_PageViewTracker_imgTemp").attr("src").replace(/\&url=(.+)/, "&url=");
        $("a[rel|='Download']").click(function (event) {
            var href = $(this).attr("href");
            $("#ctl00_PageViewTracker_imgTemp").attr("src", src + href);
        });

        var fileExtensions = ['.doc', '.docx', '.xls', '.xlsx', '.pdf', '.xps','.ppt','pptx'];
        fileExtensions.forEach(function (extension) {

            $('a[href$="' + extension + '"]').click(function () {
                setHrefSourceforPDFandExcel(this);
                //console.log(extension);
            });
        });
    });
    function setHrefSourceforPDFandExcel(element) {
        var urlhref = $(element).attr('href');
        var urlpathwithdomain = document.createElement("a");
        urlpathwithdomain.href = urlhref;
        var src = $("#ctl00_PageViewTracker_imgTemp").attr("src");
        src = src.replace(/\&url=(.+)/, "&url=");
        $("#ctl00_PageViewTracker_imgTemp").attr("src", src + urlpathwithdomain.pathname);
    }
</script>

        
<script type="text/javascript">
    _SHRM = {
        MemberInfo: {
            IsLoggedIn: function() { return 'False'; },
            SsoUuid: function() { return ''; },
            IsMembershipActive: function() { return 'False'; },
            UserFullName: function() { return ', '; },
            UserMemberId: function(){return ''}
        }
    };
</script>


        
        
  <!--googleon: all-->      
	</body>
</html>