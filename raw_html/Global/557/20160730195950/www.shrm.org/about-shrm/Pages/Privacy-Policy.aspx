

<!DOCTYPE html>

<html xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml" dir="ltr" lang="en-US">
	<head><meta http-equiv="X-UA-Compatible" content="IE=11" /><meta name="GENERATOR" content="Microsoft SharePoint" /><meta http-equiv="Content-type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Expires" content="0" /><meta name="SHRM" content="v=1.31.4129.237;" />
<meta name="msapplication-TileImage" content="/_layouts/15/images/SharePointMetroAppTile.png" /><meta name="msapplication-TileColor" content="#0072C6" /><title>
	
    Privacy Policy

</title>
		
		<!-- IE -->
		<link rel="shortcut icon" type="image/x-icon" href="https://cdn.shrm.org/image/upload/favicon.ico" />
		<!-- other browsers -->
		<link rel="icon" type="image/x-icon" href="https://cdn.shrm.org/image/upload/favicon.ico" /><link rel="stylesheet" type="text/css" href="/_layouts/15/1033/styles/Themable/corev15.css?rev=OqAycmyMLoQIDkAlzHdMhQ%3D%3D"/>
<script type="text/javascript" src="/_layouts/15/init.js?rev=RIl4brNTuKjQszVd%2Bc1CnQ%3D%3D"></script>
<script type="text/javascript" src="/ScriptResource.axd?d=pVhMdx7BaO7vWyDvmfSFy2UmUP0AZeOYCKdSH5vzevhISxxotfv42LBXS-7tTSjJj0TffjY4wbwAF7mP-zNBnhHtYYSSc5oL7iJreKPYAc7Mf4OdX6ca9cmkWj41yNKcdJFqS-i-jnY6SuvgDiG_Qf0vE5Q0fV6cL-k8_maa5NmH4AQM-B-u-Jh0aeu7IgN60&amp;t=72e85ccd"></script>
<script type="text/javascript" src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D"></script>
<script type="text/javascript" src="/ScriptResource.axd?d=7whSi3pO0Sr1q7wtXvGRhCLH5v0Hiv7OvHHHzO4fgUhg34hBa_GU9Tw_QvR8u_PEBFsbnvVVfzUmpgjKcC8n5USDVWEqSz2MwfFIySR2RS6r-Du8U1E1UFib4NdVYImD6Bl9DRe3DJtp6Pt1rOrpLxKWrXyq6EnC1udd8iI6ppclvi73BLQs6J7hCQSkoApT0&amp;t=72e85ccd"></script>
<script type="text/javascript">RegisterSod("initstrings.js", "\u002f_layouts\u002f15\u002f1033\u002finitstrings.js?rev=2N3pk\u00252FwjnG3Tj9o5NrqHcg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("strings.js", "\u002f_layouts\u002f15\u002f1033\u002fstrings.js?rev=y6khbhv1Os2YPb5X0keqRg\u00253D\u00253D");RegisterSodDep("strings.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sp.init.js", "\u002f_layouts\u002f15\u002fsp.init.js?rev=jvJC3Kl5gbORaLtf7kxULQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=yNk\u00252FhRzgBn40LJVP\u00252BqfgdQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002f15\u002fsp.ui.dialog.js?rev=3Oh2QbaaiXSb7ldu2zd6QQ\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.init.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f15\u002fcore.js?rev=rjZicIy\u00252BXBea34HbdjHJhw\u00253D\u00253D");RegisterSodDep("core.js", "strings.js");</script>
<script type="text/javascript">RegisterSod("menu.js", "\u002f_layouts\u002f15\u002fmenu.js?rev=cXv35JACAh0ZCqUwKU592w\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mQuery.js", "\u002f_layouts\u002f15\u002fmquery.js?rev=VYAJYBo5H8I3gVSL3MzD6A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("callout.js", "\u002f_layouts\u002f15\u002fcallout.js?rev=ryx2n4ePkYj1\u00252FALmcsXZfA\u00253D\u00253D");RegisterSodDep("callout.js", "strings.js");RegisterSodDep("callout.js", "mQuery.js");RegisterSodDep("callout.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clienttemplates.js", "\u002f_layouts\u002f15\u002fclienttemplates.js?rev=wFTHeoDrS3YJ52E27prOkg\u00253D\u00253D");RegisterSodDep("clienttemplates.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sharing.js", "\u002f_layouts\u002f15\u002fsharing.js?rev=XxxHIxIIc8BsW9ikVc6dgA\u00253D\u00253D");RegisterSodDep("sharing.js", "strings.js");RegisterSodDep("sharing.js", "mQuery.js");RegisterSodDep("sharing.js", "clienttemplates.js");RegisterSodDep("sharing.js", "core.js");</script>
<script type="text/javascript">RegisterSod("suitelinks.js", "\u002f_layouts\u002f15\u002fsuitelinks.js?rev=REwVU5jSsadDdOZlCx4wpA\u00253D\u00253D");RegisterSodDep("suitelinks.js", "strings.js");RegisterSodDep("suitelinks.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clientrenderer.js", "\u002f_layouts\u002f15\u002fclientrenderer.js?rev=PWwV4FATEiOxN90BeB5Hzw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("srch.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=Srch\u00252EResources\u0026rev=XDE4tiLBtKKfBTRJIu5neg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("search.clientcontrols.js", "\u002f_layouts\u002f15\u002fsearch.clientcontrols.js?rev=UGf\u00252BO28k\u00252Bxs1phvmqw0nNQ\u00253D\u00253D");RegisterSodDep("search.clientcontrols.js", "sp.init.js");RegisterSodDep("search.clientcontrols.js", "clientrenderer.js");RegisterSodDep("search.clientcontrols.js", "srch.resources.resx");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002f15\u002fsp.runtime.js?rev=5f2WkYJoaxlIRdwUeg4WEg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.search.js", "\u002f_layouts\u002f15\u002fsp.search.js?rev=dgd0nya\u00252FYKhefSSkau\u00252FgmQ\u00253D\u00253D");RegisterSodDep("sp.search.js", "sp.init.js");RegisterSodDep("sp.search.js", "sp.runtime.js");</script>
<script type="text/javascript">RegisterSod("ajaxtoolkit.js", "\u002f_layouts\u002f15\u002fajaxtoolkit.js?rev=4rOiCbaFgJMmqw9Ojtpa6g\u00253D\u00253D");RegisterSodDep("ajaxtoolkit.js", "search.clientcontrols.js");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002f15\u002fsp.js?rev=yHHzNc0Ngyb9ksRsrUasMQ\u00253D\u00253D");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("cui.js", "\u002f_layouts\u002f15\u002fcui.js?rev=VPkVoHj7Yj40n7Zep\u00252BVfxg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002f15\u002fsp.core.js?rev=tZDGLPOvY1bRw\u00252BsgzXpxTg\u00253D\u00253D");RegisterSodDep("sp.core.js", "strings.js");RegisterSodDep("sp.core.js", "sp.init.js");RegisterSodDep("sp.core.js", "core.js");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002f15\u002finplview.js?rev=gj\u00252BiqPqpNPU131h9hI01nA\u00253D\u00253D");RegisterSodDep("inplview", "strings.js");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("ribbon", "\u002f_layouts\u002f15\u002fsp.ribbon.js?rev=1F3TSGFB5\u00252FyAaRkjYHJL5w\u00253D\u00253D");RegisterSodDep("ribbon", "strings.js");RegisterSodDep("ribbon", "core.js");RegisterSodDep("ribbon", "sp.core.js");RegisterSodDep("ribbon", "sp.js");RegisterSodDep("ribbon", "cui.js");RegisterSodDep("ribbon", "sp.res.resx");RegisterSodDep("ribbon", "sp.runtime.js");RegisterSodDep("ribbon", "inplview");</script>
<script type="text/javascript">RegisterSod("sp.ui.policy.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EUI\u00252EPolicy\u00252EResources\u0026rev=wtVfoqgvnf2cuN894ZirvA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mdn.js", "\u002f_layouts\u002f15\u002fmdn.js?rev=CZrBt1\u00252Fch9YeLFLJtB0mvg\u00253D\u00253D");RegisterSodDep("mdn.js", "sp.init.js");RegisterSodDep("mdn.js", "core.js");RegisterSodDep("mdn.js", "sp.runtime.js");RegisterSodDep("mdn.js", "sp.js");RegisterSodDep("mdn.js", "cui.js");RegisterSodDep("mdn.js", "ribbon");RegisterSodDep("mdn.js", "sp.ui.policy.resources.resx");</script>
<script type="text/javascript">RegisterSod("userprofile", "\u002f_layouts\u002f15\u002fsp.userprofiles.js?rev=p5tCOm\u00252FlHUwcfll7W3pKNw\u00253D\u00253D");RegisterSodDep("userprofile", "sp.runtime.js");</script>
<script type="text/javascript">RegisterSod("followingcommon.js", "\u002f_layouts\u002f15\u002ffollowingcommon.js?rev=jWqEDmcjCSPmnQw2ZIfItQ\u00253D\u00253D");RegisterSodDep("followingcommon.js", "strings.js");RegisterSodDep("followingcommon.js", "sp.js");RegisterSodDep("followingcommon.js", "userprofile");RegisterSodDep("followingcommon.js", "core.js");RegisterSodDep("followingcommon.js", "mQuery.js");</script>
<script type="text/javascript">RegisterSod("profilebrowserscriptres.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=ProfileBrowserScriptRes\u0026rev=J5HzNnB\u00252FO1Id\u00252FGI18rpRcw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.mysitecommon.js", "\u002f_layouts\u002f15\u002fsp.ui.mysitecommon.js?rev=Ua8qmZSU9nyf53S7PEyJwQ\u00253D\u00253D");RegisterSodDep("sp.ui.mysitecommon.js", "sp.init.js");RegisterSodDep("sp.ui.mysitecommon.js", "sp.runtime.js");RegisterSodDep("sp.ui.mysitecommon.js", "userprofile");RegisterSodDep("sp.ui.mysitecommon.js", "profilebrowserscriptres.resx");</script>
<link type="text/xml" rel="alternate" href="/about-shrm/_vti_bin/spsdisco.aspx" />
			
    <meta name='description' content='' />
<meta itemprop='name' content='Privacy Policy' />
<meta itemprop='url' content='https://www.shrm.org/about-shrm/pages/privacy-policy.aspx' />
<meta itemprop='image' content='https://www.shrm.org/publishingimages/shrm-sharing-logo.jpg' />
<meta itemprop='author' content='' />
<meta itemprop='description' content='' />
<meta itemprop='datePublished' content='1/1/0001 12:00:00 AM' />
<meta itemprop='dateModified' content='1/1/0001 12:00:00 AM' />

<!-- Twitter Card data -->
<meta name='twitter:card' content='https://www.shrm.org/publishingimages/shrm-sharing-logo.jpg'>

<meta name='twitter:title' content='Privacy Policy'>
<meta name='twitter:description' content=''>

<meta name='twitter:image:src' content='https://www.shrm.org/publishingimages/shrm-sharing-logo.jpg'>
<!-- Open Graph data -->
<meta property='og:title' content='Privacy Policy' />
<meta property='og:type' content='article' />
<meta property='og:url' content='https://www.shrm.org/about-shrm/pages/privacy-policy.aspx' />
<meta property='og:image' content='https://www.shrm.org/publishingimages/shrm-sharing-logo.jpg' />
<meta property='og:description' content='' />
<meta property='og:site_name' content='SHRM' />
<meta property='article:published_time' content='1/1/0001 5:00:00 AM' />
<meta property='article:modified_time' content='1/1/0001 5:00:00 AM' />



<meta property='fb:app_id' content='649819231827420' />














			<!-- _lcid="1033" _version="15.0.4763" _dal="1" -->
<!-- _LocalBinding -->

<link rel="canonical" href="https://www.shrm.org:443/about-shrm/Pages/Privacy-Policy.aspx" />
			
		
		<!-- Start - Your references -->
		

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous"/>

<link rel="stylesheet" type="text/css" href="/v1314129237/core/assets/dist/styles.min.css ">

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>

<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" integrity="sha256-Sk3nkD6mLTMOF0EOpNtsIry+s1CsaqQC1rVLTAy+0yc= sha512-K1qjQ+NcF2TYO/eI3M6v8EiNYZfA95pQumfvcVrTHtwQVDG+aHRqLi/ETn2uB+1JqwYqVG3LIvdm9lj6imS/pQ==" crossorigin="anonymous"></script>
<script src="https://www.microsoftTranslator.com/ajax/v3/WidgetV3.ashx?siteData=ueOIGRSKkd965FeEGM5JtQ**" type="text/javascript"></script>

<!-- if you want to add a new javascript file, add it to mintool source list and it will be included in bundle here -->
<script src="/v1314129237/core/assets/dist/scripts.min.js"></script>

<script type="text/javascript" src="/v1314129237/core/assets/js/josso-client.js"></script>

<script src="//maps.google.com/maps/api/js?key=AIzaSyDbkBtKmBsBrLXHtfbDxsavW5mgnytrPYY&libraries=places"></script>  


	<link rel="stylesheet" type="text/css" href="/v1314129237/core/assets/css/wysiwyg.css ">
    <script src="/v1314129237/core/assets/3rd/cloudinary/js/shrm-media-library.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/v1314129237/core/assets/3rd/cloudinary/styles/jquery.jcrop.css"/>
    <script type="text/javascript">
        if (typeof RTE != 'undefined') {  
            RTE.RichTextEditor.paste = function() { RTE.Cursor.paste(true); }
            RTE.Cursor.$3C_0 = true;
        }
    </script>

<!--	UBERTAGS CALL (if you're making copy of this master please make sure you include this part as well)	-->
<script type="text/javascript">
var ut_params = ut_params || [];ut_params.push("UT-536665826");//version:0.4
(function() {var ut = document.createElement('script'); ut.type = 'text/javascript'; ut.async = true;
ut.src = (("https:" == document.location.protocol) ? "https://" : "http://") + 'app.ubertags.com/javascripts/ubertags.js';
var script = document.getElementsByTagName('script')[0]; script.parentNode.insertBefore(ut, script);})();
</script>

    <script type='text/javascript'>var googletag = googletag || {};googletag.cmd = googletag.cmd || [];(function() {var gads = document.createElement('script');gads.async = true;gads.type = 'text/javascript';var useSSL = 'https:' == document.location.protocol;gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';var node = document.getElementsByTagName('script')[0];node.parentNode.insertBefore(gads, node);})();function RegisterGoogleTag(_Location) {googletag.cmd.push(function () {$("[id$='pan_AdWords']").each(function () {try {var _Size1W = Number($(this).prev().prev().prev().prev().prev().val());var _Size1H = Number($(this).prev().prev().prev().prev().val());var _Size2W = Number($(this).prev().prev().prev().val());var _Size2H = Number($(this).prev().prev().val());var _Section = $(this).prev().val();if (_Size1W != '' && _Size1H != '' && _Size2W != '' && _Size2H != '') {googletag.defineSlot('/2277/shrm.dart/' + _Section, [[_Size1W, _Size1H], [_Size2W, _Size2H]], $(this).attr('id')).addService(googletag.pubads());}else if (_Size1W != '' && _Size1H != '') {googletag.defineSlot('/2277/shrm.dart/' + _Section, [_Size1W, _Size1H], $(this).attr('id')).addService(googletag.pubads());}else if (_Size2W != '' && _Size2H != '') {googletag.defineSlot('/2277/shrm.dart/' + _Section, [_Size2W, _Size2H], $(this).attr('id')).addService(googletag.pubads());}$("<script>googletag.cmd.push(function() { googletag.display('" + $(this).attr('id') + "'); });</" + "script>").appendTo($(this));googletag.pubads().setTargeting('url', window.location.href.split('?')[0]);googletag.pubads().enableSingleRequest();googletag.enableServices();}catch (err) {}});});}$(document).ready(function () {RegisterGoogleTag('AdWordsScript');});</script>

<script type="text/javascript">
$(document).on('mouseover', '#Ribbon\\.Image\\.Image\\.Properties\\.ChangeCaptionText-Medium', function (e) {
$("#Ribbon\\.Image\\.Image\\.Properties\\.ChangeCaptionText-Medium").attr('onclick', 'CloudinaryGetCaptionValue()');
});
$(document).on('mouseover', '#Ribbon\\.EditingTools\\.CPInsert\\.Cloudinary\\.CloudinarySelect-Large', function (e) {
$("#Ribbon\\.EditingTools\\.CPInsert\\.Cloudinary\\.CloudinarySelect-Large").removeAttr('href').removeAttr('onclick').unbind().attr('onclick', 'CloudinaryOpenForm(null);');
});
$(document).ready(function () {
$("[id$='AddAssetPromptLink']").removeAttr('href').removeAttr('onclick').unbind().attr('onclick', 'CloudinaryOpenForm(this);');
$("[id$='AddAssetPromptLink']").attr('href', '#');
$("[id$='AddAssetPromptLink']").text('Select image from Cloudinary');
});
$(function() {$(document).on("medialibrary:selection", function(event, imageTag, imageObject) {console.log(imageTag);console.log(imageObject);CloudinaryFormatTag(imageTag);});});</script>
<script type="text/javascript">
$(document).ready(function () {
CloadinarySetCaption();
});
</script>

<script type="text/javascript">
    function CloudinaryOpenForm(_Obj) {
        $('#hf_Cloudinary_SelectedObject').val('');
        $('#hf_Cloudinary_SelectedCaret').val('');
        $('#hf_Cloudinary_SelectedCaretParent').val('');
        if (_Obj != null) {
            $('#hf_Cloudinary_SelectedObject').val($(_Obj).attr('id'));
            if ($(_Obj).parent().parent().parent().parent().parent().parent().parent().parent().parent().hasClass('shrm-wideimage')) { $('#cloudinary-open-wideimage').click(); }
            else { $('#cloudinary-open').click(); }
        }
        else {
            $('#hf_Cloudinary_SelectedCaretParent').val($('#ms-rterangecursor-start').closest('.ms-rtestate-field').attr('id'));
            $('#hf_Cloudinary_SelectedCaret').val($('#' + $('#hf_Cloudinary_SelectedCaretParent').val()).prop('innerHTML').indexOf('ms-rterangecursor-start') - 24);
            $('#cloudinary-open').click();
        }
    }
    function CloudinaryFormatTag(_Tag) {
        if ($('#hf_Cloudinary_SelectedObject').val() != '') {
            $('#' + $('#hf_Cloudinary_SelectedObject').val()).parent().parent().next().children().eq(0).children().eq(0).html(_Tag);
            var _aTag = $('#' + $('#hf_Cloudinary_SelectedObject').val()).parent().parent().next().children().eq(0).children().eq(0).children().eq(0);
            console.log('TransBtoA: ' + _aTag.attr('data-transbtoa'));
            $('#' + $('#hf_Cloudinary_SelectedObject').val()).parent().parent().next().children().eq(0).children().eq(0).children().eq(0).attr('src', _aTag.attr('src') + "?databtoa=" + _aTag.attr('data-transbtoa'));
            $('#' + $('#hf_Cloudinary_SelectedObject').val()).parent().parent().hide();
            $('#' + $('#hf_Cloudinary_SelectedObject').val()).parent().parent().next().show();
        }
        else if ($('#hf_Cloudinary_SelectedCaretParent').val() != '') {
            var _inner = $('#' + $('#hf_Cloudinary_SelectedCaretParent').val()).prop('innerHTML');
            var _totalLength = $('#' + $('#hf_Cloudinary_SelectedCaretParent').val()).prop('innerHTML').length;
            var _innerPart1 = _inner.substring(0, Number($('#hf_Cloudinary_SelectedCaret').val()));
            var _innerPart2 = _inner.substring(Number($('#hf_Cloudinary_SelectedCaret').val()));
            $('#' + $('#hf_Cloudinary_SelectedCaretParent').val()).html('').append(_innerPart1 + $(_Tag).prop('outerHTML') + _innerPart2);
            $('#' + $('#hf_Cloudinary_SelectedCaretParent').val()).show();
            $('#' + $('#hf_Cloudinary_SelectedCaretParent').val()).closest("[id$='RichHtmlField_EmptyHtmlPanel']").hide();
        }
    }
    function CloadinarySetCaption() {
        $(".article-content  img").each(function (i) {
            if ($(this).hasClass("16x9")) { $(this).removeClass("16x9").addClass("ratio-16x9"); }
            else if ($(this).hasClass("3x4")) { $(this).removeClass("3x4").addClass("ratio-3x4"); }
            else if ($(this).hasClass("1x1")) { $(this).removeClass("1x1").addClass("ratio-1x1"); }            
        });
        $(".article-content  img[data-caption]").each(function (i) {
            if ($(this).attr('data-caption') != null || $(this).attr('data-caption') != '' || $(this).attr('data-caption') != 'undefined') {
                $(this).wrap("<figure class='imagewithcaption content-figure'></figure>");
                $(this).parent().append("<figcaption>" + $(this).attr('data-caption') + "</figcaption>");
                $(this).parent().addClass($(this).attr('class'));
            }
        });
        if ($(".article-cover-figure img").length == 0) {
            $(".article-cover-figure").hide();
        }
        else {
            var _CoverImage = $(".article-cover-figure img");
            if ($(_CoverImage).width() > $(_CoverImage).height()) { $(".article-cover-figure").addClass("ratio-16x9"); }
            else if ($(_CoverImage).width() == $(_CoverImage).height()) { $(".article-cover-figure").addClass("ratio-1x1"); }
            else if ($(_CoverImage).width() < $(_CoverImage).height()) { $(".article-cover-figure").addClass("ratio-3x4"); }
        }
        if ($(".article-cover-figure figcaption div").eq(1).text() == "" || $(".article-cover-figure figcaption div").eq(1).html() == "") {
            $(".article-cover-figure figcaption").hide();
        }
    }
    function CloudinaryGetCaptionValue() {
        $("#Ribbon\\.Image\\.Image\\.Properties\\.ChangeCaptionText-Medium").hide();
        //if ($('#Ribbon\\.Image\\.Image\\.Properties\\.CaptionValue').length == 0) {
        $("#Ribbon\\.Image\\.Image\\.Properties-MediumMedium-1-2").append("<input type='text' id='Ribbon.Image.Image.Properties.CaptionValue' class='ms-cui-tb'><input id='Ribbon.Image.Image.Properties.CaptionBtnOk' type='button' value='Ok' onclick='CloudinarySetCaptionValue();'><input id='Ribbon.Image.Image.Properties.CaptionBtnCancel' type='button' value='Cancel' onclick='CloudinaryCancelSetCaptionValue()'>");
            $("#Ribbon\\.Image\\.Image\\.Properties-MediumMedium-0-2").append("<label class='ms-cui-ctl-small ms-cui-fslb' id='Ribbon.Image.Image.Properties.CaptionLabel-Medium'>" +
	                                                                            "<span class='ms-cui-ctl-iconContainer'>" +
		                                                                            "<span class='ms-cui-img-16by16 ms-cui-img-cont-float'>" +
			                                                                            "<img src='/_layouts/15/1033/images/formatmap16x16.png?rev=23' style='top: -115px; left: -1px;'>" +
		                                                                            "</span>" +
	                                                                            "</span>" +
	                                                                            "<span class='ms-cui-ctl-mediumlabel'>Caption:</span>" +
                                                                            "</label>");
            $("#Ribbon\\.Image\\.Image\\.Properties\\.CaptionValue").val($('#ms-rterangecursor-start').next().attr('data-caption'));
            //$("#Ribbon\\.Image\\.Image\\.Properties\\.CaptionValue").attr('onkeyup', 'CloudinarySetCaptionValue();');
        //}
    }
    function CloudinarySetCaptionValue() {
        $('#ms-rterangecursor-start').next().attr('data-caption', $("#Ribbon\\.Image\\.Image\\.Properties\\.CaptionValue").val());
        CloudinaryCancelSetCaptionValue();
    }
    function CloudinaryCancelSetCaptionValue() {
        $("#Ribbon\\.Image\\.Image\\.Properties\\.ChangeCaptionText-Medium").show();
        $("#Ribbon\\.Image\\.Image\\.Properties\\.CaptionValue").remove();
        $("#Ribbon\\.Image\\.Image\\.Properties\\.CaptionBtnOk").remove();
        $("#Ribbon\\.Image\\.Image\\.Properties\\.CaptionBtnCancel").remove();
        $("#Ribbon\\.Image\\.Image\\.Properties\\.CaptionLabel-Medium").remove();
    }
</script>

		
		
		<!-- End - your references -->
		
		<script type="text/javascript">
		    $(document).ready(function () {
		        $("html head link[rel=canonical]").first().attr("href", $("html head link[rel=canonical]").first().attr("href").toLowerCase().replace(":80", "").replace(":443", ""));
		    });
        </script>
		<!--	Disable Firefox Microsoft Office Plugin Prompt (if you're making copy of this master please make sure you include this part as well)	-->
        <script type="text/javascript">
	        function ProcessImn() { }
	        function ProcessImnMarkers() { }
        </script>
	<meta name="BreadCrumbText" content="About SHRM,Privacy Policy," />
	<meta name="BreadCrumb" content="{{Privacy Policy,/about-shrm/Pages/Privacy-Policy.aspx}{About SHRM,/about-shrm/Pages/default.aspx}}" />
	<meta name="DocumentDate" content="2016-07-09" />
	<meta name="MigratedModifiedDate" content="7/9/2016 1:19:35 PM" />
	<meta name="MetaDescription" />
	<meta name="MetaKeywords" />
	<meta name="PageGuid" content="{521B1FA0-41E8-4741-B33D-28536006BA21}" />
	<meta name="PageName" content="Privacy-Policy" />
	<meta name="SHRMMemberOnly" content="False" />
	<meta name="Abstract" content=" 
   Privacy Policy Updated December 14, 2015.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource profession. &amp;nbsp;In Part I of this Privacy Statement we set forth SHRM’s privacy policy as to data and information which we collect online&amp;nbsp; through 
   www.shrm.org or other SHRM websites which link to this Privacy Statement from their site (collectively, “SHRM Websites”).&amp;nbsp; In Part II of this Privacy Statement we set forth SHRM’s Privacy Policy as to data and information which we collect other than through SHRM Websites.&amp;nbsp; 
   Your use of the SHRM Websites and/or provision of your personally identifiable information, sensitive personal information, or demographic information to SHRM constitutes your consent to the use, storage, processing and transfer of that information in accordance with this Privacy Policy. SHRM is an organization that is based in the United States.&amp;nbsp; We comply with U.S. law" />
	<meta name="RollupImage" />
	<meta name="ArticleAuthor" />
	<meta name="ArticleIsToolContent" content="False" />
	<meta name="ArticleSocialToolsEnabled" content="False" />
	<meta name="Taxonomy" content="N/A" />
	</head>
	<body id="ctl00_mainbody" class="shrm-role-member shrm-role-member-expire" ng-app="photoAlbumApp">
		

<script>
  //window.fbAsyncInit = function() { FB.init({ appId: '649819231827420', xfbml: true, version: 'v2.5' }); };

  //(function(d, s, id){
  //   var js, fjs = d.getElementsByTagName(s)[0];
  //   if (d.getElementById(id)) {return;}
  //   js = d.createElement(s); js.id = id;
  //   js.src = "//connect.facebook.net/en_US/sdk.js";
  //   fjs.parentNode.insertBefore(js, fjs);
    //}(document, 'script', 'facebook-jssdk'));
    function shrm_encodeURI(s) { return encodeURIComponent(s); }
    function RightsLinkPopUp() {
        var url = "https://s100.copyright.com/AppDispatchServlet";
        var location = url + "?publisherName=" + shrm_encodeURI("shrm") + "&publication=" + shrm_encodeURI("Legal_Issues") + "&title=" + shrm_encodeURI("Justices Hear ERISA Reimbursement Case") + "&publicationDate=" + shrm_encodeURI("11/11/2015 12:00:00 AM") + "&contentID=" + shrm_encodeURI("6badda72-62e7-49e8-b4bd-ebbd02a72a17") + "&charCnt=" + shrm_encodeURI("7399") + "&orderBeanReset=" + shrm_encodeURI("True");
        window.open(location, "RightsLink", "location=no,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=650,height=550");
    }
</script>

  		
  		<noscript><div class='noindex'>You may be trying to access this site from a secured browser on the server. Please enable scripts and reload this page.</div></noscript>
  		
  		<form method="post" action="./Privacy-Policy.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div class="aspNetHidden">
<input type="hidden" name="_wpcmWpid" id="_wpcmWpid" value="" />
<input type="hidden" name="wpcmVal" id="wpcmVal" value="" />
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value="" />
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value="" />
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value="" />
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="0x573E2C4EC6A7268A386AC48D80A60AC8A397AB6119C3B20A53720BC28F636F84F583862F2F59FC73ACFC597370229A1F08CE8B238137E4CEE364B570804B5CC1,30 Jul 2016 20:00:12 -0000" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTkyODMyNzQ5Nw9kFgJmD2QWAgIBD2QWBAIBD2QWCgIFD2QWAmYPFgIeBFRleHQFLzxtZXRhIG5hbWU9IlNIUk0iIGNvbnRlbnQ9InY9MS4zMS40MTI5LjIzNzsiIC8+ZAIID2QWAmYPZBYCAgEPFgIeE1ByZXZpb3VzQ29udHJvbE1vZGULKYgBTWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViQ29udHJvbHMuU1BDb250cm9sTW9kZSwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTUuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwFkAhcPZBYEAgEPZBYCAgEPZBYqZg8WAh8ABSY8bWV0YSBuYW1lPSdkZXNjcmlwdGlvbicgY29udGVudD0nJyAvPmQCAg8WAh8ABTE8bWV0YSBpdGVtcHJvcD0nbmFtZScgY29udGVudD0nUHJpdmFjeSBQb2xpY3knIC8+ZAIEDxYCHwAFWzxtZXRhIGl0ZW1wcm9wPSd1cmwnIGNvbnRlbnQ9J2h0dHBzOi8vd3d3LnNocm0ub3JnL2Fib3V0LXNocm0vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweCcgLz5kAgYPFgIfAAVfPG1ldGEgaXRlbXByb3A9J2ltYWdlJyBjb250ZW50PSdodHRwczovL3d3dy5zaHJtLm9yZy9wdWJsaXNoaW5naW1hZ2VzL3Nocm0tc2hhcmluZy1sb2dvLmpwZycgLz5kAggPFgIfAAUlPG1ldGEgaXRlbXByb3A9J2F1dGhvcicgY29udGVudD0nJyAvPmQCCg8WAh8ABSo8bWV0YSBpdGVtcHJvcD0nZGVzY3JpcHRpb24nIGNvbnRlbnQ9JycgLz5kAgwPFgIfAAVAPG1ldGEgaXRlbXByb3A9J2RhdGVQdWJsaXNoZWQnIGNvbnRlbnQ9JzEvMS8wMDAxIDEyOjAwOjAwIEFNJyAvPmQCDg8WAh8ABT88bWV0YSBpdGVtcHJvcD0nZGF0ZU1vZGlmaWVkJyBjb250ZW50PScxLzEvMDAwMSAxMjowMDowMCBBTScgLz5kAhIPFgIfAAVgPG1ldGEgbmFtZT0ndHdpdHRlcjpjYXJkJyBjb250ZW50PSdodHRwczovL3d3dy5zaHJtLm9yZy9wdWJsaXNoaW5naW1hZ2VzL3Nocm0tc2hhcmluZy1sb2dvLmpwZyc+ZAIWDxYCHwAFNDxtZXRhIG5hbWU9J3R3aXR0ZXI6dGl0bGUnIGNvbnRlbnQ9J1ByaXZhY3kgUG9saWN5Jz5kAhgPFgIfAAUsPG1ldGEgbmFtZT0ndHdpdHRlcjpkZXNjcmlwdGlvbicgY29udGVudD0nJz5kAhwPFgIfAAVlPG1ldGEgbmFtZT0ndHdpdHRlcjppbWFnZTpzcmMnIGNvbnRlbnQ9J2h0dHBzOi8vd3d3LnNocm0ub3JnL3B1Ymxpc2hpbmdpbWFnZXMvc2hybS1zaGFyaW5nLWxvZ28uanBnJz5kAh4PFgIfAAU1PG1ldGEgcHJvcGVydHk9J29nOnRpdGxlJyBjb250ZW50PSdQcml2YWN5IFBvbGljeScgLz5kAiAPFgIfAAUtPG1ldGEgcHJvcGVydHk9J29nOnR5cGUnIGNvbnRlbnQ9J2FydGljbGUnIC8+ZAIiDxYCHwAFXjxtZXRhIHByb3BlcnR5PSdvZzp1cmwnIGNvbnRlbnQ9J2h0dHBzOi8vd3d3LnNocm0ub3JnL2Fib3V0LXNocm0vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweCcgLz5kAiQPFgIfAAViPG1ldGEgcHJvcGVydHk9J29nOmltYWdlJyBjb250ZW50PSdodHRwczovL3d3dy5zaHJtLm9yZy9wdWJsaXNoaW5naW1hZ2VzL3Nocm0tc2hhcmluZy1sb2dvLmpwZycgLz5kAiYPFgIfAAUtPG1ldGEgcHJvcGVydHk9J29nOmRlc2NyaXB0aW9uJyBjb250ZW50PScnIC8+ZAIoDxYCHwAFLzxtZXRhIHByb3BlcnR5PSdvZzpzaXRlX25hbWUnIGNvbnRlbnQ9J1NIUk0nIC8+ZAIqDxYCHwAFSDxtZXRhIHByb3BlcnR5PSdhcnRpY2xlOnB1Ymxpc2hlZF90aW1lJyBjb250ZW50PScxLzEvMDAwMSA1OjAwOjAwIEFNJyAvPmQCLA8WAh8ABUc8bWV0YSBwcm9wZXJ0eT0nYXJ0aWNsZTptb2RpZmllZF90aW1lJyBjb250ZW50PScxLzEvMDAwMSA1OjAwOjAwIEFNJyAvPmQCMg8WAh8ABTc8bWV0YSBwcm9wZXJ0eT0nZmI6YXBwX2lkJyBjb250ZW50PSc2NDk4MTkyMzE4Mjc0MjAnIC8+ZAIDD2QWAgIBD2QWAmYPPCsABgBkAhsPZBYCZg9kFgICAQ8WAh8ABeINPHNjcmlwdCB0eXBlPSd0ZXh0L2phdmFzY3JpcHQnPnZhciBnb29nbGV0YWcgPSBnb29nbGV0YWcgfHwge307Z29vZ2xldGFnLmNtZCA9IGdvb2dsZXRhZy5jbWQgfHwgW107KGZ1bmN0aW9uKCkge3ZhciBnYWRzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7Z2Fkcy5hc3luYyA9IHRydWU7Z2Fkcy50eXBlID0gJ3RleHQvamF2YXNjcmlwdCc7dmFyIHVzZVNTTCA9ICdodHRwczonID09IGRvY3VtZW50LmxvY2F0aW9uLnByb3RvY29sO2dhZHMuc3JjID0gKHVzZVNTTCA/ICdodHRwczonIDogJ2h0dHA6JykgKyAnLy93d3cuZ29vZ2xldGFnc2VydmljZXMuY29tL3RhZy9qcy9ncHQuanMnO3ZhciBub2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ3NjcmlwdCcpWzBdO25vZGUucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoZ2Fkcywgbm9kZSk7fSkoKTtmdW5jdGlvbiBSZWdpc3Rlckdvb2dsZVRhZyhfTG9jYXRpb24pIHtnb29nbGV0YWcuY21kLnB1c2goZnVuY3Rpb24gKCkgeyQoIltpZCQ9J3Bhbl9BZFdvcmRzJ10iKS5lYWNoKGZ1bmN0aW9uICgpIHt0cnkge3ZhciBfU2l6ZTFXID0gTnVtYmVyKCQodGhpcykucHJldigpLnByZXYoKS5wcmV2KCkucHJldigpLnByZXYoKS52YWwoKSk7dmFyIF9TaXplMUggPSBOdW1iZXIoJCh0aGlzKS5wcmV2KCkucHJldigpLnByZXYoKS5wcmV2KCkudmFsKCkpO3ZhciBfU2l6ZTJXID0gTnVtYmVyKCQodGhpcykucHJldigpLnByZXYoKS5wcmV2KCkudmFsKCkpO3ZhciBfU2l6ZTJIID0gTnVtYmVyKCQodGhpcykucHJldigpLnByZXYoKS52YWwoKSk7dmFyIF9TZWN0aW9uID0gJCh0aGlzKS5wcmV2KCkudmFsKCk7aWYgKF9TaXplMVcgIT0gJycgJiYgX1NpemUxSCAhPSAnJyAmJiBfU2l6ZTJXICE9ICcnICYmIF9TaXplMkggIT0gJycpIHtnb29nbGV0YWcuZGVmaW5lU2xvdCgnLzIyNzcvc2hybS5kYXJ0LycgKyBfU2VjdGlvbiwgW1tfU2l6ZTFXLCBfU2l6ZTFIXSwgW19TaXplMlcsIF9TaXplMkhdXSwgJCh0aGlzKS5hdHRyKCdpZCcpKS5hZGRTZXJ2aWNlKGdvb2dsZXRhZy5wdWJhZHMoKSk7fWVsc2UgaWYgKF9TaXplMVcgIT0gJycgJiYgX1NpemUxSCAhPSAnJykge2dvb2dsZXRhZy5kZWZpbmVTbG90KCcvMjI3Ny9zaHJtLmRhcnQvJyArIF9TZWN0aW9uLCBbX1NpemUxVywgX1NpemUxSF0sICQodGhpcykuYXR0cignaWQnKSkuYWRkU2VydmljZShnb29nbGV0YWcucHViYWRzKCkpO31lbHNlIGlmIChfU2l6ZTJXICE9ICcnICYmIF9TaXplMkggIT0gJycpIHtnb29nbGV0YWcuZGVmaW5lU2xvdCgnLzIyNzcvc2hybS5kYXJ0LycgKyBfU2VjdGlvbiwgW19TaXplMlcsIF9TaXplMkhdLCAkKHRoaXMpLmF0dHIoJ2lkJykpLmFkZFNlcnZpY2UoZ29vZ2xldGFnLnB1YmFkcygpKTt9JCgiPHNjcmlwdD5nb29nbGV0YWcuY21kLnB1c2goZnVuY3Rpb24oKSB7IGdvb2dsZXRhZy5kaXNwbGF5KCciICsgJCh0aGlzKS5hdHRyKCdpZCcpICsgIicpOyB9KTs8LyIgKyAic2NyaXB0PiIpLmFwcGVuZFRvKCQodGhpcykpO2dvb2dsZXRhZy5wdWJhZHMoKS5zZXRUYXJnZXRpbmcoJ3VybCcsIHdpbmRvdy5sb2NhdGlvbi5ocmVmLnNwbGl0KCc/JylbMF0pO2dvb2dsZXRhZy5wdWJhZHMoKS5lbmFibGVTaW5nbGVSZXF1ZXN0KCk7Z29vZ2xldGFnLmVuYWJsZVNlcnZpY2VzKCk7fWNhdGNoIChlcnIpIHt9fSk7fSk7fSQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtSZWdpc3Rlckdvb2dsZVRhZygnQWRXb3Jkc1NjcmlwdCcpO30pOzwvc2NyaXB0PmQCHA9kFgRmDxYCHwAFhgg8c2NyaXB0IHR5cGU9InRleHQvamF2YXNjcmlwdCI+CiQoZG9jdW1lbnQpLm9uKCdtb3VzZW92ZXInLCAnI1JpYmJvblxcLkltYWdlXFwuSW1hZ2VcXC5Qcm9wZXJ0aWVzXFwuQ2hhbmdlQ2FwdGlvblRleHQtTWVkaXVtJywgZnVuY3Rpb24gKGUpIHsKJCgiI1JpYmJvblxcLkltYWdlXFwuSW1hZ2VcXC5Qcm9wZXJ0aWVzXFwuQ2hhbmdlQ2FwdGlvblRleHQtTWVkaXVtIikuYXR0cignb25jbGljaycsICdDbG91ZGluYXJ5R2V0Q2FwdGlvblZhbHVlKCknKTsKfSk7CiQoZG9jdW1lbnQpLm9uKCdtb3VzZW92ZXInLCAnI1JpYmJvblxcLkVkaXRpbmdUb29sc1xcLkNQSW5zZXJ0XFwuQ2xvdWRpbmFyeVxcLkNsb3VkaW5hcnlTZWxlY3QtTGFyZ2UnLCBmdW5jdGlvbiAoZSkgewokKCIjUmliYm9uXFwuRWRpdGluZ1Rvb2xzXFwuQ1BJbnNlcnRcXC5DbG91ZGluYXJ5XFwuQ2xvdWRpbmFyeVNlbGVjdC1MYXJnZSIpLnJlbW92ZUF0dHIoJ2hyZWYnKS5yZW1vdmVBdHRyKCdvbmNsaWNrJykudW5iaW5kKCkuYXR0cignb25jbGljaycsICdDbG91ZGluYXJ5T3BlbkZvcm0obnVsbCk7Jyk7Cn0pOwokKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7CiQoIltpZCQ9J0FkZEFzc2V0UHJvbXB0TGluayddIikucmVtb3ZlQXR0cignaHJlZicpLnJlbW92ZUF0dHIoJ29uY2xpY2snKS51bmJpbmQoKS5hdHRyKCdvbmNsaWNrJywgJ0Nsb3VkaW5hcnlPcGVuRm9ybSh0aGlzKTsnKTsKJCgiW2lkJD0nQWRkQXNzZXRQcm9tcHRMaW5rJ10iKS5hdHRyKCdocmVmJywgJyMnKTsKJCgiW2lkJD0nQWRkQXNzZXRQcm9tcHRMaW5rJ10iKS50ZXh0KCdTZWxlY3QgaW1hZ2UgZnJvbSBDbG91ZGluYXJ5Jyk7Cn0pOwokKGZ1bmN0aW9uKCkgeyQoZG9jdW1lbnQpLm9uKCJtZWRpYWxpYnJhcnk6c2VsZWN0aW9uIiwgZnVuY3Rpb24oZXZlbnQsIGltYWdlVGFnLCBpbWFnZU9iamVjdCkge2NvbnNvbGUubG9nKGltYWdlVGFnKTtjb25zb2xlLmxvZyhpbWFnZU9iamVjdCk7Q2xvdWRpbmFyeUZvcm1hdFRhZyhpbWFnZVRhZyk7fSk7fSk7PC9zY3JpcHQ+ZAICDxYCHwAFZjxzY3JpcHQgdHlwZT0idGV4dC9qYXZhc2NyaXB0Ij4KJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkgewpDbG9hZGluYXJ5U2V0Q2FwdGlvbigpOwp9KTsKPC9zY3JpcHQ+CmQCAw9kFgQCCQ9kFggCAQ9kFgICFw9kFgICAw8WAh4HVmlzaWJsZWgWAmYPZBYEAgIPZBYGAgEPFgIfAmhkAgMPFgIfAmhkAgUPFgIfAmhkAgMPDxYCHglBY2Nlc3NLZXkFAS9kZAIHD2QWCgIED2QWAmYPDxYCHwJoZBYEAgEPDxYCHwJoZBYCZg8WAh8ABRtZb3VyIG1lbWJlcnNoaXAgaGFzIGV4cGlyZWRkAgMPZBYCAgIPDxYCHwJoZBYCZg8WAh8ABRtZb3VyIG1lbWJlcnNoaXAgaGFzIGV4cGlyZWRkAgYPZBYCAg4PZBYGZg8PFgIfAmhkZAICDw8WBB4LTmF2aWdhdGVVcmwFIS9hYm91dC1zaHJtL3BhZ2VzL21lbWJlcnNoaXAuYXNweB8ABQpNZW1iZXJzaGlwZGQCAw8PFgQfBAUhL2NlcnRpZmljYXRpb24vcGFnZXMvZGVmYXVsdC5hc3B4HwAFDUdFVCBDRVJUSUZJRURkZAIID2QWCAIBDxYCHgtfIUl0ZW1Db3VudAIFFgpmD2QWAgIBDxYCHgVjbGFzcwUHZHJvcG91dBYEAgEPFgIfAAW6ATxhIGRpc2FibGVkPScnIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3Bhbl9Ecm9wb3V0TWVudScgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkhSIFRvZGF5PC9hPmQCAw9kFgYCAQ8WAh8ABa4BPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDBfcGFuX0Ryb3BvdXRNZW51JyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDBfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+SFIgVG9kYXk8L2E+ZAIDDxYCHwUCAxYGZg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABfwBPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPk5ld3M8L2E+ZAIFD2QWBAIBDxYCHwAF1gE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPk5ld3M8L2E+ZAIDDxYCHwUCAxYGZg9kFgICAQ9kFgJmDw8WBB8EBSkvaHItdG9kYXkvbmV3cy9oci1uZXdzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQdIUiBOZXdzZGQCAQ9kFgICAQ9kFgJmDw8WBB8EBTIvaHItdG9kYXkvbmV3cy9oci1tYWdhemluZS8wNzE2L3BhZ2VzL2RlZmF1bHQuYXNweB8ABQtIUiBNYWdhemluZWRkAgIPZBYCAgEPZBYCZg8PFgQfBAUVaHR0cDovL2Jsb2cuc2hybS5vcmcvHwAFCVNIUk0gQmxvZ2RkAgcPFgIfAAUGPC9kaXY+ZAIBD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFhQI8YSBkaXNhYmxlZD0nJyBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+UHVibGljIFBvbGljeTwvYT5kAgUPZBYEAgEPFgIfAAXfATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+UHVibGljIFBvbGljeTwvYT5kAgMPFgIfBQIEFghmD2QWAgIBD2QWAmYPDxYEHwQFIWh0dHA6Ly93d3cuYWR2b2NhY3kuc2hybS5vcmcvaG9tZR8ABQtUYWtlIEFjdGlvbmRkAgEPZBYCAgEPZBYCZg8PFgQfBAVCL2hyLXRvZGF5L3B1YmxpYy1wb2xpY3kvaHItcHVibGljLXBvbGljeS1pc3N1ZXMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFF0hSIFB1YmxpYyBQb2xpY3kgSXNzdWVzZGQCAg9kFgICAQ9kFgJmDw8WBB8EBSJodHRwOi8vd3d3LmFkdm9jYWN5LnNocm0ub3JnL2Fib3V0HwAFF0EtVGVhbSBBZHZvY2FjeSBOZXR3b3JrZGQCAw9kFgICAQ9kFgJmDw8WBB8EBTgvaHItdG9kYXkvcHVibGljLXBvbGljeS9zdGF0ZS1hZmZhaXJzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQ1TdGF0ZSBBZmZhaXJzZGQCBw8WAh8ABQY8L2Rpdj5kAgIPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAWMAjxhIGRpc2FibGVkPScnIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5UcmVuZHMgJiBGb3JlY2FzdGluZzwvYT5kAgUPZBYEAgEPFgIfAAXmATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+VHJlbmRzICYgRm9yZWNhc3Rpbmc8L2E+ZAIDDxYCHwUCAxYGZg9kFgICAQ9kFgJmDw8WBB8EBUgvaHItdG9kYXkvdHJlbmRzLWFuZC1mb3JlY2FzdGluZy9yZXNlYXJjaC1hbmQtc3VydmV5cy9wYWdlcy9kZWZhdWx0LmFzcHgfAAUSUmVzZWFyY2ggJiBTdXJ2ZXlzZGQCAQ9kFgICAQ9kFgJmDw8WBB8EBVIvaHItdG9kYXkvdHJlbmRzLWFuZC1mb3JlY2FzdGluZy9sYWJvci1tYXJrZXQtYW5kLWVjb25vbWljLWRhdGEvcGFnZXMvZGVmYXVsdC5hc3B4HwAFHExhYm9yIE1hcmtldCAmIEVjb25vbWljIERhdGFkZAICD2QWAgIBD2QWAmYPDxYEHwQFVC9oci10b2RheS90cmVuZHMtYW5kLWZvcmVjYXN0aW5nL3NwZWNpYWwtcmVwb3J0cy1hbmQtZXhwZXJ0LXZpZXdzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABR5TcGVjaWFsIFJlcG9ydHMgJiBFeHBlcnQgVmlld3NkZAIHDxYCHwAFBjwvZGl2PmQCBQ9kFgJmDw8WAh8CZ2QWCgIBDxYCHwAFHEhSIE1hZ2F6aW5lOiBKdWx5L0F1Z3VzdCAnMTZkAgMPDxYCHwQFRmh0dHBzOi8vd3d3LnNocm0ub3JnL2hyLXRvZGF5L25ld3MvaHItbWFnYXppbmUvMDcxNi9wYWdlcy9kZWZhdWx0LmFzcHhkFgJmDw8WAh4ISW1hZ2VVcmwFkQJodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF8zMzglMmN3XzYwMCUyY3hfMCUyY3lfNjgvY19maXQlMmN3Xzc2Ny92MS9NYWdhemluZS8yMDE2X0p1bHlBdWdfY2JkcWoyP2RhdGFidG9hPWV5SXhlREVpT25zaWVDSTZNQ3dpZVNJNk1Dd2llRElpT2pZd01Dd2llVElpT2pZd01Dd2lkeUk2TmpBd0xDSm9Jam8yTURCOUxDSXhObmc1SWpwN0luZ2lPakFzSW5raU9qWTRMQ0o0TWlJNk5qQXdMQ0o1TWlJNk5EQTJMQ0ozSWpvMk1EQXNJbWdpT2pNek9IMTlkZAIFDw8WAh8EBUZodHRwczovL3d3dy5zaHJtLm9yZy9oci10b2RheS9uZXdzL2hyLW1hZ2F6aW5lLzA3MTYvcGFnZXMvZGVmYXVsdC5hc3B4ZBYCZg8WAh8ABSFIb3cgdG8gUHJlcCBmb3IgdGhlIE92ZXJ0aW1lIFJ1bGVkAgcPFgIfAAUvTm8gSFIgcHJvZmVzc2lvbmFsIGlzIGV4ZW1wdCBmcm9tIHRoZSBwbGFubmluZy5kAgkPDxYEHwQFRmh0dHBzOi8vd3d3LnNocm0ub3JnL2hyLXRvZGF5L25ld3MvaHItbWFnYXppbmUvMDcxNi9wYWdlcy9kZWZhdWx0LmFzcHgfAAUJUmVhZCBtb3JlZGQCAQ9kFgICAQ8WAh8GBQdkcm9wb3V0FgQCAQ8WAh8ABcMBPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDFfcGFuX0Ryb3BvdXRNZW51JyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDFfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+UmVzb3VyY2VzICYgVG9vbHM8L2E+ZAIDD2QWBgIBDxYCHwAFtwE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMV9wYW5fRHJvcG91dE1lbnUnIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMV9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5SZXNvdXJjZXMgJiBUb29sczwvYT5kAgMPFgIfBQIEFghmD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFgQI8YSBkaXNhYmxlZD0nJyBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+SFIgVG9waWNzPC9hPmQCBQ9kFgQCAQ8WAh8ABdsBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5IUiBUb3BpY3M8L2E+ZAIDDxYCHwUCCxYWZg9kFgICAQ9kFgJmDw8WBB8EBUcvcmVzb3VyY2VzYW5kdG9vbHMvaHItdG9waWNzL2JlaGF2aW9yYWwtY29tcGV0ZW5jaWVzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRdCZWhhdmlvcmFsIENvbXBldGVuY2llc2RkAgEPZBYCAgEPZBYCZg8PFgQfBAU4L3Jlc291cmNlc2FuZHRvb2xzL2hyLXRvcGljcy9iZW5lZml0cy9wYWdlcy9kZWZhdWx0LmFzcHgfAAUIQmVuZWZpdHNkZAICD2QWAgIBD2QWAmYPDxYEHwQFPC9yZXNvdXJjZXNhbmR0b29scy9oci10b3BpY3MvcGFnZXMvY2FsaWZvcm5pYS1yZXNvdXJjZXMuYXNweB8ABRRDYWxpZm9ybmlhIFJlc291cmNlc2RkAgMPZBYCAgEPZBYCZg8PFgQfBAU8L3Jlc291cmNlc2FuZHRvb2xzL2hyLXRvcGljcy9jb21wZW5zYXRpb24vcGFnZXMvZGVmYXVsdC5hc3B4HwAFDENvbXBlbnNhdGlvbmRkAgQPZBYCAgEPZBYCZg8PFgQfBAVCL3Jlc291cmNlc2FuZHRvb2xzL2hyLXRvcGljcy9lbXBsb3llZS1yZWxhdGlvbnMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFEkVtcGxveWVlIFJlbGF0aW9uc2RkAgUPZBYCAgEPZBYCZg8PFgQfBAU5L3Jlc291cmNlc2FuZHRvb2xzL2hyLXRvcGljcy9nbG9iYWwtaHIvcGFnZXMvZGVmYXVsdC5hc3B4HwAFCUdsb2JhbCBIUmRkAgYPZBYCAgEPZBYCZg8PFgQfBAU/L3Jlc291cmNlc2FuZHRvb2xzL2hyLXRvcGljcy9sYWJvci1yZWxhdGlvbnMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFD0xhYm9yIFJlbGF0aW9uc2RkAgcPZBYCAgEPZBYCZg8PFgQfBAVXL3Jlc291cmNlc2FuZHRvb2xzL2hyLXRvcGljcy9vcmdhbml6YXRpb25hbC1hbmQtZW1wbG95ZWUtZGV2ZWxvcG1lbnQvcGFnZXMvZGVmYXVsdC5hc3B4HwAFJU9yZ2FuaXphdGlvbmFsICYgRW1wbG95ZWUgRGV2ZWxvcG1lbnRkZAIID2QWAgIBD2QWAmYPDxYEHwQFPy9yZXNvdXJjZXNhbmR0b29scy9oci10b3BpY3Mvcmlzay1tYW5hZ2VtZW50L3BhZ2VzL2RlZmF1bHQuYXNweB8ABQ9SaXNrIE1hbmFnZW1lbnRkZAIJD2QWAgIBD2QWAmYPDxYEHwQFQi9yZXNvdXJjZXNhbmR0b29scy9oci10b3BpY3MvdGFsZW50LWFjcXVpc2l0aW9uL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRJUYWxlbnQgQWNxdWlzaXRpb25kZAIKD2QWAgIBD2QWAmYPDxYEHwQFOi9yZXNvdXJjZXNhbmR0b29scy9oci10b3BpY3MvdGVjaG5vbG9neS9wYWdlcy9kZWZhdWx0LmFzcHgfAAUKVGVjaG5vbG9neWRkAgcPFgIfAAUGPC9kaXY+ZAIBD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFigI8YSBkaXNhYmxlZD0nJyBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+TGVnYWwgJiBDb21wbGlhbmNlPC9hPmQCBQ9kFgQCAQ8WAh8ABeQBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5MZWdhbCAmIENvbXBsaWFuY2U8L2E+ZAIDDxYCHwUCAxYGZg9kFgICAQ9kFgJmDw8WBB8EBUkvcmVzb3VyY2VzYW5kdG9vbHMvbGVnYWwtYW5kLWNvbXBsaWFuY2UvZW1wbG95bWVudC1sYXcvcGFnZXMvZGVmYXVsdC5hc3B4HwAFDkVtcGxveW1lbnQgTGF3ZGQCAQ9kFgICAQ9kFgJmDw8WBB8EBVIvcmVzb3VyY2VzYW5kdG9vbHMvbGVnYWwtYW5kLWNvbXBsaWFuY2Uvc3RhdGUtYW5kLWxvY2FsLXVwZGF0ZXMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFFVN0YXRlICYgTG9jYWwgVXBkYXRlc2RkAgIPZBYCAgEPZBYCZg8PFgQfBAUQaHR0cDovL2NmZ2kub3JnLx8ABRxJbW1pZ3JhdGlvbiBFbXBsb3llciBOZXR3b3JrZGQCBw8WAh8AZWQCAg9kFggCAQ8WAh8AZWQCAw8WAh8ABYoCPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkJ1c2luZXNzIFNvbHV0aW9uczwvYT5kAgUPZBYEAgEPFgIfAAXkATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+QnVzaW5lc3MgU29sdXRpb25zPC9hPmQCAw8WAh8FAggWEGYPZBYCAgEPZBYCZg8PFgQfBAVFL3Jlc291cmNlc2FuZHRvb2xzL2J1c2luZXNzLXNvbHV0aW9ucy9wYWdlcy9iZW5jaG1hcmtpbmctc2VydmljZS5hc3B4HwAFFEJlbmNobWFya2luZyBTZXJ2aWNlZGQCAQ9kFgICAQ9kFgJmDw8WBB8EBUsvcmVzb3VyY2VzYW5kdG9vbHMvYnVzaW5lc3Mtc29sdXRpb25zL3BhZ2VzL2RpdmVyc2l0eS1oaXJpbmctc29sdXRpb25zLmFzcHgfAAUaRGl2ZXJzaXR5IEhpcmluZyBTb2x1dGlvbnNkZAICD2QWAgIBD2QWAmYPDxYEHwQFUy9yZXNvdXJjZXNhbmR0b29scy9idXNpbmVzcy1zb2x1dGlvbnMvcGFnZXMvZW1wbG95ZWUtZW5nYWdlbWVudC1zdXJ2ZXktc2VydmljZS5hc3B4HwAFGkVtcGxveWVlIEVuZ2FnZW1lbnQgU3VydmV5ZGQCAw9kFgICAQ9kFgJmDw8WBB8EBT4vbGVhcm5pbmdhbmRjYXJlZXIvbGVhcm5pbmcvb25zaXRlLXRyYWluaW5nL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQ9PbnNpdGUgVHJhaW5pbmdkZAIED2QWAgIBD2QWAmYPDxYEHwQFRC9yZXNvdXJjZXNhbmR0b29scy9idXNpbmVzcy1zb2x1dGlvbnMvcGFnZXMvc2FsYXJ5LWRhdGEtc2VydmljZS5hc3B4HwAFE1NhbGFyeSBEYXRhIFNlcnZpY2VkZAIFD2QWAgIBD2QWAmYPDxYEHwQFSS9yZXNvdXJjZXNhbmR0b29scy9idXNpbmVzcy1zb2x1dGlvbnMvcGFnZXMvc3VydmV5LXJlc2VhcmNoLXNlcnZpY2VzLmFzcHgfAAUYU3VydmV5IFJlc2VhcmNoIFNlcnZpY2VzZGQCBg9kFgICAQ9kFgJmDw8WBB8EBRVodHRwczovL3RhYy5zaHJtLm9yZy8fAAUYVGFsZW50IEFzc2Vzc21lbnQgQ2VudGVyZGQCBw9kFgICAQ9kFgJmDw8WBB8EBSBodHRwOi8vdmVuZG9yZGlyZWN0b3J5LnNocm0ub3JnLx8ABRBWZW5kb3IgRGlyZWN0b3J5ZGQCBw8WAh8ABQY8L2Rpdj5kAgMPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAWHAjxhIGRpc2FibGVkPScnIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5Ub29scyAmIFNhbXBsZXM8L2E+ZAIFD2QWBAIBDxYCHwAF4QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlRvb2xzICYgU2FtcGxlczwvYT5kAgMPFgIfBQILFhZmD2QWAgIBD2QWAmYPDxYEHwQFQi9yZXNvdXJjZXNhbmR0b29scy90b29scy1hbmQtc2FtcGxlcy9wYWdlcy9lbXBsb3llZS1oYW5kYm9va3MuYXNweB8ABRJFbXBsb3llZSBIYW5kYm9va3NkZAIBD2QWAgIBD2QWAmYPDxYEHwQFQC9yZXNvdXJjZXNhbmR0b29scy90b29scy1hbmQtc2FtcGxlcy9wYWdlcy9leHByZXNzLXJlcXVlc3RzLmFzcHgfAAUQRXhwcmVzcyBSZXF1ZXN0c2RkAgIPZBYCAgEPZBYCZg8PFgQfBAVFL3Jlc291cmNlc2FuZHRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL2hvdy10by1ndWlkZXMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFDUhvdy1UbyBHdWlkZXNkZAIDD2QWAgIBD2QWAmYPDxYEHwQFQC9yZXNvdXJjZXNhbmR0b29scy90b29scy1hbmQtc2FtcGxlcy9oci1mb3Jtcy9wYWdlcy9kZWZhdWx0LmFzcHgfAAUISFIgRm9ybXNkZAIED2QWAgIBD2QWAmYPDxYEHwQFPS9yZXNvdXJjZXNhbmR0b29scy90b29scy1hbmQtc2FtcGxlcy9oci1xYS9wYWdlcy9kZWZhdWx0LmFzcHgfAAUHSFIgUSZBc2RkAgUPZBYCAgEPZBYCZg8PFgQfBAVLL3Jlc291cmNlc2FuZHRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL2ludGVydmlldy1xdWVzdGlvbnMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFE0ludGVydmlldyBRdWVzdGlvbnNkZAIGD2QWAgIBD2QWAmYPDxYEHwQFSC9yZXNvdXJjZXNhbmR0b29scy90b29scy1hbmQtc2FtcGxlcy9qb2ItZGVzY3JpcHRpb25zL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRBKb2IgRGVzY3JpcHRpb25zZGQCBw9kFgICAQ9kFgJmDw8WBB8EBUAvcmVzb3VyY2VzYW5kdG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvcG9saWNpZXMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFCFBvbGljaWVzZGQCCA9kFgICAQ9kFgJmDw8WBB8EBUUvcmVzb3VyY2VzYW5kdG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvcHJlc2VudGF0aW9ucy9wYWdlcy9kZWZhdWx0LmFzcHgfAAUNUHJlc2VudGF0aW9uc2RkAgkPZBYCAgEPZBYCZg8PFgQfBAU8L3Jlc291cmNlc2FuZHRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL3BhZ2VzL3NwcmVhZHNoZWV0cy5hc3B4HwAFDFNwcmVhZHNoZWV0c2RkAgoPZBYCAgEPZBYCZg8PFgQfBAVAL3Jlc291cmNlc2FuZHRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL3Rvb2xraXRzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQhUb29sa2l0c2RkAgcPFgIfAAUGPC9kaXY+ZAIFD2QWAmYPDxYCHwJnZBYKAgEPFgIfAAURRW1wbG95ZWUgSGFuZGJvb2tkAgMPDxYCHwQFLWh0dHBzOi8vc3RvcmUuc2hybS5vcmcvZW1wbG95ZWUtaGFuZGJvb2suaHRtbGQWAmYPDxYCHwcF1wFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF80OTQlMmN3Xzg4MCUyY3hfMjI5JTJjeV8wL2NfZml0JTJjd183NjcvdjEvTWFya2V0aW5nL0VCSF9TdG9yZU1hcnF1ZWVfMl93cmk2Ynk/ZGF0YWJ0b2E9ZXlJeE5uZzVJanA3SW5naU9qSXlPU3dpZVNJNk1Dd2llRElpT2pFeE1Ea3NJbmt5SWpvME9UUXNJbmNpT2pnNE1Dd2lhQ0k2TkRrMGZYMCUzZGRkAgUPDxYCHwQFLWh0dHBzOi8vc3RvcmUuc2hybS5vcmcvZW1wbG95ZWUtaGFuZGJvb2suaHRtbGQWAmYPFgIfAAUmSXMgWW91ciBFbXBsb3llZSBIYW5kYm9vayBLZWVwaW5nIFVwPyBkAgcPFgIfAAVDVGFrZSB0aGUgd29yayBvdXQgb2YgY3JlYXRpbmcgYW5kIG1haW50YWluaW5nIGFuIGVtcGxveWVlIGhhbmRib29rLmQCCQ8PFgQfBAUtaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9lbXBsb3llZS1oYW5kYm9vay5odG1sHwAFD0N1c3RvbWl6ZSBZb3Vyc2RkAgIPZBYCAgEPFgIfBgUHZHJvcG91dBYEAgEPFgIfAAXDATxhIGRpc2FibGVkPScnIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3Bhbl9Ecm9wb3V0TWVudScgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkxlYXJuaW5nICYgQ2FyZWVyPC9hPmQCAw9kFgYCAQ8WAh8ABbcBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcGFuX0Ryb3BvdXRNZW51JyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+TGVhcm5pbmcgJiBDYXJlZXI8L2E+ZAIDDxYCHwUCBRYKZg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYACPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkxlYXJuaW5nPC9hPmQCBQ9kFgQCAQ8WAh8ABdoBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5MZWFybmluZzwvYT5kAgMPFgIfBQIFFgpmD2QWAgIBD2QWAmYPDxYEHwQFLy9sZWFybmluZ2FuZGNhcmVlci9sZWFybmluZy9wYWdlcy9zZW1pbmFycy5hc3B4HwAFCFNlbWluYXJzZGQCAQ9kFgICAQ9kFgJmDw8WBB8EBT4vbGVhcm5pbmdhbmRjYXJlZXIvbGVhcm5pbmcvb25zaXRlLXRyYWluaW5nL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQ9PbnNpdGUgVHJhaW5pbmdkZAICD2QWAgIBD2QWAmYPDxYEHwQFL2h0dHBzOi8vc3RvcmUuc2hybS5vcmcvZWR1Y2F0aW9uL2VsZWFybmluZy5odG1sHwAFCWVMZWFybmluZ2RkAgMPZBYCAgEPZBYCZg8PFgQfBAVCL2xlYXJuaW5nYW5kY2FyZWVyL2xlYXJuaW5nL3BhZ2VzL2Vzc2VudGlhbHMtb2YtaHItbWFuYWdlbWVudC5hc3B4HwAFG0Vzc2VudGlhbHMgb2YgSFIgTWFuYWdlbWVudGRkAgQPZBYCAgEPZBYCZg8PFgQfBAU3L2xlYXJuaW5nYW5kY2FyZWVyL2xlYXJuaW5nL3dlYmNhc3RzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQhXZWJjYXN0c2RkAgcPFgIfAGVkAgEPZBYIAgEPFgIfAGVkAgMPFgIfAAWFAjxhIGRpc2FibGVkPScnIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5DZXJ0aWZpY2F0aW9uPC9hPmQCBQ9kFgQCAQ8WAh8ABd8BPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5DZXJ0aWZpY2F0aW9uPC9hPmQCAw8WAh8FAgQWCGYPZBYCAgEPZBYCZg8PFgQfBAU7aHR0cHM6Ly93d3cuc2hybS5vcmcvY2VydGlmaWNhdGlvbi9hcHBseS9wYWdlcy9kZWZhdWx0LmFzcHgfAAUOQXBwbHkgZm9yIEV4YW1kZAIBD2QWAgIBD2QWAmYPDxYEHwQFPmh0dHBzOi8vd3d3LnNocm0ub3JnL2NlcnRpZmljYXRpb24vbGVhcm5pbmcvcGFnZXMvZGVmYXVsdC5hc3B4HwAFGUNlcnRpZmljYXRpb24gUHJlcGFyYXRpb25kZAICD2QWAgIBD2QWAmYPDxYEHwQFOmh0dHBzOi8vd3d3LnNocm0ub3JnL2NlcnRpZmljYXRpb24vZmFxcy9wYWdlcy9kZWZhdWx0LmFzcHgfAAUXU0hSTSBDZXJ0aWZpY2F0aW9uIEZBUXNkZAIDD2QWAgIBD2QWAmYPDxYEHwQFRWh0dHBzOi8vd3d3LnNocm0ub3JnL2NlcnRpZmljYXRpb24vcmVjZXJ0aWZpY2F0aW9uL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQ9SZWNlcnRpZmljYXRpb25kZAIHDxYCHwAFBjwvZGl2PmQCAg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYgCPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkNvbXBldGVuY3kgTW9kZWw8L2E+ZAIFD2QWBAIBDxYCHwAF4gE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkNvbXBldGVuY3kgTW9kZWw8L2E+ZAIDDxYCHwUCAxYGZg9kFgICAQ9kFgJmDw8WBB8EBTYvbGVhcm5pbmdhbmRjYXJlZXIvY29tcGV0ZW5jeS1tb2RlbC9wYWdlcy9kZWZhdWx0LmFzcHgfAAUVU0hSTSBDb21wZXRlbmN5IE1vZGVsZGQCAQ9kFgICAQ9kFgJmDw8WBB8EBT4vbGVhcm5pbmdhbmRjYXJlZXIvY29tcGV0ZW5jeS1tb2RlbC9wYWdlcy9jb21wZXRlbmN5LWZhcXMuYXNweB8ABQ9Db21wZXRlbmN5IEZBUXNkZAICD2QWAgIBD2QWAmYPDxYEHwQFSi9sZWFybmluZ2FuZGNhcmVlci9jb21wZXRlbmN5LW1vZGVsL3BhZ2VzL2NvbXBldGVuY3ktZGlhZ25vc3RpYy10b29scy5hc3B4HwAFG0NvbXBldGVuY3kgRGlhZ25vc3RpYyBUb29sc2RkAgcPFgIfAGVkAgMPZBYIAgEPFgIfAGVkAgMPFgIfAAWLAjxhIGRpc2FibGVkPScnIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5TdHVkZW50IERldmVsb3BtZW50PC9hPmQCBQ9kFgQCAQ8WAh8ABeUBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5TdHVkZW50IERldmVsb3BtZW50PC9hPmQCAw8WAh8FAgQWCGYPZBYCAgEPZBYCZg8PFgQfBAU7aHR0cHM6Ly93d3cuc2hybS5vcmcvYWNhZGVtaWNpbml0aWF0aXZlcy9wYWdlcy9kZWZhdWx0LmFzcHgfAAUUQWNhZGVtaWMgSW5pdGlhdGl2ZXNkZAIBD2QWAgIBD2QWAmYPDxYEHwQFP2h0dHBzOi8vd3d3LnNocm0ub3JnL2FjYWRlbWljaW5pdGlhdGl2ZXMvYW9sL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRxHcmFkdWF0aW9uIEV4YW0gZm9yIFN0dWRlbnRzZGQCAg9kFgICAQ9kFgJmDw8WBB8EBTEvYWJvdXQvZm91bmRhdGlvbi9zY2hvbGFyc2hpcHMvcGFnZXMvYWdzLmFzcC5hc3B4HwAFDFNjaG9sYXJzaGlwc2RkAgMPZBYCAgEPZBYCZg8PFgQfBAUxL2NvbW11bml0aWVzL3N0dWRlbnQtcmVzb3VyY2VzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRVTdHVkZW50IE1lbWJlciBDZW50ZXJkZAIHDxYCHwAFBjwvZGl2PmQCBA9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABf8BPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDA0X3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwNF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkhSIEpvYnM8L2E+ZAIFD2QWBAIBDxYCHwAF2QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDRfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDA0X3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkhSIEpvYnM8L2E+ZAIDDxYCHwUCAxYGZg9kFgICAQ9kFgJmDw8WBB8EBRNodHRwOi8vd3d3LnNocm0ub3JnHwAFCXVjY0hySm9ic2RkAgEPZBYCAgEPZBYCZg8PFgQfBAUVaHR0cDovL2pvYnMuc2hybS5vcmcvHwAFEkJyb3dzZSBBbGwgSm9icy4uLmRkAgIPZBYCAgEPZBYCZg8PFgQfBAUkaHR0cDovL2hyam9icy5zaHJtLm9yZy9qb2JzL3Byb2R1Y3RzHwAFClBvc3QgYSBKb2JkZAIHDxYCHwAFBjwvZGl2PmQCBQ9kFgJmDw8WAh8CZ2QWCgIBDxYCHwAFDVNIUk0gU2VtaW5hcnNkAgMPDxYCHwQFhwFodHRwczovL3N0b3JlLnNocm0ub3JnL2VkdWNhdGlvbi9zZW1pbmFycy9maW5kLWEtc2VtaW5hci5odG1sP2xvY2F0aW9uJTViJTVkPXNhbitmcmFuY2lzY28lMmMrY2EmZGVsaXZlcnlfbWV0aG9kJTViJTVkPXB1YmxpYytpbi1wZXJzb25kFgJmDw8WAh8HBeABaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCUyY2hfMTUyNiUyY3dfMjcxNSUyY3hfMCUyY3lfMTY2L2NfZml0JTJjd183NjcvdjEvTWFya2V0aW5nL2lTdG9ja185MzA5MzQzM19MQVJHRV9hM2x6YTc/ZGF0YWJ0b2E9ZXlJeE5uZzVJanA3SW5naU9qQXNJbmtpT2pFMk5pd2llRElpT2pJM01UVXNJbmt5SWpveE5qa3lMQ0ozSWpveU56RTFMQ0pvSWpveE5USTJmWDAlM2RkZAIFDw8WAh8EBYcBaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9lZHVjYXRpb24vc2VtaW5hcnMvZmluZC1hLXNlbWluYXIuaHRtbD9sb2NhdGlvbiU1YiU1ZD1zYW4rZnJhbmNpc2NvJTJjK2NhJmRlbGl2ZXJ5X21ldGhvZCU1YiU1ZD1wdWJsaWMraW4tcGVyc29uZBYCZg8WAh8ABR1IUiBFZHVjYXRpb24gaW4gU2FuIEZyYW5jaXNjb2QCBw8WAh8ABZYBU0hSTSBTZW1pbmFycyB3aWxsIGhvc3QgSFIgZWR1Y2F0aW9uIGV2ZXJ5IG1vbnRoIGluIFNhbiBGcmFuY2lzY28gdGhpcyBmYWxsISBTZWxlY3QgdGhlIHByb2dyYW0gdGhhdCBtZWV0cyBib3RoIHlvdXIgc2NoZWR1bGluZyBhbmQgZGV2ZWxvcG1lbnQgbmVlZHMuZAIJDw8WBB8EBYcBaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9lZHVjYXRpb24vc2VtaW5hcnMvZmluZC1hLXNlbWluYXIuaHRtbD9sb2NhdGlvbiU1YiU1ZD1zYW4rZnJhbmNpc2NvJTJjK2NhJmRlbGl2ZXJ5X21ldGhvZCU1YiU1ZD1wdWJsaWMraW4tcGVyc29uHwAFDFJlZ2lzdGVyIE5vd2RkAgMPZBYCAgEPFgIfBgUHZHJvcG91dBYEAgEPFgIfAAW4ATxhIGRpc2FibGVkPScnIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3Bhbl9Ecm9wb3V0TWVudScgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkV2ZW50czwvYT5kAgMPZBYGAgEPFgIfAAWsATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3Bhbl9Ecm9wb3V0TWVudScgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkV2ZW50czwvYT5kAgMPFgIfBQIEFghmD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFgwI8YSBkaXNhYmxlZD0nJyBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+U0hSTSBFdmVudHM8L2E+ZAIFD2QWBAIBDxYCHwAF3QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlNIUk0gRXZlbnRzPC9hPmQCAw8WAh8FAgkWEmYPZBYCAgEPZBYCZg8PFgQfBAUXaHR0cDovL2FubnVhbC5zaHJtLm9yZy8fAAUjU0hSTSBBbm51YWwgQ29uZmVyZW5jZSAmIEV4cG9zaXRpb25kZAIBD2QWAgIBD2QWAmYPDxYEHwQFMGh0dHA6Ly9jb25mZXJlbmNlcy5zaHJtLm9yZy9kaXZlcnNpdHktY29uZmVyZW5jZR8ABS1EaXZlcnNpdHkgJiBJbmNsdXNpb24gQ29uZmVyZW5jZSAmIEV4cG9zaXRpb25kZAICD2QWAgIBD2QWAmYPDxYEHwQFM2h0dHBzOi8vY29uZmVyZW5jZXMuc2hybS5vcmcvbGVnaXNsYXRpdmUtY29uZmVyZW5jZR8ABSdFbXBsb3ltZW50IExhdyAmIExlZ2lzbGF0aXZlIENvbmZlcmVuY2VkZAIDD2QWAgIBD2QWAmYPDxYEHwQFOWh0dHBzOi8vY29uZmVyZW5jZXMuc2hybS5vcmcvbGVhZGVyc2hpcC1kZXZlbG9wbWVudC1mb3J1bR8ABRxMZWFkZXJzaGlwIERldmVsb3BtZW50IEZvcnVtZGQCBA9kFgICAQ9kFgJmDw8WBB8EBS1odHRwOi8vY29uZmVyZW5jZXMuc2hybS5vcmcvdGFsZW50LWNvbmZlcmVuY2UfAAUpVGFsZW50IE1hbmFnZW1lbnQgQ29uZmVyZW5jZSAmIEV4cG9zaXRpb25kZAIFD2QWAgIBD2QWAmYPDxYEHwQFMmh0dHBzOi8vY29uZmVyZW5jZXMuc2hybS5vcmcvc2hybS10YWxlbnQtc3ltcG9zaXVtHwAFEFRhbGVudCBTeW1wb3NpdW1kZAIGD2QWAgIBD2QWAmYPDxYEHwQFLy9ldmVudHMvcGFnZXMvc3RhdGUtLWFmZmlsaWF0ZS1jb25mZXJlbmNlcy5hc3B4HwAFHVN0YXRlICYgQWZmaWxpYXRlIENvbmZlcmVuY2VzZGQCBw9kFgICAQ9kFgJmDw8WBB8EBTcvbGVhcm5pbmdhbmRjYXJlZXIvbGVhcm5pbmcvd2ViY2FzdHMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFCFdlYmNhc3RzZGQCCA9kFgICAQ9kFgJmDw8WBB8EBRovZXZlbnRzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQ5TRUUgQUxMIEVWRU5UU2RkAgcPFgIfAAUGPC9kaXY+ZAIBD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFhwI8YSBkaXNhYmxlZD0nJyBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+RXZlbnQgUmVzb3VyY2VzPC9hPmQCBQ9kFgQCAQ8WAh8ABeEBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5FdmVudCBSZXNvdXJjZXM8L2E+ZAIDDxYCHwUCBhYMZg9kFgICAQ9kFgJmDw8WBB8EBSxodHRwOi8vbmlmdHl0ZWNobm9sb2dpZXMubmV0L3Nocm0zL2luZGV4LnBocB8ABRJSZXF1ZXN0IGEgQnJvY2h1cmVkZAIBD2QWAgIBD2QWAmYPDxYEHwQFJi9ldmVudHMvcGFnZXMvc3BlYWtlci1pbmZvcm1hdGlvbi5hc3B4HwAFE1NwZWFrZXIgSW5mb3JtYXRpb25kZAICD2QWAgIBD2QWAmYPDxYEHwQFLmh0dHA6Ly9jb25mZXJlbmNlcy5zaHJtLm9yZy9leGhpYml0LW9yLXNwb25zb3IfAAUjU3BvbnNvcnNoaXAgJiBFeGhpYml0b3IgSW5mb3JtYXRpb25kZAIDD2QWAgIBDxYCHwJoFgJmDw8WBB8EBRVodHRwOi8vd3d3Lmdvb2dsZS5jb20fAAUBJGRkAgQPZBYCAgEPFgIfAmgWAmYPDxYEHwQFFWh0dHA6Ly93d3cuZ29vZ2xlLmNvbR8ABQEkZGQCBQ9kFgICAQ8WAh8CaBYCZg8PFgQfBAUVaHR0cDovL3d3dy5nb29nbGUuY29tHwAFASRkZAIHDxYCHwAFBjwvZGl2PmQCAg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYUCPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkdsb2JhbCBFdmVudHM8L2E+ZAIFD2QWBAIBDxYCHwAF3wE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkdsb2JhbCBFdmVudHM8L2E+ZAIDDxYCHwUCARYCZg9kFgICAQ9kFgJmDw8WBB8EBRdodHRwOi8vd3d3LnNocm1pYWMub3JnLx8ABRxTSFJNIEluZGlhIEFubnVhbCBDb25mZXJlbmNlZGQCBw8WAh8AZWQCAw9kFggCAQ8WAh8AZWQCAw8WAh8ABYgCPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkFmZmlsaWF0ZSBFdmVudHM8L2E+ZAIFD2QWBAIBDxYCHwAF4gE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkFmZmlsaWF0ZSBFdmVudHM8L2E+ZAIDDxYCHwUCAhYEZg9kFgICAQ9kFgJmDw8WBB8EBR5odHRwczovL3d3dy5jZmdpLm9yZy9zeW1wb3NpdW0fAAUOQ0ZHSSBTeW1wb3NpdW1kZAIBD2QWAgIBD2QWAmYPDxYEHwQFKGh0dHA6Ly93d3cuaHJwcy5vcmcvbXBhZ2UvMjAxNmZvcnVtaG9tZS8fAAUSU3RyYXRlZ2ljIEhSIEZvcnVtZGQCBw8WAh8ABQY8L2Rpdj5kAgUPZBYCZg8PFgIfAmdkFgoCAQ8WAh8ABRxMZWFkZXJzaGlwIERldmVsb3BtZW50IEZvcnVtZAIDDw8WAh8EBTlodHRwczovL2NvbmZlcmVuY2VzLnNocm0ub3JnL2xlYWRlcnNoaXAtZGV2ZWxvcG1lbnQtZm9ydW1kFgJmDw8WAh8HBdgBaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCUyY2hfMjc2NCUyY3dfNDkxNCUyY3hfNDklMmN5XzExNjcvY19maXQlMmN3Xzc2Ny92MS9NYXJrZXRpbmcvSGlSZXNfaGg3b3V6P2RhdGFidG9hPWV5SXhObmc1SWpwN0luZ2lPalE1TENKNUlqb3hNVFkzTENKNE1pSTZORGsyTWl3aWVUSWlPak01TXpFc0luY2lPalE1TVRRc0ltZ2lPakkzTmpSOWZRJTNkJTNkZGQCBQ8PFgIfBAU5aHR0cHM6Ly9jb25mZXJlbmNlcy5zaHJtLm9yZy9sZWFkZXJzaGlwLWRldmVsb3BtZW50LWZvcnVtZBYCZg8WAh8ABRNTZWF0dGxlLCBXYXNoaW5ndG9uZAIHDxYCHwAFG0pvaW4gdXMsIFNlcHRlbWJlciAyNyAtIDI4LmQCCQ8PFgQfBAU5aHR0cHM6Ly9jb25mZXJlbmNlcy5zaHJtLm9yZy9sZWFkZXJzaGlwLWRldmVsb3BtZW50LWZvcnVtHwAFDlJlZ2lzdGVyIFRvZGF5ZGQCBA9kFgICAQ8WAh8GBQdkcm9wb3V0FgQCAQ8WAh8ABb0BPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDRfcGFuX0Ryb3BvdXRNZW51JyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDRfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+Q29tbXVuaXRpZXM8L2E+ZAIDD2QWBAIBDxYCHwAFsQE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwNF9wYW5fRHJvcG91dE1lbnUnIGhyZWY9JyNjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwNF9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5Db21tdW5pdGllczwvYT5kAgMPFgIfBQIDFgZmD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFgwI8YSBkaXNhYmxlZD0nJyBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+Q29tbXVuaXRpZXM8L2E+ZAIFD2QWBAIBDxYCHwAF3QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkNvbW11bml0aWVzPC9hPmQCAw8WAh8FAgUWCmYPZBYCAgEPZBYCZg8PFgQfBAUaaHR0cDovL2NvbW11bml0eS5zaHJtLm9yZy8fAAUMU0hSTSBDb25uZWN0ZGQCAQ9kFgICAQ9kFgJmDw8WBB8EBTQvY29tbXVuaXRpZXMvY29tbXVuaXRpZXMvY2hhcHRlcnMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFCENoYXB0ZXJzZGQCAg9kFgICAQ9kFgJmDw8WBB8EBRRodHRwOi8vd3d3LmhycHMub3JnLx8ABRFFeGVjdXRpdmUgTmV0d29ya2RkAgMPZBYCAgEPZBYCZg8PFgQfBAVCL2NvbW11bml0aWVzL2NvbW11bml0aWVzL2hyLXlvdW5nLXByb2Zlc3Npb25hbHMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFFkhSIFlvdW5nIFByb2Zlc3Npb25hbHNkZAIED2QWAgIBD2QWAmYPDxYEHwQFImh0dHA6Ly93d3cuYWR2b2NhY3kuc2hybS5vcmcvYWJvdXQfAAUiTGVnaXNsYXRpdmUgQWR2b2NhY3kgVGVhbSAoQS1UZWFtKWRkAgcPFgIfAAUGPC9kaXY+ZAIBD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFiQI8YSBkaXNhYmxlZD0nJyBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+U3R1ZGVudCBSZXNvdXJjZXM8L2E+ZAIFD2QWBAIBDxYCHwAF4wE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlN0dWRlbnQgUmVzb3VyY2VzPC9hPmQCAw8WAh8FAgEWAmYPZBYCAgEPZBYCZg8PFgQfBAUxL2NvbW11bml0aWVzL3N0dWRlbnQtcmVzb3VyY2VzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRVTdHVkZW50IE1lbWJlciBDZW50ZXJkZAIHDxYCHwAFBjwvZGl2PmQCAg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYICPGEgZGlzYWJsZWQ9JycgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgaHJlZj0nI2N0bDAwX2N0bDc0X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPlZvbHVudGVlcnM8L2E+ZAIFD2QWBAIBDxYCHwAF3AE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NF9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBocmVmPScjY3RsMDBfY3RsNzRfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlZvbHVudGVlcnM8L2E+ZAIDDxYCHwUCBBYIZg9kFgICAQ9kFgJmDw8WBB8EBT4vY29tbXVuaXRpZXMvdm9sdW50ZWVycy9tZW1iZXJzaGlwLWNvdW5jaWxzL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRNNZW1iZXJzaGlwIENvdW5jaWxzZGQCAQ9kFgICAQ9kFgJmDw8WBB8EBUMvY29tbXVuaXRpZXMvdm9sdW50ZWVycy9zcGVjaWFsLWV4cGVydGlzZS1wYW5lbHMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFGFNwZWNpYWwgRXhwZXJ0aXNlIFBhbmVsc2RkAgIPZBYCAgEPZBYCZg8PFgQfBAUfaHR0cDovL2NvbW11bml0eS5zaHJtLm9yZy92bHJjLx8ABSBWb2x1bnRlZXIgTGVhZGVyIFJlc291cmNlIENlbnRlcmRkAgMPZBYCAgEPZBYCZg8PFgQfBAU6L2NvbW11bml0aWVzL3ZvbHVudGVlcnMvcGFnZXMvdm9sdW50ZWVyLW9wcG9ydHVuaXRpZXMuYXNweB8ABRdWb2x1bnRlZXIgT3Bwb3J0dW5pdGllc2RkAgcPFgIfAAUGPC9kaXY+ZAIDD2QWCAIBDw8WAh8CaGRkAgMPDxYCHwJoZGQCCQ8PFgIfAmhkZAILD2QWAgIBDw8WAh8ABQ1HRVQgQ0VSVElGSUVEZGQCBQ9kFgICAw8WAh4Hb25jbGljawVYcmV0dXJuIE9wZW5Jbk5ld1dpbmRvdygnY3RsMDBfY3RsNzRfSHJKb2JzX3NlYXJjaEpvYnMnLCdjdGwwMF9jdGw3NF9IckpvYnNfc2VhcmNoVGVybScpO2QCBw9kFgICAQ8PZBYCHwgFcXdpbmRvdy5sb2NhdGlvbj0nL3NlYXJjaC9wYWdlcy9Mb2NhbENoYXB0ZXIuYXNweD9sb2NhdGlvbj0nICsgJCh0aGlzKS5jbG9zZXN0KCcuZmluZGVyLWZvcm0nKS5maW5kKCdpbnB1dCcpLnZhbCgpZAIMD2QWIgIBD2QWAgICDxYGHwYFC3Nocm0tS2lja2VyHwgFNHdpbmRvdy5sb2NhdGlvbiA9ICdodHRwczovL3d3dy5zaHJtLm9yZy9hYm91dC1zaHJtJzsfAmcWAmYPFgIfAAUKQWJvdXQgU0hSTWQCAw8WAh8BCysEAWQCBQ8WAh8BCysEAWQCBw9kFgYCAQ8WAh8BCysEAWQCAw8PFgIfAmhkFgYCAQ8WAh8GBQ9uby1hdXRob3ItaW1hZ2VkAgMPDxYCHwJoZGQCBQ8PFgIfAGVkZAIFDw8WAh8CaGRkAgsPZBYEZg8WAh8CaGQCAg8PFgIfAmhkZAINDxYCHwELKwQBZAIPDxYCHwELKwQBZAIRDxYCHwELKwQBZAIVD2QWAmYPDxYEHghDc3NDbGFzcwUmc2hybS10YWdzIHNocm0tdGFncy1lbXB0eSBhcnRpY2xlLXRhZ3MeBF8hU0ICAmQWAgIBDxYCHwVmZAIXD2QWBGYPFgIfAmhkAgIPDxYCHwJoZGQCGw9kFgJmDw8WAh8CZ2QWBgIBDw8WAh8EBXRodHRwczovL3d3dy5zaHJtLm9yZy9yZXNvdXJjZXNhbmR0b29scy9oci10b3BpY3MvdGVjaG5vbG9neS9wYWdlcy9leHBlcnRzLXNheS1tb3JlLWN5YmVyc2VjdXJpdHktd29ya2Vycy1uZWVkZWQuYXNweGRkAgMPDxYCHwJnZBYCAgEPDxYCHwcFygFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF80ODYlMmN3Xzg2NCUyY3hfMCUyY3lfNzUvY19maXQlMmN3Xzc2Ny92MS9UZWNobm9sb2d5L2xhcHRvcHdvbWFuX3Z0ODlhdT9kYXRhYnRvYT1leUl4Tm5nNUlqcDdJbmdpT2pBc0lua2lPamMxTENKNE1pSTZPRFkwTENKNU1pSTZOVFl4TENKM0lqbzROalFzSW1naU9qUTRObjE5ZGQCBQ8WAh8ABShTdHVkeTogTW9yZSBDeWJlcnNlY3VyaXR5IFdvcmtlcnMgTmVlZGVkZAIfD2QWAgICD2QWAgIBD2QWBAIHD2QWAgIBDxYCHwELKwQBZAIJD2QWAgIBDzwrAAkBAA8WAh4NTmV2ZXJFeHBhbmRlZGdkZAIjD2QWAgIBDxYCHwUCAxYGZg9kFgYCAQ9kFgJmDw8WAh8HBagCaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCUyY2hfNDc0JTJjd184NDMlMmN4XzAlMmN5XzI4L2NfZml0JTJjd183NjcvdjEvTGVnYWwlMjBhbmQlMjBDb21wbGlhbmNlL3JlbW90ZV9hY2Nlc3NfcmxvemJrP2RhdGFidG9hPWV5SXplRFFpT25zaWVDSTZNall3TENKNUlqb3dMQ0o0TWlJNk5qZzFMQ0o1TWlJNk5UWTFMQ0ozSWpvME1qUXNJbWdpT2pVMk5YMHNJakUyZURraU9uc2llQ0k2TUN3aWVTSTZNamdzSW5neUlqbzRORE1zSW5reUlqbzFNRElzSW5jaU9qZzBNeXdpYUNJNk5EYzBmWDAlM2RkZAIDDw8WBB8EBVxodHRwczovL3d3dy5zaHJtLm9yZy9yZXNvdXJjZXNhbmR0b29scy9oci10b3BpY3MvY29tcGVuc2F0aW9uL3BhZ2VzL3NhbGFyeS1idWRnZXRzLTIwMTcuYXNweB8ABSpTYWxhcnkgQnVkZ2V0cyBFeHBlY3RlZCB0byBSaXNlIDMlIGluIDIwMTdkZAIFDxYCHwVmZAIBD2QWBgIBD2QWAmYPDxYCHwcF1wNodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF8xNTI2JTJjd18yNzE0JTJjeF8wJTJjeV8wL2NfZml0JTJjd183NjcvdjEvRW1wbG95ZWUlMjBSZWxhdGlvbnMvaVN0b2NrXzg4NTI3NzY3X0xBUkdFX2JwYXlpaj9kYXRhYnRvYT1leUl4Tm5nNUlqcDdJbmdpT2pBc0lua2lPakFzSW5neUlqb3lOekUwTENKNU1pSTZNVFV5Tml3aWR5STZNamN4TkN3aWFDSTZNVFV5Tm4wc0lqSXhlRGtpT25zaWVDSTZNQ3dpZVNJNk5qWXNJbmd5SWpveU56RTBMQ0o1TWlJNk1USXlPU3dpZHlJNk1qY3hOQ3dpYUNJNk1URTJNMzBzSWpONE5DSTZleUo0SWpvMU1EQXNJbmtpT2pBc0luZ3lJam94T0RVNExDSjVNaUk2TVRneE1Dd2lkeUk2TVRNMU9Dd2lhQ0k2TVRneE1IMHNJakY0TVNJNmV5SjRJam93TENKNUlqb3dMQ0o0TWlJNk1UZ3hNQ3dpZVRJaU9qRTRNVEFzSW5jaU9qRTRNVEFzSW1naU9qRTRNVEI5ZlElM2QlM2RkZAIDDw8WBB8EBW5odHRwczovL3d3dy5zaHJtLm9yZy9yZXNvdXJjZXNhbmR0b29scy9oci10b3BpY3MvZW1wbG95ZWUtcmVsYXRpb25zL3BhZ2VzL3Nob3J0ZXItaG91cnMtZm9yLW9sZGVyLXdvcmtlcnMuYXNweB8ABS5TaG91bGQgV29ya2VycyBPdmVyIDQwIEhhdmUgRm91ci1EYXkgV2Vla2VuZHM/ZGQCBQ8WAh8FZmQCAg9kFgYCAQ9kFgJmDw8WAh8HBdoBaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCUyY2hfOTYxJTJjd18xNzA5JTJjeF82NDIlMmN5XzIxNC9jX2ZpdCUyY3dfNzY3L3YxL05ld3MvaVN0b2NrXzk4MzU0NDg1X0xBUkdFX2lwYzdtej9kYXRhYnRvYT1leUl4Tm5nNUlqcDdJbmdpT2pZME1pd2llU0k2TWpFMExDSjRNaUk2TWpNMU1Td2llVElpT2pFeE56VXNJbmNpT2pFM01Ea3NJbWdpT2prMk1YMTlkZAIDDw8WBB8EBV9odHRwczovL3d3dy5zaHJtLm9yZy9oci10b2RheS9uZXdzL2hyLW5ld3MvcGFnZXMvY2xpbnRvbi12cy10cnVtcC1lcXVhbC1wYXktZm9yLWVxdWFsLXdvcmsuYXNweB8ABStDbGludG9uIHZzLiBUcnVtcDogRXF1YWwgUGF5IGZvciBFcXVhbCBXb3JrZGQCBQ8WAh8FZmQCJQ9kFgJmD2QWCAIBDxYCHwAFCFNFTUlOQVJTZAIDDw8WBB8EBT1odHRwczovL3N0b3JlLnNocm0ub3JnL2VkdWNhdGlvbi9zZW1pbmFycy9maW5kLWEtc2VtaW5hci5odG1sHwJnZBYCZg8PFgIfBwXbAWh0dHBzOi8vY2RuLnNocm0ub3JnL2ltYWdlL3VwbG9hZC9jX2Nyb3AlMmNoXzk1NCUyY3dfMTY5NyUyY3hfMCUyY3lfMTMvY19maXQlMmN3Xzc2Ny92MS9NYXJrZXRpbmcvaVN0b2NrXzcxOTgxMzczX01FRElVTV92a3dzY3E/ZGF0YWJ0b2E9ZXlJeE5uZzVJanA3SW5naU9qQXNJbmtpT2pFekxDSjRNaUk2TVRZNU55d2llVElpT2prMk55d2lkeUk2TVRZNU55d2lhQ0k2T1RVMGZYMCUzZGRkAgUPFgIfAAUfSFIgRWR1Y2F0aW9uIGluIGEgQ2l0eSBOZWFyIFlvdWQCBw8PFgQfBAU9aHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9lZHVjYXRpb24vc2VtaW5hcnMvZmluZC1hLXNlbWluYXIuaHRtbB8ABQ5GaW5kIGEgU2VtaW5hcmRkAicPZBYCAgMPFgIfCAVscmV0dXJuIE9wZW5Jbk5ld1dpbmRvdygnY3RsMDBfUGxhY2VIb2xkZXJNYWluX0hySm9ic19zZWFyY2hKb2JzJywnY3RsMDBfUGxhY2VIb2xkZXJNYWluX0hySm9ic19zZWFyY2hUZXJtJyk7ZAIrD2QWEgIBDw8WAh8ABQhBcnRpY2xlc2RkAgMPDxYCHwAFBVRvb2xzZGQCBQ8PFgIfAAUITGVhcm5pbmdkZAIHD2QWBgIBDw8WAh8HBc4DaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCUyY2hfMTUyNSUyY3dfMjcxMyUyY3hfMCUyY3lfNjMvY19maXQlMmN3Xzc2Ny92MS9UZWNobm9sb2d5L2lTdG9ja183NjA0NTczNV9MQVJHRV9wcGV0ZzY/ZGF0YWJ0b2E9ZXlJeE5uZzVJanA3SW5naU9qQXNJbmtpT2pZekxDSjRNaUk2TWpjeE15d2llVElpT2pFMU9EZ3NJbmNpT2pJM01UTXNJbWdpT2pFMU1qVjlMQ0l5TVhnNUlqcDdJbmdpT2pBc0lua2lPakUwTVN3aWVESWlPakkzTVRNc0lua3lJam94TXpBekxDSjNJam95TnpFekxDSm9Jam94TVRZeWZTd2lNM2cwSWpwN0luZ2lPakV5TlRBc0lua2lPakFzSW5neUlqb3lOakE1TENKNU1pSTZNVGd4TVN3aWR5STZNVE0yTUN3aWFDSTZNVGd4TVgwc0lqRjRNU0k2ZXlKNElqbzVNREFzSW5raU9qQXNJbmd5SWpveU56RXpMQ0o1TWlJNk1UZ3hNU3dpZHlJNk1UZ3hNeXdpYUNJNk1UZ3hNWDE5ZGQCAw8PFgQfAAU4SG93IExlYWRlcnMgQ2FuIEhlbHAgRW1wbG95ZWVzIEFjY2VwdCBUZWNobm9sb2d5IENoYW5nZXMfBAV3aHR0cHM6Ly93d3cuc2hybS5vcmcvaHItdG9kYXkvbmV3cy9oci1tYWdhemluZS8wNzE2L3BhZ2VzL2hvdy1sZWFkZXJzLWNhbi1oZWxwLWVtcGxveWVlcy1hY2NlcHQtdGVjaG5vbG9neS1jaGFuZ2VzLmFzcHhkZAIFDxYCHwVmZAIJDxYCHwUCAxYGZg9kFgICAQ8PFgQfBAVRaHR0cHM6Ly93d3cuc2hybS5vcmcvaHItdG9kYXkvbmV3cy9oci1uZXdzL3BhZ2VzL3BhaWQtbGVhdmUtZGlzY3Vzc2VkLWF0LWRuYy5hc3B4HwAFG1BhaWQgTGVhdmUgRGlzY3Vzc2VkIGF0IEROQ2RkAgEPZBYCAgEPDxYEHwQFaWh0dHBzOi8vd3d3LnNocm0ub3JnL2hyLXRvZGF5L25ld3MvaHItbmV3cy9wYWdlcy9lbXBsb3llcnMtbXVzdC1wbGF5LWEtcm9sZS1pbi13b3JrZm9yY2UtZGV2ZWxvcG1lbnQuYXNweB8ABTNFbXBsb3llcnMgTXVzdCBQbGF5IGEgUm9sZSBpbiBXb3JrZm9yY2UgRGV2ZWxvcG1lbnRkZAICD2QWAgIBDw8WBB8EBVlodHRwczovL3d3dy5zaHJtLm9yZy9oci10b2RheS9uZXdzL2hyLW1hZ2F6aW5lLzA3MTYvcGFnZXMvd2hlbi1taWxsZW5uaWFscy10YWtlLW92ZXIuYXNweB8ABSlXaGF0IEVtcGxveWVycyBDYW4gTGVhcm4gRnJvbSBNaWxsZW5uaWFsc2RkAgsPZBYGAgEPDxYCHwcF4wFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF8zMTU3JTJjd181NjE2JTJjeF8wJTJjeV8zMTgvY19maXQlMmN3Xzc2Ny92MS9NYXJrZXRpbmcvaVN0b2NrXzg2MDM1OTYzX1hYWExBUkdFX3ltOTJmZz9kYXRhYnRvYT1leUl4Tm5nNUlqcDdJbmdpT2pBc0lua2lPak14T0N3aWVESWlPalUyTVRZc0lua3lJam96TkRjMUxDSjNJam8xTmpFMkxDSm9Jam96TVRVM2ZYMCUzZGRkAgMPDxYEHwAFRFlvdXIgY29tcHJlaGVuc2l2ZSBzeXN0ZW0gdG8gcHJlcGFyZSBmb3IgdGhlIFNIUk0gY2VydGlmaWNhdGlvbiBleGFtHwQFR2h0dHBzOi8vc3RvcmUuc2hybS5vcmcvY2VydGlmaWNhdGlvbi9jZXJ0aWZpY2F0aW9uL2xlYXJuaW5nLXN5c3RlbS5odG1sZGQCBQ8WAh8FZmQCDQ8WAh8FAgMWBmYPZBYCAgEPDxYEHwQFb2h0dHBzOi8vd3d3LnNocm0ub3JnL3Jlc291cmNlc2FuZHRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL2hyLWZvcm1zL3BhZ2VzL2FwcGxpY2F0aW9uX2VtcGxveW1lbnRhcHBsaWNhdGlvbjMuYXNweB8ABSdBcHBsaWNhdGlvbjogRW1wbG95bWVudCBBcHBsaWNhdGlvbiAjMyBkZAIBD2QWAgIBDw8WBB8EBW9odHRwczovL3d3dy5zaHJtLm9yZy9yZXNvdXJjZXNhbmR0b29scy90b29scy1hbmQtc2FtcGxlcy9oci1mb3Jtcy9wYWdlcy9hcHBsaWNhdGlvbl9lbXBsb3ltZW50YXBwbGljYXRpb24xLmFzcHgfAAUnQXBwbGljYXRpb246IEVtcGxveW1lbnQgQXBwbGljYXRpb24gIzEgZGQCAg9kFgICAQ8PFgQfBAVvaHR0cHM6Ly93d3cuc2hybS5vcmcvcmVzb3VyY2VzYW5kdG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvaHItZm9ybXMvcGFnZXMvYXBwbGljYXRpb25fZW1wbG95bWVudGFwcGxpY2F0aW9uMi5hc3B4HwAFJ0FwcGxpY2F0aW9uOiBFbXBsb3ltZW50IEFwcGxpY2F0aW9uICMyIGRkAg8PZBYGAgEPDxYCHwcF2wFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF8zMjM4JTJjd181NzYwJTJjeF8wJTJjeV8wL2NfZml0JTJjd183NjcvdjEvTWFya2V0aW5nL2lTdG9ja185Mjk4ODI3NV9YWFhMQVJHRV95ZzJwamc/ZGF0YWJ0b2E9ZXlJeE5uZzVJanA3SW5naU9qQXNJbmtpT2pBc0luZ3lJam8xTnpZd0xDSjVNaUk2TXpJek9Dd2lkeUk2TlRjMk1Dd2lhQ0k2TXpJek9IMTlkZAIDDw8WBB8ABS01MDArIEhSIGVkdWNhdGlvbiBjb3Vyc2VzIGF0IHlvdXIgZmluZ2VydGlwcy4fBAUvaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9lZHVjYXRpb24vZWxlYXJuaW5nLmh0bWxkZAIFDxYCHwVmZAIRDxYCHwUCAxYGZg9kFgICAQ8PFgQfBAVlaHR0cHM6Ly93d3cuc2hybS5vcmcvbGVhcm5pbmdhbmRjYXJlZXIvbGVhcm5pbmcvb25zaXRlLXRyYWluaW5nL3BhZ2VzL2VtZXJnaW5nLWxlYWRlcnMtZWR1Y2F0aW9uLmFzcHgfAAUcIEVtZXJnaW5nIExlYWRlcnMgRWR1Y2F0aW9uIGRkAgEPZBYCAgEPDxYEHwQFSmh0dHBzOi8vd3d3LnNocm0ub3JnL2xlYXJuaW5nYW5kY2FyZWVyL2NvbXBldGVuY3ktbW9kZWwvcGFnZXMvZGVmYXVsdC5hc3B4HwAFGkFib3V0IHRoZSBDb21wZXRlbmN5IE1vZGVsZGQCAg9kFgICAQ8PFgQfBAVSaHR0cHM6Ly93d3cuc2hybS5vcmcvbGVhcm5pbmdhbmRjYXJlZXIvY29tcGV0ZW5jeS1tb2RlbC9wYWdlcy9jb21wZXRlbmN5LWZhcXMuYXNweB8ABQ9Db21wZXRlbmN5IEZBUXNkZAItD2QWBGYPDxYCHwJnZGQCAg8WAh8ABUk8c2NyaXB0IHNyYz0nLy93d3cuc2hybS5vcmcvRG9jdW1lbnRzL1Nwb25zb3JPZmZlckZldGNoRmlsZS5qcyc+PC9zY3JpcHQ+ZAIOD2QWFGYPDxYCHwQFHWh0dHA6Ly9sLnNocm0ub3JnL29wdC9VbmlmaWVkZGQCAQ8WAh8ABRVKT0lOIFRIRSBDT05WRVJTQVRJT05kAgIPDxYCHwQFOWh0dHA6Ly93d3cuZmFjZWJvb2suY29tL3NvY2lldHlmb3JodW1hbnJlc291cmNlbWFuYWdlbWVudGRkAgMPDxYCHwQFN2h0dHA6Ly93d3cubGlua2VkaW4uY29tL2NvbXBhbnkvMTEyODI/dHJrPU5VU19DTVBZX1RXSVRkZAIEDw8WAh8EBSNodHRwczovL2luc3RhZ3JhbS5jb20vc2hybW9mZmljaWFsL2RkAgUPDxYCHwQFI2h0dHA6Ly93d3cueW91dHViZS5jb20vc2hybW9mZmljaWFsZGQCBg8PFgIfBAUaL2Fib3V0LXNocm0vcGFnZXMvcnNzLmFzcHhkZAIHDw8WAh8EBRdodHRwOi8vdHdpdHRlci5jb20vU0hSTWRkAggPFgIfBQIJFhJmD2QWAgIBDxYCHwYFCGNvbC14cy02FgICAQ8PFgQfBAUyaHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wYWdlcy9kZWZhdWx0LmFzcHgfAAUKQWJvdXQgU0hSTWRkAgEPZBYCAgEPFgIfBgUIY29sLXhzLTYWAgIBDw8WBB8EBSlodHRwOi8vZm9ybXMuc2hybS5vcmcvZm9ybXMvc3BlYWtlcnMtZm9ybR8ABRBTcGVha2VyJ3MgQnVyZWF1ZGQCAg9kFgICAQ8WAh8GBQhjb2wteHMtNhYCAgEPDxYEHwQFNWh0dHBzOi8vd3d3LnNocm0ub3JnL2Fib3V0LXNocm0vcGFnZXMvbWVtYmVyc2hpcC5hc3B4HwAFCk1lbWJlcnNoaXBkZAIDD2QWAgIBDxYCHwYFCGNvbC14cy02FgICAQ8PFgQfBAVBaHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wYWdlcy9jb3B5cmlnaHQtLXBlcm1pc3Npb25zLmFzcHgfAAUXQ29weXJpZ2h0ICYgUGVybWlzc2lvbnNkZAIED2QWAgIBDxYCHwYFCGNvbC14cy02FgICAQ8PFgQfBAVBaHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wYWdlcy9ieWxhd3MtLWNvZGUtb2YtZXRoaWNzLmFzcHgfAAUXQnlsYXdzICYgQ29kZSBvZiBFdGhpY3NkZAIFD2QWAgIBDxYCHwYFCGNvbC14cy02FgICAQ8PFgQfBAUvaHR0cDovL3d3dy5zaHJtLm9yZy9tZWRpYWtpdC9wYWdlcy9kZWZhdWx0LmFzcHgfAAURQWR2ZXJ0aXNlIHdpdGggVXNkZAIGD2QWAgIBDxYCHwYFCGNvbC14cy02FgICAQ8PFgQfBAUyaHR0cHM6Ly93d3cuc2hybS5vcmcvYXNzZXRzL3Nocm1jYXJlZXJzL2luZGV4Lmh0bWwfAAUUQ2FyZWVyIE9wcG9ydHVuaXRpZXNkZAIHD2QWAgIBDxYCHwYFCGNvbC14cy02FgICAQ8PFgQfBAUkaHR0cDovL2hyam9icy5zaHJtLm9yZy9qb2JzL3Byb2R1Y3RzHwAFClBvc3QgYSBKb2JkZAIID2QWAgIBDxYCHwYFCGNvbC14cy02FgICAQ8PFgQfBAU9aHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wcmVzcy1yb29tL3BhZ2VzL2RlZmF1bHQuYXNweB8ABQpQcmVzcyBSb29tZGQCCQ8PFgIfBAVaaHR0cDovL3d3dy5zaHJtLm9yZy9hYm91dC9mb3VuZGF0aW9uL3N1cHBvcnR0aGVmb3VuZGF0aW9uL2NvbnRyaWJ1dGlvbnMvcGFnZXMvZGVmYXVsdC5hc3B4ZGQCDw9kFgICAQ9kFgICAQ88KwAFAQAPFgIeFVBhcmVudExldmVsc0Rpc3BsYXllZGZkZAIzD2QWAgIBD2QWAgINDw8WAh8CaGQWAgICD2QWAmYPZBYCAgMPZBYCAgUPZBYCAgEPPCsACQEADxYEHg1QYXRoU2VwYXJhdG9yBAgfC2dkZAITD2QWAmYPDxYCHwcFc34vX2xheW91dHMvMTUvU0hSTS5Db3JlL3V0aWxpdHkvcGFnZXZpZXd0cmFja2VyLmFzcHg/dXNlcj1Bbm9ueW1vdXMmdXJsPSUyZmFib3V0LXNocm0lMmZQYWdlcyUyZlByaXZhY3ktUG9saWN5LmFzcHhkZGQ99T72Tv3dsxZR2GEEQVIzE0brPcdDVt/wHM/8kqMisw==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=4yL1y1QJ0QhVEUCrTTXpDzuEWyMCtTXZY6B6_TJaPJuc9HrvB8a_21p-Bsmy6K_l1LufbwXFRBZL7t0Jz3QBse1ipb8E3o9qWwtZDTbEWQ81&amp;t=635792883671809273" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaQoSEnabled = false;
var g_wsaQoSDataPoints = [];
var g_wsaLCID = 1033;
var g_wsaListTemplateId = 850;
var g_wsaSiteTemplateId = 'CMSPUBLISHING#0';
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fabout-shrm", webAbsoluteUrl: "https:\u002f\u002fwww.shrm.org\u002fabout-shrm", siteAbsoluteUrl: "https:\u002f\u002fwww.shrm.org", serverRequestPath: "\u002fabout-shrm\u002fPages\u002fPrivacy-Policy.aspx", layoutsUrl: "_layouts\u002f15", webTitle: "About SHRM", webTemplate: "39", tenantAppVersion: "0", isAppWeb: false, webLogoUrl: "_layouts\u002f15\u002fimages\u002fsiteicon.png", webLanguage: 1033, currentLanguage: 1033, currentUICultureName: "en-US", currentCultureName: "en-US", clientServerTimeDelta: new Date("2016-07-30T20:00:12.6267597Z") - new Date(), siteClientTag: "1159$$15.0.4841.1000", crossDomainPhotosEnabled:false, webUIVersion:15, webPermMasks:{High:16,Low:196673},pageListId:"{73895ffe-9776-4928-b219-3dc68dcd539a}",pageItemId:6, pagePersonalizationScope:1, alertsEnabled:false, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};var dlc_fvsi = {"DefaultViews":[],"ViewUrls":[],"WebUrl":"\/about-shrm"};
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {

    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fabout-shrm\u002fPages\u002fPrivacy-Policy.aspx");

}
//]]>
</script>

<script src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(function(){

        if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
            return;
        }
        _spBodyOnLoadFunctions.push(function() 
        {
          ExecuteOrDelayUntilScriptLoaded(
            function()
            {
              var pairs = SP.ScriptHelpers.getDocumentQueryPairs();
              var followDoc, itemId, listId, docName;
              for (var key in pairs)
              {
                if(key.toLowerCase() == 'followdocument') 
                  followDoc = pairs[key];
                else if(key.toLowerCase() == 'itemid') 
                  itemId = pairs[key];
                else if(key.toLowerCase() == 'listid') 
                  listId = pairs[key];
                else if(key.toLowerCase() == 'docname') 
                  docName = decodeURI(pairs[key]);
              } 

              if(followDoc != null && followDoc == '1' && listId!=null && itemId != null && docName != null)
              {
                SP.SOD.executeFunc('followingcommon.js', 'FollowDocumentFromEmail', function() 
                { 
                  FollowDocumentFromEmail(itemId, listId, docName);
                });
              }

            }, 'SP.init.js');

        });
    })();(function(){

        if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
            return;
        }
        _spBodyOnLoadFunctions.push(function() {

            if (typeof(SPClientTemplates) === 'undefined' || SPClientTemplates === null || (typeof(APD_InAssetPicker) === 'function' && APD_InAssetPicker())) {
                return;
            }

            var renderFollowFooter = function(renderCtx,  calloutActionMenu)
            {
                if (renderCtx.ListTemplateType == 700) 
                    myDocsActionsMenuPopulator(renderCtx, calloutActionMenu);
                else
                    CalloutOnPostRenderTemplate(renderCtx, calloutActionMenu);

                var listItem = renderCtx.CurrentItem;
                if (typeof(listItem) === 'undefined' || listItem === null) {
                    return;
                }
                if (listItem.FSObjType == 0) {
                    calloutActionMenu.addAction(new CalloutAction({
                        text: Strings.STS.L_CalloutFollowAction,
                        tooltip: Strings.STS.L_CalloutFollowAction_Tooltip,
                        onClickCallback: function (calloutActionClickEvent, calloutAction) {
                            var callout = GetCalloutFromRenderCtx(renderCtx);
                            if (!(typeof(callout) === 'undefined' || callout === null))
                                callout.close();
                            SP.SOD.executeFunc('followingcommon.js', 'FollowSelectedDocument', function() { FollowSelectedDocument(renderCtx); });
                        }
                    }));
                }
            };

            var registerOverride = function(id) {
                var followingOverridePostRenderCtx = {};
                followingOverridePostRenderCtx.BaseViewID = 'Callout';
                followingOverridePostRenderCtx.ListTemplateType = id;
                followingOverridePostRenderCtx.Templates = {};
                followingOverridePostRenderCtx.Templates.Footer = function(renderCtx) {
                    var  renderECB;
                    if (typeof(isSharedWithMeView) === 'undefined' || isSharedWithMeView === null) {
                        renderECB = true;
                    } else {
                        var viewCtx = getViewCtxFromCalloutCtx(renderCtx);
                        renderECB = !isSharedWithMeView(viewCtx);
                    }
                    return CalloutRenderFooterTemplate(renderCtx, renderFollowFooter, renderECB);
                };
                SPClientTemplates.TemplateManager.RegisterTemplateOverrides(followingOverridePostRenderCtx);
            }
            registerOverride(101);
            registerOverride(700);
        });
    })();if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();function WebForm_OnSubmit() {
UpdateFormDigest('\u002fabout-shrm', 1440000);if (typeof(_spFormOnSubmitWrapper) != 'undefined') {return _spFormOnSubmitWrapper();} else {return true;};
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F1BE4B75" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdACZuiGsbZzQbeCHhqQm5EpG8DQe9fvP9DoKx2GZjwiNT9xN0urwtbn33IEuLRzZlwHdiQnbsuM04vfT4CVkSAZltLasAR3wREc4EcLF0Pro8QO5Fg131G2Ej4kf1ENY/CDfV0+SCk+IopJUudDq/zGZgSV9KraUJ6uxsMF++AwBnEI64o5ak7pkzTIFx27zMiV1te4KUb8Fctj2zg9yb4Td7yv1cS9k2kqIcebcstbcB5ceP0qmBabi0O7O9WDtCxHA0dH+vEOmG5j2Qi0taTnLJ5+HS1tixjnF/o9Hno0ff4dj2g+PR8XhsVzdU2zTlDxFgrGYX4VMTuaT5tvQ15yMP2tFYN8RC/lbig3d4Gemc0Fp+n2tVPbzcsDJ84A4kKdSKT/EgeipEtn7+aAQNO9PL1WDlmmeKVdHQbdEfzU16EwfQIVpENndvhV4EIqM/UK5U0o06BdeGTDGvPig418/OxrvFFfhRa8vq0grEw6x4Okhe6qflqsYHcU5oNEBrkwXgTET1Ec/Sgmy4vX3BAlZwvGItlnJIgcxmKzcmVUPApJfvQ+xZvYZj1tZ6lm4FhVqojGUzr81wDZaV4tmhKqfcobiDFwfG+WlX8uDmE+rgnRGN5718eraFQHM2IrnC6dXy54dwcqv+C6KO6Z9Qd1Djv+pabzHvrxVra860l5d7fXqJUCg1bnIDZg41UvVy0E/6S6Oz8AaBMTMSGJDKbCOAB60chXRJGvkPg36XgUTzL3J2nHItex3eU8HeJuEYqvkbgsVjZbZiyL93stS8honWDpq5ba0GtKxuw81z8mnJklHk4wB+E+wMQeTN9c+edSWcUTMDjBRZblb/xivFCrPH" />
</div>
			<script type="text/javascript"> var submitHook = function () { return false; }; theForm._spOldSubmit = theForm.submit; theForm.submit = function () { if (!submitHook()) { this._spOldSubmit(); } }; </script>
			
			<!-- End - Ribbon -->
			<input type="hidden" name="ctl00$ctl71$hf_SearchHost" id="ctl00_ctl71_hf_SearchHost" value="https://shrmsearch.shrm.org" />
<input type="hidden" name="ctl00$ctl71$hf_SiteSearch" id="ctl00_ctl71_hf_SiteSearch" value="store.shrm.org" />
<input type="hidden" name="ctl00$ctl71$hf_SiteCollection" id="ctl00_ctl71_hf_SiteCollection" value="prod_kafka" />
<input type="hidden" name="ctl00$ctl71$hf_SearchClient" id="ctl00_ctl71_hf_SearchClient" value="2016_shrm_org" />
<input type="hidden" name="ctl00$ctl71$hf_SearchDisableSectionCounts" id="ctl00_ctl71_hf_SearchDisableSectionCounts" value="1" />

            <input type="hidden" id="hf_Cloudinary_SelectedObject" />
            <input type="hidden" id="hf_Cloudinary_SelectedCaret" />
            <input type="hidden" id="hf_Cloudinary_SelectedCaretParent" />
			<!-- Start - Working area (to be use as-is, but you are free to add some classes) -->
			<div id="s4-workspace" class="ms-core-overlay">
				<div id="s4-bodyContainer">
					<div id="contentRow">
						<div id="contentBox" aria-live="polite" aria-relevant="all">
  							<div id="notificationArea" class="ms-notif-box"></div>
							<div id="DeltaPageStatusBar">
	
								<div id="pageStatusBar"></div>
							
</div>
							<div id="DeltaPlaceHolderMain">
	
								<a id="mainContent" name="mainContent" tabindex="-1"></a>
								<!-- -----------------------------------	SHRM SITE START	---------------------------------------------------------->	
								<!-- TEMP SWITCH ROLE BTN :: PRESENTATION ONLY :: TO BE REMOVED	--> 
								<!--a id="switchRole" class="btn btn-default" style="position:fixed; z-index:2" data-toggle="tooltip" data-placement="right" title="Switch membership status"><i class="fa fa-random"></i></a-->
								<!--/ TEMP SWITCH ROLE BTN :: PRESENTATION ONLY :: TO BE REMOVED	--> 
								
								<div class="shrm-site">
									
                                    
								    <!-- --------------------------------- SHRM MEMBER NAVIGATION	CONTAINER STARTS	-->
								    
<input type="hidden" name="ctl00$ctl72$AutoExpandDelaySeconds" id="AutoExpandDelaySeconds" value="6" />
<input type="hidden" name="ctl00$ctl72$AutoExpandDisableHours" id="AutoExpandDisableHours" value="24" />

								    <!-- --------------------------------- SHRM HEADER STARTS	-->
								    <input type="hidden" name="ctl00$ctl73$SSOUUIDHidden" id="SSOUUIDHidden" />
<input type="hidden" name="ctl00$ctl73$EmailHidden" id="EmailHidden" />
<input type="hidden" name="ctl00$ctl73$MemberIdHidden" id="MemberIdHidden" />
<input type="hidden" name="ctl00$ctl73$MemberStatusHidden" id="MemberStatusHidden" value="0" />
<input type="hidden" name="ctl00$ctl73$AuthTokenHidden" id="AuthTokenHidden" />
<input type="hidden" name="ctl00$ctl73$MySHRMAPITokenHidden" id="MySHRMAPITokenHidden" value="oEO1eL4WrNinzOo8ph072FlCgX8tkROvvWIofjAidT5yiz6uyimoGXpiGVUbNMtarI0yRlTtE5sxldznRDUu3-fVr5Nmu9Ow6MY5SF_fy2pxbX2RwO5jKH_NyrgrtTCgLyYvzFHoIrG_GNpMLAU3J-RCTQgfNdIYy-wQhYQpofXhI5YoTvwOUXIv7Ir_XcfXvPmnohoOBiut7pLRdIaBsn52JgulgSPPZdvU-a9UOY52JT0gCLuZVa1oOznyxDKk1cYX1u0SrTxImalSd10sKuYbYdN7OeM-hPjoSVHLFbuABVNZ4u7TQnNJIvVPpNNOFkJbMvpGyeiJnHHio4uzsIlHH6BxIvgCwW4ZkQ96hHtnSi2tCTrG0c20fpBCoHaWSCkQuSDrBjot0Hs6DjY-l_aVQeMXWzdbDgw_yPiDeFDg3wOqiVzANCj9mPJMSwzLtZGR7yDx4QtCXF5wflTkdLcNTN9LT8ORdgg0DZwl_DU" />
<input type="hidden" name="ctl00$ctl73$GreetingNameHidden" id="GreetingNameHidden" />

<header class="container container-header hidden-xs">
    <div class="row">
        <div class="top-bar clearfix">
            <div class="header-brand pull-left"></div>
            <span class="header-brand pull-left"><a href="/pages/default.aspx">
                <img class="img-responsive" src="/_layouts/15/SHRM.Core/design/images/SHRMLogo.svg" /></a>
            </span>
            <div id="ctl00_ctl73_nesto" class="header-nav pull-right">
                <ul class="utility-links text-right">
                    <li class="dropdown">
                        
                    </li>
                    <li>
                        <div id="ctl00_ctl73_SignInPanel">
		
                            <a href="https://www.shrm.org/_layouts/authenticate.aspx?source=/about-shrm/Pages/Privacy-Policy.aspx">Sign In</a> |
                        
	</div>
                    </li>
                    <li class="dropdown">
                        <a id="language-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe">&nbsp;</i><i class="fa fa-angle-down"></i>
                            <span id="language-id">U.S. - EN</span></a> |
                        <ul class="dropdown-menu" aria-labelledby="language-options">
                            <li><a href="https://www.shrm.org">SHRM GLOBAL</a></li>
                            <li><a href="/about-shrm/Pages/SHRM-China.aspx">SHRM China</a></li>
                            <li><a href="https://www.shrm.org/india">SHRM India</a></li>
                            <li><a href="/about-shrm/Pages/SHRM-MENA.aspx">SHRM MENA</a></li>
                            <li class="divider"></li>
                            <li class="language-switch" data-language="en"><a href="javascript:void(0)">English</a></li>
                            <li class="language-switch" data-language="fr"><a href="javascript:void(0)">French</a></li>
                            <li class="language-switch" data-language="de"><a href="javascript:void(0)">German</a></li>
                            <li class="language-switch" data-language="pt"><a href="javascript:void(0)">Portuguese</a></li>
                            <li class="language-switch" data-language="es"><a href="javascript:void(0)">Spanish</a></li>
                        </ul>
                    </li>
                    <li><a href="/ResourcesAndTools/tools-and-samples/Pages/HR-Help.aspx">HR Help</a> | </li>
                    <li><a href="https://store.shrm.org/">SHRM Store</a></li>
                    <li><a href="https://store.shrm.org/checkout/cart/"><span class="shop-items"><span id="cartItemCount" style="display:none;"></span></span></a></li>
                </ul>
                <div class="header-ctas text-right">
                    <div class="btn btn-success btn-lg btn-cta">
                        <a id="ctl00_ctl73_MembershipCallToActionLink" href="/about-shrm/pages/membership.aspx">Membership</a>
                    </div>
                    <div class="btn btn-success-dark btn-lg btn-cta">
                        <a id="ctl00_ctl73_CertificationCallToActionLink" href="/certification/pages/default.aspx">GET CERTIFIED</a>                        
                    </div>
                </div>
            </div>
        </div>
        <!--/ .top-bar	-->
        <div class="header-bar">
        </div>
        <!--/ .header-bar	-->
    </div>
</header>
<!--/ .row-header	-->
								  
								    <!-- --------------------------------- SHRM NAVIGATION STARTS	-->  
								 	<div class="container-fluid container-nav">
                                        <div ui-view=""></div>
                                        <a id="cloudinary-open" ui-sref="start()"></a>
                                        <a id="cloudinary-open-wideimage" ui-sref="start({ only : ['21x9'] , width : '1920'})"></a>
									  	

<nav class="navbar navbar-default row">
    <div class="container">
        <div class="mobile-header">
            <div class="mobile-control">
                <div>
                    <button type="button" class="navbar-toggle navbar-inverse pull-left collapsed" data-toggle="collapse" data-target="#site-main-nav" aria-expanded="false" aria-controls="site-main-nav">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                </div>
                <div class="mobile-logo"><span class="header-brand visible-xs"><a href="/" class="mobile-logo">
                    <img src="/_layouts/15/SHRM.Core/design/images/SHRMLogo.svg"></a></span>
                </div>
                <div>
                    <a id="main-search" class="btn btn-danger pull-right visible-xs" role="button" data-toggle="collapse" href="#mobile-search-bar-row" aria-expanded="false" aria-controls="search-bar-row"><i class="fa fa-search"></i></a>
                    <a id="main-search" class="btn btn-danger pull-right hidden-xs" role="button" data-toggle="collapse" href="#search-bar-row" aria-expanded="false" aria-controls="search-bar-row"><i class="fa fa-search"></i></a>
                </div>
            </div>
            <div class="site-tagline visible-xs">
                <div>Society For Human Resource Management</div>
            </div>
        </div>
        <!--/ .mobile-header	-->

        <!--	collapsable search bar for mobile affixed navigation	-->
        <div class="row search-bar-row text-center collapse" id="mobile-search-bar-row">
            <div class="container visible-xs">
              <div class="search-bar-group">
                <div class="dropdown hidden-xs">
                  <button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary">
                    <span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
                    <li><a href="#">SHRM Foundation</a></li>
                    <li><a href="#">SHRM India</a></li>
                    <li><a href="#">SHRM China</a></li>
                  </ul>
                </div><!--/ .dropdown	-->
                <div class="search-bar-widget search-bar-xs-container">
                <!--window.location.href = '/search/Pages/default.aspx'-->
                  <a class="searchSubmitButton" role="button" 
                  onclick="window.location.href = '/search/Pages/default.aspx?k=' + $(this).next().val(); "></a>
                  <input type="text" class="form-control" id="search-bar-main" 
                  onkeydown="if(event.keyCode == 13){window.location.href = '/search/Pages/default.aspx?k=' + $(this).val();}" />          
                  <div class="search-bar-widget-results"></div>
                </div><!-- /.search-bar-lg-container	-->
                
              </div><!--	/.search-bar-group	-->
            </div><!--	/.container	-->
        </div> <!--/ .search-bar-row	-->
        <!--/	collapsable search bar for affixed navigation	-->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="site-main-nav" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                
                        <li onclick="" class="dropout">
                            <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl00_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl00_pan_DropoutMenu' data-toggle='collapse' role='button'>HR Today</a>
                            <div id="ctl00_ctl74_rep_Navigation_ctl00_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl00_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl00_pan_DropoutMenu' data-toggle='collapse' role='button'>HR Today</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>News</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>News</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/hr-today/news/hr-news/pages/default.aspx">HR News</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/news/hr-magazine/0716/pages/default.aspx">HR Magazine</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="http://blog.shrm.org/">SHRM Blog</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Public Policy</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Public Policy</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="http://www.advocacy.shrm.org/home">Take Action</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/public-policy/hr-public-policy-issues/pages/default.aspx">HR Public Policy Issues</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl02_hl_Level3_Link" href="http://www.advocacy.shrm.org/about">A-Team Advocacy Network</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl03_hl_Level3_Link" href="/hr-today/public-policy/state-affairs/pages/default.aspx">State Affairs</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Trends & Forecasting</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Trends & Forecasting</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/hr-today/trends-and-forecasting/research-and-surveys/pages/default.aspx">Research & Surveys</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/trends-and-forecasting/labor-market-and-economic-data/pages/default.aspx">Labor Market & Economic Data</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/hr-today/trends-and-forecasting/special-reports-and-expert-views/pages/default.aspx">Special Reports & Expert Views</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl74_rep_Navigation_ctl00_ctl01_pan_Main" class="column col-md-3 col-featured visible-md visible-lg">
			
    <div class="siteMenuColHeader">HR Magazine: July/August '16</div>
    <a id="ctl00_ctl74_rep_Navigation_ctl00_ctl01_hl_Image" href="https://www.shrm.org/hr-today/news/hr-magazine/0716/pages/default.aspx"><img id="ctl00_ctl74_rep_Navigation_ctl00_ctl01_img_Image" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_338%2cw_600%2cx_0%2cy_68/c_fit%2cw_767/v1/Magazine/2016_JulyAug_cbdqj2?databtoa=eyIxeDEiOnsieCI6MCwieSI6MCwieDIiOjYwMCwieTIiOjYwMCwidyI6NjAwLCJoIjo2MDB9LCIxNng5Ijp7IngiOjAsInkiOjY4LCJ4MiI6NjAwLCJ5MiI6NDA2LCJ3Ijo2MDAsImgiOjMzOH19" /></a>
    <h6>
        <a id="ctl00_ctl74_rep_Navigation_ctl00_ctl01_hl_Title" href="https://www.shrm.org/hr-today/news/hr-magazine/0716/pages/default.aspx">How to Prep for the Overtime Rule</a>
    </h6>
    <p>No HR professional is exempt from the planning.</p>
    <a id="ctl00_ctl74_rep_Navigation_ctl00_ctl01_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://www.shrm.org/hr-today/news/hr-magazine/0716/pages/default.aspx">Read more</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 visible-md visible-lg"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl01_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl01_pan_DropoutMenu' data-toggle='collapse' role='button'>Resources & Tools</a>
                            <div id="ctl00_ctl74_rep_Navigation_ctl01_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl01_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl01_pan_DropoutMenu' data-toggle='collapse' role='button'>Resources & Tools</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>HR Topics</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>HR Topics</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/resourcesandtools/hr-topics/behavioral-competencies/pages/default.aspx">Behavioral Competencies</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/resourcesandtools/hr-topics/benefits/pages/default.aspx">Benefits</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="/resourcesandtools/hr-topics/pages/california-resources.aspx">California Resources</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/resourcesandtools/hr-topics/compensation/pages/default.aspx">Compensation</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="/resourcesandtools/hr-topics/employee-relations/pages/default.aspx">Employee Relations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl05_hl_Level3_Link" href="/resourcesandtools/hr-topics/global-hr/pages/default.aspx">Global HR</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl06_hl_Level3_Link" href="/resourcesandtools/hr-topics/labor-relations/pages/default.aspx">Labor Relations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl07_hl_Level3_Link" href="/resourcesandtools/hr-topics/organizational-and-employee-development/pages/default.aspx">Organizational & Employee Development</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl08_hl_Level3_Link" href="/resourcesandtools/hr-topics/risk-management/pages/default.aspx">Risk Management</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl09_hl_Level3_Link" href="/resourcesandtools/hr-topics/talent-acquisition/pages/default.aspx">Talent Acquisition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl10_hl_Level3_Link" href="/resourcesandtools/hr-topics/technology/pages/default.aspx">Technology</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Legal & Compliance</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Legal & Compliance</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="/resourcesandtools/legal-and-compliance/employment-law/pages/default.aspx">Employment Law</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/resourcesandtools/legal-and-compliance/state-and-local-updates/pages/default.aspx">State & Local Updates</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl02_hl_Level3_Link" href="http://cfgi.org/">Immigration Employer Network</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Business Solutions</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Business Solutions</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/resourcesandtools/business-solutions/pages/benchmarking-service.aspx">Benchmarking Service</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/resourcesandtools/business-solutions/pages/diversity-hiring-solutions.aspx">Diversity Hiring Solutions</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/resourcesandtools/business-solutions/pages/employee-engagement-survey-service.aspx">Employee Engagement Survey</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl03_hl_Level3_Link" href="/learningandcareer/learning/onsite-training/pages/default.aspx">Onsite Training</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl04_hl_Level3_Link" href="/resourcesandtools/business-solutions/pages/salary-data-service.aspx">Salary Data Service</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl05_hl_Level3_Link" href="/resourcesandtools/business-solutions/pages/survey-research-services.aspx">Survey Research Services</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl06_hl_Level3_Link" href="https://tac.shrm.org/">Talent Assessment Center</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl07_hl_Level3_Link" href="http://vendordirectory.shrm.org/">Vendor Directory</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Tools & Samples</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Tools & Samples</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/pages/employee-handbooks.aspx">Employee Handbooks</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/pages/express-requests.aspx">Express Requests</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl02_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/how-to-guides/pages/default.aspx">How-To Guides</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl03_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/hr-forms/pages/default.aspx">HR Forms</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl04_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/hr-qa/pages/default.aspx">HR Q&As</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl05_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/interview-questions/pages/default.aspx">Interview Questions</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl06_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/job-descriptions/pages/default.aspx">Job Descriptions</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl07_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/policies/pages/default.aspx">Policies</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl08_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/presentations/pages/default.aspx">Presentations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl09_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/pages/spreadsheets.aspx">Spreadsheets</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl10_hl_Level3_Link" href="/resourcesandtools/tools-and-samples/toolkits/pages/default.aspx">Toolkits</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl74_rep_Navigation_ctl01_ctl01_pan_Main" class="column col-md-3 col-featured visible-md visible-lg">
			
    <div class="siteMenuColHeader">Employee Handbook</div>
    <a id="ctl00_ctl74_rep_Navigation_ctl01_ctl01_hl_Image" href="https://store.shrm.org/employee-handbook.html"><img id="ctl00_ctl74_rep_Navigation_ctl01_ctl01_img_Image" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_494%2cw_880%2cx_229%2cy_0/c_fit%2cw_767/v1/Marketing/EBH_StoreMarquee_2_wri6by?databtoa=eyIxNng5Ijp7IngiOjIyOSwieSI6MCwieDIiOjExMDksInkyIjo0OTQsInciOjg4MCwiaCI6NDk0fX0%3d" /></a>
    <h6>
        <a id="ctl00_ctl74_rep_Navigation_ctl01_ctl01_hl_Title" href="https://store.shrm.org/employee-handbook.html">Is Your Employee Handbook Keeping Up? </a>
    </h6>
    <p>Take the work out of creating and maintaining an employee handbook.</p>
    <a id="ctl00_ctl74_rep_Navigation_ctl01_ctl01_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://store.shrm.org/employee-handbook.html">Customize Yours</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 visible-md visible-lg"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl02_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl02_pan_DropoutMenu' data-toggle='collapse' role='button'>Learning & Career</a>
                            <div id="ctl00_ctl74_rep_Navigation_ctl02_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl02_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl02_pan_DropoutMenu' data-toggle='collapse' role='button'>Learning & Career</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Learning</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>Learning</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/learningandcareer/learning/pages/seminars.aspx">Seminars</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/learningandcareer/learning/onsite-training/pages/default.aspx">Onsite Training</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="https://store.shrm.org/education/elearning.html">eLearning</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/learningandcareer/learning/pages/essentials-of-hr-management.aspx">Essentials of HR Management</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="/learningandcareer/learning/webcasts/pages/default.aspx">Webcasts</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Certification</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Certification</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="https://www.shrm.org/certification/apply/pages/default.aspx">Apply for Exam</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="https://www.shrm.org/certification/learning/pages/default.aspx">Certification Preparation</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_rep_Navigation3_ctl02_hl_Level3_Link" href="https://www.shrm.org/certification/faqs/pages/default.aspx">SHRM Certification FAQs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl01_rep_Navigation3_ctl03_hl_Level3_Link" href="https://www.shrm.org/certification/recertification/pages/default.aspx">Recertification</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Competency Model</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Competency Model</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/learningandcareer/competency-model/pages/default.aspx">SHRM Competency Model</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/learningandcareer/competency-model/pages/competency-faqs.aspx">Competency FAQs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/learningandcareer/competency-model/pages/competency-diagnostic-tools.aspx">Competency Diagnostic Tools</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Student Development</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Student Development</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="https://www.shrm.org/academicinitiatives/pages/default.aspx">Academic Initiatives</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="https://www.shrm.org/academicinitiatives/aol/pages/default.aspx">Graduation Exam for Students</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl02_hl_Level3_Link" href="/about/foundation/scholarships/pages/ags.asp.aspx">Scholarships</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl03_hl_Level3_Link" href="/communities/student-resources/pages/default.aspx">Student Member Center</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>HR Jobs</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-toggle='collapse' role='button'>HR Jobs</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl00_hl_Level3_Link" href="http://www.shrm.org">uccHrJobs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl01_hl_Level3_Link" href="http://jobs.shrm.org/">Browse All Jobs...</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl02_hl_Level3_Link" href="http://hrjobs.shrm.org/jobs/products">Post a Job</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl74_rep_Navigation_ctl02_ctl01_pan_Main" class="column col-md-3 col-featured visible-md visible-lg">
			
    <div class="siteMenuColHeader">SHRM Seminars</div>
    <a id="ctl00_ctl74_rep_Navigation_ctl02_ctl01_hl_Image" href="https://store.shrm.org/education/seminars/find-a-seminar.html?location%5b%5d=san+francisco%2c+ca&amp;delivery_method%5b%5d=public+in-person"><img id="ctl00_ctl74_rep_Navigation_ctl02_ctl01_img_Image" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_1526%2cw_2715%2cx_0%2cy_166/c_fit%2cw_767/v1/Marketing/iStock_93093433_LARGE_a3lza7?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjE2NiwieDIiOjI3MTUsInkyIjoxNjkyLCJ3IjoyNzE1LCJoIjoxNTI2fX0%3d" /></a>
    <h6>
        <a id="ctl00_ctl74_rep_Navigation_ctl02_ctl01_hl_Title" href="https://store.shrm.org/education/seminars/find-a-seminar.html?location%5b%5d=san+francisco%2c+ca&amp;delivery_method%5b%5d=public+in-person">HR Education in San Francisco</a>
    </h6>
    <p>SHRM Seminars will host HR education every month in San Francisco this fall! Select the program that meets both your scheduling and development needs.</p>
    <a id="ctl00_ctl74_rep_Navigation_ctl02_ctl01_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://store.shrm.org/education/seminars/find-a-seminar.html?location%5b%5d=san+francisco%2c+ca&amp;delivery_method%5b%5d=public+in-person">Register Now</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 visible-md visible-lg"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl03_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl03_pan_DropoutMenu' data-toggle='collapse' role='button'>Events</a>
                            <div id="ctl00_ctl74_rep_Navigation_ctl03_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl03_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl03_pan_DropoutMenu' data-toggle='collapse' role='button'>Events</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>SHRM Events</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>SHRM Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="http://annual.shrm.org/">SHRM Annual Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="http://conferences.shrm.org/diversity-conference">Diversity & Inclusion Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="https://conferences.shrm.org/legislative-conference">Employment Law & Legislative Conference</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="https://conferences.shrm.org/leadership-development-forum">Leadership Development Forum</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="http://conferences.shrm.org/talent-conference">Talent Management Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl05_hl_Level3_Link" href="https://conferences.shrm.org/shrm-talent-symposium">Talent Symposium</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl06_hl_Level3_Link" href="/events/pages/state--affiliate-conferences.aspx">State & Affiliate Conferences</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl07_hl_Level3_Link" href="/learningandcareer/learning/webcasts/pages/default.aspx">Webcasts</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl08_hl_Level3_Link" href="/events/pages/default.aspx">SEE ALL EVENTS</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Event Resources</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Event Resources</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="http://niftytechnologies.net/shrm3/index.php">Request a Brochure</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/events/pages/speaker-information.aspx">Speaker Information</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl02_hl_Level3_Link" href="http://conferences.shrm.org/exhibit-or-sponsor">Sponsorship & Exhibitor Information</a></li>
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Global Events</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Global Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="http://www.shrmiac.org/">SHRM India Annual Conference</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Affiliate Events</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Affiliate Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="https://www.cfgi.org/symposium">CFGI Symposium</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl03_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="http://www.hrps.org/mpage/2016forumhome/">Strategic HR Forum</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl74_rep_Navigation_ctl03_ctl01_pan_Main" class="column col-md-3 col-featured visible-md visible-lg">
			
    <div class="siteMenuColHeader">Leadership Development Forum</div>
    <a id="ctl00_ctl74_rep_Navigation_ctl03_ctl01_hl_Image" href="https://conferences.shrm.org/leadership-development-forum"><img id="ctl00_ctl74_rep_Navigation_ctl03_ctl01_img_Image" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_2764%2cw_4914%2cx_49%2cy_1167/c_fit%2cw_767/v1/Marketing/HiRes_hh7ouz?databtoa=eyIxNng5Ijp7IngiOjQ5LCJ5IjoxMTY3LCJ4MiI6NDk2MiwieTIiOjM5MzEsInciOjQ5MTQsImgiOjI3NjR9fQ%3d%3d" /></a>
    <h6>
        <a id="ctl00_ctl74_rep_Navigation_ctl03_ctl01_hl_Title" href="https://conferences.shrm.org/leadership-development-forum">Seattle, Washington</a>
    </h6>
    <p>Join us, September 27 - 28.</p>
    <a id="ctl00_ctl74_rep_Navigation_ctl03_ctl01_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://conferences.shrm.org/leadership-development-forum">Register Today</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 visible-md visible-lg"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl04_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl04_pan_DropoutMenu' data-toggle='collapse' role='button'>Communities</a>
                            <div id="ctl00_ctl74_rep_Navigation_ctl04_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl04_pan_DropoutMenu' href='#ctl00_ctl74_rep_Navigation_ctl04_pan_DropoutMenu' data-toggle='collapse' role='button'>Communities</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Communities</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>Communities</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="http://community.shrm.org/">SHRM Connect</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/communities/communities/chapters/pages/default.aspx">Chapters</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="http://www.hrps.org/">Executive Network</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/communities/communities/hr-young-professionals/pages/default.aspx">HR Young Professionals</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="http://www.advocacy.shrm.org/about">Legislative Advocacy Team (A-Team)</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Student Resources</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Student Resources</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="/communities/student-resources/pages/default.aspx">Student Member Center</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a disabled='' aria-controls='ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Volunteers</a>
                                                <ul id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' href='#ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Volunteers</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/communities/volunteers/membership-councils/pages/default.aspx">Membership Councils</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/communities/volunteers/special-expertise-panels/pages/default.aspx">Special Expertise Panels</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="http://community.shrm.org/vlrc/">Volunteer Leader Resource Center</a></li>
                                                        
                                                            <li><a id="ctl00_ctl74_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl03_hl_Level3_Link" href="/communities/volunteers/pages/volunteer-opportunities.aspx">Volunteer Opportunities</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    

                                    <div class="place-for-local-chapter column col-md-3 visible-md visible-lg"></div>
                                </div>
                            
	</div>
                        </li>
                    
            </ul>
            

<div class="mobile-quick-access visible-xs">
	<div class="mobile-utility-links text-center">    
        <div>
            <a href="/ResourcesAndTools/tools-and-samples/Pages/HR-Help.aspx">HR Help</a>
        </div>
        
        
        <div id="ctl00_ctl74_HeaderMobile_SignInPanel">
		
            <a href="/_layouts/authenticate.aspx">Sign In</a>
        
	</div>
	</div><!--/	.mobile-utility-links	-->
	<div class="mobile-ctas text-center">
        <!-- Role All	-->
        <a href="https://store.shrm.org" target="_blank" class="btn btn-success btn-lg btn-cta">
		    	<span>SHRM Store</span>
        </a>
        <div id="ctl00_ctl74_HeaderMobile_MembershipCallToActionNonMemberPanel" class="btn btn-success btn-lg btn-cta">
		
            <a href="https://membership.shrm.org">Membership</a>
        
	</div>
        
        <div id="ctl00_ctl74_HeaderMobile_CertificationCallToActionPanel" class="btn btn-success-dark btn-lg btn-cta">
		
            <a href="https://portal.shrm.org/"><span id="ctl00_ctl74_HeaderMobile_CertificationCallToActionLabel">GET CERTIFIED</span></a>
        
	</div>        
	</div><!--/	.mobile-ctas	-->
</div><!--/ .mobile-quick-access	--> 

        </div>
    </div>
    <div id="ucc_HRJobs" style="display: none;">
        
<div class="shrm-widget shrm-job-finder">
    <h3 class="shrm-Title-Small text-uppercase text-center">Job Finder</h3>
    <h5 class="text-center">Find an HR Job Near You</h5>
    <div class="finder-form">
        <div class="input-holder">
            <input name="ctl00$ctl74$HrJobs$searchTerm" type="text" id="ctl00_ctl74_HrJobs_searchTerm" placeholder="CITY, STATE, ZIP" class="form-control" />
        </div>
        <a href="javascript:" id="ctl00_ctl74_HrJobs_searchJobs" class="btn btn-success btn-lg btn-cta" onclick="return OpenInNewWindow(&#39;ctl00_ctl74_HrJobs_searchJobs&#39;,&#39;ctl00_ctl74_HrJobs_searchTerm&#39;);"><span class="status">Search Jobs</span></a>
    </div>
</div>
<script language="javascript" type="text/javascript">
    function OpenInNewWindow(anchorTagId, textBoxTagId) {
        var searchTerm = document.getElementById(textBoxTagId);
        var searchJobs = document.getElementById(anchorTagId);
        searchJobs.setAttribute("href", "javascript:");
        searchJobs.setAttribute("target", "");

        if (searchTerm.value.length > 0) {
            searchJobs.setAttribute("target", "_blank");
            var href = "http://hrjobs.shrm.org/jobs/search/results?radius=0&location=" + searchTerm.value;
            searchJobs.setAttribute("href", href);
        }
    }
		$(function() {
			$uccHrJobsHtml = $('#ucc_HRJobs');
			$uccHrJobsPlaceholder = $('nav.navbar li a').filter(function(){
				 return $(this).text() === "uccHrJobs";
			});
			
			if( $uccHrJobsHtml.length > 0 && $uccHrJobsPlaceholder.length > 0){
				$('.shrm-job-finder',$uccHrJobsHtml).addClass('shrm-mini-job-finder');
				$($uccHrJobsPlaceholder).replaceWith( $($uccHrJobsHtml).show());
			}
		});
</script>

    </div>
    <div id="ucc_LocalChapter" style="display:none;">
        


<div class="shrm-job-finder shrm-widget">
    <h3 class="shrm-Title-Small text-uppercase text-center siteMenuColHeader">LOCAL CHAPTERS</h3>
    <h5 class="text-center">Find chapters in your area</h5>
    <div class="finder-form">
  	    <div class="input-holder">
              <input type="text" id="chapterLocate"  placeholder="CITY, STATE, ZIP"  class="form-control chapter-form-control" data-onload="chapterLocatorScript(this, new Date().getTime())" />
  	    </div>
        <a id="ctl00_ctl74_ctl00_hl_FindChapters" class="btn btn-success btn-lg btn-cta" onclick="window.location=&#39;/search/pages/LocalChapter.aspx?location=&#39; + $(this).closest(&#39;.finder-form&#39;).find(&#39;input&#39;).val()" href="javascript:">
            <span class="status">Find Chapters</span>
        </a>
    </div>
	
</div><!--	/.shrm-job-finder	-->

    </div>
</nav>


									    <!--	collapsable search bar for tablet/desktop affixed navigation	-->
									    <div class="row search-bar-row text-center collapse" id="search-bar-row">
									        <div class="container hidden-xs">
														<div class="search-bar-group">
															<div class="dropdown hidden-xs">
																<button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-cta">
																	<span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
																</button>
																<ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
																	<li><label><input type="checkbox" value="" checked><span>Entire Site</span></label></li>
																	<li><label><input type="checkbox" value="des-www.shrm.org/hranswers"><span>HR Help</span></label></li>
																	<li><label><input type="checkbox" value="stg-kafka-www/hrdisciplines"><span>News</span></label></li>
																	<li><label><input type="checkbox" value="stg-kafka-www/legalissues"><span>Legal</span></label></li>
																	<li><label><input type="checkbox" value="stg-kafka-www/templatestools"><span>Tools</span></label></li>
																	<li><label><input type="checkbox" value="stg-kafka-www/education"><span>Learning</span></label></li>
																	<li><label><input type="checkbox" value="conferences.shrm.org"><span>Events</span></label></li>
																	<li><label><input type="checkbox" value="stg-kafka-www/communities"><span>Communities</span></label></li>
																	<li><label><input type="checkbox" value="store.shrm.org"><span>Store</span></label></li>
																</ul>
															</div><!--/ .dropdown	-->
															<div class="search-bar-widget search-bar-lg-container">
																<a class="searchSubmitButton" role="button" href="javascript"></a>
           											<input type="text" class="form-control" id="search-bar-main" />           
																<div class="search-bar-widget-results"></div>
															</div><!-- /.search-bar-lg-container	-->
														</div>
													</div>
									    </div><!--/ .search-bar-row	-->
									    <!--/	collapsable search bar for affixed navigation	-->
									</div><!--/ .container-nav	-->
									<div class="affixed-nav-space-holder"></div>
									<div class="search-bar-row text-center hidden-xs">
									    <div class="container">
												<div class="search-bar-group">
													<div class="dropdown hidden-xs">
														<button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-cta">
															<span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
														</button>
														<ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
															<li><label><input type="checkbox" value="" checked><span>Entire Site</span></label></li>
															<li><label><input type="checkbox" value="stg-kafka-www/hranswers"><span>HR Help</span></label></li>
															<li><label><input type="checkbox" value="stg-kafka-www/hrdisciplines"><span>News</span></label></li>
															<li><label><input type="checkbox" value="stg-kafka-www/legalissues"><span>Legal</span></label></li>
															<li><label><input type="checkbox" value="stg-kafka-www/templatestools"><span>Tools</span></label></li>
															<li><label><input type="checkbox" value="stg-kafka-www/education"><span>Learning</span></label></li>
															<li><label><input type="checkbox" value="conferences.shrm.org"><span>Events</span></label></li>
															<li><label><input type="checkbox" value="stg-kafka-www/communities"><span>Communities</span></label></li>
															<li><label><input type="checkbox" value="store.shrm.org"><span>Store</span></label></li>
														</ul>
													</div><!--/ .dropdown	-->
													<div class="search-bar-widget search-bar-lg-container">
													<!--window.location.href = '/search/Pages/default.aspx'-->
														<a class="searchSubmitButton" role="button" href="javascript:"></a>
           									<input type="text" class="form-control" id="search-bar-main" />         
														<div class="search-bar-widget-results"></div>
													</div><!-- /.search-bar-lg-container	-->	
												</div> <!--	/.search-bar-group	-->     
											</div>
									</div><!--/ .search-bar-row	-->								  
									<!-- --------------------------------- SHRM NAVIGATION END - SP NAV	-->
									
								  	<!-- Start - Layout content -->
									
    <div class="container container-content">
        <div class="article-page">
            <div class="col-md-8">
                <div class="text-center">
                    
<button id="ctl00_PlaceHolderMain_ctl00_btn_Kicker" type="button" class="shrm-Kicker" onclick="window.location = &#39;https://www.shrm.org/about-shrm&#39;;">About SHRM</button>

                    <h2>
                        Privacy Policy
                    </h2>
                    <p class="shrm-Dek text-center">
                        
                    </p>
                </div>
                

<div id="hiddenvalue-author" class="hidden author">
    &#160;
</div>



                
                


                <div class="article-content">
                    <figure class="text-center article-cover-figure">
                        <div id="ctl00_PlaceHolderMain_ctl06_label" style='display:none'>Page Image</div><div id="ctl00_PlaceHolderMain_ctl06__ControlWrapper_RichImageField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_ctl06_label"><div class="ms-rtestate-field"></div></div>
                        <figcaption>
                            <div id="ctl00_PlaceHolderMain_ctl07_label" style='display:none'>Image Caption</div><div id="ctl00_PlaceHolderMain_ctl07__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_ctl07_label"></div>
                        </figcaption>
                    </figure>
                    <div id="ctl00_PlaceHolderMain_ctl08_label" style='display:none'>Page Content</div><div id="ctl00_PlaceHolderMain_ctl08__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_ctl08_label"><p> 
   <strong><span class="shrm-Style-NoDropCap">Privacy</span> Policy Updated December 14, 2015.</strong></p><p>Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource profession. &#160;</p><p>In Part I of this Privacy Statement we set forth SHRM’s privacy policy as to data and information which we collect online&#160; through 
   <a href="/">www.shrm.org</a> or other SHRM websites which link to this Privacy Statement from their site (collectively, “SHRM Websites”).&#160; In Part II of this Privacy Statement we set forth SHRM’s Privacy Policy as to data and information which we collect other than through SHRM Websites.&#160; 
   <strong>Your use of the SHRM Websites and/or provision of your personally identifiable information, sensitive personal information, or demographic information to SHRM constitutes your consent to the use, storage, processing and transfer of that information in accordance with this Privacy Policy.</strong> </p><p>SHRM is an organization that is based in the United States.&#160; We comply with U.S. laws governing the privacy of personal information, which may not provide the same level of data protection as provided under the laws of other countries.&#160; </p><p>The SHRM Foundation, a subsidiary of SHRM, has also adopted this Privacy Policy as the policy of the SHRM Foundation.&#160; 
   <br> </p><p class="shrm-Element-Subtitle"> PART I – SHRM WEBSITES PRIVACY STATEMENT</p><p>Throughout SHRM Websites, there are forms for visitors to request information, products, and services. These forms may ask for contact information (such as your home and/or work phone number, fax number, street address and/or e-mail address, employer name and job title), unique identifiers (usually your SHRM membership number, if one is available, or other log-in credential), financial information (such as your credit card number), or demographic information (for example, your birth year, location, certification designation, etc.). Contact information from these forms is used to provide the products, promotional materials, or memberships that you request. Forms on our site that request financial information do so in order to bill you for products or services ordered. Unique identifiers (specifically, your SHRM member number) are collected from website visitors to verify the user's identity for access to restricted content or features on the site. </p><p>This SHRM Privacy Policy discloses SHRM's privacy practices and contains detailed information about the following&#58; </p><ul><li>What information of yours do SHRM Websites collect?<br></li><li>What are &quot;cookies&quot; and how does SHRM use them?<br></li><li>What organization collects the information and who has access to it?<br></li><li>How does SHRM use the information it collects?<br></li><li>How can you modify your personal information?<br></li><li>What is the opt-out policy for SHRM Websites?<br></li><li>What types of security procedures are in place to protect against the loss, misuse or alteration of your information?<br></li><li>How do SHRM Websites use bulletin boards, discussion lists, and moderated chats?<br></li></ul><p>Questions or Complaints regarding this Privacy Statement should be directed to&#58; 
   <br> The Society for Human Resource Management<br> 1800 Duke Street 
   <br> Alexandria, Virginia, 22314 
   <br> 703-548-3440 or 800-283-7476<br><a href="mailto&#58;shrm@shrm.org">shrm@shrm.org</a> </p><p>SHRM Websites will update this policy from time to time, so please check back periodically. If at any point we decide to use personally identifiable information in a manner that is materially different from that stated at the time it was collected, we will endeavor to notify you of such changes (e.g., we will post a revised privacy policy with a new effective date, display the word “updated” next to the privacy policy link on each page on the SHRM Websites, or otherwise), prior to implementing them. </p><p> 
   <strong>Frequently-Asked Questions</strong></p><p> 
   <strong>1. What information of yours do SHRM Websites collect?</strong><br> Our goal is to become your destination for HR-related information by providing information, services, and product offerings that are most relevant to you in the most convenient way. To achieve this goal, SHRM Websites collect information about site visitors. </p><p>Information collected online is usually defined as being either anonymous or personally identifiable. </p><p>Anonymous information refers to data that cannot be tied back to a specific individual. SHRM collects some information each time a visitor comes to a SHRM website, so we can improve the overall quality of the visitor's online experience. For example, SHRM collects the visitor's IP address, browser, and platform type (e.g., Internet Explorer browser on a Windows platform). Gathering this data helps us to learn what browsers we need to support. Other anonymous information helps us determine what sections of SHRM Websites are most popular and how many visitors come to our site(s). You do not have to register with SHRM Websites before we can collect this anonymous information. </p><p>Personally identifiable information refers to data that tells us specifically who you are (e.g., your name, home and/or work email address, postal address, phone number and/or fax number, and your employer and job title, and if you are making a purchase, your credit card information). You are only required to provide such information if you want to take advantage of optional products and services provided through our website(s). We may also collect demographic information about you (e.g., birth year, certification designation, etc.).</p><p> SHRM collects personal information in the following ways from different parts of its website(s)&#58; </p><ul style="list-style-type&#58;disc;"><li> 
      <strong>SHRM Membership Applications</strong>&#58; You are sharing personally identifiable information (e.g., your name, home and/or work email address, postal address, phone number and/or fax number, and your employer and job title, etc., and if you are making a purchase, your credit card information) and demographic information about you (e.g., birth year, certification designation, etc.) with us when you join SHRM or renew your SHRM membership through our website(s). SHRM members will have SHRM membership log-in credentials which enable members to take advantage of restricted content and features on SHRM Websites.</li><li> 
      <strong>Other Registration</strong>&#58; When registering for specific services you may be asked for the same type of personally identifiable information (e.g., your name, home and/or work email address, postal address, phone number and/or fax number, and your employer and job title, etc., and if you are making a purchase, your credit card information) and demographic information about you (e.g., birth year, certification designation, etc., and in the case of certification applications, felony convictions).For example, if you subscribe to an e-mail newsletter, you will be asked to provide your e-mail address.</li><li> 
      <strong>Online Purchases</strong>&#58; When you make a purchase using the SHRMStore or when you subscribe to a SHRM publication through our website we may also ask for the same type of personally identifiable information (e.g., name, home and/or work email address, postal address, phone number and/or fax number, and your employer and job title, etc., and if you are making a purchase, your credit card information) and demographic information about you (e.g., birth year, certification designation, etc.).The number and variety of useful services on SHRM Websites that may require collection of personally identifiable information and demographic information about you will continue to grow in the future. </li></ul><p> 
   <strong>2. What are &quot;cookies&quot; and how does SHRM use them?</strong><br> A cookie is a small text file containing a unique identification number that is transferred from a website to the hard drive of your computer. This unique number identifies your web browser-but not you-to SHRM computers whenever you visit SHRM Websites. A cookie will not provide personally identifiable information about you, such as your name and address. The use of cookies is an industry standard, and cookies are currently used on most major websites. Most web browsers are initially set up to accept cookies. If you prefer, you can reset your browser to notify you when you have received a cookie. You can also set your browser to refuse to accept cookies altogether. While SHRM does not require you to use cookies, keep in mind that certain services will not function properly if you set your browser to refuse all cookies. To help serve you better, SHRM generally uses cookies to&#58; </p><ul><li>
      <span style="line-height&#58;1.6;">Identify return visitors. Cookies let us remember your web browser so we can provide personalized member services such as My SHRM and SHRM search agents. Cookies also allow us to identify SHRM members who are returning to the site.</span><br></li><li>
      <span style="line-height&#58;1.6;">Display advertisements. SHRM uses an outside ad company to display SHRM-approved ads on our website and other websites. While we use cookies on other parts of our website(s), the cookies received with banner ads are collected by our ad company. These cookies allow SHRM to manage the delivery of ads and do not collect personally identifiable information.</span><br></li></ul><p> 
   <strong>3. What organization collects the information and who has access to it?<br> </strong>Data collected through SHRM's website(s) is generally collected and maintained solely by SHRM or its contracted vendors. More specifically&#58; </p><p> 
   <strong>a.) Personally identifiable information</strong>. </p><ol style="list-style-type&#58;lower-roman;"><li>Information provided when you register for services or products. When you provide personally identifiable information or demographic information about you on SHRM Websites to register for a service, buy a product, or take advantage of a promotion SHRM reserves the right to sell or otherwise provide to selected third parties, mailing/information lists (e.g., names, home and/or work email address, postal address, phone number and/or fax number, and your employer and job title, etc. and demographic information about you such as birth year, certification designation, etc.)derived from such registrations. 
      <strong>If you wish to opt out of such list sales</strong><strong>/distribution</strong><strong> at any time, you may do so by following the directions in Item 6 below.</strong>
      <p>&#160;<strong>&#160;</strong>&#160;</p></li><li>Information provided when joining or renewing membership in SHRM. If you join SHRM or renew your membership through our website, you provide personally identifiable information and demographic information about you on the membership application. SHRM reserves the right to sell or otherwise provide mailing/information lists (names, home and/or work postal address, phone number and/or fax number, and employer and job title, etc. and demographic information about member such as birth year, certification designation, etc.) concerning members to selected third-parties. If you wish to opt out of such list sales/distributions at any time during your SHRM membership, you may do so by following the directions in Item 6 below<strong>. SHRM does not sell to third-parties, e-mail addresses obtained from member applications or renewals</strong> (except that SHRM will rent a member e-mail address if that member has expressly opted in to allow such rentals).&#160;</li><li>Sensitive Personal Information – 
      <strong>SHRM does not share with third parties, sensitive personal information such as passcodes, social security numbers, credit card numbers, felony conviction information, or health information except as necessary to complete transactions</strong> requested by you and under strict confidentiality and security protections; nor does SHRM publish such sensitive personal information.</li></ol><p> 
   <strong>b.) Anonymous information</strong>. 
   <br> We disclose to third-party sponsors/advertisers aggregate statistics (i.e., impressions and click-throughs on a company's advertisement). Also, we may share aggregate website statistics with the media or other third parties. No personally identifiable information is disclosed to these sponsors/advertisers or other third parties as part of this process, only information in an aggregate form. </p><p>Be aware that SHRM's sponsors, advertisers, and third-party content providers have links on our site(s) that take you to other websites. For example, when you click on an ad displayed on SHRM Websites, you are linked to another site. Please note also that links to other websites are provided throughout SHRM Websites for users' information and convenience. SHRM hopes that all third parties involved adhere to our policies regarding the privacy of our users. However, SHRM Websites Privacy Policy does not cover third-party data collection practices, and SHRM Websites do not assume any responsibility for any actions of third parties. </p><p>SHRM websites use several tools and tracking services to collect information about use of the site, such as the following. </p><ul><li>Google Analytics<br></li><li>Clicktale<br></li><li>Silverpop Web Tracking<br></li><li>Optimizely<br></li><li>SHRM Clickstream<br></li><li>Web Tracking Pixels from &#58;&#160;&#160;Facebook<br></li><li>DoubleClick by Google<br></li><li>Turn<br></li><li>Pingdom<br></li></ul><p>These tools and services collect information such as how often users visit this site, what pages or screens they visit on SHRM websites, mouse clicks, mouse movements, scrolling activity as well as any text you type in the website. We use the information we collect to improve the site,&#160;offer better services, and to provide you a more personalized experience, including personalized ads on SHRM websites and other websites. We do not use these tools to collect any personal information nor do we combine the collected information with personally identifiable information. The information may be collected by planting a permanent cookie on your web browser to identify you as a unique user whenever you visit the site. These cookies can only be used by the tools planting them and cannot be shared across tools and the data collected cannot be altered or retrieved by services from other domains.</p><p>You can choose to opt out of these tools and tracking services by going to the URL - 
   <a target="_blank" class="link-external" href="http&#58;//www.networkadvertising.org/choices/">http&#58;//www.networkadvertising.org/choices/</a>. You can also disable cookies on your browser to prevent these tools from recognizing you on return visits to the site. (<a target="_blank" class="link-external" href="http&#58;//www.usa.gov/optout-instructions.shtml">http&#58;//www.usa.gov/optout-instructions.shtml</a>).&#160; Or you can choose to disable the Clicktale service at 
   <a target="_blank" class="link-external" href="http&#58;//www.clicktale.net/disable.html">http&#58;//www.clicktale.net/disable.html</a>. </p><p> 
   <strong>c.) SHRM Subsidiaries</strong><br> We also allow our subsidiaries, Strategic Human Resource Management India Pvt. Ltd. (“SHRM India”), SHRM China, SHRM MEA FZ (Dubai), SHRM Foundation, SHRM Corporation, Council for Global Immigration, and HR People + Strategy, to use the information which you may provide when you register for services or products or when you join or renew membership in SHRM, to the same extent as SHRM may use such information under this Privacy Policy, and fully subject to the same limits as SHRM is subject to on the use of such information under this Privacy Policy.</p><p> 
   <strong>&#160;</strong></p><p> 
   <strong>4. How does SHRM use the information it collects?<br><br> a.)&#160; General </strong>
   <br> SHRM collects information to provide you with the services you request and to improve our website(s). &#160;We retain this information for as long as we anticipate that we still have a reasonable need for the information.</p><p>If you join SHRM or renew your SHRM membership through our website(s), we use the personally identifiable information and demographic information you provide in the membership application to send you SHRM publications, information about member benefits and special offers, and other information that SHRM believes is relevant and useful to its members. Similarly, if you register for products or services through our website(s), we may use the personally identifiable information and demographic information you provide us in doing so to send you information that SHRM believes is relevant and useful to you. See Section 3 above as to SHRM policy on list sales. &#160; </p><p>As mentioned above, SHRM uses the aggregate, anonymous data collected to let our sponsors/advertisers know the number of impressions or views and the number of &quot;click throughs&quot; on their advertisement(s). SHRM also uses this aggregate, anonymous data to perform statistical analyses of the collective characteristics and behavior of our site visitors; to measure user interests regarding specific areas of SHRM Websites; and to analyze how and where best to use our resources. Without such data, we would not know which parts of SHRM Websites are the most popular, and we would not be able to change and update the content and services appropriately. </p><p>SHRM may be required by law enforcement or judicial authorities to provide information on individual users to the appropriate governmental authorities. In matters involving a danger to personal or public safety, SHRM may voluntarily provide information to appropriate governmental authorities. </p><p> 
   <strong>b.) Information Collected in Connection With Certification and Recertification</strong> </p><p> If you register for certification or recertification services, SHRM uses any personally identifiable information and demographic information you may provide doing so in the same manner as we do when you register for other SHRM products and services.&#160; </p><ol style="list-style-type&#58;lower-roman;"><li> Examples of how SHRM will 
      <span style="text-decoration&#58;underline;">not use Certification Candidate information include&#58;</span>
      <ul><li>SHRM will not provide information on candidates who do not pass the exam</li><li>SHRM will not provide information on individual item performance to the examinee or any other interested party &#160;</li><li>SHRM will not use any information related to requests for reasonable accommodation under the ADA, or under similar Non-US requirements, other than is reasonably necessary to provide that accommodation.</li></ul></li><li> Examples of how SHRM may use Candidate/Certification information include&#58; 
      <ul><li>Inclusion in a directory of certified professionals</li><li>Email notification/reminders on the status of certification, information on professional development opportunities, organizational news and communication</li><li>Use to assist the testing administrator in the scheduling of your exam</li></ul></li></ol><p> 
   <strong>c.) Exceptions</strong>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160; </p><p>On occasion SHRM collects personal information or demographic information through SHRM Websites with the intent to afford a greater degree of privacy for such information than is otherwise set forth in this privacy policy.&#160; In those relatively rare situations where SHRM does so it will clearly disclose to you at the time it collects such information, what degree of privacy will be afforded to the personal information and demographic information collected at such time; and SHRM will follow a process to assure that the specifically disclosed degree of privacy is in fact afforded to such information.&#160; For example, SHRM may collect survey information through a survey where SHRM’s use of any personal information from that survey is more limited than the general SHRM Privacy Statement would otherwise allow; in such a case SHRM will disclose the stricter privacy policy governing that survey information at the time such personal information is collected from the survey respondent.</p><p> 
   <strong>5. How can you review and modify your personal information or demographic information?<br> </strong>You have the following options for modifying or causing to be deleted personal information or demographic information previously provided by you to SHRM. </p><p>a.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; E-mail&#58; 
   <a href="mailto&#58;shrm@shrm.org">shrm@shrm.org</a> </p><p>Or if you are a SHRM member you may also v<span style="line-height&#58;1.6;">isit&#58;</span><span style="line-height&#58;1.6;">&#160;</span><a href="/my" style="line-height&#58;1.6;">Member Dashboard</a></p><p>b.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; Send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Member Care Department. </p><p>c.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; Call&#58; 703-548-3440 or +1-800-283-7476. </p><p> 
   <strong>6. What is the opt-out policy for SHRM Websites?<br> </strong>SHRM provides members and customers the opportunity to opt-out of receiving communications from us and our partners. If you no longer wish to receive specific communications or services, you have the following options&#58; </p><p>a.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; You can send an e-mail to&#58; 
   <a href="mailto&#58;shrm@shrm.org">shrm@shrm.org</a> </p><p>b.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; You can send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Member Care Department. </p><p>c.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; You can call the following telephone numbers&#58; +1-800-283-7476 or 703-548-3440. </p><p>Instructions for opting out of any SHRM e-mail newsletter you receive are included with each e-mail. </p><p> 
   <strong></strong></p><div><a name="California"> </a> </div>
<strong>7. Your California Privacy Rights.</strong><br><strong> For California Residents Only.</strong>&#160; SHRM may disclose your personal information to our subsidiaries or other third parties who may use that information to market directly to you.&#160; As a California resident, you have the right to opt-out of having your personal information licensed to such third parties.&#160; We will not share your information after we have received your notification that you are opting out.&#160; If you wish to opt-out you have the following options&#58; 
<p></p><p>a.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; You can send an e-mail to&#58; 
   <a href="mailto&#58;shrm@shrm.org">shrm@shrm.org</a> </p><p>b.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; You can send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Member Care Department. </p><p>c.)&#160;&#160;&#160;&#160;&#160;&#160;&#160; You can call the following telephone numbers&#58; +1-800-283-7476 or 703-548-3440. </p><p> 
   <strong>8.&#160; What kinds of security procedures are in place to protect against the loss, misuse or alteration of your information?</strong><br> SHRM Websites have security measures equal to or better than those reasonably expected in the industry, such as firewalls, in place to protect against the loss, misuse and alteration of your user data under our control. While we cannot guarantee that loss, misuse or alteration to data will not occur, we take reasonable precautions to prevent such unfortunate occurrences. Certain particularly sensitive information, such as your credit card number, collected for a commercial transaction is encrypted prior to transmission.</p><p>You are ultimately responsible for the security of your SHRM login credentials. You may not share your SHRM login credentials with colleagues or friends so they can access content or features that are restricted to SHRM members only. You should log out of your browser at the end of each computer session to ensure that others cannot access your personal information and correspondence, especially if you share a computer with someone else or are using a computer in a public place like a library or Internet cafe. </p><p> 
   <strong>9. How do SHRM Websites use bulletin boards, discussion lists, and moderated chats?</strong><br> SHRM websites makes bulletin boards, discussion lists, and moderated chats available to its members. Any information that is disclosed in these areas becomes public information, and you should exercise caution when deciding to disclose your personal information. Although users may post messages that will appear on the message boards anonymously, SHRM does retain a record of who posts all notes. </p><p class="shrm-Element-Subtitle"> PART II – SHRM PRIVACY STATEMENT FOR INFORMATION COLLECTED OTHER THAN THROUGH SHRM WEBSITES </p><p>If you submit personal information or demographic information to SHRM through any channel other than SHRM Websites, the same privacy rules set forth at Part I above for SHRM Websites will be applied to such personal information and demographic information you submit through channels other than SHRM Websites (including without limitation your opt-out rights at Part I, Section 6 above), except as follows&#58;</p><p>1. The “cookies” and other tracking devices used by SHRM Websites and described in Part I above do not apply to personal information and demographic information gathered through channels other than SHRM Websites.</p><p>2. Hardcopy personal information and demographic information provided to SHRM which is not converted to electronic media and hosted by SHRM will be subject to different security procedures than will stored electronic personal information and demographic information; but SHRM does have security measures equal to or better than those reasonably expected in the industry, in place to protect against the loss, misuse and alteration of your hardcopy personal information and demographic information under our control.</p><p>3. When SHRM collects personal information and demographic information through channels other than SHRM Websites it may in some instances apply a different privacy policy to such information; but where it does so it shall conspicuously disclose to you at the time of collection what privacy policy will apply to such information.&#160; For example, SHRM may collect survey information through a survey where SHRM’s use of any personal information from that survey is more limited than the general SHRM Privacy Statement would otherwise allow; in such a case SHRM will disclose the stricter privacy policy governing that survey information at the time such personal information is collected from the survey respondent.</p><p> 
   <strong>Privacy Policy Effective May, 2002. Updated February 24, 2011, August 7, 2014 and October 1, 2015.</strong></p></div>
                </div>
                
                <div id="article-paragraph-ad" class="article-paragraph-ad shrm-widget ad-holder hidden-lg hidden-md">
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size1W" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size1H" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size1H" value="250" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size2W" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size2W" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size2H" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size2H" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Section" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Section" value="StandardArticleFlexUnitMobile" />
<div id="ctl00_PlaceHolderMain_ctl09_ctl00_pan_AdWords">
		

	</div>

                </div>
                <script>
                    if ($("[id$='_ControlWrapper_RichHtmlField']").find("p").length > 3) {
                        $("*[id$='_ControlWrapper_RichHtmlField'] p:nth-child(3)").after( $("#article-paragraph-ad") );
                    }
                </script>
                
                <div id="ctl00_PlaceHolderMain_ctl10_pan_Main" class="shrm-tags shrm-tags-empty article-tags">
		
    

	</div>

                


                

<script type="text/javascript">    

    var a = "", t = window.location.hostname;
    a = t.indexOf("dev-") >= 0 || t.indexOf("devint-") >= 0 ? "https://dev-dashboardapi.shrm.org" : t.indexOf("qa-") >= 0 ? "https://qa-dashboardapi.shrm.org" : t.indexOf("stg-") >= 0 ? "https://stg-dashboardapi.shrm.org" : "https://dashboardapi.shrm.org";
    var _baseUrl = a + "/api/";
    var _authorizationToken = $('#MySHRMAPITokenHidden').val();

    function saveBookmark() {
        var item = {};

        var _ArticleGuid = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleGuid').val();
        var _MemberId = $('#MemberIdHidden').val();
        var _SsoUUId = $('#SSOUUIDHidden').val();

        item["SSOUUId"] = _SsoUUId;
        item["BookmarkTypeId"] = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleTypeId').val();
        item["BookmarkLink"] = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleLink').val();
        item["ItemGuid"] = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleLink').val();        
        item["Headline"] = $('#ctl00_PlaceHolderMain_ctl12_hf_Headline').val();
        item["Subheading"] = $('#ctl00_PlaceHolderMain_ctl12_hf_Subheading').val();
        item["Notes"] = null;
        item["AddedBy"] = "WebUser";
        item["AddedDate"] = new Date();
        item["UpdatedBy"] = null;
        item["UpdatedDate"] = null;

        console.log(item);

        if (item) {
            $.ajax({
                url: _baseUrl + 'bookmark/create?memberId=' + _MemberId + '&ssoUUId=' + _SsoUUId,
                type: 'POST',
                headers: { 'Authorization': "Bearer " + _authorizationToken },
                data: JSON.stringify(item),
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    bookmarkExists();
                    $('#modal_bookmark_save').modal('show');
                    RefreshApiArticleSession();
                },
                error: function (x, y, z) {
                    console.log(x + '\n' + y + '\n' + z);
                    $('#modal_bookmark_error').modal('show');
                    $('#modal_bookmark_error_msg').html(x.responseText);
                }
            });
        }
        else { console.log("POST data is missing!"); }
    }
    function preDeleteBookmark() { $('#modal_bookmark_delete').modal('show'); }
    function deleteBookmarkCanceled() { $('#modal_bookmark_canceldelete').modal('show'); }
    function deleteBookmark() {
        var _ArticleGuid = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleGuid').val();
        var _MemberId = $('#MemberIdHidden').val();
        var _SsoUUId = $('#SSOUUIDHidden').val();
        //=================================================================>>
        if (_ArticleGuid != null) {
            $.ajax({
                url: _baseUrl + 'bookmark/DeleteByUserAndArticle?ssoUUId=' + _SsoUUId + '&itemGuid=' + _ArticleGuid,
                type: 'GET',
                headers: {
                    'Authorization': "Bearer " + _authorizationToken,
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                async: false,
                success: function (data) {
                    bookmarkExists();
                    $('#modal_bookmark_deleted').modal('show');
                    RefreshApiArticleSession();
                },
                error: function (x, y, z) { console.log(x + '\n' + y + '\n' + z); }
            });
        }
        else { alert("BookmarkId is missing!"); }
    }
    function bookmarkExists() {
        $("[id$='hl_SaveBookmark']").unbind();
        if ($('#MemberStatusHidden').val() == "0") {
            $("[id$='hl_SaveBookmark']").click(LoginBeforeSave);
        }
        else if ($('#MemberStatusHidden').val() == "1" || $('#MemberStatusHidden').val() == "4" || $('#MemberStatusHidden').val() == "5") {
            $("[id$='hl_SaveBookmark']").click(MembershipBeforeSave);
        }
        else if ($('#MemberStatusHidden').val() == "2" || $('#MemberStatusHidden').val() == "3") {
            var _ArticleGuid = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleGuid').val();
            var _MemberId = $('#MemberIdHidden').val();
            var _SsoUUId = $('#SSOUUIDHidden').val();
            var _Exists = false;
            //=================================================================>>
            if (_ArticleGuid != null) {
                $.ajax({
                    url: _baseUrl + 'bookmark/exists?ssoUUId=' + _SsoUUId + '&itemGuid=' + _ArticleGuid,
                    type: 'GET',
                    headers: {
                        'Authorization': "Bearer " + _authorizationToken,
                        'Content-Type': 'application/json'
                    },
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        _Exists = data;
                    },
                    error: function (x, y, z) {
                        console.log(x + '\n' + y + '\n' + z);
                    }
                });
            }
            else { console.log("BookmarkId is missing!"); }
            //=================================================================>>
            if (_Exists) {
                $("[id$='hl_SaveBookmark']").click(preDeleteBookmark);
                $("[id$='hl_SaveBookmark']").removeClass('fa-bookmark-o');
                $("[id$='hl_SaveBookmark']").addClass('fa-bookmark');
                $("[id$='hl_SaveBookmark']").addClass('shrm-bookmarked');
                $("[id$='hl_SaveBookmark']").addClass('btn-active');
            }
            else {
                $("[id$='hl_SaveBookmark']").click(saveBookmark);
                $("[id$='hl_SaveBookmark']").removeClass('fa-bookmark');
                $("[id$='hl_SaveBookmark']").removeClass('shrm-bookmarked');
                $("[id$='hl_SaveBookmark']").addClass('fa-bookmark-o');
                $("[id$='hl_SaveBookmark']").removeClass('btn-active');
            }
        }
    }

    function saveLike() {        
        var item = {};

        var _ArticleGuid = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleGuid').val();
        var _MemberId = $('#MemberIdHidden').val();
        var _SsoUUId = $('#SSOUUIDHidden').val();

        item["SSOUUId"] = _SsoUUId;
        item["ItemGuid"] = _ArticleGuid;
        item["AddedBy"] = "WebUser";
        item["AddedDate"] = new Date();
        item["UpdatedBy"] = null;
        item["UpdatedDate"] = null;
        
        if (item) {
            $.ajax({
                url: _baseUrl + 'articlelike/create?memberId=' + _MemberId + '&ssoUUId=' + _SsoUUId,
                type: 'POST',
                headers: {
                    'Authorization': "Bearer " + _authorizationToken
                },
                data: JSON.stringify(item),
                contentType: "application/json;charset=utf-8",
                success: function (data) {
                    likeExists();
                    RefreshApiArticleSession();
                },
                error: function (x, y, z) {
                    console.log(x + '\n' + y + '\n' + z);
                    $('#modal_bookmark_error').modal('show');
                    $('#modal_bookmark_error_msg').html(x.responseText);
                }
            });
        }
        else {
            console.log("POST data is missing!");
        }
    }
    function likeExists() {
        $("[id$='hl_SaveLike']").unbind();
        if ($('#MemberStatusHidden').val() == "0") {
            $("[id$='hl_SaveLike']").attr('title', 'Member-Only Feature').attr("data-toggle", "tooltip").attr('disabled', 'disabled').css("pointer-events", "auto");
        }
        else if ($('#MemberStatusHidden').val() == "1" || $('#MemberStatusHidden').val() == "4" || $('#MemberStatusHidden').val() == "5") {
            $("[id$='hl_SaveLike']").attr('title', 'Member-Only Feature').attr("data-toggle", "tooltip").attr('disabled', 'disabled').css("pointer-events", "auto");
        }
        else if ($('#MemberStatusHidden').val() == "2" || $('#MemberStatusHidden').val() == "3") {
            var _ArticleGuid = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleGuid').val();
            var _MemberId = $('#MemberIdHidden').val();
            var _SsoUUId = $('#SSOUUIDHidden').val();
            var _Exists = false;
            //=================================================================>>
            if (_ArticleGuid != null) {
                $.ajax({
                    url: _baseUrl + 'articlelike/exists?ssoUUId=' + _SsoUUId + '&itemGuid=' + _ArticleGuid,
                    type: 'GET',
                    headers: {
                        'Authorization': "Bearer " + _authorizationToken,
                        'Content-Type': 'application/json'
                    },
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        _Exists = data;
                    },
                    error: function (x, y, z) {
                        console.log(x + '\n' + y + '\n' + z);
                    }
                });
            }
            else { console.log("BookmarkId is missing!"); }
            //=================================================================>>
            if (_Exists) {
                //$("[id$='lab_LikeTitle']").text('LIKED');
                $("[id$='hl_SaveLike']").click(deleteLike);
                $("[id$='hl_SaveLike']").removeClass('fa-heart-o');
                $("[id$='hl_SaveLike']").addClass('fa-heart');
                $("[id$='hl_SaveLike']").addClass('shrm-liked');
                $("[id$='hl_SaveLike']").addClass('btn-active');
            }
            else {
                //$("[id$='lab_LikeTitle']").text('LIKE');
                $("[id$='hl_SaveLike']").click(saveLike);
                $("[id$='hl_SaveLike']").removeClass('fa-heart');
                $("[id$='hl_SaveLike']").removeClass('shrm-liked');
                $("[id$='hl_SaveLike']").removeClass('btn-active');
                $("[id$='hl_SaveLike']").addClass('fa-heart-o');
            }
        }
    }
    function deleteLike() {
        var _ArticleGuid = $('#ctl00_PlaceHolderMain_ctl12_hf_ArticleGuid').val();
        var _MemberId = $('#MemberIdHidden').val();
        var _SsoUUId = $('#SSOUUIDHidden').val();
        if (_SsoUUId != null && _ArticleGuid != null) {
            $.ajax({
                url: _baseUrl + 'articlelike/Delete?ssoUUId=' + _SsoUUId + '&itemGuid=' + _ArticleGuid,
                type: 'GET',
                headers: {
                    'Authorization': "Bearer " + _authorizationToken,
                    'Content-Type': 'application/json'
                },
                dataType: 'json',
                async: false,
                success: function (data) {
                    likeExists();
                    RefreshApiArticleSession();
                },
                error: function (x, y, z) {
                    console.log(x + '\n' + y + '\n' + z);
                }
            });
        }
        else {
            alert("Article Guid is missing!");
        }
    }
    function LoginBeforeSave() {
        $('#modal_bookmark_loginbeforesave').modal('show');
    }
    function MembershipBeforeSave() {
        $('#modal_bookmark_membershipbeforesave').modal('show');
    }
    function ProceedToLogin() {
        window.location = $('#ctl00_PlaceHolderMain_ctl12_hf_SiteUrl').val() + '/_layouts/authenticate.aspx?source=' + window.location.href;
    }
    function ProceedToMembership() {
        window.location = $('#ctl00_PlaceHolderMain_ctl12_hf_MembershipUrl').val();
    }
    // HELPER FUNCTIONS //
    function GetJson() {
        var jsonStr = $("#txtAreaInput").val();
        var jsonObj = null;
        if (jsonStr) { jsonObj = JSON.parse(jsonStr); }
        return jsonObj;
    }
    function ShowJson(jsonObj) {
        var jsonPretty = JSON.stringify(jsonObj, null, '\t');
        $("pre").text(jsonPretty);
    }
</script>
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SiteUrl" id="ctl00_PlaceHolderMain_ctl12_hf_SiteUrl" value="https://www.shrm.org" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_ArticleGuid" id="ctl00_PlaceHolderMain_ctl12_hf_ArticleGuid" value="05f756a6-7090-4d03-976f-a430f545b58e" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_ArticleTypeId" id="ctl00_PlaceHolderMain_ctl12_hf_ArticleTypeId" value="1" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_ArticleLink" id="ctl00_PlaceHolderMain_ctl12_hf_ArticleLink" value="05f756a6-7090-4d03-976f-a430f545b58e" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_Headline" id="ctl00_PlaceHolderMain_ctl12_hf_Headline" value="Privacy Policy" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_Subheading" id="ctl00_PlaceHolderMain_ctl12_hf_Subheading" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_MembershipUrl" id="ctl00_PlaceHolderMain_ctl12_hf_MembershipUrl" value="https://membership.shrm.org/" />
<script type="text/javascript">
    bookmarkExists();
    likeExists();
</script>
<div id="modal_bookmark_save" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>You have successfully saved this page as a bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="window.location = window.location.protocol + '//' + window.location.hostname + '/my/bookmarks/';">My Bookmarks</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_delete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please confirm that you want to proceed with deleting bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="deleteBookmark();">Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteBookmarkCanceled();">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_deleted" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>You have successfully removed bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="window.location = window.location.protocol + '//' + window.location.hostname + '/my/bookmarks/';">My Bookmarks</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_canceldelete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Delete canceled</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_loginbeforesave" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please log in as a SHRM member before saving bookmarks.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="ProceedToLogin();">Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_membershipbeforesave" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please purchase a SHRM membership before saving bookmarks.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="ProceedToMembership();">Join</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_error" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p id="modal_bookmark_error_msg">An error has occurred</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

                <div id="ctl00_PlaceHolderMain_ctl13_pan_ArticleRecommendedForYou" class="row-shrm-recommended-inline clearfix">
		
    <a id="ctl00_PlaceHolderMain_ctl13_hl_FakeLink" class="fake-link" href="https://www.shrm.org/resourcesandtools/hr-topics/technology/pages/experts-say-more-cybersecurity-workers-needed.aspx">&nbsp;</a>
    <div id="ctl00_PlaceHolderMain_ctl13_pan_ImageHolder" class="image-holder pull-left">
			
  	    <img id="ctl00_PlaceHolderMain_ctl13_img_Image" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_486%2cw_864%2cx_0%2cy_75/c_fit%2cw_767/v1/Technology/laptopwoman_vt89au?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjc1LCJ4MiI6ODY0LCJ5MiI6NTYxLCJ3Ijo4NjQsImgiOjQ4Nn19" />
    
		</div>
    <p class="shrm-Label text-uppercase">Recommended for you</p>
    <h5>Study: More Cybersecurity Workers Needed</h5>  

	</div>
<script>
    //console.log($("[id$='_ControlWrapper_RichHtmlField']").find("p").length);
    //if ($("[id$='_ControlWrapper_RichHtmlField']").find("p").length > 5) {
    //    var _Count = $("[id$='_ControlWrapper_RichHtmlField']").find("p").length - 2;
    //    $("[id$='pan_ArticleRecommendedForYou']").appendTo($("[id$='_ControlWrapper_RichHtmlField']").find("p:nth-child(" + _Count + ")"));
    //}
</script>

                <!--	/.shrm-comments-area	-->
<script type="text/javascript">
    $(document).ready(function () {
        if ($('#MemberStatusHidden').val() == "2" || $('#MemberStatusHidden').val() == "3") { $('.shrm-comments-area').show(); }
    });
</script>

                


<script type="text/javascript">
    function SelectTerm(_TermId) {
        FindChildNodes(_TermId, false);
        FindParentNodes(_TermId, $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").prev().prop('checked'));
        FindSameTitleTerms(_TermId, $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").prev().prop('checked'));
        FindRootPath();
        SetSelectedTerms();
    }
    function SetSelectedTerms() {
        var _HiddenValues = "";
        var _VisibleValues = "";
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").each(function () {
            _HiddenValues += $(this).next().next().text() + "|" + $(this).next().val() + ";";
            _VisibleValues += "<span class='valid-text' title=''>" + $(this).next().next().text() + "</span>; ";
        });
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val(_HiddenValues);
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children().eq(1).children().eq(0).html(_VisibleValues);
        $("[id$='PreventEmptyTaxonomy']").val(_HiddenValues);
    }
    function LoadSelectedTerms() {
        try { $("#hf_TaxonomyReset_NAID").val($(".article-taxonomy-treeview-node input[value='N/A']").prev().prev().val()); }
        catch (err) { }
        var _Terms = $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val().split(';');
        if ($("#hf_TaxonomyReset_NAID").val() != '') {
            $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").parent().parent().parent().parent().parent().hide();
            var _IsNA = false;
            $.each(_Terms, function (index, value) {
                if ($("#hf_TaxonomyReset_NAID").val() == value.split('|')[1]) { _IsNA = true; }
                else { $(".article-taxonomy-treeview-node input[value='" + value.split('|')[1] + "']").prev().prop('checked', true); }
            });
            if (_IsNA) {
                $("#cb_TaxonomyReset_NA").prop('checked', true);
                $(".article-taxonomy-treeview-node input[type=checkbox]:checked").prop('checked', false);
                $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").prev().prop('checked', true);
                SetSelectedTerms();
                $("[id$='pan_TaxonomyTreeView']").hide();
            }
        }
        else {
            $("#cb_TaxonomyReset_NA").parent().hide();
            $.each(_Terms, function (index, value) {
                $(".article-taxonomy-treeview-node input[value='" + value.split('|')[1] + "']").prev().prop('checked', true);
            });
        }
        SetSelectedTerms();
    }
    function FindParentNodes(_TermId, _Checked) {
        var _ParentCheckbox = $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").parent().parent().parent().parent().parent().parent().prev().find('.article-taxonomy-treeview-node').children().eq(0).children().eq(0);
        if ($(_ParentCheckbox).is(':checkbox')) {
            $(_ParentCheckbox).prop('checked', _Checked);
            FindParentNodes($(_ParentCheckbox).next().val(), _Checked);
        }
    }
    function FindChildNodes(_TermId, _Checked) {
        $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").parent().parent().parent().parent().parent().next().find('.article-taxonomy-treeview-node input[type=checkbox]').each(function () {
            $(this).prop('checked', _Checked);
        });
    }
    function FindSameTitleTerms(_TermId, _Checked) {
        var _Title = $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").next().next().val();
        $(".article-taxonomy-treeview-node input[value='" + _Title + "']").each(function () {
            $(this).prev().prev().prev().prop('checked', _Checked);
            FindParentNodes($(this).prev().prev().val(), _Checked);
        });
    }
    function FindRootPath() {
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").each(function () {
            FindParentNodes($(this).next().val(), true);
        });
    }
    function RemoveSelectedTerms() {
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").prop('checked', false);
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val('');
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children().eq(1).children().eq(0).html('');
        $("[id$='PreventEmptyTaxonomy']").val('');
        console.log('#cb_TaxonomyReset_NA is: ' + $("#cb_TaxonomyReset_NA").prop('checked'));
        if ($("#cb_TaxonomyReset_NA").prop('checked') == true) {
            console.log('#cb_TaxonomyReset_NA is: TRUE');
            $("[id$='pan_TaxonomyTreeView']").hide();
            $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").prev().prop('checked', true);
        }
        else {
            console.log('#cb_TaxonomyReset_NA is: FALSE');
            $("[id$='pan_TaxonomyTreeView']").show();
        }
        SetSelectedTerms();
    }
    function FixAspTreeView() {
        $(".article-taxonomy-treeview td[class!='article-taxonomy-treeview-node'] a").each(function () {
            $(this).attr('onclick', $(this).attr('href')).removeAttr('href');
        });
    }
</script>









            </div>
            <div class="col-md-4">
                <div class="shrm-widget ad-holder hidden-xs hidden-sm">
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl16$hf_Size1W" id="ctl00_PlaceHolderMain_ctl16_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl16$hf_Size1H" id="ctl00_PlaceHolderMain_ctl16_hf_Size1H" value="250" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl16$hf_Size2W" id="ctl00_PlaceHolderMain_ctl16_hf_Size2W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl16$hf_Size2H" id="ctl00_PlaceHolderMain_ctl16_hf_Size2H" value="600" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl16$hf_Section" id="ctl00_PlaceHolderMain_ctl16_hf_Section" value="StandardArticleFlexUnit" />
<div id="ctl00_PlaceHolderMain_ctl16_pan_AdWords">
		

	</div>

                </div>
                <div class="shrm-widget-row">
                  <div>

<div class="shrm-most-popular-widget shrm-widget non-tiles col-sm-6 col-md-12 ">
	<h2 class="shrm-Title-Small text-center text-uppercase">Most popular</h2>
    <div class="list-group shrm-list-stream">
        
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="javascript:"><img class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_474%2cw_843%2cx_0%2cy_28/c_fit%2cw_767/v1/Legal%20and%20Compliance/remote_access_rlozbk?databtoa=eyIzeDQiOnsieCI6MjYwLCJ5IjowLCJ4MiI6Njg1LCJ5MiI6NTY1LCJ3Ijo0MjQsImgiOjU2NX0sIjE2eDkiOnsieCI6MCwieSI6MjgsIngyIjo4NDMsInkyIjo1MDIsInciOjg0MywiaCI6NDc0fX0%3d" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/resourcesandtools/hr-topics/compensation/pages/salary-budgets-2017.aspx">Salary Budgets Expected to Rise 3% in 2017</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="javascript:"><img class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_1526%2cw_2714%2cx_0%2cy_0/c_fit%2cw_767/v1/Employee%20Relations/iStock_88527767_LARGE_bpayij?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjAsIngyIjoyNzE0LCJ5MiI6MTUyNiwidyI6MjcxNCwiaCI6MTUyNn0sIjIxeDkiOnsieCI6MCwieSI6NjYsIngyIjoyNzE0LCJ5MiI6MTIyOSwidyI6MjcxNCwiaCI6MTE2M30sIjN4NCI6eyJ4Ijo1MDAsInkiOjAsIngyIjoxODU4LCJ5MiI6MTgxMCwidyI6MTM1OCwiaCI6MTgxMH0sIjF4MSI6eyJ4IjowLCJ5IjowLCJ4MiI6MTgxMCwieTIiOjE4MTAsInciOjE4MTAsImgiOjE4MTB9fQ%3d%3d" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/resourcesandtools/hr-topics/employee-relations/pages/shorter-hours-for-older-workers.aspx">Should Workers Over 40 Have Four-Day Weekends?</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="javascript:"><img class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_961%2cw_1709%2cx_642%2cy_214/c_fit%2cw_767/v1/News/iStock_98354485_LARGE_ipc7mz?databtoa=eyIxNng5Ijp7IngiOjY0MiwieSI6MjE0LCJ4MiI6MjM1MSwieTIiOjExNzUsInciOjE3MDksImgiOjk2MX19" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/hr-today/news/hr-news/pages/clinton-vs-trump-equal-pay-for-equal-work.aspx">Clinton vs. Trump: Equal Pay for Equal Work</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
    </div>
    <div class="bottom-button" style="display: none;"> 
        <a class="btn btn-block btn-blank shrm-CTA-Green" onClick="$(this).closest('.shrm-widget').find('.collapse:not(.in)').slice(0,2).collapse()">MORE</a>
    </div>
    <input type="hidden" name="ctl00$PlaceHolderMain$ctl17$hf_Message" id="ctl00_PlaceHolderMain_ctl17_hf_Message" />
</div>
</div>
                  <div><div id="ctl00_PlaceHolderMain_ctl18_pan_Main" class="row-promo-panel shrm-widget col-sm-6 col-md-12">
		
    <div class="promo-panel-wrapper outlined text-center">
  	    <h3 class="shrm-Title-Small text-uppercase text-center">SEMINARS</h3>
        <a id="ctl00_PlaceHolderMain_ctl18_hl_ImageWrapper" class="promo-image" href="https://store.shrm.org/education/seminars/find-a-seminar.html"><img id="ctl00_PlaceHolderMain_ctl18_img_Image" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_954%2cw_1697%2cx_0%2cy_13/c_fit%2cw_767/v1/Marketing/iStock_71981373_MEDIUM_vkwscq?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjEzLCJ4MiI6MTY5NywieTIiOjk2NywidyI6MTY5NywiaCI6OTU0fX0%3d" /></a>
        <p>HR Education in a City Near You</p>
    </div>
    <a id="ctl00_PlaceHolderMain_ctl18_hl_More" class="btn btn-success btn-cta text-uppercase" href="https://store.shrm.org/education/seminars/find-a-seminar.html">Find a Seminar</a>

	</div>

<script id="promo-panel-script">
    !function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
        if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = p + "://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs); }
    }(document, "script", "twitter-wjs");
</script>        
</div>
                </div><!-- /.shrm-widget-row	-->
                
<div class="shrm-widget shrm-job-finder">
    <h3 class="shrm-Title-Small text-uppercase text-center">Job Finder</h3>
    <h5 class="text-center">Find an HR Job Near You</h5>
    <div class="finder-form">
        <div class="input-holder">
            <input name="ctl00$PlaceHolderMain$HrJobs$searchTerm" type="text" id="ctl00_PlaceHolderMain_HrJobs_searchTerm" placeholder="CITY, STATE, ZIP" class="form-control" />
        </div>
        <a href="javascript:" id="ctl00_PlaceHolderMain_HrJobs_searchJobs" class="btn btn-success btn-lg btn-cta" onclick="return OpenInNewWindow(&#39;ctl00_PlaceHolderMain_HrJobs_searchJobs&#39;,&#39;ctl00_PlaceHolderMain_HrJobs_searchTerm&#39;);"><span class="status">Search Jobs</span></a>
    </div>
</div>
<script language="javascript" type="text/javascript">
    function OpenInNewWindow(anchorTagId, textBoxTagId) {
        var searchTerm = document.getElementById(textBoxTagId);
        var searchJobs = document.getElementById(anchorTagId);
        searchJobs.setAttribute("href", "javascript:");
        searchJobs.setAttribute("target", "");

        if (searchTerm.value.length > 0) {
            searchJobs.setAttribute("target", "_blank");
            var href = "http://hrjobs.shrm.org/jobs/search/results?radius=0&location=" + searchTerm.value;
            searchJobs.setAttribute("href", href);
        }
    }
		$(function() {
			$uccHrJobsHtml = $('#ucc_HRJobs');
			$uccHrJobsPlaceholder = $('nav.navbar li a').filter(function(){
				 return $(this).text() === "uccHrJobs";
			});
			
			if( $uccHrJobsHtml.length > 0 && $uccHrJobsPlaceholder.length > 0){
				$('.shrm-job-finder',$uccHrJobsHtml).addClass('shrm-mini-job-finder');
				$($uccHrJobsPlaceholder).replaceWith( $($uccHrJobsHtml).show());
			}
		});
</script>

            </div>
        </div>
        

        

<div class="row-shrm-in-action you-may-also-like shrm-widget">
	<div class="container">
	    <h2 class="text-center text-uppercase shrm-Title">You may also like</h2>
	    <ul class="nav" role="tablist">
            <li role="presentation" class="active">
                <a id="ctl00_PlaceHolderMain_ctl20_hl_Tab_Articles" class="btn btn-cta btn-primary" role="tab" data-toggle="tab" aria-controls="tab-content-articles" href="../../_controltemplates/15/SHRM.Core/#tab-content-articles">Articles</a>
            </li>
            <li role="presentation" class="">
                <a id="ctl00_PlaceHolderMain_ctl20_hl_Tab_Tools" class="btn btn-cta btn-primary" role="tab" data-toggle="tab" aria-controls="tab-content-tools" href="../../_controltemplates/15/SHRM.Core/#tab-content-tools">Tools</a>
            </li>
            <li role="presentation" class="">
                <a id="ctl00_PlaceHolderMain_ctl20_hl_Tab_Learning" class="btn btn-cta btn-primary" role="tab" data-toggle="tab" aria-controls="tab-content-learning" href="../../_controltemplates/15/SHRM.Core/#tab-content-learning">Learning</a>
            </li>
	    </ul>
	    <div class="tab-content">
            <div id="tab-content-articles" role="tabpanel" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-sm-4 col-shrm-action-left">
                        <div id="ctl00_PlaceHolderMain_ctl20_pan_FeaturedArticle_Articles" class="shrm-action-tile tile-has-thumb">
		
	                        <img id="ctl00_PlaceHolderMain_ctl20_img_FeaturedArticle_Articles_Image" class="img-tile img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_1525%2cw_2713%2cx_0%2cy_63/c_fit%2cw_767/v1/Technology/iStock_76045735_LARGE_ppetg6?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjYzLCJ4MiI6MjcxMywieTIiOjE1ODgsInciOjI3MTMsImgiOjE1MjV9LCIyMXg5Ijp7IngiOjAsInkiOjE0MSwieDIiOjI3MTMsInkyIjoxMzAzLCJ3IjoyNzEzLCJoIjoxMTYyfSwiM3g0Ijp7IngiOjEyNTAsInkiOjAsIngyIjoyNjA5LCJ5MiI6MTgxMSwidyI6MTM2MCwiaCI6MTgxMX0sIjF4MSI6eyJ4Ijo5MDAsInkiOjAsIngyIjoyNzEzLCJ5MiI6MTgxMSwidyI6MTgxMywiaCI6MTgxMX19" />
	                        <h6><a id="ctl00_PlaceHolderMain_ctl20_hl_FeaturedArticle_Articles_Link" class="tile-link" href="https://www.shrm.org/hr-today/news/hr-magazine/0716/pages/how-leaders-can-help-employees-accept-technology-changes.aspx">How Leaders Can Help Employees Accept Technology Changes</a></h6>
	                        <span class="tile-tags">
                                
	                        </span>	
	                    
	</div>
                    </div>
                    <div class="col-sm-8 col-shrm-action-right">
                        
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/hr-today/news/hr-news/pages/paid-leave-discussed-at-dnc.aspx">Paid Leave Discussed at DNC</a></h5>
	                            </div>
                            
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/hr-today/news/hr-news/pages/employers-must-play-a-role-in-workforce-development.aspx">Employers Must Play a Role in Workforce Development</a></h5>
	                            </div>
                            
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/hr-today/news/hr-magazine/0716/pages/when-millennials-take-over.aspx">What Employers Can Learn From Millennials</a></h5>
	                            </div>
                            
                    </div>
                </div>
            </div>
            <div id="tab-content-tools" role="tabpanel" class="tab-pane fade">
                <div class="row">
                    <div class="col-sm-4 col-shrm-action-left">
                        <div id="ctl00_PlaceHolderMain_ctl20_pan_FeaturedArticle_Tools" class="shrm-action-tile tile-has-thumb">
		
	                        <img id="ctl00_PlaceHolderMain_ctl20_img_FeaturedArticle_Tools_Image" class="img-tile img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_3157%2cw_5616%2cx_0%2cy_318/c_fit%2cw_767/v1/Marketing/iStock_86035963_XXXLARGE_ym92fg?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjMxOCwieDIiOjU2MTYsInkyIjozNDc1LCJ3Ijo1NjE2LCJoIjozMTU3fX0%3d" />
	                        <h6><a id="ctl00_PlaceHolderMain_ctl20_hl_FeaturedArticle_Tools_Link" class="tile-link" href="https://store.shrm.org/certification/certification/learning-system.html">Your comprehensive system to prepare for the SHRM certification exam</a></h6>
	                        <span class="tile-tags">
                                
	                        </span>	
	                    
	</div>
                    </div>
                    <div class="col-sm-8 col-shrm-action-right">
                        
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/resourcesandtools/tools-and-samples/hr-forms/pages/application_employmentapplication3.aspx">Application: Employment Application #3 </a></h5>
	                            </div>
                            
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/resourcesandtools/tools-and-samples/hr-forms/pages/application_employmentapplication1.aspx">Application: Employment Application #1 </a></h5>
	                            </div>
                            
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/resourcesandtools/tools-and-samples/hr-forms/pages/application_employmentapplication2.aspx">Application: Employment Application #2 </a></h5>
	                            </div>
                            
                    </div>
                </div>
            </div>
            <div id="tab-content-learning" role="tabpanel" class="tab-pane fade">
	        	<div class="row">
	          	    <div class="col-sm-4 col-shrm-action-left">
	            	    <div id="ctl00_PlaceHolderMain_ctl20_pan_FeaturedArticle_Learning" class="shrm-action-tile tile-has-thumb">
		
	                        <img id="ctl00_PlaceHolderMain_ctl20_img_FeaturedArticle_Learning_Image" class="img-tile img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_3238%2cw_5760%2cx_0%2cy_0/c_fit%2cw_767/v1/Marketing/iStock_92988275_XXXLARGE_yg2pjg?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjAsIngyIjo1NzYwLCJ5MiI6MzIzOCwidyI6NTc2MCwiaCI6MzIzOH19" />
	                        <h6><a id="ctl00_PlaceHolderMain_ctl20_hl_FeaturedArticle_Learning_Link" class="tile-link" href="https://store.shrm.org/education/elearning.html">500+ HR education courses at your fingertips.</a></h6>
	                        <span class="tile-tags">
                                
	                        </span>	
	                    
	</div>
	                </div>
	                <div class="col-sm-8 col-shrm-action-right">
                        
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/learningandcareer/learning/onsite-training/pages/emerging-leaders-education.aspx"> Emerging Leaders Education </a></h5>
	                            </div>
                            
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/learningandcareer/competency-model/pages/default.aspx">About the Competency Model</a></h5>
	                            </div>
                            
	                            <div class="shrm-action-tile">
	                                <h5><a class="tile-link" href="https://www.shrm.org/learningandcareer/competency-model/pages/competency-faqs.aspx">Competency FAQs</a></h5>
	                            </div>
                            
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
</div>

        <div id="ctl00_PlaceHolderMain_ctl21_pan_Main" class="row-shrm-sponsors shrm-widget">
		
	<h2 class="shrm-Title text-uppercase">SPONSOR OFFERS</h2>
	<div class="row sponsors-wrapper">
        
        <ins class="adbladeads" data-cid="18802-1901870943" data-host="web.industrybrains.com" data-tag-type="4" data-protocol="https" style="display:none"></ins>
        
        <div class="shrm-sponsor col-md-4">
    	    <h4><a href="http://vendordirectory.shrm.org/" target="_blank">Find the Right Vendor for Your HR Needs</a></h4>
            <p>
      	        SHRM’s HR Vendor Directory contains over 3,200 companies
            </p>
            <a href="http://vendordirectory.shrm.org/" class="shrm-CTA-Green text-uppercase link-external" target="_blank">Search &amp; Connect</a>
        </div><!--	/shrm-sponsor	-->
	</div><!--	/.sponsors-wrapper	-->

	</div>

<script src='//www.shrm.org/Documents/SponsorOfferFetchFile.js'></script>

<input type="hidden" name="ctl00$PlaceHolderMain$ctl21$hf_DataCid" id="ctl00_PlaceHolderMain_ctl21_hf_DataCid" value="18801-2632239399" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl21$hf_DataHost" id="ctl00_PlaceHolderMain_ctl21_hf_DataHost" value="web.industrybrains.com" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl21$hf_DataTagType" id="ctl00_PlaceHolderMain_ctl21_hf_DataTagType" value="4" />


    </div>
<div style='display:none' id='hidZone'><menu class="ms-hide">
		<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/15/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

		</ie:menuitem>
	</menu></div>
									<!-- End - Layout content -->
								  
								    <!-- --------------------------------- SHRM FOOTER STARTS	--> 
								    

<div class="container-fluid container-footer"> 
  <!--<div class="row footer-newsletter shrm-role-member-hidden">
        <div class="container">
            <div class="text-center newsletter-title"><h6>The best of HR News</h6></div>
            <div class="text-center newsletter form">
                <div class="footer-subscribe-form" >
                    <input type="email" placeholder="YOUR EMAIL ADDRESS" />
                    <a class="btn btn-cta btn-success btn-lg">Subscribe</a>
                </div>
            </div>
            <div class="text-center newsletter-all"> 
                <a href="http://shrm.org/publications/e-mailnewsletters/pages/default.aspx">SEE ALL NEWSLETTERS</a>
            </div>								
        </div>    
    </div>-->
  <div class="row footer-member-newsletter">
    <div class="container text-center">
      <div class="footer-member-newsletter-cta">
        <h6>Stay Informed with SHRM Newsletters</h6>
        <a id="ctl00_ctl75_hl_Newsletter_SignUpToday_Link" class="btn btn-success btn-cta btn-lg" href="http://l.shrm.org/opt/Unified" target="_blank">SIGN UP TODAY</a>
      </div>
    </div>
  </div>
  <div class="container container-footer-lower">
    <div class="row">
      <div class="footer-socials text-center">
        <h3 class="text-center shrm-Label">
          JOIN THE CONVERSATION
        </h3>
        <span>
        <a id="ctl00_ctl75_hl_SocialLink_Facebook" href="http://www.facebook.com/societyforhumanresourcemanagement" target="_blank"><i class="fa fa-facebook"></i></a>
        <a id="ctl00_ctl75_hl_SocialLink_LinkedIn" href="http://www.linkedin.com/company/11282?trk=NUS_CMPY_TWIT" target="_blank"><i class="fa fa-linkedin"></i></a>
        <a id="ctl00_ctl75_hl_SocialLink_Instagram" href="https://instagram.com/shrmofficial/" target="_blank"><i class="fa fa-instagram"></i></a>
        </span> <span>
        <a id="ctl00_ctl75_hl_SocialLink_YouTube" href="http://www.youtube.com/shrmofficial" target="_blank"><i class="fa fa-youtube-play"></i></a>
        <a id="ctl00_ctl75_hl_SocialLink_RSS" href="/about-shrm/pages/rss.aspx" target="_blank"><i class="fa fa-rss"></i></a>
        <a id="ctl00_ctl75_hl_SocialLink_Twitter" href="http://twitter.com/SHRM" target="_blank"><i class="fa fa-twitter"></i></a>
        </span> </div>
      <div class="footer-nav clearfix">
        <div class="col-md-9 footer-col footer-col-about clearfix">
          <div class="row">
            <h4 class="footer-col-title col-xs-12 visible-xs">SHRM</h4>
            <div class="col-sm-8">
              <h4 class="footer-col-title hidden-xs">SHRM</h4>
              <ul class="row list-unstyled">
                
                    <li class="col-xs-6">
                      <a href="https://www.shrm.org/about-shrm/pages/default.aspx">About SHRM</a>
                    </li>
                  
                    <li class="col-xs-6">
                      <a href="http://forms.shrm.org/forms/speakers-form">Speaker's Bureau</a>
                    </li>
                  
                    <li class="col-xs-6">
                      <a href="https://www.shrm.org/about-shrm/pages/membership.aspx">Membership</a>
                    </li>
                  
                    <li class="col-xs-6">
                      <a href="https://www.shrm.org/about-shrm/pages/copyright--permissions.aspx">Copyright & Permissions</a>
                    </li>
                  
                    <li class="col-xs-6">
                      <a href="https://www.shrm.org/about-shrm/pages/bylaws--code-of-ethics.aspx">Bylaws & Code of Ethics</a>
                    </li>
                  
                    <li class="col-xs-6">
                      <a href="http://www.shrm.org/mediakit/pages/default.aspx">Advertise with Us</a>
                    </li>
                  
                    <li class="col-xs-6">
                      <a href="https://www.shrm.org/assets/shrmcareers/index.html">Career Opportunities</a>
                    </li>
                  
                    <li class="col-xs-6">
                      <a href="http://hrjobs.shrm.org/jobs/products">Post a Job</a>
                    </li>
                  
                    <li class="col-xs-6">
                      <a href="https://www.shrm.org/about-shrm/press-room/pages/default.aspx">Press Room</a>
                    </li>
                  
              </ul>
            </div>
            <div class="col-sm-4">
              <div class="clearfix">
                <h4 class="footer-col-title">OUR AFFILIATES</h4>
                <ul class="col-sm-12 col-xs-6">
                  <li class="dropdown"> <a id="footer-international" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="current-site">SHRM Global</span> <i class="fa fa-angle-down"></i> </a>
                    <ul class="dropdown-menu" aria-labelledby="footer-international">
                      <li class="collapse"><a href="/">SHRM Global</a></li>
                      <li class="collapse in" data-loc="/about-shrm/Pages/SHRM-China.aspx"><a href="/about-shrm/Pages/SHRM-China.aspx" >SHRM China</a></li>
                      <li class="collapse in" data-loc="/india/"><a href="http://www.shrmindia.org/">SHRM India</a></li>
                      <li class="collapse in" data-loc="/about-shrm/Pages/SHRM-MENA.aspx"><a href="/about-shrm/Pages/SHRM-MENA.aspx">SHRM MENA</a></li>
                    </ul>
                    <script>
											$winLocPath = window.location.pathname; 
											$('ul[aria-labelledby="footer-international"] li').each(function(ind,elm){
												$dlc = $(elm).attr('data-loc') || ''
												 $winLocPath.toLowerCase().lastIndexOf($dlc.toLowerCase() , 0) === 0 ? 
												 	$(elm).removeClass('in').siblings().addClass('in')
													.closest('.dropdown').find('.current-site')
													.text( $(elm).text()) :
													false
											});
										</script> 
                  </li>
                  <li><a href="/about/foundation/pages/default.aspx">SHRM Foundation</a>
                    <a id="ctl00_ctl75_hl_Donate" class="btn btn-success btn-cta" href="http://www.shrm.org/about/foundation/supportthefoundation/contributions/pages/default.aspx" target="_blank">DONATE</a>
                  </li>
                </ul>
                <ul class="col-sm-12 col-xs-6">
                  <li><a href="http://www.cfgi.org" class="link-external" target="_blank">Council for Global Immigration</a></li>
                  <li><a href="http://www.hrps.org/?webSyncID=6736b3a1-9b80-cc34-3ee9-249777c11d0d&amp;sessionGUID=3ad1d590-98a0-5952-5dda-eb0bb951ec89" class="link-external" target="_blank">HR People + Strategy</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom clearfix text-center">
        <div class="footer-contact-us"> <a href="/about-shrm/Pages/Contact-Us.aspx">Contact Us</a> <span class="links-divider">|</span> <a href="tel:800.283.SHRM (7476)">800.283.SHRM (7476)</a> </div>
        <div class="legal-nav"> <span class="copyright">&copy; 2016 SHRM. All Rights Reserved</span>
          <ul class="list-unstyled legal-links">
            <li><a href="/about-shrm/Pages/Privacy-Policy.aspx">Privacy Policy</a></li>
            <li><span class="links-divider">|</span></li>
            <li><a href="/about-shrm/Pages/Privacy-Policy.aspx#California">Your California Privacy Rights</a></li>
            <li><span class="links-divider">|</span></li>
            <li><a href="/about-shrm/Pages/Terms-of-Use.aspx">Terms of Use</a></li>
            <li><span class="links-divider">|</span></li>
            <li><a href="/about-shrm/Pages/Site-Map.aspx">Site Map</a></li>
          </ul>
        </div>
        <small class="text-center small">SHRM provides content as a service to its readers and members. It does not offer legal advice, and cannot guarantee the accuracy or suitability of its content for a particular purpose. <a href="/about-shrm/Pages/Terms-of-Use.aspx#Disclaimer">Disclaimer</a></small> </div>
    </div>
  </div>
</div>

							    </div><!--/ .shrm-site	------------------------------------------------------>
							
</div>
						</div>
						<!-- This should stay here -->
						<div id="DeltaFormDigest">
	
							
								<script type="text/javascript">//<![CDATA[
        var formDigestElement = document.getElementsByName('__REQUESTDIGEST')[0];
        if (!((formDigestElement == null) || (formDigestElement.tagName.toLowerCase() != 'input') || (formDigestElement.type.toLowerCase() != 'hidden') ||
            (formDigestElement.value == null) || (formDigestElement.value.length <= 0)))
        {
            formDigestElement.value = '0x106519C9A4BD8E25E2D11AD5C2AB3A414627AD31EBEC27E07359692EEDF01DA8805A9597B43D1FB32CB191E20A6DAC8B861C1F74230A952FCBBAE185A6267B99,30 Jul 2016 20:00:13 -0000';
            g_updateFormDigestPageLoaded = new Date();
        }
        //]]>
        </script>
							
						
</div>
					</div>
				</div>
			</div>
			<!-- End - Working area -->
			
			<!-- Start - Hidden staff (do not remove this) -->
			
			
			
			
			

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);function loadMDN2() { EnsureScript('MDN.js', typeof(loadFilterFn), null); }
function loadMDN1() { ExecuteOrDelayUntilScriptLoaded(loadMDN2, 'sp.ribbon.js'); }
_spBodyOnLoadFunctionNames.push('loadMDN1');
function _spNavigateHierarchyEx(nodeDiv, dataSourceId, dataPath, url, listInContext, type, additionalQString) {
    CoreInvoke('SetAdditionalNavigateHierarchyQString', additionalQString);
    g_originalSPNavigateFunc(nodeDiv, dataSourceId, dataPath, url, listInContext, type);
}

g_originalSPNavigateFunc = _spNavigateHierarchy;
_spNavigateHierarchy = _spNavigateHierarchyEx;

            ExecuteOrDelayUntilScriptLoaded(
                function() 
                {                    
                    Srch.ScriptApplicationManager.get_current().states = {"webUILanguageName":"en-US","webDefaultLanguageName":"en-US","contextUrl":"https://www.shrm.org/about-shrm","contextTitle":"About SHRM","supportedLanguages":[{"id":1025,"label":"Arabic"},{"id":1093,"label":"Bengali"},{"id":1026,"label":"Bulgarian"},{"id":1027,"label":"Catalan"},{"id":2052,"label":"Chinese (Simplified)"},{"id":1028,"label":"Chinese (Traditional)"},{"id":1050,"label":"Croatian"},{"id":1029,"label":"Czech"},{"id":1030,"label":"Danish"},{"id":1043,"label":"Dutch"},{"id":1033,"label":"English"},{"id":1035,"label":"Finnish"},{"id":1036,"label":"French"},{"id":1031,"label":"German"},{"id":1032,"label":"Greek"},{"id":1095,"label":"Gujarati"},{"id":1037,"label":"Hebrew"},{"id":1081,"label":"Hindi"},{"id":1038,"label":"Hungarian"},{"id":1039,"label":"Icelandic"},{"id":1057,"label":"Indonesian"},{"id":1040,"label":"Italian"},{"id":1041,"label":"Japanese"},{"id":1099,"label":"Kannada"},{"id":1042,"label":"Korean"},{"id":1062,"label":"Latvian"},{"id":1063,"label":"Lithuanian"},{"id":1086,"label":"Malay"},{"id":1100,"label":"Malayalam"},{"id":1102,"label":"Marathi"},{"id":1044,"label":"Norwegian"},{"id":1045,"label":"Polish"},{"id":1046,"label":"Portuguese (Brazil)"},{"id":2070,"label":"Portuguese (Portugal)"},{"id":1094,"label":"Punjabi"},{"id":1048,"label":"Romanian"},{"id":1049,"label":"Russian"},{"id":3098,"label":"Serbian (Cyrillic)"},{"id":2074,"label":"Serbian (Latin)"},{"id":1051,"label":"Slovak"},{"id":1060,"label":"Slovenian"},{"id":3082,"label":"Spanish (Spain)"},{"id":2058,"label":"Spanish (Mexico)"},{"id":1053,"label":"Swedish"},{"id":1097,"label":"Tamil"},{"id":1098,"label":"Telugu"},{"id":1054,"label":"Thai"},{"id":1055,"label":"Turkish"},{"id":1058,"label":"Ukrainian"},{"id":1056,"label":"Urdu"},{"id":1066,"label":"Vietnamese"}],"navigationNodes":[{"id":0,"name":"This Site","url":"~site/_layouts/15/osssearchresults.aspx?u={contexturl}","promptString":"Search this site"}],"showAdminDetails":false,"defaultPagesListName":"Pages","isSPFSKU":false,"userAdvancedLanguageSettingsUrl":"/about-shrm/_layouts/15/regionalsetng.aspx?type=user\u0026Source=https%3A%2F%2Fwww%2Eshrm%2Eorg%2Fabout%2Dshrm%2FPages%2FPrivacy%2DPolicy%2Easpx\u0026ShowAdvLang=1","defaultQueryProperties":{"culture":1033,"uiLanguage":1033,"summaryLength":180,"desiredSnippetLength":90,"enableStemming":true,"enablePhonetic":false,"enableNicknames":false,"trimDuplicates":true,"bypassResultTypes":false,"enableInterleaving":true,"enableQueryRules":true,"processBestBets":true,"enableOrderingHitHighlightedProperty":false,"hitHighlightedMultivaluePropertyLimit":-1,"processPersonalFavorites":true}};
                    Srch.U.trace(null, 'SerializeToClient', 'ScriptApplicationManager state initialized.');
                }, 'Search.ClientControls.js');var g_clientIdDeltaPlaceHolderMain = "DeltaPlaceHolderMain";
var g_clientIdDeltaPlaceHolderPageTitleInTitleArea = "ctl00_DeltaPlaceHolderPageTitleInTitleArea";
var g_clientIdDeltaPlaceHolderUtilityContent = "DeltaPlaceHolderUtilityContent";
//]]>
</script>
</form>
		<span id="DeltaPlaceHolderUtilityContent">
			
		</span>
		<script type="text/javascript">// <![CDATA[ 


            var g_Workspace = "s4-workspace";
		 // ]]>
</script>
		


		
		<img id="ctl00_PageViewTracker_imgTemp" src="../../_layouts/15/SHRM.Core/utility/pageviewtracker.aspx?user=Anonymous&amp;url=%2fabout-shrm%2fPages%2fPrivacy-Policy.aspx" style="height:1px;width:1px;" />
<script type="text/javascript">
    $(document).ready(function () {
        var src = $("#ctl00_PageViewTracker_imgTemp").attr("src").replace(/\&url=(.+)/, "&url=");
        $("a[rel|='Download']").click(function (event) {
            var href = $(this).attr("href");
            $("#ctl00_PageViewTracker_imgTemp").attr("src", src + href);
        });

        // Capturing PDF Data URLs on click
        $('a[href$=".pdf"]').click(function() {
            setHrefSourceforPDFandExcel(this);
        });

        // Capturing Excel Data URLs on click
        $('a[href$=".xls"]').click(function() {
            setHrefSourceforPDFandExcel(this);
        });
    });
    function setHrefSourceforPDFandExcel(element) {
        var urlhref = $(element).attr('href');
        var urlpathwithdomain = document.createElement("a");
        urlpathwithdomain.href = urlhref;
        var src = $("#ctl00_PageViewTracker_imgTemp").attr("src");
        src = src.replace(/\&url=(.+)/, "&url=");
        $("#ctl00_PageViewTracker_imgTemp").attr("src", src + urlpathwithdomain.pathname);
    }
</script>

        
<script type="text/javascript">
    _SHRM = {
        MemberInfo: {
            IsLoggedIn: function() { return 'False'; },
            SsoUuid: function() { return ''; },
            IsMembershipActive: function() { return 'False'; },
            UserFullName: function() { return ', '; },
            UserMemberId: function(){return ''}
        }
    };
</script>

	</body>
</html>
