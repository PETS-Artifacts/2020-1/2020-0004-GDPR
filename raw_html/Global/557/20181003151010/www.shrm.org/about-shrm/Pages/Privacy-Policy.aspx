

<!DOCTYPE html>

<html xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml" dir="ltr" lang="en-US">
	<head><meta http-equiv="X-UA-Compatible" content="IE=11" /><meta name="GENERATOR" content="Microsoft SharePoint" /><meta http-equiv="Content-type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta http-equiv="Expires" content="0" /><meta http-equiv="Accept-CH" content="DPR, Width" /><meta name="BreadCrumbText" content="About SHRM,Privacy Policy," />
	<meta name="BreadCrumb" content="{{Privacy Policy,/about-shrm/Pages/Privacy-Policy.aspx}{About SHRM,/about-shrm/Pages/default.aspx}}" />
	<meta name="DocumentDate" content="2018-10-02" />
	<meta name="MigratedModifiedDate" content="10/2/2018 12:29:37 PM" />
	<meta name="MetaDescription" content=" 
   
      &amp;#160;Privacy&amp;#160;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource..." />
	<meta name="MetaKeywords" content="" />
	<meta name="PageGuid" content="05f756a6-7090-4d03-976f-a430f545b58e" />
	<meta name="PageName" content="Privacy-Policy" />
	<meta name="SHRMMemberOnly" content="False" />
	<meta name="Abstract" content=" 
   
      &nbsp;Privacy&nbsp;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource profession. &nbsp;In Part I of this Privacy Statement we set forth SHRM's Privacy Policy as to Personal Information which we collect online through&nbsp;www.shrm.org&nbsp;or other SHRM websites which link to this Privacy Statement from their site (collectively, &quot;SHRM Websites&quot;).&nbsp;In Part II of this Privacy Statement we set forth SHRM's Privacy Policy as to Personal Information which we collect other than through SHRM Websites.&nbsp;&quot;Personal Information&quot;, is any information that enables us to identify you, directly or indirectly, by reference to an identifier such as your name, identification number, location data, online identifier or one or more factors specific to you. Personal Information includes &quot;sensitive Personal Information&quot; and &quot;pseudonymised Personal Information&quot; but excludes anon" />
	<meta name="RollupImage" content="" />
	<meta name="ArticleAuthor" content="" />
	<meta name="ArticleIsToolContent" content="False" />
	<meta name="ArticleSocialToolsEnabled" content="False" />
	<meta name="Taxonomy" content="N/A" />
	
<meta name="SHRM" content="v=1.62.8379.436;" />
<meta name="msapplication-TileImage" content="/_layouts/15/images/SharePointMetroAppTile.png" /><meta name="msapplication-TileColor" content="#0072C6" /><title>
	
    Privacy Policy

</title>
		
		<!-- IE -->
		<link rel="shortcut icon" type="image/x-icon" href="https://cdn.shrm.org/image/upload/favicon.ico" />
		<!-- other browsers -->
		<link rel="icon" type="image/x-icon" href="https://cdn.shrm.org/image/upload/favicon.ico" /><link rel="stylesheet" type="text/css" href="/_layouts/15/1033/styles/Themable/corev15.css?rev=OqAycmyMLoQIDkAlzHdMhQ%3D%3D"/>
<script type="text/javascript" src="/_layouts/15/init.js?rev=Xpo7ARBt8xBROO1h5n3s6g%3D%3D"></script>
<script type="text/javascript" src="/ScriptResource.axd?d=pVhMdx7BaO7vWyDvmfSFy2UmUP0AZeOYCKdSH5vzevhISxxotfv42LBXS-7tTSjJj0TffjY4wbwAF7mP-zNBnhHtYYSSc5oL7iJreKPYAc7Mf4OdX6ca9cmkWj41yNKcdJFqS-i-jnY6SuvgDiG_Qf0vE5Q0fV6cL-k8_maa5NmH4AQM-B-u-Jh0aeu7IgN60&amp;t=ffffffffb0622999"></script>
<script type="text/javascript" src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D"></script>
<script type="text/javascript" src="/ScriptResource.axd?d=7whSi3pO0Sr1q7wtXvGRhCLH5v0Hiv7OvHHHzO4fgUhg34hBa_GU9Tw_QvR8u_PEBFsbnvVVfzUmpgjKcC8n5USDVWEqSz2MwfFIySR2RS6r-Du8U1E1UFib4NdVYImD6Bl9DRe3DJtp6Pt1rOrpLxKWrXyq6EnC1udd8iI6ppclvi73BLQs6J7hCQSkoApT0&amp;t=ffffffffb0622999"></script>
<script type="text/javascript">RegisterSod("initstrings.js", "\u002f_layouts\u002f15\u002f1033\u002finitstrings.js?rev=2N3pk\u00252FwjnG3Tj9o5NrqHcg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("strings.js", "\u002f_layouts\u002f15\u002f1033\u002fstrings.js?rev=y6khbhv1Os2YPb5X0keqRg\u00253D\u00253D");RegisterSodDep("strings.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sp.init.js", "\u002f_layouts\u002f15\u002fsp.init.js?rev=jvJC3Kl5gbORaLtf7kxULQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=yNk\u00252FhRzgBn40LJVP\u00252BqfgdQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002f15\u002fsp.ui.dialog.js?rev=3Oh2QbaaiXSb7ldu2zd6QQ\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.init.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f15\u002fcore.js?rev=GpU7vxyOqzS0F9OfEX3CCw\u00253D\u00253D");RegisterSodDep("core.js", "strings.js");</script>
<script type="text/javascript">RegisterSod("mQuery.js", "\u002f_layouts\u002f15\u002fmquery.js?rev=VYAJYBo5H8I3gVSL3MzD6A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("callout.js", "\u002f_layouts\u002f15\u002fcallout.js?rev=ryx2n4ePkYj1\u00252FALmcsXZfA\u00253D\u00253D");RegisterSodDep("callout.js", "strings.js");RegisterSodDep("callout.js", "mQuery.js");RegisterSodDep("callout.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clienttemplates.js", "\u002f_layouts\u002f15\u002fclienttemplates.js?rev=NjHQZTfQs\u00252BiDIjz4PK\u00252FeWg\u00253D\u00253D");RegisterSodDep("clienttemplates.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sharing.js", "\u002f_layouts\u002f15\u002fsharing.js?rev=XxxHIxIIc8BsW9ikVc6dgA\u00253D\u00253D");RegisterSodDep("sharing.js", "strings.js");RegisterSodDep("sharing.js", "mQuery.js");RegisterSodDep("sharing.js", "clienttemplates.js");RegisterSodDep("sharing.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clientrenderer.js", "\u002f_layouts\u002f15\u002fclientrenderer.js?rev=PWwV4FATEiOxN90BeB5Hzw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("srch.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=Srch\u00252EResources\u0026rev=fPbk\u00252B7qByTbyuNVTMptzRw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("search.clientcontrols.js", "\u002f_layouts\u002f15\u002fsearch.clientcontrols.js?rev=\u00252BN5FhPOhdMDlyCSUMQPtrg\u00253D\u00253D");RegisterSodDep("search.clientcontrols.js", "sp.init.js");RegisterSodDep("search.clientcontrols.js", "clientrenderer.js");RegisterSodDep("search.clientcontrols.js", "srch.resources.resx");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002f15\u002fsp.runtime.js?rev=5f2WkYJoaxlIRdwUeg4WEg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.search.js", "\u002f_layouts\u002f15\u002fsp.search.js?rev=dMkPlEXpdY6iJ\u00252FsY5RsB0g\u00253D\u00253D");RegisterSodDep("sp.search.js", "sp.init.js");RegisterSodDep("sp.search.js", "sp.runtime.js");</script>
<script type="text/javascript">RegisterSod("ajaxtoolkit.js", "\u002f_layouts\u002f15\u002fajaxtoolkit.js?rev=4rOiCbaFgJMmqw9Ojtpa6g\u00253D\u00253D");RegisterSodDep("ajaxtoolkit.js", "search.clientcontrols.js");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002f15\u002fsp.js?rev=lrxLgKOmx0nl2elVy0T07w\u00253D\u00253D");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("cui.js", "\u002f_layouts\u002f15\u002fcui.js?rev=LPKF2\u00252BgWXqwwaFh34pQUlA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002f15\u002fsp.core.js?rev=tZDGLPOvY1bRw\u00252BsgzXpxTg\u00253D\u00253D");RegisterSodDep("sp.core.js", "strings.js");RegisterSodDep("sp.core.js", "sp.init.js");RegisterSodDep("sp.core.js", "core.js");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002f15\u002finplview.js?rev=iMf5THfqukSYut7sl9HwUg\u00253D\u00253D");RegisterSodDep("inplview", "strings.js");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("ribbon", "\u002f_layouts\u002f15\u002fsp.ribbon.js?rev=1F3TSGFB5\u00252FyAaRkjYHJL5w\u00253D\u00253D");RegisterSodDep("ribbon", "strings.js");RegisterSodDep("ribbon", "core.js");RegisterSodDep("ribbon", "sp.core.js");RegisterSodDep("ribbon", "sp.js");RegisterSodDep("ribbon", "cui.js");RegisterSodDep("ribbon", "sp.res.resx");RegisterSodDep("ribbon", "sp.runtime.js");RegisterSodDep("ribbon", "inplview");</script>
<script type="text/javascript">RegisterSod("sp.ui.policy.resources.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EUI\u00252EPolicy\u00252EResources\u0026rev=wtVfoqgvnf2cuN894ZirvA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mdn.js", "\u002f_layouts\u002f15\u002fmdn.js?rev=CZrBt1\u00252Fch9YeLFLJtB0mvg\u00253D\u00253D");RegisterSodDep("mdn.js", "sp.init.js");RegisterSodDep("mdn.js", "core.js");RegisterSodDep("mdn.js", "sp.runtime.js");RegisterSodDep("mdn.js", "sp.js");RegisterSodDep("mdn.js", "cui.js");RegisterSodDep("mdn.js", "ribbon");RegisterSodDep("mdn.js", "sp.ui.policy.resources.resx");</script>
<script type="text/javascript">RegisterSod("userprofile", "\u002f_layouts\u002f15\u002fsp.userprofiles.js?rev=p5tCOm\u00252FlHUwcfll7W3pKNw\u00253D\u00253D");RegisterSodDep("userprofile", "sp.runtime.js");</script>
<script type="text/javascript">RegisterSod("followingcommon.js", "\u002f_layouts\u002f15\u002ffollowingcommon.js?rev=jWqEDmcjCSPmnQw2ZIfItQ\u00253D\u00253D");RegisterSodDep("followingcommon.js", "strings.js");RegisterSodDep("followingcommon.js", "sp.js");RegisterSodDep("followingcommon.js", "userprofile");RegisterSodDep("followingcommon.js", "core.js");RegisterSodDep("followingcommon.js", "mQuery.js");</script>
<script type="text/javascript">RegisterSod("profilebrowserscriptres.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=ProfileBrowserScriptRes\u0026rev=J5HzNnB\u00252FO1Id\u00252FGI18rpRcw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.mysitecommon.js", "\u002f_layouts\u002f15\u002fsp.ui.mysitecommon.js?rev=Ua8qmZSU9nyf53S7PEyJwQ\u00253D\u00253D");RegisterSodDep("sp.ui.mysitecommon.js", "sp.init.js");RegisterSodDep("sp.ui.mysitecommon.js", "sp.runtime.js");RegisterSodDep("sp.ui.mysitecommon.js", "userprofile");RegisterSodDep("sp.ui.mysitecommon.js", "profilebrowserscriptres.resx");</script>
<link type="text/xml" rel="alternate" href="/about-shrm/_vti_bin/spsdisco.aspx" />
			
    
<meta itemprop='name' content='Privacy Policy' />
<meta itemprop='url' content='https://www.shrm.org/about-shrm/pages/privacy-policy.aspx' />
<meta itemprop='image' content='https://www.shrm.org/publishingimages/shrm-sharing-logo-square-v5.png' />
<meta itemprop='author' content='' />
<meta itemprop='description' content=' 
   
      &amp;#160;Privacy&amp;#160;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource...' />
<meta itemprop='datePublished' content='10/2/2018 4:29:37 PM' />
<meta itemprop='dateModified' content='10/2/2018 4:29:37 PM' />

<!-- Twitter Card data -->
<meta name='twitter:card' content='summary'>
<meta name='twitter:site' content='@'>
<meta name='twitter:title' content='Privacy Policy'>
<meta name='twitter:description' content=' 
   
      &amp;#160;Privacy&amp;#160;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource...'>

<meta name='twitter:image' content='https://www.shrm.org/publishingimages/shrm-sharing-logo-square-v5.png'>
<!-- Open Graph data -->
<meta property='og:title' content='Privacy Policy' />
<meta property='og:type' content='article' />
<meta property='og:url' content='https://www.shrm.org/about-shrm/pages/privacy-policy.aspx' />
<meta property='og:image' content='https://www.shrm.org/publishingimages/shrm-sharing-logo-square-v5.png' />
<meta property='og:description' content=' 
   
      &amp;#160;Privacy&amp;#160;Statement Updated October 1, 2018.Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource...' />
<meta property='og:site_name' content='SHRM' />
<meta property='article:published_time' content='10/2/2018 4:29:37 PM' />
<meta property='article:modified_time' content='10/2/2018 4:29:37 PM' />



<meta property='fb:app_id' content='649819231827420' />















    
    <script type='application/ld+json'>{ "@context": "http://schema.org", "@type": "NewsArticle", "mainEntityOfPage": { "@type": "WebPage", "@id": "https://www.shrm.org/about-shrm/Pages/Privacy-Policy.aspx"}, "headline": "Privacy Policy", "articleBody": "", "image": "https://www.shrm.org/PublishingImages/shrm-sharing-logo-square-v5.png", "datePublished": "10/2/2018 12:29:37 PM", "dateModified": "10/2/2018 12:29:37 PM", "author": { "@type": "Person", "name": "SHRM" }, "publisher": { "name": "SHRM", "@type": "Organization", "logo": { "@type": "ImageObject", "url": "https://www.shrm.org/_layouts/15/SHRM.Core/design/images/SHRMLogo.jpg" } }, "description": "", "isAccessibleForFree": "True"} </script>



			<!-- _lcid="1033" _version="15.0.5049" _dal="1" -->
<!-- _LocalBinding -->

<link rel="canonical" href="https://www.shrm.org:443/about-shrm/Pages/Privacy-Policy.aspx" />
			
		
        <!-- CssRegistration will remain here for now -->
		
		<!-- Start - Your references -->
        



<script>
var SHRMCoreVars =(function () {
	var private = {
		'Member2MemberSubsiteUrl'	: '/ResourcesAndTools/tools-and-samples/member2member',
        'ExpressRequestWebUrl': '/ResourcesAndTools/tools-and-samples/exreq/',
        'SearchDomain': 'https://www.shrm.org',
	    'SearchQueryUrl': '/search/pages/default.aspx',
        'SearchTopics': 'Benefits, Business Acumen, California Resources, Communication, Compensation, Consultation, Critical Evaluation, Employee Relations, Ethical Practice, Global & Cultural Effectiveness,Global HR, Labor Relations,Leadership & Navigation,Organizational & Employee Development,Relationship Management,Risk Management,Talent Acquisition,Technology',
        'SearchSourceUrl': 'https://edit.shrm.org',
        'SearchPublicUrl': 'https://www.shrm.org',
        'CobaltSearchEndpointUrl': 'https://shrmcrmapi.cobaltsaas.com/api/v1.0',
        'CobaltSearchDatabaseEnabled': '1',
        'ListenToScrollerAdSettings': '1',
        'CurrentSiteUrl': 'https://www.shrm.org',
        'UseSPSearch': '1'
	 };

	return {
		get: function(name) { return private[name]; }
	};
})();
</script>




<link rel="stylesheet" type="text/css" href="/v1628379436/core/assets/dist/styles.min.css ">
<script src="/v1628379436/core/assets/dist/scripts.min.js"></script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyDbkBtKmBsBrLXHtfbDxsavW5mgnytrPYY&libraries=places"></script>  


<link rel="stylesheet" type="text/css" href="/Style%20Library/temp.css">

        <style>
            li.ms-core-menu-item[text='SHRM Foundation - Management'] { display: none; }
            li.ms-core-menu-item[text='SHRM Foundation - Settings'] { display: none; }
            li.ms-core-menu-item[text='SHRM HRPS - Management'] { display: none; }
            li.ms-core-menu-item[text='SHRM HRPS - Settings'] { display: none; }
        </style>
        
    <script type="text/javascript">
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        (function() {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
    </script>
    <script type='text/javascript'>$(document).ready(function () {RegisterGoogleTag('AdWordsScript', '');});</script>




    <script type="text/javascript">
        function CloadinarySetCaption() {
            console.log('cloudinarysetcaption: start');
            $(".article-content img").each(function (i) {
                if ($(this).hasClass("16x9")) { $(this).removeClass("16x9").addClass("ratio-16x9"); 
									}else if ($(this).hasClass("3x4")) { $(this).removeClass("3x4").addClass("ratio-3x4"); 
									}else if ($(this).hasClass("1x1")) { $(this).removeClass("1x1").addClass("ratio-1x1"); 
									};            
            });
            $(".article-content img[data-caption]").each(function (i) {
                if ($(this).attr('data-caption') != null || $(this).attr('data-caption') != '' || $(this).attr('data-caption') != 'undefined') {
                    $(this).wrap("<figure class='imagewithcaption content-figure'></figure>")
											.parent().append("<figcaption>" + $(this).attr('data-caption') + "</figcaption>")
											.addClass($(this).attr('class'));
                }
            });
            if ($(".article-cover-figure img").length == 0) {
                $(".article-cover-figure").hide();
            } else {
                $('.article-cover-figure img').one('load', function() {
									$(this).closest('.article-cover-figure').addClass(function() {
										var figClass = 'ratio-';
										if ($(this).height() === $(this).width()) {
												figClass += '1x1';
										} else if ($(this).height() > $(this).width()) {
												figClass += '3x4';
										} else if($(this).width() > $(this).height()) {
												figClass += '16x9';
										}
										return figClass;
									});		
								}).each(function() {
									 if(this.complete || /*for IE 10-*/ $(this).height() > 0)
										 $(this).load();
								});
								
            }
            if ($(".article-cover-figure figcaption div").eq(1).text() == "" || $(".article-cover-figure figcaption div").eq(1).html() == "") {
                $(".article-cover-figure figcaption").hide();
            }
            console.log('cloudinarysetcaption: end');
        }
        $(document).ready(function () {
            CloadinarySetCaption();
        });
    </script>



		
		
		<!-- End - your references -->
		
		<script type="text/javascript">
		    $(document).ready(function () {
		        $("html head link[rel=canonical]").first().attr("href", $("html head link[rel=canonical]").first().attr("href").toLowerCase().replace(":80", "").replace(":443", ""));
		    });
        </script>
		<!--	Disable Firefox Microsoft Office Plugin Prompt (if you're making copy of this master please make sure you include this part as well)	-->
        <script type="text/javascript">
	        function ProcessImn() { }
	        function ProcessImnMarkers() { }
        </script>

        <style>
            li.ms-core-menu-item[text='SHRM HRPS - Management'] { display: none; }
            li.ms-core-menu-item[text='SHRM HRPS - Settings'] { display: none; }
            li.ms-core-menu-item[text='SHRM Foundation - Management'] { display: none; }
            li.ms-core-menu-item[text='SHRM Foundation - Settings'] { display: none; }
        </style>
        
		<!--	GTM CALL (if you're making copy of this master please make sure you include this part as well)	-->
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-N6W5WC');</script>
		<!-- End Google Tag Manager -->
	</head>
	<body id="ctl00_mainbody" class="shrm-role-member shrm-role-member-expire" ng-app="photoAlbumApp">
        <!--googleoff: all-->
		

<script>
  //window.fbAsyncInit = function() { FB.init({ appId: '649819231827420', xfbml: true, version: 'v2.5' }); };

  //(function(d, s, id){
  //   var js, fjs = d.getElementsByTagName(s)[0];
  //   if (d.getElementById(id)) {return;}
  //   js = d.createElement(s); js.id = id;
  //   js.src = "//connect.facebook.net/en_US/sdk.js";
  //   fjs.parentNode.insertBefore(js, fjs);
    //}(document, 'script', 'facebook-jssdk'));
    function shrm_encodeURI(s) { return encodeURIComponent(s); }
    function RightsLinkPopUp() {
        var url = "https://s100.copyright.com/AppDispatchServlet";
        var location = url + "?publisherName=" + shrm_encodeURI("shrm") + "&publication=" + shrm_encodeURI("Legal_Issues") + "&title=" + shrm_encodeURI("Justices Hear ERISA Reimbursement Case") + "&publicationDate=" + shrm_encodeURI("11/11/2015 12:00:00 AM") + "&contentID=" + shrm_encodeURI("6badda72-62e7-49e8-b4bd-ebbd02a72a17") + "&charCnt=" + shrm_encodeURI("7399") + "&orderBeanReset=" + shrm_encodeURI("True");
        window.open(location, "RightsLink", "location=no,toolbar=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=650,height=550");
    }
</script>

  		
        <noscript><div class='noindex'>You may be trying to access this site from a secured browser on the server. Please enable scripts and reload this page.</div></noscript>
  		
  		<form method="post" action="./Privacy-Policy.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div class="aspNetHidden">
<input type="hidden" name="_wpcmWpid" id="_wpcmWpid" value="" />
<input type="hidden" name="wpcmVal" id="wpcmVal" value="" />
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value="" />
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value="" />
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value="" />
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="0x715BA31EAECE0EC73427FA96343A12CCF28BB671F315FCAB28BDCFC476182496963247C69C15E0AF50F5C6872D4C8DB68F7430B477A760B16E116204CF2B84B7,03 Oct 2018 15:10:10 -0000" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMTc4MTEzMjE5D2QWAmYPZBYCAgEPZBYEAgEPZBYOAgYPZBYCZg8WAh4EVGV4dAW1EDxtZXRhIG5hbWU9IkJyZWFkQ3J1bWJUZXh0IiBjb250ZW50PSJBYm91dCBTSFJNLFByaXZhY3kgUG9saWN5LCIgLz4KCTxtZXRhIG5hbWU9IkJyZWFkQ3J1bWIiIGNvbnRlbnQ9Int7UHJpdmFjeSBQb2xpY3ksL2Fib3V0LXNocm0vUGFnZXMvUHJpdmFjeS1Qb2xpY3kuYXNweH17QWJvdXQgU0hSTSwvYWJvdXQtc2hybS9QYWdlcy9kZWZhdWx0LmFzcHh9fSIgLz4KCTxtZXRhIG5hbWU9IkRvY3VtZW50RGF0ZSIgY29udGVudD0iMjAxOC0xMC0wMiIgLz4KCTxtZXRhIG5hbWU9Ik1pZ3JhdGVkTW9kaWZpZWREYXRlIiBjb250ZW50PSIxMC8yLzIwMTggMTI6Mjk6MzcgUE0iIC8+Cgk8bWV0YSBuYW1lPSJNZXRhRGVzY3JpcHRpb24iIGNvbnRlbnQ9IiANCiAgIA0KICAgICAgJmFtcDsjMTYwO1ByaXZhY3kmYW1wOyMxNjA7U3RhdGVtZW50IFVwZGF0ZWQgT2N0b2JlciAxLCAyMDE4LldlbGNvbWUgdG8gdGhlIFNvY2lldHkgZm9yIEh1bWFuIFJlc291cmNlIE1hbmFnZW1lbnQgKFNIUk0pLiBTSFJNIGlzIHRoZSBsZWFkaW5nIG1lbWJlcnNoaXAgYXNzb2NpYXRpb24gZm9yIHRoZSBodW1hbiByZXNvdXJjZS4uLiIgLz4KCTxtZXRhIG5hbWU9Ik1ldGFLZXl3b3JkcyIgY29udGVudD0iIiAvPgoJPG1ldGEgbmFtZT0iUGFnZUd1aWQiIGNvbnRlbnQ9IjA1Zjc1NmE2LTcwOTAtNGQwMy05NzZmLWE0MzBmNTQ1YjU4ZSIgLz4KCTxtZXRhIG5hbWU9IlBhZ2VOYW1lIiBjb250ZW50PSJQcml2YWN5LVBvbGljeSIgLz4KCTxtZXRhIG5hbWU9IlNIUk1NZW1iZXJPbmx5IiBjb250ZW50PSJGYWxzZSIgLz4KCTxtZXRhIG5hbWU9IkFic3RyYWN0IiBjb250ZW50PSIgDQogICANCiAgICAgICZuYnNwO1ByaXZhY3kmbmJzcDtTdGF0ZW1lbnQgVXBkYXRlZCBPY3RvYmVyIDEsIDIwMTguV2VsY29tZSB0byB0aGUgU29jaWV0eSBmb3IgSHVtYW4gUmVzb3VyY2UgTWFuYWdlbWVudCAoU0hSTSkuIFNIUk0gaXMgdGhlIGxlYWRpbmcgbWVtYmVyc2hpcCBhc3NvY2lhdGlvbiBmb3IgdGhlIGh1bWFuIHJlc291cmNlIHByb2Zlc3Npb24uICZuYnNwO0luIFBhcnQgSSBvZiB0aGlzIFByaXZhY3kgU3RhdGVtZW50IHdlIHNldCBmb3J0aCBTSFJNJ3MgUHJpdmFjeSBQb2xpY3kgYXMgdG8gUGVyc29uYWwgSW5mb3JtYXRpb24gd2hpY2ggd2UgY29sbGVjdCBvbmxpbmUgdGhyb3VnaCZuYnNwO3d3dy5zaHJtLm9yZyZuYnNwO29yIG90aGVyIFNIUk0gd2Vic2l0ZXMgd2hpY2ggbGluayB0byB0aGlzIFByaXZhY3kgU3RhdGVtZW50IGZyb20gdGhlaXIgc2l0ZSAoY29sbGVjdGl2ZWx5LCAmcXVvdDtTSFJNIFdlYnNpdGVzJnF1b3Q7KS4mbmJzcDtJbiBQYXJ0IElJIG9mIHRoaXMgUHJpdmFjeSBTdGF0ZW1lbnQgd2Ugc2V0IGZvcnRoIFNIUk0ncyBQcml2YWN5IFBvbGljeSBhcyB0byBQZXJzb25hbCBJbmZvcm1hdGlvbiB3aGljaCB3ZSBjb2xsZWN0IG90aGVyIHRoYW4gdGhyb3VnaCBTSFJNIFdlYnNpdGVzLiZuYnNwOyZxdW90O1BlcnNvbmFsIEluZm9ybWF0aW9uJnF1b3Q7LCBpcyBhbnkgaW5mb3JtYXRpb24gdGhhdCBlbmFibGVzIHVzIHRvIGlkZW50aWZ5IHlvdSwgZGlyZWN0bHkgb3IgaW5kaXJlY3RseSwgYnkgcmVmZXJlbmNlIHRvIGFuIGlkZW50aWZpZXIgc3VjaCBhcyB5b3VyIG5hbWUsIGlkZW50aWZpY2F0aW9uIG51bWJlciwgbG9jYXRpb24gZGF0YSwgb25saW5lIGlkZW50aWZpZXIgb3Igb25lIG9yIG1vcmUgZmFjdG9ycyBzcGVjaWZpYyB0byB5b3UuIFBlcnNvbmFsIEluZm9ybWF0aW9uIGluY2x1ZGVzICZxdW90O3NlbnNpdGl2ZSBQZXJzb25hbCBJbmZvcm1hdGlvbiZxdW90OyBhbmQgJnF1b3Q7cHNldWRvbnltaXNlZCBQZXJzb25hbCBJbmZvcm1hdGlvbiZxdW90OyBidXQgZXhjbHVkZXMgYW5vbiIgLz4KCTxtZXRhIG5hbWU9IlJvbGx1cEltYWdlIiBjb250ZW50PSIiIC8+Cgk8bWV0YSBuYW1lPSJBcnRpY2xlQXV0aG9yIiBjb250ZW50PSIiIC8+Cgk8bWV0YSBuYW1lPSJBcnRpY2xlSXNUb29sQ29udGVudCIgY29udGVudD0iRmFsc2UiIC8+Cgk8bWV0YSBuYW1lPSJBcnRpY2xlU29jaWFsVG9vbHNFbmFibGVkIiBjb250ZW50PSJGYWxzZSIgLz4KCTxtZXRhIG5hbWU9IlRheG9ub215IiBjb250ZW50PSJOL0EiIC8+CglkAgcPZBYCZg8WAh8ABS88bWV0YSBuYW1lPSJTSFJNIiBjb250ZW50PSJ2PTEuNjIuODM3OS40MzY7IiAvPmQCCg9kFgJmD2QWAgIBDxYCHhNQcmV2aW91c0NvbnRyb2xNb2RlCymIAU1pY3Jvc29mdC5TaGFyZVBvaW50LldlYkNvbnRyb2xzLlNQQ29udHJvbE1vZGUsIE1pY3Jvc29mdC5TaGFyZVBvaW50LCBWZXJzaW9uPTE1LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTcxZTliY2UxMTFlOTQyOWMBZAIXD2QWBAIBD2QWBAIBD2QWKgICDxYCHwAFMTxtZXRhIGl0ZW1wcm9wPSduYW1lJyBjb250ZW50PSdQcml2YWN5IFBvbGljeScgLz5kAgQPFgIfAAVbPG1ldGEgaXRlbXByb3A9J3VybCcgY29udGVudD0naHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wYWdlcy9wcml2YWN5LXBvbGljeS5hc3B4JyAvPmQCBg8WAh8ABWk8bWV0YSBpdGVtcHJvcD0naW1hZ2UnIGNvbnRlbnQ9J2h0dHBzOi8vd3d3LnNocm0ub3JnL3B1Ymxpc2hpbmdpbWFnZXMvc2hybS1zaGFyaW5nLWxvZ28tc3F1YXJlLXY1LnBuZycgLz5kAggPFgIfAAUlPG1ldGEgaXRlbXByb3A9J2F1dGhvcicgY29udGVudD0nJyAvPmQCCg8WAh8ABfQBPG1ldGEgaXRlbXByb3A9J2Rlc2NyaXB0aW9uJyBjb250ZW50PScgCiAgIAogICAgICAmYW1wOyMxNjA7UHJpdmFjeSZhbXA7IzE2MDtTdGF0ZW1lbnQgVXBkYXRlZCBPY3RvYmVyIDEsIDIwMTguV2VsY29tZSB0byB0aGUgU29jaWV0eSBmb3IgSHVtYW4gUmVzb3VyY2UgTWFuYWdlbWVudCAoU0hSTSkuIFNIUk0gaXMgdGhlIGxlYWRpbmcgbWVtYmVyc2hpcCBhc3NvY2lhdGlvbiBmb3IgdGhlIGh1bWFuIHJlc291cmNlLi4uJyAvPmQCDA8WAh8ABUA8bWV0YSBpdGVtcHJvcD0nZGF0ZVB1Ymxpc2hlZCcgY29udGVudD0nMTAvMi8yMDE4IDQ6Mjk6MzcgUE0nIC8+ZAIODxYCHwAFPzxtZXRhIGl0ZW1wcm9wPSdkYXRlTW9kaWZpZWQnIGNvbnRlbnQ9JzEwLzIvMjAxOCA0OjI5OjM3IFBNJyAvPmQCEg8WAh8ABSw8bWV0YSBuYW1lPSd0d2l0dGVyOmNhcmQnIGNvbnRlbnQ9J3N1bW1hcnknPmQCFA8WAh8ABSY8bWV0YSBuYW1lPSd0d2l0dGVyOnNpdGUnIGNvbnRlbnQ9J0AnPmQCFg8WAh8ABTQ8bWV0YSBuYW1lPSd0d2l0dGVyOnRpdGxlJyBjb250ZW50PSdQcml2YWN5IFBvbGljeSc+ZAIYDxYCHwAF9gE8bWV0YSBuYW1lPSd0d2l0dGVyOmRlc2NyaXB0aW9uJyBjb250ZW50PScgCiAgIAogICAgICAmYW1wOyMxNjA7UHJpdmFjeSZhbXA7IzE2MDtTdGF0ZW1lbnQgVXBkYXRlZCBPY3RvYmVyIDEsIDIwMTguV2VsY29tZSB0byB0aGUgU29jaWV0eSBmb3IgSHVtYW4gUmVzb3VyY2UgTWFuYWdlbWVudCAoU0hSTSkuIFNIUk0gaXMgdGhlIGxlYWRpbmcgbWVtYmVyc2hpcCBhc3NvY2lhdGlvbiBmb3IgdGhlIGh1bWFuIHJlc291cmNlLi4uJz5kAhwPFgIfAAVrPG1ldGEgbmFtZT0ndHdpdHRlcjppbWFnZScgY29udGVudD0naHR0cHM6Ly93d3cuc2hybS5vcmcvcHVibGlzaGluZ2ltYWdlcy9zaHJtLXNoYXJpbmctbG9nby1zcXVhcmUtdjUucG5nJz5kAh4PFgIfAAU1PG1ldGEgcHJvcGVydHk9J29nOnRpdGxlJyBjb250ZW50PSdQcml2YWN5IFBvbGljeScgLz5kAiAPFgIfAAUtPG1ldGEgcHJvcGVydHk9J29nOnR5cGUnIGNvbnRlbnQ9J2FydGljbGUnIC8+ZAIiDxYCHwAFXjxtZXRhIHByb3BlcnR5PSdvZzp1cmwnIGNvbnRlbnQ9J2h0dHBzOi8vd3d3LnNocm0ub3JnL2Fib3V0LXNocm0vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweCcgLz5kAiQPFgIfAAVsPG1ldGEgcHJvcGVydHk9J29nOmltYWdlJyBjb250ZW50PSdodHRwczovL3d3dy5zaHJtLm9yZy9wdWJsaXNoaW5naW1hZ2VzL3Nocm0tc2hhcmluZy1sb2dvLXNxdWFyZS12NS5wbmcnIC8+ZAImDxYCHwAF9wE8bWV0YSBwcm9wZXJ0eT0nb2c6ZGVzY3JpcHRpb24nIGNvbnRlbnQ9JyAKICAgCiAgICAgICZhbXA7IzE2MDtQcml2YWN5JmFtcDsjMTYwO1N0YXRlbWVudCBVcGRhdGVkIE9jdG9iZXIgMSwgMjAxOC5XZWxjb21lIHRvIHRoZSBTb2NpZXR5IGZvciBIdW1hbiBSZXNvdXJjZSBNYW5hZ2VtZW50IChTSFJNKS4gU0hSTSBpcyB0aGUgbGVhZGluZyBtZW1iZXJzaGlwIGFzc29jaWF0aW9uIGZvciB0aGUgaHVtYW4gcmVzb3VyY2UuLi4nIC8+ZAIoDxYCHwAFLzxtZXRhIHByb3BlcnR5PSdvZzpzaXRlX25hbWUnIGNvbnRlbnQ9J1NIUk0nIC8+ZAIqDxYCHwAFSTxtZXRhIHByb3BlcnR5PSdhcnRpY2xlOnB1Ymxpc2hlZF90aW1lJyBjb250ZW50PScxMC8yLzIwMTggNDoyOTozNyBQTScgLz5kAiwPFgIfAAVIPG1ldGEgcHJvcGVydHk9J2FydGljbGU6bW9kaWZpZWRfdGltZScgY29udGVudD0nMTAvMi8yMDE4IDQ6Mjk6MzcgUE0nIC8+ZAIyDxYCHwAFNzxtZXRhIHByb3BlcnR5PSdmYjphcHBfaWQnIGNvbnRlbnQ9JzY0OTgxOTIzMTgyNzQyMCcgLz5kAgMPZBYCZg9kFgICAQ8WAh8ABb0FPHNjcmlwdCB0eXBlPSdhcHBsaWNhdGlvbi9sZCtqc29uJz57ICJAY29udGV4dCI6ICJodHRwOi8vc2NoZW1hLm9yZyIsICJAdHlwZSI6ICJOZXdzQXJ0aWNsZSIsICJtYWluRW50aXR5T2ZQYWdlIjogeyAiQHR5cGUiOiAiV2ViUGFnZSIsICJAaWQiOiAiaHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9QYWdlcy9Qcml2YWN5LVBvbGljeS5hc3B4In0sICJoZWFkbGluZSI6ICJQcml2YWN5IFBvbGljeSIsICJhcnRpY2xlQm9keSI6ICIiLCAiaW1hZ2UiOiAiaHR0cHM6Ly93d3cuc2hybS5vcmcvUHVibGlzaGluZ0ltYWdlcy9zaHJtLXNoYXJpbmctbG9nby1zcXVhcmUtdjUucG5nIiwgImRhdGVQdWJsaXNoZWQiOiAiMTAvMi8yMDE4IDEyOjI5OjM3IFBNIiwgImRhdGVNb2RpZmllZCI6ICIxMC8yLzIwMTggMTI6Mjk6MzcgUE0iLCAiYXV0aG9yIjogeyAiQHR5cGUiOiAiUGVyc29uIiwgIm5hbWUiOiAiU0hSTSIgfSwgInB1Ymxpc2hlciI6IHsgIm5hbWUiOiAiU0hSTSIsICJAdHlwZSI6ICJPcmdhbml6YXRpb24iLCAibG9nbyI6IHsgIkB0eXBlIjogIkltYWdlT2JqZWN0IiwgInVybCI6ICJodHRwczovL3d3dy5zaHJtLm9yZy9fbGF5b3V0cy8xNS9TSFJNLkNvcmUvZGVzaWduL2ltYWdlcy9TSFJNTG9nby5qcGciIH0gfSwgImRlc2NyaXB0aW9uIjogIiIsICJpc0FjY2Vzc2libGVGb3JGcmVlIjogIlRydWUifSA8L3NjcmlwdD5kAgMPZBYCAgEPZBYCZg88KwAGAGQCHA9kFgJmDxYCHwBlZAIeD2QWAmYPZBYCAgEPFgIfAAVxPHNjcmlwdCB0eXBlPSd0ZXh0L2phdmFzY3JpcHQnPiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtSZWdpc3Rlckdvb2dsZVRhZygnQWRXb3Jkc1NjcmlwdCcsICcnKTt9KTs8L3NjcmlwdD5kAh8PZBYCAgQPFgIeB1Zpc2libGVnZAIDD2QWBAIJD2QWCAIBD2QWAgIXD2QWAgIDDxYCHwJoFgJmD2QWBAICD2QWBgIBDxYCHwJoZAIDDxYCHwJoZAIFDxYCHwJoZAIDDw8WAh4JQWNjZXNzS2V5BQEvZGQCBw9kFggCBg9kFgJmDw8WBh4IQ3NzQ2xhc3MFV3JvdyBwcm9tb3Rpb25hbC1iYXIgaGlkZGVuLXhzIGhpZGRlbi1wcmludCBwcm9tb3Rpb25hbC1iYXItc2Vhc29uYWwgcHJvbW90aW9uYWwtYmFyLXJlZB4EXyFTQgICHwJnZBYIAgEPFgIfAAUaU3BlY2lhbCBQcm9tbyBFeHRlbmRlZCEgPj5kAgMPFgIfAAVNSm9pbiBvciByZW5ldyB3aXRoIGNvZGUgVE9URTIwMTggYnkgT2N0LiAxNSBmb3IgJDIwIG9mZiBhbmQgYSBmcmVlIFNIUk0gdG90ZSFkAgUPDxYGHwAFCkpPSU4gVE9EQVkeC05hdmlnYXRlVXJsBTlodHRwczovL21lbWJlcnNoaXAuc2hybS5vcmcvP1BST0RVQ1RfRElTQ09VTlRfSUQ9VE9URTIwMTgfAmdkZAIHDw8WBh8ABQlSRU5FVyBOT1cfBgU5aHR0cHM6Ly9tZW1iZXJzaGlwLnNocm0ub3JnLz9QUk9EVUNUX0RJU0NPVU5UX0lEPVRPVEUyMDE4HwJnZGQCCg9kFggCAQ8WAh4LXyFJdGVtQ291bnQCBRYKZg9kFgICAQ8WAh4FY2xhc3MFB2Ryb3BvdXQWBAIBDxYCHwAFtQE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMF9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDBfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+SFIgVG9kYXk8L2E+ZAIDD2QWBgIBDxYCHwAFtQE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMF9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDBfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+SFIgVG9kYXk8L2E+ZAIDDxYCHwcCAxYGZg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABfcBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5OZXdzPC9hPmQCBQ9kFgQCAQ8WAh8ABd0BPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+TmV3czwvYT5kAgMPFgIfBwIFFgpmD2QWAgIBD2QWAmYPDxYEHwYFKS9oci10b2RheS9uZXdzL2hyLW5ld3MvUGFnZXMvZGVmYXVsdC5hc3B4HwAFB0hSIE5ld3NkZAIBD2QWAgIBD2QWAmYPDxYEHwYFMi9oci10b2RheS9uZXdzL2hyLW1hZ2F6aW5lLzA5MTgvUGFnZXMvZGVmYXVsdC5hc3B4HwAFC0hSIE1hZ2F6aW5lZGQCAg9kFgICAQ9kFgJmDw8WBB8GBRVodHRwOi8vYmxvZy5zaHJtLm9yZy8fAAUJU0hSTSBCbG9nZGQCAw9kFgICAQ9kFgJmDw8WBh8GBTJodHRwczovL3d3dy5zaHJtLm9yZy9zaHJtLWluZGlhL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRBTSFJNIEluZGlhIFRvZGF5HwJoZGQCBA9kFgICAQ9kFgJmDw8WBh8GBRtodHRwczovL2Jsb2cuc2hybS5vcmcvc2FzaWEfAAUUU0hSTSBTb3V0aCBBc2lhIEJsb2cfAmhkZAIHDxYCHwAFBjwvZGl2PmQCAQ9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYACPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5QdWJsaWMgUG9saWN5PC9hPmQCBQ9kFgQCAQ8WAh8ABeYBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+UHVibGljIFBvbGljeTwvYT5kAgMPFgIfBwIEFghmD2QWAgIBD2QWAmYPDxYEHwYFIWh0dHA6Ly93d3cuYWR2b2NhY3kuc2hybS5vcmcvaG9tZR8ABQtUYWtlIEFjdGlvbmRkAgEPZBYCAgEPZBYCZg8PFgQfBgVCL2hyLXRvZGF5L3B1YmxpYy1wb2xpY3kvaHItcHVibGljLXBvbGljeS1pc3N1ZXMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFF0hSIFB1YmxpYyBQb2xpY3kgSXNzdWVzZGQCAg9kFgICAQ9kFgJmDw8WBB8GBSJodHRwOi8vd3d3LmFkdm9jYWN5LnNocm0ub3JnL2Fib3V0HwAFF0EtVGVhbSBBZHZvY2FjeSBOZXR3b3JrZGQCAw9kFgICAQ9kFgJmDw8WBB8GBTgvaHItdG9kYXkvcHVibGljLXBvbGljeS9zdGF0ZS1hZmZhaXJzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQ1TdGF0ZSBBZmZhaXJzZGQCBw8WAh8ABQY8L2Rpdj5kAgIPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAWHAjxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDBfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+VHJlbmRzICYgRm9yZWNhc3Rpbmc8L2E+ZAIFD2QWBAIBDxYCHwAF7QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAwX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5UcmVuZHMgJiBGb3JlY2FzdGluZzwvYT5kAgMPFgIfBwIDFgZmD2QWAgIBD2QWAmYPDxYEHwYFSC9oci10b2RheS90cmVuZHMtYW5kLWZvcmVjYXN0aW5nL3Jlc2VhcmNoLWFuZC1zdXJ2ZXlzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRJSZXNlYXJjaCAmIFN1cnZleXNkZAIBD2QWAgIBD2QWAmYPDxYEHwYFUi9oci10b2RheS90cmVuZHMtYW5kLWZvcmVjYXN0aW5nL2xhYm9yLW1hcmtldC1hbmQtZWNvbm9taWMtZGF0YS9QYWdlcy9kZWZhdWx0LmFzcHgfAAUcTGFib3IgTWFya2V0ICYgRWNvbm9taWMgRGF0YWRkAgIPZBYCAgEPZBYCZg8PFgQfBgVUL2hyLXRvZGF5L3RyZW5kcy1hbmQtZm9yZWNhc3Rpbmcvc3BlY2lhbC1yZXBvcnRzLWFuZC1leHBlcnQtdmlld3MvUGFnZXMvZGVmYXVsdC5hc3B4HwAFHlNwZWNpYWwgUmVwb3J0cyAmIEV4cGVydCBWaWV3c2RkAgcPFgIfAAUGPC9kaXY+ZAIFD2QWAmYPDxYCHwJnFgIeF2RhdGEtcGVyc29uYWxpemVkLW9mZmVyBQVGYWxzZRYKAgEPFgIfAGVkAgMPDxYEHwYFRGh0dHBzOi8vd3d3LnNocm0ub3JnL2hyLXRvZGF5L25ld3MvaHItbmV3cy9QYWdlcy9oci1uZXdzbGV0dGVycy5hc3B4HwJnZBYCZg8PFgYeCEltYWdlVXJsBXxodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wLGhfMTE5Mix3XzIxMjEseF8wLHlfNTkvd19hdXRvOjEwMDoyMjgscV9hdXRvLGZfYXV0by92MS9OZXdzL2lTdG9jay02Nzk1MDEyNDJfeWV3bmE3HgdUb29sVGlwBRBTSFJNIE5ld3NsZXR0ZXJzHg1BbHRlcm5hdGVUZXh0BRBTSFJNIE5ld3NsZXR0ZXJzZGQCBQ8PFgIfBgVEaHR0cHM6Ly93d3cuc2hybS5vcmcvaHItdG9kYXkvbmV3cy9oci1uZXdzL1BhZ2VzL2hyLW5ld3NsZXR0ZXJzLmFzcHhkFgJmDxYCHwAFEFNIUk0gTmV3c2xldHRlcnNkAgcPFgIfAAVVU2lnbiB1cCBmb3IgZnJlZSBlbWFpbCBuZXdzbGV0dGVycyBhbmQgZ2V0IG1vcmUgU0hSTSBjb250ZW50IGRlbGl2ZXJlZCB0byB5b3VyIGluYm94LmQCCQ8PFgQfBgVEaHR0cHM6Ly93d3cuc2hybS5vcmcvaHItdG9kYXkvbmV3cy9oci1uZXdzL1BhZ2VzL2hyLW5ld3NsZXR0ZXJzLmFzcHgfAAUUTWFuYWdlIFN1YnNjcmlwdGlvbnNkZAIBD2QWAgIBDxYCHwgFB2Ryb3BvdXQWBAIBDxYCHwAFvgE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMV9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDFfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+UmVzb3VyY2VzICYgVG9vbHM8L2E+ZAIDD2QWBgIBDxYCHwAFvgE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMV9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDFfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+UmVzb3VyY2VzICYgVG9vbHM8L2E+ZAIDDxYCHwcCBBYIZg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABfwBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5IUiBUb3BpY3M8L2E+ZAIFD2QWBAIBDxYCHwAF4gE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5IUiBUb3BpY3M8L2E+ZAIDDxYCHwcCDBYYZg9kFgICAQ9kFgJmDw8WBB8GBUcvUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL2JlaGF2aW9yYWwtY29tcGV0ZW5jaWVzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRdCZWhhdmlvcmFsIENvbXBldGVuY2llc2RkAgEPZBYCAgEPZBYCZg8PFgQfBgU4L1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9iZW5lZml0cy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUIQmVuZWZpdHNkZAICD2QWAgIBD2QWAmYPDxYEHwYFPC9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvUGFnZXMvY2FsaWZvcm5pYS1yZXNvdXJjZXMuYXNweB8ABRRDYWxpZm9ybmlhIFJlc291cmNlc2RkAgMPZBYCAgEPZBYCZg8PFgQfBgU8L1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9jb21wZW5zYXRpb24vUGFnZXMvZGVmYXVsdC5hc3B4HwAFDENvbXBlbnNhdGlvbmRkAgQPZBYCAgEPZBYCZg8PFgQfBgU/L1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9QYWdlcy9kaXZlcnNpdHktYW5kLWluY2x1c2lvbi5hc3B4HwAFFURpdmVyc2l0eSAmIEluY2x1c2lvbmRkAgUPZBYCAgEPZBYCZg8PFgQfBgVCL1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9lbXBsb3llZS1yZWxhdGlvbnMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFEkVtcGxveWVlIFJlbGF0aW9uc2RkAgYPZBYCAgEPZBYCZg8PFgQfBgU5L1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9nbG9iYWwtaHIvUGFnZXMvZGVmYXVsdC5hc3B4HwAFCUdsb2JhbCBIUmRkAgcPZBYCAgEPZBYCZg8PFgQfBgU/L1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9sYWJvci1yZWxhdGlvbnMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFD0xhYm9yIFJlbGF0aW9uc2RkAggPZBYCAgEPZBYCZg8PFgQfBgVXL1Jlc291cmNlc0FuZFRvb2xzL2hyLXRvcGljcy9vcmdhbml6YXRpb25hbC1hbmQtZW1wbG95ZWUtZGV2ZWxvcG1lbnQvUGFnZXMvZGVmYXVsdC5hc3B4HwAFJU9yZ2FuaXphdGlvbmFsICYgRW1wbG95ZWUgRGV2ZWxvcG1lbnRkZAIJD2QWAgIBD2QWAmYPDxYEHwYFPy9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3Mvcmlzay1tYW5hZ2VtZW50L1BhZ2VzL2RlZmF1bHQuYXNweB8ABQ9SaXNrIE1hbmFnZW1lbnRkZAIKD2QWAgIBD2QWAmYPDxYEHwYFQi9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvdGFsZW50LWFjcXVpc2l0aW9uL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRJUYWxlbnQgQWNxdWlzaXRpb25kZAILD2QWAgIBD2QWAmYPDxYEHwYFOi9SZXNvdXJjZXNBbmRUb29scy9oci10b3BpY3MvdGVjaG5vbG9neS9QYWdlcy9kZWZhdWx0LmFzcHgfAAUKVGVjaG5vbG9neWRkAgcPFgIfAAUGPC9kaXY+ZAIBD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFhQI8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkxlZ2FsICYgQ29tcGxpYW5jZTwvYT5kAgUPZBYEAgEPFgIfAAXrATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkxlZ2FsICYgQ29tcGxpYW5jZTwvYT5kAgMPFgIfBwIEFghmD2QWAgIBD2QWAmYPDxYEHwYFSS9SZXNvdXJjZXNBbmRUb29scy9sZWdhbC1hbmQtY29tcGxpYW5jZS9lbXBsb3ltZW50LWxhdy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUORW1wbG95bWVudCBMYXdkZAIBD2QWAgIBD2QWAmYPDxYEHwYFUi9SZXNvdXJjZXNBbmRUb29scy9sZWdhbC1hbmQtY29tcGxpYW5jZS9zdGF0ZS1hbmQtbG9jYWwtdXBkYXRlcy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUVU3RhdGUgJiBMb2NhbCBVcGRhdGVzZGQCAg9kFgICAQ9kFgJmDw8WBh8GBSEvc2hybS1pbmRpYS9QYWdlcy9zaW1wbGlhbmNlLmFzcHgfAAUgSW5kaWEgTGVnYWwgJiBDb21wbGlhbmNlIFVwZGF0ZXMfAmhkZAIDD2QWAgIBD2QWAmYPDxYEHwYFEGh0dHA6Ly9jZmdpLm9yZy8fAAUcSW1taWdyYXRpb24gRW1wbG95ZXIgTmV0d29ya2RkAgcPFgIfAGVkAgIPZBYIAgEPFgIfAGVkAgMPFgIfAAWFAjxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+QnVzaW5lc3MgU29sdXRpb25zPC9hPmQCBQ9kFgQCAQ8WAh8ABesBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+QnVzaW5lc3MgU29sdXRpb25zPC9hPmQCAw8WAh8HAggWEGYPZBYCAgEPZBYCZg8PFgQfBgU9L1Jlc291cmNlc0FuZFRvb2xzL2J1c2luZXNzLXNvbHV0aW9ucy9QYWdlcy9iZW5jaG1hcmtpbmcuYXNweB8ABRRCZW5jaG1hcmtpbmcgU2VydmljZWRkAgEPZBYCAgEPZBYCZg8PFgQfBgUeaHR0cHM6Ly9icm9rZXJmaW5kZXIuc2hybS5vcmcvHwAFGUJlbmVmaXRzIEJyb2tlciBEaXJlY3RvcnlkZAICD2QWAgIBD2QWAmYPDxYEHwYFUy9SZXNvdXJjZXNBbmRUb29scy9idXNpbmVzcy1zb2x1dGlvbnMvUGFnZXMvRW1wbG95ZWUtRW5nYWdlbWVudC1TdXJ2ZXktU2VydmljZS5hc3B4HwAFGkVtcGxveWVlIEVuZ2FnZW1lbnQgU3VydmV5ZGQCAw9kFgICAQ9kFgJmDw8WBB8GBUQvUmVzb3VyY2VzQW5kVG9vbHMvYnVzaW5lc3Mtc29sdXRpb25zL1BhZ2VzL1NhbGFyeS1EYXRhLVNlcnZpY2UuYXNweB8ABRNTYWxhcnkgRGF0YSBTZXJ2aWNlZGQCBA9kFgICAQ9kFgJmDw8WBB8GBRVodHRwczovL3RhYy5zaHJtLm9yZy8fAAUYVGFsZW50IEFzc2Vzc21lbnQgQ2VudGVyZGQCBQ9kFgICAQ9kFgJmDw8WBB8GBSFodHRwczovL3ZlbmRvcmRpcmVjdG9yeS5zaHJtLm9yZy8fAAUQVmVuZG9yIERpcmVjdG9yeWRkAgYPZBYCAgEPZBYCZg8PFgYfBgUwL3Nocm0taW5kaWEvYWR2aXNvcnktc2VydmljZXMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFHFNIUk0gSW5kaWEgQWR2aXNvcnkgU2VydmljZXMfAmhkZAIHD2QWAgIBD2QWAmYPDxYGHwYFOi9zaHJtLWluZGlhL2Fkdmlzb3J5LXNlcnZpY2VzL1BhZ2VzL1Jlc2VhcmNoX1NlcnZpY2VzLmFzcHgfAAUcU0hSTSBJbmRpYSBSZXNlYXJjaCBTZXJ2aWNlcx8CaGRkAgcPFgIfAAUGPC9kaXY+ZAIDD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFggI8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMV9yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPlRvb2xzICYgU2FtcGxlczwvYT5kAgUPZBYEAgEPFgIfAAXoATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAxX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDFfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlRvb2xzICYgU2FtcGxlczwvYT5kAgMPFgIfBwIMFhhmD2QWAgIBD2QWAmYPDxYEHwYFQi9SZXNvdXJjZXNBbmRUb29scy90b29scy1hbmQtc2FtcGxlcy9QYWdlcy9lbXBsb3llZS1oYW5kYm9va3MuYXNweB8ABRJFbXBsb3llZSBIYW5kYm9va3NkZAIBD2QWAgIBD2QWAmYPDxYEHwYFRS9SZXNvdXJjZXNBbmRUb29scy90b29scy1hbmQtc2FtcGxlcy9leHJlcS9QYWdlcy9UcmVuZGluZy1Ub3BpY3MuYXNweB8ABRBFeHByZXNzIFJlcXVlc3RzZGQCAg9kFgICAQ9kFgJmDw8WBB8GBUUvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvaG93LXRvLWd1aWRlcy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUNSG93LVRvIEd1aWRlc2RkAgMPZBYCAgEPZBYCZg8PFgQfBgVAL1Jlc291cmNlc0FuZFRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL2hyLWZvcm1zL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQhIUiBGb3Jtc2RkAgQPZBYCAgEPZBYCZg8PFgQfBgU9L1Jlc291cmNlc0FuZFRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL2hyLXFhL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQdIUiBRJkFzZGQCBQ9kFgICAQ9kFgJmDw8WBB8GBUsvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvaW50ZXJ2aWV3LXF1ZXN0aW9ucy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUTSW50ZXJ2aWV3IFF1ZXN0aW9uc2RkAgYPZBYCAgEPZBYCZg8PFgQfBgVIL1Jlc291cmNlc0FuZFRvb2xzL3Rvb2xzLWFuZC1zYW1wbGVzL2pvYi1kZXNjcmlwdGlvbnMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFEEpvYiBEZXNjcmlwdGlvbnNkZAIHD2QWAgIBD2QWAmYPDxYEHwYFQC9SZXNvdXJjZXNBbmRUb29scy90b29scy1hbmQtc2FtcGxlcy9wb2xpY2llcy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUIUG9saWNpZXNkZAIID2QWAgIBD2QWAmYPDxYEHwYFRS9SZXNvdXJjZXNBbmRUb29scy90b29scy1hbmQtc2FtcGxlcy9wcmVzZW50YXRpb25zL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQ1QcmVzZW50YXRpb25zZGQCCQ9kFgICAQ9kFgJmDw8WBB8GBTwvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvUGFnZXMvc3ByZWFkc2hlZXRzLmFzcHgfAAUMU3ByZWFkc2hlZXRzZGQCCg9kFgICAQ9kFgJmDw8WBB8GBUAvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvdG9vbGtpdHMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFCFRvb2xraXRzZGQCCw9kFgICAQ9kFgJmDw8WBB8GBUUvUmVzb3VyY2VzQW5kVG9vbHMvdG9vbHMtYW5kLXNhbXBsZXMvbWVtYmVyMm1lbWJlci9QYWdlcy9kZWZhdWx0LmFzcHgfAAUXTWVtYmVyMk1lbWJlciBTb2x1dGlvbnNkZAIHDxYCHwAFBjwvZGl2PmQCBQ9kFgJmDw8WAh8CZxYCHwkFBUZhbHNlFgoCAQ8WAh8ABRdKT0IgREVTQ1JJUFRJT04gTUFOQUdFUmQCAw8PFgQfBgUzaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9qb2ItZGVzY3JpcHRpb24tbWFuYWdlci5odG1sHwJnZBYCZg8PFgYfCgVyaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCxoXzg5OSx3XzE2MDAseF8wLHlfMC93X2F1dG86MTAwOjIyOCxxX2F1dG8sZl9hdXRvL3YxL01hcmtldGluZy9KRE1fZGNpcGlyHwsFF0pvYiBEZXNjcmlwdGlvbiBNYW5hZ2VyHwwFF0pvYiBEZXNjcmlwdGlvbiBNYW5hZ2VyZGQCBQ8PFgIfBgUzaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy9qb2ItZGVzY3JpcHRpb24tbWFuYWdlci5odG1sZBYCZg8WAh8ABRdKb2IgRGVzY3JpcHRpb24gTWFuYWdlcmQCBw8WAh8ABUxDcmVhdGUsIE1haW50YWluICYgT3JnYW5pemUgWW91ciBKb2IgRGVzY3JpcHRpb25zLiBJdOKAmXMgZmFzdC4gSXTigJlzIGVhc3kuZAIJDw8WBB8GBTNodHRwczovL3N0b3JlLnNocm0ub3JnL2pvYi1kZXNjcmlwdGlvbi1tYW5hZ2VyLmh0bWwfAAUKTEVBUk4gTU9SRWRkAgIPZBYCAgEPFgIfCAUHZHJvcG91dBYEAgEPFgIfAAW+ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5MZWFybmluZyAmIENhcmVlcjwvYT5kAgMPZBYGAgEPFgIfAAW+ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5MZWFybmluZyAmIENhcmVlcjwvYT5kAgMPFgIfBwIFFgpmD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAF+QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkNhcmVlcjwvYT5kAgUPZBYEAgEPFgIfAAXfATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkNhcmVlcjwvYT5kAgMPFgIfBwIFFgpmD2QWAgIBD2QWAmYPDxYEHwYFOy9MZWFybmluZ0FuZENhcmVlci9DYXJlZXIvUGFnZXMvQWNjZWxlcmF0ZS1Zb3VyLUNhcmVlci5hc3B4HwAFFkFjY2VsZXJhdGUgWW91ciBDYXJlZXJkZAIBD2QWAgIBD2QWAmYPDxYEHwYFRC9MZWFybmluZ0FuZENhcmVlci9DYXJlZXIvUGFnZXMvQ2FyZWVyLVByZXBhcmF0aW9uLWFuZC1QbGFubmluZy5hc3B4HwAFHUNhcmVlciBQcmVwYXJhdGlvbiAmIFBsYW5uaW5nZGQCAg9kFgICAQ9kFgJmDw8WBB8GBTovTGVhcm5pbmdBbmRDYXJlZXIvQ2FyZWVyL1BhZ2VzL3Nocm0tY29tcGV0ZW5jeS1tb2RlbC5hc3B4HwAFFVNIUk0gQ29tcGV0ZW5jeSBNb2RlbGRkAgMPZBYCAgEPZBYCZg8PFgQfBgVCL0xlYXJuaW5nQW5kQ2FyZWVyL0NhcmVlci9QYWdlcy9Zb3VyLVByb2Zlc3Npb25hbC1EZXZlbG9wbWVudC5hc3B4HwAFHVlvdXIgUHJvZmVzc2lvbmFsIERldmVsb3BtZW50ZGQCBA9kFgICAQ9kFgJmDw8WBB8GBTsvTGVhcm5pbmdBbmRDYXJlZXIvQ2FyZWVyL1BhZ2VzL0NhcmVlci1FeHBlcnQtSW5zaWdodHMuYXNweB8ABRZDYXJlZXIgRXhwZXJ0IEluc2lnaHRzZGQCBw8WAh8AZWQCAQ9kFggCAQ8WAh8AZWQCAw8WAh8ABfoBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5IUiBKb2JzPC9hPmQCBQ9kFgQCAQ8WAh8ABeABPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+SFIgSm9iczwvYT5kAgMPFgIfBwIGFgxmD2QWAgIBD2QWAmYPDxYEHwYFE2h0dHA6Ly93d3cuc2hybS5vcmcfAAUJdWNjSHJKb2JzZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBRVodHRwOi8vam9icy5zaHJtLm9yZy8fAAUSQnJvd3NlIEFsbCBKb2JzLi4uZGQCAg9kFgICAQ8WAh8CaBYCZg8PFgQfBgUTaHR0cDovL3d3dy5zaHJtLm9yZx8ABQEkZGQCAw9kFgICAQ8WAh8CaBYCZg8PFgQfBgUTaHR0cDovL3d3dy5zaHJtLm9yZx8ABQEkZGQCBA9kFgICAQ8WAh8CaBYCZg8PFgQfBgUTaHR0cDovL3d3dy5zaHJtLm9yZx8ABQEkZGQCBQ9kFgICAQ8WAh8CaBYCZg8PFgQfBgUTaHR0cDovL3d3dy5zaHJtLm9yZx8ABQEkZGQCBw8WAh8ABQY8L2Rpdj5kAgIPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAX7ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+TGVhcm5pbmc8L2E+ZAIFD2QWBAIBDxYCHwAF4QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAyX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5MZWFybmluZzwvYT5kAgMPFgIfBwIMFhhmD2QWAgIBD2QWAmYPDxYEHwYFLy9MZWFybmluZ0FuZENhcmVlci9sZWFybmluZy9QYWdlcy9TZW1pbmFycy5hc3B4HwAFCFNlbWluYXJzZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBT4vTGVhcm5pbmdBbmRDYXJlZXIvbGVhcm5pbmcvb25zaXRlLXRyYWluaW5nL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQ9PbnNpdGUgVHJhaW5pbmdkZAICD2QWAgIBD2QWAmYPDxYEHwYFNS9MZWFybmluZ0FuZENhcmVlci9sZWFybmluZy9QYWdlcy9TSFJNLWVMZWFybmluZy5hc3B4HwAFCWVMZWFybmluZ2RkAgMPZBYCAgEPZBYCZg8PFgQfBgVJL0xlYXJuaW5nQW5kQ2FyZWVyL2xlYXJuaW5nL1BhZ2VzL1NIUk0tRXNzZW50aWFscy1vZi1IdW1hbi1SZXNvdXJjZXMuYXNweB8ABSJTSFJNIEVzc2VudGlhbHMgb2YgSHVtYW4gUmVzb3VyY2VzZGQCBA9kFgICAQ9kFgJmDw8WBB8GBUYvTGVhcm5pbmdBbmRDYXJlZXIvbGVhcm5pbmcvUGFnZXMvU0hSTS1TZW5pb3ItTGVhZGVyc2hpcC1Qcm9ncmFtcy5hc3B4HwAFGlNlbmlvciBMZWFkZXJzaGlwIFByb2dyYW1zZGQCBQ9kFgICAQ9kFgJmDw8WBB8GBT1odHRwczovL3N0b3JlLnNocm0ub3JnL2V2ZW50cy9zaHJtLWV2ZW50cy92aXJ0dWFsLWV2ZW50cy5odG1sHwAFDlZpcnR1YWwgRXZlbnRzZGQCBg9kFgICAQ9kFgJmDw8WBB8GBTcvTGVhcm5pbmdBbmRDYXJlZXIvbGVhcm5pbmcvd2ViY2FzdHMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFCFdlYmNhc3RzZGQCBw9kFgICAQ9kFgJmDw8WBB8GBTwvTGVhcm5pbmdBbmRDYXJlZXIvbGVhcm5pbmcvUGFnZXMvU3BlY2lhbHR5LUNyZWRlbnRpYWxzLmFzcHgfAAUVU3BlY2lhbHR5IENyZWRlbnRpYWxzZGQCCA9kFgICAQ9kFgJmDw8WBh8GBSgvc2hybS1pbmRpYS9QYWdlcy90cmFpbmluZy1wcm9ncmFtcy5hc3B4HwAFGVNIUk0gSW5kaWEgVHJhaW5pbmcgKFBEUCkfAmhkZAIJD2QWAgIBDxYCHwJoFgJmDw8WBB8GBRNodHRwOi8vd3d3LnNocm0ub3JnHwAFASRkZAIKD2QWAgIBDxYCHwJoFgJmDw8WBB8GBRNodHRwOi8vd3d3LnNocm0ub3JnHwAFASRkZAILD2QWAgIBDxYCHwJoFgJmDw8WBB8GBRNodHRwOi8vd3d3LnNocm0ub3JnHwAFASRkZAIHDxYCHwAFBjwvZGl2PmQCAw9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYACPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5DZXJ0aWZpY2F0aW9uPC9hPmQCBQ9kFgQCAQ8WAh8ABeYBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDNfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+Q2VydGlmaWNhdGlvbjwvYT5kAgMPFgIfBwIFFgpmD2QWAgIBD2QWAmYPDxYEHwYFO2h0dHBzOi8vd3d3LnNocm0ub3JnL2NlcnRpZmljYXRpb24vYXBwbHkvcGFnZXMvZGVmYXVsdC5hc3B4HwAFDkFwcGx5IGZvciBFeGFtZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBT5odHRwczovL3d3dy5zaHJtLm9yZy9jZXJ0aWZpY2F0aW9uL2xlYXJuaW5nL3BhZ2VzL2RlZmF1bHQuYXNweB8ABRlDZXJ0aWZpY2F0aW9uIFByZXBhcmF0aW9uZGQCAg9kFgICAQ9kFgJmDw8WBB8GBTpodHRwczovL3d3dy5zaHJtLm9yZy9jZXJ0aWZpY2F0aW9uL2ZhcXMvcGFnZXMvZGVmYXVsdC5hc3B4HwAFF1NIUk0gQ2VydGlmaWNhdGlvbiBGQVFzZGQCAw9kFgICAQ9kFgJmDw8WBB8GBUVodHRwczovL3d3dy5zaHJtLm9yZy9jZXJ0aWZpY2F0aW9uL3JlY2VydGlmaWNhdGlvbi9wYWdlcy9kZWZhdWx0LmFzcHgfAAUPUmVjZXJ0aWZpY2F0aW9uZGQCBA9kFgICAQ9kFgJmDw8WBh8GBYIBaHR0cDovL3hscmktaHIudGFsZW50ZWRnZS5pbi8/dXRtX3NvdXJjZT1zaHJtLWIyYyZ1dG1fbWVkaXVtPXNocm0tYjJjJnV0bV90ZXJtPXNocm0tYjJjJnV0bV9jb250ZW50PXNocm0tYjJjJnV0bV9jYW1wYWlnbj1zaHJtLWIyYx8ABRxTSFJNLVhMUkkgQ2VydGlmaWNhdGUgaW4gSFJNHwJoZGQCBw8WAh8AZWQCBA9kFggCAQ8WAh8AZWQCAw8WAh8ABYACPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDA0X3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDRfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5Gb3IgRWR1Y2F0b3JzPC9hPmQCBQ9kFgQCAQ8WAh8ABeYBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDJfcmVwX05hdmlnYXRpb24yX2N0bDA0X3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwMl9yZXBfTmF2aWdhdGlvbjJfY3RsMDRfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbic+Rm9yIEVkdWNhdG9yczwvYT5kAgMPFgIfBwIEFghmD2QWAgIBD2QWAmYPDxYEHwYFNi9hY2FkZW1pY2luaXRpYXRpdmVzL3VuaXZlcnNpdGllcy9wYWdlcy9ndWlkZWJvb2suYXNweB8ABSJIUiBDdXJyaWN1bHVtIEd1aWRlYm9vayAmIFRlbXBsYXRlZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBTsvYWNhZGVtaWNpbml0aWF0aXZlcy9zdHVkZW50cy9wYWdlcy9ocnByb2dyYW1kaXJlY3RvcnkuYXNweB8ABRRIUiBQcm9ncmFtIERpcmVjdG9yeWRkAgIPZBYCAgEPZBYCZg8PFgQfBgVRL2FjYWRlbWljaW5pdGlhdGl2ZXMvdW5pdmVyc2l0aWVzL3RlYWNoaW5ncmVzb3VyY2VzL3BhZ2VzL3Rlcm1zb2Z1c2VfZmFjdWx0eS5hc3B4HwAFElRlYWNoaW5nIFJlc291cmNlc2RkAgMPZBYCAgEPZBYCZg8PFgYfBgU+L3Nocm0taW5kaWEvYWR2aXNvcnktc2VydmljZXMvUGFnZXMvQWNhZGVtaWNfUGFydG5lcnNoaXBzLmFzcHgfAAUgU0hSTSBJbmRpYSBBY2FkZW1pYyBQYXJ0bmVyc2hpcHMfAmhkZAIHDxYCHwAFBjwvZGl2PmQCBQ9kFgJmDw8WAh8CZxYCHwkFBUZhbHNlFgoCAQ8WAh8ABRdJTi1QRVJTT04gU0hSTSBTRU1JTkFSU2QCAw8PFgQfBgVMaHR0cHM6Ly9zdG9yZS5zaHJtLm9yZy90cmFpbmluZy9sZWFybmluZy1jZW50ZXIvc2VtaW5hcnMvZmluZC1hLXNlbWluYXIuaHRtbB8CZ2QWAmYPDxYGHwoFjwFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wLGhfMTcyMyx3XzMwNjUseF8wLHlfNTQvd19hdXRvOjEwMDoyMjgscV9hdXRvLGZfYXV0by92MS9Ib21lJTIwUGFnZSUyMENhcm91c2VsLzE4LTA4OTVfTWFwX2ltYWdlX295YWtkZB8LBR9Mb2NhbCBEZXZlbG9wbWVudCBPcHBvcnR1bml0aWVzHwwFH0xvY2FsIERldmVsb3BtZW50IE9wcG9ydHVuaXRpZXNkZAIFDw8WAh8GBUxodHRwczovL3N0b3JlLnNocm0ub3JnL3RyYWluaW5nL2xlYXJuaW5nLWNlbnRlci9zZW1pbmFycy9maW5kLWEtc2VtaW5hci5odG1sZBYCZg8WAh8ABR9Mb2NhbCBEZXZlbG9wbWVudCBPcHBvcnR1bml0aWVzZAIHDxYCHwAFkQFCdWlsZCBjb21wZXRlbmNpZXMsIGVzdGFibGlzaCBjcmVkaWJpbGl0eSBhbmQgYWR2YW5jZSB5b3VyIGNhcmVlcuKAlHdoaWxlIGVhcm5pbmcgUERDc+KAlGF0IFNIUk0gU2VtaW5hcnMgaW4gMTQgY2l0aWVzIGFjcm9zcyB0aGUgVS5TLiB0aGlzIGZhbGwuZAIJDw8WBB8GBUxodHRwczovL3N0b3JlLnNocm0ub3JnL3RyYWluaW5nL2xlYXJuaW5nLWNlbnRlci9zZW1pbmFycy9maW5kLWEtc2VtaW5hci5odG1sHwAFGlNFRSAyMDE4IFNFTUlOQVIgTE9DQVRJT05TZGQCAw9kFgICAQ8WAh8IBQdkcm9wb3V0FgQCAQ8WAh8ABbMBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDNfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkV2ZW50czwvYT5kAgMPZBYGAgEPFgIfAAWzATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwM19wYW5fRHJvcG91dE1lbnUnIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5FdmVudHM8L2E+ZAIDDxYCHwcCBBYIZg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABf4BPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5TSFJNIEV2ZW50czwvYT5kAgUPZBYEAgEPFgIfAAXkATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlNIUk0gRXZlbnRzPC9hPmQCAw8WAh8HAgkWEmYPZBYCAgEPZBYCZg8PFgQfBgUXaHR0cDovL2FubnVhbC5zaHJtLm9yZy8fAAUjU0hSTSBBbm51YWwgQ29uZmVyZW5jZSAmIEV4cG9zaXRpb25kZAIBD2QWAgIBD2QWAmYPDxYEHwYFHS9tbHAvUGFnZXMvRGl2ZXJzaXR5MjAxOC5hc3B4HwAFLURpdmVyc2l0eSAmIEluY2x1c2lvbiBDb25mZXJlbmNlICYgRXhwb3NpdGlvbmRkAgIPZBYCAgEPZBYCZg8PFgQfBgUzaHR0cHM6Ly9jb25mZXJlbmNlcy5zaHJtLm9yZy9sZWdpc2xhdGl2ZS1jb25mZXJlbmNlHwAFJ0VtcGxveW1lbnQgTGF3ICYgTGVnaXNsYXRpdmUgQ29uZmVyZW5jZWRkAgMPZBYCAgEPZBYCZg8PFgQfBgU5aHR0cHM6Ly9jb25mZXJlbmNlcy5zaHJtLm9yZy9sZWFkZXJzaGlwLWRldmVsb3BtZW50LWZvcnVtHwAFHExlYWRlcnNoaXAgRGV2ZWxvcG1lbnQgRm9ydW1kZAIED2QWAgIBD2QWAmYPDxYEHwYFLWh0dHA6Ly9jb25mZXJlbmNlcy5zaHJtLm9yZy90YWxlbnQtY29uZmVyZW5jZR8ABR5UYWxlbnQgQ29uZmVyZW5jZSAmIEV4cG9zaXRpb25kZAIFD2QWAgIBD2QWAmYPDxYEHwYFLy9FdmVudHMvUGFnZXMvU3RhdGUtLUFmZmlsaWF0ZS1Db25mZXJlbmNlcy5hc3B4HwAFHVN0YXRlICYgQWZmaWxpYXRlIENvbmZlcmVuY2VzZGQCBg9kFgICAQ9kFgJmDw8WBB8GBT1odHRwczovL3N0b3JlLnNocm0ub3JnL2V2ZW50cy9zaHJtLWV2ZW50cy92aXJ0dWFsLWV2ZW50cy5odG1sHwAFDlZpcnR1YWwgRXZlbnRzZGQCBw9kFgICAQ9kFgJmDw8WBB8GBTcvTGVhcm5pbmdBbmRDYXJlZXIvbGVhcm5pbmcvd2ViY2FzdHMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFCFdlYmNhc3RzZGQCCA9kFgICAQ9kFgJmDw8WBB8GBRovRXZlbnRzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQ5TRUUgQUxMIEVWRU5UU2RkAgcPFgIfAAUGPC9kaXY+ZAIBD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAFggI8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwM19yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPkV2ZW50IFJlc291cmNlczwvYT5kAgUPZBYEAgEPFgIfAAXoATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkV2ZW50IFJlc291cmNlczwvYT5kAgMPFgIfBwIHFg5mD2QWAgIBD2QWAmYPDxYEHwYFNGh0dHBzOi8vc2hybS5vcmcvbWxwL1BhZ2VzL3NwZWFrZXJzLWJ1cmVhdS9Ib21lLmFzcHgfAAUPU3BlYWtlcnMgQnVyZWF1ZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBSYvRXZlbnRzL1BhZ2VzL1NwZWFrZXItSW5mb3JtYXRpb24uYXNweB8ABR5Db25mZXJlbmNlIFNwZWFrZXIgSW5mb3JtYXRpb25kZAICD2QWAgIBD2QWAmYPDxYEHwYFLmh0dHA6Ly9jb25mZXJlbmNlcy5zaHJtLm9yZy9leGhpYml0LW9yLXNwb25zb3IfAAUjU3BvbnNvcnNoaXAgJiBFeGhpYml0b3IgSW5mb3JtYXRpb25kZAIDD2QWAgIBD2QWAmYPDxYEHwYFKGh0dHA6Ly9sLnNocm0ub3JnL3JlcXVlc3Rtb3JlaW5mb3JtYXRpb24fAAUSUmVxdWVzdCBhIEJyb2NodXJlZGQCBA9kFgICAQ8WAh8CaBYCZg8PFgQfBgUVaHR0cDovL3d3dy5nb29nbGUuY29tHwAFASRkZAIFD2QWAgIBDxYCHwJoFgJmDw8WBB8GBRVodHRwOi8vd3d3Lmdvb2dsZS5jb20fAAUBJGRkAgYPZBYCAgEPFgIfAmgWAmYPDxYEHwYFFWh0dHA6Ly93d3cuZ29vZ2xlLmNvbR8ABQEkZGQCBw8WAh8ABQY8L2Rpdj5kAgIPZBYIAgEPFgIfAAUmPGRpdiBjbGFzcz0iY29sdW1uIGNvbC1tZC0zIGNvbC1zbS00Ij5kAgMPFgIfAAWAAjxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+R2xvYmFsIEV2ZW50czwvYT5kAgUPZBYEAgEPFgIfAAXmATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAyX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkdsb2JhbCBFdmVudHM8L2E+ZAIDDxYCHwcCAhYEZg9kFgICAQ9kFgJmDw8WBB8GBSwvRXZlbnRzL3Nocm0taW5kaWEtZXZlbnRzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRFTSFJNIEluZGlhIEV2ZW50c2RkAgEPZBYCAgEPZBYCZg8PFgQfBgUjL0V2ZW50cy9QYWdlcy9zaHJtLWFwYWMtZXZlbnRzLmFzcHgfAAUQU0hSTSBBUEFDIEV2ZW50c2RkAgcPFgIfAGVkAgMPZBYIAgEPFgIfAGVkAgMPFgIfAAWDAjxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nIGNsYXNzPSdzaXRlTWVudUNvbEhlYWRlcic+QWZmaWxpYXRlIEV2ZW50czwvYT5kAgUPZBYEAgEPFgIfAAXpATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDAzX3JlcF9OYXZpZ2F0aW9uMl9jdGwwM191bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDNfcmVwX05hdmlnYXRpb24yX2N0bDAzX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkFmZmlsaWF0ZSBFdmVudHM8L2E+ZAIDDxYCHwcCAhYEZg9kFgICAQ9kFgJmDw8WBB8GBR5odHRwczovL3d3dy5jZmdpLm9yZy9zeW1wb3NpdW0fAAUOQ0ZHSSBTeW1wb3NpdW1kZAIBD2QWAgIBD2QWAmYPDxYEHwYFR2h0dHBzOi8vaHJwcy5vcmcvZXhlY3V0aXZlLWV2ZW50cy9zdHJhdGVnaWMtaHItZm9ydW0vcGFnZXMvZGVmYXVsdC5hc3B4HwAFJ0hSIFBlb3BsZSArIFN0cmF0ZWd5IFN0cmF0ZWdpYyBIUiBGb3J1bWRkAgcPFgIfAAUGPC9kaXY+ZAIFD2QWAmYPDxYCHwJnFgIfCQUFRmFsc2UWCgIBDxYCHwAFEFNIUk0gQ09ORkVSRU5DRVNkAgMPDxYEHwYFMGh0dHBzOi8vc2hybS5vcmcvbWxwL1BhZ2VzL1N5bXBvc2l1bURlcHRPbmUuYXNweB8CZ2QWAmYPDxYGHwoFhQFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wLGhfMzExLHdfNTU0LHhfMCx5XzI2L3dfYXV0bzoxMDA6MjI4LHFfYXV0byxmX2F1dG8vdjEvTWFya2V0aW5nL0hSX0RPT19NZWdhTmF2X0dyYXBoaWNfZWt3Y254HwsFJFNIUk0gU3ltcG9zaXVtOiBIUiBEZXBhcnRtZW50IG9mIE9uZR8MBSRTSFJNIFN5bXBvc2l1bTogSFIgRGVwYXJ0bWVudCBvZiBPbmVkZAIFDw8WAh8GBTBodHRwczovL3Nocm0ub3JnL21scC9QYWdlcy9TeW1wb3NpdW1EZXB0T25lLmFzcHhkFgJmDxYCHwAFJFNIUk0gU3ltcG9zaXVtOiBIUiBEZXBhcnRtZW50IG9mIE9uZWQCBw8WAh8ABYQBSm9pbiB1cyBpbiBTYW4gRnJhbmNpc2NvLCBDQSwgRGVjZW1iZXIgMyB0byBsZWFybiBob3cgdG8gbWFzdGVyIHRoZSBiYWxhbmNpbmcgYWN0IGFzIHRoZSBzb2xlIEhSIHByYWN0aXRpb25lciBpbiB5b3VyIG9yZ2FuaXphdGlvbi4gZAIJDw8WBB8GBTBodHRwczovL3Nocm0ub3JnL21scC9QYWdlcy9TeW1wb3NpdW1EZXB0T25lLmFzcHgfAAUKTGVhcm4gTW9yZWRkAgQPZBYCAgEPFgIfCAUHZHJvcG91dBYEAgEPFgIfAAW3ATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwNF9wYW5fRHJvcG91dE1lbnUnIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5NZW1iZXJzaGlwPC9hPmQCAw9kFgQCAQ8WAh8ABbcBPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDRfcGFuX0Ryb3BvdXRNZW51JyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3Bhbl9Ecm9wb3V0TWVudScgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPk1lbWJlcnNoaXA8L2E+ZAIDDxYCHwcCAxYGZg9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABf4BPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDBfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5Db21tdW5pdGllczwvYT5kAgUPZBYEAgEPFgIfAAXkATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMF91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAwX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPkNvbW11bml0aWVzPC9hPmQCAw8WAh8HAgUWCmYPZBYCAgEPZBYCZg8PFgQfBgUaaHR0cDovL2NvbW11bml0eS5zaHJtLm9yZy8fAAUMU0hSTSBDb25uZWN0ZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBTMvTWVtYmVyc2hpcC9jb21tdW5pdGllcy9jaGFwdGVycy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUIQ2hhcHRlcnNkZAICD2QWAgIBD2QWAmYPDxYEHwYFFGh0dHA6Ly93d3cuaHJwcy5vcmcvHwAFEUV4ZWN1dGl2ZSBOZXR3b3JrZGQCAw9kFgICAQ9kFgJmDw8WBB8GBUEvTWVtYmVyc2hpcC9jb21tdW5pdGllcy9oci15b3VuZy1wcm9mZXNzaW9uYWxzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRZIUiBZb3VuZyBQcm9mZXNzaW9uYWxzZGQCBA9kFgICAQ9kFgJmDw8WBB8GBSJodHRwOi8vd3d3LmFkdm9jYWN5LnNocm0ub3JnL2Fib3V0HwAFIkxlZ2lzbGF0aXZlIEFkdm9jYWN5IFRlYW0gKEEtVGVhbSlkZAIHDxYCHwAFBjwvZGl2PmQCAQ9kFggCAQ8WAh8ABSY8ZGl2IGNsYXNzPSJjb2x1bW4gY29sLW1kLTMgY29sLXNtLTQiPmQCAw8WAh8ABYQCPGEgYXJpYS1jb250cm9scz0nY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10YXJnZXQ9JyNjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDFfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRvZ2dsZT0nY29sbGFwc2UnIHJvbGU9J2J1dHRvbicgY2xhc3M9J3NpdGVNZW51Q29sSGVhZGVyJz5TdHVkZW50IFJlc291cmNlczwvYT5kAgUPZBYEAgEPFgIfAAXqATxhIGFyaWEtY29udHJvbHM9J2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMV91bF9Ecm9wb3V0TWVudTInIGRhdGEtdGFyZ2V0PScjY3RsMDBfY3RsNzVfcmVwX05hdmlnYXRpb25fY3RsMDRfcmVwX05hdmlnYXRpb24yX2N0bDAxX3VsX0Ryb3BvdXRNZW51MicgZGF0YS10b2dnbGU9J2NvbGxhcHNlJyByb2xlPSdidXR0b24nPlN0dWRlbnQgUmVzb3VyY2VzPC9hPmQCAw8WAh8HAgEWAmYPZBYCAgEPZBYCZg8PFgQfBgUwL01lbWJlcnNoaXAvc3R1ZGVudC1yZXNvdXJjZXMvUGFnZXMvZGVmYXVsdC5hc3B4HwAFFVN0dWRlbnQgTWVtYmVyIENlbnRlcmRkAgcPFgIfAAUGPC9kaXY+ZAICD2QWCAIBDxYCHwAFJjxkaXYgY2xhc3M9ImNvbHVtbiBjb2wtbWQtMyBjb2wtc20tNCI+ZAIDDxYCHwAF/QE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJyBjbGFzcz0nc2l0ZU1lbnVDb2xIZWFkZXInPlZvbHVudGVlcnM8L2E+ZAIFD2QWBAIBDxYCHwAF4wE8YSBhcmlhLWNvbnRyb2xzPSdjdGwwMF9jdGw3NV9yZXBfTmF2aWdhdGlvbl9jdGwwNF9yZXBfTmF2aWdhdGlvbjJfY3RsMDJfdWxfRHJvcG91dE1lbnUyJyBkYXRhLXRhcmdldD0nI2N0bDAwX2N0bDc1X3JlcF9OYXZpZ2F0aW9uX2N0bDA0X3JlcF9OYXZpZ2F0aW9uMl9jdGwwMl91bF9Ecm9wb3V0TWVudTInIGRhdGEtdG9nZ2xlPSdjb2xsYXBzZScgcm9sZT0nYnV0dG9uJz5Wb2x1bnRlZXJzPC9hPmQCAw8WAh8HAgQWCGYPZBYCAgEPZBYCZg8PFgQfBgU9L01lbWJlcnNoaXAvdm9sdW50ZWVycy9tZW1iZXJzaGlwLWNvdW5jaWxzL1BhZ2VzL2RlZmF1bHQuYXNweB8ABRNNZW1iZXJzaGlwIENvdW5jaWxzZGQCAQ9kFgICAQ9kFgJmDw8WBB8GBUIvTWVtYmVyc2hpcC92b2x1bnRlZXJzL3NwZWNpYWwtZXhwZXJ0aXNlLXBhbmVscy9QYWdlcy9kZWZhdWx0LmFzcHgfAAUYU3BlY2lhbCBFeHBlcnRpc2UgUGFuZWxzZGQCAg9kFgICAQ9kFgJmDw8WBB8GBR9odHRwOi8vY29tbXVuaXR5LnNocm0ub3JnL3ZscmMvHwAFIFZvbHVudGVlciBMZWFkZXIgUmVzb3VyY2UgQ2VudGVyZGQCAw9kFgICAQ9kFgJmDw8WBB8GBTkvTWVtYmVyc2hpcC92b2x1bnRlZXJzL1BhZ2VzL1ZvbHVudGVlci1PcHBvcnR1bml0aWVzLmFzcHgfAAUXVm9sdW50ZWVyIE9wcG9ydHVuaXRpZXNkZAIHDxYCHwAFBjwvZGl2PmQCAw9kFghmDw8WAh8CaGRkAgEPDxYCHwJoZGQCAw8PFgQfBgUhL2Fib3V0LXNocm0vcGFnZXMvbWVtYmVyc2hpcC5hc3B4HwAFCk1lbWJlcnNoaXBkZAIEDw8WBB8GBSEvY2VydGlmaWNhdGlvbi9wYWdlcy9kZWZhdWx0LmFzcHgfAAUNR0VUIENFUlRJRklFRGRkAgUPZBYCZg9kFgQCAQ8WAh4Hb25jbGljawVYcmV0dXJuIE9wZW5Jbk5ld1dpbmRvdygnY3RsMDBfY3RsNzVfSHJKb2JzX3NlYXJjaEpvYnMnLCdjdGwwMF9jdGw3NV9IckpvYnNfc2VhcmNoVGVybScpO2QCAg8PFgYfAAUKUG9zdCBhIEpvYh8GBSRodHRwOi8vaHJqb2JzLnNocm0ub3JnL2pvYnMvcHJvZHVjdHMfAmdkZAIHD2QWAgIBDw9kFgIfDQVxd2luZG93LmxvY2F0aW9uPScvc2VhcmNoL3BhZ2VzL0xvY2FsQ2hhcHRlci5hc3B4P2xvY2F0aW9uPScgKyAkKHRoaXMpLmNsb3Nlc3QoJy5maW5kZXItZm9ybScpLmZpbmQoJ2lucHV0JykudmFsKClkAgwPZBYmAgEPZBYCAgIPFgYfCAUYc2hybS1oYXMtdXJsIHNocm0tS2lja2VyHw0FIHdpbmRvdy5sb2NhdGlvbiA9ICcvYWJvdXQtc2hybSc7HwJnFgJmDxYCHwAFCkFib3V0IFNIUk1kAgMPFgIfAQsrBAFkAgUPFgIfAQsrBAFkAgcPZBYGAgEPFgIfAQsrBAFkAgMPDxYCHwJoZBYGAgEPFgIfCAUPbm8tYXV0aG9yLWltYWdlZAIDDw8WAh8CaGRkAgUPDxYCHwBlZGQCBQ8PFgIfAmhkZAIND2QWAmYPFgIfAmgWBAIBDxYCHwELKwQBZAIDDxYCHwJoFgICAQ8WAh8BCysEAWQCDw9kFgICAQ9kFgJmDw8WAh8CaGQWAgIBDxYCHwdmZAIRDxYCHwELKwQBZAIVD2QWAgIDD2QWAgIMDw8WBB8EBRNzY3JvbGxlci1hZCBub2luZGV4HwUCAmRkAhcPZBYCZg8PFgQfBAUmc2hybS10YWdzIHNocm0tdGFncy1lbXB0eSBhcnRpY2xlLXRhZ3MfBQICZBYCAgEPFgIfB2ZkAhsPZBYEAg0PFgIfAAWyAUhpLDxiciAvPjxiciAvPg0KDQpJIHRob3VnaHQgeW91J2QgbGlrZSB0aGlzIGFydGljbGUgSSBmb3VuZCBvbiB0aGUgU0hSTSB3ZWJzaXRlOiA8YnIgLz4NCjxhIGhyZWY9J2h0dHBzOi8vd3d3LnNocm0ub3JnL2Fib3V0LXNocm0vUGFnZXMvUHJpdmFjeS1Qb2xpY3kuYXNweCc+UHJpdmFjeSBQb2xpY3k8L2E+ICBkAg8PFgIeDGRhdGEtc2l0ZWtleQUoNkxjWTBUMFVBQUFBQU1Wb09BV1IxdzR1V1hBY2Q4MGVlSU84bzI5UmQCHQ9kFgJmDw8WAh8CZ2QWBgIBDw8WAh8GBWxodHRwczovL3d3dy5zaHJtLm9yZy9yZXNvdXJjZXNhbmR0b29scy9oci10b3BpY3MvdGVjaG5vbG9neS9wYWdlcy9oci1yZWNvZ25pdGlvbi1wZWVyLXRvLXBlZXItZW1wbG95ZWVzLmFzcHhkZAIDDw8WAh8CZ2QWAgIBDw8WBh8KBX9odHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wLGhfNDA4LHdfNzI0LHhfMCx5XzEvd19hdXRvOjEwMDoxNTAscV9hdXRvLGZfYXV0by92MS9UZWNobm9sb2d5L2lTdG9jay05MTIyMzUxMTJfdjVoemJ1HwsFKlB1dCBSZWNvZ25pdGlvbiBpbiBZb3VyIEVtcGxveWVlc+KAmSBIYW5kcx8MBSpQdXQgUmVjb2duaXRpb24gaW4gWW91ciBFbXBsb3llZXPigJkgSGFuZHNkZAIFDxYCHwAFKlB1dCBSZWNvZ25pdGlvbiBpbiBZb3VyIEVtcGxveWVlc+KAmSBIYW5kc2QCHw9kFgICAg9kFgICAQ9kFgQCBw9kFgICAQ8WAh8BCysEAWQCCQ9kFgICAQ88KwAJAQAPFgIeDU5ldmVyRXhwYW5kZWRnZGQCIw9kFgJmD2QWBAIBDxYCHw0FbHJldHVybiBPcGVuSW5OZXdXaW5kb3coJ2N0bDAwX1BsYWNlSG9sZGVyTWFpbl9IckpvYnNfc2VhcmNoSm9icycsJ2N0bDAwX1BsYWNlSG9sZGVyTWFpbl9IckpvYnNfc2VhcmNoVGVybScpO2QCAg8PFgYfAAUKUG9zdCBhIEpvYh8GBSRodHRwOi8vaHJqb2JzLnNocm0ub3JnL2pvYnMvcHJvZHVjdHMfAmdkZAIlD2QWAgIBDxYCHwcCAxYGZg9kFgYCAQ8PFgIfBgVGaHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvUGFnZXMvd29ya3BsYWNlLWhhcmFzc21lbnQuYXNweGQWAmYPDxYGHwoFhwJodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF8yNTMlMmN3XzQ1MCUyY3hfMCUyY3lfMC9jX2ZpdCUyY2ZfYXV0byUyY3FfYXV0byUyY3dfNzY3L3YxL1Rvb2xzJTIwYW5kJTIwU2FtcGxlcy8xOF8xNDg5X0h1cFBhZ2VfUnVsbHVwX1dvcmtwbGFjZUhhcmFzc21lbnRfYzVoY3VoP2RhdGFidG9hPWV5SXhObmc1SWpwN0luZ2lPakFzSW5raU9qQXNJbmd5SWpvME5UQXNJbmt5SWpveU5UTXNJbmNpT2pRMU1Dd2lhQ0k2TWpVemZYMCUzZB8LBTZXb3JrcGxhY2UgSGFyYXNzbWVudCBQcmV2ZW50aW9uIGFuZCBSZXNwb25zZSBSZXNvdXJjZXMfDAU2V29ya3BsYWNlIEhhcmFzc21lbnQgUHJldmVudGlvbiBhbmQgUmVzcG9uc2UgUmVzb3VyY2VzZGQCAw8PFgQfBgVGaHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvUGFnZXMvd29ya3BsYWNlLWhhcmFzc21lbnQuYXNweB8ABTZXb3JrcGxhY2UgSGFyYXNzbWVudCBQcmV2ZW50aW9uIGFuZCBSZXNwb25zZSBSZXNvdXJjZXNkZAIFDxYCHwdmZAIBD2QWBgIBDw8WAh8GBZsBaHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL29yZ2FuaXphdGlvbmFsLWFuZC1lbXBsb3llZS1kZXZlbG9wbWVudC9QYWdlcy9Db25maWRlbmNlLUlzLXRoZS1NaXNzaW5nLUluZ3JlZGllbnQtZm9yLUZlbWFsZS1MZWFkZXJzLmFzcHhkFgJmDw8WBh8KBYICaHR0cHM6Ly9jZG4uc2hybS5vcmcvaW1hZ2UvdXBsb2FkL2NfY3JvcCUyY2hfNTQzJTJjd185NjUlMmN4XzEyJTJjeV8wL2NfZml0JTJjZl9hdXRvJTJjcV9hdXRvJTJjd183NjcvdjEvT3JnYW5pemF0aW9uYWwlMjBhbmQlMjBFbXBsb3llZSUyMERldmVsb3BtZW50L0NsYWlyZV9TaGlwbWFuX2l6cmNnaD9kYXRhYnRvYT1leUl4Tm5nNUlqcDdJbmdpT2pFeUxDSjVJam93TENKNE1pSTZPVGMzTENKNU1pSTZOVFF6TENKM0lqbzVOalVzSW1naU9qVTBNMzE5HwsFOUNvbmZpZGVuY2UgSXMgdGhlICdNaXNzaW5nIEluZ3JlZGllbnQnIGZvciBGZW1hbGUgTGVhZGVycx8MBTlDb25maWRlbmNlIElzIHRoZSAnTWlzc2luZyBJbmdyZWRpZW50JyBmb3IgRmVtYWxlIExlYWRlcnNkZAIDDw8WBB8GBZsBaHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvaHItdG9waWNzL29yZ2FuaXphdGlvbmFsLWFuZC1lbXBsb3llZS1kZXZlbG9wbWVudC9QYWdlcy9Db25maWRlbmNlLUlzLXRoZS1NaXNzaW5nLUluZ3JlZGllbnQtZm9yLUZlbWFsZS1MZWFkZXJzLmFzcHgfAAU5Q29uZmlkZW5jZSBJcyB0aGUgJ01pc3NpbmcgSW5ncmVkaWVudCcgZm9yIEZlbWFsZSBMZWFkZXJzZGQCBQ8WAh8HZmQCAg9kFgYCAQ8PFgIfBgV/aHR0cHM6Ly93d3cuc2hybS5vcmcvUmVzb3VyY2VzQW5kVG9vbHMvbGVnYWwtYW5kLWNvbXBsaWFuY2UvZW1wbG95bWVudC1sYXcvUGFnZXMvQW1hem9uLVdvcmtlcnMtV2lsbC1Tb29uLUVhcm4tJDE1LWFuLUhvdXIuYXNweGQWAmYPDxYGHwoF6gFodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF8yMjY3JTJjd180MDMyJTJjeF8wJTJjeV8wL2NfZml0JTJjZl9hdXRvJTJjcV9hdXRvJTJjd183NjcvdjEvTGVnYWwlMjBhbmQlMjBDb21wbGlhbmNlL2FtYXpvbl9tbzJpd2E/ZGF0YWJ0b2E9ZXlJeE5uZzVJanA3SW5naU9qQXNJbmtpT2pBc0luZ3lJam8wTURNeUxDSjVNaUk2TWpJMk55d2lkeUk2TkRBek1pd2lhQ0k2TWpJMk4zMTkfCwUpQW1hem9uIFdvcmtlcnMgV2lsbCBTb29uIEVhcm4gJDE1IGFuIEhvdXIfDAUpQW1hem9uIFdvcmtlcnMgV2lsbCBTb29uIEVhcm4gJDE1IGFuIEhvdXJkZAIDDw8WBB8GBX9odHRwczovL3d3dy5zaHJtLm9yZy9SZXNvdXJjZXNBbmRUb29scy9sZWdhbC1hbmQtY29tcGxpYW5jZS9lbXBsb3ltZW50LWxhdy9QYWdlcy9BbWF6b24tV29ya2Vycy1XaWxsLVNvb24tRWFybi0kMTUtYW4tSG91ci5hc3B4HwAFKUFtYXpvbiBXb3JrZXJzIFdpbGwgU29vbiBFYXJuICQxNSBhbiBIb3VyZGQCBQ8WAh8HZmQCKQ9kFgRmDw8WBB8EBThyb3ctcHJvbW8tcGFuZWwgc2hybS13aWRnZXQgY29sLXNtLTEyIGNvbC1tZC0xMiBub2luZGV4IB8FAgJkFggCAQ8WAh8ABRNBcHBseSBieSBPY3RvYmVyIDE5ZAIDDw8WBB8GBTtodHRwczovL3d3dy5zaHJtLm9yZy9jZXJ0aWZpY2F0aW9uL2FwcGx5L3BhZ2VzL2RlZmF1bHQuYXNweB8CZ2QWAmYPDxYGHwoFvQJodHRwczovL2Nkbi5zaHJtLm9yZy9pbWFnZS91cGxvYWQvY19jcm9wJTJjaF8xMTkyJTJjd18yMTIxJTJjeF8wJTJjeV8xNzQvY19maXQlMmN3Xzc2Ny92MS9Ib21lJTIwUGFnZSUyMENhcm91c2VsL0NhbGVuZGFyXy1fQ29mZmVlLV9EZWFkbGluZV9obmM1YTg/ZGF0YWJ0b2E9ZXlJeU1YZzVJanA3SW5naU9qQXNJbmtpT2pZMExDSjRNaUk2TWpFeU1Td2llVElpT2prM01pd2lkeUk2TWpFeU1Td2lhQ0k2T1RBNGZTd2lNVFo0T1NJNmV5SjRJam93TENKNUlqb3hOelFzSW5neUlqb3lNVEl4TENKNU1pSTZNVE0yTnl3aWR5STZNakV5TVN3aWFDSTZNVEU1TW4xOR8LBWZHZXQgcmVjb2duaXplZCBhcyBhbiBIUiBleHBlcnQuIEVhcm4geW91ciBTSFJNLUNQIGFuZCBTSFJNLVNDUCBjZXJ0aWZpY2F0aW9uLCBhbmQgc2V0IHlvdXJzZWxmIGFwYXJ0LiAfDAVmR2V0IHJlY29nbml6ZWQgYXMgYW4gSFIgZXhwZXJ0LiBFYXJuIHlvdXIgU0hSTS1DUCBhbmQgU0hSTS1TQ1AgY2VydGlmaWNhdGlvbiwgYW5kIHNldCB5b3Vyc2VsZiBhcGFydC4gZGQCBQ8WAh8ABWZHZXQgcmVjb2duaXplZCBhcyBhbiBIUiBleHBlcnQuIEVhcm4geW91ciBTSFJNLUNQIGFuZCBTSFJNLVNDUCBjZXJ0aWZpY2F0aW9uLCBhbmQgc2V0IHlvdXJzZWxmIGFwYXJ0LiBkAgcPDxYEHwYFO2h0dHBzOi8vd3d3LnNocm0ub3JnL2NlcnRpZmljYXRpb24vYXBwbHkvcGFnZXMvZGVmYXVsdC5hc3B4HwAFCUFwcGx5IE5vd2RkAgIPDxYEHwQFN3Jvdy1wcm9tby1wYW5lbCBzaHJtLXdpZGdldCBjb2wtc20tNiBjb2wtbWQtMTIgbm9pbmRleCAfBQICZGQCLw9kFgJmDw8WAh8CaGRkAjEPZBYEZg8PFgIfAmdkFgYCAQ8PFgQfAAUnRmluZCB0aGUgUmlnaHQgVmVuZG9yIGZvciBZb3VyIEhSIE5lZWRzHwYFIGh0dHA6Ly92ZW5kb3JkaXJlY3Rvcnkuc2hybS5vcmcvZGQCAw8WAh8ABTtTSFJN4oCZcyBIUiBWZW5kb3IgRGlyZWN0b3J5IGNvbnRhaW5zIG92ZXIgMTAsMDAwIGNvbXBhbmllc2QCBQ8PFgQfBgUgaHR0cDovL3ZlbmRvcmRpcmVjdG9yeS5zaHJtLm9yZy8fAAUUU2VhcmNoICZhbXA7IENvbm5lY3RkZAICDxYCHwAFSTxzY3JpcHQgc3JjPScvL3d3dy5zaHJtLm9yZy9Eb2N1bWVudHMvU3BvbnNvck9mZmVyRmV0Y2hGaWxlLmpzJz48L3NjcmlwdD5kAjMPDxYCHwJoZGQCNQ9kFgICBA9kFgQCAw8WAh4FVmFsdWUFJDA1Zjc1NmE2LTcwOTAtNGQwMy05NzZmLWE0MzBmNTQ1YjU4ZWQCBQ8WAh8QBQVGYWxzZWQCDg9kFhxmDw8WAh8GBR1odHRwOi8vbC5zaHJtLm9yZy9vcHQvVW5pZmllZGRkAgEPFgIfAAUVSk9JTiBUSEUgQ09OVkVSU0FUSU9OZAICDw8WAh8GBTlodHRwOi8vd3d3LmZhY2Vib29rLmNvbS9zb2NpZXR5Zm9yaHVtYW5yZXNvdXJjZW1hbmFnZW1lbnRkZAIDDw8WAh8GBTdodHRwOi8vd3d3LmxpbmtlZGluLmNvbS9jb21wYW55LzExMjgyP3Ryaz1OVVNfQ01QWV9UV0lUZGQCBA8PFgIfBgUjaHR0cHM6Ly9pbnN0YWdyYW0uY29tL3Nocm1vZmZpY2lhbC9kZAIFDw8WAh8GBSNodHRwOi8vd3d3LnlvdXR1YmUuY29tL3Nocm1vZmZpY2lhbGRkAgYPDxYCHwYFGi9hYm91dC1zaHJtL3BhZ2VzL3Jzcy5hc3B4ZGQCBw8PFgIfBgUXaHR0cDovL3R3aXR0ZXIuY29tL1NIUk1kZAIIDxYCHwcCBhYMZg9kFgICAQ8WAh8IBRJjb2wteHMtNiBjb2wtc20tMTIWAgIBDw8WBB8GBTJodHRwczovL3d3dy5zaHJtLm9yZy9hYm91dC1zaHJtL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQpBYm91dCBTSFJNZGQCAQ9kFgICAQ8WAh8IBRJjb2wteHMtNiBjb2wtc20tMTIWAgIBDw8WBB8GBTVodHRwczovL3d3dy5zaHJtLm9yZy9hYm91dC1zaHJtL1BhZ2VzL01lbWJlcnNoaXAuYXNweB8ABQpNZW1iZXJzaGlwZGQCAg9kFgICAQ8WAh8IBRJjb2wteHMtNiBjb2wtc20tMTIWAgIBDw8WBB8GBUFodHRwczovL3d3dy5zaHJtLm9yZy9hYm91dC1zaHJtL1BhZ2VzL0J5bGF3cy0tQ29kZS1vZi1FdGhpY3MuYXNweB8ABRdCeWxhd3MgJiBDb2RlIG9mIEV0aGljc2RkAgMPZBYCAgEPFgIfCAUSY29sLXhzLTYgY29sLXNtLTEyFgICAQ8PFgQfBgU9aHR0cHM6Ly93d3cuc2hybS5vcmcvYWJvdXQtc2hybS9wcmVzcy1yb29tL1BhZ2VzL2RlZmF1bHQuYXNweB8ABQpQcmVzcyBSb29tZGQCBA9kFgICAQ8WAh8IBRJjb2wteHMtNiBjb2wtc20tMTIWAgIBDw8WBB8GBRRodHRwOi8vd3d3LmNmZ2kub3JnLx8ABR5Db3VuY2lsIGZvciBHbG9iYWwgSW1taWdyYXRpb25kZAIFD2QWAgIBDxYCHwgFEmNvbC14cy02IGNvbC1zbS0xMhYCAgEPDxYEHwYFFGh0dHA6Ly93d3cuaHJwcy5vcmcvHwAFFEhSIFBlb3BsZSArIFN0cmF0ZWd5ZGQCCQ8WAh8ABQtTSFJNIEdsb2JhbGQCCg8PFgIfBgVaaHR0cDovL3d3dy5zaHJtLm9yZy9hYm91dC9mb3VuZGF0aW9uL3N1cHBvcnR0aGVmb3VuZGF0aW9uL2NvbnRyaWJ1dGlvbnMvcGFnZXMvZGVmYXVsdC5hc3B4ZGQCCw8PFgQfBgUhL2Fib3V0LXNocm0vUGFnZXMvQ29udGFjdC1Vcy5hc3B4HwAFCkNvbnRhY3QgVXNkZAIMDw8WBB8GBRd0ZWw6ODAwLjI4My5TSFJNICg3NDc2KR8ABRM4MDAuMjgzLlNIUk0gKDc0NzYpZGQCDQ9kFgICAQ8WAh8ABQxDdXJyZW50IFVzZXJkAg8PZBYCAgEPZBYCAgEPPCsABQEADxYCHhVQYXJlbnRMZXZlbHNEaXNwbGF5ZWRmZGQCMw9kFgICAQ9kFgICDQ8PFgIfAmhkFgICAg9kFgJmD2QWAgIDD2QWAgIFD2QWAgIBDzwrAAkBAA8WBB4NUGF0aFNlcGFyYXRvcgQIHw9nZGQCEw9kFgJmDw8WAh8KBYoBfi9fbGF5b3V0cy8xNS9TSFJNLkNvcmUvdXRpbGl0eS9wYWdldmlld3RyYWNrZXIuYXNweD91c2VyPUFub255bW91cyZ1cmw9JTJmYWJvdXQtc2hybSUyZlBhZ2VzJTJmUHJpdmFjeS1Qb2xpY3kuYXNweCZpc01lbWJlck9ubHlQYWdlPUZhbHNlZGRk+Ha/nlD7QbiVfJs0rksxXjmwjETnIFOObdIKG6HL/Rg=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=4yL1y1QJ0QhVEUCrTTXpDzuEWyMCtTXZY6B6_TJaPJuc9HrvB8a_21p-Bsmy6K_l1LufbwXFRBZL7t0Jz3QBse1ipb8E3o9qWwtZDTbEWQ81&amp;t=636486628482159859" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaQoSEnabled = false;
var g_wsaQoSDataPoints = [];
var g_wsaLCID = 1033;
var g_wsaListTemplateId = 850;
var g_wsaSiteTemplateId = 'CMSPUBLISHING#0';
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fabout-shrm", webAbsoluteUrl: "https:\u002f\u002fwww.shrm.org\u002fabout-shrm", siteAbsoluteUrl: "https:\u002f\u002fwww.shrm.org", serverRequestPath: "\u002fabout-shrm\u002fPages\u002fPrivacy-Policy.aspx", layoutsUrl: "_layouts\u002f15", webTitle: "About SHRM", webTemplate: "39", tenantAppVersion: "0", isAppWeb: false, webLogoUrl: "_layouts\u002f15\u002fimages\u002fsiteicon.png", webLanguage: 1033, currentLanguage: 1033, currentUICultureName: "en-US", currentCultureName: "en-US", clientServerTimeDelta: new Date("2018-10-03T15:10:10.4260982Z") - new Date(), siteClientTag: "1283$$15.0.5047.1000", crossDomainPhotosEnabled:false, webUIVersion:15, webPermMasks:{High:16,Low:196673},pageListId:"{73895ffe-9776-4928-b219-3dc68dcd539a}",pageItemId:6, pagePersonalizationScope:1, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};var dlc_fvsi = {"DefaultViews":[],"ViewUrls":[],"WebUrl":"\/about-shrm"};
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {

    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fabout-shrm\u002fPages\u002fPrivacy-Policy.aspx");

}
//]]>
</script>

<script src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
(function(){

        if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
            return;
        }
        _spBodyOnLoadFunctions.push(function() 
        {
          ExecuteOrDelayUntilScriptLoaded(
            function()
            {
              var pairs = SP.ScriptHelpers.getDocumentQueryPairs();
              var followDoc, itemId, listId, docName;
              for (var key in pairs)
              {
                if(key.toLowerCase() == 'followdocument') 
                  followDoc = pairs[key];
                else if(key.toLowerCase() == 'itemid') 
                  itemId = pairs[key];
                else if(key.toLowerCase() == 'listid') 
                  listId = pairs[key];
                else if(key.toLowerCase() == 'docname') 
                  docName = decodeURI(pairs[key]);
              } 

              if(followDoc != null && followDoc == '1' && listId!=null && itemId != null && docName != null)
              {
                SP.SOD.executeFunc('followingcommon.js', 'FollowDocumentFromEmail', function() 
                { 
                  FollowDocumentFromEmail(itemId, listId, docName);
                });
              }

            }, 'SP.init.js');

        });
    })();(function(){

        if (typeof(_spBodyOnLoadFunctions) === 'undefined' || _spBodyOnLoadFunctions === null) {
            return;
        }
        _spBodyOnLoadFunctions.push(function() {

            if (typeof(SPClientTemplates) === 'undefined' || SPClientTemplates === null || (typeof(APD_InAssetPicker) === 'function' && APD_InAssetPicker())) {
                return;
            }

            var renderFollowFooter = function(renderCtx,  calloutActionMenu)
            {
                if (renderCtx.ListTemplateType == 700) 
                    myDocsActionsMenuPopulator(renderCtx, calloutActionMenu);
                else
                    CalloutOnPostRenderTemplate(renderCtx, calloutActionMenu);

                var listItem = renderCtx.CurrentItem;
                if (typeof(listItem) === 'undefined' || listItem === null) {
                    return;
                }
                if (listItem.FSObjType == 0) {
                    calloutActionMenu.addAction(new CalloutAction({
                        text: Strings.STS.L_CalloutFollowAction,
                        tooltip: Strings.STS.L_CalloutFollowAction_Tooltip,
                        onClickCallback: function (calloutActionClickEvent, calloutAction) {
                            var callout = GetCalloutFromRenderCtx(renderCtx);
                            if (!(typeof(callout) === 'undefined' || callout === null))
                                callout.close();
                            SP.SOD.executeFunc('followingcommon.js', 'FollowSelectedDocument', function() { FollowSelectedDocument(renderCtx); });
                        }
                    }));
                }
            };

            var registerOverride = function(id) {
                var followingOverridePostRenderCtx = {};
                followingOverridePostRenderCtx.BaseViewID = 'Callout';
                followingOverridePostRenderCtx.ListTemplateType = id;
                followingOverridePostRenderCtx.Templates = {};
                followingOverridePostRenderCtx.Templates.Footer = function(renderCtx) {
                    var  renderECB;
                    if (typeof(isSharedWithMeView) === 'undefined' || isSharedWithMeView === null) {
                        renderECB = true;
                    } else {
                        var viewCtx = getViewCtxFromCalloutCtx(renderCtx);
                        renderECB = !isSharedWithMeView(viewCtx);
                    }
                    return CalloutRenderFooterTemplate(renderCtx, renderFollowFooter, renderECB);
                };
                SPClientTemplates.TemplateManager.RegisterTemplateOverrides(followingOverridePostRenderCtx);
            }
            registerOverride(101);
            registerOverride(700);
        });
    })();if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();function WebForm_OnSubmit() {
UpdateFormDigest('\u002fabout-shrm', 1440000);if (typeof(_spFormOnSubmitWrapper) != 'undefined') {return _spFormOnSubmitWrapper();} else {return true;};
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="DD7C858C" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdADS6obsKJKkzebL4ZJ0hWdSpDQe9fvP9DoKx2GZjwiNT9+pp2zw3Tgrn8rLgj5rS+4cTdLq8LW599yBLi0c2ZcB3YkJ27LjNOL30+AlZEgGZbS2rAEd8ERHOBHCxdD66PEDuRYNd9RthI+JH9RDWPwg3ne0mjS3W5SZgCBXYpVI1c7q/WnDGF2xJ7hOWzeDq4fY3Nibwe54jb51Ly/3IDlzK2tFYN8RC/lbig3d4Gemc0Fp+n2tVPbzcsDJ84A4kKdSKT/EgeipEtn7+aAQNO9PL1WDlmmeKVdHQbdEfzU16EwfQIVpENndvhV4EIqM/UK7eg3lcuxK59UzuMAJUeEga7lub4Mzn0M5lsMUbg65LIf9U1Qkbrpf2fwV5Z0ujAVOGm24NmG/E9BJeaUpf+cVN7MFJ84RzJM4JJWPOyRBJZ+0Kw301fpjg/58MITLMs7sQLy8HNQlXaUVgfGa21GJYHTv2ozge4q7OeHadB3woPFDLXG5UAt6JnV9UdZXSRCsfI2MbS84ZZ+qBpeWgcR6l2IoXCc2/On9nJ66GEDPIi0pguoOK4ZEWPnXK1D2XaVPdIFgnVRv4/HigRIE1O5l1Zcww6FM/UpIl5/BVPICEeHVvFj/irGQXcrTJL8qbNT3h+Ja6nmZua4Hdkj4Hn7/GvE1/udu/2T8ng7y5/6ZAqkJSf7aolSABAmyM5Ybm2imYsVGOW8HRQPdJxh91CHSnp1YSVl/gw12fRl3EbpUx/ryr1KjFfaN7YFjGmiY2VVVWGOFSbujXNUn2lAF07+FWfhcZm+W9qkCtakLjITNp1eJNIa4IKdJ0M6pFRVPRZgTojqDbCfNFtf3onJGObh4mbaCTQ0uGPxY/1JfD1OQX1QetHIV0SRr5D4N+l4FE8y9fWYDLCkiXZuLIpYH5M33HdLXJ7OJgnfgjweUtGHQILHvu3IZUc7DOXtyMlLxbTDMV+TRGUoY3tP4zZJ3h9Rixt/e2XyU5imw853pkqeIup9g5scp14/1o2dgWWGEooU3ZnalUlUdd4ECNVYL+DW4v8GgLeSPI4zCsNmS9cGv58AHqT3ijFMiYhuu4+68JWn8HIzdC/wjexth7/Pvvd4pfow1WXLgDl13sriEUF+KPvwfhWPXHMwTG3/fmaeYfSVE=" />
</div>
			<script type="text/javascript"> var submitHook = function () { return false; }; theForm._spOldSubmit = theForm.submit; theForm.submit = function () { if (!submitHook()) { this._spOldSubmit(); } }; </script>
			
			<!-- End - Ribbon -->
			<input type="hidden" name="ctl00$ctl71$hf_SearchHost" id="ctl00_ctl71_hf_SearchHost" value="https://shrmsearch.shrm.org" />
<input type="hidden" name="ctl00$ctl71$hf_SearchDomain" id="ctl00_ctl71_hf_SearchDomain" value="https://www.shrm.org" />
<input type="hidden" name="ctl00$ctl71$hf_SiteSearch" id="ctl00_ctl71_hf_SiteSearch" value="https://store.shrm.org" />
<input type="hidden" name="ctl00$ctl71$hf_SiteCollection" id="ctl00_ctl71_hf_SiteCollection" value="prod_kafka" />
<input type="hidden" name="ctl00$ctl71$hf_SearchClient" id="ctl00_ctl71_hf_SearchClient" value="2016_shrm_org" />
<input type="hidden" name="ctl00$ctl71$hf_SearchDisableSectionCounts" id="ctl00_ctl71_hf_SearchDisableSectionCounts" value="1" />
<input type="hidden" name="ctl00$ctl71$hf_RestrictedKickers" id="ctl00_ctl71_hf_RestrictedKickers" value="/hr-today
/hr-today/news
/hr-today/public-policy
/hr-today/trends-and-forecasting
/resourcesandtools
/resourcesandtools/hr-topics
/resourcesandtools/legal-and-compliance
/resourcesandtools/business-solutions
/resourcesandtools/tools-and-samples
/learningandcareer
/learningandcareer/learning
/communities
/communities/communities
/communities/volunteers
/authors
/LearningAndCareer/learning/seminars-materials
/learningandcareer/career" />

            <input type="hidden" id="hf_Cloudinary_SelectedObject" />
            <input type="hidden" id="hf_Cloudinary_SelectedCaret" />
            <input type="hidden" id="hf_Cloudinary_SelectedCaretParent" />
			<!-- Start - Working area (to be use as-is, but you are free to add some classes) -->
			<div id="s4-workspace" class="ms-core-overlay">
				<div id="s4-bodyContainer">
					<div id="contentRow">
						<div id="contentBox" aria-live="polite" aria-relevant="all">
  							<div id="notificationArea" class="ms-notif-box"></div>
							<div id="DeltaPageStatusBar">
	
								<div id="pageStatusBar"></div>
							
</div>
							<div id="DeltaPlaceHolderMain">
	
								<a id="mainContent" name="mainContent" tabindex="-1"></a>
								<!-- -----------------------------------	SHRM SITE START	---------------------------------------------------------->	
								<!-- TEMP SWITCH ROLE BTN :: PRESENTATION ONLY :: TO BE REMOVED	--> 
								<!--a id="switchRole" class="btn btn-default" style="position:fixed; z-index:2" data-toggle="tooltip" data-placement="right" title="Switch membership status"><i class="fa fa-random"></i></a-->
								<!--/ TEMP SWITCH ROLE BTN :: PRESENTATION ONLY :: TO BE REMOVED	--> 
								
								<div class="shrm-site">
									
                                    
								    <!-- --------------------------------- SHRM MEMBER NAVIGATION	CONTAINER STARTS	-->
								    
<input type="hidden" name="ctl00$AutoExpandDelaySeconds" id="AutoExpandDelaySeconds" value="6" />
<input type="hidden" name="ctl00$AutoExpandDisableHours" id="AutoExpandDisableHours" value="24" />


                                    <div id="ctl00_ctl73_pan_PromotionalBar" class="row promotional-bar hidden-xs hidden-print promotional-bar-seasonal promotional-bar-red">
		
    <div class="container">
    	<div class="promotional-bar-wrapper">
            <div class="promo-msg">
                <p>
                    <b>Special Promo Extended! >></b> Join or renew with code TOTE2018 by Oct. 15 for $20 off and a free SHRM tote!
                </p>
            </div>
            <a id="ctl00_ctl73_hl_Link1" class="btn btn-default btn-sm btn-cta btn-cta-red" href="https://membership.shrm.org/?PRODUCT_DISCOUNT_ID=TOTE2018">JOIN TODAY</a>
            <a id="ctl00_ctl73_hl_Link2" class="btn btn-default btn-sm btn-cta btn-ghost" href="https://membership.shrm.org/?PRODUCT_DISCOUNT_ID=TOTE2018">RENEW NOW</a>
      </div>
    </div>

	</div>

								    <!-- --------------------------------- SHRM HEADER STARTS	-->
								    <input type="hidden" name="ctl00$SSOUUIDHidden" id="SSOUUIDHidden" />
<input type="hidden" name="ctl00$EmailHidden" id="EmailHidden" />
<input type="hidden" name="ctl00$MemberIdHidden" id="MemberIdHidden" />
<input type="hidden" name="ctl00$MemberStatusHidden" id="MemberStatusHidden" value="0" />
<input type="hidden" name="ctl00$AuthTokenHidden" id="AuthTokenHidden" />
<input type="hidden" name="ctl00$AuthTypeHidden" id="AuthTypeHidden" />
<input type="hidden" name="ctl00$MySHRMAPITokenHidden" id="MySHRMAPITokenHidden" />
<input type="hidden" name="ctl00$GreetingNameHidden" id="GreetingNameHidden" />
<input type="hidden" name="ctl00$CertificatesHidden" id="CertificatesHidden" />
<input type="hidden" name="ctl00$Member2MemberIsActive" id="Member2MemberIsActive" value="True" />
<input type="hidden" name="ctl00$HeartBeatStatus" id="HeartBeatStatus" />
<input type="hidden" name="ctl00$CareerPortalIsActive" id="CareerPortalIsActive" value="True" />
<input type="hidden" name="ctl00$hf_TourButtonVisible" id="hf_TourButtonVisible" value="0" />
<input type="hidden" name="ctl00$SiteTourEnabledHidden" id="SiteTourEnabledHidden" />

<input type="hidden" name="ctl00$TaxonomyHidden" id="TaxonomyHidden" />
<input type="hidden" name="ctl00$GeolocationHidden" id="GeolocationHidden" value="US,California" />
<input type="hidden" name="ctl00$CurrentAnnualConferenceHidden" id="CurrentAnnualConferenceHidden" />
<input type="hidden" name="ctl00$NetSuiteEnabledHidden" id="NetSuiteEnabledHidden" value="False" />

<header class="container container-header hidden-xs noindex">
    <div class="row">
        <div class="top-bar clearfix">
            <div class="header-brand pull-left"></div>
            <span class="header-brand pull-left"><a href="/pages/default.aspx">
                <img class="img-responsive" src="/_layouts/15/SHRM.Core/design/images/SHRMLogo.svg" alt="SHRM Logo" /></a>
            </span>
            <div id="ctl00_nesto" class="header-nav pull-right">
                <ul class="utility-links text-right">
                    <li class="dropdown">
                        
                    </li>
                    <li>
                        <div id="ctl00_SignInPanel">
	
                            <a href="https://www.shrm.org/_layouts/authenticate.aspx?source=/about-shrm/Pages/Privacy-Policy.aspx">Sign In</a> |
                        
</div>
                    </li>
                    <li class="dropdown">
                        <a id="language-options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe">&nbsp;</i><i class="fa fa-angle-down"></i>
                            <span id="language-id" class="notranslate">U.S. - EN</span></a> |
                        <ul class="dropdown-menu" aria-labelledby="language-options">
                            <li><a href="/pages/default.aspx?loc=null">SHRM GLOBAL</a></li>
                            
                            <li><a href="/pages/default.aspx?loc=india">SHRM India</a></li>
                            <li><a href="/about-shrm/Pages/SHRM-MENA.aspx">SHRM MENA</a></li>
                            
                        </ul>
                    </li>
                    <li><a id="ctl00_hl_AskAnAdvisor" href="/ResourcesAndTools/tools-and-samples/Pages/HR-Help.aspx">Ask an Advisor</a> | </li>
                    <li><a href="https://store.shrm.org/">SHRM Store</a></li>
                    <li><a href="https://store.shrm.org/checkout/cart/"><span class="shop-items"><span id="cartItemCount" style="display:none;"></span></span></a></li>
                </ul>
                <div class="header-ctas text-right">
                    <div class="btn btn-success btn-lg btn-cta">
                        <a id="ctl00_MembershipCallToActionLink" href="/about-shrm/pages/membership.aspx">Membership</a>
                    </div>
                    <div class="btn btn-success-dark btn-lg btn-cta">
                        <a id="ctl00_CertificationCallToActionLink" href="/certification/pages/default.aspx">GET CERTIFIED</a>                        
                    </div>
                </div>
            </div>
        </div>        <!--/ .top-bar	-->
        <div class="header-bar">
        </div>        <!--/ .header-bar	-->
    </div>
</header><!--/ .row-header	-->

								  
								    <!-- --------------------------------- SHRM NAVIGATION STARTS	-->  
								 	<div class="container-fluid container-nav noindex">
                                        <div ui-view=""></div>
                                        <a id="cloudinary-open" ui-sref="start()"></a>
                                        <a id="cloudinary-open-wideimage" ui-sref="start({ only : ['21x9'] , width : '1920'})"></a>
                                        <a id="cloudinary-open-m2m" ui-sref="start()"></a>
									  	

<nav class="navbar navbar-default row">
    <div class="container">
        <div class="mobile-header">
            <div class="mobile-control">
                <div>
                    <button type="button" class="navbar-toggle navbar-inverse pull-left collapsed" data-toggle="collapse" data-target="#site-main-nav" aria-expanded="false" aria-controls="site-main-nav">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                </div>
                <div class="mobile-logo"><span class="header-brand visible-xs"><a href="/" class="mobile-logo">
                    <img src="/_layouts/15/SHRM.Core/design/images/SHRMLogo.svg" alt="SHRM Logo"></a></span>
                </div>
                <div>
                    <a id="main-search" class="btn btn-danger pull-right visible-xs" role="button" data-toggle="collapse" href="#mobile-search-bar-row" aria-expanded="false" aria-controls="search-bar-row"><i class="fa fa-search"><span class="sr-only">Search</span></i></a>
                    <a id="main-search" class="btn btn-danger pull-right hidden-xs" role="button" data-toggle="collapse" href="#search-bar-row" aria-expanded="false" aria-controls="search-bar-row"><i class="fa fa-search"><span class="sr-only">Search</span></i></a>
                </div>
            </div>
            <div class="site-tagline visible-xs">
                <div>Society For Human Resource Management</div>
            </div>
        </div>
        <!--/ .mobile-header	-->

        <!--	collapsable search bar for mobile affixed navigation	-->
        <div class="row search-bar-row text-center collapse" id="mobile-search-bar-row">
            <div class="container visible-xs">
              <div class="search-bar-group">
                <div class="dropdown hidden-xs">
                  <button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary">
                    <span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
                    <li><a href="#">SHRM Foundation</a></li>
                    <li><a href="#">SHRM India</a></li>
                    <li><a href="#">SHRM China</a></li>
                  </ul>
                </div><!--/ .dropdown	-->
                <div class="search-bar-widget search-bar-xs-container">
                    <!--window.location.href = '/search/Pages/default.aspx'-->
                    <a class="searchSubmitButton" role="button" onclick="window.location.href = '/search/Pages/default.aspx?k=' + $(this).next().val(); "></a>
                    <label for="search-bar-main" class="sr-only">SEARCH</label>
                    <input type="text" class="form-control" id="search-bar-main" onkeydown="if(event.keyCode == 13){window.location.href = '/search/Pages/default.aspx?k=' + $(this).val();}" />          
                    <div class="search-bar-widget-results"></div>
                </div><!-- /.search-bar-lg-container	-->
                
              </div><!--	/.search-bar-group	-->
            </div><!--	/.container	-->
        </div> <!--/ .search-bar-row	-->
        <!--/	collapsable search bar for affixed navigation	-->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="site-main-nav" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl75_rep_Navigation_ctl00_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl00_pan_DropoutMenu' data-toggle='collapse' role='button'>HR Today</a>
                            <div id="ctl00_ctl75_rep_Navigation_ctl00_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl00_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl00_pan_DropoutMenu' data-toggle='collapse' role='button'>HR Today</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>News</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>News</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/hr-today/news/hr-news/Pages/default.aspx">HR News</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/news/hr-magazine/0918/Pages/default.aspx">HR Magazine</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="http://blog.shrm.org/">SHRM Blog</a></li>
                                                        
                                                            <li></li>
                                                        
                                                            <li></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Public Policy</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Public Policy</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="http://www.advocacy.shrm.org/home">Take Action</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/public-policy/hr-public-policy-issues/Pages/default.aspx">HR Public Policy Issues</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl02_hl_Level3_Link" href="http://www.advocacy.shrm.org/about">A-Team Advocacy Network</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl01_rep_Navigation3_ctl03_hl_Level3_Link" href="/hr-today/public-policy/state-affairs/Pages/default.aspx">State Affairs</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Trends & Forecasting</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Trends & Forecasting</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/hr-today/trends-and-forecasting/research-and-surveys/Pages/default.aspx">Research & Surveys</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/hr-today/trends-and-forecasting/labor-market-and-economic-data/Pages/default.aspx">Labor Market & Economic Data</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl00_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/hr-today/trends-and-forecasting/special-reports-and-expert-views/Pages/default.aspx">Special Reports & Expert Views</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl75_rep_Navigation_ctl00_ucc_FeaturedArticle_pan_Main" class="column col-md-3 col-featured visible-md visible-lg" data-personalized-offer="False">
			
    <div class="siteMenuColHeader"></div>
    <a id="ctl00_ctl75_rep_Navigation_ctl00_ucc_FeaturedArticle_hl_Image" href="https://www.shrm.org/hr-today/news/hr-news/Pages/hr-newsletters.aspx"><img id="ctl00_ctl75_rep_Navigation_ctl00_ucc_FeaturedArticle_img_Image" title="SHRM Newsletters" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop,h_1192,w_2121,x_0,y_59/w_auto:100:228,q_auto,f_auto/v1/News/iStock-679501242_yewna7" alt="SHRM Newsletters" /></a>
    <h6>
        <a id="ctl00_ctl75_rep_Navigation_ctl00_ucc_FeaturedArticle_hl_Title" href="https://www.shrm.org/hr-today/news/hr-news/Pages/hr-newsletters.aspx">SHRM Newsletters</a>
    </h6>
    <p>Sign up for free email newsletters and get more SHRM content delivered to your inbox.</p>
    <a id="ctl00_ctl75_rep_Navigation_ctl00_ucc_FeaturedArticle_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://www.shrm.org/hr-today/news/hr-news/Pages/hr-newsletters.aspx">Manage Subscriptions</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl01_pan_DropoutMenu' data-toggle='collapse' role='button'>Resources & Tools</a>
                            <div id="ctl00_ctl75_rep_Navigation_ctl01_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl01_pan_DropoutMenu' data-toggle='collapse' role='button'>Resources & Tools</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>HR Topics</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>HR Topics</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/behavioral-competencies/Pages/default.aspx">Behavioral Competencies</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/benefits/Pages/default.aspx">Benefits</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/Pages/california-resources.aspx">California Resources</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/compensation/Pages/default.aspx">Compensation</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/Pages/diversity-and-inclusion.aspx">Diversity & Inclusion</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl05_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/employee-relations/Pages/default.aspx">Employee Relations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl06_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/global-hr/Pages/default.aspx">Global HR</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl07_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/labor-relations/Pages/default.aspx">Labor Relations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl08_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/organizational-and-employee-development/Pages/default.aspx">Organizational & Employee Development</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl09_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/risk-management/Pages/default.aspx">Risk Management</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl10_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/talent-acquisition/Pages/default.aspx">Talent Acquisition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl00_rep_Navigation3_ctl11_hl_Level3_Link" href="/ResourcesAndTools/hr-topics/technology/Pages/default.aspx">Technology</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Legal & Compliance</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Legal & Compliance</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="/ResourcesAndTools/legal-and-compliance/employment-law/Pages/default.aspx">Employment Law</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/ResourcesAndTools/legal-and-compliance/state-and-local-updates/Pages/default.aspx">State & Local Updates</a></li>
                                                        
                                                            <li></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl01_rep_Navigation3_ctl03_hl_Level3_Link" href="http://cfgi.org/">Immigration Employer Network</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Business Solutions</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Business Solutions</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/ResourcesAndTools/business-solutions/Pages/benchmarking.aspx">Benchmarking Service</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="https://brokerfinder.shrm.org/">Benefits Broker Directory</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/ResourcesAndTools/business-solutions/Pages/Employee-Engagement-Survey-Service.aspx">Employee Engagement Survey</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl03_hl_Level3_Link" href="/ResourcesAndTools/business-solutions/Pages/Salary-Data-Service.aspx">Salary Data Service</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl04_hl_Level3_Link" href="https://tac.shrm.org/">Talent Assessment Center</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl02_rep_Navigation3_ctl05_hl_Level3_Link" href="https://vendordirectory.shrm.org/">Vendor Directory</a></li>
                                                        
                                                            <li></li>
                                                        
                                                            <li></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Tools & Samples</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Tools & Samples</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/Pages/employee-handbooks.aspx">Employee Handbooks</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/exreq/Pages/Trending-Topics.aspx">Express Requests</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl02_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/how-to-guides/Pages/default.aspx">How-To Guides</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl03_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/hr-forms/Pages/default.aspx">HR Forms</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl04_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/hr-qa/Pages/default.aspx">HR Q&As</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl05_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/interview-questions/Pages/default.aspx">Interview Questions</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl06_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/job-descriptions/Pages/default.aspx">Job Descriptions</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl07_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/policies/Pages/default.aspx">Policies</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl08_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/presentations/Pages/default.aspx">Presentations</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl09_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/Pages/spreadsheets.aspx">Spreadsheets</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl10_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/toolkits/Pages/default.aspx">Toolkits</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl01_rep_Navigation2_ctl03_rep_Navigation3_ctl11_hl_Level3_Link" href="/ResourcesAndTools/tools-and-samples/member2member/Pages/default.aspx">Member2Member Solutions</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl75_rep_Navigation_ctl01_ucc_FeaturedArticle_pan_Main" class="column col-md-3 col-featured visible-md visible-lg" data-personalized-offer="False">
			
    <div class="siteMenuColHeader">JOB DESCRIPTION MANAGER</div>
    <a id="ctl00_ctl75_rep_Navigation_ctl01_ucc_FeaturedArticle_hl_Image" href="https://store.shrm.org/job-description-manager.html"><img id="ctl00_ctl75_rep_Navigation_ctl01_ucc_FeaturedArticle_img_Image" title="Job Description Manager" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop,h_899,w_1600,x_0,y_0/w_auto:100:228,q_auto,f_auto/v1/Marketing/JDM_dcipir" alt="Job Description Manager" /></a>
    <h6>
        <a id="ctl00_ctl75_rep_Navigation_ctl01_ucc_FeaturedArticle_hl_Title" href="https://store.shrm.org/job-description-manager.html">Job Description Manager</a>
    </h6>
    <p>Create, Maintain & Organize Your Job Descriptions. It’s fast. It’s easy.</p>
    <a id="ctl00_ctl75_rep_Navigation_ctl01_ucc_FeaturedArticle_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://store.shrm.org/job-description-manager.html">LEARN MORE</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl02_pan_DropoutMenu' data-toggle='collapse' role='button'>Learning & Career</a>
                            <div id="ctl00_ctl75_rep_Navigation_ctl02_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl02_pan_DropoutMenu' data-toggle='collapse' role='button'>Learning & Career</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Career</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>Career</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/Accelerate-Your-Career.aspx">Accelerate Your Career</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/Career-Preparation-and-Planning.aspx">Career Preparation & Planning</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/shrm-competency-model.aspx">SHRM Competency Model</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/Your-Professional-Development.aspx">Your Professional Development</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="/LearningAndCareer/Career/Pages/Career-Expert-Insights.aspx">Career Expert Insights</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>HR Jobs</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>HR Jobs</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="http://www.shrm.org">uccHrJobs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="http://jobs.shrm.org/">Browse All Jobs...</a></li>
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Learning</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Learning</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/Seminars.aspx">Seminars</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/LearningAndCareer/learning/onsite-training/Pages/default.aspx">Onsite Training</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/SHRM-eLearning.aspx">eLearning</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl03_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/SHRM-Essentials-of-Human-Resources.aspx">SHRM Essentials of Human Resources</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl04_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/SHRM-Senior-Leadership-Programs.aspx">Senior Leadership Programs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl05_hl_Level3_Link" href="https://store.shrm.org/events/shrm-events/virtual-events.html">Virtual Events</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl06_hl_Level3_Link" href="/LearningAndCareer/learning/webcasts/Pages/default.aspx">Webcasts</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl02_rep_Navigation3_ctl07_hl_Level3_Link" href="/LearningAndCareer/learning/Pages/Specialty-Credentials.aspx">Specialty Credentials</a></li>
                                                        
                                                            <li></li>
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Certification</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Certification</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="https://www.shrm.org/certification/apply/pages/default.aspx">Apply for Exam</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="https://www.shrm.org/certification/learning/pages/default.aspx">Certification Preparation</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl02_hl_Level3_Link" href="https://www.shrm.org/certification/faqs/pages/default.aspx">SHRM Certification FAQs</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl03_rep_Navigation3_ctl03_hl_Level3_Link" href="https://www.shrm.org/certification/recertification/pages/default.aspx">Recertification</a></li>
                                                        
                                                            <li></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>For Educators</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl04_ul_DropoutMenu2' data-toggle='collapse' role='button'>For Educators</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl00_hl_Level3_Link" href="/academicinitiatives/universities/pages/guidebook.aspx">HR Curriculum Guidebook & Template</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl01_hl_Level3_Link" href="/academicinitiatives/students/pages/hrprogramdirectory.aspx">HR Program Directory</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl02_rep_Navigation2_ctl04_rep_Navigation3_ctl02_hl_Level3_Link" href="/academicinitiatives/universities/teachingresources/pages/termsofuse_faculty.aspx">Teaching Resources</a></li>
                                                        
                                                            <li></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl75_rep_Navigation_ctl02_ucc_FeaturedArticle_pan_Main" class="column col-md-3 col-featured visible-md visible-lg" data-personalized-offer="False">
			
    <div class="siteMenuColHeader">IN-PERSON SHRM SEMINARS</div>
    <a id="ctl00_ctl75_rep_Navigation_ctl02_ucc_FeaturedArticle_hl_Image" href="https://store.shrm.org/training/learning-center/seminars/find-a-seminar.html"><img id="ctl00_ctl75_rep_Navigation_ctl02_ucc_FeaturedArticle_img_Image" title="Local Development Opportunities" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop,h_1723,w_3065,x_0,y_54/w_auto:100:228,q_auto,f_auto/v1/Home%20Page%20Carousel/18-0895_Map_image_oyakdd" alt="Local Development Opportunities" /></a>
    <h6>
        <a id="ctl00_ctl75_rep_Navigation_ctl02_ucc_FeaturedArticle_hl_Title" href="https://store.shrm.org/training/learning-center/seminars/find-a-seminar.html">Local Development Opportunities</a>
    </h6>
    <p>Build competencies, establish credibility and advance your career—while earning PDCs—at SHRM Seminars in 14 cities across the U.S. this fall.</p>
    <a id="ctl00_ctl75_rep_Navigation_ctl02_ucc_FeaturedArticle_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://store.shrm.org/training/learning-center/seminars/find-a-seminar.html">SEE 2018 SEMINAR LOCATIONS</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl03_pan_DropoutMenu' data-toggle='collapse' role='button'>Events</a>
                            <div id="ctl00_ctl75_rep_Navigation_ctl03_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl03_pan_DropoutMenu' data-toggle='collapse' role='button'>Events</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>SHRM Events</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>SHRM Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="http://annual.shrm.org/">SHRM Annual Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/mlp/Pages/Diversity2018.aspx">Diversity & Inclusion Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="https://conferences.shrm.org/legislative-conference">Employment Law & Legislative Conference</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="https://conferences.shrm.org/leadership-development-forum">Leadership Development Forum</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="http://conferences.shrm.org/talent-conference">Talent Conference & Exposition</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl05_hl_Level3_Link" href="/Events/Pages/State--Affiliate-Conferences.aspx">State & Affiliate Conferences</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl06_hl_Level3_Link" href="https://store.shrm.org/events/shrm-events/virtual-events.html">Virtual Events</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl07_hl_Level3_Link" href="/LearningAndCareer/learning/webcasts/Pages/default.aspx">Webcasts</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl00_rep_Navigation3_ctl08_hl_Level3_Link" href="/Events/Pages/default.aspx">SEE ALL EVENTS</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Event Resources</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Event Resources</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="https://shrm.org/mlp/Pages/speakers-bureau/Home.aspx">Speakers Bureau</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl01_hl_Level3_Link" href="/Events/Pages/Speaker-Information.aspx">Conference Speaker Information</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl02_hl_Level3_Link" href="http://conferences.shrm.org/exhibit-or-sponsor">Sponsorship & Exhibitor Information</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl01_rep_Navigation3_ctl03_hl_Level3_Link" href="http://l.shrm.org/requestmoreinformation">Request a Brochure</a></li>
                                                        
                                                            
                                                        
                                                            
                                                        
                                                            
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Global Events</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Global Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/Events/shrm-india-events/Pages/default.aspx">SHRM India Events</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/Events/Pages/shrm-apac-events.aspx">SHRM APAC Events</a></li>
                                                        
                                                </ul>
                                            
                                        
                                            
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Affiliate Events</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl03_ul_DropoutMenu2' data-toggle='collapse' role='button'>Affiliate Events</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl03_rep_Navigation3_ctl00_hl_Level3_Link" href="https://www.cfgi.org/symposium">CFGI Symposium</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl03_rep_Navigation2_ctl03_rep_Navigation3_ctl01_hl_Level3_Link" href="https://hrps.org/executive-events/strategic-hr-forum/pages/default.aspx">HR People + Strategy Strategic HR Forum</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    <div id="ctl00_ctl75_rep_Navigation_ctl03_ucc_FeaturedArticle_pan_Main" class="column col-md-3 col-featured visible-md visible-lg" data-personalized-offer="False">
			
    <div class="siteMenuColHeader">SHRM CONFERENCES</div>
    <a id="ctl00_ctl75_rep_Navigation_ctl03_ucc_FeaturedArticle_hl_Image" href="https://shrm.org/mlp/Pages/SymposiumDeptOne.aspx"><img id="ctl00_ctl75_rep_Navigation_ctl03_ucc_FeaturedArticle_img_Image" title="SHRM Symposium: HR Department of One" class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop,h_311,w_554,x_0,y_26/w_auto:100:228,q_auto,f_auto/v1/Marketing/HR_DOO_MegaNav_Graphic_ekwcnx" alt="SHRM Symposium: HR Department of One" /></a>
    <h6>
        <a id="ctl00_ctl75_rep_Navigation_ctl03_ucc_FeaturedArticle_hl_Title" href="https://shrm.org/mlp/Pages/SymposiumDeptOne.aspx">SHRM Symposium: HR Department of One</a>
    </h6>
    <p>Join us in San Francisco, CA, December 3 to learn how to master the balancing act as the sole HR practitioner in your organization. </p>
    <a id="ctl00_ctl75_rep_Navigation_ctl03_ucc_FeaturedArticle_hl_ReadArticle" class="shrm-CTA-Green text-uppercase" href="https://shrm.org/mlp/Pages/SymposiumDeptOne.aspx">Learn More</a>

		</div>

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
                        <li onclick="" class="dropout">
                            <a aria-controls='ctl00_ctl75_rep_Navigation_ctl04_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl04_pan_DropoutMenu' data-toggle='collapse' role='button'>Membership</a>
                            <div id="ctl00_ctl75_rep_Navigation_ctl04_pan_DropoutMenu" class="dropout-menu">
		
                                <div class="menu-columns container">
                                    <div class="nav-back-btn visible-xs-block">
                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl04_pan_DropoutMenu' data-target='#ctl00_ctl75_rep_Navigation_ctl04_pan_DropoutMenu' data-toggle='collapse' role='button'>Membership</a>
                                    </div>
                                    
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Communities</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_ul_DropoutMenu2' data-toggle='collapse' role='button'>Communities</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl00_hl_Level3_Link" href="http://community.shrm.org/">SHRM Connect</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl01_hl_Level3_Link" href="/Membership/communities/chapters/Pages/default.aspx">Chapters</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl02_hl_Level3_Link" href="http://www.hrps.org/">Executive Network</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl03_hl_Level3_Link" href="/Membership/communities/hr-young-professionals/Pages/default.aspx">HR Young Professionals</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl00_rep_Navigation3_ctl04_hl_Level3_Link" href="http://www.advocacy.shrm.org/about">Legislative Advocacy Team (A-Team)</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Student Resources</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl01_ul_DropoutMenu2' data-toggle='collapse' role='button'>Student Resources</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl01_rep_Navigation3_ctl00_hl_Level3_Link" href="/Membership/student-resources/Pages/default.aspx">Student Member Center</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                            <div class="column col-md-3 col-sm-4">
                                                <a aria-controls='ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button' class='siteMenuColHeader'>Volunteers</a>
                                                <ul id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2">
                                                    <li class="nav-back-btn visible-xs">
                                                        <a aria-controls='ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-target='#ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_ul_DropoutMenu2' data-toggle='collapse' role='button'>Volunteers</a>
                                                    </li>
                                                    
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl00_hl_Level3_Link" href="/Membership/volunteers/membership-councils/Pages/default.aspx">Membership Councils</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl01_hl_Level3_Link" href="/Membership/volunteers/special-expertise-panels/Pages/default.aspx">Special Expertise Panels</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl02_hl_Level3_Link" href="http://community.shrm.org/vlrc/">Volunteer Leader Resource Center</a></li>
                                                        
                                                            <li><a id="ctl00_ctl75_rep_Navigation_ctl04_rep_Navigation2_ctl02_rep_Navigation3_ctl03_hl_Level3_Link" href="/Membership/volunteers/Pages/Volunteer-Opportunities.aspx">Volunteer Opportunities</a></li>
                                                        
                                                </ul>
                                            </div>
                                        
                                    

                                    <div class="place-for-local-chapter column col-md-3 col-sm-12 hidden-xs"></div>
                                </div>
                            
	</div>
                        </li>
                    
            </ul>
            

<div class="mobile-quick-access visible-xs">
	<div class="mobile-utility-links text-center">    
        <div>
            <a href="/ResourcesAndTools/tools-and-samples/Pages/HR-Help.aspx">HR Help</a>
        </div>
        
        
        <div id="ctl00_ctl75_HeaderMobile_SignInPanel">
		
            <a href="/_layouts/authenticate.aspx">Sign In</a>
        
	</div>
	</div><!--/	.mobile-utility-links	-->
	<div class="mobile-ctas text-center">
        <!-- Role All	-->
        <a href="https://store.shrm.org/" target="_blank" class="btn btn-success btn-lg btn-cta">
		    	<span>SHRM Store</span>
        </a>
        <div class="btn btn-success btn-lg btn-cta">
            <a id="ctl00_ctl75_HeaderMobile_MembershipCallToActionLink" href="/about-shrm/pages/membership.aspx">Membership</a>
        </div>
        <div class="btn btn-success-dark btn-lg btn-cta">
            <a id="ctl00_ctl75_HeaderMobile_CertificationCallToActionLink" href="/certification/pages/default.aspx">GET CERTIFIED</a>
        </div>
	</div><!--/	.mobile-ctas	-->
</div><!--/ .mobile-quick-access	--> 

        </div>
    </div>
    <div id="ucc_HRJobs" style="display: none;">
        <div id="ctl00_ctl75_HrJobs_pan_JobFinder" class="shrm-widget shrm-job-finder noindex">
		
    <h3 class="shrm-Title-Small text-uppercase text-center">Job Finder</h3>
    <h5 class="text-center">Find an HR Job Near You</h5>
    <div class="finder-form">
        <div class="input-holder">
            <label for='ctl00_ctl75_HrJobs_searchTerm' class="sr-only">CITY, STATE, ZIP</label>
            <input name="ctl00$ctl75$HrJobs$searchTerm" type="text" id="ctl00_ctl75_HrJobs_searchTerm" placeholder="CITY, STATE, ZIP" class="form-control" />
        </div>
        <a href="javascript:" id="ctl00_ctl75_HrJobs_searchJobs" class="btn btn-success btn-lg btn-cta" onclick="return OpenInNewWindow(&#39;ctl00_ctl75_HrJobs_searchJobs&#39;,&#39;ctl00_ctl75_HrJobs_searchTerm&#39;);"><span class="status">Search Jobs</span></a>
    </div>
    <a id="ctl00_ctl75_HrJobs_hl_PostAJob" class="shrm-job-finder-post-a-job" href="http://hrjobs.shrm.org/jobs/products">Post a Job</a>
    <script language="javascript" type="text/javascript">
        function OpenInNewWindow(anchorTagId, textBoxTagId) {
            var searchTerm = document.getElementById(textBoxTagId);
            var searchJobs = document.getElementById(anchorTagId);
            searchJobs.setAttribute("href", "javascript:");
            searchJobs.setAttribute("target", "");

            if (searchTerm.value.length > 0) {
                searchJobs.setAttribute("target", "_blank");
                var href = "http://hrjobs.shrm.org/jobs/search/results?radius=0&location=" + searchTerm.value;
                searchJobs.setAttribute("href", href);
            }
        }
		    $(function() {
			    $uccHrJobsHtml = $('#ucc_HRJobs');
			    $uccHrJobsPlaceholder = $('nav.navbar li a').filter(function(){
				     return $(this).text() === "uccHrJobs";
			    });
			
			    if( $uccHrJobsHtml.length > 0 && $uccHrJobsPlaceholder.length > 0){
				    $('.shrm-job-finder',$uccHrJobsHtml).addClass('shrm-mini-job-finder');
				    $($uccHrJobsPlaceholder).replaceWith( $($uccHrJobsHtml).show());
			    }
		    });
    </script>

	</div>


    </div>
    <div id="ucc_LocalChapter" style="display:none;">
        

<div class="shrm-job-finder shrm-widget">
    <h3 class="shrm-Title-Small text-uppercase text-center siteMenuColHeader">LOCAL CHAPTERS</h3>
    <h5 class="text-center">Find chapters in your area</h5>
    <div class="finder-form">
  	    <div class="input-holder">
            <label for="chapterLocate" class="sr-only">CITY/STATE OR ZIP</label>
            <input type="text" id="chapterLocate"  placeholder="CITY/STATE OR ZIP"  class="form-control chapter-form-control" data-onload="chapterLocatorScript(this, new Date().getTime())" />
  	    </div>
        <a id="ctl00_ctl75_ctl00_hl_FindChapters" class="btn btn-success btn-lg btn-cta" onclick="window.location=&#39;/search/pages/LocalChapter.aspx?location=&#39; + $(this).closest(&#39;.finder-form&#39;).find(&#39;input&#39;).val()" href="javascript:">
            <span class="status">Find Chapters</span>
        </a>
    </div>
</div><!--	/.shrm-job-finder	-->

    </div>
</nav>


									    <!--	collapsable search bar for tablet/desktop affixed navigation	-->
									    <div class="row search-bar-row text-center collapse" id="search-bar-row">
									        <div class="container hidden-xs">
														<div class="search-bar-group">
															<div class="dropdown hidden-xs">
																<button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-cta">
																	<span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
																</button>
																<ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
																</ul>
															</div><!--/ .dropdown	-->
															<div class="search-bar-widget search-bar-lg-container">
																<a class="searchSubmitButton" role="button" href="javascript:"><span class="sr-only">search</span></a>
           											            <input type="text" class="form-control" id="search-bar-main" />           
																<div class="search-bar-widget-results"></div>
															</div><!-- /.search-bar-lg-container	-->
														</div>
													</div>
									    </div><!--/ .search-bar-row	-->
									    <!--/	collapsable search bar for affixed navigation	-->
									</div><!--/ .container-nav	-->
									<div class="affixed-nav-space-holder"></div>
									<div class="search-bar-row text-center hidden-xs noindex">
									    <div class="container">
												<div class="search-bar-group">
													<div class="dropdown hidden-xs">
														<button id="searchOn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-primary btn-cta">
															<span class="searchLocation">ENTIRE SITE</span> <i class="fa fa-angle-down"></i>
														</button>
														<ul class="dropdown-menu list-unstyled" aria-labelledby="searchOn">
														</ul>
													</div><!--/ .dropdown	-->
													<div class="search-bar-widget search-bar-lg-container">
													<!--window.location.href = '/search/Pages/default.aspx'-->
														<a class="searchSubmitButton" role="button" href="javascript:"><span class="sr-only">search</span></a>
           									            <input type="text" class="form-control" id="search-bar-main" />         
														<div class="search-bar-widget-results"></div>
													</div><!-- /.search-bar-lg-container	-->	
												</div> <!--	/.search-bar-group	-->     
											</div>
									</div><!--/ .search-bar-row	-->								  
									<!-- --------------------------------- SHRM NAVIGATION END - SP NAV	-->
									
								  	<!-- Start - Layout content -->
									
    <div class="container container-content">
        <div class="article-page">
            <div class="col-md-8">
                <div class="text-center">
                    
<button id="ctl00_PlaceHolderMain_ctl00_btn_Kicker" type="button" class="shrm-has-url shrm-Kicker" onclick="window.location = &#39;/about-shrm&#39;;">About SHRM</button>

                    
                <!--googleon: all-->
                    <!--googleoff: snippet-->
                    <h1 class="h2">
                        Privacy Policy
                    </h1>
                    <!--googleon: snippet-->                    
                    <p class="shrm-Dek text-center">
                        
                    </p>                   
                </div>
                <!--googleoff: snippet-->
             		

<div id="hiddenvalue-author" class="hidden author">
    &#160;
</div>



                <!--googleon: snippet-->
                
                <!--googleoff: all-->
                                
                

                


                
                <div class="article-content">
                    
                                     
                    
                        



                    
                    <!--googleon: all-->
                    <div id="ctl00_PlaceHolderMain_ctl07_label" style='display:none'>Page Content</div><div id="ctl00_PlaceHolderMain_ctl07__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_ctl07_label"><p class="shrm-Element-P"> 
   <b>
      <span class="shrm-Style-NoDropCap"></span></b>&#160;</p><div><p><strong>Privacy&#160;Statement Updated October 1, 2018.</strong></p><p>Welcome to the Society for Human Resource Management (SHRM). SHRM is the leading membership association for the human resource profession. &#160;</p><p>In Part I of this Privacy Statement we set forth SHRM's Privacy Policy as to Personal Information which we collect online through&#160;<a href="/"><span style="text-decoration&#58;underline;">www.shrm.org</span></a>&#160;or other SHRM websites which link to this Privacy Statement from their site (collectively, &quot;SHRM Websites&quot;).&#160;</p><p>In Part II of this Privacy Statement we set forth SHRM's Privacy Policy as to Personal Information which we collect other than through SHRM Websites.&#160;</p><p>&quot;Personal Information&quot;, is any information that enables us to identify you, directly or indirectly, by reference to an identifier such as your name, identification number, location data, online identifier or one or more factors specific to you. Personal Information includes &quot;sensitive Personal Information&quot; and &quot;pseudonymised Personal Information&quot; but excludes anonymous information or information that has had the identity of an individual permanently removed.</p><p><strong>Your use of the SHRM Websites and/or provision of your Personal Information or sensitive Personal Information to SHRM constitutes your consent to the use, storage, processing and transfer of that information in accordance with this Privacy Statement.</strong></p><p>For the purposes of the EU General Data Protection Regulation 2016/679 (the &quot;GDPR&quot;) the data controller is Society of Human Resource Management with an office 1800 Duke Street, Alexandria, Virginia, 22314. SHRM is an organization based in the United States. The SHRM Foundation, a subsidiary of SHRM, has also adopted this Privacy Statement.&#160; Where your data is being collected and used by the SHRM Foundation, all references to SHRM herein shall mean the SHRM Foundation.</p><p><strong>QUESTIONS</strong></p><p>If you have any questions or complaints regarding this Privacy Statement, please contact us at <a href="mailto&#58;gcoffice@shrm.org"><span style="text-decoration&#58;underline;">gcoffice@shrm.org</span></a>.&#160; If you have any questions about the SHRM Websites, please contact us at <a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a>, by phone at 703-548-3440 or 800-283-7476 or send a letter to&#58;</p><p>Society for Human Resource Management<br> 1800 Duke Street&#160;<br> Alexandria, Virginia, 22314&#160;</p><p><strong>CHANGES TO THIS PRIVACY STATEMENT</strong></p><p>We will update this Privacy Statement from time to time, so please check back periodically.</p><p>If at any point we decide to use Personal Information in a manner that is materially different from that stated at the time it was collected, we will endeavor to notify you of such changes (e.g., we will post a revised Privacy Statement with a new effective date, display the word &quot;updated&quot; next to the Privacy Policy link on each page on the SHRM Websites, or otherwise), prior to implementing them.&#160;</p><p><strong>PART I – SHRM WEBSITES PRIVACY POLICY</strong></p><p>Throughout SHRM Websites, there are forms for visitors to request information, products, and services. We use Personal Information from these forms to provide the products, promotional materials, or memberships that you request.</p><p>Forms on the SHRM Websites that request financial information do so in order to bill you for products or services ordered. Unique identifiers (specifically, your SHRM member number) are collected from website visitors to verify the user's identity for access to restricted content or features on the SHRM Websites.</p><p>This Privacy Policy discloses SHRM's privacy practices and contains detailed information about the following&#58;</p><ul style="list-style-type&#58;square;"><li>What information do we collect?</li><li>What are &quot;cookies&quot; and how does SHRM use them?</li><li>Do we share information with third parties?</li><li>How do we use the information we collect?</li><li>How can you review and modify your Personal Information?</li><li>What is the opt-out policy for SHRM Websites?</li><li>Your California privacy rights</li><li>Your European privacy rights</li><li>What types of security procedures are in place to protect against the loss, misuse or alteration of your information?</li><li>How do SHRM Websites use bulletin boards, discussion lists, and moderated chats?</li></ul><p><strong>1.&#160;&#160;&#160;&#160; What information do we collect?</strong><br><strong> </strong>SHRM Websites collect the following Personal Information about site visitors&#58;</p><p><strong>a)&#160;&#160;&#160; Site Use Information</strong></p><p>SHRM collects technical information relating to each time a visitor comes to a SHRM Website, including IP address, browser type and version, time zone settings, browser plug-in types and versions, operating system and platform type (e.g. Internet Explorer browser on a Windows platform).</p><p>We may also collect information about your visit, including pages you viewed or searched for, page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), methods used to browse away from the page, and any phone number used to call our customer service number.</p><p>Gathering this technical information helps us to learn what browsers we need to support and helps us determine what sections of SHRM Websites are most popular and how many visitors come to our site(s). You do not have to register with SHRM Websites before we can collect this information.</p><p><strong>b)&#160;&#160;&#160; Information You Give Us</strong></p><p>You may provide Personal Information to us which may include your name, home and/or work email address, postal address, phone number and/or fax number, your employer and job title, demographic information about you including birth year, certification designation, etc., and if you are making a purchase, your credit card information. You are only required to provide such information if you want to take advantage of optional products and services provided through SHRM Websites.</p><p>SHRM collects Personal Information in the following ways from different parts of the SHRM Websites&#58;</p><ul style="list-style-type&#58;square;"><li><strong>SHRM Membership Applications&#58;</strong>&#160;You are sharing Personal Information with us when you join SHRM or renew your SHRM membership through the SHRM Websites. SHRM members will have SHRM membership log-in credentials which enable members to take advantage of restricted content and features on SHRM Websites.</li><li><strong>Exchange Visitor Program Applications&#58; </strong>Host Organizations (the entity hosting an exchange visitor) and Exchange Visitor Trainees/Interns use SHRM's online application system to provide SHRM with all information required to process the applications.&#160; The information collected is subject to change over time, based on applicable laws and regulations.&#160; In addition to the terms of this Privacy Policy, any information provided pursuant to an application is governed by the terms of the <a href="/about-shrm/Pages/Exchange-Visitor-Program-Privacy-Policy.aspx">Exchange Visitor Program Privacy Policy</a>.</li><li><strong>Other Registration&#58;</strong>&#160;When registering for specific services we may ask for the same type of Personal Information. For example, if you subscribe to an e-mail newsletter, we will ask you to provide your e-mail address.</li><li><strong>Online Purchases&#58;</strong>&#160;When you make a purchase using the SHRMStore or when you subscribe to a SHRM publication through the SHRM websites we may also ask for the same type of Personal Information. The number and variety of useful services on SHRM Websites that may require the collection of Personal Information about you will continue to grow in the future.</li></ul><p><strong>2.&#160;&#160;&#160;&#160; What are &quot;cookies&quot; and how does SHRM use them?</strong></p><p>A cookie is a small text file containing a unique identification number that is transferred from a website to the hard drive of your computer. This unique number identifies your web browser to SHRM computers whenever you visit SHRM Websites.</p><p>The use of cookies is an industry standard, and cookies are currently used on most major websites. Most web browsers are initially set up to accept cookies. If you prefer, you can reset your browser to notify you when you have received a cookie. You can also set your browser to refuse to accept cookies altogether.</p><p>While SHRM does not require you to use cookies, keep in mind that certain services will not function properly if you set your browser to refuse all cookies. To help serve you better, SHRM generally uses cookies to&#58;</p><ul style="list-style-type&#58;square;"><li>Identify return visitors. Cookies let us remember your web browser so we can provide personalized member services such as My SHRM and SHRM search agents. Cookies also allow us to identify SHRM members who are returning to the site.</li><li>Display advertisements. SHRM uses an outside ad company to display SHRM-approved ads on our website and other websites. While we use cookies on other parts of our website(s), the cookies received with banner ads are collected by our ad company. These cookies allow SHRM to manage the delivery of ads.</li></ul><p>For more information about how we use cookies on the SHRM Websites, please see our&#160;<a href="/about-shrm/Pages/Cookie-Policy.aspx?_ga=2.198675503.1884290357.1526907360-1177679999.1484762105"><span style="text-decoration&#58;underline;">Cookie Policy</span></a>.</p><p><strong>3.&#160;&#160;&#160;&#160; Do we share information with third parties?</strong></p><p>Personal Information collected through SHRM Websites is generally collected and maintained solely by SHRM or its contracted vendors.</p><p>We may share Personal Information in the following ways&#58;</p><p><strong>a)&#160;&#160;&#160; Information You Give Us</strong></p><ol style="list-style-type&#58;decimal;"><li>Personal Information provided when you register for services or products. When you provide Personal Information about you on SHRM Websites to register for a service, buy a product, or take advantage of a promotion SHRM reserves the right to sell or otherwise provide to selected third parties, mailing/information lists derived from such registrations.<br> <br><strong>If you wish to opt out of such list sales/distribution at any time, you may do so by following the directions in Item 6 below. We will not share Personal Information relating to individuals based in the EU unless you have provided your opt in consent.&#160;&#160;</strong></li></ol><ol start="2" style="list-style-type&#58;decimal;"><li>Information provided when joining or renewing membership in SHRM. If you join SHRM or renew your membership through the SHRM Websites, you provide Personal Information about you on the membership application. SHRM reserves the right to sell or otherwise provide mailing/information lists concerning members to selected third-parties.<br> <br><strong>If you wish to opt out of such list sales/distributions at any time during your SHRM membership, you may do so by following the directions in Item 6 below. We will not share Personal Information relating to individuals based in the EU unless you have provided your opt in consent.</strong><br><strong> </strong><br><strong>SHRM does not sell to third-parties, e-mail addresses obtained from member applications or renewals</strong>&#160;(except that SHRM will rent a member e-mail address if that member has expressly opted in to allow such rentals).&#160;</li><li>Information provided when you apply for an Exchange Visitor Program. &#160;If you apply for sponsorship through SHRM's Exchange Visitor Program, you provide Personal Information about you and/or any dependent family members on the sponsorship application.&#160; SHRM is required by law to provide your Personal Information to the U.S. Department of State and Department of Homeland Security.&#160; <strong>SHRM does not sell Personal Information related to Exchange Visitor Program sponsorship, and will only share such Personal Information as necessary to process the application and provide sponsorship to you.</strong><br> <br>Sensitive Personal Information.&#160;&#160;<strong>SHRM does not share with third parties, sensitive Personal Information such as passcodes, social security numbers, credit card numbers, felony conviction information, or health information except as necessary to complete transactions</strong>&#160;requested by you and under strict confidentiality and security protections; nor does SHRM publish such sensitive Personal Information.</li></ol><p><strong>b)&#160;&#160;&#160; Site Use Information</strong></p><p>We disclose to third-party sponsors/advertisers aggregate statistics (i.e., impressions and click-throughs on a company's advertisement).</p><p>In addition, we may share aggregate website statistics with the media or other third parties. We do not disclose Personal Information to these sponsors/advertisers or other third parties as part of this process, only information in an aggregate form.</p><p><strong>c)&#160;&#160;&#160; SHRM Subsidiaries</strong></p><p>We also allow our subsidiaries, Strategic Human Resource Management India Pvt. Ltd. (&quot;SHRM India&quot;), SHRM China, SHRM MEA FZ (Dubai), SHRM Foundation, SHRM Corporation, and HR People + Strategy, to use the information which you may provide when you register for services or products or when you join or renew membership in SHRM, to the same extent as SHRM may use such information under this Privacy Policy, and fully subject to the same limits as SHRM is subject to on the use of such information under this Privacy Policy.</p><p><strong>4.&#160;&#160;&#160;&#160; How do we use the information we collect?</strong></p><p>We use your Personal Information according to the terms of the Privacy Policy in effect at the time of our use. We will only process your Personal Information, including sharing it with third parties, where (1) you have provided your consent which can be withdrawn at any time, (2) the processing is necessary for the performance of a contract to which you are a party (including your membership agreement with us), (3) we are required by law, (4) processing is required to protect your vital interests or those of another person, or (5) processing is necessary for the purposes of our legitimate commercial interests, provided your interests and fundamental rights do not override those interests.</p><p>We use Personal Information for the following purposes&#58;</p><p><strong>a)&#160;&#160;&#160; Information You Give Us</strong></p><p>We use the Personal Information you give us&#58;</p><ol style="list-style-type&#58;decimal;"><li>to carry out our obligations arising from your membership, Exchange Visitor sponsorship, and any other agreement entered into between you and us</li><li>to update and renew your membership as required&#160;</li><li>to provide you with the information, products and services you request from us&#160;</li><li>to arrange and deliver conferences, events and programming relevant to your job and subjects of interest</li><li>to respond to your questions and provide related membership or customer services</li><li>to improve the SHRM Websites</li><li>to send you SHRM publications, information about member benefits and special offers, and other information that SHRM believes is relevant and useful to its members, where you have not opted out of receiving such information&#160;</li><li>to send you third party information that SHRM believes is relevant and useful to you, where you have given your consent</li><li>to notify you about changes to your membership or related services or to ask you to provide feedback&#160;</li><li>to enable you to partake in a prize draw, competition or complete a survey</li><li>to administer and protect our business and websites (including troubleshooting, data analysis, testing, system maintenance, support, reporting and hosting of data)</li><li>to comply with all applicable laws or legal processes, including providing information on individual users to the appropriate governmental authorities where required by law enforcement or judicial authorities&#160;</li></ol><p>a.&#160; &#160;in matters involving a danger to personal or public safety, or to protect the rights, property or safety of SHRM, our members, customers, certificants or others SHRM may voluntarily provide information to appropriate governmental authorities</p><p>b.&#160; &#160;in some cases members may receive products or services, which are paid for by their government employer, and as a result of payment by the government employer, the record must be open for public access pursuant to applicable law, such as the Florida Public Records Act, and SHRM will share such information pursuant to those laws</p><p>c.&#160;&#160; in some cases members who are veterans may receive products or services, which are paid for by the Department of Veterans Affairs (VA), and as a result of payment by the VA, the record must be made available to the VA pursuant to VA regulation, and for processing payment, and SHRM will share such information pursuant to such regulations.</p><p>&#160;</p><p><strong>b)&#160;&#160;&#160; Site Use Information</strong></p><p>As mentioned above, SHRM uses the aggregate, anonymous data collected to let our sponsors/advertisers know the number of impressions or views and the number of &quot;click throughs&quot; on their advertisement(s).</p><p>SHRM also uses this aggregate, anonymous data to perform statistical analyses of the collective characteristics and behavior of visitors to SHRM Websites; to measure user interests regarding specific areas of SHRM Websites; and to analyze how and where best to use our resources.</p><p>Without such data, we would not know which parts of SHRM Websites are the most popular, and we would not be able to change and update the content and services appropriately.</p><p><strong>c)&#160;&#160;&#160; Information Collected in Connection with Certification and Recertification</strong></p><p>If you register for certification or recertification services, or for any other SHRM credential or certificate (e.g. SHRM California Specialty Credential), SHRM uses any Personal Information you may provide doing so in the same manner as we do when you register for other SHRM products and services.&#160; Without limitation of the foregoing, SHRM (and/or SHRM's certification or credential-related vendors) may use and disclose Personal Information, certification exam performance information, and/or assessment information related to any other SHRM credential or certificate in connection with (the following is a non-exclusive list of examples)&#58;</p><ol style="list-style-type&#58;decimal;"><li>Listing all individuals who have achieved a SHRM certification in the SHRM Online Certified Directory</li><li>Sending notifications/reminders on the status of certification or other credential or certificate, information on professional development and recertification opportunities, and other news and communications of interest to the certified community or those holding any other SHRM credential or certificate</li><li>The scheduling and administration of exams</li><li>The preparation, review, updating, validation, accreditation and/or administration of the exams and exam preparation materials</li><li>Publishing information regarding a candidate/certificant/credential-holder/certificate-holder against whom disciplinary action has been taken and the reason for that action.</li></ol><p>&#160;</p><p>Examples of how SHRM will not share a candidate's/certificant's/credential-holder/certificate-holder's Personal Information, certification exam performance information, or assessment information related to any other SHRM credential or certificate are as follows&#58;</p><ol style="list-style-type&#58;decimal;"><li>SHRM will not share the names of candidates who do not pass the exam with the general public.&#160;</li><li>SHRM will not share information about an examinee's performance on individual exam items with any person or entity.</li><li>SHRM will not disclose any Personal Information related to requests for reasonable accommodation under the ADA, or under similar Non-US requirements, other than as reasonably necessary to review and/or provide that accommodation.</li></ol><p>However, SHRM may disclose the above information as reasonably necessary in relation to the administration of the exam and exam program (e.g. to communicate ADA accommodations to our exam testing centers as necessary); to comply with the law, regulation, pursuant to court order or other legal process; in connection with the preparation, review, updating, validation, accreditation and/or administration of the exam or exam preparation materials; or to protect the rights, property or safety of SHRM, our members, certificants or others.</p><p><strong>d)&#160;&#160;&#160; Exceptions&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</strong></p><p>On occasion SHRM collects Personal Information through SHRM Websites with the intent to afford a greater degree of privacy for such information than is otherwise set forth in this privacy policy.&#160;</p><p>In those relatively rare situations where SHRM does so it will clearly disclose to you at the time it collects such information, what degree of privacy will be afforded to the Personal Information collected at such time; and SHRM will follow a process to assure that the specifically disclosed degree of privacy is in fact afforded to such information.&#160;</p><p>For example, SHRM may collect survey information through a survey where SHRM's use of any Personal Information from that survey is more limited than the general SHRM Privacy Statement would otherwise allow; in such a case, SHRM will disclose the stricter privacy policy governing that survey information at the time such Personal Information is collected from the survey respondent.</p><p><strong>5.&#160;&#160;&#160;&#160; How can you review and modify your Personal Information?</strong></p><p>You have the following options for modifying or causing to be deleted Personal Information or demographic information previously provided by you to SHRM.</p><ol style="list-style-type&#58;decimal;"><li>E-mail&#58;&#160;<a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a></li><li>Or if you are a SHRM member you may also visit&#58;&#160;<a href="/my"><span style="text-decoration&#58;underline;">Member Dashboard</span></a></li><li>Send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Customer Care Department.</li><li>Call&#58; 703-548-3440 or +1-800-283-7476.&#160;</li></ol><p><strong>6.&#160;&#160;&#160;&#160; What is the opt-out policy for SHRM Websites?</strong></p><p>SHRM provides members and customers with the opportunity to opt-out of receiving communications from us and our partners. If you no longer wish to receive specific communications or services, you have the following options&#58;</p><ol style="list-style-type&#58;decimal;"><li>You can send an e-mail to&#58;&#160;<a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a></li><li>You can send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Customer Care Department.</li><li>You can call the following telephone numbers&#58; +1-800-283-7476 or 703-548-3440.</li></ol><p>Instructions for opting out of any SHRM e-mail newsletter you receive are included with each e-mail.</p><p><strong>7.&#160;&#160;&#160;&#160; Your California Privacy Rights</strong></p><p><strong>For California Residents Only.</strong>&#160; SHRM may disclose your Personal Information to our subsidiaries or other third parties who may use that information to market directly to you.&#160; As a California resident, you have the right to opt-out of having your Personal Information licensed to such third parties.&#160; We will not share your information after we have received your notification that you are opting out.&#160; If you wish to opt-out you have the following options&#58;</p><ol style="list-style-type&#58;decimal;"><li>You can send an e-mail to&#58;&#160;<a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a></li><li>You can send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Customer Care Department.</li><li>You can call the following telephone numbers&#58; +1-800-283-7476 or 703-548-3440.&#160;</li></ol><p><strong>8.&#160;&#160;&#160;&#160; Your European Privacy Rights</strong></p><p><strong>For European Residents Only.</strong>&#160;If you are based in the EU, in certain circumstances, you have rights under the GDPR in relation to your Personal Information.</p><ul style="list-style-type&#58;square;"><li><strong>Request access to your Personal Information.</strong>&#160; You may have the right to request access to any Personal Information we hold about you as well as related information, including the purposes for processing the Personal Information, the recipients or categories of recipients with whom the Personal Information has been shared, where possible, the period for which the Personal Information will be stored, the source of the Personal Information, and the existence of any automated decision making.</li><li><strong>Request correction of your Personal Information.</strong>&#160;You may have the right to obtain without undue delay the rectification of any inaccurate Personal Information we hold about you.</li><li><strong>Request erasure of your Personal Information.</strong>&#160;You may have the right to request that Personal Information held about you is deleted.</li><li><strong>Request restriction of processing your Personal Information.&#160;</strong>You may have the right to prevent or restrict processing of your Personal Information.</li><li><strong>Request transfer of your Personal Information.&#160;</strong>You may have the right to request transfer of Personal Information directly to a third party where this is technically feasible.</li></ul><p>Where you believe that we have not complied with our obligations under this Privacy Policy or European data protection laws, you have the right to make a complaint to an EU Data Protection Authority, such as the UK Information Commissioner's Office.</p><p>If you wish to exercise your European data subject rights, you have the following options&#58;</p><ol style="list-style-type&#58;decimal;"><li>You can send an e-mail to&#58;&#160;<a href="mailto&#58;shrm@shrm.org"><span style="text-decoration&#58;underline;">shrm@shrm.org</span></a></li><li>You can send mail to the following postal address&#58; SHRM, 1800 Duke Street, Alexandria, Virginia, 22314, ATTN&#58; Customer Care Department.</li><li>You can call the following telephone numbers&#58; +1-800-283-7476 or 703-548-3440.&#160;</li></ol><p><strong>9.&#160;&#160;&#160;&#160; What kinds of security procedures are in place to protect against the loss, misuse or alteration of your information?</strong></p><p>SHRM Websites have security measures equal to or better than those reasonably expected in the industry, such as firewalls, in place to protect against the loss, misuse and alteration of your Personal Information under our control. While we cannot guarantee that loss, misuse or alteration to data will not occur, we take reasonable precautions to prevent such unfortunate occurrences. Certain particularly sensitive information, such as your credit card number, collected for a commercial transaction is encrypted prior to transmission.</p><p>You are ultimately responsible for the security of your SHRM login credentials. You may not share your SHRM login credentials with colleagues or friends so they can access content or features that are restricted to SHRM members only. You should log out of your browser at the end of each computer session to ensure that others cannot access your Personal Information and correspondence, especially if you share a computer with someone else or are using a computer in a public place like a library or Internet cafe.</p><p><strong>10.&#160; How do SHRM Websites use bulletin boards, discussion lists, and moderated chats?</strong></p><p>SHRM Websites make bulletin boards, discussion lists, and moderated chats available to its members. Any information that is disclosed in these areas becomes public information, and you should exercise caution when deciding to disclose your Personal Information. Although users may post messages that will appear on the message boards anonymously, SHRM does retain a record of who posts all notes.&#160;</p><p><strong>PART II – SHRM PRIVACY POLICY FOR INFORMATION COLLECTED OTHER THAN THROUGH SHRM WEBSITES</strong></p><p>If you submit Personal Information to SHRM through any channel other than SHRM Websites, the same privacy rules set forth at Part I above for SHRM Websites will be applied to such Personal Information you submit through channels other than SHRM Websites (including without limitation your opt-out rights at Part I, Section 6 above), except as follows&#58;</p><ol style="list-style-type&#58;decimal;"><li>The &quot;cookies&quot; and other tracking devices used by SHRM Websites and described in Part I above do not apply to Personal Information gathered through channels other than SHRM Websites.</li><li>Hardcopy Personal Information provided to SHRM which is not converted to electronic media and hosted by SHRM will be subject to different security procedures than will stored electronic Personal Information. SHRM has security measures equal to or better than those reasonably expected in the industry, in place to protect against the loss, misuse and alteration of your hardcopy Personal Information under our control.</li><li>When SHRM collects Personal Information through channels other than SHRM Websites it may in some instances apply a different privacy policy to such information; but where it does so it shall conspicuously disclose to you at the time of collection what privacy policy will apply to such information.&#160; For example, SHRM may collect survey information through a survey where SHRM's use of any Personal Information from that survey is more limited than the general SHRM Privacy Statement would otherwise allow; in such a case SHRM will disclose the stricter privacy policy governing that survey information at the time such Personal Information is collected from the survey respondent.</li></ol><p><strong>Privacy Policy Effective May, 2002. Updated February 24, 2011, August 7, 2014, October 1, 2015, December 14, 2015, May 25, 2018, and October 1, 2018.</strong></p><p>&#160;</p></div></div>
                    <!--googleoff: all-->
                    

<div class="marketing-promo-blurb noindex">
    
</div>

                </div><!--	/.article-content	-->
                
                
                    <div id="article-paragraph-ad" class="article-paragraph-ad shrm-widget ad-holder visible-sm">
                        <input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Fluid" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Fluid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size1W" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size1H" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size1H" value="250" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size2W" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size2W" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Size2H" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Size2H" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl00$hf_Section" id="ctl00_PlaceHolderMain_ctl09_ctl00_hf_Section" value="StandardArticleFlexUnitMobile" />
<div id="ctl00_PlaceHolderMain_ctl09_ctl00_pan_AdWordsWrapper">
		
    <div id="ctl00_PlaceHolderMain_ctl09_ctl00_pan_AdWords">
			
    
		</div>

	</div>

                    </div>
                    <div id="article-paragraph-ad-2" class="article-paragraph-ad shrm-widget ad-holder visible-xs">
                        <input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Fluid" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Fluid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Size1W" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Size1H" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Size1H" value="600" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Size2W" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Size2W" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Size2H" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Size2H" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl09$ctl01$hf_Section" id="ctl00_PlaceHolderMain_ctl09_ctl01_hf_Section" value="StandardArticleFlexUnit" />
<div id="ctl00_PlaceHolderMain_ctl09_ctl01_pan_AdWordsWrapper" class="scroller-ad noindex">
		
    <div id="ctl00_PlaceHolderMain_ctl09_ctl01_pan_AdWords">
			
    
		</div>

	</div>

                    </div>
                    <script>
                        if ($("[id$='_ControlWrapper_RichHtmlField']").find("> p").length > 3) {
                            $("*[id$='_ControlWrapper_RichHtmlField'] > p:nth-of-type(3)").after( $("#article-paragraph-ad") );
                        }
                        if ($("[id$='_ControlWrapper_RichHtmlField']").find("> p").length > 3) {
                            $("*[id$='_ControlWrapper_RichHtmlField'] > p:nth-of-type(3)").after($("#article-paragraph-ad-2"));
                        }
                    </script>
                
                <div id="ctl00_PlaceHolderMain_ctl10_pan_Main" class="shrm-tags shrm-tags-empty article-tags">
		
    

	</div>

                


                

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script type="text/javascript" src="/v1628379436/core/assets/js/socialbar.js"></script>
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_SiteUrl" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_SiteUrl" value="https://www.shrm.org" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_ArticleGuid" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_ArticleGuid" value="05f756a6-7090-4d03-976f-a430f545b58e" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_ArticleTypeId" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_ArticleTypeId" value="1" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_ArticleLink" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_ArticleLink" value="05f756a6-7090-4d03-976f-a430f545b58e" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_Headline" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_Headline" value="Privacy Policy" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_Subheading" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_Subheading" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_MembershipUrl" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_MembershipUrl" value="https://membership.shrm.org/" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_SocialBarScript_EmailAFriend_Hash" id="ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_EmailAFriend_Hash" value="7CDCD5FE1280B4E2A9761E58F1C5AD7E" />
<script type="text/javascript">
    bookmarkExists();
    likeExists();
    function EmailAFriend() {
        //document.getElementById('emailAFriendModalContent').classList.add('in');
        var _IsValid = IsVa11d4m();
        if (_IsValid) {
            var _FromName = $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').val();
            var _FromEmail = $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').val();
            var _ToEmail = $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').val();
            var _Subject = $('#ctl00_PlaceHolderMain_ctl12_emailFriendsSubject').val();
            var _Message = $('#ctl00_PlaceHolderMain_ctl12_hf_EmailFriendMessageValue').val();
            var _Hash = $('#ctl00_PlaceHolderMain_ctl12_hf_SocialBarScript_EmailAFriend_Hash').val();
            $.ajax({
                type: "POST",
                async: false,
                url: "/_layouts/15/SHRM.Core/ajax/api.aspx/SendAFriend",
                data: JSON.stringify({ '_FromName': _FromName, '_FromEmail': _FromEmail, '_ToEmail': _ToEmail, '_Subject': _Subject, '_Message': _Message, '_Hash': _Hash }),
                //data: "{_FromName: '" + _FromName + "', _FromEmail: '" + _FromEmail + "', _ToEmail: '" + _ToEmail + "', _Subject: '" + _Subject + "', _Message: '" + _Message + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $('#emailAFriendModalBody').hide('slow');
                    $('#emailAFriendModalMessage').show('slow');
                    $('#emailAFriendModalMessageLabel').html('<span>Your message has been sent successfully.<br /><br />SHRM Members please note: Links you share to member- protected content will not be viewable by non- members.</span>');
                    $('#emailFriendsSendBtn').hide('slow');
                    $('#emailFriendsCancelBtn').hide('slow');
                    $('#emailFriendsCloseBtn').show('slow');
                    setTimeout(function () {
                        $('#emailAFriendModal').modal('hide');
                        ResetEMailAFriend();
                    }, 5000);
                },
                error: function (msg) {
                    $('#emailAFriendModalBody').hide('slow');
                    $('#emailAFriendModalMessage').show('slow');
                    $('#emailAFriendModalMessageLabel').html('<span>There was a problem sending your message.<br/>Please try again.</span>');
                }
            });
        }
    }
    function ResetEMailAFriend() {
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').val("");
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').val("");
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').val("");
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsSubject').val($('#ctl00_PlaceHolderMain_ctl12_hf_EmailFriendSubjectValue').val());
        $('#emailAFriendModalBody').show('slow');
        $('#emailAFriendModalMessage').hide('slow');
        $('#emailAFriendModalMessageLabel').html('');
        $('#emailFriendsSendBtn').show('slow');
        $('#emailFriendsCancelBtn').show('slow');
        $('#emailFriendsCloseBtn').hide('slow');
    }
    function CombineSubject() {
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsSubject').val($('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').val() + $('#ctl00_PlaceHolderMain_ctl12_hf_EmailFriendSubjectValue').val());
        IsVa11d4m();
    }
    function isEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
    function IsVa11d4m() {
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').parent().removeClass('has-error');
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').parent().removeClass('has-error');
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').parent().removeClass('has-error');
        var _IsValid = true;
        if ($('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').val() == "") {
            _IsValid = false;
            $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFrom').parent().addClass('has-error');
        }
        if (!isEmail($('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').val())) {
            _IsValid = false;
            $('#ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail').parent().addClass('has-error');
        }
        if (!isEmail($('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').val())) {
            _IsValid = false;
            $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').parent().addClass('has-error');
        }
        return _IsValid;
    }
    function Va11d4m() {
        $('#emailFriendsSendBtn').removeClass('disabled');
    }
    function EmailAFriendFromReset() {
        $('#ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail').val('');
        $('#emailAFriendModalBody').show('slow');
        $('#emailAFriendModalMessage').hide('slow');
        $('#emailAFriendModalMessageLabel').html('<span></span>');
        $('#emailFriendsSendBtn').show('slow');
        $('#emailFriendsCancelBtn').show('slow');
        $('#emailFriendsCloseBtn').hide('slow');
    }
</script>
<div id="modal_bookmark_save" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>You have successfully saved this page as a bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="window.location = window.location.protocol + '//' + window.location.hostname + '/my/bookmarks/';">My Bookmarks</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_delete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please confirm that you want to proceed with deleting bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="deleteBookmark();">Delete</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="deleteBookmarkCanceled();">Cancel</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_deleted" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>You have successfully removed bookmark.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="window.location = window.location.protocol + '//' + window.location.hostname + '/my/bookmarks/';">My Bookmarks</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_canceldelete" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Delete canceled</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_loginbeforesave" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please log in as a SHRM member before saving bookmarks.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>                
                <button type="button" class="btn btn-primary" onclick="ProceedToLogin();">Proceed</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_expiredsession" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please sign in as a SHRM member before saving bookmarks.</p>
            </div>
            <div class="modal-footer">
                <a href="javascript:" class="btn btn-empty" data-dismiss="modal">Cancel</a>
                <button type="button" class="btn btn-primary" onclick="ProceedToLogin();">Sign In</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_membershipbeforesave" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p>Please purchase a SHRM membership before saving bookmarks.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="ProceedToMembership();">Join</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_error" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p id="modal_bookmark_error_msg">An error has occurred</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div id="modal_bookmark_limit" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <p id="modal_bookmark_limit_msg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
                <button type="button" class="btn btn-primary" onclick="window.location = window.location.protocol + '//' + window.location.hostname + '/my/bookmarks/';">My Bookmarks</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="emailAFriendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content shrm-loading" id="emailAFriendModalContent">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Email A Friend</h4>
            </div>-->
            <div class="modal-body padd-v-20" id="emailAFriendModalBody">
                <div class="form-group" style="min-height: 65px;">
                    <label for='ctl00_PlaceHolderMain_ctl12_emailFriendsFrom'>From</label>
                    <input name="ctl00$PlaceHolderMain$ctl12$emailFriendsFrom" type="text" id="ctl00_PlaceHolderMain_ctl12_emailFriendsFrom" class="form-control marg-v-0" onkeyup="CombineSubject();" />
                </div>
                <div class="form-group" style="min-height: 65px;">
                    <label for='ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail'>From Email</label>
                    <input name="ctl00$PlaceHolderMain$ctl12$emailFriendsFromEmail" type="text" id="ctl00_PlaceHolderMain_ctl12_emailFriendsFromEmail" class="form-control marg-v-0" onkeyup="IsVa11d4m();" />
                </div>
                <div class="form-group" style="min-height: 65px;">
					<label for='ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail'>To Email</label>
                    <input name="ctl00$PlaceHolderMain$ctl12$emailFriendsToEmail" type="text" id="ctl00_PlaceHolderMain_ctl12_emailFriendsToEmail" class="form-control marg-v-0" placeholder="username@example.com" onkeyup="IsVa11d4m();" />
					<p class="small text-danger" id="emailFriendsToEmailMsg" style="display: none;"></p>
				</div>
                <div class="form-group" style="min-height: 65px;">
                    <label for='ctl00_PlaceHolderMain_ctl12_emailFriendsSubject'>Subject</label>
                    <input name="ctl00$PlaceHolderMain$ctl12$emailFriendsSubject" type="text" id="ctl00_PlaceHolderMain_ctl12_emailFriendsSubject" class="form-control marg-v-0 disabled" value=" sent you this SHRM article: Privacy Policy" />
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_EmailFriendSubjectValue" id="ctl00_PlaceHolderMain_ctl12_hf_EmailFriendSubjectValue" value=" sent you this SHRM article: Privacy Policy" />
                </div>
                <div class="form-group" style="min-height: 65px;">
                    <label for='ctl00_PlaceHolderMain_ctl12_emailFriendsSubject'>Message</label>
                    <span class="form-control marg-v-0">Hi,<br /><br />

I thought you'd like this article I found on the SHRM website: <br />
<a href='https://www.shrm.org/about-shrm/Pages/Privacy-Policy.aspx'>Privacy Policy</a>  </span>
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl12$hf_EmailFriendMessageValue" id="ctl00_PlaceHolderMain_ctl12_hf_EmailFriendMessageValue" value="Hi,&amp;lt;br /&amp;gt;&amp;lt;br /&amp;gt;

I thought you&amp;#39;d like this article I found on the SHRM website: &amp;lt;br /&amp;gt;
&amp;lt;a href=&amp;#39;https://www.shrm.org/about-shrm/Pages/Privacy-Policy.aspx&amp;#39;&amp;gt;Privacy Policy&amp;lt;/a&amp;gt;  " />
                </div>
                <div id="ctl00_PlaceHolderMain_ctl12_reCaptcha" class="g-recaptcha" data-callback="Va11d4m" data-sitekey="6LcY0T0UAAAAAMVoOAWR1w4uWXAcd80eeIO8o29R"></div>
            </div>
            <div class="modal-body padd-v-20" id="emailAFriendModalMessage" style="display: none;">
                <div id="emailAFriendModalMessageLabel" class="form-group" style="min-height: 65px;">
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:" class="btn btn-primary disabled" onclick="EmailAFriend();" id="emailFriendsSendBtn">Send</a>
                <a href="javascript:" class="btn btn-default" data-dismiss="modal" id="emailFriendsCancelBtn" onclick="EmailAFriendFromReset();">Cancel</a>
                <a href="javascript:" class="btn btn-default" data-dismiss="modal" id="emailFriendsCloseBtn" style="display: none;" onclick="EmailAFriendFromReset();">Close</a>
            </div>
        </div>
    </div>
</div>

                <div id="ctl00_PlaceHolderMain_ctl13_pan_ArticleRecommendedForYou" class="row-shrm-recommended-inline clearfix noindex">
		
    <a id="ctl00_PlaceHolderMain_ctl13_hl_FakeLink" class="fake-link" href="https://www.shrm.org/resourcesandtools/hr-topics/technology/pages/hr-recognition-peer-to-peer-employees.aspx">&nbsp;<span class="sr-only">Fake link</span></a>
    <div id="ctl00_PlaceHolderMain_ctl13_pan_ImageHolder" class="image-holder pull-left">
			
  	    <img id="ctl00_PlaceHolderMain_ctl13_img_Image" title="Put Recognition in Your Employees’ Hands" class="img-responsive" sizes="100vw" src="https://cdn.shrm.org/image/upload/c_crop,h_408,w_724,x_0,y_1/w_auto:100:150,q_auto,f_auto/v1/Technology/iStock-912235112_v5hzbu" alt="Put Recognition in Your Employees’ Hands" />
    
		</div>
    <p class="shrm-Label text-uppercase">Recommended for you</p>
    <h5>Put Recognition in Your Employees’ Hands</h5>  

	</div>
<script>
    //console.log($("[id$='_ControlWrapper_RichHtmlField']").find("p").length);
    //if ($("[id$='_ControlWrapper_RichHtmlField']").find("p").length > 5) {
    //    var _Count = $("[id$='_ControlWrapper_RichHtmlField']").find("p").length - 2;
    //    $("[id$='pan_ArticleRecommendedForYou']").appendTo($("[id$='_ControlWrapper_RichHtmlField']").find("p:nth-child(" + _Count + ")"));
    //}
</script>

                


<a name="TaxonomyTreeViewAnchor" id="TaxonomyTreeViewAnchor"></a>
<script type="text/javascript">
    function SelectTerm(_TermId) {
        FindChildNodes(_TermId, false);
        FindParentNodes(_TermId, $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").prev().prop('checked'));
        FindSameTitleTerms(_TermId, $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").prev().prop('checked'));
        FindRootPath();
        SetSelectedTerms();
    }
    function SetSelectedTerms() {
        var _HiddenValues = "";
        var _VisibleValues = "";
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").each(function () {
            _HiddenValues += $(this).next().next().text() + "|" + $(this).next().val() + ";";
            _VisibleValues += "<span class='valid-text' title=''>" + $(this).next().next().text() + "</span>; ";
        });
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val(_HiddenValues);
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children().eq(1).children().eq(0).html(_VisibleValues);
        $("[id$='PreventEmptyTaxonomy']").val(_HiddenValues);
    }
    function LoadSelectedTerms() {
        try { $("#hf_TaxonomyReset_NAID").val($(".article-taxonomy-treeview-node input[value='N/A']").prev().prev().val()); }
        catch (err) { }
        var _Terms = $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val().split(';');
        if ($("#hf_TaxonomyReset_NAID").val() != '') {
            $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").parent().parent().parent().parent().parent().hide();
            var _IsNA = false;
            $.each(_Terms, function (index, value) {
                if ($("#hf_TaxonomyReset_NAID").val() == value.split('|')[1]) { _IsNA = true; }
                else { $(".article-taxonomy-treeview-node input[value='" + value.split('|')[1] + "']").prev().prop('checked', true); }
            });
            if (_IsNA) {
                $("#cb_TaxonomyReset_NA").prop('checked', true);
                $(".article-taxonomy-treeview-node input[type=checkbox]:checked").prop('checked', false);
                $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").prev().prop('checked', true);
                SetSelectedTerms();
                $("[id$='pan_TaxonomyTreeView']").hide();
            }
        }
        else {
            $("#cb_TaxonomyReset_NA").parent().hide();
            $.each(_Terms, function (index, value) {
                $(".article-taxonomy-treeview-node input[value='" + value.split('|')[1] + "']").prev().prop('checked', true);
            });
        }
        SetSelectedTerms();
    }
    function FindParentNodes(_TermId, _Checked) {
        var _ParentCheckbox = $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").parent().parent().parent().parent().parent().parent().prev().find('.article-taxonomy-treeview-node').children().eq(0).children().eq(0);
        if ($(_ParentCheckbox).is(':checkbox')) {
            $(_ParentCheckbox).prop('checked', _Checked);
            FindParentNodes($(_ParentCheckbox).next().val(), _Checked);
        }
    }
    function FindChildNodes(_TermId, _Checked) {
        $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").parent().parent().parent().parent().parent().next().find('.article-taxonomy-treeview-node input[type=checkbox]').each(function () {
            $(this).prop('checked', _Checked);
        });
    }
    function FindSameTitleTerms(_TermId, _Checked) {
        var _Title = $(".article-taxonomy-treeview-node input[value='" + _TermId + "']").next().next().val();
        $(".article-taxonomy-treeview-node input[value='" + _Title + "']").each(function () {
            $(this).prev().prev().prev().prop('checked', _Checked);
            FindParentNodes($(this).prev().prev().val(), _Checked);
        });
    }
    function FindRootPath() {
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").each(function () {
            FindParentNodes($(this).next().val(), true);
        });
    }
    function RemoveSelectedTerms() {
        $(".article-taxonomy-treeview-node input[type=checkbox]:checked").prop('checked', false);
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(0).val('');
        $("[id$='pan_TaxonomyField']").children().eq(0).children().eq(1).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children().eq(1).children().eq(0).html('');
        $("[id$='PreventEmptyTaxonomy']").val('');
        console.log('#cb_TaxonomyReset_NA is: ' + $("#cb_TaxonomyReset_NA").prop('checked'));
        if ($("#cb_TaxonomyReset_NA").prop('checked') == true) {
            console.log('#cb_TaxonomyReset_NA is: TRUE');
            $("[id$='pan_TaxonomyTreeView']").hide();
            $(".article-taxonomy-treeview-node input[value='" + $("#hf_TaxonomyReset_NAID").val() + "']").prev().prop('checked', true);
        }
        else {
            console.log('#cb_TaxonomyReset_NA is: FALSE');
            $("[id$='pan_TaxonomyTreeView']").show();
        }
        SetSelectedTerms();
    }
    function FixAspTreeView() {
        $(".article-taxonomy-treeview td[class!='article-taxonomy-treeview-node'] a").each(function () {
            $(this).attr('onclick', $(this).attr('href')).removeAttr('href');
        });
    }

</script>









            </div>
            <div class="col-md-4">
                <div class="shrm-widget ad-holder hidden-xs hidden-sm">
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Fluid" id="ctl00_PlaceHolderMain_ctl15_hf_Fluid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Size1W" id="ctl00_PlaceHolderMain_ctl15_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Size1H" id="ctl00_PlaceHolderMain_ctl15_hf_Size1H" value="250" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Size2W" id="ctl00_PlaceHolderMain_ctl15_hf_Size2W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Size2H" id="ctl00_PlaceHolderMain_ctl15_hf_Size2H" value="600" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl15$hf_Section" id="ctl00_PlaceHolderMain_ctl15_hf_Section" value="StandardArticleFlexUnit" />
<div id="ctl00_PlaceHolderMain_ctl15_pan_AdWordsWrapper">
		
    <div id="ctl00_PlaceHolderMain_ctl15_pan_AdWords">
			
    
		</div>

	</div>

                </div>
                <div id="ctl00_PlaceHolderMain_HrJobs_pan_JobFinder" class="shrm-widget shrm-job-finder noindex">
		
    <h3 class="shrm-Title-Small text-uppercase text-center">Job Finder</h3>
    <h5 class="text-center">Find an HR Job Near You</h5>
    <div class="finder-form">
        <div class="input-holder">
            <label for='ctl00_PlaceHolderMain_HrJobs_searchTerm' class="sr-only">CITY, STATE, ZIP</label>
            <input name="ctl00$PlaceHolderMain$HrJobs$searchTerm" type="text" id="ctl00_PlaceHolderMain_HrJobs_searchTerm" placeholder="CITY, STATE, ZIP" class="form-control" />
        </div>
        <a href="javascript:" id="ctl00_PlaceHolderMain_HrJobs_searchJobs" class="btn btn-success btn-lg btn-cta" onclick="return OpenInNewWindow(&#39;ctl00_PlaceHolderMain_HrJobs_searchJobs&#39;,&#39;ctl00_PlaceHolderMain_HrJobs_searchTerm&#39;);"><span class="status">Search Jobs</span></a>
    </div>
    <a id="ctl00_PlaceHolderMain_HrJobs_hl_PostAJob" class="shrm-job-finder-post-a-job" href="http://hrjobs.shrm.org/jobs/products">Post a Job</a>
    <script language="javascript" type="text/javascript">
        function OpenInNewWindow(anchorTagId, textBoxTagId) {
            var searchTerm = document.getElementById(textBoxTagId);
            var searchJobs = document.getElementById(anchorTagId);
            searchJobs.setAttribute("href", "javascript:");
            searchJobs.setAttribute("target", "");

            if (searchTerm.value.length > 0) {
                searchJobs.setAttribute("target", "_blank");
                var href = "http://hrjobs.shrm.org/jobs/search/results?radius=0&location=" + searchTerm.value;
                searchJobs.setAttribute("href", href);
            }
        }
		    $(function() {
			    $uccHrJobsHtml = $('#ucc_HRJobs');
			    $uccHrJobsPlaceholder = $('nav.navbar li a').filter(function(){
				     return $(this).text() === "uccHrJobs";
			    });
			
			    if( $uccHrJobsHtml.length > 0 && $uccHrJobsPlaceholder.length > 0){
				    $('.shrm-job-finder',$uccHrJobsHtml).addClass('shrm-mini-job-finder');
				    $($uccHrJobsPlaceholder).replaceWith( $($uccHrJobsHtml).show());
			    }
		    });
    </script>

	</div>


                <div class="shrm-widget-row">
                    <div>

<div class="shrm-most-popular-widget shrm-widget non-tiles col-sm-6 col-md-12 noindex ">
	<h2 class="shrm-Title-Small text-center text-uppercase">Most popular</h2>
    <div class="list-group shrm-list-stream">
        
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="https://www.shrm.org/ResourcesAndTools/Pages/workplace-harassment.aspx"><img title="Workplace Harassment Prevention and Response Resources" class="img-responsive" sizes="100vw" src="https://cdn.shrm.org/image/upload/c_crop%2ch_253%2cw_450%2cx_0%2cy_0/c_fit%2cf_auto%2cq_auto%2cw_767/v1/Tools%20and%20Samples/18_1489_HupPage_Rullup_WorkplaceHarassment_c5hcuh?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjAsIngyIjo0NTAsInkyIjoyNTMsInciOjQ1MCwiaCI6MjUzfX0%3d" alt="Workplace Harassment Prevention and Response Resources" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"><span class="sr-only">anchor</span></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/ResourcesAndTools/Pages/workplace-harassment.aspx">Workplace Harassment Prevention and Response Resources</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="https://www.shrm.org/ResourcesAndTools/hr-topics/organizational-and-employee-development/Pages/Confidence-Is-the-Missing-Ingredient-for-Female-Leaders.aspx"><img title="Confidence Is the &#39;Missing Ingredient&#39; for Female Leaders" class="img-responsive" sizes="100vw" src="https://cdn.shrm.org/image/upload/c_crop%2ch_543%2cw_965%2cx_12%2cy_0/c_fit%2cf_auto%2cq_auto%2cw_767/v1/Organizational%20and%20Employee%20Development/Claire_Shipman_izrcgh?databtoa=eyIxNng5Ijp7IngiOjEyLCJ5IjowLCJ4MiI6OTc3LCJ5MiI6NTQzLCJ3Ijo5NjUsImgiOjU0M319" alt="Confidence Is the &#39;Missing Ingredient&#39; for Female Leaders" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"><span class="sr-only">anchor</span></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/ResourcesAndTools/hr-topics/organizational-and-employee-development/Pages/Confidence-Is-the-Missing-Ingredient-for-Female-Leaders.aspx">Confidence Is the 'Missing Ingredient' for Female Leaders</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
                <div class="list-group-item list-group-flex-item">
                    <a class="list-group-item-featured-image" href="https://www.shrm.org/ResourcesAndTools/legal-and-compliance/employment-law/Pages/Amazon-Workers-Will-Soon-Earn-$15-an-Hour.aspx"><img title="Amazon Workers Will Soon Earn $15 an Hour" class="img-responsive" sizes="100vw" src="https://cdn.shrm.org/image/upload/c_crop%2ch_2267%2cw_4032%2cx_0%2cy_0/c_fit%2cf_auto%2cq_auto%2cw_767/v1/Legal%20and%20Compliance/amazon_mo2iwa?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjAsIngyIjo0MDMyLCJ5MiI6MjI2NywidyI6NDAzMiwiaCI6MjI2N319" alt="Amazon Workers Will Soon Earn $15 an Hour" /></a>
                    <div class="list-group-item-wrapper">
					    <a class="shrm-Anchor text-uppercase" href="javascript:"><span class="sr-only">anchor</span></a>
                        <a class="list-group-item-title" href="https://www.shrm.org/ResourcesAndTools/legal-and-compliance/employment-law/Pages/Amazon-Workers-Will-Soon-Earn-$15-an-Hour.aspx">Amazon Workers Will Soon Earn $15 an Hour</a>
                        <div class="shrm-tags shrm-Fine"> 
                            
                        </div>
			        </div>
                </div>
            
    </div>
    <div class="bottom-button" style="display: none;"> 
        <a class="btn btn-block btn-blank shrm-CTA-Green" onClick="$(this).closest('.shrm-widget').find('.collapse:not(.in)').slice(0,2).collapse()">MORE</a>
    </div>
    <input type="hidden" name="ctl00$PlaceHolderMain$ctl16$hf_Message" id="ctl00_PlaceHolderMain_ctl16_hf_Message" />
</div>
</div>
                    

                    <div><div id="ctl00_PlaceHolderMain_ctl18_pan_Main" class="row-promo-panel shrm-widget col-sm-12 col-md-12 noindex " data-personalized-offer="">
		
    <div class="promo-panel-wrapper outlined text-center">        
  	    <h3 class="shrm-Title-Small text-uppercase text-center">Apply by October 19</h3>
        <a id="ctl00_PlaceHolderMain_ctl18_hl_ImageWrapper" class="promo-image" href="https://www.shrm.org/certification/apply/pages/default.aspx"><img id="ctl00_PlaceHolderMain_ctl18_img_Image" title="Get recognized as an HR expert. Earn your SHRM-CP and SHRM-SCP certification, and set yourself apart. " class="img-responsive" src="https://cdn.shrm.org/image/upload/c_crop%2ch_1192%2cw_2121%2cx_0%2cy_174/c_fit%2cw_767/v1/Home%20Page%20Carousel/Calendar_-_Coffee-_Deadline_hnc5a8?databtoa=eyIyMXg5Ijp7IngiOjAsInkiOjY0LCJ4MiI6MjEyMSwieTIiOjk3MiwidyI6MjEyMSwiaCI6OTA4fSwiMTZ4OSI6eyJ4IjowLCJ5IjoxNzQsIngyIjoyMTIxLCJ5MiI6MTM2NywidyI6MjEyMSwiaCI6MTE5Mn19" alt="Get recognized as an HR expert. Earn your SHRM-CP and SHRM-SCP certification, and set yourself apart. " /></a>
        <p>Get recognized as an HR expert. Earn your SHRM-CP and SHRM-SCP certification, and set yourself apart. </p>
    </div>
    <a id="ctl00_PlaceHolderMain_ctl18_hl_More" class="btn btn-success btn-cta text-uppercase btn-customized" href="https://www.shrm.org/certification/apply/pages/default.aspx">Apply Now</a>

	</div>

<script id="promo-panel-script" async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>     
</div>
                </div><!-- /.shrm-widget-row	-->
                <div class="shrm-widget ad-holder hidden-xs hidden-sm">
                    <input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Fluid" id="ctl00_PlaceHolderMain_ctl19_hf_Fluid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Size1W" id="ctl00_PlaceHolderMain_ctl19_hf_Size1W" value="300" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Size1H" id="ctl00_PlaceHolderMain_ctl19_hf_Size1H" value="250" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Size2W" id="ctl00_PlaceHolderMain_ctl19_hf_Size2W" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Size2H" id="ctl00_PlaceHolderMain_ctl19_hf_Size2H" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl19$hf_Section" id="ctl00_PlaceHolderMain_ctl19_hf_Section" value="StandardArticleBox" />
<div id="ctl00_PlaceHolderMain_ctl19_pan_AdWordsWrapper">
		
    <div id="ctl00_PlaceHolderMain_ctl19_pan_AdWords">
			
    
		</div>

	</div>

                </div>
            </div>
        </div>
        

        

        <div id="ctl00_PlaceHolderMain_ctl22_pan_Main" class="row-shrm-sponsors shrm-widget noindex">
		
	<h2 class="shrm-Title text-uppercase">SPONSOR OFFERS</h2>
	<div class="row sponsors-wrapper">
        <ins class="adbladeads" data-cid="18802-1901870943" data-host="web.industrybrains.com" data-tag-type="4" data-protocol="https" style="display:none"></ins>
        <div class="shrm-sponsor col-md-4">
    	    <h4><a id="ctl00_PlaceHolderMain_ctl22_hl_InHouseAdvert_Title" href="http://vendordirectory.shrm.org/" target="_blank">Find the Right Vendor for Your HR Needs</a></h4>
            <p>
      	        SHRM’s HR Vendor Directory contains over 10,000 companies
            </p>
            <a id="ctl00_PlaceHolderMain_ctl22_hl_InHouseAdvert_CTA" class="shrm-CTA-Green text-uppercase link-external" href="http://vendordirectory.shrm.org/" target="_blank">Search &amp; Connect</a>
        </div>
	</div>

	</div>
<script src='//www.shrm.org/Documents/SponsorOfferFetchFile.js'></script>
<input type="hidden" name="ctl00$PlaceHolderMain$ctl22$hf_DataCid" id="ctl00_PlaceHolderMain_ctl22_hf_DataCid" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl22$hf_DataHost" id="ctl00_PlaceHolderMain_ctl22_hf_DataHost" />
<input type="hidden" name="ctl00$PlaceHolderMain$ctl22$hf_DataTagType" id="ctl00_PlaceHolderMain_ctl22_hf_DataTagType" />

    </div>
    
    



<div style='display:none' id='hidZone'><menu class="ms-hide">
		<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/15/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

		</ie:menuitem>
	</menu></div>
									<!-- End - Layout content -->
								  
								    <!-- --------------------------------- SHRM FOOTER STARTS	--> 
								    

<div class="container-fluid container-footer noindex"> 
      <!--<div class="row footer-newsletter shrm-role-member-hidden">
            <div class="container">
                <div class="text-center newsletter-title"><h6>The best of HR News</h6></div>
                <div class="text-center newsletter form">
                    <div class="footer-subscribe-form" >
                        <input type="email" placeholder="YOUR EMAIL ADDRESS" />
                        <a class="btn btn-cta btn-success btn-lg">Subscribe</a>
                    </div>
                </div>
                <div class="text-center newsletter-all"> 
                    <a href="http://shrm.org/publications/e-mailnewsletters/pages/default.aspx">SEE ALL NEWSLETTERS</a>
                </div>								
            </div>    
        </div>-->
    <div class="row footer-member-newsletter">
        <div class="container text-center">
            <div class="footer-member-newsletter-cta">
                <h6>Stay Informed with SHRM Newsletters</h6>
                <a id="ctl00_ctl76_hl_Newsletter_SignUpToday_Link" class="btn btn-primary btn-cta btn-lg" href="http://l.shrm.org/opt/Unified" target="_blank">SIGN UP TODAY</a>
            </div>
        </div>
    </div>
    <div class="row container-footer-lower">
        <div class="container">
            <div class="footer-socials text-center">
                <h3 class="text-center shrm-Label">
                    JOIN THE CONVERSATION
                </h3>
                <span>
                    <a id="ctl00_ctl76_hl_SocialLink_Facebook" href="http://www.facebook.com/societyforhumanresourcemanagement" target="_blank"><i class="fa fa-facebook"><span class="sr-only">Facebook</span></i></a>
                    <a id="ctl00_ctl76_hl_SocialLink_LinkedIn" href="http://www.linkedin.com/company/11282?trk=NUS_CMPY_TWIT" target="_blank"><i class="fa fa-linkedin"><span class="sr-only">LinkedIn</span></i></a>
                    <a id="ctl00_ctl76_hl_SocialLink_Instagram" href="https://instagram.com/shrmofficial/" target="_blank"><i class="fa fa-instagram"><span class="sr-only">Instagram</span></i></a>
                </span> <span>
                    <a id="ctl00_ctl76_hl_SocialLink_YouTube" href="http://www.youtube.com/shrmofficial" target="_blank"><i class="fa fa-youtube-play"><span class="sr-only">YouTube</span></i></a>
                    <a id="ctl00_ctl76_hl_SocialLink_RSS" href="/about-shrm/pages/rss.aspx" target="_blank"><i class="fa fa-rss"><span class="sr-only">RSS</span></i></a>
                    <a id="ctl00_ctl76_hl_SocialLink_Twitter" href="http://twitter.com/SHRM" target="_blank"><i class="fa fa-twitter"><span class="sr-only">Twitter</span></i></a>
                </span> 
            </div>
            <div class="footer-nav clearfix">
                <div class="col-md-9 footer-col footer-col-about clearfix">
                    <div class="row">
                        <div class="col-sm-4 col-xs-12">
                            <div class="clearfix">
                                <div class="row">
                                    <h4 class="footer-col-title col-xs-12">SHRM</h4>
                                    <ul class="col-xs-12">
                                        
                                                <li class="col-xs-6 col-sm-12">
                                                    <a id="ctl00_ctl76_rep_Links_ctl00_hl_Links_Link" href="https://www.shrm.org/about-shrm/Pages/default.aspx">About SHRM</a>
                                                </li>
                                            
                                                <li class="col-xs-6 col-sm-12">
                                                    <a id="ctl00_ctl76_rep_Links_ctl01_hl_Links_Link" href="https://www.shrm.org/about-shrm/Pages/Membership.aspx">Membership</a>
                                                </li>
                                            
                                                <li class="col-xs-6 col-sm-12">
                                                    <a id="ctl00_ctl76_rep_Links_ctl02_hl_Links_Link" href="https://www.shrm.org/about-shrm/Pages/Bylaws--Code-of-Ethics.aspx">Bylaws & Code of Ethics</a>
                                                </li>
                                            
                                                <li class="col-xs-6 col-sm-12">
                                                    <a id="ctl00_ctl76_rep_Links_ctl03_hl_Links_Link" href="https://www.shrm.org/about-shrm/press-room/Pages/default.aspx">Press Room</a>
                                                </li>
                                            
                                                <li class="col-xs-6 col-sm-12">
                                                    <a id="ctl00_ctl76_rep_Links_ctl04_hl_Links_Link" href="http://www.cfgi.org/">Council for Global Immigration</a>
                                                </li>
                                            
                                                <li class="col-xs-6 col-sm-12">
                                                    <a id="ctl00_ctl76_rep_Links_ctl05_hl_Links_Link" href="http://www.hrps.org/">HR People + Strategy</a>
                                                </li>
                                            
                                        <li class="dropdown col-xs-6 col-sm-12 marg-h-0"> 
                                            <a id="footer-international" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="current-site">SHRM Global</span> <i class="fa fa-angle-down"></i> </a>
                                            <ul class="dropdown-menu" aria-labelledby="footer-international">
                                                <li class="collapse in"><a href="/pages/default.aspx?loc=null">SHRM Global</a></li>
                                                
                                                <li class="collapse in" data-loc="/pages/default.aspx?loc=india"><a href="/pages/default.aspx?loc=india">SHRM India</a></li>
                                                <li class="collapse in" data-loc="/about-shrm/Pages/SHRM-MENA.aspx"><a href="/about-shrm/Pages/SHRM-MENA.aspx">SHRM MENA</a></li>
                                            </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="clearfix col-sm-12 col-xs-6">
                                    <h4 class="footer-col-title">WORK FOR SHRM</h4>
                                    <ul class="col-sm-12">
                                        <li><a href="//www.shrm.jobs" class="link-external" target="_blank">Career Opportunities</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix col-sm-12 col-xs-6">
                                    <h4 class="footer-col-title">ELEVATE HR</h4>
                                    <ul class="col-sm-12">
                                        <li><a href="/foundation/Pages/default.aspx">SHRM Foundation</a><br class="visible-xs">
                                            <a id="ctl00_ctl76_hl_Donate" class="btn btn-success btn-cta" href="http://www.shrm.org/about/foundation/supportthefoundation/contributions/pages/default.aspx" target="_blank">DONATE</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 col-xs-12">
                            <div class="clearfix">
                                <div class="row">
                                    <h4 class="footer-col-title col-xs-12">WORK WITH SHRM</h4>
                                    <ul class="col-xs-12">
                                        <li class="col-sm-12 col-xs-6"><a href="/mlp/Pages/speakers-bureau/Home.aspx" class="link-external" target="_blank">Speakers Bureau</a></li>
                                        <li class="col-sm-12 col-xs-6"><a href="/about-shrm/pages/copyright--permissions.aspx" class="link-external" target="_blank">Copyright & Permissions</a></li>
                                        <li class="col-sm-12 col-xs-6"><a href="/mediakit/pages/default.aspx" class="link-external" target="_blank">Advertise with Us</a></li>
                                        <li class="col-sm-12 col-xs-6"><a href="http://hrjobs.shrm.org/jobs/products" class="link-external" target="_blank">Post a Job</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom clearfix text-center">
                <div class="footer-contact-us"> <a id="ctl00_ctl76_hl_ContactUs_Page" href="/about-shrm/Pages/Contact-Us.aspx">Contact Us</a> <span class="links-divider">|</span> <a id="ctl00_ctl76_hl_ContactUs_Phone" href="tel:800.283.SHRM%20(7476)">800.283.SHRM (7476)</a> </div>
                <div class="legal-nav"> <span class="copyright">&copy; 2018 SHRM. All Rights Reserved</span>
                    <ul class="list-unstyled legal-links">
                        <li><a href="/about-shrm/Pages/Privacy-Policy.aspx">Privacy Policy</a></li>
                        <li><span class="links-divider">|</span></li>
                        <li><a href="/about-shrm/Pages/Privacy-Policy.aspx#California">Your California Privacy Rights</a></li>
                        <li><span class="links-divider">|</span></li>
                        <li><a href="/about-shrm/Pages/Terms-of-Use.aspx">Terms of Use</a></li>
                        <li><span class="links-divider">|</span></li>
                        <li><a href="/about-shrm/Pages/Site-Map.aspx">Site Map</a></li>
                    </ul>
                </div>
                <small class="text-center small">SHRM provides content as a service to its readers and members. It does not offer legal advice, and cannot guarantee the accuracy or suitability of its content for a particular purpose. <a href="/about-shrm/Pages/Terms-of-Use.aspx#Disclaimer">Disclaimer</a></small> </div>
                
        </div>
    </div>
</div>

							    </div><!--/ .shrm-site	------------------------------------------------------>
							
</div>
						</div>
						<!-- This should stay here -->
						<div id="DeltaFormDigest">
	
							
								<script type="text/javascript">//<![CDATA[
        var formDigestElement = document.getElementsByName('__REQUESTDIGEST')[0];
        if (!((formDigestElement == null) || (formDigestElement.tagName.toLowerCase() != 'input') || (formDigestElement.type.toLowerCase() != 'hidden') ||
            (formDigestElement.value == null) || (formDigestElement.value.length <= 0)))
        {
            formDigestElement.value = '0x2226152DB0460B74AE84B95D0007BF3C2D91545FA52537373EA03EC25E017662C6D7C7F1908D8A59FB06BD5AA5AA3F51908FB63791C4E3E2FE67DDBF10EC430A,03 Oct 2018 15:10:11 -0000';
            g_updateFormDigestPageLoaded = new Date();
        }
        //]]>
        </script>
							
						
</div>
					</div>
				</div>
			</div>
			<!-- End - Working area -->
			
			<!-- Start - Hidden staff (do not remove this) -->
			
			
			
			
			

			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);function loadMDN2() { EnsureScript('MDN.js', typeof(loadFilterFn), null); }
function loadMDN1() { ExecuteOrDelayUntilScriptLoaded(loadMDN2, 'sp.ribbon.js'); }
_spBodyOnLoadFunctionNames.push('loadMDN1');
function _spNavigateHierarchyEx(nodeDiv, dataSourceId, dataPath, url, listInContext, type, additionalQString) {
    CoreInvoke('SetAdditionalNavigateHierarchyQString', additionalQString);
    g_originalSPNavigateFunc(nodeDiv, dataSourceId, dataPath, url, listInContext, type);
}

g_originalSPNavigateFunc = _spNavigateHierarchy;
_spNavigateHierarchy = _spNavigateHierarchyEx;

            ExecuteOrDelayUntilScriptLoaded(
                function() 
                {                    
                    Srch.ScriptApplicationManager.get_current().states = {"webUILanguageName":"en-US","webDefaultLanguageName":"en-US","contextUrl":"https://www.shrm.org/about-shrm","contextTitle":"About SHRM","supportedLanguages":[{"id":1025,"label":"Arabic"},{"id":1093,"label":"Bengali"},{"id":1026,"label":"Bulgarian"},{"id":1027,"label":"Catalan"},{"id":2052,"label":"Chinese (Simplified)"},{"id":1028,"label":"Chinese (Traditional)"},{"id":1050,"label":"Croatian"},{"id":1029,"label":"Czech"},{"id":1030,"label":"Danish"},{"id":1043,"label":"Dutch"},{"id":1033,"label":"English"},{"id":1035,"label":"Finnish"},{"id":1036,"label":"French"},{"id":1031,"label":"German"},{"id":1032,"label":"Greek"},{"id":1095,"label":"Gujarati"},{"id":1037,"label":"Hebrew"},{"id":1081,"label":"Hindi"},{"id":1038,"label":"Hungarian"},{"id":1039,"label":"Icelandic"},{"id":1057,"label":"Indonesian"},{"id":1040,"label":"Italian"},{"id":1041,"label":"Japanese"},{"id":1099,"label":"Kannada"},{"id":1042,"label":"Korean"},{"id":1062,"label":"Latvian"},{"id":1063,"label":"Lithuanian"},{"id":1086,"label":"Malay"},{"id":1100,"label":"Malayalam"},{"id":1102,"label":"Marathi"},{"id":1044,"label":"Norwegian"},{"id":1045,"label":"Polish"},{"id":1046,"label":"Portuguese (Brazil)"},{"id":2070,"label":"Portuguese (Portugal)"},{"id":1094,"label":"Punjabi"},{"id":1048,"label":"Romanian"},{"id":1049,"label":"Russian"},{"id":3098,"label":"Serbian (Cyrillic)"},{"id":2074,"label":"Serbian (Latin)"},{"id":1051,"label":"Slovak"},{"id":1060,"label":"Slovenian"},{"id":3082,"label":"Spanish (Spain)"},{"id":2058,"label":"Spanish (Mexico)"},{"id":1053,"label":"Swedish"},{"id":1097,"label":"Tamil"},{"id":1098,"label":"Telugu"},{"id":1054,"label":"Thai"},{"id":1055,"label":"Turkish"},{"id":1058,"label":"Ukrainian"},{"id":1056,"label":"Urdu"},{"id":1066,"label":"Vietnamese"}],"navigationNodes":[{"id":0,"name":"This Site","url":"~site/_layouts/15/osssearchresults.aspx?u={contexturl}","promptString":"Search this site"}],"showAdminDetails":false,"defaultPagesListName":"Pages","isSPFSKU":false,"userAdvancedLanguageSettingsUrl":"/about-shrm/_layouts/15/regionalsetng.aspx?type=user\u0026Source=https%3A%2F%2Fwww%2Eshrm%2Eorg%2Fabout%2Dshrm%2FPages%2FPrivacy%2DPolicy%2Easpx\u0026ShowAdvLang=1","defaultQueryProperties":{"culture":1033,"uiLanguage":1033,"summaryLength":180,"desiredSnippetLength":90,"enableStemming":true,"enablePhonetic":false,"enableNicknames":false,"trimDuplicates":true,"bypassResultTypes":false,"enableInterleaving":true,"enableQueryRules":true,"processBestBets":true,"enableOrderingHitHighlightedProperty":false,"hitHighlightedMultivaluePropertyLimit":-1,"processPersonalFavorites":true}};
                    Srch.U.trace(null, 'SerializeToClient', 'ScriptApplicationManager state initialized.');
                }, 'Search.ClientControls.js');var g_clientIdDeltaPlaceHolderMain = "DeltaPlaceHolderMain";
var g_clientIdDeltaPlaceHolderPageTitleInTitleArea = "ctl00_DeltaPlaceHolderPageTitleInTitleArea";
var g_clientIdDeltaPlaceHolderUtilityContent = "DeltaPlaceHolderUtilityContent";
//]]>
</script>
</form>
		<span id="DeltaPlaceHolderUtilityContent">
			
		</span>
		<script type="text/javascript">// <![CDATA[ 


            var g_Workspace = "s4-workspace";
		 // ]]>
</script>
		
<div id="ctl00_ctl79_PageinformationContainer" style="display: none">
    <input name="ctl00$ctl79$pageInfoJson" type="hidden" id="ctl00_ctl79_pageInfoJson" />
    <input name="ctl00$ctl79$pagePropertiesJson" type="hidden" id="ctl00_ctl79_pagePropertiesJson" />
    <style>
        .shrmpageinfo-filler {
            width: 100%;
            height: 450px;
            clear: both;
        }

        .shrmpageinfo-panel {
            position: fixed;
            bottom: 0px;
            left: 0px;
            width: 100%;
            height: 450px;
            border-top: 1px solid rgb(204, 204, 204);
            background: white;
            overflow: hidden;
            z-index: 9999998;
            font-size: 14px;
        }

            .shrmpageinfo-panel .nav {
                background-color: #f5f5f5;
                border-bottom: 1px solid #e5e5e5;
                width: 100%;
                padding-top: 5px;
                font-weight: bold;
            }

            .shrmpageinfo-panel .tab-content {
                max-height: 400px;
                overflow: auto;
                width: 100%;
            }

            .shrmpageinfo-panel .tab-pane {
                padding: 15px;
            }

            .shrmpageinfo-panel td:last-child {
                word-break: break-all;
            }

        #shrmpageinfo-resizer {
            cursor: n-resize;
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHMAAABkCAMAAACCTv/3AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRFAAAA5ubmSUUG+gAAAAJ0Uk5T/wDltzBKAAAAPklEQVR42uzYQQ0AAAgDseHfNC4IyVoD912WAACUm3uampqampqamq+aAAD+IVtTU1NTU1NT0z8EAFBsBRgAX+kR+Qam138AAAAASUVORK5CYII=);
            background-size: 18px;
            background-repeat: no-repeat;
            width: 24px;
            height: 24px;
            display: inline-block;
            padding: 10px 15px;
            background-position: 0 100%;
        }

        table.shrmpageinfo-claim-type-table td {
            width: 33.333%;
            padding: 5px;
            vertical-align: top;
        }
    </style>

    <div class="shrmpageinfo-block">
        <div class="shrmpageinfo-filler" id="shrmpageinfo-filler"></div>
        <div class="shrmpageinfo-panel" id="shrmpageinfo-panel">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist" id="shrmpageinfo-nav">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Page Information</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Page Properties</a></li>
                <li class="pull-right"><span id="shrmpageinfo-resizer"></span></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" id="shrmpageinfo-tab-content">
                <div role="tabpanel" class="tab-pane active" id="home">
                    <table class="table" id="shrmpageinfo-table"></table>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile">
                    <table class="table" id="page-properties-table"></table>
                </div>
            </div>

        </div>
    </div>

    <script>
        function shrmpageinfoUI() {

            var element = document.getElementById('shrmpageinfo-panel'),
                    filler = document.getElementById('shrmpageinfo-filler'),
                    resizer = document.getElementById('shrmpageinfo-resizer'),
                    tabContent = document.getElementById('shrmpageinfo-tab-content'),
                    nav = document.getElementById('shrmpageinfo-nav');

            resizer.addEventListener('mousedown', initResize, false);

            function initResize(e) {
                window.addEventListener('mousemove', Resize, false);
                window.addEventListener('mouseup', stopResize, false);
            }
            function Resize(e) {
                var wHeight = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight,
                        calcHeight = wHeight - e.clientY;
                element.style.height = filler.style.height = calcHeight + 'px';
                tabContent.style.maxHeight = (calcHeight - nav.offsetHeight) + 'px';

            }
            function stopResize(e) {
                window.removeEventListener('mousemove', Resize, false);
                window.removeEventListener('mouseup', stopResize, false);
            }


            function renderPageInfo() {
                var pageInfoTable = document.getElementById("shrmpageinfo-table");
								var theInfoJSON = [];
								var infoEl = document.getElementById('ctl00_ctl79_pageInfoJson');
								try{ theInfoJSON = infoEl && infoEl.value ? JSON.parse(infoEl.value) :theInfoJSON ;}finally {}
							
                var pageInfoJsonObj = theInfoJSON ;
                var pageInfoData = "";
                for (var key in pageInfoJsonObj) {
                    if (pageInfoJsonObj.hasOwnProperty(key)) {
                        var val = pageInfoJsonObj[key];
                        pageInfoData += pageInfoRenderTemplate(key, val);
                    }
                }
                pageInfoTable.innerHTML = pageInfoData;
            }

            function renderPagePropertiesInfo() {
                var pagePropInfoTable = document.getElementById("page-properties-table");
								var thePropJSON = [];
								var thePropEl = document.getElementById('ctl00_ctl79_pagePropertiesJson');
								try{ thePropJSON = thePropEl && thePropEl.value ? JSON.parse(thePropEl.value) : thePropJSON ;}finally {}
                var pagePropInfoJsonObj = thePropJSON;
                var pagePropInfoData = "";
                for (var key in pagePropInfoJsonObj) {
                    if (pagePropInfoJsonObj.hasOwnProperty(key)) {
                        var val = pagePropInfoJsonObj[key];
                        pagePropInfoData += pageInfoRenderTemplate(key, val);
                    }
                }
                pagePropInfoTable.innerHTML = pagePropInfoData;
            }

            function pageInfoRenderTemplate(key, value) {
                return "<tr>" +
                            "<td>" +
                                "<p>" + key + ": </p>" +
                            "</td>" +
                            "<td>" +
                                "<p>" + value + "</p>" +
                            "</td>" +
                        "</tr>";
            }

            renderPageInfo();
            renderPagePropertiesInfo();
        }
        shrmpageinfoUI();

    </script>
</div>

		
		<img id="ctl00_PageViewTracker_imgTemp" title="temp_image" src="../../_layouts/15/SHRM.Core/utility/pageviewtracker.aspx?user=Anonymous&amp;url=%2fabout-shrm%2fPages%2fPrivacy-Policy.aspx&amp;isMemberOnlyPage=False" alt="temp_image" style="height:1px;width:1px;" />
<script type="text/javascript">
    $(document).ready(function () {
        var src = $("#ctl00_PageViewTracker_imgTemp").attr("src").replace(/\&url=(.+)/, "&url=");
        $("a[rel|='Download']").click(function (event) {
            var href = $(this).attr("href");
            $("#ctl00_PageViewTracker_imgTemp").attr("src", src + href);
        });

        var fileExtensions = ['.doc', '.docx', '.xls', '.xlsx', '.pdf', '.xps','.ppt','pptx'];
        fileExtensions.forEach(function (extension) {

            $('a[href$="' + extension + '"]').click(function () {
                setHrefSourceforPDFandExcel(this);
                //console.log(extension);
            });
        });
    });
    function setHrefSourceforPDFandExcel(element) {
        var urlhref = $(element).attr('href');
        var urlpathwithdomain = document.createElement("a");
        urlpathwithdomain.href = urlhref;
        var src = $("#ctl00_PageViewTracker_imgTemp").attr("src");
        src = src.replace(/\&url=(.+)/, "&url=");
        $("#ctl00_PageViewTracker_imgTemp").attr("src", src + urlpathwithdomain.pathname);
    }
</script>

        
<script type="text/javascript">
    _SHRM = {
        MemberInfo: {
            IsLoggedIn: function() { return 'False'; },
            SsoUuid: function() { return ''; },
            IsMembershipActive: function() { return 'False'; },
            UserFullName: function() { return ', '; },
            UserMemberId: function(){return ''}
        }
    };
</script>


        
        
  <!--googleon: all-->      
	</body>
</html>