

<!DOCTYPE html>
<html lang="en" >
<head><meta id="ctl00_ctl00_meta" http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-title" content="Foxwoods" /><meta name="viewport" content="width=device-width, initial-scale=1" /><link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icons/apple-touch-icon-144x144-precomposed.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icons/apple-touch-icon-114x114-precomposed.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icons/apple-touch-icon-72x72-precomposed.png" /><link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icons/apple-touch-icon-precomposed.png" /><link rel="shortcut icon" href="/images/favicon.ico" /><meta name="apple-itunes-app" content="app-id=920603842" /><link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" /><link href="/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" /><link href="/bundles/jquery.css?v=cnHUxLe59ZKxG-21rdw_SBRYYxRtIW_VQNH6FoRGFNI1" rel="stylesheet"/>
<link href="/bundles/foxwoods.css?v=fmpTrds4Dqw2J3CUBZTBy6aqRgEfSMFGPg5-nHPOIZg1" rel="stylesheet"/>
<script src="/bundles/jquery.js?v=yOzi2ftkJfxsmAWw7nhoLoQt9XgjW_1oJSUgj4UH5No1"></script>
<script src="/bundles/plugins.js?v=qh6InXH2VnYkG6R6ElSsUhKlvHM1xQ3ouVpmS_W4wcE1"></script>
<script src="/bundles/foxwoods.js?v=8UIwvARcowOUqhxilaEjO-p99rt6fgGRanyrm0PwPdQ1"></script>

<!-- Engage Web Tracking Code -->

<!-- Please insert the following code between your HTML document head tags to maintain a common reference to a unique visitor across one or more external web tracked sites. -->
<meta name="com.silverpop.brandeddomains" content="www.pages02.net,webtixs.foxwoods.com,www.foxwoods.com" />

<!-- Optionally uncomment the following code between your HTML document head tags if you use Engage Conversion Tracking (COT). -->
<!--<meta name="com.silverpop.cothost" content="pod2.ibmmarketingcloud.com" />-->

<!--<script src="http://contentz.mkt922.com/lp/static/js/iMAWebCookie.js?b0b2b92-150cf547484-1973771dea71da7e4c551ed9f05528be&h=www.pages02.net" type="text/javascript"></script>-->

<!-- For external web sites running under SSL protocol (https://) please uncomment this block and comment out the non-secure (http://) implementation. -->
<script src="https://www.sc.pages02.net/lp/static/js/iMAWebCookie.js?b0b2b92-150cf547484-1973771dea71da7e4c551ed9f05528be&h=www.pages02.net" type="text/javascript"></script>
<script type="text/javascript">

      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-26275960-1', 'auto');
      ga('send', 'pageview');
    
    //var _yourip = '207.241.229.213';
    //if (chkit(_yourip)) {
    //    (function () {
    //        var sp = document.createElement('script'); sp.type = 'text/javascript'; sp.async = true;
    //        sp.src = ('https:' == document.location.protocol ? 'https://secure' : 'http://ib') + '.adnxs.com/seg?add=657413&t=1';
    //        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sp, s);
    //    })();
    //}
    
</script>


<title>About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)</title><meta name="title" content="About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)" />
<meta name="description" content="Foxwoods Resort Casino features deluxe accommodations, fine dining, a wide variety of entertainment attractions and shopping." />
<meta name="keywords"    content="connecticut casinos,resorts,hotels,mashantucket pequot,ledyard ct,family vacations,business meeting,luxury travel,romantic getaway,reservations,golf,best lodging,accommodations,accomodations,spa,suites,vacation,getaways,discount,rooms,area resort" />
<meta http-equiv="imagetoolbar" content="no" /><!-- disable IE image tool bar -->

<!-- Google Authorship and Publisher Markup -->
<link rel="author" href="https://plus.google.com/110190905165923896755/posts"/>
<link rel="publisher" href="https://plus.google.com/110190905165923896755"/>

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)">
<meta itemprop="description" content="Foxwoods Resort Casino features deluxe accommodations, fine dining, a wide variety of entertainment attractions and shopping.">


<!-- Twitter Card data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@foxwoodsct">
<meta name="twitter:title" content="About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)">
<meta name="twitter:description" content="Foxwoods Resort Casino features deluxe accommodations, fine dining, a wide variety of entertainment attractions and shopping.">
<meta name="twitter:image" content="http://www.foxwoods.com/images/dual_property.jpg">

<!-- Open Graph data -->
<meta property="og:title" content="About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)" />
<meta property="og:type" content="article" />
<meta property="og:image" content="http://www.foxwoods.com/images/dual_property.jpg" />
<meta property="og:url" content="http://www.foxwoods.com/privacypolicy.aspx" />
<meta property="og:description" content="Foxwoods Resort Casino features deluxe accommodations, fine dining, a wide variety of entertainment attractions and shopping." />
<meta property="og:site_name" content="Foxwoods.com" />
<meta property="fb:admins" content="91920463745" />

    <link href="/bundles/foxwoods/interior.css?v=B8J6mQzFblynHp5UNW_m4f__Wy4HJH8oHW9zKwJs-GM1" rel="stylesheet"/>
<script src="/bundles/foxwoods/interior.js?v=dInLYELijxmGL3u7prwHm7oIpakpgJMwXDsJvJrwbJg1"></script>

    
<title>

</title></head>
<body>
    <form method="post" action="/privacypolicy.aspx" id="aspnetForm" onsubmit="return true;">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X/rWuuEPjIPy+oNNigR/pvTd45r/VSYNV66M1Xc++mi3F/xvabMbKSK1Ifp3bbkfOjJiGQXVqv7l1513ov3BUcUVhu15GFNnmwMyYF6Yhu4Lfc2i5U9HOncfCjiHzwLDizWISM/XUMXhFVA4E7CVdAvmzwEBpqYvWPvIsdU0rkZovly15nJUWi2WW18Dbqk8HdBM2CHjObnJgP7G4H/iJtNBvzttxFVMqjh4BmUvmLDegmK97Z5x8LRTrfydwQEMMUGym7xyNYuKM5DhgXmjbw0oyR5gz7HpsW7CN1Eg5NhVmUwsrD2ethwkwOqcGSCm03R9eATY1qY8bl+ToYzcAf/MbKURUjjOi35z242CMBsXhZylWf4KEEAUm8D9KADM1Q/c+DOnKbnhiF7D7w66ZAI6OAUmYHwN39r32NtJ+ga/2lnbcym9acjoW0Id8I93fbLjX5EDu2tox4MupNOuJoius7LBIjmqS9Af9tnhA+4hDsFp1Q5tFbVMbNR3bnsV/23CV/BqAwZLEpnsj4EdW7fnKvz5dB4o+EmnCgvYDHNGhiG2UVqF0L5JvOAAExDIo2x3OrvCnFFUTqq7jgs9DCOQo7gtq8DgYnY97ZY7m63dk2/RvPTsRa6ENxmbpa4MBNdLzVrqFmjnmnj+zF27Z6kdw32L4cr2zdzklok9bTrKCmP8HBGyZIP/CleR7P4mqN9iPHIuTyL1qyEPVd5wycEOzqjTaqOjmncs4YFHiVuBzaT+qKxQKoaMBKLMNZNT5g97dmgPPklA1sryE3Ih/CaDkhIQxk6ChuGFx7f9Zoi8WUq0jvtD/EEaFtawYnlAxk7cMTCSy0Pt/+WFf81MWrteiWL6R7BjG7Otab+q2u1ZQM9H35wnW/6gz4WHs6aJvmWSgpW1EGMPmC7Ei9sIJRJFJnIOVQdSb8XIMzqTp1Ha0Z3b+xrjvFX3PhFltJ7ameKz2ZPpbOWx1N4JkLr2tjZ99G/CT1mAVr1IrlnsHi0itS8ZUzD49zaFCco4mAk9SoN6rrT002eUth1GmaOZzAHHGRMZ1f9ecFPSOGPV8uM6rOfQL35il6AbjgvS8vzQsR3HtR03i/NeabM7HnnUGFzIPQRDnU0MZAuEBYgO7+qdB4o63kY4QXEyMfg/EzwdBbAPRD2EK+uQptqxvuM8h3jm4AC4zzd/+TwSfYApHsvFNMLzJ8UduodAA+31dkMvLqV1CVS1O1jVopieT87PAh4MEScLD40zhNZ0oizHqlc1/97GSIcIDYKNGGBfim4fcEmA8qP/YEkrVRItit4DQg5qrcBd+Y1piGC7zEVMetUYFq03V0DAzZPLoGrs4LORO27E0aBQWCAtfFVK9Fg3eFWBLDKwU10SNXz0y04yzBsdwSjzKL7C5u5BoSf/lm2ZPu5hMLWbgk7PWYgSmtjS0+y+nplVz2a4ROiYNDWOr+TIJhuhAmfMAIpRq9ipb984gmfvs9oxaLJhLgjBk6MBajIouBbcqXISQ54FXKCUmZGqg9OkajECCC3ss+U9HEPfn1YixJMm7plXRRXOFy2zzelS4v0AcVzkruNOr+mjv/nMfGdTfbC1OBzP6fLn0BMFvQAXSkp0im5QLc5eAX4kzr/jjQ5WjxmJLVrie5dWAjb9aJAKXOD0h84f5/+Oc/8N9boIIUzNYTnReb8lyHHMycIW7MkiOMmORyPav6ztwBh/3JW0ruSHzz9MGTnrMSgL21SGSRfjSAc96Eq64Xa8wX5n9NWQ9zS6WW+8aZQ8u+BXhDtwT2rHUasRJo8JYz+52bctFyoFcQ98W5mX3GalvlBh5Yj0r737yuO9nifaU9PE2xLvSm+E88MGNnRycQPVrl15BvAH2KItwtztXioTNxQloQB3pUKE8ILRaP3WZ5VcdlHMObxcaeUTzh7V2doI7bJD4p6nw7FGsttKEmrjHm3zNOaYIFMTLLy3DCm10AP6eZY0vSEjRXi4aO8JT458wRdc2tX1cwvju/TES8pzVjT7BemyItOSYN/0Dps8s+slf6R/c2XvlvYbCEsZX3DZ0mG40lOB9go9wYFRmQHCE+M4EmckPA2Z+uauO3YdPS52F+W8duK13dbTjPIdfg7biGUrEa5C84WNOiRkXq/N7kWSS6kDM0JELhluDe80wn/LDOI6Drl8bC43tuReOsxhwu9T1/Co5XbyGi0znUmr5yG4AiFS4xuNwPJAuJ8ksSIEA76JTUoL+SSLmkHpYTtURpcuYGzonAGDITcvXUp9xJmWbYi392v208FFm3a4sa/wtSDQ0zLitlZuqdwRqAzsuCfUwnry6XMIfT91/puO0ZoSHCOjJR9yyfbg2rtt0qaMvDAQAuTGOFGOEgoQA7pkktRoxIFu9lfWCJ+u9D88JYxKbvUPqmtJvf6+LWkwtpaD4Q5YocDSZfIsDsbAV6JdPmtOL4tEmaX0HaX9X9LcuXk43q5xwKABK5vT/o5rH4OgAEzOOVpOgA2PYtch1t10Y/fudZBZu12AVaEJ1BC4SPg+UzugwOuykW86xwwSDs7XHcc4uMwkRyK0vlA1p6+pdz44WyML51lTmIKcH/FelZmeQ4XMZWJG1AEqvWpf4FAWHggqEi/uzhAEdv5vYbgiI+5rPfqFoVs4B7ZHdTQmKRWG48V/wHzgB52nU2YiMuOYWqgQjfOAtjXZRvCZCF4EseAGrolnq4C427HMlZphqTPvSOT5M9A17EEhw/9HlK3v3iPjJ5UiZzLHU81TECtgcUGujsl4/8iPqYJoljLv+P1PvUAuFryxwFVLDleOwWKvZvHRd+6mX2pW81ih7UWg/gDiVm+fCgFX4Iw2WnFCXxuKRhl37OYk5E0HwpYfeS81Bgb6K/8riZOPmJsBaKBF7Ob2quV21uPfBDl/pF+kMQUAavLO+c2kT0NnPAxH0JaXYAslRiXUHquTkRsnjQ/o7NXjgjYVcdaLb4+JJryCudZedhyN1ICIDXUJbO0qqyptdtknlTCAfScEjKdaNLWlAyuGwHz/HwJA4HEUT9POdViewTcITQVUYvODVtCJfJuxNuDVn+B2yV1KtVZS6d8E/tkWu617UsD1gT25lJOQEzsxP/lmG73xqEiwwCFWXhY/EEcUbkVHTTNH0O1qDAazeJ+Jz1LiK4mbVTsrm2V65ZP5bbHfnY4Dnft3azX/i97dMz249L5ONb1CFGjkEQNfqt8jmRvDjCP+z7DY334Y/ezkeV7Ix0prr3a2xjHK6ixDk2wtUIidKJf28nP39hHlKcxAvN44wD6WJQOztWvaa/qozM43vsmcTFhCiQzSrcGRzMpJqq/8P9GrxZLz4hIfQTSBqPEU2QDNlHCXj3hChYZAf+6umvNHJZhNj8TFsrDunZEPoPyr+cMUyh23NAJyhaAcv+SvpN/VUaQRxf6YUPMlml2VdJJuK7jl3r5ezkWXmcj5sn2l5PcE0jPne5CZpGBjIYECX1HV+y+l5Oqac//S9j+ZQ2XwqOSPsFsmlnjZFJJxr8bkEHXlaIf0ZL+cebu7xngmy+PXnh1Kwr4vZMiRv6pZBw4AU7V9h23AI/+82YhFX263sFX5bWuu/Hai8YqLXf33jtBF0i7H4qrgpI8ZCfxBvoYv65BCy08MNqOJR7dDMYiVJUIYAAOJXgYluaWo4BsWAttcw/Bm8aDZ4cBl9CW0KZA/51+2NJvywWZsONHeQ9LUlrIzA5WW/Ks0FebVnbs6ro8RSjsWSgp+LRRGc1YwGwNYZe0fAfPlELyEGEo6hU7ej6Zfu12+XZ7ekVkADM1iwhdXHi5eRO205JY64wx+CUsjM2Y5h/UPQntjkvofd8+rsJvG4XAvdOyOyD2DzDBKLVFFDJzUWSuuyx4HM1NcL7+6TyLnfpg+LqZJP2eT8GHaWEuOY9Xd6BwiLcrZX6Y2FVEQm2I1L7Qt0fKKH6n45mX3YuxR/85Sh+e3FjE605FPMg/3Ql2/z1AQT2YQMRehwpeNl7u9bgxA7qhhNz99OApnmiQMAyJiSkjNfXkhAV1QjRhNJeXe3Dhrlk7Ch0le+dp6j0mgAqTte3pBm92CNpGGAuHZyghuE9aaUqwxXKrDKFhicgd+DyPs27N7sJf2HfB+EJkdkaTwbrsNMZMq1KDdaxIT0tlhlnS4Bg53mA9CeHA4yi39S88GGWRLTQSjN/1uZMtk2JMS2V7IhXBcJ3EsL2bKECbiqR1y5I+xaQH35pRlHY9f6jJvcYDjoB+cctsMCcYMoY/gGEl5wREUZ5PBGr+4LD4olNq865IifL2zAD6nqzJOnhsJBXA2JhQGJsht3fsf+NdMHSXrjOU89pNc+5Ze0l15lBcbJxAtZeHcnBvpy3szoYzT7RDiezRFX9ScbPNEgVU/kSFe9deMCxrrpfZzuVl3AcR8p/fEZFueMh4Z2cMyr/d+0yIeBgXc6Qa2ktOgaXolnJEoPwX0Djp4O5enE8S/MC4urJUuuQEvpt0deRFVn/KDLCnCwKXvtfLzVd/Pg3HnySpgAE0y8ZX+vjcgs2oSYlWi0klsAlPS90jR/Xt4m+MiooEpYK516s3wsbc5k/Kt088dRGcVzy/sNcw+TtTOavtJ0TrAFaQBIbYI/o7RObE9owYik2YfvC38C9gSCAhsb9PP5KjMeBmb0obVoDJlGZubp+JVAkfHLhqOG7lhU2K+EQXU7VEnEL5cWmygh5uncgjoFvbF6HXcOUF6ztlaMxiJCI9+zfMpGLD6G0rV8ixZHpF7VzPx2yvTIlmEmj2H9ntPl/zvdznidpUWzNkvpf8y63F0BvfLcUfQ/9uAuSi9o2OZE7f5P75J9D1vAklIxJynCtzrM1+hu5k40AzVoBKPZKvweAL4pAs2j486yhhO90wSixY+DihfX184lPU/7EQVfMyY6nBUdGFf/i2oM3Wzb2LThY1YkzbeQeb+/Gy2k2pitxAUx1LW/41kB27cmntTXnWXP74FeNC4ODWB3FT8zAdy/tmoknKz+mwxHK3un9qFmDD9Xhgb7XRpcVkKYTNWhshe+xgN0uxkksbe7Vb3h55fNvhEzxmKRsL3UI+PJ9zxOa7jL456Hnzcs5zN2cTi6Ub9/xwUYGwh0DflIeM4sBTGV9FpfZlv2eJzoy7NGxcriKca5Z+B5kwmJCVj1QsnCqo9MAFU9RThTk7IqRSn6aR4sXOlryUKGGZhHjqQXOC7mgCmmCKxlPqZx/mN556oAEQfGPelvjnAC7gxOOr8P5UPzfpsk+v6khs0NUdwUrHVMZY8KZcJwXkn5GDs339btjD+6u1Njfa30iCBKSaL9Udf2yb3ZN6k9Ov8BHbACzL6tVf7DTuw3AgKYn9oLzIEvcNzofw3fBr1s9koYN0wmfmBL1rffV5z3tCdu4kaneZwQb+te7+cTP5yJh59lnwNf53GAsFVr+Rx6np8OjUCNHGWs516T+VPJokhu2tXeaE6C6WMDwEJrQnWRPx9on7Y0P7MKmgHZCuYJieOT0dovcYzRlWfPsYUjCCZAgkIkf0xRs+hYjuK3S62p4ojKSPmHW4vJcSJnP/hd1SwWPe+qnRAQW+8SWJJ0Uakmk4DIewaVrC32mUwdYD/7fkdV25nnf4T2OrPlBWBvnNQHPVgsoL7kBYHCGu5ep0PDnl8NYuIOtdgvmtbNovh6Pt5gJsdccZdAe2j916JnxLiI+23HPr1i8xpRB1CaxFTTlxUwNACS2EfrmVUL/mVModBEJhzjtUeCNlDY1HFxRXvhZF+NDqSg6PsotxtPnY5txO8ykRNpQe88empL6y+vgsZaJZnxxmCIUoRbd+ESKnps54XOIISAxGv+NJRgQfrSOWR44ex+Vz1rbcDPXIJGOLP8rce5VUrzkND7uObFW0Tk9mM4KuINGt7wAJIZdHFFqzPagusLKqzs7HnvlPqcWDggZsH4UYM0nVeqztwctW39k8+0ZhVlAks60m0hBuYbMbZOr5ndvKez94OgnlQrlt9VN70kJtEiVkq6WH0coKBOyOiNB0minrtqo7EZUQ3Ikx+2qFU9TrSWvyStopuJp2D5tk3hbCFjvwm1+uuY+sydyZ2zamquk3HStwahYJfey+eBV2kRUyhI/h5I9yo3uUHPngN2zeP4hSHRB3xgFf1K68wgzCy/0XMJVEaEVzTn3lGor/ch6mEGL0iZYwkep4JmRtdobiqeF/yMgJSNJSkVuaY6ImyEVjvXtMlDs1pfTBf0WBdoj++0yBsOggWs0T0Ic/GsHf9zuXfS7+rSLCBN+XeJlLkAmRZ9tpdE7k7Lvd1OM+es5TaSTor3wxY9Iti/72t+4x+F4eI821aBSxkP1FfexBvg0xXK+XnGB7kklCBgEXRhZnpYgYJYjxHLCIKrFhnx9tiIWb7LLRE5QDiY2cEFqcppfpAc6qmRr2Q6cHlaMhLWdSfaD0q5bV2FiAUFDX5TojAw3QGhWGErOhI/9twVvIp0V9NZtox0z51F6UVwKOoFJaJEeoGmVwU/yOUNTqT04dng9/Pxk9FaWxW06J9x4Xp0D0RT8SAATNmv5Ry6Pcxhh03vtldsVw0/ay5Uq4LnY+iu+abVxVIWLg269OztGtrzEykBISypHAh/8yjJcKS4fCjv5yig4NPyaW9BmNy/w1TeHkpvb+KtfKFBa2r+IZVRfOCsgeuzIeArvfxsq83BCwKSDHCpztuoBlOfksHZF/jtQnQwioLcBFgLv1gETYG+fkAtWCiLNLEEnxCvcs8V9XqCLfEKyfRozHBuaqHA7XrQnkJzcCPlRFuccGkcTxaoyf4CJfUg65qQ4Q3mP7bf96JwANttdCmWSBpGDgdwg8ZMhLATWZIKZ/cu7j5HWCim9Izlz9QoBBAM5AKTxPB4jrw0TNQ/gwLV0LPVExIxiWee3dVuDk65fAE/iB00jlveQgju2gUTsozCXxKgr3vQK5g00aDDUs3zNmPIR2Vi2uiawKg4AFLlxH/FbP6pNfrxVyzCa5lcjV3Gwd+IB23nyu8yjvpxwAHzb74mcIflSG8/xtvAtGeQUqhhasdtuzfw3+jc2MdNsyaMJLi6DLqjLaSf3gPHcyzxxKovwCNKQX8NDr3XB4eQSZPHmU7rwJZluvopVM9psZGqCC5KJN5ABY9PEyNyTw80WMjlfAapBO6pdLZIA/EiD0d88HK/qK9Lgap49G57CX1bZ6u4BSssfpCYKZOqK3PeGu3V6mJombgVcBgrxknXaFemW3UtI1U1Y9M65yRnfX024EtNViDLtCzbd9xJvEnu5jha1NLEltLoG9umocoqe2X3/HUJokB1qavLkBzJrAlxNQe8OdkRTq35kQ9H0pbpeVAuT5rA1fnEBFHr1H42TIzQ00ufaq5sfbpolLxclEuhW68kk2dIG2pyf27oJwvTfAFh3z6HF4+78SBnR6cQZnRH53lGjZhf+GlGQ2WUIX07uiI56HvbpNLSqej48a5SKWyrWrG62FfpaKmWMko+cXeUA69YGLoXV59nmNhmuyFzJ1SCWgF6XU8QS/RdWLNWZcdxL1NSrMlCsp0LXuieDPeAcGLzWbTR8ePLRuqb7kbEBT/uDMA3KMB068Qja+bHb9uf9/d6vFAsQGPp2YjpRNcvPZtPmfwBUIF/7Omf6tpV4Z9aJVhdaCJfrzx6O9P7zgkk+BP25WoXmoOL6S0AGIse/c33c3mp82WEkrJMqpywWtErL0G8n2Ct+LaUv/U+lwSboAZQ7akdP3D9wJL/opr3aF8mN1/e4I8XhJRCR46fs7HetN8TatOs3YJ7lzj9Sh5xIfDIugu66WHN/i5GWWpuMyCXSFOlrWwWfj7CtyqzZIR8ZdncrD5OQWtYjIGNP1ozBsgeNR3vxFyixrwE04RBv++MTpZ5iycuv9qJKggn1y+FDzHG9FogU9/j5TDJDtNpSxuPsjQQNpxgsVi6I/wZzEomFhoQm+hDlExlICU3Pzp04ws8pD4PUvAoJKSSBriT50x1gyoLj2TPtTpVE9Zss0tqg1wNTh695gGE5EYyOB9jTfbhVmtk+xRfdIE0INvyjDJ4t7t9d8UH4GExhlKlJq8UgtFT6Ih0/yuTNCyRzvrxhZr349QpP1oIh4FchCoyE6eSdWKpVxG/YQpm/DHxws98xGzxLIeJQJ+f46coonZe7OFLHaLn616rE5D9Gl/j/2iD3daO3qBj3gXPCHbry/XY926EfBea5QluC6uqZnU0e9C/l3icK4L3W16OCK3ArhWBX5XmuOTk147QVJ+lQtV0EROhKGw7Mqjb8Q8IyRRdPY7lOGRhHMXZjLqJ7hKYFcoB2B5VJvtIAhXoOz1lucmyZDi9qvu3GTupvtt7ufgVYek634gFMZQmgxlV5BGdNdoVJfSRnfN6hditU/TOd/MTlYB+gE/bDHtLnOiNfMGx5od3XDLDyuo4JZkUAoue7DVgakMox3+kyYOkIPYXVzIrzKM6/SidWtSCYY77lzA1l1MxQHEh6EGv7bi8W1O7/5zIZHOwt7ZWjvDh4rSFrqrKqBKN+DHn4ZRU25lvM2lRPwGo7wzTQ76OJUw0+OyzEayymlBiqdLIgKeNV76T/xyqXViYZqSi93jSaLWS8+b8fJMr3ypIFHo2EWYQg84wQ3cc3NgGmC2z0JOGR5/A/oBaaxuY5x5q7EmNqPNHAYI0pcxWHDeRAplKwpCSXWF4o/9w7cwW3UEwinyDEM/RLUQqIMd02Y7XdqCGuiC3Hx7p2XtVWcuID9TcZwE0lNUr1EmSg0r2giwMzBrSHX7Ja70SVt/t379w2x4HhI6NTh+cwqGN+D9NV5POElzgN8fOpLZMEqjHW3CHh/FDUB4cN56ASQlN9OJO8pLx1OFcGHqob0IVhynE/TRubxCrdaKeF+yNvrQid/5R/GdkDWC9GFSnrSq6v0OCrngWIfhnmnKYtEN7YCmnurODiqltax1aDCqzzAD+ylgB64S2AQtZV7b+zJ5B8QPO4EM/zVx+LHNckHd59R2laP0+Jzw98mTQhBo4g1vizpp02J08hlBesZAjC5o3E/OfGxKVYfTFngXW6bQanz++2xxhT/fMjidQ6IqGdW6Pr0Dre3O+eDaJj1JVrFPyJjgp7xt+XUL6pptlmQUKbm0NCRKPU6VCNxH0D1YhbY4E5q5UpUM5itciPOrYbMHekOOi8IbjO83DC8tRfDbB1rwJ2cR1rxMr1BfAwJj/Ir+hK6RIsV4ef8N0Bioqn4YIdEeg7Fg2Hgh4IuOf7M8K+2BhdPl3JY1lwfYw8uSyCHiYglOxH0DR9oxxGFdcD9ao13uEgCFLbkjOmliIYOh/+cjmvxDNz++B+RRHMnbZEE3rqKEoNfGhN4iyEeN/dxJ4Y5qJnAU6f1RwJLbVtC0wXUtrDBmEin+zoN2/J1nyV7Ms7jP1kLsC0QX9B8hocIX" />
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F187B426" />
</div>
    <div id="mobile-cta">
        
    
    


    </div>

    <div id="nav"><div class="foxwoods"><ul id="jetmenu" class="jetmenu black"><li class="mobile"><a href="/eventcalendar.aspx" class="cta">See What's Happening Today At Foxwoods</a></li><li class="mobile"><a href="/emailsignup.aspx" class="cta">Sign Up for Offers &amp; Updates</a></li><li class="mobile"><a href="/hotel-packages/" class="cta">See All Hotel Packages</a></li><li class="mobile"><a href="/promotions.aspx" class="cta">See All Promotions</a></li><li><a>Game</a><ul class="dropdown"><li><a href="/slots/">Slots</a></li><li><a href="/tablegames/">Table Games</a></li><li><a href="&#xA;                  /bingo/">Bingo</a><ul class="dropdown"><li><a href="http://webtixs.foxwoods.com/bingo/?key=5001-bingo&amp;amp;autosearch=1&amp;amp;venueid=2&amp;amp;hidecriteria=1">Buy Bingo Tickets</a></li><li><a href="/uploadedFiles/Pages/Game/Bingo/Firecracker(1).pdf">Firecracker Bingo</a></li><li><a href="/uploadedFiles/Pages/Game/Bingo/Foxwoods June Bingo_Calendar-PRESS.pdf">June Calendar</a></li><li><a href="/uploadedFiles/Pages/Game/Bingo/Bingo_July_2016.pdf">July Calendar</a></li><li><a href="/uploadedFiles/Pages/Game/Bingo/Bingo_August_2016.pdf">August Calendar</a></li><li><a href="/uploadedFiles/Pages/Game/Bingo/Bingo_September_2016.pdf">September Calendar</a></li></ul></li><li><a href="&#xA;                  /racebook.aspx">Race Book</a><ul class="dropdown"><li><a href="/racingresults.aspx">Racing Results</a></li><li><a href="/uploadedFiles/Pages/Game/Race_Book/July-2016-Race-Book-Calendar-Foxwoods.pdf">July Calendar</a></li><li><a href="/uploadedFiles/Pages/Game/Race_Book/Ultimate-Race-Book-Foxwoods-Racebook.pdf">Book the Book</a></li></ul></li><li><a href="/keno/">Keno</a></li><li><a href="&#xA;                  /pokerpage.aspx">Poker</a><ul class="dropdown"><li><a href="/badbeat.aspx">Bad Beat</a></li><li><a href="/poker/">Tournaments</a></li><li><a href="/pokerpromotions.aspx">Special Events</a></li><li><a href="/uploadedFiles/Pages/Game/Poker/June-July-2016-Foxwoods-Poker-Schedule.pdf">Daily Events</a></li></ul></li><li><a href="/videopokerlogin.aspx">Video Poker</a></li><li><a href="/credit-app/">Credit Application</a></li></ul></li><li><a href="/hotels.aspx">Hotels</a><ul class="dropdown"><li><a href="&#xA;                  /hotels.aspx">Overview</a><ul class="dropdown"><li><a href="/grand-pequot-tower/">Grand Pequot Tower</a></li><li><a href="/villas/">The Villas at Foxwoods</a></li><li><a href="/great-cedar-hotel/">Great Cedar Hotel</a></li><li><a href="/two-trees-inn/">Two Trees Inn</a></li><li><a href="&#xA;                  /fox-tower-hotel/">The Fox Tower</a><ul class="dropdown"><li><a href="/fox-tower-pool/">The Fox Tower Pool</a></li></ul></li></ul></li><li><a href="/reserve-your-room/">Reserve A Room</a></li><li><a href="/modify-your-reservation/">View/Cancel A Reservation</a></li><li><a href="/hotel-packages/">Hotel Packages</a></li><li><a href="http://foxwoods.pmathome.com">Shop Luxury at Home</a></li></ul></li><li><a href="/restaurants.aspx">Restaurants</a><ul class="dropdown"><li><a href="/restaurants/">Overview</a></li><li><a href="#">Fine Dining</a><ul class="dropdown"><li><a href="/VUE-24/">VUE 24</a></li><li><a href="/al-dente/">Al Dente</a></li><li><a href="/alta-strada/">Alta Strada</a></li><li><a href="/cedars/">Cedars Restaurant</a></li><li><a href="/dbp/">David Burke Prime</a></li><li><a href="/red-lantern/">Red Lantern</a></li></ul></li><li><a href="#">Casual</a><ul class="dropdown"><li><a href="/cpk/">California Pizza Kitchen</a></li><li><a href="/golden-dragon/">Golden Dragon</a></li><li><a href="/hard-rock-cafe/">Hard Rock Cafe</a></li><li><a href="/high-rollers/">High Rollers</a></li><li><a href="/juniors/">Junior&#39;s</a></li><li><a href="/matches-tavern/">Matches Tavern</a></li><li><a href="https://www.foxwoods.com/pequot-cafe/">Pequot Café</a></li><li><a href="/grill-at-two-trees/">The Grill At Two Trees</a></li><li><a href="/scorpion-bar/">The Scorpion Bar</a></li><li><a href="/veranda-cafe/">Veranda Café</a></li></ul></li><li><a href="#">Quick Service</a><ul class="dropdown"><li><a href="/benjerry.aspx">Ben &amp; Jerry&#39;s</a></li><li><a href="/boarsheaddeli.aspx">Boar&#39;s Head Deli</a></li><li><a href="/cake-by-franck/">Cake by Franck</a></li><li><a href="/dunkindonuts.aspx">Dunkin&#39; Donuts</a></li><li><a href="/einsteinbrosbagels.aspx">Einstein Bros Bagels</a></li><li><a href="/fadabakery.aspx">Fay Da Bakery</a></li><li><a href="/fuddruckers.aspx">Fuddruckers</a></li><li><a href="/gelatocafe.aspx">Gelato Cafe</a></li><li><a href="/kingclubs.aspx">King of Clubs Snack Kitchen</a></li><li><a href="/lucky7.aspx">Lucky 7</a></li><li><a href="/noodles.aspx">Noodles</a></li><li><a href="/panerabread.aspx">Panera Bread</a></li><li><a href="/pequotbay.aspx">Pequot Bay Seafood</a></li><li><a href="/phillipsseafood.aspx">Phillips Seafood</a></li><li><a href="/reginapizzeria.aspx">Regina Pizzeria</a></li><li><a href="/sbarro.aspx">Sbarro</a></li><li><a href="/starbucks.aspx">Starbucks</a></li><li><a href="/subway.aspx">Subway</a></li><li><a href="/thegrillshack.aspx">The Grill Shack</a></li></ul></li><li><a href="#">Drinks &amp; Nightlife</a><ul class="dropdown"><li><a href="/atrium.aspx">Live@Atrium Bar and Lounge</a></li><li><a href="/cedars-lounge/">Cedars Restaurant Bar &amp; Lounge</a></li><li><a href="/centrale/">Centrale Fox Tower</a></li><li><a href="/halo-bar/">Halo Bar</a></li><li><a href="/hard-rock-cafe/">Hard Rock Cafe</a></li><li><a href="/high-rollers/">High Rollers</a></li><li><a href="/shrine/">Shrine</a></li><li><a href="/scorpion-bar/">The Scorpion Bar</a></li></ul></li><li><a href="/festival-buffet/">Festival Buffet</a></li><li><a href="/find-a-restaurant/">Find A Restaurant</a></li><li><a href="/reserve-your-table/">Reserve A Table</a></li><li><a href="/viewyourtable.aspx">View/Cancel A Reservation</a></li><li><a href="/dining/">Restaurant Specials</a></li></ul></li><li><a href="/shopping.aspx">Shopping</a><ul class="dropdown"><li><a href="/shopping/">Overview</a></li><li><a href="/tanger/">Tanger Outlets at Foxwoods</a></li><li><a href="/purchase-gift-cards/">Buy A Gift Card</a></li><li><a href="/gift-card-balance/">Gift Card Balance</a></li><li><a href="/shopping-specials/">Shopping Specials</a></li><li><a href="http://foxwoods.pmathome.com">Shop Luxury at Home</a></li></ul></li><li><a href="/shows.aspx">Entertainment</a><ul class="dropdown"><li><a href="/shows/">Find A Show</a></li><li><a href="/comedy/">Comedy</a></li><li><a href="&#xA;                  /nightlife.aspx">Nightlife</a><ul class="dropdown"><li><a href="/atrium.aspx">Live@Atrium Bar &amp; Lounge</a></li><li><a href="/cedars-lounge/">Cedars Restaurant Bar &amp; Lounge</a></li><li><a href="/centrale/">Centrale Fox Tower</a></li><li><a href="/halo-bar/">Halo Bar</a></li><li><a href="/hard-rock-cafe/">Hard Rock Cafe</a></li><li><a href="/high-rollers/">High Rollers</a></li><li><a href="/shrine/">Shrine</a></li><li><a href="/scorpion-bar/">The Scorpion Bar</a></li><li><a href="/VUE-24/">VUE 24</a></li></ul></li><li><a href="&#xA;                  /liquid-sundays/">Liquid Sundays</a><ul class="dropdown"><li><a href="/liquid-sunday-schedule/">Schedule of Events</a></li></ul></li><li><a href="https://www.foxwoods.com/revolution-rock-festival/">Revolution Rock Festival</a></li><li><a href="/entertainmentspecials.aspx">Specials</a></li><li><a href="https://www.foxwoods.com/musicbox.aspx">The Music Box</a></li></ul></li><li><a>Spa</a><ul class="dropdown"><li><a href="/spa/">Overview</a></li><li><a href="/norwich-spa-at-foxwoods/">Norwich Spa at Foxwoods</a></li><li><a href="/g-spa/">G Spa</a></li><li><a href="/spa-specials/">Spa Specials</a></li></ul></li><li><a href="/golf.aspx">Golf</a><ul class="dropdown"><li><a href="/golf/">Overview</a></li><li><a href="http://www.lakeofisles.com/golf-packages.html">Golf Packages</a></li></ul></li><li><a>Activities</a><ul class="dropdown"><li><a href="/high-rollers/">Bowling</a></li><li><a href="/arcade/">Arcade</a></li><li><a href="/pequot-trails/">Pequot Trails</a></li><li><a href="http://www.pequotmuseum.org">Pequot Museum</a></li><li><a href="/activities-specials/">Specials</a></li></ul></li><li><a href="http://www.foxwoodsmeetingsandevents.com">Meetings</a></li><li><a>About Foxwoods</a><ul class="dropdown"><li><a href="/aboutus.aspx">About Us</a></li><li><a href="/careers/">Careers</a></li><li><a href="/contactus.aspx">Contact Us</a></li><li><a href="/disclaimers.aspx">Disclaimer</a></li><li><a href="/email-sign-up/">Email Sign Up</a></li><li><a href="/pequotoutpost/">Pequot Outpost</a></li><li><a href="/pressroom.aspx">Press Releases</a></li><li><a href="/privacypolicy.aspx">Privacy Policy</a></li><li><a href="/search.aspx">Search</a></li><li><a href="/tribalgamingcommission.aspx">Tribal Gaming Commission</a></li><li><a href="&#xA;                  /gettinghere.aspx">Getting Here</a><ul class="dropdown"><li><a href="/bycar.aspx">By Car</a></li><li><a href="/bus/">By Bus</a></li><li><a href="/bytrain.aspx">By Train</a></li><li><a href="/byferry.aspx">By Ferry</a></li></ul></li><li><a href="#">FAQ</a><ul class="dropdown"><li><a href="/faqgeneral.aspx">General</a></li><li><a href="/faqaccommodations.aspx">Accommodations</a></li><li><a href="/faqgaming.aspx">Gaming</a></li><li><a href="/faqfoxwoodsrewardcard.aspx">Foxwoods Rewards Card</a></li></ul></li><li><a href="#">Responsible Gambling</a><ul class="dropdown"><li><a href="/responsiblegambling.aspx">Responsible Gambling</a></li><li><a href="/selfexclusion.aspx">Self Exclusion</a></li></ul></li></ul></li><li class="mobile"><a>Social</a><ul class="dropdown"><li><a href="http://www.facebook.com/pages/Foxwoods-Resort-Casino/91920463745">Find Us On Facebook</a></li><li><a href="https://twitter.com/intent/follow?original_referer=http%3A%2F%2Fwww.foxwoods.com%2Fdefault.aspx&amp;amp;region=follow_link&amp;amp;screen_name=FoxwoodsCT&amp;amp;tw_p=followbutton">Follow Us On Twitter</a></li><li><a href="http://www.instagram.com/foxwoods">Instagram</a></li><li><a href="http://www.pinterest.com/foxwoodsct/">Pinterest</a></li><li><a href="/snapchat/">Snapchat</a></li></ul></li><li class="mobile"><a>Getting Here</a><ul class="dropdown"><li><a href="/bycar.aspx">By Car</a></li><li><a href="/bus/">By Bus</a></li><li><a href="/bytrain.aspx">By Train</a></li><li><a href="/byferry.aspx">By Ferry</a></li></ul></li><li class="mobile"><a>Foxwoods Rewards</a><ul class="dropdown"><li><a href="/rewardslanding.aspx">Program Benefits</a></li><li><a href="/rewardssignup.aspx">New Account Sign Up</a></li></ul></li><li id="search-container"><div><input autocomplete="on" id="txtSearch" onfocus="javascript:this.select();" onclick="javascript:this.select();" name="search" value="Search" class="search-box"><a href="#" class="search-button" onclick="javascript:document.location='/search.aspx?searchtext='+document.getElementById('txtSearch').value;"><span class="fa fa-search fa-md"></span></a></div></li></ul></div></div>

    
    <div class="parallax-container" data-parallax="scroll" data-position="top" data-bleed="10" data-image-src="/images/aboutfoxwoods-bg.jpg" data-natural-width="1900" data-natural-height="1648" data-speed="0.5"></div>


    <div id="top">
        <div id="mobile-foxwoods-rewards" class="clearfix">
            <a href="/rewardslanding.aspx" title="Foxwoods Rewards"><img src="/images/mobile/foxwoods-rewards.png"/></a>
            
            <div class="rewards-panel">
                <div id="non-member">
                    <a href="/rewardssignup.aspx" class="call-to-action">Sign Up</a>
                    <a href="/rewardssignin.aspx" class="call-to-action">Login</a>
                </div>
            </div>
            
        </div>
        <div id="mobile-logo"><a href="/"><img src="/images/mobile/logo-hi.jpg" /></a></div>
    </div>

    <div class="page-container">
        <div id="col-left" class="col">
            <div id="booking-engine">
                <div id="inner-container">
                    <div id="logo-container">
                        <a href="/"><img id="logo" src="/images/booking-engine/logo.png" /></a>
                        <img id="aaa" src="/images/booking-engine/aaa_4diamonds.png" />
                    </div>
                    <div class="date-picker">
                        <input type="text" id="txtCheckIn" value="Check In" class="datepicker-textbox" />
                    </div>
                    <div class="cta">
                        <a id="btnBookIt" href="/reserveyourroom.aspx"><img src="/images/booking-engine/book-it-button.png" /></a>
                    </div>
                    <div class="nofloat"></div>
                    <div class="date-picker">
                        <input type="text" id="txtCheckOut" value="Check Out" class="datepicker-textbox" />
                    </div>
                    <div class="cta">
                        <a href="/vieworcancelyourroom.aspx"><img src="/images/booking-engine/view-room-button.png" /></a>
                    </div>
                    <div class="nofloat"></div>
                    <div id="packages-and-promos" class="divider">
                        <div id="left-promo">
                            
        <div class="promo-img">
            <a href="/golf-package/"><img height="62" src="/uploadedImages/Content/Specials/Hotel/Golf Package Thumbnail.png" /></a>
        </div>
        <div class="promo-copy">
            <div class="title">Golf Hotel Packages</div>
            <div class="subtitle"></div>
            <a href="/hotel-packages/">See All Packages</a>
        </div>
    


                        </div>
                        <div id="right-promo">
                            
        <div class="promo-img">
            <a href="/promotions.aspx"><img height="62" src="/images/booking-engine/all-promos.png" /></a>
        </div>
        <div class="promo-copy">
            <div class="title"></div>
            <div class="subtitle">Stay More. Pay Less. Be Rewarded!</div>
            <a href="/promotions.aspx">See All Promotions</a>
        </div>
    


                        </div>
                        <div class="nofloat"></div>
                    </div>
                    <div id="offers-and-gift-cards" class="divider">
                        <a href="/eventcalendar.aspx" class="call-to-action">TODAY AT FOXWOODS</a>
                        <a href="/emailsignup.aspx" class="call-to-action">EMAIL OFFERS & UPDATES</a>
                        
    
    


                    </div>
                </div>
            </div>
            
    
    
    <div class="nofloat spacer"></div>

    <div class="left-pulldown">
        <div class="rounded-box-out">
            <div class="rounded-box-in">
                

<h2>Resort UPDATES</h2>
<ul class="pulldown">
    
</ul>

            </div>
        </div>
        <div class="pulldown-expand">&nbsp;</div>
    </div>
    

        </div>
        <div id="col-center" class="col">
            
    
            <div id="hero">
                <h1>Privacy Policy</h1>
                <div class="breadcrumb"><ul class="clearfix"><li><a target="_self" class="hilite" href="/privacypolicy.aspx">Privacy Policy</a></li><li><a target="" class="normal" href="javscript:0">About Foxwoods</a></li><li><a href="/" title="Home" class="normal">Home</a></li></ul><span class="nofloat">&nbsp;</span></div>
                
            </div>
            <div class="subcopy">
                <h2></h2>
                <p><span>Foxwoods Resort Casino ("Foxwoods", "us" or "we") respects the privacy of all visitors to our Web site. We maintain this site to market and promote the many services and amenities offered by Foxwoods. In order to assist us in better serving our customers, we may collect certain anonymous information automatically, and may require certain personal information, including financial information, if you are making reservations or purchases or requesting information from us through this Web site. This Privacy Statement outlines the type of information that Foxwoods may collect and the manner in which such information will be used. All efforts have been made to ensure the accuracy of the information provided herein, however Foxwoods makes no warranties or representations as to the accuracy of the information supplied herein.</span></p><p>Foxwoods is a wholly-owned instrumentality of the Mashantucket Pequot Tribal Nation.</p><p></p><p><strong>Anonymous Information Automatically Collected</strong><br />We may collect and use certain information about your use of this Web site that does not personally identify you. This information includes your IP address and other information gathered through the use of "cookies". Cookies enable us to gather anonymous click-stream information, such as pages viewed, the date and time the site was entered, the type of internet browser you are using, the type of computer you are using, and the domain name of the Web site from which you linked to our site. This anonymous information does not personally identify you, and is gathered to help us develop generalized customer profiles to better serve our customers.</p><p>If you do not want information collected through the use of cookies, most browsers allow users to accept or reject cookies. If you set your browser not to accept cookies, you may not be able to take advantage of certain services that we offer on our Web site.</p><p></p><p><strong>Personal Information Collected<br /></strong>You may be required to supply personal information such as your name, address, telephone number and/or email address in order to receive certain information, participate in promotions, make reservations or place orders. A personal identification number provided by Foxwoods may be required to access certain personal information. By providing personal information, you agree to be placed on marketing lists and may receive promotional materials from Foxwoods from time to time.</p><p>Your personal information is stored on secure servers that are not accessible by third parties. We provide you with the capability to transmit your personal information via secured and encrypted channels if you use a similarly equipped Web browser. Personally identifiable information is stored on our Web server and is not publicly accessible. Further, personally identifiable information is only accessed by Foxwoods employees on a "need to know" basis. In order to ensure that all of your personal information is correct and up to date, you will be able to update your personal information. While we strive to protect your personal information, Foxwoods cannot ensure or warrant the security of any information you transmit to us, and you do so at your own risk.</p><p></p><p><strong>Age Policy<br /></strong>Foxwoods Resort Casino has no intention of collecting any personal information (such as name, address, telephone number or email address) from individuals under 21 years of age. Any offers, promotions or contests we conduct are not open to persons younger than 21 years of age. We reserve the right to verify age at any time for any reason and may reject a person who does not have valid age identification as requested by Foxwoods. If a child has provided us with personal information, a parent or guardian of that child should contact us at the email address or phone number listed at the bottom of this Privacy Statement to identify the information that must be removed.</p><p></p><p><strong>Sharing Information<br /></strong>The personal information you provide may be shared with other businesses owned or managed by the Mashantucket Pequot Tribe. These entities include The Spa at Norwich Inn, the Mashantucket Pequot Museum &amp; Research Center and PRxN. We do not anticipate that your personal information will be shared or divulged to any non-affiliated third parties by us, provided such disclosure may be necessary to: (a) comply with a court order or other legal process or other legal requirements of any governmental authorities, including but not limited to authorities which regulate gaming activities; (b) protect and defend the rights or property of Foxwoods.</p><p>This Web site contains links to other Web sites. We are not responsible for the privacy practices or the content of such sites. We have no control over its use and you should exercise caution when deciding to disclose your personal information.</p><p></p><p><strong>Changes to Privacy Statement<br /></strong>We reserve the right, at any time and without notice, to add to, change, update or modify this Privacy Statement simply by posting such change, update or modification on the Web site. Any such change, update or modification will be effective immediately upon posting on the Web site.</p><p></p><p><strong>Copyright</strong><br />All contents of this Web site are: Copyright © 2001-2015 Foxwoods Resort Casino, Route 2, Mashantucket, Connecticut, 06338. All rights not expressly granted herein are reserved.</p>

                

                


    
<script src="/bundles/foxwoods/dialog.js?v=q7LTvoI0kTmbNulPMNK5jAEu62RSwseVkpMCvojjIBU1"></script>

    <link rel="stylesheet" href="/plugins/jquery-colorbox/css/colorbox.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $('.iframe.cboxElement').colorbox({ iframe: true, width: '80%', height: '80%' });
            $('.inline.cboxElement').colorbox({ inline: true, width: '80%', height: '80%' });

            $(window).resize(function () {
                var x = $(window).width() * .8;
                var y = $(window).height() * .8;

                $('.cboxElement').colorbox({ width: x, height: y });
            });
        });
    </script>



                <div id="ctl00_ctl00_cphCenterCol_divModalLinks"></div>
                <div id="ctl00_ctl00_cphCenterCol_divSearch"></div>
                
                
            </div>
        

    
            <div id="ctl00_ctl00_cphCenterCol_divAccordion" class="clearfix"></div>
            <div id="ctl00_ctl00_cphCenterCol_divImageGallery" class="clearfix"></div>
        
            
    

        </div>
        <div id="col-right-top" class="col">
            <div id="login-panel">
                
                        <a href="/rewardssignup.aspx"><img src="/images/rewards-promo/sign-up-button.png" /></a>
                        <a href="/rewardssignin.aspx"><img src="/images/rewards-promo/login-button.png" /></a>
                    
            </div>
            <a id="online-casino" href="http://online.foxwoods.com"><img src="/images/foxwoods-online-logo-new.png" /></a>
        </div>
        <div id="col-right" class="col">
            
                    <div id="non-member-panel">
                        <div id="rewards-promo-container">
                            <div id="rewards-promo">
                                
        <div id="promo">
            <h3>Foxwoods Rewards</h3>
            <div class="copy">Win Free meals, backstage passes, hotel stays and more!</div>
            <div class="button"><a href="/rewardslanding.aspx" title="Be Rewarded"></a><a href="/rewardslanding.aspx" title="Rewards"><img src="/uploadedImages/Content/Rewards/be-rewarded-button.png" alt="Be Rewarded Button" title="Be Rewarded Button" border="0" /></a></div>
        </div>
    


                            </div>
                        </div>
                    </div>
                
            
    
    
    
    
    <div class="nofloat spacer"></div>
    
    <div id="ctl00_ctl00_cphRightCol_cphRightCol_divOutletAd" class="right-ad rounded-box-out">
        <div class="rounded-box-in">
            
        </div>
    </div>


        </div>
        <div class="nofloat"></div>
        
    <div class="nofloat"></div>
    <div id="footer-left" class="col">&nbsp;</div>
    <div id="footer-center" class="col"> 
        

    <ul id="footer-slider">
    
        <li><a href="/rewardslanding.aspx" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl01_lnkSlide"><img src="/uploadedImages/Content/Footer/RewardsFooter.png" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl01_imgSlide" /></a></li>
    
        <li><a href="/musicbox.aspx" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl02_lnkSlide"><img src="/uploadedImages/Content/Footer/MusicBoxFooter.png?n=4053" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl02_imgSlide" /></a></li>
    
        <li><a href="https://www.foxwoods.com/hotel-packages/" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl03_lnkSlide"><img src="/uploadedImages/Content/Footer/foxwoods-hotel-packages-deals-discounts.png" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl03_imgSlide" /></a></li>
    
        <li><a href="https://www.foxwoods.com/uploadedFiles/Pages/Restaurants/VUE-24-Brunch-Menu%202.2.2016.pdf" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl04_lnkSlide"><img src="/uploadedImages/Content/Footer/VUE-24-Sky-Brunch-Sunday-Foxwoods.png" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl04_imgSlide" /></a></li>
    
        <li><a href="http://www.foxwoods.com/liquid-sundays/" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl05_lnkSlide"><img src="/uploadedImages/Content/Footer/Liquid-Sundays_Foxwoods(1).png" id="ctl00_ctl00_cphFullWidth_ctlFooterSlider_rptHeroSlides_ctl05_imgSlide" /></a></li>
    
    </ul>
    

 
    </div>
    <div id="footer-right" class="col">&nbsp;</div>

    <div class="nofloat"></div>


    </div>
    
    <div id="footer" class="clearfix">
        <div class="col right-border">
            <div class="footer-menu">
                <h2>About Foxwoods</h2><ul><li><a href="/aboutus.aspx">About Us</a></li><li><a href="/contactus.aspx">Contact Us</a></li><li><a href="/gettinghere.aspx">Getting Here</a></li><li><a href="/mobileapp.aspx">Mobile App</a></li><li><a href="/uploadedFiles/Pages/About_Foxwoods/resort_map.pdf">Property Map</a></li><li><a href="/pressroom.aspx">Press Releases</a></li><li><a href="/faqgeneral.aspx">FAQ</a></li><li><a href="/careers/">Careers</a></li><li><a href="/responsiblegambling.aspx">Responsible Gambling</a></li><li><a href="/pequot-trails/">Pequot Trails</a></li><li><a href="/disclaimers.aspx">Disclaimer</a></li><li><a href="/privacypolicy.aspx">Privacy Policy</a></li></ul>
            </div>
        </div>
        <div class="col right-border">
            <div class="footer-menu">
                <h2>About the Mashantucket Pequot Tribal Nation</h2><ul><li><a href="http://www.mptn-nsn.gov">Tribal Government</a></li><li><a href="/tribalgamingcommission.aspx">Tribal Gaming Commission</a></li><li><a href="/GivingCampaign/">Giving Campaign</a></li></ul>
                <h2>Tribal Businesses</h2><ul><li><a href="http://www.pequotmuseum.org">Mashantucket Pequot Museum &amp; Research Center</a></li><li><a href="/pequotoutpost/">Pequot Outpost</a></li><li><a href="http://www.thespaatnorwichinn.com">The Spa at Norwich Inn</a></li></ul>
            </div>
        </div>
        <div class="col">
            <div class="footer-menu">
                <a id="social-link"><img src="/images/home/social.png"  /></a>
                <div id="social-panel">
                    <div id="social-container">
                        <h3>Latest Posts</h3><a id="close">close</a>
                        <div id="wall">
	                        <div id="social-stream"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <ul id="social-menu">
                    <li><a href="http://www.facebook.com/pages/Foxwoods-Resort-Casino/91920463745" target="_blank"><img src="/images/footer/facebook.png" /> <span>Like Us On Facebook</span></a></li>
                    <li><a href="https://twitter.com/FoxwoodsCT" target="_blank"><img src="/images/footer/twitter.png" /> <span>Follow Us On Twitter</span></a></li>
                    <li><a href="https://www.youtube.com/user/FoxwoodsOfficial" target="_blank"><img src="/images/footer/youtube.png" /> <span>Watch Us On YouTube</span></a></li>
                    <li><a href="http://www.instagram.com/foxwoods" target="_blank"><img src="/images/footer/new_instagram.png" /> <span>Follow Us On Instagram</span></a></li>
                    <li><a href="http://www.pinterest.com/foxwoodsct/" target="_blank"><img src="/images/footer/pinterest.png" /> <span>Follow Us On Pinterest</span></a></li>
                    <li><a href="/snapchat/"><img src="/images/footer/snapchat.png" /> <span>Add Us On Snapchat</span></a></li>
                    <li><a href="/emailsignup.aspx"><img src="/images/footer/envelope.png" /> <span>Register for Offers</span></a></li>
                    <li><a href="http://online.foxwoods.com" title="Online Casino" target="_blank"><img src="/images/footer/mptn.png" /> <span>Online Casino</span></a></li>
                </ul>
            </div>
        </div>
        <div class="col" id="right"> 
            <a href="http://www.google.com/maps/place/Foxwoods+Resort+Casino/@41.4737961,-71.9634586,17z/data=!3m1!4b1!4m2!3m1!1s0x89e675845275a815:0x2be75433d441c13f" title="Go to Google Maps" target="_blank">
                <img src="/images/map.jpg" alt="Google Map" id="google-map"/>
            </a>
            <div id="property-address" class="nofloat">350 Trolley Line Boulevard, Mashantucket, CT 06338<br />1-800-369-9663</div>
        </div>
        <div id="property-address" class="nofloat" style="text-align:center;">Copyright ©2016 Foxwoods Resort Casino. All rights reserved. <a href="/privacypolicy.aspx">Privacy Policy</a>.</div>
    </div>
    
    

    


    


        <!--
        <script type="text/javascript">

            window._mt_ready = function () {
                if (typeof (MyThings) != "undefined") {
                    MyThings.Track({
                        EventType: MyThings.Event.Visit,
                        Action: "300"
                    });
                }
            };
            (function (atok, host) {
                window.mtHost = (("https:" == document.location.protocol) ? "https" :
        "http") + "://" + host;
                window.mtAdvertiserToken = atok;
                var mt = document.createElement("script");
                mt.type = "text/javascript";
                mt.async = true;
                mt.src = window.mtHost + "/c.aspx?atok=" + atok;
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(mt, s);
            })("2919-100-us", "rainbow-us.mythings.com");</script> 
            -->
            <!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Foxwoods Universal Floodlight
URL of the webpage where the tag is expected to be placed: http://TBD
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/10/2015
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://4728623.fls.doubleclick.net/activityi;src=4728623;type=unive0;cat=foxwo0;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://4728623.fls.doubleclick.net/activityi;src=4728623;type=unive0;cat=foxwo0;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
   
    

<!-- Twitter universal website tag code -->
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('nuosq', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nuosq&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nuosq&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<!-- End Twitter universal website tag code -->


<script type="text/javascript">
var customEventsOnLoad = window.onload;
window.onload = function()
{
    if (customEventsOnLoad) customEventsOnLoad();
    
}
</script>

<script src="https://use.typekit.net/eiq7aqb.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

    <script src = "/bootstrap/js/bootstrap.min.js"></script>
</form>
</body>
</html>
