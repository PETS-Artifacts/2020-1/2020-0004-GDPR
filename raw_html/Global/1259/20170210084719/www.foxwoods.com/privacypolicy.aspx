

<!DOCTYPE html>
<html lang="en" >
<head><meta id="ctl00_ctl00_meta" http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="apple-mobile-web-app-title" content="Foxwoods" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta name="com.silverpop.brandeddomains" content="www.pages02.net,webtixs.foxwoods.com,www.foxwoods.com" /><link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icons/apple-touch-icon-144x144-precomposed.png" /><link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icons/apple-touch-icon-114x114-precomposed.png" /><link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icons/apple-touch-icon-72x72-precomposed.png" /><link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icons/apple-touch-icon-precomposed.png" /><link rel="shortcut icon" href="/images/favicon.ico" /><meta name="apple-itunes-app" content="app-id=920603842" />
    <!--[if lt IE 9]>
        <script lang="javascript">
            if (!String.prototype.trim) {
              String.prototype.trim = function () {
                return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
              };
            }
        </script>
    <![endif]-->
    <link href="/bundles/jquery.css?v=I5yCEZAD2fgdIGudBjbIbhPW72Q4rs1hWM-SPn4mNuE1" rel="stylesheet"/>
<link href="/bundles/plugins.css?v=q2sB3hiJHLG3wp9vcCfiYt-BgPnVxm6EqV9fMKFg2q81" rel="stylesheet"/>
<link href="/bundles/foxwoods.css?v=hziDQFwfG4S4ME1MXaEM6XHwpDXnQGRNKcJZsWRhACw1" rel="stylesheet"/>
<script src="/bundles/jquery.js?v=yOzi2ftkJfxsmAWw7nhoLoQt9XgjW_1oJSUgj4UH5No1"></script>
<script src="/bundles/foxwoods.js?v=RTjMhN0LqW7ZuiAXdPyMN56X7Eg6YQepbLWLwTSVHw81"></script>
<script src="/bundles/plugins.js?v=uhrtkZD5R0PgQk_NSfDZ7B7q_z5jKX4M25sbYMofeNI1"></script>

<script type="text/javascript">

      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-26275960-1', 'auto');
      ga('send', 'pageview');
    
      var _yourip = '207.241.229.237';


    //if (chkit(_yourip)) {
    //    (function () {
    //        var sp = document.createElement('script'); sp.type = 'text/javascript'; sp.async = true;
    //        sp.src = ('https:' == document.location.protocol ? 'https://secure' : 'http://ib') + '.adnxs.com/seg?add=657413&t=1';
    //        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sp, s);
    //    })();
    //}
    
</script>

<script type="text/javascript" id="DoubleClickFloodlightTag">
function Flood(type, cat) {
        var axel = Math.random()+"";
        var a = axel * 10000000000000000;
        var spotpix = new Image();
        spotpix.src="https://5889950.fls.doubleclick.net/activityi;src=5889950;type=" + type + ";cat=" + cat + ";dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;ord=" + a + "?";
}
</script>


<title>About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)</title><meta name="title" content="About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)" />
<meta name="description" content="Foxwoods Resort Casino features deluxe accommodations, fine dining, a wide variety of entertainment attractions and shopping." />
<meta name="keywords"    content="connecticut casinos,resorts,hotels,mashantucket pequot,ledyard ct,family vacations,business meeting,luxury travel,romantic getaway,reservations,golf,best lodging,accommodations,accomodations,spa,suites,vacation,getaways,discount,rooms,area resort" />
<meta http-equiv="imagetoolbar" content="no" /><!-- disable IE image tool bar -->

<!-- Google Authorship and Publisher Markup -->
<link rel="author" href="https://plus.google.com/110190905165923896755/posts"/>
<link rel="publisher" href="https://plus.google.com/110190905165923896755"/>

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)">
<meta itemprop="description" content="Foxwoods Resort Casino features deluxe accommodations, fine dining, a wide variety of entertainment attractions and shopping.">


<!-- Twitter Card data -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@foxwoodsct">
<meta name="twitter:title" content="About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)">
<meta name="twitter:description" content="Foxwoods Resort Casino features deluxe accommodations, fine dining, a wide variety of entertainment attractions and shopping.">
<meta name="twitter:image" content="http://www.foxwoods.com/images/dual_property.jpg">

<!-- Open Graph data -->
<meta property="og:title" content="About Foxwoods - Privacy Policy | Foxwoods Resort Casino in Connecticut(CT)" />
<meta property="og:type" content="article" />
<meta property="og:image" content="http://www.foxwoods.com/images/dual_property.jpg" />
<meta property="og:url" content="http://www.foxwoods.com/privacypolicy.aspx" />
<meta property="og:description" content="Foxwoods Resort Casino features deluxe accommodations, fine dining, a wide variety of entertainment attractions and shopping." />
<meta property="og:site_name" content="Foxwoods.com" />
<meta property="fb:admins" content="91920463745" />

    <link href="/bundles/foxwoods/interior.css?v=Qomy9xp_U2DJFpv3_NUVRYRMPKutuZOViFOpxw6oFB01" rel="stylesheet"/>
<script src="/bundles/foxwoods/interior.js?v=eMupRhjSeAd0C6e4p6HhzPQTqqK7QI6LgDSQL0q1OZs1"></script>

    

    <!--[if lt IE 9]>
    <script src="/js/selectivizr.js"></script>
    <script src="/js/respond.js.min"></script>
    <![endif]-->
    <script type="text/javascript">
        var patronAuth = 'false';
    </script>
<title>

</title></head>
<body>
    <form method="post" action="/privacypolicy.aspx" id="aspnetForm" onsubmit="return true;">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0eEgnGI0rNORlRsXvJGvsqU+x8NvkBMxE8BvOh0qX79iu8JgCiOCJkK+HtAHlFqEbOPe8+OiO1kO7vbp/Y32EE8S1GZPNUKEX4Zk96PpgEsDQVPOx3/X4Osqpa0r/DVQeYknydxDSRHBvArO0PG0RQXW4gOC7f4PsQkpXbKyNA+1FqKvmPqWhcY9tZEC3WoMFvTuaAbWVR2yGCP9RjHKhPhRJF1H9atb+LOZqC1QaF1xGHBmo7pUypN/lIp6Jz7hyVR57fmgTTqmJGETJC07PGrIbFEcb5TBp3wQgraDC+IKFVDETHvtFU317sTIZ64sjJ9ZYcF4q+a2/rpJwD6nVa4Haa9tG35uNCKw7b/mGyAgF1MaLUm/9m1GF+MiZ+bD9lqUA+QqjPLseLw7OnWM0gCf1ihd4zx5KPP+qsgTqzhf58gdifiJhf6R2ktUfNsIzvp5mHPGxOTGsBrW1Olh1Bu86mtK5R/Gn/YOXHoocw9nnOJGqOoJREDkpFKBCVQadvmxcESZkONZcrMnU7UZnCSelOGErskSYSPAoYkR/mN4l0On1RDCFrHMQ9ZM4HoRrME2eIBokTV4+T+ByGYCvNZt2Jk=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F187B426" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="sZ69GRii+9kBWZ7+UHWF15DkbpE38roUptvhTchxjsuw98ELhX7QoTa92OjP4pLXiHkO49bJKd3y6Wmr53ae0JiU31y5KqxT2zshPMGeRq3AohicIiXYEQVWfweW7SLyJUPNFriqs9kppR5QghXC6hdXIh3ye26mmKjt6X0biyQh0nb2Sd8lySh/Lj32KGxWdziS01rlrWZ33xVvQSQUaqefVYs=" />
</div>
    <div id="mobile-cta">
        
        
        
        
    </div>

    <div id="mobile-logo-panel-container">
            <a href="/" title="Foxwoods Resort Casino Home"><img src="/images/2016/top-25th-logo.jpg" alt="Foxwoods 25th Anniversary Logo" /></a>
    </div>
 
    <div id="logo-panel-container">
        <div id="logo-panel">
            <div>
                <div class="sm-ticker">
                    <div id="now" style="visibility:hidden;">
                        <div id="ticker" class="ticker-container">
                            <div id="ticker-slider">
                                <div class="item" id="foxperx-winners-container">
                                    <a href="/fox-perx/" title="Go to FoxPerx">
                                    FoxPerx Winners since March 1
                                    <ul id="foxperx-winners"></ul>
                                    </a>
                                </div>
                                <div class="item" id="slot-payout-container">
                                    <a href="/slots/" title="Go to Slots">
                                    Slot Payout
                                    <ul id="slot-payout"></ul>
                                    </a>
                                </div>
                                <div class="item" id="keno-jackpot-container">
                                    <a href="/keno/" title="Go to Keno">
                                    Keno Jackpot
                                    <ul id="keno-jackpot"></ul>
                                    </a>
                                </div>
                                <div class="item" id="table-games-progressive-container">
                                    <a href="/tablegames/" title="Go to Table Games">
                                    Table Games Progressive Mega Jackpot
                                    <ul id="table-games-progressive"></ul>
                                    </a>
                                </div>
                                <div class="item" id="black-jack-progressive-container">
                                    <a href="/tablegames/" title="Go to Table Games">
                                    Blackjack Match Jackpot
                                    <ul id="black-jack-progressive"></ul>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="left-options">
                    <ul>
                        <li><a href="/eventcalendar.aspx" title="See what's happening today at Foxwoods"><i class="fa fa-calendar"></i> Calendar</a></li>
                        <li><a href="/hotel-packages/" title="Hotel Packages"><i class="fa fa-bed"></i> Packages</a></li>
                        <li><a href="/promotions.aspx" title="Offers"><i class="fa fa-tags"></i> Offers</a></li>
                    </ul>
                </div>
            </div>
            <div id="foxwoods-logo"><a href="/" title="Foxwoods Resort Casino Home"><img src="/images/2016/top-25th-logo.jpg" alt="Foxwoods 25th Anniversary Logo" id="top-25th-logo"  /></a></div>
            <div>
                <div class="search">
                  <input autocomplete="on" id="txtSearch" onfocus="javascript:this.select();" onclick="javascript:this.select();" name="search" value="Search" class="search-box"/>
                  <a href="#" class="search-button" onclick="javascript:document.location='/search.aspx?searchtext='+document.getElementById('txtSearch').value;"><span class="fa fa-search fa-md"></span></a>
                </div>
                <div class="right-options">
                    <ul>
                        <li><a href="https://foxwoods.onelink.me/1911936602?pid=Referral&c=Foxwoods_Com&af_dp=bbefox9CBC304F%3A%2F%2F&af_web_dp=http%3A%2F%2Fonline.foxwoods.com&af_force_dp=true&utm_source=FoxwoodsWebsite&utm_medium=website&utm_campaign=foxwoods_com" title="Play Online" target="_blank"><img src="/images/2016/foxwoods-online-logo-home.jpg" alt="Foxwoods Online" id="foxwoods-online-logo"/></a></li>
                        <li>
                            <a href="/rewards" title="Go to Foxwoods Rewards" class="heading">Foxwoods Rewards</a>
                            
                                    <ul>
                                        <li><a href="/rewardssignup" title="Sign up for Foxwoods Rewards"><i class="fa fa-pencil"></i> Sign Up</a></li>
                                        <li class="sign-in"><a id="sign-in" title="Log in to your Foxwoods Rewards Account"><i class="fa fa-sign-in"></i> Log In</a></li>
                                    </ul>
                                
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="account-container">
        <div class="signin" style="visibility:hidden;">
            <div>
                <img src="/images/2016/foxwoods-rewards-logo.jpg" alt="Foxwoods Rewards Logo" />
                The Foxwoods Rewards Card is the player rewards card for Foxwoods Resort Casino. With a Rewards Card, you become one of our most valued players and guests!
                <div class="learn"><a href="/rewards" title="Learn more about Foxwoods Rewards" class="btn btn-blue">Learn More</a></div>
            </div>
            <div>
                <div class="code">
                    <label for="ctl00_ctl00_rewardsNum">Foxwoods Rewards Number</label>
                    <input name="ctl00$ctl00$rewardsNum" type="text" id="ctl00_ctl00_rewardsNum" />
                    <span class="validation-error"><label id="ctl00_ctl00_errRewardsNum" for="ctl00_ctl00_rewardsNum"></label></span>
                </div> 
                <div class="code">
                    <label for="ctl00_ctl00_rewardsPin">PIN</label>
                    <input name="ctl00$ctl00$rewardsPin" type="password" id="ctl00_ctl00_rewardsPin" />
                    <span class="validation-error"><label id="ctl00_ctl00_errPIN" for="ctl00_ctl00_rewardsPin"></label></span>
                </div> 
                <div class="submit">
                    <a id="ctl00_ctl00_btnSignIn" class="btn btn-sm btn-blue" href="javascript:__doPostBack(&#39;ctl00$ctl00$btnSignIn&#39;,&#39;&#39;)">SIGN IN</a>
                    <input name="ctl00$ctl00$loginError" type="hidden" id="ctl00_ctl00_loginError" value="false" />
                </div>
            </div>
        </div>
        <div class="account " style="visibility:hidden;">
            <div>
                <div class="md-white">WELCOME,</div>
                <div class="md-white"></div>
                <img src="/images/pixel.gif" id="ctl00_ctl00_imgClub" class="club" />
                <div class="white"> Points</div>
                <div class="separator"></div>
                <div class="next-club">
                    <div class="sm-white" style="display:none;">-1  to upgrade</div>
                    <img src="/images/pixel.gif" id="ctl00_ctl00_imgNextClub" class="next-club" />
                </div>
            </div>
            <div class="links">
                <div><a class="link" href="/rewardssignin.aspx">VIEW ACCOUNT</a></div>
                <div><a class="link" href="/rewardsprofile.aspx">UPDATE PROFILE</a></div>
                <div><a class="link" href="/rewardspinchange.aspx">CHANGE PIN</a></div>
                <div><a class="link" href="/rewardsoffers.aspx">VIEW OFFERS &amp; EVENTS</a></div>
                <div><a class="link" href="/rewards-choice-benefits/">CHOICE BENEFITS</a></div>
                <div><a class="link" href="/rewardsitinerary.aspx">VIEW ITINERARY</a></div>
                <div><a class="link" href="/rewardswinloss.aspx">GET TAX FORMS</a></div>
                <div><a class="link" href="javascript:__doPostBack(&#39;ctl00$ctl00$ctl11&#39;,&#39;&#39;)">LOG OUT</a></div>
                <div class="benefits"><a href="/rewards" title="Foxwoods Rewards card member benefits." class="btn btn-blue">Member Benefits</a></div>
            </div>
        </div>
    </div>
    <div class="nofloat"></div>
    <div id="nav"><div class="foxwoods"><ul id="jetmenu" class="jetmenu black"><li class="first-row mobile"><div><ul><li><a href="https://foxwoods.onelink.me/1911936602?pid=Referral&amp;c=Foxwoods_Com&amp;af_dp=bbefox9CBC304F%3A%2F%2F&amp;af_web_dp=http%3A%2F%2Fonline.foxwoods.com&amp;af_force_dp=true&amp;utm_source=FoxwoodsWebsite&amp;utm_medium=website&amp;utm_campaign=foxwoods_com" title="Play Online" src="_blank"><img src="/images/2016/foxwoods-online-logo.png" width="150" alt="Foxwoods Online"></a></li><li><a href="/eventcalendar.aspx" title="Today at Foxwoods Calendar"><i class="fa fa-calendar"></i> Event Calendar
                  </a></li></ul></div></li><li class="mobile"><div id="search-container"><input autocomplete="on" id="txtMobileSearch" onfocus="javascript:this.select();" onclick="javascript:this.select();" name="search" value="Search" class="search-box"><a href="#" class="search-button" onclick="javascript:document.location='/search.aspx?searchtext='+document.getElementById('txtMobileSearch').value;"><span class="fa fa-search fa-lg"></span></a></div></li><li class="casino "><a href="#"><img src="&#xA;                    /uploadedImages/Top_Menu/activities(2)(1).png" alt="Casino">Casino</a><ul class="dropdown"><li><a href="&#xA;                    /slots/">Slots</a></li><li><a href="&#xA;                    /tablegames/">Table Games</a></li><li><a href="&#xA;                  /bingo/">Bingo</a><ul class="dropdown"><li><a href="http://webtixs.foxwoods.com/bingo/?key=5001-bingo&amp;amp;autosearch=1&amp;amp;venueid=2&amp;amp;hidecriteria=1">Buy Bingo Tickets</a></li><li><a href="/uploadedFiles/Pages/Game/Bingo/Foxwoods-Bingo-February-2017.pdf">February Calendar</a></li><li><a href="/uploadedFiles/Pages/Game/Bingo/Foxwoods-Bingo-March-2017.pdf">March Calendar</a></li><li><a href="/uploadedFiles/Content/Specials/Gaming/StPatricks_Bingo-Foxwoods.pdf">St. Patricks Day Bingo Celebration</a></li></ul></li><li><a href="&#xA;                    /keno/">Keno</a></li><li><a href="&#xA;                  /race-book/">Race Book</a><ul class="dropdown"><li><a href="&#xA;                    /racingresults.aspx">Racing Results</a></li><li><a href="/uploadedFiles/Pages/Game/Race_Book/Ultimate-Race-Book-Foxwoods-Racebook.pdf">Book the Book</a></li><li><a href="/uploadedFiles/Pages/Game/Race_Book/SIMO CAL - 02-2017.pdf">Simulcast Schedule</a></li></ul></li><li><a href="&#xA;                  /pokerpage.aspx">Poker</a><ul class="dropdown"><li><a href="&#xA;                    /badbeat.aspx">Bad Beat</a></li><li><a href="&#xA;                    /poker/">Tournaments</a></li><li><a href="&#xA;                    /pokerpromotions.aspx">Special Events</a></li><li><a href="/uploadedFiles/Pages/Game/Poker/JanFeb2017 Poker Calendar_8x10.pdf">Daily Event Schedule</a></li></ul></li><li><a href="/PlayVideoPoker.htm">Play Video Poker</a></li><li class="call-to-action"><a href="&#xA;                    /promotions/">Promotions</a></li><li class="call-to-action"><a href="&#xA;                    /credit-app/">Credit Application</a></li></ul></li><li class="hotels "><a href="&#xA;                /hotels.aspx"><img src="&#xA;                    /uploadedImages/Top_Menu/hotels(2).png" alt="Hotels">Hotels</a><ul class="dropdown"><li><a href="&#xA;                  /hotels.aspx">Overview</a><ul class="dropdown"><li><a href="&#xA;                    /grand-pequot-tower/">Grand Pequot Tower</a></li><li><a href="&#xA;                    /villas/">The Villas at Foxwoods</a></li><li><a href="&#xA;                    /great-cedar-hotel/">Great Cedar Hotel</a></li><li><a href="&#xA;                    /two-trees-inn/">Two Trees Inn</a></li><li><a href="&#xA;                  /fox-tower-hotel/">The Fox Tower</a><ul class="dropdown"><li><a href="&#xA;                    /fox-tower-pool/">The Fox Tower Pool</a></li></ul></li></ul></li><li><a href="&#xA;                    /hotel-packages/">Hotel Packages</a></li><li class="call-to-action"><a href="&#xA;                    /reserve-your-room/">Reserve A Room</a></li><li class="call-to-action"><a href="&#xA;                    /modify-your-reservation/">View/Cancel A Reservation</a></li></ul></li><li class="dining "><a href="&#xA;                /restaurants.aspx"><img src="&#xA;                    /uploadedImages/Top_Menu/restaurants(2).png" alt="Dining">Dining</a><ul class="dropdown"><li><a href="&#xA;                    /restaurants/">Overview</a></li><li><a href="#">Fine Dining</a><ul class="dropdown"><li><a href="&#xA;                    /VUE-24/">VUE 24</a></li><li><a href="&#xA;                    /al-dente/">Al Dente</a></li><li><a href="&#xA;                    /alta-strada/">Alta Strada</a></li><li><a href="&#xA;                    /cedars/">Cedars Restaurant</a></li><li><a href="&#xA;                    /dbp/">David Burke Prime</a></li><li><a href="&#xA;                    /red-lantern/">Red Lantern</a></li></ul></li><li><a href="#">Casual</a><ul class="dropdown"><li><a href="&#xA;                    /cpk/">California Pizza Kitchen</a></li><li><a href="&#xA;                    /guy-fieri/">Guy Fieri&#39;s Kitchen + Bar</a></li><li><a href="&#xA;                    /golden-dragon/">Golden Dragon</a></li><li><a href="&#xA;                    /hard-rock-cafe/">Hard Rock Cafe</a></li><li><a href="&#xA;                    /high-rollers/">High Rollers</a></li><li><a href="&#xA;                    /juniors/">Junior&#39;s</a></li><li><a href="&#xA;                    /matches-tavern/">Matches Tavern</a></li><li><a href="https://www.foxwoods.com/pequot-cafe/">Pequot Café</a></li><li><a href="http://www.foxwoods.com/sugar-factory/">Sugar Factory (Coming Soon)</a></li><li><a href="&#xA;                    /grill-at-two-trees/">The Grill At Two Trees</a></li><li><a href="&#xA;                    /scorpion-bar/">The Scorpion Bar</a></li><li><a href="&#xA;                    /veranda-cafe/">Veranda Café</a></li></ul></li><li><a href="#">Quick Service</a><ul class="dropdown"><li><a href="&#xA;                    /benjerry.aspx">Ben &amp; Jerry&#39;s</a></li><li><a href="&#xA;                    /boarsheaddeli.aspx">Boar&#39;s Head Deli</a></li><li><a href="&#xA;                    /cakebyfranck/">Cake by Franck</a></li><li><a href="&#xA;                    /dunkindonuts.aspx">Dunkin&#39; Donuts</a></li><li><a href="&#xA;                    /einsteinbrosbagels.aspx">Einstein Bros Bagels</a></li><li><a href="&#xA;                    /fadabakery.aspx">Fay Da Bakery</a></li><li><a href="&#xA;                    /fuddruckers.aspx">Fuddruckers</a></li><li><a href="&#xA;                    /gelatocafe.aspx">Gelato Cafe</a></li><li><a href="&#xA;                    /lucky7.aspx">Lucky 7</a></li><li><a href="&#xA;                    /noodles.aspx">Noodles</a></li><li><a href="&#xA;                    /panerabread.aspx">Panera Bread</a></li><li><a href="&#xA;                    /pequotbay.aspx">Pequot Bay Seafood</a></li><li><a href="&#xA;                    /phillipsseafood.aspx">Phillips Seafood</a></li><li><a href="&#xA;                    /reginapizzeria.aspx">Regina Pizzeria</a></li><li><a href="&#xA;                    /sbarro.aspx">Sbarro</a></li><li><a href="&#xA;                    /starbucks.aspx">Starbucks</a></li><li><a href="&#xA;                    /subway.aspx">Subway</a></li><li><a href="&#xA;                    /thegrillshack.aspx">The Grill Shack</a></li></ul></li><li><a href="#">Drinks &amp; Nightlife</a><ul class="dropdown"><li><a href="&#xA;                    /atrium.aspx">Live@Atrium Bar and Lounge</a></li><li><a href="&#xA;                    /cedars-lounge/">Cedars Restaurant Bar &amp; Lounge</a></li><li><a href="&#xA;                    /centrale/">Centrale Fox Tower</a></li><li><a href="&#xA;                    /halo-bar/">Halo Bar</a></li><li><a href="&#xA;                    /hard-rock-cafe/">Hard Rock Cafe</a></li><li><a href="&#xA;                    /high-rollers/">High Rollers</a></li><li><a href="&#xA;                    /shrine/">Shrine</a></li><li><a href="&#xA;                    /scorpion-bar/">The Scorpion Bar</a></li><li><a href="http://www.foxwoods.com/vue-24/">VUE 24</a></li></ul></li><li><a href="&#xA;                    /festival-buffet/">Festival Buffet</a></li><li><a href="&#xA;                    /dining/">Restaurant Specials</a></li><li class="call-to-action"><a href="&#xA;                    /reserve-your-table/">Reserve A Table</a></li><li class="call-to-action"><a href="&#xA;                    /viewyourtable.aspx">View/Cancel A Reservation</a></li></ul></li><li class="shopping "><a href="&#xA;                /shopping.aspx"><img src="&#xA;                    /uploadedImages/Top_Menu/shopping(2).png" alt="Shopping">Shopping</a><ul class="dropdown"><li><a href="&#xA;                    /shopping/">Overview</a></li><li><a href="&#xA;                    /tanger/">Tanger Outlets at Foxwoods</a></li><li><a href="&#xA;                    /shopping-specials/">Shopping Specials</a></li><li class="call-to-action"><a href="&#xA;                    /purchase-gift-cards/">Buy A Gift Card</a></li><li class="call-to-action"><a href="&#xA;                    /gift-card-balance/">Gift Card Balance</a></li><li class="call-to-action"><a href="http://www.shopfoxwoodsrewards.com">Shop Foxwoods Rewards</a></li></ul></li><li class="shows "><a href="&#xA;                /shows.aspx"><img src="&#xA;                    /uploadedImages/Top_Menu/entertainment(2).png" alt="Shows">Shows</a><ul class="dropdown"><li><a href="&#xA;                    /shows/">Find A Show</a></li><li><a href="&#xA;                    /comedy/">Comedy</a></li><li><a href="&#xA;                  /nightlife/">Nightlife</a><ul class="dropdown"><li><a href="&#xA;                    /atrium.aspx">Live@Atrium Bar &amp; Lounge</a></li><li><a href="&#xA;                    /cedars-lounge/">Cedars Restaurant Bar &amp; Lounge</a></li><li><a href="/centrale/">Centrale Fox Tower</a></li><li><a href="&#xA;                    /halo-bar/">Halo Bar</a></li><li><a href="&#xA;                    /hard-rock-cafe/">Hard Rock Cafe</a></li><li><a href="&#xA;                    /high-rollers/">High Rollers</a></li><li><a href="&#xA;                    /shrine/">Shrine</a></li><li><a href="&#xA;                    /scorpion-bar/">The Scorpion Bar</a></li><li><a href="&#xA;                    /VUE-24/">VUE 24</a></li></ul></li><li><a href="&#xA;                    /entertainmentspecials.aspx">Specials</a></li><li><a href="https://www.foxwoods.com/musicbox.aspx">The Music Box</a></li></ul></li><li class="activities "><a href="#"><img src="&#xA;                    /uploadedImages/Top_Menu/activities(2).png" alt="Activities">Activities</a><ul class="dropdown"><li><a href="&#xA;                  /spa/">Spa</a><ul class="dropdown"><li><a href="&#xA;                    /spa/">Overview</a></li><li><a href="&#xA;                    /g-spa/">G Spa</a></li><li><a href="&#xA;                    /norwich-spa-at-foxwoods/">Norwich Spa at Foxwoods</a></li><li><a href="&#xA;                    /spa-specials/">Spa Specials</a></li></ul></li><li><a href="&#xA;                  /golf/">Golf</a><ul class="dropdown"><li><a href="&#xA;                    /golf/">Overview</a></li><li><a href="&#xA;                    /golfpackages.aspx">Packages</a></li></ul></li><li><a href="&#xA;                    /high-rollers/">Bowling</a></li><li><a href="&#xA;                    /arcade/">Arcade</a></li><li><a href="&#xA;                    /pequot-trails/">Pequot Trails</a></li><li><a href="http://www.pequotmuseum.org">Pequot Museum</a></li><li><a href="&#xA;                    /ice-rink/">Ice Rink</a></li><li><a href="&#xA;                    /activities-specials/">Specials</a></li></ul></li><li class="meetings "><a href="http://www.foxwoodsmeetingsandevents.com"><img src="&#xA;                    /uploadedImages/Top_Menu/meetings(2).png" alt="Meetings">Meetings</a><ul class="dropdown"><li><a href="http://www.foxwoodsmeetingsandevents.com/accommodations/">Accommodations</a></li><li><a href="http://www.foxwoodsmeetingsandevents.com/meetings-events/">Meetings/Events</a></li><li><a href="http://www.foxwoodsmeetingsandevents.com/portfolio/weddings/">Weddings</a></li><li><a href="http://www.foxwoodsmeetingsandevents.com/portfolio/group_catering/">Catering</a></li><li><a href="http://www.foxwoodsmeetingsandevents.com/amenities/">Amenities</a></li><li><a href="http://www.foxwoodsmeetingsandevents.com/portfolio/dining/">Resort Dining</a></li><li><a href="http://www.foxwoodsmeetingsandevents.com/portfolio/destination-information/">Destination Information</a></li></ul></li><li class="about-us "><a href="&#xA;                /aboutus.aspx"><img src="&#xA;                    /uploadedImages/Top_Menu/about-us(2).png" alt="About Us">About Us</a><ul class="dropdown"><li><a href="&#xA;                    /aboutus.aspx">About Us</a></li><li><a href="&#xA;                    /careers/">Careers</a></li><li><a href="&#xA;                    /contactus.aspx">Contact Us</a></li><li><a href="&#xA;                    /disclaimers.aspx">Disclaimer</a></li><li><a href="https://www.foxwoods.com/foxwoodscares/">Donation and Sponsorship Requests</a></li><li><a href="&#xA;                    /email-sign-up/">Email Sign Up</a></li><li><a href="&#xA;                    /pequotoutpost/">Pequot Outpost</a></li><li><a href="&#xA;                    /pressroom.aspx">Press Releases</a></li><li><a href="&#xA;                    /privacypolicy.aspx">Privacy Policy</a></li><li><a href="&#xA;                    /search.aspx">Search</a></li><li><a href="&#xA;                    /tribalgamingcommission.aspx">Tribal Gaming Commission</a></li><li><a href="&#xA;                  /getting-here/">Getting Here</a><ul class="dropdown"><li><a href="&#xA;                    /bycar.aspx">By Car</a></li><li><a href="&#xA;                    /bus/">By Bus</a></li><li><a href="&#xA;                    /bytrain.aspx">By Train</a></li><li><a href="&#xA;                    /byferry.aspx">By Ferry</a></li></ul></li><li><a href="#">FAQ</a><ul class="dropdown"><li><a href="&#xA;                    /faqgeneral.aspx">General</a></li><li><a href="&#xA;                    /faqaccommodations.aspx">Accommodations</a></li><li><a href="&#xA;                    /faqgaming.aspx">Gaming</a></li><li><a href="&#xA;                    /faqfoxwoodsrewardcard.aspx">Foxwoods Rewards Card</a></li></ul></li><li><a href="#">Responsible Gambling</a><ul class="dropdown"><li><a href="&#xA;                    /responsiblegambling.aspx">Responsible Gambling</a></li><li><a href="&#xA;                    /selfexclusion.aspx">Self Exclusion</a></li></ul></li></ul></li><li class="book-now right fix-sub"><a href="#"><img src="&#xA;                    /uploadedImages/Top_Menu/book-now(2).png" alt="Book Now">Book Now</a><ul class="dropdown booknow"><div class="check-in"><input type="hidden" id="check-in-date"><div>CHECK IN<span id="check-in-date-txt"></span></div></div><div class="check-out"><input type="hidden" id="check-out-date"><div>CHECK OUT<span id="check-out-date-txt"></span></div></div><div class="code nofloat"><input type="text" id="discount-code" value="DISCOUNT / GROUP CODE" class="txtbox"><input type="hidden" id="discount-code-hid" value=""></div><div class="submit"><a id="btn-find-a-room" class="btn btn-sm btn-blue" href="/reserve-your-room/?step=selectHotel">FIND A ROOM</a></div></ul></li><li class="rewards-row mobile"><a href="/rewardslanding.aspx"><img src="/images/2016/foxwoods-rewards-logo.png" alt="Foxwoods Rewards"></a></li><li class="last-row mobile"><div class="logged-in"><ul><li><a href="/rewardssignin.aspx" title="Foxwoods Rewards Account"><i class="fa fa-user"></i> Account
                  </a></li><li><a title="Log out of your Foxwoods Rewards Account" id="mobile-log-out"><i class="fa fa-sign-out"></i> Log Out
                  </a></li></ul></div><div class="logged-out"><ul><li><a href="/rewardssignup.aspx" title="Sign up for Foxwoods Rewards"><i class="fa fa-pencil"></i> Sign Up
                  </a></li><li><a href="/rewardssignin.aspx" title="Login to your Foxwoods Rewards Account"><i class="fa fa-sign-in"></i> Log In
                  </a></li></ul></div></li></ul></div></div>

    
    

    

    <div class="page-container">
        
    
    <nav id="in-page-nav" class="navbar navbar-default" role="navigation"><div class="navbar-header"><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#second-nav"><span class="sr-only">Toggle navigation</span><span class="fa fa-md fa-chevron-down"></span><span>MENU</span></button></div><div id="second-nav" class="collapse navbar-collapse clearfix"><div id="second-nav-container"><ul class="nav navbar-nav"></ul></div></div></nav>

    <div id="content-container" class="container-fluid">
        <div class="row">
            <div class="col-md-10" id="main-col">
                

                <div class="subcopy card">
                    <h1>Privacy Policy</h1>
                    <div class="gray"></div>

                    

                    <p><span>Foxwoods Resort Casino ("Foxwoods", "us" or "we") respects the privacy of all visitors to our Web site. We maintain this site to market and promote the many services and amenities offered by Foxwoods. In order to assist us in better serving our customers, we may collect certain anonymous information automatically, and may require certain personal information, including financial information, if you are making reservations or purchases or requesting information from us through this Web site. This Privacy Statement outlines the type of information that Foxwoods may collect and the manner in which such information will be used. All efforts have been made to ensure the accuracy of the information provided herein, however Foxwoods makes no warranties or representations as to the accuracy of the information supplied herein.</span></p><p>Foxwoods is a wholly-owned instrumentality of the Mashantucket Pequot Tribal Nation.</p><p></p><p><strong>Anonymous Information Automatically Collected</strong><br />We may collect and use certain information about your use of this Web site that does not personally identify you. This information includes your IP address and other information gathered through the use of "cookies". Cookies enable us to gather anonymous click-stream information, such as pages viewed, the date and time the site was entered, the type of internet browser you are using, the type of computer you are using, and the domain name of the Web site from which you linked to our site. This anonymous information does not personally identify you, and is gathered to help us develop generalized customer profiles to better serve our customers.</p><p>If you do not want information collected through the use of cookies, most browsers allow users to accept or reject cookies. If you set your browser not to accept cookies, you may not be able to take advantage of certain services that we offer on our Web site.</p><p></p><p><strong>Personal Information Collected<br /></strong>You may be required to supply personal information such as your name, address, telephone number and/or email address in order to receive certain information, participate in promotions, make reservations or place orders. A personal identification number provided by Foxwoods may be required to access certain personal information. By providing personal information, you agree to be placed on marketing lists and may receive promotional materials from Foxwoods from time to time.</p><p>Your personal information is stored on secure servers that are not accessible by third parties. We provide you with the capability to transmit your personal information via secured and encrypted channels if you use a similarly equipped Web browser. Personally identifiable information is stored on our Web server and is not publicly accessible. Further, personally identifiable information is only accessed by Foxwoods employees on a "need to know" basis. In order to ensure that all of your personal information is correct and up to date, you will be able to update your personal information. While we strive to protect your personal information, Foxwoods cannot ensure or warrant the security of any information you transmit to us, and you do so at your own risk.</p><p></p><p><strong>Age Policy<br /></strong>Foxwoods Resort Casino has no intention of collecting any personal information (such as name, address, telephone number or email address) from individuals under 21 years of age. Any offers, promotions or contests we conduct are not open to persons younger than 21 years of age. We reserve the right to verify age at any time for any reason and may reject a person who does not have valid age identification as requested by Foxwoods. If a child has provided us with personal information, a parent or guardian of that child should contact us at the email address or phone number listed at the bottom of this Privacy Statement to identify the information that must be removed.</p><p></p><p><strong>Sharing Information<br /></strong>The personal information you provide may be shared with other businesses owned or managed by the Mashantucket Pequot Tribe. These entities include The Spa at Norwich Inn, the Mashantucket Pequot Museum &amp; Research Center and PRxN. We do not anticipate that your personal information will be shared or divulged to any non-affiliated third parties by us, provided such disclosure may be necessary to: (a) comply with a court order or other legal process or other legal requirements of any governmental authorities, including but not limited to authorities which regulate gaming activities; (b) protect and defend the rights or property of Foxwoods.</p><p>This Web site contains links to other Web sites. We are not responsible for the privacy practices or the content of such sites. We have no control over its use and you should exercise caution when deciding to disclose your personal information.</p><p></p><p><strong>Changes to Privacy Statement<br /></strong>We reserve the right, at any time and without notice, to add to, change, update or modify this Privacy Statement simply by posting such change, update or modification on the Web site. Any such change, update or modification will be effective immediately upon posting on the Web site.</p><p></p><p><strong>Copyright</strong><br />All contents of this Web site are: Copyright © 2001-2015 Foxwoods Resort Casino, Route 2, Mashantucket, Connecticut, 06338. All rights not expressly granted herein are reserved.</p>

                    
                </div>

                

                

                
        
                
    <input type="hidden" name="ctl00$ctl00$cphFullWidth$cphFoot$hidOfferFilterId" id="ctl00_ctl00_cphFullWidth_cphFoot_hidOfferFilterId" value="385" />

            </div>
            <div class="col-md-2" id="promo-col">
                
                
    
        <div class="offer-summary-gallery">
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/Kesha_HeroTile_980x490.jpg?n=4713" alt="Kesha and the Creepies" alt="Kesha and the Creepies" />
                <div>
                    <div class="xs-title">Kesha and the Creepies</div>
                    <div class="xs-gray">FREE SHOW</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=8913" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="/Kesha/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/Smokey-Robinson-Foxwoods-Hero-Tile.jpg?n=4141" alt="Smokey Robinson" alt="Smokey Robinson" />
                <div>
                    <div class="xs-title">Smokey Robinson</div>
                    <div class="xs-gray">FREE SHOW</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=8923" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="/SmokeyRobinson/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/JohnFogerty_HeroTile_980x490.jpg?n=8337" alt="John Fogerty" alt="John Fogerty" />
                <div>
                    <div class="xs-title">John Fogerty</div>
                    <div class="xs-gray">February 16</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=8789" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="/John-Fogerty/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/Rascal-Flatts-Foxwoods-webhero-980x490.jpg?n=4628" alt="Rascal Flatts" alt="Rascal Flatts" />
                <div>
                    <div class="xs-title">Rascal Flatts</div>
                    <div class="xs-gray">February 17</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=8788" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="/RascalFlatts/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Content/Specials/25th_Anniversary/foxwoods-25th-anniversary-sweepstakes-thumb.png" alt="$250,000 ANNIVERSARY SWEEPSTAKES" alt="$250,000 ANNIVERSARY SWEEPSTAKES" />
                <div>
                    <div class="xs-title">$250,000 ANNIVERSARY SWEEPSTAKES</div>
                    <div class="xs-gray">Fridays On the Retail Concourse</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/Anniversary-Sweepstakes/" class="btn btn-secondary btn-blue" style="display:inline;">Details</a>
                        <a href="" class="btn btn-secondary btn-blue" style="display:none;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Content/Specials/25th_Anniversary/Celebrity-Cruise-Foxwoods-thumb.jpg?n=8565" alt="WIN A CELEBRITY CRUISE " alt="WIN A CELEBRITY CRUISE " />
                <div>
                    <div class="xs-title">WIN A CELEBRITY CRUISE </div>
                    <div class="xs-gray"></div>
                    <div class="btn-group btn-group-xs">
                        <a href="/Celebrity-Cruise/" class="btn btn-secondary btn-blue" style="display:inline;">Details</a>
                        <a href="" class="btn btn-secondary btn-blue" style="display:none;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Content/Specials/25th_Anniversary/25th-Anniversary-Foxwoods-Hero-Blue.jpg?n=6112" alt="Foxwoods 25th Anniversary Cake Cutting Ceremony" alt="Foxwoods 25th Anniversary Cake Cutting Ceremony" />
                <div>
                    <div class="xs-title">Foxwoods 25th Anniversary Cake Cutting Ceremony</div>
                    <div class="xs-gray">On the Retail Concourse </div>
                    <div class="btn-group btn-group-xs">
                        <a href="/Anniversary-Cake-Cutting/" class="btn btn-secondary btn-blue" style="display:inline;">Details</a>
                        <a href="" class="btn btn-secondary btn-blue" style="display:none;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/BNEG/Morris-Day-Foxwoods.jpg?n=8884" alt="Morris Day and The Time" alt="Morris Day and The Time" />
                <div>
                    <div class="xs-title">Morris Day and The Time</div>
                    <div class="xs-gray">February 17</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=8928" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="/Morris-Day/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/BNEG/Taylor-Dayne-Foxwoods.jpg?n=3870" alt="Taylor Dayne" alt="Taylor Dayne" />
                <div>
                    <div class="xs-title">Taylor Dayne</div>
                    <div class="xs-gray">February 18</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=8929" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="https://www.foxwoods.com/Taylor-Dayne/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Content/Specials/Gaming/2017-Winter-Freeze-Deep-Freeze-with-logo.jpg?n=7313" alt="Winter Deep Freeze Poker Challenge" alt="Winter Deep Freeze Poker Challenge" />
                <div>
                    <div class="xs-title">Winter Deep Freeze Poker Challenge</div>
                    <div class="xs-gray"></div>
                    <div class="btn-group btn-group-xs">
                        <a href="/winter-deep-freeze-poker/" class="btn btn-secondary btn-blue" style="display:inline;">Details</a>
                        <a href="" class="btn btn-secondary btn-blue" style="display:none;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/Ashanti-and-JaRule-Foxwoods_295x148.jpg?n=1304" alt="Ashanti &amp; Ja Rule" alt="Ashanti &amp; Ja Rule" />
                <div>
                    <div class="xs-title">Ashanti &amp; Ja Rule</div>
                    <div class="xs-gray">February 16 &amp; 17</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=9032" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="/Ashanti-JaRule/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/KC_and_the_Sunshine_Band_Foxwoods_webhero980x490.jpg?n=8970" alt="KC and the Sunshine Band" alt="KC and the Sunshine Band" />
                <div>
                    <div class="xs-title">KC and the Sunshine Band</div>
                    <div class="xs-gray">February 19</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=9033" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="/KC-Sunshine-Band/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Content/Specials/25th_Anniversary/Vera-Bradley-Scenses-Foxwoods-Anniversary.jpg?n=4133" alt="Lotion and Fragrance Sample" alt="Lotion and Fragrance Sample" />
                <div>
                    <div class="xs-title">Lotion and Fragrance Sample</div>
                    <div class="xs-gray">at Scenses</div>
                    <div class="btn-group btn-group-xs">
                        <a href="/scences-sample/" class="btn btn-secondary btn-blue" style="display:inline;">Details</a>
                        <a href="" class="btn btn-secondary btn-blue" style="display:none;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/BNEG/Aubrey-O-Day-Foxwoods-Hero.jpg?n=4967" alt="Aubrey O'Day" alt="Aubrey O'Day" />
                <div>
                    <div class="xs-title">Aubrey O'Day</div>
                    <div class="xs-gray"></div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=9101" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="/Aubrey-O-Day/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Content/Promotions/FoxPerx-February-Foxwoods-Thumb.jpg?n=5396" alt="$200,000 Bonus FoxPerx" alt="$200,000 Bonus FoxPerx" />
                <div>
                    <div class="xs-title">$200,000 Bonus FoxPerx</div>
                    <div class="xs-gray"></div>
                    <div class="btn-group btn-group-xs">
                        <a href="/BonusFoxPerx/" class="btn btn-secondary btn-blue" style="display:inline;">Details</a>
                        <a href="" class="btn btn-secondary btn-blue" style="display:none;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Pages/Entertainment/Shows/BNEG/Jaleel-White-Foxwoods_295x148.jpg?n=6800" alt="Jaleel White " alt="Jaleel White " />
                <div>
                    <div class="xs-title">Jaleel White </div>
                    <div class="xs-gray"></div>
                    <div class="btn-group btn-group-xs">
                        <a href="/OfferTemplate.aspx?id=9130" class="btn btn-secondary btn-blue" style="display:none;">Details</a>
                        <a href="https://www.foxwoods.com/jaleel-white/" class="btn btn-secondary btn-blue" style="display:inline;">Learn More</a>
                    </div>
                </div>
            </div>
        
            <div class="item clearfix">
                <img src="/uploadedImages/Content/Specials/25th_Anniversary/25th-Anniversary-Foxwoods-Hero-Blue.jpg?n=7576" alt="25th Anniversary Hotel Packages" alt="25th Anniversary Hotel Packages" />
                <div>
                    <div class="xs-title">25th Anniversary Hotel Packages</div>
                    <div class="xs-gray"></div>
                    <div class="btn-group btn-group-xs">
                        <a href="/anniversary-hotel-packages/" class="btn btn-secondary btn-blue" style="display:inline;">Details</a>
                        <a href="" class="btn btn-secondary btn-blue" style="display:none;">Learn More</a>
                    </div>
                </div>
            </div>
        
        </div>
        


            </div>
        </div>
      </div>

        <div class="container" id="footer">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="footer-menu">
                        <div class="title">About Foxwoods</div><ul><li><a href="/aboutus.aspx">About Us</a></li><li><a href="/contactus.aspx">Contact Us</a></li><li><a href="/getting-here/">Getting Here</a></li><li><a href="/pressroom.aspx">Press Releases</a></li><li><a href="/faqgeneral.aspx">FAQ</a></li><li><a href="/careers/">Careers</a></li><li><a href="/pequot-trails/">Pequot Trails</a></li></ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-menu">
                        <!-- Prod Menu ID -->
                        <div class="title">Resources</div><ul><li><a href="/uploadedFiles/Pages/About_Foxwoods/Property_Brochure_120215.pdf">Property Map</a></li><li><a href="/mobileapp.aspx">Mobile App</a></li><li><a href="/responsiblegambling.aspx">Responsible Gambling</a></li><li><a href="/disclaimers.aspx">Disclaimer</a></li><li><a href="/privacypolicy.aspx">Privacy Policy</a></li></ul>
                        <!-- Dev Menu ID -->

                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-menu">
                        <div class="title">Getting Here</div><ul><li><a href="/bycar.aspx">By Car</a></li><li><a href="/bus/">By Bus</a></li><li><a href="/bytrain.aspx">By Train</a></li><li><a href="/byferry.aspx">By Ferry</a></li></ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-menu">
                        <div class="title">About the Mashantucket Pequot Tribal Nation</div><ul><li><a href="http://www.mptn-nsn.gov">Tribal Government</a></li><li><a href="/tribalgamingcommission.aspx">Tribal Gaming Commission</a></li><li><a href="/giving-campaign/">Giving Campaign</a></li></ul>
                        <div class="title">Tribal Businesses</div><ul><li><a href="http://www.pequotmuseum.org">Mashantucket Pequot Museum &amp; Research Center</a></li><li><a href="/pequotoutpost/">Pequot Outpost</a></li><li><a href="http://www.thespaatnorwichinn.com">The Spa at Norwich Inn</a></li></ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" id="footer2">
            <div class="row">
                <div class="col-md-4">
                    <a id="social-link" title="Click to See Foxwoods' Latest Posts on Social Media!"><img src="/images/2016/join-the-conversation.jpg" alt="Join the Conversation"  /></a>
                    <div id="social-panel">
                        <div id="wall">
	                        <div id="social-stream"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div id="footer-social" class="row">
                        <div class="col-xs-2">
                            <a class="link facebook" href="http://www.facebook.com/pages/Foxwoods-Resort-Casino/91920463745" target="_blank" title="Find us on Facebook"><span>Find us on Facebook</span></a>
                        </div>
                        <div class="col-xs-2">
                            <a class="link twitter" href="https://twitter.com/FoxwoodsCT" target="_blank" title="Follow us on Twitter"><span>Follow us on Twitter</span></a>
                        </div>
                        <div class="col-xs-2">
                            <a class="link youtube" href="https://www.youtube.com/user/FoxwoodsOfficial" target="_blank" title="Watch us on YouTube"><span>Watch us on YouTube</span></a>
                        </div>
                        <div class="col-xs-2">
                            <a class="link instagram" href="http://www.instagram.com/foxwoods" target="_blank" title="Follow us on Instagram"><span>Follow us on Instagram</span></a>
                        </div>
                        <div class="col-xs-2">
                            <a class="link pinterest" href="http://www.pinterest.com/foxwoodsct/" target="_blank" title="Follow us on Pinterest"><span>Follow us on Pinterest</span></a>
                        </div>
                        <div class="col-xs-2">
                            <a class="link snapchat" href="/snapchat/" title="Add us on Snapchat"><span>Add us on Snapchat</span></a>
                        </div>
                    </div>
                    <div style="border-top:1px solid #696969;padding:12px 0;">
                        <a href="/emailsignup.aspx" title="Email Offers"><img src="/images/2016/register-for-offers.jpg" alt="Email Signup Icon" /></a>
                    </div>
                    <div style="border-top:1px solid #696969;padding:12px 0;">
                        <a href="https://foxwoods.onelink.me/1911936602?pid=Referral&c=Foxwoods_Com&af_dp=bbefox9CBC304F%3A%2F%2F&af_web_dp=http%3A%2F%2Fonline.foxwoods.com&af_force_dp=true&utm_source=FoxwoodsWebsite&utm_medium=website&utm_campaign=foxwoods_com" title="Online Casino" target="_blank"><img src="/images/2016/foxwoods-online-logo-home.jpg" alt="Foxwoods Online Casino Icon" width="200" /></a>
                    </div>
                </div>
                <div class="col-md-8" id="footer-map">
                    <div class="inset">
                        <h1>Foxwoods Resort Casino</h1>
                        <div class="address">350 Trolley Line Boulevard</div>
                        <div class="address">Mashantucket, CT 06338</div>
                        <div><a href="/bycar.aspx">GPS Address <span class="fa fa-angle-right"></span></a></div>
                        <div class="phone link"><a class="tel" href="tel:1-800-369-9663">1-800-FOXWOODS <span class="fa fa-angle-right"></span></a></div>
                        <div class="link"><a href="http://www.google.com/maps/place/Foxwoods+Resort+Casino/@41.4737961,-71.9634586,17z/data=!3m1!4b1!4m2!3m1!1s0x89e675845275a815:0x2be75433d441c13f" title="Go to Google Maps" target="_blank">Driving Directions <span class="fa fa-angle-right"></span></a></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="property-address" class="col-md-12">Copyright ©2017 Foxwoods Resort Casino. All rights reserved. <a href="/privacypolicy.aspx">Privacy Policy</a>.</div>
            </div>
        </div>
    </div>

    


    


        <!--
        <script type="text/javascript">

            window._mt_ready = function () {
                if (typeof (MyThings) != "undefined") {
                    MyThings.Track({
                        EventType: MyThings.Event.Visit,
                        Action: "300"
                    });
                }
            };
            (function (atok, host) {
                window.mtHost = (("https:" == document.location.protocol) ? "https" :
        "http") + "://" + host;
                window.mtAdvertiserToken = atok;
                var mt = document.createElement("script");
                mt.type = "text/javascript";
                mt.async = true;
                mt.src = window.mtHost + "/c.aspx?atok=" + atok;
                var s = document.getElementsByTagName("script")[0];
                s.parentNode.insertBefore(mt, s);
            })("2919-100-us", "rainbow-us.mythings.com");</script> 
            -->
            <!--
Start of DoubleClick Floodlight Tag: Please do not remove
Activity name of this tag: Foxwoods Universal Floodlight
URL of the webpage where the tag is expected to be placed: http://TBD
This tag must be placed between the <body> and </body> tags, as close as possible to the opening tag.
Creation Date: 03/10/2015
-->
<script type="text/javascript">
var axel = Math.random() + "";
var a = axel * 10000000000000;
document.write('<iframe src="https://4728623.fls.doubleclick.net/activityi;src=4728623;type=unive0;cat=foxwo0;ord=' + a + '?" width="1" height="1" frameborder="0" style="display:none"></iframe>');
</script>
<noscript>
<iframe src="https://4728623.fls.doubleclick.net/activityi;src=4728623;type=unive0;cat=foxwo0;ord=1?" width="1" height="1" frameborder="0" style="display:none"></iframe>
</noscript>
<!-- End of DoubleClick Floodlight Tag: Please do not remove -->
   
    

<!-- Twitter universal website tag code -->
<script type="text/javascript">
    var tag = document.createElement("script");
    tag.type = "text/javascript";
    tag.src = "//platform.twitter.com/oct.js";
    tag.async = true;
    document.getElementsByTagName("head")[0].appendChild(tag);

    tag.onload = function () {
        twttr.conversion.trackPid('nuosq', { tw_sale_amount: 0, tw_order_quantity: 0 });
    }
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=nuosq&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=nuosq&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
<!-- End Twitter universal website tag code -->

<!-- Typekit tags -->
<!--
<script type="text/javascript">
    var tag = document.createElement("script");
    tag.type = "text/javascript";
    tag.src = "https://use.typekit.net/eiq7aqb.js"
    tag.async = true;
    document.getElementsByTagName("head")[0].appendChild(tag);

    tag.onload = function () {
        try { Typekit.load({ async: true }); } catch (e) { }
    }
</script>
-->
<!-- End Typekit tags -->

<!-- Engage Web Tracking Code -->
<script type="text/javascript">
    var tag = document.createElement("script");
    tag.type = "text/javascript";
    tag.src = "https://www.sc.pages02.net/lp/static/js/iMAWebCookie.js?b0b2b92-150cf547484-1973771dea71da7e4c551ed9f05528be&h=www.pages02.net";
    tag.async = true;
    document.getElementsByTagName("head")[0].appendChild(tag);
</script>
<!-- End Engage Web Tracking Code -->

<script type="text/javascript">
var customEventsOnLoad = window.onload;
window.onload = function()
{
    if (customEventsOnLoad) customEventsOnLoad();
    
}
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1839416876269853'); 
fbq('track', 'PageView');

</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1839416876269853&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code —>

</form>
    <div class="sidebar-container">
        <a id="toggle-25th" title="hide"><i class="fa fa-caret-left"></i></a>
        <a href="/anniversary" class="fw-25th-anniversary" title="Foxwoods' 25th Anniversary"><img src="/images/2016/25th-badge.png" alt="25th Anniversary Badge" /></a>
    </div>
    <a href="#" class="back-to-top" title="Back to Top">Back to Top</a>
</body>
</html>
