<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<!-- Meta -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content=""></meta>
<meta name="author" content=""></meta>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Title -->
<title>DVO Privacy Policy</title>

<!-- CSS Stylesheet -->
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Great+Vibes' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" media="screen" />
<link rel="stylesheet" type="text/css" href="css/style.css" media="screen" />
<link href="css/layout/fullwidth.css" rel="stylesheet" type="text/css" id="layout">
<link href="css/skin/green.css" rel="stylesheet" type="text/css" id="skin">
<link href="css/custom.css" rel="stylesheet" type="text/css" id="custom">
<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src="js/respond.min.js" type="text/javascript"></script>
<![endif]-->

<!--Favicons -->
 <link href="images/icon/apple-touch-icon-144.png" sizes="144x144" rel="apple-touch-icon-precomposed"></link>
<link href="images/icon/apple-touch-icon-114.png" sizes="114x114" rel="apple-touch-icon-precomposed"></link>
<link href="images/icon/apple-touch-icon-72.png" sizes="72x72" rel="apple-touch-icon-precomposed"></link>
<link href="images/icon/apple-touch-icon-57.png" rel="apple-touch-icon-precomposed"></link>
<link href="images/icon/favicon.png" rel="shortcut icon"></link>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>

</head>

<body>

     <header class="site-header">
        <div class="container">
            <div class="site-logo">
                <a href="//www.dvo.com"><img src="//www.dvo.com/images/cookn-logo.png" alt="Cook'n" /></a>
                <a href="//www.dvo.com/sign-up.php"><img class="icon-sign-up" src="//www.dvo.com/images/Sign-Up.png" alt="Sign Up!" /></a>
            </div>

            <nav class="site-nav navbar-collapse collapse">
                <ul>
                    <li>
                        <a href="//www.dvo.com/recipe-app.php">Cook'n</a>
                        <ul>
<!--                            <li><a href="//www.dvo.com/cookn-demonstration.php">Demo Videos</a></li>-->
                            <li><a href="//www.dvo.com/video-demonstrations.php">Demo Videos</a></li>
                            <li><a href="//www.dvo.com/recipe-app.php">Features</a></li>
                            <li><a href="//www.dvo.com/feedback.php">Reviews</a></li>
                            <li><a href="//www.dvo.com/versionX3.php">Upgrade</a></li>
                            <li><a href="//www.dvo.com/cooking-club.php">Membership Plans</a></li>
                            <li>
                                <a href="//www.dvo.com/sign-up.php">Sign Up for Free</a>
                                                            </li>
                                                    </ul>
                    </li>
                    <li>
                        <a href="//www.dvo.com/cooking-club.php">Add-Ons</a>
                        <ul>
                            <li><a href="//www.dvo.com/cooking-club.php">Membership Plans</a></li>
                            <li><a href="//www.dvo.com/cookn-plugins.php">Cook'n Plugins</a></li>
                            <li><a href="//www.dvo.com/cookn-themes.php">Cook'n Themes</a></li>
                                                                                </ul>
                    </li>
                    <li>
                        <a href="//www.dvo.com/cookbooks.php">Cookbooks</a>
                        <ul>
                            <li><a href="//www.dvo.com/cookbooks.php">New Releases</a></li>
                            <li><a href="//www.dvo.com/cookbooks.php">Top Sellers</a></li>
                            <li><a href="//www.dvo.com/cookbooks.php">Brand Names</a></li>
                            <li><a href="//www.dvo.com/cookbooks.php">All Cookbooks</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="//www.dvo.com/recipearchive.php">Yum!</a>
                        <ul>
                            <li><a href="//www.dvo.com/homecookn/">Current Newsletter</a></li>
                            <li><a href="//www.dvo.com/newsletterarchive.php">Newsletter Archive</a></li>
                            <li><a href="//www.dvo.com/recipearchive.php">Recipe Archive</a></li>
                            <li><a href="//www.dvo.com/forum/">Cook'n Club Forum</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="//www.dvo.com/support.php">Support</a>
                        <ul>
                            <li><a href="//www.dvo.com/support.php">Frequently Asked Questions</a></li>
                            <li><a href="//www.dvo.com/support.php#507">Video Tutorials</a></li>
                            <li><a href="//www.dvo.com/support-contactform.php">Contact Support</a></li>
                            <li><a href="//www.dvo.com/about.php">About DVO</a></li>
                        </ul>
                    </li>
                    <li class="hidden-md hidden-lg hidden-xl">
                        <a href="https://www.dvo.com/login.php">Login</a>
                    </li>                    
                </ul>
            </nav>

            <div class="site-utility">
                                <a class="login-link hidden-xs hidden-sm" href="https://www.dvo.com/login.php">Login</a>
            </div>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
    </header>


  <!-- CONTENT
===============================-->
   <div class="page-section">
  	<div class="container">
		<div class="row">
  			<div class="col-sm-12">
				<h1 class="mainheader text-center">DVO Enterprises Privacy Policy</h1>
			</div>
  		</div>
  		<div class="row">
   			<div class="col-sm-8 col-sm-offset-2">

DVO Enterprises is committed to protecting the privacy and security of its online visitors. This policy statement provides our visitors with an overview of the measures we have taken to provide a safe environment for everyone.<br><br> 

<B>PERSONALLY IDENTIFIABLE INFORMATION</B> <br><br>
We consider the following, among other things, to be personally identifiable information: first and last name, e-mail address, street address and phone number. <br><br>
We may request such personally identifiable information from our visitors to process and fulfill orders for products or services and in connection with contests, sweepstakes, product registration, newsletter subscription, and visitors' requests for information. <br><br>
<B>NON-PERSONALLY IDENTIFIABLE INFORMATION AND THE USE OF COOKIE TECHNOLOGY</B> <br><br>
We collect non-personally identifiable information through the use of a software technology called "cookies," and through our visitors' voluntary submissions to us and/or upon our request. By non-personally identifiable information, we are referring to information about our visitors' browsers (e.g., Netscape Navigator or Internet Explorer), operating systems (e.g., Windows or Macintosh), Internet service providers (e.g., AOL or NET.COM) and other similar information which we track in aggregate form.<br><br>
Cookie technology helps us to know how many people visit us and where visitors go on our site. Cookies are small bits of information we send to your computer. Among other things, this non-personally identifiable information allows us to know which areas are favorites, which areas need a bit of improvement, or what technologies and Internet services are being used by our visitors so that we may continually improve our visitors' online experiences. <br><br>
<B>USE OF IP ADDRESSES</B> <br><br>

We collect IP addresses to obtain aggregate information on the use of dvo.com. An IP address is a number assigned to your computer by a Web server when you're on the Web. When you are on our site, we have a back-end server that logs your computer's IP address. We only use the information we find out from tracking IP addresses in the aggregate, such as how many users entered a specific area of our site, and not to track a specific IP address to identify an individual user. However, we may use such information to identify a user if we feel that there are or may be safety and/or security issues or to comply with legal requirements. <br><br>
<B>SECURITY</B> <br><br>
DVO Enterprises ensures that all personally and non-personally identifiable information that it receives via the Internet is secure against unauthorized access. This information is kept in a safe and secure system isolated from direct connection to the Internet. We will give out personal information as required by law, for example to comply with a court order or subpoena or to assist in criminal investigations. We may also give out personal information when we deem it advisable in order to protect the safety and security of our sites and visitors to our sites. <br><br>

<B>Why are we so safe?</B> <br><br>
DVO Enterprises uses secure server software over a Secure Socket Layer (SSL) line to protect your credit card information. It encrypts all of your personal and credit card information so that this information cannot be read as it travels to our ordering system, and once it is received, it is stored in a location not accessible via the Internet. </font><br><br>

			
			</div>


  	</div>
  </div>

     <!-- Page Section
===============================-->
  <section id="page-section-11" class="page-section special">
    <div class="container">
      <div class="row clearfix no-padding">

        <div class="col-sm-8 ">

          <h1 class="visible-lg special"><i class="icon-star"></i> &nbsp;&nbsp;Sign Up for Free!</h1>
		  <h1 class="hidden-lg special"><i class="icon-star"></i> &nbsp;&nbsp;Sign Up for Free!</h1>
        </div>

        <div class="col-sm-4 ">
           <center><div>
			  <a class="btn-flat white" href="http://www.dvo.com/sign-up.php"><div>Sign Up</div></a>
		   </div>
        </div></center>

      </div>
    </div>
  </section>
      <!-- Page Section
===============================-->
  <section id="page-section-1" class="page-section ">
    <div class="container">
      <div class="row no-padding">
        <div class="col-lg-8 ">
          <h3>#1 Best-Selling Recipe App with millions and millions served!</h3>
        </div>
        <div class="col-lg-4 ">
          <ul class="social-icon">
            <li><img src="http://www.dvo.com/images/follow-us.png" alt="follow us" width="95" target="_blank"></li>
            <li><a href="https://www.facebook.com/Recipe.Organizer" class="icon-facebook facebook" target="_blank"></a></li>
            <li><a href="http://www.pinterest.com/cooknsoftware/boards/" class="icon-pinterest pinterest target="_blank""></a></li>
            <li><a href="https://twitter.com/CooknSoftware" class="icon-twitter twitter" target="_blank"></a></li>
			      <li><a href="https://plus.google.com/u/0/b/110972757289789589009/110972757289789589009/posts" class="icon-google-plus google-plus" target="_blank"></a></li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  
      <!-- Footer -->
    <footer class="site-footer">
        <div class="container">
            <div class="row no-padding">
                <div class="col-sm-4">
                    <h3 class="no-padding">DVO Partners</h3>
                    <ul>
                        <li><a href="//www.dvo.com/gooseberry-patch-cd.php">Gooseberry Patch </a></li>
                        <li><a href="//www.dvo.com/diabetic-cookbook.php">ADA</a></li>
                        <li><a href="//www.dvo.com/taste_of_home.php">Taste of Home</a></li>
                        <li><a href="//www.dvo.com/barbecue-food.php">Weber</a></li>
                        <li><a href="//www.dvo.com/rhodes-bread.php">Rhodes</a></li>
                    </ul>
                </div>
            
                <hr class="visible-xs"/>

                <div class="col-sm-4">
                    <h3 class="no-padding">DVO Information</h3>
                    <ul>
                        <li><a href="//www.dvo.com/contact.php ">Contact Us </a></li>
                        <li><a href="//www.dvo.com/about.php ">About DVO</a></li>
                        <li><a href="//www.dvo.com/newsletter.html">Newsletter Signup</a></li>
                        <li><a href="//www.dvo.com/privacy.php">Privacy Policy</a></li>
                        <li><a href="//www.dvo.com/200-percent-guarantee.php">200% Guarantee</a></li>
                        <li><a href="//www.dvo.com/links.htm">Other Resources</a></li>
                                            </ul>
                </div>

                <hr class="visible-xs"/>
        
                <div class="col-sm-4">
                    <h3 class="no-padding">Free Newsletter Sign Up</h3>

                    <!-- Newsletter -->
                    <div class="form-group">
                        <form method="post" action="http://www.mailermailer.com/x">
                            <input type="hidden" name="owner_id_enc" value="07566a-519e9211" />
                            <div>
                                <label>Email Address</label>
                                <div class="input-group"><input type="text" name="user_email" value="" size="28" maxlength="100" /></div>
                            </div>
                            <div>
                                <label>First Name</label>
                                <div class="input-group"><input type="text" name="user_fname" value="" size="28" maxlength="100" /></div>
                            </div>

                            <input type="hidden" name="function" value="Subscribe" />
                            <button class="btn btn-default">Subscribe <i class="icon-circle-arrow-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="site-footer-bottom">
            <div class="container">
                <div class="row no-padding">
                    <div class="col-lg-8">
                        <ul class="site-footer-menu">
                            <li><a href="//www.dvo.com/index.php">Home</a></li>
                            <li><a href="//www.dvo.com/recipe-organizer.php">Recipe Organizer</a></li>
                            <li><a href="//www.dvo.com/cookn-apps.html">Recipe Apps</a></li>
                            <li><a href="//www.dvo.com/products.php">Add-Ons</a></li>
                            <li><a href="//www.dvo.com/cookbooks.php">Cookbooks</a></li>
                            <li><a href="//www.dvo.com/recipearchive.php">Yum!</a></li>
                            <li><a href="//www.dvo.com/support.php">Support</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-4">
                        <span class="copyright">Copyright &copy 2018 DVO Enterprises</span>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer-->

    <!-- Move To Top-->
    <div id="move-to-top"><a class="icon-angle-up"></a></div>
    <!-- End Move To Top -->

  <!-- END CONTENT
===============================-->

</div>
<!-- End Wrapper All Content-->

<!--Move To Top-->
<div id="movetotop"> <a class="icon-angle-up"></a> </div>

<!-- LA JAVASCRIPT
=============================== -->
<script src="http://www.dvo.com/js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="http://www.dvo.com/js/bootstrap.js"></script>
<script src="http://www.dvo.com/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="http://www.dvo.com/js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="http://www.dvo.com/js/jquery.jcarousel.min.js" type="text/javascript"></script>
<script src="http://www.dvo.com/js/selectnav.js" type="text/javascript"></script>
<script src="http://www.dvo.com/js/menu-lamp.js" type="text/javascript"></script>
<script src="http://www.dvo.com/js/jquery.touchwipe.min.js" type="text/javascript"></script>
<script src="http://www.dvo.com/js/home-script.js" type="text/javascript"></script></script>
<script src="http://www.dvo.com/js/custom.js" type="text/javascript"></script>





<!-- Start Quantcast tag -->
<script type="text/javascript" src="http://edge.quantserve.com/quant.js"></script>

<script type="text/javascript">
_qacct="p-84yXy7xTOuoUk";quantserve();</script>
<noscript>
<img src="http://pixel.quantserve.com/pixel/p-84yXy7xTOuoUk.gif" style="display: none" height="1" width="1" alt="Quantcast"/></noscript>
<!-- End Quantcast tag -->

<!-- SiteCatalyst code version: H.20.3. Copyright 1997-2009 Adobe, Inc. More info available at http://www.Adobe.com -->
<script language="JavaScript" type="text/javascript" src="http://www.dvo.com/js/s_code.js"></script>
<script language="JavaScript" type="text/javascript">
/* You may give each page an identifying name, server, and channel on the next lines. */
s.pageName=""
s.server=""
s.channel="Cookn12"
s.pageType=""
s.prop1=""
s.prop2=""
s.prop3=""
s.prop4=""
s.prop5=""
/* Conversion Variables */
s.campaign=""
s.state=""
s.zip=""
s.events="prodView"
s.products=""
s.purchaseID=""
s.eVar1=""
s.eVar2=""
s.eVar3=""
s.eVar4=""
s.eVar5=""
/********** DO NOT ALTER ANYTHING ELSE BELOW THIS LINE! *************/
var s_code=s.t();if(s_code)document.write(s_code)//--></script>
<script language="JavaScript" type="text/javascript"><!-- if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\! -'+'-')
//--></script><noscript><a href="http://www.Adobe.com" title="Web Analytics"><img src="10xdvo.112.2O7.net/b/ss/10xdvo/1/H.20.3- -NS/0" height="1" width="1" border="0" alt="" /></a></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.20.3. -->

<!-- google analytics Start-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try{
var pageTracker = _gat._getTracker("UA-273522-1");
pageTracker._trackPageview();
} catch(err) {}
</script>
<!-- google analytics End-->

		<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0013/3586.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>



</body>
</html>
