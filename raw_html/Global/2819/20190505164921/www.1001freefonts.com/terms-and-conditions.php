<!DOCTYPE html>
<html lang="en">
    <head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Terms & Conditions - 1001 Free Fonts</title>
		<meta charset="utf-8">
        <meta name="description" content="Terms and conditions">
		<meta name="keywords" content="terms, conditions, rules, fonts, free, download">
        <meta name="google-site-verification" content="XrSKKdOsDi3FNT2bKC3X8c6_gLF0OPnyotXMHNSppps">
		<meta name="robots" content="index, follow">
        <meta name="author" content="Jason Nolan">
        <meta name="copyright" content="Copyright © 2019 - 1001 Free Fonts">
        <link rel="manifest" href="https://www.1001freefonts.com/manifest.json">
        <link rel="canonical" href="https://www.1001freefonts.com/">
        <link rel="shortcut icon" href="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/favicon.ico">
        <link rel="alternate" href="https://www.1001freefonts.com/rss.php" title="Latest Fonts" type="application/rss+xml">
        <link rel="stylesheet" href="https://font-1001freefonts.netdna-ssl.com/themes/blue/styles/screen-min.css?r=29" type="text/css"/>
    </head>

    <body>
        <a id="pageTop"><!-- --></a>
        <div class="toolbarContainer">

            <div class="toolbarInnerContainer">
                <!-- toolbar section -->
                <div class="mainNavBar">
                    <div class="mainNavBarInner">
                        <div class="navBarRightSearch responsiveCenter">
                                <div class="languageDropdown">
                                    <div class="content">
                                <div class="topNode"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/flags/en.png" width="16" height="11" alt="English (en)" title="Switch site language to English (en)"/><span>EN</span></div>                                        <div class="menu">
                                            <div class="arrow"></div>
                                        <a href="https://www.1001freefonts.com/es/terms-and-conditions.php"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/flags/es.png" width="16" height="11" alt="Español (es)" title="Switch site language to Español (es)"/><span>ES</span></a><a href="https://www.1001freefonts.com/fr/terms-and-conditions.php"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/flags/fr.png" width="16" height="11" alt="Français (fr)" title="Switch site language to Français (fr)"/><span>FR</span></a><a href="https://www.1001freefonts.com/de/terms-and-conditions.php"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/flags/de.png" width="16" height="11" alt="Deutsch (de)" title="Switch site language to Deutsch (de)"/><span>DE</span></a>                                        </div>
                                    </div>
                                </div>
                                            
                            <div class="navBarRightSearchWrapper">
                                <form method="GET" action="/search.php">
                                    <label for="searchQuery"></label>
                                    <input name="q" id="searchQuery" type="text"/>
                                    <input name="search" value="search" type="submit"/>
                                </form>
                            </div>
                            <div class="socialTop">
                                <div id="header-share-buttons">
                                    <!-- Facebook -->
                                    <a href="#" onclick="showWindowUrl('http://www.facebook.com/sharer.php?u=https://www.1001freefonts.com/terms-and-conditions.php'); return false;"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/share_icons/simple/facebook.png" alt="Facebook" /></a>

                                    <!-- Twitter -->
                                    <a href="#" onclick="showWindowUrl('https://twitter.com/share?url=https://www.1001freefonts.com/terms-and-conditions.php&amp;text=&amp;hashtags=1001freefonts'); return false;"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/share_icons/simple/twitter.png" alt="Twitter" /></a>

                                    <!-- Google+ -->
                                    <a href="#" onclick="showWindowUrl('https://plus.google.com/share?url=https://www.1001freefonts.com/terms-and-conditions.php'); return false;"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/share_icons/simple/google.png" alt="Google" /></a></div>
                            </div>

                        </div>
                        <div class="navBarLeftLinks">
<a href="/submit-font.php">Submit A Font</a>&nbsp;|&nbsp;<a href="/adesigners.php">Designers</a>&nbsp;|&nbsp;<a href="http://www.ultimatefontdownload.com" target="_blank">10,000 Fonts</a>&nbsp;|&nbsp;<a href="/faq.php">Faq</a>&nbsp;|&nbsp;<a href="/contact.php">Contact</a>                        </div>
                        <div class="clear"><!-- --></div>
                    </div>
                    <div class="clear"><!-- --></div>
                </div>
                <div class="clear"><!-- --></div>
            </div>

            <select id="tinynav" class="tinynav" onChange="eval(document.getElementById('tinynav').options[document.getElementById('tinynav').selectedIndex].getAttribute('data-jsaction'));">       
                <option>- Navigation -</option>
                <option data-jsaction="window.location='/submit-font.php';" >Submit A Font</option>
                <option data-jsaction="window.location='http://www.ultimatefontdownload.com'">10,000 Fonts</option>
                <option data-jsaction="window.location='/links.php';" >Links</option>
                <option data-jsaction="window.location='/faq.php';" >Faq</option>
                <option data-jsaction="window.location='/contact.php';" >Contact</option>
            </select> 

        </div>
        <div class="clear"><!-- --></div>
        <!--<div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>-->

        <div class="bodyContainer">
            <div class="mainPageContainer">
                <div class="pageInnerContainer">
                    <!-- header section -->
                    <div class="headerBar">
                        <!-- header ads -->
                            <div class="mainHeaderAds" style="max-height: 110px; overflow:hidden;">
                                <div class="mainHeaderAds" style="overflow:hidden;">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 111 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-2673198289811103"
     data-ad-slot="5412878245"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>                                </div>
                                </div>
                                
                                                                                            <div class="responsiveDisplay mobileLogoAdvert" style="margin-top: 1px; float: right; width: 300px; max-height: 100px; overflow: hidden;">
                                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Large Mobile Banner -->
<ins class="adsbygoogle"
     style="display:inline-block;width:320px;height:100px"
     data-ad-client="ca-pub-2673198289811103"
     data-ad-slot="9751500671"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>                                </div>
                                                    
                        <!-- main logo -->
                        <div class="mainLogo">
                            <a href="/">
                                <img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/main_logo.png" width="160" height="90" alt="1001 Free Fonts" title="1001 Free Fonts"/>
                            </a>
                        </div>

                                                                                    <div class="responsiveDisplay mobileLogoAdvertAlt" style="margin-top: 1px; float: right; width: 300px; max-height: 100px; overflow: hidden;">
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- Large Mobile Banner -->
<ins class="adsbygoogle"
     style="display:inline-block;width:320px;height:100px"
     data-ad-client="ca-pub-2673198289811103"
     data-ad-slot="9751500671"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>                                </div>
    
                        <div class="clear"><!-- --></div>
                    </div>
                    <div class="clear"><!-- --></div>
                </div>
            </div>
            <div class="clear"><!-- --></div>
            <div class="mainPageContainer">
                <div class="pageInnerContainer">

                    <!-- category section -->
                    <div id="categoryBar" class="categoryBar" style="display:block; margin-bottom: 0px;">
                        <div class="categoryBarInner">
                            <!-- letters -->
                            <div class="letterListing responsiveCenter">
                                <div class='atozTopTitle'>
Alphabetically Organized Fonts:&nbsp;
                                </div>
<div class='letterLink' onClick="window.location='/afonts.php';"><a href='/afonts.php'>A</a></div><div class='letterLink' onClick="window.location='/bfonts.php';"><a href='/bfonts.php'>B</a></div><div class='letterLink' onClick="window.location='/cfonts.php';"><a href='/cfonts.php'>C</a></div><div class='letterLink' onClick="window.location='/dfonts.php';"><a href='/dfonts.php'>D</a></div><div class='letterLink' onClick="window.location='/efonts.php';"><a href='/efonts.php'>E</a></div><div class='letterLink' onClick="window.location='/ffonts.php';"><a href='/ffonts.php'>F</a></div><div class='letterLink' onClick="window.location='/gfonts.php';"><a href='/gfonts.php'>G</a></div><div class='letterLink' onClick="window.location='/hfonts.php';"><a href='/hfonts.php'>H</a></div><div class='letterLink' onClick="window.location='/ifonts.php';"><a href='/ifonts.php'>I</a></div><div class='letterLink' onClick="window.location='/jfonts.php';"><a href='/jfonts.php'>J</a></div><div class='letterLink' onClick="window.location='/kfonts.php';"><a href='/kfonts.php'>K</a></div><div class='letterLink' onClick="window.location='/lfonts.php';"><a href='/lfonts.php'>L</a></div><div class='letterLink' onClick="window.location='/mfonts.php';"><a href='/mfonts.php'>M</a></div><div class='letterLink' onClick="window.location='/nfonts.php';"><a href='/nfonts.php'>N</a></div><div class='letterLink' onClick="window.location='/ofonts.php';"><a href='/ofonts.php'>O</a></div><div class='letterLink' onClick="window.location='/pfonts.php';"><a href='/pfonts.php'>P</a></div><div class='letterLink' onClick="window.location='/qfonts.php';"><a href='/qfonts.php'>Q</a></div><div class='letterLink' onClick="window.location='/rfonts.php';"><a href='/rfonts.php'>R</a></div><div class='letterLink' onClick="window.location='/sfonts.php';"><a href='/sfonts.php'>S</a></div><div class='letterLink' onClick="window.location='/tfonts.php';"><a href='/tfonts.php'>T</a></div><div class='letterLink' onClick="window.location='/ufonts.php';"><a href='/ufonts.php'>U</a></div><div class='letterLink' onClick="window.location='/vfonts.php';"><a href='/vfonts.php'>V</a></div><div class='letterLink' onClick="window.location='/wfonts.php';"><a href='/wfonts.php'>W</a></div><div class='letterLink' onClick="window.location='/xfonts.php';"><a href='/xfonts.php'>X</a></div><div class='letterLink' onClick="window.location='/yfonts.php';"><a href='/yfonts.php'>Y</a></div><div class='letterLink' onClick="window.location='/zfonts.php';"><a href='/zfonts.php'>Z</a></div><div class='letterLink' onClick="window.location='/numfonts.php';"><a href='/numfonts.php'>#</a></div>                                <div class="clear"><!-- --></div>
                            </div>
                            <div class="clear"><!-- --></div>

                            <table class="categoryWrapper responsiveHide">
                                <tr>
                                    <td>
                                        <!-- categories -->
                                <table><tr><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/new-fonts.php';"><a href='/new-fonts.php'>New Fonts</a></div><div class='categoryLink' onClick="window.location='/top-fonts.php';"><a href='/top-fonts.php'>Top Fonts</a></div><div class='socialButton' style='margin-top: 59px;'>                                <div class="twitter">
                                    <div id="widget">
                                        <div class="btn-o">
                                            <a id="follow-button" class="btn" title="Follow @1001FreeFonts on Twitter" onclick="showWindowUrl('https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fwww.1001freefonts.com%2F&amp;ref_src=twsrc%5Etfw&amp;region=follow_link&amp;screen_name=1001FreeFonts&amp;tw_p=followbutton'); return false;" href="#" >
                                                <i></i><span class="label" id = "l">Follow</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                        
                                <div class="facebook">
                                    <button type="submit" class="inlineBlock _2tga _49ve" title="Like 1001 Free Fonts's Page on Facebook" onclick="window.open('https://www.facebook.com/1001FreeFonts/'); return false;">
                                        <span class="_3jn- inlineBlock"><span class="_3jn_"></span>
                                            <span class="_49vg">
                                                <img class="_1pbs inlineBlock img" src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/lH1ibRl5GKq.png" alt="" width="16" height="16">
                                            </span>
                                        </span>
                                        <span class="_49vh _2pi7">Like</span>
                                        <span class="_5n6h _2pih">76K</span>
                                    </button>
                                </div>

                                </div></td><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/3d-fonts.php';"><a href='/3d-fonts.php'>3D</a></div><div class='categoryLink' onClick="window.location='/alien-fonts.php';"><a href='/alien-fonts.php'>Alien</a></div><div class='categoryLink' onClick="window.location='/animal-fonts.php';"><a href='/animal-fonts.php'>Animal</a></div><div class='categoryLink' onClick="window.location='/army-stencil-fonts.php';"><a href='/army-stencil-fonts.php'>Army Stencil</a></div><div class='categoryLink' onClick="window.location='/asian-arabic-fonts.php';"><a href='/asian-arabic-fonts.php'>Asian Arabic</a></div><div class='categoryLink' onClick="window.location='/bitmap-pixel-fonts.php';"><a href='/bitmap-pixel-fonts.php'>Bitmap Pixel</a></div><div class='categoryLink' onClick="window.location='/blackletter-fonts.php';"><a href='/blackletter-fonts.php'>Blackletter</a></div><div class='categoryLink' onClick="window.location='/blurred-fonts.php';"><a href='/blurred-fonts.php'>Blurred</a></div></td><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/brush-fonts.php';"><a href='/brush-fonts.php'>Brush</a></div><div class='categoryLink' onClick="window.location='/calligraphy-fonts.php';"><a href='/calligraphy-fonts.php'>Calligraphy</a></div><div class='categoryLink' onClick="window.location='/celtic-irish-fonts.php';"><a href='/celtic-irish-fonts.php'>Celtic Irish</a></div><div class='categoryLink' onClick="window.location='/chalk-fonts.php';"><a href='/chalk-fonts.php'>Chalk Crayon</a></div><div class='categoryLink' onClick="window.location='/christmas-fonts.php';"><a href='/christmas-fonts.php'>Christmas</a></div><div class='categoryLink' onClick="window.location='/comic-cartoon-fonts.php';"><a href='/comic-cartoon-fonts.php'>Comic Cartoon</a></div><div class='categoryLink' onClick="window.location='/computer-fonts.php';"><a href='/computer-fonts.php'>Computer</a></div><div class='categoryLink' onClick="window.location='/curly-fonts.php';"><a href='/curly-fonts.php'>Curly</a></div></td><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/decorative-fonts.php';"><a href='/decorative-fonts.php'>Decorative</a></div><div class='categoryLink' onClick="window.location='/dingbats-fonts.php';"><a href='/dingbats-fonts.php'>Dingbats</a></div><div class='categoryLink' onClick="window.location='/disney-fonts.php';"><a href='/disney-fonts.php'>Disney</a></div><div class='categoryLink' onClick="window.location='/distorted-eroded-fonts.php';"><a href='/distorted-eroded-fonts.php'>Distorted Eroded</a></div><div class='categoryLink' onClick="window.location='/dotted-fonts.php';"><a href='/dotted-fonts.php'>Dotted</a></div><div class='categoryLink' onClick="window.location='/easter-fonts.php';"><a href='/easter-fonts.php'>Easter</a></div><div class='categoryLink' onClick="window.location='/famous-fonts.php';"><a href='/famous-fonts.php'>Famous</a></div><div class='categoryLink' onClick="window.location='/fancy-fonts.php';"><a href='/fancy-fonts.php'>Fancy</a></div></td><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/fantasy-fonts.php';"><a href='/fantasy-fonts.php'>Fantasy</a></div><div class='categoryLink' onClick="window.location='/fire-fonts.php';"><a href='/fire-fonts.php'>Fire</a></div><div class='categoryLink' onClick="window.location='/fixed-width-fonts.php';"><a href='/fixed-width-fonts.php'>Fixed Width</a></div><div class='categoryLink' onClick="window.location='/gothic-fonts.php';"><a href='/gothic-fonts.php'>Gothic</a></div><div class='categoryLink' onClick="window.location='/graffiti-fonts.php';"><a href='/graffiti-fonts.php'>Graffiti</a></div><div class='categoryLink' onClick="window.location='/greek-roman-fonts.php';"><a href='/greek-roman-fonts.php'>Greek Roman</a></div><div class='categoryLink' onClick="window.location='/groovy-fonts.php';"><a href='/groovy-fonts.php'>Groovy</a></div><div class='categoryLink' onClick="window.location='/halloween-fonts.php';"><a href='/halloween-fonts.php'>Halloween</a></div></td><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/handwriting-fonts.php';"><a href='/handwriting-fonts.php'>Handwriting</a></div><div class='categoryLink' onClick="window.location='/headline-fonts.php';"><a href='/headline-fonts.php'>Headline</a></div><div class='categoryLink' onClick="window.location='/horror-fonts.php';"><a href='/horror-fonts.php'>Horror</a></div><div class='categoryLink' onClick="window.location='/ice-snow-fonts.php';"><a href='/ice-snow-fonts.php'>Ice Snow</a></div><div class='categoryLink' onClick="window.location='/italic-fonts.php';"><a href='/italic-fonts.php'>Italic</a></div><div class='categoryLink' onClick="window.location='/lcd-fonts.php';"><a href='/lcd-fonts.php'>LCD</a></div><div class='categoryLink' onClick="window.location='/medieval-fonts.php';"><a href='/medieval-fonts.php'>Medieval</a></div><div class='categoryLink' onClick="window.location='/mexican-fonts.php';"><a href='/mexican-fonts.php'>Mexican</a></div></td><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/modern-fonts.php';"><a href='/modern-fonts.php'>Modern</a></div><div class='categoryLink' onClick="window.location='/movies-tv-fonts.php';"><a href='/movies-tv-fonts.php'>Movies TV</a></div><div class='categoryLink' onClick="window.location='/old-english-fonts.php';"><a href='/old-english-fonts.php'>Old English</a></div><div class='categoryLink' onClick="window.location='/old-school-fonts.php';"><a href='/old-school-fonts.php'>Old School</a></div><div class='categoryLink' onClick="window.location='/outline-fonts.php';"><a href='/outline-fonts.php'>Outline</a></div><div class='categoryLink' onClick="window.location='/pointed-fonts.php';"><a href='/pointed-fonts.php'>Pointed</a></div><div class='categoryLink' onClick="window.location='/retro-fonts.php';"><a href='/retro-fonts.php'>Retro</a></div><div class='categoryLink' onClick="window.location='/rock-stone-fonts.php';"><a href='/rock-stone-fonts.php'>Rock Stone</a></div></td><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/rounded-fonts.php';"><a href='/rounded-fonts.php'>Rounded</a></div><div class='categoryLink' onClick="window.location='/russian-fonts.php';"><a href='/russian-fonts.php'>Russian</a></div><div class='categoryLink' onClick="window.location='/sans-serif-fonts.php';"><a href='/sans-serif-fonts.php'>Sans Serif</a></div><div class='categoryLink' onClick="window.location='/school-fonts.php';"><a href='/school-fonts.php'>School</a></div><div class='categoryLink' onClick="window.location='/sci-fi-fonts.php';"><a href='/sci-fi-fonts.php'>Sci Fi</a></div><div class='categoryLink' onClick="window.location='/scratched-fonts.php';"><a href='/scratched-fonts.php'>Scratched</a></div><div class='categoryLink' onClick="window.location='/script-fonts.php';"><a href='/script-fonts.php'>Script</a></div><div class='categoryLink' onClick="window.location='/serif-fonts.php';"><a href='/serif-fonts.php'>Serif</a></div></td><td style="width: 11.11%;"><div class='categoryLink' onClick="window.location='/square-fonts.php';"><a href='/square-fonts.php'>Square</a></div><div class='categoryLink' onClick="window.location='/tattoo-fonts.php';"><a href='/tattoo-fonts.php'>Tattoo</a></div><div class='categoryLink' onClick="window.location='/trash-fonts.php';"><a href='/trash-fonts.php'>Trash</a></div><div class='categoryLink' onClick="window.location='/typewriter-fonts.php';"><a href='/typewriter-fonts.php'>Typewriter</a></div><div class='categoryLink' onClick="window.location='/usa-fonts.php';"><a href='/usa-fonts.php'>USA</a></div><div class='categoryLink' onClick="window.location='/valentine-fonts.php';"><a href='/valentine-fonts.php'>Valentine</a></div><div class='categoryLink' onClick="window.location='/various-fonts.php';"><a href='/various-fonts.php'>Various</a></div><div class='categoryLink' onClick="window.location='/western-fonts.php';"><a href='/western-fonts.php'>Western</a></div></td></tr></table>                                        <div class="clear"><!-- --></div>
                                    </td>
                                </tr>
                            </table>

                            <!-- responsive categories -->
                            <div class="responsiveCategories">
                                        <div class='categoryLink' onClick="window.location='/3d-fonts.php';"><a href='/3d-fonts.php'>3D</a></div><div class='categoryLink' onClick="window.location='/alien-fonts.php';"><a href='/alien-fonts.php'>Alien</a></div><div class='categoryLink' onClick="window.location='/animal-fonts.php';"><a href='/animal-fonts.php'>Animal</a></div><div class='categoryLink' onClick="window.location='/army-stencil-fonts.php';"><a href='/army-stencil-fonts.php'>Army Stencil</a></div><div class='categoryLink' onClick="window.location='/asian-arabic-fonts.php';"><a href='/asian-arabic-fonts.php'>Asian Arabic</a></div><div class='categoryLink' onClick="window.location='/bitmap-pixel-fonts.php';"><a href='/bitmap-pixel-fonts.php'>Bitmap Pixel</a></div><div class='categoryLink' onClick="window.location='/blackletter-fonts.php';"><a href='/blackletter-fonts.php'>Blackletter</a></div><div class='categoryLink' onClick="window.location='/blurred-fonts.php';"><a href='/blurred-fonts.php'>Blurred</a></div><div class='categoryLink' onClick="window.location='/brush-fonts.php';"><a href='/brush-fonts.php'>Brush</a></div><div class='categoryLink' onClick="window.location='/calligraphy-fonts.php';"><a href='/calligraphy-fonts.php'>Calligraphy</a></div><div class='categoryLink' onClick="window.location='/celtic-irish-fonts.php';"><a href='/celtic-irish-fonts.php'>Celtic Irish</a></div><div class='categoryLink' onClick="window.location='/chalk-fonts.php';"><a href='/chalk-fonts.php'>Chalk Crayon</a></div><div class='categoryLink' onClick="window.location='/christmas-fonts.php';"><a href='/christmas-fonts.php'>Christmas</a></div><div class='categoryLink' onClick="window.location='/comic-cartoon-fonts.php';"><a href='/comic-cartoon-fonts.php'>Comic Cartoon</a></div><div class='categoryLink' onClick="window.location='/computer-fonts.php';"><a href='/computer-fonts.php'>Computer</a></div><div class='categoryLink' onClick="window.location='/curly-fonts.php';"><a href='/curly-fonts.php'>Curly</a></div><div class='categoryLink' onClick="window.location='/decorative-fonts.php';"><a href='/decorative-fonts.php'>Decorative</a></div><div class='categoryLink' onClick="window.location='/dingbats-fonts.php';"><a href='/dingbats-fonts.php'>Dingbats</a></div><div class='categoryLink' onClick="window.location='/disney-fonts.php';"><a href='/disney-fonts.php'>Disney</a></div><div class='categoryLink' onClick="window.location='/distorted-eroded-fonts.php';"><a href='/distorted-eroded-fonts.php'>Distorted Eroded</a></div><div class='categoryLink' onClick="window.location='/dotted-fonts.php';"><a href='/dotted-fonts.php'>Dotted</a></div><div class='categoryLink' onClick="window.location='/easter-fonts.php';"><a href='/easter-fonts.php'>Easter</a></div><div class='categoryLink' onClick="window.location='/famous-fonts.php';"><a href='/famous-fonts.php'>Famous</a></div><div class='categoryLink' onClick="window.location='/fancy-fonts.php';"><a href='/fancy-fonts.php'>Fancy</a></div><div class='categoryLink' onClick="window.location='/fantasy-fonts.php';"><a href='/fantasy-fonts.php'>Fantasy</a></div><div class='categoryLink' onClick="window.location='/fire-fonts.php';"><a href='/fire-fonts.php'>Fire</a></div><div class='categoryLink' onClick="window.location='/fixed-width-fonts.php';"><a href='/fixed-width-fonts.php'>Fixed Width</a></div><div class='categoryLink' onClick="window.location='/gothic-fonts.php';"><a href='/gothic-fonts.php'>Gothic</a></div><div class='categoryLink' onClick="window.location='/graffiti-fonts.php';"><a href='/graffiti-fonts.php'>Graffiti</a></div><div class='categoryLink' onClick="window.location='/greek-roman-fonts.php';"><a href='/greek-roman-fonts.php'>Greek Roman</a></div><div class='categoryLink' onClick="window.location='/groovy-fonts.php';"><a href='/groovy-fonts.php'>Groovy</a></div><div class='categoryLink' onClick="window.location='/halloween-fonts.php';"><a href='/halloween-fonts.php'>Halloween</a></div><div class='categoryLink' onClick="window.location='/handwriting-fonts.php';"><a href='/handwriting-fonts.php'>Handwriting</a></div><div class='categoryLink' onClick="window.location='/headline-fonts.php';"><a href='/headline-fonts.php'>Headline</a></div><div class='categoryLink' onClick="window.location='/horror-fonts.php';"><a href='/horror-fonts.php'>Horror</a></div><div class='categoryLink' onClick="window.location='/ice-snow-fonts.php';"><a href='/ice-snow-fonts.php'>Ice Snow</a></div><div class='categoryLink' onClick="window.location='/italic-fonts.php';"><a href='/italic-fonts.php'>Italic</a></div><div class='categoryLink' onClick="window.location='/lcd-fonts.php';"><a href='/lcd-fonts.php'>LCD</a></div><div class='categoryLink' onClick="window.location='/medieval-fonts.php';"><a href='/medieval-fonts.php'>Medieval</a></div><div class='categoryLink' onClick="window.location='/mexican-fonts.php';"><a href='/mexican-fonts.php'>Mexican</a></div><div class='categoryLink' onClick="window.location='/modern-fonts.php';"><a href='/modern-fonts.php'>Modern</a></div><div class='categoryLink' onClick="window.location='/movies-tv-fonts.php';"><a href='/movies-tv-fonts.php'>Movies TV</a></div><div class='categoryLink' onClick="window.location='/old-english-fonts.php';"><a href='/old-english-fonts.php'>Old English</a></div><div class='categoryLink' onClick="window.location='/old-school-fonts.php';"><a href='/old-school-fonts.php'>Old School</a></div><div class='categoryLink' onClick="window.location='/outline-fonts.php';"><a href='/outline-fonts.php'>Outline</a></div><div class='categoryLink' onClick="window.location='/pointed-fonts.php';"><a href='/pointed-fonts.php'>Pointed</a></div><div class='categoryLink' onClick="window.location='/retro-fonts.php';"><a href='/retro-fonts.php'>Retro</a></div><div class='categoryLink' onClick="window.location='/rock-stone-fonts.php';"><a href='/rock-stone-fonts.php'>Rock Stone</a></div><div class='categoryLink' onClick="window.location='/rounded-fonts.php';"><a href='/rounded-fonts.php'>Rounded</a></div><div class='categoryLink' onClick="window.location='/russian-fonts.php';"><a href='/russian-fonts.php'>Russian</a></div><div class='categoryLink' onClick="window.location='/sans-serif-fonts.php';"><a href='/sans-serif-fonts.php'>Sans Serif</a></div><div class='categoryLink' onClick="window.location='/school-fonts.php';"><a href='/school-fonts.php'>School</a></div><div class='categoryLink' onClick="window.location='/sci-fi-fonts.php';"><a href='/sci-fi-fonts.php'>Sci Fi</a></div><div class='categoryLink' onClick="window.location='/scratched-fonts.php';"><a href='/scratched-fonts.php'>Scratched</a></div><div class='categoryLink' onClick="window.location='/script-fonts.php';"><a href='/script-fonts.php'>Script</a></div><div class='categoryLink' onClick="window.location='/serif-fonts.php';"><a href='/serif-fonts.php'>Serif</a></div><div class='categoryLink' onClick="window.location='/square-fonts.php';"><a href='/square-fonts.php'>Square</a></div><div class='categoryLink' onClick="window.location='/tattoo-fonts.php';"><a href='/tattoo-fonts.php'>Tattoo</a></div><div class='categoryLink' onClick="window.location='/trash-fonts.php';"><a href='/trash-fonts.php'>Trash</a></div><div class='categoryLink' onClick="window.location='/typewriter-fonts.php';"><a href='/typewriter-fonts.php'>Typewriter</a></div><div class='categoryLink' onClick="window.location='/usa-fonts.php';"><a href='/usa-fonts.php'>USA</a></div><div class='categoryLink' onClick="window.location='/valentine-fonts.php';"><a href='/valentine-fonts.php'>Valentine</a></div><div class='categoryLink' onClick="window.location='/various-fonts.php';"><a href='/various-fonts.php'>Various</a></div><div class='categoryLink' onClick="window.location='/western-fonts.php';"><a href='/western-fonts.php'>Western</a></div>
                            </div>                                    
                            <div class="clear"><!-- --></div>

                        </div>
                        <div class="clear"><!-- --></div>

                        <div class="categoryBarInner" style="margin-top: 14px;"> 
                            <span style="color:#000000; font-weight: bold; font-family: Arial, Helvetica, sans-serif; font-size: small;">Looking for quality fonts for commercial use? We highly recommend the Ultimate Font Download. Download 10,000 fonts with one click for just $19.95. The Ultimate Font Download is the largest and best selling font collection online. The fonts are licensed for personal and commercial use. Download 10,000 fonts today. Instant and unlimited access to 10,000 fonts. <a href="http://www.ultimatefontdownload.com/" target="_blank"> Click Here For Details</a></span></div>
                        <div class="clear"><!-- --></div>
                                                                                                    <div class="topAd" style="text-align: center; margin-top: 14px; height: 90px;">
                                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 1001ff-970x90 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-2673198289811103"
     data-ad-slot="4706135471"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>                                </div>
    
<div class="responsiveDisplay" style="text-align: center; margin-top: 14px;">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 300x250-Mobile -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-2673198289811103"
     data-ad-slot="5562893474"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>
                        <div class="clear"><!-- --></div>

                        <!-- body section -->
                        <div class="bodyBarWrapper">
                            <div class="mainPageContainer">
                                <div class="mainPageContent">
                                    <table class="mainLayoutTable">
                                        <tr>
                                            <td class="cell1">	<div class='genericPageTitle'>
		<div class='contentLeft'>
			<h1>
				Terms & Conditions / Privacy
			</h1>
		</div>
		<div class='clear'><!-- --></div>
	</div><div class='pageMainContent'>By using this site and downloading the fonts, you acknowledged have read and agreed to our terms and conditions. We reserve the right to amend these terms from time to time.<br/><br/><strong>Disclaimer</strong><br/>This disclaimer is a legal agreement between you and this site. By downloading fonts from our site or using them you agree you have read and understood that the font or dingbat copyrights belongs to the designer of the related product. In cases where there are no copyright notices, you need to assume that the font is copyrighted. Use of the fonts/dingbats is at your sole responsibility. We do not take any responsibility and we are not liable for any damage caused through use of the fonts, be it indirect, incidental or consequential damages (including but not limited to damages for loss of business, loss of profits, interruption or similar). To our knowledge, all fonts available on this site are free for distribution. Should you encounter a font that isn't, please contact us so that it can be immediately deleted.<br/><br/><strong>Privacy Policy</strong><br/>	Your privacy is an important part of our relationship with you. Protecting your privacy is just one part of our mission to provide you a safe web environment. When you email us, your information will remain confidential. We, will not share, rent or trade any of your information with any third parties. When you visit our website, we collect some technical information related to your computer and the manner in which you are accessing our site and includes such information as the internet protocol (IP) address of your computer, what operating system your computer is using, what browser software (e.g., Explorer, Netscape or other) your computer is using, and who your Internet service provider is, the Uniform Resource Locator ('URL') of the web site from which you just came and the URL to which you go next and certain operating metrics, such as the amount of time you use our website. This general information may be used to help us better understand how our site is viewed and used. We may share some general information about our site with our business partners or the general public. For example, we may share information regarding the daily number of unique users to our site with potential corporate partners or advertising opportunities. This information does not contain any of your personal data that can be used to contact you or identify you.<br/><br/>When we place links or advertising banners to other sites from our website, please note that we do not control the content, practices or privacy policies of any of these other sites. We do not endorse or hold ourselves responsible for the privacy policies or information gathering practices of any other websites, other than sites run by us.<br/><br/>We use the highest security standard available to protect your personally identifiable information while it is in transit to us. All data stored on our servers is protected by a secure 'firewall' so that no unauthorized use or activity can take place. Although we will make every effort to safeguard your personal information from loss, misuse or alteration by third parties, you should be aware that there is always some risk that thieves may find a way to thwart our security system or that transmissions over the Internet will be intercepted.<br/><br/>We may, from time to time, amend our Privacy Policy. We reserve the right, at our discretion, to change, modify, add or remove portions of our Privacy Policy at any time and from time to time. These changes will be posted for general public view. When you visit our website, you agree to all terms of our privacy policy and our disclaimer. Your continued use of this website constitutes your continued agreement to these terms. If you do not agree with the terms of our Privacy Policy, you should cease using our website.<br/><br/><strong>Cookie Policy</strong><br/>To make this site work properly, we sometimes place small data files called cookies on your device. Most big websites do this too.<br/><br/>What are cookies?<br/>A cookie is a small text file that a website saves onyour computer or mobile device when you visit the site. It enables the website to remember your actions and preferences (such as login, language, font size and other display preferences) over a period of time, so you don't have to keep re-entering them whenever you come back to the site or browse from one page to another.<br/><br/>How do we use cookies?<br/>A number of our pages use cookies:<br/><ul><li>to remember your display preferences, such as font size or color.</li><li>to remember if you have agreed (or not) to our use of cookies on this site.</li><li>by external advertisers such as Google Ads.</li><li>by any login process to remember your current session.</li></ul>Enabling these cookies is not strictly necessary for the website to work but it will provide you with a better browsing experience. You can delete or block these cookies, but if you do that some features of this site may not work as intended. The cookie-related information is not used to identify you personally and the pattern data is fully under our control. These cookies are not used for any purpose other than those described here.<br/><br/>Do we use other cookies?<br/>Some of our pages or subsites may use additional or different cookies to the ones described above. If so, the details of these will be provided in their specific cookies notice page. You may be asked for your agreement to store these cookies.<br/><br/>How to control cookies<br/>You can control and/or delete cookies as you wish - for details, see aboutcookies.org. You can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some services and functionalities may not work.<br/><br/></div></td>
</tr>
</table>

</div>
</div>
</div>

<div class="clear"><!-- --></div>

</div>
</div>

<!-- footer ads -->
            <div class="footerPageAds">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 970x250 2 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:250px"
     data-ad-client="ca-pub-2673198289811103"
     data-ad-slot="6139284869"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>        </div>
    
<div class="responsiveDisplay" style="text-align: center; margin-top: 10px;">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 300x250 1 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-2673198289811103"
     data-ad-slot="6940756332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></div>

<div class="footer">
    <div class="footerSocial">
        <span class="st_facebook_hcount">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton" onclick="showWindowUrl('http://www.facebook.com/sharer.php?u=https://www.1001freefonts.com/terms-and-conditions.php'); return false;">
                <span>
                    <span class="stMainServices st-facebook-counter" style="background-image: url('https://font-1001freefonts.netdna-ssl.com/themes/blue/images/facebook_counter.png');">&nbsp;</span>
                    <span class="stArrow">
                        <span class="stButton_gradient stHBubble" style="display: inline-block;">
                            <span class="stBubble_hcount">169K</span>
                        </span>
                    </span>
                </span>
            </span>
        </span>
        <span class="st_twitter_hcount">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton" onclick="showWindowUrl('https://twitter.com/share?url=https://www.1001freefonts.com/terms-and-conditions.php&amp;text=1001+Free+Fonts&amp;hashtags=1001freefonts'); return false;">
                <span>
                    <span class="stMainServices st-twitter-counter" style="background-image: url('https://font-1001freefonts.netdna-ssl.com/themes/blue/images/twitter_counter.png');">&nbsp;</span>
                    <span class="stArrow">
                        <span class="stButton_gradient stHBubble" style="display: inline-block;">
                            <span class="stBubble_hcount">17.4K</span>
                        </span>
                    </span>
                </span>
            </span>
        </span>
        <span class="st_linkedin_hcount">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton" onclick="showWindowUrl('http://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.1001freefonts.com/terms-and-conditions.php'); return false;">
                <span>
                    <span class="stMainServices st-linkedin-counter" style="background-image: url('https://font-1001freefonts.netdna-ssl.com/themes/blue/images/linkedin_counter.png');">&nbsp;</span>
                    <span class="stArrow">
                        <span class="stButton_gradient stHBubble" style="display: inline-block;">
                            <span class="stBubble_hcount">3663</span>
                        </span>
                    </span>
                </span>
            </span>
        </span>
        <span class="st_pinterest_hcount">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton" onclick="window.open('https://www.pinterest.com'); return false;">
                <span>
                    <span class="stMainServices st-pinterest-counter" style="background-image: url('https://font-1001freefonts.netdna-ssl.com/themes/blue/images/pinterest_counter.png');">&nbsp;</span>
                    <span class="stArrow">
                        <span class="stButton_gradient stHBubble" style="display: inline-block;">
                            <span class="stBubble_hcount">10.4K</span>
                        </span>
                    </span>
                </span>
            </span>
        </span>
        <span class="st_email_hcount">
            <span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton" onclick="window.location='mailto:?Subject=1001FreeFonts&amp;Body=I%20saw%20this%20and%20thought%20of%20you!%20 https://www.1001freefonts.com/terms-and-conditions.php'; return false;">
                <span>
                    <span class="stMainServices st-email-counter" style="background-image: url('https://font-1001freefonts.netdna-ssl.com/themes/blue/images/email_counter.png');">&nbsp;</span>
                    <span class="stArrow">
                        <span class="stButton_gradient stHBubble" style="display: inline-block;">
                            <span class="stBubble_hcount">7648</span>
                        </span>
                    </span>
                </span>
            </span>
        </span>
    </div>
    
        <div class="keywordCloudWrapper">
        <div class="keywordCloud">
            <div class="topSearches">
                Popular Categories:
            </div>
            <div class="keyword"><a href="/logo-fonts.php">Logo</a></div><div class="keyword"><a href="/branding-fonts.php">Branding</a></div><div class="keyword"><a href="/handwritten-fonts.php">Handwritten</a></div><div class="keyword"><a href="/bold-fonts.php">Bold</a></div><div class="keyword"><a href="/poster-fonts.php">Poster</a></div><div class="keyword"><a href="/typeface-fonts.php">Typeface</a></div><div class="keyword"><a href="/vintage-fonts.php">Vintage</a></div><div class="keyword"><a href="/display-fonts.php">Display</a></div><div class="keyword"><a href="/elegant-fonts.php">Elegant</a></div><div class="keyword"><a href="/simple-fonts.php">Simple</a></div><div class="keyword"><a href="/business-card-fonts.php">Business Card</a></div><div class="keyword"><a href="/flyer-fonts.php">Flyer</a></div><div class="keyword"><a href="/lettering-fonts.php">Lettering</a></div><div class="keyword"><a href="/sans-fonts.php">Sans</a></div><div class="keyword"><a href="/fashion-fonts.php">Fashion</a></div><div class="keyword"><a href="/quote-fonts.php">Quote</a></div><div class="keyword"><a href="/stylish-fonts.php">Stylish</a></div><div class="keyword"><a href="/wedding-fonts.php">Wedding</a></div><div class="keyword"><a href="/cool-fonts.php">Cool</a></div><div class="keyword"><a href="/fun-fonts.php">Fun</a></div><div class="keyword"><a href="/handmade-fonts.php">Handmade</a></div><div class="keyword"><a href="/logotype-fonts.php">Logotype</a></div><div class="keyword"><a href="/magazine-fonts.php">Magazine</a></div><div class="keyword"><a href="/trendy-fonts.php">Trendy</a></div><div class="keyword"><a href="/classy-fonts.php">Classy</a></div><div class="keyword"><a href="/grunge-fonts.php">Grunge</a></div><div class="keyword"><a href="/hipster-fonts.php">Hipster</a></div><div class="keyword"><a href="/signature-fonts.php">Signature</a></div><div class="keyword"><a href="/alphabet-fonts.php">Alphabet</a></div><div class="keyword"><a href="/beautiful-fonts.php">Beautiful</a></div><div class="keyword"><a href="/clothing-fonts.php">Clothing</a></div><div class="keyword"><a href="/design-fonts.php">Design</a></div><div class="keyword"><a href="/happy-fonts.php">Happy</a></div><div class="keyword"><a href="/invitation-fonts.php">Invitation</a></div><div class="keyword"><a href="/light-fonts.php">Light</a></div><div class="keyword"><a href="/minimalist-fonts.php">Minimalist</a></div><div class="keyword"><a href="/sign-fonts.php">Sign</a></div><div class="keyword"><a href="/signage-fonts.php">Signage</a></div><div class="keyword"><a href="/stationery-fonts.php">Stationery</a></div><div class="keyword"><a href="/art-fonts.php">Art</a></div><div class="keyword"><a href="/brand-fonts.php">Brand</a></div><div class="keyword"><a href="/cafe-fonts.php">Cafe</a></div><div class="keyword"><a href="/cartoon-fonts.php">Cartoon</a></div><div class="keyword"><a href="/contemporary-fonts.php">Contemporary</a></div><div class="keyword"><a href="/cursive-fonts.php">Cursive</a></div><div class="keyword"><a href="/futuristic-fonts.php">Futuristic</a></div><div class="keyword"><a href="/graphic-fonts.php">Graphic</a></div><div class="keyword"><a href="/great-fonts.php">Great</a></div><div class="keyword"><a href="/hand-lettering-fonts.php">Hand Lettering</a></div><div class="keyword"><a href="/letter-fonts.php">Letter</a></div>        </div>
    </div>
    <div class="clear"></div>
    
    
    <a href="/terms-and-conditions.php">Terms & Conditions / Privacy</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/contact.php">Contact</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/fontslista.php">Complete List</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/popular-categories.php">Popular Categories</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://mfscripts.com" target="_blank">MFScripts</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://yetishare.com" target="_blank">YetiShare</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.fontspace.com" target="_blank">Font 
        Space</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="http://www.fontriver.com" target="_blank">Font 
        River</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="https://www.thefreesite.com" target="_blank">The Free Site</a><br/>
    <br/>

            <div class="headerMultiLanguageSwitcherWrapper">
            <div class="headerMultiLanguageSwitcher">
                <a href="https://www.1001freefonts.com/terms-and-conditions.php" class="flagSelected"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/flags/en.png" width="16" height="11" alt="English (en)" title="Switch site language to English (en)"/><span>English (en)</span>&nbsp;</a><a href="https://www.1001freefonts.com/es/terms-and-conditions.php" class="flagNoneSelected"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/flags/es.png" width="16" height="11" alt="Español (es)" title="Switch site language to Español (es)"/><span>Español (es)</span>&nbsp;</a><a href="https://www.1001freefonts.com/fr/terms-and-conditions.php" class="flagNoneSelected"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/flags/fr.png" width="16" height="11" alt="Français (fr)" title="Switch site language to Français (fr)"/><span>Français (fr)</span>&nbsp;</a><a href="https://www.1001freefonts.com/de/terms-and-conditions.php" class="flagNoneSelected"><img src="https://font-1001freefonts.netdna-ssl.com/themes/blue/images/flags/de.png" width="16" height="11" alt="Deutsch (de)" title="Switch site language to Deutsch (de)"/><span>Deutsch (de)</span>&nbsp;</a>            </div>
        </div><br/><br/>
            
    <span style="font-weight: bold;">Stats:</span>&nbsp;&nbsp;929 visitors online<br/>
<br/>
<a href="#pageTop" style="text-decoration:none;">^ Top ^</a>
</div>

</div></div>

<script src="https://font-1001freefonts.netdna-ssl.com/themes/blue/javascript/global-min.js?r=2"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-105600254-1', 'auto');
    ga('send', 'pageview');
</script>
<script>
    $(document).ready(function () {
        $(".languageDropdown .topNode").click(function ()
        {
            $(".languageDropdown .menu").fadeToggle("fast");
        });

        $.cookieBar({
            fixed: true, bottom: true, append: true, policyButton: true, policyText: 'Privacy Policy', policyURL: 'https://www.1001freefonts.com/terms_and_conditions.php'
        });
    });
</script>
</body>
</html>

