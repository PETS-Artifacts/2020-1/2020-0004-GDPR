<!DOCTYPE html>
<html lang="en-US" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://ogp.me/ns#" xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Privacy Policy</title>
<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="Largest &amp; most trusted Nanny Service in Canada. Helping families find full &amp; part-time Nannies, Babysitters &amp; Caregivers since 2002. Find a Nanny Today!" name="description">
<meta content="nanny, sitter" name="keywords">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
<meta content="207.241.229.51" id="client-ip" name="client-ip">
<meta content="#A27BB7" name="theme-color">
<meta content="D183BCEAA80D3E3C946FC37965D2BC0A" name="msvalidate.01">
<meta content="authenticity_token" name="csrf-param" />
<meta content="19Hem+t3HybbQ9PU5LCtcHTfWIkRghEjT1GvNKeL/Ug=" name="csrf-token" />
<link href="https://canadiannanny.ca/privacy" rel="canonical">
<meta content="1050486448317951" id="fb-app-id" property="fb:app_id">
<meta content="724000252,660620766" property="fb:admins">
<meta content="CanadianNanny.ca" property="og:site_name">
<meta content="https://canadiannanny.ca/privacy" property="og:url">
<meta content="CanadianNanny.ca" property="og:title">
<meta content="Largest &amp; most trusted Nanny Service in Canada. Helping families find full &amp; part-time Nannies, Babysitters &amp; Caregivers since 2002. Find a Nanny Today!" property="og:description">
<meta content="en_US" property="og:locale">
<meta content="https://d3c1grj748hoip.cloudfront.net/assets/open-graph/og-nanny-1200x630-39e0c0b7a21be342e1ed8e09ceddf105e42d556ad7efd6dd66c045dcd98062af.jpg" property="og:image">

<link href="//d3c1grj748hoip.cloudfront.net/assets/canadiannanny/application-bb0edae3afa4b7c2085bd400d06487628fb68edf1e3608490c2c67c2fe3d5704.css" media="screen" rel="stylesheet" />
<link href="//d3c1grj748hoip.cloudfront.net/assets/canadiannanny/application-bb0edae3afa4b7c2085bd400d06487628fb68edf1e3608490c2c67c2fe3d5704.css" media="print" rel="stylesheet" />
<link href="//d3c1grj748hoip.cloudfront.net/webpack/application-9d24b0906b3db1cd9582.css" media="screen" rel="stylesheet" />
<link href="//d3c1grj748hoip.cloudfront.net/assets/favicons/nanny-favicon-5fbe220eb58bad6d8c7da228d27a775c826df453b529668e1a47ff56d9d50ecc.ico" rel="shortcut icon">


</head>
<body class="site-canadiannanny ">
<div id="fb-root"></div>
<section id="container">
<div class="navbar-fixed-top">
<!--[if lt IE 10]>
<div class="browser-warning-banner row">
Your browser does not support all technologies used on CanadianNanny.ca You may experience some irregular behavior. Please
<a class="browser-warning-link" href="/upgrade-browser">Click Here
</a>for more information.
</div>

<![endif]-->
<header class="header">
<nav role="navigation">
<div class="nav-wrapper">
<div class="nav-logo-container">
<a href="/"><img alt="CanadianNanny.ca" class="masthead-logo-201502" height="70" src="//d3c1grj748hoip.cloudfront.net/assets/logos/logo-nanny-25253ce629489a2fb002d89eff7e16040e48e838fe763f99ba70f5f5bf106408.svg" />
<div class="icon nav-logo"></div>
</a></div>

<div class="nav-container">
<input id="show-menu" role="button" type="checkbox">
<label class="show-menu" for="show-menu">
<div class="menu-toggle">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar top-bar"></span>
<span class="icon-bar middle-bar"></span>
<span class="icon-bar bottom-bar"></span>
</div>
</label>

<div class="nav-links-container">
<ul class="nav-links nav-left-links">
<li class="nav-link">
<a href="/nannies/canada" title="Find Nannies"><span class="link">
Find Nannies
</span>
</a></li>
<li class="nav-link">
<a href="/nanny-jobs/canada" title="Get a Nanny Job"><span class="link">
Get a Nanny Job
</span>
</a></li>
<li class="nav-link">
<a href="/nanny-tax-payroll"><span class="link link-with-new-tag">
<img alt="New!" class="dark hidden-xs" src="//d3c1grj748hoip.cloudfront.net/assets/header/new-bc111d467d15b9455706569a10eb3aed8b13823ecfef2d376284301cc374543e.svg">
<img alt="New!" class="light visible-xs" src="//d3c1grj748hoip.cloudfront.net/assets/header/new-white-103a2fb952990fe52ee495e1df44b28b8a94a336ca7142147212bd2bc5cab1d0.svg">
Payroll
</span>
</a></li>

<li class="nav-link">
<a href="/guide"><span class="link">Guides</span>
</a></li>
<li class="nav-link">
<a href="/blog"><span class="link">Blog</span>
</a></li>
<li class="nav-link">
<a href="https://help.careguide.com/" target="_blank"><span class="link">Contact</span>
</a></li>
</ul>

<ul class="nav-links nav-middle-links">
<li class="nav-link has-drop-down-menu">
<a href="#">
<span class="link">Top Cities</span>
</a>
<div class="drop-down-menu mega-menu">
<div class="mega-menu-cities">
<div class="mega-menu-cities-column">
<h4>Canada</h4>
<ul>
<li>
<a href="/nannies/toronto,ontario" title="Toronto nannies">Toronto</a>
</li>
<li>
<a href="/nannies/calgary,alberta" title="Calgary nannies">Calgary</a>
</li>
<li>
<a href="/nannies/ottawa,ontario" title="Ottawa nannies">Ottawa</a>
</li>
<li>
<a href="/nannies/vancouver,british-columbia" title="Vancouver nannies">Vancouver</a>
</li>
<li>
<a href="/nannies/edmonton,alberta" title="Edmonton nannies">Edmonton</a>
</li>
<li>
<a href="/nannies/montreal,quebec" title="Montreal nannies">Montreal</a>
</li>
<li>
<a href="/nannies/mississauga,ontario" title="Mississauga nannies">Mississauga</a>
</li>
<li>
<a href="/nannies/winnipeg,manitoba" title="Winnipeg nannies">Winnipeg</a>
</li>
<li>
<a href="/nannies/victoria,british-columbia" title="Victoria nannies">Victoria</a>
</li>
<li>
<a href="/nannies/halifax,nova-scotia" title="Halifax nannies">Halifax</a>
</li>
</ul>
</div>
</div>
<div class="mega-menu-search">
Don't see your city?
<a href="/nannies/near">Search here</a>
</div>
</div>

</li>
</ul>

<ul class="nav-links nav-right-links">
<li class="nav-link">
<a href="/log-in"><span class="link">Log In</span>
</a></li>
<li class="get-started">
<a class="sign-up-modal-link btn btn-primary" href="#">Get Started</a>
</li>
</ul>

</div>
</div>
</div>
</nav>
</header>
</div>

<div class="modal fade" id="sign-up-modal" role="dialog" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-body">
<div class="site-badge">
<img alt="Badge nanny" class="site-badge-image" src="//d3c1grj748hoip.cloudfront.net/assets/sign-up-modal/badge-nanny-ee3923b136beaaf2600f469c6b1757079c55e577226943f49036e950204f2ed6.svg" />
</div>
<h3 class="modal-title-primary">
Create a New Listing to
<br>
Join Our
Nanny
Community!
</h3>
<p class="modal-subtitle">
Find or become a
nanny.
</p>
<p>
Have an account?
<a href="/log-in">Log In</a>
</p>
</div>
<div class="modal-footer">
<div class="row">
<a href="/new/job"><div class="col-md-6 column-button left-button">
<span class="btn-title">
Post A Job
</span>
<div class="post-category">
I need a
nanny
</div>
</div>
</a><a href="/new/profile"><div class="col-md-6 column-button right-button">
<span class="btn-title">
Post Your Profile
</span>
<div class="post-category">
I am a
nanny
</div>
</div>
</a></div>
</div>
</div>
</div>
</div>

<section id="main-content">
<div class="wrapper">
<div class="col-xs-12 hidden-print">
<div class="container">
</div>

</div>
<div class="clearfix">
<div class="jumbotron">
<div class="container">
<h1 class="page-title">Privacy Policy</h1>
</div>
</div>
<div class="container">
<p>
Last Update: October 6, 2016
</p>
<p>
CareGuide Inc. ("<strong>CareGuide</strong>") values our customers and we respect your privacy and personal information which is information about an identifiable individual (as defined by applicable privacy or data protection laws).
</p>
<p>
This privacy policy ("<strong>Privacy Policy</strong>" or "<strong>Policy</strong>") describes how CareGuide treats your personal information obtained through the CareGuide.com website and any subdomains [subsidiary websites] thereof, including those linked in the footer of the CareGuide.com website (collectively "<strong>Website</strong>" and any mobile application ("<strong>App</strong>") owned and operated by CareGuide (collectively referred to as "<strong>Platform</strong>") in this Privacy Policy.
The Policy describes the kinds of personal information CareGuide collects about you, why we collect it, how we use it, how we protect it, and under what circumstances we share it with third parties.
This Policy also describes the decisions you can make about your personal information.
You may require us to change, amend or delete the personal information that you have provided to us at any time.
</p>
<p>
The Platform may contain links to third parties' websites or apps. We are not responsible for the privacy practices or the content of those websites. The Platform may also contain links to terms and conditions and privacy policies of third party providers who provide tools or services on a website. Therefore, please read carefully any privacy policies on those links or websites before either agreeing to their terms or using those tools, services or websites.
</p>
<h3>Terms</h3>
<p>
The terms "you", "User", "Service Seeker" and "Service Provider" refer to individuals who use this Platform for the purposes of the Services offered.
</p>
<p>
In this Privacy Policy, "<em>Personal Information</em>" (also referred to as "Personal Data") means any information about an identifiable individual, such as your name, e-mail address, mailing addresses, gender, date of birth, any data about you that you elect to provide electronically through the Platform and any other information that identifies who you are.
Registration Data (as defined in the
<a href="/terms-of-service" target="_blank">Terms of Use</a>
and Personal Information will be used by CareGuide solely in accordance with these
<a href="/terms-of-service" target="_blank">Terms of Use</a>
</p>
<p>
Unless we explain otherwise in this Policy, the words and phrases used in this Policy have the same meaning as in the
<a href="/terms-of-service" target="_blank">Terms of Use</a>
</p>
<h3>Accountability</h3>
<p>
CareGuide is responsible for Personal Information under its control, including the transfer of Personal Information to a third party service provider for processing on CareGuide's behalf.
</p>
<h3>What Information Do We Gather?</h3>
<p>
When you sign up for a User Account through the Platform, you voluntarily give us certain personal information including your name, e-mail address, address, postal code/ZIP code, age and phone number.
Use of the App may involve access to your location through your mobile device.  You will be asked for permission before the App accesses and uses your location information.  In addition, when you visit our Platform or contact us, we collect certain information about your activity on our Platform, as described below under the heading "Our Use of Cookies and Log Files".
</p>
<p>
By using the Messaging Platform available through the Platform, you agree that a copy of your messages and any voice calls made through the Messaging Platform will be recorded and stored for the purposes of safety and protecting our Users from misuse of CareGuide's Services.
The recorded messages will only be stored for a period of time that is necessary to fulfil its purpose.
The collection of information from the Messaging Platform is done so in accordance with the terms in this Policy.
</p>
<p>
If you have registered for the HeartPayroll Services offered by CareGuide, certain sensitive information including your Social Insurance Number,
address and financial information including your bank account information is collected where it is necessary for the functioning of the HeartPayroll Services,
and will not be retained for longer than needed or upon termination of the Payroll Services.
We call this personal information "<strong>Financial Information</strong>".
</p>
<h3>How We Use Your Personal Information</h3>
<p>
We use the Personal Information we collect about you in order to provide you with the Services offered by CareGuide and you agree that we may use your Personal Information for the following purposes:
</p>
<ul>
<li>For the Services offered by CareGuide through the Platform</li>
<li>To support any requests or inquiries made by you</li>
<li>To contact you from time to time with user or Service updates</li>
<li>To assist you and the User(s) you have done (or have agreed to do) business with through CareGuide's Services, including the HeartPayroll Services (where applicable to you)</li>
<li>To customize, measure, report on and improve our services, content and advertising</li>
<li>To learn more about you and your product preferences by looking at the IP address of your computer and your activity on the Website (among other things)</li>
<li>To promote services related to the Website or those of the CareGuide and/or our affiliates</li>
<li>To prevent, investigate or prosecute activity we think may be potentially illegal, unlawful or harmful</li>
<li>To enforce our Privacy Policy and/or our Terms or any other purpose referenced herein or therein.</li>
</ul>
<h3>How and Where is Personal Information Processed?</h3>
<p>
Personal Information will be collected, processed, stored and used by CareGuide and may be passed to and processed by other companies under the instruction of CareGuide.
Your Personal Information may be processed and/or stored outside of Canada.
While CareGuide undertakes measures to protect your Personal Information, when it is stored and/or processed in other jurisdictions, the laws of other countries may not provide the degree of protection for Personal Information that is available in Canada.
If your Personal Information is transferred outside of Canada, it may be available to the foreign government of the country in which the information or entity controlling it is situated under a lawful order made in that country and used for purposes other than those described herein.
By providing us with your information, you are allowing your Personal Information to be transferred outside of Canada.
</p>
<p>
CareGuide may use Google Analytics to gather statistics on web traffic, Platform usage and information on User behavior.
Google Analytics is a web analysis service provided by Google Inc. ("<strong>Google</strong>"). Google utilizes the Data collected to track and examine Application use, to prepare reports on an Application's activities and share them with other Google services.
CareGuide may also use AdRoll, an advertising retargeting company, that enables CareGuide to present you with retargeting advertising on other websites based on your previous interaction with the Website.
In addition, CareGuide and its Website allows for User interaction with social media networks and other external platforms directly from the Website.
The interaction and information obtained is subject to the terms and conditions and privacy policies of Google, Adroll, and the social media and third-party platforms, respectively.
</p>
<h3>Sharing Your Personal Information</h3>
<p>
In accordance with this Policy, we may share your Personal Information including Financial Information, with our service providers, and third parties involved in providing Payroll Services to you.
We may also share information with our financial, insurance, legal, accounting or other advisors that provide professional services to us.
</p>
<p>
We will not disclose, share, sell or rent your Personal Information in personally identifiable form with any third party, except if, and to the extent necessary, in our good faith judgment, doing so is required to:
complete the Services offered by CareGuide, comply with laws or regulations, respond to a valid subpoena, order, or government request; establish or exercise the Company’s legal rights or defend against legal claims;
investigate, detect, suppress, prevent or take action regarding illegal or prohibited activities, suspected fraud, situations involving potential threats to the reputation or physical safety of any person;
or as otherwise required by law.
</p>
<p>
We may remove personal identifiers from your information and maintain and use it in anonymous form that may later be combined with other information to generate aggregated information.
Such anonymous and aggregated information may be sold, traded, rented, or shared.
</p>
<p>
We will retain your personal information only for a time and to the extent necessary for the purposes for which it was collected as described in this Policy and for reasonable backup, archival, audit, or other similar purposes.
</p>
<h3>Our Use of Cookies and Log Files</h3>
<p>
We use browser tracking cookies (or "cookies"), which are small text files that are placed on the hard disk of a computer by a website.
Cookies are uniquely assigned to you, and can only be read by a website or web server that issued the cookie to you.
We also use browser "log files" which record certain information when you visit a website, including your internet protocol (IP) address.
To improve your experience on our Platform, we use cookies and logs files to:
recognize you when you return to our Platform;
keep track of activity on our Platform and remember what items you have clicked on or viewed;
study how you navigate through our Platform and which products you request in site searches so that we can improve the design, content and function of our Platform;
and customize the message, content and delivery of online banner advertisements and e-mails that reflect how you navigate to and through our Platform based on your online behavior.
We call this "Browsing Data".
We may hire third party service providers to assist us in the collection and analysis of this Browsing Data collected through cookies, but none of your Personal Information is disclosed to these third party service providers.
</p>
<p>
You have the ability to accept or decline our use of cookies.
You can refuse cookies by turning them off or blocking them in your internet browser.
If you decide to turn off or block cookies, our Platform might not function correctly.
</p>
<h3>Accessing and Updating Your Personal Information</h3>
<p>
At any time you can contact us to:
stop receiving e-mails from us; review the personal information held by the Company in connection with your account; withdraw your consent for our use and disclosure of your information;
request a list of third parties to which CareGuide may have provided your personal information; close your account; and amend your personal information, where possible, by logging into your
<a href="/settings">User Account.</a>
</p>
<p>
If you contact us to do any of the things listed above, we may require you to provide sufficient information to allow us to identify you and tell you about the existence, use and disclosure of your Personal Information and this Personal Information will only be used for this purpose.
If you contact us about your Personal Information, we will respond to your request within a reasonable time and at no cost to you.
</p>
<h3>Links and Third-Party Websites</h3>
<p>
We provide links on our Platform to other, third party sites we think you will enjoy.
These sites operate independently of us and have established their own privacy and security policies.
Any personal information you provide on linked pages or other sites is provided directly to that third party and subject to that third party's privacy policy.
We strongly encourage you to review these policies at any site you visit.
This Policy does not apply to such linked pages or other sites, and we are not responsible for the content or practices of any linked websites which are provided solely for your convenience.
</p>
<p>
This Privacy Policy is not intended to and does not create any contractual or other legal rights in or on behalf of any party.
</p>
<h3>Data Integrity and Security</h3>
<p>
We aim to provide you with a safe experience.
We have in place certain physical, electronic, technological, and organizational safeguards to appropriately protect the security and privacy of your personal information against loss, theft, and unauthorized access, disclosure, copying use or modification.
Please note, however, that while we try to create a secure and reliable Platform for users, the confidentiality of any communication or material transmitted to or from the Platform or via e-mail cannot be guaranteed.
</p>
<p>
We limit access to your personal information within CareGuide to individuals on a need-to-know basis.
</p>
<h3>Children's Online Privacy Protection</h3>
<p>
The Platform is not intended for use by children under the age of 18 without the consent of their parent or legal guardian.
CareGuide does not knowingly collect or use any personal information from any children under the age of 13.
If we become aware that we have unknowingly collected personal information from a child under the age of 13, we will make commercially reasonably efforts to delete such personal information from our database.
</p>
<h3>Changes to the Privay Policy</h3>
<p>
CareGuide reserves the right to make changes to this Privacy Policy at any time by giving notice to its Users on this page, and by ensuring analogous protection of the Personal Information in all cases.
It is strongly recommended to check this page often, referring to the date of the last modification listed at the top of the page.
</p>
<h3>Contact Us</h3>
<p>
If you have questions or comments about this Privacy Policy or your Personal Information, please contact CareGuide at
<a href="https://careguide.com/contact" target="_blank">Contact Us</a>
, by telephone at
<a href="tel:+1-855-874-8837">+1 855-874-8837</a>
, or by mail to CareGuide at:
</p>
<address>
<em>Attention: Privacy Officer</em>
<br>
119 Spadina Ave, Unit 1100
<br>
Toronto, ON
<br>
M5V 2L1
</address>
</div>


</div>
</div>
</section>
</section>
<table id="colorbar">
<tr>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
</table>
<div class="top-cities-bar-wrapper">
<div class="container">
<div class="row">
<div class="col-xs-12">
<div class="top-cities-bar">
<table class="table table-condensed" style="border-collapse:collapse;">
<tbody>
<tr class="accordion-toggle" data-target="#top-cities" data-toggle="collapse">
<td>
<div class="fa fa-chevron-down"></div>
&nbsp;
<span class="top-cities-title">Top Cities for CanadianNanny.ca</span>
</td>
</tr>
<tr>
<td class="hiddenRow" colspan="12">
<div class="accordian-body collapse" id="top-cities">
<div class="col-xs-12">
<div class="top-cities-header">Canada</div>
<ul>
<li>
<a href="/nannies/toronto,ontario">Nanny Toronto
</a></li>
<li>
<a href="/nannies/calgary,alberta">Nanny Calgary
</a></li>
<li>
<a href="/nannies/ottawa,ontario">Nanny Ottawa
</a></li>
<li>
<a href="/nannies/vancouver,british-columbia">Nanny Vancouver
</a></li>
<li>
<a href="/nannies/edmonton,alberta">Nanny Edmonton
</a></li>
<li>
<a href="/nannies/montreal,quebec">Nanny Montreal
</a></li>
<li>
<a href="/nannies/mississauga,ontario">Nanny Mississauga
</a></li>
<li>
<a href="/nannies/winnipeg,manitoba">Nanny Winnipeg
</a></li>
<li>
<a href="/nannies/victoria,british-columbia">Nanny Victoria
</a></li>
<li>
<a href="/nannies/halifax,nova-scotia">Nanny Halifax
</a></li>
</ul>
</div>
<div class="col-xs-12">
</div>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
<div class="footer hidden-print">
<section id="care-providers">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<h3>
CanadianNanny.ca is a Part of the CareGuide Family
</h3>
<ul class="big-six">
<li>
<a class="nanny" href="https://CanadianNanny.ca" target="_blank" title="CanadianNanny.ca">CanadianNanny.ca</a>
</li>
<li>
<a class="sitter" href="https://Sitter.com" target="_blank" title="Sitter.com">Sitter.com</a>
</li>
<li>
<a class="housekeeper" href="https://Housekeeper.com" target="_blank" title="Housekeeper.com">Housekeeper.com</a>
</li>
<li>
<a class="petsitter" href="https://Petsitter.com" target="_blank" title="PetSitter.com">PetSitter.com</a>
</li>
<li>
<a class="eldercare" href="https://ElderCare.com" target="_blank" title="ElderCare.com">ElderCare.com</a>
</li>
<li>
<a class="housesitter" href="https://HouseSitter.com" target="_blank" title="HouseSitter.com">HouseSitter.com</a>
</li>
</ul>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
<ul class="meeta">
<li>
<a href="https://MeetAnAccountant.com" target="_blank" title="MeetAnAccountant.com">MeetAnAccountant.com</a>
</li>
<li>
<a href="https://MeetACarpenter.com" target="_blank" title="MeetACarpenter.com">MeetACarpenter.com</a>
</li>
<li>
<a href="https://MeetAChef.com" target="_blank" title="MeetAChef.com">MeetAChef.com</a>
</li>
<li>
<a href="https://MeetAnElectrician.com" target="_blank" title="MeetAnElectrician.com">MeetAnElectrician.com</a>
</li>
<li>
<a href="https://MeetAnEngineer.com" target="_blank" title="MeetAnEngineer.com">MeetAnEngineer.com</a>
</li>
<li>
<a href="https://MeetAGardener.com" target="_blank" title="MeetAGardener.com">MeetAGardener.com</a>
</li>
</ul>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
<ul class="meeta">
<li>
<a href="https://MeetAHandyman.com" target="_blank" title="MeetAHandyman.com">MeetAHandyman.com</a>
</li>
<li>
<a href="https://MeetALandscaper.com" target="_blank" title="MeetALandscaper.com">MeetALandscaper.com</a>
</li>
<li>
<a href="https://MeetAMechanic.com" target="_blank" title="MeetAMechanic.com">MeetAMechanic.com</a>
</li>
<li>
<a href="https://MeetAPainter.com" target="_blank" title="MeetAPainter.com">MeetAPainter.com</a>
</li>
<li>
<a href="https://MeetAPhotographer.com" target="_blank" title="MeetAPhotographer.com">MeetAPhotographer.com</a>
</li>
<li>
<a href="https://MeetAPlumber.com" target="_blank" title="MeetAPlumber.com">MeetAPlumber.com</a>
</li>
</ul>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
<ul class="meeta">
<li>
<a href="https://MeetAProgrammer.com" target="_blank" title="MeetAProgrammer.com">MeetAProgrammer.com</a>
</li>
<li>
<a href="https://MeetARenovator.com" target="_blank" title="MeetARenovator.com">MeetARenovator.com</a>
</li>
<li>
<a href="https://MeetARoofer.com" target="_blank" title="MeetARoofer.com">MeetARoofer.com</a>
</li>
<li>
<a href="https://MeetASalesman.com" target="_blank" title="MeetASalesman.com">MeetASalesman.com</a>
</li>
<li>
<a href="https://MeetASecurityGuard.com" target="_blank" title="MeetASecurityGuard.com">MeetASecurityGuard.com</a>
</li>
</ul>
</div>
<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
<ul class="meeta">
<li>
<a href="https://MeetATruckDriver.com" target="_blank" title="MeetATruckDriver.com">MeetATruckDriver.com</a>
</li>
<li>
<a href="https://MeetATutor.com" target="_blank" title="MeetATutor.com">MeetATutor.com</a>
</li>
<li>
<a href="https://MeetAVideographer.com" target="_blank" title="MeetAVideographer.com">MeetAVideographer.com</a>
</li>
<li>
<a href="https://MeetAWebDeveloper.com" target="_blank" title="MeetAWebDeveloper.com">MeetAWebDeveloper.com</a>
</li>
<li>
<a href="https://MeetAWriter.com" target="_blank" title="MeetAWriter.com">MeetAWriter.com</a>
</li>
</ul>
</div>
</div>
</div>
</section>

<div class="cca-footer-banner-ad hidden-xs hidden-sm">
<a href="https://childcareadvantage.com" target="_blank"><img alt="ChildCareAdvantage.com" src="//d3c1grj748hoip.cloudfront.net/assets/cca/cca-footer-banner-img-17bc9004d576c049e0127fa5087fec608c88b7fdd6b8178f644c1536d334af26.jpg" title="ChildCareAdvantage.com" /></a>
</div>
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="row">
<div class="bottom-footer-wrap">
<div class="col-xs-12 col-sm-2 col-sm-push-4">
<h6>Contact</h6>
<ul>
<li class="email-address">
<a href="mailto:info@careguide.com">info@careguide.com</a>
</li>
<li>
1100 – 119 Spadina Ave
<br>
Toronto, ON
<br>
M5V 2L1
</br>
</li>
</ul>
</div>
<div class="col-xs-12 col-sm-2 col-sm-push-4">
<h6>
Payroll Services
</h6>
<ul>
<li>
<a href="https://www.heartpayroll.com/home" target="_blank">Nanny Tax &amp; Payroll Services</a>
</li>
</ul>
</div>
<div class="col-xs-12 col-sm-2 col-sm-push-4">
<h6>
Support
</h6>
<ul>
<li>
<a href="https://help.careguide.com" target="_blank">Support</a>
</li>
<li>
<a href="/security-info">Security Info</a>
</li>
</ul>
</div>
<div class="col-xs-12 col-sm-2 col-sm-push-4">
<h6>
Social
</h6>
<ul>
<li>
<a class="footer-link" href="https://www.twitter.com/CanadianNanny" target="_blank">Twitter</a>
</li>
<li>
<a class="footer-link" href="https://www.facebook.com/canadiannannyca" target="_blank">Facebook</a>
</li>
<li>
<a class="footer-link" href="http://linkedin.com/company/careguide" target="_blank">Linkedin</a>
</li>
</ul>
</div>
<div class="col-xs-12 col-sm-4 col-sm-pull-8 terms-bucket">
<a href="https://careguide.com"><img alt="CareGuide" class="footer-logo-tagline" src="//d3c1grj748hoip.cloudfront.net/assets/branding/tash/logos/logo_careguide-tagline_RGB-c8f9c27c8adf4221e17375cab5e7506e81a053dd8c28b5c65ee6ba3cefa46678.svg" title="CareGuide" /></a>
<li><a href="/privacy">Privacy Policy</a></li>
&middot;
<li><a href="/terms-of-service">Terms of Service</a></li>
<div class="copyright">
&copy; 2016 <a href="http://careguide.com/">CareGuide Inc.</a>
</div>
</div>
</div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12">
<img alt="Icon cg heart" class="cg-heart-footer" src="//d3c1grj748hoip.cloudfront.net/assets/corporate/icon-cg-heart-e37ed989a794e7bfae464370a4143ff850294cada0493053da4e4052b5c2b8dc.svg" title="Made with Love" />
</div>
</div>
<div class="row">
<div class="col-sm-12">
</div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-230316-4', document.location.hostname);
  ga('create', 'UA-27527914-32', 'auto', {'name': 'allSites'});
  ga('allSites.set', 'hostname', document.location.hostname);

  ga('set', 'dimension3', 'canadiannanny.ca');
  ga('allSites.set', 'dimension3', 'canadiannanny.ca');







  ga('send', 'pageview');
  ga('allSites.send', 'pageview');


  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','https://connect.facebook.net/en_US/fbevents.js');

  fbq('init', '1062763667123916');
  fbq('track', "PageView");

  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-K58G92');
</script>

  <noscript>
    <iframe src="//www.googletagmanager.com/ns.html?id=GTM-K58G92" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1062763667123916&ev=PageView&noscript=1"/>
  </noscript>


<script type="text/javascript" async>
adroll_adv_id = "NQ3CFYXTIJFSJF5IZAFQSB";
adroll_pix_id = "IBDM53SCPBACXNHDI6NF7S";
(function () {
var oldonload = window.onload;
window.onload = function(){
   __adroll_loaded=true;
   var scr = document.createElement("script");
   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
   scr.setAttribute('async', 'true');
   scr.type = "text/javascript";
   scr.src = host + "/j/roundtrip.js";
   ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
   if(oldonload){oldonload()}};
}());
</script>

</body>
<script async="async" data-defaults="{&quot;user_id&quot;:null}" data-pageview="true" id="k6n" src="https://k6n.careguide.com/k6n.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//connect.facebook.net/en_US/sdk.js"></script>
<script src="https://js.stripe.com/v2/?"></script>
<script src="//assets.transloadit.com/js/jquery.transloadit2-v2-latest.js"></script>
<script src="//www.google.com/jsapi"></script>


<script src="//d3c1grj748hoip.cloudfront.net/webpack/application-18cdc2da025e5292076d.js"></script>
<script src="//d3c1grj748hoip.cloudfront.net/assets/application-f108c624a23916a9c2508d88469d1d357c54ec6479707513c1c57a803b25163f.js"></script>


<!-- Start of Zendesk Widget script -->
<script async>
  if(window.screen.width > 767){
    /*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(c){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("//assets.zendesk.com/embeddable_framework/main.js","careguide.zendesk.com");/*]]>*/
  }
</script>
<!-- End of Zendesk Widget script -->

<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript' defer>
  if (window.screen.width > 767 || document.getElementsByClassName('inbox').length == 0) {
    /*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
    f[z]=function(){
    (a.s=a.s||[]).push(arguments)};var a=f[z]._={
    },q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
    f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
    0:+new Date};a.P=function(u){
    a.p[u]=new Date-a.p[0]};function s(){
    a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
    hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
    return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
    b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
    b.contentWindow[g].open()}catch(w){
    c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
    var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
    b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
    loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('5681-604-10-7855');/*]]>*/
  }
</script><noscript><a href="https://www.olark.com/site/5681-604-10-7855/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->
<script> olark.configure('system.group', 'af660a5b9e0afa1654cf98c67320115a'); /*Routes to Customer Service*/ </script>

</html>
