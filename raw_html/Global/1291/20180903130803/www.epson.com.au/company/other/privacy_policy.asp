	
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
		<title>Epson Australia - Privacy Policy</title>
        <!--META DATA-->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="robots" content="all" />
        <meta name="description" content="Epson Australia Pty Limited understands that your privacy is important and recognises the importance of respecting customer privacy." />
        <meta http-equiv="Content-Script-Type" content="text/javascript" />
        <meta http-equiv="Content-Style-Type" content="text/css" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name = "viewport" content = "width = device-width, initial-scale = 1, user-scalable=no" />

        <link rel="shortcut icon" href="/favicon.ico" />

        <!--STYLESHEETS -->
        <link href ="/common/bootstrap/css/bootstrap.min.css" rel = "stylesheet" />
        <link href="/common/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/common/css/jquery-1.12.1.ui.min.css" />
        <link rel="stylesheet" href="/common/css/main_boot.css" type="text/css" />
        <link rel="stylesheet" href="/common/css/header_boot.css" type="text/css" />
        <link rel="stylesheet" href="/common/css/footer_boot.css" type="text/css" />
        <link rel="stylesheet" href="/common/css/print_boot.css" type="text/css" media="print" />
        <link rel="stylesheet" href="/common/css/epson_boot.css" type="text/css" />
        <!--[if lt IE 9]>
	    <link rel="stylesheet" href="/common/css/lt-ie9-hacks_boot.css" type="text/css" />
        <![endif]-->

        <!-- SCRIPTS -->
        <!--[if lt IE 9]>
        <script src="/common/js/jquery-1.10.2.min.js"></script>
        <script src="/common/js/jquery-1.11.1.ui.min.js"></script>
        <script src = "/common/js/html5shiv.min.js"></script>
        <script src = "/common/js/respond.min.js"></script>
        <![endif]-->

        <!--[if !IE]><!-->
        <script src="/common/js/jquery-3.1.1.min.js"></script>
        <script src="/common/js/jquery-1.12.1.ui.min.js"></script>
        <!--<![endif]-->

        <!--[if gte IE 9]>
        <script src="/common/js/jquery-3.1.1.min.js"></script>
        <script src="/common/js/jquery-1.12.1.ui.min.js"></script>
        <![endif]-->

        <script type="text/javascript" src="/common/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/common/js/jquery.swfobject.1-1-1_boot.min.js"></script>
        <script type="text/javascript" src="/common/js/common.v3.js"></script>
        <script type="text/javascript" src="/common/js/main_boot.js"></script>
        <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
<link rel="icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />

        <!--[if lte IE 6]>
        <script type="text/javascript" src="/common/js/jquery.dropdown.js"></script>
        <script type="text/javascript" src="/common/js/DD_belatedPNG.js"></script>
        <![endif]--> 
    </head>
    <body class="grey double">
        
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MT9RNK"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MT9RNK');</script>

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-W5VC7V"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>    (function(w, d, s, l, i) {
        w[l] = w[l] || []; w[l].push({ 'gtm.start':
new Date().getTime(), event: 'gtm.js'
        }); var f = d.getElementsByTagName(s)[0],
j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
'//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-W5VC7V');</script>

<!-- End Google Tag Manager -->

        <div id="container" class="mobile-container container-fluid">
            <div class="header-links">
    <div id="userLinks">
<form action="" method="get">
<ul>
	<li><a href="/" title="Home">Home</a></li>
	<li><a title="Global" href="http://www.epson.com/">Global</a></li>
	<li id="selCountryDropdown">
	<label for="selCountryLanguage">Country:</label>
	<select name="country" id="selCountryLanguage" class="jsDropdown">
		<option value="www.epson.com.au" selected="selected">Australia</option>
		<option value="www.epson.co.nz">New Zealand</option>
	</select>
	</li>
    
        <li class="signup">
            <a href="https://www.epson.com.au/shoponline/register/login.asp">Sign up</a>
        </li>
        <li class="login">
            <a href="https://www.epson.com.au/shoponline/register/login.asp">
                Log In
                <!--<span class="glyphicon glyphicon-user"></span>-->
            </a>
        </li>
    
    </ul>
<input type="hidden" name="as_fid" value="7b2219afeccc50907356d8f05735ee8858f117a2" /></form>
<!-- /userLinks --></div>
</div><!-- .main-header ends -->
<div class="main-header">
    <div id="header" class="container">
        
<div class="roundCorner" id="MastHead">
    <div class="roundInner desktop">
	    <h1 class="logo">
    <a href="/">
        <img src="/common/img/logo-b.png" class="screen-logo" alt="Epson" style="width: 100%; max-width: 116px;" />
        <img src="/shoponline/common/img/logo.gif" class="print-logo" alt="Epson" />
    </a>
</h1>
	    <div class="content">
		    <div id="primaryNav">
			    
<div class="search">
    <form id="search" action="/search.asp" method="POST">
	    <fieldset>
            <legend></legend>
		    <input id="siteSearch" type="text" name="SearchString" />
            <span class="searchButton"><input type="submit" value="Search"/></span>
            <input type="hidden" name="FormEntryID" value="685812381">
		</fieldset>
	<input type="hidden" name="as_fid" value="377a6f67a26b62440882f77e12e03eaf9fbc8fa7" /></form>
</div><!-- /search -->
		    <ul class="nav">
                <li><a href="https://www.clubepson.com.au/register_product/">Product Registration</a></li>
			    <li><a href="https://www.epson.com.au/shoponline/">Shop Online</a></li>
			    <li><a href="http://tech.epson.com.au/">Support & Downloads</a></li>
		    </ul>

		    </div><!-- /primaryNav -->
		    
		<div id="secondaryNav">
		    <ul>
			    <li class="dropdown">
                    <a href="/">All Products</a>
					<div class="dropmenu">
						<ul>
							<li><a href="/products/printers_all_in_ones/printers_for_business_corporate/">Printers for Business</a>
							</li>
							<li class="has-sub"><a href="/products/Printers_All_in_Ones/">Printers for Home</a>
								<ol class="submenu-2">
									<li><a href="/products/printers_all_in_ones/Printers_For_Home/index.asp?grouptypeid=33">For Home</a></li>
									<li><a href="/products/printers_all_in_ones/home_office_small_office/index.asp">For Home Office &amp; Small Office</a></li>
									<li><a href="/products/printers_all_in_ones/printers_for_business_corporate/">For Business &amp; Corporate</a></li>
									<li><a href="/products/printers_all_in_ones/printers_for_creative_pros/">For Photography</a></li>
								</ol>
							</li>
							<li class="has-sub"><a href="/inks-paper/">Inks &amp; Papers</a>
								<ol class="submenu-2">
									<li><a href="/consumables/ink-categories/">Inks</a></li>
									<li><a href="/consumables/paperandmedia/">Papers & Media</a></li>
									<li><a href="/shoponline/shop/Search_op_printer.asp?Cat=op_printer">Options & Accessories</a></li>
								</ol>
							</li>
							<li class="has-sub"><a href="/products/projectors/" title="Projectors">Projectors</a>
								<ol class="submenu-2">
									<li><a href="/products/business-projectors/?grouptypeID=29">Business</a></li>
									<li><a href="/products/education-projectors/?grouptypeID=30">Education</a></li>
									<li><a href="/products/large-venue-projectors/?grouptypeID=31">Large Venue</a></li>
									<li><a href="/products/home-theatre-projectors/?grouptypeID=32">Home Theatre</a></li>
								</ol>
							</li>
							<li class="has-sub"><a href="/prographics/">Professional Imaging</a>
								<ol class="submenu-2">
									<li><a href="/Prographics/products/Photography_Art/index.asp">Photography &amp; Fine Art</a></li>
									<li><a href="/Prographics/products/Technical_CAD/index.asp">Technical &amp; CAD</a></li>
									<li><a href="/Prographics/products/signage_Decor/index.asp">Signage &amp; Decor</a></li>
									<li><a href="/Prographics/consumables/">Consumables & Options</a></li>
			
									<li><a href="/Prographics/products/Proofing_Packaging/index.asp">Proofing &amp; Packaging</a></li>
									<li><a href="/Prographics/products/Posters_Graphics/index.asp">Posters &amp; Graphics</a></li>
									<li><a href="/Prographics/products/Fabric_Merchandise/index.asp">Merchandise &amp; Fabric</a></li>
									<li><a href="/shoponline/coverplus/">Services</a></li>
								</ol>
							</li>
							<li class="has-sub"><a href="/products/label_solutions/">Label Printers</a>
								<ol class="submenu-2">
									<li><a href="/products/IndustrialProducts/">Industrial Label Press</a></li>
									<li><a href="/products/Commercial-Label-Printers/?grouptypeid=39&GroupID=162">Commercial Label Printers</a></li>
									<li><a href="/products/Home-Business-Label-Printers/?grouptypeid=39&GroupID=49">Home &amp; Business Label Printers</a></li>
								</ol>
							</li>
							<li class="has-sub"><a href="/pos/">Receipt Printers</a>
								<ol class="submenu-2">
									<li><a href="/pos/products/POS-Printers/?grouptypeid=41">POS Printers</a></li>
									<li><a href="/pos/products/Banking-Printers/?grouptypeid=42">Banking Printers</a></li>
									<li><a href="/pos/products/Mobile-Printers/?grouptypeid=43">Mobile Printers</a></li>
									<li><a href="/pos/products/Kiosk-Printers/?grouptypeid=44">Kiosk Printers</a></li>
								</ol>
							</li>
							<li class="has-sub"><a href="/products/Scanners/">Scanners</a>
								<ol class="submenu-2">
									<li><a href="/products/Scanners/Scanners_for_Home_and_Pro/?grouptypeid=4">Home & Pro Photo Scanners</a></li>
									<li><a href="/products/Scanners/Scanners_for_Business/?grouptypeid=5">Business Document Scanners</a></li>
									<li><a href="/products/Scanners/Scanners_for_Large_Format/?grouptypeid=6">Large Format Scanners</a></li>
								</ol>
							</li>
							<li class="has-sub no-bb"><a href="#">Other Products</a>
								<ol class="submenu-2">
									<li><a href="/products/discpublishing/">Disc Publishing</a></li>
									<li><a href="/moverio-augmented-reality/default.asp">Smart Glasses</a></li>
								</ol>
							</li>
						</ul>
							
					<!-- / DropDown Menu -->
					</div>
				</li>
				<li><a href="https://www.clubepson.com.au/competitions/data/index.asp">Promotions</a></li>
		        <li><a href="https://www.epson.com.au/shoponline/clearance_centre.asp">Clearance Centre</a></li>
		        <li><a href="/insights/">Insights</a></li>
		    </ul>

           
    <!--<span id="Span1">
        <a href="https://www.epson.com.au/shoponline/shop/mytrolley.asp" class="mastheadCheckout">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <span>0</span>
        </a>
    </span>-->
    <div class="trolley-button">
        <a href="https://www.epson.com.au/shoponline/shop/mytrolley.asp" class="mastheadCheckout">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <span>0</span>
        </a>
    </div>
 
</div><!-- /secondaryNav -->
        
	    </div><!-- /content -->
    </div><!-- /roundInner -->
    <!--mobile and tablet header menu-->
    <div class="roundInner mobile-tablet">
        <div class="header-menu">
            <span id="mobileMenu">
            </span>
        </div><!-- /nav-menu -->
        <div class="header-logo">
            <h1 class="logo">
                <a href="/">
                    <img src="/img/logo-w.png" class="screen-logo" alt="Epson" />
                </a>
            </h1>
        </div>
        <div class="other-items">
            <div class="search-button">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <!--<div class="trolley-button">
                <a href="https://www.epson.com.au/shoponline//shop/mytrolley.asp" class="mastheadCheckout">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    <span></span>
                </a>
            </div>-->
            
           
    <!--<span id="Span1">
        <a href="https://www.epson.com.au/shoponline/shop/mytrolley.asp" class="mastheadCheckout">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <span>0</span>
        </a>
    </span>-->
    <div class="trolley-button">
        <a href="https://www.epson.com.au/shoponline/shop/mytrolley.asp" class="mastheadCheckout">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <span>0</span>
        </a>
    </div>
 
        </div>
    </div><!-- /roundInner -->
</div><!-- /roundCorner -->

    </div><!-- #header ends -->
    
<div class="mob-search">
    <form id="mob-search" action="/shoponline/shop/search.asp" method="POST">
	    <fieldset>
            <legend></legend>
		    <input id="mobSiteSearch" class="placeholder" type="text" name="SearchString" placeholder="Search Here..." autofocus="autofocus" />
            <span class="searchButton"><input type="submit" value="Search"/></span>
            <input type="hidden" name="FormEntryID" value="685812381">
		</fieldset>
	<input type="hidden" name="as_fid" value="d6a04a4509176cff0ac4cfafc538871f3c5f166a" /></form>
</div><!-- /search -->

</div><!-- .main-header ends -->
            <div class="breadCrumbs container"> 
                <ol>
                    <li>
                        <a href="" class="hidden-xs">Home</a>
                        <a href="" class="glyphicon glyphicon-home visible-xs"></a>
                        <span>&rsaquo;</span>
                    </li>
                    <li>
                        <a href="/company/">About Us</a>
                        <span>&rsaquo;</span>
                    </li>
                    <li>
                        <strong class="">Privacy Policy</strong>
                        <!--<strong class="visible-xs">HERE</strong>-->
                    </li>
                </ol>
            </div><!-- /breadCrumbs -->
            <div class="container">
                <div id="content">
                    <div id="left" class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        
    <div id="leftMenu" class="panel panel-default">
    <h3 class="left-nav-title collapsed" data-toggle="collapse" data-target="#left-nav-list" >About Us <span class="glyphicon glyphicon-triangle-bottom"></span><span class="glyphicon glyphicon-triangle-top"></span></h3>
    <ul class="heroNavigation sub-nav collapse" id="left-nav-list">
        <li>      
            <ul>
 
                
	                <li><a href="/company/" class="top">Company Information</a></li>
                
	                <li><a href="/company/businessstrategy/">Business Strategy &amp; Behaviour</a></li>
                
	                <li><a href="/company/demandquality/">Demand for Quality</a></li>
                
	                <li><a href="/company/technologies/">Core Technologies</a></li>
                
	                <li><a href="/company/environment/index.asp">Environment</a></li>
                
	                <li><a href="/company/careers/">Careers at Epson</a></li>
                	
                
                <li><a href="http://global.epson.com/IR/" target="_blank">Investor Relations</a></li>
                
                
	                <li><a href="/warranty/">Warranty</a></li>
                
	                <li><a href="https://www.clubepson.com.au/register_product/index.asp">Product Registration</a></li>
                
	                <li><a href="http://www.wheretobuy.epson.com.au/">Where to Buy</a></li>
                
                    <li><strong>Privacy Policy</strong></li>
                
	                <li><a href="/company/other/terms_of_use.asp">Terms of Use</a></li>
                
	                <li><a href="/company/other/Terms_Sales.asp">Terms of Sale</a></li>
                
	                <li><a href="/company/other/security.asp">Security</a></li>
                
	                <li><a href="/company/contactus/">Contact Us</a></li>
                		
                
            </ul>
        </li>         
    </ul>
    <!-- /leftMenu --></div>	

                    </div><!-- /left -->
                    <div id="main" class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                        <div class="roundCorner">
                            <div class="roundInner">
                                <div class="col-3-right">
		                            <h1 class="product_category">Epson Australia's Privacy Policy</h1>

<h2><br />Introduction</h2>
			
<p>Epson Australia Pty Limited (&quot;Epson&quot;) understands that your privacy is important and recognizes the importance of respecting customer privacy.</p><br />
			
<p>Within our main site <a href="/">www.epson.com.au</a> and other websites that we operate (&quot;Sites&quot;) Epson may collect information that can personally identify you (&quot;Personal Information&quot;).</p><br />
			
<p>In this Privacy Statement, and as required by the Australian Privacy Principles (&quot;APPs&quot;), we will explain the following:</p><br />

<p>(a) the kind of personal information that we collect and hold, and how we will do this;<br /><br />(b) the purposes for which we may collect, hold, use and disclose personal information;<br /><br />(c) how you may access your personal information and seek correction if necessary;<br /><br />(d) how you may complain about a breach of the APPs and how we will deal with such a complaint;<br /><br />(e) if we are likely to disclose personal information to overseas recipients and if so, the countries in which they are likely to be located. <br /></p>

<h2><br />Personal Information</h2>
			
<p>In general, you can visit any of our Sites without providing Personal Information. However, Epson may ask for and collect Personal Information from you, if you 
  choose to do any of the following things:</p><br />

<ul>
    <li>Participate in one of our competitions or promotions</li>
    <li>Register with one of our Sites</li>
    <li>Register your Epson product online</li>
    <li>Request Epson product information</li>
    <li>Request Epson technical support</li>
    <li>Make an online purchase of a product via one of our Sites</li>
    <li>Provide an online review of a product via one of our Sites</li>
</ul>
			
<h2><br />What Personal Information Will We Collect?</h2>

<p>In these instances, Epson may obtain the following Personal Information:</p><br />
			
<ul>
    <li>Your Name</li>
    <li>Address</li>
    <li>E-mail address</li>
    <li>Phone numbers</li>
	<li>Fax number</li>
	<li>Credit card details are not processed through or retained on Epson's servers. Credit card information is processed on the Commonwealth Bank's credit card processing system, or via PayPal's systems. Epson does not have access to your credit card details. We can only see the first 6 numbers and the last 3 digits of your card number, for transaction matching or verification processes.</li>
</ul><br />
			
<p>As soon as practical after we collect any Personal Information from you, we will inform you of the following:</p><br />

<ul>
  <li>our identity and contact details;</li>
  <li>the purposes for which we have collected that information;</li>
  <li>that under our privacy policy you can access and seek to correct that information if necessary; </li>
  <li>how under our privacy policy you can make a complaint and how we will deal with a complaint;</li>
  <li>if we are likely to disclose Personal Information to overseas recipients and if so, the countries in which they are likely to be located.</li>
</ul><br />

<p>Unless it is impractical for us to respond in relation to a particular request, you may choose not to identify yourself or to deal with us anonymously.</p>
	
<h2><br />Additional Information</h2>
    
<p>Epson may request (or collect where relevant) additional information for our marketing and research purposes, which may include (without limitation)</p><br />
			
<ul>	
    <li>how you found out about one of our Sites</li>
	<li>navigational data to monitor Site use and improve Site content</li>
    <li>your product functionality preferences</li>
	<li>details of Epson products that you own or use (&#8220;Products&#8221;)</li>
</ul><br />
			
<p>You will have no obligation to provide us with such additional information.</p><br />

<p>We will not collect &#8220;sensitive information&#8221;, as defined by the APPs, unless you have given consent and such collection is reasonably necessary for our business activities.</p><br />

<p> If we receive any Personal Information from you that we did not request, then as soon as practical after we become aware, and if we may lawfully do so, we will destroy or de-identify that information, and will not keep any record of it.</p><br />

<p>You are free to ask us at anytime to delete your Personal Information from our database, as explained in the &quot;Opting-out&quot; section below.</p>

<h2><br />Site Membership</h2>
			
<p> When you visit the &quot;Members Section&quot; of a Site after registering your Product, your membership number or e-mail address and password will identify you. You may also ask for your Personal Information to be removed from our membership database, as per the &quot;Opting-out&quot; section below.</p>

<h2><br />Possible Use and Disclosure of Personal Information</h2>
			
<p>If you choose to give us Personal Information, Epson may use it to support our customer relationship with you. For example, we may post or email to you from time to time information about Product upgrades, special offers, competitions, promotions and other new Products and services that we offer.</p><br />

<p>We will not use or disclose Personal Information for direct marketing purposes, unless:</p><br />

<ul>
    <li> you would reasonably expect us to use such information for that purpose:</li>
    <li> we have provided a simple method by which you can request not to receive direct marketing communications from us; and</li>
    <li>you have not made such a request to us.</li>
</ul><br />
 
<p>Epson will not share with, rent, sell or otherwise disclose your Personal Information to any other company or organization without first seeking and obtaining your 
  consent, unless we are legally required to do so, for example to a government agency.</p><br />

<p>However, Epson may disclose general information (not being Personal Information) about its customers and Site members to third parties such as advertising agencies 
  and business partners. For example, we might disclose that a certain percentage of Site users is in a particular age bracket or uses a PC or Macintosh computer.</p><br />

<p>Epson may also disclose Personal Information to independent research agencies that are contracted to act on behalf of Epson, for the sole purpose of allowing them to conduct Product or customer research. Epson has strict privacy conditions in place with such agencies to ensure the protection of Personal Information.</p><br />

<p>We may also give your address details to third parties (such as couriers, Australia Post or promotions fulfilment houses, to enable them to deliver a prize, reward 
  or online purchase to you.)</p><br />

<p>If you complete a technical or pre-sales query form, this may result in Personal Information being disclosed to overseas recipients in the United States of America, Philippines and Japan, who are either contracted to or employed by Epson. Epson has strict privacy conditions in place with such contractors and other Epson divisions to ensure the protection of such Personal Information. </p><br />

<p> If you purchase a Product on-line from one of our Sites, we do not process or retain your credit card details. All credit card transactions are processed either via the Commonwealth Bank's credit card website or the PayPal website.</p>

<h2><br />Bank Account Details</h2>
			
<p>Epson may fulfil cashback promotions via electronic funds transfer (&#8220;EFT&#8221;), whereby we will ask customers to enter their bank account numbers into our systems using the SSL secure encrypted technologies. Bank account numbers are stored in encrypted format, and six months after EFT deposits are processed, all associated bank account numbers are automatically deleted from our systems.</p>
			
<h2><br />Security of Your Personal Information</h2>
			
<p>We will take reasonable steps to keep your Personal Information confidential and to protect it from misuse, unauthorised access, modification or disclosure. We have implemented and will maintain current standards of technology and operational security to protect your Personal Information from any such interference. <a href="../../shoponline/help/security.asp" title="Epson Security Policy" target="_blank">Security policy.</a></p>

<h2><br />Opting-out and Updating Personal Information</h2>

<p>If you wish to opt out of receiving direct marketing communications from us, you may do so at any time by using the &quot;Unsubscribe&quot; link on the relevant Site. If you wish to update your Personal Information (such as your postal or email address or phone number), you may do so by using the 'Change of Details' link on the relevant Site.</p><br />

<p>If you wish to remove all your Personal Information from our Sites, please phone <strong>1300 361 054</strong> or click on the link at <a href="http://tech.epson.com.au/contact/" target="_blank">http://tech.epson.com.au/contact/</a></p>

<h2><br />Third Party Links</h2>

<p>Our Sites may provide you with links to other Internet sites (&quot;Third Party Sites&quot;) that are not operated by Epson and which have their own information privacy and collection practices. If you choose to link from a Site to a Third Party Site, you thereby acknowledge that Epson has no responsibility for the privacy or protection of any Personal Information that you may submit to the operator of that site.</p>

<h2><br />Cookies</h2>

<p>Cookies are pieces of  information that a website transfers to your computer's hard disk for  record-keeping purposes. We use cookies to operate our Site membership system  and to identify Products that you purchase via the &quot;Shopping Trolley&quot;  facility on our &quot;Shop Online&quot; site. They also help us to identify  visitors that have previously registered with  the Site.  Disabling website cookies may cause some page errors and reduced  functionality. In addition, Epson contracts with service providers to place ads  on third party websites, and Third-party Cookies maybe deployed to assist with  Network Advertising, Retargeting and click through statistics. However, this  Privacy Policy does not cover any additional use of information by service  providers.</p><br />

<h2>Network Advertising and Retargeting</h2>

<p>Epson Australia / New Zealand participates in online advertising networks and exchanges that display relevant advertisements to site visitors based on their interests as reflected in their browsing of the Sites and certain third party sites participating in the same networks and exchanges, including but not limited to networks and exchanges operated by AdRoll, Facebook and Google (this list is subject to change from a time-to-time basis). These companies use cookies, web beacons, pixels and other similar technologies to passively collect information from you which may be used to tailor the advertising you see on the Site or elsewhere on the web. This means that some information about your browsing of the Sites and certain third party sites may be shared with these companies for the purpose of delivering ads to you on the Sites and certain third party sites, and Epson Australia / New Zealand may receive from some of these companies information about third party sites that you have visited. This information is used for marketing purposes and the practice is sometimes termed "retargeting" to indicate that information from one retailer is used to suggest to you relevant products offered by another retailer. This Privacy Policy does not cover any use of information that such third parties themselves may have collected from you (e.g., type of browser, operating system, domain name, day and time of visit, page(s) visited) or the methods used by the third-parties to collect that information (e.g., cookies, web beacons and clear gifs). Should you wish, you can also learn more about opting out of interest-based advertising here: <a href="http://www.networkadvertising.org/choices/" target="_blank">Consumer Opt-out | NAI: Network Advertising Initiative.</a></p>

<h2><br />Update of This Privacy Statement</h2>

<p>From time to time, Epson may revise this Privacy Statement and we suggest that you should periodically review our current Privacy Statement to inform yourself of any change. Epson will not send you any separate notification of changes to its Privacy Statement.</p>

<h2><br />How To Contact Us</h2>

<p>If you have any questions about this Privacy Policy , the practices of this Site, or your dealings with this Site, or if you wish to lodge a complaint, please phone <strong>1300 361 054</strong> or click on the link at <a href="http://tech.epson.com.au/contact/" target="_blank">http://tech.epson.com.au/contact/</a> and complete the form. We will endeavour to answer, your complaint within 7 working days.</p>
                                </div><!--/.col-3-right-->
                                <br class="clear" /><br />
                            </div><!--/.roundInner-->
                        </div><!-- /.roundCorner -->
                    </div><!-- /#main -->
                </div><!-- /#content -->
            </div><!--/.container-->
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div class="breadCrumbs container"> 
                <ol>
                    <li>
                        <a href="" class="hidden-xs">Home</a>
                        <a href="" class="glyphicon glyphicon-home visible-xs"></a>
                        <span>&rsaquo;</span>
                    </li>
                    <li>
                        <a href="/company/">About Us</a>
                        <span>&rsaquo;</span>
                    </li>
                    <li>
                        <strong class="">Privacy Policy</strong>
                        <!--<strong class="visible-xs">HERE</strong>-->
                    </li>
                </ol>
            </div><!-- /breadCrumbs -->
            <div id="footer" class="container">
                <div class="container">
<div class="footer-signup">
    <!--<div class="container">-->
        <div class="sign-up-box">
            <div class="sign-up-top">
                <p>Sign up to receive Epson's Latest News, Updates & Exclusive Offers</p>
            </div>
            <div class="sign-up-bottom">
			    <div class="opt-in-thankyou">	     				
	     		    <strong>Thank you for signing up!</strong>
				    <p>Welcome to Epson and keep an eye out for our latest updates and exclusive offers.</p>	
			    </div>
			    <p class="opt-in-error">
				    <span id="error-msg">Oops, it looks like the system is down. Please try again later.</span>
			    </p>          
                <form method="post" id="ft_newsletter_form" name="newsletter_form" class="opt-in-form"  autocomplete="off">
                    <div class="sign-email">                                
                        <!-- ***** EMAIL INPUT ***** -->
                        <!--<label for="" class=""><span class="blue-star">*</span>Email address:</label><br />-->
                        <input name="emailaddress" type="text"  size="36" maxlength="80"  id="ft_emailaddress" class="required email empty" placeholder="Email (Required)" value="Email (Required)" /> 
 	                </div>                    
  	                <div class="sign-name">
                        <!-- ***** FIRST NAME INPUT ***** -->
                        <!--<label for="" class=""><span class="blue-star">*</span>First Name:</label><br />-->
                        <input name="firstname" type="text"  size="36" maxlength="80"  id="ft_fname" class="empty" placeholder="First Name (Optional)" value="First Name (Optional)" /> 
                    </div>
                    <div class="sign-submit">
                        <input class="login-btn" name="login" type="button" value="" id="ft_btn" />
                    </div>
               	    <input type="hidden" name="SOURCE" id="SOURCE" value=""/>
				    <input type="hidden" name="pn" id="pn" value=""/>
                    <input type="hidden" name="cl" id="cl" value=""/>
                    <input type="hidden" name="ts" id="ts" value=""/>
                    <input type="hidden" name="token" id="token" value="" />
                    <input type="hidden" name="tokenid" id="tokenid" value="" />
                <input type="hidden" name="as_fid" value="dd0e811d48944190616775477c507f30d42c7ed8" /></form>
                <div class="sign-social">
                    <a href="https://www.facebook.com/EpsonAustralia/" target="_blank" class="social-facebook"></a>
                    <a href="https://twitter.com/EpsonAust" target="_blank" class="social-twitter"></a>
                    <a href="http://www.instagram.com/epsonaust" target="_blank" class="social-instagram"></a>
                    <a href="https://www.youtube.com/user/EpsonAustralia" target="_blank" class="social-youtube"></a>
                    <a href="https://www.linkedin.com/company/epson-australia" target="_blank" class="social-linkedin"></a>
                </div>
                <div style="clear: both;"></div>
                <div class="sign-disclaimer"><p>You are providing your consent to Epson Australia Pty Ltd., so that we may send you emails with regards to interesting news, latest updates and exclusive offers.<br /> You may withdraw your consent or view our <a class="priv-policy-link" href="/shoponline/help/privacypolicy.asp">privacy policy</a> at any time.</p></div>
            </div>
        </div>
    <!--</div>-->
</div>
<div class="clearing"></div>
<link rel="stylesheet" type="text/css" href="/common/css/newsletter_boot.css" />
<!--<script type="text/javascript" src="/common/js/jquery.validate.pack.js"></script>-->
<script type="text/javascript" src="/common/js/jquery.validate.js"></script>
<script type="text/javascript" src="/common/js/newsletter_subscribe_boot.js?v=1.01"></script>






        <div class="roundCorner">
            <div class="roundInner">
                <ul class="footer-nav">
                    <li class="customer-service footer-nav-items">
                        <span>Customer Service</span>
                        <ul>
                            <li><a href="http://tech.epson.com.au/">Support & Downloads</a></li>
                            <li><a href="https://www.clubepson.com.au/register_product/index.asp">Product Registration</a></li>
                            <li><a href="http://www.wheretobuy.epson.com.au/">Where to Buy</a></li>
                            <li><a href="/warranty/">Warranty</a></li>
                        </ul>
                    </li>
                    <li class="about footer-nav-items">
                        <span>About</span>
                        <ul class="">
                            <li><a href="https://www.clubepson.com.au/competitions/data/">Promotions</a></li>
                            <li><a href="http://www.creativecorner.clubepson.com.au">Creative Corner</a></li>
                            <li><a href="/company/careers/">Careers</a></li>
                            <li><a href="https://www.channel.epson.com.au">Channel Epson</a></li>
                            <li><a href="/company/">About Us</a></li>
                            <li><a href="/company/contactus/">Contact Us</a></li>
                        </ul>
                    </li>
                    
<!--<ul class="functionNav">
	<li><a href="/company/other/privacy_policy.asp">Privacy Policy</a></li>
	<li><a href="/company/other/terms_of_use.asp">Terms of Use</a></li>
    <li><a href="/company/other/Terms_Sales.asp">Terms of Sale</a></li>
	<li><a href="/unsubscribe/">Unsubscribe</a></li>
</ul>-->
<li class="legal footer-nav-items">
    <span>Legal</span>
    <ul class="">
        <li><a href="/company/other/privacy_policy.asp">Privacy Policy</a></li>
	    <li><a href="/company/other/terms_of_use.asp">Terms of Use</a></li>
        <li><a href="/company/other/Terms_Sales.asp">Terms of Sale</a></li>
	    <li><a href="/unsubscribe/">Unsubscribe</a></li>
    </ul>
</li>

                </ul>
            </div><!-- /roundInner -->
            
<!--<p id="copyright">Copyright &copy; 2015 Epson Australia Pty Ltd. All rights reserved. ABN 91 002 625 783 EPN01</p>-->
<p id="copyright">Copyright &copy; 2015 Epson Australia Pty Ltd. All rights reserved. ABN 91 002 625 783 EPN01</p>

        </div><!-- /roundCorner -->
    
</div>





                <div style="clear: both; margin-bottom: 5px;"></div>
                <div style="clear:both;"></div>
            </div><!-- /footer -->
        </div><!-- /container -->
        <div class="hidden-upper-div">
    <a href="#cd-nav" class="cd-nav-trigger" id ="cd-nav-trigger">
        <button class="btn btn-nav btn-nav__menu js-toggle-sm-navigation" type="button">
			<span class="icon-hamburger"></span><br />
            <span class="label">Menu</span>
		</button>
    </a>
    <div class="mob-floating-tool">
        <div class="header-logo">
            <h1 class="logo">
                <a href="/">
                    <img src="/shoponline/common/img/logo-w.png" alt="Epson" />
                </a>
            </h1>
        </div>
        <div class="other-items">
            <div class="mobile-menu-search-button">
                <span class="glyphicon glyphicon-search"></span>
            </div>
            <!--<div class="trolley-button">
                <a href="https://www.epson.com.au/shoponline//shop/mytrolley.asp" class="mastheadCheckout">
                    <span class="glyphicon glyphicon-shopping-cart"></span>
                    <span></span>
                </a>
            </div>-->
            
           
    <!--<span id="Span1">
        <a href="https://www.epson.com.au/shoponline/shop/mytrolley.asp" class="mastheadCheckout">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <span>0</span>
        </a>
    </span>-->
    <div class="trolley-button">
        <a href="https://www.epson.com.au/shoponline/shop/mytrolley.asp" class="mastheadCheckout">
            <span class="glyphicon glyphicon-shopping-cart"></span>
            <span>0</span>
        </a>
    </div>

        </div>
    </div>
</div>
<div id="cd-nav" class="cd-nav">
    <div class="mob-menu-search">
        <form id="mob-search" action="/search.asp" method="POST">
		    <fieldset>
                <legend></legend>
		        <input id="mobMenuSearch" class="placeholder" type="text" name="keyword" placeholder="Search Here..." autofocus="autofocus" />
                <span class="searchButton"><input type="submit" value="Search"/></span>
		    </fieldset>
	    <input type="hidden" name="as_fid" value="e707a6cd91a404c63c9f7a585468f84e395b034d" /></form>
    </div><!-- /search -->
	<div class="cd-navigation-wrapper">
        <ul class="mob-nav">
            <li class="dropdown has-child">
                <a href="#">My Account (Log in/Sign up)&nbsp;&nbsp;<span>&#x25BC;</span></a>
                <div class="dropmenu">
	                <!-- DropDown Menu -->
                    <ul>
                    
                        <li class="signup"><a href="https://www.epson.com.au/shoponline/register/login.asp">Sign up</a></li>
                        <li class="login"><a href="https://www.epson.com.au/shoponline/register/login.asp">Log in</a></li>
                    
                    </ul>
                </div>
            </li>
        </ul>
        <div id="mobileSecondaryNav">
    <ul class="sec-nav-mobile">

        <li><a href="https://www.clubepson.com.au/register_product/">Product Registration</a></li>
        <li><a href="https://www.epson.com.au/shoponline/">Shop Online</a></li>
        <li><a href="http://tech.epson.com.au/">Support & Downloads</a></li>
        <li><a href="/insights/">Insights</a></li>


		<li class="dropdown has-child">
            <a href="/">All Products&nbsp;&nbsp;<span>&#x25BC;</span></a>
			<div class="dropmenu">
			<!-- DropDown Menu -->
				<ul>
                    <!--<li><a href="/products/">All Products</a></li>-->
					<li><a href="https://www.epson.com.au/shoponline/shop/BrowseProductCatagory.asp?catid=6">Printers &amp; All-in-Ones</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseProductCatagory_GD.asp?catid=4">Professional Imaging</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseProductCatagory.asp?catid=2">Scanners</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseproductGroup.asp?catid=6&typeid=34&gid=49">Label Printers</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/SearchInks.asp?cat=cartridge">Ink</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/SearchConsumables.asp?Cat=paper">Paper &amp; Media</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseProductCatagory.asp?catid=5">Projectors</a></li>					
					<li><a href="/moverio-augmented-reality/">Smart Glasses</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseproductGroup.asp?catid=6&typeid=34&gid=137">Dot Matrix Printers</a></li>
					<li><a href="/pos/">Receipt Printers</a></li>
				    <li><a href="/products/discpublishing/">Disc Publishing</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/coverplus/">Epson CoverPlus</a></li>
					<li><a href="/products/backcatalogue.asp">Back Catalogue Products</a></li>
				</ul>
			    <!-- / DropDown Menu -->
			</div>
        </li>
		<li class="dropdown has-child">
            <a href="/">Solutions&nbsp;&nbsp;<span>&#x25BC;</span></a>
			<div class="dropmenu">
			<!-- DropDown Menu -->
				<ul>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=6&typeid=33">Printers for Home</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=6&typeid=46">Printers for Business</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=6&typeid=35">Printers for Creative Pros</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/SearchConsumables.asp?Cat=paper">Paper &amp; Media</a></li>
					<li><a href="https://www.epson.com.au/shoponline/shop/browseProductCatagory.asp?catid=5">Projectors</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseproductGroup.asp?catid=5&typeid=29&gid=70">Interactive Business Projectors</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=5&typeid=32">Home Theatre Projectors</a></li>                    
					<li><a href="https://www.epson.com.au/shoponline/shop/browseProductCatagory.asp?catid=2">Scanners</a></li>
					<li><a href="/products/discpublishing/">Disc Publishing</a></li>
					<li><a href="/pos/">Receipt Printers</a></li>
                    <li><a href="/microsite/desktop_label_solutions/">On-Demand Labelling</a></li>
                    <li><a href="/Prographics/">ProGraphics</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=4&typeid=22">Photography & Fine Art</a></li>	
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=4&typeid=20">Posters & Graphics</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=4&typeid=23">Proofing & Packaging</a></li>
                    <li><a href="/Prographics/products/Computer_To_Plate/">Computer to Plate</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=4&typeid=21">Technical & CAD</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=4&typeid=18">Signage & Decor</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=4&typeid=19">Fabric & Merchandise</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=4&typeid=26">Commercial Printers</a></li>
     				<li><a href="https://www.epson.com.au/shoponline/shop/browseProducttype.asp?catid=4&typeid=25">Industrial Printers</a></li>
                    <li><a href="https://www.epson.com.au/shoponline/coverplus/">Epson CoverPlus</a></li>
				</ul>
			<!-- / DropDown Menu -->
			</div>
		</li>
		<li><a href="https://www.epson.com.au/shoponline/clearance_centre.asp">Clearance Centre</a></li>
		<li><a href="https://www.clubepson.com.au/competitions/data/index.asp">Promotions</a></li>
		<li><a href="/videos/">Product Videos</a></li>

    </ul>
</div><!-- /secondaryNav -->
	</div> <!-- .cd-navigation-wrapper -->
</div> <!-- .cd-nav -->
        <!-- Epson Global Google Analytics - DO NOT REMOVE-->
        <script type="text/javascript" src="/js/ww_aus_ga.js"></script>
        <script type="text/javascript" src="/js/ww_aus_tracking.js"></script>
        <!-- Epson Global Google Analytics - DO NOT REMOVE-->
    </body>
</html>
