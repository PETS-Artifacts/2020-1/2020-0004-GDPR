
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Language" content="en-us"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1.0, user-scalable=yes ">
<meta name="description" content="Best High Performance Gaming PC - Specialize in Gaming Computers and Custom PC. Award Winning Custom Built Desktops and Gaming Laptops for over 14 years."/>
<meta name="keywords" content="cheap gaming pc, custom built gaming pc, dream pc, gaming computer, gaming computers, custom pc, custom built pc, custom computers, custom build computers, gaming laptops, gaming notebooks, custom laptops, custom notebooks"/>
<title>Privacy </title>
 
<link rel="icon" href="/favicon.ico" type="image/x-icon"/>
<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
<link rel="manifest" href="/images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/images/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<link href='https://fonts.googleapis.com/css?family=Rajdhani:500,700,600' rel='stylesheet' type='text/css'/>
<link href="/tc/default.aspx?ls=global;common;thickbox_saving;Company_Service.css&v20141014&v=20160304051854" rel="stylesheet" type="text/css"/>
 
<!--[if lt IE 9]>          
          <script type="text/javascript" src="/template/default/scripts/html5shivRespond.js"></script>
        <![endif]-->
<script src="/tj/default.aspx?ls=jquery.min;common;saving;Company_Service.js&v1&v=20160223072340" type="text/javascript"></script>
<script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>
</head>
<body class="defaultbg">
<script type="text/javascript">window.lpTag=window.lpTag||{};if(typeof window.lpTag._tagCount==='undefined'){window.lpTag={site:'49044919'||'',section:lpTag.section||'',autoStart:lpTag.autoStart===false?false:true,ovr:lpTag.ovr||{},_v:'1.5.1',_tagCount:1,protocol:location.protocol,events:{bind:function(app,ev,fn){lpTag.defer(function(){lpTag.events.bind(app,ev,fn);},0);},trigger:function(app,ev,json){lpTag.defer(function(){lpTag.events.trigger(app,ev,json);},1);}},defer:function(fn,fnType){if(fnType==0){this._defB=this._defB||[];this._defB.push(fn);}else if(fnType==1){this._defT=this._defT||[];this._defT.push(fn);}else{this._defL=this._defL||[];this._defL.push(fn);}},load:function(src,chr,id){var t=this;setTimeout(function(){t._load(src,chr,id);},0);},_load:function(src,chr,id){var url=src;if(!src){url=this.protocol+'//'+((this.ovr&&this.ovr.domain)?this.ovr.domain:'lptag.liveperson.net')+'/tag/tag.js?site='+this.site;}var s=document.createElement('script');s.setAttribute('charset',chr?chr:'UTF-8');if(id){s.setAttribute('id',id);}s.setAttribute('src',url);document.getElementsByTagName('head').item(0).appendChild(s);},init:function(){this._timing=this._timing||{};this._timing.start=(new Date()).getTime();var that=this;if(window.attachEvent){window.attachEvent('onload',function(){that._domReady('domReady');});}else{window.addEventListener('DOMContentLoaded',function(){that._domReady('contReady');},false);window.addEventListener('load',function(){that._domReady('domReady');},false);}if(typeof(window._lptStop)=='undefined'){this.load();}},start:function(){this.autoStart=true;},_domReady:function(n){if(!this.isDom){this.isDom=true;this.events.trigger('LPT','DOM_READY',{t:n});}this._timing[n]=(new Date()).getTime();},vars:lpTag.vars||[],dbs:lpTag.dbs||[],ctn:lpTag.ctn||[],sdes:lpTag.sdes||[],ev:lpTag.ev||[]};lpTag.init();}else{window.lpTag._tagCount+=1;}</script>
<iframe name="ifrm_for_save_hidden" height="0" width="0" style="display:none;"></iframe>
<div id="header">
<div id="h_wrapper">
<h1 title="Welcome to CyberPower Computer" id="h_brand"><a href="/" title="Welcome to CyberPower Computer"><span>Cyberpower - Unleash The Power</span></a></h1>
<ul id="h_shortcuts">
<li id="btn_helpheader" onclick="javascript:window.open('//hc2.humanclick.com/hc/49044919/?cmd=file&amp;file=visitorWantsToChat&amp;site=49044919&amp;referrer='+window.location.href,'chat49044919','width=472,height=320');return false;">
<a href="#" title="chat"><nobr><i class="icon-chat fn_service_offline"></i><span>Leave me a message</span></nobr></a></li>
<li><a id="h-load" href="#" title="Load Configuration"><nobr><i class="icon-load"></i><span>Load Configuration</span></nobr></a></li>
<li><a href="/cart/" title="Shopping Cart"><nobr><i class="icon-cart"></i><span>Shopping Cart</span></nobr></a></li>
<li><a href="/orderstatus/" title="Order Status"><nobr><i class="icon-order"></i><span>Order Status</span></nobr></a></li>
<li><a href="/forum" title="Forum"><nobr><i class="icon-forum"></i><span>Forum</span></nobr></a></li>
<li><a href="/coupons/" title="Rebates"><nobr><i class="icon-rebates"></i><span>Rebates</span></nobr></a></li>
</ul>
<div id="h_search">
<span></span>
</div>
<div id="h_mobile_listc">
<div id="h_mobile_list">
<div class="h_mobile_list_close"><span></span></div>
<ul>
<li id="btn_helpheader" onclick="javascript:window.open('//hc2.humanclick.com/hc/49044919/?cmd=file&amp;file=visitorWantsToChat&amp;site=49044919&amp;referrer='+window.location.href,'chat49044919','width=472,height=320');return false;">
<i></i><span>Leave me a message</span></li>
<li>
<span>Load Configuration</span>
<div class="h_mobile_load_config">
<form method="post" action="/saving/doLoad.aspx">
<input id="keyid" class="input_load_config" type="text" placeholder="Input configuration number"/>
<input type="submit" class="btn btn_yellow" value="Load"/>
</form>
</div>
</li>
<li>
<span><a href="/cart/showcart.aspx/">Shopping Cart</a></span>
<span class="sc_cnt">6</span>
</li>
<li>
<span><a href="/orderstatus/">Order Status</a></span>
</li>
</ul>
</div>
<div id="h_mobile_list_icon"><i class="icon-list"></i></div>
</div>
</div>  
<div id="h_spiliter"></div>
<div id="tophide" class="tophide">
<div class="th-inner">
<div class="load-config">
<form name="f" method="post" action="/saving/doLoad.aspx">
<input type="text" id="keyid" placeholder="Please input configuration number" class="th-input-style"><input type="submit" value="Load" class="th-btn-style"><span class="th-close-btn"> </span>
</form>
</div>
<div class="searching">
<form name="f" method="post" action="/search/advancedsearch.aspx">
<input type="text" name="kw" placeholder="Input Keywords: eg. Intel Core i7, AMD, Nvidia" class="th-input-style"><input type="submit" value="Search" class="th-btn-style"><span class="th-close-btn"> </span>
</form>
</div>
</div>
</div>
<div id="h_menu_wrapper" class="global-nav">
<ul id="h_sup-nav" class='boldfont'>
<li><a idx='0' href='/LandingPages/ExclusiveSeries/' title='click to view Exclusive Series page'><i class='icon-plus'></i>Exclusive Series</a></li>
<li><a idx='1' href='/LandingPages/VenomX/' title='click to view VenomX page'><i class='icon-plus'></i>VenomX</a></li>
<li><a idx='2' href='/category/gaming-pcs/' title='click to view Gaming PCs page'><i class='icon-plus'></i>Gaming PCs</a></li>
<li><a idx='3' href='/category/notebook/' title='click to view Gaming Laptops page'><i class='icon-plus'></i>Gaming Laptops</a></li>
<li><a idx='4' href='/landingpages/syber/' title='click to view Syber Gaming page'><i class='icon-plus'></i>Syber Gaming</a></li>
<li><a idx='5' href='/LandingPages/MegaSpecial/' title='click to view Weekly Special page'><i class='icon-plus'></i>Weekly Special</a></li>
<li><a idx='6' href='/category/same_day_shipping/' title='click to view Same Day Shipping page'><i class='icon-plus'></i>Same Day Shipping</a></li>
<li><a idx='7' href='/company/about_us.aspx' title='click to view Company page'><i class='icon-plus'></i>Company</a></li>
<li><a idx='8' href='/support/' title='click to view Support page'><i class='icon-plus'></i>Support</a></li>
<li><a idx='9' href='/landingpages/microsoft/Windows_10/' title='click to view CyberPower recommends Windows. page'><i class='icon-plus'></i>CyberPower recommends Windows.</a></li>
</ul>
<div class='nav-pop' idx='0' onselectstart='return false' style='-moz-user-select:none'>
<div class='np-inner'>
<div class='np-tab'>
<ul>
<li>
<a title='IEM' idx='0' href='/LandingPages/IEM-Gaming-PCs/' class='selecthis'>IEM</a>
</li>
<li>
<a title='Luxe' idx='1' href='/LandingPages/Luxe/'>Luxe</a>
</li>
<li>
<a title='Trinity' idx='2' href='/LandingPages/Trinity/'>Trinity</a>
</li>
<li>
<a title='Fang III' idx='3' href='/LandingPages/FangIII/'>Fang III</a>
</li>
<li>
<a title='FANG BattleBox' idx='4' href='/LandingPages/FangBattleBox/'>FANG BattleBox</a>
</li>
<li>
<a title='Vector Series' idx='5' href='/landingpages/Vector/'>Vector Series</a>
</li>
<li>
<a title='Fangbook III Series' idx='6' href='/landingpages/FangbookIII/'>Fangbook III Series</a>
</li>
<li>
<a title='Fangbook 4 Series' idx='7' href='/landingpages/Fangbook4/'>Fangbook 4 Series</a>
</li>
<li>
<a title='Zeus Evo Series' idx='8' href='/LandingPages/ZeusEvo/'>Zeus Evo Series</a>
</li>
<li>
<a title='ZEUS MINI EVO' idx='9' href='/LandingPages/ZeusMiniEVO/'>ZEUS MINI EVO</a>
</li>
<li>
<a title='Pro Gamer FTW' idx='10' href='/LandingPages/ProGamer/'>Pro Gamer FTW</a>
</li>
<li>
<a title='Pro Streamer' idx='11' href='/LandingPages/ProStreamer/'>Pro Streamer</a>
</li>
<li>
<a title='Power Mega Pro' idx='12' href='/LandingPages/PowerMegaPro/'>Power Mega Pro</a>
</li>
<li>
<a title='Zeus SFF' idx='13' href='/LandingPages/ZeusSFF/'>Zeus SFF</a>
</li>
<li>
<a title='Black Pearl Gaming PC' idx='14' href='/LandingPages/BlackPearl/'>Black Pearl Gaming PC</a>
</li>
</ul>
</div>
<div class='np-box-container'>
<div class='np-box'>
<div idx='0' class='np-sys' style='display:block'>
<dl><dd>
<a href='/system/IEM_Challenger' title='customize IEM Challenger'>
<h1 class='npp-name'>IEM Challenger</h1>
<div class='npp-img'>
<img alt='IEM Challenger' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; GIGABYTE Z170XP-SLI MB</li>
<li>&bull; Intel 120GB SSD</li>
</ul>
<p class='npp-price'><strong>$1099</strong></p>
</a>
</dd>
<dd>
<a href='/system/IEM_Champion' title='customize IEM Champion'>
<h1 class='npp-name'>IEM Champion</h1>
<div class='npp-img'>
<img alt='IEM Champion' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; GIGABYTE Z170XP-SLI MB</li>
<li>&bull; Intel 120GB SSD</li>
</ul>
<p class='npp-price'><strong>$1449</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='1' class='np-sys'>
<dl><dd>
<a href='/system/LUXE_100' title='customize LUXE 100'>
<h1 class='npp-name'>LUXE 100</h1>
<div class='npp-img'>
<img alt='LUXE 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/luxe909/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; EVGA GeForce GTX 980 Ti Hydro Copper 6GB</li>
<li>&bull; GIGABYTE GA-Z170X UD5 TH Mainboard</li>
<li>&bull; 120GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$3079</strong></p>
</a>
</dd>
<dd>
<a href='/system/LUXE_200' title='customize LUXE 200'>
<h1 class='npp-name'>LUXE 200</h1>
<div class='npp-img'>
<img alt='LUXE 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/luxe909/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; EVGA GeForce GTX TITAN X Hydro 12GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$3875</strong></p>
</a>
</dd>
<dd>
<a href='/system/LUXE_4K' title='customize LUXE 4K'>
<h1 class='npp-name'>LUXE 4K</h1>
<div class='npp-img'>
<img alt='LUXE 4K' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/luxe/gld_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5960X</li>
<li>&bull; 32GB DDR4/2800MHz RAM</li>
<li>&bull; 2x GeForce GTX TITAN X</li>
<li>&bull; ASUS ROG X99 RAMPAGE V EXTREME/U3.1 MB</li>
<li>&bull; Intel 750 Series 400GB SSD</li>
</ul>
<p class='npp-price'><strong>$6835</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='2' class='np-sys'>
<dl><dd>
<a href='/system/Trinity_100' title='customize Trinity 100'>
<h1 class='npp-name'>Trinity 100</h1>
<div class='npp-img'>
<img alt='Trinity 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/tristellar/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD Athlon X4 840 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 120GB SSD + 1TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$899</strong></p>
</a>
</dd>
<dd>
<a href='/system/Trinity_200' title='customize Trinity 200'>
<h1 class='npp-name'>Trinity 200</h1>
<div class='npp-img'>
<img alt='Trinity 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/tristellar/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170I Gaming Pro AC mini ITX Motherboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1455</strong></p>
</a>
</dd>
<dd>
<a href='/system/Trinity_XTREME' title='customize Trinity XTREME'>
<h1 class='npp-name'>Trinity XTREME</h1>
<div class='npp-img'>
<img alt='Trinity XTREME' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/tristellar/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; ASRock X99E-ITX/AC Motherboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$2105</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='3' class='np-sys'>
<dl><dd>
<a href='/system/Fang_III_-_Black_Mamba' title='customize Fang III - Black Mamba'>
<h1 class='npp-name'>Fang III - Black Mamba</h1>
<div class='npp-img'>
<img alt='Fang III - Black Mamba' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian900d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5960X</li>
<li>&bull; Ultimate Overclock 30% or more</li>
<li>&bull; 32GB DDR4/2800MHz RAM</li>
<li>&bull; 2x GeForce GTX TITAN X</li>
<li>&bull; ASUS ROG X99 RAMPAGE V EXTREME/U3.1 MB</li>
<li>&bull; Intel 750 Series 400GB SSD</li>
<li>&bull; 4TB SATA3 5900 RPM HD</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$6229</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fang_III_-_Cobra' title='customize Fang III - Cobra'>
<h1 class='npp-name'>Fang III - Cobra</h1>
<div class='npp-img'>
<img alt='Fang III - Cobra' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/cmpro5/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; Extreme Overclock 20% or more</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; ASRock X99 Killer ATX Motherboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$2329</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fang_III_-_Viper' title='customize Fang III - Viper'>
<h1 class='npp-name'>Fang III - Viper</h1>
<div class='npp-img'>
<img alt='Fang III - Viper' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian450d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; ASUS Z170- PRO GAMING Mainboard</li>
<li>&bull; 120GB SSD + 2TB HDD Combo</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$1745</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fang_III_-_Rattler' title='customize Fang III - Rattler'>
<h1 class='npp-name'>Fang III - Rattler</h1>
<div class='npp-img'>
<img alt='Fang III - Rattler' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/n600/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; Pro Overclock 10% or more</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; ASUS Z170- PRO GAMING Mainboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1399</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='4' class='np-sys'>
<dl><dd>
<a href='/system/Fang_BattleBox_A-100' title='customize Fang BattleBox A-100'>
<h1 class='npp-name'>Fang BattleBox A-100</h1>
<div class='npp-img'>
<img alt='Fang BattleBox A-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/battlebox/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD A10-7700K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 370 2GB Video</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$705</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fang_BattleBox_I-100' title='customize Fang BattleBox I-100'>
<h1 class='npp-name'>Fang BattleBox I-100</h1>
<div class='npp-img'>
<img alt='Fang BattleBox I-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/battlebox/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; Integrated onboard Graphic</li>
<li>&bull; ASRock H110M-ITX/AC Wi-Fi Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$685</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fang_BattleBox_I-200' title='customize Fang BattleBox I-200'>
<h1 class='npp-name'>Fang BattleBox I-200</h1>
<div class='npp-img'>
<img alt='Fang BattleBox I-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/battlebox/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6400</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI Z170I Gaming Pro AC mini ITX Motherboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$939</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fang_BattleBox_I-970' title='customize Fang BattleBox I-970'>
<h1 class='npp-name'>Fang BattleBox I-970</h1>
<div class='npp-img'>
<img alt='Fang BattleBox I-970' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/battlebox/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170I Gaming Pro AC mini ITX Motherboard</li>
<li>&bull; 120GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1259</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fang_BattleBox_I-980' title='customize Fang BattleBox I-980'>
<h1 class='npp-name'>Fang BattleBox I-980</h1>
<div class='npp-img'>
<img alt='Fang BattleBox I-980' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/battlebox/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; ASRock X99E-ITX/AC Motherboard</li>
<li>&bull; 120GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1905</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fang_BattleBox_R9X' title='customize Fang BattleBox R9X'>
<h1 class='npp-name'>Fang BattleBox R9X</h1>
<div class='npp-img'>
<img alt='Fang BattleBox R9X' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/battlebox/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; MSI Z170I Gaming Pro AC mini ITX Motherboard</li>
<li>&bull; 120GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1059</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='5' class='np-sys'>
<dl><dd>
<a href='/system/Vector-15_100_Gaming_Laptop' title='customize Vector-15 100'>
<h1 class='npp-name'>Vector-15 100</h1>
<div class='npp-img'>
<img alt='Vector-15 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/vector15/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$1019</strong></p>
</a>
</dd>
<dd>
<a href='/system/Vector-15_200_Gaming_Laptop' title='customize Vector-15 200'>
<h1 class='npp-name'>Vector-15 200</h1>
<div class='npp-img'>
<img alt='Vector-15 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/vector15/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$1065</strong></p>
</a>
</dd>
<dd>
<a href='/system/Vector-15_300_Gaming_Laptop' title='customize Vector-15 300'>
<h1 class='npp-name'>Vector-15 300</h1>
<div class='npp-img'>
<img alt='Vector-15 300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/vector15/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$1199</strong></p>
</a>
</dd>
<dd>
<a href='/system/Vector-15_400_Gaming_Laptop' title='customize Vector-15 400'>
<h1 class='npp-name'>Vector-15 400</h1>
<div class='npp-img'>
<img alt='Vector-15 400' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/vector15/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; SanDisk X400 512GB SSD</li>
</ul>
<p class='npp-price'><strong>$1255</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='6' class='np-sys'>
<dl><dd>
<a href='/system/Fangbook_III_HX7-100_Gaming_Laptop' title='customize Fangbook III HX7-100'>
<h1 class='npp-name'>Fangbook III HX7-100</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 880M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1565</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_III_HX7-200_Gaming_Laptop' title='customize Fangbook III HX7-200'>
<h1 class='npp-name'>Fangbook III HX7-200</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 880M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1735</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_III_HX7-300_Gaming_Laptop' title='customize Fangbook III HX7-300'>
<h1 class='npp-name'>Fangbook III HX7-300</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 6X Blu-Ray Internal Player Drive</li>
</ul>
<p class='npp-price'><strong>$2169</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-100_Gaming_Laptop' title='customize FANGBOOK III HX6-100'>
<h1 class='npp-name'>FANGBOOK III HX6-100</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1189</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-160_Gaming_Laptop' title='customize FANGBOOK III HX6-160'>
<h1 class='npp-name'>FANGBOOK III HX6-160</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-160' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1239</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-200_Gaming_Laptop' title='customize FANGBOOK III HX6-200'>
<h1 class='npp-name'>FANGBOOK III HX6-200</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1259</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-300_Gaming_Laptop' title='customize FANGBOOK III HX6-300'>
<h1 class='npp-name'>FANGBOOK III HX6-300</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1319</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='7' class='np-sys'>
<dl><dd>
<a href='/system/Fangbook_4_SX6-100_Gaming_Laptop' title='customize FANGBOOK 4 SX6-100'>
<h1 class='npp-name'>FANGBOOK 4 SX6-100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX6-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX6/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$995</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX6-200_Gaming_Laptop' title='customize FANGBOOK 4 SX6-200'>
<h1 class='npp-name'>FANGBOOK 4 SX6-200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX6-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX6/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1329</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX6-4K_Gaming_Laptop' title='customize FANGBOOK 4 SX6-4K'>
<h1 class='npp-name'>FANGBOOK 4 SX6-4K</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX6-4K' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX6/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840 x 2160 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1475</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX7-100_Gaming_Laptop' title='customize FANGBOOK 4 SX7-100'>
<h1 class='npp-name'>FANGBOOK 4 SX7-100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX7-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX7/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$979</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX7-300_Gaming_Laptop' title='customize FANGBOOK 4 SX7-300'>
<h1 class='npp-name'>FANGBOOK 4 SX7-300</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX7-300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX7/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1259</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_G-SYNC_100_Gaming_Laptop' title='customize FANGBOOK 4 XTREME G-SYNC 100'>
<h1 class='npp-name'>FANGBOOK 4 XTREME G-SYNC 100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME G-SYNC 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREME/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840x2160 G-SYNC UHD LCD</li>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel Z170 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1839</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_Xtreme_G-SYNC_200_Gaming_Laptop' title='customize FANGBOOK 4 XTREME G-SYNC 200'>
<h1 class='npp-name'>FANGBOOK 4 XTREME G-SYNC 200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME G-SYNC 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREME/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840x2160 G-SYNC UHD LCD</li>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel Z170 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2205</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SK-X17_XTREME_G-Sync_Gaming_Laptop' title='customize FANGBOOK 4 SK-X17 XTREME G-Sync'>
<h1 class='npp-name'>FANGBOOK 4 SK-X17 XTREME G-Sync</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SK-X17 XTREME G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SKX17/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980 8GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2385</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SK-X17_PRO_Gaming_Laptop' title='customize FANGBOOK 4 SK-X17 PRO'>
<h1 class='npp-name'>FANGBOOK 4 SK-X17 PRO</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SK-X17 PRO' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SKX17/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1499</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SK-X17_XTREME_Gaming_Laptop' title='customize FANGBOOK 4 SK-X17 XTREME'>
<h1 class='npp-name'>FANGBOOK 4 SK-X17 XTREME</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SK-X17 XTREME' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SKX17/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1749</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_100_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 100'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970 12GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2389</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_200_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 200'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 16GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2915</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_300_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 300'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 300</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6920HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 16GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$3195</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_400_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 400'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 400</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 400' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980 16GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$3529</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_500_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 500'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 500</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 500' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6920HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980 16GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$3809</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='8' class='np-sys'>
<dl><dd>
<a href='/system/Zeus_Evo_Lightning_1000' title='customize Zeus Evo Lightning 1000'>
<h1 class='npp-name'>Zeus Evo Lightning 1000</h1>
<div class='npp-img'>
<img alt='Zeus Evo Lightning 1000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-9590 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 370 2GB Video</li>
<li>&bull; GIGABYTE 990FXA-UD3 R5 AM3 MB</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1239</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_Evo_Lightning_2000' title='customize Zeus Evo Lightning 2000'>
<h1 class='npp-name'>Zeus Evo Lightning 2000</h1>
<div class='npp-img'>
<img alt='Zeus Evo Lightning 2000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-9590 CPU</li>
<li>&bull; 16GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 370 4GB Video</li>
<li>&bull; GIGABYTE 990FXA-UD3 R5 AM3 MB</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$1445</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_Evo_Lightning_3000' title='customize Zeus Evo Lightning 3000'>
<h1 class='npp-name'>Zeus Evo Lightning 3000</h1>
<div class='npp-img'>
<img alt='Zeus Evo Lightning 3000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-9590 CPU</li>
<li>&bull; 16GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; GIGABYTE 990FXA-UD3 R5 AM3 MB</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$1589</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_Evo_Lightning_4000' title='customize Zeus Evo Lightning 4000'>
<h1 class='npp-name'>Zeus Evo Lightning 4000</h1>
<div class='npp-img'>
<img alt='Zeus Evo Lightning 4000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian900d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-9590 CPU</li>
<li>&bull; 16GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 390X 8GB Video</li>
<li>&bull; GIGABYTE 990FXA-UD3 R5 AM3 MB</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 3TB SATA3 7200 RPM HD</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$2115</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_EVO_Storm_1000' title='customize Zeus EVO Storm 1000'>
<h1 class='npp-name'>Zeus EVO Storm 1000</h1>
<div class='npp-img'>
<img alt='Zeus EVO Storm 1000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1929</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_EVO_Storm_2000' title='customize Zeus EVO Storm 2000'>
<h1 class='npp-name'>Zeus EVO Storm 2000</h1>
<div class='npp-img'>
<img alt='Zeus EVO Storm 2000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5930K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; ADATA 256GB SSD</li>
<li>&bull; 3TB SATA3 7200 RPM HD</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$2475</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_EVO_Storm_3000' title='customize Zeus EVO Storm 3000'>
<h1 class='npp-name'>Zeus EVO Storm 3000</h1>
<div class='npp-img'>
<img alt='Zeus EVO Storm 3000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian900d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5960X</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; Intel 750 Series 400GB SSD</li>
<li>&bull; 4TB SATA3 5900 RPM HD</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$3699</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_EVO_Thunder_1000' title='customize Zeus EVO Thunder 1000'>
<h1 class='npp-name'>Zeus EVO Thunder 1000</h1>
<div class='npp-img'>
<img alt='Zeus EVO Thunder 1000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1219</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_EVO_Thunder_2000' title='customize Zeus EVO Thunder 2000'>
<h1 class='npp-name'>Zeus EVO Thunder 2000</h1>
<div class='npp-img'>
<img alt='Zeus EVO Thunder 2000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$1449</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_EVO_Thunder_3000' title='customize Zeus EVO Thunder 3000'>
<h1 class='npp-name'>Zeus EVO Thunder 3000</h1>
<div class='npp-img'>
<img alt='Zeus EVO Thunder 3000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$2105</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_EVO_Thunder_Max' title='customize Zeus EVO Thunder Max'>
<h1 class='npp-name'>Zeus EVO Thunder Max</h1>
<div class='npp-img'>
<img alt='Zeus EVO Thunder Max' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian900d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; Intel 750 Series 400GB SSD</li>
<li>&bull; 4TB SATA3 5900 RPM HD</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$3149</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='9' class='np-sys'>
<dl><dd>
<a href='/system/ZEUS_Mini_EVO_A100' title='customize Zeus Mini EVO A100'>
<h1 class='npp-name'>Zeus Mini EVO A100</h1>
<div class='npp-img'>
<img alt='Zeus Mini EVO A100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/zeusmini2/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD Athlon X4 840 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$475</strong></p>
</a>
</dd>
<dd>
<a href='/system/ZEUS_Mini_EVO_A200' title='customize Zeus Mini EVO A200'>
<h1 class='npp-name'>Zeus Mini EVO A200</h1>
<div class='npp-img'>
<img alt='Zeus Mini EVO A200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/zeusmini2/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD A10-7700K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 120GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$749</strong></p>
</a>
</dd>
<dd>
<a href='/system/ZEUS_Mini_EVO_A300' title='customize Zeus Mini EVO A300'>
<h1 class='npp-name'>Zeus Mini EVO A300</h1>
<div class='npp-img'>
<img alt='Zeus Mini EVO A300' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/zeusmini2/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD A10-7860K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$889</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_Mini_EVO_I100' title='customize Zeus Mini EVO I100'>
<h1 class='npp-name'>Zeus Mini EVO I100</h1>
<div class='npp-img'>
<img alt='Zeus Mini EVO I100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/zeusmini2/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i3-6100</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; Integrated onboard Graphic</li>
<li>&bull; ASRock H110M-ITX/AC Wi-Fi Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$595</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_Mini_EVO_I200' title='customize Zeus Mini EVO I200'>
<h1 class='npp-name'>Zeus Mini EVO I200</h1>
<div class='npp-img'>
<img alt='Zeus Mini EVO I200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/zeusmini2/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6500</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI Z170I Gaming Pro AC mini ITX Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$839</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_Mini_EVO_I960' title='customize Zeus Mini EVO I960'>
<h1 class='npp-name'>Zeus Mini EVO I960</h1>
<div class='npp-img'>
<img alt='Zeus Mini EVO I960' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/zeusmini2/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; MSI Z170I Gaming Pro AC mini ITX Motherboard</li>
<li>&bull; 240GB SSD + 1TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1099</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_Mini_EVO_I970' title='customize Zeus Mini EVO I970'>
<h1 class='npp-name'>Zeus Mini EVO I970</h1>
<div class='npp-img'>
<img alt='Zeus Mini EVO I970' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/zeusmini2/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170I Gaming Pro AC mini ITX Motherboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1219</strong></p>
</a>
</dd>
<dd>
<a href='/system/Zeus_Mini_EVO_I980' title='customize Zeus Mini EVO I980'>
<h1 class='npp-name'>Zeus Mini EVO I980</h1>
<div class='npp-img'>
<img alt='Zeus Mini EVO I980' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/zeusmini2/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; ASRock X99E-ITX/AC Motherboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1835</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='10' class='np-sys'>
<dl><dd>
<a href='/system/Pro_Gamer_FTW_Xtreme_1000' title='customize Pro Gamer FTW Xtreme 1000'>
<h1 class='npp-name'>Pro Gamer FTW Xtreme 1000</h1>
<div class='npp-img'>
<img alt='Pro Gamer FTW Xtreme 1000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX750Ti 2GB</li>
<li>&bull; Avermedia Live Gamer HD Capture Card</li>
<li>&bull; ASUS Z170- PRO GAMING Mainboard</li>
<li>&bull; 1TB WD Black SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1449</strong></p>
</a>
</dd>
<dd>
<a href='/system/Pro_Gamer_FTW_Xtreme_2000' title='customize Pro Gamer FTW Xtreme 2000'>
<h1 class='npp-name'>Pro Gamer FTW Xtreme 2000</h1>
<div class='npp-img'>
<img alt='Pro Gamer FTW Xtreme 2000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; Avermedia Live Gamer HD Capture Card</li>
<li>&bull; ASUS Z170- PRO GAMING Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
</ul>
<p class='npp-price'><strong>$1655</strong></p>
</a>
</dd>
<dd>
<a href='/system/Pro_Gamer_FTW_Xtreme_3000' title='customize Pro Gamer FTW Xtreme 3000'>
<h1 class='npp-name'>Pro Gamer FTW Xtreme 3000</h1>
<div class='npp-img'>
<img alt='Pro Gamer FTW Xtreme 3000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; Avermedia Live Gamer HD Capture Card</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; ADATA 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$2265</strong></p>
</a>
</dd>
<dd>
<a href='/system/Pro_Gamer_FTW_Ultra_1000' title='customize Pro Gamer FTW Ultra 1000'>
<h1 class='npp-name'>Pro Gamer FTW Ultra 1000</h1>
<div class='npp-img'>
<img alt='Pro Gamer FTW Ultra 1000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-6300 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 250 2GB Video</li>
<li>&bull; Avermedia Live Gamer HD Capture Card</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 1TB WD Black SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1259</strong></p>
</a>
</dd>
<dd>
<a href='/system/Pro_Gamer_FTW_Ultra_2000' title='customize Pro Gamer FTW Ultra 2000'>
<h1 class='npp-name'>Pro Gamer FTW Ultra 2000</h1>
<div class='npp-img'>
<img alt='Pro Gamer FTW Ultra 2000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-8320 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; Avermedia Live Gamer HD Capture Card</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB WD Blue SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1479</strong></p>
</a>
</dd>
<dd>
<a href='/system/Pro_Gamer_FTW_Ultra_3000' title='customize Pro Gamer FTW Ultra 3000'>
<h1 class='npp-name'>Pro Gamer FTW Ultra 3000</h1>
<div class='npp-img'>
<img alt='Pro Gamer FTW Ultra 3000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-9590 CPU</li>
<li>&bull; 16GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; Avermedia Live Gamer HD Capture Card</li>
<li>&bull; GIGABYTE 990FXA-UD3 R5 AM3 MB</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB WD Blue SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1715</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='11' class='np-sys'>
<dl><dd>
<a href='/system/Pro_Streaming_I100' title='customize Pro Streaming I100'>
<h1 class='npp-name'>Pro Streaming I100</h1>
<div class='npp-img'>
<img alt='Pro Streaming I100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/enthoominixl/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; GIGABYTE GA-Z170MX-Gaming 5 mATX Motherboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1899</strong></p>
</a>
</dd>
<dd>
<a href='/system/Pro_Streaming_I200' title='customize Pro Streaming I200'>
<h1 class='npp-name'>Pro Streaming I200</h1>
<div class='npp-img'>
<img alt='Pro Streaming I200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/enthoominixl/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; GIGABYTE GA-Z170MX-Gaming 5 mATX Motherboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$2635</strong></p>
</a>
</dd>
<dd>
<a href='/system/Pro_Streaming_I300' title='customize Pro Streaming I300'>
<h1 class='npp-name'>Pro Streaming I300</h1>
<div class='npp-img'>
<img alt='Pro Streaming I300' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/enthoominixl/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 32GB DDR4/2400MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; GIGABYTE X99M-Gaming 5 Micro ATX w/ Killer GbE LAN, 3x Gen3 PCIe x16, 1 PCIe x1,2x M.2, 10x SATA 6Gb/s</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$3119</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='12' class='np-sys'>
<dl><dd>
<a href='/system/Power_Mega_Pro_1000' title='customize Power Mega Pro 1000'>
<h1 class='npp-name'>Power Mega Pro 1000</h1>
<div class='npp-img'>
<img alt='Power Mega Pro 1000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/source340/blkorg_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-4790K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA Quadro K620 2GB</li>
<li>&bull; MSI Z97 PC MATE Motherboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1565</strong></p>
</a>
</dd>
<dd>
<a href='/system/Power_Mega_Pro_2000' title='customize Power Mega Pro 2000'>
<h1 class='npp-name'>Power Mega Pro 2000</h1>
<div class='npp-img'>
<img alt='Power Mega Pro 2000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/source340/blkorg_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA Quadro K620 2GB</li>
<li>&bull; ASUS Z170- PRO GAMING Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
</ul>
<p class='npp-price'><strong>$1205</strong></p>
</a>
</dd>
<dd>
<a href='/system/Power_Mega_Pro_3000' title='customize Power Mega Pro 3000'>
<h1 class='npp-name'>Power Mega Pro 3000</h1>
<div class='npp-img'>
<img alt='Power Mega Pro 3000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440/wht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5930K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA Quadro K2200 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$2455</strong></p>
</a>
</dd>
<dd>
<a href='/system/Power_Mega_Pro_4000' title='customize Power Mega Pro 4000'>
<h1 class='npp-name'>Power Mega Pro 4000</h1>
<div class='npp-img'>
<img alt='Power Mega Pro 4000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h630/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5930K</li>
<li>&bull; 32GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA Quadro K4200 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; Samsung 850 PRO 512GB SSD</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$3205</strong></p>
</a>
</dd>
<dd>
<a href='/system/Power_Mega_Pro_Video' title='customize Power Mega Pro Video'>
<h1 class='npp-name'>Power Mega Pro Video</h1>
<div class='npp-img'>
<img alt='Power Mega Pro Video' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440/wht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; ASUS Z170- PRO GAMING Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
</ul>
<p class='npp-price'><strong>$1415</strong></p>
</a>
</dd>
<dd>
<a href='/system/Power_Mega_Pro_CAD' title='customize Power Mega Pro CAD'>
<h1 class='npp-name'>Power Mega Pro CAD</h1>
<div class='npp-img'>
<img alt='Power Mega Pro CAD' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian750d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5930K</li>
<li>&bull; 32GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA Quadro K4200 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 240GB SSD + 4TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$3065</strong></p>
</a>
</dd>
<dd>
<a href='/system/Power_Mega_Pro_3D_Model' title='customize Power Mega Pro 3D Model'>
<h1 class='npp-name'>Power Mega Pro 3D Model</h1>
<div class='npp-img'>
<img alt='Power Mega Pro 3D Model' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian900d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5960X</li>
<li>&bull; 64GB DDR4/2400MHz RAM</li>
<li>&bull; NVIDIA Quadro K6000 12GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; Samsung 850 PRO 1TB SSD</li>
<li>&bull; 4TB SATA3 5900 RPM HD</li>
</ul>
<p class='npp-price'><strong>$8305</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='13' class='np-sys'>
<dl><dd>
<a href='/system/CyberPowerPC_Zeus_SFF' title='customize CyberPowerPC Zeus SFF'>
<h1 class='npp-name'>CyberPowerPC Zeus SFF</h1>
<div class='npp-img'>
<img alt='CyberPowerPC Zeus SFF' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/inwin901/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Pentium Processor G3258</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD R5 230 1GB Video</li>
<li>&bull; ASRock H81M-ITX/WiFi Motherboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$865</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='14' class='np-sys'>
<dl><dd>
<a href='/system/CyberPower_Black_Pearl' title='customize CyberPower Black Pearl'>
<h1 class='npp-name'>CyberPower Black Pearl</h1>
<div class='npp-img'>
<img alt='CyberPower Black Pearl' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/genesis9000/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1899</strong></p>
</a>
</dd>
</dl>
</div>
</div>
</div>
</div>
<span class='clearfloat'></span>
</div>
<div class='nav-pop' idx='1' onselectstart='return false' style='-moz-user-select:none'>
<div class='np-inner'>
<div class='np-tab'>
<ul>
<li>
<a title='ASUS' idx='0' href='/LandingPages/VenomX/ASUS/' class='selecthis'>ASUS</a>
</li>
<li>
<a title='CORSAIR' idx='1' href='/LandingPages/VenomX/Corsair/'>CORSAIR</a>
</li>
<li>
<a title='EVGA' idx='2' href='/LandingPages/VenomX/EVGA/'>EVGA</a>
</li>
<li>
<a title='GIGABYTE' idx='3' href='/LandingPages/VenomX/GIGABYTE/'>GIGABYTE</a>
</li>
<li>
<a title='MSI' idx='4' href='/LandingPages/VenomX/MSI/'>MSI</a>
</li>
</ul>
</div>
<div class='np-box-container'>
<div class='np-box'>
<div idx='0' class='np-sys' style='display:block'>
<dl><dd>
<a href='/system/VenomX_PBA_I100' title='customize VenomX PBA I100'>
<h1 class='npp-name'>VenomX PBA I100</h1>
<div class='npp-img'>
<img alt='VenomX PBA I100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooPro/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; ASUS ROG MAXIMUS VIII RANGER Mainboard</li>
<li>&bull; 3TB SATA3 7200 RPM HD</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$1960</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_PBA_I200' title='customize VenomX PBA I200'>
<h1 class='npp-name'>VenomX PBA I200</h1>
<div class='npp-img'>
<img alt='VenomX PBA I200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/gun_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; ASUS X99-A/U3.1 ATX Motherboard</li>
<li>&bull; 240GB SSD + 4TB HDD Combo</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$3064</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_PBA_A100' title='customize VenomX PBA A100'>
<h1 class='npp-name'>VenomX PBA A100</h1>
<div class='npp-img'>
<img alt='VenomX PBA A100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/gun_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-9590 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; ASUS Sabertooth AM3 MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1680</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='1' class='np-sys'>
<dl><dd>
<a href='/system/VenomX_CORSAIR_Air_100' title='customize VenomX CORSAIR Air 100'>
<h1 class='npp-name'>VenomX CORSAIR Air 100</h1>
<div class='npp-img'>
<img alt='VenomX CORSAIR Air 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/air240/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-4690K</li>
<li>&bull; 8GB DDR3/1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$1515</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_CORSAIR_Challenger_100' title='customize VenomX CORSAIR Challenger 100'>
<h1 class='npp-name'>VenomX CORSAIR Challenger 100</h1>
<div class='npp-img'>
<img alt='VenomX CORSAIR Challenger 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian450d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1709</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_CORSAIR_Ultimate_100' title='customize VenomX CORSAIR Ultimate 100'>
<h1 class='npp-name'>VenomX CORSAIR Ultimate 100</h1>
<div class='npp-img'>
<img alt='VenomX CORSAIR Ultimate 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian750d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; ASUS X99-A/U3.1 ATX Motherboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$2425</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='2' class='np-sys'>
<dl><dd>
<a href='/system/Hadron_Hydro_100' title='customize Hadron Hydro 100'>
<h1 class='npp-name'>Hadron Hydro 100</h1>
<div class='npp-img'>
<img alt='Hadron Hydro 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hadronseries/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-4690K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1619</strong></p>
</a>
</dd>
<dd>
<a href='/system/Hadron_Hydro_200' title='customize Hadron Hydro 200'>
<h1 class='npp-name'>Hadron Hydro 200</h1>
<div class='npp-img'>
<img alt='Hadron Hydro 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hadronseries/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-4790K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; 3TB SATA3 7200 RPM HD</li>
<li>&bull; DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1809</strong></p>
</a>
</dd>
<dd>
<a href='/system/Hadron_Hydro_300' title='customize Hadron Hydro 300'>
<h1 class='npp-name'>Hadron Hydro 300</h1>
<div class='npp-img'>
<img alt='Hadron Hydro 300' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hadronseries/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-4790K</li>
<li>&bull; 16GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
<li>&bull; DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2145</strong></p>
</a>
</dd>
<dd>
<a href='/system/Hadron_Hydro_400' title='customize Hadron Hydro 400'>
<h1 class='npp-name'>Hadron Hydro 400</h1>
<div class='npp-img'>
<img alt='Hadron Hydro 400' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hadronseries/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-4790K</li>
<li>&bull; 16GB DDR3/2133MHz RAM</li>
<li>&bull; EVGA NVIDIA GeForce GTX980 4GB SC</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
<li>&bull; DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2369</strong></p>
</a>
</dd>
<dd>
<a href='/system/Hadron_Air_100I' title='customize Hadron Air 100I'>
<h1 class='npp-name'>Hadron Air 100I</h1>
<div class='npp-img'>
<img alt='Hadron Air 100I' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hadron/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-4460</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GT730 2GB</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1295</strong></p>
</a>
</dd>
<dd>
<a href='/system/Hadron_Air_200I' title='customize Hadron Air 200I'>
<h1 class='npp-name'>Hadron Air 200I</h1>
<div class='npp-img'>
<img alt='Hadron Air 200I' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hadron/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-4690K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1419</strong></p>
</a>
</dd>
<dd>
<a href='/system/Hadron_Air_300I' title='customize Hadron Air 300I'>
<h1 class='npp-name'>Hadron Air 300I</h1>
<div class='npp-img'>
<img alt='Hadron Air 300I' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hadron/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-4790K</li>
<li>&bull; 16GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; 120GB SSD + 2TB HDD Combo</li>
<li>&bull; DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1899</strong></p>
</a>
</dd>
<dd>
<a href='/system/Hadron_Air_100A' title='customize Hadron Air 100A'>
<h1 class='npp-name'>Hadron Air 100A</h1>
<div class='npp-img'>
<img alt='Hadron Air 100A' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hadron/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD A6-7400K</li>
<li>&bull; 4GB DDR3-PC1600</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1055</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='3' class='np-sys'>
<dl><dd>
<a href='/system/VenomX_GS_A100' title='customize VenomX GS A100'>
<h1 class='npp-name'>VenomX GS A100</h1>
<div class='npp-img'>
<img alt='VenomX GS A100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian750d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-9590 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 390X 8GB Video</li>
<li>&bull; GIGABYTE 990FXA-UD3 R5 AM3 MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1749</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_GS_I100' title='customize VenomX GS I100'>
<h1 class='npp-name'>VenomX GS I100</h1>
<div class='npp-img'>
<img alt='VenomX GS I100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian750d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; GIGABYTE Z170XP-SLI MB</li>
<li>&bull; 3TB SATA3 7200 RPM HD</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$1719</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_GS_I200' title='customize VenomX GS I200'>
<h1 class='npp-name'>VenomX GS I200</h1>
<div class='npp-img'>
<img alt='VenomX GS I200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian750d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; GIGABYTE X99-UD5 Motherboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$2689</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_GIGABYTE_P35WV4_Gaming_Laptop' title='customize VenomX GIGABYTE P35WV4'>
<h1 class='npp-name'>VenomX GIGABYTE P35WV4</h1>
<div class='npp-img'>
<img alt='VenomX GIGABYTE P35WV4' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P35WV3/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-5700HQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1299</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_GIGABYTE_P55WV4_Gaming_Laptop' title='customize VenomX GIGABYTE P55WV4'>
<h1 class='npp-name'>VenomX GIGABYTE P55WV4</h1>
<div class='npp-img'>
<img alt='VenomX GIGABYTE P55WV4' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P55WV4/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel® Core™ Processor i7-5700HQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM97 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1079</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_GIGABYTE_P37XV4_Gaming_Laptop' title='customize VenomX GIGABYTE P37XV4'>
<h1 class='npp-name'>VenomX GIGABYTE P37XV4</h1>
<div class='npp-img'>
<img alt='VenomX GIGABYTE P37XV4' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P37XV4/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-5700HQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel HM97 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1599</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='4' class='np-sys'>
<dl><dd>
<a href='/system/VenomX_MS_A100' title='customize VenomX MS A100'>
<h1 class='npp-name'>VenomX MS A100</h1>
<div class='npp-img'>
<img alt='VenomX MS A100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/gun_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-6300 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 250 2GB Video</li>
<li>&bull; MSI AMD 760G Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1229</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_MS_I100' title='customize VenomX MS I100'>
<h1 class='npp-name'>VenomX MS I100</h1>
<div class='npp-img'>
<img alt='VenomX MS I100' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/gun_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX750Ti 2GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 3TB SATA3 7200 RPM HD</li>
<li>&bull; LG 12X Blu-Ray Combo Drive</li>
</ul>
<p class='npp-price'><strong>$1659</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_MS_I200' title='customize VenomX MS I200'>
<h1 class='npp-name'>VenomX MS I200</h1>
<div class='npp-img'>
<img alt='VenomX MS I200' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/boreallight/gun_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$2329</strong></p>
</a>
</dd>
</dl>
</div>
</div>
</div>
</div>
<span class='clearfloat'></span>
</div>
<div class='nav-pop' idx='2' onselectstart='return false' style='-moz-user-select:none'>
<div class='np-inner'>
<div class='np-tab'>
<ul>
<li>
<a title='Best Sellers' idx='0' href='/category/gaming-pcs/#Best Sellers' class='selecthis'>Best Sellers</a>
</li>
<li>
<a title='Custom Configurators' idx='1' href='/category/gaming-pcs/'>Custom Configurators</a>
</li>
<li>
<a title='Intel X99' idx='2' href='/category/gaming-pcs/'>Intel X99</a>
</li>
<li>
<a title='Intel Z170' idx='3' href='/category/gaming-pcs/#Intel Z170'>Intel Z170</a>
</li>
<li>
<a title='AMD Gaming PC' idx='4' href='/category/gaming-pcs/'>AMD Gaming PC</a>
</li>
</ul>
</div>
<div class='np-box-container'>
<div class='np-box'>
<div idx='0' class='np-sys' style='display:block'>
<dl><dd>
<a href='/system/Cyber_Deal_Z170-I7' title='customize Cyber Deal Z170-I7'>
<h1 class='npp-name'>Cyber Deal Z170-I7</h1>
<div class='npp-img'>
<img alt='Cyber Deal Z170-I7' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xsentinel/org_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 240GB SSD + 1TB HDD Combo</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1289</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_Z170_i7_Configurator' title='customize CyberPower Z170 i7 Configurator'>
<h1 class='npp-name'>CyberPower Z170 i7 Configurator</h1>
<div class='npp-img'>
<img alt='CyberPower Z170 i7 Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$955</strong></p>
</a>
</dd>
<dd>
<a href='/system/March_Madness_Special_I' title='customize March Madness Special I'>
<h1 class='npp-name'>March Madness Special I</h1>
<div class='npp-img'>
<img alt='March Madness Special I' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/source340/blkorg_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 120GB SSD + 1TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$999</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_X99_Configurator' title='customize CyberPower X99 Configurator'>
<h1 class='npp-name'>CyberPower X99 Configurator</h1>
<div class='npp-img'>
<img alt='CyberPower X99 Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1239</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_Black_Pearl' title='customize CyberPower Black Pearl'>
<h1 class='npp-name'>CyberPower Black Pearl</h1>
<div class='npp-img'>
<img alt='CyberPower Black Pearl' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/genesis9000/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1899</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Infinity_8800_Pro_SE' title='customize Infinity 8800 Pro SE'>
<h1 class='npp-name'>Infinity 8800 Pro SE</h1>
<div class='npp-img'>
<img alt='Infinity 8800 Pro SE' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 32GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$2089</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Infinity_8000' title='customize Gamer Infinity 8000'>
<h1 class='npp-name'>Gamer Infinity 8000</h1>
<div class='npp-img'>
<img alt='Gamer Infinity 8000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/source340/blkorg_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R9 390X 8GB Video</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1369</strong></p>
</a>
</dd>
<dd>
<a href='/system/Mega_Special_II' title='customize Mega Special II'>
<h1 class='npp-name'>Mega Special II</h1>
<div class='npp-img'>
<img alt='Mega Special II' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$899</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='1' class='np-sys'>
<dl><dd>
<a href='/system/CyberPower_Z97_i7_Configurator' title='customize CyberPower Z97 i7 Configurator'>
<h1 class='npp-name'>CyberPower Z97 i7 Configurator</h1>
<div class='npp-img'>
<img alt='CyberPower Z97 i7 Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-4790 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; MSI Z97 PC MATE Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$955</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_Z170_i5_Configurator' title='customize CyberPower Z170 i5 Configurator'>
<h1 class='npp-name'>CyberPower Z170 i5 Configurator</h1>
<div class='npp-img'>
<img alt='CyberPower Z170 i5 Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$809</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_Z170_i7_Configurator' title='customize CyberPower Z170 i7 Configurator'>
<h1 class='npp-name'>CyberPower Z170 i7 Configurator</h1>
<div class='npp-img'>
<img alt='CyberPower Z170 i7 Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$955</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_X99_Configurator' title='customize CyberPower X99 Configurator'>
<h1 class='npp-name'>CyberPower X99 Configurator</h1>
<div class='npp-img'>
<img alt='CyberPower X99 Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1239</strong></p>
</a>
</dd>
<dd>
<a href='/system/AMD_Six_Core_Configurator' title='customize AMD Six Core Configurator'>
<h1 class='npp-name'>AMD Six Core Configurator</h1>
<div class='npp-img'>
<img alt='AMD Six Core Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-6300 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$685</strong></p>
</a>
</dd>
<dd>
<a href='/system/AMD_Quad_Core_Configurator' title='customize AMD Quad Core Configurator'>
<h1 class='npp-name'>AMD Quad Core Configurator</h1>
<div class='npp-img'>
<img alt='AMD Quad Core Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-6300 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 250 2GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$545</strong></p>
</a>
</dd>
<dd>
<a href='/system/AMD_Fusion_APU_Configurator' title='customize AMD Fusion APU Configurator'>
<h1 class='npp-name'>AMD Fusion APU Configurator</h1>
<div class='npp-img'>
<img alt='AMD Fusion APU Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD A10-7700K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; GIGABYTE A88X Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$519</strong></p>
</a>
</dd>
<dd>
<a href='/system/AMD_Eight_Core_Configurator' title='customize AMD Eight Core Configurator'>
<h1 class='npp-name'>AMD Eight Core Configurator</h1>
<div class='npp-img'>
<img alt='AMD Eight Core Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-8320 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$605</strong></p>
</a>
</dd>
<dd>
<a href='/system/AMD_SFF_Configurator' title='customize AMD SFF Configurator'>
<h1 class='npp-name'>AMD SFF Configurator</h1>
<div class='npp-img'>
<img alt='AMD SFF Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hybodus/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD A4-7300 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$369</strong></p>
</a>
</dd>
<dd>
<a href='/system/INTEL_i3_SFF_Configurator' title='customize Intel i3 SFF Configurator'>
<h1 class='npp-name'>Intel i3 SFF Configurator</h1>
<div class='npp-img'>
<img alt='Intel i3 SFF Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hybodus/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i3-6100</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; Integrated onboard Graphic</li>
<li>&bull; ASRock H110M-ITX/AC Wi-Fi Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$459</strong></p>
</a>
</dd>
<dd>
<a href='/system/INTEL_i5_SFF_Configurator' title='customize Intel i5 SFF Configurator'>
<h1 class='npp-name'>Intel i5 SFF Configurator</h1>
<div class='npp-img'>
<img alt='Intel i5 SFF Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hybodus/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6500</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R7 250 2GB Video</li>
<li>&bull; ASRock H110M-ITX/AC Wi-Fi Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$599</strong></p>
</a>
</dd>
<dd>
<a href='/system/INTEL_i7_SFF_Configurator' title='customize Intel i7 SFF Configurator'>
<h1 class='npp-name'>Intel i7 SFF Configurator</h1>
<div class='npp-img'>
<img alt='Intel i7 SFF Configurator' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/hybodus/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i3-6100</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; ASRock H110M-ITX/AC Wi-Fi Motherboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$649</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='2' class='np-sys'>
<dl><dd>
<a href='/system/March_Madness_2016' title='customize March Madness 2016'>
<h1 class='npp-name'>March Madness 2016</h1>
<div class='npp-img'>
<img alt='March Madness 2016' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/turbine700/wht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1199</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_Black_Pearl' title='customize CyberPower Black Pearl'>
<h1 class='npp-name'>CyberPower Black Pearl</h1>
<div class='npp-img'>
<img alt='CyberPower Black Pearl' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/genesis9000/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1899</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Xtreme_XT' title='customize Gamer Xtreme XT'>
<h1 class='npp-name'>Gamer Xtreme XT</h1>
<div class='npp-img'>
<img alt='Gamer Xtreme XT' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/source340/blkorg_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1695</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Infinity_8800_Pro_SE' title='customize Infinity 8800 Pro SE'>
<h1 class='npp-name'>Infinity 8800 Pro SE</h1>
<div class='npp-img'>
<img alt='Infinity 8800 Pro SE' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 32GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 240GB SSD + 3TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$2089</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='3' class='np-sys'>
<dl><dd>
<a href='/system/Gamer_Xtreme_1000' title='customize Gamer Xtreme 1000'>
<h1 class='npp-name'>Gamer Xtreme 1000</h1>
<div class='npp-img'>
<img alt='Gamer Xtreme 1000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xtitan100/wht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$915</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Xtreme_2000' title='customize Gamer Xtreme 2000'>
<h1 class='npp-name'>Gamer Xtreme 2000</h1>
<div class='npp-img'>
<img alt='Gamer Xtreme 2000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xtitan/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1099</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Xtreme_3000' title='customize Gamer Xtreme 3000'>
<h1 class='npp-name'>Gamer Xtreme 3000</h1>
<div class='npp-img'>
<img alt='Gamer Xtreme 3000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xtitan100/wht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$1339</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Xtreme_3000_SE' title='customize Gamer Xtreme 3000 SE'>
<h1 class='npp-name'>Gamer Xtreme 3000 SE</h1>
<div class='npp-img'>
<img alt='Gamer Xtreme 3000 SE' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/h440r/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1079</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Infinity_8000' title='customize Gamer Infinity 8000'>
<h1 class='npp-name'>Gamer Infinity 8000</h1>
<div class='npp-img'>
<img alt='Gamer Infinity 8000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/source340/blkorg_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R9 390X 8GB Video</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
</ul>
<p class='npp-price'><strong>$1369</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Infinity_8800_Pro' title='customize Gamer Infinity 8800 Pro'>
<h1 class='npp-name'>Gamer Infinity 8800 Pro</h1>
<div class='npp-img'>
<img alt='Gamer Infinity 8800 Pro' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/clear600c/01_200png.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1349</strong></p>
</a>
</dd>
<dd>
<a href='/system/Mega_Special_II' title='customize Mega Special II'>
<h1 class='npp-name'>Mega Special II</h1>
<div class='npp-img'>
<img alt='Mega Special II' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$899</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Infinity_XLC' title='customize Gamer Infinity XLC'>
<h1 class='npp-name'>Gamer Infinity XLC</h1>
<div class='npp-img'>
<img alt='Gamer Infinity XLC' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/obsidian750d/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$1519</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='4' class='np-sys'>
<dl><dd>
<a href='/system/Gamer_Scorpius_7500' title='customize Gamer Scorpius 7500'>
<h1 class='npp-name'>Gamer Scorpius 7500</h1>
<div class='npp-img'>
<img alt='Gamer Scorpius 7500' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xsentinel/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-6300 CPU</li>
<li>&bull; 4GB DDR3-PC1600</li>
<li>&bull; AMD R7 240 2GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1205</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Dragon' title='customize Gamer Dragon'>
<h1 class='npp-name'>Gamer Dragon</h1>
<div class='npp-img'>
<img alt='Gamer Dragon' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xsentinel/org_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-8320 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$809</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Scorpius_8000' title='customize Gamer Scorpius 8000'>
<h1 class='npp-name'>Gamer Scorpius 8000</h1>
<div class='npp-img'>
<img alt='Gamer Scorpius 8000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/source340/blkorg_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-8320 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$809</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Scorpius_9000' title='customize Gamer Scorpius 9000'>
<h1 class='npp-name'>Gamer Scorpius 9000</h1>
<div class='npp-img'>
<img alt='Gamer Scorpius 9000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/type01/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-9370 CPU</li>
<li>&bull; 16GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 390X 8GB Video</li>
<li>&bull; GIGABYTE 990FXA-UD3 R5 AM3 MB</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
<li>&bull; LG 14X Blu-Ray Rewriter</li>
</ul>
<p class='npp-price'><strong>$1319</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Ultra_7500' title='customize Gamer Ultra 7500'>
<h1 class='npp-name'>Gamer Ultra 7500</h1>
<div class='npp-img'>
<img alt='Gamer Ultra 7500' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/z11/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-6300 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 250 2GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1229</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Ultra_7000_Elite' title='customize Gamer Ultra 7000 Elite'>
<h1 class='npp-name'>Gamer Ultra 7000 Elite</h1>
<div class='npp-img'>
<img alt='Gamer Ultra 7000 Elite' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xtitan/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD A10-7860K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; GIGABYTE A88X Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$715</strong></p>
</a>
</dd>
</dl>
</div>
</div>
</div>
</div>
<span class='clearfloat'></span>
</div>
<div class='nav-pop' idx='3' onselectstart='return false' style='-moz-user-select:none'>
<div class='np-inner'>
<div class='np-tab'>
<ul>
<li>
<a title='Xplorer Xtreme' idx='0' href='/LandingPages/XplorerXtreme/' class='selecthis'>Xplorer Xtreme</a>
</li>
<li>
<a title='Vector Series' idx='1' href='/LandingPages/Vector/'>Vector Series</a>
</li>
<li>
<a title='Fangbook III Series' idx='2' href='/landingpages/FangbookIII/'>Fangbook III Series</a>
</li>
<li>
<a title='Fangbook 4 Series' idx='3' href='http://www.cyberpowerpc.com/landingpages/Fangbook4/'>Fangbook 4 Series</a>
</li>
<li>
<a title='14" Gaming Laptop' idx='4' href='/category/notebook/#14 Gaming Laptop'>14" Gaming Laptop</a>
</li>
<li>
<a title='15.6" Gaming Laptop' idx='5' href='/category/notebook/#15.6 Gaming Laptop'>15.6" Gaming Laptop</a>
</li>
<li>
<a title='17.3" Gaming Laptop' idx='6' href='/category/notebook/#17.3 Gaming Laptop'>17.3" Gaming Laptop</a>
</li>
<li>
<a title='4K Gaming Laptop' idx='7' href='/category/notebook/'>4K Gaming Laptop</a>
</li>
<li>
<a title='G-Sync' idx='8' href='/category/notebook/#G-Sync'>G-Sync</a>
</li>
</ul>
</div>
<div class='np-box-container'>
<div class='np-box'>
<div idx='0' class='np-sys' style='display:block'>
<dl><dd>
<a href='/system/Xplorer_Xtreme_X5-100_G-Sync_Gaming_Laptop' title='customize Xplorer Xtreme X5-100 G-Sync'>
<h1 class='npp-name'>Xplorer Xtreme X5-100 G-Sync</h1>
<div class='npp-img'>
<img alt='Xplorer Xtreme X5-100 G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P750DM/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel Z170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2049</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_Xtreme_X5_G-Sync_Gaming_Laptop' title='customize Xplorer Xtreme X5 G-Sync'>
<h1 class='npp-name'>Xplorer Xtreme X5 G-Sync</h1>
<div class='npp-img'>
<img alt='Xplorer Xtreme X5 G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P750DM/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 4GB</li>
<li>&bull; Intel Z170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1695</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_Xtreme_X7-100_G-Sync_Gaming_Laptop' title='customize Xplorer Xtreme X7-100 G-Sync'>
<h1 class='npp-name'>Xplorer Xtreme X7-100 G-Sync</h1>
<div class='npp-img'>
<img alt='Xplorer Xtreme X7-100 G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P770DM/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel Z170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2145</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_Xtreme_X7_G-Sync_Gaming_Laptop' title='customize Xplorer Xtreme X7 G-Sync'>
<h1 class='npp-name'>Xplorer Xtreme X7 G-Sync</h1>
<div class='npp-img'>
<img alt='Xplorer Xtreme X7 G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P770DM/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel Z170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1789</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='1' class='np-sys'>
<dl><dd>
<a href='/system/Vector-15_100_Gaming_Laptop' title='customize Vector-15 100'>
<h1 class='npp-name'>Vector-15 100</h1>
<div class='npp-img'>
<img alt='Vector-15 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/vector15/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$1019</strong></p>
</a>
</dd>
<dd>
<a href='/system/Vector-15_200_Gaming_Laptop' title='customize Vector-15 200'>
<h1 class='npp-name'>Vector-15 200</h1>
<div class='npp-img'>
<img alt='Vector-15 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/vector15/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$1065</strong></p>
</a>
</dd>
<dd>
<a href='/system/Vector-15_300_Gaming_Laptop' title='customize Vector-15 300'>
<h1 class='npp-name'>Vector-15 300</h1>
<div class='npp-img'>
<img alt='Vector-15 300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/vector15/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$1199</strong></p>
</a>
</dd>
<dd>
<a href='/system/Vector-15_400_Gaming_Laptop' title='customize Vector-15 400'>
<h1 class='npp-name'>Vector-15 400</h1>
<div class='npp-img'>
<img alt='Vector-15 400' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/vector15/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; SanDisk X400 512GB SSD</li>
</ul>
<p class='npp-price'><strong>$1255</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='2' class='np-sys'>
<dl><dd>
<a href='/system/Fangbook_III_HX7-100_Gaming_Laptop' title='customize Fangbook III HX7-100'>
<h1 class='npp-name'>Fangbook III HX7-100</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 880M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1565</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_III_HX7-200_Gaming_Laptop' title='customize Fangbook III HX7-200'>
<h1 class='npp-name'>Fangbook III HX7-200</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 880M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1735</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_III_HX7-300_Gaming_Laptop' title='customize Fangbook III HX7-300'>
<h1 class='npp-name'>Fangbook III HX7-300</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 6X Blu-Ray Internal Player Drive</li>
</ul>
<p class='npp-price'><strong>$2169</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-100_Gaming_Laptop' title='customize FANGBOOK III HX6-100'>
<h1 class='npp-name'>FANGBOOK III HX6-100</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1189</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-160_Gaming_Laptop' title='customize FANGBOOK III HX6-160'>
<h1 class='npp-name'>FANGBOOK III HX6-160</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-160' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1239</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-200_Gaming_Laptop' title='customize FANGBOOK III HX6-200'>
<h1 class='npp-name'>FANGBOOK III HX6-200</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1259</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-300_Gaming_Laptop' title='customize FANGBOOK III HX6-300'>
<h1 class='npp-name'>FANGBOOK III HX6-300</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1319</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='3' class='np-sys'>
<dl><dd>
<a href='/system/Fangbook_4_SX6-100_Gaming_Laptop' title='customize FANGBOOK 4 SX6-100'>
<h1 class='npp-name'>FANGBOOK 4 SX6-100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX6-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX6/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$995</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX6-200_Gaming_Laptop' title='customize FANGBOOK 4 SX6-200'>
<h1 class='npp-name'>FANGBOOK 4 SX6-200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX6-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX6/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1329</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX6-4K_Gaming_Laptop' title='customize FANGBOOK 4 SX6-4K'>
<h1 class='npp-name'>FANGBOOK 4 SX6-4K</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX6-4K' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX6/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840 x 2160 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1475</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX7-100_Gaming_Laptop' title='customize FANGBOOK 4 SX7-100'>
<h1 class='npp-name'>FANGBOOK 4 SX7-100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX7-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX7/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$979</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX7-200_Gaming_Laptop' title='customize FANGBOOK 4 SX7-200'>
<h1 class='npp-name'>FANGBOOK 4 SX7-200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX7-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX7/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1115</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SX7-300_Gaming_Laptop' title='customize FANGBOOK 4 SX7-300'>
<h1 class='npp-name'>FANGBOOK 4 SX7-300</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX7-300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX7/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1259</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_G-SYNC_100_Gaming_Laptop' title='customize FANGBOOK 4 XTREME G-SYNC 100'>
<h1 class='npp-name'>FANGBOOK 4 XTREME G-SYNC 100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME G-SYNC 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREME/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840x2160 G-SYNC UHD LCD</li>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel Z170 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1839</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_Xtreme_G-SYNC_200_Gaming_Laptop' title='customize FANGBOOK 4 XTREME G-SYNC 200'>
<h1 class='npp-name'>FANGBOOK 4 XTREME G-SYNC 200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME G-SYNC 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREME/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840x2160 G-SYNC UHD LCD</li>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel Z170 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2205</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SK-X17_XTREME_G-Sync_Gaming_Laptop' title='customize FANGBOOK 4 SK-X17 XTREME G-Sync'>
<h1 class='npp-name'>FANGBOOK 4 SK-X17 XTREME G-Sync</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SK-X17 XTREME G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SKX17/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980 8GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2385</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SK-X17_PRO_Gaming_Laptop' title='customize FANGBOOK 4 SK-X17 PRO'>
<h1 class='npp-name'>FANGBOOK 4 SK-X17 PRO</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SK-X17 PRO' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SKX17/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1499</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_SK-X17_XTREME_Gaming_Laptop' title='customize FANGBOOK 4 SK-X17 XTREME'>
<h1 class='npp-name'>FANGBOOK 4 SK-X17 XTREME</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SK-X17 XTREME' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SKX17/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1749</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_100_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 100'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970 12GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2389</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_200_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 200'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 16GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2915</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_300_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 300'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 300</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6920HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 16GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$3195</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_400_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 400'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 400</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 400' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980 16GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$3529</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_SX-L_500_Gaming_Laptop' title='customize FANGBOOK 4 XTREME SX-L 500'>
<h1 class='npp-name'>FANGBOOK 4 XTREME SX-L 500</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME SX-L 500' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREMESXLSLI/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 18.4" 1920x1080 FHD LCD</li>
<li>&bull; Intel Core Processor i7-6920HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980 16GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$3809</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='4' class='np-sys'>
<dl><dd>
<a href='/system/Xplorer_M3_Gaming_Laptop' title='customize Xplorer M3 Gaming Laptop'>
<h1 class='npp-name'>Xplorer M3 Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer M3 Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P640RE/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 14" 1920 x 1080 LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1295</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='5' class='np-sys'>
<dl><dd>
<a href='/system/VenomX_GIGABYTE_P35WV4_Gaming_Laptop' title='customize VenomX GIGABYTE P35WV4'>
<h1 class='npp-name'>VenomX GIGABYTE P35WV4</h1>
<div class='npp-img'>
<img alt='VenomX GIGABYTE P35WV4' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P35WV3/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-5700HQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1299</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-100_Gaming_Laptop' title='customize FANGBOOK III HX6-100'>
<h1 class='npp-name'>FANGBOOK III HX6-100</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1189</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-160_Gaming_Laptop' title='customize FANGBOOK III HX6-160'>
<h1 class='npp-name'>FANGBOOK III HX6-160</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-160' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1239</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-200_Gaming_Laptop' title='customize FANGBOOK III HX6-200'>
<h1 class='npp-name'>FANGBOOK III HX6-200</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1259</strong></p>
</a>
</dd>
<dd>
<a href='/system/FANGBOOK_III_HX6-300_Gaming_Laptop' title='customize FANGBOOK III HX6-300'>
<h1 class='npp-name'>FANGBOOK III HX6-300</h1>
<div class='npp-img'>
<img alt='FANGBOOK III HX6-300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/fb3hx6v2/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 IPS HD</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 960M 4GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1319</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_GIGABYTE_P55WV4_Gaming_Laptop' title='customize VenomX GIGABYTE P55WV4'>
<h1 class='npp-name'>VenomX GIGABYTE P55WV4</h1>
<div class='npp-img'>
<img alt='VenomX GIGABYTE P55WV4' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P55WV4/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel® Core™ Processor i7-5700HQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM97 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1079</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_X6-9500_Gaming_Laptop' title='customize Xplorer X6-9500 Gaming Laptop'>
<h1 class='npp-name'>Xplorer X6-9500 Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X6-9500 Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P650RE/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1479</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_X5-6700_Gaming_Laptop' title='customize Xplorer X5-6700 Gaming Laptop'>
<h1 class='npp-name'>Xplorer X5-6700 Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X5-6700 Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P650RE/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1849</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_X5-6820K_Gaming_Laptop' title='customize Xplorer X5-6820HK Gaming Laptop'>
<h1 class='npp-name'>Xplorer X5-6820HK Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X5-6820HK Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P650RE/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1965</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_X5-6300_Gaming_Laptop' title='customize Xplorer X5-6300 Gaming Laptop'>
<h1 class='npp-name'>Xplorer X5-6300 Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X5-6300 Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/W650RC/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i5-6300HQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 950M 2GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$975</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_X5-6600_Gaming_Laptop' title='customize Xplorer X5-6600 Gaming Laptop'>
<h1 class='npp-name'>Xplorer X5-6600 Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X5-6600 Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/W650RC/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 950M 2GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1025</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='6' class='np-sys'>
<dl><dd>
<a href='/system/Xplorer_X7-6700_Gaming_Laptop' title='customize Xplorer X7-6700 Gaming Laptop'>
<h1 class='npp-name'>Xplorer X7-6700 Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X7-6700 Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P670RE/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB GDDR5 DirectX® 12</li>
<li>&bull; Intel HM170 Chipset Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1485</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_X7-6820K_Gaming_Laptop' title='customize Xplorer X7-6820K Gaming Laptop'>
<h1 class='npp-name'>Xplorer X7-6820K Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X7-6820K Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P670RE/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB GDDR5 DirectX® 12</li>
<li>&bull; Intel HM170 Chipset Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1599</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_III_HX7-100_Gaming_Laptop' title='customize Fangbook III HX7-100'>
<h1 class='npp-name'>Fangbook III HX7-100</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 880M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1565</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_III_HX7-200_Gaming_Laptop' title='customize Fangbook III HX7-200'>
<h1 class='npp-name'>Fangbook III HX7-200</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 880M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1735</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_III_HX7-300_Gaming_Laptop' title='customize Fangbook III HX7-300'>
<h1 class='npp-name'>Fangbook III HX7-300</h1>
<div class='npp-img'>
<img alt='Fangbook III HX7-300' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 6X Blu-Ray Internal Player Drive</li>
</ul>
<p class='npp-price'><strong>$2169</strong></p>
</a>
</dd>
<dd>
<a href='/system/VenomX_GIGABYTE_P37XV4_Gaming_Laptop' title='customize VenomX GIGABYTE P37XV4'>
<h1 class='npp-name'>VenomX GIGABYTE P37XV4</h1>
<div class='npp-img'>
<img alt='VenomX GIGABYTE P37XV4' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P37XV4/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-5700HQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel HM97 Mainboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1599</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_X7-6300_Gaming_Laptop' title='customize Xplorer X7-6300 Gaming Laptop'>
<h1 class='npp-name'>Xplorer X7-6300 Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X7-6300 Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/W670RCQ/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i5-6300HQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 950M 2GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1029</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_X7-6600_Gaming_Laptop' title='customize Xplorer X7-6600 Gaming Laptop'>
<h1 class='npp-name'>Xplorer X7-6600 Gaming Laptop</h1>
<div class='npp-img'>
<img alt='Xplorer X7-6600 Gaming Laptop' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/W670RCQ/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 8GB DDR3-1600 RAM</li>
<li>&bull; NVIDIA GeForce GTX 950M 2GB</li>
<li>&bull; Intel HM170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1085</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='7' class='np-sys'>
<dl><dd>
<a href='/system/Fangbook_4_SX6-4K_Gaming_Laptop' title='customize FANGBOOK 4 SX6-4K'>
<h1 class='npp-name'>FANGBOOK 4 SX6-4K</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SX6-4K' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SX6/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840 x 2160 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700HQ</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 3GB</li>
<li>&bull; Intel HM170 Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1475</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_G-SYNC_100_Gaming_Laptop' title='customize FANGBOOK 4 XTREME G-SYNC 100'>
<h1 class='npp-name'>FANGBOOK 4 XTREME G-SYNC 100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME G-SYNC 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREME/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840x2160 G-SYNC UHD LCD</li>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel Z170 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1839</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_Xtreme_G-SYNC_200_Gaming_Laptop' title='customize FANGBOOK 4 XTREME G-SYNC 200'>
<h1 class='npp-name'>FANGBOOK 4 XTREME G-SYNC 200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME G-SYNC 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREME/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840x2160 G-SYNC UHD LCD</li>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel Z170 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2205</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='8' class='np-sys'>
<dl><dd>
<a href='/system/Fangbook_4_SK-X17_XTREME_G-Sync_Gaming_Laptop' title='customize FANGBOOK 4 SK-X17 XTREME G-Sync'>
<h1 class='npp-name'>FANGBOOK 4 SK-X17 XTREME G-Sync</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 SK-X17 XTREME G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4SKX17/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6820HK</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980 8GB</li>
<li>&bull; Intel CM236 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$2385</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_XTREME_G-SYNC_100_Gaming_Laptop' title='customize FANGBOOK 4 XTREME G-SYNC 100'>
<h1 class='npp-name'>FANGBOOK 4 XTREME G-SYNC 100</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME G-SYNC 100' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREME/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840x2160 G-SYNC UHD LCD</li>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel Z170 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1839</strong></p>
</a>
</dd>
<dd>
<a href='/system/Fangbook_4_Xtreme_G-SYNC_200_Gaming_Laptop' title='customize FANGBOOK 4 XTREME G-SYNC 200'>
<h1 class='npp-name'>FANGBOOK 4 XTREME G-SYNC 200</h1>
<div class='npp-img'>
<img alt='FANGBOOK 4 XTREME G-SYNC 200' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FB4XTREME/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 3840x2160 G-SYNC UHD LCD</li>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel Z170 Chipset Mainboard</li>
<li>&bull; 256GB SanDisk X400 M.2 SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2205</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_Xtreme_X5_G-Sync_Gaming_Laptop' title='customize Xplorer Xtreme X5 G-Sync'>
<h1 class='npp-name'>Xplorer Xtreme X5 G-Sync</h1>
<div class='npp-img'>
<img alt='Xplorer Xtreme X5 G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P750DM/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 4GB</li>
<li>&bull; Intel Z170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1695</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_Xtreme_X5-100_G-Sync_Gaming_Laptop' title='customize Xplorer Xtreme X5-100 G-Sync'>
<h1 class='npp-name'>Xplorer Xtreme X5-100 G-Sync</h1>
<div class='npp-img'>
<img alt='Xplorer Xtreme X5-100 G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P750DM/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 15.6" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel Z170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2049</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_Xtreme_X7_G-Sync_Gaming_Laptop' title='customize Xplorer Xtreme X7 G-Sync'>
<h1 class='npp-name'>Xplorer Xtreme X7 G-Sync</h1>
<div class='npp-img'>
<img alt='Xplorer Xtreme X7 G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P770DM/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 970M 6GB</li>
<li>&bull; Intel Z170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1789</strong></p>
</a>
</dd>
<dd>
<a href='/system/Xplorer_Xtreme_X7-100_G-Sync_Gaming_Laptop' title='customize Xplorer Xtreme X7-100 G-Sync'>
<h1 class='npp-name'>Xplorer Xtreme X7-100 G-Sync</h1>
<div class='npp-img'>
<img alt='Xplorer Xtreme X7-100 G-Sync' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/P770DM/combo_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920 x 1080 Full HD LCD</li>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2133MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 980M 8GB</li>
<li>&bull; Intel Z170 Chipset</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2145</strong></p>
</a>
</dd>
</dl>
</div>
</div>
</div>
</div>
<span class='clearfloat'></span>
</div>
<div class='nav-pop' idx='4' onselectstart='return false' style='-moz-user-select:none'>
<div class='np-inner'>
<div class='np-tab'>
<ul>
<li>
<a title='Vapor Series' idx='0' href='/landingpages/syber/' class='selecthis'>Vapor Series</a>
</li>
<li>
<a title='Steam Series' idx='1' href='/landingpages/syber/'>Steam Series</a>
</li>
</ul>
</div>
<div class='np-box-container'>
<div class='np-box'>
<div idx='0' class='np-sys' style='display:block'>
<dl><dd>
<a href='/system/SYBER_VAPOR_A' title='customize SYBER VAPOR A'>
<h1 class='npp-name'>SYBER VAPOR A</h1>
<div class='npp-img'>
<img alt='SYBER VAPOR A' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD Athlon X4 840 CPU</li>
<li>&bull; 4GB DDR3-PC1600</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$589</strong></p>
</a>
</dd>
<dd>
<a href='/system/SYBER_VAPOR_E' title='customize SYBER VAPOR E'>
<h1 class='npp-name'>SYBER VAPOR E</h1>
<div class='npp-img'>
<img alt='SYBER VAPOR E' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD Athlon X4 840 CPU</li>
<li>&bull; 4GB DDR3-PC1600</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; GIGABYTE F2A88XN-WIFI Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$499</strong></p>
</a>
</dd>
<dd>
<a href='/system/SYBER_VAPOR_I' title='customize SYBER VAPOR I'>
<h1 class='npp-name'>SYBER VAPOR I</h1>
<div class='npp-img'>
<img alt='SYBER VAPOR I' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i3-6100</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX750Ti 2GB</li>
<li>&bull; ASRock H110M-ITX/AC Wi-Fi Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$679</strong></p>
</a>
</dd>
<dd>
<a href='/system/SYBER_VAPOR_K' title='customize SYBER VAPOR K'>
<h1 class='npp-name'>SYBER VAPOR K</h1>
<div class='npp-img'>
<img alt='SYBER VAPOR K' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; ASRock H110M-ITX/AC Wi-Fi Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1075</strong></p>
</a>
</dd>
<dd>
<a href='/system/SYBER_VAPOR_P' title='customize SYBER VAPOR P'>
<h1 class='npp-name'>SYBER VAPOR P</h1>
<div class='npp-img'>
<img alt='SYBER VAPOR P' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i3-6100</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; ASRock H110M-ITX/AC Wi-Fi Motherboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$645</strong></p>
</a>
</dd>
<dd>
<a href='/system/SYBER_VAPOR_TITAN' title='customize SYBER VAPOR TITAN'>
<h1 class='npp-name'>SYBER VAPOR TITAN</h1>
<div class='npp-img'>
<img alt='SYBER VAPOR TITAN' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-4790K</li>
<li>&bull; 16GB DDR3/1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX TITAN X 12GB</li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; Kingston 240GB HyperX Savage SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$2319</strong></p>
</a>
</dd>
<dd>
<a href='/system/SYBER_VAPOR_Xtreme' title='customize SYBER VAPOR Xtreme'>
<h1 class='npp-name'>SYBER VAPOR Xtreme</h1>
<div class='npp-img'>
<img alt='SYBER VAPOR Xtreme' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; MSI Z170I Gaming Pro AC mini ITX Motherboard</li>
<li>&bull; ADATA 256GB SSD</li>
</ul>
<p class='npp-price'><strong>$1495</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='1' class='np-sys'>
<dl><dd>
<a href='/system/Steam_Machine_I' title='customize Steam Machine I'>
<h1 class='npp-name'>Steam Machine I</h1>
<div class='npp-img'>
<img alt='Steam Machine I' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/SyberSteam_wt_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i3-4160</li>
<li>&bull; 4GB DDR3-PC1600</li>
<li>&bull; NVIDIA GeForce GTX750 1GB</li>
<li>&bull; ASRock H81M-ITX/WiFi Motherboard</li>
<li>&bull; 500GB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$499</strong></p>
</a>
</dd>
<dd>
<a href='/system/Steam_Machine_P' title='customize Steam Machine P'>
<h1 class='npp-name'>Steam Machine P</h1>
<div class='npp-img'>
<img alt='Steam Machine P' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/SyberSteam_wt_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-4460</li>
<li>&bull; 8GB DDR3/1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 2GB</li>
<li>&bull; ASRock H81M-ITX/WiFi Motherboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$869</strong></p>
</a>
</dd>
<dd>
<a href='/system/Steam_Machine_X' title='customize Steam Machine X'>
<h1 class='npp-name'>Steam Machine X</h1>
<div class='npp-img'>
<img alt='Steam Machine X' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/sm/SyberSteam_wt_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-4790K</li>
<li>&bull; 16GB DDR3/1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 4GB </li>
<li>&bull; GIGABYTE Z97 Motherboard</li>
<li>&bull; 1TB 7200RPM HDD</li>
</ul>
<p class='npp-price'><strong>$1559</strong></p>
</a>
</dd>
</dl>
</div>
</div>
</div>
</div>
<span class='clearfloat'></span>
</div>
<div class='nav-pop' idx='5' onselectstart='return false' style='-moz-user-select:none'>
<div class='np-inner'>
<div class='np-tab'>
<ul>
<li>
<a title='March Madness Daily Deal' idx='0' href='/LandingPages/MegaSpecial/' class='selecthis'>March Madness Daily Deal</a>
</li>
<li>
<a title=' Special PC' idx='1' href='/'> Special PC</a>
</li>
<li>
<a title='Business Desktops' idx='2' href='/LandingPages/MegaSpecial/#Business Desktops'>Business Desktops</a>
</li>
</ul>
</div>
<div class='np-box-container'>
<div class='np-box'>
<div idx='0' class='np-sys' style='display:block'>
<dl><dd>
<a href='/system/Cyber_Deal_Z170-I7' title='customize Cyber Deal Z170-I7'>
<h1 class='npp-name'>Cyber Deal Z170-I7</h1>
<div class='npp-img'>
<img alt='Cyber Deal Z170-I7' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xsentinel/org_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 240GB SSD + 1TB HDD Combo</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1289</strong></p>
</a>
</dd>
<dd>
<a href='/system/Cyber_Deal_AMD_8-Core' title='customize Cyber Deal AMD 8-Core'>
<h1 class='npp-name'>Cyber Deal AMD 8-Core</h1>
<div class='npp-img'>
<img alt='Cyber Deal AMD 8-Core' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/z11/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-8320 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 360 2GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$759</strong></p>
</a>
</dd>
<dd>
<a href='/system/Cyber_Deal_X99' title='customize Cyber Deal X99'>
<h1 class='npp-name'>Cyber Deal X99</h1>
<div class='npp-img'>
<img alt='Cyber Deal X99' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/RaidmaxViperII/wht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1439</strong></p>
</a>
</dd>
<dd>
<a href='/system/Cyber_Deal_Z170-I5' title='customize Cyber Deal Z170-I5'>
<h1 class='npp-name'>Cyber Deal Z170-I5</h1>
<div class='npp-img'>
<img alt='Cyber Deal Z170-I5' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/g7/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 120GB SSD + 1TB HDD Combo</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1075</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_Black_Pearl' title='customize CyberPower Black Pearl'>
<h1 class='npp-name'>CyberPower Black Pearl</h1>
<div class='npp-img'>
<img alt='CyberPower Black Pearl' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/genesis9000/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX980 Ti 6GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; SanDisk Z400S 256GB SSD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1899</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Dragon' title='customize Gamer Dragon'>
<h1 class='npp-name'>Gamer Dragon</h1>
<div class='npp-img'>
<img alt='Gamer Dragon' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xsentinel/org_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-8320 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$809</strong></p>
</a>
</dd>
<dd>
<a href='/system/March_Madness_Fangbook_III_HX7' title='customize March Madness Fangbook III HX7'>
<h1 class='npp-name'>March Madness Fangbook III HX7</h1>
<div class='npp-img'>
<img alt='March Madness Fangbook III HX7' class='lazyload' src='/images/spacer.gif' mSrc='/images/nb/FANGBOOKEVO/02_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; 17.3" 1920x1080 HD LED-Backlit</li>
<li>&bull; Intel Core Processor i7-4710MQ</li>
<li>&bull; 16GB 1600MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX 880M 8GB</li>
<li>&bull; Intel HM87 Mainboard</li>
<li>&bull; SanDisk 128GB Z400S SSD</li>
<li>&bull; 1TB 7200RPM HDD</li>
<li>&bull; 8X DVD Rewritable Drive</li>
</ul>
<p class='npp-price'><strong>$1649</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='1' class='np-sys'>
<dl><dd>
<a href='/system/Mega_Special_I' title='customize Mega Special I'>
<h1 class='npp-name'>Mega Special I</h1>
<div class='npp-img'>
<img alt='Mega Special I' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xtitan/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD FX-8320 CPU</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R9 380 4GB Video</li>
<li>&bull; GIGABYTE AM3+ MB</li>
<li>&bull; 120GB SSD + 1TB HDD Combo</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$925</strong></p>
</a>
</dd>
<dd>
<a href='/system/Mega_Special_II' title='customize Mega Special II'>
<h1 class='npp-name'>Mega Special II</h1>
<div class='npp-img'>
<img alt='Mega Special II' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$899</strong></p>
</a>
</dd>
<dd>
<a href='/system/Mega_Special_III' title='customize Mega Special III'>
<h1 class='npp-name'>Mega Special III</h1>
<div class='npp-img'>
<img alt='Mega Special III' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/type01/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX960 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1145</strong></p>
</a>
</dd>
<dd>
<a href='/system/Mega_Special_IV' title='customize Mega Special IV'>
<h1 class='npp-name'>Mega Special IV</h1>
<div class='npp-img'>
<img alt='Mega Special IV' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/EnthooProM/blkwht_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 16GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 120GB SSD + 1TB HDD Combo</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1549</strong></p>
</a>
</dd>
<dd>
<a href='/system/Gamer_Infinity_8800_Pro' title='customize Gamer Infinity 8800 Pro'>
<h1 class='npp-name'>Gamer Infinity 8800 Pro</h1>
<div class='npp-img'>
<img alt='Gamer Infinity 8800 Pro' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/clear600c/01_200png.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-6700K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX970 4GB</li>
<li>&bull; MSI Z170A Gaming Pro CARBON Mainboard</li>
<li>&bull; 240GB SSD + 2TB HDD Combo</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1349</strong></p>
</a>
</dd>
</dl>
</div>
<div idx='2' class='np-sys'>
<dl><dd>
<a href='/system/Essential_3000' title='customize Essential 3000'>
<h1 class='npp-name'>Essential 3000</h1>
<div class='npp-img'>
<img alt='Essential 3000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xnova/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-4460</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; AMD Radeon R7 250 2GB Video</li>
<li>&bull; ASRock H97M Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$1055</strong></p>
</a>
</dd>
<dd>
<a href='/system/Essential_2000' title='customize Essential 2000'>
<h1 class='npp-name'>Essential 2000</h1>
<div class='npp-img'>
<img alt='Essential 2000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xnova/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i3-4160</li>
<li>&bull; 4GB DDR3-PC1600</li>
<li>&bull; AMD R5 230 1GB Video</li>
<li>&bull; ASRock H97M Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$839</strong></p>
</a>
</dd>
<dd>
<a href='/system/Essential_1000' title='customize Essential 1000'>
<h1 class='npp-name'>Essential 1000</h1>
<div class='npp-img'>
<img alt='Essential 1000' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xnova/01_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Pentium Processor G3258</li>
<li>&bull; 4GB DDR3-PC1600</li>
<li>&bull; ASRock H97M Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
</ul>
<p class='npp-price'><strong>$749</strong></p>
</a>
</dd>
</dl>
</div>
</div>
</div>
</div>
<span class='clearfloat'></span>
</div>
<div class='nav-pop' idx='6' onselectstart='return false' style='-moz-user-select:none'>
<div class='np-inner'>
<div class='np-tab'>
<ul>
<li>
<a title='Same Day System' idx='0' href='/category/same_day_shipping/#Same Day System' class='selecthis'>Same Day System</a>
</li>
</ul>
</div>
<div class='np-box-container'>
<div class='np-box'>
<div idx='0' class='np-sys' style='display:block'>
<dl><dd>
<a href='/system/CyberPower_APU_Same_Day' title='customize CyberPower APU Same Day'>
<h1 class='npp-name'>CyberPower APU Same Day</h1>
<div class='npp-img'>
<img alt='CyberPower APU Same Day' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xtitan/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; AMD A6-7400K</li>
<li>&bull; 8GB DDR3/2133MHz RAM</li>
<li>&bull; GIGABYTE A88X Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$649</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_Z170_Same_Day' title='customize CyberPower Z170 Same Day'>
<h1 class='npp-name'>CyberPower Z170 Same Day</h1>
<div class='npp-img'>
<img alt='CyberPower Z170 Same Day' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xtitan/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i5-6600K</li>
<li>&bull; 8GB DDR4/2800MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; ASUS Z170- PRO GAMING Mainboard</li>
<li>&bull; 1TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1035</strong></p>
</a>
</dd>
<dd>
<a href='/system/CyberPower_Core_I7_Same_Day' title='customize CyberPower Core I7 Same Day'>
<h1 class='npp-name'>CyberPower Core I7 Same Day</h1>
<div class='npp-img'>
<img alt='CyberPower Core I7 Same Day' class='lazyload' src='/images/spacer.gif' mSrc='/images/cs/xtitan/blk_200.png'/>
</div>
<ul class='npp-info'>
<li>&bull; Intel Core Processor i7-5820K</li>
<li>&bull; 8GB DDR4/2400MHz RAM</li>
<li>&bull; NVIDIA GeForce GTX950 2GB</li>
<li>&bull; MSI X99A Motherboard</li>
<li>&bull; 2TB SATA3 7200 RPM HD</li>
<li>&bull; 24X DVDRW Drive</li>
</ul>
<p class='npp-price'><strong>$1329</strong></p>
</a>
</dd>
</dl>
</div>
</div>
</div>
</div>
<span class='clearfloat'></span>
</div>
</div>
</div> 
<div id="wrap" class="c_s">
<div id="content">
<div class="c-s-header ss_bg">
<div class="c-s-img">
<img src="/ti/company/Privacy.png" alt="Privacy Policy"/>
</div>
<div class="c-s-menu">
<ul class="s-s-memu">
<li><a href="/support/" title="CyberpowerPC Tech Support">Tech Support</a><div class="slidedown"></div></li>
<li class="current"><a href="/company/privacy.aspx" title="CyberpowerPC Privacy Policy">Privacy Policy</a><div class="slidedown"></div></li>
<li><a href="/company/warranty.aspx" title="CyberpowerPC Warranty Info">Warranty Info</a><div class="slidedown"></div></li>
<li><a href="/company/extended-service-plan.aspx" title="CyberpowerPC Extended Service Plan">Extended Service Plan</a><div class="slidedown"></div></li>
<li><a href="/orderstatus/" title="CyberpowerPC Order Status">Order Status</a><div class="slidedown"></div></li>
<li><a href="/faq/" title="CyberpowerPC Q & A">Q & A</a><div class="slidedown"></div></li>
</ul>
</div>
</div>
<div class="c-s-content">
<div class="privacy">
<h3>Information</h3>
<p>When you make purchase requests, we do collect and store the names, addresses, credit card numbers and other personally-identifying information from you. We use this information to process your orders, to keep you updated on your orders and to personalize your shopping experience. This information is saved in a secure environment and is kept for our records and to make it easier for you to use our service on return visits. We do not reveal your name, address, credit card number or other personal-identifying information to any third parties or let the information become accessible to the general public without your consent.</p>
<h3>Cookie Usage</h3>
<p>A "cookie" is a small piece of information which a web server can store information temporarily with your web browser. This is useful for having your web browser remember some specific information which our web server can later retrieve. One purpose of these technologies is to help us better understand the user's behavior and which part of our website is frequented the most. CyberpowerPC and its partners use cookies to measure the effectiveness of our ad campaigns and promotions. This allows us to provide special promotions and deals that our customers would find the most interest in.</p>
<p>CyberpowerPC and our partners also employ the use of cookies that are essential to the shopping experience of our website. These cookies are used to maintain the state of your shopping cart and to complete the checkout process. Our goal in these cases is to make your shopping experience with CyberpowerPC more convenient. For example, we use cookies to store the customer's shopping cart so they may complete their shopping experience. If the customer leaves our website and comes back at a later time, we are able to restore any preexisting configurations that the customer has been working on and allows our customer resume where they left off. In addition, we also employ cookies from our partners to combat fraudulent charges during our checkout process. This allows us to expedite our order processing and verification process once your order is submitted to us.</p>
<p>If you want to disable and delete cookies stored on your internet browser, we have provided below a list of links to most used internet browsers that explain how to disable and delete cookies. If your browser is not listed below, check with your provider to find out how to disable cookies. Please note that certain features of the CyberpowerPC website will not be available once cookies are disabled.</p>
<p>
&bull; Chrome - <a href="https://support.google.com/accounts/answer/61416?hl=en" target="_blank">https://support.google.com/accounts/answer/61416?hl=en</a><br/>
&bull; Firefox - <a href="https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences" target="_blank">https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences</a><br/>
&bull; Internet Explorer - <a href="http://windows.microsoft.com/en-us/internet-explorer/delete-manage-cookies#ie=ie-11" target="_blank">http://windows.microsoft.com/en-us/internet-explorer/delete-manage-cookies#ie=ie-11</a><br/>
&bull; Opera - <a href="http://www.opera.com/help/tutorials/security/privacy/" target="_blank">http://www.opera.com/help/tutorials/security/privacy/</a><br/>
&bull; Safari - <a href="http://support.apple.com/kb/ph11913" target="_blank">http://support.apple.com/kb/ph11913</a><br/>
</p><h3>Security</h3>
<p>CyberPowerPC is committed to taking reasonable steps to ensure the security of your information. To prevent unauthorized access, maintain data accuracy, and ensure the appropriate use of information, we have put in place appropriate physical, electronic, and managerial procedures to safeguard and secure the information we collect online.</p>
<p>Your credit card security is our highest priority. CyberPowerPC utilizes Secure Sockets Layer (SSL) encryption, an advanced encryption technology that protects the information you send us against unauthorized access. This safeguard has helped, in many cases, to make shopping on the Internet significantly safer than using your credit card at a restaurant or department store</p>
<p>If you have any question regarding our online security and privacy, you may contact us at <a href="/cdn-cgi/l/email-protection#bddec4dfd8cfcdd2cad8cffddec4dfd8cfcdd2cad8cfcdde93ded2d0"><span class="__cf_email__" data-cfemail="eb8892898e999b849c8e99ab8892898e999b849c8e999b88c5888486c5">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></p>
</div>
</div>
</div>
</div>  
 
<div id="footerc">
<div id="footer">
<div class="call_share">
<div class="powerby">
<p>CYBERPOWERPC SYSTEMS POWERED BY:</p>
<a href="/company/powered.aspx" title="click to view all cyberpowerpc gaming pc powered by and where cyberpowerpc gaming pc available at">
<img src="/ti/Powered_small.png?v2" alt="Powered by logo"/></a>
</div>
<div class="available">
<p>CYBERPOWERPC SYSTEMS AVAILABLE AT:</p>
<a href="/company/powered.aspx" title="click to view all cyberpowerpc gaming pc powered by and where cyberpowerpc gaming pc available at">
<img src="/ti/Available-small3_021916.png" alt="Available at logo"/></a>
</div>
<div class="call">Order Online or Call: <span>(800)707-0393</span></div>
<div class="share">
<a class="blog" href="http://blog.cyberpowerpc.com/" title="Visit cyberpowerpc blog" target="_blank"><span>Visit cyberpowerpc blog</span></a>
<a class="shareicon si_tt" href="http://www.twitter.com/cyberpowerpc" title="Follow us on Twitter" target="_blank"><span>Follow us on Twitter</span></a>
<a class="shareicon si_fb" href="http://www.facebook.com/pages/CyberPower-Inc/39537220420" title="Become our fan on Facebook" target="_blank"><span>Become our fan on Facebook</span></a>
<a class="shareicon si_yt" href="http://www.youtube.com/cyberpowerpcinc" title="Watch our videos on YouTube" target="_blank"><span>Watch our Videos on YouTube</span></a>
<a class="shareicon si_gp" href="https://plus.google.com/u/0/115982845543827874800" title="Add us to your google plus circle" target="_blank"><span>Add us to your google plus circle</span></a>
<a class="shareicon si_bd" href="http://instagram.com/cyberpowerpc?ref=badge" title="Follow us on your Instagram" target="_blank"><span>Follow us on your Instagram</span></a>
</div>
<div style="clear:both"></div>
</div> 
<dl>
<dd>
<span>contact us</span>
<p>Customer &amp; Tech Support :<br>(888) 900-5180<br><a href="/cdn-cgi/l/email-protection#4d39282e253e383d3d223f390d2e342f283f3d223a283f3d2e632e2220" title="send email to our techsupport"><span class="__cf_email__" data-cfemail="4d39282e253e383d3d223f390d2e342f283f3d223a283f3d2e632e2220">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></p>
<p>Sales Department :<br>(800) 707-0393<br>Fax: (626) 813-3810<br><a href="/cdn-cgi/l/email-protection#3d5e445f584f4d524a584f7d5e445f584f4d524a584f4d5e135e5250" title="send email to our sales department"><span class="__cf_email__" data-cfemail="b2d1cbd0d7c0c2ddc5d7c0f2d1cbd0d7c0c2ddc5d7c0c2d19cd1dddf">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></p>
<p>RMA Status :<br><a href="/cdn-cgi/l/email-protection#99ebf4f8b7eaedf8edecead9fae0fbfcebe9f6eefcebe9fab7faf6f4" title="send email to our RMA"><span class="__cf_email__" data-cfemail="146679753a67607560616754776d767166647b63716664773a777b79">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script></a></p>
</dd>
<dd>
<span>System Categories</span>
<ul>
<li><a href="/LandingPages/ExclusiveSeries/" title="CyberpowerPC Exclusive Series">Exclusive Series</a></li>
<li><a href="/category/configurator/" title="CyberpowerPC Configurators">Configurators</a></li>
<li><a href="/category/gaming-pcs/" title="CyberpowerPC Intel PCs">Intel PCs</a></li>
<li><a href="/category/gaming-pcs/" title="CyberpowerPC AMD PCs">AMD PCs</a></li>
<li><a href="/category/notebook/" title="CyberpowerPC Gaming Laptops">Gaming Laptops</a></li>
<li><a href="/LandingPages/syber/" title="Syber Gaming">Syber Gaming</a></li>
<li><a href="/LandingPages/MegaSpecial/" title="CyberpowerPC Weekly Special">Weekly Special</a></li>
<li><a href="/category/same_day_shipping/" title="CyberpowerPC Same Day Shipping PCs">Same Day Shipping PCs</a></li>
</ul>
</dd>
<dd>
<span>Company</span>
<ul>
<li><a href="/company/about_us.aspx">About us</a></li>
<li><a href="/company/contact.aspx">Contact us</a></li>
<li><a href="/company/mailinglist.aspx" title="Subscribe to our Mailing List">Mailing List</a></li>
<li><a href="/awards">Awards</a></li>
<li><a href="/company/testimonials.aspx">Testimonials</a></li>
<li><a href="/company/jobs.aspx">Jobs</a></li>
<li><a href="/company/recycling.aspx">Recycling</a></li>
<li><a href="/company/sitemap.aspx">Site map</a></li>
</ul>
</dd>
<dd>
<span>Support</span>
<ul>
<li><a href="/support/">Technical Service</a></li>
<li><a href="/support/">Driver Download</a></li>
<li><a href="/faq/">FAQ</a></li>
<li><a href="/forum/">Customer Forum</a></li>
<li><a href="/orderstatus/#shippingfee">Shipping Fee</a></li>
<li><a href="/company/privacy.aspx">Privacy Policy</a></li>
<li><a href="/company/warranty.aspx">Warranty Info</a></li>
<li><a href="/company/extended-service-plan.aspx">Extended Service Plan</a></li>
<li><a href="/orderstatus/">Order Status</a></li>
</ul>
</dd>
<dd>
<span>forum login</span>
<form action="https://www.cyberpowerpc.com/forum/login_user.asp" method="post" name="LoginForum">
<input type="hidden" value="1" id="NS" name="NS"/>
<input type="text" placeholder="Your forum ID" name="name" class="input_txt_dark"/>
<input type="password" placeholder="Your password" class="input_txt_dark"/>
<button name="ForumEnterBtn" type="submit" class="btn btn_yellow">Login</button>
<a href="/forum/registration_rules.asp?FID=0">Register</a>
</form>
</dd>
</dl>
<div style="clear:both"> </div>
<div class="copyright">
<p>
<strong>&copy; 1998 - 2016 CYBERPOWERPC All Rights Reserved.</strong>
</p>
<p>
All images appearing on this website are copyright CyberPowerPC. Any unauthorized use of its logos and other graphics is forbidden. Prices and specifications are subject to change without notice. CYBERPOWERPC IS NOT RESPONSIBLE FOR ANY TYPO, PHOTOGRAPH, OR PROGRAM ERRORS, AND RESERVES THE RIGHT TO CANCEL ANY INCORRECT ORDERS.
<br/>
<span>System pictures shown with optional accessories.</span>
</p>
</div>
<div style="clear:both"> </div>
</div> 
</div>  
<div id="countryRedirectBG"></div>
<div id="countryRedirect">
<div id="countryRedirectInner">
<div>
<div id="countryRedirectImage"></div>
<div class="countryRedirectTextHello">Hello</div>
<div class="countryRedirectText" style="top: 8%">
Our systems indicate you are located in <span id="countryRedirect_YourCountry">&nbsp;</span>.
</div>
<div class="countryRedirectText" style="top: 22%">
CyberpowerPC can ship to UK via our UK website.<br/>Would you like to access that region's site?
</div>
<div class="countryRedirectText countryRedirectTextLink" style="top: 45%" onclick="return countryRedirect_ToUK();">
Visit UK Site
</div>
<div class="countryRedirectText countryRedirectTextLink" style="top: 60%" onclick="return countryRedirect_Close();">
Continue visit USA site
</div>
<div class="countryRedirectTextClose" onclick="return countryRedirect_Close();">X</div>
</div>
</div>
</div>
<script>
		countryRedirect_ToUK = function() {
			window.location.href='http://www.cyberpowersystem.co.uk';
		};
		countryRedirect_Close = function() {
			jQuery('#countryRedirectBG').hide();
			jQuery('#countryRedirect').hide();
			return false;
		};
        $(function () {
            cp_nav.init();

            var banner = $("#banners");
            $(window).resize(function (e) {
                var height = $(window).height() - $("#header").height();
                var width = $(window).width();

                /*if (width > 730 && height < 600) {
                height = 600;
                } else if (width <= 380) {
                height = 160;
                } else if (width < 730) {
                height = $(".bigbanner:first").find("a:first > img").height();
                }
                banner.css("height", height);

                if (width <= 380) {
                    var mobilemenu = $("#h_mobile_list");
                    mobilemenu.append($("#h_shortcuts"));
                    mobilemenu.append($("#h_sup-nav"));
                } else {
                    $("#h_brand").after($("#h_shortcuts"));
                    $("#h_menu_wrapper").prepend($("#h_sup-nav"));
                }*/
            });

            $(document).resize();
		
			//countryRedirect_ToUK
			if (cp_cookie.getCookie('countryRedirect')) {
			//if (false) {
				//no need to notice this again.
			} else {
				new cp_image_pre_loader().addImages(['/ti/VisitUK.jpg'], function() {
					jQuery('#countryRedirectImage').append('<img src="/ti/VisitUK.jpg" style="width:100%" />');
					jQuery.ajax({
						url: "/ip/default.aspx", type: "POST", cache: false, data: {c:'c'}, dataType: "text", async: false,
						success: function (s) {
						  if (s != '1') {
							jQuery('#countryRedirect_YourCountry').html(s);
							jQuery('#countryRedirectBG').show();
							jQuery('#countryRedirect').show();
							cp_cookie.setCookie('countryRedirect', 'abc')
						  }
						}
					});
				});
			}

			/*
            $("#h_mobile_list_icon").click(function (e) {
                $("#h_mobile_listc").css("left", 0);
            });
            $(".h_mobile_list_close").click(function (e) {
                $("#h_mobile_listc").css("left", "calc(100% * -1 + 100px)");
            });*/
        });
    </script>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0046/8974.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
 
<script type="text/javascript" language="javascript">
    document.write(unescape("%3Cscript type='text/javascript' src='//hc2.humanclick.com/hc/49044919/x.js?cmd=file&file=chatScript3&site=49044919&category=en;corporate;1'%3E%3C/script%3E"));
</script>
 
 
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1066522306;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "xRZYCKaC2QIQwq3H_AM";
var google_conversion_value = 0;
document.write(unescape("%3Cscript type='text/javascript' src='//www.googleadservices.com/pagead/conversion.js'%3E%3C/script%3E"));
/* ]]> */
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1066522306/?label=xRZYCKaC2QIQwq3H_AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-93500-1', 'auto');
  ga('send', 'pageview');

</script>
 
<script type="text/javascript">
    _atrk_opts = { atrk_acct: "Ihkqi1aUS/00M4", domain: "cyberpowerpc.com", dynamic: true };
    (function () { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Ihkqi1aUS/00M4" style="display:none" height="1" width="1" alt=""/></noscript>
 
 
<script type="text/javascript">
jQuery(function(){
	jQuery.getJSON("/tools/soldout.aspx", function(soldout){
	    var oSoldOut = soldout; //{sysKey: sysID, sysKey2: sysID2, ...}
	    function checkSoldOut(id_or_key) {
	        for (var i in oSoldOut) {
	            if (id_or_key == i || id_or_key == oSoldOut[i]) {
	                return true;
	            }
	        }
	        return false;
	    }
	    
		//category/landingpage listinfo sec:
	    jQuery('a[href^="/system"] > img, div[href^="/system"] > img').each(function(){
	        var jqImg = jQuery(this);
	        var jqLink = jqImg.parent();
	        var systemKey = jqLink.attr('href').replace('/system/', '').replace('/', '');
	        
	        if (checkSoldOut(systemKey)) {
	            jqLink.css({'position': 'relative','display':'inline-block'});
	            var width = jqImg.attr('src').indexOf('220') > 0 ? 220 : 200;
	            var left = 10;
	            jqLink.append('<img src="/ti/soldout_220.png" style="margin:0 auto;position:absolute;display:block;top:0;z-index: 999;width:90%;max-width:' + width + 'px;left:' + left + 'px;" />');
	        }
	    });
	    
		//menu panel:
	    jQuery('a[href^="/system"] > div > img').each(function(){
	        var jqImg = jQuery(this);
	        var jqLink = jqImg.parent().parent();
	        var systemKey = jqLink.attr('href').replace('/system/', '').replace('/', '');
	        
	        if (checkSoldOut(systemKey)) {
	            jqLink.css({'position': 'relative','display':'inline-block'});
	            var width = 200;
	            var left = 10;
	            jqLink.append('<img src="/ti/soldout_220.png" style="margin:0 auto;position:absolute;display:block;top:0;z-index: 999;width:90%;max-width:' + width + 'px;left:' + left + 'px;" />');
	        }
	    });
	});
});
</script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TTMJSC" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TTMJSC');</script>
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5090495"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5090495&Ver=2" height="0" width="0" style="display:none; visibility: hidden;"/></noscript>
<script type="text/javascript">/* <![CDATA[ */(function(d,s,a,i,j,r,l,m){try{l=d.getElementsByTagName('a');t=d.createElement('textarea');for(i=0;l.length-i;i++){try{a=l[i].href;s=a.indexOf('/cdn-cgi/l/email-protection');m=a.length;if(a&&s>-1&&m>28){j=28+s;s='';if(j<m){r='0x'+a.substr(j,2)|0;for(j+=2;j<m&&a.charAt(j)!='X';j+=2)s+='%'+('0'+('0x'+a.substr(j,2)^r).toString(16)).slice(-2);j++;s=decodeURIComponent(s)+a.substr(j,m-j)}t.innerHTML=s.replace(/</g,'&lt;').replace(/>/g,'&gt;');l[i].href='mailto:'+t.value}}catch(e){}}}catch(e){}})(document);/* ]]> */</script></body>
</html>
