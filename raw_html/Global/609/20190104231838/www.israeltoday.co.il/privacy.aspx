<!DOCTYPE html>
<html  lang="en-US">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=542" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Hi9Shz57coQTBTuNsFpfcM3L/TKaH3e2+TznrHEgW8ByuG9QDTzZWVrCQVwXlpybY6lvD1snDwn3EvEIqgMMNMUSrLohdqcwgTuAMoHr3TdecCv9ivLDQNaSm0M9zXAQxTnfb9mw8RUhr5gGu7QL8YqI0q5VMTpoSQjoq+0BVDj3FYG+OA2AYaD6ST+Ts8sDFunwXcHT5JkpwGR3N5XpXqxj1gQ++AU/0eqws2BJp5n/Nk8dKOUwlAERbC7v3rC0RZrznDYOFHaQHngBqCxXwbACFtPQV/cnmuBYMgf+zPOIPzC+/UpJsQJqY4i966MfUlmLy30rKbjKZFTh4w2u30eHdmc+m1V074K+aCnXFCs3t3Y0XIJq9jxjo+4l9MXJO4ZxEly29Y2sBkpq1c+fcFTM2gO0GqMAEyF8xzK2DFdKCAeG4pgmevQI4cZcGPjjryxnxq7sSWvirGrZCcKSbODV3EauFtd7iIseRXfeRqPh5Z56D2mDE0byb91kf/qoS/ZVHDYVPUcYpvNNBjQUrmN/SeDO8O/EnGRdMDWBOQyWTSj0ISuAs75ZF3O+1+jgkw9QWorMBnO6JqRs+hrDeLnmdRsAmKmcv7rvLp34aL7HlK8pBkMo13JQcILpYN9f71etvt2ryDrVn0ukTLvwmloDqhSog/exbxoHZq+ftO/MH+iFPpefBYBpofpMpeTIiMA4O2zmSCuDZXLSBFPSeH9hWHveZY9M6IBWcpSbCRCDkuhB93QdI47r21EzcaTmA091Z7x9WfxQI86TmjHFrS4MbcUn3Ghyq9VP9hYBOXGGY/iyKHR/Tq8aecRYnyzTKimAEepN+pFlT4QtxotNOS6bi585U1X8Rrb83ouRWdd36OXUBW1p+Xb4k1EYLp/QJssQJ1kAbe1z4HJF4Qc8cRw2++BDjWKoQTFKMHYcdNfvfBdKvD+NFw1krJs5x68PNHimZW7c67D1po9412dUF1Ig69jWBAP7F8zizgqH9GruSz8FOnl+9zu4dyu1bXjqGqUGbIq2c3rq/OyjLP760wh6646QvKBcMwIcslxq6lMThde4QrzHcp4mWXpkVg5zgZb1GT+YbmX+UstC5j+qs53l+3LZ9sCrzNu79axqbUL5CJdJT78geq16Ngrd8ATZF0fEULrFj4yuEQ+mgaamJCQa46sJ5+zV/0wpKC8GdS1pW4AOvnaI7AIWkine/pDKdW2FjSIs1Vi0eGwTqpK5uNcMUNxSvrtgsGczARoppjhRJymdE5sK672tjQm5KFz+IKidFtzTRJ7IlnuJ1Bgk5Hd7CxKYZ3VuiIa3prSpr5/kf6ySgohb1++MgUlna9hO8avT4jNHRsuCTmAdD5nUvyZejcTQM2k78TzjhdQffq2Q87GbCvQaGWjtQluEr4JwiIvbyqVW75DAdpwWHOyqP0qciEvl4VgVVoyXqzuIXbBtOOU5mXfaXf8Vd645XelzKsZHx2gOPRw+osnaA0mQXbfH9g2rZIB1z3/vfOtFt+0lf7QLpCMXmLmV1wN/PZirqjVLfGYSIkfQEr9YWwME20mA241GVZkjFTPW/nsELFSgbXa7uSSlaJmRNzI/CMIDa/DSFnLw9mXpcrBydu6CDxbR3GmkAw1Dn4LB1W4UbwLl31YjiOL4TIB8a2gmy/mejAZUEu0Zow/7X+yoeJwnu2nqXDHdU1WEdLEGPXff+TtsnFpNIrbGt4c59p5cN17g0a6Mh+MKGI1gdoWcO4/xo5qfY5f96QGUdXWUnGpJYpZx3WuC/Ivc48rOc3kRJlkiimSp83htQMmCO3r30G/AnIawm6QmN9h3grtmN5uQ7Xv5HwyyCo5kI0EJ8S6Gam7UvNc8LFb/eXXreE6lit5bCW9r0rEnWk/yh2c+0/3jVh3guYemE51s+8ykl3wOteVlMauu8Nv7lrbmw3QEh8ZcPs2ZwQ0kt4XE76tkR760dDYNzhiDa1c29o1whT+etH9s6b0hyLLjI7W9eucEI8XKvqrUVFhu/KyUBcGAxvA3TkVma6vH5wIZlSvPZVZcnZJV5PI70GxDNryhmyzvjmGtzZSxB8uAuoKqxaLzIIQwOWH56RYndjrtyX6u3lGzf6y+mDLT/mpRZ6a1bDqhW2dFoMQ1OOz+mdZcD+DIfxz65g7RNVddzbEW6B4mYhcIKT78+m2HEnb+PHRUNCYGNE6khJwf1QavhVVkyHZ9wFMy61BZd6T4EYQcsM2APNpqG93m+WJg+vkaAAx7oxFMQGW79mYnH1/kTu8OdoOnvmBtfUvgvSbbWLU1YmwDCWlVNHgr9hcioZMs7suzI4R0nHPd/XJGcby7VkrYF/qo3aUyP5uRPwevjfBEho2t4FOR1pa7uvP730DDTnb392WvY82auoB5QEprVLkFXj3N6cLq3rOQAMYwCWTo3d+wK9aPaqQV8cWN1h0oXLglTMbxU9GO8eE6zUvpgHNQU0n8OD4PUuEoiZXd5uLG68kCSsSNWVASEUGQgNLllfIyzO5L65lu0jJ4a6zcBxoyo/ixV1+V4FyHOsub/RdedWLZHq3J1bfw/nSJnbNfItEtPDbW3ZTW4ppwJS5mb+IJPfiL8HcrWNRlpU21QkA4fU9DM+OQbbzpzrjMPQua0lkXFJBjTN+vOqvGATBftplobMGtC6zAVGTsCI9OLnm9hVA0iC3PZUYAlZaK0bmMm8LBso0wop2735Ttq9PxFMxucGK9dVO1s9mN81k3kXjoVSVcok441sSnPtCY2IDvyZnWoQdyjAw9x9wyxfCxrgZ0silgqSlTzhaRQvcD/5vrk1YcZdq2ZfCSz75ZV4pbuo7XXzttVKsLhfUOiEzW660LFJJtOhK6W1PmvHHBTCNqJ16Al1ElGSFNF4PKW/QwZ9o2UZD5mICK3f6DvCcOw+Ck+5tWSKOBlHRUZpAHw7Wg8mxs2rt3yOl3vEsdaoI1kHgZyFm0AcdxPZ2oc02xn6CIuCkqF53s0VrTjRa7JmF732wnPPMg+8uTaeGXp8kCbK2JJ4lRC2YJG+vZoQDnFwUriELXOTkA4zFE4AHgN2gH+zNqRjYcOB82ADtRyHiYV4WsmxxzxhbFeAeWiVSJZyOUgD66J8AZtKoS3SvSVF5k+GpfDCF+QY6Jm/WJpKkkHaqwq6ycT8a2e2MgYsPlv7pBmWqZons0zHNkdSC0+UTKxdw/i5/HvtKOI7SMr9QVhniRXylv2vxfz4wOxOVd8W1crVe4W7YbNDPSG3HxDyM5L5u/9L5owv6VP3JwDJUgAjEJEZrjkDfQfScMe3A0+WAar+4GRrV947/GoonCWXxsQe3W1kU416I24b9IqHalBK3yZLLmb+Hx8Y9ZPD2QbAs+lmLoo4OjO1gi7Fq1cYfrFlATczc6fMT7PUar+5fhhdP7BNxkL9KZgZDm3XcOPPJEpA3Gk1dhAyjGXp8qxzRhkvlIxkkMXd3I0QzxRmShPNW2UeRj2rknoUIKtFTVgypIZtyx2FUWCvefBnwlUGDTllw9w5tp6w4hph/6zWJmdIEZQ6nkglQff3bDSEGUotbQ6ZTcDhRYnCq5XkQu+YiI8xL8+VY/2bD5UMOOVQEj13kQhqsxNrx5iKq0GmwUcZxWZYLa4s2XamNiWUsNNjrGFJrUKQ+GG/aX5eHgii5+tmLrFt2yPvCA8F2+kg0rUyg2o8lDgKMTIT8dvikZqc62beWBBEr/qooq/A71qxjpvUOes7TG57YGxYMAh4ZQ3s/rSR0oAzVeL3A7VKZJ8o3RAwTNttVuoxqCmfHzvAaZ+MCoYjo8SuQlX9p/JcE5bCPB+emIfCYfetmDzmKAxoDdpfm4s62s2lMGTd6w6OTsK0Uz2rLp9HDnpJ6s+qyWhC94KLn2rD9qoPRpnX4y62yUAmJUKGUEALca/tcgPzoX99nlw6yJkS0rARPTyOE/gL1Pdr+GKmfN2eL2+UNVPOjK2WHtYI+VPc/2GXce1gxHPVwbFYkvjCaAAb0M2CGZaZv5IHjvKMvgBWfAIQhDqWFzMEq6FLgwtI6vZVqpTubHAeoVT5HjQMQA6sJtmhdI0CVo3f++A87ug/96G+x6VpCn5vUVyMtanF4ORuyfnUzIpcXfP3WDJY8hsTLFfJ2EZ9cGm09kI9JkeV6cUbrj5SFU/3uuKDw9o75bjU5zMYUiuNLqR1uzxDv3qrE3VXkL4Vd6vYykTUP0Xg9JLtfNjN2RZvPYsZgxeERp9S1K1MRts+qSb0agtl9h1jMcu+jNVrC7pazu3G0BoqVBQELPAwX9OPOT7ngqH29Y9unhQujOkMKyCBSVV9r+qMIJ8za9bpT0Lc3wMnrcB2BvrknxXeTAU1cB5onPH58hKhDydiOhGi61O800SWm7qOtAjswDqBuvDCodXbR4TqUHyGJUVDKoxC3wRf/Z/AFuM31cJUFjRLMIp8khtPwQdsthVkV1nqtKJj0+Heje+4CLZVRvyUK20WEiWtkMMmRpjZJC9Tl9wCpJX7g5z/NIyfpo7o/exwQaT56SBCd3QIBhnBtNp1UBQ2254RtfGyHJ6mXN+pdhAptUKBFSMdXGwFB+79ibfJl0+eWBnaF2FfATecKWajGT08ei+1D/ZYUccVByt5zm3CO9v83eSQeeKuo+CDktbogmg8Wr9C64n5PN0DZDLD5xRzHz9GB6xLtKy0rEhbvI8EIa3UFECEbizwBlXktwgYSjLOMOC5zgAwSPbwn8WgexFHz6UNG7e0Cr8rGsms+DSRDv6VSeEotqDGvIyQz4i0aRq0xkpQWq4D6FDHNl07icc/YYTPP116OokHalRt4iSLJiaTJPIWsNrE+sb8OltiRjF+14d8Vcf2YPXUmcPzN2FOENmNCck0PN2SgjcvjnAIUAYfwNApRA2i8JBh2+Gs3OkBJ96JFvVJoBylM9xLqxJSa5uir4ZX7ytnsX09zJOf1PBRuPQHQHUveDmzn+KmURVSnhlloTv9Xl/DozRxbpg0kLl+z1hAABl7lWGomzQO3aUkITa0IoobShelMGbHYyKr0taJ38R0RlMWxuHSdOrmldDgcWMi15O2lP8h7PMYOhVgk9Wrr/BdZTTWmlqYiLTPgtho9iD7M3W+6EE506oRyzGA8amL59RXOS2r9Cdji9o/lGhoJoah/hc8m4lqUKFWsgN78FgKwm8YaDMis5DWILULtSigVZ6l167eS9TNQjAiENGpTPV/kP1gB1AHps3BFp5bW6KgcoB/oMyF1iSbT2QgWg9T41TaTasqAcbS0se/pdFfqZ/5DIU15Hj6WekcZrtT/+df+aX0Nh0eCSOIPvLEvSkFCoPBsdrI1zS4qV1+w7eGtLEsVNF4xFV09FMBd7MZJ6Jd8GRrH5m76f0StXGdzkl3cqYFicK7AVw42jP2QtemkSlE8y1iFXwgsqxFba7msnap41puc6rhWlQ5FPof+vwQWOWARaiFYkQVsH2TH/I7iaoS1AwbV9jIhaz2ZpmDtQFz8ztnJvJppL/Es1PFh57dzk0px4If5sCQHhktvAHYjFrYPZJJW7sYUAH3uj3rnTHUcYwnf2kiY2XcZkplJA8ILSy4oiFnNBy7sV+wJQ9diNIoDnaUttR5yuaPMIWYpp6sbUbnJYo6ePgNhF1+ZAHavT8lgfNTW/4tvAUeNMbZksBQdoNJ9vdYiahI4i4u+xZcugnT+1YNf7WN21OLP3KpIwUnamLC4hi5jvFxkFI5GcS/q4oshTBDJz2I8yk2IV/kUaIfwvIke5LS/0bSUkVzpS4dbfPN7kRX/x3msB64p0mKfW+3xd5aUlxVuOV3w3sZBR7rVv30VambouVuuEPuCdsxk1eA6H0ZiYU64SS1VqrGHh5y7oRPdw9OzarQoRq5zkiJoCLKR/ooEa1Bmo3KXrFbJhyjh2yAmiqr/6NaBHnYOG1dzdus7a7bX/qmn6BhUofrAW3K/Aol7G4ycYrPpVJmpiXYNLh+Z0gShbKd9TPJNo52JQA8MK8bseUJXU0EOcnHd2Z0rL7lijWMVaS5OBDjX4yt/5bnMPMwfaYlAgxBHwdpMFxNlZJTSF+zU9vh3z2L48jJoX5OjRKEBsc/ebWN/MRELkeKAEzZJoQ3CKRXl0sV2H7k6svZS3ipSuV27o1OjxZwLnczkl8pqoX+YgkW4aNhvWQt78kePMArvaCGDn30FEoFglXJDXWNWyxIwQ3XcFOeqH1XdJ7ql43kocv1ntiJw68j+bVxYOCBRikd1eis5IOILK3B4MZYVihJol3477r8OhmXMy3akQF3CC9fqOU/6v3GhLZHz8ikFbA1B+vgbyeUMtngomQ/o9oqVt5kSzzFdB1pxD9Fh5IL1FdVilSWeBy5Hs2hLeWwUaOJ3St1VOHzC7S+VpXDzplcdxxPhCHi1LV092ir0pr0OSPQazWl4aj4zPgG9ZyFsimGtKhCeGWWZ3lIxiB8q6H0FDfS4lLGtDYC3IgROdIyMug5fI/2PeBFvZWtzwdUA4nei51BzLmK0RYPtoiUafuNAapuoXlaxRQFAjN256TfjBgiKcK92xHAYpX/Kmb5RcSVqPfScDH8YCTHD9gUYLmwgJOohwidwkkXC3+aG83m7+5Y9x/cnijT33JQygLRKAsNhYb86Et9y3n25As7GPOQu2Xn9DuIz/pYcyJVJ49t/0q8o+GdatifnNQpkQ+KC0g/PoiMRap+YBr/jXcrQLnwGUVT4xhZCgGKobGZNpXkJO4Fsy6uDZXg5QFuPYRVja16rBndj59QQbzpuuexDpbaxteykljE4+VEW37DmQ/B22gIfqEmt3feZOWDvJrvRnOAT7Eb1fvIM6zTXfiz+L8Rj63kbvlkp34YSqGOWFPHXoPObC2m0nTouniy7L96PDciUp/bhg6yfPTdMtzE40CmkidHc1zsNWIaeMe/molrETvfOUV0YL4z1+s482PrWQxMQZzYc/yHRvYKZVfj/m8TxzgLVtuLpv7nbMqX9n/vwp594O/rU4lLNzPBK6/4MTWdc7dmG6e/B2MxsMhZKsp72LUJJXCW23iOpXdXLGyQB6A/1ZQdGT/72TxGD17UkD4xb4CGeJMS6ZvWsHcWUzk1xT7DEstr84qyWqvQ9I9vMuXnX3aTT+1Qc/nFU9CC8A+576s31z6mq4k6V/e+FLtF/2pOqvEYjSMZT/KIn7UOSu/vNLjfroHxJ6OSF9UkmznVcUCqgKQZs79QgOIYqYreR62PHTWPwYasE2YTH1VzIOWkD9Q0+cUileJv5kSiWSIsH3sAOn5/bVQyhEDcrd9YBz6X6PnGgIvbaeHZE1hf3cnRKYntZh6r4Tb+XQ1qWtFCBa/zupr2M0WRn9ZyvQcK9oGE1124NqsaSqq23kmiQiAHfBbmVhr4n4YM077WjL2PtYQaNzssETG9TGcALSub9ShgqOamXcWjGZaPasxgjRKt0B+yiWvOl8yWHzcYYFKDfn6DsQguIpSc9RF32JZw9Dh7ZJgwKQ0Lniirk/C8RGFFickJFGrzs74JHLRay5c9FBsqXLM8TIO1MMn5a3SrDKiIY0I0zy64zc2E6973RaQfLA8O3vIA/H08+j3xJFWGg8y5VS/hiYkaw93e5RnM7FjywfpGUYptcgq7xvvGwYVm6hi57pDi6z+yWsEne6yTzMOZPkOegIt45VD68WHYiCL0rw4yKvf3uJcQiwmaBFZI6sLaV2/WqQ2HQkPXwsJZGNpxDlz8McB/dtnBNGaclX7CReA4xKhN7/fKjkffP2cXBzD6v5NkGdYEWmILyG1XuEFgV6XcvQOa5hRpVUMRdKZFP0MKaQMv/F0/xAnTcTUXR9mskg4yg6RVq0NjMwAtoDLwt7qBWM9Wf7ydS39/eIELSMVXpttxpKcEk6DeNzeQl6TrGh9CxgKWU+uaCS94ninjwpFgfN5eDK1szBMBOJ/y4UzPrvX6DCRntqUfeyG5Ve92F1V0A3kUNFjXv3oWC9EiNM/LMrOBb3UQySjl4YwOteIcus2NMe8ERCe+ETAExPUBO0nP7RaURPD7RqysGefdzfKuE8VOeREiej4i/8VvfM44RK1FY2qhv7Cy8O8FbIHexl0p69YR16//c6RKIO/VUtMdz3JY0yQRG6fL8lr8m2/BiZgaKgtc1AH5fh/9uLS2a8FbS1H5L5H02x3XY6aENauJEipolfKu7aN0/48FyphElft09c3gRk+65fugUSzEw8qE0mnXw0ECPGT6BnFZoX7+88ZVRV+ZO7rifapUbp+IDJt3ZpgydprpxzwHG1KuY5tETH838cmzJL8thJqZiPm1NoYL5osrxipHGrJGr+lwwfrQL4IUnehATMblAqjYTlRAg43RGW0+rlTFdy06dE/36SoOFmynJP364ImDYrs3ZFX/1sDknJYBEJWtVplrn7xFa0eCRm2mqW5goxOpDFknkRBCvym/IubfoDNfNFxRcJqEyMTasH3gAViAq26wgj+jvynuTN0+j21xVQxk7HuhNOZYdb4W9CLk2pR4EuEFolfo6gGOZFw1rv3dibwS+h+aeLJ/C9Xrrbi5BdNZ4jUH+SvciU2m0aVs7c+9KfZyNU51AAKulY13YODpmQHgLFfgUTJZN06Pk8F1eQqdzQ5Anm1YQT+u/p+G3yK78plgW9IepRfmX3JA7mwT676V+WuSUp/bfnl0nY0mnDu0Z1LpWwxQ9bd7L14ebStOUEhpGCvqx87YjappHILq8frQN+hmtiznWWTNcPuRcs1rah/mOoMRsuwxnOnwRShwGhe1s/xkz93YcNS172l4y5aI3UZe7yIH3pZc5/bjV7Jfxx+c8A1za+sSi+ef7iM7qVqM8VhW6lh2Q26PL8C31c3CxOGeup27nwsQflqAV8PGlK9hXXUCa1VX6EMJPZp92JWUiXCaNfKGjNcRBMQ0jcp5tTc9ORpTjaK/xvFjCjpbryvgsIZ6nDqbO5IMENT9kcxY2RJQaUZbh5IqQUoObfvTaKdfi6JFfrIjRco5wmNppqd0itoxciLnYcX0EKdS8FOOkKmDh9VdnHbbqnR1N47HT5X/hTglFUWFw+CF6UpXHeYNgxPtVW7ueQdTQojP69ggtacPFvGTch3ZtK06QZcuzcNmunf95XpmUw0hcnbiUGYWcGxt6UO0BUSBPfeVOnTfEg//sKlYvqUYcBwwUsiSzwDX1RcrOGMZ7D47qAaNZTLuY4QnQFkUeMXRKZQ7aZ/BW19hRNUMpZSCaCq5k317TKeLqypwA5v2pF+qlY9N4vCLNZKla8+D/mJjkeZjvawi46q6suy74SmkoLHBtAY9C7ZF/SV27/I9q7Bv2tTGV7pLA5Y/UsEW2f99VntOX6ZH097YaMtphBcWFBqsfFO4rEH8Feu7L1wWuJTq6YMKTwG+qkT1mIeVnX0sS1Jr0XQ9JMz6uA9/JUcpDWB0FQyXfg6MXxiPurwLS0KJcXNepZamUTa0DN91UmfHyYE5M5wkGAL4HhD4OvDP+vyK4ItIMzANPYLvdg9HdEuKnRUc7c36y2Man5bdOZbrfqVImVVmDXghGRkq5IN1LUyn2w4DDGsfD9tI2ZgY7b32ak4sayCjjEOS0/wFpcxq67cU6xUhMm7JNdi3agf4iIAaRN0VMfphQZ4eT1NR76bgE31yfzWuDZiNIPnL/zC+79jkXVL/4E7hnWeFIOafFw2YGacQ4ug9VkP+lAhJ9SMMANGWdzvKd9UwBwu0dM9LDsjWW6qQMoflOKaqBofXmyPGJbbqbr1Yx3j1NSNFhnbf+8VHG6XVCsTIeyAzfqYpuDkJ/F2oxYeMgHRuHdXmHOZa4gXX+WkC46z8chxDFjZlcxMX/8OIj9GVVtfNJ+AV6POcmpI5D90+L46rtNhUVCKUJcJaWH5VrFo6351cpUtkM1suzDSOgPuw/5J4szhxTaSNrNmOQBLpyPVb8L1OGgq4siU7MN2LlnNyLK9VGkou6NuE9ApFqVlbauTs4vyyMf4wnyqN5B1BQ4pcqZtZmr0LVP+H7K1oJRGZlukzSemLSs796hv0rxNe5rwr5mhHrQvqTpmKLfdQBwi0ZehwWrn+H5AdIHKMeJrtYhLW+8TbIeouUwRr+gZmb/bwBaQtLTDWgfSgVvPtP+kGFIMnnh4oouT9oZpuz0wxRUxr2kqAl91HYBddeypROuMnCwO1VDABlvrmEsNuTZHozF6hk58QXmw5O7zntNCs+mrbbxpdWGtkAVgNQhGcudUE8zH+G/kB2gCZDl6qbr/7z9SE6Zz4U0uyCTUxu7/A3YKstm6zafOKSo58RB3ctS0V2y9PuAOx74wYeLN4slIdAFQVmaYQn2qJAAhmbPy1E+DE76yI1U5ekW7MYBHsB4Nmb6VoXa3492PJuUu4vzuOwmuNDiDasfNBXeAZQxOJXylZDDDYC5bQ4kPfMjrcr3rb0TBNs40+wtoVZTz8zU+zN5lsjDHoVFIejTUh2k7cCfDoSQCtz8oLUgQTn14KxVXMtg59MKcbfrxMvpLFD/Cl84t6uetacm0pGP+/YDtqlJRNz0hLGJNEyXUnnatIz0C1iu0E7jZMqRJ/f2uzbL2gVYfXIGkIWfgCQ8LiaS/0kmq5lm7MpamNYiny/5NJZJ5hV/jZz08a1plY/2mT6HviKCFl8Ty6ude/hyU6yrX2y1dRhYLz+qtzNoAEUOIHEMwWubgnATbtv20ToOkdxoL1Lby6pdoDEsSi+OleD3/dmKUZzRXwqhD5id8iEGATNyKA8HyKgfncUUJMpGEj1xjw/rn9WBSPTHfXyrr0Yt8xWUyN4Fcy8ausJXPefgwSmk7aI1wt0fMo2ooJU5nKcnrK6swWUtVOUyC30G+whuF2/nWB9dm0fOM0Uccdh4kYvVZF7Iqv5OCiKTQEN388sGKZCirbogEPkUOamxvfYJ9AuvFqYk5ksU/COUk8Gnv96nrxEg+F+Er7cDxe9cG1iSkLc2cGOpv/SaPTApgklI0IQ0PSFld/M4lM/c6W6iM+IdLjU3Oid2Bs9P3jJ+AMNovvG/U4jKD6KUEKoPaMPWvoej5axdaTsDmHQhlzmQWMbiqG4W1u4HWPMcb5RxtwethL4w0nbRc9lkASOWuyq4W1KsVUKBex/1OZZ/cpLbjKZ0Ff5mzr2w6x5C64VCJmY5BnvxfYnKutW6og2ALCcam+yxrgFVCb7qwq5+2swGKRRP/KOcvscFforiOSbpbz2b/yPg8zatUmemnlgwCKq6NVkD3B+j8t+cbs+3r2/ncNVOG3bqNtuFt1JN7WbCo9nVolllDtJieakAwswdVIoartq1FSq+tIt0HR8OFhlAwAv/iY7ZpLXPg7wjyGfw2ac9EUSpYjK3zlVPh760I5PAT/fl0jZdZle+/XQmRNxlDD00P499/l/EEKqsMyxFhvLZT5KsAbZaBH9rKsOA947g2i7PU1hRgmbjZ89yhT8rW9ry7xEnohCcYg42UmBFXPSsTGqOsG1lc04sErNN2Ab+CwEV" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV0AX49S4XPonzyGHDwZs-R_VWAKpvYG8_cXVgn-Lz---VSONzQ1&amp;t=636692082980000000" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=qph9tUZ6hGPMRv_P9VwIOe223LfVgEFllbRPRF1GclrFPGjUisPfO5Fl0w9cc4fIPZ3YzayVwId39zB2TSz0ppxHFLU1&amp;t=397b6a7" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=TvpD2YGOOsBHrEswZxEHEteBCJr-PW6mhkwAauBn9GUjoZ_L4qHkIePrBjHiSySgQeFgrSMv30LeZqoea_8GDSqL1RiaMiLSvG0v8g2&amp;t=397b6a7" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=542" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542" type="text/javascript"></script><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542" type="text/javascript"></script><script src="/js/dnncore.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542)-->













<!--CDF(Javascript|http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542)-->





<!--CDF(Javascript|http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542)-->



<!--CDF(Javascript|http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/MediaSupport.aspx" >Media Support</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/DaromAdom.aspx" >Darom Adom</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Superfood.aspx" >Superfood</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News site and governs data collection and usage.
        By using the Israel Today | Israel News site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply1@israeltoday.co.il">donotreply1@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2019 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    </form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=542)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=542)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542)--><!--CDF(Css|/Portals/_default/Skins/ITD2/skin.css?cdv=542)--><!--CDF(Javascript|/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542)--><!--CDF(Javascript|/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542)-->
    
</body>
</html>