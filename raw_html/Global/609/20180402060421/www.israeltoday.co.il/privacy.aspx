<!DOCTYPE html>
<html  lang="en-US">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=542" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="3f3wWVRQ97G64sfaCPPQ071GKlo64bpQB2J6eQI/c3f3xsebFPL+dsoVww2z54/vDAU1oVtYFpTNaFYfTEIgcIXPBgOlJZIuKbisYYcZTVqL87MEtB/sc1RJ3l7YSWfbrmi0F+3493EN+mOn0j0i4EDts/JHsKDw6T/NzICiTw0Ur4El24gW6n53uDny1y4GQLK95bt6bROQ1DJWcjN1OVRgGKQJwbE2cAruvbOAQBC5Sl2hMdFRMNv829og9+x6SjBjki4H4Ia5IgnCI6kFi0Xn1G7c+33fV3i6D995uhkgTgo00nsPPEHdZuaPFmV+lebrC6nTObBfRjSOOUiETcFzRg6B+EdiXP6hNqX1LTtplr02QCjco6leWZBmFTvJqCa67hLXR3oW0z+u9k3+m6KTs6sbFs83+B2JTYbMgdVfzp66ZnPMHMlt4XgwNzISsn3tdRA2KHepmI8cC0L0RrP8CyU7HIr4S/9cGnziLDtfqjbINI4scuUsXSuHElPiex8gyu/mvOe70kDwtsxyWessBz/6BS5sW4j5yKMKknQNGnNj+KUKX7uwhXB6fa73MuZ7vdQcsRrl9/nSMsSL9PpVkW7aAtqRvhLMgvS0HYoXAWX2G7mRnhIZMhV+2zjDrsWL6llImDAifqzt3db51ta8dfJf5ZgnH/Zu3x+mXkfuNtffv7BI+QjqCqlN3oI0ArNrsgfV/u6oDMyup6u6pT2zXTUjupA6Okz0YBkrzCk9a/ewCE5IvdTYRmBycjJx5u2zzlusxdcfvmwVwdqL52nOjk6ebZTyRIQIGlo+POsFbnvjoEMBndqtOdbJ02maoruxrEsS9FaLWKmOOJ2kvV0Kmgd9dUCAiMc6vdUlIuQYbUPYJ9j4fc9tsQ0X9RduLpNubUzhf2Lvf2O+qWgvB/ODZh9xAn9tJkHO3lvvV86dQlv7o9e+TBr8vhTKfIzxpG2Vh79J8XePe6QFRpPiOkZ9DiKG/fipJA7n3CeNd7PQpcFbRZCEK5goCFnt2cF4AKNx29SkU1AX6AjyW0ZlEvCbJIDV6etFNvcGiFGaKITs3+eRL2rvESY/pm+ctA+hfcphut5xOQG3jjaZwfIOCgPIGrXwjSvV/NgWhLa6bEq7sI//2wLNL6CUx+B4quZZkih/xCmKFek6//4MMn2NmhWrYXQifbsNNWj/Qk5srZMyeX6LsAmVmRKGh4o8Pz/3uGehS/R2/0N+om1LZnvZCTlywJ7hhi8i+5rCnNUcd0OCVH5XeNQkwABW1ReAEqH3o39S5BkNTfT2QxF+7QttefXf06ZqkU3M8ztuuOgv7vzA+f5jAjzbhUka8GmsMDNla97vG5AMHy23TD7Yv96HearROUAvAhNElLeywShsalQy4RGBTy6pfarniEbHVJ3vMFIn9MAuBv1PmcVHv/5d6imoU3zBzeuaV1oAIS96ZGTeWva97ZL3E4B2UHYg+FSHF3uXRasB8HV5YsmHSuQtdXwCd48gV1+AGLntehG5F09oP5C5ugRJnPBEatOaLmxnxGW5VMHpODiyZXDOJFR0gTn8Gu8fYhbkYFqD1nsI7Q5Mi0z44xVppwLevfwRBHa4Xe9tzARF8pmmCzLmeP2gtTKaYyRHdzb6KDEJsWmJVwWAWJS9R1ROpaeOnV0WkfX4KnH7qmMwfAznD7KSAw7/e1xmU+ZrJZxHqaH9lrN6ng7K9ENJb4diMvHrllW1R4NvFqIvOmDl8PCwe7P5Fr5pPvnHDpdCAm5Apt4qID++3/Kd7tTgQDRKAakalW7QeI3SgyTN1HNdEs0eiobupAxavns+9+iGQvw8WE+GJIwVFw8Nh71W5xaks7qnSoMx9Ffb4ZDztwaEAKTNVx5+EUj7lcpKXGwZ0oV3OO/Zyh85hWzsBAHwX5xeB19JboBO0+9g/u5cQfz3uI+7RBwlRp5K9ziWO5nsWczHSAa1MtcUWwEID05X/D65l3asWo0UW2KiizBLBil7P1ss0HnSz8eZ1agHJ7EHIfnXYX5RQGqZczBpxEnIW3jcuj3yfpkyna+Rq94AhTJQP9uODvQvVCIkJqTMjbc8VgIucqGPC6d0hA8D9sUmWrk6ZER2n7fDSC9HHcBJ3TOAYZPM4uJno4iO8TljZTZIYhSbV3CIobQZvGfMs6Ulk3yosV/slL2MyvwtwLjXFTlEssctv2nX1Gupgp7sf/xgISoYWkGwEgQWr00Uz9cQW5aSqXKWTgm63Vvd7FY5JyVkCKUcTyN5CFK50zpRBhmvFQvRh1mXH8cZpoKJCsrNLN+/9h1S5hVy2ieG+JCu5WPdlWCUkM4yIFN5p9DI1rwvWrS90O/VTfnC+a1XefeGcsKgA0QI7plqveo5lrmIGkQRpu6KJcUMt0PHXriVVO2V0R5oeaqjmmvqyTVzkAZalY+jLeK6RmvTwPAUCuYBahyE5eekI8p4FPaHFflDpkwEQfwVah4ql58Mv98neKlxZMn+Gzp1sJYx3BO11PFlzIWtHvEK3UOBdF4yJghh2AEvzwmyE336Da8PkItY0uCrqKkIbeNHZbdBseVvJiBeyFn26oLOpll1YNBs91aX5xHY3BLT+NvhabLddQcG1Em+SmXe0o4dw0KRr8BKZbkenPUU7KF0SlREBPVZr87REk5HDcI4V3VDlLDTZDQ84Gbrosc0bDeQ4NHluXEHSRn+fyFid8V4SKIJlahcXiF706sDU6MLa4r4EEzeQ2Kyg4Icd67FAaSfM5a3u74wl8F7qS82Dh9vRpQ4INaCkxRSJPLuRQDHxIN9YJ1i5yc7McmODuG/hiZQV4yUi7evWsl8VwJr5/9UDts50bftOCY/+tirxtl9qZZMQK3ojKteBgV+ERfwToEbKGuea84l0kSCbcE0fiIlcQUBJG36otTiSxX72fZRlHsTkFD1NKX/EdOixMgPkS069znIA9YBT/B+AWKkn3fdTNd/67raRXezYwMwBzpQ4Lu2bbd8C/mS7IDHqvRoEyN3mFXLFX0gopw14Y0oNw5U/tbdvEoQhAgq5beCHpvHGyeV4uPLwOv1WnhViy6x3OXNw0GhG6NEcx9IbT1rLtaY9INZva7P1ueiiIbQO3EknWusgI6wsUZLjZjbVM+F4tz0nLDByMpOkdH6V6sc7F13prCnIEW/KUttZXu7oLQ6nqcC+ubrsuq2SnQxqDgYj/Yz1hGLZvQLodF6Yva8f3iJIRiy4tioHCJBh/7ZirS20bbvCRdNqkApXFcNMvZ6pqmo1qt2O02KOxbUhsAl2emkPb7xYmw7nSYYIYkyfjfQv7MQKg7frN7I7me4n1EUJWwgLwobOvaDeHGGCbeDRv8fqyqpuZT4l+T3zLOKtIlSkOH0gVj5v7PPLBKZFiWYl0m3RsYQOyw6y4oznRDmFaK+DzSZNDZINTJVvgQqvdOawrYkF13YYaz1fmNes7YmKTpNrqCreigg6gP5qMKOJlxwheHjHiQUxCOILBucowHdQoIELU7Kuy8V8oLiynIm4BWb3VsBYQ7N5bico0qh+WbQMuz7Xj2W7zjNFLmN8xRlYV3525+s3fzrSDFyUOVEZ3PJMkkG4MOo7XzBcb0qMPzhje+KNZNvpXmHvMByR4qkGjY22kPBZsrsrFX+oK/MC3labvbnr7SxjsVK6D7mM0/ioQx7PnUlsunh+gqB/1gMPONPoa/es3MNzHvekaxp5dPQoo8zTzcpHUF+iRLFOkMkXw4WG1dV6cn4SeJ6jXT4UA5aGVrSoyIHdr2m4+BzE1r7QZIZjPrnQ3mYdvEl96KPKBe9EnssJ4GMxEr0IYjNAPaWJeDhuu5w50rkni3c/J49yk2A8VuJUBzKSbGyR8pyWowEktG3M5cwFqIV3VoCHnZSzzepzLBVJNRwfHj35mrachCw1+aXbZgVoWKwmYWUAFeApOyPvKpAejMnb5/7XHmDvrHdNFHdckzkrhAX5rHj/p4xb64vR9Lslcl0Ho+l43eG5J/2DxBqR+2tk0ZC4dHab8SRHJaKUocATVcvdNolYqI6R6TG+sr8WrOaIyhSt+8qZOSad18wqcl08APU9460pp8BadQIygJv+Kv/iFspNBNHnA63HFyL0qcGC8dkcQZOhevQKagaUgDQMZo5RLC9yFHNXv/KlqjksU0wZA/yh8+cO88a52fmqLCQxtA6bkjlrF0kQwdRwb3W0d5RUorHbbuWnTXiz+WqxKiEb2pg33zkBeW7oGBoLYzGorMoregSJP3hHJnliyd9BoZXc2kpEPc5Cze0BWXdCxzAg+unDKpgv7RV7Yzwt7Ft/fHWZnpihChu537VB34Sn4In1YKt2GU3+/MJm2bGsdaHT2RGYlWxxSvjmHPIFuxk8QuEInI7fV7kbi79ISGHAQuKCKKgnOHN8kc8tQEnvgY0Y8PPAxQxjcTzviasMeeO7+3qcTkgxNAfQ7+PewWzvjWMlqamj19GuhjHKuYMzw0aPI26WrfCgsVNsxZ4s/3jUpv+qddc6Ia1dRfedflNyVzh5AmCqITFO/tw9/ZMxU02yy/mLhAsdlomuDngt3+skZj3uflOQnZXmGH876pyL7c5rYnwPwc/OjTois9yJXf7urfKfzNzgDU1fml9bTVpLYbpyYTkLDRl30KZSe8qWI46xJqvGid0FsGtCN8cWsw3SCRM3IYyR4gSdmv/IgL5YRk18diwiMi2gxVAkRx09/nlc4EqySjQUen4NL498Xd0Prn53nCgoPmYnyqCyELvb7WMaQhNn6FVyi08LQfn2hvkqQ023xt9Yh0XtOks3RpJXXPGX5Vojgi07HHH2nPzempXfLW2LfSGhOpYisrfWuSXgKIA3fLNFru8z+ID+AXkpD5DXu12xmLQQSPQpucTnSDbophe10k+nP8pdAixOOAmdieXWhspKBDGecasDzOgItsiWfcU0BZQ+ZiMYMfxxk4QfMXhskPYMoRVGqUMNaHmKd0b4fDMdxSJQ9vjM6jrDRtxap2kXsnxn9K4gDxLczvzMb9oemlF2AvICX/U/unnDsnZPcyqNZ58d6LxKoC/NVO18kowrQTB+n2HQUY2B2MVyJBCvOCXr/m6ju0TzNgrCFNRoiXGIbDy/kTeaHmX9AWmFUnutotQeSTEPD6dG2TEiyY16e8teURPMrl9j88amXxOqPNY+yBkz+D0xEm9ghVz9fTxUfULt0WHI3bhH8Km012baP5Hdk8swa9fgHsRKdEMZPQSu+bItveRVfDj8bFuQaTJr7Y97GQxQHZ5EO5Wy7z3lvFfrAK85SYNywjjGPGw3t5DtSsCvqH/fJP1F71achgFXm81InxgDWGiYG4mGUnn2ScrqpW8sOSt5wQbe0ZV6OU4YRaktY/o3DlhwYImkFiSeqvnmf6/ACuDwxmluTzFhRPT8WkiaybdriQ4v8HvxjXd2TvqM07OdA3lgrodkk709faGsbOGcw/HeRQ+m+7dlO6J9z4tzEAGZhd/P6RuQOrGCyK5NS5qm+5jZqgxHyr7CfbEHOcKBQtHFajpaovqpK2DAwdahzHL9H7vlWZGuNfUbZxpwyjJK7CGLbfPy6Gshyd+dUorq2PzbGPINvoN30IsJdmaYvltUJjwFLz7JMZQ0LT5JfNQgxe3+rQ0QgPFA+Sz8z+GztKYXZp1JfeGrxGCRNR4DJVazDm/wvZEoADJfJHcJFS2tkWoKoT17hbapmNWqxsGxx7psCYO14ocPQa8VOul6C/yG36ImmeeHmuocc5QAWH/s4/lDXkcvee+LyIb4Rk9CeIpgip+SYxaSbQS/bFK4MkabaqNGZT6YNg9LqmMvG00OkGSZVTO3iXdP8nSKoPZguf9WMP4hHmH7b0xrV0UkOM+LD5HYMdfhVjywmF5onLh6gO1s+Ixdn9olPz/XbeKO0LhI/UVRPOEHLP4m12Wiip/Kj+7UZgAO38iVoV1JVwMKR2sn/7fTe52ys/p3IlChYGa0ipxa8ryn/5Z7IbWTL3Z7gm4sshpPiaX1QF8XTwwXvIIA8/eyXCGOykpP76oHrN82LcpuVrKx/VrKWfowOTfh3AL8mpwDP2SBebIiWWMqHIpmThowdHrfte3LZXVPPR+sGBIQxDC1FBeAr94WdY1P+caZVhGD8E5eecDXEvZdYJCDbBZfdc6P86GhE/tCxG9hWr8WtW52HKrCEV2/QMiHQZH7RO1BXWToJJ65a4xs/R2s3Emj+9L876QNrC3CghBCbPdbnLy44KQbxwX89vjZvo2tHkBU40np3r6uQcqwamEvcM1GqdEOyq5Ravr+AgBMlAy9KkQ8Fo2eYJoVyNunS2/vneOsLeRUUAGE6H9LzIHKoQ+iwZU7sWPnHkCOIswa2l2Z8YzREp27KzUZnTJhp1DEjtoqvX3IMtBD5LDRQHtquqRIX0RyGMBiaJcdF2t58ldhGYMZj7z8XChLEzEt4of3nFZLar6JY/rx/F49lzGrGgibZkCRBYDVEYbU56Sr9uGoVnePsUqh/IH8uV3GTTJRk/ak4tcigl1X3BMAwBtVzioz4Cxgm4QUo5d2YlZicC6IUCAoP+CHuvUcRIVVfgXwrlnttnj4p/1DS3A0XvjKz5nhGx91Sk2EYcThqbM9Dasdo7baGi9TguU/4cFqkyiuKe9uE2LaL+nrbQd5isxkhOXvuN8mT1z8CzNQtTNU0vj4giEKy5/pz5yaoOAFBEu3JS75PR5k44JOqzjQ+yPWuoK+8Nyl1DNZxy6W+xCgug1SzkIA2wASCzEa7xe5YJJ+nESLqZwIl8MM+zorvTdveSCTDuIJ9qj2i1Tejlt4Rg10RyaZRjN82gGOu5IxMpS9IZkOPMwPefdbjrB7VJQXBfonOPlFuJQKp26Hmmr+IdHmAyNUk8Z7QHsY6OfJHVMmHcLv0gX8hWAXL1SOSb+sF+n1xzboOSXQyWRzN3wXJFGSnmtc5uJPYnYQzNenuAyvKaNq5WrYIJ7DZib60AAdjXZ/UkPw9Y6hzZPsNGlwFIFpOxA7UA1iBssE2JUDpkZxqY6NG6yaly0a3PiCPhxv/xAOb91WsIvvfaId/2s83E8n+b05jcCMPN5FPLhqOr2+/G3aa1vuaJFo7N1GsaCgLepC2yhFF64bcrH4ewwa0KKJ9qEKs/2LVnAVIprX8yHwN+lFRrjhWw5KZAmktC+kPzZX1cYFdirg+/Y/xKdFDUqnovsc2kybGetqvMnaIP2ytjplJiAcngD6U7ka9vDLC2tNKrZ3BntHltN6SfLqHjUDHsenTh4sUpjZ02adV8Uu2KiR4q8qZ7ttfrj6j3lFs41aKltA49LOWoAdr1gbJMK19Tvfskn/TClvAlPVLCtnuWhIO7qBAjdd1wmNDnl0v4LZYzJNvCf6ufNyYZfkHWzCCf+6yVVo+GFOTh0gZetzicMULqWxAeOz5HrCGZVvHtQHl/GlI2rH+mtAs61rzhjPV88SKNkkBOvkzooXUiiMQtR2mazBnD+qHoH5XkH3HdHlgnUjN37jlt0EQXvFVhliMyC1g3dQ/lFjJCDlVQ/jm8WuHLV9eh2NZYWc3GR5AsiioPCaRGntKlc2IXrrhKoASbY6u/p9OLmbsU0kT6rbNOn9bSBBS5ROVejUanDQUIsVotvAolbt908AKRX/DLEonOLDSm97Kk4au98xKKVR4vvQ0GegV5hud5d43+8wyGcemMENY5RyVt4pNP3UeE9a83rxqc4BwclwV6DQVIOz+SX/r5xpJl1vLbUvr4PCmWeITbWOyBEQlDBpV+JpparGkTdEWyx6TMjPUcjlMMOqDamzZKWUTWY22CsD3UM59useE4Bax2MwsM1TwHW5OIBhFP0g7tb16stlgc0Vvx/TTNsXVJs+GkaoypVh6AboARzSqWg9Z9s8+f7IxME6ovPJ8gTrG9FGh5rzNahI+zgOGmFfSlK6uU0NM3qYeMD0nmz0EiDixPDBA5teo4rCGvWO1bEvZwnByhbPbomqYPMtngdmrAaJKpbrljn/RUc0u9O75AUXCKc9tdSfhaBsWTGhYoCjDK0VAPlAPS+ef30zfUjmn7/OoVCNtnwOSnK9E8LticOH4lFfGkIsmyd5GlvrNzSSqPadPUPPkhs6ee/GOjRfQizr8k5aYSzxFM/I3bOpjGF+L1qRcRlAN8Ei6/rFjeWccSCkUG4q65F7gEF5SwGjPDz5QVcT/+AY/DRyuRDiRcQgdYqZOtmKTgw+ftMPDV1kN99hI0zJr/rWUMnlXXhuC41pvo90iOMgDFx876rZWnc77/UmUbHl8lb9wkeh6K2qkoawspfHqrp4aNzfooSgsz7a/5OgVHR3sT01tuKzI1ZB7CsrzdnggRMsSi3HPh/E6iayreL3a3l9iaECT9pWOJzU9bZuuvK9zzIdRBxdz+fRJzjNx41rCUmI1lc1uY1qVJWdZa4+W37gSeab9sdU1sNtoH0rqq/HXbh75Mfr83GYRTKeqvU6ni5h6ungrKur4NcuLp9qRuYZ26S0rzFs3eF3RCLxGe5pAfeRicg2MAx84C9UXZW3lYPNBg99e6EiHGmth0pVwqMplEB+NyYpD8mkrxahWXmOsqQHZ9nK71r/zo3LvyGd8sXbMaDNkvynz5A0EUA9A71lKmTXRAZRonvkh4LgoOPQZ5g1u1nDEAtcPEZyMPeV8rLTcDjyfBHUamIA3i0HJpBWO5Z1qzAysv9cMC9FIZUk0jnLRAx5FCEG0tlKltFGR4s+F6c2PkX0O4NBXYthBIeQRjHhTuw6blVsckW2+l56H2wAbcnOfO6frhLt+0xEFpwRLp/M2cpEIYypU7lWTCIHJlegY4h1XKgOQunuooXc+7x8csEY89k/E7yxSliToq2tsCFNCHgFS217V0sIPXh1RRnTmjkZei59QJKwdQ9NVM0iNDV+hqJbGiqs1igqIEDU7YkvVPxeB0CI5PIYQpuqg3R3VASBv33p8/pa/o//dm72rSB55GJU9DR6/sJUnAELNfBsZSnuz7QCfYE7rjqX+A4JVNgBv8Vrvk6AptNvW4zI9GkkhuF/4E3+GRItWZjLJJ3m0cs9Q4GzEt3cDFxIdduyL0UUE4I/Nqrd/WOAGB2DXSCxJzwdJMBPlW0WChLvclfLNs/MDaVOm7VO0PexVoCk92kM2xJd9w3s7nIT0yyJgss+iinym9kC1YL3CimLSqHpL9YrnDnuCmKzl5KlCi05+hobSJvNN1nFYVvkvz4YVvx1YSa+ZJtxLQtS8WKXonvcCcOdnTeVLf3KbYGzTitOhc9Yk073OA+gDlgGjzbxB0uT2qsPRfevZODtc/swMPEC0FTeOXxEsmMVH6896Q/iYIODTbT2AnBPRCeXBP26DITRfAjm+g+Czp1t1QTnnltzcMhULnk4K90BaiHP1FHbn9IaUmYVn906LjSsaXhztQwCCRmZifYQ3L5tZ3y7QvEDcSddbwUCUAI5ZboOH2acVgKYBwIrcjOcftBkyurAJU6WpQm8kFz/8go83Y08ABL8uQedRTsF9WK0vGzLseI7XjVMhG66xhDAayXt4EasxPmZCRVddyPEqrICCnpN0XSNxm27pk4YB2urmHOmu0XxJxSdNQFwlWUkfmWuqgRIZMXFzogrsdD4MAj/oOm3yScOdOwgmarcbKJ+4pIprJEY3ItZ3OsjKeV2AbQLqOCkeQsgEAsEAozE7BPURbxwsZVNP0enQUBD/DOxk0S4kcIT/ailhxg32o45BFeSbK79Xojjt+eduztXYMVOYVUrcUOtzV32K2GaYTXbS/5ZRKKHCG1uUXUUlNDaGlSdT8/700CGI74Lodu5cMeBqh+oqQUsKgJZFwcPj52Uoy6Lnmuf+RhIyV0MXY4LT0BRSLIphurM/8tm035kN9Op08fmyCwfsZUJytT0WttTxPFIFZvNvqgZLAWw8YIua5fwmexhYSz5QQSrgExhhwBQ59niM/kzy+fYGlN9/olA+mt5fSvIIZITUKGVZpKpZr49JH5staffqIyJn2GdfO2WOo3W67Yd8lReLBGUlLebFAXhwa2qiyaiiZftVSyU8qOCwPsHzbGqnywe9RUD1DKDoZKUlAsnS2BOX2mTTZIEiHu3z1TVsre/TzqyOmNB7TrvIw0B7jdSb2q6L6N0SFkj76IhkH/bHp3ZUsGPFIwCqB1jPNcK4ROH2jYGtnO/hCEHdH8NwY9SnYqRKHQiMpxuBZMYAnCJvfPu0mkiCB26Gw/8Xwc4o45FnzTO4xHwjSGPfhfNHOgcjb+3QVO3arfma2iG4vfqHzodpTNw7tL9/B6fdYXl+IuiDDmm7+rdVz/InG4GxQuhUxID+Z39TLcv4ZMGGmsucqTKZxnXX6p93N0SeO/ErKGQtTvn7tWIUg9sFkepyhhGIuuflC6Ww98p1VnSnDsjI+pMjQHYM+yMZvDSNygr6pjfLekgUI7aFAJxcTJSIuqj3bAPWPcDuyMlOhrCA5zFvwSxLNvsPEH+F4f/ILp4684jTXtXiOqXe5qWh06Z4z41CYS5Eh6Rbv3IbuhhFaJ76MMXPkydF2dDhXKfQQTnJJ8URhJUV2KO98eFHsMg6/hieNAYzzK1eA/3yBDXXYzaFMMnAWnbS9SUztidE2dkI8R751Fa845dUBQv/JyMNQiiLhhjRx2itBb9I5MaRoDayvZ8/yIMk0mYSxYOEijZPrIlTJ4sWZxLCwJ5cyEldkn6wME3DbrCnvtQZ4Jop2O46HNyqh2ZktwxRdtjpMUvkD72g3enOYhB/5W3nVwAoDOdVmO7g97VfWnGBRF/1H4UoJY7Czy7ZcmWO/PFUkEQaUnDBSwiYgDS9zlwuv4wNPc0iZEjkzvC3WYuafjHTMucDn6F6rBuHKIr9VlOn7PSU2d+gmVsWbMrmH4o1XIFN4otQsC7dsJRdeUaY1u2J1WoJPVZVbqtm5cfUK6hA9cfqKTeyIINIJho7oJ9IWbtQUcF9xuOgMH6liNL02zK2g85J1wzOm8vqj2LvhVFJRGwu2vIoLRyr5BKgTnMYehMbVmkfr7pqs7EOjcxjUgeOnRb9TlpzzpxfIYkCam4tsgV4tZRQ9C8CAegkiEexMAFt6BmMo/yYFEWtGs0bI41nDo1S7M4Uc5uNlp4S6GT6uwVVXgph5ndtBAb6YtdBZ9ZWZ7a7YCWNe1r9cS+zvmTjZHMmalfdz2qRJxA2nin5JYEHes0X5OAOO1izGkR8QIQzxRLCoCMDIt+6MdIePH6m4atk87aUZge1WYtkV8Jr3qZcpXTQi2OCFLBZ5nca3g49VdJe2UI9lt9KGD3KXyKx8vxcaMyJiyQAU04ugi3OXN+rdkkMu+kEkjtyk4S0ytZCVqGV8rxfIFSsjR9uiDKFyltZwKJz9KWEdzVahHO/13M0Osrc6Rrggpfl9U/DVzTpiqXeKEywzWHwXbaAUbvKOyVgutx9EB6Dyv7PasVk85Tg8c6zNdXWZu0vB1bqssAxJ+NqZms+t9NHDzgelm67xZ6sWh" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV0AX49S4XPonzyGHDwZs-R_VWAKpvYG8_cXVgn-Lz---VSONzQ1&amp;t=636426639120000000" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=qph9tUZ6hGPMRv_P9VwIOe223LfVgEFllbRPRF1GclrFPGjUisPfO5Fl0w9cc4fIPZ3YzayVwId39zB2TSz0ppxHFLU1&amp;t=397b6a7" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=TvpD2YGOOsBHrEswZxEHEteBCJr-PW6mhkwAauBn9GUjoZ_L4qHkIePrBjHiSySgQeFgrSMv30LeZqoea_8GDSqL1RiaMiLSvG0v8g2&amp;t=397b6a7" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=542" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542" type="text/javascript"></script><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542" type="text/javascript"></script><script src="/js/dnncore.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542)-->













<!--CDF(Javascript|http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542)-->





<!--CDF(Javascript|http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542)-->



<!--CDF(Javascript|http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/product/tabid/61/productId/5336/Default.aspx" >We Believe in Miracles Project</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/TheChildrenareOurFuture.aspx" >The Children are Our Future</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/MediaSupport.aspx" >Media Support</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Superfood.aspx" >Superfood</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News site and governs data collection and usage.
        By using the Israel Today | Israel News site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply1@israeltoday.co.il">donotreply1@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2018 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    </form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=542)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=542)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542)--><!--CDF(Css|/Portals/_default/Skins/ITD2/skin.css?cdv=542)--><!--CDF(Javascript|/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542)--><!--CDF(Javascript|/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542)-->
    
</body>
</html>