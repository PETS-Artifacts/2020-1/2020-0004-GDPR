<!DOCTYPE html>
<html  lang="en-US">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=542" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="APaJeVoWdq7AtwdpcDUC0i44EwmxJyeE13Oq7U7rUnD1PvSWkBRp6FDHj0iaf+FKz+OFgtgmWK6amgFKu8pzPiims9ZEcAFUbP6ZESRVZYpFQxOcW9/S8dbn3STJ9KO9tj5Qu8/VEL//BRatI1VuCe8DVc8rdnOwL54/1kcAj7Lcx4dsQEAvFAYIvUqwL08P0lj6FpGA+wSLbenx3b6tAoeKYLSUcXRJbXA1xEVvQOVjZYT5xfIHmOZJMQ7n0SPJrSrCIBTIB6nPyHYAMW5ngUMWFS3mldLp2TzEj0qTENzfGUKB8EMDQJnNmDGFJWeWKhpr0RVPv88QI4YA0Jtg61+1h/GB2Ij8T5QAJ6g/k7hb7NKOQGKcMFnLxNVdFFPd7iPaQRQtVyCjlAvxMmYjovPy6YvGJDi2kLtKz35TwmQevxwCfWUNu+n0uk4eg06mPBj9GgLWK3DD6lybK9ffbf5cbteobjfYP1//AB9SabKHHLPQb0LLkUkT+ZxB0a0O5xe9kKfrrTM4LBrFL975WvALH0lJtZKZWosjlCo8CATn1L6YFI/hT6U5b5MIUU75ga+nVMjeH9Bosft4YvnAky0Jg1ko4CpeZAZYjqMB427VRpoyJe7RySZGLJk2W1JqqX4g7AxpKMOlNFF1K63q+JJ7SeujYdpDufjSdl/MpgqMEVFgGSWkqB7DxHtcs3ihXMJpkNJ/UcMFnySrQCDQQd+UtZ5/9sbBX3epTtHCwt5s+axBJ9K+RVoKCEzeNXQb1AvyAIPOHK+ONe3DuDW8rbykoWDA0jr/SHZYDVVQ0+mofE2KIj+7hhQUxoOme5l8FkrxlPQFG2fC7jJ+QAIJk/ZYLWaBC/3j/AfDlQACQOvUdT/V31DesyI8GCHeEYacrLTjXrumfd9ac3EexjlptIjeJzFR0jmgK7H62/UumyV05JtdAD7z+QEW7iDUBAbJpAI/EsMPGXuLR7gxI9ciSCUKIA+xCp3p/H3FM/aRGBiE7fyeoNZsLXE5EFGKCl0bUKQiT6kj2KH0xl0atQH9byNDnu34teRES2AdVQ+HpDonsme8gS6EAWBt8AQFMYfgnibwLf5v9ec3K5jT8kpkhHPAdVnQVPJ91NGNLz5/KWQk5FXok37IEm8L75gLym84u8YKT02wUPVi6RstZOH8oUj5YMppOkuKAxWvIJsYM9NzwYpFEOftqNRB93bGmfpO/Jw6OJNmD4TaFtfrGQXHJPZuntsUIimO+eIA1bQdQ51r8jZJt1uhyyhB625/p2dTu74RHE7i7bTwxevyWHpo6/j8VltqoIpZjcfFEbb8CDQ4GgRDJsskv2cBkxuCf1gj3iUtVXS+ltT94CE3Zp68HaWhrCZXkpISskzux52noKFieM0969ntYkJ3lVeUwaVgOEuc78lZPz46Y3Dak9CH1pJPNhg/HgkJy8nN4sQeQiYGrfKbxlkhDRPBahJS8jbxg6N/zyfNJUjaeSgQGr+IDZJpV+nGrVMXe1sc7/LpwWuhNfwOwa/CEE3ckN3t+Dzf55CYyCzEty/oYu8TWj193uiM9gmhsn5E+8u5xugiLwAfzhUSfK1XWQKeRsnE88aALpKeGwzlhauRFu1EflN+OpUGhB0LHKWiSA9XrEqFFM2+1h9iwo+SodjjZFY6ngJB2BNWsFFifv0a1voM3RUelc3PvmRz4GwMl6wtxafTyejdKUNvjC7ievWqTVXJhWbqU38hTGGR1tZHAsBJA888W2y/MnLd+YME+vUV7wkK9xyHb9vjeUGj4t5EHt65Vd7PLbqCvwByqODA36ChpdGczBbzkcG+n8hzGBcqIky5bmjAAFqPo0cDt+RDg0t+z7KOuc2dFMIzBCwNvxpEINHXygGK4JoJbOBqNGS1Ztx0P0AM7CtYxgq+XHTEqzokI602xm4+nRNFKHQyN3c6SOe6yj3bVg4tC5Zld25YAhioyYlQpM8kupBEFls5gR8SSV1c9h9Mt5bG2qxPm3LjTN5TBSHN+JI4R7nUyz/ttqrwPXLy1PDgRk3V2f1oCeiDp8WB3gtsK5E5FHgQUfgVHd3EmUC4Mw10a1a8Zi7OuFFJxXa+udBiHAtSS05jMfXqs7cELAxJ+lBk03rHJ0ZIVLl7mYfSBeWWvi9DODiBhob/jZu5j2Q2QfA9PHohwJd2XXkTH5uk0y/8DYeYtJqB7audqfzbTuf6wbmXwC2t0iqBDENBjmdk+raEULcX3k4kttBCVR2Yifmd3/ni0ncIQL9a5MHCcQq/JYOrYEbQUgI5YLNGy/WaeKc/06RaTiyTz/NIX3abFwacKC4/poPeG97AfJyQNxE5KnIIOEqwfk4lNjhabpYIY2EcGBXjbyForDzOQGZq1GpK6HcBd8j+CRrCz+Y6BiHNlu0JtxKm0ci0Z1OXGke7ejZpjbhv6keXLbaZurlZ7+QOUWZ6/01TDR809P3nm6/kS5rRsrJr8gYMn0/OiCM6w3Xa4iKr87YppHQaDIsxB5f0m6KCjojkUz6WXyuw7eN3ZQ/RGeDNct5Z5Q4mz/+6z3/mbwPBj9nBdfBXDE0+AWpYfBBH396R1FrFh8R+GZOmdS5SVnQG/oOVxF7MuvLN810O3NxnFpWgXN2FRPVAq1RYuAYq42F2A+QkI9OkOcPsnSczRi6gC+QtSo4R4sRY3+JE2UEA7Z9yXZ52NffkRqKN5TwGWTvPhenB1VXHzIbZfLXt26A9bLNVYawN77d8yEHii2dzWmuG59cgKn8oCaB6SCfSk6fSB6BDJsbkIznyhr+3dKS9kTMz0438NZhzAZYNtM8y4bl0pact78ATrET2bCZOeptTEJ0Q8ANxOSTqX87bIvy0i6AX9jAMa8HxvNlgKfcnrQ6MB+PJ56i0YrqATuKpPFhYm5XRFEEEyrhzX5mtZOezP1dCAmiIM6zyt7sUcUyl4PYLmksljOGLg2/W46sN+6Spm4ynLLJ4R6WYqkg+BUGZy6bxvuyeIJxXys/Y/vaZ0IHFyOo9GJxQ/DCmCWH60/zlEx6CylSOxeASSNrq7pVrQugsH4KtBAjV+vFvLXuQRK+0Vk+PPUqgAUZxcWyd+lo/T57apPdleV6ZP1+ASqMoRRXFBX0h7iC47XB+9/dyQdm1xOa5MmAgyNtV5XAVyhNa3/TQmGOCr0H8aVHo7oDuHPZBXR6bDDuGQ4gQb9DUQ9k4QapXO8ywN9+8j3VmHE2GxE0qOAbq1i6Vb70ULbHza6owtWDVK63rNzp63y4+QynCg2xncrlyWzIdatJmIxWzJaAZKuR8Bqdzg1l9/1rNvlIog1WJCUWF9fPSyep3lK/aTyWLn3xMTGQb9ChE7tzqyjo3p3dyxmj9hMLd36/f0jHJNN5ux1C2reaNa3H9GPkOo43DGEsPelUeiYEcTjQx6oRsBL12Tu9mTTR4xqf7hkkFo9FqFrJ/pL2QOqLDedbRUpIbEAxVmL/MXS9rF9HxQUK4gwAGwle9SXg21GUz+IBOqr9sOjopqtJxGlVfEqKOeaB2vqAxB5LERGgMGxtx1clXB8FSUTC0q2ZjilQpCsZJmFYGIfC10eRJ8X+eRwLf04p+O6+TGuHm+nC1071xW55IYJdAg6kYuoINAuoYrBZyFl0R2aP2JQRjzFWd/d6z4hL+cK2COxLEdcTejRdu7QjK3G5bc3d2AU/0y5JZ1cme2utVT79gMTQT5pZOoR737cAWngMWxBJib7LxuB+dUB3ThFIbowk4GmeqIaBEB3Tns/a31UHcNkHo510CAxIf42CVi6hJuQIfSaVaIrrBXwMJr7D4AbbEf3LXxUBYGgIsGelgE1QkQuDKtgwnhqEpVdjFFfyHJUVN2hmE8MspxfKMSy7Lszbd63uZtYL6DUg4pbMPSPykMj02ldO3jWuyhEYF2zRr9PPtQxMSHvSUfgfLslGT5KDU2H3sHL6fR2WGPM+iZKa7m1sGt6mdwZ9B46kAwezK/scZH6fRcriHcCDOfWwck3bJDrPhhXJXi98A7EdaWGldYqB294h3Nr3dCotLHLIBopiSYPR7qMbE+JiMBoW54IoPawQqIQxzjvJpz7w0NyJ6U7qp+FePhiwan7cxzNUQHsdA4lpTAXBADgHqij30ZWVMhhW5LjRN9o+hKAgzl1lIi7fJCh3qT+0kkS50hIAww7YHn6dThKOQlmxskwWn+Rzu7tHzNQFa6BeqzSQWRzrLvyFHSvDgpyn9Bc8YWAzWPBTPHiWg+H/i/iDYYIaZXWRhWN0JCGog/4G6SG/6MLMQL7RZ7Kmiit+6bApm2OItAn93B52TF2C26E7a5dWTVHBU4rWvjc8qxWFYhg/6KvqiYSayG3DS9cphgMoFpYrct7Rmm1YRWveCELAIZskYGFBg6DPtUMAy2t0jJJDMrMa6Bthi/OUjqpo2aO6POXae0XMzuIQEU4ljRhU58weDgIVjhClyMWBGafqFNp2b6ASsrTwNFksZHXC7zWlltsvs6x2/sojHeT/pYsFpxFiCpAjjKR5mo9EUeCOt/jXB/heQynYu8Kq5cdMRIKwIpPUssYB1bR+579QdcZgIZyVAtslYMl6leJJ8BBu4JyhMB5ViWJ1tKjO9qZJGvufwA52d05bTvwoy1XFo65659A9nVtEt67/b3M2vnKrQBYOkG9mFNF0HdnYvwviiJX/tdjY0y7zCQFX6/MsL4JD2BTBALu/iiFRpnyp24hRG7EeSekD1WlU5XA/S9FFBTD5Wfhs0TNZ2oHWZg/zfijU14pati74Is1c9mLslDeAd1BDuwatfKcEMFbW1M2jkfsawvBavie92fD2imO9oRnOtowJ6OrSiQHH5bAvuJcjwJlAiOz4ZF4Im2AAK+4kRwXFvf6cdsXATSKCA60+ov86QPVGqjo43wnfP9pAe6thlsYpsRJYezGR6D/SEvSC/vsvH3Y91fAoaAUa2qMLg8bd7S8DqqdnMglPiOj/7DIznRj+uPMxUP8NN8aCOPCFgu6KXVTI/9VwwO2KjNwduxgwlslaWi1GA5kUbP6l74dtBypF/mHJQeWZgtF8o6SoErORm8TdDh721NNmhrA9NqCaF/3xvg5OmDY4faJ/KT7uq4bcT6CnZeFwyVnfV4wMPyrftSsbuAIkftQviBNZ8RuSFkzgg0EcDhXmqmRgIa8KQeOcjMaq0TyBNEKm9JNmBXuo9n7CrSdzbsVgoEpsbz4+Qlrcw4QPiYbnkNe6tT5dCJI+9oNA4RR775KyDTiN9HvPkqgpkAMJ7xWF+Pzmg6NARtUAubN0JsWaM1fxgks1vq1G8tPqq10kf1xxvObOKu3vSFF+nvAVTzefFdFXHgVb6Qs+pgHjzqbmghNCX2ADchapLP2Sf+iTJL8K3kqB/MYpzejAHZ8cVQAXpRloZCAK5ccZfoxU2BKo3PbIcWPNIFc+mSK0L+GEZgeU2rjH/nOH9TEUX0Nj3ztAr2o8Ve6l1n65DVPBl6m4GCLjMJfwmfW73C80yERIY53tXCk3t8l7vjy6MgN3vj6mB4GLNwMMLjtzJZDJJzdj3DK5h5vjH0Dl0oLxAOxZ6gebjDa9kfo5YnY5Go6vTcjKY7QsTx5DGaTZbreF17IFXgVmojjTI4fQjJh+ZeRuZd90Vt95vNdj1/Vjw3L1JyJIswJGAze7iK6+MHL0+69NMxQHzraVAspDgFDmqMR0O9095YQ+syzb8krKcDZ+gFeR04glVENA9yoDjCkss7IZdJySLa5vccTZ3FQxgpJpz3TJy6XtzV1dayREDpipy1yCym0qqV6fgpKZiuAMFJMQgH6B+MJ09JQZA9SmpXKctQd7lMqO3GiDM+/cGQZTzH1p4PxM5nGCy3JnO9pXO7bBVJgo4ttLibsSk7seHQBU9hEZ/yYLVfQP2j22WDkjAmnx79/APg+GUhJvEp7su88Kw0TYVKUqRyguge3VKbFciaGbermkGeke5q/OMgKlsQOB/9PAAhQvBYuv33R33TLz6EpT1Hi9DnXUbyMaGzbUuWRoFoi1YP5TLjOU9OhkiM5TJ8Um4lyrext6IyfWvgPdqA0/mLpMu3L4oIl5Ql3dzkM/h/xOT+jNQspNIFfnPig4gQq6cfml0O6nWhAwEjA6bJOLGEN7fZf6JiwwI3R/gB9J0MSuvHkaKo9nHwQmlWnKMbVQu12s4oF0hcdVPH+ZR0uLjARO6J3q0cU0Go1Q7cYGj3G6j3jYhMdwSlNwb1+vBLG5jnC6DvTG3iYEfw/UnmjY/XbS0a/IQDL3AFAGZkonRHcnU0EsUiTlLGuvRp/RUTu/X1YTb/EBH1QU54VdsHSV9syd9KVeCv6H6PnUTWApAEGQOwZItZKqnrHxHVHy3cBTOQ9r4F8N9Q51+r0W1bkpMdk2XvNJN9YtaNzg1lSojSU/Z0q5e+bfjMvCrYNiK7imNxvTxVnJxnre7N79pyosF+NefKghC+lThnh/0G7BF1heY+LTeCxgB//9jfDRINGcdVvTj9ykyhxgKUvkiEFFV8d9dt0FKtOaG0r2pTlasIDXBFRBjKXoybI8jPUfKJewgh9uPOLOjfB2iZMSMffhGses0yRKd1IznzYEQE3z+46Bp7lWybsud/NkoyoHhhfGWSwmT/SrzHfGy7+u3RT56qdKJnGgqzhpisRPIzPYKyBotiOXSyMadk8Bcfjzbq08LXrYMG6PZc0j5v/Q58iWVbyYRMJeXVQWUfFy6WUBVuX8WuAwW3ebYdgfETsNxS2mx+PVclEREdDbwcdXWjAeCH/yRkV3IYew9/bDX5Ib6Q+HdaGhvYDTNPqrlaqeMVHSpuU990mdOI1hr4YeHCewwW7XXpiH04RLLKs2l5W6v5ShFp4uFs88zKzkCrJZvaWdJ7paghW7JJ8qr1wYvvgSYZ4Utw1jxjF9q1x5TdV0on/hpu/IrmwwBU+wTxZav7ihCOR00k4JjQ7mxJTZ2t9gslxOm7ui5UZtY6zYAZDfEupvp8zalZMJ1g2jBz01fPk681AYl4bCXTMga1lD4fguklTGTDnByil74RP0RjvXeWL8tMOhbkU6jynbZgVlNOIcWZ9ZAAEGkk8KyKzZPviW7VPNZ/rA/cFsgqDqaEtqZKsewzV3JiehOyK70yUVEVq8l7tj9zG9zctOCsIBSPUNrviSrHcbtmdB9ThGBgFU31Hpp7NA6fthqf5s2JLl0gCWU09NZmShR2G2moBnaKqy/J+VoiyohnPh4mme/r1Pqkeu3yiwUxbJkBMJhiq8KrYXNFEYasn4/lJmbEkriUZ56Fod8s/vGb0Tekczje9M3KnBpDIBokG3/IYowc4rBoL36E6GE5Rya5T2F6RUnq351YJK5/PiVxg19MZ9ocoEjsUbqNLS6Ne5BIEWaur++Mnms0ZSZdIP9Xf9D/PrsAg9amlprL2PjrkDx4iuq0j+Qd0x8C3bAe0rjRU3CmyI6S9/C+83U6rBuw0VbF6sEKV276H4puFlxwzmwOJRDYcK+LDH1chMH8VVRfBskzUq9yqc5ip/yJYWIjlkzZo7VpH54hcnvcI/v1wlp8ssYFVVcRFc03Gnd3ixej3KIBB1R4zL6mXPkPXm6NevSng9ynq6l7rroWieVLG6Uw77GUYgtg3ZfYdCWKjHrmohcirug3KCkKCWUFuTGtO1t/8fcuUDdzHQQAnQ8ALk9m80v6k1SSGbQm3kSy3A+iWB3sqKJNN7Juos1XoEV8/zROowVjXySmg942l15jEk4M7BPGF+FSbjqdsg2HK/Qdr6A/2Paihap33qHlaAkPMKF2aa8wrhZMT910t5xGF8T/Te0rcv2cpZxivchP2C6k1fKhr2inqjfXp+lnK7Yp12rO2yxNpQasErDS9CgdTq9RG9kYSD84Kv257NB2kbaSbuTMapHGnUOQ+CxMS1hgrdLXfp+MYIVcQKamA9Oi0nNSEPdU6crNV5snNchzk3vM4yK87OgVMg0M4fya59grlgUBCd1Bms/GfhLU/vgGkI0RpOK3wwNNGymARvjTe2pHjUm0ZzxDULwQ3EAnJWWMSJxfuQKAecramQgnyp9SRDXOJcmBtmcuLVqIoyBsd1pB1wrFJMRrv9wQfSsSBDpLMekfiSqGWXv3stOToYvkJTLHUTQ1fqxSsqIPa0WyvNxnKf1yJwyrvX83M8LZ+c2NH5PuEbsP+f5EtG87BVwJ3OFIqrW2xAbWl9NCRRlNOdQu6yazxX8MgUurVb5v+ql/UBNcHwKteQK44pXXTHKs382/63Exy+wMFjPoaoQPgvmcwzqRdJM6SgYONcTtFoz3rmyD8L0jHBXxB11hdd4ecvbmsHEvh/4ocSnCMR3VuzWv+4DtEa9vHA8JIoq8Sr47KDuBfxDqWyiOkvD7S2HfqFIskoI5RgRETz/Zn6xKEpSXrEPJpxFVUcgMQPumgTWLg2rPLPvxh6uckwXX1nVt8eyYp3bM17n7rl9wFO1KH+x8Wv7s94x81Tg3wV2vyZGyljNtSnGv32x1whESgsTmAMm9/Gkr7xjfUkFYH8oHzw2UW4NSSt7FEMBX3RzljgRO5l9UMOGzDUKbvCQMwYhIy6BEbEzsAYgP6yBvU01HP4hQurHhewW5h+iDIcOQySf8C8C420nPtNzqkaSsgG/8MJMqfD+kePxI/EH99BVrMS+JaqAv04ZY+/i0TGowIfPZXJEyLd40xOVvZkZeLQ7jrc1PduTFB5IVThjN64E9T4PuO54BPT5q2vYSF061S/Yp2Ge8CZP2tOTPd149lJ2U4bqXb5/o3Y+dEY5FAnlPz7u/gKG2dQLxrVzPpqY4pvNNgC/2Xmx1sgXWakK3ldrFyuTgNlOhdAMkWcLRDdOx6zaOEI5ZglS/GlJsBFReCDkUvK7V8ef9eMELe/tly1s8cr+aMILUOXoOac8wlpHivC7+orsug6pUMN2k8/KOnpQg9bd9YOEayZtio131TEiAsa8pRjNesU0Jua90jZYpGpJ2mm6Cew8YcM0S+RPlYrL9BUYBXqTiXTreGePpdqqrLLfs8beWXFGuhymcfwDMVAxZ1KneJ1pIDGk5shHLnRAwu+PU2O+1AC3m+7gHArFCfLm3tklmuvmxwc+Uu6p6QWK9q3BGhGE9spx+J1+pQbi0Kg2jfn/rbxBwMdN2W41mfvF6j9S7Upyv6mOIXtINSnvcbHjFBCADLKUGFSLWwAbCY2n5zGKOUKC57RS+YbjH/DWVFaChI+8sIWEQQzo3C9nH9TNLvupthvC1566fC0VvPEWjSpTr8fxJYa8qavG/YyXn4cq2agN8+oeeWQnCbK0p0ar6mtWfAp0KRbIpwBk2aXD0D/05vymiTEQ0ZqZk5SmT5EgT/uoJYkoUreYfhCO3hs6sYhd1pttQJeJ0kykpQoK6fmAKVJam9uEgPb61l18YFF0Rxxl1zZGy12xWAW9BdzNY40EFfJBNZhQDZGD7Z2uv4gKXpMHfAyHm8XTqSsCfFYL38TJLMauWSgn2qCu/kI7zTIJE96GojfQn634uPCOgC2vmbV/mQ/1yCyzX69iWZ8OcPdu4gcSUfi8q2zvp0M1hfKT14/FLYE+aJ8e/G4HWOiX4VAXr12AhtGWnTHUPvBUw5Wsj/HHNjIdMXfqpXnhKKm6M63mR3vqVYlkRvJe6XGaI/RsvIaY+91DvTgnrypOFVdbDRLw3931JaOq9rKvmbpBoa5C0sI1KTJeMsYpmiMbBTWv/dJCQv7YgcEZDfYyIBSXFHvts9XHP2H/cnLyywdwIHQo8USq3ydACOFf3igt3GdPUptk6arPXluygy5TuthC0IDZRxkCAvoESuTZz4TkDJfKeJksP9AKL8aQ/12Ky2dhnHGq1Ysyjw5eKeDfQKNi7iTkVug6FG+zZlMArGJpS2nFSNYmV42TewgZhY4YfiWJp/v9HoFxtA9g/3qvvgRwQ4E1sbID34ItSzDBj0UpmEqdul5WumlFzflinOXGbrC2rkXT1VvIZnebMjXlIryEoKBs87K/qpcKCGVXAIV9nBDz/912Gm0LyHC2cTd4HovrSgGnjQM+pdbh79xz/Ou+thTCh4MwrhbGldB+AIDQ5qwHZXjhvT8KcaXyVdB4Ue2wFIJQ2LjliZJUD97Mt24RpIQZxo4Ea+UkFN+hOEFDhx8E4NFttqPNAZMLJdsTHDzV1H0a1YfAEGN8oIzq/htfgkwHDod6/cDIPluY/nSfoooTfYPQxvaZirE3iY5ybNpfNcaAuTatiRx/hwJXHQTzl8Xk6tvx7hOGDq2VkbTyh26BqQvHdrRgjUGSb/7QQRgcqM/033Od/pD9YgX6IixHDPCOzqY/x+3S+ibA9Cv8zJFou90+CJgitR5nTphMrp281BV5AVbbeWdWtZFPn0AW5MmprcOkQD6TdQ9vxcv/r6Q+s3XveOUvxEs6z6EwgkMeIUz7PF4VXXru8/2IjVS1KKE4gY5/BpyAYJeiO0RcWaBsgmfDXpKlnQGcr1yTG6oDjPnobo2emrQn9Pw/i92+dmypuEaUUb09ZBTPL5E4n25tnQGg5Ex02XQDPwi0/xbnYGLHVaYD7oi+87b1zBGkLmWW8cVWYHO68W4vS/TAfA7KSDOxXYdUcS7FUjitT/44E8IvL3dBIxeVoEeZydjKG4v7EnHcv7vqDEoZjM1tSHxANQEiaf/jPlLkos0/CBT9IpODcV++arn/j+LjxnXyoYvFGUQgFZ92TrJFRWE+kdk74oAaDUeu3wi2x+sFpRsX1eWq/U06+a67E/+P7TGkrDz6Vmn50priTPjW5QlQQP4l9Sn4BQFeel0bHT4T6qYGcM1EUcAGItUdVx2kTV/QOkK1HKCQVIucVxr+iT2oE79eXwaghXx6yhmHl5dcdGgBT+xQQrc5gq+2nTy+cmpCWYGAYFztDGpLsUHs87FjV4MtVDF3F+O6RqfUQkmLU+yWLCYg9wcHlxIl6CPMlK0NIWhNIlRs2LCV7jXuqA7i5VRFdoh9SIF9gp3GJ/vAJRV3id50zXJPA2/rO7ZTgDXZQDId5sufVJcDgbj+Rjy+6lIJPK8UeJj+h3JtRirPQwyRvIM88iyiXNLPSg9sZt8qwvxCv44qhtJd6YLVjD2VM1JNdaB+50/f50Y6HD5BUGWupKGD888BLX/6tVKNcnkxkLLzE4KIMinV+TA22ZPE7/n65FhQrNTBsBXuxVwOc81h2WX4g+qsDT93Hdj0/sgqWao1jkC/yMVcT5kURiLEUYkHHl26TmOzFEEQ2kEhhfQ+fRo3kbxsnkpw9+xlP7BhCD4Gqj+eiNGp+Xn8etxH8qZtcu2tcyb9lqYRgbS59jXf8JWFi2MAoBtC+awRRakx48zwEU4zWXn7kQzHYZlhYrA72A3nq9hYazSLOSGQKogYJGYcLQ+yt44ilVGUqP5u6SbMiWfDqKioF/1CVf2CGCoAMPx4sRg2USYXnxI7+WZ7JUQeuWWELVun56O8RpC2A16mEtV4MwC6evo+G7okvzG2" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV0AX49S4XPonzyGHDwZs-R_VWAKpvYG8_cXVgn-Lz---VSONzQ1&amp;t=636426639120000000" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=qph9tUZ6hGPMRv_P9VwIOe223LfVgEFllbRPRF1GclrFPGjUisPfO5Fl0w9cc4fIPZ3YzayVwId39zB2TSz0ppxHFLU1&amp;t=397b6a7" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=TvpD2YGOOsBHrEswZxEHEteBCJr-PW6mhkwAauBn9GUjoZ_L4qHkIePrBjHiSySgQeFgrSMv30LeZqoea_8GDSqL1RiaMiLSvG0v8g2&amp;t=397b6a7" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=542" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542" type="text/javascript"></script><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542" type="text/javascript"></script><script src="/js/dnncore.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542)-->













<!--CDF(Javascript|http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542)-->





<!--CDF(Javascript|http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542)-->



<!--CDF(Javascript|http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/product/tabid/61/productId/5336/Default.aspx" >We Believe in Miracles Project</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/TheChildrenareOurFuture.aspx" >The Children are Our Future</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/MediaSupport.aspx" >Media Support</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Superfood.aspx" >Superfood</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News site and governs data collection and usage.
        By using the Israel Today | Israel News site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply1@israeltoday.co.il">donotreply1@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2018 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    </form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=542)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=542)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542)--><!--CDF(Css|/Portals/_default/Skins/ITD2/skin.css?cdv=542)--><!--CDF(Javascript|/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542)--><!--CDF(Javascript|/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542)-->
    
</body>
</html>