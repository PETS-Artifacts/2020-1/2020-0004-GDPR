<!DOCTYPE html>
<html  lang="en-US">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2016, by DNN Corporation -->
<!--*********************************************-->
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem,DotNetNuke,DNN" /><meta id="MetaGenerator" name="GENERATOR" content="DotNetNuke " /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=492" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=492" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=492" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=492" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=492" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' /><meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="k7a208MO/S7OBak0x1N7985BDZ/HhK2Rk9mw1lJGD/tTFpO0ZohxEqg9zYJ+37pnpHpQuKVdh+3rsMNzMdhHi+du5pyK3RyuddkEdf1lYHaWt3qAfFS6esiMkTFPADnS5+mX990fVLk5zVwbZqpS9nlEtQsEpWbbe8PHfok2cU22sp/iDyUep6Lg30fXm0UgygwSoTogSuiEoEUUPJkzxBdhuO64K76oO3epyyY2vL9mu8aYSNuMOKADEK57TuZWXODJKmHjyfq2JoKlipEa3zctLe6hgQX1cfsbCzNN10fFv/2df0m5Wup+rJdPwuNrxEvjK+smqZcYF6aU5LTZlq/jDNg4XSa8QjlAnPnh7B0ZYATJGGYCU+62/REG/yBChH7nK1xnFiCvo1hjyTMJQvG9+uk9vd8ZP6NHeuvsLuNkuFVCtnNfQcc2107lRx/FrKd3G3Y8pqSrFGXbk7Uo/2nHFpr+bIRPll7/n2DlNmeB+x0IurrpoRalosyFhXnKCUsgA7txVj7/U/ZqbWGL/5ucA/QcJm7zCE58SM763rIzX6RyhfQ5hUf06nQaKpch/i2F1r+uvwKOuMVzleCq+erTwGhLuyC5ad34ESKt3Wp7JK5zGmlfNOQ76Etxkz2+Ji3tQN42TaEkHmiIbjoT0cM141iExRR1PxEZrgaX3ueD+HHfAW4Td+gg/qCHMfGUuj2V+OhwVkRa1nT7nixtq3/s9NutvFf8IUxDUaTSoEEqvZCnLN7bf0lsIbPxRDcePk0JkrHR0HdItugapl8WQkT+041MPe4oDJw/COey5U/Bs9F5FjnBtE3uHPpDZG+mUWB6Vy2cdg5afaFlpsaT5eFxRY4mtHHEbDv8BTgC6m8JVbnvEJYVWF1fPBMrgAeicrAtBCRYpxPBZO79fU4u3YkI6ai9VtaeTZnop615Ph+GCSpjZ41c/0TA9AvGxVafTNkE9R2fYrozG/chgP8UOnzWWTJfRm8odLTWXTPOyaNupn1WfP+zhv3uzPnaRStG+eIE/S5krnRowqhuE8cT0+f4CkqWBNDqPBa5HzVLlya9RpuC3G06tg0GQsadKm8SnQDZ8RDSt+cT+SkDJgR2EAi4nmfmWkae9HpntIQsI0ZkoedTaZMXSQUwHp/175bo6b3H2iSoioniR0L9NaPMOV4dIMixXDoaxTlk9xQkVvCDYQeHtRSBU6QBm5WgET0qzSLlF951WEACK5oBRpESWnVg7QcExcc/dsYuIxCS2/Yr2qdvQ7kd+EZkLwamAKdQYjPQtTdVNSdFgHk4Uh5eEYIOS0XfbcJBpDIDeCZuSSZ0xl6E31EhGE4EzY450Y7rDV8c+hgOEQIkyqtFymNp2Hz7JZBsAfX/q7Cx0W9r4n+gOKkNicNpEMkSsyCH1uCyPVfLwMGxCLjSOTXFoz2FuPd2gb1niUtxx79r/3wL/ZrqGeWbm+sNJBMTTUiMk/c4ZTzMQQ/8ijq4a1PNAWZVLU3i8pWcRBXgVb4m1XhlxyVUa7ctRdnigiuKsB8cFhDKMm1Re9Hx+9evLssTikX0ZLYosai77oTM4EmGL7fZIeR8ZiUQWo5lI1vVqzJ8mZR7YiAPZihbA427Csesf8bKBnZNuQfnXbFNpeBFtN9X5jab1wy+T8BfhIxZ8wAAAeJwPrLXhtHTPnszISanjfgE3mEaYA9YdH9301UoD28zx/7bJfekTGDnqNOaZGXBnTXlXAh3TzRWDL4+BEsT3J2CTJ6yTg0JX1pSDQ0ck1Hb/qnsAmaGwzKmVQfVmYA7sB2uDpTU4/4bqysPTXCNyrZkg4q8JVA4WsLZt28jReBcLNjM1xbL2PYpIXlWHCCqbpZJ/BRb3AHeA4SH3hvK5Gm6/QU7N/tDgcKcBLA7T2W6G3HFtFkHfdAZAClZTnrQLpRD0EzCOV8E007zdj7Kl7VnJiZ3DjBHKFYZrdTX39K+KzT5R09VpsTXurpAj8nHtLpKpn8zwFoDpIm5VOaOOi3j1GfJ/VkbpACN9wcPeHB5PKUkfJ5E/xSWDpw0OFwQREKoECxxEIjU3NaX7eSgmBXu+4FTd1g/psl8h0DtDFdwGTir24s36sfB0affd6rl1UwxLs/GCvMPYsF4GVI4YGEn3vwn7CJ4yLNfXF2ptURk5GtaQqg26u+IdU7hgzxkVURdSIeCaU28dVl/wpmZFQsYMwDQbr5DYGfi9v0HedO35cagKhzEevmDrPQdLQI/OOZ0N1Wk6CloxuTh/jHlmnV343CtSgwW4BrUc6y5wvZJIQeXQsOjLcgtxTj5SrlJ6ksTY879CSKXO1+anpuURjFzQUHB36dJ6oMDXzFUmXELhBNNP16MxzRAoAqkD5p8k8U8lIjj4FnxevuAMazhc9nEJkUKQoKRXOyBO7A9oPp8tH6oz8Nz4N/hiFNWgElZNKE3Gpq69bRtHBBI8zmGy2lRe50plQF3QHXTWGOQaK8fpsRaYIovzj4a5axkFYhVQwFryFxK7CKh22vk1b5+UWG+FgPqoTIS8QYNoYFaxYQe+IXEwIcTHrXUUY9YDJyzD3WjhJFhzDPIrvFn5mfK/gvLsbQFkEoGvQI2Z4uRU0Evl5NDz+nF7qtiUQ9Ym8CHaAloWji3vByJp4YHG+r6QhYMIwLDzx6/ec9O6shO+4EgwZ8S/k11aVVRYRg6T2bhMSnlrX6Yz7rv4FwkFzRVJwGr5s2cpEBrdwFQx4f7MbbWg34Od0F2q3rn1hRRgtW9W2LOZXQmwDOqjyg/2s6xk8KgRRD02eeHYEnPTeWIwIwcTj0VMiSDfmQe1uxHum6MG0yYMYD1SPMLoIZa8wXt+WOyyJNxuo/nf9Ooh+OksegqPkQp1t8iW13lIU6gWpFW4MkZWS8wfiJhromjGrimJ2TYBnUxwF+/8Pc3f0QU8evtRbZRgpvgTlMPnwMZukeajVCmhz/lSLXSfi/3EhEfc+O0ik1WEUrqJ5uJDQwt8yMVLZS+10TOqTn84Xty90vD/nwet1uXbYOMSIxHLrvNUoTo0TmAFp1/BrV0cQTIlwJKvB8FAr4RkGqDlZIQok0JPxKXnOCTt3oJbRSDso4qOG9AFLmt2gKNCvb8SAc74BEnefI9l6HVjGbQTtZzY4DGj0x2XGgouo3Q5q3DGaxX9b6qp9t5KF0HuRvD52YacUpZRvDq2M7xXkxHGI2X1n828jzAAz+/YFhb1wUjRYkbUX9UcxW7P92eFOewww+6wR9oEDDdSCGTX0Dv6PN1h5DIHSE39tWXxJoH8RGFfHqUSxGPVZxt/twTL4IdHpggLc1t9oD4t8vK1y2x1ejivsEGLno4eySHtPVgdUm30oyfcqCr+oC7u4o8iwNESJ0S0NNlue6nSrNj2F5LsUOlAqKzNaj462jP6z2WN7n/45rsnVchEqTnuKJ1xCY7EJiz3Vs5mIhlZrogXmssCC++o/RMyQivzjOBSjL5XemLGAYc6Z0ZWGgrflok5qLmvAYyvHbEFuKlNaRGGzGrKFXuprYvegeSU8eaUS1tp6rpnXx0oFfS6Up6QEZS+PkfUKQb/6Lvq8uYmj53yQKSD9WidpzKha0SWgk0+KG9jXfEb1xmIta2dMYClGOE4BtLlCmAJb5FVsobvaN0F7XH2BcbzDQfQnMkEvsAcYFrIqpoLrAdOdmrh/VHQ8vnJVoObDKIYgwMDH4zbFkLnPnDq3CFksk84O6s+WMIH8gXdZqB2KzLmX0fv8UWbvZIRcfU/GDmzsntRyrCQQwq2KlNzUh5cev+t0YUh+9A7Y+AUmUf/+SGxdOh8vH+oX78bmsjyXK7Ld4jUep0e3QeGuSei0gB34DNBrzxA/KuOOnljqW56ZkdROcbEo2lv0tRifAshbPASPEeN432hvbsJw0aMBpUf/CupwKBYtKloHwcA8huvCBZkzkT+rUYslN/Eohp5ZcUVEpAKC9laZcoJ8E9mak8jYRppNhN0Fo32jblhd7ZtRu+kCwQCgkLGmTee+06LwbImxP7fqY1lsMZ1fILppFgkYZu9DuSS5oi+lAUh4/pFP10UU50FbVYRS1jHEXYJ8Iy7gagKkcFRcNFF6F1+5Ce62MJEfS6ERGG87S0WgQfyIojX6dU5oa+lWOnJbVw1PbbHv8kSWEZVPyOpa19txreCYTt7+5sPnOTETHfZDpY/5ousP75fJUVcEhl6Tjt70gaVjOcx+LKd2t6do/I9P6wqZ1cnOPQ58FiLvUmF9wP/KU7FG1+3D6CvBg7PB1P8jgKxAcQTIkr2LYMT6uPFHwEQV+UnptvP/PJsEuTfHvn5CbqT9BB0Tvx6O241ifA+JcZr8yc9l26Qpfos0rd8wDaZTNfhm1v0hA37aQf1jwH9p/CP0SWNBg/R+ocI5y2XS5d6mNWmGjoWavp5h7g6YH+v3QxAKHeShIXzHpJ8CdFNdm0NDjJQgwBMJ9R3vj2X7JZWaH/7lCp2MU2Lt1Irz+XboDAHVXsEj+2yUAD9DEX2Lc3g3KjdOrcOXiK5HsM44AH3TVNWF6Lr7aWlMfVrv5E4FpaFREUX9XZBMngGthiQ94KL/U+mviG2+PZMhoy+b5Fx6qxkU75U/BHgw/hOqPonoFWPIj79q0TKwkg9fs2vmOF2yYAUs7awFRPsT4KZhkHN6lnbm0idI32/xzxvFSszkn+8ZfdJebLLv6mluKi2UL0LSbo+D0UEP3VLspgaNoZPTNTm3y2kHxx4cbd/gP57WMvroEv5wWmPnrm+2lRce0UV5DOhs1Cb4zURg1X8pr6zWsVSkZkxTadq13l8dIRVFevsEnsDwdlmiZMWfx/dnKxZ44TCggsXTtbuPhuWpP7Vu8T9fCGF2VZGpXfl6Q05R4oOsngp/baHvVF/Q/A8j6lxpcS/DtWvY+3olOMMN8zdyO0o4NRCMD63FrOdEYGj4IqjLGg/+x3DmCB0xtXW4vmY46POn22A0qEuDtZBg+kT0runP4RuZMQMkciwCD4wq8Xu1yj7U8PqdsrS4JZiVw26elqdtTNYjd5KFANjv6z28TnTWDgaPj8ynjXYUo1RxXXLkEHQz7zAHSKRqc7IgT/dHkMKoyKry0EnY6N+KJKSYlF1+B5vPHdNPLIk6JzyiOapECEiQ1iGkXSnMAfnebUr8M7qnHj3QWTJUcM+B/AkjbhHcmaMhChlmxAgKJnFFbEJ7nw9wNzMC43oz/JdAQOzSJ0KNh2s8Wn31NMqj3VT0KGSo0oB1bwbGybW3YCd7DtLN1e66yYWw/NkfGtFkFkpXIDxn1I/dXEN0pR8+tRuhY/I4fzStEFtaLgubXlVqY2Ji/0cp1KLnxbBOseVDpoI8RY9WEdDJNcUuOVaIRTUhgr+g1lBHeD/CN/Uh7bU93138Zurd7UoA6cHHAZi6tsgyAgWiGQI6VFslhxO8yXf8XI/DvvHJxBkdipRtnW5SunKh+Muar8T2SthQ9kJoFlN7YdrlWxxHjP4Db9QnCxndKrpc/sO8Sg/yEwYmTQYHh6gJ2b6KRetv9GBTgG9ju9NrdbUqeMxqnEg7q79TobZyl7fa0vXWAbMfZJQrPt4Ep9woh7wYn3HmXPdGQfoklLHQ501HKC9WAeKhmnfuL8dCbWFcI62TH+BO4eO/6yXIPePaHUPC4/gdMX41TmyW/RG+Iaw+CDtRGgPIr1S6V7JRl0RUiBLu6qLqxXUWr0/6jaLJS+j+p0sesFH82bqu9veXcupirAQBM3J2BSGr46wiAjS5jzQfZHkCLOtMgRhbFCSTmRRXGu7TxSE8Z/puF3o8eM0Ge21ZJL375zlenrKPj0F+FFbwdqCasijJzqmM4/gR3ZCscVJJwJnuCCNmRc8dnDlgbtmMHFZyxXu/99r+VsCKkzUxgdZWu43RmSouXrVC/8yTHTyZAZ+Q6i0OlVeYrRFHoODxWtTTzDJ71+2o7XbuU1SU9azwtBQJXOtndpg7lHESn4v+o9eg/RaB3canjHOPPZwSHyjBNbMoQJJOl9ShhuEm0PxKKp92/yQB8pKV2k1bs0oSvC2ZTNqaB0IT0AzLKepy92vVb2XZQ8xynTXxwPylWJLAyB5YODwcqtahKWdMcyYt0RKGm55ZGnUhHmKarh0wC7Fa3+0Q7Z6DoeTZKDZq9W+F44wM5uSvp5GEPvfPpWSHmQ/Z71Pwc3J+nh1okbmvUb4+Pq6oFx4CDhMjkgS8J0cxywvq8tvI6f1Q9/4BXNjySTc0okEXyu62nDf3Cyt1+DSoaQsIWpS/i7Fh2dv1QkPvQhEf6eH3If5elbgNVqdWQuonwg3f5tKJ+7Q6EHobVEk+wOZoFqmzA67bPRfu3+i07mTKEATBssWmHJwml2VAlykxs37QzePnK8zADTbs24MCM1SuG0MuU6VyzdJ0XPTi/Ipzd15zPy2VfSt36q7rtavT6U0D3OgK66sXkOr4NAcrRvn/booA/pcKmTcnqqBP9Unel6zDl3n2MfOcFlpV2XXlX2g/8YSqyTh/9f/EfkzZRRRD8rwUQ1DEP7ExrFHtaLo8SoMWnNUmZS8UNpxyNe6MS8WxuaV+QbYV+tUALimYfcYRcik85BYjziRKr/2OdWhB/+/Nrsbf0/CQmwvObFKnzwVP1N45JIrC9FtDGZAjRD+yZZ0Dmn0H3B6O4UjpyOJpLGWYMl4Z3hPGLwQ4ej9BZV1mOWqLedks7uyDVK/RXP+PAcRI3SlfxWeJmrBpUwQOYKzEfUachjc3WxTk2X8pUAzNWHDe2ObbDSZikBTDhPn/aig2nN1SYe2cSd6iGEpl+I9JApro085ASN8KLQD/feAxTHka3hCc8iKDFMB5rUUlIGjaOzyLeuh1CQ4Y3wmgikH/rFWEvIIEo2ijKDs//0z6Wyio7YUNiDsYPMkv5oFBrxUQWZhmfpDiMXptGpuOl7+lBSRIyi0pODbgqzelIu8DKydAUWy31Q2ay5TsVK7Tj78KtIfB71w2o1zUq7h1ajukESLSZoS1XvZALTuOkRbJckNR4rIEAZLmF9VBaf7Y8z0kpqzwfheHes4dbmV/mvSM6x5ifuYPWP2pG/jutrl2XqQoWC+DjKNeew9FZok1REf16oyO7OoHpoO05pHk8r/KhAMBkQXZq+0t5Avc3afw+k2GX7gSHzmdQUJmU7Y7g/drVUO8StZHgMZRcDS2keQCWX2ZSYbyByAMpJgWNVvI9HYJm5KBb0HPSrFQScLKfxG260eQ9pGrMPtSUoOjY+cPzpIgxfEWcPnBfhABByLSR4cm45aqNzaIGtDt74RVtLycMsHirmEpD5u6g0QCNBpnocJGfqP6odta+XFBFVTOG6EZvQoF4m/I5/bTWRmgkVNEkrR7puRN+Qu7ppmrfvXLNXhIFpkBBW4yFqZ/MQ3xPx37jGETBLh0A6uU7d97kLkqjSfrfKZpLmAz+Ri4G3tukR9bSustNIIBWilUmq7+m9BiXA+q0NFpaH0qRmmOS5jFIz+AMGKd+/r23vzkPagYJC0TaQxMHvPmWSJ1lCcbweLRLhGESraodbGJsxL1wv6rBYipW3iCTGrWvczIypzA6YDmbcbnTs5wWNU1bH2aM1nlPHm//j1Ec4h8GU3SUvUf2vpTsseds23LygI3D2RSHXMOurl499bCWgVE6G7Zr5uf5cXCh/R6ovJsgE8yzS2geyIwwtxZmRWe09Jw60FLgH0LIN/QH0Z2V3ztNPwNXY3QtgzfBbXAKdV1GxAdK0CvoJOhYEUQrcX4DgMEJsNSb3u1y5rHgq7Ckwo9lzS4dmHM8HxvfZVXhL6x1DltHdY4btdDRJ6yZ6+OsAsTCD/3LZyKfL+uFUuEA0WVZPJrbAzSghF6mk+bnlzgVB/7yRmTYXkZMgEwva1CgjoULxaVn+mvyYgCJUkzRUVL0bxgBjrpsYan6vy3fbc0/Veg1IipefrYEdGiWzAHUMdcynazmEn22p9CNODkxWQA3G5zmDXauOtSRHSqsVgecopsgjMOAL8zd2ls+5GKlRIBuwPyFEkAB30dl4RCYFhuqKfze+XmZRz/2ShdVEaYQORg6hkgMm0h65cSDo5PNjRFzjQVTxlnxoZjjnMOeHYpgKMx2+OBVJqM1pBRlQYiWpzO29ZvA1qpj5yVuJykyj1YLFry3ajeRc2z1Yi0LizdE3v5wwd/suhu7xbtfZFmO8SjyUmA0ERCZVt/5G31kAjGfbX6VS0aGgJ9790lmpWQxBBydWH6yPHkQbvwRDBo3INPlLb+oGOYt8rn4PDsp4bv0YeW21iKpx8mR9lNdqNwY2qLXtfctHCzswEtL4JsQXjiKokFX+B9iBCC1EDIObWIBX5PhsJKRG9dGtnrKAdLTeEQF8xPz2r08Bk+fULoH2md0gJNq5DeBxxdAZCyzEUXGMwm9DnWkj0/tOzbHx+00w9g3ETkaCUfqwlHEFn87bSy5A3jKf3rpRUStCyxg0FxUhjCNpoGnh48ZsI8YQKrB0ncJWgC9ci10fVMbobybAyeAn63QgKOAtxgsBjb1ElelquS7y8LXUAqjPSIdDnG7DkEP2CNOXbLRjlQykKDpMCf3x4Cc/qvdL3xemBugPH4TtzDbW0rXP7GDG7Gf8HrWlkHklqn+CRmENKJlI/Ozk4rGFkiFopemaE/oue3sXV6PZmSUsoyxyQSxPUQWU81F+pjRJ5e5zxZAGu1HAx64muXL41Zui5mmkddJYaTUG0Ykm1ZtD1CdcewQD84FBxjWHDLjhQyI4zXIOcZ0o69qPTydXZxFB+tBHsW7Qmc7/faJEGS0+R8VgYuyQZ2yyT3EAVEornP3vCK03hv2tmgM5KDqyK4QrBOeWOzbLb4CYRk+2nxwi9seGqzw2Az5n00RfR2jdtfDhm1Xg/gNloGpTUMPn4XErfaSbVbzs8e8m9iW46Lv9g/JmJIQKYNxtOm1PsXsrjsLUlvtaVMln0uTcPrxnLb9qaEQ8qTwrK1c6hZ5ErnD5EdZUZ+EsUcf++5MltcGdbopx4EsqCpK7VQeYyvFYbwmNkDR+e1vnnWliv2ZOXcFLbi9PdG6AHIPO0sR7xbS5dOGhFNZiRnV2Nrl3d+7bolYjoW5DzhQQgt8Y/akL1kS50OZ60bNh/NdsMIM9RZ1o8memgfm14HMSjKCM9wclzx39DW1LCSounaE90wSh1C5VP+ftLmmWIcZ8A4esErxhNaOAH+ahQ3azT3kLOqO7T2tozO/lqT3/xZ5DQUKmNqxgGeT/y47eR5fw+UlfG1jvKxdXI3ImkVag6mDOb0KAYefrm8Ow3ljtXClpJNlfT1u1IXKAKh/s5TcfXiaKSLN3df/Ct+axg78SbM/WLbunv2fYpV+L7dg0tTuZ/ptwIrkmtUIZUiDjONyZPynU0iTiWFb0jklWgZJI4TTfNMv4sXUAlSayeQgIU14k0EOpmH/20/YGwFHhCwrxfn5OgkCkiQKqTMjdI0xCxMrP29DwQ0V4bQX/07u9RfMdvk6HiUv+rnrQ1s1BTTR7Fnc14lx4AEgOsiJufUvXslLMImVm0QbFWaGnOroaCOnz+3xiNu/jpf4BorQ9sHWMq1ovEWf7vNWMqQ3ukOWSZJ58h4ExwRdlXvRGAHIFlK3FEmqCs/655hehTsWQFdiB0HyrKtLJmu4MpeBt1e153lacpI9SQNASKHErPC5RfvsbvLJ+2ztbzCA594J5FLUvx8O4+JK/lqg+qOOEMVO0VLhja5n7QKTcN33Ct96FlO8AaDsqKmPl/AsylnEaCj2Q8KsgCSm0suRg3u2pNQL8ONxxD5+AVaqK305idiGWBzacVdK6nNUTkDrQaibqg24iTjFcitZHwI0my8BRr7VTvLY+iGxwGCGHkul4NcjlV5KamYY4uybGtFewb8LefqZkZFnVCzWteS2c/vw1ftXCRoMGjeTZFRsin95Rpahxt42mS6/may4zSFnkJR0tADjYMar06/dZfMmKt/DZVLdjbogbAoSBR586PFRupTHUIbWbc4OC9aPjpqkUw0aElTCx7ieh6E/dZ9aoEPaS46SQCRztsQlO1/LDyC5RWkh8FYqLP96KJZT+ymnVDjxr/wskR9p+o5nwuzTIyuDBNRXspWi8Jg2TT572Cfsmv0u/cXWqm4yvc7LGpFIiB/f+ZE7h6vLH52/VlMlERWCyikQXOUmG6PuQTO6tYMzRBjC4mSPrKKHPIkWTujKwx/nZ+IL541FD3iX827awK4s9eGADYlflHHkHr6sgLx00d+8FKASdyN1SbGs7CsEyeTl5kVVwRdyj9RDZ8kjQ4rEAeRP/sq0uSvZbxoclOwf6oR6lOAC9NZ25APhJCF/cO6kkPUWdhXTCphkts4tq1P6pKO+p4EPB0TgTJ/HDX1+h/a8fQHziVYpTAvOLsqc4TCdIcdKQNO4FtDCZzBHQfu6isT9lTnDVDLEoZF6J29bu2HdpmwLfYEax8CQTI0atA3SwXN0M5/TT1BAjP8y+lSPmtda4HA7ohvAWjx0k/sg/hpFmPx7vhQVbMeuZn9QaF1TXFrJO1S0aWnJXMcVkP6XiwlCtJodJPq96n5FBtrJt0Wy1j4goLuwoyg5R97NW8oRL9OxQsQtujsR3uWkyvH5vgsVEiAc4COOIMlyVcEzWGGZ2nIm/Y2/U85DxvVBpD78nWmWnJUoSwloz4h1qhpUIh6gcYxflX3i55Djjtgkiz1wQj/HHyVn8A07v87SR7AZcudpPOARvPtJa4Ladb+HRtGxe61N7EznULoJWiRim/Bo79ehRMkq+6frkXQyLc1ZaArNB3GXXMi1YXG0a2RdErE8y8Z6OqctTNS5qiox1cgK/bpY8lmeJU5Kl3hHoaeqlUJCb7u7TFlpvhNS54swC4XkyuyjQgFLfVI2KOsJtsARgIP5SJ9g/AHmxLr0bfriPOqKdplsIdxQhdUAocV1tQ8cDpxe+Mxb07F5vEPh/0qrlZaFD/JbVLnh1TPlhjxLfpnIylf8K54nAOYaDPF6bLUcs7iWHm/5w1piLKhNGpqk/vl/Z2i2ZkrAQEgXxfF02PTFNpLezd+LgCMK5LYQcoAGDZEG1UEyd+tJmBkqp/FuVloI5CdgHJAq4VhebbRdRwozB4d2zPBG0a0u4Kt/olnWdSfpXj0QjanItWCVaxYQbpnJEM7qVwmvtn3Bh/xb/11SN9CzWbbYrUM9T799cWUaBZ5w0XIXI/o+Zrx+pqrxHO4UeYZF1tXw43rUUzvqiJJxiQvryVbZfevmyQsua1N0Apg03eOjRNHHYbdOJW/X0soQo2kvI9YtcjI1QQtI35VdcmzmvTqCaFdTV8RXoe+liVeEeo+OjiAANPzv5+rUtuU7azJqgwgyjX0i9TmCIMAm2Wvj7Ayv988um9xompOzKvp2haZxsLwrrw0M0383ROlEcGiYypdfJCEiNrRsfZF0mqW0J6CxRjyd/e+yda1hr0IbpxOvYMe2Jv4RiQ5LqqiFcqDeUGjQ3wHMBcH2qISRheao+ZU7o" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV0AX49S4XPonzyGHDwZs-R_VWAKpvYG8_cXVgn-Lz---VSONzQ1&amp;t=636256279320000000" type="text/javascript"></script>


<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bAjaxControlToolkit%2c+Version%3d4.1.50927.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d28f01b0e84b6d53e%3aen-US%3ac56b20f0-b89f-420b-94a3-356adf3cc133%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=492" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=492" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=492" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=492" type="text/javascript"></script><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=492" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=492" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=492" type="text/javascript"></script><script src="/js/dnncore.js?cdv=492" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=492" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=492)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=492)-->













<!--CDF(Javascript|http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=492)-->





<!--CDF(Javascript|http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=492)-->



<!--CDF(Javascript|http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=492)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/product/tabid/61/productId/5336/Default.aspx" >We Believe in Miracles Project</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/TheChildrenareOurFuture.aspx" >The Children are Our Future</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/MediaSupport.aspx" >Media Support</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/HappySummer.aspx" >Happy Summer</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Superfood.aspx" >Superfood</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News site and governs data collection and usage.
        By using the Israel Today | Israel News site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply1@israeltoday.co.il">donotreply1@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2017 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    </form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=492)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=492)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=492)--><!--CDF(Css|/Portals/_default/Skins/ITD2/skin.css?cdv=492)--><!--CDF(Javascript|/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=492)--><!--CDF(Javascript|/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=492)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=492)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=492)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=492)-->
    
</body>
</html>