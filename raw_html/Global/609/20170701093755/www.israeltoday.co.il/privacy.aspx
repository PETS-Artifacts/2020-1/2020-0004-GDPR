<!DOCTYPE html>
<html  lang="en-US">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2016, by DNN Corporation -->
<!--*********************************************-->
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem,DotNetNuke,DNN" /><meta id="MetaGenerator" name="GENERATOR" content="DotNetNuke " /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=492" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=492" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=492" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=492" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=492" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' /><meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ZyroH0zwxCsPDT1Hh68l0kOt0IjArHb43qof4dcAVMVhuU4yfmhnaJQIQ07V4jfLDzCYm4qr271idTG/GyY9nXln3NGJnCuyDMht0+h27IDnX5zhKHBgzdw9Y5GP+srWHthuAiBWDUTf99RCw0RZaNw2wZ//X3DrtzLMDGojHjFQSBpsToHeZAsNW344ac722O9ZsVTeHci99zPZld+Hs+ND256aGE6GLawduQLELN6+pXQKii7uUlh1x3/PRLgc2kig1c4U78s4KhMvMRdvms6D6iueEZzn9nm6yWZ3P2qGz4h1UD+Vn/qggnvYSX7qq7UjTwbVY1FzHDLbaMr7ObGjTVLUZkGpTPNgz6d/1yypISyfKAEKnxzrbnP4buDK9l6fopnQFNRdAHuGq8bDa6aONwSScDe38k5A41RkenkjpwTlB4bGLVZ/M+qEXz+iJOTCxXLk9e3kZL7HvqSvo2S0Qqap6EH3Qjos90PqI73Ss+0ngIFsqEjDsGlQW5j3oxzqt2cwedR2YBu1zX3J43hHH06t6ExtI+EM7LgixkLw4Z0TdZh+JIx6AfGYDmB41eTlb9QiD/nYo9XErhtGUTEEzgO+0X4IfxXRIcTBHJk8tFIuRM08WKf2KnE6dDISs9QxZvcyvXrlbwg7cWRg9scAnUdt3OSVOrEzwXa8EAM6snbmQcBjSnnniAGDMdRBHRY9l+EbOxljKuA5rh+Tdkbrv3QOXeEqzbr0IYKRZf0FkDl0OZRgAAHHB24cdK5ayOoDm5Wm8QG/SL2CUQf/EDhnQUC7tjEOaAqGPEsXtiIZnDT3SQCxyebSKTbDdpWcKxYJEyACVZKSewxLMAu1w9TNyfZscpHXi2l1gh1rWPQhvzYi0/fNHMoDClJvsIYLl93kSegMuyuoAKucKADR70jzyla+Bb5jypmctGulYYz4jcrzB73fETHFq2ndDc3RQcHpfBMKc1M9ATxmRImeA2RyRicIN9/IIsUx+H9Na+Fbh+hz/nV198k4hWyT6uCabvwWLMtjIkS1Y2eMLaVaqZPTRMhVJrbno7Vr3ry/M4Cq5ZsbF7LxWI5d8+PsYRltsATgkIfHOWeD9J23NCygpxHD//JJ6I5xX/a6RsQ4V569q/Q6I9GEHgHKqCilk1K8Ljq5gD3RqnGlgnfvIGjpGBSElswq5aT4p1BxI8BnLz8gGwRdIQmvCohZTS6dgbvMcnk/vGbFHMjnW2o980ycvFpz0kOK376cDXoeaPY46AtT+99zN3vBp5SVQzhWCWa8NHTCB0QzOAlQ394fDMbzfjmS7O0KicAB3SJfl509xWNR/H0mt1NV4XJig5jaU6TFdkNSKUQ7dkr8sS/r8N/s0wBbBhRYOqLdhX3w3ylK2aDRygoX3OhZJdQGSBkT39IXkGaDsmFiltwJetkqN73ZKxahf11/lJlceTfvenEQpKCwmQeyuadliaSIxGwgH5xhnYJQ4KExgHzDCqRRSgSyeeorbVQjsiSpmGVSdy/0INlXwbDws4W99rS0UzcWhWIi6elpFYEsi+zXDEwMCW8BYTO6p8QJolkNrb0gVZb7k5OZ/aFi3r35ikwDnvF2JN7RnduCcA9Io/R3LmW5rTTgbOdwTtXhroLcOmbLA6iVXCzT2yTyJ6IrY+H4zVeUyTn0MKWs/Oh+2WAJcRnooqIbHZenCwqayEN9mk2YXlZ+4bdRHyTl2H4PMMiOpXC0K8Ad9dfDaKlhGnt2gY2cfnqk77PvKV+N26gKfVKDAcsSqEbjfueh7GHM1/LG1yEXXnxVhCHXCg5P2ereFCWOck4tAC98B7Mu50k8DlVSnvNFC6R0fisLGWPDgXG25QxlUmSw2W6w6qR6r4v8riuIGUhAigwolM7dbztWRFqVWPDXjQwZ0107Sy6TXhaM6reCOqCEzGxsb+RXohQvXTgbA+Ge6aSkTBq0F2WJy4h0rb5C0vlinttIbiFkTe88OtCT6xCF2uy3PbXqOpdKSibz0FDZzp1yOKUkqMsS3u2O563PqWqWd7oje5UBcFNwLSw2XQQYc9YedTF5YvQTlpOIBklbW+qyaGmiIJ6lcBGsk5W4y1CEiZJE0wgqIYjW/l3rKyyEosxthcifcjCDXWO0OnaMOIE7B4vBN7H8JRbuNxsUfQ5l24lP8MxbhuAT5rCxNFxczsd787SNj+d4f0iUlvXVHVM72coEqkSCpFN/2YQ45mm2aZFZHNiHSqUqmEjmU+LVE4WyneP2/CgvwyCdhuFaTQO0LEwYOo1veiE2YEUOagCAq/wkcMvIZUGfeT7K4wsOE/sdgfn9GfasRdQGEiM7MOpO3UPX/iN+pxh54ULiPhTGRm1OBG+hoYmLreJikbOnKIvbfyDGam9pjw+ENw+u412jH1tJ4kRCuk+cIXp0gg8Ca07a9wyiQTsA9uYo6soPS/B0tQAqpNOqTzPFphE4wFdoOSWqJn2dgXBjRyqyGXZfIW4rVZ//6bBHdkUtGqcQVS91hxG3TAat1UEPoN6tTF0NB5Pv22CMuSZK6U6ZTB38GaGYG+xboH7KMo/IAP+nvkRLKYEORiz2XPeVMzji/VNxK9kdMLqMiGm2Ox2+5h9WVFTubipxOo/riIxdSj4DSaYhf2fvYi8w+OpHDApHuzoXCRkCy9dDIW9jJE41F3BuhvLSbMxd3AV9dmy89UcLrQxd+hMIgoKUvKqY1gJD9KflCEQcbWA5AJDWsn926NZBEdsnvC7PUlXBjozF6BqMZ2Y1SxdUpGRuKCEv24q8iEeZgQKJ+7qFcJMYVGOJ25L97nARKhcTORZh0C6UXi/G1/OAM0kdakVqkfycl94x7eGDMM0y45i3iN2ScpUP/jUTSk1Cg3KGOqrZAohHEPcKoTcak27p9B3wln6Q2hF+OKuDQG7V+/9iTrUWTBBil+2rAdUbskladVHeWrtzQ4eMuP9GJfusZ1GfbY3rs0915SXXuIAbiin3KpHqdne8zCKfvgToJ+UNrICK85os5/oIMhPtBa9i17V2tOsvzm2EWXM1g+Cnv/ikGuO56K9vrRJr0a1GVLYuRSsdnhuD7nBGYJqxccGD+P0pa3AODpDoJ3Fpf0SFUO5Mj0shnWXd0nKjE0VxJeOXVXAg735k79REb2rtHgIA+9V9dypG+HWRrpLRkkIx06y+MFMiz6/PdzWMpVwKuQP8fnrChXZEILT15WqAi2qwUt+iM+2cadFWNSXhheFKOjvsZmhqtLiAsR2zfNEgFkYuY0tiPZTrSPzFZWvFtvC0Pq7vcqKS2Am396/EdMpHodsIXj2WGPS9SO0Tj8UHouY3P3BFBMiKtHd/aaTLVEu7QgaLNZW+oIvgq+aJDVnWxoeMbOxAqlx+CRSdJID0vR+6ac5NSPhZHrYKfeJoD0uZYCAQ4CgZdpDkDI47bEYN63jl0sEQqIUKqKobRBlh07DdM4RgvD6E9yivadS0t7wzPA1YCJcvCqfBWJC7jM1hiWzy9Ly9IUJ25lvolR71Hs0oj22sq8jt00Eyz0jFdzOo3nSykwNURuqvzibih4sAIWz6lEr+7dGxdCoT7hiykuzhq2jlXtjZS/WTeLT1WDnCgUWNmsklHv4fuu1a8oeRqggs1cCn74b1t6IK20zFj4TKgOAZtY1ddi/6fcGWjdVIEOgKoY21Rxf2MFucf/Ulr0iaFX8yKI4eYFMsYsRsguY0430nTamnG77+zsBG1eOMa2vofZkfa0kR1sarbFXcFA27vdVtNNKhI2SWLuQz1YSYgwvdKJOTUTpS657bZdHIDaQMf7eLLfd/5hhyVsRKbW4cPH/trkOAdQ8GqrJecffuZwGVxBe7dr/entG4M3z+Lp7nr9XBe0lgOdXDx+IVIMAz0QqIbewIAiCUA/ugbGPKzS3cZ0YCDG5tqCAutIEw24/zVtJiDg1EYA0yZiPAYQdWISRpbJMBVBpg2XOqYN3JSEAGNqChZgCzAKnfTSDqqPFGUtUzwyd8Bwgfv4B++wDLQeHq4CxJQlfMErVUz0+d4vhgp0ShHnBe2NBCsp3XGdlHyr/268X39Xlpx/2N+jakkuQIfB20yN9hwJTdch1YCq42e+r4MrXIQLYswoChc8vMKcb2erTwEqo8s3Zt+SfkAqO8wZi5z68J7GPdoGmibgP4efn8TnohbZEurE9sGdcXegDphCLSj3mIVLo5qRHKKuV7MfDktJ7OXklznPJJey9RpQIVLZHEX2bScxp4Bu/JUr+p78c5/F1+1m/Iu9oW8c52TCRbDcLnIcvx+Y3RKFVkNr2Z/TmJ8qbMFY2g7aY7xa88Z/Pk82wWzhT7mT0fknT15xmmKJenP4jZiL4yuXJRUUsSjWMri7zM6+o/ZAL9cHyF0Nw1W0jUFnxSdZcXwJfr3LJXNCYWFJqWGJJoZIZZZkpP6f0IktTWo1Wx3funBHuxWgiOEayqDn4VmXOVFqmhMlrUOwrSEL4phLGO8R/T5cNQpkogMlVsZRuBjWc2Wa9FhkCgO5dk8aQA1mrMeRzoE7vrJGQnIt+igJlzWGVCyXrRsri5ZzXQHFFD10DASzbIztKm6fTJ2te6Y10bQ/4ecoOPTWml6VySddLDBOY5ormNhlvyiiXOzZmvj3a++qz332p1g16Bapp7EZdu5gmG292hA8MEGWb7VCIjYhqlzQuoZ8Aq8j0dxjLC9ZMXtfBbI4jsIlae9CpTAzEFnjnaxcOyqrwZ+VTNDaPNZyUfcIBFqb4LvMbsd9n/QbpdX5FH6ffVHDwUyrc5lfTAkjUmc0j1DH4c2pUcYq35EssQkZTbiDNNLjae2/3v89THopkm3s4+o4kvRkgF07ceriyGpI6gB1kBmw6IOcaAiw8qWiIpKIDr6GPRvRDfXEFoTnmZTmJCzxK9G7NIm0XCA686IQhVIEVa8j08nxoQHXraAn3QOBz5JkzhfsUxLy98IT2L6sEwgYYzQ5E0YfaQ4FS/m4o2Gyzs0KKBrytyrjxln4YTfPomPZbG3moHUPhGY5IATkvPbGjPjfG2ozTSJ5MAFMwcYyl+JGIVGR132gwS6zzBmV0383ZBQskNcgw3pGxz7aSywsEiMM0+gSe7WeBQC2Fiu0ugIcKfa+nGAXOUiSGiCBm9cD0fGKbZ9B5hWqmClpOQsRIzy0+C4zhDLPo3JFQx13INskzv7QGFSJwH0k0uhXuwgRH+slDAlw5PvHdjDJTlTm0G9YJRooZIhQtzSmrCXvVRGoQVFJoiLgUIjZt0FxzOo6fsXPTFrz3zUhO3dbZOIrkvoXDMSeEEkG/JsUfMC7epPIMji6kDpfK3RuTEiG5MU7KHfkr3eFbeadKsGBUKlZ3yhBw5PSDl74doe3xvcqZvrXxmINBBga3rgMm+qz1HySBmqRH/HgTiaQTitbhwkBVyY7YKWwqQdkgzqmf0X8hZMHGhiBgUwE08tXP9G83Ur+dJV0ItqtkqauckYwnzQEozF4Aym/7IJpE7cLaLiNTWSDkinkkk1XJ/nGmA18KF9UHacvhn9OyJwTCnMzn+DteNSWbCJbJZMgbnJK7dLK9Qs6xQ9w03WKDiwQd8+zH+8hmQZRnv9cOva/VU9iU0MzklauWNY8pkFDUhjrb6NnsKe7ZjeL1z+pDB8YpVJHkHzcXMeubwLKKvrnWVoSSWGiWNQ6V7Y4Gi9BhZYjdFGVz2ef41ngZUWqnN5rUpIau3L6e2N2C30YXJzkXimjNgTn1Wt+Tc+pSUC0v5iS9rxFLBi9d8sQZAhHHylDEuMDih2+hnoWfl36Beyvfs9zRx6AWp6BRkamzchjLu2f/GTMcr2BhD98iQzHw18kDWap4RI950hKyXxC2WWrEx2MKfjTOsYP6sPrsa7uh0CBRINuU0DQgdQhs5IUK3Asg+fC6eRcB9HdhPTqfY7ar1H5v47IZh67ynGzqpmc7s3Omo9vp4ENGyb+rMGkwhXMxbI38ibsaG/Qv9ATHju656ggYO/asPGJdanlAW0cYhZ6xdpJLfuuMJbfvSzAaJQOAGU2WWzbnAi5iAjWGjOf/Rj8Zuw4FBcvrQ9ng5dEfXrZpArxUGVfvll1W5Amrt+ZzwMOsGTC8HIpbH7fNOtdnbP6qHIXisjrU0akiN/UOlTWEYJ4dO+M0mXuCRQkPq6eRllNQjGil2G16WUOcCV87KKKX8J/xb6Lf7hnXpn3DBnRMFymXy40+0yPvczcDRJf8SLdxvTl4kS++Yjp6SSAr2VSCwwN7Kcx/4hrRxeroWiMMHN9bLcUC+dGgqmiIqca63BcKbWdBh+7aWosDclTqqsPyRaht6H4CWtgddXD6uA97PQOMxSCdefsllr2qecPbrqVwWzy6bGweBHSW9ApkB/kZpQV1P0hXjuuTLUcsfvS+Dt+wqvr7KCuyEhS2eLKtcHfg24WBGnEPaGhkVOx6pmcBTcJPcLzs98oTqG3f5hTKPjLMNZUbUeS3YMSK0v5NAnzFksWiCDYl+X+B30IxTPBAV+v+38teB/XYDgiiictJzbn6Orxxptr4OaRxn6di2vKFSnS+8q8Im7KppR8wu+pEhupkXm78m/bWPv/SSYJVJBuKbIemOJuCRusQ+unW1H3BnEAD4ZZwp7fTTUUTmqzsxYfm0UYmR9Ijmv/sm9ET2fpY8WZ7dyk4Cjs6mbKZMiYceDDKNVLtZgMm3s6YboXDJnv+aXiCkIX9/vMR9EBREVNB8rCVrnsJZn2NNhtCKyaxMZ70th2bGDUz3BgR4SiVBnNIcfDfPyCOt5ziPuEhyDVydGVE2Q/fsuuuz1FIuvn1UwIb5FdXtt0cIjTf67KTeNFWwI9scedb5k/NmEaCi3X4zqN3tFPK945ACFcpqWNN6tkfDfWbvC2GXZb2Tz2YBCDRizbtgeXAcHWiPXTU+Doin3fT0GR6SF9lwj/3FJL3TmJWbxj0ZLIm63pdvvVYa+/iXgxa9u9tPCPiaGPKSxW7rkX3+bpabHD5DlBgFL0FGRkcmowlN0dvXo+Nr7AafbIm0ZWIf79hhJweSlnVaIPHKoYfY/bOXIALIQ5jtKbUj+eIerxF6wMU4hN2jCz3Qt7nh0tgm4Ym9VxhWRKJqmXzSFiceDWLTqYN1rWW1HOpmDpdfIdIy2lM/U1w8bYHIpfOWFJKZdk3gx8ejfqwOr+vckJBuHHP9CESG2Bp8lpW/wibKW9xNSEtnor85+TElwQ478LUrnOtFyCGNHs7ADLzV95h4lVdYdlcpdZYgymYN1YPvKloBgRcJt+rmVABscaeWihJidLWb2HbnGipYCkHv4ensQw4nGI+KG20iKq20oGFKsxbCgV+3nizdk6/HKZkylBy4pZ7TjCMVPL/pRgKkLm4g6ZGbF4Xfbilnbv5QtMF2KqPXdiNhQ/tXDnYM9bOpYRfDX0pcHbJU9ITBVZc9vdA92ELuZ4ci6hTJ80JQNd14KfGAA/RH6YzaumTQrl19nYl3/mxH07JOXzTpJc4mF/YVqeK5DaavF5Pbz/O5fxEio3EgTiYTbkkfduqZ5qKjdw1XFNS8C+oC1OfoEekrNFFsEsH3dhfasljik43uGq65pszLNW9C+2z78WZ3+l76AHdSs+CkPRJh0sqStRgyTry21kgFRUmFoXr5G4ojk2AsFQH+6Dgo7o8qUgpr1zaFphFMlJg6hIgsFL8wmYgtvQPCngU5JRiaeFC08BhKnPaGFmOmHztaU1HcbIiEPe1OJQmepTzP/j6/KVZAGviLmfIk+6jle6Y/AQ8I/nT3bX9DBv+8kBcLVdtjJnDjxIQuZTRApYZ773omjLf8npzEO7Fa0hid3bmbSnuVpChOnDuPDVjTiM9LRsVouCgCteVPcJScaX+vZUtVbcoR2LF5NXsWJH9IC5DwIFcRT2ODVY+6yYNlAVIHknwkX/JUumv3OPEKZuPGOKoYrJHg76/iIVL6W1ENrwsuwX61vPdqNfOLl8/cRIpnVQ5lfa2eAXcC4AKUNAQX2UPU0Q8rbAFUwmK2K4SpPqNm4Up68dHZQYmSEZ2fE+tu9jws/nWAMJupWryJNy8YFUUkrxrxUctRRGMcVjNaT1zXFvT09IT1QrvR4sQ1xmAPAoa5QL5fxwYyOkmihHsuj8NOGiMZ4KgjLE9j6d3p3FUQ4TI6DD2iIylvI+zA4FoRhp3qTmwQDwlxcJG/b4gGu9rwXFMZm/3WBQAjLjkhywTjfJoaXtfYeAc5V0f/AIHdFWRiDGkZ61ABXEH4hOfv+QIn+0jP/Ssrl676hqtH9db7ugwW77A95E6cznbfZOsu5hZuNvBqj87TRBdoWtHf3N6Qzb6kBey2SHdN8F5jtZ3mDD005fsko1EQU3RcbFaCeUWRWWCTZAEpL8m9Xz0CmCFMBs0k9DV2L2/CLJj4mrPI7JnxucQ91s9VEsxWDwG0yOAJ/MJNhW7UodmSwMenwiNhXbQmeZLXGPA4ABwZ8tOmKz2k9rDGAZIuNRDAeuWxlOgfP6wlpnehvsmuMbFGmUNSzqw936FRCaDxjTVTHh7eEQNvzTi1/Qmj7V1B3d6bjOvVXf5SgJj6T+bwqVjHP45CQG/6jq8lNEowqipwWS5PFNVUVyuT+LiiUY6eaXut0lfjy0c9umre2hzjlKqzf2vgQo914FfNgb+eVLKl5meCHhWhHhFdBkpJsNFsFM68gV5qrsO8pMkkBK6T5UiZSa8+9cwRw5LOIfUyfgE1mzaeNIbcvXIDcjuggiv/tkScaYSBJbIaRUBSxUcZRg4sZy25J6XmtCvMyva2UL4MDL3PPY0JrgjQO+vKkcp8ec8BEbo+n5YWsNOWk+lvCzY7ayijYP/zKDH594gdKN1+pVM32aZg3zMW3BQafG34LF//ZC9CqBmbaR5rmmKh6Yy+4P3xZznFDGMpnukCxBEe0QpZ/2KLn5jizG0Nyiyn2LlIFvKcL246H7sG15NH/kSppPumwXSgh/qTqScenz8B3vrNQpkJ714/7J1Pc7DbLSiySWgoKbo24RtNgPJ0Uu998tsTn8+KNRactaEMJK5zHe6wmu4B8m5uPVvdWlVza3k1YJjyKeRlTKFsSkPspDjwLPJ7b3fleiPZ6f04NxrLMhO1EMEcrUzmA7xHrWrweY0HgZ1DtA9ue59IflDlj7alClTpiS5dlsfxnVfViwFsoX5rpQcfDB9VfuLAPaMws3AAvB2znuOKK4iDas+FoGrg9aIyNPu5RSCQ05fH2VgRido9o/vwHtmEWr7PCDblB+5vvpd+GAQA6RXXV2lCZQutuSq8qTMTMHxJyrjZPtw5n3HkCA7gzxWZh3T2HkiPSZQPi/3MJlF8vbCL529Ne5Vk1LBrjfGXYiLRq8X03qkYZNgsaPpQTSVrfdAzFsQv3lKOZFXfnv6Xq9D4mqMRqbqOpzwo52WxclUJk4tyHPSLg+Tq3S2bvoK4jYJMjNgVpI0d42iXWDiqchRQDeNRylLq+klWmgETvA84LGj4oE6IsWaBLOFKKhG1yXWw96HEJrfKH8G5hukOK0Y5CACga9TiSQCC/Qu+PhptkCgxtB92vj+/DAxiaAn4K4nQMyXyaDUXPKSy9lumJGNwj7QVUHLRucaTqqnkPEZeq8kIeyLA9uzamZw6vxYGWQ8aA4B/1HYUpErPthUHFGB4MY2tJVLSpwS0Quk05N+1N5EkTUtPDZGMF8PIZz8TGRt9veUu7hP/5Vdwt0kvqo/P+42pB8r72ePHh/rCd1rW4qWWV/uL/ckQVwWtgEd9nWfWAKBozNFveCOvX5be2Yyz46BnEmVky+cuqGASJh3hF8UarmhSJJL4Izo+3ahHTNbXVD4UyVl4/MNyX31nbSNsGA1rsqN7lOaysEq+hOPpSM1/4/Lih6iWO+V048ZcKCJ2/cEm0K6sW2DtbqqqbcQbN9phzXH6HzDlRoZ6vHt/ZFyI2UyyaRBN/Am5gvTFTSD2Lkp4hSZFrA4ZJkV+AENXDxEk/4JnHWbwthDX7cVXdHYLyCdFhw2gFgosZsEJ9ZNivRRk6+oXuuIMMXZE5dLepkw8/lda0p3KgC/vFtzdrIyEUoQKHAzOYxX72I+d906d7KpICOPXnakN0X6ypa1yOU3OWTsa/dW0eLnidiQc+AjrlQp6zqBPSYL8BqJZvH2hhx7NuC9del9yq0a5GJfDm6qbV2ujKHsp/rL0oKnQlEtYcVLZWzr9yllWrfd5X8VSizcuQzSPSLoqT16Kxg2xN5R51f0fuc4F/3mJFJpFG01JnqOwLqmGNFdjbHUc+gxzzq1iz8rmMEPjISV48K0wcZH6J1KdfuO7jlrbO2LWXnFEaruCt2QcYuClUC32M6lgRSxZH63CWphqYyc7TLXX7ckdCUO8FkFFGVfcvLi7KP9Q8ZR6shwJ6OXK8KUcVUURrF0eGEYM7YINCBv4PJS7SMPxH1vMamlr7T7wCDAVY56L7Xs6V4CaP2bIC4v01EIxTlrS9DtLpQlp8jaP2EbH1JGbFJtocKdCKk9ogttHvjI2OghBIaXZUX0Kf1Gi1BgdIuekAsE+gXKuJjg8SpF6Yyh2TFRqtRmCHsJjbM81wNuk1krpq3vbHgkVQhpx+XcpTdjrQ/mvmniIrI4AcF5KNvHK03UQvoDsLY4fc6LoYoy4wdmjdTelyZC9FHrdtgVi7dwb8AZ8VuieExBvADbX1OGhghtusNRioVcLN64zPsDKg+fnsTdXqhl6rD45PbRUEkQcwIT01xsXaunKGr+RPMP2Ed0G5dvAFzhr3I5CO89WBkebxxvQN+P808FpY6XnQtMPI9FDq0Lv5q+r27PkcBeaRWO7r20fRGOIZpDhP/Ttnz7Er/WocDKT6/ZkgJu71OzCOtbNFMUjJ00xkwb+Ioxoj+8fL/4SBZCBwEOuHsW/TU2a9gzE1eXy2feDex2ihJqK3MFeA5/SwY/ipUhcxPaF2pWtcnF77GsumvHc+gMI1z2lZgVg5274ufMkzjK4U6644s67Jd5n0NxoozYC5a/wTNAjcFfMAUQHnbOoYIx6Mq99TNizeez31CXlzSxPhkiDcRZTstich4zbIQ18RqkH/l3tp+J6cx5QIrSOA2U1Be226V/min+uk1HxUaPkr/jDmQTMloa5La6Q3jLSVv3rL7z3PrlqXK3VJuJhjGx6PjZepmrFkuHijWuQtAemWuM41P3df0ZzfYO/80AEyhAIAmDM598I7IA16p/2F3TgI008Vuqq+MzdUduK7XWQ6g+2J6pj/xBXpF6XqSn7wPS+YKNF8+SxlS50ijpltGtoHRACUPEGNTFXozAL5Bno72TFct0ZqIjfMAX7a1WXRkDJ//FI3e0dIOqtj93SYA+Yfj2OFLwGCPju9GAnEUq557Cw/XMItOMrlZPMlDhmL6fPd7AjAJHN99kwSCczlj4kXsVytl6YQmf2ZoFVRHyjNnfisHJazyc47AgNnA1+WostEzQ/h5ORdDI0VzK/Fuiz11hcYgd7c5At7lHyHhFZ4A7tj2/y6bkX" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV0AX49S4XPonzyGHDwZs-R_VWAKpvYG8_cXVgn-Lz---VSONzQ1&amp;t=636256279320000000" type="text/javascript"></script>


<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bAjaxControlToolkit%2c+Version%3d4.1.50927.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d28f01b0e84b6d53e%3aen-US%3ac56b20f0-b89f-420b-94a3-356adf3cc133%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=492" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=492" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=492" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=492" type="text/javascript"></script><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=492" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=492" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=492" type="text/javascript"></script><script src="/js/dnncore.js?cdv=492" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=492" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=492)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=492)-->













<!--CDF(Javascript|http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=492)-->





<!--CDF(Javascript|http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=492)-->



<!--CDF(Javascript|http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=492)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/TempleMountSiftingProject.aspx" >Temple Mount Sifting Project</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/TheChildrenareOurFuture.aspx" >The Children are Our Future</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/MediaSupport.aspx" >Media Support</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/80/Default.aspx" >Israel Celebrates!</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News site and governs data collection and usage.
        By using the Israel Today | Israel News site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply1@israeltoday.co.il">donotreply1@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2017 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    </form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=492)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=492)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=492)--><!--CDF(Css|/Portals/_default/Skins/ITD2/skin.css?cdv=492)--><!--CDF(Javascript|/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=492)--><!--CDF(Javascript|/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=492)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=492)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=492)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=492)-->
    
</body>
</html>