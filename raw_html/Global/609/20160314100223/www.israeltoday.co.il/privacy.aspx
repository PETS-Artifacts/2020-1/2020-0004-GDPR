<!DOCTYPE html>
<html  lang="en-US">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2015, by DNN Corporation -->
<!--*********************************************-->
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem,DotNetNuke,DNN" /><meta id="MetaGenerator" name="GENERATOR" content="DotNetNuke " /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Portals/_default/default.css?cdv=370" media="all" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=370" media="all" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=370" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=370" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_10_03/jquery-ui.js?cdv=370" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTU1MDYzMDgwNw9kFgICBg9kFgICAQ9kFgICBw9kFgJmD2QWBAIND2QWAgICDw8WAh4LTmF2aWdhdGVVcmwFK2h0dHA6Ly93d3cuaXNyYWVsdG9kYXkuY28uaWwvU2hvcC9DYXJ0LmFzcHhkFgQCAQ8PFgIeB1Zpc2libGVoZGQCAw8PFgIeCEltYWdlVXJsBSp+L0Rlc2t0b3BNb2R1bGVzL2Vwc0VDb21tL0ltYWdlcy9jYXJ0Mi5wbmdkZAIQD2QWAmYPZBYCZg9kFgICAw9kFgJmD2QWAgIBD2QWAgIBDw8WAh4EVGV4dAX3QDxkaXYgYWxpZ249ImxlZnQiPg0KICAgICAgICA8cD5Jc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBpcyBjb21taXR0ZWQgdG8gcHJvdGVjdGluZyB5b3VyIHByaXZhY3kgYW5kIGRldmVsb3BpbmcgdGVjaG5vbG9neQ0KICAgICAgICB0aGF0IGdpdmVzIHlvdSB0aGUgbW9zdCBwb3dlcmZ1bCBhbmQgc2FmZSBvbmxpbmUgZXhwZXJpZW5jZS4gVGhpcyBTdGF0ZW1lbnQgb2YgUHJpdmFjeQ0KICAgICAgICBhcHBsaWVzIHRvIHRoZSBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBXZWIgc2l0ZSBhbmQgZ292ZXJucyBkYXRhIGNvbGxlY3Rpb24gYW5kIHVzYWdlLg0KICAgICAgICBCeSB1c2luZyB0aGUgSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3Mgd2Vic2l0ZSwgeW91IGNvbnNlbnQgdG8gdGhlIGRhdGEgcHJhY3RpY2VzIGRlc2NyaWJlZA0KICAgICAgICBpbiB0aGlzIHN0YXRlbWVudC48L3A+DQogICAgICAgIDxwPjxzcGFuIGNsYXNzPSJTdWJIZWFkIj5Db2xsZWN0aW9uIG9mIHlvdXIgUGVyc29uYWwgSW5mb3JtYXRpb248L3NwYW4+PC9wPg0KICAgICAgICA8cD5Jc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBjb2xsZWN0cyBwZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbiwgc3VjaCBhcyB5b3VyIGUtbWFpbA0KICAgICAgICBhZGRyZXNzLCBuYW1lLCBob21lIG9yIHdvcmsgYWRkcmVzcyBvciB0ZWxlcGhvbmUgbnVtYmVyLiBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBhbHNvDQogICAgICAgIGNvbGxlY3RzIGFub255bW91cyBkZW1vZ3JhcGhpYyBpbmZvcm1hdGlvbiwgd2hpY2ggaXMgbm90IHVuaXF1ZSB0byB5b3UsIHN1Y2ggYXMNCiAgICAgICAgeW91ciBaSVAgY29kZSwgYWdlLCBnZW5kZXIsIHByZWZlcmVuY2VzLCBpbnRlcmVzdHMgYW5kIGZhdm9yaXRlcy48L3A+DQogICAgICAgIDxwPlRoZXJlIGlzIGFsc28gaW5mb3JtYXRpb24gYWJvdXQgeW91ciBjb21wdXRlciBoYXJkd2FyZSBhbmQgc29mdHdhcmUgdGhhdCBpcyBhdXRvbWF0aWNhbGx5DQogICAgICAgIGNvbGxlY3RlZCBieSBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cy4gVGhpcyBpbmZvcm1hdGlvbiBjYW4gaW5jbHVkZTogeW91ciBJUCBhZGRyZXNzLA0KICAgICAgICBicm93c2VyIHR5cGUsIGRvbWFpbiBuYW1lcywgYWNjZXNzIHRpbWVzIGFuZCByZWZlcnJpbmcgV2ViIHNpdGUgYWRkcmVzc2VzLiBUaGlzDQogICAgICAgIGluZm9ybWF0aW9uIGlzIHVzZWQgYnkgSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MgZm9yIHRoZSBvcGVyYXRpb24gb2YgdGhlIHNlcnZpY2UsIHRvDQogICAgICAgIG1haW50YWluIHF1YWxpdHkgb2YgdGhlIHNlcnZpY2UsIGFuZCB0byBwcm92aWRlIGdlbmVyYWwgc3RhdGlzdGljcyByZWdhcmRpbmcgdXNlDQogICAgICAgIG9mIHRoZSBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBXZWIgc2l0ZS48L3A+DQogICAgICAgIDxwPlBsZWFzZSBrZWVwIGluIG1pbmQgdGhhdCBpZiB5b3UgZGlyZWN0bHkgZGlzY2xvc2UgcGVyc29uYWxseSBpZGVudGlmaWFibGUgaW5mb3JtYXRpb24NCiAgICAgICAgb3IgcGVyc29uYWxseSBzZW5zaXRpdmUgZGF0YSB0aHJvdWdoIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIHB1YmxpYyBtZXNzYWdlIGJvYXJkcywNCiAgICAgICAgdGhpcyBpbmZvcm1hdGlvbiBtYXkgYmUgY29sbGVjdGVkIGFuZCB1c2VkIGJ5IG90aGVycy4gTm90ZTogSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MNCiAgICAgICAgZG9lcyBub3QgcmVhZCBhbnkgb2YgeW91ciBwcml2YXRlIG9ubGluZSBjb21tdW5pY2F0aW9ucy48L3A+DQogICAgICAgIDxwPklzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIGVuY291cmFnZXMgeW91IHRvIHJldmlldyB0aGUgcHJpdmFjeSBzdGF0ZW1lbnRzIG9mIFdlYiBzaXRlcw0KICAgICAgICB5b3UgY2hvb3NlIHRvIGxpbmsgdG8gZnJvbSBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBzbyB0aGF0IHlvdSBjYW4gdW5kZXJzdGFuZCBob3cgdGhvc2UNCiAgICAgICAgV2ViIHNpdGVzIGNvbGxlY3QsIHVzZSBhbmQgc2hhcmUgeW91ciBpbmZvcm1hdGlvbi4gSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MgaXMgbm90IHJlc3BvbnNpYmxlDQogICAgICAgIGZvciB0aGUgcHJpdmFjeSBzdGF0ZW1lbnRzIG9yIG90aGVyIGNvbnRlbnQgb24gV2ViIHNpdGVzIG91dHNpZGUgb2YgdGhlIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzDQogICAgICAgIGFuZCBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBmYW1pbHkgb2YgV2ViIHNpdGVzLjwvcD4NCiAgICAgICAgPHA+PHNwYW4gY2xhc3M9IlN1YkhlYWQiPlVzZSBvZiB5b3VyIFBlcnNvbmFsIEluZm9ybWF0aW9uPC9zcGFuPjwvcD4NCiAgICAgICAgPHA+SXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MgY29sbGVjdHMgYW5kIHVzZXMgeW91ciBwZXJzb25hbCBpbmZvcm1hdGlvbiB0byBvcGVyYXRlIHRoZSBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cw0KICAgICAgICBXZWIgc2l0ZSBhbmQgZGVsaXZlciB0aGUgc2VydmljZXMgeW91IGhhdmUgcmVxdWVzdGVkLiBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBhbHNvIHVzZXMNCiAgICAgICAgeW91ciBwZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbiB0byBpbmZvcm0geW91IG9mIG90aGVyIHByb2R1Y3RzIG9yIHNlcnZpY2VzDQogICAgICAgIGF2YWlsYWJsZSBmcm9tIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIGFuZCBpdHMgYWZmaWxpYXRlcy4gSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MgbWF5IGFsc28NCiAgICAgICAgY29udGFjdCB5b3UgdmlhIHN1cnZleXMgdG8gY29uZHVjdCByZXNlYXJjaCBhYm91dCB5b3VyIG9waW5pb24gb2YgY3VycmVudCBzZXJ2aWNlcw0KICAgICAgICBvciBvZiBwb3RlbnRpYWwgbmV3IHNlcnZpY2VzIHRoYXQgbWF5IGJlIG9mZmVyZWQuPC9wPg0KICAgICAgICA8cD5Jc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBkb2VzIG5vdCBzZWxsLCByZW50IG9yIGxlYXNlIGl0cyBjdXN0b21lciBsaXN0cyB0byB0aGlyZCBwYXJ0aWVzLg0KICAgICAgICBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBtYXksIGZyb20gdGltZSB0byB0aW1lLCBjb250YWN0IHlvdSBvbiBiZWhhbGYgb2YgZXh0ZXJuYWwgYnVzaW5lc3MNCiAgICAgICAgcGFydG5lcnMgYWJvdXQgYSBwYXJ0aWN1bGFyIG9mZmVyaW5nIHRoYXQgbWF5IGJlIG9mIGludGVyZXN0IHRvIHlvdS4gSW4gdGhvc2UgY2FzZXMsDQogICAgICAgIHlvdXIgdW5pcXVlIHBlcnNvbmFsbHkgaWRlbnRpZmlhYmxlIGluZm9ybWF0aW9uIChlLW1haWwsIG5hbWUsIGFkZHJlc3MsIHRlbGVwaG9uZQ0KICAgICAgICBudW1iZXIpIGlzIG5vdCB0cmFuc2ZlcnJlZCB0byB0aGUgdGhpcmQgcGFydHkuIEluIGFkZGl0aW9uLCBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cw0KICAgICAgICBtYXkgc2hhcmUgZGF0YSB3aXRoIHRydXN0ZWQgcGFydG5lcnMgdG8gaGVscCB1cyBwZXJmb3JtIHN0YXRpc3RpY2FsIGFuYWx5c2lzLCBzZW5kDQogICAgICAgIHlvdSBlbWFpbCBvciBwb3N0YWwgbWFpbCwgcHJvdmlkZSBjdXN0b21lciBzdXBwb3J0LCBvciBhcnJhbmdlIGZvciBkZWxpdmVyaWVzLiBBbGwNCiAgICAgICAgc3VjaCB0aGlyZCBwYXJ0aWVzIGFyZSBwcm9oaWJpdGVkIGZyb20gdXNpbmcgeW91ciBwZXJzb25hbCBpbmZvcm1hdGlvbiBleGNlcHQgdG8NCiAgICAgICAgcHJvdmlkZSB0aGVzZSBzZXJ2aWNlcyB0byBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cywgYW5kIHRoZXkgYXJlIHJlcXVpcmVkIHRvIG1haW50YWluDQogICAgICAgIHRoZSBjb25maWRlbnRpYWxpdHkgb2YgeW91ciBpbmZvcm1hdGlvbi48L3A+DQogICAgICAgIDxwPklzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIGRvZXMgbm90IHVzZSBvciBkaXNjbG9zZSBzZW5zaXRpdmUgcGVyc29uYWwgaW5mb3JtYXRpb24sIHN1Y2gNCiAgICAgICAgYXMgcmFjZSwgcmVsaWdpb24sIG9yIHBvbGl0aWNhbCBhZmZpbGlhdGlvbnMsIHdpdGhvdXQgeW91ciBleHBsaWNpdCBjb25zZW50LjwvcD4NCiAgICAgICAgPHA+SXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3Mga2VlcHMgdHJhY2sgb2YgdGhlIFdlYiBzaXRlcyBhbmQgcGFnZXMgb3VyIGN1c3RvbWVycyB2aXNpdCB3aXRoaW4NCiAgICAgICAgSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MsIGluIG9yZGVyIHRvIGRldGVybWluZSB3aGF0IElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIHNlcnZpY2VzIGFyZQ0KICAgICAgICB0aGUgbW9zdCBwb3B1bGFyLiBUaGlzIGRhdGEgaXMgdXNlZCB0byBkZWxpdmVyIGN1c3RvbWl6ZWQgY29udGVudCBhbmQgYWR2ZXJ0aXNpbmcNCiAgICAgICAgd2l0aGluIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIHRvIGN1c3RvbWVycyB3aG9zZSBiZWhhdmlvciBpbmRpY2F0ZXMgdGhhdCB0aGV5IGFyZSBpbnRlcmVzdGVkDQogICAgICAgIGluIGEgcGFydGljdWxhciBzdWJqZWN0IGFyZWEuPC9wPg0KICAgICAgICA8cD5Jc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBXZWIgc2l0ZXMgd2lsbCBkaXNjbG9zZSB5b3VyIHBlcnNvbmFsIGluZm9ybWF0aW9uLCB3aXRob3V0IG5vdGljZSwNCiAgICAgICAgb25seSBpZiByZXF1aXJlZCB0byBkbyBzbyBieSBsYXcgb3IgaW4gdGhlIGdvb2QgZmFpdGggYmVsaWVmIHRoYXQgc3VjaCBhY3Rpb24gaXMNCiAgICAgICAgbmVjZXNzYXJ5IHRvOiAoYSkgY29uZm9ybSB0byB0aGUgZWRpY3RzIG9mIHRoZSBsYXcgb3IgY29tcGx5IHdpdGggbGVnYWwgcHJvY2Vzcw0KICAgICAgICBzZXJ2ZWQgb24gSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3Mgb3IgdGhlIHNpdGU7IChiKSBwcm90ZWN0IGFuZCBkZWZlbmQgdGhlIHJpZ2h0cyBvcg0KICAgICAgICBwcm9wZXJ0eSBvZiBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3czsgYW5kLCAoYykgYWN0IHVuZGVyIGV4aWdlbnQgY2lyY3Vtc3RhbmNlcyB0byBwcm90ZWN0DQogICAgICAgIHRoZSBwZXJzb25hbCBzYWZldHkgb2YgdXNlcnMgb2YgSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MsIG9yIHRoZSBwdWJsaWMuPC9wPg0KICAgICAgICA8cD48c3BhbiBjbGFzcz0iU3ViSGVhZCI+VXNlIG9mIENvb2tpZXM8L3NwYW4+PC9wPg0KICAgICAgICA8cD5UaGUgSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MgV2ViIHNpdGUgdXNlICJjb29raWVzIiB0byBoZWxwIHlvdSBwZXJzb25hbGl6ZSB5b3VyIG9ubGluZQ0KICAgICAgICBleHBlcmllbmNlLiBBIGNvb2tpZSBpcyBhIHRleHQgZmlsZSB0aGF0IGlzIHBsYWNlZCBvbiB5b3VyIGhhcmQgZGlzayBieSBhIFdlYiBwYWdlDQogICAgICAgIHNlcnZlci4gQ29va2llcyBjYW5ub3QgYmUgdXNlZCB0byBydW4gcHJvZ3JhbXMgb3IgZGVsaXZlciB2aXJ1c2VzIHRvIHlvdXIgY29tcHV0ZXIuDQogICAgICAgIENvb2tpZXMgYXJlIHVuaXF1ZWx5IGFzc2lnbmVkIHRvIHlvdSwgYW5kIGNhbiBvbmx5IGJlIHJlYWQgYnkgYSB3ZWIgc2VydmVyIGluIHRoZQ0KICAgICAgICBkb21haW4gdGhhdCBpc3N1ZWQgdGhlIGNvb2tpZSB0byB5b3UuPC9wPg0KICAgICAgICA8cD5PbmUgb2YgdGhlIHByaW1hcnkgcHVycG9zZXMgb2YgY29va2llcyBpcyB0byBwcm92aWRlIGEgY29udmVuaWVuY2UgZmVhdHVyZSB0byBzYXZlDQogICAgICAgIHlvdSB0aW1lLiBUaGUgcHVycG9zZSBvZiBhIGNvb2tpZSBpcyB0byB0ZWxsIHRoZSBXZWIgc2VydmVyIHRoYXQgeW91IGhhdmUgcmV0dXJuZWQNCiAgICAgICAgdG8gYSBzcGVjaWZpYyBwYWdlLiBGb3IgZXhhbXBsZSwgaWYgeW91IHBlcnNvbmFsaXplIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIHBhZ2VzLCBvcg0KICAgICAgICByZWdpc3RlciB3aXRoIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIHNpdGUgb3Igc2VydmljZXMsIGEgY29va2llIGhlbHBzIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzDQogICAgICAgIHRvIHJlY2FsbCB5b3VyIHNwZWNpZmljIGluZm9ybWF0aW9uIG9uIHN1YnNlcXVlbnQgdmlzaXRzLiBUaGlzIHNpbXBsaWZpZXMgdGhlIHByb2Nlc3MNCiAgICAgICAgb2YgcmVjb3JkaW5nIHlvdXIgcGVyc29uYWwgaW5mb3JtYXRpb24sIHN1Y2ggYXMgYmlsbGluZyBhZGRyZXNzZXMsIHNoaXBwaW5nIGFkZHJlc3NlcywNCiAgICAgICAgYW5kIHNvIG9uLiBXaGVuIHlvdSByZXR1cm4gdG8gdGhlIHNhbWUgSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MgV2ViIHNpdGUsIHRoZSBpbmZvcm1hdGlvbg0KICAgICAgICB5b3UgcHJldmlvdXNseSBwcm92aWRlZCBjYW4gYmUgcmV0cmlldmVkLCBzbyB5b3UgY2FuIGVhc2lseSB1c2UgdGhlIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzDQogICAgICAgIGZlYXR1cmVzIHRoYXQgeW91IGN1c3RvbWl6ZWQuPC9wPg0KICAgICAgICA8cD5Zb3UgaGF2ZSB0aGUgYWJpbGl0eSB0byBhY2NlcHQgb3IgZGVjbGluZSBjb29raWVzLiBNb3N0IFdlYiBicm93c2VycyBhdXRvbWF0aWNhbGx5DQogICAgICAgIGFjY2VwdCBjb29raWVzLCBidXQgeW91IGNhbiB1c3VhbGx5IG1vZGlmeSB5b3VyIGJyb3dzZXIgc2V0dGluZyB0byBkZWNsaW5lIGNvb2tpZXMNCiAgICAgICAgaWYgeW91IHByZWZlci4gSWYgeW91IGNob29zZSB0byBkZWNsaW5lIGNvb2tpZXMsIHlvdSBtYXkgbm90IGJlIGFibGUgdG8gZnVsbHkgZXhwZXJpZW5jZQ0KICAgICAgICB0aGUgaW50ZXJhY3RpdmUgZmVhdHVyZXMgb2YgdGhlIElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIHNlcnZpY2VzIG9yIFdlYiBzaXRlcyB5b3UgdmlzaXQuPC9wPg0KICAgICAgICA8cD48c3BhbiBjbGFzcz0iU3ViSGVhZCI+U2VjdXJpdHkgb2YgeW91ciBQZXJzb25hbCBJbmZvcm1hdGlvbjwvc3Bhbj48L3A+DQogICAgICAgIDxwPklzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIHNlY3VyZXMgeW91ciBwZXJzb25hbCBpbmZvcm1hdGlvbiBmcm9tIHVuYXV0aG9yaXplZCBhY2Nlc3MsDQogICAgICAgIHVzZSBvciBkaXNjbG9zdXJlLiBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBzZWN1cmVzIHRoZSBwZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbg0KICAgICAgICB5b3UgcHJvdmlkZSBvbiBjb21wdXRlciBzZXJ2ZXJzIGluIGEgY29udHJvbGxlZCwgc2VjdXJlIGVudmlyb25tZW50LCBwcm90ZWN0ZWQgZnJvbQ0KICAgICAgICB1bmF1dGhvcml6ZWQgYWNjZXNzLCB1c2Ugb3IgZGlzY2xvc3VyZS4gV2hlbiBwZXJzb25hbCBpbmZvcm1hdGlvbiAoc3VjaCBhcyBhIGNyZWRpdA0KICAgICAgICBjYXJkIG51bWJlcikgaXMgdHJhbnNtaXR0ZWQgdG8gb3RoZXIgV2ViIHNpdGVzLCBpdCBpcyBwcm90ZWN0ZWQgdGhyb3VnaCB0aGUgdXNlDQogICAgICAgIG9mIGVuY3J5cHRpb24sIHN1Y2ggYXMgdGhlIFNlY3VyZSBTb2NrZXQgTGF5ZXIgKFNTTCkgcHJvdG9jb2wuPC9wPg0KICAgICAgICA8cD48c3BhbiBjbGFzcz0iU3ViSGVhZCI+Q2hhbmdlcyB0byB0aGlzIFN0YXRlbWVudDwvc3Bhbj48L3A+DQogICAgICAgIDxwPklzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIHdpbGwgb2NjYXNpb25hbGx5IHVwZGF0ZSB0aGlzIFN0YXRlbWVudCBvZiBQcml2YWN5IHRvIHJlZmxlY3QNCmNvbXBhbnkgYW5kIGN1c3RvbWVyIGZlZWRiYWNrLiBJc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyBlbmNvdXJhZ2VzIHlvdSB0byBwZXJpb2RpY2FsbHkgDQogICAgICAgIHJldmlldyB0aGlzIFN0YXRlbWVudCB0byBiZSBpbmZvcm1lZCBvZiBob3cgSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MgaXMgcHJvdGVjdGluZyB5b3VyDQogICAgICAgIGluZm9ybWF0aW9uLjwvcD4NCiAgICAgICAgPHA+PHNwYW4gY2xhc3M9IlN1YkhlYWQiPkNvbnRhY3QgSW5mb3JtYXRpb248L3NwYW4+PC9wPg0KICAgICAgICA8cD5Jc3JhZWwgVG9kYXkgfCBJc3JhZWwgTmV3cyB3ZWxjb21lcyB5b3VyIGNvbW1lbnRzIHJlZ2FyZGluZyB0aGlzIFN0YXRlbWVudCBvZiBQcml2YWN5Lg0KICAgICAgICBJZiB5b3UgYmVsaWV2ZSB0aGF0IElzcmFlbCBUb2RheSB8IElzcmFlbCBOZXdzIGhhcyBub3QgYWRoZXJlZCB0byB0aGlzIFN0YXRlbWVudCwgcGxlYXNlDQogICAgICAgIGNvbnRhY3QgSXNyYWVsIFRvZGF5IHwgSXNyYWVsIE5ld3MgYXQgPGEgaHJlZj0ibWFpbHRvOmRvbm90cmVwbHlAaXNyYWVsdG9kYXkuY28uaWwiPmRvbm90cmVwbHlAaXNyYWVsdG9kYXkuY28uaWw8L2E+Lg0KICAgICAgICBXZSB3aWxsIHVzZSBjb21tZXJjaWFsbHkgcmVhc29uYWJsZSBlZmZvcnRzIHRvIHByb21wdGx5IGRldGVybWluZSBhbmQgcmVtZWR5IHRoZQ0KICAgICAgICBwcm9ibGVtLjwvcD4NCiAgICA8L2Rpdj5kZGRDKRcNczYQMYj5hoPqiEX26Yd7HA==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=58uv4XOX74qO3c9UBQpyUUxxz30zyyYHHeh-H1zeewric1UUzrFqn20D4gjZBAtESHzAZvyaMtgewZAu0&amp;t=635802997220000000" type="text/javascript"></script>

<script type="text/javascript" src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js"></script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bAjaxControlToolkit%2c+Version%3d4.1.50927.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d28f01b0e84b6d53e%3aen-US%3ac56b20f0-b89f-420b-94a3-356adf3cc133%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=370" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=370" type="text/javascript"></script><script src="/js/dnncore.js?cdv=370" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=370" type="text/javascript"></script><script src="//cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=370" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=370" type="text/javascript"></script><script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=370" type="text/javascript"></script><script src="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=370" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=370)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=370)-->













<!--CDF(Javascript|//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=370)-->





<!--CDF(Javascript|//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=370)-->



<!--CDF(Javascript|//cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=370)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/TheChildrenareOurFuture.aspx" >The Children are Our Future</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/product/tabid/61/productId/4964/Default.aspx" >3 in 1 - Support Package</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/product/tabid/61/productId/5177/Default.aspx" >Media Support</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/73/Default.aspx" >Deals Of The Month</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News Web site and governs data collection and usage.
        By using the Israel Today | Israel News website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply@israeltoday.co.il">donotreply@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2016 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    
<script type="text/javascript" src="/Resources/Shared/scripts/initWidgets.js" ></script></form>
    
    
</body>
</html>