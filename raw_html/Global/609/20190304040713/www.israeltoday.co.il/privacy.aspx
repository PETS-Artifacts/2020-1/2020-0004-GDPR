<!DOCTYPE html>
<html  lang="en-US">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=542" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="RA8meQOCoaUJWVfrGre9s3kzguplpfrJ2UrVSsZu31DL3c/8FJvDk5oyBbPqMd7WBGoaEUFdwV+S64Ol9qwBphThO6oJdyArbeysCyfc/LO2lsV+9TWVEdX6LwqODWliF+6ynV61fyS/qnG+vy0K0kd9pxCYYdf8L/bXpg02Hryku9jO4wSoQ08oQYZWIp9Se8xLvUQeiBrGCxVAln4UJ8Ssz+B4wYqHcWomci1WFHItObjUY+JgjM3govhtegF8KX4W9dPNSTrTwD2LydAj3289TQddsaSRFUOJxGsvF6kdopVoOcparUIP2RxEG9vQvwvD0TBJsiJNIcVbeDrBAAZs1ykkUKSvqwl0UlEC98E8hKdrP4FX46qp5QLm9/F2RXDZql7omn4Pm2zXwQmA0okGrSnGwww70/hO9Fxd9L4WApGEWerpRFH7QrLc7XJN52VUTDPiIiOf2Vf4CfN0Hg08vDz2yOMm50T0iQ8fS1h3z1XvtejJ7ABKiuJxtHIKJQ5teXWw2OIFhTD5aar4CYlCKH66wJXlgIm42J9P//vTICKDvE9HaH9LrZvIJCOg291zvnB56BV1zuTR4iXQjEoIVoAmeSbnWcHE2cdvcu+FLP+dABaGlsYhMqJhNMwaCo5livYVy6EbU0DiIHUtIfmMivn0o7YyANly9B1P0UaXhTrSRUDBvIE7Tz/KunK+ehjPvqOSkBBfg7r6RVJJ+VGUd2aN7sdQXsfr6/ZHW0QsFCxBmIXGfqER3JOYw+r6uUWJTapjFrTPwYdJIqbnXk2eqoKqA6MVNtzoN+5i6Frx1q0IU90eAheRCcvFLUQ3TVc/CunhSR5stBa47qyVh6x2KG0d8qLOu6A4ja+Vv/DcrdL9e2gbQNYJlWgTmYIkkRX98o7gKZIHF3kL09eE43sLiHk7It/gL1AAg4onVKwnrt8ptawkZhVUzThlaqeUBRW873qGveORRhGJuw+2Qfv9N/8db9vVTLP3DC3Y4n0eaNhdm6OLojggRICwxx96Z2fmGzxTGzx520+1NfeR7OTBZ/I0gCmH87A9Ecs9GV4QK+4Z0MRBLspSuzzxZTHU/jvt8GPK5kMh6ELJC3qFMecPNZB6TGwly1HFt2A/SAYZp06yote4WzcFa5sGanX9k5vBXpAlDEXIbf1AYRyMZE0NYUCyR5SYJI0CG6QRBArcBQDkJrTNkrtsGMuIpMLpPHRe4lBtdo1qtUySiVizX+yoDd8g+SVyPNO6LC72nVe145hZvVJlVtzcRXSj0HTlzrIaTEajRkD0B9dzkYbJkuqmJ/8vj2GSValEF7wY56n0yu1hG8BSsGZKLU0QbE+U4hKkThoUMnSZnFfBoABaAZjicGLDHt+MNqotaR4+ti+Jc3BrYTMcMmw8ZYFGyS2xkLUqp2YVERPYCvtkegstH9Adq23JhPZkhlPLVNUrIkAmSwUEFlo8QVaapTgYdbOx/fbG6NozsAG9cWsKD+pqjtsnqWxSoAjLzFQXbqCHl+oU7LNyAlu6vAntVpoD/fpDe4IG7T0tM6cHQmfG58PEm1UXESvMTpaCqbYlC/udJigsYFBvxGiEy9H3bPYqlEMVL6V33aJT1EAeim1i4krpKnDejuDl9nsm+0lkpe4pyQjGix7/8aq22QmrNNZCaBUchWS0aenoIOjEg9N2roGrcZ01M/IKcxCwPHQptn5M3kH5JVisbgFp47LkusdkOuZhPsszgrW74RBH0QYchPoyg8Bze8ITwnBup8a8Z0SGpozI23niUljoeZFHwdPfePTUvSsNtQ6kIwrfgNR/ECFY5jnAhmA2u+DfTiAqXQ7IfE+8/dd2+B8L2fYrVi5SSmwwGm8vV1BzBq4LN4YyTJVWgzOeOnok8CDys8AK8kUZ3RcM9JGRSj0pEgLMiWt+62FDLtRT0pO4PrDaTykIgcPF4iPddvjDVms5gWmzg75ATl8lfs7PHLa0D6D7dE8fETHaLEEZ07EXopM5FC/kvx8ROVIbME6msm1528vC1ICRRxn/y+BQbZdB3wu9rIncs2ZdN2UmvorLnRoYOSTAxZQcrad7Z26Iky+BH3dH/AUsoMC+32i9b2C2NVf+PsTjpwxmNfavj59IVxYVGMhu+5aCrGmKzqbXdYmrYEhIOGqQ7RTLIrK8F60EquAU6N0PGfa0gFSZdtafs/soKCAeij3sm7xgYxdQo3rdbgzwE6u8plMWmTq7VywXPnmXcbj0V0Crk+4Gd+ULDrONRz0hJ86+08tm/qPrE7RB3Xw4ZbsarolCTzeQwa2H2H49mp7DEQCL2AY5yYL52eQ3yqAPvxHBjHRUx0t1JVMENErZnkf2XMtNF0nBJOIRLcQhOLhdUN/PoBtlGhoBJcN/Ltrdd83CpiiVwJBfA0rPWpRnM7GcyDf5Im9JmZLTZ/kobuAKvh3Hhs1Ne989nyuWvFKQTJ9m7gnA/AoKjHQt3mytysl3lRO7CpQ37kA6pPnQGsWvmTt8FsIGm6OOQmaFsKBufv2sadDYWrPIPsAwjZO6jz+PFrutRzUYvuMy7JxMIfaFNaWtIu3GE7QLKdjXjhIi4GY674CKenLUzjxWyEstKua3w3VSxaGbIo7r4xOEjgcB/xidwd19M5YqaQVDdX8B3I5a318gsK5VtWqCRqPdQbvSIORfzrT4F1XdiY6HyImuDPimV0s/znsHaAFopO26krOrMoz9m4ZA8h7n6GyNGXWcfw4zqX4sQTbgzG82YDWv5ofK0FF6drCrUw6seLjOK29nsS5xVUxpTc4XLnmqf4oSFl/kAJp9iSkCqKAUC6efIuyOZ0vfMAMuyy0y7x+mKfGlKbtqnMTKsqA3DzB6upjn8NOJdUHqqR6z8D6brxsY28jA3tglU/J/tIc3qjoUxt9mIyML+VuGRs+gNiEWch4GAWTHGKkwPFFloVbf1N7/j9Dq61kB1GWs72eqYykXKVZ5tex88w9yHa7xq5xoQZuSM3598w/vzAmbgkGIqJ1VKsmqFfBijkdjl+N+FNCyaySGDjn9eb+DFpv1SL5ToOSY8RPmQR+Grbm8cPkghHEY448+8paS7n/BPeifWLF29tFo1ToKJ9mLq7t5QPOmVtFyx1/d97PubxpHkEJzoPRTrAwGzv3NdjUMBDzTxxq4NIVMQDt94ND7CvG4SjopWszVoYgiUg0HzJbKX5QGzEH83TYFKeRCJA7XxWLCbQT3SeJYL2yqFE1TVJopjBuK4VBMO2sLIXlGtQHiPEOrg4hmCMENk7NtF5kbdu3S/DLG3d9XxV7apT5grdwUNhU2tkFWPKqDuY4cdcXa22RIxwmVdOYobErO7jCQgsYQKUjSPfp70wohw0hsYlEtXlPNETBe0JmpaZOMdJpmm3xdd/laLHBZ5k+KYjyKsktkMV8a0JtTbiVdkcbSwrPIBZy+ytrO6+BVaUG3Y0h1LvOMshA/75vI2/CraxSBhLgN+F4PZ+5s2sYLKpMwfAKYLVip/h/ujDKGhafYl/qPtvPdiN70RhbGfVjPe8dPrOapS83rILnf6c3VonBd5yZuyvLk05Gu3gJNTz7p1vZ+giIbabwzbIDdlTO+GIGS+cwMS5zg4tHKcfOWs5EP57Pn/yMIXZWUP+AbtD2YtS8fbiwosv9CWIPqPwM20URtdglh+G17qA1Luy/9u0ODr6Kw9T/IQ2ujYzNHnlGUMPGqxCt7m1Mjs48NdIK8W+CBeF8TS7yT0KMANJLRkdgV29lG/IkboKE/bV5eVm+bxMme8Bta94FGS9lTG5JiIoVO2eHxya10UB+kO+7mU7SatjvnTnFG2wXc8mTyzL5PV3XBj8G333yFab6Q/zK4kV9VzRApglFA3vymQ1kGJUaZIhVS7UTmPODLN1moQlZlgxgl41sKzcukMRlj0YuYMq+AgAGmjcKH+n7FhTvXYTorefMjRrqFrPhavmZ9bgBl3+SQbQoLuKXg9fTeic4X+5AFDml+9HSPCvNpUixJ7repNeHugjMof4NCXYbHWEXH5+RlCfipTkiBm4t/UNWuc5ZJuUEWib2CFRZtzGlEMW6F0TRi2jIS8jVxfQlL7QGYZ/gxE4f+9Hj7ip9J7X99pdq/TcOz24Ffm47HX37MCGr26pz9i+A2NqzNsI73J8Js+PQ3zxbt6Kqtx/T3VKeKxPsYVAMGlfz3XaR/j/0dz9lGIVsPH7gSpJOmTzJpVytR21Kxwv82OYiDCdqS9i9zKQ2WrLKIBf8yNFCkjQ+jgsfzx8UmPL6KkPD92+NbAAmO1slc//yizwaEYpzHH+EGthks32HVelsp0W99Fubv+bNRtxy/QxqYSUcoTD6xN1JamwHrj8RZAtGT7WUJRhBHQZMCdAaRQ9ud3GfBm+e7I3iW9Ypj4x1jZqGSNwn1Zc+Wn+VU0nh1tGUx2Dt82naOuyIlVm3O30NGNkFO05UnW8dkqlTNzM9xW+Leh7+YsGmGZQX76Jyz+K4d4aRrbgs7K7vKBodzzT8yPp6Vxg2sEEU5hgCxBxhwLokzi7BMsVLvcwu2WecZEgAlM7757zRX7RHGzg4b4O3tMhftNBj5H5AHMK2Ie2KpnIeCK8Is+iJKNRN07mOAcF9qw4QtvncaIVamdurRzwBdrvB7wtl8ukiQP6OIxWgkUrwbq9WcaUNEUluONLP9jUeQZF+t4sGxvXNnwGH0zvKqHUccUNUAvPSBG6rF8FjSYHQgdqn1aVYKDFxzah/6KuETCX53yQwt9Se+B1IPjOR9ZOe+PxtC97NFNIFANmaR7p5NLhBXOotTbKEY8fq+Dy78SfqDj0sTTy5n9TaGx4kibp1uBS8myImDJuNgPEzOUuK1EaI2d3fSkt206kkq8WAyDZmaInkvYYo103c5Phur/LGL5tEgW236Ju8y7AHaxaLytzdDRGitFGCGiCtVXZfx2YU9wrq+NpRYOBGTqEWRgNpB2Y8AwWckI4ZJdgFVdGus28RWTKp5EWTiGKBWwYXqi7ZFjhJmWM7zduPt4u13Ck60rXFBL0Kv0C1ZRyap6N7KNSar2JS5TyQ53frvawNgpSQVK+5/hHpAO1wHAd5pS960XNXr4hk+OGocShS4WXPKB1csebIxA62A4JjY2Lt+lyncjCMdkpHTdtbMPDFgt13hug3RUMrs6HDh/aYGw+rv11AxwX4kULr8YOcMTO8vcpQyjXZaZKcFrLlJFooQ+Uf9E4ji0Z9Fq2WycuzqnQP1Rd4MnIJ4BN4cPoEpumL2GjRXCc9C3MMvWXnI/IgVU0XmXqbcDLTSJX85KcaUN7HYYrGyFsFZ2M5tbWwxR1K9pC/uNVIeJcSLxUuv1rX3IYRrp4InwubnTSD9JAph9z/VmXYsGa9estnR6GepHiSvRqRahXn1vEFUIHPd/yLmUeW8E0QvMqal1RJsDP2dgBCNiJrjPhP9gIw2CjQUnttC41j1wnBR0LHfduOmsozwd5OKidfSlcY+s8kMIgKOC81pJ/YmBIpJUcf1VZjv8CR175Ina30z5spHgOk/yvatovWgW5QIknZHsd+zz9agCrT6Q9dWtGkvnVUT8bnHhK/4ySLFVsKPdtkAUxu7GuBckkFvYXWpZ6/DXHkq9mCe0il6EbR40usjyVB0TgOPfB0PtQQSoEueQsbOtR9ZqQto4HFSVlfozfdVYZwZOPSMbpYvlPzNpinCDIlZ6cs8srDj8jajhWPEZxMA3JNXOVoIXajoxRTuAmMTM8t3rmmpETN71HSvuQNDNsTNn4pF/GlcBdpRjmoB4VIlihOLEuWfH52MDCGW++AQkdxJ6uTH8yZ+HhzXoLvYPlutLn8nZ7pZ1EiLSNz5lcGRWjV96m7D9xfdc+OZBL+JLejzzErkbHEzg9ULNH5cLoS1c0nXU5OtY7zKmMJE0I7cqbEnTUummj4HkkGqFz/q0qfEZ4lkrF42QZvgLHEEPLJM+5rvQJ2QRwNMO0YenUW+WtQQpLyxPNdg2DEcgG+DDcs0HOdJ+gulR/u2dSstcMQ/v90e1stnA47UdXnOhrydLBcUVa6AiYnl9cVF9Nf/ciCGoUfQR6e+ehe5Oxn3i7rj2eO/8qzhRaJWKbv1bWuaaunlgLPhE1ocZ7PehWSyNSJbt3wJIfx8UUfWzwnmQwYSSvnabPc6yKXz5vDnkpAAGBbmPhh1gd7HNrJvCWwB8CEPQ3H4ekCAjzSpOkDzraYNtaV67uuXmuUy6E4R9QftGPA9hVpf+kTiusD8AcSx0xb65f/zVCN/UHddrQP5Bf0nPFCatBffl2S2GyJQ/Xx5BjjeeerxCq1XrzKvRNR8xsbQTKKs7hmoiL/Xma5dQAs2y3mOMvmlPEiXQIbYHykyIo7ORUM3UygxvvyLuqov1dcAopuJPWLX0LiViz/Z/FjWGxXWhzIqeFOAosSOGez5w2G8pJpVMg06Mcdtrkt2ltAPEDYxXxlmWdpmK3g9M/d4zfwjouifFdLqTfTTd2ZQb6vI0LqUjD8bjczpM9NrbmD4pWov4vjLI8eIaSgYEitoFIxSOjD4saknhYBYF73ShWf0sI8WtebEt4NLjBtDxT06k5j9daKDHcV0Og5TK/JsSZxMp+jWJcu3AIHUTXAhqihiRxOyKRz6SWTpanjVwUmbHrVM4xmlTJI/gS8Dpo9mzy4vFvcbF+0wP1t5BPUfnEKS+pHjuMtlpU2myDO7w0WRb+UInm+YXtabcK3j26u6O7L1Tb8qQBLUR/VNW38RwZWTnc3j+dIPurWtf853Ow9rvQSXzBnKXMqVu0+QMKUD9+y++/zJ1Vn78uEUpsIZOwKZi9pkGj26B4nePfLGuGCbcc7r6qySwhnX8Kd7gMSIi5bgcN8WyWhzWKvdXCXqNxi5A/4UPxDu8cUB36MoHWyMBlE2BpMzE0aEXbxUORGTQ/cUlW1ckAqfoYTy/FL66bP5cpzf+nscDsUZBgBqlArEEvwKo+EDwwCQlUdkfKXip9hMH7buWpWf7H0ldDo+jkAkGSWUgpKSg+g2S4m67gXEqvoxfqsHCehKbD4cB7mbxSZXixzTTqnluiIjHWbwygFC2pQNOOK1kJF8TDf9yKFKmoCPL8HQ0XAf5OwWM1JApQEtk0O1yWObDaAyHpDCDGeMq+qsH7TkcrBaalovR4hu4oN0hMyocarZ0B4E72F6boiQwtt2cZGhIK5ZxABhgqJmAik3JBgiRs14s0C+Kiij/RKoqiy3E4WR1Uy4yU7D212SzyW5yj31c8id/D8x5E80khbSPymx8MBEnUjcUzPtfTG8EEgdywbLK0NGSgNna/aFkC/a4NYKlwdXiNvH6vOFt7YZzZJprNqppR2KuNkXOv5GiGORpKCNhTCTdWpSC3+Ig2bbPeVHmpKKr67lA82eozM3BdqGBniD34MrK+wloebB0XBiKgj/0uySbEkT/NT8nur+AMGRsha6XWWxtfP1VuVC7XWBo5v8JE1faBXN4Q2RPMAK5ru13rfe4/Zxg/OHFE4wyrnG5G0fDieolROu2lJSvRfrUuhtlHv/EfPSxWhHovTEfpjCecx09Ic6cq1AvdSyikCkQ7Pvo92FWQ9fwOZobA0nTydcQI4LzzI45/tKLdg225qmyq0aYpNzc/w/vyuoP0vjQ+fr4egTGw1k0ty6gm5pXqOer/eteeKrhJIim6YtdSz7a/ZQEz5+nj02bbP4V6gIA9sqnqD9F0m7Df9cv1eRmjN16i+p63EKU/vTTORAeUq4xMnBhOczPA8y5oRZ0aNq8rIscPJk9l/8KqWL6ukInEufooOEhAtumGxPg1vR1TOApnSUdn1kiN5jEIn6/qBEPWMvM15Ch0WDh6x18S1UsvxWCLBcJMJw5V1fGBCpFAHg3FipLyb+upP9JPiybFjnt1xDTzI1fA4DxGirJPrPtAewaRgRndu9/A9/FtZiNuMMgGuuFzmkpuVcQBLoL/vWrsmGYR7jl2/Vil2ZpWjR9qAmGEUwFa20mkGzPn02fJ9AP9Wbwwni8SNuCJwFkwZeYm1bhJdV7yfa1u1Ok4OipDOokWO71vRiw2Fl+IuzRl1h5MydKc+gw2IDjmmqpAlCMtVEA7qFY9YY70lqr7BBEQVhmdGL9REngJUIlOV6595WYicObJxKs1wOz/RBoJSXwGWla1NVg46MwxRXQs7ioSfmavBpEEl7kAfprOLPe3VM3BwS2Kdmqul5E2vww+U9hKWtMfhVdQLo9wMHpxwVBJOM1G7FPy+Lskv4Nv/fCdZJOUGKu+pyG1c1WXYvDyMpnyVgQPvhJFom07DFZuO/6ckeJCQ8ka9kNTtbqTUEyWVqEwEW6QF3zdAapFsgO7ZyCdHY5u2V/Dmt++ew3vH7AeTL8qVzX0Z0WFEN0qjdLyvCBVtXmEMHXVO/e7TiWZ2tH/jy9LntFzrI5u3rEBployiabfCmJbzr23LowMpi3NjuOQHlvqtWCvM5wYmTHNaD87n85D/2dTJ8VAdtSguH8si7RGxhmbprXakXtSTfA+aEYmBJAkv6XZbTXs7HBC90vpyVCYjcp+W9Q/e+cvG9bZCpV1wpIACGEqKgFKwBd0m1fx9U8dfvWn+QThJAzFAis9B3rwZ205EOa/31U4tPbUQnajz9979Dv8ywC1hqKj7eih8my9lDGKYsDGzDrH+q6Wb7KXmVFZYQRor9F+OqP8oeq/hHF4A0NryBxdPDRCDR0+6DZDmjMmaXfYDDEBd2RQcivD/G/d1bg6RqXOWbv8az41rhXNSfYttp22H4Q9jvh+Plfxuyzkwwz6APU1T7VUIKzhxRpUQ2LQlo7mH5lihpNcRCX8qVjx5goJAdvci9yGBKLsBBErUEHW0L0rosRjuQAfnI5fNPX4jD0UxwwpeAkb7cgF6ViCLR4tFgbElgaGn9MW2qfgljr1WHW6g6N/i7Xqto5IHmpCVrEI/AAflWQknQsKqHXxELlaxN9N7kbyE9x2BZoZ5w/UrM2b9DB3VuQuMdNZD1OFWFn9i6tys3/gI25qhozqnYxcTms1/a5JeDlhI/hDnxVg/0JhQUabanT+KWWjVyatt4KEuPl7VU6JCRDHJ2vsmvXSQOwyHE1uHUaUYDMQAWHTRuHQlM1ndlf5ZI/8AEZHZFz+llt7INcRUdzSkl3lblpmf65lGbEycLrNCywF1ULnMq0WA9cQRpGdmBjL/ps3W6ZMoyTvuAKpuwLD4MfJD+S28Zz/lIdfLZk4KU4iW4BeQ6KCz5fWfWwpyhwB/UivEKAIjleFvQshEtVXkvg/tO+IEW6EBHMqa063TNamHsz9MRZOjuN0sq5v+FrDbuNBS4aKJtEkZJlgbClDgsQCxvSaWFFPvJosubSFf9i24VZu/5U1S076m6PSxYuZoLp5rOq5OqhIEgOg5vt970CaBmf0b+ptuzKYxfM6p51gYUlOc/q2kpVboEYga7vGA2OJouhrysor4OEwnisBjEv98IFdmb1cM/V+r9EfYsscX/VVMR1oCZWKiug7nY91cAOSBlXpAen/8UDS08gTEkinyWwwXHaR/w9vHMyGkgtpxP740LxogePFfxy/ZCtVT0w4yibtAgcC8Yg3k+xkQaNqSc6Q89Qm3nMW8ePlIbyntIWCjKebkizRUM/ImShh6aZYkHZlx+BizrfrhKZ2epCtsK5JcM24iBSgZZeYL8VATeHmO1KQdw82ih3aKehN9kIfrl3PV8vjmnAw6eycVtnSUTi/VDUyUPLRI3VP3+e6Nxj3BWXY+rENGEdMaQVkGH0UNQ6xKbmEMBSxRndzOTqocsVa1n2ebKrvahhn/4r0SYHiIEu1erXBmDZ8teFuTuhqQm5LwIC7ykRJFid5lkTB1pPsLhV5IHjwerc6TK8VqTUzyor5AA0nhh8DB8WBHk7XwVlPFfxIvpE0cniD9hguAxlA6ibcvjmROftYtaom0Bvum1tQsOoiYWUpWKaOWvy7qajaIRqgLAOhJG+hcDeaEOt+hNw3g7zVEVyVB4tCH81RIlfeXu2C1okEEbKKU4je+i5r9KjeupYuwJizss3gbdkeAHkG3GKFtKKSo4Rn6EaJT29bLuNzRpCFhbNe/N73FxXIhQ7WsaA7irHIVbmEGjr+2ordVlgoWMIvQJ4bvrLfYGR5kcDaoYwBGlAQFWGT9rVS31G8dgH1mNrmrab2snd4XxvbeLtX+AFe7udOX9ZGXgTqsmA3eONXDsA4lFhOqTS4rZVt0WVez4IeKK1Bm+w0kedl4WCO2ZM7TjREOEis+29ZZr6neuT6q5L1gtDzNx0n3K9hIbmhaFuVA2h77XIqRiCk1P8EJCZJNsMJq60e0CO3c1kuZ/DisWBft/QkftbxOf2SAHLviyl3w9Gvu4j/qsRGW/AlICHa/+2TUefqzBc9Z4tTdwGBMobgHdaXYdckoeoRbQNB/GD9x/0HGMumhypNoEPIiynlcwAdfR/asuakzjKRgMVzhqUp6isxA/Ukn3KkinniVj5DMGJwUenbRA67b5cTGkZda9Pz/XeLUAygV5H/1U9b428v4qtaMoOox+gBmft45Bm/K7jJyrZyFFOCuD/06cJPb4F0Fs4Bh4hOgacLR6JL/RuGD7cmwY22OLTOfmwVe83abwf92qTDaBFCY8fSy22d/aFgWoiizW1T1wgF6249zjuL4U51bGpzkV7uX3+iv2tyVlyJSUg7ywuxGjmeJGKAU462YUi3HB3ZrZ+Nicel8IkG7vmZraaq0BHP9FuyluuntRH4R824J4FxOJoY36rqhU0Hdj/6oWDykbzDzMDNACmWRTTDHp5t9v7FNmDVLFun1cakb419asBB16SMFr1lzJ9IFnxc6vE6+nE1nX5XxyMoTK4CWgoQqlKjMU6/xuh1Jiu5b83gITOqxp5UPC5bh4ZcTkwfL/Umww9j70m5hXR7gjqzdX70o5K6p5FWrcPafFkUEHunNG3RRlthNdyZ/c1PRzGJJwVRNyCcJQswV/bzXuFfIGUr7ys8wnD4QV+NBdlfqtcqMeAXpjgweth+u8ERyZVrNHWgP/h1Z9czAKMnkJWR2X/txPi59fPM1B75M0LiXi96AcWWrEeILDvH20S1y30UIqPAAqp93TfG4pfoHgw4uL2f92MVVxLY46cxkl2WOzsJIuENrlgHKvfz1gUcl5mt1tlGzFY3RkTezficbWXROvfJ2HO+E7L1iLMKkE3M2VI4GnVS2P2bsSL/JP8SudoAyQXslO4dGRTXac18G8vJES0Io/fhgQmzPehwurwbWLymplWPGyY9ADR63NeKMvPUjqLqjpqhAQHXRpq0A2tdCrHDjoV2sdBqOZ1NIL0XBPmAGx8FuZA5eUrefUnNnc2009FifOqOhJxKSlI8FeBb/FDxxxR6uucXlDZ2jUdipaeY3ic/uZgIcR6x2NQ2ODid9b73pbONtH1eNHvtGsnwZGm2SHo7Yb+Iw/fkzQYCA05u7OULbJ5pokiHhw5iplR72QfOIXBGBLkQoFdMPF5fUn" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV0AX49S4XPonzyGHDwZs-R_VWAKpvYG8_cXVgn-Lz---VSONzQ1&amp;t=636692082980000000" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=qph9tUZ6hGPMRv_P9VwIOe223LfVgEFllbRPRF1GclrFPGjUisPfO5Fl0w9cc4fIPZ3YzayVwId39zB2TSz0ppxHFLU1&amp;t=397b6a7" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=TvpD2YGOOsBHrEswZxEHEteBCJr-PW6mhkwAauBn9GUjoZ_L4qHkIePrBjHiSySgQeFgrSMv30LeZqoea_8GDSqL1RiaMiLSvG0v8g2&amp;t=397b6a7" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=542" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542" type="text/javascript"></script><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542" type="text/javascript"></script><script src="/js/dnncore.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542)-->













<!--CDF(Javascript|http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542)-->





<!--CDF(Javascript|http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542)-->



<!--CDF(Javascript|http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/InhonorofShir.aspx" >In honor of Shir</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/ZAKA-Foodbasketsforterrorvictims.aspx" >ZAKA - Food baskets for terror victims</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/ZAKA-Holidaycampstaysforterrorvictims.aspx" >ZAKA - Holiday camp stays for terror victims</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/MediaSupport.aspx" >Media Support</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/DaromAdom.aspx" >Darom Adom</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Superfood.aspx" >Superfood</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/41/Default.aspx" >Clearance Sale</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News site and governs data collection and usage.
        By using the Israel Today | Israel News site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply1@israeltoday.co.il">donotreply1@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2019 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    </form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=542)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=542)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542)--><!--CDF(Css|/Portals/_default/Skins/ITD2/skin.css?cdv=542)--><!--CDF(Javascript|/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542)--><!--CDF(Javascript|/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542)-->
    
</body>
</html>