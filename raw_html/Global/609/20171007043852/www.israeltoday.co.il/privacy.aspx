<!DOCTYPE html>
<html  lang="en-US">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=539" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=539" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=539" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=539" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=539" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1y4hFjeJ97eQFfO8orrm0fbMaXTjnJbWjhlOXLc11JJ4V8ZgbSEFD/uZUVYTHAP7HgAcKsZV2fSGajxMfACWXiLG8AIJ+Ka/fVVvXtuKONlinGabANMKYZ+8iHcv7+7p0zpyadhGWeJ5CYqOzyA5JuqqrSRkYhPtsj+W9VRyPSAUwjn6yhBYd6vADtZYH3CpE/mLneFbAMXnAw30LgawWjCAgZR3F4m3W6R9gTATQM4a92/+UeNKP3Ir2GNiHei8GNrw/5ACX03UMPltTLBf2jK3L80ZVF59JpaQGUfJ9sbjkVOtkA0pMQ38fQzGBTa24zDLZ3Q3C3uHwl/KYZpJzNdV1rIIN/qvT+1vLBLaTE/55gFLULs3eaLLshOGiIKCXMDa5GvCtYRZXinzGZ4VjTKQQ9TLqnmwoyCcMPMwjoGhOacGETk4YT9Jk4TBs9sFRPVo3eQht+gM5+v+QhqZCTBIw1C7uiwTR/1QWgqZFmM03+MMKPZrOpuq/OnEp8oeDwO1rkqBx3rqoc46v4tdVw4x8FV1EMk6kF8oO7e/dVMPlk6LB+pu52bLn7uDKqPUea3ewpntkSMjPnGxYTxlgfRSHM7+O/sRsfFK3rgzf1O3aBSNj/NZBPiZHEheZoVnR/+nlEKC8UCIRcSUzF2MQnE9sj/+0CUfkvtFx/aoLhY28bQycB6pkR/wz2MuYg7zTn51OtMF93zkNkMslw3JhP7v73BqWavNhPfZ66jeJhCoU8c5QU9bJnb4Kq0CxsDbsyLTxltdDkDqULyo74esb9HpwUzqTD/4rHIVW2RDTuQ1aptCpMNKli7lRmeUmoCcuxD6OnsS4IkG/A18sR+AUrIeMeFUaGOAVS3Cyo+tpdRP5o9er5+rOjWCQW6HRjd1GcBKEbglwvlKhY9cFZgNvQ36dxJdIkEy+uJF6fLgapukz4iiV0vCxZPu2DB8SPPPl8LfJjO6DKAazAC7NR2/Lqaxoo2A1doeV+tKXaaKlU+8mKDYBB7ME7++/6FF+0uU19uBie5Mo1d/IUiPfkEwPmHU1YfwbvHyK3Iv7yPu7qa/4A8/rZzZRzBMyiSC2Qb53+fPByYLgahkE4BihAXegbgE6xOzSeC4wCfNB/n57RebO77Gee7KUNjCLFmGEMNGUSPelVYuFXrC4A59Ntr71S1B1QfQCcPqS+lU6VlcPAEV4Vuz7oTYfzn7q/MmreWcpzRXI0oGED1YeJx7B7DgiYN0CR7daHWsVjlkKYXsnrrRA8bxK4PKNchsMp6tyWiZgJDhKa8ONwwgx+DKwm3fMWtmOUoMbEmzysZX5XqQjDIuusX6RjBf1Hov9cb7+xh2DZtErCC6p5OxD5iHI5z9wtW7TGtpq6t2j3s0gantKfnVXg3IEVFQAOADwc6fYDxQVdwurvyVAATl2KldB2q8RDezOs8HBCFKwF9jIo4Ngz8x+43hDvNnD57FWo8v5nvSemlduUmPhy8pRB5zVeGpIMLZaD5EtHEJBo7d+ELFr0QaEMGhlaQR/zOmdbF75N2o/D7GcBWHA8VZaK9YqpsMbFa6vyMaWsExmrLMRXU32Da2ySco0WP8Zw4luJjkZcIySM11m1JypG2OeAJ0VZci0gNOd5o9iC8ohOJmldrL5K+7V14vzSn6nIDrEiimmPD+7u3OQtjVDnazOPrCBFrmI8xgyNIcXF2/3SGaSvjPOF3n64C1rXnwdjitLjXM8bAI/kvK4sLLEpPAaVoO4Hf6C2zc/qzvV7TvX/TlyP276DkOk7l+XkVk4rHpnnBzjcd6gDBidHDyihJQ1lu+Sve46MtUk9+y7J6vgu18oYSOyC3L+z02MbRbmb+0Lhp8DlPFckwKtOZzLUVnSTdj+2CegOiwwuFnDc5pZMdlbTZQfV0AnyPKy6shri7qKhwIdSTdzLjTBAmRW182vQikLvvITmvuxnoyyqDj4c5xNuAt2GqBVHbfc+dAEh9lBJpKzf+WI6zGv6c9WtZBEf9Eq5s/Sb2IopddxuLc2KKhjpL9c/90UGAMDg8ynx1h0IVrmNwzoWXrgNviveAyeqjoRpnoLnIi4FD4/YTllf1qYKRiFEPKbiEa4I9bJsr6wRT8JOXXKpxDst6wPwDUiG56qJT4AgeDStNh96kqVZAPZmyYxtQwQ2i8qU/ZLAk3rooyG8O/oDa9FZE+pbyqimrhHv9R+YcI8bgg7j5qlSD9C0njuid0gUi3cpx1Yz2/4uQxJo2BaMVQ+IEW1550BYnyvU8YfoxVez/9vPi06E8msfEmWwMFtQXSmTg9vzuvPuXJ9jbJKX9PDyuPZDnBfli5CWShYO7ki3A/bSUvK3zK8/KI94SELaT7VaC8N0hsfX2AVnpy/qNIUMhS9rdNKYFgPGthzu3zo04dDvtsTNeHEjZLUjVYmescC9fqJ1mEOrpFIqrFSLSShoMiDUcwb+yi033uKF5vFJQ9HCTwNqdtclQWUlVDWW0Odmzp6ZJHLZlsGCzgvcs2vNH4pzbyZDJC26m/UNGw1nrTKep2dG6a/CAhRClWXGXKloAOOEkD5o2oZEeKw9fxL6A2LWaeWKuMwJ1UP6/5oWnSeWSU3GM3ibVsGoOdKqQxLK3ZHtP5vhglb4QRdDxO3FFqo/NV2mw30mcrrBNnh8DlslnptoQNLiv+hLnITq52DXTvkzriTu8lK21t3muBi4i5/tBEuUvGiAndv3VAlY42litvDS+xndo1/Op6yMFkgg57VgVmfgZEa/vXJzWxGVeNNo3Q+pDe/a7LRdBm1URLlWzjsCfLbIh/M209OuaHisDg21vPMQY9LQdhVHZw2IihaJ4pwjh02dLWBcL3cVoI+2bRj4g4N9EFLSLI+3BDz3nS4mPtqxp7c3kgGs2uSl3yFCKb+Pu5cW20cQcxdIl0anJxKWgrf80zBUDvzy3r5gdtZgELvmsWEsn3EPEJQ+VVTv/AquF+SPGl2OX64E9B/SMn8K2tXGd20YXbHSrqAaQofyiz4Avb/f8X3VP40g8W/wmK5Vl4udj3sTctfdeuHE9gFirTm38UDh9nXuJnnix8tTMDR7Oow2/xkcDwJGBKFTYxRYfY2OmmDCTQMVnQ1TYxljv1AiDyXrtATNwWO7NagoMf7FAN+AXNm3I4EQ/kT3LtuXvaIiUIaZWH/t/vinvYe423yW12keLBmeX5a99N7mSfdTl7FjT7sdOzSHUFeV3470qsvB1S1MTR4TpSDk52a3wEfMzIqJyZNkkbjMJC4pmXm02KK78Zlrz343CEG4943HdjN+Ci9SR6VKzZAebetLfI+DcOVCDjfZMhYX7ab5ZIK9zcsEaP+Cret7bdaAGY/9ZRvlXT8Nj83DaMwEZ5NrGP4s5V/vMCnYF5z9zNQUPCmDlD71GBaPWAsQgMFXpUuzMo8AYOMsShMHjP23CijJJqPsPZklHcQ5Wp7tb1r5z+dh/jQBe8FqMmFGTybQn5LbHSak7ZjIs9ZN1xbzqU9FSHwXrsXqst1gTtcCKwpBmLA/Vsfnkp6kA2uzUXaF2Zqn3Pb84TDALfVURuFkX4nn9zVDw17eMytlvMXLihKSIfnXY3EUmNm9K8edVdZIfPKAtVA0aB1nlgU3CQ7gy5tB0dEQQ8infZgdYU0uO/7wL1m6QggAeMGOQfGEarhY0Tp7rDuvZRrGYyvLKBubVSvuLhkDQQQDBjwIY9aeWQYVGqEYGXg70iADhsm0T3U0q7y6rJMVfYLRlPhzNdYlomknKxkP2lXiGk8oP0IQXEODoOkL5IJvu1uDwyOalky52f0ZYN+vUDxOnFobcUScQGXDeIeTVMPQU5Wegs+p6PrQ/STSue5eHpK46puERWbRG1dRl1+KFQt7GYmDurl6rMZK3dyWit59Jci2+43/EpDjLddCfEgJr/wqM6M6NFA7qZD4wikNFXWoBFBrdji/mIf2rzFCu3UtkOK3+jGJBffcGNBM34N6uBi02VDRZ0HGCqyUV8vktFVLmiKsPmGm7n9vIivWqRATO8sCp2fQW9asFsQe4rMNoIKBelizIZf3Webw/GdzSoMz2Uz2PChiqylTfaYql6xHw55FZn+Nj0YrvQHF9hcKCvkJ8nlmR+IZfPLqWs8WC0IgDnFURnTR6kKA2y97ILmzCGeXqpg2LmUnnI1gPFAgPR6cbqcXSm+1F8WGTDJuKCner4qN3iBKuabrEDY/dULlsf33etyIIGrklx1wdNWLpObgQGtDcihzYEhZb5qAplMTYxpMPvX6dowazP3/zVIvM1EXz7axkKUnY5xoJ5vRmh2ck9y3KesnHZkPf2eEr/p5Ftkp/yBt+Y1l7YKx7rxc6yrUh1KwM2UcebSI7BB7/HGZOI11hx4giuXQhkML8HFxe0uMmj350HorjGeQJyGlvshuZv4RfnYEvEHnEzdc1cob19WSbFz67bqVNPefyJopO7GZ/IIfJBWeE9fKrQ5TX+gablv1x1SX3qgmpB8P1F2JuVmVf6TVAW1HWE82FtiAi11rAcD/ZXVNfq+tBuLH4X/1GNKeq8J9kf7A4jMRWIm3e/1FySnEyE80SvfWVvrH/kTbT0Czn2k+9IHZ2o8uL9Via+DN0k2b6kZPPmF2gX8XQaQY9s1L2DSBfSxTsidLNjWnH+AZ3Nnrj26REUp+R7tyCQtJ0TOgojP5vj8T+5nxMBT9Qda7AjS/PMHkp54KdTFD0XuO2tnnpyJ5XDUySOHEiXHVR2rA+G1+L4YAmyQJ6Y46rwPz7wNT/1ui/SiNOFXBbcnL3JINhUowyhMEjQLuWkgiLiAr2bPxu2ovttkRJrR8d3bIQQtlvLb0IDkXzxxDPXhcxfuIUPJk2um+dGe8z1whhYNK5r40FbbTshzmcV2KwM4f/1EHyTiG2/Oah5Zrm24SU7Ri2DyGo1Tv+Qg9iMvGtKOzoAT1ZwkXXf8bY+MTIBfRjiulPHhwjGsHCX/jGzsUtwN7N1loBjeZO1gLIY94M8uHDK7cZVeuQKxIUUpugg9byen7/BzvOBu5ueGK7VQdyxR0BdDG6RqTWws/zFKQIWzzRZdR3LbigdhX++TV75w33rmm/FPBOl/rynN9IpYmq1S/X1FBAiF09cRsPD8lfdwa52ua8f4LET+ExqptztktLRwXGdOWjN5ozUrgBjObg/4VqfjinLv3myq97Flk39f8/t8cJICaW23wFMJ5CLJCqHuAr7LNdIqSlgvlR4GlxugpesZUbku9ZceC0w5Lsj81I6am+egtqnYFAohmYGVSCGudsv4sjhFuGME11nEtCjJOLn9IF+ue6fZKIL2oKdAFInRwG475YcaMcywGki4EHSUn+q66FHOUTdhQ2rWr+MIETFipwwn4lbJ6+8QYgWtYUhc7XnjwQK1u3YyUtkCwhz58xvykeOBqgxlw4R6Caw2NOzWGXuixwl1DD9U0nTR49bLpoO8MwN67uXUH4/WMx9Iv7K5+SxHi4XqDz41Mgu7OWSRXsTtYmAWCPYB5Vd8NjZi16rNqlQHKbO2cms1k6Vjjo+18bu95XTH/dPyDGALAJdgqkuOSCMzViiRbYhmjcxQ8fodJQiEmvGSihR9FwwJlfj6L9Y5MdiRJYbxytAcAAp1S2KLSwv62CMpH33EnQ6ppxg3OisjrT4UwFsKlk1Xb8G6E5XpORCvdPJcxeEDrKnjkTZn8HJMCSAUmKFesq32nzR6rol43SO5JpRDhYrF/VPE9BbWdeEls/DgsVq4k2WGPBbfdchQdHWJ8vHPPx9zFsBS1Kda7L/tiKYbQ8AbmeLdQQ0dsafkmKi1KnaC9ivhskpVamqwKrxqq8rBgY2pAUgq7kMXYfL4q+a6fcwa1y93Qw3QS0dvVLsERWWbxGjB4iUPOCveUuieOt7Vfljwes5WibvIKT0AMutlUbk5C1t0rmPLHfwZZL/ZNzuAh51jHTT92SO8ZMf9YanTcx7VC4M87JeUGwntUmjrpPWlY8pjASuFkYVuw2Y0ZQ6gqyOrhogQNQswQfJoJ6qgbaEM22lM5RUmS0PRb0ZPMZ/PKq/aCEdb2nQyGLifyDK9TK7tn25QSZaUDrdUeXzKUSzvY1rFxXDfZr2WwT5CtyoADeW1IiUKvIzp+RA5NZFKK7TcuED4H4BMzR2M615pO8SzxUK7hNooccsb0NiDQICDxaaFDFNS1YMhGH3JcTZG171kO5d9oLnpXHR3DgmETEBAyiOASDxmv+8wz8NwHpmyEo4fuQwzBNf5DHmJlnvxl7JX6Hy6dnKbDw5tkQbKNur5W07UtMMmxe+lpZ9Hy6Z858KFaf9dFdU/Sat/fJWMlxYaOVHNCiqMLVXuqCW2TYfehv/dRK5yo4UwfTAFWrpSgGnYjNSLvtvS29tBqtzojrxyN/A8cW+QhpiDGBDXgjb3HyQDwbs+89Qtlyc9wZ8+5BLfZPawxmn/mC2NWm6g3h0cTFR3PnRZ8uNDdbVPJAx7rPxy45lliLcv9vnjS/1ygZMu8nvHEuwlutK/IUxqMluW8DPYv8y6NSo7K35a75TIzLHwoN4PCJNGnF7RxP9IgBUxqAWcMSfny3ak5FnCnyh211GkWRQ7GJC2cA1fTj00Ys7A1L5JzmApqyzGdNecbwu+dSiHbhLYG7BeDjFkrgp8IijlgQ9bbr/SDXSw0RTngd6cLz9AfBUbJKs7wpkKhqnOCnSWMvGqV7QBMcDjDIUqVzdABsbgMuCl+t+gaa2JkDle+zuowr/lVMwJmNlX7QHfQJ3sx8Y+Jp7cn3/sbH4ff6Xh/4pdRr3r9JRs/8TWi+pmbarodmJIW2NDuh0hht870+c/ItnXgf8XsT8+R8icXdJgPArqStGnAp+zmlJsBnojkV1OvtsbdbbIGqfuapjGBWP2h6PEuTCXo1GFAoLsgumecpt86/hA84nFcKYaglmW3X+YdmwzPvgPSxVT7r5ErPazH/jNA6PEMvsKcPO/FPWedn4OZZoAufBtB/uoteeGNPCB4AP5ldt+oLH8dQBONcOG4h1OuJsvZkjLW2rKqrLYG5RUpve9OSrnuIT9pRB6wWbAElQa7uRwLk18FHEVZ2+KsbnABiQCqFZLKoY0Hi2vQAGjJn8rz+0l3JvVJ21tz2z6fh4vO8fP6l7xoqZFw86RRJRVHkkmxcUGt9hJA7CMcjRHpYiK4Oh4b7uvkClb6ewBE1KcZy+dYvKgv5BmRCZW6X4Y+Td1O9EB6TkjHyLs+uIOA2LRmm7UWlJpJ+1W5JC9wNcI72vWN6DU7bHauBUSnU5TpTjVoHchSANOS3IPUS4y0Z6r1K9Ok2/dLiiYCqqawnJLvvVBINcfbS4AcdDmCIlNPV4Xavar45v7lkNHs5EGOlN/xTF9J7kjR8n+26Okd6UjVHA2c+gI0DDVOLUhRTANs9MDyvFt7M7vYOTpVDktV6adJVkeRgoKjI2sxgh4oTghnfR68A4CBwLcAcKi4RLV5xK+t7NIR9qHQO080MZk+rqaMnHS/opwW6U+TIEAYDnV/E50ualherURF0vS1nO6vqGYV9dGaAackvTOO+ddlzS9B6BddATAduu+LKnUh9Q6Wz6yHL1+65LSh/+8UwlKVa2EQGeajJBL5RWvglW//NMKdAXNIvbs++XFpVSf3lkuIxaLHCGAkT0SXXpXWeVg9XWJTwjDB73UYf3UX07K7CwwXvxU9hk9RjvksGE/ftWmO+yK1+Qiwqoltpua+q4JU7ATPAOwle3DCQaAlCXyly9ybN1HjqT0BZLfqrO5BirxcATKNaScOKkOlE3i+DECqCv8Ve7Y9j6HuGhz3ny3Vr6ibI89U/XVrnWR7D5NV2b022Bhw30thfiR83xPchxX//PzobnXBa+mqKV04zqztvM65nUQfw5VFiT9fo57W49NM7B6MCaoUOaBCjw+pv8+RVGDoJ5BfZsIa7BTrjxrG15Fh1o5nVXyOz24DthlAIFD1iD6XPWURK8RcVXW4SVkH7pDd4xXWCp5/yR7B83vycRI7n/VNIO03cY5GFZ6wZVcpBXuLY4xvWQQ/4NBNq3V4k/daMjXnJu8LUGZQE3Sy3dI6fJbw53BhwGUW8U/xYUE5VcXMNQx+klZeAFtut+vGebpOZg4PV7PgTj1dhMieZl31s31pEGphsFUI41H4ByZqN/S/RlzcZGi45z5Wmm8hme/8jYi+ZypW5efIhvl3cDOwllUPBSY8FKakTJywRtl/Fd1epF9D2rJBtoV3huyI4KZo31IoyOdAPgDkiBHvidzRvhf8JlfLiY+fMFw5tYPvriw4GVCAyJYQl7A/bj7zjTQkJ14Mn26mDj1nMYsFNRPMpKKFhD9Gs9RMEDk4IoNe8YacRBnseAKxs3tiftuZD43hdzNZFAGU/P5zwfBxpTw4qW0YLJw4QTl++C5XlfEvt3IsrhiPSmeLxtOybm64poggngQjPLkUrrQM9dBtuSgdsnO/bHtubomHHzJk0mE1F5jzdx+7m0LZ/1S2mkF2r6ZgM6PRq4i7iQJ+rFtg+BcE5084N9IQDS81VqkjvK+4QhrP5z9eU9hSpD8oK3esk9Vn1GO6j7pFCiEsNRxdGjeegpCH1GptnEeiOkhWXQe57RPCas1aK2y7bemhBDBzWZ/3b1cR6MBH0ccFGdGVjvdjBVWyB9QBRnl+sp8aPzh+7BIWwrOnjtXgiVdLk45ZNmE2l0hNLP/0XAZNkzae61Z+5MlxxUfCvnmz5FvwxDGzH0ysBVWDj7f49pbuH5TkTopU5qfCK51hp0It61qGOo33t1vby5Pm1IgimAHcwehacRChrRgylB6nVG2MUHBa+kqlapX95iHi6Ho8BdK/rPJoMYw73wycFNCROnTVQetCjBoTq8HTN7vCHaT970uYlQv32+N88LokdQwAolUCK20o6h8MIp4BIMVbvBuAZqCerAN6VIwpzmHBAC4JZrleCAtrlj620L7hIcaEJ7eMWspCIvlsPrErBfNVeTeRC8rY/k9xqcrhN5ouSCJ2WcBBlJ/zrUWtfJCufOiOFeVf9yub1ENm54v3dtnn+m6WJdD3ScpKKH2v+QSiaqW+0LICXT6tH38T6eFdN+AbOUrRG6e3EaN/hQQ5ntB2GJ2bNS+XoTDjVVDGKjYWP+F/VOJ3IvJc33RFoIPDL+4sgakF2xA8ZO/K2j9NsOBJzqDBf8Vgjaat0ohI9X2o8xen05WwhgxTrO1xtiUcDGWSypIKc+2VT+vLDrbIDipvFnyJ9SBG3QTahBklVenTwqI9yBjtk8dZkYPFxEHHZD1e5WLVztKVnoTMJHRUrbyjYMy7tR5aSV61ggWUfrO8FCZ9Uwzac9dyc3wPfCP+YE49wivHNqdBuIce7BqhfMoWLCq5puuMpcdH+CYK+9PbP6PHCmGSDnZ4SlSwzhjjimWKXFwh9H9NE81oMqN3Xe7CuyNUmYN638xvrdYpjo3Dyr4XAGBD6021dR/mRBVqhrcPoWbZ3XiuuWicqpjjBsXEHSERhUmIAaTcGK6DGP75YYhS22q0SrEmjYDYy5Z8oluGJnkf88LCm3OtfcNbW8u2Po7DZaZX+INqE9k3U487qgDwOh1H6Zk0Bjnlp6TL+0Ym/ry4sosVY/Lyo+3eCSbZLoPQeS7p7vq/6F9yWqz+IXJdGrsHhOtGSlr04h4WKQhZbtSeXm9R5bE0I9H4vmCKOnE9IgbNtX8CnyPwhv1tsStsSjMmwXM7wPO1CYEDGTjtgFwKTKv+akIF//JuOhtI6olqlYXDLCR/8demoYUBfCbnX38POYvDmlOBtKjzCn5Az0wlp3y0q80ZCL9MQ2LHbWHDcTeEpVI8dGfE9NgWemSLBq5oQ9+qVQpw2MziuPPNSZrSFlX6qKcJgWTat9fARirzLaAN/SjY/mKLdBi7Ts58RehmBpfN0KZURs/7eLwOW7UoAvh7zFvRDNHSPCMtXewlsFPVAbePL8uRdGn5HYXz65UoCAk8ww9EXLRC///Zh1rYD2lSS7k4LdoGboH5SiSbMHKLsOcdW6P/BHwC0yEP0t42x4QvzFdmodeiXy6LkFQJM7ZDlZNOV+ulJzMkfAtPBr0IRUirBVGKfqf46YDXHDh56HMz/mMNJPVCq3Q+T7X3oHLPqXNMbcegncawSD3sApCKDQwdc42vfuOd02n+sbPee51389TltJL2Ohqg9RtAs5sYWG3BPhh+gQvNxi3f5bg7hcKGtFbyky4zIZ4YroYMChqgOvtqzTt6p6eP0KURUPBSWLUk/BykR0VYGo6e9RFEpD3FaaBELmRTWHluuv748gZ0U/fL0EMiyAp5g9kBIKWRN/6LAq0BLLfhM5KsvhogsfGwTLk+Xqb91M+loQedUiflt5qZPSREp0Ukmq98IH5nM08+masui+JK1gQ0mThzgY/ZhSAEYi7iyWD5UVCgHxfl8l9m8fmMJ8jj8M+csWDIMa36L0xt6Xiim13fZI4VMQjh+jUvnPrwScWQ2sNC3kb53QWr6zYqzPTyFO9jL5Dd7DxH3n/hZLSTYRFk8Dj/Jy/0ctHXQgBPmIQSK+hlwV4cNcfI0da2lh7BvhtyFvrmHZC4Vsm3W0vtFwiPDqCwLMf1bdcPaJjZgH3nLiqr+zYJiP6cpEt6vkITO9YVLS8qGfqG5xc8ibb+SyX4lcqYDcZzmu3u2OJJfmtP7VDDW6sVylLGX15GyA0+V8NCCM5LDgh0V+uS/djkTcG5RDHejNFvDPlcCq/vLqjmYdgQDB3xvsC+DAO7bI02aWP7q9kJjWiFaH6Avv+qU6Gsm1gXF20FebmPt0DDiq09lILdW+/D1SFfwxtK2Ix065q9LRHgWXMTtueilc3Ynm27hTB3gxi/kHpphrOdvcEzvzUE3EGkwK2g433SI5LF6z8nwcsvlvSXx3CYjNIk9D9w3zQs0YLEa7jnNfSryfem3Txv2nRsb7LjyCOkGYiamAOar6QEQk960ByuNQBM+kAfvyqslknyv2TBy+qrCoMcMDss0ijNW2TbNvwKIPwqzPOpItztI9XVxaWCW0sI+9otwD41o97Mljcdv6uG9PtPWmPlDZ76BEqu7KpQCG2of6JnqwfrZdLw7FsJzZdsm6Rg/2zE6BsEX4gEYyteHBxyCj0w2OtpTRFwj1IyfKDOVrml6pZzFRl8dSoCFZh4xAvUZXD/92ZF9BZzxsAMnwKaDoO5sopAv+d1t5g9qmTKQxW/ySoM8GljMgk3vYJAqTUWWnq7tao2QG2yZ1USx9pFIt5C9246qaq9cCOwSCfqkVq8+KSeRhMZvm4F9p4LMauhkRcpTpMwffWq8qqHGNopKQgHm96LNNNF+RGJCGeqDvA+ayTOHjxy/vEVtLoyJiIL0kM0YxvtFhYmsk17KlCR/NgrfzVl65hQwmx1S7PLz5w9Ds5fFS6TAPiKvFJsosoqnfrcMyNXfK357mh5KlJ77fnqwoEunNaqvp2X/iRUrsJNmDrsbjt8fZpV2xc50reNE8OGWoja8b3L1+" />
</div>


<script src="/ScriptResource.axd?d=qph9tUZ6hGPMRv_P9VwIOe223LfVgEFllbRPRF1GclrFPGjUisPfO5Fl0w9cc4fIPZ3YzayVwId39zB2TSz0ppxHFLU1&amp;t=397b6a7" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=TvpD2YGOOsBHrEswZxEHEteBCJr-PW6mhkwAauBn9GUjoZ_L4qHkIePrBjHiSySgQeFgrSMv30LeZqoea_8GDSqL1RiaMiLSvG0v8g2&amp;t=397b6a7" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=539" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=539" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=539" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=539" type="text/javascript"></script><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=539" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=539" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=539" type="text/javascript"></script><script src="/js/dnncore.js?cdv=539" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=539" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=539)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=539)-->













<!--CDF(Javascript|http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=539)-->





<!--CDF(Javascript|http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=539)-->

<script>
    console.log('test');
</script>

<!--CDF(Javascript|http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=539)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/product/tabid/61/productId/5336/Default.aspx" >We Believe in Miracles Project</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/TheChildrenareOurFuture.aspx" >The Children are Our Future</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/MediaSupport.aspx" >Media Support</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/JewishHolidays.aspx" >Jewish Holidays</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Superfood.aspx" >Superfood</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News site and governs data collection and usage.
        By using the Israel Today | Israel News site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply1@israeltoday.co.il">donotreply1@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2017 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    </form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=539)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=539)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=539)--><!--CDF(Css|/Portals/_default/Skins/ITD2/skin.css?cdv=539)--><!--CDF(Javascript|/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=539)--><!--CDF(Javascript|/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=539)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=539)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=539)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=539)-->
    
</body>
</html>