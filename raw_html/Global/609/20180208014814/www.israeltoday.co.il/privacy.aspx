<!DOCTYPE html>
<html  lang="en-US">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	israel today | Israel News - Stay Informed, Pray Informed
</title><meta id="MetaDescription" name="DESCRIPTION" content="Latest news from israel today, the definitive source for a truthful and balanced perspective on Israel" /><meta id="MetaKeywords" name="KEYWORDS" content="Israel, Israel News, Focus on Jerusalem, Jerusalem, Jew, Jews, Jewish, Jewish Roots, Judaism, Jewish People, Judaica, Messianic Jews, Middle East, Palestinians, Palestinian Connection, Politics, Prophecy, Arab Press, Holy Land, Christians, Culture, Science and Technology, Economy, Military, Jerusalem Depot, Gifts from Israel, Gifts from Jerusalem" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/ITD2/skin.css?cdv=542" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542" type="text/javascript"></script><link id="OpenSansCondensed" rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans+Condensed:700" /><link id="FA" rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" /><link id="fancyboxCSS" rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico?ver=2011-01-11-161620-423' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-284686-1']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <meta name="viewport" content="width=device-width,initial-scale=1" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="CobFxGKkNqF0xOvphRTZWvKBlWS/1AW/VyTD7v1320sLVflouX9afBk/tEk8qNYCqqqaYANIQGtcg7WJsK1XXvTZQKm2SJSym9D7bgKLM1dUZUnZJcHkTS4f5Nub6QiPZUcH1BR7MdvfiPKWXTmoNW5uQHW5H7BakdVTeHSC/vO3ZzF6lZPKml267x0/me2zU3F36tBsSjS/BDz020FIGOgbplEbjGWrlpe13Ffk4Cm411xkDUNRKnnoiJsw49ZqhVu8Ar2bYDbLQi/PyLYBxRtyuZVSzGRc/2uFyEJY9oFbTDAtX87sTASfiQtcYaJfVsFjeTETE6oFnBjErICjIBZbRPqDFu2t4+yb93lHVorPvg6c+po9gH8UlkbC6D2MnJ10XiL3z8hecEofDGt8+r9NuXGV2H5sy3CTOHH9jrRmBHGig0Dhm7KpvR7IdTNVT+f+IRiufxgfTMzwOh+eHbXdknZYfhCQMnniRofd1qjdGzuxrmGnJpfJert1yHz+bBlt/ZS2t+oylveILIQhUn674i5wbkfPKstx6P8vcSTWIWvhbYR+3mecpKf0pCZ8gKLGLPSMN+vBwGWpE7AC3YbfY+52K6XW2LGrlpc0lL1BkkvS+Egx4apiZ0Zu9BU5lL4nzB/2jbqXDGkF9YH6MT7Tf1qPxJV/79eHLZwFVOGczy9iTsNGXyez9AeCMGy9NMDIQJVH0t1yDjcVX/vBo/qjVjlM1lAu1iYg24gCGGSsYsoI/GOT5xTg/GlAsr1zjwwywPhIsWhqJtSkkHNViKESen5nynecDF53N3agds6D8j5NqtVPOFIylh4+SHhXdbi9i29kdOomzhgL87+mXpCQxk01gaEGGFmTHdQC+heScAKdW1r10CSIGgujTllKIhxl4o/zDj4I45nZlWnPE5YmmSx58fN2I66/E1UlY4BJCkcMjmihT/IqxwaW6he3e/1UKMCv7+dgkwBJztSZchDAQIJTAbsFdTWv7e+4WB+TJEbKHnsjPjEqvjbA/GOHeJ7jMF6pwJCInVt+qgsQaMd3xe/Ohi8gPiwQCB56ZvLDS9DWB3wbOfbb/84O4PrOQMhlLt5q5vKMlTmHHT5UsI/JLWv04HDRf4qFxiO6Zv6XiPWghaOP3BjhcaJf0Bvp/+pa+y5kRsrd4y0K5w/ZR3tUQTpcxoFiR37MRn6KH0jj3CKLwApmm3PuQdquhljFVHymt94iBbI1t7yIHuF4apubuM5GMZnuwvdqxmje2Jl3onZa4HW7wCJuLP0QnaeDuwMSr5X/e2dAvw2i86xFYnd1SCnoXYmRdL1xXtmHuDkVkmQFXPMTs55x6sKYJNOBttKO/o1C0HUI4SRnX6b+6a1eKZ72InIX2yTZUIDt/oVG354b5xTdMg+syxNFPFTf3zL62jsJwsrwDJ/BZqzUfNqowNjNLRqtnS3fnv28CI9TB0h07lmTxuBEZZ0qsZdicGdofFxc+BlMptPgdg1iHHEIOFWt9fYxK6XzDQO8DjLsqs/mKnqUSoLJkmxKLgObQkCe0QOYAdnv90o5PXka9Qzt/13Yb1Sl2nhXeuJNATFIMII9jb1FyMa/RUCvH0QmG7dy0vIkU/DoY4QcpglRFCZHLm3UiNAp58OTDD/yhK09yp5oLMqRoXJpWdjayDbG9kUpkZ8sffxLuQqGmtRt+qfDZP8+fVtoY0ZYHHEdSyXs8aJTEeJVPPI9bXKsNlpKq/d+dVtIOc5eAr2MEp1jJ8xpiBqjbusKsYvKn6PIo2n1kQF2NEnYDadL352VE+GMA3B3opdegvFCL24JzmCyqI9wOADIekmLQoeqVcVrBr73DaLAdA/yj3MGjfMpT4nhAJQplvQZYGbn2Jowx6uypulBaSLHOhYofjp4az/NDysXwtIFqKN7QkUdc17xWxGyO0QYPb9YO2M79OLoHZHXYfihvlhnGkigfWaj4NLYV4s+ia+3Nkmmq2tBNGPd8F5ydirGPFhgKamUQkPd9VyPvwhFKp5aPl+T/8ALP56T1GAjkDQhOsuZAa9Oz47GV90ZZO5LZ8WuYhe2dKuwi/vWx2f5iKW7GjiFXHLHMt/vwNAue9KkKd1nPR3bhcZsQZRozl1Vy8aswXZsVpNTiEhQ+3yqLRf3AkjoaKqAr//Ah6gzdg08GwCq5raJWEqRclxzvp4k/Cl4JV8KiVhJYe6UxKAPVJy3xyEhpw6Sjm4Uu8sRPZ72gHcffcJkfQ8tNTddr5WhRLqEuXKNE7KoZXUbNULjxyn177OAudMulXrIp0ax8g77K4F2TiXOM5SlBh7idVelr5EM7DnCTeHgNt32hRV9CUzdRW0e7F06A+nUyT3IqhDuy6rXJVeG16ijoEivHEaKELmK0CbNof1X7iKb9nnIsuYFgBWY7OVBmV+8/HhZllSAk3J/5hCPkMftCzQWSnoH+2QtdgqDuzTmz4z57GByFT2TQnUrQhoQvwZFq3/FewuBtbcdopTERAZKo2l6YpZp0LWEcbMYTkjk2+QgqF+CvOb2nrAom4ZPxdI/vhDok7V+zAtQnUCBY/nIcTBWPIG5vsPjCU9L0+vPGTUZjBzYgbz5QPhvyDv6Iw1Cq4rGkzGdEA/BbvzmEG7ezN3vd3KC1trOs1fku7t2scL8/1VxmjT86Duom1EBG5z5q8VvqKPM9kTMAwgQ5glz1d7ysThPyLx4Hf9rKNXSw4a3w4sS54Uyao+SxxJy5FHduc0SXXErKNKbr0frpkQFVvUkXr0o5eIaAeZHE4MQch5by6bLoWuBWsXO7kQR9GFwSPbkf9x9soGTIz2C6UwOFRtc0Yyd5zTOKI5AW72Mmo6BG6m8W81zWq03qw7yPl9rAFO9pB1BESbfz2tivyftHRwUvwtoXb8eCrZNsw7diOsjNmpTKu/NlBX7hu/hRpql9NwkXqZpqAFIPhd3R7unGAe+qzjjlxAubO6g3RNNfh9cq1A7epRmHf5WCSMXuC2JLKjMIMa6lZR1vGzzm0BQE+tFsZdFcFG6R/+2SpxlrUabNojUpgbg8yZzJA4dcLX0kXbRYjxQXbfttniYlV568ccQgMFloGnkxmmOaPsruoXC/aBMkotogt2keZdwFU7/ovyBnb9PtD1Lik8rsKVAFDG4uLvAyT5sCz4BdnqAEgwSjSpbJ7Vcs7HUsWPHbJRUKJGZIdb5MrXXj4xBy9ANoQvPZkYS2xjjh8NLzsbwOD0Bt6BuITqsr0FsFU3bp+gCTu6el0W0M+v6imTsRqAQUoTrgj9wyLHB/mrycSgLLZZ4+hJkWVZCsUD1fPZ0j00LgETFoRCQE5s/CRPcqrjFPtukDSyQaiAY5WorqMTL+geMMF0rf+kIe3YzIBMcbzmniqwVeeUzfSBvBZ4mmDAyh9HblGpymePg3YinimnXpJ/FX1VspTGYHIVJGcEyuBwy6OaN78x471O3RfFBXFGs66KIkRNKnZH6obpkEGm6gRpS4FwXcGQYBd07pHgbkUPk7qApFLx465mJm9p1ORn85o/kQ2s/aZVKaZzaQ+zyMUDqh/3fsGDCvnd5jFcmdWkhZK8jUy0rahpG55sYsZhhnYuG0FI48zQf1VrB1FspPs4M6cTujQ0Mdq3Uu1HjR3gpSLGftJrVtiMaoQxRTsd9lbfm5CbMznxceW7XcFtfdAx7h4AllDxUcMVUoarl1drtnaYzfUebV1BPa7cZs+1j90UnRqF4F4v56hZ4TgiF0OK19+hGFSF/GsaG9piKTycCpxj6L6tVqesWU5IxHa05F3FJbRRX9veia8A337p93TvSakesCKk1qzUl6ttyZnNrjoJlYBP1eBuWFnPSwTQHR5vSQrqA4ToDLxBfiTCEbC43atOZpknw00FG3kUSoDLBkw3WmxWlhaE31rG2xd6WMBPxvhJWzF6Jr7O4Xg6XAvCWwmvCVIJcvVLIs44+4VYQUVQt3lmFfD+eXlWNqA+bq1eOzwLuIMUJZuftyt3JYnr4eCS9/B/j5pwYiy/H+nMp1RkvJpBA1efyjQepZlLqCoQ3eIbZXJG/pTJcq6jVFjezkEYreo5pb1t8ZYx8+rZHgpnXADE4CbkofsGO4ZITRQf1v2LFHHzgGXFg8zIuyt7cC6AUgP4KIv7hNRmtZZLoTuVfJzrxLz0rDhXI00WeYMoCdl11hiPSz+8CJEg6mcyYOaJIZ+b4uaZn/E1NLNSRTnYlENf+rJpdeXTd3QTr7QJonAz/gRg6r77TIP03mcIsE1/dNBDvZ8ze41FVJhXpTw3v/oDT8Ta0t1HaC7Xy0+iH/Ny7vSOAvvJYAufkqGqr7w8FmFhAK3Uuyyp4yATAm5EaCs0+dkuXXPJsisACZzQHXiIBOba0ryTGQv+PDbRIrmCeaHkQSMXtOJD+7Ag7r/KGbBvGS3qqdh9k675OMovBu/pfvTlH+gQMq/pb/gX8OfE5xE1cS1FbpRoqwdoCyB9trt7Dbgl2nUibhqMy89amNE5IIz9+0E4hRn1IGtEqbD+f/aB+3Cks46skTGRwyBPp/xxNyHxoOcujzV1j30PCOBNvKuGQF5gSCW2f1ieTvSVxtoP+RYh9Fv68subxCMj8F0Gh5GMFx5kwQ1Uj4PL6n30Kyh/Af/b3AYaxKGrkrWmkObdTa8406sv9h+SWiPLuZppu6FK3BFhrnV1X+1S9VaQIVU1v0gx6B/yFxsSZQbixgFXdqEG5Nwdk4bl624QjLRMaUsfJneJ56AHhs0DKAXsVZZ7i07PJuQeWzO46VLoSAsoiRFQeiDtay7HAzILSm16K0zvSPHNdWf2q94cvfpNNtTqKCjkG5e35MuHkzRKNxk8WB8yc5O/x6UpnLTqjbrAfPMHhS9GI8GPwJh7mt8TPyUdJgAAbnrVZL0Nw0yab/A6jdWYnUxeNTqiyKg7FEEu6W+hHwiZL5TSYRcQPSCu54M+mBECprySimcGA2CMGDOKvrfeVGnMT14nYT4Fjhbz0OlbirfvDeUaoC+Flqb+/zBd+m41y/V28+zyqM/qJ/dFCqxBpSQCjLu6RXi/dxJg506JpLPvQxfkQ2Lenw6e5OpQsJhJOupTzwM0iZraOpRj7blBozT+CW5KZgTOIuaR/5V6jUUPocuEUzWPgHd8DSr2N3VYK+/cbmkWWXLvfREbIjyjK7+kFdZUvCH03HV8PMjcsRFKMCwVczhUGBIkIPcei54twWUVM80KySMSNRQMuuwG4FCDehe7HYKZzqN/ZDhhhtsdmv/yrnxq88PyXELp3vD114uJwZMN3pW/wnM7oFnE4uxYG6E3eKcd1H5Q8sPLGzFcxZHavsbJnF1jwY8/nNh0gwJpDPKRU/GUoSqdwsC3FyoNFA0VljDq7KpDgG9QyXlYfbHQ6RJg8/b7RetrFIZ+ZKyvHKbZmGfQU2u13COvX1xdRZgs0ZevwyrjelaxxQ0JSwo7Q+guBox2vu9WARl/yw7557yQ3RG/vB/Bi2nJfEc0vnaz0YkKkx8UJzvUBFGEsRYyX9YEjwakUuWkWeU7lxidRM11U9qyoYW96mxOjlkSiAiPn204QoARIFrfsWZcvZaikpYsA/48aM2HpuRb+2Vrp24XPdSZ6y4rnqtUV8LrHdt2gpD0HOdcKncOCJSC/0qNYZZHVZE8HkCne3cSYTpKMxmZuaVy21wClYyU12zF4rUU5H0Z8I87h7pvj2IwIC2VqQ5heaKKdpBLR5DTvTqKF8ZIMfl9AcpUJII6Rec6isSLVAyl0su/myZyNgBy+TP+/IecGl6D+WtNBIh2uVDotrCNJs8xU5wRiY5z5AcuMTR/Cp9MQl6opTCkz+i0+kPXPL+ok3IEBA/0cmm/VkmMDLh9M2g7/qJ88JsxFpXGHhlfrx8535oBPMkAUPTEW0qYiGXQVFg6jGBrVasVNrBN7U+6YczbkKYE5njPmihhy8dXiAmdbzemUpGpp17SA7kT6ecRQDHWl9WFCA1Bp43JulSv3g+Ng6qDhZ4W7yd0slYcB8B2g11Eht5yxymbQc1HNRjBlDoHz6OyQGuNGVUbYL7kZ7Y1IelLvA5RSz00tBMfGkEMCktAz4JY56/Vj7i+0AoitkNFCxr4dRgMK5YZ3QnUjyeq36f0gL7Vepk6GGZzPAccwyTxjdgjlzvFNWH51qTdKqZXk9Q/eLJ47XPrz0JwDMDUQjIwE7sJhQssar0ihASImrXBzSeskJ/IDLjAIn7z5gCIzXZaRfB4KrFRcf6KOy+8kXFsRv1D944ietaCvNL4rrRGFWQFMBND0rugNtxSBjmuiS0fYAab8k2hTx/ACUv0ZMW23PKsA/AXAzuwYr/kJynmSyatSWJgORU008GGbWRFo1M2RHDXosMUX5bHBM0kIeLQf7SPadL8x7D82RROqKJQ9yjpsw2+4u09SmraMxI3K9ugByhehAPK6LawNII1PJRfhmjuJjiil5EFzaVxJVLGrhIKOTMG3rFjrtLh5Fwia3J9GzDUmu3HLAb34BzxtQp9DrxjzPr2kZaNtk8RJxPgcuDoirjiewcoU7t5R+gr/I9J+Xsi0KyA4pocRhM8VU7HrpeZME7Vicv00JVwRq6TqewtccS3uurlOnsEffFHqyfTVRVo8Myo0qYNWtebwJwena6/8+1Jw+S4pFYh+FPopJfuBD0ZVWrdT3uik6uAS0/c214D5EbtfTkayVHHa9fKGzWZceDkjVMnP/v7dzHICa2hTrStZx0QDFwzWKMh7K3/J1qP4EaDFRoUnX8GgL4d1tkBv76Hf27otvpsxxMnIooQCSxcXMBMWjgKrH8C2XuHdo7KfzsB/KKCZ/dAiS+TWL4n66QXh3HI01hyMeu1lpnH/y7nhe6gASMkAAhkEb+GreTmFm1hrD1eRanwXg2VaVSyJ/51PxylsYmHD8ccEU7pjL8BQuaXoCxxPeqrBF4xsqDJxZi4XJHVQIy+O4GNuA1n0PDmOyh6eEj5nXD4PefVWxP/fLLAMS8z102a2T4Tn4N4y3059xlDqFhUBV00pKbAY/ILgCTSXt6FkK6/TOGo6sHGekeRB4ndNEY7ts8jL/VOBB1tWatDuBORaCZiJFWqF3VaqZWlXFTOdObf1s395duIZjJSM87U7MZz1mGpOq9XX+rYoAOAqJ9KwVTpWC9EPPDgl2pi5jmP2/74PqG6rjIRykc6ncuoEhYcK8+kDABRs6WvN6E9e+3gULvQ4047ljJhAsgaziHvkw2IABl7JGY9X6H8mank4uSl6OikYKXXxhFMQYVaGqgKkg0EjGRhTvs4j3q3OQbW2wc0jAeZDfhUW6y/g29U2zaFrZrNQ0e6Br3ojXyArOhbxWRNcawd896I+w7KEK5V4v6Mr/hAhHGGTzU6+rDNV2AIn2SY7PYv15qclKKczPlcv/N3RcaY6RnM3eN90LjrQU0cmMFN9pQsr7jhTfiZm74g7VnPThq35VyhS8+nqOxkVaDNujHjhl6BY2sSXKbWXWkOfqj+M03sJUM/qwWGIJReFORTOtpya8JdFQqOlAvHoDVTchLTsEIF7scZkEUH7nvdUHzvH3G7vLm2nq1bHhMCb4/savue6wSY5/bA+8WQ537iBgQZTVIU6V72CR8AhTjwi3xNemnXxo/TXf/T+g7FyCOzyEjXvkoIkmV7tEPDboWxOu5G8obhtjSLZp3dB351C5Igvog/NdYsraaW7Vgfhg+FaijynsWbYYUiFjQ2UVDoT+pXmki/dkcwnEGBMAzfrd45X6Guplldg5QNR3x6krRdE/Bga4SC+GT+W+Dnf0iRiIAMGWx6Iz1EiRGF5aFsa94OgYj26hhlE90mXGrQhzGlr85zRj7G0VeH5mNyKwwIa9F05Rexcpo1e1lIyZQ3y5owjvcgJUUaor2PdlNhDYxCa7n7ynT3+W3unRE5eR9S60KGaNwh60LEKr5wvNuXxkkQsZ1MnKGNjUTAM8vr0Dqnc8eWVy8MRhUs42pvpWG4mPAk+cM9ljVjirk1EvTftqVIQLJUCuIMpr2HQ9mBVbZ2PQL6fvypspyil7PT9rUoqj9jpPiWjEI2nADYx3aGfJHZ048veDoLjvic58KMBr0DkQdEDgReiAWCjRWFurHFF/No3Qr/QskMgBvVec3l95U4j/gD+Pa9DDNgTf930KL3/DogQ5mK6TaPSdgD6Qo9kEPWYJYB4qP95UfDIdxZbSnA5D/AHpMJuJjxZyEllfAIemvMvi7QlixvC4ioZpTIPyHs0yhXNeC9dp8tK5rbk+HzDr8Y2Kmlpgqkn3FPQSnkDKfqFDG1DdbLpK2jvpu/t7XtoCZER5QYxzO28Va3SqvuSGC68hbVLc7LvJMkGrKQmW84UxFMaRbDsnIjYroR/H7Jmedl17cUx5ARgCNsJaSZg0w2KO7DtIp4ifTCu/PdDNZQNK+vC6dcHZtjdavcELKzrpO8g3UUr95++R2iB2X+Y8iFiFbIXaEWtZWr7YrAClokiS4GPIOCQVWXGYK8Lf5mpfEVlQS555zxyOubatx4+aUCDhQNMfx0xUiI92YLfY4TJReKWbBx+q/p3bOmUOeG9xfeig0/S5RoKpdow6nV/vETLcRJXUKC08dqONeed9y/BAUq10SwUhKIImrXWXUecifPbssINu8zANqR0J5/2jzxVjEnguXqIsUZIss6F3kKe74s3Hi0Vh6ipAX8SVeZi1xToZ7HNMSjVHkHzoCJvsdHIU4zwUdOihCttZ7n5ec592ySItczi3+gmDZB0sp3bkXNlmMWpzXbPWNvSs6LSsm9Ro1Rzg/fj2OUY/Gj8E2juyEvEaSer3f7/wAhZ2eBEy9Hkx3smoCtI6l1pIrKn/O7tjlwLOCJ98zwJP6Oroo6dHnmT+5XfNo+gvYI6MlmIeVaikzAvqXzeq+uOBtfmbPXQKXkRUq0Sjfvh+TgUf10/Uam06spuZ1sYOoMsD+VU6VVa4ZKJIVYj0tmkcQJJ2uXR22f0wx/lkJe5w0jSVhAlPuZ9EITZqyrgSXwX7f5Zui2evZR2buP4uwY21UmWBpXHKiAD+aE7+9IwZU2XQtvT6zuSkQjnnylTZgAOgwrJObCEEO2aeSnMfuj4rjYmSIDWlwder+j/Jpxk9R/gGcUBJOxZRuI4Nb3d+3nL+QfXk1Ea9TeQUdlEw0UWOh0ROkMgecCOj/dSLZi2kIa3kBG2BNVm/5eWCyYu3MxI0u8t5PYs6HivV+6PBMPWwtgjWStcX+JaDOFmULyXeeoJEAQA7lJzNDb8who60jD6srgVRxso9bLxKpN57gN6aPpMkM1Pyw78rbUD4DXdKl90CRYSDfRO2BFJ6geL1xgFPXkEW38sUZRVR+OVx+SA6RhRsBIwOgXpjhUiktnjtnWpYB1Ahj4VYVnYZTUd6zyXxljQjLHQG0A/KFna6wXBhcuhfxDbHGIrAf7gbnF1lLDRkfO9NYqf4Pv7nv5xT+MWKTnyiblq2U4JuMVtyI5GdmO7kKHrW3j3ghuboBC2HLYbYMo09wdtOjpJxmVqrYMDAZFjTA/CpCJaMtvYV/kMO7M4cvsu3F2rM7l0qRbID5FVjcZzkN/bPpCMmEclJ+rObVXsI/o0W4+58kpggyVpwc+nk/Vm/RIZcz/1adw3qG/BwrAdXDxFfoO62Gc7QFvtIZ6HctyUOsyGQZKNjoLORGlEHyW5eDdn3+loRMlRcfMJg/YRAOzYlCTU1l4V4cx1Vyhgg13OHxp+e3ahD8IqyV2uV0oV0oD+/ZhVEOCrsNLXiLJpeCpHJINkQ/ZtjbKI/igx4rtcmf6O0rL1hocXnpwHxfMc6BArtVewlIpAOGvprexLBJyQBexGAiAKQ7WuoHCtYJ14F892+LP+qdMLrjyy58i39DbrQkj/RLz8jG9uqZmPThsOhcJ6VuEFbRu4bKFZW/Lr1M2zlCpy7gsr/mSpV36JMnjNjwd7oBj10qALqKmVw5QqOWvsMDBk8X+8sZaMIp+TqHw8Ks+pC8YmLHxdRUNDwhl2Y3cX3Rvlljvr0GoY9/gZ/ghgi0YlOh+pSijPfTGZd+I5TuSwNdgVnKj6rSnufhIURytK2Ie7ZHDkxveDdhRxTHemUO+4bvrz+3cFWdj8LckiIhPflvrBX0q36L/kt/PvcRxjA8CJgIPWVJOGtgkfLhgW3lTaaxCn2XzEX3Wwo6ZceqYTxLW7poXmggSZCOnCUCdvueQJPwhWcxIGL9kXaLB3dTZPOTGSrd2YKVyBZfbcumRJlkBDmIfDrd3Q9SLRPDmYxdqQ9uv/QjnbM+Jdh8T8F1uR0UXvyaI4cCYfqu2xnMpqGbtzVhzSwEFVqNiWKGhQ2X/1jXpF2sQRKZQhdCx4eRaw6wnbd4qkRxcZu2ti78GsWVyCee8/NdNNau/AiWbTLHfAt5RPViaXdvRuE8ReoR++a5INzapR21OgpgeGyKc9ijaFe0GeAzyQsK2ZkgmqdK9zX8VtI3idR6YvyjjB1Y0PMnIYrI/cMUTG5R14H7DtOyYhnq2u/isV2D24WD0pAaECva3cwIzenEW7Kx1sQ0gYLGqp0fExztgjuJ8ZID/U0wo+oWifAmdPGq8UI6l6avjGSr8tXnXL9sZn3fltB7mrX9CfDuZ4UZvWwDuQ0zEodHYxR/zHjJs85BWo6ZjSHetkakn6ssrKQjU8KW5Vao6b5zzla2kHCZXq1u80QpuRrzkX1CyOt8hiwcrDbuXZi+bW9KBnE1yoGa7LC/3hfcGeOn11G/vFMuCfM25526y1m9FMEiAlkm9//NwfTZavBXDOEwrVFuZvcH0TO99k/cavAXZGDxQmaJtze4h0El5eYrhDbVxD3GHOPw+FB8eszWKJ0ABJvuuS+XF0V/KULeoQ3KyjtmdE6MsNisHOlJl/zNTB53gTOkaeMevfPeBx44vPUpnW7yErBDLwrPQYzW2EbsI8ykzYn5iU8wpZ/xnADf47U9IHqURmMad4sACRLJfoYp1QL12KFnodThQh1Ng9Wqvj3PHw0XDgSQE32S1G+vcJ/L91axqF8FyGVZTe0e4lUxDMOuKyMn5IgwgkJN6hIyv38PGr6AXg8OkuewFzGGXDfh4lkFq0WJCVNjIuWDWVTqKu1ArTfTy1L8h0sscZuJGRGr3FfhsvPfxtIXQwU3RiWf3xmDQitcTjzWlu4eElTBshqIM5jAXV4InIIX1l2SQC0mg+UHBjygRMqNLFrGcpdM8CNmuWFfiRL1nvYX2vsDQSST7shMqlBw/ad7TjxpXx87fr725UIslUQ1WrAq53EtuCxSZvHH0Oyzs8x9iXeavU4P2ro0s1AXjfbWzIIRbNcOiejIhPvu/O+iCTLmXhaoUrVyS4r7Vo0nfK7skizYzdvVog+iWqM3Czj9qV83HNUwwTN1BCS6NgWe1Wup4WfBaY0HCF2YbTghumzamWiBu9dumz0K1HZYmbCkoh4UxRfJrP9D3i4i2Amu+5/dbTx1HWUbgF2fD+f85A/dlin2MRZl+hZDHh" />
</div>


<script src="/ScriptResource.axd?d=qph9tUZ6hGPMRv_P9VwIOe223LfVgEFllbRPRF1GclrFPGjUisPfO5Fl0w9cc4fIPZ3YzayVwId39zB2TSz0ppxHFLU1&amp;t=397b6a7" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=TvpD2YGOOsBHrEswZxEHEteBCJr-PW6mhkwAauBn9GUjoZ_L4qHkIePrBjHiSySgQeFgrSMv30LeZqoea_8GDSqL1RiaMiLSvG0v8g2&amp;t=397b6a7" type="text/javascript"></script>
<script src="DesktopModules/epsEComm/Scripts/common.js" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
</div><script src="/js/dnn.modalpopup.js?cdv=542" type="text/javascript"></script><script src="/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542" type="text/javascript"></script><script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542" type="text/javascript"></script><script src="http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542" type="text/javascript"></script><script src="/js/dnncore.js?cdv=542" type="text/javascript"></script><script src="/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        





<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/bootstrap-hover-dropdown.min.js?cdv=542)-->
<!--CDF(Javascript|/Portals/_default/Skins/ITD2/js/scripts.js?cdv=542)-->













<!--CDF(Javascript|http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js?cdv=542)-->





<!--CDF(Javascript|http://cdn.jsdelivr.net/fancybox/2.1.5/jquery.fancybox.min.js?cdv=542)-->



<!--CDF(Javascript|http://cdn.jsdelivr.net/jshash/2.2/md5-min.js?cdv=542)-->



















<header class="site-header">			
    <div class="site-header-mainrow">
	    <div class="container">
		    <div class="site-header-mainrow-in relative">	
			    <div class="row">
                    <div class="col-sm-6 col-name-logo" style="position:static">
                        <div class="col-name-logo-in">
                            <h1 class="logo margin-top"><a href="/" title="Naar de voorpagina"><img src="/Portals/_default/Skins/ITD2//Images/header-logo.png"></a></h1>
					        <p class="tagline hide">Israel News - Stay Informed, Pray Informed</p>
                        </div>
                    </div>
					<div class="col-sm-6 col-name-tools">
                        <ul class="clearfix">
		                    <li>
		                        <i class="fa fa-user"></i> <a id="dnn_dnnUSER_registerLink" title="Register" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                |
                                <a id="dnn_dnnLOGIN_loginLink" title="Login" class="SkinObject" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

		                    </li>
                            <li>
                                <a href="https://www.facebook.com/IsraelTodayMagazine"><i class="fa fa-facebook"></i> Like us !</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/AboutUs.aspx"><i class="fa fa-envelope"></i> About us</a>
		                    </li>
                            <li>
                                <a href="http://www.israeltoday.co.il/More/Contact.aspx"><i class="fa fa-info"></i> Contact</a>
		                    </li>
		                </ul>
		                <div class="clearfix"></div>
		                <div class="wrap-search relative clearfix">
		                    <input type="text" class="form-control" placeholder="Search" name="q" onkeydown="if(event.keyCode == 13){window.location.href='http://www.google.com/search?q=site%3Aisraeltoday.co.il+' + this.value;}" />
		                    <i class="fa fa-search"></i>
		                </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</header>





                 


<nav class="navbar navbar-inverse navbar-fixed-top-bak site-navigation" role="navigation">
    <div class="navbar-inner">
    <div class="container">
	    <div class="navbar-header hidden-sm">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#site-navigation">
			    <span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			
		</div>
			  
		<div class="collapse navbar-collapse" id="site-navigation">
		    <ul class="nav navbar-nav ">

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il" >Home</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/DigitalEdition.aspx" >Digital Edition</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/subscribe/subscribe.html" >Subscribe</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Archive.aspx" >Archive</a>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/SupportIsrael.aspx" >Support Israel<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/product/tabid/61/productId/5336/Default.aspx" >We Believe in Miracles Project</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/TheChildrenareOurFuture.aspx" >The Children are Our Future</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/SolidarityforSoldiers.aspx" >Solidarity for Soldiers</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/PlanttheHolyLand.aspx" >Plant the Holy Land</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/BundleofLove.aspx" >Bundle of Love</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/SupportIsrael/MediaSupport.aspx" >Media Support</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true" target="" href="http://www.israeltoday.co.il/Shop.aspx" >Shop<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Superfood.aspx" >Superfood</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/DeadSeaCosmetics.aspx" >Dead Sea Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/48/Default.aspx" >Media</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/28/Default.aspx" >Cosmetics</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/29/Default.aspx" >Jewelry</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/31/Default.aspx" >Apparel</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/33/Default.aspx" >Judaica</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/36/Default.aspx" >Food</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/37/Default.aspx" >Solidarity</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/ShippingReturns.aspx" >Shipping &amp; Returns</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/Shop/Category/tabid/266/cid/78/Default.aspx" >Winter Specials</a>
	
	</li>

	    </ul>
	
	</li>

	<li class="dropdown ">
	
		<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="150" data-close-others="true">More...<b class="caret hidden-desktop pull-right icon-chevron-down icon-white"></b></a>
	
	    <ul class="dropdown-menu">
	    
	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Contact.aspx" >Contact</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/FAQ.aspx" >FAQ</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/AboutUs.aspx" >About Us</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/DailyEmailSignUp.aspx" >Daily Email Sign-Up</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Services.aspx" >Services</a>
	
	</li>

	<li class="">
	
		<a  target="" href="http://www.israeltoday.co.il/More/Connect.aspx" >Connect</a>
	
	</li>

	    </ul>
	
	</li>

</ul>

		    
		    <div class="wrap-card">
		        

<a id="dnn_epsMiniCart_lnkCart" href="http://www.israeltoday.co.il/Shop/Cart.aspx">
   <span id="spanEpsilonCartSummary" style="vertical-align: bottom; text-align:right; padding-right:5px; padding-left:5px">
      
   </span>
   <img id="dnn_epsMiniCart_imgCart" alt="" src="/DesktopModules/epsEComm/Images/cart2.png" /></a>


		    </div>
		    
        </div>  
    </div>
    </div>
</nav>




<div id="dnn_aboveContentFullWidth" class="above-content-full-width DNNEmptyPane" role="banner"></div>




<div class="container" role="banner">
    <div id="dnn_aboveContentCentered" class="above-content-full-centered DNNEmptyPane"></div>
</div>




<div id="content" class="margin-bottom">
        


<div class="container">
    <div class="primary" role="main">
        <div id="dnn_ContentPane" class=""><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">


<section class="DNNContainer DNNContainer_Default">

    <header>
        <h2><span id="dnn_ctr_dnnTITLE_titleLabel" class="Head">Privacy Statement</span>


</h2>
    </header>
    
    <div class="DNNContainer_Content">
        <div id="dnn_ctr_ContentPane"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Israel Today | Israel News is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Israel Today | Israel News site and governs data collection and usage.
        By using the Israel Today | Israel News site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Israel Today | Israel News collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Israel Today | Israel News also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Israel Today | Israel News. This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Israel Today | Israel News for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Israel Today | Israel News site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Israel Today | Israel News public message boards,
        this information may be collected and used by others. Note: Israel Today | Israel News
        does not read any of your private online communications.</p>
        <p>Israel Today | Israel News encourages you to review the privacy statements of Web sites
        you choose to link to from Israel Today | Israel News so that you can understand how those
        Web sites collect, use and share your information. Israel Today | Israel News is not responsible
        for the privacy statements or other content on Web sites outside of the Israel Today | Israel News
        and Israel Today | Israel News family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Israel Today | Israel News collects and uses your personal information to operate the Israel Today | Israel News
        Web site and deliver the services you have requested. Israel Today | Israel News also uses
        your personally identifiable information to inform you of other products or services
        available from Israel Today | Israel News and its affiliates. Israel Today | Israel News may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Israel Today | Israel News does not sell, rent or lease its customer lists to third parties.
        Israel Today | Israel News may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Israel Today | Israel News
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Israel Today | Israel News, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Israel Today | Israel News does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Israel Today | Israel News keeps track of the Web sites and pages our customers visit within
        Israel Today | Israel News, in order to determine what Israel Today | Israel News services are
        the most popular. This data is used to deliver customized content and advertising
        within Israel Today | Israel News to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Israel Today | Israel News Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Israel Today | Israel News or the site; (b) protect and defend the rights or
        property of Israel Today | Israel News; and, (c) act under exigent circumstances to protect
        the personal safety of users of Israel Today | Israel News, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Israel Today | Israel News Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Israel Today | Israel News pages, or
        register with Israel Today | Israel News site or services, a cookie helps Israel Today | Israel News
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Israel Today | Israel News Web site, the information
        you previously provided can be retrieved, so you can easily use the Israel Today | Israel News
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Israel Today | Israel News services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Israel Today | Israel News secures your personal information from unauthorized access,
        use or disclosure. Israel Today | Israel News secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Israel Today | Israel News will occasionally update this Statement of Privacy to reflect
company and customer feedback. Israel Today | Israel News encourages you to periodically 
        review this Statement to be informed of how Israel Today | Israel News is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Israel Today | Israel News welcomes your comments regarding this Statement of Privacy.
        If you believe that Israel Today | Israel News has not adhered to this Statement, please
        contact Israel Today | Israel News at <a href="mailto:donotreply1@israeltoday.co.il">donotreply1@israeltoday.co.il</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
    </div>
	
</section>
</div></div>
    </div>
</div>

<div class="container">

    <div class="row px-vert px-vert-9">
        <div class="primary col-sm-9">
            <div id="dnn_left9Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-3" role="main">
            <div id="dnn_right3Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
   <div class="row px-vert px-vert-6">
      <div class="primary col-sm-6">
         <div id="dnn_left6Pane" class=" DNNEmptyPane"></div>
      </div>
      <div class="primary col-sm-6" role="main">
         <div id="dnn_right6Pane" class=" DNNEmptyPane"></div>
      </div>  
    </div>
    
    <div class="row px-vert px-vert-8">
        <div class="primary col-sm-8">
            <div id="dnn_left8Pane" class=" DNNEmptyPane"></div>
        </div>
        <div class="secondary col-sm-4" role="main">
            <div id="dnn_right4Pane" class=" DNNEmptyPane"></div>
        </div>  
    </div>
    
    
   <div class="container primary" role="main">
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row1ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       
       <div class="row">
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdLeft" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdMiddle" class=" DNNEmptyPane"></div>
           </div>  
           <div class="col-sm-4">
               <div id="dnn_Row2ThirdRight" class=" DNNEmptyPane"></div>
           </div>  
       </div>
       

   </div>
    

    
</div>





</div>



<!-- Begin footer -->
<footer class="site-footer" id="footer">

    <div class="container">      
  
                  
                    
                <div class="row">
                    <div id="dnn_PreFooterPane0" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane25" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane50" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                    <div id="dnn_PreFooterPane70" class="col-sm-3 wsc_pane DNNEmptyPane">
                    </div>
                </div>
                

                
                
                <div class="row">
                    <div class="col-xs-12" style="padding-bottom:15px">
                    
                        <hr style="margin-top:0" />
                        
                                                 
                        <div class="row">
                            <div class="col-sm-6">
                                 <span id="dnn_footer_links_lblLinks"><a class="SkinObject" href="http://www.israeltoday.co.il">Home</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/DigitalEdition.aspx">Digital Edition</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/subscribe/subscribe.html">Subscribe</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Archive.aspx">Archive</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/SupportIsrael.aspx">Support Israel</a><span class="SkinObject"> | </span><a class="SkinObject" href="http://www.israeltoday.co.il/Shop.aspx">Shop</a></span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_dnnUSER2_registerLink" title="Register" class="user btn btn-danger btn-sm" rel="nofollow" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=http%3a%2f%2fwww.israeltoday.co.il%2fHome2.aspx">Register</a>

                                
                                <a id="dnn_dnnLOGIN2_loginLink" title="Login" class="user btn btn-danger btn-sm" rel="nofollow" onclick="this.disabled=true;" href="http://www.israeltoday.co.il/LoginRegister/tabid/71/Default.aspx?returnurl=%2fprivacy.aspx">Login</a>

                                
                                <a href="#top" class="btn btn-default btn-sm">Go to top <i class="fa fa-arrow-up"></i></a>

                            </div>
                        </div>
                        
                        
                        <hr />
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <span id="dnn_FooterCopyright_lblCopyright" class="SkinObject">Copyright 2018 by Israel Today</span>

                            </div>
                            <div class="col-sm-6 text-right">
                                <a id="dnn_FooterPrivacy_hypPrivacy" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/privacy.aspx">Privacy Statement</a> <span style="color:#CCC">&nbsp; &nbsp;|&nbsp; </span> 
                                <a id="dnn_FooterTerms_hypTerms" class="SkinObject" rel="nofollow" href="http://www.israeltoday.co.il/terms.aspx">Terms Of Use</a>
                            </div>
                        </div>
                       
                           
                        
                         
                         
                                    
                    </div>
                
                </div>
                
                

    </div>
    
</footer>



        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" />
        
    </form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=542)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=542)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=542)--><!--CDF(Css|/Portals/_default/Skins/ITD2/skin.css?cdv=542)--><!--CDF(Javascript|/Portals/_default/Skins/ITD2/bootstrapNav/simple.js?cdv=542)--><!--CDF(Javascript|/Resources/Shared/Scripts/jquery/jquery.hoverIntent.min.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=542)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=542)-->
    
</body>
</html>