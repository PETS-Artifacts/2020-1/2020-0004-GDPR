<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" prefix="og: http://ogp.me/ns fb: http://ogp.me/ns/fb#">

<head>
<meta name="viewport" content="width=device-width,minimum-scale=1">
<meta charset="utf-8">

<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700" rel="stylesheet">

<title>Privacy Policy | Pajiba</title>


<!-- Desktop Stylesheet-->
<link rel="stylesheet" type="text/css" href="/styles-site.css" media="screen and (min-device-width: 769px)" />
<!-- Mobile Stylesheet-->
<link rel="stylesheet" type="text/css" href="/styles-mobile.css" media="only screen and (max-device-width: 768px)" />

<link rel="alternate" type="application/atom+xml" title="Recent Entries" href="http://www.pajiba.com/atom.xml" />



<meta name="description" content="Pajiba: Entertainment. Politics. Culture. Nasty Feminist Hugbox"/>
<meta name="keywords" content="Movies, Awards, Films, New Movie, TV Guide, Actors, Movie Trailer, DVD Movies, TV Shows, Box Office, Directors, TV Listings, Producers, Actresses, Movie Reviews, TV News, Entertainment Industry News, Oscars, Hollywood Studios, TV Reviews, Entertainment News, New Movie Releases, Emmys, Movie News, Hollywood Stars, Movie Previews, Golden Globes, Movie Studios, Festival, Film News, Celeb Interviews, Dustin Rowles" /> 

<meta name="robots" content="noodp,noydir" />
<meta name="google-site-verification" content="Ze98nFoNbtECCWGXYeEK5k0eLWXhnulmcix3Ir1Fsl8" />



<meta property="fb:pages" content="250548979637" />
<meta property="fb:admins" content="504038032">
<meta property="fb:app_id" content="1733886336897121" /> 


     


     


     
 
<meta property="og:locale" content="en_US" />
<meta property="og:site_name" content="Pajiba" />

  <meta property="og:url" content="http://www.pajiba.com/" />
  <meta property="og:type" content="website"/>
  <meta property="og:title" content="Pajiba" />
  <meta property="og:image" content="http://www.pajiba.com/assets_c/2018/01/GettyImages-632421398-thumb-700xauto-191324.jpg" />
  <meta property="og:description" content="Sweetened by Mock, Lightened by Droll" />



     


     
 
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@pajiba">

  <meta name="twitter:title" content="Pajiba">
  <meta name="twitter:description" content="Sweetened by Mock, Lightened by Droll">
  <meta name="twitter:creator" content="@pajiba">
  <meta name="twitter:image" content="http://www.pajiba.com/image/2018/hebdo.jpg



" />



     
 

<!--Favicons Images and Code Courtesy of http://realfavicongenerator.net/ -->

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff"> 

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "20407962" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=20407962&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->
 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-26480279-1', 'auto');

  ga('send', 'pageview');

</script>


 

<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=343740; 
var sc_invisible=1; 
var sc_security=""; 
var sc_https=1; 
var sc_remove_link=1; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><img class="statcounter"
src="//c.statcounter.com/343740/0//1/" alt="Web
Analytics"></div></noscript>
<!-- End of StatCounter Code for Default Guide -->

<link rel="start" href="http://www.pajiba.com/" title="Home" />


<meta name='ir-site-verification-token' value='1828630119' />

<!-- PLACE THIS SECTION INSIDE OF YOUR HEAD TAGS -->
<script data-cfasync="false" type="text/javascript">
  var freestar = freestar || {};
  freestar.hitTime = Date.now();
  freestar.queue = freestar.queue || [];
  freestar.config = freestar.config || {};
  freestar.debug = window.location.search.indexOf('fsdebug') === -1 ? false : true;

  // Tag IDs set here, must match Tags served in the Body for proper setup
  freestar.config.enabled_slots = [
    "pajiba_970x90_728x90_320x50_300x250_top",
    "pajiba_728x90_300x250_320x100_320x50_content_top",
    "pajiba_300x600_300x250_Right_1",
    "pajiba_300x600_300x250_Right_2",
    "pajiba_160x600_left",
    "pajiba_728x90_300x250_320x50_320x100_articlelist_1",
    "pajiba_728x90_300x250_320x50_320x100_articlelist_2"
  ];

  !function(a,b){var c=b.getElementsByTagName("script")[0],d=b.createElement("script"),e="https://a.pub.network/Pajiba-com";e+=freestar.debug?"/qa/pubfig.min.js":"/pubfig.min.js",d.async=!0,d.src=e,c.parentNode.insertBefore(d,c)}(window,document);
</script>
</head>

<body> 
<div id="container">
<script type="text/javascript" src="http://www.pajiba.com/mt.js"></script>
<div id="ad-mobile"><center><a href="http://www.pajiba.com"><img src="http://www.pajiba.com/image/2018/hebdo.jpg" class="mt-image-none"/></a></center>

<div id="logo-desktop"><center>

<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/netflix_movies_and_tv/">streaming</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> /
<a href="http://www.pajiba.com/web_culture/">web</a> / <a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a>/ <a href="http://www.pajiba.com/trade_news/">industry</a> / 
<a href="http://www.pajiba.com/videos/">video</a> / 
<a href="http://www.pajiba.com/pajiba_love/">love</a> /   
<a href="http://www.pajiba.com/seriously_random_lists/">lists</a> / 
<a href="http://www.pajiba.com/think_pieces/">think pieces</a> / 
<a href="http://www.pajiba.com/miscellaneous/">misc</a> / 
<a href="http://www.pajiba.com/staff/">staff</a> / 
<a href="http://www.cannonballread.com/">cbr</a> 

<HR WIDTH="100%" size="1" color="#E1E1E1">
</center></div>

<div id="ad-mobile"><center>
<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> /
<a href="http://www.pajiba.com/web_culture/">web</a> / <a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a> 
</center></div>







</div>
<div id="ad-desktop"><center><a href="http://www.pajiba.com"><img src="http://www.pajiba.com/image/2018/hebdo.jpg" class="mt-image-none"/></a></center>

<div id="logo-desktop"><center>

<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/netflix_movies_and_tv/">streaming</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> /
<a href="http://www.pajiba.com/web_culture/">web</a> / <a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a>/ <a href="http://www.pajiba.com/trade_news/">industry</a> / 
<a href="http://www.pajiba.com/videos/">video</a> / 
<a href="http://www.pajiba.com/pajiba_love/">love</a> /   
<a href="http://www.pajiba.com/seriously_random_lists/">lists</a> / 
<a href="http://www.pajiba.com/think_pieces/">think pieces</a> / 
<a href="http://www.pajiba.com/miscellaneous/">misc</a> / 
<a href="http://www.pajiba.com/staff/">staff</a> / 
<a href="http://www.cannonballread.com/">cbr</a> 

<HR WIDTH="100%" size="1" color="#E1E1E1">
</center></div>

<div id="ad-mobile"><center>
<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> /
<a href="http://www.pajiba.com/web_culture/">web</a> / <a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a> 
</center></div>







</div>
<img id="mtdot" width="0" height="0" src="http://www.pajiba.com/assets_c/2010/10/mtdot.jpg"/>


<br>

<div id="pagebody">
<div id="pagebody-inner" class="pkg">

<div id="center">
<div class="content">
     <br></br>
     <p><h2><strong><center>PRIVACY POLICY</center></strong></h2></p>

<p>Pajiba.com collects personal information when you submit a comment to us &#8212; specifically, your name and e-mail address. In addition, Pajiba.com may automatically receive and record information on our server and data logs from your browsers, including your IP address and the page requested.</p>

<p>Pajiba.com uses any such collected information for the purpose of better serving you. Pajiba.com does not rent, sell or otherwise share personal information about you with other people or nonaffiliated companies unless we are responding to subpoenas, court orders, or legal processes, or to establish or exercise our legal rights or defend against legal claims.</p>

<p>Pajiba.com uses third-party analytics for internal uses, including Google Analytics. Please review <a href="https://www.google.com/policies/privacy/partners/">Google&#8217;s Privacy Policy</a> for more information, including how to opt out of Google&#8217;s analytics tracking.</p>

<p>In addition, some of the companies that advertise on Pajiba.com may use cookies on our site. However, Pajiba.com has no access to or control over these cookies once we have given permission for them to set cookies for advertising. For more on that, we turn to the NAI disclaimer:</p>

<blockquote>We use third-party advertising companies to serve ads when you visit our Web site. These companies may use aggregated information (not including your name, address, email address or telephone number) about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, <a href="http://www.networkadvertising.org/managing/opt_out.asp">click here</a>.</blockquote>

<div class="link-note"><br />
<!--Creative Commons License-->
<center><a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/2.5/"><img alt="Creative Commons License" border="0" src="http://creativecommons.org/images/public/somerights20.png"/></a></center><br/>
<!--/Creative Commons License--><!-- <rdf:RDF xmlns="http://web.resource.org/cc/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<Work rdf:about="">
<license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/2.5/" />
</Work>
<License rdf:about="http://creativecommons.org/licenses/by-nc-nd/2.5/"><permits rdf:resource="http://web.resource.org/cc/Reproduction"/><permits rdf:resource="http://web.resource.org/cc/Distribution"/><requires rdf:resource="http://web.resource.org/cc/Notice"/><requires rdf:resource="http://web.resource.org/cc/Attribution"/><prohibits rdf:resource="http://web.resource.org/cc/CommercialUse"/></License></rdf:RDF> -->
<br>
</div>
     
     <br></br>

</div>
</div>

<div>

</div>

<br></br>

<br clear="all" /><br clear="all" />

</div>
</div>

<div id="footer">
<!-- Go to www.addthis.com/dashboard to customize your tools --> <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=hixlespilk"></script> 
</div>

<script type="text/javascript">ggv2id='d35fa5db';</script>
<script type="text/javascript" src="https://js.gumgum.com/services.js"></script>

</div>
</div>
</body>
</html>

