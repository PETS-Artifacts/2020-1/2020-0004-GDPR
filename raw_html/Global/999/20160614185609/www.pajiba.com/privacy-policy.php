<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" prefix="og: http://ogp.me/ns fb: http://ogp.me/ns/fb#">

<head>

<meta name="viewport" content="width=device-width,minimum-scale=1">
<meta charset="utf-8"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var o=t[n]={exports:{}};e[n][0].call(o.exports,function(t){var o=e[n][1][t];return r(o||t)},o,o.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(e,t,n){function r(e,t){return function(){o(e,[(new Date).getTime()].concat(a(arguments)),null,t)}}var o=e("handle"),i=e(2),a=e(3);"undefined"==typeof window.newrelic&&(newrelic=NREUM);var u=["setPageViewName","setCustomAttribute","finished","addToTrace","inlineHit"],c=["addPageAction"],f="api-";i(u,function(e,t){newrelic[t]=r(f+t,"api")}),i(c,function(e,t){newrelic[t]=r(f+t)}),t.exports=newrelic,newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),o("err",[e,(new Date).getTime()])}},{}],2:[function(e,t,n){function r(e,t){var n=[],r="",i=0;for(r in e)o.call(e,r)&&(n[i]=t(r,e[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],3:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,o=n-t||0,i=Array(0>o?0:o);++r<o;)i[r]=e[t+r];return i}t.exports=r},{}],ee:[function(e,t,n){function r(){}function o(e){function t(e){return e&&e instanceof r?e:e?u(e,a,i):i()}function n(n,r,o){e&&e(n,r,o);for(var i=t(o),a=l(n),u=a.length,c=0;u>c;c++)a[c].apply(i,r);var s=f[g[n]];return s&&s.push([m,n,r,i]),i}function p(e,t){w[e]=l(e).concat(t)}function l(e){return w[e]||[]}function d(e){return s[e]=s[e]||o(n)}function v(e,t){c(e,function(e,n){t=t||"feature",g[n]=t,t in f||(f[t]=[])})}var w={},g={},m={on:p,emit:n,get:d,listeners:l,context:t,buffer:v};return m}function i(){return new r}var a="nr@context",u=e("gos"),c=e(2),f={},s={},p=t.exports=o();p.backlog=f},{}],gos:[function(e,t,n){function r(e,t,n){if(o.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[t]=r,r}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){o.buffer([e],r),o.emit(e,t,n)}var o=e("ee").get("handle");t.exports=r,r.ee=o},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!w++){var e=v.info=NREUM.info,t=s.getElementsByTagName("script")[0];if(e&&e.licenseKey&&e.applicationID&&t){c(l,function(t,n){e[t]||(e[t]=n)});var n="https"===p.split(":")[0]||e.sslForHttp;v.proto=n?"https://":"http://",u("mark",["onload",a()],null,"api");var r=s.createElement("script");r.src=v.proto+e.agent,t.parentNode.insertBefore(r,t)}}}function o(){"complete"===s.readyState&&i()}function i(){u("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var u=e("handle"),c=e(2),f=window,s=f.document;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:f.XMLHttpRequest,REQ:f.Request,EV:f.Event,PR:f.Promise,MO:f.MutationObserver},e(1);var p=""+location,l={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-943.min.js"},d=window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&!/CriOS/.test(navigator.userAgent),v=t.exports={offset:a(),origin:p,features:{},xhrWrappable:d};s.addEventListener?(s.addEventListener("DOMContentLoaded",i,!1),f.addEventListener("load",r,!1)):(s.attachEvent("onreadystatechange",o),f.attachEvent("onload",r)),u("mark",["firstbyte",a()],null,"api");var w=0},{}]},{},["loader"]);</script>

<script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script>

<title>Privacy Policy | Pajiba</title>

<!-- Desktop Stylesheet-->
<link rel="stylesheet" type="text/css" href="http://www.pajiba.com/styles-site.css" media="screen and (min-device-width: 645px)" />
<!-- Mobile Stylesheet-->
<link rel="stylesheet" type="text/css" href="http://www.pajiba.com/styles-mobile.css" media="only screen and (max-device-width: 640px)" />

<link rel="alternate" type="application/atom+xml" title="Recent Entries" href="http://www.pajiba.com/atom.xml" />

<meta name="description" content="Pajiba: Sweetened by Mock, Lightened by Droll"/>
<meta name="keywords" content="Movies, Awards, Films, New Movie, TV Guide, Actors, Movie Trailer, DVD Movies, TV Shows, Box Office, Directors, TV Listings, Producers, Actresses, Movie Reviews, TV News, Entertainment Industry News, Oscars, Hollywood Studios, TV Reviews, Entertainment News, New Movie Releases, Emmys, Movie News, Hollywood Stars, Movie Previews, Golden Globes, Movie Studios, Festival, Film News, Celeb Interviews, Dustin Rowles" /> 

<meta name="robots" content="noodp,noydir" />
<meta name="google-site-verification" content="Ze98nFoNbtECCWGXYeEK5k0eLWXhnulmcix3Ir1Fsl8" />

<script type="text/javascript" src="http://www.pajiba.com/mt.js"></script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<meta property="fb:pages" content="250548979637" />
<meta property="fb:admins" content="504038032">
<meta property="fb:app_id" content="1733886336897121" /> 


     


     
 
<meta property="og:locale" content="en_US" />
<meta property="og:site_name" content="Pajiba" />

  <meta property="og:url" content="http://www.pajiba.com/" />
  <meta property="og:type" content="website"/>
  <meta property="og:title" content="Pajiba" />
  <meta property="og:image" content="http://www.pajiba.com/assets_c/2016/06/blogdicultura_c181cb84026bf35e987ab931f6ed0d2e-640x347-thumb-600x325-157652.jpg" />
  <meta property="og:description" content="Sweetened by Mock, Lightened by Droll" />



     
 
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@pajiba">

  <meta name="twitter:title" content="Pajiba">
  <meta name="twitter:description" content="Sweetened by Mock, Lightened by Droll">
  <meta name="twitter:creator" content="@pajiba">
  <meta name="twitter:image" content="http://www.pajiba.com/image/Screen%20Shot%202015-09-26%20at%207.06.46%20PM-thumb-330x123-137892.jpg" />

 

<!--Favicons Images and Code Courtesy of http://realfavicongenerator.net/ -->
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png?v=Nmm2YBeXLG">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png?v=Nmm2YBeXLG">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png?v=Nmm2YBeXLG">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png?v=Nmm2YBeXLG">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png?v=Nmm2YBeXLG">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png?v=Nmm2YBeXLG">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png?v=Nmm2YBeXLG">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png?v=Nmm2YBeXLG">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png?v=Nmm2YBeXLG">
<link rel="icon" type="image/png" href="/favicon-32x32.png?v=Nmm2YBeXLG" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-194x194.png?v=Nmm2YBeXLG" sizes="194x194">
<link rel="icon" type="image/png" href="/favicon-96x96.png?v=Nmm2YBeXLG" sizes="96x96">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png?v=Nmm2YBeXLG" sizes="192x192">
<link rel="icon" type="image/png" href="/favicon-16x16.png?v=Nmm2YBeXLG" sizes="16x16">
<link rel="manifest" href="/manifest.json?v=Nmm2YBeXLG">
<link rel="mask-icon" href="/safari-pinned-tab.svg?v=Nmm2YBeXLG" color="#5bbad5">
<link rel="shortcut icon" href="/favicon.ico?v=Nmm2YBeXLG">
<meta name="apple-mobile-web-app-title" content="Pajiba">
<meta name="application-name" content="Pajiba">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/mstile-144x144.png?v=Nmm2YBeXLG">
<meta name="theme-color" content="#ffffff"> 

<!--Playwire Header Snippet-->  
<script src="http://cdn.intergi.com/utils/tyche.js"></script>
<script src="http://aka-cdn-ns.adtechus.com/dt/common/DAC.js"></script>    
<!--End Playwire Header Snippet-->


 

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "20407962" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=20407962&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->
 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-26480279-1', 'auto');

  ga('send', 'pageview');

</script>
 
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=343740; 
var sc_invisible=1; 
var sc_security=""; 
var sc_https=1; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="site stats"
href="http://statcounter.com/free-web-stats/"
target="_blank"><img class="statcounter"
src="//c.statcounter.com/343740/0//1/" alt="site
stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->
 

<script type='text/javascript'>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
  (function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') +
      '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
  })();
</script>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/1088036/Mobile4', [320, 50], 'div-gpt-ad-1464364461488-0').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PRTDH', [300, 250], 'div-gpt-ad-1464364461488-1').addService(googletag.pubads());
    googletag.defineSlot('/1088036/Playwire728BTF', [728, 90], 'div-gpt-ad-1464364461488-2').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PM1', [300, 250], 'div-gpt-ad-1464364461488-3').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PM2', [300, 250], 'div-gpt-ad-1464364461488-4').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PM3', [300, 250], 'div-gpt-ad-1464364461488-5').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PRDR', [[300, 250], [300, 600]], 'div-gpt-ad-1464364461488-6').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PRTA', [300, 250], 'div-gpt-ad-1464364461488-7').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PW728H', [728, 90], 'div-gpt-ad-1464364461488-8').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PW728ROS', [728, 90], 'div-gpt-ad-1464364461488-9').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>


 
 

<link rel="start" href="http://www.pajiba.com/" title="Home" />


</head>
<body>

<!--Get Started Using Playwire.com-->  
<script>
    var pw_page_width = window.innerWidth;
    var pw_device_mobile = (pw_page_width >= 645) ? false : true;
    ADTECH.config.page = { protocol: 'http', server: 'ads.intergi.com', network: '5205.1', pageid: 0, params: { loc: '100' }};
</script>
<!--Thank You for using Playwire-->
 

<div id="container">



<center>
<a href="http://www.pajiba.com"><img src="http://www.pajiba.com/image/Screen%20Shot%202015-09-26%20at%207.06.46%20PM-thumb-330x123-137892.jpg" class="mt-image-none" class="mt-image-none" /></a>
</center>

<div id="ad-desktop"><center><br>
<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/seriously_random_lists/">lists</a> / 
<a href="http://www.pajiba.com/guides/">guides</a> / 
<a href="http://www.pajiba.com/trade_news/">news</a> / 
<a href="http://www.pajiba.com/pajiba_love/">love</a> / 
<a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a> / 
<a href="http://www.pajiba.com/videos/">video</a> / 
<a href="http://www.pajiba.com/think_pieces/">think pieces</a> / 
<a href="http://www.pajiba.com/staff/">staff</a> / 
<a href="http://www.pajiba.com/podcasts/">podcasts</a> / 
<a href="http://www.pajiba.com/web_culture/">web</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> / 
<a href="http://www.pajiba.com/dc_movies/">dc</a> / 
<a href="http://www.pajiba.com/game_of_thrones/">game of thrones</a> / 
<a href="http://www.pajiba.com/preacher/">preacher</a> / 
<a href="http://www.pajiba.com/netflix_movies_and_tv/">netflix</a> / 
<a href="http://www.pajiba.com/marvel_movies/">marvel</a> / 
<a href="http://www.cannonballread.com/">cbr</a>
<HR WIDTH="100%" size="1" color="#E1E1E1">
</center></div> 

     


     


     
<center><div id="ad-mobile"><!-- /1088036/Mobile4 -->
<div id='div-gpt-ad-1464364461488-0' style='height:50px; width:320px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1464364461488-0'); });
</script>
</div></div></center>

<div id="pagebody">
<div id="pagebody-inner" class="pkg">

<div id="center">
<div class="content">
     <br></br>
     <p><h2><strong><center>PRIVACY POLICY</center></strong></h2></p>

<p>Pajiba.com collects personal information when you submit a comment to us &#8212; specifically, your name and e-mail address. In addition, Pajiba.com may automatically receive and record information on our server logs from your browsers, including your IP address and the page requested.</p>

<p>Pajiba.com uses any such collected information for the purpose of better serving you. Pajiba.com does not rent, sell or otherwise share personal information about you with other people or nonaffiliated companies unless we are responding to subpoenas, court orders, or legal processes, or to establish or exercise our legal rights or defend against legal claims.</p>

<p>In addition, some of the companies that advertise on Pajiba.com may use cookies on our site. However, Pajiba.com has no access to or control over these cookies once we have given permission for them to set cookies for advertising. For more on that, we turn to the NAI disclaimer:</p>

<blockquote>We use third-party advertising companies to serve ads when you visit our Web site. These companies may use aggregated information (not including your name, address, email address or telephone number) about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, <a href="http://www.networkadvertising.org/managing/opt_out.asp">click here</a>.</blockquote>

<div class="link-note"><br />
<!--Creative Commons License-->
<center><a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/2.5/"><img alt="Creative Commons License" border="0" src="http://creativecommons.org/images/public/somerights20.png"/></a></center><br/>
<!--/Creative Commons License--><!-- <rdf:RDF xmlns="http://web.resource.org/cc/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<Work rdf:about="">
<license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/2.5/" />
</Work>
<License rdf:about="http://creativecommons.org/licenses/by-nc-nd/2.5/"><permits rdf:resource="http://web.resource.org/cc/Reproduction"/><permits rdf:resource="http://web.resource.org/cc/Distribution"/><requires rdf:resource="http://web.resource.org/cc/Notice"/><requires rdf:resource="http://web.resource.org/cc/Attribution"/><prohibits rdf:resource="http://web.resource.org/cc/CommercialUse"/></License></rdf:RDF> -->
<br>
</div>
     
     <br></br>

</div>
</div>

<div>

</div>

<br></br>

<br clear="all" /><br clear="all" />

</div>
</div>

<div id="footer">

<div id="ad-desktop"><center>
<!-- /1088036/Playwire728BTF -->
<div id='div-gpt-ad-1464364461488-2' style='height:90px; width:728px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1464364461488-2'); });
</script>
</div></center></div>

</div>

<!-- Compete XL Code for pajiba.com -->
<script type="text/javascript">
__compete_code = 'c49041318f71b573e31fa7114c174e16';
/* Set control variables below this line. */ 
</script>
<script type="text/javascript" src="//c.compete.com/bootstrap/s/c49041318f71b573e31fa7114c174e16/pajiba-com/bootstrap.js"></script>
<noscript>
    <img width="1" height="1" src="https://ssl-pajiba-com-c49041.c-col.com"/>
</noscript>


</div>
</div>

     


     


     




<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"5ae0501072","applicationID":"3186585","transactionName":"NgYGMEoAWBVQBkQMXw9MMRZRTkYUWBNRBklMEwsIUQJPSEENQA==","queueTime":0,"applicationTime":0,"atts":"GkEFRgIaSxs=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>

</html>

