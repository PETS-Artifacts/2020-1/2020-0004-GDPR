<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" prefix="og: http://ogp.me/ns fb: http://ogp.me/ns/fb#">

<head>
<meta name="viewport" content="width=device-width,minimum-scale=1">
<meta charset="utf-8">

<script type="text/javascript">var tyche = { mode: 'tyche', config: '//config.playwire.com/1007829/v2/websites/53134/banner.json' };</script><script id="tyche" src="//cdn.intergi.com/hera/tyche.js" type="text/javascript"></script>

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/1088036/GumClick', [1, 1], 'div-gpt-ad-1499543926546-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
  });
</script>

<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,700" rel="stylesheet">

<title>Privacy Policy | Pajiba</title>

<!-- Desktop Stylesheet-->
<link rel="stylesheet" type="text/css" href="http://www.pajiba.com/styles-site.css" media="screen and (min-device-width: 645px)" />
<!-- Mobile Stylesheet-->
<link rel="stylesheet" type="text/css" href="http://www.pajiba.com/styles-mobile.css" media="only screen and (max-device-width: 640px)" />

<link rel="alternate" type="application/atom+xml" title="Recent Entries" href="http://www.pajiba.com/atom.xml" />

<meta name="description" content="Pajiba: Sweetened by Mock, Lightened by Droll"/>
<meta name="keywords" content="Movies, Awards, Films, New Movie, TV Guide, Actors, Movie Trailer, DVD Movies, TV Shows, Box Office, Directors, TV Listings, Producers, Actresses, Movie Reviews, TV News, Entertainment Industry News, Oscars, Hollywood Studios, TV Reviews, Entertainment News, New Movie Releases, Emmys, Movie News, Hollywood Stars, Movie Previews, Golden Globes, Movie Studios, Festival, Film News, Celeb Interviews, Dustin Rowles" /> 

<meta name="robots" content="noodp,noydir" />
<meta name="google-site-verification" content="Ze98nFoNbtECCWGXYeEK5k0eLWXhnulmcix3Ir1Fsl8" />

<script type="text/javascript" src="http://www.pajiba.com/mt.js"></script>
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<meta property="fb:pages" content="250548979637" />
<meta property="fb:admins" content="504038032">
<meta property="fb:app_id" content="1733886336897121" /> 


     


     
 
<meta property="og:locale" content="en_US" />
<meta property="og:site_name" content="Pajiba" />

  <meta property="og:url" content="http://www.pajiba.com/" />
  <meta property="og:type" content="website"/>
  <meta property="og:title" content="Pajiba" />
  <meta property="og:image" content="http://www.pajiba.com/assets_c/2017/07/Rooney-Mara-thumb-700x386-181856.png" />
  <meta property="og:description" content="Sweetened by Mock, Lightened by Droll" />



     
 
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@pajiba">

  <meta name="twitter:title" content="Pajiba">
  <meta name="twitter:description" content="Sweetened by Mock, Lightened by Droll">
  <meta name="twitter:creator" content="@pajiba">
  <meta name="twitter:image" content="http://www.pajiba.com/assets_c/2016/11/logo-hebdo-thumb-300x95-167633.jpg" />

 

<!--Favicons Images and Code Courtesy of http://realfavicongenerator.net/ -->

<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/manifest.json">
<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
<meta name="theme-color" content="#ffffff"> 

<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "20407962" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=20407962&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->
 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-26480279-1', 'auto');

  ga('send', 'pageview');

</script>

 
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=343740; 
var sc_invisible=1; 
var sc_security=""; 
var sc_https=1; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="site stats"
href="http://statcounter.com/free-web-stats/"
target="_blank"><img class="statcounter"
src="//c.statcounter.com/343740/0//1/" alt="site
stats"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->

<script type="text/javascript">
  var _sf_async_config = { uid: 53053, domain: 'pajiba.com', useCanonical: true };
  (function() {
    function loadChartbeat() {
      window._sf_endpt = (new Date()).getTime();
      var e = document.createElement('script');
      e.setAttribute('language', 'javascript');
      e.setAttribute('type', 'text/javascript');
      e.setAttribute('src','//static.chartbeat.com/js/chartbeat.js');
      document.body.appendChild(e);
    };
    var oldonload = window.onload;
    window.onload = (typeof window.onload != 'function') ?
      loadChartbeat : function() { oldonload(); loadChartbeat(); };
  })();
</script>

<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
</script>

<script>
  googletag.cmd.push(function() {
    googletag.defineSlot('/1088036/PM1', [300, 250], 'div-gpt-ad-1472002440297-0').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PM2', [300, 250], 'div-gpt-ad-1472002440297-1').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PM3', [300, 250], 'div-gpt-ad-1472002440297-2').addService(googletag.pubads());
    googletag.defineSlot('/1088036/Playwire728BTF', [[728, 90], [970, 90], [970, 250]], 'div-gpt-ad-1472002440297-3').addService(googletag.pubads());
    googletag.defineSlot('/1088036/PRDR', [300, 250], 'div-gpt-ad-1472002440297-4').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>



 
 

<link rel="start" href="http://www.pajiba.com/" title="Home" />



</head>
<body> 

<div id="container">
<div id="ad-desktop"><center><a href="http://www.pajiba.com"><img src="http://www.pajiba.com/assets_c/2016/11/logo-hebdo-thumb-300x95-167633.jpg" class="mt-image-none"/></a></center>

<div id="logo-desktop"><center>
<a href="http://www.pajiba.com/what_to_watch/">what to watch</a> / 
<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/netflix_movies_and_tv/">streaming</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> /
<a href="http://www.pajiba.com/web_culture/">web</a> / <a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a>/ <a href="http://www.pajiba.com/trade_news/">news</a> / 
<a href="http://www.pajiba.com/videos/">video</a> / 
<a href="http://www.pajiba.com/pajiba_love/">love</a> /   
<a href="http://www.pajiba.com/seriously_random_lists/">lists</a> / 
<a href="http://www.pajiba.com/think_pieces/">think pieces</a> / 
<a href="http://www.pajiba.com/miscellaneous/">misc</a> / 
<a href="http://www.pajiba.com/staff/">staff</a> / 
<a href="http://www.cannonballread.com/">cbr</a> /
<a href="https://twitter.com/pajiba">twitter</a> /
<a href="http://www.facebook.com/pages/Pajiba/250548979637">facebook</a>
<HR WIDTH="100%" size="1" color="#E1E1E1">
</center></div>

<div id="ad-mobile"><center>
<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/what_to_watch/">what to watch</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> /
<a href="http://www.pajiba.com/web_culture/">web</a> / <a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a> 
</center></div>



</div>
<div id="leaderboard">
<center><div data-pw-desk='leaderboard_atf'></div></center>

<div id="ad-mobile"><center><style>
.Top{ width: 300px; height: 250px; }
@media(min-width: 500px) { .Top { width: 468px; height: 60px; } }
@media(min-width: 800px) { .Top { width: 970px; height: 90px; } }
</style>
<script async src="//"></script>
<!-- Top -->
<ins class="adsbygoogle Top"
     style="display:inline-block"
     data-ad-client="ca-pub-3075075806059462"
     data-ad-slot="9959884454"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</center></div> 
</div>
<div id="ad-mobile"><center><a href="http://www.pajiba.com"><img src="http://www.pajiba.com/assets_c/2016/11/logo-hebdo-thumb-300x95-167633.jpg" class="mt-image-none"/></a></center>

<div id="logo-desktop"><center>
<a href="http://www.pajiba.com/what_to_watch/">what to watch</a> / 
<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/netflix_movies_and_tv/">streaming</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> /
<a href="http://www.pajiba.com/web_culture/">web</a> / <a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a>/ <a href="http://www.pajiba.com/trade_news/">news</a> / 
<a href="http://www.pajiba.com/videos/">video</a> / 
<a href="http://www.pajiba.com/pajiba_love/">love</a> /   
<a href="http://www.pajiba.com/seriously_random_lists/">lists</a> / 
<a href="http://www.pajiba.com/think_pieces/">think pieces</a> / 
<a href="http://www.pajiba.com/miscellaneous/">misc</a> / 
<a href="http://www.pajiba.com/staff/">staff</a> / 
<a href="http://www.cannonballread.com/">cbr</a> /
<a href="https://twitter.com/pajiba">twitter</a> /
<a href="http://www.facebook.com/pages/Pajiba/250548979637">facebook</a>
<HR WIDTH="100%" size="1" color="#E1E1E1">
</center></div>

<div id="ad-mobile"><center>
<a href="http://www.pajiba.com/film_reviews/">film</a> / 
<a href="http://www.pajiba.com/tv_reviews/">tv</a> / 
<a href="http://www.pajiba.com/what_to_watch/">what to watch</a> / 
<a href="http://www.pajiba.com/politics/">politics</a> /
<a href="http://www.pajiba.com/web_culture/">web</a> / <a href="http://www.pajiba.com/celebrities_are_better_than_you/">celeb</a> 
</center></div>



</div>




<div id="pagebody">
<div id="pagebody-inner" class="pkg">

<div id="center">
<div class="content">
     <br></br>
     <p><h2><strong><center>PRIVACY POLICY</center></strong></h2></p>

<p>Pajiba.com collects personal information when you submit a comment to us &#8212; specifically, your name and e-mail address. In addition, Pajiba.com may automatically receive and record information on our server logs from your browsers, including your IP address and the page requested.</p>

<p>Pajiba.com uses any such collected information for the purpose of better serving you. Pajiba.com does not rent, sell or otherwise share personal information about you with other people or nonaffiliated companies unless we are responding to subpoenas, court orders, or legal processes, or to establish or exercise our legal rights or defend against legal claims.</p>

<p>In addition, some of the companies that advertise on Pajiba.com may use cookies on our site. However, Pajiba.com has no access to or control over these cookies once we have given permission for them to set cookies for advertising. For more on that, we turn to the NAI disclaimer:</p>

<blockquote>We use third-party advertising companies to serve ads when you visit our Web site. These companies may use aggregated information (not including your name, address, email address or telephone number) about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, <a href="http://www.networkadvertising.org/managing/opt_out.asp">click here</a>.</blockquote>

<div class="link-note"><br />
<!--Creative Commons License-->
<center><a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/2.5/"><img alt="Creative Commons License" border="0" src="http://creativecommons.org/images/public/somerights20.png"/></a></center><br/>
<!--/Creative Commons License--><!-- <rdf:RDF xmlns="http://web.resource.org/cc/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<Work rdf:about="">
<license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/2.5/" />
</Work>
<License rdf:about="http://creativecommons.org/licenses/by-nc-nd/2.5/"><permits rdf:resource="http://web.resource.org/cc/Reproduction"/><permits rdf:resource="http://web.resource.org/cc/Distribution"/><requires rdf:resource="http://web.resource.org/cc/Notice"/><requires rdf:resource="http://web.resource.org/cc/Attribution"/><prohibits rdf:resource="http://web.resource.org/cc/CommercialUse"/></License></rdf:RDF> -->
<br>
</div>
     
     <br></br>

</div>
</div>

<div>

</div>

<br></br>

<br clear="all" /><br clear="all" />

</div>
</div>

<br></br>
<br></br>
<div id="footer">
<center><div data-pw-desk='leaderboard_btf'></div></center>
</div>

</div>
</div>

<!-- /1088036/GumClick -->
<div id='div-gpt-ad-1499543926546-0' style='height:1px; width:1px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1499543926546-0'); });
</script>
</div>

<script type="text/javascript"  charset="utf-8">
// Place this code snippet near the footer of your page before the close of the /body tag
// 
                            
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}(';q N=\'\',27=\'1T\';1A(q i=0;i<12;i++)N+=27.V(D.I(D.K()*27.F));q 2y=8,2t=4d,2u=4e,2w=4,2Z=B(t){q o=!1,i=B(){z(k.1h){k.2Y(\'2G\',e);E.2Y(\'1S\',e)}O{k.2R(\'2N\',e);E.2R(\'1Y\',e)}},e=B(){z(!o&&(k.1h||4f.2D===\'1S\'||k.2I===\'36\')){o=!0;i();t()}};z(k.2I===\'36\'){t()}O z(k.1h){k.1h(\'2G\',e);E.1h(\'1S\',e)}O{k.2H(\'2N\',e);E.2H(\'1Y\',e);q n=!1;2J{n=E.4h==4i&&k.21}2L(r){};z(n&&n.2K){(B a(){z(o)H;2J{n.2K(\'16\')}2L(e){H 4c(a,50)};o=!0;i();t()})()}}};E[\'\'+N+\'\']=(B(){q t={t$:\'1T+/=\',4j:B(e){q a=\'\',d,n,o,c,s,l,i,r=0;e=t.e$(e);1c(r<e.F){d=e.13(r++);n=e.13(r++);o=e.13(r++);c=d>>2;s=(d&3)<<4|n>>4;l=(n&15)<<2|o>>6;i=o&63;z(2U(n)){l=i=64}O z(2U(o)){i=64};a=a+T.t$.V(c)+T.t$.V(s)+T.t$.V(l)+T.t$.V(i)};H a},X:B(e){q n=\'\',d,l,c,s,r,i,a,o=0;e=e.1z(/[^A-4l-4m-9\\+\\/\\=]/g,\'\');1c(o<e.F){s=T.t$.1F(e.V(o++));r=T.t$.1F(e.V(o++));i=T.t$.1F(e.V(o++));a=T.t$.1F(e.V(o++));d=s<<2|r>>4;l=(r&15)<<4|i>>2;c=(i&3)<<6|a;n=n+R.S(d);z(i!=64){n=n+R.S(l)};z(a!=64){n=n+R.S(c)}};n=t.n$(n);H n},e$:B(t){t=t.1z(/;/g,\';\');q n=\'\';1A(q o=0;o<t.F;o++){q e=t.13(o);z(e<1C){n+=R.S(e)}O z(e>4n&&e<4o){n+=R.S(e>>6|4p);n+=R.S(e&63|1C)}O{n+=R.S(e>>12|2E);n+=R.S(e>>6&63|1C);n+=R.S(e&63|1C)}};H n},n$:B(t){q o=\'\',e=0,n=4q=1B=0;1c(e<t.F){n=t.13(e);z(n<1C){o+=R.S(n);e++}O z(n>4k&&n<2E){1B=t.13(e+1);o+=R.S((n&31)<<6|1B&63);e+=2}O{1B=t.13(e+1);2d=t.13(e+2);o+=R.S((n&15)<<12|(1B&63)<<6|2d&63);e+=3}};H o}};q a=[\'4a==\',\'3V\',\'49=\',\'48\',\'47\',\'46=\',\'45=\',\'44=\',\'43\',\'42\',\'41=\',\'40=\',\'3Z\',\'3Y\',\'3X=\',\'3W\',\'4r=\',\'4b=\',\'4s=\',\'4K=\',\'4M=\',\'4N=\',\'4O==\',\'4P==\',\'4Q==\',\'4R==\',\'4L=\',\'4S\',\'4U\',\'4V\',\'4W\',\'4X\',\'4Y\',\'4Z==\',\'4T=\',\'4J=\',\'4u=\',\'3T==\',\'4H=\',\'4G\',\'4F=\',\'4E=\',\'4D==\',\'4C=\',\'4B==\',\'4A==\',\'4z=\',\'4y=\',\'4x\',\'4w==\',\'4v==\',\'4t\',\'3U==\',\'3C=\'],y=D.I(D.K()*a.F),Y=t.X(a[y]),W=Y,Z=1,w=\'#3S\',r=\'#3m\',v=\'#3l\',g=\'#3k\',C=\'\',b=\'3j 3p 3g!\',p=\'3f 3e T, 3b 39 37 2n\\\'2i 38 3a. 2l 3c 3h 3r. \',f=\'3F 3R 3Q 3P 3O 3N 1A 3q 3M 3L 3K. \',s=\'3H 2n 3G 3E 3s 3D? 2l\\\'2i 3z 3y 3x 3w. \',o=0,u=1,n=\'3v.3t\',l=0,Q=e()+\'.2k\';B h(t){z(t)t=t.1P(t.F-15);q o=k.2B(\'3d\');1A(q n=o.F;n--;){q e=R(o[n].1G);z(e)e=e.1P(e.F-15);z(e===t)H!0};H!1};B m(t){z(t)t=t.1P(t.F-15);q e=k.3u;x=0;1c(x<e.F){1l=e[x].1E;z(1l)1l=1l.1P(1l.F-15);z(1l===t)H!0;x++};H!1};B e(t){q n=\'\',o=\'1T\';t=t||30;1A(q e=0;e<t;e++)n+=o.V(D.I(D.K()*o.F));H n};B i(o){q i=[\'3B\',\'3I==\',\'3J\',\'3i\',\'2T\',\'3n==\',\'3o=\',\'51==\',\'4I=\',\'53==\',\'59==\',\'6m==\',\'6l\',\'6k\',\'6j\',\'2T\'],r=[\'35=\',\'6i==\',\'6h==\',\'6g==\',\'6f=\',\'6c\',\'6a=\',\'5V=\',\'35=\',\'52\',\'68==\',\'67\',\'66==\',\'62==\',\'61==\',\'5Z=\'];x=0;1L=[];1c(x<o){c=i[D.I(D.K()*i.F)];d=r[D.I(D.K()*r.F)];c=t.X(c);d=t.X(d);q a=D.I(D.K()*2)+1;z(a==1){n=\'//\'+c+\'/\'+d}O{n=\'//\'+c+\'/\'+e(D.I(D.K()*20)+4)+\'.2k\'};1L[x]=1V 1W();1L[x].1X=B(){q t=1;1c(t<7){t++}};1L[x].1G=n;x++}};B L(t){};H{33:B(t,r){z(5W k.J==\'6o\'){H};q o=\'0.1\',r=W,e=k.1a(\'1q\');e.1j=r;e.j.1n=\'1M\';e.j.16=\'-1f\';e.j.U=\'-1f\';e.j.1p=\'2a\';e.j.11=\'6b\';q d=k.J.2o,a=D.I(d.F/2);z(a>15){q n=k.1a(\'29\');n.j.1n=\'1M\';n.j.1p=\'1t\';n.j.11=\'1t\';n.j.U=\'-1f\';n.j.16=\'-1f\';k.J.6t(n,k.J.2o[a]);n.1d(e);q i=k.1a(\'1q\');i.1j=\'2p\';i.j.1n=\'1M\';i.j.16=\'-1f\';i.j.U=\'-1f\';k.J.1d(i)}O{e.1j=\'2p\';k.J.1d(e)};l=6E(B(){z(e){t((e.23==0),o);t((e.1Z==0),o);t((e.1J==\'2O\'),o);t((e.1Q==\'2e\'),o);t((e.1O==0),o)}O{t(!0,o)}},26)},1D:B(e,c){z((e)&&(o==0)){o=1;E[\'\'+N+\'\'].1v();E[\'\'+N+\'\'].1D=B(){H}}O{q f=t.X(\'6D\'),u=k.6C(f);z((u)&&(o==0)){z((2t%3)==0){q l=\'6A=\';l=t.X(l);z(h(l)){z(u.1K.1z(/\\s/g,\'\').F==0){o=1;E[\'\'+N+\'\'].1v()}}}};q y=!1;z(o==0){z((2u%3)==0){z(!E[\'\'+N+\'\'].2x){q d=[\'6y==\',\'6q==\',\'6x=\',\'6v=\',\'6u=\'],m=d.F,r=d[D.I(D.K()*m)],a=r;1c(r==a){a=d[D.I(D.K()*m)]};r=t.X(r);a=t.X(a);i(D.I(D.K()*2)+1);q n=1V 1W(),s=1V 1W();n.1X=B(){i(D.I(D.K()*2)+1);s.1G=a;i(D.I(D.K()*2)+1)};s.1X=B(){o=1;i(D.I(D.K()*3)+1);E[\'\'+N+\'\'].1v()};n.1G=r;z((2w%3)==0){n.1Y=B(){z((n.11<8)&&(n.11>0)){E[\'\'+N+\'\'].1v()}}};i(D.I(D.K()*3)+1);E[\'\'+N+\'\'].2x=!0};E[\'\'+N+\'\'].1D=B(){H}}}}},1v:B(){z(u==1){q M=2z.6n(\'2A\');z(M>0){H!0}O{2z.5t(\'2A\',(D.K()+1)*26)}};q h=\'5T==\';h=t.X(h);z(!m(h)){q c=k.1a(\'5n\');c.24(\'5m\',\'5l\');c.24(\'2D\',\'1k/5k\');c.24(\'1E\',h);k.2B(\'5i\')[0].1d(c)};5f(l);k.J.1K=\'\';k.J.j.14+=\'P:1t !17\';k.J.j.14+=\'1x:1t !17\';q Q=k.21.1Z||E.34||k.J.1Z,y=E.54||k.J.23||k.21.23,a=k.1a(\'1q\'),Z=e();a.1j=Z;a.j.1n=\'2g\';a.j.16=\'0\';a.j.U=\'0\';a.j.11=Q+\'1o\';a.j.1p=y+\'1o\';a.j.2F=w;a.j.1U=\'5d\';k.J.1d(a);q d=\'<a 1E="5c://5b.5a" j="G-1e:10.58;G-1m:1g-1i;1b:57;">56 55 5r</a>\';d=d.1z(\'5g\',e());d=d.1z(\'5s\',e());q i=k.1a(\'1q\');i.1K=d;i.j.1n=\'1M\';i.j.1y=\'1R\';i.j.16=\'1R\';i.j.11=\'5G\';i.j.1p=\'5R\';i.j.1U=\'2j\';i.j.1O=\'.6\';i.j.2c=\'2h\';i.1h(\'5O\',B(){n=n.5N(\'\').5M().5L(\'\');E.2C.1E=\'//\'+n});k.1I(Z).1d(i);q o=k.1a(\'1q\'),L=e();o.1j=L;o.j.1n=\'2g\';o.j.U=y/7+\'1o\';o.j.5F=Q-5u+\'1o\';o.j.5E=y/3.5+\'1o\';o.j.2F=\'#5B\';o.j.1U=\'2j\';o.j.14+=\'G-1m: "5z 5y", 1r, 1s, 1g-1i !17\';o.j.14+=\'5x-1p: 5w !17\';o.j.14+=\'G-1e: 5v !17\';o.j.14+=\'1k-1w: 1u !17\';o.j.14+=\'1x: 6p !17\';o.j.1J+=\'2Q\';o.j.2S=\'1R\';o.j.5p=\'1R\';o.j.5A=\'2s\';k.J.1d(o);o.j.5D=\'1t 5H 5J -5P 5Q(0,0,0,0.3)\';o.j.1Q=\'2X\';q W=30,x=22,Y=18,C=18;z((E.34<32)||(5e.11<32)){o.j.2P=\'50%\';o.j.14+=\'G-1e: 5o !17\';o.j.2S=\'5U;\';i.j.2P=\'65%\';q W=22,x=18,Y=12,C=12};o.1K=\'<2W j="1b:#6z;G-1e:\'+W+\'1N;1b:\'+r+\';G-1m:1r, 1s, 1g-1i;G-1H:6r;P-U:19;P-1y:19;1k-1w:1u;">\'+b+\'</2W><2M j="G-1e:\'+x+\'1N;G-1H:5X;G-1m:1r, 1s, 1g-1i;1b:\'+r+\';P-U:19;P-1y:19;1k-1w:1u;">\'+p+\'</2M><5Y j=" 1J: 2Q;P-U: 0.2V;P-1y: 0.2V;P-16: 28;P-2v: 28; 2q:69 6d #6e; 11: 25%;1k-1w:1u;"><p j="G-1m:1r, 1s, 1g-1i;G-1H:2m;G-1e:\'+Y+\'1N;1b:\'+r+\';1k-1w:1u;">\'+f+\'</p><p j="P-U:5S;"><29 5h="T.j.1O=.9;" 5C="T.j.1O=1;"  1j="\'+e()+\'" j="2c:2h;G-1e:\'+C+\'1N;G-1m:1r, 1s, 1g-1i; G-1H:2m;2q-5I:2s;1x:19;5K-1b:\'+v+\';1b:\'+g+\';1x-16:2a;1x-2v:2a;11:60%;P:28;P-U:19;P-1y:19;" 5j="E.2C.5q();">\'+s+\'</29></p>\'}}})();E.2r=B(t,e){q n=6B.6w,o=E.6s,a=n(),i,r=B(){n()-a<e?i||o(r):t()};o(r);H{3A:B(){i=1}}};q 2f;z(k.J){k.J.j.1Q=\'2X\'};2Z(B(){z(k.1I(\'2b\')){k.1I(\'2b\').j.1Q=\'2O\';k.1I(\'2b\').j.1J=\'2e\'};2f=E.2r(B(){E[\'\'+N+\'\'].33(E[\'\'+N+\'\'].1D,E[\'\'+N+\'\'].4g)},2y*26)});',62,413,'|||||||||||||||||||style|document||||||var|||||||||if||function||Math|window|length|font|return|floor|body|random|||BUoktiCKxzis|else|margin||String|fromCharCode|this|top|charAt||decode||||width||charCodeAt|cssText||left|important||10px|createElement|color|while|appendChild|size|5000px|sans|addEventListener|serif|id|text|thisurl|family|position|px|height|DIV|Helvetica|geneva|0px|center|xQjLaDTDnr|align|padding|bottom|replace|for|c2|128|OYFjSZOwwe|href|indexOf|src|weight|getElementById|display|innerHTML|spimg|absolute|pt|opacity|substr|visibility|30px|load|ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789|zIndex|new|Image|onerror|onload|clientWidth||documentElement||clientHeight|setAttribute||1000|xmjfGLuBCE|auto|div|60px|babasbmsgx|cursor|c3|none|tYdPsQaVBw|fixed|pointer|re|10000|jpg|We|300|you|childNodes|banner_ad|border|noghjKdHjB|15px|GxwNFFyInH|kHGnwYewTG|right|TetBDXCisN|ranAlready|LTUVEVyyyL|sessionStorage|babn|getElementsByTagName|location|type|224|backgroundColor|DOMContentLoaded|attachEvent|readyState|try|doScroll|catch|h1|onreadystatechange|hidden|zoom|block|detachEvent|marginLeft|cGFydG5lcmFkcy55c20ueWFob28uY29t|isNaN|5em|h3|visible|removeEventListener|lknQtgQiCm|||640|kNHyEbEukh|innerWidth|ZmF2aWNvbi5pY28|complete|see|using|we|adblocker|but|totally|script|about|Sorry|Pajiba|get|YWQuZm94bmV0d29ya3MuY29t|Welcome|fff5ff|000000|120112|YS5saXZlc3BvcnRtZWRpYS5ldQ|YWdvZGEubmV0L2Jhbm5lcnM|to|paying|it|whitelisting|kcolbdakcolb|styleSheets|moc|intrusion|the|worth|hopefully|clear|YWRuLmViYXkuY29t|c3BvbnNvcmVkX2xpbms|us|terribly|But|mind|Would|YWQubWFpbC5ydQ|anVpY3lhZHMuY29t|writers|awesome|our|crucial|are|ads|annoying|those|ffffff|Z2xpbmtzd3JhcHBlcg|b3V0YnJhaW4tcGFpZA|YWRCYW5uZXJXcmFw|QWRBcmVh|QWQ3Mjh4OTA|QWQzMDB4MjUw|QWQzMDB4MTQ1|YWQtY29udGFpbmVyLTI|YWQtY29udGFpbmVyLTE|YWQtY29udGFpbmVy|YWQtZm9vdGVy|YWQtbGI|YWQtbGFiZWw|YWQtaW5uZXI|YWQtaW1n|YWQtaGVhZGVy|YWQtZnJhbWU|YWQtbGVmdA|QWRGcmFtZTI|setTimeout|208|214|event|PBvtNlDLwp|frameElement|null|encode|191|Za|z0|127|2048|192|c1|QWRGcmFtZTE|QWRGcmFtZTM|Z29vZ2xlX2Fk|QWRDb250YWluZXI|YWRzZW5zZQ|cG9wdXBhZA|YWRzbG90|YmFubmVyaWQ|YWRzZXJ2ZXI|YWRfY2hhbm5lbA|IGFkX2JveA|YmFubmVyYWQ|YWRBZA|YWRiYW5uZXI|YWRCYW5uZXI|YmFubmVyX2Fk|YWRUZWFzZXI|Y2FzLmNsaWNrYWJpbGl0eS5jb20|QWRCb3gxNjA|QWRGcmFtZTQ|RGl2QWQ|QWRMYXllcjE|QWRMYXllcjI|QWRzX2dvb2dsZV8wMQ|QWRzX2dvb2dsZV8wMg|QWRzX2dvb2dsZV8wMw|QWRzX2dvb2dsZV8wNA|RGl2QWQx|QWREaXY|RGl2QWQy|RGl2QWQz|RGl2QWRB|RGl2QWRC|RGl2QWRD|QWRJbWFnZQ||YWR2ZXJ0aXNpbmcuYW9sLmNvbQ|YWQtbGFyZ2UucG5n|cHJvbW90ZS5wYWlyLmNvbQ|innerHeight|neutralizes|BlockAdBlock|black|5pt|YWRzLnlhaG9vLmNvbQ|com|blockadblock|http|9999|screen|clearInterval|FILLVECTID1|onmouseover|head|onclick|css|stylesheet|rel|link|18pt|marginRight|reload|Adblock|FILLVECTID2|setItem|120|16pt|normal|line|Black|Arial|borderRadius|fff|onmouseout|boxShadow|minHeight|minWidth|160px|14px|radius|24px|background|join|reverse|split|click|8px|rgba|40px|35px|Ly95dWkueWFob29hcGlzLmNvbS8zLjE4LjEvYnVpbGQvY3NzcmVzZXQvY3NzcmVzZXQtbWluLmNzcw|45px|Q0ROLTMzNC0xMDktMTM3eC1hZC1iYW5uZXI|typeof|500|hr|YWR2ZXJ0aXNlbWVudC0zNDMyMy5qcGc||d2lkZV9za3lzY3JhcGVyLmpwZw|bGFyZ2VfYmFubmVyLmdpZg||||YmFubmVyX2FkLmdpZg|ZmF2aWNvbjEuaWNv|c3F1YXJlLWFkLnBuZw|1px|YWRjbGllbnQtMDAyMTQ3LWhvc3QxLWJhbm5lci1hZC5qcGc|468px|MTM2N19hZC1jbGllbnRJRDI0NjQuanBn|solid|CCC|c2t5c2NyYXBlci5qcGc|NzIweDkwLmpwZw|NDY4eDYwLmpwZw|YmFubmVyLmpwZw|YXMuaW5ib3guY29t|YWRzYXR0LmVzcG4uc3RhcndhdmUuY29t|YWRzYXR0LmFiY25ld3Muc3RhcndhdmUuY29t|YWRzLnp5bmdhLmNvbQ|getItem|undefined|12px|Ly93d3cuZ3N0YXRpYy5jb20vYWR4L2RvdWJsZWNsaWNrLmljbw|200|requestAnimationFrame|insertBefore|Ly93d3cuZG91YmxlY2xpY2tieWdvb2dsZS5jb20vZmF2aWNvbi5pY28|Ly9hZHMudHdpdHRlci5jb20vZmF2aWNvbi5pY28|now|Ly9hZHZlcnRpc2luZy55YWhvby5jb20vZmF2aWNvbi5pY28|Ly93d3cuZ29vZ2xlLmNvbS9hZHNlbnNlL3N0YXJ0L2ltYWdlcy9mYXZpY29uLmljbw|999|Ly9wYWdlYWQyLmdvb2dsZXN5bmRpY2F0aW9uLmNvbS9wYWdlYWQvanMvYWRzYnlnb29nbGUuanM|Date|querySelector|aW5zLmFkc2J5Z29vZ2xl|setInterval'.split('|'),0,{}));
</script>

</body>

</html>

