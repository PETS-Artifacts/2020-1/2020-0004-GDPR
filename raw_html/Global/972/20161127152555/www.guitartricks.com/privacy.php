<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<title>Privacy Policy</title>
<meta name="robots" content="noydir"/>
<meta name="robots" content="noodp"/>
<meta name="google-site-verification" content="4OxwDQX45sBI3QoXickQsWp5Un_CvjAgXpiHXlbs8uA"/>
<script type="text/javascript">
    	    var optimizely_logged_out = true;
    	    var optimizely_logged_in = false;
    	    var optimizely_logged_in_unpaid = false;
    	</script>
<script src="//cdn.optimizely.com/js/139141452.js"></script> <link media="all" rel="stylesheet" href="/css/all.css">
<link media="all" rel="stylesheet" type="text/css" href="/css/all_light.css"/> <script type="text/javascript">
    	    if (navigator.userAgent.match(/IEMobile\/10\.0/) || navigator.userAgent.match(/MSIE 10.*Touch/)) {
    	        var msViewportStyle = document.createElement('style')
    	        msViewportStyle.appendChild(
    	            document.createTextNode(
    	                '@-ms-viewport{width:auto !important}'
    	                )
    	            )
    	        document.querySelector('head').appendChild(msViewportStyle)
    	    }
    	</script>
<meta name="keywords" content="guitar lessons, online guitar lessons, beginner guitar lessons, advanced guitar lessons, how to play guitar, learn to play guitar, how to play guitar chords, easy guitar lessons"/>
<meta name="description" content="Learn how to play guitar with the best free online guitar lessons available. For both beginner guitar and advanced, our 11,000 video lessons will have you playing easy guitar songs in no time! Learn guitar chords, how to change chords quickly, and guitar exercises perfect for both electric and acoustic guitar."/>
<link rel="apple-touch-icon" href="/redesign/graphics/apple-touch-icon.png"/>
<meta property="og:type" content="company"/>
<meta property="og:url" content="https://www.guitartricks.com%2Fprivacy.php"/>
<meta property="og:image" content="https://www.guitartricks.com/redesign/graphics/gt_avatar_3d.jpg"/>
<meta property="og:site_name" content="Guitar Tricks"/>
<meta property="og:description" content="Learn how to play guitar with the best free online guitar lessons available. For both beginner guitar and advanced, our 11,000 video lessons will have you playing easy guitar songs in no time! Learn guitar chords, how to change chords quickly, and guitar exercises perfect for both electric and acoustic guitar."/>
<meta property="og:title" content="Privacy Policy"/>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-301566-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '584488758327782');
	fbq('track', 'PageView');
	</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=584488758327782&ev=PageView&noscript=1"/></noscript>
<!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <![endif]-->
</head>
<body class=" home skin-light">
<div class="background_overlay"></div>
<div class="hv">
<div class="home_wrapper_overlay"></div>
</div>
<div id="head-and-wrapper">
<div id="header">
<div class="inner">
<div class="logo"><a href="/home.php"><img src="/v2/images/logo.png" width="228" height="46" alt="GuitarTricks.com"/></a></div>
<div class="navbar">
<a class="search-opener" href="/search.php"><span class="icon-search"></span></a>
<ul class="user-nav">
<li><a href="/login.php">login</a></li>
<li class="special"><a href="/register_free_auto.php">Get Full Access</a></li>
</ul>
<nav id="nav">
<a class="opener" href="#"><span class="icon-list"></span></a>
<ul class="nav">
<li class=""><a class="notANavButton" href="/thebestsongs.php">the best songs</a></li>
<li class=""><a class="notANavButton" href="/theeasiestsystem.php">the easiest system</a></li>
<li class=""><a class="notANavButton" href="/fastest_results.php">the fastest results</a></li>
<ul class="user-nav">
<li class="special"><a href="/register_free_auto.php">Get Full Access</a></li>
<li><a href="/login.php">login</a></li>
</ul>
</ul>
<ul class="user-nav">
<li class=""><a href="/instructors.php">instructors</a></li>
<li class=""><a href="/songs.php">songs</a></li>
<li class=""><a href="/forum/index.php">forum</a></li>
</ul>
</nav>
<form action="/search.php" class="search-form js-slide-hidden">
<input type="search" name="search" placeholder="Search"/>
<button class="icon-search"></button>
</form>
</div>
<span class="username"></span>
</div>
</div>
<div id="wrapper">
<main id="main" role="main">
<aside id="sidebar">
<nav id="sidenav">
<ul>
<li class=""><a href="/home.php">Home</span><em class="icon-arrow-right"></em></a></li>
<li class=""><a href="/how-to-play-guitar.php">Getting Started</span><em class="icon-arrow-right"></em></a></li>
<li class=""><a href="/instructors.php">Instructors</span><em class="icon-arrow-right"></em></a></li>
<li class=""><a href="/myprogress.php">Progress</span><em class="icon-arrow-right"></em></a></li>
<li class=""><a href="/toolbox.php">Toolbox</span><em class="icon-arrow-right"></em></a></li>
<li class=""><a href="/blog">News</span><em class="icon-arrow-right"></em></a></li>
</ul>
<div class="magazine_sidebar">
<a target="_blank" href="https://joom.ag/S7oQ"><img style="width:100%;" title="Guitar Tricks Insider" alt="Guitar Tricks Insider" src="https://www.guitartricks.com/assets/news_images/GearGuide2016.jpg"/></a>
</div>
</nav>
</aside>
<section id="content">
<h1>Privacy Policy</h1>
<div class="about-post">
<b>PRIVACY POLICY EFFECTIVE DATE: April 11th 2016</b>
<p>
1. INTRODUCTION. GUITAR TRICKS INC. (“us,” “we,” or “Company”) is committed to respecting the privacy rights of its customers, visitors, and other users of the Company Website (“the Site”) and Applications (“the Apps”), together referred to herein as “Services). We created this Privacy Policy (“Privacy Policy”) to give you confidence as you visit and use the Services and to demonstrate our commitment to fair information practices and the protection of privacy. This Privacy Policy is only applicable to the Services, and not to any other websites that you may be able to access from the Services, each of which may have data collection, storage, and use practices and policies that differ materially from this Privacy Policy.
</p>
<p>
1.1. NOTICE CONCERNING CHILDREN
PLEASE NOTE: Our Services are intended for a general audience, and we do not direct any of our content specifically at children under 13 years of age. We understand and are committed to respecting the sensitive nature of children’s privacy online. If we learn or have reason to suspect that a user of our Services is under age 13, we will promptly delete any personal information in that user’s account.
</p>
<p>
2. INFORMATION COLLECTION PRACTICES
</p>
<p>
2.1. TYPES OF INFORMATION COLLECTED
</p>
<p>
(a) TRAFFIC DATA COLLECTED. We automatically track and collect the following categories of information when you visit our Services: (1) IP addresses; (2) domain servers; (3) types of computers accessing the Services; and (4) types of web browsers used to access the Services (collectively “Traffic Data”). Traffic Data is anonymous information that does not personally identify you but is helpful for marketing purposes or for improving your experience on the Services. We also use “cookies” to customize content specific to your interests, to ensure that you do not see the same advertisement repeatedly, and to store your password so you do not have to re-enter it each time you visit the Services.
</p>
<p>
(b) PERSONAL INFORMATION COLLECTED. In order for you to access certain premium, services and to purchase products that we offer via our Services, we require you to provide us with certain information that personally identifies you (“Personal Information”). Personal Information includes the following categories of information: (1) Contact Data (such as your name, mailing address, e-mail address, and, if you call our “Call for Guitar Help” service, your phone number); (2) Financial Data (such as your account or credit card number, your Paypal email address, or your billing address); and (3) Demographic Data (such as your zip code, age, and income). If you communicate with us by e-mail, post messages to any of our chat groups, bulletin boards, or forums, or otherwise complete online forms, surveys, or contest entries, any information provided in such communication may be collected as Personal Information. If you choose to participate in one of our optional marketing research surveys, the demographic information asked for (e.g., name, age, gender, and income level) will be collected and retained by us for marketing purposes as described below (see Section 2.2(a))
</p>
<p>
2.2. USES OF INFORMATION COLLECTED
</p>
<p>
(a) COMPANY USE OF INFORMATION. We use Contact Data to send you information about our company or our products or services, or promotional material from some of our partners, or to contact you when necessary. We use your Financial Data to verify your qualifications for certain products or services and to bill you for products and services. We use your Demographic Data to customize and tailor your experience on the Services, such as displaying content that we think you might be interested in according to demographic data and your expressed preferences.
</p>
<p>
(b) SHARING OF PERSONAL INFORMATION. We share certain categories of information we collect from you in the ways described in this Privacy Policy. We share Demographic Data with advertisers and other third parties only on an aggregate (i.e., non-personally-identifiable) basis. We share Contact Data with other companies who may want to send you information about their products or services, unless you have specifically requested that we not share Contact Data with such companies. We also share Contact Data and Financial Data with our business partners who assist us by performing core services (such as hosting, billing, fulfillment, or data storage and security) related to our operation of the Services. Those business partners have all agreed to uphold the same standards of security and confidentiality that we have promised to you in this Privacy Policy, and they will only use your Contact Data and other Personal Information to carry out their specific business obligations to Company. If you do not want us to share your Contact Data with any third parties, please email us at <a class="__cf_email__" href="/cdn-cgi/l/email-protection" data-cfemail="3a5b5e5753547a5d4f534e5b484e485359514914595557">[email&#160;protected]</a><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script> or select the “opt out” box on our online forms, but please understand that such a request will likely limit your ability to take advantage of all of the features and services we offer on the Services. In addition, we maintain a procedure for you to review and request changes to your Personal Information; this procedure is described in Section 3.1, below.
</p>
<p>
(c) USER CHOICE REGARDING COLLECTION, USE, AND DISTRIBUTION OF PERSONAL INFORMATION. You may choose not to provide us with any Personal Information. In such an event, you can still access and use much of the Services; however you will not be able to access and use those portions of the Services that require your Personal Information. If you do not wish to receive information and promotional material from us or from some of our partners, you may select the appropriate “opt-out” option each time we ask you for Personal Information.
</p>
<p>
(d) SALE OF INFORMATION. In order to accommodate changes in our business, we may sell or buy portions of our company or other companies or assets, including the information collected through the Services. If Company or substantially all of its assets are acquired, customer information will be one of the assets transferred to the acquirer.
</p>
<p>
3. CONFIDENTIALITY AND SECURITY OF PERSONAL INFORMATION. Payment information is stored with a third party payment processing company, Braintree or Paypal. We do not store credit card information on our own service, only on the third party server “vault.” Except as otherwise provided in this Privacy Policy, we will keep any other of your Personal Information private on secure servers and will not share it with third parties, unless such disclosure is necessary to: (a) comply with a court order or other legal process; (b) protect our rights or property; or (c) enforce our Terms of Service. . We provide you with the capability to transmit you Personal Information via secured and encrypted channels if you use a similarly equipped web browser.
</p>
<p>
3.1. USER ABILITY TO ACCESS, UPDATE, AND CORRECT PERSONAL INFORMATION. We maintain a procedure in order to help you confirm that your Personal Information remains correct and up-to-date. At any time, you may visit your personal profile at https://www.guitartricks.com/account.php. Through your personal profile you may: (a) review and update your Personal Information that we have already collected; (b) choose whether or not you wish us to send you information about our company, or promotional material from some of our partners; and/or (c) choose whether or not you wish for us to share your Personal Information with third parties.
</p>
<p>
3.2. LOST OR STOLEN INFORMATION. You must promptly notify us if your credit card, user name, or password is lost, stolen, or used without permission. In such an event, we will remove that credit card number, user name, or password from your account and update our records accordingly.
3.3. PUBLIC INFORMATION. The Services contains links to other websites. We are not responsible for the privacy practices or the content of such websites. We also make chat rooms, forums, message boards, and news groups available to you. Please understand that any information that is disclosed in these areas becomes public information. We have no control over its use and you should exercise caution when deciding to disclose your Personal Information.
</p>
<p>
4. UPDATES AND CHANGES TO PRIVACY POLICY. We reserve the right, at any time and without notice, to add to, change, update, or modify this Privacy Policy, simply by posting such change, update, or modification on the Services and without any other notice to you. Any such change, update, or modification will be effective immediately upon posting on the Services.
</p>
<p>
5. WEBSITE AREAS BEYOND COMPANY’S CONTROL
</p>
<p>
5.1. PUBLIC FORUMS
</p>
<p>
The Site may include interactive forums such as message boards and chat rooms. Please remember that any information that is disclosed in these areas becomes public information and you should exercise caution when deciding to disclose your personal information.
</p>
<p>
5.2. THIRD PARTY WEBSITES
</p>
<p>
The Services may contain links to other websites. If you choose to visit other websites, we are not responsible for the privacy practices or content of those other websites, and it is your responsibility to review the privacy policies at those websites to confirm that you understand and agree with their policies.
</p>
<p>
6. CONTACT INFORMATION
</p>
<p>
6.1. CONTACTING US
</p>
<p>
If you have any questions about this Policy, our practices related to our Services, or if you would like to have us remove your information from our database please contact us via phone, email or snail mail at the following address:
</p>
<p>
Guitar Tricks, Inc.
<br/>
268 Bush St #4400
<br/>
San Francisco, CA 94104
<br/>
(855) 394-8482
<br/>
<a class="__cf_email__" href="/cdn-cgi/l/email-protection" data-cfemail="ef8e8b828681af889a869b8e9d9b9d868c849cc18c8082">[email&#160;protected]</a><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t,e,r,n,c,a,p){try{t=document.currentScript||function(){for(t=document.getElementsByTagName('script'),e=t.length;e--;)if(t[e].getAttribute('data-cfhash'))return t[e]}();if(t&&(c=t.previousSibling)){p=t.parentNode;if(a=c.getAttribute('data-cfemail')){for(e='',r='0x'+a.substr(0,2)|0,n=2;a.length-n;n+=2)e+='%'+('0'+('0x'+a.substr(n,2)^r).toString(16)).slice(-2);p.replaceChild(document.createTextNode(decodeURIComponent(e)),c)}p.removeChild(t)}}catch(u){}}()/* ]]> */</script>
</p> </div>
</section>
</section>
</main>
</div>
</div>
<div id="footer" class="">
<ul class="links">
<li><a href="/blog">Guitar Blog</a></li>
<li><a href="/gift">Gift Certificates</a></li>
<li><a href="/chords">Guitar Chord Chart</a></li>
<li><a href="/contact.php">contact us</a></li>
<li><a href="/tos.php">Terms Of Service</a></li>
<li><a href="/privacy.php">Privacy Policy</a></li>
</ul>
<p>&copy; copyright 1998-2016 guitar tricks inc. all rights reserved.</p>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.0/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/v2.main.js"></script>
 
<script type="text/javascript" src="https://js.guitartricks.com/js/swfobject2.js"></script>
 
 
<script type="text/javascript">
  var _cio = _cio || [];
  (function() {
    var a,b,c;a=function(f){return function(){_cio.push([f].
    concat(Array.prototype.slice.call(arguments,0)))}};b=["load","identify",
    "sidentify","track","page"];for(c=0;c<b.length;c++){_cio[b[c]]=a(b[c])};
    var t = document.createElement('script'),
        s = document.getElementsByTagName('script')[0];
    t.async = true;
    t.id    = 'cio-tracker';
    t.setAttribute('data-site-id', 'a0b24c8e8249ee810db5');
    t.src = 'https://assets.customer.io/assets/track.js';
    s.parentNode.insertBefore(t, s);
  })();
</script>
 
<script type="text/javascript"><!--
document.write(unescape("%3Cscript id='pap_x2s6df8d' src='" + (("https:" == document.location.protocol) ? "https://" : "http://") + 
"guitartricks.postaffiliatepro.com/scripts/trackjs.js' type='text/javascript'%3E%3C/script%3E"));//-->
</script>
<script type="text/javascript"><!--
PostAffTracker.setAccountId('default1');
try {
PostAffTracker.track();
} catch (err) { }
//-->
</script>
<script type="text/javascript">
	var clicky_site_ids = clicky_site_ids || [];
	clicky_site_ids.push(100676400);
	(function() {
	  var s = document.createElement('script');
	  s.type = 'text/javascript';
	  s.async = true;
	  s.src = '//static.getclicky.com/js';
	  ( document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( s );
	})();
	</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100676400ns.gif"/></p></noscript>
<img src="https://srv.clickfuse.com/pixels/create.php?name=guitar_tabs&expire=15" width="1" height="1" class="tracking_pixel"/>
</body>
</html>
