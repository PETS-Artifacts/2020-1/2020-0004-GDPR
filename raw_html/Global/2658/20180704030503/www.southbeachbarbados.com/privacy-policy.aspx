<!DOCTYPE HTML> <html lang="en"> <head><meta name="twitter:card" content="summary"><meta name="twitter:description" content="Privacy Policy"><meta name="twitter:image" content="http://www.southbeachbarbados.com/resourcefiles/homeimages/south-beach-exterior.jpg"><meta name="twitter:site" content="www.southbeachbarbados.com"><meta name="twitter:title" content="Privacy Policy"><meta property="og:image" content="http://www.southbeachbarbados.com/resourcefiles/homeimages/south-beach-exterior.jpg"><meta property="og:title" content="Privacy Policy"><meta property="og:description" content="Privacy Policy"> <title>Privacy Policy</title><meta name="robots" content="noindex,nofollow"> <meta content="noindex,nofollow" name="robots"> <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> <meta name="ROBOTS" content="NOARCHIVE"> <meta name="description" content="Privacy Policy"> <!--[if IE]><script type="text/javascript" src="http://www.southbeachbarbados.com/js/html5.aspx"></script><![endif]--> <link href="http://www.southbeachbarbados.com/css/print.aspx" rel="stylesheet" type="text/css" media="print"> <link rel="shortcut icon" href="http://www.southbeachbarbados.com/favicon.ico"> <link href="https://plus.google.com/u/0/101217215449030632362" rel="publisher"> <script type="text/javascript">var headSection3 = document.getElementsByTagName("head")[0]; var elementCSS3 = document.createElement("link");elementCSS3.type = "text/css";elementCSS3.rel = "stylesheet";elementCSS3.href = "http://www.southbeachbarbados.com/css/fonts.aspx";elementCSS3.media = "non-existant-media";headSection3.appendChild(elementCSS3);setTimeout(function () {elementCSS3.media = "all";});</script> <link href="http://www.southbeachbarbados.com/dynamic/css/subnav.aspx" type="text/css" rel="stylesheet"> <style>
#logo {
	position: absolute;
}
#container {
	padding-top: 130px;
}
</style> <script type="application/ld+json">
{
  "exportFormatVersion": 2,
  "exportTime": "2018-02-01 11:48:04",
  "containerVersion": {
    "path": "accounts/586518963/containers/2775670/versions/0",
    "accountId": "586518963",
    "containerId": "2775670",
    "containerVersionId": "0",
    "container": {
      "path": "accounts/586518963/containers/2775670",
      "accountId": "586518963",
      "containerId": "2775670",
      "name": "www.southbeachbarbados.com",
      "publicId": "GTM-5KS7PQ",
      "usageContext": [
        "WEB"
      ],
      "fingerprint": "1478108749363",
      "tagManagerUrl": "https://tagmanager.google.com/#/container/accounts/586518963/containers/2775670/workspaces?apiLink=container"
    },
    "tag": [
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "tagId": "1",
        "name": "Booking Pages",
        "type": "ua",
        "parameter": [
          {
            "type": "BOOLEAN",
            "key": "overrideGaSettings",
            "value": "true"
          },
          {
            "type": "BOOLEAN",
            "key": "doubleClick",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "setTrackerName",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "useDebugVersion",
            "value": "false"
          },
          {
            "type": "LIST",
            "key": "fieldsToSet",
            "list": [
              {
                "type": "MAP",
                "map": [
                  {
                    "type": "TEMPLATE",
                    "key": "fieldName",
                    "value": "cookieDomain"
                  },
                  {
                    "type": "TEMPLATE",
                    "key": "value",
                    "value": "{{gaDomain}}"
                  }
                ]
              },
              {
                "type": "MAP",
                "map": [
                  {
                    "type": "TEMPLATE",
                    "key": "fieldName",
                    "value": "allowLinker"
                  },
                  {
                    "type": "TEMPLATE",
                    "key": "value",
                    "value": "true"
                  }
                ]
              }
            ]
          },
          {
            "type": "BOOLEAN",
            "key": "useHashAutoLink",
            "value": "false"
          },
          {
            "type": "TEMPLATE",
            "key": "trackType",
            "value": "TRACK_PAGEVIEW"
          },
          {
            "type": "BOOLEAN",
            "key": "decorateFormsAutoLink",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "enableLinkId",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "enableEcommerce",
            "value": "false"
          },
          {
            "type": "TEMPLATE",
            "key": "trackingId",
            "value": "{{Tracking ID}}"
          }
        ],
        "fingerprint": "1478097011456",
        "firingTriggerId": [
          "8"
        ],
        "tagFiringOption": "ONCE_PER_EVENT"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "tagId": "2",
        "name": "Ecommerce Tracking Tag",
        "type": "ua",
        "parameter": [
          {
            "type": "BOOLEAN",
            "key": "overrideGaSettings",
            "value": "true"
          },
          {
            "type": "BOOLEAN",
            "key": "doubleClick",
            "value": "true"
          },
          {
            "type": "BOOLEAN",
            "key": "setTrackerName",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "useDebugVersion",
            "value": "false"
          },
          {
            "type": "TEMPLATE",
            "key": "trackType",
            "value": "TRACK_TRANSACTION"
          },
          {
            "type": "BOOLEAN",
            "key": "enableLinkId",
            "value": "false"
          },
          {
            "type": "TEMPLATE",
            "key": "trackingId",
            "value": "{{Tracking ID}}"
          }
        ],
        "fingerprint": "1478097092125",
        "firingTriggerId": [
          "7"
        ],
        "tagFiringOption": "ONCE_PER_EVENT"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "tagId": "3",
        "name": "Reservation Pages",
        "type": "ua",
        "parameter": [
          {
            "type": "BOOLEAN",
            "key": "useDebugVersion",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "useHashAutoLink",
            "value": "false"
          },
          {
            "type": "TEMPLATE",
            "key": "trackType",
            "value": "TRACK_PAGEVIEW"
          },
          {
            "type": "TEMPLATE",
            "key": "autoLinkDomains",
            "value": "{{Auto Link Booking Engine Domains}}"
          },
          {
            "type": "BOOLEAN",
            "key": "decorateFormsAutoLink",
            "value": "true"
          },
          {
            "type": "BOOLEAN",
            "key": "useEcommerceDataLayer",
            "value": "true"
          },
          {
            "type": "BOOLEAN",
            "key": "overrideGaSettings",
            "value": "true"
          },
          {
            "type": "BOOLEAN",
            "key": "doubleClick",

            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "setTrackerName",
            "value": "false"
          },
          {
            "type": "LIST",
            "key": "fieldsToSet",
            "list": [
              {
                "type": "MAP",
                "map": [
                  {
                    "type": "TEMPLATE",
                    "key": "fieldName",
                    "value": "allowLinker"
                  },
                  {
                    "type": "TEMPLATE",
                    "key": "value",
                    "value": "true"
                  }
                ]
              },
              {
                "type": "MAP",
                "map": [
                  {
                    "type": "TEMPLATE",
                    "key": "fieldName",
                    "value": "cookieDomain"
                  },
                  {
                    "type": "TEMPLATE",
                    "key": "value",
                    "value": "{{gaDomain}}"
                  }
                ]
              },
              {
                "type": "MAP",
                "map": [
                  {
                    "type": "TEMPLATE",
                    "key": "fieldName",
                    "value": "hitCallback"
                  },
                  {
                    "type": "TEMPLATE",
                    "key": "value",
                    "value": "{{Decorate Booking Form}}"
                  }
                ]
              },
              {
                "type": "MAP",
                "map": [
                  {
                    "type": "TEMPLATE",
                    "key": "fieldName",
                    "value": "referrer"
                  },
                  {
                    "type": "TEMPLATE",
                    "key": "value",
                    "value": "{{rewritten referrer}}"
                  }
                ]
              }
            ]
          },
          {
            "type": "BOOLEAN",
            "key": "enableLinkId",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "enableEcommerce",
            "value": "true"
          },
          {
            "type": "TEMPLATE",
            "key": "trackingId",
            "value": "{{Tracking ID}}"
          }
        ],
        "fingerprint": "1478097228715",
        "firingTriggerId": [
          "5",
          "9",
          "10"
        ],
        "tagFiringOption": "ONCE_PER_EVENT"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "tagId": "4",
        "name": "Widget Analytics Tag",
        "type": "ua",
        "parameter": [
          {
            "type": "BOOLEAN",
            "key": "overrideGaSettings",
            "value": "true"
          },
          {
            "type": "BOOLEAN",
            "key": "doubleClick",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "setTrackerName",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "useDebugVersion",
            "value": "false"
          },
          {
            "type": "LIST",
            "key": "fieldsToSet",
            "list": [
              {
                "type": "MAP",
                "map": [
                  {
                    "type": "TEMPLATE",
                    "key": "fieldName",
                    "value": "allowLinker"
                  },
                  {
                    "type": "TEMPLATE",
                    "key": "value",
                    "value": "true"
                  }
                ]
              },
              {
                "type": "MAP",
                "map": [
                  {
                    "type": "TEMPLATE",
                    "key": "fieldName",
                    "value": "referrer"
                  },
                  {
                    "type": "TEMPLATE",
                    "key": "value",
                    "value": "{{rewritten referrer}}"
                  }
                ]
              }
            ]
          },
          {
            "type": "BOOLEAN",
            "key": "useHashAutoLink",
            "value": "false"
          },
          {
            "type": "TEMPLATE",
            "key": "trackType",
            "value": "TRACK_PAGEVIEW"
          },
          {
            "type": "TEMPLATE",
            "key": "autoLinkDomains",
            "value": "{{Auto Link Widget Domain}}"
          },
          {
            "type": "BOOLEAN",
            "key": "decorateFormsAutoLink",
            "value": "true"
          },
          {
            "type": "BOOLEAN",
            "key": "enableLinkId",
            "value": "false"
          },
          {
            "type": "BOOLEAN",
            "key": "enableEcommerce",
            "value": "false"
          },
          {
            "type": "TEMPLATE",
            "key": "trackingId",
            "value": "{{Tracking ID}}"
          }
        ],
        "fingerprint": "1478097298482",
        "firingTriggerId": [
          "11",
          "12"
        ],
        "tagFiringOption": "ONCE_PER_EVENT"
      }
    ],
    "trigger": [
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "triggerId": "6",
        "name": "Booking Details Ecommerce",
        "type": "DOM_READY",
        "filter": [
          {
            "type": "CONTAINS",
            "parameter": [
              {
                "type": "TEMPLATE",
                "key": "arg0",
                "value": "{{Page URL}}"
              },
              {
                "type": "TEMPLATE",
                "key": "arg1",
                "value": "/b/"
              }
            ]
          }
        ],
        "fingerprint": "1478096435865"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "triggerId": "7",
        "name": "Booking Details Page",
        "type": "DOM_READY",
        "filter": [
          {
            "type": "CONTAINS",
            "parameter": [
              {
                "type": "TEMPLATE",
                "key": "arg0",
                "value": "{{Page URL}}"
              },
              {
                "type": "TEMPLATE",
                "key": "arg1",
                "value": "/b/"
              }
            ]
          }
        ],
        "fingerprint": "1478096412680"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "triggerId": "8",
        "name": "Checkout process",
        "type": "PAGEVIEW",
        "filter": [
          {
            "type": "CONTAINS",
            "parameter": [
              {
                "type": "TEMPLATE",
                "key": "arg0",
                "value": "{{Page URL}}"
              },
              {
                "type": "TEMPLATE",
                "key": "arg1",
                "value": "rgv3.secure-hotel-reservations.com"
              }
            ]
          }
        ],
        "fingerprint": "1478096466780"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "triggerId": "9",
        "name": "Hotels",
        "type": "PAGEVIEW",
        "filter": [
          {
            "type": "CONTAINS",
            "parameter": [
              {
                "type": "TEMPLATE",
                "key": "arg0",
                "value": "{{Page URL}}"
              },
              {
                "type": "TEMPLATE",
                "key": "arg1",
                "value": "hotels-in"
              }
            ]
          }
        ],
        "fingerprint": "1478096499804"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "triggerId": "10",
        "name": "Rooms",
        "type": "PAGEVIEW",
        "filter": [
          {
            "type": "CONTAINS",
            "parameter": [
              {
                "type": "TEMPLATE",
                "key": "arg0",
                "value": "{{Page URL}}"
              },
              {
                "type": "TEMPLATE",
                "key": "arg1",
                "value": "rooms-at-"
              }
            ]
          }
        ],
        "fingerprint": "1478096529961"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "triggerId": "5",
        "name": "South Beach Hotel Hotels List",
        "type": "PAGEVIEW",
        "filter": [
          {
            "type": "CONTAINS",
            "parameter": [
              {
                "type": "TEMPLATE",
                "key": "arg0",
                "value": "{{Page URL}}"
              },
              {
                "type": "TEMPLATE",
                "key": "arg1",
                "value": "bookings.southbeachbarbados.com"
              }
            ]
          }
        ],
        "fingerprint": "1478096326556"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "triggerId": "12",
        "name": "Widget Page v1",
        "type": "PAGEVIEW",
        "filter": [
          {
            "type": "CONTAINS",
            "parameter": [
              {
                "type": "TEMPLATE",
                "key": "arg0",
                "value": "{{Page URL}}"
              },
              {
                "type": "TEMPLATE",
                "key": "arg1",
                "value": "gadgets.hotelinternethelp.com"
              }
            ]
          }
        ],
        "fingerprint": "1478096839581"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "triggerId": "11",
        "name": "Widget Page v2",
        "type": "PAGEVIEW",
        "filter": [
          {
            "type": "CONTAINS",
            "parameter": [
              {
                "type": "TEMPLATE",
                "key": "arg0",
                "value": "{{Page URL}}"
              },
              {
                "type": "TEMPLATE",
                "key": "arg1",
                "value": "widgets.regattatravelsolutions.com"
              }
            ]
          }
        ],
        "fingerprint": "1478096789185"
      }
    ],
    "variable": [
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "variableId": "7",
        "name": "Auto Link Booking Engine Domains",
        "type": "c",
        "parameter": [
          {
            "type": "TEMPLATE",
            "key": "value",
            "value": "regattatravelsolutions.com,southbeachbarbados.com,secure-hotel-reservations.com"
          }
        ],
        "fingerprint": "1478095897298"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "variableId": "6",
        "name": "Auto Link Widget Domain",
        "type": "c",
        "parameter": [
          {
            "type": "TEMPLATE",
            "key": "value",
            "value": "gadgets.hotelinternethelp.com,widgets.regattatravelsolutions.com,southbeachbarbados.com,secure-hotel-reservations.com"
          }
        ],
        "fingerprint": "1478095821974"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "variableId": "5",
        "name": "Decorate Booking Form",
        "type": "jsm",
        "parameter": [
          {
            "type": "TEMPLATE",
            "key": "javascript",
            "value": "function() {\n\treturn function() {\n  \ttry {\n    \tvar gobj = window[window.GoogleAnalyticsObject],\n        form = document.querySelector('#fmr_book'),\n        tracker, linker;\n     \tif(gobj && form.length) {\n      \ttracker = gobj.getAll()[0];\n       \tlinker = new window.gaplugins.Linker(tracker);\n       \tform.action = linker.decorate(form.action);\n      }\n    }\n   catch(e) {}\n  }\n}"
          }
        ],
        "fingerprint": "1478095145164"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "variableId": "1",
        "name": "Tracking ID",
        "type": "c",
        "parameter": [
          {
            "type": "TEMPLATE",
            "key": "value",
            "value": "REPLACE WITH GOOGLE ANALYTICS CODE"
          }
        ],
        "fingerprint": "1478094826214"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "variableId": "4",
        "name": "gaCrossDomain",
        "type": "c",
        "parameter": [
          {
            "type": "TEMPLATE",
            "key": "value",
            "value": "regattatravelsolutions.com,southbeachbarbados.com,secure-hotel-reservations.com"
          }
        ],
        "fingerprint": "1478095922010"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "variableId": "3",
        "name": "gaDomain",
        "type": "c",
        "parameter": [
          {
            "type": "TEMPLATE",
            "key": "value",
            "value": "auto"
          }
        ],
        "fingerprint": "1478095019026"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "variableId": "2",
        "name": "rewritten referrer",
        "type": "jsm",
        "parameter": [
          {
            "type": "TEMPLATE",
            "key": "javascript",
            "value": "function() {\n\tvar theReferrer = document.referrer;\n\tif (theReferrer) {\n\n    theReferrer = theReferrer.split(\"referrer\");\n   \tif(theReferrer.length > 1)\n    {\n    \ttheReferrer = theReferrer[1].split(\"=\");\n     \treturn theReferrer[1];\n    }\n    return document.referrer;\n\t}\n\treturn document.referrer;\n}"
          }
        ],
        "fingerprint": "1478094965786"
      }
    ],
    "builtInVariable": [
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "type": "PAGE_URL",
        "name": "Page URL"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "type": "PAGE_HOSTNAME",
        "name": "Page Hostname"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "type": "PAGE_PATH",
        "name": "Page Path"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "type": "EVENT",
        "name": "Event"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "type": "CLICK_CLASSES",
        "name": "Click Classes"
      },
      {
        "accountId": "586518963",
        "containerId": "2775670",
        "type": "CLICK_ID",
        "name": "Click ID"
      }
    ],
    "fingerprint": "0",
    "tagManagerUrl": "https://tagmanager.google.com/#/versions/accounts/586518963/containers/2775670/versions/0?apiLink=version"
  }
}
</script> <link rel='canonical' href='http://www.southbeachbarbados.com/privacy-policy.aspx'><meta name="generator" content="Milestone CMS 6.0"><script id="mjdata" type="application/ld+json">[{
 "@context": "http://schema.org",
 "@type": "WebPage",
 "dateCreated": "2018-06-18",
 "dateModified": "2018-06-23",
 "datePublished": "2018-06-28",
 "name": "Privacy Policy",
 "description": "Privacy Policy",
 "url": "http://www.southbeachbarbados.com/privacy-policy.aspx",
 "primaryImageOfPage": [],
 "breadcrumb": [
  {
   "@context": "http://schema.org",
   "@type": "BreadcrumbList",
   "itemListElement": [
    {
     "@type": "ListItem",
     "position": "1",
     "item": {
      "@id": "http://www.southbeachbarbados.com/",
      "name": "Home",
      "url": "http://www.southbeachbarbados.com/"
     }
    },
    {
     "@type": "ListItem",
     "position": "2",
     "item": {
      "name": "Privacy Policy"
     }
    }
   ]
  }
 ],
 "text": ""
},{
 "@context": "http://schema.org",
 "@type": "Hotel",
 "name": "South Beach Barbados",
 "url": "http://www.southbeachbarbados.com/",
 "logo": "http://www.southbeachbarbados.com/images/south-beach-barbados-logo.png",
 "image": "http://www.southbeachbarbados.com/images/south-beach-barbados-logo.png",
 "contactPoint": {
  "@type": "ContactPoint",
  "telephone": "+1-246-435-8561",
  "email": "reservations@oceanhotels.bb",
  "contactType": "Reservations and Booking"
 },
 "telephone": "+1-246-435-8561",
 "address": {
  "@type": "PostalAddress",
  "streetAddress": "Rockley at Accra Beach",
  "addressLocality": "Christ Church",
  "addressRegion": "BB",
  "postalCode": "BB15 139"
 },
 "geo": {
  "@type": "GeoCoordinates",
  "latitude": "13.07492",
  "longitude": "-59.5909987"
 },
 "aggregateRating": {
  "@type": "AggregateRating",
  "ratingValue": "4.0",
  "reviewCount": "2",
  "bestRating": "4",
  "worstRating": "1"
 }
}]</script></head><body> <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P25T65" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P25T65');</script> <a class="ADAaccessible" href="javascript:void(0);">[Skip to Content]</a><a style="display: none;" href="http://www.southbeachbarbados.com/privacy-policy.aspx">Privacy Policy</a> <div id="backtotop"><span class="icon-arrow-up2"></span></div> <div id="wrapper"> <div id="main"> <header> <div id="logo"> <a href="http://www.southbeachbarbados.com/"> <img alt="South Beach Barbados - Rockley at Accra Beach, Barbados BB15 139" src="http://www.southbeachbarbados.com/images/south-beach-barbados-logo.png" height="70" width="195"></a> </div> <div id="number"> Christ Church, Barbados <span>Hotel : 1-246-428-9441 </span> <span>Toll Free : 1-888-964-0030</span> </div> <div id="subnav"> <ul class=""> <li class="sub-abtus"><a href="http://www.southbeachbarbados.com/about-us.aspx" title="About Us" class="nav">About Us</a> </li> <li class="sub-spls"><a href="http://www.southbeachbarbados.com/special-pkg" title="Specials" class="nav">Specials</a></li> <li class="sub-family"><a href="http://www.southbeachbarbados.com/family-offers.aspx" title="Family Offers" class="nav">Family Offers</a></li> <li class="sub-group"><a href="http://www.southbeachbarbados.com/groups.aspx" title="Groups" class="nav">Groups</a></li> <li class="sub-meetings"><a href="http://www.southbeachbarbados.com/meetings.aspx" title="Meetings" class="nav">Meetings</a></li> <li class="sub-emap"><a href="http://www.southbeachbarbados.com/map.aspx" title="E-Map" class="nav">E-Map</a></li> <li class="mobile-block sub-cont"><a href="http://www.southbeachbarbados.com/contact-us.aspx" title="Contact Us" class="nav">Contact Us</a></li> <li class="sub-photos"><a href="http://www.southbeachbarbados.com/photos.aspx" title="Photos" class="nav">Photos</a> </li> </ul> </div> <div id="gtranslate"> <div id="google_translate_element"></div> </div> <script type="text/javascript">
function googleTranslateElementInit() {
	 new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false},'google_translate_element');
	}    
(function() {
	setTimeout(function(){ 
		var googleTranslateScript = document.createElement('script');
		googleTranslateScript.type = 'text/javascript';
		googleTranslateScript.async = true;
		googleTranslateScript.src = '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit';
		(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0] ).appendChild( googleTranslateScript );
	}, 5000)
})();
</script> <div id="reservation" class="reservation-close"> <span class="res-text">Check Availability</span> <div id="reservationtable"> <div id="kognitiv-widget"></div> <div class="clear"></div> </div> <div class="clear"></div> </div> <script type="text/javascript">
!function(e,t,n,i){i="string"==typeof i?i:"__kogWidget",e.KognitivWindowObject=i,i in e||(e[i]={c:null,r:!1,push:function(){this.wg.push(arguments),"object"==typeof this.c&&this.r&&this.c.new(arguments[0])},wg:[],init:function(){this.r=!0,this.wg.forEach(function(t){e[i].c.new(t[0])})}}),e[i].t=(new Date).getTime();var c=t.createElement("script");c.src="//d3knk0dmijgaf9.cloudfront.net/main.js",c.async=!0;var s=t.getElementsByTagName("script")[0];s.parentNode.insertBefore(c,s);}(window,document);

__kogWidget.push({rootId:'kognitiv-widget', clientId: 'SOUTHB', widgetId: 1});
</script> </header> <div id="nav"> <nav id="topnav"> <ul class="topnav"> <li class="home"> <a class="nav" title="Home" href="http://www.southbeachbarbados.com/"><span class="home-icon icon-home"></span> <span class="home-text">Home</span> </a> </li> <li class="location"><a href="http://www.southbeachbarbados.com/location.aspx" title="Location" class="nav"> Location</a></li> <li class="accommodations submenulist"> <a href="http://www.southbeachbarbados.com/suites" title="Accommodations" class="nav">Accommodations</a><span class="nav-arrow icon-downArrow2"></span> <ul class="room dropnav" style="display:none"> <li><a href="http://www.southbeachbarbados.com/suites#junior-suite" class="nav">Junior Suite</a></li> <li><a href="http://www.southbeachbarbados.com/suites#one-bedroom-suite" class="nav">One Bedroom Suite </a></li> <li><a href="http://www.southbeachbarbados.com/suites#two-bedroom-suite" class="nav">Two Bedroom Suite </a></li> <li><a href="http://www.southbeachbarbados.com/suites#one-bedroom-penthouse-suite" class="nav">One Bedroom Penthouse Suite</a></li> </ul> </li> <li class="amenities submenulist"> <a href="http://www.southbeachbarbados.com/resort-features.aspx" title="Resort Features" class="nav">Resort Features</a><span class="nav-arrow icon-downArrow2"></span> <ul class="resortfet dropnav" style="display:none"> <li><a href="http://www.southbeachbarbados.com/resort-features.aspx#outdoor-pool-beach" class="nav">Outdoor Pool and Beach</a></li> <li><a href="http://www.southbeachbarbados.com/resort-features.aspx#rockley-beach" class="nav">Rockley Beach</a></li> <li><a href="http://www.southbeachbarbados.com/resort-features.aspx#south-coast-boardwalk" class="nav">South Coast Boardwalk</a></li> <li><a href="http://www.southbeachbarbados.com/resort-features.aspx#the-lincoln-room" class="nav">The Lincoln Room</a></li> <li><a href="http://www.southbeachbarbados.com/resort-features.aspx#rooftop-terrace" class="nav">Rooftop Terrace </a></li> <li><a href="http://www.southbeachbarbados.com/resort-features.aspx#personalized-concierge" class="nav">Personalized Concierge</a></li> </ul> </li> <li class="dining submenulist"> <a class="nav" title="Dining" href="http://www.southbeachbarbados.com/dining.aspx">Dining</a> <span class="nav-arrow icon-downArrow2"></span> <ul class="din dropnav" style="display:none"> <li><a href="http://www.southbeachbarbados.com/dining.aspx#swagg-cocktail-lounge-bar" class="nav">Swagg Cocktail Lounge & Bar</a></li> <li><a href="http://www.southbeachbarbados.com/dining.aspx#dine-by-design" class="nav">Dine by Design </a></li> <li><a href="http://www.southbeachbarbados.com/dining.aspx#taste-restaurant" class="nav">Taste Restaurant</a></li> <li><a href="http://www.southbeachbarbados.com/dining.aspx#azul-restaurant" class="nav">Azul Restaurant</a></li> </ul> </li> <li class="act submenulist"> <a href="http://www.southbeachbarbados.com/activities.aspx" title="Activities" class="nav">Activities</a><span class="nav-arrow icon-downArrow2"></span> <ul class="activities dropnav" style="display:none"> <li><a href="http://www.southbeachbarbados.com/activities.aspx#spa-treatments" class="nav">Spa Treatments</a></li> <li><a href="http://www.southbeachbarbados.com/activities.aspx#golf" class="nav">Golf</a></li> <li><a href="http://www.southbeachbarbados.com/activities.aspx#diving" class="nav">Diving</a></li> <li><a href="http://www.southbeachbarbados.com/activities.aspx#tennis" class="nav">Tennis</a></li> <li><a href="http://www.southbeachbarbados.com/activities.aspx#surfing" class="nav">Surfing</a></li> <li><a href="http://www.southbeachbarbados.com/activities.aspx#water-sports" class="nav">Water Sports</a></li> </ul> </li> <li class="thingstodo submenulist"> <a href="http://www.southbeachbarbados.com/things-to-do" title="Things To Do" class="things nav">Things to Do</a><span class="nav-arrow icon-downArrow2"></span> <ul class="ttd dropnav" style="display:none"> <li><a href="http://www.southbeachbarbados.com/things-to-do/harrisons-cave.aspx" class="nav">Harrison's Cave</a></li> <li><a href="http://www.southbeachbarbados.com/things-to-do/catamaran-cruises.aspx" class="nav">Catamaran Cruises</a></li> <li><a href="http://www.southbeachbarbados.com/things-to-do/harbour-lights.aspx" class="nav">Harbour Lights</a></li> </ul> </li> <li class="events submenulist"> <a class="nav" title="Events" href="http://www.southbeachbarbados.com/event.aspx">Events</a><span class="nav-arrow icon-downArrow2"></span> <ul class="ttd dropnav" style="display:none"> <li><a href="http://www.southbeachbarbados.com/event.aspx#specialevents">Special Events</a></li> <li><a href="http://www.southbeachbarbados.com/event.aspx#eventsweddings">Weddings</a></li> <li><a href="http://www.southbeachbarbados.com/event.aspx#eventsvenues">Venues </a></li> <li><a href="http://www.southbeachbarbados.com/event.aspx#group-form">Request</a></li> </ul> </li> <li class="fmlyofr"><a href="http://www.southbeachbarbados.com/enhance-your-stay.aspx" title="Enhance Your Stay" class="nav">Enhance Your Stay</a></li> </ul> <div class="clear"></div> </nav> </div> </div> <div id="contentinfo" class="fixheader"> <div id="container"> <div id="content"> <div id="social-like-holder"> <div id="social-like-close" class="minus"></div> <div id="socialmediabuttons_wrap"> <ul class='socialmediabuttons'> <li> <div class="fb-like" data-width="120" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div> </li> <li> <div id="gplus"> <div class="g-plusone" data-size="medium"></div> </div> </li> </ul> </div> </div> <div id="header">Privacy Policy</div> <div id="breadcrumb"><a href="http://www.southbeachbarbados.com/">Home</a> &nbsp;>&nbsp; Privacy Policy</div> <div id="box"><div class="shadow-box"> <div id="gdpr-privacy"> <div class="box gdprtextalign"> <h1>South Beach Barbados Privacy Policy</h1> <h2>Our Core Beliefs Regarding User Privacy And Data Protection </h2> <ul> <li>User privacy and data protection are human rights </li> <li>We have a duty of care to the people within our data </li> <li>Data is a liability, it should only be collected and processed when absolutely necessary </li> <li>We loathe spam as much as you do! </li> <li>We will never sell, rent or otherwise distribute or make public your personal information</li> </ul> </div> <div class="box" id="legislation"> <h2>Relevant Legislation </h2> Along with our business and internal computer systems, this website is designed to comply with the following national and international legislation with regards to data protection and user privacy: <ul> <li> <a href="http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=uriserv:OJ.L_.2016.119.01.0001.01.ENG&toc=OJ:L:2016:119:TOC" target="_blank" rel="nofollow noopener noreferrer">EU General Data Protection Regulation 2018 (GDPR) </a></li> </ul> <div class="boxspacer"></div> <h2>Personal Information That This Website Collects And Why We Collect It</h2> This website collects and uses personal information for the following reasons: <div class="boxspacer"></div> <h3>Site visitation tracking </h3> Like most websites, this site uses Google Analytics (GA) to track user interaction. We use this data to determine the number of people using our site, to better understand how they find and use our web pages and to see their journey through the website. <div class="boxspacer"></div> Although GA records data such as your geographical location, device, internet browser and operating system, none of this information personally identifies you to us. GA also records your compute's IP address, which could be used to personally identify you, but Google does not grant us access to this. We consider Google to be a third party data processor (<a href="#thirdparty-data">see section below</a>). <div class="boxspacer"></div> GA makes use of cookies, details of which can be found on <a href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage" target="_blank" rel="nofollow noopener noreferrer"> Google's developer guides</a>. Our website uses the analytics.js implementation of GA. Disabling cookies on your internet browser will stop GA from tracking any part of your visit to pages within this website. <div class="boxspacer"></div> Disabling cookies on your internet browser will stop GA from tracking any part of your visit to pages within this website. <div class="boxspacer"></div> In addition to Google Analytics, this website may collect information (held in the public domain) attributed to the IP address of the computer or device that is being used to access it. <div class="boxspacer"></div> <h3>Blog</h3> Should you choose to add a comment to any posts that we have published on blog sites, the name and email address you enter with your comment will be saved to this website's database, along with your computer's IP address and the time and date that you submitted the comment. This information is only used to identify you as a contributor to the comment section of the respective blog post and is not passed on to any of the third party data processors detailed below. Only your name and email address that you supplied will be shown on the public-facing website. <div class="boxspacer"></div> Your comments and it's associated personal data will remain on this site until we see fit to either <div class="boxspacer"></div> <ul> <li>remove the comment or</li> <li>remove the blog post. Should you wish to have the comment and it's associated personal data deleted, please <a href="/cdn-cgi/l/email-protection#91e1e3f8e7f0f2e8d1fcf8fdf4e2e5fefff4f8ffe5f4e3fff4e5bff2fefcaee2e4f3fbf4f2e5acd3fdfef6b4a3a1e1fee2e5b4a3a1e3f4fcfee7f0fd">email us here</a> using the email address that you commented with. </li> </ul> <div class="boxspacer"></div> If you are under 16 years of age you MUST obtain parental consent before posting a comment on our blog. <div class="boxspacer"></div> NOTE: You should avoid entering personally identifiable information to the actual comment field of any blog post comments that you submit on this website. <div class="boxspacer"></div> <h3>Contact forms and email links</h3> Should you choose to contact us using the contact form on our Contact us page, none of the data that you supply will be stored by this website or passed to / be processed by any of the third party data processors defined in <a href="#thirdparty-data">section below</a>. Instead the data will be collated into an email and sent to us over the <a href="https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol" target="_blank" rel="nofollow">Simple Mail Transfer Protocol (SMTP)</a>. Our SMTP servers are protected by <a href="https://en.wikipedia.org/wiki/Transport_Layer_Security" rel="nofollow noopener noreferrer" target="_blank">TLS </a>(sometimes known as SSL) meaning that the email content is encrypted before being sent across the internet. The email content is then decrypted by our local computers and devices. <div class="boxspacer"></div> <h3>Email newsletter </h3> If you choose to join our email newsletter, the email address that you submit to us will be forwarded to <a href="https://www.benchmarkemail.com" target="_blank" rel="nofollow noopener noreferrer">Benchmark </a>who provide us with email marketing services. We consider Benchmark to be a third party data processor (<a href="#thirdparty-data">see section below</a>). The email address that you submit will not be stored within this website's own database or in any of our internal computer systems. <div class="boxspacer"></div> Your email address will remain within Benchmark's database for as long as we continue to use Benchmark's services for email marketing or until you specifically request removal from the list. You can do this by unsubscribing using the unsubscribe links contained in any email newsletters that we send you or by requesting removal via email. When requesting removal via email, please send your email to us using the email account that is subscribed to the mailing list. <div class="boxspacer"></div> If you are under 16 years of age you MUST obtain parental consent before joining email newsletter. </div> <div class="box2" id="thirdparty-data"> <h2>Third Party Data Processors</h2> We use a number of third parties to process personal data on our behalf. These third parties have been carefully chosen and all of them comply with the legislation set out in <a href="#legislation">section above</a>. <div class="clear"></div> <div class="column31"> <ul> <li>Google (<a href="https://www.google.com/intl/en-GB/policies/privacy/" rel="nofollow noopener noreferrer" target="_blank">Privacy policy</a>) </li> <li>Twitter (<a href="https://twitter.com/privacy" rel="nofollow noopener noreferrer" target="_blank">Privacy policy</a>)</li> </ul> </div> <div class="column32"> <ul> <li>Microsoft (<a href="https://privacy.microsoft.com/en-us/privacystatement" rel="nofollow noopener noreferrer" target="_blank">Privacy policy</a>) </li> <li>Facebook (<a href="https://www.facebook.com/about/privacy" rel="nofollow noopener noreferrer" target="_blank">Privacy policy</a>)</li> </ul> </div> <div class="column33"> <ul> <li>Benchmark (<a href="https://www.benchmarkemail.com/email-marketing/privacy-policy" rel="nofollow noopener noreferrer" target="_blank">Privacy policy</a>) </li> <li>Instagram (<a href="https://help.instagram.com/519522125107875?helpref=page_content" rel="nofollow noopener noreferrer" target="_blank">Privacy policy</a>)</li> </ul> </div> <div class="clear"></div> <div class="boxspacer"></div> <div id="paracookiepolicy"> <h2>Cookie Policy</h2> This policy covers the use of cookies and other technologies. The types of cookies we use fall into 3 categories: <ul> <li><h3>Essential cookies and similar technologies</h3> These are vital for the running of our services on our websites and apps. Without the use of these cookies parts of our websites would not function. For example, session cookies allows a navigation experience that is consistent and optimal to user's network speed and choice of device.</li> <li><h3>Analytics cookies and similar technologies</h3> These collect information about your use of our websites and apps, and enable us to improve the way it works. For example, analytics cookies show us which are the most frequently visited pages. They also help identify any difficulties you have accessing our services, so we can fix any problems. Additionally these cookies allow us to see overall patterns of usage at an aggregated level.</li> <li><h3>Tracking, advertising cookies and similar technologies</h3> We use these types of technologies to provide advertisements that are more relevant to your interests. This can be done by delivering online adverts based your previous web browsing activity. Cookies are placed on your browser which will remember the websites you have visited. Advertising based on what you have been looking at is then displayed to you when you visit websites who use the same advertising networks.</li> </ul> We may also use cookies and similar technologies to provide you with adverts based on your location, offers you click on, and other similar interactions with our websites and apps. </div> <div class="boxspacer"></div> <h2>Data Breaches </h2> We will report any unlawful data breach of this website's database or the database(s) of any of our third party data processors to any and all relevant persons and authorities within 72 hours of the breach if it is apparent that personal data stored in an identifiable manner has been stolen. <div class="boxspacer"></div> <h2>Changes To Our Privacy Policy </h2> This privacy policy may change from time to time to conform with legislation and/or industry developments. We will not explicitly inform our clients or website users of these changes. Instead, we recommend that you check this page occasionally for any policy changes. </div> <div id="gdprformsection" style="border:1px solid #cccccc;padding:10px;"> By entering a valid email address that you have access to, we will inform you any personal information we collect that is associated with that email address and how to manage it. <form style="margin:10px 0px 10px 0px;" onsubmit="return getGDRPdata();"> <label for="gdpremail">Enter Email Address:</label> <input type="email" required="" aria-required="true" value="" name="gdpremail" id="gdpremail" class="gdprform"> <input type="submit" value="Submit" id="gdprsubmit" class="gdprform"> </form> <div id="gdprmsg"></div> <script data-cfasync="false" src="/cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script><script language="javascript" type="application/javascript">

	var xmlHTTP_GDPR;
	
	function GetXmlHttpObject_GDPR()
  {
		var xmlHTTP_GDPR = null;
		try
		 { /* Firefox, Opera 8.0+, Safari */
		 xmlHTTP_GDPR= new XMLHttpRequest();
		 }
		catch (e)
		 {
			/* Internet Explorer */
		 try
			{ xmlHTTP_GDPR = new ActiveXObject("Msxml2.XMLHTTP");  }
		 catch (e)
			{ xmlHTTP_GDPR = new ActiveXObject("Microsoft.XMLHTTP");  }
		 }
		return xmlHTTP_GDPR;
  }
	
	function getGDRPdata() {

	  xmlHTTP_GDPR = GetXmlHttpObject_GDPR();
	     
		if (xmlHTTP_GDPR==null)
			{ 
			alert ("Your browser does not support AJAX!"); 
			return; 
			}
		
			var url = document.getElementById('hdnmilestoneSiteURL').value + "api/privacypolicy/checkuserdata";
			
			xmlHTTP_GDPR.open("POST",url,true);
			xmlHTTP_GDPR.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
			xmlHTTP_GDPR.send(JSON.stringify({ Email: document.getElementById('gdpremail').value }));

			xmlHTTP_GDPR.onreadystatechange = GDPR_Change;
			
			return false;
			
	}
	
	function GDPR_Change() {
	  if (xmlHTTP_GDPR.readyState == 4 && this.status == 200)
		{	
		
		
			var string = xmlHTTP_GDPR.responseText;
			
			if(string.indexOf('Not Found')>0)
			document.getElementById("gdprmsg").innerHTML = 'No user data found with requested email address.';
			else
			document.getElementById("gdprmsg").innerHTML = 'Email sent Successfully.';

		  
		}
	}

</script> </div> </div> </div> </div> </div> <div class="clear"> </div> </div> </div> <div id="note"> <footer class="footer-warp"> <div class="googleaddress"> <div class="google-vcard"> <div class="vcard-align"> </div> <span class="fn org title-f"> Contact </span> <span class="pipe mobilefooterpipeblock2">&nbsp; | &nbsp;</span> <span style="display: none;">Located at</span><span>South Beach Hotel</span> <div class="adr"> <span class="street-address">Rockley at Accra Beach</span>, <br> <span class="locality">Christ Church</span>, <span class="f-address"> <span class="region">Barbados</span>&nbsp;<span>BB15 139</span> </span> </div> <div class="geo-f"> </div> <div> <span class="ph-f">Phone: <span class="tel fontbold spbold"><a href="tel:246-435-8561" data-phonenumber="property-phone" target="_blank" rel="nofollow">246-435-8561</a></span><br> <span class="pipe mobilefooterpipeblock1">&nbsp; | &nbsp;</span> </span> <span class="ph-f">UK Toll Free: <span class="fontbold spbold">011 44 203-868-9916</span><span class="pipe mobilefooterpipeblock2">&nbsp; | &nbsp;</span> </span><br> <span class="pipe mobilefooterpipeblock1">&nbsp; | &nbsp;</span> <span class="ph-f">US Toll Free: <span class="fontbold spbold">1-888-964-0300</span><span class="pipe mobilefooterpipeblock2">&nbsp; | &nbsp;</span> </span><br> <span class="pipe mobilefooterpipeblock1">&nbsp; | &nbsp;</span> <span class="em-f">Email: <a href="/cdn-cgi/l/email-protection#413324322433372035282e2f32012e2224202f292e35242d326f2323" rel="nofollow"><span class="__cf_email__" data-cfemail="0775627462757166736e6869744768646266696f6873626b74296565">[email&#160;protected]</span></a><br> </span> </div> <span class="pipe mobilefooterpipeblock3 weblocate">&nbsp; | &nbsp;</span> <a href="http://www.southbeachbarbados.com/" class="url">http://www.southbeachbarbados.com/</a> <br> <div class="review-f"><span> <span class="fontbold">Rated:</span> 4.0 out of 5 Based on <a href="http://www.southbeachbarbados.com/guest-reviews.aspx"> 2 Reviews </a> </span> </div> </div> <div class="clear"></div> </div> <div class="footerLink"> <div class="social-media-wrap"> <div class="title-f st-f">Stay Connected</div> <div class="social-media"> <ul> <li> <a class="fb" target="_blank" href="https://www.facebook.com/SouthBeachHotelBarbados/?fref=ts" rel="nofollow"><span class="social-sprite-ot"></span><span class="icon-facebook4 icon-s"></span><span class="text-soc">Facebook</span> </a> </li> <li> <a class="tw" target="_blank" href="https://twitter.com/southbeachbdos" rel="nofollow"><span class="social-sprite-ot"></span><span class="icon-twitter4 icon-s"></span><span class="text-soc">Twitter</span> </a> </li> <li> <a class="ins" target="_blank" href="https://www.instagram.com/southbeachbarbados/" rel="nofollow"><span class="social-sprite-ot"></span><span class="icon-instagram icon-s"></span><span class="text-soc">Instagram</span> </a> </li> </ul> </div> <div class="clear"></div> </div> </div> <div class="ftr-rightcontent ftr-rightcontent3"> <div class="carouselitems"> <div class="title-f sub-f">Subscribe</div> <div class="ftr-txt">Enter your email below to receive our awesome hotel deals right in your inbox! </div> <a href="#" class="button signup-btn" id="quick-signup">Sign Up For Email Offers</a> </div> </div> <div id="popup-form"> <div class="formbox3"> <div class="popup-close icon-close" onclick="modelPopup.togglePopup(this,'popup-form')"></div> <div class="title-f sub-f">Sign Up For email offers</div> <div id="gdpr-emailoffer"> Your privacy and security are a top priority. We will not distribute or sell your personal information to any other entity. You may unsubscribe at any time. </div> <div class="boxspacer"></div> <form name="email_offer_form" method="post" action="http://www.southbeachbarbados.com/thanks.aspx" id="email_offer_form" enctype="multipart/form-data"> <div class="commentrow"> <div class="email-frmfieldset"> <div class="commentlable" style="display:none"> <label for="your-name">Name <span class="asterisk">*</span></label> </div> <div class="commentinput"> <input type="text" class="emailforminputbox" value="Enter Name *" name="FIRSTNAME" runat='server' id="your-name3"> </div> </div> <div class="email-frmfieldset"> <div class="commentlable" style="display:none"> <label for="email">Email <span class="asterisk">*</span></label> </div> <div class="commentinput"> <input type="text" runat='server' class="emailforminputbox" value="Enter Email *" name="EMAILREC" id="email3"> </div> </div> <div class="clear"></div> </div> <div class="clear"></div> <div class="email-frmfieldset"> <label class="emailtitle" for="imgcode-home" style="display:none">Enter Answer <span class="asterisk">*</span></label> <div class="commentinput"> <img alt="Captcha" id="imagename2" src="/js/captcha.ashx" width="120" height="32" title="Captcha"> <a class="fresh-icon" href="javascript:void(0);" onclick="RefreshImage('imagename2');"> <span class="icon-spinner6"></span></a> <input type="text" class="emailforminputbox answerinput" title="Security Code" id="imgcode-home" name="imgcode" value="Enter Answer *"> <span id="msgsecuritycode" style="display: none"></span> <div class="clear"></div> </div> </div> <div class="email-frmfieldset"> <input type="hidden" value="/email-thanks.aspx" id="urlredirect" name="urlredirect"> <input type="hidden" value="6291" id="dformId" name="dformId"> <input type="reset" name="resetform" id="resetform" value="" style="display:none"> <input type="submit" id="emailofferfrm2" value="Save" class="button" style="display:none;"> <input type="submit" style="display:none;" name="btnSubmit" id="btnSubmit"> <a class="button" href="javascript:{}" onkeypress="document.getElementById('email_offer_form').btnSubmit.click();" onclick="document.getElementById('email_offer_form').btnSubmit.click();"><span>Submit</span></a> </div> </form> <div class="clear"></div> </div> </div> <div class="popular-links"> <div class="title-f pl-f">Popular Links</div> <ul class="footer-ul"><li class="f-link1 carouselitems"> <a href="http://www.southbeachbarbados.com/suites" title="Accommodations" class="nav">Accommodations</a> </li><li class="f-link2 carouselitems"> <a href="http://www.southbeachbarbados.com/resort-features.aspx" title="Resort Features" class="nav">Resort Features</a> </li><li class="f-link3 carouselitems"> <a href="http://www.southbeachbarbados.com/dining.aspx" title="Dining" class="nav">Dining</a> </li><li class="f-link4 carouselitems"> <a href="http://www.southbeachbarbados.com/activities.aspx" title="Activities" class="nav">Activities</a> </li><li class="f-link5 carouselitems"> <a href="http://www.southbeachbarbados.com/things-to-do" title="Things To Do" class="nav">Things To Do</a> </li><li class="f-link6 carouselitems"> <a href="http://www.southbeachbarbados.com/event.aspx" title="Events" class="nav">Events</a> </li></ul> </div> <div class="information"><div class="title-f inf-f">Information</div> <ul class="footer-ul"><li id="info1" class="carouselitems"> <a href="http://www.southbeachbarbados.com/contact-us.aspx" title="Contact Us" class="nav">Contact Us</a> </li><li id="info2" class="carouselitems"> <a href="http://www.southbeachbarbados.com/about-us.aspx" title="About Us" class="nav">About Us</a> </li><li id="info3" class="carouselitems"> <a href="http://www.southbeachbarbados.com/map.aspx" title="E-Map" class="nav">E-Map</a> </li><li id="info4" class="carouselitems"> <a href="http://www.southbeachbarbados.com/photos.aspx" title="Photos & Videos" class="nav">Photos & Videos</a> </li><li id="info5" class="carouselitems"> <a href="http://www.southbeachbarbados.com/site-map.aspx" title="Site Map" class="nav">Site Map</a> </li><li id="info6" class="carouselitems"> <a href="http://www.southbeachbarbados.com/privacy-policy.aspx" title="Privacy Policy" class="nav">Privacy Policy</a> </li></ul> </div> <div class="clear"></div> </footer> </div> <div id="owner"><div class="owner-content owner-innercontent"> <div id="brand-logo" class="brand-logo-inner"> <div class="footer-logo"> <div class="tripadvisor"> <div class="ajax-tripadvisor"></div> </div> <div class="four-dimond"> <a class="pack-for-purpose-awards" href="http://www.packforapurpose.org/" target="_blank" rel="nofollow"><img src="http://www.southbeachbarbados.com/images_noindex/pack-for-purpose-awards.png" title="Pack for a Purpose" alt="Pack for a Purpose" height="32" width="200"></a></div> <div class="sister-properties"> <div class="footerlogo-title">Check Out our Sister Properties</div> <div class="ocean-two"><a href="http://www.oceantwobarbados.com/" target="_blank" rel="nofollow"><img width="200" height="72" title="Ocean Hotels &amp; Resorts" src="http://www.southbeachbarbados.com/resourcefiles/smallimages/ocean-two-resort-residences-logo.png" alt="Ocean Hotels &amp; Resorts"></a></div> <div class="sea-breeze"><a href="http://www.sea-breeze.com/" target="_blank" rel="nofollow"><img width="200" height="55" title="Sea Breeze Beach Resort Barbados" src="http://www.southbeachbarbados.com/resourcefiles/smallimages/sea-breeze-beach-resort-barbados-logo.png" alt="Sea Breeze Beach Resort Barbados"></a></div> </div> <div class="clear"></div> </div> </div> </div> </div> </div> <script data-cfasync="false" src="/cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script><script>
document.write(unescape("%3Cscript src='http://www.southbeachbarbados.com/dynamic/js/subnav.aspx' type='text/javascript'%3E%3C/script%3E"));</script> <!--[if lt IE 7]><script type="text/javascript" src="http://www.southbeachbarbados.com/js/DD_belatedPNG.aspx"></script> <script>
DD_belatedPNG.fix('*');
</script><![endif]--> <a href="http://www.southbeachbarbados.com/tracking/click-floatingbooknow.aspx" id="floatingbooknow" class="button">Book Now</a> <script>
 (function () {
  
  var params = {};

  /* Please do not modify the below code. */
  var cid = [];
  var paramsArr = [];
  var cidParams = [];
  var pl = document.createElement('script');
  var defaultParams = {"vid":"hot"};
  for(key in defaultParams) { params[key] = defaultParams[key]; };
  for(key in cidParams) { cid.push(params[cidParams[key]]); };
  params.cid = cid.join('|');
  for(key in params) { paramsArr.push(key + '=' + encodeURIComponent(params[key])) };
  pl.type = 'text/javascript';
  pl.async = true;
  pl.src = 'https://beacon.sojern.com/pixel/p/8760?f_v=v6_js&p_v=1&' + paramsArr.join('&');
  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(pl);
 })();
</script> <script src="https://static.triptease.io/paperboy/km48Nz9p2.js?hotelkey=dc4c651e94afc65371c025a651b2c3d1e27c81f9"></script> <dataset> <data> <siteid>809</siteid> <email><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="05607d646875696045607d64687569602b666a68">[email&#160;protected]</a></email> <values> <firstname>John</firstname> <lastname>Smith</lastname> <address>123 ABC</address> <address2>apt A</address2> <city>Myrtle Beach</city> <state>SC</state> <zip>29577</zip> <country>USA</country> <phone1>843-234-0986</phone1> <phone2></phone2> <fax></fax> <source>Website</source> </values> </data> </dataset> <input type='hidden' id='hdnparentpageid' value='0'><input type='hidden' id='hdnpageid' value='71772'><input type='hidden' id='hdntemplatename' value='subnav.aspx'><input type='hidden' id='hdnpagealias' value='privacy-policy'><input type='hidden' id='hdnclientaccess' value='1'><input type='hidden' id='hdndevicetype' value='desktop'><input type='hidden' id='hdntabletdetection' value='2'><input type='hidden' id='hdnmobiledetection' value='2'><input type='hidden' id='hdnismobile' value='0'><input type='hidden' id='hdnpagemoderationid' value='0'><input type='hidden' id='hdnclientmoderation' value=''><input type='hidden' id='hdndifferenttabletcontent' value='0'><input type='hidden' id='hdndifferentmobilecontent' value='0'><input type='hidden' id='hdnusertypeid' value=''><input type='hidden' id='hdnmilestoneSiteID' value='1494'><input type='hidden' id='hdnlanguagepages' value='{"languagepages":[{"en": "http://www.southbeachbarbados.com/privacy-policy.aspx"}]}'><input type='hidden' id='hdnmemberemail' value=''><input type='hidden' id='hdnmembername' value=''><input type='hidden' id='hdnexternalpage' value='0'><input type='hidden' id='hdnpagestatus' value='1'><script data-cfasync="false" src="/cdn-cgi/scripts/f2bf09f8/cloudflare-static/email-decode.min.js"></script><script id="msCookiePolicy">
var CookieBanner = null;
/* window.onload = function() { 
  "use strict"; */
  CookieBanner = {

		moveToTop_msCookieBanner : function ()
		{
				if (document.getElementById("msCookieBanner"))			
				if (window.getComputedStyle(document.getElementById("msCookieBanner"),null).getPropertyValue('position') == 'fixed')
				{
				var elems = document.body.getElementsByTagName("*");
				var len = elems.length;
			
				for (var i=0;i<len;i++) {
					
					var condition1 = false;
					var condition2 = false;
					
					if (window.getComputedStyle(elems[i],null).getPropertyValue('position') == 'fixed')
					{
						condition1 = true;
					} else condition1 = false;
					if (elems[i].className.indexOf('fixreservationbottom')>-1)
					{
						condition2 = true;
					} else condition2 = false;
					
				
					if ((condition1 == true || condition2 == true) && elems[i].id!="msCookieBanner") {
						
						elems[i].className += " pushItDown";
					} 
				
				}	
				}
		},
    hideCookieBanner: function() {

			var newClasss = "";
      var cname = "msCookieBanner";
      var cvalue = "accepted";
      var exdays = 365;
      var d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
      var expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

      var elem = document.getElementById('msCookieBanner');
      elem.parentNode.removeChild(elem);
			
			var elems = document.body.getElementsByTagName("*");
				var len = elems.length;
			
				for (var i=0;i<len;i++) {
				
					if (elems[i].className.indexOf('pushItDown')>-1) {
						newClasss = elems[i].className.replace('pushItDown', '');
						elems[i].className = newClasss;
					}
				
				}	
				
    },

    check_and_appendCookieBanner: function() {

      var userCookiePreference = "noanswer";

      var cname = "msCookieBanner";
      var name = cname + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          userCookiePreference = c.substring(name.length, c.length);
          i = ca.length + 1;
        }
      }

      if (userCookiePreference != "accepted") {

        var iDiv = document.createElement('div');
        iDiv.id = 'msCookieBanner';
        iDiv.className = 'msCookieBanner';
        iDiv.innerHTML = '<div class="left"> Our site uses cookies. By continuing to use our site you are agreeing to our <a href="/privacy-policy.aspx?#paracookiepolicy">cookie policy</a>.</div><div class="right"><button onclick="window.CookieBanner.hideCookieBanner();">Accept & Close</button></div><div style="clear:both"></div>';
        document.getElementsByTagName('body')[0].appendChild(iDiv);

        var styles = '#msCookieBanner { font-size:13px; font-family:inherit; width:100%; position:fixed;top:0px; z-index:1001}';
        styles += ' #msCookieBanner {background-color:#000000;color:#FFFFFF; border-top:2px solid #FFFFFF; border-bottom:2px solid #FFFFFF;}';
        styles += ' #msCookieBanner .left {width:75%;display:inline-block; text-align:center; padding:5px 0px 5px 0px;}';
        styles += ' #msCookieBanner .right {width:25%;display:inline-block;padding:5px 0px 5px 0px}';
        styles += ' #msCookieBanner .right button {border:none;padding:5px;background-color:#FFFFFF;color:#000000;border: solid 1px #fff;cursor: pointer;}';
        styles += ' #msCookieBanner .right button:focus, #msCookieBanner .right button:hover {background: #000;color: #fff;}';
        styles += ' #msCookieBanner a, #msCookieBanner a:link, #msCookieBanner a:visited {color:#fff; border-bottom:dashed 1px #fff; }';
        styles += ' #msCookieBanner a:hover, #msCookieBanner a:focus {color:#fff; border-bottom:solid 1px #fff; }';
        styles += ' .pushItDown {margin-top: 35px !important; }';				

        var css = document.createElement('style');
        css.type = 'text/css';

        if (css.styleSheet) css.styleSheet.cssText = styles;
        else css.appendChild(document.createTextNode(styles));

        document.getElementsByTagName("head")[0].appendChild(css);

      }
    }

  };

	setTimeout(function(){
  CookieBanner.check_and_appendCookieBanner();
	CookieBanner.moveToTop_msCookieBanner();
	},3000);
/*  return CookieBanner;

}; */
</script> <input type='hidden' id='hdnmilestoneSiteURL' value='http://www.southbeachbarbados.com/'><input id="hddevicetype" value="desktop" type="hidden"></body> </html>