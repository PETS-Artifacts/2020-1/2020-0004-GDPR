<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


  <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="/http://www.narutohq.com/favicon.ico" type="image/x-icon" />
  <title>Privacy Policy for Naruto HQ | Naruto HQ</title> 
  <meta name="description" content="Naruto and Naruto Shippuden anime and manga fan site, offering the latest news, information and multimedia about the series." />
  <link type="text/css" rel="stylesheet" media="all" href="/modules/node/node.css?G" />
<link type="text/css" rel="stylesheet" media="all" href="/modules/system/defaults.css?G" />
<link type="text/css" rel="stylesheet" media="all" href="/modules/system/system.css?G" />
<link type="text/css" rel="stylesheet" media="all" href="/modules/system/system-menus.css?G" />
<link type="text/css" rel="stylesheet" media="all" href="/modules/user/user.css?G" />
<link type="text/css" rel="stylesheet" media="all" href="/themes/nhq/style.css?G" />
  <script type="text/javascript" src="/misc/jquery.js?G"></script>
<script type="text/javascript" src="/misc/drupal.js?G"></script>
<script type="text/javascript" src="/themes/nhq/script.js?G"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, { "basePath": "/" });
//--><!]]>
</script>
  <!--[if IE 6]><link rel="stylesheet" href="/themes/nhq/style.ie6.css" type="text/css" /><![endif]-->  
  <!--[if IE 7]><link rel="stylesheet" href="/themes/nhq/style.ie7.css" type="text/css" media="screen" /><![endif]-->
  <script type="text/javascript"> </script>
  <style type="text/css" media="all">@import "/themes/nhq/roundedcorners/bubbles.css";</style>
<script type="text/javascript" src="/themes/nhq/roundedcorners/rounded_corners.inc.js"></script>
<script type="text/JavaScript">
  window.onload = function() {
    settings = {
      tl: { radius: 10 },
      tr: { radius: 10 },
      bl: { radius: 10 },
      br: { radius: 10 },
      antiAlias: true,
      autoPad: true
    }
    var myBoxObject = new curvyCorners(settings, "rounded");
    myBoxObject.applyCornersToAll();
  }
</script>

<script src="http://cdn.jquerytools.org/1.2.3/jquery.tools.min.js"></script>	
<link rel="stylesheet" type="text/css" href="http://www.narutohq.com/tabs/tabs.css" />

</head>

<body>

<script type="text/javascript">
window.google_analytics_uacct = "UA-86845-1";
</script>


    <div id="art-page-background-gradient"></div>
<div id="art-main">
<div class="art-sheet">
    <div class="art-sheet-tl"></div>
    <div class="art-sheet-tr"></div>
    <div class="art-sheet-bl"></div>
    <div class="art-sheet-br"></div>
    <div class="art-sheet-tc"></div>
    <div class="art-sheet-bc"></div>
    <div class="art-sheet-cl"></div>
    <div class="art-sheet-cr"></div>
    <div class="art-sheet-cc"></div>
    <div class="art-sheet-body">
<div class="art-header">
    <div class="art-header-png"></div>

</div>
<div class="art-nav">
    <div class="l"></div>
    <div class="r"></div>
    
<div id="art-menu-id"><ul class="art-menu"><li class="leaf first last"><a href="http://www.narutohq.com/" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Shippuden</span></a></li>
</ul></div>
</div>
<div class="art-content-layout">
    <div class="art-content-layout-row">
<div class="art-layout-cell art-content">              
<div id="node-85" class="node">
<div class="art-post">
    <div class="art-post-tl"></div>
    <div class="art-post-tr"></div>
    <div class="art-post-bl"></div>
    <div class="art-post-br"></div>
    <div class="art-post-tc"></div>
    <div class="art-post-bc"></div>
    <div class="art-post-cl"></div>
    <div class="art-post-cr"></div>
    <div class="art-post-cc"></div>
    <div class="art-post-body">
<div class="art-post-inner">
<h2 class="art-postheader"> Privacy Policy for Naruto HQ</h2>
<div class="art-postcontent">
    <!-- article-content -->
<div class="art-article"><p>If you require any more information or have any questions regarding our privacy policy, please feel free to contact us by email at contact [at] narutohq.com and we will try to get back to you as soon as possible.</p>
<p>At Naruto HQ, the privacy of our visitors is of extreme importance to us. This privacy policy page outlines the types of personal information is received and collected by Naruto HQ and how it is used. </p>
<p><strong>Log Files</strong><br />
Like many other Web sites, Naruto HQ makes use of log files. The information inside the log files includes internet protocol (IP) addresses, type of browser, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and number of clicks to analyze trends, administer the site, track user’s movement around the site, and gather demographic information. IP addresses, and other such information are not linked to any information that is personally identifiable. </p>
<p><strong>Cookies and Web Beacons</strong><br />
Where necessary, Naruto HQ uses cookies to store information about visitors preferences, record user-specific information on which pages the user access or visit, customize Web page content based on visitors browser type or other information that the visitor sends via their browser. </p>
<p><strong>DoubleClick DART Cookie</strong><br />
- Google, as a third party vendor, uses cookies to serve ads on Naruto HQ.<br />
- Google's use of the DART cookie enables it to serve ads to users based on their visit to Naruto HQ and other sites on the Internet.<br />
- Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at the following URL: <a href="http://www.google.com/policies/privacy/ads/" title="http://www.google.com/policies/privacy/ads/">http://www.google.com/policies/privacy/ads/</a>.</p>
<p>Some of our advertising partners may use cookies and web beacons on our site. Our advertising partners include:</p>
<p><strong>Google Adsense</strong><br />
These third-party ad servers or ad networks use technology to the advertisements and links that appear on Naruto HQ send directly to your browsers. They automatically receive your IP address when this occurs. Other technologies ( such as cookies, JavaScript, or Web Beacons ) may also be used by the third-party ad networks to measure the effectiveness of their advertisements and / or to personalize the advertising content that you see. </p>
<p>Naruto HQ has no access to or control over these cookies that are used by third-party advertisers. </p>
<p>You should consult the respective privacy policies of these third-party ad servers for more detailed information on their practices as well as for instructions about how to opt-out of certain practices. Naruto HQ's privacy policy does not apply to, and we cannot control the activities of, such other advertisers or web sites. </p>
<p>If you wish to disable cookies, you may do so through your individual browser options. More detailed information about cookie management with specific web browsers can be found at the browsers' respective websites. </p>
</div>
    <!-- /article-content -->
</div>
<div class="cleared"></div>

</div>

    </div>
</div>

</div></div>
<div class="art-layout-cell art-sidebar1"><div class="art-vmenublock">
    <div class="art-vmenublock-tl"></div>
    <div class="art-vmenublock-tr"></div>
    <div class="art-vmenublock-bl"></div>
    <div class="art-vmenublock-br"></div>
    <div class="art-vmenublock-tc"></div>
    <div class="art-vmenublock-bc"></div>
    <div class="art-vmenublock-cl"></div>
    <div class="art-vmenublock-cr"></div>
    <div class="art-vmenublock-cc"></div>
    <div class="art-vmenublock-body">
<div class="art-vmenublockheader">
     <div class="t">Naruto Information</div>
</div>
<div class="art-vmenublockcontent">
    <div class="art-vmenublockcontent-body">
<!-- block-content -->

<div id="art-vmenu-id"><ul class="art-vmenu"><li class="leaf first"><a href="/masashi-kishimoto.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Masashi Kishimoto</span></a></li>
<li class="expanded"><a href="/anime-guide.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Anime TV Guide</span></a><ul><li class="leaf first"><a href="/naruto-tv-schedule.php" title="">Naruto Anime TV Schedule</a></li>
<li class="leaf"><a href="/episodelist.php" title="">Naruto Episode List</a></li>
<li class="leaf"><a href="/shippuden-episode-list.php" title="Naruto Shippuden Episode List">Naruto Shippuden Episode List</a></li>
<li class="leaf"><a href="/naruto-shippuden-fillers.php" title="">Naruto Shippuden Filler List</a></li>
<li class="leaf"><a href="/voiceactors.php" title="">Naruto Voice Actors</a></li>
<li class="leaf last"><a href="/tvtokyo.php" title="">TV Tokyo</a></li>
</ul></li>
<li class="leaf"><a href="/advanced-bloodlines.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Bloodline Guide</span></a></li>
<li class="leaf"><a href="/character-biographies.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Character Guides</span></a></li>
<li class="leaf"><a href="/demon-guide.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Demons Guide</span></a></li>
<li class="leaf"><a href="/naruto-games.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Game Guide</span></a></li>
<li class="leaf"><a href="/jutsu-guide.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Jutsu Guide</span></a></li>
<li class="expanded"><a href="/general-guide.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto General Guide</span></a><ul><li class="leaf first"><a href="/chakra.php" title="">Naruto Chakra Guide </a></li>
<li class="leaf"><a href="/countries.php" title="Naruto Countries">Naruto Countries</a></li>
<li class="leaf"><a href="/food.php" title="">Naruto Food Guide </a></li>
<li class="leaf last"><a href="/quotes.php" title="">Naruto Quotes</a></li>
</ul></li>
<li class="leaf"><a href="/naruto-movies.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Movie Guide</span></a></li>
<li class="leaf"><a href="/lyrics.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Music Lyrics</span></a></li>
<li class="expanded last"><a href="/ninja-guide.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Ninja Guide</span></a><ul><li class="leaf first"><a href="/chuunin-exam.php" title="">Naruto Chuunin Exams </a></li>
<li class="leaf"><a href="/missionranks.php" title="">Naruto Mission Ranks</a></li>
<li class="leaf"><a href="/shinobi-ranks.php" title="">Naruto Shinobi Ranks</a></li>
<li class="leaf last"><a href="/weapons.php" title="">Naruto Weapons</a></li>
</ul></li>
</ul></div>

<!-- /block-content -->

    </div>
</div>

    </div>
</div>
<div class="art-vmenublock">
    <div class="art-vmenublock-tl"></div>
    <div class="art-vmenublock-tr"></div>
    <div class="art-vmenublock-bl"></div>
    <div class="art-vmenublock-br"></div>
    <div class="art-vmenublock-tc"></div>
    <div class="art-vmenublock-bc"></div>
    <div class="art-vmenublock-cl"></div>
    <div class="art-vmenublock-cr"></div>
    <div class="art-vmenublock-cc"></div>
    <div class="art-vmenublock-body">
<div class="art-vmenublockheader">
     <div class="t">Naruto Media</div>
</div>
<div class="art-vmenublockcontent">
    <div class="art-vmenublockcontent-body">
<!-- block-content -->

<div id="art-vmenu-id"><ul class="art-vmenu"><li class="leaf first"><a href="/flash-movies.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Flash Movies</span></a></li>
<li class="leaf"><a href="/narutofont.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Ninja Font </span></a></li>
<li class="leaf"><a href="/sprites.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Sprite Sheets</span></a></li>
<li class="leaf last"><a href="/smilies.php" title=""><span class="l"> </span><span class="r"> </span><span class="t">Naruto Emoticons &amp; Smilies</span></a></li>
</ul></div>

<!-- /block-content -->

    </div>
</div>

    </div>
</div>
<div class="art-block clear-block block block-block" id="block-block-3">
    <div class="art-block-tl"></div>
    <div class="art-block-tr"></div>
    <div class="art-block-bl"></div>
    <div class="art-block-br"></div>
    <div class="art-block-tc"></div>
    <div class="art-block-bc"></div>
    <div class="art-block-cl"></div>
    <div class="art-block-cr"></div>
    <div class="art-block-cc"></div>
    <div class="art-block-body">

	<div class="art-blockcontent content">
	    <div class="art-blockcontent-body">
	<!-- block-content -->
	
		<p><iframe src="http://www.facebook.com/plugins/likebox.php?id=127842690573692&amp;width=200&amp;connections=7&amp;stream=false&amp;header=true&amp;height=287" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:200px; height:287px;" allowTransparency="true"></iframe></p>

	<!-- /block-content -->
	
	    </div>
	</div>
	

    </div>
</div>
</div>
    </div>
</div>
<div class="cleared"></div>
<div class="art-footer">
    <div class="art-footer-inner">
                <div class="art-footer-text">
        <p><a href="http://www.narutohq.com/about">About NarutoHQ.com</a>&nbsp;|&nbsp;<a href="http://www.narutohq.com/privacy-policy.php">Privacy Policy</a><br />Copyright &copy; 2005-2015 NarutoHQ.com. Naruto was created by Masashi Kishimoto, and published by Shueisha Inc.<br>
NarutoHQ.com is not endorsed, associated or affiliated with Shueisha Inc. or Masashi Kishimoto, in any way.</p>        
<a href="http://www.minecraftguides.org/" target="_blank" title="Minecraft Guide">Minecraft Guides</a> 
| <a href="http://www.minecraftseeds.co/" target="_blank" title="Seeds For Minecraft">Minecraft Seeds</a>  
 </div>
    </div>
    <div class="art-footer-background"></div>
</div>

    </div>
</div>
<div class="cleared"></div>
<p class="art-page-footer"></p>

</div>


<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-86845-1";
urchinTracker();
</script>

</body>
</html>