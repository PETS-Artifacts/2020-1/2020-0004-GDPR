<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
   
     <title>Puppet Nightmares - Privacy Policy</title>
   </head>
 <body>
   <h2>Privacy Policy</h2>
     <h4>Contents</h4>
   <ol>
     <li><a href="#S1">Information we collect</a></li>
     <li><a href="#S2">Tracking Software</a></li>
     <li><a href="#S3">Data Collection Reminder</a></li>
     <li><a href="#S4">Legal Processes</a></li>
     <li><a href="#S5">Changing and Updating your information:</a></li>
     <li><a href="#S6">Sharing Information</a></li>
     <li><a href="#S7">Privacy Policy Updates</a></li>
     <li><a href="#S8">Effective Date of Policy</a></li>
     <li><a href="#S9">Minors</a></li>
   </ol>

<h4><a name="S1"> 1) Information We Collect: </a></h4>
<p>We collect information we need to better serve individuals that browse our website and play the game.
This includes users' ip addresses, configuration settings and logs of actions taken by  users on the server.
Any other information collected about the user will not be disclosed to 3rd party partners without 
their consent.</p>

<h4><a name="S2">2) Tracking Software: </a></h4>
<p> Beyond your activity on this site, we do not that track your browsing activities with cookies or software. 
We are not responsible for anything our 3rd party partners might install on a user's computer should 
he or she click their ads.</p>

<h4><a name="S3">3) Data Collection Reminder: </a></h4>
<p> We collect the following: </p>
<ul>
  <li>Logs of user actions on the server.</li>
  <li>User configuration settings.</li>
  <li>Traffic sources and conversions.</li>
  <li>Keywords used to find the site.</li>
  <li>IP addresses.</li>
</ul>

<h4><a name="S4">4) Legal Processes: </a></h4>
<p>In the event that we need to respond to any warrants, subpoenas or any other legal 
processes regarding a user, we cannot protect his or her personal data and we will hand it 
over to the proper authorities.</p>

<h4><a name="S5">5) Changing and Updating your information: </a></h4>
<p>From the user's "Player>Account" page, he or she can change or update any
personal data. In the event that you agree to any special promotions, the data will 
need to be validated by us after any submissions and changes.

<h4><a name="S6">6) Sharing Information: </a></h4>
<p>During the life of Puppet Nightmares (eg. www.puppetnightmares.com), there will be different 
promotional deals where the user can trade data about himself/herself for credit.
The user currently has a right to opt out of these offers.</p>

<h4><a name="S7">7) Privacy Policy Updates: </a></h4>
<p>Important changes to the policy will be notified to users via the news link on the World Map.  
Users are advised to keep themselves informed by frequently checking the news.  Updates may also be posted on the 
forum.

<h4><a name="S8">8) Effective Date of Policy: </a></h4>
<p>This policy has been in effect since December 2nd, 2008, and was last updated on June 19th, 2017.</p>

<h4><a name="S9">9) Minors: </a></h4>
<p>This website's content is intended for <b>adults</b> and we will not knowingly collect personal information 
from individuals under 18 years of age. If you are a parent or legal guardian of a child under age 
18 who you believe has submitted personal information to this site, please contact us <b>immediately</b> via
the <a href="../contact.php">Contact</a> page.</p> </body>
</html>


