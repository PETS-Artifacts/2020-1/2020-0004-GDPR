
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head><title>
	Privacy Policy
</title><link rel="icon" type="image/png" sizes="16x16" href="/images/icons/favicons/favicon-16x16.png"><link rel="icon" type="image/png" sizes="32x32" href="/images/icons/favicons/favicon-32x32.png"><link rel="icon" type="image/png" sizes="192x192" href="/images/icons/favicons/favicon-192x192.png"><link rel="shortcut icon" href="/images/icons/favicons/favicon.ico"><meta name="theme-color" content="#ffffff"><link rel="canonical" href="https://www.myregistry.com/Info/Privacy.aspx"><meta http-equiv="X-UA-Compatible" content="IE=edge" /><script type="text/javascript" src="/ScriptSet/PstJqueryScripts.js?version=201811021252018437"></script><script type="text/javascript" src="/ScriptSet/PstScripts.js?version=201811021252018437"></script><link href="/StyleSet/PstStyles.css?version=201811090023012526" rel="stylesheet" type="text/css" />
    <link href="Styles/terms.aspx.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/Styles/Buttons.css" />
    <script language="Javascript" type="text/javascript">
        function printWindow() {
            window.print();
        }
    </script>

<meta name="description" content="This Privacy Statement sets forth the information gathering, use, and dissemination practices of MyRegistry LLC (&quot;MyRegistry&quot;) in connection with the World Wide Web site located at www.myregistry.com (the “Site”). " /></head>
<body >
    <div id="PST_GlobalWrapper">
        <div id="PST_ContentWrapper">
            <div id="PST_TopWrapper">
                
<div id="PST_Header" class="header-visitor">
    <div class="header-top">
        <div class="menu-trigger" id="btnMenuMobile">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div class="box-left">
            <div class="content-logo pst_logo_mr">
                <a href="/">MyRegistry.com</a>
            </div>
        </div>
        <div class="box-middle">
            <div class="content-search">
                <label for="txtRegistrySearch" class="hide">Find a Registry</label>
                <input id="txtRegistrySearch" class="pst_inputtext white search" type="search" placeholder="FIND A REGISTRY">
                <a id="btnRegistrySearch"></a>
            </div>
        </div>
        <div class="box-right">
            <div class="content-action">
                <a class="action-item pst_button" href="/Signup.aspx" data-action="signup">Sign up</a>
                <a class="action-item pst_button light" href="/Login.aspx" data-action="login">Log in</a>
            </div>
        </div>
    </div>
    <div class="header-bottom">
        <nav>
            <div class="menu-container">
                <ul class="menu-root">
                    <li class="sub-menu-parent menu-search">
                        <a href="/find-a-registry.aspx">Find a Registry</a>
                    </li>
                    <li class="sub-menu-parent menu-wedding">
                        <a href="/wedding-registry.aspx">Wedding</a>
                        <ul class="sub-menu">
                            <li><a href="/wedding-registry/Signup.aspx">Create a Wedding Registry</a></li>
                            <li><a href="/free-wedding-website">Build a Custom Wedding Site</a></li>
                            <li><a href="/blog/category/Wedding">Wedding Blog</a></li>
                            <li><a href="/wedding-registry/ideas.aspx">Wedding Gift Idea Boards</a></li>
                            <li><a href="/WeddingGiftIdeas/">Popular Wedding Gifts</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu-parent menu-baby">
                        <a href="/baby-registry.aspx">Baby</a>
                        <ul class="sub-menu">
                            <li><a href="/baby-registry/Signup.aspx">Create a Baby Registry</a></li>
                            <li><a href="/blog/category/Baby">Baby Blog</a></li>
                            <li><a href="/baby-registry/ideas.aspx">Baby Gift Idea Boards</a></li>
                            <li><a href="/baby-registry-gifts-live-feed/">Popular Baby Gifts</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu-parent menu-wishlist">
                        <a href="/wish-list.aspx">Wish List</a>
                        <ul class="sub-menu">
                            <li><a href="/wish-list/Signup.aspx">Create a Wish List for any Occasion</a></li>
                            <li><a href="/Graduation-Wish-List/">Create a Graduation Wish List</a></li>
                            <li><a href="/kids-wish-list-registry.aspx">Create a Birthday Wish List</a></li>
                            <li><a href="/holiday-wish-list">Create a Holiday Wish List</a></li>
                            <li><a href="/housewarming-registry.aspx">Create a Housewarming Wish List</a></li>
                            <li><a href="/wish-list/ideas.aspx">Wish List Gift Idea Boards</a></li>
                            <li><a href="/WishlistGiftIdeas/">Popular Wish List Gifts</a></li>
                            <li><a href="/Info/fundraising.aspx">Create an Org<span class="no-mobile">anization</span>/Nonprofit Wish List</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu-parent menu-why">
                        <a href="/?param=video">Why MyRegistry?</a>
                        <ul class="sub-menu">
                            <li><a href="/?param=video" data-action="homevideo">See How it Works</a></li>
                            <li><a href="/Reasons-We-Are-The-Best-Place-To-Register-And-Top-Registry/">10 Reasons Why You’ll Love Us</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu-parent menu-software">
                        <a href="/gift-registry-software/">Gift Registry Solution</a>
                        <ul class="sub-menu">
                            <li class="header">
                                <a>MyRegistry for Commerce</a>
                            </li>
                            <li><a href="/gift-registry-software/">About our Gift Registry Software</a></li>
                            <li><a href="/Partner-Connection/">Partner Blog</a></li>
                            <li><a href="/merchants/login.aspx">Partner Login</a></li>

                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="header-chat">
        <div class="chat">
            
<script type="text/javascript">
    function openLivechat() {
        var chatIcon = $(".chatContainer .LPMcontainer a[data-lp-event='click']");
        if (chatIcon.length > 0) $(chatIcon)[0].click();
    }
</script>

<div id="LP_DIV_2018" class="chatContainer"></div>

        </div>
    </div>
</div>

                
            </div>
            <div id="PST_MainWrapper">
                <div id='PST_GlobalCenterBannerContainer'>
                    
                </div>
                <div id="PST_GlobalVideoBannerContainer">
                    
                </div>
                <div id="PST_GlobalLeftBanner">
                    
                    
                </div>
                
    <div class="forms_logo">
    </div>
    <div class="pst_container">
        <div class="main-header">
            <h1 class="pst_title">MyRegistry LLC privacy statement</h1>       
        </div>
        <div class="main-content">
            <div class="terms_content">
                <p>
                We at MyRegistry LLC (“<u>MyRegistry</u>,” “<u>we</u>,” “<u>us</u>,” or “<u>our</u>”) have created this privacy policy (this “<u>Privacy Policy</u>”) because we know that you care about how information you provide to us
                     is used and shared.  This Privacy Policy relates to the information collection and use practices of MyRegistry in connection with our Services, which are made available to you through
                     a variety of platforms, including, but not limited to, <a href="https://www.myregistry.com">www.myregistry.com</a> (the “<u>Website</u>”) and our mobile app, which is accessible through tablets, smart phones, connected televisions,
                     and other devices (the “<u>App</u>”). The Website and the App are collectively referred to as the “<u>Platform.</u>”
                </p>
                <h2 class="forms_header">
                    <u>Description of Users and Acceptance of Terms</u>
                </h2>
                <p>
                    We welcome many users to our Website and our App, and this Privacy Policy is intended to apply to all of them.
                </p>
                <p>
                    Users that visit our Website (“<u>Visitors</u>”) are only allowed to view publicly-available content. By visiting our Website, Visitors agree to our Privacy Policy and the accompanying 
                    Terms of Use.
                </p>
                <p>
                    Other users sign up to access and use our Services via the Platform such as by creating a gift registry for their special occasion (“<u>Members</u>”). We also welcome guests, who access,
                    view and/or purchase gifts from a Member’s registry (“<u>Guests</u>”).  By signing up, accessing, and/or using our Platform, each Member and Guest thereby agrees to our Privacy Policy
                    and the accompanying Terms of Use.
                </p>
                <p>
                    This Privacy policy also applies to merchants that sign up to access and use our Services via the Platform such as fulfilling the orders for gifts purchased by Guests from Members registry 
                    (“<u>Merchants</u>”). By signing up, accessing, and/or using the Platform, each Merchant hereby agrees to our Privacy Policy and the accompanying Merchant Agreement.
                </p>
                <p>
                    Capitalized terms not defined in this Privacy Policy shall have the meaning set forth in the Terms of Use (when such term concerns Visitors, Members or Guests) and the Merchant 
                    Agreement (when such term concerns Merchants).
                </p>
                <h2 class="forms_header">
                    The Information We Collect and/or Receive
                </h2>
                <p>  
                    In the course of operating the Platform and providing the Services, MyRegistry will collect (and/or receive) the following types of information. You authorize us to collect, receive,
                        use and share such information as set forth in this Privacy Policy. 
                </p>
                <ol type="1">
                    <li>
                        <h2 class="forms_header" style="font-weight:300;">
                            <u>Personal Information.</u>
                        </h2>
                        <p>
                            When you sign up to become a Member, you will be required to provide us with personal information about yourself, such as your first name, last name, and e-mail address.
                              If you sign-up through Facebook, or any other social media website, you are authorizing MyRegistry to collect, store, and use, in accordance with this Privacy Policy, any
                             and all information that you agreed that Facebook, Inc., or such other social media company would provide to MyRegistry through their application programming interface (“<u>API</u>”).
                             Such information may include, without limitation, your first and last name, social media user name, social media profile picture, unique social media identifier and access token,
                             and e-mail address.
                        </p>
                        <p>
                            If you contact us via the Contact Us page, you will need to provide your name and e-mail address.  
                            Our Third Party Partners also provide us with personal information that you have provided to such Third Party Partners.  You authorize us to collect, store, and use, in accordance
                             with this Privacy Policy, any and all such personal information.  This Privacy Policy governs our use of this personal information; it does not govern, and we have no control over 
                            and will have not responsibility or liability for collection or use of your personal information by the Third Party Partners.
                        </p>
                        <p>
                            All information that we receive under this section is collectively called “<u>Personal Information</u>.”  We do not collect any Personal Information from Visitors, Members, Guests or 
                            Merchants unless they provide such information voluntarily.
                        </p>
                    </li>
                    <li>
                        <h2 class="forms_header" style="font-weight:300;">
                            <u>Billing Information.</u>
                        </h2>
                        <p>
                            In order to process any cash gift transaction through the Platform (each, a “<u>Transaction</u>”), Guests may be required to provide certain billing information, such as information
                             related to their PayPal account, debit card number, credit card number, expiration date, bank account information, billing address, activation code, and similar information 
                            (collectively, the “<u>Billing Information</u>”).  Such Billing Information will be collected and processed by our Third Party Payment Processor pursuant to the terms and conditions of 
                            their privacy policies and terms of use, and we do not obtain access to any Billing Information in connection with such Transactions.
                        </p>
                    </li>
                         <li>
                        <h2 class="forms_header" style="font-weight:300;">
                            <u>User Generated Content.</u>
                        </h2>
                        <p>
                            User Generated Content (UGC) is any content that is made publicly available by users of a system. 
                            The systems most commonly used for posting UGC are social media websites, such as Facebook, Twitter, 
                            Instagram, etc. If you decide to post anything on a social media platform accompanied with the hashtag #ShareMyRegistry, 
                            you are thereby granting MyRegistry.com permission to copy, display, distribute, perform, publish, modify, and/or
                            translate such media in any form, as long as we act in accordance with this Privacy Policy and the relevant laws
                            regarding data privacy. We may also request permission to use UGC that is not accompanied with the #ShareMyRegistry
                            hashtag, if we believe it could serve a legitimate business purpose. If permission is granted, you will be affording 
                            us the same rights to the content as if you had originally posted it with our hashtag. Generally, we will use UGC for 
                            advertising and marketing purposes. We will not attempt to claim ownership to any of the UGC we use, but it must be 
                            understood that we will also never compensate anyone for usage of their publicly accessible data. This right to use
                            your UGC never expires, and we may also encourage third parties to use or archive such content as well.
                        </p>
                        <p>
                            Due to the fact that UGC is available to the public, please be very attentive to the information you post on any social media platform.
                            Any post that is discriminatory based on race, gender, sexual orientation, national origin, or a mental or physical disability is 
                            a violation of others' rights, and you may face legal consequences as a result. Your UGC must not be defamatory, sexually explicit,
                            or used for political gain, and the content you post must not result in a breach of contract with us or a third party.
                        </p>
                    </li>
                    <li>
                        <h2 class="forms_header" style="font-weight:300;">
                            Other Information.
                        </h2>
                        <p>
                            In addition to the Personal Information, we may collect or receive additional information (collectively, the “<u>Other Information</u>”).  Such Other Information may include:
                        </p>
                        <ol type="a">
                            <li>
                                <p>
                                    <u>From Your Activity</u>. &emsp;Information that we automatically collect when you use the Platform, such as the device from which you access the Platform and/or the Services,
                                     your IP addresses, browser type and language, referring and exit pages and URLs, date and time, amount of time spent on particular pages, what sections of the Platform you visit,
                                     and similar information concerning your use of the Platform and/or the Services.
                                </p>
                            </li>
                            <li>
                                <p>
                                    <u>From Cookies</u>. &emsp;Information that we collect using “cookie” technology.  Cookies are small packets of data that a website stores on your computer’s hard drive so that 
                                    your computer will “remember” information about your visit.  We use cookies to help us collect Other Information and to enhance your experience using the Platform.  If you do not
                                     want us to deploy cookies in your browser, you can opt out by setting your browser to reject cookies or to notify you when a website tries to put a cookie in your browser 
                                    software.  If you choose to disable cookies in your browser, you can still use the Platform, although your ability to use some of the features may be affected.
                                </p>
                            </li>
                            <li>
                                <p>
                                    <u>Third-Party Analytics</u>. &emsp;We use third-party analytics services (such as Google Analytics) to evaluate your use of the Platform, compile reports on activity, collect demographic data, analyze performance 
                                    metrics, and collect and evaluate other information relating to the Platform and Internet usage.  These third parties use cookies and other technologies to help analyze and provide us the data.  
                                     By accessing and using the Platform, you consent to the processing of data about you by these analytics providers in the manner and for the purposes set out in this Privacy Policy.
                                    For more information on these third parties, including how to opt out from certain data collection, please visit the sites below.  Please be advised that if you opt out of any service, you may not be able to use the full functionality of the Platform.
                                </p>
                            </li>
                        </ol>
                        <p>
                            For Google Analytics, please visit <a href="https://www.google.com/analytics" target="_blank">https://www.google.com/analytics</a>
                        </p>
                    </li>
                </ol>
                    <h2 class="forms_header">
                    <u>Expectations of Third-Party Partners</u>
                </h2>
                <p>
                   MyRegistry.com expects all partners and any third parties we conduct business with to comply with relevant data protection and 
                    privacy laws at all times. We take every commercially reasonable step to ensure consistent compliance on our end. 
                    Transparency is becoming more of a focus for lawmakers when drafting new data privacy laws, so we have made it a priority 
                    to ensure all users of our site have the ability to find out how their data is being used, for what purpose, and for how long.
                    We expect the same from all of our partners and any third parties, and will do our best to ensure that we do not conduct business 
                    with any individual person or entity that is not compliant with all relevant laws. If any user wishes to receive 
                    information pertaining to the use or collection of their personal data at any time, we expect all of our partners to be able to 
                    provide that information as necessary. If there is a breach of contract due to the failure of a third party to produce information, 
                    an injunction may be sought against that third party to suspend their business operations until they become compliant, and monetary
                    damages may be sought for the actual breach.
                </p>
                <h2 class="forms_header">
                    <u>The Information Collected by or Through Third-Party Advertising Companies</u>
                </h2>
                <p>
                    You authorize us to share Other Information about your activity on the Platform with third parties for the purpose of tailoring, analyzing, managing, reporting, and optimizing
                        advertising you see on the Platform, and elsewhere.  These third parties may use cookies, pixel tags (also called web beacons or clear gifs), and/or other technologies to collect such Other 
                    Information for such purposes.  Pixel tags enable us, and these third-party advertisers, to recognize a browser’s cookie when a browser visits the site on which the pixel tag is located in
                        order to learn which advertisement brings a user to a given site. You may opt-out of the tailoring of advertising based on information we collect.  To learn more about the use of this
                        information, or to choose not to have this information used by our providers or third party advertising partners by opting out, please visit the Network Advertising Initiative by clicking 
                    <a href="http://www.networkadvertising.org/managing/opt_out.asp" target="_blank">http://www.networkadvertising.org/managing/opt_out.asp</a>
                </p>
                <h2 class="forms_header">
                    <u>Information Collected through the Platform That IS NOT Covered by this Privacy Policy</u>
                </h2>
                <p> 
                    Members may post information using blogs, forums and other similar features available through the Platform. NONE of the information you provide using these features is protected by 
                    this Privacy Policy. THIS PRIVACY POLICY DOES NOT APPLY TO ANY INFORMATION (INCLUDING PERSONAL INFORMATION) THAT YOU PROVIDE USING BLOGS, FORUMS OR OTHER SIMILAR
                    FEATURES AVAILABLE THROUGH THE PLATFORM. ALL SUCH INFORMATION IS PUBLIC INFORMATION AND MAY BE SEEN, COLLECTED OR USED BY ANY USER OF THE PLATFORM.
                </p>
                <h2 class="forms_header">
                    <u>How We Use and Share the Information</u>
                </h2>
                <p>
                    You authorize us to use the Personal Information, and the Other Information (collectively, the “Information”) to provide and improve the Platform; to solicit your feedback; and to
                    inform you about our products and services and those of our promotional partners.     
                </p>
                <p>
                    You also authorize us to use and/or share Information as described below.
                </p>
                <ul type="disc">
                    <li>
                        <p>
                            <u>Agents, Providers and Related Third Parties.</u> We may engage other companies and individuals to perform certain business-related functions on our behalf.  Examples may include 
                            providing technical assistance, order fulfillment, customer service, and marketing assistance.  These other companies will have access to the Information only as necessary to
                            perform their functions and to the extent permitted by law. We may also share your Information with any of our parent companies, subsidiaries, or other companies under common
                            control with us.
                        </p>
                    </li>
                    <li>
                        <p>
                            <u>Aggregated Information.</u> In an ongoing effort to better understand our Visitors, Members, Guests and Merchants, we may analyze Personal Information with Other Information in
                            anonymized and aggregate form in order to operate, maintain, manage, and improve the Platform.  This aggregate information does not identify you personally.  We may share this 
                            aggregate data with our affiliates, agents, business and promotional partners, and other third parties.  We may also disclose aggregated user statistics in order to describe the
                            Platform to current and prospective business partners and to other third parties for other lawful purposes.
                        </p>
                    </li>
                    <li>
                        <p>
                            <u>Business Transfers.</u>	As we develop our businesses, we might sell or buy businesses or assets.  In the event of a corporate sale, merger, reorganization, sale of assets,
                            dissolution, or similar event, the Information may be part of the transferred assets.
                        </p>
                    </li>
                    <li>
                        <p>
                            <u>Legal Requirements.</u>	We may disclose your Information if required to do so by law, or in the good faith belief that such action or disclosure is necessary or appropriate to: 
                            (i) operate the Platform, (ii) comply with any legal obligation, report unlawful activity, cooperate with law enforcement, or protect against legal liability, (iii) protect and
                            defend our rights, property, personnel, suppliers, sponsors, agents or licensors, or (iv) protect the personal safety of vendors, users of the Platform or the public. 
                        </p>
                    </li>
                    <li>
                        <p>
                            <u>Third Party Partners.</u> We may disclose your Personal Information to our Third Party Partners to whom you provided your information as a part of the sign-up process for the
                            Platform. Such Third Party Partners and the company who operates the Third Party Partner website may use your Personal Information for their own marketing and business purposes.
                        </p>
                    </li>
                    <li>
                        <p>
                            <u>Third Parties Marketing Companies.</u> 	With your permission, we may disclose your Personal Information to third party marketers (who may combine such Personal Information with
                            their own records, and records available from other sources), for their own direct marketing purposes. If you opt-in to this information sharing and later want us to stop sharing
                            your Personal Information with such third party marketers, please send an email with your name and address to <a href="mailto:info@myregistry.com" class="forms_link">info@myregistry.com</a>.
                        </p>
                    </li>
                </ul>
                <h2 class="forms_header">
                    <u>Accessing and Modifying Information and Communication Preferences</u>
                </h2>
                <p>
                    Members and Merchants may access, remove, review, and/or make changes to their Personal Information by e-mailing us at <a href="mailto:info@myregistry.com" class="forms_link">info@myregistry.com</a>.  In addition, you may manage your receipt of marketing and 
                    non-transactional communications by clicking on the “unsubscribe” link located on the bottom of any MyRegistry marketing e-mail.  We will use commercially reasonable efforts to process such 
                    requests in a timely manner.  You should be aware, however, that it is not always possible to completely remove or modify information in our subscription databases.  Members cannot opt out of receiving
                        transactional e-mails related to their account with MyRegistry.
                </p>
                <h2 class="forms_header">
                    <u>How We Protect the Information</u>
                </h2>
                <p>
                    We take commercially reasonable steps to protect the Information from loss, misuse, and unauthorized access, disclosure, alteration, or destruction.  Please understand, however, that no security system is
                        impenetrable.  We cannot guarantee the security of our databases or the databases of the third parties with which we may share such Information, nor can we guarantee that the Information you supply
                        will not be intercepted while being transmitted over the Internet.  In particular, e-mail sent to us may not be secure, and you should therefore take special care in deciding what information you send to us via e-mail.                      
                </p>
                <h2 class="forms_header">
                    <u>External Websites</u>
                </h2>
                <p> 
                    The Platform may contain links to third-party websites.  MyRegistry has no control over the privacy practices or the content of these websites.  As such, we are not responsible for the content or the privacy policies of those third-party websites.
                        You should check the applicable third-party privacy policy and terms of use when visiting any other websites.
                </p>
                <h2 class="forms_header">
                    <u>Children</u>
                </h2>
                <p> 
                        We do not knowingly collect Personal Information from children under the age of 13 through the Platform.  If you are under 13, please do not give us any Personal Information. 
                        We encourage parents and legal guardians to monitor their children’s Internet usage and to help enforce our Privacy Policy by instructing their children never to provide Personal
                        Information without their permission.  If you have reason to believe that a child under the age of 13 has provided Personal Information to us, please contact us, and we will endeavor to delete that information from our databases.
                </p>
                <h2 class="forms_header">
                    <u>Important Notice to Non-U.S. Residents</u>
                </h2>
                <p>
                    The Platform and the Services are operated in the United States.  If you are located outside of the United States, please be aware that any information you provide to us 
                    will be transferred to the United States.  By visiting the Website and/or accessing and/or using the Platform and/or the Services and/or providing us with any information, you 
                    consent to this transfer.
                </p>
                <h2 class="forms_header">
                    <u>California Residents</u>
                </h2>
                <p>
                    Under California Civil Code Section 1798.83, California residents who have an established business relationship with MyRegistry may choose to opt out of our sharing your Personal Information with 
                    third parties for direct marketing purposes.  If you are a California resident and (1) you wish to opt out; or (2) you wish to request certain information regarding our disclosure of your Personal Information to 
                    third parties for the direct marketing purposes, please send an e-mail to <a href="mailto:info@myregistry.com" class="forms_link">info@myregistry.com</a> with “Privacy Policy” in the subject line or write to us at:
                </p>
                <p>
                    MyRegistry, LLC<br/>
                    2050 Center Ave, Suite 100,<br/>
                    Fort Lee, NJ 07024<br/>
                </p>
                <p>
                    In addition, MyRegistry does not monitor, recognize, or honor any opt-out or do not track mechanisms, including general web browser “Do Not Track” settings and/or signals.
                </p>


                <h2 class="forms_header">
                    <u>For Residents of the European Union</u>
                </h2>
                <p></p>
                <h2 class="forms_header">
                    <u>European Privacy Rights</u>
                </h2>
                <p>
                    For those within the European Economic Area (EEA), the General Data Protection Regulation (GDPR) that went into effect on
                    May 25, 2018 has afforded you certain rights to your personal data. These rights are as follows: (a) the right to access 
                    any personal information being held about you, (b) the right to have any personal information being held about you
                    permanently deleted (provided that the information is not required to comply with a legal obligation or to exercise 
                    a legal claim), (c) the right to rectify any inaccurate personal information without undue delay, (d) the right to 
                    restrict processing of your personal information (provided that processing is not required to comply with a legal 
                    obligation or exercise a legal claim), (e) the right to request transfer of your personal information directly to a 
                    third party if technically feasible, and (f) the right to object to processing of any personal data at any time. 
               </p>
                <h2 class="forms_header">
                    <u>Right to Object to Direct Marketing</u>
                </h2>
                <p>
                    All users can object to the processing of personal information for the purposes of direct marketing at any time, 
                    free of charge.
               </p>
                <p> 
                    If you feel that we are not adhering to the GDPR or our Privacy Policy at any time, you have the right to file 
                    a complaint with an EU Data Protection Authority.
                </p>
                <h2 class="forms_header">
                    <u>Use of Collected Information</u>
                </h2>
                <p>
                   MyRegistry.com will only use the information collected from and about you on the following lawful grounds:
               </p>
                <ol type="a">
                            <li>
                                <p>
                                    Use is necessary to perform a contract we have entered into or are about to enter into with you;
                                </p>
                            </li>
                            <li>
                                <p> Use is necessary for our legitimate business interests;</p>
                            </li>
                            <li>
                                <p> Compliance with a legal or regulatory obligation is required; or </p>
                            </li>
                             <li>
                                <p>Where consent has been provided (which can be withdrawn at any time) </p>
                            </li>
                 </ol>
                <p>If none of these conditions will be satisfied by the use of your personal information, then such information will be permanently deleted from our system accordingly.</p>
                <p>In addition to the personal information you provide to us, we may also use publicly available information as well as information gathered from you by third parties to improve your shopping experience, inform you about new products or promotions, or for any other legitimate business purpose.</p>
                
                <h2 class="forms_header">
                    <u>Right to Erasure</u>
                </h2>
                <p>All users of MyRegistry.com have the right to request that any and all Personally Identifiable Information (PII) associated with their account be erased. Once a data subject access request (DSAR) is filed and customer identity is verified, the Company must respond to and fulfill such request within 30 days. When an account is “erased,” all PII associated with the account will be permanently deleted, leaving the existence of an anonymous account solely for the purpose of maintaining proper transactional records. The right to erasure strictly pertains to PII, and it must be explicitly understood that transactional history will not be deleted from the Company’s records for any reason. The right to erasure exists to protect users of websites from having their PII stored, transferred, or otherwise utilized by another entity in any manner without proper consent. It will not extend so far as to limit the financial responsibilities and legal obligations that MyRegistry.com has to its customers, employees, partners, and the authorities.</p>
                
                <h2 class="forms_header">
                    <u>Third-Party Partners</u>
                </h2>
                <p>When MyRegistry.com members sign up through one of our software partners, we begin sharing customer data with that partner. While we are directly responsible for our handling of your information, users must be aware that our partners also store customer information for their own business purposes. Therefore, when a member desires to invoke their right to erasure, the member must send deletion requests to any partner websites they signed up through as well as to MyRegistry.com to have the totality of their data erased.</p>
                <p>Members or guests of MyRegistry.com who elect to have their PII deleted from our system may do so by contacting <a href="mailto:GDPRCompliance@myregistry.com" class="forms_link">GDPRCompliance@myregistry.com</a>. </p>

                <h2 class="forms_header">
                    <u>Right to Access</u>
                </h2>
                <p>All users of MyRegistry.com have the undeniable right to ask about and obtain a complete and accurate report on the data being stored about them. If you would like to request a report on the data we are storing about you, email us at <a href="mailto:GDPRCompliance@myregistry.com" class="forms_link">GDPRCompliance@myregistry.com</a>.</p>

                <h2 class="forms_header">
                    <u>Transmitting PII + Responding to DSARs</u>
                </h2>
                <p>It is against our policy to send PII without proper safeguards. When a user requests a report on the data being stored about them, certain guidelines must be followed:</p>
                <ol type="1">
                    <li>A DSAR (Data Subject Access Request) must be sent by the user whose information is being requested to <a href="mailto:GDPRCompliance@myregistry.com" class="forms_link">GDPRCompliance@myregistry.com</a>.</li>
                    <li>The user’s identity must be verified by the submission of a photo ID that clearly shows the user’s legal name. (This is so we can match the name on the photo ID with the name on the MyRegistry.com account.)</li>
                    <li>MyRegistry.com will send the user an encrypted, password-protected email containing the PII requested.</li>
                    <li>MyRegistry.com will send the user a separate email containing the password that unlocks the encrypted PII in the email.</li>
                </ol>

                <h2 class="forms_header">
                    <u>Option to Password-Protect Your Account</u>
                </h2>
                <p>
                    If you create a wedding registry with us, you consent to us posting the names of both yourself and your partner on 
                    our website. This information may unwittingly reveal your sexual orientation to website visitors. 
                    If you wish to limit access to this information, you have the option to password-protect your account so that 
                    only individuals with your password will be able to view your wedding registry. You can find this option under 
                    the "Visitor setup" section in our "Settings" menu.  </p>

                <h2 class="forms_header">
                    <u>Changes to This Privacy Policy</u>
                </h2>
                <p>
                    This Privacy Policy is effective as of the date stated at the top of this Privacy Policy.  We may change this Privacy Policy from time to time. Any such changes will be posted on the 
                    Platform. By accessing the Website and/or using the Platform after we make any such changes to this Privacy Policy, you are deemed to have accepted such changes.  Please be aware that, to the 
                    extent permitted by applicable law, our use of the Information is governed by the Privacy Policy in effect at the time we collect the Information.  Please refer back to this Privacy Policy on a regular basis. 
                </p>
                <h2 class="forms_header">
                    <u>How to Contact Us</u>
                </h2>
                <p>
                    If you have questions about this Privacy Policy, please contact MyRegistry via e-mail at <a href="mailto:customercare@myregistry.com" class="forms_link">customercare@myregistry.com</a> with “Privacy Policy” in the subject line. 
                </p>
                <div class="forms_end">
                    <div style="display:inline-block">
                        This Privacy Statement is effective October 8, 2018.<br />
                        <strong>Last Updated</strong>: 10/08/2018</div>                
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="TosPage" value="1" />

                <div id="PST_GlobalRightBanner">
                    
                    
                </div>
            </div>
        </div>
        <div id="PST_BottomWrapper">
            
<div id="PST_Footer">
    <div class="footer-container">
        <div class="box-left">
            <div class="content-logo pst_logo_mr">
                <a href="/">MyRegistry.com</a>
                
            </div>
            <div class="content-tagline">All Stores, One Registry<span class="reg">&reg;</span></div>
        </div>
        <div class="box-right">
            <div class="content-menu">
                <div class="menu-group">
                    <div class="group-title">Company Info</div>
                    <div class="group-items">
                        <a href="/Info/AboutUs.aspx">About Us</a>
                        <a target="_blank" href="/blog/"><span>Blog</span></a>
                        <a href="/Info/ContactUs.aspx">Contact Us</a>
                        <a href="/Info/press.aspx">News and Press Releases</a>
                        <a href="/Info/terms.aspx">Terms and Conditions</a>
                        <a href="/Info/Privacy.aspx">Privacy Statement</a>
                        <a href="/Info/Careers.aspx">Careers</a>
                    </div>
                </div>
                <div class="menu-group">
                    <div class="group-title">For Members</div>
                    <div class="group-items">
                        <a href="/Info/faq.aspx?help=1">Customer Care / FAQs</a>
                        <a href="/Info/SmartPhoneApps/">Our Mobile Apps</a>
                        <a href="/find-a-registry.aspx">Find A Registry</a>
                        <a href="/Info/RegistryMembers.aspx">Members Directory</a>
                        <a href="/Info/CashGifts.aspx">Cash Gift Service</a>
                        <a href="/Info/storeswelike.aspx">Prescreened Retailers</a>
                        <a href="/Members/ShareRegistry/">Display Your Registry</a>
                        
                        
                    </div>
                </div>
                <div class="menu-group">
                    <div class="group-title">For Partners</div>
                    <div class="group-items">
                        <a href="/Merchants/AdvertisewithUs.aspx">Advertise with Us</a>
                        <a href="/Partner-Connection/">Partner Blog</a>
                        <a href="/gift-registry-software/">Gift Registry Software</a>
                        <a href="/Merchants/login.aspx">Partner Login</a>
                        <a  rel="nofollow" href="https://www.shareasale.com/shareasale.cfm?merchantID=38014" target="_blank">Affiliate Program</a>
                    </div>
                </div>
                <div class="menu-group">
                    <div class="group-title">Nonprofits/ Organizations</div>
                    <div class="group-items">
                        <a href="/Info/fundraising.aspx">Fundraising</a>
                        <a href="/Info/OrganizationsMKT.aspx">Cash Gift Service</a>
                        <a href="/Info/storeswelikeOrg.aspx">Prescreened Retailers</a>
                        <a href="/Info/faq.aspx?categoryname=Organization%2fnonprofit&amp;catid=26">FAQs</a>                        
                    </div>
                </div>
            </div>
            <div class="content-social">
                <div class="social-group">
                    <div class="group-title">Connect with Us</div>
                    <div class="group-items">
                        <a class="icon-facebook-holder" href="https://www.facebook.com/myregistryllc" target="_blank"><span class="pst_icon1 icon-facebook"></span></a>
                        <a class="icon-twitter-holder" href="https://twitter.com/myregistry" target="_blank"><span class="pst_icon1 icon-twitter"></span></a>
                        <a class="icon-instagram-holder" href="https://www.instagram.com/MyRegistry" target="_blank"><span class="pst_icon1 icon-instagram"></span></a>
                        <a class="icon-pinterest-holder" href="https://www.pinterest.com/MyRegistryPins/" target="_blank"><span class="pst_icon1 icon-pinterest"></span></a>
                        <a class="icon-facebook-googleplus" href="https://plus.google.com/+MyRegistry" target="_blank"><span class="pst_icon1 icon-googleplus"></span></a>
                    </div>
                </div>
                <div class="social-group deals">
                    <div class="group-title">Offers</div>
                    <div class="group-items ">
                        <a class="icon-heart-holder" href="/Special-Wedding-Offers"><span class="pst_icon1 icon-heart green"></span></a>
                        <a class="icon-duck-holder" href="/Special-baby-Offers" ><span class="pst_icon1 icon-duck green"></span></a>
                        <a class="icon-gift-holder" href="/Offers/MyOffers.aspx?regType=wishlist" ><span class="pst_icon1 icon-gift green"></span></a>

                    </div>
                </div>
            </div>
            <div class="content-copyright">
                &copy;
        2018
        All rights reserved - MyRegistry LLC
        &nbsp;&mdash;&nbsp;<a href="/SiteMap/" target="_blank">Site Map</a>
            </div>
        </div>
    </div>
</div>

        </div>
    </div>
    <div id="hiddenFields">
        <input id="RegistryType" type="hidden" value="" />
        <input id="IsHideAds" type="hidden" value="False" />
        <input id="IsNewUser" type="hidden" value="False" />
        <input id="TopMemberNav" type="hidden" value="undefined" />
    </div>
    <div id="mrExternalScripts">
        

<input id="panelManagerHidden" type="hidden" value="" />
<script type="text/javascript">
    function PanelManager() {
        this.IsNewUser = $("#IsNewUser").val() || null;
        this.CurrentPanel = null;
        this.ShowPanel = function () {
            this.CurrentPanel = $("#panelManagerHidden").val();

            

            var checkBeforeOpenPanel = null;
            if (this.CurrentPanel == "" || this.CurrentPanel == null || this.CurrentPanel == "null" || this.IsNewUser == "True") {
                // do not show panel
            } else {
                var panel = JSON.parse(this.CurrentPanel);
                
                if (checkBeforeOpenPanel !=null && !checkBeforeOpenPanel()) return; // Skip panel if check is NOK
                PopupManager.ShowPopupPanel(panel.Id, panel.Path, panel.Params, null, null, null);
                
            }
        };
    }
    var panelManager = new PanelManager();
    panelManager.ShowPanel();
</script>

        <div id="fb-root"></div>
        


<!-- Quantcast Tag -->
<script type="text/javascript">
var _qevents = _qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push({
qacct:"p-6bNL-aPgbDPBU"
});
</script>

<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-6bNL-aPgbDPBU.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!-- End Quantcast tag -->


        
<script type="text/javascript">
    var _gaq = _gaq || [];
    var GoogleTracking = {
        account: 'UA-23309797-1',
        affiliateAccount: null,
        domainName: 'myregistry.com',
        isEnabled: true,
        isAdBlocked: false,
        isDoNotTrackPage: false,
        Initialize: function () {
            _gaq.push(['_setAccount', this.account]);
            if (GoogleTracking.affiliateAccount) _gaq.push(['affiliate._setAccount', this.affiliateAccount]);
            _gaq.push(['_setDomainName', this.domainName]);
            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js'; // /ga.js or /u/ga_debug.js
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

            // Track Page View 
            GoogleTracking.TrackPageView();
        },
        TrackPageView: function () {

            try {
                var callback = function () {
                    var ad = document.createElement('ins');
                    var isAdBlockEnabled = false;

                    // Ad Blocker detection bait
                    ad.className = 'AdSense';
                    ad.style.display = 'block';
                    ad.style.position = 'absolute';
                    ad.style.top = '-1px';
                    ad.style.height = '1px';
                    document.body.appendChild(ad);
                    isAdBlockEnabled = !ad.clientHeight;
                    document.body.removeChild(ad);

                    if (isAdBlockEnabled) {
                        GoogleTracking.isAdBlocked = true;
                        GoogleTracking.trackCustom(1, "AdBlocked", "1");
                    }
                    // Page view
                    if (!GoogleTracking.isDoNotTrackPage) {
                        _gaq.push(['_trackPageview']);
                        if (GoogleTracking.affiliateAccount) _gaq.push(['affiliate._trackPageview']);
                    }
                };

                if (document.readyState != 'loading') {
                    callback();
                } else {
                    document.addEventListener('DOMContentLoaded', callback);
                }
            }
            catch(err) {
                // Page view anyway
                if (!GoogleTracking.isDoNotTrackPage) {
                    _gaq.push(['_trackPageview']);
                    if (GoogleTracking.affiliateAccount) _gaq.push(['affiliate._trackPageview']);
                }
            }
        },
        trackCustom: function (index, name, value) {
            if (this.isEnabled) {
                _gaq.push(['_setCustomVar', index, name, value]);
            }
        },
        trackEvent: function (category, action, label, value) {
        
            if (this.isEnabled) {
                _gaq.push(['_trackEvent', category, action, label, value]);
            }
        }
    };
    GoogleTracking.Initialize();
</script>

        

<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
    _atrk_opts = { atrk_acct: "ggrge1aUY9008q", domain: "myregistry.com", dynamic: true };
    (function () { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=ggrge1aUY9008q" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->  
        
<!--Google Publisher Tag BEGIN-->
<script type='text/javascript'>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    googletag.mrenv = '';
    (function () {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
            '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
    })();
</script>
<!-- Google Publisher Tag END-->

        





<!-- BEGIN LivePerson Monitor. -->
<script type="text/javascript">window.lpTag=window.lpTag||{},"undefined"==typeof window.lpTag._tagCount?(window.lpTag={site:'60303108'||"",section:lpTag.section||"",tagletSection:lpTag.tagletSection||null,autoStart:lpTag.autoStart!==!1,ovr:lpTag.ovr||{},_v:"1.8.0",_tagCount:1,protocol:"https:",events:{bind:function(t,e,i){lpTag.defer(function(){lpTag.events.bind(t,e,i)},0)},trigger:function(t,e,i){lpTag.defer(function(){lpTag.events.trigger(t,e,i)},1)}},defer:function(t,e){0==e?(this._defB=this._defB||[],this._defB.push(t)):1==e?(this._defT=this._defT||[],this._defT.push(t)):(this._defL=this._defL||[],this._defL.push(t))},load:function(t,e,i){var n=this;setTimeout(function(){n._load(t,e,i)},0)},_load:function(t,e,i){var n=t;t||(n=this.protocol+"//"+(this.ovr&&this.ovr.domain?this.ovr.domain:"lptag.liveperson.net")+"/tag/tag.js?site="+this.site);var a=document.createElement("script");a.setAttribute("charset",e?e:"UTF-8"),i&&a.setAttribute("id",i),a.setAttribute("src",n),document.getElementsByTagName("head").item(0).appendChild(a)},init:function(){this._timing=this._timing||{},this._timing.start=(new Date).getTime();var t=this;window.attachEvent?window.attachEvent("onload",function(){t._domReady("domReady")}):(window.addEventListener("DOMContentLoaded",function(){t._domReady("contReady")},!1),window.addEventListener("load",function(){t._domReady("domReady")},!1)),"undefined"==typeof window._lptStop&&this.load()},start:function(){this.autoStart=!0},_domReady:function(t){this.isDom||(this.isDom=!0,this.events.trigger("LPT","DOM_READY",{t:t})),this._timing[t]=(new Date).getTime()},vars:lpTag.vars||[],dbs:lpTag.dbs||[],ctn:lpTag.ctn||[],sdes:lpTag.sdes||[],hooks:lpTag.hooks||[],ev:lpTag.ev||[]},lpTag.init()):window.lpTag._tagCount+=1;</script>
<!-- END LivePerson Monitor. -->
    </div>
</body>
</html>
