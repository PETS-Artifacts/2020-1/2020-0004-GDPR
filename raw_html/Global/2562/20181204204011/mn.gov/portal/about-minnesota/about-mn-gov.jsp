


<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--[if lt IE 8]>    <html class="no-js lt-ie12 lt-ie11 lt-ie10 lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie12 lt-ie11 lt-ie10 lt-ie9 ie8" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js lt-ie12 lt-ie11 lt-ie10 ie9" lang="en"> <![endif]-->
<!--[if IE 10]>    <html class="no-js lt-ie12 lt-ie11 ie10" lang="en"> <![endif]-->
<!--[if IE 11]>    <html class="no-js lt-ie12 ie11" lang="en"> <![endif]-->
<!--[if gt IE 11]><!-->
<html class="gt-ie11 no-js" lang="en">
<!--<![endif]-->

<head>
<meta http-equiv="content-language" content="eng">
    <meta name="PageID" content="tcm:1077-218777-64" />
        <title>about-mn-gov / mn.gov // Minnesota's State Portal</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="dc.title" content="about-mn-gov">
    <meta name="dc.description" content="">
    <meta name="dc.subject" content="Privacy Statement , Privacy Statement , ">
    <meta name="dc.created" content="2016-04-12T20:48:42.2600000Z">
    <meta name="dc.modified" content="2018-10-23T15:27:51.8916374Z">
    <meta name="dc.creator" content="">
    <meta name="dc.contributor" content="">
    <meta name="dc.publisher" content="">
    <meta name="dc.audience" content="">
    <meta name="dc.identifer" content="">
    <meta name="dc.format" content="">
    <meta name="dc.type" content="">
    <meta name="dc.source" content="">
    <meta name="dc.relation" content=", ">
    <meta name="dc.language" content="eng, ">
    <meta name="dc.coverage" content=", ">
    <meta name="dc.rights" content="">
    <meta name="dc.subjectControlled" content="">
    <meta name="dc.dateOther" content="">
<meta property="og:site_name" content="mn.gov // Minnesota's State Portal"/>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/portal/assets/layout.favicon_tcm1077-1028.png"  rel="shortcut icon">
<!--[if lt IE 9]>
	<script language="javascript" type="text/javascript" src="/portal/js/frameworks/frameworks.htmlshiv.js"></script>
	<script language="javascript" type="text/javascript" src="/portal/js/frameworks/frameworks.respond.js"></script>
<![endif]-->
<!--[if IE]>
	<script language="javascript" type="text/javascript" src="/portal/js/frameworks/frameworks.css3-mediaqueries.js"></script>
<![endif]-->

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800,300' rel='stylesheet' type='text/css' />


		<!-- Generic LESS -->
		<link rel="stylesheet" type="text/css" media="" href="/portal/css/core.css"/>

<!-- core compiled jquery and layout files //-->
<script src="/portal/js/frameworks/jquery/frameworks.jquery.js"></script>    <!-- content-specific jquery //-->

    


	<script type="text/javascript">
/*<![CDATA[*/
(function() {
var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;
sz.src = '//siteimproveanalytics.com/js/siteanalyze_1535995.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);
})();
/*]]>*/
</script>
</head>
<body>

<a href="#content" id="skip-to-content" tabindex="0" class="sr-only" onclick="$('#content').focus();">skip to content<br /><span class="fa fa-arrow-down"></span></a>
<a href="/portal/" id="contextURL" class="hidden">Site URL</a>
<header id="header" role="heading">
  		<a href="/portal/" title="Navigate back to the home page"><img src="/portal/assets/header-mn-logo_tcm1077-352403.png" id="header-logo" alt="minnesota logo"></a>
        <button class="btn btn-md search-toggle"><span id="search-toggle-icon" class="fa fa-search"></span><span class="sr-only">use this button to toggle the search field</span></button>

        <div id="header-search" tabindex="0" class="passive" aria-hidden="true">

                <form id="header-search-form" method="get" action="/portal/search/" class="margin-bottom">
                    
                    <div class="searchgroup">				
                        <label for="search-input" class="sr-only">Search:</label>
                        <div class="input-group input-group-md">
                            <input tabindex="0" id="search-input" name="query" type="text" class="form-control" placeholder="Search">
                            <span class="input-group-btn">
                                <button tabindex="0" type="submit" class="btn btn-md header-search-submit"><span class="fa fa-search"></span><span class="sr-only">submit</span></button>
                            </span>
                        </div>
                    </div>
                    
                </form>	
            </div>
            <script>
            
            $(document).ready(function(event) {
                $('button.search-toggle').on('click', function(event) {
                   var currStyle = $('#header-search').css('display');
                   switch(currStyle) {
                        case "none": 
                            $('#header-search').fadeIn(200);
                            $('#search-input').focus();
                            $('#header-search').attr('aria-hidden', 'false');
                            $('span#search-toggle-icon').removeClass('fa-search');
                            $('span#search-toggle-icon').addClass('fa-times-circle');
                        break;
                        case "block": 
                            $('#header-search').fadeOut(200);
                            $('.button-search-toggle').focus();
                            $('#header-search').attr('aria-hidden', 'true');
                            $('span#search-toggle-icon').removeClass('fa-times-circle');
                            $('span#search-toggle-icon').addClass('fa-search');
                        break;
                   }
                });
            });
            </script>
</header>
<!-- can't start at the root — need to skip to the next set of structure groups as the top level parent links //-->

 
	<div class="sr-only" id="primaryNavigationLabel">Primary navigation</div>
  	<nav role="navigation" id="top_nav" class="top" aria-label="primaryNavigationLabel">
        <div class="container">
            <ul id='top_nav_menu'>    <li class=""><a href='/portal/residents/index.jsp' target="_self">Residents</a>
            <ul class="">
        <li class=""><a href='/portal/residents/financial.jsp' target="_self">Consumer & Financial</a></li>
        <li class=""><a href='/portal/residents/health.jsp' target="_self">Health</a></li>
        <li class=""><a href='/portal/residents/legal.jsp' target="_self">Legal</a></li>
        <li class=""><a href='/portal/residents/natural-resources.jsp' target="_self">Natural Resources</a></li>
        <li class=""><a href='/portal/residents/public-safety.jsp' target="_self">Public Safety </a></li>
        <li class=""><a href='/portal/residents/social-services.jsp' target="_self">Social Services</a></li>
        <li class=""><a href='/portal/residents/taxes.jsp' target="_self">Taxes</a></li>
        <li class=""><a href='/portal/residents/transportation.jsp' target="_self">Transportation</a></li>
        <li class=""><a href='/portal/residents/volunteer.jsp' target="_self">Volunteering</a></li>
        <li class=""><a href='/portal/residents/voting.jsp' target="_self">Voting & Elections</a></li>
            </ul>
    </li>
    <li class=""><a href='/portal/business/index.jsp' target="_self">Business</a>
    </li>
    <li class=""><a href='/portal/government/index.jsp' target="_self">Government</a>
            <ul class="">
    <li class=""><a href='/portal/government/federal/index.jsp' target="_self">Federal</a>
            <ul class="">
        <li class=""><a href='/portal/government/federal/minnesota-delegation.jsp' target="_self">Minnesota Members of Congress</a></li>
            </ul>
    </li>
    <li class=""><a href='/portal/government/legal/index.jsp' target="_self">Legal</a>
    </li>
    <li class=""><a href='/portal/government/local/index.jsp' target="_self">Local</a>
            <ul class="">
    <li class=""><a href='/portal/government/local/associations/index.jsp' target="_self">Associations</a>
    </li>
    <li class=""><a href='/portal/government/local/cities/index.jsp' target="_self">Cities</a>
    </li>
    <li class=""><a href='/portal/government/local/counties/index.jsp' target="_self">Counties</a>
    </li>
    <li class=""><a href='/portal/government/local/special/index.jsp' target="_self">Special Districts & Regional Government</a>
    </li>
    <li class=""><a href='/portal/government/local/townships/index.jsp' target="_self">Townships</a>
    </li>
            </ul>
    </li>
    <li class=""><a href='/portal/government/state/index.jsp' target="_self">State</a>
            <ul class="">
    <li class=""><a href='/portal/government/state/agencies-boards-commissions/index.jsp' target="_self">Agencies, Boards, Commissions</a>
    </li>
    <li class=""><a href='/portal/government/state/state-supported/index.jsp' target="_self">State Supported Institutions</a>
    </li>
            </ul>
    </li>
    <li class=""><a href='/portal/government/tribal/index.jsp' target="_self">Tribal</a>
            <ul class="">
    <li class=""><a href='/portal/government/tribal/mn-indian-tribes/index.jsp' target="_self">Minnesota Indian Tribes</a>
            <ul class="">
        <li class="hidden"><a href='/portal/government/tribal/mn-indian-tribes/federally-recognized.jsp' target="_self">Minnesota Federally Recognized Indian Tribes</a></li>
            </ul>
    </li>
    <li class=""><a href='/portal/government/tribal/resources/index.jsp' target="_self">Other Resources</a>
    </li>
            </ul>
    </li>
    <li class=""><a href='/portal/government/voting-elections/index.jsp' target="_self">Voting & Elections</a>
    </li>
            </ul>
    </li>
    <li class=""><a href='/portal/employment/index.jsp' target="_self">Employment</a>
    </li>
    <li class=""><a href='/portal/education/index.jsp' target="_self">Education</a>
    </li>
    <li class=""><a href='/portal/travel/index.jsp' target="_self">Travel</a>
    </li>
    <li class="hidden"><a href='/portal/about-minnesota/index.jsp' target="_self">About Minnesota</a>
            <ul class="">
    <li class=""><a href='/portal/about-minnesota/accessibility-issues/index.jsp' target="_self">Accessibility Issues</a>
            <ul class="">
        <li class="hidden"><a href='/portal/about-minnesota/accessibility-issues/thankyou.jsp' target="_self">Thank you</a></li>
            </ul>
    </li>
        <li class=""><a href='/portal/about-minnesota/about-mn-gov.jsp' target="_self">About this website</a></li>
            </ul>
    </li>
    <li class="hidden"><a href='/portal/buffer-law/index.jsp' target="_self">Buffer Law</a>
            <ul class="">
    <li class=""><a href='/portal/buffer-law/land/index.jsp' target="_self">Does this apply to my land?</a>
    </li>
    <li class=""><a href='/portal/buffer-law/plant/index.jsp' target="_self">What can I plant?</a>
    </li>
    <li class=""><a href='/portal/buffer-law/support/index.jsp' target="_self">Where can I get support?</a>
    </li>
    <li class=""><a href='/portal/buffer-law/more-time/index.jsp' target="_self">I need more time</a>
    </li>
    <li class=""><a href='/portal/buffer-law/practices/index.jsp' target="_self">Alternative Practices</a>
            <ul class="">
        <li class=""><a href='/portal/buffer-law/practices/water-quality-cert.jsp' target="_self"> Water Quality Certification</a></li>
        <li class=""><a href='/portal/buffer-law/practices/nrcs-filter.jsp' target="_self">NRCS Filter Strip</a></li>
        <li class=""><a href='/portal/buffer-law/practices/grassed-waterway.jsp' target="_self">Grassed Waterway</a></li>
        <li class=""><a href='/portal/buffer-law/practices/negative-slope.jsp' target="_self">Ditches – Negative Slope</a></li>
        <li class=""><a href='/portal/buffer-law/practices/flat-land.jsp' target="_self">Ditches – Flat Land</a></li>
        <li class=""><a href='/portal/buffer-law/practices/altered-waters.jsp' target="_self">Altered Public Waters</a></li>
        <li class=""><a href='/portal/buffer-law/practices/tillage-cover.jsp' target="_self">Tillage & Cover Crops</a></li>
            </ul>
    </li>
    <li class=""><a href='/portal/buffer-law/map/index.jsp' target="_self">Buffer Maps</a>
            <ul class="">
        <li class=""><a href='/portal/buffer-law/map/compliance-map.jsp' target="_self">Buffer Compliance Map</a></li>
            </ul>
    </li>
            </ul>
    </li>
    <li class="hidden"><a href='/portal/brand/index.jsp' target="_self">State Brand</a>
            <ul class="">
    <li class=""><a href='/portal/brand/faqs/index.jsp' target="_self">FAQs</a>
    </li>
    <li class=""><a href='/portal/brand/style-guide/index.jsp' target="_self">Style Guide</a>
            <ul class="">
    <li class=""><a href='/portal/brand/style-guide/logo/index.jsp' target="_self">Logos</a>
    </li>
    <li class=""><a href='/portal/brand/style-guide/colors/index.jsp' target="_self">Colors</a>
    </li>
            </ul>
    </li>
            </ul>
    </li>
    <li class="hidden"><a href='/portal/connect/index.jsp' target="_self">State Mobile Sites & Apps</a>
    </li>
    <li class="hidden"><a href='/portal/natural-resources/index.jsp' target="_self">Natural Resources</a>
            <ul class="hidden">
    <li class="hidden"><a href='/portal/natural-resources/buffer-law/index.jsp' target="_self">- Buffer Law</a>
    </li>
            </ul>
    </li>
    <li class="hidden"><a href='/portal/guest-wireless/index.jsp' target="_self">guest-wireless</a>
            <ul class="hidden">
        <li class="hidden"><a href='/portal/guest-wireless/guest-login.jsp' target="_self">Guest Wireless</a></li>
            </ul>
    </li>
    <li class="hidden"><a href='/portal/search/index.jsp' target="_self">search</a>
            <ul class="hidden">
        <li class="hidden"><a href='/portal/search/json.jsp' target="_self">JSON Search</a></li>
        <li class="hidden"><a href='/portal/search/xml.jsp' target="_self">XML Search</a></li>
            </ul>
    </li>
</ul>
        </div>
    </nav><!-- end navigation //-->

<!-- meta redirect //-->


	<!-- skip to content jump //-->
	<div id="content"></div>
	<!-- jumbotron //-->


	<!-- breadcrumb //-->
	<div class="container">
	
		<ol class="breadcrumb"></ol>
	
	</div>


    <div class="content white-back padding" tabindex="0">
        <div class="container">

			<!-- jumbo page introductions //-->



  			<div class="row">
  				
  				
  				<div class="col-md-9 col-md-push-3 search-index">

					<!--  page introduction //-->
						<div class="content search-index">
							<!-- Error rendering region tag: no XPM component context publication configured -->
<!-- Error rendering region: Some invalid content type titles found for region --> 	
								<div class="margin-bottom">
        <h1>
            
                
                    About this Website
                
            
        </h1>
        
        
            
                <p><a href="#Privacy%20Statement">Privacy Statement</a> <a href="#Disclaimer">Disclaimer</a> <a href="#Security%20Statement">Security Statement</a> <a href="#Linking%20Policy">Linking Policy</a> <a href="#Accessibility">Accessibility</a></p>
<h2>Privacy Statement:</h2>
<h3>Privacy Notice Information</h3>
<p>This statement addresses collection, use and security of and access to information that may be obtained through use of the Minnesota.gov portal. This notice covers the following topics:</p>
<p>What is Minnesota.gov
<br />
Information Collected and How it is Used
<br />
Personal Information and Choice
<br />
Public Disclosure
<br />
Access and Correction of Personal Information
<br />
Cookies
<br />
Security, Intrusion, Detection Language
<br />
Disclaimer</p>
<h3>What is the Minnesota.gov Portal</h3>
<p>MN.IT Services is the developer and manager of The Minnesota.gov portal. Minnesota.gov is the electronic entry point to Minnesota State government information and services. It is the gateway for providing direct government-to-citizen public service provision and interaction through the use of information technology. The Minnesota.gov portal is dedicated to protecting your privacy on-line. The following privacy statement outlines how individual information will be handled by Minnesota.gov while using our services.</p>
<p>Protection of individual privacy is a primary concern, and is governed by <strong><a href="http://www.revisor.leg.state.mn.us/stats/13/" shape="rect">Minnesota Statutes Chapter 13, GOVERNMENT DATA PRACTICES</a>.</strong> Minnesota.gov has created this privacy statement in order to demonstrate our firm commitment to privacy. The following discloses our information gathering and dissemination practices for this site; <a href="http://mn.gov/">http://mn.gov/</a> .</p>
<h3>Information Collected and Stored Automatically</h3>
<p>For site management functions, information is collected for analysis and statistical purposes. This information is not reported or used in any manner that would reveal personally identifiable information, and will not be released to any outside parties unless legally required to do so in connection with law enforcement investigations or other legal proceedings.</p>
<p>We use Log analysis tools to create summary statistics, which are used for purposes such as assessing what information is of most interest, determining technical design specifications, and identifying system performance or problem areas. Our website logs do not collect personally identifiable information, and we make no attempt to link them with the individuals who browse the Minnesota.gov portal. When you visit the Minnesota.gov portal and browse or download information, we automatically collect and store the following information about your visit:</p>
<ol>
<li>
<p>The Internet Protocol Address and domain name used but not the email address. The Internet Protocol address is a numerical identifier assigned either to your Internet service provider or directly to your computer. We use the Internet Protocol Address to direct Internet traffic to you;</p>
</li>
<li>
<p>The type of browser and operating system you used;</p>
</li>
<li>
<p>The date and time you visited this site;</p>
</li>
<li>
<p>The web pages or services you accessed at this site; and</p>
</li>
<li>
<p>The website you visited prior to coming to this website.</p>
</li>
</ol>
<p><br />
If during your visit to the Minnesota.gov portal you participate in a survey, fill out a web form, or send an email, the following additional information will be collected:</p>
<ol>
<li>
<p>The email address and contents of the email; and</p>
</li>
<li>
<p>Information volunteered to complete a web form or in response to a survey.
<br /></p>
</li>
</ol>
<p>The information collected is not limited to text characters and may include audio, video, and graphic information formats you send us. The information is retained in accordance with <strong><a href="http://www.revisor.leg.state.mn.us/stats/138/17.html" shape="rect">Minnesota Statutes Chapter 138.17, subd. 1</a> ,</strong> and <strong><a href="http://www.revisor.leg.state.mn.us/stats/15/17.html" shape="rect">Minnesota Statutes Chapter 15.17</a></strong> ; destruction, preservation, reproduction of records, prima facie evidence.</p>
<p>If you send us an email with a question or comment that contains personally identifiable information, or fill out a form that emails us this information, we will only use the personally identifiable information to respond to your request and analyze trends. This may be to respond to you, to address issues you identify, or to further improve our website. We may redirect your message to another government agency that is in a better position to answer your question. Survey information is used for the purpose designated.</p>
<h3>Personal Information and Choice</h3>
<p>Personal information is information about a natural person that is readily identifiable to that specific individual. Personal information includes such things as an individual's name, address, and phone number. A domain name or Internet Protocol address is not considered personal information.</p>
<p>We collect no personal information about you unless you voluntarily participate in an activity that asks for information (i.e. sending an email, filling out a web form, or participating in a survey). If you choose not to participate in these activities, your choice will in no way affect your ability to use any other feature of the Minnesota.gov portal.</p>
<p>If personal information is requested on the website or volunteered by the user, State law and the federal Privacy Act of 1974 may protect it. However, this information is a public record once you provide it, and may be subject to public inspection and copying if not protected by Federal or State law.</p>
<p>Users are cautioned that the collection of personal information requested from or volunteered by children on-line or by email will be treated the same as information given by an adult, and may be subject to public access.</p>
<h3>Public Disclosure</h3>
<p>In the State Of Minnesota, laws exist to ensure that government is open and that the public has a right to access appropriate records and information possessed by State government. At the same time, there are exceptions to the public's right to access public records that serve various needs including the privacy of individuals. Exceptions are provided by both State and Federal laws.</p>
<p>All information collected at this site becomes public records that may be subject to inspection and copying by the public, unless an exemption in law exists. Minnesota Rules, Department of Administration Chapter 1205 defines Minnesota's Data Practices Act. In the event of a conflict between this <strong><a href="http://www.revisor.leg.state.mn.us/stats/13/">Data Practices Act</a></strong> and the <strong><a href="http://www.revisor.leg.state.mn.us/stats/138/17.html">Public Records Act</a></strong> or other law governing the disclosure of records, the Public Records Act or other applicable law will control.</p>
<h3>Access and Correction of Personal Information</h3>
<p>You can review any personal information we collect about you. You may recommend changes to your personal information you believe in error by submitting a written request that credibly shows the error. If you believe that your personal information is being used for a purpose other than what was intended when submitted, you may contact us. In all cases, we will take reasonable steps to verify your identity before granting access or making corrections. See Contact Information section.</p>
<h3>Cookies</h3>
<p>To better serve our users we are now using cookies to customize your browsing experience with The Minnesota.gov portal. A cookie contains unique information a website can use to track such things as the Internet Protocol address for the computer you used to access this site, the identification of the browser software and operating system you used, the date and time you accessed the site, and the Internet address of the website from which you linked to this site. Pursuant to Minn. Stat. 13.15 Subd 4, this information is used for purposes of evaluating the State's electronic government services; to prevent unlawful intrusions into the State Government's electronic systems; or as otherwise provided by law. Cookies created on your computer by using this website do not contain personally identifying information and do not compromise your privacy or security. You can refuse the cookie or delete the cookie file from your computer by using any of the widely available methods. If you choose not to accept a cookie on your computer, it will not prevent or prohibit you from gaining access to or using this site.</p>
<h3>Security , Intrusion, Detection Language</h3>
<p>MN.IT Services, as developer and manager of The Minnesota.gov portal, has taken several steps to safeguard the integrity of its telecommunications and computing infrastructure, including but not limited to authentication, monitoring, auditing and encryption. Security measures have been integrated into the design, implementation and day-to-day practices of the entire Minnesota.gov portal operating environment as part of its continuing commitment to risk management. This information should not be construed in any way as giving business, legal, or other advice, or warranting as fail proof, the security of information provided via Minnesota.gov portal supported websites.</p>
<p>Unauthorized attempts to deny service, upload information, change information, or to attempt to access a non-public site from this service are strictly prohibited and may be punishable under Title 18 of the U.S. Code to include the Computer Fraud and Abuse Act of 1986 and the National Information Infrastructure Protection Act.</p>
<p><a name="Disclaimer" id="Disclaimer" /> <a href="#Privacy%20Statement">Privacy Statement</a> <a href="#Disclaimer">Disclaimer</a> <a href="#Security%20Statement">Security Statement</a> <a href="#Linking%20Policy">Linking Policy</a> <a href="#Accessibility">Accessibility</a></p>
<h2>Disclaimer:</h2>
<p>The Minnesota.gov portal has links to other websites. These include links to Web sites operated by other State of Minnesota government agencies, other states, nonprofit organizations and private businesses. When you link to another site, you are no longer on The Minnesota.gov portal and this Privacy Notice is not applicable. When you link to another website, you are subject to the privacy policy of that new site.</p>
<p>Neither the State of Minnesota, nor any agency, officer, or employee of the State of Minnesota warrants the accuracy, reliability or timeliness of any information published by this system, nor endorses any content, viewpoints, products, or services linked from this system, and shall not be held liable for any losses caused by reliance on the accuracy, reliability or timeliness of such information. Portions of such information may be incorrect or not current. Any person or entity that relies on any information obtained from this system does so at his or her own risk.</p>
<h3>Use of Information on this Site</h3>
<p>The state agency pages are presented as an informational service and any content should be independently verified with specific agencies when an official record of action is required. State agency authored documents are in the public domain. Copyright and access restrictions apply.</p>
<p><a name="Security%20Statement" /> <a href="#Privacy%20Statement">Privacy Statement</a> <a href="#Disclaimer">Disclaimer</a> <a href="#Security%20Statement">Security Statement</a> <a href="#Linking%20Policy">Linking Policy</a> <a href="#Accessibility">Accessibility</a></p>
<h2>Security Statement:</h2>
<p>The State of Minnesota, as developer and manager of Minnesota.gov, the state of Minnesota's portal, has taken a number of steps to safeguard the integrity of its data and prevent unauthorized access to information that is maintained in agency computer systems. These measures are designed and intended to prevent corruption of data, block unknown or unauthorized access to our systems and information, ensure the integrity of information that is transmitted between users of e-government applications and the State, and to provide reasonable protection of private information that is in our possession.</p>
<p>For security purposes and to assure that this service remains available to all users, Minnesota.gov uses special software programs for monitoring and auditing network traffic to identify unauthorized attempts to upload or change information, or otherwise to cause damage to this government computer system. Minnesota.gov also uses industry-standard software tools to control access to specific applications and services and to protect data that is transmitted electronically between users and the State of Minnesota. These programs help us identify someone attempting to tamper with this website.</p>
<p><strong>WARNING</strong> -- If security monitoring reveals possible evidence of criminal activity, information pertaining to such activity may be provided to law enforcement officials.</p>
<p>Unauthorized attempts to upload information and/or change information on Minnesota.gov, the state of Minnesota portal, or its composite websites are strictly prohibited and are subject to prosecution under the Computer Fraud and Abuse Act of 1986 and Title 18 U.S.C. Sec.1001 and 1030.</p>
<p>The information contained in this policy statement should not be construed in any way as giving business, legal, or other advice, or warranting as fail proof, the security of information provided via the Minnesota.gov portal or the websites of individual state agencies.</p>
<p><a name="Linking%20Policy" /> <a href="#Privacy%20Statement">Privacy Statement</a> <a href="#Disclaimer">Disclaimer</a> <a href="#Security%20Statement">Security Statement</a> <a href="#Linking%20Policy">Linking Policy</a> <a href="#Accessibility">Accessibility</a></p>
<h2>Minnesota.gov Linking Policy:</h2>
<p>The Minnesota.gov portal contains links to other websites, including those owned or maintained by other government, private, or not-for-profit entities. Such links are provided for convenience and/or information purposes only.
<br />
In the course of providing comprehensive service and information, Minnesota.gov may provide links to content not controlled by the State of Minnesota. No endorsement is intended or made of any link, product, service or information either by its inclusion or exclusion from this portal.</p>
<p>Hypertext links to external websites and pages may be removed or replaced at the sole discretion of Minnesota.gov, at any time without notice.</p>
<p>In addition, hypertext links may be created by Minnesota.gov for informational purposes where the linked external website will provide useful and valuable information to visitors to this web portal, or where the linked external website is required or authorized by law.</p>
<h3>Links From Minnesota.gov</h3>
<p>To request a link on the Minnesota.gov site send your request via <a href="mailto:info.mn.gov@state.mn.us?subject=FROM%20MINNESOTA.GOV">Contact Us</a>.</p>
<h3>Criteria for including an external link on Minnesota.gov</h3>
<p>Minnesota.gov staff members will evaluate each requested link based upon the following criteria:</p>
<p>The suggested website must either be a state or local government agency/quasi-agency or non-profit entity, or must provide information related to state or local government or other statewide interest. Commercial websites will not be considered unless they provide significant information or services of a statewide interest or are in partnership with a state entity.</p>
<h3>Links To Minnesota.gov</h3>
<p>Any website can link to Minnesota.gov. If you link to this site, you may not portray any person or subject in a false or misleading light. You must also refrain from creating frames, or using other visual altering tools, around the Minnesota.gov pages. Lastly, you may not imply that the state of Minnesota is endorsing your product or services.</p>
<p>For easier linking, you may download the following graphic and use it for linking to the homepage of the Minnesota.gov website. The icon should only be used to link to the Minnesota.gov homepage.</p>

<br />
<p>To link to Minnesota.gov, use the following html code: <a href="http://minnesota.gov">Minnesota.gov</a></p>
<p><a name="Accessibility" id="Accessibility" /> <a href="#Privacy%20Statement">Privacy Statement</a> <a href="#Disclaimer">Disclaimer</a> <a href="#Security%20Statement">Security Statement</a> <a href="#Linking%20Policy">Linking Policy</a> <a href="#Accessibility">Accessibility</a></p>
<h2>Accessibility:</h2>
<p>Minnesota.gov is designed to offer Minnesota citizens, businesses, and agencies easy and efficient access to state government resources. We appreciate that many visitors to Minnesota.gov are concerned about accessibility to digital government. Currently, Minnesota.gov makes every effort to adhere to the <a href="https://mn.gov/mnit/assets/Stnd_State_Accessibility_tcm38-61585.pdf" target="_blank" title="MNIT Website - Accessibility Standard">state accessibility standard</a>. Ongoing validation testing is conducted using industry-recognized tools for coding and usability.</p>
<p>All executive branch agencies are required, effective September 1, 2010, to follow the standard, which combines the <a href="http://www.w3.org/TR/WCAG/" target="_blank" title="W3C Web Content Accessibility Guidelines">Web Content Accessibility Guidelines 2.0</a> and <a href="http://section508.gov/" target="_blank" title="Rehabilitation Act of 1973, Section 508">Section 508 of the Rehabilitation Act of 1973</a>. The goal of the Accessibility Standard is to improve the accessibility and usability of information technology products and services for all government end-users in the State of Minnesota. Please <a href="mailto:info.mn.gov@state.mn.us" title="Minnesota technical issue email ">contact us</a> if you are having difficulties accessing portions of Minnesota.gov.</p>
            
        
        
</div>						</div>
		
					<!-- general content //-->


			
		        


		        </div><!-- col //-->
		        
  			
  				<!-- navigtaion column //-->
  				<div class="col-md-3 col-md-pull-9 hidden-sm hidden-xs">
  					<nav role="navigation" id="left_nav" class="left"></nav>
   				</div>
  				<!-- end left nav column //-->
		        
		        
			</div><!-- row //-->
		</div><!-- container //-->
    </div><!-- content //-->

<footer id="footer">
  	  	<div class="container">
	  	
	  		<h1 class="sr-only">Footer contents</h1>
	  		<div id="footer-menu" class="row margin-bottom" role="navigation">
	  			
	  			<div class="col-sm-3">
	  				<h2>About</h2>	  				
	  				<ul>
	  					<li><a href="https://mn.gov/admin/demography/" target="_blank" >Demographics</a></li>
	  					<li><a href="https://www.mnhs.org/" target="_blank" >History & Facts</a></li>



	  					<li><a href="https://mn.gov/portal/about-minnesota/about-mn-gov.jsp">Privacy Notice</a></li>
                                                <li><a href="https://mn.gov/portal/brand/">State Brand</a></li>



                                                <li><a href="http://www.dot.state.mn.us/statemap/" target="_blank" >State Map</a></li>
                                                <li><a href="https://www.leg.state.mn.us/leg/Symbols" target="_blank" >State Symbols</a></li>
	  				</ul>
	  			</div>




	  			<div class="col-sm-3">
	  				<h2>Connect</h2>	  				
	  				<ul>



	  					<li><a href="https://mn.gov/portal/government/state/agencies-boards-commissions/index.jsp">Agency Websites</a></li>
	  					<li><a href="https://mn.gov/portal/government/local/cities/index.jsp">City Websites</a></li>
	  					<li><a href="https://mn.gov/portal/government/local/counties/index.jsp">County Websites</a></li>
	  					<li><a href="https://mn.gov/portal/government/state/index.jsp">State Officials</a></li>
                                                <li><a href="https://mn.gov/portal/connect/">State Mobile Sites & Apps</a></li>



	  					<li><a href="https://www.usa.gov/" target="_blank" >USA.gov</a></li>
	  				</ul>
	  			</div>
	  			<div class="col-sm-3">
	  				<h2>Help</h2>		  				
	  				<ul>
	  					<li><a href="mailto:info.mn.gov@state.mn.us">Report an Issue with this Site</a></li>



	  					<li><a href="https://mn.gov/portal/about-minnesota/accessibility-issues/index.jsp">Report an Accessibility Issue</a></li>



	  					<li><a href="https://dps.mn.gov/divisions/bca/bca-divisions/administrative/Pages/amber-alert-active-amber-alerts.aspx" target="_blank" >Amber Alerts</a></li>
	  					<li><a href="https://mn.gov/mmb/be-ready-mn/weather/" target="_blank" >Weather Emergencies</a></li>
	  					<li><a href="https://www.211unitedway.org/" target="_blank" >211 – Service Information</a></li>
	  					<li><a href="http://www.511mn.org/" target="_blank" >511 – Travel Information</a></li>
	  					<li><a href="https://dps.mn.gov/divisions/ecn/programs/911/Pages/text-to-911.aspx" target="_blank" >911 - Emergency Help</a></li>
	  				</ul>
	  			</div>
	  			<div class="col-sm-3">
                      
                    <a href="/portal/"><img src="/portal/assets/footer-mn-logo_tcm1077-352400.png" alt="mn logo" id="footer-logo"></a>

	  			</div>
	  		
	  		</div><!-- end row //-->
	  		
	  		
	  		
	  	</div><!-- end container //-->
</footer>
 
<a href="#content" id="back-to-top-modal" tabindex="0">back to top <span class="fa fa-level-up"></span></a>
<script id="corejs" defer src="/portal/js/core.js"></script>

</body>
</html>