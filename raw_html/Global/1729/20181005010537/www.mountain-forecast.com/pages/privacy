<!DOCTYPE html>
<html lang="en">
<head>
  <title>Privacy Statement for Mountain-Forecast.com</title>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
  <script type="text/javascript"><!--
    var uj_funcs = [];
    var height_units = 'm';
    var imperial = true;
    //-->
  </script>

  <script>(function() {
  var fclayout = (window.FCLAYOUT || (window.FCLAYOUT = {}));
  if (window.matchMedia) {
    fclayout.largeUp = window.matchMedia('(min-width: 948px)').matches;
  } else {
    fclayout.largeUp = (window.innerWidth >= 948);
  }
}).call(this);</script>
  <link rel="stylesheet" media="screen, print" href="/assets/application-b709635d01c182d9e4a93028f41f34221e2f8609d4c4f64e1f3f59b65fe0cc7c.css" />
  <link rel="stylesheet" media="screen" href="/packs/application-f0b85ddefac14b50f76978af46f6839a.css" />
  <link rel="stylesheet" media="print" href="/assets/print-004a9f261d5b9d2a82d635ca7037db02189aa16b0214175d5c0e01074f41ae99.css" />
  
  <meta name="description" content="Mountain weather forecasts for over 11300 mountain summits around the world. Detailed 6 day hill, mountain and summit forecasts for up to 5 different elevations per mountain. The comprehensive weather resource is aimed at climbers, mountaineers, hillwalkers, hikers or outdoor enthusiasts planning expeditions where mountain weather is critical." />
  <meta name="keywords" content="mountain weather,mountain forecast,mountain,summit,weather,mountain maps,www.mountain-forecast.com" />


    <script type='text/javascript'>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    </script>

    <script type='text/javascript'>
    (function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') +
    '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
    })();
    </script>

    <script type='text/javascript'>
    googletag.cmd.push(function() {
      var mappingLeader = googletag.sizeMapping().
                      addSize([0, 0], [[300, 250],[320, 100]]).
                      addSize([728, 1], [728, 90]).
                      addSize([970, 1], [[970, 250], [728, 90]]).
                      build();

  googletag.defineSlot('/1013347/Mountain_leaderboard_responsive', [300, 250], 'div-gpt-ad-resp-leader').defineSizeMapping(mappingLeader).addService(googletag.pubads());
  if (FCLAYOUT.largeUp) {
    googletag.defineSlot('/1013347/Mountain_160_Sky', [160, 600], 'div-gpt-ad-1378222498404-0').addService(googletag.pubads());
  }

    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
    });

    </script>

  

  <!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "28221962" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=28221962&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->
  <script>
window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-226744-23', 'auto');
ga('send', 'pageview');
</script>
<script async src='https://www.google-analytics.com/analytics.js'></script>

</head>

<body>
  <header class="top-header">
    <div id="cont">

      <div class="mobile-search-container off-canvas position-top" id="offCanvas" data-off-canvas>
        <form method="post" id="locationform--mobile" action="/peaks/catch">
          <input id="location--mobile" class="mobile-search js-mobile-search-field" name="query" tabindex="1" type="text" placeholder="search mountain or country" autocomplete="off"/>
          <input id="loc_id--mobile" name="loc_id" type="hidden" />
          <input type="hidden" name="action" value="save" />
        </form>
      </div>
      <div class="off-canvas-content" data-off-canvas-content></div>


      <div class="mobile-menu-container off-canvas position-left" id="offCanvasMenu" data-off-canvas>
        <nav class="mobile-menu">
          <ul class="mobile-menu__list">
            <li class="mobile-menu__list-item">
              <a href="/">Home</a>
            </li>
            <li class="mobile-menu__list-item">
              <a class="active" href="/countries">Mountain weather</a>
            </li>
            <li class="mobile-menu__list-item">
              <a href="/mountain_ranges">Mountain ranges</a>
            </li>
            <li class="mobile-menu__list-item">
              <a href="/weather_maps">Weather maps</a>
            </li>
            <li class="mobile-menu__list-item">
              <a href="/photos">Photos</a>
            </li>
          </ul>
        </nav>
        <div class="off-canvas-content" data-off-canvas-content></div>
      </div>


      <div id="MM_mainheader" class="row">

        <div id="MM_logo" class="medium-4 show-for-medium columns">
          <a href="/">
            <img alt="Mountain-Forecast" src="/images/elements/logo-img.svg" width="247" height="57" />
          </a>
        </div>

        <nav class="b-navigation-menu small-12 medium-8 columns">

          <ul class="b-navigation-menu__list show-for-medium">
            <li class="b-navigation-menu__list-item">
              <a href="/">Home</a>
            </li>
            <li class="b-navigation-menu__list-item">
              <a class="active" href="/countries">Mountain weather</a>
            </li>
            <li class="b-navigation-menu__list-item">
              <a href="/mountain_ranges">Mountain ranges</a>
            </li>
            <li class="b-navigation-menu__list-item">
              <a href="/weather_maps">Weather maps</a>
            </li>
            <li class="b-navigation-menu__list-item">
              <a href="/photos">Photos</a>
            </li>
          </ul>

          <ul class="b-navigation-menu__list b-navigation-menu__list--mobile show-for-small-only">
            <li class="b-navigation-menu__list-item b-navigation-menu__list-item--mobile">
              <a href="#" data-toggle="offCanvasMenu">
                <i class="fa fa-bars"></i>
              </a>
            </li>
            <li class="b-navigation-menu__list-item b-navigation-menu__list-item--mobile">
              <a href="/">
                <img alt="Mountain-Forecast" src="/images/elements/logo-img.svg" width="247" height="57" />
              </a>
            </li>
            <li class="b-navigation-menu__list-item b-navigation-menu__list-item--mobile">
              <a href="#" data-toggle="offCanvas">
                <i class="fa fa-search"></i>
              </a>
            </li>
          </ul>

        </nav>
      </div>


    <div id="navs" class="b-peak-locator row">
      <div id="MM_search" class="show-for-medium medium-3 columns">
        <form method="post" id="locationform" action="/peaks/catch">
          <input id="location" class="textbox js-desktop-search-field" name="query" tabindex="1" type="text" placeholder="> search mountain or country" autocomplete="off"/>
          <input id="loc_id" name="loc_id" type="hidden" />
          <input type="hidden" name="action" value="save" />
        </form>
      </div>
      <div id="MM_select" class="small-12 medium-9 columns">
        <form id="dropform" action="/peaks/selected" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="&#x2713;" /><input type="hidden" name="authenticity_token" value="z9CEw5csrv8aXZlYj17TY40lITurZIIzufJGjo2+WB2twxilAeCtQ6QGKK2HUZWO3JxfDQU/ZOCKnZDq58AB6g==" />
          <div id="dropformcont">
            <div id="for_mtn_range">
              <span class="textlabel" style="margin-left:0px;">range:</span>
              <select name="mountain_range_id" id="mountain_range_id" tabindex="2"><option value="1272">All other</option>
<option value="alaska-yukon-ranges">Alaska/Yukon Ranges</option>
<option value="alborz">Alborz</option>
<option value="alps">Alps</option>
<option value="anatolia">Anatolia</option>
<option value="andes">Andes</option>
<option value="antarctica-ranges">Antarctica Ranges</option>
<option value="appalachians">Appalachians</option>
<option value="arabian-peninsula">Arabian Peninsula</option>
<option value="arctic-cordillera">Arctic Cordillera</option>
<option value="armenian-highlands">Armenian Highlands</option>
<option value="atlantic-islands">Atlantic Islands</option>
<option value="atlas-mountains">Atlas Mountains</option>
<option value="baetic-system">Baetic System</option>
<option value="balkan-peninsula">Balkan Peninsula</option>
<option value="brazilian-highlands">Brazilian Highlands</option>
<option value="cambrian-mountains">Cambrian Mountains</option>
<option value="canadian-rockies">Canadian Rockies</option>
<option value="cantabrian-mountains-cordillera-cantabrica">Cantabrian Mountains (Cordillera Cantabrica)</option>
<option value="carpathian-mountains">Carpathian Mountains</option>
<option value="caucasus-mountains">Caucasus Mountains</option>
<option value="central-american-ranges">Central American Ranges</option>
<option value="central-asia-ranges">Central Asia Ranges</option>
<option value="central-siberian-plateau">Central Siberian Plateau</option>
<option value="coastal-south-america">Coastal South America</option>
<option value="columbia-mountains">Columbia Mountains</option>
<option value="dinaric-alps">Dinaric Alps</option>
<option value="east-africa-mountains">East Africa Mountains</option>
<option value="eastern-europe-ranges">Eastern Europe Ranges</option>
<option value="eastern-ghats">Eastern Ghats</option>
<option value="ethiopian-highlands">Ethiopian Highlands</option>
<option value="great-dividing-range">Great Dividing Range</option>
<option value="greater-himalaya">Greater Himalaya</option>
<option value="greenland">Greenland</option>
<option value="indian-ocean-islands">Indian Ocean Islands</option>
<option value="interior-mountains-of-canada">Interior Mountains of Canada</option>
<option value="interior-plateau-of-canada">Interior Plateau of Canada</option>
<option value="intermountain-west">Intermountain West</option>
<option value="iranian-plateau">Iranian Plateau</option>
<option value="ireland-ranges">Ireland Ranges</option>
<option value="italian-peninsula-and-islands">Italian Peninsula and Islands</option>
<option value="japanese-archipelago">Japanese Archipelago</option>
<option value="jura-mountains">Jura Mountains</option>
<option value="lake-district">Lake District</option>
<option value="laurentian-mountains">Laurentian Mountains</option>
<option value="levant-ranges">Levant Ranges</option>
<option value="malay-archipelago-and-melanesia">Malay Archipelago and Melanesia</option>
<option value="massif-central">Massif Central</option>
<option value="mexican-ranges">Mexican Ranges</option>
<option value="minor-ranges-in-south-america">Minor ranges in South America</option>
<option value="minor-ranges-of-africa">Minor Ranges of Africa</option>
<option value="minor-ranges-of-asia">Minor ranges of Asia</option>
<option value="minor-ranges-of-europe">Minor ranges of Europe</option>
<option value="new-guinea-ranges">New Guinea Ranges</option>
<option value="new-zealand">New Zealand</option>
<option value="north-pacific-volcanic-islands">North Pacific Volcanic Islands</option>
<option value="other-ranges-of-america">Other ranges of America</option>
<option value="other-ranges-of-australia">Other ranges of Australia</option>
<option value="other-ranges-of-canada">Other Ranges of Canada</option>
<option value="other-ranges-of-england">Other ranges of England</option>
<option selected="selected" value="pacific-coast-ranges">Pacific Coast Ranges</option>
<option value="pennines">Pennines</option>
<option value="pyrenees">Pyrenees</option>
<option value="ranges-of-southeast-asia">Ranges of Southeast Asia</option>
<option value="ranges-of-taiwan">Ranges of Taiwan</option>
<option value="ranges-of-the-philippines">Ranges of the Philippines</option>
<option value="rocky-mountains-usa">Rocky Mountains (USA)</option>
<option value="sahara-desert-ranges">Sahara Desert Ranges</option>
<option value="scandinavia-european-arctic-ranges">Scandinavia/European Arctic Ranges</option>
<option value="scottish-highlands">Scottish Highlands</option>
<option value="scottish-lowlands">Scottish Lowlands</option>
<option value="siberia-ranges">Siberia Ranges</option>
<option value="sierra-nevada">Sierra Nevada</option>
<option value="sikhote-alin-mountains">Sikhote-Alin Mountains</option>
<option value="sistema-central">Sistema Central</option>
<option value="sistema-ib-rico">Sistema Ibérico</option>
<option value="sistemas-preb-ticos">Sistemas Prebéticos</option>
<option value="south-pacific-volcanic-islands">South Pacific Volcanic Islands</option>
<option value="southern-africa">Southern Africa</option>
<option value="the-caribbean">The Caribbean</option>
<option value="tibet-and-central-china">Tibet and Central China</option>
<option value="urals">Urals</option>
<option value="west-africa-mountains">West Africa Mountains</option>
<option value="western-ghats">Western Ghats</option>
<option value="zagros">Zagros</option></select>
            </div>
            <span id="second_step_span">
  <div id="for_subrange">
  <span class="textlabel" style="margin-left:20px;">subrange:</span>
  <select name="subrange_id" id="subrange_id" tabindex="3"><option value="bedded-range">Bedded Range</option>
<option value="berkeley-hills">Berkeley Hills</option>
<option value="big-salmon-range-1">Big Salmon Range</option>
<option value="black-hills-2">Black Hills</option>
<option value="blue-mountains-orgeon-1">Blue Mountains - Orgeon</option>
<option value="british-columbia-coast-ranges-4">British Columbia Coast Ranges</option>
<option value="caliente-range">Caliente Range</option>
<option value="california-coast-ranges">California Coast Ranges</option>
<option value="cascade-range-3">Cascade Range</option>
<option value="central-and-southern-california-ranges-3">Central and Southern California Ranges</option>
<option value="cerbat-mountains">Cerbat Mountains</option>
<option value="channel-islands">Channel Islands</option>
<option value="chehalem-mountains">Chehalem Mountains</option>
<option value="clark-mountain-range">Clark Mountain Range</option>
<option value="coast-mountains">Coast Mountains</option>
<option value="columbia-plateau-4">Columbia Plateau</option>
<option value="diablo-range">Diablo Range</option>
<option value="eldorado-newberry-mountains">Eldorado-Newberry Mountains</option>
<option value="elk-river-mountains-vancouver-island">Elk River Mountains (Vancouver Island)</option>
<option value="fraser-plateau">Fraser Plateau</option>
<option value="garibaldi-ranges">Garibaldi Ranges</option>
<option value="gowlland-range">Gowlland Range</option>
<option value="haihte-range-vancouver-island">Haihte Range (Vancouver Island)</option>
<option value="hazelton-mountains">Hazelton Mountains</option>
<option value="jurupa-mountains">Jurupa Mountains</option>
<option value="kitimat-ranges">Kitimat Ranges</option>
<option value="klamath-mountains-1">Klamath Mountains</option>
<option value="little-san-bernardino-mountains">Little San Bernardino Mountains</option>
<option value="mayacamas-mountains">Mayacamas Mountains</option>
<option value="mayacmas-mountains-2">Mayacmas Mountains</option>
<option value="mccullough-mountains">McCullough Mountains</option>
<option value="mcdowell-mountain-range">McDowell Mountain Range</option>
<option value="nass-ranges-1">Nass Ranges</option>
<option value="north-coast-ranges">North Coast Ranges</option>
<option value="north-shore-mountains-2">North Shore Mountains</option>
<option value="northern-oregon-coast-range-1">Northern Oregon Coast Range</option>
<option value="northern-sierra-nevada">Northern Sierra Nevada</option>
<option value="northwest-u-s-coast-ranges-3">Northwest U.S. Coast Ranges</option>
<option value="ogilvie-mountains-1">Ogilvie Mountains</option>
<option value="olympic-mountains-1">Olympic Mountains</option>
<option value="oregon-coast-range">Oregon Coast Range</option>
<option value="peninsular-ranges">Peninsular Ranges</option>
<option value="queen-charlotte-mountains">Queen Charlotte Mountains</option>
<option value="rainbow-range-1">Rainbow Range</option>
<option value="san-bernardino-mountains">San Bernardino Mountains</option>
<option value="san-emigdio-mountains">San Emigdio Mountains</option>
<option value="san-francisco-hills">San Francisco Hills</option>
<option value="san-gabriel-mountains">San Gabriel Mountains</option>
<option value="san-jacinto-mountains">San Jacinto Mountains</option>
<option value="san-rafael-mountains">San Rafael Mountains</option>
<option value="santa-ana-mountains">Santa Ana Mountains</option>
<option selected="selected" value="santa-cruz-mountains">Santa Cruz Mountains</option>
<option value="santa-lucia-range">Santa Lucia Range</option>
<option value="santa-monica-mountains">Santa Monica Mountains</option>
<option value="santa-rosa-mountains">Santa Rosa Mountains</option>
<option value="santa-susana-mountains">Santa Susana Mountains</option>
<option value="santa-ynez-mountains">Santa Ynez Mountains</option>
<option value="santa-ynez-san-rapael">Santa Ynez/San Rapael</option>
<option value="sierra-juarez-mountains">Sierra Juarez Mountains</option>
<option value="sierra-pelona-mountains">Sierra Pelona Mountains</option>
<option value="siskiyou-1">Siskiyou</option>
<option value="siskiyou-mountains-1">Siskiyou Mountains</option>
<option value="sonoma-mountains">Sonoma Mountains</option>
<option value="spectrum-range-1">Spectrum Range</option>
<option value="spring-mountains">Spring Mountains</option>
<option value="tagish-highland-1">Tagish Highland</option>
<option value="tehachapi-mountains-1">Tehachapi Mountains</option>
<option value="transverse-ranges">Transverse Ranges</option>
<option value="trinity-alps-1">Trinity Alps</option>
<option value="tualatin-mountains">Tualatin Mountains</option>
<option value="vancouver-island-ranges">Vancouver Island Ranges</option>
<option value="wallowa-mountains">Wallowa Mountains</option>
<option value="678">All other</option></select>
  </div>
<span id="third_step_span"><div id="for_location"><span class="textlabel" style="margin-left:10px;">mountain:</span><select name="location_key" id="location_filename_part" tabindex="4" class="js-nav-location-select"><option value="">Choose</option><option value="Bernal-Heights-Summit/145">Bernal Heights Summit</option>
<option value="Black-Mountain-near-Los-Altos-California/857">Black Mountain (near Los Altos, California)</option>
<option value="Loma-Prieta/1155">Loma Prieta</option>
<option value="Long-Ridge-San-Mateo-County-California/792">Long Ridge (San Mateo County, California)</option>
<option value="Montara-Mountain/578">Montara Mountain</option>
<option value="Mount-Bielawski/986">Mount Bielawski</option>
<option value="Mount-Davidson-San-Francisco-California/283">Mount Davidson, San Francisco, California</option>
<option value="Mount-Sutro/277">Mount Sutro</option>
<option value="Mount-Thayer/1060">Mount Thayer</option>
<option value="Mount-Umunhum/1062">Mount Umunhum</option>
<option value="San-Bruno-Mountain/402">San Bruno Mountain</option>
<option value="Twin-Peaks-San-Francisco-California/281">Twin Peaks (San Francisco, California)</option></select></div></span>
</span>
            <input type="submit" value="Go" id="go" onclick="if (!document.getElementsByClassName('js-nav-location-select')[0].value) return false;" />
          </div>
</form>      </div>
      <div style="clear: both;"></div>
    </div>
  </div>
</header>



<main class="main-content">

  <div id="cont">


    <div id="MM_content">
      
      
<div class="advertising" id='div-gpt-ad-resp-leader' style="text-align: center;">
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-resp-leader'); });
  </script>
</div>

      <div class="bodytext">
        

          <div id="rightd">
              <div class="img_col leftbsect">
    <a href="/peaks/Mount-Augusta/photos/4167"><img alt="Mount Augusta" class="column-image" src="https://www.mountain-forecast.com/system/images/4167/thumb_col/Mount-Augusta.jpg" /></a>
    <p class="col_link"><a href="/peaks/Mount-Augusta/photos/4167">Mount Augusta</a></p>
  </div>



            
<script type="text/javascript">if (FCLAYOUT.largeUp) { document.write('\n  <!-- Mountain_160_Sky -->\n  <div class=\"sticky-sky\" id=\'div-gpt-ad-1378222498404-0\' style=\'width:160px; height:600px;\'>\n  <script type=\'text/javascript\'>\n  googletag.cmd.push(function() { googletag.display(\'div-gpt-ad-1378222498404-0\'); });\n  <\/script>\n  <\/div>\n'); } </script>

          </div>


        <div class="text_left">
          <div id="flash">
</div>
          
          <h1>Privacy Statement for Mountain-Forecast.com</h1>

<p>
<b>Mountain-forecast.com </b> has
created this privacy statement in order to demonstrate our firm
commitment to privacy. The following discloses the information
gathering and dissemination practices for this Web site.
</p>

<h2>Information
Automatically Logged</h2>

<p>We use your IP address to gather broad demographic information.</p> 

<h2>Advertisers</h2>

<p>We
use an outside ad company to display some of our ads on our site.
These ads may contain cookies. While we use cookies in other parts
of our Web site, cookies received with banner ads are collected
by our ad company, and we do not have access to this information.</p>

<p>Some
customer data is shared with the advertising companies.</p>

<h2>Registration
Forms</h2>

<p>Our
site's registration form requires users to give us contact information
(like their name, email, and postal address), and unique identifiers
(like their social security number).</p>

<p>Contact
information from the registration forms is used to ship purchases,
information about our company. The customer's contact information
is also used to get in touch with the customer when necessary.</p>


<p>Users
may opt-out of receiving future mailings; see the choice/opt-out
section below.</p>

<p>Unique
identifiers (such as social security numbers) are collected from
Web site visitors for use as account numbers in our record system.</p> 

<h2>Order Forms</h2> 

<p>Our
site uses an order form for customers to request information, products,
and services. We collect contact information (like their email
address), and financial information (like their account or credit
card numbers).</p> 

<p>Contact
information from the order forms is used to send promotional material
from some of our partners to our customers. The customer's contact
information is also used to get in touch with the visitor when
necessary.</p>

<p>Users
may opt-out of receiving future mailings; see the choice/opt-out
section below.</p>


<p>Financial
information that is collected is used to bill the user for products
and services.</p>

<h2>External
Links</h2>

<p>This
site contains links to other sites. <b>Mountain-forecast.com </b>
is not responsible for the privacy practices or the content of such
Web sites.</p>


<h2>Public
Forums</h2> 

<p>This
site makes forums, message boards, and/or news groups available
to its users. Please remember that any information that is disclosed
in these areas becomes public information and you should exercise
caution when deciding to disclose your personal information.</p>

<h2>Security</h2>

<p>This
site has security measures in place to protect the loss, misuse,
and alteration of the information under our control. Our servers
are protected by a firewall and UPS. Daily backups are made of
the site and data which are stored on separate computers in another
part of the country.</p>

<h2>Choice/Opt-Out</h2>

<p>Our
site provides users the opportunity to opt-out of receiving
promotional/marketing
information from us:
</p>
<ul>
  <li>
    You
    can send email to our helpdesk.
  </li>
</ul>

<h2>Data
Quality/Access</h2>

<p>This
site gives users the following options for changing and modifying
information previously provided:
</p>
<ul>
  <li>
    You
    can send email to our helpdesk.
  </li>
</ul>

<h2>Contacting
the Web Site</h2>

<p>If
you have any questions about this privacy statement, the practices
of this site, or your dealings with this Web site, you can contact us
via our helpdesk.</p>
        </div>
        <div style="clear: both;"></div>
      </div>
    </div>
</main>



    <div class="footerfoot"></div>
    <div id="bg" style="z-index:0"></div>
  </div>
  <script src="/assets/application-bab15e69c015b788bc4fa9705728dc521d52f613000a8a2b71772b499e58e7eb.js"></script>
  <script src="/packs/application-35a382eda17b5e70f2ea.js"></script>
  <div id="sfooter">
  <div class="sfooter-int row expanded">
    <div class="footer-section footer-partners small-6 medium-3 columns">
      <h3>&nbsp;</h3>
      <ul>
          <li>
            <img alt="Weather-Forecast logo" height="37" src="/images/weather.png" width="130">
          </li>
          <li>
            <img alt="Snow-Forecast logo" height="37" src="/images/snow.png" width="130">
          </li>
          <li>
            <img alt="Surf-Forecast logo" height="37" src="/images/surf.png" width="130">
          </li>
          <li>
            <img alt="Tide-Forecast logo" height="37" src="/images/tide.png" width="130">
          </li>
      </ul>
    </div>

    <div class="footer-section footer-company small-6 medium-3 columns">
      <h3>
        Company
      </h3>
      <ul>
        <li>
          <a href="/pages/contact">Contact us</a>
        </li>
        <li>
          <a href="/pages/terms">Terms</a>
        </li>
        <li>
          <a href="/pages/privacy">Privacy</a>
        </li>
        <li>
          <a href="/pages/cookie_policy">Cookie Policy</a>
        </li>
      </ul>
    </div>

    <div class="footer-section footer-explore small-6 medium-3 columns">
      <h3>
        Explore
      </h3>
      <ul>
        <li>
          <a href="/mountain_ranges">Mountain Ranges</a>
        </li>
        <li>
          <a href="/weather_maps">Dynamic Weather Maps</a>
        </li>
        <li>
          <a href="/maps">Static Weather Maps</a>
        </li>
        <li>
          <a href="/photos">Mountain Photos</a>
        </li>
      </ul>
    </div>

    <div class="footer-section footer-services small-6 medium-3 columns">

    </div>

    <div class="footer-section footer-social small-6 medium-3 columns">
      <h3>
        Social
      </h3>
      <ul class="social-list">
        <li>
          <a href="https://www.facebook.com/pages/Mountain-Forecast/148064945255923?ref=hl"><img src="/images/fb.png">Like us</a>
        </li>
        <li>
          <a href="http://twitter.com/meteo365"><img src="/images/t.png">Follow us</a>
        </li>
        <li>
          <a href="https://plus.google.com/+Mountain-forecast/posts" rel="publisher"><img src="/images/g.png">Add us</a>
        </li>
        <li class="last">
          <a href="http://www.youtube.com/snowforecast"><img src="/images/y.png">Watch us</a>
        </li>
      </ul>
    </div>
  </div>

  <div class="row">
    <div class="copyright small-12 columns">
      <p>
        &copy; 2018 Meteo365.com.
      </p>
    </div>
  </div>
</div>

    <script>
    if (FCLAYOUT.largeUp) {
      $('.useful-field__element').sticky({
        stopper: ".footerfoot",
        topSpacing: 90,
      });
    }
  </script>

  <script type="text/javascript"><!--
    if ((typeof init_uj) != "undefined") { init_uj() };
    //-->
  </script>
</body>

</html>
