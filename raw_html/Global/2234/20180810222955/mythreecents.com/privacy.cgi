
<!doctype html>
<html lang="en">
<head>
<title></title>

<meta http-equiv="content-type" content="text/html,charset=utf-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
<link rel="shortcut icon" href="/favicon.ico?v=3" />
<link rel="stylesheet" type="text/css" href="/main_style.css" />
<link href='https://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
<!--[if lte IE 6]>
<script src="/scripts/catch.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="/ie6_style.css" />
<![endif]-->
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-20994541-1']);
	_gaq.push(['_trackPageview']);
	setTimeout("_gaq.push(['_trackEvent', 'adjust_bounce', 'read'])",15000); //adjusted bounce rate
	setTimeout("_gaq.push(['_trackEvent', 'time_on_page', '11-30', null, null, true])",10000);
	setTimeout("_gaq.push(['_trackEvent', 'time_on_page', '31-60', null, null])",30000);
	setTimeout("_gaq.push(['_trackEvent', 'time_on_page', '61-180', null, null])",60000);
	setTimeout("_gaq.push(['_trackEvent', 'time_on_page', '181-600', null, null])",180000);
	setTimeout("_gaq.push(['_trackEvent', 'time_on_page', '601-1800', null, null])",600000);
	setTimeout("_gaq.push(['_trackEvent', 'time_on_page', '1801+', null, null])",1800000);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</head>
<body>
<a name="top"></a>	
<div id="heading"></div>
<div id="container">
<div id="subcontainer">
<div id="headBar">
<span id="logo"><a href="http://mythreecents.com"><img src="/m3c_logo3.gif" border="0" id="logoImg" /><img src="/media/mobile_logo2.png" border="0" height="55" id="logoMobileImg" /></a></span>
<div id="menuBtnBar">
	<span id="menuCompose">
		<span id="menuBtnCompose"><a href="http://mythreecents.com/getStarted.cgi?ref=M"><img src="/media/compose_mobile3.gif" border="0"></a></span>
	</span>
	<span id="menuList">
		<span id="menuBtn"><img src="/media/search_grey2.gif" border="0" id="menuImg" onClick="$('#searchBoxId').toggle();$('#menu').toggle();" /></span>
		<span id="menuBtnActive"><img src="/media/menu_lines_active2.gif" border="0" id="menuImgActive" onClick="document.getElementById('menu').style.display='none';document.getElementById('menuImg').style.display='block';document.getElementById('menuBtnActive').style.display='none';document.getElementById('searchBoxId').style.display='none';document.getElementById('jumpLink').style.display='block';" /></span>
	</span>
</div>

<div id="headerSocial">
	<div id="manage-account-links">
	<a href="https://mythreecents.com/myProfile.cgi">Login</a>
	</div>

	<div id="socialStuff">
		<span id="feedcnt"><a href="http://mythreecents.com/newsletterSignup.cgi"><img src="/images/feedburner.gif" style="border:0" alt="Feedburner count"></a></span>
		<span id="fbcnt"><iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fmy3cents&amp;width&amp;layout=box_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=65" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:65px;" allowTransparency="true"></iframe></span>
	</div>

	<!--<div id="fb_home_btn" class="fb-like" data-href="http://www.facebook.com/my3cents" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false" data-font="verdana"></div>-->
</div>
</div>

<div id="navwrap">
	<ul id="menu">
		<li id="menuItem0"><a href="#top"><img src="/images/top.png" border="0" id="topIcon" class="topJump" title="Jump to top of page"></a></li>
		<li id="menuItem1"><a href="http://mythreecents.com/getStarted.cgi?ref=2"><img src="/media/compose4.gif" border="0" id="composeIcon" title="Write a Review"> <span id="composeTxt">Write Review</span></a></li>
		<li id="menuItem2">
		<span id="searchBoxId" >
			<form action="https://www.google.com" id="cse-search-box">
			<input type="hidden" name="cx" value="partner-pub-0200629403145096:1265365941" />
			<input type="hidden" name="ie" value="UTF-8" />
			<input type="text" name="q" size="20" id="gSearchInput" /><span id="g-search-button-bar" onClick="$(this).closest('form').submit();"></span>
			</form>
		</span>
		</li>
	</ul>
</div>
<hr id="topLine" />

<div id="pageContainer"><div id="privactyPolicy">
	<h1>Privacy Policy</h1>

	<p>The policy explains how we will gather and use the information at this Web site: 
	http://mythreecents.com</p>

	<p><b>How we use your IP address</b><br>
	We may use your Internet Protocol (IP) address to help diagnose problems with our server and track movement within our web site only to optimize your experience. We may also collect the referring URL, access times and browser type. We will not link this information to any personally identifiable information unless you provide us permission to do so.</p>

	<p><b>How we use cookies</b><br>
	Our site uses "cookies" to make your experience at our site specific to you. Currently, if you set your browser to reject cookies, you cannot use Mythreecents.com. We also plan to use cookies to keep track of how you found out about our site (i.e., what link to Mythreecents.com on another site you may have clicked on).</p>

	(A cookie is a tiny amount of data that's sent to your web browser from a web server and is stored on your computer's hard drive. Cookies generally are used to help web sites identify their users and determine the status of a "client.")<br><br>

	<b>Information Collected and Use and Disclosure of Information</b><br>
	Our site's registration form requires users to give us contact information (like your name and e-mail address), basic demographic information (like your zip code) and a password. We may use your contact information to get in touch with you when necessary (for instance, to get more information about a company you're contacting, to see what follow-up action you may want us to take, or if you send comments to us). <br><br>

	Our letter generator creates individual letters that are sent at your request to companies. Your e-mail address, and any other contact information you choose to give such as mailing address and phone number, will be shared with the company you're writing to so the company can respond to your letter. We send your letter only to those people whom you specify and we will not share it with others. Mythreecents.com is not responsible for any company's use of your personal information. From time to time, we single out individual letters as examples for our "Top Stories" section, but we will not post these letters or any part of them on our site or disclose them to others without the consent of the person who wrote the letter. <br><br>
 
	We will always ask you for permission before sharing your contact information with third parties interested in sending information and promotional offers about their products and services. This basically means that you won't get any junkmail from third parties unless we get you specific permission. <br><br>

	If you request to be contacted by a lawyer regarding your review, your contact information and location may be shared with an attorney who may attempt to contact you regarding your issue.
	<br><br>

	From time to time, we also conduct more detailed surveys of visitors to our site. You decide whether you want to respond to our surveys, and the answers to the questions are reported anonymously. <br><br>
 
	In addition, Mythreecents.com may disclose personally identifiable user information to others when we believe, in good faith, that such release is reasonably necessary to (i) comply with the requirements of the law or a legal process served on us; (ii) enforce or apply the terms of any of our user agreements; or (iii) protect the rights, property or safety of Mythreecents.com, our users, or others. <br><br>

	Consumers should be aware that information provided to us may be subpoenaed by courts of law and that in most instances we will be obligated to honor lawful court orders.
	<br><br>
	We do not make the e-mail addresses of those who access our site available to commercial organizations for marketing purposes except in the case of companies wishing to resolve consumer complaints.
	<br><br>

	<b>Other ways we gather information about you.</b>
	<br><br>
	We use third-party advertising companies to serve ads when you visit our Web site. My3cents.com does not evaluate or endorse the products and services advertised.  These companies may use information about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, click here: <a href="http://networkadvertising.org/consumer/opt_out.asp">http://networkadvertising.org/consumer/opt_out.asp</a>).
	<br><br>
	In the course of serving advertisements to this site, a third-party advertiser may place or recognize a unique "cookie" on your computer. A cookie is a data file that is installed on your computer’s hard disk. We may also place a “cookie” on your computer in order to track your web browsing or to provide information to other companies about your web browsing.  You can instruct your browser not to accept cookies, although some services may not function properly if you disable cookies.
	<br><br>
	This site implements Google Analytics features based on Display Advertising data, including Google Analytics Demographics and Interest Reporting. Using Google Ad Settings, visitors can opt-out of Google Analytics for Display Advertising and customize Google Display Network ads. We use data from Google’s Interest-based advertising or third party audience data (such as age, gender, and interests) to help improve the site and user experience.

	<br><br>

	<b>Send Your Letter to Others/Pass It On E-mail</b><br>
	Our site gives you the opportunity to enter a friend's e-mail address to send a copy of your letter or opinion or other information you feel may be of interest. This use is a one-time only e-mail sent to the recipient and our site will not send any further communications to the recipient. <br><br>


	<b>Public Forums</b><br>
	Mythreecents.com may in the future add chat rooms, forums, message boards and/or news groups available to its users. Any information disclosed in these areas becomes public information. You should exercise caution when deciding to disclose your personal information. We reserve the right to remove any posted material for any reason (for instance, if you are being offensive, or using vulgar language). <br><br>

	<b>Feedback On Us</b><br>
	The Feedback on Us feature creates individual letters that are sent to us at your request. Your e-mail address, and any other contact information you give us such as mailing address and phone number, will not be shared with anyone except the Mythreecents.com employees responsible for the site feature(s) you are writing about. As a result of your feedback, we may contact you for clarification or more information, to let you know the results of your feedback, or to thank you for your interest in our site. From time to time, we single out individual letters to use as testimonials, but we will not post these letters or any part of them on our site or disclose them to others without the consent of the person who wrote the letter. <br><br>

	<b>E-mail Links</b><br>
	We use an e-mail form located on the "Contact Us" page to allow you to contact us directly with any questions or comments you may have. We read every message sent in and try to respond directly to your questions or comments. This information is used to respond directly to your questions or comments. We may also file your comments to improve the site and program, or review and discard the information. Your personal information is only shared with third parties with your permission. <br><br>

	<b>Employment Opportunities</b><br>
	Users who are interested in learning more about employment opportunities at Mythreecents.com are invited to contact us.  Your e-mail address, and any other contact information you give us such as mailing address and phone number, will not be shared with anyone except for the Mythreecents.com employees responsible for recruitment and hiring. We may contact you if we wish to explore possible employment opportunities. <br><br>

	<b>Links to Other Sites</b><br>
	This site contains links to Web sites operated by third parties. Mythreecents.com is not responsible for the privacy practices or content of such sites. <br><br>

	<b>Security</b><br>
	Mythreecents.com has security measures in place to protect the loss, misuse and alteration of the information under our control. However, perfect security does not exist on the Internet. Also, in theory third parties can intercept Internet transmissions containing your personal data, and we don't control the privacy/security of transmissions to and from the site while in transit. 
	<br><br>

	<b>Third-Party Collection and Use of Information</b><br>
	From time to time, Mythreecents.com partners with third parties to provide certain features to Mythreecents.com users. A third party's collection and use of personally identifiable information is subject to that third party's privacy and security policy. Mythreecents.com does not have control or access to the information contained in cookies set by third parties.
	<br><br>
	We also use third party advertisements on Mythreecents.com to support our site.  Some of these advertisers may use technology such as cookies and web beacons when they advertise on our site, which will also send these advertisers (such as Google through the Google AdSense program) information including your IP address, your ISP , the browser you used to visit our site, and in some cases, whether you have Flash installed.  This is generally used for geotargeting purposes (showing New York real estate ads to someone in New York, for example) or showing certain ads based on specific sites visited (such as showing cooking ads to someone who frequents cooking sites).
	<br><br>
	DoubleClick DART cookies<br>
	We also may use DART cookies for ad serving through Google's DoubleClick, which places a cookie on your computer when you are browsing the web and visit a site using DoubleClick advertising (including some Google AdSense advertisements).  This cookie is used to serve ads specific to you and your interests ("interest based targeting").  The ads served will be targeted based on your previous browsing history (For example, if you have been viewing sites about visiting Las Vegas, you may see Las Vegas hotel advertisements when viewing a non-related site, such as on a site about hockey).  DART uses "non personally identifiable information".  It does NOT track personal information about you, such as your name, email address, physical address, telephone number, social security numbers, bank account numbers or credit card numbers.  You can opt-out of this ad serving on all sites using this advertising by visiting http://www.doubleclick.com/privacy/dart_adserving.aspx  
	<br><br>
	You can chose to disable or selectively turn off our cookies or third-party cookies in your browser settings, or by managing preferences in programs such as Norton Internet Security.  However, this can affect how you are able to interact with our site as well as other websites.  This could include the inability to login to services or programs, such as logging into forums or accounts.
	<br><br>
	Deleting cookies does not mean you are permanently opted out of any advertising program.  Unless you have settings that disallow cookies, the next time you visit a site running the advertisements, a new cookie will be added.

	<br><br>
	<b>Conversion Tracking and Use of Information</b><br>
	Occasionally Mythreecents.com will offer services that require the use of a 3rd party payment gateway.  If conversion tracking is used, please note that no personally identifiable information is collected through our conversion tracking utilities.

	<br><br>
	<b>Limits on Our Abilities</b><br> Our site is very complicated, so we might inadvertently use or disclose your data in ways inconsistent with this statement. For example, a temporary software glitch could lead to public display of your personal data despite your preferences otherwise. We might also make ad hoc uses of your data in ways not mentioned here. In these anomalous situations, your sole remedy is that we will try to rectify the anomaly as soon as we can. 
	<br><br>

	<b>Updating Your Information</b><br>
	If you need to update information about your account or any opinions/letters you have posted on our website, please contact us via email using admin@my3cents.com!<br><br>

	<b>Notification of Changes</b><br>
	If we change our privacy policy, we will post a notice on our site so our users are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. If at any point we decide to use personally identifiable information in a manner different from that stated at the time it was collected, we will notify users by e-mail. Users will have a choice as to whether or not we use their previously submitted information in this different manner. We will use information in accordance with the privacy policy under which the information was collected. <br><br>

	<b>Contacting the Web Site</b><br>
	If you have any questions about this privacy statement, the practices of this site or your dealings with this Web site, you can contact: <br><br>

	Mythreecents.com<br>
	Contact us at admin@my3cents.com
	<br><br>
</div></div>
</div>
</div>
<a name="bottom"></a>
<div id="footerLinks"><a href="https://mythreecents.com/myProfile.cgi">Login</a> | <a href="http://mythreecents.com/resources.cgi">Consumer Resources</a> | <a href="http://mythreecents.com/faq.cgi">FAQ</a> | <a href="http://mythreecents.com/company_services.cgi">Company Responses</a> | <a href="http://mythreecents.com/site_rules.cgi">Site Rules</a> | <a href="http://mythreecents.com/agreement.cgi">Terms & Conditions</a> | <a href="http://mythreecents.com/privacy.cgi">Privacy Policy</a> | <a href="http://mythreecents.com/about.cgi">About Us</a></div>
<script src="/m_script.min.js" type="text/javascript"></script>
<script src="/scripts/menufloat.min.js" type="text/javascript" defer="defer"></script>	
<script type="text/javascript" src="https://www.google.com/coop/cse/brand?form=cse-search-box&amp;lang=en" defer="defer"></script>
</body>
</html>