
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]--><head>

<!-- DFP head code - START -->
<script>
    var adsStart = (new Date()).getTime();
    function detectWidth() {
        return window.screen.width || window.innerWidth || window.document.documentElement.clientWidth || Math.min(window.innerWidth, window.document.documentElement.clientWidth) || window.innerWidth || window.document.documentElement.clientWidth || window.document.getElementsByTagName('body')[0].clientWidth;
    }

    var TIMEOUT = 1000;
    var EXCHANGE_RATE = 3.6;
    var screenSizeMobile = 768;

    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    var pbjs = pbjs || {};
    pbjs.que = pbjs.que || [];
    var adUnits = adUnits || [];

    function initAdServer() {
        if (pbjs.initAdserverSet) return;
        (function() {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        })();
        pbjs.initAdserverSet = true;
    };
    pbjs.timeout = setTimeout(initAdServer, TIMEOUT);
    pbjs.timeStart = adsStart;

    var dfpNetwork = '52304935';

    // START: Defining Adunits
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_IA_300x250_A",
            size: [[300, 250]],
            code: 'div-gpt-ad-Lyrics_IA_300x250_A',
            assignToVariableName: false // false if not in use
         }); 
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_IA_300x250_B",
            size: [[300, 250]],
            code: 'div-gpt-ad-Lyrics_IA_300x250_B',
            assignToVariableName: false // false if not in use
         }); 
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_LR_300x250_A",
            size: [[300, 250], [300, 600]],
            code: 'div-gpt-ad-Lyrics_LR_300x250_A',
            assignToVariableName: false // false if not in use
         }); 
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_LR_300x250_B",
            size: [[300, 250], [300, 600]],
            code: 'div-gpt-ad-Lyrics_LR_300x250_B',
            assignToVariableName: false // false if not in use
         }); 
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_LR_300x250_C",
            size: [[300, 250], [300, 600]],
            code: 'div-gpt-ad-Lyrics_LR_300x250_C',
            assignToVariableName: false // false if not in use
         }); 
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_LR_300x250_D",
            size: [[300, 250], [300, 600]],
            code: 'div-gpt-ad-Lyrics_LR_300x250_D',
            assignToVariableName: false // false if not in use
         }); 
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_Mobile_IA_300x250_A",
            size: [[300, 250]],
            code: 'div-gpt-ad-Lyrics_Mobile_IA_300x250_A',
            assignToVariableName: false // false if not in use
         });
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_Mobile_LR_300x250_A",
            size: [[300, 250]],
            code: 'div-gpt-ad-Lyrics_Mobile_LR_300x250_A',
            assignToVariableName: false // false if not in use
         });
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_Mobile_LR_300x250_B",
            size: [[300, 250]],
            code: 'div-gpt-ad-Lyrics_Mobile_LR_300x250_B',
            assignToVariableName: false // false if not in use
         });
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_Mobile_LR_300x250_C",
            size: [[300, 250]],
            code: 'div-gpt-ad-Lyrics_Mobile_LR_300x250_C',
            assignToVariableName: false // false if not in use
         });
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_Mobile_LR_300x250_D",
            size: [[300, 250]],
            code: 'div-gpt-ad-Lyrics_Mobile_LR_300x250_D',
            assignToVariableName: false // false if not in use
         });
    adUnits.push({
            network: dfpNetwork,
            adunit: "Lyrics_LR_125x125",
            size: [[125, 125]],
            code: 'div-gpt-ad-Lyrics_LR_125x125',
            assignToVariableName: false // false if not in use
         }); 

    // END: Defining Adunits

    googletag.cmd.push(function() {
      if(adUnits){
        var dfpSlots = [];
        for (var i = 0, len = adUnits.length; i < len; i++) {
          dfpSlots[i] = googletag.defineSlot('/'+adUnits[i].network+'/'+adUnits[i].adunit, adUnits[i].size, adUnits[i].code).addService(googletag.pubads());
          if(adUnits[i].assignToVariableName && (adUnits[i].assignToVariableName !== null)) window[adUnits[i].assignToVariableName] = dfpSlots[i];
        }
      }
    });
    googletag.cmd.push(function() {
        // Header Bidding Targeting
        pbjs.que.push(function() {pbjs.setTargetingForGPTAsync();});

        // Init DFP
        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();
    });
</script>
<!-- <script async='async' src="https://s3.us-east-2.amazonaws.com/hb-lyrics/cmp.config.js"></script> -->
<script type="text/javascript" async src="https://www.lyrics.com/adunits/prebid.js"></script>    
<!-- DFP head code - END --><meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Privacy Policy</title>
<meta name="description" content="">
<meta name="keywords" content="">
	<meta name="viewport" content="width=device-width">
<base href="https://www.lyrics.com/">

<script>
version = '1.0.83';
</script>

<!-- Bootstrap compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!--<link rel="stylesheet" href="--><!--/app_common/css/normalize.css">-->
<link rel="stylesheet" href="root/app_common/css/lyrc.css?v=1.2.36">
<!--[if lt IE 9]> <link rel="stylesheet" href="root/app_common/css/lyrc-ie8.css"> <![endif]-->
<!--[if lt IE 8]> <link rel="stylesheet" href="root/app_common/css/lyrc-ie7.css"> <![endif]-->
<link rel="shortcut icon" type="image/x-icon" href="root/app_common/img/favicon_lyrc.ico">
<link rel="search" type="application/opensearchdescription+xml" title="Lyrics.com" href="https://www.lyrics.com/open-search.xml">

<!--[if lt IE 9]>
<script src="root/app_common/js/libs/modernizr-2.8.3.custom.min.js"></script>
<script src="root/app_common/js/libs/html5shiv.min.js"></script>
<script src="root/app_common/js/libs/respond.min.js"></script>
<![endif]--><script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-172613-15']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script><meta property="fb:app_id" content="118234861672613"/>
<meta name="google-signin-client_id" content="567628204450-49mrbnlqde6k322k6j1nmpstf86djv24.apps.googleusercontent.com">

	<meta property="og:url" content="https://www.lyrics.com/privacy.php" />
	<link rel="canonical" href="https://www.lyrics.com/privacy.php" />

</head>
<body id="s4-page-privacy" data-fb="118234861672613" data-atp="ra-4f75bf3d5305fac2">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=118234861672613&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="page-container">
<header id="header">
	<div id="header-int" class="clearfix">		
					<div id="user-login">
				<a href="login.php">Login</a>&nbsp;<i class="fa fa-sign-in" aria-hidden="true"></i>
			</div>
				<div id="network-header"><span id="network-header-trigger">The&nbsp;<span class="hidden-xs">STANDS4&nbsp;</span>Network<span class="arw">&#9776;</span></span>
	<ul id="network-header-links" style="display:none;">
		<li class="nw-abbreviations"><a href="https://www.abbreviations.com/">ABBREVIATIONS</a></li>
		<li class="nw-anagrams"><a href="https://www.anagrams.net/">ANAGRAMS</a></li>
		<li class="nw-biographies"><a href="https://www.biographies.net/">BIOGRAPHIES</a></li>
		<li class="nw-convert"><a href="https://www.convert.net/">CONVERSIONS</a></li>
		<li class="nw-definitions"><a href="https://www.definitions.net/">DEFINITIONS</a></li>
		<li class="nw-grammar"><a href="https://www.grammar.com/">GRAMMAR</a></li>
		<li class="nw-literature"><a href="https://www.literature.com/">LITERATURE</a></li>
		<li class="nw-lyrics"><a href="https://www.lyrics.com/">LYRICS</a></li>
		<li class="nw-math"><a href="http://math.stands4.com/">MATH</a></li>
		<li class="nw-phrases"><a href="https://www.phrases.com/">PHRASES</a></li>
		<li class="nw-poetry"><a href="https://www.poetry.net/">POETRY</a></li>
		<li class="nw-quotes"><a href="https://www.quotes.net/">QUOTES</a></li>
		<li class="nw-references"><a href="https://www.references.net/">REFERENCES</a></li>
		<li class="nw-rhymes"><a href="https://www.rhymes.net/">RHYMES</a></li>
		<li class="nw-scripts"><a href="https://www.scripts.com/">SCRIPTS</a></li>
		<li class="nw-symbols"><a href="https://www.symbols.com/">SYMBOLS</a></li>
		<li class="nw-synonyms"><a href="https://www.synonyms.com/">SYNONYMS</a></li>
		<li class="nw-uszip"><a href="https://www.uszip.com/">USZIP</a></li>
	</ul>
</div>	</div>
</header><div id="main" role="main" class="container"><div id="content-top" class="content-top">
	<div class="view-search view-desktop hidden-xs">
		<form id="search-frm" method="get" action="subserp.php">
	<input type="hidden" name="genre" value="">
	<input type="hidden" name="style" value="">
	<input type="hidden" name="year" value="">
	<input type="hidden" name="dec" value="">
    <div class="page-top-search rc5">
        	<div class="page-word-search">
        		<input id="search" type="text" class="page-word-search-query rc5" name="st" value="" placeholder="Search for lyrics or artists..." autocomplete="off">
        		<div class="page-word-search-icon"><i class="fa fa-search" aria-hidden="true"></i></div>
        		<button type="submit" class="btn primary" id="page-word-search-button">Search</button>
        	</div>
        	<div id="page-word-search-ops">
        		<div><input type="radio" class="custom-rb" name="qtype" id="page-word-search-op1" value="1" checked="checked"><label for="page-word-search-op1"><span>in Lyrics</span></label></div>
        		<div><input type="radio" class="custom-rb" name="qtype" id="page-word-search-op2" value="2"><label for="page-word-search-op2"><span>in Artists</span></label></div>
        		<div><input type="radio" class="custom-rb" name="qtype" id="page-word-search-op3" value="3"><label for="page-word-search-op3"><span>in Albums</span></label></div>
        	</div>
    </div>
    <div id="page-letter-search" class="rc5"><a href="/artists/0">#</a><a href="/artists/A">A</a><a href="/artists/B">B</a><a href="/artists/C">C</a><a href="/artists/D">D</a><a href="/artists/E">E</a><a href="/artists/F">F</a><a href="/artists/G">G</a><a href="/artists/H">H</a><a href="/artists/I">I</a><a href="/artists/J">J</a><a href="/artists/K">K</a><a href="/artists/L">L</a><a href="/artists/M">M</a><a href="/artists/N">N</a><a href="/artists/O">O</a><a href="/artists/P">P</a><a href="/artists/Q">Q</a><a href="/artists/R">R</a><a href="/artists/S">S</a><a href="/artists/T">T</a><a href="/artists/U">U</a><a href="/artists/V">V</a><a href="/artists/W">W</a><a href="/artists/X">X</a><a href="/artists/Y">Y</a><a href="/artists/Z">Z</a><span class="vbar">&nbsp;</span><a href="justadded.php" class="z">NEW</a><a href="random.php" class="z">RANDOM</a></div>
</form>		<div class="page-top-logo" onclick="location.href='https://www.lyrics.com/';"><img src="root/app_common/img/top_logo_lyr.png" alt="Lyrics.com" title="Lyrics.com"></div>
	</div>
	<div class="view-search view-mobile visible-xs">
		<form id="search-frm-mob" method="get" action="subserp.php">
	<input type="hidden" name="genre" value="">
	<input type="hidden" name="style" value="">
	<input type="hidden" name="year" value="">
	<input type="hidden" name="dec" value="">
	<div class="page-top-search" class="rc5">
		<div class="page-word-search" style="width: calc(100% - 110px);">
			<div class="input-group">
				<input id="search-mob" type="text" class="page-word-search-query form-control rc5" name="st" value="" placeholder="Search for lyrics..." autocomplete="off">
				<span class="input-group-btn"><button id="page-word-search-button-mob" class="btn primary" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button></span>
			</div>
		</div>
		<div id="page-word-search-ops">
			<div><input type="radio" class="custom-rb" name="qtype" id="page-word-search-m-op1" value="1" checked="checked"><label for="page-word-search-m-op1"><span>in Lyrics</span></label></div>
			<div><input type="radio" class="custom-rb" name="qtype" id="page-word-search-m-op2" value="2"><label for="page-word-search-m-op2"><span>in Artists</span></label></div>
			<div><input type="radio" class="custom-rb" name="qtype" id="page-word-search-m-op3" value="3"><label for="page-word-search-m-op3"><span>in Albums</span></label></div>
		</div>
	</div>
	<div id="page-letter-search"></div>
</form>		<div class="page-top-logo" onclick="location.href='https://www.lyrics.com/';"><img src="root/app_common/img/top_logo_lyr.png" alt="Lyrics.com" title="Lyrics.com"></div>
	</div>
</div>
<div class="row">

	<div id="content-main" class="col-sm-8 col-sm-push-4">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="category-header clearfix">
					<div class="sub-category-header-icon"><i class="fa fa-user-secret fa-3x" aria-hidden="true"></i></div>
					<hgroup>
						<h3><a href="privacy.php">Legal</a></h3>
						<h1>Privacy Policy</h1>
						<h4>The privacy policy for the STANDS4 network of websites.</h4>
					</hgroup>
				</div>
			</div>
		</div>
		
		<div id="content-body">
			
			<article class="legal">
			
				<p>STANDS4 LLC cares about your privacy. We want to make you feel comfortable and secure using our web site. This privacy policy explains the types of information we gather, how it is protected, what we do with it and how you can correct or change information.</p>
				
				<h3>Consent to this Privacy Policy</h3>
				
				<p>By providing STANDS4 LLC with Personal Information (defined below), you signify that you agree with the terms of our current Privacy Policy as posted here. If you do not agree with any term in this Policy, please do not provide any Personal Information.</p>
				<p>If you choose not to provide Personal Information, you may not be able to take certain actions, like becoming an editor, access particular areas of the web site, posting comments to our Blog, or send us e-mail.</p>
				
				<h3>Continuing Agreement</h3>
				
				<p>STANDS4 LLC may change or revise this Privacy Policy from time to time by updating this posting. Please review this Privacy Policy each time you visit this web site in order to be aware of the most current terms regarding your use of this web site. Your use of this web site reaffirms your continuing agreement to the most current Privacy Policy.</p>
				
				<h3>What Personal Information is collected and how?</h3>
				
				<p>When you are applying to become an editor, adding new entries or sending us email, you may provide us with your name, biogpraphy, photo, Internet Protocol (“IP”) address, username and password, e-mail address and any additional types of Personal Data you may provide us with ("Personal Information"). We also collect information from you if you correspond with us directly.</p>
				<p>STANDS4 LLC also uses "cookies" to receive and store certain types of information whenever you interact with the web site. Cookies are identifiers transferred to your computer's hard drive through your browser which enable our systems to recognize your browser. This recognition allows web sites to give you customized and personalized service and helps web site owners identify ways to improve your online experience. Cookies may also be used to help web site owners track responses to messages sent on behalf of our advertisers so that STANDS4 LLC may aggregate such information and report results to them. The "help" portion of the toolbar on most browsers will tell you how to reject or disable cookies or receive notice when a new cookie appears. If you choose to reject or disable cookies, you will be unable to use those specific areas of the web site that require the use of cookies. For more information about cookies, visit <a href="http://www.allaboutcookies.org">http://www.allaboutcookies.org/</a> or <a href="http://www.aboutcookies.org.uk">http://www.aboutcookies.org.uk/</a>.</p>
				<p>STANDS4 LLC does not knowingly collect or maintain any Personal Information from children under the age of 13. In addition, no part of this web site is designed with the purpose of attracting any person under age 13.</p>
				
				<h3>How does STANDS4 LLC use your information?</h3>
				<p>STANDS4 LLC uses your Personal Information to enhance and personalize your online experience. For example we use Personal Information to operate, maintain and provide our features and services on the STANDS4 websites; Create, manage and verify user accounts; Contact you about service announcements, updates or offers; Send emails related to our services; Serve relevant and behavoural advertisements, and respond to user inquiries.
				<p>STANDS4 LLC does not sell Personal Information about individual customers to third parties, sell or rent lists of customers or use information about individual customers except as described below.</p>
				<p><strong>Aggregated Information:</strong> STANDS4 LLC may use aggregated information regarding its customers and usage of this web site and disclose such aggregated information to advertisers, partners and others for various purposes.</p>
				<p><strong>Third Party Service Providers:</strong> STANDS4 LLC may employ other companies and individuals to perform certain functions such as delivering packages, processing credit card payments and analyzing data. They may have access to personal information to perform their functions.</p>
				<p>We allow third-party companies to collect certain information when you visit our web site or within our email newsletters. These companies may utilize cookies, pixels or other technologies to collect user information (e.g., hashed, de-identified, data, click stream information, browser type, time and date, subject of advertisements clicked or scrolled over) during your visits to this and other websites in order to provide advertisements about goods and services likely to be of greater interest to you. To learn more about this behavioral advertising practice or to opt-out of this type of advertising, you can visit the websites of the Digital Advertising Alliance at <a href="http://www.aboutads.info/choices">http://www.aboutads.info/choices</a></p>
				<p><strong>YouTube Data:</strong> By visiting our websites you acknowledge and agree that we use YouTube's API to display videos on certain pages. Therefore, you notably agree to be bound to the Google Privacy Policy available at the following URL address: <a href="http://www.google.com/policies/privacy">http://www.google.com/policies/privacy</a>.</p>
				
				 <h3>Links to Third Party Websites</h3>
				
				<p>The STANDS4 websites, from time to time, contains links to and from third party websites of our partner networks, advertisers, partner merchants, retailers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for their policies. Please check the individual policies before you submit any information to those websites.</p>


				<h3>Enforcement of Agreement or Compliance with Law</h3>
				
				<p>STANDS4 LLC may use or disclose personal information when STANDS4 LLC believes it is appropriate to enforce these Terms of Use, to protect the rights, property or safety of STANDS4 LLC or its users or to comply with the law.</p>
				
				<h3>How may you change or remove your personal information?</h3>
				
				<p>You may at any time review and change your Personal Information stored by this web site by contacting us using our <a href="contact.php">contact page</a>.</p>
				<p>You may also request that STANDS4 LLC permanently remove all Personal Information stored by STANDS4 LLC by delivering notice requesting such removal. Upon such request, STANDS4 LLC will remove all such information and will not thereafter use personally identifiable information about you in any way.</p>
				
				<h3>Site Security</h3>
				
				<p>STANDS4 LLC will take reasonable precautions to keep your Personal Information secure. All personally identifiable information is subject to restricted access to prevent unauthorized access, modification or misuse.</p>
				
			</article>
			
		</div>
		
	</div>
	
	<div id="content-aside" class="col-sm-4 col-sm-pull-8">
		<div class="hidden-xs">
    <div class="sep-area"><hr class="sep"></div>
<div class="tagline">
	<h3>The Web's Largest Resource for</h3>
	<h2>Music, Songs <span>&amp;</span> Lyrics</h2>
</div>    <div class="nsep"><hr><h3>A Member Of The <span>STANDS4 Network</span></h3></div>
    <div id="sb-social">
	<div class="clearfix">
	<div class="social" title="Share this page on Facebook"><a id="share-facebook" href="javascript:void(0);" target="_blank"><span class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></span></a></div>
	<div class="social" title="Share this page on Twitter"><a id="share-twitter" href="javascript:void(0);" target="_blank"><span class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></span></a></div>
	<div class="social" title="Share this page on Reddit"><a id="share-reddit" href="javascript:void(0);" target="_blank"><span class="rd"><i class="fa fa-reddit-alien" aria-hidden="true"></i></span></a></div>
	<div class="social" title="Share this page with AddThis"><a id="share-addthis" href="javascript:void(0);" target="_blank"><span class="at"><i class="fa fa-plus" aria-hidden="true"></i></span></a></div>
	</div>
</div>    <div class="sep-area"><hr class="sep"></div>
</div>		
<section><!-- /52304935/Lyrics_LR_300x250_A -->
<div id='div-gpt-ad-Lyrics_LR_300x250_A'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-Lyrics_LR_300x250_A'); });
</script>
</div></section>
<section><!-- /52304935/Lyrics_LR_300x250_B -->
<div id='div-gpt-ad-Lyrics_LR_300x250_B'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-Lyrics_LR_300x250_B'); });
</script>
</div></section>
	<div class="cblocks">
		<div class="cblock promotions-list">
			<hgroup class="clearfix">
				<h4>Our awesome collection of</h4>
				<h3>Promoted Songs</h3>
			</hgroup>
			<span class="more"><a href="promoted-songs.php">&raquo;</a></span>
			<div class="cblock-int">
				<ul><li><div class="promo-img"><a href="/sublyric/57471/Carman+Bryant/Midnight+Star"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAAB8CAYAAADuM7t5AAAgAElEQVR42ny9ebBl13Xe99vDGe6977655240mg10gyBAkCAJjiIFkRJlMpY12HJsUxRllR07qXI5JadKlVSqnMjlVFyVssp2Jao4TmRJjiRbUYkSKZMMKVGkKA4ACRIg5m4AjZ67X7/53XvPOXvIH3vvc85t0Oni42vcfu+Ma6/hW9/6tvgbn/h5P149jrUOEAAgBR4I/wdCiPgfAoEIPxf+F76ECF8IEOEY8Vfj78p05Phh+3/xd9LPpdOn83SXBOEc3vt4/PB3hwfv8d7jHXg8PnwUfq67dHy8EiHmDhx+wINUEiEkzrnwM717Er1LT9eaDiNEuD/v/dx9h38XCJnupXdfvXN75/HO4byL1+3v/olwj4T7TPcTTu/5gX96P+MhnMN353DpAYV/nT9vPAUi/EX47t7D8/ftc45X1zuvv+v0/q77AI/jniOrPPXE135HL6we5+sv7TKdVEihQCq8FDgB3hFemQgvTSGRXiCkwguBFCAFZFKjlUJKCVJGA5bhgQuFjMdIdyGQwaBkMCopJVIqQKCkRAmJFMEwvQgGoIRGCIHtvWTjPMZYvLUYa6mtxTmPc57GOgBcOAg+nBItFEpKpJSdMQfLpchyhFJYZ5FCxutK9xFMRwmJkhKRFo0Q7b0757AunDd8plBKIVQ4hlTh3MmIPeCsxRpDU1c0tsFaE64HD+FQuGQ4zmGMaZ+jDMuwNfhkHEIIvHMQP7M4nHNUVYV1FmcdtbXpBeO8x1qLx7ZG1C4k58G7cM3BksMx4zW5uHD6xuuCW4iWHP5NxM/xDm8N//CTb+P8+Q20sZa9yYzJfoUUEpQGKfECnAsWL4RAa02uNK5xCClxCKTwKCHQUoWHKyRC6+BFWiOTSOKLRCBl6zeRUrYGiLCAINMaiQg+UwYPJpBI6cNxvQchcB6sczTGYI2hahqc81gP1joaa8Mvy7BgrA8vRAsbDFzK+Kx963XrqsJLFTywCPcZrs2316lEuLZ07eHLtgYQvKdAa49SHq1BKB+OpQTBFsOCds5hmgbbGOq6oWlqPDZ6cBuPFzyXtRbvLc65GA1CsJE9b+6jl5Siuy9jLcY0NKbBWodzFuscxgXDcM7hvcU603mv+F3gEc4H428NMhiu6RlgMuT+Nfi4/H08T2vUPiy4qm7CQg0uKazQ8D1+EZxZuGGJdx6LRSVDiqHHC4mXvd/z4XsyuPaz9qV13nDeAGUbGoQIxpc8T/DC8bjCtymARyCEx2Paz4KLj0Yuk5fxED2PFBIRryMtLhENUMoYAdqQI9Ihu7Cawq8QCCFRKtybj2lAMI7OewohWkPuf5ZecvDYDmtj+BUSKcFaH8/s8F5EQwQhVHt+KXyXSaSFFM/hbPDG1jqMTcf33X0J2R4/fBIWWjhH+C5JqYjEieSQfWtvPjppKYKxpucZnoe7K82Jz9GL4Lfjc9RtPiWCQ085itIa73z78mV8v1mW4azHxSUoo2EIoeKLTZ+pYAjxv9ML677TeiEh43L2MZTE0CulAp+MVMWcLi4OL/DSg3BtuPdIvPB4XLymGGbjYhBCIJVGxWOkTEUKEby3AO+7HDG8pOANpZQxLIvo0VVrTCmMh1NJlNIolYWflTK83p4Btp6iZ4Rhgap4nPCSnfM47wlRvUtXugXQT9STEYYIkaJDY0zrRJJhgUvpXXtcIdJxowf0PhqXb4/to+V5EY0oRjOBQMR7T+FZpPepFN7Z8H6cD+8W0S5ynR60aL1MCFlSSHShqaomhEARXLtS4SFJEaw4/ayUwQDT35Oh0Dt2/wFKKeZWLELi4k3gCcacCgAR/p7KoLYI8qLN1YRUYH2qEoJR+2iwInmlZBDpYfcMUKqQn8yt2uTt0rXH47SLqMslvXfBI0qFUnq+2BD9++xsxsWczsZ8VRBSC+dcm8t6HwoIYu4thGrzTyG6CiEZjfceayzW2hi2W+ukVza2RUTwxsk4fZery/DsQt4Wy8iYxznhcdGTCamQXgC2tQlnQ7gV0cqzPKepmxiY572iTjbsBXgp2hCL99jGtCFKSImQgsa6NoQJBCrmaykn8UIG99om2xIfypDwEqNRqViEILvwqloDiw885VgyeVLRhde4OsOLzeL5LTgXwpTwwVPH8KqikSXDmTsHgmIwwFpLXdWtESdDFhKU0mgd89xo2FIqhBA4b9tQq3SGVLIN215IpFDtV/A/DmcNtqmDhyKlDbFSda5XsfqQ3cRCLoR82jRFhCBNKv2tqTHWYJwLxxUC78Nz9s7HsiUtoIgciOTZXFut+2Rs+F5uF8OwDx5RxPfupAQH1vvw/BHYmDcKHwoQm7xzuyh7IVjEC3Ux6VTSI1uPQwuTiJgn+lhUiLgyRB+eSblbW/KKLnzc5QlDuJXx12QvUeiOKWOeGaAM2R43pA1dihBWIkgfV621CJnCMSgpyTPdGp8Q4YXkWYa1jqqehVDVpgUSqSRaKlCgtQ7FVlyswTB7eaQKnincf8yhZKqE43k9wcN5sI3BGYP33WtxJDvyLTySqtqUtnSFj4jh0kXPGXK+YLy0nk+IZHwOjw/nnwPVUv7HXAHi44t3qRLH0ytF2uvBg/WhgGyfi+vCrPPg6iYlesEL9iA3TfylgL3JFloIMTyGHTkfdoL3U6FWSaVYrIS7BFm0ya2XMkA4vdJNxO/hfUfDEt3qVkr2DLb78rHIEHHtk7xt+/ZDviPjy3KEqlcJjQhXgfMOpbJQcba5l4/hXyCVRCqFVgotFUrHnE+I1pMLFa/FO5AqhJL4Ap3zLbSkVIRiEiRiLN4abGMxVrTHaL96L5megYTjMZcD0sPlnANjLNZ0+WSqmD3Jo/argWiErTFI+vmBmEPzuvDtU/SIIdsDzkfMroe1du8iFg8u2JNok6nWALu8IHk62av26H3vjK9L6lPhgZBkeUFT19H4aOGC/ovrjE+14S9VZb6FZ7qkuA2FQrYeNcExCSNMriet8B6I0Ms/Jd7HVRoNWUgV8yzfu++Qx4ZCIng9LVX0eKItspL3c/GtKkWAOKyLHl6jtUaprlBxzuGsQzQ1eIf1ErAx4wnHKvMCYwx1XaGVjsYczwld/hr9CV60xw0phQzuT3ikEME7+ZiviwCvBeuKgT9AHdHjdY435Ijh/TrnQ7QRtBBLes7WuTYf9wnv8ylKC9oDx+sKtYJuP9dznYy7uhj9UNq9yOBFECnvk9FgBHVdk0kZE3paGEbERB8ZKicvQxiPR415YTRA0SX9wgfYhNbgI+QhJTjf5pzhzdj2fB6PFXFFylCRi17V2i+G0gJN+KQWCqU1OsvItKbQUEhD7cpggAlw9yHMvvXeJ8l0xTdfeE8sREIofvPpl5nWy9zcPhmLCocxFuFqjh0acWt7n4ODgJN5F3I9KcDEqjWE7YhHinnckQh5xJQL69LCE3d1mHzI84RrQ2SKFinXFtFnpcjRHTvCbyLkht7ZNn90bS8kLMqEdqQiI+R+CSqLx4724GNtkRybvBvfeuMf0aZyqfCglw+KngdtK8O+J+l7VNHlhB0WJNtKmZQGkPK9ztN2Xrr3Waqw+1hmrDRb3EpKUDIitr2WYQhcnDt1ibXlvVg4CVSmybOcPM/JspL3veU7/M0P/za5lmQ6I8tydJZF76b5yKNf5G1nv9vz3pIi93zqI/8nZ4+93OZ0zjYcW3mVn/3Qb/Opj/0mH3v3FxiVe7Hy9q1XdfgI5ai2ApdvwBEjZhoxRO9o0QClFFr3IaLeM5776jkdedf7iaHUeY/1PuSWMQ/0MT/0/TZkix+LNnPtcv8+mhC8db91J+e9lI54W4BEhO9yEJfccntPqVUWc0iZ8jgFQkc4QsUHGzA7IUIWpmLZDx4n45eIIaHt2ElE7Mq4ZLAErC/AETp8T+AxAiclBrBCYqXESY0WofK1QuISXhnD8Wgw43/62/8rj51/HiEzlC5QWUExKBgMBpRFQZaB8SNUXqCzkqVRw6m1ayyPDtBKU2QNzmly1ZBnhkw7Pv6uP+bc8Rf48Uf/kFLvU1WOR848yT/5+f+eh04/w8adazi7Aw7On3yVX/nUr3L66M14LxovwjtQKkNHTFHEz60PaJPzAisENvQHkUqRZTlZVqB00T6Tdt3GtakUaCXCcWNUUEqBiAWXELiIxbpYSPgI6gslcTI8R0SEhIi5eoS3hJQ4Fb2kILQhI9oRMj7dpVcpB0yrqvNkXW4WVrB4g1Oc/0u/pxsMLlXHRHjHpzCcqt05TCrlgWKuk9Dmf72aMGJCEf8SvUpP4G2XOhTKc/boNY6v7fLtl99MmRtWRhNubx5uj1Wbkv3piDNHb1BkkgfueQ3vF3n4zAU29s5w8fpD3No5xdmjF8hzycroDn/9h36New8/z9OX3s2nv/EJBJ7jq5f5+//Zv2T7YIl//9Wf47nLD7M3G/P0q48yrTKMsawvXmdpuMOvfuXv8uSLb8d5S6Zq/vPH/4i3nX2BxdEEtRmRARcyWqUUjTGdd/Id0SI9swQF9YsS59xcKiWlwLn4/EU6gOj1QYJzMM5FzDF6QAHWR8DZ302ymO8Z+55x+IgihLDd4ZB+jtLRwwFlC6j2wdOA5wliGQ/YGBS9T90N37rs1K0IeKGcY5K0hU4MmRFcoJ/3dt0K2Z1bdMbq2yJItTfpEmYVjXBY1vgKbKN5/KEX+Ke/8Ou8dPUUL146x4ff/h1+7J1P8F//2j/COUWeNbz51OvkmeHk+m2OruzxSz/9v7MwqNHa4Z3kX332n7A+vs173/xnfO3Fv8pf/8C/xDnNb3/1l/i5H/6feeriuxDCcd/xl3j5ypv5oYf+jL94/kdQ0uKc4KmLDzOtQ2705MuP8tBz7+HvfOw3OXnoGp/5xkeBgsMr2xiruffodV7fOMNCucfW3iKzSmOdD16xDW2p9eYRMliJUiJU2QnYtjYUaD6lVgolFKPFBWbTKc6ZFlsLfVwTc1fRRp9UzAkfOkT4gCZ43wFhXRj2bas25Xu+fS+RjCB8eO+9XNTT64S00ImUb/BK/ZZa647nWjiirXoDjKBistthg64ty2Pe57uV4uIz1VmGMaZneKK3uqLxxXAsIpblYu9U65oPv/3Pec8D3+MbL57j9/78A5w/eY3l0T7HV+9wbGWDpy6eZ2YXKDLPZCZ4/K3f5lM/9mnwkrPHX0fIkt2DVSZVzmef/AR/7y/9jxxfvcKsLlHScmz1KicPXeQ3vvTLXL1zb0wGQnLwzRfez+e+/ZP8N4deJ5NTctUwqYZs7i2G7o7w7EzG/Npn/zZ/9Yc+zc9+6NNAxmee+MvsTJa4eOMsX37mI7zz/m/zM+//fV65fprf/NLPsLW/3Hq91GJrvX8s0HQW4CHnPc4YXJOgJR3KCyHAeUxtoqPRbX4XHGKCVWSAa2J+iQAFaA9Ih7PgReq6O2xM8xyh7+xTAZigGWd79DzZGrkj9uVTR0wIAubWIwokuEEQcgNkl8S2vxxDrejBJEIE8DZ1ShI1a+57xJJcAgKEwAK1MTEl7RZAV6x0/Wp6xUvnLeGDDz/Nxx77Jr/00/8PP/m+bzIoG77/2lmcl3z83d9kWg356fd+kQ88+DQLheCTH/4sf/79D/BvPv9JhvmM8cBya+c4e9MVLm08yM7+GvcffRbrxhRZxdr4OsZm/Pijv8XPPf7PGA+3yNSM5dEd9iZjDiYFxiqstUgxozI5k1mBc3Du5Av8D5/4FfLM8u++/Ld48cp5PvDQNzlxeIt7j1zhpRuPkA8GbM+O8dSr7+R9Dz3Jp37s9xmV8OA9l3jo3lfQOkMpHXrZKiPPC4rBgKwoEErjvMc4h42L0svATJJah1wciVYZWmexIyN7hBAVDVG03itFsVTEtGQQREuXa9+NEL0KOb6fVJQk793Dl/tOSwv6+F5XxcwVnqKPA6auRqRJxWo3hV7R/+/kvUKlEkkKod/Z9UMjLufizTiR8Oi4Erv8UMTWmeu3goC6zvijb/wwj973PFt7Y37xo/+RV6+d4Mb2Gk+/dj8fePh7/O5XP8bedMRbz7zMUxffxsJgwubeEaBgOJhw+vA1Zs2Y+5afZZDNuLlzmoXhDjuTw+zPllHS8MRLH+Hxh/8Dr908z6RaIM+mLAx2mdYlQtRIaWmMREpDphsyXeO9575jFzl56Brvf/AJhuWE+09c4OsvPM7icIpSju2De/ixt3+JDz/8RyhpWCgOGJYVZen51Ed/G+8FT118hG9feJgL1+5FSBEqXa2w1mFdTWMdoZETmDNKJs+TOJnRM1kTQyIRgA89ZakUIuKHQnik9zjhA+EDcDLSzXzLQAwBt6UOJYKB7BoE0fv2Ox8hBXNtnqpT31PFdpgTgYyK76pcKVIjXHbUJZFWgr6rvA89z7ao8IkZFUkKHkTMC1qWaPrysgVlwXWwD314JhAVveuSASkEr904xc2tdf7smbdx5uhVfvydX+dz334PX/juY3z0nd/gHfc/z7OX7ufxR77D4rjh5s5Rfv5Hf4uNnTWc1bzr/JM8f+VRMq1orObZ19+LtTlbe0f4t3/633Jl414OZkM+88TPsjK6yS//zD9kfzrkn/3eL3Pl9nGaxvGv/+gXuHL7ONN6xHdefBRjQ4fl9vYat7aO8Jff8xl2J8t8+ZmP84dPfJJ3n/sK03oJx5ifevdv8+LVB/ncd36aX/zIr3LxxsMcXTvgLfe8wPWtE5w9foWnXn0HWZmhdMAqjbGYqsE4i/MOIQmguRc4Lzv2cmqvuQCpBINLTGmDtSnCS7SWbUtPJaTCuza3a1MfL0KfN7WPY7sPEc7vvY8GLeb6Kin/n+8F9zA+EZnO9IoBmbybVB1Cl8pucVcFnFpzvR5nbD+0XQ6XSBypUQ5zIbdt4d1VePtU5fVwJu8MUkl2p0u8fusUx9c2+I0/+Qne+qYLCOl57fYJLlw/xfsefJrPf+eD/Nz6Z7nvxBX+3Zc/ycff9ce8dvMMt7ZPsl8d5dLt8zz92gdwfsATFz4Sr0Hy8rW3Y63BOYvOGt5z/otMm4IrG8d55cbJ4IWM4c+fewwhBAfViH/z+U9RmfAMn7jwbl7beIDF0ZTaLnNQH0bIBbxcYH+2gvM5l+/cz4P3fI9T669xYvV1Xr31Ft5y+jlu7x7hf/vCP+LSrZPUjaIsAsPaGItpDMaatvUnBGQEr9On3Ftnsc5GODD2wm3szES6mrUmOJ72Gfu5554Mz/ke97HHp/QxnDvnOnw4teAiHhhilotEFTlPRmjjskwtscgPjDmCVLrF+mQkCagesaAzQNXmaT6xo1oWSkcomOfehVBhhW/9XGt4vsf4EPQa7bLHTZNUZshTr7yFv/n47/Nbf/ZX+O9+4x+wvnyHOwdLfPG77+Tjj32LO/tH+cqzH8S6Ic+9/jaeu/xIwNukpNAarcM9KwnW6ZBmyPgSrUPJhr/7Y/+UY2uv8htf+jtc3jiF82B84EfKeI0WSQD7JTrLQBVszxbZN4pBkZPlGUopnr3yUb5/5XGqWvN/fOlXOLx0iRMrF/nQQ3+Azkree/6rfOfV9/H67ftprERnsePUNFTR83UE1xhKw/uPTf5gZMJLMAJrLUrpQH3DRE+XmAehULDeYWOoxYd5FeNsgLmEnCPAeiE73+b9PPU05VGya9O1MyHCz3vA5FCUUjH/kzgn5nHBlpAQKPhCiMgDE3OoemDVxPZaInzSkRu6kJsYub15ER8fZGJW9JsxsYBxCZeMzf0+w/qla29mZbTLw/de4D/8+UdwGJyDP33mMS5ef4hrW6f59T/5RRozDGzv2K5SKmS/smuUzg3ZpH6xsYovPPUT7Bws8NK1M9Q2egZiQTbXwgxsaSU1WZaRZYos0+R56KJIGQsxp5DS0bgxl+88xI3ts1y4+XYak/HqrQd54sJ7sBRkOnibMILgYu8/0PxTPiWED73n+PBcYiX72B+I5FrhDcaFhWNjiE6zNgnsTwbjehSstt0XK3GBI8tyfNMEBlI7m0KPcZ1acQJEdz0JQdR98NC3wzIdBteFxI5Cn8DhwPTQIf+4mxjg5wFH0YbXPqLcUdsT1iViVdVS3yMX0HnfYU4iQghShpcZi5fX75zhe689zPriPlJmWOtRwrM9Ocrsek6RaQ5mOg4GCVqSue9T23uG5z3OWRobQ5qH7158Z5ypCK/HJfa17ICjFA3yPA9zLpkmLzKyTKMz1Q4xWWNbtrRzDik9xpVs7J3EGMPvfO2/wDqLisbdNA11XQevpiQ6gtOBIxjflBSRTe0i18/jnehoYNZRG0Pd1JGw6iOZwbfwjHMhBDuf6FOy4wYKWhAbBMZ2ZITQ1JifTbEmkEZ8yvklMde/i5LvEFQ2NsETXBI9lQuATGil9ckFIpAWg5HIjogjWs5inJ6THZboUkVMZI7Ilogg7mLt9vu9Xa7oY1EUMEpkKJy8EjSN5F985u9ze3eIwdOkVFHGAacewSnNlEBs5svA+JAETiGxh2tM9AUxr7Ix9Pj4oGWkrIXb8+0iVUqFVpeSwftpRaZi2uIF3sZxzNb4uxUQPBIdQIyIQ0W2hUgi9NzNyLQcqzAFF1CCmM/FL2MtlWmojcE4H/mHsWCJOK4BjABjXTsOSiTFOhFJIGGGFXzIIZXXWCwmxAJE6009c22sQCCd46Vq70XoL/Z8lWqxNjHnHfutmH4gFT6NKnZ0UX8XnjdP6xIdNafXKekgod73Xp/Rt/Mlsd2nMpAC5wWN80gluLZ9gmnVYFzd5rC2Ba1pOwl+juLfm6dAtXSvQKeLHiXmRULqGIpsr4UZn4US8waodGt4mdSoSPC0ceKt9bI+TY8Fg7GRku9j8m5tpMUrhRJdU0v0xjIT4yZ8hTHVRMs31lLVDVVTUzc1xloaZ+fadqmj1HhoIr9MAlI48KG7ItLIgugPM7VNwRi8w7Fa0msbSbs0oT/2oH27Ctp2Y4uE85/kx4iW4ydjZatEpKdDD7jszXC0RIIY/pxHSWK+0RlC6gO3RhahnTTbkTBGKRVSZ8EDOo9pLEiPwdH4kJclSr2nK6xoq2vRzoNIwrCSVOE8tm3Cyxg+XDvFFfhztqWKybYnK6IBKg4t57zz/LiFrFquYcLiooG1FWcko7Y8POexCBA2VKvetnxi5xzWC/C2HTLy3uMjDd8aE9jR1oXfs+H4VWNorInVfKiMfZwHDs40phuRnWOdCzPBcewyjQfsTg1PvbrNtDaxHxeMRniBdKItLvvxxkcM1/dY8ul9a0+YYvKpEop5gKRL/NPEmnMu0uMDYzj5Lbv3Ou7g1bm+b91YmrpJ7KgQyhNTsTek7kJzLVZxom07JUNN8ygpqRYRgA4s4zS/HJNl72kag7OOQnbjpXiPUYoDHI2c9+rpBco4D2OdDWROsnZFytaQE5jrenlGn8oUjlUtlazeXonDRjGn8q71eN6H4fBkCCncW2tbbmDwjBbhdWh8JSJCKty87bUqY7vubo8W5z2kE5HA6zp4LOV+vQF2fOeFWmp/CqZxwcz8EG/PA3nkagosBokgE6GSDhOTacgrVsoyDZoT54hTDhj7xd08qGeO/NLnCvZGCftsZVttM2YjhNRoWZdvXmN7c4syT3OqHmMc1tj2d523eBlfsNAgRFA6iIm1cx6pJHmWYZow1Z/C93AwQGuFNUENIcsymrpuV3YxKNFK09Thd2SmwTfIMsM04eHuTycc1HUwKCSZVsyqOqL+Ok6DOfIiZzQoQ44nJLPpFKkD8cIYT103EGdIqlnF61Ly7JNlO8ZIL31JY5cgfqAMx7y8hUDHdCYdyXlQhOiRC4PHYTw0LmvlShKZ1wmFpImAfwya/z+R7Q2RrjcDncZOXbmGOXkWoctWwcHLQL5UscXqhe8QDlwcrQ35n3e+426mEOyIU0stwk3EeLoptkTN6tGlW7BRZxnHj5xgYWEBIQWmcbz6yusMipLRcIBWkv2d3ZbSmOk8sINjZZBYz846dKQoeuPIM0VW5rjGoBCUeYGzhny4wHi0QNM0eG0QeIpM4gpJNbNUxlGqAqUUzleUeUlReA6vrbBQ5pjKcmdrD98YpBc45VBa0xiDkRonFc7JNteyxqF0zsJoQFlk7G7tsndwwKAc4rRHy4r96RQhBGVR0pimnWzrD6GLHnTEXZIacws7OoyV0nJkMeP1LcdB3Q0aeODREwV/7bETZNrz+e9t8ZnnJ8io4JA89UJWcWI5Z3Pfsj01CBTOR36mnwea/9N/ZDdwn7I33xvYFzIkOMLjpQuhuJOViUYYxx4SyiE74+4G09suiKcHN7dVWBsQY9wX8UsJAUpy5PARVlZX0FpzZ3ObqqoY5iVLC0tU1SyC2B4tJVoFz6VlQIG0zsLIpzEtNJAXOXmZY72J+VVOJgV5mVOOx2ihKaUKSiJYcgV5rtjcmrC9O6HQikQI11qxvKA5c/IEGjjYm3BwMGNlacxinMmtmppJVcWBa4W1MqgJxLw1k5qFvGRpYYS2kumsQUjFIM/C/TjPZDphMBgidIbpyVz43t9lzI2d9Z2wTxL56eXeKwPJJ957mNUFwb/+kw32q25m13nPA0czzq5atIK3nsr43PNJByfia8Lx2L0L/LV3LLN1YHn6yj7PXZ1w8XbDxIpAfO2NPqVqfG42mO66UmNDpmga06Usy8hVwWQ6wbYiTa4l7oseCNcWpV7cTceSrWQCyDnWghBiblLujSs2NLsXFhZYXFxEa83FVy5R1zWLgyHDvKDaP2gH97Ms65QSgDIvyLKMykStkMje0FmGcwYVAaw8zxmVBYujEiEVCkmWFRhXMy4HLC8MIRds71/C0+CFibmkw9oZZ06cZX04YrrfsDk9YLy4wIKr0VG+YlZrDmYZha6onMOTURsXi4EwcVZXNcXqCmsrq2xt7TI9mFIs51hrybIMbTL29/fI8iBy9EbPFtIOJVX70j2eUnnuWxNYJM/fsAivMMYykA1Hy4wjY7i8081LF9rwpkw0zq4AACAASURBVLWMetrQaMuZ1Zz1geXWRMUq07OQC95//4iV3LJeeM6tL2HescKVTcuFG/t89cKMFzcarO1y4kRMhZS/0WKl0KUBPjGivKduGpq6G2qSnl6xFqU9nG8jpnN3efpOmqNPexdvyAU6Q+zCRuoN4rufaZqGq5dfD9R7KbHeRsWosGKkisYmZSvkk0DtsixbA7TWIr1EC433jrLMGAwKtNJIa1geD8E3aGFZXxiwMhoyyAbUswYvPI2p0UqTCUHuHeuLI5wz7BzMaJxnoBWLmeDo0oBDiwPGhWaYSYaFZpBnDMucXEuGw5JyMKQylqnx7M9q8nIQQGbvaJoZUkHd1OgsCwpephuv7HuTgO+JVgnBAcI5zh7K+C9/9AQ/cm6E8AYnPBNjubbTMCgkh8c6iBVFj7Q6lJxYH1GrgkmjWB1knFrL8QQ6lgcWc8FiDpMGLm1D7TU5jnuWLR+6f8h7zy2SteHX97pp0TOLN3rv9K9pEKodclK6tSEXZ73bicm5ORz/Bnk+nareVFA4EsvBveEBttNWvuvLWh+m39MB9/f3uX7jBlJInLNUVR1oQwgsAuODMpPWOpAWfdChsaYB53DGIRTkSgXNPOdjJ0EjpGdvd5sTh9dR3iBNxcmj6xxeWQwjjLVjWnmcK8jIObK8wvXZTVbGQwaDATdu32Fjd5dxkXF6tWQ8GKHyjK2tXZrZjCYTNF5RkFG5QFWSusAJTV0ZprOKvb09yiJjYVyyu7+JMTV5nqOUpGpMuI4EAs8VH7RaLta7qOrgEEaxOzEMqDlzaECZOSaNwVnPxm5FmY04e7hAPH/QikI9fHzIalbxzPWa27sVP3x+gbfdM+LZa1scWA3esbqgWBkobmzP+I2v3UIowX3HFnjH6THHV0tubu0zixQ44S1KOBwK7yUKg+0hxn0v7qGdbHTOt0P4LpEcCIppMSF4I4DX01PsWnGJNOi7cTvPGxPjdhq/l9sINy9QuLV1h92dXTwhbDWmASmCXJpW0X2L2HoKsE9qL3kXYBYVF0EgCzqKPENLyayaoqUPlKnphLXxiDedOEGRB+B4a2OfylistyyNhqwtDrh9wzMelpiZYfPmJsuDkjffc4Sjh4Zo7dnZ3qUuFKVy5MJTSEnjJU4pnCaQOlXOVGdU9YzJdMLenoq93QyHwBgTio9mn9ragCd63ogYeFAYBtJhrKJGYSVsTS07FayPBMdHjsY53nFmzGNvGiGd4MhYkkkwXpApOH2ooPbw4vUZ17Zr3n//AucOZawOYbofBtBHhULjuefQiJ/70AmeuLjDhRt7fPviFmvjkq1KgXNIJOPc88g9I165scfVg6zNS/0PqJo9adBI9sY6XY+LIPiB5Y1IuabshfVESI3QjA0laU/+LHg36ZljXRCnzII4UAeGCiG4fPl1JpMZWqqABboo2+AcmVJgA/akVUiCZ7Yhc7KnzqqCXoGpkd5Q5hnDXIP1+MYxXhxhraVQnnNnT7K+PkYIwayybGxdwdk6vKQThxhIiTAhx9vY3GFrc5sP/aXHuffoIpn21PUE21TYuma5UFijqZ3H2QiTKkGRB75jIRW19OxPdhDeMhwuUJYlu5PgmZSGhcEIM6ujAoHoIf+BA+k9PHhM8xPvOMJr1/f53LN7bFSOaePZPLAcGQr+qx89xco4Z6w8TexqHFocsFRKNqaCQyM4dzjDCcH6QuhrV7Xg0ILn1GrO1V0DQrFx4LhdSU6WljetKx48coTKHuHiRsPnvneHp281CCFYLi1/47F1Pnh2yMXNRf75565yZ5a3IwACi4s5YRcDg2qZjLxNLyQ6RlIfpdpCTSGjncZOjw/5o/eq0/fxvSrYeYF1nX6JT2pN9Hli3c8nqnySz3TOc+XyrTAj4IOHq+vQQCdKu6VKsD/FleZmERbnmmjMRFXRwKQ2pkZIR1FkNNWUkyeOsb6+QpFJhmVOMRogtSJHcmxlmZOH1hhmEpUJGinZ3N1jZW3I8aPLlIOSrCyQWUamMnKVMchyBlqhRIVjSlXPsNaAMEjlKQpJUeQ0tWdvMmVSzVBZjlKKuqmp65qmaciLojfdl/DTwCscaMdPvW2Nhw45PvLgAh+8b4ByUFvF5Y0pUlmOj2GUWSrnubVn2ZoZxkPBoXHIhU8sS9YGwbu95+wCP/7wMsNCsDCAB44XsQfsee1Ozb/92m2+8PwBr297Jl6SS8+5VXjPfUuR5uZ59J4Bj57UFMpw37rgrzyyzpJuekJSAWq+S+Uv0PGkQkfNG610K73i6Wh5iUzS8gN7wk8tI9rFsroVpYksaKLuh3PgZIdPudRXFp3QoBSwt7vPtWs3QUiKokQrSVVV7UXneU7TNGgpaWJu2Ar8RJ0YYw0yyptlKMq8COeJnDvvasZFzolDa5SZosjCrEQtoGlqykzz5rP3srJQUrrgDW9vbjPJNG8/f4KyyPAEKrvwEi0FWSYoCk1Ra0aFZmIarKmxIsdYh1AOXWoKkTFRmtoa9iYHLCwsUJZD6jpQpKw1ra6Nl7QVn8RSaPipR8Y8dKygcY5SGn7ybYu8tu347pUZr2w2zKzEN4qvvrjL11/e5+qdmk+8/xAfOuc4s17y0o19HjxZsjz0WB+o7XiB9ZKBEDx6YsC/z/aZRlHKZ69VPHd1RqG3OHMo44P3LfCjb11hkNVoGo4tZfzMo6ssD+DFm5Zjy5LHHxyBhN/61jaNl+BSltZ5QCkCAURGUE8iMNa0qVySYQ4cDdcxqmI7kh4fUPrIZ7M+KAS7HoEgVcypzeV78g/9+VQfGRm3b99mNpsgBBR5EV+Kpa4btFQUWR4p43GmoyVwuCgLFxP2yEwJfDqJt55MKfIsx85qDq8sU0jQSSJYK4Q12GrC4dUxa+MBwlmWFhdZHo2Y7h1QNw3DckDdNDgsUkCRZ5TDkrxQ6FyRZwqNRnuJaQzeB1C9thapdUD7lYoyGw113aB0jpC67WE3TXNX4hQqw0dOKD54bkxtHV95bpNLGw0LheStJzJy7djac9Q13Nmr+ewzOzx327Njc67sWpyDwyPJWFvOHl7Ao/n0d3b5V1/c4F98cYNf+8ptbk0Ui8OcEyuK5YHkvedGHF+WZNJTWckLtw2v3A793c2DhiKX/PhbljlUOqzzfPrJ63z5pX1KLXj8gUXed09G1uJ5bj6X66kn+Jb5Ilstmf4gU2IMSKl6ZJAeG6absRWtsEzggYlWOixVu30yoVJBqDwYZqALXXzlFax1DMpBxHxsAJebhtHCItrDIC84MGFE0DpLVuTQeJRWNE0dLzRcdpZlwejiIA3WsTwYcmhpkUGZMSwzRoMcpTOmdc04lxy55xCrwwysQGQ5bzpxiiu3djmYTMJsszWYxlOUBSDRWR4UBfIcpWp0ptGZxfsZAs+smqGwFMVwTubWGEtVVZTFAK0008m01Ztx1sZx1hAOTyw4PvnuQ6yU8NULB3zhxV3+wakxDgOixLk97uwbtqeWQSkY54obEW+7tDFlrx5zbLXg7JGMew9l7NaW//fFCTf3QtsrFw0/9EDNO+8Z8sjJIZc2Z/y9Hz6CqeHizQMu3J7w4uUZZw9LprXl0lbFj9w/5INnc6TWSAyPP3yY3/nGddYWFO84NeBvvfcIO2aDpy7X8/rPcY42ye0l4C+03RPpwkELvUVmk7ddb1/MGeCcacdRy46EkIgJrTG6JJiY1JBCJVvNam7d3EDJnEIPMLXBGxfaXQgGZRllPzxaKhoh0FIgpQclUFLQEEZEU1UkhafUktoLKgE0hpXxIlmWI2SGEDlSZ2R5TpkbFvOClZUVRsuLFDpDeDh2ZIn11SEXb21xbWMzjIQ6j20aJA4tJIOyJM+yACaLKlDYXVSXspZ6ViOVwvkaj0GI4AXrqgkwUySgmiZidT4M/ggEY2n5+MOrrA0FmxP4wtMbfPDcGidWFLuV4OsvXsc62J4Jru40vPVkweHFjBc3agSOOweSg8pyeFxycn2Bmzs11+4YticO2p0DNE+8NkVrCVpxeLkIQL2e8eCxjAeOLfPhBxUZwSGIxvDhR45SZI5XblccW1E8cixj+6F1/vDJDZYGp1gdW/DxK8oj93W4RRImcmH4KI1ftg2NHsFXtjBUZJ73aoDQipOd2qZEoIWILNyAC9refhc6/nuqdIiT85ubm9y5cycar8TZBhfZHUVRhPzPNFSziiwCthDyJi9gZhqMtwyzAucbtDcUsqRUGa4xHMxqRlKwujJEZwJdZORakxclWZaxNJacPXMP02rK2tISeZG3D+H8ffdyaXOTi9dusrl5wMkjywglkYk8JiWDvGSYG3JVoaVp1d2tdVgB0+kB1jX4KMZIjBSz2awFz5vGhEUpQu4kvOfeYxmPnCpxeJ55fYfzxwd85M0LTGeO331imwtbILxiZj3Xdx3vG2juXc356sUGCdyZSv7w+zOu3bnF63c8n396CysUTRxvTJ2rLzy3z5eeu4NFUWq4unHAQ/eMeeuJEYcXJOOsQivB7T3Ju8+ucGzBcHHD8qufu8qjZxb51A+t8ZG3jLm9X/HrX73G6SNjnr0e0wnvespbndiRiUym1H8WSRRThsk6gW+Z4mlqTiG78e9kgDKNXnrRDShF7r+KbjRR7FPHRPT4dSC4vbHBbDYLunqZwpqA9WmtGZRFK5pd1VXbSdFRyanxjqqp8VJgnEW4Bi1gGNtvVT3DVBUrR9ZYWRoyKDPKIkMXmrIISlZlnnPm3tO88srLlEoFcDjXmMpw8vA6a2XJrY1tXnr9KkcOr7JQlGRYcA3WNhRlTlnkZFqTRV2/Vq4YhzWd8KQ1rhW0bJomFh6aLPfUddXS/Z1wXN70PHd9xgfPKt5+z5C3n1aUmeCrL9Z869VpEEwi8PuubTc4EQSQhJiAFExq+MoL+xHI74R9As02YGo2zufUlAhg0ni+d83z/PUd/nSwxak1zSP3LnL/kQEvXp9werXAecHrt6fcmkr+9KUpDxyf8t4zQz768BpXdjb42ot7VF4gUT12C51qbV/TJ1bFqShKLTYZVWtFrMx97LuFPnj0gFIIMqE6zWbRTbbLKIVBzxMqAmGzL78LgmvXrmOtRassiC/a8ICKXFNojTEVtWlw3lM1daugYK0JnjVt1eDCyGAxHLCytMzS0og7B5sMMrj32EkOr66zvFRSLuSUg4wiE2gFQmlOHD/M4ngQJNKGJdbDsBjgjOGd972JP/729/jyE99labzMRz74GAuFgKUlqoMDrJVUlaEoSvLMoIUgV4r9qB8ifJihVXisiBqH3gc5Ee+DYmrUJHTeItEIL9iawu88scWxpcOcXw8QwtXthv/7idvsNDpQlUJWzQs3av6X/3iFi7eqVhMG72hi2JJJNFIEbC4MhhGuJ8lxRvIqQlLhuTrJuDpxfPO1O+QqzAP/9NvXOX9kgfc/sMp3rsz4zuuG3/yLbZDB+1YGmiRoGUiF+P6EULsBUUJLAp/Ut8ypQLlSIvDrpYjSwb2OUDfTTRTwjmLarTgNob3iY8ntkuwC3YBLqpHq2rB5Z7Pl6jVNE7AhnVGWAxCCg4NJCMne03iLwTOtK6qInwkXWzg2mHxZFEilqK3BNYal4QKLy0sMlhcZry4zXlig1BpTz/C2RviGTFqWxgWjgUTLBkGFwrC2OubMqSOcO3Gcazdu8sW/+Au++8IrbM4EU4ZUagyDNUSxGOnv3Qi1tRXeG5QwYC3SyhaTTf3vTlRS9bSvfZyPDWH0D767zbXd8Du705qpEa26gxNhUH/jwPPEpZrN6d16jR1xQeBbhQjiWBRRBxvhI4ublgWeFoWVmpnPmRjN55/Z4uJGQyEbfvbdRzh7WHBn5vm9b93ij5/Z48WbJjARfSdW3vaIE0VVChxyboBMtszvHnO9Hc+9e9eV8Ec9+p7H//GVbYUxAYtL4kAuKd7LREtPI5kdwzXRtartS+zdeL7FelSkXmVFhve2ra6VztrtrIwL8wpKipZVrOIouNaCYZlja8PO3j7T6ZS8KHACytEQpUvG42WGZYnA4F0YGnfOJk53R1NvKnAOpRULC4tURnLt1h2eff5lvv/iy7z0yqs898IFXrlyncvXb3Nje5cDW7N1MEHpjEk1BWBQZGGKTea46Bl8K9wdijWldJyX7Wk4R29wc8ewfVDx4KkFjo8zRAav3apDZS0EHjVH8r2bG9jl9R4nFMp7Do8Fx5YCMUA4G/uwUCjLiaWQZ09M6tWnSlYwtY6LtyrOHhlxZhnOHx/z9KVdru57Xrnd0HgRZDncG/eR89kIs/4QXoX3kdqzaeMimVS2ZOI9ul5joWN1v//he6juvPZ99Y73PP6PL29rGmPb2YV25yJk+5mK8xiqp7kso7HZvSvMNi/EbRBCdSuURGeapqoo8iJMs0lJHfcrCx5Sk+m8ZQELiHxBSSE1GCiykvF4xMJoEedE8LZbOxwc7KGVZHV5HHWMXau2EJhCrpUsEwiqacXW5g6zKvAQfaRXTfanzCY19bQG6zmYTdmbzdibNWiVsz+bIYBBUXAwnWEQNKbTvTPO4pzBR4xQtipT3YaGwgc49taeZWWoObOWcXipYGu/4bUtFzsF84M4LbO5LxovOnZdJi0/9MASH39kmfvXMw4vDTixmnPfIc3bT494y+llXr19wP40kUN7kzBCsje13Nqd8a6zS6yWIPMRL1yb0cR2rLBJkuOuXnC+gF1/C1IP2h2S0neRBEZFDx+MW6T12czee9738CmqO699X/f1AlU7XB7yCpfkNKIiUhrRTHt/+HbDwU4UXAqwzqL7vEHRwTceyPMi6JsoFTslkQcIgZDgJBkZZ46fYnV5zGBYgrNMJvsob7HVhI1re/iDTdzsBIvLY/IsANWBd2iiFkqcEDMO0xgy7zlUalaOHaYcDBFSBg9nHIcOr7OyuMi1mzf4xrPPYZ2MG7iIdmGJOA/iY86TlCJcHGew1gYVKzTWmbk91xCaysGnv7OJlqt8+PwCP/Puwzx94zp3Dtyc9vPdgpNzAp0+TO4cGQseOZWxPrAcXyh462mNsSC9wQrNH3xri1vbDUS5vNYOIkXeIPje1Zr/62u3+Nl3H+HyxjaNc3gRGwXSI5yYI1QkUSrdRrmEH1u8j+yYllMYheRjtZ52p5Kia+X1tGHu2hWyJ2Lge73gToo5bRzTl9LyrdB3EiUyxiG0ROdZOyY4Gg5DeI+D1gFDE+0gjiQovi+MRhSDjGFZsLywRFmUCGnIS7BuRjPZxNYTbt++TWMqhoOSYlAwWhiiovi8s56msRwcTKjrGeOFBaQcRb6aDILjWjIohwwXhoBjMjtgWOS4rVkYcMKH3TURFFmObRwZYfND7TuEwMbCK8vCRjVJoCf1koQLhcJGJfnjZ7Y5d2zAwcxTVxWCrDW0vgekt6umn9PHsdx3bMzhpQKkAeGQvkEhqY3ge9emfP3VPWoHNs7qdqMmSeIuyCV86+IM47b4/uUDGieTTHlP35l5TmBckEqquAmNa4lXwTHR9qODQHonLdWJoXchXSeVg7lN+OIUmvACK7sqWEayoU3znqKnbh+n5pQKbT3vBaY2aCmoKkOeF3EkMOJrER9st021Bl/P0MJR5CVSCZq6RuscmQ/wKqMoR4yXBhSFwjVHONi7xf7uJju7e1hvQDoGw4JcZRgclWmomoamERinycZjltaWsU4wm02RSlEWJbnSeAxmtkehPIuDElNvMpEVzhmEKGnqmjzPmNaz0AuNaqG51mGmJlb47X4jiYCblOLTllooLm17fu3Lt5nVhgOTdgx4I/9yzgv2wm+Zw6DM+INv3eZNh4csDzRSOLamjidfnfDSzSmTRkdszsetwkQ3whliFQjBxHn+4qVtvNK9+V1x1z4k8yJRQYKZu/5dRQkOP7djppSBNSM9PSX+br8T/YMmodL+yigRQlHcHFCJIEjtexqraXefdisBBN5ZjPVxpyAdSKvW0tR18JxZEP7xzjGrA2CdYSgUlHnOoCyDQI6UXN3a4sKtDXYmezg84/GYw8tj3nTqCMvjRYqxw9WzsPdEb7YhhczGWCYzQ+MznMy5unGHV65cYWPzDlIqhkXJ4mjE4ZUlFguJrCuyLMfhMbZBEqfyqilKh8TeWYfwDp1pqqaZY3wEjqPv7XYk7jKgkDO9etuENCe+grTX3HzlS7cvcfvCLfeslly5M+XZyxO++cqMTAVvUxnBtBFzG/60vM27BqFEu60COJnNaTynQff+CMYcHzBtKNmOcXZbtbVhmCRETzTAJEoZ6W53ixOlg7VzsinMxsHuVJykUWwtVJQJSQULcb9eF9jUnug9HBaJEhlCKGbVDDurMNZgrSdTmlwKnLDIskDnwRvd2t/m0u2bTGYTUBkyz5nNKoo8Z2lYcOnqIc6ePsmbz99LUVQo7VC6IJMZxaAEI8FIDkSN15advQkvPP8yr97aIhsu8OrVS1TGYI3AYyh1zspozKHFhZDf5QrjHBJP1VTUdUMpJEqEHLAY5CBsKEJiypI0X6SSzOr6DXR80VMVDZszio7QOWd8/T2CxFzyPsrhnrUhX3sxdD0mBnzT8SmFFEjv53SYux3v+5GONsdyws9pkvmerMccrSxR8tOOuSoUpYl+532nrBvy/Sh15EPLWLQba8m+BwzME5eSx6RoH3dHl1EKwgmPS7tmRi06kFgfQzLdjaX9ahNQ2zQN1gQGSu1s2DxaBuEaYSxlmZPJjFFeMiw11fSA/f0JjbHMphMWllY5tH6ImzduUeYF5++7n9NHDvH6xQtkeN7x0H0IPwUvyfMhZTmkmkwwdobOFUtLizzz8ivs7Gxw8vARzr3lYTIheOW118PsbSbZ3tnkyu4uOwdDlsqSzBu0CkVXdbAbWR4aISxFmaG1oGqivEVstGsddkdqTNPOPaToELA6H4BiJ5DKxqlf5pTmBXcpnSWOelQgOLRYsD+tmRru0pRxbQ/fiU6V6o2htJcPChhoD95QuSyp97X7/YZNviT9TS7TWKaNekF435Pdcz1jjzJ8kdWelMek6PSpextW93Em5sr+TIbdz1udj3YrVtUKFyY3L32326WUUXbXhWLAOBu0qFVGXU8xxiCR1LZhWjmGCwOWRwscWRoxPHqULC/IioKt3W0q6xD5kHuOHmVpcczh9RXWFka8+ex9mGpCnhUMVejR6CxHi4yDJgj5lMOSYT7msUffwX333sfm9gEH2xucO3WSk4cOB/zQVihvKJRgfW0Jbw07W7vs7Rxw7cYtrIeZgM39fZQIibeSeZDJVTN8EyJGWZahPWeaTtTHR2UC6RhIyZElOL2e8cjJNb52YZ9vXW7m52jv2ge4NS8hkDhOr2ds7U44s5aDFFSVY+ugZmp1Oy6RFBjEXWMVaUer/gD8jz6wwCOnF/n1r15npwpOYlo56sgzRPg5WLLdJy9iobLtiEWVsCQzFel2qsWL4x7PSfqyHR8VxM382n0TQ76sRBfDBQGSoFOqsoJu4z4RTqTT2J2Q2LQaEqE1bqZXlAWZNoGy5BwWx8yYYER5zmJRcGhtGa01VV2xfPQw5XhEVo7J8gEL4xGjUcFQh6208CO0b3CzbYpBERkaoX0kdUamJYNRybnRKttrB2zu7DFrDMbETZ2bGd5UmNkBwtUsjEoypZitL7O7tcfRhZL9ScXl7V2stRw0LrBeXJh+S54+7Z5U1zU2qv2HNNqyWMK5Y0MePDrgwRMZq8OcUsHN7SlPXG6ipxH8IEke3xO4XR9JHjm9yI2dnI09ixKaew8V7O5P+OKz29yZhvI/pMHurs0I6Q1G0XrNU2uSB4+XrAwFjz94mPWx48LNfb5/uebqdvCMiWafNvy2Yn4rtxQNWyw2ae7EzVNlYsLEKco5Nkyn+dzbrCZNr/tEN0j7u4nO5ca2T7dbJR0TBIWIQDAi7tQYO1yz6ZQsCxhgY2NP2EMuFQOVMcpzFheGLK+toJTA1DOM9+jcUxSO8UgyHuWMF8dkmaSpJtT7hqbRcTpNtateS4HUklxBOQiA9qFDa0ymM6azmsaE7ol3BttMEd4wyDVmNmGbLUSTYasBmXTszAr2a4ulZmI9VdO0ejfO+0DXbxomk0kbfsa54RPvWuPcsZyj4xyNx9qG7WlNOcy5/8gAxT5O6J6c2htHORGS1ULwsbet86fP3OLlGxW1USAdD53M+IX3H+PUasbvfv02l3ZlKzbe12C8W/YDIBOGEytDtvamLBWex+8vWBzC+8+MuPOQ409e3OV3n9wMhafQcXxW4qXCybSBtW/3BBaRCSOjIICSAhEHzUSsgqWQcY/SuxVSobe7UVfVSN9pS6XNljtS2P/H13sGW3adZ3rPCnvvk+65oXNO6G6gG42gJkASJMCgwCRKQ1GUREn2qKyZHzM/7LEke+xyTdVYHnvsUc24pqyyx0EaWSPJI5nksCRRAiMoAszIQCN1Que+t2++J+ywgn+stfc5DarMqi6Sjb6N7nvXXev73u/9nnfyLTUB2UQWSB0RzwR+UyN561mxkPXgzOF9RYInVZ5WIpjtteh2O7S7HWSaIJMQINhutWhlCVII8jynHFbYqmjyi7WIkG3vMdaRoWPnlSNVQqfdptUOy+Rhbhq8fc4U2LKgyoeMnI0OIUeqRBCXtY4TFktRjEBlofLVEu3jglWeR1FdYbwjkYJHjvToJI47Q4etDDv6iq+9dof33buNhV5GKh3jaZqUmMZ50yBRhBZcXSl5/daYymu8UighWN4SjIuSk7s0jx7pcvX5YajBBU3m81014NRZ7KaKXf0WN1bHzLQSEul5/UZBN0vYv03z4P42n/uhJyiMPmq3ZpL5K4JYXdMQAiVNoqRA4Zo4NuGmfDNC3AWYvwvRS71tV4dLM6E+heYDhFJNXHsdCCXxEXhU15MRkCgmsddCyAmPTkCAUImQaukthcmR0tJJBS0t6GSKmW5Gr99BZ4GekChNkiZ4pahM/bMIIQAAIABJREFUFQpaazC2IlXBGu+js7tO6rEu/DmSRKN0Ajpky9WwdBuXZzApvmoxxFIOBKn2JMqFWbULn7xECLw1IV8tSahKEzpBGaBG0hPS0qNreCuHr7y2xcpmzmu3xhzdkfAPPrQH5yWX7pQ8enSW+Y5mvBWTPaeoUfWV4L1BCMnqoOSb5wqcTCPGOPAKuxm0UklLeh441OPPXx5QNbILU93sZBYccGmOnX1NS3vevjNk73yLNEv4w+9coywM//1njjLMLaa+dvzUTkedYSyCoVg5h9QSIVyMuRVIF0lfMjpovIv0fn9XgKGexL9PAqWliE7rWgeSxB/TYxQZaz8ZrUNMvitwMdTSTpD9MdbVWt+YD1xEN0THHdabcGU7C86gcIEqKkWIpEpkoH7F4rosSvLxOAjWrQQnNBYdx4BEEldJkZYIMUYnju5sG5Ukk2hXqSMRLCRVeu/AxT+Hr0nyDmcCyChTms6OPpt5yagKMabW2oZ/Z0w4oFIHF/cXX1jFGEkpJUqG1cwDOzKWNnOUmGVXX3Fzy/6tQMYajRHMsQ5UqMfqYYLznp09STsJv66TSBIlKEx4udw7ROR6+av2Me2eSUmU4/ZqwQNHZxnljpUh7Oi3kFpxba3Ae3UXs7uGhwavX3CtJ8qHckfVUy0XS7aIUaiRoGIS3VX/dXXdeNQJbDJahJrmWPiYDzzFFZVhPKWjScHFP6CNw2jpmTQDEpyZIsg6H8OfHc5apPW0pKKdprFslmEBxVi8Dc6ZVIXdDYfAloaqLNjcGDDYGmCr2NAgkEmGyGYQwiLGOc6PwIansagMxno6gzEzswu0Oh2yrIUSSTQPQBXgfzhjMGVIH/fGhQBpa3De0O+16PS6bI5zjI87rNbdxRoUadBHpYUcifBhP2R5YFkbOQ4stFlcWce5iqO7Orx0a/COnjfOI3zB4QXFe+6ZYWXg+MHFIVtVDA90HkXFg/tn6eipCDUfSqBaZpkI0RMybW0Y2TffpSwtO/uKe3akLG2VbBaWs9v64Dw314omqqNmxQgPaayv8TLmQ2uUDPl1riavRqjl3agD7tInmyZEiakDWIcc14dpOmFRTiDj3rtIF5XNCEaoSVckYOr5FjEgRaK0whgD2CAL+LDGmKYZUgWch6kqiqIgL3LSokCnKVKGXIyyqsiLIaNhsMJLn+Cqirwc0PZtlAp/+azdIh/HLTylKIzBecHGxgbD4Yh+f5as3aLV7gX0rbdU+QgXD7S1NtBGnY1/9mC8aLUyTGUaYb52jFhr3+FmCTZ2IxRSGDoC5tuGvCjZP5/SOzNPpgTtVKPi4fdNcR4OmASeuG+Wx4+3GIwVK5uWF24WDW92vis5srPVJB8NCk/pYu03JT5LISfdb1yhkMrTaiVk2vP4fQt0U8drN0skiv3zKaPcsLhl7sK91V/STitDZSlYG9QT6fHYuw6frckWk/XJxgMwrX3qEO3uG7acFBMy/eQWbF7XSS709IGdwnZM1PIYZY8E4UJKoxeoJNRqpqqQSpIlgu29Dv0sQ3owxpPnBfloFHU2jbWGMhvFFc8S6zzWWIyUaB2eeDMYMMRj8xzExIWbZu3gvM5zyqJEqYSt4SbDrVV6M306nQ5aSxIRdpXzzS1cGYRza4IVHydIdUaaxbVV4claLcq1LYx1UXObFPzOhY/xwtHTnrMHOjx+T4ujO1vMpJpbGwVJmvDmkuO5S+uhIJ7kRobbRgR66MH5hARJS1tamWjs8YnyPHJ0lvlOKGWMV7x6bZOq5lp7NwWVkkgsXqh4G4Zn/Vuv3WHv7Hb2zyUMbMLLNzZJE8nuWc1m5Vkee4RSYO3dNlJn0ZKGC+2FiGctLmjKgP1wIjisAojKRljlBA88heYQjZGwPlx+qjFpGMdiip0/5cqvKQZNNNR0QrZ3hExpi7GGyhgy3SHRHaTynDi0n+1pIFVR5JSloSgqimFBqlJyPcC5Cttu45yjKHK8FwzGBbkpEViqYYEoDMNRTjsfo5MWzhmKvKAyoTvLMkWWZWzb3kcKwfLyElVZYopZUq1ItMQVJeV4SFUU8fA5vAWBRiUCaYOOOcxHDLa2MKWdoGwbBru8y3nS0Y4P3zfLg/tSKic5dyvnr15YBalZ3yrQmefhQx2evzIME6VIOpUIrAjLUcoL3lp0vL1UxS1Ex0JP8vD+Nsp7BIqbm/Di9TE+7l3Lqf3HgNeIrw2eRFp+/L45VjZy/o9vLrJnNqHf1bxxvaTf1sy1U1aHhlFZW6Em3bloaBUCpGuI+A3+xkOWpRG3NwmoaYwQkaw/yQkRgaEX1tMn4zcn3ETRriMImigC3/x/NbXKLqWM0QvTxoZ4wLWm0+1y7Pi9JEguXXyLVppSjXMOH7mXxSuX0NZQjQtyJRmk43irWJIqZTQahJ1eY9FZxnhUsrS4xOKtJYbDnE6nQ6edMj/XQwrP5voWG2sD8rKg1085cuwwh4/dQ5amaJ0wHG6xsrLMaDii08rItARjqYqccjyOqA2DFSAyjfKgKsXs3Cwb1zfJsowkLRiOioZ9I8RkAB+Cnz2j0nFhqeDWSskbtwa8dtuwVrpgqZoT/KOf2MtCV/NvvrHM05fGVPHzLvA4p3hzqUIKwR8+fYuVMkEAmXR8+L559m/TGOHZKuDzz65weTnkonih3jFT1vFyMKRa8InTs/zCexbYGBl+96uLvHq7xPjw9ezh+dob6xROMywIKdHi7qDpOrlKEgv+ukmKGcTD4bjGbdSM82hgiRgO5N01oPbUhvA4950OLg46jqwTMgVT0TWRlDnl1p3OHK4lF3B0enN88lOf5urV6zz3vafZ2lgl1Smi2+XNS5eohgN2ddvhuS3L8ARLB8KgbYJMJMZa2p0u3nkSnTA3v0BeGlQypChyNjY3cFWBEgFeuXPvXvpzfWZm2mzfscDsTB/hDNJ7et0eW4NN1laXKdKUTGdhZFaFHZSy9FgnQWmkC9Ckza0t9hzew7F7DlO8fYstJ6nEFltbw+Z18F5OjLneU1jFX7+yGebaVjfZyQttwccemmPXjATv+NmH+txayzm3ZlFeN2FBz709pJsmSKlQztCSnocOtnn38Q6phKUty9fOrXPuZj5JJ2iS3uIrFMeHeLh/b8KHTs8ibEk/k3z8wTlWvrPIrS2HRTEsKp56Y4SULYyTjSYcMk2YBPHUZU7NiXQRFu+nV2piylRNWb0rtjAeQOkh8R4d6eYBizGJ26ztQPUSSpP4UvOBhQosOT+1fCxqdlz8VHjP+z7wYWRrhu98+xmq0QbOeUb5iBtVweLaGvOdjG39Q8FWVJUMRx5PhREVyih0lpC12k2EQ7udkaYtOjNdinFJkZeYMmeu32NmdoZ2u4NOdNCp8CQqIN68M+AcqdZsm99GOS7ZWt9iRNTxXKDYlyagStChbipcwcY45+riCgvbtrGyOWRlfYO9ew+xe9de3jr/OkuLi3fLKXGxfy2fPM3Kg1Sen394hieOtrh8a8i1TccTJ2f4tcd38D/+1U3WS8ItJixXVyu+fG6dsye2kwjJ7jnBntmUy3cqLi+O+N6FActbHiNVsNLLMBOfBPvErT4ku2ccv/K+XWzraD7//CJ75lLefaxPZXfxv35zldx4IKUSKTK+ZLXk1WiJflKOuUj2Ny4IF5ODSEO+F3VWnGRSp0wFWeqJpOym6EZTYXPNthWUk/Z2QlPzTDliadzQokF7OJIkodfJ+OaX/xptxngl8C6maDswKAoPXkukDmuNxodIKVkWKJWgWmmMSghDdSk1UgqSVNNr9xEyQQpPqiVJmoQFJQGJVAhX4awPNZUMnBEfO7iZfh9jFOPhMOaWmLADXCd7A946nJYcPHGC81eu89zrb7O6vklpHZcuXkapFr/4S7/I5z/3Z9y4udzUzxNQd30DhGfu7H7N+4930dIilOaZ12+xf3ubY7sTfurULF98ZY3K6QhxT7izafnO66v0Oop2qnHGsDGsGJRQetmkU4pGLpk4m30MsO7oip9+cAd7e4JXrgz5y5c22D2rOL6nx7sOt3jPoZRnLheYOH5lyr94tyXLN42Wd7aJBKuvvVo58U3zE5vgeIOK2ihRv5hO+LgW6JA4lPNoH3a0dNQH1dQfxXrRBNo5H6jrddtSfwKEd3HDTYFXdDsz3Lh6haVbb2OFQKmMNEnJkhZaarQAYSusK9GpDjsjMkGqBKlbaNlGiRTnBMb6oA+OPfk6jBcFxU2olhRuo40ddKgGKVvLjrVbJRuLORsrFcONkvFGST60jIqKUZVTuAInoDfXpdPv0uq2UJnEJ+HGF0ohlMcYS54bzl24xOsXrrC0uknIxZFYM+bc6y9ROfj5z3w6dPvO/yigMkozp3ZLfuXRHVQWLq56jmxr8dGHdvFn377B7XXLh07Pc3R7GxnzQsKXVTOsFLfWHZcWC95esazmkmKKszdZe5xYGERtSnCOD5zo8/iJFjfWCv7k+3cYm5QLq5p//907rOaWzzy6gx/bn2K9a5LsnfdTisZkNFgnANT3UyCCCFIlSbUiTULkhdaTPZe7MKaTyNNQAzaxT96j6mJyym5Ws3+jS3ASXliHGkbBs95Aq4GXAWSoUbLD5uZgas8hLCFZYTFxGhKE7PC3kd6jE0naSshaYT/YEzI5xsOcjcUNBks5fixRLkM6RdrqknQy2p2Ayy29pTIlphhRmTFSVMjUo7oJ7ZkWnfkuKkspXWio2r0WqqpwboQwDiEcUiZUTlBiubO+zs3FZbzQcb0wTsUlSAfPP/cKv/BLnyJLZbyF1F0TCBnjXB87McuuWclXX9ngmfOb/MOf2s/D+9u8fLXFV15eZf/OLkVR8pEzs5y/PeDSnRInksanJ6J/U9SKrhRNIpOb2t0QwiCx7OwnUT0oqSwkStBvC9x6OKjPXR1xcCHl7zw8x3tOzPCDK3dCJtw0EYtpTVGQaI3UYW/Y1amc3k6vU1MPQZz3U1rglCMbMXmCNS5aI11zdaoI2HEInFcBrh2t2EKEaUYa7kGMcMiaH6hCEJ+QHu9N3JIbk+gFVP28+wlr2jkbdoaFDXWCjLm7CnSiSLMMlMIZy2g05ubV2wxXCtoqpTvTozs/j1YeleU4CopRhSkrWtvnULMthJqhJWcYbmwyzEdcvnCVzdV1uolk177t3PfgCWbnZnDlmDh0msw4pcNbT2E9dzaGjAuDQaKmSPAOT6Ikb9+8zOZoRJZ2glnhR7ja4RPfa2kUms2R49Ia/OEzd/j1x3fxybO7+N+fWuRb55f5qTNz/PK75xkVC3zjjXWeeWuDi8uGyqtQawsFcjo+YSrpyDva2nHvzoT3HJ3lPfcssLxl+J0vXeQvX1R89tHt/Opju7n15C2WhnGvRUZfng85wnVqwrR5plkriNa7MFadxO56JmQ1V6cweTfhSvswZfOE8WWzlDQJk5x6w/1kfS4kOcUQ43oiEi34dqJIB72vCabzIC3OWJSU5KMtep0OnVaLra0qtO/eR5uPiAF9nrwom997Qm4PB6ISjpWNTa7eXMY5xYGT++kePcTxM6dIEkl/ZgZvHZlSLC8tsePAPlr9GVwasCPXXjvPhdfOM7zwNtYk+EQxP7+dmX6fTtaiMEW4WUI1EjPwwk6JFQkq7YTPRfzCy0alV3glyfMBWxurSC2jRd5PcHY+RHtZBN+5sMa9O7fzxKlZ3lguefVWyVff2OLD9/UpUCSJ4PSeFlhLN5F87P4eDx/q8uTL6zx9ccjayCNtEc6HD8xlLzRIUFiOblM8dnIb7zvWZqENqShpa8nOuTZff32Lhw7OcGp3i5883eeLzy/z8MEZ3n+yz+0BPP36cri5/aRbrXdCpGiWBzDWIq1pHFCuWSclRpAFn6ePjqmwgefjwlIdgj5tRsCh4i9SPlBggrtZ4qXDCo8XNozUIqrDR8tj4AJ7lNR4r4NxVYpGEZeAqSpW79zhwO69XBhcCGMaEebBSim8s0id4mVC5cKccWLlN8gkCfjddof+wh5uLC6xtDmgVxqQCb3ZWWYWZtGtjHavS3f/XnqdLkprNgebDBZX2Lx4G3dljUd2nUAfgIWjfeb39ul027hyjLMVVVlgjAnrA0qBUAwLQ+lS9h08yFtvX22K7KaIDm5d2jJh484qZVWGZ7FeNo91VP3rf3C5YKG1xq++dwe/9v4d/PMvXeNLL6/yyo2ci8tjHtjd4vC2lO9fHvH8lU1+/MwuDs5Lfvm9e9jIb7BVwmP3dJE2fHMYL6is4Kk319nW7/CbH9lNP3Fs5o5zt0rurIx436l5Dm5v8+otyx995w7/yeO7+ckzfXqJ4+zROboZ/N43lnjpVomXrQA+ml7FrMXm2uThJ5MMWyOWnZ+ylAlUfMGVDJG6pvLhO1vYpi5sdkKaxZH4pMbGN2bHKRCORDiUsFgpMcRMWqJd34eTLwVIp8LCjQuOZ1Q4bDdv3ubogcPsnt/J8toKha2ic1aB8py+/xSdBKrS0E4SjIeyMiEGgZRESWb7PcTePrv6uxhvDqgu3uby8LssLcywfc8uWjNdVK/FaDigpTJSlTFYvkO1MSRbh8Oz+9hSy6i+Y35Hj6yTkmjFOA/agbMOEylYSklK59nKC26tDvjwz3yGF156mfXN9agQiCb/BGD37l2URY6zjkSHhaZ6MjTtc6mc4qm3RvzY4ZwH9qV8/P7t/NmzK7y5VCGc4OjOhE5L8MLVEV9/a8Rrt6/y7mMzPHBgG7fXc37m7HYeOdAm0SZCA8LAM68sL98YkknPYFTy7565zdXVgg+cnAfvGQzGCCyXViV/8coK//ADO/jwvTMICc9c3OLZi1uUIps0DFPTHbj7mXc1E/BHrF6Tv6+KpaOSkyfa8o5Q8MkNWEdWy+iG8VM5D0Gi0fHUK6L2J1QoQJtI91gjxMhOJTReOpw1SKko84pLb13k4K7dzB48wNpwi1E+BimYmZ9jazDk2s2rbD95jBmd4OKTnBYVSduRpCmdmYRqZOiZPnsWDpN1WyQ7utDyFHlBaR3b0hZz3QWkk5RbBVnexxczlG7EZrVENmfp7e7SmWmh0wRnbbhpLYzHFePCIrxEWMGgLLlwc4VXri3z7vGY4yeO89yzP2hWw6R3KBUAmfefvp+Lly6F0ZnUmFjnhCbN3QV6GdqEP/reEn//Q3uZ7bVItILSk0jPwwe7VA5evTnC+pTrm54bz4/4yrkhpbXs7EqMgx9cKADNXAdO7ZHsnUv5ymvr3Fgp2N4VHNnV41Pv3se+WYW1FR88vcCltWWursF3LxQcnF/nEw/Mc+5GxR9/b53C64hRi1SJuPNRr7fWy+rNyquUOBtyhOusYxlQV9GIEaQaY0pwrhnQvjOXTgsEysvJzVeTKpvkJI+ohU0fuHQaE0HVRE9dINlPx08pFZaSiOEnRjqMM1y9c5N+N2NmboYk7TAaj1m7c5v14YjUWTaHY+aSBK2hrCrGZYXKS/rdDlor2j0Yro8ot3rsWzhCK+0h24CKbpuhxK07KEHkGl91qDBUcgx9QWdnm+5siywJN3cR8+mKyjDOK6wDLRRl5VhZG3FlcYNRYfkPX/gie3bvIk1TTG7x0a2ikTx4+gxFnvPWm2/F25/4+fFNLfvOwJ8r657PPbvK1sCwWYQdlu0zgoMLCddXC+4MDIjgW/RSsFVKtLBkSrAxsvz+39ymcAnHt3l+46P78MIyrhyXlguObuvy0TNzKCm4vmHYGlWc2tPi1O6Mt9cC6+Ybb4yZnenyrXNLLA88LpIO/tagaCHusvJ7wte2zgoUSsTM6Tp61U2IuhMpeZKvIKYc0dPJ20LYRlG6eyEmCjM1giOwAKg9tzIukUjh8SKsJFobr3JFYMmJwDMucayON9goNwPcp4r0JDxSC3JbsDrcZPvcDM56xqMxSZpQFV2SJKPVBbsAd64u4y612D/Yx9zCAkmvTdZKsUrgjMcUBpeXrG1tMLB30DNbdBegvW2GVtYGFMb5YPEqDKNRRT4uwHqM8ixtjXn9+gprwwovFOcvnuf24iK79+ynyAuEcXRbbQ4cPIDQiif/+kmKsgKS6Dqxd1vhG/FVxAmR4vkrY5zX+DjWOrV/hk4Gr1zeoDIKH5rd+IUMn3+twzbiyCkKqynqbtyGeesr10c8drhF7uDJV9b58mvrbO8m/NOfO8KZA12efGOAFZrFgeP3/mYp0CpkuP2m+qap1PIJnLTO85LeoXwoxLx3OBnHbdaGCF5vgwM65kOrCIyyNdTeiWlLfpBMhIxPr7B3bYjUx1cKP9F0omhd27CUc7SsIPOS1EsSJ0iTgMh1EqwyWGepnCD3FqeSUEtEBqFzHu+rUPhLycZgTJakdNsZRW4o8opinJO12ygtac2mVLNjltbepry6wfb1PbTnZslm2ohUY2zF1voqo3yD3K/Q2u7p75qnM5OQtrJQQkSNKnCeDfmopCo9GsGoKri2ssr1jXUK75rx48bGJsZUzPT7zMz0EB4uXLrE2tYGeVUiZQhilEI2xTnvIA5Mx/S5JgwoFOt7FxKkkxzY1mX37IjbWxaPatIIasXC2jrX2U+NxYL58+paxUblWd40fPWNAWtFinWOrVHF0Z0ZvQQ2q7AkVHqPF0lsln6kPPv/+c+kq2VqVFfzhIQLiOf6AGuhUHjKqpba3oHoDbMU1yQmhpqlPmAqmglFNKraCOSxkRMNfaF5cPYkO5N5WjIFFxPVpaIQhlW7wo3hEqt2xBojxq7CRL3IRuKqkJClKUnWpqq2WFxfZztzdFLNcJCTtXOS1ojebB/VadE9YCnnh4w3bnI9v4NaTBBLAq/AKUvS8rR6kl37e8zu3EGrO4uUsSj2Qc23xpCPc/Jhzmg4wlSGvLTcWl/n2p011osCI0O9V39hiqKkWltjY2MDEePGiLqYwGNMhUrE3/qSNRYOP5kr1BQDh+XGaklhFY8dn+GePR2+fX6LL7+2xu0tKN3ENmei/0vGJ0wKSeVCjsqdgeXyWsXJbSl7+inLuWezsrx6c8TZY3Nsn8nYWrFUtYCNRzoaA8qPZmf/aFxbkAkn3j0dLyUvJ92ycBIlfJCuvME7ixJTI7vGjhV/zrkIsGm23w0eHcySEmTEbYlaD2y24jypUBzUO9glF5Ak+EQEF7OSjMlxoxEbMsW1oDQV0gddz3iH8QaDR2tFL0uRSpN0MkYbm6xvbtDevoBxlvG4IMtL2r0KpTWik5K1BGLO4ivAWKQMyelJu0uv36XTa9PptlFZgpTJ5InxAT5e5QXFeMxwMKAcj6lKw53lNZZHY6yVmJImzy4oR4F1oyM1XqgJdKhmrwjpokM5ukj8O8gEYlLS1zoZsab6zqURiVrkx0/Ocd+uFp98oM/+7V1+71uLXNsMOlsiofQW6YrYbaYgBIULX9ZRBecXC951oMWBecUrizkeyTPnc966vRRu1Vq+9ZMq/0cIRP6dLcMEEqeFIxHRQuVFEOY9TR3ovMM2VvwwmqXxHPiGnhXTMuu228eNyvoQxlvR2Ri7Gea7ftqgHReNhVK0uz1avot0GikliU4RGvJ8EzEesaud0RIOYRJsklB4G+hVFioXEjL7rSQQ8JMO1XhMnuesb6wzNztLMaoYpzmtdpvOrKbd6ZBoRStNSbQk1THvI0niBCUgxJTSuMju89ZhjaUqK0ajAZubm2yuDxhsDMk3c1Y3NlkbF1ilyW2F8TUebbJ+IKVCijQmPDm8M81xomY5SzGZ0IofRW1MEBcTDIbzkqKCJ18b8PSbA5442eeD987gvGdUVSAUqQyBPvvnU/7xJ49RGUEnsSReMCoiCsULXr425rOP7uLE/jmefP0mXqS8eH0YQeHJlD1f3P3kCqac3f4ddK7ocBeeGeVIEhtjOwi+v/iqGBPkt7Av7Cb8jNqI6v1U8nCsAcFNilAsTnqUUPX6wIT3hmuCl1yNZhOCqiMxp2eoyg56yeBzQeoCEDsvtwKovN0PmI52iWt5vPQUVcJWXpBXFgT00pRMJaRa4jptxrZiOByT6DbIjOFgRKfTIsk0rbRNohPanRnarYRuJyNNNUrpEGitZDM+st4jfIlzUNqCvByzNd5iY7jF2toGw8GArfVNxqUBneKFYFyOMJ4IuAzShNIaKXRow1wszsVUalS9stakj/q/Bbd2t27W9Ci4CLrUjAx8/dwmr17fYK7TYSsXcV0B1krB7jnFuw4nOGMpbVhEH41d83otbhq+c2nIi1e24g6Fj4+dv2uu+07yFT/Ser4jJdUHE2pHeZT0VM5T+TpUMSD3wnxYxCTOiQrgfNjUs82i0lQXLH1gK0s/FR4Y0bL1IavzINwUOdVFT6BPBYNDmvZ8l9QoGDqKlRyxMaLykizvoRIYmwELcxm6Ezx2eVWSjizDHIyDmVYa4JDeMb9tG94bhsMxy4MBpZaUrkSug8gEaSLQUmGNxZHiUFivwSs0wbxgnWu0qqqwjEYjBoMBW4Mhy8sb3FlcYWVpJdSB1uC0BKEwVlCUBiVEKJydj07qMKf0Ioyhgusl2nOlbzCkTXjAtPEt3nTE1Uh/1+Q+gn4INXEd+HdjQ3BjLW9ixcdW80dP3+Lknh4d7cha0NaKwjqeuzLGBWgzg0Lwr796He88lhR5F+neTW6/ml8T2YH1cFnwToaHn4znBBgkxnrKuLXobdyD8VOVbtzn8FEDdNEQW8+K/eQJrsVkFw+bnzIMTDBetRvGMxEmBYEMWooOb97a4sraGl6poClpge4nyCN7MaMKnKXyc5AGw0JtmSn7YV0SINcJq60WzgaLT9kbgwmZbQOpMalilGiWfZvWuEVqU7IiJd1MkDoBpUJdFrvW8CzE+CyjMCbDmC7WOMbsYdzPMWkIyKkfUnzIhNt5cMRCMzIKe8n1xpqfaCNTxk/fsJiVUmFnxFqm1I0pLBpN/IGrKQH1IcBNxmCxQw6JBOH3uu4tNwuLKhxiJEniCqc/KNgRaRPNnyfCQJuxZN3vAAAgAElEQVSfmfrCS+8muDTqcZuLM/rJQrut6a/x8Oj2HAU6MKTrsyMirb8OryYYEXz049cu+yBQu8mLWz/BVrjQWEwNjkPxGJJBRAxF8U0haeM+cDQi9PaxpBKk0JTxiRJCIlsS1Q3SRKJ1vDUTkIpUeRKpSCV0JWiZBNFaxvwQEfaOW1oFRrWUKCHxSlJJQSV0ICFIibVBlXRKYqXGOHBONsvQOhDrKMcbFJu3US3L9lMnUUkbXR8rP9lo8wK2+zA6uivXIpo1p+GOTCPV3oExa4zB02K0rdhcvsZ4cx0hJWl3nmxmAaWzBlNMdI6EoX58keINFTh8QW+rd5JxsdBvkGoO50p8MaSddQKVKvKyEYGkn1CGA+oswrn4URZnTZDIrKFyntw6SusonY+f1yqkEkS9L+xUB/OJxwYzQvhqhMMm459P2qhlBiSnltM7IfViup+kQUgR42gi+w5s82mV0ZodTAeRyOSC2KhqPqYIByYRnkRKMmGRSAwWiyRBkiIC9NE7hK/wIgmb+E7FfLJgfZIxEHkSIQulq6hc+L1qU6yzgZygiBl0hFolEYLrL/01r3/rT1i78RoAH/tHf8L2Iw+GDj/yTIQHlYQ1w8KGpsxNY3AawvIU089PcsLfmS4up8aUtZJXjDZ58S/+JdfPfQOdtdFpl733PcFDn/xNVNqKtxZNGHSNvJUNdi3s34YAbB+5WqHYD015uIlWbr7JG8/8EY998r9Apx18lEnq39NaG5soG+xR8d9jnKdyljIm2DvnMd7F4zT5RNQ3pPA2yDCxtnO1XiwiScOFel96otsofGPJZjLpQXuHchbpDMobdPxNdfxOrP17yntSb0mdJXEW7UL1lQJJbO1T72hj6aqSnqzoYGj5isyVtHzJDDlzjJmhpEdF5goSV5G4kswXtCjIREUqDJkyZLIik4ZMelIcwpRceO4vOfftPw3ojehJE5HUoPGk0pNiSKVFe8O15/+Cb/3Bf05/ficP/eTfI2t1me32SEUd1VWG3QZb55cYtIBMCVoSEl/S0p6WhnYCqXRoQfzh0FgS4dFywuATQoSbRoQZr1LhbtKdPnN7jqOk4H2/9NvM7tjPq1/5XcaL52krQSYhk2HkKX0V6nPh45PpwhSC8LXJgI6ETiJIU49OXPBjOsul5/6ca+ee4tobf4PwBVIGk21IMK0o66bBGUQMAguXTTzQYR8LrSCTnkxYWsKQUdEShpawdISnK6AtPS0FqQhfo1R4EiyJ9yS48MN7Uh9qPukmEbA6OGFcvOHkJI4u1iZe1QguixSOCOmaFJre40lCeIuwMdIhOKuVC12TV6Et0Jjw5PmwZ6F9kDW8bMiDd3nIRKM3gonduvGOpWuv4tIuQiXNWKfZpZeTjxEI8sEq5775f7P3vvfzyKf/G9qdHodPP87OPYeQieaVZ/5fbl58ke0HTnPPwx9hc7jGpRe/xtGHPky+tYHUCTfe+j6H7v8AzpQI4O1XnuLI2U8yv/sYb3z78yxefpGdh05z7Mc+QlVVXHrhq2w/eIbByjX2nHwvWXceiUfJIFVkaRJMDDiq8QZ7j51lx86DDG9f5Nob32XH4YfZXLvG8tVzHHjwp0h727jx8tc5cO8TVFXBxR9+EVvl7L7nUXYdPcvK9VfZWrlC2p5FSsHOA/fi8vUwDqvGsdy2CBsujkxYEspAxFBhUbxwjlJGPVUYEht3fp1rLPjWOwwmLK25CbPGetOgOJwPBuMg3ru4lBZKCeen84VrGSZGI8i4jV8jWJE2QgUNwnskFuVDLSV8SMPW2HhsbPxXKSqRxT3Z8CQpDNLHj3W1jT8q6U4jVNKEHrpGUauL1ZAkiQcrgwBkvcMiuPXGt9GtGQ499DFWr7/GaO0GR85+gvPf/RwnH/8V2rO7QizW7YusXH2FJ37tX5HNbMfj6e8/w7DwUOSMtgbkg3W+/m9/i9WrL7H3+Lv4wef/Oee+9n+x59hZdhw6w/e+8C946cl/w3B9ke7cbkYbiyxdfpmf/PV/ha+G2PE6T/3b3+TOpRc48uCP88wf/9e0e/MM1hc586H/mA989p+G/RcEWjquiIqNpUt88w9+g7IYc+qxz5AIx83X/4Zn/vS36fR3sLD/Xpbefol9J97NYOM2L37pX7LzwH0IqRCi4vb5p3nj6T/gE//g/+TtH3yBN3/453R6c9iq5KO/8tvc+8CHWLz8IveeeZyuMGgcKgmvXLDPm2A/s7WM4sJGoK3nuMFUGsYE4fA570KWSGwwnI8f523jgvbRiGAEjeZXx2+EmtxNpa/XOmBz9dZCZOjQhLdRmnEoL9FCousFJBe29PE6dD4evExwcY1JyJBZJoUj8RZppmIgRBjVyXjdez/ZZK3noy4K4zF3MgyyI+LBOYfSKatXX2LrztskrR4Sz3j9Jtde/TonHvt0/PsITDHAlmPSVqfJNREitFXWOvJ8wMadK+A9d26cZ//Js9iq4MwTv8j9j3+Gt579Mjpt8cFf/id8+3O/wz0P/wRZZ5bXv/8XmPEGVfx47z3rty+iHv5JhJC8/1O/xdLVcyy+/SJtO2Su046vQoUsB2zffYRP/ke/zdXLL/PdL/8+83uONOCmBz78d5nbc5xv3XiDLNH4KsSdhhqvJN9YpBpvYoohanCLloKde47yxMf+Pk/+2f/E5uIlZrpzKDy7hKGrS6wL+zfGWYz3YXJiQpwZWBIHqQ0hQz6EVsRAnvpWC593EX+4aErwcYHJh3XFgCQJRzbcgGIy6JBxib0GGxA/JD6ZNYiyNhpYpDchAMWXqFgfKluhTAmmwFYlVVViTBXjqgypKchMScsYWtbQciWJr1C+QnrTdGoB7RXB2lLjhMSIsHdSCYERYITHCNuE5UnvY40FB44/wunHfp71m2/gyiH51grj9dtxPhqWcoQwdGfnac3Mc/vNb2NGK2DGrF57laXLz3P5ub/g+S/9a069/7OceM+nw1hJt+jM7uDU479Ed/4Aznl683vYf+JRWt1ZZrcfYPfhM1T5gDee+VN+8Of/Mw9+4LOcft+nw1qo9GidcObhJzh27H5cMWTGjphXBXOyoKtCbZe1uhy45yGOP/zjpFmbreW3SbOEHXuP8/B7P86uHTuRwnPn/DOsXf4+GsduXfHs5/877MplPv0Lv8mOXYfQrkTj2LF9H6dP/hi97ixqvMyMLEJ2nM+jEcRTekcOFHhc7WYRYR/cSqgUVFJQSE8hfAQMhR5AehnLpMAJD4qDwspATPWRHe6ExEuBkkFuUwKkDGYWjUV5h3ImjufqUZyrW/jYCcdop3j9RdI5YZe3Vvljd9OIDtJGPIcJ4ziSJkPMC9mEHNexo7Usar3H+KCoN11ZJPULEYbcCmJGSbghlRTcfOt7LF15iX0nHqG/4xDf+w+/w/rihdDyO4OMy/Vzuw9y6MwHOffUHzJcvUna6nHjze+y997H6G/bhynHLF5+geuvP017ZoGyMg07sIY12XKMsCHVKcva4e/kHIP121TFmNuXXuDKuafpzczTlkES6UhHK644tIWho8I3Xu4EUiu21pf4/lN/wmvPf42qHPPQox9n5dYlUgU7MtDZPLt37OX7f/W/MTMzj5bQFWNGG4tkrR6vvPB11lZuYPJ1FJbEG2ZcHi4PEbF43jP0irZIsMIEF7u0CGejPUpSCUnlA+VLeIOTAe+mUVhJDC0UTVHkRLiewlZg/FkX/p0+SnS1jCR8lH5qWKUP/27hbKMDqve99/3/dGsjx0TiqAwGIHTDfokM6dqiH1WF+iZCBgt+iOaEBIcWHiUcKj6vTTqOIP6zIJhZqciFIpcyLDhFxmCARBAbmvBdpOJzjnes3HyLYrzByUd/mtPv+zQLe46xc9897Np3gpUbb3LqXR8h6/SQMXXp0L3vJk0ytu5cwZZDjj70Yd718b/H3iNnEM5gywE79h3H25IDJ8/S681zz5n3080SyNeZ37aH0w8+wfEzH+DQ8UcYri3izJj3f+zXaScJohxy8MAJMAVnTr+Hmd4MZx96nNHGEsYa7n/ocXySMXaOkRWsr93h5qWXGK9e48EH3sNnfvUf8+DJMzBcZtvsLA/e+wALM32OH3+Iw4dPcfTYGS6cf4H3vvfjHD5yL+V4k353hizN2LV9D/Oz29m17xjbdh7g7esX2H3gFO3+bq5deYPth07Tmt1B6aFAUiKposVrkgfomITvTogWVkiMUJGYIXFSxH2gQI+gPlze4F0FzkQvoEE6g/AVuDCEEDVL0Ydn/YF7D7G6ePVV8Vu/8V/5G1fXGI/HwR4UN5aUmErFlCrs5sbYKSEkSoU6z8kkOHeVDgmZQqCEajLghJQIqSNFVSFUMCsIqXE6ZSwyjEpwQjXuaqEUiUyQMiZvSoGMgYEezWhrNfCiewtoFWbWyjteevpz3Dz/HD/9a/8MOvMBpB1DGPHRHi4FOs3C5CQgTcOTXVVIUzDX3xbK5bSNcYbh1gpKJbR7c0FfFCIwqV1Ju9XGFyNaSuFNQTHeYnZuO95ZEp2Ql4ZBXqGyDK8UpS1p6wxRjfDDJfqZpt9tg8qwxlEVJZWzOKkD8QCHtIYvfekPeOGH3+A/+0//B/r9bVRFQNStbaxB2sWIhJHXVF4xGm+F2Fjn2FxbojezHZFIrDPNOEzEZ9DH2ax1DussthaSvQsxsj5EttqYCBp+3gRSRNz5cfE2887EWbCNlFkbV27rpiVgnJ0zGGv57M8+wflXnvn3OhgNogYUmc7SBz1tEsVZO8cIDYsA5cPP6pifToQ4KqmQwqIiGyE0Ko2nFx9poSKOihJlA1Ja1FjXYA1PESRItJTxmyIssBsPSX8uJjcaUhF0zG9+8X/h2oUX+dlf/i/Z1m0zFrZpbKQXgezeSmMJIJEu7EJnaWADpq0U5do4YagQlDZnuLbIX/7hP6PXn+dd7/87HDl6ik6icK5gptNjfeUa555/ig888TM4Db35OapySLuVMR6t0c7aXL7+PAlw/8kHefJr/w8/8YGP02m3EbPdhhXjbIH0kCThWbTe0ncVa2vLfOFLf8TT3/4yP/fJv8tCb4HSCSqdMbSOqr+b0sWEUW9JsaTddhgfOkN3xw6stVRTt5t0NM8nUV4T3ja1eYMEdmEG7qJQ7Zqcv3qUVjufw/MawsjrH34i0/i60ZzsLePtRAekvk4bi8wEwVUfPhG/E4IEULMRAkNZxkBqqYLmp2uTYw2mjm4UmpTvCulVs9ytI73ORx3SeQlOkQhL4hVaaBQKJcMOh51y1EpRe80c95/9IGcf+yj79h1DuILMV2FXRUqUlCQyku9jfp1wKnwRasKpCzMFI0RcNnC0tcJXYxJmufDckyxf+CGHD97D6+d+wIMPvZ/zb73AxtoSF9/YycbmKkhJK03ZuWMP3//BU+zbe4jX33yBRx58D227xeriJb7+tT+j1eqwe9d+WlmHUycfRHkTmjLvmi+gABaHK1x++3V+4sOf4t2PfZRcKnJjsTEeLRWOJHaUoToJsWHGOyosVoYOVnuBqicnUTrxEeBprQ0dsndYb+Nt5+LB881t15yRpqab1HbOuwac6acmODUhDCY/NzmAU4ZU2WR4+SYTNiz5CILqFgfxXjQwyqDPxYMp6j0BgfIWVRehSHASpxyC+EnGIr0M95sPnbOQGiXC0bV1r2XDE6+sCgfQB6NBInS4wYRoEjudEGw7dAwlIRGhypFSEVqxsDjUjBqdxlmJ8bFjU7IZm4WDrcNkyAsSXzLf7fBzn/hFXn/tWQ7uO8Kbb75AN1V85S9/n317D1GO1nnu2a8xGGwiEXz207/GD59/hmuXX+PWtbfYvWsv0gUtNEszRqMtFpeuce36Re4/cZre/acQzsQxqKTyiso5Ku/ZfeAw/+0/+V2MlxTGUTlPFgeBGRanbXwiw8vggHB/h98vaMAC4uzW+TD3t1hM5TAxvd56F1WMGrgZwEP1nDksnNh4eMKvq02msSWJ+p+bGhHGRIT6GW40QtuYHiaTkChESxFuu0A2Vc2sU8q7w+rwHi8dMlpUJySZEH0gaiygrJGykQxFGPFJJMISaPjKIYQNMJ669ZASLYIBVstw+0kh0VKhfYX0ISRPSYlC4WOjIppQvABOquMMtBAIobAyhLuAjKWCinEUsdmqN7tiiptL4YOP/QT7duzEHTnOzm076SWSK1fPc/b0L3J76Sat4ycxpqIqS2Z7sywuXuORBx+lnaVsW9jO5tY6B3bvpq3gwM5dHNyzmx3bdvC1b3+DLJFoBUaogDhzoSMNzhzb7OIoZ6i5qUbG2zqmCFlPPFRh7lD6Kmh9Lpwf7xyVi6GK3gWksLNIa4PwTKjPRFyxUH4iMAcp2sbYjXBr1gSEmnzm6xIsHkIRb/DwDNtmS474z4LIPVl8185DZS2VjTdTbWaLdRc+7DtI4RCyjmB1KK9CmpKs3RcypmIGqcRGN4kT4f9o79FxH0PJMEMljtuEtdE1oRFaIr0kUZqWh8wJFBKtJdqrIMfIIIpLBMrXbhkVFmHivoJxGidU4KlAXOAJn0wtJEIaBLFZqqPnp0hTWoBsC97zY4+CsNx3zzGU8OxdOM4D954Ia5lnHo4jQBnVgyis4jhz4jhT8dB47/jln/kUXgg2h0N6vT4nj5/C1mPDSKrVwqOi+B8Al+GLWeEoIjDdmnoSEV4gZ32YajiLMCXKhvJI+YDR0M40T6qLt5uliAJ/HLF5MLX+1txmcUYczaXhtq3pX3Zyo1nfUNHAobxDxgMvnAt7L37SuEhn644Cbb1nbDzjyiPVxNER5ro+cPiiq0Q5okAtsfXI1QWVXsehnMaFOi1sE+OaLlQ2CUoeiVfBIZH4JHCOXTiEGk2CJPOQOE/iAug8cSocWimQXoZNK6nQMtRBSTyYPgbpVcpTeCidoiIs0dduFi3qqbdrBFEZo6dUzUEmfOMF+lQANU2iThVC63i7erR0zUGjwRm7KRLUxJOHkMx0WnzqI5+g3enFMkZQiehGIbhRjA2Zy4UVVC6sLXjnESZIHDiH9oEMGbg9YKhtTxbjwt6LweJEhRU2DEy9xXpP7idWefzkKXdxLFfDhbyrn0/fNBf1EhvxRpPRfu9jTUicmtS0ysAGjHV+0/Q0N6CgcJLcCmowKqJevfRIJZAu1IdaglIqJCbV9YUMz55ChMOoBMp5Ui+QPrrtpAkquYuZIl7S9MkehA20e4kgMZZUKJQKMQ6128XX0Vo2egZ1be2WVFNuXackToYbw4jww8aHwkuJdFHnlBHKpDxlPuLcuWd58IFHqKqS737365w8fob7Tt4fmNlSBddw9EC+8MoP2bNnHwf27gVX8f0Xf0hV5Bzad5ATR49y+drbXLl+lQN79zMcDXn49P0I7ymKnG/+8Ic88tAjdNpt8KYGrqHwOBlA6N6biLHwwQUkAthCYPA6slVsMPKaGElWRXdSEee79QGsnKXyrnmCg+TiqZwMxDMnms679oDWZZVqntjJsxx2fYOYHOSWePDqm/OuAxq/RsI1B1PY8IPJJCTUCsb5SRxD/C4mRiz4SLysgUTC2fDPVUAxSCFC9JYMT52OhlZlHcIJjAzOXx1vFKl8MClIGWO/PM5LRES9VsJjhcd6ifIK7eIT7xyJDDdNKCM8om4mwhgGg8M4SdXcwKaxyDsRIrpQOgbwgPaC0ljOvfosg4011jbX2Fhf4fiRE1y/doFWGqKnxuMh+3btQgHf+d43+OkPfZjbdgst4a3zr1AUOQu9lF56iHy4wnMvfZ/h1gpCCI7t3c7inTvs37WTl889y7ZexsLsLKPxiCN792CM4dbyKvv37uX8+Yvs272H+W4fJ21zm1jCDLfCUVlHacLfs3BgnA0UL+8oYlNhXK3x1c+uiA1J2IbT3iCdm9T/8SmO/vrGt+djqmkU8eIBdGDjOM2F7rheN5BT/1sQkpSm3dZTkI/6BvSTGlDKSfyqmKC3RB1S7VVIRI+p8M1M18VkxAhr9NJjcFgZvGy6JqRHRd1bsME8hxcOIyXKKbQM2XFSyei0VWEioyTaebSTtKVEWRA63NJaeTrSkqjIqHNyEk3gJU6EzTitZLSdCRAWX4/aPPQ6CbbMefml7+G8JUtT8o3bPP3UF9gxHxEXN69z9tRp1jc32Vy+xs23X+OlN99kvt+n3WrT0dBPHXOJQdsRFy6dZ2VlkfuOHuXffe6PKcqcJ86eRdmcL3zp8xRFjvOO4wf2szEcURnD2VOn+O6Lr/Cbv/qr7BKaEsvIe3IXDp0xgrwKRoLSymAuMFXM3QgNR01WlVOyi4rxZ87V9bqjsn5ye9WWq7rjddG+76aeYOenOlkXL6HQmEhXd78R8hQbGVEzAl1US6I1y7mpG5DYLjsXg7LqfDghmvVC0bhyfYwjiMT82KnhZDQx1Ohej5GRuOpCs+C8xAgJLhbq4cMwkhh4FzQ6LaMX0XuQMYHRgwoQLQwVqQ6zyprWbuI2nyDcsiomA3kRBFcpXMCEiTjbjJJShiDzAq0lvUxjCoGWaTAVUHLl+lXedf8D3FlZ4uwDD7O8usz1WzcpjeX89Wu8dfUKv/yJj3Nz6Q5lWWCtQQpPv9vh8bMPcfLIYZ5+7gWUFOxcmGc8HuCdYcfcDMf2nwQEL751novXbvLEww/w7nuP870XX+LKjavsnOtTWUNhPWPryY2jtJ7SxG8yF9LXNXffQPUBa4Rk74KWZ0PtZ+PNZp0NgT/xv8MZmPD9XC3PECUZ5+I3cA0xsE0zIuLhJzYlYkrrq1kwHgf1bXqXGcFPTnawoNeBhZGSbwPpHDeNgphEdRLHdY2QTBCrHaGbdUiMFxGcLXFSNqZVLzwyCsA+ru3Vz6qKkHApHU5KvKt1O0HlBYkL40BjJVW0KmkZ2MTJ/9femTVZdqVn+VnDHs6YU2XWIKkmqUpYU0st3LLC3W0TGLcCbogA7vgHXPIz4B/wCwgi4IIgbEyHDdGBMabV8iDJLbXmUpcqs7JyOOMe1sDFt/Y+p+SGMMGtjyJDUYqqVNY5a6+1vvd7v+c1BqND6iNHlIcMla4AiV8YpW2YoTBR8c/f+RFN27BYLyiLgvu377A/KphOprxy7y6DMqdta9qm4XIx4/6d2/zOb/4GV3YmvPzCbZxreebqEWjP87evMx5/n+lkxNXDCbk1zBcL3nr5RQ73JkxHY+48c53ZsuLV+y+gY+RisaBqG3745uu8ev95GeqKhiLZ5aISDc/okDTdRHfwri9Ygo+bFKk0TxJ82JJCNrua7qpZv9mhYuyqWZFlTPCY7kim6+PK76UTp4P4NGPsihbXT751xAMVNjvjpqgJW2SE9NToHvuqEyc45X6EDt27hWToyFjqKW5NGuVMgmHqWMQO99//o5PfT4Q7HTRRp+tEWoiKVHEZk8jqsnN6pfspPhMjTilarbFakRtD8BGnXVqIilxHMqPQQSe0hUlABw2e/uG4euVArv3G9IyaN197A5sZWdxWYTXkRuQeaxUv332WzCjhSSesr9KOw/0RhwdDYgw8e21HxhrS0//3vvc6PiicC+RFwc5kJHdfL7LLszduEIF50+BdpG2T7OJEKzUp5kpHkmbnE0ZNFqjqTqbkMlLR99WoSu95SFb8EDYDUDpx/zohOqQ7oQ+bAiImj2YCQvais/R8ZTfs8uK6ofWQ5kpCGvX1ISa2TboDxsRI8d4J3VQJWCZuIbZUd+nTG8E5hnQPDBv+VpeYFFVI6ESRREjpjin/lI3nJg1BpUEWrSToRdpxCpXubTEJyEFrYjTpvpkkEq1xUf4cUfgoWkHlZUgn6/LsjJKc4EQuEHKn/B2OHz+iHJQsVwvKXKDewyLH+5bDvV3mqxm3blxjtVrivaSqv/L8s3zx4BGX8xkv3nmWg/1pb7Ls2hCL1Zpq1XC4MyV64U1X1Zr3P/0S7wM3r18lz3KclwXpu4LCByofCD4SvVD6nfcSGOSFhNBNs+ngMSF1NbzHO93fyUyIGB/wQYGTY1c0u4hLC1CO3JiqY1IrThZVn/8RXAoVj72eSFf5sjnWZSNzKQ9uA64P/UKNBC+RbT4NX9mmqXl8csxyXZFlGWVZUpYDCYDu9LK4mSvd8PHjFlSGHuPVHctphqqfbU1nrGhoPRCTdNfr45cSiSEQlEbFgNey8H1CvSqCXB6NQgVh58XUUnNKEbo+se7Y0rq/c6I8RkesEv1Qa/Ch5ePPPmM0GnJxec4Lt29y+uQxTb3m+PQxgyLj2tEBn3z2C45PTzFaUeYZ9278Y/7i5z/n5OyMTx98yQ9+/TUikTyzxJRx8vDRY87P59w4vMK13V3qqmV/Mua//um7xAgvPHeDw91dxsMxRMX5YolSiqZ1HO0dUGY5Lnppf/mA86B82ol8SIs6bI5R72Un9bF3QLcuteqcDF1tZJhNtRzSr11i5ojrxcsO5iM+eoKS3x97dUV2RpXCyjvsRjf30f86bI7tTgZyfpNlbOum4ezJKWeXsxSnasnynNFwQp5JVH1WFIzKAWWWUeQZVouTRa5U6YnTIkoHE0VwjjrhGUxKIJIqGRN6s0KnM3YgnBAVKANKYZRP30One1uQdPIU665iR9TXne8GjCIE+W/R++R3M1tBLjEtWHHHWA1t2+CbmuPZOfdv3+XK3i4np8dYYxgOCjSRm1ev8P7Hv2BcWiajAR988hmZEc/jwd6Yh49P+Tf/7j9yPluwvzOhKDJOnpwzLHKmw5J//+Of8OLNZ7h77So/eusNLi9nvHr3FsvFgv/whz/hcGdKbi0n5xcs1jV14/iX/+yfsHftiMq3KC/3Kh80Ki20mFg3zsVe92t9oPFIxeyd8A+dFBcy8SfHso+yqHyiRvgQCV5wGsL4S0SJrUXTVbY+IUn6EO2wnaQUEmov6SdxqwgiCeapAHqKEd2h4SPggsPVgXXTJhSrlQHqrKCwGdPRkFFZsDuZUJZ5cmNIpaqjJ8aMkF96fBsAABNESURBVNK8opajmvThq0SP0lGwZEa4ZLKDJRdySDPFMbXqon6aR6dDKi6SWdUqg+36GlGhghG1MnU25EraRc5LQ0Zy9IR3rbShKEaURclHn/2C+eoSnKOuKkZZycHulF8+POH7r73Koyen7E0GvPHCLXaGOUcHEz744kve+a03ePeDT7DG8OjJOZNRCQj6txhYbt444NrBlMIqNA1v3HuG33nzFf7wp+9z//pVru7t8P4XX/Hm/dtUdcvXj88ZDizO1QQXwMndL0uFotDzXO9+VunDVV1yJ52skooO32VoxX6BqCA6reiMohF6L627jUEh9v/uvnw3jN4xAdMiVT0ZQqpj1+M4ukUst3/fXRfilhumdzuwxVVNObAxgbirNlDphsv5QhZkYckzy3g0YlyWTMsB0+GQaSadkmBSpabF4mR0SDPGMfUpNSaKDiOFRSSoVNInh7RYvFKMs0ZE6WgxUUlvmUCGJpN9MWEy0t7qN3p+d+B7pXp7OenoVtpy7849ch0Z2L8jsawaxmVOaQ1lZslzTZkrfv2le4wHRsJolOe333qJ3/rBS2il+M5Lt/sHJRD4z//tXa5d2eH1+zelV7vyWK9QjeOf/vANXBv5R997jd99/TW+ePSYYW75+2+8JDQvH/Cto/VVn/ms0kKKXiU+Y3fMOjlOPXK0tZHgoG2h9Yo6KIEBpKM7+NjvbGIiFeKBiyRd0Sdbluyqri9aZFG7btgybrTCnuaQHPVxK7imG2byntR5ibi2o+pv0bFStFGXbrcJKxFcfoLxODCaGBVV46lax3ztUGpGZgyjwYDr0wk70wnj8RAbU4dUSWNcJ4i5BXHApHgc4caICyQT3CfRmATsEX0x0ZIIWjyEuYoMFBQqEMyG3uRDshUEnVC5Aj4KiKU8piAa6fxoGR1QCkzgyXzO6eyU60dHZAd7rNetCL3R0bYr3njxNoNigFI5ympCDjGLqWEkGhg6ojX83TfusjsagJEPpQkNX359yn455vOHJ7xw7YAvvzljVQcOJiPuXd9ntZzxxaMnPLe3x8FkR+7AKTdPB491aS4ngg7JRJwOMFlYAe9b2tbTeEXjNHW6w4nArPoiJKgtp3JU1G1g2bTUbdujStsUIhRDV9UGGqBuXe8j7CWVpFyY1EUrVOh1ZIFd6a1Uwg1RYrMACX3EJnQ01C09JRUNMaTSMRrxnSHGzzoYatdwsVhgHz1iZzzi2sE+O+WAXCsKq1GZwQVFpg2jQpNbnyo/RRUFx1Gkvpk3GcZoSqsYZjZpSxpUK3dKbSlUzijLCIrUAQisQ6SJ8j0DhtobGocM3iRaU2aFWW1M2iOtprCKr48f8+M//iNevHOX733nNY7PT1mtVnz96CF70yFnZ6e8dPs641EOI8VLr9/h5PySB1+doEPg8GDMbLkmalgs1jx3Y5/gPM/fOOQ//fd3+b0/+jPe/rXbLGc1s9s3ee+Tr7h34wp/8pcnHJ9f8P2X7vD77/4Vz+7v8i/e+QHat6gAeYyso2IVI41rkyIRcW2gcoomKFaNY1U1XNSBRe1YVh4f5A7sYktd1UQ0xljqtmFVr/uR2EXV8PD0kkWbOhddynnq2cbtRPjQ9n0017Ypnm3LHeOcjPN2bhdj+mFfjeTChBBYLBdbRzDxW18qySixFwylGFbfAgknmFGMKB+S8UDcG2ezJXXTcnV3h2lumY5HLJZLFlWQbBAVyK3BFiYtFktuLNNBIVGuDqyxDHPDpMyYDiyZCgyKHBUjM+PJjKPMMzILAyNC99pHVs7TRI2PgXlVsao8bRTqfgiByXgEMTCKkuje5opre1MuFi1lNuTzXz7kyWqFMorD/X3WPjJB88cf/IKPHzxgPCyxE83N797mLz55wI9//FOi80xGBY3zTKZDnrm+x3sffsY/+OGrBB35nx98xrp1lKXh4WnN77/7ES/fvMWwmPL+lx9Q5IavL1YEk3HvzrOcLjxLZ3k0a7moI7PaU7eei+WaygdUkfPg8TnLRsYjF6uG+arisq7F2e188kMq9gYZrq1onGdZN6zqhnXq5cq9H5yX0yb0BUQ6Cb3rj1q5g3u0EdhUU63TFS3tsKkQyYywfOq6wrFJDO0d066lrtYdGWE7vTD20SeRth9PTFPHqeRW6RYs7pSkPvdfMT1BIVjm1ZL1yYpcZ+R5SR0iQWXsFIGX79/i40++5uj6LmcXc+aVTZ0WqaydMCvJFIzKjGlpGRcZO8ORKPnaMBgMGQxKBoWhahzzdc2ycqyd53S1pmoaFJoyK7mcz6nTm6SNmFxNCrg1WcaNo0P8esHxvGJ/74hvHl4wGk/4ZjVjNlvy9TwyLCy/nK+5fWMHP5vzv96/5HI95u4z91gcn/PzTx/S5IFnXM5B6di1Q/78Jx+Tf9fQrAYc7NzlX//bnzEYTBgODvi999/DBc9oOCUS+YOPPybPSv7kwScY8wCdDanblioE1l6ByTbxWOkIFD5iOoKTeK+Sd6/bJM5zxd5AUWjFcrlm2WpC8voNByWhbfB1xWCQE2JkvV5v9pikNwpfMBUmbaSNLp2aLiVydgwhTwgg2T9+s6nFrphUKYXdf/sIjmlRxU0sgyCJNumPyfOktr+n2hCT4gaal1AfEWsKgvKcLy9kKk5pLpqWDz5qmC8956tLfNQ4Cql848ZbFTFoYzlfLcV3ZzSlFQpqCFH8gMZiFHhtqINkkSgirfB1xcAaZ8muntgPPvQ8bNm9PfMHDxL3+ZB6ZVHmkGUFvgLDLtVKwwpsVnL86ZosKv7qX/0BRWbInMMEzWU1wmeOJxeRzz8/49qVMQPT8Gef/oy//KJmWbfo6S1mHp5UhsYe4JzjcilVYVS7km/SRDwVMdbkRraFKihQVT9XoYi9YBy3Ohi+N4pu8HGLKnIxa8lixMcch5V3IzraetF3LVwleBahJXRlQeJI+9jvdNIBccmm7791fUsW/KfyhTcNDGLEKEemI2uZCepkGCf6RGptxBQ8vcla98TkKJauA8mqjWh1QfV3B7AdVZHazVJNk6VskRanDKfNTABEtZKcCV2lBbpp5Sklgc0RLTOtIVK5iNbCpMkLg8UzHWkenx7jEqZNp4UXUpdFJBubPMtpIB7V97V9AiiSZB3ftvImd1CkRAVQiDMHIuPRgHBe4VuH0gplDa2TgMZvurfli0tyFbHK40MuNnojHYfg5YPNjKbxgdZ7rDUo55gMhyzXFQHH7s6Y+bwmVB6lpSsStiIgQtp9Qq+thaey2KQwkDZY5SVuTYeYzq9InhnapiWESN1srllKgwmBg50B0TlWq4q8sMyWdSLfi2spdCL4drjk9tWtLzu6nm3LsFQc7k+YzWfdERy+RdIWjlvvIU5WKi3ekp7+3pkVY/cTo1AqQ9EwHWocOeu64wKLVjcuxOVb+0jUDh116gl7SZ5MSU1KKTJbsDfdIS8G+Ci2qjyRDY2xGJNhjaZ1K9r6caK4Jqu/kbZWVPJT6mjIbElZFOKcjiTopWJVVfJBZZYrB7sMikLsS4ig2zQty9WKqq5oa/n7XjSLhLMNTIdj9idjdsZjFhdzGu+oggcnSe2V8yzWc5SJiTRqUsJbYLw7ZVIWXFyek2vHwc6I565f53K+JETHjaM95ouak8sVUQXWdZ1cLemeHiPOuX4HdMmc0Pd0k6GgahoaFykM2EyxrD1WBcosw2pL63wfSg4Rm2VYDYWF6e4Oi5nh6NohXzw8oW5kUZVlyWI+p22ExO/aNlXFGylvUzCINS8Ai5Xj+HS2dQfsiFgdeLInvG/cBLmO3H72Ot+cXrCoGumrksmC0yY9OQqrMyw1v/nWPT754pKTC08bakKyOf3GK7f59Js5j86XMnoSEk/PyJ831mKsFSZzlpPZAmss1tp+nkRr3Q8WxQi5mnDrxi2cb0B52roiRsO6rUEbrBX7/mg4Zn9/P13QA9ZYcmtT20lsZoUtKIxhMiwIoWW+WvDg+CHNei73rajRRlMoy2BYcrS/y8haru0Mef7GAaW5SlCezGqq2vFffvYLvrxocKGWWQkl/mefLvmzuaOoc0qtuHqwy8t3bpGjuLY7onViHJ3sZhxOJ+ADLpAs+nA2m3E2vxS6X7puBWNZV65PqFqva+q6oq4qYnCsKp9saTK+Wa9nvdbRM/NjoE5mlMvY8jBGtM44fnKMzgzaZGRZDqEmuIoYG7K8wGaWarmG0PbojW5H7tO4kIwZF+SYt1oblB1ic58GjgyoFB9lEnUeRWkV+XCXYlKgJ8lkEBWZ0YmEpVPZbcmV5vhMEeyUnSNNdBGvcqyBdQUmKzk8HKLQZEocKtEYwc4qWVhN27BczDlbHLO7u8v+/gGL+Vwq4+EwLUjZ2k1mODjYoxcLWofRBq9EkW+ahqapaFzD6eNHzGczGteS5wXWmCQ3dNN+Ml4wGuSURUYkYrOSG9dGEklvIM9yiqwgt4oMLy23UcG6XpGXGaNRwZXJkOBhffmE8ydztILrV6bcu75HWZYsK2nIa6DMDJPhkEGes1fCqChwMbCqHPXa4XwrNyWjGWSGPCtZrJb8+ecf8PB8hskymkTyx7veItXXn6k4IDgpFMO2hSn0mYDbx3c3/SZIcoX3jSSJVnK/0F1CeTp2XbVIm5XrrfjbkkmMsUe0sIVEtsYWlOMjasZYJZFRIQqTOSjR26JvWNdzPnv4CG1z8rKkcZ7gI0UxwWa2B1bq5FZ5spRJK6MMRqeEdaM4XQv5pbCZjFumqFej5Ontg5KtwUx3GA7H5FlGiDAcT8iU2Ky0jizXS1ZNKyaKohT90AWGZUlsK5aLBQ+PH7FqKlrXbu4iqchZKp7K81BdSxB4ci7Ob2sMeV4yKAqsVkSXEBVaU1Vrqmotju1EXLA6Msg040HBZDRg3opumRN57mCfN194lqOdEd4HVm3k069PeHxxyZPzSy7mK7765gSb5UBg0das65bgOrwZFDGwMxrwyq/d5+YzV5lMx9K+++YRPpJcyl0fN9nnvO8DejqvHzH2YeEbx0hqpdHhjeWub4ymaZqncmtCcImssKUXx02AkYp/LSSqb+GxFdRgQ3R4X/Xf3PsaH0HrAQpomhnV8pK2WRK9Ix8MaWtL2zrycgerJqgQaNtWFllmqauGuq7RNmc03hWuTMrXFT6dVNJZITtQm+YKdJTCICYMmzWWkOX9g2S0xppIZuUNOzs75vjkFGtz9g4OmE52KMsB63olFjOtGe/tMuDpN0Qp1dPgtU5zwdYm9oxOYduJ2BojOrNYnVzgIXBycsJ6tSTGQFFI7p0MCJFyMxyXq4p4viAG6Q44H3nv4y94cvqI3337O1wZZXz0+UP+x4df8/Dsgtb79B6J2QCk64O2adY5tbfQtMsVP/3oE4aDAt+0tG3gyu4u+/tXQItNv24ci+WSx6enKKvY29mRDDsni8R56Wa0bZs6SC0xNL2bBSQ/WfJWNl0ytRVU1E0B/g0D5n7ly3pX01SX+DajbVrado7OM+xEjp/V/DGumiUfX6RZNqAU5XBMmQfmF9+AUrRtK9KGq6iqFQTNeOeAzCrKnf3Ej/Gs6xp0JlVZFchtJvgMJbb7LotWd4uky6dNjBGvxPWhgCuHV1is1njnqFdzXFFQTify1pQlY6XYjbvJVr4dSKXS/1NtHwqp+DE97lcrlWgRMraZaSnEbh0d0bRtMlfI7tk5RkDkIoJPoHax/J/P58xWa6ZlzvnC09SBYnSF1165wt1qRbVebSA/3rNar6mbmsFgQDkYCCcasCZjPN3lT3/2Hg9++VCkLjR7O7tcPTogs7q3XNV1i9WKx6fHlIOCyWhMbgvW6zXz1SXL5ZLQNL2/T0YsO8xaUguc76PHSD+DUjJkhtL4QJ+L3CWE/qqMuf/jApRmaUW1XIBvibEBUwpJKgobT6zK9BdUIjTrJa6u8N4/lbATk/iotcG1M84eLyE0mGKIwnN5fo7Jh0wm+0LNbxuskSJBKRn7VEphjGZoFaPcptkPUfWDsb3PLIuGW9efE6u6tVgjQdday7BRTHcs9S0uccdHUUmq6QaxOv0z6r4PREjDVhbp8ORGJuu6dp5U1J7ckAAgaVdVG+FVK8vVacbRsMXYKIlHrTwUeQiM84zSjhMOLyaDrscq3fsL29b1Xky04u7tmwzGQwKKQZ4xzAtsTIaF1LmwRrMznXJ8cszxoxPO8zPwnqZZ49yqj354+qiMT29p3wpO6grAEAJbUVpPjWH+v+yItiiKs3fe+RFV3fZJP1pbbD5AK021vkSliiU+FWC8Jdko9VTGbF9NKekIGpOjjO0dtEU5oignfTKx0ioFXrMJWVGa3CixMKktZsFWDluIitr7lEPbvVcdWFJvXaSfCshKjZ/tQfOtdMutgJ5u1pjkstZKkVm5pzrfytGtNfPFnBgCk9Gol6NIfkeVELadyUOnRRk7WHv0uDQai9qUDjEKEF7COFXS/ujjwW7Fm3Jch24QXm1mo7cSr1p/n7fe/m4v0V5ePJHoMfXXF0p/TdlahPFX5H0q1Eb5UDrxYgJPPeb/lx0whMDOzg5fffXVSr399tv/8ObNm/zt6//vFfsMqb99/U1eTdPw4YcfPvzfuFiHxVQvJiwAAAAASUVORK5CYII=" alt="" title="Carman Bryant"></a></div><div class="promo-details"><div class="promo-title"><strong><a href="/sublyric/57471/Carman+Bryant/Midnight+Star">Midnight Star</a></strong></div><div class="promo-artist"><a href="/sublyric/57471/Carman+Bryant/Midnight+Star">Carman Bryant</a></div></div></li></ul>
			</div>
			<p class="text-right"><button type="button" class="btn primary xxsml" onclick="location.href='promotion.php'">Get promoted&nbsp;<i class="fa fa-bullhorn"></i></button></p>
		</div>
	</div>
<section><!-- /52304935/Lyrics_LR_300x250_C -->
<div id='div-gpt-ad-Lyrics_LR_300x250_C'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-Lyrics_LR_300x250_C'); });
</script>
</div></section>
<section><!-- /52304935/Lyrics_LR_300x250_D -->
<div id='div-gpt-ad-Lyrics_LR_300x250_D'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-Lyrics_LR_300x250_D'); });
</script>
</div></section>

	</div>
	
</div></div>
<footer id="footer">
	<div id="footer-int" class="clearfix container-fluid">

		<div class="clearfix">
			<div class="col-xs-12 col-sm-3">
				<div class="row">
					<div class="col-xs-6 col-sm-5">
						<ul>
							<h5>Company</h5>
							<li><a href="https://www.lyrics.com/">Home</a></li>
							<li><a href="about.php?slc=Lyrics">About</a></li>
							<li><a href="news.php">News</a></li>
							<li class="ex"><a href="press.php">Press</a></li>
							<li><a href="awards.php">Awards</a></li>
							<li><a href="testimonials.php">Testimonials</a></li>
						</ul>
					</div>

					<div class="col-xs-6 col-sm-7">
						<ul>
							<h5>Editorial</h5>
							<li><a href="login.php">Login</a></li>
							<li><a href="addlyric.php">Add new Lyrics</a></li>
							<li><a href="addalbum.php">Add a new Album</a></li>
							<li class="ex"><a href="signup.php">Become a Member</a></li>
							<li><a href="editors.php">Meet the Editors</a></li>
							<li><a href="justadded.php">Recently Added</a></li>
							<li class="ex"><a href="activity.php">Activity Log</a></li>
							<li><a href="toplyrics.php">Most Popular</a></li>
                             <li class="ex"><a href="sell-your-lyrics.php">Sell Your Lyrics</a></li>
						</ul>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4">
				<div class="row">
					<div class="col-xs-6 col-sm-6">
						<ul>
							<h5>Services</h5>
							<li><a href="https://www.abbreviations.com/tools.php">Tools</a></li>
							<li class="ex"><a href="/playlist">Your Playlist</a></li>
							<li><a href="invite.php">Tell a Friend</a></li>
							<li><a id="page-bookmark" href="">Bookmark Us</a></li>							
							<li class="ex"><a href="promotion.php">Promote&nbsp;<i class="fa fa-bullhorn fa-1x"></i></a></li>
							<li><a href="lyrics_api.php">Lyrics API</a></li>
							<li class="ex"><a href="top-on-radio.php">Top on Radio&nbsp;<i class="fa fa-microphone fa-1x"></i></a></li>
							<li><a href="song-lyrics-generator.php">Lyrics Generator</a></li>
                            <li><a href="/marketplace">Marketplace&nbsp;<i class="fa fa-shopping-bag fa-1x"></i></a></li>
						</ul>
					</div>

					<div class="col-xs-6 col-sm-6">
						<ul class="last">
							<h5>Legal &amp; Contact</h5>
							<li><a href="terms.php">Terms of Use</a></li>
							<li><a href="privacy.php">Privacy Policy</a></li>
							<li><a href="contact.php">Contact Us</a></li>
							<li class="ex"><a href="advertise.php">Advertise</a></li>
							<li><a href="affiliate-program.php">Affiliate Program	</a></li>
						</ul>
					</div>
				</div>
			</div>

			
<div id="s4-network" class="col-xs-12 col-sm-5">
	<div  class="row">
		<h5 class="col-xs-12 col-sm-12">The STANDS4 Network</h5>
	</div>

	<!-- Desktop version -->
	<div class="clearfix row hidden-xs">
		<div class="col-xs-12 col-sm-8">
			<div class="row">
				<div class="col-xs-6 col-sm-6">
					<ul>
						<li class="nw-abbreviations"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.abbreviations.com/">Abbreviations</a></li>
						<li class="nw-convert"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.convert.net/">Conversions</a></li>
						<li class="nw-lyrics"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.lyrics.com/">Lyrics</a></li>
						<li class="nw-phrases"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.phrases.com/">Phrases</a></li>
						<li class="nw-references"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.references.net/">References</a></li>
						<li class="nw-symbols"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.symbols.com/">Symbols</a></li>
					</ul>
				</div>

				<div class="col-xs-6 col-sm-6">
					<ul>
						<li class="nw-anagrams"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.anagrams.net/">Anagrams</a></li>
						<li class="nw-definitions"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.definitions.net/">Definitions</a></li>
						<li class="nw-ua"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.literature.com/">Literature</a></li>
						<li class="nw-poetry"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.poetry.net/">Poetry</a></li>
						<li class="nw-rhymes"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.rhymes.net/">Rhymes</a></li>
						<li class="nw-synonyms"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.synonyms.com/">Synonyms</a></li>
					</ul>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-4">
			<ul class="last">
				<li class="nw-biographies"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.biographies.net/">Biographies</a></li>
				<li class="nw-grammar"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.grammar.com/">Grammar</a></li>
				<li class="nw-math"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="http://math.stands4.com/">Math</a></li>
				<li class="nw-quotes"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.quotes.net/">Quotes</a></li>
				<li class="nw-scripts"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.scripts.com/">Scripts</a></li>
				<li class="nw-uszip"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.uszip.com/">Zip Codes</a></li>
			</ul>
		</div>

	</div>

	<!-- Mobile version -->
	<div class="clearfix row hidden-sm hidden-md hidden-lg">
		<div class="col-xs-12 col-sm-8">
			<div class="row">
				<div class="col-xs-6 col-sm-6">
					<ul>
						<li class="nw-abbreviations"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.abbreviations.com/">Abbreviations</a></li>
						<li class="nw-anagrams"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.anagrams.net/">Anagrams</a></li>
						<li class="nw-biographies"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.biographies.net/">Biographies</a></li>
						<li class="nw-convert"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.convert.net/">Conversions</a></li>
						<li class="nw-definitions"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.definitions.net/">Definitions</a></li>
						<li class="nw-grammar"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.grammar.com/">Grammar</a></li>
						<li class="nw-ua"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.literature.com/">Literature</a></li>
						<li class="nw-lyrics"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.lyrics.com/">Lyrics</a></li>
						<li class="nw-math"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="http://math.stands4.com/">Math</a></li>
					</ul>
				</div>

				<div class="col-xs-6 col-sm-6">
					<ul>
						<li class="nw-phrases"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.phrases.com/">Phrases</a></li>
						<li class="nw-poetry"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.poetry.net/">Poetry</a></li>
						<li class="nw-quotes"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.quotes.net/">Quotes</a></li>
						<li class="nw-references"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.references.net/">References</a></li>
						<li class="nw-rhymes"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.rhymes.net/">Rhymes</a></li>
						<li class="nw-scripts"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.scripts.com/">Scripts</a></li>
						<li class="nw-symbols"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.symbols.com/">Symbols</a></li>
						<li class="nw-synonyms"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.synonyms.com/">Synonyms</a></li>
						<li class="nw-uszip"><svg class="s4-shape"><polygon points="0,1 10,0 10,15 0,9"></polygon><polygon points="20,1 10,0 10,15 20,9"></polygon></svg><a rel="nofollow" href="https://www.uszip.com/">Zip Codes</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="clearfix">
		<div class="copyright"><strong>&copy; 2001-2019 STANDS4 LLC.</strong><br>All rights reserved.</div>
		<div id="social-icons">
			<a rel="nofollow" href="https://www.facebook.com/STANDS4" target="_blank"><span class="social fb"><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
			<a rel="nofollow" href="https://twitter.com/lyrics_dot_com" target="_blank"><span class="social tw"><i class="fa fa-twitter" aria-hidden="true"></i></span></a>
			<a rel="nofollow" href="https://www.linkedin.com/company/stands4/" rel="publisher" target="_blank"><span class="social lkd"><i class="fa fa-linkedin" aria-hidden="true"></i></span></a>
		</div>
	</div>
</div>
		</div>
		
	</div>
</footer>

</div>
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Droid+Sans:400,700|Droid+Serif:400,700,400italic,700italic|Droid+Sans+Mono|Yanone+Kaffeesatz:200,300,400,700|Goudy+Bookletter+1911|Lobster+Two:400,700,400italic,700italic|Original+Surfer" media="all">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="root/app_common/css/smoothness/jquery-ui-1.11.3.custom.min.css">

<!--[if gt IE 8]>-->
<script src="root/app_common/js/libs/modernizr-2.8.3.custom.min.js"></script>
<!--<![endif]-->

<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script>window.jQuery || document.write('<script src="root/app_common/js/libs/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<script>window.jQuery || document.write('<script src="root/app_common/js/libs/jquery-ui-1.11.3.custom.min.js"><\/script>')</script>

<script src="root/app_common/js/libs/jquery.placeholder.min.js" async></script>


<script src="root/app_common/js/libs/wselect.min.js" async></script>
<!-- Bootstrap compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- <script src="https://use.fontawesome.com/1d5fda5f00.js" async></script> -->
<script src="js/lyrc.min.js?v=1.0.83" async></script>

<script src="/js/ads.js" type="text/javascript"></script>
<script src="root/app_common/js/s4-adsob.min.js"></script>

</body>
</html>
<!-- Timer: 0.0371 secs | Server: ip-172-30-5-142 -->