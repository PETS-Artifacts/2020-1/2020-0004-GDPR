<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="head"><title>
	CES.tech Privacy Statement - CES 2020
</title><meta name="description" content="Please read the CES.tech privacy statement carefully prior to using this website." /> 
<meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
<meta http-equiv="pragma" content="no-cache" /> 
<meta http-equiv="content-style-type" content="text/css" /> 
<meta http-equiv="content-script-type" content="text/javascript" /> 
<meta name="keywords" content="International CES, CES, CES 2010, CES 2011, CES 2012, CES 2013, CES 2014, CES 2015, CES 2016, CES 2017, CES 2018, Consumer Electronics Show, Consumer Technology, Consumer Electronics, Consumer Electronics Association, CES Las Vegas, ces show, ces vegas, CES Unveiled, ces tradeshow, ces conference, ces registration, ces exhibitor, ces innovation awards, innovation awards, best of ces, ces keynote, ces show floor, technology, silicon valley, technology innovation, disruptive technology, start-ups,CES, consumer electronics show, consumer technology, trade show, innovation, startups, business, networking, venture capitalists, global" /> 
<link href="/CMSPages/GetResource.ashx?stylesheetname=Blank_1" type="text/css" rel="stylesheet"/>
<!-- CSS -->
<link href="/CMSPages/GetResource.ashx?stylesheetname=ices-captions" type="text/css" rel="stylesheet">
<link href="/CMSPages/GetResource.ashx?stylesheetname=ices-slider-style" type="text/css" rel="stylesheet" media="screen">
<link href="/CMSPages/GetResource.ashx?stylesheetname=ices-flexslider" type="text/css" rel="stylesheet">
<link href="/CMSPages/GetResource.ashx?stylesheetname=ices-prettyphoto" type="text/css" rel="stylesheet">
<link href="/CMSPages/GetResource.ashx?stylesheetname=ices-settings" type="text/css" rel="stylesheet" media="screen">
<link href="/CMSPages/GetResource.ashx?stylesheetname=ices-fonts" type="text/css" rel="stylesheet" media="screen">
<link href="/CMSPages/GetResource.ashx?stylesheetname=ices-print" type="text/css" rel="stylesheet" media="print">

<!--Start Master Template / Root-->
<link href="/App_Themes/CES/style.min.css" type="text/css" rel="stylesheet" type="text/css">
<link href="/App_Themes/redesign/css/main.css" type="text/css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!--End Master Template / Root--><!-- open graph -->
<meta property="og:type" content="website" />
<meta property="og:site_name" content="CES" />
<meta property="og:url" content="http://www.ces.tech/privacy.aspx" />
<meta property="og:title" content="Privacy " />
<meta property="og:description" content="Please read the CES.tech privacy statement carefully prior to using this website.">
<meta property="og:image" content="http://www.ces.tech">
<!-- open graph -->

<meta property="twitter:account_id" content="10668202" />
<!-- Twitter Summary Card -->
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="@CES">
<meta name="twitter:creator" content="@CES">
<meta name="twitter:title" content="Privacy ">
<meta name="twitter:description" content="Please read the CES.tech privacy statement carefully prior to using this website.">
<meta name="twitter:image" content="http://www.ces.tech">
<!-- End of Twitter Summary Card -->

<link rel="apple-touch-icon-precomposed" sizes="57x57" href="https://cdn.ces.tech/ces/media/favicons/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://cdn.ces.tech/ces/media/favicons/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://cdn.ces.tech/ces/media/favicons/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://cdn.ces.tech/ces/media/favicons/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="https://cdn.ces.tech/ces/media/favicons/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://cdn.ces.tech/ces/media/favicons/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="https://cdn.ces.tech/ces/media/favicons/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://cdn.ces.tech/ces/media/favicons/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="https://cdn.ces.tech/ces/media/favicons/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="https://cdn.ces.tech/ces/media/favicons/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="https://cdn.ces.tech/ces/media/favicons/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="https://cdn.ces.tech/ces/media/favicons/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="https://cdn.ces.tech/ces/media/favicons/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="https://cdn.ces.tech/ces/media/favicons/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="https://cdn.ces.tech/ces/media/favicons/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="https://cdn.ces.tech/ces/media/favicons/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="https://cdn.ces.tech/ces/media/favicons/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="https://cdn.ces.tech/ces/media/favicons/mstile-310x310.png" /> 
<script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0044/9850.js" async="async"></script><!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '336104283403107');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=336104283403107&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code --><!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 865584843;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/865584843/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Twitter universal website tag code -->
<script>
!function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
},s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
// Insert Twitter Pixel ID and Standard Event data below
twq('init','nvf70');
twq('track','PageView');
</script>
<!-- End Twitter universal website tag code --><!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-3975226-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag()

{dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-3975226-4');
</script><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KSBFDB');</script>
</head>
<body class="LTR ENUS ContentBody" >
     <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KSBFDB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <form method="post" action="/privacy.aspx" id="form">
<div class="aspNetHidden">
<input type="hidden" name="__CMSCsrfToken" id="__CMSCsrfToken" value="DJryV+U9aKJATygAwU4eYxwQJtLNYWcJsatqF5Wk5DuVQsLI58XOCD9z5euzCxd+1MOtc/WZQJUGitALT8SGgMBH5Wm63+yrlHvB+SrPrAQ=" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />

</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form'];
if (!theForm) {
    theForm = document.form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV13He1Qd6_TZO3JTfxptV7ejZA-lEraTfajOzujnIjGHzQVeXCh_iip3i7wOdnAI3qoH6TtSKLjmQ2&amp;t=636765319264470882" type="text/javascript"></script>

<input type="hidden" name="lng" id="lng" value="en-US" />
<script type="text/javascript">
	//<![CDATA[

function PM_Postback(param) { if (window.top.HideScreenLockWarningAndSync) { window.top.HideScreenLockWarningAndSync(1080); } if(window.CMSContentManager) { CMSContentManager.allowSubmit = true; }; __doPostBack('m$am',param); }
function PM_Callback(param, callback, ctx) { if (window.top.HideScreenLockWarningAndSync) { window.top.HideScreenLockWarningAndSync(1080); }if (window.CMSContentManager) { CMSContentManager.storeContentChangedStatus(); };WebForm_DoCallback('m$am',param,callback,ctx,null,true); }
//]]>
</script>
<script src="/ScriptResource.axd?d=qph9tUZ6hGPLbkznkRkqTaTQxZV9uVF1k4ArO9PbWWXQWsdmq02TOtPFtJdtDtZdyeU1kwvf_tZ8Igq4m-fG2sAyk7OnyAZnyHWSEHa16mYNztKrij7l63LEyOqmVgfr0&amp;t=7c776dc1" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=TvpD2YGOOsCm1yWcLkKnBQ6cEqMgG-reC1U7zf_AcCAiKx19hh6dW15MAE1dTdF3Ij0JsyQnxZIb5fdAa2Dv0dz8xXIFltTdxkMcRnomfDdDs0WjPifk5I6_THPN56980&amp;t=7c776dc1" type="text/javascript"></script>
<script type="text/javascript">
	//<![CDATA[

var CMS = CMS || {};
CMS.Application = {
  "isRTL": "false",
  "isDebuggingEnabled": true,
  "applicationUrl": "/",
  "imagesUrl": "/CMSPages/GetResource.ashx?image=%5bImages.zip%5d%2f",
  "isDialog": false
};

//]]>
</script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A5343185" />
	<input type="hidden" name="__SCROLLPOSITIONX" id="__SCROLLPOSITIONX" value="0" />
	<input type="hidden" name="__SCROLLPOSITIONY" id="__SCROLLPOSITIONY" value="0" />
</div>
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('manScript', 'form', ['tctxM',''], [], [], 90, '');
//]]>
</script>

    <div id="ctxM">

</div>
    <div class="cols-12">
  <div class="nav-mobile nav-mobile-header d-xl-none">
    <nav class="navbar">
      <a class="navbar-brand" href="/">
          
           <img class="nav-mobile__logo" src="https://cdn.ces.tech/ces/media/new-images/ces-logo-black.png"> 
      </a>
      <button type="button" class="navbar-toggler" data-target="#nav-mobile-content" aria-controls="nav-mobile-content" aria-expanded="false" aria-label="Toggle navigation">
          
           <img class="nav-mobile__burger-toggle" src="/CES/new-design/img/icon/burger-dark.svg"> 
      </button>
    </nav>
</div>
  
<div class="cols-12">
    <div class="global-nav nav-mobile nav-mobile-content d-xl-none" id="nav-mobile-content">
        <div class="navbar navbar-expand">
            <a class="navbar-brand" href="/"><img class="nav-mobile__logo" src="https://cdn.ces.tech/ces/media/new-images/ces-logo-black.png"></a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <button type="button" class="navbar-toggler" data-target="#nav-mobile-content" aria-controls="nav-mobile-content" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fa fa-times"></i>
                    </button>
                </li>
            </ul>
            <div class="nav-mobile-content__search-container" id="nav-mobile-search-container">
                <a class="nav-mobile-content__search-button" id="nav-mobile-search-button"><i class="fa fa-search" style="color: #898989"></i></a>
                <input class="nav-mobile-content__search-input" id="nav-mobile-search-input" type="text" placeholder="SEARCH" onkeypress="return EnterEventMobile(event);">
            </div>
        </div>
        <div class="nav-mobile-content__brief text-center text-uppercase mt-3">
            <strong>Jan 7-10, 2020</strong>
            Las Vegas, NV
        </div>
        <div class="container-fluid">
          <div class="row">
<div class="col-12"><a class="btn ces-btn nav-mobile-content__button text-uppercase my-3" href="https://www.ces.tech/Logistics/Register-Now.aspx" target="_blank">Register</a></div>
</div><div class="row">
  <div class="col-12">
    <ul class="nav-mobile-content__main-links text-uppercase">
      <li>
        <a class="nav-mobile-content__link-category" href="" data-target="#mobile-nav-slideout-topics">
          Topics
          <i class="fa fa-angle-right nav-mobile-content__menu-caret pull-right"></i>
        </a>
      </li>
      <li>
        <a class="nav-mobile-content__link-category" href="" data-target="#mobile-nav-slideout-conference">
          Conference
          <i class="fa fa-angle-right nav-mobile-content__menu-caret pull-right"></i>
        </a>
      </li>
      <li>
        <a class="nav-mobile-content__link-category" href="" data-target="#mobile-nav-slideout-showfloor">
          Show Floor
          <i class="fa fa-angle-right nav-mobile-content__menu-caret pull-right"></i>
        </a>
      </li>
      <li>
        <a class="nav-mobile-content__link-category" href="" data-target="#mobile-nav-slideout-logistics">
          Logistics
          <i class="fa fa-angle-right nav-mobile-content__menu-caret pull-right"></i>
        </a>
      </li>
      <li>
        <a class="nav-mobile-content__link" href="/Schedule">
          Schedule
          
        </a>
      </li>
    </ul>
  </div>
</div><div class="nav-mobile-content__slideout-list position-fixed">
  <div class="nav-mobile-content__slideout position-fixed" id="mobile-nav-slideout-topics">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 text-center">
          <h3 class="nav-mobile-content__slideout-title text-uppercase">
            <a class="nav-mobile-content__menu-caret pull-left" href="" data-target="#mobile-nav-slideout-topics"><i class="fa fa-angle-left"></i></a>Topics
          </h3>
        </div>
      </div>
      
      <div class="row">
        <div class="col-12">
          <ul class="nav-mobile-content__topics-list nav-mobile-content__topics-list--main pl-0" id="mobile-nav-topics-accordion" data-children=".nav-mobile-content__topics-item">
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-techtopics" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Tech Topics</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-techtopics" role="tabpanel">
              <ul>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-5gandinternetofthingsiot" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">5G and Internet of Things (IoT)</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-5gandinternetofthingsiot" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/5G-and-Internet-of-Things-(IoT)/5G">5G</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/5G-and-Internet-of-Things-(IoT)/Resilience">Resilience</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/5G-and-Internet-of-Things-(IoT)/Smart-Cities">Smart Cities</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/5G-and-Internet-of-Things-(IoT)/Sustainability">Sustainability</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-advertisingentertainmentcontent" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Advertising, Entertainment &amp; Content</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-advertisingentertainmentcontent" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Advertising-Entertainment-Content/Entertainment-Content">Entertainment &amp; Content</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Advertising-Entertainment-Content/Marketing-Advertising">Marketing &amp; Advertising</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Advertising-Entertainment-Content/Music">Music</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-automotive" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Automotive</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-automotive" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Automotive/Self-Driving-Cars">Self-Driving Cars</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Automotive/Vehicle-Technology">Vehicle Technology</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-blockchain" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Blockchain</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-blockchain" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Blockchain/Cryptocurrency">Cryptocurrency</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-healthwellness" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Health &amp; Wellness</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-healthwellness" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Health-Wellness/Accessibility">Accessibility</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Health-Wellness/Digital-Health">Digital Health</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Health-Wellness/Fitness-Wearables">Fitness &amp; Wearables</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-homefamily" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Home &amp; Family</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-homefamily" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Home-Family/Family-Lifestyle">Family &amp; Lifestyle</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Home-Family/Home-Entertainment">Home Entertainment</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Home-Family/Smart-Home">Smart Home</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-immersiveentertainment" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Immersive Entertainment</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-immersiveentertainment" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Immersive-Entertainment/Augmented-Virtual-Reality">Augmented &amp; Virtual Reality</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Immersive-Entertainment/Gaming">Gaming</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-productdesignmanufacturing" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Product Design &amp; Manufacturing</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-productdesignmanufacturing" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Product-Design-Manufacturing/3D-Printing">3D Printing</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Product-Design-Manufacturing/Design-Sourcing-Packaging">Design, Sourcing &amp; Packaging</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-roboticsmachineintelligence" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Robotics &amp; Machine Intelligence</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-roboticsmachineintelligence" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Robotics-Machine-Intelligence/Artificial-Intelligence">Artificial Intelligence</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Robotics-Machine-Intelligence/Drones">Drones</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Robotics-Machine-Intelligence/Robotics">Robotics</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-sports" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Sports</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-sports" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Sports/eSports">Esports</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Sports/Sports-Technology">Sports Technology</a></li>
              </ul>
              </div>
            </li><!-- jay -->
			<li>
              <a class="nav-mobile-content__topics-item font-weight-bold collapsed text-uppercase" href="#mobile-nav-topics-accordion-startups" data-toggle="collapse" data-parent="#mobile-nav-topics-accordion">
                <div class="row">
                  <div class="col-10">Startups</div>
                  <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
                </div>
              </a>
              <div class="nav-mobile-content__accordion-content collapse" id="mobile-nav-topics-accordion-startups" role="tabpanel">
              <ul>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Startups/Investors">Investors</a></li>
                <li class="font-weight-normal"><a class="nav-mobile-content__accordion-link" href="/Topics/Startups/Startups">Startups</a></li>
              </ul>
              </div>
            </li><!-- jay -->
            <li>
              <a class="nav-mobile-content__topics-item font-weight-bold text-uppercase">Featured</a>
              <a class="global-nav__topics-feature d-block" href="/Topics/Advertising-Entertainment-Content/Marketing-Advertising.aspx"><img class="global-nav__topics-img" src="https://cdn.ces.tech/ces/media/topics/c-space/nav-c-space-tvs-half_580x204.jpg?ext=.jpg">
                <p class="global-nav__topics-label font-weight-bold">C Space</p></a>
              <a class="global-nav__topics-feature d-block" href="/Topics/Sports/Sports-Technology.aspx"><img class="global-nav__topics-img" src="https://cdn.ces.tech/ces/media/topics/sports-tech/nav-sports-zone.png?ext=.png">
                <p class="global-nav__topics-label font-weight-bold">CES Sports Zone</p></a>
              <a class="global-nav__topics-feature d-block" href="/Topics/Startups/Startups.aspx"><img class="global-nav__topics-img" src="https://cdn.ces.tech/ces/media/topics/startups/eureka-park-nav.jpg?ext=.jpg">
                <p class="global-nav__topics-label font-weight-bold">Eureka Park</p></a>
              <a class="global-nav__topics-feature d-block" href=""><img class="global-nav__topics-img" src="">
                <p class="global-nav__topics-label font-weight-bold"></p></a>
            </li>
          </ul>
        </div>        
      </div>
      
    </div>
  </div>
        
  <div class="nav-mobile-content__slideout position-fixed" id="mobile-nav-slideout-conference">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 text-center">
          <h3 class="nav-mobile-content__slideout-title text-uppercase">
            <a class="nav-mobile-content__menu-caret pull-left" href="" data-target="#mobile-nav-slideout-conference"><i class="fa fa-angle-left"></i></a>Conference
          </h3>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a class="arrow-link d-block pt-2 pb-4" href="/Conference/Conference-Program">
            Conference Program
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
              <title>arrow-black</title>
              <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z" transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
            </svg>
          </a>
          <ul class="nav-mobile-content__accordion-content px-0">
            <li><a class="nav-mobile-content__accordion-link" href="/Conference/Explore-Sessions">Explore Sessions</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Conference/Keynote-Addresses">Keynote Addresses</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Conference/SuperSessions-redirect">SuperSessions</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Conference/Speaker-Directory">Speaker Directory</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Conference/Featured-Speakers">Featured Speakers</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Conference/Speaker-Resources">Speaking at CES</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="nav-mobile-content__slideout position-fixed" id="mobile-nav-slideout-showfloor">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 text-center">
          <h3 class="nav-mobile-content__slideout-title text-uppercase">
            <a class="nav-mobile-content__menu-caret pull-left" href="" data-target="#mobile-nav-slideout-showfloor"><i class="fa fa-angle-left"></i></a>Show Floor
          </h3>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a class="arrow-link d-block pt-2 pb-4" href="/Show-Floor/Exhibit-Directory">
            Exhibitor Directory
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
              <title>arrow-black</title>
              <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z" transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
            </svg>
          </a>
          <ul class="nav-mobile-content__accordion-content px-0">
            <li><a class="nav-mobile-content__accordion-link" href="/Show-Floor/Show-Floor-Tours">Show Floor Tours</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Show-Floor/Marketplaces">Marketplaces</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Show-Floor/Official-Show-Locations">Official Show Locations</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="nav-mobile-content__slideout position-fixed" id="mobile-nav-slideout-logistics">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 text-center">
          <h3 class="nav-mobile-content__slideout-title text-uppercase">
            <a class="nav-mobile-content__menu-caret pull-left" href="" data-target="#mobile-nav-slideout-logistics"><i class="fa fa-angle-left"></i></a>Logistics
          </h3>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a class="arrow-link d-block pt-2 pb-4" href="/Logistics/Dates-and-Hours">
            Dates and Hours
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
              <title>arrow-black</title>
              <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z" transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
            </svg>
          </a>
          <ul class="nav-mobile-content__accordion-content px-0">
            <li><a class="nav-mobile-content__accordion-link" href="/Logistics/Registration-Information">Registration Information</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Logistics/Badge-Pickup">Badge Pickup</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Logistics/Transportation">Transportation</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Logistics/Hotels">Hotels</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Logistics/Travel-Tips">Travel Tips and Discounts</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Logistics/On-Site-Services">On-Site Services</a></li>
            <li><a class="nav-mobile-content__accordion-link" href="/Logistics/Safety-and-Security-at-CES">Safety and Security at CES</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="nav-mobile-content__slideout position-fixed" id="mobile-nav-slideout-schedule">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 text-center">
          <h3 class="nav-mobile-content__slideout-title text-uppercase">
            <a class="nav-mobile-content__menu-caret pull-left" href="" data-target="#mobile-nav-slideout-schedule"><i class="fa fa-angle-left"></i></a>Schedule
          </h3>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <a class="arrow-link d-block pt-2 pb-4" href="">
            
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
              <title>arrow-black</title>
              <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z" transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
            </svg>
          </a>
          <ul class="nav-mobile-content__accordion-content px-0">
          </ul>
        </div>
      </div>
    </div>
  </div>
  
</div>


<div class="row">
<div class="col-12"><p class="nav-mobile-content__info-title font-weight-bold mb-1">Information for:</p>

<ul class="nav-mobile-content__info-list text-uppercase font-weight-bold pl-0">
	<li><a class="nav-mobile-content__info-item d-block" href="/exhibitor">Exhibitors</a></li>
	<li><a class="nav-mobile-content__info-item d-block" href="/media">Media</a></li>
	<li><a class="nav-mobile-content__info-item d-block" href="/international">International</a></li>
</ul>
</div>
</div>
            <div class="row">
                <div class="col-12">
                    <ul class="nav-mobile-content__topics-list pl-0 mb-5" id="mobile-nav-util" data-children=".nav-mobile-content__topics-item">
                        <li>
  <a class="nav-mobile-content__topics-item nav-mobile-content__topics-item--util font-weight-bold text-uppercase collapsed" href="#mobile-nav-util-1" data-toggle="collapse" data-parent="#mobile-nav-util" aria-expanded="false">
    <div class="row">
      <div class="col-10">My Agenda</div>
      <div class="col-2"><i class="fa fa-angle-up pull-right nav-mobile-content__menu-caret"></i></div>
    </div>
  </a>
  <ul class="nav-mobile-content__accordion-content collapse" id="mobile-nav-util-1">
    <li><a href="/create-account" class="nav-mobile-content__accordion-link">Create Account</a></li>
    <li>
      <a href="/login?returnurl=%2fmy-agenda.aspx" class="arrow-link text-uppercase nav-mobile-content__accordion-link">
        Sign In
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
<title>arrow-black</title>
<path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z" transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
</svg>
      </a>
    </li>
  </ul>
</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="nav-mobile-content__slideout-list position-fixed">
            
        </div>
    </div>
</div>
  
<div class="cols-12">
    
     <div class="container-fluid nav-desktop d-none d-xl-block nav-desktop--dark"> 
        <div class="row nav-desktop-secondary">
            <div class="col-12">
                <ul class="nav-desktop__util d-inline-block pull-left pl-0">
	<li class="nav-desktop__util-item">Information for:</li>
	<li class="nav-desktop__util-item text-uppercase"><a href="/Exhibit.aspx">Exhibitors</a></li>
	<li class="nav-desktop__util-item text-uppercase"><a href="/media">Media</a></li>
	<li class="nav-desktop__util-item text-uppercase"><a href="/international">International</a></li>
</ul>

                <ul class="nav-desktop__util d-inline-block pull-right pl-0">
                    <li class="nav-desktop__util-item caret-dropdown">
  <a class="text-uppercase" href="javascript:void(0)">My Agenda<i class="fa fa-caret-down ml-1"></i></a>
  <div class="nav-desktop__panel nav-desktop__panel--agenda">
    <ul class="nav-desktop__panel-list pl-0">
      <li class="nav-desktop__panel-list-item"><a href="/login?returnurl=%2fmy-agenda.aspx">Create Account</a></li>
      <li class="nav-desktop__panel-list-item nav-desktop__panel-list-item--bold"><a href="/login?returnurl=%2fmy-agenda.aspx">Sign In</a>
      </li>
    </ul>
  </div>
</li><li class="nav-desktop__util-item caret-dropdown">
  <a class="text-uppercase ml-1" id="nav-desktop-search-link" href="javascript:void(0)">
    <i class="fa fa-search mr-1"></i>Search</a>
  <div class="nav-desktop__panel nav-desktop__panel--search">
    <div class="mb-0" action="" method="GET" _lpchecked="1">
      <input class="nav-desktop__search" id="nav-desktop-search-field" type="text" placeholder="What would you like to find?" onkeypress="return EnterEvent(event);">
      <button class="d-none" type="submit"></button>
      <button class="nav-desktop__search-icon" id="nav-desktop-search-button">
        <img src="/CES/new-design/img/icon/times.svg"></button>
    </div>
  </div>
</li>
                </ul>
            </div>
        </div>
        <div class="nav-desktop-primary">
            <div class="row">
                <div class="col-12">
                    <nav class="navbar">
                        <div class="navbar-brand">
                          <a href="/">
                              
                               <img src="https://cdn.ces.tech/ces/media/new-images/ces-logo-black.png"> 
                          </a>
                        </div>
                        <ul class="navbar-nav mr-auto">
                            <!-- Topics Nav starts here -->
<li class="nav-item caret-dropdown">
  <a class="nav-link">Topics</a>
  <div class="nav-desktop__panel nav-desktop__panel--large nav-desktop__panel--featured">
    <div class="row">
      <div class="col">
        <ul class="nav-desktop__topics-list pl-0">
          <li class="nav-desktop__topics-list-item">5G and Internet of Things (IoT)</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/5G-and-Internet-of-Things-(IoT)/5G">5G</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/5G-and-Internet-of-Things-(IoT)/Resilience">Resilience</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/5G-and-Internet-of-Things-(IoT)/Smart-Cities">Smart Cities</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/5G-and-Internet-of-Things-(IoT)/Sustainability">Sustainability</a></li>
            </ul>
          </li>
          <li class="nav-desktop__topics-list-item">Advertising, Entertainment &amp; Content</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Advertising-Entertainment-Content/Entertainment-Content">Entertainment &amp; Content</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Advertising-Entertainment-Content/Marketing-Advertising">Marketing &amp; Advertising</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Advertising-Entertainment-Content/Music">Music</a></li>
            </ul>
          </li>
          <li class="nav-desktop__topics-list-item">Automotive</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Automotive/Self-Driving-Cars">Self-Driving Cars</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Automotive/Vehicle-Technology">Vehicle Technology</a></li>
            </ul>
          </li>
      </ul>
      </div>          
      <div class="col">
        <ul class="nav-desktop__topics-list pl-0">
          <li class="nav-desktop__topics-list-item">Blockchain</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Blockchain/Cryptocurrency">Cryptocurrency</a></li>
            </ul>
          </li>
          <li class="nav-desktop__topics-list-item">Health &amp; Wellness</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Health-Wellness/Accessibility">Accessibility</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Health-Wellness/Digital-Health">Digital Health</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Health-Wellness/Fitness-Wearables">Fitness &amp; Wearables</a></li>
            </ul>
          </li>
          <li class="nav-desktop__topics-list-item">Home &amp; Family</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Home-Family/Family-Lifestyle">Family &amp; Lifestyle</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Home-Family/Home-Entertainment">Home Entertainment</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Home-Family/Smart-Home">Smart Home</a></li>
            </ul>
          </li>
          <li class="nav-desktop__topics-list-item">Immersive Entertainment</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Immersive-Entertainment/Augmented-Virtual-Reality">Augmented &amp; Virtual Reality</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Immersive-Entertainment/Gaming">Gaming</a></li>
            </ul>
          </li>
      </ul>
      </div>          
      <div class="col">
        <ul class="nav-desktop__topics-list pl-0">
          <li class="nav-desktop__topics-list-item">Product Design &amp; Manufacturing</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Product-Design-Manufacturing/3D-Printing">3D Printing</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Product-Design-Manufacturing/Design-Sourcing-Packaging">Design, Sourcing &amp; Packaging</a></li>
            </ul>
          </li>
          <li class="nav-desktop__topics-list-item">Robotics &amp; Machine Intelligence</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Robotics-Machine-Intelligence/Artificial-Intelligence">Artificial Intelligence</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Robotics-Machine-Intelligence/Drones">Drones</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Robotics-Machine-Intelligence/Robotics">Robotics</a></li>
            </ul>
          </li>
          <li class="nav-desktop__topics-list-item">Sports</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Sports/eSports">Esports</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Sports/Sports-Technology">Sports Technology</a></li>
            </ul>
          </li>
          <li class="nav-desktop__topics-list-item">Startups</li>
          <li>
            <ul class="nav-desktop__topics-sublist pl-0">
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Startups/Investors">Investors</a></li>
              <li class="nav-desktop__topics-sublist-item"><a href="/Topics/Startups/Startups">Startups</a></li>
            </ul>
          </li>
      </ul>
      </div>          
      <div class="col featured">
        <ul class="nav-desktop__topics-list pl-0">
          <li class="nav-desktop__topics-list-item">Featured</li>
        </ul>
        <a class="global-nav__topics-feature d-block" href="/Topics/Advertising-Entertainment-Content/Marketing-Advertising.aspx">
          <img class="global-nav__topics-img" src="https://cdn.ces.tech/ces/media/topics/c-space/nav-c-space-tvs-half_580x204.jpg?ext=.jpg">
          <p class="global-nav__topics-label font-weight-bold">C Space</p>
        </a>
        <a class="global-nav__topics-feature d-block" href="/Topics/Sports/Sports-Technology.aspx">
          <img class="global-nav__topics-img" src="https://cdn.ces.tech/ces/media/topics/sports-tech/nav-sports-zone.png?ext=.png">
          <p class="global-nav__topics-label font-weight-bold">CES Sports Zone</p>
        </a>
        <a class="global-nav__topics-feature d-block" href="/Topics/Startups/Startups.aspx">
          <img class="global-nav__topics-img" src="https://cdn.ces.tech/ces/media/topics/startups/eureka-park-nav.jpg?ext=.jpg">
          <p class="global-nav__topics-label font-weight-bold">Eureka Park</p>
        </a>
        <a class="global-nav__topics-feature d-block" href="">
          <p class="global-nav__topics-label font-weight-bold"></p>
        </a>
      </div>
  </div>
  <script>
    jQuery(document).ready(function($) {
      if($('.nav-desktop__panel .col:last-child').hasClass('featured')) {} 
      else {
        $('.nav-desktop__panel').removeClass('nav-desktop__panel--featured');
      }
    });
  </script>
</li>
<!-- Topics Nav ends here -->          

<!-- Main Nav starts here -->
<li class="nav-item caret-dropdown position-relative">
  <a class="nav-link">Conference</a>
  <div class="nav-desktop__panel nav-desktop__panel--main">
    <div class="row">
      <div class="col">
        <ul class="nav-desktop__topics-list pl-0">
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Conference/Conference-Program" class="arrow-link text-uppercase">Conference Program
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
                <title>arrow-black</title>
                <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z" transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
              </svg>
            </a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Conference/Explore-Sessions">Explore Sessions</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Conference/Keynote-Addresses">Keynote Addresses</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Conference/SuperSessions-redirect">SuperSessions</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Conference/Speaker-Directory">Speaker Directory</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Conference/Featured-Speakers">Featured Speakers</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Conference/Speaker-Resources">Speaking at CES</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</li>
<li class="nav-item caret-dropdown position-relative">
  <a class="nav-link">Show Floor</a>
  <div class="nav-desktop__panel nav-desktop__panel--main">
    <div class="row">
      <div class="col">
        <ul class="nav-desktop__topics-list pl-0">
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Show-Floor/Exhibit-Directory" class="arrow-link text-uppercase">Exhibitor Directory
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
                <title>arrow-black</title>
                <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z" transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
              </svg>
            </a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Show-Floor/Show-Floor-Tours">Show Floor Tours</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Show-Floor/Marketplaces">Marketplaces</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Show-Floor/Official-Show-Locations">Official Show Locations</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</li>
<li class="nav-item caret-dropdown position-relative">
  <a class="nav-link">Logistics</a>
  <div class="nav-desktop__panel nav-desktop__panel--main">
    <div class="row">
      <div class="col">
        <ul class="nav-desktop__topics-list pl-0">
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Logistics/Dates-and-Hours" class="arrow-link text-uppercase">Dates and Hours
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
                <title>arrow-black</title>
                <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z" transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
              </svg>
            </a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Logistics/Registration-Information">Registration Information</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Logistics/Badge-Pickup">Badge Pickup</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Logistics/Transportation">Transportation</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Logistics/Hotels">Hotels</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Logistics/Travel-Tips">Travel Tips and Discounts</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Logistics/On-Site-Services">On-Site Services</a>
          </li>
          <li class="nav-desktop__topics-sublist-item">
            <a href="/Logistics/Safety-and-Security-at-CES">Safety and Security at CES</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</li>
<li class="nav-item">
  <a class="nav-link" href="/Schedule">Schedule</a>
</li>
<!-- Main Nav ends here -->
                        </ul>
                        <div class="navbar-nav nav-desktop__cta-block">
                            <div class="nav-desktop__date-place text-uppercase text-right">
                                <div class="nav-desktop__date">Jan 7-10, 2020



</div><div class="nav-desktop__place">Las Vegas, NV



</div>
                            </div>
                            <a href="/Logistics/Register-Now.aspx" class="nav-desktop__cta text-uppercase">Register</a>
                        </div>
                    </nav>
                </div><!--/col-12-->
            </div><!--/row-->
        </div><!--/nav-desktop-primary-->
    </div><!--/nav-desktop-->
    </div>
</div><!--/cols-12-->



<div class="cols-12 white-bg">
    <header class="global-hero text-block-font-styles">
    <section class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <h1 class="global-hero__title">Privacy </h1>
                        
                    </div>
                </div>
                <hr class="global-hero__rule">
            </div>
        </div>
    </section>
</header>
    <main>
        <section class="text-block"><div class="container"><div class="row"><div class="col-12"><em>Last Modified: August 6, 2018 (<a href="https://www.ces.tech/Privacy/Privacy-Policy-Archive.aspx">View Archived Versions</a>)&nbsp;</em>
<p><br />
At the Consumer Technology Association (CTA)&trade;, we respect your privacy and are committed to protecting your data. This Privacy Policy describes how CTA collects and uses your personal data, and sets forth your privacy rights.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">When This Privacy Policy Applies</h2></div><div class="col-12 col-lg-9"><p>This Privacy Policy applies to websites and applications (&ldquo;Services&rdquo;) offered by CTA including CTA.TECH, CES.TECH, MECP.ORG, CTA-branded apps, CTA events, and CES&reg; events but excludes services that have separate privacy policies that do not incorporate this Privacy Policy or to information that we collect by other means (including offline) or from other sources.</p>

<p>Our Services may contain features or links to websites and services provided by third parties. Our Privacy Policy does not apply to services and products offered by other companies and individuals. Any information you provide through third-party sites or services is provided directly to the operators of such services and is subject to those operators&rsquo; policies, if any, governing privacy and security, even if accessed through our Services. We are not responsible for the privacy policies and practices of other websites, even if you access those links from a CTA Service.&nbsp;We encourage you to learn about third parties&rsquo; privacy and security policies before providing them with information.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">Updates to This Privacy Policy</h2></div><div class="col-12 col-lg-9"><p>We may update this Privacy Policy when necessary to reflect changes in our Services, to implement changes required by applicable law, or as we may otherwise deem necessary or appropriate. If you continue to use our Services after those changes are in effect, you agree to the revised policy. If the changes are significant, we will notify you either by prominently posting a notice of such changes before they take effect. We encourage you to periodically review the Privacy Policy to learn how CTA is protecting your information. We will also keep prior versions of this Privacy Policy in an archive for your review.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">Collection and Use of&nbsp;Information</h2></div><div class="col-12 col-lg-9"><p>You may access and use many of our Services without telling us who you are or revealing any personal information about yourself. However, some Services may require you to provide us with personal information (referred to for convenience as &quot;Submission Pages&quot;).</p>

<p>These Submission Pages collect information that you give us, for example, when you create an account with us; register for one of our events, such as CES or Innovate Celebrate (and these events will each have specific and additional terms and conditions that each attendee will be required to accept as part of the registration process); download one of our publications; purchase a product; complete a form on the website; or subscribe to our publications and newsletters like&nbsp;<em>i3</em>&nbsp;or Tech5. The information collected includes (but is not limited to) your name, job title, employer name, address, email address, phone number, credit card or account information, photos, and information about your interests in and use of various products. In these situations, if you choose to withhold any personal data requested by us, it may not be possible for you to gain access to certain Services and for us to respond to you.</p>

<p>We also collect information about you when you use our Service, for example, when you visit our websites, or view and interact with our ads and content. This information may include internet protocol (IP) addresses, the region or general location where your computer or device is accessing the internet, browser type, operating system, other information that is provided by the end user device and information about the use of the CTA&rsquo;s services, including a history of the pages you view. We also use cookies and similar technologies, and you may view our <a href="https://www.ces.tech/Privacy/Cookie-Policy.aspx">Cookie Policy</a>.<br />
<br />
Additionally, we may use third-party service providers to include social media features and widgets, analytic tools, or targeted advertising in our Services. These service providers operate independently and may collect your IP address and use cookies. Interactions with these features are governed by the service providers&rsquo; own privacy policies. More information on this is outlined in our <a href="https://www.ces.tech/Privacy/Cookie-Policy.aspx">Cookie Policy</a>.<br />
<br />
From time to time, CTA receives personal information about individuals from authorized third parties and sources other than through a CTA Service, such as our partners. This may happen, for example, if:<br />
&nbsp;</p>

<ul>
	<li>
	<p>Your employer is a member of CTA and signs you up for an event.</p>
	</li>
	<li>
	<p>Your employer is an exhibitor at a CTA event and signs you up for the event.</p>
	</li>
	<li>
	<p>You choose to participate in a CES Delegation and provide authorization to your delegate coordinator to sign you up for the event.</p>
	</li>
	<li>
	<p>You participate in an event which is co-sponsored or co-hosted by CTA.</p>
	</li>
	<li>
	<p>Instead of registering directly on our website, you choose to register with LinkedIn, which authorizes LinkedIn to share information that includes (but is not limited to) your name, your profile picture, your company name, your email address, and your LinkedIn ID number.<br />
	<br />
	If you give us information about another person, you confirm that the other person has appointed you to act on their behalf and agreed that you may consent to the processing of their personal data.&nbsp;</p>

	<p>We use the information we collect in a variety of ways in providing the Services and operating our business. We process your personal information for account administration, to deliver the products and services you require, and to inform you of CTA-related events, meetings, content, initiatives and other benefits or opportunities associated with CTA and the industry. We may also use this information to help understand your needs and interests to better tailor our products and services to meet your needs. We will use your information to respond to your questions. If we get personal information from a source outside of CTA Services, we will treat the personal information in accordance with this Policy.</p>
	</li>
</ul>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">When Do We Disclose Your Data to Third Parties?</h2></div><div class="col-12 col-lg-9"><p>We do not share your personal information with others except as indicated within this Policy or when we inform you. We do not sell personal information to anyone and only share your personal data with third parties for their use in authorized and necessary instances:<br />
You authorize the sharing of your data by registering to an event or conference where we share data with event co-sponsors. In these instances, you will be asked to review specific and additional terms and conditions as part of the registration process.</p>

<ul>
	<li>
	<p>You authorize the sharing of data by allowing exhibitors to scan your attendee badge at events. Again, in these instances, the event terms and conditions will outline this as part of the registration process.</p>
	</li>
	<li>
	<p>You request a special service with us. This typically relates to conference attendees who request additional services like accessibility or transportation services.</p>
	</li>
	<li>
	<p>You are the contact person for your company that registers for an event. We will share your information with third-party service providers, agents or contractors to perform services on our behalf or to assist us with providing services to you. This relates to exhibition coordinators whose contact information is shared with companies that provide show services for services such as booth setup, transportation, electricity and similar exhibition services.</p>
	</li>
	<li>
	<p>The information is provided to third-party service providers and Data Processors who perform functions on our behalf to process your data for one of the above services. These third parties may have access to or process your personal information as part of providing those services for us. Examples include companies that provide registration services for our events and companies that help to send our marketing email messages.</p>
	</li>
	<li>
	<p>Any personally identifiable information that you elect to make publically available through our Services, will be available to others.</p>
	</li>
	<li>
	<p>If you consent to CTA sharing your personal information.</p>
	</li>
	<li>
	<p>The information is provided in good faith to: comply with the law and the reasonable requests of law enforcement or government agencies; to enforce our Terms of Service or an agreement we have with you; to take precautions against liability; and/or to protect our rights, property or safety, or the rights, property or safety of our employees or others.</p>
	</li>
</ul>

<p>When we share information with third parties, we limit the information provided to what is reasonably necessary for them to perform their functions, and our contracts require them to maintain the confidentiality of such information.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">Legal Basis</h2></div><div class="col-12 col-lg-9"><p>As stated above, we collect different types of information from or through our Services. The legal bases for processing of personal data are primarily to enable us to provide our services to you.<br />
<br />
<em>Legitimate Business Interests</em>:&nbsp;To provide you with information relating to our Services. To notify you about any changes to our website, such as improvements or service changes, that may affect our Services. If you are an existing member or customer, we may contact you with information about services similar to those provided to you by us in the past.&nbsp;<br />
<br />
<em>Performance of Contract</em>:&nbsp;To meet our contractual commitments to you and in performance of contractual obligations to you.<br />
<br />
<em>User Consent</em>:&nbsp;We may use your data so that you can be provided with information about services that may be of interest to you. We will follow applicable laws and regulations regarding consent to receive this information.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">Your Choices and Control</h2></div><div class="col-12 col-lg-9"><p>Under privacy laws, data subjects have certain rights. This Privacy Policy is intended to give transparency about what personal data CTA collects and how it is used, enabling you to make meaningful choices.<br />
Please contact us by using the details set out at the Contact Us section below if you wish to do any of the following:</p>

<ul>
	<li>
	<p>Confirm that CTA is processing your personal data, or to have access to the personal data CTA may have about you. Reasonable access to your personal data will be provided at no cost upon request.</p>
	</li>
	<li>
	<p>Correct any personal data that CTA has that is inaccurate.</p>
	</li>
	<li>
	<p>Request that CTA erase your personal data or stop processing it. Please note that in some cases, we may not be able to remove your personal information, in which case we will let you know if we are unable to do so and why.</p>
	</li>
	<li>
	<p>Assert a complaint.</p>
	</li>
</ul>

<p>We provide many choices in relation to our marketing communications via an unsubscribe option on every marketing email, but you may also request that CTA stop using your data for direct marketing purposes by using our <a href="https://cea.formstack.com/forms/optout">Email Opt-Out Form</a>.&nbsp;If you opt-out of marketing emails, we may still send you transactional and administrative emails about the Services you use.<br />
CTA retains personal data for the duration of the customer&rsquo;s or member&rsquo;s business relationship with CTA and up to five years after the relationship ends. Subsequently, the data is stored for archival uses only.</p>

<p>CTA&rsquo;s headquarters, servers, this website and the majority of CTA Services are located and operated in the United States.&nbsp;If you are using the Services from outside the US, please be aware that your information will be transferred to, stored, and processed in the United States unless otherwise stated. By using our Services and providing us with information, you consent to this transfer, processing and storage of your information in the United States.&nbsp;It is important to note that data privacy laws vary worldwide and some may not be as comprehensive as others. When personal data is transferred to the US, we will adequately protect it as described in this Privacy Policy.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">Security</h2></div><div class="col-12 col-lg-9"><p>We are committed to protecting the security of your personal information. We have implemented appropriate physical, technical and administrative safeguards. We train our employees about the importance of confidentiality and maintaining the privacy and security of your information. We follow generally accepted industry standards to protect the information submitted to us, both during transmission and once we receive it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. We cannot ensure or warrant the security of any information you transmit to us, and you do so at your own risk. If you believe your Personal Data has been compromised, please contact us. In the event that personal information is compromised, we will promptly notify our customers in compliance with applicable law.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">Sensitive Data</h2></div><div class="col-12 col-lg-9"><p>We request that you do not send us any sensitive data such as social security or national identification numbers, information related to racial or ethnic origin, political opinions, religious beliefs, health data, biometrics or genetic, criminal background or trade union membership information. If you do send us this information, then you are consenting to its processing in accordance with this Privacy Policy.&nbsp; To avoid processing of sensitive data, do not submit it.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">Children&#39;s Privacy</h2></div><div class="col-12 col-lg-9"><p>Protecting the privacy of minors is especially important. Our site is not directed towards minors and CTA does not knowingly collect or solicit personal information from minors. If you believe that we might have any information from or about a minor, please contact us and we will delete that information.</p>
</div></section><section class="text-block"><div class="container"><div class="row"><div class="col-12 col-lg-3"><h2 class="text-block__title">Contact Us</h2></div><div class="col-12 col-lg-9"><p>If at any time you would like to contact us with your views about our privacy practices, or with any inquiry relating to your personal information, please send an e-mail to <a href="mailto:dataprivacy@cta.tech">dataprivacy@cta.tech</a>&nbsp;or write to Data Privacy &ndash; Legal, CTA, 1919 S. Eads Street, Arlington, VA 22202, United States.</p>
</div></section>
        <section class="link-list">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-3">
        <p class="eyebrow">More in this Section</p>
        <h2 class="link-list__title">Privacy </h2>
        <p class="link-list__link-container">
          <a class="arrow-link " href="/Privacy/Cookie-Policy">Cookie Policy
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
              <title>arrow-black</title>
              <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z"
                transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
            </svg>
          </a>
        </p>
      </div>
      <div class="col-12 col-lg-3 offset-lg-1">
          <p class="link-list__link-container">
            <a class="arrow-link " href="/Privacy/Privacy-Policy-Archive">Privacy Policy Archive
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 11">
              <title>arrow-black</title>
              <path d="M16.18,1A0.28,0.28,0,0,0,16,1.5L21,5.73H1.3A0.29,0.29,0,0,0,1,6a0.28,0.28,0,0,0,.3.25H21L16,10.51a0.28,0.28,0,0,0,0,.39,0.28,0.28,0,0,0,.39,0L21.9,6.21a0.28,0.28,0,0,0,0-.43L16.38,1.07A0.27,0.27,0,0,0,16.18,1Z"
                transform="translate(-0.5 -0.5)" fill="" stroke=""></path>
              </svg>
            </a>
          </p>
      </div>
      <div class="col-12 col-lg-3 offset-lg-1">
      </div>
    </div>
  </div>
</section>

    </main>
</div>



<div class="cols-12">
  <footer class="footer" id="footer">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <img class="footer__logo" src="https://cdn.ces.tech/ces/media/new-images/ces-logo-grey_1.png" alt="CES Logo">
        </div>
        <div class="col-12 col-lg-4 col-xl-3 mr-lg-auto">
          <p class="small">CES is owned and produced by the Consumer Technology Association, which provides the ultimate platform for technology leaders to connect, collaborate, and propel consumer technology forward.



</p><p class="footer__member-link">
  <a href="/About-CES/CTA-Member-Benefits">
      Become a CTA Member
      <i class="fa fa-external-link" aria-hidden="true"></i>
  </a>
</p>
        </div>
        <div class="col-12 col-lg-3">
          
<p class="footer__link">
    <a href="/navigation-promo/About-CES.aspx">About CES</a>
</p>
<p class="footer__link">
    <a href="/navigation-promo/CES-Unveiled.aspx">CES Unveiled</a>
</p>
<p class="footer__link">
    <a href="/navigation-promo/Innovation-Awards.aspx">Innovation Awards</a>
</p>
<p class="footer__link">
    <a href="/navigation-promo/Garys-Book-Club.aspx">Gary's Book Club</a>
</p>
<p class="footer__link">
    <a href="/navigation-promo/CES-Tech-Talk-Podcast.aspx">CES Tech Talk Podcast</a>
</p>
<p class="footer__link">
    <a href="/navigation-promo/Promote-Your-Brand.aspx">Promote Your Brand</a>
</p>
<p class="footer__link">
    <a href="/Navigation/2nd-Column/Success-Stories.aspx">CES Success Stories</a>
</p>


        </div>
        <div class="d-none d-lg-block col-lg-3">
          
<p class="footer__link">
    <a href="/navigation-section/Topics.aspx">Topics</a>
</p>
<p class="footer__link">
    <a href="/navigation-section/Conference.aspx">Conference</a>
</p>
<p class="footer__link">
    <a href="/navigation-section/Show-Floor.aspx">Show Floor</a>
</p>
<p class="footer__link">
    <a href="/navigation-section/Logistics.aspx">Logistics</a>
</p>
<p class="footer__link">
    <a href="/Navigation/3rd-Column/Schedule.aspx">Schedule</a>
</p>
<p class="footer__link">
    <a href="/navigation-section/Partners.aspx">Our Partners</a>
</p>


        </div>
        <div class="col-12 col-lg-2">
          <p class="footer__title footer__title--first">Information for:</p>

<p class="footer__link"><a href="/exhibitor.aspx">Exhibitors</a></p>

<p class="footer__link"><a href="/media.aspx">Media</a></p>

<p class="footer__link"><a href="/international.aspx">International</a></p>

<p class="footer__title footer__title--second">Access the:</p>

<p class="footer__link"><a href="https://speaker.ces.tech" target="_blank">Speaker Portal</a></p>

        </div>
      </div>
      <hr class="d-none d-lg-block">
      <div class="row">
        <div class="col-12 col-lg-4 col-xl-3 footer__external-col">
    <p class="footer__external-title">Follow CES</p>
    <p class="footer__external-icon-link">
        <a href="https://www.facebook.com/CES"><i class="fa fa-facebook" aria-hidden="true"></i></a>
    </p>
    <p class="footer__external-icon-link">
        <a href="http://www.youtube.com/user/cesonthetube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
    </p>
    <p class="footer__external-icon-link">
        <a href="https://www.instagram.com/ces/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </p>
    <p class="footer__external-icon-link">
        <a href="https://twitter.com/ces"><i class="fa fa-twitter" aria-hidden="true"></i></a>
    </p>
</div><div class="col-12 col-lg-3 footer__external-col">
    <p class="footer__external-title">Get the app</p>
    <p class="footer__external-icon-link"><a href="https://itunes.apple.com/ca/app/ces-2019/id928640355?mt=8" target="_blank"><i class="fa fa-apple" aria-hidden="true"></i></a></p>
    <p class="footer__external-icon-link"><a href="https://play.google.com/store/apps/details?id=com.cta.ces&hl=en" target="_blank"><i class="fa fa-android" aria-hidden="true"></i></a></p>
</div><div class="col-12 col-lg-4 ml-lg-auto footer__copyright">
<p class="small">
<a href="/Navigation/Utility/Contact-Us-(1).aspx">Contact Us</a>
<span class="footer__vertical-rule"></span><a href="/Navigation/Utility/Code-of-Conduct.aspx">Code of Conduct</a>
<span class="footer__vertical-rule"></span><a href="/Navigation/Utility/Terms-of-Use.aspx">Terms of Use</a>
<span class="footer__vertical-rule"></span><a href="/Navigation/Utility/Privacy-Policy.aspx">Privacy</a>
<span class="footer__vertical-rule"></span><a href="/Sitemap.aspx">Sitemap</a>

</p>
<p class="small">Copyright © 2003 - 2019. All rights reserved.</p>
</div>
      </div>
    </div>
  </footer>
</div>



<script type="text/javascript"> _linkedin_data_partner_id = "96062"; </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=96062&fmt=gif" /> </noscript><div style="display:none;"><span id="p_lt_WebPartZone14_zoneExtras_whatServer_serverName">WEB-IIS70</span></div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/scrollreveal@3.3.6/dist/scrollreveal.min.js"></script>
<script src="/CES/new-design/js/app.js"></script>
<script>
  //Searchbox: enable functionality on keypress enter and redirect
  function EnterEvent(e) {
      if (e.keyCode == 13) {
          var val = document.getElementById('nav-desktop-search-field').value;
          var sURL = '/search-results?searchtext=' + val + '&searchmode=anyword';
          window.location.href = sURL;
          return false;
      }
  }
  //Searchbox: enable functionality on keypress at mobile
  function EnterEventMobile(e) {
      if (e.keyCode == 13) {
          var val = document.getElementById('nav-mobile-search-input').value;
          var sURL = '/search-results?searchtext=' + val + '&searchmode=anyword';
          window.location.href = sURL;
          return false;
      }
  }

</script>
  
<!--Start Cookie Script-->
<script type="text/javascript" charset="UTF-8" src="//cookie-script.com/s/8bd2b29957a6661b8dd0f387777c4929.js"></script>
<!--End Cookie Script-->
    
    

<script type="text/javascript">
//<![CDATA[

var callBackFrameUrl='/WebResource.axd?d=beToSAE3vdsL1QUQUxjWdZMPfqkYA07YHyNQbkaxqVYaDpUYr0je8zYHP-0A5FKFoK8L02dz0Z9N1LA06s9lfg2&t=636765319264470882';
WebForm_InitCallback();
theForm.oldSubmit = theForm.submit;
theForm.submit = WebForm_SaveScrollPositionSubmit;

theForm.oldOnSubmit = theForm.onsubmit;
theForm.onsubmit = WebForm_SaveScrollPositionOnSubmit;
//]]>
</script>
  <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="X6yuV445OQ5dgAYHUDWjtn09YeTmQ09kWJOtNAzzp0M9lKQBkzqOmUx6aAZLUNnVyFLlKv302Rqfzh049K25qJVlob9uTBIweKw74O9z7SyKNcq4KDOTIs6zKoo3vOkmFwLR4+mraPvqc0rYOutwcXy11gQ5GJjG19PljF2JVh7INL/jzqZjoE46CPcJDsAZWSrLPwrk0DFjxVAgPjB2Av4D84eJCMK2rYGo9FJBat0LL+kkQVmyDGyatyJPz/Nwe3v7p/HlpRaydt7ywtBqKwGPprJcVCL5cC+hg1vFlRO/qllly6nfSPki5uwLg0hTHrFnB4dvpngRZadUIH3Dgd3T8yLmE8sYY8nTnhEkpGYOYFNulZ0ydDkcuXXKBsTVw0nqRtkX2W1aCAyeuhXz3tIMNqg+uGxIUMMM9mBk1yRMs1EuBk/1eQeVP0FlRdk0XVFp4YHppQtF9FVQ+LZI6LIkE99s/KdH64lkkrfQC99E7m76zh0Yc2kExncbN9DabUxiFfXFroDOiUosU4q6ztjvF+OUu70DYeEiDDeNtNoHfnZ5J36BX5DqU6iat53Wzk1D2ocDwfQgkg+W1cUqUV7MwBHcsTUDRmDQ+X3hIBGIAV+9NETWB+OeF0rRUPXpek2t28rfW9DMzZadSUjRc032tRJKSrhIn2u5bihiRNT1kxkt1NQSFtcsnR8PfMPOQo8/0D6wj8mzaF2saigzlxt23bbQXlaFpbkdJy4mJnOYMf48tl/jsQVQRM8Ga6fewXQBw1Wp3zZPnaSOmGKWTTVbL+P39jhJlqVooWs0Wuoum27Iv0rrSsmU0bzs8YeaEDIWDXfVCqWRyZx1X0gKbs0wgFADWAwHQykEb9eq0pz95zwsFNC4FUW1vYj7IkllD5vfO9vmFmXtQDefVt0EQdfUPokW7/OWw2YXY5Pnmky+1tzK3pexsYzF4qKLNtkA52xGLfcOyjoMRi51pCNAQyFf7NJWpsNo3TG6+5q6cJEc/m7Owl8MUR7ynfh+QOG54mfK5cQ9Sq694/WrhKLerMFe3YQkmkkxPvWFCyrqBpO7FxGlexGqNAl1DhnJOCGvt2hZYKo/8T/MOB4mVXmA7b9yehbbCX1VCtZ0cKJJlgD91Sv3Dxkpg1kjzI3hnic0RTcoDXNbQPFBRg2qdKG1HoFVtSuH8CetztZZhlV75pi9hCYgoUT2HHvW90Mw9AxDAwG4iRJJIHXis/LzBO2ldskl8fMUfrSoWi7k6J47TVV2eIMhKePf1Yl7KvDHX49SX6kzB3leGRa8CWHR3xRvdoaxah8lZyx4ALfVIAS6hbxHH5E3kvjBJh20+Ep5GzHjBZKaaUQswBCe2RFvJ6AMhRWW9vzAGXNlesEocCl8T/fsDV7s9YwG0QTmbdAhbrPt68TRWRsQi+ytDOjAlZWtix77pBdvRcbAGYmktQjpXWcoQ/UazU+Mjjo2K2+LrioPsNoO7x3LizQsV7bsCLbrl5YtU7qQBfvlf3G9K0OpIIEGEwqy4Ju9merkNxMuwOyv8J2sqXf4P3ezCsdHdzYHxRIp4ljCnyg1xW8l3xlbEzcl5gScJTuqIekqdrxkeQ9D0e3rHhiEuakHLS3bGmegi0wCweVGkbgZmrfa1umPxcPbLuLZ6EVV32mcfndPVz6CF5sn4O0DZC7/D79Zp5uAx7g1CDJDVzrLGRKnNu8J8epNWIoITLi3xccF1sCjK+5iwkRMksvt6n9QHFlgNePD7b+C1l4JbHVwlhfWc1IwKaUBPX731yAzR3chaxRjvROGFlc4b2WlHwNqNzihY51oM07fdMfrTJ9cCfHlBBc6uDRTquiw2Q3LmrUKbgP9SXnw8hLg+huL4Imq1GPZcyylyjoUPNDp1ybOMeGj+KFxOemRvsAziMThMzKtGLkTyOKbGmUDPvHg2KxbhxIeStM4krqiiy49X3WNGTaIXlMgLZYVSyelGkU/SzL1zUISF6Pw9SEBORg79BMSldE9tuYU7XwTTW9suuHuCQH8IVTT9oXyPgn5XR5Yd7b5TGLLwWCCbZdewfe+R+nZ4YqUL82IAU2lr5kuBuEDJZhCcPqC/gj1LgTKc0lOahJ3HAMkf1rGpuM/9BmLWQguhTpZjx+7WIPSOrteWb5piVdC62M6JnaUlY2+LRF/HelKZCPbTr3gq9Dg/XTJoTENr4PVjNt5Y3494I3weo17km4oAGj9VUOihLTsl3hsS24oPbj1hPmOfVRJ2YFxqRAkoIIIuSDeNSCYXdb1jxxDwIPII3MHamudK4DE4y2P0C3pYLqcOKjCtqvV5EiB5Pl1Wj0gYTEACpCCk7jsQJWzFaDI4yTW/cGvT9Bl4n3RwcOqRduHHwvXG0a/nlFuxdNBgjV6Ka5R3heUj/uU0WQdJbB/bR5q8V4WUHBPxfrn0qFHOT5CXl85CmSJ1Ci+CyeujcRIlhUBnmgXW/QchhL7JBJEZ/FUoYRv9I6R00hVmFYMLr/J3U6oE8pdG95OWmvE/f4kgZadpEBy/4M1WZORuN9Gl7viVWxw29dTazi1DJFVKdKYZG50Xjly8PR+sK6ZBgnziCyqEP0aGcJqVMmUrSwIegjPCyW53DWUYI/t/WGlQRcheqMOZ2Ku10Qs8vIL6q/dMhujCb5wivw5IMMNRKlIV3Cd0bsC9WsC3nRhuwjWAXk/dHPBQAHb94kWBayWAGOjHNfEd1tHuLjExCIMN2UF6Kx+/pyrmZXbwYWEoE5myJI9eWXc4mWOZK/siCrRXQN2vGqQ2iri32MR9dMF/LTI6hiVCyWa+D3u3geqmqXYsuU9QqVgZ8zLsQatKKjDTCW5J5Sk8D+Ph9c0mMtOI+7m8EXkvxMdBCE8G+45CUIBcwgFxChU/pVVzl795JCqNxRgbUtyowifNlH62ub5uIrnFUE/Dl2DwdKL8ha6PT16z6ySkTq9BuUVaxr+Fv1UyZnTmodXVxdtXNYddrZM/Kd85RHjZlG6O4BsfpkEEl357v7E1r+LtzN1DNvIU41gaho5Dk2xn0V/Xh5tDWfe8i3uyaNrtu6fx3Dkj6O222ZFgWGK3ydApZ/WyqltpsYhw/tE21rUYFvhhjb2EQ29g3cglVa53IBoywAI2wJ8eKVXfe2MgRZRZoZT7AkY9gClyvuzAS4h9x5nc/dI1K1nm0DcITYGQDtSCtPwvrYWSWXLBIomz+5wCXhKSytQvVxkST2eG8u6FRtmQwhl94h8cTguyZkqQHLWPH9SkQKf33xdawl1U0mI7pOPgiV3m6bmJU4GtMWnAmECbwl82FCjdIYIB+3whrtf0xssKf2ZLBe7OkZJkVB7FG/K1vfImtgPcgg6d2zReY7XTlz/CBhQj8/QBzBRxXwDxFbraPNOxUU6de1jFULv4Gp0zmGjCHaRNOTWNXzKfh6W/Pmnntll8mKpBPcgmKWHtA+pt1ggk2cGGAugujyO59zRcwBOQ0xefHVe9PGq+Rev21wDYFhJsuYY5U4Tx5loVeXKLT6rx6Jq7jWzwKXAjLM5hrrSpeJ+9teVMK1Q80f4LXEH4umMyzJsejx536rmA4Plh4I9PbrXrOs4reg3syk3i9KD+wMbMJiCrd4Tp41n5h+iP013vL2vUs10UOTOtNmcW0IbrxmTl2B+ol2EAvebSEjCgwab/6dwFadL+8ivVmO5oE5dBZA2EWAwdmZyrwJA4X7AokaPlChdnkoMdVM56EGEGNikUPnDiQEXMg6UxBtsGNC83q3FDPbPovUwi98VZSGEhjgpSz9o2Bv8M+BF9KNOTz6O7Oj82ZHMNZTTljO6xZv6p20L40XM2+KkGRWEuTal4EE9aRaZe8ZNLuHmLfKUpDP9HjoTQJV9dqSi3hgNH0nYv4kArsC6qCaqitFC0kz2b9LGCEKUbst1LvhR6VHcFqn7AEPGpkxs2NwpBPMhvDhXBes0kL9seMAVQIzPtTpC5DPTWZI7jgjnWYQswrsxedjbgDSi1JBZ4C2XhMfGOWpS3fMRE/Wj3eKFGHFESmVKvyeg1cN4Gow5RmB7MrhXW/yExn2zvTritoU9B+AyXJkJBrqAY9mrgfGVDFhYJQBQkP5BQPWxnUTTrmVWnIyrGZd0Z3iXGiNYBP6arCWnC1clWpwGLRhNdXcXSaZeBscaGBpsnJkaNc/fox2XNDbykr8EfnYtJQDqDeFjBoZmSYauvEknEq85c7t9mfAQ1+YG3HZzG87ueAKTUIHECy0E70f3EQHBKvts+kRLyKX6saeLAZ4VslF3qynSvlOBWPdTNiA26+EEoirQMgTvB60Red0MUrscdd2v1cNzRxfmCyNZD4M4nf2GboQKX8xrmVuH28s01FQqJpa2n9wFU/9FWk0rCVSYLyKXvzvp6h4rELscaJ+6qClcYnzbntpfhX6Cf5QRwErJC2g7CX5/XHgtWUwr8Cu7fm9OWVJPBesfH4aAYdJsqYVcDY5wxNjfakKXomqx1eJXsraff5WYlnmQAkNf6xlYNEKTa47aQuhWMOjhgbqF2tjpFBAQHqrRoOZkt0vF6djQ0dNm5Hm4ZXLqrSFPhSOES0jADpiMO6/xanx90lhq1tI5ym0ADJXGCGtp3jvh5ib9JzCkV1sIJ18d5U168B3Gf+kBlMxAnBavJ57Dh7dElonP/GRod0JVCMLaL6r32uXSeX219ntQQC8xu++WNUTZOIWs3nMK8MSHbGCYuaTAJAUNHuxDVANC64PNdBRqE2hUR4nmzbscQLLOhSF60XMGAJXSANKzHE/xcF2WVOD5+8VAXqKmhd3tx9RzF7j1+Qch+rwuQMQ4sv/HO11zgSJWD09tm2Ky6m7M2tLep0xei+iapjDDILvhshRa9ln7TMfBJcpCKKL6vUYPrmujdReqRWdRQEGSVAdmsVsMAK9v2mBOx0T9qi3aQ2HHOVsoP1/wVUNoPgH4/4SauGKm5gH0Snam1qWkr9OyOrj63eN3rWLuxX/LhdCZAKryy+KyyzHFHnGfq22pzb+4pVp8JtirxLMoW1iI3TKkuyltKOBL7fShqrGBtpt+Ww31/jybIea676d1dfINWYgFN2+2ZvTTEutP8mz1GQM6YvZ/vAlKvBtaQoBAfxQVdDwt/89DcOnOeKeZ68JWTQcTYmADBkKUJqlWQQaSWg62shhz+hNoCJQlgbBhCBS4VjrgMuYCP1sHeCzXigKXZln9JDWExhq7bnfotgshiHE3QO7l65LCxpbJg79EqI5icgJttHYJOaSs4QUm7p7zCvn6ysEdCwE5EI2AGvQt9sLgr5YjE9QwGfK8hh51P22iNb656kotJdvGORRmQffsNtVKuEA+IDKDwO8Gqp2spnsVP+capdE9rUlgWVXBmp4nR+upMzUklZ1nn0YmcwoCxfCWHkdn8hCHYEsd9XgKkGah1YwEyqXDiIM0mCLVvE4IICjA6UHXvqtUg26fT+ETzWJkvP1EzZad5QOMgxykjzSSPkrPJyXzqiR2516u+k3gGkgEg/tIObkrVxSEHDLlY640wxeLO9hNHqxnObYa0qMZuQUFDM4CJNqZGwbraK21oSd0vV9QcrqVrUK/xv0KcFOq5mZHPi4kkrbOvq9RR2rkkAVM43zxx9TQ9D+ZNKaO/8SWPXmx0CGdgltnQkThSzhDY4mw/CiwRBeqQIJlsz5rz/IWqlC/WpR3WhtcYrtHgEdZxTGxUrOzdh9vEggU3mUQOzDJx7jh1uRD7ikv6+NvPl1o4RBMnZPrC//WWevNCdfEWp890Op+56cxUQfyhlglMhsQ/RZfB/SotHu8A2kzoqQ1dF3vaaER7cVunCa9JgTXBFsDX+dgsmtP/RzJfuIzyZ4xye5tCi+Nz/0548aSI1qG3daVoEPzq8ZWCHafefX1712XXVQobPAzzETNDwz9GKiRDZ1P8ForX1jg8+LuPJ9j0BSuXzkYDnhJajbN1vdDl/ku/fsVbAKJb9s/0J7VeODAjibUucj7AWTwibEHjXGRnWZk3EmcQgzTTmG5dGPubbxNFuiNK+iqhWWtGwMDnsnHvOikjg3Ard+jZALDKGiFshfxOqu2uR/1nz+SeDW5R8NMG3TDVXLHme68c0jS3O5JryvtNTlbNNvoL+dtLaa8N83LD85CSeZItdz785HDJLLuJwj2r/jLWJUF33jIRTLzpwt3dGpwFszoBjhMsutuPFXG/m6ElP8tGpj0ytOVWqR+3UUtKDURyPWp5fVQswetNtuoJxQAStBdALhlJ5e7eI9KHS14qNuDQ5MIhQsFO4aEC0Bs3cJBD+pjoLWTTXIpkSJ1+Dljze/lwNtCYe/3P9DFhYdZ+UM7qCyflapQLjI5YYgJ+MMO/MF5PIuaBLzWQq3PMq+h66YZ4YmImedxtxNCNsgzukhlThbemG4451XuaWsTYrGjckfhC0SQbxYBHE0Qp3RJA8F5gaiGR7CuelKAg75xf0pEfnsK6kWXOrkQ2kQPrvI7bqKio9tYUTNnXXeSnPOZV5CB6iR7osHQsXJM8dukoCfkCpO2JmKRLKwGXPcOGIiXnNRNdx7tQyQTzExeesPh0TbpsA1qaiRCKENgrM4kIsxBXnlvCyE/vKri8UReZ9U944glKGqTUZnKo6Y+TqkPsFZ/FvGLBh/9RRjHJOw6liZJot6a+3vdZO2rLjZ8yVwOQtrZgUAM9SvrR+3Q4j8UlLmyRf97EyO197Vi1Z+K8NvjBYtOiAD/pAYfdsYPMJkIKeK1ZutpVIbCNz7gwqzNCwwpgDRDM/0+hVCX78odyh7tn/RvNmBjQ4KBogSvRiwsRmqOb/SHoxrKNRSGOuQg/oIF+QkOloifgQtgTupEf/MqWjNItUkMrmPt3eBmZMxhAtbCjW5zKnRGyUs3oMOmiYA//ClYMpOsWRGUYsX65sz2iIxDLoYZdpf5h8fUWrrUiyMzoAhWIIVYYBNddaGWpjFiwECn9F6LXKcKhEIaVTD+jpuxzrNP34mma1+lpAEk7voP+sb+xNkjkoih7fS4xLTvRcpM1cM4Gh5rV0w/UErjzufbYKeKDcGYp2jAkQaQSJuVAIQM4bvFAszx9VJhD/QBckfwMgzIKZHTw/7igVE6nTdHVprzj/9Bz/QUy2EqJAcKiq5yuRCIJZSVrGTBtNameHTfDFEdKLJGHp8M7b7I+0nj5FvaVeQvF2neW8NIYd/i97+WlBPw4Wr+Gku5pSwKvcngAlmSVQsY0ugAx6gPLAbnikb6u+QrrgKkNJ/Vx5iBs0fx9r5mK+egD2yy6GgpcSvkLpIGmO1aTIJNhqKCk5HY7Xs1fZrNh03qnOH/GOW0GMWn5g3lLpMJI3abbldUofcwh0kZK3fOm1qFQkzFvsfU2oRQj5beyaCrsxjnrBfduXgU2te+sRx7bC0ujp3sc4n9fbrcaxTFr3O4gXEJOCAaLr4pVZ2idPUfFhRCb64Yi1kvP1voDGVdlhixiVZdx2mTAzSGpZ4mVrAaUQZplKTof5Zsk3REkcGGJre9DCKaA94mhcrRNw+t8m3Lj5z3vCnVuCcLVD+Hh79qAgp1cPuJihNH3RdWKpOOMd8WhC8fSmXjP4I0cpy3zAkpzvXPWFrBiubmdIWXC4Y0otC7PuYgTuhuVhDC8HhAhUUqrVZCQVsoQ4pWsLrABGrB6iZS4+k17loCKHgOtxcZDXQqZ9nAzWXEsZlpD30RpXj9htC4WDYM/qF9X/x7+K4pjWz3uo0bHASYkODbZsPPh4mTtea9KGSoWwZTclS6tgAr09WY8wDaMmzChpsdV3a6jeIDFpR0eJZ2aL4zYWUtlhbxKRzQ4QqhRecetb2jqGzKsiKyn2GfSjDZA2P9Y7SvlR38u7ZAG2CSoEsUbzpES7rCK7U+ihqiDD8HPHEMbYQKpzfmfTjG0b/eKGgeHkbOjILChDq8cb006H8qLPCFGqPqVWUZcW8LvYsbaj6KzvZW1fRTOWmPuVeA3MqjCzIilYuZJRV3RVRh+NCrGS9G9y6I5kkCYh2DsqSQokTshhwtPg2uBDNmjrSyIgpPOzUphrAR+MiG1EcfiIfacPqHOv83aOlrKWyWuZx1HOSe7qXyEy305mKB2HV0TPb5KBXqOv5P8YVcC+aJWS2jYP0EUzOb0i4Gp9tzhGHtmD6nNPQUlzqxvPPZBspfHGEl0ESyDIuqs2ChKpGmvq3XlhejBWwIfZlOMOn9+m7WIN/y5hJCsumhOREybORDvLz0Lh6n7I/2GdnJllMSOo45nWDPyPlTJturX/JOqmVm3QRqnw5QToM5cegBtWdxfcx3wfKW9l9KvodrX4IO2EkkmvvXYK6qrVviymdvADghqGAXOdaqFxv+0lSWGlDsKWgWYUBbLHuoL9AE9tPuzQsUL9D/Z727s30/geMI6VH3Eyw5TSjUAjwdwi5SJLRVsBcCv3U5LikIRFX6kKIdPTgZ9kOmmbJp2lVG5NO6G0WsBrl7sD0EIGhTeMMOCTWS8wmgj5QvElksgoErD1jfu3C3QuNzQrAqSk6vrYRjeLdvySEC08jG7NzQ+uljvDjWy/c8zA5rVLQ13iD/RZ6y3BV5epUXvtCSNzL2bCVzh6gtOog2pfauOLSyAwt7XJ0Ia9iCsLUnzZqnYd8dN3BeINH+MT0JSrQLj4zgWryaWs+CmmNe+D435mZ12Vo8LQrBLwdluGi1PCZznQ35ngkesw1nALwnxYv0E3cHnZXVZK6V/LpCahEdiyaukqar07P6/+nKb9BsWD1TRpBQ57q5tJuCzkpKoq9VZZxf9y+cKEIbWKtf6lvxwbV2EuBVdLrnLmLVSObo6Rg5krygclkl8hPntaq9qmqVYhNYl0PDqblWTm/zmE1xMCFW6EgnhzwFKPS3E6VSd7zeBEsD5l2ufVM0ge8HAVXeEEgSkrYFsm6/AQj2GXenLbAWArCzMhgwbhgIRJw/E/QfHe142g/ELcu/re/Z5FfrmmEmXdbZ+AaJwffx9YRB7m+UVhBmhJ/JFfL4ANbexrGH484WhwIM65ZNcO8jPKLUGYfgVqCb6p8m5vKWtE8kWYkL1LD0u7i7yTJQSacmUdyfYPd3WHZgoP+ose+Oe0MF3BbXzqjOAgE/EibaWPYB96il/g1CuateL0j9InzGV2BUSlStNAAgN8S+DsUP0k7ra6/AF8yadRU8AS9fOOj3iizb7rFLhzHFQHZKLjpCZBScBDWvqlDCLZHlh+6SMeOPHO5tyNFgVnSIeKru7BXLR8QvIRcq7KKw1M9ZITZxJbeTg6+73M5eysXOydeFUMcG1p9vmyQQoC+lOCYdlFtsajdqNVvM6MlKyIkxa+7jb4CMLb6Cviq89WVs1EU0foY/qDmZjRFHi3BBZGihD5nLEM2svCcibjkOYWz+3evgeMmgV5p9CoIo/PYfJ9nrXBkvGhzwEjdWuokVDLjWy+uvTjhlb+3N4GVFwdASZuFFO/HPrcmu2KtjGCk7NInHz8Ce53YwWlUK8wDq/LPT6JK6YbJoQDeyFX4C66c6OexC8ObyDHPtsAbBjAUDMj5ASeXrnoSczA5ruH1MLiGnC+NI65qDkNF43wLmtCE60uEONDeokj2JtB2pbzoHR4+I/CoFx9MpUoV2jD1tAG225yfWEBSnxqz3AxZk3/bOw7idCiExSSiPRpmRiyN0OFKhebstN7E8sycFQU8/5CTwzD17mjl75/+TbQLfEeaCJsWsh5jQewavei1IpGzcWcSkY9AtCr06l/4Tx+XiHwVe42Hz640EAWzUrqy0F1C/KAOh2EUODZg5pcGhg2rhK/vTT+amF3DbpZXNg6+qNuI43KWlzuUeH4LeM25BEIyuqeJpPFMbpb2P/fh3r06JDrCNbovaLen6pksvD2bi0Yhdlpi1I9Q0T5ygEWz7CY94AgPB2auTUTLyMsWfN8A2PVpL2btoiCQc+2kSS9bhviGd6VBi1T1iDZcvS9z0eGMCoJt/hwFZtZvw7AfUQhcHtvZAMeh+mbL3e17BivdoCtYXj2Qdfgzcmqt/Zdf2EutxxXbYl1481vza0YI+jy+hYX7eSPQTDruVwFfIjGXOUjCfsBgQpAsIL9F9esgIyEM1wpbr/5fUC1/YS5rdXzsi3g8ISGzCTz1s8Scg6PTUhCKu2/3imyqfgMdl/e9n7fMff6HGERkWoN5JjDHiXZmX2up9nw0WAyawlm0dPChTRnBMw8gEjgdctlLgZZnqPSQHkHfJGtltgvkx+pXOqxicMMRPTO0z5aOHqUCRIa44qMHaRoMTxnNx2CquwoJ/A6xz0T+HeevifNK7HIHIehdYeofGR4B1c7i6H3mSC21RVsaaZ2QIiN5fQSPi2HLT1vujM0TmzBPBSIVN0z9Sw6fBdTabnKi7Dcoi401gI0oH+1/bH1BDYPvkULEaYnqlEG4N9ws4rfvQquKIAJQ+N8+a2OK+HDWmYwMp715y9d2Jm0k/1ZceIaa+SySFY/OZTZUdt6wMT/vtfT6iwX10+gqrMTUBHT4JbSEJSuv+608ICfWVjhW8dPtnNDtd9vwFkkplKlnYWaRUS15NyvK5oVrHcFAejsqlWtOntuDvcGoMo0HDzvhv5OV2AwnRlUFuEuDc1Rz/+twOkvwEApaJRLtu5ieMu07kk6oj/fs69WS6n/6IxlewgjDQdDaXCeB66isNa3cTSx/9uAjlt1P/Ebacg1sMYtSWNYTmVRmN0Sa5VsXJRnRAgIon7XtMlaiRn1mv3dwdJMOYh6EadMWxbTuIbZ4ydcApDVobfZY3wF3XSByCw6Ux9FiQQmfm1irUu748XhdziOlR0COh3XBgEPB6MwKn59O6Z+OUJErR1kzDnsjpw4ipSvBz9qHIobXxeJqPl0DrgGBYKliMrtR2g6hQS2HQ3Ej8ZpEryQ6aA3C9z2rk/OPxmlrTNpkHdCczvSJMQ3dZuYitkfjvoU+TnQbfoe510AcktEZMtFz50sBZtG3ox32bgfvuYEhJ9Y2D5bbY3wtSYjbaeCfeguZudltCfXrkhY+tuAHuFs0jOsghnNHITQO/RIpXRIaEEZtmMT889UBmMo86XyMfnoKcR7puTo6KSW7hKrxWt+k50UhIUZbdjk0ViEfxjjQnLy3iQZ5AeaHXdkq8oGjujjoSTUGjX3nS7o9tg0opI/Fs5RQkzG9Xq7H4WmUYHx4WoYTqyy7YfQgpvAaGLP1i6uDgBRfBrEX/JANwgQGjWC6m4r8KU8XHLD/UB2x7qvVlMwG9698FpF5RkBux2P0paoHb+sZtKXKuGjmERaEF69YqxG8Fvwjx0Z3P7UlmHmq6hG6Tst3R1c/nysQCSXWJt0xM3wvqRevTj6yjqgoSoEacpUBbtt0BE8ODW39jIiag814FPrCtDwnZzjfj5sN7eHu65Wvd8cXSzUpwSiV2/g2x5t/UzWxA4xcLPemh2pFSZgyMLyatsSvKeHPxKPucya8bTbzwCJ9wZ5H6IlG+ByMvh4Wg6oGVWFoTuX4gwumyrugPOh4AfkJnP2Ed+HK2mLL65dGuW3zq6vsNN/BuFcCVR4pNK1bkeRRO3bKw91SId5NHkSlNu+enXT5zm2k4oFfV2NEK2NDGGRpS7dzi9Mt1hDbf8BQVQAJoGfaS/H4o8FNTh2uMoA0EjyNp0yWlgu/UYMacYyugjCQv1Jq21+Ok1icA401g//gHAm9q8xIfy/ZXbTTmR+bmBz2Y3kU01VJM9EO0OHvr1LllMyobYZEXiOy+JLmEOwHm5Svq9JyV3SsghSaEG7C8uw0t732ifhLMdu67VTMqCHBwHyQmxJUH9KO92xfjIaVXh921/jZXENImvf7PvJtlVMIZMzcUFboe5iWpwr8AQqAtD9k0wbdG/b9EI12aW3ooykvRGIqfnz3r924Cdy9mAFQJufYJjY4fRTCHroND26Xr2q3zN6JT3dT0cpKK1zymlksPH0BQPwZaG90UmmfEQ8Ljky8+tYUaxrTW3uxGBoI9q5204GIJyIr+oZRvRm+6/wK7xlL2DF2GHzcIzrLzs/48THZbdR16QLOcL79uTLlP1k+GrG0ZlO152fOMivIL6FiH0Jv4PIn3ZsP771jevSHoZ1oCqlOduKE/rz83wFEg2l0X2QvEy9oBk0H1Q+Hw043YkXnYI3a001LS8GlUfTpJryq0xgaCB9VxBie+ggf4+gXqeDBVdwwFABVST3ZqvYeTaJzD7UOKMp2R77GAw9Xx8jgIqUvjMImsYx6XtqF1BwRHxXOaggnmEhn6s2sMgL7zZ4T3eM0V7vACAhXdDxAk8SqtsSz4ryoLBU4+wJfN2MmCfcodR4n0PYKI/mzm/r49Dr6vZmkAqq4ZQwV4Pe/pQhQYbPghxuvD8rlpJPTGZiEud+43ObVzC+0V14jp8UBVefR7UlFknQNUqnir3ODFrrGJT84AhHp+sLI27entgE4RIB+ggRCSR3X8MQBNDgpHe8Ix/PVKj9BZYIIBIB4YCyRj9hINpgnZ47qusz0amjZhEm75KyIwlas1KXsHuStK5BJE6USfoUiFORfKeFpJgJs/EOshXqvxjo1DdIHdYOkrImMVc1ioxOCWvTL/e59J/a0AP3vNtfls2AU0+RvQLrCH1heoTSMku+p2Ou+PzM0LwiTx4rXUCQwEH+dWxZ84bPh8xB3ate4hLS4nnE+L8Vt9baQelu9jxUzOgNgS55SrfJE8Lmskzy4zswDdwEnk4f1W9o76h5eIx0iIAxOkoIbLgCwA7xHl3bDdG+UwYxAptKg6LdcXudhgRm1gwyafYkijAmqtuG8GsCBzZ3AqFBtcOY17JpylWRp4X03NA83uoMZFxZmVluSmsGHV4AyI9ZrE37vcazCDLurJ96JPD7m/mElGw8bf3u45QesyJxd0/jfKdrcYc5UjpO8UJXW03bOLIMYAdfKOGNo3lOvRNf4EMVqDa+ZuuHfo/O09ARyp3BXoOUIJZj47TCycYwXp8zs7we9m/rcCZyMV+JdIYG38wICWgbrIvL5eL6uaKLUlEdqhBF2eGbcUDiSMllQQ8lLFNrNXEB1+5EgslQdKvFnoA8a4C7zf3jmebxif6CH7Q40FsiXMJQXRSANZcq5Qvy9ANc0KEFkSHgnA/jzjsYXZ//CfnCWV/7FAxJ9ymSXueXeWnsxb4vdIlvzAWd88w8E/NnSHSEGZxOJf83BzMvVc9elMQu35W9aYcD3z1pgPzFSXc3h0U1VaA/XKjaBr/OIipBJJk20/z+sfqVTlAG8T07CpHXsi1k9wozhi1bilz2u5EXkmTgVsV2IrLCfx+rWj4EIRKaKFWiTPEWKpNCyCcrjfweeWVS0J4wAcSQ8+S8b2D5FSaiVZUqbX9R2jSeM/1M4j1Nask4fmeugDevV31LG3bBg19XdCPdj3t2/BSyNcBwqpIWvf0K0xkBGn4Cp2gq/Ap7bIPznrwZc95Ww2Rriml5bjGg21nOSUPaa4dgd3j65ei4d1xOdfqN/P9Csgnbo6b40jPbTWpkPSg00PzfuSPKCAgAR8miUFla0XJbZVLmLPzOk3AGWnzhLRGY7YV5WWVd9cFRphcb0mz4TCcm+OHB1MWDmgmxVWR1r6NJwmHjlbsjdHMBjTYaEkR9htDYMzahHrHTowjdU8Iw7ClVaee9q1zFwnezxj5dqNWrsjiiF7B2SVPtvRh8CVRHL0IPeD94q5bvSHXCT6n84ojU4pMU1fEEoKJTBn/y4F0i/DCHZMktoMwluPAAhvT7oLQ1ZkXQxHrTvXCc5ucujMdRVlYv6AJMObs0/Ptf7z+BlxrUijiNTT8ow62dQH+c/AjNGGD2qWNlHJMpQyiVn2xF4Q1RdaNPVaFob3vzLjDpfGKdxnZwivwN4tFUdTeAiqPdqxxJX4E7sCE6G+ZkyGivZ9itUQkUgDCE9LW/TpLlkdVd0SEUQp2IcTM8fBXZH6cXpAKDD2EE1302WNtCwio2Pw7sZojNSB+u2uY0ZS3JkIZ4EH8u0DLVboc4hSanKseBBQcn3nuyE16RUsCAguGAmcBBob19vZtOv+2297gD5y6SUzouPGQ56FTBVMIxRaHajOrr0OIl7Ja/z6eYGZIgAvPIrA3l/uEbK+dbhPCeHhcFfzfKAVL2X6NcEP1LjAUuKFQ5gkUK8tidxuGgmWxNmJzTbfk/AqC7zZjzTNjb0X7CFK7sYAzrJF5syzFB32YfYn/qhEthenf15RH3RVvyhVjI3bfsaaM7OQI6J03l0i/ajAiPVrW/+OEcoLfBTTgkhWjiPhFMWCyGOxSl46ET8zk30Cv3M/4D0jS/LTBSPWimx7xTWxJkLrmQpbD664vYVDkIGPsuzSoonkoiPyq/9JqREfl93C3SzG2ZxR6W0q/dvEZAKy34Zbxlon4Va+FH6049ivD5oCGReWRR+y7yW1rJ3gkW3rHdbs6ugQo6rydi7smVo0Qd1kaZuiyLh9c+iA6wJZCXsPpf9bSL8YbkV7Kl9kWqkfuAgGjVBx0kf3ENQIxPn4gV2GnHWldLTu+pPjkYN+gZod3+oQ9iHjMAygiYy49UYjLz5b7w9kFGSQxG/12djZsKnUMvroZfMdGCcjvd00wJQCPM4bx/JiJnDMg6YP9xFVIfkoBRMJM/rbhO+4jubTCZ1t2vV3h2RCF4KalBYgeqfmLYxmt1n1C0OHJDMtRo+4H/Zbl6U1muqbfxbwqpNA2zNLWQ/OCWbUHhxjp3MPc9ifr8bXiXfsAYzz9erRrxeDx2KSCXgbn2AAV54ey5qqOtc1MEbUt45aEkI2l/p+7aZfgzvBAtm8NVS7aAIms0TkyGAzo71qM8EmRIossJz0guAfoPj2eqpSUOEciUzs4g0R/Usdd8cs/472yQKnW7zQrOlW/fW+ObxAdtuZvrNq5dw0Zht3niz7D5hHZP437qerCGz5gSTOU0Yzpw6Cgyk0g9xut4FVhcfLAhk3LanzzcKjvM7epVizDwxgOKhIrZJkoUvNWZxMS5m4OyQSYnpzzmA6+EWo2CNP2T70Wfpq9k/Go9K7khh+DwgaTim/cp4RMAWb+X/o5nJ+zRM4fewbFFlnV86h/SUT/z3dbX6GWsLpm4EpVngGkQspeQFh2NTphlbcVfUEN6xzrGSqSsUaic19tZhmPpZ5iLdz5Gv/UGR36PsyscrpJL6nWq9q/QLWNmd67Eot6xegRJ3tylBMAti9dwrofWN8RRWixP/jcyxS22ueFLk3oKhccsBOlNwcumBYDrLisSDkeZDQGhBvQbnQ1zvafje37P9tsmn87D85XRSFgvgTjWw+0AxacSxddmx6+iRz39QM6hWsqH+kRSEnLWxFwJ5Dk7BmB+yC1+XpT0l9nQh8Xm5nUys8nAnZRjCx22w1U46i/nmAtt0YvgakkNJSoLu/RPVl1y7XooiujL1nsf1FIpG2N2gfZlTHsXEMfUqJwlRQq4Ouoa7NjYadNpu32pAJgbE9j8yZKMFw3RW0iU26gom48Vx9TT9pQbKokSQYzGxzNwmYtO8tt9X+ke7/DW7XfNAWavqfTiZbOb5+8V71qoKm59t+fZr+v/YF8vj19UhaEofHwH3qzNXPzcaD6tbQSEgiY2HiIdqDjEIFWFRK3m7H+WtwjcppJOUwLIP8t5scUCICqeJCRHRGAk0IXxeQtmwotozA7Q0ilRzjRMkai4Rs9DBZ2mNjQnOfMaEhCBwaVIDV0N1+rikkUDT4AW9WVvxu2+7XlMSlvzuZI12Nu3guLZaFMXVIPOD1S0jcNcByM+/Jy6+BYXhryXtQ/0EDvrN5bGm8e0sMFykpS/cL+9i2sne7MFXSDO0/tLIxWeAaFEeLX4x8CGkVYKISlBznHx+Nn1EPxNBOAo6np9awTC0a5wWohNbfzbg9tQBZ91NVeHD3UkYxkH8SB55HQ5Z+B3Aul2obUm4V9+XR8fCTqGXJyzolyuxvJ+BopD6NHRN44CNB7XZAmQEYM+WsthtfSh8ImMDmn8BD3zSbDxWTw12DLo/OzRG35vpOtxA6U7uLOgHTrANVxBw3cxj+8ngHq4uUZ87wScENGYks9xx3cbBjuSrsg4Cf4X2385KUWhg6DSLetzY2ylbm4tar+mN9QRnVRq4zATnMtQ6jE714y/o5RmdI36ad2B4A9J7TTaMOGVAidJ2a0kEak18fUIJp3KQrwwV3AGnrgppl6LHvdtwyKhaXQCgR99qMgyUWmmukvJhxkK+F/GzuNC4toPPtWoitefsNSuO0wvRpdNxJK9mCxoNYGMqpGxQL6pBt2QQkQT5imPztbnf2uAhxX3npGE/BNB4pMfbf0m8jAG9cxAbXG5os126FZgQ7czV8jhn72fyL/BjgUhVf0zjAD8kKP20qiD8Ta4WUPY0EY5+PH/X/JS2Z27kQM6qZ4RNFS+dknYMIsbiWk4uI79tld7CEGrh6NzVL/DKUMyn1M90lTjMMONKzAqX97jjMgC0UGRwAmRmiDbAKC5RFnulAS2cWzAB1ZvnYs6yvugMxJ60CoHCPzKPT4IBiu1pPFZQ18UreIhoD9lJ0vNiJqRO9Wirq/2fgWdusy85H0BliRgiTDf526jL+QHoNvU8kFN+Ko3AWryv9Djek/8Jz60+HvtL3e40+aebJ4J+HR1G3BD+TAux7csz/5MfBv1gCW25X2Dh9Vx8b/Ey6u+QX0VXwrdAadj7S4mSgAVeHeLO/vNynWA3TadZKoqY+UwkdtNZBZKn9EM2LYtx1ZtZ4E4ucfvXrVErY5gP3IQvEqkRsvI6BJgMhgk70hecABvg4N/POybnSYxLXuaMMMiSgyIx9GGA68I7BMBH4PP6TGuvUFGMJtkssSYEDvaOMX7Id+wjdZor9Koxjpt9aj5Fkzj2KQLr7R6gN9eGjbYvBKQ72fROxJ7VM9lxNLAQCOKfI6xWZDJn8OWWI6ItZCpqD3aND8qMrg2Ei/RQas1CoXTKuxydNo0nuhyq4vUDJR24WpmYxi5YTDpnrX/lZbE9Diq1hCA/SDVtmjBJ116DiJ+kyQavfyYmZBcspWDjpMBwymTR9kBFSK1OqIeG0Jp2J4VLCxKlhM2VQCqe7xt2CWPLYOaMFhoBxbpp83BLsnG3OLkQPE8xXM2NTGCdyjTQvpoLnV0jBaEf2FET9Rwv7zAVgVrwRMmEUndXUpyiRo04Sr22sk7dev2D4jXNXCoZ01o8kPKr4etagq3UjCX9LuenqMEvNETWrXzHnMqLyAdG85xPC3rvJ6kKk/tquTq3DtKKP8HoEx/V3Bkz4BqO1WFXcMICVs3pIUVYp/5MYj4Ickhm3VmuRz6IhVRGqg6OmsI+CmZ0zDsbCnPv/FcbE4xvZrS+SRNzVXknLiAh0QFHDYr9aOk9rYM8X2a/g/lJOGkdQRoxzWTA0oMS56yw4uxNe7LfYbuCbM7leLp1mtHLn1Tb4U6lF754UdN3L4V1hOiHhV7bGeakWx4CTjWGy7Kp1u2KiSTsCpRqRU1pl+cFdenas7BLlYs8Ung1xhbID8uiQd+4g1BHmkacSwGTfggjZ5r54QiRNaO6OqGLHOFjj9GNRmvm5l4WNEkMmzfgNeZTVfL4hNZB1wd2mQ5CT3YJ4zMioatlKTNmEW+cLtM/WMdzPinM0F3FxVNo/5QOpjXcQLGauVdjhCcsm/AbZGyKGTxJ/ICMIoB5Z5naE4h+kmh6UCidulg1GCp2GUYuIogWdRNSu6dvncfvUAWCH92SP6Yl4i97UEcCS7uzbXFlcFLpGokPrCpSR5xviEBXwOItFxU8GQ5ykYYfLwMFHwq6dh1Bgu2CaE/66pVkLbtrdFBCIN+m5d/GXdVBnPbppGSAIfgfG2yWst6QjwMUCG+BH3XrErKCaGTSbJLMWM3feRE8c+z/Gq/T3dpSYjU9UD5lSrjb+9gq38gwzGxPDHklWDOIkFQFTitGSjlC0muq+0rTyor8IRimib8qqN7M/nQaUWsAV+35aVyawkq7LNNxJXdN/srIElr5oQlLoOq/3m1XQYNtxr5UoP7S737rTBQYycUr8LthDiQjQx/X2GNT2f1p+6h2sqM9IpPKPDddluLcXSGsfoJAngBwgWluMvQ8EjcF2yM8PXknX1cyY/Vx8oV3ahun0DS61sDOyAbZ0sKfeqa1P6BuJNeDFsUTVVzmf/U+VmntINE7J4gKESY/StPxVmuAT4vDgCMkry5/m7aX4zsG2MvKDOIPLEW4MfYFg+erm+Uckox/Pmg9vQUwPU81QqZKNw6UC9trabEjJ3VnShZeWeg67kOyxh5NKeJG2CGj2n+hOLqQOHpR/7rNGYo5YZ2xFpxj8pvrelyNDL74RMgdAySc5EUfm68UZUUoPZDNwYAigtwJnQglFFx/Op4fYMHYvSjtuAnkn33f2zhZ6GBv6xu6/b2neS8ISdLD/Jv1KMXIQDvK+RB2ySVKAIZ9aGT8M0RCx9mi4Ct0ERvvYpQzeD+0BUd+6TqCLJdt4M5jmk7OlziEKl0jwm3i3uQS5Cn2WTk5uo6L7nDirwKLB8N2OqIezeUunGlCbmpQHk4e2QzM2A3MFxcTHePjC6l495lCzXS1//ju02t6hkxrqoHVr61toE4vgOwqgAjg9wjpVK6YgNh1NgUEtdWp3RHFqevaTwvwc3BffHPnZAveEY7zh/TtB6k38PkMHigLVsdVZl5b3dgJW9DjMfqbkORiuCmdVhT6O/6nZa51ZyzmdQ7p3OgKuj6zkzErKdyZKggGl8DY51A/KimFl3/Dp7lnBXQ3IqPTtdblAjQqn3nE5VOE6RcewezZcITt2SDtjYGc69VjVvy1QpJueVrdYCqZJWBXPVb4e0xSrCpJ+vfl4qxDf9ReSrXr0z6IqWUpI5Djqd20xLY1pOzEA94HuTV7Py5s9ZfBXPVIoAQtyS/Tnzzmq/ukNuuOLpoTr/PmunbkK1S4z9IQenoM696MBR+HPALYiMcn+f8MXekbnTpMoFMMcWM3VYQ8K06/iuVVCW1wmn72pXcEANqrRKq01nBVwP5jeZ9mPbr47Kfg2+UoYaygVdVndddr7iPLauMb6jA07xk+1VttZavjlTWGPl2K+HROwR7yDDq+r2ISOaCfNE7k/uQDqJr1+2R3spDuxSiRdwWFidFH3ymHMnLTOv8OvZIV9yh+bOadbERvrz+UJjuO7bwvwk5W7YkM/X8+GnydTi8B+3UfSTjao7uNiI28VCLa/q+ofRVvIOIks9XUmMHc7xajFGf3ua2KgBq2dgUSEWWvwrHDK+WfpuoigcWji+jCiaHOsnh9PeWrSlQURjJjHBhH544gbWbAgB9inrJ244u2ewXAWexPZN/1H/0jV9lHhroRX49+yofQd4M/BNbE62WqE2IRk3DUCOW2YoZcEfqQa9UFxy6U2W7z6ZP8tZMcg9+ZXzbMMdVx+qEW9POtdiZdrX5XKOt6aa7IV2CB+mHr5YGbOZ6/dCcL8wbDUjnDBjLtTNcpugDv6KeKsC97D2Cu/VG4hWHBPzrCFJyNeL1O19Z0R6ppQ+XGPL7ERvC2or52+6nF3mPAvWnqlCO50pmdFV4bYIYjT2NZih54m5+HBhiPEbpik3StlytIaP2KHhh6OKtkvJWnWOuC5LBwMTwSdK5uOD9zrHlY+XmT7tbaX+jhwkeyv7huRJzWz7pGRo9UbR2J72QIfJPyj+gGkx21Jdy1IqZQkKb5qz4VFnlYYTPlaQG5AndxWp0hMFh0SBRP6tqMHvHhs2cRti1cf8QYoK6TR+27UChSPww4nHhtZ6eQGCzz0UXwHd/xsp8sTsGZ1rKAuMKgV2bXuQqYF9VPVLMCzdrmgT1XxxpsBlCfVf/JDvTHrbpdzaMwzUDaFkkceyFwb8mWVPAsq142Dl2OL8nKv1JlebZYpjnx9WtUj7qNYF1H6DvQxyWylYGDlpL6Nh+sODDqvnCFabm6oa38OoMOHSeTK1mKiZMIjTNdWI1pgsOjqHG8fN03uCBRLvMTeCeF7Ud3dNW2GE1zoXPHSxqotOp7FbYryYB0lejBs8bQTzDsSB4vfw1SwjmW3nBltvCQorZh9wFLx+6ItNBxoGJStxuvekHvqkbPdwOZ7Tkk5e1mdtoh0bZirdXthmHRz2MQ07k8QRghHNE0UD2iVrh7OFYB4kGfkrNyjAKhO2Boe5JJFnLrn5oJdRahLADO+znwSLg0TWXbnc/KU/cP4XJOYjpw7ZzhlpCyFOxl6VWJAnNiPdiQotBKsWNQeFYonjLzivon6KAydef88zVnydmxnUz8N1oJi/rbRXjj1mwwWdHiX4aDXC+qHiptPcDD7OO+M1b+W6aPWVLsD4ZZwQZqlTg+R+s9c9P0z4QCrS8gaQ9nxKNLYcfmY9L66I23JcBL3kGYj6uxbPhE2N5XXNpbhiECr+e8OI8/4G1oUMAxuIsjMqEslsMuEHIxnNKKZ15DYoCYR2se0oy1dJVb1axPc4tw/52J6KViiMh6DRieVhyBZR73sZyNhBKJlDwdQSxRGQtEaC+iRWcwWLF8fSIjsxdqQ5gdKOnVIsg3GdLEHEtm1Xw4KbNqUhZ09ePrmeol2Z8mq7J7112h5C0KNzwjXvqkAwxuZwi2zypwpF555jcqETMiNLKtrmB8g291jEFIdoq2O81vsIQooXi5zXO6tGFM4A6OVUnllDUoHprnSfnMBichKO2c7tEA1Hi8JkqwG2H932EUGMfO4AszNWoDPzgXa3FXwCsN7tYg9y0iwlG/Jv0M/4tKsV6tEXHTjrJPCJEOGrBuNC1CDknbKl5r9Ssruajc+qS6P1t3k1PDBcdahD2h2N4W2i0ykpVDFgdSdBT9j3hqOVNyY2UATrCpvXzUUqcddrftTEGDLpceOcdce0zP+AwJCDqBWp6rHjshEPJsg0QRrG4O1jbnCmqwR4mY/kpjhNB08e/gd0fT/pY1b29qrliVbjGy0hupQqraAtpSD/+jA6Bgljizovy67c+cXQ/6MgnJPRYc9H6f1nPnmOYt1hwrJb+Zxzq0bYCjf6mM/mAE3RGLt5W4rjXQ6bpDmYn1kZFZbyktesLd8MXhtChvH0zi9tdc8nt+M2JoJdott7izARJwNcE0HubghQZfY5UabEv5/I0xhbiueFCMsq0MDkUarW770fkpf1b0EG4+donj3fltzPXIRm9Dcgbw9qk7sBrNLIkmBE5qvF7qRqGFHtlJT6NH/5gd8ShVNRaPDaTcThZFUkr4B5H6OxMmLAoMTKbHS1O278p2mnlTfznjQFyLaNfdVb5xXs6OGLcKw0HvOumvejNkPDYlEFiX8UjEzJmgqCaAy3pOA0RdC03lIUd6Q2/Q8sUEcGAlGzAJo+wmICOQ1upQ8aHbd5fkJlCwFao1qvhwTO3AAjuK7WSpopgVUtsZsg5wCW2GBMbPP1s/Vx5y2wokLzis7TMZ9qctTcbc+J7y2rEW7grlhYRCkjMY/5+2PQfHyb6gVkJ/4YHzhJDVhGYUCnk8iVIpPBtvHhq7i1VXY2kg09ce8cq5jtPXsPQ0JXlh/1NRWSzvpjVZx1n7v9ZGwrIv3eglV0VubijPL9o1l0S6ILuIgv60182COXHw59pnPULc6AykXm9Ga3HakkItfHkarqemAPtZPgJrq2COC62BSvXEXlv2SvaIfHgL3vIUIgbUZY2dV7Xrgy7u7onzxd0pobZo9fK/AHDJjUKgN94Ard9zUmq0WKH9oQ5gtXaHaVYxqAnlooyVZenMpFfpAuB0grnE1h+KH9XO4Pr1avbGwlVadSHGfZe7OQHq2eB++DqvZ7j036vbpZjZerxW2S5IpD5LnHikN90B3y9mQMHiSidfuW1R6SjLemTpvXTw5dNNIlOaq4mbkl9tji4PT/3dW9zOnHYArORWcVMZbxmOMomZRqgW+0rK0fiEyIxf5Xzv9D4lC2OsQLujhhFn2NbmHCJhyfn9zQdDvmUTq0maISDOTomAyex5+vd12KK3DLu5FKaDejAMuiTNIAWei/Q8Ueynwv3I47xIQhJ5GcPyIjA6zJ0hwNj0gpFW90mtcoVYyvB1bz5sdARi1MkTOeNAFqMpgKrXuFCXq78Aouraf3KTJxKug293/Y5ygh6u0x2NDMs/im4C4DZ1/mS2JSVqgjUSlCGohF1AoeYyFe+eXDtQNp4gi7ubcvMXZRMi5Pn+7ajFMsPBVRg0TSfapXvYnk3i9Nl53/d2+H4Q05uMjvqzcsNdzaQPPLiOgF2HDsm9dGTrmEDiMlNAOtyhDPbX23cKxsfeUoNAh4bK0q+oJYZoA+cC6jauFPu8fFTrVGoc40b8EjINH8KwcWkQGSMh/ZTjIjF1p8jvN83LGYbjSQ2wXt5w0HMuUGE8w4SFlqRpi8bUU1meojhV3/BfTduVNYlHjhSP2BBmmGsD5z0mz03G//9SIzwcfKeBiFsXTx+/8EcN3WBrJyuVHFF/tSADXKl+hmWQRy6cT1TynHDU3KkxxAQO/hc3xgjh1QNm5OybwDlUD8VHBzyc0P6E2i+GNsLLgoRBCxqrTBfD4IsnzGHcgkiQADPvNsWgb59wdbYdMBKLCvo152iHbYVS+aJ8kFT0zXey4eQv1plI08ZGpkvfSFSFbL6qA8mf7PFDP2X7TTaut6LwcvYjYunHdxEWJ8TpH1OkSiYr5+uXIo6Yq4wQLbD198TWTELKaKnLmVLkY269XwOmaWSPVh3JSje+dvlc4DofZY1DBbSkhSEd8NOPCVIWhn/Mp4+Se9nHjfSYtl1utm4WjZfzHA93kpV6/wSVopEq8syEG72rYs/fQXQ8gUlK6ULa1kSyMpkGqoF1tZya8ylTEbY9MMaL5YshIb0wO2LQgdQhKMBmG/P/NgV1g0ySssZqZLB4qeqVNolx6sWA82fUOdJ0GIiengKUVRHZrYfIyerfj7n8e+MxGHKaFgm+Z/gwx5Gkk1cnv7cnw5inaHCTy/ZNnvT9ci5O8mgsJsFvEGyPG0EFRpVBr9/qs7Kh92Tv73/xnOTt/+NB9TRO7rKyeHIPg7vKXXoi2SRIslz+K4nbF4ggn5uwGBLUnNFS/stFIteQ2SfhAbL6BOlpUCY/nMW1h+552AZKLTadmqLgSfGJsN8NdsCHKTO4TVm4ok7KrmwMEY+bU9lkK5l0ZaklZENqWVpuXXYUn20ORtY6wEc1SfAU1OOJIkArGonmTvwBCybq6mXwGbNLyawseAp5x1f0bjMcbNVDNm+EjuLtvml3FNC8JmkpU9PuyH+DDDQZNeznInkK2JsRx5Ze0WTbZWeZgRvsLvy0rR5v8bLgKa/f4AcjWA7R2Ga6gxfGOWIk5LFpbCTqLdcIVUGtyMjw54EZo++iMbdEhcRvNH18vzT37wCA7PDwrtpvCjbrq7yF9WxC384uGK8wb7BqaNloNgWmJf+/LDNEBs++/DUQmh5fSVXTDfDtnIiV5TpYK2NBdhv5aqqCtN4m2R9D8OgBo13ogAURszfJxWvq0g4OLepvRPvcG9y5XNIazw39oxBzpNYBrzGgTRr6sZxHM7uJd9sdrE+dc3mM5A7MQX1F1bF0lvlt5OVjP5nYD/TlT7lme88wUyDPaL/3c4UNeWCKWP/YpQJ+6q5lC2L/HEQXDfjwGwrKBsx8rrMYtntWG2x9nGM1Z94K9gdGf4NpnhMVJbc8qXjOXbNQ6Oz9aVVJgC9++zWgNSG6Zy2le/hFtMauNRCSZEMOdlzCggArwQq29yWoPOJ0VzywNClplXw8XMrvvyWpuqUt9fQNKnywuvYdjf2CAqSZk8HODAvVne7weWL2OfeL6iMiDOlFVjzKe94GTdjbeF0TJsP8xUQG6D3IPt3KnEGak2L3QIi1z+HFptDJyXO29uxcpOYdZaVk8su721dqcJdEk5S1f0CuSehulkvV84KTdbL9xYxfcr57aOP+Pruhhwyy72cmAlFEgFnf069aZYpOAk2XMq4vJk2lF/MsJ2+smiHbezXqRgEX1d14BZSdQvA3fIbJhLhBJrrnjVffdH7f5+CngGk9w4ob8/7dPXgje9OpP5ZgmN26FkoMKPorweV+yI2qx4IBuNOpxYoWBuYjTM6hFzvZkxrG0wL703a90tYZreEvg1lgrLdZNE8LsR1FgtkGOX1/ZpugKeGOrbMDK2Jo7X5aMR1T0GQj0hAd9llGAtIWswUAuMPrO/fTY1KvqQNhls4G2MhGwX5sjz2gxBc8K474ObxFl3xtJ+PEh5yAoDO33adZi74hUmFx/wGFpaWS8DQMY5Xx3x4Osh3rUbWGybmCkcTDEzBi04JkKGllK7vgBM3LVZfu6ry0lvXmcPFfYwjxFGHeiIaUxIgAOELBJvzax7f79qBuGFOH+7IjTd+cST7Fn7R/YvbowCrd+EH8oi13rJkXyK32YMkSH9q4Irb+fkwcaW12IPVPnnHcl/IeVPRwNzRg5Nr5+RjKww8cch6LcKk4jgCpbfJdkovsauwMARCKsZNAjVsYafh2nB+Vid8hop2tiMbkoVWZldbJf62Fc5g7VNqDySgqYpTIoWWEjF4zc2xuHGi/TuYUtIuHOENXyaLtwh44UOeGmX46lkj2ENYJm3CYR+rd6dJswRfu8Xd2b89i5+mXnhbj8Isx7kAiLweE3DiDnACLoBdu39OO7nBai5JbX/zMw1qVyXAELuMRUhhkW0iN3vaBzVnXbaGHlO7c9Mu+DwEdxOUWU3jP2lKBaRN4YuzeTIKZV3St7g12M9Q93RKlU4X0w6wqCngpKF83pLpHgqTacJqW5tdnmg+I0MkLpw95y44BrHVsYyhswYdKhVQNhvvzhifgfcvC58wjYYBjz8e2vuMpKm47NJSmD+9FfI6UagnSoSfxsYsH8wvBAGUAY9tlvOpJwZnspyrXrY6ZJQkfouV9JtZX9C9nT07nWEK6UcbawFsEVYM2eK3AOA63GcRlxW2fVdN1crhYkPcJiDwg2fGRtLFBgy2xxk3Lgye73C0gYVVEFeTf03RyD4NM9ldB0E2JTe5P5zW5dBIT4l1aPhGyyi3EX/wc272rbEAZeqM3gZJx0YZReNvht6jKsmHgTEnFrbM5jGatR+OYh78xHY4cQFiuSYhHCRymksWEl7TrMd6gvZmh8G8y30n9NuhAA6fUM8YEjnJRXDKOCYDGJyMX/97MF+IXWSnDPbV0my5C07ZPZJeXLPqg57Dnbc/bvO0stRsht06sn/Ef/1ZoQ8w3BnNYLio6hRFxPYtMRaHM0uMNlLFkuBYfmEsSdZt1JglBGWS/pweyBR4QRCVAm5lXGUjqDkrRudBWCwAolAHS+nTwaYHJmzxm/7cxWMp7zo/yeJ3mw8XXLU/LAYECPhdniNjt7IM/JqSii03pXw8qdr77UmMmKHkvXbIQFIJE0CSmRIxfMiuBw9eSs5TrzwVg3+7xED4uxfDe5zpNs2E0ILBwxP+JpSLWV69I7rAe/BjhS1aEk82IS0Le1TI3oV+BUlOL7V77hALh1xnZ8LBM2qCvJC0jv3R7tWSo47SbL0Gi+pFrnS27Sbm7bx1pk5GttICATi2+JCUPsLXmtjh2KEQJQdyyuSquQWk4jHQJ8ciXklVvC0vqreIsuBkZ579IrM9QoSsf2uRXEQ+H7h53Rug8nHwh3/W2a0e4dLLsaulLuGuaO9X9sqp0T49SD0P0au7M55mITpqsiu7X6YncLAMtS6Bvx2KECw299/txTeiYS/OqP0Zb5bm72R1YLVloTSZRmV1bp3XS79bsi1p+d9bm4ATSXu93gGmL07XzQ3Lt8iEPIW3J2x0SxxJE3JRNQtNqKexgmtpZoGk0g7Cxej88s12aD41RcOwnKY+9U4zhtZQAUEv7+lK/mOgBoYrMu+zCiNUEdaSsKEFJ1J2j4XWaEC+NFDm3J4ZzfS3AC4K+aq7VKfeifDLz1wR+rWFApVkMHcb8MNT0pKKHsX8SLSVPgNnwRZ7DdyU/0RNZfVK4AsxvUhFILJ8HqrZl3EMGXt/xdGlWA6ZkF1slSTSf68RrnquwRxi+Nu4eTrfTxn06QndTCeiWvZ7nNt5iDM6hPsRDIH8yuvaxgtdQcbPhLAAFvU4MHZkX+5spsZJdTn2enm/hUeFnQ/CPc2cyue1EPgjJ/Qi8BPO6MNAaF8cbJwbT+IpxWtQ1Wl7ioD+BtT9A5M4Rqnaf7SL4luyzmwuMeNtkMPpQk18Q3wkslGIHhJtf3reLzoXQcoJcbw2dGFaJb0d7P2GaOffxNCE6iiy2jaeZFejtHhuzXv5Vv36aC/4lx4MrENv9wvG7IB/zzS+FZAUWIF3fUiVEM5wUHZPQ5U5coHYZjCXtcPwiVDxctMTA56L4o1sGSGchKtsLqNZboaBbm4P+lOjEJL7h8zPmFCm2huKx5RTrM9hCHfgDzdJZYypXiR8vhMR5OswYA5WuXJSMqr0CZE9qiHERNax19GwkCG6nvRhBbayoQXHcmhg6VlEE/0L4E1zFJXWBBhc5o9ULI4ybI/D+Bl16WEDQi4RLSWh2V1i4XhxQyFnHvvLRLY43TFBnTVFsqj6cgLufs2dJp7jzEdBgl+NPgVMvb7WZlMjRu0oBE5cNJkcm6BWf5r5usrKIwHek4NvBlwleBqzB4cxQq6G8n5sgtRg2Nr/1ekadN0RXPWOxoH8n2izrfcImk0gVtvbn6HfSmWl4gKAJG8n9bDJybN6cZdX0qdvXwZrDgKsAWYwyC1WDrguXbbKFZ2T/YguB94FlWPrWeD5WBxg0fxOTCDy24vOzUzIw5KcJ2N2AVYjfw4ImtMlh1UvxrWrVNGCEisE7po5jew2HYhJc18WpachBBWeHaW5kWZ9exXK9cdjxnQECEfq1sNYsi0hbCJJMUYlnk4dPn4N7Ff+3uLL9Lsq3fkWw+9rWO1KVtrREKN6xq7qhVWPONMMM7zQleRocGeUBfLPTFD64vPdR9pjCmPoyv0iF+EYcX7Igxyr5H0ERqSOvW86xA7yBgZIWt8iiAtJGAH5w/lmv1ctClO403GJuy3H1msMpP5HtvXDR6lxRr8qpgHK2eAZlsq8LE4VevoKmsEXaTiMd3DFhTSgHBJEjYIhQRwwyUQCU1FI6WtUpnA3Rs/mYt1rUl6DdoOPBGl89ZZjo7s4MjQexx+VLqkOMLuY4VrFhUA+tH13axKM9MgUmwuaRyw8SIOssgqr0x7YGj501bX75b5uTsUQm6UeGEm7plfpH4vQSe3jTexHFrGN8kYWIYPZ113OWJ4/UHcIbaV9OEpsF+YCYneaPQqjCxwBLzEEKyD82stLIcBdLTWWFM5HBtasdVQQKGxkrUGhG9oyuryV+C/2M5vXnhHmVaneJ5JZGQ32qUSt281CWFF/qy4g1WRkVfTXm8RaDPpqEr/LaPeWfsi/02PasC2obD6lZRCePU47aHwNaDDPFywh8ornxHZ+Ecl2hzEluoOwwWkLRCbjUZbN8NCDjkNEmULBCU96sd+AJXP6XoxkNDwAvP2EY+Y0vBNfaol890FIsk2UGgh41IVSASBNSLc/BX7P2uj+QJ8aJTQF3uyHzRhjaV6+9QpsN5ZVcr6BTzIb8jzeYT3RLB52SyoX9c9vrd/JP+btuXPJ4iaz/e8W9xuY6zWA0IzO1nxEg1h53wnYds1I7m74w9a1EBLdOntCu3VjHTXHShVeKan3pKq7arSPrCz+HVP5IJh+vprJfpaKRtKyOZWTJunGhRjwgLvXsILs8hNWVSPGLtMV8aJzuucAQMFhYMHTc6Wi19kxY/ZkyPPiWZPhWfjuX1zUvYeriHOel6MORWXtl8jThskof7i1cGZ8SDzywzRGpaAknSEoAnICuYpyuhJdg81RZEcww9j9XxIuCEA2Fr32WEpFBbXtm92ibKl17F3UXq85x00Nji7hSPWbqbtYIE2T2XFLN1nyNdkFEYV9AUPOVbcQYFWmmG0I+MdhpMhJxb+aJwt5l1rAbmmjlZpbo1FHC15FILDeJZYEZ+GlBsjmwcHId+G92JbyaQp1DxeSc4idih7Mj3piv2Cl3eJgnecovycFv/bIimLGb9o6KhSTE38GiH0/Ta+tsRKZtdSEeZNuSz78y0Ctw3BC1esx4pAqCRTyVhR+8nC9Vw42kygda9zUHxxE4BORZSKOEvFHL3WZve8Qxi6G2nEDS+z+OFIqo9DBCTfn9TrzJ4Gqvjua1a28a4oSK0i27VFocJo6i0kPdy3mxPO8JjwZpI13Gbg9X4+wb9rNPK6Hy6NVWIvjiqgzDb1azMywV+pwLkdaK8p8m4koH+/xWebKzGxQ1U5fXASgFakxxgYiEv1X4/yNlRUTk0aEV2j2pSYGxd5TCZllmweoGB9C09cXyNZFpC7cR2hu9OJUIfM2ViNlj2P1HifNA0UavseVB1jkjMbxkWxoMBGRMrmkFlyliljPVgxjtq7Lv956cnBRQQcYHfdFCQVYuxWg/kmBpFgywcmjzV5HVy22SdF5HMNws2caEmhaKMBW+WfjHY9Ofq9K189SNqdsHjTHYNCVCqKvkTbBnxmztmGAKPqh9EwXSAgtLZPEiRoOVP4Zt5Ms2cp4Dx9w6bd4hkIOQl7NojFdJOxnEXmN/O9hUvubWn3FqpnXg+WJT0WW9eMznnGcyofN+Md9Za8kaQ6A075gnvzJHf/oiVaZlz9BJqb8RT7bIpaLQJ/mV8LRaV0PhvTHufMJ8d71zpyrNRPejF4fYGBrcU1M/kZtpCcpYOiyWRPs+OGW2oom8FyHZhGFvnXuNCD0aN3aaDNJcVGT9NlsF3mDAk18D/2/Sx3E47BMd7wJPq0wwkGNAwlADxZB7mExSAfHsPMqLyPydkt7XAFJD1d6yvsrppdgc1tbUrCrCN1fqbLyP+dAvsFB4h7So447ODFGY0c+vQXcU2lX8KvCvJ4A/m564rKzKBd4L4g4r4F9VWcUihDJMbqiABCuPHBwxZQ7lPcnYw7OitcNrIt/DTCGHIV5xKwJPEm8VbQhQ1MjwtoDr+Gjcsn7vJxYuH1ArIvsFTP8z/4aMWAzmXl4BNsV6+TsPQFZTZzlqqauAf+D8I2/hhiv9mf01z/nETquUa38xeeEnnPLE5COcSDOsoIiPj1xuXQPdNC2jbjIal6gMaQVBldB4KEmRXNQ0WlmzYk5uZSGHL4/74qlkvZKwFloASTFEIi32qke6q7f6T8KfgBAM3yePmRYZqze/mQ0PRGrN8CbE7ZWSY1qeSNf4AwnBtUZTF43ILPG3Svhsqqwl7Tc321j5JkCrVGDfu58aDQKl5LAI1kCFzMBKHdeIp7LMgH62EfB4GZh7ySujfIH7dGE/vZN6yScvMBf/3gFbnPFLgCDAcK4miABN+GLuqKiCHT9Qgn/mE2EVqhFtkLdvAf+lZt6lVQz33qHpoRscEGP66RNjnS6KgxRoxxkch7CZt90aNEC89oQk74fznvO2iOeH3a/mqG6XvGuKyuPamu6GEfCGQMYrazIHDzABSuz7JmyfiViNq/OY+6tOI93rscXH432ZFIMtR9wu9cxVOjsD+PMJ+Ejl2VOEz2WfdvARovGdbc/2/o7r5+iXB0l8psFpgXo61hRXYxeEW9+nNC4NZ/7q81S8HgmHzGlJDIlEL5h02pRLA1MU4cggaAP9ZPVTgomxjEmn1pQCB76nRYSbmEx5jLEOcXTojCk0702SRwksPvNplLFji5GbLZrMuMeN3D/TBnnIN+buNd85SCXpE66pmQIf+Nh1eTjnhhkpn9jL0SR16Bel0jZeP7ErHvCbnE6b/vu8xwqg7a3SinKjiJaW2Zf2PQ+eWtiRWd+YdeNUV2AOUuJrvhOFR5+URRC8NfrI5yT8tcdQDlyMcEhVIrXrwgQbr/DqZ1Jf4mKETGzq3Wth0DXxxpPLtPzHJ6RDnmOcpv5snfx2OZhp3xRObmt2LPqbE/wHcbwdliQeA3nkgMFiCnCzn9bfop/VTq8SOvmBz0+KAUWYIdcqMazK4eQQv5759rVhPkcqebFBNhWwfIFt8W6r9qNVko3ULCbT/K3ykLr66929DYwIGgJ377dF/5GIAUfFm2aOddOxPI6J4RYNuCHB+NcrsD+7Bt6PEtrHIP/cnbt4FBNqoGVhFMOlC3jvLEKB448BmV7CsmRvCvl+qDI0AMKq0CWAsGT9PpG8Xi3rAIVEYVKt7r64Tj7FaNz5SYNNo8LwD4+wgdWAmEgLdvSfwH8MT7B6kGThgweFzNJfe9BPCsDC5lQJfFUzdfKvVrWJJsy5QNk3hr80BgiT39avQLvDbobsKgErCWVuFwzwqCBGksKXmXC63l5IYd5uSwb8uCkNb15VpS4xpz7F1EfRmegj4tt2sa68eg4q4r7BptJodMO4ahXHcc3ik5MMb8R+QPKkz8GXuV/Jh12FvalnusiUX+28+53WM14LM9OlCQp0pZuMH/BwBxs7aoTsyOPmQ/T+NKzBf76IQ5Zh+5b1NqvU4uWQ530MzG6p9ce5zA0B+DNs8WeIURkVm6Py79asqrNua0+TAjKRBZFHrlO1fraJuMO23BfLk+8PNHtLasXQm514b415HpKwudycYpndo7gduu4g2bJEeT3XsuG13TmfmKBYjE2QmAYa0ucU8nbbLDfU/av0uClqpM5q5YgvA58DGlpIFfCHaEE0QRkMaolU8g8jyuE1f5kCYDg5H136fwAhe188fF4F1f/8ZROAAQlGmxT/9w7pb0T/ABhOMJJm621yPgg7zD9PDdYnqJLCsFZ1EckN3ERkpRp5tv0ORDMtC8+qKZhV9X90HV5rItRjdy5v4wdMmnbVZZEUgTYPpOXAWv25Q6YJtWiwSxNhd6dAYc1sPEwGfH9fbl0OW1Lm77YKbj8x1TKSi2b0gogskVOkz0pgAQ2gJL/v/FZNKPfQHkMSJbQwclJVTb1GcOCGC+A8uyX+KHxTVeqjzbQf2tLhHqGKn0MbjOMSdiSZHSv+2QxMB6466YwaShqg/sG9FzSVG7lS/tgTnXfojK5DufHUN63D4Un+3yfklAhw3fIO+ja5sCSVexqXi1kz6tY6NqbTjVdrSZfWjx6PTBZ38h+D2M7gH6qywRyOZ7nbl1j+e9Jw47fsZgnjyYKv8pRCT7AX+ghD6tWWQvvQbyLi/q/gj8gP3RKMTmHbcwoFRI0beCsxEHh9ijfDgQfLQ7Y21dnDF7T8dODmyzuO7E2npSvVH5V5VMwUSorEV7WFNkaO9GSqXSCmUnpjQNQib1igt/ScYn4uu7g3ODNjJd3Lkn0UWxJ/yLfIWFOq2s0ahP5hB+YeaALUtNdvimjCbPANwM8TWXLWWOSFZzKi7yvWWYdDOLSznY0D3dseHuZS+k87H9zCv6e0IwLrnlZZDQ61CsBAD98CtcRGWaGhr/yr3NuBzwmEq0OD/Gj/t4C5tm/kxPWbIOmFgyXriFqjnZWsmZzs2rn3XmaAYl1fzcKc7E+FgN4p4sfOvBylaKE/GHRiwL5b7wmGFbGGdAQOySKJO+yOWYe15B9yDAW7Ib4OtK80RkG7nfi+FJcZI1hGdByycktO8hkLFDf1XIvkOZ6v/eNrcpUh11Sl1ClMAn4+DcdtneSenWrX4TZSXONCyOJtNwRXmSoIy9g+kNNKg5gT2zrxVQXyOgrz2ES1otjp050+QytVa3DZ1/ZjaCtQegSjH8NNfGx5sk5RDJCy6xO/IJGzPwUEJHc0I0F1qJkXQetgiS46BBe/Gk77GSioenYCkeRH8sNOuaBmXnIoiAe4xoOfwQtxErxCC1+B5Ume7d9OitfcfFqEX7MrdV7ezcMb0tmAeXMH7W1BnrbmXvXu3knaR8zSd5KPDO8ghc+LO0QNELzjRc0AbaYtDVxer4lrtCsbkwbh3/5g83hEsRunzgfXOBXiaSin4PQgHMr5c//upuHmwVmuqij9XIz71ErobguAdu0WpN8CbgGkKS3V+v/rcyVhsEh08NjQ/LRC6cPBTQLDWR18UKBfzZwsNdBLwyljoHg7rTOXX6jKWvf9ASj790gQ1SfnP9ZHpfkk1LPfGFy1PzX1lilhPiBiMLxElV6i0cdGv4dQ3exoNXG1LseOOVeAu6wg0vdzzIKT4H8W/IKxOCJ4JjQoB0t6AtPYMDGPMPhfuu1gGgfTM4wIR9YL08vPZbGKqJxCDKCWSNhglJiVFJqpk3ZaNHV5UM6GIBkh0C0MbXSAtop5JEBVvuZq8HDPLQlsI3Nv0xYVk+Toez3uIo43SxLEtPhXfLee1wIMceVKiTIN8XFgB4MmFI7wZYw4QDwpDw32Y0i/ioOCBnioi1GI624u3LN1nc+PIT41VQ9yY6qsb4doXhjROu5XlKKht5w924yTmf/3nctCHmYAIphQe6F22+9fO5TPPCR+rwS3yKr22qs/4vO3xiKjBXq+te+Nd4/xNTbmTP8P3Xo+KDahRuA0M8gZaJ5RUe0+Us1Gr1GbuJ0j/DJWr9jWFmscf0X558y5HGZo7Jv5o4zO6JUx46YRdL6oB5cp7cvJrKraqEoKrv/N14rNmCIclMqy1smMV/y0BY5tebILyZzlmkq3WkkSTiIQyIpqJVUWXAd+x5lkF7qhSev/VFDwVSh2XcXqfePSeY7Uwmn1FZsutnY2IaLaQsZ0h7cw+M1q2XJM8SKcWt68VmQ9xepavdmy6AqUtnhkK21jJcyYViiw2yyGDBb6sQRU7FnpHOFZzbOd4wUChbYp69ZylK6gxehClVD2tVphS5UfnF5jnzO2LuivZ+r4KKvfaXzut41zd+qqklXTvS8PDtXq3Vj9zxOJajwxarE+Dv8tb5Sm9HeNTrFOQCGjncYxKDKVU/jpryW0sF85yHqwLFUG6p5ojzAH9x9RaNbdew//crylG/A2tS6mtbOdiUxJzCBpDkCvIyv8HLv/4AJ5Um5wYjbJP9sU8Z0dbyKnXBrvoJD4rDpqChWts6Rg9CueQ28jQxQ00yYi5qta7DFYHToGfjir3mlmMOqxSrm5CYy5RWiKmhQWNKPAaI69cpHQ0H0lKdNo3zKpZlF1gJst5+UG+yWyZvB13Dun1x26tNor5VU3W+xnbJq9D7m+dO7qRQqGBZ2vyWPChmgmpENTkXOqOn1sglu2f2yhAC+b600y8HgqVj3VN4TpdJhlcLehJHyjK1/Lpdm0p2vhM+TajWwgH05nlfTrLKLxLV4JeCFXkGLeeLVHyoZWxuwKkP8fTstcKMfHLz8Ku50KYId3wI2wmTd1EaH6+IwupCHjoCWH0iMlHo0kHyIn7OgDsWp59zrVf5OrxIe1TPH513sNLtGlNQjFMaiqYqy4xjUrKkgsv4xSOyWxtY4dpB85lW2M8C/W7Qsitq+IRo1zvj2Q/9DfaZAgISOde9HxvFuXvpossQ2pYDdh5IEW//X2oSLF2NM2lxKIODi4UHEF+yBnlthSPgo//oKYLaVivF5UK6cyxwpf09L5HAHOV5W7GKuw0G4CqMLuzizWGxdLGKRhPIb8dN7AppxkOSqvQn84g63Z5FiZYfC4rOlUX5DE+oiIxxneWX1FcBIHSuCMfu30RS2rmNDTbGpJ/HEsskSN/s3z1l8rQTLtNgN7uMiL6DkHspee9fX60/jxk45keHlVBCUkhrKW9Aba4SG5pjzLNSgoMn9j3q/EqZsv9v4DVUkxwW262MpQ1TzoOk+uiz1+UOA9JfE7HkcE3EPkfmgrt9x/RLaNNevulBlfeZHMyv6fQurOL1nnBZISc/aYFsYo2xvga+UXD8CJLWJbOt300WM6zhK4z2BW9BHaSdast2EgOhj/izybxV0U3ru2R5MMzIqWB7Neto0GZrt4jXENgedKaV+1m3UUlOSMrDHMsGGwMjHDBFroNIisxWv1LjVf287jYUJ+zil+egT04NF7pRt32SGuEmBMFY8XKbUuF+3jFB3U/1H52ZHaIfsNwx6FmdccSm/S5Maj4Zt6LoeFZonOo9LIkZ8+XqGrA0GfF5FqUYD4sYX0rFpunVmJJ8mx/GD+j3wQ+is1ArXjnfSbc/x6NAHQt8x7Y8NGUYOjxt6mcdDb9tI7HlgdEBJsDS352ZAFtXcB3y83wIZTd+YJ1encunKtDjx0xZHaznrCkddH8kcNiJn/BABhyLh9bz/0fnEDGPZRIRwOsTQbO09bmMElFw55tZ+jM/wUjdsMyAv5zDoWw+st5ZLWE2Cgm5PnkFzrddTAfpE+cl5Yxfb5cc7mCBlHUCIcewV9P9hi95NXC2+rIDuHpaBFO6nKjvNpYBFp+NKodTFEsmXSKQYjGv2g+LSaw6nVLU5Ais71CeCEpVZga5Haqlu23Gmdq4eCKfexP/3+e1tg7p2LhubqLOQizSaaIGPF7Yjhmn5AtW5oJSLUuEaoRRWqjXKEKbC+VouOmBWSWNt++98L40xlbNzgsv8NsFH+BJ8iHHMC/mytv/0P507jXZT9kQWW8LPZ67YBGROAyElxrnnSizhonoFmuLAao+rSpy893J3fwE7EQcfbcPTs9J7NDjAiyvPRyXwHn+xk0zGnL9d4p9+Kq7dR734xkUA+m3n1zpSRjUcI5oz/EBvSMIR1bQxVI+iqQKVxymzpJUnoev4zxExlmytwXW7teb4FQu1YymZbFQSK+sC7htHMFqlQC2t45dgtewv82/qI/OntXnt9RTyvL+He4Q0rV/cpU1k/8+1QCR7Hz9Wu+qHpg/GGXoX91DaDIUSlkOH94CiKkNolExvmJVpzxJIbzSz8n6FfgRrkq7ctEo9e2LPZOdA3laWQg5bDOe2vVy91y6g18/Jlg4EJovsn3moTZlKTyiE5sZDzM6Kvdc2UA/TcsuqJ+TY1c4Gogvyk3H73Gql8bqfeqajDJMBr104mjEvSVwl6gWfx+QzcZZ68LaeSNfofn/T9AGNxzR3kG/311aJlLSGpa2qxGT6qlA/VcHZeq9smSBAOdMKO7lzSXMe6eLDEHOhHdrUiQbVLtL0Gu6HccfWKM15+3n016VU/JNe+rrud9Qnbn+TpNwJ52L5BhPM4fQZryKXDr18p0dEPb7iqa3x1lYVznNXCl2qfpTpRVzJShZb7K3YjPope6aE4JOGI0MFKXidwaej0P0obRnojot/V4DksNleSU+yZ3em1dpdqvccw2P1ghouE0EQhystL07oAKBtZ54KbfyQSSqb4lHRRTj6HOlxx0RwgwworbpfwuRIY/RaR0HkmfAkbhL3UH7sV5gmT98Nse37Dl5Pt+XONQpjWpbx5S7s7xFDoJ2Ei+6EA2eBk/EsrNv9AC+MFFjuVREPhT/P49MYY0vu0K1GowHE1HQV9+ge27/uSQbpdYbMZlJMn6WqTEayEB5dOVlC6TdhQ2apEW8QSmMy6Qk4w0I87RgMdELYKXAC2gSeW6KVBbmWhPVUHwe6FrM/7JP1T/hOVO4DgR3pBfEQg5DvqF3hrEhMmQl0a6qLKTJW+vx0vPClAYTYTPhGDpK0GrRvSZskDxiBvnQluio3GEH7IC5Ny5EHJovnFXAxt4RjTJ1ZuJzHwDzgJ6UcQxbrrPsTKZSHiQrnj10PRsO5DKPtIebgOSQS75P/Mgf1QCGCg1ANnAymycOk9pZkFs1D6IPnVCM6RvMC14gXkAyRK1iVSXQPoqZcEkM6213fA5SDZCCb1cYw3uLTl3yt5q82DRQD3tKn9aXpjv59dS/SIJMVmvZ3EPF7yw0JjKQIFtuHpyw9Y43UL4BetTEzkgx39PWtXcmoY1D0mOnuWEChAa6/fo8Pa8fP4XL5tKojcrsFlSoyphL6naJDuoQl8fUjUhRt8jKt643XHUohk4AIFfidptskXvGsFSPFYb1khjTJkoFCtSqBLtLluyyJXJsGNvFeWaD7nJTVH9yO09FF3x44LA/4Wy7GWPnbI+63oNFcqyRx0d0gk+iCjyeVV6HVV39PIvzXzxq3SqLFN/izE1jZwQoISGT/uySD0aRgwvtGY5KqoAnyTs1iNRNUXWp/aQB0YNNkefXx0mX9UBKUec8CQyKdDIRNepqdgQeLvvHYn999xNvWOpd/k5lt+wkb/cyFXKX4dPt3DtRdV1ZtcYQHFbl0xtd0fhSDhvOFbZ2769xUk4BOtRSWfbURqwY/yndHWb8ETma8XdmPVfwtZlemq245dqp9RNcpaQKzMziDaMfiPSy/cfirRcjOh2qsoyRCmcTX//xfnU6RUrhafnpjlC08KwZjC8ZQGv6i1WxVhVm78PPKx/qoPReLrBGDZ7uKUo3kJrn1p+bsD2W3eVNPaTErZROSQlEm2SstcI7SkCUa1naT1bLDrB1hDyPvPYUl1/Jg87++JLaNEkbd6RN2sJJDs/uNGw9UU9CEoGVgu95bxaGSRRLmsloElI0NGKCVTL2abTnIUKcZ1tSNUyIhiYV0Aps540OrGAKQitUwlm3tH4GrhNJGxU3njJLVXV3/j58BK71f1OrZsKblRH2ded+U3w5jJg+sVHER7PWgFhCrgNfJuabbYOTkKX+q5x0BUYZh+5DwuiPrK5USIoH6tY1Z+bhqDjxY33Cz24hOJgy1qr0YxP+/I/XxwJ/02TLxTp8evYrxSoYPkFIJcccBjwqUWJDSS2/u/DK+ZuDHcfcQb6qi2QPELE1pRP47MoF0+DK5Vv6V0QsDgBku9k+hANcMqycDIi4dUyb2sTGEy0q5mf4crimT2Phc0yCOG8f+MmuEWk/5qn1pNG2E7/TPf80U1ArmY9ZfTFjMm6oxUw5CV9rgcLQbQmvzdHJuIM04LtTJF2nyL4lk9Wdw7jw/W5P0h1AhNTq3GU0SkiXD9eMkFLgSd0NfGA3pjclrvdie0lRwE1boO/+e/xT9pCTV/ciNFFQZQIvXa6iYMzTgjI7WstTzcOZkyvuem8biYCYqCdgacnFpS5haoSvyXMJjGM2Tn39mEHajH1qJERXYNIyJ9e8gX1Iuyu0SKTJP3y+sARM+qBK5D0TjBOeBglc/OTUrddlWBHx1qJdSPz4izMgBNvqW9dQyt9WuUQ+UgihAjwaeRE47Ym+mWBK/aAawMw33gL2Rxc6EgIhswyeRK57/HwCpwO+Tp4Ign/Hom1wANeoxrXcgYG8t9cFRwoxyCxmzeNNeloNb2QXelcrQuUhIV1edxBczqIUvd4rc7YGAsNn3ElP1GGHvzP6euCl8GmRKcWrsbP2+aqtcyeQWwWBSnAtMrZQjgZpGA5x2cMN5+fnx/GOLeLIlQBmrNNKiwCMLrIIW0gv77X4SNK/beTdk8ApqblF8k3gQt9ihc/V9F8ucz/DOKyewMvArLBHzondBs50K3luQ95DJx8MUtu2PgJRlFeQbaxx5dpPd8FEEAWGs0N9sHMLN+dzK/eu65FHTCTfL+88He1uEhB8LEJ4SHljwmTb0eub0cySUYEYklu6eonZYkyVYxem800tEE4jXpAGhl/CIhJMDUiLZadOniqG8gJnM29S9QhIWrPWhgycDwHsJuVAYjE/hTyBIESyeAluUCwhbMZ01yMJ3KEi9NZG3oe5k0TjEkRjQP41CegCE+jfmTlKlvHgy4qqWVGjX3J0jXMbXoZLaV27OKEdMJ3Z+Uc3ViOYxORcQbmLeIqTPo8vRCuYKrxZ+15wGXjasCZZnVAfs2rcf3URYQjnTcLxPiFNLHNBkTCwUHDX7zdVzyYyABiHi9qZDT62fCNjlBk/k7ocdWobZifbuGFpr+kv+Eck3Ww+0/1nglwbDDKOc31HyMnaCkLmnEKgCl0tuR4HktNrqNDcO/x2xTTlSe+7JpEc4AMbTfeV1ALKeT3cIwC7XZFms+2YWBcNnywgghsgurLl2y46OATGrVIEXI9bMqi5RxPLiGA6pmIjV2eaC+TlLph9yt99Q8t8Ilg2eVB8RBq/zS2q6H4b4EasIR5HbOm3M5yHkTMpz/Iqh9dvwZb1i1t81rhYYHsW7JkGZvwjW3eaHEkfq3aGlN7FZB10RGaqUknpZUbMGkNHLQDikKALeO4CJPGhn7qOz/waPb81NHkIWSUtfMaF1zoFGUkEoRSrOjrCa2KTRnPsUpo7lBpeVKzGd+265sOKjihSVdKBVuhL2o91gvs3vmD/bi7d3SM7NMK8AI//k4cjWqZG2EQajSS6hQH+ekZlGp4pAtGQHk7Zhh/Oi0RK2wI/JN1Jg3LcUe2z/mQAXvBBNDH7h7zDJQ9of5SoFkS8VmVxafGhVjXVV34qz2vJrWH07frtJqpgDVgO9cR027m7iWQCtTE+bjisNzy/ULufWxzSZQFFOG20tzV3UpJ44OLDjbEo4NWmeL3z6G8sfGi+2Qr4AaEjJkRM78KHggbu5iiA0ybDcJEHVRr/TdsVMNzXmbsSEaANwCY2lrssscG0gw9AJavO5FM2EeO4G3PGcXGFW92dYnZNotZutkHMf0YU+HF/PZsc73uGbygz53ET6f7yM+KaapYFE7kQF8bxUomI9G1T9MSIslIPukbLbsn97613rvXE5c5mhr4RxwwaTMg4aGZC7rhibl1ybpCpk4oDMMTawdJll8aGDcTmyS8+7bx18wC2P8W4hqiKR47SPbWOFH9HAW7mRaX2ckLKDvM/5Rj3RZKbRXpSwSBTKIjsLBMFHj5hWSDJC+bevFIiM9hZYzC1ua5LHWvaoN59ZniTQfNXpnf5Vqr6tWQGgEk5PBlVWv37qwU5rg32ecaevfXjk1kUduG8uKDNwXi8t+XAkjZ0G60q2cL1y1NTmH7ZCpFIzK9qHb7a9HmUUiz9Pb5Jg+NmA39vqRF0R+KdxRqBLGDc92+/oJ9DWE4cFsC/r+xPnYeJNtxTq9heMKJ1pYxnXmqQzs/ncia0Y5VxF1XlXLLue42kxmH077IQo9Xnnw4WKzsfNVykRiz0DAxcQIcMJm796RHBWyNX1Vi76yz332pOTmYY92IUk+NxW+i5/XyulmZ4LyhJyyru3Gj5LEc1cjGlV4ZZqlhQI1ERrwffZIkYKZ8YHO2oq8zX1DHwEyHmGoN5WjsaLQdSRAj9DQf2w2eUoLyy41caVLbHs0EsvIEo25zLKfsunDWJWWvltgtN893dO+mOGKkrNXxp2WW4+WPNzczKsUNdonyEZaWhh7TCjagpJYZOv24/zP+pG4ZnzRFza7i5n1RLRPu5YbFwD08m65bY74L/oIk4gRThEeeDDq9f/frsHhkQDsqVWT2x5qQOUMu6KwA6TgbRxcOIlaGDq3nks2K4blwIes/KHm/ij40UrJWt0/XS7t8jL57KKuxb1Lyu4QKjsbvndyOa1x5+nZ8ehry8Llu9cpgqU/03QwqzvqbxpZRt6b+FQ/uFTjOJfp9BHAoDpzBSkgNzKXqg+3a1PlB7+hlpVAYBupDN6mM4+iCPMNjvcFIKwKtIEQeP3+OHVd8nyrFwuLaCAyoTfJfX05J6n4Kw8KWZTHXkOkSonhhwRmTJYCYdE1Z/IcKg0rCztyO7iZ+dXfgxtBd5eRuunhHIxRW7QjxKYEUAJUcCZvv1pnz9Cf/d9n08nb2sEJj3SDVuZ+l4GD8dJEGBv2qCvIl63ct2bD8PklDgSkVZd+0d2Ebs3pe+cN6liLyF06TO64YT5/HZ0U8G1vWI5Q4aQZfrjd7NLEeICgnFe5uaGunC97Yl7jWbTmBQVm0nmkPlht2IVCyvhOpPbZcd/xb7+ThVsSI9pkgBYPHtuoEzpb8xazo5HYEZYPJcYOPHCqSw5wCIOZJQ0C9I8Gy6QFg90HpqppSelbNtq8ptV0rt3A/WBjMrvmbAmpkp2u9W0QRAcn6eueS7vjCMYbtonLGCSXGKfApdplFD5RSzmwSdbJFk1tNLsyqj5nNhlpcZ7eXefeh8vf3oGkh547LJihPMw2m+DX4BOHhZubmHa4jBi3hQQqhmt0wa9yQAv3lL4RCAt7pIKfb/LxGMN6Lm6uWQUORW6YLduruFqipZJQLTmcqNVghDaUpP8bDsW9rbKJODhnGGpKKNExxKXNA5kH8rSHCxWYkiqmyd4hmyxXtDn6AuU6iajflIsZxyLjtqieOQhBgkvZGQy8NpoiLFz6b1CSEX93GrxdwaGSqAOAElaarXpp4aXzzj3C5k+fZODPt+8IOHvJ9iSLhE1eCCtGK5RCjoPAkOZxbSUf65J6eR7Z98j5Mq9rPncmlbrvPTn0n9N1yIwnFmK4u9ZiFMYiRz70CoRRXm/DF3DS8zIQsaBy0ZHhrEwOq8of3EwBI2VyScJagybA1gm27Bm9PfPBp7Wm1Ed0cCLf+H4ig8besfS3Zsqtxy3bzJUV2N1veA/CoRAETsG83uVk40NKqirC613vQ81F3FrufxkRTg9gul/mX+T1FyjSQQKtTgPbWogtyDWp2Sx4Ci9XIAR5fPk98QghY4EywkCuK3PvXLDHX0oyFofWGAuTxMlUHWXjhUp4tNqBLdYlxZubhVz/9mfT17g+vIaCmbyTya3qacWfScQ7Pt0SFLVkJO+GLPnbTuE9mfH+awN6cMP+4sDswL+i4clnaztf6xH86DXor+slsH7YK4qiL361FEU3Tn3svyDvhPZJDghM0CNKEUKMIpBJzrLdn/y6+3CVuNYoIk4ED2DcqN8fe6l00pnktiLNpl1duKPjzGST2NtiOXTmi8efS8aSUC9iLdeqrJZypBKXxd2Wvpi7U00tCOLqGmpHdEFK4ZPDhQiG0aTZni1ZXBsWnwR/JOUJ/PYgbz39jLYeFxVrhw/6Z+zuoztseMeHXn03k+qcdFT6tqXdx4wUFXUHs8dZLfItqvvvfjgzWAfrlDJqjYpmn9DQ7xg+EnS60GeIrz/fwHInLurpSPUkCkSutuwiGEYi2Upgj4Ifo14Be/bmBvz0me8inPeg7JW0QZDuDQ8Ec9jc8pDLghyYPw9heYwY+7ovU1xO4/4CZvVkH/sC/1WEXFO2y6iBDMb5EbQFdlTwgdHpb/fFPEM5m6zarmU1WJsTvesI/e7sqznmwrCpgSWiVZIcKC6GOygC9rY7fUdSYuD+5LTsiQjb/X0EgBrlluZ2NttiAeTMaB6s5cO8/h0Y1s2zvjaabiWlKCeiXufifpUTxNViBZPMCuFjHokNHkWAyvH+TdxSvPMuYmv9Sg2Fwa0mj7RFjI1+E9qE1NvLbwj0F5IJN3+yVqunUSJGDtE8DSrvImSC/z6XwNzm4ajoNC+oD7bSZXafb40eLLwPAKle+Mj2RkWCs8Zi1/akLjXtWr0a7Vb1UJ1/nxx+hEyIvzETanES/PlKTlgDhWNO7ZxERA0L1oWgy69Ews3I3GpzTyh81mlYBVOK74DHUM/06YPNKxXhs/kdLdJYc3uph7ux/ZK4akkeWrVttoK2phtma+G83KQdwElt+dCXJZbRJlbgwVcWv3oVf3HrE54+yztoGV3NXFYP5NBX5GPdjrs7zpinAzAFCnnIMaYWSAIbtPaiqpE3JJH5zB9JWU3uXRjY/ZUYZ0KnnZNXqQ+zM90HI3kICQXqi9hAGkIrisbxGtjYuSVrsxGivA66OvW0up1dvZVz6ZjynfsjxuauhYFknAK2xCXuiVvjGLm8oycvHtnkuQpuOfsH3S4QL+f2MijvmTMsz3oRxSr6mug05m5//TY9VDrfmkfCT0uEwU4IXnYbSmH3OoqAs766Vh7YA56CdwG9qI/6/82IeC+49TharD2TfjEzQpbr8kgOG/kRt3QXfXqZsdjoGpQageXU4AqrqORt9d7qbxWmLT+MPYuxt3cC1o8LNbDnEShV31LNC9VRY41uh0v0hjGBxvaRDtD+2pchhVK1uI8linSEulpkFZZvJkWaA6q6/BoG8706GPaoH+aMzX2DXjLbSGDzvgz7kIqJDYsk9Ge4Mvqoc6qdmTzUR/cWTP/hvgoA2Wi0+yI1jRbfZIWgK2OoQNF6ou6JNGNaqezGFOU5O9h4jojn60HTpPdRQHUw+bxKv/sXn/OadYeCqKI1d3epPb3zOyqOOCFdMhYfNRN1bwX8dFS76hBYOAU5l9Izr+sry1ZCoVGvl8HF18p1Txo0IU7YFcEKZdhkrAJ496OIjaZG8aGpehhCfcVSWLwCdRbrWbBr2u1tVG1OCq1qI3O8lyyKwfIDoNwoH3SxSgo32/NCKEUWWCrn66UiiqTid3Mc2qRIJwlEP3DjSUfWBTQ61bZJRyMeb1rmBHUpkL1Vgb+nQmg0rLEXV3GJnOHACsgpgyxB17uqSKnZHa80tUkNpXQ6pA+nli23IPQ+12P9sowbFk1qhvgPQn7pSD2ss5eGIo7QemnQvcP9XmH1npXDUBd+Tiu6zRHUTEsRMpGVMQ7AbmnxfgZ3rfCKVOHePL2JAv3j4dW9ZftQeEmgUpWdJN4cLSicoZZcm9BJM7Kl21HWr9fSQ8ZDUMYrrMKT54XRKCHzRCpUMALCAZK+Yv1QPIDFyq87KPMzVyejT2wY8ATY+yomUlkUMjL/FOJ6jsUJZBpcv1vEH7UDyXaXq3qXeUl+dYlRH1tpE+GCPWEwZzZr9eLIFuqGzz2iGg7hS7zzoZIGUqmezPvepXgGtOv/VrY1x/CDlTBSwlOIByjbAgNfeacZ33bupAlOfSpQTnXiIe7hiblTQ82t8qwFMkGzSQOdlf2lWIuRpdAhMnFg5sRyRNfjQNjQf2tSPNRHLB+oq2PdaY/kvDcBsq1JEKxsa81uacLr1O9rpx9Feib3O0oS8Tg/c42yD8vV5jLXw+P++w9J6XZ+2m0QSt4IwPr87ZvsG3iRx7IWHqsxUGnlL2P6pRw36Lcw139BBrglPjX7oUWb9oIfIHJ27jABWkTO02fMbUNzvTXjkWiYDvaXos1hxFJUXkFhqISvYlEmdfgGcGZb3cqvyrGo3hnyfpegmhfpcirNtsNUXJ61gT2JQwF4Ir98KiXickfP90gjM2YckCyF+01K7TagcRR4Ggtgr8lwdYOvItjJMs2h4Thc65Gf9tHf0plL1WFgXc5g+tQeiDoX3y5MhiZNiCjVOlB1vqu2Xkd/Y/4ef2MyjB0hooAbVn2rwIbvgWuZLRA2Vv6YF6DtoGrD+xW9NC1X6lqi8ARG+vtfdwXSjv293kuZ/+HOueC6tZP/t7gIhJce9M+OXwPIoBARWR4u+6ezz50o04NO5YMW5tPvG4/hzpSSr7BgOpKhuCCwGTk+Wz1ALQ96hwELwQhujwsk+klGcqI6gWQEKletBBd/0w0ykNWw1UlsgoZbGNzQjjdKjrnYCLRVBHdYad25cNyOPhBbf88c9rOj4pqORESUq0d8PSAFbiqgw7bony7cnPf3NYnad4iL73XHqiwXKk8LxpLxF4c8zjfmuZf5Uqf5399pGqAcEdG+Ydr/rLvg0pBe4Dzx9goU9v66Xu+6IFYfBh7wBoSzUy7uRCw3zn2Kq3hJa6YXIR5cZm+8i/Zb7whM5Y7PREEKhLF5Rk3rBFpu27cuiFGmLuzjCKwuTRPgnRr/mSzYwldSsM8yh2Z8jk/HoZFzh2UKn4ZD3dvQRvxPkgTPM7dEBoTg+yl+3lZpqNky5lBeljro7twmUrfVU9EkOdMNjSABu3vFn7CAJAj5t4d0fvrLY4n4ZlMSCCLbLY5a8ul2OF9hg9FEvoUATOlOvLvlZrJdvuRmTeUa1AtQqlpw3TwCugAjiICVRT0NJHI4DgXu7HN1/hQH0THiVAwpvS8nAsaMuO/yLOa1+11/9qiqCCKPtdNG2KyMyEXU70+Y+ijwiSpNoJA4RgQaV5fRgytoKroNmAepVojDlfaU6HN6K7TCrw3gH6OceYpPTufjFR6rQceeEfxhfQyZ0Ng47UpIiF4CsthPuRQepIYiKpi/e+ENSnAgIJi0y0mH1LW6r5znEPLVruORVBOPdQ64gsY/ZmZXWXEd3ocICEMnetTA9viQR8B7cS4qQl7W5SgzP38x4/D7PaiAfp1VdbgdBfS+oGH2yNIanD8Dl7buajSg/bunkQSh84p1tvP/RJmLaGFkZRzzV1s8YFWHKWBgrqRIqH8wY0ozDEXj1ieBId18HwPYXMfzYwEZfX6Pg8Nqp9J30O7dX885k0+5b6UNFCXAoZWIvaF50vYnVIBRhUsvczh9nAk9Ay822dVQgBpsbg88xSNYU/qty6RcacK5mhdQHMBOiPXezWynn6c3l7TRJctF4iJUlGF2M/+c2S3JnO51SiP1iDCVsVtd4NKP8ywlXAUSGyfEJcZwiCSCZBubu6CzDG1oA0FHcZNruzNcQavJTFYvqForLehyhnpJkLhNFxATf6UpCk6MGRGwzlfsy+pnU0fd4pzv8S8+sHj09X0gxfW9iyjY/ldRt/HpcPApJj+MotjQ8RQ+tVmFV0zJtgIafj3OyQ+Db+0RSNZamjQ96tLz+WccWCWniyL3bmH8628ogl314IA87mn2aiAI3yBVbvhsNRlDYSekrySgJQ/DtEj/Uxz3HtgN1YvNrb8VdDOK1gQcvpEPdWaFpNgeXUM21siJqAxA9fhvap1ZqonNE47Yxis0MHY8ywejgVbqV0yeKmKSCPevCme6enTG1MKnI5QNZjIhQd+K7aezl6uegB6VpTIkQkbTmRgeBvAOCq6cvDfZdswIo1nhC4xkNI0g61j2rHuD9WwfhppCK73WK5SDlK9jM/h6QoHkn0kutqTQYgZoTXcnV0bpToJF3UjQnQCBr2egdwkkzmxDwqZ6i3I77EOmOBT5O3oC6qJ4DFlS9HqUeDKvn1bqHmhQQv6GL28Ht8jS/qVShChjsS3v16dtckl4/6y38c154a0KOKvTYcz7+EhONR3T4/MNjH/bGGBcij9yZxiy3Utszqzhaq/phCqMtykDtDctmjpCDsBKFqFpr36lgha/jwfnrI0+X3vKKZzJDi72fyf/3w2ulTPiinVCPQ8Gk3lzwruXP77HvzQ5ELHhbfCEOGizBM3qNskdhZIeGIBpJL2HzQNfotf8Jp8h2uILZXRe1BHd7SXNLI7nk9VZu9LnTjS7+5DCpHN6Nfw4RGIEP3HXZhz4ZdYnmXOUKZEqYFl+DjrXlLlc14nX4z/1uu6Ic9wKJabD3/nxQ1mk7Kva1U3yeyEyjeuvb72IFn0WvWNLyk3wjLzRm6SqzArUx2O8ZU5DGp0RyOo50uPZJzvLvL+xN1L2t6jNdkXHfeauBJa1l2pCMpJw8zWKO3Ue+X515ET65Jvm5vKdVHh8s6UvpeOZq1C8bRdF2/yVBcbdt7WIGQqEuWJvnpKHXQyqKpVAKdb83hQ8WdQfgmZ5uwoM0u7d0cLbjnS5k3AiDtRY43uGHVYUir8Q1pcAgJbYZn2Yb/a9lk/TZNhtlNWQg46feSIXNpE4sM+QA5RGK9vkWamOYOhUUS3ViC5xJHQgJlRPQwVG+94R9v8zJnL2CYdVqfc3auXnvTx2q+LRuXgr1TgrzjzkkVy2s4+Gs1nw/tNal/KiVUT+GO/OgRQjnWS+VF3zgSWF98hCbiqoMjuBWTMr8fR0sMbVouw6m0J67FwCzfoZmCXy3QAk2J2fD2LtPCMgd4Q+yrdpQXzT1Zx5wqEt9k7cuqYxChYtQ4q3YNn5IqKHf4fLpTBYQfME8VA9NwHo63GYp8gPnlhlTB4sg12N0JQ7K4SPqciMQoP2sEsaWk7QpWxqD56vwkR+3Go8XoWePnsXjUNWBcgG/GqOItc1DOtRnQjIeizDaY3NPxvbtEra3baLUFWVuPmmg813h3oo8+LrlLk1yuChizN/ZJ1JDuTQzGjZuOT0lYl7o5zrKns98fMqnXNqyxOAdvalSqrYODCV7bk4QBxD0ExkHUqCzBT7ceQPuHriz2y7bdpcsnVXHC597NAXuGQqnUjf+9xQLLraGXUwV6Oqzyh4JIK94hXBqj8g9VyzGGo3cMY+Wbcwkh6Cef5f7Br0w3Cz8H49547y4uNCRW+Wpvl8haWb9XlBN/eFqx0Ju997ySJ89ovpG6K0Oe5c4rGvf4I7aPHkk6sZMLyQI4EpAX0CzZqIio0TMa7/J4fKHADKaO21r/kVAXIUgp8ZpS8NlaJfX/9r7eNkI1UCCJda4KE7I+nayhumdpTWA/RhXF1dUfZcBJb1h7kTdD9qbS38+gzAwRKki/dcby5SLww+NwabPE/QN1wlyCEG9PvMCtElKcubKULgRAl1D/AogMqY/JZQZ/czv/yvDTU9H4ete/Syh11YhhPwhJv8+lOsrpIy5TtSTJ1opIKa/H/1+zx5WBE18FuSyYj3BR9ovUGRwv2mF0KXB11cskOINGhhfypuQPePH3A0sjwosRqauvSPsG9kwdacevPGrs0dk443Jm7jnHWcLO266QfGbw4grDF0+sn5gP0ghi2fs2sCX5rVNFsEx1hFprRXTNnqgvkG21+a41o8Co8u/Y71ScdS4rolu0ewd/IEgEKS6LwUiSeDm6hR/FIE1XQ7jOAXZO0KdGiL5g4ZCnrSl9k5GrGgz90e+24uZglQVY2VZDSjK7rYVQLdloW7CRolu5pn9wrz6lbnSVG+h1k2wywQlio29CKfZkFYWl9ruBIZ0dBQvKvuCQ6YEPRryyVpEe3r9FFA5PaLYaPM5YWiM4CxLD7aR4K6OVEPW0NIB6UFX+cmp5orvlS8Jq/6vWE6F35MTj62uO2AbqB4EjxkGXlpufB0pTrJfhMxgns7S7lOF9+SiWG6ZtsySKKEJDwuqF2bF+/gXWiUS8mqMankyuj5lyLFa5jBLjdxotyqH+Dear6Ztr5voHRryvxgvfts49Yym8XM1fFUpxP+ssYpCogo/rOjE5AuwchEI6spK7zYpa+bXSa15hTR+BpjCq3g/vAF4lncymil1iM5Kj9HfDjOooo5LDP0Xa3j8TsVcg6Fd8m0KxBSFL+EHsMKzJs0CQPI3JeTl2DHGG4vBK+f3JqovBnfcY+hVyLMXmB9vJnIMYSxvQwkjOd+K5B263I4DM0z5LYpS6tOL4LAWvgH6kYL7ytsgKMnL50UjB7sG/ad4Bki+w0b8fWLtpOKpx1q3HqdUe5IBUOEos9OSzIgtOPXhxNu+oxeKzwabZVcx31NgQ1mpNwn6Y5BjwN5KG7vSWLVtaP/7e2chcy7iSDi1AQzL6bTJM21FYu7D49uiadfxFP1xX0Eo07CP9UF4fsQrXltacSJAmkPdbYtF4oUWKq/W4N1TWJGuKdAXfukbLP8USUOgDCyHb+rwtoKCK5ES7v1nW2O25kIs0eabfxFp8eHqz+jL5uW+LJbF84Fu1rdJrH8WehWepW+chnZMPWOEQQVYMh+iYEHSWzaciXgW+xl6kR12f4VRXjlIA7+RSSbqSorWGXn8r7vdJ4toOHzljdtUbe4a5IrD/dv6Nu68WrnPhjekJ1nKq6Tk0rhm9cVJ4vjn6brH1cuAXD+MD2U6aiewBTk09ILYILiJY98syLDIrrYFEFa7WNXbJ8PckT8/roZ9YDwPKhpYGF7JEOvDjyKrkxCRUsTwK/pXeKQZDST4JKn8XmCjLHiFAc5J46QdGQUHDQMBFQLPtupDPXNZTJwZguUI3xT4CzRdBTFmZ1OzrflzxbE60gCIZbPxLfHLqeFe9BdBFc9nhS5mH2Vs5H7ytjMn6Kche4MFcZvmqP10gXfbSgDuanKGVB8ZTeCx5ylmJUc/oTTOLw/Gfni0sgt3TVmxJ79MZRJvEdS4f9IKmdzZ5TZ/e/cx3q6vPP1Mub5pNASkP7c/4yFbfdipjkN3UQnxsyzvLVr3vVig2GJU5dP/s+/pLz7xa+rPtzPREBnZRKYyk+5U7ITi0MkThRSoZzaPT3CJXGP3HKLCNpKw6Pg3DyVZd+3CKtNb2umRV0XF2GrImz9wpYJQK9vUY/n/KHeZCtRPoG/r6j21AnpGdRB/UWsOGNhuUbfRQ9uUWq9r3XLqW7h8WcCyGpIPoE/dcQEP1HXQNL5aJWT04SFtdBGFmXkybeX5NzQ1dI9B56TtsHI+9rRcLSbKfv6stySSrqWkitkb81LasHW3SOtlYUxdO31KNHV807NkwHgRTcCgyvD0WpDnP0dWZTwQiZOXP2X1pS+dfVsUoaglvuwiL4Q799YC/4A62rXQa/W/vGomAVSiRAdr9rZr64C9Oh0IX5NZzYEqdS02Czs1fDMu7fkjP2MZ4ZMniELioZRbK33n36ywf0xi0EzhrsDjX/a4t/JSVHEh6uoiwnX/iH8BPqzR4MYQiYP/nY9BhTa08bgyeG97TXjxG4fCpCV+kYCgFcr1AYyKLNLDKU9hkadZulWoLDOrlQOXQc13F70vKXL74PDLTGPrSDTUJ1xFMUYTfUs+hk41DYaG/39fcqFi4Uez5i9j1W135ir9BmwfPVQT5lAuHl4+BpaVbfq+PSMZxJ1FFLLzz7S51MPjNnYH4c/P5H45YxqhRIdFAUBx8QiFLdDpU8udGzmZv2ELAXigjRk6vwrMN6FhAzRMkK0eYGuJSBfqRkr75IHqYzRnD3p/WQzvzvE2pRtTbejICsnvziFxRpOuvdWPCgefHCffJoTNM/a/zEpWVA6kHpEn0vfAGxmVEwdkMci/C6BtipfIR7EsDw+DT7NCmTCl7uQaYZDPX6EmvHD/qqoOJ1flSpRf3RrKP2NthlPUJLKn5vvIZde9tQ2iCFqlkIUbrOlqGhgdGNRT0y7ymICRs0vL3t5sLb0GfG51DZXkSzn9KAZiL8LiOltRAHjzGKnRTxYP5N7gZVM0bkNaAdJ3WAxJ5i9w8QzSprg4zRYGyR6xXuKTtadkAn7J2rWbfjCLsSNJ0vxFKTPoLS84YvHpWQIjYhgZpyxK0ZO7iE3eVIH/TuoHr7Ya5KAOQ+gS4TQ0MNioNZT3mj0UA2T4thLQEzRZGMcTkbVr/TfqhWuVbV7tgoefzJ6Dg5wE5HU7GFWIR6PgG0JGPeiLLUETzznvv4omkIBbTgtQXYavYa3GZwBWrJ7VJUGrZCJmd7GMzMBHwT6tiX9dxe/I8kyjcUhvP779gWCBPUrwhUFASjUERj7e4fU6e5fnJtfd4wJ19+NJRb7KX82NxI6fEerRs3m0mWZOgItdquBmAOnaJ9BTR9sAi9jDIvV2k/W8yEqnCvUIWSfvUSWpugy/tukbpT727HxRRzs/ohIFBX2R4NtF5t4g1qPT1E4msfFWxcFAH+zBZqWhqoRxwyhrRgPoO+nJpe4a33VGfdwwm5jPAJ5OvLrziwvWbEi2UFHu4xl0CF5Z1M49G3LZXpU+nt0GTi/gLbCfpZ9V6Z6k7YCLxFGOxB/JvQ1mByXZ5/eJxxFf1iXgQjXNSd9hVg0WEooOq2M2ZYPfbsk41g8JUK+aS/k76KbR4Q8BxuUCCR6cbKKIlWg4qQHB4OaZU9L5QKEKjpHXALCdgBI6bPM3cPQHw0fgxiFBPayP9sa+9F2bnMtdM3jkBF0By4U6f4geJ7Ox2PxUL+Id9un2ahqVE++DEeORMII0pbjqIMLavYvLfvigf1Ty61/Qsv8z6BCU8X5BNk3qvrCnF6TQR1fBHnDlznYwB5d5vZyt/gkGaaUvWJgQoljgmM4wT1sMsFprCyzm2c+jnKCQ7fOoQdz4GZtYd+z2LAi9t7lI/8SWP92dOgnLRNorAPLZ+H4pG0izsoC8v2EHZ6oJ3en7Flum1BHnyJg8M2Y3Vx/OuU6mqHUGYGq2/d32MhukyWO4/NJNoakcv48EswYQyUmrviFLnjHtA7xP2/Wq3uNJlz3yczf+dG3tRSy6f+AFdRgd/pmaCJ4HIA/VT0PWPnYGk5KJA2ubQwKLUOGqfJUrdvv+ax4td06rt1YjYfpufwkwLkV6/xaVFnEDoBtD2PML8au7RpfiFOqcgvL/K/E4wOf5dPmkUIqlBWhih8Ymj/m/XFanIZmjrc6eEu1aQRACGtoS+NIAiNOYCyGQycJ78X7cz/MjuzdR1thhT1BPOkVpC8qzNEEGMlHw3V7D8C6MBMRaUALwgcL1aVXEU8CVdGvSwzTT0/CnM55eJ34SK/hEEUm3N9S7gQLP/mr4kCkqBC8IuZ96zYJRHVTZgSxlU5gVwkuKVCacm6AVRRDrvnlFIKQfnDMOhvYYD6zHshzX5IUA+SUjyaE7J9hyaU5FPrVZ5m5A7vBsLFFnLUMg9T9SPBK2tIFnNHKKKFQ7KfowMFkWBlgKL+iKdV6EqCFs+JmLJ0wc4l4BXeerrxxvZzO57RSGj3OnnqOMYkpHRiqA31NrV07FNWMhC4/aaLE2e/UvalAr4BkjnXDYIBxkt8EsAbhMz2y++ihscVtJtCfR4vyb4E6idO7ZGN+wJsVu6Rrnm2k8tf7jyT9/rh4B80nsGAQNculhxuSjKjxaDTaAELTPYO7xpvrXVuYhRUBP3ZjZLvjUwdD9mHwnsIk/0K+dPN/c03j2a4h56KL/RRewmFXkvVEFL5nIcfg3LbGzB4p4yYyByJCP328a2yRmG5hPnm/f4plzLfc2w1tTrqRxA5PdUFclJnJAqg2uNOHi+xpZeBxPbeAGN3xWIzLuMx6CFHFkdzyw2I0i8TAe/21Fj1QP4z13aBIJPJCp9eGyfTueiN3UoG5O1XgUwXApUlIZaeafoO3USjro6OoHaOV3NfzgPJQOpdhdRWww5NCO3eTYrhjoruTVEePSzRPbJzM+gTQHkNXNW9xRnNfmVPrkq4ydd1APeeTv6FcdEZvp54IFqJ+dUMlvm2Sa1SbaMy42oauoFq9PTXGWucy7BtcDRiXk6aKZaXlZR6oHL83hGbpc8DkQcGeH4yxlJ3pjyz2tyDK2hYsNJiVfFpH1ILgWBzii9t4xkKmWjDQtTZwhLOAPy28M862NJUG4VNWRLIUw0as4ydVIVt/r7FLYMZa/dWllW4IE85osIQCY1e4xPqJMYdOYpM7svZ8gG2eW2S9OC2aK2lppr8q25dc0AU/2JoxEcQN/y3MUqHnFKyIpS7P1XMZoCRvjQ6HjOFkxi6hImkNKEg5W2Dpyw0YBWjO7D0rR1OSRnqec0n2+87qXSwE1YuC/WsE9k8iXpVzjiq75Y8P5x6dbeonC0FEZlDoykyO9/LPNEbJcdf1M1khbmrP/Q3eD2j2djprd9Yw0jiZeajQzskvG+JX9V0Y6UGUCzML+acWOKQOs7+93GSPxNvH1RNYyrzj6qDwq4SAkD5Vek+XvMIpxemxBUpdl4bVw2R1F2YSpb5T69FhfCF+1v8ADBJ+xykcQNnlbbe0iC6Epref5yJY8/gvfLUXLwfSuq6zdQ500CH5qDJ0jFQhoFFb/7DXV3iDom7c5dggMz6aLmjCkBeKKHnwE8qQ+XytPS5l/sj0hV9dfalgGUMyt6UH51sqATMW489OThy448ozN75mlkPc9iexMLVquyodIoln3PqOUST7y1oVUK9vHXiePtHPeIwojwXxgOzJijN793vjmQ+CkKi6XZ59NJQ9oeeN6+t+MVp64AxWzqkR3GE1ogwt7vpn2HaPzVNnL1zNTH1wzbVOsrBisRJWW/NWJIYlzcrRt2PkN9UJalxPcYWmRTNpbo0ngQ9HKGa6ceZQKJ06WwoPpS+V2gL2dHHFLzaK3fSKFR3dhi0sVqwJ4vuCG66ywsQtXmTT5YjyIfY9y63+rbOCYqS2+y/brE5iF/iw+cp9e5vXEq5RRnf8/AsgTr9JI5DnM0pAtFIQEOgoxGMVDjkxhsPNoDrdOxWU9dOl8HIJ2LylyFX4PhrlBFHY9hplJSeU8Xo4jLgKQCXBOC73bx59DSACM/B3xPfT6hZghT4bZACH8h+3XC7/EQpdrd1I9EdpMhJ6MKD+4EwNJ+/qwpOh/+T/AVFPIzD60YpM7RI4dYAD2E7/BxB3lE5GM4tPX+Rr2uWLajRFUjQoDXm5wlMF1tZDiWF2Yu2Q3nlEAsrYJqgg+MhF4x8M6F1JlR63gg0ZZMVtv1N1fl5jyG/agzc+3+VQT7DGy9LU2WnehegcDI4Dhrf1eUIGT+1vGeROAmCTrmcOCESFbMscjbDBTAXlsfqszWkEZqammZoUq27L1j4KWREcVl0lRWiEg9pacKH+1Yykt5WMUqP3JyRbFEE94tcjt4YGgIHK4HimjVG5qJEjsZI+3n37DGlMTVzqkdWGwGsb4ocSmbOe8AX5+/uw49R+RmxyogS+8vVOt/EBIDA3UpfF23R6Hjlz9/MU5yOQ+sjfbC6ysH/SeJPU7PdTZKJDKeBmzDxZ2I8hdeibNlP4dzuFqwVmVQGTcomKH2CIGj65cpPFsu49KQk+nGfalNyg4f1X8+/iPvqf1U69zco+hhH73rETJc3z4t/aPoX5dQUjbWZcPI30mdBrhnqb2c7xPvXuag80OgvOEHADw8xxFKLvTVQgCXhAoICL7JUd8BBrXlAKDHKNSQUPT/ur7x3eJEdwXsmtEprzwSVbbFQKJS5ZxHksM4I4OwAuOW4YtfX9UqZaBtUXmN3024D+9W4fLaFJHw2YN+a96SLJW6788tnd0yM7UiTBqDzsAI5T9imnQFKj5C0G+TOR5SRzWAzHeJSNStb9A3Sv0dtPtrKquD/OKFPQG5qxu3NhXWtjT6bNgahGU85/PA8sDQGsjdrTHdSpsHGc8hPx9142ozDtyIsv3MTxIpTkDCxq+LYFF1Hp1kBIq2izlMxCQRhEgMDMK" />
  <script type="text/javascript"> 
      //<![CDATA[
      if (window.WebForm_InitCallback) { 
        __theFormPostData = '';
        __theFormPostCollection = new Array();
        window.WebForm_InitCallback(); 
      }
      //]]>
    </script>
  </form>
</body>
</html>
