<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Privacy Statement - About - Catholic Online</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="author" content="Catholic Online">
		<meta name="section" content="About">
		<meta name="keywords" content="news, news online, breaking news, catholic, world news, US news, catholic news, daily newspaper, national, business, financial, arts, entertainment, movie reviews, catholic online, catholic church, film, forums, regional news, diocese news, home, family, politics, women, your money, books, newspaper archives, church, prayers, religion">
		<meta name="description" content="About Catholic Online.">
		<meta name="google-site-verification" content="PfV_R-97inKQ3E1bcck5YmDQoil_8k2tYvZEN_UYYkw">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({QJf3ax:[function(e,n){function t(e){function n(n,t,a){e&&e(n,t,a),a||(a={});for(var u=c(n),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,t);return s}function a(e,n){f[e]=c(e).concat(n)}function c(e){return f[e]||[]}function u(){return t(n)}var f={};return{on:a,emit:n,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=e("gos");n.exports=t()},{gos:"7eSDFh"}],ee:[function(e,n){n.exports=e("QJf3ax")},{}],3:[function(e,n){function t(e){return function(){r(e,[(new Date).getTime()].concat(i(arguments)))}}var r=e("handle"),o=e(1),i=e(2);"undefined"==typeof window.newrelic&&(newrelic=window.NREUM);var a=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit","noticeError"];o(a,function(e,n){window.NREUM[n]=t("api-"+n)}),n.exports=window.NREUM},{1:12,2:13,handle:"D5DuLP"}],gos:[function(e,n){n.exports=e("7eSDFh")},{}],"7eSDFh":[function(e,n){function t(e,n,t){if(r.call(e,n))return e[n];var o=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return e[n]=o,o}var r=Object.prototype.hasOwnProperty;n.exports=t},{}],D5DuLP:[function(e,n){function t(e,n,t){return r.listeners(e).length?r.emit(e,n,t):void(r.q&&(r.q[e]||(r.q[e]=[]),r.q[e].push(n)))}var r=e("ee").create();n.exports=t,t.ee=r,r.q={}},{ee:"QJf3ax"}],handle:[function(e,n){n.exports=e("D5DuLP")},{}],XL7HBI:[function(e,n){function t(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:i(e,o,function(){return r++})}var r=1,o="nr@id",i=e("gos");n.exports=t},{gos:"7eSDFh"}],id:[function(e,n){n.exports=e("XL7HBI")},{}],G9z0Bl:[function(e,n){function t(){var e=d.info=NREUM.info,n=f.getElementsByTagName("script")[0];if(e&&e.licenseKey&&e.applicationID&&n){c(p,function(n,t){n in e||(e[n]=t)});var t="https"===s.split(":")[0]||e.sslForHttp;d.proto=t?"https://":"http://",a("mark",["onload",i()]);var r=f.createElement("script");r.src=d.proto+e.agent,n.parentNode.insertBefore(r,n)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=e("handle"),c=e(1),u=window,f=u.document;e(2);var s=(""+location).split("?")[0],p={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-768.min.js"},d=n.exports={offset:i(),origin:s,features:{}};f.addEventListener?(f.addEventListener("DOMContentLoaded",o,!1),u.addEventListener("load",t,!1)):(f.attachEvent("onreadystatechange",r),u.attachEvent("onload",t)),a("mark",["firstbyte",i()])},{1:12,2:3,handle:"D5DuLP"}],loader:[function(e,n){n.exports=e("G9z0Bl")},{}],12:[function(e,n){function t(e,n){var t=[],o="",i=0;for(o in e)r.call(e,o)&&(t[i]=n(o,e[o]),i+=1);return t}var r=Object.prototype.hasOwnProperty;n.exports=t},{}],13:[function(e,n){function t(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(0>o?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=t},{}]},{},["G9z0Bl"]);</script>
		<!-- CSS -->
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css">
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css">
		<link rel="stylesheet" href="/assets/css/screenV3.css">
		<!-- JS -->
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="/assets/js/respond.min.js"></script>
		<![endif]-->
		<script src="//code.jquery.com/jquery.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
		<!-- Fav and touch icons -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/assets/img/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/assets/img/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/assets/img/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="/assets/img/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="/assets/img/favicon.ico">
		<!-- RSS -->
		<link rel="search" type="application/opensearchdescription+xml" href="http://www.catholic.org/xml/opensearch_desc.xml" title="Catholic Online" />
		<link rel="alternate" type="application/rss+xml" title="Top News" href="http://www.catholic.org/xml/rss_top_news.xml">
		<link rel="alternate" type="application/rss+xml" title="Daily Readings" href="http://www.catholic.org/xml/rss_readings.xml">
		<link rel="alternate" type="application/rss+xml" title="Politics" href="http://www.catholic.org/xml/rss_politics.xml">
		<link rel="alternate" type="application/rss+xml" title="Most Popular Videos" href="http://www.catholic.org/xml/rss_video_most_popular.xml">
		<link rel="alternate" type="application/rss+xml" title="Recently Added Videos" href="http://www.catholic.org/xml/rss_video_recently_added.xml">
		<link rel="alternate" type="application/rss+xml" title="International" href="http://www.catholic.org/xml/rss_international.xml">
		<link rel="alternate" type="application/rss+xml" title="Africa" href="http://www.catholic.org/xml/rss_africa.xml">
		<link rel="alternate" type="application/rss+xml" title="Americas" href="http://www.catholic.org/xml/rss_americas.xml">
		<link rel="alternate" type="application/rss+xml" title="Asia Pacific" href="http://www.catholic.org/xml/rss_asia_pacific.xml">
		<link rel="alternate" type="application/rss+xml" title="Europe" href="http://www.catholic.org/xml/rss_europe.xml">
		<link rel="alternate" type="application/rss+xml" title="Middle East" href="http://www.catholic.org/xml/rss_middle_east.xml">
		<link rel="alternate" type="application/rss+xml" title="U.S." href="http://www.catholic.org/xml/rss_national.xml">
		<link rel="alternate" type="application/rss+xml" title="Diocese" href="http://www.catholic.org/xml/rss_diocese.xml">
		<link rel="alternate" type="application/rss+xml" title="Catholic PRWire" href="http://www.catholic.org/xml/rss_prwire.xml">
		<link rel="alternate" type="application/rss+xml" title="Entertainment" href="http://www.catholic.org/xml/rss_ae.xml">
		<link rel="alternate" type="application/rss+xml" title="Movies" href="http://www.catholic.org/xml/rss_ae_movies.xml">
		<link rel="alternate" type="application/rss+xml" title="Books" href="http://www.catholic.org/xml/rss_ae_books.xml">
		<link rel="alternate" type="application/rss+xml" title="TV" href="http://www.catholic.org/xml/rss_ae_tv.xml">
		<link rel="alternate" type="application/rss+xml" title="Featured Today" href="http://www.catholic.org/xml/rss_featured.xml">
		<link rel="alternate" type="application/rss+xml" title="Featured Authors" href="http://www.catholic.org/xml/rss_featured_authors.xml">
		<link rel="alternate" type="application/rss+xml" title="Featured Articles" href="http://www.catholic.org/xml/rss_featured_articles.xml">
		<link rel="alternate" type="application/rss+xml" title="Women in the 3rd Millennium" href="http://www.catholic.org/xml/rss_featured_women.xml">
		<link rel="alternate" type="application/rss+xml" title="Opinion/Editorials" href="http://www.catholic.org/xml/rss_featured_opinions.xml">
		<link rel="alternate" type="application/rss+xml" title="Business & Economics" href="http://www.catholic.org/xml/rss_finance.xml">
		<link rel="alternate" type="application/rss+xml" title="Saint of the Day" href="http://www.catholic.org/xml/rss_sofd.xml">
		<link rel="alternate" type="application/rss+xml" title="Home & Family" href="http://www.catholic.org/xml/rss_hf.xml">
		<link rel="alternate" type="application/rss+xml" title="Living Faith" href="http://www.catholic.org/xml/rss_hf_faith.xml">
		<link rel="alternate" type="application/rss+xml" title="Home & Food" href="http://www.catholic.org/xml/rss_hf_home.xml">
		<link rel="alternate" type="application/rss+xml" title="Family" href="http://www.catholic.org/xml/rss_hf_family.xml">
		<link rel="alternate" type="application/rss+xml" title="Love" href="http://www.catholic.org/xml/rss_hf_marriage.xml">
		<link rel="alternate" type="application/rss+xml" title="Health" href="http://www.catholic.org/xml/rss_hf_health.xml">
		<!-- New site is live -->
		<!--  -->
		<!-- Open Graph -->
		<meta property="og:title" content="Privacy Statement - About - Catholic Online"/>
		<meta property="og:image" content="http://www.catholic.org/images/fb-share-image.jpg"/>
		<!-- Google +1 -->
		<meta itemprop="name" content="Privacy Statement - About - Catholic Online"/>
		<meta itemprop="image" content="http://www.catholic.org/images/fb-share-image.jpg"/>
		<!-- /Meta Head -->
		<!-- Head Ads -->
		<!-- /Head Ads -->
		<!-- Head Script -->
		<!-- /Head Script -->
		<!-- ros -->
		<!-- /about -->
		<!-- /about -->
		<!--  -->
	</head>
	<body>
		<a name="top" href="#content-page" class="sr-only">Skip to content</a>
		<div class="top-padding hidden-print"></div>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container" id="container-nav">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand visible-xs" href="/"><img src="/assets/img/logo-header-nosub.png" border="0" alt="Catholic Online Logo" title="Catholic Online: Inform - Inspire - Ignite"/></a>
			<a class="navbar-brand hidden-xs navbar-brand-lg" href="/"><img src="/assets/img/logo-header.png" border="0" alt="Catholic Online Logo" title="Catholic Online: Inform - Inspire - Ignite"/></a>
		</div>
		<div class="collapse navbar-collapse">
			<div class="row">
				<div class="col-sm-4 col-md-5 col-lg-6">
					<form class="navbar-form" role="search" action="/search/" method="get">
						<div class="input-group col-xs-12">
							<label class="sr-only" for="search">Search</label>
							<input type="text" name="q" class="form-control" placeholder="Search">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-default"><i class="icon-search"></i></button>
							</span>
						</div>
					</form>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-2 hidden-xs">
					<ul class="nav navbar-nav navbar-right">
						<li class="visible-sm"><a href="http://catholiconline.email/">Mail</a></li>
						<li class="hidden-sm"><a href="http://catholiconline.email/" class="text-danger"><i class="icon-fixed-width icon-envelope"></i> Mail</a></li>
						<li class="hidden-sm"><a href="http://catholicshopping.com/pages/catholic-onlines-new-email" class="text-danger">Sign Up</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="nav navbar-nav">
						<li class="hidden-sm nav-first"><a href="http://catholicshopping.com/"><i class="icon-shopping-cart"></i> Shopping</a></li>
						<li class="visible-sm nav-first"><a href="http://catholicshopping.com/">Shop</a></li>
						<li><a href="/saints/">Saints</a></li>
						<li><a href="/prayers/">Prayers</a></li>
						<li><a href="/bible/">Bible</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">News <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/news/blog/">Blog</a></li>
								<li><a href="/news/business/">Business & Economics</a></li>
								<li><a href="/college/">College & University</a></li>
								<li><a href="/news/ae/">Entertainment </a></li>
								<li><a href="/news/green/">Green</a></li>
								<li><a href="/news/health/">Health & Wellness</a></li>
								<li><a href="/news/hf/">Home &amp; Family</a></li>
								<li><a href="/news/international/">International</a></li>
								<li><a href="/prwire/">PRWire &amp; Events</a></li>
								<li><a href="/news/politics/">Politics & Policy</a></li>
								<li><a href="/news/sports/">Sports</a></li>
								<li><a href="/news/technology/">Technology</a></li>
								<li><a href="/news/national/">U.S. News</a></li>
								<li><a href="http://studio.catholic.org">Video News</a></li>
								<li><a href="/vocations/">Vocations </a></li>
								<li class="divider"></li>
								<li><a href="/contributors/">Contributors</a></li>
								<li><a href="/mostpopular.php">Most Popular</a></li>
							</ul>
						</li>
						<li class="dropdown hidden-sm">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Daily <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/bible/daily_reading/">Daily Readings</a></li>
								<li><a href="/homily/">Homily</a></li>
								<li><a href="/newsletters/">Newsletters</a></li>
								<li><a href="/saints/sofd.php">Saint of the Day </a></li>
								<li><a href="/prayers/station.php">Stations of the Cross</a></li>
							</ul>
						</li>
						<li class="hidden-sm"><a href="/encyclopedia/">Ency</a></li>
						<li><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "358929" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="516f12d5c6939" name="516f12d5c6939" src="http://ox-d.catholic.org/w/1.0/afr?auid=358929&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="1" height="1"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=516f12d5c6939&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=358929&cs=516f12d5c6939&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></li>
						<li><a href="http://ycvf.org/?utm_source=Catholic%20Online&utm_medium=menu%20tab&utm_campaign=give">GIVE</a></li>
						<li><a href="/clife/lent/ashwed.php">Ash Wed.</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">More <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="/clife/">Catholic Life / Resources</a></li>
								<li><a href="/clife/advent/">Christmas / Advent</a></li>
								<li><a href="/clife/lent/">Easter / Lent </a></li>
								<li><a href="/everything/">Everything </a></li>
								<li><a href="/clife/jesus/">Jesus Christ </a></li>
								<li><a href="/clife/mary/">Mary, Mother of God </a></li>
								<li><a href="/clife/teresa/">Mother Teresa</a></li>
								<li><a href="/maps/">Parish Locator</a></li>
								<li><a href="/pope/">Popes and Bishops</a></li>
								<li><a href="/travel/">Travel & Leisure</a></li>
								<li><a href="/video/">Video</a></li>
								<li><a href="/about/">About Us</a></li>
								<li><a href="/services/advertising/">Advertisers </a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div><!-- /.nav-collapse -->
	</div><!-- /#container-nav -->
</nav>
<div class="visible-print" id="page-title-print">
	<img src="/assets/img/logo-print.png" border="0" alt="Catholic Online Logo" title="Catholic Online: Inform - Inspire - Ignite"/>
</div>
		<!-- #content-page -->
		<div id="content-page" class="container">
			<!-- Ad: 728x90 -->
			<div class="row hidden-print">
				<div class="col-md-12 spacer">
<div id="ad_ros728" class="ad_728">
	<center><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this 

tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ 

-->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "358934" });
</script>
<script 

type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="515d12ab55e65" name="515d12ab55e65" src="http://ox-d.catholic.org/w/1.0/afr?auid=358934&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="970" height="90"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=515d12ab55e65&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=358934&cs=515d12ab55e65&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></center>
</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li><li><a href='/about/'>About</a></li><li class='active'>Privacy Statement</li>					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h1>Privacy Statement</h1>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8" id="page-content">
<div id="pageMenu">
	<div class="col-sm-offset-1 col-sm-5 col-xs-12 pull-right">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Learn More</h3>
			</div>
			<ol type="I" class="list-group">
				<li class="list-group-item"><a href="/about/privacy.php">Privacy Statement</a></li>
				<li class="list-group-item"><a href="/about/tos.php">Terms of Service</a></li>
				<li class="list-group-item"><a href="/about/spam.php">Universal Spam Policy</a></li>
			</ol>
		</div>
	</div>
</div>
<em>Catholic Online, LLC takes your privacy seriously. Please read the following to learn more about our privacy policy.</em><br /><br />
The federal government and technology industry have developed <a href="http://www.onguardonline.gov/">practical tips</a> to help you guard against Internet fraud, secure your computer and protect your personal information. <br /><br />
<em>How Catholic Online, LLC Uses Your Personal Information</em><br /><br />
This policy covers how Catholic Online, LLC treats personal information that Catholic Online, LLC collects and receives, including information related to your past use of Catholic Online, LLC products and services. Personal information is information about you that is personally identifiable like your name, address, email address, or phone number, and that is not otherwise publicly available.<br /><br />
<em>This privacy policy only applies to Catholic Online, LLC</em><br /><br />
This policy does not apply to the practices of companies that Catholic Online, LLC does not own or control, or to people that Catholic Online, LLC does not employ or manage. <br /><br />
<em>INFORMATION COLLECTION AND USE</em><br /><br />
<em>General</em><br /><br />
Catholic Online, LLC collects personal information when you register with Catholic Online, LLC, when you use Catholic Online, LLC products or services, when you visit Catholic Online, LLC pages or the pages of certain Catholic Online, LLC partners, and when you enter promotions or sweepstakes. Catholic Online, LLC may combine information about you that we have with information we obtain from business partners or other companies. <br /><br />
When you register we ask for information such as your name, email address, birth date, gender, ZIP code, occupation, industry, and personal interests. For some financial products and services we might also ask for your address, Social Security number, and information about your assets. When you register with Catholic Online, LLC and sign in to our services, you are not anonymous to us. <br /><br />
Catholic Online, LLC collects information about your transactions with us and with some of our business partners, including information about your use of financial products and services that we offer.<br /><br />
Catholic Online, LLC automatically receives and records information from your computer and browser, including your IP address, Catholic Online, LLC cookie information, software and hardware attributes, and the page you request.<br /><br />
Catholic Online, LLC uses information for the following general purposes: to customize the advertising and content you see, fulfill your requests for products and services, improve our services, contact you, conduct research, and provide anonymous reporting for internal and external clients. <br /><br />
<em>Children</em><br /><br />
When a child under age 13 attempts to register with Catholic Online, LLC, we ask the child to have a parent or guardian obtain parental permission. <br /><br />
Catholic Online, LLC does not contact children under age 13 about special offers or for marketing purposes without a parent's permission. <br /><br />
Catholic Online, LLC does not ask a child under age 13 for more personal information, as a condition of participation, than is reasonably necessary to participate in a given activity or promotion.<br /><br />
<em>INFORMATION SHARING AND DISCLOSURE</em><br /><br />
Catholic Online, LLC does not rent, sell, or share personal information about you with other people or non-affiliated companies except to provide products or services you've requested, when we have your permission, or under the following circumstances:
<br /><br />
<ul>
<li>We provide the information to trusted partners who work on behalf of or with Catholic Online, LLC under confidentiality agreements. These companies may use your personal information to help Catholic Online, LLC communicate with you about offers from Catholic Online, LLC and our marketing partners. However, these companies do not have any independent right to share this information.</li><br />
<li>We have a parent's permission to share the information if the user is a child under age 13. Parents have the option of allowing Catholic Online, LLC to collect and use their child's information without consenting to Catholic Online, LLC sharing of this information with people and companies who may use this information for their own purposes.</li><br />
<li>We respond to subpoenas, court orders, or legal process, or to establish or exercise our legal rights or defend against legal claims.</li><br />
<li>We believe it is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of Catholic Online, LLC's terms of use, or as otherwise required by law. </li><br />
<li>We transfer information about you if Catholic Online, LLC is acquired by or merged with another company. In this event, Catholic Online, LLC will notify you before information about you is transferred and becomes subject to a different privacy policy.</li>
</ul>
<br /><br />
Catholic Online, LLC displays targeted advertisements based on personal information. Advertisers (including ad serving companies) may assume that people who interact with, view, or click targeted ads meet the targeting criteria--for example, men ages 18-24 from a particular geographic area. 
<br /><br /><br />
<ul>
<li>Catholic Online, LLC does not provide any personal information to the advertiser when you interact with or view a targeted ad. However, by interacting with or viewing an ad you are consenting to the possibility that the advertiser will make the assumption that you meet the targeting criteria used to display the ad. </li><br />
<li>Catholic Online, LLC advertisers include financial service providers (such as banks, insurance agents, stock brokers and mortgage lenders) and non-financial companies (such as stores, airlines, and software companies).</li>
</ul>
<br /><br />
Catholic Online, LLC works with vendors, partners, advertisers, and other service providers in different industries and categories of business.<br /><br />
<em>COOKIES</em><br /><br />
Catholic Online, LLC may set and access Catholic Online, LLC cookies on your computer.<br /><br />
Catholic Online, LLC lets other companies that show advertisements on some of our pages set and access their cookies on your computer. Other companies' use of their cookies is subject to their own privacy policies, not this one. Advertisers or other companies do not have access to Catholic Online, LLC's cookies.<br /><br />
Catholic Online, LLC uses web beacons to access Catholic Online, LLC cookies inside and outside our network of web sites and in connection with Catholic Online, LLC products and services.<br /><br />
<em>Your Ability to Edit and Delete Your Account Information and Preferences</em><br /><br />
<em>General</em><br /><br />
You can edit your Catholic Online, LLC Account Information, including your marketing preferences, at any time. <br /><br />
New categories of marketing communications might be added to the Marketing Preferences page from time to time. Users who visit this page can opt out of receiving future marketing communications from these new categories or they can unsubscribe by following instructions contained in the messages they receive. <br /><br />
We reserve the right to send you certain communications relating to the Catholic Online, LLC service, such as service announcements, administrative messages and the Catholic Online, LLC Newsletters that are considered part of your Catholic Online, LLC account, without offering you the opportunity to opt out of receiving them. <br /><br />
You can delete your Catholic Online, LLC account by visiting our Account Deletion page. Please click here to read about information that might possibly remain in our archived records after your account has been deleted. <br /><br />
<em>Children</em><br /><br />
Parents can review, edit, and delete information relating to their child's Catholic Online, LLC account. . <br /><br />
<em>CONFIDENTIALITY AND SECURITY</em><br /><br />
We limit access to personal information about you to employees who we believe reasonably need to come into contact with that information to provide products or services to you or in order to do their jobs. <br /><br />
We have physical, electronic, and procedural safeguards that comply with federal regulations to protect personal information about you. <br /><br />
<em>CHANGES TO THIS PRIVACY POLICY</em><br /><br />
Catholic Online, LLC may update this policy. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your Catholic Online, LLC account or by placing a prominent notice on our site. <br /><br />
<em>QUESTIONS AND SUGGESTIONS</em><br /><br />
If you have questions or suggestions, please contact us at:<br /><br />
Catholic Online, LLC<br />
Privacy Policy Issues<br />
P.O. Box 11236<br />
Bakersfield, CA 93389<br /><br />
Effective Date: October 13, 2009<div class="spacer-large"></div>
	<div id="ad_google_content336" class="ad_336 hidden-print">
<script type="text/javascript"><!--
google_ad_client = "ca-pub-2293526441634301";
/* Text: 336x280: Content */
google_ad_slot = "3514617354";
google_ad_width = 336;
google_ad_height = 280;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>	</div>
<div class="spacer-large"></div>
				</div>
				<div class="col-md-4 hidden-print" id="page-sidebar">
					<!-- Sidebar: about -->
<div class="text-center">
<div id="ad_ros300" class="ad_300">
	<center><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "358947" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="515f690f6f4b2" name="515f690f6f4b2" src="http://ox-d.catholic.org/w/1.0/afr?auid=358947&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="300" height="1050"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=515f690f6f4b2&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=358947&cs=515f690f6f4b2&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></center>
</div>
</div>
<div class="clearfix"></div>
<div class="sideBarWrapper">
	<h3 class="uppercase"><a href="/newsletters/">Newsletters</a></h3>
	<div class="media">
		<a href="#" class="pull-left"><img src="/images/newsletters/newspaper-icon.jpg" border="0" alt="Newsletter Sign Up icon" title="Newsletter Sign Up" class="media-object"/></a>
		<div class="media-body">
			<form name="newsletterForm" method="post" action="/newsletters/signUp.php" role="form">
				<input type="checkbox" name="list[]" value="Special Offers" checked style="display:none;" />
				<div class="input-group">
					<p>Stay up to date with the latest news, information, and special offers</p>
					<label class="sr-only" for="newslettersSignUp">Newsletter</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="icon-envelope-alt"></i></span>
						<input type="text" id="newsletter-email" name="email" value="" tabindex="1" class="form-control" placeholder="your@email.com" />
					</div>
					<input type="submit" name="Subscribe" value="Subscribe Now" tabindex="1" class="btn btn-default"/>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="clearfix spacer-large"></div>
<a href="https://www.youtube.com/channel/UCoNabUSDcUb9iuSr256mLMg/?sub_confirmation=1" target="_blank"><img src="http://www.catholic.org/files/images/media/14439129265.png" alt="Subscribe to Catholic OnlineYouTube Channel" title="Subscribe to Catholic Online YouTube Channel" class="img-responsive"></a>
<div class="sideBarWrapper">
	<h3><span class="red-color">the FEED</span><br><small><small>by Catholic Online</small></small></h3>
	<ul class="none">
		<li><a href="/saints/sofd.php?month=February&day=7" class="none">St. Moses: Saint of the Day for Sunday, February 07, 2016</a></li>
		<li><a href="/bible/daily_reading/?select_date=2016-02-07" class="none">Daily Readings for Sunday, February 07, 2016</a></li>
		<li><a href="/news/ae/music/story.php?id=67101" class="none">Earth, Wind & Fire founder Maurice white dies at 74</a></li>
		<li><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "538139158" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="0f888540c8" name="0f888540c8" src="http://ox-d.catholic.org/w/1.0/afr?auid=538139158&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="1" height="1"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=0f888540c8&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=538139158&cs=0f888540c8&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></li>
		<li><a href="/news/national/story.php?id=67103" class="none">Lower Your Nets For a Catch!  We Are All Called to Evangelize</a></li>
		<li><a href="/news/hf/faith/story.php?id=66837" class="none">MAKE YOURSELF COUNT! Complete this quick Ash Wednesday survey</a></li>
		<li><a href="https://www.youtube.com/watch?v=QIkdIj-0TD8" class="none">Daily Reading for Monday, February 8th, 2016 HD Video</a></li>
		<li><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "538151622" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="0bb07a785c" name="0bb07a785c" src="http://ox-d.catholic.org/w/1.0/afr?auid=538151622&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="1" height="1"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=0bb07a785c&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=538151622&cs=0bb07a785c&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></li>
		<li><a href="https://www.youtube.com/watch?v=XN1SqznKl7o" class="none">Daily Reading for Sunday, February 7th, 2016 HD Video</a></li>
		<li><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "538151623" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="1d12276733" name="1d12276733" src="http://ox-d.catholic.org/w/1.0/afr?auid=538151623&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="1" height="1"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=1d12276733&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=538151623&cs=1d12276733&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></li>
		<li><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "538151715" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="992c609ecd" name="992c609ecd" src="http://ox-d.catholic.org/w/1.0/afr?auid=538151715&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="1" height="1"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=992c609ecd&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=538151715&cs=992c609ecd&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></li>
	</ul>
</div>
<div class="clearfix spacer"></div>
<div class="sideBarWrapper">
	<h3 class="uppercase"><a href="/mostpopular.php">Most Popular</a></h3>
	<div class="media">
		<a href="/news/hf/faith/story.php?id=59123" class="pull-left"><img src="/files/images/ins_news/2015035742pope-t.jpg" alt="Click to go to this article" title="Click to go to this article" width="78" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="/news/hf/faith/story.php?id=59123" title="Click to go to this article">Pope Francis wants to change two major Catholic laws he sees as ...</a> <a href="/news/hf/faith/story.php?id=59123" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
	<div class="media">
		<a href="/news/international/europe/story.php?id=67099" class="pull-left"><img src="/files/images/ins_news/2016023354glasstn.jpg" alt="Click to go to this article" title="Click to go to this article" width="78" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="/news/international/europe/story.php?id=67099" title="Click to go to this article">Remains of St. Padre Pio of Pietrelcina, St. Leopold Mandic arrive in ...</a> <a href="/news/international/europe/story.php?id=67099" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
	<div class="media">
		<a href="/news/hf/faith/story.php?id=57689" class="pull-left"><img src="/files/images/ins_news/small_2014112019.jpg" alt="Click to go to this article" title="Click to go to this article" width="78" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="/news/hf/faith/story.php?id=57689" title="Click to go to this article">Here are 10 Very Interesting Facts About the Catholic Church You ...</a> <a href="/news/hf/faith/story.php?id=57689" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
	<div class="media">
		<a href="/news/hf/faith/story.php?id=60158" class="pull-left"><img src="/files/images/ins_news/small_2015055739.jpg" alt="Click to go to this article" title="Click to go to this article" width="78" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="/news/hf/faith/story.php?id=60158" title="Click to go to this article">3 things Christians must recognize about anxiety and depression</a> <a href="/news/hf/faith/story.php?id=60158" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
	<div class="media">
		<a href="/news/hf/faith/story.php?id=67049" class="pull-left"><img src="/files/images/ins_news/2016023850pope_francis_did_not_speak_of_religion_or_freedom_when_praising_china_as_a_progressive_country_ii.jpg" alt="Click to go to this article" title="Click to go to this article" width="78" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="/news/hf/faith/story.php?id=67049" title="Click to go to this article">Sparks fly over Pope Francis' interview with Asia Times</a> <a href="/news/hf/faith/story.php?id=67049" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
</div>
<div class="spacer-large"></div>
<div class="clearfix"></div>
<div class="spacer-large"></div>
<div class="text-center ad_300" id="ad-wam-sidebar">
<div id="ad_wam_sidebar0" class="ad_0">
	<center><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "488342" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="524f2bdde1a98" name="524f2bdde1a98" src="http://ox-d.catholic.org/w/1.0/afr?auid=488342&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="300" height="450"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=524f2bdde1a98&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=488342&cs=524f2bdde1a98&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></center>
</div>
</div>
<div class="sideBarWrapper">
	<h3 class="uppercase"><a href="/bible/daily_reading/">Daily Readings</a></h3>
	<div class="media">
		<div class="media-body">
			<p><a href="/bible/daily_reading/" class="uppercase">Reading 1, <em>Isaiah 6:1-2, 3-8</em></a><br/>
			<sup>1</sup> In the year of King Uzziah's death I saw the Lord seated on a high and ... <a href="/bible/daily_reading/" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
	<div class="media">
		<div class="media-body">
			<p><a href="/bible/daily_reading/" class="uppercase">Psalm, <em>Psalms 138:1-2, 2-3, 4-5, 7-8</em></a><br/>
			<sup>1</sup> [Of David] I thank you, Yahweh, with all my heart, for you have ... <a href="/bible/daily_reading/" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
	<div class="media">
		<div class="media-body">
			<p><a href="/bible/daily_reading/" class="uppercase">Gospel, <em>Luke 5:1-11</em></a><br/>
			<sup>1</sup> Now it happened that he was standing one day by the Lake of Gennesaret, ... <a href="/bible/daily_reading/" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
	<div class="media">
		<div class="media-body">
			<p><a href="/bible/daily_reading/" class="uppercase">Reading 2, <em>First Corinthians 15:1-11</em></a><br/>
			<sup>1</sup> I want to make quite clear to you, brothers, what the message of the ... <a href="/bible/daily_reading/" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
</div>
<div class="sideBarWrapper">
	<h3 class="uppercase"><a href="/search/">Top Searches</a></h3>
	<ul class="list-inline">
		<li><a href="/search/?q=apostles+creed">apostles creed</a></li>
		<li><a href="/search/?q=luke">luke</a></li>
		<li><a href="/search/?q=john">john</a></li>
		<li><a href="/search/?q=st+jude">st jude</a></li>
		<li><a href="/search/?q=saint+joseph">saint joseph</a></li>
		<li><a href="/search/?q=st+anthony">st anthony</a></li>
		<li><a href="/search/?q=Michael">Michael</a></li>
		<li><a href="/search/?q=padre+pio">padre pio</a></li>
		<li><a href="/search/?q=saint+anthony">saint anthony</a></li>
		<li><a href="/search/?q=patrick">patrick</a></li>
		<li><a href="/search/?q=saints+names">saints names</a></li>
		<li><a href="/search/?q=Hail+Mary">Hail Mary</a></li>
		<li><a href="/search/?q=Deacon+Keith+Fournier">Deacon Keith Fournier</a></li>
		<li><a href="/search/?q=angels">angels</a></li>
		<li><a href="/search/?q=Advent">Advent</a></li>
		<li><a href="/search/?q=francis">francis</a></li>
		<li><a href="/search/?q=st+peter">st peter</a></li>
		<li><a href="/search/?q=saint+patrick">saint patrick</a></li>
		<li><a href="/search/?q=st+francis">st francis</a></li>
		<li><a href="/search/?q=saint+peter">saint peter</a></li>
		<li><a href="/search/?q=grace">grace</a></li>
		<li><a href="/search/?q=saint+grace">saint grace</a></li>
		<li><a href="/search/?q=saint+mary">saint mary</a></li>
		<li><a href="/search/?q=confirmation+names">confirmation names</a></li>
		<li><a href="/search/?q=Our+father">Our father</a></li>
	</ul>
</div>
<div class="spacer-xlarge text-center">
<div id="ad_btf300" class="ad_300">
	<center><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "358937" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="515f6f7a2643b" name="515f6f7a2643b" src="http://ox-d.catholic.org/w/1.0/afr?auid=358937&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="300" height="1050"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=515f6f7a2643b&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=358937&cs=515f6f7a2643b&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></center>
</div>
</div>
<div class="sideBarWrapper">
	<h3 class="uppercase"><a href="/saints/sofd.php">Saint of the Day</a></h3>
	<div class="media">
		<a href="/saints/saint.php?saint_id=5171" class="pull-left"><img src="/files/images/saints/thumb_5171.jpg" alt="Saint of the Day for February 7th, 2016 Image" title="Saint of the Day for February 7th, 2016" width="70" class="media-object img-circle"/></a>
		<div class="media-body">
			<p><a href="/saints/saint.php?saint_id=5171" class="uppercase">St. Moses</a><br/> February 7: Arab hermit and bishop who is called "the Apostle ... <a href="/saints/saint.php?saint_id=5171" class="red">Read&nbsp;More</a></p>
		</div>
	</div>
</div>
<div class="sideBarWrapper">
	<h3 class="uppercase"><a href="/video/">Latest Videos</a></h3>
	<div class="media">
		<a href="https://www.youtube.com/watch?v=QIkdIj-0TD8" class="pull-left"><img src="https://i.ytimg.com/vi/QIkdIj-0TD8/default.jpg" width="70" alt="Image of Daily Reading for Monday, February 8th, 2016 HD video" title="Daily Reading for Monday, February 8th, 2016 HD video" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="https://www.youtube.com/watch?v=QIkdIj-0TD8">Daily Reading for Monday, February 8th, 2016 HD</a> <a href="https://www.youtube.com/watch?v=QIkdIj-0TD8" class="red">View&nbsp;Video</a></p>
		</div>
	</div>
	<div class="media">
		<a href="https://www.youtube.com/watch?v=XN1SqznKl7o" class="pull-left"><img src="https://i.ytimg.com/vi/XN1SqznKl7o/default.jpg" width="70" alt="Image of Daily Reading for Sunday, February 7th, 2016 HD video" title="Daily Reading for Sunday, February 7th, 2016 HD video" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="https://www.youtube.com/watch?v=XN1SqznKl7o">Daily Reading for Sunday, February 7th, 2016 HD</a> <a href="https://www.youtube.com/watch?v=XN1SqznKl7o" class="red">View&nbsp;Video</a></p>
		</div>
	</div>
	<div class="media">
		<a href="https://www.youtube.com/watch?v=ihvyIWePl1I" class="pull-left"><img src="https://i.ytimg.com/vi/ihvyIWePl1I/default.jpg" width="70" alt="Image of Stations of the Cross - Third Station: Jesus falls the first time HD video" title="Stations of the Cross - Third Station: Jesus falls the first time HD video" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="https://www.youtube.com/watch?v=ihvyIWePl1I">Stations of the Cross - Third Station: Jesus falls the first time HD</a> <a href="https://www.youtube.com/watch?v=ihvyIWePl1I" class="red">View&nbsp;Video</a></p>
		</div>
	</div>
	<div class="media">
		<a href="https://www.youtube.com/watch?v=oCCoePjOQjI" class="pull-left"><img src="https://i.ytimg.com/vi/oCCoePjOQjI/default.jpg" width="70" alt="Image of Stations of the Cross - Second Station: Jesus carries His cross HD video" title="Stations of the Cross - Second Station: Jesus carries His cross HD video" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="https://www.youtube.com/watch?v=oCCoePjOQjI">Stations of the Cross - Second Station: Jesus carries His cross HD</a> <a href="https://www.youtube.com/watch?v=oCCoePjOQjI" class="red">View&nbsp;Video</a></p>
		</div>
	</div>
	<div class="media">
		<a href="https://www.youtube.com/watch?v=Swutrn5ZzbY" class="pull-left"><img src="https://i.ytimg.com/vi/Swutrn5ZzbY/default.jpg" width="70" alt="Image of Daily Reading for Saturday, February 6th, 2016 HD video" title="Daily Reading for Saturday, February 6th, 2016 HD video" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="https://www.youtube.com/watch?v=Swutrn5ZzbY">Daily Reading for Saturday, February 6th, 2016 HD</a> <a href="https://www.youtube.com/watch?v=Swutrn5ZzbY" class="red">View&nbsp;Video</a></p>
		</div>
	</div>
	<p class="pull-right"><a href="/video/" class="red">View All Videos</a></p>
</div>
<div class="spacer-xlarge text-center">
<div id="ad_google_sidebar300" class="ad_300">
	<center><script type="text/javascript"><!--
google_ad_client = "ca-pub-2293526441634301";
/* Text: 300x250: Sidebar */
google_ad_slot = "4134152154";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></center>
</div>
</div>
<div class="sideBarWrapper">
	<h3 class="uppercase"><a href="http://catholicshopping.com/">Marketplace</a></h3>
	<div class="media">
		<a href="http://catholicshopping.com/products/mysteries-of-the-rosary-folder-150-040" class="pull-left"><img src="https://cdn.shopify.com/s/files/1/0223/1885/products/150-040_compact.jpeg?v=1402701494" alt="Learn More!" title="Learn More!" width="70" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="http://catholicshopping.com/products/mysteries-of-the-rosary-folder-150-040">Mysteries Of The Rosary Folder</a><br><small>$10.99</small></p>
		</div>
	</div>
	<div class="media">
		<a href="http://catholicshopping.com/products/prayers-our-father" class="pull-left"><img src="https://cdn.shopify.com/s/files/1/0223/1885/products/Screen_Shot_2015-06-26_at_11.18.05_AM_compact.png?v=1435342828" alt="Learn More!" title="Learn More!" width="70" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="http://catholicshopping.com/products/prayers-our-father">Prayers - Our Father</a><br><small>$0.00</small></p>
		</div>
	</div>
	<div class="media">
		<a href="http://catholicshopping.com/products/world-meeting-of-families-prayer-card-english" class="pull-left"><img src="https://cdn.shopify.com/s/files/1/0223/1885/products/english_compact.png?v=1439308321" alt="Learn More!" title="Learn More!" width="70" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="http://catholicshopping.com/products/world-meeting-of-families-prayer-card-english">World Meeting of Families Prayer Card - English</a><br><small>$0.00</small></p>
		</div>
	</div>
	<div class="media">
		<a href="http://catholicshopping.com/products/medals-10681" class="pull-left"><img src="https://cdn.shopify.com/s/files/1/0223/1885/products/9076SS_18SS_compact.jpeg?v=1389748806" alt="Learn More!" title="Learn More!" width="70" class="media-object img-thumbnail"/></a>
		<div class="media-body">
			<p><a href="http://catholicshopping.com/products/medals-10681">St. Michael the Archangel Pendant (Sterling Silver)</a><br><small>$43.25</small></p>
		</div>
	</div>
</div>
<div class="spacer-xlarge text-center">
<div id="ad_ros160" class="ad_160">
	<center><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "358927" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="5154d1f3a0db6" name="5154d1f3a0db6" src="http://ox-d.catholic.org/w/1.0/afr?auid=358927&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="160" height="600"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=5154d1f3a0db6&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=358927&cs=5154d1f3a0db6&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></center>
</div>
</div>
				</div>
			</div>
			<div class="spacer-large"></div>
			<div class="row hidden-print">
				<div class="col-md-12" id="page-editors-choice">
					<div id="carousel-ec" class="carousel slide carousel-ec spacer-large">
	<h2 class="text-center">Editor's Choice</h2>
	<hr>
	<!-- Carousel items -->
	<div class="carousel-inner">
		<div class="active item">
			<div class="row">
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Soldiers fleeing</h5>
						<a href="http://www.catholic.org/news/international/africa/story.php?id=67097"><img src="/files/images/editors_choice/2016024027cars.jpg" alt="Soldiers fleeing" title="Soldiers fleeing" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/international/africa/story.php?id=67097">ISIS fighters quickly vacating Syria - why?</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<h5>Advertisement</h5>
					<div id="ad_ec_slider_two300" class="ad_300">
<!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.catholic.org/...'
 * to
 * 'https://ox-d.catholic.org/...'
 */ -->

<script type="text/javascript">
if (!window.OX_ads) { OX_ads = []; }
OX_ads.push({ "auid" : "350487" });
</script>
<script type="text/javascript">
document.write('<scr'+'ipt src="http://ox-d.catholic.org/w/1.0/jstag"><\/scr'+'ipt>');
</script>
<noscript><iframe id="5139358fc8bde" name="5139358fc8bde" src="http://ox-d.catholic.org/w/1.0/afr?auid=350487&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="300" height="250"><a href="http://ox-d.catholic.org/w/1.0/rc?cs=5139358fc8bde&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.catholic.org/w/1.0/ai?auid=350487&cs=5139358fc8bde&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript></div>
<div class="spacer-xlarge"></div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Church ladies 'out' phony</h5>
						<a href="http://www.catholic.org/news/national/story.php?id=67093"><img src="/files/images/editors_choice/2016024201arrestsl.jpg" alt="Church ladies 'out' phony" title="Church ladies 'out' phony" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/national/story.php?id=67093">Phony priest stumbled and fell over feasts</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="row">
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Seeking reconciliation</h5>
						<a href="http://www.catholic.org/news/international/europe/story.php?id=67091"><img src="/files/images/editors_choice/2016024310psl.jpg" alt="Seeking reconciliation" title="Seeking reconciliation" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/international/europe/story.php?id=67091">Pope, Patriarch to meet in Cuba to repair rifts</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Barbarity continues</h5>
						<a href="http://www.catholic.org/news/international/africa/story.php?id=67089"><img src="/files/images/editors_choice/2016024533cry.jpg" alt="Barbarity continues" title="Barbarity continues" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/international/africa/story.php?id=67089">Female gential mutilation still practiced in world</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Annunciation here?</h5>
						<a href="http://www.catholic.org/news/hf/faith/story.php?id=67079"><img src="/files/images/editors_choice/2016024757mary.jpg" alt="Annunciation here?" title="Annunciation here?" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/hf/faith/story.php?id=67079">Ancient artwork points to presence of Mary</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="row">
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>'Dam will burst!'</h5>
						<a href="http://www.catholic.org/news/international/middle_east/story.php?id=67077"><img src="/files/images/editors_choice/2016024906kidssl.jpg" alt="'Dam will burst!'" title="'Dam will burst!'" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/international/middle_east/story.php?id=67077">King of Jordan says Syrian refugees taxing system</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Mexican martyrs</h5>
						<a href="http://www.catholic.org/news/international/americas/story.php?id=67075"><img src="/files/images/editors_choice/2016025024killsl.jpg" alt="Mexican martyrs" title="Mexican martyrs" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/international/americas/story.php?id=67075">Photo of slain family points to drug violence</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>'What We Did'</h5>
						<a href="http://www.catholic.org/news/hf/family/story.php?id=67073"><img src="/files/images/editors_choice/2016025603book.jpg" alt="'What We Did'" title="'What We Did'" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/hf/family/story.php?id=67073">Book tells of family's struggle with cancer</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="row">
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>English-speaking boy --</h5>
						<a href="http://www.catholic.org/news/international/middle_east/story.php?id=67071"><img src="/files/images/editors_choice/2016025840behead.jpg" alt="English-speaking boy --" title="English-speaking boy --" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/international/middle_east/story.php?id=67071">-- featured in latest Islamic State video</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>ISIS coward!</h5>
						<a href="http://www.catholic.org/news/international/middle_east/story.php?id=67069"><img src="/files/images/editors_choice/2016020058atsl.jpg" alt="ISIS coward!" title="ISIS coward!" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/international/middle_east/story.php?id=67069">Suicide bomb maker says he wouldn't do it</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Special Rome appearance</h5>
						<a href="http://www.catholic.org/news/international/europe/story.php?id=67099"><img src="/files/images/editors_choice/2016020233glasssl.jpg" alt="Special Rome appearance" title="Special Rome appearance" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/international/europe/story.php?id=67099">Remains of saints arrive for Jubilee of Mercy</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="row">
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Trump promises --</h5>
						<a href="http://www.catholic.org/news/politics/story.php?id=67041"><img src="/files/images/editors_choice/2016023547trumpsl.jpg" alt="Trump promises --" title="Trump promises --" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/politics/story.php?id=67041">-- to beat the CENSORED out of ISIS!</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Faith antidote for fear</h5>
						<a href="http://www.catholic.org/news/politics/story.php?id=67063"><img src="/files/images/editors_choice/2016023935prez.jpg" alt="Faith antidote for fear" title="Faith antidote for fear" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/politics/story.php?id=67063">President Obama speaks at prayer breakfast</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="ec-item">
						<h5>Mexico visit</h5>
						<a href="http://www.catholic.org/news/hf/faith/story.php?id=67061"><img src="/files/images/editors_choice/2016024109popesl.jpg" alt="Mexico visit" title="Mexico visit" width="300" height="250" class="img-responsive img-rounded" /></a>
						<p><a href="http://www.catholic.org/news/hf/faith/story.php?id=67061">Pope will visit Mexico, not as 'wise man'</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-ec" data-slide="prev">
		<span class="icon-prev"></span>
	</a>
	<a class="right carousel-control" href="#carousel-ec" data-slide="next">
		<span class="icon-next"></span>
	</a>
</div>
				</div>
			</div>
			<div class="spacer-large"></div>
		</div>
		<!-- /#content-page -->
		<!-- #footer -->
		<div id="footer" class="footer hidden-print">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-4">
						<p><img src="/assets/img/logo-footer.png" border="0" alt="Catholic Online Logo" title="Catholic Online" class="img-responsive"/></p>
<p>Copyright 2015 Catholic Online. All materials contained on this site, whether written, audible or visual are the exclusive property of Catholic Online and are protected under U.S. and International copyright laws, &copy; Copyright 2015 Catholic Online. Any unauthorized use, without prior written consent of Catholic Online is strictly forbidden and prohibited.</p>
					</div>
					<div class="col-xs-12 col-md-7 col-md-offset-1">
						<div class="row">
	<div class="col-md-4">
		<h4 class="text-danger">About Us</h4>
		<ul class="list-unstyled">
			<li><a href="/about/" title="About Us">About Us</a></li>
			<li><a href="/services/advertising" title="Advertisers">Advertisers</a></li>
			<li><a href="/services/advertising/job_ap.php " title="Employment">Employment</a></li>
			<li><a href="/about/contactus.php" title="Contact Us">Contact Us</a></li>
			<li><a href="/about/ourmission.php" title="Our Mission">Our Mission</a></li>
			<li><a href="/about/privacy.php" title="Privacy Policy">Privacy Policy</a></li>
			<li><a href="/about/tos.php" title="Terms of Service">Terms of Service</a></li>
		</ul>
		<h4 class="text-danger">Social</h4>
		<ul class="list-unstyled">
			<li><a href="http://www.twitter.com/catholiconline" title="Twitter">Twitter</a></li>
			<li><a href="http://www.facebook.com/catholiconline" title="Facebook">Facebook</a></li>
		</ul>
	</div>
	<div class="col-md-4">
		<h4 class="text-danger">News</h4>
		<ul class="list-unstyled">
			<li><a href="/national/" title="U.S. News">U.S. News</a></li>
			<li><a href="/international" title="International">International</a></li>
			<li><a href="/politics/" title="Politics">Politics</a></li>
			<li><a href="/prwire/" title="PRWire">PRWire</a></li>
			<li><a href="/college/" title="College & University">College & University</a></li>
			<li><a href="/ae/" title="Entertainment">Entertainment</a></li>
			<li><a href="/hf/" title="Home & Family">Home &amp; Family</a></li>
			<li><a href="/business" title="Business & Economics">Business & Economics</a></li>
			<li><a href="/travel/" title="Travel">Travel</a></li>
			<li><a href="/photos/" title="Photos">Photos</a></li>
			<li><a href="/mostpopular.php" title="Most Popular">Most Popular</a></li>
			<li><a href="/newsletters/" title="Newsletters">Newsletters</a></li>
			<li><a href="/xml/" title="RSS">RSS</a></li>
		</ul>
	</div>
	<div class="col-md-4">
		<h4 class="text-danger">Catholic Life</h4>
		<ul class="list-unstyled">
			<li><a href="/bible/" title="Bible">Bible</a></li>
			<li><a href="/bookstore/" title="Bookstore">Bookstore</a></li>
			<li><a href="/clife/advent/" title="Christmas / Advent">Christmas / Advent</a></li>
			<li><a href="/bible/daily_reading/" title="Daily Readings">Daily Readings</a></li>
			<li><a href="/directory/" title="Directory">Directory</a></li>
			<li><a href="/clife/lent/" title="Easter / Lent">Easter / Lent</a></li>
			<li><a href="/encyclopedia/" title="Encyclopedia">Encyclopedia</a></li>
			<li><a href="http://forum.catholic.org/" title="Forum">Forum</a></li>
			<li><a href="/prayers/" title="Prayers">Prayers</a></li>
			<li><a href="/saints/" title="Saints & Angels">Saints &amp; Angels</a></li>
			<li><a href="/saints/sofd.php" title="Saint of the Day">Saint of the Day</a></li>
			<li><a href="/shopping/" title="Shopping">Shopping</a></li>
			<li><a href="/vocations/" title="Vocations">Vocations</a></li>
		</ul>
	</div>
</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /#footer -->
<!-- Modal: One Per 24 -->
<div class="modal fade"  id="modalOnePer24" tabindex="-1" role="dialog" aria-labelledby="modalOnePer24Label" aria-hidden="true">
        <div class="modal-dialog">
                <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title text-danger" id="modalOnePer24Label"><strong><center>The most important email of the day!</center></strong></h4>
                        </div>
                        <div class="modal-body">	<form name="newsletterForm" method="post" action="/newsletters/signUp.php" class="form-horizontal" role="form">
		<div class="media">
			<input type="checkbox" name="list[]" value="Special Offers" tabindex="1" class="pull-left" checked />
			<div class="media-body">
				<p class="lead">Sign up to receive your <strong>FREE</strong> Catholic Online newsletters and special offers by email.</p>
				<p>There is NO COST for the subscription.</p>
			</div>
		</div>
		<div class="input-group">
			<span class="input-group-addon"><i class="icon-envelope-alt"></i></span>
			<input id="newsletters-email" name="email" value="" tabindex="1" class="form-control" placeholder="your@email.com" type="text">
		</div>
		<div class="clearfix spacer"></div>
		<div class="panel-group" id="accordion">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><i class="icon-plus"></i> <strong>Optional</strong></a></h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="form-group">
							<label for="newsletters-first-name" class="control-label col-sm-3">First Name</label>
							<div class="col-sm-9">
								<input id="newsletter-first-name" name="first_name" value="" tabindex="1" class="form-control" placeholder="John / Jane" type="text">
							</div>
						</div>
						<div class="form-group">
							<label for="newsletters-last-name" class="control-label col-sm-3">Last Name</label>
							<div class="col-sm-9">
								<input id="newsletter-last-name" name="last_name" value="" tabindex="1" class="form-control" placeholder="Doe" type="text">
							</div>
						</div>
						<div class="form-group">
							<label for="newsletters-gender" class="control-label col-sm-3">Gender</label>
							<div class="col-sm-9">
								<select name="gender" id="newsletters-gender" class="form-control" tabindex="1">
									<option value=""></option>
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="newsletters-city" class="control-label col-sm-3">City</label>
							<div class="col-sm-9">
								<input id="newsletter-city" name="city" value="" tabindex="1" class="form-control" placeholder="Los Angeles" type="text">
							</div>
						</div>
						<div class="form-group">
							<label for="newsletters-country" class="control-label col-sm-3">Country</label>
							<div class="col-sm-9">
								<input id="newsletter-country" name="country" value="" tabindex="1" class="form-control" placeholder="United States" type="text">
							</div>
						</div>
						<div class="form-group">
							<label for="newsletters-postal-code" class="control-label col-sm-3">Postal Code</label>
							<div class="col-sm-9">
								<input id="newsletter-postal-code" name="postal_code" value="" tabindex="1" class="form-control" placeholder="90012" type="text">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix spacer"></div>
		<input name="submit" value="Subscribe Now" tabindex="1" class="btn btn-danger" type="submit">
	</form>                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div><!-- /.modal -->		<!-- Share -->
<!-- AddThis Smart Layers BEGIN -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=catholiconline"></script>
<script type="text/javascript">
	addthis.layers({
		'theme' : 'dark',
		'share' : {
			'position' : 'right',
			'numPreferredServices' : 5,
			'services': 'facebook,twitter,email,print,more',
			'theme' : 'transparent'
		},
		'follow' : {
			'services' : [
				{'service': 'twitter', 'id': 'catholiconline'},
				{'service': 'pinterest', 'id': 'catholiconline'},
				{'service': 'instagram', 'id': 'catholiconline'}
			]
		}
	});
</script>
<!-- AddThis Smart Layers END -->
		<!-- Analytics -->
<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1273940-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- /Google Analytics -->
<!-- Quantcast --> 
<script type="text/javascript">
	var _qevents = _qevents || [];
	(function() {
		var elem = document.createElement('script');
		elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
		elem.async = true;
		elem.type = "text/javascript";
		var scpt = document.getElementsByTagName('script')[0];
		scpt.parentNode.insertBefore(elem, scpt);
	})();
</script>
<script type="text/javascript">
	_qevents.push( { qacct:"p-d4dkf-TE9VMC6"} );
</script>
<noscript>
	<div style="display: none;"><img src="//pixel.quantserve.com/pixel/p-d4dkf-TE9VMC6.gif" height="1" width="1" alt="Quantcast"/></div>
</noscript>
<!-- /Quantcast -->
<!-- /Analytics -->
		<script>
			// Carousel: Editor's Choice
			jQuery( '#carousel-ec' ).carousel();
			// Carousel: Products
			if( jQuery( '#carousel-products-content' ).length > 0 ){
				jQuery( '#carousel-products-content' ).carousel({
					interval: 9000
				});
			}
			// Social tabs
			jQuery( '#tabShare a' ).click( function( e ){
				e.preventDefault();
				jQuery( this ).tab( 'show' );
			});
			// Modal: One per 24 hours
			var now = ( new Date() ).getTime();
			var onePer24 = 0;
			var onePer24Str = localStorage['onePer24'];
			if( onePer24Str ){
				onePer24 = parseInt(onePer24Str, 10);
			}
			if( now - onePer24 > 1*24*60*60*1000 ){
				if( jQuery( '#modalOnePer24' ).length > 0 ){
					jQuery( '#modalOnePer24' ).delay(2000).modal();
				}
			}
			localStorage['onePer24'] = ""+now;
			// Back to Top
			$(function () {
				Application.init ();
			});
			var Application = function () {
				return { init: init };
				function init () {
					enableBackToTop ();
				}
				function enableBackToTop () {
					var backToTop = $('<a>', { id: 'back-to-top', href: '#top' });
					var icon = $('<i>', { class: 'icon-chevron-up' });
					backToTop.appendTo ('body');
					icon.appendTo (backToTop);
					backToTop.hide();
					$(window).scroll(function () {
						if ($(this).scrollTop() > 150) {
							backToTop.fadeIn ();
						} else {
							backToTop.fadeOut ();
						}
					});
					backToTop.click (function (e) {
						e.preventDefault ();
						$('body, html').animate({
							scrollTop: 0
						}, 600);
					});
				}
			}();
		</script>
	<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"4a4249cee1","applicationID":"11038939","transactionName":"b1RVMBdQV0JSBkAPCVYeYhYMHlhTXBBASRZKWEEFBkgXQVsV","queueTime":0,"applicationTime":323,"ttGuid":"","agentToken":"","userAttributes":"","errorBeacon":"bam.nr-data.net","agent":"js-agent.newrelic.com\/nr-768.min.js"}</script></body>
</html>
 
