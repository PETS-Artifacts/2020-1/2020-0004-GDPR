<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head><title>Privacy Statement</title><meta name=viewport content='width=device-width, initial-scale=1'><meta http-equiv='Content-Type' content='text/html; charset=UTF-8' /><meta name="Keywords" content="pixel art, pixelart, pixel artists,, pixel art forum, 8bit, retro, game art, pixel art message board, pixel art community, pixel art message board, icon art, sprites, sprite art, comic book icons, movie icons, sports icons, television icons, video game sprites, Messenger icons, AOL avatars, forum avatars" /><meta name="Description" content="Welcome to Pixel Joint: home of the world's largest pixel art community. Featuring a massive pixel art gallery, forum, mockups, games, links, icons, downloads and sprites." /><meta name='Author' content="Luke Sedgeman" /><meta property="og:image" content="http://pixeljoint.com/pixels/images/og.png" /><meta name="verify-v1" content="pOFV4U4s9tN3BgFtHs89nbwYNjQ9cWBKghrevNGPrEI=" /><meta name="theme-color" content="#008ece">
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<link rel="shortcut icon" href="http://pixeljoint.com/favicon.ico" />
<link rel="icon" href="http://pixeljoint.com/favicon.png" type="image/gif" /> 
<link rel="stylesheet" type="text/css" href="/pixels/includes/styles.css?v=3" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<script src="http://www.google.com/jsapi?key=ABQIAAAA61CrcQf2KHXZWMOyhN4_HxQthrAFtrDwvOnRXD40ZBTAYtJw7BQNLCeCjAa3Btflx5h6DARDUnUQyQ" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="/pixels/includes/js/jquerytools.js?v=5" type="text/javascript"></script><!--PROFILE TABS-->
<script src="/pixels/includes/js/pixels.js?v=4" type="text/javascript"></script>
</head>

<body>
<div id="header">
	<div class="layout">
		<div id="headpictext"><a href="/"><img src="/pixels/images/cdot.gif" alt="Pixel Art Character" width="320" height="48" border="0" /></a></div>
		<div class='nav-search'></div>
		<div class='nav-search-large'>	
<form action="/pixels/search.asp" id="cse-search-box">
  <div>
    <input type="hidden" name="cx" value="013448474317442146314:w9ybqizwyg8" />
    <input type="hidden" name="cof" value="FORID:10" />
    <input type="hidden" name="ie" value="UTF-8" />
    <input type="text" name="q" size="31" id="searchinput" /><button type="submit" name="sa" class='buttonicon' value='Search'><i class="fa fa-search"></i></button>
  </div>
</form>
		</div>
	</div>
	<div class="layout nav-holder">
		<div class="nav"><a href="/pixels/new_icons.asp" onmouseover="ddm(this,event,mGallery,'110px')" onmouseout="dhm()">Gallery</a></div>
		<div class="nav"><a href="/pixels/links.asp?id=2225" onmouseover="ddm(this,event,mFeatures,'110px')" onmouseout="dhm()">Features</a></div>
		<div class="nav"><a href="/pixels/members.asp" onmouseover="ddm(this,event,mCommunity,'105px')" onmouseout="dhm()">Community</a></div>
		<div class="nav tablet-span"><a href="/forum/">Forum</a></div>
		
		<div class="nav nav-login"><a href="/pixels/logon.asp"><i class="fa fa-user"></i></a></div>
		<div class="nav-login-large">
			<form id="frmtop" name="frmtop" action="/pixels/logon.asp">	
				<input type="hidden" name="action" value="login" />
				<input type="hidden" name="remember" value="1" />
				<table align="right" cellspacing="0" cellpadding="0" border="0">				
					<tr>
						<td><input onfocus="wipelogin();" style='margin-right:2px;' type="text" name="login" value="Username" class="login-top-input" size="10" /></td>
						<td><input onfocus="wipelogin();" type="password" name="password" value="XXXXXXX" class="login-top-input" size="10" /></td>				
						<td><button type="submit" name="sendlogin" value="login" class="buttonicon"><i class="fa fa-chevron-right"></i></button></td>
					</tr>	
				</table>
			</form>		
		</div>		
				
	</div>
</div>	
<div id="container" >	
<h1>Privacy Policy</h1><div class="bx"><div style='width:100%'>
<strong>What This Privacy Policy Covers </strong><br />
This policy covers how PixelJoint.com treats personal information that PixelJoint.com collects and receives, including information related to your past use of PixelJoint.com products and services. 
Personal information is information about you that is personally identifiable like your name, address, email address, or phone number, and that is not otherwise publicly available. 
This policy does not apply to the practices of companies that PixelJoint.com does not own or control, or to people that PixelJoint.com does not employ or manage. 
<br /><br />

<strong>Collection of your Personal Information </strong><br />
PixelJoint.com collects personally identifiable information, such as your e-mail address, name, home or work address, telephone number, demographic information, and information that is not unique to you, such as your ZIP code, age, gender, preferences, interests and favorites. 
<br /><br />
PixelJoint.com does not associate sensitive data, such as your race, religion, or political affiliations, with personally identifiable information. PixelJoint.com takes precautions to segregate any sensitive data you may provide on PixelJoint.com and services that reference your race, religion, sexual orientation, ethnicity or political affiliations. 
<br /><br />
Please keep in mind that if you directly disclose personally identifiable information or personally sensitive data through PixelJoint.com public online forums or through your own PixelJoint.com public site, this information may be collected and used by others. Note: PixelJoint.com does not read any of your private online communications. All messages and communication through PixelJoint.com are completely confidential. 
<br /><br />
Information about your connection and software on your computer is automatically collected by PixelJoint.com. This information can include: your IP address, browser type, domain names, access times and referring web site addresses. This information is used by PixelJoint.com for the operation of the service, to maintain quality of the service, and to provide general statistics regarding use of PixelJoint.com. 
<br /><br />

<strong>Use of your Personal Information </strong><br />
PixelJoint.com does not sell, rent or lease its member lists to third parties. PixelJoint.com may, from time to time, contact you on behalf of external business partners about a particular offering that may be of interest to you. 
In those cases, your unique personally identifiable information (e-mail, name, address, telephone number) is not transferred to the third party. In addition, PixelJoint.com may share data with trusted partners to help us perform statistical analysis. All such third parties are prohibited from using your personal information except to provide these services to PixelJoint.com, and they are required to maintain the confidentiality of your information.
<br /><br />
PixelJoint.com keeps track of the pages our visitors visit within PixelJoint.com, in order to determine what pages on PixelJoint.com are the most popular. This data is used to get a better idea of the interests of our visitors. 
<br /><br />

<strong>Use of Cookies </strong><br />
PixelJoint.com uses "cookies" to help you personalize your online experience. 
A cookie is a text file that is placed on your hard disk by a Web page server. 
Cookies cannot be used to run programs or deliver viruses to your computer. 
Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. PixelJoint.com is the only domain that can read PixelJoint.com issued cookies. 
<br /><br />
One of the primary purposes of cookies is to provide a convenience feature to save you time. 
The purpose of a cookie is to tell the Web server that you have returned to a specific page. For example, if you personalize PixelJoint.com pages, or register with PixelJoint.com, a cookie helps PixelJoint.com to recall your specific information on subsequent visits. When you return to PixelJoint.com, the information you previously provided can be retrieved, so you can easily use the PixelJoint.com features that you customized.
You have the ability to accept or decline cookies. 
<br /><br />
Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of PixelJoint.com. 
<br /><br />

<strong>Control your Personal Information </strong><br />
PixelJoint.com offers its members choices for the collection, use and sharing of personal information. You may go to your personal profile to proactively make choices about the use and sharing of your personal information. 
You may choose not to publicly expose and personal information and or your personal PixelJoint.com site. 
<br /><br />
In addition, you may visit your profile to view, edit or delete your personal information from the PixelJoint.com database.
<br /><br />
In both cases described above, you will be asked to make your requests while logged into your PixelJoint.com member account, so that PixelJoint.com can associate your preferences with a particular account. This security safeguard prevents someone else from making choices about the usage and control of your personal information.
<br /><br />

<strong>Security of your Personal Information</strong> <br />
PixelJoint.com secures your personal information from unauthorized access, use or disclosure. PixelJoint.com secures the personally identifiable information you provide on computer servers in a controlled, secure environment, protected from unauthorized access, use or disclosure. 
<br /><br />

<strong>Changes to this Privacy Statement</strong> <br />
PixelJoint.com may update this policy. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your PixelJoint.com account or by placing a prominent notice on our site.  

</div></div>
<div align='center' class="bxfull google-ads">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-5197635564983687"
     data-ad-slot="3282203312"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>

</div>
<br />
<div id="footer">
	<div class="layout">
		<div id='footer-icons-holder'>
			<div id='footer-icons'>
			<a href="https://www.facebook.com/pixeljoint" class='footer-icon' target='_blank'><i class='fa fa-facebook'></i></a>
			<a href="https://twitter.com/pixeljoint" class='footer-icon' target='_blank'><i class='fa fa-twitter'></i></a>
			<a href="http://www.youtube.com/user/sedgemonkey" class='footer-icon' target='_blank'><i class='fa fa-youtube'></i></a>
			</div>	
				
	
			</div>
		</div>
		<div id='footer-links'>
		<a href="/pixels/register.asp">Register</a> | <a href="/pixels/contact.asp">Contact</a> | <a href='/pixels/aboutus.asp'>About</a>
		
		</div>		
		<div id="footer-legal">
All pixel art copyrights are retained by the pixel artists.<br /><a href='/pixels/privacy.asp'>Privacy Policy</a>&nbsp;|&nbsp;<a href='/pixels/terms.asp'>Terms of Service</a>&nbsp;|&nbsp;<a href='/pixels/faqs.asp'>FAQs</a>
		</div>
	</div>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-71835-1");
pageTracker._trackPageview();
} catch(err) {}</script>
</body>
</html>
