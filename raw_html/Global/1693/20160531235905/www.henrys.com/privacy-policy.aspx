
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head id="Head1"><title>
	Privacy Policy - Henry's best camera store in Canada
</title><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info = {"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"311198b2e4","applicationID":"11674521,11674490,2215095","transactionName":"MgdaYEtVWUFSVBBQCQtNeWdpG0dAWkEFWh9IEldYUFdOHFJEFEE=","queueTime":0,"applicationTime":157,"ttGuid":"DFA299914428F150","agent":""}</script><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var o=t[n]={exports:{}};e[n][0].call(o.exports,function(t){var o=e[n][1][t];return r(o||t)},o,o.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(e,t,n){function r(e,t){return function(){o(e,[(new Date).getTime()].concat(a(arguments)),null,t)}}var o=e("handle"),i=e(2),a=e(3);"undefined"==typeof window.newrelic&&(newrelic=NREUM);var u=["setPageViewName","setCustomAttribute","finished","addToTrace","inlineHit"],c=["addPageAction"],f="api-";i(u,function(e,t){newrelic[t]=r(f+t,"api")}),i(c,function(e,t){newrelic[t]=r(f+t)}),t.exports=newrelic,newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),o("err",[e,(new Date).getTime()])}},{}],2:[function(e,t,n){function r(e,t){var n=[],r="",i=0;for(r in e)o.call(e,r)&&(n[i]=t(r,e[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],3:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,o=n-t||0,i=Array(0>o?0:o);++r<o;)i[r]=e[t+r];return i}t.exports=r},{}],ee:[function(e,t,n){function r(){}function o(e){function t(e){return e&&e instanceof r?e:e?u(e,a,i):i()}function n(n,r,o){e&&e(n,r,o);for(var i=t(o),a=l(n),u=a.length,c=0;u>c;c++)a[c].apply(i,r);var s=f[g[n]];return s&&s.push([m,n,r,i]),i}function p(e,t){w[e]=l(e).concat(t)}function l(e){return w[e]||[]}function d(e){return s[e]=s[e]||o(n)}function v(e,t){c(e,function(e,n){t=t||"feature",g[n]=t,t in f||(f[t]=[])})}var w={},g={},m={on:p,emit:n,get:d,listeners:l,context:t,buffer:v};return m}function i(){return new r}var a="nr@context",u=e("gos"),c=e(2),f={},s={},p=t.exports=o();p.backlog=f},{}],gos:[function(e,t,n){function r(e,t,n){if(o.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[t]=r,r}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){o.buffer([e],r),o.emit(e,t,n)}var o=e("ee").get("handle");t.exports=r,r.ee=o},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!w++){var e=v.info=NREUM.info,t=s.getElementsByTagName("script")[0];if(e&&e.licenseKey&&e.applicationID&&t){c(l,function(t,n){e[t]||(e[t]=n)});var n="https"===p.split(":")[0]||e.sslForHttp;v.proto=n?"https://":"http://",u("mark",["onload",a()],null,"api");var r=s.createElement("script");r.src=v.proto+e.agent,t.parentNode.insertBefore(r,t)}}}function o(){"complete"===s.readyState&&i()}function i(){u("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var u=e("handle"),c=e(2),f=window,s=f.document;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:f.XMLHttpRequest,REQ:f.Request,EV:f.Event,PR:f.Promise,MO:f.MutationObserver},e(1);var p=""+location,l={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-943.min.js"},d=window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&!/CriOS/.test(navigator.userAgent),v=t.exports={offset:a(),origin:p,features:{},xhrWrappable:d};s.addEventListener?(s.addEventListener("DOMContentLoaded",i,!1),f.addEventListener("load",r,!1)):(s.attachEvent("onreadystatechange",o),f.attachEvent("onload",r)),u("mark",["firstbyte",a()],null,"api");var w=0},{}]},{},["loader"]);</script><link rel="Stylesheet" type="text/css" href="Styles/Common.css?v=2016.03.17" /><link rel="Stylesheet" type="text/css" href="Styles/colorbox.css?v=2014.08.21" /><link rel="Stylesheet" type="text/css" href="Styles/jquery.fancybox.css?v=2014.08.21" /><link rel="Stylesheet" type="text/css" href="Styles/jquery.alerts.css?v=2014.08.21" /><link rel="Stylesheet" type="text/css" href="Styles/Henrys.css?v=2015.08.21" />
    
<!--Start of Zopim Live Chat Script-->
<!-- OFF
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2tGk2NEvRr4JOhG1GbhjpcyonVzVBkH7";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
-->
<!--End of Zopim Live Chat Script-->
    

    <script type='text/javascript'>
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-1012104-3']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
			</script>


	
	<link rel="alternate" media="only screen and (max-width: 640px)" href="//m.henrys.com/Privacy-Policy.aspx" />

	<!--[if IE]>
	<style type="text/css">
		.browsetabs .carousel-dots { top: -8px !important; }
	</style>		
	<![endif]-->
	<link rel="shortcut icon" type="image/x-icon" href="/Images/favicon.ico"/>
	<link rel="icon" type="image/x-icon" href="/Images/favicon.ico"/>		
	<link rel="image_src" href="/Images/logo.jpg" />

	
    <script>        (function () {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', "1421570248097921"]);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(["track", "PixelInitialized", {}]);
    </script>
    <noscript><img height="1" width="1" border="0" alt="" style="display:none" src="https://www.facebook.com/tr?id=1421570248097921&amp;ev=NoScript" /></noscript>


    
    	
 <script type="text/javascript">
var google_tag_params = {
ecomm_pagetype: 'other'
};
</script>
	

<!--START AffiliateTraction CODE-->
<img src="https://henrys.affiliatetechnology.com/impression.php" width="0" height="0" style="display:none;" />
<script type="text/javascript" src="https://henrys.affiliatetechnology.com/abandonment.js"></script>
<!-- END AffiliateTraction CODE -->



<script type='text/javascript' >
var google_tag_params = {ecomm_pagetype: 'other'};</script>
<script type='text/javascript'>
				var _gaq = _gaq || [];
				
				_gaq.push(['_setAccount', 'UA-1012104-3']);
				_gaq.push(['_setCampSourceKey', 'utm_source']); 
				_gaq.push(['_setCampTermKey', 'utm_term']);
			_gaq.push(['_setCampNameKey', 'utm_campaign']);
			_gaq.push(['_setCampMediumKey', 'utm_medium']);
			_gaq.push(['_setCampContentKey', 'utm_content']);       
				_gaq.push(['_trackPageview']);
				(function () {
					var ga = document.createElement('script');
					ga.type = 'text/javascript';
					ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(ga, s);
				})();
			</script></head>
<body class="en-CA">
<script>
	dataLayer = []; // for Google Tag manager
</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-SN9F"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-SN9F');</script>
<!-- End Google Tag Manager -->

    <form method="post" action="/Privacy-Policy.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VSTATE" id="__VSTATE" value="H4sIAAAAAAAEAOx9CXgcxZXwtKUZaeRDNjaGcDaGmGtGmtFlyxiDTktBF5J8EAJDa6ZH07ine+jukTQOm5PcCQlJNjdZchHIHZKw5CKEkGPJsUnI5t6ce2bzZ9lsjs0F/3tV1d3Vx0gj2dhCX+azS12v6lW9evXq1dGvXj8uNJ++KdqZtdRU6gIaFmQpJxsXqKbVJ1nyrG4osglJhpq6YMLQcyOKaQ2RLM3bLo1sytHf3ksj9ZF19fCUX/c4/JpzUXGpQn346yPr1vP4tRHVtjyiaiu0/YkotNtf6Gwu2lUTZjrA+dlaK921Ysx0COY5mUyfrlmGrpqT8k1lxZAndNPqlbJHrpIrmcx2IXohX/a8rGb1ojyq5yT1ghlLmyrPFBVroCgpao1EdC6vIyb4QrNAqaxZE6qUlYd0FSswlNmC1aer5aLmActZyKhWDiryvJw7pORmZYsWwtV3qrBuOAfRKPwX1t3xffr30e/Rv3e9ncFfSf/ec3f9uujyZWT3irur44kQ2a4gOc3R9cl0d6q7u213965Uc277urwd1JNgXWPzqXU7WluLLQVZMypmC4hA64ShzEnZSnJCV5VspUUySwtbW4eL0qxstualOQX6qgWCMFhuXbx5+7pz6guGnI9utjOo+qzecmNpNrdO2JyD+tfVIRXros3Ocx45sK55e/056zPn9UuW1KuXtdwsxoYtudgHMQu7iNLetE4gxGPYlG8+tSHR3d6xqy05OTB4YLJ3eGpooD85NnzV+Fiyf1dnKtmfnBqZTPaO919D2hIpFCyrtKe1VTqa7mrfvaujZU5vKZpyVsu1aLLVmu5oz3bnO9Od2Xy+I7e7S5rpyqe6u7rzkpRt39XdZrcKuV3OWiatuzO1AFW1pbo7OjpTneld2N4dHEEiIUhEgkRCkIgEicujvIYCgcfNyMcGRl7b92+vjwiRSARlCf/ib8M6CORxA6Skv6JJRSVrtvTphtzSWzYVTTbNQSkLIpQQD8qGqeja5emWVEtnqiWVEPvKqlU25Ms1uWwZkpoQJ8ozICagUqb1I7J2eVdXe6qrs607lWrvmpE65CjWmVyyqhZG7rCW1y9BCjMlChiTinJTpgQcn1YsVd6SMQu6YfXLZtZQShbQtjmj6tosB4hlZgxJyzVnipJWzktZJNcY7t+QMWQLlBlId1bekMmWTUsvTssLVpqPtPGR9njmiFyZ142c2ZCZl1EfxTIKyKPZCOQpWUWbXZ/JOqNwW0YBJTZrSEjFlGxZkG6eFgY8XFQ3Z6ACWcvJuXFCtbnVD4BMp2ck01RmNTnXY1mGMlO25P2GXi6Z6zOSDTCbMoPKgpzrA+W+ITMvGXJBL5vyMDBAlUzrQCkHBOYOWNmtfHwSVATAnpIpQQ/rmqQqRwmFV5dlk9S+PTMqG9kCMFIxpRlVZv3TmOlF5g73N2YsaaFPz8kbM1hgTy5HKolmIPlIU0Yx+2VVhorWA9vnFBSi4VxTZk7SFKtywFDrMkoultF0oH8DtkXKFoqg1s3tXrHolUz5Ush6XhjYLJdKIAvmGJZy4WI5etwKnhKWj9BxblgKR5qAv7oIhvX19XX1Qr1QVxeJ1GFABpYQwUcItKmKCVLSMlZWVeTdDelrr2WgYc1qb0uIRTOrG6oy446wDhhhNQ2wmV27pM5sZ1caNIec2t193XXRi5YeXoeI9OKw71g6M6pcHIgw+apyFsUBEffVPIgn6NgIFvEJgbHBBZst+2VNBoQWnLmQU0tXwya+ClaQEE+IGrvuukuWpmvKAsJ6pVls6u6auTXgHfaI/Kwa2PQECtTnjk83sfY5quugpJblE9Zf0cZm1gpYT8DkUZRvqj4s7SzHmZFfOa6MnAhX1SeMo0LjxkahsfGe49Mo1NonjPT7jg/N7jxywihvFIS/MvxEMhz1bwzn8RoW27F6yHfR0vna23elU72xaI2lxiBfYW9Z3bdXVfa1dbS0i6MT4uDhZF43ipIlTsmaqRsb97ZCKuboaukU8yVTxN2+opVh9SdOFXQdV5pupt6yolpJRRMPKclBhcFboY5YA9QWJZQ0NaS7u7tburtjjQD7+Z4TQQL8b29pExUtWxCnIR0wxIOSoSQlbVaVxZG+fjfjIChucahfPKjkZN0Fd6aTJR1W2WLPoGgSqRPnFasgpjvFrKGbZtKqlGTRJCSbIqxexfZ+oFTVy4Y4KsHktCCOwlLVwKqHh4fdgvfOkCqTeQM2IWLekOWcXkyIetkyLVwYQ35pVlFhRUtKtQqyCPvgOVlTZC0ri3penPE0eW/rzD6uNboBKLopi/MFXcwrUIKimSWFbhVEeU42KvMF2QC5xgwmtAl4NCNb87KsiaD6VZW2Zg7ZQZoMlIlFBbYMSBlklSyKOy8Bd5A8VdePiLqmAsFi3m0aFb6spIkw2hSomZSLmKoOEbMgEeZACYoBMd0yE6Q4IrmKSZ4tXVchEMuaKktmQawgeyXDgrFuVFqAAdAjeaAIRqPJGirnxJmK2N8xRarr351OccXOYJWmmJOOHlWxcgV33eJNZQn5nRBhgSmDJCpZ1vysVJJmsC9gI0aKg/1j0qkG9zG0FGiiqClFmP7FnIx7KyorkmiFCh8pytuL2McaqrQ5IMTtzgHNKoA6gi2WKZZLsOEj8pE39CIU3u+MHMrqeeg8sSgZc7IqSpbT6J3nt3dfZvI9A9szggjy5FYF6wET5Fk+QvsZtqJQVEUEeTJlIC6HkSxgG9DJAMwDTcmSlAU+mDgoTVq9VCoZclaBlU+gflCxqiIZLtuQC2YJ5N8logdAeUUtFqUjoHWJZJEGQ4USsKgIFdqtBakgvUloJUsZFHCAqkpeRrIt0n8oGmJvchJmDEohGROOQEiEGcB4gFueQdoD7TYUVDpECPOEERRAGgz07fGOvWm70IJk4lCgcglYFsqvckQOjA6QWWCcqRzl+mEYcpsyFX8Tc0plS8/r2TKVwKKtVCw5W9BA38zCsKO5bZknzyD3YlIcU47oGuuAkj4PTS2r4nFWbbasd6cTqVQqWVIWQP4m9/eyEviWOaJgWqRcGLUlmMOVOfkyylTR1vkgv8gszKGrZexar5Aodlmwy1FhsgblA6rNhLGrOmUxbQZDrAgTtlKCwUfHCygaV71BCQUcCTkYuiAbCl39mkUooQDjtygnValkIj9uKqMGxoGIstWdgGmN6i2oIsmGBukiWEUA7yUicVCIaZU1DZmFKq+ESwvaeAU0r0XW3qDrx0ugfzV5nki5bhIyJBU6DRSvo4JIcSgyMLslyeyWTrS1dR8Rc9DcKromp5glVQLlBgIMgmWPI0tBbWXCGLdKBV2jIwY3MJZIpNeQiyDj0BbgCA53+D8C/STiCTnlr2cahgGDE7pnPqf9T9o6cHhiAJYmHb4RM4KDgsxJVFIDA4RT3WwSQdpUGVUczIQe9Q0Dbl6ETpMNkT+kM8kUBPRkC7wommKA3r7R8SlWJCMdZqGSRHQ94bzdCpYJ1A6Ig2krKCLVlHaYbWUQMqBUnxc1XTFxugWJE4enxiFRmyXzeDqVQkFKtyV2w9NF8kIJWIVdwCQMM3dCB6dSFydEe5myuOCyUQXaEMCkYhxJBWW2QErDZilkjlGQviJgFUHIirpi7Dy/rb39soR4ozQ7a892eUk1cfWBI59CJLufFAMam+R0EzengDTOE02cbB84yE0wHCexV53xChO6IVWQI1z3A0Pd+Q0WkleNT0K/a741ApmgVcJNS1bZrFKBEaeDSMFigfQ99hZM7B5h7SuTQ1NRzkFfOGzl53ufqJKlBvR4HnucDM2QJQK/AnG0MOGb5jZS0Uqw1iNgWFrhs7dNMPzmoS1Js6xYUBSKFkypukGm/pykgKojUxwWAANBswiKYiEukUM6f9ozLCETVg6lAreumFJQJUqaDNMZlEfLh1kKVQOoXFPOEVwuastfOrU7BdXoFo4A6JquVGtnqrU91drW2drWUWqBGUKD8QiFZul7QhQ80KcFYDiMTDLfU5aAxJD5cb6AMuA2MUlaRXTVBE5ZYo+d0y6RzMVUQSsGrhRhSFGNSdnaU2ajh8sIo0s3sRAus8uPA1DZEIwSFQ80xX6qMqmoHpVnQK/DelMp4aysIxUg/kWnxAQueqC9gyqwY0IhWkfs40iFlasCzaDjSLQXcTAtAb49DxL+Az4qDISQIUuUuOmOGEfrh6t6j4B790XOapvWRlQ746ZPzqe4jIRVedkgALL0zktz8AfkkkorzBwWrBoVOqUnsKV015Iva/zQMWQ2R7NVLrLREXm2fFhiVkrAak+fN+mykk62bAVQhOaQtamRSxDVo+pSzk8unafZ6gj2QfgGGh9h52XJC2SxXCQzZIlgkygQ6StlXp4B0eE0yTVMxYCi1InEEgRCP2jlOQWUYdhsSseWKyEOL5K4AocRwZbvbFEBMKaQFYMygA0n774P+wDkbJ6x2+k+1C/4WEQdQV4zirABM8TB6QmxbLI9mHhoOtnZI9p9JU4zDCzxUvHAdDINMl0sljUlS7eSB6Ap4kU9uTk6B9lrYsIOus4AbrFOM0ScmsJXGyCiQ9NASVHP4X5P1GG04/aBtZ2xibZ/RgZFHrIOablY9Aj/gHajTjXwHGTzSXgPm73opmLJdYezvZRWsOryLOtxWwDr4kMgLbBFxPMnJghEL8xIKpk86ZtCdhwLilNH1Y5MnZHpKgaYDWwCAL7aMzQyfxOx8qozhYwyuhImG0amUnJMx6BqSZAsJd1KmjIeqKEuBtbbpOTKZOQH+U3wpGyWSBkwCjYTZVxXyGQuoVtgIBNfpdicsHdkBbL6Mdk7S1i7KdkjgEUoJJItlyTS/xOoW5wDH0L4qA7d6YKAg2VOh48ic+gAxF4nGCyGM7arRmHd9VTgOgwBnMP0Euz3yXQuz+PeUDbweAPX5nQbN0NMRhJiVpVh86obsxIMAXFgxGkY20KhONnTbu5G6ETyfs85QbFoEiFIruCKJU+IkbLezQ1I7xh/kuAXXo2ucJclwnIRtFrFdLfQCZFMdPTtM92Hq0oR5UjP6rDNEWf0XIUR4N3fQj+B2JJkopBIlPASFjhkToGZEvbico52KWjlGV1L5pUZGZcxCkiEgeoCG1HUS/jyWMmaCbYIQIoNZDNR1HqJlmySokFxwdZJKRdxY6XDfoY2nskqWavMKBrVTbDKoscx7marPDsr5zDKtwcmLtJw06o4JxKyMQtsKEIfQ+diz2F183RpJRVxEzkLSwEYNVJWthdJkEa0mXO24Vs34pYNj6lhr25Kc1iTvZVIOKtUWE6wmfGILJecoUbqpfsB0DrtKTqTXUJqpuDOTlzI40t7bLe7jsK9q+9EIwvMxWEKCAvVDi+AJ84AhtLhAVUCkVuG7jmhYnqNmBngRp5sCWDQSNkjskWXc5RM2AHLdHtEtuTp1vYEBJ2oF9LiwEHoA7lkeumRVKgQJE2hq1AsPUeP6t1NlO+UybN9Y1rT2ScRDeCcuw31T4ZsZPuIGMnukYG9zEOqcXqCtBy0QiJaA/UdqgparXMM4G3GVAlPxFRxII8HTSad6WDqM0nf08Wj0zMyy0TOi6aIVkYwPX5J2McwU8DcbCEhjsIOjp4y0cLd8wdoIBSpEikgZ2U6OYlwznftPY/pFxGy5C+wzqS6bRFRmcF+JvsdMqvT/sjj+xOL7btmWSFQ7RwsusgwSfKrPUcvkwOfrPNqB0sbPExPcQ/zW0DoiiQ8zMr0UJIocDo+6VEC2f15yWWLCdxd4UotrHaqRfvsfhhB1YjU01dOZOOWnJ4ewd6Uc0R94g6fHEWZrlDlVTympjMHrpcknFBg4sIXSabuDPayM0HDvgI0k+FO5qQEe9VPDlNNVwj5BQPRpUXJdzaWo+d/eokdII0OwMptCsqGveqokjV0uv6iu0p6TsCYQ48L2JxPN7eOMkm4q0rv7gF4MNqb7E93iaM4YJLkkFHslXDdWBEnQDro9KLDgIJxgqLpETi6O+lj/UFeewSkjezzuQW//+Tc3oFXCGvJiwgY9WXTHvGevYUyge1PwF8pR/qvFc95JnRUdkih7l2+Q3vZ4R0eLgNje7ScoSs5tkx1xdsrb84yelSfwYntgMXe6MBgtxfQRVgjIKEKTJmgSYBaVgXbOMzIeTKQndWf2yLL5YmHm6PcuZc4KefLxowCwpQLLCJENDiDYiSqvWfw5Q9/uEHWcTiO6CQwrxtk9aLj21Ic7JLniE3UvUduSXwLADN3ToSNfE43Wny9WZRhb0Gayk7uXSLwBEXG5QNdcGky7SSghh3EATVFGbYNnvoRR8kzxlL9ppBZRl7IysBaskbRcnTXz1HTQ06YHTaJsLux6GoDZlNcw7A+AFIrBGzqaq7Fw3KOy0yF9OPbiT7SOSZKnmxLYVq8Btc2EyBdlMYRaQYH8iE8GtIstghmekjSpJzU4qZBe8bGp8U52DvgKz2iLCzWVy1c95K3r3G0HItjGGuC4G0CeRObQMoSdHE4eGBkJDE42TM6kDi/s2sXpLeluhLnp9K721K7d3emu3e1J+gL5sTg4URbx+hEAo9HEyPjhxIjw/uHphM4AyTw/6GBnumhgcmJyfHxwcSh4eTgMIQQDPUnDg73D4wnBkeGJxKwKUr0TE4P9x0Y6Zke6E/QN9aJC67uTeH74ehvH3/88WWZnNnGk9EM9lqkPpqoFRt3rlgC/o+mWto6U9HfQO3LwkfbvIY5tD3KZCKN65gJbHwjBpswaMZgMwRN8S0kPAUBW2mVqVQKC2jcOPHo+d+IVR4ZaopvQ1s/fGseOxWCjdPSApkp9+t6zpwovmbo+X2PDGUjzOQ2th0ezlvSvlcsCpH4aVjv6RBgTBDceHTjsRjuxTLkQDOWyaIZdX2kBu7ZBV1LzCEI0+JnRqidY3TT8TEG9JPVtZIieQLPYgTWNz/JTQ3rCHeIoXE0g68cGzNztNj6SCS1PKIZgxrjZ6Npqt2FKO3LMGhEnLrDRVUg/XYOBLen916xUFRFRtflO6C9O2D5ntVxDXL5jrKVT6a7dlyxb2PT3h7s5vH8FLCeGK/BYIHGE2tAfNEAKRYuZMZnbsRlMZSqmXsWzNzlO5id/vz8fMt8ewvs51vbUql06+HRkalsQS5KO5zMytKZk2Tyhs3FDqBJFPfWRg7JC7lJsggV7cE3rpfvAPr2mCTfDrHVzgTF7IMpA+awqSPlva0YJVW1LqOu40DXjK7jfLxjH2zv5b2tJAtP4pBkDhTyoLZWAYnkvVU4jaOwBVG0SXkGZHB1UzqAS8aj8qRswhoBFqKrgNoqXe8d8lMFvVSCcqdA1YBqxY2EYbYcghUSrNLxFo4NzLRlBjSkKrcKmlatI1batvY13LaONdy2zjXctq413LZdJ79toTN3CK3OSmoVdMMxaHRY2sG6d8rSNXnNafSwtq0VjR7WtrWi0cPatlY0eljb1opGD2vbKtDox0NNIrRXt9acivS3a62oR3+71opq9LdrrahFf7vWikr0t2sNqcNpfXht6kNPw9aSQvQ0bC1pRE/D1pJK9DRsLelET8OOTSnubV3WS4R90S3H4oLhAnytl1EV7QjxOTMtzW7OyIT6Xtm0xvN52diembEf0bCnWC4SvzKxzLySswoNmQLzFpOTS1ZhayZbNvC1wMCCXCxZ09ICOospoWMUvWxywDPtjBOwxAZ+Troea8yzHISQxOaM3CtVnPc+/dtIfAqtAlzgBhfo5CgotPNkAw0ZNtNi8H2cUaHOXQiAEJWt0FrQi5ikYiotA+goG5rZk81CW+XcKTwQTeoU7TQexHnqOYOH26RAb+Z6KxRlAv+pxAdZTy6HhmW0SrT6RxNSdNkwKi1QuvCd5Vi5OCMb8cxNZUmzFKuyOWNCsdP6iM4MDjYSANaD/nJOc2JQPLvHhfAzM9TIVs4R9o7PgdTl5B5iRbsB0iQjW+jVIWM8kzP0EhYiOn1MxRwvNiB8GN//z8FIOs/t8Co5GtFrjmJYlU0ZA3oA2kZfJm1BT0bSrIxWqhQiRCLogoa4niEOacgvIlA/NJAAyUL00qXlvx8q0Uz2QvaJzn+SvZqc5OofOE6+QAKD/4S9ln7yt6Ax3tjYGI0K62t4993PBvZfJVdYnvs2ZFj/0hijjk6zkV0IncVtwx9iHRURUtGfPP7448tVOrEMMf2xTY9ircvA562P7F/0x0DEcgsJN0ESfgRFYZvssoV/hsiPGUD4oT/1B1xqfCcGF2JwEQYXR4I/wjdfELsUn+hc3aOqYVlcltsP9WgGRQKIPOeK6CNAxDJGUBUDrBYMWrFhcczRVH/KyR9p1S1vGtCIpjGe5vhbjyZia8JX1qIGR90rpN6xPGpjVkcYrUfjubXlFmtR5vUeazMcLrYzqSNcRMvAJ6NbqEV5VYNNKRLrcKSDH4toJvnkdTq1KF9alkOyw51OjjsNZ0bYGETDv/oadLdtAEomgy4INjY3nOUppAZz1KC5JiluFy3ubLs4dKJVX0MjebNGUtBuDLox2IMTiIBzIrHdtbU0li5c6IcSecFZ88m/oF1UbmoxqA2Q7sjPXpQbpm8EXF6QdcblnBYSWsI4K7SGQZvtSGNDm6ffd61wdiECcAVKUl1Du10i6dkrj1XlEpP1Dk+ZO2tTTQSz04NZg7G1O3iJmTuOtmWM0DMgO3rTbbI9Jg/nPM6T40Q88LiGPiFsE1kNj+dHZQmvKm3PmLKWI8cs0ArndsaWDPP9O4w+zfCm1taMXrbwqE3PHumVC9KcohubbQ/IlVGJHKU0SbnctE6EaT15pKb4p5Bn55QHk7fN6FrZxIYwNw6UXpNZHW9wng4XVb8346aMYlPVkBlVtKst/CstwN/6TLGkneK6QrYPfza7TpOJCJnNmfmCrsqmhO6NgZ4qbovpEQv0JVEnENZH6gTi+7exURBq0IPjAaaRjl4fXaZP2Y3pGsTC5goilok32cYT7I/4OKlVlIuTtB4uLeIpVs6iI57jvcUm2yL9uwK5+kF3aj2o4ZwLP7HeSG3OI/tQtHDTacLfT8FObUUCWmXPRggjl2JwK2g/0r3jIM7BV37jlN23v+SRofh+pgSbuN3lMMbJrZgorgFWtILAyycNGXTtMcyrPPfIN0Zcs4OKw9vNqG6oJspIRTzWHjRkeQN6Wjekkq7C0GrIEAdr5pYM+ne0jzFwQm3I6NoUqIZNGd1QZhWYJ0hJoJ8AOF0pyVHy5JzaOhMgHs5GyEFTo9AoRGtY2UyxEge0cnFdbeofUeyFkGAfUpB7Q2TqpX48U6lG+8mZienW3s0R/Rj0xzJpDJeOJiGKK7JlruTOZfNXNKORmWm2rOTk3Jg0h5NBDe7+uY8D+N/ZFGFZw+XcSABXMY/+TZmD1Bn95AgsmkDVwzhvyEjkDnfNnwcInarwrYI+P6ztt1uizJL5Z31mBr9qgQsuMw5U40n+cK6BPcUz2YKi5uCRvTVqypjQdMLMrQDJS6BVJnqnbNhpjBJYwQFoYIHeu9e1DRl0m0Oy9FY2ZMitby1HP6MQOr2tz9gXmmGqc18m4GM9urSvr8OpDyc/kGZhmVOWcJIPdDbwZa9gj/FkvkbW2Fj70U0vdf7jSBe2/diw6ZSGu4jYCKoLds02NspHxvjIOB+ZgMigfTU3KfaDBrYk1b6smxAHFbXoxkblHLobGWTeAW3wCHFBELuaL3iSj0wdx1qmuYIFwXeoSqdBIX4Q12ONRFvGD7kTRiR6F2jhlTN8kelauBNKvot9/CV2LUckmYlpHtxEo+7G7V3sOgjW26yAhUXsej8g4wfc4AdIALiCA4guk9nV6850CqVa0iAyKc/IqnizOIQfIbrQZJljMzibLK+QGF68vU2Ylo5QFzVF9NtG3I6RO7rMCxo6n6BXvcNKZx4B8QW/SD4lY7agJ6MSgXJ1JWwqkmiVwPx+EY8eIB3eppwXg11E5CMC/cpSQqR/mV+AhOM8DF/NJyiR1A2gJ2Kn5xjNNrYdN1XDhfkjQTTY+lZYUpY0yiC9QCEaaRk+x2SgfCvjE7mvvJSMzxIZx5EfL2CgRNzzX+F1TBpd+XwtL59FnyBxMrqHyeg+zKdD0OwbrrESRynfo7GbwnIbYUAzDGiFActhwLkw4DwHtJkXeOvSxNZlFcI8HIfxoxg8kzEO5Vq4JcC85/HMe3ZI7S4DG/baygaPdWo6vwucDpFTl+di8DwMno/BLRi8AIMXYvAirOByu6rIMVUVxQOelZ4Q4RkUt0OI4neKpG3+rPh1oYuDwJIPQj67NJy7sMac5wbzOWcPuBRbLB0X2ecE06kHGWrlFJLMXJwRXp0Xgo3evwa03EG60L0omKNsyjCx+desZ4cQWirBgnJaP2Co4iLNmMrqJfmsYAZqPYPa+PxqRBAfM4aMfux2LMYoXVenlVJbMItFU1hjxwMr8UQAZe+AswbedySTwa8kAp8HFVnNkQ0dWf+SI0vYYcHebgVCSQQDpbp2Rwc9vEyQoxxYlK+gatIXRP0Kgr1PjL0Y2zM2PjbwfADh8Wz8JahEXooaGNY96E2S7HLLoEBWSHJwWUIOEF4OQd0zU3/DvpoVNaGGlbYqWIXArX2aBDyVwPcVsVdCcGv3X70WLGneyZ2BTBj0I4En/abmvrEQQsmBFT3mXMXWssMasbgc19TVcIu0GpWH5JlVTuGYPinDlmduNfuCGO2ZYEeVJ49IRbN27Eumw8eLIWfSJ5m0VFXK2lYtZe2rlrKOVUtZ56qlrGvVUrZr1VK2e9VS1r1qKUunVi9pq3cWSK/eaSC9eueB9OqdCNKrdyZIr96pIL1654L06p0M0id7NuiqvrpdvbNB2+pVuW2rV+W2rV6V23ayVW6yrTptq1fntq1endu2enVu28nWuYvs21evzm1fvSvw9tU7HbSf7Omg+rla98keBouQ1rUaaXMslTOHbEvtk0xl56JUjo+txknVpW9M1046iR2LUjg5PLWq5XC8bKmy1SejQ4YT46YEraSXb9SPNtCbM7o2JGk5J2lLxpDJRz+mC4ZsFnQ1t4kYfxIY0tHsRkeUomJtRIcUTupZTqyHfvuHfAWlX7LkA1Z2k5NIMLdm8ENbeO3Drf6UjImv44CFTv3NGUsyZmXramaZvYVc4ZiUSUZyiWMLsclALx6mJRVLANmWUdltDbtkRTa9JtZ1EfanEc2rb1rEVB8IxLKPs0lpI/w2bjxO937xLeuIr8WVE2YhSowBosxMh76Nb4rwUfhNfP80YtHf1zbwtfegZf+rMLEebfmf/Bc9jvX+XJB05/7cbRwfBTT7JHfiXs3MrwgUDUDJx0deY0Pw/6yd97V8XrSkQ+s3YlaHBrVxNKSNowFtHA1n42gwG0dL1zhauMbRsjU+TUw+/Jf8/84xj4i/DW2kmuJv52m9LmBkdi1nZBZHC1LOKk+x23AX34aK3Ya7+TYcjbhWpnG0Mo2jZWkcrUnjN2CAVqNxtPqMo7lbHG0m43JoGz7qtuHeiNfc8L4IZ264P9CYAb4xRW9jnmk35v6I+4uivduKTdhO2ztF7qr4rYq27yVZAsZGkcaobcTSwO5oCGhr91zWIgf4fAZs4IG3MGAdAXal2xD4Agas53O+kAGjPPBFDBjjgFG0ClqpuRNW6V7uWO/abplnLmpxVSc03v/EOFY4YaoFOwutqmKfQWZSgyoz/oAth/WoRtfEJHKsziNCqXfU6IOcWmm4zVZS+KvFrDSon7HYZlsLN9p6oo4Nj6gt/Pj/NXZtK76g/rkI3k9ufm0t1Qmoljc7aupttgb9vE0OQt9uK6gvMEyCfdcxE/pFSujdNRH6UQ+h99qE/gNP6H02oQ/z/Xe/p/9WQGg9jp8nu2ZYdMDUfinSIdwZK1+OuL+GBz1CUYMCDx2HRDq+QqRDQGk+PtO3fVPgOxHv1P29CDd17wxM3RdUn7qbcZg0LiW6hDFfsBnTuNLR8uOIbZ3/Uwx+hsHPMfgXZNQ64YsR96ZCHG8qxPFmQhxvIcTx1kEcbxnE8VZBHG8RxPHWQHw+4jQ0eCvgvyPeWwH/E+FuBWwKMGoDz6hnexn1DzUz6mGPBK2EUb8ngtPwZbsg/NW3LV/CyTYFpXClYoy7mgYy5IZz21zPAU6+XGA7HbbH9W9o/T4JzkX79WFtIKsXi7KRlYmBap+kZssqqWbHXmbC6BbpX/qct3dYA3SI2pX4c5y2d8CSoLLgypH+6uoayQ5VONHX/k/8Tty+My9E3F0s/h694Slky0pErqlJQDVDpqj/i3C7EVQ2ZIr6Q8SdooQfs7GL1+hjf4yEXOf5Uxjwz2HAv4QBHwPggA2cwLvn4k68XqY7NwrFpDiuVoqlspkQp3StYl9wC9yPwxF/2UqKYjfXYtjkNwjeUvFz9/ghd98VOPxE66JVsTty3ltm5DKdQ8GEpEmmrinZMGIgGYaEtODcojsvJgB5X3pi7swZ8iwMSveKHHECQL9/TBrlwKGZJhcjOQKlBK7WKeTr815wbB0051zfPbAku+uShIKTveVKcKeLF0/YdBkTvNMlqnBHcH//mH8W+O1j3N2wDcJid8OEnzKxxwPJ2CbIu4G/+hprDkA2ByBbApBTApCtAci2AORUgDwktHcWi77rt1MjkxD2TEy5IMzlyA+TphFZyUr2rVznCm/JxC+ya2yhRZPFnpxUwm2ZLXuQu3yjkhBHFU1XLShkSDJNWZ1R8QPUEzp0ta7AExswsGAEJEPRAQFdCegzgDGmz0niVFa3FKkltt1uXNU7k/HT3e49w9e9Z7HuJU5+fx7o3p/y3Sv62cj17c9Y3+KRXWyHgBey6Y3l87nnC7jnp3LPO+G53ys3jLWgW8LGL/STMluUgtrqQiipfQUlxS4CxNf7tVRWMuidW3Feycki9dfMPkXvHXfsQ/RMP1FVagsHvcBrx5gCsqO0HXZsWioaTl5OSV0s4I6kBiUlQqDDpL9cZWVDudYk6GfSsxaL8TeAGcR9LBk6+0a8NzNXXuwSaMRGytkkbWFQUhOupLb4JDUluAtF4R8DkvpVXlI7XOHiZPTnTEbfi1m6kBrPZfvYriBodxDUjaAiBdH7krE9wVyXBUF7g6DLAfSKOliJihN8d1XxAdBr4Lwm2QoENA0RYF5/DOmGKRclzU0dhVVsRXLFfRL3s4Ddq+cUmeirYknXIBErkGE5Ok9qQo8eCOor4GoUtVdPH6fKeiULHoDUfkVnoH5DmcMCiZ97qv+myzOOdsRlJHnWciT7oJ4tm6DEDBmV5BB+1xpUoonfZ5c0WYc06gqdvJwRr8IPsTMP+rBumFZUC1qWBcaZVk6choFJRFw8hOO0R5uFifHpul50BhYZ2D97zht5fTptDxVbXyCJidg+t5uStAeCYnqlK6a9PjHtZ2KKa0XhkwEx/TgvpkMBieCk9V+YtL4NVqCxp0HOTT1U+dhz2FUhsJEQ2GgIbCwENh4CmwiBXR0Cm3Rh1aehaZdrB31cO8y4Rvaa7+Z4RPO/i+faM4L1c2z7b3sdfr3ArcP/JxJy6C78PnK89tDxG+22ReKq4N1HawK3j74tIBGvfKz6PtreSyx+hviHSOQY99DoPHpjHbl/7B664UqQcHJO4A7dkBTCyXl+YXi6B/MMG7PCY55lYx4VGKkITXgwW2zMm3nMlI2Jt3nRFyH+F670YPbamM/mMfttzOeg0ETof2Hag3nQxnwej3lYCJGYZluoFj+8vNFTvmqX/wK+fC2sfAE74vie6tzmk8bX8NKoBqTxxkWkEeWgtlOdeVswViyRbyQS2Vypucqjx1zlm3AIvxmDt2BwO3bQzTXX/zd2/eJK638r1vp3GNyBwdsweDsG78DgnRi8C4M7MXg3BndhcDcG78HgvRi8D4P3Y/ABDD6IwYcw+DAG92DwEQw+isHHMLgXg79Hfsean11ze59jt3fzStt7H9b7cQw+gcEnMfgUBp/G4H4MPoPBAxh8FoMHMfgcBg9h8HmkeV3z82qlufkFteYUbrPH7Bf4WeQ1oWP2jWzM9mMhXxJCjmP+IQz4cBjwy2HAr4QBvxoG/BoAXyT4oK4nIw/cXkbSU5Kw8xN7ycg2u3RXk+S2XJqUk+DBXbsqWXGqXIIVGu4mYv8I1PyZ7loutLcrpu0IqKTPQz7AFM1cd3cqcDribj8cRN+xj72z6tN1taQs0JaIfZUZWi4sUofHx54uDhelWUWbxS0KXU9Cw/BcibYfF6Qywe5XTE2uiIMqRuZBWA3ACGz0HE6JU1ZFRYa5DZlNQzPMhXRKVBB+qdkS+zrXS1Xd8sS/6S4evkXUNR4M0jcA3+bV9cUBdX0hvzD6fohIcCujNzE5HcK8P8S85PyjZ9ARn38OA/4oDPjjMOBPwoA/DQP+LAz4cwB2kL2CzeqpkUk85yLLedxCioacV+UFZwtP/FYsJGL/YheX7Bmsvvz8N5vLQvw/CJfxHIq+XfkFz+VTA1zeynP5VyG0c1x+M+MymsrEHnXyjrrt/J8w4K/DgP8bBvxNGPC3YcDfhQF/D8B3gYqAwWtrgCFdnYXIVXpOOoJ/6CZzTF7AsWcEDkwmsRPoG1FyKFUGaaO7ObvfSJ19fCfmoVsTooyf/IKenJNVsUgRiSOwGRlGqCa7J6W0Y2P/5/Tr6CL9+ke3X//s69fH+H4VAv36+F+4fl23Lsgtrl/fwvoVZ8tYPeQ9heT1HFPHolXgsSrwhirwxirweBV4UxX4eoBnRvQibHLZhmlKwlNjw9Gu2DULco4d25Ai7E7IK2ZBrshcnBuaJCfp1dgGqGQb6SVSebJHyyVJ9cGu2rTO6arN67xddco6rqv+31/8XfVffFdtD28u11u3s946jNlPh+yb3eyUuNhTQqFnhELPDIWeFQo9OxR6Tij03FCoaEOTHDTIzB0uMy/wMXMnz8zvBZj5HZ6Zl4TRwLHyrYyVr8JziAQE28MPWWLJRdJaFklrXSQtBWl94WmwEiGHp/6jebIKCBwMp6GgtuUXFGsDvFuF/TKsTERNnof1CWLj+oV6eSQOHquUG/DQyI6Hq2SfqbDz34SPnsA7q3Yg6rNPzDsrzwEwOfXmHiVnJvYAjmIjOGiwDC4ffyLcYfd8knAjidyoejTc5Yr7biLu+KqB2kXsYeJOFumf/4v/9OhzvLjvqy5tnND/HRP6V6DQXwnBqaNKLjkJ7fDKZ0/1pN7qSX3Vk/qrJw1UTxqsnrS/etKQJ2lx7j/N5f6Ij/tjjPukBz4c4P4Hee5PVqWGY/4djPmvROZPI4Z7wMvTf6B60sHqSYeqJx2GpMtDk6q8PQrqmWugiMRyiog9HTBeKfRJJTRMsNUKqI5ZQ5bIu5dZq8D0jCSGlNwihr9bp/jsxbqqHJEXe//F1KarYq4Fqu44ASrGchrkj6O2SPC6xZtENcgzPJ25uAhf74rwDT4RnlnnngcItwdE+M28COeryg8nwm9jInwrinABgg0ThlKUbUFTApAbA5AjAYgKkCt4CG95YXenPXlJRbMMu96AgKLv/5blFRLDr9W8ZCkTjptgOa9YFdFTdl43RHkBPwRIvnvLxJvNg/Tkwa6TiSWr03MQ0YtbbE448fzoGcdFNj2yWCKUE7kqOcyvKk6GK06WT5zmmDiRA+6XBMTpRbw4HfV3MydFb2dS9BqUopsxI33nxeThbwKQZwUgzw5AnhOAPDcAeV4A8vwA5BYHUpVFL3RZ9GIfi17KWERek5UDLDJ5Ft3qr5tj0TsYi16NLMIl6hlTJTmrgKj1lEoq+8iQTfJtS6S/eon01yyR/tol0v92ifTXLZH++iXS38ClJ7n0qh30JreD3uLroLeyDsJjUUEKdFCG76C3L04X113vZN2FPRF7JwRnufYysI3rwzlLmXM03ruWzHHnkjnevWSOu5bMcfeSOd6zZI73LpnjfZDjHDcH2Ujbeap24QfcLvyQrwvvYV1Izq9HA114Fd+F9y5FHdeJ72KdiPvf2H0QNON0yCyf0Bgg9vEw4CfCgJ8MA34qDPjpMOD9YcDPhAEfsIFJFxjk5oMuNx/ycfMLjJv4AkHoDuypd/HcfDiEAo6BdzIG4tdLYl+BYNMofjfIsa+IfTUE9rUQ2D+GwL4eAvtGCOybIbBHQmDfcmBJGxbk27ddvn3Xx7fvM76R1KcG+HY+z7cfBevn2PZuxranYdafYFb7GJYNoJ+GwH4WAvt5COxfeBi15or9awjs30Jg/w6wO6OOaaBmzkgzlcUW2glnpz+tH1HwJBY/JAWrJTxF9a12PWvgIpkF6bNSlGALZVrkGvVRPNM7MDWaoAflUgmErowrHFPPW7Rg90SP4pdVy5CSZH/O1VDSDQArVpLuPHg6ZNuyR7TKM/7F09VlRbaQJwewWLJwE0cBzWBnAEn+8GAkadKvWXClD9CuIJ+FOKI7n4foTKH1JX1nRJ9NYjLX7oOL6ZYO+40Qi+8Oxu38NDctia9hV8p+kvMi+e4HjeWy7eTlDkEgnWCntKc49DSN0FpJipjvcBPFtja7jF2YxgqikDaunLaUp6VcZBdXQXo3iVB+iW0dPhQx34ZN5iEAiP2HLcD4skStMqv8wh3Pv/SN51+x8YzXHYQtgfHczI/nXwcHGjee72LjGW8ax37jZB11xuRvQ2C/C4H9PgT2fzyMjdM/hMD+GAL7E8C+WOcbvgeVOdjGGpzhbl/So6qcQ4EZqaJrMBhmmImdVYCpFPcumFllFmmDIgz4PjpaaYpkF2MPgLHjMAD+KvCxPzsCP1pd4B9zBT5S5xX4dXVU4Ndj6l/+7Bf4P/2ZE/hYXUASOYG/mwn8VZi1sQ6/QoNfkGESG/cDmvyA9X7ABj9gIwBO4wDila7pY2wTJH5eIFtv3JfzH7HxnVhTS0n6gh+fq73iP088JBMr6Qo7ruH0OXfwS3buzsyFxwIGzDfsaMosZwuiZPpOpwJHEtRQGksi5/LN0JZccAb0H0Xzkw4/d3J0jpcs2K6oIdMpObiKbbZ5XHUJfkqdIzvbfLKzncnOBkz9z4Ds/DsvO2f4OpMTnPcwwcG9e+wsyLeFO8FmXX92OPiccPC54WARwM0+cOy8MOCOMOD5YcALbCA75UdgkIk7XSZe5GPiJYyJGzH1n/7s38c8wjOxJbRdHCvfy1hZxve4Kcjd5J4sxNK+eJsv3o5d5MbFWAcA+vijCTqyQr4jtsT7qU4oqG35BcW66vD9FPkUmQRDUoMVHjF24d5OOSd04cVzl6i85Tsjzz4Z1qEkw3OuzA7yfG+5YruAqO+egMNjfkQvej3Bc0kj4dFFoAvopQu2/KVTuI2mOQl0ViGHg7sdqagiy3tcWd7rk+V9TJaJHepnAwrhM7ws93qljxPi9zEhRiPSWD/NZmSZkA744oO++H4qxHZcjA35Mgz74k/zxa/yxUeceBV+jLn8mPDxY5LxA83qhPcH+PFenh8HvfVy/Hg/4weaz8YOQ7bTq51Gxa5ZNPXpi6ZeC6lPqZYqxp6xKPJ1i6Zev2hqZtHUG7jUwLFfsDdm3N7I+Xojz3oDbTqFNwZ64/V8b9y4GE1c33yA9Q1aYcRUQDplygLdIRk5991JrFgFrlWB61XgpSrwm6rAjSpwswrc4uHu66Ygk+dcJi/4mHyUMXkLpr4gwOTn80x+VjgdHHs/yNiLBiax59ThQSzL3spulpBzKPKGIfbcJdKft0T685dIv2WJ9Bcskf7CJdJftET6i/n0JLvEQw79SHqwm17qdtPLfd10K+smNAMWbgp0k85306sXp4vrrg+x7kIrodhrAW2b+yrRfQsU+9uqKa+rmvL6qilvqJryxqopb6qa8uaqKW/xpCTd92dBxr/VZfwdPsa/nTF+K6ZeF2D8tTzj76xGC8fyDzOWo2lX7C5A2Bry9jZ2d7WE91RLeG+1hPdVS3h/tYQPVEv4YLWED3kSFtNE97ic/qiP0/cyTm/D1OEAp/fznP5EFUo4Rt/DGH0eMvpTuBAn9/TQOk/JW5TuT4eD7w8HfyYc/EA4+LPh4AfDwZ8LBz9E2glgZscICVUY+wWXsV/yMfZhxthTMbUzwNh2nrFfC6WDY+tHGFtLuLj5uk0e3Qq7dH+jWsI3qyU8QsZOMEGMfasayj9VS/h2tYTvVEv4rsNnaQk+f9/l8w99fP4R4/N2TD0vwOdzeT7/rAolHKc/yjiNpqGxf4H8p3I7SU7P/Wv1pH+rnvTv1ZP+o3rSf1ZP+kX1pP+qnvRLO4ltxxfT0r9yWf+oj/W/Zqw/DVM3Bli/nmf976pSwzH/Y4z5aBMb+z+UzTArvdgfqqb8sWrKn6qm/Llqyl+qpjxWNeXxqiloCrItzNIxyPN19Q7P6+u9PI/VU56fjql/+JOf57//E8fzpvoqtHAsv5exHM09YhvqQ85uNoYBN4UBm8OAm8OAW8KAp4QBt4YBt9XXcpy03eXi6T4unsG4+BRM/dc/+Y+Tfs5z8ZwQCjgG/j1jIFrQxMT60CO188LBO8LB54eDLwgHPzUcvDMcfGE4+CIbzDE07JjzEpelCR9LWxhLyb3cbwRY+o88S9tCqeCYeh9jKl6yi3VgpV0dnSI6LKjEOr3RLm90F5KFURLb7SaO918T6/ZG93ijl0H0O+s8rhTaBsWh9v4EBsPDCdHj8sG+0WKfOl3K4oX2XFJRAv4v0Cm2C6QxT3nO+dZEgb6qpX/J29t5iRyUGbrITqzhMS+bJmdRZx+gDSVN4gwrQY723BpJBEugYPKKlt7ZSYiDqrzQp6u6wVVWUGYLooyn9t5GkytC+DapKM9KJWVBVkUTug9fVvVNJtNt7dA25hhiqK9fbNtNejcR28s6Jom8DkrWPleyriSShR46qLe73nrOtPvTAZX3SV6yBj3SwEnUx5lEoZfU2JAtJPSsyxN7mid2lSc24sQGxqZio57YmCc2DrGCx6sPO0kdouywX4N5us7H6cDbC+7NBpFK9kI1EZuweYt1B3k76fJ22sfbg/Wc4fbdAd6+m+ft03lecKz9BGPtA5jpGc74AwmPXeeJXe+JZTyxG5xYT99VMckTm/HEshC7oy6Mt97BNdlzKCG2D9p/J3kvKRRov/3qmRgX+3TDoDfaEuIw+Ra2hSdahj6HGYxsQbEgmZhTDPf0uHXN68aRAnpcAfEfPJhELcFVc1CcYoPRfnLuxOXLqirmDYgx71J0XOJQzOJQdPUAltvenYAx1953qN/+S6rqG0zi61XyZ3SKPGBO8gfjoMDwLN9QxafLimk6592dqZTIzC9srXNQyVqgAHi+2m30CKkEGkefLcteKLkYxkNiOWe8Q6cFZTLvymTBJ5M31nOW2H8bkMnX8DKp8ULEyeQnmUx+FjOhe+hmzMS5k4ndFAY0woAmADcD8ME7eKjlZO3rG5iaGp8cHpiKlcOAc2HAeQC+p85x5+Nlu06NAAam8e5rSZZzqoJ+Y9vEeani2ggwPStmqX8eFzBrKCVABAVfsm2BbBzbJZDfjievSmaB/XFz5xVDnof/ICQzqp3LidDZiXjvYdVksUKGLR5RLPRcp8qSCQXoRi7hzgwkd0kvZwvs2SwQt7dFaVY6qmgylSf8uAOSYfsvyhPn1glxTpHn88SJEAwSQ2e4Q6I98c1QH0ZMVUq5G8v4SleVfQklhmpTbHIvpgjr5g3FtFgktsB6Mcn1YlCsj7pifbNPrJ9Vz1mEPzcg1s/mxfp5IXLISfenmHR/A/PeQobA4S664nmBJ/ZCT+xFntiLnRgufl7iib3UE3sZxE6nEipTESUqCI1bYKC/nOWtMrHf6nLkVT6OvLqeMwAvBjhyhOfI63jaOVZ8mrHic5jpDXYmerDpib3JE3uzJ/YWJ4Zz9+2e2Fs9sb+D2FWho/bpsnaUvTRVJU2T7PeOfYPDbMrvGxxgT2ywwKQRu8PmXvjU/XaXe+/0ce/Oes46+5oA9w7x3HsP31yOe/cz7j2Emd7nsBgn5Pd7Yh/wxD7oiX3IieH0/GFP7B5P7CMQO9fDPTXg9TD2UUeeQieOe12O3OfjyCfqOWPngQBH+niO3M83gePIZxhHHsRMD0CmTZjpoKNxYp8NgT0YAvtcCOwhBzY8cGhweKx/YDL2+RDYF0JgXwRYa6jkTYCawovYVCGicrQjsS+xgpJuQUGOPuxy9Cs+jn6tnjN4Tgc42spz9JvBBnN8fYDxFf1nx75F1Btk5SfUfwoDfjsM+B0yIR/u8k7I33WyctPs98KA3w8D/gCAd3qWlTi1zYP6l0sltKfA7ykl/FFnrnamtsDM6pkmA3M2mVJpzqICW4ISdmaC5TdLKk6i5A+xTWLFYf0FXc/x064Fk2OrSY40nfnbkIu6hZOvZhm66p12qcEhbUXoFJzV57AMiGtKvuJWSRA5hlS0LJve7fk5q5eBYjRBUSVct8yDMGA0a0jaEaSKtt5dHcyULQs1p3e1QBwPlEtofZInVpcAKClyFrTmD1kPLj4Z/8gV7J/4BPtn9ZxF+tkBwT6TF+x/C5FBTrI/yySbfCrhP6ha2cUObj2xX3hi/+WJ/dKJ4Szz/zyxX3li/w2x8+0dz7R0pFxE1tB5ZWq0zwHFHmVoVWaWX7vM+Y2POb+r58x74wHmNPDM+SPfDI4rDzKuvA8z/ZlycJdnFP8lDPhYGPBxOt53ecc7LhsCWYUw4LowYB0A220+4qKCDiOzUMYRyoRbL+OzBxarZ6UludKC7I1FHfY2Rr3sbYpyxqS//aOfvf/7R469m0JI57j8Ocblb2LezZD3VHtP63UyuqV60inVk7ZWT9oWtfdduEA81RPb7omd5omdHl30FOgMl2tn+bh2TpQzo/xpgGs/5rm2oyrhHO8eYrx7Bx7cXhBlC2538jJjTw0D7gwDXhgGvCgMeHEY8JIw4KVhwAQDJjlgkI0tLhtTPja2RTm3IF/9o/+Y9ss8G7tCKOAY+HnGQLz6F9ttCypPbXcYcE8Y8LIw4N4w4OVhwH1hwCvCgFfao3dRBva6DOz3MXCQZ+DHAwz8e56BTwuhgHOmaDuWW9yTJHoGcz1JonMw4pVuJMp5kkQXYUGvdP/mwfwPG3Ocx/xFKOYfPZh/tjEneczHQjHRvY+LiR5+COYBHhP9/AQxd3gwL7AxD/OYO0MxuzyYu23Ma3nMPaGYT/NgjtiY1/OYY6GY13swb7AxJR5zJhTT8GBaNmaOx5wLxXyhB/PFNuZslPOl+tJQzDd5MN9iY97I1/nWUMwPeDA/ZGMWecx7QjEf9GA+ZGOWeMwvhGJ+24P5XRvT5DG/H4r5Cw/mL23MOR7zV6GYj3kwcSRS77U8Jt4CCWKi6b+Luc3GvJnH3B6KudODeZGN+ewo1QDkWOSSUMw9Hsy9Nubz+Dr3hWKOeTAnbMwX8JiToZgzHsycjfliHjMfijnnwVywMV/GYx4NxXypB/PlNuatPOatoZhv9WDeYWPexmO+PRTzHg/mR23M1/KY94ZifsGD+SUb8/U85sOhmN/3YP7QxnwTj/mjUMxfeTAftTFv5zF/HYqJRgYuJtoZEMw7eEy0Nghibvdgnm5jvoPHPCMU8xIPZsLGvJPHbAnF3OfBvNLGvJvH7A3FnPRgTtuY7+MxD4Zi5j2YBRvzgzzmjaGYRz2YN9uY9/CYzwrFvNWD+Sob82M85qtDMd/uwXynjXkfj3lnKOa9Hsz7bMxP8pifCMV82IP5FRvzfh7za6GYP/Jg/sTG/CyP+bNQzF97MH9jYz7EY/4uFBP3ZC4mbssI5hd5TNycBTHP8GCeZWM+zGOeE4rZ4sFM2Zhf5THbQjF7PZj9NubXeczBMMxmXBrW5g15vOackzXnPFBzzsM157zWzumsikMwSM7ra84p1ZwzVzOd9lKs0X7RY6c231hzGcWac5ZqboFZc865mmuv1Jzz5ppzPtufs96Xw/41P6/mMl9Qc84X15zzZXZOz07Nh0Vy3lpzmbfV3EevrbnM19dc5ptqLvP2mnPeUXPt76g5550157y7ZjrfV3POD9Zc+z01l/mxmnPeV3POT9ac8/6aW/TZmst8qOacX6w558M15/xqzTm/XusoXn/Oph60HOjJ5ab1PsmwZnO5deuac9vX5ZtPFTaTL6zvES9IJ+inzHPr6pq3rzun4aBiKjOqXKDZ4lx6PcOtu7D1kGQYkmZVRvWcpOI5UYtklhauKNFPkw7nLu9u79jVFiXheiAAfUZkgQRayblRWno8QTIkJwcGD0z2Dk8NDfQnx4avGh9L9u/qTCX7k3h5Hc9cSemRQsGySntaW6Wj6a723bs6Wub0lqIpZ7VciyZbremO9mx3vjPdmc3nO3K7u6SZrnyqu6s7L0nZ9l3dba3oUF82W9n3U01ad2dqAapqS3V3dHSmOtO7Wm4sze7gCBIJQSISJBKCyJGwuDzKaygw0B/LqoF2bP06AcIm4GwDfge0rSM5Nbx/tCfZnhodTQ6mkx3J/r5kX7J/LDk63Dc5DvF28p6FEikfd/YCAZS9u9O7utu7UsjbCwlJIpIkDqZbOsT+PrFP7B8TCUliR2s7eWMkrqABtRadAzY1b68/t67t/bBVwaHzODv7xB96BY/IJ+Rr11GsM1nzF3/xAySXIIUZNs7GpKLclCkB26cVS5W3ZMyCblj9spk1FOK4cnNG1bVZDhDLkPvvzRn0Yp+XiG2gMdy/IWPIlqSoRAY3ZLJl09KL0/KCleYjbXykPZ45IlfmdSNnNmTmZWW2YLHPdjcCeUpW0WbXZ7L00ymKbOJ3heHZIDdtp2QL38Wap4UBDxfVzRmZfjEtN06oNrf6AZDp9IxkmsqsJuecLyHvN/RyyVyfkWyA2ZQZRC/xfbppbcigiXBBx68Q5/wfJ97KxydlCWFPyaC3ELRVVo4SCq8uyyapfXtmVDayBWCkYqKJFuufxkwvMne4vzFjSQt9ek7emMECQf+RSqIZfJXclFHMflmVoaL1wPY5xSTfWG7KzOFnMisHDLUuo+RiGU0H+jdgW6RsoYjfudvuFYteyZQvhaznhYHNcgkdYpljWMqFi+XocSt4Slg+Qse5YSkcaQL+6iIY1tfX19UL9UJdHX5uuY7OTXg4iXNTXd0J/u5y9KKlh9chIr047Gv4vjx+QRsHYh9+jpCY3iLivpoH8QQdG8EiPiEwNrhgs2W/rMmA0DKimBZyanlfK0qIJ0SNXXfdJUvTNWUBYb0Seg6M7K6ZWwPeYY/Iz6qBTU+gQH3u+HST/yPuByW1LJ+w/oo2Nvs+Pn7iv0/+lePKyIlwVX3COCo0bmwUGhvvOT6NQq19wki/7/jQ7M4jJ4zyRkH4K8NPJMOJ5S7O47UutfFVTCRdY+a+9lR/X//Y6OA0nnnXXgdugkf2ltV9e1Vln3tFd+PeVogjjGD3MM+iLnjKtook7j4ZvBXKieGWOkqqb4p1dOJuLIZb7u9uO161YHbdwM+1J/s4kGKYFm0rerkaRdNKcVAvG+J0QYGFtpsTb0TIRVi/SWiWSb+IhJ2dEHsMS1TREpJsx8SrmXN7FxWKMkUVTRmTtrNVdB5HHbUQm8pZ/B6yqM9r6CZfz4tFxTBA70Pxjs8tdM2lk/VjGRetuL2XF0p4N0fLyiQ1XyZfGyefxMPWk3thegnNSYlzL9LKGQNXXkh4C+epD6udV1RVlLUb9QpzAyaVLSVfVsUZEM2CKOfzMHAgSbJEXVMrouRvErMx1ai7f3J1HgvifPoDllmQjBJQRR3pASq5xYZNwNZkYYlOPf5JaJssc372W8QDpizSz6snsVWO41rScxKWjGcvYg5qKyAj8oqskmMQundDnpc1jdibMse19Lvp5M6HpZcgHVmgKnkZmQipUG5W1U3KXk91xLMZNNJXmfNJQuCNljOzgENKMTWpRIps4cTCy35kHNehvs6iJXtkBro7S9mK4sA++kH8Sco3lZU5CTIRb77AZpX65wUyu9DTJdocQ4MCws4uxrS4JO6d2TcVzlbK83Ah2bO3dWYfL/4yE5WcAsKMbCMNCBNKsSgdkdH22CRHYtW71Ve/W5tHWpNiWZPmYJtNrvWgPbIqalCYomHJXnZCgcFh5+VFj4l9RSSXeL8YUGWyFxP70JIZRJf1Qh/7HGW/jNvlADsUerMIqIDFHWoUC6VEQ7d8Tvkkg0zLpx71ID2nlwlTwrMl0DXmvAwiLKG8ONhFvLsEjMObuklDzhtAGjprRzuzBXqBF3rGLDF37rOwL2edQ+72SppelFT8ynpJMiz04MVlhtFVVDSlqByV0UTdoA48Zwuw+4eB1iKSvkcac7KqoMN4JEwvW4T3OBTpF/6AOhVFAMsjOiM3KxNFiBHqbXO+oEDTWWUEEwcg3psugL6AJWhWlGZkg55sQMU6WofjVU0kF6YBuja1bHosImdSbk7SLCzeU1fWueSJxr7Ey6eluAQFxITecBKpwT3msAsALpNvftqYtt6jA43ocK1AtB/Ri1QtI89A+UE2QzZhwSAqtDsXLAM6VHVmH9YQVMgOdyn99vdVSJqBuoDlAzJwuDszFr3n6RXyQQlmxKkiKLGC2IOuxsnlb58QDzuii02W3OsORZxyOT6zT794JwHQT9kjRFJuIq7A0aO5c2GBnxPIN2Eoh3LiHGh+dm3US/EoMElJjkgVYDQZl306octHM3ELu/P89u7LQJSBlWjtWySoWZpftFCrkzFjyDhjmCEyTSJsRqHSDO0qQlF4Gxd4OyOpSC76XMdbwYjh6RQv6dDX0FTkyRS9eoiqQzcVSxYvmp7qu9jXBKJYgDYy7nS8cKkXi2Q2LpsALelqJSsZM8BmXFxDAWIBdAEOY2CNjAduShYFA9ugEsG0KAEgf4aiHSFXRzBRsQimxFRAHuSRLTpAgePEDaoAHecWlCze/jdhXqT1JG3Zo6sBWGdbRpkOJaJSbKrp53qxsRpqUB9XKiUyTNwZsuJnBPviBDTSkECDJMRDco4olJ1SsXSZODCHpeJ1MTrTJ8RBWKirFW7FieYS1GgihhcY3imQ1WeiPZXA9W8CJ6YE/j8/lU7tH5iGVTk87u5Mt8EaHf53JNzFc6K/L9E/lhgdnpwcnxwZmJpKDI/0JWDJPJGYgvXy+GSi/3CCrDwTQ1Ojib7xsemB0YnxyZ7JaxJkaZ0YHD8wmZgeGp7sn0qMJmGhncD/oxjgijtxQU9HKgXr4uhvH3/88WUdsNlHxdEMrhsj9dFErdgHIL995z+aamnrTEV/A7UvCx9PIhvm8KQlk6HWDoTr6J43jn5N48QqBc1XmuJbSEiMV7bSKlOpFBbQuPHRvm8op1ceGWqKE/9jZbQkR4dZG6elBTJs9ut6zvz6f1+ycO3ljwyhyTV5wRBDZ087a3uh8agqROLooSiOLnMwJghuPLrxWM4qYxl8eVeJZbJ4J6o+UgML7YKuJTtAwrn4mey1Y3TT8Tn/9JPVtZIieQLPYgTWNz/JT1frCHfIu5VoxoTFTWOGfe0Gdtep5RHNGNQYP5u9LSZdiCK/jDNcxKk7XFQF0m/nQHB7eu8VC0VVZHRdvgPauwNUcVZHNXj5jrKVT6a7dlyxb2PT3h7s5vH8FLCenNfBiIHGkwPQCUkxIMXCO0vjMzfieglK1cw9C2bu8h3s/eT8/HzLfHuLbsy2tqVS6dbDoyNT2YJclHY4mZWlMyfJp5phbtwBNIni3trIIXkhN0kWoaI9VqUkX74D6Ntjknw7xFY7ExSzbwImSM2aOlLe24pRUlXrMuo6DnTN6LoqS9qOfTD3yXtbSRaexCHJHCjkQXetAhLzsDisQuMobJwUbVKeARlc3ZQOWMQR/6RswhIM1mWrgNoqXe8d8ugSHZfNU6BqJnC1iDd/Wg4psBuYUfolS7KBmbbMgIZU5VZB06p1xErb1r6G29axhtvWuYbb1rWG27br5LctdOYOodVZSa2CbjgGjQ5LO1j3Tlmw2V1zGj2sbWtFo4e1ba1o9LC2rRWNHta2taLRw9q2CjT68VCTCO3VrTWnIv3tWivq0d+utaIa/e1aK2rR3661ohL97VpD6nBaH16b+tDTsLWkED0NW0sa0dOwtaQSPQ1bSzrR07BjU4p7W5f1EmFfdMuxWJ1fgO/2MqqiHSHXbKal2c0ZmVDfK5vWONoUbM/M2I+jaBRSLpKrNLHMvJKzCg2ZArsgQ+yGtmayZQNfCwwsyMWSNS0t4P2YEt4F0csmBzzTzjgBS2zg56R7Scc8y0EISWzOyL1SxXnv07+NxKfwy5QucIMLdHIUFNp5sjEH5WymxeD7OKNC77MQACEqW6G1TOimJamYSssAOsqGZqLrt5Il507hgYcUq6Bop/Eg7nLSGTzcJgV6M9dboSgT+E8dKEI7e3I5A/qNVtmvmCXJyhbQSn1UWqB04TvLsXJxRjbimZvKkmYpVmVzxoRip/URnX7yz9xIAFgPXhE6zYlB8Qo1EEH4mRl0IozmgIS943MgdTmZOrfbAGmSkS306pAxnskZegkLEZ0+pmKOtisIH9Ys4C2MpPPcDq+SoxEvCimGVdmUMdBYrCjTl0lb8PKWNEvMqShEiETw1g25bUPu4JBfRKBXbyABkoXopUvLfz9UQtyA4ou9Jzr/Sb7IcZKrf+A4XX8IDP4T9lr6yd+CxnhjY2M0Kqyv4d13PxvYf5VcYXk3VpFh/UtjjDo6zUZ2IXQWt61/iJUUeqX5yeOPP75cpRPLEPsf2/4o1roMfN4Eyf5FfwxELLeQcDsk4UdQ1E+Yf1AC+GeI/JgBhB/6U3/ApcZ3YnAhBhdhcHEk+PO7iyTGZpfiE52re1Q1LIvLcvuhHm2hSACR51wRfQSIWMYIqmKF1YJBKzYsjjma6k85+SOtuuVNAxrRNMbTHH/r0U5sTVwPXNTgqHuF1DuWR20R+iE44ikHLejW1k3ARZnXe6zNcLjYzqSOcBEtA5+MN+EW5VUNhqVIrMORDn4sooA9ee/ZLcqXluWQ7HCnk+NOw5lkAolQV231Nehu2wCUTAZdEGxsbjjLU0gN5qhBc01S3C5a3Nl2ccQrbA2N5M0aSUHo1jnejcEenEAEnBOJAa+tpcm32C70Q4m84Kz55F/QLio3tRjUBkh35GdvhHoawqiAywuyzric00JCSxhnhdYwaLMdaWxos3sGh2z9rhXOLkQArsDgSgx6MOjFoA+DfgwGMBjEYD8GQxgMY/A0DK7CYASDUQzGMBjHYAKDqzGYxGAKg2kMDmBwENnSbjeBiNKVx6rjiaF8h6fMnbXpQoLZ6cGswbrb1RbEuB6H9zJUAn46Ez2WNNleaYZzHgc1cSKPeD5EnxC2iSy/x/OjsoSXNbZnTFnLkXMdaIWSV+jpzJYM868yrOEFChjqWzN62cKzPT17pFcuSHOKbmy2vcxURiVydtMkocMrIr3rySO9AHAKeXaOlTB524yulU1sCJ4gqVKF0msyM+cNztPhour3GNOUUWyqGjKjina1hX+lBfhbnymWtFNcdzP2adNm1zENkVmzOTNf0FXZlNCFDNBTxTUMPdOBviT6C8L6SJ1A/Ks0NgpCDYp3PMA00tHro8v027ExXYNY2FxBxDLx2NF4gn2+HCc9jnJxkhbgpUW8cchZpYiFHN89PdmHvfw7ArlrQreGh1G/OdeNYtdElnEl/+koX7jVNeHvu2B/uCIprbJTJNSR+zi4AbUf6Y71OgiaUr/81Ll/99xHhuLXM03YxO1pb8A4uZATxZXHitYteOWlIYMX44Z5veceNMeIDyzQc3iZEHUOVUcZiXyHaNCQ5Q3o0sqQSjreHG3IlHRFs8wtmTwk2YcnOI03ZHRtCvTDpoxuKLN4D5OUBEoKgNOVkhwlT85ZsTPt4pFwhBxvNQqNQrSG9dQUK3FAKxfX1TYHIIq9/BLsoxFyWwmHPfWckEo1sAdn9qfHCU569K3QGcskMFw0moQoLgKXuXg8l81g0YxG5qbZspKTc2PSHE4HNThV41yw+V8T4f1ELudGAriK+U1ryhykLr8mR2CdRt0uNmTofeianbCFTlb4IkOfH9b22y1RZskMtD4zA/KXwzWeGc+UyMuD4VwDe4pnsgVFzcEje1HVlDGh6YSZWwGSl0CvTPRO2bDTGCWwaATQwEIJ38hALRsyVkHWSJbeyoYMftgWpnfqrC50gltP3LsZINww2bnvL/CxHh2H1dfh5IfTH4iysMxJSzjJZ0gb+LJXsK15Mt9ca2ys/bSo19DnTSpKBAfbfmzYdFLDjUsML2E20I8Ym7EcH5H5SJ6PzEJkkEXEpPPdZQZJiIP4tVcn5vlgkwtmn6ov8AUrfOTG41jLEa7gwOeBmtingYqoehuJtoxr7mwRid4GWnjlDF9krhZeCSXfZn9eyOCI5L7LhPt21N24I4tZGLBGlbnnOe55nntegOd+L+sYT4ClffQDp2PKEfKdU/bxNHL/XrxZHJI1o3KhybBiOIG2r6Ck2FFAfL3gLQ1d3pLPVVNHMSJ9i4y3z/W8851q5rSBOT8Q0VKCVsTqZfU5MVqtE6XtsGPTUtFw8pK783jF/bzYM4G6+4UCUge56F/nI9rO94PxNjwEOqzffR955yI0X8JPP4NyrUl4XEO4CKZqf2U25z7CGoq6VPBl5sqL3YxbEMrZJG1hVSF/FhFyHPrxZ2PwnIh7tiQ8m4mjK6B/wwvoLa5gcfK5h8nnrXjb+4XwsMH1AQUS+KIA5MUByEsCkJcC5AoeAkI2rlaKpbLpyheTuimpaJa12aDIvgxXPMsrJPZywHmJX1qZdwzqb8m0HMcdnrKJEwzXfwaVDFtup3St4tbJxgmrE+CSJpn4KVj8dOE8c8pE5fMVQM8zahDPpcXSI4Cuw6bYrQ7zl5KdVxHZISdDt2Hw6gj3CewiJyk0/xFedl7n72JXghr22poWj9FqOi8NnMaRU643YPBGDN6EwZsxeAsGt2PwVqzgcruqyDFVFcUDtZWeyOGZH7c3iqIrXGmbPys6sL04CCz5IMSz73DuwhpznhvM5xy94Dp0sXTcYZwTTCdeYCxqVRaSnKMHSIRX54Vgo1Id0HLMufpFwRxlU4ZZ3b9gPzuE0FIJVtPT+gFDFRdpxlRWL8lnBTNQayU8VDi/GhHMrwu6dd+xGKN0XZ1WSm3BLBZNYY0dD2xDEgGUvQPOBmDfkUwGP28NfB5E51pkK0sW/2QIwt4SdrUrEEoiGCjVtTuW6OFlgpxkwY5kBVWTviArUkGwd8hEJ9wNGxs8KonfgcrjbRBsJi6DJlyvQ9HrQYGskOTwNVnsHRDUPTP1NwJVEtFnQA0rbVWwCurumW7xm4QrcYZjTY7dCQ9bRl1/VVRFvsfDhLvwiTCBtOAQ0Ha980F4RjhjpXAAEp7BEpk6F5qEHr7K96PUtKeKxfd6avkg6ulBnflOm7UKwgRf0YdZRbY93VhoRb18RR9FfZlvTbd0vM9T073YqaPSAvHKZHuHFIb52u5jtdWz2vaH1tbH1/ZJeDgVSt0jkiphY6Bo5Lnr/Z7aPw3BdrtW8SLAaBF3YuaWi4VenobPMBqipAcF4cogDdCb/TwND9Le9Lnt+4Cn/ocwD1tejJJvd5NevYyv+gus6hhrfndo8wf4qv8htOoPeqr+MgTb7I0SajRLIa7TKkIHX/tXWe0NrOFtoQ0f5Gv/OgoPbIl3iTl51pBl80OeivGzxht7tFmV+HFDLSok+Rq/xWpsZDVeGlrjfr7G72B7Uy3tKbEoXpROt+wWsQc/7Kn2e9jVzPiYekmDzSR1cSLs5Ov/Aas/zuq/ILT+Ib7+H0WIB6R0x8I9njp/AsE5tnD3oXPK5IGSOEo+vs5e4gjn8nX/jNXdxOo+O7TuYb7uf4WHrbtE6tu/tdtxc/gRDyX/DkEzy2PnEE7nq/5PVvV6VvX20Kqfxlf9S0zq/qinol/heO5XYMlpSLNFsVcF1WgKW/iaHmU1bWA1NYfWdBVXE4F9DOq5Fuv5XyzkNyhFRA8OyhKOXlNYzypB/NjvIh4lLsS5Slz1O8I35w/wsG5Mv9dTz5+QvdRXLDp9U+yXkUI936S/RDjNC41YF9qkUb42pKruGtn8e0916wR8O2t72BP+8hhXCc7xjtaFYv/0WFglY3wlDQJp0n2eOtB+bOO0oZT0nIiHZpIh/I6vZ73A6Vso+Teh9Yz7++fjUMkzsJJNkDHWDEHjRKFiom844dHHuK7ZIni75lePhXXNBN+ObbjI6WwrFj/hqWQ7tmRQUdFL6XTBkKWc8J98S04XvN3y76EtuZqv6UxIE7u6WlId4oK4q72lq0ssglJpa+mCeFtLN9Esn/RQcbZARJ4ZMJriRf0LIxcLP+MJOdfXdT8JJWSSJ2QHpJ3S1gUUdIqz4kXdLe2ifrTl4k95qr4AA/r+VvgBX+FOXx9+L7TCKb7Ci5HHY+NjA88HdqH9QfwSLP1SCNbbPMbJ6VusHvKaKcn1JVb0Tb4vI/bkNM3Xg17ijqCj6CbHz6OIKxDnjVn/mOjx34yjnLgCJAhkzA/pes4FDeLugeyWJ2H1Dtv1kp0ffQfeDM1BI4t4GpvTBsGWQwXJgn39MHWt26svCF9ljcJJJ9bBNwoa8OVQAT3gHwCn1tHjufgurGi3gEsL556EiAfLONmi7vgiPxz2+IbD50NrO8iz8HJIO2uXeA22lv9SjTjPvju13UPKFTgW7U9SCQ/wctLjGyH3h8oJvjVEK6ZYP8Bv6f6rL7MlL31x7ygnDPq1pFXmv4W8SaZGCKv48tywRi5gjWvqanAqU43KQ/LMKqdwTJ+UTdmYW82u4UZ7JpgNwckjUtGsHfvaQogjYphJn2TKUlUpazvJlLVXpax91fKsY9VS1rlqKetatZTtWrWU7V61lHWvWsrSqVWr0NKrdxZIn+xpYBHSVu88kF69E0F69c4E6dU7FaRX71yQXr2TQXr1zgZtJ3s2WIS01aty21avym1bvSq3bfWq3LbVq3LbVq/KbVu9Krdt9arc9tWrcttX7wK8ffXOBu0nezboqr4NXb3DoLvrZNMWxjbnFmHmkH2L8iRTuXtRKsfHVmEHu+SN6dpJp3BxBk4OT61qDo6XLVW2+mR0znZiXBbi3cXl37fFm4mbM7o2JGk5J2lLxpB1LAltCMyCruY2kVtZBIZ0NLvREaWoWBvROZ2TepYT66FfLyVmVPhZ+ANWdpOTSDC3ZvADqXgj263+lIyJ7+KAhU79zRkLv8FqXc3uS24ht6snZZKR3K/eQuyF0aOfaUnFEkC2ZVR2kdouWZFN78XHugj704iXHk/8N+0b4bdx43HyAYTvVkd8La6cuA/a4xvzTRFmIUCCpggfhd8NT/0iuWd79ivrPnr3Sx4Zig/g+/36GyLHzZfGSbyDfay+NIKkO740BgWXjwLexyL+MfYL9GoAgeLNLPI1wiGBQfD/s+y8w3xevOGCV1PIdRe86RbHG25xvNkWxxttcbzJFscraHG8ehbHK2fxI64BBBdMC7ZRS/wgPDY1xQ/xtN71mP/mzJ2Pcfch8GoXd2XmOXYbruPb8Cq7Ddfzbbgt4l7/iuP1rzje+4rjha843vSK4xWvOI7yON60iuOFpvjNoW1Q3DYcEbzXgIoCdw3o1YHGvIpvzC3exrzaboxpSz/8ongXY8XXK07bO0VukPst3rfvJVkChvCRxqhtYM2+Si/gNRCkYR0HexODNXCwNzMYEer2XamW7t3CWxisnst3O4NFOdhbGSzmwqJ3RFZ0MYQYaGJ97nXr9e6FAvPMRa8B1AmN9z8x3tVOmE7BbkJT0hi6ANza29Pit/aPlx3hRVNGZDPaNcbm0QSvt7fFYxAaX3Ayo7EcZkbLudhRkrmPFc5MFOPPdDJfwjKjDVwMzbIaqPmbGX+WkyfN8qBhWew5aIaINmXmsDZdkHv1hfhznZy7WE5ycfP5EHnK05/eEm4XFr/FxqrHmWJNzJPH6isvlHpnpnghpzkbBu0n/NVyqys4BWGxzfZE0ygwzVHHtECUjXJMbBiya1uxP64XodjUNQ/XUp2AM89mRxPj5EMmiZcwZJIH5yKig18qMHWEKdcdM6Evo4ReXxOhiofQIzahr+AJLdqE3goP6CAJ/zeYnv5bAaH1qB2e7Dpw0QFTuzcWh3BnrLySm5gFVI1El3qgzwyFPisU+txQ6C1h0IYXekSwhokxdNQTWXwVkUUBx87xWQ/Z11rf7FsL3c6vhWYCa6Ebqq+FmnFQNi41UAhjXmozpnGlY/MdyOx3YvAuwb6R+258ugsZtU54meDey47jvew4XsWO4+3rOF64jr+UjHAMXk7GKRmWizHqI4L3/u/HBO7+79Me89//HeIhr/My6hU1M+pWm1HiShn1SWTKpzD4NAb3Y/AZDB7A4LMYPIjB5zB4CIPPY/AFDL6IwZcw+AcMHsbgyxh8BYOvYvA1DP4Rg69j8A0MvonBIxh8Czsj1vBKj4ZrW/5gJreOcQisdAzhHrWBaJfh3DbXRZuTLxc4HAk7sfAfT/idv52LN2WHtYGsXizKRlYmtsZ9kpotq6SaHXuZNapbpH89e97eYQ3QIWpX4s9x2t4BS4LKgvsA+qurayTnDcKJ9q924s9VbOdkKFr2mQT5ve9L5ACCiFxTk/Bmezb+J4HbW95uz8bf5lZTwjuY4sB7lbHvYBaf75XYd8OA3wsDfj8M+AMADtjACfTvJe5ENwm647iFd9RAXSdQxx8BFw8/hKIuW0lRzAlD7J+hgDcs5e9hxoBxYYqKJi5aFfP14PXQYYozFc7rhOvpIUiM48LEdf/wIyDvS7W4J1muW5KEaMizMCgNJ04crZGqTdIoBw7NNLkYyREoxV+6qRRLquwDx34MzTmX8TDJuJZkt+qTUHCyt1wJnlv81NlSxX/um6v/lRfcMwJz9encFBT7zxBZ5M4x3snEHl1ExP4LIuvtvFMjk7Ff+gH/zw/4lR/w3wC4ggNwEkkd1/R3plMpW7Qn5RlZDQr4o1BIy/IKif0P4NwmTEtHZCLCRR1EuCiDQKFOYn5JRHkObw0pVkEMKx1k1if9TLgRytVl++DpT+LpPedZR5QsX1POi/0a6PrIEyPMvAsdB+aPBNFgKFZYUpY0yiC9QCEaaRk+x/4XKN9qyy3wyZbdoKz+1pXV3/9/9r4ETo6ibH8nu9O7szk2d4AA6YQjQHb2ziYLOdwr2TXZg+wm4RDG3pne3Saz08P0zG4WRC4R+RARAQERAfkQEBEVEQS5QURAjojIh8glIvLx50NE5DL/equrut/urp7tHCZhf+YHvVNPvW8dT51d/VaVq65+wOoqXaqSPHW1CNfVT1yVCdXTa1k9hX3QEkhPwidhSRC8Ewl5kAkepNCDFHmQsAeRCPJgqHYxbOFznMZFUkyejd09NgRSVj/Her21qhZXyq0zmlgYafKzWU+xdx/TW25MKGlYibEP0VmVO0Gj2/v1ZJYE0qYYhprsSyoJIqKTLknXyC9WBck7HFHKaDpRgJMF9T6i0akPK3JPXM9qSoVUzDPnX7SRCVbRTpzgLNrJE8yipbs8//aJu2j/7xNUtNPcNKKyvY6V7Y0gOIMITnacaybN9EKzvNBsgIZMyFxikuZ4pfbyQnt7oX0IdF4hmY3K3bhJ+hy31pSBsU3hhQObOaGocNm06RlDHVJStm8HmcmOKnbBroPXdw2OQkpoKq0LQ2k9BfvWCaaSKekIjQkOTwSoeRBmpFAzGptRNWlSsuQHSWqLpjOoJaMNQ4D0FjOzbvXm+qyaB1NJ+juVoOLmUQE98YzKN54apLoZcZIEJaXqxM+86Ip+bpPXaFlDZvejkblDr5bMkpzBQQxGNiH3qkmVdmPyRjjvzDwE4RhdH7IqP62pr5x2Ga6rvfzIMd4TQBLLpbl2MUXNEvBW1v3syjrPVVnns8pKT7T4o6ey/gFX1oM8NQLV1utZbb0aTv5aSB5TGs1D3Hj/cIgAO1SAHSbAFgmwcgEWFWAVAqzSxvybeLXNWq2LtcWMNfiMFPr1J+6X3V9h1hq88SPabuFz8SMmoLn4rSHBZ5TQnYzirwPFy8ljtrj+SCvy+K3M4/eZPH6NxK9Z7EemH3T/trtHh0mtdxLTRAKq2faApGai97XQapVMS+WUOkImF/TskBSfw9Dpi0+4nvkHm8D4iJMJj3lEYLkrPZ4peQtJ1H3/nlmM44g2ejAi+mlm3gOcBJlAqDcMJIcPDWzlJR+lbESBDb8T4CKr7YbRPsG5CrRmApvSwP8/9TSMn+CG0eVf21AD+QWr9OdBpT9yAhzooyWi60g+nPVznb9Xj79Xr7/Xen+vDf5eG/29jvL3OtrhlZ/9Y232j3OxH2Ps0xK4ysP+dzD7cd/UIPLvYuSfD+TDstMse+zC6e/39xrw9xr099KI13Khl88Bo95+5gQSRPm2BCFtIhrnh8ickx4DxboV0nUMZFSFHs85kB1k/YwiC0KukMVLB6Y+WzdIapvUfEeksm7T7mKSJFVX7YIuJmtlyO2G3qIc9y1OL7MHGXIUZv4qrNtV+ERXFTYm2EvAoa96qvB/4So84lt/UBW+e6etfEfOtJN9livZZ09Aq9+bPcke/sR39Tt0D0vghdDGoJebZM4UWUv4mgc534N83YNc4EG+4UEu9CAXeZCLLcS3MC+xWbnMxcrljBU6uUx4WOnDhXmlO25E0b2MIsiDBBPMfXrSalwjNbkxnU6yI7R4kr87hv81Y/j/9xj+147h/70x/K8bw//6MfxvQP5R5O9bQDfaBXSTq4BuZgVED5Xr9hRQJy6gW/KnCxXXfay4oBJKt5LHvvYbvHyw3AzdIXn54hn62ZgSt40pcfuYEj8fU+KOMSXuHFPiF2NK3EUk9rclyHwrEeUyvkV4j12E97mK8AFWhPTdYZmnCA/HRfjwWKlDhXg/K8R9oRAfIY8y6GnZWgy8Qku/FoGPisDHRODjIvA3IvAJEfikCHyKg1Eb9LK5xWbzGRebzzI26amDh3hehw/GbD4vSAEi8AFGIEyApBfgJdA82pGvSkh/FGAvCrCXBNjLAuwVAfaqAPuTAHvNwqIc8/L2us3bGy7e3mS8Ud+ZHt6mY97e9saPaHuQ0QZHDErvgChdUmxcxRvQ3wTYuwLs7wLsPYyZZ8lL/xBg7wuwfxLse2FrsTJl9Cl9o/nmcOXWS2SvvklLERSuuZHpkX7uiZRjejVER0Hzt0bPHDTYmYNqplxe39NRTj1lhZ1YCkvX/Vkz4HK5XzMG1VH+cphLZjNKlL76oRjSeobAWjZqTmpxOlS+HiZnc33uk8KPzGnk/Ztwsh6Cpd+u5A6ilmGvl1H8Xro2apifAVDorWZR0PX0Tbq1rr64CtaDzVV387dhHQiHcZme4tpp6RD3Uq+by5vSZkg4hiVV/JfaL9MPJqYrEa+truIKtBC4T20VUq82HWas1Efur7M95ZoaHsYS8GMBmUgNCqemypFT5FiCIqheSh0mX3JNnUtF7q+BLGOEANIHvAJHG1dFkz6jykd2e/7E1Z63svZsnkDqac//+hi1Z7hizdXQUHt+iLVnOGNTCluiHVablARYsQArEWARjLF2WirAJgqwSQR7uNDVfDdow+QNKYM+JTRHHV2V9b7Zp4zqKdIY+tjCdJYePCnTm6tYFOXyKpk0+GaztZo+Cg+GN4DOndAA/lPhpcm8hKMd/hW+rNCq8NMKnRV+RqFZ4em5t//7sbvC/xVX+Dnemogq/C9ZhYeTa6W9C+HTHXxMZTV2Hzcw1w3s6wb2cwP7E2AOAuTP2B8MpHnE86EQXd2ESyjw91zXYqj5fYEMaKROwm/0UYd06z05MrrQdYP58kaV3tEyylYCUH+O1hTpioU1csEdGBky3rBVDyMXH5QVw7Xw4bl/wzwGFEKiS74yyUvCOwK6VznxoIPHTpTOrnQWzLsFwyldE5Hmc459p+AH2HXnIFfdWcjqDj3J+PeeuvM7XHcWuQoTVZyHWcU5DSaM0UI4oNReHGVFXyGGK8VwlRiuJnCZC5ZqRGCtCKwTgYs5yBaQAfSSuMQmscFF4hGMxMl0Evix+z3mfkziSmG+EJW/YlSCDarUSKRL7ZUFqcnlbna5W6CIbLcstRKgGS9NmC1LYIAxxqePVSSgmm0PSFpdCJ8+qA2HQppkiszwtNSA48OHZaAhDh6ZHznDt1oeX3TUSUgZx5Il+1bi+oAitZFE/X4XrEviFp33ciTHFVHljr6I9AXmlU9s+msO4VwtZXmYowpdz2y3aoVPXV5j1+UOV13uYnUZjPBCP/J0CD/EdbnHWftQJX6EVeIUVOL1plgmzirpBpd7o8t9lFmJuVuWjnYJHONyH+tyf87lPs5y+/ARs/lQXHzEGR/wSTP0bQ8f38J8DDjjRXz8mvEBu4IkjYjt5bcaJZ2Q13dTXt8k8d3bz1eWhvIqp/L66nl903l9T0S+nmU/b2kYdmnkXKUxwkoDtmeEvuIpjS/j0jg5X5pQ2TzKygbsSCQ433x6T5b0HUomYS/LS1/0wU/1wU/zwU/3wc/wwc/0wb/kg5+FcftLhpfks22Sz3GRfC4jeRr4Zj0kZzDJ54vTgeh9jNG7P9B7QSEsxDLxSmaPQdeh6JVa0jfG8L9wDP+LxvC/eAz/b47hf8kY/peO4X8Z9o8y0xe66Ef9vcV0uV1MV7iK6UpWTGCCEfq8p5iOx8V0Tf50oeJ6nBUX2N1I1xK1mfZXKvvaM+l7vj7X+fpc7+tzg6/P9319bvT1+YGvz00On6h9WZyX+Jtt4n/sIv4WRvwM8F3rIf6zmPjb/NKCKP8NoxxM1CS4g2KG4MOgdIefx51+Hr/w87jLz+NuP497/Dzu9fO4z+GRryd6wGb6IRfTDzOmZ4LvUg/T9ZjpR31Sgoh+ghENhmPS4zARp9ZtYP2u9WfNdP9GDD8hhp8Uw0+J4afF8BYx/Fsx/AzNJ4Hp5xfq4UPsszaxz7mIfZ4ROwt8D/QQuwAT+6IwHYjWJxmtaZjcvMyTZ74K2+l+xc/jVT+PP9G24/WQpdf8VP7s5/G6n8df/DzesHhWxuD5TZvnt1w8v814ng2+Uz08T8E8v+uTEsT0U4xpsG+V3iPys9CbJOrn/uHv9b6/1z/9vT7w9/rQ3+sjf6+P/b0+4V7sdTxfL73Vpj5U5KS+sMikfg74fvyRm/oPP0LUFxf5pQaR/zQjH8xQpQjRmCkyAJNKfX0m+vpM8vWZ7OszxdenzNdnqq/PNO7jNqLzcj6jyOJ8lovzOYzzvcD3Lx7O/4w5n+uXFkT5Fkb5GUD5fkWCtZv9ReA8ESiLwPkicIEIPEAEHigCDyoKspy00GbxUBeLixiLe4Pvbz9yLyc9jVmsFKQAEfhbRuDpQGB1kXBJrUYM14rhOjG8WAzXi+ElYnipGG7gMCJUtMx5hE3pchelKxmlsEs/dK+H0rsxpc3CVKA9v3zrYf7TFX7Ht3Nt96EFrUV0ozjsGrOPI3iVG12vLkLHEbzGja7bigrsfWTvOTTf55qfxZofcM01WBO2iNiasEuEanZgTdgrQjU7i1gmAd3PoTmPa3Zjzflc88gic6GJLjZVOzRruWYP1lw8QWBcXsbtz/Mf67DaEX47D38DDn+NKPzQsQ7N47jm0VgzJtTUHZoncs3PYU1DqHmmQ/MsrhnDmmcLNS9xaF7GNftwKV0u1LzRoXkT11RxnDcLNe9xaN7HNQex5gNCzS0OzWe45ias+axQ83WH5htcM4U13xRqfuTQ/IRrnog1two14SucrQkf4qhmFmvC5ziv5gEOzYO45gjWXCjUXOLQbOCaJxWZ9ZsaKB4h1Fzj0OzgmqfgOLuEmjGHpsI1T8OacaGm4dDMcc0zseaIUPNsh+Y5XPPLWPNcoeblDs0ruOY5WPNKoebNDs0fc82vYs1bhJoPODQf4prnY82HhZrPOjSf45rfwJrPCzXfdGi+xTUvxppvCzW3OjRhqkw1L8WaMGH2asJsz9acxTUvx5pzhJoLHZqHcs3vYM1FQs0jHJrLuebVWHOlUBOGzJ17VshNrpnEzUXIWrrbM5PoxIjrrBAYsYOdFdJmXnq5A3OHW+ncoeyzgaNcs8NR/syMsiNwlJ07HOVtMOO7HR4/h8cdUCe6A8d/JI9/6vbGfyfE+gt43AWPu+FxDzzuhcd98LgfHg/A40F4PASPX8LjYXj8CjibUNYTNM1lG7ikNa0RaFDJowNLfi6wZCxwOvlso4S3Fe5bpgYOYzCw5KbAOUgFljwxcOzZwJIjgSVPcksWuST4v7JTAod5WmDJMwNLfplLOqbaLi0qeU7gML8auIzODxzmNwKHeXHgMC8NLHl54Ni/E1jy6qCSoZv4APpIEdqne7NwAL2VDaAtkIVHiwSH3jwmAh8Xgb8RgU+IwCdF4FMEPDvkQu3jQhw436hv2i6LTqnhxiLsqAbz6I8osgwBU7KFvoZk0tMkNR+bJiMLua2IZYGR1keIHNGUjURDQ5XnDBoiB2fDxLO2outwHW5B2KzrybTGjVqaR/vMcMvlpvauzmNkuMadms9kdHPHPskYLMub+Yct/yrVbtGMlDoqr0qCY4QMWxmiofd7rFUYU3JPdjSZo8bVPCMD1Lpyc3WVrAG+yKiQtqBS4lvP8ZTJfDxTxH4VRJ6lc6dm+EkPeXuOzZ3iUNMO9iwPHohXYV4QVAm0sPUzVk9XgOyLourzUpF91Ak+EkZ6WST9igh8VQT+SQS+JgL/LAJfD0TiGzaJb1IS4TAf85C8tzCJ0z0kTsUkvpOfxNsYiW0g+y7IclNontq/i8D3ROA/ROD7IvCfIvADEfghAevokRa8vpJyBEsny2hSzqj9SXWzZZVJr8neXC59xIMDG3LfUxI+4SyHIlspy3AUjXkKYSiMWP7Xh26WP/4QsRwOe9OOWL6dsdwNssWWbIedzxIRGBGBpSJwogicJAIni8ApBLyW9LNKyupG2/TkAHGs0RPKJvhjnoXSqW6GDizj2TiyDgrBPKeUnkuTI7XNPHSElxuNsxkXYj8p1nKZBJAZJSU5rCbpNfCmfVy53KeSbi6l2scemQUrlfEMgKm0b7lOC1vlOiPsLNdZuFz/6inXv+By3VvAFirXn7Ny/Rl89pxLZKdTWceJatK+Pvh+Pvj+Pvg8H1z2wef74AsIHlurD+nlMjvXo0eBA84y1hAFRbNZTTCrQhoELwS+Pcd2o6ZJJWmpSgeQSGbSUqKRsw/VJHpvUR1kF9VCV1Ediovqd56i+i0uqqg4u6i07mCldRSIVxLxqba4mTipSohWC9EaIVorROuE6GIhWi9El3A0ilAvmQ02mUe4yFyOybzfQ+a9mMxGURoQlXcyKu8G4WaIs75usQzHHI1KLU5nq9O5ClIFTupabXt2tRwttTmd7U7nZ4nz2QmOA5hqVslttS3l8GhvL5cdB0XxDoZPeBYx92BtIqpp7umXDJej2KDpcoRnVXMyP6Rb1cy/dPfaiEINhTO6zCz2yc9+MjszT0d3GBC3RQ16jGY5NW22Y6QOCMGE6RY1swslXTIZ4Jr1pJ5BkQ1qA4Ok80y4M017bNhNM6QOKGQmSLpVg7RjmCc2r4tW19SSvLHjpNqaW+SapfTrVrm0hhVMFLj2VqwOu2J10YoF53qZh/QeGUanpvzQU7F+gCvWekdtwAelsBp1D0ht5JXENAZ2uI52uI5xuI61XK2dPdLnHK7jHK7jiWvQcc4aG8jaTDr4NiBH0bmY9uzeQDs7aK1kG8rKpRjnFuL2cqvY3MZd3KphdCbKtzzcXoq51TAX+BgURu29ILTJan+khktJh2vI4Uo5XLrlamxeI6UdrhMdrgxxXVUo4tbZuNY1biRDxir+dx0+W80E+bS1sbuLvBVlMuYEo1xuh5kAbGpLk/YyDAKZ+KCWJd50O2l7Y6Md14ie2TQI57SR6r9qQxR6CRTNBrmHNUb+y5qi9OeSSbk/Q1zsvD+zXUJTjENTtPsBCLe2oZy0udrmjS38L42qeVUUtpfRPx099AdI0j/gJh0YvIhmkvIxqmYYlr3/4qoqmW0/5b3OBi1O3kId58/xPDoqKZnDJfWBnOpE6TiNEcmw2jspNG+dzNl1csRVJ0fD6JCTL3vq5JdwnTwFVyJ8rgmrk/eB0KkwywIhdAiddJoIPF0EngGDFQHvvwqjZ1qizc2tPT1d69pbe6QvicCzROCXCfj9QusQQCfturkJsrUXXkXSqppIanC4fg15zx6190iyflaOm6f62cBARksTRdLBp/leaK7DDxJ072PuTyrGIPtjS/drGXWE/E8qSV+SS1kOc3SiZ/6xaOIQIdOWN2lZOPM2qSoGCUDPJMrtkYFKp/VcfJD9Ngbp3QBDyoBykpZSzfoEl3xBMviph/30hpFyeVhTR/rp0YOkkWR0ptsm84Gvzzz5kHWVSuKEHGxpS6oujzRT5Sk20MYcSt1IRjOyzCGdzUoxikrRW63Psav1ua5qfV4YfdbJeKp1GlfrCwT1EB+Kw2r3UyB7IW0CR9WbM56LHK6LHa5vOlyXWC6Y/FzqcF3mcH2LuPYya6hqVlHaBcHmXtLQL2eyPgP7FTYjV7oYuTqMDsA53sPI5zAj1+K048NvGBUPgNB1XMg0Bne4bnC4vu9w3Wi5YOz+gcN1k8P1Q+JaI2y1x6ipk9imsaSSSil831XzqnY25DevamW/WGMhg4Z0M2dPPHT/2GbvFhd7t4bR6TSf9bDXhtn7Oc4uPouGsfcgCN1pUQwD8i8crrscrrsdrnssFwzP9zpc9zlc9xPXPAd7Sc85tNIDVn0SDhwP2Yw87GLkkTA67KXew0gdZuRxnAV8sAtj5H4QeoIITQGhDVaPIz0pwJ4SYE8LsC0W1t66cVV7Z0vrOum3AuwZAfY7glUKa1436abgvdjsEKFz5A7pWRZQ1A7Iy+hzNqPPuxh9IYwOfFngYVTGjL7szTA+74Xx+hCIvkq7NyKKB9Q/icDXROCf6YB8VL1zQH7dEkXD7F9E4Bsi8K8E/J5jWglD2wjp/tV0GhbE4V7NcrfTGqutoc0zsjqGSc+YTYdUU3JII68EaSjMciZvpJMwiNI/dG82Cw7iJ2/MCTzsZsngWGlQk25r/M6oQ3oWBt9UNqMnncOueeCCmQvhEBzXhyEM4k5p/aN2lFQRETKairPhnY/PcT1HUgzfEJIKzFtGSGUAZzyjpDZBqszc27ODvlw2Cz2nc7ZA14FyafhQ0U9PnSBAWlPjpNd8k5Vg/sH4Lbtiv+2q2O+E0Yk8UzwVexKu2O8J6iA+kofV7F+C7Ptmt7KEmbs7XB84XB86XB9ZLhhlPna4PnG4/kVcB/A3nl5lU24IqDHHlZ6OZguStjI1n5ElJFnkFEpOcsISOt7kww/c5PzzA0RORELZwAebMFZ+AEITJcrgEkcrniQCJ4vAKRJt70uc7b1MJDpVBE4TgdMJWMt5hEmF2YyMwRy0UFa59Rz8dmDSDBZaFIXmpXeWTe8cF717S+gwjT976P0Tpnc/QdLxaRqM5adBdh6RncXfaZ1Hk8v+XvP9vRb4ex0g8fcumCAe6HAd5HAd7HAtlPKuAh1qs7bIxVpUQsdIPO1h7UnMWrVvwvGBEow7OPFQqpXYhNsevAypTgQuFoH1InCJCFwqAhtE4OEi8AgGRhHopXG5TeNKF42NElqlvRuRxpbJMI2tghTgYyQYgXCkpLSaV1Sc2jYR2C4CPysC14jAtSKwQwR2isAu3nrzEnikTWCPi8D1mMDvewi8HhN4tCAFyFqPG0vkNyqHr922qeKz3NLiWAmZKj4ntLR4w6H5Jtc8Hmu+JdT8xKG5lWsqWBO+X3o14cOYrQnfxqhmAmvOEmoe5NBcyDUHsOahQs0Gh+YRXPMErLlcqNnh0OzimkNY80ihpuLQjHPNNNZUhZo5h+YI1zSw5qhQ8xyH5rlccxhrnifUvMKheSXXHMWaVws1f+zQvIVrfgFr3irUfMih+TDXPBVrPiLUfM6h+TzXPANrviDUfMuh+TbXPAtrviPUhOmQrQkzIqr5FawJ8yKv5iyH5hyueS7W3FuoeahDcxHX/BrWjAo1lzs0V3LNC7Bmo1DzSIdmD9e8CGuuF2mWQYcTzBbt+MCSSmDJRGDJgcCSJwSWHAosmQ4saXDJMe3whgOHORpY8guBJU8NLHlG4BydFTjMrwSWPDew5NcCS14QWPIiLukYu11a8G/ivKKBRGLChLLE7An9ZbNCU+g97YfLB5q3oicmFJbNnjAvPGj6FltwEVMoXFi5UcmQN+nsaIeeUJIwtahQjPTmlWnzHsz2xPKG2rqaujB9TmxMJOCYxbiSyTrDjlRTgWhP++qOxmhtVUdHdFV1tC7a0hxtjrZ0Rjvam9d1EXctfYGkURSog9ls+vDKSuWk6vrapUvqKob1iiFDjacSFSk1W1ldVxtv6F9cvTje31+XWFqv9NX3VzXUN/QrSrx2SUNNJRgXqkYlu7HTMBOwuGrz4qro0uolDbX1VRUnpAcW0iTJkCR5VXVFndzSLDfLLZ0yTZJcV1lLX4Xl7chA0KBdpbLtMQHXidlFE0LkWUrYLo4ura+trol2N3Y29nR1tjdHWztb13Z1dUcbG+ui1dGaDdGmxt7e1nVHmwnVdi7TZuQm09Gaqoaauqq66oY6YPsAK0kyS5JMkiRXV9RskFmS5G1MfJAgE4SastlF8wprPrmjiLaUrWzaDP8mQbNRd8lt0mGIMxr4mlnYmXEYpDDG2lunMqSWxtKE7l4tm1SnxYxBPZNtUY14RkvDB+WpsaSeGkCAFKNHx5XFwB6tX6GflTPtLZNiGTWraEla9SbF4jkjqw/1qpuz1dhRgx21kdgmdXREzySM4tiIqg0MZtm12CUkeVpcSw1MjMXNPSWaasBltuR3hh5S1aNmYRnPmCMCjxpKTo2p5hVdiS6aamOGGyBCe8UUw9AGUmrCun53dUbPpY2JMYUDRmlsFdh7NetGdlIMrEsGdbj6NuG+EXcGdq9TFcD2joF9NJi5aCfRFB6ZUw0a++xYh5qJDxIiNQO+7rHyKYk1AbntLSWxrLK5WU+ok2MQIOkHaSThGKxClsY0o0VNqiSiiYT2Yc2gF/uWxobhzrvR9ZlkYUxLSLGUTtI/CfKixAeH4GK12c5q0aQY6iIiOl8EG7k0nCVtdEIoC/NJNNoR7C2So+mYJ/JBSQvBv8ICeBYVFRUWhYpCcPpwITxowwoVwE/y2MWX/YYPGbt5baS1F5p9gPvb4dpmaIj29fWguCJwI+4224Y3iDtCjAYbNipWqymVKFSs1YwsMLVt27jK5V3SjR133GFjp6snSxLWpMCh+wVLA7PV6mz2oPzFADT9GyvUAzunmNw3h29Qkjl1l5VXuKTMdeP1rr8U+7GdSmS3uKveZYyGSiaXhEpKfrJzMgW99i5L+u07J832OLLLUl4SCv2H8F1JODX6gHE8yDRbgvfOaADBpjW1Hc3NjXVNq6Rw0LDhPbd/WS65YllSW1EnbyYi8jr2dZcaXJk3y5KZ5+RllUQExGgInVq0o83G1qfhFbWmvKqqSh5qHISLjZU4mYYxiUoSgwTv0xErUaXhangvk+CV+53Cf38SyP+kCLO69ek+YR4sbYDZigy1ZgCJ8gTIOTPUahJolnTeKA3dGTXKQ4JNfHA9wigIk5kxfHaU9X56ajVJxSbyaoEV9WEtAddwqvSj5MEn5vTsEQaYuJGalpCH4WKGAdWEzd128rBRISvJTaRjTolCIjNsanOciqtyQh9JQTKMXF/0JDWjy6SJEH8F6igY0xlyUh+BP+QtUk6Q1waVBNCsJg0th3K3lsgYahJ2AxpmLm2/Tt28YX1UVvv7ScOzfdr0ZMKQl1QdJDvoq66SR1UlY+DaAKuX5hqmBF+pT7EqRjmrreXd67rKSY0th8IuZzW2/ICqJbXwmlxVU1W9uNyu8rZkzeLFVXJHYxvRb21ua1y3urWlvK19dVt5c2N3Y3N779HlB7aRtlhV3cDU65oaw++RN9dtmuLyl7VwLEdeNwqKwuVBtdcTeW6wHa6qqFlcFf47iX2b9OFdoHgY5jqxmLl9nrIJZ8tH4JykCF38hVXi0sg0+qRrxDPMKEnmIYCSyVf1N8kvbN7SVhqhh2cmoDDgtMfJvcpm2vRWwz3Q6dq16vrOp9rgexl9xZfgpMIFYy8lHHJkqCACR+tF4Kw3cIVCtjs8eUfeFKQYLKGNSrE4GLMUFQSgjwd0LO1/KWuRuWyNLzxl57x9uJNVvz1B4gTuyxJYVPYpf7cppOzQlY1wzNBOUkti7Jo2Mr5VbVuiGUElkf3Y0iwtQqju2/AGBTqFRw0lQ7Tc9ieP71QvW7l5KCmzdC1fQPK7gAwNcR2Mn5YvyGX7o9X1C1aumFy6rBGKuau/h1BPZ8uktZDM09ePbkXLEJ8sGJt09Z1AykkmoaaMwzcbieUL2KrgyMhIxUhthZ4ZqKwhfVHlUR1re+KD6pCywBLWxhaO0i2PpNdfQNIky8uCJYfKEmnqLZOIDs+OptXlC0j6Djeo3AK5kguRYFZ0KxkyPerZlFtWCU4aVeU2xLUT0tWn60lVSS1Y0a8kDXVZJZXBaWxTjNbBftJx7dlp7FA0MoavU/tIJdyzU9qapVfIrFMNPZeJq8YekNpsJidKrLPNw2UeYN/ZQ/oaNkHKGBUbNWNQ6dPIS7jCwVhNrDUFqUrsAVnzK4jtzVvtOM5b3TjO2+JxnLf6cZy3Jbs/b8KhW5BWayq1BxTDDvToZG5HJr49WT2ljrseXZS38dKji/I2Xnp0Ud7GS48uytt46dFFedsDevSd0U0C2qRnx10X6c7XeOke3fkaL12jO1/jpVt052u8dInufI2j7rBXbx+f/aEjY+OpQ3RkbDz1iI6Mjacu0ZGx8dQnOjK2Y53isspt+oqwIjxtR4y+DoQPe7GkltpErVx7lYGpMZWmvkk1sl39/WpmdqyP/+zQUtpQbohaskqxES2RHSyODTL71ISazg7OiMVzGfgu0LpZHUpne5XNYJ6aBlNMPWcgcC4X7CZTbMLnOttG1tjXUhB4lsXUJmXU+vDTMpO6e+CoXRucZIOWxKBmFp6aGSbhTDWDgQ9ymVHTnJQCNFHxUTOWbt3IKknwNcMg6chlUgZs2k1n1cR0DG7UsoNaag6GkG3wPhjnSSGlmWgaNVW64b9k6xDJZ2MikSHlZkbZohlpJRsfBCOxDmWzmS74aNmZG+pTM5HYiTklldWyo1NjBgm2V1+rm5fVGpMpAPGAhe4cy0WC17L0hDnA58bg+Jc4yQ2lt2uY1LqEam5LnkT8lEx8sEkngpFYIqOnIRDZKmOzmjfmsjrg7aks4Za0pPl2gftIlICdrpbJjk6JZeDYgSHV/Jo0DWynlQEVDG9MJFRQAEav1NiVmsDSfwUh0/KVeBDvUHjR2PW/hURCD3CAL3v/bvndbEe5m6O/dydZH3oa/y77Lv3pz0FJpKSkJBwOTQzw8buFNez/1NzQtm0YAcJaxtbosPo0rmwj5ijOTX+o6VNBqCr80tatW7e105Fi1PiHGx9Jldugj+2P+L/wiyQR2xqI2Agp9EcS1EvsZAcKvEAcLzIg9Ae37/PIN3IwPBbC4xB4HFrg/efe6E8tyBbBL3OsbkwmRSI25fxHERhC0QdxnLYyvIUkYhtakI8JVgU8KiFjEZAoLZq++1uav+lNMVjRlESqEb9FYCQ2Lqzz81ocNWxn6i3To5oCe3doEZjPjS9D/LzkNe1oNiwWa1mtoyyCaeCn0RA9L1cBrEohsRYjdbgtgp3kp9fMPS8vAa7SspNssbMYsVM8t4C1QXo3V4C+m1uA0sGgnjwmlxXv6wgkgD2q116TBrfEDG4/HhxY7W/XfWFwIE+kAR6HwwASgjGRWu/yXpqeor3QjdL6AqPmp39Cm7feBLGo9STdqj/LCsx9/fS0bJhe0HnGctQLhSpEzIYqRWgZd5QU1zjKfcl2ji60AqyEmlRYXMtDpCX7mR3tcqnRep0jzIODdU1Uc7FDM4C1td14qaE7tLZtaKFwBzPs3y3le7TbE47t2hFaPWC5xvwF2BQ6G+7q71AVg1S42TFDTSXoMgvJhdavmYsl02Jst3F7apgkjrS8GTE9l4WlNj2+qUkdVIY1PTOV77ke7VDoUkqpkkj06rQyTaQ/TWP86fS3tcoD3jP79FTOgIzAgk5SGTXTazCz40nWr6OGku7906UxjaeqONahpY7Mwl9lM/lbFBtKp6bbm6/54s9Ue5s2rUJGWWxkUE+qhgIbqkl6fDZKm0sspCxpd0KeRQWFIbrbuKQkFArQD3Z5SKMFPTG8jbtYJ1cHqBacFVDM0f2rJbt4B/RO6lahXuym+XA6z95UNa4NQSA79xWbvha13hCiez/MN7VG6OFq6hq6G5uramqlpoKAG9XggrAwvHUa5O8vyKvadtVQn5c2mjK6LwbeBflP8+VxFQzCP7r5VOmr52xpi6xmvWAper1sBzfdGBOGScB2TSFg+0kxGeuyajvu8+w1X4meBkH6OCWXpf2N2RXFFHqY66qMqk6Cwx0ySlqHI1uLY/R2OWNarJ948XUMGFGLY3qqh/QNU2J6RhvQyEBBQyIdFAF7R9NqmP6ylm2tERBWZwvoSlNJqCQUDjC16WEhtqZyQxOC9f+gwmdCIb5KQXcOAUF0x2BVlWT+tYZh872e+4ZvJSWxjakT14vSUBgmY9s4iZvHhq5wLEUHpYGcllATncowjAMBzhZBJ5G4P9cMkRkNkpxMgTXs+JDS2Abz5It1a8l8ifTypIkXxxTSUw0HP4tEOErBBwV9pD21mudEG6BDz8RYH6l8CZhrGRGSaljEb08Us1+RWHxQSybIT/bBqDRmkKxTMmcQpF8hHUp3Uw/H5rCUkMkbgVo3pzPm9TiTYtlBNUVFmkYnxeBqEDKum2e2CEe2ifSUkwyp2WSUs78jwM8iOD+jqBBGPRj3SD0ObeNoFdrNazmTcNjb8Xrxad5CVlISfNWmKaOPGGZVojqQ9x3TNkczmJVLa8ljIj6KuMMNdLqBLjfQ7QaOdAPr3ECPG+h1Ab4XlG+ACVMJ7dMiG1nvRmdr15O+cvtpyTOchr5HQr6eH6N6rCuh6AxaeNOFXrYV5I4jj+nm/TqOo56P98FjPvjnfXCF4E1eXI7Sw69hyzjc8gCGAvQyEQC71ZT8BXQFLOhKfSSc6m0OR4IdtN8ICfSS2iaVS/Ej4S1tfkK8wQ+fBw3YcU494Eh3Qya/SFuSR/Ucvw5R1gx5RE0m5axCWhOcHqfKen8F3HmXdmVnvgQ7f58wL64lsZh/rXuFrCtVSMWQyUNPZd33XiGHKee5PI2hzKXYebcwlmFn9vmJ95yWtPOmGouBctmgt/rGzQs8UnD3pKTatSDvUd+8lQzQVgItPDIID41N9uCQhNA3WX22a/hFuIYPiWscquiHs4r+KojrEIp1sIGUdjpPdDozTqfhdGadzpzTOex0jjicvkyMUiagSUZOgsfJBejI5C95mDgDM3GqIwqbgOJlbBpJF2ACrbR51nHo+sjp8DgDHmfC40vwOAseX4bH2RDBch5VwQ5FFYalmO1dy4HVIjSVD8MZZspMtyicPHaoF0y7EHokW3tiYUDJeV45a5UAZk75/GFOvL/X37zTzrRHEngnzLUOytV8gTb0Gq2pxAZzXnqIVyJnqKv0jHuKuZ8goWnSCSR69fWZpJwnGz1xPa3u6xUw7Vzg/fcAv0Q0wyWnGRXO5VyQjyhdT/Zq6RqvSNb0YZnt8kycyz0qy1qtKeuKTbEYXGlDeF6lqckEffOi01W6uEhehchL2HZUSloxoFYHP5OgEdcJuuhC5tDbETUtCzqHCoX4C530FchPZ1dn65kEgoXUyDnQffwXTBVW0XtIZPo6miMdyHYm2Ts/oW/6XyWPwpOrTmEn6oUNEsP25sobRQhNgkpDsHwAXxak88njvxr+c8DAmIaYaLGiO2MeILqH7amkS0rmSuQebNDanqJGkV2p5J6w0dMvlRvVvj08hZ36OpW89Azvycc1dDR2s8XE3ZdILZVdsKJOkDhaDWPVuzllNb4pq9nNKav1TVntHstZ3R6bssV7bMrq99h6tmQ3p6zaN2VL99jSbNhjU1ZdtZuTVu+ftN09Ciz2T9qeOwxU7+5xoMo/aXvuQFC9544E1bt7KMhToLt7LMiTtN09GOSZ3O7u0cC/86jZ3aNBnon37u5y8yRtd3e5eZK2u7tc/9laze7ucvMkbc/tcmt2d5ebh7Xd3eXmSdru7nL9k1a7u7vcpf5J290TcP9mULvnjga1u300WOr/4tKwuxuC/9yjoX53p01MnGVOHNvIzal39yuziEQ7mV2de+DU105ep57a7SlcImq+dhLXtffsgc0ElXAum1SzzSocm7BrDhMBU+ZtN70HQ+WpMT3VBje0ca9psYyqQ0i9gxnVGNSTiSnUTpNikI4y27lWG9Kyk+HYCMt3X8vVOKxoSaVPS2rZUbgvaX02PsXypJozYkoyCVsfVDv66TEDvsgRCq34y2JZuH8ieyQzn55GN1qsU6kg3WoxjdpjwFkbRlYZShNkZizJ9lTwkDXVcNpBFxawPyVgA73rL3sqIf8mT95Ju3PhC+taV45Hd91NT9SeiNnomF/iS5nBjmW5c9XUC6nZ/c+OezF9M5jffx08i8Dg/tO/HWNHd7l5k27tcrsA8RgC20+6c+0bBfadtiGwAqWXhFzIEfh/gMtehGUHmeUatYwD29cI2LtGwMY1AnatEbBljYD9agRsViNgpxrppeYe7q34V1qmEZGrwT6qNPJdllZqOnGcx7TsWGRaFgEzUmRRp/E8XI/zMMrzcAPOw0kFtplpBMxMI2BTGgED0sjn4QGmoRGw64yAkVsELCIjqjAPP7Xz8LMCp8Xg7QXIYnC1JzOtODNDzsyczDNzd4H9Lwy2btttvjZnWQ/dUOK2KJq9jIp4DI0KSsLcgMW8nCkEVnans8Jh0JkMKrahLzGokEL1FdU1obMYVGRLfZlBYRs6m0GSBYXBBmh7jZsgMnvnBb6/dG5e+6rCUMnd/54DD3bpjWJgQyXdA0Vjmk8ZkXt5zSuCjnNcDBs7eqiDMPVWx3k/6kiKaS+6LUak3h4Zgi3j/W7eK8iLL+SxbffG8QcKYN9w2UVBogtBRzzV6piu5n3mQzw5gH6Xd0m/hJDZ/8XX73BCHzYTekOghP7UkdCf8YQ+ghN6O0/orwtMEz9qb3y3o/y2I6FF0H4+7T1D3gYTfK+ilXCrrTxaYP8rvt9RKQJ04MJ2SGvHY7R2hKA275wBmxu1P1vgHKyfK0CD9cGewfpA/8G6DJpJyVhVlxLzS07M1O1tLS/C4yV4vAyPV+ABewhMy/zX4PFneLwOj7/A4w14/BUeb5pcPlxgbz2I0K0HdMMB3WZANxfQLQV0IwHdPkA3DYzk4/IfBc4NAv8sQBsEij1chjGXpzq5fCQwl7/eYS7/BQ9IVwTCiYTgMQEehfAAG+sIXCYfkchjcnHxozxC+FdUs+2Nhc6boEJvb4uAV6Ji2nrbEzPtwwEsuYTnXVz0gux+G3YfOzAPDN/bU61xfWhIzcRVauDarCTjuSSNZsEyZgJpB+meRc1f1p4i6sTJI3FLzFnWmlVIZN5pp/mvsLCEvt6GdvXO/l3/Gs+3xUPV4q/A8O9H119M33cpVFoagh6LjnbFIfQqA/0WHe2gxVivPS+yNg6NWoKzx6QOekenVIp+T0S/J6Hfk9HvKeT3vuZvuUdP5uiIJ3/G3DP2ymmXGVIZkbg1RDeSKVkbl/v1jEwqH1x8Sjei8S1g7LLQlKomDFlLxZM5MHeX6TaLeFZeRSrjYLnc0wL/tzUTrrV4RqdunoysFt9ULq/riXZ0NFtoXMmYt67SXXKD4EpktGHYUpYYTsiw4pRLlxOJIX0YylVe39MkTSVpj7MAmokK7NNzp4N7QGr4b2dKOnpg6wlPqZm/cjlNfidyulw30FcuTbMp9b5CzghZr5CzQs5RaQ4u1L/8y92T/vlfaKvVXDsO9Cb5EqsJ8EIu7Rey3wek/bFjHnbI2DEfOxZgxwHYcSBxHM22vDGsXG5atBH2O5IW06aPKuVyr9bfD3v2mnXS4Mvl9RtI84Abd7WTSBUAOElqipoolzvN1iO3wAGD2dFy6SAUl5fBhTaDh7oYXBSyT2YL/dbD4NOYwUoUCaLwZUYhrHNI1UQmTCuHVGP/rLV/1tk/F9s/68nPw+hPeS0cFQNVHnZt4n2fuFUtIfJPm60KGpJbEzU0QpuSUhIKbXTdsPWS9EpaQtVp+C2s1fXk0iRT8B1mvsw2esr0sAg1IfebVd3Ikpaoc1c6mTNIa4HjIxJ4hyYNdUBVMhVybRXbBspXSqE9y10pMrxoJBEdcACB3keKvZM0Obknrmc1RVpKMpZgCehgCWBtDXMBmbbSvnadmUs9JfekSb+R1GDK3AnbOjlAiCmXVyukUa7SCUGEKanBYt9bYY6wK8xyV4VZySoMPXf4Xk+FuRtXmGYrClRdXmHVBY5XklqhSvVmtLSeMKRV2LEaO9qwo504jqAlKR/MipJ5Ebf5y8GVZ0PyZ6GyBQ9AWkPkzwo5A5H7VfOSaFmRR0gQsnlcNCllepU1dKFZ6PJJbYA9QbQPz5qRWJt4aQUza4zphSuScPNxhbSWpOXaIJuPt3XTcTlLgxUKdxpDMExxl5UzlzTNpDNnUgcqNW8d67Lr2JGuOtYTst+PQj/w1LHv4zq2EUWCatmrrJbB8qp0NOwKbFIGDOkY69ex1q/PWb+Og8a3WkmQ2Z8MALQqA4bItUoa8txHsbSeiw8COkIaf4aMY3p/uUwqDMx8mjO5oXQStmKv1UfUNAx763Q9S4R7laGMEid/B7XUJul4K86Y9evz1i+F//KyFrdZU12sDTDW4FUgdKmHtW9i1jbxGBBlrzHKYKlaglNeZvAeBjeGlJ+H7ueR9vM40c8j4+dh+HnA+bpfDXEfs7EYrOfnXaHTl/eYbnRV7gSNB0BmNXpq1Ha5w4f+VUmSYodLAXjH0JQhLcEol3IorXl31EdG7DIddZXpySF7F3joS54yPQOX6ak+5KAi/jMr4ltA/nQiX7YqQ1IN/V5jsi83ZEhniMAzReCXIDoOKkPpI7jHWSLpL4vAswm4NbRKIx0g9DF0HtytZ7L9elLTCd+mmHPko7OCTHxQGya57IAWqClJA4b8DRp5DdnEx3u7p6bmFXJU7tZSinyMkhpQoFmu1UmRrc0NDCgDpAC7M1oqCxMz8tsOXc0O0v66G07XkZt0fRP5qWRRwrgoxEEDaiKZoXM7kg+aDd6FQBjkbZBOBGhQPF/SV0TUnEPAaSYYbUwloibsrTvn2nXnPFfdOZ/VHVgvCKU9dSeF686FgjSgevM6qzdfBNmLiexsXs+6tTgMg7KpK30zj98lefwuzeN3WR6/b+XxuzyP37fhzcb8XW7OCmV2ACFq5pY/qZL0ncgUhK3upIhJjeqBgd3YVG5VPWty18tO/pCuQImIskREzYAFH/zswrzaVZjXsMKkvp/zFOYxuDCv8884KtO/sDKF9WDpBpip0Vopfd/+eaP98wf2z5vsnz+0f95s//yR/fPH1k9vbm+xc3urK7e3sdzCik6ozZPbVTi3d1pRoMy9wTJ3J3mvke6CnqoRZu+VHXofaeOVXf39cFXL3X4e9/h53OvncZ+fx/1+Hg/4eTzo5/GQ5RE1PaKmh5fah21qH3FR+yijli4+1iEiTfkaTO0TPilBRP+VEX0aEZWeIo/S1Vp/1lwIkJ52ube43L91uZ9xuX/ncj/rcv/e5X6Ou6PU7eXleZuXF1y8vIh5kT1Vbn/My6vOeBEdbzI6Hgex14jYZLPPIK94ajI7Kv3ZC73uhf4SAkIdmEzPI4RRzJ7IR7OD6pDr9dNeMaIvqvTNjy74kDFkRMkM0bEpnuvvh8Py2LuHeRjT6gx5o5EHIFfkfUYx5ktvQPELUiH9FUaLnu7W5vbGtb1Hy+29rR090pt7RqL/V5S2txxglILe2vG2XTvecdWOd3HtmOSpHaW4drzvKVBUQeBbAF2l/CCEVinhi4DHniH0L1aZTodQPwJ586SgUelj7PgEO/6FHVuxA5Z4LEcIOyYQxyrmkOODsPqdQcdnsZkym0aT4RCmMZvNubE5XyYDX3J0KJ0js95CFLCXYTCZYAwXT3B+FIlMYItQlKVP3Az//RPE8GQUCeJ2K+MLvpVKZURmsjUDJFXPkKZ6oWleaLoXmjHBPgnNhEjV25ghgz5zEjJI1YSXwyZS3lzGs/Awk4RTta3hSLOI1gWhNaqapi3CkOGUUjmRy9ClLnpkpfl6LxvmIZCGPKJlB2VHRBWutMCCMG17IzR6g0VvsOj7IHoTpM1tUE8a5poluIZgnUyaTdJ16b9nKcLSNlPFnUm6DMpcKOEWZie7XJpjl2PU5MBbH/e26+NcV33cj9VHWidf8dTHl3B9nO+pMqhWQo2GWgmmANIBRLKUHnvXBkfRSQe63Ae53Ae73Atd7kNc7kNd7sNc7kXcHaVuLyNRm5FKFyPVE+xPjqHfeBh5DDOy2BkvoiPE6ICzfqQlIGZ2leRdqEJa6nI3uNyHu9xHuNzLXO7lLvcK4j6OLaeutY/Dw9WtXO7ny/PkLTJt/jL46x1f9aJDlDykZqGnjJtveHR1SFppRQkv/VEvw402w80uhlsZw3Td6w4Pw7djhtudWUMMTwjtrA/YkfV2Wje60nr0BPQR+3uetP43SqvrI3aokCXwj5CPGAmkjLUdeggkaePS50WgIgL7RGBcBCZEoCoC+0XggA1GOeglTLMJ2+QibAgT9g0PYV/HhXuiIAWIwCJGIEy+JTjgPWKdpCllnc6c0znsdI44nZudzlGn8ySn82TujDYL+9ZTbCpOdVFxOqbiix4qvoCpOMsRKyIhzEh4D6TOJlKz16nm5IW+q9vHOH4lj985efz+K4/fuXn8vprH77w8fl/L43e+2y/qfy5l5AKb+QtdzF+MmT/Bw/wgZv4y//SgYpB4bwNv2peTx/TejEJmvY7l2W/74Ff44N/xwa/0wa/ywa/2wb/rg19j4/mXbK+1Kb7ORfENmOINn7hfsHsxxTeJ04EMfrgxQ357SOobKtgBM8ObJ1DjJ/jWbhsQwud2+rLy4wnIgBA+utOXlZ9MMJNAbcIWOjQP5Zo/xZqLuOatWPMIh+Zyrnkb1lzJNW/H07Iuh+aRXPMOrNnDNWEpCNaT6JpS3KGpcs27sOYA17wbGkKB+X9oxKE5yjXvxZonc837MHquQ/M8rvkAljmfaz6IGbrSoXk11/wl1ryGaz48AdlE3OLQvJVrPoI1bwuJXj4fdmg+wjUfw5qPcs3HcZzPOzRf4JpPYM0XhXG+7dB8h2s+jTXf5ZpbUJxl/G06v3ksvH7a4cMbKA3/GRw+vId6U7a3Q3Mu1/w91txPqBl1aFZyzeexZrVQs9Gh2cw1/4g1W4Wa6x2aG7nmy1jzaKGm5tDcxDX/hDWHhJqnODRP5ZqvY83ThZoXODQv5Jp/xZoXCzWvdWhexzXfwpo3CDWhu9u5Rp4fuIaBj/AwUOQZaSf4z4/LoLcNZuT5E9oNFpji29Xvh0jYkQnwgNuMInDdQyQMD4k8Jk8o+2ngtNy6w2kphnhL4BGBRyk8JsJjkpmW2wKn5Xaelu0eDydDvFPMeO8IHO+dPN7tNrotg3inwmMaPKbDYwY8ZsJjFjxmw2MOPPaC9ElldwVO3908ffL2pm9viHcfeMyFx77w2A8e+8NjHjxkeMyHxwJ4HACPA+FxEDwOhsdCeBwCj0PhcRg8FsGjHB5ReFRA3krL7g2ct/t2uP5VQrxV8KiGRw08auFRB4/F5FFa9kDg9Dy4w+mph2iXwGMpPBrgcTg8jjDr5S8Dp+XhHW4PywrpNpRHgkZZ9hiXtEZfgQZN3OM7nLjlZuKeCBpl2dOBmduyw4lbYSbumcDM/T6w5POBJf8YWPLlwJJ/Ciz5emDJvwaWfCtoYYc+4POClYXok8tHwnkBDIcwLwDbXamROKY0t8iVcsuGFrlDTWiK1CTAmgVYiwBrFWCrBNhqAdYmwNoJNqm5JUqQKEW876lrCvmEI9JRCBMUOsOhu2S6CtEE5UcfuycoP/wYvaf2eGPHC46MNLAPltYTx0RmAE7XXDe4gY1u4Cg3cLQbOMYNHOsGPucGjrMBn4XYmM2N4uImjrn5toebb2FuBlwx44VORswhQIxGHDOQMb28zrTGl07w89jk55H08xjy80j5eeh+Hmk/jxNtD/qZPco8vAQbNsE5F8EjmOCveAj+Mib4ZJ+U4AVRRjRsWJBOgWaBdy1IX/Qgp3qQ0zzI6R7kDA9ypgf5kgc5y0b87BLOtpk6x8XUuZiprIepDGbqfHfceLmUUQQ7BaQLiGOqZdbEjNekbwjRC4XoRUL0YiH6TSF6iRC9VIhehtEoQ700Xm7TeIWLxisxjZ/30Hg8pvEaURrwkiejshyovBamhet7muQWup1G+p7LfZ3Lfb3LfYPL/X2X+0aX+wcu903MHTXdXlJutkn5sYuUWzApaz2kfBaTcpszXkRHMaMDzBWknxPHbHYjCfSGYE3IPqNJd+TxuzOP3y+I3z4OP9PclPvfhXQVU5d/upPuzuN3Tx6/e/P43Uf85ph+UeJH7TO5p5f/B2z+H6L807M/XoLHw5j/pR7+6zH/j/oThMqihJXFC6DyeCHagPQb7HgCO57EjqeIo5l9eaJfXZiH/MppF8pr1NQm3d6mZG5aopuVvLYOTxfatg6OgKJ5gpG2EK1zQ62pQbhGRVaV+KDTXMA0cHDYNzgC78/oQ3J2UJX7wCK7j5pis50Uwlh7tIEhxbJpsDYOodvefktS9Kt/q5UD3MvGbmczPz1bPi6nQ2QTZMdyZWmuLOcgyZzlMGgeTZf0DCpsb1191q6rz7nq6vO4rh7oqasLcF19EUWCKmeEVc6/w9r2y3ajNrczNZq7Z5htvfTKGP6vjuH/pzH8XxvD/89j+L8+hv9fxvB/A/lHiX/U9I8yf2/ZvGmXzVuusnkbl83Uj92fnKbgsnk3f7pQcZWy4noPius94piO1VoyOnnrlf7hg7/vg//TB//AB//QB//IB//YB/8E4ZRuE/fSvNWmOVTkpLmwCNH88Udumj9EiFRcJEwHoncioxdOgZIiRHwqE6+0BrZSITpRiE4SopOF6BQhWiZEpwrRaQiNNvqOfTOKLDJnucicg8n8y0eerbuYzLmiNCAqJzEqwfpW2o8IT+nJquk02MetA3s2aX8BNk+AyQJsvgBbIMAOEGAHCrCDMBalmGDHrk3coS7iFmHifush7mlMXKU3fkTbZEbbZpjHVhfZG3ftn7X2zzr752L7Z739c4n9c6n9s8H6KdhmaudyOc0lPSrtZXisxLm815PLu3Eum60oUOamsMzBF1qptQh209BNwvhz/yoxvFoMt4nhdjH8WTG8RgyvFcMdFpzfHKHLJvJIF5E9mMgfeIj8PiZyozAViNQyRip8hJWOJtLFLXoyCSk9BjuOxY7PYcdx2HE8dsSw4/PYoSCHYEOknXWVZh32FZsHvwzgrF/qyfo3cdY3oUhQhqeyDP8cMjwE4XfoKZ1uQ045XLrDlXa4TnS4Mg6X4XBlHa4cdgm2Ddo5H3Xl/GSc8y95cn4GzvmpOBaU9Wks6ydB1k8nQnub1oZsg3SzPtSnyyOVbaqSkM7I731mfu8v5fc+i3q3dfV2yb3r2ru7WuTmro6mLnljZVtrY4v05fzeZ+f3/kp+73Ms76jpHaXe0Y1R8BZsx7PL5DxXmZyPyyTtKZMULpML8zKCCmk6K6RhKKSLYc7h0AJpQ/qmD36JD36pD34ZxTFXQEKP9C0f/HIf/Ns++BUWzsmmuGCfnM3y1S6Wr8Esf87D8jGY5evE+UT0zmD03gH03kDEy3phTaiyQ0tpcje00e+LwBtF4A9E4E0i8Ici8GYR+CMR+GMLjAIY7Rb2HrfYHN7q4vA2zGGbh8NVmMM7BSlABM5kBML1sdJdRdTazXO+wt0++D0++L0++H0UN2tUY3Nza09P17r21h7pfh/8AR/8QR/8IRuPIlyw/c7m9hEXt49ibus83NZgbp8Q5xPRO4vRuzfQ+xQRn8S7CzBJl572IFs8yG89yDMe5Hce5FkP8nsP8pyNRClS4N2PZxP1gouoFzFRsoeo/TFRr7rjRhTNZhTBNlHpNSI4lQmuVQcMGa5Tlf4sRF8Xon8Rom8I0b8K0TeF6P8K0bdsNApoFFDBxjWbxndcNL6LaZzkobEU0/i+KA2IyjmMSjDMkz6AIQofnuIctD/M7/1Rfu+P83t/kt/7X/m9t+b3hg/HebxDlneU123wjo5EwdtbNoVhq2zCYWfZFIdR2fzzQ3fZ/ONDVDYT86YKFdJerJD+BoU0mWhNd2iZI/kUH7zMB5/qg0/zwaf74DN88Jk++CwL52RT3MvyHJvlvV0sz8Us/8nD8iuY5XnidCB692b0XgKLYvOJ+D7d5nEwdJGnW9dS2ejB0Z5BOPRFWjCG/wFj+B84hv9BY/gfPIb/wjH8DxnD/1Divy/zp4tqpj98DzElvAW1yC6oKC0oev75q/CoDKMdoE9+6F5e+w0uqNr8KUMFtg8rsEuhwBYTtclEJJdMqBl6xI9U74WWeKGlXqjBCx3uhY7wQsu80HIMRcUn/6y0mWt0MdccRkbxv/AwdwdmbrUndkTWXEbWRUBWOzQGOHwyTf6HY0HMvanSZ33wNT74Wh+8wwfv9MG7fPBuH/xIgs+0cLNWJsXLbT02t+td3G4Mo12P13u4/R7m9lhxShDB+zKCXwLx40CcbW0ik5ZKVp+l433wmA/+eR9cceHs4CqpzweP++AJH1zluDm9izLcS++ATa/mondTGG15vMjTO38D06uL84no3Y/RCydlSCdCTe/QE7mkkmFT0owXMrxQ1gvlvNCwFxrxQpu90CiC/CbGJ9uMneJi7FTGGD3m6zQPY1/EjJ3piR2RtT8jC/ZiSGcRyUnr4NRunswve5CzPchXPMg5HuS/PMi5HuSrHuQ8GzE58taq822OLnBxdCHjiO6eSXo4OgFzdIk7bkTRPEYRrKRKlxHB0jYwQDKT+C2X+3KX+9su9xUu93dc7itd7qtc7qu524+Qa2xCrnURch0jhJo/HuUhZAMm5EZnvIgOmdEBZ9pINxGxqRutI/BYIn8oRG8Woj8Soj8Woj8RorcI0Z8K0VudqB+Jt9kk/txF4p2MRHqgVouHxCZM4j2iNCAq5zMqfwlU3gfNFE7WzWX5jOB+L/SAF3rQCz3khX7phR72Qr/yQo9gyGda8qjN1+Muvp5gfFHfKg9fFZivLZ7YEVkLGFlw+pL0TBi+aaXVuKbAETM0ob8TYM8KsN8LsOcE2P8IsOcF2B8E2AsOzIe1F23WXnax9ipjje5T3NfD2j6Ytde98SPaDmC0wdlU0hsQozU3kf7qdL7pdP6v0/mW0/n/nM63nc7/czrfcTi9TLxrM/Gei4n3GRMTwbfEw4SEmfjIEQ0i4UBGAhy3JX0CfDG7IT5L+pcA2yrAYDBxYyEBNkGAFQqwIgEWtrFot99sqliyGItITsYmSiZjk8D37x+4GfvbB4ixMm/8iLaDGG1wGJk0jYhOazOPgKk8RteHzJo+XQzPEMMzxfAsMTxbDM8Rw3uJ4b0RHAXYpzXOtRndz8XoPMYo3OYSesnD6B8xowcIU4FIPZiRCtd9SQcR6VJzIksTe7DLvdDlPsTlPtTlPszlXuRyl7vcUcvtw0qlzUq1i5VaxgpccBh6zMPKrzErS5zxIjoWMjrgejWpgYhNQqeXVkiHe5AjPMgyD7Lcg6zwICs9yGc8SCNCfDYQNNsEtboIWs0Igh0mods9BP0ME7TGHTei6BBG0R9AsIMITkTHE0udbqDLDXS7gSPdwDo30OMGet3AehvwqTobbWaOdjFzLGMGdkiG/tvDzHcxMzFXzIiYQ/mLNPRPCpGb2tXaIa/SsnBgujnh6xOicSGagKZL0PuvcsCqJdze29vaIjc39rT2SP1CdECIDgpRjaFRE41S1EvjJpvGIReNOqNxGvh+3UPj1zCNhijLiMvDGJf0hoScBIf+wWoarF/Aahp/3/XzGPHz2OznMerncZKfx8l+Hl/w8ziFeMxyrUf6vW2favN8uovnMxnPsIEs9AUPzydhns/2SQuiehGj+hOQPwei7Vm7jr8oO53nOp1fdTrPczq/5nSe73R+3em8gDn96LjQpuNiFx2XMDpmgO+gh45+TMfljlgRCeWMBHrrxxUwDQEpOmCyN2MBdqUAu0qAXS3AvivArhFg/y3ArmWYOX/wYew6m7EbXIzdyBibCb69HsbWYcZu9saPaIsy2mBzu/RjIro/m7216bAFlR5Xr8N1ZaT2degZVfpJAJlbAsj8NIDMrQFkfhZA5rYAMrcHkPk5kZnHZtJcJmqK0P4AhLwd7p12Od7lKsd7JPQZ6TOo1Ez5FbgcHxg7gahcK1i5wvEG0kNE9QBYfKl0Lx7Ahg9k7PDLoIIPBxX8VVDBR4IK/jqo4KNBBR8LKvg4ETyILpa5l3zM49TzWVM+YdeCp1y1YAuuBeWeWnAYrgXPBkwrqgqVrCrAwSPScxL9msCO3OLX4VRI/+ODP++D/8EHf8EH/6MP/qIP/pIEb9ctfkd4wr6aOLuIwfy0yq9a6M0oCQ02HzguZWB3LTjvbLBDhTsb3BtpuFT7kDJAjwh17sSBs/3VjKylNpXje4G8h7W/7MhhVDErimDm/6pdQ16jNYRe7/waPF7HNWQvT38/G9eQN8WMogpRxSoE3CImvSXBqYB8jawZjK4M6f+JwLdF4P+JwHdE4N9E4Lsi8O8i8D0MRk3Qy+H7NocfuDj8CHNY5OFwAuZwqyAFiMBqRiBcvS2Fit1veRM8SKEHKfIgYQ8ieZBiD1JSPF7aSaR4zHfjicVW6U4udpZuWTEq3f/7p7t0/98/UenOcLOIiraGFS1crSbNKnZeflJB9wWSyjjbz2OOn8defh57+3ns4+cx189jXz+P/YjHdEytiXsJnmcTPN9F8AGY4D94CP4fTPBCn4QgnmsZz3AJiHQokd/LeQ8OOqbysLy+i/L6luf1jeb1rcjrW5nXtyqvbzXyZXvz8h29WWuXymJXqSzBpfIrT6n8EpfKEfnShIqmju+oBKXlUHU6NIOUoXP9ywdf6YN/xgdv9MGbfPBmH7zFB28l+DTAo/m7ltU2x+0ujtdgjn/q4fgnmOMucTIQu4sZu3BloXQkEZ/ak8sMqHCbZda8601aJ0R7hGivEF0vRDcI0Y1C9CgherSFRm3US+axNpnHuciMYTKv8pD5HUxmXJQGRGU9oxIuKJfUYjhcx7znqB/9HkC/B9FvDf0+Af3eVPyf66MMKYkYGaK/1zatF11vodtFfSItargFzLwO28BF/VVPUf8XLuoROz5UwEtYAT8CIqMgwm5aOgn9Phn9/gL6fQr9va6xo7VH+iL6fSr6fRr5vWkX3NgkG31yfVVVuXS6HbmXyzNtLs9ycXk25nKzh8thzOW5NgmIy6WMSzgnXTqPiJSRimqaOTLCviYCzxeBXxeBF4jAb4jAC0XgRSLwYg5SQ1C/G64usXm7zMXb5Zi3hIe3PszblYIUIAIbGIEzgcCri+F8JriQkrQb8jYufdcNXOMG/tsNXOsGvucGrnMD17uBGywgSgEvOTfa5NzkIudmTE63h5xOTM4trpgRMYczYkBVupWOtyAHm4N7cuk03W75MzF8mxi+XQz/XAzfIYbvFMO/EMN3YTjKYS+d99h03uei8wFM5zIPnYdjOh8WpgKRegQjdR+obY8UW5f/rNKT9MCsX3uhR73QY17ocS/0Gy/0hBd60gs9ZUFRBnkJ22IT9oyLsGcxYYd4CDsYE/a8J3ZE1jJGFhx5K70Ako6+W/qjF3rRC73khV72Qq94oVe90J+80GsIMrsyB0/08TonqyDyBiXrixSFx5uYrJkesqZjst72xI7IWs7I+gWsDL/jM23+mw/+rg/+dx/8PR/8Hz74+z74P33wDwJO7z+ya+EnlFg4VDfyBnhtxcQW/NO9+vqv9xGxhSVjTe9XMHZ/A+LhEs8tbJIXKvZCJSV7xLVnkRLBtWelInCiCJwkAic7QL9b08pKrPKaVgLl9ThtA/CYUYLK63/fdzeEv+LymuOhFp2IzU/uzH+8Oxx2aR8HDudd0mM/9y5Bx4HDqZfeYz9jDk2Fa+6LNeNCTcOhmeOa87DmiFDzbIfmOVxzAdY8V6h5uUPzCq55ENa8Uqh5s0Pzx1zzEKx5i1DzAYfmQ1xzEdZ8WKj5rEPzOa5ZgTWfF2q+6dB8i2tWY823hZpbHZpwjg/VrMOacJqPVxNOrEEXYHDNJVhzjlBzoUPzUK55ONZcJNQ8wqG5nGsux5orhZpdDs0jueZnsGaPUDPu0FS5ZjPWHBBqjjg0R7nmKqx5slDzXIfmeVyzHWueL9S80qF5NddcizWvEWre4tC8lWt2Yc3bhJoPOzQf4ZrrsOajQs3nHZovcM31WPNFoebbDs13uOZRWPNdoSZsM7U1Yacp1TwWa8J+U6/mHIfm3lzzeKw5V6i5yKEZ5ZoK1qwUaq50aDZyzQTWbBZq9jg013PNgRJzLKCbYjYKNQccmhrXPAHHuUmoebJD8xSuOYQ1TxVqnu/QvIBrprHmhULNaxya13JNA2teJ9S8zaH5c645jDXvFGo+6tB8nGuOYs0nhJovOjRf5ppfwJqvCjXfdWi+xzVPxZrvCzXBDNrWBEtoqnkG1gR7aK/mXIfmflzzLKw5T6hZ6dCs5ppfwZq1Qs1mh2Yr1zwXa64Wam50aB7NNb+GNY8Vam5yaA5xzQuwpi7UPNWheTrXvAhrninUvNCheTHXvARrXiLUvM6heQPX/BbWvFGoeadD8y6ueQXWvEeo+YRD8ymueRXW3CLUfNWh+RrXvAZrvi7UfN+h+QHX/B7W/EioCd9UbU34rEo1b8Ca8HHVqznPoTmfa/4Aax4g1Kx1aC7mmjdjzSVCzdUOzXau+ROsuUaoeaxD8ziueSvWjAk1dYfmiVzzdqxpCDXPdGiexTXvxJpnCzUvcWhexjXvxpqXCzVvdGjexDXvw5o3CzXvcWjexzUfxJoPCDW3ODSf4ZoPY81nhZqvOzTf4Jq/xppvCjU/cmh+wjUfx5pbhZrwbmlrwusl1XwSa8JLJtV8ivygZweQ/8vgpS/YRRL7cknrPVKgQSXnBQ5zQWDJgwJLHhI4nYsCh1kRWLKaSxYi3yKUCkuyjktyVKRBJZcEjv3wwJLLA0t+JrBkc2DmVwUOsz1wmGsDS3YFllwXWHJ94BwdFVjy2MCxHx9YUgksmQgsOcAli10SUoHzX9kJgfM+FFgyHVjSCJyj4cBhjgYO8wuBJU/lko5VO5c0lTwjcJhnBc7RVwJLnhtY8muBJS8InKOLAod5SWDJbwWWvCJwOq8KHOY1gSW/F1jyhsCSPwgseXNgyZ8Elrw1sOTtgSXvDCx5d2DJ+wJLPhhY8uHAkr8OLPl4YMkng0oWw8Rxx+54e7qE3p4Mf+CL0hMQw5aSAu8nld+KwGdE4O9E4LMi8Pci8DkR+D8i8HkH6PeV54USa6XlRSC2BL6a0bvtIi+XmNzSrzz7e77y7Iu/8rwmSAH6JAex2PN9iMi8RLYEzfchOs+bQtnrQUt74ryigURiwoSyxOwJ/WWzQpO7M1pcPVw+sLqhoqEhMaGwbPaEeeFB01PiaBETL1xYuVHJZJRUdrRDTyhJsCyqUIz05pXpjJ7IxbPtieVL62ura8L0ObExkZCzuhxXMlln0JEoFYh2N3Y29nR1tjdHWztb13Z1dUcbG+ui1dGaDdGmxt7e1nVH0+ALtMFsNn14ZaVyUnV97dIldRXDesWQocZTiYqUmq2srquNN/Qvrl4c7++vSyytV/rq+6sa6hv6FSVeu6ShphJMvuHIJDOVhhn54qrNi6ui0Zqqhpq6qrrqhrqKE9IDB1hJklmSZJIkubqiZoPMkiRvY+KDBOksh22LwCyfogkh8iwl5BbXLiUc1URXN647Orqqq3N1dG376rbenu621nWtUVL9WluiHV3rO3uja3uaoz0dZioHdjLFNAmM4vq62ob6+qpqIPhQSJYMyZJRsmSaLJkmSzaTJW9XNoIHnyCElc0umldYc/6DRbQD3MraKfybBK1IHbMzLJc3qBm4lGd5dUVVxeKqiqpyuTmXzOYy6vKUmstmlGS53J3rS2rxNepor75JTS2vr6+tql9c01BVVVvfp9SpYYgzOna/y6iFbvcwSGGMNbpOZUgtjaUJ/b1aNqlOixmDeibbohrxjJYGi8mpsaSeGkCAFKN3A5XFhpRUrl+JQ3Iz7S2TYhk1q2hJWhcnxeI5I6sP9aqbs9XYUYMdtZHYJnV0RM8kjOLYiArf8KWYBgYAJSR5WlxLDUyMxc0BQ1ONmTHYZDGQoYacPWoWjIqMOSLwqKHk1BiJQE0l1EQXTbUxww0Qob1iimFoAyk10ZjNZrS+XFZdndFzaWNiTOGAURpbpW1WE826kZ0UG1Ey6qCeM9R2QkBSMbLr0wmSwMT6bHwGdsM9ewTbO5YmJQy7T7STaAqPzKkGjX12rEPNxAepTSWYW7LyKYk1AbntLSWxrLK5WU+ok2MQIOkMaSThGPHeVBrTjBY1qZKIJhLahzWoRO2J0tiwktKyo+szycKYlpBiKZ2kfxLkRYkPDqmprDHbWS2aFENdRETni2Ajl06TumB0QigL80k02hHsLZKj6Zgn8kFJC8G/wgJ4FhUVFRaFiuglkIXwoA0rVAA/ySPVM2qQWlLRmUsmgbvPVx97LIPaU9namnJ5yIjrmaTWZ7ewOtLCAjWwviVLlMXxxfXVDbV1atXShuOOCx8ydvPaSGsvNPu6sYXBxgUaYjOcnEbNvUFxReBG3G22DW8Qd4QYDTZsVKxWUypRqFirGVlgatvmaOXyLunGjjvusLHT1ZMlCWtSBiCrSwOz1eps9qD8xQA0/Rsr1AM7p5hY/qyua4OSzKm7rLzCJWUsFy2kVHq1IfVE/2bJRXYykY/tVCK7xV31LmM0VDK5JFRS8pOdkynotXdZ0m/fOWm2x5FdlvKSUOg/hO9KwunZojCOB59sS/AyWhFYnPzp6ZDC2xYFvOcml+WSK5YltRVrqTlpelDNqHJPWlUTMjVin7yskniCABmJRg25R42TvCdHZS0ldyeVuIoE9H5CaeuwmpLbk8nckJaifQoTqCTxwLbngoiVwFKpugre3sBytuDbRbsqJeR/MDQ35FVJBYVq2ewbtJbJI4NafBB2iukjBtr5DBuDyCu6wWM3Z3PU2tYY0bLkp57LykN6QuvXQJakjsjqqYRhR9UFt5USjzQku9wVeFxJyRm4jpTM00kUCTk7qGTpJaWYGGgDStrQ6JXDNknyiJZMygYhyE6hHZHe308aC8kiiUI9MUfyNipnIVC5X8vSvTEpVclApkgg1MhYNrSTVJTyXjIVN5K5OGk9Mnlp0k7SU2AqDYESkjL6sJZQZYPSrwL9GqLfvIZVkYlbG8oNyUndMEiS5LQ+ombsKDy/aM2JWCtEEpx0eWHIqkXl8Kuc/mruWru2sae7p71pbWs5rfHltMaXH7C0ura6qqqquqGqoaqcNpbyVUS0rZxfzHJ0OZFY19pTtaS+/AD7J2pCZoBmeAe2LCWBVTWUH9XS2lxTV72YyoXfI+/D2zRx5q+A4ViOvMQUFIXLg2qvJ/L8DPdwVUXN4qrw30ns26QPbxjFwzCDisXM76+UZDi7MAJH9UXouhkssJVGptEnXV6bYUZJ6IQASiZ/ffrf0gtHt7SVRuihRS/D7oNZ5NfkXmUz3VW3WtcTxtfvHKo64m9PtsGyH104kGaTH4cEXay4anWoIDIHot+LPMAVCtnu8OQdeQuRYrBGNyrF4tCEigoCkMgDOpb27ZS7yFy2LByesnPebNzJqt+eIHEC9+Xr1mWf8vemQsoOXTUJx6CHKokNm8GSUbNq2xLNCCqJ7MdWgWkRQqXfhrcz0Ck8aigZouW2P3l8ULNs5eahpMzStXwBye8CWU3FddjBsXxBLtsfra5fsHLF5NJljVDMXf09dHMxmYmTNkMyT19tuhUtQ3yy0MF29Z1AykkmoaaMwzcbieUL2ArkyMhIxUhthZ4ZqKwh3VLlUR1re+KD6pCywBLWxhaOaikybpCRaQFJkywvC5YcKkukqbdMIjo8O5pWly8g6Tu8T9eTqpJasKJfSRrqskoqwxVIkCvaFKN1sJ90E8sqwUmjrdyGeHdRGjsULaml1ql9pLD37JS2ZklKT1LXqYaey8RVYw9IbTaTEyXW2bbgxnW4c7aHtOlucxaRMSo2asag0qeRF2mFg7GaWGsKUpXYA7LmVxDbm7facZy3unGct8XjOG/14zhvS3Z/3gwqt0CuxPkRpNWasuwBxbADPTqZQ5EJZk9WT6njrkcX5W289OiivI2XHl2Ut/HSo4vyNl56dFHe9oAefWd0k4A26dlx10W68zVeukd3vsZL1+jO13jpFt35Gi9dojtf46g77NXbx2d/6MjYeOoQHRkbTz2iI2PjqUt0ZGw89YmOjO0BnaLwrb9byaipbM+m3Kec8NW6PpCkpo47qb9eVrlNH0hWhKftiK3cgfDlMgZHaFHj4F5lYGpMpalvUo1sF3xBnx3r4z87zG/Z1ABYio1oiexgcWyQmfUm1HR2cEYsnstAybZuVofS2V5lM1j1psGCVc8ZCJzLBbvJWw1hdJ1tWmzsaykIPMtihO1R65tWy0zqpufQ2uAkG7QkBjWz+NTMMAlnqhkMfGvMjJpWuBSgiYqPmrF060ZWSYKvGQZJRy6TMuBo2XRWTUzH4EYtO6il5mAImVTvg3GeFFKaiaZRU6Ub/ku2DpF8NiYScCauGSUc46pk44NgW9ehbDbTBd9jO3NDfWomEjsxp6SyWnZ0aswgwfbqa/U4tT8wJlMA4gHD5jmWiwTPzssHfG7MUOGDqJqg9HYNk1qXUM1DgScRPyUTH2zSiWAklsjoaQhEtsrYrOaNuawOeDsckD9M2tJ8u8B9JErAvFnLZEenxDJghzKkmv3BNDA5VwZUsFcykVBBAdgKUxthajlM/xWETINh4kG8Q+FFY9f/FhJJymAfm//d8rvZ/HQ3R3/vTjLa9DT+XfbJ/dOfg5JISUlJOByaGOC7fgtr2P+puaFt22cDhLWMrdFh9Wlc2UbMUZzbNplnpoaqwi9t3bp1WzsdKUatm7h1lVS5DfrYwIr/C79IErGtgYitrEJ/JEG9xHY+UuAF4niRAaE/uH2fR76Rg+GxEB6HwOPQAu8/94ZMajm3CH6ZY3VjMikSsSnnP4rA0os+iOO0leEtJBHb0IJ8bMwq4FEJGYuARGnR9N3f0vytiorBQKgkUo34LQIruHGxqSGvMVXDdqbesqqqKTA32oKzCOwDx9f+hbzkNe1oNiwWa1mtoyyC1eOn0X4/L1cBzGYhsRYjdbgtggnop3d3QF5eAhwvYCfZYmcxYqd4Lh1ACsyt8kUB+m5u3EoHg3rymFxWvK8jkACmtl5TVBrcEjO4/Xhw232GAhycEGmAx+EwgIRgTDTP++VDJvy/0I3S+gKj5qd/Qpu33gQxFvYk3ao/ywrMExDAGYLpBZ1nLEe9UKhCxGyoUoSWcUdJcQ0vGRgTipZs5+hCK8BKeHwGHo3waIJHMzxa4NEKj1WQjVoeJS36z+xon0zN9uscYR4crO+imosdmgEsze3WTU39oTluQxPeh4jDvuhSvve9PeHYBh+h9QfWc8xfgE2h0+Wu/g5VgQ0os2OGmkrQdRiSC61fM1dTpsXYLu721DBJHGmaM2J6LgtrcXp8U5M6qAxremYq38s+2qHQtZZSJZHo1Wltm0h/mtsRptPf1jIQeM/s01M5AzLCLu4x02swk+tJ1q+jhpLufemlMY2nqjjWoaWOzMJfZTP5WxQbSqem25va+erQVHv7O61jRllsZFBPqoYCG9VJenw2oJtrMKQsaX9DnkUFhSG6i7ukJBQK0FF2eUijBT0xvI27gydXB6gWnBVQzNF9wSW7eGf5Tup3oV7spglzOs+eXzWuDUEgO/cdnL43/d+3QnTfi/kq10Y7Wb5PSWov2Kadf5+FGgYvpwb5eyt5o9uueurzbkfTR/cHwSsj/2m+Y3bAWF1158yKm87Z0hbpZH1hKXoL7QY33SAUhrnCds00YANOMRkSs2o77vnspWGJnrVBejoll6W9jtkhxZQhWP5elVHVSXB0RkZJ60nSwopjaV1LZY1psX7ixZc7YOAtjumpHtJDTInpGW1AI8MFDYl0UwTsHU2rYfrLWt21BkpYxC2gC1IloZLQ/2fvOuDjKK6+VmWlk4tsbGyKgbUBY5M7Wc1ywRjUbAvUkORCPVZ3K+nw3e1xRcWhh9A7hAAhtNBbCB1CCL13CCGE0MJHCCEJIUAIGPPNm53Zfbs7ezqX2Ip/0Q/GN/+ZN+U/b8rOzrwtymEF1MVSbIpnYvm5zQIgwhdMEt/MoHenoOMbly8rKorZD3O+NjYAzPCiW0ljrGcBxapRKhXBsm09l3u7sDmsKBins1NfJhLWwm3qAEwIORhvQaZenC92YmTtg2KOo8D+zD5LaXCFYVqks4WsrCL0rmVxUCVD1kDuxl6E0xW8etAHm+NLeU0ifXQOGhPsIfoXhlVZykdKDdv9zeFi9ssXDPVHomHyk71aKg2mSNUpmZMI0quSkaWjvotjU1lJyDKPQE1DCXiHQnIZG0z3a3EapX54bLBHDZFhJ2wYxRFOcWOoGZkkUW4y3VlvHOBnIRgoKSyA6Q8mQKLK0npOW9IW3vUZi9PegAeR/+Z7dCUlue/v1Cf1wZShSlQG6r5x0sa0BstzuYs4Y9B3N+VuJ7DcCaxwAiudwConcKATOMgJHOwAXIbtSplRu0NhgCyhY5rvMDa60WXblWSs3HBassyo0uUk5Su5YbweR0Hx1wzzDKuCMNXKYSjTErjJLWvWz17rZ5/1s9/6GYH5hf40Lp0bd8bDtk+j7mt99Fs+gsR/RYLzAMYXFh2Safx9cOODqfDb+OAV/3w5pM8/vdWVARtS8LJ6usI/rEqnZi1s3Ev3K6l0JhzRuS8RzaSUpAaTtf2jWZBqn6Ymy5XqCvbV9Ch/Hww3480PerbCWK/3qH74npSqdIXIs44qryYVC7MCtLICLDGyxFxApc2yt3QatdTjxs38aAQ22YwPrzOAEONXlqrJYWUJmauAKTlqsu+pdHGqdNBhfDo4CbZ8oi+Cz2bqYSnMmVhh0mby+PuOTFWG4VOEA+THRKPlcJcYFMNDYnhYDK8Rw98Xw0eJ4aPF8DEmHMil5x5HSYTO4TsenBPy0EfHjnaR+H1M4knCEliEFi9iazu6eZLTLplrD4bubZwCzqngnAbO6eCcAc6Z4JwFGezNs8rbqKyKYBtlQ/dhYKcHra+LwGybOtkZFYytzXaDCQdCrdA1h/fIMeYu7njmAzysZbKFwyp1Z3c46cUxNW2cJRIEh41tCMrVdIE0fBC4KR5eYawUZ7ljZFLaEj3pXPTtJChoIkFWZN368mRUyVINMkYltGnuCMYZFXg03dWrEA16DBaVYI90RjaidD3aHUlUuaOkjRBW2XbXUtbvElnUZC4iF68OBuvJspPwvCSiRcP0cYguIOmGMHk+IU9GG6CUVDFAq3M3lVCHdYLuh5BV7QZkTduCrmokiT9lyWdDfdra25p+QCDYBPWdA8PIuTB5L4lECR0KfUaMkwFkA4vsXjHQx+/zYVr4fsXRzIhgUZTksKG1cmchoWVJqbQv+WcfVmWKlZA2VCHChVCUH+fBt94dD2lSPylQnI+rF+PSkjR7SUDUtC1qPI6WSnUoH/nSPLz/4bPleBkk19rRJqk4kytYJmxVJQWFmdTjTH4GIm3aYKkt+WvgV4MeNw6NSQfhTK5jmRSwTFYJM2nAmdwI/BhLjJQyU4FjiKHUGFuONwOp/AlB6sIZ3soyZLbwpQOEGTbiDH9Bfoxnaxo6mWmpsbbs4Du+hVS5WnBWd7GsmEl3aT9hVk04q3vz+KpjnC2H+4kzti68Eh6zaaeVmnBOD7Cc2IcTpAZhTktwTr/OE03N4225PoxybVF7tKi0GOf6KMuVmX6WFglzhZUDvAOTnyDOSQv+Z+VjxEPDaL+sI2lYCB5lR6/prqaxJT6KD183x+nx3fZ4dDTcAvcq5UqtZ5SXsE3v1MhD98BotuXSWtfB9rO3XCEj8fSMxVWCwlE1DFaO2pJVjdqSVY/aktWM2pLN3cIlq/QsWe2o5WzeqOVs/qgt2YJR25qVFaO3aFt6FvBuz8rROw1Ubul5oMK7aKN3IqgcvTNB5eidCiq39FyQRde29GSQhbXROxtUjd7ZoGr0DrlVW3rIzVK00TvkVm3pITdL0bb0kOs9rlVt6SE3C2tbesjNwtroHXKrt/SQ673yqN7SC3DvBq0evbNB9eidDRaM3m6woHY0ls080x5cyc/0b+FS1oj6hFXM9rYtXL7arMVr0+NbvIRzszPY2dw1qhWxPZOOaukGDYx7bB6TN3CSfv3vf8A5+QlBPb4MPr/IgyYGk5oOKXX3J7VUvx4Nj6dnhCkG5SizvC2RWCQ9DoybmKHTTF/dgBqJqj2RaCQ9DB9DW54OjTcDqeSkIHxeB24IWdlvE0zB2zhCoZl/WTCtJvu09AHs9P5EetunU6MR6X2fifTkEViESaXVWIIgk4P8mB9POaKl7Mfw2Tv6ArioUyJt/i+5lZC/ceM20R1yeLva4qjx8Ob7jBscqqjKYy/OqVOah72gaO3b01sfp3433PBzuP3xJAQWwn2P//47QRt7F9NddPMu5lOIRwnOHdP7lU/nse9bw/9wApl+q+cZjsD/cR73WRwXTo7CmU96jBTOXfvgrLUPzlf74Ey1D85R++DstA/OS/vgjLQPzkW7P/L9Ov8l+d6AsxGlvt+xstJDQjNdhyl3Q4cpfXCEGZ1FTfA6vIPrcByvw7u4DsfnWUecfXDE2QeHm31wrNkHB5p9cJTZB4eYfXAy2QeneH1RYR3+YtXhr3n247V/z0PHaye4KjMeVyZtr8wJvDJf5Fl/RXCqc4MPak5d1EXvMznPzk1ZRKO4jtTllRTxo1rsM2sSHCg9hbUOx05jWDHCTmcYVLxo/vzyeZXSGQwqRNHOZFgRws5imGxhRXDmbUMP80F+1t0f/IniHbOeJyyQSh78zxjn2KwfDYQzg/KX0DzGccGU71+m/sGRJqAWzjfJ/ybOeMPyHrfK5/uaRy2EkXarmGc21laJsPTmSLsWjTzFdNhdn/PV7iEcki3jA3WJxHpeAetGRaybQGDxMzy3DbaH8C1xxhWUPZtLdhKM3BNMTXqDD7Lf8eIA+js+hkG54Ptq8H/xOxtdUEmiBX03p4L+xVbQv/KCFkiooH/nBYUDxPyWY/EXtvbbgIIWQlf7bx9EsnaY3O/WmgU3+0oRpxZYhpGGDkwYLV5rU5UcZgBh76Q6I1OdkUDHN828z2+DTJTsc/4kCc35n6xzzvkfr/Oc88sgWslICk2JMQeZCRvah7Yn0r4dJH6HZRr82gmcncHZBRwFnOngzABnV3B2A2d3g0vohvzujw/u/vjgpo8P7vX44PnIB3d2fHBDxwf3cXxw+8Z3TDYuKyT7zZoqCd2sed7F5bOYy5PsXEL/zo3LQs7lhhuSAQIWACn5xUU8NTpoVK1//6BXakBbN1Td4bGpmHbY5vBky4qFGS/sel4XPUQ7n5id9jF2gWsgzfGmkB6LacmQRg/ANqjRUCZKs5mxiB2RtJJ0rrGmL2qOE3Hi5Zk4Y0xd1JRWSWbupanxV1BQQh+Bpc1tgmLzP+pz+w2gWvwxGf46/mI8E1OVKy2VYDiiE9xCCT3uwKBEJ7i9JPRotD3rwEdCl1gEN/hatRhpKHlv9Hsx+r0P+r0v+l1Hfk8zfitdejRj3I3E1zvrSYy7jOud+C4nvfYInwrWe5VhPZNUwuwKZMxILK5pYbhkGYpm4Di8Qi8dhdL8AmVXI/y/rIFwHQkldernxUhHQqv9SmdXoLW1wURDajKsJDUVzEz7lX7whZORAY14wgNhBXalMgk/iRHTB6BdleVd9XIDKXuIJdBAROBuprMcPABKw3/bS9LaBRexeEmN+vnhg8dKOKMrNX09frnRotT9mLlEMh8zlzmmnP1wo97lGibvQMOk3GblgZ42d2CacABE6ZCspwX5AOzpxJ4u7OnGnuXYswJ7VhLPgfziiIH5lfrvrfQrLRrpMcv0YdWvdEd6e7U4kEw6vF9ZvoJ0Dz2qJiNriAoAHNXhq9Z+pc3oPUojmMpMD/vlVSgvN4MHWQwe4mDwMMYgfd690sXg5ZjBHpQJonAaoxAMO8lhiNOdjCT0cErWsKcXe/qwp5949qL3mZWZ7EIzCyJ+45ftxvBRRjfaI8WuN8sRksCeuScgH0Hi/1CyJ6L0aiqMTClFVQbpl7g1tmiFLpqGPpGGPgzfKk8TFzpl2sjET3qXdc3auDdtBOHr1OUKHQTsuZbLq0lZrpH6AYWOSf9l6Znp0rvX+FvnwzYPD+dDCJc2ymCmwr2pGIw73GfWzBGbVtJeMzmKWs2tZXFLyxIOLUtK1kJDOtOlZadjLRtAmSAt24lpGWy7yUPEU1iv9qXkYfPXGvPX981fR5Ff4aVqmEznCgBwtzwFY16LmoA691AsoWdC/YAOkhksSQYmvdevEIWBqawhmYklolqSiOiDWgLGsU5dT5PI3WosqYbIv/2R+Gr5aDPPY8xfx5q/juO/3KydYLF2ooO1kxhrsHCThl2sDWLWTuM5IMp2ZpTBtTL5DJgemarjvnCmB36WB362B34OwevduBIgA1wc1D0B7MPNDTr0Adihxd19+VySTuV6pyOfR8TOkwRy0chqjcfyK1FII2RKG16aZI+aJo3PzR7QgH749rpCfpFhwZikWW+IpJRBDfqQSpYsMLVqZIwQ9u/p8vmkYC/m0r8V4uhxsOmwfv2co8yHRhwTYxW2V5/5EpyWhMWPmvBbDJABQ48PQ1pxMA4bB1sM8gWWFmS1HOC70FLvixzqfQlTb3gmkTSXeoewel8m1jqk7LswZYe3efIVxDOJG5XAWnqlV8BVXgE/8wq42ivgGq+Aa70CriMBZ0g8hN9nNYx9cOsX9lBuJMOJLskcEeEJkDUZaTjL50wfTGqoUaI58HEOPgvWJ4n6p/zy9ais2Vv4RquFb3a08K2SZdFBOsDVwu24hW/3IAc1scKaGO7YynfCwn5JkpQaJvm6aE8mlpLvEoF3i8B7IDsOqrHEXjzgXlHs+0Tg/QT8TloSIcMFTKh0gOjQk+lePRrRCd9GNLuxE2oIJhnqjwyQWrbCdBNRoyl4MlgRIQ9Rq7mJF2tZQo+PkNGvIxJXlYPUeJ8Kc1CLTpqsJdPXp/aRBuxIRuJpWFaS31bqWrqfLk46wHaVUq/rq8lPNY0KxqNCHjShelIZOjCSetBq8PkS0iDPsvQBlybF6yX/UkTNAxLcKKZgoC4eDhiwW3cetHTnIYfuPMJ0B7YypL1durMX1p0nBGVAejOd6Q3swMhPEc8UrmcdkRCs+RRDVn46S9gzWcKezRL2XJaw57OEvZAl7EV4LjN++w1DQAqz84m6uRlOVJI+0RkRwWwFaWKiUV2wik2t9puqZ9rz6WYzkfwSKkSAFSJgJOxuzFesxnzN0Zivs8akobNdjbkHbsw3vSuO2nQGa1O4JS+/RTxFVCvlP1g/37Z+vmP9fNf6+Z71833r5x+tnx+YP921/dCq7UeO2n7Magv7UdK2rtpOwrX9m5kFqtyurHK/JE9l8qcwUtWBwaY5rXoP6eNz2nt74ZNJ//AK+Mwr4J9eAZ97BXzhFfClV8C/vAK+MgMCRkDACHBT+7VF7VoHtesYtXRfVMK7oDT+d98iavPzxSVBRO/GiIb1uVxI4pcujfSmjW0Mucjhlx3+Yoe/xOH3OfylDv8Yh38s9weo383L+HyTlwn5dl62yUe8/PVbp8r9BfMyxZ4vomN3RsfzEG07Em2cMWa06QNaND0sb++GdnBDO+YDoTZMoQY/YRazVrOBdL8Wc1gcs/a7qG0yauyLbleROWRQTcbo3BTK9PaCKUr2oG08HCxNksd3pQ9qRR7e1dR0eRo0v6AU8k4koKyro6mhua6l+0ClubuptUveeXQUehdR2RQbGKCgWztmWNqxm0M7ZmLt+J1LO36LtWNPV4MiBYHXFHSP1Z+P9ljhZYXrxIY0nykTPKvL5fmmfbw51s8K62el9bPK+llt/ayxfs61ftaaP92EzLcIWZhvf72yKB+9XnnURcjDmJB9zSywoUBpU70B8rVYxWxzFLMDF/PnrmLe8q33WyC+CZ791fleUt5GvgFakU/fiMEerfWueRlXk1X56F3zflxNDsxnh1wAPcgmeQiXPBhLHsYlD8GScZtkgksehiWTXDKYb8zHdE4+wSZ5IpdUseRJXLKHoFPyjP+lC22SF3HJMJa8hEvCGy3axIDeaJO8mUv2YclbuWQ/Rh+0ST7EJY/AcR7hkqsxQ6/YJF/jkjEs+TqXjOejvfQPbZIfcckElvxY2O2/tkmu5ZIpLLmOS6ZxnjC9WZIww1HJASwJ85w7zxk2yd245DCWnMkl16A8y/g4lv0kxXxb+gt5+kfh9BcJS9Zik2zjksdiyQ6hJHSsTfuO+XTH6HImHl2SrtElkWV0gX6d2zvmA/lBOvrRig0ZYc6HXnQBOD8C50JwfgzORTD05JcdnHNZDtnoslwM+V4Czk/AuRScn4JzmVGWw3IuS5CXZYPPMVwO+V4BzpXgXAXOz8C5GpxrwLkWnOvAuR7KJ5epOZevh5dP2dDy3QD53gjOTeDcDM4t4NwKzs/BuQ2cX4BzOzh3gHMnOHeBczc494BzLzj3gXM/OPBY5HsAnF+B8yDUrbQsnHPdtI3m/teQ70PgPAzOI+A8Cs5j4DwOzhPgPAlFKy7ry7lo/Rutok9Bvk+D8ww4z4LzHDjPg/MCTINlR+RcntUbXZ4XIduXwHkZnFfAeRWc14wuE8u5LHFelg1erPwmnx6fS+SaZVmKxzSnAoEELVx6owv3ulG4gVyzLBvOmbk1G1243xqFOypn5o7NNaZ0Op8U38hHzxRnCifF89mkCBsk8pv5YIuxUZmjNK5oVFq1cESVfy/A3hJgfxBgbwuwdwTYuwLsPQH2PsHGNjQGCBKgiHvt/0E+n219HwJhJXAgxTih9lE+mp2numbnbfEjyifu3NGzygWMtDlA2t+IZww7n1EXCpXLf3cCnzqBfziBz5zAP53A507gCyfwpQXA64WAm5uvLG6+dnCzFnNT4OJGwtyA4uGcETE/YsTMAmLySbxJ6KyL0mkclpELvAIKvQKKvAJkr4Bir4ASrwCfV0CpFUD3kQIswE3w2AKT4PEFdoInFCCC/77WSfBf1yKCJ3uUBBF9ISN6AhA9hcQfiw8VyVNdyHYuZHsXsoML2dGFTHMhO7mQnS3Ea+NNsZia4WBqN8zU711M/Q4zNcuZN6Lox4wi2IKT9yQRJ5j79uztjPw9IeoXogEhWi5E5wjRCiFaKUSrMBpgqJvGGovGWgeN8zGNT7pofBzTuEhUBkTlRYxKP1C5mEQuXd5VrzTS027yPg7/vg5/ncNf7/A3OPyNDn+Tw7+E+QOG303KMouU/RyktGBS7nCR8gtMSoc9X0THxYyOdRCtk0Sbwsxnw2gIr8vCasI41pYlrDtL2HIStoMtzHifysNXIFnVkFV52MosYauyhB2YJewgEjbVCAuQMPoCkge6+T/E4v8wyj+cBDTOhx+O+b/cxf9PMf9hb4JQW1zC2uIPINJbgM4H9mFPP/ZEsOcI4mlgb4bpeRgWoLx/3PnK/lp8tW6dIjTOFNKzhO5jNqtJQhWihAJZkpGjROp0qSneD1aQFU0N9dvPpxgfSVIGI+l+RZR4b1KPKel+TemBIwc99KwBOzcjzLUr0hdT6Y55DN6C8wO06HhNjJToqf/M8TkmDQdh2HGYXloNM8ThtUVZDdUxfWlaK9PbTypnelK0joZPjqPGdutqwtLVpENX01hXT3fp6qlYV4dQJkg5f8KUExZv8hqrUxtnleuMs5Ds8Ij8/RHCjxoh/OgRwo8ZIfzYEcKPGyH8+BHCT0DhARIeMMIDLNzdNidabXOSo21OwW0zuNb50jKD2+aM7OVCzXUpay5YWstnEbFtsFhjUo/DaT0P/BwP/FwP/DwP/HwP/AIP/Ece+IUIp3QbuJvmiyyaL3HQfCmmOeSiWcU0XyEuB6L3p4zeToh+FSw5WPQ55sT2MyF6tRC9RoheK0SvE6LXC9EbhOiNCA3Uec59N1tk3uog8zZMZrtrPGnFZN4pKgOi8jJGJVxIlO8mkcd3pTV6z1bpJE5KvkeA3SvA7hNg9wuwXwqwBwTYrwTYgxgLUMxN3EMWcY84iHsME7eXi7gFmLin3Pkj2i5ntMGpDfkZGLMb9WgUzgs+iz3PYc/z2PMC9ryIPS9hz8vY8wryuOv9mlXv12m94cy/cYXuDVzvPVz13h3X+y2UCarwFazCcNpOfhvSb9XjOr0i8I7N967N957N977N90eb7wOb7/9svg+xz13zj6yaf+yo+Se45pNcNZ+Ia/4pzgVV/UpWdTjELn9GIm1vvIJnlxca9FiPrgzOWUYereV/Zg/+PHvwF9mDv6TBy9q725XuzuaO9kalob21vl1ZOWdZU12j/K/swV9lD/539uCvzeCAERygwYGVAQh2t8laq03WOdoEthrNNvnuG2ebfPsNapPCwmyMoEa6ijUS3FCQZSK1jU0KYqfkYg+8xAP3eeClFMdcAQld8hgPfKwHPs4DH2/inGyKu1meUGiyvE2hneXJmOW/uFj+M2Z5O3E9Eb0/Y/TCuVp5BxK9rBue8Oe0RuIRpQP66I4icJoI3EkE7iwCdxGBigicLgJnmGAAwECHcPTYzeJwpoPDWZjD37o4/A3m0C8oASLwakbg0fTEDfAtuPs0xwOv8MArPfAqihsaVdfQ0NTV1d7Z3NQlV3vgNR74XA+81sIDCBcc87G4XejgdhHm9lEXtw9jbvcV1xPRew2jF/Zm5HoSfSwfLuCMtNzgQhpdSJMLWeJClrqQZS6k2YXsZyEBiuS5DxpZRLU5iOrARP3cRdQtmKhuZ96IomsZRXAoXF5BIk5gEVu0vpQC37aRVwrRVUL0QCF6kBA9WIgeIkQPFaKHWWgA0ACgbn073KKxx0FjGNP4ExeNF2Ma+0VlQFRex6iEU77yETBF4YuN9kl7dfbgaPbgWPbgePZgPXtwInvwkdmDk2ZwgOs2BAcGAxDsbpu01TYDjrYZwm1zsqttfojb5qispUKNdD1rJDhxLR8DI4hNypjJj/XAj/PAj/fAT/DAf+CBn+iB/9ADP8nEOdkUd7N8isXyaQ6Wz8Asp1wsH4lZPkdcDkTvDYxeeDkin0ei79BhXNWkj+wdeiSeDswMdPXDhUz5/BHCLxgh/EcjhF84QviPRwi/aITwi0cIv4SET2PhdIvECIfdbSOGu6EutRrqMtpQ1GYdNXZyBWso+rI9+I1zs+RQ3FBXZy8ZarAbWYPBKxj5WiI2jkTJRMNakl6/la9zQ9e7oRvc0I1u6CY3dLMbusUN3YqhgPhW7m0Wc7c7mLuz0LKUJO3vYq4ZM3evK3dE1k2MLHg/L98PnQEsfSTI/3CLqStqbGV44A944L/ywB/0wH/tgT/kgT/sgT9C8MkmbmhlVLx58pjF7RMObp8qtA6LSPNc3M7F3D4nLgki+GZG8LsQ/QWIzq4Qk0XLHKbP8ose+Ese+Mse+CsOnF0ql1/1wF/zwH/jgb/OcWN5F2C4m943LHrfdND7FqO3EEJ3dY3O0zG974rriei9hdH7OEx+74Omt+rhTFRNsiXpH93QB27o/9zQh27oT27oIzf0Zzf0MYK8FsafWIz9zcHYp4wxegW/zMXYOMzY567cEVm3MrLgJpr8JSyhO8EqGi/mv1zIVy7k3y7kaxfyjQtZ60K+dSHrLMTgyK1VwADjKL/IzlFhETOLCaHffO3k6N9fI45Kihx5I4p+zih6DyKWkoily+A4iVHEMQ7/WId/nMM/3uEvc/gnOPwTHf5tuN+LkMkWIVMchGzHCKGH2f7kIuT/MCHT7PkiOm5jdMDRfnlnEm3CStM8BSvkLkJUEaLThegMIbqrEN1NiO4uRGfaUS8SZ1kk7ukg0c9IpPd/X3WR+DImsUJUBkTlLxiVcE9YriKRx4EZo0yarwiq3VCNG5rrhmrd0Dw3NN8NLXBDCzHksSxZZPG12MHXvowvGvprF1+/wnw1unJHZN3OyIKLJvKSInhDkdBCERVuxNGCLhVgywRYswDbT4DtL8BaBFirAGuzYR6sdVisdTpY62as0WtBN7lYuwGztsqdP6LtDkbbw0DbQZCjuTaRD7Z7D7F7D7V7D7N7g3bv4Xavavf22LxuJsIWE70OJvoZE2Mg9McuJn6EmYjaskEk3MlIeBRIiBdZ30rnqyRdgCUE2JECLCnAUgIsLcAyAmzAwgIdXqupIYuxNQ7GjmKMjYXQH7gYOx4zdpw7f0TbXYy2x4C2E0jUicv0aIr0zTkH6XrM0PQfiOETxfAPxfBJYvhkMXyKGD5VDJ+G4ADAHr3xDIvRsxyMnsMYBWu5ku5iNIYZvUBYCkTq3YxUMNEuXwjTrLGQpYX9scN/kcN/scN/icP/E4f/Uof/pw7/Zabfg5UrLFaucrByNWMFvjghHexi5UDMyvX2fBEd9zA6wES9fCMsw5CxlXL5Jhdyswu5xYXc6kJ+7kJucyG/cCG3I8TjOPidFkF3Owi6lxEE9wWkpS6CmjBBDzjzRhTdyyh6CyI+SCKOQabD5F87gYecwMNO4BEn8KgTeMwJPO4EnrAAD9V5ymLmGQczzzFm4KKTVO1iphIz85IjZ0TMffxBGsanV2Cx1d7UqiyJpNMaXy+/KkRfE6K/ga5L0EeusMGvm5Gbu7ubGpWGuq6mLvm3QvQNIfo7IfomQwMGGqCom8a3LBrfdtD4LqNxIoTu4qJxJ0zjB6IqIy7vZ1zuCFx+WAQ2CmA3DfYvYDeNP+96BXzkFfBnr4CPvQL+4hXwiVfAX70C/kYCtnXsR3o9bX9q8fyZg+fPGc9wHUga4+LZh3n+yqMsiOpfMqrB9LX8NWTb1dLJH5Tt3rV277d27zq79zu7Fx58kVeye/OZ14uOQtmsnizb6YArYEDHJAj917+ddHzxb0THWFuuiIQHGAlgHlceL8MSlsSiEyZ7MhZgEwTYRAG2jQCbJMAmC7BtBdgUhhnrBw/GtrMY28HB2DTG2GQI/aOLsfcwY4o7f0TbrxhtMCfIM0jUndnqbZkOV+WoKUkdzMET7WvVk5q8aw5xdsshzu45xJmZQ5w9cogzK4c4s3OIsyeJswtbSfM4ASMKHQ8gknvA9VvtWO5oxwoZvUZ6EbWaEf953I41IxcQteuDrF1hOSTXEtFdYfNljnPzAI7vo8MO83KNOD/XiAtyjbgw14h75RpxUa4R98414mIScXe6Webc8jGsv2UzH7ivpQX1Di1oxFrwS5cW3Ie1YFmOZUWq8GumCseD/H5Evrie2gAdlvfHnhbsacWeNuxpx54O7DmAeJYwjxLqB+PySWRvlJlyZHYe/UoH2NkbMow3GgYd/Up7dDiWyKT8cidK2M1lt8XlCsplE6D0uwarZPS+7zrXyHgN5vIQlAni6yHG1w0Q5zAZtpC4icKkmkjJQTd0uBtS3VCPbJmONSAloKxMRlJp5vWzdztgqjLKQfedlpBs3WnJOR05TKTOlfbXtAQ12QSXVDRNCWfgE4GKGkpHBjTjtgi/3pKy3W8xUi53lAWMwlLjUIM0+xTLPsWy74HsDZDebuk3nmNJCL/rMl3WSLku+o/ebOGl4t4otTLOfKjgJmYV2y/3Wu0YMDhw62O/pY9HOPQxKqO3qOe79PFcrI8Jl8ogrXyYaeX1EDNJYpbSl1bLwHavnHL40w5/xuEfcPgHHf4hh3/Y4V/D/QHqdzNylMXIMQ5GjpPRu89jXYwcjRk50Z4vouMRRscSiHYSRDPOXNLH7JMd/lMc/lMd/tMc/tMd/jMc/jOJ/1D2ZYAWy34wVjc/u+kEqs5uCvipmlL7o9wGObWhpsQ0dr2KmiCltrrls8wsPfYJzrEYPs/B8AUyev252sVwBDN8kb1qiOFHGcN/hGiXyHQnlA3l8k/s3kvt3p/avZfZvZfbvVfYvVfavVfZvG4WrrZYuNbBwvV4Vl3pYmE5ZuFmWzaIhMcYCW9DrFtJrDLWP6llbjKOyD8XgbeJwF+IwNtF4B0i8E4ReJcIvNsCAxx0U3evRd39DuoewNQ1uKirw9Q9JCgBIvBxRuDHEPcRyNI0by4/avc+Zvc+bvc+Yfc+afc+Zfc+bfc+w72BBuH4/ZxFxQsOKl7CVMxxURHAVLxmyxWR8AS/wgaxXiexpnRqxgKJXu221P23WcLeyBL2uyxhb2YJ+32WsLeyhP0hS9jbzrBAlv77rsX8+w7mP8DM7+hifnvM/Efe5UHN8CQf0eCZ6GOZnoNWBzS7jfK/eOCfeOB/9cD/5oH/3QP/1AP/hwf+mYVnf/D43KL4SwfFX2GKi10PHkWY4rXiciB6n2L0wjfP5HUyPbWEvzVAp87vPHA4RCDCJQ883wMv8MALPfCiYniL1+i1cITb2CFmn944wskt0BMqwhG4smqzVc9M0NtN2Vupgil75yKVx2qOqX10YWpf5YLJcy2pROKr/dZXlUQ2rGVbDQOq8UAqWDmUFJsKUVoMCgGfEjA+CDe2GCnEP79y9rl/fIUUYoKYUaQQTzOFKIMtuW2KYZ7g7+IboJOm5EkicLII3FYEThGBU0XgdiJwexG4AwYDBujmcJrF4c4ODhXM4TsuDv+AOdxNUAJE4DOMwHaIO7PY+TZpDxcyy4XMdiF7upDvuRC/CwlsNf2kvHjEd3AVVutWOVq3Brfus67WfRq37nwni6hpn2VNC5a55YXF9m9ClNOnCtjr8gpY5BWwt1fAYq+AfbwC9vUKqPMKqIchAVNr4G6CGy2ClzgIXoYJvsdF8F2Y4BaPgiCen2M8/wnit5H429k/D4IWLu1ZQzuyhh6QNbQza2hX1tDurKHLs4auQKHMokO2xdgqq1UOcrTKIbhVfuZqlStxqxyerUyoaZ7ndjhAqAdUpzWSIm1oG3dCHnjYA9c88F4PvM8D7/fAIx74EQSfCHgg+9AStTiOOzhOYI7PdnF8JuY4LS4GYvcFxm4NDDADJPqErgxZHMMnCtPG997kQSE6JESHhegaIfp9IXqUED1aiB5jogELdZN5nEXmCQ4yT8Rkft9F5jAm8xRRGRCVLzIq74TIp5HIMvv8y+no9xno95no91no99no9znF//uqTko+FzFyHv3dUr9cZPX/AqupL6RNDR9HMj5gfBFu6j5XU2u4qS+18kMN/BJr4KchymUQhX2A5nL0+wr0+0r0+yr6u7OutalL/hn6fTX6fQ35vXozfMhGSfUotRUVfvlaK3M3l9dbXN7o4PJmzGWXi8sDMJe3WSQgLl9mXMLnmeXbYX1LFNW4TsUIu0ME3ikC7xKBd4vAe0TgvSLwPhF4PwfphTOvD/88YPH2oIO3hzBv+7h42xvz9pigBIjAVxiBk4HAJ4rBqid8lJL0GzUZlp90Ak85gaedwDNO4Fkn8JwTeN4JvGACAQq4yXnJIucVBzmvYXK+5yJnNibnDUfOiJhXGTEgKr9J51uIB6+vujKJBDXr8nsx/JYY/oMYflsMvyOG3xXD74nh9zEc4LDAYq5F54cOOj/CdE510bktpvMTYSkQqa8xUsGmj/y3YvObKEv0KDWz+nc39Kkb+ocb+swN/dMNfe6GvnBDX5pQgEECM7oWYV87CFuLCStwESZhwuAehj13RNZvGFnHQsx8iGkbu+UCN1TohorckOyGit1QiRvyuaFSBBlDmY0n6owtYb/yfONLgCz4oIIPvh/nm1CCTeL+y2US91/YJK4rd0TW64wsOJglTykRL5uneuDbeeDbe+A7eOA7euDTPPCdPPCdS3Jb3islphbOoMRSG/5gxtu3Gyb29/9ybrb+DhM7S1wMxO5vGbsvQPQ9LaXlH6f6nhvyu6FAyaj4GlR5ieBrUHNEYIUIrBSBVTbQ62NSNVZ71dL2gk+C+cA4sW8+bq8nXR3hcdxei1zUoo+IcHvv2b+9AibSrS+ogJV0aix+cQn6ggrYSncbi//KJvk1l6zDkmuFkmAZ25IE49hUshFLgolst6Rik5zBJZdiyd2EkjU2yVouuR+WnC+UXGaT3I9LtmLJFqHkITbJw7hkB5Y8XCiZsEkmuWQXlkwLJU+0SZ7EJVdgyVOEkhfZJC/hkgdiyUuFkjfbJG/lkodgyduEkg/ZJB/hkkEs+ZhQ8jWb5OtcsgdLviGU/Mgm+TGX1LDkJ0LJtTbJdVyyH0tCD3NLgpEvSxLsfFHJ1VhyslByN5vkTC4Zx5KzhJLzbZILueSRWHKRULLFJtnGJdNYskMoebhNsodLDmLJsFAybZMc4JJrsOSQUPIUm+RpXPJoLHmGUPJSm+RlXPI4LHmFUPI2m+TtXPIHWPJOoeRjNsknuORJJca4DP9LTwkl37BJvsklT8V5viWU/MQm+TcueQaW/FQoCeWxJOFmOpU8G0vC/XS35GSb5BQueR6W3E4oOcsmuSeX/BGW9AslF9kkF3PJi7DkvkLJDptkJ5f8CZbsFkqGbZK9XPIyLNkvlByySa7hkldiyaOEkmfYJM/ikldjyXOEklfYJK/iktdhyauFknfaJO/mkjdiyXuFkk/ZJJ/hkrdgyeeEkm/ZJN/mkrdhyXeFkp/aJD/jkndgyc+FknBfxJKEKyNU8m4sCRdH3JLb2SR34JL3YclpQkm/TbKcSz6AJSuEkvvaJOu55K+xZKNQstsmuYJLPoIlVwkl+22SR3DJx7FkVCh5lE3yGC75FJY8Tih5jk3yPC75LJa8QCh5tU3yWi75Apa8Xih5r03yfi75MpZ8QCj5nE3yBS75GpZ8SSj5rk3yfS75Wyz5gVDyc5vkl1zyTSz5lVASzmVYknA0g0r+AUvCAQ235DSb5M5c8l0sqQglK2ySVVzyj1iyRijZaJNcwiU/xJLLhJKrbJIHcck/Y8lDhJJRm2ScS36CJRNCyeNskidwyb9jyROFkhfYJC/kkp9hyYuEktfbJG/kkl9gyZuFkg/YJB/kkl9hyYeEki/ZJF/hkt9gydeEkh/YJD/kkutsz59Cya9skl9zScmHnz+FkrANhZ4/S5hkIZaE/SjB86dNcgaXLMaSuwkla2yStVyyFEvO55JjfHnWN1ThUTy3j8LV8Zjm071AgsZszDnNpTnH3C/nmK05l7Mj5zS7co65gscsQKGFqBRmzAN5TI6KJGjMQ3LOPZhzzJ6cWdJyTrM/5zRX5xwznnPMI3OOmc65RoM5x1yTc+5H5xzzuJxj/iDnmCfxmMWOGHKe/a/s1JzrfkbOMc/OOeZ5OdfoRzmneVHOaf4k55iX8Zi2fU9HbBrzypzTvDrnGl2Xc8wbc455S84xb8u5RnfknObdOce8L+eYD+Rczl/nnOYjOcd8POeYT+Uc89mcY76Qc8yXc475Ws4xf5tzzDdzbqM/5JzmuznH/GPOMT/MOeafc475Sc4x/55zzM9yjvlFzjG/yjnmNznHXJdzTFh65xazMOeYxTnHLM01ZvEYugbP24jvNY/1wfeaJfgH3oa+CDmM8+W5XweOF4FlInCCCJwoArcRgZNE4GQRuK0N9HpDOdXHn1V82wOxJfDGl36n2rejz+CWvqH84EvnG8r3v0RvKHcRlAC9ToZcrKciyIg+FU3HT0WQnet5qmx6rq09ZpfCvnA4P78sPCW/t2xbaXxHMhLSFiq7VVYsKF+wIJxfUDYlf5eifiO02IQLmUDBHnNWqsmkGk8Pt+phNQrn4srVVGJon0RSD2dC6ebw3vOr58+rKqLumLpwWEnrcEE9bU/bV00jBJbWdR4YWNLetjTQ0rx0WXdXx7KmzqYA4aipMdDavrytO9DS1RDoaqWZ5PX1p9OJhXPmqGsqa4l4TfmAXh5LaaF4uDyupedU1lSHFvTOrZwb6u2tCc+vVXtqeysW1C7oVdVQ9bwFVXPg2gKYFzbKmjKKMLdiaG5FIFBbU72gtraisvyIRN9sKJYCxVJQsRRaLIUWSzGKpWxQNXJP3tE+G5JbOL+INF5hvkTcUsJ8sX9exfzqmkBXe9uBga6mlkDl/EBVRUWgvasr0NJU37I/cdu62ppWGWUNb1rKjbwNyufV1FTXUr5nQmEUUhjFKIxCCqO0NM0ipZmtQHFmkfLMVtav5DkmGibMlE0p3KWg6tSrC+lA+B3rr/A3FnqTNuKg6FdWaEmw17B3ZXlF+dyK8gq/0pCJpjNJbe+4lkkn1ahf6cj0RCOh/bXhbn21Ft+7tra6onZu1YKKiuraHrVGK4I8AyOPv4xKGH73hBIGWddrU2NaaTBB6O6OpKPaxGCqX0+mG7VUKBlJwKnfCcGoHu9DgBykX0UtC8bUeKZXDUFxk82NY4NJLa1GolT3xgZDmVRaj3VrQ+lK7KnCnmpfcLU2PKgnw6ni4KAG51DkYAQOsZSQ4kVCkXjfmGDImDgiWmpyEC4K9SXpYeQuLQ0H41JTReCqWHRCkGSgxcNauJ2WOjXJCZBI2wXVVCrSF9fCdel0MtKTSWtLk3omkRoTVDmQKg0uiQxp4QY9lR4bHFSTWr+eSWnNhIComkovT4RJAcPL06FJ2A9fGCfY9sEEaWG4QRVZQ0t4QEZL0dynBFu1ZKifnguGI8OsfUqC9UBuc2NJMK0ONehhbVwQEiRDIs2kKEiCV5cGI6lGLaqRjMYQ2gcioETN4dLggBqPpIeXJ6MFwUhYDsZ1Uv6xUBc11B/T4unUFLta1Ksp7Xsk6nQRnMokEkQXUm2Qyh7ZYtRZGWwvikfLsYsoBBVNgr+CPHALCwsLCqVCCT5sXwAO7VhSHvwkTrxrOEW0pLwtE40Cd4dXHnwwg5rj6eoqvxJLhfRkNNJj9bAa0sNy6mA98+apc0NzaysXVNdoFfMXHHpo0ayRu9dKqr3Q7WtGjgzntKAjNoCVcXplAQQX59yJO4y+4U7ifonRYMGp8qVaXCMC5S2RVBqYWr+1ml/ZLMPYoYfuOXK5utKkYPVqH1R1fs5sNdm7PQgfkwNN/0GFenTTNBOrnzl0rVCjGW2ztVdRSRmrRSNple5ITDvSu1vyKJuYyOc2KZEd4qF6szEqlYwrkUpKbt80lYJRe7MV/d5NU2ZrHtlsJS+RpP8RvjkJh/FXhnk8x5U2fMU0ryK3uCRC5XwS3NIkF61HDvCsG12UiS5eFI0sXkYmcqVV7YtHeiMh494bmN8ct2gOCYQIMI+E6OU5ssYb7urX01ZYfVJXw8oSHcJbtHgfGGEDM49kea2wWHNIPmDwIa8QSldaXFlRQx7SauAeQd53O2yuUpD/m8MaCYeLTF10EUcPerep0JxWpK4IK0I8HR1WUjGVfhcFYtJj4+R3mqxhlZAeS6hJevUtSu9x+wmuUQOBqFGUfqhVzFarNWDaFGSUSEohi+Veosy0UKoyGAlrClnuklLrvUoKDOjCofVUJJ2hsimrmCvB3J1qFIPIpyJrNIUodkQb0MJKJK6QwqWVnmElrA/GSSAkA+VTM2m9VycPJEpMT+tGvXRGbAQeQZVUWu2J8BlByZDVNdSMFJUWWe/thft9qpLQB0nRM1ElFhmCwoJlTyISJYtxmqiR2JEZFRC/rZ5WvUJaHO4EppTBfi1JCmdQ7DdrxUw2UPIhd4L3ROJGyUiJoOCkfSxSukkNe6gq9FJViBqqEOKqQMoJJNC0AkpvkjRE5fxYzOCd0E4aM60rpOUIliZPG4aJwVlV88CvK9U0gJBbPZf8qx2ZgSulWjykzSapxdTVcNMgTdKhVwWitO0D9rafeWRGT++VppZpjN9GYTyaP40avEFNgKIq2lBCJS06QKoBlhFDagJ0D6wJxYBTUOykGkkzg4iJqKamDFXuiWaSSaIcPWpodR955ovDjVmiLnElZfWGOO0NzrzdXU+ZRUaW2YSkUH9cj+p9w0ooQ5IAbVNIPSEv0nrkqTEMSshNb/QTjmg2pByZEKErqUUjKiEQZGhXae5qV1LsmRa0Im5pC+E9rCZXK1p8IJLU4/QJyiriop7F8EFIqjaQQwtokvGErZBHedKpFy6a07N4vfs57dSsP9O28tsg0sWJIgLCNZy2J+zlgdKtX5+u+4/2X96paAGzdldS5mhUURMJ0GJSFbjzTZoyCS2iK2HC5vp2Vls7VVYOifoGjIuONtqg/qwYnRXai/winVcxe6/Cu6+C+2969qbvvMjei1Wd5ZRDonEK6ZSRKPSLbH06FVcTtCNCgiQKXG6HOL0qdB2VFJYVqT8TU+MBY0+EFcwYzGBcjmiD3sNDSu/1GBpMo6do+owratiwj2N0AmuIJCWmWkd0B7eJSWvEnHndY42QKqIm7kHHoR1NpAGJjoaNqniNUX6rOKCp5F/yZGY0WpZRSlhr0CI6cgGTVBhSievJGCgMqVK/Fg3bTf0ISqVGU/omGwD9Sh/cUCNtqMVJCUJ8nNB6YTAi2gKLWWiTZXDXjZavm/RuOixSCw3xdERp1Q1jB1CjmE76t60d6MIsYhgyoqMc1JcUOWtrpAd1palRmdU0RNQtENUHqQ0BYzE9W+mLqqRURHtoHWhBesGmhJpK9MN6nhDGA9lwWwe72MZ4S5tSiyWi+rAx8qZIuxHFTsA3/oiqR5Ih+rU/0omSVMF6QaGBPVLsYRi32HqNLD56orRS0GKsR/RoZIiNkIUNoZT2BDLeGQOuSoi2M0NfUS1pbqjrbm5v63LwwTU4NQfs/DgCqeHF7uGEpixUPBa+aMys4zWZ1aoOlc9eqPTOqS6fS0a82vJqcbRI3IhWVUVi1VQojpyNLd9AE+N4oVJZrfRRzK9UzrMaxhKDT9vHMjFYYhMqSFumqQooNXPL51WFbIVlbahYqdeYDetXqkAtBGWuj5LnrYXKPNKvyI+UMqvB2Y6zkZhtpT/LvhxLk5rXdXQtVKrmBYwVmxGhyYywkD0dxWI4TVsTLFSGlIpyMnfYGtTY8zQUtpVoAymVUUV76zZGCGpYpZ5Vl0gkddpotfNJmgtqy8liEi3jjRUKilZTW9Vnz7UOBoxmYylSrw85MlsC1jTYooqwZQx0agJz3NIQ6FpWWVVjBIL9X/Rg5jNfwsrwwbV/SPCQ5q9r6VhW5yerHL9Blb+lyV/fUtewv3/Xiqp5VTVV8+fWVNfM9VvrIH9Lc2tzd1Ojv6mxGfqDnwy+/q7uuvrmluaDmjr97R3dpKe0+MkDqL+hvbWjrqHb33VgV3dTq7+hrrWps87f0NXAcmttxQnvOrd2PilUReUCf5N/CdH8wNzyWn9tFYm1Wz1p4QU1C/xDULVQKuQfCqeiSbpOoxAZrAbAxJt/VWNTQ1VN5Vx4CPbvWrOgYu7cqpr58+ctqF1Q9MV33323Xjve/N1NURDWV3mFRf5cpZeT+Pns5XMRUbC5FUWfk9zXSx5eDRQPwNZnMGgcM6VNCB/o8sH3qHz0xTe8IS/1TaQufT8+yciyoqICEigZ13HjyR2hY19dVuqjX+aAmPK2xBnXrQ7R59qlYMb60z8FGsuWvrgM3tvTN37yFPJj+ojvFm+fLOX5pkK+2xEHfJJk+YvGbcx7AzkI79aH5WAIjEUU5uXAHk/oYLobQ0nz7cgOdBSN3zTvIpzFqt2QJHEBp/ETJ2X/5W86Cig79D1nURC2KkqCA0ayhXl5FetXaEZQiW8ndn6DNiFo+3q8TwGZglWxqETbbWfi/Ktq0T5DsajCyrX3DFLfGWSJENLBbsDeMzLp3kBl7Yx9Fo8rXVQHzdze20VNWpEnLdJZSOXpy4gONZIkIWn4UEF7zxHwQEhSJfP/UCq89wx2RmBwcLB8sLpcT/bNIX2ncs6q1pauUL8WU2eYkSMjRw5E4sYsPIOUSVEW5VYcGpfEpsEKyWhhmixD9p5ByrewR9fBKPaMxelkRls0h0bh8UmKi5epqab+XjI8LJoDXprrnPXIdlMWsZcsoz3K2KqSBW68U+shbT0KSupBpl1ZyZNBIkHS7SKdhAwKMGclU+UrI+RhpCfSqKZVDgargk2wbtXCo6BqXo2woXWr3orrVrMV123uVly32q24bvO2fN1SNN4MZQ6uj6Cs5hpgFDTDRozoZFFCVmxdaT2ubXUjuqhuW8uILqrb1jKii+q2tYzoorptLSO6qG6jYETfFMMkoPV6eqsbIp312lqGR2e9tpah0VmvrWVYdNZraxkSnfXaiobDbr156xwPbRXbmgZEW8W2phHRVrGtaUi0VWxrGhNtFRtNg2KWbdwmOJ60RuvUUnomGdJSo2yPokNNavF01+rMKGBxI6aWpbreF6VXkzbR5LJoznq9HllcNHFj7rbsBi8sg2C2mV7m61b7JgTpuQ6tXkul2+G06pRgD//JTjHQC3tycDASTvcXB/vZNbywlkj3TwqG4ChUnOSjxRLpbnUIbuEl4MaZnkkhcEcesYM8ghFGO62rgKlppoAgsCxI2B4232g1TqZ++u0TCxxrgWaM/ojRfFpygKQzwUgG3jQmh41bcxSghQoNG7l06Km0GoVQIw1SjkwynoLPmSTSWngbDMKB4kh8KobQFcgdMM6LQlozXD9siHTAf9GmGKlnXTgMR9WMLOHYj5oO9cNdmFZ1yCgXvI1ty8R6tKQveGRGjacj6eEJwRRJtltv0UPsXCQFIB+4iDjV9NWF+TfaAN8xmNLgdagWpvS2DxCtC2vGh2jGkjA1Geqv10lEXzCc1BOQiGK2saHmdZm0DngzfJRtgAxI060G94hRAtcRI8n08PhgEr5XENOM4WAiXBFV++ipEAOR8vLgbh+900dv+tG/PMm44EcCSLBU9L2R9d88VAKvLP/T8bfwdbEtnP1Dm+iSlavzb7YX7v/9NSjxlZSUFBVJY3J4q9/IOvb/NFdav3vxQFjjyBKt5pjGhS3EmMX5kSbjOx1SRdG733333foOOnKQHmrih6rkOeshj89V8b+id0gh1jcR8eEq6W2S1LvMYgkF/kA87zBAessZ+nsU6psJzh7gzAJndp77z2lIhR7H+x78MubqumhUFMWinP8ohANe1CGe4/YpepUUYj16kMfRsnJw5kDFfBCjtHCbLd/TvM8UFcPxoBJfJeK3EA6/bRWXkLMepVqwgaU3z1RVkX8UpliFcCxw67pvnJW8+o2thsliNdM6yiKcefxvvG+blascTstCYU1GanBfhAOg/723ebPykoNZMKvIJjtzETvFO9IJJM8wcVWYw9jNj7bSyaCWOOPKiqfZEsnhoK37ICpNbp6R3E48uQ22fQYGz3wLwFkIE4gEcyI9lcxHaWq5eA8nSvUFZs3//gVtVr3J5aiwq+im/izKMyyXgVeC5QVdZ+yNRiGpXMSsNEeElnFPSXEVbxmYEwrnbeDsQhVgH3D2BacOnHpwGsBpBKcJnCXgLAVnGTjN4OwHzv7gtIDTCg79cC58Zt7XAc4B4HSC0wVONzjLwVkBzkpwVoFzIDgHgXMwOIeAcyg4hwF91byqVOX23di5gN4SqLGlOTO3MZNKzrVJ5nC+3RpV6M0CGAbWY+jYgUQH+0ml3EZWc9hmLstH9Rb2kYxfgI2ny/T2XnYPZkowpcXDdP+H1MK8RzMxyKw9NccHSOHIkDApqGfSsAeoh1bXa/3qQERPTuA2r4ZbVbrHU6qGw9061fIx9Kdx+2Eb+tvcfoLgyT16PJOCirCP1BrlTbGD3mPNX6tiUaf9qtJghJeqONgaiR+Qhn/VIfJvYTCWiG9jGb/iu1ITLDNZVLdTZcHBfj2qpVQwaEXK42Goytj7IW1Jx7lC+OJTgUStPZWUSFIOA3S7izTa0GOK1tOK0LjKHNSCswKCGWo/qGQzW6DaROM96MUWWqgnstgG0kJgt2BTP/vT57Uvl0n0to3xCHk4jG/m9SlZzcvdREgPqBc8EafIvz8lj5EbpKQeD5S0cPQuEjyn8p/Gg61GnNJ9X7z8ikdPeXWZr5cNhKXo0bcf/PQyUhEsUDZoeQN3forJPJzWmvGwZ+1Hy9QgHxnm4Mo9DDnGaBRUY7DnviSpaWPBvl5STehR0r2Kgwk9Ek+nJgZ7SRDfY4HZvjiox7vI8DA+qCcjfREyV9CUyBhFQLgDWkR/mVvK5uwMO8d5dBesRCqRinJYdnWxFJvimVh+blMAiPBVmsR3UOh1Lej1C8BSC1HqYvbDXCQYuw5meNGFpDHWs4Bi1SiVimCtuJ5rzF3YBFYUjNOpqS8TCWvhNnUAZoMcLDwie5DOt0kxsuBCMcdRYH9mxLE0uMKwP9jZQpZzZKwnHb04qNKb1zlbhBTOVfC+Qx9sji/lNYn00QloTLAHbiXDUjDlI6WGdwzN4WL2yxcM9UeiYfKTvc8qDaZI1SmZkwjSq5JhpaO+i2NTWUnI2pJATczGgB4fG0z3a3EapX54bBDsApDZ3bCcKZzfxlBbk0mi3GSus15zwM9CsGJYWABzH8x+RJWl9ZyzpC281TQWp70BTz//zVf3Skpy31SqT+qDKUOVqAzUfeOkjTkNnm/k1TBcNFATDSk5ij0x7Iljj048S5hHCZifJ2aIX1kSicYsX6sWjtAr7smYmrbgFmouQE7ghI/EnuQmzCWFEnbZ1y5ltrUzMPSW0NHSN2DNFnlFp5JReMMJzzJXSyeTlE/l9rnXoEIiu9zweA9jNzy4yUeBwyp1NPp9DPp9LPp9HPndaKeOcUIobVDjsEvYFlkN/3SQgU4d8itdkb6YqhylLNPiyeE9UkxKPp6kVL0BKcknEMEfS/bUwCQ3/YQ0M/BivGwGGw16rxJmmTDDIYZdiXIFDloYGbF8WX6mz8jW9Br14L5uNZY045pWWKbLPyCle1Dqh9LBp6vpvyxnv2m6BA4MKMTRyfLdb7dEgjxGPL+z/AxFtfHbjAZZAqlokiPWT7KGUuhde0dklJ58IjyBGMwGjBp6KvlJVMmh6/tOBueUPGsLShpk6mgpaAYr6BmWYiH9XMj080yi2vJZ5Me2rZFwoJOa7KEWN5gqnu0ddI530LneQed5B53vHXSBd9CPvIMutAUFIGgkpi+iTNPtlovBuYStAukArCFejfghzPRlniWxiC9exAcoagQwl+W6a6+L7iFdAc6V4FwFzs/AuRqca8C5FjLYm2eVt1FZFcF21Ybud8GOGnqkKAJz1upkZ1QwQj3bDSYcCDXV0hzeI8eYu7jjmRsWsHzLFg4L853d4dTeT9o4syUIDhvbLpSr6QJpGIua4uEVxuJ4ljtGJqWRydC5zt1JUNBEgixCu/XlyaiSpRpdIT2hTXNHMM4CwaP4rl6FoAbkkhp8rWFGNqJ0PdodSVS5o6SNEFbZdtfq3e8SWdRkrpsXrw4G68lKm/C8JKJFw/QJkK6Z6ahHHsnIw+AGKCVVDNDq3A1S1GGdoPs/ZCG/AVnTtqDjiCTxB0s6JtxAngfoXuz1MIrcQJwJ1DJRh2VfqmgFGUA2sMjipYx8E3EKvl9xNPtMYlE3yWFDa+XOwjDZbjwZl0r7EncfVmX5VvJjYmskmSQPJWCQyxgib7SRcBv8Mg1DSe2kbCv4MHs7KzijUmolAd0skI3rUqlUh7O8C1qN2zm6yZbTPcQZi00oSctwZvexzPiJtSXCzOpxZg9A2tQqVaC2vPpmW24PQuO2qkPUfhS39yTV4RwfYjkWshz3EebYgHN8lPyYRlJlxrAgW7K2jsQNq1eBmopbbIV4nDhT7Ha0lJkKtZMlLcRFeZIVhX5TnWQ+310U0riNuCjPAJ3U8GuTQjfPbrVl/Ry0PVtNtsLekdG+1TjXF1iuMiOgUkhAE871ZfJjUmNXS6cyq66jK9CgdBHV0ZOzf27L/FXiTObPGjC6pSOGnT7Jj/P/Dcu/mNV6T2Gtl+D834AVQlvdbbbs3iTOuDpuexDGUWl3nM9bLJ8Sls+uwnyW4nzegX7GzIz9wpbZe9CmYsNk0s441z+yXH0s12nCXJfhXD/MM+wzzRu63ZbnR8TZmStzQ1RPaYHlCbv1Nmkqzvtjlncpy3tbYd7NOO+/kh/bVlYzK21zKueZptTusJXl73nwqVUjEo8hTcCZ/4NlPoZlPl6Y+X44888haN6dtoy+hB7cGCGPDkm1L2aYaktJPpzTVyynsSynYmFO+6OcKHYXyYe+jfsGElkL2kNHwCUatQ2ZkgpYJiAvr8uzDd+ShDKxBt4WXB3Ygyo4UEvdbcuogKCT6Bd3lC5sLlVauw7VCT40Yw66JLev14nq1IqzK2HZ3WPLrlSC157ccqv0Bc5krIQGW5LsP4WZtOFMyiToefq9tjwmSmC5KxlJ6GEFtpnUpPQ3nM8kCQ2xJOVPhPm0OxvoPpIJfVM6hUSUpxKnpKN/OAUmGqWP1qG22V6yt82H60Rt04HrMQ3WN7VVsdj9tkx2hposiUTBBG93f1JTw9J7uCaKo1neEdbkAJzTriRsDLK/90tbhrtLVL0tm32NQy2zpTdxnns4WukNYZ6dOM89oXZgzO8BW2Z+cIy3mdKrOItyRwO9LMyiC2dRScKK2+MK7Kr/gNABL+59VZBBNVSYcwizzXMsK/riZS5qK8jrGdxW/JPuUjfOaj4Juz6/G+wIs+cOpZf1US8Dln7TMLFl/NTPzPkKLbX7kZlYl4F2v5LaeMPspZvIMnupwIyz39yrKVdKSCPCuzbfAmiMhaBfXY5XENLjuPEX4RYh7D/qbhHS+Mtxi+wDTYxMMfpsedZBgq0dbdKDOJsG1HUgmweE2azA2SwBkTZtsNSW/DIY0Rr0uHETQ7oHZ7If6iuQyV3CTFbiTFqBIb6DOtMwmpsaY8uxHYYevgMu3YYzPAD1HMjwVmGGq3CG3ZK5D8XW4mNtua2Azks7zg04p1WStTiEnK4T5nQgzulgydyKGmfL4lDijK0Lr4TXSHQSl67CWQUla0UIWV0hzOognFWP5LUXM96Wcxjl3KL2aFHpJzjnXslaC0LOFwtzPhjnHCFhuxufPtCi5ntl+pwzqye6ejYtxqy4NjS7zFaS1fQhDF61SRfgIsQka5kIRThPWIRDcBHg6HJDFnuo1OKpgsyhVi4gXTxtGBVHRlGPIn0DjkX5joQCwp74xJX9anqPFLLAKp3JCgvlk9OOvnu6cOY7NM8xs25bYOyU+wYhoyEJnk/Mm00KvOOBRTv0rpPxPLvGMc/+UJjbYZgaiDu3ShkGOvAX7JRB9olKZVakV0lkYHckZVifb1DjalidPcVWxmOhC/LPWkrH4QY73jEnHyOcvODNPhxIlE8k+OUL/2dwccRrsOgcQUfS+LziKLsGS097GOeERsFFWC8em+P0LmV7PLoF7YdF4ukZiysEpaNlC1aO2pJVjdqSVY/aktVs4ZJVepZs7qjlrHbUlmzeqC3Z/FFbsgWjtmSVFaO3aKN3Fqjc0tOA95BWOXrngcotPRFkKdronQkqR+9UUDl654LK0TsZVI7e2aBq9M4GVVt6yM1StNE75FaN3iG3avQOuVWjd8it2tJDrvfKo2r0DrlVo3fIrR69Q2716F2AV4/e2WD+lmbNu4cuGL3dYEHtli6biDbz9mdwJb/9uoVLWZW1lO1to5rENj2+JUs44tb4Sq1nVG+LV2/BheaI5LXpnVpKSw5s6R5SnVUFO5u7RmHzoh6cSUe1dIMG1hVHcWO31nWwC8SbxzAqXH1e/9v6cLF5QlCPwwd/zaCJwaSmQ0pwoCrVr0fD4+mlTopBOcosb0skFkmPAxOYZug001c3oEai7FPPjWpaW54OjTcDqeSkoBqNgrUEzcp+m2AKXhOSdjbzLwum1WSflj6AXbeeSG0zkN4EEal1hon03gTYDU2l1ViCIJODUWaGgacc0VL2e9Ps0EkBmFUokY7McgefFBDS3sRXRUvI37hxm8jSGLz2bXHUeHiz3fykB/bZmRp2tr00D3vJ320n/Zxe0498cvavbzz11WW+H8KZhEK4oP/fb8FhYy32uItuWuw5SbJ4lOA6J7XCc7JkXJGiKFzspB9yPUViCPx/Eo97Ko4LF+TgZhu9LQcXZX1wQdYHF2N9cCHWBxdhfQl6rAUcuLHqS1lnM5BzgcRPAPouJD9LS30/xmXtX+e8eNe7Dl0Qg5uh6MbdKbwOP8V1uIjX4TJch4vzrNujPrg96oNroz64L+qDi6K+48A5Hhy4qOmD+5C+E4V1uMGqw02S/RbhLRK6RdjpqkwHrswZ9spcwitzB9d+8ld0RV5uBiiE18ymLuqiBiicN3+mLKJRXBeC8kqK+EWT4soKsL1QI11JPFCIfAxexcBiDP6MgVSv59XWllfOk65mWCGOeA0DizB4LQNlBBbB8fANvZEEWVo2G8ZY16tSO2a9FFUglTz4n7HkuNlGFmgrOLon30maclJ9Xbnz7pPvLlOF4XQ30AwH7uR74KBkfX257ZC8714zMhwxhshwNE6+n0ZuYImzU9u+X5qRq1hkODYs/wr6onFiOOV70IyzgMWBM3vyQ8Qz3jAYz43J+x42ox7JosLpOflROGAJB+dSzfHufq1eH/I9ZsYcZDHpRfEniGf7gw4qFx9+8z3JpQphatkqJtaNNeEpLL05tTyNhtrik/gv+MvlOqx7zoJky/jMBPcb6EhTwMaMIj4iwP+n8Nw22EzgM6A2BWWn5pKdBFPVBHPohtmKzirPMWEaByYvOmg/LxmSVPqnG13QF4yC8hmsxCyUoMDSDbaC3sQL+hIu6C28oC+TH2CPDf4vvsPWfhtQ0EIYSP7bh8usHSZ3609mwc2+8gqaySUYRemwa0N/KUQfFKIPC9HHhOiTIrT4aZti5jCzCscCqqGvUg2VoEdtmmUVNxfwtmNJ9S5eUu3iWlLt5L2kKoOuCmeos/ZzSszznJiSDe2xHwLZfwLnI3CopYOP4ddfgKh86QXJsg7hA+sQPrAD4QOLDz6w7eADKw4+sNfgA8sMPrDB4LswG1FfSna7Cl9JyK5C8TqnXYUijFxmJ+qlnIl6mROlbChR64AUKIUPcvLBY6AvH5wCcArBgVWoTwanGBy4/ezzgVMKzhhwxoIzDpzx4JSBMwGcieBsA84kcCaDsy04U8CZSpxxcvErtnGvav27ODXiAF1gQ/sQPOoW0zGnOTzZshNpxgu79lhEGx/OXQ6nBcpdwPBAc7wppMdiWjKk0dPUDWo0lInSbGYsYvuLVpLOBfH0Rc1xIk68PBNnjKmLmtIqycz9OGH8FRSU0G0LaXMbedz82zPcQiIfr8w//110H4OqXGmp9Dafo7fLR4+o7/I5evt89Dj7IRs44Hq6vAMJKXNYgJJ3FIHTROBOInBnAjZxsAOsDCozwciQbpqPUgJKe3Q4lsiANSE9Tpayhvkhl22kXUhSe21IUsyEkayQBC5y2kjS4dtKKXo7Bj61pPQkSb9IwT2SrFkxS0l2O0EppWfYKkGHGldTejwSEhXGNKRkGUmaTor3VC5GktbXOJJfSWp9pFMmTT8190izppcBLZxUM4V8NIYrFWfqqUgsEdUcsDyDVGcXxmGAsRZgRkoCJOFAfWbYvf2xW765/TEz3z5Xz8KK+9tvnXP1b75Fpn38Al1E2yF/YmoPFnfkchJ3DI/b1dIpz3ECFU6g0glUEWAfBCCNNMxnNc6trKjgqt2p9WhRt4JXk0TK1y8RuYbInCt1q6s1qsIxnahwTCMKRS+RUtVIKdoAXJiKpPsVUepEZx3az5QbUJQXtwTWGICXAMi+l6KmHVWZLs8l5brjP6PM2JCXiTk9brEUXPEzgkK0UknaCgYSpzWD33JtPtimYHpLeOK669bV+ZauLnTo6qJ863FKetSlqw9jXd3XoUxITz9iegpGJeT6fLB8guzxyQ0upNGFNLmQJS5kqQtZ5kKaCfKYVD03FnPYBCQlJm5dR5cFQSxznGOjXosWCal+01IcSyNBfjbocfZEZAQrdWE1AVs5lo27JZkjItRCih5Nk0SWqamUFu2JqmESRSdDkh4hv5gKkic7IpSM6EQA7JvqPUSiTR9Qla6Qno6o5fJ+vHLeTdtiNW2bo2k7WNPS6/Q/dzXtLbhpu500orb9mLUt2PWRV8DS0WZdUV7phla5oQMBihmQsfEkH+SOdbAbOsQNwQrjzAKyGlU6cJf0MPpYn4S5TeWNQ1qRNhVum2V6MqXF1LgV2kpWssOq1bCd8FBPpOv1cESjuhBL6HEwBkIwjSxJB2lOYMIVoIZ+WJGCZtQ1IDWpV9PkBylqY0RnUGMyMgAJ0k8uGrrVnekxNQ+WkvR3PEyjGxZYukJJDRRwma6TJUBrJBUiRVDjmk7CjK/y0bd2yv6RdEphH3Mka4fuSDRNahYixKXSYaVbi2p0GFNWwsV7w6IMutDMNPX94y7GutrNDR/ykQCK6JcPs5opYLSAW1kPt5S1x6GsYaasdCvnJy5lvRgra79LI5C2/oVp65Vgf/AI4oyvM0xJ8vFhtQCLCrCYAIsLMF2AJQTYkQIsaWHeXTxtsTbgYG2IsRaC0JO/dT7s/hCzdpQ7f0Tbl3wtfgxei38lCd7GSOsYxecAxccRZ4pYf+Tjs4SdkCXsB1nCTiRhDeIwsvygVj6dIzq9M+9axPyQJFS1/gnJJxG5s6SlGlmWKnFtkCwuqCGmOF/D0OWLR7qu9QdbwHhEJwsew1Cp31Ee15L8ZFKoh/8zqxibpVJqoQP9NCrvAkwbHwx1p4HiYdOlp/CWD1A2slrW9J1mdYwz8u27QGflsyUN/H+kq2PouGOc561tqIN8t8l2rnyXWeW+wlHuq/LR7tWhrnIf/K3n7pUEMlDAs6FXXk+cba3xHXegG7yDbvQOusk76GYStLcwyMMUsLsv3kKS8K9PEvKtROJsiazLqKk51vVI9+pLaio1pNuX7md9UVUEKZcr4sdrQ549W0cjq7VsxozZ0GJ1w5+TUl2xGbph2qyQ07+G2gRC/c8eZPSy22yNmb2T3W4p650OZb0739omlZpdyroUd7L7PfUHqbDEVBh6r/wALEk7kpGYxhXtVy7kQRfyaxfyEDzxYgRvvvDm5AO8Gktl4n1uBX0YnnjXLxH5ESJz6ki7OEdmVDjlpdjSBmtI2hB8ktJ4VWuoN5srjC0anidTS5anbS+nXh9kppsM5XyUlOeQTaKbNl1M0JJTvXrMJN9TnZ6w1Okphzo9w9SJvuKY61KnaqxOLzibGWlRPtOi80GLXoKIxpKX6cPLLuQVF/KqC3nNhfzGhbzuQn7rQt4wEU+K3rQoestB0duMIrpKnu6iaBdM0fvOvBFFBYwimPrkD4izQ1dCC0WIqtUlElFuqYsV+f9GCP9whPA/jRD+0Qjhfx4h/OMRwv8yQvgnKDyAwj0b6G9WA33qaKDPWANRE6PjXA00BjfQl9nLhZqrkDXXudBcXxFnmrUVocxUGmDOIk+RvEL/HjHG1yPG+GbEGGtHjPHtiDHWjRjjuxFjwPSzsxWDLBzDAR7HswnzC8wmLCywN6FcwI55Qei/1zqb8F8IkUsLRigdasQi1ojwSkIeSwTLYDpkm0qwFyCPE4HjRWCZCJwgAieKwG1E4CQROJmDAQt0sznFYnM7B5s7MDapLdr/W+t8rv8jZnNnQQkQgTIjEL5WIysk7njD0C/fXpGnC7AZAmxXAbabANtdgM0UYHsIsFkmFuCYm7c9Ld78Dt7KGW809GUXby9i3qrc+SPaihltYIBWroGodG+0bgnvQHMFWK0AmyfA5mPMsAQpLxBgCwXYXgS7tsjcdY2netSe4WwLbb/5NNytr47ECQpfDVOovVfnate2Bo7RWdD4HaEGaVPMIK2W9CvLu1r9hn06lZmuhj343rSRsF/pjaT6tWH+lJuJppNqgD7DohzAkGZSjaQDxpMHLofGN/aUdKbHuXg6IBPR0sDJckiWLtyUViKWZM/JAfyA3RJIGe8zUOpNRlPQFwOrdfMFwdwK2Ng2Xh8Yv1P0+yfVDlypLK/hr0uYf77bz+MbsY2UcA7zKvgvrVehb34MXzhUXVnBBWgj8JDqCiReaXiMXGmI0ltjBSpVVTyNeRDGEjKQKpROVYWtpsgzD2VQOZ96DL6UqhqHiNJbBVXGCAHkRVyBA3VLAlGPWWWx1Z/3dfTnetafqYXqX7n68y9xf17i7mioP5ew/gwWmOVlZtRWs082C7D9BNj+AqwFY6yftgqwNgHWTrAnCxzdd0VkgDzGJtE7kYaAbagyNwV61GE9TjpDD9thT1OrxAr9EKBpZHaJQjp8g9FbjRCVJ8M7QNsm6AD/U3i5w1T4Vm+F77QUvtuh8CuYwlOr6De4FP46rPAHuTURKbyPKTzYNZcPKYB3kPBWmGnsoU7gMCcQdAKHOwGVAFMRoOxrvfmQe0jg4xJ99Ibncvxi2rGra7woMax6wm/0dooM610ZMrvQB+jpykqNfvJqmG3XoPEcbY7SJ3dz5oJtgSSZb9jWVCoT6lfUlGN3yrUlYXz1ClKie9chUpewewZ0btfiSQfPnaic7czqtDsxunElhznHnkvwXkt3+h26cwTTHWrn/kcu3Tkf607c0ZhIcUqZ4sArCjlRAEZmrV1e1vRHiuGkGE6J4TQsXB2wnBGBAyJwUAQOcZDthAPoJnGNReJRDhKPYSSOg9DjXc8xx2ISTxDWC1E5hlGZgQPeJ5LYpdbOgvxDh/8kh/9kaCLLr8inEKABb00YPUtwkmSEdzinkoSq1j8h+bQCeIdDD6OopEvGyQoP7IzjNzjmDp04eXSOyp6+2fP4zrBOUkra9pXZRp7jTZB8OinUG5th8xj36KzfmrN9cc9vG4vIWGB8QY8tf40pnIvFzQBjVqGbg2eYWuGhy2dZunyOQ5fPY7oM126lmGtAWI11+UK79iElHsuUOA5KfJERLRliSnqxw3+Jw/8TQ4m5X5EvdUT4qcN/mcN/ucN/hen34OMqi4+rHXxcy/iAd7PSgS4+VmI+brTni/gYx/gA29oyfE5oO6/dKPmWrKG3Zg2Fb+Vs7xWqyLdlFf5F1tDbs4bekTX0ThTq2vZzt8bdVmvc62iN+1lrwO0TqcnVGg24NR7MVibUNuNZ28DRWPkhIrRNV5qMHWoybL07kR/2wB/xwB/1wB/zwB/3wJ/wwJ/0wJ/CuPW6yU3yMxbJzzlIfoGRPBFCK10kz8EkvyIuB6K3jNELp37l1wpgI5ZFn8MOltB9KPqGQf7NCOGvjxD+2xHC3xgh/HcjhL85QvjvRwh/C4cH2BkeuulHw93N9LbVTO86mul91kxwlkTaydVMO+Jm+jB7uVBzTWDNBSe35Y+I2GTrVaL1Fkj+s2fIx54hf/EM+cQz5K+eIX/zDPm7Z8intpCA9f7MTfxnFvGfO4j/khE/iYa6iC/GxH/tVRZE+URGOZy1l9cSgUmCt7fyt14B67wCvvMKgPd+wgDJKyDfK6DAK6DQFpBtJJILTabhOzOYafhICzA9GUK/+MbJ9D+/QUyP9ygJInobRjScqpcnkPgT6TE9OMYf6U0b5Z4ohrcRw5PE8GQxvK0YniKGp4rh7Wg9CUxfv9AAD2J3sIid5iB2Z0bsthD6novYdzCxM4TlQLROYrTCJ0vk3XjxjEdhq9y7ewXM9ArYoxD6jjtAkWd5icz2CtjTK+B7XgF+k2d1BJ7LLZ4rHDxXMZ6nQOjzLp6fxTzXepQEMT2ZMQ33deT5JP626EkSjXMLvIMWegft5R20yDtob++gxd5B+3gH7cuD2ON4tlG63qK+0UH9Ekb9VAi9z0X9PZj6/TxLg8jflpEPd47kFtBN0Uk2udUzpM0zpN0zpMMz5ADPkE7PkC7PkG4e4jwN6OZ8hcX5KgfnBzHOt4PQa1yc/wxzfphXWRDlUxjlcIJVPrxQsHejisAeERgSgWERqInAXhHYJwL7C3PZTjrCYjHqYDHOWNweQs/9xrmddDZmMSkoASJwKiMQjgfL6ULhllpGDA+I4UExPCSGh8XwGjH8fTF8FIcRoaJtzmMsSo9zUHoCoxSMEEhHuyj9Pqb0JGEp0OVlfocyu/GI7enB0ryNsMlwSiG98Q7X3yxrC3ADjp4eP60QWVuAe3D09PjphXnWhbj5NsmFXPJMLLmIS56FJVtskm1c8hws2cElzy1klQT0cJtkD5c8H0uGueQFhcZGE91sStskB7jkhVhyiEviU/Jl/CB9dqsVp9nSP4OnfzFO/yxR+tJlNskruOSlWPIqoeTtNsk7ueTlWPJuoeQTNsmnuORVWPIZoeSbNsm3uOQ1uJXeFkr+zSb5KZe8Huf5mVASDvlYknDOh0rehCXhtI9bcopNcjsueSuW3EEouadN0s8lf4Ely4WSi22S+3LJO7FkvVCy0ybZzSXvwZIrhJK9Nsl+Lnk/ljxCKLnGJnkUl/xVoaHXVLePEUqeZZM8h0s+hPM8Tyh5lU3yai75KJa8Vih5t03yXi75BJa8Xyj5jE3yOS75NJZ8QSj5tk3yXS75HJZ8Xyj5mU3ycy75Ipb8UigJT62WJDy4UslXsCQ8vrold7BJTuOSv8GSOwsly22SFVzyDSxZJZSst0k2csnfY8klQskVNslVXPJtLHmQUPIIm2SUS76HJeNCyWNsksdxyQ+w5AlCSZgyN63Rk08dK4nPCtG1kXGulcQYjDiMnsCMnZvRk9PpIJ23EWuHr+jaoezMnLM8a6Oz/LeR5Tk5Z3nuRmf5Naz4vgFnLTjfgk6cn3P+F/D8J2xo/usg1+/AgdR9Ejj54BSAUwhOETgyOMXglIDjA6cUnDFFYKun7MJcy1x2MY+ZbalDY16ac8zLc455Vc7l5KsNOEJFf/DQsutzTuOmnGPemnMNfpFzzDtzzv2enGPen3PMXzljFjpi8L+yh3JO89GcYz6Rc8yneUybJTuHFI35XM5pvphzG72Sc5q/yTnNN3JO8/c5x3w759zfyznmB7nGlD7lE+jYInTh+DPhBPoVm0AboQrjiwTWe8pE4AQROFEEbiMCJ4nAyQQ8WXKglt0TG84tDhhnl0XmdvhhEWZzwrBhEkAnQ+Ao2R6eB8nkbUlp1hpHRvbgZ0XMExgJuP+VIpJKKrxgQYXLmA6JB0ZuQmlL0GEliJ8gbND1aCLCD7U0DPcY6fqV+ub2toOU5pjaR4/PJHXD9ACpGGzLG/UH2wUalW6MpOLasLIkCp5BMm0liYTe6zqtwphSutLD0Qw9XM0r0kdPVw5VVigRwL+XKpenoFbid+jxkslwtitiv/J8OxTB2qkBflJrddOKDA0Ngab98Wvn9uB7X6NdGEWgEmhj699MTxdD3Bki9dm1yLLZgm3byLuJYu8uAmeKwD1E4CwROFsE7pkTiX6LxHJKIlglMqz9VWASX3SR+DwmsSY7iV8zEpdB3FqIy49C89LOE4HzReACEbhQBO4lAheJwL0JWENtc3B9Je0IJ53MQ5NKUuuNatblTPpF8yG/vJgnB2fIPc097MtZlnz1lGWwqWOYU2zELP/SxfJ9mOVlgrIjlr9hLHdA3P3MuK1WPfcXgS0isFUEtonAdhHYIQIPIOA1ZJxV4+YwukyP9hHP/npYXQ3/GEZd2rQhGMCSrosjndAIhhlWamAnQ7TNsJ7C243m2YAbsZc0q18hCSSHSUsOaFElZghS20s9Ghnm4pplv8loWLnTbNfWLO3abbXrCke7rsLtep2rXa/B7XqIgC3UrmtZu94Nrz0PI3G3oXFtpuHkoAd+uAeueuA9HnjIAw974BrBgy16TPcrzEBJlwqW2pLmFAVNM6SF2alCmgRvBH49x/Kjrklj0laVe0kmk2kr0czZi2qSvbup+q2mOsLRVFHcVOe7mupc3FQJcXVRa33LWmsVRE+S6BOs6Ebh5JQQTQvRjBAdEKKDQnRIiA4L0TUcDSDUTeZRFpnHOMg8DpN5rIvMozGZJ4rKgKhcx6h8ECKfBHnW/n9lVx8bR3HFi53bilI1Vduk0CKyohEk5c539vkzSgLO2ZatJHbqC7aJ+KPr2z3fxeu70+2dHQeKIoRUCgTSKEJRRdukRAFF0KZNSQFFCjWEhgZFpIqaftEixB9V1S9ahECq2s6bj933dsfR5g/79r19v9nZ376ZN3M387azy4R8TYvGN6n4MBW/BbUCkUuPBCfHBu4xHqXiY1Tcz8QrLSSTVMeQOZwdSMK/kZGkSTJeqQ5GDXjukHIpa6fK5fDwy4SXxQRKIZHyfDdn40O+VU188t1rCxZfKFyvmnLFPjssstEZyiigFhAPpzyeDzTJlzYHV+QClCDUfIua6EJZl8wCXK7qVuvoYqXyTIl1nnb4pnmPDbtp5pwZi40EWbfqsXYM48TceKq9I8vuTebFGs4NmB29/NetpPG4fDAp4DrqWAcCxzrIHQsSlIlsw4cSKP3LbMSxytixDhNvwBlfpEedBavvKCcRi4GJ9BSRvkuk7/nS4Gje+D6RjhDpKJNKJGGcDGTDgg61DYg8uhDTkd0baGcH90q5oSxp/EBxC9eOcnss4PZ4iNtnE+i7xskIt3djbp/DXOBcNQlB7Stg9EO//TEPN35EpJNE+jGRfuJL/bmtxiki/ZRILzDpSKuOW9q4xvsnWcgYUp/jOEmcUKpha/+OMTYrqtfFACNpjsBIADa11Vh7mQeDeqFUbrDTfDvpSH9/cK2Fan22BAnnmPsPTaSgl0CXmTDzsjGqI3+IUmy6rlmsM0kmLhTtEppiAZpi0A9Audm+JGtz2dzkgPrkl8oNpWB7Gf/YnucHYMk/QGYdGExE6665yyl7nr/evyuTMeX2U9XrTJQLbBZKEumpeyROysZwbnWm6VAtj9NYY5z22zt7aFGffDHwyZdDPnkmgTLR5CI+2Y998hXsRDj5jPTJn4PREoyywAhl0zNe1Slf0ynPQbBiyqUjWPu6b5rLDebzY+Mjg3njFzrleZ3yDaY80epnM6S0V8UmyMGdMBWpOY7tluHdAR1snr0Y7JGU/axZEOkJA8VMvVxjQNbB19ReaIVRGRHD+5iLruWV5EdgXSzXnQX2x5xk2lVWviCiE09eKC9TgAtKtDlbbkDyXtexPFZAtW4ng8jArWvVZqEkj70Sf/XBnDVj7S1XHOFP8NIzqIZK31jk71pJmvNlZ6HIcyiyRlKvSuywqQLftEjhKLtKy97dhC1trhM6UZNQVWMPbczh1C3Uy15DCsYv5VNMoacYdes3A7e+GHLrtxIoI0464tYp7NaXNX6Ik+JI774Etr/mTWCqW4x4rhDpN0T6LZF+50sw+Pk9kf5ApLeZdKPwUEe4KO+CYHMva+h/lLbLBPZ3AkbeDTHyXgIlwPlyhJGbMCN/xnXHyW8kFa+C0V+UkVgMTqS/EulvRPq7L0Hs/geR/kmk95m0VdtqdzmVvXLTmGtVKpbad5UbGpEhPzc0KI9kY2FBw/iXYk8fuj8I2PswxN5HCZSd5pMR9hKYvf/g28W5aCR7r4HRf32KISD/j0hwlUC6jkgtvgThuZVIK4iUYNIawp4bSahrGIbyJ23ggC+jJSOfMigjnzZQspd/fxxm5P2PESOfxbeAE7tIRpbA6HPM6DNgNOH3OMbnNbovaHSrNLrVvm5kcHJoZHRgcNz4okZ3o0Z3E9OltZ63g3VTMC8WHSJ0jkowviQLSgUFRRm9OWD0lhCjpoESvvwpwujbmNG10RvG+V4kr+fA9DYDujdmigPq7TrlOp1yvQEBeaqbBuSv+qYozN6hUyZ1yhRTHifDSghtC6z7d/hru0x4z2gyLPqx2g9tkchKwmQkZvOQKiznymxKUIOHmZT2Xs2FIMo/+N5sWRxcn82YbRx2Gyw4pj2+pNuP33VnrtqA4Ftp1KsuDbsi4YK4C20ILlTnoQwmV8rFxeCSHIgIWawUZHhX8blQbbIaw28IrgXjlgXmDCAW6lZlFmol7j4YHUw3Gw3oOelogX8P1KzBDxVFnnWCKWplp8B6zTb5BK8ejDOBY3eEHLvTQBl5LkQc+w3s2L0aH8QpeaRnvw62G0S30iOXuxNpI5E2EWmzL0GUuZNIdxGpn0lfUTOendZscw6oEXElvz3nq4wtErZMZBkIyBkKkTNsoPQmP4uQ8wImZxu+DZzYRLLyHBiNCgZ7SCse0yl36JRfE+29h7b3cZ1pXqfcqVPezZRZxSMMKkQz8kpNaKHSuatNOCY6Y0KWlkKlRemdCujdFaL3XgMl03g6Qu9RTO/XNVXH2TQky78C22lmu0rNaWmO9cLyp+zlTznLnyoaat4FA8QZIpWIVCbSbuOq3wK5AWuVEGs1A6WReCLC2n7MWmPZiuOEEpK7p2Hh+rwhB9xB8PKMBZ1yj065qFPu1Snv0ynv1ym/oVM+IJUppIzSuC+g8cEQjQ8Z6Fva+xBpwn4R0/iwpgY4jYQkENYlG48oR8W1fVSnfEyn3K9TPq5TPqFTHtApv61THlSt96oEHgoIfDJE4GFM4EyEQAcT+JSmBmi1nloscfVF5fBrd7BUEX7wFm8CNtBSRfjZO7rSIkmQbQp5FCMzWuRdBLlFIY9h5IAWuZMgJxTyGYyc0iJLBLlbIU9gpKtF3k+QDyjk8xi5T4s8QJAHFfIkRh7SIo8R5HGFPIWRz2qRLxLkywp5GiPPaJFvEuRFhXwJI9/SIt8hyHcV8gxGvqdFfkCQHyrkWYz8SIsErw6QMFfjyCWMhBlbFHkzQd6ikOcw0tQiMwTZoZDnMbJTixwgyCGFvICRw1rkFEHuUsiLGHmvFukSZEUhL2FkTYvcR5APKuRljHxIizxEkE8q5BWMPKxDroQOJ95atKOxLY/FtnwmtuWJ2JbPx7Y8GdvyVGzL08ryar0/t3wpdplnYluejW25FNvyXOw7Oh+7zAuxLS/GtrwU2/JybMsryhKf0a1SvWHNihnbbmlZaa9uKa5cdV1yo1ezKmbBtTxv062e5Tr8Rfa3bs6zww3m2j54bXzfxjRYbbZbWleublmTKAns9Wvbk+K18nbLCllg6+3pSavOZtqNxe1V23Jh6NFmebU9d9bECz9H7E09md5sZ4L/v6HftiENY8GqN2jp1ye5QSo/NnpPKj+4LdXem+rIZFJj+TybUm7ZtpVPLEcHp3jpn7BLjUZtQzpt7W3vzvb2dLbNV9vmPKdQsdsqTiPd3pkt9BW72rsKxWKn3dttTXcXM33dfUXLKmR7+jrSsO7Q8dLyraSeuHZXZk9XJtXT2Zntbm/bXZu5DSpjssqYojImqwybFq9jtVnPp8frWH3Wm9dW87iFXsuDuqYa2P8H4c8nIFZ9BQA=" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="" />
</div>


<script type="text/javascript">
//<![CDATA[
var rootRel = '/';//]]>
</script>

<script src="/WebResource.axd?d=7_gNHu64AZzUXmccBsUVV8xEPunxrKBin_tdVFQPcENkPUDnnqLZEaoyXJnbnmcxreE_pkBxtrdjuqt33Xf7-zUKturmkeEnFOtPD4xHaEJoiu3k9Q9oGANeWxoxFH0bWBkvwTJh7bnh0yRscffI2e3nXLTvy5b0X7IPs9GlYIZ4hhflnZ1HDMNDZe9jfyPz7V60Ug2&amp;t=635989471085703795" type="text/javascript"></script>
<script src="/WebResource.axd?d=J-EELoUx3kpMgypNbusI6RQ_n0W9X2U3SgTKMaD1N5fSxMithCWCNvyeSlXxyJICdZABhFohKbV9NMFNZ4dsxTRClIUnG8Qwby9npGyoIMTJAk2dd1bc55yB31yZ0onfZmoQuDwHBO4YAc4rmUzo418VD1qcgRwfroO1E-Rc9yUJLn3Ijn94wQ5oQQHCzSUPWWRYuA2&amp;t=635989471085703795" type="text/javascript"></script>
<script src="/WebResource.axd?d=A9RIp8zGg4Gw3exV1N0P958fkZ6ohvRvEUkD3_gB8LX6Zzf_G1aQl9xnmRi3XfUHu-gvcho71LrRksQXwTl0u-6HYV7Wl_2m4s-isSjict3GYon2IjElokmjux_8634TvQq3hQwQAi8TRkjJGilD3w64aEZh2bjkLisT7jFAm-SIXCUMJ5Ebz0mmsvLIYKjVlQHXGA2&amp;t=635989471085703795" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
var rootRel = '/';//]]>
</script>

<script src="/ScriptResource.axd?d=jm-01pauvma80AkBIkGPGD24rMBGUxSY3rtoQ_08GUM7OAkDepTzH_5Ql_ZkNushkCLj2I6hGCvfgIEJaUfFAk7K0uzx6sNpbtRPLlFNZZLSrHDqwZWp9fv9cP_hieKO1p4IFGOaFiALRH6zR_7YDmpImXMJZ-EhT6Sg6xZEJ5hImu9l0HwOQhq7prk88Y_IrDmR6g2&amp;t=3f13b516" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=He9_wRJ32Dd606ebJutyoKPYzMCSBBr-HxcKCUZlq1phMG8pxidO73H0O_ueLHWdGjBcicZF0gjjCi6jq9JuZmH5samcHVT6I9kWM5VfUc2AhvlKXfSu47EbDWWe7IXvny_bNk1cWbWQrhNmmuxdfoDGSrTxJ983J7DcQX0Bac9Y4abZJaJslWJUqItAHb4czZCM3S4_DDJ1Rd5RqS-ag10x6J5_1kKPEHxp1SlLusXGACeRlIaMMNbXohFZxjI6AnLiOVoXirnCA2hZLhevYDTL6GSwV-UcOUoqmLOaHcMV_cCS-7wLnRiDOaLDbMMXGl5uSnWcXcg72UcDNQxk4CrJVKTyBFIrOC7D9B37AW82yZ0UMWG5UxKxPLB7u4byO90vaqdo7djcWqvDucnMyftaXFP6aJhOub7rmy6en41gYXRf4YxluiFGQWCTrltw9zzTcPty6rGNEGYPHM6Oul-NKOfMwMy6RUedWQD0OZQmxQ8Mqil0E6sOc9PSPpFrnvh4NiwDsHyqWnpK9N_0-L_rAqjuxq74c6gNTeHZuZc6OOQ3AENWTUJA9vgI6Whp-_hM1iOxGbQ9lm6Fwm7MBvmZZCXv6Z7Vnuv1vzThV_hOAeMl2aivARcqZUdjWsq2aS4ujBtOCetK7L1_Nnm9DoNgd-w1" type="text/javascript"></script>
<script src="WebServices/ShoppingCartService.asmx/js" type="text/javascript"></script>
<script src="WebServices/NewsLetterSignupService.asmx/js" type="text/javascript"></script>
<script src="WebServices/ReserveInStoreService.asmx/js" type="text/javascript"></script>
				
        
       	

		<div id="wrap" class="clearfix">	
			
<script>
    $( document ).ready(function() {
        //grid view
        $( "span.salepriceGrid:contains('$0.02')" ).css( "display", "none" );

        //list view
        $( "span.saleprice:contains('$0.02')" ).css( "display", "none" );

        //recently viewed
        $( "div.pricingContainer:contains('$0.02')" ).css( "display", "none" );

        //quick view
        $( "span.productSaleprice:contains('$0.02')" ).css( "display", "none" );
    });
</script>



<div id="headerContainer">
	<div id="header">
		<h1><a href="Default.aspx">Henrys - Photo - Video - Digital</a></h1>	
		<ul id="header-nav">
        	<li id="middle-storelocations"><a href="Store-Locations.aspx">Store Locations</a></li>
            <li id="middle-blog"><a href="http://blog.henrys.com">Henry's Blog</a></li>		
			<li id="header-nav-custcare"><a href="Customer-Care.aspx">Customer Care</a></li>
			<li id="header-nav-login"><a class="noauth" href="SignIn.aspx">login or register</a></li>
			<li id="header-nav-cart" class="last"><a class="lnkheadercart" href="ShoppingCart.aspx">Shopping Cart (0)</a></li>
		</ul>
		<div id="header-search"><input id="txtSearch" type="text" value="" /><a href="javascript:{}" id="btnSearch">Search</a></div>
	</div>
	<div id="header-menucontainer">		
		
				<ul id="header-menus">
					
				<li id="header_lstCategories_category_0" class="A">
					<a href="javascript:void(0);">Cameras &amp; Lenses</a>
					<div id="header_lstCategories_dropNav_0" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_0" class="drop-contentA">
								<h1>Cameras & Lenses</h1>
								<div class="col left"><h2>Cameras</h2><ul>    <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy.aspx">Point &amp; Shoot Cameras &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Small-And-amp-Easy.aspx/1/318%5eSmall+%2526+Easy">Small &amp; Easy</a></li>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Waterproof.aspx/1/318^Waterproof">Waterproof</a></li>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Super-Zoom.aspx/1/318^Super+Zoom">Super Zoom</a></li>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Advanced.aspx/1/318^Advanced">Advanced</a></li>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Wi-Fi.aspx/1/318^Wi-Fi">Wi-Fi</a></li>    </ul>    <p>					<a href="/Categories/67-Digital-Cameras-Compare-and-Buy.aspx"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Point &amp; Shoot Cameras</span></a>				</p>    </div>    </div>    </li>    <li><a href="/Categories/61-Digital-SLR-Cameras-Mirrorless-Camera.aspx/1/318^Mirrorless+Camera">Mirrorless Cameras &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/61-Digital-SLR-Cameras-Mirrorless-Camera-Stylish-And-amp-Sophisticated.aspx/1/318%5eMirrorless+Camera%2c%23319%5eStylish+%2526+Sophisticated">Stylish &amp; Sophisticated</a></li>        <li><a href="/Categories/61-Digital-SLR-Cameras-Mirrorless-Camera-Pro-Grade.aspx/1/318^Mirrorless+Camera%2c%23319^Pro+Grade">Pro Grade</a></li>    </ul>    <p>					<a href="/Categories/61-Digital-SLR-Cameras-Mirrorless-Camera.aspx/1/318^Mirrorless+Camera"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Mirrorless Cameras</span></a>				</p>    </div>    </div>    </li>    <li><a href="/Categories/61-Cameras-Compact-System-Camera-Digital-SLR.aspx/1/Categories%252f61-Digital-SLR-Cameras-Digital-SLR.aspx%252f1%252f318%5eDigital+SLR%2c%23318%5eDigital+SLR">Digital SLRs &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/61-Cameras-Compact-System-Camera-Digital-SLR-Hobbyist-And-amp-Beginner.aspx/1/Categories%252f61-Digital-SLR-Cameras-Digital-SLR.aspx%252f1%252f318%5eDigital+SLR%2c%23318%5eDigital+SLR%2c%23319%5eHobbyist+%2526+Beginner">Hobbyist &amp; Beginner</a></li>        <li><a href="/Categories/61-Cameras-Compact-System-Camera-Digital-SLR-Enthusiast.aspx/1/Categories%252f61-Digital-SLR-Cameras-Digital-SLR.aspx%252f1%252f318%5eDigital+SLR%2c%23318%5eDigital+SLR%2c%23319%5eEnthusiast">Enthusiast</a></li>        <li><a href="/Categories/61-Digital-SLR-Cameras-Digital-SLR-Hobbyist-And-amp-Beginner-Professional-Grade.aspx/1/318^Digital+SLR%2c%23319^Professional+Grade">Pro Grade</a></li>    </ul>    <p>					<a href="/Categories/61-Cameras-Compact-System-Camera-Digital-SLR.aspx/1/Categories%252f61-Digital-SLR-Cameras-Digital-SLR.aspx%252f1%252f318%5eDigital+SLR%2c%23318%5eDigital+SLR"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Digital SLRs</span></a>				</p>    </div>    </div>    </li>    <li><a href="/Categories/673-Action-Cameras.aspx">Action Cameras</a></li>    <li><a href="/Categories/171-Medium-Format.aspx">Medium Format &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/210-645-BODY.aspx"><span>Digital Medium Format</span><br />        Digital capture at its finest. </a></li>        <li><a href="/search/xmedfilm.aspx"><span>Film Medium Format</span><br />        Explore with a 645, 6x6 or 6x7 camera!</a></li>    </ul>    <p>					<a href="/Categories/171-Medium-Format.aspx"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Medium Format Cameras &amp; Accessories</span></a>				</p>    </div>    </div>    </li></ul><h2>Lenses</h2><ul>    <li><a href="/Categories/60-Cameras-Mirrorless-Lenses.aspx/1/3AA^Mirrorless+Lenses">For Mirrorless Cameras &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Wide-Angle-Zoom-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^656">Wide-Angle Zoom Lenses</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Mid-Range-Zoom-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^653">Mid-Range Zoom</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Telephoto-Zoom-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^655">Telephoto Zoom</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Prime-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^654">Prime Lens</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Macro-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^659">Macro Lens</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Conversion-And-Creative-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^657">Conversion &amp; Creative Lenses</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Mount-Adapters.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^615">Mount Adapters</a></li>    </ul>    <p>					<a href="/Categories/60-Cameras-Mirrorless-Lenses.aspx/1/3AA^Mirrorless+Lenses"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Mirrorless Lenses</span></a>				</p>    </div>    </div>    </li>    <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses.aspx/1/3AA^Digital+SLR+Lenses">For Digital SLRs &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Wide-Angle-Zoom-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^656">Wide Angle Zoom</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Mid-Range-Zoom-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^653">Mid-Range Zoom</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Telephoto-Zoom-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^655">Telephoto Zoom</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Prime-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^654">Prime Lens</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Macro-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^659">Macro Lens</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Special-Application-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^658">Special Application Lenses</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Conversion-And-Creative-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^657">Conversion &amp; Creative Lenses</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Tele-Converters.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^540">Tele Converter</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Mount-Adapters.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^615">Mount Adapters</a></li>    </ul>    <p>					<a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses.aspx/1/3AA^Digital+SLR+Lenses"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Digital SLR Lenses</span></a>				</p>    </div>    </div>    </li>    <li><a href="/search/xmedforlens.aspx">For Medium Format</a></li></ul><br /><a onclick="_gaq.push(['_trackEvent','Nav: Camera &amp; Lenses - Bottom','Click','Canon Best In Glass Event 160502']);" href="/canon-best-in-glass-sales-event.aspx"><img height="100" width="435" alt="Canon Best In Glass Event On Now" src="//www.henrys.com/Images/Nav-PCAs/Canon-Best-in-Glass-Nav-Btm-151030.jpg" style="margin-top: 45px;" /></a></div><div class="col right"><h2>Shop by Lens Type</h2><ul>    <li><a href="/Categories/656-Wide-Angle-Zoom-Lenses.aspx">Wide Angle Zoom</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E653">Mid-Range Zoom</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E655">Telephoto Zoom</a></li>    <li><a href="/Categories/654-Prime-Lenses.aspx">Prime Lens</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E659">Macro Lens</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E658">Special Application Lenses</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E657">Conversion &amp; Creative Lenses</a></li>    <li><a href="/Categories/540-Tele-Converters.aspx">Tele Converter</a></li>    <li><a href="/Categories/615-Mount-Adapters.aspx">Mount Adapters</a></li></ul><ul>    <li>&nbsp;</li>    <li><a href="/New-Photo-Video-Gear.aspx" style="text-decoration: underline; color: #336699;">View our Selection of Pre-order Cameras &amp; Lenses</a></li></ul></div>
							</div>
							<div id="header_lstCategories_dropProds_0" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="89520-SIGMA-150-600MM-F5-6-3-C-DG-OS-CANON.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products89520-75x75-1234542.jpg' alt="SIGMA 150-600MM F5-6.3 C DG OS CANON " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="89520-SIGMA-150-600MM-F5-6-3-C-DG-OS-CANON.aspx" title="">
                                                        SIGMA 150-600MM F5-6.3 C DG OS...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $1,399.99</span><br />
                                                        <span class="yousave" >Save: $170.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="85177-LENSBABY-5-8MM-CIRCULAR-FISHEYE-NIKON.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products85177-75x75-134683687.jpg' alt="LENSBABY 5.8MM CIRCULAR FISHEYE NIKON " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="85177-LENSBABY-5-8MM-CIRCULAR-FISHEYE-NIKON.aspx" title="">
                                                        LENSBABY 5.8MM CIRCULAR FISHEY...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $319.99</span><br />
                                                        <span class="yousave" >Save: $120.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="87415-FUJIFILM-X100T-SILVER-16MP-23MMF2-APS-C.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products87415-75x75--354906352.jpg' alt="FUJIFILM X100T SILVER 16MP 23MMF2 APS-C " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="87415-FUJIFILM-X100T-SILVER-16MP-23MMF2-APS-C.aspx" title="">
                                                        FUJIFILM X100T SILVER 16MP 23M...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $1,499.99</span><br />
                                                        <span class="yousave" >Save: $100.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="57903-HASSELBLAD-EXT-TUBE-16E--3040654.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products57903-75x75-797274.jpg' alt="HASSELBLAD EXT TUBE 16E       3040654 " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="57903-HASSELBLAD-EXT-TUBE-16E--3040654.aspx" title="">
                                                        HASSELBLAD EXT TUBE 16E       ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $374.50</span><br />
                                                        <span class="yousave" >Save: $74.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_0" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Camera and Lenses - Right','Click','Nikon Spring Sales Event 160429-160602']);" href="/nikon-sales-event.aspx"><img height="380" width="185" alt="Which Nikon Are You? Spring Sales Event" src="//www.henrys.com/Images/Nav-PCAs/Nikon-Spring-Sales-Event-160418.png" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_1" class="B">
					<a href="javascript:void(0);">Drones</a>
					<div id="header_lstCategories_dropNav_1" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_1" class="drop-contentB">
								<h1>Drones</h1>
								<div class="col left"><h2>Drones / UAVs</h2><ul>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-3DR.aspx/1/80000000^754%2c%2380000010^3DR">3DR Drones</a></li>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-DJI.aspx/1/80000000^754%2c%2380000010^DJI">DJI Drones</a></li>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-OMINUS.aspx/1/80000000%5E754%2c%2380000010%5EOMINUS">Ominus Drones</a></li>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-PARROT.aspx/1/80000000^754%2c%2380000010^PARROT">Parrot Drones</a></li>    <li><a href="/Categories/754-Aerial-Imaging-Platforms-and-Drones-VISTA.aspx/1/80000010^VISTA">Vista Drones</a></li></ul><p><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-Aerial-Imaging-Platforms-and-Drones.aspx/1/80000000^754" style="text-decoration: underline; color: #336699 ! important;">View all Drones / UAVs</a></p></div><div style="float: none; display: inline-block; padding-left: 20px;" class="col right"><h2>Learning</h2><ul>    <li><a target="_blank" href="/Droneography.aspx">Getting Started</a></li>    <li><a target="_blank" href="http://www.schoolofimaging.ca/search/Flying%20Cameras.aspx">In-Person Classes</a></li>    <li><a target="_blank" href="https://www.facebook.com/Henrys.Drones">Facebook: Henry's Drone/UAV Community</a></li></ul></div><div class="col right"><h2>Drone Accessories</h2><ul>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-Drone-Accessories.aspx/1/80000000^752">Accessories</a></li>    <li><a href="/Categories/81-Filters-Filters-for-Drones.aspx/1/80000000%5E757">Filters for Drones</a></li>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-Drone-Cases-and-Bags.aspx/1/80000000^755">Cases &amp; Bags for Drones</a></li></ul><p><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-Drone-Accessories.aspx/1/80000000^752" style="text-decoration: underline; color: #336699 ! important;">View all Drones Accessories</a></p></div><div style="clear: both;"></div><br /><a href="/93114-DJI-PHANTOM-3-4K-DRONE.aspx" onclick="_gaq.push(['_trackEvent','Nav: Drones - Bottom','Click','DJI Phantom 3 4K Drone']);"><img width="435" height="100" style="margin-top: 100px; text-align: left;" src="//www.henrys.com/Images/Nav-PCAs/DJI-Phantom-3-4K-Drone-160401.gif" alt="DJI Phantom 3 4K Drone" /></a>
							</div>
							
							<div id="header_lstCategories_dropPCARight_1" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Drones - Right','Click','3DR Solo Drone']);" href="http://www.henrys.com/92503-3DR-SOLO-AERIAL-DRONE-W-3-AXIS-GIMBAL.aspx"><img height="380" width="185" alt="3DR Solo Drone" src="//www.henrys.com/Images/Nav-PCAs/160108-right-3DR_Solo_DroneV2.jpg" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_2" class="A">
					<a href="javascript:void(0);">Video</a>
					<div id="header_lstCategories_dropNav_2" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_2" class="drop-contentA">
								<h1>Video</h1>
								<div class="col left"><h2>Video Cameras</h2><ul>    <li><a href="/Categories/673-Action-Cameras.aspx">GoPro and Action Cameras</a></li>    <li><a href="/Categories/65-Digital-Camcorders.aspx">Camcorders &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>					<!--<li><a href="/Categories/65-Digital-Camcorders-Camcorders.aspx/1/80000000^66%2c%232F2^Pocketable">Pocketable</a></li>            <li><a href="/Categories/65-Digital-Camcorders-Camcorders.aspx/1/80000000^66%2c%232F2^Everyday+Use">Everyday Use</a></li>            <li><a href="/Categories/65-Digital-Camcorders.aspx/1/2F2%5eEnthusiast+%2526+Professional">Enthusiast and Advanced</a></li>-->        <li><a href="/Categories/112-Pro-Video-Cameras.aspx">Professional</a></li>    </ul>    <p>					<a href="/Categories/65-Digital-Camcorders.aspx"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Camcorders</span></a>				</p>    </div>    </div>    </li>    <li><a href="/Categories/112-Pro-Video-Cameras.aspx">Professional Video</a></li>    <li><a href="/Categories/61-Digital-SLR-Cameras.aspx">Digital SLRs &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <p>					<a href="/Categories/61-Digital-SLR-Cameras.aspx">Video isn't just for camcorders anymore.<br />    <br />    <span class="nav-ViewAll" style="color: #336699;">Check out our selection of Digital SLRs!</span></a>				</p>    </div>    </div>    </li>    <li><a href="/Categories/750-Drones.aspx">Drones &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/750-Drones.aspx">Drones</a></li>        <li><a href="/Categories/752-Drone-Accessories.aspx">Drone Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/747-Dash-Cams.aspx">Dash Cams</a></li></ul></div><div class="col right"><h2>Accessories</h2><ul>    <li><a>Bags &amp; Cases &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/661-Camcorder-Bags.aspx">Camcorder Bags</a></li>        <li><a href="/Categories/664-Shoulder-Bags.aspx">Shoulder Bags</a></li>        <li><a href="/Categories/660-Backpacks-And-Slings.aspx">Backpacks &amp; Slings</a></li>        <li><a href="/Categories/479-Roller-Cases.aspx">Roller Cases</a></li>        <li><a href="/Categories/695-Hard-Waterproof-Cases-And-Accessories.aspx">Hard/Waterproof Cases &amp; Accessories</a></li>        <li><a href="/Categories/696-Camera-Housings-Covers-And-More.aspx">Camera, Housings, Covers &amp; More</a></li>    </ul>    </div>    </div>    </li>    <li><a>Video Lighting &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/256-Video-Lights.aspx">Video Lighting</a></li>        <li><a href="/Categories/309-Video-Lights-Acc-.aspx">Video Lighting Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a>Supports &amp; Tripods &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/497-Video-Tripod-Combo-w-Head.aspx">Complete Video Tripods</a></li>        <li><a href="/Categories/498-Video-Tripod-Heads.aspx">Video Tripod Heads</a></li>        <li><a href="/search/xportablesupport.aspx">Stabilizers, Shoulder Brackets, Rigs &amp; More</a></li>        <li><a href="/search/xfollowfocus.aspx">Follow Focuses<br />        Systems, Gears, Rings and More.</a></li>        <li><a href="/search/xjibrailslide.aspx">Jibs, Rails, Sliders and Dollies</a></li>    </ul>    </div>    </div>    </li>    <li><a>Audio &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/119-Microphones.aspx">Microphones and Mounts</a></li>        <li><a href="/Categories/669-Headphones.aspx">Headphones</a></li>        <li><a href="/search/xwindscreens.aspx">Windscreens</a></li>        <li><a href="/search/xboompoles.aspx">Boom Poles</a></li>        <li><a href="/search/xsoundblimps.aspx">Sound Blimps</a></li>        <li><a href="/Categories/672-Mixers.aspx">Mixers</a></li>        <li><a href="/Categories/128-Audio-Mobile-Office.aspx">Recorders </a></li>        <li><a href="/search/xxlrsaccess.aspx">XLR Adapters, Audio Cables and More </a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xpovadons.aspx">Action Camera Add-Ons &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xpovbatorcharg.aspx">Batteries and Chargers</a></li>        <li><a href="/Categories/756-Filters-for-Action-Cameras.aspx">Filters</a></li>        <li><a href="/search/xhousingandcase.aspx">Housings and Cases</a></li>        <li><a href="/search/xpovstrapormount.aspx">Straps and Mounts</a></li>        <li><a href="/search/xpovaccessory.aspx">Other Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a>Lenses &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/240-Video-Lenses.aspx">Conversion Lenses</a></li>        <li><a href="/search/Pro%20Video%20Lenses.aspx">Pro Video Lenses</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/671-Monitors.aspx">Monitors &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/671-Monitors.aspx">Monitors</a></li>        <li><a href="/search/xshadenmount.aspx">Shades and Mounts</a></li>    </ul>    </div>    </div>    </li>    <li><a>Batteries &amp; Chargers &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/383-Video-Batteries.aspx">Camcorder Batteries</a></li>        <li><a href="/Categories/375-Video-Battery-Acc-.aspx/1">Camcorder Chargers and Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/246-Video-Cables-And-Acc-.aspx">Video Cables</a></li>    <li><a href="/Categories/123-Video-Editing.aspx">Editing Software</a></li></ul></div><br /><br /><a href="/87701-GOPRO-HIGH-DEFINITION-HERO4-12MP-SILVER.aspx" onclick="_gaq.push(['_trackEvent','Nav: Video - Bottom','Click','GoPro HERO4 Silver Edition']);"><img width="435" height="100" src="//www.henrys.com/Images/Nav-PCAs/GoPro-Hero4-Silver-Ed-160401.gif" alt="GoPro HERO4 Silver Edition" style="margin-top: 45px;" /></a>
							</div>
							<div id="header_lstCategories_dropProds_2" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="93484-CANON-VIXIA-HF-R70-CAMCORDER-BUNDLE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products93484-75x75--259936441.jpg' alt="CANON VIXIA HF R70 CAMCORDER BUNDLE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="93484-CANON-VIXIA-HF-R70-CAMCORDER-BUNDLE.aspx" title="">
                                                        CANON VIXIA HF R70 CAMCORDER B...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $479.99</span><br />
                                                        <span class="yousave" >Save: $50.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="87350-COMODO-M7-PRO-7-HIGH-DEFINITION-FIELD-MONITOR.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products87350-75x75--258576949.jpg' alt="COMODO M7 PRO 7&quot; HIGH DEFINITION FIELD MONITOR " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="87350-COMODO-M7-PRO-7-HIGH-DEFINITION-FIELD-MONITOR.aspx" title="">
                                                        COMODO M7 PRO 7&quot; HIGH DEF...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $599.50</span><br />
                                                        <span class="yousave" >Save: $161.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="82424-ZOOM-IQ5-BLACK-PRO-STEREO-MICROPHONE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products82424-75x75-962182.jpg' alt="ZOOM IQ5 BLACK PRO STEREO MICROPHONE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="82424-ZOOM-IQ5-BLACK-PRO-STEREO-MICROPHONE.aspx" title="">
                                                        ZOOM IQ5 BLACK PRO STEREO MICR...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $89.50</span><br />
                                                        <span class="yousave" >Save: $30.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="87924-MANFROTTO-MHX-PRO-2WAY-FLUID-HEAD.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products87924-75x75-1091567.jpg' alt="MANFROTTO MHX-PRO 2WAY FLUID HEAD " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="87924-MANFROTTO-MHX-PRO-2WAY-FLUID-HEAD.aspx" title="">
                                                        MANFROTTO MHX-PRO 2WAY FLUID H...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $189.99</span><br />
                                                        <span class="yousave" >Save: $40.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_2" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Video - Right','Click','Sony Actioncam FDR X1000VR']);" href="/88961-SONY-FDR-X1000VR-4K-ACTIONCAM-W-REMOTE.aspx"><img height="380" width="185" alt="Sony Actioncam FDR X1000VR" src="//www.henrys.com/Images/Nav-PCAs/160108-right-Sony-Actioncam-FDR-X1000VR.jpg" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_3" class="A">
					<a href="javascript:void(0);">Accessories</a>
					<div id="header_lstCategories_dropNav_3" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_3" class="drop-contentA">
								<h1>Accessories</h1>
								<div class="col left"><h2>Essential Accessories</h2><ul>    <li><a href="/Categories/83-Bags.aspx">Bags, Cases &amp; More &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/663-Pouches-for-Point-And-Shoots.aspx">Pouches for Point-and-Shoots</a></li>        <li><a href="/Categories/664-Shoulder-Bags.aspx">Shoulder Bags</a></li>        <li><a href="/Categories/660-Backpacks-And-Slings.aspx">Backpacks &amp; Slings</a></li>        <li><a href="/Categories/661-Camcorder-Bags.aspx">Camcorder Cases</a></li>        <li><a href="/search/xcarryingccase.aspx">Carrying Cases</a></li>        <li><a href="/Categories/83-Bags-Camera-Housings-Covers-And-More.aspx/1/80000000^696">Camera Housings, Covers &amp; More</a></li>        <li><a href="/Categories/83-Bags-Hard-Waterproof-Cases-And-Accessories.aspx/1/80000000^695">Hard/Waterproof Cases &amp; Accessories</a></li>        <li><a href="/Categories/83-Bags-Computer-Bags.aspx/1/80000000^454">Computer Bags</a></li>        <li><a href="/Categories/83-Bags-Lens-Cases-Pouches.aspx/1/80000000%5E224 ">Lens Cases/Pouches</a></li>        <li><a href="/Categories/83-Bags-Modular-Cases.aspx/1/80000000^453">Modular Cases</a></li>        <li><a href="/Categories/83-Bags-Roller-Cases.aspx/1/80000000^479">Roller Cases</a></li>        <li><a href="/Categories/83-Bags-Specialty-Bags.aspx/1/80000000^448">Specialty Bags</a></li>    </ul>    <p>    <a href="/Categories/83-Bags.aspx"><span style="text-decoration: underline; color: #336699;" class="nav-ViewAll">View All Bags &amp; Cases</span></a>    </p>    </div>    </div>    </li>    <li><a href="/Categories/113-Memory.aspx">Memory Cards &amp; Media &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/272-Memory-Cards.aspx">Memory Cards</a></li>        <li><a href="/Categories/296-Memory-Card-Readers.aspx">Memory Card Readers</a></li>        <li><a href="/Categories/303-Memory-Acc-.aspx">Memory Card Storage &amp; More</a></li>        <li><a href="/Categories/293-Portable-Storage.aspx">External Harddrives</a></li>        <li><a href="/Categories/300-USB-Drives.aspx">USB Drives</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/52-Tripods.aspx">Tripods, Monopods, Supports &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/442-Table-Mini-Pods.aspx">Tabletop &amp; Mini Tripods</a></li>        <li><a href="/Categories/374-PHOTO-TRIPOD-HEADS.aspx">Tripod Heads</a></li>        <li><a href="/Categories/443-Tripod-Legs-Only.aspx">Tripod Legs</a></li>        <li><a href="/Categories/378-PHOTO-TRIPOD-COMBO-W-HEAD.aspx?video=grid">Complete Tripods</a></li>        <li><a href="/Categories/498-Video-Tripod-Heads.aspx">Video Heads</a></li>        <li><a href="/Categories/497-Video-Tripod-Combo-w-Head.aspx">Complete Video Tripods</a></li>        <li><a href="/search/xportablesupport.aspx">Stabilizers, Shoulder Brackets, Rigs &amp; More</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/222-Camera-Cleaning.aspx">Cleaning Supplies</a></li>    <li><a href="/Categories/81-Filters.aspx">Filters &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/81-Filters.aspx">Filters</a></li>        <li><a href="/Categories/235-Stepping-Rings.aspx">Stepping Rings</a></li>        <li><a href="/Categories/254-Filter-acc-And-adapters.aspx">Filter Accessories &amp; Adapters</a></li>    </ul>    </div>    </div>    </li></ul><h2>Camera Accessories</h2><ul>    <li><a href="/Categories/227-Batteries.aspx">Batteries &amp; Power &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/227-Batteries.aspx">Batteries</a></li>        <li><a href="/Categories/245-Rechargeable-Batteries.aspx">Rechargeable Batteries</a></li>        <li><a href="/Categories/231-Digital-Camera-Batteries.aspx">Camera Batteries </a></li>        <li><a href="/Categories/383-Video-Batteries.aspx">Camcorder Batteries</a></li>    </ul>    </div>    </div>    </li>    <li><a>Flashes, Modifiers &amp; More &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/376-Flash.aspx">External Flash Units</a></li>        <li><a href="/search/xexternallitemod.aspx">Light Modifiers &amp; Diffusers</a></li>        <li><a href="/search/xexternalbattery.aspx">Battery Packs &amp; More</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xvieworipiece.aspx">Viewfinders &amp; Eyepieces</a></li>    <li><a href="/search/xscreenorloop.aspx">Screen Protectors &amp; Loops</a></li>    <li><a href="/Categories/169-Camera-Straps.aspx">Camera Straps</a></li>    <li><a href="/search/xphotoglove.aspx">Photo Gloves</a></li>    <li><a href="/search/xtriggerorrelease.aspx">Triggers &amp; Releases</a></li>    <li><a href="/search/xsynccablemore.aspx">Sync Cords, Adapters &amp; More </a></li>    <li><a href="/Categories/184-Books.aspx">Books &amp; DVDs</a></li>    <li><a href="/Categories/69-Camera-Accessories-Travel-Accessories.aspx/1/80000000%5e739">Travel Accessories</a></li></ul></div><div class="col right"><h2>Lens Accessories</h2><ul>    <li><a href="/Categories/221-Filters.aspx">Filters &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/221-Filters.aspx">Filters</a></li>        <li><a href="/Categories/235-Stepping-Rings.aspx">Stepping Rings</a></li>        <li><a href="/Categories/254-Filter-acc-And-adapters.aspx">Filter Accessories &amp; Adapters</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/224-Lens-Cases-pouches.aspx">Pouches</a></li>    <li><a href="/Categories/234-Lens-Caps.aspx">Lens Caps</a></li>    <li><a href="/Categories/168-Lens-Hoods.aspx">Lens Hoods</a></li></ul><h2>Mobile Accessories</h2><ul>    <li><a href="/Categories/729-Mobile-Mobile-Audio-Accessories.aspx/1/80000000%5e730">Audio Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e730">HTC</a></li>        <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e730">HTC One</a></li>-->        <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e730">iPhone</a></li>        <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e730">LG</a></li>-->        <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e730">Samsung</a></li>        <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e730">Samsung Galaxy</a></li>-->    </ul>        <p>    <a href="/Categories/729-Mobile-Mobile-Audio-Accessories.aspx/1/80000000%5e730" style="color: #336699 ! important; text-decoration: underline;">View All Audio Accessories</a>    </p>        </div>        </div>        </li>        <li><a href="/Categories/731-Mobile-Cables-And-Chargers.aspx">Cables &amp; Chargers &rsaquo;</a>        <div class="nav-marketing-flyout">        <div class="flyout-container">        <h3>Narrow Your Search:</h3>        <ul>        <!--<li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e731">HTC</a></li>-->        <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e731">HTC One</a></li>-->            <li><a href="/search/xkiphone.aspx/1/80000000%5e729%2c%2380000000%5e731">iPhone</a></li>            <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e731">LG</a></li>-->            <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e731">Samsung</a></li>            <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e731">Samsung Galaxy</a></li>-->    </ul>            <p>    <a href="/Categories/731-Mobile-Cables-And-Chargers.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Cables &amp; Chargers</a>    </p>            </div>            </div>            </li>            <li><a href="/Categories/732-Mobile-Cases.aspx">Cases &amp; Screen Protectors &rsaquo;</a>            <div class="nav-marketing-flyout">            <div class="flyout-container">            <h3>Narrow Your Search:</h3>            <ul>                <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e732">HTC</a></li>                <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e732">HTC One</a></li>-->                <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e732">iPhone</a></li>                <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e732">LG</a></li>-->                <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e732">Samsung</a></li>                <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e732">Samsung Galaxy</a></li>-->    </ul>                <p>    <a href="/Categories/732-Mobile-Cases.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Cases &amp; Screen Protectors</a>    </p>                </div>                </div>                </li>                <li><a href="/Categories/734-Mobile-Lenses-And-Filters.aspx">Lenses &amp; Filters</a></li>                <li><a href="/Categories/736-Mobile-Lighting-Accessories.aspx">Lighting Accessories &rsaquo;</a>                <div class="nav-marketing-flyout">                <div class="flyout-container">                <h3>Narrow Your Search:</h3>                <ul>                    <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e736">HTC</a></li>                    <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e736">HTC One</a></li>-->                    <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e736">iPhone</a></li>                    <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e736">LG</a></li>-->                    <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e736">Samsung</a></li>                    <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e736">Samsung Galaxy</a></li>-->    </ul>                    <p>    <a href="/Categories/736-Mobile-Lighting-Accessories.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Lighting Accessories</a>    </p>                    </div>                    </div>                    </li>                    <li><a href="/Categories/733-Mobile-Remotes-And-Triggers.aspx">Remotes &amp; Triggers &rsaquo;</a>                    <div class="nav-marketing-flyout">                    <div class="flyout-container">                    <h3>Narrow Your Search:</h3>                    <ul>                        <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e733">HTC</a></li>                        <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e733">HTC One</a></li>-->                        <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e733">iPhone</a></li>                        <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e733">LG</a></li>-->                        <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e733">Samsung</a></li>                        <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e733">Samsung Galaxy</a></li>-->    </ul>                        <p>    <a href="/Categories/733-Mobile-Remotes-And-Triggers.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Remotes &amp; Triggers</a>    </p>                        </div>                        </div>                        </li>                        <li><a href="/Categories/735-Stabilizers-And-Mounts.aspx">Stabilizers &amp; Mounts &rsaquo;</a>                        <div class="nav-marketing-flyout">                        <div class="flyout-container">                        <h3>Narrow Your Search:</h3>                        <ul>                            <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e735">HTC</a></li>                            <!-- <li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e735">HTC One</a></li>-->                            <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e735">iPhone</a></li>                            <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e735">LG</a></li>-->                            <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e735">Samsung</a></li>                            <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e735">Samsung Galaxy</a></li>-->    </ul>                            <p>    <a href="/Categories/735-Stabilizers-And-Mounts.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Stabilizers &amp; Mounts</a>    </p>                            </div>                            </div>                            </li>                            <li><a href="/search/xtablet.aspx/1/80000000%5e51%2c%2380000000%5e83">Tablet Bags &amp; Cases</a></li>                            <li><a href="/search/XKIPAD.aspx">iPad Accessories</a></li>                            <li><a href="/Categories/737-Miscellaneous-Mobile-Accessories.aspx">More Mobile Accessories</a></li>                        </ul>                        <h2>Shop For Gifts</h2>                        <ul>                            <li><a href="/Gift-Cards.aspx">Gift Cards</a></li>                        </ul>                        </div>
							</div>
							<div id="header_lstCategories_dropProds_3" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="60121-LEXAR-32GB-ECHO-SE-BACKUP-DRIVE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products60121-75x75-747891.jpg' alt="LEXAR 32GB ECHO SE BACKUP DRIVE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="60121-LEXAR-32GB-ECHO-SE-BACKUP-DRIVE.aspx" title="">
                                                        LEXAR 32GB ECHO SE BACKUP DRIV...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $59.50</span><br />
                                                        <span class="yousave" >Save: $40.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="63240-AQUATECH-SOFT-PAD-TRIPOD-WRAP-2-SET-ASPT.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products63240-75x75-490401.jpg' alt="AQUATECH SOFT PAD TRIPOD WRAP 2 SET ASPT " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="63240-AQUATECH-SOFT-PAD-TRIPOD-WRAP-2-SET-ASPT.aspx" title="">
                                                        AQUATECH SOFT PAD TRIPOD WRAP ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $29.50</span><br />
                                                        <span class="yousave" >Save: $10.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="77025-PORTABRACE-LP-LED2-LITE-PANEL-CASE-BLUE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products77025-75x75-1027009.jpg' alt="PORTABRACE LP-LED2 LITE PANEL CASE BLUE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="77025-PORTABRACE-LP-LED2-LITE-PANEL-CASE-BLUE.aspx" title="">
                                                        PORTABRACE LP-LED2 LITE PANEL ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $219.50</span><br />
                                                        <span class="yousave" >Save: $80.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="73819-BELLEKS-CAP-BUCKLE-43-52-55MMCAP-HOLDE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products73819-75x75-820300.jpg' alt="BELLEKS CAP BUCKLE (43,52,55MM)CAP HOLDE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="73819-BELLEKS-CAP-BUCKLE-43-52-55MMCAP-HOLDE.aspx" title="">
                                                        BELLEKS CAP BUCKLE (43,52,55MM...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $8.50</span><br />
                                                        <span class="yousave" >Save: $1.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_3" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a href="/78473-MANFROTTO-BEFREE-TRAVEL-TRIPOD-W-BH-Plus-BAG.aspx" onclick="_gaq.push(['_trackEvent','Nav: Accessories - Right','Click','Manfrotto Befree Travel Tripod with Ball Head and Carrying Case']);"> <img height="380" width="185" src="//www.henrys.com/Images/Nav-PCAs/160108-right-Manfrotto_190xPOr3.gif" alt="Manfrotto Befree Travel Tripod with Ball Head and Carrying Case" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_4" class="A">
					<a href="javascript:void(0);">Lighting &amp; Studio</a>
					<div id="header_lstCategories_dropNav_4" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_4" class="drop-contentA">
								<h1>Lighting & Studio</h1>
								<div class="col left"><h2>Lights</h2><ul>    <li><a href="/Categories/211-Studio-Lights.aspx">Studio Strobes</a></li>    <li><a href="/Categories/748-LED-Lighting.aspx">LED Lighting</a></li>    <li><a href="/Categories/749-Continuous-Lighting.aspx">Continuous Lighting</a></li></ul><h2>Lighting Accessories</h2><ul>    <li><a>Bulbs &amp; Lamps &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul style="padding-top: 15px;">        <li><a href="/Categories/199-FLASHTUBES.aspx">Flash Tubes</a></li>        <li><a href="/Categories/239-Modeling-Lamps.aspx">Modeling Lamps</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/212-Light-Meters.aspx">Light Meters</a></li>    <li><a href="/search/xlightmodifier.aspx">Light Modifiers &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xsoftbox.aspx"><span>Softboxes</span><br />        Soften and defuse the light!</a></li>        <li><a href="/search/xumbrella.aspx"><span>Umbrellas</span><br />        Cast a broad light!</a></li>        <li><a href="/search/xbeautydish.aspx"><span>Beauty Dishes</span><br />        Achieve beautiful portraits!</a></li>        <li><a href="/search/xeggcrate.aspx"><span>Egg Crate &amp; Honeycomb Grids</span><br />        Take control and avoid the light spill!</a></li>        <li><a href="/search/xsnoot.aspx"><span>Snoots</span><br />        Control the radius of your light!</a></li>    </ul>    <p><a href="/search/xlightmodifier.aspx"><span style="text-decoration: underline; color: #336699;" class="nav-ViewAll">View All Light Modifiers</span></a></p>    </div>    </div>    </li>    <li><a href="/search/xaccesslightmod.aspx">Light Modifier Accessories</a></li>    <li><a href="/search/xexternalpack.aspx">Battery Packs</a></li>    <li><a href="/search/xaddstudioaccess.aspx">Additional Accessories</a></li></ul></div><div class="col right"><h2>Studio Accessories</h2><ul>    <li><a href="/Categories/216-Backgrounds.aspx">Backgrounds &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/500-Fabric-Backgrounds.aspx">Fabric Backgrounds </a></li>        <li><a href="/Categories/501-Paper-Backgrounds.aspx">Paper Backgrounds </a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xrailorboom.aspx">Supports, Light Stands, &amp; Booms &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/662-Background-Support.aspx">Background Supports</a></li>        <li><a href="/Categories/133-Light-Stands.aspx">Light Stands</a></li>        <li><a href="/search/xboom.aspx">Booms</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xtriggerorrelease.aspx">Triggers &amp; Releases</a></li>    <li><a href="/search/xsynccablemore.aspx">Sync Cords, Adapters &amp; More </a></li>    <li><a href="/search/xproducttool.aspx">Product Photography Tools &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xlightingtent.aspx"><span>Lighting Tents</span><br />        A simple solution for small product photography! </a></li>        <li><a href="/search/shootingtable.aspx"><span>Shooting Tables</span><br />        Capture a soft and seamless background!</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xadditionalstudio.aspx">More Studio Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xapplebox.aspx">Apple Boxes</a></li>        <li><a href="/search/xstool.aspx">Stools</a></li>        <li><a href="/search/xsandbag.aspx">Sand Bags</a></li>        <li><a href="/search/xgaffertape.aspx">Gaffer Tape</a></li>        <li><a href="/search/xother.aspx">Other</a></li>    </ul>    </div>    </div>    </li></ul><h2>External Flashes &amp; Accessories</h2><ul>    <li><a href="/Categories/376-Flash.aspx">External Flash Units</a></li>    <li><a href="/search/XOCFPROFOTOAUG15.aspx">Off-Camera Flash by Profoto</a></li>    <li><a href="/search/xexternallitemod.aspx">Light Modifiers &amp; Diffusers </a></li>    <li><a href="/search/xexternalbattery.aspx">Battery Packs &amp; More</a></li></ul></div><a href="/profoto-off-camera-flash.aspx" onclick="_gaq.push(['_trackEvent','Nav: Video - Bottom','Click','Profoto Light Shaping Tools 150826']);"><img width="435" height="100" src="//www.henrys.com/Images/Nav-PCAs/Profoto-Light-Shaping-Tools-150826.jpg" alt="Profoto Light Shaping Tools" style="padding-top: 10px;" /></a>
							</div>
							<div id="header_lstCategories_dropProds_4" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="71892-AURORA-LBDR68W-RECESSED-WHITE-REC-BOX-24X32.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products71892-75x75-805419.jpg' alt="AURORA LBDR68W RECESSED WHITE REC BOX 24X32 " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="71892-AURORA-LBDR68W-RECESSED-WHITE-REC-BOX-24X32.aspx" title="">
                                                        AURORA LBDR68W RECESSED WHITE ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $49.50</span><br />
                                                        <span class="yousave" >Save: $161.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="84803-LASTOLITE-UMBRELLA-KIT-W-BRKT-STAND-CASE-34-.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products84803-75x75--300089460.jpg' alt="LASTOLITE UMBRELLA KIT W/BRKT/STAND/CASE 34&quot; " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="84803-LASTOLITE-UMBRELLA-KIT-W-BRKT-STAND-CASE-34-.aspx" title="">
                                                        LASTOLITE UMBRELLA KIT W/BRKT/...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $159.50</span><br />
                                                        <span class="yousave" >Save: $50.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="78511-CAMERON-10X12-MUSLIN-PINK-MIST.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products78511-75x75-948542.png' alt="CAMERON 10X12 MUSLIN PINK MIST " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="78511-CAMERON-10X12-MUSLIN-PINK-MIST.aspx" title="">
                                                        CAMERON 10X12 MUSLIN PINK MIST...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $49.50</span><br />
                                                        <span class="yousave" >Save: $40.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="74872-BOWENS-JETSTREAM-350-WIND-MACHINE-BW2558.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products74872-75x75-1039790.jpg' alt="BOWENS JETSTREAM 350 WIND MACHINE BW2558 " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="74872-BOWENS-JETSTREAM-350-WIND-MACHINE-BW2558.aspx" title="">
                                                        BOWENS JETSTREAM 350 WIND MACH...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $1,299.99</span><br />
                                                        <span class="yousave" >Save: $330.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_4" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Lighting - Right','Click','Cameron CF700 Tripod with BH30 Ballhead']);" href="/89998-CAMERON-CF700-TRIPOD-W-BH30-BALLHEAD.aspx"> <img height="380" width="185" alt="Cameron CF700 Tripod with BH30 Ballhead" src="//www.henrys.com/Images/Nav-PCAs/160108-right-Cameron_CF700_Tripod.jpg" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_5" class="A">
					<a href="javascript:void(0);">Printers</a>
					<div id="header_lstCategories_dropNav_5" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_5" class="drop-contentA">
								<h1>Printers</h1>
								<div class="col left"><h2>Printers</h2><ul>    <li><a href="/Categories/758-3D-Printers.aspx">3D Printers</a></li>    <li><a href="/Categories/302-All-In-One-Printers.aspx">All-In-One Printers</a></li>    <li><a href="/Categories/257-Dye-Sub-Printers.aspx">Dye Sub Printers</a></li>    <li><a href="/Categories/297-Inkjet-Printers.aspx">Inkjet Printers</a></li>    <li><a href="/Categories/306-Wide-Format-Printers.aspx">Wide Format Printers</a></li></ul><a onclick="_gaq.push(['_trackEvent','Nav: Printers - Bottom','Click','Canon PIXMA MG7520']);" href="/90046-CANON-PIXMA-MG7520-ALL-IN-ONE-BLACK.aspx"><img width="435" height="100" alt="Canon PIXMA MG7520" src="//www.henrys.com/Images/Nav-PCAs/160108-Bottom1-Canon_PIXMA_Printer.jpg" style="margin-top: 150px;" /></a></div><!-- Right Column --><div class="col right"><h2>Printer Supplies</h2><ul>    <li><a href="/Categories/759-3D-Printer-Filament.aspx">3D Printer Filament / Ink</a></li>    <li><a href="/Categories/59-Inkjet-cartridges.aspx">Inkjet Cartridges</a></li>    <li><a href="/search/PTPRINTPACK.aspx">Print Packs</a></li>    <li><a href="/Categories/122-Inkjet-Paper.aspx">Inkjet Paper</a></li>    <li><a href="/Categories/286-Print-Trimmers.aspx">Print Trimmers</a></li>    <li><a href="/Categories/217-Inkjet-Printer-Acc-.aspx">Printer Accessories</a></li></ul></div>
							</div>
							<div id="header_lstCategories_dropProds_5" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="21753-EPSON-STANDARD-PROOFING-PAPER-44-X164.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products21753-75x75-798648.jpg' alt="EPSON STANDARD PROOFING PAPER (44&quot;X164&#39;) " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="21753-EPSON-STANDARD-PROOFING-PAPER-44-X164.aspx" title="">
                                                        EPSON STANDARD PROOFING PAPER ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $277.99</span><br />
                                                        <span class="yousave" >Save: $138.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="72538-ILFORD-PRESTIGE-G-FIBER-SILK-8-5X11-10SH.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products72538-75x75-907089.jpg' alt="ILFORD PRESTIGE G FIBER SILK 8.5X11 10SH " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="72538-ILFORD-PRESTIGE-G-FIBER-SILK-8-5X11-10SH.aspx" title="">
                                                        ILFORD PRESTIGE G FIBER SILK 8...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $16.50</span><br />
                                                        <span class="yousave" >Save: $3.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="21633-INKPRESS-WHITE-GLOSS-FILM-24X50.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products21633-75x75-799016.jpg' alt="INKPRESS WHITE GLOSS FILM 24X50&#39; " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="21633-INKPRESS-WHITE-GLOSS-FILM-24X50.aspx" title="">
                                                        INKPRESS WHITE GLOSS FILM 24X5...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $149.50</span><br />
                                                        <span class="yousave" >Save: $151.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="85220-FUJI-SP-1-INSTAX-SHARE-PRINTER.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products85220-75x75--1305518414.jpg' alt="FUJI SP-1 INSTAX SHARE PRINTER " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="85220-FUJI-SP-1-INSTAX-SHARE-PRINTER.aspx" title="">
                                                        FUJI SP-1 INSTAX SHARE PRINTER...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $179.99</span><br />
                                                        <span class="yousave" >Save: $20.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_5" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Printers - Right','Click','Canon PIXMA iP8720']);" href="/83486-CANON-PIXMA-IP8720-PRINTER.aspx"><img width="185" height="380" alt="Canon PIXMA iP8720" src="http://www.henrys.com/Images/Nav-PCAs/160108-right-Canon_PIXMA.jpg" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_6" class="B">
					<a href="javascript:void(0);">More</a>
					<div id="header_lstCategories_dropNav_6" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_6" class="drop-contentB">
								<h1>More</h1>
								<div class="col left"><h2>Digital Darkroom</h2><ul>    <li><a href="/Categories/56-Computer-software.aspx">Photo Editing Software</a></li>    <li><a href="/Categories/213-Scanners.aspx">Scanners &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/289-Film-Scanners.aspx">Film Scanner</a></li>        <li><a href="/Categories/301-Flatbed-Scanners.aspx">Flatbed Scanners</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/671-Monitors.aspx">Monitors</a></li></ul><h2>Home and Portable Entertainment</h2><ul>    <li><a href="/Categories/665-Projectors.aspx">Projectors</a></li>    <li><a href="/Categories/260-Projection-Screens.aspx">Projection Screens</a></li></ul><br /></div><div style="float: none; display: inline-block; padding-left: 20px;" class="col right"><h2>Film &amp; Darkroom</h2><ul>    <li><a href="/Categories/90-Instant-And-Disposable.aspx">Instant &amp; Disposable</a></li>    <li><a href="/Categories/175-Film.aspx">Film &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/268-B-And-W-Bulk-Film.aspx">B&amp;W Bulk Film</a></li>        <li><a href="/Categories/379-B-And-W-Film.aspx">B&amp;W  Film</a></li>        <li><a href="/Categories/261-Colour-Bulk-Film.aspx">Colour Bulk Film</a></li>        <li><a href="/Categories/232-Colour-Print-Film.aspx">Colour Print Film</a></li>        <li><a href="/Categories/241-Colour-Slide-Film.aspx">Colour Slide Film</a></li>        <li><a href="/Categories/269-Instant-Film.aspx">Instant Film</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/96-Projection-Accessories.aspx">Projection Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/230-Bulbs.aspx">Bulbs</a></li>        <li><a href="/Categories/178-Misc-Proj-Acc-.aspx">Misc. Projection Accessories</a></li>        <li><a href="/Categories/260-Projection-Screens.aspx">Projection Screens</a></li>        <li><a href="/Categories/280-Slide-Accessories.aspx">Slide Accessories</a></li>        <li><a href="/Categories/200-SLIDE-MOUNTS.aspx">Slide Mounts</a></li>        <li><a href="/Categories/177-Slide-Trays.aspx">Slide Trays</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/107-Paper-And-Chemistry.aspx">Paper &amp; Chemistry &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/274-B-And-W-FB-MG-Paper.aspx">B&amp;W FB MG Paper</a></li>        <li><a href="/Categories/247-B-And-W-Film-Developers.aspx">B&amp;W Film Developers</a></li>        <li><a href="/Categories/238-B-And-W-Fixers.aspx">B&amp;W Fixers</a></li>        <li><a href="/Categories/262-B-And-W-Misc-Chemicals.aspx">B&amp;W Misc. Chemicals</a></li>        <li><a href="/Categories/264-B-And-W-Paper-Developers.aspx">B&amp;W Paper Developers</a></li>        <li><a href="/Categories/242-B-And-W-RC-MG-Paper.aspx">B&amp;W RC MG Paper</a></li>        <li><a href="/Categories/248-B-And-W-RC-Paper.aspx">B&amp;W RC Paper</a></li>        <li><a href="/Categories/263-B-And-W-Stop-Baths.aspx">B&amp;W Stop Baths</a></li>        <li><a href="/Categories/277-Colour-Negative-Paper.aspx">Colour Negative Paper</a></li>        <li><a href="/Categories/270-Colour-Print-Developers.aspx">Colour Print Developers</a></li>        <li><a href="/Categories/305-Misc-Chemicals.aspx">Misc. Chemicals</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/99-Darkroom.aspx">Darkroom &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/100-Enlargers-Acc-.aspx">Enlargers/Acc.</a></li>        <li><a href="/Categories/101-Enlarging-Equipment.aspx">Enlarging Equipment</a></li>        <li><a href="/Categories/251-Print-And-Negative-Pages.aspx">Print &amp; Negative Pages</a></li>        <li><a href="/Categories/185-Retouching.aspx">Retouching</a></li>        <li><a href="/Categories/382-Timers.aspx">Timers</a></li>    </ul>    </div>    </div>    </li></ul><h2>Sporting/Hobby</h2><ul>    <li><a href="/Categories/92-Binoculars.aspx">Binoculars</a></li>    <li><a href="/Categories/94-Telescopes.aspx">Telescopes</a></li>    <li><a href="/search/xspotscope.aspx">Spotting Scopes</a></li></ul><h2>Learning</h2><ul>    <li><a href="http://www.schoolofimaging.ca/Default.aspx">School of Imaging</a></li>    <li><a href="/Categories/184-Books.aspx">Books &amp; DVDs</a></li></ul></div><div class="col right"><h2>Gift Ideas</h2><ul>    <li><a href="/Categories/205-SPECIALTY-ITEMS.aspx">Photo Novelty</a></li>    <li><a href="/Gift-Cards.aspx">Gift Cards</a></li></ul><h2>Henry's Exclusive Brands</h2><ul>    <li><a href="/search/Bowens.aspx/1/80000010%5EBOWENS">Bowens</a></li>    <li><a href="/search/cameron.aspx">Cameron</a></li></ul><h2>Photofinishing</h2><ul>    <li><a href="/Moments-Online.aspx">Photofinishing Services</a></li>    <li><a href="/Categories/179-Frames-And-Albums.aspx">Frames &amp; Albums &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/180-ALBUMS.aspx">Albums</a></li>        <li><a href="/Categories/201-FRAMES.aspx">Frames</a></li>        <li><a href="/Categories/259-Mats-for-Frames.aspx">Mattes for Frames</a></li>        <li><a href="/Categories/276-Mount-Board.aspx">Mount Board</a></li>        <li><a href="/Categories/255-Mounting-Supplies.aspx">Mounting Supplies</a></li>    </ul>    </div>    </div>    </li></ul></div>
							</div>
							
							<div id="header_lstCategories_dropPCARight_6" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: More - Right','Click','Gift Cards 141024']);" href="/Gift-Cards.aspx"> <img width="185" height="380" alt=" Henry's Gift Carfs " src="//www.henrys.com/Images/Nav-PCAs/Gift-Cards-141017.gif" /></a></div>
							</div>
							<div id="header_lstCategories_dropPCABottom_6" class="dropPCABottom">
								
							</div> 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_7" class="B blue">
					<a href="javascript:void(0);">Used</a>
					<div id="header_lstCategories_dropNav_7" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_7" class="drop-contentB">
								<h1>Used</h1>
								<div class="col left"><h2>Still &amp; Video Equipment</h2><ul>    <li><a>Film Cameras &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/141-35mm-Auto-Focus.aspx">35mm Auto Focus</a></li>        <li><a href="/Categories/149-35mm-Manual-Focus.aspx">35mm Manual Focus</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/138-Digital-Cameras.aspx">Digital Cameras</a></li>    <li><a href="/Categories/164-Digital-SLR.aspx">Digital SLRs</a></li>    <li><a href="/Categories/134-Medium-Format.aspx">Medium Format</a></li>    <li><a href="/Categories/136-Lenses.aspx">Lenses &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/142-35mm-Auto-Focus.aspx">35mm Auto Focus</a></li>        <li><a href="/Categories/137-35mm-Manual-Focus.aspx">35mm Manual Focus</a></li>        <li><a href="/Categories/294-Used-Digital-Lenses.aspx">Used Digital Lenses</a></li>    </ul>    </div>    </div>    </li></ul><ul>    <li><a href="/Categories/158-Camcorders.aspx">Camcorders</a></li></ul></div><div class="col right" style="float: none; display: inline-block; padding-left: 20px;"><h2>Additional Equipment</h2><ul>    <li><a>Camera Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/148-35mm-Auto-Focus.aspx">35mm Auto Focus</a></li>        <li><a href="/Categories/147-35mm-Manual-Focus.aspx">35mm Manual Focus</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/131-Accessories.aspx">Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/132-Used-Acc-.aspx">Used Acc.</a></li>        <li><a href="/Categories/189-Used-Digital.aspx">Used Digital</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/143-Flash.aspx">Flash &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/145-35mm-Auto-Focus.aspx">35mm Auto Focus</a></li>        <li><a href="/Categories/144-35mm-Manual-Focus.aspx">35mm Manual Focus</a></li>        <li><a href="/Categories/556-Used-Digital-Flash.aspx">Used Digital Flash</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/152-Light-Meters.aspx">Light Meters</a></li></ul><h2>Refurbished</h2><ul>    <li><a href="/search/refurb.aspx/1/80000000^60%2c%2380000000^61">Refurbished DSLR Cameras</a></li>    <li><a href="/search/refurb.aspx/1/80000000^60%2c%2380000000^73">Refurbished Lenses</a></li>    <li><a href="/search/refurb.aspx/1/80000000%5E51">Refurbished Accessories</a></li></ul></div><div class="col right"><h2>Useful Information</h2><ul>    <li><a href="/Used-Photo-Video-Gear.aspx">Why Buy Used</a></li>    <li><a href="/Henrys-Extended-Life-Plan.aspx">Henry's Extended Life Plan</a></li>    <li><a href="/Trade-In-Trade-Up-Check-List.aspx">Henry's Trade-in Check List</a></li>    <li><a href="/Trade-In-Trade-Up-Used-Condition.aspx">Henry's Used Condition Chart</a></li></ul></div>
							</div>
							
							<div id="header_lstCategories_dropPCARight_7" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a href="/Used-Photo-Video-Gear.aspx" onclick="_gaq.push(['_trackEvent','Nav: Used - Right','Click','Used Gear Callout 151030']);"> <img width="185" height="380" src="//www.henrys.com/Images/Nav-PCAs/Used-Nav-Right-151030.jpg" alt="Used Gear Callout 151030" /></a></div>
							</div>
							<div id="header_lstCategories_dropPCABottom_7" class="dropPCABottom">
								<div style="width:665px;height:73px;padding:0;margin:0;border:0;overflow:hidden;"><a href="/Categories/136-Lenses.aspx?view=grid" onclick="_gaq.push(['_trackEvent','Nav: Used - Bottom','Click','Used Lenses 141024']);"><img width="665" height="73" src="//www.henrys.com/Images/Nav-PCAs/Used-Lenses-141110.jpg" alt="Used Lenses at Henry's" /></a></div>
							</div> 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_8" class="B orange">
					<a href="javascript:void(0);">On Sale</a>
					<div id="header_lstCategories_dropNav_8" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_8" class="drop-contentB">
								<h1>On Sale</h1>
								<div class="col left"><h2>Flyers &amp; Ads</h2><ul>    <li><a href="/Flyers-And-Ads.aspx">Current eFlyer</a></li></ul><h2>Camera &amp; Lenses</h2><ul>    <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy.aspx/1/283^On+Sale">Point and Shoots</a></li>    <li><a href="/search/compact system.aspx/1/80000000^60%2c%2380000000^61%2c%23283^On+Sale">Compact System Cameras</a></li>    <li><a href="/Categories/61-Digital-SLR-Cameras.aspx/1/283^On+Sale">Digital SLRs</a></li>    <li><a href="/Categories/171-Medium-Format.aspx/1/283^On+Sale">Medium Format</a></li></ul><ul>    <li><a>Lenses &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xlenscsc.aspx/1/283^On+Sale">Lenses for Compact System Cameras</a></li>        <li><a href="/search/xdslrlens.aspx/1/283^On+Sale">Lenses for Digital SLRs</a></li>        <li><a href="/search/xmediumformatlens.aspx/1/283^On+Sale">Lenses for Medium Format Cameras</a></li>    </ul>    </div>    </div>    </li>    <li><a>Lens Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/234-Lens-Caps.aspx/1/283^On+Sale">Lens Caps</a></li>        <li><a href="/Categories/168-Lens-Hoods.aspx/1/283^On+Sale">Lens Hoods</a></li>        <li><a href="/Categories/221-Filters.aspx/1/283^On+Sale">Filters</a></li>    </ul>    </div>    </div>    </li></ul><h2>Video</h2><ul>    <li><a href="/Categories/673-Action-Cameras.aspx/1/283^On+Sale">Action Cameras</a></li>    <li><a href="/search/xcamcorder.aspx/1/283^On+Sale">Camcorders &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xpocketable.aspx/1/283^On+Sale">Pocketable</a></li>        <li><a href="/search/xcamcordereveryday.aspx/1/283^On+Sale">Everyday Use</a></li>        <li><a href="/search/xcamenthusandpro.aspx/1/283^On+Sale">Enthusiast and Professional</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/114-Video-Accessories.aspx/1/283^On+Sale">Video Accessories</a></li></ul></div><div style="float: none; display: inline-block; padding-left: 20px;" class="col right"><h2>Camera &amp; Photography Accessories</h2><ul>    <li><a href="/Categories/83-Bags.aspx/1/283^On+Sale">Bags, Cases &amp; More &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/663-Pouches-for-Point-And-Shoots.aspx/1/283^On+Sale"><span>Pouches for Point-and-Shoots</span><br />        Protect your everyday gear.</a></li>        <li><a href="/Categories/664-Shoulder-Bags.aspx/1/283^On+Sale"><span>Shoulder Bags</span><br />        Designed for Compact System Cameras and DSLRs</a></li>        <li><a href="/Categories/660-Backpacks-And-Slings.aspx/1/283^On+Sale"><span>Backpacks &amp; Slings</span><br />        Ultimate comfort.</a></li>        <li><a href="/Categories/661-Camcorder-Bags.aspx/1/283^On+Sale"><span>Camcorder Cases</span><br />        For long lasting security.</a></li>        <li><a href="/search/xcarryingccase.aspx/1/283^On+Sale"><span>Carrying Cases</span><br />        Ideal for equipment mobility.</a></li>    </ul>    <p>    <a href="/Categories/83-Bags.aspx?view=grid"><span style="text-decoration: underline; color: #336699;" class="nav-ViewAll">View All On Sale Bags &amp; Cases</span></a>    </p>    </div>    </div>    </li>    <li><a>Batteries &amp; Power &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/227-Batteries.aspx/1/283^On+Sale">Batteries</a></li>        <li><a href="/Categories/245-Rechargeable-Batteries.aspx/1/283^On+Sale">Rechargeable Batteries</a></li>        <li><a href="/Categories/231-Digital-Camera-Batteries.aspx/1/283^On+Sale">Camera Batteries</a></li>        <li><a href="/Categories/383-Video-Batteries.aspx/1/283%5EOn+Sale">Camcorder Batteries</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/169-Camera-Straps.aspx/1/283^On+Sale">Camera Straps</a></li>    <li><a href="/Categories/222-Camera-Cleaning.aspx/1/283%5EOn+Sale">Cleaning Supplies</a></li>    <li><a href="/Categories/81-Filters.aspx/1/283^On+Sale">Filters</a></li>    <li><a>Flash, Modifiers &amp; More &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/376-Flash.aspx/1/283^On+Sale">External Flash Units</a></li>        <li><a href="/search/xexternallitemod.aspx/1/283^On+Sale">Light Modifiers &amp; Diffusers </a></li>        <li><a href="/search/xexternalbattery.aspx/1/283^On+Sale">Battery Packs &amp; More </a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/113-Memory.aspx/1/283^On+Sale">Memory Cards &amp; Media &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/272-Memory-Cards.aspx/1/283^On+Sale">Memory Cards</a></li>        <li><a href="/Categories/296-Memory-Card-Readers.aspx/1/283^On+Sale">Memory Card Readers</a></li>        <li><a href="/Categories/303-Memory-Acc-.aspx/1/283^On+Sale">Memory Card Storage &amp; More</a></li>        <li><a href="/Categories/293-Portable-Storage.aspx/1/283^On+Sale">External Harddrives</a></li>        <li><a href="/Categories/300-USB-Drives.aspx/1/283^On+Sale">USB Drives</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xphotoglove.aspx/1/283^On+Sale">Photo Gloves</a></li>    <li><a href="/search/xscreenorloop.aspx/1/283^On+Sale">Screen Protectors &amp; Loops</a></li>    <li><a href="/search/xsynccablemore.aspx/1/283^On+Sale">Sync Cords, Adapters &amp; More</a></li>    <li><a href="/search/xtriggerorrelease.aspx/1/283^On+Sale">Triggers &amp; Releases</a></li>    <li><a href="/search/tripod OR monopod.aspx/1/283^On+Sale">Tripods, Monopods &amp; Supports &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/442-Table-Mini-Pods.aspx/1/283^On+Sale">Tabletop &amp; Mini Tripods</a></li>        <li><a href="/Categories/374-PHOTO-TRIPOD-HEADS.aspx/1/283^On+Sale">Tripod Heads</a></li>        <li><a href="/Categories/443-Tripod-Legs-Only.aspx/1/283^On+Sale">Tripod Legs</a></li>        <li><a href="/Categories/378-PHOTO-TRIPOD-COMBO-W-HEAD.aspx/1/283^On+Sale">Complete Tripods</a></li>        <li><a href="/Categories/498-Video-Tripod-Heads.aspx/1/283^On+Sale">Video Heads</a></li>        <li><a href="/Categories/497-Video-Tripod-Combo-w-Head.aspx/1/283^On+Sale">Complete Video Tripods</a></li>        <li><a href="/search/xportablesupport.aspx/1/283^On+Sale">Stabilizers, Shoulder Brackets, Rigs &amp; More</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xvieworipiece.aspx/1/283^On+Sale">Viewfinders &amp; Eyepieces</a></li></ul></div><div class="col right"><h2>Lighting &amp; Studio</h2><ul>    <li><a>Studio Lighting &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xstudiohead.aspx/1/283^On+Sale">Studio Strobe Heads</a></li>        <li><a href="/search/xstudiokit.aspx/1/283^On+Sale">Studio Strobe Kits</a></li>        <li><a href="/search/xexternalpack.aspx/1/283^On+Sale">Battery Packs</a></li>        <li><a href="/search/xaddstudioaccess.aspx/1/283^On+Sale">Additional Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a>Studio Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/216-Backgrounds.aspx/1/283^On+Sale">Backgrounds</a></li>        <li><a href="/Categories/662-Background-Support.aspx/1/283^On+Sale">Background Supports</a></li>        <li><a href="/Categories/133-Light-Stands.aspx/1/283^On+Sale">Light Stands</a></li>        <li><a href="/search/xrailorboom.aspx/1/283^On+Sale">Rail Systems &amp; Booms</a></li>    </ul>    </div>    </div>    </li>    <li><a>Lighting Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/212-Light-Meters.aspx/1/283^On+Sale">Light Meters</a></li>        <li><a href="/search/xlightmodifier.aspx/1/283^On+Sale">Light Modifiers</a></li>    </ul>    </div>    </div>    </li></ul><h2>Printers</h2><ul>    <li><a href="/Categories/302-All-In-One-Printers.aspx/1/283%5EOn+Sale">All-In-One Printers</a></li>    <li><a href="/Categories/257-Dye-Sub-Printers.aspx/1/283%5EOn+Sale">Dye Sub Printers</a></li>    <li><a href="/Categories/297-Inkjet-Printers.aspx/1/283%5EOn+Sale">Inkjet Printers</a></li>    <li><a href="/Categories/306-Wide-Format-Printers.aspx/1/283%5EOn+Sale">Wide Format Printers</a></li>    <li><a>Ink, Paper &amp; Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/59-Inkjet-cartridges.aspx/1/283%5EOn+Sale">Inkjet Cartridges</a></li>        <li><a href="/search/PTPRINTPACK.aspx/1/283%5EOn+Sale">Print Packs</a></li>        <li><a href="/Categories/122-Inkjet-Paper.aspx/1/283%5EOn+Sale">Inkjet Paper</a></li>        <li><a href="/Categories/286-Print-Trimmers.aspx/1/283%5EOn+Sale">Print Trimmers</a></li>        <li><a href="/Categories/217-Inkjet-Printer-Acc-.aspx/1/283%5EOn+Sale">Pritner Accessories</a></li>    </ul>    </div>    </div>    </li></ul><!--     <h2>Online Only</h2>    <ul>        <li><a href="/photo-and-video-clearance-sale.aspx">Clearance Shelf</a></li>    </ul>    --></div>
							</div>
							
							<div id="header_lstCategories_dropPCARight_8" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: On Sale - Right','Click','Canon ELPH 160 160531 -160606']);" href="/online-only-special.aspx"><img height="380" width="185" alt="Online Only Special" src="//www.henrys.com/Images/Nav-PCAs/Canon-ELPH-Bundle-160531.png" /></a></div>
							</div>
							<div id="header_lstCategories_dropPCABottom_8" class="dropPCABottom">
								
							</div> 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_9" class="C header-menu-blue">
					<a href="javascript:void(0);">Learning</a>
					<div id="header_lstCategories_dropNav_9" class="drop BL">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_9" class="drop-contentC">
								<h1>Learning</h1>
								<div class="custom-nav-left"><h1>Learning</h1><p>You've got a new camera, camcorder or perhaps some lighting gear. Excellent. Now what? Henry's has several books for sale on a variety of topics to help you get up and running. Or take a course with <a href="http://www.schoolofimaging.ca">Henry's Learning Lab</a> and get advanced, in-depth instruction in a classroom environment.	</p><a class="button" href="http://www.schoolofimaging.ca">See Our Classes</a></div><div class="custom-nav-right"><h3>Classes by Category</h3><ul>    <li><a href=" http://www.henrys.com/Learning-Lab.aspx"><strong>Learning Lab Online<br />    Powered by KelbyOne</strong><br />    Online classes at your own pace</a></li>    <li><a href="http://www.schoolofimaging.ca/Categories/682-Photo-Courses.aspx"><strong>Photo Classes</strong><br />    Capture those perfect moments</a></li>    <li><a href="http://www.schoolofimaging.ca/categories/614-Special-Events.aspx"><strong>Special Events</strong><br />    From the classroom to real life</a></li>    <li><a href="http://www.schoolofimaging.ca/Categories/678-Software-Courses.aspx"><strong>Software Classes</strong><br />    Manage &amp; enhance your photos</a></li>    <li><a href="http://schoolofimaging.ca/search/flying%20cameras.aspx"><strong>Flying Camera Classes</strong><br />    There's something new in the air!</a></li></ul></div>
							</div>
							
							
							<div id="header_lstCategories_dropPCABottom_9" class="dropPCABottom">
								<div style="width:665px;height:73px;padding:0;margin:0;border:0;overflow:hidden;"><a href="/Learning-Lab.aspx" target="_blank" onclick="_gaq.push(['_trackEvent','Nav: Learning - Bottom','Click','Learning Lab 151203']);"><img alt="Henry's learning Lab" src="//www.henrys.com/Images/Nav-PCAs/151203-bottom-learning-lab-banner.gif" style="width: 665px; height: 73px;" /></a></div>
							</div> 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_10" class="C header-menu-blue last">
					<a href="javascript:void(0);">Events</a>
					<div id="header_lstCategories_dropNav_10" class="drop BL">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_10" class="drop-contentC">
								<h1>Events</h1>
								<!-- EVENTS NAV --><div class="custom-nav-left"><h1>Events</h1><p>Take a look at some of the awesome events we have coming up:</p><div><ul class="nav-events-list">    <!--<li><p>Sorry, there are currently no events scheduled, but check back often. We update regularly with new events and seminars!</p></li>-->  <li>    <p><a href="/in-store-seminars.aspx#yuridojc">"The Love of Seeing &ndash; It's All About Life"<br />    A Lecture with Photographer Yuri Dojc</a><br />    Thursday, June 2nd - Henry's Halifax</p>    </li>    <li>    <p><a href="/in-store-seminars.aspx#tupfwalk">Toronto Urban Photo Walk</a><br />    Saturday, June 4th - Henry's Toronto</p>    </li>    <li>    <p><a href="/in-store-seminars.aspx#canalwalk">Ottawa Urban Photo Walk</a><br />    Sunday, June 5th - Henry's Ottawa</p>    </li> <li>    <p><a href="/Droneography-In-Toronto.aspx">Above &amp; Beyond: Drones and the Future of Imaging</a><br />    Thursday, June 16th - Henry's Toronto</p>    </li>    <!-- ##POSTPONED - HOLD ONTO THIS CODE    <li>    <p><a href="/in-store-seminars.aspx#urban-ottawa">Urban Street Photography with Graeme Roy</a><br />    Saturday, May 28th - Henry's Ottawa (Bank Street)</p>    </li>    --></ul>    </div>    <div>    <ul class="nav-events-list">        <li>        <p>    <a href="/in-store-seminars.aspx">See All Events</a><br />        Check back regularly. We always have something happening!    </p>        </li>    </ul>    </div>    </div>    <div class="custom-nav-right">    <h3>The Henry's Family</h3>    <ul id="events-nav-right-list">        <li><a href="/Store-Locations.aspx"><strong>Store Locations</strong><br />        Find a Henry's Near You!</a></li>        <li><a href="/Rentals.aspx"><strong>Rentals</strong><br />        Rent Before You Buy</a></li>        <li><a href="/Professional-Services.aspx"><strong>Professional Services</strong><br />        Total Imaging Solutions</a></li>        <li><a href="http://www.learninglab.ca" target="_blank"><strong>Henry's Learning Lab</strong><br />        Online &amp; In-Person Learning</a></li>    </ul>    </div>
							</div>
							
							
							<div id="header_lstCategories_dropPCABottom_10" class="dropPCABottom">
								
							</div> 
						</div>
					</div>					
				</li>					
						
				</ul>
			
	</div>

</div>
<div class="center-content-container"><!-- ends in footer.aspx -->
<h2 class="center-lined" id="shippingBanner">
<span>All pricing in Canadian Dollars.<br />
Duty-Free exemption limit is now $800 USD for online purchases to USA. <a href="/Shipping-Policy.aspx">See Details.</a></span>
</h2>
			<div id="content-wrap" class="clearfix">
		        	
	<div id="content" class="clearfix">
		<div id="breadcrumb">
			
	<link type="text/css" rel="Stylesheet" href="styles/Static-Pages.css" />
    <meta name="keywords" content="privacy policy, Toronto, Ontario, Henrys" />
    <meta name="description" content="View our privacy policy at Henry's Camera" />
	<a href="Default.aspx">Home</a>
	: Privacy Policy
	
		</div>
		
    <div class="content-page">
        <h1>Privacy Policy</h1>
       	<p>Thanks for visiting! We want to assure you that your privacy is safe with us at Henry’s. This Privacy Policy covers our treatment of personal information that this site gathers when you access or use our online store. For details, continue reading:</p>
        
        <h2>Information We Collect</h2>
		
        <h3>Our visitors - Automatic Data Collection</h3>
        <p>Like most websites, henrys.com collects non-personally identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Our purpose in collecting non-personally-identifying information is to better understand how our visitors use this website. From time to time, we may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of this website.</p>
		<p>Henry’s also collects potentially personally-identifying-information like Internet Protocol (IP) addresses. We do not use such information to identify our visitors, however, and do not disclose such information, other than under the same circumstances that we use and disclose personally-identifying information, as described below.</p>
        
        <h3>Our Customers - Information You Provide to Us</h3>
        <p>Certain actions on our website do require us to collect personally-identifying information. Purchasers are asked to provide additional information, including as necessary the personal and financial information required to process transactions. In each case, Henry’s collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor's interaction with our company. We will never disclose personally-identifying information other than as described below. </p>
        
        <h3>Aggregated Statistics</h3>
		<p>Henry’s may collect statistics about the behavior of visitors to its websites. For instance, Henry’s may monitor the most popular sections or products on our site. Henry’s may display this non-personally-identifying information publicly or provide it to others. However, Henry’s does not disclose personally-identifying information other than as described below.</p>
        
        <h3>Protection of Certain Personally-Identifying-Information</h3>
		<p>Henry’s discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors, and affiliated organizations that (i) need to know that information in order to process it on our behalf, or to provide services requested by the customer, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors, and affiliated organizations may be located outside of your home country; by using Henrys.com, you consent to the transfer of such information to them. Henry’s will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, Henry’s discloses potentially personally-identifying and personally-identifying information only when required to do so by law, or when we believe in good faith that disclosure is reasonably necessary to protect the property or rights of Henry’s, third parties or the public at large. Henry’s takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.</p>
        
        <h3>Cookies</h3>
        <p>A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. Without cookies, Henry’s can't keep track of the items you've placed in your cart or perform other crucial ecommerce capabilities. We do not use cookies to intrude in your privacy or do anything other than match you up with your information on our site.</p>

		<h2>Communications</h2>
		<h3>Email Privacy Policy</h3>
		<p>We have created this email privacy policy to demonstrate our firm commitment to your privacy and the protection of your information.</p>
        
        <h3>Why did you receive an email from us?</h3>
		<p>If you received a mailing from us, (a) your email address is either listed with us as someone who has expressly shared this address for the purpose of receiving information in the future ("opt-in"), or (b) you have registered or purchased or otherwise have an existing relationship with us. We respect your time and attention by controlling the frequency of our mailings.</p>

		<h3>How we protect your privacy</h3>
		<p>We use security measures to protect against the loss, misuse, and alteration of data used by our system.</p>

		<h3>Sharing and Usage</h3>
		<p>We will never share, sell, or rent individual personal information with anyone without your advance permission or unless ordered by a court of law. Information submitted to us is only available to employees managing this information for purposes of contacting you or sending you emails based on your request for information and to contracted service providers for purposes of providing services relating to our communications with you.</p>
        
        <h3>How can you stop receiving email from us?</h3>
		<p>Each email sent contains an easy, automated way for you to cease receiving email from us, or to change your expressed interests. If you wish to do this, simply follow the instructions at the end of any email. </p>
		<p>If you have received unwanted, unsolicited email sent via this system or purporting to be sent via this system, please forward a copy of that email with your comments to <a href='mail&#116;&#111;&#58;inf&#111;%40&#104;%6&#53;nr&#121;%73&#46;co&#109;'>inf&#111;&#64;h&#101;nrys&#46;&#99;&#111;m</a> for review.</p>

        <h2>Conditions of Use</h2>
		<p>If you decide to visit the Henry’s website, your visit and any possible dispute over privacy is subject to this Privacy Policy and our Terms of Use, including limitations on damages, arbitration of disputes, and application of Ontario provincial law.</p>

		<h2>Privacy Policy Changes</h2>
		<p>Although most changes are likely to be minor and infrequent, Henry’s may change its Privacy Policy from time to time, and in our sole discretion. We encourage visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</p>
		<p>For information on our security policies, <a href="/Security-Policy.aspx">click here</a>. </p>

		<h2>Questions</h2>
		<p>Our full contact information, including location, can be found on our Contact page - <a href="/Customer-Care.aspx">click here</a>.</p>

    </div>
	
	</div>
	<div id="right-column">
		
    <div style="width:225px;height:100px;padding:0;margin:0;border:0;overflow:hidden;"><a href="http://www.learninglab.ca" id="learninglab-home-btn" onclick="_gaq.push(['_trackEvent','Homepage PCA','Click','Henrys Learning Lab 150324']);">Henry's Learning Lab</a></div>
    <br />
    
		<div class="recentlyviewedwidget">
			<h3>You've recently viewed:</h3>
			<ul>
				
		
			<li>
				<div class="tab-right-prod-top">
					<a href="93472-REFURBISHED-NIKON-D750-D-SLR-BODY.aspx" title="">
						<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products93472-50x50-2094450517.jpg'
							alt='REFURBISHED NIKON D750 D-SLR BODY ' />
					</a>
					<h4><a href="93472-REFURBISHED-NIKON-D750-D-SLR-BODY.aspx" title="">
						REFURBISHED NIKON D750 D-SLR BODY 
						</a>
					</h4>
                </div>
				<div class="tab-right-prod-bottom">
                	Price: $1,999.99
						
                        <a rel="nofollow"  class="addtocart" onclick ="LoadWarrantyModal1('/WarrantyModalView.aspx?productId=93472','93472');" href="#">
                        <span>Add to cart</span></a>
					
            	</div>
			</li>
		
		
	
		
			<li>
				<div class="tab-right-prod-top">
					<a href="93424-SIGMA-30MM-F1-4-DC-C-DN-MICRO-4-3-LENS.aspx" title="">
						<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products93424-50x50-8179360.jpg'
							alt='SIGMA 30MM F1.4 DC C DN MICRO 4/3 LENS ' />
					</a>
					<h4><a href="93424-SIGMA-30MM-F1-4-DC-C-DN-MICRO-4-3-LENS.aspx" title="">
						SIGMA 30MM F1.4 DC C DN MICRO 4/3 LENS 
						</a>
					</h4>
                </div>
				<div class="tab-right-prod-bottom">
                	Price: $459.99
						
                        <a rel="nofollow"  class="addtocart" onclick ="LoadWarrantyModal1('/WarrantyModalView.aspx?productId=93424','93424');" href="#">
                        <span>Add to cart</span></a>
					
            	</div>
			</li>
		
		
	
		
			<li>
				<div class="tab-right-prod-top">
					<a href="86312-PANASONIC-ENELOOP-AA4-1-2V-BATTERY.aspx" title="">
						<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products86312-50x50--2092404194.jpg'
							alt='PANASONIC ENELOOP AA4 1.2V BATTERY ' />
					</a>
					<h4><a href="86312-PANASONIC-ENELOOP-AA4-1-2V-BATTERY.aspx" title="">
						PANASONIC ENELOOP AA4 1.2V BATTERY 
						</a>
					</h4>
                </div>
				<div class="tab-right-prod-bottom">
                	Price: $19.99
						
                        <a rel="nofollow"  class="addtocart" onclick ="LoadWarrantyModal1('/WarrantyModalView.aspx?productId=86312','86312');" href="#">
                        <span>Add to cart</span></a>
					
            	</div>
			</li>
		
		
	
		
			<li>
				<div class="tab-right-prod-top">
					<a href="83872-GARY-FONG-LIGHTSPHERE-SPEED-MOUNT-LSC-SM.aspx" title="">
						<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products83872-50x50--64396601.jpg'
							alt='GARY FONG LIGHTSPHERE SPEED MOUNT LSC-SM ' />
					</a>
					<h4><a href="83872-GARY-FONG-LIGHTSPHERE-SPEED-MOUNT-LSC-SM.aspx" title="">
						GARY FONG LIGHTSPHERE SPEED MOUNT LSC-SM 
						</a>
					</h4>
                </div>
				<div class="tab-right-prod-bottom">
                	Price: $109.99
						
                        <a rel="nofollow"  class="addtocart" onclick ="LoadWarrantyModal1('/WarrantyModalView.aspx?productId=83872','83872');" href="#">
                        <span>Add to cart</span></a>
					
            	</div>
			</li>
		
		
	
		
			<li>
				<div class="tab-right-prod-top">
					<a href="70834-SONY-SEL-18-200-OSS-LEBLK-LENSNEX.aspx" title="">
						<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products70834-50x50-744361.jpg'
							alt='SONY SEL 18-200 OSS LE(BLK) LENS(NEX) ' />
					</a>
					<h4><a href="70834-SONY-SEL-18-200-OSS-LEBLK-LENSNEX.aspx" title="">
						SONY SEL 18-200 OSS LE(BLK) LENS(NEX) 
						</a>
					</h4>
                </div>
				<div class="tab-right-prod-bottom">
                	<span class="salePrice">Sale: $949.99</span>
						
                        <a rel="nofollow"  class="addtocart" onclick ="LoadWarrantyModal1('/WarrantyModalView.aspx?productId=70834','70834');" href="#">
                        <span>Add to cart</span></a>
					
            	</div>
			</li>
		
		
	
			</ul>
		</div>
	


	</div>
				
			</div>
			    

	<div id="footer-services">
        <h2 class="center-lined">More Reasons to Shop at Henry's</h2>
        <div><a href="/Henrys-Extended-Life-Plan.aspx"><img src="/Images/Homepage-Btns/exclusive-services/Help-v3.gif" width="192" height="150" alt="HELP" /></a></div>
        <div><a href="/Trade-In-Trade-Up.aspx"><img src="/Images/Homepage-Btns/exclusive-services/TradeUpgrade-v3.gif" width="192" height="150" alt="Trade-it p Grade It" /></a></div>
        <div><a href="/Half-Back.aspx"><img src="/Images/Homepage-Btns/exclusive-services/Halfback-v3.gif" width="192" height="150" alt="Henry's Halfback" /></a></div>
        <div><a href="/Price-Protection.aspx"><img src="/Images/Homepage-Btns/exclusive-services/PriceProtection-v3.gif" width="192" height="150" alt="Henry's Price Promise" /></a></div>
        <div class="last"><a href="/Reserve-Online-Pick-Up-In-Store.aspx"><img src="/Images/Homepage-Btns/exclusive-services/ReserveOnline-v3.gif" width="192" height="150" alt="Reserve Online" /></a></div>
    </div>
    
    <div id="footer-featured-brands" class="center-content-container">
        <h2 class="center-lined">Shop Featured Brands</h2>
        <a href="/bowens.aspx"><img src="/Images/footer-featured-brands/featured-bowens.jpg" width="120" height="60" alt="Bowens" /></a><a href="/cameron.aspx"><img src="/Images/footer-featured-brands/featured-cameron.jpg" width="120" height="60" alt="Cameron" /></a><a href="/search/Canon.aspx/1/80000010%5eCANON"><img src="/Images/footer-featured-brands/featured-canon.jpg" width="120" height="60" alt="Canon" /></a><a href="/search/Epson.aspx/1/80000010%5eEPSON"><img src="/Images/footer-featured-brands/featured-epson.jpg" width="120" height="60" alt="Epson" /></a><a href="/search/Fuji.aspx/1/80000010%5eFUJI"><img src="/Images/footer-featured-brands/featured-fuji.jpg" width="120" height="60" alt="Fujifilm" /></a><a href="/GOPRO-HERO4.aspx"><img src="/Images/footer-featured-brands/featured-gopro.jpg" width="120" height="60" alt="GoPro Hero4" /></a><a href="/search/Lowepro.aspx/1/80000010%5eLOWEPRO"><img src="/Images/footer-featured-brands/featured-lowepro.jpg" width="120" height="60" alt="Lowepro" /></a><a href="/search/Manfrotto.aspx/1/80000010%5eMANFROTTO"><img src="/Images/footer-featured-brands/featured-manfrotto.jpg" width="120" height="60" alt="Manfrotto" /></a>
        <a href="/search/Nikon.aspx/1/80000010%5eNIKON"><img src="/Images/footer-featured-brands/featured-nikon.jpg" width="120" height="60" alt="Nikon" /></a><a href="/search/Olympus.aspx/1/80000010%5eOLYMPUS"><img src="/Images/footer-featured-brands/featured-olympus.jpg" width="120" height="60" alt="Olympus" /></a><a href="/search/Panasonic.aspx/1/80000010%5ePANASONIC"><img src="/Images/footer-featured-brands/featured-panasonic.jpg" width="120" height="60" alt="Panasonic" /></a><a href="/search/Samsung.aspx/1/80000010%5eSAMSUNG"><img src="/Images/footer-featured-brands/featured-samsung.jpg" width="120" height="60" alt="Samsung" /></a><a href="/search/Sigma.aspx"><img src="/Images/footer-featured-brands/featured-sigma.jpg" width="120" height="60" alt="Sigma" /></a><a href="/search/Sony.aspx/1/80000010%5eSONY"><img src="/Images/footer-featured-brands/featured-sony.jpg" width="120" height="60" alt="Sony" /></a><a href="/search/Ricoh.aspx/1/80000010%5eRICOH"><img src="/Images/footer-featured-brands/featured-ricoh.jpg" width="120" height="60" alt="Ricoh" /></a><a href="/search/Westcott.aspx/1/80000010%5eWESTCOTT"><img src="/Images/footer-featured-brands/featured-westcott.jpg" width="120" height="60" alt="Westcott" /></a>
    </div>
</div> <!-- end .center-content-container -->
<!-- eNEWS SIGNUP -->
<div id="eNews-signup-container">
	<!-- h2 class="center-lined">Stay In Touch</h2 -->
    <h2 class="center-lined">Subscribe For Your Chance To Win!</h2>
	<div id="eNews-signup" class="center-content-container">
		<p>Sign up for Henry's  eNewsletter to learn about events near you, great deals, new products and be entered to Win Your Purchase! <a  style="color:#FFFFFF;" href="/Win-Your-Purchase-Contest-Rules.aspx" target="_blank">Find out more</a>.</p>
        <div id="signup-controls-container">
            <div id="signupEmail">
                <label>Email:</label><br />
                <input type="text" id="txtNewsletterSignup" title="Email Address" />
            </div>
            <div id="signupPostal">
                <label>Postal Code: <!--<span id="infoIcon" title="Enter your postal code to receive email content relevant to your area">?</span>--></label><br />
                <input type="text" id="txtPostalCode" title="Postal Code" />
            </div>	
            <div id="signupSubmit">
            	<input type="hidden" name="vars[source]" value="Henrys-Footer">
                <a href="#" id="btnNewsletterSubmit">Submit</a>
            </div>
        </div>	
    </div>
    <div id="footer-social" class="center-content-container">
        <ul class="socialMediaLinks">
            <li class="twitter"><a href="http://www.twitter.com/henryscamera" target="_blank">Twitter</a></li>
            <li class="facebook"><a href="https://www.facebook.com/HenrysCamera" target="_blank">Find Us On Facebook</a></li>
            <li class="googleplus"><a href="https://plus.google.com/113532467082526153366/posts" target="_blank">Google Plus</a></li>
            <li class="henrystv"><a href="http://instagram.com/henryscamera" target="_blank">Henry's Instagram</a></li>
        </ul>
    </div>
</div>

<div id="footerWrap" class="clear">
	<div id="footer" class="center-content-container">
		<div class="footer-box-top">		
			<div class="footPCA">
				<a href="http://www.learninglab.ca" target="_blank" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Henry\'s Learning Lab']);"><img width="191" height="60" src="//www.henrys.com/Images/footer-logos/HLL-Logo.jpg" alt="Henry's Learning Lab" /></a><a href="http://www.henrys.com/Henrys-Photofinishing.aspx" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Photo Printing']);"><img width="192" height="60" src="//www.henrys.com/Images/footer-logos/photo-printing-logo.jpg" alt="Photo Printing" /></a><a href="https://www.henrys.com/rentals.aspx" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Henry\'s Rentals']);"><img width="192" height="60" src="//www.henrys.com/Images/footer-logos/rentals-logo.jpg" alt="Henry's Rentals" /></a><a href="http://stores.ebay.com/henrys-com" target="_blank" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Henry\'s eBay Store']);"><img width="192" height="60" src="//www.henrys.com/Images/footer-logos/ebay-logo.jpg" alt="Henry's eBay Store" /></a><a href="http://www.henrys.com/professional-services.aspx" target="_blank" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Henry\'s Professional Services']);"><img width="191" height="60" src="//www.henrys.com/Images/footer-logos/HPS-logo.jpg" alt="Henry's Professional Services" /></a>
			</div>
		</div>	
		<div id="footer-box">
			<div class="column">		
				<h5>Shop</h5>			
				<ul>				
					<li><a href="Categories/60-Cameras.aspx">Cameras</a></li>
					<li><a href="Categories/69-Camera-Accessories.aspx">Camera Accessories</a></li>
                    <li><a href="Droneography.aspx">Drones/UAV</a></li>
					<li><a href="Categories/729-Mobile.aspx">Mobile</a></li>
					<li><a href="Categories/64-Video.aspx">Video</a></li>
					<li><a href="Categories/48-Lighting.aspx">Lighting</a></li>
					<li><a href="Categories/53-Computers.aspx">Computers</a></li>
					<li><a href="Categories/57-Printers.aspx">Printers</a></li>
					<li><a href="Categories/91-Binoculars.aspx">Binoculars</a></li>
					<li><a href="Categories/130-Used.aspx">Used</a></li>
					<li><a href="Categories/89-Film-And-Darkroom.aspx">Film and Darkroom</a></li>
                    <!-- <li><a href="Photo-And-Video-Clearance-Sale.aspx">Henry's Clearance Shelf</a></li> -->
				</ul>
			</div>
			<div class="column">
				<h5>Shopping Tools</h5>
				<ul>
					<li><a href="ShoppingCart.aspx">My Cart</a></li>
					<li><a href="WishList.aspx">My Wishlist</a></li>
					<li><a href="Account.aspx">My Account</a></li>		
					<li><a href="Flyers-And-Ads.aspx">Flyers and Promotions</a></li>	
                    <li><a href="Gift-Cards.aspx">Gift Cards</a></li>
					<li><a href="http://stores.ebay.com/henrys-com" target="_blank">eBay Store</a></li>		
					<li><a href="Sign-Out.aspx">Sign Out</a></li>
				</ul>
			</div>
			<div class="column">
				<h5>Information</h5>
				<ul>
					<li><a href="Shipping-Policy.aspx">Shipping Policy</a></li>				
					<li><a href="Returns-Exchange-Policy.aspx">Returns/Exchange Policy</a></li>	
					<li><a href="Warranty-Policy.aspx">Warranty Policy</a></li>
					<li><a href="Repairs.aspx">Repairs</a></li>			
					<li><a href="Privacy-Policy.aspx">Privacy &amp; Security Policy</a></li>
					<li><a href="Terms-of-Use.aspx">Terms of Use</a></li>				
					<li><a href="Price-Protection.aspx">Price Match Policy</a></li>	
					<li><a href="In-store-seminars.aspx">Events &amp; In-store Seminars</a></li>
					<li><a href="Sitemap.aspx">Sitemap</a></li>
					<li><a href="Subscribe-to-Henrys.aspx">Subscribe to eNewsletters</a></li>						
				</ul>
			</div>
			<div class="column">
				<h5>Services</h5>
				<ul>
					<li><a href="http://www.schoolofimaging.ca/?cart=lx7XDQa6HCOyEjYaUz9OgaNQrTlotrrW6DITEbVInMqfkbc2m%2b6DOl9V3AxLSkQH3DQy6rjJnw3Uet0JSSnLEg9WckhUIviOEMQOgKfPyHynATqyo0S9tUXDoJ9AUyW86VBPiRJCCrhY4GXI5v3DB7p1yBo%2bPZIwtvID4UEDg2M%3d">Henry's Learning Lab</a></li>		
					<li><a href="Henrys-Photofinishing.aspx">Henry's Photo Printing</a></li>				
					<li><a href="Rentals.aspx">Henry's Rentals</a></li>				
					<li><a href="Half-Back.aspx">Half Back</a></li>
					<li><a href="Henrys-Leasing-Equilease.aspx">Leasing</a></li>					
                    <li><a href="Ship-Direct.aspx">Ship Direct</a></li>
					<li><a href="Trade-In-Trade-Up.aspx">Trade It, Upgrade It</a></li>	
					<li><a href="Henrys-Extended-Life-Plan.aspx">Henry's Extended Life Plan</a></li>		
					<li><a href="Outlet-Centre.aspx">Henry's Outlet Centre</a></li>			
				</ul>
			</div>
			<div class="column">
				<h5>About Henry's</h5>
				<ul>
            		<li><a href="Store-Locations.aspx">Locations</a></li>				
					<li><a href="Company-Info.aspx">Company Info</a></li>
					<li><a href="Customer-Care.aspx">Customer Care</a></li>
					<li><a href="Professional-Services.aspx">Henry's Professional Services</a></li>
					<li><a href="careers-at-henrys.aspx">Careers</a></li>
					<li><a href="https://henrys.affiliatetechnology.com/">Affiliates</a></li>
					<li><a href="Company-History.aspx">Henry's 100th Anniversary</a></li>
					<li><a href="Get-Connected-Henrys-Social-Media.aspx">Henry's Social Media</a></li>
                    <li><a href="http://www.henrys.com/love.aspx">Community Involvement</a></li>
                     <li><a href="http://blog.henrys.com/" target="_blank">Henry's Blog</a></li>
				</ul>
			</div>				
		</div>
	</div>
</div>

<div id="footerFinePrintWrapper">
    <div id="footer-copyright" class="center-content-container">
        <div style="float:left;background-color: #fff;border-radius: 6px;padding: 10px;">
            <script type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
        <img src="/Images/payment-methods-footer.jpg" width="350" alt="Accepted Payments: Paypal, Visa, Mastercard, Visa Debit, Amex" />
        </div>
        <div style="float: left; width: 456px; margin-left: 16px;">
            <p><strong>Need help? Call us at  1-800-461–7960 or by reach us <a href="/Customer-Care.aspx#email" style="color: #fff;">by email</a>.</strong></p>
            <p>All prices listed are in Canadian dollars. We make every effort to ensure our prices are accurate. We do, however, reserve the right to advise you of any errors prior to processing your invoice. If you are not willing to accept changes on these errors we will cancel your order. Henry's reserves the right to limit quantities. We apologize for any inconvenience this may cause.</p>
            <p>&copy; 1996-2016 Cranbrook Glen Enterprises Ltd. All Rights Reserved.</p>
        </div>
    </div>
</div>

        </div>
        
<div id="minicart">
    <div id="minicart-lineitems" class="clearfix"></div>
    <div id="minicart-subtotal-container" class="clearfix">
		<span class="label">Sub-Total:</span> <span id="minicart-subtotal" class="value"></span>
    </div>
    <!--<p class="fullcart" style="color:red"> </p>-->
    <div class="cartsummary1">Enter postal/zip code at your</div>
    <div class="cartsummary2"><a href="ShoppingCart.aspx">full shopping cart  </a> for taxes and shipping.</div>
   		
	<div class="controls">
		<a href="ShoppingCart.aspx"><img src="/WebResource.axd?d=x0KL7lKQiTHdUOV1VziUpa6-a09hXkY365p2Au_uslkZGZkkmRu8PLUrPxxEmC6LELz902-VVN9Pv6RCgaat3fgbiZGiQuPn4h3tBjXlvdBMVHD1gFUN6ed-ANkAGlzJ3O6qEZTC3T7qii2m3gLiNIS2F8elN_GR5-Lc5f2iGoVhg_8I0&amp;t=635980466583434759" alt="Checkout" /></a>			
		<div style="display: inline">
		<a class="btnReserveMiniCart" href="ReserveInStoreCartNotification.aspx?origin=2"><img src="Images/minicar-reserve.jpg" alt="Reserve In Store" /></a>			
		</div>

	</div>
	<div class="controls">
		<a href="ShoppingCart.aspx">Modify Cart</a> | <a href="WishList.aspx">Wish List</a> | <a href="#" id="btnCloseMiniCart">Close</a>
	</div>
</div>

 <script id="miniCartItemTemplate" type="text/html">
	<!--
	<# 	
		for(var i = 0; i < lineItems.length; i++) {       
         var lineItem = lineItems[i]; #>
         
		<div class="minicart-lineitem" id="minicart-lineitem-<#= lineItem.Id#>">
			<div class="img"><a href="<#= lineItem.LinkUrl === '' ? hashSymbol : lineItem.LinkUrl #>"><img src="<#= rootRel #>ImageHandler.axd?imageId=<#= lineItem.ImageId #>&amp;width=48&amp;height=48&amp;constraint=4&amp;async=false" alt="" \/><\/a><\/div>			
			<div class="details">
				<h3><a href="<#= lineItem.LinkUrl === '' ? hashSymbol : lineItem.LinkUrl #>"><#= lineItem.LineItemType === Henrys.Web.WebServices.ShoppingCartLineItemType.BonusProduct && lineItem.UnitPrice === 0 ? '<span class="bonus">Bonus!<\/span> ' : ''#><#= lineItem.DisplayText #><\/a><\/h3>
				<div class="price"><#= lineItem.LineItemType === Henrys.Web.WebServices.ShoppingCartLineItemType.BonusProduct && lineItem.UnitPrice === 0 ? '<span class="bonus">FREE<\/span>' : lineItem.UnitPrice.localeFormat('C2') #><#= lineItem.RetailPrice > lineItem.UnitPrice && lineItem.LineItemType !== Henrys.Web.WebServices.ShoppingCartLineItemType.BonusProduct ? ' <span class=\"savelight\">(Save ' + (lineItem.RetailPrice - lineItem.UnitPrice).localeFormat('C') + ')<\/span>' : '' #> <#= lineItem.Quantity > 1 ? '<span class=\'qty\'>x ' + lineItem.Quantity + '<\/span>' : '' #><\/div>
			<\/div>
			<div class="delete" rel="<#= lineItem.Id#>">x<\/div>			
		<\/div>
	<# } #>
	-->
</script>
        
<div id="WelcomeModalContainer">
<div id="welcomeModalContent">
<div id="WelcomeModalTop">
<div class="WelcomeClose"><a style="display:block;width:30px;height:30px;" id="WelcomeModalClose" onclick="CloseWelcomeModal();" href="#"></a></div>

</div>
<div id="WelcomeModalBottom">
<div style="padding-top:15px;">




<div style="padding:5px;"><span class="emailSpan">Email</span>
<input name="ctl00$ctl00$welcomeModal$txtWelcomeSignUp" type="text" id="welcomeModal_txtWelcomeSignUp" style="width:250px;height:30px;margin-right:20px;font-size:20px;" />
<span class="emailSpan">Postal Code</span><input name="ctl00$ctl00$welcomeModal$txtWelcomePostalCode" type="text" id="welcomeModal_txtWelcomePostalCode" style="width:150px;height:30px;" />
<div style="padding:5px;text-align:center;padding-top:20px;">
<input type="image" name="ctl00$ctl00$welcomeModal$btnSubmitEmail" id="welcomeModal_btnSubmitEmail" src="Images/Welcome_Modal_Submit.png" onclick="trackClick();WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$welcomeModal$btnSubmitEmail&quot;, &quot;&quot;, true, &quot;WelcomeSubmit&quot;, &quot;&quot;, false, false))" style="cursor:pointer;" />
</div>
</div></div></div>

</div>
<div id="WelcomeThankYou"><div class="WelcomeThankYouContent" alt="Thanks for subscribing"></div>
<div class="WelcomeClose"><a style="display:block;width:30px;height:30px;" id="WelcomeModalThankYouClose" onclick="CloseWelcomeModal();" href="#"></a></div>
<div class="WelcomeThankYouClose" style="margin-left: auto; margin-right: auto; text-align: center;"><a href="#" onclick="CloseWelcomeModal();">Close This Window</a></div>
</div>
</div>

<script type="text/javascript">
    function trackClick() {
        _gaq.push(['_trackEvent', 'Welcome Modal', 'Click', 'RuleSet Name:']);
    }
   
</script>




		
	   		

		<!-- Sailthru Horizon -->
		<script type="text/javascript">
			(function () {
				function loadHorizon() {
					var s = document.createElement('script');
					s.type = 'text/javascript';
					s.async = true;
					s.src = location.protocol + '//ak.sail-horizon.com/horizon/v1.js';
					var x = document.getElementsByTagName('script')[0];
					x.parentNode.insertBefore(s, x);
				}
				loadHorizon();
				var oldOnLoad = window.onload;
				window.onload = function () {
					if (typeof oldOnLoad === 'function') {
						oldOnLoad();
					}
					Sailthru.setup({
						domain: 'horizon.henrys.com'
					});
				};
			})();
		</script>   
  
        <!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1067831525;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1067831525/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


 

<script type="text/javascript">
//<![CDATA[

			if (txtResource == null) {
				var txtResource = {};
			}
			txtResource['Alert_Err_ItemCombination'] = 'Sorry, we don\\\'t carry this item combination';txtResource['Alert_Err_ValidSelection'] = 'Please make a valid selection before adding this to your cart';txtResource['Alert_Err_OutofStock'] = 'Sorry, this item is currently out of stock';$(document).ready(function(e) {
var cultureObject = Sys.CultureInfo.CurrentCulture;
var nfObject = cultureObject.numberFormat;
nfObject.CurrencyDecimalSeparator = ".";
nfObject.CurrencyDecimalDigits = "2";
nfObject.CurrencyGroupSeparator = ",";
nfObject.CurrencyGroupSizes = new Array('3');
nfObject.CurrencyNegativePattern = "1";
nfObject.CurrencyPositivePattern = "0";
nfObject.CurrencySymbol = "$";
});
          function resetPosition(object, target) {

			var tbposition = findPositionWithScrolling(target);
			var xposition = tbposition[0] + $(target).width();
			var yposition = tbposition[1] + 30;

			$common.setLocation(object, new Sys.UI.Point(xposition, yposition));
		}

		function findPositionWithScrolling(oElement) {
		if (typeof (oElement.offsetParent) != 'undefined') {
			var originalElement = oElement;
			for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
				posX += oElement.offsetLeft;
				posY += oElement.offsetTop;
				if (oElement != originalElement && oElement != document.body && oElement != document.documentElement) {
					posX -= oElement.scrollLeft;
					posY -= oElement.scrollTop;
				}
			}
			return [posX, posY];
		} else {
			return [oElement.x, oElement.y];
		}
	}

	function UpdatePopUpExtenderPosition(objectid, targetid) {
		var a = document.getElementById(objectid);
		var b = document.getElementById(targetid);
		if(a != null){
			resetPosition(a,b);
		}
	}
	ValidatedTextBoxOnKeyPress = function ValidatedTextBoxOnKeyPress(event) {
		if (event.keyCode == 13 && !$.browser.mozilla) {
			ValidatorOnChange(event);
			var v;
			if ((typeof (event.srcElement) != 'undefined') && (event.srcElement != null)) {
				v = event.srcElement.Validators;
			} else {
				v = event.target.Validators;
			}
			return AllValidatorsValid(v);
		}
		return true;
	};//]]>
</script>
</form>
    <!-- 
	Connected To: ODWEBB02
	-->	
</body>
</html>
