
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml">
<head id="Head1"><title>
	Privacy Policy - Henry's best camera store in Canada
</title><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info = {"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"311198b2e4","applicationID":"11674521,11674490,2215095","transactionName":"MgdaYEtVWUFSVBBQCQtNeWdpG0dAWkEFWh9IEldYUFdOHFJEFEE=","queueTime":0,"applicationTime":59,"ttGuid":"40B1934F2AD7C169","agent":""}</script><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({1:[function(e,n,t){function r(e,n){return function(){o(e,[(new Date).getTime()].concat(a(arguments)),null,n)}}var o=e("handle"),i=e(2),a=e(3);"undefined"==typeof window.newrelic&&(newrelic=NREUM);var c=["setPageViewName","setCustomAttribute","finished","addToTrace","inlineHit"],u=["addPageAction"],f="api-";i(c,function(e,n){newrelic[n]=r(f+n,"api")}),i(u,function(e,n){newrelic[n]=r(f+n)}),n.exports=newrelic,newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),o("err",[e,(new Date).getTime()])}},{}],2:[function(e,n,t){function r(e,n){var t=[],r="",i=0;for(r in e)o.call(e,r)&&(t[i]=n(r,e[r]),i+=1);return t}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],3:[function(e,n,t){function r(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(0>o?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=r},{}],ee:[function(e,n,t){function r(){}function o(e){function n(e){return e&&e instanceof r?e:e?c(e,a,i):i()}function t(t,r,o){e&&e(t,r,o);for(var i=n(o),a=l(t),c=a.length,u=0;c>u;u++)a[u].apply(i,r);var s=f[m[t]];return s&&s.push([w,t,r,i]),i}function p(e,n){g[e]=l(e).concat(n)}function l(e){return g[e]||[]}function d(e){return s[e]=s[e]||o(t)}function v(e,n){u(e,function(e,t){n=n||"feature",m[t]=n,n in f||(f[n]=[])})}var g={},m={},w={on:p,emit:t,get:d,listeners:l,context:n,buffer:v};return w}function i(){return new r}var a="nr@context",c=e("gos"),u=e(2),f={},s={},p=n.exports=o();p.backlog=f},{}],gos:[function(e,n,t){function r(e,n,t){if(o.call(e,n))return e[n];var r=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(e,n,t){function r(e,n,t,r){o.buffer([e],r),o.emit(e,n,t)}var o=e("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(e,n,t){function r(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");n.exports=r},{}],loader:[function(e,n,t){function r(){if(!b++){var e=y.info=NREUM.info,n=s.getElementsByTagName("script")[0];if(e&&e.licenseKey&&e.applicationID&&n){u(m,function(n,t){e[n]||(e[n]=t)});var t="https"===g.split(":")[0]||e.sslForHttp;y.proto=t?"https://":"http://",c("mark",["onload",a()],null,"api");var r=s.createElement("script");r.src=y.proto+e.agent,n.parentNode.insertBefore(r,n)}}}function o(){"complete"===s.readyState&&i()}function i(){c("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var c=e("handle"),u=e(2),f=window,s=f.document,p="addEventListener",l="attachEvent",d=f.XMLHttpRequest,v=d&&d.prototype;NREUM.o={ST:setTimeout,CT:clearTimeout,XHR:d,REQ:f.Request,EV:f.Event,PR:f.Promise,MO:f.MutationObserver},e(1);var g=""+location,m={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-952.min.js"},w=d&&v&&v[p]&&!/CriOS/.test(navigator.userAgent),y=n.exports={offset:a(),origin:g,features:{},xhrWrappable:w};s[p]?(s[p]("DOMContentLoaded",i,!1),f[p]("load",r,!1)):(s[l]("onreadystatechange",o),f[l]("onload",r)),c("mark",["firstbyte",a()],null,"api");var b=0},{}]},{},["loader"]);</script><link rel="Stylesheet" type="text/css" href="Styles/Common.css?v=2016.03.17" /><link rel="Stylesheet" type="text/css" href="Styles/colorbox.css?v=2014.08.21" /><link rel="Stylesheet" type="text/css" href="Styles/jquery.fancybox.css?v=2014.08.21" /><link rel="Stylesheet" type="text/css" href="Styles/jquery.alerts.css?v=2014.08.21" /><link rel="Stylesheet" type="text/css" href="Styles/Henrys.css?v=2015.08.21" />
    
<!--Start of Zopim Live Chat Script-->
<!-- OFF
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2tGk2NEvRr4JOhG1GbhjpcyonVzVBkH7";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
-->
<!--End of Zopim Live Chat Script-->
    

    <script type='text/javascript'>
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-1012104-3']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
			</script>


	
	<link rel="alternate" media="only screen and (max-width: 640px)" href="//m.henrys.com/Privacy-Policy.aspx" />

	<!--[if IE]>
	<style type="text/css">
		.browsetabs .carousel-dots { top: -8px !important; }
	</style>		
	<![endif]-->
	<link rel="shortcut icon" type="image/x-icon" href="/Images/favicon.ico"/>
	<link rel="icon" type="image/x-icon" href="/Images/favicon.ico"/>		
	<link rel="image_src" href="/Images/logo.jpg" />

	
    <script>        (function () {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', "1421570248097921"]);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(["track", "PixelInitialized", {}]);
    </script>
    <noscript><img height="1" width="1" border="0" alt="" style="display:none" src="https://www.facebook.com/tr?id=1421570248097921&amp;ev=NoScript" /></noscript>


    
    	
 <script type="text/javascript">
var google_tag_params = {
ecomm_pagetype: 'other'
};
</script>
	

<!--START AffiliateTraction CODE-->
<img src="https://henrys.affiliatetechnology.com/impression.php" width="0" height="0" style="display:none;" />
<script type="text/javascript" src="https://henrys.affiliatetechnology.com/abandonment.js"></script>
<!-- END AffiliateTraction CODE -->



<script type='text/javascript' >
var google_tag_params = {ecomm_pagetype: 'other'};</script>
<script type='text/javascript'>
				var _gaq = _gaq || [];
				
				_gaq.push(['_setAccount', 'UA-1012104-3']);
				_gaq.push(['_setCampSourceKey', 'utm_source']); 
				_gaq.push(['_setCampTermKey', 'utm_term']);
			_gaq.push(['_setCampNameKey', 'utm_campaign']);
			_gaq.push(['_setCampMediumKey', 'utm_medium']);
			_gaq.push(['_setCampContentKey', 'utm_content']);       
				_gaq.push(['_trackPageview']);
				(function () {
					var ga = document.createElement('script');
					ga.type = 'text/javascript';
					ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(ga, s);
				})();
			</script></head>
<body class="en-CA">
<script>
	dataLayer = []; // for Google Tag manager
</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-SN9F"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-SN9F');</script>
<!-- End Google Tag Manager -->

    <form method="post" action="/Privacy-Policy.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VSTATE" id="__VSTATE" value="H4sIAAAAAAAEAOw9CXQcxZVqSdPSyJZlY7A5DG6bwwZmRtLosoxt0Gkrtg408gGEnbRmWlLjmemhu0cHuSAkIWwCCSGEEDYJgUAgJJCQA0ISEkLuJclm2dwHOZccm5ucXPt/dXV3dXeNNJKNLfQyzy51/apf/9evX7+Orvr9nFB3/IpQS8rMNDScZoUTipxW9NMyhtklm8q4pquKAUl6puG0IV1L71ENcxfJUnfs2WUr0tZv29lllWXllfA0Vv4c/OrSIWmuQn34y8rKl7H4pTEVnx9TpRXa7i90PB1qLQmzMSAkQB1iUVNazlRy5lBGTim7tAwWo6vjE2aXlilkcx6wkoKMmZl9qjKlpPer6XHFtAphKnmcUN6XhmgF/BfKH7imkvy9/Err701/qiivmH/92ziVKA2zkYN5SjLZBZXWtYwxrFxaUHVlSDPMTjl1cLcyk0yuEUKb2LKnlExKyyr9WlrOnDZq5hKF0axq9mRlNVMiEy3Ph040PR+FblmwoJufD3Zag+zUhZZFG9sb2tvjW9rbGurSa8rH7KCSBOXVdcdVbKyvz8YmlJw+Y8Sg8eqHdHVSTs1Eh7SMmpqJyUZ+enV9X1YeV4z6MXlShU4Qg4AHS5eH69aUn1I5oStjoZV2how2rsUuyY+ny4WVaaBfXoFclIfqnOcxlEB53ZrKU5YlN3TLptypFXLpcYz1mUq2C2Im9gWLd7FcIMxjWDNWd1xVtLUt3tIa7Rzc3zMcbYu29PdHe5uiLdHevsSungt6ont6BhLR/uZoE6lMWXrCNPNb6+vlyxpbm7a0NccmtVjWUFK5dCynmPWNzU2p9rGWxpbU2FhzekurPNo61tDe2j4my6mmtva4XS0UdyFlGhbxlobploZo65b2li0tWNnTCTdSWwy4kXqbYi0S5UZCbqTN/c31TWdK8+S8tEJBznUoyyrKYfyKqyvLhLKyMtQn/Iu/5eUQKIM6aEr3TE7Oqikj1qXpSqyzYKg5xTB65RSoUUTap+iGquW2N8YaYi0NsYaI1FXImAVd2Z5TCqYuZyLSUGEUVAUMwoh2UMltb21tamhtibc3NDS1jsrNSghpRuckFaPs9uXGtLOQw2TeAgzIWaUmmQehj6hmRlmVNCY03exWjJSu5k3gbWUyo+XGGYCYHNXlXLoumZVzhTE5hezqfd3Lk7pigikCDU8py5OpgmFq2RFl2mxkI3E20hROHlRmpjQ9bVQlpxQ09mJSBZ00qoE9NaXmxpclU05PPDapwggxrsvIRUIxTUg31vKAB7KZlUkgoOTSSnqQcG2s9gMg0/FJ2TDU8ZyS7jBNXR0tmMpOXSvkjWVJ2QYYNcledVpJd4FpXp6cknVlQisYSh8IICMb5t58GhhM7zVTq9n4MJgJgJ2QzEMLazk5o15GODy/oBiE+ppkv6KnJkCQqiGPZhTaPtXJThRuX3d10pSnu7S0UpvEAjvSaUIklITkgzVJ1ehWMgoQWgZin1RRifrSNclJOaeaM3v1TEVSTYvJnAb8L8e6yKmJLIyZxhqvWnTKhnI2ZN3AAxuFfB50wRjAUjbNlqPDJXACLx/hYz0vhWFNwF9FGYaVlZUVlUKlUFFRVlaBAelYQhk+QpBLzBigJbGBQiaDsntJ40UXUVBfzmyKR6SskdL0jDrq9rBm6GEldbDRtja5JdXS2tje1Kw0bGm/+OLQ5rm7136ivdjtm+fOjGYXOyLMbDJKCtUBEXeU3ImHrL4RLOJBgYrBBRuxnUpOAYQYjl4oqbnJ0MFvBglEpCNixi6++Ky5+UqYwFinPI5V3VKytHq83R6RX1GCmJ5HhXrk8DQTrZ9juvbJmYJyxNorVF1HawFzChg8ssqlxbulneUwC/LRwyrIIb6pPmISFaprq4Xq6vsOT6XQah8x1h84PDy748gR47xaEP4l8CMpcLS/Io7jpc23xUrIGispa2LPBW0tnf3NTWKo9OJFyFrYVsjs2JZRd+zNQCWi+9W0IvWqxoQyo0h7lJxRu60eEjFDl5bNw4w3Iu3B4d6askak7oKO9s7N1pEbzyiSNibhDsVWqXFLg5SGOaqi2EXVA0GxCkjXWlzWS10dA4MDNRXNTQ1iNcA/U37EeYL/vZou9aspXZN6tYIeHZlQYWrOUsoZpl4g2rZVapOsuXJ9u9STUcgUzs3ar+bUbCEL5aRQh8albtBJOZdStkrYFGou5uZ1WZHSqjyOJlgaU5VMGrmdBG4lWKtMggAMSZnOyzlDnVQk+KvpqKqSiitGKSXnUREZ8YZxzhjGUKyB4E7BFvXgngv6h/YmIkQ1MIxQzYigZkT293X3RE5tbWpt39LQ3AgryNaIo1mRoeG+/p5If8fA3o49kd7BLigGdSuC//v7uoYHAbh3ODKyq2+4OxFBHYvg/849HV27I6d1xLEftEdObWhoHu5JNDQ2RaYz0JgpIxU50Llnd2MLchD6Cywl5zXntFdPoWQB5v9llaFIqdh7IT+WgP9DDbF4S0PoSaA+L3ycnFdN4uQjmSyrLqdr4HAtBiswqMNgJQQ14VUkPAYBqy2SDQ0NWEB17cr0ZV+//vWP7aoJH4uT/SroHuJx2EVG5GlUZWmnpqWNPzwR7a7b+fVdqTK65hbXwMPGuRf5D4hCWXgtEj4eAowJghsP1R7K1F1Myrouz4jJFG6mVJaVID67oIuIQSRSC59UZq10QisOz3LAz1brQopkGVxHGayse4EvNiqIdMhWQyhpqJcp1clJq1gYbhrmxzQVUHX4ZFyc2k2I6j6PJQ3iVBzIZgTSbqdA8Lf4tnOnsxmJ8rV9I9R3o6TkUloaWmb7xoI5Fm1s3XjujtqabR3YzINjCRA9mb5Cb4HKk/XAkKzqkAJLhNz44Ogl0E4SlJoztk4b6e0b6Wbd1NRUbKoppunj9WChGusP9O9JpCaUrLzRyazOnTmq5iwbvxF4kqRtpbFD8kJukiwBoa3mTF7ZvhH42zqqaRlFzm3cAaMOmHaSxc4PJe7YJRs9E2NgH7bVY5RQrZ8H2cPJ4picMYrw2C+rGTU3rIxCWy8CTosI06usiQktn4dyE9BJhqzhVzdi+2EGIo+quItsA5PxZE8OuUovgqoVa4SF1q1pCdeteQnXrWUJ1611Cdet7ejXzSD5Nkr1bH04vDpzgEXQDIdg0WFSAjO2hKnllCVn0Xl1WyoWnVe3pWLReXVbKhadV7elYtF5dVsEFv1wmEmEdmrmkjOR/notFfPor9dSMY3+ei0Vs+iv11Ixif56LSFzOKL1LU176KnYUjKInootJYvoqdhSMomeii0lm+ip2GIyirNs4/aYKtBQhhVDK+gpxVhkexRDsq7kzMTBwiKQ4iEMLTs1bTxDTgcepsFlW/28Xo/sCK06lONlp+Eby2RGzR0k52lH5PGVSYVw36kY5uDYmKKvSY7aj/Q1OTkzKyan1LQ5UZWcoCdh00renFidTBV0bNieaSWbN0fkaTwIm8dDn1rBYIAn2RmHYAkGEh12T+Ma6xwETmJdEqQ947zR6j6WxGEVpysucLkLdHJMqFbzKfoklLPSKgbfNOoz1sFVAiBMpWYsKni/Qc5gqlUG8FHQc0ZHKgV1VdLHsMD9qjmh5tayIOYU8oks3GYFWjPdOWOhDOG/DLkd0ZFO69BuFslu1cjLZmoCj6P1y9MWX/g2dqCQHVX0cPLSgpwzVXNmZdKAYke0PVqKnAczagkA6eBZ4LVODIpXMQNWzDBPShoKvg5V0kS8g5OgdWkFa2gYyyFN1lMTnRpkDCfTupbHQiSnjS017yiYGsL7cibIFgzSBrfBi+SoxhPBqm7OrEjq0AJQN8scrMJT2vI4OZ5hQYSyMjxeS47VksO25FcmWGdsIQGShdDZc+t/NxDJGfRV8/Od/yif2DzK5D9zmM45Bjr/EXvh/sKvQXW4uro6FBKWlfBWv5t27H9prjC/qykosO65Mfodm2YjuxBrFLfPNJHzX2VCQ+jHzz333HyNjpgkp5rsU1Vi/Tzw2YNV9i/0ODAx30L4p6uEH0FRWCe7bOGHEHmcAoQf+FO/z6SGz8BgEwabMTizLPgjcvMF4tn4ZI3VHZkML4srcvuhEk94kQAil58begyYmEcPKnK2LIZBPVYsjDlqKo85+j2t+JmiKjweVB1uZORbiafflsQ9gFmPUrUvkHvnTFUc/qynilWJ5wKX1pH/WYXXeajVcKTYRLWOSBHPPL4Qj7zPKqsSjssis45Emtm+SA6EvmAP1M8ql9h8WHak08JIp+okMoDAfzzSWFmC7baPtpLBoBWC2rqqdZ5CSjhoGzyISoprs4o72S4ObwdUllBJ9sAmKWgLBu0YbMUBRMAxkRxLtq00li5s8kOJvuCo+cKf0M6qN6UcFQ6w7ujPNtQbam8EnF6QecZ2xgoJMZ5khXoetM6OVFfF7ZbBMaGybYGjC1GAczE4D4MODDox6MKgG4MeDHox2InBLgz6MHgRBrsx2INBPwYDGAxiMITB+RgMY5DAYASDvRjsw2A/BgcwuACDCzG4CIMXY3AxSq7JriXRtvMOdRggNwSaPWWeUZq5JJgtHswSjra7BoXcKkALMA+rcSJkx9vLNfYN9b6057J6mKgsbiFZTwhbQWbog2P9imxAJ1iTNJRcmmz9QC3UMdXawFmVpHet+3KTwBxYg9VJrWDi9p+WOtipTMiTqqavtG+cz/TLZHunRk6nRzSi4MvIo3Xz4Rjy7Ow8YfKxo1quYGBFcJMpI89Y/Br0jPdy5+lANuO/PV6TVG2uqpL9au58E//K0/C3MpnN545xr57bG1Ir3UvqRK2NuuTUhJZRDBmvkwM/Ra6JW9s+0JbExEFYWVYhkLvW1dWCUIJtHgwIjTT0stA87/DWNpagFrZUELFAbu9WH+H734fJ1KNeHKU5en6Wm7lKSs1iIYd32U+WanuaBXLRxlo9JtG+ORefxJeUlXxHT0btwrWwAX/fCQvIBelokaUk4Y1cQ8IVqv1oLWmhq5TVrJx5T1/+5sd2hRVqB2vEMU9VrLwTmEJuJIVwkrKgKQ7e+6mCsdhU+lj75+5Ji8QvBtg7uWAS22OZpaScxX33Xl1RlqObC13OaxnoZ1XJvKbmTGNVcgyS7H0WHPGrklouAXZiRVLT1XEVBg1SEhgrAI7M5JUQeXK2lZ0RGnePy8hOWLVQLYRKmHolaIk9uUK2vLSxAFHsmZpg76KQK1vY/Zta26EnNDRU0QdnomDtPDjpobdCY8yTQb6S1AghnC/Oc565no5koWSOjFHjBTWtpAfkSRwWSnC0wrhl8b9RysKki8lZSwC7qS+VmuQ+yw3I8B6Y0oHRhx5flZTBcE2W7piFO2jhOw9tqi+3066JOk5GomXJUfQphNNBIwxc43uGvnQVfQonUxNqJg2P9J1WTdKAqhNhrgbImAz2ZagzYcPWUk5gfgmgnuk8vrwBKsuT5oSSI1k6Z5YnR+UUGJ+05cCGO9AtIy5fdFBuGPTcVx34WInORCorcBDEYRBUWZjn4CUc5e2m5WzZC1gBvZCv71VXl76x1KlrU4alSgQH635o2Nbghmsc8SCaiy7QQF02xAwbybKRHBvRINJLI1JU6gbza8oZiUIiUq+aybqxfiWtknvUelY2XTBeAVcMMc8WfCkb0Q8jFYMpWBB8W741JBTCBTS91cRahifd0aIsdDVY4YULfJZRW7gKSr6aut0SL2OYJMOwlQeX+Gi7cfEmvgwDWqmXM8+vYJ5fyTxfDs/dXtFRmYBIu+Qc7hQOqAfxzxAYOnk6IiXU8awsvUzahT7fNhkUS7wCSmpaQEniqwDxRsFbmpSSdXSIJcnSFPoFsF44Q6fCC/NpSiRlZc0QIjEJD1tYhChdSs+JWWSdqFUPOzYiZ3UnryTn0lIWmnCDeCVw95Bg+beLSNZfSjlic2DgoQEJAg3m8REpP6GZ2jjMUCZmPBErX8TPP4UytYGI5fGAxmwEI6PbEPcR5lASHiLRfJmZ8sRX41LEkmzUqmFRJX8tUXLs+uGrMHhdmbsNJUxRdXQVtMAq6BtcxWL0cyvVz2tBtcVr4WE5TMSyit313hiAvCkAuS4AeTNAzmUhoGSDmZlsvmC4+kW1LiFnjUJuPKiy1+OMZ36FiG8BnKv92qrhSRdDgsFbwoMvEkxmMzA9kTxlj2m6pEzjqyVyksLSDFtvE1puxqVJ+wmlCXA5JxtaTk1FpE5tStEZ/bwB+HlxCeo5t1p6FDBPOCfK81ZH+HPpztuI7pBNpJsweDud1xLdURhNsfKnWN15p7+JXQ2q2mZbWtxxK2lrNbBxRzbEbsHg3RjcisFtGLwHg9sxuAMJbLdJlR0SqRDuvS108w63B5m1UQjd48nH+rOiU7szg8C8D0I8mPSlN5WYc30wn7MFg/PQ2dJxhXFKMH2MDLnWATROctraSCKy2sDBRqPak0vvs2b5m4M5CoYCo7p/wn4yh9F8HmbTI9pePSPNUo1ESssr64IZrINNuLlwajEmiKMaXUH/MxtnE5SmZUbUfDyYxbRSaGUHA8uQSABlW4+zANhxMJlE17og5150LkOWsmTyT7ogrC1hVbsApSSKgVpduneNDlYnyI4WrEgWQJq0BZmRCoK9QiY24S5Y2JCN5TvReNwFwUo0G2BBdaJvuZQS2gcGZIEs8+dk4t0QVLy04eWCZSRCI0BhobUKkrBcQFpL/BrhPBzhaJXFe+BhVb+q67C6gnKpiXyfRwgfxCciBFKDQeBtn21d76OMU1EK/ZAwQhOpORdqhA6W5EfRjLXFWrLZuz1k7kdD3aulYAQDYuPmhLCLpfRxSsk+e9fLpdTJUvokUhqrb4q1vN9D6SFs1X55mrhZ6sgrOq6hhA6W2mcotUpK7VwutS6W2iPwcByUulUiJGFloObwOR7/gIf65yFYY1OVNgNGTDoDM8fOFLayPHyR8hAiTSgIW4I8QHN2szx8xWpO2w2VZLmhusdD/1HMQ+cX/bj3ZTVrE0v6a5S0SKvfyK1+D0v6G1zS93pIPwbBsfZKCU2aqY6qOKERIiz1b1LqVbTiZ3Er3stS/w48LGMcc33QQ/d7ENR6vHgJp7MEf0AJVlOCp3IJ7mQJPl5GXDA1NmQ/5CH1E2xdjwsvx3+XcApL82eUZpjSXMeluYul+b+IMlDfcZ+H4i8hOMXW5q6MZijRvXmpXx7POa9vhLUs5V9TyjWU8nFcyn0s5d/Cw2qO17IPezj5PQR1NI+dQ1jJkv4jJb2Mkl7BJf0ilvSTtNIf8ZD6K3bhbhWmmbo8npU6M2AODSHM0vo7pbWc0qri0trN0CKwjwId8lLxKSzkadQcYvt6FRk7rCFUUCKILz5b5jHcgsAQcU3uHrZCuI1WPqB9zEOnAoCriQNvKWHK2CmsF5HC088yVUKP1Y61BWL/fJZXpX6WWrVF7X4PtRoB390WTG0MFVT4C0tjucDYWSj1z1waAyyNOovGAx4aqwT0P6areS0t4T6ZrAu/Y+msFhgLCyX/H5fOoL95Pg5EyOveNZBRXAtB9dDEjAGKnhF++SzTMicI3pb532d5LTPkL/9BT/knYyV61QzMhMCg6YqcFn7KVmK9r0F+zK3E+aywNiLL8VjLdDzWsPETHmqnCUSp6QlGQ9rcPb3nTOH7LMEzfK3zXS7BYZbgmThba2xuGf+kh9jZGFjvY4X/YUlEfQ3z31wSCZYEuqGrGsxJ+DrgSpAFnjoINyKBOATLbAHiMPNVSoq8O2pm2ghp/SfbRmX2MDPCkmqDtHsEGFwMia4zpDHaM6Ui/h6dXY+IlGH9PKYtP48RWPQW8+8YIYvrgF9HLHEOf44RZykdk6pBlPgiLrwFRdKOrZzwvcEQvsA2wTmsXEAGnwvKBZpgLyuXHZC23PK8aP3CHqLnYYn9QwPCp1k6nYz6Ip1PcensY+n0IMqAMlXjKX4nGpQuLWfd5hAeYIn0MSqLRD7GJbKfJbIHRWTvwJ4h4f2glLHMQ3EAu5G9gy58iCU4xCgwEryXS/AASzAhOPtYdAq83ENtL/Yhor/vYyntF9wZGlK6k0vpApbShYKzlVXrIfFibMCO9H58DUW0SbiNJfVvgjsjQ1Lv5pK6kCUlC76djxUegimG4B55VMkI/8ESVAR3EoYE384leBFLcALSTrP2kcj8nr6XZrqitDmL76XrPIxcQlY8+IJOuIHlICO4szLk4HouBy8u81nw4yqsLeuwhoXjMeg17jUjCV+24PwT1fSN7Hih+8aLa7jjxcVsfQuQtq5RmoH1u8R+0UGaQueROXNmjYeVKVTZ/TRJeB1b1RnfOPJars3Fd+p4CFB8GcAfPudfTg7nvHrKvLcf0q2vihz1q6c7BjiMkuMW1jmdRXAHtZg4+3LkGuNgLnMUXXepOXPjjga+DHUl2bhoOYsvWs6aFi1nzYuWs5ZFy1nrouWsbdFytmXRcta+aDlrbFi8rC3eUaBx8Q4DjYt3HGhcvANB4+IdCRoX71DQuHjHgsbFOxg0Lt7RIL54R4P44h0N4kd7NGgsztriHQ3ii3c0iC/e0SC+eEeD+OIdDeKLdzSIL97RoGnxjgZNi3c02HK0WSs+GrQvXl1rbz3avPHE5ty+TO63b58uai4HBxZhA7vsDWi5RckhtSlHcRo35679fmV0Ue/YNx3tiebsHWO4L7GIG3dAG1YMRZ882sZl9t47WDAzitmloM/FRSzM/o4heqX4yLhLxWvR87/Ij1edVya13C782r2dtCqpKxqWhMeUjAktk15BrnkSGPJR50b3qFnVrEXHmE7qOifWMSmrGdk6lonfnt5rplY4iQRzdVLOZNCRguKSPyZp4BtMaGeHfl3SlPVxxTyfXsBeRdw2gLZiRuK4YRW5gIDeRA1TzuYBcmwyQz002CWriuG9SU2PkVSgx4Vq4ch/OLsafrW1h8n/GL6Y3uOr8cyR+2o262XPOudQU8ZG4feHm+8gV/h3ZO+59+pbH9sVxgMSQiVe2X/hO3c4VD8+QdYdPz6vEFw5CnjBk/jmeaVg3TUiULzqSb7verlAIfj/tXbeK9i8eGUO77qR+3N4dTaMV2bDeFU2nCMnTDDAO61hvMsaxjusYcM9PcIEbxDso3Xha+Gxpib8RpbX/LP+q3i5Z5kLVnhXlLmD9zq7DjewdXibXYe3snW4qcy9TxrG+6RhvEgafgURDJEDqTYGeHUzjDckw6/m1uHdbh1uE7z3Cm8XmHuFFwYqc4CtzBu8lXm7XZm7be2HX+iWstJcUnDva63dliAuKfxXaNZsI1kCN2vKqkP2jQ38oLXwbni4hfZSAriVAqpswG0UQHQ53t4Ui7cJ76GwSjvT7RQQsgF3UIBIASE8Vb3QKzyEjOOtYZl7H8k4adZbRBVC9UPPjx/HI/pJdjx0J74fmmx1Z0fMf1ko/AFHVfFYNIoZz8yJ9+IRx87OmOdsefiDTmY8o4uZ8XibeB/J3EULp8edwx92MjfSzHjuVvwo9jnryK0R/piTZwvNg+fuxAcgssJyF2+7kg9/3Mmq0azk4vcnIHLChRfG+Gfowp+0sSpxYFgSw+KhuuXkcu8MDA8xhrLqFfYT/kq5FRoccbDYOntcwdP+lq2gvT9E+zgmVl1uU1uw679P4znZirorSiEn4ECz0jG8ONaQMeFhikzy4NBDTO5nBQuTYN9wyIw+YjH61pIYfbeH0dtsRj/PMnq7zegX4AEdreH/qrs97bcARivRPLzQjeCsHaZ0b04O405f+SIzDgtoG4kx9UA/zIV+jAv9OBf6SR606iGPCpYwMnJ7PdHFLxFdFLDvHJ7pj30t/jHf1Oeb7NTnrMDUZ3PxqU8ddsrquToKEcxnbcFUL7Rvfh+F/QMMfogBudH/OD79GAVVLjwiuH4dwujXIYyuHMLovSGMDhvC6KMhjD4Wwug0IYyeCsJvnU1QvxG8/gN+KzD+A4551u8/YCULeadXUJ8vWVBfsAUlLVRQf0ahPInBXzD4KwZ/w+DvGPwDg39i8BQGT2PwDAbPYoDMh5HBMK7ywuUYVGBQiQFOMsMiBlUY4C3hcBiDGgyWYbAcglqx6oseCxeff2cmXguwCyy0D+GStIpYl770sa6rRydfOrAXwtug8O9G+J1Irseb9n25npSWzSp6SiEHsrvkTKqQIWQ2bqP7bG6R/gnthm19OUCHqE3En2Ptth5TBmLBab/1q6ioJtsLwpH203jkt1FsJ4d2N7J/DXfdRfYbiMrV1AiP2aNxbTmzlPymPRqvKGeWnd+nhgOvZYt1kFLn890kruQBV/GAx/CAqwHYYwOH0D+gdAa6WdEcx0+soxfL9YrlOCjgIuZYKOqchRRFnbiIx0EBb5vLX8yoDv3CkNScNCsp6ivG6+HHkEZnGK81rqeYIDOOCyTXfcwaYO9Lpbg3mq9bo4ikK+PQKXUnThw1EtIGqZQDh2oaTIzkCJTiL91Qs/mM4gOLa6E666kMo1RqUeqVIwoFRzsLM8FtihPKnW2Kk8q9Y/XJrOL+9Bn/WP3jZxgXNhs4ushsW/yAqj26mBFPhbzL7LyJPcPiaX7A6X7AGX7AJgCcywAYjbQcX3W3NDY02Ko9rIwqmaCCb4ZCYvMrRDwTcK4TRuSDClHhrAYqnFVAodAmUb9GkjKJN6xUc0LilQ4669N+qtwIZWjZPry6o7hZz3jmkmTTV5UN4lnA14efH2VmXXA5MH8kiAZdcYYmpUildNIKFiRHaobP4tnA+Wpbb0FOtu4GdTXq6mq9T1cbqa6SnamvBXT1UVZXW3zKxOjpD6meoh8Fsa0cvX0wnvTELQFIewCyNQA5JwDZFoBsD0B2AORzQhO5o+jx5gccQ9gxlHBBmMuxc9Tq7VHUlBxxfLzRMvLw2KXl6NrHSpY60nIet2JcJ1y9hUtU4h1Ey5hQyC7ZMJTMaEZOQxYNTJKmwhNVQVjDAZKuaoCAnkm1UcAY0CZlKZHSTFWOiefalSvetB1u03b5mraHNi25T/5goGkfYJu2zy9Gpm0fp22LjmzE3Th19PhFFPcEQf1B0ACCshbI2mISB4O5hoKg84OgYQBdUwGzUWmI7ZJF3DV26ji2yXbjQCuSpmLbZpemG0pWzrmp/TCTnZHdhh3G5buKrtTSqkJ0IZvXctZt8E4FpqRThBI6X0VQ1wTOSFEzOroYNemUTXgAVrtVjYK6dXUSCyQfTLR0a6Qw6mgeTiXJcy5NsltuRxIpXUEF3KVpMAXoV40UsCDnFA3SrG/qkbdr0m7VNCT6KUaYO4yoGRNqhjfeDTMtjSgZhZgxidyrt5yoXKhpWUf5iab+9PKbWF0dsV0W2pYAWYyICbeZolYLBJV1r6us+33KegFVVuIQ546Asr6HVdaLAxrBaOuPqba+Gz0HJiFY0WE5gbTtw0s4MJkDG+XAUhxYmgNTOLAxDmzchRXv4qortYM+qWWp1FKY+uZn/IvdN7FSuzRInxHbb+y5uMHOxX8rcN6aCH+mIn4TirgAwRq+/oiTs6RNzZI2PUsa7rt08dNg+kH8c/otOk5qg5OYy6Cg+PwLEl8KeNcKOxWYlko5Zcr2MJGz5zBk+lKk3MD8g05gimSHCY/lYjTi4ycwJX8ZMPXw8zOL8bh4JI5VmUer8gHAZVgJBhosg8nHOh19ud3yUSKNKEqjmAfJ8CvdjnFFuXcXCD2V2C/bhJcFOsZlbMe4qri2MR3kSar016DSX12O/sDUdHQY6uHVz38vnvT64klvKJ50TfGka4snvbF40puKJ13nSZpd+te70r/BJ/0bqfRJC0wEpD/GSv/motwwwv8LFT7WSnwHYrhjF8v/O4snvat40i3Fk3AU2c5NKuKgOGhnboUiIvMpQrwNMN4owJyTeJGjZgVMx7iuyMS977g5Qe2MLHFKjkn8rQMLn+4bZNSDymwulqnZdE3Me4CrW46AiTGdCvnjaC0irG3xJlkW5HZPY86uwu91VfgunwrfXe5uAQsjARUeZlX43qL6w6jwXw/bznf4Ey7bn/Kx/elyZvf7vADbO54puvst/I0yiP1a/DyuB6yZIu0JXwhAvhiAfCkA+XIA8pUA5D8DkEcDkK86kKKN+XVXKt/wSeUxKhUyuYwEpHIW25jf9tNmRPR3KqI3o4i+C8GJibySUkGTO/L5DHXIZ7P8vTnSvz9H+g/mSP/hHOk/miP98TnSfzxH+k+Y9CiTXrSBfuY20C98DfQEbSDik/L4QAOtYRvoN7PzxTTXP2hz4agm/haCde4KXjpD6kJzCIsvu0K/mzPH7+fM8Yc5c/xxzhx/mjPHn+fM8eScOf4COU5xc8B8Kx218xRtwr+5TfgPXxM+RZuQrB0qA01Yzjbhc3NxxzTiP2kjrsNGFMAc16GlpXsxuIQWy3nACh6wkgcM8YAiD1jFA1bzgGEbGHWBQWkuq3CkWVvhlWZdhSVN4rX0D0/7l8O/e5qR5moOB4wAn6ICxLWmiA60VlieYe1dCXENB7aWAzueAzuBAzuRAzuJA1vHgZ3swKI2LCi39a7cNvjkdiqVm7V5HpDb91i5bQrSZ8T2NBUbOiwVz8SsZEuxo9fuQGdxYGdzYBEOLMrCrG9RiDEOrJ4DawDYHSFnszJnjMqjM7PN4SLOInJEO6jmAIqfyZKIf1D/RMozvcqSUdB6Von/UoP6L1X0iLQ30R8hiZJMHR7j1vWYaRUckcaobzgLv0D8N5KlH0Mhr+kAVs2oNall+VDs/TDJLIz6vzRwfkGF9TfIhLiFJO+upH5A0+nyMsquS/dEDes1AFN6Ty91H5lTDx7UnH31lgbcD7Z23a1ng3zwo8kHlxpjzfZbBhrfEozb+a3cVkkshbYG+0kZk8gLEyuWTjU1NtgIpBHslKYGBr3RilhUSYo01uwmSvG4XUYbptGCLEicKSfe4KkpE2ljCDRuIRFLXlK82YcijcWxyiwEAGKjrcDRjt5opsio0uT25xZff26j/Zn4M/5SoD9/ge3P5wQ7GtOfn6H9Gf31itudrP1On9zBgZ3LgZ3HgXWwMNpPOzmwLg6sG2BfrPB1333qJKyQdOZVQlfUY6qc9eaoPKPloDOM0o1pk7iylciX7yiJiNQrQYfvsnqrlSLbxdgdYOAwdIB/KbzY4yh8f3GF3+kqfJ9P4XdThSdetD8SUPj7WIUfDGoio/DPUoVHL9ji+RX46g5fplKNHfYDEn7AiB+w1w/YB4C1DEA6z31hIO6HxM8LZHcT/eyy73N9m6HW+wUY0EAn8Zl5qQNmPVGA0YXsG2yQ9ivkG08zdCeAsefMniLZsXBGLvyGjg7jDd31MAqpCUk2fBsfge/3WJ95wpLIlu8BqEs6OAL6dznZQYcdOxk+ibtbEEOwMLInIl5gy7joFPwiV3cu9ulOkuoO8Yp+S0B33snqTsrXmIziPEcV53KcMCqQbxWzOUqbfowPHueDJ/hgFSeuPrB4CQ94kAfM8IBZG0g3kBEYFKLmCvFSnxANKsRaTH3D0/51zL+zQpzi1osRJZaEokRvsuIM5K5xdxbEy3zxl/riL8MmcuOSiG6Wu9itCatncQ5gzPHq4xVQUHz+BYmvrMBXH+QMhwxdMgczPDU37nnx4RzQ4BfPHD/ylu/0PHvTUYOSdM+WJX1X4nuBIl4OTH3nCOxLsj161o+reT4xF/HYIrAF1ifj6PTXGsJttJyTYI0qZD/zCkcriujyla4uv8any1dRXcZbpcJ0wCBMsrr8eq/2MUosUCXOoRJfY2XTU1RJr/XF3+iLv8lSYjsuidf5MrzZF7/eF3+LL36DEy8ijxtdedzkk8fNVB74SlNIB+QxysrjXV66jDzKqTxMlMe7IdvxxXajxFtnTb1t1tT3QOoJxVIl8fZZke+YNfW9s6beOWvqXUxqYNsv2Bp3u63xAV9r3EtbA69nCEOB1hhgW+PDs/HEtE0FbZvjcezCz30ckzDBdsh62t2WFz9WBH5/EfgDReAfLwJ/sAj8E0XgnywC/xQLd99kBIX8aVfID/uE/AgV8ipM3RYQ8lZWyF/k88GIt5KK9xQU75crcCOWZq+n5zHIPhRxTC9+ZY70/5wj/dE50r86R/rX5kj/+hzp/zVH+jfY9Cg9+kI2/Uh6sJkec5vpm75m+jZtJjyCIWwONNMZbDN9f3a+mOYK0ebCA6niDwHtWPctlfvxAPFHRVMeL5ry46IpPyma8tOiKT8rmvLzoim/8KRE3Y9NBgX/hCv4X/kE/xsq+NWYemxA8Mewgv99MV4YkYtU5CeiyP8ICKs5LwbFPxVL+HOxhCeLJfylWMJfiyX8rVjC34sl/MOTMJslesqV9DM+ST9HJX0smRgHJP3sU4ykKyr5nDCCrqKCxgPUYgjyryKn2/D0uzpmWnyLfHAVH1zNB4f54Bo+eBkfvJwPriX1BDB5/UISiggWP6tBBbuq0ivY1ZWWYI/D1P97yi/YX7OCXcvlgxFrNRUrfltDPMFmz1oKu3yfWCzhpGIJ6yqx7wQTJPHkYiinFEtYXyxBKpawwZGzPIecT3XlfLpPzpuonNdg6ncCcv4WK+ezi3DCSDpMJY0XCcQo5D+OWUkydi5WPKm+eFJD8aTG4knx4klNxZOaiye12El0OT6blW5zRd/uE/05VPRrMfVzAdF/lhX9uUW5YYRfQ4W/HoXfgbrJOwAmdhZN6Sqa0l00padoSm/RlJ1FU3YVTemzU/yH6IIy3+3KvN8n80Eq8+Mx9YMBmd/DyjxRjBdG5MuoyF+FIt9bydm72ccD7ucBD/CAF/CAF/KAF/GAL+YBL64sZTsp6UpR9kkxRaV4Aqb+x1P+7aS3s1Ic53DACHA5FSCedRTVSu6W2iV88EE+OMMHZ/ngHB+s8cF5PvhSG8wIlLfNabgiLfhEOkVFirf0hdcFRPpaVqQv5XLB3Pm1rx7O7l1hBTnJWHYITgvwi2W1FQIae9cdAV4cs3wHVTLuCHC2bvkOqixz75FFPZj1NuarWMxGG/NKFrPDg9llY76GxeyxMV9bSSuJ0L0ezP025utYzAtszKsrrY0mstmkejAP2pivZzGzNiZ7uLzOKKVBhFd6yr/CLv9atvwreeUL13swb7Axr2Mxb+RivteDeZeN+RYW824u5ic8mJ+yMW9kMT/Nxfy6B/MbNubb2VZ6jIv5Mw/mL2zMd7A0n+Bi/s2D+Q8b8xYW8ykuJh5ocTHxTAvBvI3FxJMtQcz1HswNNuYdLOapXMwmD2aLjXkXi9nGxdzpweyzMd/PYu7mYl7kwbzYxryXxUxyMTUP5qU25n2Vln6TA4oGF/NKD+ZrbMyPsjSv4mLe6MG8ycZ8gMW8mYt5twfzAzbmJ1jMe7mYn/ZgPmxjPsRiPsLFfMyD+U0b82EW89tczCc8mL+yMT/HYv6Gi/mUB/MZG/OLLOZzXExclrmYuDIjmF9hMXF9FsQ81YN5uo35VRZzExezzYPZbmP+F4t5Dhdztwez38Z8jMUc5GImPZiyjfktFjPFxTQ8mAUb87ss5hQXE4fMw+sr5Be+mcQTlcxp6eMDM4k1LMTnKwRH7NJ8hVxOSi87hLnDb8ncoe5VJZO88pBJ/s4i+ZqSSb72kEn+Hmd8f8Dgjxj8CXXidSXTv9qmv3Kh9P+MVJ/E4C8Y/BWDv2Hwdwz+gcE/MXgKg6cxeAaDZzF4DgNkqra87vWl8lx3rZ3TmdZwMEjO60rO+ZaSc95YMp/2bKPa7it2at07Si7jlpJz3lZyDe4oOeddJVN/f8k57y05533+nJW+HPav7qMll/lAyTk/UXLOh+ycnqm2D4vkfLjkMj9Xcht9seQyv1JymV8tucz/KjnnYyVT/1bJOb9bak7hF/YAKoSYe7pPcAfQ39IBtBurUBHiOL2p5AFDPKDIA1bxgNU8YBiAVwk+qOsuxAO3L+pbZ5d5XmrswyLUVYPl+iPKnAzBo2Sbih4kE2uAm6etIyOb7LMizgmMPH7k2gBMyUi3tzcEfNA4n5x3EX3OdewThF2alsmr9qGWrplRq9yI1Nk3OHCh1JeVx8nxGV2zbuxDxXBb3qo/XvlXCHa3auSUGak3g5EpGLZ0wNDGAqdVqKSkhDmTKZDD1XZFxsnpyunGBklF+NlGTFzGtJJ99ZydMllBbYg+lYXrQjh36sJH4uRtVcjS0BRq2u/+6d8e/L9/Mrswx3FUgtnY+h3V0x2Ydy1PfY4Pua5OWJcw4gm83CfygCfxgOt4wJN5wFN4wPUlCXGDK8RTiRDRmY/lJO90VojfCwjxO6wQz5xdiL+nQtyFec/GvPZRaJvbCA8Y5QFjPGA9D9jAAzbygHEANhOXFra+QjviSSfn0KSkK2MZZdo5lUk+KT4dEZvs4vAMeVEvCS22lIVwG5EyuqKxvBC2s1L+QkDKn2OlvJ3DOyPlP1ApD2Hec528/W49z+MBO3jATh6wiwfs5gF7eMBeAN4OdlbOOWZ0l5YZh8huLS0fxD+WL5QBZRoNmB64ODKMjWD5KSV+aQqgbZbTEbvdCM0uthHHoFkjEhSgz0BLTioZKWshEpdFowqYuZziuj2yGlbc6bRr/yzt2ue2625fu/az7XpfoF0/yLbr+RxpMe36R9qu6FxUTEDeY0hej0c1caQIfG8R+L4i8P1F4AeKwC8oAr8Q4Mk9WlaLSNSvR0JGB2e6M0Rh00wraXqqkBRhN4J9PceNM12T5CStKl4ERI4lrUSI0xfVQD7YVBe7TZX0NZXMNtU7A031H2xTKfzqMq31J9paBzD7OGRf6Wa3mBMnuFCVC72ECz3IhWa40CwXmuNCNRsaZaBBYV7qCtPwCbPACvPfA8J8HSvMGR4PjCj/TEX5EGZ+KdJsbW6R0M3RjPgyb/Tl3ugrkCuMktgr3cTB7gvEy73RK7zRV0H02+UeB0zxXmlXU3cEg76+iORxFGUbGHvCczaNTzSlo6rqn35J+C0UF2jFPOU5ag7zQ3JVzfpLbq9NyeSgsK5J9MQ+PI7B7Mzyju45QLwrahA3mhFytNmlSCJYggUmV9QsEwomGQa4Li2j6QyxCXV8Aoxn2l9pYrHxNk1WGZdhJghm1YB+jPPEruFoY7wJ6kbdSe3q6pbiW8jbrYh4JW2YKMo6qFivcRXrKqJY6NfLctJ7dYjxmjIZUCyTVaxrPNrAOkqhGvVpzPVGW0msw8Ce2HWe2Js9seudWM9AQnyLJ3aDJ/ZWiE14/KzRgWyXJQ77GpCn6XySDtzeYG52EK2kF8oi4o22bJF2ULY3ubK92Sfbd4QYnyijAdm+hJXtu1lZsG5QqGg/g5luc/ofaLj4Hk/sdk/sDk/svU6so2u3eKcndpcn9j6I3VLBk623cw137Icho9f+O8z6VrOA9rS1Y2gQVkW6bk0wIlIfzgTwUlse+sskZtBTE6oJyeQ6aV9Hh0trStMPTqCfNlD/3n1RtBIMmX1SgnZG+8mZoowVMhlpTIcY9fdn9Uvsiinsiq4dwHKb2iPQ55q69nfbfwmprt4oXi8jf/oT5AFzkj8YBwOGC1E9I12oqIbhnPdvaWiQ6PVT2+rsU1OwCvX4n7Pr6FFSmMNltPGC4oWScZqFiHc7/R0aLaiTH3B18l6fTn4oxDg5GQjo5B5WJz/KKhHr14Tq5MOY6X6cZWEmxgmd+AAP+HEe8EEcrAD42VtY6CecrF1dPYnE4HBfT0L8JA/4KR7wIQC+r8JxAugVu2ZdguwZwaVIXlHSGRWd68dhnT3j3pGkdlZKWV79XMC4ruYBEQx83r4LbePYjgT995jHMrIxQf+4ucdUXZmC/6Akoxk7lxOxRifi84+SSSFBii0dVE30eZtRZAMK0PR0xB0ZSO68VkhN0GdjgnwbICuPy5epOcXSJ/ymF7Jhez0cI58YiUiTqjI1RlwPQifRNYq7S7IHvlHL8yE1lXL6kgJeacsovoQ8RbU5NpiLOUR0U7pqmDQifpq2YpRpxaBaP+yq9SM+tf58iHmtszWg1ltYtf4yRw9ZpzhUu7+Bef+TdIEDrdaM51FP7Kue2Nc8sa87MZz8/Jcn9g1P7L8hdryloYqlosQE4eVe6OiP0bxFBvZvuhL5tk8i3w0xDnDOCEjkNFYiP2R5Z53fUFE8gpketzNZh8E9sZ94Yj/1xH7mxHDs/rkn9gtP7H8htpvbay9UcpfRS2MZOZeT7XtXXb19dMjv6u2hT7SzwKAhPmFLjz90/8qV3m980vttiPFOc0xAeitZ6f2RrS7ri4ZK73OY6c+OiHFAftIT+4sn9ldP7G9ODIfnv3ti//DE/gmx9R7pZQJ+aMWnHH3iDhzPuBJ5zicRQWScvTz7D79Env4HI5GQyFSBdexCJfJZzFQFmVZgpn2OxRGrObAwB1bDgS1zYH09+3v7Brp7hsXlHFgtB7YCYPVczRsCM4XrYssgonG0I2IdLSjqFhSU6CrRkehq0SvR40TG4cuvAxL9JSvRE4IVZv29ULl+HrOeJKJ5g6zsgLqOBzyZBzxFxAH5QKt3QF7vZGWGWYkH3MADbgTgHZ5pJQ5tU2D+FfK1Kgk/oxnxR52x2hnaAiOrZ5gMjNlkSLVyZlVYEuSxMSM0v5HP4CBK/pC72bQ4pA8r5jQ77JowONYb5Ei3M37rSlYzcfDNmbqW8Q67lsMFqxbcITilTWIZEM+pYzMuSYLICGQml6LDuz0+p7QCcIzvEDIyzlumQBkwmtLl3EHkyqq9OzsYLZgmWk7vbIHsAxXy+KJijHidAEBeVVJgNU+lLTj7YHy6q9ibfIp9psh45PlWQLH/h1XsKEcHWZc8VLO/gHnrLbPSRo+7e2KNnljcE2tyYjjKNHtiLZ5YK8ROtVc8I/LBQhZFY40rif4uByS2UbQiI0u7K5xzfMLZLjLuTT4bEM5nWOF0sNVgHZtQqbwfM3VZEmzz9OJuHrCHB+y1+nubt7/v5GXdxQP28YAvAmCTLUecVFjdyJgoYA+lyq0V8NkDE3fT0qJMaUHx9rviHfSJ93yRcaZxT0C872fFu5fDOutNg0r5vzHvfsh7nL2m9bomP1A86YLiSRcWT7pItNddOEF8sSd2sSf2b55YUpx1F0h2pZbySU0RGTcSbw9I7W2s1NSijLMOJajs0K+peFCkE2538DLEDA+Y5QFzPKDGA+Z5wEt5QJ0HNCgwygCDYiy4YpzyiXFGZHZpX8sIzcr/alaML+dwwLqRCFkCRFey4ittRWW5vZwHvIIHfBUPeCUP+Goe8DU84Gt5wKvs3jurAK92Bfh6nwCvYQWoBwSYZwV4HYcD5rSefVhi9kPl+LbbPaqIL7zJSYvrReaoIr72Dp602ODBPNXGfCuLeToXs8WD2WZj3sRitnMx+zyYu23M/2Ax+7mYF3swkzbmu1hMmYt5qQfTsDFvZTELXMzXeDCvsjFvZzGv5mLe5MG82ca8k8V8BxfzAx7Me23Mu1nMD3ExH/ZgPmJj3sNifp6L+U0P5rdtzA+xmN/lYv7Kg/kbG/MjLOZvuZjPeDCfszHvZzFxxRbExAWJi4lrEoL5IIt5HBfzdA/mJhvzUyzmmVzMdg/mOTbmZ1jM7VzMfg/moI35CIt5PhdT9mCmbMwvsJgKF7PgwZyyMb/MYs5wMa/2YL7exnyUxbyGh1mHBqe0s2hvLTnnTSXn/I+Sc76r5Jy3lpzz9pJz3llyzrvtnI6l52CQnPeUXOaHSs75kZJz3l9yzgdLrtGnSi7zMyXnfKTknF8oOeeXS875qJ3TM3b7sPC37JQVHbgL3ZFOj2hdsm6Op9Pl5XXpNeVjdccJkW1GXs5JqYxsGNs3GnJGIV9t37gjAY9bpdOaWttjLQ3b6jHXjnR5Rd2a8lOq9qmGOppRJqwiqk5rbmqINTSkyytpqRWb6vfLOiy+zZl+LS1ncDYSk4389Ll569OZfentrW3xltYQCZcBa+iZMQXMWSTWh6yyw1GSASbv+3uGo23Rlv7+aG9TtCXa25fY1XNBD1lqRvubo02k+LL0hGnmt9bXy5c1tjZtaWuOTWqxrKGkculYTjHrG5ubUu1jLY0tqbGx5vSWVnm0dayhvbV9TJZTTW3t8Xo8i6gY9fQDn4ZFvKVhuqUh2rqlvWVLS+yS/PjphBupLQbcSL1NsRaJckPWy9Lm/ub6pjOleXJeWqHzaq35cWDpRGW5AGENiL6qqbWlPd4QTfTt7O+IDiaijQ2khHh0S7TnQLR7Z7S/o2t4MNrVMTA4EO0feX4agLBgNcCWLS2tzQ3YAGcSnqTBhER4knrjsS1SzwGpe6dEeJIIT1L/iLSgOpRefBqkVbemcn1F/GtvqySd7zk6E8cfXlQuU47IB6pDSDNa8pdr8bLHWchhkvbHATmr1CTzIPsR1cwoq5LGhKab3YqR0tU8vqNemcxouXEGICaJN7q6JB5xG5PJm2q9r3t5UldMWc0QtVyeTBUMU8uOKNNmIxuJs5GmcPKgMjOl6WmjKjmlqOMTJv3SdjWwp6bU3PiyZMq6pqIqBn4fF5514vcqoZi4M2is5QEPZDMrk4r11a/0IOHaWO0HQKbjk9Cb1PGckna+6LtT1wp5Y1lStgFGTbIXj5B1aYa5PIkHViY0/Jpu2v+R3dVsfFiREXZCEo9c48kZ9TLC4fkFxSDU1yT7FT01AYJUDXxhSNunOtmJwu3rrk6a8nSXllZqk1gg2ElCJJTEjc2apGp0KxkFCC0DsU+qBvlWcE1yEj+jN7NXz1Qk1bSYzGnA/3Ksi5yayOK32tZ41aJTNpSzIesGHtgo5NE9tTGApWyaLUeHS+AEXj7Cx3peCsOagL+KMgwrKysrKoVKAX3YV2BAOpZQho8QHOHvB4c2z9299hPtxW5fwifh8UvQ2BG78JN65CAIIu4ouRMPWX0jWMSDAhWDCzZiO5WcAgixPaphoqTmdzMsIh0RM3bxxWfNzVfCBMY6ZfTjX7alZGn1eLs9Ir+iBDE9jwr1yOFpJv/HyPfJmYJyxNorVF3n+4j2kf/O9qOHVZBDfFN9xCQqVNdWC9XV9x2eSqHVPmKsP3B4eHbHkSPGebUg/EvgR1Lg5BwJjuOlT7bRi11ZY8nZBxOYY1cX3tibDxVcSuvbCpkd2zLqDsarpOWCz/UMXLutHjJgpl0zML3Dy2321zekzbsS/We6GciVO6kfhKRGM/IMPHdpMk5RaZZ6oCbi8r3WYrPeYqdGbGtvj7W3i7jM/9XyI8sT/O8lb9elbhUWB/CwVWqNZ7OSm247cU847tuBqdRETsto4zNMtoQ0VshZFzPoF/jQOzbMniHqvOokpw4NqZDHjYBm9ESdN6QMXt2D3LDkndIKGeoNe0rFN/iKlNcMsg3BsDQyoQIWvut3vsZiH3I2ffIiRxAoP3IapswpJS0BDjkMDRFJxXW2ZihR4Il1kp1WrMUCMppVFNP9jnsav51MDh7kSGWgekWOzQNRRkA5RbJ9+u7RpvCiY55+EWpzYk/3mZYfcMhETsjrypgOyy38SBS+35mWIItVGaggmUIj0Uk8yq1M4/eQAYTnG+hhY2QKj/sWdEkeVXRrxUQowBIE9IRE59Qjm4QhXYqfoKGH90kbkrLkVKoARSvkEztR5wiJbEhTwBH+JaeRTXRJR+9IjdHLVXmiTOYMw0SHNJaxlNM9jWIdvQS5T+IXbaY0YB/bUqFf+5DGyQoOGxGbR6OqmpfRNzubGZRIJXYzoEXkGGvG0KQxRUZFMqzPIZxxalP7OVgPGXWRXkElKkHcu6NaA9EJWceDqxqog7/0PlNSacmOBgNLiGg6hIkTeevzC3hYHJU35XzVCu+YwdQKxYCrcuwijnKj9C0F54rS6vZZptunqGSzak7NQic28ISQbn0/YnwC1BoTo5KMd6myeC9Y14BjcmnAf+GXoUN8WgLHhqkXiN5tlRpbXQ0FCTU20jZykfqRBXIfgTYyqiT2RUCOx5rOuLSgmedIm5saY/FUlrFl/fI0wbNODtkusYlqo7o3bm1087ZLeNZKkYA0LgeABHRHXR7PSin0sY/f44DuAJNp2sdGM9BTQK5EiwomlmdpKshIJl0cU6zvWARMpwHytMymm2StEbdKbfGWcWlzvCXWql12pkdw4+bEVqkp1mjXty22pc1TX9cit9h5QD7ePH25VKaQVuipT+fzOCb0Z3KsirSv9WAd3mKGInxTY72vEfFEzn8LnmEpQmKRwURkcGikr6tjTyQx0tHZsafvwp7hCO4pRk5t2NLSGG+Nt2xpaY5H7IE40t23s28EslulkHE3Am0T6TkQaY3390fI0IxhBD8pg0N0pKM30gOEenojp53f1dAA42GkeyeU39iws2ekeUtr5EB3T1e8ubGF0D0ApFoaCHsH+jsS8YbGlsg0fpGAHIydxhC0Fu2WFvrLc889N68Vvb03FUoWcqpZVhmKlIq9F/LbV15CDbF4S0PoSaA+L3zc+qiaxKVdMmk5ICFthF/nCKOnuTB5fYbv2WrCq0hI3rKttkg2NDRgAdW1Kzcd/Odb3v3YrpowcT/8mUq8lY4TjxF5mhxY3wkaYfzhiWh33c6v78ITB2RHU0Rfr5tL3UW9XBDKwuiiNIw+MzEmCG48VHso2yNiEt8rzIjJFB4KrCwrQYh2QReRSSeRXfgk+q4ktOLwbLn42WpdSJEsg+sog5V1L/ANnQoiHbKdG0qiPaxO0s9dgu41zI9pKqDq8Mn0FRdpQlT6eWwbIU7FgWxGIO12CgR/i287dzqbkShf2zdCfTfCEJvScC63fWPBHIs2tm48d0dtzbYObObBsQSInmwRQJ+BypM9lyFZ1SHFxEN7g6OXQDtJUGrO2DptpLdvpO9FpqamYlNNMU0fr483NDTWH+jfk0hNwLxxo5NZnTtzlFwdz+GLn9oaSdpWGjskL+QmyRIQ2oqjwfaNwN/WUU3LKHJu4w4Yr5Vt9SSLnR9K3LFLNnomxsBKbKvHKKFaPw+yh5PFMZg7FeGxX4YJT25YGYW2Xtyc9phkyTSsGDAVTynGIuC2SNN7uxZ+ewiPoyegSw9ZiwDdiO1XjQmYbHbLpmwDk/FkD85SlfQiqFqxhlho3ZqWcN2al3DdWpZw3VqXcN3ajn7dDJJvo1TP1ofDqzNjWQTNcAgWHaZQML9MmLBqXHIWnVe3pWLReXVbKhadV7elYtF5dVsqFp1Xt0Vg0Q+HmURop2YuORPpr9dSMY/+ei0V0+iv11Ixi/56LRWT6K/XEjKHI1rf0rSHnootJYPoqdhSsoieii0lk+ip2FKyiZ6KLQKjyF31D8l4dCBxsLAIBH4Ixnqnpo1nyAnsw2Sut9XP6/XIjtCqQznCexq+t0xm1NxBcmdhRB5fmSTnIpROxTAH8YDHmuSo/UjPFpB7CWJySk2bE1XJCXrbIK3kzYnVyVRBx4btmVayeXNEnsbLBnk8WK8VDAZ4kp1xCBY1INFh98aDsc5B4CTWJUHaM84bre5jSTyBvthd4HIX6OSYUK3mU/RJKGelVQy+adRnrMsBBECYSs1YVIY0w5QzmGqVAXwU9JyBXh3yppI+hgXuV80JNbeWBTE3PU5k4TYr0JrpzhkLZQj/ZXqyUM+OdFqHdrNI4jkm2UxN4JHffnna4gvfxg4UsqOKHk5eWpBzpmrOrEwaUOyItkezjm4YtQSAdPC+xVonBsWT4zRYMcM8KYn+wVJQGyLewUnQurRi+a1YDmmynpro1CBjOJnWtTwWIjltbKl5R8HUEN6XM0G2YLs2uA1eJEc13rpQdXNmRVJHvzRZxTIHq/AmjDxODrhYEKGsDK8wkKsL5EID+ZUJ1j0GSIBkIXT23PrfDUSIhx98Zfl85z/Kp+KPMvnPHKaz5IHOf8ReuL/wa1Adrq6uDoWEZSW81e+mHftfmivM7/ofCqx7box+x6bZyC7EGsXtk03kvFiZ0BD68XPPPTdfoyMmydkm+2yVWD8PfPZ4lf0LPQ5MzLcQ/hkr4UdQ1I+p6x8C+CFEHqcA4Qf+1O8zqeEzMNiEwWYMziwL/vyeYMixu7PxyRqrOzIZXhZX5PZDJZ7zIgGeujo39BgwMY8eVOSEWQyDeqxYGHPUVB5z9Hta8TNFVXg8qDrcyMi3Es/ALYm7VrMepWpfIPfOmao4/DmNKlYlng5cWteqZhVe56FWw5FiE9U6IkU88/hCvFY0q6xKODSLzDoSaWb7Ih4AfeFeWppVLiV8a9Fl2ZFOCyOdqpPIAFJGP95Ygu22j7aSwaAVgtq6qnWeQko4aBs8iEqKa7OKO9kuDm9fLeiDkuixLdyOwVYcQAQcE8nhZNtKk88sbPJDib7gqPnCn9DOqjelHBUOsO7oz7Yyy/EL+ZwCTi/IPGM7Y4WEGE+yQj0PWmdHqqvidsvgmFDZtsDRhSjAuRich0EHBp0YkA+o4Qf5wj0Y9GJAvuyC3+0K92HwIgx2Y7AHg34MBjAYxAC/PBU+H4NhDBIYjGCwF4N9GOzHAL+NE74AgwsxuAiDF2NwMQb/hkESg5dgIGMwiiJtsqtP1PC8Qx0fyAWCZk+ZZ5RmRwlmiwezhDPvrqUhlw7QNMzDnJwI2dF1RI3tHqQv7fEUEia6jHtL1hPCVpCp++BYvyIb0DvWJA0llyZ7QlAL51LOqiR1dNGXmwTmwEysTmoFE/cFtdTBTmVCnlQ1faXt7mOmXyb7PjUy+i4imr+MPFoXI44hz86WFCYfO6rlCgZWBHefMvKMxa9BD38vd54OZDN+1x01SdXmqirZr+bON/GvPA1/K5PZfO4Y1++HvVO10vUQQvTdqEtOTWgZxfHMU8RHh7UfBG1JbB+ElWUVAnF0UV0tCCUY7cGA0EhDLwvN04FCbWMJamFLBRELxHVC9RF2vnGYxgDUi6M0ec/P4hZBSalZLOTw7geQNdzbzhLIDRxrWZlG++ZcnhKVsnndjB5DDcOFsgF/b4LV5YL0tMg6k/BHbirh8tV+tNa7KgQ1l38tQa4uhS+htrBGPOipjpU3iynk0lIIZzALmv/gpaAqGKhNpY+1ge6GtUgcE4HNw+utaH8s05SUiavyXl1RlqOfIV3Oa+iQvCpJvp1qrEqOQZK9CYPTgaqklkPXXSuSmq6OqzBwkJLAYAFwZCavhMiTs+fsDN+4tVxGtsmqhWohVMK8LEFL7MkVsuWljQeIYk/jBHuLhdznQhPQ1oqX0xsaquiDM4uwtiWc9NB10BjzZJCvJDVCCCeT85yErqejWSiZI+PUeEFNK+kBeRKHhhI8XTF+sfyvm7IwI2Ny1hLAburMqia5z/LDNLwH5nuWx7yqpHWttGTPWNyBC1+IaFN9uZ12TdRxMhotS47irVacKxph4BpfQvSlq+hTOJmaUDNpeKQvvGqSBlSdCHM1QMZksDFDnQkbtpZyApNPAPVM53XrIvvypDmh5EiWzpnlSfz2FQz1lgcx7mC3jPjc0kG5YeBz34PgYyV6c6qswIEQh0JQZWGeA5hwlPeilrNlL2B59EK+21ddXfquU6euTRmWKhEcrPuhYVsDHC6AxDyaC/vbr5eyEZ2NGGzEhEiv+wVu3xeFIxL5Rq8T8/h0d8F7yOfNxAJb8CQbmTqMVKaZggMexGuo9/DL0PRWE2sZfqk7WpSFXg1WeOECn2XUFl4FJb/a9kD+SoZJxnU7rv/RduPKTrwCA1qpVzHPVzLPr2aeXwPP3V7RUZmASLusbyD5Pt9ruU54GfMtdMQSXwslNS2gJPEqQLxR8JaG/krJF+1k68uJ1tto6mPD9zFO6zt4MfyEa94iROnaH0m3YxZZJ2rVw46NyFndyUtu8GehCTeIrwPuHrI+5A65rL/Od/acT4xBVgkCLWf6vwPJujch+QIfE6VQpjbu99+tmI1gZOwPUaXdR/wMKLl378vMlCdejcsRS7JRq4ZFlfz1RMmx64ffgME1Ze4elaBTdXQVNM8q6HWuYjH6uZXq5/X4aYHr4WG561UHNPAtAcgNAchbA5AbA5C3BSA3BSBvD0BudiBzCeYdRDBk++SdGLyrjPng2UsYMVj5/40VzG1+uq54qrbZZgT3mkraVAxsWZGtoNsxuAOD92JwJwZ3YfA+DPCbB1XbbVJlh0QqhLtOC922wo0xZuIfQueb8rH+rOgy88wgMO+DEF+ifelNJeZcH8zn7DHgJGu2dJw+nxJMt77vah294iSnrZ0SIqsNHGy0GD25NHX6vDmYo2AoMGT5Z6MncxjN52GqOKLt1TPSLNVIpLS8si6YwTrSg6vnU4sx0YU2SVfQ4fTG2QSlaZkRNR8PZjGtFFrZwcAcOxJA2dbjzG53HEwm8fNuIOdeVcmkyTqNzGyJbYKFEyzZFqCURDFQq0v3K9HB6gTZsoHp9gJIk7Yg0y1BsJd/xCbcBbN2sqX6ATQe90Cwkjj9GXJdaIXOBwOyQJb5Ew7xgxBUvLTh5YJlJEKDQGGhtQqSsBzMWuvXGuE8CM+lVRY/DA/HdLuftqY28n0eKXwUn4gUSBVeBMydb5vX+ynnVJbCLkgYpInUngs1QgdL80G0Y40NLdns3R4yn0RL3auhQyvLXZDQxVJ6iFKyj511cCl1spTwq7Whsfp4bMv7PZTwI5crbfdKHdSjmrCNpfZ5Sq2SUtvKpdbFUvsSPBwHpW6VCEmY96o58hz/gIf6VyBYY1OVNgNGTDoDM8fOFFpYHh6lPJBhDqg2BXmA9uxmefg6dglrItbTe4+HLH7kdBWd4vWTj9aR1oyxFB+jFEVa6wi31j0sxW/BQ5x8gh4n/fW9hUwm2ks+x+yolf1Zc8LW5o6hRLTrzHs9vH0HgmPt9QHaOlO1fHwJm1j2vkfZq6ICOZ0rkF6WvR+iUsWbYk1SWhnXFcX4oIfw4xDUdpBvg8PsFu2rILEUf0IpVlOKp3Ap7mQp/ryMOCdqasx+yEPqf7HZbV9g5IPx6BKP+DsRTmBp/pLSDFOaa7k0d7E0f4OWGNa80/d5SP4WglNsPe8iXv/25qV+1p+YcAxL+veUdA0lvZJLuo8l/SfU+sZGyfItXt/YKvVQd2gf9vDyJAR1NJOdQ1jGEv8rJb6MEg9zib+IJf4PTGr/iIfQU9i5ux3/Z53oF80QKllKz1BKyymlci6l3QwlAvso0CFv2tCiirjXU0usYi/1pCc8+6xFBPHFCsFj04Wnn3WJuNZ4D1sdEVEuUIyPeQjhRzNWky8IOD4prab727NMnWoExhBDLf7yLK9O/Sy5Wkrufg+5OgFfaRZMjTiEE/7AElklMDYYiv0dl8gAS+RYSCsf0B7w0FiDshvR1byWlnCHSNaFX7F0jhcY6wslP8GlM+hvoI8DEfIW9CQksg6C6qGJGQN9JAo/Y9vmFF/b/ITbNkNsPTbgpAd93j3oIXIq1oT6xRuZ0BU5LfyArcnpvmb5Hrcm57OUNiPfTbHG6ZZYw8ZPeKidJRDdpqf7DGlz9/SeM4VvsgQjviZ6jEtwmCVYj1Vri7eOf9JDrBED65Wk8DWWRJOvdR7lkkiwJFqRxMDgQM+VIAh8HR9uw9K3QLDMlh6ORl+kdMh7k61MKyGhz7OtVGaPRiMsne2Q9l6BeLqkyxDXz+UsHmYj0gTfI2hEMop5lLW/0+rzJBtxvHJyHchGnP2NmFQNAiTv5negLM7Ftk34tu2Fz7CC72AFApV/KCgQEPxeViDdKGHGb2HYQ7MXC+wfGhA+zpLZxegskrmfS2YfS2Y3ogwoUzWe4vvRlHTZ3kKF+1gig4yeIpEPconsZ4kMo4TsXcczJOKo11jmoTgikPmPtWss3M0S3MdoLRK8i0vwAEvwAsHZu6ET4+UeahehVhO9fQ9L6WLBnbchpVu5lC5gKb1EcLZvaj0kRiFY3pHej69eyOgpvIMllRbcCRuSuplL6kKWFLqK82yIrPAQVBmCe+RRJSO8lSV4UHCnYEjwLVyCF7EEcwJ6WSQ7foOGRCb+9H3stNQ9TnuhNTnsN+s8zODx3RB5MSW8keVCF9xpGXJxDZeLF7NcFLAoco8vAnpNTiJPIokp1Jg+Yxg3KDPC61gqM77u9lqu/bmYpfIypBLHt7xRD5VXQFDVLc8Mg2oKV1Ai5ADV5Zj80oatXXGnv72SS+ffWDpXIp1WpBPz0HkN1ma/ohwkhC5jCV3lErL73DSXUJIl9O9ExYf39kx66LwBtcSSWecMuqcTDFZy1/r62qVcQi9hCV3nSO53FSyl69GAYY2UXJpUKsNW6ga3UnZvu4Q7lstlvrnCcRXWtn34bVjETQKuyZx7WBK+cMLVCJqtMXbmcLNv5pDmUhtla/ZOSFvXJs0osi6xnxWSpujHxNZ4WLmFNCFNEpKsWG/1TSYu5g68eK4AT0mKtwP8oXP+5QVyzmu8zNmFId36tNUiu5tLzppYB5UWwe3cYnLsy5ELnoO5zFF0E6bmzI07GjjcEd6SjUeZs8ainMUXLWdNi7Y1mxetzFoWLWeti5aztkWrZ1sWrczaF63MGhsWrdAaj/YoMIvUFu8w0Lh4x4HGoz0QzMLa0R4JZmHtaA8Fs7C2eMeCxqM9GMzC2tEeDWaZ3R7t0aC41OJHezSYRWpHezSYRWqLdzSIL97RIH60R4NZdG3xjgbxoz0azCK1xTsaxI/2aFCctabFOxo0He3RYJZV6NFmLdpUfB16tJWNy5tzyTO5377kerQtCa95XTYHB44yf/FZ2RvQcouSQ6qDrUdbCWcxK0dxJjfnxv1+ZXRRb9o3HcW55pzCG9CGFUPRJ4+6ZWmetesO9yUWYcdlLF/BzChml5Izj+bbtzlbu79jiN6pPjLOZPFe+Py9GeBd75VJLbdLzqWdpFVJXdGwJDyoZkxomfQKcs+VwJCPOje6R82qZi26DXVS1zmxjklZzdCv8HbLprLXTK1wEgnm6qScyaA3CcUlf0zSwLeY0M4O/bqkiV+1Ns+nN9BXEd8V0J0wI/FesYpcUkFfq4YpZ/MAOTaZoW4q7JJVxfBeJafnGyrQ7US1cOksPgqAQSz7MN+erYZfbe1h8s6Gb6X3+Go8c8Quw5LbEcvK6NkKEtSUsVH45ae/s2bLLY/teubW71z/ulsf2xW+A09GVKLPghe+h4tD9XIUZN3xcvRewZWjgDdcieeiOwXrPhqB4l1X8g3cuwQKwf+vt/O+j82Ldwbxsh+5QIh3h8N4ZziMd4XDeEc4jHeDwwVysgYDvMQbnnaPjjDBRwX7lE74fnisqQk/wPK681n/XcSeZ5lLeHhZlrmEeI1dh4fYOrzDrsOnBXrDEf+/s8y9UBu+AgO8SRvGK7RhvDsbxkuzYbzvGsa7q2G8Ihq+mluHr7h1eFTwXqz8msBcrGwMVKaercx13sq8y67M/9jaD78QXgBc8J2+tdsSxCeH/5rVmm0kS+D2VVl1yL7VI7a1ozcKAe8eIg/lDOy9FEYUuLWpOdbeItxJYZVMvrsoLMTA3kdhIgO7m8KqXFgID9wv9OIX8uA6sFjm3mIzTpr17lmFUP3Q8+P38ojZFGwmPJMpfhMacXVnR8x/xSz8LUd5sZ1RzHikUvwOnoDt7Ix5rh2Ev+tkPolmxpOP4vdJ5i5aOD0HH/6Bk7mNZsbj2OKP0JJYJ7GN8ONOnh00Dx6fE38CkRWWe33b9X74p07WSZoVj+6JP8firPOBRvgXTp630TzkvvwTEDnhwgtj/GN34V/aWJU4nCyJwfRQXZ1yuXeGk18z5rXqvfYT/kq5bxwcp7DYOns0wksoxLxUUPMRouYBE6vusqkt2J3ibwC7tqLufaWQE3B4WumYaxyhyEjyW4pM8uCARQz17wRqxzDloUNm9PcWo/aoRS5YVrLMfcXD3KM2c39kmfuazdyf4AH90uH/qv/xtNkCmKtEs/FCN46zdpLSHV85jDv948/MiC2gzSRG1gP9ARf6OBf6Uy70F1zoL3nQql97lLGEcZTb/4lWPkm0UsBedHimT7brhed8UydcDzhTp2ef8U+dnn6m6NSpDrtn9Vx9mwjmd7ZgqhfaS6sgDGPfDIcxIF4jluHTcghqy4XfC65jjDA6xgijL4wwur8Io8eLMDq5CKNfizC6sgij94rwzbMJam2510fFCeWMj4r/fcbvo+LnLOQ2r6D+WLKg/mQLSlqooNajACQMNmCwEYNTMTgNg9MxOAODTRhsxuBMDM7C4GwM8DZFGC87hPEmQrgegwYMGjGIY9CEQTMGLRi0YtCGwRZsDLHqzx67F59/Fydyxi6w0D6ES9oqYnP60se6/jKdfOnAXgpvg8O/m+H3xLkevTn05XpSWjar6CmFHOrukjOpQoaQ2biNbiS6Rfqnvxu29eUAHaI2EX+Otdt6TBmIBZcN1q+ioppsTwhH2tnlkd+GsT1FomrZWxj4G1r+Q7JfQVSupkZ4zh6j28uZ5TRaOjJGby1nlq1oWNBw4M1/8RzM4nN+JW7jAbfzgDt4wHMB2GMDh9DBonQG+lfSHM9ZUlQazMxk8wV0pKTlZuyb9QG3UOdBUecspCjqvUnsgALe5ncPpeF3pwzJnFAk/AyVNKpDvzAkNSfNSoo6ifK6SDKk0RmXgyE5Jxt4A5PHjONDyvUP1QnsfakU/1Dz9QsVkXRlHDql7sSJp0tC2iCVcuBQTYOJkRyBUvylG2o2n1F8YLELqrOeyjBKpRalnl+iUHC0szAT3OboKXe2OXaWe8fqPlZxPxYYqz/CDEFiP0cXmW2Paqr26MZIHITIMsZpiDjkB5zvBwz7AQkAnMs6HnE10vIc1t3S2NBgq/awMqpkggo+AoXE5leIuBdwrhNG5IMKUeGsBiqcVUCh0CZZqmFIyiRez1LNCYlXOuisT/upciOUoWU7QeuO4mY/49pMkk1fVTaI+4CvDz8/ysz6MHNg/kgQDbriDE1KkUrppBUsSI7UDJ/F/cD5altvQU627gZ19QJXVy/y6erFVFfJztatAV29hdVV2adMjJ6GqZ6iRw4xVY4OZRhXhGI6AFECkLEAZDwAmQhA1ADkEoB8TnA8o7h+D4FjCDuGEi4Iczl2jlq9PYqakiOOkzxaRh4eu7QcXRFZyVJHWs7jxo3r3q+3cIlKHNBoGRMK2SUbhpIZzchpyKKBSdJUeKIqCCs7QNJVDRDQtas2ChgD2qQsJVKaqcox8aBdueJNm3WbVvM17aW0aYlHgmsDTfsGtmkLfjEybbuMti1uRIpTOHX0OJYUp4OgmSDoMgRlLZC12SS+NJjrZUHQy4OgVwDomgqYjUpDbJcs4u+yU8exTbYbB1qRNBXbNrs03VCycs5N7YeZ7IzsNuwwLuoBu1NLqwrRhWxey6EzFYApMCWdIpTQey2CuiZwRoqa0dHFqEmnbMIDsNqtahTUrauTWCD5HKWlWyOFUUfzcCpJnnNpkt1yYJNI6Qoq4C5NgylAv2qkgAU5p2iQZn2xkLydk3arpiHRD13C3GFEzZj03rdhphmPDPvR4aTljudCTcs6yk809aeX38Tq6ojt89G2BMhiRHyl20xRqwWCynqFq6xX+pT1NVRZybLtsoCyTrPKenVAIxhtXU619d3wV3w9BCs6LC+atn14Awd2DQd2LQf2Rg7sTRzYdRzYmzmw611Y8S5+gyu1G31Su4lKLYWpY4HFbpqV2juC9Bmx4TKazMXfVc68FjqhnPPWRVhPRYw1F1HOa/j6I946S9pts6S9Z5a02yGti58G0w/i7sBv0XFSG5zE3AEFxedfkPhewLtW2KnAtFTKKVMwuSBerHL2HIZMX4qUG5h/0AlMkeww4bF8tEZ8/ASm5HcCUw8/P7MYj5NW4pmWebQqHwBchpVgoMEymHys19a77JaPEmlEURrFvJSG73Y7xgd8u0D30l0gosVDgY4xwHaMDxfXNqaDSFTp0VSIHy1Hl3NqOjoM9fDq58eKJ91fPOmB4kkfL570YPGkTxRP+mTxpE95kmaX/qdd6T/sk/4jVPqkBbYFpL+Vlf4Xi3LDCH8DFT7aX/HLiOGOXSz/Xyme9J/Fkx4tnvRVSNrOTSri4TloZ74GRUTmU4T4dcB4owBzTuKokJoVMB3juiIT/8jj5gS1M7LEKTkm8bcOLHy6b5BRDyqz+aimZtM1Mf8FXN1yBEyM6VTIH0drEWFtizfJsiDf8DTm7Cr8mKvC3/Sp8LfL3S1gYXNAhc9gVfj7RfWHUeGNVIVxWiH+EKfbQ7qaVWxF+1EA8ngA8uMA5Ce4mmch7MaS3Zz24CVnjUJuPKigP8XV/PwKEX8GOFfPtUN1aUHGk2qSp2yY+0vKNH6K1Hr1bKk3HQet7SebJlVLStOzT9WpTSk6o5w/B35efFh006OLecI50atfOMIvqk5PuOr0K586/YaqE5lZHRtQp2NYdfq9v5kZLTq1/HC9Pwk/5XL7jI/b58qZdyhlAW6ffbroOxThNMogznbFEPSeExN5JaVCQ3fk8xnq/NJWX3GO9Ko50qvnSA/PkV4zR/qyOdKXz5Fey6RHmfSiGlRX4bTJqgpvm6yuoMeMMPX/nva3ya8ZiLh2dr6Y5jqdNhcuWMQTAG2du8khnSF14YgB61O7QifOmeOkOXOsmzPHyXPmOGXOHOvnzCHNmWMD5DjFzQFT0nTUzlO0CU91m/B0XxNuok1IXnV8J9CE32Kb8Oy5uGMa8QzaiOuwEaOAWIeDEd2uwl0GMcYD1vOADTxgIw8Y5wGbeMBmHrDFBkZdYFCaba40233SPIdKk7gI/tzT/h2Dz7LSPJfDASPATVSAuOEodkDeFZajZnvjRuzkwLo4sG4OrIcD6+XAdnJguziwPgcWtWFBue125dbvk9sglRtJ/WBAbvewcksE6TNi20zFhq6Bxb2Yley6dvTaHWgfB7afAzvAgV3AwqzvnYgXcmAXcWAvBtgdIWc/N2eMyqMzs01zI846e0Q7qOYAip9ik4gnXv9c0zMDzZLNNOtZJa6CDcf7aETam+iPkERJpm7HcXd/zLQKRkemxoQyY6+fCxlTl6NkdcxQyGs6gFUzas37WT4Ue8tQMguj/qnL+QVVMf+/vS+Bk6I4397Zo3dnORZEQLxoQEVlZ09OBXEvYIVll51FvMdmpnd3ZHZmnINlxfsIUWPUGK+oIcYYb41RY9R4EjVGjVH0b4zxilfUGDXGA9Hw1VtdNf1WV/XQgAHCF37aO/VUPV1VT1VXVVdXvQWaLIbbpm0Dq+wNPIBf3RcE0taXEnT3ljnMRGs8umxZIvfpYUoNTJlbHyas32lqYrLegeu1VZP5hxjmni67eXgrtHUnHMO0Gv7L7NbpNyXLFQnX19ZwAi0E7lNfg+i1lsOKlfro3ZNtT72ujt9jGvixG1lIHbpPXY2QU+SYhiKonU4dll563WQHRe+m1ukxQgDtaF6BAw1zAjGXXiVkP8+G43kOs+eZ2g6/Unqef4Sf5x75QUPP837seQbb2Fo0F7Qt90wep8CWKbCYAuvDGHtO4wosocCSBHu8yPH4HhpdTl4iU+hrS1NAaKpyr+RLjYFEnDwMS9ncfYbai9bp6Yosikp9jk4e+CbrabV8DH4b/gAs/BYegP9VeO34XIVvc6/wabvCZx0Vvp9VeGqv/rtShf8OrvAr5ZqIKvz+rMKDxXkNrLMOarbP4tBOdgKnOIFTncBpTuB0AoxGgH6w/U1FO4N4PuqjL77wViwfB5KbL7Y+wZAOjdRJ+I2+e5FmPQimsunr6zh9iUnPERtgkyWoPUfTrvS9OddzwUt5ivQ3bGIonQ336kbaMTckTQhYhoXhTnRW/EySl4jcAzongnGng/tOlM52ZsZbvhmdNtLO4hq7DsFX2XXnbEfdOZfVHXoCQUaqOylcd853FCaqOJNYxTkVBowXknDD0fwxK/ofqOGL1PAP1fDFMHB1wNolKvBSFXiZCrycg2yOHUBZxCtsEa9yiLiaiTgEfI+V3mOOwSJeo8wXkrKSSQl2orVrSehy20a29nOH+zqH+3ooItuta3CMUBM2sm09WYo1Khv5OgQn8dRt+o00OFrn+9YyF4M8knEywovGe4RvQ7n5MfXt0Qot8f65J4/PyybInVLCrC6bRnN8Y9LgFJ4/bYWpW/xE5z3ATzjGsFJoi0hbYB1LyIa/VhfOafGch9Wr0Km5W3K1wqUu32bX5dsddfkOVpdh465vgdQgHILr8t1i7UOVOMAqMZhc1+6xgqXCrJLe63Df53D/xqrE3K1r9zsCPOBwP+hwP+RwP5xzu+ixxtbjUYcejzM94Kuvb7qkx1Ssx5NivEiPKqZHBvR4mgTbxW02SvtDXt9n8vr+kfiOcfPVtWfzkp/L67s2r+/zeX1fQL7StJ9cGi/apfGSozReZqUB+1p8e0mlMR6Xxmv50oTKppqVzS7Qd71BSDsFM6TtMFIR+8uF9lcX/E0X/C0X/G0X/B0X/F0X/G8u+HsYtz/2yCJ/YIv8oUPkj5jIw8F3mCTyUCzyp+p0IHlrmLx7gryfFcFELAtejQ5Csb6JaJ9vxP+Ljfh/uRH/dRvx/2oj/us34v/1Rvy/wf4BtjqITvpRf7mYNtjF5CsWi6mo2ComWKXi+3q9s5i+Wo+KqbQ4b7pQcdWy4toDigsOadnZ/pBnf4PRyl19Brn6DHb1GeLqM9TVp8LVZ5irz3DBJ2B/vZKFH1GcE36kQ/jRTPgR4Ps3Sfh3sPC7uaUFSV7HJN8VJN+DEEYovp1qe7p5jHXz0N08xrl5jHfzmODmsZebx96CR76WaKKt9H4OpScxpXcG3+clpZ/DSle7pAQJXc+EhtUTWi0JP5wuAIQNAtHujJXuOjVcr4Ynq+EpaniqGp6mhqer4Rk0nwSmn1+oh4uwB9rCznIIO5sJOxJ8H5KEfQAL26RMB5J1MpMVDsnRWnjyrFdhO91z3DzmunnMo8+O7KFrrW6UQ9w85rt5LHDzaMvpbGxE53Zb50UOnYNM51Hge7Ok841Y5yUuKUFKT2FKwzJE7XASfiR6k0Tt3BHuXke6ex3l7nW0u9cx7l4hd69j3b0M7sVex/O10mFbetMhfQ+TfjT4XiZJfwmWfplrapD4U5n4sNpT64O6qVojp8VdfRKuPklXn+NdfVKuPmlXn4yrT5b7ONcZypr325oPODRfyTTfBXzPlDQ/HWt+iltakOTTmOSng+SnFSvmbk5XgWeowDNV4Fkq8DsqcJUK/K4KPLvYy3TSubaK5zlUPJ+pOAZ8k+ud00lxrOJFihQgAaczAWGhuXZxsXJK7RI1fKkavkwNX66Gf6SGr1DDV6rhqziMBFVNc662Jb3aIek1TFIwb+A7SpL0CCzpdcpUoG3RfHdmflMUB/Dltptt4eGGYrqXHjbW2XYcYG8dXZd+UzGy4wA77Oi69JuLC+ytdocLzCM581bMPJozb8PMPoGZ4MzbMfN4zvxlMcskoKcJzDM4807MPIsz7yq2JproZNPFAvNSzrwbMy/nTLz+voIv0ZdtYNwk3PMWfs978T1vU93T96DAfJgz78fMNUrmWoH5Amc+hJkvKpnvCsz3OHMNZn6gZK4XmN9w5mO4ZDYombDCymbCIivKfALHCUutZOYEgbk3Zz6FmROVzGkCcwZnPoOZByqZ8wVmG2c+h5ntSmZIYBqc+QJmhpXMtMDMcuafMLNfyVwlMM/mzJcx81wl8wqBeRVnvlpsNTJ0HeJqJfM2gXk7Z76B47xDyVwjMB/lzLcw83El80WB+RJnvouZLyuZHwjMDznzfcz8SMncIDBhbsQyvYOZMEMiM+HN3maO5MyPMXO0kjlRYO7HmZ9i5iQl80CBOYszP8fM2Upmu8BcxJnrMDOoZIYFpsmZX2Nmj5LZLzAHOHMDZq5UMs8VmOdxZmEJYp6vZK4WmFdzZglmXqNkQjf57ZpQGV7iWGpagpb//v0raakpRq6BNNljBeilvZlQuZk20gVbMF4YU0ItQt3qOcrbtjjKXa0ob/cc5S+3OMrdyD39u8NlD7jsSS4Vd3qO/y4e/7DNjX8sxKrDZRxcxsNlAlz2gsvecNkHLhPhsi9c9oPL/nCZBJdK0Kyw4m6vaa64l4fMDWsUDBryfs8hH/Icco3ndPLRhjQMq3jC8z2e8hzyGc85eM5zyBc8x/4nzyFf9hzyVWfIYkcI/q/iDc/3fMtzyHc9h3yfhxRs4TlYNOSHnu/5secy+tTzPT/3fM91nu/5teeQGzzHDr2jt5AlXkP6oAujHWigBG1fho5M7kCh34AOtBmyUF2isAVUowJrVWCdCqxXgZNV4BQCrvI5UNuKioBz+wXWemWV8R6+QIRZsLAsogTQahBYPjbRdfGYNpWk5mtrmchEvj4kt+oiCTuu0oSppyMzZtRIpnlIODCZE87YRIfNIb5qsCmRiCWjfCFL08BS676VemNr+8Ij9NY+o4cumUklLEMGJGMwFW/lHywhmJTdHE3HzQF9Tgwc/aTbShFGoltaocKU0oOZgViWLqjmGemhKypX1NboUcAnpau0aaiU+I58PGSyLjNK2K8C/4F07NQEP6ntu1ls7BSGmvbIV84pwYfQ2ElrUFQJNJm1K6unB0HYJlX1aS6xLcBgSzlaiyr0HBU4VwXOU4GtKvAQFTjfk4httojtVESwcWTZDlyERbxVEvFmLOLi/CLuxkScB2GXQFi+/Jmn9jAVeLgKPEIFHqkCj1KBR6vAYwg4mVr64PWVlCOsbsotlNRTZnfMtLdD0mPaV1RqIX47WDfuajzC4Cr7/GGq8lzqBSqbWOUfSSpfhlWOKtKOVN6dqdwBYZflwrbZ+YypwD4VGFeBCRWYVIHHq8AUAa8l7awRzzWj8xKxHuKYn4gYy+CPZSJmobkCGrCUtFmkEwrBMupKzfVkSW2zbLHwcqNxNuFC7CbFWqmTG6QGSEkuN2N6n0WklpyWmqSZi5u2NSirYLV0rlzb8pRr1i7Xfke5DuBy/Y5Urmficj1JoRYq1z1Yuf4KPnWeAn0sDSsYmtNOdcFPc8FPd8HPcMHPdMHPcsG/Q/DQgkRfolJn5k6CBth9S+W6KCiaFWaErSSkt+CFwLfk2G70aNKQtFS1VSSSnWkp0cjZx2kSvVxUZ9tFda6jqM7DRZWSiiqJi+pCdXZRae3JSuswCH4RcQyzg1uJ036oRC9Wopco0UuV6GVK9HIl+iMlegVHAwiVxbzKFnO1Q8yrsZjHSGIehcW8VpUGJOVYJuUDEPg6iHPq5Ck6WH8a0K4XnTeIzhshVeCkrptsz/bmw7WbRectovNW4nyxULBLVTdHn1ffXAmX1tZKXbCfxRsYPuCZxNy99ZFANOocfulwxIwNWi7hfrlqTsaHdHua9ZfuWOs36OLgVEJnq/TJz24yOkN7+Pmi4XmBNLUuWkmXM9sxUgfcwYLptjSrCSVNMungmhKxRApF1hvt6SWNZ8SZadpiww6aPrPHICNB0qymyXMM48SmzkBtXT3JG7OyNa+pWa+bTr9oVWq3sYIJgNZyxbrdrlh30IoF5s4s28V3lSBjModIFWserlj3CLUB249hNepBCHUfryTWAmDBdb/gekBwPZhztSwMag8JrocF1yPE1SuYn2Md2TxLDr71Ryg6h9LSjg20m4PWSraJrFJbw7WFuGVtH7W1fdyh7RMlyFTMVEnbyVjbp7EW2DoMk/YhCPRM7vkjNVz7o+B6VnA9J7jW5lwNTfO15wXXC4Lr/4jrJ0UqbcWHq7NhCeky5vC/ndjknAXyYWtDRzt5K0qlrAFGpd4KIwHYyJYkz8tyCJAK90YzxJtuIW1taLDj6k+klvWC+TpS/eccGoBWAkVzqB5kDyP/lRuidGdjMb07RVzMDKL1XMKjGIZH0W4H4L71MyrJM1fftKSZ/6VRNc0JwJYy+qctSH9ASPoH3KQBgxfRVEw/woym07k1/lNqanS25ZS3OodGw+QtVDDLx/MoVFIyhoslerKmiNJ+GiPai7nnnRSaXCdfsuvky446+UoJsv0yXqqTOq6Tb+BKhM29sDr5MAR6E0ZZEAjZ5tPeUoFvq8B3oLMi4CM/wei7uaBNTS3BYHtna0tQ+5sKfE8Fvk/AG4tythFF2RPWxseWLngVSZpmJBaFkwjqyHv2gL0vkrWzetgydmgDPalokhBJA5/k+585h9tXdO5d7o4Z6V72xw7dHU2Z/eR/UkmWxnionMPqnagpRBZNGCJkbH1ZNAOmgGOmkSY3SKQilXbPQEMnE9lwL/ud7qUHKfQZPcYJ0bhp1Sc4Kg2SwY1BdtNzWir15VGzv5taZCQPSSrBuPN03vEttQxCsqbSiByXhW1sMdPhkWRUnuI02oxDpetPRdMZ5tA+YKUYQKUoV+sP7Wr9kaNaf1KCbNAMlar1YFytP1PUQ2yGhtXuZyHsF/QROGyqNeL5UnCtE1xfCa71ORcMfr4WXN8Irn8T1y5WDTWtKkqbINjQSx70DSysS8fu03KKFGmiIiUa+tD11TqnIl+uQ4r4NZR2bPCGSbEGAg3igawF4IJriOAaKrgqci7ou4cJruGCayfimq98ao8w4yewjWIxIx43+F6rpjmtrMtvmtPCfrGHhXQa2gh2e5eue6St3miHemM0ZJHmHUm9t7B6e+DsYvszTL3fQqCxOYmhQ9YF1zjBNV5wTci5oHveS3DtLbj2Ia6xgnoxyTyvNpErou449rMVmeRQJKAhAy/PSYr8EStSi7OAjbkwRR6BQPUk0FAIdGiuxdEmK7ApCmyqApuWw1pblsxpXdjc0qlNV2AzFNgBBKtW1rwO0kzBe7HVIELjyB3agexGAftGsqKzbEVnOxRt0JCRlwckRX+DFW2RM4xtvDBdH4WgczVo3khQ3KHOU4GtKvAQDTrkw6aKHfL8XFDUzS5QgW0qcCEBfy4MK6Fr6yfNv0mP/NLhdNJKpzPXV+e6NqlnFbpJqc+mXaoVsi9KXgmSUJiVLHw6GYNOlP6h+7HZ7SB+8sYcwd1uhnSO1Wm6jDvXf6fMvkQGOt94JpWIid2uZWTByoWyCw4nlsM9iDse7R6wo6REJMhAPMy6d94/hxNZkmL4hhAzYNzSTyoDOMMpI74MUmXl3h4dLM1mMtByiqMFOg+UTcKHim5qaYIAyagZJq1mOyvB/J3xIrtiBx0Ve7GGrPDcKFXs63HFPlxRB7EZHlazH4OwR1rNyjS2xF1wHS24jhFcoZwLepljBZchuJYS1wT+xtNlLMv2gTRWvxJsa8pBWpjRXHoW0xanxyFOVEMmTS6RxPkhFqcPZwMbM2Gq3AyBEpaC04SnOKkCj1eBKet5nyY+72lV0IwKzKrA5QSs5zrCoMJ6jNK9WXhCWeVOZOG3gGn97G4BdDdZ3gFb3pUOeU/SkAGN0yV5T8XynqZIOragwVR+DsKeQcKO5O+0osX2M929znL3+o671yqNv3fBAPG7gutswXWO4DpXyzsLdJ6t2vkO1S7UkOmIuKRaDKt2sWvCsREJph1YwtYu1diA2+680tplKvByFfgjFXiFCrxSBV6lAn+sAlczMIBAWcarbRmvcch4rYZmaY9AolnhD8My3qBIATYdwQQEM+PaTbyi4tTerAJvUYG3qsDbVOAvVODtKvCXKvAO/vTmFfAuW8C7HQLegwWcIwnYjAW8X5ECtFqPL5aQF5LDF257eSJ85LbODNbQ8kT41C2vrmgTmO2c+QhmLlIyDYEZ5sxHMdNUMrMCs58zf4eZA0rm2QLzXM58EjPPUzKvEpirOfMPmHm1knm7wLyDM5/FzLuUzEcF5uOc+TxmPqFkviQwX+bMFzHzFSXzQ4H5EWf+GTM/UTLhxdtmwrs3Zb6CmfAGLjNHCszRnPk6Zo5RMvcTmJM4803MDCiZswTmbM58BzMblMxFAjPIme9h5mIl0xSYPZz5d8yMKpkDAnMlZ36EmScpmecJzPM585+YeaGSebXAvIYzP8PMa5XMuwTm3Zz5JWbeo2JWQIPjbf3ZI55DPuo55O88h3zSc8g/eA75rOeQz3sO+SIPmWvpFQwa8s+e7/mK55Cvew75pueQ73jO0Xue7/l3zyE/8hzyn55DfuY55Jc8JPZRrUwdNLa4JxIpLKyIjCrsrhjpq5yZThpxPUze/dOzxqeNmEmPvB9/UJD8PEDfa9pUOGZ+ZjWEOihSWFQxqnBsSa/FLd3LOoQ+UljMblc0sXqJkSLv1pmBtkTEiMFgo8pIJ1fMTloHhrZGZk2dMqOupoReBzVEImBsMWykMuK9/fU0QCDYOretIdAeDNTWTGlrC8ypC0wPtBwWaJ4baGto6mwPNDUsbF8YaOuikRREejOZ5AHV1cYJtVPrp0+bXLU8UdWXNsPxSFXczFTXTq4Pz+ieUjsl3N09OTJ9qrF0anfNjKkzug0jXD9tRl01LDg009XscNO0lYQpNSum1ASmT58ydXJN1XHJnv1omvT2oE7TpM+pq5qutxymN8/VaZp0mia9rUvfrDx4v/0mFd3mpAWKJDKquNBHruWkUEqJquSmgcYFDU3zOxs6WpsDncFAsKO9sysQXNC6cC6ht7V0NgSCXcT3P1MkNAFWkUypmVw7eQoUyUQ7RTpPkU5TpFsp0mmK9M1Iv9dbR4hKFaOKxxbVnfVkMX3yNrChN/yDo6gKzK1yfHcJxBnwfIIv7O7YH1IYYk/oQqPPLA8liepd0UzMHB5K9yZSmWYzHU5F6UkNw0KxRLwHAVqImpyrCMGatm6DfppOtTYPDqXMjBGN0Uo5OBTOpjOJvi5zRaYWO+qwo94fWmYO9CdSkXRpqN+M9vRm2DnkZSR50XA03jMoFLb2pUTNNJwTTH6nqHGroJmBqcD0aBV4WF9sWMi0Tj+LtNNUp0c4ARJolxB5lqI9cTOSO9l4biqRTaYHhQwOpMtDc2DNWFMinRkcghUqvQk4VTjiPGx4BHZ3mgZgY0KwxhqWykRPoClclDXTNPZRoTYzFe4lQkbT8IWQlU9ZqBHEbW0uC2WMFU2JiDkkBDckLSeNpCQEM5nloWi62YyZJKJBRPbl0TQ9M7k8tByOExxYnIoVhaIRLRRPkPQPhrwY4d4+OLNulFgtGo20OYkEHaeC09kk2KBOL4S7TMwXosGOYIwqHE3HWJUPSpoP/hUVwLW4uLio2FfsKyqC45OLrK6NXOAnuWzlc5RL9t3447WE1l547Cd7OxEbHsQmOFqQrvwA4kGeH+IO69mQb3Gvj8lgw+mquWbcJISqBdF0BpTatK1glfpWacaOPnr/jacrmCEJazTAWH/BdM9qtYiPPZBP9iDTf7BCrfl2isl5KPuhRixrbrXyKimrcBwmvvXPG3/qWxWyQ91UbzVFfWVDynxlZb/8djIFrfZWS/qvv5002/3IVkt5mc/3P8G3puB04Qj0416H2hq8y1Z5DNzYSb20kk2JAF6gP/fNzMYOmhmLHtSZ7ekxI3jxFhn4kiFrUo8PxOCkcSMZjehBWJs1ZGY1IQCJjI/pOBIWzYWj3WDHPDYA77rLTDOpDySyfLdHNf3i3WMaKT1tdFsW2UlBGpEBPRqHY9Dj9inp6UzKjGfhaN90NJOljVJat+PkedpnQv2MA0mYPvJarUdoQvQonPNHRiTWqJrGEkv008Xb0ZjJ7lFNcqzBzEG5rVS5VlsP75EaTC/cWPT/nyjoHkCIxsMpMxJdShIeTvR1k9EuzT1wj89Gw8vg/FdIWyRl9Nt3WALmvuEI5jR5OkguWAKAlYVvMbqR6qNZzRA9TLDmXUnvgmKO0xxk9J4ELGAw4gP9JJRpiRHtBvXITUxUkDDFak20avD5fJXPLtTKuS0Lu1qaKvntK1sO6+psaWuptJ6DSvocVE6YPmP69Lrampq62hmTK9mTVNnV0tA1r6Wz0nIFibP58MraydM6W4K102dUTrB/7tVEHnNSeyo7STy1jY3wl/wp+Yy8GW/SEJq/DJaEsuR1pqC4pNIrezEJzxeVl9RU1U2pKfkXiX2T+PCuUbocxlKhkPWBjAoLNu/9YL/JTyerYVa73D+cXumc9ggrypqaGrhB2ZALvv57y8VXr51X7qdGPeHVRQMrlEO6jBW0Cs2FI7w/fjfQXDH3mXnwTY9OIWhgQXEfb1MWY9aReMHsnx/s0IHL57PdJUO25G1EC8HE3oAWCsOim+ICDxLyGx1J2/gyy4gFm5csGfrtvOE4kzV1c26JE7g7S2BxxX/5+1MRVYfOnpSE0tETzLIQO0KO9J81m5ZoJlCZfw82nUyLEKr8JrylAafosL6Yj5bbnuTyZd3M2Sv6YjpL16zxJL/jdTMeTsAirVnjs5nuQO3U8bMPGlI+swGKub07SKSnI3LyxJDM01ecDiOaIj4ZWBTTvvQ4Uk46uWs8fcCKdGTWeDYB2d/fX9VfX5VI9VTXkVap+rC2BcFwr9lnjM8Fjm48cIBuzYzDLOuQcl2f6S05NCwJTb11EtEBmYGkOWs8Sd8BSxOJmGnExx/UbcTSpPmmYTiB3PKgeUa6pbebNBIzq8FJo63ehHi3UhrbjGgsGu80l5LC3r5T2pKhx8h0mmky4Aib6e0gtZlUVpVY8dmCAz1gvWeQPNOkDVsehTUcVUui6V5jaZS8UBscDNWFWuKQqsh2kDW3gtjcvNXvwHmbvAPnbcoOnLepO3Depm37vKVpuPF6Nc6PIq25Ict2UAxb0KKTMRQZYAYzibi5w7XoqrztKC26Km87SouuytuO0qKr8rajtOiqvG0HLfq30UwC2pjI7HBNpDNfO0rz6MzXjtI0OvO1ozSLznztKE2iM187UHPYlWjdMdtDIWM7UoMoZGxHahGFjO1ITaKQsR2pTRQyth00isq3/g4jZcYzwWXZ7UDwLWis5yYSPTG64vFbaq5nVm/S95GDSoZvyZK5veCzZSgWjS+ja4S7jJ5hIZOmvtFMZ9q7u83UqNBS/rMtGo/2ZfvoOmAt1B+NZHpLQ71sdW/ETGZ6R4TC2RQUbMsKsy+Z6TJWwOLeJCxkTWTTCNyNB+wgLzVE0U57hXF69xxB4VkRImoP5D5pNe9M3UEwdmyDg20wF6I3ahWfmVpO7jPMug18akwNWItxKUATFR6wYulIpDNGDHyte5B0ZFPxNGybTmbMyE4YhHUB0fhoDKGV1btinCeFlGakccCidMB/sZY+ks+GSCRFys2KsjmaThqZcC8ssWszVljpgs+xC7N9S82UP3R81ohnopmBYaE0uW1XYkHCOiI4PYQCEA+sbx6dc5HbRzPUxh/gu4XAAE+Y5IbK276c1LqIaW0MH0z8jFS4tzFBAvpDkVQiCTfRc2VsVfOGbCYBeGs8Q7Qlbdc4u8BdQpTBKudoKjMwNJQCww99ptUcDIeV50aPCcuWLMRXUABLhulSYbqAmP4r8FnrhokH8faVTNp4/W8mkVATGvDN8j8dfhuvQt3G0T/0La3dlB7+rfbF/b8/B2X+srKykhLfIA+f9ZvZg/2/muvbtO02IFjzxhltuTaNk23E6sX5wia6xqvAV1Py+oYNGza10dFCdGkTX1qlVW8CH6+u4v9KXiOJ2NSbqJdY+V4lt3qd2dagwCvE8RoDfH9x+r6MfP37wGUiXPaFy34F8j+nqQW6VG4S/LL66oZYTBXElpz/KIZlXvRCHKfOLllLErEJT5DLArMquFRDxvwQorx4p23/pLkvKiqF9UFl/lqkbzEsgdsh9jbkXUs1YzNTn1tUVVdgWX+itjdgceCOtY0hr3iNW5qNnIr1rNZRFWHR43/jMv68WnlYMwuJzSkyGT+LsAL0v3eTQF5dPBxmZic5p84UpE7pbrQDKWCno3lou/naVtoZTCWXIRWluws38bDSVl6JSm83zbrdHvx2m31iHJhE8s+AywHQgfigT6Rrk3krTe2YT3SitL5Ar/nfP6DNW2+8rBWWkp6rPzMLLCsL1F45DC/oOGMWaoV8VSplfdUqtII7ykrreMnQocW0zexdaAWYDZeD4dIAl0a40BOK4MQrfwtc5sAFjk4orefx0vI/eEsbZrpwf7Jwz328NWCUOUVgelhtbj/idLE/PJOb8BzD+diwR7qc74NvjQhb4v20EsGkjvULsKF0zNze3WYaaVItR4XSZjxCJ2NILuj2FtBheIjt6G6NLyeJI8/niFAim4EJuUR4WaPZayyPJlLD+L72gTaDTriUG5FIV4JWuUH0p7UhYSf6OzcXBN47L03Es2nICEz7xIwBK71ptux6cO7XYX0x5x718lCUp6o01BaNL8rAX2MF+Vsc6kvGd7I3uPMpomH2Vnha0dIVof7eRMzMGaBw2YxuTcSQsqSNDrkWFxT56DaIsjKfz0Nr2S6JRgt6UMkm7hQeUuuhWnBVgJile4TLtvIu82+p8YV6sY1Gzck8+3/NcLQPbvLtvojTl6e3R/no3hfrfa6VtrR8T5J2SMEm7AOcD/UL3k/T5O+d5KVus2qpy+sdTR3dHwRvjfyn9Zq5ELrr5JFv0w1D/nbWEpZrHUJmrLCd4EO3CpXAwGGzhh2wGaeU9I8ZsxW3gPY8sUbtb5AWz8hmaOtjNUwhg5rgnZMyzcFgTiNlJBNgaLc0RM8ETA8PdRMvPvcBvXBpKBEH+zRDQ4lUtCdKug16J9JcEbBrIGmW0F+5qd5crwkzugV0dqrMV+Yr8TAcCrI7tsSzfYXeegOg8NGTj89s0H1U0ABYGyFrakrZj1znbc0G5PxLbiaFsYkJVFeScl8JjOE2cew3lvVlJaE47aV6stGIGVloLIeOwYNBF2T+xfmVp48MhFDIIRSYz2y2lIcOtcyNdC4gwyzS7JNnvjRkkKZruXcDMMpuC75DJPpb43N5TqI9tC8aFFpK6l8EhmhpP0k1zP23RkrZL38o3BuNRchP9p2pPJQmWadijiBIt0FamI7GIMdGs5SQMR+BWlYkU9a5RoNDmV4zToM0DgwOwZkupKO3DOUou7pB1LRMilRu0u3Znx/gZzEYLSkugm4QOkJSlX2b2H35tvEU0GB87814K/lv3lNXVuZ9sqcxlehPW1WJciDvW8a2ujcYpmuLyWUQtiF9qBNY4gQOcwKHO4EjnMCRTuAoJ3C0A3A9WT4EDWQZbdP8x7LWjQ7fVpO2cvNlydO3+q4id17N7d9GHAlFxoPhBRlaWXgd0rrJZSfrYCTBRnePC97rgkdd8OMI3ijjeoBaLdebjCQczwELDOgpMAB2mHH9RHR2L3C1ZeQ+tZt8Hy1GaD/wKXix6DKTh+K2/HNsbto/zU8NAAbdLQ8eYIs/rZNf5FnCm/hht3y/GYvpGYM8TWDkz9QT3VVwWGHSkZ1xWh9J2DPWicMkFutv7kCo3Fk4pGLo5JKIZ5wHliGHFU469Y6hzGXYec9hLMNi9vlRBVyWpHjEUE6BSj1Nj2MOWyevxOHQUC1u14K8Ntr5U5KkTwk84f7j4ZJiIz8wSOE7j9Vnu4afi2v4cnWNQxX9AFbRb4DgK+BNiAWnZhrS2oAMnSBDK2XoRFSpLYjUwyX0yCLLWakH4RQG8rfRjHFQrtQnkfvUbOp9tJMJ60LffDAhAebT0jqMOfVINkXtJNDRh1U99LTVn5NqCeYYhIiqHGmB6kqrOTp5iUSbZtEvhegtkD4I5AU4bZ3bBK4+UtPGaaeQdF3mpUpvalUWD4dCZ0XF6EkBzIWPjMqd/JhLdqV2ql2OAUsD12p5Oq2WdLroDLicWYCO6lspVcsBXC2/K1UXu0aWzmRDezqR5mnGVJqPo/Nc58DlXLh8Dy7nweX7cDkfLhdABLN4VAVbFFUJTKlt7pwczPqh16sSsORn7OwMCvb39pPBpAOhhglbIxM9hhwrh8vN48BQNp8/vKTsKftbp0Na68oU3hFrNopqNU7Bhma8JR451HpR2FcOkU2bcxIp55h/D0VCk6RVjnQlFqdiep5sBMOJpLm7HMBarwQzFBPcEtEExwWnTLBnOz6fUIlErCuarJODZCwfltl26U2mUqLMbMm9Qxy0LBSCw6GIznOiZixC34bp+wP9OOAjI8Oyss2olLRiQK32bjWjAdcJOi1GXmo2I2paFrT98Pn4S7Z2IeRnYfvCljMIBBPi/h9AE3IRjN3m0BN9dDpFECMNyGYmWR4w0nmYi8mlaGXNScyuZMlxJIbNzZUchQ+NSst9B5M/s3mWLyM/Lirq6iVjJfbY6t2mAa8laX0TbCxVMotC345tpfK8xpUq9RSbNJu4SfaUbN6m2lGqtDrsLbSfNDGjl+c3nmT33lV6GXmwDCi1y6F+/IhchgUdEyc+k9SSGO/orsRVCI6tIB7H5WwAW1NE5b4GXPiryY/B3MgSnf0TIr0a7tjWsdB3DI7nGhYPe9nxHaWMpxHH83OgLDT7y4XbXw+/mhJxa2GnbwmO5EYWSRGLZLEykiYcyS0gETs4Xt9Hh6XC4fQgIcbboL3gL+6+Dhzh7SxCZqfct1AZYTOO8M4C5YB3sBDlr6BJoQ9+K47u1yw6ZhLeN1cZXQuO7j51dEOE6O6HEm2ILIHpMNq6+hpxtA+yaDUW7cHKaOfgaOH0P3H8NFSI8bcoxgUGGeD5DsQxPsZiZJbifTOUMc7FMT5BfuzbGCP9TYo2MJ3pQBqs+uqkQ0qZfaYwcK4QEvMkuZTQ2ULfZJyKp1kqylgq6pSpgFlv+HSu/ZFcfnPg/2wDbXRvB5pZ70hZ9sW3sw0b9EuI9RFt26UsGs+MP6hOoR9d6x+q3cYpq3FNWd02Tlmta8rqt1vNJm+3mk3ZblM2dbstzWnbrWbTt1vNZmy3mtXWbLei1W6/vUDttu4G8iRt++0Hard1R5Anadu6J8iTtO23K6jd1n1BnqRt684gT5O7rXuDPKPb7bc3qNt+e4O6bd0buNe1uu23N6jb1r1BHtW2396gbvvtDeq2396gbvvtDeq2dW+QZ0ZhW/cG7hNE9dtvbzB9Wydtmvtr6Laua6qk5Rb2h5bwjQ3b+olQ1Ts7me0Lt8PHwk7ewkR8W6ZwozPkrXFaGdvjsW1oFXhj8zVTt/WT4p62+m042txo4S4xl24HBZunS9vW42GYaMjz8Ha2Brfj0l2Y6DTTZmr5tm6gVQ8Hap+zmZiZaTLBSNF2LGZbQwfb77N17IvBnqVN32cH+5CGhRLxeXDkLfcaHkqZCbhTV2/KTPcmYpGhdA8GxSAdFbZzQbQvmhkClqRyvrvnXA3LjWjMWBqNRTMDcADl4kx4aM6TMkeEjFgM9jmadvQ7hdLwDZOUcy7+ilDGSPWYmUVsd9RwuquS1FYISPdVDqdL+8D8Vjpj9CUJsnMoxjZQ8jtHzbS4zYkttiiCDZFlvq1/emYZ+TdkyLdksAO+SS9w5Hhg6x2dCatjRhaw5Qb0Ul6AneTfH1d23fnJT9bOK1h73Q1n/3TtPP+z4FkM++n++/debunGdznpuY3vzyEdfbCvg25mX1tgreKl6LHkQk9Fe54j8H+Sh30Bh4WF7rAqna56h30tftjL4of9K37Ys+KHfSp+2Jvih/0oftiD4od9J9JyZf/r/JfP/1dYUVLuf5Olla7C211aqbwrWqnshy0iaLV8iufhPZyH03ke3sd5OKPA3kLihy0kftgv4ofNIX7YCeKHbR9+2LPhhx0Yftjt4I8r8/CZnYcvCsTdAOsK0G4ATcpMMc7McjEzZ/LM5KwlkX8lsGx6s1dCj54ZpPtFnYtTR82kQaQ1qwVlJXwtJDtS0gcrts9hpcOx7zGMVuDa2tqq2lrfeQwrRuG+z7AShJ3PMA1hFzCs1MZKYFXp5i6XhTTYmyvxufC75V2xW+Qre+A/Ywppq57UWkvlJYVYai3ITft9Pl4BYQkYSAvrwbQiAg+17JxyG6j+Yh60GJraHaKj2VLLUMrU55pazWc3PaW03d2UHQxyGw63reAtdZmPPXpF7NEqYY8OeJY+z2PbbOszpYQ9pKjiBS/R+V4vsNb+WTXpr7yVBctnpTzMm7wRK4c7F1j/l763xQkdZCX0fU8J/UxI6Bc8oUNwQtfxhA71WevL6WaXDUL5bUZCi+FR+29vRPI+MN6NF+QSnntWKri0BZZ5kQLaMGG0VOPPEq0qHnoA5dNJ68wwWmd8UMe/nY6f77XazSd2+nv4UKf/1387O/3X/+3a6VfAw1O2sQpNhSnnwgzb3GdoAoi9F1z2hss+cJno4xvG9oNf+8NlElwq4RKASxVcqi0tB/ns7Yl+2J7oh9ciP2xB9MOmQz9sM/TDHkE/bPnzw/46/6n5tDzAJ+5bm+lD+9bWSFo+jLX8rqjlEM9aDt1iLRtBlCa4NMOlBS5z4DIXLvPg0gqXQ0C30tIKHiFtV+o2/RGi+9qgQm/uEwGvVqX0mW6N7GxbFMqFi0jv9KoXbedbtdNW0VjYi9Uabwkn+vrMVNikS4ubjFg4G6PRjJ/J5ovsWzqHYeNmtsYJnTh5JM4Qo2e2ZAwSmTx8tf4VFZXR12Tf1jYHtPWnA7gtHaha/FUa/tV0L6bvzbTKlZf7oMWifeB8H3olgnaL9oEL0BjGN4E949C0aW0+uJh9pKC0heh3O/rdgX4vQr87ye/drd96MBHLWse+H2zt4v3rqZentSAJcZePbjY3MjaudydSOql8eqLb2hXF99b2WTeLm2aE7k6KZWGjgU53/oUz+hxSGXsr9WAz/D+viWgdDacS1M2TkYmGl1XClqO2tqYcGjZSET1lGhG6O7gXXJFUdDlsO48sj+gwc5VNVpIQfYnldAfU4mCj1kXSHmY3aCIU2MvvTAf3gNTw32JK2oKwG5Kn1MpfJezG0iPZhD65Z2mlttiWVH4VXZIb2fsPd/RKR+JC/ZnUkv4UtaRayI4DvZHuxWrCIghioBcKbSl2hLEjgh0mdnRjRw929BLH4WzHCsMq9cZJS8AmAnli5iUGjEq9K9rdDfv6mxLkga/UFx9KHo9EzEhFT6A7qslQi9QUM1KpL7SeHr0ZbBdnBiq1KIpLVnCZrWCfQ8EEU5D2RRdICn4fK5hGkSAJ92YStkGYLHGU0MqhLbd/9ts/V9g/B+yfJ5Cf+9Of+gLYtGdtj4sItiHwU7WShH/OeqrgQXIy0YNGZDPiRsSgD10H7GknrVI0Yibo/ZvZUxfMJkmm4GvJOJ1vLKI2pcyI3m1V9XSGPIkJ7krGsmnytICVqQi24kDvCrsaq/T6GmYqgs+4wvOst8dJ9xIliWgDI0WJpaTYF5JHTg+GE5mooZ1IMhZhCWhjCWDPGtYCMp1L+4JOK5eJuB5MknYjFoWB9EIw/cABIkylPtcgD+WcBBGIKKWdlFNfrjCn2BXmNEeFOYNVGHqkwYlShTkBV5hVuShQddmHVRewyaidDVWqKxVNJiJp7RzsOBc7vocd5xHHgbQk9X1YUTIv4rZ+CVpJ9h2+D5XN+w2080n4s3wOwwy57bGG3k9uoVsnUZBShhadbiKFHbLWrlZyhWqZsSLJmUGgFcyqMZYXrkhKAyVV2gUkLdf+Z6w5WGnI3YU7033QTXFXLmeO0DSTYs60C1GpyXXsIruOXeyoY5f67PcjX69Ux7pxHbsCRYJq2URWy4IQ5iriKG40etLaj3O/Vud+/ST362p4+OYaETL60wGApyoNXeQCIwl5XkqxZCIb7gW0nzz8KdKPJbordVJhYOTTlMr2JWOw7XdBot9MQrfXmUhkSOAuoy9lhMnf3mh8mfbTXJzX5H79LPfrWv5LVu06W7UbHKrdxFSDVwFfl6RaJ1btNh4Dkmw/Jhnsf9RuJ44RvIXBD8Mv3TzucPO4083jLjePX7l53O3m8Wvi8T0f9+Hbf62WnzeFoi9vMZ3onOxxUX4DMqpJxAdsl/P+0L4asKMczhviDUNjCqy/VGr3oLTmtbrjv88u0/sdZfqgzzZM4jtYKtODcJmucREHFfH+rIjvgPCPwtB4ToqkGtq9htjSbF9ae0wFPq4CfwfRcdDoSx7IPZ5Qhf69CnySgBt8c6KkAYQ2ho6DOxKpTHciFk0Qva1gYs9HRwWpcG90OcllGzyBUSOWhi7/0Ch5DVnG+3u7paaLIPSA3hGNG/oRRrzHgMdyAdgVWJDt6TF6SAF2pKLxDAzMyG/77maml7bXHWCBT29MJJaRn0YGJYwHhTjojRqjcWuITfJBs8GbELgHeRukAwF6K54v7SmVNE8TcLgFBhrikYAFy3XnGbvuPOuoO2tZ3YH5Al+lVHf2x3XnRUUaUL2ZxOoNTHhoLxHHKF7POqJh6AZ1i6v9OY/fy3n8/pLH75U8fq/m8Xstj9/r8GZj/a60RoU6s1qMHvOcP7cKYQUE6yukiEmNCkLHnl5Wmat6ucFdF7MOpr2BEhFgiQhYN5YL8027MN92FOa7rDCp7y5SYY7ChfmBe8ZRmVayMgUbBNqHMFKjtVL7h/3zI/vnx/bPT+yf/7R/fmr//Jf987PcTzm3X9i5XefI7XqWW5jR8RVLuS3Eud2QiwJlLsAydx95r9HgK/yIBhi9V7cllpJnvLq9uxtOgSt08yhy8yh28yhx89DcPErdPMrcPPw5j4DlEbA8ZGkHFeakHVIoSltRaEkbBt+Pv0FTjTT8PxCijXBJCRK6igl9KvmrjSThy+dGuzPWRIA2yuEe7XDv4nCPcbh3dbh3c7h3d7j34O4Adcu6jLV1GefQZQLW5S/fOKvcn7EuE8V4kRzVTA6wNKHtR4INsdoM8opnxjID2v4yNEmGKgtBUAHTqdli6MXsgXwg02v2OV4/7Rkj+qJK3/zohA/pQ/qNVB/tm8LZ7m4wqMvePSyDjXNT5I1G74FckfcZIz1OC0DxK1KhVRGPimBHS1Nrw4Kuw/XWrpa2oFa9fSS6RpW2WgEMUFCuHfV27ZjiqB3TcO34nVQ7HsO140CpQLHxQx+bpZxViGYp4YuAtC7C18gq02lw19kQvpGavRzQDsaOBuxoxI4m7GjGjhbsmEMcc5hDD/fC7HcKmdhkI2U2jCbdIQxjVlhjY2u8TDq+2EBfMktGvXPRjWWFW22F5xeKH0XaCtkkFPz/S0nhX2CFF6FIkLZNvm/ry43/aDuhIUdCjUL09ebHUkKv/Mb1642vmSUQTB5pYEW5nFpJnQeWS7VehzvqcB/ncC9zuGMOd5/DHXe4E9wdoG5Zg+NtDdIODbKF9tcn3zmSBt/FhTUgxovkaGFygKEhbSUEs54aMiyu0k50uE9yuE92uE9xuE91uE9zuE8n7qPZzNoC23oqNmlZqXfzmVryQpG0fqX5SJ9PgNDWSu8zM/DQhK3BPp0o0M7IRQnvfwFZ4bNshVc5FD6bKUynQJZLCmewwueJWUMKz2EKw5IF7XyIrZE/1doFovNC0fkD0XmR6Pyh6LxYdF4iOi8VnLIKl9sqXOFQ4SqmAm12l0oqHItVuFqIBokwl4nwKoS6BnoC9mJI7RKTXkf7mQq8VgX+XAVepwKvV4E3qMAbVeBNNhjgoCzdLbZ0tzmkux1Lt1CSbgGW7i5FCpCA85iAsDJFuxuizBl31n4tOu8RnfeKzvtE529E5/2i8wHR+SB3BppUNmL9D9tSrHFI8SiW4gBJiulYiieEWJEIrUwEWHijPUlCjeo0rb6Svhra1f2pPH5P5/H7Qx6/Z/L4/TGP37N5/J7L47fW6RfI8/y+YCv/okP5l7Dy+0jK74WVf8U9PagYDuEtGgmtvUYuO3WlDDLIEmYDX3fB33DB/+qCv+mCv+WCv+2Cv+OCv2vj+WcI37Ml/sAh8YdY4p2k97lhWOJP1OlA60v4t/P8i/IW0EFawRasdfu0kK61gU+79iq2w/nY+LNCtIrtSD42/ryQLakFdJnA7OPMLzEzwZnrMPMUgXkaZ67HzDM482s82LtIYF7Mmf/GzEtzI/lCa/qCTmFcJzBv4ExfEWLexJmFBB1VYP3vu09g3s+ZxZj5IGeWYPQZgfksZ5biMGs5s6wIKfSmwHybM8sx813OHFSEPsF/ITDXceYQzFyvfNeBGQubCZMWlDkMM2HqgjKH4zjHCsxxnDkCMycUquKsF5hTOHMUZk7jzNEozgr+8pZ/jWarcP/5/P5j8P3blCk7WmCGOHN3zDSUzOMFZpozx2JmVsk8S2Cu4szxmHm2knm5wLyCM/fGzKuUzFsE5m2cuS9m3q5kPiww13DmJMx8VMl8QWC+yJlVmPmSkvmewPyAM2sx80MlE5q7b3dN4awisRuYXYQaqoe+dva0D3ztvqYQWltvawo/p3cvsIJvVrvfQu7tnwOXuXCZB5dWuBxCLkMKK770nJZ1W5yW+RDvAri0wWUhXNrh0mGlZb3ntHzN07LZ/eEiiLfTivffnuPdwOPd7DWeQYi3Cy6L4XIoXJbA5TC4HA6XI+ByJFyOgvRpFdBreUtfYRFLn7656YN12v5j4BKCy7FwMeCyFC5huETgYsKlGy49cOmFSxQux8FlGVxicOmDSxwuCbgk4XI8XFKQt/KKYs95K+F52+z6l4Z4M3DJwmU5XPrhsgIuA+RSXlHqOT1lW5yeEyDalXA5ES5gdNt/MlxOsepluee0DOJp2ezn4dQiuhdiiNcoK4bxkLneV8GgiRu+xYk7zUrcCK9RVozyrNzoLU7c6VbixnhWbnfPIcd6Djnec8i9PYfc13PISZ5DVnkOWeu1sH3QM9NxwRlFaIYf+md5XADdIYwLYD2ydhZxDG1q1qv15kOb9TYzEjW07yiwVQrsuwrsbAV2jgI7V4F9T4GdR7DBTc0BggQoIr+nnl/EBxz+C+kAxfq0Bh96LypC76kxaYByHBqgaJfKseNJTSYafGvTLieOQWy9MZ3X/ZETuMIJXOkErnICP3YCq53AT5zA1TbgMtl7ja3NtQ5trsPaHCZpcyjW5iZHzHiikwmzLwhzC3GMQGu39U5r8bd2q5vHbW4ev3DzuN3N45duHne4edzp5nGX7UG/6gaYhyzw3bbA9zgEvg8L3CwJ3IgFftAlJXhClAkNm1W0h+GxwIvktUckZI2E/FZCHpWQxyTkcQn5nYQ8YSNun8GftJV62qHUM1ipGkmpKqzUWmfceLqUSQSLw7UXiGNYbhUNWyul/Z8SfVGJ/kmJvqRE/6xEX1aif1Gir2A0wFBZxtdsGd9wyPgmlnF3ScZdsYzvqtKApzyZlLBgR3sPhoWLg416M929ob3vcH/gcP/d4f7Q4f6Hw/2Rw/2xw/0JcwcstyzKp7YonzlE+QKLUiaJomFR1ovxIjnmMzn+DcG+IY5R7EwmaA1h8Rr7VKf9O4/fhjx+0J3vKvhZqxu5v6/Y5hoWl38e1Arz+BXl8SvO41dC/EZbfgHiR5cDck9Z/9LinP5wjE9ZGexssbZEwik4Of3/td6p/z/XI/0ril0FQmWxgJXFK0AZXoz2u+yEHSOwY2fsGEkcTezLE/3qwjz0v556kT7fjC9L2LtirD0ydG+MvKZ/VLF9ZqNwo0Ce22ijCetcX0u8F85L0U0j3CsulLcOahTOaRRu3p1K9NEDnpbCAuCldOUvW7ivjDUY7ekzcqc75fapoANIdyEp+t1/9LRGOHeLHRhqfd7O+TicQpBlkJ2cK0NzlXP2kszlHGmaR8uljUGFLdfV3ey6uoejro7FdfV1qa6+iuvqBBQJqpxtrHL+C2b/97Yfamv3TIO1WYMt5db22Yj/xI3477sR//024r//RvwnbcS/ciP+AeQfIP4Byz/A/OWyqbbLptZRNvW4bJ5a7/zk9HtcNtPypwsV10JWXDAdqc0gtJ0wrTmVIG+92gEu+IEu+EwXfJYLfpALPtsFP9gFb0A4ldvCZZmbbJlbHDLPxTL/WpL5V1jm+ep0IHnbmbxgV0prI8GHseDVuY5toRJtV6IdSnSREu1UokEl2qVEFyM00ODa9y2xxTzcIeaRWMyfSe3JT7GYIVUakJQdTMoPILBBAg8NZkxqWkbvJJe0tlSBhRVYRIGZCqxbgfUosF4FFsVYgGKKDaK2cH0O4RJYuAsk4b6PhUvL8SPZFjHZYAeoli2294naP/vtnyvsnwP2zxPsnyvtnyfaP0/K/VTsarRzeRrNJexYtQxFnIFzeaKUyxNwLlflokCZ62SZgz2t2tnFsHmD7knFn/vPUcPnquHvqeHz1PD31fD5avgCNXxhDs6/HOEiW8iLHUJeioXslYTsxkJeoUwFEjXIRIW9BdpV0Ms3J2IxSOmPsWM1dvwEO67Gjp9ixzXY8TPsuBY5FPvv7KzfQLMO21gtOyM34ax3SVnvxFm/DUWCMtzFMgx7x7Tb4f5tiXiC7nr9peC6Q3DdKbjuEly/Elx3C65fC657sEuxS83O+f2OnD+Ic36wlPODcM7X4FhQ1hezrMNOcO1REmiMtaKR7cdtSvQtTej91fNMI6I9lt/78fzev8vv/QT1ntfe1a53dbZ2tDfrTe1tje36kup5LQ3N2u/zez+Z3/up/N5P57wDlneAegeWBMBbsfvLLpNnHWWyFpdJpVQm++MyeTGvIqiQDmWFBFv7tZdgzCGwIHRa+7ML/rIL/hcX/BWKY61AhKD2qgv+mgv+ugv+Rg7nYlNcsS3LVvlth8rvYpV3kVQehVX+QJ1PJO8SJu+9IO+HJHhFF8wJVbdF41G9A57Rf6jAj1TgxyrwExX4TxX4qQr8lwr8LAcGAAx0KFuPL2wN1zk0XI81LJY0LMQablCkAAl4GBMQbBtovhK62k3azl/oghe54MUueAnFrRrV0NTUEgy2d7a2BDXNBS91wctccL+NBxCu2O1VktN2SImobUUJ0vbjr5za/uMrvNtLnU8k7+FM3jEg70gSfDBvLmDZuzZKQkZLyC4SMkZCdpWQ3SRkdwnZw0YCFCmQt3/ZQo1zCDUBC/UXSag/Y6EmOuNGEh3BJIJ1e9p+JOAwFnCB2ZPWwUy8tr8SnaREK5VoQIlWKdFqJVqjRGttNABoAFDFPilbxikOGadhGX8nyfgYlvFAVRqQlEcyKWG/qTaLBB6DbXWInfZB+b1n5/c+OL93Q37vxvzeTfm9m/N7t+S8A7xug3egPwDectnMtcum1VE283HZ3CmVzS9x2bTnTRUqpKNYIcFOYG0RtCACy+rJO13woAve5YIvdsEPdcGXuOCHueCH53AuNsVllY+0VT7aoXIIq/wTSeUfY5XD6nQgeY9m8sIeGs0kwXftsKyP0EmejkQ0ngnsEwj2go0RrXsj/j0b8e/diH90I/7HbcR/2Ub8Yxvx7yP+uzN/Oqlm+cP3ECuEXFAJu6COpwUF9mAsi5DpErTh8HtfOafXzsEF1Z8/ZajAjmEFdhkU2AChDSFBsrGImaIWZbQTZGilDJ0oQyfJ0MkydIoMnSpDp2EooDY0c4at3FkO5VaVoLWmKyTllmPlzpViR2KFmFiwnUw7Dx4GsHWYJP+DFYpgjE5gfd8FP98Fv8AFv9AF/4ELfpEL/kMX/GKC75zDrVoZU0+3XWpre7lD2ytK0M7KiKTtUqztanVKkMDHMoFfh+BXQ3C2tYkMWqpZfdZ+6oJf44L/zAW/1oEzO0naz13w61zw613wGzhuDe8CDJflvcmW9xaHvLeVoG2VHVLrvBDLe4c6n0heg8kLlnS0u6CmtyUi2ZiRYkPSX8nQ3TL0axm6R4bulaH7ZOg3MnQ/gtwGxg/aij3sUGwNU6wEfGdKih2AFXtcih2JtZSJBWZZtCdgCN0JpqN5Mn8vIU9KyFMS8rSE/EFCnpGQP0rIszZiaSTXqrW2Ri84NHqRaUR3z+wrabQP1uhlZ9xIojCT6A0I+AoJWD4PFiBZSXzV4X7N4X7d4X7D4f6rw/2mw/2Ww/02d7sJ8q4tyHsOQT5ggtDljztLguyEBflIjBfJEWFyPAI15hN4W1iSs7jGEvlPJfqpEv2XEv1MiX6uRL9Qol8q0XUi6ibielvEbxwibmAiUvtNBZKI/16HRCzSFGlAUppMSjDvpZWQwEPAkGs2w0cEmgyVylCZDPllqFyGBsnQYBkagiGXYUmFltNruCbqNUKz9KK+f1/n1Ot9rNdoKXYkVjcT6yEQa4wG37SSZjhqgEUTmtBdFdhuCmx3BbaHAttTgY1VYLoCGydgLqpNsFXb26HaRKYa3af4J0m1/8OqTZLjR7L1MNkeBtkCEGNubKJVic5q0VkjOmtFZ53orBedk0XnFMEpKzHNVmKGQ4kDmRKDwPe3khKPYCVmC9EgEXqZCGCMT2sAvdi6IT5KalRgTQqsWYG1KLA5CmyuApunwFptLNDhNpqabyvW5lCsnSk2GHx/ISl2K1YsKMePZIsy2X4Lsi0mQYfPS8TS5NmsPiKR6LNq+qFqeIkaPkwNH66Gj1DDR6rho9Tw0QgOAOzyNIZsRQ2HomGmKBwp4rtSUvRHWNEeZSqQqMcxUbsgdJSELrcGsjSxxzncyxzumMPd53DHHe6Ew510uI/PuV1USduqZB2q9DNV4Fw+33clVb6DVVkpxovkWMbkgCO9tJNIsMHIWGaVdrKEnCIhp0rIaRJyuoScISFnSshZCHHZQLDKFuhsh0DnMoFgh4kvIwmUwgKd74wbSRRjEv0FAl5IAg5C1nC1HziBi5zAD53AxU7gEidwqRO4zAlcbgMuVecKW5mrHMqsZsrADknfsZIyx2BlrnHEjITp4y/S0D5dC4Ot9pY2fU40A/a5rQHfz5XodUr0enh0CfrITwT4hlzg1q6ulma9qSHYEtRuVKI3KdGblegtDA1YaICisoy32TLe7pDxDibjcPBdIMl4CJbxblWWkZZxpiUc2KDdo4GNOZhNg/kLmE3j77tuHve5efzGzeN+N48H3DwedPN4yM3jYeIx0jEf6fa2vcbW+VGHzo8znWEDmW+6pPNUrPOTLmlBUieY1N9A+Kch2uCCTv6iLDqfEZ1/FJ3Pis7nROda0fm86HyBOd3keNGW4yWHHC8zOUaA716SHOOxHK8JsSIRkkyE4VDf3qBDWBKKdpjszViBvanA3lJgbyuwdxTYuwrsbwrsPYZZ4wcXxT6wFfvQodhHTLGdwXeYpNhQrNincvxItuOZbGDhSfuMBN2Tjd7mJWALKrWOnoAzs0jta0ukTO1zD2G+8BDmSw9h1nkI85WHMOs9hPnaQ5hvSJixbCTNwwSsILQ9gEByg7vBLkdfqViORaXoM9LXXzrngb9CiFZautEE4gNFWbmCIS/NT6gTYPKl2jl5ABs+0GKHcq8BB3kNONhrwCFeAw71GrDCa8BhXgMOJwH3ppNlzikfy3p3vtWUI0pztWCkoxaMxrXgb1IteAfXgt08phVVhTSrChng71FKvyYwk1v89JUqbU8XfKwLrrvg41zw8S74BBd8r1J4u25Wb26xDlwJM7v/1qdVbtm/K2VEorD5QDgDgJn2F48IsO8KRwQ4N9LwUK19Rg89mEfciQOm5M2UHo0vq8TH0Mi2wfcWchgwrIqiGPlPtGvIfrSGwBEN1ml2k3ANef5LZ3v/HK4h1WpFUYXIsAoBZ7lptaVgFZDPkTXBoqu0VqcC61XgZBU4RQVOVYHTVOB0FTgDgwELlDU80NZwlkPD2VjDhyQNH8AaNilSgATMMgHh3HmtpdT5ljdHQuZKyDwJaZWQQyRkvoQs2GGek7bSjb4bt9ulu8hRukFcujdLpXsjLt0lThVR0S5nRQu7IrTDS8WzNqrovkBSGY9w8zjSzeMoN4+j3TyOcfMIuXkc6+ZhQJOApbVwWeCwLbDpELgHC3yZJPAlWOBlLglBOvcznd+F8H0k/C7isSvITGU8r28ir28yr+/xeX1TeX3TeX0zeX2zyJftzctnerPfLpUBR6msxKVyplQqp+NSOSVfmlDRrOA7KoF0GlSdtmialKE4/+WCn+GCn+mCn+WCf8cFX+WCf9cFP5vgwwEP5G9azrU1Ps+h8flY46SkcRxrfJE6GUjdAabuZGhgLibBhwWzqR4TDk/MWEeLaZco0UuV6GVK9HIl+iMleoUSvVKJXpVDAzYqi7naFvNqh5jXYDGPksQ8Aot5nSoNSMoTmJR3QuAbSuFTjHWszo3o903o983o9y3o963o922l/zutKK39AilyO/29oHGx6jSFO+yivosWNRw6ZZ2+fDcu6nlSUc/BRX2fHR8q4JWsgJ+AIPdDEHawzwPo94Po90Po98P0d2dDW0tQewT9XoN+/5b8XrYVDgjS00v1qTU1ldqjduSylo/bWj7h0PJJrOVkScs6rOUztghIyxOZlkXQ7jwL41tSUa1ljkyw51TgWhX4vAp8QQX+nwp8UQX+SQW+xEG6ENTtQKWXbd1ecej2GtZNl3TbE+v2piIFSMCTmIA7g4Bvl4J9Jjj/kDw35G1ce8cJvOsE/uYE3nMC7zuBD5zA353AhzkgQAFZnI9scT5xiPMpFmewJE45FucLR8xImJOZMEDV1tH+FsLB5uBgNpmk2y2/UsPr1fDXavgbNfxvNbxBDcNCGwXsw3CAw7KcRWU5OUvKRDlLy5CcX37hlPPzL5Ccg5SpQKKewkTdFWrbkLLcWTNzEjFqMGuoDFXI0DAZGi5DO8nQCBnaWYZG5qAAg2TBRtuCjXEIthsW7C1JsL9iwcZKsSOxTmVigUFdbRyEFNpubbwMTZChvWRobxnaR4YmytC+MrQfgqymTNCJXiZxsQr8ASoWtRMM5/L5q7FYf5TE+gMWq16KHYl1GhMLzmXQppSph81TXfBpLvh0F3yGC36AC36gCz7TBZ9V5m14P9uuhQ1UWHpIBZwf52/Cwv7mC+fs671Y2LnqZCB1T2fq/gGCt9qVlh/6dYgMzZehBWXbxSlbbWWKU7YWqsB2FdihAhcJoNshXUG7vBbT8oKj1vxgC9K/BJfX9dKD8HNcXkdK0iKL2NxyZ37z7mDs0jYHDvYuqdnPo8uQOXCweimb/bxGYF7Lmcdi5nVK5t0C8x7ODGPmfUrmkwLzac7sxsxnlMzXBOYbnBnFzDeVzE8F5mecGcPML5RMsGJmM8GQGWUmMBPMmcnM3QTmHpyZwsyxSma1wKzlzCxm1iuZTQKzhTNXYOZcJXOJwDycM1di5pFK5jKB2ceZJ2NmQsk8RWCexpmnYeYZSuZFAvNizjwTMy9VMq8TmDdw5irMvEnJvE9g3s+Z52Dmg0rmMwLzWc48DzPXKplvCsy3OfMCzHxXyfxCYK7jzIswc72SCZu40QEYJYx5CWbCbm6ZOVZgjuPMyzFzgpJZLzCncOaVmDlNyZwrMFs5czVmzlcyjxSYR3PmTzEzpGQmBObxnHktZqaVzDME5lmceT1mrlIyLxWYl3PmTWVWX0A3xVyhZN4kMG/hzFtxnLcpmQ8KzIc583bMXKNkrhWYL3DmnZj5opL5rsB8jzPvxswPlMz1AvMbzrwXMzcombCU32bCan7KvB8zYU2/zJwgMPfmzIcwc6KSOU1gzuDMNZh5oJI5X2C2ceZjmNmuZIYEpsGZT2BmWMlMC8wsZz6Fmf1K5iqBeTZnPoOZ5yqZVwjMqzjzOcxcrWTeJjBv58wXMPMOJXONwHyUM/+EmY8rmS8KzJc482XMfFnJ/EBgfsiZr2LmR0rmBoEJC3co8w3MhOU7MhNWetjMkZz5FmaOVjInCsz9OPNdzJykZB4oMGdx5vuYOVvJbBeYizjzQ8wMKplhgWly5seY2aNk9gvMAc78FDNXKpnnCszzOPNzzDxfyVwtMK/mzHWYeY2SeYfAvIszv8bMu5XMxwXmE5y5ATOfVDJfFpivcGahHzFfUzI/EpifcGYJZn6qZMLUls2E2S3KLMNMmOOSmaMF5hjOHISZuymZkwRmgDOHYma1kjlbYDZw5nDMbFIygwJzMWfujJlLOHMkQantAPJ/Bbz0kf88HCRxLA+Ze49UMGjIsOd7dnsOGfUcMuY5nQnP90x5DpnlIYuQbzFKRS7kCh6SoyoGDbnSc+wnew55mueQZ3oOucqz8ud4vud5nu95geeQF3kOeYnnkJd7ztGVnkOu9hz7Tz2HvNZzyOs9h7yJhyx1hNAKxH8Vt3rO++2eQ97pOeTdnnN0r+d73u/5ng95DrmGhxRm7RyhacjHPN/zCc85espzyGc8h3zOc8gXPOfoT57v+bLnkK96DvmG53S+5fme73oO+b7nkB96Dvmx55Cfeg75ueeQ6zyH/NpzyA2eQ8Ig1FvIEs8hyzyHHOQ55FDPIYd7Drmz15ClI+nosmALzngb5aenJ8Mf+KL0DMQw2l8gf1LZRQWOUYG7qsDdVODuKnAPFbinChwrgG5fecb5+SjcPwGELYOvZvRsO//efktb+pXnz587v/L86XP0lWc/RQrQJzmIxR7vQ0TWIbJ4vA/RSW8KFZO8lvagscU9kUhhYUVkVGF3xUjf0I5UNGweoO9VWz+jasaMSGFRxajCsSW9lm9pDi5mhKKJ1UuMVMqIZwbaEhEjBmuLqox0csXsZCoRyYYzrZFZU+unTZ1RQq+DGiIRPZPQw0YqI97bX0sDBBoXNDTN72zoaG0OdAYDwY72zq5AcEHrwrmBpoa2ls6GQLCL+NIoCiK9mUzygOpq4wRCnj5tctXyRFVf2gzHI1VxM1NdO7k+PKN7Su2UcHf35Mj0qcbSqd01M6bO6DaMcP20GXXVsPAbDCdZKU1bCZhSs2JKDblOrp08peq4ZM9EO0U6T5FOU6RbKdJpivTNSL/XWzsKZdNjivw/a5IULjFFAwA=" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="" />
</div>


<script type="text/javascript">
//<![CDATA[
var rootRel = '/';//]]>
</script>

<script src="/WebResource.axd?d=7_gNHu64AZzUXmccBsUVV8xEPunxrKBin_tdVFQPcENkPUDnnqLZEaoyXJnbnmcxreE_pkBxtrdjuqt33Xf7-zUKturmkeEnFOtPD4xHaEJoiu3k9Q9oGANeWxoxFH0bWBkvwTJh7bnh0yRscffI2e3nXLTvy5b0X7IPs9GlYIZ4hhflnZ1HDMNDZe9jfyPz7V60Ug2&amp;t=636022248582084348" type="text/javascript"></script>
<script src="/WebResource.axd?d=J-EELoUx3kpMgypNbusI6RQ_n0W9X2U3SgTKMaD1N5fSxMithCWCNvyeSlXxyJICdZABhFohKbV9NMFNZ4dsxTRClIUnG8Qwby9npGyoIMTJAk2dd1bc55yB31yZ0onfZmoQuDwHBO4YAc4rmUzo418VD1qcgRwfroO1E-Rc9yUJLn3Ijn94wQ5oQQHCzSUPWWRYuA2&amp;t=636022248582084348" type="text/javascript"></script>
<script src="/WebResource.axd?d=A9RIp8zGg4Gw3exV1N0P958fkZ6ohvRvEUkD3_gB8LX6Zzf_G1aQl9xnmRi3XfUHu-gvcho71LrRksQXwTl0u-6HYV7Wl_2m4s-isSjict3GYon2IjElokmjux_8634TvQq3hQwQAi8TRkjJGilD3w64aEZh2bjkLisT7jFAm-SIXCUMJ5Ebz0mmsvLIYKjVlQHXGA2&amp;t=636022248582084348" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
var rootRel = '/';//]]>
</script>

<script src="/ScriptResource.axd?d=jm-01pauvma80AkBIkGPGD24rMBGUxSY3rtoQ_08GUM7OAkDepTzH_5Ql_ZkNushkCLj2I6hGCvfgIEJaUfFAk7K0uzx6sNpbtRPLlFNZZLSrHDqwZWp9fv9cP_hieKO1p4IFGOaFiALRH6zR_7YDmpImXMJZ-EhT6Sg6xZEJ5hImu9l0HwOQhq7prk88Y_IrDmR6g2&amp;t=541915ef" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=juFyDJamL_2b2tVpF5ZPYx_RbuyWoH8haEeP1OntXGRaqnGbblJEUkvEiVbIdzp9HUn-Mhlu_l33mSSK1cj1--NYUG7WbPliIEEuI1DYOOc57QEn3SgaMRsWJNH4Sie5wuqQE5zYwwaG3cuxTp6yV147V97zLtIS8npLAueK1kfeUNthCIGaGYM75_LMnf0hK3aPwCVxQ8yD8qrWh2w7JGpbeUaebRkvWeoG4AuR37DUbvDv0foo6Wvv-lzTC6EgJjV24941-DIPZ6oChQhSbbo7bdela7dgPFkw2ZqLdgLlPIwJXIL4oS_LwPRSdbg59tO4fFns23emHBdVMZmPTnk4sVXja6U0mIrORbXiqXTk-ddNNweUzeMUHZXd0pXKNA1PRsUZ8lQVIFG5NtiBmu4jSqNHt3TNZ58OhkeAo-WgfdM9Bz49JGE3h49ZbIqjsgRAfJ345ObiF9JH76mLQi0e_BKJYzS_XsmA3O1IYldjdOI9mNJfHr_jVGwA9OsI3ArEs1AX6-92uJ7FkaIANxC3E4OEYxnZJ_AODhkWK9FakFvV5xz--BMGNtosIaDnG0aICG0wk6tB-lTNl-xSTZ5KCMJdimlCXQjxiXMyu9Y4SHQfOv5vTNeCSTAlOQ14PciiksDDNG105sIS0E_Vrwm7Cz01" type="text/javascript"></script>
<script src="WebServices/ShoppingCartService.asmx/js" type="text/javascript"></script>
<script src="WebServices/NewsLetterSignupService.asmx/js" type="text/javascript"></script>
<script src="WebServices/ReserveInStoreService.asmx/js" type="text/javascript"></script>
				
        
       	

		<div id="wrap" class="clearfix">	
			
<script>
    $( document ).ready(function() {
        //grid view
        $( "span.salepriceGrid:contains('$0.02')" ).css( "display", "none" );

        //list view
        $( "span.saleprice:contains('$0.02')" ).css( "display", "none" );

        //recently viewed
        $( "div.pricingContainer:contains('$0.02')" ).css( "display", "none" );

        //quick view
        $( "span.productSaleprice:contains('$0.02')" ).css( "display", "none" );
    });
</script>



<div id="headerContainer">
	<div id="header">
		<h1><a href="Default.aspx">Henrys - Photo - Video - Digital</a></h1>	
		<ul id="header-nav">
        	<li id="middle-storelocations"><a href="Store-Locations.aspx">Store Locations</a></li>
            <li id="middle-blog"><a href="http://blog.henrys.com">Henry's Blog</a></li>		
			<li id="header-nav-custcare"><a href="Customer-Care.aspx">Customer Care</a></li>
			<li id="header-nav-login"><a class="noauth" href="SignIn.aspx">login or register</a></li>
			<li id="header-nav-cart" class="last"><a class="lnkheadercart" href="ShoppingCart.aspx">Shopping Cart (0)</a></li>
		</ul>
		<div id="header-search"><input id="txtSearch" type="text" value="" /><a href="javascript:{}" id="btnSearch">Search</a></div>
	</div>
	<div id="header-menucontainer">		
		
				<ul id="header-menus">
					
				<li id="header_lstCategories_category_0" class="A">
					<a href="javascript:void(0);">Cameras &amp; Lenses</a>
					<div id="header_lstCategories_dropNav_0" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_0" class="drop-contentA">
								<h1>Cameras & Lenses</h1>
								<div class="col left"><h2>Cameras</h2><ul>    <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy.aspx">Point &amp; Shoot Cameras &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Small-And-amp-Easy.aspx/1/318%5eSmall+%2526+Easy">Small &amp; Easy</a></li>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Waterproof.aspx/1/318^Waterproof">Waterproof</a></li>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Super-Zoom.aspx/1/318^Super+Zoom">Super Zoom</a></li>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Advanced.aspx/1/318^Advanced">Advanced</a></li>        <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy-Wi-Fi.aspx/1/318^Wi-Fi">Wi-Fi</a></li>    </ul>    <p>    <a href="/Categories/67-Digital-Cameras-Compare-and-Buy.aspx"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Point &amp; Shoot Cameras</span></a>    </p>    </div>    </div>    </li>    <li><a href="/Categories/61-Digital-SLR-Cameras-Mirrorless-Camera.aspx/1/318^Mirrorless+Camera">Mirrorless Cameras &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/61-Digital-SLR-Cameras-Mirrorless-Camera-Stylish-And-amp-Sophisticated.aspx/1/318%5eMirrorless+Camera%2c%23319%5eStylish+%2526+Sophisticated">Stylish &amp; Sophisticated</a></li>        <li><a href="/Categories/61-Digital-SLR-Cameras-Mirrorless-Camera-Pro-Grade.aspx/1/318^Mirrorless+Camera%2c%23319^Pro+Grade">Pro Grade</a></li>    </ul>    <p>    <a href="/Categories/61-Digital-SLR-Cameras-Mirrorless-Camera.aspx/1/318^Mirrorless+Camera"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Mirrorless Cameras</span></a>    </p>    </div>    </div>    </li>    <li><a href="/Categories/61-Cameras-Compact-System-Camera-Digital-SLR.aspx/1/Categories%252f61-Digital-SLR-Cameras-Digital-SLR.aspx%252f1%252f318%5eDigital+SLR%2c%23318%5eDigital+SLR">Digital SLRs &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/61-Cameras-Compact-System-Camera-Digital-SLR-Hobbyist-And-amp-Beginner.aspx/1/Categories%252f61-Digital-SLR-Cameras-Digital-SLR.aspx%252f1%252f318%5eDigital+SLR%2c%23318%5eDigital+SLR%2c%23319%5eHobbyist+%2526+Beginner">Hobbyist &amp; Beginner</a></li>        <li><a href="/Categories/61-Cameras-Compact-System-Camera-Digital-SLR-Enthusiast.aspx/1/Categories%252f61-Digital-SLR-Cameras-Digital-SLR.aspx%252f1%252f318%5eDigital+SLR%2c%23318%5eDigital+SLR%2c%23319%5eEnthusiast">Enthusiast</a></li>        <li><a href="/Categories/61-Digital-SLR-Cameras-Digital-SLR-Hobbyist-And-amp-Beginner-Professional-Grade.aspx/1/318^Digital+SLR%2c%23319^Professional+Grade">Pro Grade</a></li>    </ul>    <p>    <a href="/Categories/61-Cameras-Compact-System-Camera-Digital-SLR.aspx/1/Categories%252f61-Digital-SLR-Cameras-Digital-SLR.aspx%252f1%252f318%5eDigital+SLR%2c%23318%5eDigital+SLR"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Digital SLRs</span></a>    </p>    </div>    </div>    </li>    <li><a href="/Categories/673-Action-Cameras.aspx">Action Cameras</a></li>    <li><a href="/Categories/171-Medium-Format.aspx">Medium Format &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/210-645-BODY.aspx"><span>Digital Medium Format</span><br />        Digital capture at its finest. </a></li>        <li><a href="/search/xmedfilm.aspx"><span>Film Medium Format</span><br />        Explore with a 645, 6x6 or 6x7 camera!</a></li>    </ul>    <p>    <a href="/Categories/171-Medium-Format.aspx"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Medium Format Cameras &amp; Accessories</span></a>    </p>    </div>    </div>    </li></ul><h2>Lenses</h2><ul>    <li><a href="/Categories/60-Cameras-Mirrorless-Lenses.aspx/1/3AA^Mirrorless+Lenses">For Mirrorless Cameras &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Wide-Angle-Zoom-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^656">Wide-Angle Zoom Lenses</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Mid-Range-Zoom-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^653">Mid-Range Zoom</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Telephoto-Zoom-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^655">Telephoto Zoom</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Prime-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^654">Prime Lens</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Macro-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^659">Macro Lens</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Conversion-And-Creative-Lenses.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^657">Conversion &amp; Creative Lenses</a></li>        <li><a href="/Categories/60-Cameras-Mirrorless-Lenses-Mount-Adapters.aspx/1/3AA^Mirrorless+Lenses%2c%2380000000^615">Mount Adapters</a></li>    </ul>    <p>    <a href="/Categories/60-Cameras-Mirrorless-Lenses.aspx/1/3AA^Mirrorless+Lenses"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Mirrorless Lenses</span></a>    </p>    </div>    </div>    </li>    <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses.aspx/1/3AA^Digital+SLR+Lenses">For Digital SLRs &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Wide-Angle-Zoom-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^656">Wide Angle Zoom</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Mid-Range-Zoom-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^653">Mid-Range Zoom</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Telephoto-Zoom-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^655">Telephoto Zoom</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Prime-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^654">Prime Lens</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Macro-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^659">Macro Lens</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Special-Application-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^658">Special Application Lenses</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Conversion-And-Creative-Lenses.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^657">Conversion &amp; Creative Lenses</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Tele-Converters.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^540">Tele Converter</a></li>        <li><a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses-Mount-Adapters.aspx/1/3AA^Digital+SLR+Lenses%2c%2380000000^615">Mount Adapters</a></li>    </ul>    <p>    <a href="/Categories/73-Camera-Lenses-Digital-SLR-Lenses.aspx/1/3AA^Digital+SLR+Lenses"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Digital SLR Lenses</span></a>    </p>    </div>    </div>    </li>    <li><a href="/search/xmedforlens.aspx">For Medium Format</a></li></ul><br /><a onclick="_gaq.push(['_trackEvent','Nav: Camera &amp; Lenses - Bottom','Click','Nikon D750 160630']);" href="http://www.henrys.com/87442-NIKON-D750-D-SLR-BODY.aspx"><img height="100" width="435" alt="Nikon D750 DSLR" src="//www.henrys.com/Images/Nav-PCAs/Nikon-D750-btm-160630.jpg" style="margin-top: 45px;" /></a></div><div class="col right"><h2>Shop by Lens Type</h2><ul>    <li><a href="/Categories/656-Wide-Angle-Zoom-Lenses.aspx">Wide Angle Zoom</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E653">Mid-Range Zoom</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E655">Telephoto Zoom</a></li>    <li><a href="/Categories/654-Prime-Lenses.aspx">Prime Lens</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E659">Macro Lens</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E658">Special Application Lenses</a></li>    <li><a href="/search/lens.aspx/1/80000000%5E73%2c%2380000000%5E657">Conversion &amp; Creative Lenses</a></li>    <li><a href="/Categories/540-Tele-Converters.aspx">Tele Converter</a></li>    <li><a href="/Categories/615-Mount-Adapters.aspx">Mount Adapters</a></li></ul><ul>    <li> </li>    <li><a href="/New-Photo-Video-Gear.aspx" style="text-decoration: underline; color: #336699 !important;">View our Selection of Pre-order Cameras &amp; Lenses</a></li></ul></div>
							</div>
							<div id="header_lstCategories_dropProds_0" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="87415-FUJIFILM-X100T-SILVER-16MP-23MMF2-APS-C.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products87415-75x75-1864539169.jpg' alt="FUJIFILM X100T SILVER 16MP 23MMF2 APS-C " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="87415-FUJIFILM-X100T-SILVER-16MP-23MMF2-APS-C.aspx" title="">
                                                        FUJIFILM X100T SILVER 16MP 23M...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $1,499.99</span><br />
                                                        <span class="yousave" >Save: $100.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="57903-HASSELBLAD-EXT-TUBE-16E--3040654.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products57903-75x75-797274.jpg' alt="HASSELBLAD EXT TUBE 16E       3040654 " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="57903-HASSELBLAD-EXT-TUBE-16E--3040654.aspx" title="">
                                                        HASSELBLAD EXT TUBE 16E       ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $374.50</span><br />
                                                        <span class="yousave" >Save: $74.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="85177-LENSBABY-5-8MM-CIRCULAR-FISHEYE-NIKON.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products85177-75x75-134683687.jpg' alt="LENSBABY 5.8MM CIRCULAR FISHEYE NIKON " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="85177-LENSBABY-5-8MM-CIRCULAR-FISHEYE-NIKON.aspx" title="">
                                                        LENSBABY 5.8MM CIRCULAR FISHEY...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $319.99</span><br />
                                                        <span class="yousave" >Save: $120.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="89520-SIGMA-150-600MM-F5-6-3-C-DG-OS-CANON.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products89520-75x75-1234542.jpg' alt="SIGMA 150-600MM F5-6.3 C DG OS CANON " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="89520-SIGMA-150-600MM-F5-6-3-C-DG-OS-CANON.aspx" title="">
                                                        SIGMA 150-600MM F5-6.3 C DG OS...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $1,299.99</span><br />
                                                        <span class="yousave" >Save: $270.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_0" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Camera and Lenses - Right','Click','160630 - Canon T6i With 18-55mm Lens']);" href="/89368-CANON-EOS-REBEL-T6I-W-18-55-IS-STM-LENS.aspx"> <img height="380" width="185" alt="Canon T6i With 18-55mm Lens" src="//www.henrys.com/Images/Nav-PCAs/Canon-T6i-160630.png" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_1" class="B">
					<a href="javascript:void(0);">Drones</a>
					<div id="header_lstCategories_dropNav_1" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_1" class="drop-contentB">
								<h1>Drones</h1>
								<div class="col left"><h2>Drones / UAVs</h2><ul>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-3DR.aspx/1/80000000^754%2c%2380000010^3DR">3DR Drones</a></li>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-DJI.aspx/1/80000000^754%2c%2380000010^DJI">DJI Drones</a></li>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-OMINUS.aspx/1/80000000%5E754%2c%2380000010%5EOMINUS">Ominus Drones</a></li>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-PARROT.aspx/1/80000000^754%2c%2380000010^PARROT">Parrot Drones</a></li>    <li><a href="/Categories/754-Aerial-Imaging-Platforms-and-Drones-VISTA.aspx/1/80000010^VISTA">Vista Drones</a></li></ul><p><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-Aerial-Imaging-Platforms-and-Drones.aspx/1/80000000^754" style="text-decoration: underline; color: #336699 !important;">View all Drones / UAVs</a></p></div><div style="float: none; display: inline-block; padding-left: 20px;" class="col right"><h2>Learning</h2><ul>    <li><a target="_blank" href="/Droneography.aspx">Getting Started</a></li>    <li><a target="_blank" href="http://www.schoolofimaging.ca/search/Flying%20Cameras.aspx">In-Person Classes</a></li>    <li><a target="_blank" href="https://www.facebook.com/Henrys.Drones">Facebook: Henry's Drone/UAV Community</a></li></ul></div><div class="col right"><h2>Drone Accessories</h2><ul>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-Drone-Accessories.aspx/1/80000000^752">Accessories</a></li>    <li><a href="/Categories/81-Filters-Filters-for-Drones.aspx/1/80000000%5E757">Filters for Drones</a></li>    <li><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-Drone-Cases-and-Bags.aspx/1/80000000^755">Cases &amp; Bags for Drones</a></li></ul><p><a href="/Categories/750-Drones-Aerial-Imaging-Platforms-and-Drones-Drone-Accessories.aspx/1/80000000^752" style="text-decoration: underline; color: #336699 !important;">View all Drone Accessories</a></p></div><div style="clear: both;"></div><br /><a href="/93480-DJI-PHANTOM-4-DRONE.aspx" onclick="_gaq.push(['_trackEvent','Nav: Drones - Bottom','Click','DJI Phantom 4 Drone']);"><img width="435" height="100" style="margin-top: 100px; text-align: left;" src="//www.henrys.com/Images/Nav-PCAs/160606-Bottom-DJI-Phantom-4.gif" alt="DJI Phantom 4 Drone" /></a>
							</div>
							
							<div id="header_lstCategories_dropPCARight_1" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Drones - Right','Click','3DR Solo Drone']);" href="http://www.henrys.com/90830-3DR-SOLO-AERIAL-DRONE.aspx"><img height="380" width="185" alt="3DR Solo Drone" src="//www.henrys.com/Images/Nav-PCAs/160606-right-3DR-Solo-Drone.jpg" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_2" class="A">
					<a href="javascript:void(0);">Video</a>
					<div id="header_lstCategories_dropNav_2" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_2" class="drop-contentA">
								<h1>Video</h1>
								<div class="col left"><h2>Video Cameras</h2><ul>    <li><a href="/Categories/673-Action-Cameras.aspx">GoPro and Action Cameras</a></li>    <li><a href="/Categories/65-Digital-Camcorders.aspx">Camcorders &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>					<!--<li><a href="/Categories/65-Digital-Camcorders-Camcorders.aspx/1/80000000^66%2c%232F2^Pocketable">Pocketable</a></li>            <li><a href="/Categories/65-Digital-Camcorders-Camcorders.aspx/1/80000000^66%2c%232F2^Everyday+Use">Everyday Use</a></li>            <li><a href="/Categories/65-Digital-Camcorders.aspx/1/2F2%5eEnthusiast+%2526+Professional">Enthusiast and Advanced</a></li>-->        <li><a href="/Categories/112-Pro-Video-Cameras.aspx">Professional</a></li>    </ul>    <p>					<a href="/Categories/65-Digital-Camcorders.aspx"><span class="nav-ViewAll" style="text-decoration: underline; color: #336699;">View All Camcorders</span></a>				</p>    </div>    </div>    </li>    <li><a href="/Categories/112-Pro-Video-Cameras.aspx">Professional Video</a></li>    <li><a href="/Categories/61-Digital-SLR-Cameras.aspx">Digital SLRs &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <p>					<a href="/Categories/61-Digital-SLR-Cameras.aspx">Video isn't just for camcorders anymore.<br />    <br />    <span class="nav-ViewAll" style="color: #336699;">Check out our selection of Digital SLRs!</span></a>				</p>    </div>    </div>    </li>    <li><a href="/Categories/750-Drones.aspx">Drones &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/750-Drones.aspx">Drones</a></li>        <li><a href="/Categories/752-Drone-Accessories.aspx">Drone Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/747-Dash-Cams.aspx">Dash Cams</a></li></ul></div><div class="col right"><h2>Accessories</h2><ul>    <li><a>Bags &amp; Cases &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/661-Camcorder-Bags.aspx">Camcorder Bags</a></li>        <li><a href="/Categories/664-Shoulder-Bags.aspx">Shoulder Bags</a></li>        <li><a href="/Categories/660-Backpacks-And-Slings.aspx">Backpacks &amp; Slings</a></li>        <li><a href="/Categories/479-Roller-Cases.aspx">Roller Cases</a></li>        <li><a href="/Categories/695-Hard-Waterproof-Cases-And-Accessories.aspx">Hard/Waterproof Cases &amp; Accessories</a></li>        <li><a href="/Categories/696-Camera-Housings-Covers-And-More.aspx">Camera, Housings, Covers &amp; More</a></li>    </ul>    </div>    </div>    </li>    <li><a>Video Lighting &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/256-Video-Lights.aspx">Video Lighting</a></li>        <li><a href="/Categories/309-Video-Lights-Acc-.aspx">Video Lighting Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a>Supports &amp; Tripods &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/497-Video-Tripod-Combo-w-Head.aspx">Complete Video Tripods</a></li>        <li><a href="/Categories/498-Video-Tripod-Heads.aspx">Video Tripod Heads</a></li>        <li><a href="/search/xportablesupport.aspx">Stabilizers, Shoulder Brackets, Rigs &amp; More</a></li>        <li><a href="/search/xfollowfocus.aspx">Follow Focuses<br />        Systems, Gears, Rings and More.</a></li>        <li><a href="/search/xjibrailslide.aspx">Jibs, Rails, Sliders and Dollies</a></li>    </ul>    </div>    </div>    </li>    <li><a>Audio &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/119-Microphones.aspx">Microphones and Mounts</a></li>        <li><a href="/Categories/669-Headphones.aspx">Headphones</a></li>        <li><a href="/search/xwindscreens.aspx">Windscreens</a></li>        <li><a href="/search/xboompoles.aspx">Boom Poles</a></li>        <li><a href="/search/xsoundblimps.aspx">Sound Blimps</a></li>        <li><a href="/Categories/672-Mixers.aspx">Mixers</a></li>        <li><a href="/Categories/128-Audio-Mobile-Office.aspx">Recorders </a></li>        <li><a href="/search/xxlrsaccess.aspx">XLR Adapters, Audio Cables and More </a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xpovadons.aspx">Action Camera Add-Ons &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xpovbatorcharg.aspx">Batteries and Chargers</a></li>        <li><a href="/Categories/756-Filters-for-Action-Cameras.aspx">Filters</a></li>        <li><a href="/search/xhousingandcase.aspx">Housings and Cases</a></li>        <li><a href="/search/xpovstrapormount.aspx">Straps and Mounts</a></li>        <li><a href="/search/xpovaccessory.aspx">Other Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a>Lenses &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/240-Video-Lenses.aspx">Conversion Lenses</a></li>        <li><a href="/search/Pro%20Video%20Lenses.aspx">Pro Video Lenses</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/671-Monitors.aspx">Monitors &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/671-Monitors.aspx">Monitors</a></li>        <li><a href="/search/xshadenmount.aspx">Shades and Mounts</a></li>    </ul>    </div>    </div>    </li>    <li><a>Batteries &amp; Chargers &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/383-Video-Batteries.aspx">Camcorder Batteries</a></li>        <li><a href="/Categories/375-Video-Battery-Acc-.aspx/1">Camcorder Chargers and Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/246-Video-Cables-And-Acc-.aspx">Video Cables</a></li>    <li><a href="/Categories/123-Video-Editing.aspx">Editing Software</a></li></ul></div><br /><br /><a href="/91882-RICOH-THETA-S-BLACK.aspx" onclick="_gaq.push(['_trackEvent','Nav: Video - Bottom','Click','Ricoh Theta S']);"><img width="435" height="100" src="//www.henrys.com/Images/Nav-PCAs/160606-bottom-Ricoh-Theta-S.gif" alt="Ricoh Theta S" style="margin-top: 45px;" /></a>
							</div>
							<div id="header_lstCategories_dropProds_2" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="82424-ZOOM-IQ5-BLACK-PRO-STEREO-MICROPHONE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products82424-75x75-962182.jpg' alt="ZOOM IQ5 BLACK PRO STEREO MICROPHONE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="82424-ZOOM-IQ5-BLACK-PRO-STEREO-MICROPHONE.aspx" title="">
                                                        ZOOM IQ5 BLACK PRO STEREO MICR...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $89.50</span><br />
                                                        <span class="yousave" >Save: $30.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="87350-COMODO-M7-PRO-7-HIGH-DEFINITION-FIELD-MONITOR.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products87350-75x75--258576949.jpg' alt="COMODO M7 PRO 7&quot; HIGH DEFINITION FIELD MONITOR " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="87350-COMODO-M7-PRO-7-HIGH-DEFINITION-FIELD-MONITOR.aspx" title="">
                                                        COMODO M7 PRO 7&quot; HIGH DEF...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $599.50</span><br />
                                                        <span class="yousave" >Save: $161.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="87924-MANFROTTO-MHX-PRO-2WAY-FLUID-HEAD.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products87924-75x75-1091567.jpg' alt="MANFROTTO MHX-PRO 2WAY FLUID HEAD " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="87924-MANFROTTO-MHX-PRO-2WAY-FLUID-HEAD.aspx" title="">
                                                        MANFROTTO MHX-PRO 2WAY FLUID H...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $184.99</span><br />
                                                        <span class="yousave" >Save: $45.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="93484-CANON-VIXIA-HF-R70-CAMCORDER-BUNDLE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products93484-75x75--259936441.jpg' alt="CANON VIXIA HF R70 CAMCORDER BUNDLE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="93484-CANON-VIXIA-HF-R70-CAMCORDER-BUNDLE.aspx" title="">
                                                        CANON VIXIA HF R70 CAMCORDER B...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $529.99</span><br />
                                                        
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_2" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Video - Right','Click','GoPro HERO4 Silver - 160630']);" href="/87701-GOPRO-HIGH-DEFINITION-HERO4-12MP-SILVER.aspx"><img height="380" width="185" alt="GoPro HERO4 Silver" src="//www.henrys.com/Images/Nav-PCAs/GoPro-Hero4-Silver-160630.png" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_3" class="A">
					<a href="javascript:void(0);">Accessories</a>
					<div id="header_lstCategories_dropNav_3" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_3" class="drop-contentA">
								<h1>Accessories</h1>
								<div class="col left"><h2>Essential Accessories</h2><ul>    <li><a href="/Categories/83-Bags.aspx">Bags, Cases &amp; More &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/663-Pouches-for-Point-And-Shoots.aspx">Pouches for Point-and-Shoots</a></li>        <li><a href="/Categories/664-Shoulder-Bags.aspx">Shoulder Bags</a></li>        <li><a href="/Categories/660-Backpacks-And-Slings.aspx">Backpacks &amp; Slings</a></li>        <li><a href="/Categories/661-Camcorder-Bags.aspx">Camcorder Cases</a></li>        <li><a href="/search/xcarryingccase.aspx">Carrying Cases</a></li>        <li><a href="/Categories/83-Bags-Camera-Housings-Covers-And-More.aspx/1/80000000^696">Camera Housings, Covers &amp; More</a></li>        <li><a href="/Categories/83-Bags-Hard-Waterproof-Cases-And-Accessories.aspx/1/80000000^695">Hard/Waterproof Cases &amp; Accessories</a></li>        <li><a href="/Categories/83-Bags-Computer-Bags.aspx/1/80000000^454">Computer Bags</a></li>        <li><a href="/Categories/83-Bags-Lens-Cases-Pouches.aspx/1/80000000%5E224 ">Lens Cases/Pouches</a></li>        <li><a href="/Categories/83-Bags-Modular-Cases.aspx/1/80000000^453">Modular Cases</a></li>        <li><a href="/Categories/83-Bags-Roller-Cases.aspx/1/80000000^479">Roller Cases</a></li>        <li><a href="/Categories/83-Bags-Specialty-Bags.aspx/1/80000000^448">Specialty Bags</a></li>    </ul>    <p>    <a href="/Categories/83-Bags.aspx"><span style="text-decoration: underline; color: #336699;" class="nav-ViewAll">View All Bags &amp; Cases</span></a>    </p>    </div>    </div>    </li>    <li><a href="/Categories/113-Memory.aspx">Memory Cards &amp; Media &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/272-Memory-Cards.aspx">Memory Cards</a></li>        <li><a href="/Categories/296-Memory-Card-Readers.aspx">Memory Card Readers</a></li>        <li><a href="/Categories/303-Memory-Acc-.aspx">Memory Card Storage &amp; More</a></li>        <li><a href="/Categories/293-Portable-Storage.aspx">External Harddrives</a></li>        <li><a href="/Categories/300-USB-Drives.aspx">USB Drives</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/52-Tripods.aspx">Tripods, Monopods, Supports &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/442-Table-Mini-Pods.aspx">Tabletop &amp; Mini Tripods</a></li>        <li><a href="/Categories/374-PHOTO-TRIPOD-HEADS.aspx">Tripod Heads</a></li>        <li><a href="/Categories/443-Tripod-Legs-Only.aspx">Tripod Legs</a></li>        <li><a href="/Categories/378-PHOTO-TRIPOD-COMBO-W-HEAD.aspx?video=grid">Complete Tripods</a></li>        <li><a href="/Categories/498-Video-Tripod-Heads.aspx">Video Heads</a></li>        <li><a href="/Categories/497-Video-Tripod-Combo-w-Head.aspx">Complete Video Tripods</a></li>        <li><a href="/search/xportablesupport.aspx">Stabilizers, Shoulder Brackets, Rigs &amp; More</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/222-Camera-Cleaning.aspx">Cleaning Supplies</a></li>    <li><a href="/Categories/81-Filters.aspx">Filters &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/81-Filters.aspx">Filters</a></li>        <li><a href="/Categories/235-Stepping-Rings.aspx">Stepping Rings</a></li>        <li><a href="/Categories/254-Filter-acc-And-adapters.aspx">Filter Accessories &amp; Adapters</a></li>    </ul>    </div>    </div>    </li></ul><h2>Camera Accessories</h2><ul>    <li><a href="/Categories/227-Batteries.aspx">Batteries &amp; Power &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/227-Batteries.aspx">Batteries</a></li>        <li><a href="/Categories/245-Rechargeable-Batteries.aspx">Rechargeable Batteries</a></li>        <li><a href="/Categories/231-Digital-Camera-Batteries.aspx">Camera Batteries </a></li>        <li><a href="/Categories/383-Video-Batteries.aspx">Camcorder Batteries</a></li>    </ul>    </div>    </div>    </li>    <li><a>Flashes, Modifiers &amp; More &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/376-Flash.aspx">External Flash Units</a></li>        <li><a href="/search/xexternallitemod.aspx">Light Modifiers &amp; Diffusers</a></li>        <li><a href="/search/xexternalbattery.aspx">Battery Packs &amp; More</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xvieworipiece.aspx">Viewfinders &amp; Eyepieces</a></li>    <li><a href="/search/xscreenorloop.aspx">Screen Protectors &amp; Loops</a></li>    <li><a href="/Categories/169-Camera-Straps.aspx">Camera Straps</a></li>    <li><a href="/search/xphotoglove.aspx">Photo Gloves</a></li>    <li><a href="/search/xtriggerorrelease.aspx">Triggers &amp; Releases</a></li>    <li><a href="/search/xsynccablemore.aspx">Sync Cords, Adapters &amp; More </a></li>    <li><a href="/Categories/184-Books.aspx">Books &amp; DVDs</a></li>    <li><a href="/Categories/69-Camera-Accessories-Travel-Accessories.aspx/1/80000000%5e739">Travel Accessories</a></li></ul></div><div class="col right"><h2>Lens Accessories</h2><ul>    <li><a href="/Categories/221-Filters.aspx">Filters &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/221-Filters.aspx">Filters</a></li>        <li><a href="/Categories/235-Stepping-Rings.aspx">Stepping Rings</a></li>        <li><a href="/Categories/254-Filter-acc-And-adapters.aspx">Filter Accessories &amp; Adapters</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/224-Lens-Cases-pouches.aspx">Pouches</a></li>    <li><a href="/Categories/234-Lens-Caps.aspx">Lens Caps</a></li>    <li><a href="/Categories/168-Lens-Hoods.aspx">Lens Hoods</a></li></ul><h2>Mobile Accessories</h2><ul>    <li><a href="/Categories/729-Mobile-Mobile-Audio-Accessories.aspx/1/80000000%5e730">Audio Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e730">HTC</a></li>        <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e730">HTC One</a></li>-->        <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e730">iPhone</a></li>        <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e730">LG</a></li>-->        <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e730">Samsung</a></li>        <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e730">Samsung Galaxy</a></li>-->    </ul>        <p>    <a href="/Categories/729-Mobile-Mobile-Audio-Accessories.aspx/1/80000000%5e730" style="color: #336699 ! important; text-decoration: underline;">View All Audio Accessories</a>    </p>        </div>        </div>        </li>        <li><a href="/Categories/731-Mobile-Cables-And-Chargers.aspx">Cables &amp; Chargers &rsaquo;</a>        <div class="nav-marketing-flyout">        <div class="flyout-container">        <h3>Narrow Your Search:</h3>        <ul>        <!--<li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e731">HTC</a></li>-->        <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e731">HTC One</a></li>-->            <li><a href="/search/xkiphone.aspx/1/80000000%5e729%2c%2380000000%5e731">iPhone</a></li>            <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e731">LG</a></li>-->            <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e731">Samsung</a></li>            <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e731">Samsung Galaxy</a></li>-->    </ul>            <p>    <a href="/Categories/731-Mobile-Cables-And-Chargers.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Cables &amp; Chargers</a>    </p>            </div>            </div>            </li>            <li><a href="/Categories/732-Mobile-Cases.aspx">Cases &amp; Screen Protectors &rsaquo;</a>            <div class="nav-marketing-flyout">            <div class="flyout-container">            <h3>Narrow Your Search:</h3>            <ul>                <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e732">HTC</a></li>                <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e732">HTC One</a></li>-->                <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e732">iPhone</a></li>                <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e732">LG</a></li>-->                <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e732">Samsung</a></li>                <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e732">Samsung Galaxy</a></li>-->    </ul>                <p>    <a href="/Categories/732-Mobile-Cases.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Cases &amp; Screen Protectors</a>    </p>                </div>                </div>                </li>                <li><a href="/Categories/734-Mobile-Lenses-And-Filters.aspx">Lenses &amp; Filters</a></li>                <li><a href="/Categories/736-Mobile-Lighting-Accessories.aspx">Lighting Accessories &rsaquo;</a>                <div class="nav-marketing-flyout">                <div class="flyout-container">                <h3>Narrow Your Search:</h3>                <ul>                    <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e736">HTC</a></li>                    <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e736">HTC One</a></li>-->                    <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e736">iPhone</a></li>                    <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e736">LG</a></li>-->                    <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e736">Samsung</a></li>                    <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e736">Samsung Galaxy</a></li>-->    </ul>                    <p>    <a href="/Categories/736-Mobile-Lighting-Accessories.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Lighting Accessories</a>    </p>                    </div>                    </div>                    </li>                    <li><a href="/Categories/733-Mobile-Remotes-And-Triggers.aspx">Remotes &amp; Triggers &rsaquo;</a>                    <div class="nav-marketing-flyout">                    <div class="flyout-container">                    <h3>Narrow Your Search:</h3>                    <ul>                        <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e733">HTC</a></li>                        <!--<li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e733">HTC One</a></li>-->                        <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e733">iPhone</a></li>                        <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e733">LG</a></li>-->                        <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e733">Samsung</a></li>                        <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e733">Samsung Galaxy</a></li>-->    </ul>                        <p>    <a href="/Categories/733-Mobile-Remotes-And-Triggers.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Remotes &amp; Triggers</a>    </p>                        </div>                        </div>                        </li>                        <li><a href="/Categories/735-Stabilizers-And-Mounts.aspx">Stabilizers &amp; Mounts &rsaquo;</a>                        <div class="nav-marketing-flyout">                        <div class="flyout-container">                        <h3>Narrow Your Search:</h3>                        <ul>                            <li><a href="/search/XKHTC.aspx/1/80000000%5e729%2c%2380000000%5e735">HTC</a></li>                            <!-- <li><a href="/search/XKONE.aspx/1/80000000%5e729%2c%2380000000%5e735">HTC One</a></li>-->                            <li><a href="/search/XKIPHONE.aspx/1/80000000%5e729%2c%2380000000%5e735">iPhone</a></li>                            <!--<li><a href="/search/XKLG.aspx/1/80000000%5e729%2c%2380000000%5e735">LG</a></li>-->                            <li><a href="/search/XKSAMSUNG.aspx/1/80000000%5e729%2c%2380000000%5e735">Samsung</a></li>                            <!--<li><a href="/search/XKGALAXY.aspx/1/80000000%5e729%2c%2380000000%5e735">Samsung Galaxy</a></li>-->    </ul>                            <p>    <a href="/Categories/735-Stabilizers-And-Mounts.aspx" style="color: #336699 ! important; text-decoration: underline;">View All Stabilizers &amp; Mounts</a>    </p>                            </div>                            </div>                            </li>                            <li><a href="/search/xtablet.aspx/1/80000000%5e51%2c%2380000000%5e83">Tablet Bags &amp; Cases</a></li>                            <li><a href="/search/XKIPAD.aspx">iPad Accessories</a></li>                            <li><a href="/Categories/737-Miscellaneous-Mobile-Accessories.aspx">More Mobile Accessories</a></li>                        </ul>                        <h2>Shop For Gifts</h2>                        <ul>                            <li><a href="/Gift-Cards.aspx">Gift Cards</a></li>                        </ul>                        </div>
							</div>
							<div id="header_lstCategories_dropProds_3" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="77025-PORTABRACE-LP-LED2-LITE-PANEL-CASE-BLUE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products77025-75x75-1027009.jpg' alt="PORTABRACE LP-LED2 LITE PANEL CASE BLUE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="77025-PORTABRACE-LP-LED2-LITE-PANEL-CASE-BLUE.aspx" title="">
                                                        PORTABRACE LP-LED2 LITE PANEL ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $219.50</span><br />
                                                        <span class="yousave" >Save: $80.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="63240-AQUATECH-SOFT-PAD-TRIPOD-WRAP-2-SET-ASPT.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products63240-75x75-490401.jpg' alt="AQUATECH SOFT PAD TRIPOD WRAP 2 SET ASPT " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="63240-AQUATECH-SOFT-PAD-TRIPOD-WRAP-2-SET-ASPT.aspx" title="">
                                                        AQUATECH SOFT PAD TRIPOD WRAP ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $29.50</span><br />
                                                        <span class="yousave" >Save: $10.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="73819-BELLEKS-CAP-BUCKLE-43-52-55MMCAP-HOLDE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products73819-75x75-820300.jpg' alt="BELLEKS CAP BUCKLE (43,52,55MM)CAP HOLDE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="73819-BELLEKS-CAP-BUCKLE-43-52-55MMCAP-HOLDE.aspx" title="">
                                                        BELLEKS CAP BUCKLE (43,52,55MM...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $8.50</span><br />
                                                        <span class="yousave" >Save: $1.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="60121-LEXAR-32GB-ECHO-SE-BACKUP-DRIVE.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products60121-75x75-747891.jpg' alt="LEXAR 32GB ECHO SE BACKUP DRIVE " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="60121-LEXAR-32GB-ECHO-SE-BACKUP-DRIVE.aspx" title="">
                                                        LEXAR 32GB ECHO SE BACKUP DRIV...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $59.50</span><br />
                                                        <span class="yousave" >Save: $40.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_3" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a href="/search/XCAMERONVARNDFILTERS.aspx" onclick="_gaq.push(['_trackEvent','Nav: Accessories - Right','Click','Cameron Variable and ND Filters']);"> <img height="380" width="185" src="//www.henrys.com/Images/Nav-PCAs/160606-right-Cameron-Variable-ND-Filters.jpg" alt="Cameron Variable and ND Filters" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_4" class="A">
					<a href="javascript:void(0);">Lighting &amp; Studio</a>
					<div id="header_lstCategories_dropNav_4" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_4" class="drop-contentA">
								<h1>Lighting & Studio</h1>
								<div class="col left"><h2>Lights</h2><ul>    <li><a href="/Categories/211-Studio-Lights.aspx">Studio Strobes</a></li>    <li><a href="/Categories/748-LED-Lighting.aspx">LED Lighting</a></li>    <li><a href="/Categories/749-Continuous-Lighting.aspx">Continuous Lighting</a></li></ul><h2>Lighting Accessories</h2><ul>    <li><a>Bulbs &amp; Lamps &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul style="padding-top: 15px;">        <li><a href="/Categories/199-FLASHTUBES.aspx">Flash Tubes</a></li>        <li><a href="/Categories/239-Modeling-Lamps.aspx">Modeling Lamps</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/212-Light-Meters.aspx">Light Meters</a></li>    <li><a href="/search/xlightmodifier.aspx">Light Modifiers &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xsoftbox.aspx"><span>Softboxes</span><br />        Soften and defuse the light!</a></li>        <li><a href="/search/xumbrella.aspx"><span>Umbrellas</span><br />        Cast a broad light!</a></li>        <li><a href="/search/xbeautydish.aspx"><span>Beauty Dishes</span><br />        Achieve beautiful portraits!</a></li>        <li><a href="/search/xeggcrate.aspx"><span>Egg Crate &amp; Honeycomb Grids</span><br />        Take control and avoid the light spill!</a></li>        <li><a href="/search/xsnoot.aspx"><span>Snoots</span><br />        Control the radius of your light!</a></li>    </ul>    <p><a href="/search/xlightmodifier.aspx"><span style="text-decoration: underline; color: #336699;" class="nav-ViewAll">View All Light Modifiers</span></a></p>    </div>    </div>    </li>    <li><a href="/search/xaccesslightmod.aspx">Light Modifier Accessories</a></li>    <li><a href="/search/xexternalpack.aspx">Battery Packs</a></li>    <li><a href="/search/xaddstudioaccess.aspx">Additional Accessories</a></li></ul></div><div class="col right"><h2>Studio Accessories</h2><ul>    <li><a href="/Categories/216-Backgrounds.aspx">Backgrounds &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/500-Fabric-Backgrounds.aspx">Fabric Backgrounds </a></li>        <li><a href="/Categories/501-Paper-Backgrounds.aspx">Paper Backgrounds </a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xrailorboom.aspx">Supports, Light Stands, &amp; Booms &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/662-Background-Support.aspx">Background Supports</a></li>        <li><a href="/Categories/133-Light-Stands.aspx">Light Stands</a></li>        <li><a href="/search/xboom.aspx">Booms</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xtriggerorrelease.aspx">Triggers &amp; Releases</a></li>    <li><a href="/search/xsynccablemore.aspx">Sync Cords, Adapters &amp; More </a></li>    <li><a href="/search/xproducttool.aspx">Product Photography Tools &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xlightingtent.aspx"><span>Lighting Tents</span><br />        A simple solution for small product photography! </a></li>        <li><a href="/search/shootingtable.aspx"><span>Shooting Tables</span><br />        Capture a soft and seamless background!</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xadditionalstudio.aspx">More Studio Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <!-- li><a href="/search/xapplebox.aspx">Apple Boxes</a></li -->        <li><a href="/search/stool.aspx">Stools</a></li>        <li><a href="/search/357JJA002%20OR%20357RES020%20OR%20357AMP066%20OR%20357AMP065.aspx">Sand Bags</a></li>        <li><a href="/search/xgaffertape.aspx">Gaffer Tape</a></li>        <li><a href="/search/xother.aspx">Other</a></li>    </ul>    </div>    </div>    </li></ul><h2>External Flashes &amp; Accessories</h2><ul>    <li><a href="/Categories/376-Flash.aspx">External Flash Units</a></li>    <li><a href="/search/XOCFPROFOTOAUG15.aspx">Off-Camera Flash by Profoto</a></li>    <li><a href="/search/xexternallitemod.aspx">Light Modifiers &amp; Diffusers </a></li>    <li><a href="/search/xexternalbattery.aspx">Battery Packs &amp; More</a></li></ul></div><a href="/profoto-off-camera-flash.aspx" onclick="_gaq.push(['_trackEvent','Nav: Video - Bottom','Click','Profoto Light Shaping Tools 150826']);"><img width="435" height="100" src="//www.henrys.com/Images/Nav-PCAs/Profoto-Light-Shaping-Tools-150826.jpg" alt="Profoto Light Shaping Tools" style="padding-top: 10px;" /></a>
							</div>
							<div id="header_lstCategories_dropProds_4" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="78511-CAMERON-10X12-MUSLIN-PINK-MIST.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products78511-75x75-948542.png' alt="CAMERON 10X12 MUSLIN PINK MIST " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="78511-CAMERON-10X12-MUSLIN-PINK-MIST.aspx" title="">
                                                        CAMERON 10X12 MUSLIN PINK MIST...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $49.50</span><br />
                                                        <span class="yousave" >Save: $40.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="84803-LASTOLITE-UMBRELLA-KIT-W-BRKT-STAND-CASE-34-.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products84803-75x75--300089460.jpg' alt="LASTOLITE UMBRELLA KIT W/BRKT/STAND/CASE 34&quot; " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="84803-LASTOLITE-UMBRELLA-KIT-W-BRKT-STAND-CASE-34-.aspx" title="">
                                                        LASTOLITE UMBRELLA KIT W/BRKT/...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $159.50</span><br />
                                                        <span class="yousave" >Save: $50.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="74872-BOWENS-JETSTREAM-350-WIND-MACHINE-BW2558.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products74872-75x75-1039790.jpg' alt="BOWENS JETSTREAM 350 WIND MACHINE BW2558 " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="74872-BOWENS-JETSTREAM-350-WIND-MACHINE-BW2558.aspx" title="">
                                                        BOWENS JETSTREAM 350 WIND MACH...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $1,299.99</span><br />
                                                        <span class="yousave" >Save: $330.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="71892-AURORA-LBDR68W-RECESSED-WHITE-REC-BOX-24X32.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products71892-75x75-805419.jpg' alt="AURORA LBDR68W RECESSED WHITE REC BOX 24X32 " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="71892-AURORA-LBDR68W-RECESSED-WHITE-REC-BOX-24X32.aspx" title="">
                                                        AURORA LBDR68W RECESSED WHITE ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $49.50</span><br />
                                                        <span class="yousave" >Save: $161.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_4" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Lighting - Right','Click','Cameron LS-10AC Light Stand']);" href="/88935-CAMERON-LS-10AC-LIGHT-STAND-10FT.aspx"> <img height="380" width="185" alt="Cameron LS-10AC Light Stand" src="//www.henrys.com/Images/Nav-PCAs/160606-right-Cameron-Light-Stand.jpg" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_5" class="A">
					<a href="javascript:void(0);">Printers</a>
					<div id="header_lstCategories_dropNav_5" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_5" class="drop-contentA">
								<h1>Printers</h1>
								<div class="col left"><h2>Printers</h2><ul>    <li><a href="/Categories/758-3D-Printers.aspx">3D Printers</a></li>    <li><a href="/Categories/302-All-In-One-Printers.aspx">All-In-One Printers</a></li>    <li><a href="/Categories/257-Dye-Sub-Printers.aspx">Dye Sub Printers</a></li>    <li><a href="/Categories/297-Inkjet-Printers.aspx">Inkjet Printers</a></li>    <li><a href="/Categories/306-Wide-Format-Printers.aspx">Wide Format Printers</a></li></ul><a onclick="_gaq.push(['_trackEvent','Nav: Printers - Bottom','Click','Canon PIXMA MG7520']);" href="/90046-CANON-PIXMA-MG7520-ALL-IN-ONE-BLACK.aspx"><img width="435" height="100" alt="Canon PIXMA MG7520" src="//www.henrys.com/Images/Nav-PCAs/160108-Bottom1-Canon_PIXMA_Printer.jpg" style="margin-top: 150px;" /></a></div><!-- Right Column --><div class="col right"><h2>Printer Supplies</h2><ul>    <li><a href="/Categories/759-3D-Printer-Filament.aspx">3D Printer Filament / Ink</a></li>    <li><a href="/Categories/59-Inkjet-cartridges.aspx">Inkjet Cartridges</a></li>    <li><a href="/search/PTPRINTPACK.aspx">Print Packs</a></li>    <li><a href="/Categories/122-Inkjet-Paper.aspx">Inkjet Paper</a></li>    <li><a href="/Categories/286-Print-Trimmers.aspx">Print Trimmers</a></li>    <li><a href="/Categories/217-Inkjet-Printer-Acc-.aspx">Printer Accessories</a></li></ul></div>
							</div>
							<div id="header_lstCategories_dropProds_5" class="product-listing">
								
										<h2>On Sale</h2>
										<ul class="prod-list">
											
										<li class="clear">
											<div class="img">
												<a href="21633-INKPRESS-WHITE-GLOSS-FILM-24X50.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products21633-75x75-799016.jpg' alt="INKPRESS WHITE GLOSS FILM 24X50&#39; " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="21633-INKPRESS-WHITE-GLOSS-FILM-24X50.aspx" title="">
                                                        INKPRESS WHITE GLOSS FILM 24X5...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $149.50</span><br />
                                                        <span class="yousave" >Save: $151.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="72538-ILFORD-PRESTIGE-G-FIBER-SILK-8-5X11-10SH.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products72538-75x75-907089.jpg' alt="ILFORD PRESTIGE G FIBER SILK 8.5X11 10SH " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="72538-ILFORD-PRESTIGE-G-FIBER-SILK-8-5X11-10SH.aspx" title="">
                                                        ILFORD PRESTIGE G FIBER SILK 8...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $16.50</span><br />
                                                        <span class="yousave" >Save: $3.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="85220-FUJI-SP-1-INSTAX-SHARE-PRINTER.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products85220-75x75--1305518414.jpg' alt="FUJI SP-1 INSTAX SHARE PRINTER " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="85220-FUJI-SP-1-INSTAX-SHARE-PRINTER.aspx" title="">
                                                        FUJI SP-1 INSTAX SHARE PRINTER...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $164.99</span><br />
                                                        <span class="yousave" >Save: $35.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										<li class="clear">
											<div class="img">
												<a href="21753-EPSON-STANDARD-PROOFING-PAPER-44-X164.aspx" title="">
													<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products21753-75x75-798648.jpg' alt="EPSON STANDARD PROOFING PAPER (44&quot;X164&#39;) " />
												</a>
											</div>
                                            <div class="prod-list-text">
                                                <h4>
                                                    <a href="21753-EPSON-STANDARD-PROOFING-PAPER-44-X164.aspx" title="">
                                                        EPSON STANDARD PROOFING PAPER ...
                                                    </a>
                                                </h4>
                                                <div class="price">
                                                        <span class="retail">Price: $277.99</span><br />
                                                        <span class="yousave" >Save: $138.00</span>
                                                            
                                                </div>
                                            </div>	
										</li>
									
										</ul>
									
							</div>
							<div id="header_lstCategories_dropPCARight_5" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: Printers - Right','Click','Canon PIXMA iP8720']);" href="/83486-CANON-PIXMA-IP8720-PRINTER.aspx"><img width="185" height="380" alt="Canon PIXMA iP8720" src="http://www.henrys.com/Images/Nav-PCAs/160606-right-Canon-PIXMA-Printer.jpg" /></a></div>
							</div>
							 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_6" class="B">
					<a href="javascript:void(0);">More</a>
					<div id="header_lstCategories_dropNav_6" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_6" class="drop-contentB">
								<h1>More</h1>
								<div class="col left"><h2>Digital Darkroom</h2><ul>    <li><a href="/Categories/56-Computer-software.aspx">Photo Editing Software</a></li>    <li><a href="/Categories/213-Scanners.aspx">Scanners &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/289-Film-Scanners.aspx">Film Scanner</a></li>        <li><a href="/Categories/301-Flatbed-Scanners.aspx">Flatbed Scanners</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/671-Monitors.aspx">Monitors</a></li></ul><h2>Home and Portable Entertainment</h2><ul>    <li><a href="/Categories/665-Projectors.aspx">Projectors</a></li>    <li><a href="/Categories/260-Projection-Screens.aspx">Projection Screens</a></li></ul><br /></div><div style="float: none; display: inline-block; padding-left: 20px;" class="col right"><h2>Film &amp; Darkroom</h2><ul>    <li><a href="/Categories/90-Instant-And-Disposable.aspx">Instant &amp; Disposable</a></li>    <li><a href="/Categories/175-Film.aspx">Film &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/268-B-And-W-Bulk-Film.aspx">B&amp;W Bulk Film</a></li>        <li><a href="/Categories/379-B-And-W-Film.aspx">B&amp;W  Film</a></li>        <li><a href="/Categories/261-Colour-Bulk-Film.aspx">Colour Bulk Film</a></li>        <li><a href="/Categories/232-Colour-Print-Film.aspx">Colour Print Film</a></li>        <li><a href="/Categories/241-Colour-Slide-Film.aspx">Colour Slide Film</a></li>        <li><a href="/Categories/269-Instant-Film.aspx">Instant Film</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/96-Projection-Accessories.aspx">Projection Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/230-Bulbs.aspx">Bulbs</a></li>        <li><a href="/Categories/178-Misc-Proj-Acc-.aspx">Misc. Projection Accessories</a></li>        <li><a href="/Categories/260-Projection-Screens.aspx">Projection Screens</a></li>        <li><a href="/Categories/280-Slide-Accessories.aspx">Slide Accessories</a></li>        <li><a href="/Categories/200-SLIDE-MOUNTS.aspx">Slide Mounts</a></li>        <li><a href="/Categories/177-Slide-Trays.aspx">Slide Trays</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/107-Paper-And-Chemistry.aspx">Paper &amp; Chemistry &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/274-B-And-W-FB-MG-Paper.aspx">B&amp;W FB MG Paper</a></li>        <li><a href="/Categories/247-B-And-W-Film-Developers.aspx">B&amp;W Film Developers</a></li>        <li><a href="/Categories/238-B-And-W-Fixers.aspx">B&amp;W Fixers</a></li>        <li><a href="/Categories/262-B-And-W-Misc-Chemicals.aspx">B&amp;W Misc. Chemicals</a></li>        <li><a href="/Categories/264-B-And-W-Paper-Developers.aspx">B&amp;W Paper Developers</a></li>        <li><a href="/Categories/242-B-And-W-RC-MG-Paper.aspx">B&amp;W RC MG Paper</a></li>        <li><a href="/Categories/248-B-And-W-RC-Paper.aspx">B&amp;W RC Paper</a></li>        <li><a href="/Categories/263-B-And-W-Stop-Baths.aspx">B&amp;W Stop Baths</a></li>        <li><a href="/Categories/277-Colour-Negative-Paper.aspx">Colour Negative Paper</a></li>        <li><a href="/Categories/270-Colour-Print-Developers.aspx">Colour Print Developers</a></li>        <li><a href="/Categories/305-Misc-Chemicals.aspx">Misc. Chemicals</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/99-Darkroom.aspx">Darkroom &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/100-Enlargers-Acc-.aspx">Enlargers/Acc.</a></li>        <li><a href="/Categories/101-Enlarging-Equipment.aspx">Enlarging Equipment</a></li>        <li><a href="/Categories/251-Print-And-Negative-Pages.aspx">Print &amp; Negative Pages</a></li>        <li><a href="/Categories/185-Retouching.aspx">Retouching</a></li>        <li><a href="/Categories/382-Timers.aspx">Timers</a></li>    </ul>    </div>    </div>    </li></ul><h2>Sporting/Hobby</h2><ul>    <li><a href="/Categories/92-Binoculars.aspx">Binoculars</a></li>    <li><a href="/Categories/94-Telescopes.aspx">Telescopes</a></li>    <li><a href="/search/xspotscope.aspx">Spotting Scopes</a></li></ul><h2>Learning</h2><ul>    <li><a href="http://www.schoolofimaging.ca/Default.aspx">School of Imaging</a></li>    <li><a href="/Categories/184-Books.aspx">Books &amp; DVDs</a></li></ul></div><div class="col right"><h2>Gift Ideas</h2><ul>    <li><a href="/Categories/205-SPECIALTY-ITEMS.aspx">Photo Novelty</a></li>    <li><a href="/Gift-Cards.aspx">Gift Cards</a></li></ul><h2>Henry's Exclusive Brands</h2><ul>    <li><a href="/search/Bowens.aspx/1/80000010%5EBOWENS">Bowens</a></li>    <li><a href="/search/cameron.aspx">Cameron</a></li></ul><h2>Photofinishing</h2><ul>    <li><a href="/Moments-Online.aspx">Photofinishing Services</a></li>    <li><a href="/Categories/179-Frames-And-Albums.aspx">Frames &amp; Albums &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/180-ALBUMS.aspx">Albums</a></li>        <li><a href="/Categories/201-FRAMES.aspx">Frames</a></li>        <li><a href="/Categories/259-Mats-for-Frames.aspx">Mattes for Frames</a></li>        <li><a href="/Categories/276-Mount-Board.aspx">Mount Board</a></li>        <li><a href="/Categories/255-Mounting-Supplies.aspx">Mounting Supplies</a></li>    </ul>    </div>    </div>    </li></ul></div>
							</div>
							
							<div id="header_lstCategories_dropPCARight_6" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: More - Right','Click','Gift Cards 141024']);" href="/Gift-Cards.aspx"> <img width="185" height="380" alt=" Henry's Gift Carfs " src="//www.henrys.com/Images/Nav-PCAs/Gift-Cards-141017.gif" /></a></div>
							</div>
							<div id="header_lstCategories_dropPCABottom_6" class="dropPCABottom">
								
							</div> 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_7" class="B blue">
					<a href="javascript:void(0);">Used</a>
					<div id="header_lstCategories_dropNav_7" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_7" class="drop-contentB">
								<h1>Used</h1>
								<div class="col left"><h2>Still &amp; Video Equipment</h2><ul>    <li><a>Film Cameras &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/141-35mm-Auto-Focus.aspx">35mm Auto Focus</a></li>        <li><a href="/Categories/149-35mm-Manual-Focus.aspx">35mm Manual Focus</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/138-Digital-Cameras.aspx">Digital Cameras</a></li>    <li><a href="/Categories/164-Digital-SLR.aspx">Digital SLRs</a></li>    <li><a href="/Categories/134-Medium-Format.aspx">Medium Format</a></li>    <li><a href="/Categories/136-Lenses.aspx">Lenses &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/142-35mm-Auto-Focus.aspx">35mm Auto Focus</a></li>        <li><a href="/Categories/137-35mm-Manual-Focus.aspx">35mm Manual Focus</a></li>        <li><a href="/Categories/294-Used-Digital-Lenses.aspx">Used Digital Lenses</a></li>    </ul>    </div>    </div>    </li></ul><ul>    <li><a href="/Categories/158-Camcorders.aspx">Camcorders</a></li></ul></div><div class="col right" style="float: none; display: inline-block; padding-left: 20px;"><h2>Additional Equipment</h2><ul>    <li><a>Camera Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/148-35mm-Auto-Focus.aspx">35mm Auto Focus</a></li>        <li><a href="/Categories/147-35mm-Manual-Focus.aspx">35mm Manual Focus</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/131-Accessories.aspx">Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/132-Used-Acc-.aspx">Used Acc.</a></li>        <li><a href="/Categories/189-Used-Digital.aspx">Used Digital</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/143-Flash.aspx">Flash &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/145-35mm-Auto-Focus.aspx">35mm Auto Focus</a></li>        <li><a href="/Categories/144-35mm-Manual-Focus.aspx">35mm Manual Focus</a></li>        <li><a href="/Categories/556-Used-Digital-Flash.aspx">Used Digital Flash</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/152-Light-Meters.aspx">Light Meters</a></li></ul><h2>Refurbished</h2><ul>    <li><a href="/search/refurb.aspx/1/80000000^60%2c%2380000000^61">Refurbished DSLR Cameras</a></li>    <li><a href="/search/refurb.aspx/1/80000000^60%2c%2380000000^73">Refurbished Lenses</a></li>    <li><a href="/search/refurb.aspx/1/80000000%5E51">Refurbished Accessories</a></li></ul></div><div class="col right"><h2>Useful Information</h2><ul>    <li><a href="/Used-Photo-Video-Gear.aspx">Why Buy Used</a></li>    <li><a href="/Henrys-Extended-Life-Plan.aspx">Henry's Extended Life Plan</a></li>    <li><a href="/Trade-In-Trade-Up-Check-List.aspx">Henry's Trade-in Check List</a></li>    <li><a href="/Trade-In-Trade-Up-Used-Condition.aspx">Henry's Used Condition Chart</a></li></ul></div>
							</div>
							
							<div id="header_lstCategories_dropPCARight_7" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a href="/Used-Photo-Video-Gear.aspx" onclick="_gaq.push(['_trackEvent','Nav: Used - Right','Click','Used Gear Callout 151030']);"> <img width="185" height="380" src="//www.henrys.com/Images/Nav-PCAs/Used-Nav-Right-151030.jpg" alt="Used Gear Callout 151030" /></a></div>
							</div>
							<div id="header_lstCategories_dropPCABottom_7" class="dropPCABottom">
								<div style="width:665px;height:73px;padding:0;margin:0;border:0;overflow:hidden;"><a href="/Categories/136-Lenses.aspx?view=grid" onclick="_gaq.push(['_trackEvent','Nav: Used - Bottom','Click','Used Lenses 141024']);"><img width="665" height="73" src="//www.henrys.com/Images/Nav-PCAs/Used-Lenses-141110.jpg" alt="Used Lenses at Henry's" /></a></div>
							</div> 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_8" class="B orange">
					<a href="javascript:void(0);">On Sale</a>
					<div id="header_lstCategories_dropNav_8" class="drop OG">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_8" class="drop-contentB">
								<h1>On Sale</h1>
								<div class="col left"><h2>Flyers &amp; Ads</h2><ul>    <li><a href="/Flyers-And-Ads.aspx">Current eFlyer</a></li></ul><h2>Camera &amp; Lenses</h2><ul>    <li><a href="/Categories/67-Digital-Cameras-Compare-and-Buy.aspx/1/283^On+Sale">Point and Shoots</a></li>    <li><a href="/search/compact system.aspx/1/80000000^60%2c%2380000000^61%2c%23283^On+Sale">Compact System Cameras</a></li>    <li><a href="/Categories/61-Digital-SLR-Cameras.aspx/1/283^On+Sale">Digital SLRs</a></li>    <li><a href="/Categories/171-Medium-Format.aspx/1/283^On+Sale">Medium Format</a></li></ul><ul>    <li><a>Lenses &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xlenscsc.aspx/1/283^On+Sale">Lenses for Compact System Cameras</a></li>        <li><a href="/search/xdslrlens.aspx/1/283^On+Sale">Lenses for Digital SLRs</a></li>        <li><a href="/search/xmediumformatlens.aspx/1/283^On+Sale">Lenses for Medium Format Cameras</a></li>    </ul>    </div>    </div>    </li>    <li><a>Lens Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/234-Lens-Caps.aspx/1/283^On+Sale">Lens Caps</a></li>        <li><a href="/Categories/168-Lens-Hoods.aspx/1/283^On+Sale">Lens Hoods</a></li>        <li><a href="/Categories/221-Filters.aspx/1/283^On+Sale">Filters</a></li>    </ul>    </div>    </div>    </li></ul><h2>Video</h2><ul>    <li><a href="/Categories/673-Action-Cameras.aspx/1/283^On+Sale">Action Cameras</a></li>    <li><a href="/search/xcamcorder.aspx/1/283^On+Sale">Camcorders &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xpocketable.aspx/1/283^On+Sale">Pocketable</a></li>        <li><a href="/search/xcamcordereveryday.aspx/1/283^On+Sale">Everyday Use</a></li>        <li><a href="/search/xcamenthusandpro.aspx/1/283^On+Sale">Enthusiast and Professional</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/114-Video-Accessories.aspx/1/283^On+Sale">Video Accessories</a></li></ul></div><div style="float: none; display: inline-block; padding-left: 20px;" class="col right"><h2>Camera &amp; Photography Accessories</h2><ul>    <li><a href="/Categories/83-Bags.aspx/1/283^On+Sale">Bags, Cases &amp; More &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/663-Pouches-for-Point-And-Shoots.aspx/1/283^On+Sale"><span>Pouches for Point-and-Shoots</span><br />        Protect your everyday gear.</a></li>        <li><a href="/Categories/664-Shoulder-Bags.aspx/1/283^On+Sale"><span>Shoulder Bags</span><br />        Designed for Compact System Cameras and DSLRs</a></li>        <li><a href="/Categories/660-Backpacks-And-Slings.aspx/1/283^On+Sale"><span>Backpacks &amp; Slings</span><br />        Ultimate comfort.</a></li>        <li><a href="/Categories/661-Camcorder-Bags.aspx/1/283^On+Sale"><span>Camcorder Cases</span><br />        For long lasting security.</a></li>        <li><a href="/search/xcarryingccase.aspx/1/283^On+Sale"><span>Carrying Cases</span><br />        Ideal for equipment mobility.</a></li>    </ul>    <p>    <a href="/Categories/83-Bags.aspx?view=grid"><span style="text-decoration: underline; color: #336699;" class="nav-ViewAll">View All On Sale Bags &amp; Cases</span></a>    </p>    </div>    </div>    </li>    <li><a>Batteries &amp; Power &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/227-Batteries.aspx/1/283^On+Sale">Batteries</a></li>        <li><a href="/Categories/245-Rechargeable-Batteries.aspx/1/283^On+Sale">Rechargeable Batteries</a></li>        <li><a href="/Categories/231-Digital-Camera-Batteries.aspx/1/283^On+Sale">Camera Batteries</a></li>        <li><a href="/Categories/383-Video-Batteries.aspx/1/283%5EOn+Sale">Camcorder Batteries</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/169-Camera-Straps.aspx/1/283^On+Sale">Camera Straps</a></li>    <li><a href="/Categories/222-Camera-Cleaning.aspx/1/283%5EOn+Sale">Cleaning Supplies</a></li>    <li><a href="/Categories/81-Filters.aspx/1/283^On+Sale">Filters</a></li>    <li><a>Flash, Modifiers &amp; More &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/376-Flash.aspx/1/283^On+Sale">External Flash Units</a></li>        <li><a href="/search/xexternallitemod.aspx/1/283^On+Sale">Light Modifiers &amp; Diffusers </a></li>        <li><a href="/search/xexternalbattery.aspx/1/283^On+Sale">Battery Packs &amp; More </a></li>    </ul>    </div>    </div>    </li>    <li><a href="/Categories/113-Memory.aspx/1/283^On+Sale">Memory Cards &amp; Media &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/272-Memory-Cards.aspx/1/283^On+Sale">Memory Cards</a></li>        <li><a href="/Categories/296-Memory-Card-Readers.aspx/1/283^On+Sale">Memory Card Readers</a></li>        <li><a href="/Categories/303-Memory-Acc-.aspx/1/283^On+Sale">Memory Card Storage &amp; More</a></li>        <li><a href="/Categories/293-Portable-Storage.aspx/1/283^On+Sale">External Harddrives</a></li>        <li><a href="/Categories/300-USB-Drives.aspx/1/283^On+Sale">USB Drives</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xphotoglove.aspx/1/283^On+Sale">Photo Gloves</a></li>    <li><a href="/search/xscreenorloop.aspx/1/283^On+Sale">Screen Protectors &amp; Loops</a></li>    <li><a href="/search/xsynccablemore.aspx/1/283^On+Sale">Sync Cords, Adapters &amp; More</a></li>    <li><a href="/search/xtriggerorrelease.aspx/1/283^On+Sale">Triggers &amp; Releases</a></li>    <li><a href="/search/tripod OR monopod.aspx/1/283^On+Sale">Tripods, Monopods &amp; Supports &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/442-Table-Mini-Pods.aspx/1/283^On+Sale">Tabletop &amp; Mini Tripods</a></li>        <li><a href="/Categories/374-PHOTO-TRIPOD-HEADS.aspx/1/283^On+Sale">Tripod Heads</a></li>        <li><a href="/Categories/443-Tripod-Legs-Only.aspx/1/283^On+Sale">Tripod Legs</a></li>        <li><a href="/Categories/378-PHOTO-TRIPOD-COMBO-W-HEAD.aspx/1/283^On+Sale">Complete Tripods</a></li>        <li><a href="/Categories/498-Video-Tripod-Heads.aspx/1/283^On+Sale">Video Heads</a></li>        <li><a href="/Categories/497-Video-Tripod-Combo-w-Head.aspx/1/283^On+Sale">Complete Video Tripods</a></li>        <li><a href="/search/xportablesupport.aspx/1/283^On+Sale">Stabilizers, Shoulder Brackets, Rigs &amp; More</a></li>    </ul>    </div>    </div>    </li>    <li><a href="/search/xvieworipiece.aspx/1/283^On+Sale">Viewfinders &amp; Eyepieces</a></li></ul></div><div class="col right"><h2>Lighting &amp; Studio</h2><ul>    <li><a>Studio Lighting &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/search/xstudiohead.aspx/1/283^On+Sale">Studio Strobe Heads</a></li>        <li><a href="/search/xstudiokit.aspx/1/283^On+Sale">Studio Strobe Kits</a></li>        <li><a href="/search/xexternalpack.aspx/1/283^On+Sale">Battery Packs</a></li>        <li><a href="/search/xaddstudioaccess.aspx/1/283^On+Sale">Additional Accessories</a></li>    </ul>    </div>    </div>    </li>    <li><a>Studio Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/216-Backgrounds.aspx/1/283^On+Sale">Backgrounds</a></li>        <li><a href="/Categories/662-Background-Support.aspx/1/283^On+Sale">Background Supports</a></li>        <li><a href="/Categories/133-Light-Stands.aspx/1/283^On+Sale">Light Stands</a></li>        <li><a href="/search/xrailorboom.aspx/1/283^On+Sale">Rail Systems &amp; Booms</a></li>    </ul>    </div>    </div>    </li>    <li><a>Lighting Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/212-Light-Meters.aspx/1/283^On+Sale">Light Meters</a></li>        <li><a href="/search/xlightmodifier.aspx/1/283^On+Sale">Light Modifiers</a></li>    </ul>    </div>    </div>    </li></ul><h2>Printers</h2><ul>    <li><a href="/Categories/302-All-In-One-Printers.aspx/1/283%5EOn+Sale">All-In-One Printers</a></li>    <li><a href="/Categories/257-Dye-Sub-Printers.aspx/1/283%5EOn+Sale">Dye Sub Printers</a></li>    <li><a href="/Categories/297-Inkjet-Printers.aspx/1/283%5EOn+Sale">Inkjet Printers</a></li>    <li><a href="/Categories/306-Wide-Format-Printers.aspx/1/283%5EOn+Sale">Wide Format Printers</a></li>    <li><a>Ink, Paper &amp; Accessories &rsaquo;</a>    <div class="nav-marketing-flyout">    <div class="flyout-container">    <h3>Narrow Your Search:</h3>    <ul>        <li><a href="/Categories/59-Inkjet-cartridges.aspx/1/283%5EOn+Sale">Inkjet Cartridges</a></li>        <li><a href="/search/PTPRINTPACK.aspx/1/283%5EOn+Sale">Print Packs</a></li>        <li><a href="/Categories/122-Inkjet-Paper.aspx/1/283%5EOn+Sale">Inkjet Paper</a></li>        <li><a href="/Categories/286-Print-Trimmers.aspx/1/283%5EOn+Sale">Print Trimmers</a></li>        <li><a href="/Categories/217-Inkjet-Printer-Acc-.aspx/1/283%5EOn+Sale">Pritner Accessories</a></li>    </ul>    </div>    </div>    </li></ul><!--     <h2>Online Only</h2>    <ul>        <li><a href="/photo-and-video-clearance-sale.aspx">Clearance Shelf</a></li>    </ul>    --></div>
							</div>
							
							<div id="header_lstCategories_dropPCARight_8" class="dropPCARight">
								<div style="width:185px;height:380px;padding:0;margin:0;border:0;overflow:hidden;"><a onclick="_gaq.push(['_trackEvent','Nav: On Sale - Right','Click','Audio Technica Microphone Kit 160628-160704']);" href="/online-only-special.aspx"><img height="380" width="185" alt="Online Only Special" src="//www.henrys.com/Images/Nav-PCAs/Audio-Technica-NavPCA-160628.png" /></a></div>
							</div>
							<div id="header_lstCategories_dropPCABottom_8" class="dropPCABottom">
								
							</div> 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_9" class="C header-menu-blue">
					<a href="javascript:void(0);">Learning</a>
					<div id="header_lstCategories_dropNav_9" class="drop BL">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_9" class="drop-contentC">
								<h1>Learning</h1>
								<div class="custom-nav-left"><h1>Learning</h1><p>You've got a new camera, camcorder or perhaps some lighting gear. Excellent. Now what? Henry's has several books for sale on a variety of topics to help you get up and running. Or take a course with <a href="http://www.schoolofimaging.ca">Henry's Learning Lab</a> and get advanced, in-depth instruction in a classroom environment.	</p><a class="button" href="http://www.schoolofimaging.ca">See Our Classes</a></div><div class="custom-nav-right"><h3>Classes by Category</h3><ul>    <li><a href=" http://www.henrys.com/Learning-Lab.aspx"><strong>Learning Lab Online<br />    Powered by KelbyOne</strong><br />    Online classes at your own pace</a></li>    <li><a href="http://www.schoolofimaging.ca/Categories/682-Photo-Courses.aspx"><strong>Photo Classes</strong><br />    Capture those perfect moments</a></li>    <li><a href="http://www.schoolofimaging.ca/categories/614-Special-Events.aspx"><strong>Special Events</strong><br />    From the classroom to real life</a></li>    <li><a href="http://www.schoolofimaging.ca/Categories/678-Software-Courses.aspx"><strong>Software Classes</strong><br />    Manage &amp; enhance your photos</a></li>    <li><a href="http://schoolofimaging.ca/search/flying%20cameras.aspx"><strong>Flying Camera Classes</strong><br />    There's something new in the air!</a></li></ul></div>
							</div>
							
							
							<div id="header_lstCategories_dropPCABottom_9" class="dropPCABottom">
								<div style="width:665px;height:73px;padding:0;margin:0;border:0;overflow:hidden;"><a href="/Learning-Lab.aspx" target="_blank" onclick="_gaq.push(['_trackEvent','Nav: Learning - Bottom','Click','Learning Lab 151203']);"><img alt="Henry's learning Lab" src="//www.henrys.com/Images/Nav-PCAs/151203-bottom-learning-lab-banner.gif" style="width: 665px; height: 73px;" /></a></div>
							</div> 
						</div>
					</div>					
				</li>					
			
				<li id="header_lstCategories_category_10" class="C header-menu-blue last">
					<a href="javascript:void(0);">Events</a>
					<div id="header_lstCategories_dropNav_10" class="drop BL">
						<div class="dropContainer">
							<div id="header_lstCategories_dropContent_10" class="drop-contentC">
								<h1>Events</h1>
								<!-- EVENTS NAV --><div class="custom-nav-left"><h1>Events</h1><p>Take a look at some of the awesome events we have coming up:</p><div><ul class="nav-events-list">    <!--<li><p>Sorry, there are currently no events scheduled, but check back often. We update regularly with new events and seminars!</p></li>-->    <li>    <p><a href="http://schoolofimaging.ca/Courses/94203-Toronto-Island-Outdoor-Photo-Workshop.aspx">Toronto Island Outdoor Photo Workshop</a><br />    Friday, July 15th or Saturday, September 17th - Toronto, ON</p>    </li>    <li>    <p><a href="http://www.schoolofimaging.ca/Courses/65572-Photographing-Port-Credit.aspx">Photographing Port Credit</a><br />    Sunday, July 17th or Saturday, September 17th - Mississauga, ON</p>    </li>    <li>    <p><a href="http://schoolofimaging.ca/Courses/78723-Summer-Camp-Intro-to-Photography.aspx">Summer Camp - Intro to Photography</a><br />    Thursday, July 21st - London, ON</p>    </li>    <li>    <p><a href="http://schoolofimaging.ca/Courses/94405-St-Johns-Downtown-Outdoor-Photo-Workshop.aspx">Capturing the Character of St. John's</a><br />    Saturday, July 23rd - St. John's, NL</p>    </li>    <li>    <p><a href="http://www.schoolofimaging.ca/Courses/80120-Urban-Photography-in-Toronto.aspx">Urban Photography In Toronto</a><br />    Saturday, July 23rd or Saturday, August 20th - Toronto, ON</p>    </li></ul></div><div><ul class="nav-events-list">    <li>    <p>    <a href="/in-store-seminars.aspx">See All Events</a><br />    Check back regularly. We always have something happening!    </p>    </li></ul></div></div><div class="custom-nav-right"><h3>The Henry's Family</h3><ul id="events-nav-right-list">    <li><a href="/Store-Locations.aspx"><strong>Store Locations</strong><br />    Find a Henry's Near You!</a></li>    <li><a href="/Rentals.aspx"><strong>Rentals</strong><br />    Rent Before You Buy</a></li>    <li><a href="/Professional-Services.aspx"><strong>Professional Services</strong><br />    Total Imaging Solutions</a></li>    <li><a target="_blank" href="http://www.learninglab.ca"><strong>Henry's Learning Lab</strong><br />    Online &amp; In-Person Learning</a></li></ul></div>
							</div>
							
							
							<div id="header_lstCategories_dropPCABottom_10" class="dropPCABottom">
								
							</div> 
						</div>
					</div>					
				</li>					
						
				</ul>
			
	</div>

</div>
<div class="center-content-container"><!-- ends in footer.aspx -->
<h2 class="center-lined" id="shippingBanner">
<span>All pricing in Canadian Dollars.<br />
Duty-Free exemption limit is now $800 USD for online purchases to USA. <a href="/Shipping-Policy.aspx">See Details.</a></span>
</h2>
			<div id="content-wrap" class="clearfix">
		        	
	<div id="content" class="clearfix">
		<div id="breadcrumb">
			
	<link type="text/css" rel="Stylesheet" href="styles/Static-Pages.css" />
    <meta name="keywords" content="privacy policy, Toronto, Ontario, Henrys" />
    <meta name="description" content="View our privacy policy at Henry's Camera" />
	<a href="Default.aspx">Home</a>
	: Privacy Policy
	
		</div>
		
    <div class="content-page">
        <h1>Privacy Policy</h1>
       	<p>Thanks for visiting! We want to assure you that your privacy is safe with us at Henry’s. This Privacy Policy covers our treatment of personal information that this site gathers when you access or use our online store. For details, continue reading:</p>
        
        <h2>Information We Collect</h2>
		
        <h3>Our visitors - Automatic Data Collection</h3>
        <p>Like most websites, henrys.com collects non-personally identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Our purpose in collecting non-personally-identifying information is to better understand how our visitors use this website. From time to time, we may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of this website.</p>
		<p>Henry’s also collects potentially personally-identifying-information like Internet Protocol (IP) addresses. We do not use such information to identify our visitors, however, and do not disclose such information, other than under the same circumstances that we use and disclose personally-identifying information, as described below.</p>
        
        <h3>Our Customers - Information You Provide to Us</h3>
        <p>Certain actions on our website do require us to collect personally-identifying information. Purchasers are asked to provide additional information, including as necessary the personal and financial information required to process transactions. In each case, Henry’s collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor's interaction with our company. We will never disclose personally-identifying information other than as described below. </p>
        
        <h3>Aggregated Statistics</h3>
		<p>Henry’s may collect statistics about the behavior of visitors to its websites. For instance, Henry’s may monitor the most popular sections or products on our site. Henry’s may display this non-personally-identifying information publicly or provide it to others. However, Henry’s does not disclose personally-identifying information other than as described below.</p>
        
        <h3>Protection of Certain Personally-Identifying-Information</h3>
		<p>Henry’s discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors, and affiliated organizations that (i) need to know that information in order to process it on our behalf, or to provide services requested by the customer, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors, and affiliated organizations may be located outside of your home country; by using Henrys.com, you consent to the transfer of such information to them. Henry’s will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, Henry’s discloses potentially personally-identifying and personally-identifying information only when required to do so by law, or when we believe in good faith that disclosure is reasonably necessary to protect the property or rights of Henry’s, third parties or the public at large. Henry’s takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.</p>
        
        <h3>Cookies</h3>
        <p>A cookie is a string of information that a website stores on a visitor's computer, and that the visitor's browser provides to the website each time the visitor returns. Without cookies, Henry’s can't keep track of the items you've placed in your cart or perform other crucial ecommerce capabilities. We do not use cookies to intrude in your privacy or do anything other than match you up with your information on our site.</p>

		<h2>Communications</h2>
		<h3>Email Privacy Policy</h3>
		<p>We have created this email privacy policy to demonstrate our firm commitment to your privacy and the protection of your information.</p>
        
        <h3>Why did you receive an email from us?</h3>
		<p>If you received a mailing from us, (a) your email address is either listed with us as someone who has expressly shared this address for the purpose of receiving information in the future ("opt-in"), or (b) you have registered or purchased or otherwise have an existing relationship with us. We respect your time and attention by controlling the frequency of our mailings.</p>

		<h3>How we protect your privacy</h3>
		<p>We use security measures to protect against the loss, misuse, and alteration of data used by our system.</p>

		<h3>Sharing and Usage</h3>
		<p>We will never share, sell, or rent individual personal information with anyone without your advance permission or unless ordered by a court of law. Information submitted to us is only available to employees managing this information for purposes of contacting you or sending you emails based on your request for information and to contracted service providers for purposes of providing services relating to our communications with you.</p>
        
        <h3>How can you stop receiving email from us?</h3>
		<p>Each email sent contains an easy, automated way for you to cease receiving email from us, or to change your expressed interests. If you wish to do this, simply follow the instructions at the end of any email. </p>
		<p>If you have received unwanted, unsolicited email sent via this system or purporting to be sent via this system, please forward a copy of that email with your comments to <a href='mail&#116;&#111;&#58;inf&#111;%40&#104;%6&#53;nr&#121;%73&#46;co&#109;'>inf&#111;&#64;h&#101;nrys&#46;&#99;&#111;m</a> for review.</p>

        <h2>Conditions of Use</h2>
		<p>If you decide to visit the Henry’s website, your visit and any possible dispute over privacy is subject to this Privacy Policy and our Terms of Use, including limitations on damages, arbitration of disputes, and application of Ontario provincial law.</p>

		<h2>Privacy Policy Changes</h2>
		<p>Although most changes are likely to be minor and infrequent, Henry’s may change its Privacy Policy from time to time, and in our sole discretion. We encourage visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change.</p>
		<p>For information on our security policies, <a href="/Security-Policy.aspx">click here</a>. </p>

		<h2>Questions</h2>
		<p>Our full contact information, including location, can be found on our Contact page - <a href="/Customer-Care.aspx">click here</a>.</p>

    </div>
	
	</div>
	<div id="right-column">
		
    <div style="width:225px;height:100px;padding:0;margin:0;border:0;overflow:hidden;"><a href="http://www.learninglab.ca" id="learninglab-home-btn" onclick="_gaq.push(['_trackEvent','Homepage PCA','Click','Henrys Learning Lab 150324']);">Henry's Learning Lab</a></div>
    <br />
    
		<div class="recentlyviewedwidget">
			<h3>You've recently viewed:</h3>
			<ul>
				
		
			<li>
				<div class="tab-right-prod-top">
					<a href="67256-BOWER-7-5MM-F3-5-FISHEYE-LENS-M4-3.aspx" title="">
						<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products67256-50x50-689585.jpg'
							alt='BOWER 7.5MM F3.5 FISHEYE LENS (M4/3) ' />
					</a>
					<h4><a href="67256-BOWER-7-5MM-F3-5-FISHEYE-LENS-M4-3.aspx" title="">
						BOWER 7.5MM F3.5 FISHEYE LENS (M4/3) 
						</a>
					</h4>
                </div>
				<div class="tab-right-prod-bottom">
                	<span class="salePrice">Sale: $369.50</span>
						
                        <a rel="nofollow"  class="addtocart" onclick ="LoadWarrantyModal1('/WarrantyModalView.aspx?productId=67256','67256');" href="#">
                        <span>Add to cart</span></a>
					
            	</div>
			</li>
		
		
	
		
			<li>
				<div class="tab-right-prod-top">
					<a href="65920-SIGMA-OS-105MM-F2-8-EX-DG-MACRO-CANON-MT.aspx" title="">
						<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products65920-50x50-885640.jpg'
							alt='SIGMA OS 105MM F2.8 EX DG MACRO CANON MT ' />
					</a>
					<h4><a href="65920-SIGMA-OS-105MM-F2-8-EX-DG-MACRO-CANON-MT.aspx" title="">
						SIGMA OS 105MM F2.8 EX DG MACRO CANON MT 
						</a>
					</h4>
                </div>
				<div class="tab-right-prod-bottom">
                	<span class="salePrice">Sale: $769.99</span>
						
                        <a rel="nofollow"  class="addtocart" onclick ="LoadWarrantyModal1('/WarrantyModalView.aspx?productId=65920','65920');" href="#">
                        <span>Add to cart</span></a>
					
            	</div>
			</li>
		
		
	
		
			<li>
				<div class="tab-right-prod-top">
					<a href="63769-BLACKRAPID-RS-SPORT-SLING-CAMERA-STRAP.aspx" title="">
						<img src='http://az163874.vo.msecnd.net/143c9f515cff4d86ab6f0969faac3792/Images/Products63769-50x50-504145.jpg'
							alt='BLACKRAPID RS-SPORT SLING CAMERA STRAP ' />
					</a>
					<h4><a href="63769-BLACKRAPID-RS-SPORT-SLING-CAMERA-STRAP.aspx" title="">
						BLACKRAPID RS-SPORT SLING CAMERA STRAP 
						</a>
					</h4>
                </div>
				<div class="tab-right-prod-bottom">
                	Price: $139.99
						
                        <a rel="nofollow"  class="addtocart" onclick ="LoadWarrantyModal1('/WarrantyModalView.aspx?productId=63769','63769');" href="#">
                        <span>Add to cart</span></a>
					
            	</div>
			</li>
		
		
	
			</ul>
		</div>
	


	</div>
				
			</div>
			    

	<div id="footer-services">
        <h2 class="center-lined">More Reasons to Shop at Henry's</h2>
        <div><a href="/Henrys-Extended-Life-Plan.aspx"><img src="/Images/Homepage-Btns/exclusive-services/Help-v3.gif" width="192" height="150" alt="HELP" /></a></div>
        <div><a href="/Trade-In-Trade-Up.aspx"><img src="/Images/Homepage-Btns/exclusive-services/TradeUpgrade-v3.gif" width="192" height="150" alt="Trade-it p Grade It" /></a></div>
        <div><a href="/Half-Back.aspx"><img src="/Images/Homepage-Btns/exclusive-services/Halfback-v3.gif" width="192" height="150" alt="Henry's Halfback" /></a></div>
        <div><a href="/Price-Protection.aspx"><img src="/Images/Homepage-Btns/exclusive-services/PriceProtection-v3.gif" width="192" height="150" alt="Henry's Price Promise" /></a></div>
        <div class="last"><a href="/Reserve-Online-Pick-Up-In-Store.aspx"><img src="/Images/Homepage-Btns/exclusive-services/ReserveOnline-v3.gif" width="192" height="150" alt="Reserve Online" /></a></div>
    </div>
    
    <div id="footer-featured-brands" class="center-content-container">
        <h2 class="center-lined">Shop Featured Brands</h2>
        <a href="/bowens.aspx"><img src="/Images/footer-featured-brands/featured-bowens.jpg" width="120" height="60" alt="Bowens" /></a><a href="/cameron.aspx"><img src="/Images/footer-featured-brands/featured-cameron.jpg" width="120" height="60" alt="Cameron" /></a><a href="/search/Canon.aspx/1/80000010%5eCANON"><img src="/Images/footer-featured-brands/featured-canon.jpg" width="120" height="60" alt="Canon" /></a><a href="/search/Epson.aspx/1/80000010%5eEPSON"><img src="/Images/footer-featured-brands/featured-epson.jpg" width="120" height="60" alt="Epson" /></a><a href="/search/Fuji.aspx/1/80000010%5eFUJI"><img src="/Images/footer-featured-brands/featured-fuji.jpg" width="120" height="60" alt="Fujifilm" /></a><a href="/GOPRO-HERO4.aspx"><img src="/Images/footer-featured-brands/featured-gopro.jpg" width="120" height="60" alt="GoPro Hero4" /></a><a href="/search/Lowepro.aspx/1/80000010%5eLOWEPRO"><img src="/Images/footer-featured-brands/featured-lowepro.jpg" width="120" height="60" alt="Lowepro" /></a><a href="/search/Manfrotto.aspx/1/80000010%5eMANFROTTO"><img src="/Images/footer-featured-brands/featured-manfrotto.jpg" width="120" height="60" alt="Manfrotto" /></a>
        <a href="/search/Nikon.aspx/1/80000010%5eNIKON"><img src="/Images/footer-featured-brands/featured-nikon.jpg" width="120" height="60" alt="Nikon" /></a><a href="/search/Olympus.aspx/1/80000010%5eOLYMPUS"><img src="/Images/footer-featured-brands/featured-olympus.jpg" width="120" height="60" alt="Olympus" /></a><a href="/search/Panasonic.aspx/1/80000010%5ePANASONIC"><img src="/Images/footer-featured-brands/featured-panasonic.jpg" width="120" height="60" alt="Panasonic" /></a><a href="/search/Samsung.aspx/1/80000010%5eSAMSUNG"><img src="/Images/footer-featured-brands/featured-samsung.jpg" width="120" height="60" alt="Samsung" /></a><a href="/search/Sigma.aspx"><img src="/Images/footer-featured-brands/featured-sigma.jpg" width="120" height="60" alt="Sigma" /></a><a href="/search/Sony.aspx/1/80000010%5eSONY"><img src="/Images/footer-featured-brands/featured-sony.jpg" width="120" height="60" alt="Sony" /></a><a href="/search/Ricoh.aspx/1/80000010%5eRICOH"><img src="/Images/footer-featured-brands/featured-ricoh.jpg" width="120" height="60" alt="Ricoh" /></a><a href="/search/Westcott.aspx/1/80000010%5eWESTCOTT"><img src="/Images/footer-featured-brands/featured-westcott.jpg" width="120" height="60" alt="Westcott" /></a>
    </div>
</div> <!-- end .center-content-container -->
<!-- eNEWS SIGNUP -->
<div id="eNews-signup-container">
	<!-- h2 class="center-lined">Stay In Touch</h2 -->
    <h2 class="center-lined">Subscribe For Your Chance To Win!</h2>
	<div id="eNews-signup" class="center-content-container">
		<p>Sign up for Henry's  eNewsletter to learn about events near you, great deals, new products and be entered to Win Your Purchase! <a  style="color:#FFFFFF;" href="/Win-Your-Purchase-Contest-Rules.aspx" target="_blank">Find out more</a>.</p>
        <div id="signup-controls-container">
            <div id="signupEmail">
                <label>Email:</label><br />
                <input type="text" id="txtNewsletterSignup" title="Email Address" />
            </div>
            <div id="signupPostal">
                <label>Postal Code: <!--<span id="infoIcon" title="Enter your postal code to receive email content relevant to your area">?</span>--></label><br />
                <input type="text" id="txtPostalCode" title="Postal Code" />
            </div>	
            <div id="signupSubmit">
            	<input type="hidden" name="vars[source]" value="Henrys-Footer">
                <a href="#" id="btnNewsletterSubmit">Submit</a>
            </div>
        </div>	
    </div>
    <div id="footer-social" class="center-content-container">
        <ul class="socialMediaLinks">
            <li class="twitter"><a href="http://www.twitter.com/henryscamera" target="_blank">Twitter</a></li>
            <li class="facebook"><a href="https://www.facebook.com/HenrysCamera" target="_blank">Find Us On Facebook</a></li>
            <li class="googleplus"><a href="https://plus.google.com/113532467082526153366/posts" target="_blank">Google Plus</a></li>
            <li class="henrystv"><a href="http://instagram.com/henryscamera" target="_blank">Henry's Instagram</a></li>
        </ul>
    </div>
</div>

<div id="footerWrap" class="clear">
	<div id="footer" class="center-content-container">
		<div class="footer-box-top">		
			<div class="footPCA">
				<a href="http://www.learninglab.ca" target="_blank" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Henry\'s Learning Lab']);"><img width="191" height="60" src="//www.henrys.com/Images/footer-logos/HLL-Logo.jpg" alt="Henry's Learning Lab" /></a><a href="http://www.henrys.com/Henrys-Photofinishing.aspx" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Photo Printing']);"><img width="192" height="60" src="//www.henrys.com/Images/footer-logos/photo-printing-logo.jpg" alt="Photo Printing" /></a><a href="https://www.henrys.com/rentals.aspx" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Henry\'s Rentals']);"><img width="192" height="60" src="//www.henrys.com/Images/footer-logos/rentals-logo.jpg" alt="Henry's Rentals" /></a><a href="http://stores.ebay.com/henrys-com" target="_blank" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Henry\'s eBay Store']);"><img width="192" height="60" src="//www.henrys.com/Images/footer-logos/ebay-logo.jpg" alt="Henry's eBay Store" /></a><a href="http://www.henrys.com/professional-services.aspx" target="_blank" onclick="_gaq.push(['_trackEvent','Footer PCA','Click','Henry\'s Professional Services']);"><img width="191" height="60" src="//www.henrys.com/Images/footer-logos/HPS-logo.jpg" alt="Henry's Professional Services" /></a>
			</div>
		</div>	
		<div id="footer-box">
			<div class="column">		
				<h5>Shop</h5>			
				<ul>				
					<li><a href="Categories/60-Cameras.aspx">Cameras</a></li>
					<li><a href="Categories/69-Camera-Accessories.aspx">Camera Accessories</a></li>
                    <li><a href="Droneography.aspx">Drones/UAV</a></li>
					<li><a href="Categories/729-Mobile.aspx">Mobile</a></li>
					<li><a href="Categories/64-Video.aspx">Video</a></li>
					<li><a href="Categories/48-Lighting.aspx">Lighting</a></li>
					<li><a href="Categories/53-Computers.aspx">Computers</a></li>
					<li><a href="Categories/57-Printers.aspx">Printers</a></li>
					<li><a href="Categories/91-Binoculars.aspx">Binoculars</a></li>
					<li><a href="Categories/130-Used.aspx">Used</a></li>
					<li><a href="Categories/89-Film-And-Darkroom.aspx">Film and Darkroom</a></li>
                    <!-- <li><a href="Photo-And-Video-Clearance-Sale.aspx">Henry's Clearance Shelf</a></li> -->
				</ul>
			</div>
			<div class="column">
				<h5>Shopping Tools</h5>
				<ul>
					<li><a href="ShoppingCart.aspx">My Cart</a></li>
					<li><a href="WishList.aspx">My Wishlist</a></li>
					<li><a href="Account.aspx">My Account</a></li>		
					<li><a href="Flyers-And-Ads.aspx">Flyers and Promotions</a></li>	
                    <li><a href="Gift-Cards.aspx">Gift Cards</a></li>
					<li><a href="http://stores.ebay.com/henrys-com" target="_blank">eBay Store</a></li>		
					<li><a href="Sign-Out.aspx">Sign Out</a></li>
				</ul>
			</div>
			<div class="column">
				<h5>Information</h5>
				<ul>
					<li><a href="Shipping-Policy.aspx">Shipping Policy</a></li>				
					<li><a href="Returns-Exchange-Policy.aspx">Returns/Exchange Policy</a></li>	
					<li><a href="Warranty-Policy.aspx">Warranty Policy</a></li>
					<li><a href="Repairs.aspx">Repairs</a></li>			
					<li><a href="Privacy-Policy.aspx">Privacy &amp; Security Policy</a></li>
					<li><a href="Terms-of-Use.aspx">Terms of Use</a></li>				
					<li><a href="Price-Protection.aspx">Price Match Policy</a></li>	
					<li><a href="In-store-seminars.aspx">Events &amp; In-store Seminars</a></li>
					<li><a href="Sitemap.aspx">Sitemap</a></li>
					<li><a href="Subscribe-to-Henrys.aspx">Subscribe to eNewsletters</a></li>						
				</ul>
			</div>
			<div class="column">
				<h5>Services</h5>
				<ul>
					<li><a href="http://www.schoolofimaging.ca/?cart=iqqqnI3Z7m2Vgw4BCt9BCfNl4jWkInFswxYoGwxN2IDx2lMylLSgx1v4Q1EwXx1Nux%2fIWUApx%2brM2gvTXqQY3RnrfIlRgRXMbTxRu0bWCpZPBGHuQvWA0nP2k%2b9MVdtNRedqGMAAuhQYFrZz68e%2faeS1cO7ry4qnb7Dm%2fxs0xTc%3d">Henry's Learning Lab</a></li>		
					<li><a href="Henrys-Photofinishing.aspx">Henry's Photo Printing</a></li>				
					<li><a href="Rentals.aspx">Henry's Rentals</a></li>				
					<li><a href="Half-Back.aspx">Half Back</a></li>
					<li><a href="Henrys-Leasing-Equilease.aspx">Leasing</a></li>					
                    <li><a href="Ship-Direct.aspx">Ship Direct</a></li>
					<li><a href="Trade-In-Trade-Up.aspx">Trade It, Upgrade It</a></li>	
					<li><a href="Henrys-Extended-Life-Plan.aspx">Henry's Extended Life Plan</a></li>		
					<li><a href="Outlet-Centre.aspx">Henry's Outlet Centre</a></li>			
				</ul>
			</div>
			<div class="column">
				<h5>About Henry's</h5>
				<ul>
            		<li><a href="Store-Locations.aspx">Locations</a></li>				
					<li><a href="Company-Info.aspx">Company Info</a></li>
					<li><a href="Customer-Care.aspx">Customer Care</a></li>
					<li><a href="Professional-Services.aspx">Henry's Professional Services</a></li>
					<li><a href="careers-at-henrys.aspx">Careers</a></li>
					<li><a href="https://henrys.affiliatetechnology.com/">Affiliates</a></li>
					<li><a href="Company-History.aspx">Henry's 100th Anniversary</a></li>
					<li><a href="Get-Connected-Henrys-Social-Media.aspx">Henry's Social Media</a></li>
                    <li><a href="http://www.henrys.com/love.aspx">Community Involvement</a></li>
                     <li><a href="http://blog.henrys.com/" target="_blank">Henry's Blog</a></li>
				</ul>
			</div>				
		</div>
	</div>
</div>

<div id="footerFinePrintWrapper">
    <div id="footer-copyright" class="center-content-container">
        <div style="float:left;background-color: #fff;border-radius: 6px;padding: 10px;">
            <script type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
        <img src="/Images/payment-methods-footer.jpg" width="350" alt="Accepted Payments: Paypal, Visa, Mastercard, Visa Debit, Amex" />
        </div>
        <div style="float: left; width: 456px; margin-left: 16px;">
            <p><strong>Need help? Call us at  1-800-461–7960 or by reach us <a href="/Customer-Care.aspx#email" style="color: #fff;">by email</a>.</strong></p>
            <p>All prices listed are in Canadian dollars. We make every effort to ensure our prices are accurate. We do, however, reserve the right to advise you of any errors prior to processing your invoice. If you are not willing to accept changes on these errors we will cancel your order. Henry's reserves the right to limit quantities. We apologize for any inconvenience this may cause.</p>
            <p>&copy; 1996-2016 Cranbrook Glen Enterprises Ltd. All Rights Reserved.</p>
        </div>
    </div>
</div>

        </div>
        
<div id="minicart">
    <div id="minicart-lineitems" class="clearfix"></div>
    <div id="minicart-subtotal-container" class="clearfix">
		<span class="label">Sub-Total:</span> <span id="minicart-subtotal" class="value"></span>
    </div>
    <!--<p class="fullcart" style="color:red"> </p>-->
    <div class="cartsummary1">Enter postal/zip code at your</div>
    <div class="cartsummary2"><a href="ShoppingCart.aspx">full shopping cart  </a> for taxes and shipping.</div>
   		
	<div class="controls">
		<a href="ShoppingCart.aspx"><img src="/WebResource.axd?d=x0KL7lKQiTHdUOV1VziUpa6-a09hXkY365p2Au_uslkZGZkkmRu8PLUrPxxEmC6LELz902-VVN9Pv6RCgaat3fgbiZGiQuPn4h3tBjXlvdBMVHD1gFUN6ed-ANkAGlzJ3O6qEZTC3T7qii2m3gLiNIS2F8elN_GR5-Lc5f2iGoVhg_8I0&amp;t=636027984997195045" alt="Checkout" /></a>			
		<div style="display: inline">
		<a class="btnReserveMiniCart" href="ReserveInStoreCartNotification.aspx?origin=2"><img src="Images/minicar-reserve.jpg" alt="Reserve In Store" /></a>			
		</div>

	</div>
	<div class="controls">
		<a href="ShoppingCart.aspx">Modify Cart</a> | <a href="WishList.aspx">Wish List</a> | <a href="#" id="btnCloseMiniCart">Close</a>
	</div>
</div>

 <script id="miniCartItemTemplate" type="text/html">
	<!--
	<# 	
		for(var i = 0; i < lineItems.length; i++) {       
         var lineItem = lineItems[i]; #>
         
		<div class="minicart-lineitem" id="minicart-lineitem-<#= lineItem.Id#>">
			<div class="img"><a href="<#= lineItem.LinkUrl === '' ? hashSymbol : lineItem.LinkUrl #>"><img src="<#= rootRel #>ImageHandler.axd?imageId=<#= lineItem.ImageId #>&amp;width=48&amp;height=48&amp;constraint=4&amp;async=false" alt="" \/><\/a><\/div>			
			<div class="details">
				<h3><a href="<#= lineItem.LinkUrl === '' ? hashSymbol : lineItem.LinkUrl #>"><#= lineItem.LineItemType === Henrys.Web.WebServices.ShoppingCartLineItemType.BonusProduct && lineItem.UnitPrice === 0 ? '<span class="bonus">Bonus!<\/span> ' : ''#><#= lineItem.DisplayText #><\/a><\/h3>
				<div class="price"><#= lineItem.LineItemType === Henrys.Web.WebServices.ShoppingCartLineItemType.BonusProduct && lineItem.UnitPrice === 0 ? '<span class="bonus">FREE<\/span>' : lineItem.UnitPrice.localeFormat('C2') #><#= lineItem.RetailPrice > lineItem.UnitPrice && lineItem.LineItemType !== Henrys.Web.WebServices.ShoppingCartLineItemType.BonusProduct ? ' <span class=\"savelight\">(Save ' + (lineItem.RetailPrice - lineItem.UnitPrice).localeFormat('C') + ')<\/span>' : '' #> <#= lineItem.Quantity > 1 ? '<span class=\'qty\'>x ' + lineItem.Quantity + '<\/span>' : '' #><\/div>
			<\/div>
			<div class="delete" rel="<#= lineItem.Id#>">x<\/div>			
		<\/div>
	<# } #>
	-->
</script>
        
<div id="WelcomeModalContainer">
<div id="welcomeModalContent">
<div id="WelcomeModalTop">
<div class="WelcomeClose"><a style="display:block;width:30px;height:30px;" id="WelcomeModalClose" onclick="CloseWelcomeModal();" href="#"></a></div>

</div>
<div id="WelcomeModalBottom">
<div style="padding-top:15px;">




<div style="padding:5px;"><span class="emailSpan">Email</span>
<input name="ctl00$ctl00$welcomeModal$txtWelcomeSignUp" type="text" id="welcomeModal_txtWelcomeSignUp" style="width:250px;height:30px;margin-right:20px;font-size:20px;" />
<span class="emailSpan">Postal Code</span><input name="ctl00$ctl00$welcomeModal$txtWelcomePostalCode" type="text" id="welcomeModal_txtWelcomePostalCode" style="width:150px;height:30px;" />
<div style="padding:5px;text-align:center;padding-top:20px;">
<input type="image" name="ctl00$ctl00$welcomeModal$btnSubmitEmail" id="welcomeModal_btnSubmitEmail" src="Images/Welcome_Modal_Submit.png" onclick="trackClick();WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$welcomeModal$btnSubmitEmail&quot;, &quot;&quot;, true, &quot;WelcomeSubmit&quot;, &quot;&quot;, false, false))" style="cursor:pointer;" />
</div>
</div></div></div>

</div>
<div id="WelcomeThankYou"><div class="WelcomeThankYouContent" alt="Thanks for subscribing"></div>
<div class="WelcomeClose"><a style="display:block;width:30px;height:30px;" id="WelcomeModalThankYouClose" onclick="CloseWelcomeModal();" href="#"></a></div>
<div class="WelcomeThankYouClose" style="margin-left: auto; margin-right: auto; text-align: center;"><a href="#" onclick="CloseWelcomeModal();">Close This Window</a></div>
</div>
</div>

<script type="text/javascript">
    function trackClick() {
        _gaq.push(['_trackEvent', 'Welcome Modal', 'Click', 'RuleSet Name:']);
    }
   
</script>




		
	   		

		<!-- Sailthru Horizon -->
		<script type="text/javascript">
			(function () {
				function loadHorizon() {
					var s = document.createElement('script');
					s.type = 'text/javascript';
					s.async = true;
					s.src = location.protocol + '//ak.sail-horizon.com/horizon/v1.js';
					var x = document.getElementsByTagName('script')[0];
					x.parentNode.insertBefore(s, x);
				}
				loadHorizon();
				var oldOnLoad = window.onload;
				window.onload = function () {
					if (typeof oldOnLoad === 'function') {
						oldOnLoad();
					}
					Sailthru.setup({
						domain: 'horizon.henrys.com'
					});
				};
			})();
		</script>   
  
        <!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1067831525;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1067831525/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


 

<script type="text/javascript">
//<![CDATA[

			if (txtResource == null) {
				var txtResource = {};
			}
			txtResource['Alert_Err_ItemCombination'] = 'Sorry, we don\\\'t carry this item combination';txtResource['Alert_Err_ValidSelection'] = 'Please make a valid selection before adding this to your cart';txtResource['Alert_Err_OutofStock'] = 'Sorry, this item is currently out of stock';$(document).ready(function(e) {
var cultureObject = Sys.CultureInfo.CurrentCulture;
var nfObject = cultureObject.numberFormat;
nfObject.CurrencyDecimalSeparator = ".";
nfObject.CurrencyDecimalDigits = "2";
nfObject.CurrencyGroupSeparator = ",";
nfObject.CurrencyGroupSizes = new Array('3');
nfObject.CurrencyNegativePattern = "1";
nfObject.CurrencyPositivePattern = "0";
nfObject.CurrencySymbol = "$";
});
          function resetPosition(object, target) {

			var tbposition = findPositionWithScrolling(target);
			var xposition = tbposition[0] + $(target).width();
			var yposition = tbposition[1] + 30;

			$common.setLocation(object, new Sys.UI.Point(xposition, yposition));
		}

		function findPositionWithScrolling(oElement) {
		if (typeof (oElement.offsetParent) != 'undefined') {
			var originalElement = oElement;
			for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
				posX += oElement.offsetLeft;
				posY += oElement.offsetTop;
				if (oElement != originalElement && oElement != document.body && oElement != document.documentElement) {
					posX -= oElement.scrollLeft;
					posY -= oElement.scrollTop;
				}
			}
			return [posX, posY];
		} else {
			return [oElement.x, oElement.y];
		}
	}

	function UpdatePopUpExtenderPosition(objectid, targetid) {
		var a = document.getElementById(objectid);
		var b = document.getElementById(targetid);
		if(a != null){
			resetPosition(a,b);
		}
	}
	ValidatedTextBoxOnKeyPress = function ValidatedTextBoxOnKeyPress(event) {
		if (event.keyCode == 13 && !$.browser.mozilla) {
			ValidatorOnChange(event);
			var v;
			if ((typeof (event.srcElement) != 'undefined') && (event.srcElement != null)) {
				v = event.srcElement.Validators;
			} else {
				v = event.target.Validators;
			}
			return AllValidatorsValid(v);
		}
		return true;
	};//]]>
</script>
</form>
    <!-- 
	Connected To: ODWEBB04
	-->	
</body>
</html>
