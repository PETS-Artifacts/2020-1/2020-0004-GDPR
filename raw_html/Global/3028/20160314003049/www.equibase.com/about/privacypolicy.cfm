<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

	<link rel="stylesheet" type="text/css" media="screen" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="/css/layout.css?v=3" />
	<link rel="stylesheet" type="text/css" media="screen" href="/css/queries.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="/css/chardinjs.css">
	<link rel="stylesheet" type="text/css" media="screen" href="/css/selectbox.css" />
	<link href='//fonts.googleapis.com/css?family=Raleway:800,400,200' rel='stylesheet' type='text/css'  media="screen">

	<!-- link to the CSS files for menu -->
	<link rel="stylesheet" media="screen" href="/css/megafish.css">
	<link rel="stylesheet" media="print" type="text/css" href="/css/printNewPromo.css" />

	<script src="/js/jquery.js"></script>
	<script src="/js/hoverIntent.js"></script>
	<script src="/js/bootstrap.js"></script>
	<script src="/js/superfish.js"></script>
	<script src="/js/chardinjs.min.js"></script>
	<script src="/js/main.js"></script>
	<script type="text/javascript" src="/js/jquery.selectbox-0.5.js"></script>
	<script src="/js/home/search.js"></script>
	<script src="/js/home/racing-news.js"></script>

	<script src="http://jwpsrv.com/library/8jaPKnGsEeSJyg6sC0aurw.js"></script>
	<script>jwplayer.key="6h2q1dgYIzmCPurLH8UJZ8dJRSvyufS33mmXvA==";</script>


	<title>Horse Racing | Horse Racing Entries | Horse Racing Results | Past Performances | Mobile | Statistics</title>
	<meta name="Description" content="Welcome to Equibase.com, your official source for horse racing results, mobile racing data, statistics as well as all other horse racing and thoroughbred racing information. Find everything you need to know about horse racing at Equibase.com." />
	<meta name="author" content="Equibase.com" />
	<meta name="Keywords" content="equibase, DRF, thoroughbred racing, mobile, statistics, race program, race, racing, race tracks, racetracks, race entries, race results, horse racing, expert selections, Daily Racing Form, TrackMaster, top leaders, jockeys, trainers, turf, owners, race results, horse, pedigrees, programs, sport of kings, databases, equine, horses, purebred, charts, crown, axcis, derby, breeders cup, bris, jockey club" />
<meta name="norton-safeweb-site-verification" content="qgptd7c2-k6znysvtyi62yqtlg0s00z3yy3dsb6mrta5r51sx2zmzxy7-9bstj0a5wk558fk2e4dxme7wh9l4seye5ph8iqj-bsxtpuvkn19gsbvitez94ip4y0nvb1x" />
	<script>
		oldIE = false;
	</script>

	<!--[if IE 9]>
		<script>
			var oldIE = true;
		</script>
	<![endif]-->

	<script>
	function keepSearch(sel) {
		document.getElementById('searchVal').value = sel.value;
	}

       (function($){ //create closure so we can safely use $ as alias for jQuery

         $(document).ready(function(){


		var exampleOptions = {
			 speed: 'fast'
		}
		// initialise plugin
		var example = $('#example').superfish(exampleOptions);

		// buttons to demonstrate Superfish's public methods
		$('.destroy').on('click', function(){
			 example.superfish('destroy');
		});

		$('.init').on('click', function(){
			 example.superfish(exampleOptions);
		});

		$('.open').on('click', function(){
			 example.children('li:first').superfish('show');
		});

		$('.close').on('click', function(){
			 example.children('li:first').superfish('hide');
		});

		//Toggle Tabbed Content
		$('.tab-info').hide();
		$('#tab-one-info').show();
		$('.tab-toggler').click(function (){
			$('#tab-one-info').hide();
			$('#' + $(this).attr('id') + '-info').addClass('active');
			$('#' + $(this).attr('id') + '-info').show().siblings('.tab-info').hide();
			$('#' + $(this).attr('id') + '-info').show().siblings('.tab-info').removeClass('active');
			return false;
		});

		$(".tab-toggler").click(function() {
			$(".tab-toggler").removeClass("active");
			$(this).addClass("active");
		});


	});

       })(jQuery);


       </script>
       <style>
   	.show10People {height: 10px; overflow: hidden;}
   	.showAllPeople {height: auto; width: 100%; margin: 0 auto;}
	</style>


<!-- OpenX Market Header Bidding Script -->
<script type="text/javascript" src="//equibase-company-d.openx.net/w/1.0/jstag?nc=2542414-equibase"></script>



<!-- DFP Header Script -->
<!-- DFP Code Enables Ads Accross Site, imports Necessary code for the rest of the ad code to work -->

<script type='text/javascript'>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
  (function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') +
      '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
  })();
</script>


<!-- Script from TS enables section targeting Cold Fusion -->
<script type="text/javascript">
	var pageTarget = {"section": ""};
</script>


</head>

<body>


<div class="container">
<center><div class="texttop"><span class="top text mediumgraytxt"><small>Use (including viewing) of the material contained herein constitutes
acceptance of the <a href="/about/terms.cfm" class="mediumgraytxt">TERMS OF USE</a></small></span></div></center>

<!-- BEGIN NAV HEADER -->
	<div class="navigation row" id="noprint">
		<div class="logo">
		<!-- BEGIN LOGO AND SOCIAL -->
			<div class="col-xs-3">
				<a href="/index.cfm">
                <img src="/img/eqb-logo.png" class="img-responsive" alt="" class="print"/>

                            </a><br />
					<ul class="socialMedia hidden-print">
						<li><a href="https://www.facebook.com/Equibase"><img src="/img/facebook.png" class="img-responsive" alt="facebook" /></a></li>
						<li><a href="https://twitter.com/Equibase"><img src="/img/twitter.png" class="img-responsive" alt="twitter" /></a></li>
					</ul>
			</div>
		<!-- END LOGO AND SOCIAL -->





		<!-- BEGIN AD -->
			<div class="col-xs-9 hidden-print">
<center>
			<!-- /2542414/EQ_Desktop_ROS_728x90_ATF_Header -->
				<div id='headerAdDiv' style='height:90px; width:728px; border:1px solid #e6e6e6;'>
				<!-- Left Blank - Displays non targeted Header ads pushed in footer -->
				</div>
</center>
			</div>
		<!-- END AD -->
		</div>

		<div class="store-title hidden-print">
			<h5 class="lightgoldtxt hidden-print">Handicapper's Store</h5>
		</div>



		<div class="navitems hidden-print">

	<!-- BEGIN NAVIGATION -->
			<div class="col-xs-8 hidden-print">

				<ul class="sf-menu hidden-print" id="example">

				<!-- pps-->
					<li class="current store hidden-print">
					  <a href="/premium/eqpEquibaseFullPP.cfm?SAP=TN">PPs</a>
						  <div class="sf-mega">
						  <div class="">
							<div class="col-xs-4">
								<h2 class="lightgoldtxt">Equibase PPs</h2>
								<ol>
									<li><a href="/premium/eqpEquibaseFullPP.cfm?SAP=TN">Equibase Premium PP</a></li>
									<li><a href="/premium/eqpRaceProgram.cfm?SAP=TN">Equibase Basic PP</a></li>
									<li><a href="/premium/pubentriesplus.cfm?SAP=TN">Entries Plus Premium</a></li>
									<li><a href="/premium/eqpSpanishSelections.cfm?SAP=TNH">En Espa&#241;ol</a></li>
								</ol>
								<br />

								<h2 class="lightgoldtxt">Mobile Apps</h2>
								<ol>
									<li><a href="/premium/eqpipp.cfm?SAP=TN">iPPs</a></li>
									<li><a href="/premium/eqpTrackMasterPocketHandicapper.cfm">Pocket Handicapper</a></li>
								</ol>
								<br />
								<h2 class="lightgoldtxt">Individual<BR>Horse PPs</h2>
																<ol>
																	<li><a href="/premium/eqpHorseLookup.cfm?PID=50214&SAP=TN">Lifetime PPs</a></li>
																 </ol>
																<br />

							</div>

							<div class="col-xs-4">
								<h2 class="lightgoldtxt">TrackMaster & DRF</h2>
								<ol>
									<li><a href="/premium/eqpTrackMasterPlatinumPPs.cfm?SAP=TN">TrackMaster Platinum PP</a></li>
									<li><a href="/premium/eqptrackmasterequigraphix.cfm?SAP=TN">TrackMaster EquiGraphix</a></li>
									<li><a href="/premium/eqpTrackMasterPlusPro.cfm?SAP=TN">TrackMaster Plus Pro</a></li>
									<li><a href="/premium/eqpdrfBuildEdition.cfm?SAP=TN">Daily Racing Form</a></li>
								</ol>
								<br />

								<h2 class="lightgoldtxt">Harness PPs</h2>
								<ol>
									<li><a href="/premium/eqpHarnessPPs.cfm?SAP=TNH">TrackMaster PPs</a></li>
									<li><a href="/premium/eqpHarnessPlatinumPPs.cfm?SAP=TN">TrackMaster Platinum</a></li>
									<li><a href="/premium/eqpharnesspro.cfm?SAP=TNH">TrackMaster Harness Pro</a></li>
								</ol>
								<br />

							</div>

							<div class="col-xs-4">
								<h2 class="lightgoldtxt">Shop & Compare</h2>
									<a href="/products/handicapperstore.cfm?SAP=TN" class="green-borderbutton">Handicapper's Store</a><br /><br />
									<a href="/products/ppcomparison.cfm?SAP=TN" class="green-borderbutton">Compare PPs</a><br /><br />
									<a href="/premium/mppindex.cfm?SAP=TN" class="green-borderbutton">Shop by Track</a><br /><br />
									<a href="/products/daypasscomparison-new.cfm?SAP=TN" class="green-borderbutton">Compare Day Pass Plans</a><br /><br />
						<a href="/premium/daypassbundles.cfm?SAP=TN" class="green-borderbutton">Shop Day Pass Plans</a><br /><br />
					</div>
						</div>

							<div class="nav-helpbar">
								<h5 class="whitetxt">
									<a rel="popover" data-container="body" data-toggle="popover" class="pull-right"
									data-content="Past performances are a horse's racing record, earnings, bloodlines and other data, presented in composite form.">
			<img src="/img/icons/white-questionmark.png" class="tinyicon-dd" /></a> <a href="/products/whatisprogram.cfm" class="whitetxt">What are PPs?</a>&nbsp;&nbsp;
								</h5>
							</div>
						  </div>
					</li>

					<!-- selections -->

					<li class="store hidden-print">
					  <a href="/static/eqpaxisselections.html" class="longtitle">Selections</a>
						  <div class="sf-mega">
						  <div class="">
							<div class="col-xs-4">
								<h2 class="lightgoldtxt">Expert Picks</h2>
								<ol>
									<li><a href="/premium/eqpfastsheetsselections.cfm?SAP=TN">F.A.S.T Sheets</a></li>
									<li><a href="/premium/eqpwalkergroup.cfm?SAP=TN">Walker Group</a></li>
									<li><a href="/premium/eqpTurfClubAnalysts.cfm?SAP=TN">Turf Club Analysts</a></li>
									<li><a href="/premium/eqpEllisStarr.cfm?SAP=TN">Ellis Starr</a></li>
									<li><a href="/premium/eqpAXCISHarnessSelections.cfm?SAP=TNH">Harness Selections</a></li>
								</ol>
								<br />
								<a href="/static/eqpaxisselections.html?SAP=TN" class="green-borderbutton">View All Selections</a>
							</div>

							<div class="col-xs-4 hidden-print">
								<h2 class="lightgoldtxt">Top Tracks</h2>
								<h5 class="mediumgraytxt">(Racing This Week)</h5>
								
								<ol>
									
										<li><a href="/premium/eqpAXCISTrackSelections.cfm?tid=AQU">Aqueduct</a></li>
									
										<li><a href="/premium/eqpAXCISTrackSelections.cfm?tid=FG">Fair Grounds</a></li>
									
										<li><a href="/premium/eqpAXCISTrackSelections.cfm?tid=GP">Gulfstream Park</a></li>
									
										<li><a href="/premium/eqpAXCISTrackSelections.cfm?tid=LRL">Laurel Park</a></li>
									
										<li><a href="/premium/eqpAXCISTrackSelections.cfm?tid=OP">Oaklawn Park</a></li>
									
										<li><a href="/premium/eqpAXCISTrackSelections.cfm?tid=PRX">Parx Racing</a></li>
									
										<li><a href="/premium/eqpAXCISTrackSelections.cfm?tid=SA">Santa Anita Park</a></li>
									
										<li><a href="/premium/eqpAXCISTrackSelections.cfm?tid=TAM">Tampa Bay Downs</a></li>
									
								</ol>
								<br />
								<a href="/static/eqpaxisselections.html" class="green-borderbutton">View All Tracks</a>
							</div>

							<div class="col-xs-4 hidden-print">
								<h2 class="lightgoldtxt">Shop & Compare</h2>
									<a href="/products/handicapperstore.cfm?SAP=TN" class="green-borderbutton">Handicapper's Store</a><br /><br />
									<a href="/products/selections.cfm?SAP=TN" class="green-borderbutton">Compare Selections</a><br /><br />
				<a href="/products/daypasscomparison-new.cfm?SAP=TN" class="green-borderbutton">Compare Day Pass Plans</a><br /><br />

								<h2 class="lightgoldtxt hidden-print">About Selections</h2>
								<ol>
									<li><a href="/products/whoshot2.cfm">Who's Hot</a></li>
									<li><a href="/products/freepicks2.cfm">Free Picks</a></li>
									<li><a href="/products/handicapperbios.cfm">Meet the Handicappers</a></li>
									<li><a href="/premium/eqpAXCISFreeSelections.cfm">Yesterday's Picks</a></li>
									<li><a href="/products/whatistrackmaster.cfm">Who is TrackMaster?</a></li>
								</ol>
								<br />
							</div>

						</div>

							<div class="nav-helpbar hidden-print">
								<h5 class="whitetxt">
									<a rel="popover" data-container="body" data-toggle="popover" class="pull-right"
									data-content="Selections provide human and/or computerized analysis for every race on a card that provide a faster, better way to handicap.">
			<img src="/img/icons/white-questionmark.png" class="tinyicon-dd" /></a>
									<a href="/products/whatisselection.cfm" class="whitetxt">What are Selections?</a>&nbsp; &nbsp;
								</h5>
							</div>
						  </div>
					</li>

			<!-- reports -->
					<li class="store hidden-print">
					  <a href="/products/handicapperstore.cfm#Reports">Reports</a>
						<div class="sf-mega">
							<div class="">
								<div class="col-xs-4 hidden-print">
																	<h2 class="lightgoldtxt">Flashnet</h2>
																	<ol>
																		<li><a href="/premium/eqpFlashNet.cfm?SAP=TN">Thoroughbred</a></li>
																		<li><a href="/premium/eqpHarnessFlashnet.cfm?SAP=TNH">Harness</a></li>
																	</ol>
																	<br />
																	<h2 class="lightgoldtxt">Thoro-Graph</h2>
																			<ol>
																				<li><a href="/premium/eqpthorographsheet.cfm?SAP=TN">Full Sheets</a></li>
																				<li><a href="/premium/eqpthorographquick.cfm?SAP=TN">Thoro-Quick</a></li>
																				<li><a href="/premium/eqpthorograph.cfm?SAP=TN">Form Ratings</a></li>
																						</ol>
															<br />
							</div>
								<div class="col-xs-4 hidden-print">
									<h2 class="lightgoldtxt">Reports & Specialty Products</h2>
									<ol>
										<li><a href="/premium/eqpperformancecycles.cfm?SAP=TN">Performance Cycles</a></li>
										<li><a href="/premium/eqpcycleslite.cfm?SAP=TN">Cycles Lite</a></li>
										<li><a href="/premium/eqptrackmasterrdss.cfm">RDSS</a></li>
										<li><a href="/premium/mphpro.cfm">MPH Pro</a></li>
										<li><a href="/premium/eqpEGraph.cfm?SAP=WHAT">E Graphs</a></li>
									</ol>
									<br />
									<a href="/products/handicapperstore.cfm#reports" class="green-borderbutton">View All Reports</a>
								</div>




								<div class="col-xs-4 hidden-print">
									<h2 class="lightgoldtxt">Shop & Compare</h2>
										<a href="/products/handicapperstore.cfm#Reports" class="green-borderbutton">Handicapper's Store</a><br /><br />
										<a href="/products/reportscomparison.cfm" class="green-borderbutton">Compare Reports</a><br /><br />
										<a href="/products/daypasscomparison-new.cfm?SAP=TN" class="green-borderbutton">Compare Day Pass Plans</a><br /><br />
						<a href="/premium/daypassbundles.cfm?SAP=TN" class="green-borderbutton">Shop Day Pass Plans</a><br /><br />
					</div>
							</div>

								<div class="nav-helpbar hidden-print">
									<h5 class="whitetxt">
										<a rel="popover" data-container="body" data-toggle="popover" class="pull-right"
									data-content="Reports are specialty products that give you a
			deeper analysis of how to play every race on a card.">
			<img src="/img/icons/white-questionmark.png" class="tinyicon-dd" /></a>
										<a href="/products/reportscomparison.cfm" class="whitetxt">What are Reports?</a>&nbsp; &nbsp;
									</h5>
								</div>
						</div>
					</li>

				<!-- entries -->

					<li>
					  <a href="/static/entry/index.html?SAP=TN">Entries</a>
					  <div class="sf-mega hidden-print">
							<div class="hidden-print">
								<div class="col-xs-4">
									<h2 class="lightgoldtxt">All Entries</h2>
									<ol>
										<li><a href="/static/entry/index.html?SAP=TN">Thoroughbred Entries (N. American)</a></li>
										<li><a href="/static/foreign/entry/index.html?SAP=TN">Thoroughbred Entries (International)</a></li>
										<li><a href="/premium/eqbstakesentries.cfm?SAP=TN">Stakes Entries</a></li>
										<li><a href="/premium/pubentriesplus.cfm?SAP=TN">Entries Plus</a></li>
										<li><a href="/static/harness/entry/index.html?SAP=TNH">Harness Entries</a></li>
									</ol>
									
								</div>

								<div class="col-xs-4">

									<h2 class="lightgoldtxt">Today's Cards</h2>
									<ol>
										<li><a href="/static/latechanges/html/latechanges.html?SAP=TN">Scratches & Changes</a></li>
										<li><a href="/static/latechanges/html/cancellations.html?SAP=TN">Cancellations</a></li>
										<li><a href="/premium/eqpInToday.cfm?SAP=TN">In Today</a></li>
										<li><a href="/premium/eqpInTodayTrack.cfm?SAP=TN">In Today By Track</a></li>
										<li><a href="/products/racingtoday.cfm?SAP=TN">Racing Today Dashboard</a></li>
									</ol>
									<br />
								</div>

								<div class="col-xs-4">
									<h2 class="lightgoldtxt">Other</h2>
									<ol>
										<li><a href="/static/workout/index.html?SAP=TN">Workouts</a></li>
											<li><a href="/products/racedates.cfm?SAP=TN">Race Dates & Calendar</a></li>
										<li><a href="/premium/eqbHighestPayouts.cfm?SAP=TN">Top Payouts</a></li>
										<li><a href="/premium/eqpCarryoverCorner.cfm?SAP=TN">Carryovers  &amp; Guaranteed Pools</a></li>
										<li><a href="/virtualstable/horse.cfm">Virtual Stable</a></li>
									</ol>
								</div>
							</div>

								<div class="nav-helpbar">
							<h5 class="whitetxt">
								<a rel="popover" data-container="body" data-toggle="popover" class="pull-right"
									data-content="The information provided from a racetrack that describes every race that will be run
									on a given day along with the horses (and their connections) that are scheduled to compete.">
			<img src="/img/icons/white-questionmark.png" class="tinyicon" /></a>
			 								<a href="/products/whatisentries.cfm" class="whitetxt">What are Entries?</a>&nbsp; &nbsp;
							</h5>
							</div>
						</div>
					</li>

				<!-- results -->

					<li>
					  <a href="/static/chart/pdf/index.html?SAP=TN">Results</a>
						<div class="sf-mega">
							<div class="">
								<div class="col-xs-4">
									<h2 class="lightgoldtxt">Race Results</h2>
									<ol>
										<li><a href="/static/chart/pdf/index.html?SAP=TN">Full Charts </a></li>
										<li><a href="/static/chart/summary/index.html?SAP=TN">Quick / Summary Results</a></li>
										<li><a href="/results/stakesresults.cfm">Stakes Results</a></li>
										<li><a href="/premium/eqbRaceChartCalendar.cfm?SAP=TN">Historical Charts </a></li>
										<li><a href="/products/racingtoday.cfm?SAP=TN">Racing Today Dashboard</a></li>
									</ol>
									<br />
								</div>

								<div class="col-xs-4">

									<h2 class="lightgoldtxt">Race Replays</h2>
									<ol>
										<li><a href="/premium/eqpVideoRaceReplaysSearch.cfm?SAP=TN">Video Replays Search</a></li>
										<li><a href="/premium/eqpViewSubscription.cfm?Incltxt=eqpViewVideoRaceReplaysDisplay.cfm&pid=50360">Daily Plan</a></li>
										<li><a href="/premium/eqpViewSubscription.cfm?Incltxt=eqpViewVideoRaceReplaysDisplay.cfm&pid=50361">Monthly Plan</a></li>
										<li><a href="/premium/eqpViewSubscription.cfm?Incltxt=eqpViewVideoRaceReplaysDisplay.cfm&pid=50364">Quarterly Plan</a></li>
										<li><a href="/premium/eqpViewSubscription.cfm?Incltxt=eqpViewVideoRaceReplaysDisplay.cfm">View All Plans</a></li>
									</ol>
									<br />

								</div>

								<div class="col-xs-4">
									<h2 class="lightgoldtxt">Downloadable Charts</h2>
									<ol>
										<li><a href="/premium/eqpTMResultChartSearch.cfm?SAP=TN">By Card</a></li>
										<li><a href="/premium/eqpViewSubscription.cfm?Incltxt=eqpViewDownloadableChartsDisplay.cfm&pid=50421">1 Month Subscription</a></li>
										<li><a href="/premium/eqpViewSubscription.cfm?Incltxt=eqpViewDownloadableChartsDisplay.cfm&pid=50422">3 Month Subscription</a></li>
										<li><a href="/premium/eqpViewSubscription.cfm?Incltxt=eqpViewDownloadableChartsDisplay.cfm&pid=50423">6 Month Subscription</a></li>
										<li><a href="/premium/eqpViewSubscription.cfm?Incltxt=eqpViewDownloadableChartsDisplay.cfm&pid=50424">Annual Subscription</a></li>
									</ol>
									<br />
								</div>
							</div>

								<div class="nav-helpbar">
									<h5 class="whitetxt">
										<a rel="popover" data-container="body" data-toggle="popover" class="pull-right"
									data-content="Replays are videos of the race, typically from a variety of angles, from when the
									horses break from the gate until they cross the wire and are a great handicapping tool.">
			<img src="/img/icons/white-questionmark.png" class="tinyicon" /></a>
												<a href="/products/whatarereplays.cfm" class="whitetxt">What are Replays?</a>&nbsp; &nbsp;
									</h5>
								</div>
						</div>
					</li>

			<!-- stats -->

					<li>
					  <a href="/stats/View.cfm?tf=year&tb=horse">Stats</a>
					  <div class="sf-mega">
							<div class="">
								<div class="col-xs-4">
									<h2 class="lightgoldtxt">All Starters</h2>
									<ol>
										<li><a href="/stats/View.cfm?tf=year&tb=horse">Horses - By Racing Year</a></li>
										<li><a href="/stats/View.cfm?tf=year&tb=foal_crop">Horses - By Foaling Year</a></li>
										<li><a href="/stats/View.cfm?tf=year&tb=jockey">Jockeys</a></li>
										<li><a href="/stats/View.cfm?tf=year&tb=trainer">Trainers</a></li>
										<li><a href="/stats/View.cfm?tf=year&tb=owner">Owners</a></li>
									</ol>
									<br />
									<h2 class="lightgoldtxt">Starters by Track</h2>
									<ol>
										<li><a href="/stats/View.cfm?tf=meet&tb=horse">Horses - By Racing Year</a></li>
										<li><a href="/stats/View.cfm?tf=meet&tb=foal_crop">Horses - By Foaling Year</a></li>
										<li><a href="/stats/View.cfm?tf=meet&tb=jockey">Jockeys</a></li>
										<li><a href="/stats/View.cfm?tf=meet&tb=trainer">Trainers</a></li>
										<li><a href="/stats/View.cfm?tf=meet&tb=owner">Owners</a></li>
									</ol>
									<br />
								</div>

								<div class="col-xs-4">

									<h2 class="lightgoldtxt">All-Time Leaders</h2>
									<ol>
										<li><a href="/stats/ViewAllTime.cfm?tf=all-time&tb=horse&vb=W">Horses by Wins</a></li>
										<li><a href="/stats/ViewAllTime.cfm?tf=all-time&tb=horse&vb=E">Horses by Earnings</a></li>
										<li><a href="/stats/ViewAllTime.cfm?tf=all-time&tb=jockey&vb=W">Jockeys by Wins</a></li>
										<li><a href="/stats/ViewAllTime.cfm?tf=all-time&tb=jockey&vb=E">Jockeys by Earnings</a></li>
										<li><a href="/stats/ViewAllTime.cfm?tf=all-time&tb=trainer&vb=W">Trainers by Wins</a></li>
										<li><a href="/stats/ViewAllTime.cfm?tf=all-time&tb=trainer&vb=E">Trainers by Earnings</a></li>
									</ol>
									<br />
								</div>

								<div class="col-xs-4">
									<h2 class="lightgoldtxt">Other</h2>
									<ol>
										<li><a href="/static/statistics/eleaders.html">E-Speed Figures</a></li>
										<li><a href="/about/northamericanrecords.cfm">North American Records</a></li>
										<li><a href="/about/ifharatings-home.cfm">IFHA World Rankings</a></li>
									</ol>
									<br />
								</div>
							</div>

								<div class="nav-helpbar">
									<h5 class="whitetxt">
										<a href="#" class="whitetxt"></a>&nbsp; &nbsp;
									</h5>
								</div>
						</div>
					</li>

				<!-- horsemen -->

					<li>
					  <a href="/static/horsemen/horsemenarea.html?SAP=TN" class="longtitle">Horsemen</a>
					  <div class="sf-mega">
							<div class="">
								<div class="col-xs-4">
									<h2 class="lightgoldtxt">Horsemen's Information</h2>

									<ol>
										<li><a href="/static/horsemen/horsemenarea.html?SAP=TN">Horsemen Index</a></li>
										<li><a href="/static/horsemen/horsemenareaCB.html?SAP=TN">Condition Books</a></li>
										<li><a href="/static/horsemen/horsemenareaON.html?SAP=TN">Overnights</a></li>
										<li><a href="/static/horsemen/horsemenareaSB.html?SAP=TN">Stakes Book</a></li>
										<li><a href="/static/horsemen/horsemenareaSNHW.html?SAP=TN">Stakes Nominations</a></li>
									</ol>
									<br />
								</div>

								<div class="col-xs-4">
									<h2 class="lightgoldtxt">Products & Services</h2>
									<ol>
										<li><a href="/virtualstable/horse.cfm">Virtual Stable</a></li>
										<li><a href="/premium/eqpHorseLookup.cfm?PID=50214">Lifetime PP</a></li>
										<li><a href="/premium/eqpHorseLookup.cfm?PID=50133">Premium Pedigrees</a></li>
											<li><a href="/marketplace/index2.cfm">Marketplace</a></li>
									</ol>
									<br />
								</div>


								<div class="col-xs-4">
									<!--<h2 class="lightgoldtxt">Ad</h2>-->
									<a href="/marketplace/index2.cfm"><img src="http://www.equibase.com/img/marketplacead2.gif" class="img-responsive" /></a>
								</div>
							</div>

								<div class="nav-helpbar">
									<h5 class="whitetxt">
										<a href="#" class="whitetxt"></a>&nbsp; &nbsp;
									</h5>
								</div>
						</div>
					</li>

					<!-- more -->

					<li>
					  <a href="#">More</a>
					  <div class="sf-mega">
							<div class="">
								<div class="col-xs-4">
									<h2 class="lightgoldtxt">Free Services</h2>
									<ol>
										<li><a href="/virtualstable/horse.cfm">Virtual Stable</a></li>
										<li><a href="/rotd/index.cfm?SAP=TN">Race of the Day</a></li>
										<li><a href="/free/index.cfm?SAP=TN">Featured Race of the Week</a></li>
										
										<li><a href="/products/freepicks2.cfm?SAP=TN">TrackMaster Free Selections</a></li>
										<li><a href="/yearbook/">Equibase Racing Yearbook</a></li>
										<li><a href="/marketplace/index2.cfm">Marketplace</a></li>

									</ol>
									<br />
								</div>

								<div class="col-xs-4">
									<h2 class="lightgoldtxt">Mobile</h2>
									<ol>
										<li><a href="/apple/today.cfm">Today's Racing app</a></li>
										<li><a href="/apple/get.cfm">Equibase Racing Yearbook app</a></li>
										<li><a href="/premium/eqpTrackMasterPocketHandicapper.cfm">TrackMaster apps</a></li>
										<li><a href="/mobile/index.cfm">Equibase Mobile</a></li>
									</ol>
								</div>

								<div class="col-xs-4">
								<h2 class="lightgoldtxt">Account Services</h2>
																	<ol>
																		<li><a href="/premium/CUSTOMERADMINMAIN.CFM">My Account</a></li>
																	

																	<li><a href="/premium/eqbCustomerOffers.cfm">My Offers</a></li>
																	<br />

								</ol>
									<h2 class="lightgoldtxt">Social</h2>
									<ol>
										<li><a href="/communitypage.cfm?SAP=TN">Community Hub</a></li>
										<li><a href="http://facebook.com/Equibase">Equibase on Facebook</a></li>
										<li><a href="https://twitter.com/Equibase">@Equibase on Twitter</a></li>
									</ol>
									<br />

								</div>
							</div>

								<div class="nav-helpbar">
									<h5 class="whitetxt">
										<a href="" class="whitetxt"></a>&nbsp; &nbsp;
									</h5>
								</div>
						</div>
					</li>
				  </ul>
			</div>
	<!-- END NAVIGATION -->

	<!-- BEGIN SEARCH -->
			
			<div class="col-xs-3 hidden-print">
				<div class="search hidden-print">
					<div class="searchbar">
						<form  id="search" method="post">

							<select id="searchOptions" onchange="javascript:keepSearch(this);">
								<option value="horses" selected="selected">Horses</option>
								<option value="jockeys" >Jockeys</option>
								<option value="trainers" >Trainers</option>
								<option value="owners" >Owners</option>
								<option value="tracks" >Tracks</option>
								<option value="stakes" >Gr. Stakes</option>
							</select>

							

							<input type="hidden" id="searchVal" value="horses" name="searchVal">
							<input class="input" id="searchInput" type="text" placeholder="Horse Name"/>
							<span class="glyphicon glyphicon-search"></span>
							<input type="submit"value="">
							<span style="clear: both;">.</span>
						</form>
					</div>
				</div>
			</div>
	<!-- END SEARCH -->


	<!-- BEGIN CART -->
			<div class="col-xs-1 hidden-print">
				<div class="cart hidden-print">
					<a href="/premium/CUSTOMERADMINMAIN.CFM?&logon=Y">Account</a>
						<br /><img src="/img/icons/cart.png" class="16x16" /><br />
					<a href="/premium/eebCustomerCheckoutLogon.cfm">Checkout</a>
				</div>
			</div>
	<!-- END CART -->
		</div>
	</div>
<!-- END NAV HEADER -->

<div class="hidden-print">
<div class="hidden-print">

	<div id="promoCodeDiv" class="promoCode default displayNone">
		<img id="noprint" style="width:150px; height:41px; margin-bottom:5%" src="/images/eqb-logo_150x41.png"/>
		<br>
		<p>
			<b>Special Offer</b> - You qualify for Promo Code:
			<p style="text-align:center; font-size:1.2em;">
				<strong id="promoCodeFloater" style="text-align:center;color: #00724b;"></strong>
			</p>
		</p>
		<a id="openPromotionButton" href="#" class="gold-borderbutton" data-toggle="modal" data-target="#promoCode">Open Promotion</a>
		<a class="green-borderbutton" href="/premium/eqbCustomerOffers.cfm">View All Offers</a>
	</div>

<link rel="stylesheet" media="print" type="text/css" href="/css/printNew.css" />
	<!-- Modal -->
	
		<div class="modal fade" id="promoCode" tabindex="-1" role="dialog" aria-labelledby="promoCodeLabel" aria-hidden="true">
		  <div class="modal-dialog" id="noprint">
		    <div class="modal-content">
		      <div class="modal-header" style="height:65px;">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <div>
		        	<div style="float:right;margin-right:225px;margin-top:20px">
		        		<h4 class="modal-title" style="text-align:center" id="promoCodeMTitle">Special Offer</h4>
		        	</div>
		        	<div style="float:left;">
		        		<img style="width:150px; height:41px;" src="/images/eqb-logo_150x41.png"/>
		        	</div>
		        </div>
		      </div>
		      <div class="modal-body" id="promoCodeMBody">
		      	<p>
		      		<center><span id="promoCodeLongDesc"></span></center>
		      	</p>
		      	<br>
		      	<p style="text-align:center; font-size:1.2em;">
		      			Promo Code: <strong style="color: #00724b;" ><span id="promoCodeModalSpan" name="promoCodeModalSpan"></span></strong>
		      		<br>
		      		<p id="promoCodePromoDesc" style="text-align:center"></p>
		      	</p>
		      	<p style="text-align:center; margin-bottom:10px;">
		      		<a id="modalShopButton" href="" class="gold-borderbutton">Shop Now</a>
		      		<p style="text-align:center;font-style:italic;font-size:8pt" id="promoCodeDisclaimer"></p>
		      	</p>

		      </div>
		      <div class="modal-footer" style="text-align:center;">
		      	<a id="modalViewAllLink" href="/premium/eqbCustomerOffers.cfm" class="green-borderbutton displayNone">View All Offers</a>
		        <button type="button" class="green-borderbutton" data-dismiss="modal">Close</button>
		      </div>
		    </div>
		  </div>
		</div>
	
</div>
<script type="text/javascript" src="/js/promoCodeInclude.js"></script> <style>
.browserUpgrade{
	border: 2px groove #777;
	padding: 0 5px 10px;
	margin: auto;
	text-align: center;
	font-size: 12px;
	font-family: arial;
	box-shadow: 3px 3px 0 rgba(0,0,0,0.4);
}
</style>

<div class="modal fade displayNone" id="browserUpgrade" tabindex="-1" role="dialog" aria-labelledby="promoCodeLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
		<div>
			<div style="float:right;margin-right:225px;margin-top:20px">
				<h4 class="modal-title offerTitle" style="text-align:center" id="browserUpgradeMTitle">Browser Compatibility Issues</h4>
			</div>
			<div>
				<img style="height:45px" src="/img/eqb-logo.png"/>
			</div>
		</div>
	  </div>
	  <div class="modal-body" id="browserUpgradeMBody">
		<p>
			<div style="text-align: center; font-weight: bold">We've detected that you are using an older vesion of Internet Explorer. This version will not provide the best possible experience on our site.</div>
			<br />
			<div style="text-align: center; font-weight: bold">Please upgrade your version of Internet Explorer or switch to an alternative browser. This site is also compatible with current versions of Chrome, Safari, and Firefox.</div>
		</p>
	  </div>
	  <div class="modal-footer" style="text-align:center;">
		<button type="button" class="green-borderbutton" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<!--[if lte IE 8]>
<script>
	if( getCookie("BROWSERUPGRADE") === ""){
		setCookie("BROWSERUPGRADE", "true", 1);

		$("#browserUpgrade").modal('show');
	}
</script>
<![endif]-->

</div>

<title>About equibase.com</title>

<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "201583fb-6854-49a0-8ded-677db0b7b115", doNotHash: true, doNotCopy: true, hashAddressBar: true});</script><title>About equibase.com</title>

<!-- DFP Sidebar Ad Requests -->
<!-- DFP Sidebar Ad Requests -->
<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/2542414/EQ_Desktop_ROS_300x250_ATF_Sidebar1', [[300, 600], [300, 250]], 'div-gpt-ad-1444076119943-0').addService(googletag.pubads());
    googletag.defineSlot('/2542414/EQ_Desktop_ROS_300x250_BTF_Sidebar2', [[300, 600], [300, 250]], 'div-gpt-ad-1444076119943-1').addService(googletag.pubads());
    googletag.defineSlot('/2542414/EQ_Desktop_ROS_300x250_BTF_Sidebar3', [300, 250], 'div-gpt-ad-1444076119943-2').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });
</script>

  <!-- HEADER -->
  	<div class="header">
  		<h1>Privacy Policy</h1><div class="clear">
  		<a href="/index.cfm">Home</a> &raquo; <a href="#">About Equibase</a> &raquo; <a href="#">Privacy Policy</a>
  	</div>	</div>
  <!-- END HEADER -->


  <div class="row">
  <!-- INTERIOR CONTENT AREA -->
  		<div class="interior-content col-sm-8">
  			<div id="addThis">

			<span class='st_facebook' title="Facebook" ></span>
			<span class='st_twitter' title="Twitter"></span>
			<span class='st_linkedin' title="LinkedIn"></span>
			<span class='st_sharethis' ></span>
			<span class='st_email' ></span>
			<a title="Print Screen" alt="Print Screen" onclick="window.print();" target="_blank" style="cursor:pointer;"><img border="0" src="http://www.equibase.com/images/prient_icon.jpg"></a>
			</div>

  			<p class="asof" title="Data may be delayed up to 60 minutes">&nbsp;<!--As of #DateFormat(Now(), "mm.dd.yy")#--></p>


          <!-- CONTENT GOES BELOW HERE -->
<table border="0" cellpadding=5><tr><td>
<center><DIV CLASS="smboldblack">PRIVACY STATEMENT FOR <I>equibase.com</I></div></CENTER>

<br><br><DIV CLASS="smblack"><b>Please Note:</b> This privacy policy was last updated on February 3, 2014 and is subject to future change without notice.
<br><br>Equibase Company LLC has created this privacy statement in order to demonstrate our firm commitment to privacy. The following discloses our information gathering and dissemination practices for this website: <b><I>equibase.com</I></B>.
<br><br>We use your IP address to help diagnose problems with our server, and to administer our web site. Your IP address is used to help identify you and your shopping cart and to gather broad demographic information.  We monitor access to the site via IP address for data scrapers and automated access of information on the site.  We block IP addresses engaging in these activities.
<br><br>Our site uses cookies to keep track of your shopping cart. We use cookies to deliver content specific to your interests, to save your password so you don't have to re-enter it each time you visit our site, and for other purposes.
<br><br>Our site's registration form requires users to give us contact information (like their name and e-mail address) and basic demographic information. We use customer contact information from the registration form to send the user information about our company and the Thoroughbred racing industry. The customer's contact information is also used to contact the visitor when necessary. Users may opt-out of receiving future mailings; see the Opt Out Procedures for Mailings section below. Demographic and profile data is also collected at our site. We use this data to tailor the visitor's experience at our site, showing them content that we think they might be interested in, and displaying the content according to their preferences. This information is shared with advertisers on an aggregate basis. Further, Equibase may occasionally provide, sell, or rent limited user data to third parties, including companies that offer products and services for sale to Equibase’s users. This limited data includes names, job titles, companies/organizations, and business addresses, but do not include business phone numbers or business fax numbers.
<br><br>Aggregate demographic information collected via use of the site may be shared with advertisers and other partners. This aggregate information is not traceable to an individual user.

<br><br>When registering at our site, your registration information is also sent to TrackMaster, a wholly-owned subsidiary of Equibase Company LLC so that your registration information can be used to purchase products and services at their website <b><I>trackmaster.com</I></b>.  It will be necessary for you to complete additional registration information at <b><I>trackmaster.com</I></b> before being able to make purchases at that site.  No additional registration information is required to make purchases at <i><b>equibase.com</b></i>.  Products and services on <b><I>trackmaster.com</I></b> are covered by a separate set of Terms and Conditions that will be provided for your approval before any purchases can be made.  Registration at our web site does not obligate you to make purchases at <b><I>trackmaster.com</I></b>.  Any questions concerning the terms and conditions for the <b><I>trackmaster.com</I></b> web site should be referred to TrackMaster via the contact information provide via that site.
<br><br>This site contains links to other sites. <b><i>equibase.com</i></b> is not responsible for the privacy practices or the content of such web sites.
<br><br>Our site uses an order form for customers to request information, products, and services. We collect visitor's financial information (like their account or credit card numbers). Financial information that is collected is used to bill the user for products and services.  Billing is processed at the time of purchase, unless you have reached customer account status, at which time your card will be billed at the end of the month for purchases made during that month.  Monthly account billing will continue until the visitor notifies Equibase Company in writing or if the monthly billing process fails, at which time billing for all future purchases will be processed at the time of purchase.</div>

<P ALIGN="LEFT"><DIV CLASS="smblack">Security</div>

<br><br><DIV CLASS="smblack">We work to protect the security of your information during transmission by using Secure Sockets Layer (SSL) software, which encrypts information you input.  This site has security measures in place to protect the loss, misuse and alteration of the information under our control.  <b><I>equibase.com</I></b> uses 128-bit encryption for all secure links, which adheres to Federal and International guidelines for secure server transactions.  We reveal only the last four digits of your credit card numbers when confirming an order. Of course, we transmit the entire credit card number to the appropriate credit card company during order processing.</div>

<br><br><DIV CLASS="smblack"><i><b>equibase.com</b></i> is tested and certified daily to pass the McAfee Secure Security Scan. To help address concerns about possible hacker access to your confidential data, and the safety of visiting <i><b>equibase.com</b></i>, the "live" McAfee Secure mark appears only when this site passes the daily McAfee Secure tests.</div>

<P ALIGN="LEFT"><DIV CLASS="smblack">Opt Out Procedures for Mailings</div>

<br><br><DIV CLASS="smblack">Our site provides users the opportunity to opt-out of receiving communications from Equibase Company and third parties at the point where we request registration information about the visitor.</div>

<br><br><DIV CLASS="smblack">This site gives users the option for removing their information from our database to not receive information about Equibase Products and Services.  In order to complete this request, the User must send a written request via e-mail, including their Customer ID to: <a class="dkbluesm" href="mailto:registration@equibase.com?subject=Equibase Mail List Remove">Equibase Mailing List Removal</a></div>

<br><br><DIV CLASS="smblack">This site gives users the option to no longer receive the Equibase Inside Track mailing.  In order to complete this request, the User must send a written request via e-mail, from the e-mail address used at registration, to: <a class="dkbluesm" href="mailto:insidetrack@equibase.com?subject=Inside Track Remove">Inside Track Removal</a></div>

<br><br><DIV CLASS="smblack">This site gives users the option to de-activate their Virtual Stable.  In order to complete this request, the User can do so from the <a class="dkbluesm" href="http://www.equibase.com/virtual/gvsVirtualStableMain.cfm?SID=1" target="_new">Virtual Stable Main Page</a>, or by sending a written request via e-mail, from the e-mail address used for the Virtual Stable, to: <a class="dkbluesm" href="mailto:feedback@equibase.com?subject=De-activate Virtual Stable">De-activate Virtual Stable</a></div>

</TD></TR>
<TR>
<TD>
<HR>
<p>
<P ALIGN="LEFT"><DIV CLASS="smblack">Contacting the web site</div>
<br><br><DIV CLASS="smblack">If you have any questions about this privacy statement, the practices of this site, or your dealings with this web site, you can contact</div>
<BR>
<DIV CLASS="smblack">Shelby Cook</div>
<DIV CLASS="smblack">Online Services Coordinator</div>
<DIV CLASS="smblack">Equibase Company LLC</div>
<DIV CLASS="smblack">821 Corporate Drive</div>
<DIV CLASS="smblack">Lexington, KY  40503</div>
<DIV CLASS="smblack">1-800-333-2211</div>

<DIV CLASS="smblack">scook@equibase.com</div>
<br>
</td></tr></table>


            <!-- CONTENT GOES ABOVE HERE -->
											   </div>
											 <!-- AD AND SIDEBAR AREA -->
											<div class="sidebar">
		<!-- /2542414/EQ_Desktop_ROS_300x250_ATF_Sidebar1 -->
			<div id='div-gpt-ad-1444076119943-0' class='adunit'>
			<script type='text/javascript'>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1444076119943-0'); });
			</script>
			</div> 		<!-- /2542414/EQ_Desktop_ROS_300x250_BTF_Sidebar2 -->
			<div id='div-gpt-ad-1444076119943-1' class='adunit'>
			<script type='text/javascript'>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1444076119943-1'); });
			</script>
			</div> 		<!-- /2542414/EQ_Desktop_ROS_300x250_BTF_Sidebar3 -->
			<div id='div-gpt-ad-1444076119943-2' class="adunit">
			<script type='text/javascript'>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1444076119943-2'); });
			</script>
			</div>

								   </div>
								   </div>

									  <!-- FOOTER -->


	<!-- footer links -->
	<div class="row footer-links  hidden-print">
		<ul>
			<li><a href="/sitemap.cfm">Site Map</a></li>
			<li><a href="/about/about.cfm">About Us</a></li>
			<li><a href="/about/privacypolicy.cfm">Privacy Policy</a></li>
			<li><a href="/about/contact.cfm">Contact Us</a></li>
			<li><a href="/about/faq.cfm">F.A.Q.</a></li>
			<li><a href="/about/advertising.cfm">Advertising</a></li>
			<li><a href="/about/terms.cfm">Terms of Use</a></li>
			<li><a href="/products/handicapperstore.cfm">Price List</a></li>
		</ul>
	</div>
	<!-- end footer links -->

	<!-- company info / accepted payments -->
	<div class="row companyinfo  hidden-print">
		<div class="col-xs-9">
			<p>Equibase Company is the Official Supplier of Racing Information and Statistics to America's Best Racing, Breeders' Cup, Daily Racing Form, ESPN, MSNBC, NTRA, The Jockey Club, TRA, TVG and XpressBet. </p>
			<img src="/img/footer-logos.gif" class="img-responsive center" />
		</div>

		<div class="col-xs-3">
			<h5 class="mediumgraytxt  hidden-print">Accepted Payments</h5>
			<img src="/img/footer-cards.jpg" class="img-responsive center  hidden-print" />
		</div>
	</div>
	<!-- end company info/accepted payments -->

	<!-- legal -->
	<div class="row legal">
		Proprietary to and &copy; 2016 Equibase Company LLC. All Rights Reserved.<br />
		The Terms of Use for this web site prohibit the use of any robot, spider, scraper or any other automated means to access the contents of this site. The Terms of Use also expressly prohibit the republication or dissemination of the contents of this screen without the prior written consent of Equibase Company LLC.
	</div>
	<!-- end legal -->
<!-- END FOOTER -->

</div>

<script type='text/javascript'>
  googletag.cmd.push(function() {
    googletag.defineSlot('/2542414/EQ_Desktop_ROS_728x90_ATF_Header', [728, 90], 'headerAdDiv').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
    googletag.enableServices();
  });

  googletag.cmd.push(function() { googletag.display('headerAdDiv'); });
</script>


<!-- question mark overlay js -->

<script>
	$('.showHelp').click(function() {
		//alert("Click!");
		$('body').chardinJs('start');
		return false;
	});

	$('body').popover({
		selector: '[rel=popover]'
	});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2507067-1', 'equibase.com');
  ga('send', 'pageview');

</script>

<!-- Quantcast Tag -->
<script type="text/javascript">
var _qevents = _qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push({
qacct:"p-t_0-ux4SRATvh"
});
</script>

<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-t_0-ux4SRATvh.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!-- End Quantcast tag -->


</body>
</html> 