

<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>

    
<title>1105 Media Privacy Policy -- 1105 Media</title>
<meta name="description" content="1105 Media Privacy Policy, Opt-Out Links and More">
<meta name="keywords" content="1105 Media, Privacy Policy, Opt-out, no contact requests, etc. ">
   
    <!-- NEW GA HEAD CODE -->
<script type='text/javascript'>
(function(){
var useSSL = 'https:' == document.location.protocol;
var src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
})();
</script>


    <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="shortcut icon" type="image/x-icon"  href="~/media/ECG/1105media/favicon.ico">
<link href='//fonts.googleapis.com/css?family=Raleway:400,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/design/ECG/1105/css/style.css?v=14">


<!--[if lt IE 9]>
<script src="/design/ECG/05group/js/html5shiv.min.js"></script>
<script src="/design/ECG/05group/js/respond.min.js"></script>
<![endif]-->

<!--[if (gt IE 8)&(lt IE 10)]>
    <style type="text/css">
        .topnavlist ul {top: 20px;}
    </style>
<!--<![endif]-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-36382972-2', 'auto');
  ga('send', 'pageview');

</script>



    
    
    
    
</head>

<body id="Body1">
    
    
    
    
    
    <form name="form1" method="post" action="privacy.aspx" id="form1">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTgwOTIwMjgxMQ9kFgQCAQ9kFgYCAQ8WAh4EVGV4dAUnMTEwNSBNZWRpYSBQcml2YWN5IFBvbGljeSAtLSAxMTA1IE1lZGlhZAIDDxYCHwAFO2NvbnRlbnQ9IjExMDUgTWVkaWEgUHJpdmFjeSBQb2xpY3ksIE9wdC1PdXQgTGlua3MgYW5kIE1vcmUiZAIFDxYCHwAFSWNvbnRlbnQ9IjExMDUgTWVkaWEsIFByaXZhY3kgUG9saWN5LCBPcHQtb3V0LCBubyBjb250YWN0IHJlcXVlc3RzLCBldGMuICJkAg8PZBYCAgsPZBYEAjUPZBYCZg9kFgJmD2QWAgIBDxYCHgVjbGFzcwULY3VzdG9tUGFnZXMWBAIBD2QWAmYPFgIfAAUOUHJpdmFjeSBQb2xpY3lkAgMPFgIfAAXQUzxwPjxlbT48c3Ryb25nPkVmZmVjdGl2ZSBOb3ZlbWJlciAyMCwgMjAxMzwvc3Ryb25nPjwvZW0+PC9wPgo8cD4xMTA1IE1lZGlhIEluYy4gcmVjb2duaXplcyB0aGUgaW1wb3J0YW5jZSBvZiBwcm90ZWN0aW5nIHRoZSBwcml2YWN5IG9mIGluZm9ybWF0aW9uIHByb3ZpZGVkCnRvIHVzLiBBY2NvcmRpbmdseSwgd2Ugc3RyaXZlIGZvciBjb21wbGlhbmNlIHdpdGggYWxsIGZlZGVyYWwgYW5kIHN0YXRlIHByaXZhY3kgbGF3cyBhbmQgZm9sbG93CmluZHVzdHJ5IGJlc3QgcHJhY3RpY2VzIGluIG9yZGVyIHRvIGJlc3Qgc2VydmUgb3VyIGNvbW11bml0eS4gV2UgY29uc2lkZXIgYWxsIHN1YnNjcmliZXJzLApjdXN0b21lcnMsIG1lbWJlcnMsIHJlZ2lzdHJhbnRzLCBjb25mZXJlbmNlIGF0dGVuZGVlcyBhbmQgc3BlYWtlcnMgcGFydCBvZiB0aGUgMTEwNSBNZWRpYSBJbmMuCmNvbW11bml0eS4gVGhlIGZvbGxvd2luZyBndWlkZWxpbmVzIGhhdmUgYmVlbiBjcmVhdGVkIHRvIGRlbW9uc3RyYXRlIG91ciBjb21taXRtZW50IHRvIHByaXZhY3kuPC9wPgo8cD5PdXIgbG9jYXRpb24gYW5kIGNvbnRhY3QgaW5mb3JtYXRpb246PC9wPgo8YmxvY2txdW90ZT4KPHA+PGVtPlByaXZhY3kgSXNzdWVzPGJyPgoxMTA1IE1lZGlhIEluYy48YnI+CjkyMDEgT2FrZGFsZSBBdmVudWUsIFN1aXRlIDEwMTxicj4KQ2hhdHN3b3J0aCwgQ0EgOTEzMTE8YnI+ClBob25lOiAoODY2KSA0MTAtMTMyMTxicj4KPGEgaHJlZj0ibWFpbHRvOnByaXZhY3lAMTEwNW1lZGlhLmNvbSI+RS1tYWlsPC9hPjwvZW0+PC9wPgo8L2Jsb2NrcXVvdGU+CjxoND48YSBuYW1lPSJjb2xsZWN0ZWQiPjwvYT5Db2xsZWN0ZWQgSW5mb3JtYXRpb248L2g0Pgo8cD5UaGUgaW5mb3JtYXRpb24gd2UgY29sbGVjdCBpcyBnYXRoZXJlZCBmcm9tIHNldmVyYWwgc291cmNlcy4gVGhlc2UgaW5jbHVkZSBvdXIgY29uZmVyZW5jZXMsIHByb2R1Y3Qgc3Vic2NyaXB0aW9uIGZvcm1zL3JlZ2lzdHJhdGlvbnMsIHJlYWRlciBzZXJ2aWNlIGNhcmRzLCB3ZWJzaXRlIG1lc3NhZ2UgcG9zdGluZ3MsIHN1cnZleSBpbmZvcm1hdGlvbiBhbmQgcHVyY2hhc2VzIHZpYSB0ZWxlcGhvbmUsIGRpcmVjdCBtYWlsLCBlbWFpbCwgYW5kIG90aGVyIGVsZWN0cm9uaWMgc291cmNlcy4gRnJvbSB0aGVzZSBzb3VyY2VzIHdlIG1heSBjb2xsZWN0IHBvc3RhbCBhZGRyZXNzZXMsIGUtbWFpbCBhZGRyZXNzZXMsIHRlbGVwaG9uZSBudW1iZXJzLCBhbmQvb3IgZGVtb2dyYXBoaWMgaW5mb3JtYXRpb24gZm9yIHRob3NlIGluZGl2aWR1YWxzIHRoYXQgdm9sdW50ZWVyIHN1Y2ggaW5mb3JtYXRpb24uIEZvciBlYWNoIHZpc2l0b3IgdG8gb3VyIHdlYnNpdGVzLCBvdXIgd2ViIHNlcnZlciBhdXRvbWF0aWNhbGx5IHJlY29nbml6ZXMgdGhlIHZpc2l0b3ImcnNxdW87cyBkb21haW4gbmFtZSBhbmQgSVAgYWRkcmVzcyBvbmx5LCBidXQgbm8gb3RoZXIgaWRlbnRpZmlhYmxlIGluZm9ybWF0aW9uIHVubGVzcyB2b2x1bnRlZXJlZC4gSW4gYWRkaXRpb24sIHdlIGNvbGxlY3QgYWdncmVnYXRlIGluZm9ybWF0aW9uIG9uIHdoYXQgd2ViIHBhZ2VzIHZpc2l0b3JzIGFjY2VzcyBvciB2aXNpdC48L3A+CjxoND48YSBuYW1lPSJob3d3ZXVzZSI+PC9hPkhvdyBXZSBVc2UgQ29sbGVjdGVkIEluZm9ybWF0aW9uPC9oND4KPHA+VGhlIGluZm9ybWF0aW9uIHdlIGNvbGxlY3QgaXMgdXNlZCB0byBpbXByb3ZlIHRoZSBjb250ZW50IG9mIG91ciB3ZWIgcGFnZXMsIHRvIGN1c3RvbWl6ZSB0aGUgY29udGVudCBhbmQgbGF5b3V0IG9mIG91ciBwYWdlcyBmb3IgZWFjaCBpbmRpdmlkdWFsIHZpc2l0b3IsIGFuZC9vciB0byBub3RpZnkgdmlzaXRvcnMgYWJvdXQgdXBkYXRlcyB0byBvdXIgd2Vic2l0ZS4gQXMgcGFydCBvZiBhIHN1YnNjcmlwdGlvbiBhbmQvb3IgcmVnaXN0cmF0aW9uIGFncmVlbWVudCwgc3Vic2NyaWJlcnMgYW5kIHJlZ2lzdHJhbnRzIHdpbGwgYmUgY29udGFjdGVkIGJ5IHVzIHRocm91Z2ggbWFpbCwgZS1tYWlsLCBhbmQvb3IgcGhvbmUgdG8gY29tbXVuaWNhdGUgd2l0aCB0aGVtIGFib3V0IHRoZWlyIHN1YnNjcmlwdGlvbiBhbmQvb3IgcmVnaXN0cmF0aW9uLiBUaGV5IHdpbGwgYmUgY29udGFjdGVkIGZvciByZW5ld2FsLCByZS1xdWFsaWZpY2F0aW9uLCBzdXJ2ZXlzLCB1cGRhdGVzIGNvbmNlcm5pbmcgdGhlaXIgYWNjb3VudCBwcm9maWxlIGFuZCBzcGVjaWFsIG9mZmVycyByZWxhdGVkIHRvIHRoZWlyIHN1YnNjcmlwdGlvbiBhbmQvb3IgcmVnaXN0cmF0aW9uLiBXZSBhbHNvIHVzZSB0aGUgaW5mb3JtYXRpb24gdG8gY29udGFjdCB0aGVtIGZvciBtYXJrZXRpbmcgcHVycG9zZXMgYW5kIHNoYXJlIHRoaXMgaW5mb3JtYXRpb24gd2l0aCBvdGhlciByZXB1dGFibGUgb3JnYW5pemF0aW9ucyB0byBhbGxvdyB0aGVtIHRvIGNvbnRhY3Qgb3VyIHN1YnNjcmliZXJzIGFuZCByZWdpc3RyYW50cy4gV2Ugc2hhcmUgYm90aCBpbmRpdmlkdWFsIGNvbnRhY3QgYW5kIGRlbW9ncmFwaGljIGluZm9ybWF0aW9uIGFuZCBhZ2dyZWdhdGUgc3RhdGlzdGljcyBhYm91dCBvdXIgbWVtYmVycyB3aXRoIGFkdmVydGlzZXJzIGFuZCByZXB1dGFibGUgdGhpcmQgcGFydHkgdmVuZG9ycyBmb3IgbWFya2V0aW5nIGFuZCBwcm9tb3Rpb25hbCBwdXJwb3NlcywgdW5sZXNzIG90aGVyd2lzZSBzcGVjaWZpZWQgd2hlbiB0aGUgaW5mb3JtYXRpb24gaXMgY29sbGVjdGVkLiBDZXJ0YWluIGluZm9ybWF0aW9uICZtZGFzaDsgc3VjaCBhcyB5b3VyIHBhc3N3b3JkIGFuZCBjcmVkaXQgY2FyZCBudW1iZXIgJm1kYXNoOyBpcyBuZXZlciBkaXNjbG9zZWQgaW4gYW55IGZvcm0gd2hhdHNvZXZlciB0byBhbnkgb3V0c2lkZSBwYXJ0eS48L3A+CjxoND48YSBuYW1lPSJlbWFpbCI+PC9hPkUtbWFpbCBDb21tdW5pY2F0aW9uczwvaDQ+CjxwPklmIHlvdSBoYXZlIHJlZ2lzdGVyZWQgZm9yIG9uZSBvZiBvdXIgbWFnYXppbmVzLCB3ZWJzaXRlcywgZXZlbnRzLCBwcm9kdWN0cywgb3Igc2VydmljZXMgYW5kIHlvdSBoYXZlIHN1cHBsaWVkIGFuIGUtbWFpbCBhZGRyZXNzLCB5b3UgbWF5IHJlY2VpdmUgYm90aCB0cmFuc2FjdGlvbmFsIGVtYWlscyBwZXJ0YWluaW5nIHRvIHlvdXIgcmVnaXN0cmF0aW9uL3B1cmNoYXNlIGFuZCBwcm9tb3Rpb25hbCBlbWFpbHMgd2l0aCB2YWx1YWJsZSBvZmZlcnMuIDwvcD4KPHA+VHJhbnNhY3Rpb25hbCBlbWFpbHMgYXJlIHNlbnQgaW4gb3JkZXIgdG8gbWFpbnRhaW4geW91ciBhY2NvdW50IHdpdGggMTEwNSBNZWRpYSBJbmMuIFlvdSBjYW4gb3B0LW91dCBvZiByZWNlaXZpbmcgdGhlc2UgdHlwZXMgb2YgZS1tYWlscyBieSB1c2luZyB0aGUgb3B0LW91dCBtZWNoYW5pc20gbG9jYXRlZCB3aXRoaW4gYSByZWNlaXZlZCBlLW1haWwgbWVzc2FnZSBvciBieSBjYW5jZWxsaW5nIHlvdSBzdWJzY3JpcHRpb24sIHJlZ2lzdHJhdGlvbiwgb3IgcHVyY2hhc2UuIDwvcD4KPHA+UHJvbW90aW9uYWwgZW1haWxzIGFyZSBzZW50IHRvIGJyaW5nIHlvdSB2YWx1YWJsZSwgc2NyZWVuZWQgb2ZmZXJzLiBZb3UgY2FuIG9wdC1vdXQgb2YgcmVjZWl2aW5nIHRoZXNlIHR5cGVzIG9mIGUtbWFpbHMgYnkgdXNpbmcgdGhlIG9wdC1vdXQgbWVjaGFuaXNtIGxvY2F0ZWQgd2l0aGluIGEgcmVjZWl2ZWQgZS1tYWlsIG1lc3NhZ2UsIGJ5IDxhIHRhcmdldD0iX2JsYW5rIiBocmVmPSJodHRwczovLzExMDUuc3ViLWZvcm1zLmNvbS9PTkU2OTA5XzExMDVwcmVmJmFtcDtwaz1QUCI+Y2xpY2tpbmcgaGVyZTwvYT4gdG8gdXBkYXRlIHlvdXIgZW1haWwgcHJlZmVyZW5jZXMsIGNhbGxpbmcgdXMgYXQgdGhlIG51bWJlciBhYm92ZSwgb3IgYnkgd3JpdGluZyB0byB1cyBhdCB0aGUgYWRkcmVzcyBhYm92ZS48L3A+CjxwPkl0IG1heSB0YWtlIHNldmVyYWwgZGF5cyB0byBwcm9jZXNzIHlvdXIgZS1tYWlsIG9wdC1vdXQgcmVxdWVzdC4gUGxlYXNlIG5vdGUgdGhhdCB5b3UgbWF5IHJlY2VpdmUgYWRkaXRpb25hbCBjb21tdW5pY2F0aW9ucyBmcm9tIHVzIG9yIGFuIG91dHNpZGUgY29tcGFueSBhZnRlciBzdWJtaXR0aW5nIHlvdXIgcmVxdWVzdCBpZiB5b3VyIGUtbWFpbCBhZGRyZXNzIHdhcyBzZWxlY3RlZCBmcm9tIG91ciBkYXRhYmFzZSBqdXN0IHByaW9yIHRvIHJlY2VpdmluZyBhbmQgcHJvY2Vzc2luZyB5b3VyIHJlcXVlc3QuPC9wPgo8aDQ+PGEgbmFtZT0icG9zdGFsIj48L2E+UG9zdGFsIENvbW11bmljYXRpb25zPC9oND4KPHA+SWYgeW91IHZvbHVudGVlciBhIHBvc3RhbCBhZGRyZXNzIHlvdSBtYXkgcmVjZWl2ZSBwZXJpb2RpYyBtYWlsaW5ncyB0byByZWxhdGUgaW5mb3JtYXRpb24gYWJvdXQgbmV3IHByb2R1Y3RzLCBzZXJ2aWNlcywgYW5kL29yIHVwY29taW5nIGV2ZW50cy4gV2UgbWF5IGFsc28gcHJvdmlkZSB0aGlzIGluZm9ybWF0aW9uIHRvIG90aGVyIGNhcmVmdWxseSBzZWxlY3RlZCBvdXRzaWRlIGNvbXBhbmllcyBmb3IgcHJvbW90aW9uYWwgcHVycG9zZXMuIElmIHlvdSBkbyBub3Qgd2lzaCB0byByZWNlaXZlIG1haWxpbmdzIGZyb20gdXMgb3Igb3RoZXIgY29tcGFuaWVzLCBwbGVhc2UgbGV0IHVzIGtub3cgYnkgY2xpY2tpbmcgb24gdGhlIHN0YXRlbWVudCBiZWxvdyBhbmQgZm9sbG93aW5nIHRoZSBzdGVwcyBwcm92aWRlZCwgY2FsbGluZyB1cyBhdCB0aGUgYWJvdmUgdGVsZXBob25lIG51bWJlciwgb3Igd3JpdGluZyB0byB1cyBhdCB0aGUgYWJvdmUgYWRkcmVzcy48L3A+CjxibG9ja3F1b3RlPgo8cD48YSBocmVmPSIvZm9ybXMvZG9ub3RtYWlsLmFzcHgiPjxzdHJvbmc+SSBkbyBub3Qgd2lzaCB0byByZWNlaXZlIGFueSBtYWlsaW5ncyBmcm9tIDExMDUgTWVkaWEgSW5jLiBvciBvdGhlciBjb21wYW5pZXMuPC9zdHJvbmc+PC9hPjwvcD4KPC9ibG9ja3F1b3RlPgo8cCBjbGFzcz0idHh0c20iPkl0IHdpbGwgdGFrZSB1cCB0byAzIG1vbnRocyB0byBwcm9jZXNzIHlvdXIgZG8tbm90LW1haWwgcmVxdWVzdC4gUGxlYXNlIG5vdGUgdGhhdCB5b3UgbWF5IHJlY2VpdmUgYWRkaXRpb25hbCBjb21tdW5pY2F0aW9ucyBmcm9tIHVzIG9yIG90aGVyIGNvbXBhbmllcyBhZnRlciBzdWJtaXR0aW5nIHlvdXIgcmVxdWVzdCBpZiB5b3VyIHBvc3RhbCBhZGRyZXNzIHdhcyBzZWxlY3RlZCBmcm9tIG91ciBkYXRhYmFzZSBqdXN0IHByaW9yIHRvIHJlY2VpdmluZyBhbmQgcHJvY2Vzc2luZyB5b3VyIHJlcXVlc3QuIDwvcD4KPGg0PjxhIG5hbWU9InBob25lIj48L2E+VGVsZXBob25lIENvbW11bmljYXRpb25zPC9oND4KPHA+SWYgeW91IHZvbHVudGVlciBhIHRlbGVwaG9uZSBudW1iZXIgeW91IG1heSByZWNlaXZlIHBlcmlvZGljIHRlbGVwaG9uZSBjYWxscyBhYm91dCBuZXcgcHJvZHVjdHMsIHNlcnZpY2VzLCBhbmQvb3IgdXBjb21pbmcgZXZlbnRzLiBXZSBtYXkgYWxzbyBwcm92aWRlIHRoaXMgaW5mb3JtYXRpb24gdG8gY2FyZWZ1bGx5IHNlbGVjdGVkIG91dHNpZGUgY29tcGFuaWVzIGZvciBwcm9tb3Rpb25hbCBwdXJwb3Nlcy4gSWYgeW91IGRvIG5vdCB3aXNoIHRvIHJlY2VpdmUgdGVsZXBob25lIGNhbGxzIGZyb20gdXMgb3Igb3RoZXIgY29tcGFuaWVzLCBwbGVhc2UgbGV0IHVzIGtub3cgYnkgY2xpY2tpbmcgb24gdGhlIHN0YXRlbWVudCBiZWxvdyBhbmQgZm9sbG93aW5nIHRoZSBzdGVwcyBwcm92aWRlZCwgY2FsbGluZyB1cyBhdCB0aGUgYWJvdmUgdGVsZXBob25lIG51bWJlciwgb3Igd3JpdGluZyB0byB1cyBhdCB0aGUgYWJvdmUgYWRkcmVzcy48L3A+CjxibG9ja3F1b3RlPgo8cD48YSBocmVmPSIvZm9ybXMvZG9ub3RjYWxsLmFzcHgiPjxzdHJvbmc+SSBkbyBub3Qgd2lzaCB0byByZWNlaXZlIGFueSB0ZWxlcGhvbmUgY2FsbHMgZnJvbSAxMTA1IE1lZGlhIEluYy4gb3Igb3RoZXIgY29tcGFuaWVzLjwvc3Ryb25nPjwvYT48L3A+CjwvYmxvY2txdW90ZT4KPHAgY2xhc3M9InR4dHNtIj5JdCB3aWxsIHRha2UgdXAgdG8gMSBtb250aCB0byBwcm9jZXNzIHlvdXIgZG8tbm90LWNhbGwgcmVxdWVzdC4gUGxlYXNlIG5vdGUgdGhhdCB5b3UgbWF5IHJlY2VpdmUgYWRkaXRpb25hbCBjb21tdW5pY2F0aW9ucyBmcm9tIHVzIG9yIG90aGVyIGNvbXBhbmllcyBhZnRlciBzdWJtaXR0aW5nIHlvdXIgcmVxdWVzdCBpZiB5b3VyIHRlbGVwaG9uZSBudW1iZXIgd2FzIHNlbGVjdGVkIGZyb20gb3VyIGRhdGFiYXNlIGp1c3QgcHJpb3IgdG8gcmVjZWl2aW5nIGFuZCBwcm9jZXNzaW5nIHlvdXIgcmVxdWVzdC48L3A+CjxoND48YSBuYW1lPSJmYXgiPjwvYT5GYXggQ29tbXVuaWNhdGlvbnM8L2g0Pgo8cD5JZiB5b3Ugdm9sdW50ZWVyIGEgZmF4IG51bWJlciB5b3UgbWF5IHJlY2VpdmUgcHJvbW90aW9uYWwgZmF4ZXMgb24gYSB2ZXJ5IGxpbWl0ZWQgYmFzaXMuIE9ubHkgdGhvc2Ugd2l0aCB3aG9tIHdlIGhhdmUgYW4gZXhpc3RpbmcgYnVzaW5lc3MgcmVsYXRpb25zaGlwIChzdWNoIGFzIGFuIGVzdGFibGlzaGVkIHN1YnNjcmlwdGlvbiBvciByZWdpc3RyYXRpb24pIHdpbGwgcmVjZWl2ZSBzdWNoIGZheGVzLiBJZiB5b3UgZG8gbm90IHdpc2ggdG8gcmVjZWl2ZSBzdWNoIGZheGVzLCBwbGVhc2UgbGV0IHVzIGtub3cgYnkgc2VuZGluZyB1cyBhbiBlLW1haWwgdGhyb3VnaCB0aGUgZS1tYWlsIGxpbmsgcHJvdmlkZWQgYWJvdmUsIHdyaXRpbmcgdG8gdXMgYXQgdGhlIGFib3ZlIGFkZHJlc3MsIG9yIGNhbGxpbmcgdXMgYXQgdGhlIGFib3ZlIHRlbGVwaG9uZSBudW1iZXIuPC9wPgo8YmxvY2txdW90ZT4KPHA+PGEgaHJlZj0iL2Zvcm1zL2Rvbm90ZmF4LmFzcHgiPjxzdHJvbmc+SSBkbyBub3Qgd2lzaCB0byByZWNlaXZlIGFueSBmYXggbWVzc2FnZXMgZnJvbSAxMTA1IE1lZGlhIEluYy48L3N0cm9uZz48L2E+PC9wPgo8L2Jsb2NrcXVvdGU+CjxwIGNsYXNzPSJ0eHRzbSI+SXQgd2lsbCB0YWtlIHVwIHRvIDEgbW9udGggdG8gcHJvY2VzcyB5b3VyIGRvLW5vdC1mYXggcmVxdWVzdC4gUGxlYXNlIG5vdGUgdGhhdCB5b3UgbWF5IHJlY2VpdmUgYWRkaXRpb25hbCBjb21tdW5pY2F0aW9ucyBmcm9tIHVzIGFmdGVyIHN1Ym1pdHRpbmcgeW91ciByZXF1ZXN0IGlmIHlvdXIgZmF4IG51bWJlciB3YXMgc2VsZWN0ZWQgZnJvbSBvdXIgZGF0YWJhc2UganVzdCBwcmlvciB0byByZWNlaXZpbmcgYW5kIHByb2Nlc3NpbmcgeW91ciByZXF1ZXN0LjwvcD4KPGg0PjxhIG5hbWU9ImFjY2Vzc2luZyI+PC9hPkFjY2Vzc2luZyBQZXJzb25hbCBJbmZvcm1hdGlvbjwvaDQ+CjxwPlVwb24gcmVxdWVzdCwgd2Ugd2lsbCBwcm92aWRlIGFuIGluZGl2aWR1YWwgd2l0aCBhY2Nlc3MgdG8gaWRlbnRpZnlpbmcgaW5mb3JtYXRpb24gdGhhdCB3ZSBoYXZlIGNvbGxlY3RlZCBhYm91dCB0aGVtIChwcm92aWRlZCB0aGF0IHRoZXkgaGF2ZSBnaXZlbiBwcm9vZiBvZiBpZGVudGl0eSkuIFRoaXMgaW5mb3JtYXRpb24gY2FuIGJlIHJlcXVlc3RlZCBieSBzZW5kaW5nIHVzIGFuIGUtbWFpbCB0aHJvdWdoIHRoZSBlLW1haWwgbGluayBwcm92aWRlZCBhYm92ZSBvciB3cml0aW5nIHRvIHVzIGF0IHRoZSBhYm92ZSBhZGRyZXNzLjwvcD4KPGg0PjxhIG5hbWU9ImNvcnJlY3RpbmciPjwvYT5Db3JyZWN0aW5nL1VwZGF0aW5nIFBlcnNvbmFsIEluZm9ybWF0aW9uPC9oND4KPHA+VXBvbiByZXF1ZXN0LCB3ZSBvZmZlciBpbmRpdmlkdWFscyB0aGUgYWJpbGl0eSB0byBoYXZlIGluYWNjdXJhY2llcyBjb3JyZWN0ZWQgd2l0aGluIHRoZWlyIGNvbnRhY3QgaW5mb3JtYXRpb24uIFRoaXMgaW5mb3JtYXRpb24gY2FuIGJlIGNvcnJlY3RlZCBieSBzZW5kaW5nIHVzIGFuIGUtbWFpbCB0aHJvdWdoIHRoZSBlLW1haWwgbGluayBwcm92aWRlZCBhYm92ZSBvciB3cml0aW5nIHRvIHVzIGF0IHRoZSBhYm92ZSBhZGRyZXNzLiBDdXJyZW50IHN1YnNjcmliZXJzIGFuZCByZWdpc3RyYW50cyBjYW4gYWxzbyBhY2Nlc3MgYW5kIGNoYW5nZSB0aGVpciBpbmZvcm1hdGlvbiB0aHJvdWdoIG91ciB3ZWJzaXRlcyBieSBzZWxlY3RpbmcgdGhlIGFwcHJvcHJpYXRlIGxpbmsgb24gdGhlIHdlYnNpdGUgd2hlcmUgdGhlaXIgY3VycmVudCBzdWJzY3JpcHRpb24vcmVnaXN0cmF0aW9uIHJlc2lkZXMuPC9wPgo8aDQ+PGEgbmFtZT0iY29va2llcyI+PC9hPkNvb2tpZXM8L2g0Pgo8cD5XZSB1c2UgICBjb29raWVzIHRvIHN0b3JlIHZpc2l0b3JzJyBwcmVmZXJlbmNlcywgcmVjb3JkIHNlc3Npb24gaW5mb3JtYXRpb24gKHN1Y2ggYXMgICBpdGVtcyB0aGF0IHZpc2l0b3JzIGFkZCB0byB0aGVpciBzaG9wcGluZyBjYXJ0cyksIHJlY29yZCB1c2VyLXNwZWNpZmljICAgaW5mb3JtYXRpb24gc3VjaCBhcyB3aGF0IHBhZ2VzIHVzZXJzIGFjY2VzcyBvciB2aXNpdCwgZW5zdXJlIHRoYXQgdmlzaXRvcnMgYXJlICAgbm90IHJlcGVhdGVkbHkgc2VudCB0aGUgc2FtZSBiYW5uZXIgYWRzLCBjdXN0b21pemUgd2ViIHBhZ2UgY29udGVudCBiYXNlZCBvbiBhICAgdmlzaXRvcidzIGJyb3dzZXIgdHlwZSBvciBvdGhlciBpbmZvcm1hdGlvbiB0aGF0IHRoZSB2aXNpdG9yIHNlbmRzLCBhbmQgc3RvcmUgICB1c2VycyBuYW1lcyBhbmQgcGFzc3dvcmRzLjwvcD4KPHA+V2UgICBhbHNvIHVzZSB0aGlyZC1wYXJ0eSBhZHZlcnRpc2luZyBjb21wYW5pZXMgdG8gc2VydmUgYWRzIHRvIHZpc2l0b3JzIG9uIG91ciAgIHdlYnNpdGVzLiBUaGVzZSBjb21wYW5pZXMgbWF5IHVzZSBpbmZvcm1hdGlvbiAobm90IGluY2x1ZGluZyB2aXNpdG9ycycgbmFtZXMsICAgYWRkcmVzc2VzLCBlbWFpbCBhZGRyZXNzZXMsIG9yIHRlbGVwaG9uZSBudW1iZXJzKSBhYm91dCB2aXNpdHMgdG8gdGhpcyBhbmQgb3RoZXIgICB3ZWJzaXRlcyBpbiBvcmRlciB0byBwcm92aWRlIGFkdmVydGlzZW1lbnRzIGFib3V0IGdvb2RzIGFuZCBzZXJ2aWNlcyBvZiBpbnRlcmVzdCAgIHRvIHZpc2l0b3JzLiBGb3IgbW9yZSBpbmZvcm1hdGlvbiBhYm91dCB0aGlzIHByYWN0aWNlIGFuZCBjaG9pY2VzIGFib3V0IG5vdCAgIGhhdmluZyB0aGlzIGluZm9ybWF0aW9uIHVzZWQgYnkgdGhlc2UgY29tcGFuaWVzLCA8YSBocmVmPSJodHRwOi8vd3d3Lmdvb2dsZS5jb20vcHJpdmFjeV9hZHMuaHRtbCI+Y2xpY2sgaGVyZTwvYT4uPC9wPgo8aDQ+PGEgbmFtZT0iYWQiPjwvYT5BZCBTZXJ2ZXJzPC9oND4KPHA+VG8gdHJ5IGFuZCBicmluZyB5b3Ugb2ZmZXJzIHRoYXQgbWF5IGJlIG9mIGludGVyZXN0IHRvIHlvdSwgd2UgaGF2ZSByZWxhdGlvbnNoaXBzIHdpdGggb3RoZXIgY2FyZWZ1bGx5IHNlbGVjdGVkIGNvbXBhbmllcyB0aGF0IHdlIGFsbG93IHRvIHBsYWNlIGFkcyBvbiBvdXIgd2ViIHBhZ2VzLiBBcyBhIHJlc3VsdCBvZiB5b3VyIHZpc2l0IHRvIG91ciBzaXRlLCBhZCBzZXJ2ZXIgY29tcGFuaWVzIG1heSBjb2xsZWN0IGluZm9ybWF0aW9uIHN1Y2ggYXMgeW91ciBkb21haW4gdHlwZSwgeW91ciBJUCBhZGRyZXNzLCBhbmQgY2xpY2tzdHJlYW0gaW5mb3JtYXRpb24uIFRoZSB1c2Ugb2Ygc3VjaCBpbmZvcm1hdGlvbiBpcyBnb3Zlcm5lZCBieSB0aGUgcHJpdmFjeSBwb2xpY3kgb2YgdGhlc2Ugb3RoZXIgY29tcGFuaWVzLjwvcD4KPGg0PjxhIG5hbWU9InNlY3VyaXR5Ij48L2E+U2VjdXJpdHk8L2g0Pgo8cD4xMTA1IE1lZGlhIEluYy4gdGFrZXMgZXZlcnkgcHJlY2F1dGlvbiB0byBwcm90ZWN0IHlvdXIgaW5mb3JtYXRpb24gZnJvbSB1bmF1dGhvcml6ZWQgdXNlLiBXaGVuIHlvdSBzdWJtaXQgc2Vuc2l0aXZlIGluZm9ybWF0aW9uIHZpYSBvdXIgd2Vic2l0ZXMsIHRoZSBpbmZvcm1hdGlvbiBpcyBwcm90ZWN0ZWQgYm90aCBvbmxpbmUgYW5kIG9mZmxpbmUuIFdoZW4gb3VyIHJlZ2lzdHJhdGlvbi9zdWJzY3JpcHRpb24gZm9ybSBhc2tzIGZvciBhIGNyZWRpdCBjYXJkIG51bWJlciBvciBvdGhlciBzZW5zaXRpdmUgaW5mb3JtYXRpb24sIHRoYXQgaW5mb3JtYXRpb24gaXMgZW5jcnlwdGVkIGFuZCBwcm90ZWN0ZWQgd2l0aCB0aGUgYmVzdCBlbmNyeXB0aW9uIHNvZnR3YXJlIGluIHRoZSBpbmR1c3RyeSAmbWRhc2g7IFNTTC48L3A+CjxoND48YSBuYW1lPSJwb2xpY3kiPjwvYT5Qb2xpY3kgVXBkYXRlczwvaDQ+CjxwPkZyb20gdGltZSB0byB0aW1lLCB3ZSBtYXkgdXNlIHByb3ZpZGVkIGluZm9ybWF0aW9uIGZvciBuZXcsIHVuYW50aWNpcGF0ZWQgdXNlcyBub3QgcHJldmlvdXNseSBkaXNjbG9zZWQgaW4gb3VyIHByaXZhY3kgcG9saWN5LiBJZiBvdXIgaW5mb3JtYXRpb24gcHJhY3RpY2VzIGNoYW5nZSB3ZSB3aWxsIGltbWVkaWF0ZWx5IHBvc3QgYW55IGFkanVzdG1lbnRzIHRvIG91ciBwb2xpY3kgb24gdGhpcyB3ZWJzaXRlLiBUaGlzIHdpbGwgc2VydmUgYXMgeW91ciBub3RpZmljYXRpb24gb2YgdGhlc2UgY2hhbmdlcy4gSWYgeW91IGFyZSBjb25jZXJuZWQgYWJvdXQgaG93IHlvdXIgaW5mb3JtYXRpb24gaXMgdXNlZCwgYm9va21hcmsgdGhpcyBwYWdlIGFuZCBjaGVjayBiYWNrIHBlcmlvZGljYWxseS48L3A+CjxoND48YSBuYW1lPSJlbmZvcmNlbWVudCI+PC9hPkVuZm9yY2VtZW50PC9oND4KPHA+SWYgeW91IGZlZWwgdGhhdCB0aGlzIHNpdGUgaXMgbm90IGZvbGxvd2luZyBpdHMgc3RhdGVkIGluZm9ybWF0aW9uIHBvbGljeSwgcGxlYXNlIGNvbnRhY3QgdXMgdGhyb3VnaCB0aGUgYWJvdmUgcG9zdGFsIGFkZHJlc3MsIGUtbWFpbCBsaW5rLCBvciBwaG9uZSBudW1iZXIuPC9wPgo8cD48aW1nIGFsdD0iIiB3aWR0aD0iOTAiIGhlaWdodD0iNDciIHNyYz0iL34vbWVkaWEvRUNHLzExMDVNZWRpYS9JbnRlcm5hbFBhZ2VzL2RtYWxvZ28uanBnIj4gPC9wPmQCXQ9kFgJmDxYCHgdWaXNpYmxlaBYCAgEPFgIfAGVkZCPEzHh2oqXK8Vj6Q/VmjXRdF1uw" />


<script src="/ScriptResource.axd?d=lA4YcjqYGqEtyoU9SXv1fQNKpBVAd6uY2C59BfMJQTQ9nr7Lr6gxPie6DPWr94TstFBGwfF2UXhKpdYzXUsQ-WVmHM4v8Czi7POlmms2P0tIAYy0_EKlV3DxrXSWozwLP8RJ1gBcASOW15ht9rI28NaFfdkyWcTVyUMRpmy8LWEqVrXz0&amp;t=2e2045e2" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A9DC696D" />
    
    
        <div class="wrapper">
            
            
            
            
            
            
            <div class="leaderboard">
                
                
                
                
                
            </div>		            

            <header role="banner" class="header">            
                <div class="header-content">
<h1 class="site-logo">
  <a href="/home.aspx"><img src="~/media/ECG/1105Media/1105_logo.png" alt="1105 Media"></a>
</h1>

<div class="mobile-button">
    <svg width="25" height="15">
        <image xlink:href="~/media/ECG/The05Group/mobilemenubutton.svg" src="~/media/ECG/The05Group/mobilemenubutton_fallback.png" width="25" height="15"></image>
    </svg>
</div>

<div class="mobile-closebutton hide">
    <svg width="20" height="20">
        <image xlink:href="~/media/ECG/The05Group/mobileclosebutton.svg" src="~/media/ECG/The05Group/mobilemenuclosebutton_fallback.png" width="20" height="20"></image>
    </svg>
</div>




<NOINDEX>

<!-- component markup start -->
<div class='topnav'>
    <ul class="topnavlist"><li class=" brandslist"><a class="hasChild" href="javascript:void(0);"><span data-hover="Our Brands">Our Brands</span></a><ul><li class=""><a href="/pages/psmg.aspx#acquireshow"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/acquireshow.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#adtmag"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/adtmag.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#awsinsider"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/awsinsider.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#campustechnology"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/campustechnology.ashx"></span></a></li><li class=""><a href="/pages/shs.aspx#collegeplanning"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/collegeplanning.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#defensesystems"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/defensesystems.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#enterprisearchitecture"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/enterprisearchitecture.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#enterprisesystems"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/enterprisesystems.ashx"></span></a></li><li class=""><a href="/pages/shs.aspx#environmentalprotection"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/environmentalprotection.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#facetoface"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/facetoface.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#fcw"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/fcw.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#federalsoup"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/federalsoup.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#fias"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/fias.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#gcn"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/gcn.ashx"></span></a></li><li class=""><a href="/pages/shs.aspx#hmebusiness"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/hmebusiness.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#live360"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/live360.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#mcpmag"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/mcpmag.ashx"></span></a></li><li class=""><a href="/pages/shs.aspx#mobilitymanagement"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/mobilitymanagement.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#msdnmagazine"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/msdnmagazine.ashx"></span></a></li><li class=""><a href="/pages/shs.aspx#occupationalhealth"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/occupationalhealth.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#redmondmag"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/redmondmag.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#redmondchannelpartner"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/redmondchannelpartner.ashx"></span></a></li><li class=""><a href="/pages/shs.aspx#schoolplanning"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/schoolplanning.ashx"></span></a></li><li class=""><a href="/pages/shs.aspx#securityproducts"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/securityproducts.ashx"></span></a></li><li class=""><a href="/pages/shs.aspx#securitytoday"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/securitytoday.ashx"></span></a></li><li class=""><a href="https://1105media.com/pages/tdwi.aspx"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/tdwi.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#techmentorevents"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/techmentorevents.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#thejournal"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/thejournal.ashx"></span></a></li><li class=""><a href="https://the05group.com"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/the05group.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#virtualizationreview"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/virtualizationreview.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#visualstudiolive"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/visualstudiolive.ashx"></span></a></li><li class=""><a href="/pages/ecg.aspx#visualstudiomagazine"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/visualstudiomagazine.ashx"></span></a></li><li class=""><a href="/pages/psmg.aspx#washingtontechnology"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/washingtontechnology.ashx"></span></a></li><li class=" last"><a href="/pages/shs.aspx#waterwastewater"><span class="navbrand"><img alt="" src="~/media/ECG/1105media/brands/waterwastewater.ashx"></span></a></li></ul></li><li class=""><a href="https://the05group.com/home.aspx" target="_blank"><span data-hover="Marketing Services">Marketing Services</span></a></li><li class=" aboutus"><a class="hasChild" href="javascript:void(0);"><span data-hover="About Us">About Us</span></a><ul><li class=" groups"><a class="hasChild" href="javascript:void(0);">Groups</a><ul><li class=""><a href="https://1105media.com/pages/ecg.aspx">Enterprise Computing</a></li><li class=""><a href="https://1105media.com/pages/psmg.aspx">Public Sector</a></li><li class=""><a href="https://1105media.com/pages/shs.aspx">Security, Safety, Health & Facilities</a></li><li class=" last"><a href="https://1105media.com/pages/tdwi.aspx">TDWI</a></li></ul></li><li class=""><a href="https://1105media.com/pages/locations.aspx">Locations</a></li><li class=""><a href="https://1105media.com/pages/from-our-ceo.aspx">From Our CEO</a></li><li class=""><a href="/pages/from-our-ceo.aspx#ourexecutives">Our Executives</a></li><li class=""><a href="https://1105media.com/blogs/company-news/list/blog-list.aspx">Latest News</a></li><li class=" last"><a href="/home.aspx#events"><span data-hover="Upcoming Events">Upcoming Events</span></a></li></ul></li><li class=" careers"><a class="hasChild" href="https://1105media.com/pages/careers.aspx"><span data-hover="Careers & Culture">Careers & Culture</a></span></a><ul><li class=""><a href="/Pages/Careers.aspx#whywork">Why Work for 1105?</a></li><li class=""><a href="/pages/careers.aspx#philosophy">Our Philosophy</a></li><li class=" last"><a href="/pages/careers.aspx#jobs">Apply Today!</a></li></ul></li><li class="ChildSelected last help"><a class="hasChild" href="javascript:void(0);"><span data-hover="How Can We Help?">How Can We Help?</span></a><ul><li class=""><a href="https://1105media.com/pages/customer-service.aspx">Subscriptions</a></li><li class=""><a href="/pages/customer-service.aspx#changeemail">Change E-Mail</a></li><li class=""><a href="/pages/customer-service.aspx#optouts">Opt-Out Requests</a></li><li class=""><a href="https://1105media.com/forms/mediakit.aspx">Media Kits</a></li><li class=""><a href="https://1105media.com/pages/ad-specs.aspx">Ad Specs</a></li><li class=""><a href="https://www.meritdirect.com/1105-media/" target="_blank">List Rental</a></li><li class=""><a href="http://www.1105reprints.com/" target="_blank">Reprints</a></li><li class="selected"><a href="https://1105media.com/pages/privacy-policy.aspx">Privacy Policy</a></li><li class=""><a href="https://1105media.com/pages/terms-of-use.aspx">Terms of Service</a></li><li class=" last"><a href="https://1105media.com/pages/contact-us.aspx">Contact Us</a></li></ul></li></ul>
    <br class="clear" />
</div>
<!-- component markup end -->

</NOINDEX>

<div id="ph_header1_2_divBlockBox" class="social">
	
	<div id="ph_header1_2_divBody" class="summary">	<a href="https://twitter.com/1105media" target="_blank" class="twitter"><img src="~/media/ECG/1105Media/icon_twitter.png"></a>
	<a href="https://www.facebook.com/pages/1105-Media/369068519831852" target="_blank" class="facebook"><img src="~/media/ECG/1105Media/icon_facebook.png"></a>
	<a href="https://www.linkedin.com/company/1105-media" target="_blank" class="linkedin"><img src="~/media/ECG/1105Media/icon_linkedin.png"></a></div>
	
</div></div>



                
                
                
                
            </header>
                

            <div id="main">
    		
                
                
                
                
                

                 <div class="sideBar">
                    
                    
                    
                    
                    
                </div>
                
                <div class="pContent">
                    
<!-- component markup start -->
<div id="ph_pcontent1_0_customPages" class="customPages">
	<h3 id="ph_pcontent1_0_TitleH3">Privacy Policy</h3>
    <div class="customPages-content"><p><em><strong>Effective November 20, 2013</strong></em></p>
<p>1105 Media Inc. recognizes the importance of protecting the privacy of information provided
to us. Accordingly, we strive for compliance with all federal and state privacy laws and follow
industry best practices in order to best serve our community. We consider all subscribers,
customers, members, registrants, conference attendees and speakers part of the 1105 Media Inc.
community. The following guidelines have been created to demonstrate our commitment to privacy.</p>
<p>Our location and contact information:</p>
<blockquote>
<p><em>Privacy Issues<br>
1105 Media Inc.<br>
9201 Oakdale Avenue, Suite 101<br>
Chatsworth, CA 91311<br>
Phone: (866) 410-1321<br>
<a href="mailto:privacy@1105media.com">E-mail</a></em></p>
</blockquote>
<h4><a name="collected"></a>Collected Information</h4>
<p>The information we collect is gathered from several sources. These include our conferences, product subscription forms/registrations, reader service cards, website message postings, survey information and purchases via telephone, direct mail, email, and other electronic sources. From these sources we may collect postal addresses, e-mail addresses, telephone numbers, and/or demographic information for those individuals that volunteer such information. For each visitor to our websites, our web server automatically recognizes the visitor&rsquo;s domain name and IP address only, but no other identifiable information unless volunteered. In addition, we collect aggregate information on what web pages visitors access or visit.</p>
<h4><a name="howweuse"></a>How We Use Collected Information</h4>
<p>The information we collect is used to improve the content of our web pages, to customize the content and layout of our pages for each individual visitor, and/or to notify visitors about updates to our website. As part of a subscription and/or registration agreement, subscribers and registrants will be contacted by us through mail, e-mail, and/or phone to communicate with them about their subscription and/or registration. They will be contacted for renewal, re-qualification, surveys, updates concerning their account profile and special offers related to their subscription and/or registration. We also use the information to contact them for marketing purposes and share this information with other reputable organizations to allow them to contact our subscribers and registrants. We share both individual contact and demographic information and aggregate statistics about our members with advertisers and reputable third party vendors for marketing and promotional purposes, unless otherwise specified when the information is collected. Certain information &mdash; such as your password and credit card number &mdash; is never disclosed in any form whatsoever to any outside party.</p>
<h4><a name="email"></a>E-mail Communications</h4>
<p>If you have registered for one of our magazines, websites, events, products, or services and you have supplied an e-mail address, you may receive both transactional emails pertaining to your registration/purchase and promotional emails with valuable offers. </p>
<p>Transactional emails are sent in order to maintain your account with 1105 Media Inc. You can opt-out of receiving these types of e-mails by using the opt-out mechanism located within a received e-mail message or by cancelling you subscription, registration, or purchase. </p>
<p>Promotional emails are sent to bring you valuable, screened offers. You can opt-out of receiving these types of e-mails by using the opt-out mechanism located within a received e-mail message, by <a target="_blank" href="https://1105.sub-forms.com/ONE6909_1105pref&amp;pk=PP">clicking here</a> to update your email preferences, calling us at the number above, or by writing to us at the address above.</p>
<p>It may take several days to process your e-mail opt-out request. Please note that you may receive additional communications from us or an outside company after submitting your request if your e-mail address was selected from our database just prior to receiving and processing your request.</p>
<h4><a name="postal"></a>Postal Communications</h4>
<p>If you volunteer a postal address you may receive periodic mailings to relate information about new products, services, and/or upcoming events. We may also provide this information to other carefully selected outside companies for promotional purposes. If you do not wish to receive mailings from us or other companies, please let us know by clicking on the statement below and following the steps provided, calling us at the above telephone number, or writing to us at the above address.</p>
<blockquote>
<p><a href="/forms/donotmail.aspx"><strong>I do not wish to receive any mailings from 1105 Media Inc. or other companies.</strong></a></p>
</blockquote>
<p class="txtsm">It will take up to 3 months to process your do-not-mail request. Please note that you may receive additional communications from us or other companies after submitting your request if your postal address was selected from our database just prior to receiving and processing your request. </p>
<h4><a name="phone"></a>Telephone Communications</h4>
<p>If you volunteer a telephone number you may receive periodic telephone calls about new products, services, and/or upcoming events. We may also provide this information to carefully selected outside companies for promotional purposes. If you do not wish to receive telephone calls from us or other companies, please let us know by clicking on the statement below and following the steps provided, calling us at the above telephone number, or writing to us at the above address.</p>
<blockquote>
<p><a href="/forms/donotcall.aspx"><strong>I do not wish to receive any telephone calls from 1105 Media Inc. or other companies.</strong></a></p>
</blockquote>
<p class="txtsm">It will take up to 1 month to process your do-not-call request. Please note that you may receive additional communications from us or other companies after submitting your request if your telephone number was selected from our database just prior to receiving and processing your request.</p>
<h4><a name="fax"></a>Fax Communications</h4>
<p>If you volunteer a fax number you may receive promotional faxes on a very limited basis. Only those with whom we have an existing business relationship (such as an established subscription or registration) will receive such faxes. If you do not wish to receive such faxes, please let us know by sending us an e-mail through the e-mail link provided above, writing to us at the above address, or calling us at the above telephone number.</p>
<blockquote>
<p><a href="/forms/donotfax.aspx"><strong>I do not wish to receive any fax messages from 1105 Media Inc.</strong></a></p>
</blockquote>
<p class="txtsm">It will take up to 1 month to process your do-not-fax request. Please note that you may receive additional communications from us after submitting your request if your fax number was selected from our database just prior to receiving and processing your request.</p>
<h4><a name="accessing"></a>Accessing Personal Information</h4>
<p>Upon request, we will provide an individual with access to identifying information that we have collected about them (provided that they have given proof of identity). This information can be requested by sending us an e-mail through the e-mail link provided above or writing to us at the above address.</p>
<h4><a name="correcting"></a>Correcting/Updating Personal Information</h4>
<p>Upon request, we offer individuals the ability to have inaccuracies corrected within their contact information. This information can be corrected by sending us an e-mail through the e-mail link provided above or writing to us at the above address. Current subscribers and registrants can also access and change their information through our websites by selecting the appropriate link on the website where their current subscription/registration resides.</p>
<h4><a name="cookies"></a>Cookies</h4>
<p>We use   cookies to store visitors' preferences, record session information (such as   items that visitors add to their shopping carts), record user-specific   information such as what pages users access or visit, ensure that visitors are   not repeatedly sent the same banner ads, customize web page content based on a   visitor's browser type or other information that the visitor sends, and store   users names and passwords.</p>
<p>We   also use third-party advertising companies to serve ads to visitors on our   websites. These companies may use information (not including visitors' names,   addresses, email addresses, or telephone numbers) about visits to this and other   websites in order to provide advertisements about goods and services of interest   to visitors. For more information about this practice and choices about not   having this information used by these companies, <a href="http://www.google.com/privacy_ads.html">click here</a>.</p>
<h4><a name="ad"></a>Ad Servers</h4>
<p>To try and bring you offers that may be of interest to you, we have relationships with other carefully selected companies that we allow to place ads on our web pages. As a result of your visit to our site, ad server companies may collect information such as your domain type, your IP address, and clickstream information. The use of such information is governed by the privacy policy of these other companies.</p>
<h4><a name="security"></a>Security</h4>
<p>1105 Media Inc. takes every precaution to protect your information from unauthorized use. When you submit sensitive information via our websites, the information is protected both online and offline. When our registration/subscription form asks for a credit card number or other sensitive information, that information is encrypted and protected with the best encryption software in the industry &mdash; SSL.</p>
<h4><a name="policy"></a>Policy Updates</h4>
<p>From time to time, we may use provided information for new, unanticipated uses not previously disclosed in our privacy policy. If our information practices change we will immediately post any adjustments to our policy on this website. This will serve as your notification of these changes. If you are concerned about how your information is used, bookmark this page and check back periodically.</p>
<h4><a name="enforcement"></a>Enforcement</h4>
<p>If you feel that this site is not following its stated information policy, please contact us through the above postal address, e-mail link, or phone number.</p>
<p><img alt="" width="90" height="47" src="/~/media/ECG/1105Media/InternalPages/dmalogo.jpg"> </p></div> 
</div>
<!-- component markup end -->
                    
                    
                    
                    
                </div>
                
                <div class="sContent">
                    
                    
                    
                    
                    
                </div>
                
                <div class="xContent">
                    <div id="ph_xcontent1_0_divBlockBox" class="getstarted">
	<h3 id="ph_xcontent1_0_h3Header">Get Started Now</h3>
	<div id="ph_xcontent1_0_divBody" class="summary"><a href="/forms/mediakit.aspx">
  <div class="subtest icon icon1" id="startbox1">
  <span>Media</span> <br>
  <span>Kits</span>
</div>
</a>

<a href="https://the05group.com">
  <div class="subtest icon icon2" id="startbox2">
  <span>Marketing</span> <br>
  <span>Services</span>
  </div>
</a>


<a href="/pages/locations.aspx">
  <div class="subtest icon icon3" id="startbox3">
  <span>Locations</span>
  </div>
</a>

<a href="/pages/ad-specs.aspx">
  <div class="subtest icon icon5" id="startbox4">
  <span>Ad Specs</span>
  </div>
</a>

<a href="/blogs/company-news/list/blog-list.aspx">
  <div class="subtest icon icon4" id="startbox5">
  <span>Company News</span>
  </div>
</a>

<a href="/pages/contact-us.aspx">
  <div class="subtest icon icon6" id="startbox6">
  <span>Contact Us</span>
  </div>
</a>

<div class="subtest" id="startbox7">
  <a href="https://twitter.com/1105media" target="_blank"><img src="~/media/ECG/1105Media/icon_twitter_getstarted.png" alt="Follow 1105 Media on Twitter"></a>
  <a href="https://www.facebook.com/pages/1105-Media/369068519831852" target="_blank"><img src="~/media/ECG/1105Media/icon_facebook_getstarted.png" alt="Like 1105 Media on Facebook"></a>
  <a href="https://www.linkedin.com/company/1105-media" target="_blank"><img src="~/media/ECG/1105Media/icon_linkedin_getstarted.png" alt="Join 1105 Media on Linkedin"></a>
</div>
 	

<a href="http://careers.1105media.com" target="_blank">
  <div class="subtest icon icon8" id="startbox8">
  <span>Jobs</span>
  </div>
</a>

<a href="/pages/customer-service.aspx">
  <div class="subtest icon icon9" id="startbox9">
  <span>Help</span>
  </div>
</a></div>
	
</div>
                    
                    
                    
                    
                </div>
    		
            </div>

            <footer role="contentinfo" class="footer">
                <div id="ph_footer1_0_divBlockBox" class="blockBox">
	
	<div id="ph_footer1_0_divBody" class="summary"><div class="copyright" id="Copyright_copyright">
<img width="200px" alt="1105 Media" src="~/media/ECG/1105Media/1105_logo.png">
&copy; 2016 1105 Media Inc. See our <a href="/pages/privacy-policy.aspx">Privacy Policy</a> and  <a href="/pages/terms-of-use.aspx">Terms of Use</a> &nbsp;| <a href="/pages/sitemap.aspx">Site Map</a> | <a href="/pages/locations.aspx">Locations</a> | <a href="/pages/contact-us.aspx">Contact Us</a>
</div></div>
	
</div>
                
                
                
                
                
            </footer>
            
            
            <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="/design/ECG/1105/js/scripts-min.js?v=2"></script>



            </div>



            
            
            
    	            
                
                
    </div>

    
    
    
    
    
    
    

<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>
</body>

</html>
