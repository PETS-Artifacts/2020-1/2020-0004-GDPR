<!doctype html>
<html>
<head class="notie">
	<meta charset="utf-8">	
	<title>All About Jazz Privacy Policy</title>
	    <script type="text/javascript">
        if (screen.width <= 1024)
			        	location.href= '/privacy.php?&width=' + screen.width;
        	    </script>

	<meta name="keywords" content="">
	<meta name="description" content="All About Jazz Privacy Policy" />
	<meta name="author" content="All About Jazz">
	<meta class="viewport" name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<!-- FB tags -->
	<meta property="og:title" content="All About Jazz Privacy Policy" />
	<meta property="og:site_name" content=""/>
	<!--<meta property="og:url" content="http://www.allaboutjazz.com/privacy.php" />-->
	<meta property="og:description" content="All About Jazz Privacy Policy" />
	<meta property="og:image" content="http://www.allaboutjazz.com/icon/all_about_jazz_logo-300x300.png" />
	<!--<meta property="fb:app_id" content="228288063973092" /> this requires enabling the app -->
  <!-- Favicon -->
	<link rel="shortcut icon" href="http://www.allaboutjazz.com/icon/favicon.ico" />
  
	<!-- Fonts -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Styles -->
	<link rel="stylesheet" href="http://www.allaboutjazz.com/compressor_boot.php?params=css/bootstrap.min/bootstrap-theme.min/animate/base/v1.css" media="all" />
	<!--<link rel="stylesheet" href="http://www.allaboutjazz.com/css/base.css" media="all" />-->

  

<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<link rel="stylesheet" href="http://www.allaboutjazz.com/css/ie/ie8.css">
<![endif]-->	<!-- JS Head -->
<!--[if (!IE)|(gt IE 8)]><!-->
	<script src="http://www.allaboutjazz.com/js2/jquery-2.1.3.min.js"></script>
<!--<![endif]-->

<!--[if lte IE 8]>
<script src="http://www.allaboutjazz.com/js2/jquery-1.9.1.min.js"></script>
<![endif]-->

	


<!-- Google search -->
<script>
  (function() {
    var cx = '005595936876858408448:smj9t3-43fm';
    var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);
  })(); 
</script><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1973392-1', 'auto');
  ga('send', 'pageview');

</script><script>
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    (function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') + 
    '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
    })();
    </script>
    
    <!-- For Sortable -->
    <script src="//tags-cdn.deployads.com/a/allaboutjazz.com.js" async ></script>
    
    <script>
    googletag.cmd.push(function() {
    googletag.defineSlot('/1038983/300x250_Right_Top', [300, 250], 'div-gpt-ad-1420041586706-0').addService(googletag.pubads());
    googletag.defineSlot('/1038983/300x250_Right_Middle', [300, 250], 'div-gpt-ad-1420041542225-0').addService(googletag.pubads());
    googletag.defineSlot('/1038983/300x250_Right_Bottom', [300, 250], 'div-gpt-ad-1420041059048-0').addService(googletag.pubads());
    googletag.defineSlot('/1038983/Leaderboard_Top', [728, 90], 'div-gpt-ad-1420041696178-0').addService(googletag.pubads());
    googletag.defineSlot('/1038983/Leaderboard_Bottom', [728, 90], 'div-gpt-ad-1420041662835-0').addService(googletag.pubads());
    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
    });
</script><script src="http://www.allaboutjazz.com/compressor.php?params=js/jquery.autocomplete-min/paginator3000/jquery.flexslider-min/aaj_search_bootstrap/aaj_login/contrib_bootstrap/v1.js"></script>
<!-- Google Captcha -->
<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit'></script>
</head><body class="fixed-header body-www">
<div class="page-box">
	<div class="page-box-content">
		<header class="header">
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container primary">
			<button title="Search All About Jazz" data-toggle="modal" data-target="#search_overlay" class="btn btn-xs btn-primary" role="button">
				<i class="fa fa-search"></i> Search
			</button>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-target="#navbar_collapse" data-toggle="collapse" aria-expanded="false">
					<span class="sr-only">Menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="navbar_collapse">
				<ul class="nav navbar-nav">
					<li class="li-first">
						<a href="http://www.allaboutjazz.com/php/article_center.php" title="Recent Jazz Articles and Archive">Articles</a>			
					</li>	
					<li>
						<a href="http://news.allaboutjazz.com" title="Read Today's Jazz News">News</a>
					</li>		
					<li>
						<a href="http://media.allaboutjazz.com/" title="Download Thousands of Free MP3s">Music</a>
					</li>	
					<li>
						<a href="http://musicians.allaboutjazz.com/" title="Jazz Musician Database">Musicians</a>
					</li>	
					<li>
						<a href="http://photos.allaboutjazz.com" title="Jazz Image Gallery">Gallery</a>
					</li>
					<li>
						<a href="http://forums.allaboutjazz.com" title="Jazz Forum" target="_blank">Forum</a>
					</li>					
					<li class="li-last"><a title="View Local Events, Venue Listings and More!" href="http://www.jazznearyou.com">Events</a></li>
				</ul>
			</div><!--/.nav-collapse -->
			<ul class="nav navbar-right">
				<li class="dropdown li-only" id="nav-follow">
					<a aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle a-only" href="#" title="Follow us, read Jazz Near You RSS feeds, and use calendar widgets"><i class="fa fa-plus"></i><span class="hide-1400">Follow Us</span><i class="fa fa-caret-up"></i></a>
					<ul class="dropdown-menu">				
						<li>
							<a href="http://twitter.com/AllAboutJazz" target="_blank" title="Visit All About Jazz on Twitter"><i class="fa fa-twitter"></i>Twitter</a>
						</li>
						<li>
							<a href="https://www.facebook.com/allaboutjazz/" target="_blank" title="Visit All About Jazz on Facebook"><i class="fa fa-facebook"></i>Facebook</a>
						</li>
						<li>
							<a href="http://plus.google.com/+Allaboutjazz" target="_blank" title="All About Jazz on Google"><i class="fa fa-google-plus"></i>Google +</a>
						</li>					
						<li>
							<a href="http://www.allaboutjazz.com/php/live/rss.php" title="Subscribe to the All About Jazz RSS Feeds"><i class="fa fa-rss"></i>RSS</a>
						</li>
						<li>
							<a href="http://www.allaboutjazz.com/php/live/index.php" title="Use the All About Jazz Content Widgets on Your Website or blog"><i class="fa fa-cog"></i>Widgets</a>
						</li>				
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>		<section class="section-top" id="top" >
    <div class="container">
        <div id="logo-aaj" class="logo-container">
            <h2><a href="http://www.allaboutjazz.com" title="All About Jazz">All About Jazz</a></h2>
        </div>
		<ul class="navbar-user">  
			
                <li class="hide-740 dropdown">
                    <a href="#">MORE</a>
					<ul class="dropdown-menu">				
						<li><a title="Featured Jazz articles" href="/php/article_center.php?type=featured">Featured</a></li>
						<li><a title="Popular Jazz articles" href="/php/top_articles.php?t=read">Popular</a></li>
						<li><a title="Recommended Jazz articles" href="/php/top_articles.php">Recommended</a></li>	
						<li class="last-txt"><a title="Future Jazz articles" href="/php/future_articles.php">Future</a></li>			
					</ul>
                </li>
            				

			    <script type="text/javascript">
    $(function(){
	$('.authorization').css('visibility', 'visible');
	$('#status-load').css('display', 'none');
});
</script>
<div id="status-load">
	<img src="http://www.allaboutjazz.com/icon/049.gif" alt="Loading User Status" width="40" height="5" /> <span>Loading...</span>
</div>
	<li class="authorization"><a  onclick="signin(1); return false;" href="javascript:void(0);" title="Sign up and get a free local concert calendar newsletter plus much more!" data-toggle="modal" data-target="#login-box">Join</a></li>
    <li class="login authorization"><a onclick="signin(0); return false;"  href="javascript:void(0);" data-toggle="modal" data-target="#login-box">Sign In</a></li>    

<form name="logoutform" id="logoutform" action="#" method="post"><input type="hidden" name="logout" value="logout"/></form>		</ul>
	<p class="breadcrumbs_line nowrap overflow">
        <a title="All About Jazz home page" href="http://www.allaboutjazz.com">Home</a> &#187;
			Privacy Policy    </p>
    </div>
</section>		<section id="main">
			<div class="container prime-content-container">
				<div class="row">
					<div class="col-xs-12 col-md-8">
						<!--  -->
															<div class="leaderboard_ad bottom-40">
			<!-- Leaderboard_Top -->
			<div class="ad-tag" data-ad-name="responsivebanner_allaboutjazz_1" data-ad-size="auto" ></div><script src="//tags-cdn.deployads.com/a/allaboutjazz.com.js" async ></script><script>(deployads = window.deployads || []).push({});</script>
		</div>
													<article class="content">
							
							<h1>All About Jazz Privacy Policy</h1>
													﻿<h3>Your credit card transaction at All About Jazz is 100% safe - We Guarantee It.</h3> 
<p>We have been successfully processing secure credit card transactions since 1997.</p> 
<p>When we request personal information from you (such as name, address, phone number and credit card number) it is done on our secure servers. We use industry standard technology to protect confidential information - SSL (Secured Socket Layer) - an advanced encryption technology that makes personal information unreadable as it travels over the Internet. In addition, once your transaction is complete, your credit card number is deleted from our database.</p> 
<p>We are so confident in our security, that we guarantee you'll pay nothing if unauthorized charges are made to your credit card as a result of shopping at All About Jazz.</p> 
<p>Federal law limits your liability for unauthorized charges to your account. The Fair Credit Billing Act states that your credit card company (creditor) cannot hold you liable for more than $50 in the unlikely event a fraudulent charge occurs on your account. If you encounter a fraudulent credit card charge at All About Jazz, don't worry, we'll pay the first $50. No credit card risk - Guaranteed. </p> 

<h3>Guarantee Details</h3> 
<p>In the unlikely event of a fraudulent use of your credit card at All About Jazz, you must notify your credit card company in accordance with their rules and procedures.</p> 
<p>If your creditor holds you responsible for any of the $50, let us know. All About Jazz will cover the full liability for you up to $50.</p> 

<p>Please note that All About Jazz will only cover this liability if the unauthorized use of your card resulted through no fault of your own from purchases made at All About Jazz while using our secure servers.</p> 
<p>We realize that customers are concerned about how the information they provide online will be used. We are committed to protecting your privacy and will use the information we collect about you to process your order and provide a more personalized shopping experience. Please read on for more details regarding our Privacy Policy.</p> 

<h3>Privacy Information: What Information Do We Collect and How Do We Use It?</h3>

<p>When you place an order, we ask you to provide information to complete your order by filling out and submitting an online form. This information may include your name, telephone numbers, mailing address, e-mail address and credit card information. All information regarding our online customers is stored on a highly secured server that is not accessible through the Internet.</p> 

<p>When you participate in an online contest or survey, we may ask you for  personal information such as name or e-mail address to complete this process. Your participation in these events is voluntary and information collected will be used to enhance your future shopping/visiting experiences at All About Jazz and provide you with special offers that we believe will benefit you. To continuously assess the value of our online shopping/visiting experience, we may track the way customers use our site; however, this information is analyzed only in the aggregate and not at the individual level. All About Jazz and Vision X Software (AAJ's parent company) has a strong history of providing excellent customer service and our goal is to continue this tradition.</p>

<p>All About Jazz  is committed to the preservation of online privacy for all of its Web site visitors. We do not share any personal information with any third parties nor do we use any personal information for any purposes other than to enrich your experience at AAJ.</p> 
<p>Any e-mail we send you will also include information on how to unsubscribe.</p> 
<h3>Cookies and How We Use Them</h3>
<p>A cookie is a small file stored on your computer by a Web site to give you a unique ID. Cookies help make it easier for you to use our site, and they help us provide you with the information you need--when and where you need it. Cookies provided by our Web site do not contain any credit card or personal information. We do not know who you are unless you provided that information voluntarily. All About Jazz uses cookies to track new visitors to our site and recognize past customers so that we may present more personalized content. We do not use cookies to personally identify anonymous site visitors. </p> 				
<p>All About Jazz  uses third-party advertising companies to serve ads when you visit our Web site. These companies may use aggregated information (not including your name, address, email address or telephone number) about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, please see: <a href=http://www.networkadvertising.org/managing/opt_out.asp>networkadvertising.org</a></p>

<h3>External Links</h3>
<p>This site contains links to other sites. All About Jazz is not responsible for the privacy practices or the content of such Web sites.</p>					
<h3>Contacting All About Jazz</h3>
<p>If you have any questions about this privacy statement, the practices of this site, or your dealings with these Web sites, you can contact: <A HREF="http://www.allaboutjazz.com/php/submit_contributor_inquiry.php?id=1">Michael Ricci</A>.</p>				
							                                             </article>
					</div>
					<div class="sidebar col-xs-12 col-md-4">
						<div class="sbar-grid">
								
<div class="sbar-item sbar-item--height1">    
	
<div class="flex-container showcase-block">
	<div id="showcaseSlider" class="flexslider clearfix"><!--feat-cont-box-->
		<ul class="slides clearfix">
						<li style="background: url(http://www.allaboutjazz.com/images/ads/showcase/orderitem-1177-1505998032.jpg) 0 0 no-repeat;">					
				<div class="showcase-album">
<a href="http://www.cdbaby.com/cd/joshhanlon2" class="pic-100" title="Order “The Baytree Sessions, Vol. 1” by Josh Hanlon">
<img src="http://www.allaboutjazz.com/coverart/large/baytreedigcoverasdf.jpg" width="100" height="100" alt="“The Baytree Sessions, Vol. 1” - showcase release by Josh Hanlon" />
</a>
<a href="http://www.cdbaby.com/cd/joshhanlon2" class="btn btn-warning btn-xs" title="Order “The Baytree Sessions, Vol. 1” by Josh Hanlon"><i class="fa fa-shopping-cart"></i> Buy Now</a>
				</div>
				<h4><span>The Baytree Sessions, Vol. 1</span> by <a href="http://musicians.allaboutjazz.com/joshhanlon">Josh Hanlon</a></h4>
			</li>
						<li style="background: url(http://www.allaboutjazz.com/images/ads/showcase/orderitem-641-1940985845.jpg) 0 0 no-repeat;">					
				<div class="showcase-album">
<a href="http://bit.ly/1LEnI05" class="pic-100" title="Order “A Cosmic Rhythm With Each Stroke” by Vijay Iyer">
<img src="http://www.allaboutjazz.com/coverart/large/71lYm3DPhaL_.jpg" width="100" height="100" alt="“A Cosmic Rhythm With Each Stroke” - showcase release by Vijay Iyer" />
</a>
<a href="http://bit.ly/1LEnI05" class="btn btn-warning btn-xs" title="Order “A Cosmic Rhythm With Each Stroke” by Vijay Iyer"><i class="fa fa-shopping-cart"></i> Buy Now</a>
				</div>
				<h4><span>A Cosmic Rhythm With Each...</span> by <a href="http://musicians.allaboutjazz.com/vijayiyer">Vijay Iyer</a></h4>
			</li>
						<li style="background: url(http://www.allaboutjazz.com/images/ads/showcase/orderitem-1192-439641800.jpg) 0 0 no-repeat;">					
				<div class="showcase-album">
<a href="http://amzn.to/1SqWrPP" class="pic-100" title="Order “District Six” by Andreas Loven">
<img src="http://www.allaboutjazz.com/coverart/large/image1.JPG" width="100" height="100" alt="“District Six” - showcase release by Andreas Loven" />
</a>
<a href="http://amzn.to/1SqWrPP" class="btn btn-warning btn-xs" title="Order “District Six” by Andreas Loven"><i class="fa fa-shopping-cart"></i> Buy Now</a>
				</div>
				<h4><span>District Six</span> by <a href="http://musicians.allaboutjazz.com/andreasloven">Andreas Loven</a></h4>
			</li>
					</ul>			
		<script>
			$(document).ready(function () {
				$('#showcaseSlider').flexslider({
					animation: 'fade',
					slideshowSpeed: 5000,
					pauseOnAction: false,
					directionNav: false,
					slideshow: true,
					controlsContainer: '#showcaseSlider'
				});
			});
		</script>
		<div class="caption-text pull-right"><a href="http://www.allaboutjazz.com/php/showcase.php" title="All Active Showcases">View All</a> | <a href="http://www.allaboutjazz.com/php/advertising/showcase.php" title="About New Album Showcase">About Showcase</a></div>
		<!-- https://github.com/woothemes/FlexSlider/wiki/FlexSlider-Properties -->
	</div>
</div>
</div>
	<div class="sbar-item hide-992">
		<div class="bottom-40" id="jazzstory_section">
	<div class="pull-right caption-text top-10">
		<a href="http://bit.ly/1S2FuJz" title="Submit Your Jazz Story">About Jazz Story</a>
	</div>
	<h2 class="bottom-20"><a href="http://www.allaboutjazz.com/php/member_stories.php" class="callout-link" title="Jazz Stories">Jazz Stories <i class="fa fa-arrow-right"></i></a> </h2>
	<blockquote class="quote-text">
				<div id="firstpart_story">
			<p>I love jazz because of the freedom it allows me to express myself, musically. I was first exposed to jazz-like music by my mother, by way of her practicing Scott Joplin tunes on the piano when I was a little kid</p>
			<a id="showfullstory" class="callout-link" href="javascript:void(0);">Read more <i class="fa fa-arrow-right"></i></a>
		</div>
		<div id="lastpart_story" style="display:none;">
			<p>I love jazz because of the freedom it allows me to express myself, musically. I was first exposed to jazz-like music by my mother, by way of her practicing Scott Joplin tunes on the piano when I was a little kid. I believe the ragtime set up my ears for later being drawn to jazz.
I definitely want to thank my first guitar teacher, Ray Scott for handing me countless tapes of jazz players when all I wanted to do was learn rock & roll! My life was forever changed at about age 13 when he gave me a beat up Tonemaster cassette with Wes Montgomery playing &#147;Road Song.&#148; Man, my world was turned upside-down!</p>
			<a id="hidefullstory" class="callout-link" href="javascript:void(0);"><span class="fa fa-arrow-left"></span> Show less</a>
		</div>
		<script>
			$("#showfullstory").click(function(){
				$("#firstpart_story").css("display", "none");
				$("#lastpart_story").css("display", "block");
			});
			$("#hidefullstory").click(function(){
				$("#lastpart_story").css("display", "none");
				$("#firstpart_story").css("display", "block");
				$('html, body').animate({scrollTop: $("#jazzstory_section").offset().top}, 400);
			});
		</script>
			<i class="fa fa-caret-down"></i>
	</blockquote>
	<p class="quote-author">
				<a href="http://www.allaboutjazz.com/php/profile.php?id=47193" title="Visit Joshua Workman"><img src="http://www.allaboutjazz.com/photos/profile/josh_thumbnail.jpg" class="pic-60 f-left pic-round" height="60" alt="View Joshua Workman's All About Jazz profile" /></a> 
				
		By <a href="http://www.allaboutjazz.com/php/profile.php?id=47193">Joshua Workman</a>
					<br />Member since: 2008					
	</p>
</div>
	</div>

        
    <div class="sbar-item sbar-item--height2">    
        <div id="ad-top" class="adblock_300x250">
            <div class="ad-tag" data-ad-name="300x250_Top" data-ad-size="300x250" ></div>
            <script src="//tags-cdn.deployads.com/a/allaboutjazz.com.js" async ></script>
            <script>(deployads = window.deployads || []).push({});</script>
        </div>
    </div>

	<div class="sbar-item sbar-item--height1">
		<div class="padding-20 border-1 sbar-content-box">
	<h4><a href="http://media.allaboutjazz.com" class="callout-link">Track of the Day <i class="fa fa-arrow-right"></i></a></h4>
	<a href="http://media.allaboutjazz.com/jazzdownload.php?id=10631" target="_blank"><img src="http://www.allaboutjazz.com/coverart/large/CDBabyResized1400x1400FINALCOVERblesSINGSCD.jpg" width="200" height="200" /></a>
	<br /><br /><strong class="aaj">Track</strong>
	<br /><a class="aaj" href="http://media.allaboutjazz.com/jazzdownload.php?id=10631" target="_blank">Passion Dance</a>
	<br /><br /><strong class="aaj">Artist/Band</strong>
	<br /><a class="aaj" href="http://musicians.allaboutjazz.com/musician.php?id=1879" target="_blank">Nora McCarthy /and the people of peace quintet</a>
			<br /><br /><strong class="aaj">Album</strong>
		<br /><em><a class="aaj" href="http://www.blessings-noramcarthy.com" target="_blank">blesSINGS</a></em>
	</div>
	</div>

    <div class="sbar-item sbar-item--height2">
        <div id="div-gpt-ad-1420041542225-0" class="adblock_300x250">

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 300x250 middle -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:250px"
     data-ad-client="ca-pub-7212609363493973"
     data-ad-slot="3170517607"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

        </div>
    </div>

       
    <div class="sbar-item sbar-item--height4 hide-992">    

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 300x600 Wide -->
<ins class="adsbygoogle"
     style="display:inline-block;width:300px;height:600px"
     data-ad-client="ca-pub-7212609363493973"
     data-ad-slot="6224108928"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

    </div>
        
    <div class="sbar-item sbar-item--height2 hide-992">    
        <div id="ad-bottom" class="adblock_300x250">
            <!-- 300x250_Right_Bottom -->
            <div class="ad-tag" data-ad-name="300x250_Bottom" data-ad-size="300x250" ></div>
            <script src="//tags-cdn.deployads.com/a/allaboutjazz.com.js" async ></script>
            <script>(deployads = window.deployads || []).push({});</script>
        </div>
    </div>



		
						</div>
					</div>
				</div>
			</div><!-- .container prime-content-container-->
		</section><!-- #main -->
		<a name="bigbopper"></a>
<div id="bigbottom_adv">
	<a href="http://bit.ly/1PRZlXq" target="_blank" title="Purchase Now" class="bigbottom_adv_link">
		<img src="http://www.allaboutjazz.com/images/ads/BARTSCH_2016_2000X600.jpg" width="100%" />
    </a>
    <p class="caption-text pull-right top-10">SPONSOR: <a target="_blank" href="http://bit.ly/1PRZlXq">ECM Records</a> | <a target="_blank" href="http://bit.ly/1PRZlXq">ORDER NOW</a> &nbsp; </p>
</div>
<footer id="footer">
	<div class="footer-top">
		<div class="container">
			<div class="row bottom-60 text-center">
				<div class="col-xs-12 col-sm-4">
									<a href="http://www.jazznearyou.com/" title="Live Jazz Near You!">
						<span class="small">visit our partner website</span>
						<i class="footer-logo jny_footer_logo"></i>
						<p>jazz events, festivals, and venues</p>
					</a>
								</div>
				<div class="col-xs-12 col-sm-4">	
					<h3>Follow Us</h3>
					<div class="social">
						<a class="icon rounded icon-facebook" href="https://www.facebook.com/allaboutjazz" title="All About Jazz on Facebook"><i class="fa fa-facebook"></i></a>
						<a class="icon rounded icon-twitter" href="https://twitter.com/AllAboutJazz" title="All About Jazz on Twitter"><i class="fa fa-twitter"></i></a>
						<a class="icon rounded icon-google" href="http://plus.google.com/+Allaboutjazz" title="All About Jazz on Google+"><i class="fa fa-google"></i></a>
						<a class="icon rounded icon-rss" href="http://www.allaboutjazz.com/php/live/rss.php" title="All About Jazz RSS"><i class="fa fa-rss"></i></a>
						<a class="icon rounded icon-widget" href="http://www.allaboutjazz.com/php/live/index.php" title="All About Jazz Widgets"><i class="fa fa-cog"></i></a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4">
					<h3>Jazz Near You App</h3>
					<a class="" href="http://bit.ly/1hCpcKk" title="Download Jazz Near You App from the Google Play Store"><img src="http://www.allaboutjazz.com/icon/google_play_40.png" alt="Download Jazz Near You App from the Google Play Store" /></a>
					<a class="" href="http://bit.ly/1rr92jx" title="Download Jazz Near You App from the Apple Store"><img src="http://www.allaboutjazz.com/icon/app_store_40.png" alt="Download Jazz Near You App from the Apple Store" /></a>
				</div>
			</div>
			<div class="row bottom-60">
				<div class="col-xs-6 col-sm-3 bottom-20 col-foot">
					<h5>Community</h5>
					<ul>
						<li><a href="http://www.allaboutjazz.com/php/article.php?id=298">Welcome</a></li>
						<li><a href="http://www.allaboutjazz.com/php/article.php?id=45574">Join Us</a></li>
						<li><a href="http://www.allaboutjazz.com/php/contributor_center.php">Staff Directory</a></li> 
						<!-- <li><a href="http://musicians.allaboutjazz.com/search.php">Local Musicians</a></li>	
						<li><a href="http://musicians.allaboutjazz.com/search.php?teachers=1">Local Teachers</a></li> -->						
						<li><a href="http://forums.allaboutjazz.com">Forum</a></li> 
					</ul>				
				</div>
				<div class="col-xs-6 col-sm-3 bottom-20 col-foot">
					<h5>Services</h5>
					<ul> 
						<li><a href="http://musicians.allaboutjazz.com/toolkit.php">Musician Toolkit</a></li>
						<li><a href="http://www.jazznearyou.com/event_edit.php">Add Event</a></li>
						<li><a href="http://www.jazznearyou.com/guide_center.php">Add Business</a></li>
						<li><a href="http://www.jazznearyou.com/channels.php">Event Distribution</a></li>
					</ul>
				</div>
				<div class="col-xs-6 col-sm-3 bottom-20 col-foot">
					<h5>Get in touch</h5>
					<ul>
						<li><a href="http://news.allaboutjazz.com/become-a-jazz-near-you-partner.php">Partner Up!</a></li>					
						<li><a href="http://www.allaboutjazz.com/php/submit_suggestion.php">Make Suggestion</a></li>
						<li><a href="http://www.allaboutjazz.com/php/tech_support.php">Report Bug</a></li>
						<li><a href="http://www.jazznearyou.com/faq.php">FAQ</a></li>				
					</ul>
				</div>
				<div class="col-xs-6 col-sm-3 bottom-20 col-foot">
					<h5>Advertise</h5>
					<ul>
						<li><a href="http://www.allaboutjazz.com/php/advertising/index.php">Rates and Options</a></li>   
						<li><a href="http://www.allaboutjazz.com/php/advertising/about.php">About</a></li>					
						<li><a href="http://www.allaboutjazz.com/php/advertising/demographics.php">Demographics</a></li>
						<li><a href="http://www.allaboutjazz.com/php/submit_contributor_inquiry.php?id=1">Contact Us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div><!-- .footer-top -->
	<div class="footer-bottom">
		<div class="container">
			<div class="col-xs-12 col-sm-3">
				&copy; 2016 All About Jazz | <a href="http://www.allaboutjazz.com/privacy.php">Privacy Policy</a>
			</div>
			<div class="col-xs-10 col-sm-8">          
				<strong class="title">We exist to inform jazz fans and alert them to local shows. And while we're at it, we're improving audience attendance and building active jazz communities worldwide.</strong>
			</div>
			<div class="col-xs-2 col-sm-1">
				<a href="#" class="up"><i class="fa fa-arrow-up"></i></a>
			</div>
		</div>
	</div><!-- .footer-bottom -->
</footer>
<!-- Modal -->
<div class="modal fade" id="login-box" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button id="sign_button" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<h4 class="modal-title">Member Authorization</h4>
      </div>
      <div class="modal-body">
        <ul class="nav nav-pills" role="tablist" id="login_ul">
            <li id="signinli" role="presentation" class="active"><a id="a_signin" href="#signinli_tab" aria-controls="events" role="tab" data-toggle="pill">Already a member? Sign in</a></li>
            <li id="signupli" role="presentation" class="pull-right"><a id="a_signup" href="#signupli_tab" aria-controls="business" role="tab" data-toggle="pill">Not a member? Sign up</a></li>
        </ul>
        <div class="tab-content" id="panes">
            <div id="signinli_tab" role="tabpanel" class="tab-pane fade in active">
				<form method="POST" action="#" id="authForm" class="container-fluid">
<div class="row">
  <label for="email" class="col-xs-12 col-sm-6 control-label">Email address
	<span class="error form-validation" id="email_err"></span>
  </label>
  <div class="col-xs-12 col-sm-6">
	<input id="login_email" type="TEXT" name="email" class="form-control" value="" />
  </div>
</div>
<div class="row">
  <label for="pswd" class="col-xs-12 col-sm-6 control-label">Password
	<span class="error form-validation" id="pswd_err"></span>
  </label>
  <div class="col-xs-12 col-sm-6">
	<input type="PASSWORD" id="login_pswd" name="pswd" class="form-control" value="" />
	<a class="gray-text" id="a_reset" href="#3">Reset your password</a>
  </div>
</div>
<div class="row">
	<div class="col-xs-12">
		<button class="btn btn-success" type="submit" id="bsubmit">Sign in</button>
	</div>
</div>
				</form>
			</div>
            <div id="signupli_tab" role="tabpanel" class="tab-pane fade">
				<form method="POST" action="#" id="signupform" class="container-fluid">
<div class="row">
  <label for="name" class="col-xs-12 col-sm-6 control-label">First name <em>and</em> Last name
	<span class="error form-validation" id="signupname_err"></span>
  </label>
  <div class="col-xs-12 col-sm-6">
	<input id="input_name" type="TEXT" name="name" class="form-control" value="" />
  </div>
</div>
<div class="row">
  <label for="salutation" class="col-xs-12 col-sm-6 control-label">First name
	<span class="error form-validation" id="signupsalutation_err"></span>
  </label>
  <div class="col-xs-12 col-sm-6">
	<input id="input_salutation" type="TEXT" name="salutation" class="form-control" value="" />
  </div>
</div>
<div class="row">
  <label for="email" class="col-xs-12 col-sm-6 control-label">Email address
	<span class="help-block">It will remain <a href="http://www.allaboutjazz.com/privacy.php" target="_blank" title="All About Jazz Privacy Policy">private</a>.</span>
	<span class="error form-validation" id="signupemail_err"></span>
  </label>
  <div class="col-xs-12 col-sm-6">
	<input id="input_email" type="TEXT" name="email" class="form-control" value="" />
  </div>
</div>
<div class="row">
  <label for="country_subd" class="col-xs-12 col-sm-6 control-label">Country</label>
  <div class="col-xs-12 col-sm-6">
	<select id="country_subd" name="country_subd" class="form-control"><option value="al">Albania</option><option value="dz">Algeria</option><option value="ad">Andorra</option><option value="ao">Angola</option><option value="ai">Anguilla</option><option value="ag">Antigua / Barbuda</option><option value="ar">Argentina</option><option value="am">Armenia</option><option value="aw">Aruba</option><option value="au">Australia</option><option value="at">Austria</option><option value="az">Azerbaijan</option><option value="bs">Bahamas</option><option value="bh">Bahrain</option><option value="bd">Bangladesh</option><option value="bb">Barbados</option><option value="by">Belarus</option><option value="be">Belgium</option><option value="bz">Belize</option><option value="bj">Benin</option><option value="bm">Bermuda</option><option value="bt">Bhutan</option><option value="bo">Bolivia</option><option value="ba">Bosnia / Herzegowina</option><option value="bw">Botswana</option><option value="br">Brazil</option><option value="bn">Brunei Darussalam</option><option value="bg">Bulgaria</option><option value="bf">Burkina Faso</option><option value="bi">Burundi</option><option value="kh">Cambodia</option><option value="cm">Cameroon</option><option value="ca">Canada</option><option value="cv">Cape Verde</option><option value="ky">Cayman Islands</option><option value="cf">Central African Republic</option><option value="cl">Chile</option><option value="cn">China</option><option value="co">Colombia</option><option value="cg">Congo</option><option value="cr">Costa Rica</option><option value="hr">Croatia</option><option value="cu">Cuba</option><option value="cy">Cyprus</option><option value="cz">Czech Republic</option><option value="dk">Denmark</option><option value="dj">Djibouti</option><option value="do">Dominican Republic</option><option value="ec">Ecuador</option><option value="eg">Egypt</option><option value="sv">El Salvador</option><option value="gq">Equatorial Guinea</option><option value="ee">Estonia</option><option value="et">Ethiopia</option><option value="fj">Fiji</option><option value="fi">Finland</option><option value="fr">France</option><option value="pf">French Polynesia</option><option value="de">Germany</option><option value="gh">Ghana</option><option value="gi">Gibraltar</option><option value="gr">Greece</option><option value="gd">Grenada</option><option value="gp">Guadeloupe</option><option value="gt">Guatemala</option><option value="gy">Guyana</option><option value="ht">Haiti</option><option value="hn">Honduras</option><option value="hk">Hong Kong</option><option value="hu">Hungary</option><option value="is">Iceland</option><option value="in">India</option><option value="id">Indonesia</option><option value="ir">Iran (Islamic Republic of)</option><option value="iq">Iraq</option><option value="ie">Ireland</option><option value="il">Israel</option><option value="it">Italy</option><option value="jm">Jamaica</option><option value="jp">Japan</option><option value="je">Jersey</option><option value="kz">Kazakhstan</option><option value="ke">Kenya</option><option value="kw">Kuwait</option><option value="kg">Kyrgyzstan</option><option value="la">Lao People's Democratic Republic</option><option value="lv">Latvia</option><option value="lb">Lebanon</option><option value="li">Liechtenstein</option><option value="lt">Lithuania</option><option value="lu">Luxembourg</option><option value="mo">Macau</option><option value="mk">Macedonia</option><option value="mg">Madagascar</option><option value="mw">Malawi</option><option value="my">Malaysia</option><option value="ml">Mali</option><option value="mt">Malta</option><option value="mq">Martinique</option><option value="mr">Mauritania</option><option value="mu">Mauritius</option><option value="yt">Mayotte</option><option value="mx">Mexico</option><option value="fm">Micronesia (Federated States of)</option><option value="md">Moldova, Republic of</option><option value="mc">Monaco</option><option value="mn">Mongolia</option><option value="me">Montenegro</option><option value="ma">Morocco</option><option value="mz">Mozambique</option><option value="na">Namibia</option><option value="np">Nepal</option><option value="nl">Netherlands</option><option value="an">Netherlands Antilles</option><option value="nc">New Caledonia</option><option value="nz">New Zealand</option><option value="ni">Nicaragua</option><option value="ng">Nigeria</option><option value="kp">North Korea</option><option value="no">Norway</option><option value="om">Oman</option><option value="pk">Pakistan</option><option value="pa">Panama</option><option value="py">Paraguay</option><option value="pe">Peru</option><option value="ph">Philippines</option><option value="pl">Poland</option><option value="pt">Portugal</option><option value="pr">Puerto Rico</option><option value="qa">Qatar</option><option value="ro">Romania</option><option value="ru">Russian Federation</option><option value="rw">Rwanda</option><option value="ws">Samoa</option><option value="sm">San Marino</option><option value="sa">Saudi Arabia</option><option value="sn">Senegal</option><option value="rs">Serbia, Republic of</option><option value="sc">Seychelles</option><option value="sg">Singapore</option><option value="sk">Slovakia</option><option value="si">Slovenia</option><option value="so">Somalia</option><option value="za">South Africa</option><option value="kr">South Korea</option><option value="es">Spain</option><option value="lk">Sri Lanka</option><option value="sd">Sudan</option><option value="sz">Swaziland</option><option value="se">Sweden</option><option value="ch">Switzerland</option><option value="sy">Syrian Arab Republic</option><option value="tw">Taiwan, Province of China</option><option value="tj">Tajikistan</option><option value="tz">Tanzania, United Republic of</option><option value="th">Thailand</option><option value="tg">Togo</option><option value="tt">Trinidad / Tobago</option><option value="tn">Tunisia</option><option value="tr">Turkey</option><option value="tm">Turkmenistan</option><option value="tv">Tuvalu</option><option value="ug">Uganda</option><option value="ua">Ukraine</option><option value="ae">United Arab Emirates</option><option value="gb">United Kingdom</option><option value="us" SELECTED>United States</option><option value="uy">Uruguay</option><option value="uz">Uzbekistan</option><option value="va">Vatican City State (Holy See)</option><option value="ve">Venezuela</option><option value="vn">Viet Nam</option><option value="vg">Virgin Islands (British)</option><option value="vi">Virgin Islands (U.S.)</option><option value="ye">Yemen</option><option value="yu">Yugoslavia</option><option value="zr">Zaire</option><option value="zw">Zimbabwe</option></select>
  </div>
</div>
<div class="row" id="p_state" >
  <label for="state_subd" class="col-xs-12 col-sm-6 control-label">State</label>
  <div class="col-xs-12 col-sm-6">
	<select id="state_subd" name="state_subd" class="form-control" ><option value="al">Alabama</option><option value="ak">Alaska</option><option value="az">Arizona</option><option value="ar">Arkansas</option><option value="ca">California</option><option value="co">Colorado</option><option value="ct">Connecticut</option><option value="de">Delaware</option><option value="dc">District Of Columbia</option><option value="fl">Florida</option><option value="ga">Georgia</option><option value="hi">Hawaii</option><option value="id">Idaho</option><option value="il">Illinois</option><option value="in">Indiana</option><option value="ia">Iowa</option><option value="ks">Kansas</option><option value="ky">Kentucky</option><option value="la">Louisiana</option><option value="me">Maine</option><option value="md">Maryland</option><option value="ma">Massachusetts</option><option value="mi">Michigan</option><option value="mn">Minnesota</option><option value="ms">Mississippi</option><option value="mo">Missouri</option><option value="mt">Montana</option><option value="ne">Nebraska</option><option value="nv">Nevada</option><option value="nh">New Hampshire</option><option value="nj">New Jersey</option><option value="nm">New Mexico</option><option value="ny" SELECTED>New York</option><option value="nc">North Carolina</option><option value="nd">North Dakota</option><option value="oh">Ohio</option><option value="ok">Oklahoma</option><option value="or">Oregon</option><option value="pa">Pennsylvania</option><option value="ri">Rhode Island</option><option value="sc">South Carolina</option><option value="sd">South Dakota</option><option value="tn">Tennessee</option><option value="tx">Texas</option><option value="ut">Utah</option><option value="vt">Vermont</option><option value="va">Virginia</option><option value="wa">Washington</option><option value="wv">West Virginia</option><option value="wi">Wisconsin</option><option value="wy">Wyoming</option></select>
  </div>
</div>
<div class="row" id="p_city" >
  <label for="city_subd" class="col-xs-12 col-sm-6 control-label">City</label>
  <div class="col-xs-12 col-sm-6">
	<select id="city_subd" name="city_subd" class="form-control"><OPTION value=""><option value="albany">Albany</option><option value="beacon">Beacon</option><option value="buffalo">Buffalo</option><option value="nyc">New York City</option><option value="poughkeepsie">Poughkeepsie</option><option value="rochester">Rochester</option><option value="syracuse">Syracuse</option></select>
  </div>
</div>
<div class="row">
  <label for="pswd1" class="col-xs-12 col-sm-6 control-label">Password
	<span class="help-block">Enter 6+ characters and enter it twice.</span>
	<span class="error form-validation" id="signuppswd1_err"></span>
  </label>
  <div class="col-xs-12 col-sm-6">
	<input type="PASSWORD" name="pswd1" id="input_pswd1" autocomplete="off" class="form-control" value="">
	<input id="input_pswd2" type="PASSWORD" name="pswd2" autocomplete="off" class="form-control" value="">
  </div>
</div>
<div class="row">
  <label class="col-xs-12 col-sm-6 control-label">Subscribe
	<span class="help-block">To the weekly local jazz events calendar.</span>
  </label>
  <div class="col-xs-12 col-sm-6">
	<input type="checkbox" checked="checked" id="send_calendar_mail" name="send_calendar_mail">
  </div>
</div>
<div class="row">
  <label class="col-xs-12 col-sm-5 control-label">Verification code</label>
  <div class="col-xs-12 col-sm-7">
		<div id="googlecapt"></div>
  </div>
</div>
<div class="row">
	<div class="col-xs-12">
		<button class="btn btn-success" type="submit" id="signup">Submit</button>
	</div>
</div>
				</form>	
			</div>
			<div id="resetdiv">   
				<form method="POST" action="#" id="resetPass"  onsubmit="resetpass(); return false;" class="container-fluid">
<div class="row">
  <label for="email" class="col-xs-12 col-sm-6 control-label">Email address
	<span class="error form-validation" id="emailfound_err"></span>
  </label>
  <div class="col-xs-12 col-sm-6">
	<input id="email_found" type="TEXT" name="email" class="form-control" value="" />
  </div>
</div>
<div class="row">
	<div class="col-xs-12">
		<button class="btn btn-success" type="submit">Reset Password Now</button>
	</div>
</div>
				</form>				
			</div>
		</div>
      </div>
    </div>
  </div>
</div>
				
<!-- Modal -->
<div class="modal fade" id="loggedin" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="msg_txt"></h4>
			</div>
			<div class="modal-body">
				One moment, you will be redirected shortly.
			</div>
		</div>
	</div>
</div>

 <!-- Modal -->
<div class="modal fade" id="search_overlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Search All About Jazz</h4>
      </div>
      <div class="modal-body">
        <ul class="nav nav-pills" role="tablist" id="ul_seach">
            <li role="presentation" class="active"><a href="#search_article" aria-controls="search_article" role="tab" data-toggle="pill">Article</a></li>
            <li role="presentation"><a href="#search_news" aria-controls="search_news" role="tab" data-toggle="pill">News</a></li>
			<li role="presentation"><a href="#search_download" aria-controls="search_download" role="tab" data-toggle="pill">Download</a></li>
            <li role="presentation"><a href="#search_musician" aria-controls="search_musician" role="tab" data-toggle="pill">Musician</a></li>
			<li role="presentation"><a href="#search_gallery" aria-controls="search_gallery" role="tab" data-toggle="pill">Gallery</a></li>
		        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="search_article">
                <form action="http://www.allaboutjazz.com/php/article_center.php" method="get" class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>musician</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" id="aajsearch_artist" name="in_artist" value="" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>title</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" name="in_album" value="" />
                        </div>
                    </div>			
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>record label</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" id="aajsearch_label" name="in_label" value="">
                        </div>
                    </div>
					<div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>author</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" id="aajsearch_author" name="in_author" value="">
                        </div>
                    </div>
					<div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>type</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select name="in_type"><option value=0></option><option value="85">AAJ PRO</option><option value="74">African Jazz</option><option value="169">Afrobeat Diaries</option><option value="1">Airplay 101</option><option value="107">All About Jazz: South Africa</option><option value="4">Ask Ken</option><option value="140">Audio High Product Profile</option><option value="111">Audio Reviews</option><option value="136">Back Roads Beat</option><option value="83">Bailey's Bundles</option><option value="184">Behind the Lens With...</option><option value="6">Berklee Buzz</option><option value="79">Best of / Year End</option><option value="145">Big Band Caravan</option><option value="125">Big Band in the Sky</option><option value="7">Big Band Report</option><option value="134">Big Jazz Nerd</option><option value="160">Bill Anschell's Notes from the Lobby</option><option value="8">Biographies</option><option value="126">Book Excerpts</option><option value="9">Book Reviews</option><option value="72">Building a Jazz Library</option><option value="163">Built to Sound</option><option value="120">Call and Response</option><option value="11">Call to Listen</option><option value="189">Catching Up With</option><option value="100">CD/LP/Track Review</option><option value="71">Compare & Contrast</option><option value="138">Confessions of a Piano Player</option><option value="89">Contemporary Vibes</option><option value="133">Contributor News</option><option value="157">Cool Vic Files</option><option value="191">Cymbalism</option><option value="158">Dan's Den</option><option value="112">Digital Music</option><option value="116">Discography</option><option value="81">District Jazz</option><option value="110">Download Reviews</option><option value="225">Drum Addiction</option><option value="216">Drummer to Drummer</option><option value="68">DVD/Video/Film Reviews</option><option value="96">Extended Analysis</option><option value="21">Eye and Ear</option><option value="205">First Impressions</option><option value="22">First Steps</option><option value="23">First Time I Saw...</option><option value="177">Forgotten Finds</option><option value="198">Fre-Formation</option><option value="149">From Far and Wide</option><option value="24">From the Hip</option><option value="25">From the Inside Out</option><option value="144">Future Jazz</option><option value="181">Future Theory, Future Practice</option><option value="26">Gateway Grooves</option><option value="27">General Articles</option><option value="28">Genius Guide to Jazz</option><option value="42">Getting Into Jazz</option><option value="174">Gnome Notes</option><option value="220">Guitarist's Rendezvous</option><option value="197">Hardly Strictly Jazz</option><option value="95">Highly Opinionated</option><option value="108">History of Jazz Timeline</option><option value="211">Hobson's Choice</option><option value="206">Ideas and Sounds</option><option value="30">Improvise!</option><option value="31">In 10</option><option value="137">In the Artist's Own Words</option><option value="117">In the Studio</option><option value="32">Interviews</option><option value="119">Jazz and the Net</option><option value="148">Jazz Art</option><option value="170">Jazz Biofiles</option><option value="131">Jazz Ed</option><option value="207">Jazz Emerges</option><option value="156">Jazz Fiction</option><option value="33">Jazz From The Vinyl Junkyard</option><option value="102">Jazz Humor</option><option value="190">Jazz in the Aquarian Age</option><option value="93">Jazz Journal</option><option value="221">Jazz Near You</option><option value="193">Jazz on the 90th Floor </option><option value="58">Jazz Poetry</option><option value="118">Jazz Primer</option><option value="175">Jazz That Scratches, Swings and Pops</option><option value="34">Jazz Uncorked</option><option value="202">Jazz Wench</option><option value="35">Jazzin' Around Europe</option><option value="168">JazzLife UK</option><option value="36">Jazzology 300</option><option value="37">Journey into Jazz</option><option value="38">LA Jazzin</option><option value="40">Late Night Thoughts on Jazz</option><option value="43">Liner Notes</option><option value="130">Listen To This!</option><option value="52">Live From New York</option><option value="186">Live From Philadelphia</option><option value="19">Live Reviews</option><option value="44">London Calling</option><option value="196">Lost Masters</option><option value="46">Making a Jazz CD</option><option value="155">Making Music</option><option value="103">Meet the Staff</option><option value="82">Megaphone</option><option value="210">Mentoring the Millennials</option><option value="47">Mighty Like the Blues</option><option value="146">Misterioso</option><option value="176">Mixed Tape</option><option value="161">Mr. P.C.'s Guide to Jazz Etiquette...</option><option value="84">Multiple Reviews</option><option value="127">Muse From the Front</option><option value="153">Music and the Creative Spirit</option><option value="48">Music Biz 101</option><option value="49">Music Theory</option><option value="50">Musings In Cb</option><option value="224">Musings of a Jazz Piano Teacher</option><option value="97">Must Hear Review</option><option value="214">My Blue Note Obsession</option><option value="45">New & Noteworthy</option><option value="51">New York Beat</option><option value="80">Nite & Disk</option><option value="104">Nordic Sounds</option><option value="115">Not For Sale</option><option value="204">Notes From The Coast</option><option value="165">Old, New, Borrowed and Blue</option><option value="188">On and Off the Grid</option><option value="182">On the Road With...</option><option value="65">One LP</option><option value="54">Open Ears</option><option value="55">Opinion/Editorial</option><option value="56">Out-of-Print</option><option value="132">Podcast</option><option value="147">Previews</option><option value="139">Product Reviews</option><option value="3">Profiles</option><option value="178">Race and Jazz</option><option value="106">Readers Write Back</option><option value="59">Reassessing</option><option value="39">Record Label Profiles</option><option value="219">Rediscovery</option><option value="212">Rethinking Jazz Cultures</option><option value="60">Rhythm In Every Guise</option><option value="98">Roads Less Travelled</option><option value="203">Scumbles</option><option value="77">Shrinktunes</option><option value="113">Spirit of '76</option><option value="101">Straight From The Vault</option><option value="185">Stretch Movement</option><option value="122">Swing Set</option><option value="141">Take Five With...</option><option value="183">Talkin' Blues</option><option value="217">Talking 2 Musicians</option><option value="209">Tell Me More</option><option value="162">The Art and Science of Jazz</option><option value="167">The Art Of The Artist To Fan Relationship</option><option value="223">The Audiophile</option><option value="218">The Big Question</option><option value="172">The Business of Jazz</option><option value="213">The Club Car</option><option value="135">The Cool Vic Files</option><option value="194">The Doorman's Diary</option><option value="179">The Frozen Freeloader</option><option value="88">The Low End</option><option value="195">The Moment's Energy</option><option value="201">The Mort Report</option><option value="215">The Vinyl Post</option><option value="187">The Write Stuff</option><option value="222">Top Ten List</option><option value="166">Unsung Heroes</option><option value="128">Video Feature</option><option value="142">Vinyl Vibe</option><option value="199">We Travel the Spaceways</option><option value="69">Website News</option><option value="143">What is Jazz?</option><option value="208">Where Legends Lived</option><option value="70">Wide Open Jazz and Beyond</option><option value="171">Working Out to Jazz</option></select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 f-right">
                            <button type="submit" class="btn btn-success">Find!</button>
                        </div>
                    </div>
                </form>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="search_news">
                <form action="http://news.allaboutjazz.com/index.php" method="post" class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>title</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input name="in_title" type="text" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>type</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select name="in_type"><option value=0></option><option value="109">Advocacy</option><option value="12">Award / Grant</option><option value="115">Birthday</option><option value="2">Book / Magazine</option><option value="102">Career</option><option value="17">Contest Giveaway</option><option value="113">Crowdfunding Campaign</option><option value="110">Economics</option><option value="4">Education</option><option value="10">Event</option><option value="11">Festival</option><option value="103">For Sale</option><option value="105">Interview</option><option value="114">Job Posting</option><option value="101">Looking For...</option><option value="5">Music Industry</option><option value="6">Obituary</option><option value="16">Opinion</option><option value="1">Performance</option><option value="7">Radio</option><option value="3">Recording</option><option value="107">Rights</option><option value="104">Social Gathering</option><option value="106">Special Offers</option><option value="9">Technology</option><option value="108">Trends</option><option value="8">TV / Film</option><option value="15">Video / DVD</option><option value="13">Website</option></select>
                        </div>
                    </div>			
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>source</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select name="in_entity"><option value=0></option><option value="37103">"Sunny Live Music" blog</option><option value="49719">'stache Media</option><option value="10551">104 Weeks</option><option value="12077">1888 Media</option><option value="14270">250 Jazz Patterns</option><option value="12990">2media</option><option value="15104">319 Public Relations</option><option value="10361">4961 Polemics</option><option value="44983">A & R Marketing</option><option value="28007">A Dope Jazz Blog</option><option value="8561">Aabacus Agency</option><option value="9208">Abstract Logix</option><option value="8586">ADA Creative Communications</option><option value="8963">Affinity Public Relations</option><option value="25188">Agency Nouveau</option><option value="9447">AIR</option><option value="43444">Air Patrol</option><option value="18715">Airborne Jazz Blog</option><option value="13852">All About Jazz @ Spinner</option><option value="13299">All About Jazz Blog</option><option value="12005">All About Jazz Publicity</option><option value="9529">All That Jazz - Portuguese</option><option value="13575">All the Things... New York Jazz and More</option><option value="8955">Allegro Media</option><option value="12737">Allen/James Design</option><option value="11822">Alta LeCompte</option><option value="22263">Alzy Trio</option><option value="45832">Ambersingleton</option><option value="19352">American Tune Tribune, The</option><option value="9762">AmJamJazz</option><option value="9632">AMP3 Public Relations</option><option value="10958">AMT Public Relations</option><option value="12821">Amy Bakari Public Relations  </option><option value="9850">An (Occasional) Jazz Blog </option><option value="24860">An Introduction To Jazz by Michael Ramirez</option><option value="18159">Andre' Jenkins Public Relations</option><option value="13316">Andrew Boscardin: Composer and Musician</option><option value="11157">Andrew McIntosh</option><option value="9444">Anita Wong</option><option value="13592">Ann Forster</option><option value="10237">Anne Leighton</option><option value="4723">Anne Smith</option><option value="15448">Annie Jennings PR</option><option value="16512">Annuaire Pages Blanches</option><option value="14381">Anthology of American Folk Music</option><option value="7097">Arcos Communications</option><option value="10631">Ariel Publicity Artist Relations and Cyber Promotions</option><option value="11917">Arlene Kriv Public Relations</option><option value="15046">Ars Nova Workshop</option><option value="5802">Art and Style Magazine</option><option value="9487">Art Pepper Publicity</option><option value="12546">Artistic Sound</option><option value="8362">Artistry In Jazz</option><option value="13223">Artists Recording Collective, LLC</option><option value="15447">Ask QTP</option><option value="10459">ASR Records - Publicity Division</option><option value="12213">AUDIENCE LOGIC-arts marketing</option><option value="13887">Aural Addict</option><option value="22468">Australian JazzScene Magazine</option><option value="8975">Avant Coast</option><option value="28627">Žan Anderle</option><option value="12807">B. T. Entertainment</option><option value="9084">B3Jazz.com</option><option value="14260">Backstage with Steinway</option><option value="9102">Balding Lion Productions</option><option value="8865">BAMM Music</option><option value="26294">Bandzoogle Tips</option><option value="13937">Barcelona Music</option><option value="1184">Bass Player Magazine</option><option value="8374">BayTaper.com</option><option value="7051">be.jazz</option><option value="14652">Beautiful Planning </option><option value="9540">Bebop is Not a Cowboy</option><option value="14304">Bebop Memories</option><option value="18055">bebopified</option><option value="42537">Benjamin Scholz Productions</option><option value="9124">Bennett Alliance</option><option value="19405">Best. Saxophone. Website. Ever.</option><option value="18506">Between Sound and Space - An ECM Records Resource</option><option value="14525">Between the Grooves with Philip Booth</option><option value="9810">Big Bad Music</option><option value="8560">Big Hassle</option><option value="8884">BigDaddyBlues.com</option><option value="22727">Bill Hartzell</option><option value="12798">Billboard Magazine</option><option value="17477">BiLLfOoLeRy.CoM</option><option value="22563">Bird is the Worm</option><option value="9728">Bird Lives</option><option value="12974">Bisson Barcelona</option><option value="17171">Black History Guide</option><option value="9546">Blak's Lair</option><option value="10310">Blitz Media</option><option value="8686">Blue Isle Jazz</option><option value="13907">Blue Rhythm- Uncovering the Story of Jazz in India</option><option value="12985">BlueGal 110</option><option value="14399">blueplate pr</option><option value="14271">Blues & Rhythm Changes in All Keys</option><option value="13694">Blues in the Digital Age </option><option value="10995">Bluprint Media</option><option value="16616">Bob Gluck</option><option value="26202">Bob Van Eekhout</option><option value="8177">Bobby Hackett-Legendary Jazz Musician</option><option value="11388">BoingBoing</option><option value="12089">Bop and Beyond</option><option value="12850">Boss Sounds</option><option value="13493">Bottomless Cup Music </option><option value="10948">Boyd Tamney Cross Public Relations</option><option value="8899">Braithwaite & Katz Communications</option><option value="17362">Brave New World Media Group</option><option value="13738">Breachmedia</option><option value="14197">Brian McCoy</option><option value="14524">Brilliant Corners, a Boston Jazz Blog</option><option value="7070">Brock Woerner Blog/ News</option><option value="6984">Brody PR</option><option value="22164">Brooklyn Radio</option><option value="6935">Bullhorn Media</option><option value="13741">Bustin' Thru Entertainment</option><option value="8871">Buzzword PR</option><option value="1180">Cadence Magazine</option><option value="7061">Cahl's Juke Joint</option><option value="10269">Calabro Music</option><option value="11481">Canary Promotion + Design</option><option value="7786">Canberra Jazz blog</option><option value="43840">Candy Hawke</option><option value="13510">Caren West PR</option><option value="9572">CaribPR.com</option><option value="9842">Carla Oehmd Jazz Group</option><option value="9161">Carolina Jazz Connection</option><option value="45043">Carolrx</option><option value="9269">Carolyn McClair Public Relations</option><option value="7058">Casa Valdez Jazz</option><option value="13346">Cave 17</option><option value="17309">Center of the Indie Label Universe</option><option value="13966">Central Brooklyn Jazz Consortium PR</option><option value="13034">Chang Management</option><option value="12287">Charlie Wright</option><option value="14394">CHC Network</option><option value="16477">cheatsgaming</option><option value="12155">Cheryl Duncan & Company</option><option value="8450">Cheryl Hughey Promotions</option><option value="18882">Chez Janie</option><option value="1193">Chicago Jazz Magazine</option><option value="14115">Chillout Productions</option><option value="14862">Christina Jensen PR, LLC</option><option value="12375">Chronicles of a Cruise Ship Musician</option><option value="14536">Chuck Anderson Jazz Guitar Blog</option><option value="10061">Cindy Byram PR</option><option value="10514">CineMedia Promotions</option><option value="13068">Circle of One Marketing</option><option value="14178">Cirillo World</option><option value="11709">CJO Notes</option><option value="44662">Clauss Park</option><option value="9331">CODA</option><option value="12129">Cohn Dutcher Associates</option><option value="10546">Colehour+Cohen</option><option value="14272">Coltrane Changes</option><option value="20467">Complete Communion</option><option value="25743">Conor McDonough's We Got the Jazz</option><option value="9021">conqueroo</option><option value="44362">Cosmos Clinic</option><option value="8480">Costa Communications</option><option value="13543">Creality Communications</option><option value="12629">Creative Mind Entertainment</option><option value="8547">Creative Service Company</option><option value="12191">Creativity in Music</option><option value="12359">Cromarty & Co.</option><option value="9166">CSP-Media</option><option value="13891">Cultchas</option><option value="14207">Curtis Macdonald</option><option value="4940">CWJEF Music Publications</option><option value="9930">Dalmath Associates Inc.</option><option value="18880">Damore Public Relations</option><option value="13204">Dan Klores Communications</option><option value="1245">Daniel Kassell - Authentic Marketing</option><option value="7227">Darcy James Argue's Secret Society</option><option value="9202">Dave Douglas / Greenleaf Music</option><option value="10960">David Marriott / Red Raspus Music</option><option value="6462">Davis & Associates Public Relations</option><option value="10478">Dawn Singh Publicity</option><option value="24944">Dünya Küçük "Göç Yollarında Karşılaşmalar"</option><option value="9764">De Lafayette  Encyclopedia</option><option value="11900">Dean Lofton</option><option value="6933">DeBlaze & Associates</option><option value="8710">Deborah Radel</option><option value="8551">Debra Mercado Public Relations</option><option value="6513">DeLafayette World Media</option><option value="29704">Dernier PR</option><option value="10194">Derringer Media LLC</option><option value="22546">des annonces immobilieres</option><option value="8358">Destination: OUT</option><option value="14077">Detlev Schilke Photographie</option><option value="11981">Diacritical</option><option value="12764">Diane Hadley Public Relations</option><option value="14765">Diane Payes</option><option value="24687">Digital Cowboys By Dave Kusek</option><option value="8810">Digital Divas</option><option value="16126">Digital Music News</option><option value="13326">DIY Mummy</option><option value="14624">Dizzy Plays Parker</option><option value="969">DL Media</option><option value="8704">Dmitry Kiselev</option><option value="11324">Do The Math</option><option value="11994">Don't Explain</option><option value="11768">Donald Elfman - Ride Symbol</option><option value="15412">DooBeeDooBeeDoo</option><option value="44024">Doorcare</option><option value="12771">Doug Deutsch Publicity Services</option><option value="13914">Douglas Norström (Swedish jazz critic and journalist)</option><option value="1179">Downbeat</option><option value="49378">Downbeat Magazine</option><option value="11064">DP Public Relations</option><option value="8383">Dr. Jazz Operations</option><option value="12011">DSF Communications</option><option value="9059">Dusty Wright's Culture Catch</option><option value="45016">Dwight Hansen</option><option value="14313">Dylan Kay</option><option value="8379">EastWest Media</option><option value="12390">ECM Records/Universal Classics</option><option value="8631">Ed Bride Associates</option><option value="8820">Eda Kalkay Public Relations</option><option value="44631">Eh Joinery</option><option value="13805">Elefante Rosso Label & Club Venue</option><option value="14506">Elements of Jazz</option><option value="12718">Elise Brown, Drummer PR</option><option value="9638">Ellen Jacobs Associates</option><option value="11038">Elly Wininger</option><option value="43833">Emma Perry Publicity</option><option value="13799">Entertainment PR</option><option value="12277">entertainmentPR</option><option value="12478">Entire Productions LLC</option><option value="11588">Epiphany Marketing</option><option value="1186">EQ Magazine</option><option value="8978">Eric Kahi / Chillout Productions</option><option value="12754">Errand Girl For Jazz</option><option value="9120">Esalazioni etiliche</option><option value="11866">Estuary Public Relations</option><option value="8850">Etnobofin</option><option value="14531">European Guide</option><option value="10325">European Latin Jazz Foundation</option><option value="12442">eViewsandNews.com</option><option value="29033">Evolver.fm</option><option value="12257">extempore</option><option value="13774">Eye Magnet Group</option><option value="11030">Falco Ink.</option><option value="10345">FAP Stage Sound</option><option value="16302">FCEtier</option><option value="25235">Feathersound Media</option><option value="11776">Fen Castro</option><option value="13039">Festival International de Jazz de Montreal</option><option value="16788">Fine & Mellow Music</option><option value="10362">Fiona Bloom</option><option value="45664">First Take PR & Consulting</option><option value="9536">Flamenco Express</option><option value="14527">Flauto Jazz</option><option value="10466">Fletcher Henderson Museum</option><option value="24967">FlipswitchPR, LLC</option><option value="7041">Fojazz: The Blog</option><option value="12918">FortyThree PR</option><option value="43293">Frank Abay</option><option value="15515">Freddies World by Frederick Bernas</option><option value="13531">Frederick Bernas - portfolio</option><option value="43334">Free And Expressive Art Music Show</option><option value="18499">Free Background Music - Whisperings!</option><option value="17394">Free Energy Media</option><option value="11988">Free Jazz by Stef Gijssels</option><option value="24135">Free Jazz Lessons</option><option value="18124">Free Piano Sheet Music in the New Age Style</option><option value="44875">Fresno Media</option><option value="14363">Fretterverse.com</option><option value="9732">Friends of Cape May Jazz, Inc.</option><option value="13825">From Riches to Ragtime: Peter Cobb</option><option value="9099">Frozen Sky Records</option><option value="13281">Fully Altered Media</option><option value="12671">Fumi Tomita</option><option value="9883">FYI Communications, Inc.</option><option value="14511">G.A.S. Experiences</option><option value="16486">Gapplegate Guitar and Bass Blog</option><option value="16485">Gapplegate Music Review by Grego Edwards</option><option value="5086">Generation Media</option><option value="10816">Georgia Gould-Lyle</option><option value="11970">Giant Noise</option><option value="2739">Giant Step</option><option value="17235">Giant Step</option><option value="6517">Gina Vodegel Writing Affairs</option><option value="12426">Girlie Action Media and Marketing</option><option value="9229">Glass Onyon PR - William James</option><option value="43628">Glyn Jones</option><option value="32535">GNUStar - Talent Services Division</option><option value="14303">Goings on @ Urban Nerds</option><option value="8559">GoMedia PR</option><option value="11718">Gorgeous PR, Inc</option><option value="10074">Graffiti PR</option><option value="14420">Grassroots Jazz</option><option value="13865">Graycat Productions</option><option value="9671">Graziella PR</option><option value="9897">Great Scott P.R.oductions</option><option value="5247">Greater Love Productions</option><option value="14277">Green Galactic</option><option value="17449">Green Type of Tube</option><option value="44910">Greenhouse Publicity</option><option value="7053">Greg Hester Jazz</option><option value="12702">Groove Notes</option><option value="11774">Groove Therapy Records</option><option value="12204">Groshelle Communications</option><option value="7698">GruvAvenue</option><option value="8157">Guia de Jazz SobreSites</option><option value="1182">Guitar Player</option><option value="10400">Gurtman and Murtha Artists</option><option value="9992">H. S. Communication</option><option value="8558">Hands On Public Relations and Marketing</option><option value="7108">Hard Pressed Publicity</option><option value="12544">Helen Maleed</option><option value="12009">Hello Wendy</option><option value="10697">Hemant Sareen</option><option value="7042">Hemispherical</option><option value="12881">Here We Go Agency</option><option value="13978">Hollis Group</option><option value="1195">Hot House Jazz Magazine</option><option value="9346">House of Blues Concerts Canada</option><option value="15802">hubtone PR</option><option value="43305">Hudson Media Services</option><option value="9884">Hummingbird Publicity</option><option value="12897">HypeBot</option><option value="7060">I Was Doing All Right</option><option value="18128">Ian</option><option value="11770">Ideal PR, LLC</option><option value="13572">Ignite Media Consulting </option><option value="14552">Illinois Blues</option><option value="9432">Imaginary Friends</option><option value="9085">imajinnWEST</option><option value="8049">Impress Media - Jennifer Niederhoffer</option><option value="11062">IMPro</option><option value="7560">Improvisations on a Theme</option><option value="5494">Improvised Communications</option><option value="10054">In A Mellow Tone</option><option value="9260">In the Groove, Jazz and Beyond</option><option value="13974">Indaba Music Artist in Residence with Greg Osby</option><option value="22134">Indian Music</option><option value="24315">Indiana Avenue And Beyond</option><option value="11952">Indianapolis Symphony Orchestra</option><option value="4768">INDIEgo Promotions</option><option value="9223">Infinite Records</option><option value="13507">Inner Merit</option><option value="10669">Inque Public Relations</option><option value="7249">International News Agency</option><option value="14291">Inverted Garden by Eric Benson</option><option value="13170">IrwinPR LLC</option><option value="14227">Isabella Schrammel - Isilistening</option><option value="14443">ISL Public Relations </option><option value="13542">Island Jazz</option><option value="11669">J-B Associates</option><option value="9280">J.P. Stevens</option><option value="13202">J:>)azZClefs#</option><option value="14572">JamaicaMusic Offbeat</option><option value="12002">JamBase</option><option value="14651">James Armstrong Music</option><option value="45545">James Watkins, Jr.</option><option value="14217">Jan Matthies Music Management</option><option value="10080">Janice Miller</option><option value="1381">JANLYN PR - Jana La Sorte</option><option value="5942">Jason Byrne, Red Cat Publicity</option><option value="13318">Jason Parker Music</option><option value="13727">jazz  fotografie</option><option value="13689">Jazz & Blues Florida </option><option value="1235">Jazz & Blues Report</option><option value="13161">Jazz (Jazzers Jazzing) by Carl L. Hager</option><option value="21911">Jazz Addict</option><option value="14562">Jazz Advice</option><option value="7077">Jazz and Blues Music Reviews</option><option value="7264">Jazz and Blues USA</option><option value="19138">Jazz and Draw</option><option value="22239">Jazz at Night</option><option value="4357">Jazz At Ronnie Scotts</option><option value="7057">Jazz Authority</option><option value="11301">Jazz Beyond Jazz by Howard Mandel</option><option value="7096">Jazz Bird</option><option value="7815">jAzZ Blog</option><option value="13127">Jazz Blog</option><option value="9668">Jazz Blog (Germany)</option><option value="9009">Jazz By Net</option><option value="13061">Jazz Chronicles</option><option value="24497">Jazz Club Jury</option><option value="9463">Jazz Doll PR</option><option value="6064">Jazz DVD Marketing</option><option value="7421">Jazz Elements</option><option value="30959">Jazz En Dominicana</option><option value="9309">Jazz en Dominicana by Fernando Rodriguez</option><option value="17004">Jazz Evolution</option><option value="9973">Jazz for the Asking</option><option value="10214">Jazz Frisson</option><option value="10828">Jazz from Gallery 41</option><option value="11500">Jazz Gazetal</option><option value="21304">Jazz Goa</option><option value="10558">Jazz Goa blog</option><option value="4354">Jazz Guide</option><option value="1177">Jazz Improv Magazine</option><option value="9855">Jazz in India</option><option value="13333">Jazz In London</option><option value="18162">Jazz In Perspective</option><option value="13595">Jazz in Queens</option><option value="8277">Jazz in the Village</option><option value="4341">Jazz Journal</option><option value="13198">Jazz Listeners Guide</option><option value="12511">Jazz Lives by Michael Steinman</option><option value="16016">Jazz Loft Project by Sam Stephenson</option><option value="13192">Jazz Loft Publicity</option><option value="17519">Jazz Mechanica</option><option value="22044">Jazz Moods</option><option value="11650">Jazz My Two Cents Worth</option><option value="6088">Jazz N Blues Entertainment Magazine</option><option value="6912">Jazz no pais do Improviso!</option><option value="20381">Jazz North of 49</option><option value="11916">Jazz Note SDP</option><option value="9646">Jazz on Stage</option><option value="18920">Jazz Online By Joseph Vella</option><option value="6924">Jazz på Svenska (Jazz in Swedish)</option><option value="11349">Jazz Perspectives</option><option value="18524">Jazz Photos</option><option value="9558">Jazz Photos and Chronicles</option><option value="6057">Jazz Podium</option><option value="1187">Jazz Quarterly Magazine</option><option value="1201">Jazz Report Magazine</option><option value="4342">Jazz Review</option><option value="7571">Jazz Scene Ireland</option><option value="17537">Jazz Science by Brian Prunka</option><option value="25643">Jazz Special</option><option value="12781">Jazz Suite</option><option value="17331">Jazz Talk With J-Sug</option><option value="6060">Jazz thing</option><option value="7050">Jazz Thinks</option><option value="22467">Jazz Today</option><option value="24001">Jazz Toilet</option><option value="13406">Jazz Trumpet Licks</option><option value="4345">Jazz UK</option><option value="10527">Jazz Underneath</option><option value="7044">jazz unDone</option><option value="9192">Jazz upon a time</option><option value="15576">Jazz Vocalist and All About Jazz</option><option value="22770">Jazz, ese ruido</option><option value="16625">Jazz-Propaganda</option><option value="11485">Jazz: The Music of Unemployment</option><option value="9673">Jazz@Rochester</option><option value="20074">Jazzamatazz by Alan Bryson</option><option value="8476">Jazzblog</option><option value="14079">Jazzblog.ca by Peter Hum</option><option value="14362">Jazzblogg (N) </option><option value="7510">JazzBrew - A Musician's Online Journal</option><option value="45013">Jazzchops</option><option value="11347">JazzClinic Media</option><option value="12652">JazzEDge</option><option value="7176">Jazzenzo e-zine</option><option value="14847">Jazzevangelist</option><option value="9677">Jazzflute</option><option value="16894">JazzINK by Andrea Canter</option><option value="1112">JAZZIZ Magazine</option><option value="9708">Jazzline</option><option value="7055">JazzListening</option><option value="8668">JazzMando.com</option><option value="20201">Jazzmogul Blog</option><option value="7059">JazzNyt on Jazzspecial.dk</option><option value="7046">jazzofonikjamaica</option><option value="17420">Jazzpages</option><option value="14338">JazzPDX</option><option value="7056">JazzPortraits</option><option value="12786">JazzPress</option><option value="9825">Jazzseen</option><option value="8732">JazzSpecial</option><option value="10655">JazzStage Productions</option><option value="14334">JazzStage.net</option><option value="6058">Jazzthetik</option><option value="6059">Jazzthing</option><option value="1178">JazzTimes Magazine</option><option value="7754">JazzTO.ca</option><option value="22824">JazzTonic</option><option value="11322">JazzWax by Marc Myers</option><option value="33127">JazzWeek</option><option value="35995">Jazzwise Magazine</option><option value="14607">Jazzword</option><option value="7043">JazzWriter</option><option value="10187">Jazzy Jazz</option><option value="13679">JAZZzology by Richard Watters  </option><option value="13295">JBPR/Joanne Barrett Public Relations</option><option value="13166">JCM Media</option><option value="13145">Jeff Newelt</option><option value="9705">Jeff Rzepiela Public Relations</option><option value="11136">Jeffrey James Arts Consulting</option><option value="13371">Jenine Baines</option><option value="10998">Jeremy Bonaventura</option><option value="10422">Jerry Zigmont Blog</option><option value="9464">Jessica Fagan</option><option value="4880">Jim Eigo, Jazz Promo Services</option><option value="12720">Jim Goyjer Associates</option><option value="7570">Jim Pepper Lives!</option><option value="8752">Jizel Music</option><option value="12742">JM Creative </option><option value="9742">Jo Morello, Inc.</option><option value="14311">Johnson Music</option><option value="18670">Jona Rapoport Artist Management/PR</option><option value="9478">Jonas Music Services</option><option value="10745">Jonathan Kantor - Blue Note Jazz Club</option><option value="44295">JP Cutler Media</option><option value="11689">Judi Kerr Public Relations</option><option value="16854">Juliet Kelly</option><option value="4837">Jus' Jazz</option><option value="13985">Jussi Halttunen</option><option value="11056">Just Roots PR</option><option value="8745">Karen Sloan</option><option value="5098">Kari-On Productions</option><option value="10904">Karin Haerter</option><option value="14324">Kate Ross / KR Publicity </option><option value="10949">Kayos Productions, Inc.</option><option value="14521">Keep Swinging</option><option value="13663">Ken Franckling's Jazz Notes</option><option value="1185">Keyboard</option><option value="14764">Killerbees@Waid's Blog</option><option value="6216">Kim Smith Public Relations</option><option value="14127">Kim Taylor PR</option><option value="9690">Kimmel Center for the Performing Arts</option><option value="12465">KJPR Publicity and Artist Relations</option><option value="44209">Klein Lyons</option><option value="10593">Kluger Media Group, LLC</option><option value="12534">KMP, Inc.</option><option value="44857">Krakower Poling PR Corp</option><option value="17335">Kristalyn van de Water</option><option value="14181">Kultur</option><option value="14130">La Belle Usine Productions</option><option value="5555">La Femme Magazine</option><option value="13276">LA Music / Louie Austen Music</option><option value="16522">LA Weekly</option><option value="20281">Ladbury PR</option><option value="12594">Lampkin Publicity Service</option><option value="11554">Landon Music Company</option><option value="7741">Larry's Improv Page</option><option value="23850">Latin Perspective by Tony Vasquez</option><option value="15454">Latin Perspective-Latin Jazz Blog</option><option value="10149">Laura Henrich</option><option value="10755">Le son du grisli</option><option value="13160">Leading Edge Consulting</option><option value="10780">Leah Grammatica Public Relations</option><option value="14033">Leah's Blog</option><option value="11132">Learning Secrets</option><option value="9630">Lee Walter Associates</option><option value="11258">LenMar Promotions</option><option value="43373">Leonardo</option><option value="13900">Leonardo Pavkovic</option><option value="7039">Lerterland</option><option value="11036">LGB Media</option><option value="12436">Liberty Hill Music</option><option value="9660">Liberty Media Group, Inc.</option><option value="11274">Like Rain Whispers Mist</option><option value="12548">Linda S Froiland</option><option value="14695">Lip Service Media</option><option value="45518">Lisa Bautista, Public Relations </option><option value="10209">Lisa Cooney Carter</option><option value="4761">Lisa Reedy Promotions</option><option value="10275">Lisa St Cyr</option><option value="8825">Listen & Be Heard</option><option value="11786">LKS Associates</option><option value="13178">LMC Media</option><option value="13006">LondonJazz by Sebastian Scotney</option><option value="13322">Looking Glass Media</option><option value="8555">Lori Hehr Public Relations</option><option value="8992">Loronix</option><option value="10772">Lorraine Tucci</option><option value="11418">Lost In Transposition</option><option value="17308">Lotos Nile Media and Marketing</option><option value="11268">LRI Publicity</option><option value="14225">Lubricity</option><option value="24443">LUCK Media & Marketing, Inc.</option><option value="13456">Lura Belle Productions</option><option value="10484">Ly-chi</option><option value="10387">Lynk PR</option><option value="8548">Madison House Publicity</option><option value="7415">MAKIS - Jazznotes -Mykonos Greece</option><option value="44596">Mangomatter</option><option value="10684">Mantra Public Relations</option><option value="19259">Marble Arch PR</option><option value="17348">Marc Antomattei Productions</option><option value="8699">Margaret Davis Grimes</option><option value="45847">Mark Jonson</option><option value="11151">Mark Pucci Media</option><option value="9165">MarketPlan</option><option value="12676">Marseille & Company Management</option><option value="8500">Marshall Lamm Promotions & Public Relations</option><option value="12024">Marvelous Public Relations</option><option value="9928">Mary Curtin Productions</option><option value="7012">MaryLenore Arsenault, BopStar-PR</option><option value="30967">Mason Jar Media</option><option value="4693">Massive Music</option><option value="13718">MassJazz</option><option value="13959">MassJazz: Jazz in Massachusetts</option><option value="14032">Master Music Teacher Blog</option><option value="14401">Master of a Small House</option><option value="14008">Matthew</option><option value="21845">Matthew Kassel: Cold Jazz</option><option value="13484">Maven Communications</option><option value="38262">Mcfadden & Mcfadden Entertainment P.r.</option><option value="13749">McGuckin Entertainment PR</option><option value="47171">McMullen & Company Public Relations</option><option value="14116">MCRN - Public Relations</option><option value="19170">Media Maverick by by Greg Sandoval</option><option value="10021">Media Monster Communications</option><option value="14348">Media Stew Public Relations</option><option value="9442">Mel Bay Publications</option><option value="4690">Melomag Magazine</option><option value="10819">Mensch & Company, Inc.</option><option value="6507">MFA - Mitchell Feldman Associates</option><option value="10857">MG Consultants</option><option value="9685">Mi Bulin</option><option value="8557">Michael Bloom Media Relations</option><option value="10306">Michael Purdy</option><option value="5806">Michelle Hall Public Relations & Marketing</option><option value="10902">Michelle Roche Media Relations</option><option value="12898">MidemNet</option><option value="8040">Midwest Coast</option><option value="13489">Miles Davis Online</option><option value="10360">Miles High Productions</option><option value="13745">Miller Wright & Associates</option><option value="13413">Milos Jazz Cuarteto</option><option value="17470">minds on song with Gordon Marshall</option><option value="7782">Mingus Lives</option><option value="1234">Mississippi Rag</option><option value="11723">Mix Management, Bonnie Ahl</option><option value="4396">Mixed Media</option><option value="14133">MM Entertainment Publicity</option><option value="44823">Mobile Technologies Changes</option><option value="44824">Mobile Technologies Changes Today</option><option value="1183">Modern Drummer</option><option value="7607">Mondo Jazz</option><option value="12214">Mone Consulting</option><option value="11694">Monifa Brown</option><option value="9445">Monterey Jazz Festival</option><option value="4832">Monthly Herald</option><option value="14568">Mooney Marketing</option><option value="11289">More Better Jazz</option><option value="14374">Moses Supposes</option><option value="11148">Mosnar Communications, Inc</option><option value="18309">Mostly jazz, mostly from Italy</option><option value="19050">Mostly Music by Ronan Guilfoyle </option><option value="9724">Motormouthmedia</option><option value="32859">Mouthpiece Music</option><option value="9597">Mozilla Firefox Blog</option><option value="12285">Mt Crested Butte Town Center Community Association</option><option value="10033">MTN Media Relations</option><option value="10659">Mulligan Productions LLC</option><option value="12760">Muse Media</option><option value="16446">Music and More by Tim Niland</option><option value="5303">Music Book World</option><option value="12224">Music City News Media & Marketing</option><option value="46131">Music Notes From Wonder Of Sound</option><option value="9055">Musica es, entre otras cosas, musica</option><option value="14502">Musical Missing Piece</option><option value="22079">MusicDish</option><option value="11041">MusicDish Network</option><option value="14534">musicgrafias - goio villanueva jazz concerts photographer</option><option value="11732">Musicians of El Paso - Past and Present</option><option value="12713">MusicianWages.com</option><option value="7104">Musick</option><option value="10273">Musick by George Sessum</option><option value="5376">Musik International</option><option value="13644">Musikverlag Christoph Dohr</option><option value="12464">MusiMedia, L.L.C.</option><option value="7048">My Jazz Explorations</option><option value="11672">My Jazz World</option><option value="12715">My Job</option><option value="12954">My PR Music Wire</option><option value="25672">My Publicist</option><option value="14450">Myers Productions</option><option value="11387">Mykonos - Greece /  A Jazz Listener / by Makis</option><option value="13765">MyMusicSuccess.com</option><option value="9076">Nancy Edwards Entertainment</option><option value="13590">Nathan J. Silverman Company</option><option value="48084">New Album Latin Jazz "como Suena"</option><option value="19051">New Day</option><option value="20363">New Jazz Audience Media</option><option value="7899">New Jazz Noises</option><option value="14309">New Orleans jazz band-Hotstompers-BLOG</option><option value="13480">New York Jazz Academy Publicity</option><option value="15384">Ni Kantu by Clifford Allen</option><option value="11325">Night After Night</option><option value="11590">Night Lights Classic Jazz</option><option value="16873">nilesh Ahu</option><option value="13773">Nina Gordon Public Relations</option><option value="9003">Nina Lesowitz</option><option value="20470">Nippertown!</option><option value="11246">Noble PR</option><option value="13638">Nonesuch Publicity</option><option value="15513">Notes on Jazz by Ralph A. Miriello</option><option value="9681">Novol</option><option value="10277">Now Forward Music</option><option value="13408">Nu Jazz Entertainment</option><option value="11509">Nu Jazz in a Nu Time</option><option value="43922">Nu Jazz Publicity</option><option value="13341">Nu-Jazz International</option><option value="9837">O&M Co.</option><option value="11662">Obie Joe Media</option><option value="26477">Oceanbound Entertainment Inc.</option><option value="9094">Off Minor</option><option value="13019">Offbeat Music Magazine</option><option value="10961">Olios Music Page</option><option value="44949">Omni Eye & Vision</option><option value="8441">On Target Media Group</option><option value="7049">On The Fringe Of Jazz</option><option value="12961">On Wax Intimate Portrait Of Chanda Rule </option><option value="14345">One Creative Publicist & Management</option><option value="14193">One Working Musician by Jason Parker</option><option value="32641">Optfirst Inc</option><option value="12719">Orange Grove Artists</option><option value="15707">orange moon</option><option value="12320">Orloff/Williams Advertising & Public Relations</option><option value="11618">other-now.</option><option value="13453">Outerglobe</option><option value="14892">OUTHOUSE pr</option><option value="14835">Outside Organisation</option><option value="21612">Oxford University Press</option><option value="9060">Palmetto Records</option><option value="11152">Paperwork Media</option><option value="9074">Paradigm Shift PR</option><option value="8578">Paris Loves Jazz</option><option value="13159">Pat Courtemanche</option><option value="10359">Patricia Adams Ensembles</option><option value="8709">Paul Collins Artist Management International</option><option value="43134">Paula Lavitz - PR, Management & Booking</option><option value="45726">Paula Maya: A Brazilian Living In Austin, Tx</option><option value="12144">PC Magazine</option><option value="13093">PCS Management</option><option value="10044">Perfect Sounds</option><option value="13826">Peter McDowell Arts Consulting</option><option value="17245">Peter Pearson</option><option value="11888">Photonality Jazz Series</option><option value="9720">Phred, Inc.</option><option value="8512">Piano Music by Paul Toboey</option><option value="1194">Piano Today</option><option value="8859">Piano y Jazz-Flamenco</option><option value="10743">pianologist</option><option value="4720">Picaro Press</option><option value="12654">Pitch Perfect PR</option><option value="13556">PKPR</option><option value="11544">Plastic Sax</option><option value="5699">PlaybillArts.com</option><option value="11002">PlayJazzNow PR</option><option value="19260">Polish Jazz </option><option value="14057">Pop Dose</option><option value="14529">Porão do Jazz</option><option value="14645">Pork Pie Jazz</option><option value="12164">Powderfinger Promotions</option><option value="11780">Prague Jazz</option><option value="10133">Prana Marketing</option><option value="10059">PRemiere Public Relations</option><option value="10168">Press Dept</option><option value="13806">Press Junkie PR</option><option value="32521">Pressat</option><option value="14623">Prog Sphere</option><option value="13693">Protege</option><option value="13366">PRThatRocks.com</option><option value="14070">Public Emily</option><option value="13467">PublicEye</option><option value="6263">Purpose Media Management</option><option value="14518">PUSH Revolution</option><option value="9415">Quiet + Loud PR</option><option value="14372">Quintaesencia</option><option value="11741">Radio Jazz Guy</option><option value="24531">RadioCast Marketing</option><option value="5258">Rainmaker Public Relations</option><option value="9511">Randex Communications</option><option value="12656">Rebecca Bell Sorensen</option><option value="44984">Rebecca Davis PR</option><option value="7943">Red Hot Jazz</option><option value="7054">Redhouse Jazz</option><option value="7808">Reflections of a Jazz Theologian</option><option value="15033">Rehearsing The Blues</option><option value="12003">Relix Magazine</option><option value="11737">Renee Priemon</option><option value="9868">Reva Cooper</option><option value="11239">Reviews and All That Jazz</option><option value="12530">RGillettPR</option><option value="46343">Rhino Digital Printing Inc.</option><option value="11175">Richard Underhill Promotions</option><option value="9655">Rick McLaughlin's Listen</option><option value="14208">Riffs on Jazz by John Anderson</option><option value="7040">Rifftides by Doug Ramsey</option><option value="8567">RioVida Networks</option><option value="22558">RJ Johnson's Modern Music</option><option value="14710">RJ on Jazz by R.J. DeLuke</option><option value="9461">RL Productions</option><option value="43906">Robert Neeson</option><option value="22319">Robert Rucker Project</option><option value="22320">Robert Rucker Project</option><option value="13098">rock paper scissors, inc. </option><option value="8689">Rocket Words</option><option value="14400">Rockonola</option><option value="17471">Rockyoumentally</option><option value="21789">Rogers & Cowan</option><option value="1181">Rolling Stone</option><option value="17338">Rolling Thunder PR</option><option value="14577">Ron Moss's Blog</option><option value="13075">Ron Ross</option><option value="22556">Ron s Audio Blog</option><option value="8521">Rooftop Promotion</option><option value="8802">RoseKing Productions</option><option value="12704">Rouse Public Relations</option><option value="19257">RV IT Group</option><option value="11287">RVAjazz</option><option value="12617">Ryan Xzavier Public Relations</option><option value="14170">Ryko</option><option value="12788">Sacks and Co.</option><option value="11739">Sally Fischer Public Relations</option><option value="8853">SaskJazzJam</option><option value="8587">SASSAS</option><option value="8024">Saxindia</option><option value="17541">Schell Barkley</option><option value="14495">Science and Music</option><option value="32401">Scott Thompson Public Relations</option><option value="13000">Scott Yanow</option><option value="7062">Scratch My Brain</option><option value="11473">Scullyone Productions</option><option value="13060">Seattle Jazz Scene</option><option value="13785">See! Hear! by Richard Kamins</option><option value="10568">Seeing Jazz</option><option value="15854">seoworkblog</option><option value="13796">Services for Artists</option><option value="10284">Seth Cohen PR</option><option value="11430">Settled in Shipping</option><option value="25103">SF Media</option><option value="8835">Shannon Butcher Sings</option><option value="5065">Sheba Media PR</option><option value="44186">Shikshanow</option><option value="15614">Shira Gilbert PR</option><option value="8642">Shore Fire Media</option><option value="13267">Shout Marketing and Media Relations</option><option value="8870">ShrumV</option><option value="43708">Shuttercraft Bristol</option><option value="43707">Shuttercraft Cambridge</option><option value="43706">Shuttercraft Cheshire</option><option value="9933">Sibiu People</option><option value="11695">Siddhartha Mitter</option><option value="4104">Signal to Noise</option><option value="12577">Silkworks</option><option value="43908">Simply Elegant</option><option value="8473">Simply Jazz</option><option value="15984">Sing</option><option value="9438">Sirius Satellite Radio</option><option value="14642">SIX media marketing inc</option><option value="7777">Skip's Live Journal</option><option value="22606">SloaneView </option><option value="22589">Smooth-Jazz-Band Indigo mit neuem Album</option><option value="14247">So What</option><option value="8101">SobreSites Jazz Guide</option><option value="14302">SoCal Creative Music</option><option value="9854">SOJAZZ</option><option value="14488">Something Else!</option><option value="22022">Sony Music Entertainment/Legacy</option><option value="13780">Sortiesjazznights.com PR</option><option value="9541">Soul on Track</option><option value="10605">Soulstice Marketing</option><option value="18141">Sound Insights by Doug Payne</option><option value="9697">Sound Slope</option><option value="20792">Sound Unities by Chris Rich</option><option value="12316">Sound Visions Media</option><option value="19792">Speakin' the Blues</option><option value="9698">Special Ops Media</option><option value="49783">Sphinx Publicity</option><option value="13899">Spinergy Group</option><option value="11382">Spinning in Air</option><option value="13249">Spirit Of New Orleans Productions</option><option value="18753">Spoony's Music Diary </option><option value="7404">St. Louis Jazz Notes by Dean Minderman</option><option value="13862">STAR PR</option><option value="9300">Stax Museum of American Soul Music</option><option value="13905">Stephan Earl - The Art of Expression</option><option value="9788">Steve Carter</option><option value="20923">Steve Griggs Music</option><option value="45569">Steven Smith</option><option value="12268">Stop and Hear the Music</option><option value="13197">Straight No Chaser </option><option value="12346">StraightOut Media & Marketing </option><option value="1206">Sub Par Publicity</option><option value="8874">Sue Auclair Promotions</option><option value="12276">Suezenne Fordham Chamber Jazz LA</option><option value="12395">Sunnyside Records</option><option value="12445">Survival Of The Cool: Jazz and everything.</option><option value="10590">Susan Blond, Inc.</option><option value="13255">Susan Marie Public Relations</option><option value="25430">Susan O'Kane</option><option value="10077">Swing Journal</option><option value="10166">Swing, Jazz and Blues</option><option value="14528">Sydney Jazz Blog</option><option value="12766">Sylvain Music Notes</option><option value="11552">SZPR, Inc.</option><option value="7744">Take Five Music</option><option value="15059">Take The "A" Train </option><option value="10550">Tampa Jazz Scene</option><option value="10245">Tanisha Jackson</option><option value="48150">Tapping PR</option><option value="10932">TD Entertainment</option><option value="10406">Tehillah Enterprises, LLC</option><option value="27736">Tellef Øgrim</option><option value="13968">Tellem Worldwide</option><option value="18545">Tenfour Communications </option><option value="8637">Teresa Conboy PR</option><option value="9360">Terri Hinte</option><option value="14252">Texoma Living! Magazine Jazz Series</option><option value="8339">That Jazz</option><option value="13297">The Aroengbinang Project</option><option value="9502">The Attention Group</option><option value="14368">The Augmented Ear by John Patten</option><option value="11048">the BandA ecLectics</option><option value="25765">The Brokaw Company</option><option value="15514">The Business Musician by Craig M. Cortello</option><option value="13321">The Chill Vibe Experience/Infinite Rhythms</option><option value="9664">The Cinarae Group</option><option value="7179">The Daily Jazz</option><option value="14192">The Domino Theory by Jeff Winbush</option><option value="11428">The Ear of the Mind</option><option value="13200">The Ferraro Group</option><option value="19086">The Folk Who Feel Jazz</option><option value="9818">The Frank Agency</option><option value="8417">The Golden Era of Jazz</option><option value="22153">THE GREAT AMERICAN SONGBOOK BLOG</option><option value="9522">The Guitar Channel</option><option value="10220">The Hi De Ho Blog</option><option value="12437">The HUSH Foundation Public Relations Dept.</option><option value="11398">The Independent Ear by Willard Jenkins</option><option value="15432">The Jazz 89.5 Blog</option><option value="8892">The Jazz Clinic</option><option value="8266">The Jazz Guitarist</option><option value="22564">The Jazz Line</option><option value="7045">The Jazzcat</option><option value="13064">The Jessamine Vine</option><option value="14147">The Judy Nelon Group</option><option value="12031">The Klein Group, Inc.</option><option value="10399">The Latin Jazz Corner by Chip Boaz</option><option value="14229">The Mantle</option><option value="22071">The Melodic Drummer</option><option value="12893">The Miles Davis Movie</option><option value="14451">The Mirror Group, LLC Events</option><option value="46111">The Mix</option><option value="13356">The New York Times</option><option value="12026">The Next Level PR & Events - Sue McCallum</option><option value="11679">The Opulent Influence Agency</option><option value="11138">The Outward Spiral</option><option value="14644">The Phillips Agency</option><option value="8636">The Practice Room</option><option value="11205">The Ring Modulator by Adrian Stevenson</option><option value="13621">The SideMan</option><option value="14353">The Stringslinger</option><option value="7802">The Torch Singer</option><option value="5964">The Wattree Chronicle By Eric L. Wattree</option><option value="12714">The Working Musician</option><option value="13716">Theater League</option><option value="8852">theloneliestmonk</option><option value="11675">ThinkKing Media, Inc.</option><option value="10689">ThinkTank Marketing</option><option value="14409">This Shape of Jazz</option><option value="35293">Thomas Pena</option><option value="8930">Tijuana Gift Shop</option><option value="9736">Times Square Press</option><option value="13829">TMA E-Marketing</option><option value="7771">Tokyo Nights</option><option value="9890">Tom Abbott</option><option value="12014">Tom Estey Publicity & Promotion</option><option value="12828">Tom La Meche's jazz Blog</option><option value="16427">Tom Powers Jazz House</option><option value="12892">Tom Tallitsch Productions, LLC</option><option value="7047">Tomorrow Jazz</option><option value="11715">Toni Ballard PR</option><option value="15567">Tony Flood's House of Hard Bop</option><option value="12787">Toolshed</option><option value="10541">Total Marketing Solution</option><option value="13279">Tracey Miller and Associates</option><option value="19136">Tracy's Musical Exploratorium</option><option value="12078">Translantis Media Group</option><option value="9499">Translantis Music</option><option value="22757">Trivitt Public Relations, Inc.</option><option value="7973">Tsunami Publicity</option><option value="9220">Tulip Songs International</option><option value="7527">Tune Up</option><option value="8083">Two for the Show Media</option><option value="9018">Two Sheps That Pass</option><option value="9563">ugEXPLODE</option><option value="10039">Ulverston Jazz Blog</option><option value="16107">underyourskin</option><option value="12389">Universal Music Group</option><option value="11802">University of the Arts, Media Relations</option><option value="12572">Unlimited Resources, Inc.</option><option value="13364">Urban Fuzhen Enterprises</option><option value="16649">USA News Trends</option><option value="22637">Valon kuvia - Blogi</option><option value="9233">Verbronics Enterprises</option><option value="12393">Verve Music Group</option><option value="14659">Visa Views</option><option value="8280">Vision Song</option><option value="13854">Vivo Musique Internationale</option><option value="26367">Vjm's Jazz & Blues Mart</option><option value="7905">Vocal Jazz</option><option value="9782">VocalJazzJournal</option><option value="14369">Voll-Damm Festival Internacional De Jazz De Barcelona</option><option value="8550">W3 Public Relations</option><option value="9746">Walkabout</option><option value="25395">Watch Jazz Happen by Dan Kassell</option><option value="14964">We All Make Music</option><option value="10520">WeareFlex</option><option value="16329">What Is Life</option><option value="12521">Wildfire Publicity</option><option value="12777">Winged Flight Publicity</option><option value="10927">Wiredset</option><option value="13136">Wolfgang's Vault</option><option value="4756">World Art Celebrities Journal</option><option value="11281">World Improvised Music</option><option value="9795">World Who's Who in Jazz Cabaret Music and Entertainment</option><option value="14542">Wreckhouse Jazz & Blues</option><option value="11593">Writers House</option><option value="13940">Wynton Marsalis Enterprises, Inc.</option><option value="42965">Ximen Reed</option><option value="7231">Yoon Choi's "What's Cooking?"</option><option value="49312">You And Whose Army Media</option><option value="13485">Zarmedia</option><option value="20488">zeblog</option><option value="9665">Zerodimension</option><option value="14049">Zzzing Agency</option></select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 f-right">
                            <button type="submit" class="btn btn-success">Find!</button>
                        </div>
                    </div>
                </form>
            </div>			

            <div role="tabpanel" class="tab-pane fade in" id="search_download">
                <form action="http://media.allaboutjazz.com/index.php" method="get" class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>musician</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input  id="aajsearch_musicianname" type="text" name="name" maxlength="30" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 f-right">
                            <button type="submit" class="btn btn-success">Find!</button>
                        </div>
                    </div>
                </form>
            </div>
			
            <div role="tabpanel" class="tab-pane fade in" id="search_musician">
                <form action="http://musicians.allaboutjazz.com/index.php" method="post" class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>name</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input name="in_name" id="aajsearch_musician" type="text" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>instrument</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <select name="in_instr"><option value=0></option><option value="1">accordion</option><option value="3">arranger</option><option value="136">author</option><option value="4">bass</option><option value="5">bass, acoustic</option><option value="7">bass, electric</option><option value="122">band/orchestra</option><option value="9">banjo</option><option value="127">bandoneon</option><option value="11">bongos</option><option value="124">bassoon</option><option value="12">clarinet</option><option value="138">clarinet, bass</option><option value="17">cello</option><option value="19">clave</option><option value="21">composer/conductor</option><option value="22">congas</option><option value="23">cornet</option><option value="25">cymbal</option><option value="26">drums</option><option value="27">drum programming</option><option value="121">drums, steel</option><option value="30">digital transfer</option><option value="133">Electronics</option><option value="31">engineer</option><option value="32">english horn</option><option value="134">euphonium</option><option value="33">flute</option><option value="35">Fender Rhodes</option><option value="36">flugelhorn</option><option value="37">french horn</option><option value="38">guitar</option><option value="39">guitar, 8-string</option><option value="40">guitar, 12-string</option><option value="110">guitar, acoustic</option><option value="41">guitar, cello</option><option value="120">guitar, electric</option><option value="44">guitar, slide</option><option value="45">guitar, steel</option><option value="49">harmonica</option><option value="119">h'arpeggione</option><option value="50">harp</option><option value="52">keyboard</option><option value="137">kora</option><option value="131">koto</option><option value="135">live sampling</option><option value="130">lute</option><option value="123">lyricist</option><option value="53">mandolin</option><option value="57">melodica</option><option value="132">multi-instrumentalist</option><option value="60">organ, Hammond B3</option><option value="62">oboe</option><option value="128">oud</option><option value="64">piano</option><option value="65">piano, electric</option><option value="125">pandeiro</option><option value="67">percussion</option><option value="69">piccolo</option><option value="70">producer</option><option value="71">programming</option><option value="72">reeds</option><option value="73">saxophone</option><option value="74">sax, alto</option><option value="75">sax, baritone</option><option value="76">sax, soprano</option><option value="77">sax, sopranino</option><option value="78">sax, tenor</option><option value="79">samples/effects</option><option value="80">sequencing</option><option value="82">sitar</option><option value="139">sousaphone</option><option value="85">synthesizer</option><option value="86">trumpet</option><option value="87">trumpet, bass</option><option value="88">trumpet, piccolo</option><option value="89">tumpet, soprano</option><option value="90">tablas</option><option value="93">trombone</option><option value="95">tuba</option><option value="96">turntable</option><option value="129">ukulele</option><option value="99">vocalist</option><option value="101">various</option><option value="102">vibraphone</option><option value="103">viola</option><option value="104">violin</option></select>
                        </div>
                    </div>			
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 f-right">
                            <button type="submit" class="btn btn-success">Find!</button>
                        </div>
                    </div>
                </form>
            </div>

            <div role="tabpanel" class="tab-pane fade in" id="search_gallery">
                <form action="http://photos.allaboutjazz.com/index.php" method="get" class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-4">
                            <label>by <strong>photo tag</strong></label>
                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <input id="aajsearch_tagInput" type="text" name="tag" size="32" maxlength="32" />
                        </div>
                    </div>		
                    <div class="row">
                        <div class="col-xs-12 col-sm-8 f-right">
                            <button type="submit" class="btn btn-success">Find!</button>
                        </div>
                    </div>
                </form>
            </div>
				</div>		
		<p><a href="http://www.allaboutjazz.com/php/search.php">More search options</a></p>
		or search site <strong>with Google</strong>
		<gcse:searchbox-only resultsUrl="http://www.allaboutjazz.com/googlesearch.php" newWindow="true" queryParameterName="q"></gcse:searchbox-only>
      </div>
    </div>
  </div>
</div><div id="slide-panel" class="padding-20">
	<div id="slide-jazzstory">	
		<a href="#" class="btn btn-danger" id="opener" title="Share your jazz story with our readers">
							<i class="fa fa-pencil"></i>
					</a>
		<h3>Celebrate jazz with your story!</h3>
		<p class="styledtext bottom-40">Tell us what jazz means to you and we'll share your story with our readers.</p>
		<div class="text-center bottom-40">
			<a role="button" class="btn btn-lg btn-primary" href="http://bit.ly/1S2FuJz" title="Learn more about jazz story of the day!">Get Started</a>
		</div>
	</div>
</div>
<script>

		$('#opener').on('click', function() {		
		var panel = $('#slide-panel');
		if (panel.hasClass("visible")) {
			$("#opener").html('<i class="fa fa-pencil"></i>');
			panel.removeClass('visible').animate({'margin-right':'-400px'});
		} else {
			$("#opener").html('<i class="fa fa-times"></i>');
			panel.addClass('visible').animate({'margin-right':'0px'});
		}	
		return false;	
	});
</script>	</div><!-- .page-box-content -->
</div><!-- .page-box -->

<div class="clearfix"></div>


<script src="http://www.allaboutjazz.com/js2/bootstrap.min.js"></script>
<script src="http://www.allaboutjazz.com/js2/main_v2015.js"></script></body>
</html>