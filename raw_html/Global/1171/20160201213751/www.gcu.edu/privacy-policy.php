﻿<!DOCTYPE html SYSTEM "about:legacy-compat"><html lang="en">
<head>
<META http-equiv="Content-Type" content="text/html">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
                
                                    
            
                    
                                                <base href="http://www.gcu.edu/">
                    
                
<!--BEGIN SEOGroup-->
<title>Academic Policies &amp; Accreditations | Grand Canyon University</title>
<meta name="description" content="Review GCU’s accreditation, the organizations that confer program and regional accreditation, and key academic policies that impact students.">
<meta name="keywords" content="academic policy, accreditation, Grand Canyon University">
<!--END SEOGroup-->
<!--CSS--><link rel="stylesheet" href="prebuilt/css/inc/bootstrap3.3.5.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto: 400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Crimson+Text: 400,400italic,600,600italic,700,700italic">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="prebuilt/css/main.css">
<noscript><link rel="stylesheet" href="prebuilt/css/noscript.css"></noscript>
<style type="text/css">
            

            .extra-padding.rounded {
                padding: 30px 15px;

            }

            .Section_Tabbed .container.margin-center {
                margin-bottom:60px;
            }

            .Section_Tabbed .container.margin-center:last-child {
                margin-bottom:0px;
            }



                
        </style><!--Header Javascripts--><script src="prebuilt/js/jquery.1_11_2.js"></script><script src="prebuilt/js/modernizr.custom.17475.js"></script><script src="prebuilt/js/json2.js"></script><script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-23969835-30', 'auto');
            ga('send', 'pageview');
        </script><script src="prebuilt/js/typed.js"></script><script>
            $(function(){
                $(".future-student-search-input").typed({
                    strings: ["Registered Nurse", "Software Developer", "Business Analyst","Nutritionist"],
                    attr: "placeholder",
                    // typing speed
                    typeSpeed: 100,
                    // time before typing starts
                    startDelay: 700,
                    // backspacing speed
                    backSpeed: 10,
                    // time before backspacing
                    backDelay: 3000,
                    // show cursor
                    showCursor: true,
                    // character for cursor
                    cursorChar: "|",
                    loop: true
                });
            });

            $(document).ready( function() {
                // Show/hide on focusin/focusout
                $(".future-student-search-input").focusin( function() {
                    $(this).addClass("hidePlaceholder");
                });
                $(".future-student-search-input").focusout( function() {
                    $(this).removeClass("hidePlaceholder");
                });
            });

            $(document).ready(function(){
                var thisPage = window.location.href;
                $('a[data-href].anchor').on('click',function(){
                    var anchor = $(this).attr('data-href');
                    var scroll = $(anchor).offset().top;
                    $(window).scrollTop(scroll - 250);
                });
            });
        </script>
            
                <!--[if IE 9]>
                <link rel="stylesheet" href="prebuilt/css/inc/ie9-fixes.css">
                <![endif]-->

                <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <link rel="stylesheet" href="prebuilt/css/inc/ie8-fixes.css">
                <![endif]-->

                <!--[if lte IE 7]>
                <link rel="stylesheet" href="prebuilt/css/inc/ie7-fixes.css">
                <![endif]-->
            
        <!--IGX Variables--><meta name="igx-site" content="">
<meta name="igx-environment" content="pub_redesign">
<meta name="igx-base" content="http://www.gcu.edu/">
<meta name="igx-site" content="Redesign"><!--END IGX Variables-->
</head>
<body>
<div id="bodyOverlay"></div>
<div class="site-msg ie-msg alert alert-danger" role="alert">Attention User: For a better user experience, please consider updating <a href="http://windows.microsoft.com/en-US/internet-explorer/download-ie" target="_blank">Internet Explorer</a> or using <a href="https://www.mozilla.org/" target="_blank">Mozilla Firefox</a> or <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Google Chrome</a>.</div>
                
                            
            <header role="banner"><!--Start Navigation--><!--Start Mobile Navigation--><nav id="mobileNav" class="navmenu navmenu-default navmenu-fixed-right offcanvas" aria-label="Mobile navigation"><div id="mobileInfoBlock">
<div id="mobilePhone"><i class="fa fa-phone-square"></i> 1-855-GCU-LOPE</div>
<div id="mobileApply"><a href="http://apply.gcu.edu" target="_blank" onclick="ga('send','event','navigation','click','apply-now')">Apply Now</a></div>
<div id="mobileSearch">
<form action="search.php" method="get" class="form-horizontal">
<div class="input-group"><input type="search" name="q" id="input" class="form-control" value="" required="required" title=""><span class="input-group-addon"><a href="" role="button"><i class="fa fa-search"></i><span class="sr-only">Search</span></a></span></div>
</form>
</div>
</div>
<div>
<h2>Main Navigation Title</h2>
<ul class="nav navbar-nav">
<div class="panel-group" id="accordion" aria-multiselectable="true">
<div class="panel panel-default">
<div class="panel-heading" id="headingOne">
<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            ABOUT GCU
                                        </a></h4>
</div>
<div id="collapseOne" class="panel-collapse collapse" aria-labelledby="headingOne">
<div class="panel-body">
<ul>
<li><a href="about-gcu/leadership-team.php" target="">Leadership</a></li>
<li><a href="about-gcu/university-snapshot.php" target="">University Snapshot</a></li>
<li><a href="about-gcu/history-of-gcu.php" target="">History &amp; Campus Growth</a></li>
<li><a href="about-gcu/christian-identity-and-heritage.php" target="">Christian Identity and Heritage</a></li>
<li><a href="about-gcu/community-outreach-volunteering.php" target="">Outreach</a></li>
<li><a href="about-gcu/offices-services.php" target="">Offices</a></li>
<li><a href="about-gcu/media-and-branding.php" target="">Media &amp; Branding</a></li>
<li><a href="about-gcu/locations.php" target="">Locations</a></li>
<li><a href="about-gcu/contact.php" target="">Contact</a></li>
</ul>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" id="headingTwo">
<h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Academics
                                        </a></h4>
</div>
<div id="collapseTwo" class="panel-collapse collapse" aria-labelledby="headingTwo">
<div class="panel-body">
<ul><label>Colleges</label><li><a href="college-of-doctoral-studies.php" target="">College of Doctoral Studies</a></li>
<li><a href="colangelo-college-of-business.php" target="">Colangelo College of Business</a></li>
<li><a href="college-of-education.php" target="">College of Education</a></li>
<li><a href="college-of-fine-arts-and-production.php" target="">College of Fine Arts and Production</a></li>
<li><a href="college-of-humanities-and-social-sciences.php" target="">College of Humanities and Social Sciences</a></li>
<li><a href="college-of-nursing-and-health-care-professions.php" target="">College of Nursing and Health Care Professions</a></li>
<li><a href="college-of-science-engineering-and-technology.php" target="">College of Science, Engineering and Technology</a></li>
<li><a href="college-of-theology.php" target="">College of Theology</a></li>
<li><a href="honors-college.php" target="">Honors College</a></li>
</ul>
<ul><label>More Information</label><li><a href="academics/provost-message.php" target="">Provost Message</a></li>
<li><a href="academics/educational-alliances.php" target="">Educational Alliances</a></li>
<li><a href="academics/majors-and-programs.php" target="">Majors &amp; Programs</a></li>
<li><a href="academics/faculty-directory.php" target="">Faculty Directory</a></li>
<li><a href="academics/academic-policies.php" target="">University and Academic Policies</a></li>
<li><a href="academics/operation-impact.php" target="">Operation Impact </a></li>
<li><a href="academics/office-of-assessment.php" target="">Office of Assessment</a></li>
</ul>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" id="headingThree">
<h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Admissions
                                        </a></h4>
</div>
<div id="collapseThree" class="panel-collapse collapse" aria-labelledby="headingThree">
<div class="panel-body">
<ul>
<li><a href="admissions/college-transfer-center.php" target="">Transfer Center</a></li>
<li><a href="admissions/requirements.php" target="">Admission Requirements</a></li>
<li><a href="admissions/tuition-and-financing.php" target="">Tuition &amp; Scholarships</a></li>
</ul>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" id="headingFour">
<h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            Future Students
                                        </a></h4>
</div>
<div id="collapseFour" class="panel-collapse collapse" aria-labelledby="headingFour">
<div class="panel-body">
<ul>
<li><a href="future-students/campus-experience.php" target="">Campus</a></li>
<li><a href="future-students/online-degree-programs.php" target="">Online</a></li>
<li><a href="future-students/evening-classes.php" target="">Evening</a></li>
<li><a href="future-students/international-students-.php" target="">International Students</a></li>
<li><a href="future-students/foreign-students-.php" target="">Students with Foreign Credentials</a></li>
<li><a href="future-students/military-university.php" target="">Military &amp; Veteran</a></li>
<li><a href="future-students/army-rotc-programs.php" target="">ROTC</a></li>
<li><a href="future-students/tribal.php" target="">Tribal</a></li>
<li><a href="future-students/visit-gcu-campus.php" target="">Visit Campus</a></li>
<li><a href="future-students/campus-resources.php" target="">Campus Resources</a></li>
</ul>
</div>
</div>
</div>
</div>
<h2>Secondary Navigation Title</h2>
<ul id="mobileVisitorType">
<li><a href="http://students.gcu.edu/" onclick="ga('send','event','navigation','click','current-students')">Current Students</a></li>
<li><a href="http://alumni.gcu.edu/" onclick="ga('send','event','navigation','click','alumni')">Alumni</a></li>
<li><a href="http://www.gculopes.com/" target="_blank" onclick="ga('send','event','navigation','click','athletics')">Athletics</a></li>
<li><a href="http://www.gcuarena.com/" target="_blank" onclick="ga('send','event','navigation','click','arena')">Arena</a></li>
</ul>
</ul></div></nav><nav id="mainNav" class="navbar navbar-default navbar-fixed-top" aria-label="Navigation"><div class="navbar-header" data-spy="affix" data-offset-top="150">
<div class="site-msg ie-msg alert alert-danger" role="alert">Attention User: For a better user experience, please consider updating <a href="http://windows.microsoft.com/en-US/internet-explorer/download-ie" target="_blank">Internet Explorer</a> or using <a href="https://www.mozilla.org/" target="_blank">Mozilla Firefox</a> or <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Google Chrome</a>.</div>
                
                            
            <div class="collapse navbar-collapse">
<div class="container">
<div class="row">
<div class="col-md-5">
<ul class="nav navbar-nav">
<li><a href="http://students.gcu.edu/" onclick="ga('send','event','navigation','click','current-students')">Current Students</a></li>
<li><a href="http://alumni.gcu.edu" onclick="ga('send','event','navigation','click','alumni')">Alumni</a></li>
<li><a href="http://www.gculopes.com/" target="_blank" onclick="ga('send','event','navigation','click','athletics')">Athletics</a></li>
<li><a href="http://www.gcuarena.com/" target="_blank" onclick="ga('send','event','navigation','click','arena')">Arena</a></li>
</ul>
</div>
<div class="col-md-7">
<div class="pull-right" id="actions">
<div id="headerPhone"><i class="fa fa-phone-square"></i> 1-855-GCU-LOPE
                                        </div>
<div id="rmi">
<form id="rmiButtonForm" action="/lp/rmi_hawthornform"><button id="rmiButton" class="btn btn-primary" type="submit">Request Information</button></form>
</div>
<div id="apply"><a href="http://apply.gcu.edu" target="_blank" onclick="ga('send','event','navigation','click','apply-now')">Apply Now</a></div><button class="btn btn-primary" id="headerSearch"><i class="fa fa-search"></i><span class="sr-only">Search</span></button><form action="search.php" method="get">
<div id="search-box">
<div class="form-group"><label class="sr-only" for="mainSearch">Enter a keyword to search</label><div class="input-group"><input type="text" class="form-control" id="mainSearch" name="q" placeholder="keyword search"><a href="#" id="mainSearchClose" class="input-group-addon"><i class="fa fa-close"></i><span class="sr-only">Close</span></a></div>
</div>
</div><button class="btn btn-primary" id="goButton" type="submit">Go</button></form>
</div>
</div>
</div>
</div>
<div class="navbar-bottom container-fluid">
<div class="container">
<div class="row">
<div class="breadcrumb-box" style="bottom: 0px;" role="menu" aria-label="Breadcrumbs"><a href="index.php" target="" role="menuitem">Home</a>  
                <i class="fa fa-angle-double-right"></i>  
                    <a href="academics/academic-policies.php" target="" role="menuitem">University and Academic Policies</a>  
                </div>
<div class="navbar-brand hidden-xs"><a href="http://www.gcu.edu"><img src="prebuilt/img/logo.png" alt="Grand Canyon University Logo" aria-haspopup="true"></a></div>
<ul class="nav navbar-nav pull-right" id="main-nav">
<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">ABOUT GCU</a><ul class="dropdown-menu first">
<div class="row">
<div class="col-sm-6 left">
<ul>
<li><a href="about-gcu/leadership-team.php" target="">Leadership</a></li>
<li><a href="about-gcu/university-snapshot.php" target="">University Snapshot</a></li>
<li><a href="about-gcu/history-of-gcu.php" target="">History &amp; Campus Growth</a></li>
<li><a href="about-gcu/christian-identity-and-heritage.php" target="">Christian Identity and Heritage</a></li>
<li><a href="about-gcu/community-outreach-volunteering.php" target="">Outreach</a></li>
</ul>
</div>
<div class="col-sm-6 right">
<ul>
<li><a href="about-gcu/offices-services.php" target="">Offices</a></li>
<li><a href="about-gcu/media-and-branding.php" target="">Media &amp; Branding</a></li>
<li><a href="about-gcu/locations.php" target="">Locations</a></li>
<li><a href="about-gcu/contact.php" target="">Contact</a></li>
</ul>
</div>
</div>
</ul>
</li>
<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">ACADEMICS</a><ul class="dropdown-menu second">
<div class="row">
<div class="col-sm-7 left"><label>Colleges</label><ul>
<li><a href="college-of-doctoral-studies.php" target="">College of Doctoral Studies</a></li>
<li><a href="colangelo-college-of-business.php" target="">Colangelo College of Business</a></li>
<li><a href="college-of-education.php" target="">College of Education</a></li>
<li><a href="college-of-fine-arts-and-production.php" target="">College of Fine Arts and Production</a></li>
<li><a href="college-of-humanities-and-social-sciences.php" target="">College of Humanities and Social Sciences</a></li>
<li><a href="college-of-nursing-and-health-care-professions.php" target="">College of Nursing and Health Care Professions</a></li>
<li><a href="college-of-science-engineering-and-technology.php" target="">College of Science, Engineering and Technology</a></li>
<li><a href="college-of-theology.php" target="">College of Theology</a></li>
<li><a href="honors-college.php" target="">Honors College</a></li>
</ul>
</div>
<div class="col-sm-5 right">
<ul>
<li><a href="academics/provost-message.php" target="">Provost Message</a></li>
<li><a href="academics/educational-alliances.php" target="">Educational Alliances</a></li>
<li><a href="academics/majors-and-programs.php" target="">Majors &amp; Programs</a></li>
<li><a href="academics/faculty-directory.php" target="">Faculty Directory</a></li>
<li><a href="academics/academic-policies.php" target="">University and Academic Policies</a></li>
<li><a href="academics/operation-impact.php" target="">Operation Impact </a></li>
<li><a href="academics/office-of-assessment.php" target="">Office of Assessment</a></li>
</ul>
</div>
</div>
</ul>
</li>
<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">ADMISSIONS</a><ul class="dropdown-menu third">
<div class="row">
<div class="col-sm-6 left">
<ul>
<li><a href="admissions/college-transfer-center.php" target="">Transfer Center</a></li>
<li><a href="admissions/requirements.php" target="">Admission Requirements</a></li>
</ul>
</div>
<div class="col-sm-6 right">
<ul>
<li><a href="admissions/tuition-and-financing.php" target="">Tuition &amp; Scholarships</a></li>
</ul>
</div>
</div>
</ul>
</li>
<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">FUTURE STUDENTS</a><ul class="dropdown-menu third">
<div class="row">
<div class="col-sm-6 left">
<ul>
<li><a href="future-students/campus-experience.php" target="">Campus</a></li>
<li><a href="future-students/online-degree-programs.php" target="">Online</a></li>
<li><a href="future-students/evening-classes.php" target="">Evening</a></li>
<li><a href="future-students/international-students-.php" target="">International Students</a></li>
<li><a href="future-students/foreign-students-.php" target="">Students with Foreign Credentials</a></li>
</ul>
</div>
<div class="col-sm-6 right">
<ul>
<li><a href="future-students/military-university.php" target="">Military &amp; Veteran</a></li>
<li><a href="future-students/army-rotc-programs.php" target="">ROTC</a></li>
<li><a href="future-students/tribal.php" target="">Tribal</a></li>
<li><a href="future-students/visit-gcu-campus.php" target="">Visit Campus</a></li>
<li><a href="future-students/campus-resources.php" target="">Campus Resources</a></li>
</ul>
</div>
</div>
</ul>
</li>
</ul>
</div>
</div>
</div>
</div>
</div></nav><div id="rmiFormContainer" class="container-fluid">
<div class="container" style="height:0;">
<div class="row" style="height:0;">
<div id="rmiForm" class="col-md-offset-7 col-md-5"><button id="rmiClose" class="btn btn-default" type="button"><i class="fa fa-close"></i></button><div id="rmi-container"></div>
</div>
</div>
</div>
</div><script type="text/javascript">
                $(document).ready(function(){
                $('#rmi-container').load('//www.gcu.edu/prebuilt/includes/hawthornform_js_include.php');
                });
            </script><button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target="#mobileNav" data-canvas="body"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="mobile-brand visible-xs" href="http://www.gcu.edu"><img src="prebuilt/img/logo.png" alt="Grand Canyon University Logo"></a></header><section role="main" class="white"><section id="subHeader" class="
                        college-page-header header-content header-small
                    " style="
            background-image:url('Images/header-1400/gcu-sign-campus.jpg');
        "><div class="container">
<div class="row">
<div id="title-tab" class="angle-heading">
<h1 id="university-and-academic-policies">UNIVERSITY AND ACADEMIC POLICIES</h1>
</div>
</div>
</div></section><div class="container extra-padding Layout_OneColumn">
<div class="
                    row
                ">
<div class="col-md-12"></div>
<div class="col-md-12">
<div class="clearfix Block_Text">
<div class="inside"><p>As a registered student, you are responsible for knowing information pertaining to Grand Canyon University's (GCU) academic policies. If you are not in compliance with policies, requirements or regulations, it may impact your enrollment and ability to graduate from GCU. Browse to find more information about the University Policy Handbook, accreditation, state authorizations and GCU's policies.</p>
<p>Title IX prohibits sexual discrimination in education programs that receive federal funding. GCU is committed to upholding this policy and students' rights under Title IX.</p>
<p><a class="btn btn-default" href="academics/academic-policies/title-ix.php" title="Learn More">Learn More About Title IX</a></p></div>
</div>
</div>
</div>
</div>
<div class="container row-hack Block_HorizontalRule">
<div>
<hr>
</div>
</div>
<div class="container row-hack extra-padding before-tab Block_Tabs">
<h2 id="grand-canyon-university-accreditations">Grand Canyon University Accreditations</h2>
<ul class="nav nav-tabs">
<li class="
                                active
                            "><a data-toggle="tab" href="#tab-IDK4PPFIM2QF0ELORSWQIYUKGZ0OB3E33OGLPVVGBL5LJCXEVHQOYH-1">Regional Accreditation</a></li>
<li><a data-toggle="tab" href="#tab-IDK4PPFIM2QF0ELORSWQIYUKGZ0OB3E33OGLPVVGBL5LJCXEVHQOYH-2">Additional Accreditation</a></li>
<li><a data-toggle="tab" href="#tab-IDK4PPFIM2QF0ELORSWQIYUKGZ0OB3E33OGLPVVGBL5LJCXEVHQOYH-3">Accrediting Bodies</a></li>
</ul>
<div class="tab-content">
<div class="
                                    tab-pane fade in active
                                " id="tab-IDK4PPFIM2QF0ELORSWQIYUKGZ0OB3E33OGLPVVGBL5LJCXEVHQOYH-1">
<div class="Layout_TwoColumn">
<div class="
                    row
                ">
<div class="col-md-12"></div>
<div class="col-md-6">
<div class="clearfix Block_Text">
<div class="inside"><p>As an accredited university, GCU holds regional, college and program-specific accreditations. Accreditations speak to the quality of the university and its academic programs, and can represent acknowledgment by peer institutions.</p>
<p><strong>Regional Accreditation</strong><br />In choosing a university, you should ensure your choice is regionally accredited, as this provides an assurance of quality as well as continual institutional program improvement.</p>
<p>Regional accreditation in higher education originated almost a century ago as an American process conferred by a nongovernmental agency. Today, one of the most respected agencies is the Higher Learning Commission, which is recognized by the Council for Higher Education Accreditation.</p>
<p>The Higher Learning Commission (HLC) and its predecessor have accredited GCU continually since 1968. GCU obtained its most recent 10-year reaccreditation in 2007.</p>
<p>To be accredited, the HLC's examiners and trained peers visit GCU's campus, analyze and accept the university's operations and academic outcomes as meeting HLC's core competencies.</p>
<p></p></div>
</div>
</div>
<div class="col-md-6">
<div class="clearfix Block_Text">
<div class="inside"><p>Among the areas HLC audit reviews are:</p>
<ul>
<li>Educational activities</li>
<li>Admissions and student personnel services</li>
<li>Administration</li>
<li>Student academic achievement</li>
<li>Financial stability</li>
<li>Student resources</li>
<li>Organizational effectiveness</li>
<li>Relationships with outside constituencies</li>
</ul>
<p><img height="102" src="Images/8-semesters/8Sem-Business-500x214.png" style="float: right;" width="275" /></p>
<p></p></div>
</div>
</div>
</div>
</div>
</div>
<div class="
                                    tab-pane fade
                                " id="tab-IDK4PPFIM2QF0ELORSWQIYUKGZ0OB3E33OGLPVVGBL5LJCXEVHQOYH-2">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU has earned accreditations and approvals for our core program offerings from:</p>
<ul>
<li>Accreditation Council Business Schools and Programs</li>
<li>Arizona State Board of Education</li>
<li>Arizona Department of Education</li>
<li>Commission on Collegiate Nursing Education</li>
<li>Commission on Accreditation of Athletic Training Education&nbsp;(caate.net)</li>
</ul>
<p></p>
<p></p></div>
</div>
</div>
<div class="
                                    tab-pane fade
                                " id="tab-IDK4PPFIM2QF0ELORSWQIYUKGZ0OB3E33OGLPVVGBL5LJCXEVHQOYH-3">
<div class="Layout_TwoColumn">
<div class="
                    row
                ">
<div class="col-md-12"></div>
<div class="col-md-6">
<div class="clearfix Block_Text">
<div class="inside"><p><strong>Higher Learning Commission</strong><br />The HLC accredits GCU.</p>
<p>The Higher Learning Commission<br />230 S. LaSalle St., Suite 7-500 <br />Chicago, IL 60604<br />Phone: 312-263-0456<br />Toll-free: 800-621-7440<br />hlcommission.org</p>
<p><strong>Arizona State Private Post-Secondary Education</strong><br />GCU is licensed in Arizona by the Arizona State Board for Private Postsecondary Education.</p>
<p>Arizona State Board for Private Postsecondary Education<br />1400 W. Washington St., Room 260<br />Phoenix, AZ 85007<br />Phone: 602-542-5709<br />azppse.state.az.us</p>
<p><strong>Veterans Administration</strong><br />GCU is approved for the education and training of veterans under the provisions of Title 10 and 38, U.S. Code. We accept Chapter 30, 31, 32, 33, 35, 1606 and 1607 recipients. Veterans are approved for benefits for numerous programs. For more information regarding using your VA benefits at GCU, call us at 855-GCU-LOPE.</p>
<p><strong>Colangelo College of Business<br /></strong>The Accreditation Council for Business Schools and Programs (ACBSP) accredits the following College of Business programs:</p>
<ul>
<li>Bachelor of Science in Accounting</li>
<li>Bachelor of Science in Marketing</li>
<li>Bachelor of Science in Business Administration</li>
<li>Bachelor of Science in Entrepreneurial Studies</li>
<li>Bachelor of Science in Finance and Economics</li>
<li>Bachelor of Science in Sports Management</li>
<li>Master of Business Administration</li>
<li>Executive Master of Business Administration</li>
</ul>
<p>Accreditation Council for Business Schools and Programs <br />7007 College Blvd., Suite 420<br />Overland Park, KS 66211<br />Phone: 913-339-9356</p>
<p>The most recent ACBSP two-year <a href="Documents/Policies/ACBSP-Quality-Assurance-Report-2013.pdf" target="_blank">Quality Assurance Report</a> can be reviewed here.<br /><br />In addition, the Colangelo College of Business uses the business field test administered by Peregrine Academics to assess the business knowledge of our college's undergraduate students. The most recent results from the Peregrine test can be reviewed here.</p>
<p></p>
<p></p></div>
</div>
</div>
<div class="col-md-6">
<div class="clearfix Block_Text">
<div class="inside"><p><strong>College of Education</strong></p>
<p>GCU's College of Education is approved by the Arizona State Board of Education and the Arizona Department of Education to offer institutional recommendations (credentials) for the certification of early childhood, elementary, secondary and special education teachers and administrators.</p>
<p><strong>College of Nursing and Health Care Professions</strong><br />The baccalaureate degree in nursing and master's degree in nursing at GCU are accredited by the Commission on Collegiate Nursing Education (CCNE).</p>
<p>GCU's College of Nursing and Health Care Professions is approved by the Arizona State Board of Nursing.</p>
<p>The nursing program is approved by the New Mexico Board of Nursing.</p>
<p>The Bachelor of Science in Athletic Training program is accredited by the Commission on Accreditation of Athletic Training Education.</p>
<p><strong>College of Humanities and Social Sciences</strong><br />Our Master of Science in Addiction Counseling program has been approved for accreditation through the National Addiction Studies Accreditation Commission (NASAC). NASAC accreditation is a new accreditation body resulting from a combined effort of The International Coalition for Addiction Studies Education (INCASE) and the Association for Addiction Professionals (NAADAC) to create a single higher education addiction studies accreditation.<br /><br /><strong>College of Science, Engineering and Technology</strong> <br />As an established regionally accredited university, our next step is to seek and obtain program-specific accreditation for our engineering and technology programs. Program-specific accreditation is voluntary and will provide degrees earned from the College of Science, Engineering and Technology with added value for our students.</p>
<p>Yet, before GCU can receive accreditation for our engineering and technology programs, graduates must have earned a degree from the program. We will not be able to seek accreditation for several years because our programs are so new; however, accreditation for engineering and technology programs will be retroactive when these programs do become endorsed.</p>
<p>GCU remains committed to aligning our engineering and technology programs with accreditation standards. We strive to exceed these high standards to ensure our STEM students receive a quality education during their time here. <br /><br />We acknowledge accrediting bodies seek the following in engineering and technology programs pursuing endorsement:</p>
<ul>
<li><span style="font-size: 9pt; line-height: 17pt;">The institution is committed to improving their educational experience.</span></li>
<li><span style="font-size: 9pt; line-height: 17pt;">The program is committed to using best practices and innovation in education.&nbsp;</span></li>
<li><span style="font-size: 9pt; line-height: 17pt;">The program is guided by its industry, government and academic constituents through formal feedback.</span></li>
<li><span style="font-size: 9pt; line-height: 17pt;">The program considers the students' perspective as part of its continuous quality improvement process.</span></li>
</ul>
<p>Accrediting bodies look for institutions to continually improve upon their programs, and GCU constantly evaluates our programs. We seek feedback from industry on a regular basis to ensure that our curricula are as relevant as possible in today's marketplace.</p>
<p><strong>Intercollegiate Athletics</strong><br />Intercollegiate athletics function under the guidelines of the National Collegiate Athletic Association (NCAA) Division I regarding eligibility to participate in intercollegiate sports. In the 2013-14 season, GCU began the process of reclassifying from Division II to Division I as a member of the Western Athletic Conference.</p>
<p>National Collegiate Athletic Association (NCAA) Division I<br />700 W. Washington St.<br />P.O. Box 6222<br />Indianapolis, IN 46202</p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div><section class="Section_Tabbed"><div class="
                    container-fluid extra-padding dk-purple">
<div class="container">
<div class="row">
<div class="angle-heading">
<h2>Policy Handbooks</h2>
</div>
</div>
</div>
<div class="container margin-center Layout_TwoColumn">
<div class="
                    row white rounded extra-padding
                ">
<div class="col-md-12"></div>
<div class="col-md-6">
<div class="clearfix Block_Text">
<h3 id="university-policy-handbook">University Policy Handbook</h3>
<div class="inside"><p>The University Policy Handbook provides information including graduation requirements, degree programs offered, admission requirements and general academic regulations.</p>
<p><a class="btn btn-default" href="Documents/University-Policy-Handbook.pdf" title="University Policy Handbook" target="_blank">View Policy Handbook</a></p></div>
</div>
<div class="Block_Accordion">
<div class="panel-group simple" role="tablist" aria-multiselectable="true" id="accordion-IDN2D5MX3VABGLEKKO1TH2HJPY1NBZ0LUNLNPGEWPPRVBXSSSYTKLH">
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDF0EBO2GMRZVIMDPNLSUYBXTEP1UKWSIKSRJ4OO4BV5DESPZXF4E">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDN2D5MX3VABGLEKKO1TH2HJPY1NBZ0LUNLNPGEWPPRVBXSSSYTKLH" aria-expanded="false" href="#collapse-IDF0EBO2GMRZVIMDPNLSUYBXTEP1UKWSIKSRJ4OO4BV5DESPZXF4E" aria-controls="#collapse-IDF0EBO2GMRZVIMDPNLSUYBXTEP1UKWSIKSRJ4OO4BV5DESPZXF4E">View Archived Policy Handbooks</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDF0EBO2GMRZVIMDPNLSUYBXTEP1UKWSIKSRJ4OO4BV5DESPZXF4E" id="collapse-IDF0EBO2GMRZVIMDPNLSUYBXTEP1UKWSIKSRJ4OO4BV5DESPZXF4E">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>From the links below you can download a policy handbook from a previous semester.</p>
<table class="table table-striped table-hover" style="width: 100%;">
<tbody>
<tr>
<td style="text-align: left;"><a href="Documents/University-Handbooks/University-Policy-Handbook.pdf" title="2015 Summer Policy Handbook" target="_blank">2015 Summer Policy Handbook</a><a href="Documents/University-Handbooks/2014-Fall-University-Policy-Handbook.pdf" title="2014 Fall Policy Handbook" target="_blank"> </a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/2014-Fall-University-Policy-Handbook.pdf" title="2014 Fall Policy Handbook" target="_blank">2014 Fall Policy Handbook</a><a href="Documents/University-Handbooks/University-Policy-Handbook-2014-Summer-v9.pdf" title="2014 Summer Policy Handbook" target="_blank"> </a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2014-Summer-v9.pdf" title="2014 Summer Policy Handbook" target="_blank">2014 Summer Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2014-Spring-v12.pdf" title="2014 Spring Policy Handbook" target="_blank">2014 Spring Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2013-Fall.pdf" title="University Handbook Fall 2013" target="_blank">2013 Fall Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/Student-Handbook-2013-Spring.pdf" title="2013 Spring Student Policy Handbook" target="_blank"></a><a href="Documents/University-Handbooks/University-Policy-Handbook-2013-Summer.pdf" title="2013 Summer Policy Handbook" target="_blank">2013 Summer Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/Student-Handbook-2013-Spring.pdf" title="2013 Spring Student Policy Handbook" target="_blank">2013 Spring Traditional Student Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2013-Spring.pdf" title="2013 Spring Policy Handbook" target="_blank">2013 Spring Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2012-Fall.pdf" title="2012 Fall Policy Handbook" target="_blank">2012 Fall Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/upload/Admissions/University-Policy-Handbook-2011-2012-Spring.pdf" target="_blank"></a><a href="Documents/University-Handbooks/University-Policy-Handbook-2012-Summer.pdf" title="2012 Summer Policy Handbook" target="_blank">2012 Summer Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2011-2012-Spring.pdf" title="11-12 Spring Handbook" target="_blank">2012 Spring Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2011-2012-Fall.pdf" title="11-12 Fall Handbook" target="_blank">2011 Fall Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2011-2012-Summer.pdf" title="2011 Summer Policy Handbook" target="_blank">2011 Summer Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2011-2012-Spring.pdf" title="2011 Spring Policy Handbook" target="_blank">2011 Spring Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2010-Summer.pdf" title="2010 Summer Policy Handbook" target="_blank">2010 Summer Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2010-Spring.pdf" title="2010 Spring Policy Handbook" target="_blank">2010 Spring Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2010-Fall.pdf" title="2010 Fall Policy Handbook" target="_blank">2010 Fall Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Policy-Handbook-2009-Fall.pdf" title="2009 Fall Policy Handbook" target="_blank">2009 Fall&nbsp;Policy Handbook</a></td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p>From the links below you can download a student policy handbook from a previous semester.</p>
<table class="table table-striped table-hover" style="width: 100%;">
<tbody>
<tr>
<td><a href="Documents/Student-Resources/2013-2014-traditional-student-handbook.pdf" title="Student Handbook" target="_blank">2013 Student Policy Handbook</a></td>
</tr>
<tr>
<td><a href="Documents/University-Handbooks/University-Handbook-2012.pdf" title="Student Handbook" target="_blank">2012 Student Policy Handbook</a>&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp; <a href="Documents/University-Handbooks/University-Handbook-2012.pdf" title="Student Handbook" target="_blank">&nbsp; </a></td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="clearfix Block_Text">
<h3 id="academic-catalog">Academic Catalog</h3>
<div class="inside"><p>Get information about our degree programs, foundational and degree-specific coursework.</p>
<p><a class="btn btn-default" href="Documents/Academic-Catalog/2015-2016-Fall-Academic-Catalog.pdf" title="Academic Catalog" target="_blank">View Academic Catalog</a></p></div>
</div>
<div class="Block_Accordion">
<div class="panel-group simple" role="tablist" aria-multiselectable="true" id="accordion-IDTL1P3VD5SO2RDP1N541ZKYA0NBPI2QOMG41J3HENSPQH5EO2G5UD">
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDPNSGIFF5AUQRHTLIYVDOUPJT2M3PE21F3SSF0ZMGZCREHL4PR2DC">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDTL1P3VD5SO2RDP1N541ZKYA0NBPI2QOMG41J3HENSPQH5EO2G5UD" aria-expanded="false" href="#collapse-IDPNSGIFF5AUQRHTLIYVDOUPJT2M3PE21F3SSF0ZMGZCREHL4PR2DC" aria-controls="#collapse-IDPNSGIFF5AUQRHTLIYVDOUPJT2M3PE21F3SSF0ZMGZCREHL4PR2DC">View Academic Calendar</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDPNSGIFF5AUQRHTLIYVDOUPJT2M3PE21F3SSF0ZMGZCREHL4PR2DC" id="collapse-IDPNSGIFF5AUQRHTLIYVDOUPJT2M3PE21F3SSF0ZMGZCREHL4PR2DC">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>Our Academic Calendar provides important dates that you need to know as a GCU student.</p>
<table class="table" style="width: 100%;">
<tbody>
<tr>
<td><strong>2015 - 16 Traditional Campus Calendar</strong></td>
<td></td>
</tr>
<tr>
<td><em>Summer Semester 2015</em><br />May 4 - Aug. 16, 2015</td>
<td>15-week classes</td>
</tr>
<tr>
<td>Move-In Date</td>
<td>May 1</td>
</tr>
<tr>
<td>Instruction Begins</td>
<td>May 4</td>
</tr>
<tr>
<td>&nbsp; &nbsp; &nbsp;Session A</td>
<td>May 11 - June 28</td>
</tr>
<tr>
<td>&nbsp; &nbsp; &nbsp;Session B</td>
<td>June 29 - Aug. 16</td>
</tr>
<tr>
<td>Instruction Ends</td>
<td>Aug. 14</td>
</tr>
<tr>
<td>Fall Residents Move to Fall Housing</td>
<td>July 31</td>
</tr>
<tr>
<td>Move out Date for Non-Fall Residents</td>
<td>Aug. 14</td>
</tr>
<tr>
<td>End of Term</td>
<td>Aug. 16</td>
</tr>
<tr>
<td>Final Course Grades Due</td>
<td>Aug. 23</td>
</tr>
<tr>
<td>Fall Break</td>
<td>Aug. 17 - 23</td>
</tr>
<tr>
<td><em>Fall Semester 2015</em><br />Aug. 24 - Dec. 13</td>
<td>15-week classes</td>
</tr>
<tr>
<td>Move-In Date</td>
<td>Aug. 17 - 21<br />Welcome Week</td>
</tr>
<tr>
<td>Instruction Begins</td>
<td>Aug. 24</td>
</tr>
<tr>
<td>Thanksgiving Break</td>
<td>Nov. 23 - 29</td>
</tr>
<tr>
<td>Instruction Ends</td>
<td>Dec. 11</td>
</tr>
<tr>
<td>Move out Date for Fall-only Students</td>
<td>Dec. 12 at 10 a.m.</td>
</tr>
<tr>
<td>Living Area Closed for Christmas</td>
<td>Dec. 12 at 10 a.m</td>
</tr>
<tr>
<td>End of Term</td>
<td>Dec. 13</td>
</tr>
<tr>
<td>Final Course Grades Due</td>
<td>Dec. 20</td>
</tr>
<tr>
<td>Christmas Break</td>
<td>Dec. 14, 2015 - Jan. 3, 2016&nbsp;</td>
</tr>
<tr>
<td>2015 Online Christmas Break Schedule</td>
<td></td>
</tr>
<tr>
<td>Undergraduates</td>
<td>Dec. 21, 2015 - Jan. 3, 2016</td>
</tr>
<tr>
<td>Graduates</td>
<td>Dec. 24, 2015 - Jan. 6, 2016</td>
</tr>
<tr>
<td>Fall Commencement Ceremonies*</td>
<td></td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
<tr>
<td>Nontraditional Campus Commencements</td>
<td></td>
</tr>
<tr>
<td>College of Humanities and Social Sciences</td>
<td></td>
</tr>
<tr>
<td>College of Doctoral Studies</td>
<td></td>
</tr>
<tr>
<td>College of Education</td>
<td></td>
</tr>
<tr>
<td>College of Theology</td>
<td></td>
</tr>
<tr>
<td>College of Nursing and Health Care Professions</td>
<td></td>
</tr>
<tr>
<td>Colangelo College of Business</td>
<td></td>
</tr>
<tr>
<td>College of Fine Arts and Production</td>
<td></td>
</tr>
<tr>
<td></td>
<td></td>
</tr>
<tr>
<td>Traditional Campus Commencements</td>
<td>Dec. 11, 2015</td>
</tr>
<tr>
<td>All Colleges</td>
<td></td>
</tr>
<tr>
<td>* Dates and times of commencement ceremonies may be subject to change.</td>
</tr>
<tr>
<td><em>Spring Semester 2016</em><br />Jan. 1 - April 24</td>
<td>15-Week Classes</td>
</tr>
<tr>
<td>Move-In Date</td>
<td>Jan. 2-3</td>
</tr>
<tr>
<td>Instruction begins</td>
<td>Jan. 4</td>
</tr>
<tr>
<td>Spring Break</td>
<td>March 21- 27</td>
</tr>
<tr>
<td>Instruction ends</td>
<td>April 22</td>
</tr>
<tr>
<td>Move out Date</td>
<td>April 23 at noon</td>
</tr>
<tr>
<td>End of Term</td>
<td>April 24</td>
</tr>
<tr>
<td>Final Course Grade Due</td>
<td>May 1</td>
</tr>
<tr>
<td>Summer Beak</td>
<td>April 25 - May 1</td>
</tr>
<tr>
<td>Spring Commencement Ceremonies*</td>
</tr>
<tr>
<td>Traditional Campus Commencement</td>
<td>April 22, 2016</td>
</tr>
<tr>
<td>Spring Nontraditional Campus Commencements</td>
<td>April 23 &amp; 25, 2016</td>
</tr>
<tr>
<td>*Dates and times of commencement ceremonies may be subject to change.</td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="Block_Accordion">
<div class="panel-group simple" role="tablist" aria-multiselectable="true" id="accordion-IDTL0JLZC3XKF5LR4S5MWAGVKKGFXMFUL44FIYHSCPV2VKUIJVLJ2B">
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDVO3AGPOZFNIZBGR1ABQFJWY4NPFAJG32D5ATZ4NRZXXJZHNV5XRC">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDTL0JLZC3XKF5LR4S5MWAGVKKGFXMFUL44FIYHSCPV2VKUIJVLJ2B" aria-expanded="false" href="#collapse-IDVO3AGPOZFNIZBGR1ABQFJWY4NPFAJG32D5ATZ4NRZXXJZHNV5XRC" aria-controls="#collapse-IDVO3AGPOZFNIZBGR1ABQFJWY4NPFAJG32D5ATZ4NRZXXJZHNV5XRC">View Archived Academic Catalogs</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDVO3AGPOZFNIZBGR1ABQFJWY4NPFAJG32D5ATZ4NRZXXJZHNV5XRC" id="collapse-IDVO3AGPOZFNIZBGR1ABQFJWY4NPFAJG32D5ATZ4NRZXXJZHNV5XRC">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><table align="left" border="0" class="table table-striped table-hover" style="width: 100%;">
<tbody>
<tr>
<td style="text-align: left;"><a href="Documents/Academic-Catalog/2015-Summer-Academic-Catalog.pdf" title="2015 Summer Academic Catalog" target="_blank">2015 Summer Academic Catalog</a><a href="Documents/Academic-Catalog/2014-Fall-Academic-Catalog.pdf" title="2014-2015 Fall Academic Catalog" target="_blank"> </a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2014-Fall-Academic-Catalog.pdf" title="2014-2015 Fall Academic Catalog" target="_blank">2014-2015 Fall Academic Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/Academic-Catalog-2014-Summer-v.1.4.pdf" title="2014-2015 Summer Academic Catalog" target="_blank">2014-2015 Summer Academic Catalog</a><a href="Documents/Academic-Catalog/Academic-Catalog-2014-Spring.pdf" title="2013-2014 Spring Academic Catalog" target="_blank"> </a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/Academic-Catalog-2014-Spring.pdf" title="2013-2014 Spring Academic Catalog" target="_blank">2013-2014 Spring Academic Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2013-2014-Academic-Catalog-Fall.pdf" title="2013-2014 Fall Academic Catalog" target="_blank">2013-2014 Fall Academic Catalog</a><a href="Documents/Academic-Catalog/2013-2014-Summer-Academic-Catalog.pdf" title="2013-2014 Summer Academic Catalog"> </a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2013-2014-Summer-Academic-Catalog.pdf" title="2013-2014 Summer Academic Catalog" target="_blank">2013-2014 Summer Academic Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2012-2013-Spring-Academic-Catalog(0).pdf" title="Spring 2013 Academic Catalog" target="_blank">2012-2013 Spring Academic Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2012-2013-Fall-Academic-Catalog-2.0.pdf" title="2012-2013 Fall Academic Catalog" target="_blank">2012-2013&nbsp;Fall Academic Catalog&nbsp; </a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2012-2013-Summer-Academic_Catalog_v1.1_5_23_12.pdf" title="Summer Academic Catalog" target="_blank">2012-2013 Summer Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2012-2013-Spring-Academic-Catalog.pdf" title="2012-2013 Spring Catalog" target="_blank">2012-2013 Spring Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2011-2012-Fall-Academic-Catalog.pdf" title="2011-2012 Fall Catalog" target="_blank">2011-2012 Fall Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2011-2012-Spring-Academic-Catalog-v3.1.pdf" title="Spring 2012 Catalog" target="_blank">2011-2012 Spring Catalog&nbsp; </a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2011-2012-Summer-Academic-Catalog-v1.0.pdf" title="Summer 2011 Catalog" target="_blank">2011-2012 Summer Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2010-2011-Spring-Academic-Catalog-v2.0.pdf" title="2011 Spring Catalog" target="_blank">2010-2011 Spring Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2010-2011-Fall-Academic-Catalog-v1.pdf" title="Fall 2010 Catalog" target="_blank">2010-2011 Fall Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2009-2010-Spring-Academic-Catalog.pdf" title="2009 - 2010 Spring Catalog" target="_blank">2009-2010 Spring Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2009-2010-Fall-Academic-Catalog.pdf" title="2009 - 2010 Fall Catalog" target="_blank">2009-2010 Fall Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2009-2010-Summer-Academic-Catalog-.pdf" title="2009 -2010 Summer Catalog" target="_blank">2009-2010 Summer Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2008-2009-Spring-Academic-Catalog.pdf" title="2008 - 2009 Spring Catalog" target="_blank">2008-2009 Spring Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2008-2009-Fall-Academic-Catalogl.pdf" title="2008 - 2009 Fall Catalog" target="_blank">2008-2009 Fall Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2008-2009-Summer-Academic-Catalog.pdf" title="2008 - 2009 Summer Catalog" target="_blank">2008-2009 Summer Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2007-2008-Spring-Academic-Catalog.pdf" title="2007 - 2008 Spring Catalog" target="_blank">2007-2008 Spring Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2007-2008-Fall-Academic-Catalog.pdf" title="2007 - 2008 Fall Catalog" target="_blank">2007-2008 Fall Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2007-2008-Summer-Academic-Catalog.pdf" title="2007 - 2008 Summer Catalog" target="_blank">2007-2008 Summer Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2006-2007-Spring-Academic-Catalog.pdf" title="2006 - 2007 Spring Catalog" target="_blank">2006- 2007 Spring Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2006-2007-Fall-Academic-Catalog.pdf" title="2006 - 2007 Fall Catalog" target="_blank">2006-2007 Fall Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2006-2007-Summer-Academic-Catalog.pdf" title="2006 - 2007 Summer Catalog" target="_blank">2006-2007 Summer Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2005-2006-Spring-Academic-Catalog.pdf" title="2005 - 2006 Spring Catalog" target="_blank">2005-2006 Spring Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2005-2006-Fall-Academic-Catalog.pdf" title="2005 - 2006 Fall Catalog" target="_blank">2005-2006&nbsp;Fall Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2003-2004-Academic-Catalog.pdf" title="2003 - 2004 Catalog" target="_blank">2003-2004 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/2001-2003-Academic-Catalog.pdf" title="2001 - 2003 Catalog" target="_blank">2001-2003 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1999-2000-Academic-Catalog.pdf" title="1999 - 2000 Catalog" target="_blank">1999-2000 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1997-1999-Academic-Catalog.pdf" title="1997 - 1999 Catalog" target="_blank">1997-1999 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1996-1997-Academic-Catalog.pdf" title="1996 - 1997 Catalog" target="_blank">1996-1997 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1993-1995-Academic-Catalog.pdf" title="1993 - 1995 Catalog" target="_blank">1993-1995 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1992-1993-Academic-Catalog.pdf" title="1992 - 1993 Catalog" target="_blank">1992-1993 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1991-1993-Academic-Catalog.pdf" title="1991 - 1993 Catalog" target="_blank">1991-1993 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1989-1991-Academic-Catalog.pdf" title="1989 - 1991 Catalog" target="_blank">1989-1991 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1986-1988-Academic-Catalog.pdf" title="1986 - 1988 Catalog" target="_blank">1986-1988 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1984-1986-Academic-Catalog.pdf" title="1984 - 1986" target="_blank">1984-1986 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1982-1984-Academic-Catalog.pdf" title="1982 - 1984 Catalog" target="_blank">1982-1984 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1980-1982-Academic-Catalog.pdf" title="1980 - 1982 Catalog" target="_blank">1980-1982 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1978-1980-Academic-Catalog.pdf" title="1978 - 1980 Catalog" target="_blank">1978-1980 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1976-1978-Academic-Catalog.pdf" title="1976 - 1978 Catalog" target="_blank">1976-1978 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1974-1975-Academic-Catalog.pdf" title="1974 - 1975 Catalog" target="_blank">1974-1975 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1973-1974-Academic-Catalog.pdf" title="1973 - 1974 Catalog" target="_blank">1973-1974 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1971-1973-Academic-Catalog.pdf" title="1971 - 1973 Catalog" target="_blank">1971-1973 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1969-1971-Academic-Catalog.pdf" title="1969 - 1971 Catalog" target="_blank">1969-1971 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1967-1968-Academic-Catalog.pdf" title="1967 - 1968 Catalog" target="_blank">1967-1968 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1965-1967-Academic-Catalog.pdf" title="1965 - 1967 Catalog" target="_blank">1965-1967 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1964-1965-Academic-Catalog.pdf" title="1964 - 1965 Catalog" target="_blank">1964-1965 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1963-1964-Academic-Catalog.pdf" title="1963 - 1964 Catalog" target="_blank">1963-1964 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1962-1963-Academic-Catalog.pdf" title="1962 - 1963 Catalog" target="_blank">1962-1963 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1958-1960-Academic-Catalog.pdf" title="1958 - 1960 Catalog" target="_blank">1958-1960 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1957-1958-Academic-Catalog.pdf" title="1957 - 1958 Catalog" target="_blank">1957-1958 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1956-1957-Academic-Catalog.pdf" title="1956 - 1957 Catalog" target="_blank">1956-1957 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1955-1956-Academic-Catalog.pdf" title="1955 - 1956 Catalog" target="_blank">1955-1956 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1954-1955-Academic-Catalog.pdf" title="1954 - 1955 Catalog" target="_blank">1954-1955 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1953-1954-Academic-Catalog.pdf" title="1953 - 1954 Catalog" target="_blank">1953-1954 Catalog</a></td>
</tr>
<tr>
<td><a href="Documents/Academic-Catalog/1949-1954-Academic-Catalog.pdf" title="1949 - 1954 Catalog" target="_blank">1949-1954 Catalog</a></td>
</tr>
</tbody>
</table></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div></section><div class="container extra-padding Layout_OneColumn">
<div class="
                    row
                ">
<div class="col-md-12">
<div class="page-header" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:jslib="http://www.url.com/jslib">
<h2 id="state-authorizations">State Authorizations</h2>
</div>
</div>
<div class="col-md-12">
<div class="clearfix Block_Text">
<div class="inside"><p>If you are considering enrolling as an online student at GCU, we suggest you review whether your state authorizes GCU to offer your preferred educational program to its residents.</p></div>
</div>
<div class="Layout_ThreeColumn">
<div class="
                    row
                ">
<div class="col-md-12"></div>
<div class="col-md-4">
<div class="Block_Accordion">
<div class="panel-group simple" role="tablist" aria-multiselectable="true" id="accordion-ID2OQ5A4NKL5IWBTO53DNMCPF4EFS24ETCBL2SILG0E4WV3RAA4IYE">
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-ID5OQQLV43OTXWEYGZLNA21NCTSIVPPPGYSXLFOOFKP3ZOYVTHGRZL">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-ID2OQ5A4NKL5IWBTO53DNMCPF4EFS24ETCBL2SILG0E4WV3RAA4IYE" aria-expanded="false" href="#collapse-ID5OQQLV43OTXWEYGZLNA21NCTSIVPPPGYSXLFOOFKP3ZOYVTHGRZL" aria-controls="#collapse-ID5OQQLV43OTXWEYGZLNA21NCTSIVPPPGYSXLFOOFKP3ZOYVTHGRZL">Alabama</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-ID5OQQLV43OTXWEYGZLNA21NCTSIVPPPGYSXLFOOFKP3ZOYVTHGRZL" id="collapse-ID5OQQLV43OTXWEYGZLNA21NCTSIVPPPGYSXLFOOFKP3ZOYVTHGRZL">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU has been granted authorization by the State of Alabama under Ala. Code 16-5-10 (14) (1975) to offer the academic degree programs described herein. Since credentials earned through the College of Education do not automatically qualify for teacher certification, endorsement and/or salary benefits within the State of Alabama, you are advised to contact the Alabama State Superintendent of Education (alsde.edu/sites/boe/Pages/home.aspx).</p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDHYBHLVANHFQYMPRQNNKB4IFYYN0VFZAERPE3TWBKQHAHOE5ZBQAF">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-ID2OQ5A4NKL5IWBTO53DNMCPF4EFS24ETCBL2SILG0E4WV3RAA4IYE" aria-expanded="false" href="#collapse-IDHYBHLVANHFQYMPRQNNKB4IFYYN0VFZAERPE3TWBKQHAHOE5ZBQAF" aria-controls="#collapse-IDHYBHLVANHFQYMPRQNNKB4IFYYN0VFZAERPE3TWBKQHAHOE5ZBQAF">Arkansas</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDHYBHLVANHFQYMPRQNNKB4IFYYN0VFZAERPE3TWBKQHAHOE5ZBQAF" id="collapse-IDHYBHLVANHFQYMPRQNNKB4IFYYN0VFZAERPE3TWBKQHAHOE5ZBQAF">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>The Arkansas Higher Education Coordinating Board (adhe.edu) certification does not constitute an endorsement of any institution or program. Such certification merely indicates that certain criteria have been met as required under the rules and regulations implementing institutional and program certification as defined in Arkansas Code &sect;6-61-301.</p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-ID0PY23IRWLKBCGLXVS5WIJWG4EIFLJ1I10C3CSFMXTBM5ISBD2ZBN">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-ID2OQ5A4NKL5IWBTO53DNMCPF4EFS24ETCBL2SILG0E4WV3RAA4IYE" aria-expanded="false" href="#collapse-ID0PY23IRWLKBCGLXVS5WIJWG4EIFLJ1I10C3CSFMXTBM5ISBD2ZBN" aria-controls="#collapse-ID0PY23IRWLKBCGLXVS5WIJWG4EIFLJ1I10C3CSFMXTBM5ISBD2ZBN">Hawaii</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-ID0PY23IRWLKBCGLXVS5WIJWG4EIFLJ1I10C3CSFMXTBM5ISBD2ZBN" id="collapse-ID0PY23IRWLKBCGLXVS5WIJWG4EIFLJ1I10C3CSFMXTBM5ISBD2ZBN">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>Any Hawaii residents who have exhausted the institution's complaint process can file a complaint with the Hawaii Post-Secondary Education Authorization Program (cca.hawaii.gov/hpeap).</p></div>
</div>
<div class="clearfix Block_Text">
<div class="inside"><p>The Arkansas Higher Education Coordinating Board (adhe.edu) certification does not constitute an endorsement of any institution or program. Such certification merely indicates that certain criteria have been met as required under the rules and regulations implementing institutional and program certification as defined in Arkansas Code &sect;6-61-301.</p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDSM4TFU2FTGP4EZYCKCU20G0GYM5FM534C3NPCGPEGB53WX0A4LGE">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-ID2OQ5A4NKL5IWBTO53DNMCPF4EFS24ETCBL2SILG0E4WV3RAA4IYE" aria-expanded="false" href="#collapse-IDSM4TFU2FTGP4EZYCKCU20G0GYM5FM534C3NPCGPEGB53WX0A4LGE" aria-controls="#collapse-IDSM4TFU2FTGP4EZYCKCU20G0GYM5FM534C3NPCGPEGB53WX0A4LGE">Indiana</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDSM4TFU2FTGP4EZYCKCU20G0GYM5FM534C3NPCGPEGB53WX0A4LGE" id="collapse-IDSM4TFU2FTGP4EZYCKCU20G0GYM5FM534C3NPCGPEGB53WX0A4LGE">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>This institution is authorized by The Indiana Board for Proprietary Education (in.gov/bpe).</p>
<p>The Indiana Board for Proprietary Education<br />101 W. Ohio St., Suite 670<br />Indianapolis, IN 46204</p></div>
</div>
<div class="clearfix Block_Text">
<div class="inside"><p>This institution is authorized by The Indiana Board for Proprietary Education (in.gov/bpe).</p>
<p>The Indiana Board for Proprietary Education<br />101 W. Ohio St., Suite 670<br />Indianapolis, IN 46204</p>
<p></p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDG3FJC1GB5IMLDJ1TUWQZCIUOVJKJVRSYWMAQQYCH1XY1JTFTMNLP">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-ID2OQ5A4NKL5IWBTO53DNMCPF4EFS24ETCBL2SILG0E4WV3RAA4IYE" aria-expanded="false" href="#collapse-IDG3FJC1GB5IMLDJ1TUWQZCIUOVJKJVRSYWMAQQYCH1XY1JTFTMNLP" aria-controls="#collapse-IDG3FJC1GB5IMLDJ1TUWQZCIUOVJKJVRSYWMAQQYCH1XY1JTFTMNLP">Kansas</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDG3FJC1GB5IMLDJ1TUWQZCIUOVJKJVRSYWMAQQYCH1XY1JTFTMNLP" id="collapse-IDG3FJC1GB5IMLDJ1TUWQZCIUOVJKJVRSYWMAQQYCH1XY1JTFTMNLP">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU has been approved to operate by the Kansas Board of Regents (kansasregents.org).</p></div>
</div>
<div class="clearfix Block_Text">
<div class="inside"><p>GCU has been approved to operate by the Kansas Board of Regents, kansasregents.org</p>
<p></p>
<p></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="Block_Accordion">
<div class="panel-group simple" role="tablist" aria-multiselectable="true" id="accordion-IDYJC02GQ3YATDIYQ1G3IZX0INZEMKMDKAUA5FYXCYXVJDBIZPAJ2M">
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDVL4YNVKOVE2LEUGZPD4PHOBFQFJL1FMCRU0TMKKHM02PXRYNOZQP">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDYJC02GQ3YATDIYQ1G3IZX0INZEMKMDKAUA5FYXCYXVJDBIZPAJ2M" aria-expanded="false" href="#collapse-IDVL4YNVKOVE2LEUGZPD4PHOBFQFJL1FMCRU0TMKKHM02PXRYNOZQP" aria-controls="#collapse-IDVL4YNVKOVE2LEUGZPD4PHOBFQFJL1FMCRU0TMKKHM02PXRYNOZQP">Kentucky</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDVL4YNVKOVE2LEUGZPD4PHOBFQFJL1FMCRU0TMKKHM02PXRYNOZQP" id="collapse-IDVL4YNVKOVE2LEUGZPD4PHOBFQFJL1FMCRU0TMKKHM02PXRYNOZQP">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>Please be advised that our educator preparation programs are NOT accredited in Kentucky by the Education Professional Standards Board and are NOT recognized for initial, additional or renewal of certification or salary enhancement (rank change) for K-12 educators in Kentucky. For more information, visit the Education Professional Standards Board's Website (epsb.ky.gov/certification/outofstate.asp).</p>
<p>To view a listing of approved programs, check the Kentucky Council on Postsecondary Education's website (dataportal.cpe.ky.gov/acadprog.aspx).</p>
<p>For more information, contact:<br />Council on Postsecondary Education<br />1024 Capital Center Drive, Suite 320<br />Frankfort, Kentucky 40601</p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDQ0HOH40BYCNZLM13YCH5DGDBH1XXHIVMBOD1WPH1CL44MX1CMVL">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDYJC02GQ3YATDIYQ1G3IZX0INZEMKMDKAUA5FYXCYXVJDBIZPAJ2M" aria-expanded="false" href="#collapse-IDQ0HOH40BYCNZLM13YCH5DGDBH1XXHIVMBOD1WPH1CL44MX1CMVL" aria-controls="#collapse-IDQ0HOH40BYCNZLM13YCH5DGDBH1XXHIVMBOD1WPH1CL44MX1CMVL">Maryland</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDQ0HOH40BYCNZLM13YCH5DGDBH1XXHIVMBOD1WPH1CL44MX1CMVL" id="collapse-IDQ0HOH40BYCNZLM13YCH5DGDBH1XXHIVMBOD1WPH1CL44MX1CMVL">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>The following programs have been authorized to operate in Maryland by the Maryland Higher Education Commission:</p>
<ul>
<li>Bachelor of Science in Counseling with an Emphasis in Addiction, Chemical Dependency and Substance Abuse</li>
<li>Master of Arts in Teaching</li>
<li>Master of Education in Curriculum and Instruction: Reading</li>
<li>Master of Education in Curriculum and Instruction: Technology</li>
<li>Master of Education in Educational Administration</li>
<li>Master of Education in Educational Leadership</li>
<li>Master of Education in Teaching English to Speakers of Other Languages (TESOL)</li>
<li>Master of Science in Addiction Counseling</li>
<li>Master of Science in Professional Counseling</li>
<li>Master of Science in Nursing with an Emphasis in Public Health</li>
<li>Master of Science in Nursing with an Emphasis in Nursing Education</li>
<li>Master of Science in Nursing with an Emphasis in Nursing Leadership in Health Care Systems</li>
<li>Master of Education in Special Education</li>
<li>Post-Master of Science in Nursing</li>
</ul>
<p>The Bachelor of Science in Early Childhood Education, Bachelor of Science in Elementary and Special Education, Bachelor of Science in Secondary Education, Master of Education in Early Childhood Education, Master of Education in Elementary Education and Master of Education in Secondary Education programs have not been recommended for implementation by the Maryland Higher Education Commission based upon on a possible shortage of student teaching placement opportunities.</p>
<p>All other programs are pending registration by the Maryland Higher Education Commission.</p>
<p>Maryland Higher Education Commission <br />6 North Liberty St., 10th Floor<br />Baltimore, MD 21201<br />Phone: 410-767-3303<br />(mhec.state.md.us)</p>
<p></p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDE5JL04ADQDLYO4LAR3VU3ODXSLPKEGTMGAINDLVFV1FJM4A32NH">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDYJC02GQ3YATDIYQ1G3IZX0INZEMKMDKAUA5FYXCYXVJDBIZPAJ2M" aria-expanded="false" href="#collapse-IDE5JL04ADQDLYO4LAR3VU3ODXSLPKEGTMGAINDLVFV1FJM4A32NH" aria-controls="#collapse-IDE5JL04ADQDLYO4LAR3VU3ODXSLPKEGTMGAINDLVFV1FJM4A32NH">Minnesota</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDE5JL04ADQDLYO4LAR3VU3ODXSLPKEGTMGAINDLVFV1FJM4A32NH" id="collapse-IDE5JL04ADQDLYO4LAR3VU3ODXSLPKEGTMGAINDLVFV1FJM4A32NH">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU is registered as a private institution with the Minnesota Office of Higher Education (ohe.state.mn.us) pursuant to sections 136A.61 to 136A.71. Registration is not an endorsement of the institution. Credits earned at the institution may not transfer to all other institutions.</p>
<p></p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-ID0GQC1YUNMXYXCWF4LLUVXAED5NYJRTXZDYCIRKBENTOXOJ1JW5RI">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDYJC02GQ3YATDIYQ1G3IZX0INZEMKMDKAUA5FYXCYXVJDBIZPAJ2M" aria-expanded="false" href="#collapse-ID0GQC1YUNMXYXCWF4LLUVXAED5NYJRTXZDYCIRKBENTOXOJ1JW5RI" aria-controls="#collapse-ID0GQC1YUNMXYXCWF4LLUVXAED5NYJRTXZDYCIRKBENTOXOJ1JW5RI">Missouri</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-ID0GQC1YUNMXYXCWF4LLUVXAED5NYJRTXZDYCIRKBENTOXOJ1JW5RI" id="collapse-ID0GQC1YUNMXYXCWF4LLUVXAED5NYJRTXZDYCIRKBENTOXOJ1JW5RI">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU has been approved to operate by the Missouri Coordinating Board for Higher Education (dhe.mo.gov/cbhe). GCU's Bachelor of Arts in History for Secondary Education program has not been reviewed by the Missouri Department of Elementary and Secondary Education and therefore may not satisfy teacher certification requirements.</p>
<p></p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDEFQY1QNIBYB2NXNESRA3RCDONB5K0NFPCO4JIFFQX3HJQ1JJGFR">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDYJC02GQ3YATDIYQ1G3IZX0INZEMKMDKAUA5FYXCYXVJDBIZPAJ2M" aria-expanded="false" href="#collapse-IDEFQY1QNIBYB2NXNESRA3RCDONB5K0NFPCO4JIFFQX3HJQ1JJGFR" aria-controls="#collapse-IDEFQY1QNIBYB2NXNESRA3RCDONB5K0NFPCO4JIFFQX3HJQ1JJGFR">Ohio</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDEFQY1QNIBYB2NXNESRA3RCDONB5K0NFPCO4JIFFQX3HJQ1JJGFR" id="collapse-IDEFQY1QNIBYB2NXNESRA3RCDONB5K0NFPCO4JIFFQX3HJQ1JJGFR">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU is approved by the Ohio Board of Career Colleges and Schools (scr.ohio.gov). Any Ohio residents who have a complaint can file a complaint with:</p>
<p>Ohio Board of Career Colleges and Schools<br />35 East Gay St., Suite 403<br />Columbus, OH 43215<br />Phone 614-466-2752<br />Toll Free: 877-275-4219</p>
<p></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-4">
<div class="Block_Accordion">
<div class="panel-group simple" role="tablist" aria-multiselectable="true" id="accordion-IDP4MKEEJJ5QP5MMKDDZHK5VRK3BGUZPK5OYPBH4M3SXC21Q13HNZG">
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-ID5FR0QKTKASNDFYJL31A5F0USBNZJ0UQ5LDHMC0IRSQ1ZGVPQOWYF">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDP4MKEEJJ5QP5MMKDDZHK5VRK3BGUZPK5OYPBH4M3SXC21Q13HNZG" aria-expanded="false" href="#collapse-ID5FR0QKTKASNDFYJL31A5F0USBNZJ0UQ5LDHMC0IRSQ1ZGVPQOWYF" aria-controls="#collapse-ID5FR0QKTKASNDFYJL31A5F0USBNZJ0UQ5LDHMC0IRSQ1ZGVPQOWYF">Pennsylvania</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-ID5FR0QKTKASNDFYJL31A5F0USBNZJ0UQ5LDHMC0IRSQ1ZGVPQOWYF" id="collapse-ID5FR0QKTKASNDFYJL31A5F0USBNZJ0UQ5LDHMC0IRSQ1ZGVPQOWYF">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>Teacher education programs have not been reviewed or approved by Pennsylvania. Candidates will need to apply for certification and meet requirements for certification as out-of-state candidates.</p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDZQUVT2QG31UPOMC1FS2ZMIIB4GSBDZ5340GHQYNMH3CE5BOLA3YL">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDP4MKEEJJ5QP5MMKDDZHK5VRK3BGUZPK5OYPBH4M3SXC21Q13HNZG" aria-expanded="false" href="#collapse-IDZQUVT2QG31UPOMC1FS2ZMIIB4GSBDZ5340GHQYNMH3CE5BOLA3YL" aria-controls="#collapse-IDZQUVT2QG31UPOMC1FS2ZMIIB4GSBDZ5340GHQYNMH3CE5BOLA3YL">South Carolina</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDZQUVT2QG31UPOMC1FS2ZMIIB4GSBDZ5340GHQYNMH3CE5BOLA3YL" id="collapse-IDZQUVT2QG31UPOMC1FS2ZMIIB4GSBDZ5340GHQYNMH3CE5BOLA3YL">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>Licensed by the South Carolina Commission on Higher Education (che.sc.gov). Licensure indicates only that minimum standards have been met; it is not an endorsement of quality. Licensure is not equivalent to or synonymous with accreditation by an accrediting agency recognized by the U.S. Department of Education.</p>
<p>South Carolina Commission on Higher Education <br />1122 Lady Street, Suite 300&nbsp;<br />Columbia, SC 29201<br />Phone: 803-737-2260</p>
<p></p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-IDTP3DI2K5CISSH2DANUTPMHEJZKGB40CCCPS3FTGS5YORLDLGZ1IB">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDP4MKEEJJ5QP5MMKDDZHK5VRK3BGUZPK5OYPBH4M3SXC21Q13HNZG" aria-expanded="false" href="#collapse-IDTP3DI2K5CISSH2DANUTPMHEJZKGB40CCCPS3FTGS5YORLDLGZ1IB" aria-controls="#collapse-IDTP3DI2K5CISSH2DANUTPMHEJZKGB40CCCPS3FTGS5YORLDLGZ1IB">Tennessee</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-IDTP3DI2K5CISSH2DANUTPMHEJZKGB40CCCPS3FTGS5YORLDLGZ1IB" id="collapse-IDTP3DI2K5CISSH2DANUTPMHEJZKGB40CCCPS3FTGS5YORLDLGZ1IB">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU is authorized for operation as a postsecondary education institution by the Tennessee Higher Education Commission. In order to view detailed job placement and graduation information on the programs offered by GCU, visit state.tn.us/thec and click on the Authorized Institutions Data button.</p>
<p>This authorization must be renewed each year and is based on an evaluation by minimum standards concerning quality of education, ethical business practices, health and safety and fiscal responsibility.</p>
<p>If a complaint is not settled at the institutional level, the student may contact:</p>
<p>Tennessee Higher Education Commission<br />Nashville, TN 37243<br />Phone: 615-741-5293</p>
<p>Transferability of Credits for Tennessee Students: <br />Credits earned at GCU may not transfer to another educational institution. Credits earned at another educational institution may not be accepted by GCU. You should obtain confirmation that GCU will accept any credits you have earned at another educational institution before you execute an enrollment contract or agreement. You should also contact any educational institutions to which you may want to transfer credits earned at GCU to determine if such institutions will accept credits earned at GCU prior to executing an enrollment contract or agreement. The ability to transfer credits from GCU to another educational institution may be very limited. Your credits may not transfer and you may have to repeat courses previously taken at GCU if you enroll in another educational institution. You should never assume that credits will transfer to or from any educational institution. It is highly recommended and you are advised to make certain that you know the transfer of credit policy of GCU and of any other educational institutions to which you may in the future want to transfer the credits earned at GCU before you execute an enrollment contact or agreement.</p>
<p></p>
<p></p></div>
</div>
</div>
</div>
</div>
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-ID0A3WBZ1G5ZBCHG53OSEHEGKKKCBHW1DICJX3XUDIC4144AQGI53D">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDP4MKEEJJ5QP5MMKDDZHK5VRK3BGUZPK5OYPBH4M3SXC21Q13HNZG" aria-expanded="false" href="#collapse-ID0A3WBZ1G5ZBCHG53OSEHEGKKKCBHW1DICJX3XUDIC4144AQGI53D" aria-controls="#collapse-ID0A3WBZ1G5ZBCHG53OSEHEGKKKCBHW1DICJX3XUDIC4144AQGI53D">Washington</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-ID0A3WBZ1G5ZBCHG53OSEHEGKKKCBHW1DICJX3XUDIC4144AQGI53D" id="collapse-ID0A3WBZ1G5ZBCHG53OSEHEGKKKCBHW1DICJX3XUDIC4144AQGI53D">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU is authorized by the Washington Student Achievement Council (wsac.wa.gov) and meets the requirements and minimum educational standards established for degree-granting institutions under the Degree-Granting Institutions Act. This authorization is subject to periodic review and authorizes GCU to offer specific degree programs. The council may be contacted for a list of currently authorized programs. Authorization by the council does not carry with it an endorsement by the council of the institution or its programs. Any person desiring information about the requirements of the act or the applicability of those requirements to the institution may contact:</p>
<p>Washington Student Achievement Council <br />P.O. Box 43430<br />Olympia, WA 98504</p>
<p>This authorization is subject to periodic review and authorizes GCU to advertise and recruit for the following degree programs:</p>
<ul>
<li>Associate of Arts</li>
<li>Bachelor of Arts in Christian Studies</li>
<li>Bachelor of Arts in Communications</li>
<li>Bachelor of Arts in English Literature</li>
<li>Bachelor of Arts in History</li>
<li>Bachelor of Arts in Interdisciplinary Studies</li>
<li>Bachelor of Science in Accounting</li>
<li>Bachelor of Science in Applied Management</li>
<li>Bachelor of Science in Business Administration</li>
<li>Bachelor of Science in Business Management</li>
<li>Bachelor of Science in Counseling</li>
<li>Bachelor of Science in Elementary Education</li>
<li>Bachelor of Science in Entrepreneurial Studies</li>
<li>Bachelor of Science in Finance and Economics</li>
<li>Bachelor of Science in Health Care Administration</li>
<li>Bachelor of Science in Health Science in Professional Development and Advanced Patient Care</li>
<li>Bachelor of Science in Justice Studies&nbsp;</li>
<li>Bachelor of Science in Marketing</li>
<li>Bachelor of Science in Medical Imaging Sciences</li>
<li>Bachelor of Science in Nursing (RN to BSN)</li>
<li>Bachelor of Science in Psychology</li>
<li>Bachelor of Science in Public Safety and Emergency Management</li>
<li>Bachelor of Science in Respiratory Care</li>
<li>Bachelor of Science in Secondary Education</li>
<li>Bachelor of Science in Sociology</li>
<li>Bachelor of Science in Sports Management</li>
<li>Bridge to Master of Science in Nursing</li>
<li>Executive Master of Business Administration</li>
<li>Master of Arts in Christian Studies</li>
<li>Master of Arts in Teaching</li>
<li>Master of Business Administration</li>
<li>Master of Business Administration and Master of Science in Leadership</li>
<li>Master of Business Administration and Master of Science in Nursing</li>
<li>Master of Education in Curriculum and Instruction: Reading</li>
<li>Master of Education in Curriculum and Instruction: Technology</li>
<li>Master of Education in Education Administration</li>
<li>Master of Education in Early Childhood Education (IP/TL)</li>
<li>Master of Education in Early Childhood Education (IP/Non-TL)</li>
<li>Master of Education in Elementary Education (IP/Non-TL)</li>
<li>Master of Education in Secondary Education; Master of Education in Special Education</li>
<li>Master of Education in Special Education for Certified Special Educators</li>
<li>Master of Public Administration</li>
<li>Master of Public Health</li>
<li>Master of Science in Accounting</li>
<li>Master of Science in Addiction Counseling</li>
<li>Master of Science in Criminal Justice</li>
<li>Master of Science in Health Care Administration</li>
<li>Master of Science in Health Care Informatics</li>
<li>Master of Science in Leadership</li>
<li>Master of Science in Nursing with an Emphasis in Nursing Education</li>
<li>Master of Science in Nursing with an Emphasis in Leadership in Health Care Systems</li>
<li>Master of Science in Nursing with an Emphasis in Health Care Informatics</li>
<li>Master of Science in Professional Counseling</li>
<li>Master of Science in Psychology</li>
<li>Doctor of Education in Organizational Leadership</li>
</ul>
<p>The following programs lead to initial teacher certification or licensure in Arizona, and program applicants are encouraged to check with their state or local credentialing agency regarding applicability in their state:</p>
<ul>
<li>Bachelor of Science in Elementary Education (IP/TL)</li>
<li>Bachelor of Science in Elementary Education and Special Education (IP/TL)</li>
<li>Bachelor of Science in Secondary Education (IP/TL)</li>
<li>Master of Education in Early Childhood Education (IP/TL)</li>
<li>Master of Education in Educational Administration (AP/PL)</li>
<li>Master of Education in Elementary Education (IP/TL)</li>
<li>Master of Education in Secondary Education (IP/TL)</li>
<li>Master of Education in Special Education (Cross-Categorical) (IP/TL)</li>
</ul>
<p>The following programs do not lead to initial building-level administrator certification or licensure, but may lead to professional development credit, and program applicants are encouraged to check with their state or local credentialing agency regarding applicability in their state:</p>
<ul>
<li>Master of Arts in Teaching (AP/CPE)</li>
<li>Master of Education in Curriculum and Instruction: Reading (AP/CPE)</li>
<li>Master of Education in Curriculum and Instruction: Technology (AP/CPE)</li>
<li>Master of Education in Early Childhood Education (IP/Non-TL)</li>
<li>Master of Education in Educational Leadership (AP/CPE)</li>
<li>Master of Education in Elementary Education (IP/Non-TL)</li>
<li>Master of Education in Secondary Education (IP/Non-TL)</li>
<li>Master of Education in Special Education (Cross-Categorical) (IP/Non-TL)</li>
<li>Master of Education in Special Education for Certified Special Educators (AP/CPE)</li>
<li>Master of Education in Teaching English to Speakers of Other Languages (AP/CPE)</li>
</ul>
<p></p></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container row-hack Block_HorizontalRule">
<div>
<hr>
</div>
</div>
<div class="container extra-padding Layout_TwoColumn">
<div class="
                    row
                ">
<div class="col-md-12">
<h2 id="consumer-information">Consumer Information</h2>
</div>
<div class="col-md-6">
<div class="clearfix Block_Text">
<div class="inside"><p>GCU compiles data and maintains reports to help you decide if GCU is a good match to help you achieve your academic goals. Take a look at the helpful reports and links below to see if we can help you fulfill your academic achievements.</p>
<p>To request a copy of any of our consumer information reports, call 855-GCU-LOPE.</p></div>
</div>
<div class="Block_Accordion">
<div class="panel-group simple" role="tablist" aria-multiselectable="true" id="accordion-IDN0FR0MQY0CWTC0IZX35V3EKL0MKQIGUQVDD0S1NZSQUFFR0HK2DC">
<div class="panel panel-default">
<div class="panel-heading" role="tab" id="heading-ID5MVJ3E3TUQMABYKGOMN142BO1GVYP0OCSHKVG43YIAHQKRUUFIM">
<div class="panel-title" style="font-family:'Roboto',Arial; font-weight:400;"><a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion-IDN0FR0MQY0CWTC0IZX35V3EKL0MKQIGUQVDD0S1NZSQUFFR0HK2DC" aria-expanded="false" href="#collapse-ID5MVJ3E3TUQMABYKGOMN142BO1GVYP0OCSHKVG43YIAHQKRUUFIM" aria-controls="#collapse-ID5MVJ3E3TUQMABYKGOMN142BO1GVYP0OCSHKVG43YIAHQKRUUFIM">View Consumer Information</a></div>
</div>
<div class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-ID5MVJ3E3TUQMABYKGOMN142BO1GVYP0OCSHKVG43YIAHQKRUUFIM" id="collapse-ID5MVJ3E3TUQMABYKGOMN142BO1GVYP0OCSHKVG43YIAHQKRUUFIM">
<div class="panel-body">
<div class="clearfix Block_Text">
<div class="inside"><ul>
<li><a href="Documents/Consumer-Information/Annual-Fire-Safety-Report-9-30.pdf" title="Annual Fire Safety Report" target="_blank">Annual Fire Safety Report</a></li>
<li><a href="Documents/Consumer-Information/Annual-Security-Report-9-30(0).pdf" title="Annual Security Report" target="_blank">Annual Security Report</a></li>
<li><a href="Documents/Consumer-Information/Constitution-and-Citizenship-Day-9-30.pdf" title="Constitution and Citizenship" target="_blank">Constitution and Citizenship</a></li>
<li><a href="Documents/Consumer-Information/Copyright-Infringement-Peer-to-Peer-9-30.pdf" title="Copyright Infringement Policy" target="_blank">Copyright Infringement Policy</a></li>
<li><a href="Documents/Consumer-Information/Drug-and-Alcohol-Abuse-Prevention-9-30.pdf" title="Drug and Alcohol Abuse Prevention Information" target="_blank">Drug and Alcohol Abuse Prevention Information</a></li>
<li><a href="Documents/Consumer-Information/GCU-Equity-in-Athletics-Report.pdf" title="Equity in Athletics Disclosure Act" target="_blank">Equity in Athletics Disclosure Act</a></li>
<li><a href="Documents/Consumer-Information/Family-Educational-Rights-and-Privacy-9-30.pdf" title="Family Educational Rights and Privacy Act" target="_blank">Family Educational Rights and Privacy Act</a></li>
<li><a href="Documents/Consumer-Information/Financial-Assistance-Information-9-30.pdf" title="Financial Assistance Information" target="_blank">Financial Assistance Information</a></li>
<li><a href="Documents/University-Policy-Handbook.pdf" title="University Policy Handbook" target="_blank">Grand Canyon University Policy Handbook</a></li>
<li><a href="Documents/Consumer-Information/Institutional-Information-9-30.pdf" title="Institutional Information" target="_blank">Institutional Information</a></li>
<li><a href="Documents/Consumer-Information/GCU-Student-Right-to-Know.pdf" title="Student Right-to-Know Information" target="_blank">Student Right-to-Know Information</a></li>
<li><a href="http://textbooks.gcu.edu/consumer-information-site/" title="GCU Textbook Information" target="_blank">Textbook Information</a></li>
<li><a href="academics/academic-policies/title-ix.php" title="GCU Title IX Policy" target="_blank">Title IX</a></li>
</ul></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="clearfix Block_Text">
<div class="inside"><p>In addition, we provide the following links to help you compare educational institutions and learn more about GCU.</p>
<ul>
<li>College Navigator (nces.ed.gov/collegenavigator)</li>
<li>College Scorecard (ed.gov/category/keyword/college-scorecard)</li>
</ul></div>
</div>
</div>
</div>
</div></section><div id="mobileRmi" class="visible-xs md-purple50" data-spy="affix" data-offset-bottom="150" data-offset-top="0"><button id="mobileRmiButton" class="btn btn-primary" type="button">Request Information</button></div><footer role="contentinfo"><div class="container-fluid dk-purple">
<div class="container">
<div class="row equal-padding">
<div id="social" class="col-md-4 col-sm-12"><a href="https://www.facebook.com/GrandCanyonU" target="_blank"><i class="fa fa-2x fa-facebook"></i><span class="sr-only">Facebook link</span></a><a href="https://twitter.com/gcu" target="_blank"><i class="fa fa-2x fa-twitter"></i><span class="sr-only">Twitter link</span></a><a href="https://www.linkedin.com/company/grand-canyon-university" target="_blank"><i class="fa fa-2x fa-linkedin"></i><span class="sr-only">LinkedIn link</span></a><a href="https://plus.google.com/+grandcanyonuniversity/posts" target="_blank"><i class="fa fa-2x fa-google-plus"><span class="sr-only">Google Plus link</span></i></a><a href="https://www.youtube.com/user/gcu" target="_blank"><i class="fa fa-2x fa-youtube"></i><span class="sr-only">YouTube link</span></a><a href="https://instagram.com/gcu/" target="_blank"><i class="fa fa-2x fa-instagram"></i><span class="sr-only">Instagram</span></a></div>
<div id="copyright" class="col-md-8 col-sm-12">
                            Grand Canyon University © 2015 - All Rights Reserved
                            <br>
                            3300 West Camelback Road - Phoenix, AZ 85017 |  <a href="tel:+18554285673">1-855-GCU-LOPE</a><br><ul class="footer-links">
<li><a href="http://www.gcu.edu/academics/academic-policies/title-ix.php">Title IX</a></li>
<li><a href="http://www.gcu.edu/privacy-policy.php">Privacy Policy</a></li>
<li><a href="http://jobs.gcu.edu/" target="_blank">Careers at GCU</a></li>
<li><a href="http://www.gcu.edu/about-gcu/contact.php">Contact Us</a></li>
<li><a href="http://www.gcu.edu/about-gcu/media-and-branding.php">Media and Licensing</a></li>
<li><a href="http://www.gcu.edu/academics/academic-policies.php">Consumer Information</a></li>
<li><a href="http://investors.gcu.edu" target="_blank">Investor Relations</a></li>
</ul>
</div>
</div>
</div>
</div></footer><script type="text/javascript">
            // adjust mobileRMI to affix to bottom at the correct spot.
            // This needs to run before page finishes loading all other JS to allow Bootstrap Affix to work correctly.

            var footerAndMobileRMIHeight = $('footer').height();
            $('#mobileRmi').attr('data-offset-bottom', footerAndMobileRMIHeight);
        </script><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-JM6G" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>
            
                        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-JM6G');
            
        </script><script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script><script type="text/javascript" src="prebuilt/js/jquerypp.custom.min.js"></script><script type="text/javascript" src="prebuilt/js/jquery.elastislide.min.js"></script><script type="text/javascript">
                

                    jQuery('.imageCarousel').each(function(){
                        jQuery(this).elastislide( {
                            minItems : 2
                        });
                    })

                    
            </script><script src="prebuilt/js/custom.js"></script></body>
</html>
