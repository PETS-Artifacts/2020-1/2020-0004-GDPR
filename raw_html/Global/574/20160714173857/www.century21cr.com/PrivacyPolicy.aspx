

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrivacyPolicy</title>
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link 
href='/PortalOfficeShared/Office.css' 
type=text/css rel=stylesheet>
	</HEAD>
	<body class="Page PrivacyPolicyPage" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		marginheight="0" marginwidth="0">
	    
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-V23M"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script type='text/javascript'>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-V23M');</script>
    <!-- End Google Tag Manager -->


		<form name="_ctl1" method="post" action="PrivacyPolicy.aspx" id="_ctl1">



<script src="/WebResource.axd?d=2LiBfxUzpQryE6RcLvFatc08xFKGnkYA0p6IuRSv8yRh3TS-nGjsRMiViOvcE6vOe0CZZPdW3PfFPnoxhTjiThrwvDXanPJEyXnKMhTJW26QHZpC0&amp;t=636038881702983166" type="text/javascript"></script>
			<br>
			<br>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<tr>
					<td class="title">Privacy Policy</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span id="PrivacyPolicyLabel">We recognize the importance of protecting the privacy of information collected about our customers and prospective customers. This privacy statement discloses what information is gathered, how it is used, and how to correct or change it. It is our intention to give you an understanding about how we collect your Information and the use we make of it in the course of our business.<br><br>The information provided by users of this site or emailed to this office is collected for the purpose of better serving the user's real estate needs.<br><br>The user can request, by email or phone, that our office alter their information.<br><br>This website uses cookies to record user-specific information on what pages users access or visit, record past user activity to provide better service when users return to this website and to customize web page content based on visitors' browser type or other information that the visitor sends.<br><br>Appropriate security measures are in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="CloseButton" value="Close" id="CloseButton" class="btn btn-primary" style="width:80px;" />
					</td>
				</tr>
			</TABLE>
			<br>
		
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="r+n6l+KlBuRak9B+aJ+xLNyptRaK2KRIuGL6VVNDqGZai8nqeErHwG9Dq16I3DVzAvXY1bQUOxpDQq6A1XtSFeILaKT940qxPK0JkPhwzAdouq4HfJYyqRNqB+ams6B5hn6DA51EfGDAMsGm" /><input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="JOVM3/wf39/YhcKzZ3sScK9k4PXszUnE0TcZCaV7xoJGMHCK+mXcSyoeE+3Ba6Dt+iRS+n2spnAKWIh1qNyvOccMhHKM+jT6hNqa3gJlv+GbRc2j74Y7zTS/bdr9+/OXFw8qftIf3aul7gaksYr0xiAmNs6HNU9NRImoZp6A3Mo5zbF+yLCDRY1Y8soB4QTtUzusj7ekYXaOXN5KFfdvZvMxlS8tX9hA/RoIhaf4xD1kqD+SK7mCBx3zghtP+H7GB/WSUQrj87fQVLdLjYSLTvcFfMQwaCjmkGRGjUeNRJ+SGXYxBnpq4RTmi9YULWXOL9qYV2covGdb+OvIbTVOZrwyz6L0OnhuOzaF9eahRyZbYGARAJwrcZjP/kVtWww3sPdkLZavag9QrHrXvPuaV9hlneIwnBsoH38J2HhUSAN9sO2F5x3152uMxpGduN5N40guG30kiypv+2LDNu7+valp6fK8BSUVgpz3nsbVx9o8cDM/4SHRSJdJ76kgIK5lzejORMk8uPArHE9u2hPzsjFVOO74qQps/NBgM5eqns0LgYgXF3CMP9YmVTO2oP8BknZ4JHz/bl379WOAqIVdoeEaod56TIR3gE8aIM1kTySjvH6b+Li80zmZCfrcajD5jcbopWkzqX7s6fL9ptMca8xxN7L8DoUihB41q0uwXv/z1sOgjdbK9MSuqF8fXF4UhMO2S1v5pZOG9tXUkE9LFp8boud/q12DZioZ//+ydktS5GcdvaFJNoHmSE6Po2ej7g3VgVYTws1yham+IMKQbnqAJAwqve4v8l40tkg36AtksauKGXaw5pcAt3PgUWdNDeU9Prow9DT7Ski1jHaIIXrX/zm9/UR495zKy3NXx+zT+G0NeoeK1kZJFiTGJA4LEyEIJNAukJV8DdP/MaOH5QKl8arbVc8eUsk5L7EtPSIiYsJuO/WW5qkvFEAqjuHFyOICri46o5IeZu5R3V9FDmvm84vHjW9sGARwH0T9fRjIcT9z0Raku9AvU5XmqLIMU5sV1MVpLNldcZ3AmtLDBXLfrbdZ8eMgAtQnTMg8ym960T5oMNjFoc3T5jUhepaf+H+JNrSE0Ef9a8LKSb7mNxC+U85cHwjft+lA4fTfKxUQK/F7oK/J7rAmb+s/JKb2gewHU0d4fyDQzauD4zjbrnnV0ipzB2mjYrb1o28U5yRwQZLgdM+8CobOd06XMzoIu9JAwh4mk9lH3n3meSyTsOMAcLTJw6WoPPz3BIpyU66bIIgb9bXX5/SiLoN/GQ6n6ecZkOWLkGWIhPxO05jtAB/WABx/Vv1SWDiXscbjqof2sdXs/WGNwCAILBd0IGmSrD0tjPk/CQ//GdtuMIJLf0PowwNKxEkUtt6YN8af0IkTMJgtChWC1XcgI5X3W6IigQ4EWGY/hoI/ChDRM9zmL4W3CIMf09QVv0n0NqQjLBBFx6hPLBPosxvN5XEjFaMUTM6TDiRCzo1POHpSUqRw2r3UK1R9U47pPlhnpgv3WHA4lxUQsXAKa0O00nJlnYN2vJcvbDgCTiTY50CeKN//QmWG6K5hLqP2Pua+Pj1FgiyCsrrT9p3RULpuVtUjuILhL0Yn/G4rdwW8TcIpkTEawfRuMj62lAunuWu9BohYuAoWhnzT" /></form>
		<script language='javascript'><!--
document.getElementById('CloseButton').onclick = function(e) {

 				window.close();
 				
			
}
//-->
</script>
		<img id="MultiTracker_TrackingImage" src="http://www.century21cr.com/Util/Stats.ashx?tv=1&amp;tpv=1" style="border-width:0px;height:1px;width:1px;position:absolute; top:0px; left:0px" />
	</body>
</HTML>
