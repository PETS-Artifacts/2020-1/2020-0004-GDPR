

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrivacyPolicy</title>
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link 
href='/PortalOfficeShared/Office.css' 
type=text/css rel=stylesheet>
	</HEAD>
	<body class="Page PrivacyPolicyPage" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		marginheight="0" marginwidth="0">
	    
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-V23M"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script type='text/javascript'>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-V23M');</script>
    <!-- End Google Tag Manager -->


		<form name="_ctl1" method="post" action="PrivacyPolicy.aspx" id="_ctl1">



<script src="/WebResource.axd?d=2LiBfxUzpQryE6RcLvFatc08xFKGnkYA0p6IuRSv8yRh3TS-nGjsRMiViOvcE6vOe0CZZPdW3PfFPnoxhTjiThrwvDXanPJEyXnKMhTJW26QHZpC0&amp;t=636639363592191889" type="text/javascript"></script>
			<br>
			<br>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<tr>
					<td class="title">Privacy Policy</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span id="PrivacyPolicyLabel">We recognize the importance of protecting the privacy of information collected about our customers and prospective customers. This privacy statement discloses what information is gathered, how it is used, and how to correct or change it. It is our intention to give you an understanding about how we collect your Information and the use we make of it in the course of our business.<br><br>The information provided by users of this site or emailed to this office is collected for the purpose of better serving the user's real estate needs.<br><br>The user can request, by email or phone, that our office alter their information.<br><br>This website uses cookies to record user-specific information on what pages users access or visit, record past user activity to provide better service when users return to this website and to customize web page content based on visitors' browser type or other information that the visitor sends.<br><br>Appropriate security measures are in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="CloseButton" value="Close" id="CloseButton" class="btn btn-primary" style="width:80px;" />
					</td>
				</tr>
			</TABLE>
			<br>
		
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="Jw5J8wbiElcvra0oyVyLCN4kGU2RvVG5caLueE+2XlBH/4XVSLMV5b6AyR2K/RMunZ3h+7L+spycg2XSeQebGif6rrFbrt2TMfU4GBuKnRp9yFMO3CrFzdeq7+UVYDjq/xVrPHCjPXYFLd5N" /><input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="DgmzAOKHcTp3Pr6o7XEYkUemm751nGv5rpgFvtzurLMkQJYcp8yVQoym18Ql9bxAz/uCk22LLWSl4/7t2qawV8Tp0NVIhxuBWHmrKimcYD7rb/lqczZyNxoQYKMG6x+i8Pw5e8jFDUwZdroFz4kwCxWOhSiU4ZWzMyzSG26cmGCf6tOyFlhnJ/nZKMjrqG3IH5OXB0e+1tXtOz7MfrKU2BpszVnbU9AfnP4i3OEMNwR6tr5LL+8TYajkfNzfJhBUJD9/5BhDyirHhKg92xDs+tjvrwK669YgXkKnhx4rjMSFrhScYN7AKT91i8djOW+gDI/soWjstTe95B6mevxbXj1VOlNqXnXaPDJ88KrrFL4CHoDKCtC9o9MFDPCiuIZPzKauB51dapzA+gtFLqPE5Lge+fbd2iXHDyWu8gsfUlGVWByMsw/Vbb+xOT0WjBjQl8dcCmnFXluQP7e3ObRFAduM1pkpZaIJsMvlEN2EW9xsOFP4AVXBGTwnWafyHRPR/bfBEfEvgnBv1y7y1NTxC7rTf1mPjboBF+kEMyftW9cnXaXxQRG28uClpqQcpq15mungA0V6ACuNYNrlr6tezJ9v8NGPp7G0o/ibn0TvCXJgfu4MvgTJuEZUTH/ORyUxv/eCEugmDtPYsldLMMU8+Dtrb/WGJwfJThRJ/tvB56OAABlBu3XUtp5chQU6SnBWkJEEsAUSpJu2xdXisH9rgjNV1+I0LxuFSJp3gQfVdsfWEsY+7yTSugYB0UQ9rygfGLQlYPRgxoTfkn8UgUzWQsGgPz5egYz6qnv6TFMo4k0IkkcYAcjA93M1U6MCAJ0U8pnSsJOK4o1xLP9LPF3BQw+Ubnfn3VPFl+BJe4it03z038WW+uIOBMJ7dr7egbXeX3IVFUo26CJF7XP2zt0JiLrAabYETOJMAtgpTtGMzAggu++7aRvpTbprzsULLyrAmGEEMMkZqAB/+QUJrBz/+qkH7O2VWb4+uUU82w7R32/tpoU/iOE5yt/kT5X8nwWcO+7sl5lShzvyi14Y52Tl9oOfuA1/o1CdYJBppzIIdvtlGEyyicIv4L/f7fFSrRwBet4HbZYooqG4jUxnikFZKVZahsuxLIaa+fefPS1u8C1bU3flxcICqiAqBM4TQLtgWWHZxfqhQ2KrxHQinQEvsKdtrdyD5bP354aAaeFoKtD0PEdoFw7YqxgB3EJf9gkAJiEVdK0dUdjQzoMpfaxkcZQpH1uPljIL6g/1tsYzdpkUeHtn4kBkl9I3kwXR6POKgEbVh2shpAPzSbpVCD5jgd7J9eZtAnKaQnaZtVed2ZFQ4cVgcWaUEY2gyDxmJFEW5eyvdMXWEp59CDfL40sKLi4tfodwVfNr/nM9C+WTKLBFQpaeQCqBh1c5dkzA8sWrgGH8Ld6cZMzbRLHrX6rMY5CICHYm3M7UNiz2eLystvUh815LX6kRPYw0ekv4hORlENC6OXdNLrN0EszoCA85iF2lbKGblgrasp2lx86yVxHc/DEjDmHC6aKIiY9Qm2/f5SUmadUdmhZTZzyrF//oLPa9vBYB7R0zaUJ3NZOn7C60e18YJsLQInLvPZds54GU2SCdnvBeUVYNAik70Oxnqbw28s1Qt+OT9rTZTWXbxEwLurO1" /></form>
		<script language='javascript'><!--
document.getElementById('CloseButton').onclick = function(e) {

 				window.close();
 				
			
}
//-->
</script>
		<img id="MultiTracker_TrackingImage" src="http://www.century21cr.com/Util/Stats.ashx?tv=1&amp;tpv=1" style="border-width:0px;height:1px;width:1px;position:absolute; top:0px; left:0px" />
	</body>
</HTML>
