

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrivacyPolicy</title>
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link 
href='/PortalOfficeShared/Office.css' 
type=text/css rel=stylesheet>
	</HEAD>
	<body class="Page PrivacyPolicyPage" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		marginheight="0" marginwidth="0">
	    
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-V23M"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script type='text/javascript'>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-V23M');</script>
    <!-- End Google Tag Manager -->


		<form name="_ctl1" method="post" action="PrivacyPolicy.aspx" id="_ctl1">



<script src="/WebResource.axd?d=2LiBfxUzpQryE6RcLvFatc08xFKGnkYA0p6IuRSv8yRh3TS-nGjsRMiViOvcE6vOe0CZZPdW3PfFPnoxhTjiThrwvDXanPJEyXnKMhTJW26QHZpC0&amp;t=636131353326607946" type="text/javascript"></script>
			<br>
			<br>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<tr>
					<td class="title">Privacy Policy</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span id="PrivacyPolicyLabel">We recognize the importance of protecting the privacy of information collected about our customers and prospective customers. This privacy statement discloses what information is gathered, how it is used, and how to correct or change it. It is our intention to give you an understanding about how we collect your Information and the use we make of it in the course of our business.<br><br>The information provided by users of this site or emailed to this office is collected for the purpose of better serving the user's real estate needs.<br><br>The user can request, by email or phone, that our office alter their information.<br><br>This website uses cookies to record user-specific information on what pages users access or visit, record past user activity to provide better service when users return to this website and to customize web page content based on visitors' browser type or other information that the visitor sends.<br><br>Appropriate security measures are in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="CloseButton" value="Close" id="CloseButton" class="btn btn-primary" style="width:80px;" />
					</td>
				</tr>
			</TABLE>
			<br>
		
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="gKYdhnCJLRc9MRJ0iBhmWO+sKQCNnRo/BUF1sGksIn5UV3xkDiH6jeQ0RV6rcHGyHKjqTNRvwV6vWSAErClJXqXNAYQKrdKqTldMMSbBj9U9EOQY72mahtat6mKd28/hhbY9yPXOIY4fgS9E" /><input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="IKxEjbqqf80v3G8cqhh8xcX1g44qKUbKoLWsqrlG+8KzUb+0wy9AM5Jff0+7DaE0k13HzmFG7XfeXbN/AdR5IkpWardOVfNaJxCM5NZtI0Y2k1O6SVP+VO8q6LKLwk2P3MQdqIePakB1U4cwXdHT+AfLIW93q1IYWLzdeSssDut6S7KIkpFXRaw0fo8Z3We+57h2JfhC6m7Yhh1xT4O4TELyMKA9/vY8WCn0ImEG35rhoxK3Fa/l7v/dqukcphHywkJ0/8QSAiIo8uynU8FsHO9bbBTqYNjKohPpuE1xVJYGq9mKC6fZWbGtw7TCcCGcU5QZwICO55PmhqSgibFAAla4sRXV1b/Mo50MRtUIQl/Rb3SN/t4WjfiYCqMtGLREnta4yS+wv9iS7N26anaU+e+ODuNCJgFS5brPK3FUw69HOjgo6wWweRL4sbwCr2ZF1HFhCKCwOvrTYZEDkwPof/J/PDqoYGaiVgr7nc9OMetjsxXMoArz39X0QzNOF9jHBhy2ZgVstBUGF0jl2uLMmYIByQaeUMPPmbNwzQb+FDMznoQJ+ZvxmcvkOel2iHtlC4r3JsuYHtGzR3rX3DycqxBYq+aavQzCZ9umFADJXEkWqtkbH5oWq0qTKXTHmPicUT90cdcL3YqzErfN7kLY0Fuo9kQSHEBPLBsUO34z/DHNjP6607lkYqdiHHu07SHiBp8rtJxbBqxHGjWTXJhmXD9jehb1UjLxEqNfhz9dY7jL3ghnqoGUwVBiK4LL42/cpnMSgWBBQysFN9h6kqE8DTymeZrLbYv2QPWO7rinGZfGd33eZF7bSBnzYLsbN0YV90/WJCGhVLesvH2JOzudUhf+g0I0b2Nldr7JBB7rnvRefG2qtiDzNxOjQE3OjXs+T3Bm7/+rf+CQZr3IXpdbO5lBDIqAgU1gXHC3kTJrktkw23GUUZmPi8UN2aVC0bXESXjN7y8i7MElV7fnAiIWophUlKi4fT1wkMRtnN9Du9YNsw7I7oAVfNOUsek4Wyh8tr72sVbLAFnJNVsG3sSydkgzyCZAA2lwwqEqo4qR9Q8/9Z9oRkGkKqzq1HW8JPjxEr8Iss3wJnFGFbJqR3wWikW9HaMKqBVmqgw0FOeK01yfimKY/3IwmDVlLUvGD8xtU60a9k0Va0vG0BCzoCUhBXzN4Bm7tjQuPNfGS58ikTCyWCMJlZKmeVM0n6CRfFc7W36wygFU5kOedha5XUtYfU8lRnH0tUyHHqqhLev3fgQRvj6nUiz6komMgZqUonhqBbwlQpEuEkMwKj0c+P4lkYS04AFHqKA52ZUNQ7kvwf8OJpCYMVBAjd9cZlEfT3pzoRizAfR5+KKJyKcokZx/R3ge51ntma6I4m8Drr50E0W5thcTNVLQC0njFATQ8O/7y8YoCLHDjUqCm+Jz/gOgAN3zfSJQisJvyCZgJZIS6em9E05DEaw61vlF6ofsRibSkJWTwT1t1sBLr8FKpJg6bpnaFyiYKtey4K7XKKKv8h+RkErl42emU7PsS02QvhmTXHoSArz2QuHDvKOE5m/Lc82idHDrQWqPk4MfW0VSUBfoAXdmPYfMat6J8KiS1Gx6ZrSnit/qEA9cY02ofdpkLzWTea41AgNJfn1kHybqhh4WyNrc" /></form>
		<script language='javascript'><!--
document.getElementById('CloseButton').onclick = function(e) {

 				window.close();
 				
			
}
//-->
</script>
		<img id="MultiTracker_TrackingImage" src="http://www.century21cr.com/Util/Stats.ashx?tv=1&amp;tpv=1" style="border-width:0px;height:1px;width:1px;position:absolute; top:0px; left:0px" />
	</body>
</HTML>
