

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrivacyPolicy</title>
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link 
href='/PortalOfficeShared/Office.css' 
type=text/css rel=stylesheet>
	</HEAD>
	<body class="Page PrivacyPolicyPage" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		marginheight="0" marginwidth="0">
	    
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-V23M"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script type='text/javascript'>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-V23M');</script>
    <!-- End Google Tag Manager -->


		<form name="_ctl1" method="post" action="PrivacyPolicy.aspx" id="_ctl1">



<script src="/WebResource.axd?d=2LiBfxUzpQryE6RcLvFatc08xFKGnkYA0p6IuRSv8yRh3TS-nGjsRMiViOvcE6vOe0CZZPdW3PfFPnoxhTjiThrwvDXanPJEyXnKMhTJW26QHZpC0&amp;t=636213438639772381" type="text/javascript"></script>
			<br>
			<br>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<tr>
					<td class="title">Privacy Policy</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span id="PrivacyPolicyLabel">We recognize the importance of protecting the privacy of information collected about our customers and prospective customers. This privacy statement discloses what information is gathered, how it is used, and how to correct or change it. It is our intention to give you an understanding about how we collect your Information and the use we make of it in the course of our business.<br><br>The information provided by users of this site or emailed to this office is collected for the purpose of better serving the user's real estate needs.<br><br>The user can request, by email or phone, that our office alter their information.<br><br>This website uses cookies to record user-specific information on what pages users access or visit, record past user activity to provide better service when users return to this website and to customize web page content based on visitors' browser type or other information that the visitor sends.<br><br>Appropriate security measures are in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="CloseButton" value="Close" id="CloseButton" class="btn btn-primary" style="width:80px;" />
					</td>
				</tr>
			</TABLE>
			<br>
		
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="ogfET1SdAgRjkMU7r2txbNLseGECsi6jV8qkeuM5hj5dJFHFH9mPEptRzELKHMnPKdx73e0HWCREPerXHTxGi+6X2kkVxB/Dx04oeUj81RUKCFr5P3bIHq75tWXOudUW5klMZ/4uKlbC0IsY" /><input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Xhz7+BvIiHHsiwr8i3TO0QWK0hAdnUi1RnZhqY3rsm/gzpblbLajdaofaZUVy7MtoXoNa366VY2PZtMPPRuzWnLogfqXIRG51CXkvXvHqeJCjGKHHz9/1vMV2/etqWyuNRxopL+LxUVTpcWOAL/pW+CnytFPT/Q/2GWlwOrbCp51NnaQTJ+kO0jXOWxXYI0dhyzo4b7p8VxsKrIUq642D6eutVphfFL0IhOzmxkvusj5tYrsj3nP06bwAnnk7yMamgl0UzQWmw34K3EVquTuG+zZyRO0rFlWi5HLgoMltpduYUQEzJb0J3IlmJZ54lb6cG6UcrLbgKfhIRTZAeGj2DlEuDik1CnbnRmp6Ckp8p1JNJdDnpFkqFpfLOGs6VRIkoAbRfudiXg1LQPbAXKjEryk1VU+jH94TftATFPcdtp0fnZqAEGZ5A8B1a/hgeaM8Xp3qYduF/TBfW9VogFmaPiXOQp2gb4R4rmOVfS4ixPnNW7/V2c6Le7a7tT4QmqziVy4KM2BElCYedVfcrpd4wONaH9y7bqGqnMSP2qz5XN0p71avgISp8Zg42Z5bqOgfx90YfnDqjCKtG/LDNlPV9k8Q3/KkeGCFaUdjSnG/lOpByEOOukNnILNHVl/ap0YstRg2MALDOCGqZal3UArA/t78UX2UxloF99ZDGcGZS5T5OMOsVwFR7sZTV8COZW4ZvEeFz3H/ytm74BRrBUMvSNn0uLcKsDhL4HGTLzCpQbfHlro0H4gQv2bBYXtJy34ZtdjhKIwCesDXeKPGgIJc38jKzxc5ifYTkzAK4NWVm0TyZicvb6+SIo08r0AqWmhmwLIxIeHgHVZVEL+R4phF9b0fpVJN5jmo+bo7fN+BNpleIM83Vuzu+ZrGJH2uqk9yzqPAEtHfseS1rPHkt6VSg7+pHiIM+IHA0Npcb7HvdcqGjPgc05ze5nrJ5H7mDrorU+NfWHTBoMOZVFRrux/SQtiVgsox5l9CoKaUZVIKrxZE1LWUasHKk4wpgFy1gV2XMqIG0WHzkNxMoMDOKvfhEtpzYoPAoTVbSemnSCZJAaaAxP8H2HqAmjUaER9XzzdczJ05lj4KHgmxphYe56pv1OQu8Mu4uoghwyNYskvNvS5gKaq9xUL70uTU4NzHO2iQj3LssOE1Bg4qg1JB/1KSoDcvflvl7JF6fE2y6rK37kNyCOpVfXJgxvKnUpuxzPrvv/tYeCBS8XNMR+oaueqXmw3oo/G/6Mv3JgV42aytnQC3OSj2x8wfWH6hIT5Aeuzbhbmvd/PXjZVNgdwk1P+nlzW2IToLv/9gcs99kGR3UvqtsQPjP5l0naYzUBpK1o469pGdNcuG/wSG9K8TI7MaSn1NlpI3AKo7WiHijGzghPh3bK5woe+TOGJ89eqjzBIzP5d6nmCZBILJDI/cyYELVT8vfAGLmEp0NSv/qseNUIVxEXmxLFWoWpR+r3BS+fIvCQDa8nQfNsJlRtsertDgNHq8hi7oZUnH2a0oN9AHWgx/xJFLUm7u4V4mVjJ6va/PDXrRYyoroCou12Xq9PJ6fXjATbIx7NXqGhlb31Pjq8bD3vCoTa+12x0SiXHvcqVWE0paGI/vD011BYlbHLGvibT8xWFqOiMclfoOZR4YGYCEhtm" /></form>
		<script language='javascript'><!--
document.getElementById('CloseButton').onclick = function(e) {

 				window.close();
 				
			
}
//-->
</script>
		<img id="MultiTracker_TrackingImage" src="http://www.century21cr.com/Util/Stats.ashx?tv=1&amp;tpv=1" style="border-width:0px;height:1px;width:1px;position:absolute; top:0px; left:0px" />
	</body>
</HTML>
