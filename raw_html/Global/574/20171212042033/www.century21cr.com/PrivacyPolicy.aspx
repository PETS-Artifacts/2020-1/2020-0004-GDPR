

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrivacyPolicy</title>
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link 
href='/PortalOfficeShared/Office.css' 
type=text/css rel=stylesheet>
	</HEAD>
	<body class="Page PrivacyPolicyPage" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		marginheight="0" marginwidth="0">
	    
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-V23M"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script type='text/javascript'>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-V23M');</script>
    <!-- End Google Tag Manager -->


		<form name="_ctl1" method="post" action="PrivacyPolicy.aspx" id="_ctl1">



<script src="/WebResource.axd?d=2LiBfxUzpQryE6RcLvFatc08xFKGnkYA0p6IuRSv8yRh3TS-nGjsRMiViOvcE6vOe0CZZPdW3PfFPnoxhTjiThrwvDXanPJEyXnKMhTJW26QHZpC0&amp;t=636470031625220638" type="text/javascript"></script>
			<br>
			<br>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<tr>
					<td class="title">Privacy Policy</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span id="PrivacyPolicyLabel">We recognize the importance of protecting the privacy of information collected about our customers and prospective customers. This privacy statement discloses what information is gathered, how it is used, and how to correct or change it. It is our intention to give you an understanding about how we collect your Information and the use we make of it in the course of our business.<br><br>The information provided by users of this site or emailed to this office is collected for the purpose of better serving the user's real estate needs.<br><br>The user can request, by email or phone, that our office alter their information.<br><br>This website uses cookies to record user-specific information on what pages users access or visit, record past user activity to provide better service when users return to this website and to customize web page content based on visitors' browser type or other information that the visitor sends.<br><br>Appropriate security measures are in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="CloseButton" value="Close" id="CloseButton" class="btn btn-primary" style="width:80px;" />
					</td>
				</tr>
			</TABLE>
			<br>
		
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="nI6SbsFhVnF7zDBB3VtJTBnZVvDCNs1FMrdLrOJYMVZOOEZarwjqYQmQFifeU9gzUAw+i3VPqw4xXYImMZJ4a5rqb77OIjHGXoh8cXAoDcGMyUJvfeiTzBCsJGvh18RKiREDm3/g1ZYttq9X" /><input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ugRibt1690MbGbednoPRpPSxXHbx/vAsK587Kp2OjqQK7GcXwIKEwWBDwZuogk8Wdfr/5TvqJAF+VFuOhc1xiG06sc1+ogQFbiL5XhLm/VcPNTe/iI3VZPOdJoaudu/UYY4ROSsfoUOJILgm768gAeHTlYLXEJ23o4Ij6v0cptunkNSebQoT8207/HDPXPGisdAKJG6w2w2lhpG6uHZgQogwMVyyTwI1rCSJBm6dn2lHSg6rVg+g6vanGHKhnq+O/pn3PBIR7THu/6HUp78dGiRi402gTVBj7D1tQZTIxZ4wtVF92XrmiBOlCull5dijP/fn6eT/r9wKQDF1gP6SSdWC1qvLoP4BbUDQChE2n4FP1PdlPLaFO/yQiIn150UELM3mytLHKCJVeV/D1riJLM0aDIF8MQ4fHpiYjQ8F9vrBxGPw2SSTYFXUDiKcC+qKMssA2RweW06iE1lXEIMmRE8WzFdT3AgGmkIhGR69KI2m8lgENb7C4vdmMdVjAh5+0cfmxnP3wbjpPWyg3AAakC0KiqIUneUns3Ot0qM0bCp0fdWmil1Y8jb8IABBeTVDjxSHKSVMJZqM8kpnnTKNge3tpLK2Lwnmjeh6xOH/tBjCe8tFsNobRSkOVah7tMI5JmddCyuXsxMDfwyoHd1ktSMNOp3DLjAHZfa391vzJU7SGgkSJ5PIT6UEGQYHqo/uIOYOTENTaHXT+nk9N/jF72QGFlzIlu0gl4DK7/vr0uH/+E5+Xfzxw8iKRGss8+ODorfNdNrRQTys0N6bD0iAUm+unq7aRL+YVipJeeneZt09OOLI5L5/hpIinQRqaEynQBwix/38pJhnGP/YRkEXw8lB6tx2sbuddXiEW34k6csn2SVZU+MD3be/cquDjsW5JAdlJ3NOO5XsKZMPKqu7HvJR/Cqplo6Fn5F2BMA4SYlFs/vcg+nzserbqhfzDMREEzRjGQBqQEuJ2VoiNCakQA8ISB3bSVLBExkVXY8ID5FGVCXwqGoiqj3R9RgPr2kRhZgF9rdTvfnqUTnrX2JhbwPj+BzhLD/4L8TciIlVIQaNKBKm4kWAXfKyE7A2eRYqrGyO/qYsHkeoSxG2Cw4Z7CVWCH1WllKt7LestV7sbPIrhrEmZDSCCqVBm0tKqy8CpoZwIFMobtSAfvSyXiQUH4W9Tae8knQYsMkkPuvEt/u8JbqTwGdGiQsC6JYT+sHjpb2PUz+gXk5PLHwQSK3z6YKCHrEI0JGB1oTl23TQjS/w/zZAukIPyivwKcMBQgQ4rWxh+OkdgJX/93iCKdH9N+p5cAReSaviSzMYj9GC3X8iZUwyaN0kFO2aaBLaUZDE8Spr6h8uDYIeRzVbbl1eRQ+fskJuu5CXbufYlrScPxmrdEHMj+O9X1tHJ7+S/twm4oD/yHdIp2ToGt4DNhqAMZHjplfLFiS9hTiQ6cYDB3jjP6S68njkAKAsZjhudrdg5ninhXc1Qu2iSpJfYYD1Ol8pzOIJRu0LYymdTJ5nDjFiUPC29x3DadccIooSAZwPKX3TX3gBBOBky3JBep2MuaIaPfYcnYAUMH7dRApqoBedP532bmLySFvJXS0Pfir4i0fCIOTjwg+o8Kr+TLNFAgc7EiSwKIgScVSNf2jJ5Qqktq2/" /></form>
		<script language='javascript'><!--
document.getElementById('CloseButton').onclick = function(e) {

 				window.close();
 				
			
}
//-->
</script>
		<img id="MultiTracker_TrackingImage" src="http://www.century21cr.com/Util/Stats.ashx?tv=1&amp;tpv=1" style="border-width:0px;height:1px;width:1px;position:absolute; top:0px; left:0px" />
	</body>
</HTML>
