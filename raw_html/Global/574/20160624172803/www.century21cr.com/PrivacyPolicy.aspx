

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrivacyPolicy</title>
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link 
href='/PortalOfficeShared/Office.css' 
type=text/css rel=stylesheet>
	</HEAD>
	<body class="Page PrivacyPolicyPage" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		marginheight="0" marginwidth="0">
	    
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-V23M"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script type='text/javascript'>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-V23M');</script>
    <!-- End Google Tag Manager -->


		<form name="_ctl1" method="post" action="PrivacyPolicy.aspx" id="_ctl1">



<script src="/WebResource.axd?d=2LiBfxUzpQryE6RcLvFatc08xFKGnkYA0p6IuRSv8yRh3TS-nGjsRMiViOvcE6vOe0CZZPdW3PfFPnoxhTjiThrwvDXanPJEyXnKMhTJW26QHZpC0&amp;t=636016388680926790" type="text/javascript"></script>
			<br>
			<br>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<tr>
					<td class="title">Privacy Policy</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span id="PrivacyPolicyLabel">We recognize the importance of protecting the privacy of information collected about our customers and prospective customers. This privacy statement discloses what information is gathered, how it is used, and how to correct or change it. It is our intention to give you an understanding about how we collect your Information and the use we make of it in the course of our business.<br><br>The information provided by users of this site or emailed to this office is collected for the purpose of better serving the user's real estate needs.<br><br>The user can request, by email or phone, that our office alter their information.<br><br>This website uses cookies to record user-specific information on what pages users access or visit, record past user activity to provide better service when users return to this website and to customize web page content based on visitors' browser type or other information that the visitor sends.<br><br>Appropriate security measures are in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="CloseButton" value="Close" id="CloseButton" class="btn btn-primary" style="width:80px;" />
					</td>
				</tr>
			</TABLE>
			<br>
		
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="XgAa0VZkrUe8NrBTK133Se96A5YWRiEUtWj7aaT39VstQG3g8abmw4U6Oqeb76th8/11aPUJNn88/b/QymI984pACBaWoPORvAQk9gYnOzTX0UG7qTTqtJSYxE46ZNV04QngUUF2a4HWUSW4" /><input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8wQ7fFMvmU7OCHbVcp/UxqYXfj0WPSOTDo/YS3DI6Y8LGKxgvHOq207gc/9ZWfZ7Pfd3hWmmSOtxzfp3NyavMAPGaq15wd2a1izJ7mEEIhwXPqhDHaT4MHSSR/jzw4D0G9NbGsZsRb8fIVYXVSuYnEarCiOOKRHGtkq2SrkG/7dsrimi9NaWsjMw/LdImoBZTbDWhlc92ZdUAMInD7WprzJcPZEDwfXkfIp5rktdxLLvcg8YRevMhaYSkBR0jaVCqXLLZWpLIywd+Y3quUyXPbHCDoyOJnqKu5mIXTOvuE0nYfqC4jtDVzZVaFa4AU7NVk/q+LO/cjHn5Cn9DtPW+VPwPSbDuD5P5V5DF3UAuZid9S2BicFJLfzWyEO1CpgXSGpyspG+CP4ttYkLSbUCUJN/qLzx/c3m3WIqZJ9cHHJWBg8WfBOfC0XxhUkjusfSvryA5Cb97Tb0F/uhB1aQ2yNbY96yv25QJo34TVSxhz0TO2Uuq3T8z9bKbmCjkc6f+3Aa8q7UT02tJK7pNlmSOZjgLCugfjmdgU7zyANazBy0Xhso9k61blMKulPmMzSJxBDg5HN2Bdz5P07Jn/14/EKLgSuR/XX2dKnEBbxI6gK1ha3z6wfL4u8W41W/WM3mHeeyapB3fm+ybtVVIXkMsK3l1XmOQA7yflcW+I6cSBsHbVAWC97/Nb8xwAxZAKX6TPkNpgQbmUyytk+zwUC9BcMsnVM8o53ofFJvJ0tTKWzlc2UAEC0guk7DoKKciU72JP1EK+RbtLIvTQQUvJEs3eLr9sBmM3f4AafCoMI68Bo/INqfDkHAHD1m0CaoJjk9GFFkYBTAhXqf4HnbJh5T9FKC6sP+KD3UYU4uGCg6cY7wX647KUvNhkBzHGSWizixX2xhO40PK9DzgL0aKcpjWCAf1WPhnM6J3ESwDLgN1Lv3jwLpepshBujYlzGHPRPmm8/QtqFROWRfPk9SDb5iEQ25DWDq/5zR010PdiCsWFd+tzpdGH8eeTox00mP9VLbfGawNuIgvCCOCqwd6LLTFVcdeSEpfBl9b3IWLtSZ/w95EnLuDd7InvK7pttYF2KOGClCpMtjkRxWOvM0P4lqkJmDt6TSzQSWZwegdKYjTOBxObVNzvr8YpW3mtserttE1Om/bKqh0fIb1Er/eXr4GaPXyKU96qBVeyknH7kMyO/PhpOcRPWMizRct1ohGrVsYJOHvrXny4chjbCBiKsM6LzwJkgZ0SW89F8EDBmsbneRgs2yjDpwC4rjMVsEnLjf0xXAVFT//2sVfK4Y0iJtZyoZOQaYBU4XtDhD/wM+tljs06dF4oMzx6lbvGP8yaE3qNxnL9vsu3NX87xwLXxWxGqYvllLa0N70HHF0sJy6Bv0G50XDTCHGhucJ5LALPewTMHWkey6Dll8U7boYbugcfP7uSWSYY3HtK8/qfsf1oiSq03sn+n3NwK8aVeJpQ0wfjS2GXMjkjjYBJGsQKAmjf8q5xd/80DFoGckK94i4KwIT/9tkIW9js1cL7wyh8olFGkVdWm3VQQZml2Ok0kmw1XL2yApCrKIUfIHEjYZ8IBorZe1G4wn7aWNzv19zIcdYsLrqLT110iIjU+TJF3Ix17WIM6ifzht45TLMPLmlmJSs99A" /></form>
		<script language='javascript'><!--
document.getElementById('CloseButton').onclick = function(e) {

 				window.close();
 				
			
}
//-->
</script>
		<img id="MultiTracker_TrackingImage" src="http://www.century21cr.com/Util/Stats.ashx?tv=1&amp;tpv=1" style="border-width:0px;height:1px;width:1px;position:absolute; top:0px; left:0px" />
	</body>
</HTML>
