

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PrivacyPolicy</title>
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link 
href='/PortalOfficeShared/Office.css' 
type=text/css rel=stylesheet>
	</HEAD>
	<body class="Page PrivacyPolicyPage" bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0"
		marginheight="0" marginwidth="0">
	    
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-V23M"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script type='text/javascript'>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-V23M');</script>
    <!-- End Google Tag Manager -->


		<form name="_ctl1" method="post" action="PrivacyPolicy.aspx" id="_ctl1">



<script src="/WebResource.axd?d=2LiBfxUzpQryE6RcLvFatc08xFKGnkYA0p6IuRSv8yRh3TS-nGjsRMiViOvcE6vOe0CZZPdW3PfFPnoxhTjiThrwvDXanPJEyXnKMhTJW26QHZpC0&amp;t=636384476114039979" type="text/javascript"></script>
			<br>
			<br>
			<TABLE cellSpacing="0" cellPadding="0" width="95%" align="center" border="0">
				<tr>
					<td class="title">Privacy Policy</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span id="PrivacyPolicyLabel">We recognize the importance of protecting the privacy of information collected about our customers and prospective customers. This privacy statement discloses what information is gathered, how it is used, and how to correct or change it. It is our intention to give you an understanding about how we collect your Information and the use we make of it in the course of our business.<br><br>The information provided by users of this site or emailed to this office is collected for the purpose of better serving the user's real estate needs.<br><br>The user can request, by email or phone, that our office alter their information.<br><br>This website uses cookies to record user-specific information on what pages users access or visit, record past user activity to provide better service when users return to this website and to customize web page content based on visitors' browser type or other information that the visitor sends.<br><br>Appropriate security measures are in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.</span>
					</td>
				</tr>
				<tr>
					<td align="center">
						<br>
						<input type="submit" name="CloseButton" value="Close" id="CloseButton" class="btn btn-primary" style="width:80px;" />
					</td>
				</tr>
			</TABLE>
			<br>
		
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="2uMJuWwyYi2Vr2Y7BS9w9fom+quF84Q9VRrlimWx9pBh9oPvdCAgZed3UyutdAnsxS9+JfIHAoxbHJD7Bd7VpCUQ0OrKwTI1hRk0kgfp+c6VehU6xwFhH8W3cOe9Mk5BLbiO4cIyqno4qE5a" /><input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="88ym3s1zWhwTY0dopirqQUluJQGBUeVLepNMqdYgufMcgBjC/hgfRDF3jhjl3quGRHxvcxIIX2Uqscp+7VbU2Y6uAtPuAjG4mChKj1QBJrOrKo4H61hkWug8Z/B4dgo5kJ8VQgKnaeMXLTb7IoaEktEM5HqTPVHLaYwAa79hpJmOxZpAtMh8x1wCWHyJR1vafL8Ec2JcBRAvyyeLE6LTovebg3+iRa2EgO5LkndxsBtA1tzEPANlM1A1O5nR6/V5RpMTY2ILWAoGs1hChzGk7+Uhs8Jr/+4DF67d7JYh+KJBUkeWlbR38LHdl6jt8GGYvqg4L4fFLVxoyaMJhWKGa7Yp7abDA77I623Ic2CPuO457SdCkFjbdpXQC3mmLtaO3eNTiklF34B3Z3TtPElOuR7xhm+R5l5Az4ilrKbcNSlVM9A5dKJj1VZY1q39dYALOO2A9GMMeW7imQj0+hDE6DnRLj2WQVyZG6tCC4PdVLfYFLNrweTTJYBnhZYYrpMbvyJer/srVkX9vaoYUGhfG2MqO0Am3Uc5nJ5qYpm6jWYKJAkU4cF5im6ilz2erznEluf1TzIcJOR6lxZVuYcEp22AH2OjA7gc6SVYno9uOBbi4ZdtyMDeKHdgVk9cN+/KI8J9xbQXziZW2yT4jvy+JDbltvQy+2boyI19nOX4/nA6Kk4F+e0GW4+Hpzy2tp8vUdNPPz1auzHvcD21ZgRRLgW1XBIsH4pt8JiqAazGsY1rywrL1yWNfy6BX9FhjAgznjZY6PFXWY8Am6tgSxEo3Y2u7yPSqV2eRZ3xSEdAQh50sRkSUpNBqACqDxSP2kI6Hb+Zhb5TUQF3/OlopWdi/nxXfoMub2Lh6VWaIdSxBzRWux9urjxud2JXd/WFgg5mPzSzBT6BHBBu4ZxAOeOCZpsOEZzfWaYPWBxgT+ETdyQQLZEci8+xm3wUPnGQTMISjhgujz2IFLxuxz23/OIBvGsQFJkteqkFWQy6eA+0HlP+ejSmIYh+2ys+7FZjbsMqT9FWA2qaaa/cfMEd44fh5Wf5chLll9jK7EbV9JVhZCkj3pshOBe1N9Wtef5Kiajej111/EGrZV8Vu/BaO4eKx7MQHQ8TnceNA0DgGFIyTq8Aa167rCBop6hO3b8ajNZx97urNekA9F7R+c1oQmh92iAdvEYrxnzDUTpLqF79LGuIfpr6aG2YGW7mvrsOlRG5EbD7+A3r0D/ZcknilODlF026+Ksfx0EHbRJ6NlPXIABIl5G6tI1qKTX8+AvQtqpv7N6731Kw4DvT5TVDJaePIcm79BaiyBDewU8YDyNdMIOEOuo+IwjaTSWf0knVy4T07YW+gk85a7bth4BkI7NyO7HXKU7lbkEW4JYJ2JZVhbod176SPIX/m/WPrO/pkeq3cN5ZcxqRHHXRYIIK+Dtagh0eSFPoDK71lIkx/cnwOrSHJ6B7nGqeU39u7khBfP7odyBwvSef6KAo25dbVuT9LTP8Jf51BSOGjN3zOHJiJ4G5AVF73/0jOV3i/TorZih+44cjXF30DCTubJ+iq6eEhyaZtr7LRGkyDSOcLbO8x08q5/QsAB9tZ/n9oVx/KdJY/AGKF0dKSHztIwaBL2R7av5rQPGBuTQ8XIw/3AGNcRGHiDHa" /></form>
		<script language='javascript'><!--
document.getElementById('CloseButton').onclick = function(e) {

 				window.close();
 				
			
}
//-->
</script>
		<img id="MultiTracker_TrackingImage" src="http://www.century21cr.com/Util/Stats.ashx?tv=1&amp;tpv=1" style="border-width:0px;height:1px;width:1px;position:absolute; top:0px; left:0px" />
	</body>
</HTML>
