<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="en-US" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><meta content="text/javascript" http-equiv="Content-Script-Type" /><meta content="text/css" http-equiv="Content-Style-Type" /><meta id="MetaDescription" name="DESCRIPTION" content="Official Website of North Maharashtra University, Jalgaon" /><meta id="MetaKeywords" name="KEYWORDS" content="North Maharashtra University, Jalgaon, Maharashtra, Higher Education, Academic Institute, Research Organization, University, NMU, India, Maharashtra, Research, Under Graduate, Post Graduate, Certificate,Courses, " /><meta id="MetaCopyright" name="COPYRIGHT" content="&lt;a href=&quot;copyright.aspx&quot; style=&quot;color:#fff;&quot;>Copyright&lt;/a> 2012 by North Maharashtra University" /><meta id="MetaAuthor" name="AUTHOR" content="North Maharashtra University, Jalgaon" /><meta name="RESOURCE-TYPE" content="DOCUMENT" /><meta name="DISTRIBUTION" content="GLOBAL" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><meta name="REVISIT-AFTER" content="1 DAYS" /><meta name="RATING" content="GENERAL" /><meta http-equiv="PAGE-ENTER" content="RevealTrans(Duration=0,Transition=1)" /><style id="StylePlaceholder" type="text/css"></style><link id="APortals__default_default_css" rel="stylesheet" type="text/css" href="/Portals/_default/default.css?7230930" /><link id="APortals__default_Skins_Xeon_skin_css" rel="stylesheet" type="text/css" href="/Portals/_default/Skins/Xeon/skin.css?7230946" /><link id="APortals__default_Containers_Xeon_container_css" rel="stylesheet" type="text/css" href="/Portals/_default/Containers/Xeon/container.css?7314682" /><script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery.min.js?1.6.1" ></script><link href="/Portals/_default/Skins/_default/WebControlSkin/Default/ComboBox.Default.css" rel="stylesheet" type="text/css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico' type='image/x-icon' /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/CSS/menu.css" rel="stylesheet" type="text/css" /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/Colors/lightBlue.css" rel="stylesheet" type="text/css" /><title>
	North Maharashtra University, Jalgaon > Home
</title></head>
<body id="Body">
    <script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery-ui.min.js?1.8.13" ></script>
    <form method="post" action="/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="Rk1vfuNYQn9+APHLkMH8CsfvkvR0OxlJOrBgNLby3xjfYgNEoyJt6mG3DkitWvbWcwdhPIsbiT+PUJDvvTfK8rRGXdhM2yomhHadDELpylOVmM+v7Q5Dg/hYNUP8DnCAmUGqFhGGroY9Y00cBu45owo408POIJqDqj9fgpg1q8ysrAdk38pa5EL56fHI1OCKcyFdydtKejao8cP2hfnA6dntE/g9upyqYgZ7LI94GZdPpTIFAcG4tTAlHgwkpkDDZrYDF7A5zU3a84b7Mw1HXXmss832G9lVv0rc0P0/1R23oBGT1MeJTcu9erqAXKlGsKYg2d3n3eG9qXg593ejLnVTC9Qes+KY5WWL6owot+CO6615Sbg9PIYhvHHrYw1u2tnyApt6OEQKFXGpxSKPZgI7boQkgxjzB98W7ON4JzYy6r+Q0iPXY/z4CwDuZZcTPecCpg6hHb5fej3jLbdAUGueBRh/qTT5K5RYwoD5hIYDmDrtXusY5mFONVf6xFp+5XIqkDQa9TfSHxlq6my8Qn3di6migrUsaquUz118Q4/Y/BAoCN8LElrLnMJFW3AwM/LSQZjIv/tkh42nAMbudqJNgKI44vhnAMBu032ghx07320O1x9Xrn/g+4G+Mcv2U6g+8Kawo0TU9fSCUHyBPNRhPvDC37qijhdBCy4SfKtKK9Vka1sMI/VZA4nAhRq+vsFuc8ozQjrKnJ3VekE6tPJadZSGFVr3KZyilj1k3SZDulPocHHBHg9Kt6+3HR+Rkl9r0nqE+bOE0Kme4zWXd7MSndcOaL2ZlB4Nmz8BE0xN78SH7brlUqwiLPTQVK3HTzcoOkNfIo2G1Iwc6UydKNvc9t5RGUZ5VBHHk6mEs/xatZYUpoPZwTmRqoA8Z/geSszKu2UgB1ICGV+sGH0eFp7W8hijxEQAnVY6Q4kgWP9TORmesdduCdnT9EEP2pGGh14I67DL/OdGdZBCJMyMPF39GCgwjMBara1Lv8yT///u5WZCZPp+gRjQLtt24RFn3Bum+FImXmmA+9hdyMJMbrm4SVdD3bisS+1rWazBaO3YBzYFqUf2QhY97+MF5zWcHeknPmBJ0nmFn74JQryj8toH+jZbb1xC3ALziXCrtromYwcQjWuTxX8OLvqcGFig4hLQLEd36rExaWVaZHXPGCvEq80RWijLJUWF9yb7ElXPkfFi0U+tPk02GairvJEjVh6iECZFn2eMyQ+8e9QBu2KPHF+NPnebMm/swl0ChgPdxX3ybBEq8kqUcM/XkkqBzysYAV29kcOZv+w+ZPh0zY9T9C0IwteGX6wVfEKKfP8uOAhBUHWWaZ3jU55Km15gZ9udZOsFDCuKK4rLcCkYxZiMVzsm5xFwOw7fZQnAl5bF3UqW4EGygjRq4QuXyecK2ItwxvOLjARCHhu3Qk3aBleV/wDPN8eFBkzhBZZeFJr75ArhUeshg8iH4+v1oCVU0JvIU+ceGQpznW18ylWSzwimFiNnN30DSVCxLKoD2Uo9OBYcCnh+2sQsOohYaqqHG0xBoM+cgMn9FWBGOSldh8FdCajpMj4gtU3WsXt19AAbuvHHjRf7RF5ljW/J/dHOx9t6iEU08Vn7uNvYuPXHpQgBleb6y/sD5bynPzQXUGZXw2yfC0sSEH6KQ8psJTh3i5zCRZtky6w8+5KfruCKHa10N45l5et7J9Xl8fqjLngSS6U9/3Zium7/95jTgOsrUbl9fosrjMxJeYK6Va3mawClphTbwh9rQ/pGJvYENdFK5cfVh2sl6HJw7hOks6Fj9BePeSWpkgTCljfaqx2bd5DWK/6ZphPJ2o7rPBJtvDKAlCKQsmCJ/0yoZbUGrajA3rDQjRpo4U6UuSONJ7lEVua/95oIPoomptXo8Pa7gCkLJ2splm/tqkGJoyZ2ecAUd7erKi6HzHNeYaCpn8YRfwjOR2DDg8z5QJNaWYf9G3GGNirF46+26BAzwehFpOI9Q5O1iiNX2MNzpx5iqFA65tG62ZOnZbMVMFguoPaJVE+6Czl7Wi3Rfl3WwA8o2mSt2sSbRRSENlMHCv71xuyRAyRf7og3emhJFmkhdrKHsJ1iImmMWZv2LmhHj7BZbGzi9c57yIa2MLsUqXxak36jSMp10DmFV0NfIsvAeCOyq+jGUNzrV4EJ/OBZORGuHsPvbEOYAA4GYPQowIWPPg3F99SKjIYQp4AayGw12WZ+IAb9M0elq2BQBR70HEdMuRtp90OK23wlQGV2bAPOADn0n+Pd8YWq1D7zDkm+caMomhP+8AJ9NRgV5F5fjVjqZ5FwTmGk5xlL/oE+gmE/FiZy7zioT1fnpMmfZxifCnN8Pz2N8U3F+WGFta13zzZLXuKfQnWjQZWm1jhkxmUPw/+EUyIz8luPCuT3YnGVVXK4vbJ3TRqeZHOfQJnu5Ga5FNQLkwjjRMxholKnnhFNQ3TbGzTbbyENdRImGDjGjZ9pMe9T/v/LnTV4m3F8oLMwNcCOiGUiiH9GvcUvZJreYSwBNYUXRCI5ZbNpve1lfLfDs2YpyYKsiYrZdflKuuANlvlxD2nLHSJt9pO0kr+8c12ff7q+tfJg3EAjNdPd5r3rROKXdXOMawfg8O0t39BLDfLq6Q9e/6BfIjUogp0aj0+L6ISV7/PDY2teufCaJEbBN5jo5uJ+3tTHJIlE/6Kz2Somskof3AggkHF5+reVQ5izS72WO3O9ZWsc/ULo5U7a9zbcMRgQFGn1TFALNafsEms6BvcDOWsCdb6QoHuCKrqc4Sek+j5yDj8tPnjSbDxMSIqvnUGRaCrkBbWrtDT0h05fqdvF3fcbSf1At9hXn3k9NKf5KrstPuD/M5NevZ8M1gKtxBxITcSsQ0kTveI314B4yRU+bdychHkXiaf0kWmGFDMEOv7vrjzjNfkc5/Lwc+h0YBRWJ5WrA5y8pVDN4N6TUVcngb32RP5R5p+gVoUeFHypX8ENw1DLMF4phUuL228+YoVYUmtinyeaZKvKOJUlyw/uKUa3M7gdvqCLS7d9YO7eSmP8r48KtFKY1IIDn047EdMqXVlwE0KapMBowLL77X+DvYNHw+vM6umD6HYxlRDmqd5aRXM7epEJK/zLsF7o1Q+s8rvGPNpaS8ihfLlMkF8XKr0DmVW1FkxJjO5BJ9MrChITB9pmoqO87z68f/SYOgMt2Sl1WIrSzCBB56MnJxUid4SBpFCshJ3h/Dhn6ximV8MoyIj/QjWw+PtmDMbvM8whxX98keK9TbM4KSwpd98/mt9fcAiqxDhqKpvCVZzK0Yqvh2fSNYLvNbvMPLGTwGAfzds47i+xGp0LFf1L9Ka07vSMwZunzx+b4Ssqokpqa3Z7I7wGfIpfBv8pzhq/2TjaSoGP2VVyw2Oi53gwVO+xlWlDKYXLiUDG8b1ZQiekBLHS9nbLDT5dBCP/zZW1fc+7z1bqXHythSZzk09YytfuCOggArE+exb/lqejobsDKS0DBpbhSzA0TEczx029hGPi8lW3dA4LlbtXBNE5SklSbxevj4I1imeQFcsdaOLiOorpkWdoDtXrxBVA+y6Gda01KGn4LWVNkM+jBjFZzRCYOa2Fwlgzgo7Zn24hZdNpfltbWb0CiaWTo9k5pBx9Bba9xgp7Ufqs84IEwMf903UnKxzaNbBiE4pHXcUWrc+iq+pzU2Kowrtqjg8k5k45JVt5m68qIxDftJ2ANQvO2D2sGKNJ/3QyHn6FBtL7Z1I8t+v6MgkiIcuA+j60xTl5mkp9AGC6DCa/aisT0gWkR5fLbTS+0zkml2EHRXKZ4Qj8iDbuq51DMBdYF1F6yIh5i5qEqW5hmXjHUH9TD4wdk5Cn5LZlNyUROmPj0f2qDF1VFivD9YstCe2cbgG3bScIxRxXUccvfRV8vRGLEqnsl2ACU28i0sHp4S6I7OI6I7SsYbGXngYwbcCcWV84FI7OSeOctuQ+D6wWDmoNhf8m5RbVxBMmvMwuSdTgCc2PSanxyTIoNJUTJLgu3tn/FlSjTOiiibtAxB+T6Aw1AWhyelOwfHpG0eRX0acg74Ax9AdazcqP6PfMuv1rvbKI8heT3V7KCSFjM61a1Xv1biXp6X42LksaakBPfH/uX8ovk7kfwqW7hSKmrZBcd54jZarEQhcooaTXX4Oo4/6FeAWuQJdkOn1YCmLgBLGRTXkM2rVRxi/vErB1vV7SntMZ/93d8NM00ITXEREH8VnCoMX/z4oGfHAeE346ileGKuH6REc4M/FjXwD4vThB6a8cNr4vdaHw9+r0DtQTRebrJLhbkUZHbyYO+oWGHfAVl1GcJlrb9Fkm4UIW0lX3emzRV+6QpChIOhokgTghy8tA9z8395UoSF+Zi6wd+DBGOVs/gjPmzYc7yLUUiMaotcAjjcV6H9t0zqdI++mldlsQzLZBNqEgH8fGybDB8CeaqgfSCU5nKQXUMpVbXZ4YI0BI3GBd+DugejKlN1YQ2FwKDEaqC02F1pw/e438Odp3bQMrlRkJ3kwRhdL4aY///h0tLNJaRfy4gHIgbPfX5AvVkqmZhtOE0dTxYBsOMUlzGZyiBOFzflKu2FcC93yHbR5G/I0NcbKXsYTOVJ7D+IaAR0pm8rdPmzRHWEK/pKzEhvfUAVE4p/HhscKOAjcavYON7n5nKGw9R1n6y7U2gcXo1jM6iVqkeMcdU4Bhjd91Nyat33xITGeUPttBzWvpz6xhZopfdpyiWh3PUWZLd8yxzEzFqDtBLBZci6xZHA6644DKTyFJbNd70O4J4KeBVc2dgQ8AjEo9edajDZVMdNCUhmQcQzcpSdvJ+lh5an2kMz22iot0aS447XBapiMuXRPWo3n+x2IpUGcNtcYUAwMNr0Eoq+fksjWKtDfTpUU9/UAde9+g9hndkYIFLo7WYpv3ODtCIKc5WUIOdzr5QoUmIiRaRSqOpkuqliqLPW8i+Hvv15M0ksXlYK5j2tjbFzvgoviYsX+LSe7Bs7Gt6MN0inmUJk85xifA1XF9c94Z7JeKQ0HE+cuaHtojAxMkPBwKxbF4dqChaRjJypV6RMI8wAIgcBzmwJwuchMB9be3gKJ1hYCCZDs+8DAvCOzAt1UjsX2abPpUsD0Erb/m5xbn3qb49kt/VhQ7yLe3779yw9P6IU0On3nY0PZyRIO6rlyj5pVNjd8yeldEKWDRi5SqirXVbnNpDF+z/KH8Q4aIhys/9pEe8qU07DGKJy9uMv7AqOvMfp3U+uluk2MDxG2HKapx86LTPPDG25sT3H0N8IicL2FaqHkxZ/DE5Sdcta3pgJv1X89FP66Dg74vxOrZN2W/VWEZY93OGQzHYtMUl0B3Oyf9ZHG2P1cLEq9rAcCQ2L2ehLask5cOvQjjHXxPqHAdMVyjVHZEyzCkAeal9tnxmyDC0XKX0xU1VUw9j9d8Cs+weLEOlU0JYYMXy8EcfRsQXkJ+eiZJYgtIUdNY84ABsPknER37ZoGLfPiOjlgS/vI+MZeJUtuYtGBF4H9ymg/IIjGQgKJM/vSjiXcZPin4ugJan4YWRdHhc56tvbFk2sKbRUy+IBn3wzCxOKlbCgr8g81v/HiEjfB3mfZCkDQSu95PFX84Bf2bylpHl9DFRmQM0JmWd8F7n2wpoUjgDSi8CS7JKUXLESVOvD2Dn69LZFdCVpr27wz71+ppm8G315jURY+dME0XL8x0rKsLcZDZvvhxg6h5d3ooaqQvZv2foIp8P2/+QBm4TupklkrxyrsT/Av9ipt7qhxSaqOHujLmDJ/+NAa2sIjrbZJrVt8+T0bas5pB4a1ChLDcbqSEJ2eeDhEjIhD18ZUa5RCNRdhpffe/rb7d+1hk9uB4at7icXij8TxfxbscH4XF2w+x3PBC8V3rAUMRPKO8XiTCj1jYeE7krotNm1hUnIsX1fzzeCpWRtHOGLFn+s86EmmLir80zBuvn+agqm/ebWBvWdQ5KhquAr75G8/X5u300ntAP0MF1266iH9I3Nr/hDXRjSLQSQlqJcToa1VuOfS5GH9qCodHQYLnnFe2Vaom4I/rNvmbC0f4KGkGji7B7ciG6Fvy/wV3DBfAyrXTQXqjM+d54jypEcguzmmhT4mwQcnpya6MQ7fely2JTXOyKT2DCgNXAHGeP1tLqsl3y/64dFfvQ4cJERzCbhofs7dxxcg8irrmqsM4LXYndomkEDIXTCe5EUi/eQqDRV6kFtXEAqdL5URwXWQ/VeV6dUYGkRjAxdLmEBIE1TGsRdjRJP+Qh0Oe+W1tVnQoBmyGOfPT7rhsSwzWOQCCFIWs21YQg/2gVBx482tWLnCQKTuJHgNwq6CTd3y+WUVW4dc3ECarc2rblnPmA6efmvUplWZziPDvAnBLkOt3GequkpLqmJQDuZurE6J+6NOuDMttFKG4WtgWblB4sG6x1c11vBdaT4o8I14Sy1GSRGAsw9GGSmZi18VzWF0UzgtCQR4PMyVmD/kNiXljea/HjU4mP2uNxRYYWEUEtcuL8eGV8oLnBu3lI11WzUztGcptbd0gCrFk2FbBNWjzXQCZHXo6QwvX93GA+ICYHYPnFh4JQDBdQYpxg3IDnbi946ao7jSTNfWQzGlsxx2VHsgvf2V7BHWCvBdDBUNtrsEufQgreb+458QC2ydse3mmXDX3uSHu+KCkF9VJ5PgY8FayMU44z4gdFbo0X/gNHqYq8EyjKy0PE21IqA81zStfIW4TMij3A4e4S8o3IhejeJJEtW1SLaZ486EkUMNIEnKJTIVKzJxYOXOKVQ2+KqoSUeX2HQjder2SYXDfV8GYqaZlOBIpcrgzRUmCEs8Zd9AVjWtza5Jg8yGJheEjEQhBLwQZDRqnLlC0DUYIZyEghlGCkpTXoiaSucVEisAThpd9j05oR440PvJ+H6fAuukwxIQW5GGjBaKQxriCIBAvms1OfLak+kdGSn7KT9xLjRYa6GXQr9/UiKyAEIdPDUr0Xqpah2/KtkNVO5VhNeug+cGxxzJP8cB8mGjQcxBG630/qloTXzXR3qwC8Ekb3oSZyNISgN3ITSMI7q4FKDbbpQmXnKL/dejgD5r5qkXpSHkKKYQX5+SJ4ruvj78RcJtMyzphPCkrg6rURHjZmJXCk6Cq2kqfQq/9k+8yWk4EEnLqMEQ5nepmgflgrEQOzY4U026/8r4BNm5k9fLGAR6Uk/PbZQoonE5rcl4pbAKl+ewfEIEJu/6AiUOEzWF45ePG80OfdNbske/o+gVIHaBdQNsJbjrgSHOb3cREryVA+tAp+aRUqLDKveteCSLJJgTQuqCBdJjsFi+8ptbl03ih1NCmk955P2hox/lwc5QfI2wKFGBrNnvVFdWlwef/Wk3+wuqorBWGMbCQSztGppzDo82ePmguTXWPNEeX/zTTLE21HeefcIN7+d5JjBWAw/FxX37Tvv9jIocxP4v+AYG7+Veay4fIClbAgqev3K0AZ5Z0YE4WqM39rOHyueE12C6svfqC8yMO2wSGHnV1HZHe6DZ8ZsNMF7Ld/xm9Bz6FreSZ32ipteCKyMAQ+LP4ILLkPyCvYghScbbtP9pqaLkvKMlRC31OdRHAyBcStycleutjlOO227cZhts5NYfTu2a8cyTR+iHdze2fL3WmKvf44s+QUz/rH+Py7Q7j11JIHgqtYv6KCMVxnGYiENqw4LzQeXJBQxy1Wo5VyHQXSVx9OOLcZAD2bdjEOE7SYXSVV98icABkRiWqUXVgO77lU2a1HgAO3VHSv+u62Sxc7DH8l4xN5nY3+gFCJUoMEbXLAdSAIfmFOFPeEVPiRi91WH/CwsxPRUBA6NuaGNWT5/Qk4HU7GUZwKkSnt63cIu3f4tc9f3kxmi5RFGFxeYdyux64acgSfIsNqWoZx50ub/p262Hz7qulCcsw3MT31weAe8oKkbMmHayrC49ahtzFw0gYjdbLBThrDjFBdBqk0wN/hw0vlO+l/J7pIVXWhK8DeZfKi5seTXciwMpAgynXVoEthyopEssOamj3xTD5op40wbda7R0Q5fXkti7jazU4a6UUzeJIhEG5IwHUgKFYAByizbeCw7945Q7l8+mBvxsfF4j7UZu18tlJv7yd+SLaT43i+CzBFpm9U7isZHokIJl2DxRXXqasI14EDLHKmBmDgv4wD3z7LlJ/zgogVPRCLZchEsQeSr4EdLk7fiCf0CTtS5WiAV26aJ0I2OmhEU6LXODUM8w240wh6qGjqiirQarERzlIWkygLepsMD6xBs1DIRwmxdQ8yIkwI5I1UGVrSODWPg/XkgGjFp9p+beNBDI5qiBVgcbMJyKLMkrU7STk53iioMlmcmuh1K0CpPMItvpT03utjn62xH2qBXYwnLFGXydpPX6X41JF3tJwv0c/EMvSOEIzsEGJHTpEhEYSKqMoHGUxoSEmt7X+5l/qtFsOwyoJjKa9FSkzHgfuWHwzaJwjvAO2kWChGDHNBAX7SAwpCkn5DKa1nhStKDofQnch//Yt9BdfkX8lih80B2p4D+TLiqmcPx0xYkoPOB11BIPsRn3BT1xDv2viZEGpI6wPbh06KH/qm8mltGmyFnNrvH3RmNqgACMR+5aBNteFHfnZtn2fgOQuiKDURrNDQKN9+2Ei8UvKti4g+ij+1xx3THi5jUi1Y87jUR+FILoDR4lb+EoNxi/5BA1Hpd4VnmHeKWHyYFlnTZX9JfQw3slM4Ov9kutK0A+yVk6fWsm0NQzXHD3ZZRQWXojwxDNfiuyqtmH0Acw7RLFzt1A+LXayo4SaQDg6lfmSnVoZR1Ue6Rd/LZQixO7vYUVuki6Nae/sUJC19ostM02luBFucrFD/tNRBDvUfQORxDUVNTzzGaaLw5CVxbJF17BUQT9w0r8EsioIt4DbZufu9+4IQus/pccKDsIjXo7y+1OBFGtxxcxOT7TR9kSdSBCleLJj7IwMCOOmFdw1wab0S5fqRfoQQuYGKcP/Nyu45xJvg9W3KeSxjvwUyP+m3B+C/jzGtx/cS4xqesevgvadiYNj/zRYclT4x6CDlOlka6oFGRHvwj63623OoOufUWD4jgCuXt9iPlGEHSzk1Xr8j0t4kwMx5PV5SrELZPtviFTEwVbPVXG3aQoo9pdHa9eh+9vlmkW9EoKkX+FiqxZFfTghCAtflYfaVQVRih43rtlW0GYMEr1ZYQVkMMOqfEsZ9RsYQOr8YBNryxYpUo1HTB4dfqZ8gQCxbCfB6hlc4f4HHBn8DTdvXKPXJIlSJdZGtYWy6ssMK4CO5nawyGd+3E5uHkS6QEwnxuO4vs+/GYhMdfNQoZXtzo7+u6lzY47S+yGQyo862jhKAVLry02RERuiTe1ZZe8c4sBJ1Zv3Z4B082inIuhZiRe43KTXyXfGZyQJlDe0LuMbJDkNtrSu4lBXKliVPyHEMqd7MVpOu3OMs8ATu7wrsgOVscJ5QMRO8qdcXQswgqaZl+ahbmQvUdfuDFC88a9RHT4tUgSTfEu4uWQA8TzRobvVLjfMGuS0tW4pwRxf4+nxwajNtPwq4nJc5ZABEKhYtRO4aDWosZxDlIAUrfL/bHq+eNK4Y7XD0O7DXlSKbQrPIA/c6P3SKoESlCjhxlsHCgTFriEN8Jrza09DW9TCgfsfLcLuy59Q3Yl2yDjVIaknwNyoMm3+tc7jc5YFbp+3Kr+PSQhutDJgQz3AGuor1rboL4pCRiCTuPk3B4+6E/KABCnLVTHJZxOfelvwW6oFDZnKMV6g6FVtHIWVWdrkV+okmgYvT42IOeqXXdMaghaDTA6zFH+9XsiAcDw7Cns2rDXQ3o7U5ayusY1MOh+3didoMVnTaDcsEW7slLrMaWuxqv2BIBcluS7zjCKxeNTXmAUzFK/SBZZ7ESiVfmrBNSJINHO6YpeZAwWCq3xn/Xc+0UWZsrJLyL0eeeeLqYFSIyDMNPBgJ9jDJfFm3FahNSgbGoJKFUTf9obiQsiZKYOUaXWDmzx3Gwt/U6sVSVnec6D4DMyILJLaEYKjjJu0FdfQR/YRidyhOQnhvPQhXBRPQwjMAfGLs4wJS5aT+WgdzfDJ3zijvO/1ypoRXVRIdOer0fvrnaGxsh331+VIs5gYsLfbGIbKp4KUI6xEdG0W1gd8MQQWhWX8veMIgiWmLVsmpYUJenRNoGbCBQrZEMpBl41s0AvOoP/DzZi69nEOrVz3o3sRXDXzFfZjnN2EMyxv/R/M4Zr8jVGop00XskcvsksioNff4aCQTlTzVHuI3lFiBV6e7q9HqRZsdnU6fNF6r1udsSM398qdoy5wBe8dhjBuq5a+fqpj1ydxzecQFuOicJShkBwdT0hQ0JHkVlSvqU/I8OidJVF3vIKaefDqjOE/Ceo8ZkInUHi0snA2xFRPszxYfF+efEEdwMqR8XFGv1eiH1kNjZkXe9phpeo88OgApCw3aP3lnM5hTZQzl5lFe9VLA0FNioQLUGORcQK6jIGxom0W8jMkv/bnHbj20mHgGgXxErZPhsEAmmHmcTFKOpmo1IM8KIC2wxgmVRWzKF57nUQjeOdOPrNpPkffnDcc0nLn7Nsse+jSJOYIBmUw8UWw9FKacJwjNEVILrzkY6k7wyU/K/i7nqtBffOmonyZxL7DanZmPO03Jb4ftOTNSXxoLY5PqGnkdBVWyrzAzMUwdLMPGR3zfmuB3Vv26J0WmEI6Uza6c/uldr0vhqhxb4AbD055L+xO4rbZDmmI55arjRBsI0TdZTfDkA0UzF792aLv0rOGbARZsQfBeMYm49zsdMtgkV+PEZSj2wklbFk5whxzv+g2dLclziyKg42RWY9bP3ORJXbYFfqhqfJeDvh/DQCkMeRkCz3eKAPqD1OZ3lzm2fa/dmC2LQRWwTuiCT2PgeDxJY/vHc1u3bFPII3okQpxOfAv2kas5xfPF7BCaxgUTEtqf/f8/zQko3C2OAn7o7hy7NvJeK0pzMAqL/6IWM6Svy4PDz6l6R0Gj+I/rg2QIhCTiYDGPS528CU+fKWIzMggqoKPULGJ7U5cGMoPApKRjWtsk7hy5qkXJEJV3GAkg8aGbzXtkp9wbm/pfkLnewZKm2H25TILSJIbDFGeSXHaE3kAQ6BJlfx1vLNsNqI2LbSluDF8MymKGNDwHyyETo4fxumZ2SPV5zMFkFwSE4WnGwlp/ixo1RZpZttpZBItg7YlMxA79WcUCCivt9egySg+NGwmMSav6XU81VaNKx6Sra2T3MUZHdoN8hI9+V8h2A5i0Oo+8WIPqgqx31KKMTwY6Aizzu5zz+ABQ7MIor2iyINEz6Lbm4n1TBBWKZDOG/TsKJOKNEeXnhJSrEjYoTYSEkHxZC4n1rJPikoHFybAYkwBQ+nhLihfQEUpVcSFtFWQmwnnGQJryLf2clvpiZRhYMM3BLNuqCDciSChifMLao1Fc3pC6WaZ/Wdn6/4hGHnLKkj/qBCZkiwL64LjH9v6z3jPweKnNmBgIaLqR6LSdz4MjNmmaoPHvdqVe5uxdJBlgVz3F4/wAcyJJ/ANrPqre/LFEC9tlY3Y8l0/9y0uMUSToVeI+oW3pdckZxIPUka1y15clqZ3EAdobOiSH6IKi7qndrkbRE/AWDo6qAfcDMF2Y+o1J4AkVtgIXgYgE6fukn4mDRdOn00QBygXptb/iUD/fYGyjq8edO0Ru+UfuYTt2uLRQw/m3SbBPHeXI5vjMgFV1vKXw7evhoQrFGpAOFR5FF4/YQYWgf64O9YtdWokS3dhkJf0JIf+N/u9M5ZSHeOopTRK/LWBlURvKxKeI/QYcwZuJCtW1EdVdONlzk8Jsfo7yf9sZpeQ95Cyj8ViUnHdukWSqs9QMeIQEsp6+iZYh9MNc3nvEKenfRKZNmETTpfam+GU1NnYRLdOAz/Ae7RY71PwIGxrLRCwskjZmMlaGsQalFkNIHOr6GyAyFVerCk1QjUWOWW9Cgg/JOfINiuMND+ith9kJwuyn9JGX/aB663xwFpy0DTS6WNi/XAnndTDd9MOyLxIk9EvRZ9x+hR8/Yig1qgrpDl/N2YEhEUTPe/c9UTKdh/JF6gXIzaZSnV3Fjm/iRtQg4gLyXq08q6/NOh8xyJYPuG49fCCX0Bqn7tV4bpiPI+6SLbhSvHS4VvHljmw3mOkyQK3JQG+7sgTZ/n7qieaMGDYEmQ9cUk2B6QJW0DYXAfTmPYjMrdxQxK+Qdf50uFaTuL4AEo5MdRUOBZNIjYkFxZjJjtErkketxrtJlyFpS34WbUKNl5h2VpRS8aksRljXNqiBEaA6lG59dNuZmGVwAOhZHoQtslsP+WBLMF7ItUmlT3AGkj1Ny8/GssMd/sGVntXkDOLpKzGCPEmJ8WZ/sr0zrRMhflX4q8AoL4gMkb87JT8Xk1JMPh/erqXwjGMud/0oz0rm+Igk54N6fAazkzLgsmbO3Iwd5R1N3DirzXQzSNwLE63MRCxfzQV6qcJ0vXiJaz0OU2m3C8fiwBuFBajaPryatqzPtAPR0HBtlt64lbvJg/mqgSd+ZAOsYA0TW7N/4DI7o+zQEELzV6BFeR5yDHzanzqM54OHlH7lAHKXnIFht0x4MGH4SW5jbW5uQjnfjPEtB8PHm9xcoOwBDOmCl4okkf+G37BKFzxV5coa43bvozqyYG0wY3iP9bswY1DQ6KC4K/0uj9Eo27RlEA+bsUGH9NNaZw6jDw89wbF8bsQi+w/k9do6i2LI/+kpYI3SS7jyhL/i2sfBEfwzzwJvKPf/3AlTdrRB1ma1ltzMKoumP1zM1gyd2Wy5CsdSUsMaGXtzL3lVLxa7zCaPWgoEjRLgOXsiWkRCxteAI0nOtkn0iBWNn6BVDj7wVxglwJuTn97URt2v86u+/ZkqjyBFV0AmLWnytMlqlbt0FbBqlgn0m31X3FoAVVUacVwdGmBEwOGHPejFxOe9q/Av2ld42loP4ci0aJJRoyfmRISLWKdy0hPycEIeTpxVd82hXtEBte3YIi6+a95Lyuj3OHB5kuWSRE7k5fpslK9/JSW4Iq6B8zaiZjPbYNa8HojwiN7wdXvluKyqhoGlLGt6DwwwyDFVEuSVM4koux2AtZBW4KfpBSpdI1uvA0zsxKBHMfS6b3EGwaGi5OmSkyqzIdH6tZ7yLSBpRyh+pMiGx6JunuQ/TPJFFXv9htWf1Ys2ot2ir4BNugDPMVh3RLQfK35rthjj2asis3nSge1vMCLAJM3VLIxnks53ehX+lOMjDIPnma6CKPIOgeFj/Ksnnwhkhjaz4aoUiSFpAsT5RZ+8FwfSdmF6Ls8oHFsSfeskJ95OklJSQd79KgSBS4ed9R02PkTEtqaJ9SQgjVCQ5LYvdu570FSGvqytYxTk0ys4of/uLa+qmYd+X9mL6F2mw1JFfGZz/Kv9Y6RI0GEFNHFIZUG5vexKVg+AtSaIOsCCJkrOh4v1zyxR3+zREWpowQROefhc7rJzIiABrNXUaDIJuZ3g3R/sw6lAi+u4fWCeSqnflJuh5BZseJFMuuaA/tD1YU9Kgs/AiX+VnDrbCBrsrlgW/667U2O5AajZe/ML7vhf2wrhPUBfSlZ28A73hKYnjWjhAOmlaF9nFRin0krWUHc3qIUPbNrl2NXcSnfMZUdseUFcP6vwz41Q92lUUgNj1eUNE48FoBljC0Bf4N4eF8bFy/3Z71VqxcmdOVQNKf5rTyob95nyHf2xxZrYV90D+gH5omOTg8TVYGsthRNL8BgLDp4B+ufJ5y4BkMj2yXq2gGucVNftbmGRqEH0eps+ceDd+jsYeTbs7wP1c7kbayalM2H9hG4Vnu2XJsd7e9x92ihWriMETwCkiRaoOWxXPujFZH3ML3a5VKt+3gGU31h4C/mW9r2lD0iAOU3lEMC0TH5PUjQSeWMNajBHfFjaIQt+VL1tjB69RfFDC01cs35Ip1ZBUoHydYeSEJSQ4FRnHLFBbk/R0V0W2EHgpe1wg6mKNyE49GTg6ndrjbum/UCaosalSdee8nYuDSBwH5Bc86PuNEHqToEO7BnkMcQBeiyW7dTd6EaQ9oXUTnDw+7MEyiZQozTqEBd5nxL8ONxBtmlQR3w0Jr7my8YSAAH0qLiOSqHVEF7Y2eSTnjcsdb0L8zL8cbaxQ5rdYuPvdR0g2dN44JxJ6pPa6zL8vHFNqQBggbKK5t5Ah5LaF6rMJKOkB5CfNR9RtHfniFmgeZFuhyxcIx5TatielWEBJrfsPJFa59PmRNSMqPEmaRCMPekW2024bTrTd/wUdTJBme2Neu3Cf2oE5CPDXsPnFblylG9iZMGYY8C26o/Hmlm+dfh0+UfeLLEIQEuBM57BR/yz3XmFeck0oNRknWtyWEn/00SCJkblVqIRii2SgrqBEMKH59wHQGluBQoNcOWz8zWO3pVr4Vsk2jZJRzSFnvRwTipHXkeUJpA20E5bkQ1R9YcbG+VwH6CkDgQIoJzve5Yx3qtkMweRDsTWpSLEWdICfJNFj4pSAFb4TfeHfd+daIqm6AdMBCg+wJ06GHaViSjqLNWEAUv5dv+AYVa1bw7B+NgAlLtRm2OC6NfTxBgrtAd6s/GyPl6AOfPIU8CWKLsomJlcdm9mSJV8u9JatEl1qV8ynVwMhcRIXlxOVVpVPTlsKwbRUxNGz8ic6mB6QXVWTsAvR+QVd8R9aJuvDueESJnlmc4oeANKFYqoOjH/VP6i9IYQ132JpB9irCk+1y4OCpWnczO19Vg7iSmhEoba8NKzkKU/jiwBBdRH+TOx06MWaFVW0ROTY5DvHOs0rxdfYGNRTCqhYEr+K4flgTVZXjJkeV2pnVvaTwTfExzOkl32Nnb9k/26VbVEHHQSgsHhrEsy//LKL2+wsJ38+st2nDiRjsg6uxQTGlCveD1CUM0RIXTxzOZwlSv8q1jaefTG6NNTTzJfVjYR+I1p5JtZXhQ++IfbHkwE5UuK2xvGT+1LhLGLkPhj2PJYeFrtL1xXKQ7cj6HCPrbBRAAR0SMlzXnw1916MtRhSTunuCdIszuzowXN94ZraKTqetZkNGez07NeceVwyT0FyV8Z2bMDctz5b5POe63wRaOrmc9ih4jL426Pu2FVWMFC44QiSAtjAEXCO/3LrjwdiXzIUdWEwhJp4R0Z/kUshQ0HOUdm8F7vcM1fTmXMEd9l+mdNSoQ7beKH5Y0gm2LIALohqtJ2S7VbbfXI2+hMKpEtzRJsTc6lGLoETVhD5c7QDJmXfkiwGb+A10d6ePClJAo1KVjSO23ngww9+sN4Y6b7NUa9y+o61FUDL+OJnRrqfRZc2JE/lGtbqrGUAM71+nKb4eOt+6jr5kVUbR/pRT6EaFvqDUt4DPIVXl47YFuQeYBW3TuB5juKRmhcXDbQM9G3h/0TBwg850djSjK/oVxAEf7Ajn4TLqHubEY8IjBaFLsWViwplgppNqUstMuC3LUPZKmnek9aU6VcYyRYml+NaJEoaLg1xrMNIW3Umr1stiS+K7DCPGPABqhCbBvIYpQUZrptFJB3f3lhCnvkaNWZKLAuo2prW4c8Jgx0hvsxuUQgPkiE+o6WSURFgPfVxxkH0BAuN2UD7sGwXrUfXRnW5rDEJ0t8JBDv/LcLHtr7rJlfMBNBsJ2psm9uNxlMoFTaNfOEg7HqV9oih5UfKGJXajbgx4j29agWMC+yhb0CUyyN6lqEiA/ixFyAbDrhc0kYJ/DWzysmD2Y1O5BJxIeFZHN8+befNB90GXB8yKJ22iKUemTz1uui1QcJ0SO8UvNZ5ME7z4duatk40v4/l2HXuk=" />
</div>


<script src="/js/dnncore.js" type="text/javascript"></script>
<script src="/js/dnn.modalpopup.js" type="text/javascript"></script><script type='text/javascript' src='/DesktopModules/Exionyte/Menu/1/Xeon/Scripts/menu.js'></script><script type='text/javascript' src='/DesktopModules/Exionyte/Slideshow/2/Xeon/Scripts/jquery.cycle.all.2.74.js'></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { $('.slideshow').cycle({cleartype: true, cleartypeNoBg: true, fx: 'scrollHorz',timeout: 6000,prev:   '#Prev', next:   '#Next'}); var a1 = document.getElementById('arrows'); a1.style.display = ''; var b1 = document.getElementById('bannerPane'); if (b1) b1.style.display = 'none'; });//]]>
</script>

<script src="/js/dnn.jquery.js" type="text/javascript"></script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3a4d4da635-3815-4b70-912b-f389ade5df37%3aea597d4b" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="UGgAEVSswVCuyRe5GfQZQXyKHjVGEWj1psEY1ikYnFJ1IsWGnvM4/DBaMKJcjzEYkDLQS1yGPbg3NV457Q1YbUqCezUAx9qz1L71u4OIDaj0hY6uu+RqaPxN6v3s8qqkZUF8wG2ggVSH/nz0ZMBp4WhaaojCVbqjMxTUhgk1Jp5HXeAtC5V/8qvJ3T5XAhRKcBNDlmOSZ1ASzfB7faqZYW4nlev/X/vRTT8KVg==" />
</div>
        
        
<script src="http://cufon.shoqolate.com/js/cufon-yui.js?v=1.09i" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Futura.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Font.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/jquery.pngFix.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/general.js" type="text/javascript"></script>
<div style="display: none;" id="message">
    <div>
        <h4 style="text-align: center;">
            Sorry, This page dosen't support Internet Explorer 6, If You'd like to view all
            the contents properly please upgrade your browser to IE7, 8 or 9. <a href="http://www.microsoft.com/download/en/details.aspx?id=2">
                Click here to get the new version of Internet Explorer</a></h4>
    </div>
</div>
<!--flash section-->
<div id="flashPane" class="flashSection" style="display: none;">
    <div class="flashPadding">
    </div>
    <div id="website" class="flashContent">
        <p>
            In order to view this page you need Flash Player 10+ support!</p>
        <p>
           
        </p>
    </div>
</div>
<!--/flash section-->
<div class="skinLayout">
    <div class="skinWidth">
        <!--header-->
        <div class="skinHeader">
            <div class="skinMinWidth">
                <div class="logoLayout">
                    <div class="logoPosition">
                        
<div style="float: left; display: inline;">
    <a id="dnn_dnnLOGO_hypLogo" title="North Maharashtra University, Jalgaon" href="http://nmu.ac.in/en-us/home.aspx"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/0/logo.png" alt="North Maharashtra University, Jalgaon" style="border-width:0px;" /></a>
</div>


                         <div id="dnn_logoPane" class="DNNEmptyPane">
                        </div>
                    </div>
                </div>
                <div class="menuLayout">
                    <div id='menu'><ul class='menu'>
<li class='current'><a class='parent' href='http://nmu.ac.in/en-us/home.aspx'><span>Home</span></a></li>
<li><a class='parent' href='http://nmu.ac.in#'><span>About Us</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity.aspx'><span>About University</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/misson.aspx'><span>Misson</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/universitysong.aspx'><span>University Song</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/informationatglance.aspx'><span>Information at Glance</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/achievements.aspx'><span>Achievements</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/vicechancellorslist.aspx'><span>Vice Chancellors List</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/annualreport.aspx'><span>Annual Report</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies.aspx'><span>Governing Bodies</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/managementcouncil.aspx'><span>Management Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/academiccouncil.aspx'><span>Academic Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/senate.aspx'><span>Senate</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/deansoffaculties.aspx'><span>Deans of Faculties</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>University Campus</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/aboutcampus.aspx'><span>About Campus</span></a></li>
<li class='child'><a href='http://nmu.ac.in/kamc'><span>Khandesh Archives and Museum Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/centralschool.aspx'><span>Central School</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/campusmap.aspx'><span>Campus Map</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/howtoreach.aspx'><span>How to Reach?</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/extensionactivities.aspx'><span>Extension Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/researchfacilities.aspx'><span>Research Facilities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/mous.aspx'><span>MoUs</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/home.aspx'><span>Satellite Centers</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/eklavyatrainingcentrenandurbar.aspx'><span>Eklavya Training Centre, Nandurbar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/rcp'><span>Pratap P.G. Research Centre of Philosophy</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols/en-us/research/labtoland.aspx'><span>Pratap Shashvat Adhunik Sheti Tatvdyan Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/mahatmagandhiphilosophycentredhule.aspx'><span>Mahatma Gandhi Philosophy Centre,Dhule</span></a></li>
</ul></div>
</li>
<li class='child'><a href='/LinkClick.aspx?fileticket=oqDHHmONXsE%3d&tabid=3881&language=en-US'><span>RGS and TC</span></a>
<div><ul>
<li class='child'><a href='http://apps.nmu.ac.in/circulars/Bcud/29-09-2015%20Submission%20of%20Pre-proposals%20under%20the%20scheme%20assistance%20for%20S%20and%20T%20applications%20through%20university%20system%20of%20RGS%20and%20TC,%20Govt%20of%20Maharashtra.pdf'><span>RGS and TC Schemes</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Information</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Academics</span></a>
<div><ul>
<li class='child'><a><span>Schools</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/soes'><span>School of Environmental & Earth Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols'><span>School of Life Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/socs'><span>School of Chemical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sops'><span>School of Physical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docs'><span>School of Computer Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/domaths/'><span>School of Mathematical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/udct'><span>University Institute of Chemical Technology</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docll'><span>School of Languages Studies &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doe'><span>School of Education</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doms'><span>School of Management Studies</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soss'><span>School of Social Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soah'><span>School of Arts and Humanities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/schools/schoolofthoughts.aspx'><span>School of Thoughts</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/dat'><span>Buddhist Study and Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/smcrc'><span>Chhatrapa Shivaji Maharaj Chair & Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/mpsrc/'><span>Mahatama Phule Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/svsrc'><span>Swami Vivekananda Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/grc'><span>Mahatma Gandhi Study &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sgsk'><span>Sane Guruji Sanskar Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/bcsrc/en-us/home.aspx'><span> Kavayatri Bahinabai Chaudhari Study and Research</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/nmuccc.aspx'><span>NMU-CCC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/phd.aspx'><span>Ph.D.</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation.aspx'><span>Adult Education</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/eventsandactivities.aspx'><span>Events and Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/coursesoffered.aspx'><span>Courses Offered</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/tbc'><span>BACECC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/committees.aspx'><span>Committees</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Administration</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/registraroffice.aspx'><span>Registrar Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/administration.aspx'><span>Administration</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/lawrtisection.aspx'><span>Law/RTI Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/faooffice.aspx'><span>FAO Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/examinationcenter.aspx'><span>Examination Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/eligibilitysection.aspx'><span>Eligibility Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/sportssection.aspx'><span>Sports Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/scstcell.aspx'><span>SC|ST Cell</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/constructionsection.aspx'><span>Construction Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/prosection.aspx'><span>PRO Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/approvalsection.aspx'><span>Approval Section</span></a>
<div><ul>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Online Approval</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/computercenterexam.aspx'><span>Computer Center (Exam)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/universityofficers.aspx'><span>University Officers</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=ZprmAC3RMhY%3d&tabid=1350&language=en-US'><span>Phone Directory</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/rd/functioningofrdsection.aspx'><span>R & D</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/developmentsection.aspx'><span>Development Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/affiliationsection.aspx'><span>Affiliation Section</span></a>
<div><ul>
<li class='child'><a href='http://affiliation.oaasisnmu.org/'><span>Online Affiliation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/affiliationsection/licreports.aspx'><span>L.I.C. Reports</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/research.aspx'><span>Research</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/iqac.aspx'><span>IQAC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/avishkar.aspx'><span>Avishkar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/bsrschemeprogrammee.aspx'><span>BSR Scheme/Programmee</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Student Corner</span></a>
<div><ul>
<li class='child'><a><span>Academics</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/admission.aspx'><span>Admission</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/syllabi.aspx'><span>Syllabi</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/questionbank.aspx'><span>Question Bank</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/academiccalendar.aspx'><span>Academic Calendar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/papercodelist.aspx'><span>Paper Code List</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac'><span>e-Suvidha</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://exam.nmu.ac.in/'><span>Examination</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examtimetable.aspx'><span>Exam Time Table</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlineresultscollege.aspx'><span>Online Results (College)</span></a></li>
<li class='child'><a href='http://182.56.2.247/nmu/'><span>Online Result (Student)</span></a></li>
<li class='child'><a href='http://www.nmuexams.in/NMUEXAMS/LoginScreens/frmStudentLoginPage.aspx'><span>Application for Photocopy/ Verification/ Challange</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/resultsofverificationredressal.aspx'><span>Results of Verification|Redressal</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/convocation.aspx'><span>Convocation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examreports.aspx'><span>Exam Reports</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlinedegreeverification.aspx'><span>Online Degree Verification</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac/QPCourseSelection.aspx?ID=660'><span>Previous Exam Question Paper</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examination.aspx'><span>Examination</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Facilities</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/financialassistanceschemes.aspx'><span>Financial Assistance Schemes</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails.aspx'><span>Hostel Details</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/hostelday.aspx'><span>Hostel Day</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/boyshostel.aspx'><span>Boy's Hostel</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/girlshostel.aspx'><span>Girl's Hostel</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Services</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Download  Formats</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/examinationsforms.aspx'><span>Examinations Forms</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Formats For Colleges</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforemployee.aspx'><span>Formats for Employee</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formsforstudents.aspx'><span>Forms For Students</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Statistical Format</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline.aspx'><span>Student Helpline</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline/studentfacilitationcentre.aspx'><span>Student Facilitation Centre</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studentwelfare.aspx'><span>Student Welfare</span></a></li>
<li class='child'><a><span>E-Resources</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/eresources/ejournals.aspx'><span>E-Journals</span></a></li>
<li class='child'><a href='http://nptel.iitm.ac.in/'><span>NPTEL IIT Lectures</span></a></li>
<li class='child'><a href='http://apps.webofknowledge.com/'><span>Web of Science</span></a></li>
<li class='child'><a href='http://www.sakshat.ac.in/'><span>NME-ICT</span></a></li>
<li class='child'><a href='http://jgateplus.com/search/'><span>JGate</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Useful Links</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/events.aspx'><span>Events</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/tenders.aspx'><span>Tenders</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/rti.aspx'><span>RTI</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/circulars.aspx'><span>Circulars</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pressroom.aspx'><span>Press Room</span></a></li>
<li class='child'><a><span>Job Openings</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/university.aspx'><span>University</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/collegesinstitues.aspx'><span>Colleges/Institues</span></a></li>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Apply Online</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/sciencepark.aspx'><span>Science Park</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=2wk8wKpV9MI%3d&tabid=1088&language=en-US'><span>Citizen Charter</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/publications.aspx'><span>Publications</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pbasproformaandapidetails.aspx'><span>PBAS Proforma and API Details</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/otherlinks.aspx'><span>Other Links</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Us</span></a></li>
</ul></div>
                </div>
            </div>
        </div>
        <!--/header-->
        <!--banner-->
        <div class="lightBlueCenterRepeat">
            <div class="lightBlueLine topLine" style="z-index: 101; position: absolute; top: 0px;">
            </div>
            <div class="lightBlueLine bottomLine" style="z-index: 101; position: absolute; bottom: 0px;">
            </div>
            <div id="bannerLeft" class="lightBlueBannerLeft">
            </div>
            <div id="bannerRight" class="lightBlueBannerRight">
            </div>
            <div id="bannerCenter" class="lightBlueBannerCenter">
                <div class='slideshow'><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage5.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage6.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage7.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage8.jpg' alt=''/></a></div></div>

            </div>
            <div id="bannerPane" class="banner" style="display: none;">
                <div class="skinMinWidth">
                    <div class="contentPadding bannerPadding">
                        <div class="top-cols">
                            <div id="dnn_Banner_Pane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Banner_PaneLeft" class="colHalf left DNNEmptyPane">
                            </div>
                            <div id="dnn_Banner_PaneRight" class="colHalf right DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/banner-->
        <!--banner arrows-->
        <div class="skinMinWidth">
            <div id="arrows" class="arrowPositioning" style="display: none;">
                <div align="center" class="arrowLayout">
                    <div class="lightBlueArrowBGLeft">
                    </div>
                    <div class="lightBlueArrowBGRight">
                    </div>
                    <div id="Prev">
                    </div>
                    <div id="Next">
                    </div>
                </div>
            </div>
        </div>
        <!--/banner arrows-->
        <!--slider panel-->
        <div id="toppanel" class="topPanelLayout" style="display: none;">
            <div class="topContent">
                <div id="panel">
                    <div class="skinMinWidth">
                        <div class="top-cols">
                            <div id="dnn_Content_TopLeftPane" class="col1 left DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopRightPane" class="col3 right DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopCenterPane" class="col2 DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Content_TopFullPane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottomPanelSpacer">
                </div>
            </div>
        </div>
        <!--/slider panel -->
        <!--slider nav-->
        <div class="fullWidth">
            <div class="centerSliderPanel">
                <div id="toggle" class="topSliderLayout" style="display: none;">
                    <div id="close" class="open sliderOpen">
                    </div>
                    <div id="open" class="sliderClose close" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <!--/slider nav -->
        <!--content header-->
        <div class="skinContent contentTop">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="contentPosition">
                        <span style="color: #fff;">n</span>
                        <div class="contentPositionLeft">
                            <div class="loginImage">
                            </div>
                            <div class="displayText paddingText">
                                
                                <a href="http://apps.nmu.ac.in/Login.aspx" target="_blank" class="lightBlueContentLinks">
                                    Portal Login</a>
                            </div>
                            <div class="loginImage">
                            </div>
                            <div class="displayText">
                                <a href="https://login.microsoftonline.com/" class="lightBlueContentLinks">Mail Login</a>
                                </div>
                        </div>
                        <div class="contentPositionCenter">
                            <div class="paddingText">
                                <strong>अंतरी पेटवू ज्ञानज्योत</strong></div>
                        </div>
                        <div class="contentPositionRight">
                            <div class="displayText paddingText">
                               Select Language :
                                    <div class="language-object" >
<select name="dnn$dnnLANGUAGE$selectCulture" id="dnn_dnnLANGUAGE_selectCulture" class="NormalTextBox">
	<option selected="selected" value="en-US">English (United States)</option>
	<option value="mr-IN">मराठी (भारत)</option>

</select>

</div>

                            </div>
                            <div class="lightBlueContentLinks searchText">
                                <div class="searchTextLayout">
                                    Search&nbsp;&nbsp;
                                </div>
                                <div class="searchImage">
                                    <div class="search_bg">
                                        <span id="dnn_dnnSEARCH_ClassicSearch">
  
  
  <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" onkeydown="return __dnn_KeyDown(&#39;13&#39;, &#39;javascript:__doPostBack(%27dnn$dnnSEARCH$cmdSearch%27,%27%27)&#39;, event);" />&nbsp;
  <a id="dnn_dnnSEARCH_cmdSearch" class="dnnSearchCss" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/_default/Skins/Xeon/images/sright.jpg" align="top" border="0" alt=""></img></a>
</span>


</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content header-->
        <!--content containers-->
        <div class="contentContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane1" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane1" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1" class="col1Home left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane1" class="col3Home right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane" class="col2Home">
                        <div class="DnnModule DnnModule- DnnModule--1">

<div class="containerPadding"></div>

<div class="overflowHidden">
    <div class="lightBlueBG">
        <div class="style1TitlePosition">
            <span id="dnn_ctr_dnnTitle_titleLabel" class="style1Title">Privacy Statement</span>



        </div>
        <div class="style1Arrow"> 
            
        </div>
         <div class="clear"></div>
    </div>
    <div class="spacer"></div>
    <div id="dnn_ctr_ContentPane" class="style1Content style1Border lightBlueContent"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>North Maharashtra University, Jalgaon is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the North Maharashtra University, Jalgaon Web site and governs data collection and usage.
        By using the North Maharashtra University, Jalgaon website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. North Maharashtra University, Jalgaon also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by North Maharashtra University, Jalgaon. This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by North Maharashtra University, Jalgaon for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the North Maharashtra University, Jalgaon Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through North Maharashtra University, Jalgaon public message boards,
        this information may be collected and used by others. Note: North Maharashtra University, Jalgaon
        does not read any of your private online communications.</p>
        <p>North Maharashtra University, Jalgaon encourages you to review the privacy statements of Web sites
        you choose to link to from North Maharashtra University, Jalgaon so that you can understand how those
        Web sites collect, use and share your information. North Maharashtra University, Jalgaon is not responsible
        for the privacy statements or other content on Web sites outside of the North Maharashtra University, Jalgaon
        and North Maharashtra University, Jalgaon family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects and uses your personal information to operate the North Maharashtra University, Jalgaon
        Web site and deliver the services you have requested. North Maharashtra University, Jalgaon also uses
        your personally identifiable information to inform you of other products or services
        available from North Maharashtra University, Jalgaon and its affiliates. North Maharashtra University, Jalgaon may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>North Maharashtra University, Jalgaon does not sell, rent or lease its customer lists to third parties.
        North Maharashtra University, Jalgaon may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, North Maharashtra University, Jalgaon
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to North Maharashtra University, Jalgaon, and they are required to maintain
        the confidentiality of your information.</p>
        <p>North Maharashtra University, Jalgaon does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>North Maharashtra University, Jalgaon keeps track of the Web sites and pages our customers visit within
        North Maharashtra University, Jalgaon, in order to determine what North Maharashtra University, Jalgaon services are
        the most popular. This data is used to deliver customized content and advertising
        within North Maharashtra University, Jalgaon to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>North Maharashtra University, Jalgaon Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on North Maharashtra University, Jalgaon or the site; (b) protect and defend the rights or
        property of North Maharashtra University, Jalgaon; and, (c) act under exigent circumstances to protect
        the personal safety of users of North Maharashtra University, Jalgaon, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The North Maharashtra University, Jalgaon Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize North Maharashtra University, Jalgaon pages, or
        register with North Maharashtra University, Jalgaon site or services, a cookie helps North Maharashtra University, Jalgaon
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same North Maharashtra University, Jalgaon Web site, the information
        you previously provided can be retrieved, so you can easily use the North Maharashtra University, Jalgaon
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the North Maharashtra University, Jalgaon services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon secures your personal information from unauthorized access,
        use or disclosure. North Maharashtra University, Jalgaon secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>North Maharashtra University, Jalgaon will occasionally update this Statement of Privacy to reflect
company and customer feedback. North Maharashtra University, Jalgaon encourages you to periodically 
        review this Statement to be informed of how North Maharashtra University, Jalgaon is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>North Maharashtra University, Jalgaon welcomes your comments regarding this Statement of Privacy.
        If you believe that North Maharashtra University, Jalgaon has not adhered to this Statement, please
        contact North Maharashtra University, Jalgaon at <a href="mailto:support@nmu.ac.in">support@nmu.ac.in</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
</div>

<div class="endContentPadding"></div>

<div>
    
    
    
    
    
</div>

<div class="containerPadding"></div>

</div></div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1Inner" class="col1Inner left DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPaneInner" class="col2Inner DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane2" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane2" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane2" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane2" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane2" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane2" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane3" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane3" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane3" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane3" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane3" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_CenterPane3" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content containers-->
        <!--spacer-->
        <div class="spacer">
        </div>
        <!--/spacer-->
        <!--footer line-->
        <div class="footerLineTop">
        </div>
        <!--/footer line-->
        <!--footer containers-->
        <div class="lightBlueFooterContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Footer_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_HalfLeftPane" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_HalfRightPane" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_LeftPane" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_RightPane" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_CenterPane" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
            <div class="lightBluefooterBottom">
                <div class="skinMinWidth">
                    <div class="contentPadding">
                        <div class="footerPosition">
                            <div class="BottomLinks footerPositionLeft contentWidth">
                                <div class="displayText">
                                    <a id="dnn_dnnPRIVACY_hypPrivacy" class="BottomLinks" rel="nofollow" href="http://nmu.ac.in/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx">Privacy Statement</a></div>
                                <div class="displayText">
                                    &nbsp;&nbsp;|&nbsp;&nbsp;</div>
                                <div class="displayText">
                                    <a href="http://nmu.ac.in/disclaimer.aspx"
                                        class="BottomLinks">Disclaimer</a></div>
                            </div>
                            <div class="footerPositionLeft bottomContainerWidth">
                                <div id="dnn_BottomPane" class="DNNEmptyPane">
                                </div>
                            </div>
                            <div class="footerPositionRight contentWidth">
                                <div class="displayText">
                                    <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="BottomLinks"><a href="copyright.aspx" style="color:#fff;">Copyright</a> 2012 by North Maharashtra University</span>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/footer containers-->
    </div>
</div>
<script language="javascript" type="text/javascript">
    function Showie6Message() {
        var IE6 = (navigator.userAgent.indexOf("MSIE 6") >= 0) ? true : false;
        if (IE6) {
            document.getElementById("message").style.display = "inline";
        }
    }
    Showie6Message();
</script>

        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" />
    </form>
    <script type="text/javascript">
        
        //This code is to force a refresh of browser cache
        //in case an old version of dnn.js is loaded
        //It should be removed as soon as .js versioning is added
        jQuery(document).ready(function () {
            if (navigator.userAgent.indexOf(" Chrome/") == -1) {
                if ((typeof dnnJscriptVersion === 'undefined' || dnnJscriptVersion !== "6.0.0") && typeof dnn !== 'undefined') {
                    window.location.reload(true);
                }
            }
        });
    </script>
</body>
</html>
