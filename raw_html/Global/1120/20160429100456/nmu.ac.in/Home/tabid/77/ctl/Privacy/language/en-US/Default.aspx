<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="en-US" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><meta content="text/javascript" http-equiv="Content-Script-Type" /><meta content="text/css" http-equiv="Content-Style-Type" /><meta id="MetaDescription" name="DESCRIPTION" content="Official Website of North Maharashtra University, Jalgaon" /><meta id="MetaKeywords" name="KEYWORDS" content="North Maharashtra University, Jalgaon, Maharashtra, Higher Education, Academic Institute, Research Organization, University, NMU, India, Maharashtra, Research, Under Graduate, Post Graduate, Certificate,Courses, " /><meta id="MetaCopyright" name="COPYRIGHT" content="&lt;a href=&quot;copyright.aspx&quot; style=&quot;color:#fff;&quot;>Copyright&lt;/a> 2012 by North Maharashtra University" /><meta id="MetaAuthor" name="AUTHOR" content="North Maharashtra University, Jalgaon" /><meta name="RESOURCE-TYPE" content="DOCUMENT" /><meta name="DISTRIBUTION" content="GLOBAL" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><meta name="REVISIT-AFTER" content="1 DAYS" /><meta name="RATING" content="GENERAL" /><meta http-equiv="PAGE-ENTER" content="RevealTrans(Duration=0,Transition=1)" /><style id="StylePlaceholder" type="text/css"></style><link id="APortals__default_default_css" rel="stylesheet" type="text/css" href="/Portals/_default/default.css?5568161" /><link id="APortals__default_Skins_Xeon_skin_css" rel="stylesheet" type="text/css" href="/Portals/_default/Skins/Xeon/skin.css?5568163" /><link id="APortals__default_Containers_Xeon_container_css" rel="stylesheet" type="text/css" href="/Portals/_default/Containers/Xeon/container.css?5770949" /><script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery.min.js?1.6.1" ></script><link href="/Portals/_default/Skins/_default/WebControlSkin/Default/ComboBox.Default.css" rel="stylesheet" type="text/css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico' type='image/x-icon' /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/CSS/menu.css" rel="stylesheet" type="text/css" /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/Colors/lightBlue.css" rel="stylesheet" type="text/css" /><title>
	North Maharashtra University, Jalgaon > Home
</title></head>
<body id="Body">
    <script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery-ui.min.js?1.8.13" ></script>
    <form name="Form" method="post" action="/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx" id="Form" enctype="multipart/form-data">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="K9m01pPQiWJCVsiw350ndQMCAoNOw6k+wpPdgnsbC7zUcGJmplsMw2nCo7xaYD7dl+0boyNeb3nSMv+pEvGxtER+rNm4sTDUSDtyr48RB2Sn7/YHHVl4+4mAIz2NSp09sXvFR0MYuyn/ivCHpG5TlbxlWnov4dGKVoRO9SCaMKoLO8c4ebWq5d8nWgjslDozp/GMwZJC4RUC4uo2HKUxnNoU1EHS4nTwM83DDgAR/9tNV4S8QLr/J/FPpjNsBmmkIbgczx1keJam45Imd00Q5r0LAxN8SMTf6zBD90crlt/TjfcZH5Cb21ZJi1M6PdJKyk78kaiB/bhqpYGe2CdHY99gB4pxJrM5NNKOb1QyU7wupqmBr3LXNuw0iOmR962oN/w5muhuxMZspvrq4A9w8xkyB1lYr+kHdZ6x/7yDSfi/hyBDenJ2AFrG9b6MTfcCSoi82oRhI0tYk9a962bffNnowY1c+cTEEedMmYM2tY5nQVqHoPot+whnD4Cakhq0Zntix/NY4oyFKfSTV+MKxEZ1Xd6gg5cbo7U2e8O+4Zzd3ESZJCdL77bBLr3kjHI4avQgRIIHs3327LHvxpCLz6NN5IGxtnfvnyNH4HA6mY4IOTHF/ZsDyHQ+hegKLhNN2IeCxaU9e82WP5/3CCLFoEjWE/FAAo6/Y+T/zLORD7K0OcWz7ggyBavlvxrOFz4UCr0DnXBoNrdl4O4LVWa1EkWZXCbAA+MQ516sfPgaP0SIIuwTjlQ6or6MQKSWQ3zQaJrXq1vDUIW7VBkipLt4iB75l17LnudfZVDRIf1IpG9lY20s3rPfM+rpoO4QeAcOz8/wmt3Je3NQ3oCKCw0G3tBDuUY9eh3CEFTYj+2CLC2QCIV7JOl2oXnJQFA54Y7e4bTTtuCXtQSyoYpzfc23fpDnV3imhQk1qHWWmVpLQD8LtxbU3fEmU25FnDIF4V16WIX4bmgRCrxjy+GbgLoc8mmsLb1Gthp12gDp8c6i0KkLmG/IYRt8/wLUc++biiKA0p6IKf2g1FQLDHaS0Ctpqmmuok+yh33jRWW7r0d2oaVKJqjnyBMhji8tG0/oT6cVcB85ED2kbpW+GY2AdaPMuv35mhtOFK5MXMpek45uVuX/jZ4fwWp13s3tSUSlwD17/mX9EDUTV6HoHjnSkDYetLI2vCeMD8Zvehjp1RxQvC0oCOBdtUZSoXdb0kvXPk84cphn70azgby/nb4XG5FTmmhbkvhCeaPH7l0QVubOCRHYZ53EU1kFiAkUMmFAC3/+a4Ng76o4M/hZuPDojNIxojYWwTMbN/FTmolSTW7qk/BXBGZdI2CuftupIwRchXYYRsdykXodd4R0mdL/rlpIl/EIQ8f9LG79C544sXRTu2S3azZpJIJwgeeZfVQQCDohtEqS0ViRrv2F0+CcX1GUIhFo1CjnSwMIDgu+7W1kLat2fMNpiOJZqOr77pgzsiKJfQCgYLvslDsOnPA3TYyEhFRQJdVDvXvVVYArOmtMU6gUYts3dN8+wu1xGkTunXe8TADuTd5E7NheolbKf/0Ss70UHMqE1ZyK+asrDmz3ReZYHylE19CqBCtSFX2rQXxBJZiGKbScKYWHDLc6ZnLmiW6LmjDm958VhxfXM616nRof1CwgI1KZqNEmRMuOI3Te9I6po9AqvtxFJGS0A3Kxjs3Rxgn9m8sAXGFrRtfmY9Ah4k3ms8F2gwSFKUeNUzbejq9cEzARl8MFIxhjGX8kqhA8Ojv5HbtBQVa3UgKLV4Nwb94Bu3Kk2KpOqJtfFmntQUZoqIexwR2+otc5R08kN1SSyPbpSgbnA5LqwksUGtu+OC0rLJIaxtB0r8i87W62jCyrA3agNurHffppZbZzATnnKUDWDIK5/6wLu5gEqfxLEMEsM7EkOAqMBaNidXnU/77VHxQnfcBzgQ0PYCRZC+XadcswK/q3oyI1V4FUuak7uRDoc5abDs9O9zDRg7VOTqARM7OWrI5mwGEC46qDXZ+2YCsyhRjhVlYKruo4UENb8qbJHWowqEXWHL6gDFs5RgtrO9ijQZqnr/MHCqB5H2E9gA2+CPiOGvlv4kqHSKZ1HGIAbftSaqRgTcdK/OEsa2sL3jEo0UoBnpGarZkCQEwjTDi/ErLVP8cDq+nuN59bvEvXzRYuB734KR8NhlwR5Uzg6DukVnStDRIrEy7kk+FH1PzwOWH0RxC2UxJSpYxt6fWIBkIbi07Dqn32jhVdnzcv2H56J79QYfwS/i7AtZ44bZGKDA9FFwUZsyDeqiAff5bT9rMMHg5bPfQVt9KxZY2Gnl+z8bA2cdUYhACG3DN7xwjkdIkFxavb5wM13mMrfY0CphGetzpJk9h/EyOSUesPbmf7d2mC4sKzONxq0rOACOGwclaiZix1W4X/LmqIMehSVuCoyVm5cd+Nm6/joVzd8PNYhI4Yl6gcP150/gwVXbpEnqW2eHgrIofWDk66tfAkW18bsvTzypusNv4kI4WFIDDiCXNu9H9LsVu2jdVZg0vnGYAVuas4NhKFcJZj9p+uJxlHJUVoSDWWTFsCUkjqZHIrWnjNXig3ft3q9Uf0zTiWXFRCcOtp6iZdCAnQv4DgSYDtayC1N2nAeHmhnFA2418kyUK2na4Tm2wgswPYoN9VUVk6BmsEVL2OcrdyA+xotmJcQ3UBuCelSyFOCc7Hz75dGtrbN3w8LhY5OewigOo/5xqyJf0gGG4W8CjAMbw2EXjSnMjmgovx1eHffOjIsuWxePBAD598q/0Bv5FyghthUKHUsvpQPIKn4xwLGTIRwT21R6FpWXgjF8qpeemSuhnZj4F8R5jwPEJOTQYIUgqHswtyQIKGLMEI3Yhmx5Qu51XbLvFHcm0e5pxAvaA1IdHNs6kwTJZ7y0T8K0A4ISez6mHs6aPbxqU5v1F1jDsrdVpdHEJSc+zXwq7ROV5nBiJpKGyFoe95yJYdEWbEyh5XETAPw69YaCmz+8zHob0Jfb5cLf6NR2vA1BpRtjpjmGZEo2wC5NQ6uhmc8wkMq5pvbOZ3KPl6viUTOJf1M9akOgEqb/6qCUgFwmPoYRQ5g2LU9zS0nNZDym+c1mcnr+KBweX4tSZfpNMKHPWzyPx8rg4fpnVWVnSXH9CXOlSlw6sBzog5YDoefYOi9nK71LL+msv8STANOLCvmVqMRJikW5Q3V0EPJBBPEPoRSj+L0dqsCdzWyGiQGETiRuAmPf4MGAV+CifFpFGv2CplEcvXB9fAr7TAI4Ec94mSkQaV09aKWC0tA736F10Bor1dKOOkqDB4sceAUERhPdal5px/t0kb/QLO7c6TUM/zsaoX4glBzyJCbATOoqTgN9Kw6d4/gn6PyTwSklqMlNgXmr/r3ZvlRwJbSaZe7GLDZ3vP/L7Hd7mPTea6Q7va9N19QjNtUY538eN7OktK7XEU5IByusv4JUbv5bGLmt5IMLryBjc07tSugX13idWTzEqbJRYbIUmaR6MOOD19P6Y1UgGhhDepDotW7geLp4KHl43+w06gIR1GBTHjd1DRHINFHfrjtGRoLrlnt+FvGPWuVr5xIlj4Z2GE2nJrpKXqpyAfJBvV93Y1C8wG9Mh3GNuxgzus3SuxrlrvF7g4PWdcLlCPlJQoOsxRdhyogYyJ3JAX1MnUhkZBab+q1Z9vWl1Z1r0zv3E5kNtQtbYMOHEd7+3m9XZZbHKaOehPjLhO11fyF6X/gxN34y6ch1XrnHnOnR1sf/mLUlOSTgNVUxffKh77eJCu+ZEBw+RQ0phVsreB+C0FwHth5j3ZMVCaBzi+Fn1KxlyrL2St19hvHMI82WZ4TJaE5v072vId9+eNwEQT0dFYzqZ2FFO3J5GQkrlvGk2gUT0u2qI0ANmk2Lu/E8ojVxvUTuQqJoTdet3vQ96SW9yAaYQyFpGQzkPEDwBn+zs7qHN+tNe1TN2jo44/crSgawi/LNJgMpRGF4OCOK5oeWpo+tIW/pStE042YB2qvckGU2ugrufGIZcZ3jShdibBztKnDyuTgxWMQWmQVyHI/eGhjf9gZdcF0FGzdZDyC3jd6JihnAdqMF0KkY187lmWniycNFCCy3RqSV2wdeUG84nGvo62mnj90XdWbFolC8krUIJsqOOxW4yrXvzgs28dB151GVlKzmTt48JWlB2qCm7bxGtSaBYZvzadN8mVbt1O5zz776/GEurdBBlpTSFB79oiUMDiBQ8xr0b7oRTeGPjZYEugAYnYnnpyXqtkswMhfPUTJB9HXux78Iqv0L10W8/d+WhI5bIN4o+/eYeCPAwRlqiD1QX5GJ1SlhcyPKQEClAW0UI/0SH4DHH2Q8MxbbP797W0pAjUPsX0RttTxv9BtDZ7vXCPvcgTm/bpff9qCm4sxWPyM30Vy8MxwDX7ogTtkMAUbQjcuXYGhu50MQ1mI9E0XI4CcM9gW/T8q1MYzHwNo/SRN2jSyZXgHV6/q3EKgkpjMMVIRamH7f99ZiXX4p1AU3r2Tpjim79VYbtgfKrl47ozkVmbC0hjVlnM+mW5ep9+bZWd6y/frzXFVWM6Vinnh0yBvSMiOrMg/yIih+aht+fQF/BOkpwfkgEPkG9+T2oh4XNBbKBhYzvLys1ZFyg8G3EjsIbLqqqPrR5csoyci0dlpzuSyn4YU2Q7wfFC748AgqTNtbXHwT/tQxdzbD14YoUHs5NZQZnirVlbMgjCyOkWKWVcEQIUoGZQpoHU4rnQpgML38vn6F2ZItghQyVX7Qg7JfhPuYYIS1jL/Hq7sB4MOkh7oiBLqelFX8kFed8FNKsVPEVhQxRrHSVbePuv1TNSxnMDlbAEqYN4JV09xLYXbchOlJStpURuAe8uhpvNOiDFO7LQjDA6hZ7xYL1SfkRpRPuaMxPDpjXDYf003SN37bvjCa1mkEfEVbj8BUofZG+i8RmRFB/yLS5yKqTKD4h837jdTcnqo3IdSJsGj3Rje0YLpTbzfC138POB94XmkrJYJGeeMT/4xHYY4r3cpEz137d7pKR4xfVIh2VKm67KTQV+8ztqf+LMMGyWv6vOsvBXGNJTA9Sf1vIpCCKVNiKJNeNDPUmq39npKRz/PrtBGE5Jish7ChypbExEW6ffpjxf3vB4mbKU5DvZv2COY+15YKT8rpkBl97juP8eZcy1SxoYRSb90ckhLHYJvBaaT1U50jEs218NDs8kdSDZAVT5X2TLaKdikfCVBhP5x8BmZtiWigTw87lKiQKmw6/mBU0E9vJo4xmC8aykbuYx7KorwnNkBw3f6JQ3F3kw0Bpknl0BRZWJp762o4iX75wh/vmusW2G02DN3dqDCskm79qW7eWplaUyjiWE+LPeC5Kl/4rTHaKcH27Pb3SfJIMf8uQWeDENgoHXJ1/xMR6XjTNtFT3f8Nzuq5olXpLUs6CvrWBLU90yHyCwhm9wHkM0MylUxdD1AhEpP46xE4bxN0oOeLbFaLbKkx6w+ekEGIPdzHU94RpBZx8gdVWy/EAQ2beca2kMoQurmVr2LhL/N6UqYlDjGu1dMUKJEqQ5HkR4MCwTDarHulBf6Faa9/CQa1J2AalsX8FCKM/DhgedAKu7gpfUIabEqZU2QEqsTPZd4FNNrufh9lVOwUB6RQpNj7NXHa+7XAI2M68RDFU9K1FSst/1MkG3ZCazpzlEbDoSy8auRl9HRO+LWiPdBJps+5sSVgRtQ33F+wapwegFF1UIJ1oHCU6Qt5CrTd13efJ8hVrgvoIyI9wJUbQ0nQ8kzPVp1zkZxsfOMtFt33wBniHnkBdylmHnNu72VlNPagzW+gvMUJ3IDa43o06iQsLO4EoTS+8xwUVVMKBR1VjDXudyOObNbdA5iMhIopcAonVvDAkOlQZDXVQZI9xlcWz8DeMJ8LqxqqmhhgGSVxlcqY2rZZdD6jOnEciBhwv0PB0nXKa3F6SX6lC1SFATKV8DC7b+1LBPbFD+oi8cciyUwiICC9ofjcedGqW/lXLPhK7SNHPnEE0kI5Zb9IRVHGnGJG5C4oZiRj1KHWhtQse8wceA+RNG+XVuMrBmxqJUYAC+uqxkKoBKr0rPcgfe1oQuHscwwjoHiVwamOuns6nTTJVMLe9LaapYV/LFzk8ZQ29OgDePBE7N1v3Esa+p6+2fGisb7Ak1yWQf07GKhWnZpLA0rlqQeEUMgkUr3pBIBBRMApYemta8PvAsB8XNpIZq8xsN7qL205OMin/Sr8H2oBkfvTinsOaSBTgp2br893UCPt1c4d0IfWm6yZEZda2yVVeg1yNQ59QfzZospXfNxP3Pq0kqiWr2SaVt4nZGdkkjadqi+fy7mMTqdsjhZBYoL8jsisg2oXa7JHNKKmGwSiNo4R03OEpPl0Sxfcavwzu1OnTUQXm5gSLfQwAzcc7MeHozCHfl4JdrCuu7vkRZdKy6Iy1FTNLprJZzRLM9dyHIMq7GOKvOLBh31eo+T+tOFztq6C1JXqK43iSGgDfwNswRuUTI4Wg69m3GfaQIlHO/8HaQoeumQnXrzB+x2xKmMVv9J5dFNhXOh7vn8iL2a+ejXxXUlEAIgxTohi/igFKe9RruuA6riTngHvHmti9ohQIyFGlTvAXnBFOyuQe/mPwQY8jgKjx0v2iXVZCNfs7TXQJcmruk35ZGj61CmUHlWB4D9mQxVS7B0VpfK+5BMevwkJoBDYupUPPqRwZpCwpPJvU8KCK1WMVSLLFuApKyOjL1e/3XhJPeZyuY+IXaqYB/phfJqRr7mpDzNVR5hO2JqWbMLJA8Z7PiRneyOwyyEez72MFs1wm1eWqZZWzjIJYVr4ukDhHpOilscRVjoJT08XPeua1wcrG+CHeUi6sx29XycIS7h3BG9PjnEk4vKmdvS4y+qQQS/b9BqTg7/U5JSymroO3UqQLFqFvwph2kjg5CyST0qH08E9SCf/VkWRquUHxmykTJa27cm11dWm3AP+lRl0aKs0at99fmpxyAE4A5eyQTLSb7raN/+8WuiIJZ7msh0X/7aCUNTpFQ+XBlhGGUO/1Pb+eFXdKeFam7URgoKkwWFbysrLL4XVwF+TAgeU7UWBYfmHmpB3yEVlGeasAy/Yfzrxzl9pOydNbGTTqKdYpzQMwn9mOhTMFeR/LVcYXv2bqRuEci5ati254Gi78yTcH4aDVeRXQBxM1fKFtSMqatxKR+Mqt04gCngg9khzpCz/3UYIZkUT6HHeCtt5u7XZmzGCZUknvZukiskXR3HvmVmxOeysNZNpMUyoeuq8XFBwPoyCNig03+O5sTIcu0D9yIwhoSFARPbiEGk3Gt5KaEruZLFcrBXCsozkizkmeVp0b0pVYvm0Ceipa650S2Fa74nAJzjYXT1bYxbT8Srr0flg8DOLtLKFeo1OoRq0szQ9KUOwPu6DrkLxjselvnuZoLc0ZBpkGqTgCCRWwq56ncskEGUPY61nMyWTk7yfxmT/i+DkoypRN7je6o57t00oR2Rk9QolBMNhMTDelpZbaRVK5Iqg7/IooUFdOXqg8+TSBxNGj0vpTbnSPFyAm1s9hOxZx/M+eaP3CZS9UIGXSUwdz4Bu4JAd4dUPpsmI02Mr/zHzzANuP4P4lmM4Qst9HxiDUV5HBD1pLG09b8O6KfSaTzE4OrouFgHbr1NXCrR+aQsWtrX1gojwJ9KxFiyQv5/2laCvXzg8ZV/bA0Nt53qgFWh8Oz4fzTpD9ytA/jWPnMsTlGhEYaU5hlgxyyFrj0o5Gh2j1NR69Q+GT0suohy8MQAjVPWJVLqNCxrwBNdoHblK8a3mZHQwUWAXQlv4G5re5RweR9Cpx5LZR7CN6iYZfNqgleJ9gUOQT1KAvIFLFZXUGftpVnUB0JHlev4gMNi1D+UIEoMw3smEb7FsFxyMXldAT8UuT8FbwcWIf2Ve4WvGq7ejWNvrnvIuxTzeLh/j10nBdgibCDgdH+J0Y8obhCASKlmN/KXEHIiZkU221qrtanB0VMt1JoO1KOlfvvX7isFi03Uf8qVd6JZ10OK5Yc0gQiPtUpSw0Uk8WiEXpW8mOlvHHiwVijbqss+Wn1A/4F1G3gyPrD3GkmVre9V7+DG0Y03okDvJawJy0kz3AIyPtQVftdCCVnl7Ai+ObhgYuEko30O8dMsUDX05VizZx4V7OnKY3uPRZqDJsygw+3s8MvqwmZUv9CjZ4dZTxGX8wBnwgK6oNRsDRwgCv4BP8zjE6Mvq/7adKXKc0ykjphui80o+t4+SmOCblDKUvTWQD1tZgsD5I/EItIk8utP8RN8szJO98R9trrFjon7+nDxnZv6VsrJX8hSIlVSwgTnF5xvLBCOHpkf5lubzXBUJkt7Y0MzJi4EhdNNyQCpobLyab3NGRVHAIPfvREgVcdeUudtkHyOn3xDbLH4dAZ7AZRIzrqNSCXHMF03Wp5Pl3LnUY2nJaoTGLxqpaYYVZGxanX6O8t55qy8HJJK6qxIufr0yM/KJjHMxrpzykdIy3srB1hyR8EwrTiF3iBI6nsknBlmHMNEsBJUD0q8vvdSwpgdCliWIKzaeGdQfEQzGq+ROHbM7vHaUpAsKibuxU9tPgX1Wr0LwaB/kGIG69fo2NEZiGLRG61B0wEIDnw7O++0do2C0ukYYEL4jEpqZLHFPUho/qsFG5uQ+a1ExVWN7NwbeWHy3L6zDus9hrPOBzNpLleGDnJuvhPgnvbgT8wuc4dcySdnHL/7UFcTZslyzulBGBmq1Ddm0Vor6MtSTa0/LheMetcaWmNxaB4TiX+Up1t/X8IA4N5j7/DKCriLJj6Cp4ElaiDpHkBY396p0fpLhLOT8AvTC8B4gZfek0kDdke7aCFXDgMHOvuAPd1Z9ChAAddFQsFgJ9nPUjTEB4Ps+XX4OMQnowLVYxNiPo8L4OurRcI16pNvQRuA7KjvVxaXVJPJxteXJpGQXaNOIOUCZuA+5/BIlOJlnVzb1tlOBr0YoCpf78nrobSO2upYPMyvWZ6+MvksFswxn25rdGFXBYFsgKOWdW3a6qbFPMrosiXfqq4wDU5N1lGZAVVD+BJs+i7wLhqsi+IOJ7p9n9XhDKAxX6InZpgvO8R8XLBtFqxxjf4wzFJV7Q3f9MurZEnJ7I7rwDDWaOdOKnvfbQxWjmnTrwVQlD39qKD5eO2mTUEI1U9jUpFERY+iP8+i4yP5KWwN6nQpjqksJcH7SuzRcutpeL6ZeFHn7ghei5KavN7DFam+hq4x8Zm4ShFusFq7I7QH1Uc3jsCSyw0+dzu5OKxxu/YW6gsAAL0/TGkYSU56BKX6w+w8bGXJH4KoQA/qc6mhlcuXcgae/IrdZ0h51RWl9gWZVHbcEScVIIbgtTAkVqA0x8Q3J4pUEH3ThOnvj6yJebi0FEWJLA4Fh/f0eOhFRjbgVuH6KtEdm9KptX6Y2Qu8uWWjqhFnRofSzkAK8V4YlYXT2uhat/xhUh1eA3LSB2xHEhmwRBCCVUSkSMv1GVXXR6NwE9T22wLhd0cmDMpAdnqsvUjEnECKakxbqqc/GtTUWr+aCNMCJUXO3gnJelhTFEWYeoQE9l5k/nvzm61iuzlLFPcyBlikNgtValeBHhmg8Fy4U22yZbQi+wXvOYmN1ZpZ0M+WMTriqRfZVdj1rao6c52hrJIIjdcq6daoI6vQT5+4fBysKn/Ur+rJAYTbhWOafe4X/ee1CdiL9V7T4J5MEr6QFh9sPB/BegLNXs6puLbwRSYPn7WIXe+XA6M1x05EtyZ2Z0kuNHjuTEaBPFqHV20zHXKwC2OKFIQUnOTUBt7GzP1JlfPWkiymHXa6NxI/sa4i4n1lkgZPHUst0ce7qQDqx2jVZT3X4iMzhpgCYRwLsA+PGUW5ZIbsEU0BmYm9tKMkVUBmlGuLGLGix2Ox9KsRnZhXUd/JJkCwpl3NTDWf/rZd8WB4c31q1eP0nccrVT6sT4ejr/WLQDzezOdOKKsBtd34O+mxR/hKyeOXWVLgixsx0zzuhbDHHDf+aFX4k4Q8l67FtDKbhOgD61FCl02UQrKIluatjaf5S+1qoafvX3Huc1mWBVSXN/NByRJghWNy6ZGm3tdMphdNMme3oE4ZPMb0XG74ENAZXZE5PQ+Y4OtYZYGPcDjm6ysx/zh2N+XmSro3MdafOEweVCu0HQZ0dXHtbazrcs4p1HD25Y1LKTL2N4yWGBZgIXqUnAMYw4TAg6Uo4fQ8OzVW7m/kWU/ev1IiORpB9k6iu5kBCNq1wnYMdgL0lHEuKhYAUVfllxxMOQI3WZvbZHHrrwBkDtxf5G9TBeUADkjXtT3mDxBpInnkItEVY15THRINoNfA2wmngakc7jTyk4oC7LXRinpH1bdjcJDfEMYxNqbXDmKyYjZNzuz5QKWoIRjYv9zOuFSWyWFA9S3n8HdUoHqEX5Yd0XFDWjqhCF2SNCP1fH8HKV2vLVa8LGwNl5OiScYUgHYCIx04CQH+p2DCrTEH6hs1YjepRS/VKFN0wU6ELNH8xh40j/b7Dz20363U9qAk+AHCEabRjY9/Inu+XEz016nxf42wlvAU+5LnE401mplbxRKmFJ7qLYRRg+wQ55C/yQ7G+9bji1uFOqF2s7NXRiszu1VY5qcFN9g+5ee8lyaXfp1/X8eUT79OOWbEoyOw0VFCEo1jPJ2VMmLEypQVF8D8uuoDAkEBrdRla6pwU7bJO+FGiY07TvM/0cuqg9cypANsVhZvoDoTejHrWz5NF+Mz2qJO0KeEa2SHttSkZ4vxrp0OpNTPm1mHL7e0Cr7SNz4bqqhthHxam7jK89RKMUwIwukGJAp6dJ7ZS0YIOwxtR86xXf5QbAQkkstryGCPXim8+4/fLkPQ01yYaq8PFNzSUT5XIj1NJEbQ8RSDrjJ88LmfWfXQKbZIE4WSLySxQydCI9lqahPZ54QApGKP384XiHXrrX9Lr5lvf5i2MYSotmfT44x7ikQKrQB1mdlncIqxcLhJJ0dyjyQRW97khA82cbVFI2hqBhAvcmjf10y3b92f4Z6qmnyisUZKBo6jgmjJ/y22Gpe6N3hLnDva8jRcQXZWEohbYdFX90eA4fRpWm6VW/m9+JTcao6xjSN23ZH6g6fM91BO+OrkeSqN7mPQq2NWIdlJ4mcfJ4tyAEVKuoG7PwQqXw2MkfN7c2/V5pg2s7QHrsAZh1lEa3meOMKZh9xcf70sa+kUiSJF4ndZ43u+LnDTSnCSER412W4nd/aVmtmZdsmaXnFiJUTNReazWc2A6qJQBJ98j9lEglhTjSph4C3WkZxa5Fiip+38XtPMtGcdp6sCeq9aKhTvov79xwCGf4gs1RJOZS9h6nK1z5yHDDM9sYVR4eHth5kqA6067j7ZSGdh0ls4yoJa0P8RrBro49DGclVwVmuL+vmJd8MjiWvFpqu+VS3iD0Zfkarpl8imKCIRz5Ra4f1eh0np18uTJF8z7018txXkJvedjN7QDf4kkJDvLHvEtFUMfXlBSHSPnmjRhnGhxfNb7+evvEh59Ns8BocA5RV2BL/l5TciCiMbFZpof2wnVlirLFmu1nyYdHWYO2ANbdTJTDXOGqGAeB5V9gI9iDj+gkj8NBdSGbokbtJUue6oMKQbvD90kk2SLpxO709w2bQF9L19rMjv5WPpoE/qshZCNOHjn6fOH1CZ9qmN6Bu8FvHmMUPbU+4oQBNkwlk0m04EF9TtBXuoMA7sF9OJFcVt2pOUpjM5b2Nf5zS0X5hHFk4TFACht0xgtHWQtVr6c8Je3PCrNeZR7zSNcKtApuEZ2k5u7xpzEiOtIu+DKL22M7gZHPsgo1Ba/xti48NFBoPa1gt6r9K5YXZ8o9Qh/E+kwYgLxHZV8NX6iv8Ek1d7xKfEfh+Y8szSo36S/UIONUWfmKsYbDjkhhHQW0PSmCP6KwydD2FtX8XoUmRLHif3oCGKprzdY6xS+E1xvVa+cJ3SC11SJoFjb5tJSpkycAcHL1Sk59ZKLNegU0QjPnNOQBCxkHVRVQam41y9jYcgMhLy9Co5z/HK74hqS9ZMm8hkaiqMzQD9qUZOIkodLk+IuICDmsybkWX8SBj+f66gjAtUTr8EUPElxXG/v2Mx03D8ifNdmYmy4d6vLT0MDSuQpvol0hUJfSWkHb+A5V72O2njL9GO5utqK292MmQEesfxCZNiqH929r8Wj/JUNAtoR04zJrPxsFKA9MNrXXLVuv1RqgBX+GnAmoRAyO0zRIIA7imtuzo0LVqfSV6YdxaeVdIwid5Ivljej2CY0WbfSkpud9eDknn4op9l31NVaKri3owFt4mMakoHqQNOPnXHZuuqABUsDEJk+R9urAsiq3OeiL8RYtw+2wwmYQv1tzeWEa1FMfuYSlH8e9Qo1rPQJ5GpDThtyWl9WyIcrc2FOZkUFhAfAOqNYtyRRQBhPwvwyARLBhDZuB5D38ttsRazjp6V+lszb+Rqc73TtMIPkOFAP+9+V+1RkW5YNKjINtYGXcNCvYYEjV3ptdDATpfrcykgatrVfAWUSScpPOsViF9DE8v1E644FIwwbM68qnjqr7AtZpBq9XV7xXfOuZMUWEOm/w6fAbB609rfX0dD6GVf2atJUQMKWWXldn4mlFsqn3lHv2lYmZpwREIZw7ceqhWXmZ1GDFvm1JPgKJaJ+QDRJYe55b9AmwMnzeT+TF4QHWkv+KrblL8OZk7n8a+Ph1YS3KtXV1unUFUNliVv32QeSjx2M6dVOHDHVUG9FNbFaMHLqr6ykG6kRRmO2zo920zDI1GIJqslKnJf8ICzV1hegWeFX/xFffNjWsrC3ywOjAcyO8hORHRQONkaG7r8ZqmaTNQyyrno/Tt9yfsrbPKfzC23Pj6Iq5WcbmIQRa2Y5h7za94UNZCzjk1hRzM47xlt4zATSSQzFUwreKDifnYD4zot3LZWvhesnUfCKsme/vVElRehhE1JfeZle06cyC8CQdVutbH6xDbT/9ah3tsM/PPYhr78cqBGLw9AjVumfyywp9rXrXpW6CL6WzHl+lIWO0vzYl+Gm5XQNDX1mm0A9hgDz2H0Se/t2CH+tUOMUUWzd41l12Ms4utc0TLp5cKobEb5pRpnPFOHKW+L2WCiBxu784E0eHP8fYOpi3meq3zmADNTfxtDrbROA7qPO4flscMrib5Lanozl6FYZ8XC3O8Fih0lct+BSd4bv/jUbO8OZCaCnHY7e66C/BTqZN3A5ZGlQ7LrFkFS8KEykKsjKqJ8SfSLO4Y0YJuzjwCa1NGifPDI+yICVrRrXL0WEFCFozx3YAR2/+/AVeo8CYZT6vQxIra/nL+Via4yBb3GEQwbhABaxsDChC2cXWTibObLLZkwaFHvrAOo1QKGrURdTTjhSULU76BcxpIbPtRSppiCb5Mh0V2nRHGc0ySesTkazzEoeBYWL/pRZHaC7HEm6OKePE15lcRs9UddL6xK0S0oEsFxjAazgRjJNH8IM542b8wH5KfKz+ag7PoVsIyTv28aIVm7+d7tRx42hWO7Ga2FbKVHtJ3XFtMMYuk/CcSbNdkL0LJDkqoP7W1XgN8YofLthmOCfPwSb7JfmNGjaJf5iUPDYeWCs8ZHXp46mbNNiu/ylcg2LK//Oup+hSuDHNO+enNPU53R0jQAqm9FaHOjqoPaBqY1LqEM4iEh+6NFTXuvZHF2Q5L6b1/ybGzcNWjBvLPy8Fr7YbkVqP8f8q2TbuDhtiDb5DcOahMf7CzHtbMyRLFLRIqK+RgRQLAtWdFezfkUKFsb6wKtvFXBsNjd/UM8Nqxe+UIbXEMOj+vYwbB9n6rxYKezYOuDKMVrCoGfcMOZO/LQ0vjmyk3KBhASGGJq+r1NUuG68503sZv9RtIlvkCwCR/gGYs51xPA++IkKsx3Ah39zHSzp24NAc5zO5qZxZXUCcAdjDC0dtvfz4KkkSkyJH4vzyH1tVZwcML4B3yQL0Q3r2r4rKRAxnG2U82cm244zH2Geq8kA+S4WehIwjhDHtvwD1zLzoKzZph8whcUJKK3pBCQBFygUlfFu1zdzavqPoLQPRejJTocCxirtWWnW9/nrOLJiVdL7Vl/iPPpoH8m1ZWnXFAmRyZNocgVOIOWlqNSzl3n0IK+yFYJCU8OQJpADA02n3J8aydrlGSE4AbzoDD+HmVQqJI8p/hCfGbNKEHE2ag8RTQxlJ/9Mr50LJe68vYLrvwrQQLM8y0uAnU7fkrRti0yqV1xKO3KVQ8GEyABQV892NIeKLR/DoMoWh8PHuRZR8bLWQBXQfnEeLGPJSu3wPaZpS/qH8yurt4PbAowrUjG0OGAaFIe4aRk9+F/hTpJichhgZ9DIrKSkHDkUh5bqaQ2uPsQPStSypcmusa8Z1PMTSvxYhbdKSEFBJWRlMsBwoaqddmDxPpS1+kaxqN0SDFbd4+kUw8AwwF57iz8f3SBbhY/gid9BuClcp5znfG/ijKb/buyH0+8ChC1miC8RywL7wyMg6tGrhnvj03kt3iXvm5VAJayNEA6DSOcgO97/z2L06yfFIKCa9HBVFmC77O6RAZzGDFhAh/YRbouAQaOPmvvmzijIaRO5D/F0AUCM1rQCUucJPsafEVDSNmEF/Aak43cURomDsd1MGQDIxHObDePQmc08p3ZWln255reLGQEvbyu5wRhloM+eWNMaIXdXIGDg9P9iswACvVjmM1bRvGJpQO5lCm1gMkRBcy0lhQzB+LI0dI2UFlAGZhRucYgvJ8z27tLeTrPveQh9tlYYCyyor4B+JduK6G8Ag8zs2+FJnNiWOcDwd2JJxijekd/uRE/GP9kMw455meYHnb7BsvfYuJvVxguObkWd+xbg7q8VkB7gv0b43Zc92Hgd21oEZyqClihAck3vOezOZOl3/nhjxXigzFuNy5JrMvIfVwGMth6qDMUJml5Prs83SUWugBom5n4HBO+JV2ZACoZ6mAFH7dpWfPCSMyT5E70fh1Sc8oWfpv8VJdREMqwHlNIUAsH+sssPTcs6Q+XgyRvvmU/evirs95Cpk4NcW377vwtL59Q1S3PUUQAISjk8WMPeKBJHA+DWp2Z8TvSWyQnqxe7yLU31p82Gm++u9FIS7PTGs4+zuPYus3VlVIfIJQ1+k+HRg0fVbDeVQQT76Zh5kTYDlMYJbkEWnPADqHhkOSG2fT3rs08tFpObgWNAdKSrhwcAHSTBc5o4KJ9bMYQIqupsLq2KMdoefJu97aSx9hsfArDdPjWkw8d+8CHZ0ZZVPGa9JgzE8+ZBMc612AyRgY5O+WEaEwtkFrI099qISYF1Bdv2HkSAFqkxg5IQ4aaKWOa8k7w7zqOME98+h/eGSLfG8DwjwwZ0Qqm0uftfIZSnfK/XgSNlydvB8zLLj7DizWd2OJp/Z3P0MgCFA7A/hIkIiXuFdu1P4MLE4hYniBaM3RxoilhI5apA7X7NHGxHL/TIqyeNgsc8AFlIhxYZ8V49c/vDfDWEeKKp6EbrFlYqcdKewn0Qw31GwJX0r5kquXJ/Cr0pye0jciiFI7UxA5OBLxvVEOforccV2H1ymtstZUNQPYOdfZ27D1Tm24k7Da3fBAcQMbEVjV/XiEbrA7MvlWfxSCBs8pvsSsc01c1fY+/mt+okyMQJZ8JTaIwt7QlSeChBV3dy5+WdCBMTGk3t4F97Gl86Oip8qrFbOG/RaqD0/38aJKaH4Cg9SwuzMapVPuiHvjpf7EJ2SevqCdNHkijJKO1Io9cQs6dyLwnKrQq0Hz1nFYa5EX69j98bq7gxjnwb/4rZepmUK6hFRzB+0=" />


<script src="/js/dnncore.js" type="text/javascript"></script>
<script src="/js/dnn.modalpopup.js" type="text/javascript"></script><script type='text/javascript' src='/DesktopModules/Exionyte/Menu/1/Xeon/Scripts/menu.js'></script><script type='text/javascript' src='/DesktopModules/Exionyte/Slideshow/2/Xeon/Scripts/jquery.cycle.all.2.74.js'></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { $('.slideshow').cycle({cleartype: true, cleartypeNoBg: true, fx: 'scrollHorz',timeout: 6000,prev:   '#Prev', next:   '#Next'}); var a1 = document.getElementById('arrows'); a1.style.display = ''; var b1 = document.getElementById('bannerPane'); if (b1) b1.style.display = 'none'; });//]]>
</script>

<script src="/js/dnn.jquery.js" type="text/javascript"></script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d3.5.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3ab4491a8b-c094-46c4-848c-d2a016749dfa%3aea597d4b" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="vWnkODAZn7Bq2bTQX9MaOISCNvkiNiaRLPUxpa6C3K0Ajnbz3+EmsowlhEvTqbRPzBrd2QuJpNP+wq6W8AbhqD1LXg+2oUZ1rrYd5vq/s6x3lOph6qWAs0x/ZcPt8bTcFurITg==" />
        
        
<script src="http://cufon.shoqolate.com/js/cufon-yui.js?v=1.09i" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Futura.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Font.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/jquery.pngFix.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/general.js" type="text/javascript"></script>
<div style="display: none;" id="message">
    <div>
        <h4 style="text-align: center;">
            Sorry, This page dosen't support Internet Explorer 6, If You'd like to view all
            the contents properly please upgrade your browser to IE7, 8 or 9. <a href="http://www.microsoft.com/download/en/details.aspx?id=2">
                Click here to get the new version of Internet Explorer</a></h4>
    </div>
</div>
<!--flash section-->
<div id="flashPane" class="flashSection" style="display: none;">
    <div class="flashPadding">
    </div>
    <div id="website" class="flashContent">
        <p>
            In order to view this page you need Flash Player 10+ support!</p>
        <p>
           
        </p>
    </div>
</div>
<!--/flash section-->
<div class="skinLayout">
    <div class="skinWidth">
        <!--header-->
        <div class="skinHeader">
            <div class="skinMinWidth">
                <div class="logoLayout">
                    <div class="logoPosition">
                        
<div style="float: left; display: inline;">
    <a id="dnn_dnnLOGO_hypLogo" title="North Maharashtra University, Jalgaon" href="http://nmu.ac.in/en-us/home.aspx"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/0/logo.png" alt="North Maharashtra University, Jalgaon" border="0" /></a>
</div>


                         <div id="dnn_logoPane" class="DNNEmptyPane">
                        </div>
                    </div>
                </div>
                <div class="menuLayout">
                    <div id='menu'><ul class='menu'>
<li class='current'><a class='parent' href='http://nmu.ac.in/en-us/home.aspx'><span>Home</span></a></li>
<li><a class='parent' href='http://nmu.ac.in#'><span>About Us</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity.aspx'><span>About University</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/misson.aspx'><span>Misson</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/universitysong.aspx'><span>University Song</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/fromvcsdesk.aspx'><span>From VC's Desk</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/informationatglance.aspx'><span>Information at Glance</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/achievements.aspx'><span>Achievements</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/vicechancellorslist.aspx'><span>Vice Chancellors List</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/annualreport.aspx'><span>Annual Report</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies.aspx'><span>Governing Bodies</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/managementcouncil.aspx'><span>Management Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/academiccouncil.aspx'><span>Academic Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/senate.aspx'><span>Senate</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/standingcommitteemembers.aspx'><span>Standing Committee Members</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/deansoffaculties.aspx'><span>Deans of Faculties</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>University Campus</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/aboutcampus.aspx'><span>About Campus</span></a></li>
<li class='child'><a href='http://nmu.ac.in/kamc'><span>Khandesh Archives and Museum Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/centralschool.aspx'><span>Central School</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/campusmap.aspx'><span>Campus Map</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/howtoreach.aspx'><span>How to Reach?</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/extensionactivities.aspx'><span>Extension Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/researchfacilities.aspx'><span>Research Facilities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/mous.aspx'><span>MoUs</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/home.aspx'><span>Satellite Centers</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/eklavyatrainingcentrenandurbar.aspx'><span>Eklavya Training Centre, Nandurbar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/rcp'><span>Pratap P.G. Research Centre of Philosophy</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols/en-us/research/labtoland.aspx'><span>Pratap Shashvat Adhunik Sheti Tatvdyan Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/mahatmagandhiphilosophycentredhule.aspx'><span>Mahatma Gandhi Philosophy Centre,Dhule</span></a></li>
</ul></div>
</li>
<li class='child'><a href='/LinkClick.aspx?fileticket=oqDHHmONXsE%3d&tabid=3881&language=en-US'><span>RGS and TC</span></a>
<div><ul>
<li class='child'><a href='http://apps.nmu.ac.in/circulars/Bcud/29-09-2015%20Submission%20of%20Pre-proposals%20under%20the%20scheme%20assistance%20for%20S%20and%20T%20applications%20through%20university%20system%20of%20RGS%20and%20TC,%20Govt%20of%20Maharashtra.pdf'><span>RGS and TC Schemes</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Information</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Academics</span></a>
<div><ul>
<li class='child'><a><span>Schools</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/soes'><span>School of Environmental & Earth Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols'><span>School of Life Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/socs'><span>School of Chemical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sops'><span>School of Physical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docs'><span>School of Computer Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/domaths/'><span>School of Mathematical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/udct'><span>University Institute of Chemical Technology</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docll'><span>School of Languages Studies &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doe'><span>School of Education</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doms'><span>School of Management Studies</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soss'><span>School of Social Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soah'><span>School of Arts and Humanities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/schools/schoolofthoughts.aspx'><span>School of Thoughts</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/dat'><span>Buddhist Study and Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/smcrc'><span>Chhatrapa Shivaji Maharaj Chair & Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/mpsrc/'><span>Mahatama Phule Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/svsrc'><span>Swami Vivekananda Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/grc'><span>Mahatma Gandhi Study &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sgsk'><span>Sane Guruji Sanskar Kendra</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/nmuccc.aspx'><span>NMU-CCC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/phd.aspx'><span>Ph.D.</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation.aspx'><span>Adult Education</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/eventsandactivities.aspx'><span>Events and Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/coursesoffered.aspx'><span>Courses Offered</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/tbc'><span>BACECC</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=xCiw3cuw70w%3d&tabid=3419&language=en-US'><span>Community College</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=8DmFd7nqvqo%3d&tabid=3801&language=en-US'><span>Tribal Academy Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/committees.aspx'><span>Committees</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Administration</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/registraroffice.aspx'><span>Registrar Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/lawrtisection.aspx'><span>Law/RTI Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/faooffice.aspx'><span>FAO Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/examinationcenter.aspx'><span>Examination Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/eligibilitysection.aspx'><span>Eligibility Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/sportssection.aspx'><span>Sports Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/scstcell.aspx'><span>SC|ST Cell</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/constructionsection.aspx'><span>Construction Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/prosection.aspx'><span>PRO Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/approvalsection.aspx'><span>Approval Section</span></a>
<div><ul>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Online Approval</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/computercenterexam.aspx'><span>Computer Center (Exam)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/universityofficers.aspx'><span>University Officers</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=ZprmAC3RMhY%3d&tabid=1350&language=en-US'><span>Phone Directory</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>BCUD</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/aboutbcud.aspx'><span>About BCUD</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/developmentsection.aspx'><span>Development Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/affiliation.aspx'><span>Affiliation</span></a>
<div><ul>
<li class='child'><a href='http://affiliation.oaasisnmu.org/'><span>Online Affiliation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/affiliation/licreports.aspx'><span>L.I.C. Reports</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/research.aspx'><span>Research</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/iqac.aspx'><span>IQAC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/avishkar.aspx'><span>Avishkar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/bsrschemeprogrammee.aspx'><span>BSR Scheme/Programmee</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Student Corner</span></a>
<div><ul>
<li class='child'><a><span>Academics</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/admission.aspx'><span>Admission</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/syllabi.aspx'><span>Syllabi</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/questionbank.aspx'><span>Question Bank</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/academiccalendar.aspx'><span>Academic Calendar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/papercodelist.aspx'><span>Paper Code List</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac'><span>e-Suvidha</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://exam.nmu.ac.in/'><span>Examination</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlineresultscollege.aspx'><span>Online Results (College)</span></a></li>
<li class='child'><a href='http://182.56.2.247:81/nmu'><span>Online Result (Student)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/convocation.aspx'><span>Convocation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/resultsofverificationredressal.aspx'><span>Results of Verification|Redressal</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examtimetable.aspx'><span>Exam Time Table</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examreports.aspx'><span>Exam Reports</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlinedegreeverification.aspx'><span>Online Degree Verification</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac/QPCourseSelection.aspx?ID=660'><span>Previous Exam Question Paper</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examination.aspx'><span>Examination</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Facilities</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/financialassistanceschemes.aspx'><span>Financial Assistance Schemes</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails.aspx'><span>Hostel Details</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/hostelday.aspx'><span>Hostel Day</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/boyshostel.aspx'><span>Boy's Hostel</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/girlshostel.aspx'><span>Girl's Hostel</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Services</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Download  Formats</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/examinationsforms.aspx'><span>Examinations Forms</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Formats For Colleges</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforemployee.aspx'><span>Formats for Employee</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formsforstudents.aspx'><span>Forms For Students</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Statistical Format</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline.aspx'><span>Student Helpline</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline/studentfacilitationcentre.aspx'><span>Student Facilitation Centre</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studentwelfare.aspx'><span>Student Welfare</span></a></li>
<li class='child'><a><span>E-Resources</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/eresources/ejournals.aspx'><span>E-Journals</span></a></li>
<li class='child'><a href='http://nptel.iitm.ac.in/'><span>NPTEL IIT Lectures</span></a></li>
<li class='child'><a href='http://apps.webofknowledge.com/'><span>Web of Science</span></a></li>
<li class='child'><a href='http://www.sakshat.ac.in/'><span>NME-ICT</span></a></li>
<li class='child'><a href='http://jgateplus.com/search/'><span>JGate</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Useful Links</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/events.aspx'><span>Events</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/tenders.aspx'><span>Tenders</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/rti.aspx'><span>RTI</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/circulars.aspx'><span>Circulars</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pressroom.aspx'><span>Press Room</span></a></li>
<li class='child'><a><span>Job Openings</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/university.aspx'><span>University</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/collegesinstitues.aspx'><span>Colleges/Institues</span></a></li>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Apply Online</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/sciencepark.aspx'><span>Science Park</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=pDjshMkKzgA%3d&tabid=1088&language=en-US'><span>Citizen Charter</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/publications.aspx'><span>Publications</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pbasproformaandapidetails.aspx'><span>PBAS Proforma and API Details</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/otherlinks.aspx'><span>Other Links</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Us</span></a></li>
</ul></div>
                </div>
            </div>
        </div>
        <!--/header-->
        <!--banner-->
        <div class="lightBlueCenterRepeat">
            <div class="lightBlueLine topLine" style="z-index: 101; position: absolute; top: 0px;">
            </div>
            <div class="lightBlueLine bottomLine" style="z-index: 101; position: absolute; bottom: 0px;">
            </div>
            <div id="bannerLeft" class="lightBlueBannerLeft">
            </div>
            <div id="bannerRight" class="lightBlueBannerRight">
            </div>
            <div id="bannerCenter" class="lightBlueBannerCenter">
                <div class='slideshow'><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage1.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage2.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage3.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage4.jpg' alt=''/></a></div></div>

            </div>
            <div id="bannerPane" class="banner" style="display: none;">
                <div class="skinMinWidth">
                    <div class="contentPadding bannerPadding">
                        <div class="top-cols">
                            <div id="dnn_Banner_Pane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Banner_PaneLeft" class="colHalf left DNNEmptyPane">
                            </div>
                            <div id="dnn_Banner_PaneRight" class="colHalf right DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/banner-->
        <!--banner arrows-->
        <div class="skinMinWidth">
            <div id="arrows" class="arrowPositioning" style="display: none;">
                <div align="center" class="arrowLayout">
                    <div class="lightBlueArrowBGLeft">
                    </div>
                    <div class="lightBlueArrowBGRight">
                    </div>
                    <div id="Prev">
                    </div>
                    <div id="Next">
                    </div>
                </div>
            </div>
        </div>
        <!--/banner arrows-->
        <!--slider panel-->
        <div id="toppanel" class="topPanelLayout" style="display: none;">
            <div class="topContent">
                <div id="panel">
                    <div class="skinMinWidth">
                        <div class="top-cols">
                            <div id="dnn_Content_TopLeftPane" class="col1 left DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopRightPane" class="col3 right DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopCenterPane" class="col2 DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Content_TopFullPane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottomPanelSpacer">
                </div>
            </div>
        </div>
        <!--/slider panel -->
        <!--slider nav-->
        <div class="fullWidth">
            <div class="centerSliderPanel">
                <div id="toggle" class="topSliderLayout" style="display: none;">
                    <div id="close" class="open sliderOpen">
                    </div>
                    <div id="open" class="sliderClose close" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <!--/slider nav -->
        <!--content header-->
        <div class="skinContent contentTop">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="contentPosition">
                        <span style="color: #fff;">n</span>
                        <div class="contentPositionLeft">
                            <div class="loginImage">
                            </div>
                            <div class="displayText paddingText">
                                
                                <a href="http://apps.nmu.ac.in/Login.aspx" target="_blank" class="lightBlueContentLinks">
                                    Portal Login</a>
                            </div>
                            <div class="loginImage">
                            </div>
                            <div class="displayText">
                                <a href="https://login.microsoftonline.com/" class="lightBlueContentLinks">Mail Login</a>
                                </div>
                        </div>
                        <div class="contentPositionCenter">
                            <div class="paddingText">
                                <strong>अंतरी पेटवू ज्ञानज्योत</strong></div>
                        </div>
                        <div class="contentPositionRight">
                            <div class="displayText paddingText">
                               Select Language :
                                    <div class="language-object" >
<select name="dnn$dnnLANGUAGE$selectCulture" id="dnn_dnnLANGUAGE_selectCulture" class="NormalTextBox">
	<option selected="selected" value="en-US">English (United States)</option>
	<option value="mr-IN">मराठी (भारत)</option>

</select>

</div>

                            </div>
                            <div class="lightBlueContentLinks searchText">
                                <div class="searchTextLayout">
                                    Search&nbsp;&nbsp;
                                </div>
                                <div class="searchImage">
                                    <div class="search_bg">
                                        <span id="dnn_dnnSEARCH_ClassicSearch">
  
  
  <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" onkeydown="return __dnn_KeyDown('13', 'javascript:__doPostBack(%27dnn$dnnSEARCH$cmdSearch%27,%27%27)', event);" />&nbsp;
  <a id="dnn_dnnSEARCH_cmdSearch" class="dnnSearchCss" href="javascript:__doPostBack('dnn$dnnSEARCH$cmdSearch','')"><img src="/Portals/_default/Skins/Xeon/images/sright.jpg" align="top" border="0" alt=""></img></a>
</span>


</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content header-->
        <!--content containers-->
        <div class="contentContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane1" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane1" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1" class="col1Home left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane1" class="col3Home right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane" class="col2Home">
                        <div class="DnnModule DnnModule- DnnModule--1">

<div class="containerPadding"></div>

<div class="overflowHidden">
    <div class="lightBlueBG">
        <div class="style1TitlePosition">
            <span id="dnn_ctr_dnnTitle_titleLabel" class="style1Title">Privacy Statement</span>



        </div>
        <div class="style1Arrow"> 
            
        </div>
         <div class="clear"></div>
    </div>
    <div class="spacer"></div>
    <div id="dnn_ctr_ContentPane" class="style1Content style1Border lightBlueContent"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>North Maharashtra University, Jalgaon is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the North Maharashtra University, Jalgaon Web site and governs data collection and usage.
        By using the North Maharashtra University, Jalgaon website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. North Maharashtra University, Jalgaon also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by North Maharashtra University, Jalgaon. This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by North Maharashtra University, Jalgaon for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the North Maharashtra University, Jalgaon Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through North Maharashtra University, Jalgaon public message boards,
        this information may be collected and used by others. Note: North Maharashtra University, Jalgaon
        does not read any of your private online communications.</p>
        <p>North Maharashtra University, Jalgaon encourages you to review the privacy statements of Web sites
        you choose to link to from North Maharashtra University, Jalgaon so that you can understand how those
        Web sites collect, use and share your information. North Maharashtra University, Jalgaon is not responsible
        for the privacy statements or other content on Web sites outside of the North Maharashtra University, Jalgaon
        and North Maharashtra University, Jalgaon family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects and uses your personal information to operate the North Maharashtra University, Jalgaon
        Web site and deliver the services you have requested. North Maharashtra University, Jalgaon also uses
        your personally identifiable information to inform you of other products or services
        available from North Maharashtra University, Jalgaon and its affiliates. North Maharashtra University, Jalgaon may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>North Maharashtra University, Jalgaon does not sell, rent or lease its customer lists to third parties.
        North Maharashtra University, Jalgaon may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, North Maharashtra University, Jalgaon
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to North Maharashtra University, Jalgaon, and they are required to maintain
        the confidentiality of your information.</p>
        <p>North Maharashtra University, Jalgaon does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>North Maharashtra University, Jalgaon keeps track of the Web sites and pages our customers visit within
        North Maharashtra University, Jalgaon, in order to determine what North Maharashtra University, Jalgaon services are
        the most popular. This data is used to deliver customized content and advertising
        within North Maharashtra University, Jalgaon to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>North Maharashtra University, Jalgaon Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on North Maharashtra University, Jalgaon or the site; (b) protect and defend the rights or
        property of North Maharashtra University, Jalgaon; and, (c) act under exigent circumstances to protect
        the personal safety of users of North Maharashtra University, Jalgaon, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The North Maharashtra University, Jalgaon Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize North Maharashtra University, Jalgaon pages, or
        register with North Maharashtra University, Jalgaon site or services, a cookie helps North Maharashtra University, Jalgaon
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same North Maharashtra University, Jalgaon Web site, the information
        you previously provided can be retrieved, so you can easily use the North Maharashtra University, Jalgaon
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the North Maharashtra University, Jalgaon services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon secures your personal information from unauthorized access,
        use or disclosure. North Maharashtra University, Jalgaon secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>North Maharashtra University, Jalgaon will occasionally update this Statement of Privacy to reflect
company and customer feedback. North Maharashtra University, Jalgaon encourages you to periodically 
        review this Statement to be informed of how North Maharashtra University, Jalgaon is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>North Maharashtra University, Jalgaon welcomes your comments regarding this Statement of Privacy.
        If you believe that North Maharashtra University, Jalgaon has not adhered to this Statement, please
        contact North Maharashtra University, Jalgaon at <a href="mailto:support@nmu.ac.in">support@nmu.ac.in</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
</div>

<div class="endContentPadding"></div>

<div>
    
    
    
    
    
</div>

<div class="containerPadding"></div>

</div></div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1Inner" class="col1Inner left DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPaneInner" class="col2Inner DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane2" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane2" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane2" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane2" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane2" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane2" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane3" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane3" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane3" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane3" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane3" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_CenterPane3" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content containers-->
        <!--spacer-->
        <div class="spacer">
        </div>
        <!--/spacer-->
        <!--footer line-->
        <div class="footerLineTop">
        </div>
        <!--/footer line-->
        <!--footer containers-->
        <div class="lightBlueFooterContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Footer_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_HalfLeftPane" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_HalfRightPane" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_LeftPane" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_RightPane" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_CenterPane" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
            <div class="lightBluefooterBottom">
                <div class="skinMinWidth">
                    <div class="contentPadding">
                        <div class="footerPosition">
                            <div class="BottomLinks footerPositionLeft contentWidth">
                                <div class="displayText">
                                    <a id="dnn_dnnPRIVACY_hypPrivacy" class="BottomLinks" rel="nofollow" href="http://nmu.ac.in/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx">Privacy Statement</a></div>
                                <div class="displayText">
                                    &nbsp;&nbsp;|&nbsp;&nbsp;</div>
                                <div class="displayText">
                                    <a href="http://nmu.ac.in/disclaimer.aspx"
                                        class="BottomLinks">Disclaimer</a></div>
                            </div>
                            <div class="footerPositionLeft bottomContainerWidth">
                                <div id="dnn_BottomPane" class="DNNEmptyPane">
                                </div>
                            </div>
                            <div class="footerPositionRight contentWidth">
                                <div class="displayText">
                                    <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="BottomLinks"><a href="copyright.aspx" style="color:#fff;">Copyright</a> 2012 by North Maharashtra University</span>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/footer containers-->
    </div>
</div>
<script language="javascript" type="text/javascript">
    function Showie6Message() {
        var IE6 = (navigator.userAgent.indexOf("MSIE 6") >= 0) ? true : false;
        if (IE6) {
            document.getElementById("message").style.display = "inline";
        }
    }
    Showie6Message();
</script>

        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" />
    

<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>
    <script type="text/javascript">
        
        //This code is to force a refresh of browser cache
        //in case an old version of dnn.js is loaded
        //It should be removed as soon as .js versioning is added
        jQuery(document).ready(function () {
            if (navigator.userAgent.indexOf(" Chrome/") == -1) {
                if ((typeof dnnJscriptVersion === 'undefined' || dnnJscriptVersion !== "6.0.0") && typeof dnn !== 'undefined') {
                    window.location.reload(true);
                }
            }
        });
    </script>
</body>
</html>
