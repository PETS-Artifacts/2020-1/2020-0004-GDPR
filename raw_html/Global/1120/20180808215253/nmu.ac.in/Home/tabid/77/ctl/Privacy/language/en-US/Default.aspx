<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="en-US" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><meta content="text/javascript" http-equiv="Content-Script-Type" /><meta content="text/css" http-equiv="Content-Style-Type" /><meta id="MetaDescription" name="DESCRIPTION" content="Official Website of North Maharashtra University, Jalgaon" /><meta id="MetaKeywords" name="KEYWORDS" content="North Maharashtra University, Jalgaon, Maharashtra, Higher Education, Academic Institute, Research Organization, University, NMU, India, Maharashtra, Research, Under Graduate, Post Graduate, Certificate,Courses, " /><meta id="MetaCopyright" name="COPYRIGHT" content="&lt;a href=&quot;copyright.aspx&quot; style=&quot;color:#fff;&quot;>Copyright&lt;/a> 2012 by North Maharashtra University" /><meta id="MetaAuthor" name="AUTHOR" content="North Maharashtra University, Jalgaon" /><meta name="RESOURCE-TYPE" content="DOCUMENT" /><meta name="DISTRIBUTION" content="GLOBAL" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><meta name="REVISIT-AFTER" content="1 DAYS" /><meta name="RATING" content="GENERAL" /><meta http-equiv="PAGE-ENTER" content="RevealTrans(Duration=0,Transition=1)" /><style id="StylePlaceholder" type="text/css"></style><link id="APortals__default_default_css" rel="stylesheet" type="text/css" href="/Portals/_default/default.css?7230930" /><link id="APortals__default_Skins_Xeon_skin_css" rel="stylesheet" type="text/css" href="/Portals/_default/Skins/Xeon/skin.css?7230946" /><link id="APortals__default_Containers_Xeon_container_css" rel="stylesheet" type="text/css" href="/Portals/_default/Containers/Xeon/container.css?7314682" /><script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery.min.js?1.6.1" ></script><link href="/Portals/_default/Skins/_default/WebControlSkin/Default/ComboBox.Default.css" rel="stylesheet" type="text/css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico' type='image/x-icon' /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/CSS/menu.css" rel="stylesheet" type="text/css" /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/Colors/lightBlue.css" rel="stylesheet" type="text/css" /><title>
	North Maharashtra University, Jalgaon > Home
</title></head>
<body id="Body">
    <script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery-ui.min.js?1.8.13" ></script>
    <form method="post" action="/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/Un5siHIPX3Hh0mbD26DRLt3jWulsOUYuYSnkxUV0But0r7LsqGcFkSZY0v0o/SwPXksuYlhOjVwZF5az3jrKw0NRtO49hjnutdTCEMOzIk6HH/NwdzFlkdvH7cTALKSMLsYi33AvO2iV/Uss9jtz7rXbj1LsiN1xAZ9njAC41IgZmOGoPSVKaFD2fKBwcXGfIXwvptQUv/Ne6IHJdaeyG1HwLw+zk32rw///9hP7b0tLST27G2aD6U+gz47ujvL+Bgk/bWspb+TL9x42HcfX7xWrUPDXvvcyvLOcIvVILGUcehuD787Ud/y0GMyMLgu5Vz5xVA+guTsGgrqMatTNnyZTSAQYbf/5RPPoSFBKpcQZonLbPK4TOKD0m/OaTAi6dBsiZDQJ/x2dxMfiRed7FcccS704SJfo0nsq9Bjwh4dTG9nuoFCNS8uTaESyL2u7QGE/GH+X6VgJ/lti3QoZInCGvTXxh5FDAIgAR4CTMM73vAecEECG1NzfVa4+w5dsg0troIPuWA2pq29kunwh1XTDolt1JlaCu5d9xvsefs5DEm/Y8QgQFB37Jy4gm7f1peuudJsGnoXNrGBcO1pEZ4oBgvLzdSWhLeDQR5rg5RBmycMEvtr+yhQUf5OwAy+YmOeQGS0lNrBaqoAXH4+5UvP7meQp/yYt1JF+w71gVVIKI6trLVxcy5vkoQ+VSl+kKzbdSCNv7FkLASJI6S4gV45Oan+a8rNtn7LsaiZFlggHJP0FJfqKNJrez3KPttfxl7MYDFetrsIiKLiKudtsh0x5rVX7dcf/lmACpmLLeT7s1oMcvnjz6JtZ/vYGH1gUK0x6rnbVs1BMiA3ybF5eL9GpGwoJBmiAM2n+XT6/5ONFwnalCMsFNDA72HT54Jcr+wP9yYVZtNDazPLoea0iILZgboTYvOAo0dhMEZ0X0LOWLMwxlz5e/ih3r8OHQHaRRf4dc8XWmUH9Ty+rZ+5FAN4NqTiq7Ht+738/c7uwkT9RkEc0CG3Ot7cu96U8vLcxTSejKV0iAiLjMZVgr2d+A1Q6+r2jj8DbDTq41o4/E2utupV7zGX92UUXQNkr/idPRGmNd2x592lOopeGBJbtA++qzHTzHb1AwxfwG2RZN9FSDjpbg9m3YH/nI+TWDhxcVRBTe53U3WRGP5GoBTh5JwggBFH8qOWtCqyBP/hsSu47UbfmsTkmA2R++k2mPpxOLsvVnbYF5PHrQwFnOcqKMLQStiE8AGztwIjjmQvPlE6nFJrm/BGErGavu9h6nGJzJP0IbEpRCwj+lAM96Ol3BuZGljj7T+Px8bB9NUMzA3yKWsMmwcj0lNePk+PvmSA2jYaNlyr+O5xd/xVnCPK0OKv2EG+ZColzpvgz2GlatmqESowSf0Lw7+Vx/r2Agz0a1uOpcxE5JnkEXkDmzTgVcAp5mcp+4JV0vpiom9SpAtNalzuzQTOOirty1394XiFWXGwS/U97aDo7ddtRytV+NHd3Mr62b+G+gP9qQGx5nUnGjU8XC7DvoSK3Z71CG9gPzLAVkOUp1Qdtxh2odnfsPj1Ix9x3OclYmoWvF285LxDbXFFr75ZEQ8D6PAVc/73VQPQl6p6DFdC9Flx7nHZRtHj88bvJJGc7LZ7ucfDZU/xfDDDmE03YnvVhnZisGyCAEVkHMQaCyZS5UoHQXKl9sNCRry/MEkALcYcktCda94HQXaUnXTo9jdktkAkwR+93wr1uUtwj267Pn8EzzmNXlV/T7Osp4mwP+imxAiJ/btyOeCBGJi5jTfeAPMN1NojsJxKslxLs7tHhkbHuoBUqKrtNR+bYYJLS3l01PIarq2jfbenECTv3CWumVKfBk7TeAdgKHQe1U1eVtVECN+S3rh9l5p4kWL3KZmKxfZWmu9Lu0P3FRSkpQKpR35Q+pwCi0HHAtx4aU4vmAw+ec98uu31jNM4cVXLSLs4N8u2XtNegG5xL+tdOwSVT3sGrnfZ/jrcq+tgh8Lmx7lYbRuRG7DmHSbjojU+X7DiN8AlBIORmK/F3MP7ddFkZA0VON0kMLe+7jYgsSv5aBva1kTJPJHBF04Km67oqAYEgjaThCNa6Vdb1AGBBOpqX1wgKpy9h33vDtZv8NkfUU7CoETsnBCp8PTyBc9LhnKwU7p8c2eUbnfw8XxQF35FcFm9EBrGgKKCgSUenE08wkmKi8lNBcGd6g0CFDFaNvWlEF6vd9oOY72GuYeOcu/WH+brV9VwgUZHyeN+ctdLQ9FQ1vquEXC/epO3IyBgL64hpAAJKWUZgPDoolxeoSDX0A1dZI62Wna7mCGgoNwytYhb+pVGT+uYCqpehVHmoMmgxFyLTMT2vkpM43UTbp26cABesiNUNSGUQqwPHWrvVpSvlcBm+rs50iCuv+xzcEehC0UM/FIapY9AWWFw1j8aXIWRp0D6MEzxBGgG+DhrB6H/o4HYMbptP2OBBA8BnfP1C+zvqOuA4F/+wRSpxpt1XbF6kCa12pLqGvFkkDJ0Jw1OTcSVFlrNVmAbOW49T3BCwBcoGB4/PgeIEB+aEv93iCEz6W8lQABXqCyXi5BWC5ClWgRgazPLGjGCu5l0vwm2GUNsp4J3dE0sFNNzD+0MzuP3eoB163nwmCBY6xpqOy+YzghzdSxbYecNKduVb5eh9lkloYOwBOFQ4JCsavwOLYXBTGMrifOWAj4/8w8OKoAxDG3S35wYT9JEDr1p6OB6kZ14QNQAQ6NYnELz1dmQTco7nH0yv529SuaSDNSWwAtlv/wwt4wrDGk1tbLXVUuPzGCkvIzDW09778JupqQ7fHFVL7CBawBbNeLtVZlOYPXrm/blWsIUtufDJd049xA6b/q0TjOy5fsuFGtCeoTGIQFrez79zxlUce6zqKZ8Op8XANuYpw/iOxyO05a5hYjzEQOFzAv1iTji5y5thqpvvfvf39whzyov7bq2GTVKd0sGg5JlMrbD+1+mkfmifeLOjwMpb1Y6ZirZ70urTz9Uk1mbReC+5Br0MpfRkZ18IiVXz2Es4ri0/5JYTm7jbupNNSfnLfSRb9gH5a13zJXMkTa4r6g9PX8cEm5ardzF8TvGiBXjQoPB+iBgL7eQV4f8UDmpJW3o7QtF8sr1tcYLyIfAPkHi9vv84nvYs0GPUrqX1NnxMJpXvxWbPrIK52FqeQq2PhzsHzN2UjkMjzlORiydUVBznbztzsPNkKsMFej3BcauAjM4CJhpKlbuQuoURKrJVF+unwviTXJh6aLAyeSMRQ9mckDUhgurT+WWpUWF9l4/rFc2M5pf/GYR7PnQSWz8EhnCmJqccBRQ67gbdGD1nduwDU9/T2QTxsnXrnrLkJAbkWsTk5L/j+2+LhvvAa6bY3zo1TEYE816Ykl9vxX1G8fsOCRXnXFPOzIvIUz7tQOd1aBjI7jo6iPhXEdf4HZby70pRhScwQI6Jr3lXZdD/5t9WgnIxNRs8U/dht+0XENnuT0lQUySRq44QQNHyWrzIYv4sV4AuV9LNwm+PR0W5wFeVQV+rlElw8+5FFOFLhPpmRHhjj50BYHz/ctksEC5oWdbizRgkcLa1nQSST2dWsAtamFmUAYhLRL8dU4bl2aa1psqEbpk3mRQqNXf8AmNTw+O0W80Ke39cygj0nVKSUiab/QZHOMZVByXRzbVNWikmdVhDxyiFQcPrVyT2j8C6KhbJFGGGSIK73/vrH76kuX2aDbkarhwhMx4pcQ71qRWSwtcbOk0WSSNcSqqOKM3ICqsePqQPYBYTGEGBT00l8Z6plrUpP/e7U1w3AI5ZoT8ef2LjDUWZzrl5KVNVSuVXJoyIOpit1j3iPezZc+o3DZidbvP7YmDjZ9+GRavRGTFMxG2/qcWBEwWTUIGeT+mqeFDOl9r8G5FY3K1U24pKhYXJ+OR45DW7RZ4EfsQLJK+z1CKS+mGIDU0a4Drh6hvfaYCCwMHeGysIU1rhuvIJabjTvQ3P6lcclz5C7By+vSOchdmKYgc+zJsZkPo8YsPvOy0D54RKQx2X5c5VIsLIniOaP9RroJ16xarWwLSarC5zS+CNJuP//QHaXxzxi49j3+m5DMIGO2UY/b2Y4RQwWEVYW3oQxjfHrPo1EI3BfWAYqPjUdrcoMZldix8O7cn8DplbdeoL0nRopAbERJX3/zOZMNchaOH7neKpQygvu3tlZmVb2OLgy9fz7OC6CTjl3LcFqn6D+TL5Z1w3ak+w/ZfvmhcD3OhXVRIYwpxzJRLcmDSXBSkluhwdXmSrT8beOYHYJ1sIwDaISjB9RG9FYTCh+dFkbcPrse9Z1mcuO9MAcfuZ7HExOfaVz2YFmXwS7Gqce2SxO9lw7yfh9x43HupGed9jD57SatEVXF/myiarqs8bwxMDem2VC7u5k3+TzfLVbK/aci8iMcBUxXaFX0TgGFrUfdDWOzCPKP8qPOFi/6dr7ohvc+cgqwgwgGzODVZ1bz6YpAxklIYiUpzcJbq5Oy51HNBNVO3Zuzn2fbGmhAEokAg6d+E5zhCOZrNC92FCko1dNVzBMJVQI8xXwPmLeW+emy4DQsZVwZCLUECEoRkByMj+xyA5Tit5Sy1h8JSRK/pexjEfqdAMCEfBvLOfD2uSZIedRt5qufE38xulCPpAuhOrDQIPvAn2E61bV2Ww8n56TPofqH2INrXv4LlY8R0YIHyGCF4SG5oZzFzwmbvp50NSLxghUKGHU60drq9u6wb47xwcBSnYoHBUY2INlW5OJyHIhINziCwy+RazyWME/ZT/PJetEisCJH0HXtPir8nCf40lWXfqbakcobC86aWqzw6xC/bBb2hPoxtE45xRfbuFOpI4k/PUuXT0t6W4W9kyhYECrwoZtbFBQh6ww07es0vRhmStSe1BytCIc/qEVwwaxNpfnmEPeMPu1603/qMYDR6OOLpldMRte1y2X06KdU4OlzhSS4LL488IroS/RsqJNXqo19jwCOFKr/YCLsE6aYK7nnVqdR4kASa6BerMPeeuWx1bq7lNg63jt+s57+xZd3/OWlmsC1rZq1rY/HMf4mYkjL8nVKTJfhQQvrLKtzxQlTPqFPFTLlwfrgqWXPM3sWiC+PD53u6G4SXItSbCca0286cVvUnXQPakmy7JH1dqot4rWP9ut768ZvTeVqL/xBDzowOUx/q0Iq+FqAdKGMS896QFtm+OYMxdIAvQOGZl0kNPzuy+TrPeUulP1/6PsQzbRAsf0Ja6GPnzQcH9qa/0gUkpNpMpcZdPNVqX4VbcX4rYbecwNjvviY7RB2o23gO0sZ+lJQU7qAp2pm19EiTE2OisrXt711iN+y+utz5VDYX1oIkOyYpr3TAG0ZCt6CZDKqGwjnerkj7l5PsRtSymZdJYeao32tqYISdNBqfuFc46Ln5nk1ptpxunqzmIElqQ7kNheao0bEtW+7wTHOSyIrnS0P+6al42KFYDFfq0RIykYTQVsfFQ2DgDcDX8GzTopSsT/jDWjuduXPcmRlcVZnjAPGwrWHAk6PG9t3oweuF5V28soAAMsV0wv9IxV0fNFjFqnY4hxj6g7nPRNM9FGMku5y8eGN7KEME53I2CRwcPDxL/njmwa4Jb9nLpBB8cqvP17zA20ykPIBjV0gXz9RKUO9yg8RWE2HmAxRXzBVlJnzv7TStTkeNZjDI99002SMsFtTjPJievkNn0r5lgtc1E7HNUJMpMtwNAsKUqZ+TiFWFBwy/mXCoNVOmwGqxrVXv7nCueVXxJ/AOIKtxU8RY+iDQFJKSzcqteE7fsttWQjAteV3aZQgW/CpHNQyvcctA8e7Zzl/Ed+y/L2MZb/15mPnaNR0cQPJ0Ce+pacJ0yuGNQLUWMA3Ch0mi/T/yeeS7FQYBTMTcNnjLIHxBp90I0yTUtPO8qV2Rh2VtUVJRfhesAo9aZE8ephKAFwQS5XgdHP7dn1bfFT0QnUK+jOkNelWy1V7L/5DYgEce+J3PY83p9dd3VOIcaa/MFX9F/id3vga+R3UUgjXjKjoZTGXvGcCLJhB91KgStzQe/K873c1Q2kW/rP05id2k2bFDeoOZ9HCFMsHj/2g3UfHlB3qR96uPc6G3/goqi+c1X6MjAYC2qSjD5AlWhFeplR0jX32HGfmt9gSW6LFQJI0nvuTFL3j6VkHiyyIN+HoO0x67q4LHGkAEHF6qzPHO+h74sm4AJ9sFq8+V7YYAlunSJGmtYGuDA63NdCgO55GypY+nWBoxzNdJFkA4rpSQ1FSoX5GBqgbJS/Kd+Aw+LILkNhT/cyxyS+DVDUeRJrJdVR4fbwUcxGEt72oELm51WFAkWD9DAttfw7dqxLRdC08rjonpKEg8/actA8tEnxZjkCbg1OXZxbvyfgevzjMrE/5Rp5IHyIxDkDXXbUBwy4mtWoZSSVJ57yV+Qn4xDAbrGgr0znsAyvQ9hMywQNi5PvOb7JoMF5WcgGzRTWPyFTvI0F+MDIRb6tnWCX2U5geVbGohKzBRqicHxUgQWMcZU0YcIh46XTgIzjdkW9uH5okMgYdY+RYe1/C6T0sc9U96ihdnpCvBl7xSEr1Gmni2da0/uzqz6cnx6XB4H/DdN80PkfrEWcKx5g1NKdjAIukbnx7IP4VEwSVI2uy5nEgWNazutIbXtLhWg6/XOrWuuChXY4Mat6SJyn6YUQ8ga+wG+geBvhVvTC1jcKTBznUJjJVRPrpKoiHPzctuWMzBagoJNPbygU/ADDomCkD5FR6p393Bi4eB2Zj0OtHl03YNKJvdJrpE7TJahG8hj3h4CZ5RchWSyNZ7UbqYXwax4a5iyT2+GGkkSXPrG6Dq/a1fwLLljEzxASdnm/YOgK5ulEhcg38nsg/HXFqgcY+rIooUO7bp50ju8gxW+zOWskxa6IGS6yus14lvhE3Zlb+Vg4shzygZWg5bpWHiGvhb0hSLA7zvQwGf3yHS3jnCX7tFr0N+1GsT4goIsR/DEK58Bx0Ofd34Szu5LONPiFXINKJYj1VTB00EMpcDho5VSk2Y0+UFxcXawhULtGsH4DTH3IgIh7PEnev2tVW9k5/hssM6silwK0HoO6WM7o7k1n5qh6t7GSAhWgq083Ct0kLp3mjRs5Ke9F64kCrTugvvONbl4IXhgsZ1XIDJ+Hd5aBkOadEhj7LlHjoicf8lVnz7kB3jm7lNKTqaB8mPsNr9QmC0gjYdCqto83V2De9+gT7WPDK+50MTn9FSKqRaVKdlTCjRuZev2/yjjVmJasUswTd2D9198olr2v5Jqm2LC0MiJA09ocuQofTFV4mL4S1h50tfCtDfJsxZ0q4m/TUc/mrxrMftSg+yd7K0YKL67zqe8RJXVqm4McEmYmK/E6+ty+QEuohiNkJUpLMutO4lVxce6pbc2jNXSDq8A9dtQj2ucu1kfyORr0cuPgcb2JTmOCcJklRnwHkNfBT0AhjDVNeHdpnhYEVvGt9Cobh6Egvg7I8ruI82fLywh9as+FXUrEDulL0GVn/tzS8h3U0YuRd3XupylsUrWT1VxFQ4F14QheVkErRU+j+Bj5rX8je4bHq4UIl5dkJ54Nk2714/k4P82MysBFnyzNgQ4aSi0TQKLx+fKkSQABU72WJC9Sts01qD5VQXCnaDlB67MWRIiEpjVmC5566hbrMQpBqJXtna6J+8+GQH20sUojexQQ/y9rMTNBjJhHr6AJgOzQ8xSoz3UBiL+1rBNthDCTt0AdZmq91490pp7q7Om0I9MAnu9nvuPT6F3PJ94r0fGVUqcPYx+duBkIuLVSzyinEswtglF4fn3+C9B95UilviunFkqYcKJf0HkHIu7FI6An6uZN4V8/kaTziZdkK2j/6OowCr4WVuNbx7zDGoa0D3rPtTJl0f5ImJHdXWspxiiU03ZnMN2UlNVcigsArQvUDK5N7XvyjJFFf1E6Ui8tyIsj8qjhTuuAHVtNEB+/ybxUELdVg33YrGcxck7pcXTbS0XISGBQkkvwzjV0BauUJrQBD2Nctrpe8kGic2CyrQn9JyyHLSYWtNELWNP7NTrTMoU6U2hiBw2e/Bi7sg/HWiZo2fksYgFr9ERBqU0iCeSJ9Bjvv7AlG91RSrrSllK23cBP8WASMIsCF0RM+XRHoyqFFA+aTLf5dIvl70dGExKrY8zd4S6g2lNdtDzt1Zuvl64VzAO+dTGDZ+lxhRsBIioipymxAMC/fX0tiAoxbiVgz4S4wvYtPpqIKVoQ3dGKcF2WQfbA7AU30ofjX637gIGIZHqXh46Q6S8N/zWA9O66R78dsfhNEgccNvlX1PgXayVGY+ODwhFOgHuSD3ydCj2I6LELzlrm7UBll/Lbw26pcBSRqeaXMvpijnYgDMUet5sjy7TDXGToReI30JrdWvffJWxxUq8GxPWP2uvlNH6ho1w/VeKh/juhFfMpa5FfB2oqc2tNMFsevAyl6vr4kPHIQz1LQFHWTQaYqvuiMY4ZpWq9jTHMsf4yhoYb9dgpqJkez8tEAZ7yNGcyxzXiePMDjo0rhhhCIwUSqlPuo9nT+ShXLCydu9F/BH3Kf0YXDIFHt4jmUiP4kS+gpuQgL4vmpmVjtUfH4tBuR3fk1RjfS35T5tavtT0Pk0edhAUYBAQU8IBcyK5GMFH0FK3kBjCC9n/MGWlWaI0xcnHTh5jod2SbLWpF6Ht9f44meRPEqW3eQ5QFPxUsl7DyDNTymWMbELJnGT0twatAlp3zADVomQb6RDq+QcjG7rkODPo+0BzEW6KZI4y0+wv+RIN4WJUyTScj1MIpYQK8Q2gat6TsS1DHglZdpQWwMTHupigqUMwaR8jU7BefChy4DySzojXw/35MpW++NgDM2UFygs8leDaT2R8xgHxjiWxWDjLTA9jBRRdBLIXmjcWtOZPXXNKLh2LRdzrpQfcigQpejoh7u5q7dc+btG6YTGjPBy2vF9dUJJdwNQNQ09E+AZZIkq21FgA+QNYaC/k9rlisE5AYcMMIwWurzP2lJ6uOdXBREnp0DIpl/KiWi25j2o1y/fxPTQ9wJcslrXQdggS+bLWYqTcRAq8SVxJ7kJxXEm5U4jrR9Bo9tLIqyadrDkPoQF5gtjBim6Fpez+QpzGz2ptAMGCDbkK1NjLIJiZk2ZYbpsAuBFHC8zwCZNblbwiUcPgv5C+QRs1GktJBKhwEW/arrmZ23zkCVrd53SmnlhR+aKwxDqSimgs0lCY7jlySQmYmqClWSv9bCHmufxxMK5TcnSovzosYCYIGRKQ+/4QEm1WFv28KyKdjUrERzURGz6WV/v1f0OQsoBxtsCUoLXZ/3270D7qjnT3AFKeY92Sppib7T4Mauds9znQQx1eqBzesdOEZEdJnNtvNtatlQ5ie31YdepD+IQDR3PSE9p+K8f7Ijk5j8FdPb+y8q1mwO73B0GOiOJWX17R8ihxBi8cz9IZk8lSmTFShptMHx9HnETUXuY8WBDFkGZVFWLpZIDUkNvyZtsOsrtLdVOUAZJWEYlqWvYCqg3RA8VgDe7d1kqv39M1XJ2iEBK72DDuAIMA7m7G6EkWtdl90bG/VyTC2NWFVRgGJ242GRngYXVcnHYT5pq9oWHOunx4dNKARB9Wl3MY2Laccd3ubEeTuNWiBjjNASST+I4a6qAaLu2a0kkrw4zG/gICM3LdSuvIv1suWc/GN4EZIGn7DAWtv9zE42aRUIcwTMJF1Z7jaFlXReavtyW8PMW81BXUIpUWxcpbBl9ky1gmDBV3so25jB4JPZ2dBPlXPfGFPvBsBhw6psnopeBV7LypdoKy6UR3N3HBq5qowV09FBLS81FH0eA6pqCfqpt3jF90QpwN0DiecTXylm6bniCiIvuHwChGl1k5++rDwYifh33XPtL8z1+dHKpVz6zUStNYPUQoYLuSyDoW2At3grXRZ4qhaXa+hB8Z1LddUD+N6hs6TySfy2QYxuTMdb2mKqxcUo14LhwNRspvUDP+7LOt+E0PqknXxnM5W0nrH+CVtDaMC1cxzf+A+4leJPEUrWKNS9ThauPOmIg8Brs2thKUDcpEjuLcIMH/k2GEpsLtpiY+E0ufGaADwWN0cNmxrGpEeWwKSQu3OlLLtkCMs023ASDJwbr21QIpV72DQCm/x8KjLLAn7tmpDnyYjLKefZQ1WC9c8/YtfVaENb6OjtHrlMa45DwzdNBvKKcN7ZJvyQm1K78CzrrrYYzJoohHNJkS2Ou0njKupJZlKs+AH8opSBaPdmS1fS86457NnPCYKp1koxaLfJ7NLTRZzpBOGswYRIPZrFPJQzGGPnTJaUytzXttqj1p43GbHgmY5InIf4RmYIL4THwnE9t+htSAz6I7I3pJ6Gv9phE2hJwGRfLcGJ87UHR15X2icQa7KUixSoSjsI4NSBVTBA2Wtk6cBDJDcMwTI+uzUFk6El1nE8aYLxQ/PXPv3/yuCCTUzMJhDi8WNZ57UZjLXJk1iCJRD6HD7uEtO+La7jn01yU6o3aqCFc9r8xCIaGt6YT5BrzjR1eOF07o/txQsi42xflI2uep1SPTkvYhousA10raok5NenXRWxtSMDVzEaenBUvNOCB4Y6ep1//CWvvAgsgcbePDkJEy+RmmorCIuU2QQwTDHKUtTgxyzP5/+ankdgwhAJj32qymGbyL5H1U8bhYMRXHpcicUO1NgOL/Yn2Z5tbSKqGs+uZQNHp8nq2uNuM1WYXHh5CIH4sMleRhnxcc7GIVxLZpAi06v/gS9EgKZFFaNFkTxrzQGHcRELmoQOQdnLNtbcogLVy+B252L9fpju//CKZ4Yj6F0+LVkA531HiqT+ScTPNfvutue6pxni+c6WSBXyL/vERfYsG/oOuG4cDaGy0CQEuXsLyZikCSosGjjzdnAntQC2XlExESpWn7xPwhq8nTXp16Ma0C40GS5mghzhQCeHz+E8hJcDhYiS0kAqsFZVBluMATubO6bUgqsmK9JLm+/8wKPFZZMlIbnbx6+HWViT/8ySrn/DVspoXuQQZ0pHw8+3XQaMMmG9eXJPdNi5KOJDN1RsESvxLnSeBj3kivbkQeq3esj5qGfzYr0wWLw7u8FLJ96e+rpUa54PHM+xX+rabi0f4KnKn7bikOLmsU/yJNh1lxPP95dx0AujnrpEDhikfD1BlNFq33kQKAy9R2SBGvU8srViscIo6bfe0lD5aUuGmOdYCOY4m1VW4r/TypDhKp0ae5RPFCslaEY3eSGxojjtJl9j6TN+3SHZHgivHovq1XxZ4NplxopcL9aU418ka1YnZmVgbXdaPpb/R+bTCil8aWmIuIdbvzlymcLmXvtTwrW15bNg40fgPsYAuSSppZCgmpFXYi18xz2Vr9AJ4Ya6HdiCcYVSWhCpWhzIQAIYyZh2GTknWmdyn3rXRo/FVlvjF90nTZSXlCAJhqL7rbkPbWjb+3ZS6k3r+riInryEV4kvzxpgv4rDF97+B48WNv+v7omPNW2XXTN0Z+F2XsNA3fd5bRUJ1lFCa1B3u6DjtOg4q3LWIHCCBOk0Zdc9b2xZU8+Eg7CUki/lHRsqTX8x6tHzgkf3sfRsc1jV+1fuUWNrDTjbNfZ/NqXdcACVQSCYEfXCzTDX4oXAMNTywI/rUGh+k8SGI5RBmy4c9C7ZMXU8vUuYxHGPJtQc3URrVVd7ory1J7FwcmmJm0U3AnDfbIUc1Ehu4bvE50eE8YGcptm7LvNGGj5VdDSh1Imjaj1X793oYzCpdt5yzI3EvSGfbX+1tpiULQbzbLelAvBjGijSplc3RyXxo/AU13K4rWXdzcTZaKX8lGXDmJxozsEFM4KhK/DpAMJwfQiuve+RZxEyypjsbzhQ7XMY8yQgyn9Eu3CP+cIgw/NUBmHSSX+wjUjHx5qMtRu3FEfi7jUTidSG3nYAhbWQA4j7wm/YTQ+Podmxv7/bHB6V6oiYVXeuxd7axIuTyeA058/SwYnDtfuWcf5l0FiB0nuqGj6EuddhT1DNf7SIhwDlmJ0moqn5TBtrbFD1a8IX3Upx/TgYfdqZRs4hIPdS7VL5F2Gvav5r6g5LUB3W2MOocBuBx/5J3Iosu4/BSrnZsJPMe+ECAAC7G4Whj49brIax88QmUZ1VSer3fSvSixwLVmHyxaPLb0fZahifSr31YzkWhedOWIG44ULE/FElzTQNS95iJGjW4NrRL2gT7PwlRIW2dZnUAyOuOKMiwjXIEllies2BxBLBrI+P/zD92/BNTwgpR+Rlp1702+S/qQs8ebRzkjEtrxeWDN4dzZwcSpyfy32V6TMmCCUvntKVOMxbuDfBrna10wTfV3+VyDd+qxLocPouoNsmNTELYwCuRNBHvmoNBQVsm3xaqOW95gBrWtrd6Lygk0zPELV5zUsFAOD5JcQ/iWswXcsVQ2UpnTX4+Mo+b/sLuX6gUrlSmSoiSCnuCFrCXxcghb+ZOpG0MgrbJvtLZ5vVGTnOzT56Fgy0rmV134z+VT6gKc+0mJGJaq+C43xLJi1bPruGf2wI6u2yLI/U8QrbU8yZ/7M+K7R5hMWgyua1xIbIHLctM/sOkXHw0H2TulMLr9HuC8JoaSCIKu/C9yNby1Vz5ROstBd7Ym9OEpVacsL3UVE7GY2UQTa30onF1s3HBrhjpvRYh1a6dMDJUcluFqienXoy+AOXXVbq/ANQ9F2dKT1qmpPFWLXHiAqv2bXgMP47ksNPYaIMjPUaJmJMjgP6iyNwtlM1yr5Mzs/iBtxWmuTZ+sVQG+ZkEntn7xHyk3G2nP9VFeEYt/sIKZTD5W6JGsj3zQDXmlQo434zBlEBCYu80SZtDSYH9mEbIMcx8N93s1kLDZydnleoQEqcOEwu0PrpKC8VHaR5DVpv/a0lTvfr6bE+LQxJrdDjenXGQhF4OskKqXEJ7WL6rk4j3EQHL5Gvl+z3n8RXE8quGL1Q1pfda4MEKp/L84gXF39enOxVwm9mzKmDK0uSxvnUz4OpEs1LWqDoOZiyYBKZXTOl7SsnD6afMe1cPGd7fEboR2FoHVXpSwnjFQSjOmN4EjpG+VTEaV2Kle1Yohy4L8sjEMjBJDW5HLU318tlXrFC3K8PAolTA+N+pf4L1cAnoHtkYI7+ijNjrnahGC8YSUqFVp7b8n88mRanLd6128BBgcPZLTvzH5lPSE+LuYbg1QjGT3TtnJRP3ZBO8IZdP2d6pyngD/LkqunL2j70yRcjNq+9RKGgUD/6ccCDaGLb4nYY6uev08O9baF6GWFh+q27Z4EIISlgEdkXj5vY0Rqn03wBeOQ0wrW3KLFYomKdN9M5dTO6X4aLEFXW3Xq50OD4jqabv5IZuW+gp7U6vNfGQqmtuFUWe4TCHZg4gBPsLSrzu0ZTtcssS09oDQI7GVYTVzalUWEmRRnQjk2onUQWoHhXcRV6wFPiRsOhOBpc4AD/+RXCt0Qvd5Af7YC2RSkkodv6iu3Emb/TFGMD3Z1qSyVu6rIGOLWfTY9iYA+b8oKolkOCF4eu38GRdU9f0EuaOaMp7aCDhndkhoIEvBRRMQzfPD50459qNjneV+G9YFI+qBkqaXxq7cxwoNb7d6Ipxlm3cpsKvBHbQWas+5NwFNZGd71Uq7o6aRwRlVtXScqNv1Zxg1+9Gzdt9f+jzvptVVKnUakHYtTp3pD996aLvwLI5LVUB/AE8pVxsREyR7/kHxAc+4T4miMuqv2kk89jaiT40/h+bRNFeylj6s1y+ThWC2KabM40qmNKhI5T2uoCbYRwtdPuC3JlwPdUOSX9gT5lKa2p5c2eRxBE58rLp+2Q5JGQTGogKB7VY9wf3SJByHU7ZzTuZfmTSvWeC1yxZbPv9pb9dQRDDyEyO+gVC7A+mbfHSYIWwWkZT29ren00flaxoGKXQytmCOVJduCgarz7mYndmFAE5ioxKPvVfisEePqZBdwkx8rIpXpbcZBl2DyEG3hxWVp+COp9yqGYKGnTYP16TAJcY0Jopxydpdt52tvQ9CPtL9pC50r5SHHD/5yIdXtbLuPktX4OTsF775ukWl32uDoEIoV4MnzKq0kcvnGlXvtSBRZMrtuY19EkQprpLLUdZrmnHCoz6OnkpdH3MJ3b+gBmPolY/Xkhf5sCJ/snWEtzeJNYXwnAUfNf7wAhXyEkPYAuaZ4LQWpYfHrlF7HZ8EfypqPAAblbWuhgrPTfa7KfPkuiaAsDLftmcNwWFAznwAekivclD4NIhtX2Pl2kjxZ201sp7ouoEuH6xhceTgpVL0WEq82VSXHi8QSfRBNO5H9dGQzUVwVKTUdsKmHbUZ/UjR0DPCbWtY5IHoBFJttGTrobEyJg6gEur82SQa/e01Y450lz3Ea4PUKVlm/GnpPOd8blSbfP3+0jvfaF3ABgl/qwO+54+E0kUTn/EHJMcib61/GHmqOcaPz25dyEc8O0oNCjOFdtW4Q2b4OWn/qAxwYOCZjO+qzJ822+hkY494fFigHIY6pOKrE3b2PdtEql7hizl1UtvxT9mzAvwoimguAoKfYI/Zvq7RygOpagcc+ssG03lvfaPbUangHR0LLC88dQpMVymkj90ylLugE1Oz8C2T0pGhJ5FSjBi7Bq7qED+2G1aLk6YKMHJKGc7FwiyAy+IsUJIC/ke5K//CBrEuODWSmz6DvMpR+cEfVOHaGWOwUqPboxGRZRQhdiJoEWR2naqhGkuQKEj6PkoaYNz0+8Fj35pkgASeS6T2wFkja3Ef3d14XHFO5CvRYpNE9eZFlRAiLEC28oxRGxlx/aLTB+vv5ionuF4Y99JvoXO1FsMoskaSrat6wAo1MMKGE85qEsMAVdEfJtMNWLNWKLG5wCoJm+TkSMbFZ8dglhAh6UuJnrKBWBrQOptpI0jNtz12KniZbcEpTSF+evLUDNFm+XN8pmL3SBbzb1fv/7oDBlxnWj+pZmtyTmIP7FwJ1hsMTI5DRrFkDknMRqHZjAdkfzNy4DmjVmSTM+4NJ9vXmQ1yTIdLc3gDL6ASK4K4bKpuNZSptsAQJbSaARR/Eslqls/GQf8MGdW/2Ys5nDCSMQ3NzXUx13MVFF8vvT3BNGA7VN1/1B5ROPsUc8QatYqgzgf0XNz5weWJbTlnrUAWoir7bls0OKy3Gy4GbNoeNbFV8mNt0ZdUuc4JvzIvvQzwiqAL4UGrqiCwHKv3WMgYkDYNfTlYE+U9EKjT/v07TLgRseXu6JZmJqUqmyqyqI6oKu+P2mAk8C9HKMacVGmN16IoS3RlajEVM3gj3a8hfNIlJO3s3Tp2OUXedbkn7lVod/0zLEjxy3Tof/ErLtQ2E7RtCwakGeTV9/HIUjbbFNnu5BNSt+0mjERSVX+dlLqf4jEGFzTfDBmsK8ZNlo5wUnGniAK/O+Rqgl5xrvEIUrEW0KIhN+FRxhiKiNqnMGGPAgVWv8fsIjYGD3EuI+KILH6OzWmg8E5gvj8BufizN3vrhByYy9Lvvou2qXj1RNJVO4ibbQ5DsDYAkGZeKNPzgXwDmuv0eMWfjIvdQr7sPFa5/lSwRLgvlLPG8S3oGSPHxuXFXcezgVS+fwXdyqhJMHSEUpC+JeeJDA1qRR6XvyrlOMzmnjHvTB+XPT7EDSaHb73QP7afmpvwTH22tzJadso370F/2EkjIP+S59PiVs0unxBqhQzVLqvpAM/+VQrmb1Sg/vW1gLT/y0Opck0dbgL8DBaNgW3nqJfhAqgLCyu8r+5k1bmbbfYxU4zOzh88v1KODQUrD9jHcBkGX7j1h0vQzuxL1fHh0UCxaFkytDLgVEu04aIdS7XU8I=" />
</div>


<script src="/js/dnncore.js" type="text/javascript"></script>
<script src="/js/dnn.modalpopup.js" type="text/javascript"></script><script type='text/javascript' src='/DesktopModules/Exionyte/Menu/1/Xeon/Scripts/menu.js'></script><script type='text/javascript' src='/DesktopModules/Exionyte/Slideshow/2/Xeon/Scripts/jquery.cycle.all.2.74.js'></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { $('.slideshow').cycle({cleartype: true, cleartypeNoBg: true, fx: 'scrollHorz',timeout: 6000,prev:   '#Prev', next:   '#Next'}); var a1 = document.getElementById('arrows'); a1.style.display = ''; var b1 = document.getElementById('bannerPane'); if (b1) b1.style.display = 'none'; });//]]>
</script>

<script src="/js/dnn.jquery.js" type="text/javascript"></script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3a4d4da635-3815-4b70-912b-f389ade5df37%3aea597d4b" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="WW+yUA8w3uA+MhqsUX1I44VVx7unE0oO6PmaR5YIfaw/fj0zmlMOExI/dQ9Jt3GgN2tqBzU1ANNykojJoTM5sEbnQ4ro/pM1FNLNlpapqZQ7ylNryxQpWIXdIVo161LICAYWTziGbBzJLWENW7Z7peUblKt7x3TmAb2rhxBGrnBD2UtdgKueQRISvoP1FJctrky+qJ3QSSAfVoo02EtQrXL20crjG/cwurnZZg==" />
</div>
        
        
<script src="http://cufon.shoqolate.com/js/cufon-yui.js?v=1.09i" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Futura.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Font.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/jquery.pngFix.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/general.js" type="text/javascript"></script>
<div style="display: none;" id="message">
    <div>
        <h4 style="text-align: center;">
            Sorry, This page dosen't support Internet Explorer 6, If You'd like to view all
            the contents properly please upgrade your browser to IE7, 8 or 9. <a href="http://www.microsoft.com/download/en/details.aspx?id=2">
                Click here to get the new version of Internet Explorer</a></h4>
    </div>
</div>
<!--flash section-->
<div id="flashPane" class="flashSection" style="display: none;">
    <div class="flashPadding">
    </div>
    <div id="website" class="flashContent">
        <p>
            In order to view this page you need Flash Player 10+ support!</p>
        <p>
           
        </p>
    </div>
</div>
<!--/flash section-->
<div class="skinLayout">
    <div class="skinWidth">
        <!--header-->
        <div class="skinHeader">
            <div class="skinMinWidth">
                <div class="logoLayout">
                    <div class="logoPosition">
                        
<div style="float: left; display: inline;">
    <a id="dnn_dnnLOGO_hypLogo" title="North Maharashtra University, Jalgaon" href="http://nmu.ac.in/en-us/home.aspx"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/0/logo.png" alt="North Maharashtra University, Jalgaon" style="border-width:0px;" /></a>
</div>


                         <div id="dnn_logoPane" class="DNNEmptyPane">
                        </div>
                    </div>
                </div>
                <div class="menuLayout">
                    <div id='menu'><ul class='menu'>
<li class='current'><a class='parent' href='http://nmu.ac.in/en-us/home.aspx'><span>Home</span></a></li>
<li><a class='parent' href='http://nmu.ac.in#'><span>About Us</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity.aspx'><span>About University</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/misson.aspx'><span>Misson</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/universitysong.aspx'><span>University Song</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/informationatglance.aspx'><span>Information at Glance</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/achievements.aspx'><span>Achievements</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/vicechancellorslist.aspx'><span>Vice Chancellors List</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/annualreport.aspx'><span>Annual Report</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies.aspx'><span>Governing Bodies</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/managementcouncil.aspx'><span>Management Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/academiccouncil.aspx'><span>Academic Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/senate.aspx'><span>Senate</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/deansoffaculties.aspx'><span>Deans of Faculties</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>University Campus</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/aboutcampus.aspx'><span>About Campus</span></a></li>
<li class='child'><a href='http://nmu.ac.in/kamc'><span>Khandesh Archives and Museum Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/centralschool.aspx'><span>Central School</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/campusmap.aspx'><span>Campus Map</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/howtoreach.aspx'><span>How to Reach?</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/extensionactivities.aspx'><span>Extension Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/researchfacilities.aspx'><span>Research Facilities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/mous.aspx'><span>MoUs</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/home.aspx'><span>Satellite Centers</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/eklavyatrainingcentrenandurbar.aspx'><span>Eklavya Training Centre, Nandurbar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/rcp'><span>Pratap P.G. Research Centre of Philosophy</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols/en-us/research/labtoland.aspx'><span>Pratap Shashvat Adhunik Sheti Tatvdyan Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/mahatmagandhiphilosophycentredhule.aspx'><span>Mahatma Gandhi Philosophy Centre,Dhule</span></a></li>
</ul></div>
</li>
<li class='child'><a href='/LinkClick.aspx?fileticket=oqDHHmONXsE%3d&tabid=3881&language=en-US'><span>RGS and TC</span></a>
<div><ul>
<li class='child'><a href='http://apps.nmu.ac.in/circulars/Bcud/29-09-2015%20Submission%20of%20Pre-proposals%20under%20the%20scheme%20assistance%20for%20S%20and%20T%20applications%20through%20university%20system%20of%20RGS%20and%20TC,%20Govt%20of%20Maharashtra.pdf'><span>RGS and TC Schemes</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Information</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Academics</span></a>
<div><ul>
<li class='child'><a><span>Schools</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/soes'><span>School of Environmental & Earth Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols'><span>School of Life Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/socs'><span>School of Chemical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sops'><span>School of Physical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docs'><span>School of Computer Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/domaths/'><span>School of Mathematical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/udct'><span>University Institute of Chemical Technology</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docll'><span>School of Languages Studies &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doe'><span>School of Education</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doms'><span>School of Management Studies</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soss'><span>School of Social Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soah'><span>School of Arts and Humanities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/schools/schoolofthoughts.aspx'><span>School of Thoughts</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/dat'><span>Buddhist Study and Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/smcrc'><span>Chhatrapa Shivaji Maharaj Chair & Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/mpsrc/'><span>Mahatama Phule Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/svsrc'><span>Swami Vivekananda Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/grc'><span>Mahatma Gandhi Study &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sgsk'><span>Sane Guruji Sanskar Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/bcsrc/en-us/home.aspx'><span> Kavayatri Bahinabai Chaudhari Study and Research</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/nmuccc.aspx'><span>NMU-CCC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/phd.aspx'><span>Ph.D.</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation.aspx'><span>Adult Education</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/eventsandactivities.aspx'><span>Events and Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/coursesoffered.aspx'><span>Courses Offered</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/tbc'><span>BACECC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/committees.aspx'><span>Committees</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Administration</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/registraroffice.aspx'><span>Registrar Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/administration.aspx'><span>Administration</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/lawrtisection.aspx'><span>Law/RTI Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/faooffice.aspx'><span>FAO Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/examinationcenter.aspx'><span>Examination Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/eligibilitysection.aspx'><span>Eligibility Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/sportssection.aspx'><span>Sports Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/scstcell.aspx'><span>SC|ST Cell</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/constructionsection.aspx'><span>Construction Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/prosection.aspx'><span>PRO Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/approvalsection.aspx'><span>Approval Section</span></a>
<div><ul>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Online Approval</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/computercenterexam.aspx'><span>Computer Center (Exam)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/universityofficers.aspx'><span>University Officers</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=ZprmAC3RMhY%3d&tabid=1350&language=en-US'><span>Phone Directory</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/rd/functioningofrdsection.aspx'><span>R & D</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/developmentsection.aspx'><span>Development Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/affiliationsection.aspx'><span>Affiliation Section</span></a>
<div><ul>
<li class='child'><a href='http://affiliation.oaasisnmu.org/'><span>Online Affiliation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/affiliationsection/licreports.aspx'><span>L.I.C. Reports</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/research.aspx'><span>Research</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/iqac.aspx'><span>IQAC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/avishkar.aspx'><span>Avishkar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/bsrschemeprogrammee.aspx'><span>BSR Scheme/Programmee</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Student Corner</span></a>
<div><ul>
<li class='child'><a><span>Academics</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/admission.aspx'><span>Admission</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/syllabi.aspx'><span>Syllabi</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/questionbank.aspx'><span>Question Bank</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/academiccalendar.aspx'><span>Academic Calendar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/papercodelist.aspx'><span>Paper Code List</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac'><span>e-Suvidha</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://exam.nmu.ac.in/'><span>Examination</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examtimetable.aspx'><span>Exam Time Table</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlineresultscollege.aspx'><span>Online Results (College)</span></a></li>
<li class='child'><a href='http://182.56.2.247/nmu/'><span>Online Result (Student)</span></a></li>
<li class='child'><a href='http://www.nmuexams.in/NMUEXAMS/LoginScreens/frmStudentLoginPage.aspx'><span>Application for Photocopy/ Verification/ Challange</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/resultsofverificationredressal.aspx'><span>Results of Verification|Redressal</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/convocation.aspx'><span>Convocation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examreports.aspx'><span>Exam Reports</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlinedegreeverification.aspx'><span>Online Degree Verification</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac/QPCourseSelection.aspx?ID=660'><span>Previous Exam Question Paper</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examination.aspx'><span>Examination</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Facilities</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/financialassistanceschemes.aspx'><span>Financial Assistance Schemes</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails.aspx'><span>Hostel Details</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/hostelday.aspx'><span>Hostel Day</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/boyshostel.aspx'><span>Boy's Hostel</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/girlshostel.aspx'><span>Girl's Hostel</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Services</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Download  Formats</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/examinationsforms.aspx'><span>Examinations Forms</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Formats For Colleges</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforemployee.aspx'><span>Formats for Employee</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formsforstudents.aspx'><span>Forms For Students</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Statistical Format</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline.aspx'><span>Student Helpline</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline/studentfacilitationcentre.aspx'><span>Student Facilitation Centre</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studentwelfare.aspx'><span>Student Welfare</span></a></li>
<li class='child'><a><span>E-Resources</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/eresources/ejournals.aspx'><span>E-Journals</span></a></li>
<li class='child'><a href='http://nptel.iitm.ac.in/'><span>NPTEL IIT Lectures</span></a></li>
<li class='child'><a href='http://apps.webofknowledge.com/'><span>Web of Science</span></a></li>
<li class='child'><a href='http://www.sakshat.ac.in/'><span>NME-ICT</span></a></li>
<li class='child'><a href='http://jgateplus.com/search/'><span>JGate</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Useful Links</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/events.aspx'><span>Events</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/tenders.aspx'><span>Tenders</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/rti.aspx'><span>RTI</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/circulars.aspx'><span>Circulars</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pressroom.aspx'><span>Press Room</span></a></li>
<li class='child'><a><span>Job Openings</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/university.aspx'><span>University</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/collegesinstitues.aspx'><span>Colleges/Institues</span></a></li>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Apply Online</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/sciencepark.aspx'><span>Science Park</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=2wk8wKpV9MI%3d&tabid=1088&language=en-US'><span>Citizen Charter</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/publications.aspx'><span>Publications</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pbasproformaandapidetails.aspx'><span>PBAS Proforma and API Details</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/otherlinks.aspx'><span>Other Links</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Us</span></a></li>
</ul></div>
                </div>
            </div>
        </div>
        <!--/header-->
        <!--banner-->
        <div class="lightBlueCenterRepeat">
            <div class="lightBlueLine topLine" style="z-index: 101; position: absolute; top: 0px;">
            </div>
            <div class="lightBlueLine bottomLine" style="z-index: 101; position: absolute; bottom: 0px;">
            </div>
            <div id="bannerLeft" class="lightBlueBannerLeft">
            </div>
            <div id="bannerRight" class="lightBlueBannerRight">
            </div>
            <div id="bannerCenter" class="lightBlueBannerCenter">
                <div class='slideshow'><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage5.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage6.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage7.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage8.jpg' alt=''/></a></div></div>

            </div>
            <div id="bannerPane" class="banner" style="display: none;">
                <div class="skinMinWidth">
                    <div class="contentPadding bannerPadding">
                        <div class="top-cols">
                            <div id="dnn_Banner_Pane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Banner_PaneLeft" class="colHalf left DNNEmptyPane">
                            </div>
                            <div id="dnn_Banner_PaneRight" class="colHalf right DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/banner-->
        <!--banner arrows-->
        <div class="skinMinWidth">
            <div id="arrows" class="arrowPositioning" style="display: none;">
                <div align="center" class="arrowLayout">
                    <div class="lightBlueArrowBGLeft">
                    </div>
                    <div class="lightBlueArrowBGRight">
                    </div>
                    <div id="Prev">
                    </div>
                    <div id="Next">
                    </div>
                </div>
            </div>
        </div>
        <!--/banner arrows-->
        <!--slider panel-->
        <div id="toppanel" class="topPanelLayout" style="display: none;">
            <div class="topContent">
                <div id="panel">
                    <div class="skinMinWidth">
                        <div class="top-cols">
                            <div id="dnn_Content_TopLeftPane" class="col1 left DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopRightPane" class="col3 right DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopCenterPane" class="col2 DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Content_TopFullPane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottomPanelSpacer">
                </div>
            </div>
        </div>
        <!--/slider panel -->
        <!--slider nav-->
        <div class="fullWidth">
            <div class="centerSliderPanel">
                <div id="toggle" class="topSliderLayout" style="display: none;">
                    <div id="close" class="open sliderOpen">
                    </div>
                    <div id="open" class="sliderClose close" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <!--/slider nav -->
        <!--content header-->
        <div class="skinContent contentTop">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="contentPosition">
                        <span style="color: #fff;">n</span>
                        <div class="contentPositionLeft">
                            <div class="loginImage">
                            </div>
                            <div class="displayText paddingText">
                                
                                <a href="http://apps.nmu.ac.in/Login.aspx" target="_blank" class="lightBlueContentLinks">
                                    Portal Login</a>
                            </div>
                            <div class="loginImage">
                            </div>
                            <div class="displayText">
                                <a href="https://login.microsoftonline.com/" class="lightBlueContentLinks">Mail Login</a>
                                </div>
                        </div>
                        <div class="contentPositionCenter">
                            <div class="paddingText">
                                <strong>अंतरी पेटवू ज्ञानज्योत</strong></div>
                        </div>
                        <div class="contentPositionRight">
                            <div class="displayText paddingText">
                               Select Language :
                                    <div class="language-object" >
<select name="dnn$dnnLANGUAGE$selectCulture" id="dnn_dnnLANGUAGE_selectCulture" class="NormalTextBox">
	<option selected="selected" value="en-US">English (United States)</option>
	<option value="mr-IN">मराठी (भारत)</option>

</select>

</div>

                            </div>
                            <div class="lightBlueContentLinks searchText">
                                <div class="searchTextLayout">
                                    Search&nbsp;&nbsp;
                                </div>
                                <div class="searchImage">
                                    <div class="search_bg">
                                        <span id="dnn_dnnSEARCH_ClassicSearch">
  
  
  <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" onkeydown="return __dnn_KeyDown(&#39;13&#39;, &#39;javascript:__doPostBack(%27dnn$dnnSEARCH$cmdSearch%27,%27%27)&#39;, event);" />&nbsp;
  <a id="dnn_dnnSEARCH_cmdSearch" class="dnnSearchCss" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/_default/Skins/Xeon/images/sright.jpg" align="top" border="0" alt=""></img></a>
</span>


</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content header-->
        <!--content containers-->
        <div class="contentContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane1" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane1" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1" class="col1Home left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane1" class="col3Home right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane" class="col2Home">
                        <div class="DnnModule DnnModule- DnnModule--1">

<div class="containerPadding"></div>

<div class="overflowHidden">
    <div class="lightBlueBG">
        <div class="style1TitlePosition">
            <span id="dnn_ctr_dnnTitle_titleLabel" class="style1Title">Privacy Statement</span>



        </div>
        <div class="style1Arrow"> 
            
        </div>
         <div class="clear"></div>
    </div>
    <div class="spacer"></div>
    <div id="dnn_ctr_ContentPane" class="style1Content style1Border lightBlueContent"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>North Maharashtra University, Jalgaon is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the North Maharashtra University, Jalgaon Web site and governs data collection and usage.
        By using the North Maharashtra University, Jalgaon website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. North Maharashtra University, Jalgaon also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by North Maharashtra University, Jalgaon. This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by North Maharashtra University, Jalgaon for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the North Maharashtra University, Jalgaon Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through North Maharashtra University, Jalgaon public message boards,
        this information may be collected and used by others. Note: North Maharashtra University, Jalgaon
        does not read any of your private online communications.</p>
        <p>North Maharashtra University, Jalgaon encourages you to review the privacy statements of Web sites
        you choose to link to from North Maharashtra University, Jalgaon so that you can understand how those
        Web sites collect, use and share your information. North Maharashtra University, Jalgaon is not responsible
        for the privacy statements or other content on Web sites outside of the North Maharashtra University, Jalgaon
        and North Maharashtra University, Jalgaon family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects and uses your personal information to operate the North Maharashtra University, Jalgaon
        Web site and deliver the services you have requested. North Maharashtra University, Jalgaon also uses
        your personally identifiable information to inform you of other products or services
        available from North Maharashtra University, Jalgaon and its affiliates. North Maharashtra University, Jalgaon may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>North Maharashtra University, Jalgaon does not sell, rent or lease its customer lists to third parties.
        North Maharashtra University, Jalgaon may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, North Maharashtra University, Jalgaon
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to North Maharashtra University, Jalgaon, and they are required to maintain
        the confidentiality of your information.</p>
        <p>North Maharashtra University, Jalgaon does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>North Maharashtra University, Jalgaon keeps track of the Web sites and pages our customers visit within
        North Maharashtra University, Jalgaon, in order to determine what North Maharashtra University, Jalgaon services are
        the most popular. This data is used to deliver customized content and advertising
        within North Maharashtra University, Jalgaon to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>North Maharashtra University, Jalgaon Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on North Maharashtra University, Jalgaon or the site; (b) protect and defend the rights or
        property of North Maharashtra University, Jalgaon; and, (c) act under exigent circumstances to protect
        the personal safety of users of North Maharashtra University, Jalgaon, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The North Maharashtra University, Jalgaon Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize North Maharashtra University, Jalgaon pages, or
        register with North Maharashtra University, Jalgaon site or services, a cookie helps North Maharashtra University, Jalgaon
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same North Maharashtra University, Jalgaon Web site, the information
        you previously provided can be retrieved, so you can easily use the North Maharashtra University, Jalgaon
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the North Maharashtra University, Jalgaon services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon secures your personal information from unauthorized access,
        use or disclosure. North Maharashtra University, Jalgaon secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>North Maharashtra University, Jalgaon will occasionally update this Statement of Privacy to reflect
company and customer feedback. North Maharashtra University, Jalgaon encourages you to periodically 
        review this Statement to be informed of how North Maharashtra University, Jalgaon is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>North Maharashtra University, Jalgaon welcomes your comments regarding this Statement of Privacy.
        If you believe that North Maharashtra University, Jalgaon has not adhered to this Statement, please
        contact North Maharashtra University, Jalgaon at <a href="mailto:support@nmu.ac.in">support@nmu.ac.in</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
</div>

<div class="endContentPadding"></div>

<div>
    
    
    
    
    
</div>

<div class="containerPadding"></div>

</div></div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1Inner" class="col1Inner left DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPaneInner" class="col2Inner DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane2" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane2" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane2" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane2" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane2" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane2" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane3" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane3" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane3" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane3" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane3" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_CenterPane3" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content containers-->
        <!--spacer-->
        <div class="spacer">
        </div>
        <!--/spacer-->
        <!--footer line-->
        <div class="footerLineTop">
        </div>
        <!--/footer line-->
        <!--footer containers-->
        <div class="lightBlueFooterContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Footer_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_HalfLeftPane" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_HalfRightPane" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_LeftPane" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_RightPane" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_CenterPane" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
            <div class="lightBluefooterBottom">
                <div class="skinMinWidth">
                    <div class="contentPadding">
                        <div class="footerPosition">
                            <div class="BottomLinks footerPositionLeft contentWidth">
                                <div class="displayText">
                                    <a id="dnn_dnnPRIVACY_hypPrivacy" class="BottomLinks" rel="nofollow" href="http://nmu.ac.in/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx">Privacy Statement</a></div>
                                <div class="displayText">
                                    &nbsp;&nbsp;|&nbsp;&nbsp;</div>
                                <div class="displayText">
                                    <a href="http://nmu.ac.in/disclaimer.aspx"
                                        class="BottomLinks">Disclaimer</a></div>
                            </div>
                            <div class="footerPositionLeft bottomContainerWidth">
                                <div id="dnn_BottomPane" class="DNNEmptyPane">
                                </div>
                            </div>
                            <div class="footerPositionRight contentWidth">
                                <div class="displayText">
                                    <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="BottomLinks"><a href="copyright.aspx" style="color:#fff;">Copyright</a> 2012 by North Maharashtra University</span>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/footer containers-->
    </div>
</div>
<script language="javascript" type="text/javascript">
    function Showie6Message() {
        var IE6 = (navigator.userAgent.indexOf("MSIE 6") >= 0) ? true : false;
        if (IE6) {
            document.getElementById("message").style.display = "inline";
        }
    }
    Showie6Message();
</script>

        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" />
    </form>
    <script type="text/javascript">
        
        //This code is to force a refresh of browser cache
        //in case an old version of dnn.js is loaded
        //It should be removed as soon as .js versioning is added
        jQuery(document).ready(function () {
            if (navigator.userAgent.indexOf(" Chrome/") == -1) {
                if ((typeof dnnJscriptVersion === 'undefined' || dnnJscriptVersion !== "6.0.0") && typeof dnn !== 'undefined') {
                    window.location.reload(true);
                }
            }
        });
    </script>
</body>
</html>
