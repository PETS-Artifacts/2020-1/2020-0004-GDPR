<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="en-US" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><meta content="text/javascript" http-equiv="Content-Script-Type" /><meta content="text/css" http-equiv="Content-Style-Type" /><meta id="MetaDescription" name="DESCRIPTION" content="Official Website of North Maharashtra University, Jalgaon" /><meta id="MetaKeywords" name="KEYWORDS" content="North Maharashtra University, Jalgaon, Maharashtra, Higher Education, Academic Institute, Research Organization, University, NMU, India, Maharashtra, Research, Under Graduate, Post Graduate, Certificate,Courses, " /><meta id="MetaCopyright" name="COPYRIGHT" content="&lt;a href=&quot;copyright.aspx&quot; style=&quot;color:#fff;&quot;>Copyright&lt;/a> 2012 by North Maharashtra University" /><meta id="MetaAuthor" name="AUTHOR" content="North Maharashtra University, Jalgaon" /><meta name="RESOURCE-TYPE" content="DOCUMENT" /><meta name="DISTRIBUTION" content="GLOBAL" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><meta name="REVISIT-AFTER" content="1 DAYS" /><meta name="RATING" content="GENERAL" /><meta http-equiv="PAGE-ENTER" content="RevealTrans(Duration=0,Transition=1)" /><style id="StylePlaceholder" type="text/css"></style><link id="APortals__default_default_css" rel="stylesheet" type="text/css" href="/Portals/_default/default.css?7230930" /><link id="APortals__default_Skins_Xeon_skin_css" rel="stylesheet" type="text/css" href="/Portals/_default/Skins/Xeon/skin.css?7230946" /><link id="APortals__default_Containers_Xeon_container_css" rel="stylesheet" type="text/css" href="/Portals/_default/Containers/Xeon/container.css?7314682" /><script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery.min.js?1.6.1" ></script><link href="/Portals/_default/Skins/_default/WebControlSkin/Default/ComboBox.Default.css" rel="stylesheet" type="text/css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico' type='image/x-icon' /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/CSS/menu.css" rel="stylesheet" type="text/css" /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/Colors/lightBlue.css" rel="stylesheet" type="text/css" /><title>
	North Maharashtra University, Jalgaon > Home
</title></head>
<body id="Body">
    <script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery-ui.min.js?1.8.13" ></script>
    <form method="post" action="/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9hhPq2LgVlHLdBeuuX8gHSpBhtGPjoYBeYk+B6XKKbheD6SNONeUcinzSqLhXx/JXkRKn5BMDmC/qwv2Fn4Xw3NiUy9tSD5emb39vXvkKQLWJOaWlU5rlOyceiM6ibpDSwLljhqOD3pVdliTvh6Lxhze2m/rjMtYXI0nI8Q9S1IYCtmXTqc2o+Ld1ok0/0NUEPqZyHRW54xQFRypIw4y3zQuZ6YENBEp20hi9NYpddB+iWxpyY61cEGAp9KBNR1BFfbnsemFcXUXs3mkYyVmFKaqtlosBykPuFBzxhMn6+pp1Uei1+yVCXQtl9WHmArQNOLvG0vhHj/605YU2Oee+vWEODjcqoFKZbDtunruCQlSlm7+LuWrPFA9FR+JK/k4SeepltLQ+eklRTDI9aJECiN9zyUVli1faD82msYuItVYFAOqjU52rRZ9R7fb1X4IRdIv6fus5m9tiYdWwAaYrKO96KRpvUCmO25QAzjg/QNpibPTSW4pxfhJejHRgpGBiel1MYDtK7sZGk3jLxTcENNhT2QkbkoDF+7Iqzisc1FIwD807SllwPmjC948oK0EW3uTiYoju+9q3I2bTUqlXSq7ZzgtGq9thjCaFtea2lDM5+PaOU+chsEM2Eo+4lqYv+EWKd2mFIghwmmMqjKbxwUNQpzOjjxB8GZhBO396mubMGX9yk5W5AEst8aqPidy3kJarK6BNvYwsHMTKF+dEXIDst1qMlnk6X4rC8FdQXmXmsSMJsvyweSAUu6IhieFU6MQFF+xzX7cXcoIMsu/FqYDN93gZPX87Ze98AIeDBhDnMr5fUxf0Be+s31lbsIYkiAHvUshQH8Be28RVISXTD3lVbtoO9qJ961MdnN4t+lz+3s1uQ0bJkUx98pWcdGDDPXRFmom48hZNuOBdLGNKGePIyHh2dRqaLhLunQllgs6QxCuGmklEiw6FkhjihSzaGtfN4c4nQkrSOIVqy9TUeY96kQBWeNurFZCW6KaCVI3h+UCrhwwmvB1mL97GSHfZS7N9w7AB5aNDMJhBPQmDww5L5ceFQmbwOTivkWyEXlffEOq4Wu6I9Zqd0DLhvShrbWpZ2YCOqGF1wlFGuZKAG+RXf0j0mXOw9j1YzbYwQH1boKQUKRYNAYUXpMO1mYMxbQ0jR9ML0vWxrWrUK3jplN9cFvrGjlq83elo/GiXFTq1xzftQRbKAbwwaMa+8vvgcdIFVoA180DagZ3DP3Nvit8zDty/atHNWgNU2vrNUosTKlgvkAFIcbehQRdUi/TycFZyZ5d+CCDp2PtaBQYR3tmf7+kOJTfFbs5zKOqOrIGzlHXLBUDD5ssEu0OcZIKa3Qa1DZIEiUGsbqviiSKhdzbbv9eOapYceO9ksLsOkYbWmW23OGHuw3FFseDUvWYs6w3QspcvMYE97uEfZcRXk38wB6Zr4SbaFhMRwKmxdGIwoj23LBL6nFl6Z0a5P3oZGdBZWf4oUVwFPmHZ8lPpvpbH59xIfuZnhEXxyaG+ezs589cK/mjsOdHmcWt1T8/3wfAtXg42yDjNc7A5AZ7oqbnwPWtW9DyU0DX8HN4tKyX9BFFmSDXPAchptR9ezdpjghYWBVZqDrk0ePKTpbV77W7FlhfiIrZ5G46YtAzmF1v4i219+Ts6cnlU8Xq+Er6ZzJkqSIPmmbwgdDiWMFqbCn/qwxFT5O8DD5+vV5AsLa32YLLrsYxW7/c8kxemnFckGLuzvmp10QRkhNBMtCVHD0k2tMQJege8Qmfeg5ztR6mE7f4qVHRMi/eXvMAfISkHSRZuyNNT7i0048zUVbxybsWCJYq+Yjn4i6kF5PLDXPMSl57xGq5/qPVSR+ONCh7H5jvkW3BokOGdRvHa1DSkXBX5AYmyhX7nm8TLW2qYPfqaQD/J39bPbMgNGtMQWHhzF5VyvFP9AqLdDBszZhrxW2Hr8xUzsf8dzNVP+4OegP/lqkPHrH4o0navw69I0jk/eRT3j4lFn7MBxZOjQgBCRzusG27Y5ULXpcWR2lnA5O3DkbzZAuvZ4r1F2LLZqqzyFcF8FPrI2dBj2ipAOU0JLeafx7pZ4KjzP539d4rn6xJBBaBEkV65RClZFi4YcKk6+4u42tiIMk7Cx5i/jJ4pD1jtnn9jwh6rNpkBuaqWN3XxmcYUAe8YtgRm46PB9fvJCdl8fz0YUfAjdOur+NYgqZVQxMkdXTSAAcEarhh6gNXjzn5AESnNGJhy5V1jr7yK0gAZnOL5zqVdDI2Er4vREevhe4UsbjYk/VopA3k+xgz290AAZDVh1B2LvAQNrBMbo9A/6PFrPIyW48/9Zn1jUBRSGzNamB5CiqcHl1Y8Ojjg9XvACnxB8XZqRL83YL/eLbLV95gnsti/dp8EzpOe2ruYZbAcpc+jFR+ZmwEeCt5A59NOBMOuXVdfi5Uac9KzTpUn+HdiWW71qvrU85pXBAmHY4ebqix83pft/JR7tnIxXHJpU3tt2r251N4Z7jEdcAbYvGBMSx/6jw0a0cUpgs95a2QRRy5azEB317nR+SApAoSTmEbWH98tu8RUdQQwnJTpUh2JD6iBDK6OyG0qM5OsKnlMvgR6ntEWQC6Ymn9pJlWZB7zBd7ulFnqpzT7YBXEE1CgLcGGIeO3xrAofGntRjaJkkxUyCHANG4mMu035IG/K0lij7wv0YuKtRFbRVnnJDqmxlW67SpYNhKzGms5q5O71tTpz0mfrbHe7ag5bdGvgCzFkHfG7aBRO9ZJX/e8rJX104Al/Md9h8PzCPOYxAplKOswwfKLauuyWWt/GjMIqjDgPcP4m4aIkfKeQYGF9CJIGL23JyevFIRLqn9QEFvboIAae+0BA9iokDeBGvmDJ8Bv1IOyB2UF7gKbvQk4483IlwtxFmZa5nVRtaNtod9hVgJ+DKAP8u73kHyom+gXrP9Koq85Wit9mXObpHvoH1/6vL5o9QFLQH5B+2H1NLsbvysr8HKEwkmGteJ+bg4jYpgBm7L2S/XJbffVtofIOZX0YSlzMDtBbA0MsxKzfTX9Eqo4QmQQroEfJlD+3WlI7perVpPrbmP6Za0CM5j0Q6kZz/aNCmc2P77QtpcfmcUu6zqLhYOfB/5VZ1rKLAgE2EVkY1OQJAojUuP2UzISgK12jqa20S7SO809lQbtAbpQZ660dbGaPBL+mwMN25IivYTnjGKVxzY6GSQlSblLhm7CpSsc0Ru/Z/Ps9m3c92BiplNAWe9h1mnBooxyhIeAvEpiJuGKZ9nwoLkIwu8LrFaKdOuSR8TdA6eUqbhxLfbUATv84KGe0IzjTagdrrXNdop5ObszahpiGFhSxwzOjhmbtE/hlSFKVIJUnqenQQkvSbaOyfnylvE5lt3Od4F4I5lk/Ocf60wn7mA5eqn1V4fSnadDXLPkzHXHIKArWEql58cYVqVtssC2g1LpqFrnIylP4NMC7qPMF/rUpWdoWK30CqxPoajYfmQc/itQC/N4yVd6V6HVGUUtgpKymHLMvH4/F2OFLJlHWf5L6pDrVqoONyUjGL1JxMYEGRcGWuvpdUlZQzMsZUaMOB5djFnXKE9u5KvbCOJ1rbWl84SGXtUzd9zMztAuE9ln/Jdm0CqV+pe5EBxsrAplKAadHjO7vZPS/+vZELSk+1uvT8vYup0NVMrMqC9NvTs0F3A1iTZ+Ta93DJzatHjf16tmVsLQ/tBGlBBYNd5eNeby7ZOsPEERk/lJUCtmi4FHt+d1tcxn9nVdPgGrFvbay4V4og66tyaeY4XRGBLsoVhNLFFv6SbaE2xXIQV8cjPDaoZwTM1/FIjvUrbxACwhtkokY0dirqFO1KovS7JYEkHpFvH8k63NcHiEEjqrlWFfvxqQmABAAVVv7AOknPshYQtj0R2l3Ux5Okh7/YNEObZt5ZOzqUm4KIfsygiLlYUdXEeNOWP6fb448KTmy3OGADhO8Ta2Bmf0D2YP1pZQUIu45iCMmZu6CinJrgSEUz56dF3lum87exFpUUlqoMhyYRzUfrXpRxzqnU8JFOsI0t/hxS0uvmCIRkAfPMyJu1925stAshlM+I2QwmdGXTOPVLmB4plJscAthHv6UnVQW8mLP7s7A4w1zea6g5vFM8YxD3wlmgaG0cgClR7/tGRCTLKBdXp+D8hefOLOCaI+YjoTK913cqcFU+PgPdt49VDEMoV8u3b9eQmVp5Cg7iLO0SZHIxVd8qMYh8LqUndRSp4M/7orgE71CjvlfZt5lDkbtwNZ0/0GJQnyevMb/hY5WimnKoA3rErld6PRLOQawPeNNoYWgaeucJ/I5qqSwG06GLjnhKG0c5PbqsZT6RtEEPj4Aixryly4uAb4Hgk99YPdxDagzvxNMpI17FMKUMdlaZ7WXVhQ2UDjXbu3mpbE35JEBCZgNKtqWuU5BwcjqQOsRaskbmlnIHo53FeeUcNCJw1+XSNounP57nA+eA7YSWMfzARVwWIg6lJc4dOewau26IkYZAp0f4ebxG8Oi58cr8FHpvh6O7DXdpGfWz+Sh4e5GoNFMKQ5dB2Jgen+VgCsr6Qk7alK7h7v/MMoV1mHBoXxm/4e5yy+UOSii4QpQFZxV+h06cApFeY6QwsUZwoqZmexz9gjm2gaPVDNXqiTzYsmXQS4v+TSS4dSouujCsQ8ytCjgMh9FhGm9ZyGumZ28EvZQ7FwcqG24GsAVbk66XgeTOxSU3rxLwy+kO+9sImEjzIAditvQfY1qPqapYNQaFtRr21bI5nxX5d97rL+BKPDyM4HqPFq2+rBQI26CdL1LCQY32DwaSAYyHJmCxPeLus2tNQP6FCihWoQkndfFlwSlsSzFTqiKhAmPfBM11RT3Zhyw9AgYsaqFqXkxzWkCSxy/ej7H9sHwLkatdi5D+IPHPsSNn4UwBcfjISj/ahXrdqxVgqU0ctaqutVDZfhS9kNt7A5CjaxIyTe/IzW5NXARY7i/N0PC74xzOiffzKbfR5q0T6ougoSGR466GfRfW/xc6a+BF4kfGBsUncIvO3DEvEgF/VY0jW29c59ZGt9qZcSXkQXapExBbx9zi/TEtNnmdqRFtVxLr/516BpBv99Rwq9V1SpqBCqRbxXQ1oJGQuznilvsvc4rDRXR8XbputEblMW4Dcg9NpN6JbMLYvuOhJs9NGpuuWRyYHNxie3Q4QlYiZk0a5gyhtBQPHTBUa6wRaqm/0YH1fmBr0Y8+RORtMi1AtQHOi+I7nXunLfs3ZloatbW5dK/jbOmYHpnsS7fN+U9KMHYupOCEvPDRp8KJRaIapgo8f5DkkIGkITsc7U3sTLOQBLBP0b4ifTbPpGwSyR1EnV3XEt1baz9uDcY2n3zeEyOUMxbW08uXP0xWVwwnPW+w7EpgFvcStsQm37IDOtV+8EgbhVd2jjwzbh+GBvmmnT/IrUFO7obnxbXusZGc5ntCPhflG06OJE9wdpKNYuU95HQENZNOy9HJnSFY9oYqnM6qyd830ZrkSwHmMYuxG/BkTv6IeaicHG8IK3OZ6K6+4J4iUNE4J35WVrdKhVZKgh6JAzjeRxOAVehFS7W/E+MGRUQu3nwm/jW5pF4MdfJgnAnvXy8h8mKka9Rmn0ci2lXveQ4cTS6gKgkJSrI9T7gpNRNJSyW96nBXGlrq1Jud10Fcml//x0AWEJygq5SzwMAhdNHeSOgYmZ0qYc69JdYiXtKUedkM7IJR3GEp4oboSQguUImaVxTpXNRW/NexxTyyATm6RExCN5/gcpsDVmai4EQQSheCQYnZZCxJewnA4qvUNbhKLcotRQzBaF7kIiBrxmjAsBsq2dpGw/eSe80W5LE6RklfwTlUl9NlK0czJ+5lMvk+Ffzx6tMfYo0n5kU+7oJsLvF3OuPjgGKqYcJf3EGoRaItnUTMZV1Si1gqqShBLY8/sH7l+ygRe928yoH1uSC1vr4LWsiMrlXpQMA5W0wNj2B2ZOK//n+kIYgi314n5HBdDa9q10kYHhDyBmfiX2Dmmu966AE9uxdJUcWgWdx7h2I187jB129QqXcGDBHWHyTZTztZgw81WFtcEqcTu6QpjOmnVj7VUMAsdp/+/PzKV7Cwx+Gz91hL0w+KjL3ct5h0UjsMTZ58l6boJnKCIewu1F6YiZnqK4CxXe07ou0OUysQMEBnfMztdZqzy22F0Ug3xLIMxg0LMihhTD72qoJD06itxvJwMjfVhRLunLBDpnlJuwV8nJacXR/k2dQRsDpUeWoBFrtqWDiHLw9YbjbE1YyekM07820C9mhilSEL4YRcrLHleeTR7p3I0UtxaiVr0qOCbX/abdTXYa4bF/eIE/AaqRa//jeOyiKW/jjRpVqVPojGP19TXCWesQYEjdWdVeN2YrkzlXDds3x9+S6vRNKGTzU6pX3Bsfv8h6M1Gw7CIIZ7UZ5AdZ2euHRwHBPJY+7/XxoMS9fNEVj0FfM4n5+peeLon7/OczjvmQasF86fM0nnGaUD/ZjzaBi6OPBAW8r0oD+E0gxjxZZeDMYZbZg+8v+T/hhzv+sPo0YzXamH4EqxWQYB76iU0WFEjQ8g4lFoW/ihdjir3psUXsLlSCLEHKeHCqzwTxmyt48kv/crPW8Eet7+Qns99z7CTPkqg05f4+d1jnazhXEl+6Zs7/DPP9BH1y9BdlV9q55gTeQe5ixO2FXE0gkqYQDTB/NcKSL0wsf7jtGXuWuWo231cLNSQlgyhfDrQw/AfPE6buRr0kf7ZsRDSA8pXS4f2qROaKA6dlbHbx/Nce5NYowp2Kfkr0Vz1eCXQRaK3qnZOUDopqIR44K3WcJ/yd4SjymAt2KJw5QU4FokDBV6wasNYRpGoUuPLaJDOIoa4n+HI9lol4FjzjarYvsurAGbjpG6MdHWnDGziRfYdPvs+izNfmPa+08LcdaJzScDFrrx/gAUg6A9KiNSQ8FtPTOGMDUXl+jLJJICRKt7zO0K36OHtdSdzAAKMOfkYqpW3xZsRrHciV0mP+bqpqRFwExKSjhpLj87aVRg/xNuOkgpyipDCUMel8X2/6oiw48skxBUvzPrwbGxZa+RkymRmKgaxCZuY4booNX/rrQgWlbO7f2fFAY7euWyk7Xph9kSAOrO/07nFQSwcRpsCDgznJoJUK5mZ65bYPgu5izBV6JZFJ/8PEn9XkLtADtIzIRWPH++zRNA/jO12gg3W+Yqk//7+UyOE9w5Kk1cK2Fi8e6ZbqW85SeIZtSoNOTOX0gl97a5ROm/HykElIZYugsXXIffb4lLld7QSy/Z+VauMPM66yYVLlAVp2g8U9pIn/NdGbCle6u5G7mAIj1H35+Zlgb08HbGeDROjRJjhFIY0wrl5I7jJqi/zSUc/u1OT0dWh9TXEM/eHfQwDgvqedMq8wBwCv5BMy8PcHMxyERPF1mLw2S0bTuFZc5k20P3RLL67pR64FpEtIuV/AF4uuQw57f3jKv1cS+0mE9lIvMXa4FAfkQr4y1vvEzcqk1S+4zSpS4Zca5pFXIdpr4gtjRJwbIUvekGrX6YKTegb+x5iLnxSqqfIH9wzEbiuAWNzhKHo/bpbzvrsoE9Givdw1H+fGmJv4CIaPKuDDCJEJRt1TIdmXg8rWFFUHfGMoVB1C1RyxFHQLiYj5Evh7nRCA52xg4xQLX+UdlnmLtN/9VECxH1u1z8b56PLazxYC8zaCiBCdJ0kiePBJz+3l9s3Eiq1Tqr+FrtH8qfGn2h01ZXY7zccLIirTHb2zbXQCXiVPus9cV5XhlwzImpSg7F+/NHtbm5jk8vqBT49igVasR8Pxq1c05G6IOIqInieJ8OKgLyOXnTD4EIwbGbUy5mazPlOJin9mdSAgAHWGgm1yVFzIhCX5isBD8dpT9JOk8ERlliQ/R/EvShUtQEjSZE4NnqacuUhaLujsPlfgC+2y6lGdG0ANwoS9NkUbnZMbKFlqpKCfmjehc/pNjGtA+pMVRYDsI4Xvf5eh7ayvdClsx/DmL2jhc4PF7xicnXR5nE2wLsf94fxFby+YGPz9nLKwxawx9WJ97jEDb2RRI2+iFeoMRrlS1SphHfOcqb2XnmsPVYcgt1P3MibsDfVazqYe6eyPC5f9g9Nv2JqeAA6mkG/8QyCC08SZJJc39Q8d5twoXWP2TgCILPTLCemjyTJJshdy4HwjGqS9CxvCU3RB+byKmCHrghOzHxFRT+3Cfck5cjKf7MbUekH27WMz6KkppmYm6VjvYAQwSYGxMK2KiCMhkLAoEv108nG/dw8ewf4mON01uq9hbdbTmiFVi/bhUVtOXcB4D4F2hGzJ+hMlznggTXwXWZuoj8IY1rDe7jM/16lwIVgSrIj3iQagqG/FoJJC0F75jgHDj0XpVt6fSQo638ZyCXfZAWwTpnoPzIsK8x7BIXO/Y5AgH0BLL1C57D5tqB2fNnAxC1BTX1cBfKJNG8o+HJPN1yFfFElzu39QRIIVdxXWR3xgKY/kHerEVR3A2OyAb8baJBYVon5TKFUya1U2mT1Qes++NCx/RIn0dpelNzMFpzp1xYL45droy8pMndvlECtBUXEeX2TwpsEKdTW/lJokUrVA1zwAQaX2E8BjPHypwxJdbngf3X80l4sxNCj5MfN5CRRT+4Ht7psnIIqtBBxAHX8xwqkbm9OJpIEkPL00m8apX1Wrs0o9KTvsPZBqQaKkdD2uLlSnostueYIhenspFQtQQdUzGfkssTshdZBsk8OlnpfsEcwPNkDtWeqZLMcoG0XpuV5NqHzyHgqYp3cyQjEUSc2pIbhT2TSgmUBuScWKC93LccZQcLTITPhEVOKEOsCa/g5VTZykbH5b3P1rCa0an/n+rJQ7X5bTKd+6e5pABDgeV3+FDxJAgbZvxGYSDaqfIREsCpwZ3QZcnSSuGArb8isSaFZtjHW38PVG/WaaVuj0v6k7tXngniDOahTJByDdsWclERk8ysFC/2QayozeGKh3Jo+7T0WclK6rstJFDr0JrFXUcATw0VjzeAeMd4kYv/wY2XX24bcW7AyG7+c+rmY3jxq8byEGn7IUOnfj4VBdpIL9CYXGo+lDQVtWOg4T6QwvD4unVwx0h8yU9qcomWacOS62q16TJtZmlO1uQ5wC9Pq2S75ire4lUqLj9t79zAQ+46WjOtp9CnXpF9Vru9azu1eKEnfIYOR63z/ic97W4hQl7vCKiY8cTfblxXb812+OWY61nrrY6WHQx0nNzCLXsaZcXzVvCrrmhpwxW7/MxdHbR5Jhob0fu+j2lNIyYYLeUBlRTyRjtLhoDd6NN/YLy/EjUzafXMgpsPLwbdqlWns6nSVMyNyxiIdx7nDDrWAiz/FZ3XMSBKTCyYgpJp3E4DdOAWrNHlSY0/pntTYDmyqqdHwiaMc5g+tjKa0WMWeAnORmVgYX4ipv5lL6OsG0Zlg6x0/HnBeuy2IuuAWKtb2S7wT76ICIBVUuE0N9g/cN9SDDAOS2voJqrfcf0kxKZq3ncnw4sKbWDCKbmXYb3xu4owVHK9oAwFBX/mgSZqbTf/YGMod14scrnW9uy5dSHNo2BibJOZqF8OqJ0ql9tFrvICSpXLk4Sc2ATZavZVg51RBtu3CJtw43le9IjTdUkJb9tA832qLU51MRU68Zz5eW+dFydGU4AnPu3J9SCjavg+k9o5z8JY1u84CNEeD7C8mO0pC7QiEvMRQlnvxl4ULu9aCbZqPoKWEtW6c8dfICOmtV98ds4LBrXdDc2iZs2BvDOM/vfUuAefGz/4HcfWNCZDuWL25zgQrzQ/21gZ+fskydtkdU2kyQOHDeo5V1wedAFNT2MGGtkrvHfnDfU23z0m3UD6WJirDTPeC91zhhOJtrQ//lqJmQ5FGUrP8lf47Qxtw05nEtI24hq8o6tIH6CdRGKZBxCxU8iJ3dUnCdCT4nzD1+sR1MDTiT9YU6NiXxw+rs1kx+KSZxWWCJWvWyMGGBrV7wjKUtITfbKLs1KSZLjGQeOS0sleBGtz0iOqTmQqCo3YvS0K8aB1T/SVyNVSj7N64H/ZJqs6gekzwY6JKcZokf0JLaYDwE2TKQqXrw7eHyIm/rmalwbi9TKlgY8nj3cEAkahaBkygti9jsSewQks9OZHvDGTyu61leMNr5ItygTSLCzRETQYRuGFxNj5y5GRAcn8nPcsOmmscConJfno5pMOLzNiOneKzW4JCPJ8tDk0AU161ITygplL4eJmz/Cljba3krlctjAJL9O4jFM/xIqxXiosyO1wv8SYb1M0hC+dq3r+FRXQyp6V003BzHeE/5smIlfw/k/tbhzQ5119vLJwImoAzHsN4vCVr7hqhYyCX8gHXwB6///vTE2W7q+LwzzYU0SnnO5o7ytEs0w1CT++djT3eR32t1UiTtk9BnuhM9gvyrNGjKasGJSLLXbPFC4lIq6rOppehhOUY1B3VkqDow8cnoKJzwZ08KXLAePYFobtjfeYnx8kBM35WWkypUvTvPOH2ijNtZ137GHLYzdzQZAmkt7wfiCPzSxn21Lq6xuPDjDRcwE5AgU+Zy5MgmuNQIjQBoSv59aSUr2r2SSOWpDg0Dp9/mubQe+cBB/8h8fAuLkmuyJ3RCIvAVyobPZi7O/l4hthy8AeVuTojpZCKxkoTkK++WqeXaIEATyAH9VdPknxhSAQn+RxWHbN4uf60WepAC8qvsfS6INv7rkglMcFxREVdPw/CogdoZUspz6ZPE5m6y+g5qIxo5fpudBE89/31mVIxxA1daTj8SjGUVqReDwRtSDtUfj/XjHCd61ZPQJmy0gKlqdI+wx6psQjeXO6ceyU+86rtqiQUkrWl4jbQsQwpbb2OYahcs7EnvHjhQfv5QM04g/1duG22g//1047Gir7kOCPwJ/wS2yMWNyfAxqIns4Lfw9GDXJnzOkbq0XWpU+cu808ddzFVJAbOgji709Rai/uRqUwCAuLiStoDNSCnJ9KM/n49ag+guHAUFYBRCwBgOrg4eINb0Cc11gs8tasXaJOxv4Cj6OZk9dq3F7hvKsQE93fhkKdlc8jpr+vq6PihdXU5DCW2aZJrfjhU1oHfckDMj+YBfcrCfSaVfff7pEyHDT40NfQmGd3YBOp1QxmYT0I8OCSY2GIUnX3DepKCgADqCNS1MxGCpUXl3Mxr8hz58EiJexjkaEbiaWE95DytwAqN6scga02+Gb98PeSUWgOuovqWS4C1nW3wNf6VMzUoAbaSRxaq1ILZDT5ekGG+lEukglVEUtLW82yVsk3zZaRVPSNlP+QThV6I+PT0jQd6p9ziKWprIRG8EYbV7xyJ43deStFm8vq7y5dTqXO/3eod5dwpz4iIsqKRMGXfbOMFxWQ1qSI2pCoVCzIBiSOg60SlBTNdjI8xbeL+HGrkPD29WK+iqh0tklNHK8tr5x5xIkG3GYDgQ5A+lLnXQl0lzyYOAB0gHyS4+ZxiPP9ygqDq6i9et3/BDa1i/liMBzT1fHc1l35wpflR2O0G8m4MMVyrKrWgp8mkhmZQHL2SuM/2yHpLM9wfqChE3uKZWyiMKgMFbjoWJ8earaSfl27Zz4veWUegjgcWV0osGFufFcy0ZJBL2GptPspTb1ipdh/kp1XXt425ie4UsSsB0O1jTlBki3AxnKjKRI4j7FRGm3maUMXBpJsEH54YHb6BsGefEBcaIQvmcO3Q+3yYz0qUj7aDCpqoBT9KVYrVBlK0ajZOwIETsaS95KD/qBMOz/lOpopsghtdF8MxzPYMVsqjZSgZpCeJc8bw/6Lm62sFlgX76URaIdhEFqj9eBc/EPDJ4QwcYLvnJ3v4shgd0n/NO1f+mnG4uCr6aoD77qc0RD5EmIJJuGJTgJhBovwzvS/qnSuQ027RM9gX4ls83I3FXvhiWQp2tAqDGmI3NsZd3RpbBRKedMpVw5lHuorXjoLt7+13X5epBZASbAJNJhc0fnCfvqR6Ep2gND0iO1ANpUJEh+SyO77ganKqE+TGAf/2zZqI3Quoyleb0UI6GQz84DsDXBrzWe/DGR0KvR830h1QoWBcJCmlUDR3nACOX0jn453nXX4nARIVrQVoE9c/b76UyHsu2lMCbkOJ45n1FfkAj8kyPk3InU4fEJitaZAXUV/Ca0koR2c0S4hE2Ji6gLt39gYpYkGujVsGB6LZRr8+GzFEsJ/5CrHRISVna+qLY/57e35C6zwfuCrdwZ3GLJYa0XPAMc0UXLuRzWFkXLPHhCikdKNRRlfZCpNMK1cm4r0aVX9zxFL3xiOUxrbxElKwIVvLmb21Ufwv4Rej+6wkzyZ+YQoDV8cg1+G9uj6ipWSaWl9nWCmA4GvTmu6IyYC7uVf+vIFiOeyZ4tlRmzd24z4hgH8XQe2GPQCY7b0N43ROnspca/s9LoUvMWV6W3lwAXm14ZaykOD/9FVvk9jmjc71drzdRINglBYwSlRv8ftlSdfjh42S+fAEH6WoFdOzWQVnMOFT+OofkyBTGmNqHmBmHng2pLBLATW0ipyKjvqt2GIQ5U/NnGM2JxMinjDeHvrWJb+VOHT3KfOu/FU8WZCn8O60AkN9DIEUtYVV9NURNxRj0+i5MJ8jzbnBPxJDuWnD+9beqqKkT7GAB0Fm43RVtq9twW491TDy7PR1OXFC/tW9xUztoK1nxRbxe17DOxrFMbNdEfhQ/8dcYRXKCwN/Carti8HiIVzGzmKembpgf4InO78/0bxxKRApZlR0CgM1v64uuTmXTgU7UKkFazal7hGMCRliWX341CDzGZ/sCztge8bYZKobWzeQV8aRBRxJN6W9dRIeLe98m6F18U46ERCew5G9pq53+DoeccmElQmgPloAj7X+cPmz0o3lCUvgfBWW1kMV9RwHzzK3m9Kc7jYzn6wwoudQWolp/YGiaSldkB323BfifnaUbD4VoQ7Qdsq1+nssDZPwaf8+Qm6R7grR4o8XwdlHhnrECgHLZYoq9swWVaid2iThTY1V1mQ0CacFBH1Fmmy++b9xbMjYWdswQKuGvZkk9dVLRDynuSzswnnWJWHzvceh6rJgtpAJsXyCIF4/iiCzHs3HQrhEvYE7W7YOnzSM8POk1zCjfZCLUEOBzJBGroiF9ym/7XLi0eVDfyFPLPZTmcWE/6qdK+jg4Rd72Y7+dm6GKC1ihlemA7BNMn7toTcr6YsfIffhJrHBRFZ7Vi/6eUdnP8BZuU9iNSF+U1dcR2oj0lWdTqRk1krzhmEWt/QxO72oZcSTxD01Uee6TP8wzorQKDUcXal03ahyf59CqGyJNebhK49DuSJ83yLVzXidIkn9CCpxkLXxmFFO4ET2Cz1gIcTEHfnkB+SN25stuT4xdDNzt/Oar78vll2idwr9NZarE3HsWFsrxbsD/XUghYYiwyTQx4mITPM0D5e3fIAgS6aVo+hVamlT/0Ksh2j1d3ezjfUuwP1Y+VBrcNuhVwn+612zOOixcUZPN6EyZSV+Jtud3+7KLCGKCnbLQcpcwDUvpcKhIADJk50JQMNz1dzexxEtl/p+E6jfzEQPrtnDbOcB01f5tPEJLa2Lqho/WZrL/Ah97M0BXLtAq2HPhmPIzbW8u3CxG9wU7q34YntaNTAWMQ0euNgACOrji+61XNqWT6fAocDGhJ+kO8YylFY8NRyOJrxmUV7UWN5cLrzMIw3a8/J3eTv11tsoa3S+H0BHWBYD6OeTGxNjNPO4PmzZJ9Y8KIarFCgeKWu1bV0gcuT4aVGIAhxU2BQHlS40g2GYocmgtAGk80DIsi+7h0DrmIsPUr/K4QYDhU1KjBSXzPO/6ywXPfsxABGDydKw+8mjmINYahBxT5kebOFFO6D8mjQ1D94/wp2Ho2YoQdCzfqpWvm5TEiyzds/SNwA6RfePuHpZWTiDqr4iheqCmXCus5wQH3AXpFEuUrRknbSxQ0iUL2RnJ4TyPlgEMfUUzkM9yjaGzCbxfux8TdJ1I0hf50db3CTy6uQy8+IirnjUNodiDMLl35CNMfpOhPmrdZZY35KSY4of1EoxQtMtX83FLKyoZSoz6Qq6tByeH+pCgCvVX1mtD/hcurFGhqaBP6qA1hJrUCJiHxSVHzHw4KMhYJXR2P1JZmmiSOjDaRv7Uy8mH+/s+OjTyCA8XCX1jWHJtFHNGVvIaj9HoW8G1UG9a2xuX00MRPGBSclOpOZJOSfMDoA0rxHfm5T0xHnNOz2dm/rcgtLgqWWaScD3sjGLIQbef0NgpyTOxpDl+oXdUMbY8Rit6zYRYvEttPWxDCS5qWZT+JbyVHBgk6bxlJPMvNExbAzRGQ7RU6WGjHWIcnDJeykrQkLSpZg0dccm81Npc9pWN0Qw7F+HoP2PDSKd4HoO0/Y2T3aHUrMkmcUlTw1ydyELgr7mA5VydN0HZFKoMftN1Go1iO+dv7E5mUxUafMVMj+NJPtsAZub6c7BRkY2p8MVUcU8utoyMzjqzyPWIbWZUqBY1LcwHX823UN+2185XPSuEnDvbRL7kpJivkLphm/xrMF+cTpaJ0u3iBvreNi5dOHMGklUdZth+NkWmQ0rsLNKFUBAwG4qVopQ6mzWJHnlfy1LQSUJ/WfjR6hqXqZFJ8+lvuEUXUoXv50xcQjqvCStBys3daLydrCOWmTzopJX92SwwlEussiT+cQR4MHnIPKYr9rnU7BM7x1Zf4HJLFQoCOYHO+rIatW/ilelYfY/MW6upPKC4Y0r1gcKfILtCrIt+Ox3KC/BR7we/XYEtd/J2boW6mDXUJVaKzAyKG/4OKrNzzZqzYcCqfJTktfwd8A3Ny329SwDXUj+mbjUBBSKgF9b2PminIwi42s3UNWpoUNZQRFDiaaUaRaZpiBFvzZqZvUMuY1oK9tzRxYOm3TBFKlrVtSgUKK1gXylqNuH1uWmTt2oYvc8JnjtA3csyujmTHJcUmhVZ7OodC38hxzQkvSC2c6zQtD3PNCQicPbWwZGiiTYoIFPMXygx/CQSwPEcL/kE3Zlvu25Ng8wWoqQws5Ix5l8is8YwyiKxFsBF0YHZJb1o/ZVTY1aj/30Jsw6tUBhW5BgSTPnWyPJLfyxPlDk7iVnP7rXM274lhTE9/zhBErkKrdoXUE7vhilr6b42R9f7KrlklGWE9T1gZ7RfViXYRNLA2LD3T48gNh5K6zYEKbyL8x6yXRNWl1Bpiuk2bv4heYOQXtDqwIWaFiMzVwMt4Dw7/Un+YeCgF7ATE1C/0/v3FQilIG3jnwRZlKqDwvuuVWE7Cg7jqq17vkULlMSspUkjyTF1uIfVKsOQWuBNoKeyCbzNwuwzUpLoBsJFBdSM8y0MA+oW1xB3P0ip4OLpBu0OE5vPYpYxXy+FtrPLd6AC1H699ppkC8GiF7SxxSup0lOIX0Z4Kth3STRMmDw7zND3dhrD1JETD3Dc1PisrG6S5Vcc7pKpD7hZm8mRBo3VmtDoX5yyTaczDq2DeCaq8vIhOmyqJVGu6x18l0MvNvUcOBmoFIUQS4xmbI8RSUFOu5T156Hjyz7SVaoOkZvKoSCdedd2+mlcdFTBzm28Cq/d+X/9h34zRuD1AyVqj50M7PrFVEzF+TNeXI9s9pByIYOw/X4QebPaS6oyiFvTjNZt31govN3XdUEZy8zM7AKIV7+9EyPuT/zyYukjY1mUHIbpy3cwPmOjTprqHbqhVolj6P5r/eSDvHwdYluiF5UMCLXZut2s6MZkwQoYNCwggQcJQ6iRG85RNjIj6i68WTZKvC33J1DvxRf/9Mb8l78MpsM0UwnMQ5BSkE9cbi28HZJFTeYCSPu2+Sc=" />
</div>


<script src="/js/dnncore.js" type="text/javascript"></script>
<script src="/js/dnn.modalpopup.js" type="text/javascript"></script><script type='text/javascript' src='/DesktopModules/Exionyte/Menu/1/Xeon/Scripts/menu.js'></script><script type='text/javascript' src='/DesktopModules/Exionyte/Slideshow/2/Xeon/Scripts/jquery.cycle.all.2.74.js'></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { $('.slideshow').cycle({cleartype: true, cleartypeNoBg: true, fx: 'scrollHorz',timeout: 6000,prev:   '#Prev', next:   '#Next'}); var a1 = document.getElementById('arrows'); a1.style.display = ''; var b1 = document.getElementById('bannerPane'); if (b1) b1.style.display = 'none'; });//]]>
</script>

<script src="/js/dnn.jquery.js" type="text/javascript"></script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3a4d4da635-3815-4b70-912b-f389ade5df37%3aea597d4b" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="IpmJIJeJKWv8JjoeyjARrWMY5daizulvtcREGwUA1vm4dU/fmQgLnCtnk9qxSUOHzdLkc9lZKgC4DkluRpw/IKREA/2h7i7HAyjsH0EvDvRuSoU6gq2Xfq3FbMdqRls8uW+LPZRz2WTYwNdPv+WqzNCG+FAM7INdncgICXQjvMNA2j0KVM0cX8K+rPNwyPtNvA0I0BP2eaCQ3g+VVl6qslcdxHvXRhFBVveC+Q==" />
</div>
        
        
<script src="http://cufon.shoqolate.com/js/cufon-yui.js?v=1.09i" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Futura.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Font.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/jquery.pngFix.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/general.js" type="text/javascript"></script>
<div style="display: none;" id="message">
    <div>
        <h4 style="text-align: center;">
            Sorry, This page dosen't support Internet Explorer 6, If You'd like to view all
            the contents properly please upgrade your browser to IE7, 8 or 9. <a href="http://www.microsoft.com/download/en/details.aspx?id=2">
                Click here to get the new version of Internet Explorer</a></h4>
    </div>
</div>
<!--flash section-->
<div id="flashPane" class="flashSection" style="display: none;">
    <div class="flashPadding">
    </div>
    <div id="website" class="flashContent">
        <p>
            In order to view this page you need Flash Player 10+ support!</p>
        <p>
           
        </p>
    </div>
</div>
<!--/flash section-->
<div class="skinLayout">
    <div class="skinWidth">
        <!--header-->
        <div class="skinHeader">
            <div class="skinMinWidth">
                <div class="logoLayout">
                    <div class="logoPosition">
                        
<div style="float: left; display: inline;">
    <a id="dnn_dnnLOGO_hypLogo" title="North Maharashtra University, Jalgaon" href="http://nmu.ac.in/en-us/home.aspx"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/0/logo.png" alt="North Maharashtra University, Jalgaon" style="border-width:0px;" /></a>
</div>


                         <div id="dnn_logoPane" class="DNNEmptyPane">
                        </div>
                    </div>
                </div>
                <div class="menuLayout">
                    <div id='menu'><ul class='menu'>
<li class='current'><a class='parent' href='http://nmu.ac.in/en-us/home.aspx'><span>Home</span></a></li>
<li><a class='parent' href='http://nmu.ac.in#'><span>About Us</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity.aspx'><span>About University</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/misson.aspx'><span>Misson</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/universitysong.aspx'><span>University Song</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/informationatglance.aspx'><span>Information at Glance</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/achievements.aspx'><span>Achievements</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/vicechancellorslist.aspx'><span>Vice Chancellors List</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/annualreport.aspx'><span>Annual Report</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies.aspx'><span>Governing Bodies</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/managementcouncil.aspx'><span>Management Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/academiccouncil.aspx'><span>Academic Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/senate.aspx'><span>Senate</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/deansoffaculties.aspx'><span>Deans of Faculties</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>University Campus</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/aboutcampus.aspx'><span>About Campus</span></a></li>
<li class='child'><a href='http://nmu.ac.in/kamc'><span>Khandesh Archives and Museum Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/centralschool.aspx'><span>Central School</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/campusmap.aspx'><span>Campus Map</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/howtoreach.aspx'><span>How to Reach?</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/extensionactivities.aspx'><span>Extension Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/researchfacilities.aspx'><span>Research Facilities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/mous.aspx'><span>MoUs</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/home.aspx'><span>Satellite Centers</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/eklavyatrainingcentrenandurbar.aspx'><span>Eklavya Training Centre, Nandurbar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/rcp'><span>Pratap P.G. Research Centre of Philosophy</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols/en-us/research/labtoland.aspx'><span>Pratap Shashvat Adhunik Sheti Tatvdyan Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/mahatmagandhiphilosophycentredhule.aspx'><span>Mahatma Gandhi Philosophy Centre,Dhule</span></a></li>
</ul></div>
</li>
<li class='child'><a href='/LinkClick.aspx?fileticket=oqDHHmONXsE%3d&tabid=3881&language=en-US'><span>RGS and TC</span></a>
<div><ul>
<li class='child'><a href='http://apps.nmu.ac.in/circulars/Bcud/29-09-2015%20Submission%20of%20Pre-proposals%20under%20the%20scheme%20assistance%20for%20S%20and%20T%20applications%20through%20university%20system%20of%20RGS%20and%20TC,%20Govt%20of%20Maharashtra.pdf'><span>RGS and TC Schemes</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Information</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Academics</span></a>
<div><ul>
<li class='child'><a><span>Schools</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/soes'><span>School of Environmental & Earth Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols'><span>School of Life Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/socs'><span>School of Chemical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sops'><span>School of Physical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docs'><span>School of Computer Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/domaths/'><span>School of Mathematical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/udct'><span>University Institute of Chemical Technology</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docll'><span>School of Languages Studies &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doe'><span>School of Education</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doms'><span>School of Management Studies</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soss'><span>School of Social Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soah'><span>School of Arts and Humanities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/schools/schoolofthoughts.aspx'><span>School of Thoughts</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/dat'><span>Buddhist Study and Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/smcrc'><span>Chhatrapa Shivaji Maharaj Chair & Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/mpsrc/'><span>Mahatama Phule Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/svsrc'><span>Swami Vivekananda Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/grc'><span>Mahatma Gandhi Study &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sgsk'><span>Sane Guruji Sanskar Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/bcsrc/en-us/home.aspx'><span> Kavayatri Bahinabai Chaudhari Study and Research</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/nmuccc.aspx'><span>NMU-CCC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/phd.aspx'><span>Ph.D.</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation.aspx'><span>Adult Education</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/eventsandactivities.aspx'><span>Events and Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/coursesoffered.aspx'><span>Courses Offered</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/tbc'><span>BACECC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/committees.aspx'><span>Committees</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Administration</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/registraroffice.aspx'><span>Registrar Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/administration.aspx'><span>Administration</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/lawrtisection.aspx'><span>Law/RTI Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/faooffice.aspx'><span>FAO Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/examinationcenter.aspx'><span>Examination Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/eligibilitysection.aspx'><span>Eligibility Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/sportssection.aspx'><span>Sports Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/scstcell.aspx'><span>SC|ST Cell</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/constructionsection.aspx'><span>Construction Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/prosection.aspx'><span>PRO Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/approvalsection.aspx'><span>Approval Section</span></a>
<div><ul>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Online Approval</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/computercenterexam.aspx'><span>Computer Center (Exam)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/universityofficers.aspx'><span>University Officers</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=ZprmAC3RMhY%3d&tabid=1350&language=en-US'><span>Phone Directory</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/rd/functioningofrdsection.aspx'><span>R & D</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/developmentsection.aspx'><span>Development Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/affiliationsection.aspx'><span>Affiliation Section</span></a>
<div><ul>
<li class='child'><a href='http://affiliation.oaasisnmu.org/'><span>Online Affiliation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/affiliationsection/licreports.aspx'><span>L.I.C. Reports</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/research.aspx'><span>Research</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/iqac.aspx'><span>IQAC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/avishkar.aspx'><span>Avishkar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/rd/bsrschemeprogrammee.aspx'><span>BSR Scheme/Programmee</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Student Corner</span></a>
<div><ul>
<li class='child'><a><span>Academics</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/admission.aspx'><span>Admission</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/syllabi.aspx'><span>Syllabi</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/questionbank.aspx'><span>Question Bank</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/academiccalendar.aspx'><span>Academic Calendar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/papercodelist.aspx'><span>Paper Code List</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac'><span>e-Suvidha</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://exam.nmu.ac.in/'><span>Examination</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examtimetable.aspx'><span>Exam Time Table</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlineresultscollege.aspx'><span>Online Results (College)</span></a></li>
<li class='child'><a href='http://182.56.2.247:81/nmu'><span>Online Result (Student)</span></a></li>
<li class='child'><a href='http://www.nmuexams.in/NMUEXAMS/LoginScreens/frmStudentLoginPage.aspx'><span>Application for Photocopy/ Verification/ Challange</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/resultsofverificationredressal.aspx'><span>Results of Verification|Redressal</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/convocation.aspx'><span>Convocation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examreports.aspx'><span>Exam Reports</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlinedegreeverification.aspx'><span>Online Degree Verification</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac/QPCourseSelection.aspx?ID=660'><span>Previous Exam Question Paper</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examination.aspx'><span>Examination</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Facilities</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/financialassistanceschemes.aspx'><span>Financial Assistance Schemes</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails.aspx'><span>Hostel Details</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/hostelday.aspx'><span>Hostel Day</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/boyshostel.aspx'><span>Boy's Hostel</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/girlshostel.aspx'><span>Girl's Hostel</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Services</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Download  Formats</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/examinationsforms.aspx'><span>Examinations Forms</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Formats For Colleges</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforemployee.aspx'><span>Formats for Employee</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formsforstudents.aspx'><span>Forms For Students</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Statistical Format</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline.aspx'><span>Student Helpline</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline/studentfacilitationcentre.aspx'><span>Student Facilitation Centre</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studentwelfare.aspx'><span>Student Welfare</span></a></li>
<li class='child'><a><span>E-Resources</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/eresources/ejournals.aspx'><span>E-Journals</span></a></li>
<li class='child'><a href='http://nptel.iitm.ac.in/'><span>NPTEL IIT Lectures</span></a></li>
<li class='child'><a href='http://apps.webofknowledge.com/'><span>Web of Science</span></a></li>
<li class='child'><a href='http://www.sakshat.ac.in/'><span>NME-ICT</span></a></li>
<li class='child'><a href='http://jgateplus.com/search/'><span>JGate</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Useful Links</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/events.aspx'><span>Events</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/tenders.aspx'><span>Tenders</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/rti.aspx'><span>RTI</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/circulars.aspx'><span>Circulars</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pressroom.aspx'><span>Press Room</span></a></li>
<li class='child'><a><span>Job Openings</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/university.aspx'><span>University</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/collegesinstitues.aspx'><span>Colleges/Institues</span></a></li>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Apply Online</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/sciencepark.aspx'><span>Science Park</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=2wk8wKpV9MI%3d&tabid=1088&language=en-US'><span>Citizen Charter</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/publications.aspx'><span>Publications</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pbasproformaandapidetails.aspx'><span>PBAS Proforma and API Details</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/otherlinks.aspx'><span>Other Links</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Us</span></a></li>
</ul></div>
                </div>
            </div>
        </div>
        <!--/header-->
        <!--banner-->
        <div class="lightBlueCenterRepeat">
            <div class="lightBlueLine topLine" style="z-index: 101; position: absolute; top: 0px;">
            </div>
            <div class="lightBlueLine bottomLine" style="z-index: 101; position: absolute; bottom: 0px;">
            </div>
            <div id="bannerLeft" class="lightBlueBannerLeft">
            </div>
            <div id="bannerRight" class="lightBlueBannerRight">
            </div>
            <div id="bannerCenter" class="lightBlueBannerCenter">
                <div class='slideshow'><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage5.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage6.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage7.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage8.jpg' alt=''/></a></div></div>

            </div>
            <div id="bannerPane" class="banner" style="display: none;">
                <div class="skinMinWidth">
                    <div class="contentPadding bannerPadding">
                        <div class="top-cols">
                            <div id="dnn_Banner_Pane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Banner_PaneLeft" class="colHalf left DNNEmptyPane">
                            </div>
                            <div id="dnn_Banner_PaneRight" class="colHalf right DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/banner-->
        <!--banner arrows-->
        <div class="skinMinWidth">
            <div id="arrows" class="arrowPositioning" style="display: none;">
                <div align="center" class="arrowLayout">
                    <div class="lightBlueArrowBGLeft">
                    </div>
                    <div class="lightBlueArrowBGRight">
                    </div>
                    <div id="Prev">
                    </div>
                    <div id="Next">
                    </div>
                </div>
            </div>
        </div>
        <!--/banner arrows-->
        <!--slider panel-->
        <div id="toppanel" class="topPanelLayout" style="display: none;">
            <div class="topContent">
                <div id="panel">
                    <div class="skinMinWidth">
                        <div class="top-cols">
                            <div id="dnn_Content_TopLeftPane" class="col1 left DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopRightPane" class="col3 right DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopCenterPane" class="col2 DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Content_TopFullPane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottomPanelSpacer">
                </div>
            </div>
        </div>
        <!--/slider panel -->
        <!--slider nav-->
        <div class="fullWidth">
            <div class="centerSliderPanel">
                <div id="toggle" class="topSliderLayout" style="display: none;">
                    <div id="close" class="open sliderOpen">
                    </div>
                    <div id="open" class="sliderClose close" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <!--/slider nav -->
        <!--content header-->
        <div class="skinContent contentTop">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="contentPosition">
                        <span style="color: #fff;">n</span>
                        <div class="contentPositionLeft">
                            <div class="loginImage">
                            </div>
                            <div class="displayText paddingText">
                                
                                <a href="http://apps.nmu.ac.in/Login.aspx" target="_blank" class="lightBlueContentLinks">
                                    Portal Login</a>
                            </div>
                            <div class="loginImage">
                            </div>
                            <div class="displayText">
                                <a href="https://login.microsoftonline.com/" class="lightBlueContentLinks">Mail Login</a>
                                </div>
                        </div>
                        <div class="contentPositionCenter">
                            <div class="paddingText">
                                <strong>अंतरी पेटवू ज्ञानज्योत</strong></div>
                        </div>
                        <div class="contentPositionRight">
                            <div class="displayText paddingText">
                               Select Language :
                                    <div class="language-object" >
<select name="dnn$dnnLANGUAGE$selectCulture" id="dnn_dnnLANGUAGE_selectCulture" class="NormalTextBox">
	<option selected="selected" value="en-US">English (United States)</option>
	<option value="mr-IN">मराठी (भारत)</option>

</select>

</div>

                            </div>
                            <div class="lightBlueContentLinks searchText">
                                <div class="searchTextLayout">
                                    Search&nbsp;&nbsp;
                                </div>
                                <div class="searchImage">
                                    <div class="search_bg">
                                        <span id="dnn_dnnSEARCH_ClassicSearch">
  
  
  <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" onkeydown="return __dnn_KeyDown(&#39;13&#39;, &#39;javascript:__doPostBack(%27dnn$dnnSEARCH$cmdSearch%27,%27%27)&#39;, event);" />&nbsp;
  <a id="dnn_dnnSEARCH_cmdSearch" class="dnnSearchCss" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/_default/Skins/Xeon/images/sright.jpg" align="top" border="0" alt=""></img></a>
</span>


</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content header-->
        <!--content containers-->
        <div class="contentContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane1" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane1" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1" class="col1Home left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane1" class="col3Home right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane" class="col2Home">
                        <div class="DnnModule DnnModule- DnnModule--1">

<div class="containerPadding"></div>

<div class="overflowHidden">
    <div class="lightBlueBG">
        <div class="style1TitlePosition">
            <span id="dnn_ctr_dnnTitle_titleLabel" class="style1Title">Privacy Statement</span>



        </div>
        <div class="style1Arrow"> 
            
        </div>
         <div class="clear"></div>
    </div>
    <div class="spacer"></div>
    <div id="dnn_ctr_ContentPane" class="style1Content style1Border lightBlueContent"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>North Maharashtra University, Jalgaon is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the North Maharashtra University, Jalgaon Web site and governs data collection and usage.
        By using the North Maharashtra University, Jalgaon website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. North Maharashtra University, Jalgaon also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by North Maharashtra University, Jalgaon. This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by North Maharashtra University, Jalgaon for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the North Maharashtra University, Jalgaon Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through North Maharashtra University, Jalgaon public message boards,
        this information may be collected and used by others. Note: North Maharashtra University, Jalgaon
        does not read any of your private online communications.</p>
        <p>North Maharashtra University, Jalgaon encourages you to review the privacy statements of Web sites
        you choose to link to from North Maharashtra University, Jalgaon so that you can understand how those
        Web sites collect, use and share your information. North Maharashtra University, Jalgaon is not responsible
        for the privacy statements or other content on Web sites outside of the North Maharashtra University, Jalgaon
        and North Maharashtra University, Jalgaon family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects and uses your personal information to operate the North Maharashtra University, Jalgaon
        Web site and deliver the services you have requested. North Maharashtra University, Jalgaon also uses
        your personally identifiable information to inform you of other products or services
        available from North Maharashtra University, Jalgaon and its affiliates. North Maharashtra University, Jalgaon may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>North Maharashtra University, Jalgaon does not sell, rent or lease its customer lists to third parties.
        North Maharashtra University, Jalgaon may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, North Maharashtra University, Jalgaon
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to North Maharashtra University, Jalgaon, and they are required to maintain
        the confidentiality of your information.</p>
        <p>North Maharashtra University, Jalgaon does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>North Maharashtra University, Jalgaon keeps track of the Web sites and pages our customers visit within
        North Maharashtra University, Jalgaon, in order to determine what North Maharashtra University, Jalgaon services are
        the most popular. This data is used to deliver customized content and advertising
        within North Maharashtra University, Jalgaon to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>North Maharashtra University, Jalgaon Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on North Maharashtra University, Jalgaon or the site; (b) protect and defend the rights or
        property of North Maharashtra University, Jalgaon; and, (c) act under exigent circumstances to protect
        the personal safety of users of North Maharashtra University, Jalgaon, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The North Maharashtra University, Jalgaon Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize North Maharashtra University, Jalgaon pages, or
        register with North Maharashtra University, Jalgaon site or services, a cookie helps North Maharashtra University, Jalgaon
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same North Maharashtra University, Jalgaon Web site, the information
        you previously provided can be retrieved, so you can easily use the North Maharashtra University, Jalgaon
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the North Maharashtra University, Jalgaon services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon secures your personal information from unauthorized access,
        use or disclosure. North Maharashtra University, Jalgaon secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>North Maharashtra University, Jalgaon will occasionally update this Statement of Privacy to reflect
company and customer feedback. North Maharashtra University, Jalgaon encourages you to periodically 
        review this Statement to be informed of how North Maharashtra University, Jalgaon is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>North Maharashtra University, Jalgaon welcomes your comments regarding this Statement of Privacy.
        If you believe that North Maharashtra University, Jalgaon has not adhered to this Statement, please
        contact North Maharashtra University, Jalgaon at <a href="mailto:support@nmu.ac.in">support@nmu.ac.in</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
</div>

<div class="endContentPadding"></div>

<div>
    
    
    
    
    
</div>

<div class="containerPadding"></div>

</div></div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1Inner" class="col1Inner left DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPaneInner" class="col2Inner DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane2" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane2" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane2" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane2" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane2" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane2" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane3" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane3" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane3" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane3" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane3" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_CenterPane3" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content containers-->
        <!--spacer-->
        <div class="spacer">
        </div>
        <!--/spacer-->
        <!--footer line-->
        <div class="footerLineTop">
        </div>
        <!--/footer line-->
        <!--footer containers-->
        <div class="lightBlueFooterContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Footer_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_HalfLeftPane" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_HalfRightPane" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_LeftPane" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_RightPane" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_CenterPane" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
            <div class="lightBluefooterBottom">
                <div class="skinMinWidth">
                    <div class="contentPadding">
                        <div class="footerPosition">
                            <div class="BottomLinks footerPositionLeft contentWidth">
                                <div class="displayText">
                                    <a id="dnn_dnnPRIVACY_hypPrivacy" class="BottomLinks" rel="nofollow" href="http://nmu.ac.in/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx">Privacy Statement</a></div>
                                <div class="displayText">
                                    &nbsp;&nbsp;|&nbsp;&nbsp;</div>
                                <div class="displayText">
                                    <a href="http://nmu.ac.in/disclaimer.aspx"
                                        class="BottomLinks">Disclaimer</a></div>
                            </div>
                            <div class="footerPositionLeft bottomContainerWidth">
                                <div id="dnn_BottomPane" class="DNNEmptyPane">
                                </div>
                            </div>
                            <div class="footerPositionRight contentWidth">
                                <div class="displayText">
                                    <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="BottomLinks"><a href="copyright.aspx" style="color:#fff;">Copyright</a> 2012 by North Maharashtra University</span>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/footer containers-->
    </div>
</div>
<script language="javascript" type="text/javascript">
    function Showie6Message() {
        var IE6 = (navigator.userAgent.indexOf("MSIE 6") >= 0) ? true : false;
        if (IE6) {
            document.getElementById("message").style.display = "inline";
        }
    }
    Showie6Message();
</script>

        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" />
    </form>
    <script type="text/javascript">
        
        //This code is to force a refresh of browser cache
        //in case an old version of dnn.js is loaded
        //It should be removed as soon as .js versioning is added
        jQuery(document).ready(function () {
            if (navigator.userAgent.indexOf(" Chrome/") == -1) {
                if ((typeof dnnJscriptVersion === 'undefined' || dnnJscriptVersion !== "6.0.0") && typeof dnn !== 'undefined') {
                    window.location.reload(true);
                }
            }
        });
    </script>
</body>
</html>
