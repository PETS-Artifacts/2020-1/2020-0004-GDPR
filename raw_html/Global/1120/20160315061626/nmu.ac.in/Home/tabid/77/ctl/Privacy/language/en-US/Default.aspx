<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="en-US" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><meta content="text/javascript" http-equiv="Content-Script-Type" /><meta content="text/css" http-equiv="Content-Style-Type" /><meta id="MetaDescription" name="DESCRIPTION" content="Official Website of North Maharashtra University, Jalgaon" /><meta id="MetaKeywords" name="KEYWORDS" content="North Maharashtra University, Jalgaon, Maharashtra, Higher Education, Academic Institute, Research Organization, University, NMU, India, Maharashtra, Research, Under Graduate, Post Graduate, Certificate,Courses, " /><meta id="MetaCopyright" name="COPYRIGHT" content="&lt;a href=&quot;copyright.aspx&quot; style=&quot;color:#fff;&quot;>Copyright&lt;/a> 2012 by North Maharashtra University" /><meta id="MetaAuthor" name="AUTHOR" content="North Maharashtra University, Jalgaon" /><meta name="RESOURCE-TYPE" content="DOCUMENT" /><meta name="DISTRIBUTION" content="GLOBAL" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><meta name="REVISIT-AFTER" content="1 DAYS" /><meta name="RATING" content="GENERAL" /><meta http-equiv="PAGE-ENTER" content="RevealTrans(Duration=0,Transition=1)" /><style id="StylePlaceholder" type="text/css"></style><link id="APortals__default_default_css" rel="stylesheet" type="text/css" href="/Portals/_default/default.css?5568161" /><link id="APortals__default_Skins_Xeon_skin_css" rel="stylesheet" type="text/css" href="/Portals/_default/Skins/Xeon/skin.css?5568163" /><link id="APortals__default_Containers_Xeon_container_css" rel="stylesheet" type="text/css" href="/Portals/_default/Containers/Xeon/container.css?5770949" /><script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery.min.js?1.6.1" ></script><link href="/Portals/_default/Skins/_default/WebControlSkin/Default/ComboBox.Default.css" rel="stylesheet" type="text/css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico' type='image/x-icon' /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/CSS/menu.css" rel="stylesheet" type="text/css" /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/Colors/lightBlue.css" rel="stylesheet" type="text/css" /><title>
	North Maharashtra University, Jalgaon > Home
</title></head>
<body id="Body">
    <script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery-ui.min.js?1.8.13" ></script>
    <form name="Form" method="post" action="/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx" id="Form" enctype="multipart/form-data">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yIJTDf/GD9Q25iVebUi5+Jy0CkF8T+QLM2vs9FnJWj4XKdW6pxM8hpqCslu19/+oSYIKmJgHJkxArgW4qcto6n+zPHjd+12nBFAlPO0CZW4XfwzH3kFpZsbQC+EdOwSjWKZdCVrkfrpzR53Gy3rf/E1D1+/KggsXRgKO2qfqpD/CD6xBPo4BW+t4WtrLOUngqp/O1sUvtBpoFPRtH38qgK6W/+duPIw05DAcKKbU8Ab2BtGom3T492ASCqJe/4bUtdmCBCsgFftKoJzAb8ByvQqMY3iIcqSVGtxv9gsA6WLpvpck3DCF/MIg7HHAERNhmoJR37Ce0GcRPYdOVi4T45O3vqXD4CILIHCz/+Yjjw/30/DpOfLXKqyO1ejiJ/eHE85uMvSk3IfXBppAP3rfk5QmmBdAcf66cpvn1Y/Q2/zy3URLXQuxvfRqFIfoJKcvl11GtX+C3yP/ojNX6t15QPF8XpCRs406vcX4DrRyMemf1ZJVmsJj4pYYAee7pCWyTTgIN5+uSPAYFJdDviPqD29ez//waQdNVizeZhrbteaDQLgN6Upf5fTgS46o2KojsHP8SkjUzmaAd5GVUCvqywDvGL6RSQoOqvqQnmQmhsjl6vbsB6SxYe2K5i3d6OlF/Sef25MQtKQg2b8Frh8wejZR9O2GEqptybcCRtK3UyYyVPKWaOQV8nq6IWxyzz2vMJJ1O6k+Q+OnquWVTreVXk8CPEr2ZAViqEVhyTE1cEcEW9PLbfESoUgW0EZPj6fCwvLUOuVz1t8FPA8t0TIHW1GEcOUf9c34ljGUASGeVih6OfdfaV4Tx0yT96bBMzd01g3Ey1+ecdMghALMULuxEGCBzXr56quuBWkZQE6wR83qNj9j70ItFfLCCEj5XnOj6Z7EBlZ3LU8vRXoVJQ9zr85880pE7EdMtBJk5JOYdWEbXhV57BhxWAo1wRt5I3hHVm/SjHTSOk7cN0K5D29WVGwytdBW51kqd+8qwcortZhfP9QzUhVAuZJA0psHo997AWmnYWZnZYdUsY0agXcPHH5plqcfY77P9zNZbR6sZmAeQxyt6vjAp8pOKYpD1ZoQP68tg8yp5G2pOOKMhfw1sa03Cy4XTvwW/De5icmZfXeFv/s57o4yPBi7lXIgPqtHzZtL/9wT2dFkSxv7Cuf9vN9Z45sSKbBeOozttnZAeYwbKqdKZ9qi98ZX3nJ5WQlMsYv5VGGG0Tvn4Cszra8uj6PV8I3I4VNzzvM7hfUQL6EN/5y/Bcn0ccLdgfPfDUO06KhlUzn6BbbA37AbpXEeHH8+jchj9dPW5tJIQJylKKvoFIMPm8mvavt2RNTjKMHPbt4WjfvHWirNa3DDgDLZefVmSCnZElDKCpxhTc05b0GLzkYvBP/ApmKBIbnuHhBzgRCOGDvq7Xs+Z8VvcN6cl5UUJA63n8QTEH3yNCJK1X/GihZdmWsCcBZRDcaJOCr9dCIPC4VzdndHHVl03XFAidg/sRSprhBgdte5iTQ5hv1lCpGsZyu9bjZOkOzCct62MLQsMzlBs97LrnQ5JpGlbsvI6ecXLT7mV3pYtrwJpK3yAUAebcISrrJZZ/jx6TLDE2/fmuH4G5i49NsSwq81nV/h8fWJbbWcDy/y3kgeG993LkhaMiN24cArr7d8BLibAIn+WPcVjAWn3+PF/a7jBgitvd0r0WyOuSSGTFBY2KBNNoJ6vf1oyBrSP6OFWiPO5EGlZgCiY2khYGpQksomUIKfM8zNclRrNPzqSaOY03ySRNj5I3gYq/OQV7yv4twP68TQQKE8wP6jMhuyj/SBKey0UB2+dVaZVmt1b5yVSsQXiImEIqpLH1w1d19XOMUDDQcJsThAwi2Yt7JCMg/xxgqnZZQ88rbhDbMAq+XbB2tcM0Pynk35OVi9nOoc6x9fnKWSDNYUPAZOxstuANOLKAXcBJGRwmTjXyhGnpAI40C8zOdWdSo5Z28zdDKwnY5o7Rzuj9cdj9aqaivohKDWMLLNxsy8rmPp4fnFbEjs2shwlWQcLniH3s15TYTh5PMIRbCUJbH3t5b1JYMSgbFnToLIScOVILVnRNd4/3bXzG/zRJrkNZOXDiKs1fL7AObMuOnosuwZwshKOzIp7p34S4oqPkW6hgYI+xV/yLP6NB/UPRzS0wA25CYQaU9fgpV0xY4gTuXFkbp93oWaE3ygcgsJPZcuLzNMpV9B+ynZwRq3hCkSDSn5A2Qw2W+rH98iTyd3HTrhY/AvmAxZ7joDSiy8bwntXzueJ62F9KWcEs/cBqJEDee1GKN0fYLE0iQek0OZIyuIzmPidYc+NshQaS9fo640uuybDCL9ylcX2daZqWN1C7cXJeQ/2hA+WUTfgZ6ps1jin6u7UiZaCM0/LrYc8i9YoFevgAHjf7xpVlQwboSOOfn8RktY/lRzRk954afWb77h2GM7fZzN4VQHjrcq0DXVjc3eEpPB0gtp3pKerEMeyDCsGbsbngjVGNnbbdxuCKZuiYohx6v7w7VUGSD3XFuiUbj25+dA5D24tXEb3DsPdXgAdRLbycNRRxrp2lDo4Us3qkOQWaISNEbJ2aY51JUKSrhl0b9YVP+4+lPx3AIiDNNKwRCkM5uBGF1A6IEvBOwFW5wMhLM7V0znJxe0AUC1VTiuLoNpvpwwqDeK51mdGWXD/quaKeSwoJa8HzdFgGaI25ITXwkiFNdvs+1NZ08LFzHanlYiNkab5YW8N4PKQLG9wWjJ6EIzagPHSxFNU2CiARzl4JRmwBL0R2pjU/095RegqRtk6zNPbpWdeAQ/42KhQCVig1DYmNl4f6D58sW5Dt6wtu3mgOYVGDV940HNzgquwyT+X7cj0FKnIcErgLErVGtlhydiVzcnUfsvWO3ZPFC7tW8brM0k6dfF/bdy1VIy17rz0vWhDYWtJq70e3SfugAYHNM2M+GfH92vBKfGsRlqp9ykiPqulGqFYLDyFkAUa7kKPKQ4xcb5X40TfAxAlq8+0mnitsUHOmZHzpnFSBQk6SjGMLdWT+fuuRHLBFwyBRJmGrRlGgH7jhmPRW4XsLQFMJb2nhpMHSIn1br1tAShNAnyQVdadhm6EvNau2PHG5HrFsU0I6mml2cQrmKz9eStmrZdQJO/dDjD6YZSjzbvtbic0b14/30b7FGCBfQlj9sBmRM3j3RFpYGsdozHaCAvhWxA5qWyQbcx+XtF7SS+GBA75TxY3K2yw7yHXxmN5JbIpV5v3XCJgEyd9vQZyUPaE18JXzROFXFBIaTWTNwynpjxS7jhAemd++KVjrGqbnp2Jl/uRuH9LljGRW7OnMtCLXmwg50mSWWA29VIdA50Efa/4JTYSLTQo5+J1eI+wRgUI8T5BazNVjEqLkRJ8kgE8yccz4nPlOuD0VhgMDpkJaF8hyHrxZPlX696L56rHubNA2aeZqLeX+BEJSrqwopWvaT/m7ecwQlPv8B9a2BUYh2KWW7ihhdwGycbBf7Z9y+3UJL3uGpyI8wq/fjDH8nHeaxYm9yvgT0Z5l19yt/AsCq+dQWvw0vsPRGlPZd92Smp08GtVKwBwOunI5K3O0LE9/VzaRkcqQO4d7kjSYFlYDGBqsWv8UaQxiQRSRnYbHP1rvaDP41Z+TsnjQd5hL85JHBHdCJ05ea4Ox4NaruHhTwV3MLgXjmunbpKSABhDn+35moou8hM/Lbu4O4Mf5exEffybrQJMqgCXZZQ6gqFNaK1B8oldQ0Z00VP9VmD1w6WjiKh3HKc4JKscZaVSTi2FtOJs0AVu+mdn4USx3sM5q+UZHn5/q3xh4c0rl23+/fA3LBnqL6gIgGABQ4o1+Y4wnC/J7kZMrnLmQf3/WRl1wV39AqcvlHJwEl4uS4w8Zb+VvolcLta9WN9nNi8sQE/jZZllh1tMNeTgKGvNuCFvANoHEj73nBzCYbR1wWOmXYnCW1N7h1hhh6BRG+Dp9fc/dXo3moxONZEgPrjLaFJtFToqyUoCVDC9HK3g1MLvsdZyXHLsS6l1HMYE2dzLcq2N85cnwWVHFAgk6nVNiU7hvRJNXjh/jjwqlBM+4hMTFAedGq+cRVKbv1TYZy5wAl1kuwPLhCyP+b3nj3p7Z5cD+wW77/xO3zKjdRlQ/L4B9Pzq7WjfAEpZ9GXB+dbKIAnsWZIi/uOGckBtNc+NCg2l0H7ln1xNqerxH90p3a5Au3ADxIYcENp7tTkL0KhBy/KjftwRvxEHPpuIod+FXh+XnGzmE2aO1Dfvpt95MVBqb4SXBx3weyQ3QRNKhRIXlVjm/cFnJ70J51hoaCngwnBMDdlJNiqhxJRV/VaGzQFzYaK/fFZck0oHwNy9gW4V2Xm4JIhS9kZhf+u6s576cTdV2Xf/NStM6iWe8kw6tc5/qLju+4gS5JnNgCQGY8NPDEu/zf6JpQ/umCMsyWol0eM9qd8KjIIgChlQc9wvByI5+aVCPldIxiTToqHhmZHVm1kEk+3SncuUQJnaDMj403Ew6n9ZzKqIaDOjfjx7fDZd6HsF9oFZfO4+JhSAJAxV9sNCjE1sLmqvHTZXJMgdjgH+AB2JnNV7jWddRx3R4SAKacjRtAk7CeqDHL8kKpj9mQVbFFANcNR6gdDg4HrEty7JiS78oOU3VRJwyUp7l5ydpGOs65ShwUoo61Q9DoFL/Sue7JVNhnf4cNGCVXCouZ3Nz3wKh/EB1pCf1jLDnpFeqQDdkj/fWmDGmcIOgb8oJk1ZoGFkZTejqaUrNmKYU3uFZORR85wzXxRzqT8E1Q4m6HQYSqsoCuuK5Yd89BzfquoesW557C6i+DgCwpKkk8A5f3EJ58xgAuaqt28e/aJaRc21OIjT75LeBP1OimJ+ssUpNDJJ9hWx6CuFd4z3sdVc2K7EQ77QbzgC8jJZWXSZ2ZsPMffJnZnVwflvjUSQdeQnXpz9aRhJAflwvz6L0Q6Tqu8OGgLRJv6EVTI2CVV3sP0tHgB1fSeOCOV0vVnQngXWK3byooMFH6VQlAaw/Nt/gxL2n+1omP4baBZLoQSai9SVDPql/NT/4zgmwTMjAvzwPR8icBCajxlw2fGNrL4dYZkShJk37Oxf9pDra3cGsXLhWfx1QvEC3042M6sHOH49yQcv3nhjnnUNOZ78bDPx01EMwvTmObLo1WktWVo7VDM0vmpJIlx3o/PY2wPFtBoayr0AjxpvOl40+EyR2LcQWqPMnODH/VTa+K4fBHxqZ5Do89wHKVDcF77ffSMuQfnA65vPgszid4QVDEcVke8ktD9l7HACgZV4SAu/DLNO1dns2RfLtEHj4TmwRCLI4a3qNZViqNGbW4G1HXTfM90sgNk8hbng1J1MbHAwWGdJvof6UJGeJpugKteTlyZpbmea7QxEHGe4n84aAfjASzANgCh7Z0BaTPc0zG+e0yA/gSpZ9z9mjrGCAYzXIwmIia/+X+5/XJcd3EUV4mSAuS9GviZTr9Rt8ZoqksLiKA+OpVJR75xrgfZVUD19sOi8GUvvEp2I+O8s/qdaH5K8aeni0UpBWczY8T+/ZaonSG/lQvLYltquB7RWqyvfJZNq+PyhoKphx8TJkTDtr3hGfVPnwJx0trR+RDMBLGj4Y71bORdKdDaDC2mk+cJRcdH6W+Klg0SXYIpzAOtQoSpAioNVRiqaRL34LO4slLCz+u/2nzJoFZpHZ7hHhROuGhRZM1BJrcZjh5JLgaa125RDC9fC9MA4dnNnylRccxlleKv8exqD70oQL1QJ1ocOLri/Ed9T8QIxuUHle0anFE6JOQn8glKN7prjS7z3rqfIknBuB1V1XkeDLHCcvZTNCibnPhdplhvj9vl/NSjQnRKgw37+OyOUXgZ9B8lscQkqS/qSMUOmMJh3B0Q6eEeuu6bTGiueubvmKhlnxfcs0tI6kYr6jZbteSDOn2cMIAN1FQQAg7zYSNiK/SWKRIN7fuBZBsMoDbDEbCgUEb3ShH4E8Bois0oUE87YFoDq3xMxzbeG3dv7EK5s9Y8VbY+610MPyJknofKF4ToSyGYS0nFxfMk+n0AgZkEzmEBrMQjUj7+J8JfPs4J/OBX1ZbyNt2ZBaGQ8ir9Pyg+rK3PI1apv//Ynco5LCicpo1qxk8mWEs9qC8zCQmoLw1AmdM8Lez8wOzj15UJruZjl6N2UNeowEIoXj2CxP48i+cM3XvgrEsxJWi8J/pJp3KSr1+A9GCjpd7m0dvd8NUIDT/DDFpMOMV7AOmJcybz6ZolyipJn/PtMZlQve++8R3bNl4q+9XUTSUV5xuv56bCtZYxTsaGFNfx5lzL+i/XmXl6WEvZFNNNTNq3Gm4iUYCT5rwF5tvDkoU/3rnabxngS9RX7WbW52Uc8ujkfrB+qfwb3Fjsi8kL2nM4fARlgVBDI3MF2xBM5TCzc+XqaxxA0c9Lr8ToCRJdgOs2DbHbI9JyO7g0POwaWiBNuUzT1BtO+hE5SqOrcBWZylyCQs7A4VuddN3cZqhE8ci9DF19CqmWn5n94g6ltw9BvKdifKTF1BJZ041EXtayYjCboZ74CVNH49t0bdUPVilgQyX836ZCWGNPvB2XD8l3WSRQkYZZSXJchiVQAqukChmYKIY+NLfrW31j/W4qq0633VYttWrNl/mVT7Jae0m8pC0gtkz3xmgsoxHnSTtPUvsKra+sRyAKS0YOM1ktrdjBuYAdePF99e5vvG1PUl5t6hZpCJQgl11qeg0Hrj3W+qCH+/sz9TdO+AbGqt34+v54Q/5T68LNrGGRDSNugUxg1YcFSALvUJe5IOdrdaYNB1sPvdBN9gXRI5L7cn8TvLYmU190jRQdzMOOUNbnSDoZhn8d63hr2hVlVx18rinAZAUOk6i+2TYZZ+s8TKUoAhGX/W06Z1hSL6JmexPK3FQSn3qQrTk3dD4HnzUpd+5Ym7J/XxqPZs21BTDFfjRpVKP4HnQHoDELeVxDwqHU3qhN1R2qZqUf3cxzBadohjIMyFo+78PpB4jDwDdeUw8Cx+0SLOAoUwYVWdMEKX1P5RSuqumA22Rdplp2lxwg57S7uMsiblaAtai4rqc/VquAW456vU0U9c+d88DH4AbxxbW1Nh/OiZ5MVgJv5Sh7DMFJvhxIPTZs1ahEEwqlDvL/+yq9fiYLitBSiK1EZ/AjUj7vVZaX+aXY02r9Udsjmb78Mcd+Ka6zQuJym7ZapGpBOqBsMhZqjTP7BkDnwsc4xbxsKXMNDvOFuuAPy7rgIcQXfcm7h52nYpdlFFdRVgw/vwpot56kWDhi8Qplvlugp1w77dHT+Um1J8hXH5yTVVufI+mZLWTrh2Zjo/ZsKvBOhBYPOv4iNw/xPzLO3MrSzNoUoJFxPAcOIDEVDTWBpvscnoNCam5ng2E26qOm4rIpJekRts2h9xcdaRnANjDNWo35oN+8dk6n4bIAjsyxSg0BAoYGM3yOO+9aDQcm/JMvfI8DMjiVOgdXcTYIDlGHajON5fcPjtgp2SUofc8nzgX7cFxTYZAz2zrk82qwOw8incrN4AePo15fHTmE+dWGU09YhDo9B1PzzztsbJ/Ezvv9PlNNkLdFovp4GmnW3kkmzFbvBw/LzQ7AFJC0Qasy4WLsir5AmFzVdqDa9iZqjH6dy7OUTdNxSWNklR7pva6LimStXhN8CLMCrqLp2oFRCq31dHxXU9SZSAVT8bBJrEnmngarYAOVa4gTsBOxYLE0s5SoyPIV8KL0baCKgpc7BBFNzQNyBB9QFNS48taJgpa2MInZ6mA3b1zlOOY3ZHRk5rINbFxPDtos200CIDgxVdUlsZAboRyLsNTDR/7YzPkQpUGQ9Syey7BRN7jDK73L5viqCTPxJ0CR2ufJwLJuevHIo4MeEnVOmE/oHA7n6qjQ9aItwSLukS4JisUdduCJyfdW0krqR35pFJnPR18iMuW43iwvSsE8Mx/XuV5glgD4r94yZ/nk5gOVXlyfX8rMtQEYUYrnXL27Z7gYDzc0q/NoFwPFduXLRqjkCTeDIkPcOSQJ7IbXD3Pmt0279mPgM/Q/BL13d2QWSbG3pNZhTkGeLquYtqTCYNm5aCUaGd+1kCPEikyP9DGAmO6Yz2evrYPscbY6D0AYCCcY6LJhTB1s4xf/Cex71+2SQNBLWzJ4n5doaOS8D6N8jJQpS0EgKtMgPi/QOacs72HyqdRNrjhbn/Eihf4ihAv+yYsjXK9I/x/sWt67w0LLBMM2BSzsJXz5RokGWtKFsu00vPEmavPU5+w0C9WMGELoiwR1CZSiINuwBXtqMss5Eyi2R/GYwkSP6q6BUVbpUqu7YaA7dYJZpLvI8KHaKypET6E9/w4oHIxSRWzW98v59LHfxsIVmnV7gb0UHP3140m/AOYGmdaU5g3KWjBz7Xm1pWJJIAL8gxKbINpeI7Qbv30q9kf8IEku4omGP99eQNcDOhrn8qvKNB9jjTRdH61NAvcpN5Hx7l4mcSTGZlwdgNLzJeitkNfeyeTyOuv+3JORUBwfOCoZDLS3UG8IeLT7ljNyb90Tqp/HPaHYcwbYdQT5ZEDbJrrUyZv/ImkuEzmctvphYmoScSe/v/duDelLnJW9pRJwFxnlZcg6+yQ79qEy1CmhGc5zjKULiUrjkJzUJYNVWDdlzbrakqJGIfe0Rz6+W5o1OIut0hubYKBIDxJ/grOjhCghUKSIIrYfupfvGW6ZuOXdsfxMSeiAc3oiFVEngViSOPO3D2kAJLjnxy6HfMPHdvHNp/Sij6eW0FDzFRxCMaZU+oTcEFgAZKpf5o3dJAVejKvUnEjQ9eo6rWkfJ+IABhTqOeoAXpJirZjBGdY/C9ymXJrTgPOP4AP4MLBQgZoRVMYd3yR9MB7qzPBZOix6xmPC/7dO0ssUk9lfRmYKaOlUnynAt+mrxvYkNZjldfcibKpfYPKlSwqGmV4zmKYXxsjGw+/P0VfKeMQIWXf4DaoImFkCdJ5kM0y9sKn5Q3LRbL7a4lITWUHS7N9uSY/fmXOPHp/7masTSXhE4cJhDJKTqxN1kFrnOE16U5wfITw8cCV8ILcV/sBnQRam3Y+Rd3DoQaCoUQSqvj7nPg8HtduT9QjFnXWSrMZU8sNda5+RBd/RYcJmm5evoiy6wr/HZ18c5/BZF0pNb0OYAZ7+PGBo8F+Qnhj7CZ6zUsR+GP8TYdskHAn9f740vE9XEgYlcd9+NajN05QmexjrbqUiIjx6c0/Bt76emFsMygaWM2HCRNj+b8YDh7SmfrzbARBtE+tcB2VhEIZ5kaueondn1YC6X8u5pbp6S1jI6Q/pVZmbt4Ee0UjRhTJEBfftIymJCOn7vWOXxkF7Li0vBjmuxDWeIf2bJXTJ04r5XaaRNnxXgqbIjJnJTQXuyOR5XZY5/E3SBcnYP9TOuqAtdRBtQaWnPrfqGqpD/4joI8B0o6jdwANsH8oE3Lf0HkSinRHQAjCLnNL2B4injaaiB3zMmn9NDPFLlZO5py2OcKQAr5sx07r0FNP9hI5IHU0KAbwgOBfSwSwpLRVkhdebtMQk1Zuv+fMrtxZhmIs65feEmzHtpvfkVjEJu4oAJxhF/XBvVYIgjsbXs+qa4PlnxXNsu3Ujc81hg/1FKkGLHwxdrDrdvkCEwpHYDXDGPc0QihrZgu3d5iqeksyjoPALKe8gNVOpY4Yq9dUo9dn6h47+paG0GAo7CSzBrhIZNfBK4jsULqFaPLFNknirhnVh2V+LUtCEXNZxd5m7QJjUDtSPUfcBVFlCvYKob1OeIMKuPmgW3XM2Ca/LTAY1rUkNBNFescVXPM1d2b5CdkG9h/srAS/fte+Eo6892TyLvGOILTBJkuzkjcqmM8js9c+swU5a2S/AJe+DlJd8T25Ip1e12AIaqsR0muSSdmQm4/BPXZXfxzPIiMjGT6vODVJsshMf4l2/Sm8C9/h/MUsYVU/k21Z6ojnwWqL8+NEVg9uTC1QVCUUhv/jpABOS3RG4+P74Cjev7eq6zvqVFAyF0kzDkQhQHY4a2YElXoInEy+FHs9cccWFE7HVWB2nHVGXy2WuYhqsSNXIUJnY7l2E46jFQDL3njhLFe6HPo08BZe4GLZWUEq2VuCRLvjOJi5VteKPFtpp0+9foU4CQ1ZBToTz6uLNHF7Ol2pdQOWK/EyqbO9OzyUlYVHaT0d+iKr9uTmUBfheu1VDJi2l42U7DsL5JNc9KnfycTfJi6wZLQ9VWzJvya5NRFhgA0lgKX++vHRXaLH+vWLp0iIiJxftpv6tzSACymSv3uK5yUwgUbwJ/RYsSa/Ma/CTIaPDwwvQ7v2Ya+yL2zWYv0Ipyxitvtl+57J1k2M3S7uJXsj1LDeB7s3K9crA9kmWd3icPWtiXhIcJ6PRe3qrAKN0bEjjWZb3WDTptu5PFqmvMZgjr+1GbuJmaoz4U85mpqDVVWbs0gcRw7aCBfo03JQMwBVSrhA0p+4yc75oJe1oQCflLL+kxwrJihrG7augxEABIYLu4S1UJBpYQGOcO4taZgY3w1CUF2ijq1GQHGAo4GFL2mFfw81nWPQb7GiLSq61hJphRmvzB8+UgQxF/c9JwKJmjlY0zDhtXHbK0IyGtcs847hMjM0HO9F54b6F0jtGI4CkrgR49R/VrokrEVzHrxRuccSDQ+Fs/HUEJGDIr9hjAcAxQyV18qk5xr3mBVIsITKpriV2IkztGGvDWJbLnlXqDzqpCvB+cwFXn+Rf5RiDYq8uzzWmZDUc7V/9j920mD0q+HQ9GR0RdTNMfP1JNPouBqQVKPhtw3t4FThybYgtpLQJyJNFF56wt5EkBldIKu1FMw2QO4hgfLN6MrwsvW9oo+zWr0w9POCPOtiE1tAIklWRSnrluCNNhCZM2pugs3OntmbzftQEZ+Jq8HnNvx+N0omYusm+6RBjvNyTlDq3Xg73EvNqQAiRtlBbygawQvvkVbcwU8XaG8zW+NbqEwOEzJAXnp+L6VCtEEV7RKu2nvswPwKExZVn/UxEjbEz0OlqDj2hXQAKWyKsTMEqCk3j6N454GG4QSURAAtjKLuB942fmkihnB6CvsMHlcg+BiZk6JzRAntLBhkIWrlLfyFm2pWBP0Tpxb07uSIRLoetbEXpAIyTtFOLauJtFoH+khhP0iZ5N1zzPbytL4lX2RNpIJjS/zuxRkcgjnFPAbheYDnlUuMxMbhe4YCyGFFKfWDn48ToB9xNSiRgf2vQC78TnqlSmhmLB1gdljfFTig+t0F84EB8MttBfvPVC4eRK+1q9O9+f1wva/IO0WMgZHX4+O5bTN6JtqA5jNAGD0jUITIG8SHMCSjTljUFCuAWK/ejVfck8DlfltldTrwAQavM5/a8edO6qcbbVFrKHPXc16U8c1+j0G4gqqcLEl9MN0trhyo4gKsMDtLI+0XOEFKHRkFVoySa2qQIPGOC+2TYxGDdVTq/LOdO1Ms3YHtOizkwb4idiMh7Pv2lVq5hccMiItQ5feXwdg0Ify8e6oF8txS0kqjj/lLr8vS565xZnGUC0HAbNBKoBvPdV4Wi6cSkSytoAQmhS/0g4t2oftl+KVE6DLaJE/5jmABnFv5Mi1xAAiL3usygmijkli6OJuaIjP2jQXcI5G6ablRF2Cw3es9fM+aO9mXFUoEQAAcauAq7DAH1BIMW0fK0bADEcHa7ICZWnNEU7cMCiDzTnJIaYIKf44W+F3TqXAfr5n1lBziS+dz6pb0k2ohOSZ0aewug0XBFVbZcLekmny3K/cBhI9nUX3qJPU81+jxU9GJMkRn/fVieOBjsZycfOyCsI1bxeu/BShjZQwgPYnDSqWToNaqsSgCVH1UoFvVlEJRul9Xq+jIv0gkGvPsIZfXC45TzTHINc4EfKLc6iiIwtGrvlwxvShnWp9zQ+y+pL5UKPPiCEU71dhyaOPJG97rJpJoX9zLjEdg76/7ZcedYlHMjV0yDJPh/tGa1w+N8Uh+FbTqmiDJNV6QzC+xVpPTzzDlMERI8ncpMLPvMbrOrkXw9pjTKT7602Gxdubc/N6ijooxVDsO8LLFxsEJq7mQHNg9flIsFJQdOUjzwgG1+7BVJcYTuhpIJUp1Ej6a1y3HZbe9Hyf5JLXw3/LUVfiBRYC8DsOOmWE4Ot9flYYeei3ZedfiKzgNO1Ks9riQyagzRmVVf0UjO1/h89S1WSDmc6G4yZ/ZxGdQZ87lYoQGV0QXw7dQ3jRWj0jML2vdbvfwgGiBSdHOqFHSnoeKuj5Cts3fCCFb25i96BiO91UKQpycI8D1elSQAuzuafHbu68otFjEbuHLYBGQU0Lg+hHYrIAbToaxfD78qKcpkmSWDnKvijpLTjvw9RV8mGWcHqrKBCxXciIlkxmaSe0AaDNcLTo+ryFA7P08gsNGvILSS79BeZpVWaUqLHQp7FSdQN4bqOZ2mFFcnuTUEQKhCoAzmFNOnnHdONdsF+VsaifnJ1ykyArBJeYrodVL9cjETC3IXiTaphEaPqKtya4oZ/36X6EG9IF/A/FBaINBIZ86XIpawgDCjQ2WYN/ZR1488qjsJ/gNPmJIKEDQm7ecsXkYiP2Lmscsbw7C9ICmBiyzP8V93opnbFhC/s2QPP1H3dDWNV0gagD/c7rmG276ixmgkf6NZ2DHju1g5Hlbwxct/PkEsCVBALMVewUgWypGfjh/T1MF1iSyLZhGPxq46LaLKZWXgZZA0aAy/BOg+LEcoQZWjG/m66SbQX1rhHrTGGjh492b/Jeu+SZ/QHmYsFB/nAuRNmwCiJ0YkRK4lFOhCueXBwudVp8jRS+w0WlSu21DEicj8jX2Nj8FM2ykZbKy6CYBxlqAykXzE2uWzJ1ArK326MYHwhri71X5pb8h5HKwpAnhonVO12OFyIezPaOWJjAv9k557HVWxq0zOowZ0O2rSZJ5jaqNlvYXzlJCIusTvkEIJRsXSko72xhqiYy9k/hsAn5SshFNl9mReFXPEvSrrb4qwECV/0oSezJTGAhymHcnUF4QY2CPmHkFeuh7lYj9iGE6N+rTWu+ogpSQMyw1zjB2gaxgJc3Fr9uqoodnWPLjqc9wLRHkKbgzxddzCoEf+Fh2ytyIJenZXxWpjnYnzsqHe2ZxWpucicfcYqGvMc/r90DvqVmoG47yQaV0cVgf7srLp+dVnuUv/Z5hGa60ENTIC/8JZYVaS2OBsRWyGgKyJ/+Jsy3AwJRJKf4mpaNP8nEYfFJXYTSEeB/gA9F8dM9bv2riLKWaG5NkekrKJTJSUmwALApGX2TTlyW4/Akxk/VMECBE10s3cN36TWz0hECaHmTqvt/lH+8zLDpuHLDzg6ApdkkqaL+nIDmhYiIk3olhvkBGmA/yEWIaWTs7H9DVrEXPM0G/MuaGaZqE2jQ82zCfotmUzO3VD157c3oVvEpn47EdN+tBfeLmA4m+ylDtTh+3Ra9PaeNvuIrGj+JmzVVuKQn5Y/IbphgGVqxUvJ37oQCOH5tflF5uRZit3vMiBOhvHD5ZHzW+cVrEtjigxkhrMKHOerBSzHN8Rc3ryB9b9U2fPk8gpfpRvtTVCy+QGEysvMMXL/leLz7ZDWYkvUhxiTBRuqHmIOt9fs5avcx3QpkmSljQHzm4gOUFO/99H+3Fqjw9NvhyTRJHabL1RFrhnwxuW1GaXFleiPq946YGcnoy6/zEGyNfC0ss9jPbZj8moLxUgJ/OFhkbM3tXSBLKg3zq66dZlz8ujfQ9cLqab6Z1UuIVIn4aekJ+yDddKGTjpILpwqoKj8VohkqZs54yKSDWBv3HLmTXHd4SgftMkk0JZaMVSXwEsTGgtuZMI3qQu2MnOs88MoiXuq6nyrS3r9WEiWJLtkFfM9AnKg4uD+p3OwTFDaMdjtUNi0ITBAlVWMLbsIDthlyvnOseN8pPGtUUvdS6s2V98MoRbPPzc7iA0xGobCPUFvyarBuVyB5GZhjsymbiuGmjaY6Q80pTeCSlAWcVoEFt/8PXwJ4pIscrDLGu/xVu8/emmOBoH3G+y/Zsdtj+/dtDy+DFySgYrMOKEI9PJl6EpvVrZ5uYWs/5XDz6F7jd7xCbeNQGM9xZj1s61ItFwpZ7vFUm3ZCwpwyzHuTJwnx12TvOGXCgw+f44/C++4Xp8T6h7Wp20PS8IVk+O3c3k7Y0Y7XInXGyxt4k82y6F7ewD9lBcD59KJhG86QwGiyth9/5HIZGHpfbYeWGJbaCKVlb23MqFIMJQ9xUTxesR+I4fAzThtEUT1vOUlppop54tMi/WHiflRfVOVmtSa5nZ+7qEWfzYKbAq5fD51YFqBPu7R166wDmnP62a4ovPDiDl2Zh1XZEJ+jkbNB3qGyy2VtnO8MKFQ8Y9pfYpGC3YN30B0HXT7IMkETjwlTf0hYGOV6BxPmvek7KtRPh8XEhDWkyBfxHRd6wmUaxE0Vi1/6YyvtXl4kH1O4KOSlglWsT01yAPyG8xSpVxB0+9mKX0eYI9Fm6HzI400rWuPhvq8V/6QM65RlDMXE/hA/YpPcVSRP80N+8ukbowbS5iz2s6iRhXKH+tVYhWq8rFe+wSSoOgrnfbpLRdc40AJAICdaS/DBLNsfzrgJv4wya4rCYDR6prnKu6YifAEHY1+JRjv4B9b9W0wefIylNZIvgnrIvxyVBzGRw3KzGik4EG5gU5uUyftYAN1qzKuGQ/BhDzMdeOTRKDPPv+zCQk+uvajJJJJFe7JpmvkvLrQdzogxhzrRbe3d5eBWZEjKBXni2Enre/yGHkiTIVKPDw+wAkqXWW0n3oD3DEr5pmTaN8te+qq6zYEmM3ZgqaWmVJiJu9qFxeQcOs/uNRp/0mdWaGXmT4Cx7GLBF/XX62hdOh/w+0rYQGL2RQeZeRvu53wzbAIadtlmu518WmbOSDKE0VHxxB8OgVjEVL9PL5zcuhbkEgqYIJK/mvmFxaoRfWByuV9any376mx2TIi5/w8UKQbrf3tO69gJh+wz+rheBnxjwdjmgq9S2A+NKj5zqisjAXLkGwi/0BRYxK8ojkJ66pZtLJ3KpIvVWXKJzetQcl8WK6bTVmrUI5WwbK1kVreeHIqslXNr1OGrFZPqRs98lo3lzzdMb6aWsHUYdSQt40IJMKQ2mxyyWRtegMHHUqm9Lkqc4DhAOEpzZsgqjMyo7SpaH2iz+AiLB+SEVgBYlKaF5sbLW3ZjVpgp3WUDk7PLVBjaCgPvpmRKanigvQHyIX7+Kwo7mdbruWRI8xCpmIl+OQ6c2byu4Hy8pSb3C7jF1zAOcyi86PtInQmzkKlM1I4ZDBr4d21PMtkBZg4ORGhpbse2gn/mQzJom8eexeKDAg2HTS1SB6Hm/xt/nXTMB9jdpaS8x9976xxAoj2XXXlVKABzRm5wxVj3A9KUo20DsqkHSKE2C50XOKILKxZMby2GyJ+LI1SdQmQTw43Di6u/69yLnydHeDkKYgEuEMJeaYcjkGpqbE8FQ2SOvRADPrUsHWlwM37Oi9AoOyuxlInKXo6EdHefjibWGrfyokmOyQl5ggtluM1zpBBkW8vkEyFujme3AQWt36VS18np/TMbH4sDbOZpthKP/+Y/N/DlT+loPASGkbLYapFdsSjjWUlS41hJYallm+Ak9j6DJO41CfLxupdlvz651ds8h45kUR8K1ZWHMk7nWlDdh7F1//PisgvDfwxrdy2ISoPGu994osSwuzjNEcpQYgYW10yHAnbwaPhWfQNxWo+0T7+youLmDOsStYoYlICrT37DnDH/mity71Jrh+AcI8Cdlcvq479/+lRkM+q1lFAdN87aw=" />


<script src="/js/dnncore.js" type="text/javascript"></script>
<script src="/js/dnn.modalpopup.js" type="text/javascript"></script><script type='text/javascript' src='/DesktopModules/Exionyte/Menu/1/Xeon/Scripts/menu.js'></script><script type='text/javascript' src='/DesktopModules/Exionyte/Slideshow/2/Xeon/Scripts/jquery.cycle.all.2.74.js'></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { $('.slideshow').cycle({cleartype: true, cleartypeNoBg: true, fx: 'scrollHorz',timeout: 6000,prev:   '#Prev', next:   '#Next'}); var a1 = document.getElementById('arrows'); a1.style.display = ''; var b1 = document.getElementById('bannerPane'); if (b1) b1.style.display = 'none'; });//]]>
</script>

<script src="/js/dnn.jquery.js" type="text/javascript"></script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d3.5.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3ab4491a8b-c094-46c4-848c-d2a016749dfa%3aea597d4b" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="W/YlVS6D9Ji+0WFAFrfyjqHKEMJFTd20FJAZargnWnr5rMFKpxCW5BUguyE3KNWXsHEhffvar1kR56b0liVV2fbw0TlE/fjt9AOI+XRSYjS39uSefpKwe/ZFdkwShWFQuz9j3g==" />
        
        
<script src="http://cufon.shoqolate.com/js/cufon-yui.js?v=1.09i" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Futura.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Font.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/jquery.pngFix.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/general.js" type="text/javascript"></script>
<div style="display: none;" id="message">
    <div>
        <h4 style="text-align: center;">
            Sorry, This page dosen't support Internet Explorer 6, If You'd like to view all
            the contents properly please upgrade your browser to IE7, 8 or 9. <a href="http://www.microsoft.com/download/en/details.aspx?id=2">
                Click here to get the new version of Internet Explorer</a></h4>
    </div>
</div>
<!--flash section-->
<div id="flashPane" class="flashSection" style="display: none;">
    <div class="flashPadding">
    </div>
    <div id="website" class="flashContent">
        <p>
            In order to view this page you need Flash Player 10+ support!</p>
        <p>
           
        </p>
    </div>
</div>
<!--/flash section-->
<div class="skinLayout">
    <div class="skinWidth">
        <!--header-->
        <div class="skinHeader">
            <div class="skinMinWidth">
                <div class="logoLayout">
                    <div class="logoPosition">
                        
<div style="float: left; display: inline;">
    <a id="dnn_dnnLOGO_hypLogo" title="North Maharashtra University, Jalgaon" href="http://nmu.ac.in/en-us/home.aspx"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/0/logo.png" alt="North Maharashtra University, Jalgaon" border="0" /></a>
</div>


                         <div id="dnn_logoPane" class="DNNEmptyPane">
                        </div>
                    </div>
                </div>
                <div class="menuLayout">
                    <div id='menu'><ul class='menu'>
<li class='current'><a class='parent' href='http://nmu.ac.in/en-us/home.aspx'><span>Home</span></a></li>
<li><a class='parent' href='http://nmu.ac.in#'><span>About Us</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity.aspx'><span>About University</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/misson.aspx'><span>Misson</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/universitysong.aspx'><span>University Song</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/fromvcsdesk.aspx'><span>From VC's Desk</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/informationatglance.aspx'><span>Information at Glance</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/achievements.aspx'><span>Achievements</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/vicechancellorslist.aspx'><span>Vice Chancellors List</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/annualreport.aspx'><span>Annual Report</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies.aspx'><span>Governing Bodies</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/managementcouncil.aspx'><span>Management Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/academiccouncil.aspx'><span>Academic Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/senate.aspx'><span>Senate</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/standingcommitteemembers.aspx'><span>Standing Committee Members</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/deansoffaculties.aspx'><span>Deans of Faculties</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>University Campus</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/aboutcampus.aspx'><span>About Campus</span></a></li>
<li class='child'><a href='http://nmu.ac.in/kamc'><span>Khandesh Archives and Museum Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/centralschool.aspx'><span>Central School</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/campusmap.aspx'><span>Campus Map</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/howtoreach.aspx'><span>How to Reach?</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/extensionactivities.aspx'><span>Extension Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/researchfacilities.aspx'><span>Research Facilities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/mous.aspx'><span>MoUs</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/home.aspx'><span>Satellite Centers</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/eklavyatrainingcentrenandurbar.aspx'><span>Eklavya Training Centre, Nandurbar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/rcp'><span>Pratap P.G. Research Centre of Philosophy</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols/en-us/research/labtoland.aspx'><span>Pratap Shashvat Adhunik Sheti Tatvdyan Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/mahatmagandhiphilosophycentredhule.aspx'><span>Mahatma Gandhi Philosophy Centre,Dhule</span></a></li>
</ul></div>
</li>
<li class='child'><a href='/LinkClick.aspx?fileticket=oqDHHmONXsE%3d&tabid=3881&language=en-US'><span>RGS and TC</span></a>
<div><ul>
<li class='child'><a href='http://apps.nmu.ac.in/circulars/Bcud/29-09-2015%20Submission%20of%20Pre-proposals%20under%20the%20scheme%20assistance%20for%20S%20and%20T%20applications%20through%20university%20system%20of%20RGS%20and%20TC,%20Govt%20of%20Maharashtra.pdf'><span>RGS and TC Schemes</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Information</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Academics</span></a>
<div><ul>
<li class='child'><a><span>Schools</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/soes'><span>School of Environmental & Earth Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols'><span>School of Life Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/socs'><span>School of Chemical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sops'><span>School of Physical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docs'><span>School of Computer Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/domaths/'><span>School of Mathematical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/udct'><span>University Institute of Chemical Technology</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docll'><span>School of Languages Studies &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doe'><span>School of Education</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doms'><span>School of Management Studies</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soss'><span>School of Social Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soah'><span>School of Arts and Humanities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/schools/schoolofthoughts.aspx'><span>School of Thoughts</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/dat'><span>Buddhist Study and Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/smcrc'><span>Chhatrapa Shivaji Maharaj Chair & Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/mpsrc/'><span>Mahatama Phule Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/svsrc'><span>Swami Vivekananda Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/grc'><span>Mahatma Gandhi Study &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sgsk'><span>Sane Guruji Sanskar Kendra</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/nmuccc.aspx'><span>NMU-CCC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/phd.aspx'><span>Ph.D.</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation.aspx'><span>Adult Education</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/eventsandactivities.aspx'><span>Events and Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/coursesoffered.aspx'><span>Courses Offered</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/tbc'><span>BACECC</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=xCiw3cuw70w%3d&tabid=3419&language=en-US'><span>Community College</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=8DmFd7nqvqo%3d&tabid=3801&language=en-US'><span>Tribal Academy Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/committees.aspx'><span>Committees</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Administration</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/registraroffice.aspx'><span>Registrar Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/lawrtisection.aspx'><span>Law/RTI Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/faooffice.aspx'><span>FAO Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/examinationcenter.aspx'><span>Examination Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/eligibilitysection.aspx'><span>Eligibility Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/sportssection.aspx'><span>Sports Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/scstcell.aspx'><span>SC|ST Cell</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/constructionsection.aspx'><span>Construction Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/prosection.aspx'><span>PRO Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/approvalsection.aspx'><span>Approval Section</span></a>
<div><ul>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Online Approval</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/computercenterexam.aspx'><span>Computer Center (Exam)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/universityofficers.aspx'><span>University Officers</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=ZprmAC3RMhY%3d&tabid=1350&language=en-US'><span>Phone Directory</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>BCUD</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/aboutbcud.aspx'><span>About BCUD</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/developmentsection.aspx'><span>Development Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/affiliation.aspx'><span>Affiliation</span></a>
<div><ul>
<li class='child'><a href='http://affiliation.oaasisnmu.org/'><span>Online Affiliation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/affiliation/licreports.aspx'><span>L.I.C. Reports</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/research.aspx'><span>Research</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/iqac.aspx'><span>IQAC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/avishkar.aspx'><span>Avishkar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/bsrschemeprogrammee.aspx'><span>BSR Scheme/Programmee</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Student Corner</span></a>
<div><ul>
<li class='child'><a><span>Academics</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/admission.aspx'><span>Admission</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/syllabi.aspx'><span>Syllabi</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/questionbank.aspx'><span>Question Bank</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/academiccalendar.aspx'><span>Academic Calendar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/papercodelist.aspx'><span>Paper Code List</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac'><span>e-Suvidha</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://exam.nmu.ac.in/'><span>Examination</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlineresultscollege.aspx'><span>Online Results (College)</span></a></li>
<li class='child'><a href='http://182.56.2.247:81/nmu'><span>Online Result (Student)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/convocation.aspx'><span>Convocation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/resultsofverificationredressal.aspx'><span>Results of Verification|Redressal</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examtimetable.aspx'><span>Exam Time Table</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examreports.aspx'><span>Exam Reports</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlinedegreeverification.aspx'><span>Online Degree Verification</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac/QPCourseSelection.aspx?ID=660'><span>Previous Exam Question Paper</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examination.aspx'><span>Examination</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Facilities</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/financialassistanceschemes.aspx'><span>Financial Assistance Schemes</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails.aspx'><span>Hostel Details</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/hostelday.aspx'><span>Hostel Day</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/boyshostel.aspx'><span>Boy's Hostel</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/girlshostel.aspx'><span>Girl's Hostel</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Services</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Download  Formats</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/examinationsforms.aspx'><span>Examinations Forms</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Formats For Colleges</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforemployee.aspx'><span>Formats for Employee</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formsforstudents.aspx'><span>Forms For Students</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Statistical Format</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline.aspx'><span>Student Helpline</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline/studentfacilitationcentre.aspx'><span>Student Facilitation Centre</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studentwelfare.aspx'><span>Student Welfare</span></a></li>
<li class='child'><a><span>E-Resources</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/eresources/ejournals.aspx'><span>E-Journals</span></a></li>
<li class='child'><a href='http://nptel.iitm.ac.in/'><span>NPTEL IIT Lectures</span></a></li>
<li class='child'><a href='http://apps.webofknowledge.com/'><span>Web of Science</span></a></li>
<li class='child'><a href='http://www.sakshat.ac.in/'><span>NME-ICT</span></a></li>
<li class='child'><a href='http://jgateplus.com/search/'><span>JGate</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Useful Links</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/events.aspx'><span>Events</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/tenders.aspx'><span>Tenders</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/rti.aspx'><span>RTI</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/circulars.aspx'><span>Circulars</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pressroom.aspx'><span>Press Room</span></a></li>
<li class='child'><a><span>Job Openings</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/university.aspx'><span>University</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/collegesinstitues.aspx'><span>Colleges/Institues</span></a></li>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Apply Online</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/sciencepark.aspx'><span>Science Park</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=pDjshMkKzgA%3d&tabid=1088&language=en-US'><span>Citizen Charter</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/publications.aspx'><span>Publications</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pbasproformaandapidetails.aspx'><span>PBAS Proforma and API Details</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/otherlinks.aspx'><span>Other Links</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Us</span></a></li>
</ul></div>
                </div>
            </div>
        </div>
        <!--/header-->
        <!--banner-->
        <div class="lightBlueCenterRepeat">
            <div class="lightBlueLine topLine" style="z-index: 101; position: absolute; top: 0px;">
            </div>
            <div class="lightBlueLine bottomLine" style="z-index: 101; position: absolute; bottom: 0px;">
            </div>
            <div id="bannerLeft" class="lightBlueBannerLeft">
            </div>
            <div id="bannerRight" class="lightBlueBannerRight">
            </div>
            <div id="bannerCenter" class="lightBlueBannerCenter">
                <div class='slideshow'><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage1.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage2.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage3.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage4.jpg' alt=''/></a></div></div>

            </div>
            <div id="bannerPane" class="banner" style="display: none;">
                <div class="skinMinWidth">
                    <div class="contentPadding bannerPadding">
                        <div class="top-cols">
                            <div id="dnn_Banner_Pane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Banner_PaneLeft" class="colHalf left DNNEmptyPane">
                            </div>
                            <div id="dnn_Banner_PaneRight" class="colHalf right DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/banner-->
        <!--banner arrows-->
        <div class="skinMinWidth">
            <div id="arrows" class="arrowPositioning" style="display: none;">
                <div align="center" class="arrowLayout">
                    <div class="lightBlueArrowBGLeft">
                    </div>
                    <div class="lightBlueArrowBGRight">
                    </div>
                    <div id="Prev">
                    </div>
                    <div id="Next">
                    </div>
                </div>
            </div>
        </div>
        <!--/banner arrows-->
        <!--slider panel-->
        <div id="toppanel" class="topPanelLayout" style="display: none;">
            <div class="topContent">
                <div id="panel">
                    <div class="skinMinWidth">
                        <div class="top-cols">
                            <div id="dnn_Content_TopLeftPane" class="col1 left DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopRightPane" class="col3 right DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopCenterPane" class="col2 DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Content_TopFullPane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottomPanelSpacer">
                </div>
            </div>
        </div>
        <!--/slider panel -->
        <!--slider nav-->
        <div class="fullWidth">
            <div class="centerSliderPanel">
                <div id="toggle" class="topSliderLayout" style="display: none;">
                    <div id="close" class="open sliderOpen">
                    </div>
                    <div id="open" class="sliderClose close" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <!--/slider nav -->
        <!--content header-->
        <div class="skinContent contentTop">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="contentPosition">
                        <span style="color: #fff;">n</span>
                        <div class="contentPositionLeft">
                            <div class="loginImage">
                            </div>
                            <div class="displayText paddingText">
                                
                                <a href="http://apps.nmu.ac.in/Login.aspx" target="_blank" class="lightBlueContentLinks">
                                    Portal Login</a>
                            </div>
                            <div class="loginImage">
                            </div>
                            <div class="displayText">
                                <a href="https://login.microsoftonline.com/" class="lightBlueContentLinks">Mail Login</a>
                                </div>
                        </div>
                        <div class="contentPositionCenter">
                            <div class="paddingText">
                                <strong>अंतरी पेटवू ज्ञानज्योत</strong></div>
                        </div>
                        <div class="contentPositionRight">
                            <div class="displayText paddingText">
                               Select Language :
                                    <div class="language-object" >
<select name="dnn$dnnLANGUAGE$selectCulture" id="dnn_dnnLANGUAGE_selectCulture" class="NormalTextBox">
	<option selected="selected" value="en-US">English (United States)</option>
	<option value="mr-IN">मराठी (भारत)</option>

</select>

</div>

                            </div>
                            <div class="lightBlueContentLinks searchText">
                                <div class="searchTextLayout">
                                    Search&nbsp;&nbsp;
                                </div>
                                <div class="searchImage">
                                    <div class="search_bg">
                                        <span id="dnn_dnnSEARCH_ClassicSearch">
  
  
  <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" onkeydown="return __dnn_KeyDown('13', 'javascript:__doPostBack(%27dnn$dnnSEARCH$cmdSearch%27,%27%27)', event);" />&nbsp;
  <a id="dnn_dnnSEARCH_cmdSearch" class="dnnSearchCss" href="javascript:__doPostBack('dnn$dnnSEARCH$cmdSearch','')"><img src="/Portals/_default/Skins/Xeon/images/sright.jpg" align="top" border="0" alt=""></img></a>
</span>


</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content header-->
        <!--content containers-->
        <div class="contentContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane1" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane1" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1" class="col1Home left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane1" class="col3Home right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane" class="col2Home">
                        <div class="DnnModule DnnModule- DnnModule--1">

<div class="containerPadding"></div>

<div class="overflowHidden">
    <div class="lightBlueBG">
        <div class="style1TitlePosition">
            <span id="dnn_ctr_dnnTitle_titleLabel" class="style1Title">Privacy Statement</span>



        </div>
        <div class="style1Arrow"> 
            
        </div>
         <div class="clear"></div>
    </div>
    <div class="spacer"></div>
    <div id="dnn_ctr_ContentPane" class="style1Content style1Border lightBlueContent"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>North Maharashtra University, Jalgaon is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the North Maharashtra University, Jalgaon Web site and governs data collection and usage.
        By using the North Maharashtra University, Jalgaon website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. North Maharashtra University, Jalgaon also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by North Maharashtra University, Jalgaon. This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by North Maharashtra University, Jalgaon for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the North Maharashtra University, Jalgaon Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through North Maharashtra University, Jalgaon public message boards,
        this information may be collected and used by others. Note: North Maharashtra University, Jalgaon
        does not read any of your private online communications.</p>
        <p>North Maharashtra University, Jalgaon encourages you to review the privacy statements of Web sites
        you choose to link to from North Maharashtra University, Jalgaon so that you can understand how those
        Web sites collect, use and share your information. North Maharashtra University, Jalgaon is not responsible
        for the privacy statements or other content on Web sites outside of the North Maharashtra University, Jalgaon
        and North Maharashtra University, Jalgaon family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects and uses your personal information to operate the North Maharashtra University, Jalgaon
        Web site and deliver the services you have requested. North Maharashtra University, Jalgaon also uses
        your personally identifiable information to inform you of other products or services
        available from North Maharashtra University, Jalgaon and its affiliates. North Maharashtra University, Jalgaon may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>North Maharashtra University, Jalgaon does not sell, rent or lease its customer lists to third parties.
        North Maharashtra University, Jalgaon may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, North Maharashtra University, Jalgaon
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to North Maharashtra University, Jalgaon, and they are required to maintain
        the confidentiality of your information.</p>
        <p>North Maharashtra University, Jalgaon does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>North Maharashtra University, Jalgaon keeps track of the Web sites and pages our customers visit within
        North Maharashtra University, Jalgaon, in order to determine what North Maharashtra University, Jalgaon services are
        the most popular. This data is used to deliver customized content and advertising
        within North Maharashtra University, Jalgaon to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>North Maharashtra University, Jalgaon Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on North Maharashtra University, Jalgaon or the site; (b) protect and defend the rights or
        property of North Maharashtra University, Jalgaon; and, (c) act under exigent circumstances to protect
        the personal safety of users of North Maharashtra University, Jalgaon, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The North Maharashtra University, Jalgaon Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize North Maharashtra University, Jalgaon pages, or
        register with North Maharashtra University, Jalgaon site or services, a cookie helps North Maharashtra University, Jalgaon
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same North Maharashtra University, Jalgaon Web site, the information
        you previously provided can be retrieved, so you can easily use the North Maharashtra University, Jalgaon
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the North Maharashtra University, Jalgaon services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon secures your personal information from unauthorized access,
        use or disclosure. North Maharashtra University, Jalgaon secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>North Maharashtra University, Jalgaon will occasionally update this Statement of Privacy to reflect
company and customer feedback. North Maharashtra University, Jalgaon encourages you to periodically 
        review this Statement to be informed of how North Maharashtra University, Jalgaon is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>North Maharashtra University, Jalgaon welcomes your comments regarding this Statement of Privacy.
        If you believe that North Maharashtra University, Jalgaon has not adhered to this Statement, please
        contact North Maharashtra University, Jalgaon at <a href="mailto:support@nmu.ac.in">support@nmu.ac.in</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
</div>

<div class="endContentPadding"></div>

<div>
    
    
    
    
    
</div>

<div class="containerPadding"></div>

</div></div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1Inner" class="col1Inner left DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPaneInner" class="col2Inner DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane2" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane2" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane2" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane2" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane2" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane2" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane3" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane3" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane3" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane3" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane3" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_CenterPane3" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content containers-->
        <!--spacer-->
        <div class="spacer">
        </div>
        <!--/spacer-->
        <!--footer line-->
        <div class="footerLineTop">
        </div>
        <!--/footer line-->
        <!--footer containers-->
        <div class="lightBlueFooterContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Footer_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_HalfLeftPane" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_HalfRightPane" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_LeftPane" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_RightPane" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_CenterPane" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
            <div class="lightBluefooterBottom">
                <div class="skinMinWidth">
                    <div class="contentPadding">
                        <div class="footerPosition">
                            <div class="BottomLinks footerPositionLeft contentWidth">
                                <div class="displayText">
                                    <a id="dnn_dnnPRIVACY_hypPrivacy" class="BottomLinks" rel="nofollow" href="http://nmu.ac.in/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx">Privacy Statement</a></div>
                                <div class="displayText">
                                    &nbsp;&nbsp;|&nbsp;&nbsp;</div>
                                <div class="displayText">
                                    <a href="http://nmu.ac.in/disclaimer.aspx"
                                        class="BottomLinks">Disclaimer</a></div>
                            </div>
                            <div class="footerPositionLeft bottomContainerWidth">
                                <div id="dnn_BottomPane" class="DNNEmptyPane">
                                </div>
                            </div>
                            <div class="footerPositionRight contentWidth">
                                <div class="displayText">
                                    <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="BottomLinks"><a href="copyright.aspx" style="color:#fff;">Copyright</a> 2012 by North Maharashtra University</span>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/footer containers-->
    </div>
</div>
<script language="javascript" type="text/javascript">
    function Showie6Message() {
        var IE6 = (navigator.userAgent.indexOf("MSIE 6") >= 0) ? true : false;
        if (IE6) {
            document.getElementById("message").style.display = "inline";
        }
    }
    Showie6Message();
</script>

        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" />
    

<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>
    <script type="text/javascript">
        
        //This code is to force a refresh of browser cache
        //in case an old version of dnn.js is loaded
        //It should be removed as soon as .js versioning is added
        jQuery(document).ready(function () {
            if (navigator.userAgent.indexOf(" Chrome/") == -1) {
                if ((typeof dnnJscriptVersion === 'undefined' || dnnJscriptVersion !== "6.0.0") && typeof dnn !== 'undefined') {
                    window.location.reload(true);
                }
            }
        });
    </script>
</body>
</html>
