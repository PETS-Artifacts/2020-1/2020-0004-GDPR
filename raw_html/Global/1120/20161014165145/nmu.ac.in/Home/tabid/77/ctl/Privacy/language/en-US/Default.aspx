<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="en-US" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><meta content="text/javascript" http-equiv="Content-Script-Type" /><meta content="text/css" http-equiv="Content-Style-Type" /><meta id="MetaDescription" name="DESCRIPTION" content="Official Website of North Maharashtra University, Jalgaon" /><meta id="MetaKeywords" name="KEYWORDS" content="North Maharashtra University, Jalgaon, Maharashtra, Higher Education, Academic Institute, Research Organization, University, NMU, India, Maharashtra, Research, Under Graduate, Post Graduate, Certificate,Courses, " /><meta id="MetaCopyright" name="COPYRIGHT" content="&lt;a href=&quot;copyright.aspx&quot; style=&quot;color:#fff;&quot;>Copyright&lt;/a> 2012 by North Maharashtra University" /><meta id="MetaAuthor" name="AUTHOR" content="North Maharashtra University, Jalgaon" /><meta name="RESOURCE-TYPE" content="DOCUMENT" /><meta name="DISTRIBUTION" content="GLOBAL" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><meta name="REVISIT-AFTER" content="1 DAYS" /><meta name="RATING" content="GENERAL" /><meta http-equiv="PAGE-ENTER" content="RevealTrans(Duration=0,Transition=1)" /><style id="StylePlaceholder" type="text/css"></style><link id="APortals__default_default_css" rel="stylesheet" type="text/css" href="/Portals/_default/default.css?7230930" /><link id="APortals__default_Skins_Xeon_skin_css" rel="stylesheet" type="text/css" href="/Portals/_default/Skins/Xeon/skin.css?7230946" /><link id="APortals__default_Containers_Xeon_container_css" rel="stylesheet" type="text/css" href="/Portals/_default/Containers/Xeon/container.css?7230946" /><script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery.min.js?1.6.1" ></script><link href="/Portals/_default/Skins/_default/WebControlSkin/Default/ComboBox.Default.css" rel="stylesheet" type="text/css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico' type='image/x-icon' /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/CSS/menu.css" rel="stylesheet" type="text/css" /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/Colors/lightBlue.css" rel="stylesheet" type="text/css" /><title>
	North Maharashtra University, Jalgaon > Home
</title></head>
<body id="Body">
    <script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery-ui.min.js?1.8.13" ></script>
    <form method="post" action="/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="BmK9ME6QWd3Z95zQGRCXBTCX0TZhXdhs6BEIlJbRLd94GWML5V12CH8H+qQ61l+HdHP4eW7z/4on2OY1/qAOnxQoH3/ySN/ejnFT2t6OyR3RHOga0KGft0Mz227lg/u4AzC6pFOHNxgnQZ1d9IqnCT7sYmwxzT/2ZaXn0vsNBUIgIZQ7tJrQD6Xk6YoyrXkxbNEFcuUmQ29Lt4Axly9D6N0zHm8U4HP0lhwgmzbyIYzWAV/sZ040SmPcP7qQo8410L7aeQsAZWhXYZgDWUMwseAjI/ZlGk/3t2ZE5eD+wzPzk9ifTQCTTcDyITS1TSswpe0B3LWrWVnpw6Q7CguFYPsedYIB40w8S02wd9/4pQb23kU3M7cpXbwlb229cKLM57jlI+JjvWShP/wQ2aRoMPHiJ8Igw/GXHT3STiSPJae/m5gQEoFdsqsPp4pc6mW3hWgQowcUH7E8IC5M13uwzLYItPyHBBUPtOaHSYxTGMcvmZt/NpwXdds44TJXEMYi8AHttVaxVE9PPULfttCuLqs39gYRoWCJWWeXnh5NGIhdhZtuYfrqqPLGxTBgXPkK0cPZhuSj2B2j25VUJZ1DZV92eaDEpvfFRPqS6DoJ3uOyFV8Luir3iZRvyfF6OWaZzs6v5mY0eGheAn0abUzC3qp80q8zFvjBGcDOcadkt3upql1SQnPZ9qaZyynrMNFcRNqqITUlwklbjZyGRQbAwoTDBUA3E4EWtdjPwnviM7idcW4W3RjlOpA7ddyJqWq4urN74HUyE9n1afPYc115gv/OuiYMUT8RSmJ61qqX3szY98Sbgadmjv+ZnWwHVtVe3zmuLWksixPHqY0nKhHxH8GXz5UZdLEs6beQAXYOYHGhZSvKljdTFDk2n3rNWN947ixvCp61OPblsq5JcUY1NWeelOxJmo6FGPB52JotS9deQ/AHwAqrNMOCIvavXvdotdrvJsR2u7pCdQ/C1CTVEdrhOt+Y+ydW3OlfOnsYDTGJAaFB+IqgIFiJuuDx3hvfksaoRcUwPH6J5LvesN10nLmAb/H28K5jJ00MUcuJtufFf2mhIHXj5XS3PRAW65GgTmZOIyehPWqrakm2VyvxAHMfKc6Y/JU0gXet06XpQLD3G8s9S+hVF0NxmewTkl9C2lxqFI+FVvz309xUysGKDE2+3QEI+J8E7EG0ry9Hg0Zxakk7lYITasBNvMWYm0PxUQNnUdXEOYQXGtb5AutSNryca/+pZaQXd97hrcKDnZxK4VqlGKibxEFbhek811U/t8sbVpLarEYPlqjV1s1Hh3/i0Be0rCi+Tqwp4jIutC3E0nn/FCCHYhiJk1tqtEoM2NtoDH6RRZomIje8EdpSxJYoUT/g1GMjCBwKtifgaj3e/NUp1m0mm4vq0WdwYFKdFhwrVzQl36Mi1NTsG3/ZHjwFC4X5A7/TAfD9rFJAZHGdKWmwfRKLgIXPZxKlH40JBzS7i1YVgXaPZe86vkuQ44dBirfI8668+8IFBlBy5nuj4c4B4dctGf6ArSumqcri8aRJHFU4SClAYYt3wwO6/glvTNco+NQOwmfDjzgtLDKPz5sK1qtPEaBBawMwRvSzxh5U+XGNfxcx1OnzJiw0oiMsQE9n/Hn03rlcxgMhSJes0NCtS8SaJ1/K6DbKPyOMn7uwyGzirSleR2cNasTnIQFKWxaPLXURE9T8Kym2CeeFlbPvx58JbXaAjnolScj7TvdrNJ310hujtb20q7VmPsDxLcSQl8D1OpFpgdTepsZ6ll5lTktCAnZrAcENfT+lDY7bkpw/X7q4qGB0pQ+Aru67azi9a+cJn5ThhTkU8iMb97yuAOMW4FiV8bR9PLg03Vz72zGrYJBDej+vsuMu4Cb5px7ZfAhPZq2Zek5XJzDRXKGLz84ZKa8San5CyaHxzPYczyxZFqDilYIyBBA/0gDMz6fmA6RzNnukFw5A5Vo2CQape5Lr3PBtRfDPMHzemncbrcQOLFepB8bn34TgSHElxKjDtn6s3K8LzfrVVMn512wpV4oBvIhi3bEu7yrmTDoYnBIoqz11FlkP2G/+jCK25b0HXubnxH3jOLK/RYP8eTCEr4PwemxwsoYsfm25RD+McFi0sfA5ARc1iaIe4B2Ger3D+fkO/+n+PFy/IbP343IL8m2w3ld8Vk7YyiPCEXwSPdWbP2RQ4FgZfeKbA4NA87gfQida3g/pSRbEJGPYmZK6UowApXSPGOt6mUHxFthfbrQCHr5IwflQykP4KzJcV9yP73WNcbeqWeT/03eU25zPZlSY7boIV2lPjV7U2s9Zi5RzB9u7L9hv/OFWQqHT5oMYAjuE7OSgQ2FZ9KpyFjoPX2CWPeasccsXQ0S+eGAEbA2vsaTf1vWsqPYtKcmfmgRZUIbjQKaMsKOhYY5P35MynNveub6llQ8YHfflDnNOSsSqIIvBUDmRdYqHV29/BuQD2wUa8glBtg6EzAgzgojfUwRgynjy2vj5+3eyxJEl2mCOoZt4OxrOiWx3K2IcAbBra3W5Y5kkoA75aU/1dRIPdO2cZga+SrI3i0UgNOphLTp51LrvQA2Uib+Ovgn9f8N4/cOXKvKao4JwMutU4zeoRicfCZC/o1A8nD+3rnBvIwQkiqn+bpBk5l60nCp627kwX2JsKotRjt9SI8vIr1+iQG5UQo8/st4CkEZEC46PX3HIQDYuKqqkfMcYOPXbfs47vKmwuCGOARkbQq8bznQQYuOa33TjyILpw0TqkoKV0Mq/0ehMjqahBiYqQbOoC66NHNZG1ASQFkNyDklzi5YvBir4Q12Ic+EDn8SkHOlIK/glQ9U9kdnuOh+sGw0rB9DpmHjMPBPRxZZ6V0jPJE0HcTkmjUiPPjiVTTaMvGUkrtkWwmdgANd5zqiQkmcQ8x8pzcfNhKcRgAnA6w8g3J1vrKwkFmBVRUWSzeiYQlla/6CVLnh0PNiGr/KNrjGzsQ5+lxlKJ8nzyVaeZNPPPSx1nd9KgDX1qjYXiqqxbITO5fYsEFkbNMrwitvnaQrjtRLMahK8wb1CxjVX3//sp4eYKLJ8CAUHNT85iL5F6MmXk1jCdbbPGN4BNfVX83HREzsRF3DJtDm8BpPJDY2PONRlB2dPx1RwBLoIkeE/zpDFVpkut2Kg+/PxHxeom5I0I9/ccCUuNxYxr26yi+DByB9j1ex+ZyGla/D+c08F5y9zP7ma9PUAPhfVp8iP6WyH8H+FsksvCb8x5DiCV1OZIyOVZ7N2mlibgZ2f7hcH/WMFy+IelH8PuaVm0HFo3KcHIl/1x1/lP8yPzPCZA/RsQLopTjaXnK0sL6g7nwYgotUT0QtZtnMVBGPB0wca9SYhqYApIbuHbR9JpWzNwmQ/4YVS4UGXB5aZw1n/YgancZd6eq2SOatWPJpy2ADl11eYFQnpSzvB5q/iszycAkV2a/j2OPIvl6+ZC/RgjbsyxrJ+alt/gm8NFcF6PFUMQ4GRg96ni60heq1JE7WXvPerNFHUjp+xqNPCTT9lov46b+qeTdG6Eo1GERYPFvP4bFPbNnxyydI4Q31vCiDopwO1gPt/qT2n75ayA4oIEtpnrGSWbqjON7JKcFZHnHOWW95RtipiNMeoo0gaVBO9WrTkXMp0mztEjhEOtlHseudVP/Y226umQeM9iHOfG8dZbpPT2vgEkyEXjFEuAMUUAt3xr3tKbaRM4ftiWhZmLWflq5kDxYNJFSGoh3BkFDkZ3BNl3dYUIHYhVfJcGSiXx1KwWRKhjE9ZMueSO4dcx3w41M2pXIbcLvypYKUhtPeotqv5ZYk3vEl6I629LVwq+PdzM16UOWqrGyYMhev8oWiAu9VgyzeWbHDWhN8OT6Kj+hM4EpP+/gFSEsi8DujsAhwA+YRxvvVzLiKhYn8k/J5nS5QNU20kKxP7DvzCQIqJcV6mjnamRn68jCKgLM8Znytfrsu3OE46WU2IoCiUlbGKm7HnKnQ1tjmKiBqQiRskuzfWyP34LhYyjIq2XigyZ8NhUDH0vVGHJbQWVixA61WvmzyZwHh9f+TBGnb0GrHG8T9rwWl7jMfN91yBzuMb7G3y+szGu7c53bUWUSWlf/1mGudQtBvMy/PnHz5t0YAfeYPIaxW8vw6ihI7m8J/AEX/3SWsK7cQ7clZFlQmkEIkTQ6XqawwM6IPvqTzTeA0V2Xq53V5jNQekj6TKz2fv360STMP60cUokduc6R+a1iwWFfCIiU6B3E06xUA/2CD48htU+UNfTb+b8UzkV3FCaplJMr4mSpmAxkr7kbJuh5YPAKf5YqlxyDToDTUOmSxPybBR4tPzOeQdC4f/FLIXvWdqgrZ2ZBHzAEF6F7uc+Isv+0D6hW4lRERmkHR4NSM6bLJTteqZqIHOmo9A57sLDrZgI7I5exbcbQJmMGtFhfDITJmcXY+Str8I//7rvtdXT+MEhKVJN82MZp76HwdPMY0Ah2eu8rgkqW0RgS44oanqQqeJd3alN6qCoy2SyOnvJR2taKef7kV4p4fLxwLvcMhU13ltPCqTt8ZgZYn9nYaSV4eBUYKLMWKiCJpLyJqblHRhysFJ/USzrMs2Hx/krh+pRiIxe3k2JtUd8fQ4QvQ2MwL5Io6+dvc27kwy4q66/nUkPOCOGAuJg95MN+qlxR2Vgpa7p0LCq/kLhX/65YV11uWM9qfww42SfUeDyN3ZrlVakJ7Sc3h0/5FjGttJ8YgBqGuvlFgRBdO23y8RoXPJ6Vj7p5+4d8baefsE65QwMkFTIUio20aBiqJCf/jdY0G9xinIf0P/V2CAVlOspnoAP76aOkhiGmH406ivXCcSx0SZh3F0vtWz9r0B1P/bLGlIzVdS1g4B2Q18KRGc6njmr6t588hNN8KT4dQQs0/QW292AO+k3qZgaQfnBkIJcCyrDpiM+EHDaXhbPtDj0NX3JJq8M1MtpvEtKP8m0tZDo2pt6F7+HFW+DPdbbJAm6+JqwJbjDA4HMsLKDSotEqtAxC7RTVVHKJY3aw8VTS+ZqD2XQ5kXJ6c/S4DkVtIE3KbvLGgraBkJ0xvxJIrAXCIapYam8NpWFzbtJPUpvB/GqT//7lp+IhIAfuFNKBaH6xE++ehGC0g9XtIYpUH0qh/doUR3dfMnZ41zUaSErg9truwrS5OVv1ZJAFclbyKdmS4L4G3HN2BZsFuiamMeOR8ZEWyWxkwDn6qsHwiTqr7I/PeJe4hWeTHfq2GXLLeU+mWj71nLRsHNvfTCf38SzWf9ofPvUuCjLdSzi0kqhs7jBeRLkrUUYPo/8xcoKzgOmGMXvqZXvTYKe2FMZP94PjgEGU0VTwTfHR6tjIblKhRrFbjOPiM3UcrdwyDFlObA95FneH/+0NFfZDEbNpo5EEN3pXv8MJdEffqokl3MwnW71pmEKjTStYw1uciplPPnnifh7eYlE44mrSDl/r5HhBJ4LigVXO9+mhw6h7GkBEg3RFwzDG7i4D7uBT22FbVCPwBYZCE0hOf4Tj4MKdvyQ2rdAq9zQPja62r5sRxFxoltxu1ZNjKv6T2JsLReeEbDXWwiHmhyIHXcUZtVX0l6CXE3pZDLmxaA94x1rDMS+bgcMmJid7Zt5uDpOApAP0DR3Al/+FsZ+NQ7O50Q+GFcCkjJ+a18qjMVQMP/VRR/n2bB1MiNRqT2xGAlWf5iymWVY1OrbdtWmHPLj10Aa/DODKF9puGHR6AlqA0V0f+Y1MIszI93PT+5X/snjgI0Am+jwKa/OKqGChYBfJ/g/jkgtcNctdAxIjdZ9V5aOUErbyOXIfGH6V0BoJldVfPCr+jvpw4PJuR14iWi5Ife0obWalofkOxjzbzpaXB6roINnU3EISygOWS8B0ZPSzIwc8Ue4bSArPenivVfoX3FpCMj7WaXZ7neeWZRywAzd1tyWye5WLDc09F+uA6fubf1o0MF6jEOzd1WsxPce+HzLxbxrKSXqhBdFTNyjSkj3ijX5lPnVWJ+Ty4KWuRmeOe0sS6gk0nMKLxEPvcK3LpXRjMOsrVDWqrQuju7Jk26h3k6M8PJtQcM3uDt2kN8z601dFCPvh87jlK/YpcRrboUCEVJJ++ncZpz7NzpmjY55a6/gSUd5lvOePkIQOzJ0mdfmJ6wjNfU0cZvzM9mqGIOnyKLeh5/E8eNJLIwWPUn2iClYOP/HHYSGtrkWHzNEvL51QWkLZU1O9GBt0cAw2KYgWmqvcdg/AXEnxNcpj5nQ7/CCNk6lF3eaMaa1OZ60IYsoQj+M9HMZk3Z3DAtHsAdBKaOlK/h0TDO4cGmen+GekQL3nX4XRuz0nh+qnaNmNFYXbW3tYmNR5Ulu/IRSA0HN41rjmoksgZ6qAO0DYxHXwXgZcM6dU7cO3EU+RgX3f/Lab1QinLGxOrM+g+FchyYJk+fT7bmbRY1g4BVKW7tUbv6rzr5ERsD8rXkELF+kicPRptoGgfVFzqcutpiShlEymElN+wW8V37Poi20+dC2w5DCw+xpY01/Dvq5wMXaElRinkk3kgV6tIOd/RIADAAN5cAL2gVCO7B9i0af7b5Qke7KX1MMBI3LTTDkA3ArfFHqAeoTFckuzKxn9OtNaNv2hXo8yf/n8PoK6e88HIaYtK21KxHN0hihttQD9jSlSWYbLg5sATeAJh5AiGM5992/wRtGzV8CcCCC3N7fecFTcgLC9xVQSwfrYaGky6TjjugEMTFwtF+YUJEFvgA+9Z37Bq+nySpbwiitLdG7Dn/Dv3uypgPJDmlHdS8f1LJX3Ml1nCAo6Kzo+EUagHGtH3tMtSasqZgeNaOVaAeGiDZDG4Nmpet7f954QPi2aSbk5cKPJPqjxNWfwCooKCesihNzT+r2TRDTx+j4szgqjKLq0VPrTM4za1EXfGB4OZglm6iF4/PvY+lhg/UYnM4ri5D2i80JgwuNuE0wvliw2ZcRQjB95/OBI0zyjKiQxAeC67dM5k90Gk8bcbWBXqbqBmG39raJv6ljHBqGtw4X9otOvT46peijc5pxKF+4GVkxqyBG7YPfW5uDToYhfXuzgN8UhAuKyY/ZSwe0R8Y6/j7q1Tz9BY87HcmHog9gRKd6m1/739vvOwctgJ2ayB1SklZXeqxWUb6SVa1y84p4s/Dc43v1Ldjz35miAmeyp/iHYZFF+m2u2y3gxeS4nY/gKNUv7s5wv/PPenrVIuX5PmaU7gyqoMTsY/o1DG72OlQzpV+GwlQSsRfkrzA6Cd98q8HaRXV35QAUPgT1xue6Ybaq0/L3M+KFlQ7GP5qzMrhPxkEx0IZLD5AJMfrVOf/B5uyP6LUMEaYl77mTOLtBY6IjDUcpCwmIdfQac68ee2hX9VmA5TYzCYElxsCDRo+RPTo6zLUsejo6wUFQrjvyvy2MEmSLO0NX2bwDpNv4sFGoS8VDtCXv11p8lCXzg3ra4ipWqh0Q6zyQKRaMbaszhjeG4Ij55ieOnB9OesQMQTp4EdXN2UbQicN5+on+ia1nMyf3NoVPAj2D/F0PNxnFqGh/28M0YXnHErZbw4DMMQJZSNdAHuaC6D2quEjLFsor8v5kf2x8aMNe9SiwxubayY63fiPhwBzpxjKhvlIzsMrLJLpZ3FjpTe9mojEXlHtLOl6wNNF356ISZvXpGy1mARqyzQOYu2BpeDzsOgyr4P4IwtI434YkPq7Z8N+nkXA3OFQEn6JSglAWGG6TSsbwrlLYryBO5rO5bgwyiuRZpFGYGfTCTDdvnp6wylzQ+4WKdGGmddifcwoKZay5+gv0MuRseCmYu4sPJh/JzxXvsI88XyhJ4WBzMOCaiejV8a3wNzgIOcIfyWoS+/KHTtRSqnqyu/YsbZ1Joy4ha01HCiR9U/Z0wkCX7fTqw4TVvFqNroPUUEcdmCq/LM5TC2IaDGEzQWYGhJ8D1drZkxYrBuAQPUrxiW8X0oeAPkNhM0PSZ3ImqYO+5U5vdQR9SFux15/k4YWySe9tZuXKT0v8+Sr1z/NdmFlJHuRKAbPfP1h7pqmZHw0HRPjPR3v/uivHtBse9JVAwY4rrphKbA8MXMRlV7ilFnSsJKgwYqABF+C+xq4Hrp7bIo6L8sn8iPdyVDiz0DA87+iQhDNGE+/jGGBEsLXlpdsiY9/Nh/2nWmVL869Yi0MeWbENZ/7SqjknqXLXG7IowFf6qw4Xrgb9k6N4myGhPbIqXdmmSmsJOSohz4qfojjK5zgl1sH/qSbm6JPPT1mdwqZff61tH0jZ62lbf9wqBbJ6kWaA+T9wKhPcbPSb92vXXJcFSPXHfZ06Ws5sdPA1ufwr3jd4MYgM3l/o3Mw2ZGtIo49YdPDXnhWTM7E4iq0GPLJU875AkX3jFgl1E56kz68gnUEyDTBw/0RAi8zAOT1nByCrOryX4TiFWIgPTWe+e5G3pC4WBu9OQUyJ32zLDdzJPl6kRAGyPMwfJKqxqpXDBpeHaLnL9wKRi3Y8htDx3yIk6Qg2nJ4Xfwcxo3NbZRP4pCQZSLMaAgcDIr6W8xq3c50bvkjwqofWx71S/fTVkQN/B3MA+WpqDScH7GN39DWDSvGHcwfrrDS2PF6cSTw34mTJ2Bw09WdudL8a+H/6TNwSPtMN3u9JW7WYJv7jOREE8s6G90AxbOJYa4znEkB80lD3MyYD9tum0SLblbs//+cGSDPa8rC/q3qc9abZOrQjpD0Sv4A3J0SEUxnyxw/40L+uRjuJ6f8l6J3PZUQE74gBLuLOs9rdNPISR800KsaG32WMG/JLFYEjCA00S7BfcaPlOXJH4Zq6YN+B9OA22AUDZApcFghgm6cfgasKp5uapY4GExEceVhydTBmgv7KWBEWg4FZ00h1tk5UZAvGWR2ElYEXXv0KzioEbUEz2X60OzwC7E3ulr8QyaZM9wu6IE6v838Jid67NJQeNXFPQ54suwDjukmDqZYbiuntd8m5loHrUquNP76ft6I2OI5iygZ9QlGLxL0hEZGTFsH9Yr6Kk4Y4oaa+l0LjroVtixbRACq0aMbcl1T7RpQkSWrffGVg6CRiDLdbD7LcAlC479l9hCKzMUPjE5V6r2hzWtLH2CM1WpBHrC9VzoPJO5x5Yc7teBF1GsJNTW9xEFEMWhGWCDesipwdEuUKR+7ZI3eiQMCVbzQUixycztvtUOPcTYMAJnxYatd2xQxTSnCElAnad8YCwsuQFLjt+w3VbYvH59lRYnIyMaLf3qTwB2jbqjwrbgqrgVd87S3FTiZxv5j4dp7rW0vugS7tkh969XCdJefhXlvUqI7TuHY0IcBzYcCeB6T92vh4/KCZ+KUrbliuP77iG1Lb2wswjBBM4V54Ok8xihD/FcEbsMrikLp0rcK9sG/RbxxVDUUPgLX/o4W1s1iqYqns9FFMLcZkcL8c7a/lDgWkBhu2td1nlGXRASUrV57WupCt3nqM4AhoEyy930WOEoILVuTPr6cvjzEE9Sg5KEZpCDe06crLFBU19fx5nIHBahRUZh83WFIAP+pQ1etNQB+GHxwImtDj16AeCO6saytFCos9191Sn/g6Mw3wcWO0bn3ftOUrOyIIOTFFVfVpsyXjk0FQs41KNJTkNBe4LgBwvZqN2VaIvw0aiIz/6A/SqsL3wA0/QiYCLmhbAk/6NDFyAaSXnsWYagcTAjV2Ux2wN+IB4Lnf+eiHOkV91av9lVx4tdpHX0oZlTr46EJphJ8xbg1YJWdpE6TVuGH166oVD1yqZk5LGPcxvUz1QAfwtaDUeGEv6EudOkZ7wMR2lwB5kQ5IkQaeITQ9Gc9hw+VkO+GdyIyDe6yrwqFdiEwB8CQEwukVIl4qM877bId1IXP0BuO6ELkob1Ywb19+EdToeRz3N16xPdLZwRYiiQfFh2Oqblaag+vm2aj7cvGmGt2H7OgH2eOmmSo+wuMDCZrSe+5QzZ5CNvWvH0X6N2byxV8fdLCEFZRnZyvcN461z2gYDnN+nYlp15IRIy39uP3wmnlAdPdW/vZlkr1/kIVsae+BvebBUr1f+/0xo3HfdQqFFlOVqXIBjHtTG7ZTY8Hr3c/Gjahrbb777OnhQ4ABT9Az8NaZJb2P6UikCIcjxOIL38MitXFxvYcEvyPZrT0ZZDDlb/syeC9ioa9rFshm99qfxeNxrJfZwoVoinO/5eMD2GJqpmibhEkDTSYHvfKhxnqV3maPSw7M2CkbQCVsIjjtjCikQnJ9/fnEJxNM6Ixe7mU+AU4oj6+6D9E3gzjOkt3FJnlNL+ey+ZeubqS/EaKNDVcDF9ixSRGdt6Xv3+OMwliDHqhEyyuOVpzGUi7FXTRFd9UL5u7k6CiJn4Q1kDpU48FqEQfXnqf/ehELRriLAhmvhSirPWQqCLFcTwSc7xpseDU9QzQC/ORlaT54TwvHBhGw+0VnQqpO8ftNdDarhKwHwqV9B5GuprB40YCiNumXhgWi10lFyZJqUIOe2FgyoBLjnlBDtz+YJ7513oWK71PPbdFHSI+tiIrB1PcGwlUqaGGtjQk0ZIi7NFJ/IqwRyy5S0HYsp1p3oDi9M6ROS5fcMoPE360Gc8K8+p6Y5JCRvFacVLy6/XrK9ZJ/p7wvAI4s9UwGAo7GbflxEvb4qdIwqqur6LxEEQuFxf4FXlUVVVrlbprV46Z8kz8IEJ3VulXk24i7pgzCS37QE3/zBwEyJtHur+00vQZlg0jGQBGxOIusPBqms6KR9+WMLRwpo2ejTNYHe+qbhr7M/VOGHAio+WWqHaiApD+WWxijevJESqJFavZcPh/zK+ZkM30gAQUm7OKvRfzx+RyNilQSMMP/GUAljmKvufGYxUHYqEGTTTW6KFRAYQFzE8A2P/tKV05CYj50HRKqXrix/v/zacwNjZTs6xoe51AeBU/0TpaWmJhbxCyNxjNjEgxnZh0fyP6p9788T+wceV707XzW8VeCLVk/Pk34LOQMiCSXY4EumMBjBoKzypaQImZqp9c9HwRKULpNTVzsHMC4yqklRqgJseRsKLEXsI4LsF7UD1XFkHGCusLJkwKfEttJ1VKLs2vIRZWDRKmIwq+Wbs6TM7L8Ai7dOfX+Xx1lvRmLzPDTdr9oj01lDJBJYWs3IB3eho5FysSDH1PYdXHXUL1EoZdat7pGM/eLZLN/v0Qlt6zm94xbyDlP7j6vDX5GvWaz2sZwAiuOQkf5lcEODEv6BhP2vhJT4Z5JOJVsw4xHli6bWKX4/3J9SlbAAJ1IAOw9Kd6B3abJ923C85tz+C/wQq4zIU++k8H4cKb6Sr+VXbXKUOCBNY0opuVpYFPnmBP7ygOOlki/G2SDdsnFzSuN4vusSrGghvc1zF7lbBUK4aGj5SjiYrkP3XBD60/SWSx2dwME3WLKMNeNTTAo2trMo5WT5HrvkQ3uHlNbrcJsGdG2D8oR4oSb7un8Lem3Lc81fZiQF70d07GMvd48a0YmBhsmqlabhepogrq76PW/U2t4i7mnUuTwV+Z9F0wAjGTOQlVi8pAHXDnfp2n4mAgIgQcgfCjK0g3dRWaFNDMhCeFuvPT7b6Q82l30uyZjS4Hpi7AXdjMrQPcQbDX0VJPfeiQnIn4VXVighD3eK5PkkGaaSto9CT2oQaD/qhXjSpuGiFe58rc5IgYjHlF9htEG9jkRXK+bZF06FV7MVPLkMSh/eVdNztrY7G4ExIlLjXYU9qO4e9RZNXF23RH8S9PqKnfeq7wwRFzXxOKNv3hMrRS+/cNlX31oaoWQPvT4cCDtVqCQ8aOgfUllxe3WlbtC4q8sbP5h2B8jGk0LrSTm1BJpu1wLwWtGbz24PwXfVBhpHljiQKSbucBhDmiBjQDDkTVYU9ga3u7FVR/sr4PmLMb+0dJbgyeWSYNP6sNQx8LsBekgQGLgwqWcuACIBv1/TKR4hGlncqekQ+E5yC4HU3gns8mFJN+1/eEvcdX1ViPImaPQJNHvy6p7oh9wEK5uF3jEnvN4+uhy1QVnThuEy7Vwkrqd3SFf2f2K8BRxlAeBJZwo8YKjtq0/aJTPzKdREA3NqyntYLIrz82fbOoxviu+xjcFT6gOkkL3JfZwMTWLzpyZRdcevo6Szm5pS8L9+V+NYFmX9I3zkDGZSIwF8rE0xmgjn/6+R+3Jue6pkTdtdxlt6xUdoZvvMXmYhRvoG1ikYnlXnq09TfHjJxIjvFIWqt2U/89aVQ1ojp2xnUh67+S1rYjXvKmvuyrKlvii8dAM6jQahG1deH0VGOXfQI/vOID4CpPQ1NVkKNJmevf0l7ir+Adn1oXN6ZxN0zxXRuRzwYxySO1vrtl/iGgpBGc9xSTWwHOP8DBXvTSumj0Z6dCQN5an9d2feyJy/sH0UmgPuEuv9S0+5mJ3ALOizLy2d/cdZFZbPveoim4GgU3KezSr4L9F/vxbV/WIxqwAJdr+ly5YBGoiherCh13SvygHvzowzw8AWF7X6xVSejTfgD2ZZbX9RSjSvZRG1yBDNoABxO7wVnwZgwNi0FVMrAm649DEocGLZ7ElmqSSG5LOrvpffHSmHbwamVhB3n3DpDgYvHFXdvzMvKucV+vshhwf2sUz7bj09wmyPo8o68nvnT/9P4QyfaJCeVXcVAl8RMKkAZpY/2R1rVPOBooMsZWaJ2DD2mZwi4RKPT50RCLKuxDTHsQo+A2OeaUy7lVwazdCQ0XjIHrGc7c0Vtesd1R60/xMO32DxfCW1Z3KJ5/Zvm8iOPfXoGJnMLYQfiBRNltDHfDI8LBFsjbrvihXNQ9dBM7vbKTwTD6xqRF+s3GC9iLximHz9HNC3c2ibeVYI4lGQQp4HhUTleEOfU6YnrvfDTzm3n6UkktI83vKpz7rR4PhXd2KeKQ5fcPnxxFREeXW9+BtCu8s00T0TvkyAUvT1aH9ndKxOJ73RSrRnL9vOteqaq6d5x/J+uPs+zt4ba3SWqRxCdh5Um6Sg4rx2opaOiB1JU15jHBDqJEET5WUStz9IUNEgo0E72YRBpM7zzifB5Xm8RX8QHyzkUhnJjG++YO/wlh940WQHH00jAvFl5GhmzQvSqCP8qzZrAsWKy3VCR4WepHaPVzCQW4YJ1k4mzYK/ac4WUW0JzqccEq1aXrJGfCpIroECbbChkSZ09eIoDeXcf+WoOMVJSCaSciwJy0Y5N5kEg9If/SJsev2WKVhsBwceA1wgGOoAC/WJmY9plewAp/F5TOvwdsqtTSKofNIab+wOKRONYVjavQ5yvnp+8X0NT6memXeqG7Uyo6sryFQR4XTgyck5mkiuYkJQ+vEXlZ4m55IMTUTUkUhI1n0JL49XdLQ8T34vTtsbOkiZBVVjNNLyrS8kY2nSCZryJDHJ+nGkPH6oJRoTBPEkgDaHZFbfkAIwUITZGe4P2lLRSq4V6nVc4VgGcNeOYjDSRlkFAn1XeiL5t8kjsmKcyU4/iOdN7MXOc/o68m2HPwsIjiLM/5mjKow3LRnGFVkPDxcc/EGc+jZomv4PD+W2+4pPdQAW7MShgS1AxpejAutdQJOA7sDWEhaI5Y05m01/cH4ygt4Hl+/GaP/nLFxeruTFh5XTL1vnlv2fMveRAIyKqyQeqBPrUji+YhnXUZJdqE3Uzj8YZtD0v846I//eCLSto5JcT1mIEcKpMfZG6bH+ZCiTk/LIa35Y+liJ9FXI8Iqgu5vgpSH4Oq7DHuXK6thVDCuOLS5RL3ACbaZsoeU9TBGiU+NTrrOag/kND7z4L6qmW/jfsZb0IZiX/imQYZ76UL0euLzbOYoyN/LJKkpuzUi8djMFn0QjvWolquv+1mcomQI7u0axEdfTY6rZmejtfBz5GlxjRaouiWzxxYj763afX0IQOzx9QHb3/tjaFG6DYqjyXHyYwGrIv76t9w3v/S5T5pfsG/RUR4i3sN/fSMAD4UUjCvn+5F4i6Bz4rtg275Z1nCgSg38MnUwPLGVYl/8eOG0q+z3ZM6p/weLmEHGvgJ6J7hbc5PmCuxSGLE7JJER87+a9nHhenaklO/6oO6hZ9duOtgQLGcqSakvJp9Oy9QBW29N8qYLwjwlypPpvY13nROoNp3Oamtuxmf/cuwLY/9slW0jytXjxNSbriHhXt7PnBTxA6EMzLyxc/e0WnZjoYbrIKhodWBZVrjtwFVp8mfTWscSivM3T7T/NwV/FndIy9b5wmgxRNCE5OrIXT9g+syUEPR5iXL1eD570I87CpM04oiKn+hvI8OHJEimS6dDuz72T2BHoLXC7l0n9cU9AXknclh0svozxNcKdgb9LGkX7u7ms2+Gbd2UriC2iz2diI8OvXKYF2RAcaY+KikTcvjf+bXp6uCFuA4tUd5wBBzjadf8+3dOHq7NGlR7iKF5diNxvfUnHOYCOmRBtdTom7CUYF8q/2GGQW6j5PCaTeEZdS2OE6aUMHhmxWwpJ+S7O4gBqH7YnY6xZL4vEqkdolVzafus4P+M6+ep18jbgn1+oJBPMDWVjCq051X33GPLA0kpavEo6njmZ+7TMK5fQ9HVQbr0Sh7HFGnM+feWT2BkPN1J/r6MXwVCcc9O2D4w6gWTsJ0Lcf7YyiVxi9mAfU8VAhnPvQueK9DWhMGiRHuMoHyHwulRQI8rBGmEKewUd8QskHoromt/iSOWgdrSxjUtPIvzTFXRC8lIPcSIwd5fJZ+L5ZiogI/vYGtyET6waOdrcOY53qzdD4VNqLpwdTMiMNZEzWtCQd30nCKfj07RJFfUWerWKWI1Sa6HSPTA8Y01zphPACLT4XQ/2dXvw87InKIrN2ajg4q/U7k+lhvElfFCms6G3k5BX9alwVl2EqU7h6PwN/Pd1/7IPRVpTt5+SlOAoc+quDNyb9Fz3B8EGz7Ye7Go06kaUarTXwNvpw4MRwYkgl120izCLHO8gBX3DLj5OryATwSB/3psUWOOM/ZIFUgSUg4x2nPgBv8SHcTwckZv1m738UjXbRfhLCCfU0w53YUL2zYfQoHH2ARpFo5voB8aF5s65OCPZPazyq5zi1ijpRfi2KwdygUashh6PWZzb6UCQE5jywxkJD57N/I9oHelSZytj7maT4fzeDOQvVkcGjvVP4aobDCutRqF4A6cgi54UZnhPRdInDakxgN3ehztoZLfPLDmN/1V96P3Rnd3i6zpGbdPBt9UgmwHzDznCYHYoqKYZNlpY1+Uu9taFJiMB3jIhYGQd/8rHMLSDJCltE4vKpqlyjRCMkpNSmqgFcFJ67FgzdeP8jNorSK89uuwv5/6SMk0rzz8rNWuyAi9H+zclTytMs3SMf3ggTdL7rMwadW9RVKNgZQG2yeeOZ8wAgA7MXcS17l4X7DJz/RlxPKY6DcSVft7QGeR0W7UWXL8wG2KuNEtqFqOuWRfIXs8Y3lBCI2abDBO7YIntIDz141TVs9VHPep9il4pt1FNZMUrLY1NHo6gaSrpZ5ladUMgaSRHM+yfipKNuzurJIz6XJRnpu2iIR+34BbUhsubvYqxYxTLaUfj3ljDNTAlJCHjz5miZOGBKL2fJGeTXL2k9LyETocbs/2cGuCqxhWsXKiMWIBUN8d9/yngx4j15Jc6L6up++SKEmPHuYRnPYmU6FtgkE+E0Ru0ia72NcJioAYz9r9kjxJFll+IR7oYRaIhDwhORp5VTR6exaByEnHWSusTK3fR+fENG5TLkhGMWcweGTMnNiXUDMlXZ4KzataEpSjqom9C9rByLK7EuoU0llhUDqGy5tVa43MNiXdip3c4oc0W7r3Y2dlih+sWVkjm/IrBbkZ+M2DbLpDmn+aMRco4+eK9G0tbDbrTB0Dow9dzYzSpY=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=q1dvvDZ44oMVQvgLMHT2KShp_uD60bVj_YtPqwLThfPLPIXz3mXb3Q9_AjUNTz8_z90yeyfV5mqFoc840&amp;t=635332215015596117" type="text/javascript"></script>


<script src="/js/dnncore.js" type="text/javascript"></script>
<script src="/js/dnn.modalpopup.js" type="text/javascript"></script><script type='text/javascript' src='/DesktopModules/Exionyte/Menu/1/Xeon/Scripts/menu.js'></script><script type='text/javascript' src='/DesktopModules/Exionyte/Slideshow/2/Xeon/Scripts/jquery.cycle.all.2.74.js'></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { $('.slideshow').cycle({cleartype: true, cleartypeNoBg: true, fx: 'scrollHorz',timeout: 6000,prev:   '#Prev', next:   '#Next'}); var a1 = document.getElementById('arrows'); a1.style.display = ''; var b1 = document.getElementById('bannerPane'); if (b1) b1.style.display = 'none'; });//]]>
</script>

<script src="/js/dnn.jquery.js" type="text/javascript"></script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3a509f92a1-e5fd-464f-a450-13846a6c973b%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="1SKp2hOggGq8RxVh56oUb/oFbIogU3jg2gUTrSFTAV2eHzrsl+eh+shcb/khn0g1JQm3jOg8KeZ2x0sGRBnXV6+84cisU5lRATVy6gekuNOpEqYNV25YEnJsCm54wXLqzA7SxW5WYhLdjjqUKa8oQoOaU3vfsFfdydXfNXlyFZ97yxYFrYp3w/PETXlMubFxKU19rXe1PNictRUcjdztyGt2zZeLUvY7WuGCeXDBy3YiRDhujeu1Z3ofOZc=" />
</div><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
<script src="http://cufon.shoqolate.com/js/cufon-yui.js?v=1.09i" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Futura.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Font.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/jquery.pngFix.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/general.js" type="text/javascript"></script>
<div style="display: none;" id="message">
    <div>
        <h4 style="text-align: center;">
            Sorry, This page dosen't support Internet Explorer 6, If You'd like to view all
            the contents properly please upgrade your browser to IE7, 8 or 9. <a href="http://www.microsoft.com/download/en/details.aspx?id=2">
                Click here to get the new version of Internet Explorer</a></h4>
    </div>
</div>
<!--flash section-->
<div id="flashPane" class="flashSection" style="display: none;">
    <div class="flashPadding">
    </div>
    <div id="website" class="flashContent">
        <p>
            In order to view this page you need Flash Player 10+ support!</p>
        <p>
           
        </p>
    </div>
</div>
<!--/flash section-->
<div class="skinLayout">
    <div class="skinWidth">
        <!--header-->
        <div class="skinHeader">
            <div class="skinMinWidth">
                <div class="logoLayout">
                    <div class="logoPosition">
                        
<div style="float: left; display: inline;">
    <a id="dnn_dnnLOGO_hypLogo" title="North Maharashtra University, Jalgaon" href="http://nmu.ac.in/en-us/home.aspx"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/0/logo.png" alt="North Maharashtra University, Jalgaon" style="border-width:0px;" /></a>
</div>


                         <div id="dnn_logoPane" class="DNNEmptyPane">
                        </div>
                    </div>
                </div>
                <div class="menuLayout">
                    <div id='menu'><ul class='menu'>
<li class='current'><a class='parent' href='http://nmu.ac.in/en-us/home.aspx'><span>Home</span></a></li>
<li><a class='parent' href='http://nmu.ac.in#'><span>About Us</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity.aspx'><span>About University</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/misson.aspx'><span>Misson</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/universitysong.aspx'><span>University Song</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/informationatglance.aspx'><span>Information at Glance</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/achievements.aspx'><span>Achievements</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/vicechancellorslist.aspx'><span>Vice Chancellors List</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/annualreport.aspx'><span>Annual Report</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies.aspx'><span>Governing Bodies</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/managementcouncil.aspx'><span>Management Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/academiccouncil.aspx'><span>Academic Council</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/senate.aspx'><span>Senate</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/standingcommitteemembers.aspx'><span>Standing Committee Members</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/governingbodies/deansoffaculties.aspx'><span>Deans of Faculties</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>University Campus</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/aboutcampus.aspx'><span>About Campus</span></a></li>
<li class='child'><a href='http://nmu.ac.in/kamc'><span>Khandesh Archives and Museum Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/centralschool.aspx'><span>Central School</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/campusmap.aspx'><span>Campus Map</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/howtoreach.aspx'><span>How to Reach?</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/extensionactivities.aspx'><span>Extension Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/researchfacilities.aspx'><span>Research Facilities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/mous.aspx'><span>MoUs</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/home.aspx'><span>Satellite Centers</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/eklavyatrainingcentrenandurbar.aspx'><span>Eklavya Training Centre, Nandurbar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/rcp'><span>Pratap P.G. Research Centre of Philosophy</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols/en-us/research/labtoland.aspx'><span>Pratap Shashvat Adhunik Sheti Tatvdyan Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/mahatmagandhiphilosophycentredhule.aspx'><span>Mahatma Gandhi Philosophy Centre,Dhule</span></a></li>
</ul></div>
</li>
<li class='child'><a href='/LinkClick.aspx?fileticket=oqDHHmONXsE%3d&tabid=3881&language=en-US'><span>RGS and TC</span></a>
<div><ul>
<li class='child'><a href='http://apps.nmu.ac.in/circulars/Bcud/29-09-2015%20Submission%20of%20Pre-proposals%20under%20the%20scheme%20assistance%20for%20S%20and%20T%20applications%20through%20university%20system%20of%20RGS%20and%20TC,%20Govt%20of%20Maharashtra.pdf'><span>RGS and TC Schemes</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Information</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Academics</span></a>
<div><ul>
<li class='child'><a><span>Schools</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/soes'><span>School of Environmental & Earth Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols'><span>School of Life Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/socs'><span>School of Chemical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sops'><span>School of Physical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docs'><span>School of Computer Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/domaths/'><span>School of Mathematical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/udct'><span>University Institute of Chemical Technology</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docll'><span>School of Languages Studies &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doe'><span>School of Education</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doms'><span>School of Management Studies</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soss'><span>School of Social Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soah'><span>School of Arts and Humanities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/schools/schoolofthoughts.aspx'><span>School of Thoughts</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/dat'><span>Buddhist Study and Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/smcrc'><span>Chhatrapa Shivaji Maharaj Chair & Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/mpsrc/'><span>Mahatama Phule Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/svsrc'><span>Swami Vivekananda Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/grc'><span>Mahatma Gandhi Study &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sgsk'><span>Sane Guruji Sanskar Kendra</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/nmuccc.aspx'><span>NMU-CCC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/phd.aspx'><span>Ph.D.</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation.aspx'><span>Adult Education</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/eventsandactivities.aspx'><span>Events and Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/adulteducation/coursesoffered.aspx'><span>Courses Offered</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/tbc'><span>BACECC</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=xCiw3cuw70w%3d&tabid=3419&language=en-US'><span>Community College</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=8DmFd7nqvqo%3d&tabid=3801&language=en-US'><span>Tribal Academy Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/committees.aspx'><span>Committees</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Administration</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/registraroffice.aspx'><span>Registrar Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/administration.aspx'><span>Administration</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/lawrtisection.aspx'><span>Law/RTI Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/faooffice.aspx'><span>FAO Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/examinationcenter.aspx'><span>Examination Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/eligibilitysection.aspx'><span>Eligibility Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/sportssection.aspx'><span>Sports Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/scstcell.aspx'><span>SC|ST Cell</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/constructionsection.aspx'><span>Construction Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/prosection.aspx'><span>PRO Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/approvalsection.aspx'><span>Approval Section</span></a>
<div><ul>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Online Approval</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/computercenterexam.aspx'><span>Computer Center (Exam)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/universityofficers.aspx'><span>University Officers</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=ZprmAC3RMhY%3d&tabid=1350&language=en-US'><span>Phone Directory</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>BCUD</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/aboutbcud.aspx'><span>About BCUD</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/developmentsection.aspx'><span>Development Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/affiliation.aspx'><span>Affiliation</span></a>
<div><ul>
<li class='child'><a href='http://affiliation.oaasisnmu.org/'><span>Online Affiliation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/affiliation/licreports.aspx'><span>L.I.C. Reports</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/research.aspx'><span>Research</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/iqac.aspx'><span>IQAC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/avishkar.aspx'><span>Avishkar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/bcud/bsrschemeprogrammee.aspx'><span>BSR Scheme/Programmee</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Student Corner</span></a>
<div><ul>
<li class='child'><a><span>Academics</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/admission.aspx'><span>Admission</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/syllabi.aspx'><span>Syllabi</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/questionbank.aspx'><span>Question Bank</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/academiccalendar.aspx'><span>Academic Calendar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/papercodelist.aspx'><span>Paper Code List</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac'><span>e-Suvidha</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://exam.nmu.ac.in/'><span>Examination</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlineresultscollege.aspx'><span>Online Results (College)</span></a></li>
<li class='child'><a href='http://182.56.2.247:81/nmu'><span>Online Result (Student)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/convocation.aspx'><span>Convocation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/resultsofverificationredressal.aspx'><span>Results of Verification|Redressal</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examtimetable.aspx'><span>Exam Time Table</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examreports.aspx'><span>Exam Reports</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlinedegreeverification.aspx'><span>Online Degree Verification</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac/QPCourseSelection.aspx?ID=660'><span>Previous Exam Question Paper</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examination.aspx'><span>Examination</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Facilities</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/financialassistanceschemes.aspx'><span>Financial Assistance Schemes</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails.aspx'><span>Hostel Details</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/hostelday.aspx'><span>Hostel Day</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/boyshostel.aspx'><span>Boy's Hostel</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/girlshostel.aspx'><span>Girl's Hostel</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Services</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Download  Formats</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/examinationsforms.aspx'><span>Examinations Forms</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Formats For Colleges</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforemployee.aspx'><span>Formats for Employee</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formsforstudents.aspx'><span>Forms For Students</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Statistical Format</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline.aspx'><span>Student Helpline</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline/studentfacilitationcentre.aspx'><span>Student Facilitation Centre</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studentwelfare.aspx'><span>Student Welfare</span></a></li>
<li class='child'><a><span>E-Resources</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/eresources/ejournals.aspx'><span>E-Journals</span></a></li>
<li class='child'><a href='http://nptel.iitm.ac.in/'><span>NPTEL IIT Lectures</span></a></li>
<li class='child'><a href='http://apps.webofknowledge.com/'><span>Web of Science</span></a></li>
<li class='child'><a href='http://www.sakshat.ac.in/'><span>NME-ICT</span></a></li>
<li class='child'><a href='http://jgateplus.com/search/'><span>JGate</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Useful Links</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/events.aspx'><span>Events</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/tenders.aspx'><span>Tenders</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/rti.aspx'><span>RTI</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/circulars.aspx'><span>Circulars</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pressroom.aspx'><span>Press Room</span></a></li>
<li class='child'><a><span>Job Openings</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/university.aspx'><span>University</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/collegesinstitues.aspx'><span>Colleges/Institues</span></a></li>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Apply Online</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/sciencepark.aspx'><span>Science Park</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=pDjshMkKzgA%3d&tabid=1088&language=en-US'><span>Citizen Charter</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/publications.aspx'><span>Publications</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pbasproformaandapidetails.aspx'><span>PBAS Proforma and API Details</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/otherlinks.aspx'><span>Other Links</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Us</span></a></li>
</ul></div>
                </div>
            </div>
        </div>
        <!--/header-->
        <!--banner-->
        <div class="lightBlueCenterRepeat">
            <div class="lightBlueLine topLine" style="z-index: 101; position: absolute; top: 0px;">
            </div>
            <div class="lightBlueLine bottomLine" style="z-index: 101; position: absolute; bottom: 0px;">
            </div>
            <div id="bannerLeft" class="lightBlueBannerLeft">
            </div>
            <div id="bannerRight" class="lightBlueBannerRight">
            </div>
            <div id="bannerCenter" class="lightBlueBannerCenter">
                <div class='slideshow'><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage1.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage2.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage3.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage4.jpg' alt=''/></a></div></div>

            </div>
            <div id="bannerPane" class="banner" style="display: none;">
                <div class="skinMinWidth">
                    <div class="contentPadding bannerPadding">
                        <div class="top-cols">
                            <div id="dnn_Banner_Pane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Banner_PaneLeft" class="colHalf left DNNEmptyPane">
                            </div>
                            <div id="dnn_Banner_PaneRight" class="colHalf right DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/banner-->
        <!--banner arrows-->
        <div class="skinMinWidth">
            <div id="arrows" class="arrowPositioning" style="display: none;">
                <div align="center" class="arrowLayout">
                    <div class="lightBlueArrowBGLeft">
                    </div>
                    <div class="lightBlueArrowBGRight">
                    </div>
                    <div id="Prev">
                    </div>
                    <div id="Next">
                    </div>
                </div>
            </div>
        </div>
        <!--/banner arrows-->
        <!--slider panel-->
        <div id="toppanel" class="topPanelLayout" style="display: none;">
            <div class="topContent">
                <div id="panel">
                    <div class="skinMinWidth">
                        <div class="top-cols">
                            <div id="dnn_Content_TopLeftPane" class="col1 left DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopRightPane" class="col3 right DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopCenterPane" class="col2 DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Content_TopFullPane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottomPanelSpacer">
                </div>
            </div>
        </div>
        <!--/slider panel -->
        <!--slider nav-->
        <div class="fullWidth">
            <div class="centerSliderPanel">
                <div id="toggle" class="topSliderLayout" style="display: none;">
                    <div id="close" class="open sliderOpen">
                    </div>
                    <div id="open" class="sliderClose close" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <!--/slider nav -->
        <!--content header-->
        <div class="skinContent contentTop">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="contentPosition">
                        <span style="color: #fff;">n</span>
                        <div class="contentPositionLeft">
                            <div class="loginImage">
                            </div>
                            <div class="displayText paddingText">
                                
                                <a href="http://apps.nmu.ac.in/Login.aspx" target="_blank" class="lightBlueContentLinks">
                                    Portal Login</a>
                            </div>
                            <div class="loginImage">
                            </div>
                            <div class="displayText">
                                <a href="https://login.microsoftonline.com/" class="lightBlueContentLinks">Mail Login</a>
                                </div>
                        </div>
                        <div class="contentPositionCenter">
                            <div class="paddingText">
                                <strong>अंतरी पेटवू ज्ञानज्योत</strong></div>
                        </div>
                        <div class="contentPositionRight">
                            <div class="displayText paddingText">
                               Select Language :
                                    <div class="language-object" >
<select name="dnn$dnnLANGUAGE$selectCulture" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;dnn$dnnLANGUAGE$selectCulture\&#39;,\&#39;\&#39;)&#39;, 0)" id="dnn_dnnLANGUAGE_selectCulture" class="NormalTextBox">
	<option selected="selected" value="en-US">English (United States)</option>
	<option value="mr-IN">मराठी (भारत)</option>

</select>

</div>

                            </div>
                            <div class="lightBlueContentLinks searchText">
                                <div class="searchTextLayout">
                                    Search&nbsp;&nbsp;
                                </div>
                                <div class="searchImage">
                                    <div class="search_bg">
                                        <span id="dnn_dnnSEARCH_ClassicSearch">
  
  
  <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" onkeydown="return __dnn_KeyDown(&#39;13&#39;, &#39;javascript:__doPostBack(%27dnn$dnnSEARCH$cmdSearch%27,%27%27)&#39;, event);" />&nbsp;
  <a id="dnn_dnnSEARCH_cmdSearch" class="dnnSearchCss" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/_default/Skins/Xeon/images/sright.jpg" align="top" border="0" alt=""></img></a>
</span>


</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content header-->
        <!--content containers-->
        <div class="contentContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane1" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane1" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1" class="col1Home left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane1" class="col3Home right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane" class="col2Home">
                        <div class="DnnModule DnnModule- DnnModule--1">

<div class="containerPadding"></div>

<div class="overflowHidden">
    <div class="lightBlueBG">
        <div class="style1TitlePosition">
            <span id="dnn_ctr_dnnTitle_titleLabel" class="style1Title">Privacy Statement</span>



        </div>
        <div class="style1Arrow"> 
            
        </div>
         <div class="clear"></div>
    </div>
    <div class="spacer"></div>
    <div id="dnn_ctr_ContentPane" class="style1Content style1Border lightBlueContent"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>North Maharashtra University, Jalgaon is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the North Maharashtra University, Jalgaon Web site and governs data collection and usage.
        By using the North Maharashtra University, Jalgaon website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. North Maharashtra University, Jalgaon also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by North Maharashtra University, Jalgaon. This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by North Maharashtra University, Jalgaon for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the North Maharashtra University, Jalgaon Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through North Maharashtra University, Jalgaon public message boards,
        this information may be collected and used by others. Note: North Maharashtra University, Jalgaon
        does not read any of your private online communications.</p>
        <p>North Maharashtra University, Jalgaon encourages you to review the privacy statements of Web sites
        you choose to link to from North Maharashtra University, Jalgaon so that you can understand how those
        Web sites collect, use and share your information. North Maharashtra University, Jalgaon is not responsible
        for the privacy statements or other content on Web sites outside of the North Maharashtra University, Jalgaon
        and North Maharashtra University, Jalgaon family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon collects and uses your personal information to operate the North Maharashtra University, Jalgaon
        Web site and deliver the services you have requested. North Maharashtra University, Jalgaon also uses
        your personally identifiable information to inform you of other products or services
        available from North Maharashtra University, Jalgaon and its affiliates. North Maharashtra University, Jalgaon may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>North Maharashtra University, Jalgaon does not sell, rent or lease its customer lists to third parties.
        North Maharashtra University, Jalgaon may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, North Maharashtra University, Jalgaon
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to North Maharashtra University, Jalgaon, and they are required to maintain
        the confidentiality of your information.</p>
        <p>North Maharashtra University, Jalgaon does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>North Maharashtra University, Jalgaon keeps track of the Web sites and pages our customers visit within
        North Maharashtra University, Jalgaon, in order to determine what North Maharashtra University, Jalgaon services are
        the most popular. This data is used to deliver customized content and advertising
        within North Maharashtra University, Jalgaon to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>North Maharashtra University, Jalgaon Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on North Maharashtra University, Jalgaon or the site; (b) protect and defend the rights or
        property of North Maharashtra University, Jalgaon; and, (c) act under exigent circumstances to protect
        the personal safety of users of North Maharashtra University, Jalgaon, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The North Maharashtra University, Jalgaon Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize North Maharashtra University, Jalgaon pages, or
        register with North Maharashtra University, Jalgaon site or services, a cookie helps North Maharashtra University, Jalgaon
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same North Maharashtra University, Jalgaon Web site, the information
        you previously provided can be retrieved, so you can easily use the North Maharashtra University, Jalgaon
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the North Maharashtra University, Jalgaon services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>North Maharashtra University, Jalgaon secures your personal information from unauthorized access,
        use or disclosure. North Maharashtra University, Jalgaon secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>North Maharashtra University, Jalgaon will occasionally update this Statement of Privacy to reflect
company and customer feedback. North Maharashtra University, Jalgaon encourages you to periodically 
        review this Statement to be informed of how North Maharashtra University, Jalgaon is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>North Maharashtra University, Jalgaon welcomes your comments regarding this Statement of Privacy.
        If you believe that North Maharashtra University, Jalgaon has not adhered to this Statement, please
        contact North Maharashtra University, Jalgaon at <a href="mailto:support@nmu.ac.in">support@nmu.ac.in</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
</div>

<div class="endContentPadding"></div>

<div>
    
    
    
    
    
</div>

<div class="containerPadding"></div>

</div></div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1Inner" class="col1Inner left DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPaneInner" class="col2Inner DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane2" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane2" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane2" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane2" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane2" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane2" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane3" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane3" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane3" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane3" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane3" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_CenterPane3" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content containers-->
        <!--spacer-->
        <div class="spacer">
        </div>
        <!--/spacer-->
        <!--footer line-->
        <div class="footerLineTop">
        </div>
        <!--/footer line-->
        <!--footer containers-->
        <div class="lightBlueFooterContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Footer_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_HalfLeftPane" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_HalfRightPane" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_LeftPane" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_RightPane" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_CenterPane" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
            <div class="lightBluefooterBottom">
                <div class="skinMinWidth">
                    <div class="contentPadding">
                        <div class="footerPosition">
                            <div class="BottomLinks footerPositionLeft contentWidth">
                                <div class="displayText">
                                    <a id="dnn_dnnPRIVACY_hypPrivacy" class="BottomLinks" rel="nofollow" href="http://nmu.ac.in/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx">Privacy Statement</a></div>
                                <div class="displayText">
                                    &nbsp;&nbsp;|&nbsp;&nbsp;</div>
                                <div class="displayText">
                                    <a href="http://nmu.ac.in/disclaimer.aspx"
                                        class="BottomLinks">Disclaimer</a></div>
                            </div>
                            <div class="footerPositionLeft bottomContainerWidth">
                                <div id="dnn_BottomPane" class="DNNEmptyPane">
                                </div>
                            </div>
                            <div class="footerPositionRight contentWidth">
                                <div class="displayText">
                                    <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="BottomLinks"><a href="copyright.aspx" style="color:#fff;">Copyright</a> 2012 by North Maharashtra University</span>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/footer containers-->
    </div>
</div>
<script language="javascript" type="text/javascript">
    function Showie6Message() {
        var IE6 = (navigator.userAgent.indexOf("MSIE 6") >= 0) ? true : false;
        if (IE6) {
            document.getElementById("message").style.display = "inline";
        }
    }
    Showie6Message();
</script>

        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" />
    </form>
    <script type="text/javascript">
        
        //This code is to force a refresh of browser cache
        //in case an old version of dnn.js is loaded
        //It should be removed as soon as .js versioning is added
        jQuery(document).ready(function () {
            if (navigator.userAgent.indexOf(" Chrome/") == -1) {
                if ((typeof dnnJscriptVersion === 'undefined' || dnnJscriptVersion !== "6.0.0") && typeof dnn !== 'undefined') {
                    window.location.reload(true);
                }
            }
        });
    </script>
</body>
</html>
