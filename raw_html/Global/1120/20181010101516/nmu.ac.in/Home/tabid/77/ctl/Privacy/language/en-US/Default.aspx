<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="en-US" lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><meta content="text/javascript" http-equiv="Content-Script-Type" /><meta content="text/css" http-equiv="Content-Style-Type" /><meta id="MetaDescription" name="DESCRIPTION" content="Official Website of Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon" /><meta id="MetaKeywords" name="KEYWORDS" content="Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon, Maharashtra, Higher Education, Academic Institute, Research Organization, University, NMU, India, Maharashtra, Research, Under Graduate, Post Graduate, Certificate,Courses, " /><meta id="MetaCopyright" name="COPYRIGHT" content="&lt;a href=&quot;copyright.aspx&quot; style=&quot;color:#fff;&quot;>Copyright&lt;/a> 2012 by KBCNMU, Jalgaon" /><meta id="MetaAuthor" name="AUTHOR" content="Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon" /><meta name="RESOURCE-TYPE" content="DOCUMENT" /><meta name="DISTRIBUTION" content="GLOBAL" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><meta name="REVISIT-AFTER" content="1 DAYS" /><meta name="RATING" content="GENERAL" /><meta http-equiv="PAGE-ENTER" content="RevealTrans(Duration=0,Transition=1)" /><style id="StylePlaceholder" type="text/css"></style><link id="APortals__default_default_css" rel="stylesheet" type="text/css" href="/Portals/_default/default.css?7230930" /><link id="APortals__default_Skins_Xeon_skin_css" rel="stylesheet" type="text/css" href="/Portals/_default/Skins/Xeon/skin.css?8221750" /><link id="APortals__default_Containers_Xeon_container_css" rel="stylesheet" type="text/css" href="/Portals/_default/Containers/Xeon/container.css?7314682" /><script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery.min.js?1.6.1" ></script><link href="/Portals/_default/Skins/_default/WebControlSkin/Default/ComboBox.Default.css" rel="stylesheet" type="text/css" /><link rel='SHORTCUT ICON' href='/Portals/0/favicon.ico' type='image/x-icon' /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/CSS/menu.css" rel="stylesheet" type="text/css" /><link href="/DesktopModules/Exionyte/Menu/1/Xeon/Colors/lightBlue.css" rel="stylesheet" type="text/css" /><title>
	Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon > Home
</title></head>
<body id="Body">
    <script type="text/javascript" src="/Resources/Shared/Scripts/jquery/jquery-ui.min.js?1.8.13" ></script>
    <form method="post" action="/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8AABaE9IH5o1rZs18W+Yz250brrAyS0eZ0hTPux6yxrBmwWPOo77pQl++lG5iWZutRTy4sxlTtgQuB6udjAue5aecyZChqo+SozMHtM2C16PjQo05fLSzLdsdZjoGlNWSgqHDt6qTg9l2QnCOl21HTbCnFm98uaWk2A+PLPDfWCTwqEQm0asf9E53ZrhM9nOOM7QiQA/dWThMoL2FCY7tvyoNhJ0YFf0N0XaEjYRDzlbLYxI4r1p1BM+qnBiDUgH4fOiY1XBXFXEizOL5wCQEW6AdeQ6+hSEHwIxrFQYgrPDnqmVtjDILVXhagtx+ZJ6QzSlJ/vqObqw7YJwrbu8v1p+wp7T8UsDxouNE9iPSAkfRzqs7mia9jVpKWDoFFUoP5BXKKh18OsBnaD1wKzhpZt9lCLSzouTIpFHzz12UfZfhXy6oDsiaZ2JxBIZiUy3aCChDrCgnWtr4j7zIB5weOkSLkK0XDVSDS2qdOpNTBdUX7riBkuGmHYmLcMeBMdd9xmlOB4/SQaO5idZrO33e4vViTZGD7nJDe4uZ/mgAyxhh4B3FlRBhNYTNgjXhm8ALueHJAwP4DAKvjELwI29GcAEnBEujji5ZJzHXXMy2QI9u9s1KOTyGZErRmjlq0wuVvviL/70a3yQBzHhjlioqtE4zH/PoRrMKKpFeAaj64pCjElzSScuTILe4R6LpRoh3NG6o9pkEuMgJ+gtnrKkcjiMfgTHhUsJm2Wk2jKHTXTvyYLYx9bvOEeowWBayzJiYhN551cukUvYh8wdIzS5Qsh4Sj+sz5bIRwvKMFBbyYibdo0cxQbb/4hZRCQtjG2EC9C/wiY6WRZCwcMmKA51WkvqG4HMpHJ4xRY44lB2tAP0MFWlliNTbdhQNlT4tNhrqQpZkfB+H0qhOjnQhHLT0EPX8CpqXmGEuEGO3YtI7nlgo/OWi7LVOH3ZOZj2TEspRz0S8VdpDrUh8XhYcupWm1Yqc08SFSPphakAYUtU20caLAhQee7cSPbbWE/aRY5/CqLHvHEmcaZMj0/LU4aziTqpgXc/kakIUsm3AHquzofadbD9U296sngMwj6+Lxs/pYuq/yn2c/ZStuC6N3RioLFeH6gE4e+5+z8xhoJ5vKLXk4bhQO5iOOYrBS1sViczL6n9KY5lUDWB+F7T8zkd13/H2Alt9GqLo8EA8OUJtXizGI3tvndC9hCC7Eqk1zIMxmMlFFYtZdE5yDEIykyzneUFGnfRKNvH+LO51rFUHUxDxa2Swwl07/d3n6AbLUVFM93RZRCIh13EN1PPJAlNjKduQ487aYPfLyXkpYal7jYLoJEeO2oUGOGoQV7I5ORUK717hVtCNwv7Y6DUBgRFyHevW5XzOWIU1cxBa96cy6cn/BvgBx4Op4pMU5z3CMhfERg3qxZk21W9dEhaVKs55GzeziWaRv7qsE+kjHptpbEZN8n3UydAEzKrVhDB4hOQVpdyqfLMHuhYu61C/0TZv/vqgdlNqeajhuxnw/2VJ3GKjbMP7X9l0fOleJI42I82moAPUThkGHa2YFAoZ45poqCyitKvF/MqCtqt5dKwSv9pvXC5HANFUBwvvq3I3zPlP3Da9MK91Y25EfteIUJdDzUCxZtHY/IYo3ohSVlU9elG2T3PMcjT++ZnSNufk/ZAaIONC7Ch0PTvIW7wLUEyv/lZzikO9Ao3rkF+oDHelwUVudkt6LgFdxCih75HW1X69JZcuTQ4AotRrIiUFX0ZFM7JiwPjLZJxXb4tIB7qs0YMB14TNYJeUcuqfcHfSYJACgUVt90klAl2r4NfTrcLKiNtIyrnen4TW7ihm3iyUuuwvLJQwAzqp2McyJvfapx0beJV7GALKx4Qy9bLY+0K7DzR7ydc9kxmgl6ryIYWlFGpOQrGsRODJR/sWA8nfYqfrspx1gwaLnkJ9/G7FEiKEJBWGYxzcvXKQAoIxFSo4dzpknd034Q5E+rMx2IoiekXxN5PUAk89CQ7V90Q5XkGsI23rI7OsjHVA99wsKQKqNCXQaK1HMsk19kAGcOQ/4yBr8+o2VygTcM0+FQ8mzXjmfFZ/A/9RPnBgdOzEHA0e8nnQRi+9U4pTqtOWtHmI/Ekf0tVMOi2hQdfoMd7AK584Z5eRNNuxWo5VEYQJgDWgDw1vJPHlyINkGUSqr7+0wuqES2IAl3NVkzWcVKHv2/RD3P3RWHxVJMtbYpEhLDNOI36Nf7v4ZrizDsIbenZ3E7YM1R6sbnEXE7Cya4uvFls/SmkU5q3Ykvrd1900kDkljmUI2gdpbC/CELrXTaCbaAjjL0IS4f6cowyNA2CHfgjgUPIiB81sKd6FY+QPalzkzw3tF/MsJePdKAPfnDgTLNhGqcq08MJ0AES3Dv2YgUMDQL22uCvfs26aOJ79BVLfkP2pSSEfyne8gK4h9z4MGhkHA0kP3KuX+bR6alQkosRA5aEjmH4Spt/RP7SXzXPxVbSy8rCVfb7uPrhUcs/qkftUTZo/Dbu3jsk0P1phB/6Cj6BFg9bZLcoGWoqG7OGEjnHorrPOnXHUjR3EygA9x1R7hZrd1WZkBJ867Aqfhlbf7k0hgFNhJn3tR9Yo++phVyJgRbARPHnkX6NLvhkBx8KY37Nv7+Zcxc0+qXbC/vKiXFZwDEv14lwrnA/ujbj+0EXxqTm8zQQOG0WiopV9aGaNvTpUbdQCFYl32ngf7f2ZAu91RJmKBARqqKe7FEZX/o0kU4SRtWHtdrEyO381p8kDQinc1p6WLPA+psAw4f327p1u6CZMGHFrWM0a6Bhx2WdJKSXEsWbB+Vfbp7atDm90kNtZ0yYtumY/9+Wv1SMnWyiVMusy5FDbc2OSzdkf5WvaJFnYCbh+AmBF5MUTm/tXMJEe4BGbjbdcmf/cv7q+1E5+r7tGqi/24Obc4BN0sIAOfWqGGtZ4LORsE7YYSeZQ/hlsVAqbACw5dhkQqYP6rHo42WD/cPruHDYTIBVXSUaapFbPYdTsDwvrBbD5YBAizDmhev+tggft1EsDRpz03nBI5cfAt1oEi9EMsEwzmvNkMb2QWp3gJe6xxJfRPlZ0eI34vatNmdVF5yEKj10LG042ZvgfcX/UoVC13cBPDDHlwZq86Bdxd2Abk0K1jBCdmz+kdU9Qco5sn78fqN/WZJ3yyRp3XDfurwJoazRB9QE8DY7K1R55xLHBcSgmcPreDVUHJUE+gt/QnZgZh3oeNkESK6sqfT3AhSbv8MZQfxjfe7ivKPZ4bT5+ll04N3t5L67Ukza2nEpoRUwxiNy9W7sMnyViPydBRqiqWPNmHSiRJg24qRFhu+YJeYGxz0T2MRctfsZUsFsnP8IeLM9CMdyQHxzNQj4jPYDOEdLwvkXDpoXQhHdiNAI6t/O2BGdYd2R6lnTbBRj7sCBa+QVxrhBy4Ac3WZpB0tThDIvsViHcxNIniwtUhr5r4l3YuQMb5SJ+jSaVJ+jd/lnjkYlhMrcphC3EX215KZn1tnSwECs5j1MGbCDw9Jodibhqu44rzUBAKfzWg5cxWDWfnnNf8pqiICcGsU+Xlw0rLpioaPak7rF0VCC0617g3zK723Ykw2R1N5A9M3K6ga6lceevKH3yLIDnLqyiwBN0urU00MaGE1XoxOGiQh5Zp2uZwV7pENPMdra3XZtcmoLzWdaLEUEboWlpKURI0aNKxPcDajuP2dixgzTiMoIw7+d9jDQNiFkUX7w1PU0Ju57af/7kBbUWjcWEiIyabUqWV++lYE6SVh9fM133EvGA48i7e5ac5MfxVciFpI6aC2w7FWEPbWfBF7rYW7K2Bsiaq3gb5AoLJHn6hJNV2E8XRl7juay47zrz7WW5ueigzveq2vffobJXFxtLW1MCJVBSXirHgPb8bG6vC1CeAXVFbI7D023/DUtAcE4rW1rS2exnTpZfXjoUH8+j9B+sD4oztyDDG103WcEoOjJdf9cWoZBM5pVKdjwAyN9pFiPawyek5L4Lwg0nyBQwhIhCjI7L6Cj4HXPn5+NfXDr/gi+6KfIrKzQxKtBgvVfRD9Qbgh72EFYoVO0WhbwCPJiZzBm2NjCJNWGZkPzC8EBFNDNr//VP6Rg8OfJ3hI3RRd5BGtkbR2Kxkq3VExJL+cV/oNNYQPomSmj7IOVplzz2GUzPs1kt2R7Hz2dvQjqCqcPl4M3REc/HdYHa0fXuoYO5vUh1ocxwoWaVH4tV0R8TORM5mZx3rNkIIVH98z2mBcstzEiXxYn8hYvsPnEb60A2eZ8aEpBo+Eyl4E0bkxTatzjsoRYtFUZQQvGXhWgF+3iLPoGbv6OqkuT0FBHRsSiQH2eLugQEgARgSTl8+5LEP5MBOVBuChmGqDZtALnLSOYdr075ShbNTxQHNcwk9e/mElgS3r/mdMd0uZw9l5lwkuVGpxL+ezkoOWN39W+7cWR4QFNPFfVvC2kPHR+qfvmF6s2SjPtofSARobX17hAJW6j1YIGZ9Pbq+thr65kWH7n1QGmMKzufPuGpCFMApwi0GVq0+6kZCswQUp94f30YrtHRJE9u9SaZll7TMr7SLfnx9f+8kYBXwfxZCbDQ+FZzc6M/IruEqPLPMELnLb0ZpFS/RoC0Bqx7fSddQODP7EI5wxvmEnnKleSGsZ268D4XYHl/wGd65vwdEbVtKPz5jnQ/43PNhzXXmlTjUrry+2ueSVI5GCEES+kSWFYyWr5h3I2it3Ogl+rfUQp6n3LNWquwNP7czELUdGKazQ9R6UHX4D2gribF209rFQoCxc7uPbh7bke5ruueEbukv+rorE/kvgVdL4kvAwRl/B6/lz9Vo9zAkpn2DWj3BfT3HZ5PC0RJ/YEA/biOe7b4cjnvkZvw+42qbVwDYJR+69HA1FpJ87H74mEoSUWRd3yPbl9zqi4IhezHof3Uqt2w1a8Yeu0l+dWOMgbcy2GZUw4G1bHW+kBh9CLsYx9aSlhVSkUUl6UTvCEWCyuGg2ypopZfvOMZdL5z21z89L0BbT6wib8ZNTs/r+ElDXPFaIKByGmYmi0T1j+TsCNifzmuxpEB/SLnWA4c2MnWBa9UMMIQeB5uNAnTgMqo9kulsIYrPtRhRiMB57RzPsFmwf43XVcanJDbuU7kDjZ9Qn9NaXkcJyrN3Vl3B/DwmM1xkR0H6QnKErXx1ojCG7rURS/t0HvRgMA9BNrXTWpyyzCWTh6poM5NgEVR2n39blqb9LIcDRqHfB4+UxxS3BPidbXWhav6vGdlWLxBuxCwSTkcVGXLhZJt9AYX2yxWQ26G78EGp3dSSnGFbPJ6M07LMIEu5TbSgHULuFTK1JIJJOJgfNnbAvbTR5764FLW/bwt+6IpZ68HR1WoXIJiErAxthZ1oYoU34nggiMiwTwDBH50R2EdQSnQo4bFaqV7rr377foXhneU1MqHhnnVXIBuUdaubtRg7UJ3uULP9loYPYsxOAB32WLg3IgMj865HHe8B5gFHgtNeyOhEno1yCY27VmHbBYQZyPxIKD7CiZlp13cJM2FumF2OhFpRpKwxqq3DNxFYx15dZj3bQFfwFeag5rjBzWX9lAEpDbBZdfMCZpkgBCvhhYqBexzGiBwvzguwszK2Ze93NJScf8bepPmhFSriM7Z0HJ8w42WPcnKp3KHwfqod3ZLiyvluXOj427nWVCI9pnyTJSL/v65OoEY6MNo0OYT3juGIvK8J7My+xBTuiImZJA34KglGgqiIsJk38dvYvFmrxOOO6x3AFmCFi0CjRQYNOPJQDiiWo/Y4DggR6i59Ba3Uqx1q/QJ0sn7iUVTwPApvZZbDONFB2XvzO6rvhuF5UHM3rdGd1EHVYlco0puuzGGeBQK6EZhF+m4pa+Zh8MghJK0UhpG/qERcW73Vv0EAcmKm7pBZu1lcx7tHx0/B9gR/1m2nWhAE6fjsmqch9WJVOBenxt2fBhdrcgKfiU1pDR2mOfdo2k/fflLZ3m9m5fe2b1I0VzT3mx/V5bEiGWYp2XSasulX3nrqN4Jq3tW1uZkTk276eoxMyJuTrbxL4M0js7o6lrqPT1n2e1aa1D9AGu+waiLv62aDCBvqpabmF5loycpy8ZYKivVxaNxmRAZJ6Twbj/JtJUMX7LuoQRLzrYq1nJxIRbf54ChYV++LJ89iyiFliRKQGK0WpBHwKJU6MQjA6kTIMJQ5+4YY9Xgj52aRRvRlFGWR03nJNNkhxu/yGvB8CHyls+BvLFcYN4McSw5jfF4wiUDeEQUUN3nbo2HYvnSUJ65H9caBgRMZb3Kfa4Bu2yxwMLOcCy1MDnhEATuZkyx9phFxok3ZN7O2WwioSRhgV3jcpkRmwjpXhUdqXgqCPUMJ32H+rqWBjljisyCZbRuZ/vLUjhicPLDktYcuAcAodMez5/WcsfSO5gGWiHNi2qI1CBT+ugoerjAjG7zHKzVgjBzExCjWfNAvWSUhOrkwJiPCRn2VdX57Nt9mKsfaE73W1VrGzc/K+D8BLLPflG0D6LZiuNVZQEYYEooVcgLYb8sTwMrRvooZ94gvmkOAfj5Ge1KKDhpG24cLP8ya31BzSakut0Jj1Nc2cywQy7IYKF47SOFlioCt2p+342DYkNYXYu6pe18dMz1fkOmCqRlvgELBCVMaKvBmp62Pbe4jd1jJSZkWzdH7xaeqPew26zQRMN/sqG9YYqvdnQjdSpMg0BEDkX9qGP4dqNs+11nXtivrafuEobsEG+1qnJcdSgi/0sFq1AAxoh634sQGZ1zHgSIH0o0tV3Di/79DdVZZxW9FS9b4b8HthNHL89w4PjvOekGbnmxwzg6uiqRNEUTPfqKwR5XVzBkrPRnjUmFcHcb7QnZtC2d3Z84GnIjo+51rx0/xm4wdlirEYbzM4vgOjDN3UllrPzfXq2Alvn061duPH/GAqX1Tvj3TotSrVmZ+bDiTEBFFr+PY0aMA9pdbsby3pQ0g1sWNfahdalZm8WIqV8jlJOeoopdSg1L9MVk6PJfL28cMqSp6nkkTiECMkKF3LMj15bkkJUE4fJKB2GeZrjcmQrC9XAtqr5ZLX6RhN/OTiUsKpFUe3XIZ9H3xjT9R1onH1rcGCmE2I2I0Dam4LzkaWwJVfxGr+h0oSHuVjnKFHzYb4X5mXpRdV5KEJ2rzNqVYfwa2Q2pZ6y4Sl5Xu/Z4YAPQbc5oXPn+k2m0u90dbCsxiRCgexbsKtlBDRmVg8zpc4qKr1kRakqAAChQuIBKC3u3KEDJ11kt9C8TCcq1HdV2mAFtqFtvQFifLWqqFUM/0lzXyKk6Ott6qQum1xQL0Rd2DlQHMaHsTrHxS2ME6kxYVbg9lUxek0Mq6gE6miZJlxRttk5RdhroYzDx0klEwD4jtBJfV9iaDyWnAzSIh8JN3OI0DQMslqPg7Zz7E86xeiK86qwERbVwq0RwIVbc4LvehNhN1RL2T8Wun735o7Ajwvb4PdEH0taSq54TKSKcpWwzD+gYx1mbHNdypt8MX0/uA+aBhXJzFKXZD+SxJnFRhvQPypmU5h9gWRhY2nVGqRGZxTfaDYD7PJHmrx3A697aBI20bx4xoRnY04hsJxzl4/slNSSr9yBzBLtJKNxsAAhleUucnFREdKQjZANd9lGWJwsPcUTIAI91+GJRqFvafA4Wikkqga0kMezSPjC4v33pWBx5rk0RTHoIRkxBYx8D9EfVmDhEK2p1YkWCpjJlIwF9VjwPaVUasnmXmMua2V62+y43hRhj4hpE2UONAl5TY1WhaHhyJcUXjflPMYeXGgNEJmFEZKIzIq68LnvJhoodwijF5tiFKb1y6ePh3vbuKKX8o4Dum4Xrp9hVpuC05WOdOBzvrDdjcalxnPr1sJNxWxBNIEuFQiiGfhozGhD8wNJdat4BMd0AoP9yNvZ/Wa448Agm3tQL+iVh1ZLe7WGlSA//cFl6sM4V6wexnI/6PjTJpUCwLYxBOtLK0XH16Gn/54LK7tVTbv4vOHy/K77LL5g4cEL4aCCbtTYojAljiRCueBOoJsHMT2khb5qdsC8DjOIbGEOVN2lgLNkmpvf+xstTkYg2su1p68KYzgV2iEJ8lMD6lZKhrImDBCmekoMI5UpnTnZL5b6o75SYazOMYH+czSmLn35LnbzefjPZFOCvY38zP7JZBYhSu6/kEpbi0Yf0jNvEaulbPFP9NaGm0OA5mm0QxXE5fbBfcU/xcoLPBpOO2nEU2hB7VnK2/qqdxDuj4BWVPP3wYA622Vxbm54oJ9iJX48frTk/GMiBE9QUKFDowC8NvovoLNHJZSBBr9J6dUsleWbP0kStGSh19zwJO1G81U2nL5nuMd1BSkXNWgA7yec19d47WK2jWnK88u/mMX3EhD5jxi6Wb/AzuYCK8vTp7FMQ63G/9N3wjy70sp1klyEVGFy8FQe7pM7tmewxXDtu51oyW2zoZweIjKPhZ1VoX999PBH3mwl8AHKJ86c9sLLWYnP+NId6w43pYzdeQQ3BcfqRUgmyAO4AsCfMyJRvXmKuGHPd8QnwWIGJp4zM3OyRWy4WzuaSnMOLxFbFsdDFXfCxZe2bpo2RwxORao8/+BZMmxZtgUbLq/oIDPGOAFWTeWZ6A2HQZLmlwTiKPNDUfMxoxKqzBeu6bMqwmfABExQld8Mp9rYv0dcc1yoXF79WBiBNYfjTsyvUdkFJjeweyByADScWMjLbPmqTBM/3tzocjiWtqGVrQ8tB//7Hq0w45aaJhCymdNUavKepWsetCL0Acv2HB/7ID1ZUejO2ms4bt38RVb3Sdh5B5aTMvMFwv1Lz4obcgucultzqbCzm+OWOLJy4wlvvk0vviQkOr/2VeHyAQT20mc20ltqF+uu852k/0/8xE+aJx1BnJZBz8AY3HcI5wgGwxn0NGNxp9hEebjKKCykYUVnTUqRqESlz4XNcH8+N3l1VrPghlqehJJudlDEjFC7DdtfXPllw8HB4+E6YXaqkyN4PW3bAmkHOWzD9jkPTXJZkLkeroYQ2n0T2ef+aL9Tu+jr449Bd0o6bAhNRuzWnHmwIAcgixgwkMYWBGSGWJMviAzB6M9NsgXCiQDoxV3PvZSEHQtPIpBg/Ibz2RRFuELA/44T7eChKzGCVPAPFNPDn9t2nM9RmO5loF3vmjfdXe8qQi2Z6KItXtu9h80HkDXFNODr1zlEsC/u2RVe8EWlSMDfGPI/XdPQ9C6sxJ5wlCctfS1vqc/AuOfYhSQN84XFEFhwhpx3OognNnTejNtK2qf5kvmO5U9t65lmFtdL7QwIr2YJaBSh9yZ/k0lsR8m+Re6ARzmzbCGauZgxLtsVszJCoKdHDjq2+rvf7Lg8OlFTorAlfyJdTBzwDu6ZHiVqAH3I7aT4Hhi8iK6UsrXR92KNm1oDpfHtNRSGvRZBR8WUJgM4JHAn1hVaoy9liFOp8yOvawy3BtQmiRXUtpdw1g+myV3oInfrWLK+rI7JE5TGqezPE90NdWb4As15R8qlY5zVVXXo3VNjE30U9PwJ/LfpsNkg2ew/VFGEqsVoFq2jw+FAKhsyz/nT6ozOyCB0fLFl1xgFH1LCdrseIz9r3qfSLz8S733F6VG9FIRLFqBxJshpmbzTpaASCPVa7NkYuS0Ak/RxLEKo9TDed79bD6J/XrJqD/yeXAteriqRlbMdGzWRlz2a+ebfAGRb83rkf+twcW1qTZPcBndz5aH9RrOt+QzdnL/FzjThrF4dae14aoqNDu6G8cfMdHSehavAZ6hAynLXang0nvp6uSN8O+mSgPOtQksiuyFwvb4bRwDfphawANoTZaVlRKEXdMocAGl1Z7Emn2PV9RUaJ6XSNZYDe9zRQ1zMtqpge1cJhNKigC2fecOdTlVWRlSRrNTEGUZkbePypusMHS164B1U1hwKiOerdmjkH9qW8mRMXbLV8FgXj2YR4suMFXLvQpDRGPP9yI7RzqYauDhW3zRrtwjzRUyT9dSB+VP1h0VpB2cHjDSIR+TwhQG25v42+y9u4qMDWYH3NqwFay8wYqYZrpVs7tmEoHhVD3N4AMupJBdrJ6H0CjnFBujBnpmFJOqt6x5b2eDCiJiLBXEbGpd58yPvvjTznV6Nj3BdmbLmN4CK6Zi0pU3a62K16kbBvMt5Ok3mhWmndEPWrYleILUU1sYlEYmGezArtVGVd7c5IF/G7n0cZzMVsVaaPXGqntUjSmd49nc0uF0qQ++qPWQJyWyU9WzNhJgxdjZ9taBLKl0b9amUU2OlxOB8sR/qj7JaLlTqbKVzSDebmm2QUs4z5wE6ZUDcoTl3/jXTxWBVDiBL/rpZzfyPvIE72dt9iVeewbiACoAl3mcenvTsWFo/Jk41wF/wsqUM0K+pO2LnR38qg0utScHC5nupJzGxeSuGMVe/vRBglW/K6oq1EYBzFcPTme/lLdCxSAKY1VBmwPkkXO8YfXm8Nboho31IxxFArIyHp7i8Uj1ka2lbv0Cio09Q+FnV4+z+KwuhLRXlC6KLJhPMbtJMonF7AOuLO/zf5hxicReWk/ukS+eoFa05/dJ2Yc9OVlLTTYhuWd3Uc5TlsjWpo2OahZjqmLXZmKcGxVazS2p82iYimcvuese5FjqWIkFJSh1lM2nUSXT9RgQzYsjtMCkRs3IQ+/SEN/9c4qGjsGl8OhLt159m+kM/naGzzmyuo8Eq/vldUc6ACh7+KLfu6g/Paf0AsMYYbi7GFNUgSpvK/G3ixoBYAEFFEq2Ls3ciEkVlGA/JwbhF+clZgKQMp7PKA1b6Fi3R2/VRv+OJclVHdeC3OTrDxvxWn8UTcgfafJMDXAmOy8MxB2vaSRlSvY0NegjobOZooGqVZpI3Xz89zImdUzV5W4tKzMDTGjaBrOV3VHrBerSNLSgwAWkryDOMBkaqx1l9nzXxbISISPN0MOJ3aSzgAdf0B0yH2HtGZTvf60j05Gqnrlx4Zg0cs5Pt8hNQq50rTeBLq8xfvNMBEwF2cKXR6prO1QZ61zAif4wDBm8W1+lC86DTEYKpeobZx91N9vkvgbCmu30luk9dlbvXpxyhPnclY88Cp0fdxITeyF5nYgDKecgkjIAg9c9q6pon1kMQcCC0eCi6pJx71nI1zvFd4lsKbcyFUkKVFcqOwdjCJt+rQoJ8cRALj8vW+8wALwIDHCMLR2gezVOJSyid1dP+EB2izB3pFw3ebPRENK/nIrDUM4xbGEWRUi9jBuGll67T9PN/lWxrGIV4oLcsm2c/57cW7219SrR1e8l8omdb4oFXTYDCDDpPYQd82uJ2EpzXGhN2t9pmM+ZSVXiIeWYET9d0SXtLt6NW86EtU9etkmrAQWU5pQgaQHV+nHqUv2XwzeOg805rB/ZKbRK4GWOKrWSgaDUasqpihtd8D3n9Br23ztnfFBb0pa1FFnFMtl/Xl+Gx/iima4y6CFCIAe9q3mlkzTpLMFF8dGANwbNzn9pq8phn8lgLJON0HBN6fkocYevZVeSDMtbwqBRz4WkT9DDyIEStQN7jMxZLN+JfCXDp/7oBfdzaE+njnKBTtsH/SF8UHqWS3qPXQDbYcsjh2naPpC+tP7lYKZKt0cNb1G31NG4vh03KZp6uRDrhyxjYuHia43R3mwLrthuvkW63rXDZKrQuhc8GUxU26QKtpF/vqmJayjIqwGIJbD3aHesx9GmMHNP2XakEZ9NtrVLxvanryzdqZaHyYx3OZxP1spf8bpZrm65AAaGBB6EnleQPf17tKZW5TKjW2T+u0d0B8BC5nIAnf6DZ8C13e/YWnSiHQZBWs9YLZlHWjSk8xkemXebxd6KNZxK5LBy2CkLMm7o36TC8xtmr6MJ/ozgmbLTOlciBx6ofNbI/19Tp2PbShJzCsSI27UCY4xje/EhdCOx0FiKH1t0ivXRgBEzZ9nGux8uH7etpl2RlWO7FhCw6B4QDr76baJoNMrUCHE5hhKkibtJv99R6fqUSm/I9IWheRhk1AQ4YOpddgIwQHiFS17OnJKUT/xAZZNm8FwoGAwLnewF2VlFYWo+5bf0NVYCTowAPZniiUIbCnkJFcJw9qu9aBxB8UNI/Tic53X6Rs1fYuvJ+PiN70j9JmDhhiPjkR19Ll4sdXwx4XD8QLM7TNEpoNqz6XFffwB5MAjAoSK3GFfzQSsye5LuZbw36hAQau7uf26qGB4riJKEqLIvskyrIlclno/PCJTcjML7zMN7hc/zTgvmHQVi7q3F6ZcCKKPmgldRI7nknoz0swls8GmZEDGlzR2BBV3q47tYaanEsQVzQAJytQYAIwoQB4L4aiDSwjdbW3GAcSpmo09D8jQPNry+vkjo6z74lZGZNHfqzY6YcJJSFQqY4Jh8wJhQe2W6f5dKLrOoHF6s/WW+z8FFncIRJP+MQ31vpLE5iGygj6FBeMWSX7NyYUwTg9WTyFWYaeCfdMWQZ0YIliWOuuqHyUnae/KW3iWbfZEOHwiVQ5rTbLT3LT9fmkD0XabcY+3RDYvjX438bhrURjmZE+vliUYCpycg324L1XfHeQS4pmKUKbEIMBmn9LHLy+OA6eKMfpSU10hekcDypdb1ADJbikVlaSpqu6lz1QHTc5z7e2f+kbn8mdcSnAXAQnfWWtii4d55XKUSMAJI+eIX+RyNnLf3JPi27ptRYecPGOz4IYFz+pNW9gkVJh1i+IHhOye5BVHqdnkxihF1X54LbaFRsmLF9vyuCHKQqQVZZ1c6mhR0kWCxAKE7o7hRbcDXEJ9rOM8v5mO92rq1vnnUz3UPVaW1cmG0AeDoZJDBFJTuZET41dubKaDva3XF96kNtk8QLk96aDl6Ru2OagHrl7nhv7cRbziZQCjdQclJWBeshY9XK0GBb0VU/9MJ80cu2m/xHWtRfgsjxK/2srs6/K+WerVgc9qgjb1qGDZcnIwXoNNZDWnn7Q9g15qWbSrRV6XkWclakEgTJqS2MoSFnSSm+FFVLK+d7vv4QTHhoPR5vgLgtHfzh8DQmCT7FdFAu+cN4QcBdWIqwkxVsyRvf1U7ErQ9CcXh+0KS69FtIZpGYkOEx/2po9eBRV4TlhIOP3HNu88oP4D167w6sApEeotBsvxLSeUSOiIc5o4/HV0e58rOIA06u572ZJY4jDr3XjQ17ehlqcwZ8IKM8hOOsFOoKgJGgJnOqALkUz3vsDVNg5Pus3q2JWY3LbgKvE38rrqfNEJwoNCHf+Wkz225HXWC8FgY0BuKBYNyKgT2r1+8dNqeINv6Siw1PJ2jrrOR+i02Zlk/jW2cPKtOCxfiZjhpox9Ci7nLtodOJW2lczNq4PtuwzBb4Gy/0jqqpwcfbjEu3Sp8wNg3zJ1SPS1ocJKs2K5ugD+L2nZ/9JG6ivM9odc/tlKztfjyGwbLy6lhXahnXbYEggj5FP7aRPPa9Ml20JVhubxj/NoQZ+43KqrAlcq1WpwtXMVVoY2J3mBw+Opm6102dGcfh7B3N9D2DBgJMNAaj7YVTZDCd8sTZiOKfVLBoDayGLqb99YFu/JNEv8CO2tLVFHyhwwSngFX9GkFlEbwFYsYglIj7AUNn93iY3HJT6kGOBoKnC2LsCSk9357Mz4A66u+66E9YV4vBEIZ5eGEFsclpojCjEvPb37VxLrRdYHjgWGr3kpXicJdGoRiTmY5P6K83PXagAUQszEiuKWguQ+QTalUVXqg1zfKNaOUM5qR7Rb1pZKPBllDdMAY2j+xAkVxWzyikd7LJ/PvF/lWBpIiE8Ha2CV9LvIRgl/BWncMNMUEjNXofwXHZi4ZXv9Fh93nNhXE6u2wf0bhA5oxD4pEuU7ZSPMqYTpSxPn2tEmaE0r8iSefGG64K7vBub9uglkqHmxvF+ynrPMsOuyxmDnz8ReZOkE63PpktcknKQdf7Do5Q0RpYsD0yE+F2GRZbSIUB95Xtje4DxkWJrIl/27Q4FXZVw1QR34QmbMVBZu5sp4BH6Cg/e5sV8tmJxyXcTkmfpDEvrFJBfKELfu2nu+b5lmJpcBHkWtwgZTYohGME+/lMWdYvqQU6rDH/eo1D3jgBqs7eBOyhmZWbioZdT9XTw2r+4+5nV2+CCLIL/1/8BeiLFuPUHyd6EioMmRJVJoMvu//9R5/jyP5xwr4ZOQMMtUAI33+FlaVFAKym31FgUWqNZ0CsC7wuE3JPgC8H/ezQI4MA7WR3QPEsFQuT1unIho4vcjYmcyYA5PjIZpjPPxO6FDf41lxlG5HFRxzle7axRgEJWzeFKVNSljs6O+SPUSBQvWPnsCDvuGlSXFtlOeIDMX+AUGOWXnk9idBPEFjCHN3uq7A62cCN5yXLIGVSGUtI7emDoX37ih1jnuAUkPGhqV4XStvM4PDEwPY5N4paOaPXKp4U4h5k20cnD3eTz/Gd5giAcePbu3jr2qaaEO5Y1ySRs6g/uv3TsTwTjrgbPgc7V+mH9DvUSm5xXSOtzjJfGyv4mM/DXs/Nqa+nslKXv95Z81LgvamtdqS90pLXhZmQgOKCL9h63eP6ax/q69flGjv2WsblVSxo8WZHozsU/QsqOxPInr+5bVYTag+jLpjVHGrNL/aVZZngEeP70aeIrv/mQKN4+fuD6oWVaoTUniDo+EY5CmFbi+eKS+wN8pNgnNc8EbfSW/GDsWLUml/Sb/huj4XQQnj0wXye/9E2KyncJXaKpHznwyAuq+v+nbLKAmfkFTNNIv/2nCTwkSbUEQAgNOYbHJhPj6HNEKkUbn6MoGblvCScsUaABFd/r8uKKBYwiu0a3I8L6piqvBTFmS6/jwKphrkfasJ20hq8kfQ8POpf1gv8lBAGt2ZAupyOQIvWmHlBSigmIGv/IHVSYdQX6tFE7PkY1oalAup0pL4JrEBE/31a849esux1Ah6rW3kyNBMV0ovJ8C6fBlWaa37n4I+fYBpSXg1K/+ywgF/r4TaP02kYgmTjHe5Gdjo6TjDXtu8fC5rjxbMfxIX5B2A45Vyrjg4vfEZwd20NKbW7zp+/A5tdSO8dyWYI//g+nVzasZoYkEyFVFOoaX334r0JBxofycRonPTprS366ERLAbu20Nh62p4DRHsjdw0OqvnYHyc+/Zrm0iXL6br7XvFbozJ06fT9cNRUAJTSwu3DaEMfSKXXvRluapHNHkPSXbI9CUu7M2+8i8khBH+Pb0NikTXJjfNoTK9vwX08OHG2KAMFY2RifL8ZblzBmhuiccLdTE/GaUVEO2z1cboyiMkzDLrxfHezn5rbV4wEa/tbs0OCh6jZPLaKXA+oIaLqjFdKFl65IS/ALPPO5fj7Q6lq03Uc7UoYI9j8ttIdtRqKMuUrs4+VWDD09xguwjye/nyMSATK6sfX2OxOngAqUYSqZb6D5D40Hjpz9eyLUi49fxbwuEOO/2gi2EMkGCnWijynGXqVWWpZNBMxaVaObia2vWFm8K6uFZiODW6vhPflZSaIigCPXjOfSO4Af1gDEBjcjltJZohq8cDFR8+jwZjQD2aKBwpwetBD8EifZFhk2AHO2O/OIWjVnEUC1FbX/aeUocvuXKcjBsI4v8M2gx9SxLjgo++dG0uvDwRo9eRfJT+Wu2/WA3PW/TIqwX1DDyUhzhWB7G1ov1+idCHu8o6cGfaVRY1VmoRzmPOuDNAU5dZwU6MlHbovHiX0XGxWF1xzsZy+xTTKsLMkzDO4fbzmIW1tLnoAC9z6+VC+98nwGy6a5LKmERb8S8SGvBI+2fiVZR3yY4mrmK07JIAmpqXwMYcik7uvNf0tidJ47fkBARKC+CFIotU/i3GkKUocCUFpvN4ASu1GNKaT7MRsHt9oHz6IWJz83nD3oY2VG4N0+yBo9qp5ycm6bJg8uzBbsvWjVTt7B1hfIYzZEsMCx+g7pMdyR+D1BhCLZNDwIs+w75PMceB4nED3pTBgYpbFHMSE0rgyBfcqn/y1LPS1TfeZ3A8LDR//qpUbNXbmTzPoXC4TflzgB+jndkQrdRXCHrnX7dO+q/7TlWP/nw8I+lL+W7l8TWaUHSuBxKSs06KWvzZE3ijiLHkHvMrsBWf2C+82JHD2w+yKP9ZIfxzi+usK9eMb0MlGR3ovPZNAMXF1LbzeKaTbtsAAtDA6JNwII2+NnjVqzw/9LgW629W7alF0Gsh4+dbmA9o1bZL7cYk8zg80RFpOXahcPwtwPzaryo1JT7G38n5YoJ6ZtaqAWCqNMUE/hUFcwdv+Kg/2Gu4INfx44x/c6wIsLTkVo66k91b2DVICUsFQEoSgeENQG71xsOec+c8mK+G0lq+IIbWiOuoS1/y/tbvBE2OVaBtXa2JbTNi5Obs5cYQUyA5CBPgqs9KqhK++o4zJks9ux43K/xwCxLSXOVEoLj69ovrl3UezXXf+x+VSMq2Ef37Y4FdXC2rKP8Qy7Rw4TrjS21nNjhyExfJzl5CwruxD6+lncZM8L+xauGQ9x3l6qYxp1q36XQTHJWlEehU7lmR4beV/en/tXQd+wQe2p33aK7LPBASscGSVfAR9FZfd/fawbQdwPLp1FfJ9PV+LawCJdAUl3xwMgdaGwRaJTO7JGQDNH+fIQJ7lLRMNk1geN0wzCLQO0B/gI//mkgU7BpjE8zMsyshAJTVU53hVgq1cAeD46jfpCrqgqtaww7BRW8BjTnuZ0tQDDZsp2RH4VAiaFjdDGPc0O5s5MLYD59f7TXz8IlBW6ivWVgFEV23yOhu6qDp0mNpqZ7mEoKMIFH/QHdBHaDiKcVM2tnBbx8CetLQ3Nf0ZHsMREGlooxAwFpf6oU5tLXkqVNFCLFR+aobxh/VOBNggAq0uXDdxlDWhi8mabklfqmoUbG8jxD50D2xSIVAD57FuwPTzKKG8V1938jU75qG8tvxmBJEbAtNpfPubPZZB6HxIvIwWNoA8SNUZO+wPg2kOG7oqkeWryjrc4utnBFpC/2GNBCoKe/vP6TwBUszazmb/pJCtjQ+8CFpGE17oQKNvOpX4o0jbf8zTYeHgf6ySCyUCCiBvCvdM/zftOATV3ni4DIBQrAbD0SGnwCOYR8CEE/D0BNgY7cp1/u5GDg7GwnaIN3OcPOg9vtJ0WcV6CgPciieRl0wYRKDpbZCXiC5EAARVEKMo61V+Rc4ca4N2tGkdS+YgbS0MWD/el7Mh+LR73yQfaKdG2TSCYzwS46wqw9ST8FNCbDXtX44mXcxuagn82hk9p9z/TCoUODBGoFN20IAIXwD5phi0fkelE0JGmQvqK0KWhJwrK3Ih6z0hjz4FRIVDhxfsfczPA4t4cnww/zsUEPpUe+IG6TgtSok2u0RCTET8pdyXbzYfENcsLyN9hdXz1tknYqY6h41abHlL34EQ+9fIKi7RJfzTNmellnqV7QfaiHpBHvm5li6SbFRh9f2sdUuUWACD/y5VvW/nCUfKMMmCkk1MljsoG3CCK4HBgjqIH3GwbpxTcwD3jgl++Tmsq0bA8nz+4b8yMu0ZV9+JKU0DeS0+TGLib8rcgzCnlZ+EEiMPb89vEkwWuhKq5XYlZI8hWPRg08IKKMMdLTaWN+mAspdQyHMFGmJL0vJOtLxykjLTdsprQjsOuDknGd7fLbBpclsCdmone2UcaXs5lV6T1uEMwnIHIWELue5OI5ulzDSIwB0v81BloAWeMW4q4Shvq4vx3dHmQ2ERNV1EU1rzKlskAxg6A/77j+ix/wF+x+f8jJ9Q13YYUmHYwDEC8Zv6VLru+QpTijZIhuyymWJk7Ss2h3A3s+Hf0SDcMy1xU9x7GXzFme3ld+UklcogpTw0mob4z5bnaoxU8nzlrFRd9MzgPE9c8LLkY7EWF+ApyEkGgmQJxxDD2qJEQQnVRf62jwMtGL616qIw6p6wYz54uQ7UJw==" />
</div>


<script src="/js/dnncore.js" type="text/javascript"></script>
<script src="/js/dnn.modalpopup.js" type="text/javascript"></script><script type='text/javascript' src='/DesktopModules/Exionyte/Menu/1/Xeon/Scripts/menu.js'></script><script type='text/javascript' src='/DesktopModules/Exionyte/Slideshow/2/Xeon/Scripts/jquery.cycle.all.2.74.js'></script>
<script type="text/javascript">
//<![CDATA[
$(document).ready(function() { $('.slideshow').cycle({cleartype: true, cleartypeNoBg: true, fx: 'scrollHorz',timeout: 6000,prev:   '#Prev', next:   '#Next'}); var a1 = document.getElementById('arrows'); a1.style.display = ''; var b1 = document.getElementById('bannerPane'); if (b1) b1.style.display = 'none'; });//]]>
</script>

<script src="/js/dnn.jquery.js" type="text/javascript"></script>
<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3a4d4da635-3815-4b70-912b-f389ade5df37%3aea597d4b" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="SZ8ThEVP9x5hR9OKCzqM+0Bg54K7HdLwG3NIu/SNgjyVR7OlvEPywbYYPV+gXrzffMm0/AJR1pdkCVyFjDZvI6Ux9wqMGl1aqT8Kat8d1qxHMduKGD7nJqqaZw+QznmbGbs9fyD8IBAGaeP+BrWxh+qQXrAVNI9Ax2h/NJrbmgzCxlFSf3Mvzo8vqAmPrBK6aSkOO/ge/AGo63nvA2MytOf5Kt5SQufqsvPyKA==" />
</div>
        
        
<script src="http://cufon.shoqolate.com/js/cufon-yui.js?v=1.09i" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Futura.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/Font.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/jquery.pngFix.js" type="text/javascript"></script>
<script src="/Portals/_default/Skins/Xeon/scripts/general.js" type="text/javascript"></script>
<div style="display: none;" id="message">
    <div>
        <h4 style="text-align: center;">
            Sorry, This page dosen't support Internet Explorer 6, If You'd like to view all
            the contents properly please upgrade your browser to IE7, 8 or 9. <a href="http://www.microsoft.com/download/en/details.aspx?id=2">
                Click here to get the new version of Internet Explorer</a></h4>
    </div>
</div>
<!--flash section-->
<div id="flashPane" class="flashSection" style="display: none;">
    <div class="flashPadding">
    </div>
    <div id="website" class="flashContent">
        <p>
            In order to view this page you need Flash Player 10+ support!</p>
        <p>
           
        </p>
    </div>
</div>
<!--/flash section-->
<div class="skinLayout">
    <div class="skinWidth">
		  <div class="lightBlueFooterContainers">
			<div class="lightBluefooterBottom">
                <div class="skinMinWidth">
                    <div class="contentPadding">
                        <div class="footerPosition">
                            <div class="BottomLinks footerPositionLeft contentWidth">
                                <div class="displayText">
                                   <a href="#" style="color:white;">Skip to Main Content</a></div>
                                <div class="displayText">
                                    &nbsp;&nbsp;|&nbsp;&nbsp;</div>
                                <div class="displayText">
                                   Screen Reader Access</div>
                            </div>
                            <div class="footerPositionLeft bottomContainerWidth">
                                
                            </div>
                            <div class="footerPositionRight contentWidth">
                                <div class="displayText" style="color:white;">
                                   <a href="https://login.microsoftonline.com/" target="_blank" style="color:white;"> Mail Login</a> | <a href="http://apps.nmu.ac.in" target="_blank" style="color:white;">Portal Login</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>
		<!-- Logo -->
		 <div style="height:95px;text-align:center;margin-top:0px;margin-bottom:0px;">
			 <div class="skinMinWidth"> 
				<div style="float:left;"><img src="/portals/0/images/logo.jpg"/></div>
                <div style="float:left;text-align:center;color:#02164B;width:800px;">
					<!--<div style="float:left;text-align:center;color:#2daef2;width:800px; #02164B">-->
					<div style="text-align:center;font-size:large;font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Trebuchet MS', Helvetica, Arial, sans-serif;font-weight:300;">Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon</div>
					<div style="text-align:center;font-size:large;font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Trebuchet MS', Helvetica, Arial, sans-serif;font-weight:300;">कवयित्री बहिणाबाई चौधरी उत्तर महाराष्ट्र विद्यापीठ, जळगाव  </div>
					<div style="text-align:center;font-size:small;padding:0.4px;font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Trebuchet MS', Helvetica, Arial, sans-serif;font-weight:300;">Maharashtra, India</div>
					<div style="text-align:center; font-size:smaller;padding:0.4px;font-family: 'Lucida Sans Unicode', 'Lucida Grande', 'Trebuchet MS', Helvetica, Arial, sans-serif;font-weight:100;">'A' Grade NAAC Re-Accredited (3<sup>rd</sup> Cycle)</div>
                </div>
                <div  style="float:right;"><img src="/portals/0/images/bhainibai.png"/></div>
            </div>
        </div>
			<!--End Logo -->
		<div class="style3Border"></div>
        <!--header-->
        <div class="skinHeader">
            <div class="skinMinWidth">
                <div class="logoLayout" style="display:none;">
                    <div class="logoPosition">
                       <!-- 
<div style="float: left; display: inline;">
    <a id="dnn_dnnLOGO_hypLogo" title="Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon" href="http://nmu.ac.in/en-us/home.aspx"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/0/logo.png" alt="Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon" style="border-width:0px;" /></a>
</div>

-->
                         <div id="dnn_logoPane" class="DNNEmptyPane">
                        </div>
                    </div>
                </div>
                <div class="menuLayout">
                    <div id='menu'><ul class='menu'>
<li class='current'><a class='parent' href='http://nmu.ac.in/en-us/home.aspx'><span>Home</span></a></li>
<li><a class='parent' href='http://nmu.ac.in#'><span>About Us</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity.aspx'><span>About University</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/misson.aspx'><span>Misson</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/universitysong.aspx'><span>University Song</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/informationatglance.aspx'><span>Information at Glance</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/achievements.aspx'><span>Achievements</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/vicechancellorslist.aspx'><span>Vice Chancellors List</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/aboutuniversity/annualreport.aspx'><span>Annual Report</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/authoritiescommittees/committeemembers.aspx'><span>Authorities & Committees</span></a></li>
<li class='child'><a><span>University Campus</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/aboutcampus.aspx'><span>About Campus</span></a></li>
<li class='child'><a href='http://nmu.ac.in/kamc'><span>Khandesh Archives and Museum Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/centralschool.aspx'><span>Central School</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/campusmap.aspx'><span>Campus Map</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/universitycampus/howtoreach.aspx'><span>How to Reach?</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/extensionactivities.aspx'><span>Extension Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/researchfacilities.aspx'><span>Research Facilities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/mous.aspx'><span>MoUs</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/home.aspx'><span>Satellite Centers</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/eklavyatrainingcentrenandurbar.aspx'><span>Eklavya Training Centre, Nandurbar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/rcp'><span>Pratap P.G. Research Centre of Philosophy</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols/en-us/research/labtoland.aspx'><span>Pratap Shashvat Adhunik Sheti Tatvdyan Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/aboutus/satellitecenters/mahatmagandhiphilosophycentredhule.aspx'><span>Mahatma Gandhi Philosophy Centre,Dhule</span></a></li>
</ul></div>
</li>
<li class='child'><a href='/LinkClick.aspx?fileticket=oqDHHmONXsE%3d&tabid=3881&language=en-US'><span>RGS and TC</span></a>
<div><ul>
<li class='child'><a href='http://apps.nmu.ac.in/circulars/Bcud/29-09-2015%20Submission%20of%20Pre-proposals%20under%20the%20scheme%20assistance%20for%20S%20and%20T%20applications%20through%20university%20system%20of%20RGS%20and%20TC,%20Govt%20of%20Maharashtra.pdf'><span>RGS and TC Schemes</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Information</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Academics</span></a>
<div><ul>
<li class='child'><a><span>Schools</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/soes'><span>School of Environmental & Earth Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sols'><span>School of Life Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/socs'><span>School of Chemical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sops'><span>School of Physical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docs'><span>School of Computer Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/domaths/'><span>School of Mathematical Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/udct'><span>University Institute of Chemical Technology</span></a></li>
<li class='child'><a href='http://nmu.ac.in/docll'><span>School of Languages Studies &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doe'><span>School of Education</span></a></li>
<li class='child'><a href='http://nmu.ac.in/doms'><span>School of Management Studies</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soss'><span>School of Social Sciences</span></a></li>
<li class='child'><a href='http://nmu.ac.in/soah'><span>School of Arts and Humanities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/schools/schoolofthoughts.aspx'><span>School of Thoughts</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/dat'><span>Buddhist Study and Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/smcrc'><span>Chhatrapa Shivaji Maharaj Chair & Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/mpsrc/'><span>Mahatama Phule Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/svsrc'><span>Swami Vivekananda Study and Research Centre</span></a></li>
<li class='child'><a href='http://nmu.ac.in/grc'><span>Mahatma Gandhi Study &amp; Research Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/sgsk'><span>Sane Guruji Sanskar Kendra</span></a></li>
<li class='child'><a href='http://nmu.ac.in/bcsrc/en-us/home.aspx'><span> Kavayatri Bahinabai Chaudhari Study and Research</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/phd.aspx'><span>Ph.D.</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/lifelonglearningandextentionboard.aspx'><span>Life long Learning and Extention Board</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/lifelonglearningandextentionboard/eventsandactivities.aspx'><span>Events and Activities</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/academics/lifelonglearningandextentionboard/coursesoffered.aspx'><span>Courses Offered</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/tbc'><span>BACECC</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Administration</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/registraroffice.aspx'><span>Registrar Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/administration.aspx'><span>Administration</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/lawrtisection.aspx'><span>Law/RTI Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/faooffice.aspx'><span>FAO Office</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/examinationcenter.aspx'><span>Examination Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/eligibilitysection.aspx'><span>Eligibility Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/sportssection.aspx'><span>Sports Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/scstcell.aspx'><span>SC|ST Cell</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/constructionsection.aspx'><span>Construction Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/prosection.aspx'><span>PRO Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/approvalsection.aspx'><span>Approval Section</span></a>
<div><ul>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Online Approval</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/computercenterexam.aspx'><span>Computer Center (Exam)</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Center</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/universityofficers.aspx'><span>University Officers</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=ZprmAC3RMhY%3d&tabid=1350&language=en-US'><span>Phone Directory</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/researchanddevelopment/functioningofrdsection.aspx'><span>Research and Development</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/researchanddevelopment/developmentsection.aspx'><span>Development Section</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/researchanddevelopment/affiliationsection.aspx'><span>Affiliation Section</span></a>
<div><ul>
<li class='child'><a href='http://affiliation.oaasisnmu.org/'><span>Online Affiliation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/researchanddevelopment/affiliationsection/licreports.aspx'><span>L.I.C. Reports</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/researchanddevelopment/research.aspx'><span>Research</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/researchanddevelopment/iqac.aspx'><span>IQAC</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/researchanddevelopment/avishkar.aspx'><span>Avishkar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/researchanddevelopment/bsrschemeprogrammee.aspx'><span>BSR Scheme/Programmee</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Student Corner</span></a>
<div><ul>
<li class='child'><a><span>Academics</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/admission.aspx'><span>Admission</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/syllabi.aspx'><span>Syllabi</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/questionbank.aspx'><span>Question Bank</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/academiccalendar.aspx'><span>Academic Calendar</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/academics/papercodelist.aspx'><span>Paper Code List</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac'><span>e-Suvidha</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://exam.nmu.ac.in/'><span>Examination</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examtimetable.aspx'><span>Exam Time Table</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlineresultscollege.aspx'><span>Online Results (College)</span></a></li>
<li class='child'><a href='http://182.56.2.247/nmu/'><span>Online Result (Student)</span></a></li>
<li class='child'><a href='http://www.nmuexams.in/NMUEXAMS/LoginScreens/frmStudentLoginPage.aspx'><span>Application for Photocopy/ Verification/ Challange</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/resultsofverificationredressal.aspx'><span>Results of Verification|Redressal</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/convocation.aspx'><span>Convocation</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examreports.aspx'><span>Exam Reports</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/onlinedegreeverification.aspx'><span>Online Degree Verification</span></a></li>
<li class='child'><a href='http://nmuj.digitaluniversity.ac/QPCourseSelection.aspx?ID=660'><span>Previous Exam Question Paper</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/examination/examination.aspx'><span>Examination</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Facilities</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/financialassistanceschemes.aspx'><span>Financial Assistance Schemes</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails.aspx'><span>Hostel Details</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/hostelday.aspx'><span>Hostel Day</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/boyshostel.aspx'><span>Boy's Hostel</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/facilities/hosteldetails/girlshostel.aspx'><span>Girl's Hostel</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/administration/healthcenter.aspx'><span>Health Services</span></a></li>
</ul></div>
</li>
<li class='child'><a><span>Download  Formats</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/examinationsforms.aspx'><span>Examinations Forms</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Formats For Colleges</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforemployee.aspx'><span>Formats for Employee</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formsforstudents.aspx'><span>Forms For Students</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/downloadformats/formatsforcolleges.aspx'><span>Statistical Format</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline.aspx'><span>Student Helpline</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studenthelpline/studentfacilitationcentre.aspx'><span>Student Facilitation Centre</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/studentwelfare.aspx'><span>Student Welfare</span></a></li>
<li class='child'><a><span>E-Resources</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/studentcorner/eresources/ejournals.aspx'><span>E-Journals</span></a></li>
<li class='child'><a href='http://nptel.iitm.ac.in/'><span>NPTEL IIT Lectures</span></a></li>
<li class='child'><a href='http://apps.webofknowledge.com/'><span>Web of Science</span></a></li>
<li class='child'><a href='http://www.sakshat.ac.in/'><span>NME-ICT</span></a></li>
<li class='child'><a href='http://jgateplus.com/search/'><span>JGate</span></a></li>
</ul></div>
</li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in#'><span>Useful Links</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/events.aspx'><span>Events</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/tenders.aspx'><span>Tenders</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/rti.aspx'><span>RTI</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/circulars.aspx'><span>Circulars</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pressroom.aspx'><span>Press Room</span></a></li>
<li class='child'><a><span>Job Openings</span></a>
<div><ul>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/university.aspx'><span>University</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/jobopenings/collegesinstitues.aspx'><span>Colleges/Institues</span></a></li>
<li class='child'><a href='http://weblearn.co.in/nmu/jobs/'><span>Apply Online</span></a></li>
</ul></div>
</li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/sciencepark.aspx'><span>Science Park</span></a></li>
<li class='child'><a href='/LinkClick.aspx?fileticket=2wk8wKpV9MI%3d&tabid=1088&language=en-US'><span>Citizen Charter</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/publications.aspx'><span>Publications</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/pbasproformaandapidetails.aspx'><span>PBAS Proforma and API Details</span></a></li>
<li class='child'><a href='http://nmu.ac.in/en-us/usefullinks/otherlinks.aspx'><span>Other Links</span></a></li>
</ul></div>
</li>
<li><a class='parent' href='http://nmu.ac.in/en-us/contactus.aspx'><span>Contact Us</span></a></li>
</ul></div>
                </div>
            </div>
        </div>
        <!--/header-->
        <!--banner-->
        <div class="lightBlueCenterRepeat">
            <div class="lightBlueLine topLine" style="z-index: 101; position: absolute; top: 0px;">
            </div>
            <div class="lightBlueLine bottomLine" style="z-index: 101; position: absolute; bottom: 0px;">
            </div>
            <div id="bannerLeft" class="lightBlueBannerLeft">
            </div>
            <div id="bannerRight" class="lightBlueBannerRight">
            </div>
            <div id="bannerCenter" class="lightBlueBannerCenter">
                <div class='slideshow'><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage5.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage6.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:500px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='1333' height='500' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage7.jpg' alt=''/></a></div><div style='margin:0 auto; text-align:center; width:100%; height:300px; min-height:400px;'><a href=' http:///www.nmu.ac.in' target='_self'><img width='800' height='300' src='http://nmu.ac.in/DesktopModules/Exionyte/Slideshow/2/Xeon/BannerImages/CenterImage8.jpg' alt=''/></a></div></div>

            </div>
            <div id="bannerPane" class="banner" style="display: none;">
                <div class="skinMinWidth">
                    <div class="contentPadding bannerPadding">
                        <div class="top-cols">
                            <div id="dnn_Banner_Pane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Banner_PaneLeft" class="colHalf left DNNEmptyPane">
                            </div>
                            <div id="dnn_Banner_PaneRight" class="colHalf right DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/banner-->
        <!--banner arrows-->
        <div class="skinMinWidth">
            <div id="arrows" class="arrowPositioning" style="display: none;">
                <div align="center" class="arrowLayout">
                    <div class="lightBlueArrowBGLeft">
                    </div>
                    <div class="lightBlueArrowBGRight">
                    </div>
                    <div id="Prev">
                    </div>
                    <div id="Next">
                    </div>
                </div>
            </div>
        </div>
        <!--/banner arrows-->
        <!--slider panel-->
        <div id="toppanel" class="topPanelLayout" style="display: none;">
            <div class="topContent">
                <div id="panel">
                    <div class="skinMinWidth">
                        <div class="top-cols">
                            <div id="dnn_Content_TopLeftPane" class="col1 left DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopRightPane" class="col3 right DNNEmptyPane">
                            </div>
                            <div id="dnn_Content_TopCenterPane" class="col2 DNNEmptyPane">
                            </div>
                        </div>
                        <div class="top-cols">
                            <div id="dnn_Content_TopFullPane" class="colFull DNNEmptyPane">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottomPanelSpacer">
                </div>
            </div>
        </div>
        <!--/slider panel -->
        <!--slider nav-->
        <div class="fullWidth">
            <div class="centerSliderPanel">
                <div id="toggle" class="topSliderLayout" style="display: none;">
                    <div id="close" class="open sliderOpen">
                    </div>
                    <div id="open" class="sliderClose close" style="display: none;">
                    </div>
                </div>
            </div>
        </div>
        <!--/slider nav -->
        <!--content header-->
        <div class="skinContent contentTop">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="contentPosition">
                        <span style="color: #fff;">n</span>
                        <div class="contentPositionLeft">
                            <div class="loginImage">
                            </div>
                            <div class="displayText paddingText">
                                
                                <a href="http://apps.nmu.ac.in/Login.aspx" target="_blank" class="lightBlueContentLinks">
                                    Portal Login</a>
                            </div>
                            <div class="loginImage">
                            </div>
                            <div class="displayText">
                                <a href="https://login.microsoftonline.com/" class="lightBlueContentLinks">Mail Login</a>
                                </div>
                        </div>
                        <div class="contentPositionCenter">
                            <div class="paddingText">
                                <strong>अंतरी पेटवू ज्ञानज्योत</strong></div>
                        </div>
                        <div class="contentPositionRight">
                            <div class="displayText paddingText">
                               Select Language :
                                    <div class="language-object" >
<select name="dnn$dnnLANGUAGE$selectCulture" id="dnn_dnnLANGUAGE_selectCulture" class="NormalTextBox">
	<option selected="selected" value="en-US">English (United States)</option>
	<option value="mr-IN">मराठी (भारत)</option>

</select>

</div>

                            </div>
                            <div class="lightBlueContentLinks searchText">
                                <div class="searchTextLayout">
                                    Search&nbsp;&nbsp;
                                </div>
                                <div class="searchImage">
                                    <div class="search_bg">
                                        <span id="dnn_dnnSEARCH_ClassicSearch">
  
  
  <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" onkeydown="return __dnn_KeyDown(&#39;13&#39;, &#39;javascript:__doPostBack(%27dnn$dnnSEARCH$cmdSearch%27,%27%27)&#39;, event);" />&nbsp;
  <a id="dnn_dnnSEARCH_cmdSearch" class="dnnSearchCss" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/_default/Skins/Xeon/images/sright.jpg" align="top" border="0" alt=""></img></a>
</span>


</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content header-->
		<a name="mainContent"></a>
        <!--content containers-->
        <div class="contentContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane1" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane1" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1" class="col1Home left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane1" class="col3Home right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane" class="col2Home">
                        <div class="DnnModule DnnModule- DnnModule--1">

<div class="containerPadding"></div>

<div class="overflowHidden">
    <div class="lightBlueBG">
        <div class="style1TitlePosition">
            <span id="dnn_ctr_dnnTitle_titleLabel" class="style1Title">Privacy Statement</span>



        </div>
        <div class="style1Arrow"> 
            
        </div>
         <div class="clear"></div>
    </div>
    <div class="spacer"></div>
    <div id="dnn_ctr_ContentPane" class="style1Content style1Border lightBlueContent"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon Web site and governs data collection and usage.
        By using the Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon. This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon public message boards,
        this information may be collected and used by others. Note: Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon
        does not read any of your private online communications.</p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon encourages you to review the privacy statements of Web sites
        you choose to link to from Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon so that you can understand how those
        Web sites collect, use and share your information. Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon is not responsible
        for the privacy statements or other content on Web sites outside of the Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon
        and Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon collects and uses your personal information to operate the Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon
        Web site and deliver the services you have requested. Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon also uses
        your personally identifiable information to inform you of other products or services
        available from Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon and its affiliates. Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon does not sell, rent or lease its customer lists to third parties.
        Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon, and they are required to maintain
        the confidentiality of your information.</p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon keeps track of the Web sites and pages our customers visit within
        Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon, in order to determine what Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon services are
        the most popular. This data is used to deliver customized content and advertising
        within Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon or the site; (b) protect and defend the rights or
        property of Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon; and, (c) act under exigent circumstances to protect
        the personal safety of users of Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon, or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon pages, or
        register with Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon site or services, a cookie helps Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon Web site, the information
        you previously provided can be retrieved, so you can easily use the Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon secures your personal information from unauthorized access,
        use or disclosure. Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon will occasionally update this Statement of Privacy to reflect
company and customer feedback. Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon encourages you to periodically 
        review this Statement to be informed of how Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon welcomes your comments regarding this Statement of Privacy.
        If you believe that Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon has not adhered to this Statement, please
        contact Kavayitri Bahinabai Chaudhari North Maharashtra University, Jalgaon at <a href="mailto:support@nmu.ac.in">support@nmu.ac.in</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></div>
</div>

<div class="endContentPadding"></div>

<div>
    
    
    
    
    
</div>

<div class="containerPadding"></div>

</div></div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane1Inner" class="col1Inner left DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPaneInner" class="col2Inner DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane2" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane2" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane2" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane2" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane2" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_ContentPane2" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_FullPane3" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_HalfLeftPane3" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_HalfRightPane3" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Content_LeftPane3" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_RightPane3" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Content_CenterPane3" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/content containers-->
        <!--spacer-->
        <div class="spacer">
        </div>
        <!--/spacer-->
        <!--footer line-->
        <div class="footerLineTop">
        </div>
        <!--/footer line-->
        <!--footer containers-->
        <div class="lightBlueFooterContainers">
            <div class="skinMinWidth">
                <div class="contentPadding">
                    <div class="top-cols">
                        <div id="dnn_Footer_FullPane1" class="colFull DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_HalfLeftPane" class="colHalf left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_HalfRightPane" class="colHalf right DNNEmptyPane">
                        </div>
                    </div>
                    <div class="top-cols">
                        <div id="dnn_Footer_LeftPane" class="col1 left DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_RightPane" class="col3 right DNNEmptyPane">
                        </div>
                        <div id="dnn_Footer_CenterPane" class="col2 DNNEmptyPane">
                        </div>
                    </div>
                </div>
            </div>
            <div class="lightBluefooterBottom">
                <div class="skinMinWidth">
                    <div class="contentPadding">
                        <div class="footerPosition">
                            <div class="BottomLinks footerPositionLeft contentWidth">
                                <div class="displayText">
                                    <a id="dnn_dnnPRIVACY_hypPrivacy" class="BottomLinks" rel="nofollow" href="http://nmu.ac.in/Home/tabid/77/ctl/Privacy/language/en-US/Default.aspx">Privacy Statement</a></div>
                                <div class="displayText">
                                    &nbsp;&nbsp;|&nbsp;&nbsp;</div>
                                <div class="displayText">
                                    <a href="http://nmu.ac.in/disclaimer.aspx"
                                        class="BottomLinks">Disclaimer</a></div>
                            </div>
                            <div class="footerPositionLeft bottomContainerWidth">
                                <div id="dnn_BottomPane" class="DNNEmptyPane">
                                </div>
                            </div>
                            <div class="footerPositionRight contentWidth">
                                <div class="displayText">
                                    <span id="dnn_dnnCOPYRIGHT_lblCopyright" class="BottomLinks"><a href="copyright.aspx" style="color:#fff;">Copyright</a> 2012 by KBCNMU, Jalgaon</span>
</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/footer containers-->
    </div>
</div>
<script language="javascript" type="text/javascript">
    function Showie6Message() {
        var IE6 = (navigator.userAgent.indexOf("MSIE 6") >= 0) ? true : false;
        if (IE6) {
            document.getElementById("message").style.display = "inline";
        }
    }
    Showie6Message();
</script>

        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" />
    </form>
    <script type="text/javascript">
        
        //This code is to force a refresh of browser cache
        //in case an old version of dnn.js is loaded
        //It should be removed as soon as .js versioning is added
        jQuery(document).ready(function () {
            if (navigator.userAgent.indexOf(" Chrome/") == -1) {
                if ((typeof dnnJscriptVersion === 'undefined' || dnnJscriptVersion !== "6.0.0") && typeof dnn !== 'undefined') {
                    window.location.reload(true);
                }
            }
        });
    </script>
</body>
</html>
