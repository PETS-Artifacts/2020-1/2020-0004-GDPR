
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns:o="urn:schemas-microsoft-com:office:office" __expr-val-dir="ltr" lang="en-us" dir="ltr">
<head><meta http-equiv="X-UA-Compatible" content="IE=8" /><meta name="GENERATOR" content="Microsoft SharePoint" /><meta name="progid" content="SharePoint.WebPartPage.Document" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="Expires" content="0" /><meta name="description" content="National University of Sciences and Technology (NUST) is a national institution imparting high-quality higher education at both undergraduate and postgraduate levels. NUST Offers Bachelors, Masters and Ph.D in Engineering, Medical Sciences, Management Sciences, information Technology, Natural Sciences, Social Sciences." /><meta name="keywords" content="National University of Sciences and Technology, National University of Sciences &amp; Technology,  Universities in Pakistan, Pakistan Universities, World Ranking Universities, Engineering Universities, Undergraduate Admissions, Postgraduate Admissions, Engineering, Management, Social Sciences, Information Technology, Information and Technology, Medicine, Basic Sciences, Biosciences. Applied Biosciences, Energy System, Natural Sciences, International Peace, International Peace &amp; Stability" /><title>
	
	Privacy Statement

</title><link rel="stylesheet" type="text/css" href="/Style%20Library/en-US/Themable/Core%20Styles/controls.css"/>
<link rel="stylesheet" type="text/css" href="/_layouts/1033/styles/Themable/search.css?rev=T%2Bhraxktc1A8EnaG5gGMHQ%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/_layouts/1033/styles/Themable/corev4.css?rev=p63%2BuzTeSJc22nVGNZ5zwg%3D%3D"/>
<link rel="stylesheet" type="text/css" href="/Style%20Library/layout_V2.css"/>
<link rel="stylesheet" type="text/css" href="/Style%20Library/Editor_Styles.css"/>
<link rel="stylesheet" type="text/css" href="/Style%20Library/Menu_V2.css"/>
<link rel="stylesheet" type="text/css" href="/Style%20Library/style_V2.css"/>
<link rel="stylesheet" type="text/css" href="/Style%20Library/XSLT_Rltd.css"/>

	<script type="text/javascript">
	var _fV4UI = true;
	</script>
	<script type="text/javascript">
// <![CDATA[
document.write('<script type="text/javascript" src="/_layouts/1033/init.js?rev=lEi61hsCxcBAfvfQNZA%2FsQ%3D%3D"></' + 'script>');
document.write('<script type="text/javascript" src="/ScriptResource.axd?d=4hPSwRL1q8diyfKnIbUtIq6mLr5NZOzAoAtwAoGyhB-TgqfyEZCQM8FATgPh6tb_voP3SoB11MD4vs-uGTLKZ0dibfpFPkhUeqjnmUeyY_iaXVx6X2YlKpiT6QNTzuBGNkiV6hddUWJ7_DA_RekiAItaNm41&amp;t=2e2045e2"></' + 'script>');
document.write('<script type="text/javascript" src="/_layouts/blank.js?rev=QGOYAJlouiWgFRlhHVlMKA%3D%3D"></' + 'script>');
// ]]>
</script>
<link type="text/xml" rel="alternate" href="/_vti_bin/spsdisco.aspx" /><link rel="shortcut icon" href="/siteassets/favicon.ico" type="image/vnd.microsoft.icon" /><link href="/Style Library/NUST_CSS_V2.css" rel="stylesheet" /></link>
		
	<link type="text/css" href="/SiteAssets/jquery.treeview.css" rel="stylesheet" /></link>
	<link href="/SiteAssets/redmond/jquery-ui-1.8.23.custom.css" rel="stylesheet" /></link>
	<script type="text/javascript" src="/SiteAssets/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="/SiteAssets/jquery-ui-1.8.23.custom.min.js"></script>
	<script type="text/javascript" src="/SiteAssets/jAccordion.js" ></script>
	<script src="/SiteAssets/JDialog.js" type="text/javascript"></script>
  	<script type="text/javascript" src="/SiteAssets/jquery.treeview.js" ></script>
  	<script src="/SiteAssets/jquery.innerfade.js" type="text/javascript"></script>

  	<script type="text/javascript" src="../../SiteAssets/MyPicturePreview/DialogBoxes/tinybox.js"></script>
	<link rel="stylesheet" href="../SiteAssets/MyPicturePreview/DialogBoxes/style.css" />	
	
	<script type="text/javascript" src="/SiteAssets/treemenu-newsevents.js" ></script>
	<script type="text/javascript" src="/SiteAssets/jquery.cookie.js" ></script>
	
  	

<script type="text/javascript" src="/SiteAssets/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="/SiteAssets/LeftNavigation.js" ></script>	
<style type="text/css">
	.s4-skipribbonshortcut { display:none; }

</style></head>
<body scroll="no" onload="if (typeof(_spBodyOnLoadWrapper) != 'undefined') _spBodyOnLoadWrapper();" class="v4master">
  <form name="aspnetForm" method="post" action="Privacy-Policy.aspx" id="aspnetForm">
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value="" />
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value="" />
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value="" />
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="0xE1183D6CC4F98C330136AB121806AC02E194B82488392F73D6C715754C8F44616F4BAF330199C45FDF409897E63CF574ADC9DAADAFCD9DC9A22788691EDA7B06,21 Aug 2017 03:00:40 -0000" />
<input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse" />
<input type="hidden" name="MSOSPWebPartManager_ExitingDesignMode" id="MSOSPWebPartManager_ExitingDesignMode" value="false" />
<input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value="" />
<input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value="" />
<input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value="" />
<input type="hidden" name="_wpSelected" id="_wpSelected" value="" />
<input type="hidden" name="_wzSelected" id="_wzSelected" value="" />
<input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse" />
<input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false" />
<input type="hidden" name="MSOSPWebPartManager_EndWebPartEditing" id="MSOSPWebPartManager_EndWebPartEditing" value="false" />
<input type="hidden" name="_maintainWorkspaceScrollPosition" id="_maintainWorkspaceScrollPosition" value="0" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWAgIBD2QWBAIBD2QWAgIID2QWAmYPZBYCAgEPFgIeE1ByZXZpb3VzQ29udHJvbE1vZGULKYgBTWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViQ29udHJvbHMuU1BDb250cm9sTW9kZSwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwFkAgMPZBYIAhEPZBYEZg9kFgQCAQ8WAh4HVmlzaWJsZWgWAmYPZBYEAgIPZBYGAgEPFgIfAWhkAgMPFggeE0NsaWVudE9uQ2xpY2tTY3JpcHQFZmphdmFTY3JpcHQ6Q29yZUludm9rZSgnVGFrZU9mZmxpbmVUb0NsaWVudFJlYWwnLDEsIDUzLCAnaHR0cDpcdTAwMmZcdTAwMmZudXN0LmVkdS5waycsIC0xLCAtMSwgJycsICcnKR4YQ2xpZW50T25DbGlja05hdmlnYXRlVXJsZB4oQ2xpZW50T25DbGlja1NjcmlwdENvbnRhaW5pbmdQcmVmaXhlZFVybGQeDEhpZGRlblNjcmlwdAUiVGFrZU9mZmxpbmVEaXNhYmxlZCgxLCA1MywgLTEsIC0xKWQCBQ8WAh8BaGQCAw8PFgoeCUFjY2Vzc0tleQUBLx4PQXJyb3dJbWFnZVdpZHRoAgUeEEFycm93SW1hZ2VIZWlnaHQCAx4RQXJyb3dJbWFnZU9mZnNldFhmHhFBcnJvd0ltYWdlT2Zmc2V0WQLrA2RkAgMPZBYCAgEPZBYCAgMPZBYCAgEPPCsABQEADxYCHg9TaXRlTWFwUHJvdmlkZXIFEVNQU2l0ZU1hcFByb3ZpZGVyZGQCAQ9kFgYCBQ9kFgICAQ8QFgIfAWhkFCsBAGQCBw9kFgJmD2QWAmYPFCsAA2RkZGQCCQ8PFgIfAWhkZAIvD2QWAgIHD2QWAgIBDw8WAh8BaGQWAgIDD2QWAmYPZBYCAgMPZBYCAgUPDxYEHgZIZWlnaHQbAAAAAAAAeUABAAAAHgRfIVNCAoABZBYCAgEPPCsACQEADxYEHg1QYXRoU2VwYXJhdG9yBAgeDU5ldmVyRXhwYW5kZWRnZGQCMw9kFgICAQ9kFgJmD2QWAgIBDw9kFgYeBWNsYXNzBSJtcy1zYnRhYmxlIG1zLXNidGFibGUtZXggczQtc2VhcmNoHgtjZWxscGFkZGluZwUBMB4LY2VsbHNwYWNpbmcFATBkAkkPZBYCAgIPZBYGAgcPPCsACQIADxYEHgtfIURhdGFCb3VuZGcfD2RkCBQrAA0FMTA6MCwwOjEsMDoyLDA6MywwOjQsMDo1LDA6NiwwOjcsMDo4LDA6OSwwOjEwLDA6MTEUKwACFgweBFRleHQFCEFib3V0IFVzHgVWYWx1ZQUIQWJvdXQgVXMeC05hdmlnYXRlVXJsBSIvQWJvdXRVcy9QYWdlcy9SZWN0b3ItTWVzc2FnZS5hc3B4HgdUb29sVGlwBS5OQVRJT05BTCBVTklWRVJTSVRZIA0KT0YgU0NJRU5DRVMgJiBURUNITk9MT0dZHghEYXRhUGF0aAUIL0FCT1VUVVMeCURhdGFCb3VuZGcUKwAMBSwwOjAsMDoxLDA6MiwwOjMsMDo0LDA6NSwwOjYsMDo3LDA6OCwwOjksMDoxMBQrAAIWCh8UBRBSZWN0b3IncyBNZXNzYWdlHxUFEFJlY3RvcidzIE1lc3NhZ2UfFgUiL0Fib3V0VXMvUGFnZXMvUmVjdG9yLU1lc3NhZ2UuYXNweB8YBSAvQWJvdXRVcz9TUE5hdmlnYXRpb25Ob2RlSWQ9MjA1OR8ZZ2QUKwACFgofFAUQVmlzaW9uICYgTWlzc2lvbh8VBRBWaXNpb24gJiBNaXNzaW9uHxYFIi9BYm91dFVzL1BhZ2VzL1Zpc2lvbi1NaXNzaW9uLmFzcHgfGAUiL0FCT1VUVVMvUEFHRVMvVklTSU9OLU1JU1NJT04uQVNQWB8ZZ2QUKwACFgwfFAUKTGVhZGVyc2hpcB8VBQpMZWFkZXJzaGlwHxYFJi9BYm91dFVzL0xlYWRlcnNoaXAvUGFnZXMvZGVmYXVsdC5hc3B4HxcFLU5BVElPTkFMIFVOSVZFUlNJVFkNCk9GIFNDSUVOQ0VTICYgVEVDSE5PTE9HWR8YBRMvQUJPVVRVUy9MRUFERVJTSElQHxlnZBQrAAIWDB8UBRlOZXR3b3JraW5nICYgUGFydG5lcnNoaXBzHxUFGU5ldHdvcmtpbmcgJiBQYXJ0bmVyc2hpcHMfFgUyL0Fib3V0VXMvTmV0d29ya2luZy1QYXJ0bmVyc2hpcC9QYWdlcy9kZWZhdWx0LmFzcHgfFwUuTkFUSU9OQUwgVU5JVkVSU0lUWSANCk9GIFNDSUVOQ0VTICYgVEVDSE5PTE9HWR8YBR8vQUJPVVRVUy9ORVRXT1JLSU5HLVBBUlRORVJTSElQHxlnZBQrAAIWDB8UBQ1OVVNUIElkZW50aXR5HxUFDU5VU1QgSWRlbnRpdHkfFgUkL0Fib3V0VXMvSWRlbnRpdHkvUGFnZXMvZGVmYXVsdC5hc3B4HxcFLk5BVElPTkFMIFVOSVZFUlNJVFkgDQpPRiBTQ0lFTkNFUyAmIFRFQ0hOT0xPR1kfGAURL0FCT1VUVVMvSURFTlRJVFkfGWdkFCsAAhYMHxQFD0hvbm9ycyAmIEF3YXJkcx8VBQ9Ib25vcnMgJiBBd2FyZHMfFgUzL0Fib3V0VXMvSG9ub3VyLUF3YXJkcy9QYWdlcy9Ib25vcnMtYW5kLUF3YXJkcy5hc3B4HxcFLU5BVElPTkFMIFVOSVZFUlNJVFkNCk9GIFNDSUVOQ0VTICYgVEVDSE5PTE9HWR8YBRYvQUJPVVRVUy9IT05PVVItQVdBUkRTHxlnZBQrAAIWCh8UBQ1OVVNUIENhbXB1c2VzHxUFDU5VU1QgQ2FtcHVzZXMfFgUhL0Fib3V0VXMvUGFnZXMvTlVTVC1DYW1wdXNlcy5hc3B4HxgFIS9BQk9VVFVTL1BBR0VTL05VU1QtQ0FNUFVTRVMuQVNQWB8ZZ2QUKwACFgwfFAUKQ29udGFjdCBVcx8VBQpDb250YWN0IFVzHxYFMC9BYm91dFVzL0NvbnRhY3QtVXMvUGFnZXMvQ29udGFjdC1EaXJlY3RvcnkuYXNweB8XBS5OQVRJT05BTCBVTklWRVJTSVRZIA0KT0YgU0NJRU5DRVMgJiBURUNITk9MT0dZHxgFEy9BQk9VVFVTL0NPTlRBQ1QtVVMfGWdkFCsAAhYKHxQFDU5VU1QgUmFua2luZ3MfFQUNTlVTVCBSYW5raW5ncx8WBR4vUUEvUmFua2luZy9QYWdlcy9kZWZhdWx0LmFzcHgfGAUgL0Fib3V0VXM/U1BOYXZpZ2F0aW9uTm9kZUlkPTIwMzgfGWdkFCsAAhYMHxQFE1Jlc291cmNlcyAmIE9mZmljZXMfFQUTUmVzb3VyY2VzICYgT2ZmaWNlcx8WBTgvQWJvdXRVcy9OVVNULVN0cnVjdHVyZS9QYWdlcy9SZXNvdXJjZXMtYW5kLW9mZmljZXMuYXNweB8XBS5OQVRJT05BTCBVTklWRVJTSVRZIA0KT0YgU0NJRU5DRVMgJiBURUNITk9MT0dZHxgFFy9BQk9VVFVTL05VU1QtU1RSVUNUVVJFHxlnZBQrAAIWDB8UBRBOVVNUIGF0IGEgR2xhbmNlHxUFEE5VU1QgYXQgYSBHbGFuY2UfFgU1L0Fib3V0VXMvTlVTVC1hdC1HbGFuY2UvUGFnZXMvSGlzdG9yeS1NaWxlc3RvbmVzLmFzcHgfFwUuTkFUSU9OQUwgVU5JVkVSU0lUWSANCk9GIFNDSUVOQ0VTICYgVEVDSE5PTE9HWR8YBRcvQUJPVVRVUy9OVVNULUFULUdMQU5DRR8ZZ2QUKwACFgwfFAUMSW5zdGl0dXRpb25zHxUFDEluc3RpdHV0aW9ucx8WBSAvSU5TVElUVVRJT05TL1BhZ2VzL2RlZmF1bHQuYXNweB8XBS5OQVRJT05BTCBVTklWRVJTSVRZIA0KT0YgU0NJRU5DRVMgJiBURUNITk9MT0dZHxgFDS9JTlNUSVRVVElPTlMfGWcUKwAFBQ8wOjAsMDoxLDA6MiwwOjMUKwACFgwfFAUHU2Nob29scx8VBQdTY2hvb2xzHxYFKC9JTlNUSVRVVElPTlMvU2Nob29scy9QYWdlcy9kZWZhdWx0LmFzcHgfFwUsTkFUSU9OQUwgVU5JVkVSU0lUWSBPRiBTQ0lFTkNFUyAmIFRFQ0hOT0xPR1kfGAUVL0lOU1RJVFVUSU9OUy9TQ0hPT0xTHxlnZBQrAAIWDB8UBQhDb2xsZWdlcx8VBQhDb2xsZWdlcx8WBSkvSU5TVElUVVRJT05TL0NvbGxlZ2VzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBS5OQVRJT05BTCBVTklWRVJTSVRZIA0KT0YgU0NJRU5DRVMgJiBURUNITk9MT0dZHxgFFi9JTlNUSVRVVElPTlMvQ09MTEVHRVMfGWdkFCsAAhYMHxQFB0NlbnRlcnMfFQUHQ2VudGVycx8WBSgvSU5TVElUVVRJT05TL0NlbnRlcnMvUGFnZXMvZGVmYXVsdC5hc3B4HxcFLk5BVElPTkFMIFVOSVZFUlNJVFkgDQpPRiBTQ0lFTkNFUyAmIFRFQ0hOT0xPR1kfGAUVL0lOU1RJVFVUSU9OUy9DRU5URVJTHxlnZBQrAAIWDB8UBQNDSUUfFQUDQ0lFHxYFLC9JTlNUSVRVVElPTlMvRGlyZWN0b3J0ZXMvUGFnZXMvZGVmYXVsdC5hc3B4HxcFLkNFTlRSRSBGT1IgSU5OT1ZBVElPTiAmIEVOVFJFUFJFTkVVUlNISVAgKENJRSkfGAUZL0lOU1RJVFVUSU9OUy9ESVJFQ1RPUlRFUx8ZZ2QUKwACFgwfFAUJQWNhZGVtaWNzHxUFCUFjYWRlbWljcx8WBR0vQWNhZGVtaWNzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBS5OQVRJT05BTCBVTklWRVJTSVRZIA0KT0YgU0NJRU5DRVMgJiBURUNITk9MT0dZHxgFCi9BQ0FERU1JQ1MfGWcUKwAIBRswOjAsMDoxLDA6MiwwOjMsMDo0LDA6NSwwOjYUKwACFgofFAUWVW5kZXJncmFkdWF0ZSBQcm9ncmFtcx8VBRZVbmRlcmdyYWR1YXRlIFByb2dyYW1zHxYFIy9BY2FkZW1pY3MvUGFnZXMvVW5kZXJncmFkdWF0ZS5hc3B4HxgFIy9BQ0FERU1JQ1MvUEFHRVMvVU5ERVJHUkFEVUFURS5BU1BYHxlnZBQrAAIWCh8UBRBNYXN0ZXJzIFByb2dyYW1zHxUFEE1hc3RlcnMgUHJvZ3JhbXMfFgUmL0FjYWRlbWljcy9QYWdlcy9NYXN0ZXJzLVByb2dyYW1zLmFzcHgfGAUmL0FDQURFTUlDUy9QQUdFUy9NQVNURVJTLVBST0dSQU1TLkFTUFgfGWdkFCsAAhYKHxQFDFBoRCBQcm9ncmFtcx8VBQxQaEQgUHJvZ3JhbXMfFgUeL0FjYWRlbWljcy9QYWdlcy9HcmFkdWF0ZS5hc3B4HxgFHi9BQ0FERU1JQ1MvUEFHRVMvR1JBRFVBVEUuQVNQWB8ZZ2QUKwACFgofFAUPUHJvc3BlY3R1cyAyMDE2HxUFD1Byb3NwZWN0dXMgMjAxNh8WBT4vUGFnZXMvRG93bmxvYWRfRGV0YWlscy5hc3B4P0RvY0lEPTM5JmNhdGVnb3J5PVByb3NwZWN0dXMgMjAxNh8YBSIvQWNhZGVtaWNzP1NQTmF2aWdhdGlvbk5vZGVJZD0yMDA5HxlnZBQrAAIWCh8UBRFBY2FkZW1pYyBTY2hlZHVsZR8VBRFBY2FkZW1pYyBTY2hlZHVsZR8WBU8vQWNhZGVtaWNzL0RvY3VtZW50cy9OVVNUIFJldmlzZWQgQWNhZGVtaWMgU2NoZWR1bGUgRmFsbCAyMDE2IC0gU3VtbWVyIDIwMTcucGRmHxgFIi9BY2FkZW1pY3M/U1BOYXZpZ2F0aW9uTm9kZUlkPTIwMTQfGWdkFCsAAhYKHxQFEUFjYWRlbWljIENhbGVuZGFyHxUFEUFjYWRlbWljIENhbGVuZGFyHxYFGi9TaXRlUGFnZXMvQ2FsZW5kYXJWMi5hc3B4HxgFIi9BY2FkZW1pY3M/U1BOYXZpZ2F0aW9uTm9kZUlkPTIwMTAfGWdkFCsAAhYKHxQFGkNyZWRpdCBUcmFuc2ZlciBQRyBDb3Vyc2VzHxUFGkNyZWRpdCBUcmFuc2ZlciBQRyBDb3Vyc2VzHxYFHWh0dHA6Ly9wZ2NvdXJzZXMubnVzdC5lZHUucGsvHxgFIi9BY2FkZW1pY3M/U1BOYXZpZ2F0aW9uTm9kZUlkPTIwMDcfGWdkFCsAAhYMHxQFCkFkbWlzc2lvbnMfFQUKQWRtaXNzaW9ucx8WBR8vQWRtaXNzaW9ucy9QYWdlcy9XaHktTlVTVC5hc3B4HxcFLk5BVElPTkFMIFVOSVZFUlNJVFkgDQpPRiBTQ0lFTkNFUyAmIFRFQ0hOT0xPR1kfGAULL0FETUlTU0lPTlMfGWcUKwAIBRswOjAsMDoxLDA6MiwwOjMsMDo0LDA6NSwwOjYUKwACFgofFAUKV2h5IE5VU1QgPx8VBQpXaHkgTlVTVCA/HxYFHy9BZG1pc3Npb25zL1BhZ2VzL1doeS1OVVNULmFzcHgfGAUjL0FkbWlzc2lvbnM/U1BOYXZpZ2F0aW9uTm9kZUlkPTIwMDgfGWdkFCsAAhYMHxQFDUZlZSBTdHJ1Y3R1cmUfFQUNRmVlIFN0cnVjdHVyZR8WBS4vQWRtaXNzaW9ucy9GZWUgYW5kIEZ1bmRpbmcvUGFnZXMvZGVmYXVsdC5hc3B4HxcFLk5BVElPTkFMIFVOSVZFUlNJVFkgDQpPRiBTQ0lFTkNFUyAmIFRFQ0hOT0xPR1kfGAUbL0FETUlTU0lPTlMvRkVFIEFORCBGVU5ESU5HHxlnZBQrAAIWDB8UBQxTY2hvbGFyc2hpcHMfFQUMU2Nob2xhcnNoaXBzHxYFKy9BZG1pc3Npb25zL1NjaG9sYXJzaGlwcy9QYWdlcy9kZWZhdWx0LmFzcHgfFwUuTkFUSU9OQUwgVU5JVkVSU0lUWSANCk9GIFNDSUVOQ0VTICYgVEVDSE5PTE9HWR8YBRgvQURNSVNTSU9OUy9TQ0hPTEFSU0hJUFMfGWdkFCsAAhYKHxQFCURvd25sb2Fkcx8VBQlEb3dubG9hZHMfFgUVL1BhZ2VzL0Rvd25sb2Fkcy5hc3B4HxgFIy9BZG1pc3Npb25zP1NQTmF2aWdhdGlvbk5vZGVJZD0yMDA0HxlnZBQrAAIWDB8UBQ1VbmRlcmdyYWR1YXRlHxUFDVVuZGVyZ3JhZHVhdGUfFgUtL0FkbWlzc2lvbnMvVW5kZXItR3JhZHVhdGUvUGFnZXMvZGVmYXVsdC5hc3B4HxcFLk5BVElPTkFMIFVOSVZFUlNJVFkgDQpPRiBTQ0lFTkNFUyAmIFRFQ0hOT0xPR1kfGAUaL0FETUlTU0lPTlMvVU5ERVItR1JBRFVBVEUfGWdkFCsAAhYMHxQFEE1hc3RlcnMgUHJvZ3JhbXMfFQUQTWFzdGVycyBQcm9ncmFtcx8WBS8vQWRtaXNzaW9ucy9NYXN0ZXJzLVByb2dyYW1zL1BhZ2VzL2RlZmF1bHQuYXNweB8XBS5OQVRJT05BTCBVTklWRVJTSVRZIA0KT0YgU0NJRU5DRVMgJiBURUNITk9MT0dZHxgFHC9BRE1JU1NJT05TL01BU1RFUlMtUFJPR1JBTVMfGWdkFCsAAhYMHxQFDFBoRCBQcm9ncmFtcx8VBQxQaEQgUHJvZ3JhbXMfFgUqL0FkbWlzc2lvbnMvUGhELVByb2dyYW0vUGFnZXMvZGVmYXVsdC5hc3B4HxcFLk5BVElPTkFMIFVOSVZFUlNJVFkgDQpPRiBTQ0lFTkNFUyAmIFRFQ0hOT0xPR1kfGAUXL0FETUlTU0lPTlMvUEhELVBST0dSQU0fGWdkFCsAAhYMHxQFC0NhbXB1cyBMaWZlHxUFC0NhbXB1cyBMaWZlHxYFHy9DYW1wdXMtTGlmZS9QYWdlcy9kZWZhdWx0LmFzcHgfFwUuTkFUSU9OQUwgVU5JVkVSU0lUWSANCk9GIFNDSUVOQ0VTICYgVEVDSE5PTE9HWR8YBQwvQ0FNUFVTLUxJRkUfGWcUKwAMBSwwOjAsMDoxLDA6MiwwOjMsMDo0LDA6NSwwOjYsMDo3LDA6OCwwOjksMDoxMBQrAAIWDB8UBQ9TdHVkZW50IEFmZmFpcnMfFQUPU3R1ZGVudCBBZmZhaXJzHxYFLy9DYW1wdXMtTGlmZS9TdHVkZW50LUFmZmFpcnMvUGFnZXMvZGVmYXVsdC5hc3B4HxcFLk5BVElPTkFMIFVOSVZFUlNJVFkgDQpPRiBTQ0lFTkNFUyAmIFRFQ0hOT0xPR1kfGAUcL0NBTVBVUy1MSUZFL1NUVURFTlQtQUZGQUlSUx8ZZ2QUKwACFgofFAUUTlVTVCBTVFVERU5UIEhPU1RFTFMfFQUUTlVTVCBTVFVERU5UIEhPU1RFTFMfFgUsL0NhbXB1cy1MaWZlL1BhZ2VzL0FtZW5pdGllcy1GYWNpbGl0aWVzLmFzcHgfGAUsL0NBTVBVUy1MSUZFL1BBR0VTL0FNRU5JVElFUy1GQUNJTElUSUVTLkFTUFgfGWdkFCsAAhYMHxQFF1RlY2hub2xvZ3kgU21hcnQgQ2FtcHVzHxUFF1RlY2hub2xvZ3kgU21hcnQgQ2FtcHVzHxYFLy9DYW1wdXMtTGlmZS9QYWdlcy9UZWNobm9sb2d5LVNtYXJ0LUNhbXB1cy5hc3B4HxcFF1RlY2hub2xvZ3kgU21hcnQgQ2FtcHVzHxgFLy9DQU1QVVMtTElGRS9QQUdFUy9URUNITk9MT0dZLVNNQVJULUNBTVBVUy5BU1BYHxlnZBQrAAIWDB8UBRJTdXBwb3J0ICYgU2VydmljZXMfFQUSU3VwcG9ydCAmIFNlcnZpY2VzHxYFMC9DYW1wdXMtTGlmZS9TdXBwb3J0LVNlcnZpY2VzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBS5OQVRJT05BTCBVTklWRVJTSVRZIA0KT0YgU0NJRU5DRVMgJiBURUNITk9MT0dZHxgFHS9DQU1QVVMtTElGRS9TVVBQT1JULVNFUlZJQ0VTHxlnZBQrAAIWDB8UBRVTdHVkZW50cyBBY2hpZXZlbWVudHMfFQUVU3R1ZGVudHMgQWNoaWV2ZW1lbnRzHxYFLC9DYW1wdXMtTGlmZS9QYWdlcy9TdHVlbnRzLUFjaGlldmVtZW50cy5hc3B4HxcFFFN0dWVudHMgQWNoaWV2ZW1lbnRzHxgFLC9DQU1QVVMtTElGRS9QQUdFUy9TVFVFTlRTLUFDSElFVkVNRU5UUy5BU1BYHxlnZBQrAAIWCh8UBQdHYWxsZXJ5HxUFB0dhbGxlcnkfFgUfL0NhbXB1cy1MaWZlL1BhZ2VzL0dhbGxlcnkuYXNweB8YBR8vQ0FNUFVTLUxJRkUvUEFHRVMvR0FMTEVSWS5BU1BYHxlnZBQrAAIWCh8UBQpOVVNUIFJhZGlvHxUFCk5VU1QgUmFkaW8fFgUiL0NhbXB1cy1MaWZlL1BhZ2VzL05VU1QtUmFkaW8uYXNweB8YBSIvQ0FNUFVTLUxJRkUvUEFHRVMvTlVTVC1SQURJTy5BU1BYHxlnZBQrAAIWCh8UBRxOVVNUIERheSBDYXJlIGFuZCBNb250ZXNzb3JpHxUFHE5VU1QgRGF5IENhcmUgYW5kIE1vbnRlc3NvcmkfFgUfL0NhbXB1cy1MaWZlL1BhZ2VzL2RheUNhcmUuYXNweB8YBR8vQ0FNUFVTLUxJRkUvUEFHRVMvREFZQ0FSRS5BU1BYHxlnZBQrAAIWCh8UBQlEb3dubG9hZHMfFQUJRG93bmxvYWRzHxYFIS9DYW1wdXMtTGlmZS9QYWdlcy9Eb3dubG9hZHMuYXNweB8YBSEvQ0FNUFVTLUxJRkUvUEFHRVMvRE9XTkxPQURTLkFTUFgfGWdkFCsAAhYKHxQFCVRyYW5zcG9ydB8VBQlUcmFuc3BvcnQfFgUhL0NhbXB1cy1MaWZlL1BhZ2VzL1RyYW5zcG9ydC5hc3B4HxgFIS9DQU1QVVMtTElGRS9QQUdFUy9UUkFOU1BPUlQuQVNQWB8ZZ2QUKwACFgwfFAUGU3BvcnRzHxUFBlNwb3J0cx8WBSovQ2FtcHVzLUxpZmUvbnVzdHNwb3J0cy9QYWdlcy9kZWZhdWx0LmFzcHgfFwUsTmF0aW9uYWwgVW5pdmVyc2l0eSBvZiBTY2llbmNlcyAmIFRlY2hub2xvZ3kfGAUXL0NBTVBVUy1MSUZFL05VU1RTUE9SVFMfGWdkFCsAAhYMHxQFDUludGVybmF0aW9uYWwfFQUNSW50ZXJuYXRpb25hbB8WBSEvSW50ZXJuYXRpb25hbC9QYWdlcy9kZWZhdWx0LmFzcHgfFwUuTkFUSU9OQUwgVU5JVkVSU0lUWSANCk9GIFNDSUVOQ0VTICYgVEVDSE5PTE9HWR8YBQ4vSU5URVJOQVRJT05BTB8ZZxQrAAQFCzA6MCwwOjEsMDoyFCsAAhYKHxQFKUludGVybmF0aW9uYWxpemF0aW9uICYgR2xvYmFsIFBlcnNwZWN0aXZlHxUFKUludGVybmF0aW9uYWxpemF0aW9uICYgR2xvYmFsIFBlcnNwZWN0aXZlHxYFQS9JbnRlcm5hdGlvbmFsL1BhZ2VzL0ludGVybmF0aW9uYWxpemF0aW9uLUdsb2JhbC1QZXJzcGVjdGl2ZS5hc3B4HxgFQS9JTlRFUk5BVElPTkFML1BBR0VTL0lOVEVSTkFUSU9OQUxJWkFUSU9OLUdMT0JBTC1QRVJTUEVDVElWRS5BU1BYHxlnZBQrAAIWCh8UBRlJbnRlcm5hdGlvbmFsIGNvbmZlcmVuY2VzHxUFGUludGVybmF0aW9uYWwgY29uZmVyZW5jZXMfFgUzL0ludGVybmF0aW9uYWwvUGFnZXMvSW50ZXJuYXRpb25hbC1jb25mZXJlbmNlcy5hc3B4HxgFMy9JTlRFUk5BVElPTkFML1BBR0VTL0lOVEVSTkFUSU9OQUwtQ09ORkVSRU5DRVMuQVNQWB8ZZ2QUKwACFgofFAUETU9Vcx8VBQRNT1VzHxYFHi9JbnRlcm5hdGlvbmFsL1BhZ2VzL01PVXMuYXNweB8YBR4vSU5URVJOQVRJT05BTC9QQUdFUy9NT1VTLkFTUFgfGWdkFCsAAhYMHxQFAlFBHxUFAlFBHxYFFi9RQS9QYWdlcy9kZWZhdWx0LmFzcHgfFwUWUXVhbGl0eSBBc3N1cmFuY2UgKFFBKR8YBQMvUUEfGWcUKwALBScwOjAsMDoxLDA6MiwwOjMsMDo0LDA6NSwwOjYsMDo3LDA6OCwwOjkUKwACFgwfFAUIQWJvdXQgVXMfFQUIQWJvdXQgVXMfFgUfL1FBL0Fib3V0IFFBL1BhZ2VzL2RlZmF1bHQuYXNweB8XBRZRdWFsaXR5IEFzc3VyYW5jZSAoUUEpHxgFDC9RQS9BQk9VVCBRQR8ZZ2QUKwACFgwfFAUKQXNzZXNzbWVudB8VBQpBc3Nlc3NtZW50HxYFGS9RQS9TQS9QYWdlcy9kZWZhdWx0LmFzcHgfFwUWUXVhbGl0eSBBc3N1cmFuY2UgKFFBKR8YBQYvUUEvU0EfGWdkFCsAAhYMHxQFC01lbWJlcnNoaXBzHxUFC01lbWJlcnNoaXBzHxYFIS9RQS9NZW1iZXJzaGlwL1BhZ2VzL2RlZmF1bHQuYXNweB8XBRZRdWFsaXR5IEFzc3VyYW5jZSAoUUEpHxgFDi9RQS9NRU1CRVJTSElQHxlnZBQrAAIWDB8UBQdSYW5raW5nHxUFB1JhbmtpbmcfFgUeL1FBL1JhbmtpbmcvUGFnZXMvZGVmYXVsdC5hc3B4HxcFFlF1YWxpdHkgQXNzdXJhbmNlIChRQSkfGAULL1FBL1JBTktJTkcfGWdkFCsAAhYMHxQFDUFjY3JlZGl0YXRpb24fFQUNQWNjcmVkaXRhdGlvbh8WBSQvUUEvQWNjcmVkaXRhdGlvbi9QYWdlcy9kZWZhdWx0LmFzcHgfFwUWUXVhbGl0eSBBc3N1cmFuY2UgKFFBKR8YBREvUUEvQUNDUkVESVRBVElPTh8ZZ2QUKwACFgwfFAURQ2FwYWNpdHkgQnVpbGRpbmcfFQURQ2FwYWNpdHkgQnVpbGRpbmcfFgUoL1FBL0NhcGFjaXR5LUJ1aWxkaW5nL1BhZ2VzL2RlZmF1bHQuYXNweB8XBRZRdWFsaXR5IEFzc3VyYW5jZSAoUUEpHxgFFS9RQS9DQVBBQ0lUWS1CVUlMRElORx8ZZ2QUKwACFgwfFAUSSW50IENvbGxhYm9yYXRpb25zHxUFEkludCBDb2xsYWJvcmF0aW9ucx8WBS0vUUEvSUMvUGFnZXMvSW50ZXJuYXRpb25hbC1Db2xsYWJvcmF0aW9uLmFzcHgfFwUhSW50ZXJuYXRpb25hbCBDb2xsYWJvcmF0aW9ucyAoSUMpHxgFBi9RQS9JQx8ZZ2QUKwACFgofFAUHR2FsbGVyeR8VBQdHYWxsZXJ5HxYFFi9RQS9QYWdlcy9HYWxsZXJ5LmFzcHgfGAUWL1FBL1BBR0VTL0dBTExFUlkuQVNQWB8ZZ2QUKwACFgofFAUERkFRcx8VBQRGQVFzHxYFEi9RQS9QYWdlcy9GQVEuYXNweB8YBRIvUUEvUEFHRVMvRkFRLkFTUFgfGWdkFCsAAhYMHxQFDFB1YmxpY2F0aW9ucx8VBQxQdWJsaWNhdGlvbnMfFgUjL1FBL1B1YmxpY2F0aW9ucy9QYWdlcy9kZWZhdWx0LmFzcHgfFwUWUXVhbGl0eSBBc3N1cmFuY2UgKFFBKR8YBRAvUUEvUFVCTElDQVRJT05THxlnZBQrAAIWDB8UBQhSZXNlYXJjaB8VBQhSZXNlYXJjaB8WBRwvUmVzZWFyY2gvUGFnZXMvZGVmYXVsdC5hc3B4HxcFCg0KUkVTRUFSQ0gfGAUJL1JFU0VBUkNIHxlnFCsACwUnMDowLDA6MSwwOjIsMDozLDA6NCwwOjUsMDo2LDA6NywwOjgsMDo5FCsAAhYMHxQFD1Jlc2VhcmNoIEdyb3Vwcx8VBQ9SZXNlYXJjaCBHcm91cHMfFgUrL1Jlc2VhcmNoL1Jlc2VhcmNoIEFyZWFzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBQhSRVNFQVJDSB8YBRgvUkVTRUFSQ0gvUkVTRUFSQ0ggQVJFQVMfGWdkFCsAAhYMHxQFDFImRCBQcm9qZWN0cx8VBQxSJkQgUHJvamVjdHMfFgUoL1Jlc2VhcmNoL1JuRFByb2plY3RzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBQhSRVNFQVJDSB8YBRUvUkVTRUFSQ0gvUk5EUFJPSkVDVFMfGWdkFCsAAhYMHxQFDkNvbGxhYm9yYXRpb25zHxUFDkNvbGxhYm9yYXRpb25zHxYFLi9SZXNlYXJjaC9SZXNlYXJjaCBMaW5rYWdlcy9QYWdlcy9kZWZhdWx0LmFzcHgfFwUIUkVTRUFSQ0gfGAUbL1JFU0VBUkNIL1JFU0VBUkNIIExJTktBR0VTHxlnZBQrAAIWDB8UBQhQb2xpY2llcx8VBQhQb2xpY2llcx8WBSUvUmVzZWFyY2gvUG9saWNpZXMvUGFnZXMvZGVmYXVsdC5hc3B4HxcFCg0KUkVTRUFSQ0gfGAUSL1JFU0VBUkNIL1BPTElDSUVTHxlnZBQrAAIWCh8UBRVSZXNlYXJjaCBQdWJsaWNhdGlvbnMfFQUVUmVzZWFyY2ggUHVibGljYXRpb25zHxYFJy9SZXNlYXJjaC9QYWdlcy9QYXRlbnQtb3ItUHJvZHVjdHMuYXNweB8YBScvUkVTRUFSQ0gvUEFHRVMvUEFURU5ULU9SLVBST0RVQ1RTLkFTUFgfGWdkFCsAAhYMHxQFCkNvbnRhY3QgVXMfFQUKQ29udGFjdCBVcx8WBScvUmVzZWFyY2gvQ29udGFjdC1Vcy9QYWdlcy9kZWZhdWx0LmFzcHgfFwUIUkVTRUFSQ0gfGAUUL1JFU0VBUkNIL0NPTlRBQ1QtVVMfGWdkFCsAAhYKHxQFF0RpcmVjdG9yYXRlIG9mIFJlc2VhcmNoHxUFF0RpcmVjdG9yYXRlIG9mIFJlc2VhcmNoHxYFIi9SZXNlYXJjaC9QYWdlcy9SZXNlYXJjaC1DZWxsLmFzcHgfGAUiL1JFU0VBUkNIL1BBR0VTL1JFU0VBUkNILUNFTEwuQVNQWB8ZZ2QUKwACFgofFAUhSG9EIFJlc2VhcmNoIGF0IE5VU1QgSW5zdGl0dXRpb25zHxUFIUhvRCBSZXNlYXJjaCBhdCBOVVNUIEluc3RpdHV0aW9ucx8WBSMvUmVzZWFyY2gvUGFnZXMvUmVzZWFyY2gtR3JvdXAuYXNweB8YBSMvUkVTRUFSQ0gvUEFHRVMvUkVTRUFSQ0gtR1JPVVAuQVNQWB8ZZ2QUKwACFgofFAURUmVzZWFyY2ggUHJvamVjdHMfFQURUmVzZWFyY2ggUHJvamVjdHMfFgU1L1Jlc2VhcmNoL1BhZ2VzL1Jlc2VhcmNoLUhpZ2hsaWdodHMtQWNoaWV2ZW1lbnRzLmFzcHgfGAU1L1JFU0VBUkNIL1BBR0VTL1JFU0VBUkNILUhJR0hMSUdIVFMtQUNISUVWRU1FTlRTLkFTUFgfGWdkFCsAAhYKHxQFCURvd25sb2Fkcx8VBQlEb3dubG9hZHMfFgUeL1Jlc2VhcmNoL1BhZ2VzL0Rvd25sb2Fkcy5hc3B4HxgFHi9SRVNFQVJDSC9QQUdFUy9ET1dOTE9BRFMuQVNQWB8ZZ2QUKwACFgwfFAUMTlVTVCBMaWJyYXJ5HxUFDE5VU1QgTGlicmFyeR8WBRsvTGlicmFyeS9QYWdlcy9kZWZhdWx0LmFzcHgfFwUODQpOVVNUIExJQlJBUlkfGAUIL0xJQlJBUlkfGWcUKwAKBSMwOjAsMDoxLDA6MiwwOjMsMDo0LDA6NSwwOjYsMDo3LDA6OBQrAAIWCh8UBQlEb3dubG9hZHMfFQUJRG93bmxvYWRzHxYFHS9MaWJyYXJ5L1BhZ2VzL0Rvd25sb2Fkcy5hc3B4HxgFHS9MSUJSQVJZL1BBR0VTL0RPV05MT0FEUy5BU1BYHxlnZBQrAAIWDB8UBQ1BYm91dCBMaWJyYXJ5HxUFDUFib3V0IExpYnJhcnkfFgUwL0xpYnJhcnkvQUJPVVQtTElCUkFSWS9QYWdlcy9MaWJyYXJ5V2VsQ29tZS5hc3B4HxcFDg0KTlVTVCBMSUJSQVJZHxgFFi9MSUJSQVJZL0FCT1VULUxJQlJBUlkfGWdkFCsAAhYKHxQFC1F1aWNrIGxpbmtzHxUFC1F1aWNrIGxpbmtzHxYFHy9MaWJyYXJ5L1BhZ2VzL1F1aWNrLWxpbmtzLmFzcHgfGAUfL0xJQlJBUlkvUEFHRVMvUVVJQ0stTElOS1MuQVNQWB8ZZ2QUKwACFgwfFAUOUmVzZWFyY2ggVG9vbHMfFQUOUmVzZWFyY2ggVG9vbHMfFgUeL0xpYnJhcnkvUlQvUGFnZXMvZGVmYXVsdC5hc3B4HxcFDg0KTlVTVCBMSUJSQVJZHxgFCy9MSUJSQVJZL1JUHxlnZBQrAAIWCh8UBQhDaGFwdGVycx8VBQhDaGFwdGVycx8WBRwvTGlicmFyeS9QYWdlcy9DaGFwdGVycy5hc3B4HxgFHC9MSUJSQVJZL1BBR0VTL0NIQVBURVJTLkFTUFgfGWdkFCsAAhYMHxQFBE5ld3MfFQUETmV3cx8WBSAvTGlicmFyeS9OZXdzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBQxOVVNUIExJQlJBUlkfGAUNL0xJQlJBUlkvTkVXUx8ZZ2QUKwACFgwfFAULTlVTVCBFdmVudHMfFQULTlVTVCBFdmVudHMfFgUiL0xpYnJhcnkvRXZlbnRzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBQxOVVNUIExpYnJhcnkfGAUPL0xJQlJBUlkvRVZFTlRTHxlnZBQrAAIWDB8UBRhOYXRpb25hbCBEaWdpdGFsIExpYnJhcnkfFQUYTmF0aW9uYWwgRGlnaXRhbCBMaWJyYXJ5HxYFHy9MaWJyYXJ5L25kbC9QYWdlcy9kZWZhdWx0LmFzcHgfFwUMTlVTVCBMaWJyYXJ5HxgFDC9MSUJSQVJZL05ETB8ZZ2QUKwACFgwfFAUJSGVscCBEZXNrHxUFCUhlbHAgRGVzax8WBSQvTGlicmFyeS9IZWxwRGVzay9QYWdlcy9kZWZhdWx0LmFzcHgfFwUMTlVTVCBMaWJyYXJ5HxgFES9MSUJSQVJZL0hFTFBERVNLHxlnZBQrAAIWDB8UBQZBbHVtbmkfFQUGQWx1bW5pHxYFGi9BbHVtbmkvUGFnZXMvZGVmYXVsdC5hc3B4HxcFF05VU1QgQUxVTU5JIEFTU09DSUFUSU9OHxgFBy9BTFVNTkkfGWcUKwAQBUAwOjAsMDoxLDA6MiwwOjMsMDo0LDA6NSwwOjYsMDo3LDA6OCwwOjksMDoxMCwwOjExLDA6MTIsMDoxMywwOjE0FCsAAhYMHxQFDEludHJvZHVjdGlvbh8VBQxJbnRyb2R1Y3Rpb24fFgUnL0FsdW1uaS9JbnRyb2R1Y3Rpb24vUGFnZXMvZGVmYXVsdC5hc3B4HxcFF05VU1QgQUxVTU5JIEFTU09DSUFUSU9OHxgFFC9BTFVNTkkvSU5UUk9EVUNUSU9OHxlnZBQrAAIWDB8UBQhCZW5lZml0cx8VBQhCZW5lZml0cx8WBSMvQWx1bW5pL0JlbmVmaXRzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBQxOVVNUIEFMVU1OSSAfGAUQL0FMVU1OSS9CRU5FRklUUx8ZZ2QUKwACFgwfFAUSQWx1bW5pIEFzc29jaWF0aW9uHxUFEkFsdW1uaSBBc3NvY2lhdGlvbh8WBTIvQWx1bW5pL05VU1QgQWx1bW5pIEFzc29jaWF0aW9uL1BhZ2VzL2RlZmF1bHQuYXNweB8XBRdOVVNUIEFMVU1OSSBBc3NvY2lhdGlvbh8YBR8vQUxVTU5JL05VU1QgQUxVTU5JIEFTU09DSUFUSU9OHxlnZBQrAAIWDB8UBRRTY2hvbGFyc2hpcCBQcm9ncmFtcx8VBRRTY2hvbGFyc2hpcCBQcm9ncmFtcx8WBS8vQWx1bW5pL1NjaG9sYXJzaGlwIFByb2dyYW1zL1BhZ2VzL2RlZmF1bHQuYXNweB8XBRdOVVNUIEFMVU1OSSBBc3NvY2lhdGlvbh8YBRwvQUxVTU5JL1NDSE9MQVJTSElQIFBST0dSQU1THxlnZBQrAAIWCh8UBQ5GaW5kIGFuIEFsdW1uaR8VBQ5GaW5kIGFuIEFsdW1uaR8WBSEvQWx1bW5pL1BhZ2VzL0ZpbmQtYW4tQWx1bW5pLmFzcHgfGAUhL0FMVU1OSS9QQUdFUy9GSU5ELUFOLUFMVU1OSS5BU1BYHxlnZBQrAAIWDB8UBRNBbHVtbmkgRGlzdGluY3Rpb25zHxUFE0FsdW1uaSBEaXN0aW5jdGlvbnMfFgUuL0FsdW1uaS9BbHVtbmkgRGlzdGluY3Rpb25zL1BhZ2VzL2RlZmF1bHQuYXNweB8XBRdOVVNUIEFsdW1uaSBBc3NvY2lhdGlvbh8YBRsvQUxVTU5JL0FMVU1OSSBESVNUSU5DVElPTlMfGWdkFCsAAhYKHxQFDVBob3RvIEdhbGxlcnkfFQUNUGhvdG8gR2FsbGVyeR8WBSAvQWx1bW5pL1BhZ2VzL1Bob3RvLUdhbGxlcnkuYXNweB8YBSAvQUxVTU5JL1BBR0VTL1BIT1RPLUdBTExFUlkuQVNQWB8ZZ2QUKwACFgofFAUPU3VjY2VzcyBTdG9yaWVzHxUFD1N1Y2Nlc3MgU3Rvcmllcx8WBSIvQWx1bW5pL1BhZ2VzL1N1Y2Nlc3MtU3Rvcmllcy5hc3B4HxgFIi9BTFVNTkkvUEFHRVMvU1VDQ0VTUy1TVE9SSUVTLkFTUFgfGWdkFCsAAhYKHxQFC0FsdW1uaSBDYXJkHxUFC0FsdW1uaSBDYXJkHxYFHi9BbHVtbmkvUGFnZXMvQWx1bW5pLUNhcmQuYXNweB8YBR4vQUxVTU5JL1BBR0VTL0FMVU1OSS1DQVJELkFTUFgfGWdkFCsAAhYMHxQFDEdldCBJbnZvbHZlZB8VBQxHZXQgSW52b2x2ZWQfFgUnL0FsdW1uaS9HZXQgSW52b2x2ZWQvUGFnZXMvZGVmYXVsdC5hc3B4HxcFF05VU1QgQUxVTU5JIEFzc29jaWF0aW9uHxgFFC9BTFVNTkkvR0VUIElOVk9MVkVEHxlnZBQrAAIWCh8UBQtDb252b2NhdGlvbh8VBQtDb252b2NhdGlvbh8WBR4vQWx1bW5pL1BhZ2VzL0NvbnZvY2F0aW9uLmFzcHgfGAUeL0FMVU1OSS9QQUdFUy9DT05WT0NBVElPTi5BU1BYHxlnZBQrAAIWCh8UBQ5Qcm9tb3RlIEFsdW1uaR8VBQ5Qcm9tb3RlIEFsdW1uaR8WBSEvQWx1bW5pL1BhZ2VzL1Byb21vdGUtQWx1bW5pLmFzcHgfGAUhL0FMVU1OSS9QQUdFUy9QUk9NT1RFLUFMVU1OSS5BU1BYHxlnZBQrAAIWCh8UBQpNZWRpYSBGZWVkHxUFCk1lZGlhIEZlZWQfFgUdL0FsdW1uaS9QYWdlcy9NZWRpYS1GZWVkLmFzcHgfGAUdL0FMVU1OSS9QQUdFUy9NRURJQS1GRUVELkFTUFgfGWdkFCsAAhYMHxQFBkV2ZW50cx8VBQZFdmVudHMfFgUhL0FsdW1uaS9FdmVudHMvUGFnZXMvZGVmYXVsdC5hc3B4HxcFC05VU1QgQUxVTU5JHxgFDi9BTFVNTkkvRVZFTlRTHxlnZBQrAAIWCh8UBQZBbHVtbmkfFQUGQWx1bW5pHxYFHS9BbHVtbmkvUGFnZXMvZGVmYXVsdG9sZC5hc3B4HxgFHS9BTFVNTkkvUEFHRVMvREVGQVVMVE9MRC5BU1BYHxlnZBQrAAIWDB8UBQRIb21lHxUFBEhvbWUfFgUYL3BjdG4vUGFnZXMvZGVmYXVsdC5hc3B4HxcFM1Bha2lzdGFuIENoYXB0ZXIgb2YgDQpUaGUgVGFsbG9pcmVzIE5ldHdvcmsgKFBDVE4pIB8YBQUvUENUTh8ZZxQrAAYFEzA6MCwwOjEsMDoyLDA6MywwOjQUKwACFgwfFAUKQWJvdXQgUENUTh8VBQpBYm91dCBQQ1ROHxYFIi9wY3RuL2Fib3V0cGN0bi9QYWdlcy9kZWZhdWx0LmFzcHgfFwUyUGFraXN0YW4gQ2hhcHRlciBvZg0KVGhlIFRhbGxvaXJlcyBOZXR3b3JrIChQQ1ROKSAfGAUPL1BDVE4vQUJPVVRQQ1ROHxlnZBQrAAIWDB8UBQ9BYm91dCBUYWxsb2lyZXMfFQUPQWJvdXQgVGFsbG9pcmVzHxYFLS9wY3RuL2Fib3V0bmV0d29yay9QYWdlcy93ZWxjb21lLXRvLXBjdG4uYXNweB8XBTJQYWtpc3RhbiBDaGFwdGVyIG9mDQpUaGUgVGFsbG9pcmVzIE5ldHdvcmsgKFBDVE4pIB8YBRIvUENUTi9BQk9VVE5FVFdPUksfGWdkFCsAAhYMHxQFB0pvaW4gVXMfFQUHSm9pbiBVcx8WBR8vcGN0bi9Kb2luVXMvUGFnZXMvZGVmYXVsdC5hc3B4HxcFM1Bha2lzdGFuIENoYXB0ZXIgb2YgDQpUaGUgVGFsbG9pcmVzIE5ldHdvcmsgKFBDVE4pIB8YBQwvUENUTi9KT0lOVVMfGWdkFCsAAhYMHxQFB01lbWJlcnMfFQUHTWVtYmVycx8WBSAvcGN0bi9NZW1iZXJzL1BhZ2VzL2RlZmF1bHQuYXNweB8XBTNQYWtpc3RhbiBDaGFwdGVyIG9mIA0KVGhlIFRhbGxvaXJlcyBOZXR3b3JrIChQQ1ROKSAfGAUNL1BDVE4vTUVNQkVSUx8ZZ2QUKwACFgwfFAUKQ29udGFjdCBVcx8VBQpDb250YWN0IFVzHxYFIy9wY3RuL0NvbnRhY3QgVXMvUGFnZXMvZGVmYXVsdC5hc3B4HxcFM1Bha2lzdGFuIENoYXB0ZXIgb2YgDQpUaGUgVGFsbG9pcmVzIE5ldHdvcmsgKFBDVE4pIB8YBRAvUENUTi9DT05UQUNUIFVTHxlnZBQrAAIWDB8UBQlBbmFseXRpY3MfFQUJQW5hbHl0aWNzHxYFHS9BbmFseXRpY3MvUGFnZXMvZGVmYXVsdC5hc3B4HxcFLU5BVElPTkFMIFVOSVZFUlNJVFkNCk9GIFNDSUVOQ0VTICYgVEVDSE5PTE9HWR8YBQovQU5BTFlUSUNTHxlnZGQCCw8WAh8ACysEAWQCDQ8WAh8ACysEAWQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFH2N0bDAwJFBsYWNlSG9sZGVyTWFpbiRUcmVlVmlldzEFR2N0bDAwJFBsYWNlSG9sZGVyVG9wTmF2QmFyJFBsYWNlSG9sZGVySG9yaXpvbnRhbE5hdiRUb3BOYXZpZ2F0aW9uTWVudVY0Dw9kBQRIb21lZOmD8IpxXWCdGqoutSfjfkNpNUh+" />


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaLCID = 1033;
var g_wsaSiteTemplateId = 'BLANKINTERNET#0';
var g_wsaListTemplateId = 850;
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002f", webLanguage: 1033, currentLanguage: 1033, webUIVersion:4,pageListId:"{23519bba-fa50-462f-bf6b-12ae5b240dae}",pageItemId:19, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};var dlc_fvsi = {"DefaultViews":[],"ViewUrls":[],"WebUrl":"\/"};//]]>
</script>

<script type="text/javascript">
//<![CDATA[
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {
    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fPages\u002fPrivacy-Policy.aspx");

}
//]]>
</script>
<script src="/_layouts/blank.js?rev=QGOYAJlouiWgFRlhHVlMKA%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();//]]>
</script>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="BAB98CB3" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWDwKusO/TCAKpn5bCCwLNrvW5AwK9+p7tAgLqtqmeDAL3wd7/AwKh/ZbmAgK/8OOGDALHw8XWBgKwsIusBgLbt5vvBwLuvqy1BgLTqICqDwLm36+aBgL+gIHSCabYpDId5kfRl3Jo0cjWDPQnnRs9" />
  
	
<noscript><div class='noindex'>You may be trying to access this site from a secured browser on the server. Please enable scripts and reload this page.</div></noscript>
<div id="TurnOnAccessibility" style="display:none" class="s4-notdlg noindex">
	<a id="linkTurnOnAcc" href="#" class="ms-TurnOnAcc" onclick="SetIsAccessibilityFeatureEnabled(true);UpdateAccessibilityUI();document.getElementById('linkTurnOffAcc').focus();return false;">
	Turn on more accessible mode</a>
</div>
<div id="TurnOffAccessibility" style="display:none" class="s4-notdlg noindex">
	<a id="linkTurnOffAcc" href="#" class="ms-TurnOffAcc" onclick="SetIsAccessibilityFeatureEnabled(false);UpdateAccessibilityUI();document.getElementById('linkTurnOnAcc').focus();return false;">
	Turn off more accessible mode</a>
</div>
<div class="s4-notdlg s4-skipribbonshortcut noindex">
	<a href="javascript:;" onclick="javascript:this.href='#startNavigation';" class="ms-SkiptoNavigation" accesskey="Y">
	Skip Ribbon Commands</a>
</div>
<div class="s4-notdlg noindex">
	<a href="javascript:;" onclick="javascript:this.href='#mainContent';" class="ms-SkiptoMainContent" accesskey="X">
	Skip to main content</a>
</div>
<a id="HiddenAnchor" href="javascript:;" style="display:none;"></a>

<div id="s4-ribbonrow" class="s4-pr s4-ribbonrowhidetitle">
	<div id="s4-ribboncont">
		<div class='ms-cui-ribbonTopBars'><div class='ms-cui-topBar1'></div><div class='ms-cui-topBar2'><div id='RibbonContainer-TabRowLeft' class='ms-cui-TabRowLeft ms-siteactionscontainer s4-notdlg'>
					   <span class="ms-siteactionsmenu" id="siteactiontd">
					   </span>
				
						<span class="s4-breadcrumb-anchor"><span style="height:16px;width:16px;position:relative;display:inline-block;overflow:hidden;" class="s4-clust"><a id="GlobalBreadCrumbNavPopout-anchor" onclick="CoreInvoke('callOpenBreadcrumbMenu', event, 'GlobalBreadCrumbNavPopout-anchor', 'GlobalBreadCrumbNavPopout-menu', 'GlobalBreadCrumbNavPopout-img', 's4-breadcrumb-anchor-open', 'ltr', '', false); return false;" onmouseover="" onmouseout="" title="Navigate Up" href="javascript:;" style="display:inline-block;height:16px;width:16px;"><img src="/_layouts/images/fgimg.png" alt="Navigate Up" style="border:0;position:absolute;left:-0px !important;top:-112px !important;" /></a></span></span><div class="ms-popoutMenu s4-breadcrumb-menu" id="GlobalBreadCrumbNavPopout-menu" style="display:none;">
							<div class="s4-breadcrumb-top">
								<span class="s4-breadcrumb-header">This page location is:</span>
							</div>
							
								<ul class="s4-breadcrumb">
	<li class="s4-breadcrumbRootNode"><span class="s4-breadcrumb-arrowcont"><span style="height:16px;width:16px;position:relative;display:inline-block;overflow:hidden;" class="s4-clust s4-breadcrumb"><img src="/_layouts/images/fgimg.png" alt="" style="border-width:0px;position:absolute;left:-0px !important;top:-353px !important;" /></span></span><a class="s4-breadcrumbRootNode" href="/">Home</a><ul class="s4-breadcrumbRootNode"><li class="s4-breadcrumbNode"><span class="s4-breadcrumb-arrowcont"><span style="height:16px;width:16px;position:relative;display:inline-block;overflow:hidden;" class="s4-clust s4-breadcrumb"><img src="/_layouts/images/fgimg.png" alt="" style="border-width:0px;position:absolute;left:-0px !important;top:-353px !important;" /></span></span><a class="s4-breadcrumbNode" href="http://nust.edu.pk/_layouts/listform.aspx?ListId=%7B23519BBA%2DFA50%2D462F%2DBF6B%2D12AE5B240DAE%7D&amp;PageType=0">Pages</a><ul class="s4-breadcrumbNode"><li class="s4-breadcrumbCurrentNode"><span class="s4-breadcrumb-arrowcont"><span style="height:16px;width:16px;position:relative;display:inline-block;overflow:hidden;" class="s4-clust s4-breadcrumb"><img src="/_layouts/images/fgimg.png" alt="" style="border-width:0px;position:absolute;left:-0px !important;top:-353px !important;" /></span></span><span class="s4-breadcrumbCurrentNode">Privacy-Policy</span></li></ul></li></ul></li>
</ul>
								
						</div>
						<div class="s4-die">
							
						</div>
				
				
			</div><div id='RibbonContainer-TabRowRight' class='ms-cui-TabRowRight s4-trc-container s4-notdlg'>
	

	 <a href="#" tabindex="-1" style="display:none"></a><a href="#" tabindex="-1" style="display:none"></a>
	 <div class="s4-trc-container-menu">
		 <div>
			
<a id="ctl00_IdWelcome_ExplicitLogin" class="s4-signInLink" href="http://nust.edu.pk/_layouts/Authenticate.aspx?Source=%2FPages%2FPrivacy%2DPolicy%2Easpx" style="display:inline;">Sign In</a>

			
<script type ="text/javascript">
// <![CDATA[
function OnSelectionChange(value)
{
	var today = new Date();
	var oneYear = new Date(today.getTime() + 365 * 24 * 60 * 60 * 1000);
	var url = window.location.href;
	document.cookie = "lcid=" + value + ";path=/;expires=" + oneYear.toGMTString();
	window.location.href = url;
}
// ]]>
</script>


		</div>
	</div>
	
	<span>
		<span class="s4-devdashboard">
			
		</span>
	</span>
			</div></div></div>
<script type="text/javascript">
//<![CDATA[
var g_commandUIHandlers = {"name":"CommandHandlers","attrs":{},"children":[]};
//]]>
</script>
	</div>
	<div id="notificationArea" class="s4-noti">
	</div>
	
			
<span id="ctl00_SPNavigation_ctl00_publishingConsoleV4_publishingRibbon"></span>



	
	<div id="WebPartAdderUpdatePanelContainer">
		<div id="ctl00_WebPartAdderUpdatePanel">
	
				<span id="ctl00_WebPartAdder"></span>
			
</div>
	</div>
</div>
<div id="s4-workspace">
		<div id="s4-bodyContainer">
			<div id="s4-titlerow" class="s4-pr s4-notdlg s4-titlerowhidetitle" style="display:none;">
				<div class="s4-title s4-lp">
					<div class="s4-title-inner">
						<table class="s4-titletable" cellspacing="0">
							<tbody>
								<tr>
									<td class="s4-titlelogo">
										<a id="ctl00_onetidProjectPropertyTitleGraphic" href="/">
											<img id="ctl00_onetidHeadbnnr2" name="onetidHeadbnnr0" src="/_layouts/images/siteIcon.png" alt="Home" border="0" />
										</a>
									</td>
									<td class="s4-titletext">
										<h1 name="onetidProjectPropertyTitle">
											
												<a id="ctl00_PlaceHolderSiteName_onetidProjectPropertyTitle" href="/">Home</a>
											
										</h1>
										<span id="onetidPageTitleSeparator" class="s4-nothome s4-bcsep s4-titlesep">
										<span><span style="height:11px;width:11px;position:relative;display:inline-block;overflow:hidden;"><img src="/_layouts/images/fgimg.png" alt=":" border="0" style="position:absolute;left:-0px !important;top:-585px !important;" /></span></span> </span>
										<h2>
											
										</h2>
										<div class="s4-pagedescription" tabindex="0" >
											
										</div>
									</td>
									<td class="s4-socialdata-notif">
										
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div id="s4-topheader2" class="s4-pr s4-notdlg">
					<a name="startNavigation"></a>
					<div id="s4-searcharea" class="s4-search s4-rp">
						
						<span class="s4-help">
								<span style="height:17px;width:17px;position:relative;display:inline-block;overflow:hidden;" class="s4-clust"><a href="../_catalogs/masterpage/#" id="ctl00_TopHelpLink" style="height:17px;width:17px;display:inline-block;" onclick="TopHelpButtonClick('HelpHome');return false" accesskey="6" title="Help (new window)"><img src="/_layouts/images/fgimg.png" style="left:-0px !important;top:-309px !important;position:absolute;" align="absmiddle" border="0" alt="Help (new window)" /></a></span>
						</span>
					</div>
					<div class="s4-rp s4-app">
					</div>
					<div class="s4-lp s4-toplinks">
						
					</div>
				</div>
			</div>
			<div id="s4-statusbarcontainer">
				<div id="pageStatusBar" class="s4-status-s1">
				</div>
			</div>
				
			<div id="s4-mainarea" class="s4-pr s4-widecontentarea">
				<div id="s4-leftpanel" class="s4-notdlg" style="display:none;">
					<div id="s4-leftpanel-content">
						
						
						
							
						
				<div class="ms-quicklaunchouter">
				<div class="ms-quickLaunch">
				
				
				
				<div id="ctl00_PlaceHolderLeftNavBar_QuickLaunchNavigationManager" class="ms-quicklaunch-navmgr">
	
				<div>
					
					
					
							<div id="zz1_V4QuickLaunchMenu" class="s4-ql">
		<div class="menu vertical menu-vertical">
			<ul class="root static">
				<li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Pages/Rector-Message.aspx"><span class="additional-background"><span class="menu-item-text">About Us</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" href="/AboutUs/Pages/Rector-Message.aspx"><span class="additional-background"><span class="menu-item-text">Rector&#39;s Message</span></span></a></li><li class="static"><a class="static menu-item" href="/AboutUs/Pages/Vision-Mission.aspx"><span class="additional-background"><span class="menu-item-text">Vision &amp; Mission</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Leadership/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Leadership</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Networking-Partnership/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Networking &amp; Partnerships</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Identity/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">NUST Identity</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Honour-Awards/Pages/Honors-and-Awards.aspx"><span class="additional-background"><span class="menu-item-text">Honors &amp; Awards</span></span></a></li><li class="static"><a class="static menu-item" href="/AboutUs/Pages/NUST-Campuses.aspx"><span class="additional-background"><span class="menu-item-text">NUST Campuses</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Contact-Us/Pages/Contact-Directory.aspx"><span class="additional-background"><span class="menu-item-text">Contact Us</span></span></a></li><li class="static"><a class="static menu-item" href="/QA/Ranking/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">NUST Rankings</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/NUST-Structure/Pages/Resources-and-offices.aspx"><span class="additional-background"><span class="menu-item-text">Resources &amp; Offices</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/NUST-at-Glance/Pages/History-Milestones.aspx"><span class="additional-background"><span class="menu-item-text">NUST at a Glance</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/INSTITUTIONS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Institutions</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY OF SCIENCES &amp; TECHNOLOGY" href="/INSTITUTIONS/Schools/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Schools</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/INSTITUTIONS/Colleges/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Colleges</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/INSTITUTIONS/Centers/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Centers</span></span></a></li><li class="static"><a class="static menu-item" title="CENTRE FOR INNOVATION &amp; ENTREPRENEURSHIP (CIE)" href="/INSTITUTIONS/Directortes/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">CIE</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Academics/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Academics</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" href="/Academics/Pages/Undergraduate.aspx"><span class="additional-background"><span class="menu-item-text">Undergraduate Programs</span></span></a></li><li class="static"><a class="static menu-item" href="/Academics/Pages/Masters-Programs.aspx"><span class="additional-background"><span class="menu-item-text">Masters Programs</span></span></a></li><li class="static"><a class="static menu-item" href="/Academics/Pages/Graduate.aspx"><span class="additional-background"><span class="menu-item-text">PhD Programs</span></span></a></li><li class="static"><a class="static menu-item new-window" href="/Pages/Download_Details.aspx?DocID=39&amp;category=Prospectus 2016" target="_blank"><span class="additional-background"><span class="menu-item-text">Prospectus 2016</span></span></a></li><li class="static"><a class="static menu-item new-window" href="/Academics/Documents/NUST%20Revised%20Academic%20Schedule%20Fall%202016%20-%20Summer%202017.pdf" target="_blank"><span class="additional-background"><span class="menu-item-text">Academic Schedule</span></span></a></li><li class="static"><a class="static menu-item" href="/SitePages/CalendarV2.aspx"><span class="additional-background"><span class="menu-item-text">Academic Calendar</span></span></a></li><li class="static"><a class="static menu-item new-window" href="http://pgcourses.nust.edu.pk/" target="_blank"><span class="additional-background"><span class="menu-item-text">Credit Transfer PG Courses</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Pages/Why-NUST.aspx"><span class="additional-background"><span class="menu-item-text">Admissions</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" href="/Admissions/Pages/Why-NUST.aspx"><span class="additional-background"><span class="menu-item-text">Why NUST ?</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Fee%20and%20Funding/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Fee Structure</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Scholarships/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Scholarships</span></span></a></li><li class="static"><a class="static menu-item" href="/Pages/Downloads.aspx"><span class="additional-background"><span class="menu-item-text">Downloads</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Under-Graduate/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Undergraduate</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Masters-Programs/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Masters Programs</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/PhD-Program/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">PhD Programs</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Campus-Life/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Campus Life</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Campus-Life/Student-Affairs/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Student Affairs</span></span></a></li><li class="static"><a class="static menu-item" href="/Campus-Life/Pages/Amenities-Facilities.aspx"><span class="additional-background"><span class="menu-item-text">NUST STUDENT HOSTELS</span></span></a></li><li class="static"><a class="static menu-item" title="Technology Smart Campus" href="/Campus-Life/Pages/Technology-Smart-Campus.aspx"><span class="additional-background"><span class="menu-item-text">Technology Smart Campus</span></span></a></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Campus-Life/Support-Services/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Support &amp; Services</span></span></a></li><li class="static"><a class="static menu-item" title="Stuents Achievements" href="/Campus-Life/Pages/Stuents-Achievements.aspx"><span class="additional-background"><span class="menu-item-text">Students Achievements</span></span></a></li><li class="static"><a class="static menu-item" href="/Campus-Life/Pages/Gallery.aspx"><span class="additional-background"><span class="menu-item-text">Gallery</span></span></a></li><li class="static"><a class="static menu-item" href="/Campus-Life/Pages/NUST-Radio.aspx"><span class="additional-background"><span class="menu-item-text">NUST Radio</span></span></a></li><li class="static"><a class="static menu-item" href="/Campus-Life/Pages/dayCare.aspx"><span class="additional-background"><span class="menu-item-text">NUST Day Care and Montessori</span></span></a></li><li class="static"><a class="static menu-item" href="/Campus-Life/Pages/Downloads.aspx"><span class="additional-background"><span class="menu-item-text">Downloads</span></span></a></li><li class="static"><a class="static menu-item" href="/Campus-Life/Pages/Transport.aspx"><span class="additional-background"><span class="menu-item-text">Transport</span></span></a></li><li class="static"><a class="static menu-item" title="National University of Sciences &amp; Technology" href="/Campus-Life/nustsports/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Sports</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/International/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">International</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" href="/International/Pages/Internationalization-Global-Perspective.aspx"><span class="additional-background"><span class="menu-item-text">Internationalization &amp; Global Perspective</span></span></a></li><li class="static"><a class="static menu-item" href="/International/Pages/International-conferences.aspx"><span class="additional-background"><span class="menu-item-text">International conferences</span></span></a></li><li class="static"><a class="static menu-item" href="/International/Pages/MOUs.aspx"><span class="additional-background"><span class="menu-item-text">MOUs</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="Quality Assurance (QA)" href="/QA/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">QA</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" title="Quality Assurance (QA)" href="/QA/About%20QA/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">About Us</span></span></a></li><li class="static"><a class="static menu-item" title="Quality Assurance (QA)" href="/QA/SA/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Assessment</span></span></a></li><li class="static"><a class="static menu-item" title="Quality Assurance (QA)" href="/QA/Membership/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Memberships</span></span></a></li><li class="static"><a class="static menu-item" title="Quality Assurance (QA)" href="/QA/Ranking/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Ranking</span></span></a></li><li class="static"><a class="static menu-item" title="Quality Assurance (QA)" href="/QA/Accreditation/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Accreditation</span></span></a></li><li class="static"><a class="static menu-item" title="Quality Assurance (QA)" href="/QA/Capacity-Building/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Capacity Building</span></span></a></li><li class="static"><a class="static menu-item" title="International Collaborations (IC)" href="/QA/IC/Pages/International-Collaboration.aspx"><span class="additional-background"><span class="menu-item-text">Int Collaborations</span></span></a></li><li class="static"><a class="static menu-item" href="/QA/Pages/Gallery.aspx"><span class="additional-background"><span class="menu-item-text">Gallery</span></span></a></li><li class="static"><a class="static menu-item" href="/QA/Pages/FAQ.aspx"><span class="additional-background"><span class="menu-item-text">FAQs</span></span></a></li><li class="static"><a class="static menu-item" title="Quality Assurance (QA)" href="/QA/Publications/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Publications</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="
RESEARCH" href="/Research/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Research</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" title="RESEARCH" href="/Research/Research%20Areas/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Research Groups</span></span></a></li><li class="static"><a class="static menu-item" title="RESEARCH" href="/Research/RnDProjects/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">R&amp;D Projects</span></span></a></li><li class="static"><a class="static menu-item" title="RESEARCH" href="/Research/Research%20Linkages/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Collaborations</span></span></a></li><li class="static"><a class="static menu-item" title="
RESEARCH" href="/Research/Policies/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Policies</span></span></a></li><li class="static"><a class="static menu-item" href="/Research/Pages/Patent-or-Products.aspx"><span class="additional-background"><span class="menu-item-text">Research Publications</span></span></a></li><li class="static"><a class="static menu-item" title="RESEARCH" href="/Research/Contact-Us/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Contact Us</span></span></a></li><li class="static"><a class="static menu-item" href="/Research/Pages/Research-Cell.aspx"><span class="additional-background"><span class="menu-item-text">Directorate of Research</span></span></a></li><li class="static"><a class="static menu-item" href="/Research/Pages/Research-Group.aspx"><span class="additional-background"><span class="menu-item-text">HoD Research at NUST Institutions</span></span></a></li><li class="static"><a class="static menu-item" href="/Research/Pages/Research-Highlights-Achievements.aspx"><span class="additional-background"><span class="menu-item-text">Research Projects</span></span></a></li><li class="static"><a class="static menu-item" href="/Research/Pages/Downloads.aspx"><span class="additional-background"><span class="menu-item-text">Downloads</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="
NUST LIBRARY" href="/Library/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">NUST Library</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" href="/Library/Pages/Downloads.aspx"><span class="additional-background"><span class="menu-item-text">Downloads</span></span></a></li><li class="static"><a class="static menu-item" title="
NUST LIBRARY" href="/Library/ABOUT-LIBRARY/Pages/LibraryWelCome.aspx"><span class="additional-background"><span class="menu-item-text">About Library</span></span></a></li><li class="static"><a class="static menu-item" href="/Library/Pages/Quick-links.aspx"><span class="additional-background"><span class="menu-item-text">Quick links</span></span></a></li><li class="static"><a class="static menu-item" title="
NUST LIBRARY" href="/Library/RT/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Research Tools</span></span></a></li><li class="static"><a class="static menu-item" href="/Library/Pages/Chapters.aspx"><span class="additional-background"><span class="menu-item-text">Chapters</span></span></a></li><li class="static"><a class="static menu-item" title="NUST LIBRARY" href="/Library/News/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">News</span></span></a></li><li class="static"><a class="static menu-item" title="NUST Library" href="/Library/Events/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">NUST Events</span></span></a></li><li class="static"><a class="static menu-item" title="NUST Library" href="/Library/ndl/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">National Digital Library</span></span></a></li><li class="static"><a class="static menu-item" title="NUST Library" href="/Library/HelpDesk/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Help Desk</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="NUST ALUMNI ASSOCIATION" href="/Alumni/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Alumni</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" title="NUST ALUMNI ASSOCIATION" href="/Alumni/Introduction/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Introduction</span></span></a></li><li class="static"><a class="static menu-item" title="NUST ALUMNI " href="/Alumni/Benefits/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Benefits</span></span></a></li><li class="static"><a class="static menu-item" title="NUST ALUMNI Association" href="/Alumni/NUST%20Alumni%20Association/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Alumni Association</span></span></a></li><li class="static"><a class="static menu-item" title="NUST ALUMNI Association" href="/Alumni/Scholarship%20Programs/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Scholarship Programs</span></span></a></li><li class="static"><a class="static menu-item" href="/Alumni/Pages/Find-an-Alumni.aspx"><span class="additional-background"><span class="menu-item-text">Find an Alumni</span></span></a></li><li class="static"><a class="static menu-item" title="NUST Alumni Association" href="/Alumni/Alumni%20Distinctions/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Alumni Distinctions</span></span></a></li><li class="static"><a class="static menu-item" href="/Alumni/Pages/Photo-Gallery.aspx"><span class="additional-background"><span class="menu-item-text">Photo Gallery</span></span></a></li><li class="static"><a class="static menu-item" href="/Alumni/Pages/Success-Stories.aspx"><span class="additional-background"><span class="menu-item-text">Success Stories</span></span></a></li><li class="static"><a class="static menu-item" href="/Alumni/Pages/Alumni-Card.aspx"><span class="additional-background"><span class="menu-item-text">Alumni Card</span></span></a></li><li class="static"><a class="static menu-item" title="NUST ALUMNI Association" href="/Alumni/Get%20Involved/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Get Involved</span></span></a></li><li class="static"><a class="static menu-item" href="/Alumni/Pages/Convocation.aspx"><span class="additional-background"><span class="menu-item-text">Convocation</span></span></a></li><li class="static"><a class="static menu-item" href="/Alumni/Pages/Promote-Alumni.aspx"><span class="additional-background"><span class="menu-item-text">Promote Alumni</span></span></a></li><li class="static"><a class="static menu-item" href="/Alumni/Pages/Media-Feed.aspx"><span class="additional-background"><span class="menu-item-text">Media Feed</span></span></a></li><li class="static"><a class="static menu-item" title="NUST ALUMNI" href="/Alumni/Events/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Events</span></span></a></li><li class="static"><a class="static menu-item" href="/Alumni/Pages/defaultold.aspx"><span class="additional-background"><span class="menu-item-text">Alumni</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="Pakistan Chapter of 
The Talloires Network (PCTN) " href="/pctn/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Home</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item" title="Pakistan Chapter of
The Talloires Network (PCTN) " href="/pctn/aboutpctn/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">About PCTN</span></span></a></li><li class="static"><a class="static menu-item" title="Pakistan Chapter of
The Talloires Network (PCTN) " href="/pctn/aboutnetwork/Pages/welcome-to-pctn.aspx"><span class="additional-background"><span class="menu-item-text">About Talloires</span></span></a></li><li class="static"><a class="static menu-item" title="Pakistan Chapter of 
The Talloires Network (PCTN) " href="/pctn/JoinUs/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Join Us</span></span></a></li><li class="static"><a class="static menu-item" title="Pakistan Chapter of 
The Talloires Network (PCTN) " href="/pctn/Members/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Members</span></span></a></li><li class="static"><a class="static menu-item" title="Pakistan Chapter of 
The Talloires Network (PCTN) " href="/pctn/Contact%20Us/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Contact Us</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item" title="NATIONAL UNIVERSITY
OF SCIENCES &amp; TECHNOLOGY" href="/Analytics/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Analytics</span></span></a></li>
			</ul>
		</div>
	</div>
						
				</div>
				
</div>
			
			
					
				
				
				
				
						<ul class="s4-specialNavLinkList">
							<li>
								
							</li>
							<li>
								
							</li>
						</ul>
					
				
				</div>
				</div>
						
					</div>
				</div>
				<!--Header Starts!-->
				<div class="sectionDiv headerTopDiv">
				</div>
				<div class="sectionDiv headerDiv">
					<table class="headerTbl" cellpadding="0" cellspacing="0">
						<tr>	
							<td style="vertical-align: top;">
								<div class="logoDiv">
								<a href="/" >
									<img src="/SiteCollectionImages/new-nust-logo.png" alt="National University of Science and Technology" class="logoImage"/></a>
								</div>
							</td>
							<td>
								<div class="titleText">
									NATIONAL UNIVERSITY  <br>OF SCIENCES &amp; TECHNOLOGY 
									
								</div>
								<div class="headerRightDiv">
									<div class="headerMenuDiv">
										<ul>
											<li>
												<a href="http://nust.edu.pk/Pages/Home.aspx" >
													<img src="/SiteCollectionImages/menuHomeLogo.png" alt="Nust Home"/>
													NUST Home
												</a>
											</li>
											<li>
												<a href="http://hr.nust.edu.pk" target="_blank" >
													<img src="/SiteCollectionImages/Alumni.png" alt="Nust Home"/>
													Jobs
												</a>
											</li>
											<li>
												<a href="/AboutUs/Contact-Us/Pages/Contact-Directory.aspx" >
													<img src="/SiteCollectionImages/icon-contct.png" alt="Nust Home"/>
													Contact Us
												</a>
											</li>
										</ul>
									</div>
									<div class="searchDiv">
										
												<table class="s4-wpTopTable" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td valign="top"><div WebPartID="00000000-0000-0000-0000-000000000000" HasPers="true" id="WebPartWPQ1" width="100%" OnlyForMePart="true" allowDelete="false" style="" ><div id="SRSB"> <div>
			<input name="ctl00$PlaceHolderSearchArea$ctl01$ctl00" type="hidden" id="ctl00_PlaceHolderSearchArea_ctl01_ctl00" value="http://nust.edu.pk" /><table class="ms-sbtable ms-sbtable-ex s4-search" cellpadding="0" cellspacing="0" border="0">
				<tr class="ms-sbrow">
					<td class="ms-sbcell"><input name="ctl00$PlaceHolderSearchArea$ctl01$S3031AEBB_InputKeywords" type="text" value="Search this site..." maxlength="200" id="ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_InputKeywords" accesskey="S" title="Search..." class="ms-sbplain" alt="Search..." onkeypress="javascript: return S3031AEBB_OSBEK(event);" onfocus="if (document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_ctl04').value =='0') {this.value=''; if (this.className == 's4-searchbox-QueryPrompt') this.className = ''; else this.className = this.className.replace(' s4-searchbox-QueryPrompt',''); document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_ctl04').value=1;}" onblur="if (this.value =='') {this.value='Search this site...'; if (this.className.indexOf('s4-searchbox-QueryPrompt') == -1) this.className += this.className?' s4-searchbox-QueryPrompt':'s4-searchbox-QueryPrompt'; document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_ctl04').value = '0'} else {document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_ctl04').value='1';}" style="width:170px;" /></td><td class="ms-sbgo ms-sbcell"><a id="ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_go" title="Search" href="javascript:S3031AEBB_Submit()"><img title="Search" onmouseover="this.src='\u002f_layouts\u002fimages\u002fgosearchhover15.png'" onmouseout="this.src='\u002f_layouts\u002fimages\u002fgosearch15.png'" class="srch-gosearchimg" alt="Search" src="/_layouts/images/gosearch15.png" border="0" /></a></td><td class="ms-sbLastcell"></td>
				</tr>
			</table><input name="ctl00$PlaceHolderSearchArea$ctl01$ctl04" type="hidden" id="ctl00_PlaceHolderSearchArea_ctl01_ctl04" value="0" />
		</div></div></div></td>
	</tr>
</table>
											
									</div>
								</div>
								<div class="headerMainMenu">
									
								
	<div id="zz2_TopNavigationMenuV4" class="s4-tn">
	<div class="menu horizontal menu-horizontal">
		<ul class="root static">
			<li class="static selected"><a class="static selected menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Pages/Home.aspx" accesskey="1"><span class="additional-background"><span class="menu-item-text">Home</span><span class="ms-hidden">Currently selected</span></span></a><ul class="static">
				<li class="static dynamic-children"><a class="static dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Pages/Rector-Message.aspx"><span class="additional-background"><span class="menu-item-text">About Us</span></span></a><ul class="dynamic">
					<li class="dynamic"><a class="dynamic menu-item" href="/AboutUs/Pages/Rector-Message.aspx"><span class="additional-background"><span class="menu-item-text">Rector&#39;s Message</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/AboutUs/Pages/Vision-Mission.aspx"><span class="additional-background"><span class="menu-item-text">Vision &amp; Mission</span></span></a></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Leadership/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Leadership</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" href="/AboutUs/Leadership/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Rector</span></span></a></li>
					</ul></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Networking-Partnership/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Networking &amp; Partnerships</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" title="National University of Sciences &amp; Technology" href="/AboutUs/Networking-Partnership/nio/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Internationalization</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/INSTITUTIONS/ORIC/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Industry Academia Linkages</span></span></a></li>
					</ul></li><li class="dynamic"><a class="dynamic menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Identity/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">NUST Identity</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="NATIONAL UNIVERSITY
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Honour-Awards/Pages/Honors-and-Awards.aspx"><span class="additional-background"><span class="menu-item-text">Honors &amp; Awards</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/AboutUs/Pages/NUST-Campuses.aspx"><span class="additional-background"><span class="menu-item-text">NUST Campuses</span></span></a></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/Contact-Us/Pages/Contact-Directory.aspx"><span class="additional-background"><span class="menu-item-text">Contact Us</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" href="/AboutUs/Contact-Us/Pages/Contact-Directory.aspx"><span class="additional-background"><span class="menu-item-text">Contact Directory</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/AboutUs/Contact-Us/Pages/Location-Map.aspx"><span class="additional-background"><span class="menu-item-text">Location Map</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/AboutUs/Contact-Us/Pages/Frequently-Asked-Questions.aspx"><span class="additional-background"><span class="menu-item-text">Frequently Asked Questions (UG Admissions)</span></span></a></li>
					</ul></li><li class="dynamic"><a class="dynamic menu-item" href="/QA/Ranking/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">NUST Rankings</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/NUST-Structure/Pages/Resources-and-offices.aspx"><span class="additional-background"><span class="menu-item-text">Resources &amp; Offices</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/AboutUs/NUST-at-Glance/Pages/History-Milestones.aspx"><span class="additional-background"><span class="menu-item-text">NUST at a Glance</span></span></a></li>
				</ul></li><li class="static dynamic-children"><a class="static dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/INSTITUTIONS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Institutions</span></span></a><ul class="dynamic">
					<li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY OF SCIENCES &amp; TECHNOLOGY" href="/INSTITUTIONS/Schools/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Schools</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" title="
NUST BUSINESS SCHOOL (NBS)" href="/INSTITUTIONS/Schools/NBS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">NBS</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="ATTA-UR-RAHMAN SCHOOL OF APPLIED BIOSCIENCES (ASAB)" href="/INSTITUTIONS/Schools/ASAB/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">ASAB</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="SCHOOL OF ART, DESIGN
AND ARCHITECTURE (SADA)" href="/INSTITUTIONS/Schools/SADA/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">SADA</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="School of Civil &amp; Environmental Engineering  (SCEE)
" href="/INSTITUTIONS/Schools/SCEE/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">SCEE</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="School of Chemical &amp; Materials Engineering  (SCME)" href="/INSTITUTIONS/Schools/SCME/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">SCME</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="School of Electrical Engineering 
&amp; Computer Science (SEECS)" href="/INSTITUTIONS/Schools/SEECS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">SEECS</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="School of Mechanical &amp; Manufacturing Engineering (SMME)" href="/INSTITUTIONS/Schools/SMME/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">SMME</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="NUST Institute of Peace &amp; Conflict Studies (NIPCONS)" href="/INSTITUTIONS/Schools/NIPCONS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">NIPCONS</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="School of Social Sciences &amp; Humanities (S3H)" href="/INSTITUTIONS/Schools/S3H/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">S3H</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/INSTITUTIONS/Centers/CAMP/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">SNS</span></span></a></li>
					</ul></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/INSTITUTIONS/Colleges/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Colleges</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" title="College of Aeronautical Engineering (CAE)" href="/INSTITUTIONS/Colleges/CAE/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">CAE</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="College of Electrical &amp; Mechanical Engineering  (CEME)" href="/INSTITUTIONS/Colleges/CEME/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">CEME</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="Military College of Engineering  (MCE)
" href="/INSTITUTIONS/Colleges/MCE/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">MCE</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="Military College of Signals  (MCS)" href="/INSTITUTIONS/Colleges/MCS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">MCS</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="Pakistan Navy Engineering College (PNEC)" href="/INSTITUTIONS/Colleges/PNEC/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">PNEC</span></span></a></li>
					</ul></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/INSTITUTIONS/Centers/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Centers</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" title="Center for Counseling and Career Advisory (C3A)" href="/INSTITUTIONS/Centers/C3A/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">C3A</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="U.S.-Pakistan Center for Advanced Studies in Energy (USPCAS-E)" href="/INSTITUTIONS/Centers/CES/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">USPCAS-E</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="RESEARCH CENTER FOR MODELING &amp; SIMULATION  (RCMS)" href="/INSTITUTIONS/Centers/RCMS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">RCMS</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="Centre of International Peace &amp; Stability (CIPS)" href="/INSTITUTIONS/Schools/NIPCONS/nipcons-institutions/CIPS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">CIPS</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="Research Institute for Microwave and Millimeter-wave Studies (RIMMS)" href="/INSTITUTIONS/Centers/RIMMS/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">RIMMS</span></span></a></li>
					</ul></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="CENTRE FOR INNOVATION &amp; ENTREPRENEURSHIP (CIE)" href="/INSTITUTIONS/Directortes/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">CIE</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" title="CENTRE FOR INNOVATION &amp; ENTREPRENEURSHIP (CIE)" href="/INSTITUTIONS/Directortes/AboutUs/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">About Us</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="CENTRE FOR INNOVATION &amp; ENTREPRENEURSHIP (CIE)" href="/INSTITUTIONS/Directortes/O-ric/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">ORIC</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/INSTITUTIONS/Directortes/TIC/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">TIC</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/INSTITUTIONS/Directortes/PDC/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">PDC</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/INSTITUTIONS/Directortes/CDC/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">CDC</span></span></a></li>
					</ul></li>
				</ul></li><li class="static dynamic-children"><a class="static dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Academics/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Academics</span></span></a><ul class="dynamic">
					<li class="dynamic"><a class="dynamic menu-item" href="/Academics/Pages/Undergraduate.aspx"><span class="additional-background"><span class="menu-item-text">Undergraduate Programs</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/Academics/Pages/Masters-Programs.aspx"><span class="additional-background"><span class="menu-item-text">Masters Programs</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/Academics/Pages/Graduate.aspx"><span class="additional-background"><span class="menu-item-text">PhD Programs</span></span></a></li><li class="dynamic"><a class="dynamic menu-item new-window" href="/Pages/Download_Details.aspx?DocID=39&amp;category=Prospectus 2016" target="_blank"><span class="additional-background"><span class="menu-item-text">Prospectus 2016</span></span></a></li><li class="dynamic"><a class="dynamic menu-item new-window" href="/Academics/Documents/NUST%20Revised%20Academic%20Schedule%20Fall%202016%20-%20Summer%202017.pdf" target="_blank"><span class="additional-background"><span class="menu-item-text">Academic Schedule</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/SitePages/CalendarV2.aspx"><span class="additional-background"><span class="menu-item-text">Academic Calendar</span></span></a></li><li class="dynamic"><a class="dynamic menu-item new-window" href="http://pgcourses.nust.edu.pk/" target="_blank"><span class="additional-background"><span class="menu-item-text">Credit Transfer PG Courses</span></span></a></li>
				</ul></li><li class="static dynamic-children"><a class="static dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Pages/Why-NUST.aspx"><span class="additional-background"><span class="menu-item-text">Admissions</span></span></a><ul class="dynamic">
					<li class="dynamic"><a class="dynamic menu-item" href="/Admissions/Pages/Why-NUST.aspx"><span class="additional-background"><span class="menu-item-text">Why NUST ?</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Fee%20and%20Funding/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Fee Structure</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Scholarships/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Scholarships</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/Pages/Downloads.aspx"><span class="additional-background"><span class="menu-item-text">Downloads</span></span></a></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Under-Graduate/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Undergraduate</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" href="/Admissions/Under-Graduate/Pages/Dates-to-Remember.aspx"><span class="additional-background"><span class="menu-item-text">Dates to Remember</span></span></a></li><li class="dynamic"><a class="dynamic menu-item new-window" href="https://ugadmissions.nust.edu.pk/" target="_blank"><span class="additional-background"><span class="menu-item-text">Applying for NET</span></span></a></li><li class="dynamic"><a class="dynamic menu-item" href="/Admissions/Under-Graduate/Pages/Eligibility-Criteria.aspx"><span class="additional-background"><span class="menu-item-text">Eligibility Criteria</span></span></a></li>
					</ul></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/Masters-Programs/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Masters Programs</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" href="http://www.nust.edu.pk/Admissions/Masters-Programs/Pages/Masters-Admissions.aspx"><span class="additional-background"><span class="menu-item-text">Masters Admissions</span></span></a></li>
					</ul></li><li class="dynamic dynamic-children"><a class="dynamic dynamic-children menu-item" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" href="/Admissions/PhD-Program/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">PhD Programs</span></span></a><ul class="dynamic">
						<li class="dynamic"><a class="dynamic menu-item" href="http://www.nust.edu.pk/Admissions/PhD-Program/Pages/PhD-Admissions.aspx"><span class="additional-background"><span class="menu-item-text">PhD Admissions</span></span></a></li>
					</ul></li>
				</ul></li><li class="static dynamic-children"><span class="static dynamic-children menu-item"><span class="additional-background"><span class="menu-item-text">Research</span></span></span><ul class="dynamic">
					<li class="dynamic"><a class="dynamic menu-item" href="/Research/Pages/default.aspx"><span class="additional-background"><span class="menu-item-text">Research Home</span></span></a></li><li class="dynamic"><a class="dynamic menu-item new-window" href="/Research/Publications/Pages/University-Journal.aspx" target="_blank"><span class="additional-background"><span class="menu-item-text">University Journals</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item new-window" href="http://advancement.nust.edu.pk" target="_blank"><span class="additional-background"><span class="menu-item-text">Giving to NUST</span></span></a></li><li class="static"><a class="static menu-item new-window" title="Quality Assurance" href="/QA/Pages/default.aspx" target="_blank"><span class="additional-background"><span class="menu-item-text">QA</span></span></a></li><li class="static"><a class="static menu-item" href="/Pages/Downloads.aspx"><span class="additional-background"><span class="menu-item-text">Downloads</span></span></a></li>
			</ul></li>
		</ul>
	</div>
</div>
	
								
							
								</div>
							</td>
						</tr>
					</table>
				</div>
				<!--Header Ends!-->
				<!-- Body Starts !-->
					<div class="sectionDiv">
						<div class="bannerTopDiv">
						</div>
						<div class="s4-ca s4-ca-dlgNoRibbon" id="MSO_ContentTable">
							<div class="s4-die">
								
								
								<div id="onetidPageTitleAreaFrame" class='ms-pagetitleareaframe s4-pagetitle'>
								</div>
								
								<span class="s4-die">
								
								</span>
								
									
									
									
							</div>
								
							<div class='s4-ba'><div class='ms-bodyareacell'>
								<div id="ctl00_MSO_ContentDiv">
									<a name="mainContent"></a>
									

<table class="maintTable" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table class="contentTable" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table class="contentInnerTable" border="0" align="center" cellpadding="0" cellspacing="0">
							<tr>
								<td valign="top" class="breadCrumb_outer" colspan="3">
									<div id="crumDiv" class="coloring_padding BreadCrumb">
										<span SiteMapProviders="SPSiteMapProvider,SPXmlContentMapProvider" HideInteriorRootNodes="true"><span><a title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" class="breadcrumbRootNode" href="/Pages/Home.aspx">Home</a></span><span>
												<img src="/SiteCollectionImages/collapse_V2.png" />
											</span><span class="BreadCrumbCurrent">Privacy Statement</span></span>
									</div>
								</td>
							</tr>
							<tr>
								<td valign="top" class="leftPanel">
									<div class="Layoutleftpanel">
										<table >
											<tr>
												<td id="LeftNavgiationBar" valign="top" style="width:232px;">
													
													<a href="#ctl00_PlaceHolderMain_TreeView1_SkipLink"><img alt="Skip Navigation Links." src="/WebResource.axd?d=fimzrPu0ou1ZzsoHwL19WzmP4S2b2LboNC6GNO5nUYSRgZ_-1EkggfQE_fFzRb25OU0ng2qtAwaND1nvg4GVsAFxF9M1&amp;t=636271851501517547" width="0" height="0" border="0" /></a><div id="ctl00_PlaceHolderMain_TreeView1">
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tAbout Us')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand About Us" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/AboutUs/Pages/Rector-Message.aspx" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" id="ctl00_PlaceHolderMain_TreeView1t0">About Us</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tInstitutions')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand Institutions" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/INSTITUTIONS/Pages/default.aspx" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" id="ctl00_PlaceHolderMain_TreeView1t12">Institutions</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tAcademics')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand Academics" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/Academics/Pages/default.aspx" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" id="ctl00_PlaceHolderMain_TreeView1t17">Academics</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tAdmissions')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand Admissions" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/Admissions/Pages/Why-NUST.aspx" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" id="ctl00_PlaceHolderMain_TreeView1t25">Admissions</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tCampus Life')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand Campus Life" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/Campus-Life/Pages/default.aspx" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" id="ctl00_PlaceHolderMain_TreeView1t33">Campus Life</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tInternational')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand International" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/International/Pages/default.aspx" title="NATIONAL UNIVERSITY 
OF SCIENCES &amp; TECHNOLOGY" id="ctl00_PlaceHolderMain_TreeView1t45">International</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tQA')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand QA" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/QA/Pages/default.aspx" title="Quality Assurance (QA)" id="ctl00_PlaceHolderMain_TreeView1t49">QA</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tResearch')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand Research" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/Research/Pages/default.aspx" title="
RESEARCH" id="ctl00_PlaceHolderMain_TreeView1t60">Research</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tNUST Library')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand NUST Library" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/Library/Pages/default.aspx" title="
NUST LIBRARY" id="ctl00_PlaceHolderMain_TreeView1t71">NUST Library</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tAlumni')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand Alumni" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/Alumni/Pages/default.aspx" title="NUST ALUMNI ASSOCIATION" id="ctl00_PlaceHolderMain_TreeView1t81">Alumni</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><a href="javascript:__doPostBack('ctl00$PlaceHolderMain$TreeView1','tHome')"><img src="/SiteCollectionImages/collapse_V2.png" alt="Expand Home" border="0" /></a></td><td class="leftpanel_Links " nowrap="nowrap"><a class="leftpanel_Links " href="/pctn/Pages/default.aspx" title="Pakistan Chapter of 
The Talloires Network (PCTN) " id="ctl00_PlaceHolderMain_TreeView1t97">Home</a></td>
		</tr>
	</table><table cellpadding="0" cellspacing="0">
		<tr>
			<td><img src="/WebResource.axd?d=qOeN8K5_M4Ym3O7oyRfAbU83GIZMcptnybyz-OLY7ewdI_Dls_cjD1Wi9VuG96pr1_XQ1ej9QcbuceuEZKNpUCmn8uTujfybz03N3ScvwwT0_NPI0&amp;t=636271851501517547" alt="" /></td><td class="leftpanel_Links  leftpanel_sub_Links" nowrap="nowrap"><a class="leftpanel_Links  leftpanel_sub_Links" href="/Analytics/Pages/default.aspx" title="NATIONAL UNIVERSITY
OF SCIENCES &amp; TECHNOLOGY" id="ctl00_PlaceHolderMain_TreeView1t103">Analytics</a></td>
		</tr>
	</table>
</div><a id="ctl00_PlaceHolderMain_TreeView1_SkipLink"></a>
												</td>
											</tr>
											<tr>
												<td id="LeftNavgiationBar" valign="top">
													<menu class="ms-SrvMenuUI">
	<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

	</ie:menuitem>
</menu>
												</td>
											</tr>
										</table>
									</div>	
								</td>
								<td valign="top" style="padding-right:0px; padding-left:10px;">
									&nbsp;
								</td>
								<td valign="top" style="width:768px" class="coloring_padding">
									<div style="min-height:450px">
										<table style="width: 100%;">
											<tr>
												<td class="rightpanel-title">
													Privacy Statement
												</td>
											</tr>
											<tr>
												<td class="rightpanel-normal-text">
													<div id="ctl00_PlaceHolderMain_RichHtmlField1_label" style='display:none'>Page Content</div><div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField1_label"><p> This web portal belongs to the National University of Sciences and Technology (NUST), Pakistan, and the University collects personal data with the sole objective of supporting its core functions of teaching &amp; learning, research &amp; development as well as outreach and access, administration, admissions, personal &amp; professional development and other support functions of the University.</p>
<p class="my-rteElement-ContentStyle">The web portal does not collect data that allows us to individually identify you if you are only browsing this website or using the Search function. As per routine, this site automatically receives and records information on our server logs from your browser, including your IP address, cookie information, and the page(s) requested while preserving user anonymity.</p>
<p class="my-rteElement-ContentStyle">However, in case the user shares personally identifiable data through web-based messages / applications / emails / queries etc, then NUST reserves the right to share the relevant data while remaining within our legislative rights, within or outside NUST, if sharing it is necessary for processing your query or providing you the desired service. We may also need to disclose personal data as required by law or a court order. For your ease and speed, we may prompt  your transaction / query that involves personally identifiable data, with personal data that you have already supplied to our web-portal or to other Government agencies (or non-Government entities that have been authorized to carry out specific Government services). You reserve the right to update any obsolete information. </p>
<p> To safeguard your personal data, all electronic storage and transmission of personal data are secured with appropriate security technologies. While we are responsible for data security on the nust.edu.pk domain, we are not accountable for independently managed website that may be linked to this web portal. You are requested to examine their privacy policy for privacy practices and security separately.</p>
<p> NUST students should be mindful that whenever they voluntarily disclose personal data online – for example on message boards, through e-mails, or in chat areas – that information can be collected and used by others. By posting personal data online that is publicly accessible, you may receive unsolicited messages from other parties in return. If you have any questions or concerns regarding this Statement, you should first contact the NUST Webmaster (email: webmaster@nust.edu.pk ).</p></div>
												</td>
											</tr>
											<tr>
												<td>
													
												</td>
											</tr>
											<tr>
												<td>
													
												</td>
											</tr>
										</table>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Body Ends !-->
				<!-- Footer Starts !-->
				<div class="sectionDiv footerBG">
					<div class="footerSeperator">
					</div>
					<div class="footerDiv">
						<div class="footerLeftDiv">
							<div class="socialMediaLinks" style="display:none;">
								<a href="javascript:void();">
									<img src="/SiteCollectionImages/facebook.png" alt="facebook"/>
								</a>
							</div>
							<div class="socialMediaLinks">
								<a href="https://twitter.com/Official_NUST" target="_blank">
									<img src="/SiteCollectionImages/twitter-bird_ico.png" alt="twitter"/>
								</a>
							</div>
							<div class="socialMediaLinks" style="display:none;">
								<a href="javascript:void();">
									<img src="/SiteCollectionImages/google-plus.png" alt="www.plus.google.com"/>
								</a>
							</div>
							<div class="socialMediaLinks">
								<a href="https://www.linkedin.com/edu/school?id=163093&trk=hp-feed-school-name" target="_blank">
									<img src="/SiteCollectionImages/linkedin.png" alt="www.linkedin.com"/>
								</a>
							</div>
							<div class="NustAddress">
							National University of Sciences and Technology
							H-12, Islamabad, Pakistan
							</div>
						</div>
						<div class="footerRightDiv">
							<div class="footerMenu">
								<ul>
									<li>
										<a href="/Pages/home.aspx">
											Home
										</a>
									</li>
									<li>
										<a href="/Pages/Privacy-Policy.aspx">
											Privacy Policy
										</a>
									</li>
									<li>
										<a href="/Pages/nust-disclaimer.aspx">
											Disclaimer
										</a>
									</li>
									<li>
										<a href="/AboutUs/Contact-Us/Pages/Contact-Directory.aspx">
											Contact Us
										</a>
									</li>
								</ul>
							</div>
							<div class="footerCopyRights">
								<span>
									2014 &copy; All rights reserved.
								</span>
							</div>
						</div>
					</div>
				</div>
				<!-- Footer Ends !-->
				<div class="s4-die">
					
					
				</div>
			</div>
		
	</div>
</div>


		<script type="text/javascript">
			var path=window.location.href.indexOf("authoring");
			
             if(path > -1) 
             {
				window.document.getElementById('s4-ribbonrow').style.display = 'block';  
      		 }   
      		 else
      		 {
      		 	window.document.getElementById('s4-ribbonrow').style.display = 'None';
      		 }      		 
        
        </script>
	

  
		
  
   <input type="text" name="__spText1" title="text" style="display:none;" />
   <input type="text" name="__spText2" title="text" style="display:none;" />
  <div id="ctl00_panelZone">
	<div style='display:none' id='hidZone'></div>
</div><input type='hidden' id='_wpcmWpid' name='_wpcmWpid' value='' /><input type='hidden' id='wpcmVal' name='wpcmVal' value=''/>

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);function loadMDN2() { EnsureScript('MDN.js', typeof(loadFilterFn), null); }
function loadMDN1() { ExecuteOrDelayUntilScriptLoaded(loadMDN2, 'sp.ribbon.js'); }
_spBodyOnLoadFunctionNames.push('loadMDN1');
function _spNavigateHierarchyEx(nodeDiv, dataSourceId, dataPath, url, listInContext, type, additionalQString) {
    SetAdditionalNavigateHierarchyQString(additionalQString);
    g_originalSPNavigateFunc(nodeDiv, dataSourceId, dataPath, url, listInContext, type);
}

g_originalSPNavigateFunc = _spNavigateHierarchy;
_spNavigateHierarchy = _spNavigateHierarchyEx;

function EnsureScripts(scriptInfoList, finalFunction)
{
if (scriptInfoList.length == 0)
{
finalFunction();
}
else
{
var scriptInfo = scriptInfoList.shift();
var rest = function () { EnsureScripts(scriptInfoList, finalFunction); };
var defd;
try
{
eval('defd = typeof(' + scriptInfo[1] + ');');
}
catch (e)
{
defd = 'undefined';
}
if (scriptInfo[2])
{
EnsureScript(scriptInfo[0], defd, null);
ExecuteOrDelayUntilScriptLoaded(rest, scriptInfo[0]);
}
else
{
EnsureScript(scriptInfo[0], defd, rest);
}
}
}
function PublishingRibbonUpdateRibbon()
{
var pageManager = SP.Ribbon.PageManager.get_instance();
if (pageManager)
{
pageManager.get_commandDispatcher().executeCommand('appstatechanged', null);
}
}var _fV4UI = true;
function _RegisterWebPartPageCUI()
{
    var initInfo = {editable: false,isEditMode: false,allowWebPartAdder: false,listId: "{23519bba-fa50-462f-bf6b-12ae5b240dae}",itemId: 19,recycleBinEnabled: true,enableMinorVersioning: true,enableModeration: true,forceCheckout: true,rootFolderUrl: "\u002fPages",itemPermissions:{High:16,Low:196673}};
    SP.Ribbon.WebPartComponent.registerWithPageManager(initInfo);
    var wpcomp = SP.Ribbon.WebPartComponent.get_instance();
    var hid;
    hid = document.getElementById("_wpSelected");
    if (hid != null)
    {
        var wpid = hid.value;
        if (wpid.length > 0)
        {
            var zc = document.getElementById(wpid);
            if (zc != null)
                wpcomp.selectWebPart(zc, false);
        }
    }
    hid = document.getElementById("_wzSelected");
    if (hid != null)
    {
        var wzid = hid.value;
        if (wzid.length > 0)
        {
            wpcomp.selectWebPartZone(null, wzid);
        }
    }
}
ExecuteOrDelayUntilScriptLoaded(_RegisterWebPartPageCUI, "sp.ribbon.js"); var __wpmExportWarning='This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.';var __wpmCloseProviderWarning='You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.';var __wpmDeleteWarning='You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';
ExecuteOrDelayUntilScriptLoaded(
function()
{
var initInfo = 
{
itemPermMasks: {High:16,Low:196673},
listPermMasks: {High:16,Low:196673},
listId: "23519bba-fa50-462f-bf6b-12ae5b240dae",
itemId: 19,
workflowsAssociated: true,
editable: false,
doNotShowProperties: false,
enableVersioning: true
};
SP.Ribbon.DocLibAspxPageComponent.registerWithPageManager(initInfo);
},
"sp.ribbon.js");
var g_disableCheckoutInEditMode = false;
var _spWebPermMasks = {High:16,Low:196673};//]]>
</script>
<script type="text/javascript" language="JavaScript" defer="defer">
<!--
function SearchEnsureSOD() { EnsureScript('search.js',typeof(GoSearch)); } _spBodyOnLoadFunctionNames.push('SearchEnsureSOD');function S3031AEBB_Submit() {if (document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_ctl04').value == '0') { document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_InputKeywords').value=''; }SearchEnsureSOD();GoSearch('ctl00_PlaceHolderSearchArea_ctl01_ctl04','ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_InputKeywords',null,true,false,null,'ctl00_PlaceHolderSearchArea_ctl01_ctl00',null,null,'This Site','\u002fSearch\u002fPages\u002fResults.aspx', 'This Site','This List', 'This Folder', 'Related Sites', '\u002fSearch\u002fPages\u002fresults.aspx', '', 'Please enter one or more search words.');if (document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_ctl04').value == '0') { document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_InputKeywords').value=''; }}
// -->
</script><script type="text/javascript" language="JavaScript" >
// append an onload event handler
$addHandler(window, 'load', function() {
  document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_InputKeywords').name = 'InputKeywords';
});
function S3031AEBB_OSBEK(event1) { 
if((event1.which == 10) || (event1.which == 13))
{   
S3031AEBB_Submit();return false;
}
}
{ var searchTextBox = document.getElementById('ctl00_PlaceHolderSearchArea_ctl01_S3031AEBB_InputKeywords');if (searchTextBox.className.indexOf('s4-searchbox-QueryPrompt') == -1) searchTextBox.className += searchTextBox.className?' s4-searchbox-QueryPrompt':'s4-searchbox-QueryPrompt'; }// -->
</script><script type="text/javascript">
// <![CDATA[
// ]]>
</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002fsp.core.js?rev=7ByNlH\u00252BvcgRJg\u00252BRCctdC0w\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=b6\u00252FcRx1a6orhAQ\u00252FcF\u00252B0ytQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002fsp.ui.dialog.js?rev=Tpcmo1\u00252FSu6R0yewHowDl5g\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.core.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f1033\u002fcore.js?rev=yyfs\u00252FhNVhwP3uKwHyJ6V5w\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002fsp.runtime.js?rev=IGffcZfunndj0247nOxKVg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.core.js");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002fsp.js?rev=\u00252B4ZEyA892P3T0504qi0paw\u00253D\u00253D");RegisterSodDep("sp.js", "sp.core.js");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("cui.js", "\u002f_layouts\u002fcui.js?rev=k\u00252B4HtUzT9\u00252B3mSycgD7gPaQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002finplview.js?rev=WB6Gy8a027aeNCq7koVCUg\u00253D\u00253D");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("ribbon", "\u002f_layouts\u002fsp.ribbon.js?rev=F\u00252BUEJ66rbXzSvpf7nN69wQ\u00253D\u00253D");RegisterSodDep("ribbon", "core.js");RegisterSodDep("ribbon", "sp.core.js");RegisterSodDep("ribbon", "sp.js");RegisterSodDep("ribbon", "cui.js");RegisterSodDep("ribbon", "sp.res.resx");RegisterSodDep("ribbon", "sp.runtime.js");RegisterSodDep("ribbon", "inplview");</script>
<script type="text/javascript">RegisterSod("sp.ui.policy.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EUI\u00252EPolicy\u00252EResources\u0026rev=YhBHGmUAGyJ3lAgSdE4V\u00252Fw\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mdn.js", "\u002f_layouts\u002fmdn.js?rev=gwmFFJ2\u00252FfFacqXWAqG\u00252FqKg\u00253D\u00253D");RegisterSodDep("mdn.js", "sp.core.js");RegisterSodDep("mdn.js", "sp.runtime.js");RegisterSodDep("mdn.js", "sp.js");RegisterSodDep("mdn.js", "cui.js");RegisterSodDep("mdn.js", "ribbon");RegisterSodDep("mdn.js", "sp.ui.policy.resources.resx");</script>
<script type="text/javascript">RegisterSod("sp.publishing.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EPublishing\u00252EResources\u0026rev=q6nxzZIVVXE5X1SPZIMD3A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.pub.ribbon.js", "\u002f_layouts\u002fsp.ui.pub.ribbon.js?rev=RGQSBI9Dm0E345iq\u00252FxUpHg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("msstring.js", "\u002f_layouts\u002f1033\u002fmsstring.js?rev=QtiIcPH3HV7LgVSO7vONFg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("browserScript", "\u002f_layouts\u002f1033\u002fnon_ie.js?rev=EVTj1bu32\u00252FMla6SDN\u00252FsNTA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("WPAdderClass", "\u002f_layouts\u002fwpadder.js?rev=rmznE9UTHIeAZF\u00252FGRiGNVA\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("search.js", "\u002f_layouts\u002fsearch.js?rev=BjP0\u00252BmPXUFhF7kDZmHIaVg\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSodDep("browserScript", "msstring.js");</script>
<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
function init_zz1_V4QuickLaunchMenu() {$create(SP.UI.AspMenu, null, null, null, $get('zz1_V4QuickLaunchMenu'));}ExecuteOrDelayUntilScriptLoaded(init_zz1_V4QuickLaunchMenu, 'sp.js');
function init_zz2_TopNavigationMenuV4() {$create(SP.UI.AspMenu, null, null, null, $get('zz2_TopNavigationMenuV4'));}ExecuteOrDelayUntilScriptLoaded(init_zz2_TopNavigationMenuV4, 'sp.js');
//]]>
</script>
</form>
  
	
	
	<script type="text/javascript" src="/SiteAssets/MegaMenu_V2.js" ></script>
	<script type="text/javascript">
		/*$(documnet).ready(function(){
			try{
			alert("hello");
			alert($("ul.static").children().length);
			}
			catch(e){
			alert(e);
			}
		});*/
	</script>
</body>
</html>
