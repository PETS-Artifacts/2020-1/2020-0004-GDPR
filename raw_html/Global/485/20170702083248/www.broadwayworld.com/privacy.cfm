
<!DOCTYPE HTML>
 
<html lang=en>
<head>
<title>Broadway World.com - Broadway's Premier Web Resource</title>
<meta name=description content="BroadwayWorld.com - Broadway's Premier Web
Resource"/>
<meta name=keywords content="broadway, Broadway, Theater, Broadway shows, Theatre, Broadway Message Board, Show, Review, musicals, broadway news, plays, stars, tickets, musical, stage, tony awards, national tour, actor, actress, interviews, stage shows, show tickets, theater reviews, theater news, off-broadway"/>
<meta name=apple-itunes-app content="app-id=530770227"/>
 
<meta http-equiv=Content-Type content="text/html; charset=UTF-8"/>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=viewport content="width=device-width, initial-scale=1, maximum-scale=1"/>
 
<link rel=apple-touch-icon-precomposed sizes=57x57 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-57x57.png"/>
<link rel=apple-touch-icon-precomposed sizes=114x114 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-114x114.png"/>
<link rel=apple-touch-icon-precomposed sizes=72x72 href="https://newimages.bwwstatic.com/2017/xapple-touch-icon-72x72.png.pagespeed.ic.8tKwTK3gnG.png"/>
<link rel=apple-touch-icon-precomposed sizes=144x144 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-144x144.png"/>
<link rel=apple-touch-icon-precomposed sizes=60x60 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-60x60.png"/>
<link rel=apple-touch-icon-precomposed sizes=120x120 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-120x120.png"/>
<link rel=apple-touch-icon-precomposed sizes=76x76 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-76x76.png"/>
<link rel=apple-touch-icon-precomposed sizes=152x152 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-152x152.png"/>
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-196x196.png" sizes=196x196 />
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-96x96.png" sizes=96x96 />
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-32x32.png" sizes=32x32 />
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-16x16.png" sizes=16x16 />
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-128.png" sizes=128x128 />
<meta name=application-name content="&nbsp;"/>
<meta name=msapplication-TileColor content="#FFFFFF"/>
<meta name=msapplication-TileImage content="https://newimages.bwwstatic.com/2017/mstile-144x144.png"/>
<meta name=msapplication-square70x70logo content="https://newimages.bwwstatic.com/2017/mstile-70x70.png"/>
<meta name=msapplication-square150x150logo content="https://newimages.bwwstatic.com/2017/mstile-150x150.png"/>
<meta name=msapplication-wide310x150logo content="https://newimages.bwwstatic.com/2017/mstile-310x150.png"/>
<meta name=msapplication-square310x310logo content="https://newimages.bwwstatic.com/2017/mstile-310x310.png"/>
 
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/reset.min.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/font-awesome-4.7.0/css/font-awesome.min.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/bootstrap-3.3.7-dist/css/bootstrap2.min.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/main-nonminified13b.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/shortcodes1113.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/responsive2a.min.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/dat-menu.min.css"/>
<link type="text/css" rel=stylesheet href='https://fonts.googleapis.com/css?family=Montserrat:400,700'/>
 
 
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
<!--[if lte IE 8]>
		<link type="text/css" rel="stylesheet" href="https://nav.bwwstatic.com/2017//ie-ancient.css" />
		<![endif]-->
<script>(function(){var b,c=window.deployads_ab_pct=5;b=Math.random()>c/100;var f=location.search.match(/[?&]deployads-ab=([^&]+)/);f&&2===f.length&&(b="pub"===f[1]);b&&(window.deployads=[],window.deployads.push=function(){var a=document.querySelectorAll('script[type\x3d"text/x-ab-test"]:not([data-processed])');if(a&&0<a.length){var a=a[0],e=a.innerHTML.replace(/xscript/g,"script");if("complete"!==document.readyState)document.write(e),a.setAttribute("data-processed","true");else{var d=a.parentElement;a.isProxyNode&&a.proxiedNode&&(a=a.proxiedNode);d.removeChild(a);d.innerHTML+=e;(window.adsbygoogle||[]).push({})}}return window.deployads.length});window.deployads_disabled=b;})();</script>
<script>if(window.deployads_disabled){https:window.googletag=window.googletag||{};googletag.cmd=googletag.cmd||[];window.m2hb=window.m2hb||{};m2hb.que=m2hb.que||[];m2hb.ssl='https:'==document.location.protocol;window.M2_TIMEOUT=1000;m2hb.maxRetries=20;m2hb.retries=0;m2hb.loadJS=function(src,async){m2hb.loadedJS=m2hb.loadedJS||[];m2hb.loadedJS.push(src);(function(){var script=document.createElement("script");script.async=async||true;script.type="text/javascript";script.src=(m2hb.ssl?'https:':'http:')+src;var target=document.getElementsByTagName("head")[0];target.insertBefore(script,target.firstChild);})();};if(typeof m2hb.initAdserver=='undefined'){m2hb.initAdserver=function(){if(m2hb.initAdserverSet)return;if(typeof m2hb.timeRemaining=='undefined'){m2hb.timeRemaining=M2_TIMEOUT;}if(m2hb.timeRemaining>0){m2hb.timeRemaining-=100;if(!m2hb.m2hbBidsReady){setTimeout(function(){m2hb.initAdserver();},100);return;}}if(!googletag.pubadsReady&&(m2hb.retries<=m2hb.maxRetries)){m2hb.retries++;setTimeout(function(){m2hb.initAdserver();},50);return;}m2hb.initAdserverSet=true;googletag.cmd.push(function(){m2hb.que.push(function(){m2hb.setTargetingForGPTAsync();});googletag.pubads().refresh();});};}window.initAdserver=function(){return m2hb.initAdserver();}
m2hb.loadJS('//m2d.m2.ai/m2d.broadwayworld.desktop.min.js');googletag.cmd.push(function(){googletag.pubads().disableInitialLoad();m2hb.disabledGptInitialLoad=true;});m2hb.loadJS('//www.googletagservices.com/tag/js/gpt.js');m2hb.initAdserver();var width=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;googletag.cmd.push(function(){googletag.defineSlot('/106293300/NEW300A',[[300,250],[300,600]],'div-gpt-ad-1479672818651-0').addService(googletag.pubads());googletag.defineSlot('/106293300/NEW300B',[[300,250],[300,600]],'div-gpt-ad-1479672818651-1').addService(googletag.pubads());googletag.defineSlot('/106293300/NEW300C',[[300,250],[300,600]],'div-gpt-ad-1479672818651-2').addService(googletag.pubads());if(width<=1050){googletag.defineSlot('/106293300/NEW468',[468,60],'div-gpt-ad-1479672818651-5').addService(googletag.pubads());}if(width>=1201){googletag.defineSlot('/106293300/NEW160',[160,600],'div-gpt-ad-1479672818651-160').addService(googletag.pubads());googletag.defineSlot('/106293300/NEW970',[970,90],'div-gpt-ad-1479672818651-7').addService(googletag.pubads());}else{googletag.defineSlot('/106293300/NEW728',[728,90],'div-gpt-ad-1479672818651-6').addService(googletag.pubads());}googletag.defineSlot('/106293300/NEW300D',[300,250],'div-gpt-ad-1479672818651-3').addService(googletag.pubads());googletag.defineSlot('/106293300/Inter',[1,1],'div-gpt-ad-1479672818651-8').addService(googletag.pubads());googletag.defineSlot('/106293300/New728468Bottom',[728,90],'div-gpt-ad-1479672818651-New728468Bottom').addService(googletag.pubads());googletag.defineSlot('/106293300/TCPULLDOWN-MUSICALS',[100,30],'div-gpt-ad-1479672818651-15').addService(googletag.pubads());googletag.defineSlot('/106293300/NEWTCLEFT',[130,135],'div-gpt-ad-1479672818651-10').addService(googletag.pubads());googletag.defineSlot('/106293300/Onscroll_VET_unit',[1,1],'div-gpt-ad-1479672818651-Onscroll').addService(googletag.pubads());googletag.pubads().setTargeting('Region','Broadway');googletag.pubads().enableSingleRequest();googletag.pubads().collapseEmptyDivs();googletag.enableServices();});}</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-253633-20','auto');ga('send','pageview');setTimeout("ga('send','event','adjusted bounce rate','page visit 15 seconds or more')",15000);</script>  
 
</head>
 
<body class="ot-menu-will-follow  has-dat-menu  ">
<div id=dat-menu class=effect-2><div class=dat-menu-container><div class=dat-menu-wrapper>
 
<div class=boxed>
 
<div id=header style="margin-bottom: -1px;">
 
<nav id=top-menu>
 
<div class=wrapper>
<div class="top-panel-weather right">
<span class=w-stats style="background-color: #f8f8f8;border-width: 1px;border-style: solid;border-color: black;"><a href="/newlogin.cfm">Log In</a></span>&nbsp;&nbsp;
<a href="/register.cfm"><span class=w-stats style="background-color:#b20223;" style="color:#fff;">Register Now</span></a>
</div>
<ul class="load-responsive hd-nav" rel="Jump To">
<li><a href="/"><i class="fa fa-home"></i></a></li>
<li><a href="/westend/">West End</a></li>
<li><a href="/off-broadway/">Off-Broadway</a></li>
<li class=dat-has-sub><a href="/regionalmain.cfm">United States</a>
<div class=h-sub-menu>
<ul>
<li><a href="/national-tours/">NATIONAL TOURS</a></li>
<li class=dat-has-sub><a href="#">A-D</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/birmingham/">ALABAMA - Birmingham</a></li><li><a href="/anchorage/">ALASKA - Anchorage</a></li><li><a href="/phoenix/">ARIZONA - Phoenix Metro</a></li><li><a href="/tucson/">ARIZONA - Tucson</a></li><li><a href="/little-rock/">ARKANSAS - Little Rock</a></li><li><a href="/costa-mesa/">CALIFORNIA - Costa Mesa</a></li><li><a href="/los-angeles/">CALIFORNIA - Los Angeles</a></li><li><a href="/palm-springs/">CALIFORNIA - Palm Springs</a></li><li><a href="/sacramento/">CALIFORNIA - Sacramento</a></li><li><a href="/san-diego/">CALIFORNIA - San Diego</a></li><li><a href="/san-francisco/">CALIFORNIA - San Francisco</a></li><li><a href="/santa-barbara/">CALIFORNIA - Santa Barbara</a></li><li><a href="/thousand-oaks/">CALIFORNIA - Thousand Oaks</a></li><li><a href="/denver/">COLORADO - Denver</a></li><li><a href="/connecticut/">CONNECTICUT</a></li><li><a href="/delaware/">DELAWARE</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">E-F</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/miami/">FLORIDA - Miami</a></li><li><a href="/fort-lauderdale/">FLORIDA - Ft. Lauderdale</a></li><li><a href="/ft-myers-naples/">FLORIDA - Ft. Myers/Naples</a></li><li><a href="/jacksonville/">FLORIDA - Jacksonville</a></li><li><a href="/orlando/">FLORIDA - Orlando</a></li><li><a href="/palm-beach/">FLORIDA - Palm Beach</a></li><li><a href="/st-petersburg/">FLORIDA - St. Petersburg</a></li><li><a href="/tallahassee/">FLORIDA - Tallahassee</a></li><li><a href="/tampa/">FLORIDA - Tampa</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">G-K</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/atlanta/">GEORGIA - Atlanta</a></li><li><a href="/hawaii/">HAWAII</a></li><li><a href="/boise/">IDAHO - Boise</a></li><li><a href="/chicago/">ILLINOIS - Chicago</a></li><li><a href="/indianapolis/">INDIANA - Indianpolis</a></li><li><a href="/south-bend/">INDIANA - South Bend</a></li><li><a href="/des-moines/">IOWA - Des Moines</a></li><li><a href="/wichita/">KANSAS - Wichita</a></li><li><a href="/louisville/">KENTUCKY - Louisville</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">L-M</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/new-orleans/">LOUISIANA - New Orleans</a></li><li><a href="/maine/">MAINE</a></li><li><a href="/baltimore/">MARYLAND - Baltimore</a></li><li><a href="/boston/">MASSACHUSETTS - Boston</a></li><li><a href="/detroit/">MICHIGAN - Detroit</a></li><li><a href="/minneapolis/">MINNESOTA - Minneapolis</a></li><li><a href="/st-paul/">MINNESOTA - St. Paul</a></li><li><a href="/jackson/">MISSISSIPPI - Jackson</a></li><li><a href="/kansas-city/">MISSOURI - Kansas City</a></li><li><a href="/st-louis/">MISSOURI - St. Louis</a></li><li><a href="/montana/">MONTANA</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">N</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/omaha/">NEBRASKA - Omaha</a></li><li><a href="/new-hampshire/">NEW HAMPSHIRE</a></li><li><a href="/new-jersey/">NEW JERSEY</a></li><li><a href="/las-vegas/">NEVADA - Las Vegas</a></li><li><a href="/albuquerque/">NEW MEXICO - Albuquerque</a></li><li><a href="/brooklyn/">NEW YORK - Brooklyn</a></li><li><a href="/buffalo/">NEW YORK - Buffalo</a></li><li><a href="/central-new-york/">NEW YORK - Central NY</a></li><li><a href="/long-island/">NEW YORK - Long Island</a></li><li><a href="/rockland/">NEW YORK - Rockland</a></li><li><a href="/rockland/">NEW YORK - Westchester</a></li><li><a href="/charlotte/">NORTH CAROLINA - Charlotte</a></li><li><a href="/raleigh/">NORTH CAROLINA - Raleigh</a></li><li><a href="/fargo/">NORTH DAKOTA - Fargo</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">O</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/cincinnati/">OHIO - Cincinnati</a></li><li><a href="/cleveland/">OHIO - Cleveland</a></li><li><a href="/columbus/">OHIO - Columbus</a></li><li><a href="/dayton/">OHIO - Dayton</a></li><li><a href="/oklahoma/">OKLAHOMA - Oklahoma City</a></li><li><a href="/tulsa/">OKLAHOMA - Tulsa</a></li><li><a href="/portland/">OREGON - Portland</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">P-T</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/central-pa/">PENNSYLVANIA - Central PA</a></li><li><a href="/philadelphia/">PENNSYLVANIA - Philadelphia</a></li><li><a href="/pittsburgh/">PENNSYLVANIA - Pittsburgh</a></li><li><a href="/rhode-island/">RHODE ISLAND</a></li><li><a href="/south-carolina/">SOUTH CAROLINA</a></li><li><a href="/sioux-falls/">SOUTH DAKOTA - Sioux Falls</a></li><li><a href="/memphis/">TENNESSEE - Memphis</a></li><li><a href="/nashville/">TENNESSEE - Nashville</a></li><li><a href="/austin/">TEXAS - Austin</a></li><li><a href="/dallas/">TEXAS - Dallas</a></li><li><a href="/houston/">TEXAS - Houston</a></li><li><a href="/san-antonio/">TEXAS - San Antonio</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">U-W</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/salt-lake-city/">UTAH - Salt Lake City</a></li><li><a href="/vermont/">VERMONT</a></li><li><a href="/norfolk/">VIRGINIA - Richmond/Norfolk</a></li><li><a href="/seattle/">WASHINGTON - Seattle</a></li><li><a href="/washington-dc/">WASHINGTON, DC</a></li><li><a href="/west-virginia/">WEST VIRGINIA</a></li><li><a href="/appleton/">WISCONSIN - Appleton</a></li><li><a href="/madison/">WISCONSIN - Madison</a></li><li><a href="/milwaukee/">WISCONSIN - Milwaukee</a></li><li><a href="/casper/">WYOMING - Casper</a></li> </ul>
</div>
</li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="/regionalmain.cfm">The World</a>
<div class=h-sub-menu>
<ul>
<li class=dat-has-sub><a href="#">NORTH AMERICA</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/montreal/">CANADA - MONTREAL</a></li><li><a href="/ottawa/regionalshows.cfm?ModPagespeed=off">CANADA - OTTAWA</a></li><li><a href="/toronto/">CANADA - TORONTO</a></li><li><a href="/vancouver/">CANADA - VANCOUVER</a></li><li><a href="/mexico/">MEXICO</a> </li>
<li><a href="/cuba/regionalshows.cfm?ModPagespeed=off">CUBA</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">AUSTRALIA/NEW ZEALAND</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/adelaide/">AUSTRALIA - ADELAIDE</a></li><li><a href="/brisbane/">AUSTRALIA - BRISBANE</a></li>
<li><a href="/australia-melbourne/">AUSTRALIA - MELBOURNE</a></li><li><a href="/perth/">AUSTRALIA - PERTH</a></li><li><a href="/sydney/">AUSTRALIA - SYDNEY</a></li><li><a href="/new-zealand/">NEW ZEALAND</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">UK/EUROPE</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/westend/">LONDON - WEST END</a></li><li><a href="/austria/">AUSTRIA</a></li><li><a href="/belgium/regionalshows.cfm?ModPagespeed=off">BELGIUM</a></li><li><a href="/finland/regionalshows.cfm?ModPagespeed=off">FINLAND</a></li><li><a href="/france/regionalshows.cfm?ModPagespeed=off">FRANCE</a></li><li><a href="/germany/">GERMANY</a></li><li><a href="/hungary/regionalshows.cfm?ModPagespeed=off">HUNGARY</a></li><li><a href="/ireland/">IRELAND</a></li><li><a href="/italy/">ITALY</a></li><li><a href="/luxembourg/regionalshows.cfm?ModPagespeed=off">LUXEMBOURG</a></li><li><a href="/monaco/">MONACO</a></li><li><a href="/netherlands/regionalshows.cfm?ModPagespeed=off">NETHERLANDS</a></li><li><a href="/norway/regionalshows.cfm?ModPagespeed=off">NORWAY</a></li><li><a href="/poland/regionalshows.cfm?ModPagespeed=off">POLAND</a></li><li><a href="/prague/regionalshows.cfm?ModPagespeed=off">PRAGUE</a></li><li><a href="/scotland/">SCOTLAND</a></li><li><a href="/spain/">SPAIN</a></li><li><a href="/sweden/">SWEDEN</a></li><li><a href="/switzerland/">SWITZERLAND</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">SOUTH AMERICA</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/argentina/">ARGENTINA</a></li><li><a href="/brazil/">BRAZIL</a></li><li><a href="/colombia/regionalshows.cfm?ModPagespeed=off">COLOMBIA</a></li><li><a href="/venezuela/regionalshows.cfm?ModPagespeed=off">VENEZUELA</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">ASIA</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="http://china.broadwayworld.com">CHINA</a></li><li><a href="/india/">INDIA</a></li><li><a href="/indonesia/">INDONESIA</a></li><li><a href="/japan/">JAPAN</a></li><li><a href="/malaysia/regionalshows.cfm?ModPagespeed=off">MALAYSIA</a></li><li><a href="/philippines/">PHILIPPINES</a></li><li><a href="/singapore/">SINGAPORE</a></li><li><a href="/south-korea/regionalshows.cfm?ModPagespeed=off">SOUTH KOREA</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">AFRICA/MIDDLE EAST</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/israel/">ISRAEL</a></li><li><a href="/russia/regionalshows.cfm?ModPagespeed=off">RUSSIA</a></li>
<li><a href="/south-africa/">SOUTH AFRICA</a></li><li><a href="/turkey/regionalshows.cfm?ModPagespeed=off">TURKEY</a></li>
</ul>
</div>
</li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="/regionalmain.cfm">Entertainment</a>
<div class=h-sub-menu>
<ul>
<li><a href="/bwwtv/index.cfm">TV/MOVIES</a></li>
<li><a href="/cabaret/">CABARET</a></li>
<li><a href="/bwwmusic/index.cfm">MUSIC</a></li>
<li><a href="/bwwbooks/index.cfm">BOOKS</a></li>
<li><a href="/bwwclassical/index.cfm">CLASSICAL MUSIC</a></li>
<li><a href="/bwwdance/index.cfm">DANCE</a></li>
<li><a href="/bwwopera/index.cfm">OPERA</a></li>
<li><a href="/bwwfitness/index.cfm">FITNESS</a></li>
<li><a href="/food-wine/index.cfm">FOOD + WINE</a></li>
</ul>
</div>
</li>
</ul>
</div>
 
</nav>
 
<div class=wrapper>
<div class=header-panels>
 
<div class=header-logo>
<a href="/">
<img class=image src="https://newimages.bwwstatic.com/2017/broadwayworld-new-nonretina-2.png" data-ot-retina="https://newimages.bwwstatic.com/2017/broadwayworld-new-retina.png" alt=BroadwayWorld style="height: 92px;max-width: 153px;"/>
</a>
 
</div>
 
 
<div class=header-pob align=right>
<div class=ad-desktop>
 
 
<div class=ad-tag data-ad-name="sortable new970" data-ad-size=970x90>
<script type="text/x-ab-test">
<!-- /106293300/NEW970 -->
<div id='div-gpt-ad-1479672818651-7' style='height:90px; width:970px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479672818651-7'); });
</xscript>
</div>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script>
</div>
<div class=ad-tab>
<center>
 
<div class=ad-tag data-ad-name="sortable new728 nonresponsive" data-ad-size=728x90>
<script type="text/x-ab-test">
<!-- /106293300/NEW728 -->
<div id='div-gpt-ad-1479672818651-6' style='height:90px; width:728px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479672818651-6'); });
</xscript>
</div>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script>
</center>
</div>
<div class=ad-mob>
<center>
 
<div class=ad-tag data-ad-name="sortable new468" data-ad-size=468x60>
<script type="text/x-ab-test">
<!-- /106293300/NEW468 -->
<div id='div-gpt-ad-1479672818651-5' style='height:60px; width:468px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479672818651-5'); });
</xscript>
</div>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script> </center>
</div>
<div class=ad-mob-sm>
<center>
 
<div class=ad-tag data-ad-name="sortable new320" data-ad-size=320x50>
<script type="text/x-ab-test">
<!-- /106293300/NEW320 -->
<div id='div-gpt-ad-1479672818651-4' style='height:50px; width:320px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479672818651-4'); });
</xscript>
</div>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script>
</center>
</div>
 
</div>
</div>
 
</div>
 
<nav id=main-menu>
<a href="#dat-menu" class=dat-menu-button><i class="fa fa-bars"></i>Show Menu</a>
<div class=main-menu-placeholder style="background-color: #c31f2a;border-top:5px #343233;border-style: solid;height:40px;">
 
<div class=wrapper>
<div class="search-nav right" style="margin-top:-10px; width: 280px;">
<div class=social-nav>
<a href="https://www.facebook.com/BroadwayWorld" target=_new><i class="fa fa-facebook"></i></a>
<a href="https://www.twitter.com/BroadwayWorld" target=_new><i class="fa fa-twitter"></i></a>
<a href="https://www.instagram.com/officialbroadwayworld/" target=_new><i class="fa fa-instagram"></i></a>
<a href="http://broadwayworld.tumblr.com/"><i class="fa fa-tumblr"></i></a>
<a href="/article/BroadwayWorld-is-Everywhere--Interact-with-Us-With-Apps-Social-Media-Today-20151106"><i class="fa fa-apple"></i></a>
</div>
<form action="/search/" id=cse-search-box onSubmit="if($F('search-text')=='Enter Search' || $F('search-text')=='') {alert('You must enter some search criteria'); return false;}">
<input type=text name=q autocomplete=off id=search-text results=0 value="" placeholder=Search style="font-family: 'Montserrat';"/><input type=hidden name=cx value=003281041504712504013:ma8kxnaa1hu /><input type=hidden name=cof value=FORID:11 /><input type=hidden name=ie value=UTF-8 /><input type=hidden class=radio name="search_type[]" id=all value=site checked />
<button type=submit style="float: right;"><i class="fa fa-search"></i></button>
</form>
</div>
<ul class=load-responsive rel="Main Menu">
<li><a href="#" data-ot-css="border-color: orange;" style="padding-top:10px;"><span style="padding-top:10px;">Sections</span></a>
<ul class=sub-menu>
<li><a href="/newsroom/">Latest News</a></li>
<li><a href="/bwidb/">BWW Database</a></li>
<li> <div id=div-gpt-ad-1479672818651-13 style='width:100px;height:30px;float:right;padding-right:4px;padding-top:4px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-13');});</script>
</div>
<a href="/cdsbooksdvds.cfm">CDs/Books/DVDs</a></li>
<li><div id=div-gpt-ad-1479672818651-14 style='width:100px;height:30px;float:right;padding-right:4px;padding-top:4px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-14');});</script>
</div><a href="/grosses.cfm">Grosses - <font color=green>Updated 6/25 </font>
</a></li>
<li><a href="/interviews.cfm">Interviews</a></li>
<li><a href="/gallerynew.cfm">Photos</a></li>
<li><a href="/topic/Podcasts">Podcasts</a></li>
<li><A href="/reviews.cfm">Reviews</A></li>
<li><a href="/tonyawards.cfm">Tony Awards</a>
</ul>
</li>
<li class=has-ot-mega-menu><a href="#"><span>Shows</span></a>
<ul class=ot-mega-menu>
<li>
<div class=widget-split>
 
<div class=widget>
<ul class=menu style="padding-top:0px;">
<li><a href="/shows/shows.php?page=shows">Broadway Shows</a></li>
<li>
<a href="/shows/shows-musicals.php">Broadway Musicals</a>
</li>
<div style='background-color:white;height:35px;width:110px;'>
 
<div id=div-gpt-ad-1479672818651-15 style='height:30px; width:100px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-15');});</script>
</div>
</div>
<li>
<a href="/shows/shows-plays.php">Broadway Plays</a>
</li>
<div id=div-gpt-ad-1479672818651-16 style='width:100px;height:30px;float:right;padding-right:4px;padding-top:4px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-16');});</script>
</div>
<li><a href="/shows/shows.php?page=oshows">Off-Broadway</a></li>
<li><a href="/cabaret/">Cabarets/Concerts</a></li>
<li><a href="/pickashowregional.cfm?ModPagespeed=off">Search by Zip Code</a></li>
<li><a href="/stage-to-screen.cfm">Stage to Screen</a></li>
<li><a href="/shows/shows.php?page=tour">Tours</a></li>
<li><a href="/shows/shows.php?page=westend">West End</a></li>
</ul>
 
</div>
</div>
<div class=widget-split>
 
<div class=widget>
<h4>Hot Discounts</h4>
<div class="w-article-list w-article-list-small">
<div class=item>
<div class=item-header>
<a href="/special-broadway-offers.cfm">
<img src="https://newimages.bwwstatic.com/xbronx200.jpg.pagespeed.ic.6t7H9pg-eI.jpg" alt="A Bronx Tale" data-ot-retina="" width=200 height=200>
</a>
</div>
<div class=item-content>
<h4><a href="/special-broadway-offers.cfm">A Bronx Tale</a>
</h4>
<span class=item-meta>
<a href="/special-broadway-offers.cfm" class=item-meta-i>Save $68</a>
</span>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/special-broadway-offers.cfm">
<img src="https://newimages.bwwstatic.com/chicago200.jpg" alt=Chicago data-ot-retina="" width=200 height=200>
</a>
</div>
<div class=item-content>
<h4><a href="/special-broadway-offers.cfm">Chicago</a>
</h4>
<span class=item-meta>
<a href="/special-broadway-offers.cfm" class=item-meta-i>Save $50</a>
</span>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/special-broadway-offers.cfm">
<img src="https://newimages.bwwstatic.com/xnb200.jpg.pagespeed.ic.TFAwxfMWQn.jpg" alt="Napoli, Brooklyn" data-ot-retina="" width=200 height=200>
</a>
</div>
<div class=item-content>
<h4><a href="/special-broadway-offers.cfm">Napoli, Brooklyn</a>
</h4>
<span class=item-meta>
<a href="/special-broadway-offers.cfm" class=item-meta-i>Save $20</a>
</span>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/special-broadway-offers.cfm">
<img src="https://images.broadwayworld.com/marv200.jpg" alt="Marvin's Room" data-ot-retina="">
</a>
</div>
<div class=item-content>
<h4><a href="/special-broadway-offers.cfm">Marvin's Room</a>
</h4>
<span class=item-meta>
<a href="/special-broadway-offers.cfm" class=item-meta-i>Tix from $59</a>
</span>
</div>
</div>
</div>
<a href="/special-broadway-offers.cfm" class=widget-view-more>View More Discounts</a>
 
</div>
</div>
</li>
</ul>
</li>
<li><a href="#"><span>Chat Boards</span></a>
<ul class=sub-menu>
<li><a href="/board/">Broadway</a><li>
<li>
<a href="/westend/board/">West End</a></li>
<li><a href="/board/index.php?boardname=off">Off-Topic</a>
</li>
<li><a href="/board/index.php?boardname=student">Students</a></li>
</ul>
</li>
<li><a href="#"><span>Jobs</span></a>
<ul class=sub-menu>
<li><a href="/equity.cfm">Equity</a></li>
<li><a href="/nonequity.cfm">Non Equity</a></li>
<li><a href="/classifieds/">Classifieds</a></li>
</ul>
</li>
<li><a href="#"><span>Students</span></a>
<ul class=sub-menu>
<li><a href="/bwwjr.cfm">BWW Junior</a></li>
<li><a href="/studentcenter.cfm">College Center</a></li>
<li><a href="/studentcalendar-elementary.cfm">Elementary Center</a></li>
<li><a href="/studentcalendarhighschool.cfm">High School Center</a></li>
<li><a href="/topic/COLLEGE-CENTER">Student Blogs</a></li> </ul>
</li>
<li><a href="#"><span>Video</span></a>
<ul class=sub-menu>
<li><a href="/tvmainnew.cfm">BroadwayWorld TV</a></li>
<li><a href="/tvmainnew.cfm?type=show">Broadway Show Previews</a></li>
<li><a href="/topic/BACKSTAGE-WITH-RICHARD-RIDGE">Backstage w/ Richie Ridge</a></li>
<li><a href="/tvmainnew.cfm?type=stagetube">Stage Tube</a></li>
<li><a href="/author/TV---Red-Carpets">Red Carpets</a></li>
<li><a href="/author/TV---Opening-Night-Special">Opening Nights</a></li>
<li><a href="/author/TV---Press-Previews">Press Previews</a></li>
</ul>
 
</div>
</div>
 
</nav>
 
</div>
<div id=content>
 
<div class=wrapper>
 
<div class="paragraph-row portus-main-content-panel">
<div class=column12>
<div class=portus-main-content-s-block>
 
<div class=portus-main-content>
<div class=theiaStickySidebar>
<div class=portus-main-article-block>
<table width=100% cellpadding=5 cellspacing=5><tr><td>
<strong><center><h1 class=header>BroadwayWorld.com Privacy Policy<br></h1></strong>
<p></center>
<font face=verdana size=-1>
BroadwayWorld.com ** DOES NOT DISCLOSE YOUR PERSONAL INFORMATION** to outside parties. We only release account and other personal information when we believe release is appropriate to comply with law; enforce or apply our <a href="http://www.broadwayworld.com/board/guide.cfm">Terms and Conditions of Use</a> and other agreements; or protect the rights, property, or safety of BroadwayWorld.com, our users, or others. This includes exchanging information with other companies and organizations for fraud protection.
<br><Br>Periodically, aggregate statistics will be provided to our advertising partners, but *NO* specfic or personal information will be included. We do ask you if you are interested in receiving relevant information or offers from theater vendors via e-mail. You may choose to receive this information or easily "unsubscribe" and choose NOT TO RECEIVE any e-mails from BroadwayWorld.com outside your newsletter subscriptions.
<br><Br>If you choose to receive additional information other than your newsletters, this information is sent to you directly by BroadwayWorld.com. If you choose not to receive other information from us, this WILL NOT AFFECT your regular subscriptions. If you choose to discontinue your newsletter subscriptions, you may also do this by simply unsubscribing at any time during your subscription period. When you send us your "unsubscribe" request to each of our offerings, YOUR RECORD GETS PERMANENTLY REMOVED from these databases as we receive your remove requests.
<br><Br>We use third-party advertising companies to serve ads when you visit our Web site. These companies may use aggregated information (not including your name, address, email address or telephone number) about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, <a href="http://www.networkadvertising.org/managing/opt_out.asp">click here</a>.
<br><Br>
Our properties may feature Nielsen proprietary measurement software, which will allow you to contribute to market research, such as Nielsen TV Ratings. To learn more about the information that Nielsen software may collect and your choices with regard to it, please see the Nielsen Digital Measurement Privacy Policy at <a href="http://www.nielsen.com/digitalprivacy">http://www.nielsen.com/digitalprivacy</a>.
<br><Br>
Thank you for making BroadwayWorld.com the web's premier theater resource and please let us know how we can serve you better!
</center>
</center>
</font>
</td></tr></table>
</div></div></div>
<aside class="sidebar portus-sidebar-small">
<div class=theiaStickySidebar>
 
<div class=widget>
<h3 style="margin-bottom:-4px;">Hot Stories</h3>
<div class=w-article-list style="margin-top:1px;">
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/BWW-TV-Exclusive-Well-Strung-Unveils-Gorgeous-Cover-of-Waving-Through-a-Window-from-DEAR-EVAN-HANSEN-20170629"><img src="https://images.bwwstatic.com/columnpic8/170918C8C1B-B13E-A751-9AB4CB5624A2563C.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic8/340918C8C1B-B13E-A751-9AB4CB5624A2563C.jpg" alt="TV Exclusive: Well-Strung Unveils Gorgeous Cover of 'Waving Through a Window' from DEAR EVAN HANSEN" width=180 height=74 /></a>
<h4 class=hot><a href="/article/BWW-TV-Exclusive-Well-Strung-Unveils-Gorgeous-Cover-of-Waving-Through-a-Window-from-DEAR-EVAN-HANSEN-20170629">TV Exclusive: Well-Strung Unveils Gorgeous Cover of 'Waving Through a Window' from DEAR EVAN HANSEN</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Review-Roundup-Critics-Weigh-in-on-Roundabout-Theatres-MARVINS-ROOM--Updating-Live-20170629"><img src="https://images.bwwstatic.com/columnpic8/1709F314E13-DAC8-4C74-FDF91A6BB8E167BB.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic8/3409F314E13-DAC8-4C74-FDF91A6BB8E167BB.jpg" alt="Review Roundup: Critics Weigh in on Roundabout Theatre's MARVIN'S ROOM" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Review-Roundup-Critics-Weigh-in-on-Roundabout-Theatres-MARVINS-ROOM--Updating-Live-20170629">Review Roundup: Critics Weigh in on Roundabout Theatre's MARVIN'S ROOM</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Photo-Coverage-Barnes-and-Noble-Gives-a-Royal-Welcome-to-the-ANASTASIA-Company-20170630"><img src="https://images.bwwstatic.com/columnpic8/170A345A842-971B-B362-6DAE0F40312DE360.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic8/340A345A842-971B-B362-6DAE0F40312DE360.jpg" alt="Photos: Barnes and Noble Gives a Royal Welcome to the ANASTASIA Company!" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Photo-Coverage-Barnes-and-Noble-Gives-a-Royal-Welcome-to-the-ANASTASIA-Company-20170630">Photos: Barnes and Noble Gives a Royal Welcome to the ANASTASIA Company!</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Oh-Say-Can-They-Sing-Broadway-Takes-a-Stab-at-the-National-Anthem-20170629"><img src="https://images.bwwstatic.com/columnpic8/x170261DAC24-E37C-2F29-85F1FB9952FEEC3C.jpg.pagespeed.ic.iPn7sGFMi1.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic8/340261DAC24-E37C-2F29-85F1FB9952FEEC3C.jpg" alt="Oh Say Can They Sing! Broadway Takes a Stab at the National Anthem" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Oh-Say-Can-They-Sing-Broadway-Takes-a-Stab-at-the-National-Anthem-20170629">Oh Say Can They Sing! Broadway Takes a Stab at the National Anthem</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Photo-Coverage-MARVINS-ROOM-Celebrates-Opening-Night-on-Broadway-20170630"><img src="https://images.bwwstatic.com/columnpic8/170C0594407-A56A-43FE-70B740C6BEA9B4B3.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic8/340C0594407-A56A-43FE-70B740C6BEA9B4B3.jpg" alt="Photos: MARVIN'S ROOM Celebrates Opening Night on Broadway!" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Photo-Coverage-MARVINS-ROOM-Celebrates-Opening-Night-on-Broadway-20170630">Photos: MARVIN'S ROOM Celebrates Opening Night on Broadway!</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/VIDEO-Nobody-Will-Get-You-in-the-Patriotic-Spirit-Like-BANDSTAND-20170630"><img src="https://images.bwwstatic.com/columnpic8/17027D135D0-B10A-CB5B-944077DF0D78C367.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic8/34027D135D0-B10A-CB5B-944077DF0D78C367.jpg" alt="VIDEO: Nobody Will Get You in the Patriotic Spirit Like BANDSTAND!" width=180 height=74 /></a>
<h4 class=hot><a href="/article/VIDEO-Nobody-Will-Get-You-in-the-Patriotic-Spirit-Like-BANDSTAND-20170630">VIDEO: Nobody Will Get You in the Patriotic Spirit Like BANDSTAND!</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="http://www.broadwayworld.com/article/BWW-Morning-Brief-June-30th-2017-THE-CAKE-GUYS-AND-DOLLS-and-More-20170630"><img src="https://images.bwwstatic.com/columnpic8/x170A2D59552-C565-F325-3A77A7229C76ECBB.jpg.pagespeed.ic.QkbxKz24kB.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic8/340A2D59552-C565-F325-3A77A7229C76ECBB.jpg" alt="BWW Morning Brief June 30th, 2017 - THE CAKE, GUYS and DOLLS & More!" width=180 height=74 /></a>
<h4 class=hot><a href="http://www.broadwayworld.com/article/BWW-Morning-Brief-June-30th-2017-THE-CAKE-GUYS-AND-DOLLS-and-More-20170630">Morning Brief June 30th, 2017 - THE CAKE, GUYS and DOLLS & More!</a></h4>
</div>
</div>
</div>
 
</div>
 
<div class=widget>
<div class=do-space><Center>
 
<div class=ad-tag data-ad-name="sortable new160" data-ad-size=160x600 data-ad-sticky=manual>
<script data-pagespeed-no-defer>pagespeed.lazyLoadImages.overrideAttributeFunctions();</script><script type="text/x-ab-test">
<script type="text/javascript">
var width = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;
if (width >= 1150) {
    document.write('<div id="div-gpt-ad-1479672818651-160" style="width:160px; height:600px;">');
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1479672818651-160'); });
    document.write('</div>');
}
</xscript>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script>
</Center> </Center> </div>
 
</div>
</div>
</aside>
<aside class="sidebar portus-sidebar-large">
<div class=theiaStickySidebar>
 
<div class=widget style="width:300px;">
<h3>BroadwayWorld TV</h3>
<div style="align-content: center;" align=center>
<i class="fa fa-play-circle-o" style="font-size:30pt;position: absolute;
    top: 90px;
        opacity: 0.5;
color: white;
    right:5px;
    z-index: 10000;
    height: 100px;"></i>
<ul id=demo1>
<li><a href="/article/VIDEO-Brittain-Ashford-Responds-to-Grobans-Ham4All-Challenge-with-Chilling-Quiet-Uptown-20170701"><img src="https://images.bwwstatic.com/columnpic8/34002AE0456-CC60-F2D1-D3EE74F7965FA169.jpg" alt="" width=400 height=165>Brittain Ashford Responds to Groban's #Ham4All Challenge</a>
</li>
<li><a href="/article/BWW-TV-Summer-is-Here-and-NYMF-is-Coming-Go-Inside-Rehearsals-for-Five-New-Musicals-20170701"><img src="https://images.bwwstatic.com/columnpic8/x34065F89ED3-9FD7-943F-93A25CF3DBA69D0D.jpg.pagespeed.ic.MW7zKY5cDq.jpg" alt="" width=400 height=165>Summer is Here and NYMF is Coming! Go Inside Rehearsals for Five New Musicals</a>
</li>
<li><a href="/article/BWW-TV-The-Family-is-Back-Together-on-Opening-Night-of-MARVINS-ROOM-20170701"><img src="https://images.bwwstatic.com/columnpic8/x340C0F5C3AB-FC41-5B86-A9A70D10046E202A.jpg.pagespeed.ic.p9AaFk1B68.jpg" alt="" width=400 height=165>The Family is Back Together on Opening Night of MARVIN'S ROOM!</a>
</li>
<li><a href="/article/VIDEO-Bill-Murray-Takes-on-a-PORGY-AND-BESS-Tune-and-More-in-New-TimesTalk-20170630"><img src="https://images.bwwstatic.com/columnpic8/x340CDE06CD6-D097-4F35-A3DEF4A0DE794719.jpg.pagespeed.ic.X8skcp8nsU.jpg" alt="" width=400 height=165>Bill Murray Takes on a PORGY AND BESS Tune and More in New TimesTalk</a>
</li>
<li><a href="/article/BWW-TV-Watch-Baayork-Lee-Get-Glamorous-for-the-Tonys-20170630"><img src="https://images.bwwstatic.com/columnpic8/x340C8C24113-AB9D-E743-142883C7940537D5.jpg.pagespeed.ic.KtEg0lcK9j.jpg" alt="" width=400 height=165> Watch Baayork Lee Get Glamorous for the Tonys!</a>
</li>
<li><a href="/article/VIDEO-Nobody-Will-Get-You-in-the-Patriotic-Spirit-Like-BANDSTAND-20170630"><img src="https://images.bwwstatic.com/columnpic8/x34027D135D0-B10A-CB5B-944077DF0D78C367.jpg.pagespeed.ic.pj1B4COUWZ.jpg" alt="" width=400 height=165>Nobody Will Get You in the Patriotic Spirit Like BANDSTAND!</a>
</li>
<li><a href="/article/BWW-TV-Curl-That-Lip-and-Howl-Highlights-from-ATTACK-OF-THE-ELIVS-IMPERSONATORS-Off-Broadway-20170630"><img src="https://images.bwwstatic.com/columnpic8/x340BF3CF941-DE90-2CAB-2A799CAA78F0E508.jpg.pagespeed.ic.p8q6tLpo2r.jpg" alt="" width=400 height=165>Curl That Lip & Howl! Highlights from 'ELVIS IMPERSONATORS'</a>
</li>
<li><a href="/article/VIDEO-OH-HELLOs-Nick-Kroll-Kroller-Skates-Onto-TONIGHT-SHOW-Stage-20170630"><img src="https://images.bwwstatic.com/columnpic8/x340BA1E59B9-0D0A-B718-E1AFC44259B3375D.jpg.pagespeed.ic.BEmVr_48oA.jpg" alt="" width=400 height=165>OH, HELLO's Nick Kroll 'Kroller-Skates' Onto TONIGHT SHOW</a>
</li>
<li><a href="/article/VIDEO-Lin-Manuel-Miranda-Talks-HAMILTON-Immigrants-Ham4All-More-on-LATE-NIGHT-20170630"><img src="https://images.bwwstatic.com/columnpic8/x340B7C5C818-B7A4-9BC4-085D5AD493FA7F76.jpg.pagespeed.ic.0ueTpzT47i.jpg" alt="" width=400 height=165>Miranda Talks HAMILTON, #Ham4All & More on LATE NIGHT</a>
</li>
<li><a href="/article/VIDEO-Josh-Groban-Performs-Heart-Wrenching-Burn-for-Ham4All-Challenge-20170629"><img src="https://images.bwwstatic.com/columnpic8/x34099AA0354-D2DF-6251-C055746F936F1FD1.jpg.pagespeed.ic.uyymFZ4TOA.jpg" alt="" width=400 height=165>Josh Groban Performs Heart-Wrenching 'Burn' for #Ham4All</a>
</li>
</ul>
<script>jQuery(document).ready(function(){var demo1=$("#demo1").slippry({});});</script>
</div>
 
<center>
 
<div class=ad-tag data-ad-name="sortable new300A" data-ad-size=auto>
<script type="text/x-ab-test">
<div id=div-gpt-ad-1479672818651-0 style='width:300px;overflow:hidden;position:relative;padding-top:6px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-0');});</xscript>
</div>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script>
</center>
<h3 style="margin-top:10px;">Ticket Central <small style="float:right;padding-top:3px;"><a href="/shows/shows.php?page=shows" style="color: white;">Browse All Shows</a></small></h3>
 
<table border=0 style="padding:0px;width:100%;"><tr><td valign=top style="padding-right:8px;padding-left:0px;margin-left:0px;">
<div id=div-gpt-ad-1479672818651-10 style='height:135px; width:130px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-10');});</script>
</div>
</td>
<td style="padding-top:0px;margin-top:0px;vertical-align:middle;">
<li class=special><a href="/special-broadway-offers.cfm">WAITRESS</a></li>
<li class=special><a href="/special-broadway-offers.cfm">MARVIN'S ROOM</a></li>
<li class=special><a href="/special-broadway-offers.cfm">A BRONX TALE</a></li>
<li class=special><a href="/special-broadway-offers.cfm">NAPOLI, BROOKLYN</a></li>
<li class=special><a href="/special-broadway-offers.cfm">CHICAGO</a></li>
</td></tr></table>
 
<div class=widget style="border-bottom: none; ">
<h3 style="margin-top:10px;">Watch Now on BWW TV <small style="float:right;padding-top:4px;"><a href="http://www.broadwayworld.com/tvmainnew.cfm" style="color: white;">More</a></small></h3>
<div class=widget-instagram-photos style="height: 135px;">
<div class=item>
<div class=item-header>
<a href="/article/VIDEO-Josh-Groban-Performs-Heart-Wrenching-Burn-for-Ham4All-Challenge-20170629">
<img src="https://images.bwwstatic.com/columnpic8/17099AA0354-D2DF-6251-C055746F936F1FD1.jpg" alt="TV: Josh Groban Performs Heart-Wrenching 'Burn' for #Ham4All" width=180 height=74>
</a>
</div>
<div class=item-content style="height:60px;">
<h4><a href="/article/VIDEO-Josh-Groban-Performs-Heart-Wrenching-Burn-for-Ham4All-Challenge-20170629">TV: Josh Groban Performs Heart-Wrenching 'Burn' for #Ham4All</a></h4>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/article/BWW-TV-Its-a-World-of-Pure-Imagination-Inside-Rehearsals-for-the-2017-Jimmy-Awards-20170626">
<img src="https://images.bwwstatic.com/columnpic8/x1705E360466-0E1D-FE5C-6C901D2673E37351.jpg.pagespeed.ic.WbI3xuV-zb.jpg" alt="TV: It's a 'World of Pure Imagination' in Rehearsal for the 2017 Jimmy Awards!" width=180 height=74>
</a>
</div>
<div class=item-content style="height:60px;">
<h4><a href="/article/BWW-TV-Its-a-World-of-Pure-Imagination-Inside-Rehearsals-for-the-2017-Jimmy-Awards-20170626">TV: It's a 'World of Pure Imagination' in Rehearsal for the 2017 Jimmy Awards!</a></h4>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/article/VIDEO-Ladies-Gentlemen-The-Official-Trailer-for-THE-GREATEST-SHOWMAN-Has-Arrived-20170628">
<img src="https://images.bwwstatic.com/columnpic8/x170543D0E14-DA87-F326-2EFD4E62DF79F2F4.jpg.pagespeed.ic.-ZDCOGUvv6.jpg" alt="The Trailer for THE GREATEST SHOWMAN Has Arrived!" width=180 height=74>
</a>
</div>
<div class=item-content style="height:60px;">
<h4><a href="/article/VIDEO-Ladies-Gentlemen-The-Official-Trailer-for-THE-GREATEST-SHOWMAN-Has-Arrived-20170628">The Trailer for THE GREATEST SHOWMAN Has Arrived!</a></h4>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/article/VIDEO-Glenn-Close-and-Cast-of-SUNSET-BOULEVARD-Take-Final-Broadway-Bows-20170626">
<img src="https://images.bwwstatic.com/columnpic8/x170E6D47963-A9FB-2CC6-05DD8E22F0758D68.jpg.pagespeed.ic.wQ5Z6T0teH.jpg" alt="SUNSET BOULEVARD Takes Final Broadway Bows" width=180 height=74>
</a>
</div>
<div class=item-content style="height:60px;">
<h4><a href="/article/VIDEO-Glenn-Close-and-Cast-of-SUNSET-BOULEVARD-Take-Final-Broadway-Bows-20170626">SUNSET BOULEVARD Takes Final Broadway Bows</a></h4>
</div>
</div>
</div>
 
</div>
<center>
 
<div class=widget style="border-bottom:0px;margin-bottom:0px;">
<center>
<div class=do-space>
<center>
 
<div class=ad-tag data-ad-name="sortable new300B" data-ad-size=auto>
<script type="text/x-ab-test">
<div id=div-gpt-ad-1479672818651-1 style='width:300px;overflow:hidden;position:relative;padding-top:6px;'>
<script type="text/javascript" data-pagespeed-no-defer>pagespeed.lazyLoadImages.overrideAttributeFunctions();</xscript><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-1');});</xscript>
</div>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script>
</div>
 
</div>
 
<div class=widget style="border-bottom:0px;margin-bottom:8px;">
<h3>Subscribe Now</h3>
<div class=widget-subscribe>
<div>
<p>Register now for discounts, specials & more!</p>
</div>
<form action="/register.cfm" method=post>
<label class=label-input>
<span>E-mail address</span>
<input name=email type=email value=""/>
</label>
<input type=submit class=button value=Subscribe />
</form>
</div>
 
</div>
<div class=widget style="border-bottom:0px;padding-bottom:4px;margin-bottom:10px;">
<center>
<div class=do-space>
 
<div class=ad-tag data-ad-name="sortable new300C" data-ad-size=auto>
<script type="text/x-ab-test">
<div id=div-gpt-ad-1479672818651-2 style='width:300px;overflow:hidden;position:relative;padding-top:6px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-2');});</xscript>
</div>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script>
</div></center>
 
</div>
</center>
<div class=widget style="border-bottom:0px;margin-bottom:8px;overflow: hidden;">
<h3>Recommended for You</h3>
<div id=test1>
</div>
<script>function createIframe(){var i=document.createElement("iframe");i.src="https://www2.broadwayworld.com/recommended.cfm?ModPagespeed=off";i.scrolling="no";i.frameborder="0";i.width="300px";i.height="380px";document.getElementById("test1").appendChild(i);};if(window.addEventListener)window.addEventListener("load",createIframe,false);else if(window.attachEvent)window.attachEvent("onload",createIframe);else window.onload=createIframe;</script>
 
</div>
<div class=widget style="border-bottom:0px;margin-bottom:8px;overflow: hidden;">
<h3>Around the Broadway World</h3>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/philadelphia/article/Exclusive-Lesli-Margherita-Elena-Shaddow-Ruth-Gottschall-and-More-Set-For-Bucks-County-Playhouse-GUYS-AND-DOLLS-20170626" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>Philadelphia</b></font>: Exclusive: Lesli Margherita, Elena Shaddow, Ruth Gottschall and More Set For Bucks County Playhouse GUYS AND DOLLS</a></h4>
</div>
</div>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/toronto/article/COME-FROM-AWAY-Names-Outstanding-New-Musical-at-Torontos-Dora-Awards-20170626" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>Toronto</b></font>: COME FROM AWAY Takes Top Honors at Toronto's Dora Awards</a></h4>
</div>
</div>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/los-angeles/article/BWW-Review-I-AM-NOT-A-COMEDIANIM-LENNY-BRUCE-Will-Open-Your-Eyes-to-his-Comedic-Genius-and-Dedication-to-Free-Speech-20170625" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>Los Angeles</b></font>: Review: I AM NOT A COMEDIAN - I'M LENNY BRUCE Will Open Your Eyes to his Comedic Genius and Dedication to Free Speech</a></h4>
</div>
</div>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/los-angeles/article/George-Takei-to-Return-to-ALLEGIANCE-for-Los-Angeles-Premiere-20170627" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>Los Angeles</b></font>: George Takei to Return to ALLEGIANCE for L.A. Premiere</a></h4>
</div>
</div>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/los-angeles/article/Lin-Manuel-Miranda-Offers-A-Ham4All-Shot-at-Attending-Opening-Night-of-Hamilton-In-Los-Angeles-20170626" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>Los Angeles</b></font>: Miranda Offers #Ham4All Shot at Attending Opening of Hamilton In LA</a></h4>
</div>
</div>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/los-angeles/article/BWW-Interview-BeBe-Winans-Talks-BORN-FOR-THIS-20170629" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>Los Angeles</b></font>: Interview: BeBe Winans Talks BORN FOR THIS</a></h4>
</div>
</div>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/spain/article/Anunciado-el-reparto-completo-de-EL-GUARDAESPALDAS-20170626" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>Spain</b></font>: Anunciado el reparto completo de EL GUARDAESPALDAS</a></h4>
</div>
</div>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/costa-mesa/article/TV-Exclusive-Tony-Winner-Christopher-Ashley-Talks-Broadway-Bound-ESCAPE-TO-MARGARITAVILLE-20170628" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>Costa Mesa</b></font>: TV Exclusive: Tony Winner Christopher Ashley Talks Broadway Bound ESCAPE TO MARGARITAVILLE</a></h4>
</div>
</div>
<div class=item>
<div class=item-content>
<h4><a href="http://www.broadwayworld.com/new-jersey/article/Paper-Mill-Selects-Students-for-2017-Summer-Musical-Theatre-Conservatory-20170629" style="font-size: 14px;font-weight: 500;"><font color=b20223><b>New Jersey</b></font>: Paper Mill Selects Students for 2017 Summer Musical Theatre Conservatory</a></h4>
</div>
</div>
</div>
<div class=widget>
<div class=do-space><center>
 
<div class=ad-tag data-ad-name="sortable new300D" data-ad-size=auto>
<script type="text/x-ab-test">
<div id=div-gpt-ad-1479672818651-3 style='width:300px;overflow:hidden;position:relative;padding-top:6px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-3');});</xscript>
</div>
</script>
</div>
<script src="//tags-cdn.deployads.com/a/broadwayworld.com.js" async></script>
<script>(deployads=window.deployads||[]).push({});</script>
</center> </div>
 
</div>
</div>
</aside>
</div>
</div>
 
</div>
 
</div>
 
</div>
 
</div>
 
<div id=footer>
<div id=footer-widgets>
<div class=wrapper>
<div class=paragraph-row>
<div class=column12>
 
<div class=widget>
<center>
 
<div id=div-gpt-ad-1479672818651-New728468Bottom>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-New728468Bottom');});</script>
</div><br>
 
</div>
</div>
<div class=paragraph-row style="margin-top: 12px;">
<div class=column3>
<div class=widget>
<h3>Follow Us</h3>
<div>
<a href="https://www.facebook.com/BroadwayWorld" target=_new><div class=short-icon-text>
<i class="fa fa-facebook"></i>
<span style="color:white;">Facebook</span>
</div></a>
<a href="https://www.twitter.com/BroadwayWorld" target=_new><div class=short-icon-text>
<i class="fa fa-twitter"></i>
<span style="color:white;">Twitter</span>
</div></a>
<a href="https://www.instagram.com/officialbroadwayworld/" target=_new>
<div class=short-icon-text>
<i class="fa fa-instagram"></i>
<span style="color:white;">Instagram</span>
</div></a>
<a href="http://broadwayworld.tumblr.com/" target=_new>
<div class=short-icon-text>
<i class="fa fa-tumblr"></i>
<span style="color:white;">Tumblr</span>
</div></a>
<a href="/article/BroadwayWorld-is-Everywhere--Interact-with-Us-With-Apps-Social-Media-Today-20151106" target=_new>
<div class=short-icon-text>
<i class="fa fa-apple"></i>
<span style="color:white;">Apps</span>
</div></a>
</div>
</div>
</div>
<div class=column3>
<div class=widget>
<h3>BWW Sister Sites</h3>
<ul class=menu>
<li><a href="/bwwtv/index.cfm">TV/MOVIES</a></li>
<li><a href="/bwwmusic/index.cfm">MUSIC</a></li>
<li><a href="/bwwbooks/index.cfm">BOOKS</a></li>
<li><a href="/bwwclassical/index.cfm">CLASSICAL MUSIC</a></li>
<li><a href="/bwwdance/index.cfm">DANCE</a></li>
<li><a href="/bwwopera/index.cfm">OPERA</a></li>
</ul>
</div>
</div>
<div class=column3 style="width: 46%">
<div class=widget>
<h3>BWW Hub</h3>
<table>
<Tr><Td style="padding-bottom: 10px;vertical-align: middle;"> <div class=item>
<a href="/bwwtv/article/VIDEO-Lea-Michele-Takes-Ham4All-Challenge-with-Sweet-Rendition-of-Dear-Theodosia-20170629" style="color:white;font-size:12pt;">
<img src="https://images.bwwstatic.com/columnpic8/x17086B97BBA-FC11-6122-EF55BF2035EAD5FC.jpg.pagespeed.ic.4qp_I8MW4X.jpg" alt="VIDEO: Lea Michele Takes #Ham4All Challenge with 'Dear Theodosia' Cover" data-ot-retina="" style="float:left;padding-right: 8px;" width=180 height=74>VIDEO: Lea Michele Takes #Ham4All Challenge with 'Dear Theodosia' Cover
</a>
</div></Td></Tr>
<Tr><Td style="padding-bottom: 10px;vertical-align: middle;"> <div class=item>
<a href="/bwwtv/article/VIDEO-Julie-Andrews-On-MARY-POPPINS-RETURNS-Emily-Blunt-Im-a-Huge-Fan-20170629" style="color:white;font-size:12pt;">
<img src="https://images.bwwstatic.com/columnpic8/17089492141-ECDC-C0DC-5C1369FC83D1A29F.jpg" alt="VIDEO: Julie Andrews Is 'Huge Fan' of 'POPPINS' Emily Blunt" data-ot-retina="" style="float:left;padding-right: 8px;" width=180 height=74>VIDEO: Julie Andrews Is 'Huge Fan' of 'POPPINS' Emily Blunt
</a>
</div></Td></Tr>
</table>
</div>
</div>
</div>
</div>
</div>
<div id=footer-info>
<div class=wrapper>
<ul class=right>
<li><a href="/mediakit.cfm" style="color:white;">Advertising Info</a></li>
<li><a href="/contact.cfm" style="color:white;">Contact Us</a></li>
<li><a href="/article/Join-the-BWW-Interns-Contributors-Team-20141229" style="color:white;">Join the Team</a></li>
<li><a href="/submitnews.cfm" style="color:white;">Submit News</a></li>
<li><a href="/privacy.cfm" style="color:white;">Privacy Policy</a></li>
</ul>
<p>&copy; 2017 - Copyright <a href="http://www.wisdomdigital.com" style="color:white;"><b>Wisdom Digital Media</b></a>, all rights reserved. </p>
</div>
</div>
 
</div>
 
</div>
 
<script src="https://nav.bwwstatic.com/2017/bootstrap-3.3.7-dist/js/bootstrap.min.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/theia15min.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/modernizr-custom.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/jsiscroll/iscroll-lite.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/dat-menu-fixed.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/theme-scripts1113dcompressed.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/slippry/dist/slippry.min.js"></script>
<script src="https://nav.bwwstatic.com/2014/js/shadow-interstitial2017.js"></script>
<script>Shadowbox.init({skipSetup:true});</script>
<div id=div-gpt-ad-1479672818651-8 style='width:1px; height:1px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-8');});</script></div>
</div></div></div>
 
<script>var _comscore=_comscore||[];_comscore.push({c1:"2",c2:"18162732"});(function(){var s=document.createElement("script"),el=document.getElementsByTagName("script")[0];s.async=true;s.src=(document.location.protocol=="https:"?"https://sb":"http://b")+".scorecardresearch.com/beacon.js";el.parentNode.insertBefore(s,el);})();</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=18162732&cv=2.0&cj=1"/>
</noscript>
 
 
<script>var _qevents=_qevents||[];(function(){var elem=document.createElement('script');elem.src=(document.location.protocol=="https:"?"https://secure":"http://edge")+".quantserve.com/quant.js";elem.async=true;elem.type="text/javascript";var scpt=document.getElementsByTagName('script')[0];scpt.parentNode.insertBefore(elem,scpt);})();_qevents.push({qacct:"p-61Pu-3TC5IB0I",});</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-61Pu-3TC5IB0I.gif" border=0 height=1 width=1 alt=Quantcast />
</div>
</noscript>
<script>document.body.onload=function(){if('safari'in window&&'pushNotification'in window.safari){var permissionData=window.safari.pushNotification.permission('web.com.broadwayworld');checkRemotePermission(permissionData);}};var checkRemotePermission=function(permissionData){if(permissionData.permission==='default'){window.safari.pushNotification.requestPermission('https://secure.broadwayworld.com/push','web.com.broadwayworld',{},checkRemotePermission);}else if(permissionData.permission==='denied'){}else if(permissionData.permission==='granted'){}};</script>
 
 
</body>
 
<div id=div-gpt-ad-1479672818651-Onscroll style='width:1px; height:1px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-Onscroll');});</script></div>
</html>
