
<!DOCTYPE HTML>
 
<html lang=en>
<head>
<title>Broadway World.com - Broadway's Premier Web Resource</title>
<meta name=description content="BroadwayWorld.com - Broadway's Premier Web
Resource"/>
<meta name=keywords content="broadway, Broadway, Theater, Broadway shows, Theatre, Broadway Message Board, Show, Review, musicals, broadway news, plays, stars, tickets, musical, stage, tony awards, national tour, actor, actress, interviews, stage shows, show tickets, theater reviews, theater news, off-broadway"/>
<meta name=apple-itunes-app content="app-id=530770227"/>
 
<meta http-equiv=Content-Type content="text/html; charset=UTF-8"/>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=viewport content="width=device-width, initial-scale=1, maximum-scale=1"/>
 
<link rel=apple-touch-icon-precomposed sizes=57x57 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-57x57.png"/>
<link rel=apple-touch-icon-precomposed sizes=114x114 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-114x114.png"/>
<link rel=apple-touch-icon-precomposed sizes=72x72 href="https://newimages.bwwstatic.com/2017/xapple-touch-icon-72x72.png.pagespeed.ic.8tKwTK3gnG.png"/>
<link rel=apple-touch-icon-precomposed sizes=144x144 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-144x144.png"/>
<link rel=apple-touch-icon-precomposed sizes=60x60 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-60x60.png"/>
<link rel=apple-touch-icon-precomposed sizes=120x120 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-120x120.png"/>
<link rel=apple-touch-icon-precomposed sizes=76x76 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-76x76.png"/>
<link rel=apple-touch-icon-precomposed sizes=152x152 href="https://newimages.bwwstatic.com/2017/apple-touch-icon-152x152.png"/>
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-196x196.png" sizes=196x196 />
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-96x96.png" sizes=96x96 />
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-32x32.png" sizes=32x32 />
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-16x16.png" sizes=16x16 />
<link rel=icon type="image/png" href="https://newimages.bwwstatic.com/2017/favicon-128.png" sizes=128x128 />
<meta name=application-name content="&nbsp;"/>
<meta name=msapplication-TileColor content="#FFFFFF"/>
<meta name=msapplication-TileImage content="https://newimages.bwwstatic.com/2017/mstile-144x144.png"/>
<meta name=msapplication-square70x70logo content="https://newimages.bwwstatic.com/2017/mstile-70x70.png"/>
<meta name=msapplication-square150x150logo content="https://newimages.bwwstatic.com/2017/mstile-150x150.png"/>
<meta name=msapplication-wide310x150logo content="https://newimages.bwwstatic.com/2017/mstile-310x150.png"/>
<meta name=msapplication-square310x310logo content="https://newimages.bwwstatic.com/2017/mstile-310x310.png"/>
 
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/reset.min.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/font-awesome-4.7.0/css/font-awesome.min.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/bootstrap-3.3.7-dist/css/bootstrap2.min.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/main-nonminified13b.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/shortcodes1113.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/responsive2a.min.css"/>
<link type="text/css" rel=stylesheet href="https://nav.bwwstatic.com/2017/dat-menu.min.css"/>
<link type="text/css" rel=stylesheet href='https://fonts.googleapis.com/css?family=Montserrat:400,700'/>
 
 
<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
<!--[if lte IE 8]>
		<link type="text/css" rel="stylesheet" href="https://nav.bwwstatic.com/2017//ie-ancient.css" />
		<![endif]-->
<script>window.googletag=window.googletag||{};googletag.cmd=googletag.cmd||[];window.m2hb=window.m2hb||{};m2hb.que=m2hb.que||[];m2hb.ssl='https:'==document.location.protocol;window.M2_TIMEOUT=1000;m2hb.maxRetries=20;m2hb.retries=0;m2hb.loadJS=function(src,async){m2hb.loadedJS=m2hb.loadedJS||[];m2hb.loadedJS.push(src);(function(){var script=document.createElement("script");script.async=async||true;script.type="text/javascript";script.src=(m2hb.ssl?'https:':'http:')+src;var target=document.getElementsByTagName("head")[0];target.insertBefore(script,target.firstChild);})();};if(typeof m2hb.initAdserver=='undefined'){m2hb.initAdserver=function(){if(m2hb.initAdserverSet)return;if(typeof m2hb.timeRemaining=='undefined'){m2hb.timeRemaining=M2_TIMEOUT;}if(m2hb.timeRemaining>0){m2hb.timeRemaining-=100;if(!m2hb.m2hbBidsReady){setTimeout(function(){m2hb.initAdserver();},100);return;}}if(!googletag.pubadsReady&&(m2hb.retries<=m2hb.maxRetries)){m2hb.retries++;setTimeout(function(){m2hb.initAdserver();},50);return;}m2hb.initAdserverSet=true;googletag.cmd.push(function(){m2hb.que.push(function(){m2hb.setTargetingForGPTAsync();});googletag.pubads().refresh();});};}window.initAdserver=function(){return m2hb.initAdserver();}
m2hb.loadJS('//m2d.m2.ai/m2d.broadwayworld.desktop.min.js');googletag.cmd.push(function(){googletag.pubads().disableInitialLoad();m2hb.disabledGptInitialLoad=true;});m2hb.loadJS('//www.googletagservices.com/tag/js/gpt.js');m2hb.initAdserver();var width=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;googletag.cmd.push(function(){googletag.defineSlot('/106293300/NEW300A',[[300,250],[300,600]],'div-gpt-ad-1479672818651-0').addService(googletag.pubads());googletag.defineSlot('/106293300/NEW300B',[[300,250],[300,600]],'div-gpt-ad-1479672818651-1').addService(googletag.pubads());googletag.defineSlot('/106293300/NEW300C',[[300,250],[300,600]],'div-gpt-ad-1479672818651-2').addService(googletag.pubads());if(width<=1050){googletag.defineSlot('/106293300/NEW468',[468,60],'div-gpt-ad-1479672818651-5').addService(googletag.pubads());}if(width>=1201){googletag.defineSlot('/106293300/NEW160',[160,600],'div-gpt-ad-1479672818651-160').addService(googletag.pubads());googletag.defineSlot('/106293300/NEW970',[970,90],'div-gpt-ad-1479672818651-7').addService(googletag.pubads());}else{googletag.defineSlot('/106293300/NEW728',[728,90],'div-gpt-ad-1479672818651-6').addService(googletag.pubads());}googletag.defineSlot('/106293300/NEW300D',[300,250],'div-gpt-ad-1479672818651-3').addService(googletag.pubads());googletag.defineSlot('/106293300/Inter',[1,1],'div-gpt-ad-1479672818651-8').addService(googletag.pubads());googletag.defineSlot('/106293300/New728468Bottom',[728,90],'div-gpt-ad-1479672818651-New728468Bottom').addService(googletag.pubads());googletag.defineSlot('/106293300/TCPULLDOWN-MUSICALS',[100,30],'div-gpt-ad-1479672818651-15').addService(googletag.pubads());googletag.defineSlot('/106293300/NEWTCLEFT',[130,135],'div-gpt-ad-1479672818651-10').addService(googletag.pubads());googletag.defineSlot('/106293300/Onscroll_VET_unit',[1,1],'div-gpt-ad-1479672818651-Onscroll').addService(googletag.pubads());googletag.pubads().setTargeting('Region','Broadway');googletag.pubads().enableSingleRequest();googletag.pubads().collapseEmptyDivs();googletag.enableServices();});</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');ga('create','UA-253633-20','auto');ga('send','pageview');setTimeout("ga('send','event','adjusted bounce rate','page visit 15 seconds or more')",15000);</script>  
 
</head>
 
<body class="ot-menu-will-follow  has-dat-menu  ">
<div id=dat-menu class=effect-2><div class=dat-menu-container><div class=dat-menu-wrapper>
 
<div class=boxed>
 
<div id=header style="margin-bottom: -1px;">
 
<nav id=top-menu>
 
<div class=wrapper>
<div class="top-panel-weather right">
<span class=w-stats style="background-color: #f8f8f8;border-width: 1px;border-style: solid;border-color: black;"><a href="/newlogin.cfm">Log In</a></span>&nbsp;&nbsp;
<a href="/register.cfm"><span class=w-stats style="background-color:#b20223;" style="color:#fff;">Register Now</span></a>
</div>
<ul class="load-responsive hd-nav" rel="Jump To">
<li><a href="/"><i class="fa fa-home"></i></a></li>
<li><a href="/westend/">West End</a></li>
<li><a href="/off-broadway/">Off-Broadway</a></li>
<li class=dat-has-sub><a href="/regionalmain.cfm">United States</a>
<div class=h-sub-menu>
<ul>
<li><a href="/national-tours/">NATIONAL TOURS</a></li>
<li class=dat-has-sub><a href="#">A-D</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/birmingham/">ALABAMA - Birmingham</a></li><li><a href="/anchorage/">ALASKA - Anchorage</a></li><li><a href="/mesa/">ARIZONA - Mesa</a></li><li><a href="/phoenix/">ARIZONA - Phoenix</a></li><li><a href="/tempe/">ARIZONA - Tempe</a></li><li><a href="/tucson/">ARIZONA - Tucson</a></li><li><a href="/little-rock/">ARKANSAS - Little Rock</a></li><li><a href="/costa-mesa/">CALIFORNIA - Costa Mesa</a></li><li><a href="/los-angeles/">CALIFORNIA - Los Angeles</a></li><li><a href="/palm-springs/">CALIFORNIA - Palm Springs</a></li><li><a href="/sacramento/">CALIFORNIA - Sacramento</a></li><li><a href="/san-diego/">CALIFORNIA - San Diego</a></li><li><a href="/san-francisco/">CALIFORNIA - San Francisco</a></li><li><a href="/santa-barbara/">CALIFORNIA - Santa Barbara</a></li><li><a href="/thousand-oaks/">CALIFORNIA - Thousand Oaks</a></li><li><a href="/denver/">COLORADO - Denver</a></li><li><a href="/connecticut/">CONNECTICUT</a></li><li><a href="/delaware/">DELAWARE</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">E-F</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/miami/">FLORIDA - Miami</a></li><li><a href="/fort-lauderdale/">FLORIDA - Ft. Lauderdale</a></li><li><a href="/ft-myers-naples/">FLORIDA - Ft. Myers/Naples</a></li><li><a href="/jacksonville/">FLORIDA - Jacksonville</a></li><li><a href="/orlando/">FLORIDA - Orlando</a></li><li><a href="/palm-beach/">FLORIDA - Palm Beach</a></li><li><a href="/st-petersburg/">FLORIDA - St. Petersburg</a></li><li><a href="/tallahassee/">FLORIDA - Tallahassee</a></li><li><a href="/tampa/">FLORIDA - Tampa</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">G-K</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/atlanta/">GEORGIA - Atlanta</a></li><li><a href="/hawaii/">HAWAII</a></li><li><a href="/boise/">IDAHO - Boise</a></li><li><a href="/chicago/">ILLINOIS - Chicago</a></li><li><a href="/indianapolis/">INDIANA - Indianpolis</a></li><li><a href="/south-bend/">INDIANA - South Bend</a></li><li><a href="/des-moines/">IOWA - Des Moines</a></li><li><a href="/wichita/">KANSAS - Wichita</a></li><li><a href="/louisville/">KENTUCKY - Louisville</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">L-M</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/new-orleans/">LOUISIANA - New Orleans</a></li><li><a href="/maine/">MAINE</a></li><li><a href="/baltimore/">MARYLAND - Baltimore</a></li><li><a href="/boston/">MASSACHUSETTS - Boston</a></li><li><a href="/detroit/">MICHIGAN - Detroit</a></li><li><a href="/minneapolis/">MINNESOTA - Minneapolis</a></li><li><a href="/st-paul/">MINNESOTA - St. Paul</a></li><li><a href="/jackson/">MISSISSIPPI - Jackson</a></li><li><a href="/kansas-city/">MISSOURI - Kansas City</a></li><li><a href="/st-louis/">MISSOURI - St. Louis</a></li><li><a href="/montana/">MONTANA</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">N</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/omaha/">NEBRASKA - Omaha</a></li><li><a href="/new-hampshire/">NEW HAMPSHIRE</a></li><li><a href="/new-jersey/">NEW JERSEY</a></li><li><a href="/las-vegas/">NEVADA - Las Vegas</a></li><li><a href="/albuquerque/">NEW MEXICO - Albuquerque</a></li><li><a href="/brooklyn/">NEW YORK - Brooklyn</a></li><li><a href="/buffalo/">NEW YORK - Buffalo</a></li><li><a href="/central-new-york/">NEW YORK - Central NY</a></li><li><a href="/long-island/">NEW YORK - Long Island</a></li><li><a href="/rockland/">NEW YORK - Rockland</a></li><li><a href="/rockland/">NEW YORK - Westchester</a></li><li><a href="/charlotte/">NORTH CAROLINA - Charlotte</a></li><li><a href="/raleigh/">NORTH CAROLINA - Raleigh</a></li><li><a href="/fargo/">NORTH DAKOTA - Fargo</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">O</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/cincinnati/">OHIO - Cincinnati</a></li><li><a href="/cleveland/">OHIO - Cleveland</a></li><li><a href="/columbus/">OHIO - Columbus</a></li><li><a href="/dayton/">OHIO - Dayton</a></li><li><a href="/oklahoma/">OKLAHOMA - Oklahoma City</a></li><li><a href="/tulsa/">OKLAHOMA - Tulsa</a></li><li><a href="/portland/">OREGON - Portland</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">P-T</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/central-pa/">PENNSYLVANIA - Central PA</a></li><li><a href="/philadelphia/">PENNSYLVANIA - Philadelphia</a></li><li><a href="/pittsburgh/">PENNSYLVANIA - Pittsburgh</a></li><li><a href="/rhode-island/">RHODE ISLAND</a></li><li><a href="/south-carolina/">SOUTH CAROLINA</a></li><li><a href="/sioux-falls/">SOUTH DAKOTA - Sioux Falls</a></li><li><a href="/memphis/">TENNESSEE - Memphis</a></li><li><a href="/nashville/">TENNESSEE - Nashville</a></li><li><a href="/austin/">TEXAS - Austin</a></li><li><a href="/dallas/">TEXAS - Dallas</a></li><li><a href="/houston/">TEXAS - Houston</a></li><li><a href="/san-antonio/">TEXAS - San Antonio</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">U-W</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/salt-lake-city/">UTAH - Salt Lake City</a></li><li><a href="/vermont/">VERMONT</a></li><li><a href="/norfolk/">VIRGINIA - Richmond/Norfolk</a></li><li><a href="/seattle/">WASHINGTON - Seattle</a></li><li><a href="/washington-dc/">WASHINGTON, DC</a></li><li><a href="/west-virginia/">WEST VIRGINIA</a></li><li><a href="/appleton/">WISCONSIN - Appleton</a></li><li><a href="/madison/">WISCONSIN - Madison</a></li><li><a href="/milwaukee/">WISCONSIN - Milwaukee</a></li><li><a href="/casper/">WYOMING - Casper</a></li> </ul>
</div>
</li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="/regionalmain.cfm">The World</a>
<div class=h-sub-menu>
<ul>
<li class=dat-has-sub><a href="#">NORTH AMERICA</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/montreal/">CANADA - MONTREAL</a></li><li><a href="/ottawa/regionalshows.cfm?ModPagespeed=off">CANADA - OTTAWA</a></li><li><a href="/toronto/">CANADA - TORONTO</a></li><li><a href="/vancouver/">CANADA - VANCOUVER</a></li><li><a href="/mexico/">MEXICO</a> </li>
<li><a href="/cuba/regionalshows.cfm?ModPagespeed=off">CUBA</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">AUSTRALIA/NEW ZEALAND</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/adelaide/">AUSTRALIA - ADELAIDE</a></li><li><a href="/brisbane/">AUSTRALIA - BRISBANE</a></li>
<li><a href="/australia-melbourne/">AUSTRALIA - MELBOURNE</a></li><li><a href="/perth/">AUSTRALIA - PERTH</a></li><li><a href="/sydney/">AUSTRALIA - SYDNEY</a></li><li><a href="/new-zealand/">NEW ZEALAND</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">UK/EUROPE</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/westend/">LONDON - WEST END</a></li><li><a href="/austria/">AUSTRIA</a></li><li><a href="/belgium/regionalshows.cfm?ModPagespeed=off">BELGIUM</a></li><li><a href="/finland/regionalshows.cfm?ModPagespeed=off">FINLAND</a></li><li><a href="/france/regionalshows.cfm?ModPagespeed=off">FRANCE</a></li><li><a href="/germany/">GERMANY</a></li><li><a href="/hungary/regionalshows.cfm?ModPagespeed=off">HUNGARY</a></li><li><a href="/ireland/">IRELAND</a></li><li><a href="/italy/">ITALY</a></li><li><a href="/luxembourg/regionalshows.cfm?ModPagespeed=off">LUXEMBOURG</a></li><li><a href="/monaco/">MONACO</a></li><li><a href="/netherlands/regionalshows.cfm?ModPagespeed=off">NETHERLANDS</a></li><li><a href="/norway/regionalshows.cfm?ModPagespeed=off">NORWAY</a></li><li><a href="/poland/regionalshows.cfm?ModPagespeed=off">POLAND</a></li><li><a href="/prague/regionalshows.cfm?ModPagespeed=off">PRAGUE</a></li><li><a href="/scotland/">SCOTLAND</a></li><li><a href="/spain/">SPAIN</a></li><li><a href="/sweden/">SWEDEN</a></li><li><a href="/switzerland/">SWITZERLAND</a></li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="#">SOUTH AMERICA</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/argentina/">ARGENTINA</a></li><li><a href="/brazil/">BRAZIL</a></li><li><a href="/colombia/regionalshows.cfm?ModPagespeed=off">COLOMBIA</a></li><li><a href="/venezuela/regionalshows.cfm?ModPagespeed=off">VENEZUELA</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">ASIA</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="http://china.broadwayworld.com">CHINA</a></li><li><a href="/india/">INDIA</a></li><li><a href="/indonesia/">INDONESIA</a></li><li><a href="/japan/">JAPAN</a></li><li><a href="/malaysia/regionalshows.cfm?ModPagespeed=off">MALAYSIA</a></li><li><a href="/philippines/">PHILIPPINES</a></li><li><a href="/singapore/">SINGAPORE</a></li><li><a href="/south-korea/regionalshows.cfm?ModPagespeed=off">SOUTH KOREA</a></li> </ul>
</div>
</li>
<li class=dat-has-sub><a href="#">AFRICA/MIDDLE EAST</a>
<div class=h-sub-menu-inner>
<ul>
<li><a href="/israel/">ISRAEL</a></li><li><a href="/russia/regionalshows.cfm?ModPagespeed=off">RUSSIA</a></li>
<li><a href="/south-africa/">SOUTH AFRICA</a></li><li><a href="/turkey/regionalshows.cfm?ModPagespeed=off">TURKEY</a></li>
</ul>
</div>
</li>
</ul>
</div>
</li>
<li class=dat-has-sub><a href="/regionalmain.cfm">Entertainment</a>
<div class=h-sub-menu>
<ul>
<li><a href="/bwwtv/index.cfm">TV/MOVIES</a></li>
<li><a href="/cabaret/">CABARET</a></li>
<li><a href="/bwwmusic/index.cfm">MUSIC</a></li>
<li><a href="/bwwbooks/index.cfm">BOOKS</a></li>
<li><a href="/bwwclassical/index.cfm">CLASSICAL MUSIC</a></li>
<li><a href="/bwwdance/index.cfm">DANCE</a></li>
<li><a href="/bwwopera/index.cfm">OPERA</a></li>
<li><a href="/bwwfitness/index.cfm">FITNESS</a></li>
<li><a href="/food-wine/index.cfm">FOOD + WINE</a></li>
</ul>
</div>
</li>
</ul>
</div>
 
</nav>
 
<div class=wrapper>
<div class=header-panels>
 
<div class=header-logo>
<a href="/">
<img class=image src="https://newimages.bwwstatic.com/2017/broadwayworld-new-nonretina-2.png" data-ot-retina="https://newimages.bwwstatic.com/2017/broadwayworld-new-retina.png" alt=BroadwayWorld style="height: 92px;max-width: 153px;"/>
</a>
 
</div>
 
<div class=header-pob>
<div class=ad-desktop>
 
<div id=div-gpt-ad-1479672818651-7 style='height:90px; width:970px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-7');});</script>
</div>
</div>
<div class=ad-tab><center>
<div id=div-gpt-ad-1479672818651-6 style='height:90px; width:728px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-6');});</script>
</div></center>
</div>
<div class=ad-mob><center>
<div id=div-gpt-ad-1479672818651-5 style='height:60px; width:468px;'>
<center>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-5');});</script></center>
</div></center>
</div>
<div class=ad-mob-sm><center>
<div id=div-gpt-ad-1479672818651-4 style='height:50px; width:320px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-4');});</script>
</div></center>
</div>
 
</div>
</div>
 
</div>
 
<nav id=main-menu>
<a href="#dat-menu" class=dat-menu-button><i class="fa fa-bars"></i>Show Menu</a>
<div class=main-menu-placeholder style="background-color: #c31f2a;border-top:5px #343233;border-style: solid;height:40px;">
 
<div class=wrapper>
<div class="search-nav right" style="margin-top:-10px; width: 280px;">
<div class=social-nav>
<a href="https://www.facebook.com/BroadwayWorld" target=_new><i class="fa fa-facebook"></i></a>
<a href="https://www.twitter.com/BroadwayWorld" target=_new><i class="fa fa-twitter"></i></a>
<a href="https://www.instagram.com/officialbroadwayworld/" target=_new><i class="fa fa-instagram"></i></a>
<a href="http://broadwayworld.tumblr.com/"><i class="fa fa-tumblr"></i></a>
<a href="/article/BroadwayWorld-is-Everywhere--Interact-with-Us-With-Apps-Social-Media-Today-20151106"><i class="fa fa-apple"></i></a>
</div>
<form action="/search/" id=cse-search-box onSubmit="if($F('search-text')=='Enter Search' || $F('search-text')=='') {alert('You must enter some search criteria'); return false;}">
<input type=text name=q autocomplete=off id=search-text results=0 value="" placeholder=Search style="font-family: 'Montserrat';"/><input type=hidden name=cx value=003281041504712504013:ma8kxnaa1hu /><input type=hidden name=cof value=FORID:11 /><input type=hidden name=ie value=UTF-8 /><input type=hidden class=radio name="search_type[]" id=all value=site checked />
<button type=submit style="float: right;"><i class="fa fa-search"></i></button>
</form>
</div>
<ul class=load-responsive rel="Main Menu">
<li><a href="#" data-ot-css="border-color: orange;" style="padding-top:10px;"><span style="padding-top:10px;">Sections</span></a>
<ul class=sub-menu>
<li><a href="/newsroom/">Latest News</a></li>
<li><a href="/bwidb/">BWW Database</a></li>
<li> <div id=div-gpt-ad-1479672818651-13 style='width:100px;height:30px;float:right;padding-right:4px;padding-top:4px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-13');});</script>
</div>
<a href="/cdsbooksdvds.cfm">CDs/Books/DVDs</a></li>
<li><div id=div-gpt-ad-1479672818651-14 style='width:100px;height:30px;float:right;padding-right:4px;padding-top:4px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-14');});</script>
</div><a href="/grosses.cfm">Grosses - <font color=green>Updated 4/02 </font>
</a></li>
<li><a href="/interviews.cfm">Interviews</a></li>
<li><a href="/gallerynew.cfm">Photos</a></li>
<li><a href="/topic/Podcasts">Podcasts</a></li>
<li><A href="/reviews.cfm">Reviews</A></li>
<li><a href="/tonyawards.cfm">Tony Awards</a>
</ul>
</li>
<li class=has-ot-mega-menu><a href="#"><span>Shows</span></a>
<ul class=ot-mega-menu>
<li>
<div class=widget-split>
 
<div class=widget>
<ul class=menu style="padding-top:0px;">
<li><a href="/shows/shows.php?page=shows">Broadway Shows</a></li>
<li>
<a href="/shows/shows-musicals.php">Broadway Musicals</a>
</li>
<div style='background-color:white;height:35px;width:110px;'>
 
<div id=div-gpt-ad-1479672818651-15 style='height:30px; width:100px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-15');});</script>
</div>
</div>
<li>
<a href="/shows/shows-plays.php">Broadway Plays</a>
</li>
<div id=div-gpt-ad-1479672818651-16 style='width:100px;height:30px;float:right;padding-right:4px;padding-top:4px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-16');});</script>
</div>
<li><a href="/shows/shows.php?page=oshows">Off-Broadway</a></li>
<li><a href="/cabaret/">Cabarets/Concerts</a></li>
<li><a href="/pickashowregional.cfm?ModPagespeed=off">Search by Zip Code</a></li>
<li><a href="/stage-to-screen.cfm">Stage to Screen</a></li>
<li><a href="/shows/shows.php?page=tour">Tours</a></li>
<li><a href="/shows/shows.php?page=westend">West End</a></li>
</ul>
 
</div>
</div>
<div class=widget-split>
 
<div class=widget>
<h4>Hot Discounts</h4>
<div class="w-article-list w-article-list-small">
<div class=item>
<div class=item-header>
<a href="/special-broadway-offers.cfm">
<img src="https://images.broadwayworld.com/dehlogo0320.jpg" alt="Dear Evan Hansen" data-ot-retina="">
</a>
</div>
<div class=item-content>
<h4><a href="/special-broadway-offers.cfm">Dear Evan Hansen</a>
</h4>
<span class=item-meta>
<a href="/special-broadway-offers.cfm" class=item-meta-i>Win Tix!</a>
</span>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/special-broadway-offers.cfm">
<img src="https://images.broadwayworld.com/bww.jpg" alt="Follow BWW!" data-ot-retina="">
</a>
</div>
<div class=item-content>
<h4><a href="/special-broadway-offers.cfm">Follow BWW!</a>
</h4>
<span class=item-meta>
<a href="/special-broadway-offers.cfm" class=item-meta-i>Don't Miss Out!</a>
</span>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/special-broadway-offers.cfm">
<img src="https://images.broadwayworld.com/atacsc.jpg" alt="Atlantic Acting" data-ot-retina="">
</a>
</div>
<div class=item-content>
<h4><a href="/special-broadway-offers.cfm">Atlantic Acting</a>
</h4>
<span class=item-meta>
<a href="/special-broadway-offers.cfm" class=item-meta-i>Spring Classes</a>
</span>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/special-broadway-offers.cfm">
<img src="https://images.broadwayworld.com/wp200.jpg" alt="War Paint" data-ot-retina="">
</a>
</div>
<div class=item-content>
<h4><a href="/special-broadway-offers.cfm">War Paint</a>
</h4>
<span class=item-meta>
<a href="/special-broadway-offers.cfm" class=item-meta-i>Tix on Sale</a>
</span>
</div>
</div>
</div>
<a href="/special-broadway-offers.cfm" class=widget-view-more>View More Discounts</a>
 
</div>
</div>
</li>
</ul>
</li>
<li><a href="#"><span>Chat Boards</span></a>
<ul class=sub-menu>
<li><a href="/board/">Broadway</a><li>
<li>
<a href="/westend/board/">West End</a></li>
<li><a href="/board/index.php?boardname=off">Off-Topic</a>
</li>
<li><a href="/board/index.php?boardname=student">Students</a></li>
</ul>
</li>
<li><a href="#"><span>Jobs</span></a>
<ul class=sub-menu>
<li><a href="/equity.cfm">Equity</a></li>
<li><a href="/nonequity.cfm">Non Equity</a></li>
<li><a href="/classifieds/">Classifieds</a></li>
</ul>
</li>
<li><a href="#"><span>Students</span></a>
<ul class=sub-menu>
<li><a href="/bwwjr.cfm">BWW Junior</a></li>
<li><a href="/studentcenter.cfm">College Center</a></li>
<li><a href="/studentcalendar-elementary.cfm">Elementary Center</a></li>
<li><a href="/studentcalendarhighschool.cfm">High School Center</a></li>
<li><a href="/topic/COLLEGE-CENTER">Student Blogs</a></li> </ul>
</li>
<li><a href="#"><span>Video</span></a>
<ul class=sub-menu>
<li><a href="/tvmainnew.cfm">BroadwayWorld TV</a></li>
<li><a href="/tvmainnew.cfm?type=show">Broadway Show Previews</a></li>
<li><a href="/topic/BACKSTAGE-WITH-RICHARD-RIDGE">Backstage w/ Richie Ridge</a></li>
<li><a href="/tvmainnew.cfm?type=stagetube">Stage Tube</a></li>
<li><a href="/author/TV---Red-Carpets">Red Carpets</a></li>
<li><a href="/author/TV---Opening-Night-Special">Opening Nights</a></li>
<li><a href="/author/TV---Press-Previews">Press Previews</a></li>
</ul>
 
</div>
</div>
 
</nav>
 
</div>
<div id=content>
 
<div class=wrapper>
 
<div class="paragraph-row portus-main-content-panel">
<div class=column12>
<div class=portus-main-content-s-block>
 
<div class=portus-main-content>
<div class=theiaStickySidebar>
<div class=portus-main-article-block>
<table width=100% cellpadding=5 cellspacing=5><tr><td>
<strong><center><h1 class=header>BroadwayWorld.com Privacy Policy<br></h1></strong>
<p></center>
<font face=verdana size=-1>
BroadwayWorld.com ** DOES NOT DISCLOSE YOUR PERSONAL INFORMATION** to outside parties. We only release account and other personal information when we believe release is appropriate to comply with law; enforce or apply our <a href="http://www.broadwayworld.com/board/guide.cfm">Terms and Conditions of Use</a> and other agreements; or protect the rights, property, or safety of BroadwayWorld.com, our users, or others. This includes exchanging information with other companies and organizations for fraud protection.
<br><Br>Periodically, aggregate statistics will be provided to our advertising partners, but *NO* specfic or personal information will be included. We do ask you if you are interested in receiving relevant information or offers from theater vendors via e-mail. You may choose to receive this information or easily "unsubscribe" and choose NOT TO RECEIVE any e-mails from BroadwayWorld.com outside your newsletter subscriptions.
<br><Br>If you choose to receive additional information other than your newsletters, this information is sent to you directly by BroadwayWorld.com. If you choose not to receive other information from us, this WILL NOT AFFECT your regular subscriptions. If you choose to discontinue your newsletter subscriptions, you may also do this by simply unsubscribing at any time during your subscription period. When you send us your "unsubscribe" request to each of our offerings, YOUR RECORD GETS PERMANENTLY REMOVED from these databases as we receive your remove requests.
<br><Br>We use third-party advertising companies to serve ads when you visit our Web site. These companies may use aggregated information (not including your name, address, email address or telephone number) about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, <a href="http://www.networkadvertising.org/managing/opt_out.asp">click here</a>.
<br><Br>
Our properties may feature Nielsen proprietary measurement software, which will allow you to contribute to market research, such as Nielsen TV Ratings. To learn more about the information that Nielsen software may collect and your choices with regard to it, please see the Nielsen Digital Measurement Privacy Policy at <a href="http://www.nielsen.com/digitalprivacy">http://www.nielsen.com/digitalprivacy</a>.
<br><Br>
Thank you for making BroadwayWorld.com the web's premier theater resource and please let us know how we can serve you better!
</center>
</center>
</font>
</td></tr></table>
</div></div></div>
<aside class="sidebar portus-sidebar-small">
<div class=theiaStickySidebar>
 
<div class=widget>
<h3 style="margin-bottom:-4px;">Hot Stories</h3>
<div class=w-article-list style="margin-top:1px;">
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Jane-Krakowski-and-Christopher-Jackson-Will-Announce-2017-Tony-Nominations-20170403"><img src="https://images.bwwstatic.com/columnpic7/1700C7A3B05-EEE9-3A29-B6AD4EC58CA2020A.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic7/3400C7A3B05-EEE9-3A29-B6AD4EC58CA2020A.jpg" alt="Krakowski and Jackson Will Announce 2017 Tony Nominations" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Jane-Krakowski-and-Christopher-Jackson-Will-Announce-2017-Tony-Nominations-20170403">Krakowski and Jackson Will Announce 2017 Tony Nominations</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Photo-Coverage-Sara-Bareilles-Will-Swenson-Chris-Diamantopoulos-Take-First-Bows-in-WAITRESS-20170401"><img src="https://images.bwwstatic.com/columnpic7/1708DF89394-AAA9-6392-15992EEDFC9BCAE9.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic7/3408DF89394-AAA9-6392-15992EEDFC9BCAE9.jpg" alt="Photo Coverage: Sara Bareilles, Will Swenson, Chris Diamantopoulos Take First Bows in WAITRESS" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Photo-Coverage-Sara-Bareilles-Will-Swenson-Chris-Diamantopoulos-Take-First-Bows-in-WAITRESS-20170401">Photos: Sara Bareilles, Will Swenson, Chris Diamantopoulos Take First Bows in WAITRESS</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Review-Roundup-AMELIEs-Broadway-Dreams-Come-True-Tonight-Updating-Live-20170403"><img src="https://images.bwwstatic.com/columnpic7/1701576360main100.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic7/3401576360main100.jpg" alt="Review Roundup: AMELIE's Broadway Dreams Come True Tonight! - All the Reviews!" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Review-Roundup-AMELIEs-Broadway-Dreams-Come-True-Tonight-Updating-Live-20170403">Review Roundup: AMELIE's Broadway Dreams Come True Tonight! - All the Reviews!</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Review-Roundup-THE-PLAY-THAT-GOES-WRONG-Attempts-to-Open-on-Broadway-Updating-Live-20170402"><img src="https://images.bwwstatic.com/columnpic7/1707524FF77-EA34-DB67-C1958C20A525F0B9.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic7/3407524FF77-EA34-DB67-C1958C20A525F0B9.jpg" alt="Review Roundup: THE PLAY THAT GOES WRONG Attempts to Open on Broadway - All the Reviews!" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Review-Roundup-THE-PLAY-THAT-GOES-WRONG-Attempts-to-Open-on-Broadway-Updating-Live-20170402">Review Roundup: THE PLAY THAT GOES WRONG Attempts to Open on Broadway - All the Reviews!</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/Sara-Bareilles-Hits-Times-Square-with-the-WAITRESS-Band-in-Tow-20170401"><img src="https://images.bwwstatic.com/columnpic7/170A0C17D89-E726-9CBC-A03D5CBFCCB2DB51.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic7/340A0C17D89-E726-9CBC-A03D5CBFCCB2DB51.jpg" alt="Sara Bareilles Hits Times Square with the WAITRESS Band in Tow" width=180 height=74 /></a>
<h4 class=hot><a href="/article/Sara-Bareilles-Hits-Times-Square-with-the-WAITRESS-Band-in-Tow-20170401">Sara Bareilles Hits Times Square with the WAITRESS Band in Tow</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="/article/From-Screen-to-Stage-An-In-Depth-Look-at-Broadways-Latest-Adaptations-20170401"><img src="https://images.bwwstatic.com/columnpic7/1706BF4C5E8-91C2-522E-5D0F81AF10EF22BC.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic7/3406BF4C5E8-91C2-522E-5D0F81AF10EF22BC.jpg" alt="From Screen to Stage - An In-Depth Look at Broadway's Latest Adaptations" width=180 height=74 /></a>
<h4 class=hot><a href="/article/From-Screen-to-Stage-An-In-Depth-Look-at-Broadways-Latest-Adaptations-20170401">From Screen to Stage - An In-Depth Look at Broadway's Latest Adaptations</a></h4>
</div>
</div>
<div class=item>
<div class=item-content onMouseOver="this.style.backgroundColor='#d4d4d4'" onMouseOut="this.style.backgroundColor='#f8f8f8'">
<a href="http://www.broadwayworld.com/article/BWW-Morning-Brief-April-4th-2017-INDECENT-LIGHTNING-THIEF-and-More-20170404"><img src="https://images.bwwstatic.com/columnpic7/170263E6366-EA5A-9351-D6EE161E334E1D89.jpg" data-ot-retina="https://images.bwwstatic.com/columnpic7/340263E6366-EA5A-9351-D6EE161E334E1D89.jpg" alt="BWW Morning Brief April 4 - INDECENT, LIGHTNING THIEF & More!" width=180 height=74 /></a>
<h4 class=hot><a href="http://www.broadwayworld.com/article/BWW-Morning-Brief-April-4th-2017-INDECENT-LIGHTNING-THIEF-and-More-20170404">Morning Brief April 4 - INDECENT, LIGHTNING THIEF & More!</a></h4>
</div>
</div>
</div>
 
</div>
 
<div class=widget>
<div class=do-space><Center>
<script>var width=window.innerWidth||document.documentElement.clientWidth||document.body.clientWidth;if(width>=1050){document.write('<div id="div-gpt-ad-1479672818651-160" style="width:160px; height:600px;">');googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-160');});document.write('</div>');}</script>
</Center> </Center> </div>
 
</div>
</div>
</aside>
<aside class="sidebar portus-sidebar-large">
<div class=theiaStickySidebar>
 
<div class=widget style="width:300px;">
<h3>BroadwayWorld TV</h3>
<div style="align-content: center;" align=center>
<i class="fa fa-play-circle-o" style="font-size:30pt;position: absolute;
    top: 90px;
        opacity: 0.5;
color: white;
    right:5px;
    z-index: 10000;
    height: 100px;"></i>
<ul id=demo1>
<li><a href="/article/BWW-TV-Kyle-Taylor-Parker-Sings-Cover-of-Run-and-Tell-That-from-HAIRSPRAY-for-Soul-Sessions-Series-20170404"><img src="https://images.bwwstatic.com/columnpic7/3404CF5A862-01E5-0DFF-6781E6D2D1D21D4F.jpg" alt="" width=400 height=165>Kyle Taylor Parker Sings Soulful Cover of HAIRSPRAY Tune</a>
</li>
<li><a href="/article/BWW-TV-Broadway-Comes-Out-to-Celebrate-the-SPEECH-DEBATE-Premiere-20170404"><img src="https://images.bwwstatic.com/columnpic7/3404441B7E4-F658-AE25-EA581514DF59EEBD.jpg" alt="" width=400 height=165>Broadway Comes Out to Celebrate the SPEECH & DEBATE Premiere!</a>
</li>
<li><a href="/article/BWW-TV-The-Time-Is-Still-Right-Go-Inside-Opening-Night-of-AMELIE-20170404"><img src="https://images.bwwstatic.com/columnpic7/x34045CD7A6A-C0D5-BBE0-CDA57966949F1016.jpg.pagespeed.ic.P61_9-1aLc.jpg" alt="" width=400 height=165>The Time Is Still Right! Go Inside Opening Night of AMELIE</a>
</li>
<li><a href="/article/VIDEO-Sally-Field-Reveals-Why-She-Decided-to-Return-to-Broadway-20170404"><img src="https://images.bwwstatic.com/columnpic7/340448E17F4-E88C-3332-AB7F25A0C36C44D0.jpg" alt="" width=400 height=165>Sally Field Reveals Why She Decided to Return to Broadway</a>
</li>
<li><a href="/article/BWW-TV-See-Harvey-Fierstein-and-More-in-Highlights-from-GENTLY-DOWN-THE-STREAM-at-The-Public-20170404"><img src="https://images.bwwstatic.com/columnpic7/3403F7B0245-0E4E-977B-E2E35A990F243619.jpg" alt="" width=400 height=165>See Highlights from GENTLY DOWN THE STREAM at The Public</a>
</li>
<li><a href="/article/Lea-Michele-Shares-First-Listen-to-New-Song-Run-To-You-from-Forthcoming-Album-20170404"><img src="https://images.bwwstatic.com/columnpic7/3403EDA7B88-F9FA-DDB9-B9CA1F6EC6FE93B9.jpg" alt="" width=400 height=165>Lea Michele Shares First Listen to New Song 'Run To You'</a>
</li>
<li><a href="/article/VIDEO-James-Corden-Stephen-Curry-Sing-Moana-Frozen-Carpool-Karaoke-20170404"><img src="https://images.bwwstatic.com/columnpic7/3403937625F-9B10-18ED-582AF09451501D02.jpg" alt="" width=400 height=165>James Corden & Steph Curry Sing Disney 'Carpool Karaoke'</a>
</li>
<li><a href="/article/VIDEO-On-This-Day-April-4--The-Broadway-Babies-of-FOLLIES-Open-at-the-Winter-Garden-Theater-20170404"><img src="https://images.bwwstatic.com/columnpic7/x3402609D375-0D49-33D7-82EFC020CCDDBEC4.jpg.pagespeed.ic.yaQBFh4yCC.jpg" alt="" width=400 height=165>On This Day, April 4- The Broadway Babies of FOLLIES Open at the Winter Garden Theater</a>
</li>
<li><a href="/article/VIDEO-Kate-Walsh-Talks-New-Off-Broadway-Play-IF-I-FORGET-on-Live-20170403"><img src="https://images.bwwstatic.com/columnpic7/x34013714449-9617-F889-176D9B96A3C95436.jpg.pagespeed.ic.PT1FUavpVX.jpg" alt="" width=400 height=165>Kate Walsh Talks Forgetting Her Lines in Off-Broadway's IF I FORGET!</a>
</li>
<li><a href="/article/VIDEO-First-Listen-Constantine-Maroulis-Shares-New-Single-All-About-You-20170403"><img src="https://images.bwwstatic.com/columnpic7/x34008431FD0-E3CB-A04A-8390589368306BBA.jpg.pagespeed.ic.PUyVXuF7uQ.jpg" alt="" width=400 height=165>First Listen - Constantine Maroulis Shares New Single 'All About You'</a>
</li>
</ul>
<script>jQuery(document).ready(function(){var demo1=$("#demo1").slippry({});});</script>
</div>
 
<center>
<div id=div-gpt-ad-1479672818651-0 style='width:300px;overflow:hidden;position:relative;padding-top:6px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-0');});</script>
</div>
</center>
<h3 style="margin-top:10px;">Ticket Central <small style="float:right;padding-top:3px;"><a href="/shows/shows.php?page=shows" style="color: white;">Browse All Shows</a></small></h3>
 
<table border=0 style="padding:0px;width:100%;"><tr><td valign=top style="padding-right:8px;padding-left:0px;margin-left:0px;">
<div id=div-gpt-ad-1479672818651-10 style='height:135px; width:130px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-10');});</script>
</div>
</td>
<td style="padding-top:0px;margin-top:0px;vertical-align:middle;">
<li class=special><a href="/special-broadway-offers.cfm">WAR PAINT</a></li>
<li class=special><a href="/special-broadway-offers.cfm">DEAR EVAN HANSEN</a></li>
<li class=special><a href="/special-broadway-offers.cfm">54 BELOW</a></li>
<li class=special><a href="/special-broadway-offers.cfm">ATLANTIC ACTING</a></li>
<li class=special><a href="/special-broadway-offers.cfm">BWW PERSONAL WEBSITES</a></li>
</td></tr></table>
 
<div class=widget style="border-bottom: none; ">
<h3 style="margin-top:10px;">Watch Now on BWW TV <small style="float:right;padding-top:4px;"><a href="http://www.broadwayworld.com/tvmainnew.cfm" style="color: white;">More</a></small></h3>
<div class=widget-instagram-photos style="height: 135px;">
<div class=item>
<div class=item-header>
<a href="/article/VIDEO-GRANNIE-is-the-Rags-to-Riches-Story-That-Everyone-KnowsAlmost-20170402">
<img src="https://images.bwwstatic.com/columnpic7/170D56E26E1-EBE0-F753-2D2190000B6E4E2B.jpg" alt="GRANNIE is the Rags to Riches Story That Everyone Knows...Almost" width=180 height=74>
</a>
</div>
<div class=item-content style="height:60px;">
<h4><a href="/article/VIDEO-GRANNIE-is-the-Rags-to-Riches-Story-That-Everyone-KnowsAlmost-20170402">GRANNIE is the Rags to Riches Story That Everyone Knows...Almost</a></h4>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/article/VIDEO-On-This-Day-March-29--NEWSIES-Seize-the-Day-on-Opening-Night-20170329">
<img src="https://images.bwwstatic.com/columnpic7/170E5133CBA-0FE2-222F-404A580C31120A66.jpg" alt="On This Day, March 29- NEWSIES Seize the Day on Opening Night" width=180 height=74>
</a>
</div>
<div class=item-content style="height:60px;">
<h4><a href="/article/VIDEO-On-This-Day-March-29--NEWSIES-Seize-the-Day-on-Opening-Night-20170329">On This Day, March 29- NEWSIES Seize the Day on Opening Night</a></h4>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/article/VIDEO-Andrew-Rannells-Talks-Girls-Performance-Takes-Pup-Quiz-on-TONIGHT-SHOW-20170330">
<img src="https://images.bwwstatic.com/columnpic7/170355DA451-F40B-C560-80728E855D272196.jpg" alt="Andrew Rannells Talks 'Girls' Performance; Takes 'Pup' Quiz on 'TONIGHT'" width=180 height=74>
</a>
</div>
<div class=item-content style="height:60px;">
<h4><a href="/article/VIDEO-Andrew-Rannells-Talks-Girls-Performance-Takes-Pup-Quiz-on-TONIGHT-SHOW-20170330">Andrew Rannells Talks 'Girls' Performance; Takes 'Pup' Quiz on 'TONIGHT'</a></h4>
</div>
</div>
<div class=item>
<div class=item-header>
<a href="/article/VIDEO-On-This-Day-March-31--Tune-In-Turn-On-Drop-Out-HAIR-Returns-to-Broadway-20170331">
<img src="https://images.bwwstatic.com/columnpic7/17054310186-CED9-A7CC-2A8CD87B076D55E6.jpg" alt="On This Day, March 31- Tune In, Turn On, Drop Out: HAIR Returns to Broadway!" width=180 height=74>
</a>
</div>
<div class=item-content style="height:60px;">
<h4><a href="/article/VIDEO-On-This-Day-March-31--Tune-In-Turn-On-Drop-Out-HAIR-Returns-to-Broadway-20170331">On This Day, March 31- Tune In, Turn On, Drop Out: HAIR Returns to Broadway!</a></h4>
</div>
</div>
</div>
 
</div>
<center>
 
<div class=widget style="border-bottom:0px;margin-bottom:0px;">
<center>
<div class=do-space>
<center>
<div id=div-gpt-ad-1479672818651-1 style='width:300px;overflow:hidden;position:relative;padding-top:6px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-1');});</script>
</div></center>
</div>
 
</div>
 
<div class=widget style="border-bottom:0px;margin-bottom:8px;">
<h3>Subscribe Now</h3>
<div class=widget-subscribe>
<div>
<p>Register now for discounts, specials & more!</p>
</div>
<form action="/register.cfm" method=post>
<label class=label-input>
<span>E-mail address</span>
<input name=email type=email value=""/>
</label>
<input type=submit class=button value=Subscribe />
</form>
</div>
 
</div>
<div class=widget style="border-bottom:0px;padding-bottom:4px;margin-bottom:10px;">
<center>
<div class=do-space>
<center>
<div id=div-gpt-ad-1479672818651-2 style='width:300px;overflow:hidden;position:relative;padding-top:6px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-2');});</script>
</div> </center> </div></center>
 
</div>
</center>
<div class=widget style="border-bottom:0px;margin-bottom:8px;overflow: hidden;">
<h3>Recommended for You</h3>
<div id=test1>
</div>
<script>function createIframe(){var i=document.createElement("iframe");i.src="https://www2.broadwayworld.com/recommended.cfm?ModPagespeed=off";i.scrolling="no";i.frameborder="0";i.width="300px";i.height="380px";document.getElementById("test1").appendChild(i);};if(window.addEventListener)window.addEventListener("load",createIframe,false);else if(window.attachEvent)window.attachEvent("onload",createIframe);else window.onload=createIframe;</script>
 
</div>
<div class=widget>
<div class=do-space><center>
<div id=div-gpt-ad-1479672818651-3 style='width:300px;overflow:hidden;position:relative;padding-top:6px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-3');});</script>
</div> </center> </div>
 
</div>
</div>
</aside>
</div>
</div>
 
</div>
 
</div>
 
</div>
 
</div>
 
<div id=footer>
<div id=footer-widgets>
<div class=wrapper>
<div class=paragraph-row>
<div class=column12>
 
<div class=widget>
<center>
 
<div id=div-gpt-ad-1479672818651-New728468Bottom>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-New728468Bottom');});</script>
</div><br>
 
</div>
</div>
<div class=paragraph-row style="margin-top: 12px;">
<div class=column3>
<div class=widget>
<h3>Follow Us</h3>
<div>
<a href="https://www.facebook.com/BroadwayWorld" target=_new><div class=short-icon-text>
<i class="fa fa-facebook"></i>
<span style="color:white;">Facebook</span>
</div></a>
<a href="https://www.twitter.com/BroadwayWorld" target=_new><div class=short-icon-text>
<i class="fa fa-twitter"></i>
<span style="color:white;">Twitter</span>
</div></a>
<a href="https://www.instagram.com/officialbroadwayworld/" target=_new>
<div class=short-icon-text>
<i class="fa fa-instagram"></i>
<span style="color:white;">Instagram</span>
</div></a>
<a href="http://broadwayworld.tumblr.com/" target=_new>
<div class=short-icon-text>
<i class="fa fa-tumblr"></i>
<span style="color:white;">Tumblr</span>
</div></a>
<a href="/article/BroadwayWorld-is-Everywhere--Interact-with-Us-With-Apps-Social-Media-Today-20151106" target=_new>
<div class=short-icon-text>
<i class="fa fa-apple"></i>
<span style="color:white;">Apps</span>
</div></a>
</div>
</div>
</div>
<div class=column3>
<div class=widget>
<h3>BWW Sister Sites</h3>
<ul class=menu>
<li><a href="/bwwtv/index.cfm">TV/MOVIES</a></li>
<li><a href="/bwwmusic/index.cfm">MUSIC</a></li>
<li><a href="/bwwbooks/index.cfm">BOOKS</a></li>
<li><a href="/bwwclassical/index.cfm">CLASSICAL MUSIC</a></li>
<li><a href="/bwwdance/index.cfm">DANCE</a></li>
<li><a href="/bwwopera/index.cfm">OPERA</a></li>
</ul>
</div>
</div>
<div class=column3 style="width: 46%">
<div class=widget>
<h3>BWW Hub</h3>
<table>
<Tr><Td style="padding-bottom: 10px;vertical-align: middle;"> <div class=item>
<a href="/bwwtv/article/VIDEO-James-Corden-Stephen-Curry-Sing-Moana-Frozen-Carpool-Karaoke-20170404" style="color:white;font-size:12pt;">
<img src="https://images.bwwstatic.com/columnpic7/1703937625F-9B10-18ED-582AF09451501D02.jpg" alt="VIDEO: James Corden & Steph Curry Sing Disney 'Carpool Karaoke'" data-ot-retina="" style="float:left;padding-right: 8px;" width=180 height=74>VIDEO: James Corden & Steph Curry Sing Disney 'Carpool Karaoke'
</a>
</div></Td></Tr>
<Tr><Td style="padding-bottom: 10px;vertical-align: middle;"> <div class=item>
<a href="/bwwtv/article/Hugh-Jackman-Saving-You-from-a-Burning-Building-Its-Every-Girls-Dream-and-Zac-Efrons-Reality-20170401" style="color:white;font-size:12pt;">
<img src="https://images.bwwstatic.com/columnpic7/1704586AA54-C4E2-E9F4-7EAF4FBA41D5B590.jpg" alt="Zac Efron Saved from Burning Set by Hugh Jackman, Real Life Action Hero" data-ot-retina="" style="float:left;padding-right: 8px;" width=180 height=74>Zac Efron Saved from Burning Set by Hugh Jackman, Real Life Action Hero
</a>
</div></Td></Tr>
</table>
</div>
</div>
</div>
</div>
</div>
<div id=footer-info>
<div class=wrapper>
<ul class=right>
<li><a href="/mediakit.cfm" style="color:white;">Advertising Info</a></li>
<li><a href="/contact.cfm" style="color:white;">Contact Us</a></li>
<li><a href="/article/Join-the-BWW-Interns-Contributors-Team-20141229" style="color:white;">Join the Team</a></li>
<li><a href="/submitnews.cfm" style="color:white;">Submit News</a></li>
<li><a href="/privacy.cfm" style="color:white;">Privacy Policy</a></li>
</ul>
<p>&copy; 2017 - Copyright <a href="http://www.wisdomdigital.com" style="color:white;"><b>Wisdom Digital Media</b></a>, all rights reserved. </p>
</div>
</div>
 
</div>
 
</div>
 
<script src="https://nav.bwwstatic.com/2017/bootstrap-3.3.7-dist/js/bootstrap.min.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/theia15min.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/modernizr-custom.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/jsiscroll/iscroll-lite.js.pagespeed.jm.zJuLqn_8je.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/dat-menu-fixed.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/theme-scripts1113dcompressed.js" defer></script>
<script src="https://nav.bwwstatic.com/2017/slippry/dist/slippry.min.js"></script>
<script src="https://nav.bwwstatic.com/2014/js/shadow-interstitial2017.js"></script>
<script>Shadowbox.init({skipSetup:true});</script>
<div id=div-gpt-ad-1479672818651-8 style='width:1px; height:1px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-8');});</script></div>
</div></div></div>
 
<script>var _comscore=_comscore||[];_comscore.push({c1:"2",c2:"18162732"});(function(){var s=document.createElement("script"),el=document.getElementsByTagName("script")[0];s.async=true;s.src=(document.location.protocol=="https:"?"https://sb":"http://b")+".scorecardresearch.com/beacon.js";el.parentNode.insertBefore(s,el);})();</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=18162732&cv=2.0&cj=1"/>
</noscript>
 
 
<script>var _qevents=_qevents||[];(function(){var elem=document.createElement('script');elem.src=(document.location.protocol=="https:"?"https://secure":"http://edge")+".quantserve.com/quant.js";elem.async=true;elem.type="text/javascript";var scpt=document.getElementsByTagName('script')[0];scpt.parentNode.insertBefore(elem,scpt);})();_qevents.push({qacct:"p-61Pu-3TC5IB0I",});</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-61Pu-3TC5IB0I.gif" border=0 height=1 width=1 alt=Quantcast />
</div>
</noscript>
<script>document.body.onload=function(){if('safari'in window&&'pushNotification'in window.safari){var permissionData=window.safari.pushNotification.permission('web.com.broadwayworld');checkRemotePermission(permissionData);}};var checkRemotePermission=function(permissionData){if(permissionData.permission==='default'){window.safari.pushNotification.requestPermission('https://secure.broadwayworld.com/push','web.com.broadwayworld',{},checkRemotePermission);}else if(permissionData.permission==='denied'){}else if(permissionData.permission==='granted'){}};</script>
 
 
</body>
 
<div id=div-gpt-ad-1479672818651-Onscroll style='width:1px; height:1px;'>
<script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-1479672818651-Onscroll');});</script></div>
</html>
