<!DOCTYPE html>
<html class="no-js" lang="en">
<head>

<meta http-equiv=X-UA-Compatible content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@mentor_graphics" />
<meta name="twitter:creator" content="@mentor_graphics" />
<meta property="og:title" content="Mentor Graphics Privacy Policy" />
<meta property="og:description" content="Privacy Policy" />
<meta property="og:image" content="https://mgc-images.imgix.net/home/MentorASBLogoBlackHires-AFBF7559.png" />
<meta property="og:url" content="https://www.mentor.com/terms_conditions/privacy" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en" />
<title>Mentor Graphics Privacy Policy - Mentor Graphics</title>
<meta name="Description" id="Description" content="Privacy Policy" />
<meta name="EntityType" id="EntityType" content="" />
<meta name="SubsiteType" id="SubsiteType" content="" />
<meta name="LastModified" id="LastModified" content="2019-03-29" />
<meta name="Section" id="Section" content="" />
<link type="text/css" rel="stylesheet" href="https://static.mentor-cdn.com/css/mgc/generated/prod/v253/mgc_agg.css" />
<script type="text/javascript" src="https://static.mentor-cdn.com/common/lib/jquery/1.12.4/jquery.min.js"></script>
<script src="https://static.mentor-cdn.com/common/lib/jquery/jquery-migrate-1.4.1.min.js"></script>
<script src="https://static.mentor-cdn.com/common/lib/raven-js/1.2.0/raven.min.js"></script>
<script>
Raven.config('https://bc4da2b20bcf452c9ada8eebf5999184@app.getsentry.com/29357', {
ignoreErrors: [
'Blocked a frame with origin "http://www.mentor.com" from accessing a cross-origin frame.'
],
ignoreUrls: [
/extensions\//i,
/^chrome:\/\//i,
/KW__injectedScriptMin\//i,
/s3\.mentor\.com\/search\//i
],
whitelistUrls: [/mentor\.com/,/mentor-dev\.com/,/static\.mentor-cdn\.com/]
}).install();
</script>

<script type="text/javascript">STATIC_ASSET_DOMAIN = "";STATIC_ASSET_DOMAIN_LOGIC = function(p) { return p };static_host='https://static.mentor-cdn.com';lang="en";</script>
<script type="text/javascript" src="https://static.mentor-cdn.com/common/jwplayer/6.10.4906/jwplayer.js"></script>
<script type="text/javascript" src="https://static.mentor-cdn.com/mgc/app/generated/prod/v200/app.min.js"></script>
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=aljPzogMmq">
<link rel="icon" type="image/png" href="/favicon-32x32.png?v=aljPzogMmq" sizes="32x32">
<link rel="icon" type="image/png" href="/favicon-16x16.png?v=aljPzogMmq" sizes="16x16">
<link rel="manifest" href="/manifest.json?v=aljPzogMmq">
<link rel="mask-icon" href="/safari-pinned-tab.svg?v=aljPzogMmq" color="#3769ac">
<link rel="shortcut icon" href="/favicon.ico?v=aljPzogMmq">
<meta name="theme-color" content="#ffffff">
</head>
<body class=" mgc flex-body">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MB9JF3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MB9JF3');</script>
<!-- End Google Tag Manager -->
<a name="__topdoc__"></a>
<!--/pageElement:HEADER-->
<!-- //// MGC PAGE START  //// -->

<header class="header-main bg-secondary flex-none" role="navigation">
<div class="header-main-logo">
<a class="logo-mentor m-t-xs" href="https://mentor.com"><span class="sr-only">Mentor, A Siemens Business</span></a>
</div>
<div id="user" class="header-main-ancillary m-l-auto text-inverse user">


	
	
		
	
	
	
	
    <div>
    	
    		<a href="https://www.mentor.com/signin?next=https%3A%2F%2Fwww%2Ementor%2Ecom%2Fterms%5Fconditions%2Fprivacy" class="text-no-underline signintoggler show-hide toggle-collapsed" id="signin-link">
        		<svg class="icon icon-user icon-circle-border" aria-hidden="true"><use xlink:href="#icon-user"></use></svg> <span class="hidden-sm-down">Sign In</span>
            </a>
            <span class="text-pipe hidden-sm-down">|</span>
    		<div id="signin">
    			
    			<form name="login" id="login" action="https://www.mentor.com/signin?next=https%3A%2F%2Fwww%2Ementor%2Ecom%2Fterms%5Fconditions%2Fprivacy" method="post">
    				<label for="emailAddress">Email Address</label>
    				<input type="text" id="emailAddress" name="email" class="text" autocomplete="off" />
    				<label for="passwordField">Password</label>
    				<input type="password" id="passwordField" name="password" class="text" autocomplete="off" />
    				<a href="https://signin.mentor.com/reset?next=https%3A%2F%2Fwww%2Ementor%2Ecom%2Fterms%5Fconditions%2Fprivacy" class="display-block m-t-sm text-xs">Forgot Password?</a>
    				
    				<div class="text-right m-t-sm">
        				<input type="submit" class="btn btn-info btn-sm" value="Sign In">
                    </div>
    				<a href="#" id="signin-cancel" class="close">&times;</a>
    			</form>
    		</div>
    	
    	<span class="user-action hidden-sm-down"><a class="text-no-underline" href="https://signin.mentor.com/register?next=https%3A%2F%2Fwww%2Ementor%2Ecom%2Fterms%5Fconditions%2Fprivacy">Create Account</a>
    	</span>
	</div>

</div>
<nav class="header-main-navigation navbar-primary" id="primary">
<ul>
<li class="products" id="navbar-products">
<a href="/products/" title="Products">
<i class="menu-icon menu-icon-animated position-relative m-r-sm" style="top: -3px;"><span></span></i>
Products & Solutions
</a>
<li class="training"><a href="/training/" title="Training and Services">Training</a></li>
<li class="services"><a href="/training-and-services/" title="Training and Services">Services</a></li>
<li class="company"><a href="/company/" title="Company">Company</a></li>
<li class="blogs"><a href="/blogs/" title="Blogs">Blogs</a></li>
<li class="support"><a href="/support/" title="Support">Support</a></li>
</ul>
</nav>
</header>
<div class="primary-dropdown bg-gray-lightest" id="nav-products">
<div class="container p-t-lg p-b">
<div class="row">
<div class="col-md-4 col-xs-12 display-flex flex-column">
<div class="card border-none flex-1">
<div class="card-header bg-secondary text-inverse" style="padding: 5px"></div>
<div class="card-block border-bottom">
<div class="text-center">
<img src="https://s3.amazonaws.com/images.mentor.com/home/icon-eda.svg" alt="EDA Icon" width="64">
<h2 class="text-lg m-t-sm m-b-0 text-dark">Electronic Design Automation</h2>
</div>
</div>
<div class="card-block">
<ul class="list-unstyled text-emphasis text-md m-b-0">
<li><a href="/esl/" class="link-hover-cta"><span>Virtual Prototyping</span></a></li>
<li><a href="/hls-lp/catapult-high-level-synthesis/" class="link-hover-cta"><span>High-Level Synthesis</span></a></li>
<li><a href="/hls-lp/powerpro-rtl-low-power/" class="link-hover-cta"><span>RTL Low-Power Opt & Analysis</span></a></li>
<li><a href="/products/fv/" class="link-hover-cta"><span>Functional Verification</span></a></li>
<li><a href="/tannereda/ams-ic" class="link-hover-cta"><span>Tanner AMS IC Design Flow</span></a></li>
<li><a href="/tannereda/mems-design" class="link-hover-cta"><span>Tanner MEMS Design Flow</span></a></li>
<li><a href="/products/ic_nanometer_design/" class="link-hover-cta"><span>IC Design</span></a></li>
<li><a href="/products/ic-manufacturing/" class="link-hover-cta"><span>IC Manufacturing</span></a></li>
<li><a href="/products/silicon-yield/" class="link-hover-cta"><span>IC Test</span></a></li>
<li><a href="/products/ip/" class="link-hover-cta"><span>Intellectual Property </span></a></li>
<li><a href="/products/fpga/" class="link-hover-cta"><span>FPGA</span></a></li>
</ul>
</div>
</div>
</div>
<div class="col-md-4 col-xs-12 display-flex flex-column">
<div class="card border-none">
<div class="card-header bg-info text-inverse" style="padding: 5px"></div>
<div class="card-block border-bottom">
<div class="text-center">
<img src="https://s3.amazonaws.com/images.mentor.com/home/icon-systems.svg" alt ="Systems Icon" width="64">
<h2 class="text-lg m-t-sm m-b-0 text-dark">Systems</h2>
</div>
</div>
<div class="card-block">
<ul class="list-unstyled text-emphasis text-md m-b-0">
<li><a href="/products/electrical-design-software/" class="link-hover-cta"><span>Electrical Systems, Networks & Harnesses</span></a></li>
<li><a href="/products/sm/" class="link-hover-cta"><span>System Modeling & Design Management</span></a></li>
<li><a href="/pcb/" class="link-hover-cta"><span>PCB & IC Package Design</span></a></li>
<li><a href="/pcb-manufacturing-assembly/" class="link-hover-cta"><span>PCB Manufacturing, Assembly & Test</span></a></li>
</ul>
</div>
</div>
<div class="card border-none flex-1">
<div class="card-header bg-secondary-dark text-inverse" style="padding: 5px"></div>
<div class="card-block border-bottom">
<div class="text-center">
<a href="/products/mechanical/" title="CAE Simulation & Test">
<img src="https://s3.amazonaws.com/images.mentor.com/home/icon-thermal.svg" alt="Thermal Icon" width="64">
<h2 class="text-lg m-t-sm m-b-0 text-dark">CAE Simulation & Test</h2>
</a>
</div>
</div>
<div class="card-block">
<ul class="list-unstyled text-emphasis text-md m-b-0">
<li><a href="/products/mechanical/floefd/" class="link-hover-cta"><span>CAD-Embedded CFD</span></a></li>
<li><a href="/products/mechanical/flotherm/" class="link-hover-cta"><span>Electronics Cooling CFD</span></a></li>
<li><a href="/products/mechanical/magnet" class="link-hover-cta"><span>Low-Frequency Electromagnetics & Motors</span></a></li>
<li><a href="/products/mechanical/micred/" class="link-hover-cta"><span>Semiconductor Device Thermal Testing</span></a></li>
<li><a href="/products/mechanical/flomaster/flomaster/" class="link-hover-cta"><span>1D CFD</span></a></li>
</ul>
</div>
</div>
</div>
<div class="col-md-4 col-xs-12 display-flex flex-column">
<div class="card border-none">
<div class="card-header bg-primary text-inverse" style="padding: 5px"></div>
<div class="card-block border-bottom">
<div class="text-center">
<a href="/mentor-automotive" title="Mentor Automotive">
<img src="https://s3.amazonaws.com/images.mentor.com/home/icon-automotive.svg" alt="Automotive Icon" width="64">
<h2 class="text-lg m-t-sm m-b-0 text-dark">Automotive</h2>
</a>
</div>
</div>
<div class="card-block">
<ul class="list-unstyled text-emphasis text-md m-b-0">
<li><a href="/mentor-automotive/connectivity/" class="link-hover-cta"><span>Connectivity</span></a></li>
<li><a href="/mentor-automotive/electrification/" class="link-hover-cta"><span>Electrification</span></a></li>
<li><a href="/mentor-automotive/autonomous/" class="link-hover-cta"><span>Autonomous</span></a></li>
<li><a href="/mentor-automotive/architecture/" class="link-hover-cta"><span>Architecture</span></a></li>
</ul>
</div>
</div>
<div class="card border-none flex-1">
<div class="card-header bg-secondary text-inverse" style="padding: 5px"></div>
<div class="card-block border-bottom">
<div class="text-center">
<a href="/embedded-software" title="Mentor Embedded">
<img src="https://s3.amazonaws.com/images.mentor.com/home/icon-embedded.svg" alt="Embedded Icon" width="64">
<h2 class="text-lg m-t-sm m-b-0 text-dark">Embedded</h2>
</a>
</div>
</div>
<div class="card-block">
<ul class="list-unstyled text-emphasis text-md m-b-0">
<li><a href="/embedded-software/products/" class="link-hover-cta"><span>Software Products</span></a></li>
<li><a href="/embedded-software/iot/" class="link-hover-cta"><span>IoT Solutions</span></a></li>
<li><a href="/embedded-software/services/" class="link-hover-cta"><span>Services</span></a></li>
<li><a href="/embedded-software/industries/" class="link-hover-cta"><span>Industries</span></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>
</div>
<header id="header-secondary" class="bg-secondary-darker p-y" role="banner">
<div class="container">
<div class="row row-flex middle-lg">
<div class="col-md-9 col-xs-12">
</div>
<div class="col-md-3 col-xs-12">
<div id="search">
<form id="searchbox" action="/search/">
<div class="search-box input-group">
<input type="text" id="searchTerm" class="form-control bg-transparent text-inverse p-x-0" autocomplete="off" name="query" data-dauuid="" placeholder="Search Mentor.com" value="" rel="Search Mentor.com" />
<span class="input-group-btn">
<button type="submit" class="searchbutton btn bg-transparent text-inverse p-x-0"><svg class="icon icon-search2" aria-hidden="true"><use xlink:href="#icon-search2"></use></svg></button>
</span>
</div>
</form>
</div>
</div>
</div>
</div>
</header>
<div id="content" class="flex-content">
<div class="bg-white border-bottom text-webfont-one m-b-md">
<div class="container position-relative">
<nav id="bread" class="breadcrumb item-flex-main">
<ol>
<!-- BREADCRUMB -->
<li class="breadcrumb1 first">
<a href="/">
<svg class="icon icon-home2" aria-hidden="true"><use xlink:href="#icon-home2"></use></svg>
<span class="sr-only">
<span>Home</span>
</span>
</a>
</li>
<li class="breadcrumb2 first">
<a href="/terms_conditions/">
<span>Terms & Conditions</span>
</a>
</li>
<li class="breadcrumb3 last current">
<span>
<span>Mentor Graphics Privacy Policy</span>
</span>
</li>
<!-- /BREADCRUMB -->
</ol>
</nav>
</div>
</div>
<div id="copy" class="container">
<div class="row row-flex content-container">
<div id="sidebar" class="col-md-2 col-xs-12 last-xs first-md content-sidebar">
<div>
<ul class="list-group">
<li class="list-group-item list-group-item-nav p-y-0"><a href="/terms_conditions">Terms of Use</a>
</li>
<li class="list-group-item list-group-item-nav p-y-0 active"><a href="/terms_conditions/privacy">Privacy Policy</a>
</li>
<li class="list-group-item list-group-item-nav p-y-0"><a href="/terms_conditions/swquality">SW Quality Policy</a>
</li>
<li class="list-group-item list-group-item-nav p-y-0"><a href="/terms_conditions/enduser">End-User License</a>
</li>
<li class="list-group-item list-group-item-nav p-y-0"><a href="/terms_conditions/trademarks">Trademarks</a>
</li>
</ul>
</div>
</div>
<div class="
col-md-10
col-xs-12 first-xs last-md content-main">
<div class="card">
<div class="card-header card-header-secondary text-inverse card-page-title">
<h1><span>Mentor Graphics Privacy Policy</span></h1>
</div>
<div class="card-block">
<div class="row-container  "
>
<div class="row row-fluid ">
<div class="column count1 col-md-12 col-xs-12 number-columns-1">
<p>Mentor Graphics Corporation, a Siemens business (Mentor or Mentor Graphics) takes your concerns about privacy very seriously.&nbsp;&nbsp; The purpose of this privacy notice is to describe Mentor Graphics (Mentor) policy regarding the collection and usage of personal data from our web site <a href="http://www.mentor.com"><strong>www.mentor.com</strong></a> and from any customer satisfaction or product improvement program in which our customers choose to participate.</p>
</div>
</div>
</div>
<div class="row-container flag-left "
>
<h2 class="modularlayout-title flag-left header-flag"><span>Policy</span></h2>
<div class="row row-fluid ">
<div class="column count1 col-md-12 col-xs-12 number-columns-1">
<div class="row-container  "
>
<div class="row row-fluid ">
<div class="column count1 col-md-6 col-xs-12 number-columns-2">
<h3>How we treat your personal information</h3>
<p>Mentor considers all information collected to be our information. It will be collected from you via the forms in various locations on Mentor Graphics websites and from your voluntary participation in product improvement programs, but will not be shared with, bartered, or sold to any third party without your consent.</p>
<p>We collect information from you for the purposes of fulfilling your requests, improving your experience on the Mentor Graphics websites, verify your identity, improving our ability to create individually relevant communications to you about our products and services and improving our products and features to help better solve design problems. You may choose to not supply any personal information to us; however, doing so will limit your access to certain content on our site, such as product demonstrations, white papers, and the like.</p>
<p>At times, we disclose relevant information about our customers to our contractors in order for them to provide specific services for Mentor, or to distributors to fulfill a customer order. Our contractors and distributors are bound by strict contractual requirements to keep all information they receive confidential and to use such information solely on behalf of Mentor Graphics. We take reasonable measures to protect the information you share with us from unauthorized access or disclosure.</p>
<h3>How we treat email lists</h3>
<p>Mentor Graphics maintains several e-mail lists to keep interested persons informed about events and other items of interest. Individuals can sign up to join our mailing lists in several ways, for example; in response to an email message or by using a form on our website. Other than disclosing to contractors as required for them to perform specific services for Mentor Graphics, we do not sell, trade, lease, rent, loan or otherwise disclose the addresses on these lists to any third party without your consent.</p>
<h3>Information change requests</h3>
<p>We realize that your personal information, including name, company, and email address, is subject to change over time. if you need to update your information in our records, please send email to&nbsp;<a href="mailto:privacy@mentor.com"><strong>Privacy@mentor.com</strong></a>.&nbsp;</p>
<p>If you are registered with Mentor Graphics&rsquo; profile center, you can update your data <a href="https://signin.mentor.com/register?next=https%3A%2F%2Fwww%2Ementor%2Ecom%2Fterms_conditions%2Fprivacy" target="_blank"><strong>online</strong></a>.</p>
<h3>How we treat your browser information</h3>
<p>Mentor Graphics analyzes the log files of our website to better understand the volume of traffic to particular areas of our site. This information helps us to serve your information needs. Individual users remain anonymous in our website log files unless they have logged into one of our password-protected sites.</p>
<p>For example, when accessing SupportNet, our technical support web site, we may correlate individual registration and browser information to your personal and company information as stored in our business databases. These databases contain support contract expiration dates and other information relevant to delivering and improving support.</p>
<h3>Our use of cookies</h3>
<p>A &quot;cookie&quot; is a marker or an identifier that is entered into the memory of a visitor&#39;s browser and is saved to a file for future reference. Mentor Graphics uses cookies on selected areas of our website as a tool to &quot;remember&quot; information about our visitors so that they do not have to repeatedly enter their personal information onto numerous forms on the site.</p>
<p>In the future, cookies will help us tailor our website to the visitor&#39;s individual interests. You can set your browser to notify you when you are sent a cookie so that you can decide whether or not to accept it. Even without a cookie, you can access most information on our web site.</p>
</div>
<div class="column count2 col-md-6 col-xs-12 number-columns-2">
<h3>How we treat information collected from a product improvement program</h3>
<p>Technological features in our products and on our website may provide data to Mentor Graphics or its contractors and are used for the purpose of making improvements to products and enhancing features used most by our customers. Typically, the limited data collected includes information on product performance, settings, options, output log files, and similar product functionality.</p>
<p>Mentor Graphics analyzes this information along with data from other program participants to help improve user experience. Mentor Graphics will not collect any personally identifiable data in this process and will not disclose your data collected to any third party without your prior written consent, except to Mentor Graphics&rsquo; outside attorneys or as may be required by a court of competent jurisdiction.</p>
<h3>Transfer of Personal Information:</h3>
<p>Mentor Graphics has its headquarters in the United States, so the information we collect from you maybe be processed in the United States.&nbsp; Mentor Graphics takes suitable safeguards for the protection of your personal data.&nbsp; Mentor Graphics participates in, complies with, and has certified its adherence to the EU-U.S. Privacy Shield Framework as set forth by the U.S. Department of Commerce regarding the collection, use, and retention of personal information transferred from the European Union and the United Kingdom to the United States. Mentor Graphics is committed to subjecting personal data received from European Union (EU) member countries and the United Kingdom in reliance on the Privacy Shield Framework and to the Framework&rsquo;s applicable principles. If there is any conflict between the terms in this privacy policy and the Privacy Shield Principles, the Privacy Shield Principles shall govern. To learn more about the Privacy Shield Frameworks, and to view our certification, visit the U.S. Department of Commerce&rsquo;s <a href="http://www.privacyshield.gov/welcome" target="_blank"><strong>Privacy Shield List</strong></a>.</p>
<p>Sometimes the recipients to whom Mentor Graphics transfers your personal data are located in countries in which applicable laws do not offer the same level of data protection as the laws of your home country. In those cases, we will only transfer personal data to external recipients who have (1) entered into EU standard contractual clauses with Mentor Graphics, (2) implemented Binding Corporate Rules in its Organization or (3) U.S. companies that entered into the Privacy Shield self certification program.&nbsp;</p>
<p><strong>Data Storage and Retention:</strong></p>
<p>We retain your personal data for the duration of the business relationship with Mentor Graphics.&nbsp; We only store it for as long as the purposes for which the information was collected or otherwise processed, or to comply with legal obligations (such as Retention obligations under tax or Commercial laws).</p>
<p><strong>California Privacy Rights</strong></p>
<p>California&rsquo;s &ldquo;Shine The Light&rdquo; law permits those of our customers who are California residents to annually request a list of their personal data (if any) we disclosed to third parties for direct marketing purposes in the preceding calendar year, and the names and addresses of those third parties. At this time, we currently do not share any personal data with third parties for their direct marketing purposes.</p>
<p>The European Union&rsquo;s General Data Protection Regulation provides certain rights to data subjects living in the EU.&nbsp; Subject to the statutory requirements, EU citizens are entitled to:</p>
<ul>
<li>Obtain confirmation as to whether or not personal data concerning you are being processed, and where that is the case, access to the personal data;</li>
<li>Obtain the rectification of inaccurate personal data concerning you;</li>
<li>Obtain the erasure of your personal data;</li>
<li>Obtain restriction of processing regarding your personal data;</li>
<li>Data portability concerning personal data, which you actively provided; and</li>
<li>Object, on grounds relating to your particular situation, to processing of personal data concerning you.</li>
</ul>
<p>&nbsp;</p>
</div>
</div>
</div>
<h3>For more information</h3>
<p>Mentor Graphics data privacy organization provides support with any data privacy related questions, comments, concerns or complaints or in case you wish to exercise any of your data privacy related rights. The Data Privacy Organization may be reached by emailed mail as follows:</p>
<p>Mentor Graphics Corporation<br />
Legal Department &ndash; Data Privacy Office<br />
8005 Sw Boeckman Road<br />
Wilsonville, Oregon 97070<br />
USA<br />
<a href="mailto:privacy@mentor.com?subject=Privacy%20Policy">privacy@mentor.com</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<footer class="footer-section flex-none text-webfont-one">
<div class="container">
</div>
</footer>
<!--pageElement:FOOTER-->
<footer class="footer-site flex-none">
<div class="container">
<div class="row-flex middle-md">
<div class="col-md-8 col-xs-12 text-center text-lg-left">
<p class="text-xs">
<a href="/sitemap">Site Map</a> <span class="text-pipe">|</span>
<a href="/company/news/">News & Press</a> <span class="text-pipe">|</span>
<a href="/company/careers/">Careers</a> <span class="text-pipe">|</span>
<a href="/consulting/">Consulting</a> <span class="text-pipe">|</span>
<a href="/company/partner_programs/">Partners/Foundry Support</a> <span class="text-pipe">|</span>
<a href="/company/international">International Websites</a>
</p>
<p class="text-xs">
<a class="text-muted" href="/terms_conditions">Terms</a>  <span class="text-pipe">|</span>
<a class="text-muted" href="/terms_conditions/privacy">Privacy Policy</a>
</p>
<p class="text-xs m-b-md m-lg-b-0">&copy; Mentor, a Siemens Business, All rights reserved</p>
</div>
<div class="col-md-4 col-xs-12 text-center text-lg-right text-xs">
<div class="m-b" id="footer-site-social-links">
<h3 class="text-uc m-b-sm">Follow Mentor</h3>
<a href="http://www.linkedin.com/company/mentor_graphics" class="circle circle-md bg-opaque-light m-x-xs"><svg class="icon icon-linkedin2"><use xlink:href="#icon-linkedin2"></use></svg></a>
<a href="https://twitter.com/mentor_graphics" class="circle circle-md bg-opaque-light m-x-xs"><svg class="icon icon-twitter"><use xlink:href="#icon-twitter"></use></svg></a>
<a href="https://www.facebook.com/pages/Mentor-Graphics/362609027104610" class="circle circle-md bg-opaque-light m-x-xs"><svg class="icon icon-facebook"><use xlink:href="#icon-facebook"></use></svg></a>
<a href="http://www.youtube.com/channel/UC6glMEaanKWD86NEjwbtgfg" class="circle circle-md bg-opaque-light m-x-xs"><svg class="icon icon-youtube"><use xlink:href="#icon-youtube"></use></svg></a>
<a href="https://plus.google.com/b/102197424811444669688/102197424811444669688/posts" class="circle circle-md bg-opaque-light m-x-xs"><svg class="icon icon-google-plus"><use xlink:href="#icon-google-plus"></use></svg></a>
</div>
<p class="m-b-0"><a class="text-white p-r-sm text-emphasis text-no-underline" href="tel:+18005473000">1-800-547-3000</a> <a href="/company/contact_us" class="btn btn-sm btn-info">Contact Mentor</a></p>
</div>
</div>
</div>
</footer>
<!--/pageElement:FOOTER-->
<div class="bg-info text-inverse p-a raised-lg" style="display:none;position:fixed;bottom:0;left:0;right:0;z-index: 2000" id="cookieBanner">
<div class="container-lg item-flex item-flex-responsive">
<div class="item-flex-main p-r-md">This site uses cookies to improve your user experience and to provide you with content we believe will be of interest to you. Detailed information on the use of cookies on this website is provided in our <a href="/terms_conditions/privacy" class=“text-underline”>Privacy Policy</a>. By using this website, you consent to the use of our cookies.</div>
<button id="cookieConsent" class="btn btn-inverse-outline btn-sm btn-pill item-flex"><svg class="icon icon-cross2 text-lg m-r-xs"><use xlink:href="#icon-cross2"></use></svg> <span class="text-muted">Ok, don't show me this again</span></button>
</div>
</div>
<!--pageElement:PAGETAGGING-->
<script language="JavaScript">
var s_account="mentorcom2";
var s;
</script>
<script type="text/javascript" src="https://static.mentor-cdn.com/mgc/s-code/generated/prod/v17/script.min.js"></script>

<script language="JavaScript">
//s.loadModule("Media");
<!-- Page-based Tagging -->
    if (s && typeof s.account !== "undefined") {
        
        s.channel = 'company';
        s.eVar19 = '';
        s.eVar38 = 'D=c38';
        s.eVar41 = 'D=c41';
        s.eVar5 = 'D=c5';
        s.eVar8 = '';
        s.eVar9 = 'D=c9';
        s.hier1 = 'Terms&Conditions,Mentor Graphics Privacy Policy,Mentor Graphics Privacy Policy';
        s.pageName = 'Terms&Conditions:Mentor Graphics Privacy Policy';
        s.prop19 = '';
        s.prop38 = 'mentor';
        s.prop41 = 'en';
        s.prop5 = 'Account';
        s.prop6 = '';
        s.prop8 = '';
        s.prop9 = '38f6986c-14cd-82ea-09ac-933d4abebea6';
        s.eVar7 = s.Util.getQueryParam('pid');
        s.prop4 = s.Util.getQueryParam('sfm');
        s.eVar4 = s.Util.getQueryParam('sfm');
        s.prop36 = s.getPreviousValue(s.pageName, 'gpv_pn');
        s.prop37 = s.getPercentPageViewed();
    }
</script>
<script language="JavaScript">
var s_code;
if (s && typeof s.account !== "undefined") {
	<!-- Session-based Tagging -->
	s.eVar12 = '';
	s.eVar13 = 'D=c13';
	s.eVar27 = '';
	s.eVar34 = 'D=v0';
	s.prop12 = '';
	s.prop13 = 'Anonymous';
	s.prop27 = '';
	s.prop34 = 'D=v0';
	s.visitorID = '1553165935147027E6373E44743130433C9D111DFE609499D39B77CB43471CEBAF4FBE6BF669D';
	<!-- Request-based Tagging -->


		s.eVar24 = 'D=c24';
		s.eVar25 = 'D=c25';
		s.prop24 = '0-50 ms';
		s.prop25 = 'true';
	s_code = s.t();if (s_code)document.write(s_code); //send
}
</script>

<noscript>
<img src="https://mentorcom2.122.2o7.net/b/ss/mentorcom2/1/H.26.1--NS/0" height="1" width="1" border="0" alt="" />
</noscript>
<script>
if(navigator.appVersion.indexOf('MSIE')>=0) document.write(unescape('%3C')+'\!-'+'-');
</script>
<!-- Level 2 -->
<!--/pageElement:PAGETAGGING-->
<!--pageElement:Google Analytics-->
<!-- Google Analytics -->
<script>
var google_mgc_vars=false;
var google_training_vars=false;
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-7441435-1', 'auto', {
'cookieDomain': 'mentor.com'
});
ga('set', 'dimension2', 'Anonymous');
ga('set', 'dimension1', 'Company');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
<!--/pageElement:Google Analytics-->
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"Zo/El1agWBr1N8", domain:"mentor.com",dynamic: true};
(function() { var as = document.createElement("script"); as.type = "text/javascript"; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName("script")[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Zo/El1agWBr1N8" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->
<!-- LOADBALANCE-OOB-PAGE-SUCCESS -->
<script src="https://static.mentor-cdn.com/common/js/svg.js"></script>
<!-- Scroll To Top -->
<a href="#top" id="scrollToTop" class="scroll-top" title="Scroll to Top"><svg class="icon icon-arrow-up3" aria-hidden="true"><use xlink:href="#icon-arrow-up3"></use></svg></a>
<div id="mgc-player-wrapper">
<div id="mgc-player" class="mgc-player"></div>
</div>
<!-- //// MGC PAGE END //// -->

</body>
</html>



    

