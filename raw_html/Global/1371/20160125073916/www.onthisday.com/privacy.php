<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Privacy Statement | OnThisDay.com</title>
<meta name="description" content="This privacy statement has been created to demonstrate our firm commitment to privacy and to discloses our information gathering and dissemination practices."/>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
<meta http-equiv="Content-Language" content="en"/>
<link rel="stylesheet" href="/inc/style.css"/>
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/flick/jquery-ui.min.css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
<script src="/inc/jquery-ui.min4.js" type="text/javascript"></script>
<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script><script type='text/javascript'>
googletag.cmd.push(function() {
googletag.defineSlot('/1006136/HistoryOrb_ATF_300x250', [300, 250], 'div-gpt-ad-1426299362135-0').addService(googletag.pubads());
googletag.defineSlot('/1006136/HistoryOrb_ATF_728x90', [728, 90], 'div-gpt-ad-1426299362135-2').addService(googletag.pubads());
googletag.defineSlot('/1006136/HistoryOrb_BTF_300x250', [300, 250], 'div-gpt-ad-1426299362135-3').addService(googletag.pubads());
googletag.pubads().enableSingleRequest();
googletag.enableServices();
});
</script></head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-956772-9', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');

</script><div id="wrapper"><div id="page"><div id="header"><a id="header_link" href="/">OnThisDay.com</a>
<div id="header_cse"><form method='post' action='/site-search.php' class="form-wrapper cf">
<input id="search" type="text" name="site-search" placeholder=' Person, Event, Place, Year ...' required />
<button type="submit">Search</button></form></div></div><div id="menubox"><div id="menu"><ul>
<li><a href="/today/events.php">Historical Events</a></li>
<li><a href="/today/birthdays.php">Famous Birthdays</a></li>
<li><a href="/today/weddings-divorces.php">Weddings</a></li>
<li><a href="/today/deaths.php">Famous Deaths</a></li>
<li><a href="/famous-people.php">Famous People</a></li>
<li><a href="/dates-by-year.php">By Date</a></li>
<li><a href="/calendar.php">By Day</a></li>
<li><a href="/countries.php">By Country</a></li>
</ul></div></div><div id="ad_top"> 
<div id='div-gpt-ad-1426299362135-2' style='width:728px; height:90px; margin-left:auto; margin-right:auto;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1426299362135-2'); });
</script></div></div><div id="breadcrumbs"><a href="/">Home</a> &raquo; Privacy Statement</div>
<div id="content">
<h1>OnThisDay.com's Privacy Statement</h1>
<div id="main_text"><p><b>OnThisDay.com</b> has created this privacy statement in order to demonstrate
our firm commitment to privacy. The following discloses the information gathering
and dissemination practices of: <a href="http://www.onthisday.com"><i>OnThisDay.com</i></a></p>
<h3>Information Automatically Logged</h3>
<div id="ad_square"> 
<div id='div-gpt-ad-1426299362135-0' style='width:300px; height:250px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1426299362135-0'); });
</script></div></div><p>We use your IP address to help diagnose problems with our server and to administer our web site.</p>
<h3>Cookies</h3>
<p>Our site uses cookies to make sure you don't see the same ad repeatedly and also for statistical analysis.</p>
<h3>Advertisements</h3>
<p>We use third-party advertising companies to serve ads when you visit our web site. These companies may use aggregated information
(not including your name, address, email address or telephone number) about your visits to this and other web sites in order to provide
advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices
about not having this information used by these companies, click <a href="http://www.networkadvertising.org/managing/opt_out.asp">here</a>.</p>
<h3>Surveys</h3>
<p>Our online surveys ask visitors for demographic information (like age, or income level).</p>
<p>Demographic and profile data is also collected at our site.
This information is shared with advertisers on an aggregate basis. We use this data to tailor our visitor's experience at our site showing them content that we think they might be interested in, and displaying the content according to their preferences.</p><h3>External Links</h3>
<p>This site contains links to other sites. <b>OnThisDay.com </b> is not responsible
for the privacy practices or the content of such Web sites.</p>
<h3>Data Quality/Access</h3>
<p>This site gives users the following options for changing and modifying information previously provided:
<ol><li><p>You can apply to have the entry corrected on our <a href="corrections.php">corrections page</a>.</p></li>
</ol><h3>Contacting the Web Site</h3>
<p>If you have any questions about this privacy statement, the practices of this
site, or your dealings with this Web site, please <a href="contact.php">contact us</a>.</p></div>
<div id="ad_banner"> 
<div id='div-gpt-ad-1426299362135-3' style='width:300px; height:250px; margin-left:auto; margin-right:auto;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1426299362135-3'); });
</script></div></div><br/></div>
<div id="sidebar"><br/>
<h3 class='subtitle2'>Date Search</h3>
<div class='sidebar_textbox'><form name='searchVertical' method='post' action='/today/search.php'><div class='date-wrapper'>
<label><select name='day2'><option value='all' selected='selected'>All Days</option>
<option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option>
<option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option>
<option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option>
<option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option>
<option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option>
<option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option>
<option value='31'>31</option></select></label>
<label><select name='month2'><option value='all' selected='selected'>All Months</option>
<option value='1'>January</option><option value='2'>February</option><option value='3'>March</option><option value='4'>April</option><option value='5'>May</option>
<option value='6'>June</option><option value='7'>July</option><option value='8'>August</option><option value='9'>September</option><option value='10'>October</option>
<option value='11'>November</option><option value='12'>December</option></select></label>
<label><input name='year2' type='number' placeholder=' All Years' min='1' max='2016'/></label>
<label><select name='type2'><option value='all' selected='selected'>All Types</option>
<option value='e'>Events</option><option value='b'>Birthdays</option><option value='w'>Weddings</option><option value='v'>Divorces</option><option value='d'>Deaths</option></select></label>
<input type='hidden' name='searchBox' value='V'/>
<button type="submit" class="sc-btn sc--flat sc--red center_btn">Search by Date</button></div></form></div><br/><br/></div><div id="footer"><div class="footermenu"><div class="footer_line"></div>
<h4><a href="/">Today in History</a></h4><ul>
<li>&raquo; <a href="/today/events.php">Historical Events</a></li>
<li>&raquo; <a href="/today/birthdays.php">Famous Birthdays</a></li>
<li>&raquo; <a href="/today/weddings-divorces.php">Weddings &amp; Divorces</a></li>
<li>&raquo; <a href="/today/deaths.php">Famous Deaths</a></li></ul>
<p align="center"><a href="/events/date/2015" class="sc-btn sc--flat sc--facebook"><span class="sc-text">2015 Events</span></a></p>
<p align="center"><a href="/deaths/date/2015" class="sc-btn sc--flat sc--red"><span class="sc-text">2015 Deaths</span></a></p></div>
<div class="footermenu2"><div class="footer_line"></div><h4><a href="/calendar.php">By Day</a></h4><ul>
<li>&raquo; <a href="/events-calendar.php">Events</a></li>
<li>&raquo; <a href="/birthdays-calendar.php">Birthdays</a></li>
<li>&raquo; <a href="/weddings-divorces-calendar.php">Weddings &amp; Divorces</a></li>
<li>&raquo; <a href="/deaths-calendar.php">Deaths</a></li></ul><br/>
<h4><a href="/dates-by-year.php">By Year</a></h4><ul>
<li>&raquo; <a href="/events-by-year.php">Events</a></li>
<li>&raquo; <a href="/birthdays-by-year.php">Birthdays</a></li>
<li>&raquo; <a href="/weddings-divorces-by-year.php">Weddings &amp; Divorces</a></li>
<li>&raquo; <a href="/deaths-by-year.php">Deaths</a></li></ul></div>
<div class="footermenu2"><div class="footer_line"></div><h4><a href="/famous-people.php">Famous People</a></h4><ul><li>&raquo; <a href="/people/professions.php">By Profession</a></li>
<li>&raquo; <a href="/people/age-groups.php">By Age Group</a></li>
<li>&raquo; <a href="/people/cause-of-death.php">By Cause of Death</a></li>
<li>&raquo; <a href="/people/star-signs.php">By Star Sign</a></li></ul><br/>
<h4><a href="/countries.php">By Country</a></h4><ul>
<li><a href='/countries/australia'><img src="http://r.onthisday.com/au.gif" alt="Australia" width="18" height="12" border="0"/> Australia</a></li>
<li><a href='/countries/canada'><img src="http://r.onthisday.com/ca.gif" alt="Canada" width="18" height="12" border="0"/> Canada</a></li>
<li><a href='/countries/india'><img src="http://r.onthisday.com/in.gif" alt="India" width="18" height="12" border="0"/> India</a></li>
<li><a href='/countries/united-kingdom'><img src="http://r.onthisday.com/uk.gif" alt="UK" width="18" height="12" border="0"/> United Kingdom</a></li>
<li><a href='/countries/usa'><img src="http://r.onthisday.com/us.gif" alt="USA" width="18" height="12" border="0"/> U.S.A</a></li></ul></div>
<div class="footermenu3"><div class="footer_line"></div>
<h4><a href="/topics.php">Topics in History</a></h4><ul>
<li>&raquo; <a href="/art-history">Art History</a></li>
<li>&raquo; <a href="/business">Business</a></li>
<li>&raquo; <a href="/entertainment/movie">Film &amp; TV</a></li>
<li>&raquo; <a href="/literature">Literature</a></li>
<li>&raquo; <a href="/religion">Religion</a></li>
<li>&raquo; <a href="/science">Science</a></li>
<li>&raquo; <a href="/war-history">War</a></li></ul></div>
<div class="footermenu2"><div class="footer_line"></div><h4>Follow Us</h4><ul>
<li><a href="https://www.facebook.com/OnThisDaycom"><svg viewBox="0 0 33 33" width="25" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M 17.996,32L 12,32 L 12,16 l-4,0 l0-5.514 l 4-0.002l-0.006-3.248C 11.993,2.737, 13.213,0, 18.512,0l 4.412,0 l0,5.515 l-2.757,0 c-2.063,0-2.163,0.77-2.163,2.209l-0.008,2.76l 4.959,0 l-0.585,5.514L 18,16L 17.996,32z"></path></g></svg>
Facebook</a></li>
<li><a href="https://twitter.com/OnThisDaycom"><svg viewBox="0 0 33 33" width="16" height="16" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M 32,6.076c-1.177,0.522-2.443,0.875-3.771,1.034c 1.355-0.813, 2.396-2.099, 2.887-3.632 c-1.269,0.752-2.674,1.299-4.169,1.593c-1.198-1.276-2.904-2.073-4.792-2.073c-3.626,0-6.565,2.939-6.565,6.565 c0,0.515, 0.058,1.016, 0.17,1.496c-5.456-0.274-10.294-2.888-13.532-6.86c-0.565,0.97-0.889,2.097-0.889,3.301 c0,2.278, 1.159,4.287, 2.921,5.465c-1.076-0.034-2.088-0.329-2.974-0.821c-0.001,0.027-0.001,0.055-0.001,0.083 c0,3.181, 2.263,5.834, 5.266,6.438c-0.551,0.15-1.131,0.23-1.73,0.23c-0.423,0-0.834-0.041-1.235-0.118 c 0.836,2.608, 3.26,4.506, 6.133,4.559c-2.247,1.761-5.078,2.81-8.154,2.81c-0.53,0-1.052-0.031-1.566-0.092 c 2.905,1.863, 6.356,2.95, 10.064,2.95c 12.076,0, 18.679-10.004, 18.679-18.68c0-0.285-0.006-0.568-0.019-0.849 C 30.007,8.548, 31.12,7.392, 32,6.076z"></path></g></svg>
Twitter</a></li>
<li><a href="/today/rss-feeds.php"><svg viewBox="0 0 33 33" width="25" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><path d="M 32,20 L 32,32 L 0,32 L 0,20 L 4,20 L 4,28 L 28,28 L 28,20 ZM 6,22L 26,22L 26,26L 6,26zM 6.473,17.671L 7.339,13.766L 26.865,18.094L 26,21.999zM 8.739,9.642L 10.43,6.017L 28.556,14.469L 26.865,18.094zM 30.991,11.296 L 28.556,14.47 L 12.689,2.294 L 14.45,0 L 16.27,0 Z"></path></g></svg>
RSS</a></li></ul><br/>
<h4>Channels</h4><ul>
<li><a href="/music/"><img src="http://r.onthisday.com/uk.gif" alt="On This Day in Music" width="18" height="12" border="0"/> Today in Music</a></li>
<li><a href="/sport/"><img src="http://r.onthisday.com/uk.gif" alt="On This Day in Sport" width="18" height="12" border="0"/> Today in Sport</a></li>
<li><a href="http://www.hoyenlahistoria.com"><img src="http://r.onthisday.com/es.gif" alt="Today in History in Spanish" width="18" height="12" border="0"/> Hoy en la Historia</a></li></ul></div>
<div class="footermenu3"><h4>Site Info</h4><ul>
<li>&raquo; <a href="/about.php">About Us</a></li>
<li>&raquo; <a href="/contact.php">Contact Info</a></li>
<li>&raquo; <a href="/corrections.php">Corrections</a></li>
<li>&raquo; <a href="/privacy.php">Privacy</a></li>
<li>&raquo; <a href="/search-help.php">Search Help</a></li>
<li>&raquo; <a href="/today/webmasters.php">Webmasters</a></li></ul></div>
<div class="footer_copy"><p>&copy; 2000-2016 <a href="/">OnThisDay.com</a></p></div></div></div></div></body>
</html>