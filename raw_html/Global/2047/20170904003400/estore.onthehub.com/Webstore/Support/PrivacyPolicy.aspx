
<!DOCTYPE html>
<html id="ctl00_htmlElement" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" lang="en">
	<head id="ctl00_Head1"><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta id="ctl00_ogSiteName" property="og:site_name" content="OnTheHub.com" /><meta id="ctl00_ogTitle" property="og:title" content="OnTheHub" /><meta id="ctl00_ogType" property="og:type" content="website" /><meta id="ctl00_ogUrl" property="og:url" content="http://e5.onthehub.com/index.html" /><meta id="ctl00_ogImage" property="og:image" content="http://e5.OnTheHub.com/Static/Images/OTH_network_96x55px_roundedcorners_whitebackground.png" /><meta id="ctl00_ogDescription" property="og:description" content="Student Discount Software | Educational Software Discounts | OnTheHub links students, faculty and staff to exclusive academic discount software." /><meta id="ctl00_fbAppId" property="fb:app_id" content="134512176588098" /><title>
	Privacy Policy | OnTheHub | Save on Minitab, SPSS, EndNote, Parallels and More
</title><script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script><script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script><script>this.jQuery || document.write('<script src="//static.onthehub.com/production/js/jquery/jquery-1.9.1.min.js">\x3C/script>')</script><script>$.fn.alert != undefined || document.write('<script src="//static.onthehub.com/production/js/bootstrap/bootstrap.min.js">\x3C/script>')</script><link rel="stylesheet" href="//static.onthehub.com/production/Themes/Default/Default.css?20170822134806" type="text/css" /><script type="text/javascript" src="//static.onthehub.com/production/js/webStore/SocialNetworkControls.js?4.0.6396.19"></script><script>var kvLocalStore = {"AccessGuarantee":{"SdmBuyNow":"Click to purchase selected add-on offerings"},"Common":{"Close":"Close","Error":"Error","Loading":"Loading...","PleaseWait":"Please Wait","Processing":"Processing...","Remove":"Remove"},"OfferingDetails_aspx":{"BackupMediaAvailable":"Backup media available in most countries","BackupMediaAvailableHoverMessage":"Backup media for this product may be available from your shopping cart. Availability depends on the country from which you are ordering."},"PageContext":{"Interstitial":false},"ShoppingCart_aspx":{"ViewFullCart":"View Full Cart"},"WebStore":{"Add":"Add","AddedToYourShoppingCart":"Added to your shopping cart","AddToCart":"Add to Cart","Brand":"onthehub e-store","Checkout":"Check Out","ContinueShopping":"Back to Shopping","CookieBanner":"<span>By using this site you agree to the use of cookies for analytics, personalized content and ads.</span><a href=\"https://privacy.microsoft.com/en-us/privacystatement\" target=\"_blank\" class=\"learn-link\">Learn more</a>","DemoQuestionsContact":"If you have questions, please <a href=\"/WebStore/Support/ContactUs.aspx\">contact</a> your administrator.","ExpressCheckout":"Express Checkout","ExpressCheckoutMessage":"By placing this order, you agree to all terms and conditions associated with its items.","ExternalUrl":"http://onthehub.com/estore","Id":"49c547ba-f56d-dd11-bb6c-0030485a6b08","InCart":"In Cart","InterstitialUrl":"/WebStore/AddToCartInterstitial.aspx","NoRecommendationsSelected":"No recommended items are selected.","Ordered":"Ordered","Organization":"OnTheHub","PlatformConflictPrompt":"The platform of the product you have chosen appears to be different than the computer you are presently using. Are you sure you would like to add this product to your cart?","ShoppingCartUrl":"/WebStore/ShoppingCart.aspx","ViewAllTermsAndConditions":"View all terms and conditions."}};kvLocalStore.StatusMessages = [];if (!kvLocalStore.hasOwnProperty('ValidationSummary')) kvLocalStore.ValidationSummary = [];</script><script src="/bundles/webstore?v=phZdTiAEWvXHWW0djcAQMkhC-mLSQUF8Fx1eHMz57pY1"></script>
</head>
	<body>
		<noscript>
			<div>
				<p>
					This website requires javascript to be enabled. It appears that your browser is configured to disable javascript.<br />
<br />
For instructions on enabling javascript in your browser, see <a href='https://www.google.com/adsense/support/bin/answer.py?hl=en&answer=12654'>https://www.google.com/adsense/support/bin/answer.py?hl=en&answer=12654</a>.<br />
<br />
If javascript is enabled in your browser and you are still getting this message, please contact your network administrator.
				</p>
			</div>
		</noscript>
		<form name="aspnetForm" method="post" action="./PrivacyPolicy.aspx" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJNDczMzI3NTY3D2QWAmYPZBYCAgEPFgIeBGxhbmcFAmVuFgICBQ9kFgYCAw8PFgIeB1Zpc2libGVoZBYCAgUPDxYCHgRUZXh0BWhJZiB5b3UgaGF2ZSBxdWVzdGlvbnMsIHBsZWFzZSA8YSBocmVmPSIvV2ViU3RvcmUvU3VwcG9ydC9Db250YWN0VXMuYXNweCI+Y29udGFjdDwvYT4geW91ciBhZG1pbmlzdHJhdG9yLmRkAgUPZBYQAgcPZBYEAgUPZBYEZg9kFgJmDw8WAh4LTmF2aWdhdGVVcmwFX34vV2ViU3RvcmUvQ29tbW9uL1NlbGVjdEN1cnJlbmN5QW5kQ3VsdHVyZS5hc3B4P3J1cmw9JTJmV2Vic3RvcmUlMmZTdXBwb3J0JTJmUHJpdmFjeVBvbGljeS5hc3B4FgIeCmFyaWEtbGFiZWwFJ1dlYnN0b3JlIGN1bHR1cmUgYW5kIGN1cnJlbmN5IHNlbGVjdGlvbhYGZg8WAh4JaW5uZXJodG1sBQpVUyBEb2xsYXJzZAIBDxYCHgVjbGFzcwUWZmxhZy1pY29uIGZsYWctaWNvbi11c2QCAg8PFgIfAgUHRW5nbGlzaGRkAgIPFgIfAWgWBgIBDxYCHwUFClVTIERvbGxhcnNkAgIPFgIfBgUWZmxhZy1pY29uIGZsYWctaWNvbi11c2QCAw8PFgIfAgUHRW5nbGlzaGRkAgcPZBYGAgEPFgIfAWgWBgIFDxYCHwFoZAIHDw8WAh8DBSN+L1dlYlN0b3JlL0FjY291bnQvWW91ckFjY291bnQuYXNweGRkAgsPDxYCHwMFHH4vV2ViU3RvcmUvU2hvcHBpbmdDYXJ0LmFzcHhkZAIDDxYCHwFnFgICAQ8PFgIfAwVRL1dlYlN0b3JlL1NlY3VyaXR5L1NpZ25Jbi5hc3B4P3J1cmw9JTJmV2Vic3RvcmUlMmZTdXBwb3J0JTJmUHJpdmFjeVBvbGljeS5hc3B4JTNmZGQCBQ8WAh8BZ2QCCQ8PFgIfAwVkfi9XZWJTdG9yZS9Qcm9kdWN0c0J5TWFqb3JWZXJzaW9uTGlzdC5hc3B4P2NtaV9jcz0xJmNtaV9tbnVNYWluPWVkNmFkNzNjLTdiYzctZTAxMS1hZTE0LWYwNGRhMjNlNjdmNmRkAgsPDxYCHwFoZGQCDQ9kFgQCAQ9kFgQCAw8PZBYEHgV0aXRsZQUOUHJvZHVjdCBTZWFyY2geC3BsYWNlaG9sZGVyBQ5Qcm9kdWN0IFNlYXJjaGQCBQ8PFgIeDU9uQ2xpZW50Q2xpY2sFf3Nob3dEaWFsb2coeyd0aXRsZSc6a3ZMb2NhbFN0b3JlLkNvbW1vbi5QbGVhc2VXYWl0LCdjb250ZW50JzprdkxvY2FsU3RvcmUuQ29tbW9uLlByb2Nlc3NpbmcsJ2FsbG93Q2xvc2luZyc6ZmFsc2V9KTtyZXR1cm4gdHJ1ZTsWAh8EBQZTZWFyY2hkAgMPDxYGHgxDdXN0b21NZW51SUQoKVhTeXN0ZW0uR3VpZCwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5JGZjZTBmZWI5LTdlYzctZTAxMS1hZTE0LWYwNGRhMjNlNjdmNh4XQ3VycmVudEN1c3RvbU1lbnVJdGVtSUQoKwQkMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDAtMDAwMDAwMDAwMDAwHwFnZGQCDw8PFgIfAWhkZAIXDw8WBh4JRm9yZUNvbG9yDB4IQ3NzQ2xhc3MFEmFsZXJ0IGFsZXJ0LWRhbmdlch4EXyFTQgIGZGQCHw9kFgQCAQ8PFgIfAgUfVGVybXMgb2YgVXNlIGFuZCBQcml2YWN5IFBvbGljeWRkAgMPDxYCHwIF2W08cD4NCglCeSBicm93c2luZyB0aGlzIFNpdGUsIHlvdSBhZ3JlZSB0aGF0IHlvdSBoYXZlIHJlYWQgYW5kIHVuZGVyc3RhbmQgdGhlIGZvbGxvd2luZyBUZXJtcyBvZiBVc2UgYW5kIFByaXZhY3kgUG9saWN5IGluIGl0cyBlbnRpcmV0eSwgaW5jbHVkaW5nIGFueSB1cGRhdGVzIHRoYXQgbWF5IGJlIHBvc3RlZCBvbiB0aGUgU2l0ZSBmcm9tIHRpbWUgdG8gdGltZS4gS2l2dXRvIFNvbHV0aW9ucyBJbmMuIHJlc2VydmVzIHRoZSByaWdodCB0byByZXZpc2UgdGhlIFRlcm1zIG9mIFVzZSBhbmQgUHJpdmFjeSBQb2xpY3ksIGF0IGl0cyBzb2xlIGRpc2NyZXRpb24sIGJ5IHVwZGF0aW5nIHRoaXMgcG9zdGluZy48L3A+DQo8cD4NCglSZWZlcmVuY2VzIHRvIEtpdnV0byBTb2x1dGlvbnMgSW5jLiAob3IgS2l2dXRvKSBpbiB0aGlzIGRvY3VtZW50IGFsc28gYXBwbHkgdG8gS2l2dXRvJnJzcXVvO3Mgd2hvbGx5IG93bmVkIGJyYW5kcywgVGV4aWRpdW0gYW5kIE9uVGhlSHViLjwvcD4NCjxvbD4NCgk8bGk+DQoJCTxhIGhyZWY9IiMxIj5XaGF0IGluZm9ybWF0aW9uIGRvIHlvdSBnYXRoZXIgYWJvdXQgbWU/PC9hPjwvbGk+DQoJPGxpPg0KCQk8YSBocmVmPSIjMiI+SG93IGRvIHlvdSBjb2xsZWN0IGFuZCB1c2UgcGVyc29uYWxseSBpZGVudGlmaWFibGUgaW5mb3JtYXRpb24/PC9hPjwvbGk+DQoJPGxpPg0KCQk8YSBocmVmPSIjMyI+SG93IGNhbiBJIGFjY2VzcywgdXBkYXRlIG9yIGRlbGV0ZSBteSBwZXJzb25hbCBpbmZvcm1hdGlvbj88L2E+PC9saT4NCgk8bGk+DQoJCTxhIGhyZWY9IiM0Ij5Ib3cgZG8geW91IGNvbGxlY3QgYW5kIHVzZSBub24tcGVyc29uYWxseSBpZGVudGlmaWFibGUgaW5mb3JtYXRpb24/PC9hPjwvbGk+DQoJPGxpPg0KCQk8YSBocmVmPSIjNSI+V2hhdCBzZWN1cml0eSBpcyBpbiBwbGFjZSB0byBwcm90ZWN0IG15IGluZm9ybWF0aW9uPzwvYT48L2xpPg0KCTxsaT4NCgkJPGEgaHJlZj0iIzYiPldoZXJlIHdpbGwgbXkgaW5mb3JtYXRpb24gYmUgcHJvY2Vzc2VkPzwvYT48L2xpPg0KCTxsaT4NCgkJPGEgaHJlZj0iIzciPldoYXQgaXMgeW91ciBlbWFpbCBwb2xpY3k/PC9hPjwvbGk+DQoJPGxpPg0KCQk8YSBocmVmPSIjOCI+V2hhdCBhYm91dCBjaGFuZ2VzIHRvIHRoZSBQcml2YWN5IFBvbGljeT88L2E+PC9saT4NCjwvb2w+DQo8b2w+DQoJPGxpPg0KCQk8aDM+DQoJCQk8YSBuYW1lPSIxIj5XaGF0IGluZm9ybWF0aW9uIGRvIHlvdSBnYXRoZXIgYWJvdXQgbWU/PC9hPjwvaDM+DQoJCTxwPg0KCQkJQm90aCBwZXJzb25hbGx5IGFuZCBub24tcGVyc29uYWxseSBpZGVudGlmaWFibGUgaW5mb3JtYXRpb24gYWJvdXQgZWFjaCB2aXNpdG9yIG1heSBiZSBnYXRoZXJlZCBmcm9tIHRoaXMgc2l0ZS4gVGhlIHR5cGUgb2YgaW5mb3JtYXRpb24gZ2F0aGVyZWQgZGVwZW5kcyBvbiB5b3VyIGFjdGlvbnMgYW5kIGludGVyYWN0aW9uIHdpdGggdGhlIHNpdGUuPC9wPg0KCQk8cD4NCgkJCTxiPlBlcnNvbmFsbHkgSWRlbnRpZmlhYmxlIEluZm9ybWF0aW9uPC9iPjxiciAvPg0KCQkJUGVyc29uYWxseSBpZGVudGlmaWFibGUgaW5mb3JtYXRpb24gaXMgb25seSBjb2xsZWN0ZWQgd2hlbiB5b3Ugdm9sdW50YXJpbHkgc3VibWl0IHRoaXMgaW5mb3JtYXRpb24gb24gYW4gYXMtbmVlZGVkIGJhc2lzIHN1Y2ggYXMgZHVyaW5nIHJlZ2lzdHJhdGlvbiBhbmQgb3JkZXJpbmcuIEFueSBwZXJzb25hbCBpbmZvcm1hdGlvbiB0aGF0IHlvdSBtYXkgc2hhcmUgd2l0aCB1cyBpcyBrZXB0IHByaXZhdGUuIE5laXRoZXIgeW91ciBuYW1lIG5vciBhbnkgb3RoZXIgaWRlbnRpZnlpbmcgZGF0YSBhYm91dCB5b3UgaXMgc29sZCBvciBzaGFyZWQgd2l0aCBhbnkgb3RoZXIgdGhpcmQgcGFydHkgdW5sZXNzIHJlcXVpcmVkIGZvciB0aGUgZnVsZmlsbG1lbnQgb3IgcmVmdW5kIG9mIHlvdXIgb3JkZXIgb3Igc2VydmljZS4gTW9yZSBpbmZvcm1hdGlvbiBvbiB0aGlzIHVzZSBpcyBwcm92aWRlZCBsYXRlciBpbiB0aGlzIHByaXZhY3kgcG9saWN5LjwvcD4NCgkJPHA+DQoJCQk8Yj5Ob24tUGVyc29uYWxseSBJZGVudGlmaWFibGUgSW5mb3JtYXRpb248L2I+PGJyIC8+DQoJCQlOb24tcGVyc29uYWxseSBpZGVudGlmeWluZyBpbmZvcm1hdGlvbiBpcyBnYXRoZXJlZCB0aHJvdWdoIG91ciBzaXRlIHRlY2hub2xvZ3kgdG8gY3JlYXRlIGEgbW9yZSBwZXJzb25hbGl6ZWQgYW5kIHJlbGV2YW50IGV4cGVyaWVuY2UgZHVyaW5nIHlvdXIgdmlzaXQuIFRoZSBpbmZvcm1hdGlvbiBhbHNvIGFsbG93cyB1cyB0byBpbXByb3ZlIHRoZSBmdW5jdGlvbmFsaXR5IG9mIHRoZSB3ZWJzaXRlIHRvIGJldHRlciBzZXJ2ZSB5b3UuPC9wPg0KCTwvbGk+DQoJPGxpPg0KCQk8aDM+DQoJCQk8YSBuYW1lPSIyIj5Ib3cgZG8geW91IGNvbGxlY3QgYW5kIHVzZSBwZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbj88L2E+PC9oMz4NCgkJPHA+DQoJCQk8Yj5Db2xsZWN0aW5nIFBlcnNvbmFsbHkgSWRlbnRpZmlhYmxlIEluZm9ybWF0aW9uPC9iPjxiciAvPg0KCQkJUGVyc29uYWxseSBpZGVudGlmaWFibGUgaW5mb3JtYXRpb24gbWF5IGJlIGNvbGxlY3RlZCBkdXJpbmcgcmVnaXN0cmF0aW9uLCBvcmRlcmluZyBhbmQgYmlsbGluZywgZHVyaW5nIGN1c3RvbWVyIHNlcnZpY2UgaW50ZXJhY3Rpb25zIG9yIHdoZW4geW91IGVudGVyIGEgY29udGVzdC4gUGVyc29uYWxseSBpZGVudGlmaWFibGUgaW5mb3JtYXRpb24gaXMgbmV2ZXIgc2hhcmVkIGluIHdheXMgdW5yZWxhdGVkIHRvIHRoZSBtZXRob2RzIGRlc2NyaWJlZCBiZWxvdyB3aXRob3V0IG5vdGlmeWluZyB5b3Ugb2YgdGhlIGludGVudCBiZWZvcmVoYW5kLCBhbmQgYWxzbyBwcm92aWRpbmcgeW91IHdpdGggYW4gb3Bwb3J0dW5pdHkgdG8gb3B0LW91dCBvciBvdGhlcndpc2UgcHJvaGliaXQgc3VjaCB1bnJlbGF0ZWQgdXNlcy48L3A+DQoJCTxwPg0KCQkJPGI+UmVnaXN0cmF0aW9uPC9iPjxiciAvPg0KCQkJRHVyaW5nIHJlZ2lzdHJhdGlvbiwgeW91IG1heSBiZSBwcm9tcHRlZCBmb3IgeW91ciBuYW1lIGFuZCBlbWFpbCBhZGRyZXNzIGZvciBhdXRoZW50aWNhdGlvbiBhbmQgdmVyaWZpY2F0aW9uIHB1cnBvc2VzLiBUaGVzZSBzdGVwcyBtYXkgYmUgbmVjZXNzYXJ5IHRvIGVuc3VyZSB1c2VycyBhcmUgcHJvcGVybHkgaWRlbnRpZmllZCBhbmQgYXV0aGVudGljYXRlZCBhcyBhIHF1YWxpZmllZCB1c2VyLiBTb21lIG9mZmVyaW5ncyBhbmQgcHJvbW90aW9ucyBvbiB0aGlzIHNpdGUgbWF5IGJlIHJlc3RyaWN0ZWQgdG8gbWVtYmVycyBvZiBjZXJ0YWluIG9yZ2FuaXphdGlvbnMgYW5kLCBieSBwcm92aWRpbmcgeW91ciBlbWFpbCBhZGRyZXNzLCBhbmQgc3Vic2VxdWVudGx5IGhhdmluZyB5b3VyIGFjY291bnQgdmVyaWZpZWQsIHlvdSBtYXkgYmVuZWZpdCBmcm9tIHRoZXNlIG9mZmVyaW5ncyBhbmQgcHJvbW90aW9ucy48L3A+DQoJCTxwPg0KCQkJPGI+T3JkZXJpbmcgYW5kIEJpbGxpbmc8L2I+PGJyIC8+DQoJCQlXaGVuIHlvdSBwbGFjZSBhbiBvcmRlciwgeW91IG1heSBuZWVkIHRvIHN1cHBseSB5b3VyIGNyZWRpdCBjYXJkIGluZm9ybWF0aW9uIGFsb25nIHdpdGggdGhlIGNvcnJlc3BvbmRpbmcgbWFpbGluZyBpbmZvcm1hdGlvbi4gVGhpcyBpbmZvcm1hdGlvbiBtYXkgYmUgcmVxdWlyZWQgdG8gZnVsZmlsbCB5b3VyIG9yZGVyLiBZb3VyIGNyZWRpdCBjYXJkIGluZm9ybWF0aW9uIGlzIG5vdCBzdG9yZWQgb24gb3VyIHNlcnZlcjsgaXQgaXMgc2ltcGx5IHBhc3NlZCBvbiB0byB0aGUgY3JlZGl0IGNhcmQgcHJvY2Vzc2luZyBzeXN0ZW0gZm9yIHZhbGlkYXRpb24uIFlvdSBtYXkgYWxzbyBiZSBwcm9tcHRlZCBmb3IgeW91ciBuYW1lIGFuZCBlbWFpbCBhZGRyZXNzIHRvIGNyZWF0ZSB5b3VyIG9ubGluZSByZWNlaXB0IGFuZCBmb3IgdXMgdG8gc2VuZCB5b3Ugb3JkZXIgcmVsYXRlZCBlbWFpbHMuPC9wPg0KCQk8cD4NCgkJCTxiPkN1c3RvbWVyIFNlcnZpY2U8L2I+PGJyIC8+DQoJCQlQZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbiBtYXkgYmUgY29sbGVjdGVkIGlmIHlvdSByZXBvcnQgYSBwcm9ibGVtIG9yIHN1Ym1pdCBhbiBpbnF1aXJ5IHRvIG91ciBjdXN0b21lciBzZXJ2aWNlIHRlYW0ocykuIEEgcmVjb3JkIG9mIHRoYXQgY29ycmVzcG9uZGVuY2UgbWF5IGJlIG1haW50YWluZWQgdG8gcHJvdmlkZSB5b3Ugd2l0aCBhbiBvcHRpbWFsIGN1c3RvbWVyIHNlcnZpY2UgZXhwZXJpZW5jZS48L3A+DQoJCTxwPg0KCQkJPGI+RW50ZXJpbmcgYSBDb250ZXN0IG9yIFByb21vdGlvbjwvYj48YnIgLz4NCgkJCUZ1cnRoZXJtb3JlLCBpbmZvcm1hdGlvbiBmcm9tIHlvdSBtYXkgYmUgY29sbGVjdGVkIGZvciBlbnRyeSBpbnRvIGNvbnRlc3RzIGFuZCBzaW1pbGFyIHByb21vdGlvbnMgd2hlbiB5b3Ugdm9sdW50YXJpbHkgY2hvb3NlIHRvIHBhcnRpY2lwYXRlLiBBbGwgY29udGVzdHMgaGF2ZSB0aGVpciBvd24gdGVybXMgYW5kIGNvbmRpdGlvbnMgd2hpY2ggY2FuIGJlIHZpZXdlZCBvbiB0aGUgY29udGVzdCZyc3F1bztzIHBhZ2UuPC9wPg0KCQk8cD4NCgkJCTxiPlVzaW5nIFBlcnNvbmFsbHkgSWRlbnRpZmlhYmxlIEluZm9ybWF0aW9uPC9iPjxiciAvPg0KCQkJWW91ciBwZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbiBpcyBub3Qgc29sZCwgdHJhZGVkIG9yIHJlbnRlZCB0byBvdGhlcnMuPC9wPg0KCQk8cD4NCgkJCVdlIGRvIGVtcGxveSBvdGhlciB0cnVzdGVkIGNvbXBhbmllcyB0byBwZXJmb3JtIGNlcnRhaW4gZnVuY3Rpb25zIG9uIG91ciBiZWhhbGYsIGluY2x1ZGluZyBvcmRlciBmdWxmaWxsbWVudCBhbmQgZGVsaXZlcnksIGFzIHdlbGwgYXMgY3JlZGl0IGNhcmQgcHJvY2Vzc2luZy4gVGhlc2UgYnVzaW5lc3MgYWZmaWxpYXRlcyBoYXZlIGFjY2VzcyB0byBwZXJzb25hbCBpbmZvcm1hdGlvbiBuZWVkZWQgdG8gcGVyZm9ybSB0aGVpciBmdW5jdGlvbnMsIGJ1dCBhcmUgbm90IHBlcm1pdHRlZCB0byB1c2UgeW91ciBwZXJzb25hbCBpbmZvcm1hdGlvbiBmb3IgYW55IG90aGVyIHB1cnBvc2UuPC9wPg0KCQk8cD4NCgkJCUFkZGl0aW9uYWxseSwgaW5mb3JtYXRpb24gcmVsYXRlZCB0byB0aGUgb3JkZXIgb2YgYSBwcm9kdWN0IG1heSBiZSBzaGFyZWQgd2l0aCB0aGUgc3VwcGxpZXIgb2YgdGhhdCBwYXJ0aWN1bGFyIHByb2R1Y3QsIGZvciB3YXJyYW50eSBhbmQgc3VwcG9ydCBwdXJwb3Nlcy4gVGhpcyBtYXkgaW5jbHVkZSB5b3VyIG5hbWUsIGRhdGUgb2YgeW91ciBvcmRlciBhbmQgcHJvZHVjdCBwdXJjaGFzZWQuPC9wPg0KCQk8cD4NCgkJCUlmIHlvdSBoYXZlIHF1ZXN0aW9ucyBhYm91dCBob3cgb25lIG9mIG91ciBidXNpbmVzcyBwYXJ0bmVycyBvciBzdXBwbGllcnMgdXNlIGluZm9ybWF0aW9uIGFib3V0IHlvdSwgY29udGFjdCB0aGVtIGRpcmVjdGx5LjwvcD4NCgk8L2xpPg0KCTxsaT4NCgkJPGgzPg0KCQkJPGEgbmFtZT0iMyI+SG93IGNhbiBJIGFjY2VzcywgdXBkYXRlIG9yIGRlbGV0ZSBteSBwZXJzb25hbCBpbmZvcm1hdGlvbj88L2E+PC9oMz4NCgkJPHA+DQoJCQlVc2VycyBoYXZlIHRoZSBhYmlsaXR5IHRvIGNvcnJlY3Qgb3IgY2hhbmdlIGFueSBpbmZvcm1hdGlvbiBvciBwcmVmZXJlbmNlcyBpbiB0aGVpciBhY2NvdW50IHByb2ZpbGUuIFVzZXJzIG1heSBjaGFuZ2UgdGhpcyBpbmZvcm1hdGlvbiBhdCBhbnkgdGltZSwgcHJvdmlkZWQgdGhleSBhcmUgc2lnbmVkIGluLjwvcD4NCgkJPHA+DQoJCQlZb3UgY2FuIGhhdmUgeW91ciBhY2NvdW50IGRlYWN0aXZhdGVkIG9yIGRlbGV0ZWQgYnkgY29udGFjdGluZyBTdXBwb3J0LiBIb3dldmVyLCBkb2luZyBzbyB3aWxsIGxpbWl0IHlvdXIgYWNjZXNzIHRvIG1lbWJlcnMtb25seSBhcmVhcyBvZiB0aGUgc2l0ZS4gQW55IHB1cmNoYXNlIGhpc3RvcnkgaW5mb3JtYXRpb24sIGhvd2V2ZXIsIHdpbGwgcmVtYWluIGluIHByb3RlY3RlZCBwZXJtYW5lbnQgcmVjb3Jkcy48L3A+DQoJPC9saT4NCgk8bGk+DQoJCTxoMz4NCgkJCTxhIG5hbWU9IjQiPkhvdyBkbyB5b3UgY29sbGVjdCBhbmQgdXNlIG5vbi1wZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbj88L2E+PC9oMz4NCgkJPHA+DQoJCQlXZSB1c2UgdGVjaG5vbG9naWVzIGxpa2UgY29va2llcywgcGl4ZWwgdGFncyAoJnF1b3Q7cGl4ZWxzJnF1b3Q7KSwgYW5kIGxvY2FsIHN0b3JhZ2UgdG8gZGVsaXZlciwgc2VjdXJlLCBhbmQgdW5kZXJzdGFuZCBwcm9kdWN0cywgc2VydmljZXMsIGFuZCBhZHMsIG9uIGFuZCBvZmYgb3VyIHNpdGVzLjwvcD4NCgkJPHA+DQoJCQk8Yj5Db2xsZWN0aW5nIG5vbi1wZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbjwvYj48YnIgLz4NCgkJCVlvdXIgdmVyaWZpY2F0aW9uIHN0YXR1cyBpcyBjb2xsZWN0ZWQgb25jZSB5b3Ugc3VjY2Vzc2Z1bGx5IHJlZ2lzdGVyIHRocm91Z2ggYW55IG9mIG91ciB2ZXJpZmljYXRpb24gc2VydmljZXMuIFRoZSByZXN1bHRpbmcgc3RhdHVzIGlzIG5vbi1wZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbiBhbmQgaXMgc3RvcmVkIGluIGNhc2UgeW91IG5lZWQgdG8gdmVyaWZ5IHlvdXIgc3RhdHVzIGFnYWluIGluIHRoZSBmdXR1cmUuPC9wPg0KCQk8cD4NCgkJCU5vbi1wZXJzb25hbGx5IGlkZW50aWZpYWJsZSBpbmZvcm1hdGlvbiBpcyBjb2xsZWN0ZWQgdXNpbmcgdGVjaG5vbG9neSBzdWNoIGFzIGNvb2tpZXMuICZxdW90O0Nvb2tpZXMmcXVvdDsgYXJlIHNtYWxsIHBpZWNlcyBvZiBpbmZvcm1hdGlvbiB0aGF0IGFyZSBzdG9yZWQgYnkgeW91ciBicm93c2VyIG9uIHlvdXIgbG9jYWwgc3RvcmFnZS4gVGhlIGluZm9ybWF0aW9uIGNvbGxlY3RlZCBieSB0aGlzIHRlY2hub2xvZ3kgaXMgbm9uLXBlcnNvbmFsbHkgaWRlbnRpZmlhYmxlIG9ubHkgYW5kIGFsbG93cyB1cyB0byBvcHRpbWl6ZSB5b3VyIGludGVyYWN0aW9ucyB3aXRoIHRoZSBzaXRlLjwvcD4NCgkJPHA+DQoJCQk8Yj5Vc2luZyBub24tcGVyc29uYWxseSBpZGVudGlmaWFibGUgaW5mb3JtYXRpb248L2I+PGJyIC8+DQoJCQlDb29raWVzIGFsbG93IGZvciBjdXN0b21lciB0cmFmZmljIHBhdHRlcm5zIGFuZCBzaXRlIHVzYWdlIHRvIGJlIG1vbml0b3JlZCBvbiBhIGdlbmVyYWwgYmFzaXMgdG8gaGVscCBkZXZlbG9wIGFuZCBpbXByb3ZlIHRoZSBkZXNpZ24sIGxheW91dCBhbmQgZWZmaWNpZW5jeSBvZiB0aGUgd2Vic2l0ZS4gVW5kZXJzdGFuZGluZyB0aGVzZSBwYXR0ZXJucyBhbGxvd3MgdXMgdG8gcHJvdmlkZSBtb3JlIHJlbGV2YW50IGNvbnRlbnQgdG8geW91IGFuZCBpbXByb3ZlIGN1c3RvbWVyIHJlbGF0aW9uc2hpcHMuIEZ1cnRoZXJtb3JlLCB0aGUgaW5mb3JtYXRpb24gY29sbGVjdGVkIGlzIHVzZWQgdG8gY3JlYXRlIHRhcmdldGVkIGNvbW11bmljYXRpb25zIHRvIG9mZmVyIHlvdSBhIG1vcmUgcGVyc29uYWxpemVkIGFuZCBjdXN0b21pemFibGUgZXhwZXJpZW5jZS4gQ29va2llcyBhbHNvIHNhdmUgeW91IHRpbWUgYnkgcmVtZW1iZXJpbmcgeW91ciBwZXJzb25hbGl6ZWQgc2V0dGluZ3MuPC9wPg0KCQk8cD4NCgkJCU5vdGUgdGhhdCBhbGwgaW5mb3JtYXRpb24gaXMgY29sbGVjdGVkIGFuZCBhbmFseXplZCBpbiBhZ2dyZWdhdGVkIGZvcm0gb25seS4gQ2hlY2sgeW91ciBicm93c2VyIHRvIGVuc3VyZSBjb29raWVzIGFyZSBlbmFibGVkIGZvciB0aGlzIHNpdGUgdG8gd29yayBwcm9wZXJseS48L3A+DQoJCTxwPg0KCQkJS2l2dXRvIFNvbHV0aW9ucyBJbmMuIG1heSB1c2UgdGhpcmQtcGFydHkgYWR2ZXJ0aXNpbmcgdGVjaG5vbG9neSBwYXJ0bmVycyB0byBzZXJ2ZSB5b3UgcmVsZXZhbnQgYWR2ZXJ0aXNlbWVudHMgb24gd2Vic2l0ZXMgb3V0c2lkZSBvZiBvdXIgbmV0d29yay4gVGhlIGFkdmVydGlzaW5nIHRlY2hub2xvZ3kgcGFydG5lciBwcm92aWRlcyB1cyB3aXRoIHRyYWNraW5nIHRlY2hub2xvZ3ksIHN1Y2ggYXMgYSBjb29raWUsIHdoaWNoIGlzIGFkZGVkIHRvIHlvdXIgYnJvd3NlciBieSB1cy4gVGhpcyBhbGxvd3MgdXMgdG8gc2VydmUgeW91IHZlcnkgcmVsZXZhbnQgYWRzIG9uIHRoZWlyIHBhcnRuZXIgd2Vic2l0ZXMgYWNjb3JkaW5nIHRvIHlvdXIgZW5nYWdlbWVudHMgd2l0aCB0aGlzIHNpdGUuIElmIHlvdSB3b3VsZCBsaWtlIHRvIGxlYXJuIG1vcmUgYWJvdXQgdGhpcyBwcm9jZXNzIG9yIGV2ZW4gb3B0LW91dCwgcGxlYXNlIGNvbnRhY3QgdXMuPC9wPg0KCQk8cD4NCgkJCTxiPkhvdyB0byBtYW5hZ2UgY29va2llczwvYj48YnIgLz4NCgkJCUJyb3dzZXIgY29udHJvbHMgYXJlIGF2YWlsYWJsZSB0byBhbGxvdywgYmxvY2sgb3IgZGVsZXRlIGNvb2tpZXMuIFRvIG1hbmFnZSB0aGUgY29va2llcyB0aGlzIHNpdGUgYW5kIG90aGVycyB1c2UsIHZpc2l0IHRoZSBmb2xsb3dpbmcgc2l0ZSBmb3IgaW5zdHJ1Y3Rpb25zOiA8YSBocmVmPSJodHRwOi8vd2luZG93cy5taWNyb3NvZnQuY29tL2VuLVVTL3dpbmRvd3MtdmlzdGEvQmxvY2stb3ItYWxsb3ctY29va2llcyIgdGFyZ2V0PSJfYmxhbmsiPmh0dHA6Ly93aW5kb3dzLm1pY3Jvc29mdC5jb20vZW4tVVMvd2luZG93cy12aXN0YS9CbG9jay1vci1hbGxvdy1jb29raWVzLjwvYT48L3A+DQoJCTxwPg0KCQkJUGxlYXNlIG5vdGUgdGhhdCBieSBhZGp1c3RpbmcgeW91ciBjb29raWUgc2V0dGluZ3MsIGFueSBzZXR0aW5ncyBvciBwcmVmZXJlbmNlcyB3aWxsIG5vIGxvbmdlciBiZSBpbiBlZmZlY3QuIFRoZSB1c2FnZSBhbmQgZnVuY3Rpb25hbGl0eSBvZiB0aGUgc2l0ZSB3aWxsIGJlIHN1YmplY3QgdG8gY2hhbmdlIGluY2x1ZGluZyB0aGUgYWJpbGl0eSB0byBwdXJjaGFzZSBjZXJ0YWluIHByb2R1Y3RzIGFuZCB2aWV3IGNvbnRlbnQgb24gdGhlIHNpdGUuPC9wPg0KCTwvbGk+DQoJPGxpPg0KCQk8aDM+DQoJCQk8YSBuYW1lPSI1Ij5XaGF0IHNlY3VyaXR5IGlzIGluIHBsYWNlIHRvIHByb3RlY3QgbXkgaW5mb3JtYXRpb24/PC9hPjwvaDM+DQoJCTxwPg0KCQkJWW91ciBpbmZvcm1hdGlvbiBpcyBwcm90ZWN0ZWQgYm90aCBkdXJpbmcgdHJhbnNtaXNzaW9uIGFuZCBpbiBzdG9yYWdlLiBZb3VyIGFjY291bnQgYW5kIHByb2ZpbGUgaW5mb3JtYXRpb24gYXJlIHBhc3N3b3JkLXByb3RlY3RlZCBzbyB0aGF0IHlvdSBhbmQgb25seSB5b3UgaGF2ZSBhY2Nlc3MgdG8gdGhpcyBwZXJzb25hbCBpbmZvcm1hdGlvbi4gRHVyaW5nIHB1cmNoYXNlLCB0aGUgc2VjdXJlIHNlcnZlciBzb2Z0d2FyZSAoU1NMKSBlbmNyeXB0cyBhbGwgaW5mb3JtYXRpb24geW91IGlucHV0IGJlZm9yZSBpdCBpcyB0cmFuc21pdHRlZC4gQXMgc3VjaCwgYWxsIGN1c3RvbWVyIGRhdGEgaXMgcHJvdGVjdGVkIGFnYWluc3QgdW5hdXRob3JpemVkIGFjY2VzcyBvbiB0aGUgc3lzdGVtJiMzOTtzIHNlY3VyZSBzZXJ2ZXJzLjwvcD4NCgk8L2xpPg0KCTxsaT4NCgkJPGgzPg0KCQkJPGEgbmFtZT0iNiI+V2hlcmUgd2lsbCBteSBpbmZvcm1hdGlvbiBiZSBwcm9jZXNzZWQ/PC9hPjwvaDM+DQoJCTxwPg0KCQkJS2l2dXRvIFNvbHV0aW9ucywgSW5jLiBpcyBhIENhbmFkaWFuIGNvcnBvcmF0aW9uIHdpdGggb2ZmaWNlcyBhdCAxMjYgWW9yayBTdHJlZXQsIFN1aXRlIDIwMCwgT3R0YXdhLCBPbnRhcmlvLCBDYW5hZGEuIFBlcnNvbmFsIGluZm9ybWF0aW9uIGdhdGhlcmVkIGJ5IEtpdnV0byBpcyBzdG9yZWQgb24gc2VydmVycyBsb2NhdGVkIGluIENhbmFkYSBhbmQgd2lsbCBub3QgYmUgdHJhbnNmZXJyZWQgdG8gYW55IG90aGVyIGNvdW50cnkgdW5sZXNzIGl0IGlzIHJlcXVpcmVkIGFzIHBhcnQgb2YgdGhlIGZ1bGZpbGxtZW50IG9mIHNlcnZpY2UuIFNlZSA8c3Ryb25nPlNlY3Rpb24gMjwvc3Ryb25nPiBmb3IgbW9yZSBkZXRhaWxzIGFib3V0IEtpdnV0byZyc3F1bztzIHRyZWF0bWVudCBvZiBwZXJzb25hbCBpbmZvcm1hdGlvbi48L3A+DQoJCTxwPg0KCQkJRHVlIHRvIGFncmVlbWVudHMgd2l0aCBvdXIgc29mdHdhcmUgcHVibGlzaGVyIHBhcnRuZXJzLCB5b3VyIGRhdGEgd2lsbCBiZSBzdG9yZWQgb24gb3VyIHNlcnZlcnMgZm9yIHNldmVuIHllYXJzLjwvcD4NCgkJPHA+DQoJCQlQcml2YWN5IGxhd3MgYW5kIHJlcXVpcmVtZW50cyBkaWZmZXIgYWNjb3JkaW5nIHRvIGVhY2ggY291bnRyeS4gUGxlYXNlIGtub3csIGhvd2V2ZXIsIHRoYXQgYWxsIGluZm9ybWF0aW9uIGNvbGxlY3RlZCB0aHJvdWdoIEtpdnV0byZyc3F1bztzIFdlYnNpdGVzIHdpbGwgYmUgcHJvdGVjdGVkIGJ5IHRoZSB0ZXJtcyBvZiB0aGlzIFByaXZhY3kgUG9saWN5IGFuZCBhbnkgUHJpdmFjeSBOb3RpY2VzIHRoYXQgYXBwbHkgdG8geW91LCByZWdhcmRsZXNzIG9mIHdoYXQgY291bnRyeSB0aGUgaW5mb3JtYXRpb24gb3JpZ2luYXRlcyBmcm9tLjwvcD4NCgk8L2xpPg0KCTxsaT4NCgkJPGgzPg0KCQkJPGEgbmFtZT0iNyI+V2hhdCBpcyB5b3VyIGVtYWlsIHBvbGljeT88L2E+PC9oMz4NCgkJPHA+DQoJCQlLaXZ1dG8gU29sdXRpb25zIEluYy4gY29tcGxpZXMgZnVsbHkgd2l0aCB0aGUgZmVkZXJhbCBDQU4tU1BBTSBBY3Qgb2YgMjAwMyBhbmQgQ2FuYWRhJiMzOTtzIEFudGktU3BhbSBMZWdpc2xhdGlvbiAoQ0FTTCkuIFdlIHdpbGwgbm90IHNoYXJlLCBzZWxsLCByZW50LCBzd2FwIG9yIGF1dGhvcml6ZSBhbnkgdGhpcmQgcGFydHkgdG8gdXNlIHlvdXIgZW1haWwgYWRkcmVzcyBmb3IgY29tbWVyY2lhbCBwdXJwb3NlcyB3aXRob3V0IHlvdXIgcGVybWlzc2lvbi4gSWYgeW91IGZlZWwgeW91IGhhdmUgcmVjZWl2ZWQgYW4gZW1haWwgZnJvbSB1cyBpbiBlcnJvciwgY29udGFjdCA8YSBocmVmPSJtYWlsdG86ZmVlZGJhY2tAS2l2dXRvLmNvbSI+ZmVlZGJhY2tAa2l2dXRvLmNvbTwvYT4uPC9wPg0KCQk8cD4NCgkJCVlvdSBtYXkgcmVjZWl2ZSBvbmUgb3IgbW9yZSBvZiB0aGUgZm9sbG93aW5nIGVtYWlscyBmcm9tIEtpdnV0byBTb2x1dGlvbnMuPC9wPg0KCQk8cD4NCgkJCTxiPkFjY291bnQgYW5kIFNlcnZpY2UtUmVsYXRlZCBFbWFpbDwvYj48YnIgLz4NCgkJCUtpdnV0byBTb2x1dGlvbnMgSW5jLiByZXNlcnZlcyB0aGUgcmlnaHQgdG8gc2VuZCB5b3UgZW1haWxzIHJlbGF0aW5nIHRvIHlvdXIgYWNjb3VudCBzdGF0dXMuIFRoaXMgaW5jbHVkZXMgb3JkZXIgY29uZmlybWF0aW9ucywgcmVuZXdhbC9leHBpcmF0aW9uIG5vdGljZXMsIG5vdGljZXMgb2YgY3JlZGl0LWNhcmQgcHJvYmxlbXMsIG90aGVyIHRyYW5zYWN0aW9uYWwgZW1haWxzIGFuZCBub3RpZmljYXRpb25zIGFib3V0IG1ham9yIGNoYW5nZXMgdG8gdGhlIHNpdGUgc2VydmljZXMgYW5kL29yIHRvIG91ciBQcml2YWN5IFBvbGljeS4gSWYgeW91IGhhdmUgcmVnaXN0ZXJlZCBmb3Igb25saW5lIGRpc2N1c3Npb25zIG9yIG90aGVyIHNlcnZpY2VzLCB5b3UgbWF5IHJlY2VpdmUgYW4gZW1haWwgc3BlY2lmaWMgdG8geW91ciBwYXJ0aWNpcGF0aW9uIGluIHRob3NlIGFjdGl2aXRpZXMuPC9wPg0KCQk8cD4NCgkJCTxiPkVtYWlsIE5ld3NsZXR0ZXJzPC9iPjxiciAvPg0KCQkJS2l2dXRvIFNvbHV0aW9ucyBJbmMuIG9mZmVyIHNldmVyYWwgZW1haWwgbmV3c2xldHRlcnMuIFlvdSB3aWxsIG9ubHkgcmVjZWl2ZSB0aGVzZSBpZiB5b3UgcHJldmlvdXNseSBjaG9zZSB0byBvcHQtaW4gdG8gdGhlc2UgdHlwZXMgb2YgY29tbXVuaWNhdGlvbnMuIElmIHlvdSBubyBsb25nZXIgd2lzaCB0byByZWNlaXZlIGEgc3BlY2lmaWMgbmV3c2xldHRlciwgZm9sbG93IHRoZSAmcXVvdDt1bnN1YnNjcmliZSZxdW90OyBpbnN0cnVjdGlvbnMgbG9jYXRlZCBuZWFyIHRoZSBib3R0b20gb2YgZWFjaCBuZXdzbGV0dGVyLjwvcD4NCgkJPHA+DQoJCQk8Yj5Qcm9tb3Rpb25hbCBFbWFpbDwvYj48YnIgLz4NCgkJCUtpdnV0byBTb2x1dGlvbnMgSW5jLiBtYXkgcGVyaW9kaWNhbGx5IGVtYWlsIHlvdSBtZXNzYWdlcyBhYm91dCBwcm9kdWN0cyBhbmQgc2VydmljZXMgdGhhdCB3ZSB0aGluayBtYXkgYmUgb2YgaW50ZXJlc3QgdG8geW91IChpZiB5b3UgaGF2ZSBwcmV2aW91c2x5IG9wdGVkIGluIHRvIHJlY2VpdmUgdGhlc2UgdHlwZXMgb2YgY29tbXVuaWNhdGlvbnMpLiBZb3UgY2FuIGNob29zZSBub3QgdG8gcmVjZWl2ZSBtZXNzYWdlcyBpbiB0aGUgZnV0dXJlIGJ5IGZvbGxvd2luZyB0aGUgJnF1b3Q7dW5zdWJzY3JpYmUmcXVvdDsgaW5zdHJ1Y3Rpb25zIGxvY2F0ZWQgbmVhciB0aGUgYm90dG9tIG9mIGVhY2ggZW1haWwuPC9wPg0KCQk8cD4NCgkJCTxiPlN1cnZleSBFbWFpbDwvYj48YnIgLz4NCgkJCVdlIG1heSBzZW5kIHlvdSBlbWFpbHMgaW52aXRpbmcgeW91IHRvIHBhcnRpY2lwYXRlIGluIHVzZXIgc3VydmV5cywgYXNraW5nIGZvciBmZWVkYmFjayBvbiBvdXIgc2VydmljZXMgYW5kIGV4aXN0aW5nIG9yIHByb3NwZWN0aXZlIHByb2R1Y3RzIGFuZCBzZXJ2aWNlcywgYXMgd2VsbCBhcyBpbmZvcm1hdGlvbiB0byBiZXR0ZXIgdW5kZXJzdGFuZCBvdXIgdXNlcnMuIFVzZXIgc3VydmV5cyBncmVhdGx5IGhlbHAgdXMgdG8gaW1wcm92ZSBvdXIgc2VydmljZXMsIGFuZCBhbnkgaW5mb3JtYXRpb24gd2Ugb2J0YWluIGluIHN1Y2ggc3VydmV5cyB3aWxsIGJlIHVzZWQgaW50ZXJuYWxseS4gVGhlIGluZm9ybWF0aW9uIG1heSBiZSBzaGFyZWQgd2l0aCBhZmZpbGlhdGVkIHRoaXJkIHBhcnRpZXMgaW4gYWdncmVnYXRlIGZvcm0gb25seS48L3A+DQoJCTxwPg0KCQkJPGI+SFRNTCBFbWFpbDwvYj48YnIgLz4NCgkJCUlmIHlvdSBzaWduIHVwIHRvIHJlY2VpdmUgZW1haWwgZnJvbSB1cyB5b3Ugd2lsbCByZWNlaXZlIGVtYWlscyBpbiBib3RoIHBsYWluIHRleHQgYW5kIEhUTUwgKHdpdGggaW1hZ2VzKSBmb3JtYXQuIElmIHlvdSBzZWxlY3QgdG8gdmlldyBvdXIgZW1haWxzIGluIHRoZSBIVE1MIGZvcm1hdCwgd2UgbWF5IHBsYWNlIGEgb25lLXBpeGVsIC5naWYgdG8gZGV0ZXJtaW5lIHdoZXRoZXIgb3Igbm90IHJlYWRlcnMgdmlld2VkIHRoZSBlbWFpbC4gVGhpcyBwcm9jZXNzIGRvZXMgbm90IGxlYXZlIGFueSBpbmZvcm1hdGlvbiBvbiB5b3VyIGNvbXB1dGVyLCBub3IgZG9lcyBpdCBjb2xsZWN0IGluZm9ybWF0aW9uIGZyb20geW91ciBjb21wdXRlci4gV2UgbWF5IHNoYXJlIHRoaXMgZGF0YSB3aXRoIG90aGVyIHRoaXJkIHBhcnRpZXMgaW4gYWdncmVnYXRlIGZvcm0gb25seSB0byBpbXByb3ZlIHRoZSBxdWFsaXR5IG9mIHRoZSBlbWFpbHMgYW5kIG9mZmVycyB3ZSBkaXN0cmlidXRlLjwvcD4NCgkJPHA+DQoJCQk8Yj5FbWFpbHMgRnJvbSBZb3U8L2I+PGJyIC8+DQoJCQlJZiB5b3Ugc2VuZCB1cyBlbWFpbHMsIGJlIGF3YXJlIHRoYXQgaW5mb3JtYXRpb24gZGlzY2xvc2VkIGluIGVtYWlscyBtYXkgbm90IGJlIHNlY3VyZSBvciBlbmNyeXB0ZWQgYW5kIHRodXMgbWF5IGJlIGF2YWlsYWJsZSB0byBvdGhlcnMuIFdlIHN1Z2dlc3QgdGhhdCB5b3UgZXhlcmNpc2UgY2F1dGlvbiB3aGVuIGRlY2lkaW5nIHRvIGRpc2Nsb3NlIGFueSBwZXJzb25hbCBvciBjb25maWRlbnRpYWwgaW5mb3JtYXRpb24gaW4gZW1haWxzLiBXZSB3aWxsIHVzZSB5b3VyIGVtYWlsIGFkZHJlc3MgdG8gcmVzcG9uZCBkaXJlY3RseSB0byB5b3VyIHF1ZXN0aW9ucyBvciBjb21tZW50cy48L3A+DQoJPC9saT4NCgk8bGk+DQoJCTxoMz4NCgkJCTxhIG5hbWU9IjgiPldoYXQgYWJvdXQgY2hhbmdlcyB0byB0aGUgUHJpdmFjeSBQb2xpY3k/PC9hPjwvaDM+DQoJCTxwPg0KCQkJVGhpcyBwcml2YWN5IHBvbGljeSBtYXkgYmUgY2hhbmdlZCBhdCBhbnkgdGltZTsgYW55IGNoYW5nZXMgd2lsbCBiZSBwb3N0ZWQgb24gdGhpcyB3ZWIgcGFnZS4gSW5mb3JtYXRpb24gY29sbGVjdGVkIHdoaWxlIGEgcGFydGljdWxhciB2ZXJzaW9uIG9mIHRoaXMgcG9saWN5IGlzIGluIGVmZmVjdCB3aWxsIGJlIGhhbmRsZWQgaW4gYWNjb3JkYW5jZSB3aXRoIHRoYXQgdmVyc2lvbi48L3A+DQoJCTxwPg0KCQkJSWYgeW91IGhhdmUgcXVlc3Rpb25zLCBjb21tZW50cyBvciBjb25jZXJucywgcGxlYXNlIGVtYWlsIHVzIGF0IDxhIGhyZWY9Im1haWx0bzpmZWVkYmFja0BLaXZ1dG8uY29tIj5mZWVkYmFja0BraXZ1dG8uY29tPC9hPi48L3A+DQoJPC9saT4NCjwvb2w+ZGQCIw9kFggCBw8PFgQfAwUhfi9XZWJTdG9yZS9TdXBwb3J0L0NvbnRhY3RVcy5hc3B4HgZUYXJnZXQFBV9zZWxmZGQCCQ8PFgQfAwUkfi9XZWJTdG9yZS9TdXBwb3J0L1NhZmVTaG9wcGluZy5hc3B4Hw8FBV9zZWxmZGQCCw8PFgQfAwUlfi9XZWJTdG9yZS9TdXBwb3J0L1ByaXZhY3lQb2xpY3kuYXNweB8PBQVfc2VsZmRkAhUPDxYCHwIFFXY0LjAuNjM5Ni4xOSAoRmV3czAxKWRkAgsPFgIfAmVkZIRkQj2yJ8BHsVWohjm+8tBj0siX" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=u6a1ShgHp8oy3Xk0QdrLMpAZkyIUYqBxBKeldwOU4P6dLogVK-mcAZiul1SNKO3WL1Xd38Z9CUvwdlsarV35O5VhXlM1&amp;t=636354762046607314" type="text/javascript"></script>


<script src="//static.onthehub.com/production/js/common/analytics.js?4.0.6396.19" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function HideStatusMessages() {{var statusMessages = document.all ? document.all['ctl00_smStatusMessages'] : document.getElementById('ctl00_smStatusMessages');statusMessages.style.display = 'none'; return false;}}//]]>
</script>

<script src="/ScriptResource.axd?d=ADX6pgLHQhaJu07wkPVBzCPj15G4gddXh9Z8y4-O0GEfrsoX9w94YjHB8upW_MlSmptz0KKrAwuOaXWYesI6-XMi7HAXBtgaTX9dWRgeOKaT-_vfxQXPnUsNHejeeH0VpC3xcpPhJ2dKo9xZX8MngcRPaEXuw2jpKyl2oMNEhaJZESZs0&amp;t=3d6efc1f" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Vuo1A-JX6MBacXB_LZ4djyr8WPkawjwNr4h0STdXJwVokmmZvLToRyclrBf8nyG5i8qbu2AsyNQpKV9cL85FGNUTuB8RH1pcP5nIKOfLRlfwgDgyNFhGMmFTFlmlADqN8lvtzCU3EeXtiW5dKrZRJk4KNl-6YgUZo-TKrLqecOSMGEsO0&amp;t=3d6efc1f" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
var PageMethods = function() {
PageMethods.initializeBase(this);
this._timeout = 0;
this._userContext = null;
this._succeeded = null;
this._failed = null;
}
PageMethods.prototype = {
_get_path:function() {
 var p = this.get_path();
 if (p) return p;
 else return PageMethods._staticInstance.get_path();},
OptInForTypeNoticeOnly:function(inputData,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'OptInForTypeNoticeOnly',false,{inputData:inputData},succeededCallback,failedCallback,userContext); },
HideTestBanner:function(webstoreID,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'HideTestBanner',false,{webstoreID:webstoreID},succeededCallback,failedCallback,userContext); },
ChangeResourceOptOutStatus:function(userResourceID,optOutStatus,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'ChangeResourceOptOutStatus',false,{userResourceID:userResourceID,optOutStatus:optOutStatus},succeededCallback,failedCallback,userContext); },
RemoveFromCartAsync:function(data,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'RemoveFromCartAsync',false,{data:data},succeededCallback,failedCallback,userContext); },
AddOfferingToCartAsync:function(input,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'AddOfferingToCartAsync',false,{input:input},succeededCallback,failedCallback,userContext); },
AddAddonToCartAsync:function(input,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'AddAddonToCartAsync',false,{input:input},succeededCallback,failedCallback,userContext); },
AddShopperResourceToCartAsync:function(input,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'AddShopperResourceToCartAsync',false,{input:input},succeededCallback,failedCallback,userContext); },
UpdateQuantityAsync:function(input,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'UpdateQuantityAsync',false,{input:input},succeededCallback,failedCallback,userContext); },
ClearNewAdditionsToCart:function(userID,succeededCallback, failedCallback, userContext) {
return this._invoke(this._get_path(), 'ClearNewAdditionsToCart',false,{userID:userID},succeededCallback,failedCallback,userContext); }}
PageMethods.registerClass('PageMethods',Sys.Net.WebServiceProxy);
PageMethods._staticInstance = new PageMethods();
PageMethods.set_path = function(value) { PageMethods._staticInstance.set_path(value); }
PageMethods.get_path = function() { return PageMethods._staticInstance.get_path(); }
PageMethods.set_timeout = function(value) { PageMethods._staticInstance.set_timeout(value); }
PageMethods.get_timeout = function() { return PageMethods._staticInstance.get_timeout(); }
PageMethods.set_defaultUserContext = function(value) { PageMethods._staticInstance.set_defaultUserContext(value); }
PageMethods.get_defaultUserContext = function() { return PageMethods._staticInstance.get_defaultUserContext(); }
PageMethods.set_defaultSucceededCallback = function(value) { PageMethods._staticInstance.set_defaultSucceededCallback(value); }
PageMethods.get_defaultSucceededCallback = function() { return PageMethods._staticInstance.get_defaultSucceededCallback(); }
PageMethods.set_defaultFailedCallback = function(value) { PageMethods._staticInstance.set_defaultFailedCallback(value); }
PageMethods.get_defaultFailedCallback = function() { return PageMethods._staticInstance.get_defaultFailedCallback(); }
PageMethods.set_enableJsonp = function(value) { PageMethods._staticInstance.set_enableJsonp(value); }
PageMethods.get_enableJsonp = function() { return PageMethods._staticInstance.get_enableJsonp(); }
PageMethods.set_jsonpCallbackParameter = function(value) { PageMethods._staticInstance.set_jsonpCallbackParameter(value); }
PageMethods.get_jsonpCallbackParameter = function() { return PageMethods._staticInstance.get_jsonpCallbackParameter(); }
PageMethods.set_path("PrivacyPolicy.aspx");
PageMethods.OptInForTypeNoticeOnly= function(inputData,onSuccess,onFailed,userContext) {PageMethods._staticInstance.OptInForTypeNoticeOnly(inputData,onSuccess,onFailed,userContext); }
PageMethods.HideTestBanner= function(webstoreID,onSuccess,onFailed,userContext) {PageMethods._staticInstance.HideTestBanner(webstoreID,onSuccess,onFailed,userContext); }
PageMethods.ChangeResourceOptOutStatus= function(userResourceID,optOutStatus,onSuccess,onFailed,userContext) {PageMethods._staticInstance.ChangeResourceOptOutStatus(userResourceID,optOutStatus,onSuccess,onFailed,userContext); }
PageMethods.RemoveFromCartAsync= function(data,onSuccess,onFailed,userContext) {PageMethods._staticInstance.RemoveFromCartAsync(data,onSuccess,onFailed,userContext); }
PageMethods.AddOfferingToCartAsync= function(input,onSuccess,onFailed,userContext) {PageMethods._staticInstance.AddOfferingToCartAsync(input,onSuccess,onFailed,userContext); }
PageMethods.AddAddonToCartAsync= function(input,onSuccess,onFailed,userContext) {PageMethods._staticInstance.AddAddonToCartAsync(input,onSuccess,onFailed,userContext); }
PageMethods.AddShopperResourceToCartAsync= function(input,onSuccess,onFailed,userContext) {PageMethods._staticInstance.AddShopperResourceToCartAsync(input,onSuccess,onFailed,userContext); }
PageMethods.UpdateQuantityAsync= function(input,onSuccess,onFailed,userContext) {PageMethods._staticInstance.UpdateQuantityAsync(input,onSuccess,onFailed,userContext); }
PageMethods.ClearNewAdditionsToCart= function(userID,onSuccess,onFailed,userContext) {PageMethods._staticInstance.ClearNewAdditionsToCart(userID,onSuccess,onFailed,userContext); }
var gtc = Sys.Net.WebServiceProxy._generateTypedConstructor;
Type.registerNamespace('eAcademy.Munthika.Web.Common');
if (typeof(eAcademy.Munthika.Web.Common.CartResult) === 'undefined') {
eAcademy.Munthika.Web.Common.CartResult=gtc("eAcademy.Munthika.Web.Common.CartResult");
eAcademy.Munthika.Web.Common.CartResult.registerClass('eAcademy.Munthika.Web.Common.CartResult');
}
//]]>
</script>

<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="98D4A53E" />
	<input type="hidden" name="__PREVIOUSPAGE" id="__PREVIOUSPAGE" value="42oWp96ZmECnpsvtrMnyRb2U1HX86f-vqaJ6SrFwfaftuHcsqngzB941KScjRI1GF5YDEt0HDpegMw-FEL2hUlmFbqUUyxFvK58Nzg5Cz2ckKGFGhBTf1uFkF5JPCvlhjRmxXA2" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAfJ/wzNf1c9wxMudam1nrEgVTvu3gWKD/R4jdhvYGyOVMx8J+VBLuxwTXAOXg9eqdVmsGuKKp5oS00Y4nHNvjI7VXpGQB+5+uolroumqcYWCyES1Bs+3i9wBXINDiumMtj1yEuv5c7anYxf+FNIvF++KSFs9RAznkfG0IwJ2/Rogx2AamA=" />
</div>
			<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$smScriptManager', 'aspnetForm', ['tctl00$ctl05','','tctl00$upCheckout',''], [], [], 300, 'ctl00');
//]]>
</script>

			
			
			<div id="ctl00_divWebStoreMaster">
				<input type="hidden" name="ctl00$hfUILocale" id="ctl00_hfUILocale" value="en_US" />
				
				<div id="header">
					<div class="navbar navbar-default" id="e5-1st-nav">
						<div class="container">
							<div id="ctl00_pnlRightMenuCollapse" class="row">
	
								<div class="navbar-header">									
									<div class="col-sm-offset-10 col-sm-2 col-xs-12">
										<div class="row">
											<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#kv-usermenu">
												<span class="sr-only">Toggle Navigation</span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
											</button>
										</div>
									</div>
								</div>
							
</div>
							<div id="header-row" class="row" role="navigation">
								<div class="header-row-right">
                                    
									    <div class="collapse navbar-collapse" id="kv-usermenu" style="overflow:hidden">
										    <div class="clearfix">
											    <ul class="nav navbar-nav navbar-right">												
												    <li>
													    <a id="ctl00_hlHomeTop" href="../ProductsByMajorVersionList.aspx?cmi_cs=1&amp;cmi_mnuMain=ed6ad73c-7bc7-e011-ae14-f04da23e67f6">Home</a>
												    </li>
												    <li>
													    <a id="ctl00_hlHelp" href="WebStoreHelpContents.aspx">Help</a>
												    </li>												
												    <li>
													    <a id="ctl00_ucCultureCurrencySelection_hlCulture" class="language" aria-label="Webstore culture and currency selection" href="../Common/SelectCurrencyAndCulture.aspx?rurl=%2fWebstore%2fSupport%2fPrivacyPolicy.aspx"><span id="ctl00_ucCultureCurrencySelection_linkCurrency" class="sr-only">US Dollars</span><span id="ctl00_ucCultureCurrencySelection_imgCountryLink" class="flag-icon flag-icon-us"></span><span id="ctl00_ucCultureCurrencySelection_lblCountry">English</span></a>

												    </li>
											    </ul>
										    </div>
										    <div id="ctl00_pnlUserContext" class="clearfix">
	
											    <ul class="nav navbar-nav navbar-right">																					
												    
												    <li id="ctl00_liSignIn">
													    <a id="ctl00_hlSignIn" href="/WebStore/Security/SignIn.aspx?rurl=%2fWebstore%2fSupport%2fPrivacyPolicy.aspx%3f">Sign In</a>
												    </li>
												    <li id="ctl00_registerListItem">
													    <a id="ctl00_llbRegister" href="javascript:__doPostBack(&#39;ctl00$llbRegister&#39;,&#39;&#39;)">Register</a>
												    </li>
											    </ul>
										    
</div>																																
									    </div>
                                    
								</div>								
								<div class="header-row-left">
									<div class="webstore-logo">
										<a id="ctl00_hlHome" href="../ProductsByMajorVersionList.aspx?cmi_cs=1&amp;cmi_mnuMain=ed6ad73c-7bc7-e011-ae14-f04da23e67f6"><img src="https://static.onthehub.com/production/attachments/15/49c547ba-f56d-dd11-bb6c-0030485a6b08/b4508715-1050-4b81-8d8b-de9cc7e3afbd.jpg" alt="OnTheHub" style="border-width:0px;" /></a>
                                        
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="e52ndNavContainer" class="container">
	
						<div ID="e5-2nd-nav" class="navbar">
							<div class="col-xs-12">
								<div id="ctl00_pnlSearch" class="search-panel" role="search" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ctl00_btnSearch&#39;)">
		
									<div class="form-group has-feedback">
										<div class="input-group">
											<span class='sr-only'>Product Search</span>
											<input name="ctl00$txtProductSearchName" type="text" maxlength="100" id="ctl00_txtProductSearchName" class="form-control" title="Product Search" placeholder="Product Search" />
											<span class="input-group-addon unpadded-group">
												<input type="submit" name="ctl00$btnSearch" value="" onclick="showDialog({&#39;title&#39;:kvLocalStore.Common.PleaseWait,&#39;content&#39;:kvLocalStore.Common.Processing,&#39;allowClosing&#39;:false});return true;WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$btnSearch&quot;, &quot;&quot;, false, &quot;&quot;, &quot;../ProductSearchOfferingList.aspx&quot;, false, false))" id="ctl00_btnSearch" class="fa search-btn" aria-label="Search" />
											</span>
										</div>
									</div>
								
	</div>
							</div>
							<div class="row">
								<div class="col-xs-12">
									<div class="tab-wrapper" id="ctl00_mnuMain">
		<div class="parent-nav" role="navigation">
			<ul class="nav nav-tabs">
				<li role="presentation"><a href="/WebStore/ProductsByMajorVersionList.aspx?cmi_mnuMain=ed6ad73c-7bc7-e011-ae14-f04da23e67f6">Most Popular</a></li><li role="presentation"><a href="/WebStore/ProductsByMajorVersionList.aspx?cmi_mnuMain=b1a6c67e-78c7-e011-ae14-f04da23e67f6">New Deals!</a></li><li role="presentation"><a href="/WebStore/ProductsByMajorVersionList.aspx?cmi_mnuMain=2ff73789-74c7-e011-ae14-f04da23e67f6">Data Analysis</a></li><li role="presentation"><a href="/WebStore/ProductsByMajorVersionList.aspx?cmi_mnuMain=59b45b5b-7cc7-e011-ae14-f04da23e67f6">Photo, Video & Design</a></li><li role="presentation"><a href="/WebStore/ProductsByMajorVersionList.aspx?cmi_mnuMain=d6d02f44-7dc7-e011-ae14-f04da23e67f6">Security & Utilities</a></li><li role="presentation"><a href="/WebStore/ProductsByMajorVersionList.aspx?cmi_mnuMain=7ff9e421-7fc7-e011-ae14-f04da23e67f6">Business & Office</a></li><li role="presentation"><a href="/WebStore/ProductsByMajorVersionList.aspx?cmi_mnuMain=8659da1b-241d-e611-941a-b8ca3a5db7a1">Browse by Publisher</a></li><li role="presentation" class="dropdown" style="display:none"><a data-toggle="dropdown" class="dropdown-toggle">More Options</a><ul class="dropdown-menu" id="parent-nav-dropdown">

				</ul></li>
			</ul>
		</div><div class="NavigationChildMenuSpacer PrimaryColor NavigationMenuPrimaryColor"></div>
	</div>
								</div>
							</div>
						</div>
					
</div>
				</div>
				<div id="main">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								
								
								
								
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div id="contentHeading" class="StatusMessageContainer">
									
									<div aria-live="polite">
										
									</div>
									<div id="validationSummaryContainer" aria-live="assertive" aria-atomic="true" tabindex="-1">
										<div id="ctl00_LocalizedValidationSummary1" class="alert alert-danger" style="display:none;">

</div>
										<div id="ctl00_ctl05">
	
												<span style="display:none" id="asyncValidationSummary"></span>
											
</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="e5flex">
								
								
								<div class="e5flex-body">
									
	<div class="card">
		<div class="card-header">
			<h2>
				<span id="ctl00_cpContent_lblPrivacyPolicy">Terms of Use and Privacy Policy</span>
			</h2>
		</div>
		<div class="card-body">
			<span id="ctl00_cpContent_lblPrivacyPolicyContent"><p>
	By browsing this Site, you agree that you have read and understand the following Terms of Use and Privacy Policy in its entirety, including any updates that may be posted on the Site from time to time. Kivuto Solutions Inc. reserves the right to revise the Terms of Use and Privacy Policy, at its sole discretion, by updating this posting.</p>
<p>
	References to Kivuto Solutions Inc. (or Kivuto) in this document also apply to Kivuto&rsquo;s wholly owned brands, Texidium and OnTheHub.</p>
<ol>
	<li>
		<a href="#1">What information do you gather about me?</a></li>
	<li>
		<a href="#2">How do you collect and use personally identifiable information?</a></li>
	<li>
		<a href="#3">How can I access, update or delete my personal information?</a></li>
	<li>
		<a href="#4">How do you collect and use non-personally identifiable information?</a></li>
	<li>
		<a href="#5">What security is in place to protect my information?</a></li>
	<li>
		<a href="#6">Where will my information be processed?</a></li>
	<li>
		<a href="#7">What is your email policy?</a></li>
	<li>
		<a href="#8">What about changes to the Privacy Policy?</a></li>
</ol>
<ol>
	<li>
		<h3>
			<a name="1">What information do you gather about me?</a></h3>
		<p>
			Both personally and non-personally identifiable information about each visitor may be gathered from this site. The type of information gathered depends on your actions and interaction with the site.</p>
		<p>
			<b>Personally Identifiable Information</b><br />
			Personally identifiable information is only collected when you voluntarily submit this information on an as-needed basis such as during registration and ordering. Any personal information that you may share with us is kept private. Neither your name nor any other identifying data about you is sold or shared with any other third party unless required for the fulfillment or refund of your order or service. More information on this use is provided later in this privacy policy.</p>
		<p>
			<b>Non-Personally Identifiable Information</b><br />
			Non-personally identifying information is gathered through our site technology to create a more personalized and relevant experience during your visit. The information also allows us to improve the functionality of the website to better serve you.</p>
	</li>
	<li>
		<h3>
			<a name="2">How do you collect and use personally identifiable information?</a></h3>
		<p>
			<b>Collecting Personally Identifiable Information</b><br />
			Personally identifiable information may be collected during registration, ordering and billing, during customer service interactions or when you enter a contest. Personally identifiable information is never shared in ways unrelated to the methods described below without notifying you of the intent beforehand, and also providing you with an opportunity to opt-out or otherwise prohibit such unrelated uses.</p>
		<p>
			<b>Registration</b><br />
			During registration, you may be prompted for your name and email address for authentication and verification purposes. These steps may be necessary to ensure users are properly identified and authenticated as a qualified user. Some offerings and promotions on this site may be restricted to members of certain organizations and, by providing your email address, and subsequently having your account verified, you may benefit from these offerings and promotions.</p>
		<p>
			<b>Ordering and Billing</b><br />
			When you place an order, you may need to supply your credit card information along with the corresponding mailing information. This information may be required to fulfill your order. Your credit card information is not stored on our server; it is simply passed on to the credit card processing system for validation. You may also be prompted for your name and email address to create your online receipt and for us to send you order related emails.</p>
		<p>
			<b>Customer Service</b><br />
			Personally identifiable information may be collected if you report a problem or submit an inquiry to our customer service team(s). A record of that correspondence may be maintained to provide you with an optimal customer service experience.</p>
		<p>
			<b>Entering a Contest or Promotion</b><br />
			Furthermore, information from you may be collected for entry into contests and similar promotions when you voluntarily choose to participate. All contests have their own terms and conditions which can be viewed on the contest&rsquo;s page.</p>
		<p>
			<b>Using Personally Identifiable Information</b><br />
			Your personally identifiable information is not sold, traded or rented to others.</p>
		<p>
			We do employ other trusted companies to perform certain functions on our behalf, including order fulfillment and delivery, as well as credit card processing. These business affiliates have access to personal information needed to perform their functions, but are not permitted to use your personal information for any other purpose.</p>
		<p>
			Additionally, information related to the order of a product may be shared with the supplier of that particular product, for warranty and support purposes. This may include your name, date of your order and product purchased.</p>
		<p>
			If you have questions about how one of our business partners or suppliers use information about you, contact them directly.</p>
	</li>
	<li>
		<h3>
			<a name="3">How can I access, update or delete my personal information?</a></h3>
		<p>
			Users have the ability to correct or change any information or preferences in their account profile. Users may change this information at any time, provided they are signed in.</p>
		<p>
			You can have your account deactivated or deleted by contacting Support. However, doing so will limit your access to members-only areas of the site. Any purchase history information, however, will remain in protected permanent records.</p>
	</li>
	<li>
		<h3>
			<a name="4">How do you collect and use non-personally identifiable information?</a></h3>
		<p>
			We use technologies like cookies, pixel tags (&quot;pixels&quot;), and local storage to deliver, secure, and understand products, services, and ads, on and off our sites.</p>
		<p>
			<b>Collecting non-personally identifiable information</b><br />
			Your verification status is collected once you successfully register through any of our verification services. The resulting status is non-personally identifiable information and is stored in case you need to verify your status again in the future.</p>
		<p>
			Non-personally identifiable information is collected using technology such as cookies. &quot;Cookies&quot; are small pieces of information that are stored by your browser on your local storage. The information collected by this technology is non-personally identifiable only and allows us to optimize your interactions with the site.</p>
		<p>
			<b>Using non-personally identifiable information</b><br />
			Cookies allow for customer traffic patterns and site usage to be monitored on a general basis to help develop and improve the design, layout and efficiency of the website. Understanding these patterns allows us to provide more relevant content to you and improve customer relationships. Furthermore, the information collected is used to create targeted communications to offer you a more personalized and customizable experience. Cookies also save you time by remembering your personalized settings.</p>
		<p>
			Note that all information is collected and analyzed in aggregated form only. Check your browser to ensure cookies are enabled for this site to work properly.</p>
		<p>
			Kivuto Solutions Inc. may use third-party advertising technology partners to serve you relevant advertisements on websites outside of our network. The advertising technology partner provides us with tracking technology, such as a cookie, which is added to your browser by us. This allows us to serve you very relevant ads on their partner websites according to your engagements with this site. If you would like to learn more about this process or even opt-out, please contact us.</p>
		<p>
			<b>How to manage cookies</b><br />
			Browser controls are available to allow, block or delete cookies. To manage the cookies this site and others use, visit the following site for instructions: <a href="http://windows.microsoft.com/en-US/windows-vista/Block-or-allow-cookies" target="_blank">http://windows.microsoft.com/en-US/windows-vista/Block-or-allow-cookies.</a></p>
		<p>
			Please note that by adjusting your cookie settings, any settings or preferences will no longer be in effect. The usage and functionality of the site will be subject to change including the ability to purchase certain products and view content on the site.</p>
	</li>
	<li>
		<h3>
			<a name="5">What security is in place to protect my information?</a></h3>
		<p>
			Your information is protected both during transmission and in storage. Your account and profile information are password-protected so that you and only you have access to this personal information. During purchase, the secure server software (SSL) encrypts all information you input before it is transmitted. As such, all customer data is protected against unauthorized access on the system&#39;s secure servers.</p>
	</li>
	<li>
		<h3>
			<a name="6">Where will my information be processed?</a></h3>
		<p>
			Kivuto Solutions, Inc. is a Canadian corporation with offices at 126 York Street, Suite 200, Ottawa, Ontario, Canada. Personal information gathered by Kivuto is stored on servers located in Canada and will not be transferred to any other country unless it is required as part of the fulfillment of service. See <strong>Section 2</strong> for more details about Kivuto&rsquo;s treatment of personal information.</p>
		<p>
			Due to agreements with our software publisher partners, your data will be stored on our servers for seven years.</p>
		<p>
			Privacy laws and requirements differ according to each country. Please know, however, that all information collected through Kivuto&rsquo;s Websites will be protected by the terms of this Privacy Policy and any Privacy Notices that apply to you, regardless of what country the information originates from.</p>
	</li>
	<li>
		<h3>
			<a name="7">What is your email policy?</a></h3>
		<p>
			Kivuto Solutions Inc. complies fully with the federal CAN-SPAM Act of 2003 and Canada&#39;s Anti-Spam Legislation (CASL). We will not share, sell, rent, swap or authorize any third party to use your email address for commercial purposes without your permission. If you feel you have received an email from us in error, contact <a href="mailto:feedback@Kivuto.com">feedback@kivuto.com</a>.</p>
		<p>
			You may receive one or more of the following emails from Kivuto Solutions.</p>
		<p>
			<b>Account and Service-Related Email</b><br />
			Kivuto Solutions Inc. reserves the right to send you emails relating to your account status. This includes order confirmations, renewal/expiration notices, notices of credit-card problems, other transactional emails and notifications about major changes to the site services and/or to our Privacy Policy. If you have registered for online discussions or other services, you may receive an email specific to your participation in those activities.</p>
		<p>
			<b>Email Newsletters</b><br />
			Kivuto Solutions Inc. offer several email newsletters. You will only receive these if you previously chose to opt-in to these types of communications. If you no longer wish to receive a specific newsletter, follow the &quot;unsubscribe&quot; instructions located near the bottom of each newsletter.</p>
		<p>
			<b>Promotional Email</b><br />
			Kivuto Solutions Inc. may periodically email you messages about products and services that we think may be of interest to you (if you have previously opted in to receive these types of communications). You can choose not to receive messages in the future by following the &quot;unsubscribe&quot; instructions located near the bottom of each email.</p>
		<p>
			<b>Survey Email</b><br />
			We may send you emails inviting you to participate in user surveys, asking for feedback on our services and existing or prospective products and services, as well as information to better understand our users. User surveys greatly help us to improve our services, and any information we obtain in such surveys will be used internally. The information may be shared with affiliated third parties in aggregate form only.</p>
		<p>
			<b>HTML Email</b><br />
			If you sign up to receive email from us you will receive emails in both plain text and HTML (with images) format. If you select to view our emails in the HTML format, we may place a one-pixel .gif to determine whether or not readers viewed the email. This process does not leave any information on your computer, nor does it collect information from your computer. We may share this data with other third parties in aggregate form only to improve the quality of the emails and offers we distribute.</p>
		<p>
			<b>Emails From You</b><br />
			If you send us emails, be aware that information disclosed in emails may not be secure or encrypted and thus may be available to others. We suggest that you exercise caution when deciding to disclose any personal or confidential information in emails. We will use your email address to respond directly to your questions or comments.</p>
	</li>
	<li>
		<h3>
			<a name="8">What about changes to the Privacy Policy?</a></h3>
		<p>
			This privacy policy may be changed at any time; any changes will be posted on this web page. Information collected while a particular version of this policy is in effect will be handled in accordance with that version.</p>
		<p>
			If you have questions, comments or concerns, please email us at <a href="mailto:feedback@Kivuto.com">feedback@kivuto.com</a>.</p>
	</li>
</ol></span>
		</div>
	</div>

								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div id="footer">
					
  <div class="container-fluid footer-content">
	<div class="container">
        <div class="row">
            
                <div class="col-md-4 col-sm-6 crunch-center-sm">
				    <div class="row">
					    <div class="col-md-12">
						    <h2>
                                Connect With Us
						    </h2>
					    </div>
				    </div>
				    <div class="row">
					    <div class="col-md-12 social-link">
                            <a id="ctl00_ucFooter_hlTwitter" href="http://www.twitter.com/onthehub" target="_blank">
                                <span class="sr-only">
                                    Twitter
                                </span>
								<span class="fa-stack tw">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa-twitter fa-stack-1x fa-inverse"></span>
								</span>
                            </a><a id="ctl00_ucFooter_hlFacebook" href="http://www.facebook.com/pages/On-The-Hub/25131788360?sid=7fd7b2be4847d87eb78d0e758b86dc7c" target="_blank">
								<span class="sr-only">
                                    Facebook
								</span>
                                <span class="fa-stack fb">
									<span class="fa fa-circle fa-stack-2x"></span>
                                    <span class="fa fa-facebook fa-stack-1x fa-inverse"></span>
								</span>
                            </a><a id="ctl00_ucFooter_hlGooglePlus" href="https://plus.google.com/115290003144094460033/about" target="_blank">
								<span class="sr-only">
								    Google+
                                </span>
                                <span class="fa-stack gp">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa-google-plus fa-stack-1x fa-inverse"></span>
								</span>
                            </a><a id="ctl00_ucFooter_hlBlog" href="http://blog.onthehub.com/" target="_blank">
								<span class="sr-only">
								    Blog
                                </span>
                                <span class="fa-stack bl">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa fa-rss fa-stack-1x fa-inverse"></span>
								</span>
                            </a><a id="ctl00_ucFooter_hlPinterest" href="https://www.pinterest.com/onthehub/" target="_blank">
                                <span class="sr-only">
                                    Pinterest
                                </span>
                                <span class="fa-stack pi">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa fa-pinterest-p fa-stack-1x fa-inverse"></span>
								</span>
                            </a><a id="ctl00_ucFooter_hlInstagram" href="https://instagram.com/onthehub/" target="_blank">
                                <span class="sr-only">
                                    Instagram
                                </span>
                                <span class="fa-stack ig">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa fa-instagram fa-stack-1x fa-inverse"></span>
								</span>
                            </a>
					    </div>
				    </div>
			    </div>
            
            <div class="col-md-4 col-sm-6 crunch-center-sm crunch-spacer">
			    <div class="row">
				    <div id="ctl00_ucFooter_pnlOTHHeader" class="col-md-12">
	
					    <h2>OnTheHub</h2>
				    
</div>
					
			    </div>
			    <div class="row">
				    <div class="col-sm-6">
					    <div class="row">
						    <div class="col-md-12">
							    <div class="footer-label kivuto">
                                    <a id="ctl00_ucFooter_lhlContactUs" href="ContactUs.aspx" target="_self">Contact Us</a>
							    </div>
						    </div>
						    <div class="col-md-12">
							    <div class="footer-label kivuto">
                                    <a id="ctl00_ucFooter_lhlSafeShopping" href="SafeShopping.aspx" target="_self">Safe Shopping</a>
							    </div>
						    </div>
					    </div>
				    </div>
				    <div class="col-sm-6">
					    <div class="row">
						    <div class="col-md-12">
							    <div class="footer-label kivuto">
                                    <a id="ctl00_ucFooter_lhlPrivacy" href="PrivacyPolicy.aspx" target="_self">Privacy Policy</a>
							    </div>
						    </div>
                            
					    </div>
                    </div>
			    </div>
		    </div>
            <div id="ctl00_ucFooter_kvtImageHolder" class="col-md-4">
				<div class="row hidden-sm hidden-xs">
					<div style="height:60px"></div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<a id="ctl00_ucFooter_hlEAcademy" href="http://www.kivuto.com" target="_blank"><img id="ctl00_ucFooter_Image2" class="img-responsive center-block" src="//static.onthehub.com/production/images/PoweredByKivuto.png?4.0.6396.19" alt="Powered by Kivuto Solutions Inc." style="border-width:0px;" /></a>
					</div>					
					<div class="col-xs-6">
						<img id="ctl00_ucFooter_imgSslSeal" class="img-responsive center-block" src="//static.onthehub.com/production/images/RapidSSL_SEAL-90x50.gif?4.0.6396.19" alt="Secured by RapidSSL" style="border-width:0px;" />
					</div>              
				</div>
			</div>
        </div>
    </div>
</div>
<div class="container-fluid e5-version">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="version-text">
                    <small>                        
						<span id="ctl00_ucFooter_lblCopywrite">
							© Kivuto Solutions Inc. All Rights Reserved. - 
						</span>
						
                        <bdi><span id="ctl00_ucFooter_lblVersion">v4.0.6396.19 (Fews01)</span></bdi>
                    </small>
                </div>
            </div>
        </div>
    </div>
</div>
				</div>
			</div>
			
			<div id="notice-only-opt-ins">
				
			</div>
			<div id="ctl00_upCheckout" style="display:none">
	
					<a id="llbGeneralExpressCheckout" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$llbGeneralExpressCheckout&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, true))">Express Checkout</a>
					<a id="llbGeneralCheckout" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$llbGeneralCheckout&quot;, &quot;&quot;, true, &quot;&quot;, &quot;&quot;, false, true))">Check Out</a>
				
</div>
			<div id="fb-root"></div>
			
		
<script type="text/javascript">
//<![CDATA[
var Page_ValidationSummaries =  new Array(document.getElementById("ctl00_LocalizedValidationSummary1"));
//]]>
</script>


<script type="text/javascript">
//<![CDATA[

(function(id) {
    var e = document.getElementById(id);
    if (e) {
        e.dispose = function() {
            Array.remove(Page_ValidationSummaries, document.getElementById(id));
        }
        e = null;
    }
})('ctl00_LocalizedValidationSummary1');
//]]>
</script>
</form>
		

<script type="text/javascript">

    /* Google Analytics */
    var _gaq = _gaq || [];
    var _ga = _ga || {};
    (function (data) {
        if (DoTracking) {
            DoTracking(data); // in Analytics.js
        }
    })({"config":{"classicTrackerCode":"UA-2768482-4","classicUmbrellaTrackerCode":"UA-2768482-14","umbrellaTrackingDomain":"onthehub.com","universalTrackerCode":"UA-2768482-18"},"customVariables":{"brand":"onthehub e-store","customerLoyalty":"Prospect (0 purchases)","groups":null,"program":null,"webstore":"OnTheHub"},"eCommerceData":null});

    

</script>

		

<!-- Facebook Conversion Code for e5 Visits -->
<script type="text/javascript">
    var fb_param = {};
    fb_param.pixel_id = '6017038665868';
    fb_param.value = '0.00';
    fb_param.currency = 'USD';
    (function () {
        var fpw = document.createElement('script');
        fpw.async = true;
        fpw.src = '//connect.facebook.net/en_US/fp.js';
        var ref = document.getElementsByTagName('script')[0];
        ref.parentNode.insertBefore(fpw, ref);
    })();
</script>
<noscript>
    <img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6017038665868&amp;value=0&amp;currency=USD" />
</noscript>
	</body>
</html>