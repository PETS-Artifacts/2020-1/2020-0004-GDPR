<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Language" content="en">
<title>Fastcooking.ca - Privacy Policy</title>
<meta name="keywords" content="pressure cooker, pressure cookers, pressure cooking, fagor, fagor pressure cooker, fagor duo pressure cooker, fagor splendid pressure cooker, pressure cooker recipe, pressure cooker recipe books, pressure cooker energy savings, pressure cooker corn, pressure cooker canada, pressure cooker ottawa, pressure cooker pot roast, pressure cooker chicken, slow cooker, healthy fast food, saving energy">
<meta name="description" content="New generation pressure cookers, Fast stainless steel Fagor pressure cookers. Use a pressure cooker to cook delicious, nutritious and healthy home cooked food in about 1/3 the time it would take without using a pressure cooker.">
<meta name="copyright" content="©2005-2009 Circular Input Products Ltd. All Rights Reserved">
<link rel="stylesheet" type="text/css" href="css/screen_default.css">
<!--[if lt IE 7]><style type="text/css" media="screen">body{behavior:url(css/csshover.htc);}</style><![endif]-->

<style type="text/css">
        body { text-align: center; margin: 0; padding: 0; color: black; background: #E4833C; font: 0.86em arial,helvetica,sans-serif; height: 100%; min-height: 100%; }
        html { height: 100%; min-height: 100%; }
        #wrapper { text-align: left; width: 760px; margin: 0 auto; background: white; padding: 0 10px; min-height: 100%; }
        * html #wrapper { height: 100%; } /* hack that only IE can read */
        a:hover { background-color: #FFFF93 }
        img { border: none }
        h1 { font-size: 27px; color: #FFFFFF; margin: 0; }
        #header_out { float: left; margin: 2px 0 0 0; } /* with FF "#header_out p" results in top margin of 1em */
        #header_in { float: right; text-align: right; font-size: 20px; margin: 11px 0 0 0; }
        .smaller_font { font-size: 0.9em; }
</style>
</head>
<body>
<div id="wrapper">


<p id="header_out"><a href="index.htm"><img src="img/45-fastcooking_logo_berlin_298_by_34.gif" alt="Fastcooking.ca :: new generation pressure cookers" width="298" height="34"></a></p>
<p id="header_in">The New Generation Pressure Cooker Experts</p>
<div class="clearthefloats"></div>

<div align="center">
 <table cellspacing="1" cellpadding="0">
  <tr>
   <td><a class="linkopacity" href="index.htm"><img src="img/Button_Home_blue.gif" alt="Home" width="91" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/cooking_times_pressure_cooker.php"><img src="img/Button_Cooking_Times_blue.gif" alt="Pressure Cooker Cooking Times" width="98" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/recipes_pressure_cooker_index.php"><img src="img/Button_Recipes_blue.gif" alt="Pressure Cooker Recipes" width="91" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/cookbooks_pressure_cookers.php"><img src="img/Button_Cookbooks_blue.gif" alt="Pressure Cooker Recipe Books" width="98" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/energy_savings_pressure_cooker.php"><img src="img/Button_Energy_Savings_blue.gif" alt="Pressure Cooker Energy Savings" width="98" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/how_pressure_cookers_work.php"><img src="img/Button_How_It_Works_blue.gif" alt="Pressure Cookers:  How They Work" width="93" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/safety_pressure_cookers.php"><img src="img/Button_Safety_blue.gif" alt="Pressure Cooker Safety" width="86" height="20"></a></td>
   <td><a class="linkopacity" href="pricing.php"><img src="img/Button_Pricing_blue.gif" alt="Pressure Cooker Pricing" width="98" height="20"></a></td>
  </tr>
 </table>
</div>

<p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
<div align="center">
  <table cellspacing="0" cellpadding="10">
   <tr>
    <td bgcolor="#E4833C"><h1>Privacy Policy</h1></td>
   </tr>
  </table>
</div>

<p>&nbsp;</p>
<p>We have an industry leading privacy policy that is clear and concise.&nbsp; Feel free to compare it to any other 
 retailer.</p>
<p>This privacy policy applies to Circular Input Products Ltd.'s customers and fastcooking.ca visitors.</p>
<p>Circular Input Products Ltd. does not sell, rent, lease, trade, loan, give away or otherwise disclose to third 
 parties any personal information you provide to us when you make an inquiry or place an order except in the 
 following situation:&nbsp; Your name and address may be electronically submitted to Canada Post or another 
 shipping company and will appear on a shipping label in order to ship you the product you purchase.&nbsp; Your phone number 
 may also be disclosed to Canada Post or another shipping company, in the  event of a late delivery or insurance 
 claim; otherwise, your phone number is only used to contact you in case of an issue with your inquiry or order.&nbsp; 
 We do not and will not do any telemarketing.&nbsp; We aren't going to sell, rent, lease, trade, loan or give away 
 the e-mail address or other informtion you gave us to obnoxious spammers, data brokers or data compilers.</p>
<p>We may disclose your information without your consent in order to comply with a subpoena, warrant or order made 
 by a court with jurisdiction to compel the production of such information; to legal authorities for the detection 
 and prevention of fraud or other criminal activity; or to our legal counsel in the event that any matter may 
 be the subject of litigation.</p>
<p>If anyone were to ever buy Circular Input Products Ltd. (we are privately held and 
 not for sale, so it's pretty unlikely), then they would own your personal information, but they would also be required to 
 follow these same policies as terms of the sale or would be required to allow you to delete your personal information 
 held by them if the policy changes materially.</p>
<p>It is possible that you might one day get a postcard, letter or paper catalogue from us in the mail, but we 
 aren't going to bombard you with constant mailings because it is expensive, generally useless, wastes trees and 
 defeats the purpose of having a web site in the first place.</p>
<p>If you'd like to remain anonymous in purchasing our products, feel free to send us cash or a money order in the mail,
 along with a drop box location that can be served by a shipper where you would like your order shipped.&nbsp; We don't
 see any reason to be that private, but we'll work with you even if you are!&nbsp; Just be sure to include an anonymous
 e-mail address so that we can contact you if we can't read your handwriting or the amount is incorrect.</p>
<p>We only collect the personal information you provide to us.&nbsp; We may also gather general information such as the 
 date, time, browser type, operating system, platform, navigation history, and IP address of all users who visit 
 our web site if the browser you use sends this information in the HTTP headers.&nbsp; If you have JavaScript turned on 
 in your browser, we may also collect whether or not you have various media players installed.</p>
<p>If you have any questions or concerns, you can reach us at the following mailing address:</p>
<p>PRIVACY OFFICER<br>
 CIRCULAR INPUT PRODUCTS LTD.<br>
 PO BOX 44059<br>
 OTTAWA ON&nbsp; K1K 4P8<br>
 CANADA</p>
<p><img src="img/envelope.gif" align="absmiddle" width="23" height="14">&nbsp; <script language="JavaScript" type="text/JavaScript">var a,b,c,d;a='<a href="mai';b='buyaPC';c='">';a+='lto:';b+='@';d='</a>';b+='fastcooking.';b+='ca';document.write(a+b+c+b+d);</script>
 <noscript>Turn JavaScript on to view our e-mail.</noscript></p>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p align="center"><a href="index.htm"><img src="img/45-fastcooking_logo_berlin_219_by_25.gif" align="bottom"
 alt="click for pressure cookers :: home" title="click for pressure cookers :: home" width="219" height="25"></a></p>

<div align="center" style="margin-top: 1em; margin-bottom: 10px">
 <table width="350">
  <tr>
   <td align="center" width="175" class="smaller_font"><a href="about_us.php">About Us</a></td>
   <td align="center" width="175" class="smaller_font"><a href="return_policy.php">Return Policy</a></td>
   <td align="center" width="175" class="smaller_font"><a href="contact_us.htm">Contact Us</a></td>
  </tr>
 </table>
</div>

<div align="center">
 <table cellspacing="1" cellpadding="0">
  <tr>
   <td><a class="linkopacity" href="index.htm"><img src="img/Button_Home_blue.gif" alt="Home" width="91" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/cooking_times_pressure_cooker.php"><img src="img/Button_Cooking_Times_blue.gif" alt="Pressure Cooker Cooking Times" width="98" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/recipes_pressure_cooker_index.php"><img src="img/Button_Recipes_blue.gif" alt="Pressure Cooker Recipes" width="91" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/cookbooks_pressure_cookers.php"><img src="img/Button_Cookbooks_blue.gif" alt="Pressure Cooker Recipe Books" width="98" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/energy_savings_pressure_cooker.php"><img src="img/Button_Energy_Savings_blue.gif" alt="Pressure Cooker Energy Savings" width="98" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/how_pressure_cookers_work.php"><img src="img/Button_How_It_Works_blue.gif" alt="Pressure Cookers:  How They Work" width="93" height="20"></a></td>
   <td><a class="linkopacity" href="pressure_cookers/safety_pressure_cookers.php"><img src="img/Button_Safety_blue.gif" alt="Pressure Cooker Safety" width="86" height="20"></a></td>
   <td><a class="linkopacity" href="pricing.php"><img src="img/Button_Pricing_blue.gif" alt="Pressure Cooker Pricing" width="98" height="20"></a></td>
  </tr>
 </table>
</div>

<p class="footer">&copy;2005-2009 Circular Input Products Ltd., All Rights Reserved.&nbsp;
&nbsp;&nbsp; Problems or suggestions about website:&nbsp; contact <script language="JavaScript"
type="text/JavaScript">var a,b,c,d;a='<a href="mai';b='webpeople';c='">';a+='lto:';b+='@';d='</a>';b+='fastcooking.';b+='ca';document.write(a+b+c+b+d);</script>.</p>

</div>
</body>
</html>