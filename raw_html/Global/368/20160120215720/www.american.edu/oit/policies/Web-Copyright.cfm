<!DOCTYPE html> <html lang="en"> <!-- Content Copyright American University -->
<!-- Page generated 2016-01-20 14:40:48 on server 4 by CommonSpot Build 9.0.3.119 (2015-08-14 15:00:01) -->
<!-- JavaScript & DHTML Code Copyright &copy; 1998-2015, PaperThin, Inc. All Rights Reserved. --> <head> <title>American University's Web Copyright Policy</title> <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="Viewport" content="width=device-width, initial-scale=1.0">
<meta name="Description" id="Description" content="Web Copyright" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="Keywords" id="Keywords" content="technology, american university, dc, students, washington dc" />
<meta name="Generator" id="Generator" content="CommonSpot Build 9.0.3.119" />
<link rel="stylesheet" href="/style/base.css" type="text/css" />
<link rel="stylesheet" href="/style/grid.css" type="text/css" />
<link rel="stylesheet" href="/style/mediaupload.css" type="text/css" />
<link rel="stylesheet" href="/style/accordion.css" type="text/css" />
<link rel="stylesheet" href="/style/content-layout.css" type="text/css" />
<link rel="stylesheet" href="/style/content-modules.css" type="text/css" />
<link rel="stylesheet" href="/style/modules/wrapper-styles.css" type="text/css" />
<link rel="stylesheet" href="/style/modules/homepage-modules.css" type="text/css" />
<link rel="stylesheet" href="/style/modules/category-page-modules.css" type="text/css" />
<link rel="stylesheet" href="/style/modules/profile-page-modules.css" type="text/css" />
<link rel="stylesheet" href="/style/sIFR-screen.css" type="text/css" />
<link rel="stylesheet" href="/style/ie.css" type="text/css" />
<link rel="stylesheet" href="/style/default.css" type="text/css" />
<link rel="stylesheet" href="/style/cs_overrides.css" type="text/css" />
<style type="text/css">article, aside, details, figcaption, figure, footer, header, hgroup, main, nav, section, summary {display: block;}</style>
<style type="text/css">
.mw { color:#000000;font-family:Verdana,Arial,Helvetica;font-weight:bold;font-size:xx-small;text-decoration:none; }
a.mw:link	{color:#000000;font-family:Verdana,Arial,Helvetica;font-weight:bold;font-size:xx-small;text-decoration:none;}
a.mw:visited	{color:#000000;font-family:Verdana,Arial,Helvetica;font-weight:bold;font-size:xx-small;text-decoration:none;}
a.mw:hover	{color:#0000FF;font-family:Verdana,Arial,Helvetica;font-weight:bold;font-size:xx-small;text-decoration:none;}
</style> <script type="text/javascript">
<!--
var gMenuControlID = 0;
var menus_included = 0;
var jsDlgLoader = '/oit/policies/loader.cfm';
var jsSiteID = 1;
var jsSubSiteID = 337;
var js_gvPageID = 13073;
var jsPageID = 13073;
var jsPageSetID = 0;
var jsPageType = 0;
var jsControlsWithRenderHandlers = ",8,8,8,20,20,20,20,23,23,23,23,23,23,23,36,36,55,55,55,55,1333,1467,1494,1801,1810,1810,1810,1810,1810,1976,1982,1982,1982,1982,1982,1982,1982,1982,1982,2001,2001,2001,2001,2001,2419,2419,2419,2419,2503,2638,2638,2638,2638,2638,2638,2833,2833,2898,2898,2898,2898,2898,2898,2898,2953,2953,2953,2953,2953,2953,2953,2953,2953,2953,2953,2953,2953,2953,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3037,3109,3109,3109,3109,3109,3109,4456,4499,4562,4562,4562,4562,4562,4562,4562,4562,4717,4866,4866,4866,4866,4866,4866,4866,4866,4866,4866,5165,5165,5165,5165,5165,5165,5165,5165,5165,5318,6031,7263,8088,8118,8118,8118,8350,8954,8954,8954,8954,8954,8954,10471,10471,10492,10492,10719,10719,10719,10719,11983,11983,12588,12588,13551,13658,13658,13817,13817,13817,13817,16152,16498,16498,16498,16498,19441,25087,29572,29572,29572,29572,29572,29572,29572,29572,32119,33825,37008,50070,50070,50070,50070,86091,88515,93959,93959,93959,93959,93959,93959,93959,93959,93959,93959,97233,97233,97263,97274,97286,97777,97777,99291,99291,99291,99291,99291,99966,99966,101583,101583,101583,101583,102258,112430,112430,112430,112430,112430,130643,131079,131079,131079,131079,131079,144996,144996,172292,174249,174304,174307,202612,267288,304341,304341,756671,1680191,1805183,1805183,1807446,1807446,1807446,2013080,2051000,2051000,2916090,2916090,2916090,2916090,3014155,3014155,3014155,3014155,3014155,3014155,3035885,3542087,3543241,3543241,3543241,3543241,3543241,3543403,3561554,3561554,3732115,3732115,3904245,3938645,3938684,4132797,4132805,4132815,4132843,4133032,4133052,4135770,";
var jsDefaultRenderHandlerProps = ",,";
var jsAuthorizedControls = ",1,2,3,4,6,7,8,9,10,11,16,18,20,21,22,23,25,26,27,28,29,30,31,36,39,40,41,42,43,45,46,47,50,51,52,53,55,1333,1467,1494,1801,1810,1936,1976,1982,2001,2221,2239,2318,2347,2390,2392,2398,2406,2414,2419,2420,2426,2434,2445,2450,2456,2463,2503,2638,2833,2898,2953,3037,3109,3181,3359,3711,4292,4430,4450,4456,4499,4562,4642,4717,4866,4874,5165,5318,6031,7263,7511,7513,8088,8116,8118,8350,8554,8954,9194,10273,10471,10492,10719,11983,12588,12595,12600,12609,13551,13658,13817,13968,16152,16498,18786,18810,18816,18823,18830,18836,18842,18848,18854,18860,19387,19436,19441,19509,25087,25219,25237,25262,26776,28298,29287,29559,29572,31106,31399,32119,33825,37008,37611,38022,40032,40590,41642,41659,41766,41768,50070,86091,88515,89868,89872,90992,93764,93959,96365,97160,97233,97263,97274,97286,97777,99291,99966,101583,102258,111193,111230,112430,118753,118934,119264,127999,130643,131079,132803,140035,140044,142898,143502,144996,146018,158585,159431,160531,160535,161327,161335,161553,162107,162114,162207,162221,166183,172292,174249,174304,174307,177702,179029,179821,185920,188890,188967,195220,196342,196345,196614,202612,207946,211307,213101,215794,215803,215815,215824,217418,234188,236787,236789,267288,271919,304341,306245,308739,308874,308876,311273,311276,394158,394297,466283,472823,480281,480283,480286,480369,480371,480378,504758,523087,523102,596529,608143,609180,611516,693430,755942,756662,756671,1026449,1032256,1032258,1467221,1467225,1467232,1467264,1467316,1680191,1681336,1681348,1681397,1681399,1681402,1681404,1681406,1683757,1716819,1719928,1720607,1720609,1721900,1722045,1722072,1722112,1722115,1722120,1722125,1722134,1805183,1807446,1809135,1931316,1933214,1933216,2009425,2013080,2051000,2246516,2751779,2765042,2916090,2933423,2940736,2972692,3000775,3000777,3008347,3014155,3016570,3016572,3030677,3035885,3035936,3048631,3065882,3091338,3100794,3111515,3120600,3126546,3165274,3205794,3225243,3328388,3383385,3389051,3450879,3538120,3538131,3541991,3542087,3542134,3543241,3543259,3543403,3561554,3732115,3766807,3904245,3938645,3938684,4029512,4065080,4132797,4132805,4132815,4132843,4133032,4133052,4134552,4135770,4154984,4154999,";
var jsCustomRenderHandlerPairs = "";
var jsStandardRenderHandlers = "";
var jsSiteSecurityCreateControls = 0;
var jsShowRejectForApprover = 1;
// -->
</script><script type="text/javascript" src="/commonspot/javascript/browser-all.js"></script>
<!--[if lt IE 9]>
<script src="/commonspot/javascript/lib/html5shiv/html5shiv.js"></script>
<![endif]--> <link rel="canonical" href="http://www.american.edu/oit/policies/Web-Copyright.cfm">
<!-- Server: WWWPROD902 -->
<meta name="verify-v1" content="C8BKbPWIV/0vmitgtXynwUnk3XHxNzpqpXOLS4oKc+c="/>
<link rel="stylesheet" href="/style/body.css" type="text/css" />
<link rel="stylesheet" href="/style/nosifr.css">
<link rel="stylesheet" href="/customcf/emergency/css/emergency.css">
<link rel="stylesheet" href="/style/reskin.css" type="text/css">
<script src="/templates/js/prototype-1.7.2.0.js" type="text/javascript"></script>
<script src="/templates/js/src/scriptaculous9f06.js?load=effects" type="text/javascript"></script>
<script src="/templates/js/src/effects.js" type="text/javascript"></script>
<script src="/templates/js/accordion.js" type="text/javascript"></script>
<script src="/templates/js/reskin-AU.js?v=20150603" type="text/javascript"></script>
<script src="/templates/js/carousel.js?d=20150129" type="text/javascript"></script>
<script src="/ADF/thirdParty/jquery/jquery-1.7.js" type="text/javascript"></script>
<script src="/templates/js/reskin.js" type="text/javascript"></script>
<!--[if lt IE 10 ]>
<link rel="stylesheet" type="text/css" href="/style/reskin-ie.css">
<script type="text/javascript">
window.onload = function() {
var sbox = document.forms["search"]["q"];
sbox.value = "Search AU";
sbox.onblur = function() { if(sbox.value=='') sbox.value = 'Search AU'; };
sbox.onfocus = function() { if(sbox.value=='Search AU') sbox.value = ''; };
}
</script>
<![endif]-->
<!--[if lt IE 9]>
<link rel="stylesheet" type="text/css" href="/style/reskin-ie8.css">
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
jQuery("nav.home-nav > ul > li:last-child").css({"border-right":0,"width":"166px"});
jQuery("nav.home-nav > ul > li:last-child > ul.sub-nav").css("width","165px");
});
</script>
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" type="text/css" href="/style/reskin-ie7.css">
<script type="text/javascript">
jQuery(document).ready(function() {
jQuery("nav.site-nav ul.main > li").hover(function() {
jQuery(this).find("span.bullet").css("background", "#2588c6");
}, function() {
jQuery(this).find("span.bullet").css("background", "#ccc");
});
if(jQuery("nav.site-nav ul.site").length > 0) {
jQuery("nav.site-nav ul.site li").each(function() { jQuery(this).replaceWith("<td>" + jQuery(this).html() + "</td>") });
jQuery("nav.site-nav ul.site").replaceWith("<tr>" + jQuery("nav.site-nav ul.site").html() + "</tr>");
jQuery("nav.site-nav > div").replaceWith('<table border="0" cellpadding="0" cellspacing="0">' + jQuery("nav.site-nav > div").html() + "</table>");
var snav = jQuery("nav.site-nav");
var lis = jQuery("nav.site-nav td");
var avgw = parseInt(snav.find("tr").outerWidth() / lis.length * 1.1);
var els = 0;
lis.each(function(){if (jQuery(this).outerWidth() < avgw) els++; else jQuery(this).css("width", jQuery(this).outerWidth() + 1);});
var extrapx = parseInt((snav.outerWidth() - snav.find("tr").outerWidth() - (lis.length - els)) / els);
lis.each(function(){ if (jQuery(this).outerWidth() < avgw) jQuery(this).css("width", jQuery(this).outerWidth() + extrapx); });
extrapx = snav.outerWidth() - snav.find("tr").outerWidth();
if (extrapx > 0) lis.last().css("width", lis.last().outerWidth() + extrapx);
}
setTimeout(function(){jQuery("header").css("overflow","hidden");},1200);
});
</script>
<![endif]-->
<script type="text/javascript">
if(typeof scriptsLoaded === 'undefined'){
var scriptsLoaded= new Array();
}
jQuery.noConflict();
scriptsLoaded["jQuery"] = true;
</script>
<script type="text/javascript">
function gup( name )
{
name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
var regexS = "[\\?&]"+name+"=([^&#]*)";
var regex = new RegExp( regexS );
var results = regex.exec( window.location.href );
if( results == null )
return "";
else
return results[1];
}
var urlTracker = gup('tracker');
var utmSource = gup('utm_source');
var utmName = gup('utm_name');
var utmMedium = gup('utm_medium');
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-10527037-1']);
_gaq.push(['_trackPageview']);
if(utmSource != '')
{
_gaq.push(['_setCampMediumKey', utmMedium ]);	// Medium
_gaq.push(['_setCampNameKey', utmName ]);		// Name
_gaq.push(['_setCampSourceKey', utmSource ]);    // source
}
else if(urlTracker != '' )
{
_gaq.push(['_setCampMediumKey', 'redirect']);
_gaq.push(['_setCampNameKey', 'Redirects']);
_gaq.push(['_setCampSourceKey', urlTracker]);    // source
}
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
<script type="text/javascript" src="/customcf/scripts/utils.js"></script>
<script type="text/javascript" src="/ADF/thirdParty/jquery/jquery-1.7.js"></script>
<script type="text/javascript">
jQuery.noConflict();
</script>
<link rel="stylesheet" href="/customcf/scripts/jquery/thickbox/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/customcf/scripts/jquery/thickbox/thickbox.js"></script>
<link rel="stylesheet" href="/commonspot/commonspot.css" type="text/css" id="cs_maincss" /> </head><body lang="en" class="CS_Document"><a name="__topdoc__"></a><script src="/commonspot/pagemode/always-include-common.js" type="text/javascript"></script><script src="/commonspot/pagemode/always-include-ns.js" type="text/javascript"></script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-M7XX5D" height="0" width="0" style="display:none;visibility:hidden" title="No content. Google Tag Manager Includes." aria-hidden="true">Warning: iframes not supported.</iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-M7XX5D');</script>
<!-- End Google Tag Manager -->
<!-- // Server: wwwprod902.american.edu -->
<script type="text/javascript">
jQuery("document").ready(function(){
jQuery.ajax({
url: '/customcf/emergency/actives.txt',
cache: false,
dataType: "json",
success: function(response) {
jQuery.each(response.DATA, function(index) {
if (this[9]) {
var returnHTML = '<p><span class="interiorMsg">' + this[1] + '</span><span class="interiorMsg2">' + this[2];
if (this[6]) {
returnHTML += ' <a href="' + this[7] + '" class="emergency-link">Learn More</a>';
};
returnHTML += '</span>';
jQuery('#emergency-top').append(returnHTML).parent().show();
};
});
}
});
});
</script>
<div id="emergency-wrapper" style="display:none;">
<div id="emergency-top">
</div>
</div>
<div class="reskin">
<div id="wrapper-reskin">
<header>
<div class="sideshades">
<div class="etched-top"></div>
<div id="logo-reskin">
<a href="/" class="crest"><img src="/images/AU-crest.png" alt="American University"><span class="sr-only"> Homepage</span></a>
<a href="/"><img src="/images/AU-logo.png" alt="School Logo: AU"><span class="sr-only"> Homepage</span></a>
<div id="utilities-reskin">
<div id="login-reskin">
<a href="#">Log in<span></span></a>
</div>
<div id="login-detail">
<span class="arrow"></span>
<ul>
<li><a href="https://myau.american.edu/">myau.american.edu</a></li>
<li><a href="http://mail.google.com/a/student.american.edu">Student Gmail</a></li>
<li><a href="https://blackboard.american.edu/">Blackboard</a></li>
</ul>
</div>
<form name="search" id="search-reskin" method="GET" action="/search.cfm">
<div id="search-box-reskin">
<input name="site" value="AU_website" type="hidden">
<input name="entqr" value="0" type="hidden">
<input name="ud" value="1" type="hidden">
<input name="sort" value="date:D:L:d1" type="hidden">
<input name="output" value="xml_no_dtd" type="hidden">
<input name="client" value="default_frontend" type="hidden">
<input name="proxystylesheet" value="default_frontend" type="hidden">
<input name="oe" value="UTF-8" type="hidden">
<input name="ie" value="UTF-8" type="hidden">
<label for="searchQ" class="sr-only">Enter Keywords</label>
<input id="searchQ" class="search" type="text" name="q" placeholder="Search AU">
<button id="submit-search" aria-label="Search" name="btnG"><span></span><span class="sr-only">Search</span></button>
</div>
<div id="search-detail">
<span class="arrow"></span>
<ul>
<li><input name="context" value="/search.cfm" type="radio" checked="checked" onclick="document.getElementById('search-reskin').action = this.value"> AU Web</li>
<li><input name="context" value="/directory" type="radio" onclick="document.getElementById('search-reskin').action = this.value"> Faculty/Staff Directory</li>
</ul>
<h2><span>Trending</span></h2>
<ul>
<li><a href="/search.cfm?site=AU_website&entqr=0&ud=1&sort=date%3AD%3AL%3Ad1&output=xml_no_dtd&client=default_frontend&proxystylesheet=default_frontend&oe=UTF-8&ie=UTF-8&q=New Student Info&btnG=">New Student Info</a></li>
<li><a href="/search.cfm?site=AU_website&entqr=0&ud=1&sort=date%3AD%3AL%3Ad1&output=xml_no_dtd&client=default_frontend&proxystylesheet=default_frontend&oe=UTF-8&ie=UTF-8&q=Academic Calendar&btnG=">Academic Calendar</a></li>
<li><a href="/search.cfm?site=AU_website&entqr=0&ud=1&sort=date%3AD%3AL%3Ad1&output=xml_no_dtd&client=default_frontend&proxystylesheet=default_frontend&oe=UTF-8&ie=UTF-8&q=Career Center&btnG=">Career Center</a></li>
<li><a href="/search.cfm?site=AU_website&entqr=0&ud=1&sort=date%3AD%3AL%3Ad1&output=xml_no_dtd&client=default_frontend&proxystylesheet=default_frontend&oe=UTF-8&ie=UTF-8&q=Library&btnG=">Library</a></li>
</ul>
</div>
</form>
</div>
</div>
</div>
</header>
<nav class="home-nav">
<ul>
<li id="admissions-reskin">
<a href="#" onclick="return false;">Admissions<span class="separator"></span></a>
<ul class="sub-nav">
<li><a href="/admissions/index.cfm" title="Overview of Admissions">Overview</a></li>
<li><a href="/admissions/apply/index.cfm">Apply</a></li>
<li><a href="/degrees.cfm">Degrees</a></li>
<li><a href="/admissions/freshman/index.cfm">Undergraduate</a></li>
<li><a href="/programs/graduate/index.cfm">Graduate</a></li>
<li><a href="/finance/studentaccounts/Tuition-and-Fees-Information.cfm">Tuition & Fees</a></li>
<li><a href="/financialaid/index.cfm">Financial Aid</a></li>
</ul>
</li>
<li id="academics-reskin">
<a href="#" onclick="return false;">Academics<span class="separator"></span></a>
<ul class="sub-nav">
<li><a href="/academics/index.cfm" title="Overview of Academics">Overview</a></li>
<li class="has-child"><a href="/about/schoolsandcolleges.cfm" onclick="if(!isMobile)return false;">Schools & Colleges</a>
<ul class="sub-nav-child">
<li><a href="/about/schoolsandcolleges.cfm" title="Overview of American University Schools &amp; Colleges">Overview</a></li>
<li><a href="/cas/">College of Arts and Sciences</a></li>
<li><a href="/kogod/">Kogod School of Business</a></li>
<li><a href="/soc/">School of Communication</a></li>
<li><a href="/sis/">School of International Service</a></li>
<li><a href="/spexs/">School of Professional &amp; Extended Studies</a></li>
<li><a href="/spa/">School of Public Affairs</a></li>
<li><a href="http://www.wcl.american.edu/">Washington College of Law</a></li>
</ul>
</li>
<li><a href="/degrees.cfm">Degrees</a></li>
<li><a href="/provost/registrar/universitycatalog.cfm">Catalog</a></li>
<li><a href="/provost/registrar/registration/registrationspring2016.cfm">Registration</a></li>
<li><a href="/library/index.cfm" title="Academics: Library">Library</a></li>
<li><a href="/universitylife/calendars.cfm">Calendars</a></li>
<li><a href="http://auabroad.american.edu">Study Abroad</a></li>
<li><a href="/summer/index.cfm">Summer Programs</a></li>
<li><a href="/onlinelearning/index.cfm">Online Programs</a></li>
<li><a href="/learning-communities/index.cfm">Honors &amp; <br>Learning Communities </a></li>
</ul>
</li>
<li id="research-reskin">
<a href="#" onclick="return false;">Research<span class="separator"></span></a>
<ul class="sub-nav">
<li><a href="/research/index.cfm" title="Overview of Research">Overview</a></li>
<li><a href="/research/awards.cfm">Awards</a></li>
<li><a href="/research/centers-institutes.cfm">Centers & Institutes</a></li>
<li><a href="/research/upcoming-events.cfm">Events</a></li>
<li><a href="/library/index.cfm" title="Research: Library">Library</a></li>
<li><a href="/research/funding.cfm">Funding Opportunities</a></li>
<li><a href="/provost/osp/index.cfm">Sponsored Programs</a></li>
</ul>
</li>
<li id="university life-reskin">
<a href="#" onclick="return false;">University Life<span class="separator"></span></a>
<ul class="sub-nav">
<li><a href="/universitylife/index.cfm" title="Overview of University Life">Overview</a></li>
<li><a href="http://www.aueagles.com">Athletics</a></li>
<li><a href="/ocl/housing/index.cfm">Housing</a></li>
<li><a href="/ocl/dining/index.cfm">Dining & Retail</a></li>
<li><a href="/ocl/orientation/index.cfm">New Students</a></li>
<li><a href="/universitylife/career.cfm">Career Services</a></li>
<li><a href="/ocl/activities/index.cfm">Student Organizations</a></li>
<li><a href="/universitylife/calendars.cfm">Calendars</a></li>
<li><a href="/cas/auarts/index.cfm">AU Arts</a></li>
<li><a href="http://www.wamu.org">WAMU 88.5</a></li>
</ul>
</li>
<li id="alumni-reskin">
<a href="#" onclick="return false;">Alumni<span class="separator"></span></a>
<ul class="sub-nav">
<li><a href="/alumni/index-old.cfm" title="Overview of Alumni">Overview</a></li>
<li><a href="http://alumniassociation.american.edu/s/1395/event.aspx?sid=1395&gid=1&pgid=338">Events</a></li>
<li><a href="http://alumniassociation.american.edu/s/1395/start.aspx?sid=1395&gid=1&pgid=339">Volunteer</a></li>
<li><a href="/alumni/benefits/Index.cfm">Benefits</a></li>
<li><a href="https://securelb.imodules.com/s/1395/index1colnonav.aspx?sid=1395&gid=1&pgid=5701&cid=9061">Give to AU</a></li>
<li><a href="/careercenter/alumni/index.cfm">Alumni Career Services</a></li>
<li><a href="/library/services/alumni.cfm">Alumni Library Privileges</a></li>
<li><a href="https://securelb.imodules.com/s/1395/index1col.aspx?sid=1395&gid=1&pgid=3&cid=40">Community Login</a></li>
</ul>
</li>
<li id="about-reskin">
<a href="#" onclick="return false;">About<span class="separator"></span></a>
<ul class="sub-nav">
<li><a href="/about/index.cfm" title="Overview of About">Overview</a></li>
<li><a href="/about/fast-facts.cfm">Facts</a></li>
<li class="has-child"><a href="/about/schoolsandcolleges.cfm" onclick="if(!isMobile)return false;">Schools & Colleges</a>
<ul class="sub-nav-child">
<li><a href="/about/schoolsandcolleges.cfm" title="Overview of American University Schools &amp; Colleges">Overview</a></li>
<li><a href="/cas/">College of Arts and Sciences</a></li>
<li><a href="/kogod/">Kogod School of Business</a></li>
<li><a href="/soc/">School of Communication</a></li>
<li><a href="/sis/">School of International Service</a></li>
<li><a href="/spexs/">School of Professional &amp; Extended Studies</a></li>
<li><a href="/spa/">School of Public Affairs</a></li>
<li><a href="http://www.wcl.american.edu/">Washington College of Law</a></li>
</ul>
</li>
<li><a href="/about/leadership.cfm">Leadership</a></li>
<li><a href="http://www.american.edu/finance/">Financial Statements</a></li>
<li><a href="/hr/index.cfm">Work at AU</a></li>
<li><a href="/directory/index.cfm">Faculty & Staff Directory</a></li>
<li><a href="/offices.cfm">Office & Service Directory</a></li>
</ul>
</li>
</ul>
</nav>
</div>
</div>
<div class="clear"></div>
<div class="page-title-holder">
<div id="cs_control_27039" class="cs_control CS_Element_Custom"></div><div id="cs_control_1364" class="cs_control CS_Element_Custom">
<div class="page-title page-title-promo">
<h1 class="sifr-head">Office of Information Technology</h1>
<div id="cs_control_174601" class="cs_control CS_Element_Custom"></div>
<div class="clear"></div>
</div>
</div>
</div>
<div id="main-container">
<div id="main">
<div id="cs_control_1351" class="CS_Element_Schedule"><div  title="" id="CS_Element_maincontainer"><a name="customcf2339" id="customcf2339"></a><a name="CP_JUMP_2339" id="CP_JUMP_2339"></a><div id="cs_control_2339" class="cs_control CS_Element_CustomCF"><div id="CS_CCF_13073_2339">
<div class="grid-e">
<div class="column first">
<div id="cs_control_1441" class="CS_Element_Schedule"><div  title="" id="CS_Element_13073_1441"><a name="customcf4800" id="customcf4800"></a><a name="CP_JUMP_4800" id="CP_JUMP_4800"></a><div id="cs_control_4800" class="cs_control CS_Element_CustomCF"><div id="CS_CCF_13073_4800">
<script src="/templates/js/navigation.js" type="text/javascript"></script>
<ul id="nav-accordion-holder" class="sideAccordion content-navigation">
<!-- Building Nav 1: PageID 758685,UniqueID 758689, ElementInstanceType 756671 -->
<li><a href="/oit/index.cfm" class="red-arrow red-active"  >OIT Home</a></li>
<!-- Building Nav 2: PageID 758685,UniqueID 758690, ElementInstanceType 756671 -->
<li class="accordion_toggle top" id="accordion_first"> <a href="#">Get Connected</a></li>
<li class="accordion_content">
<ul>
<li><a href="/oit/accounts/accounts.cfm">AU Accounts</a></li>
<li><a href="/oit/email/Email.cfm">E-mail</a></li>
<li><a href="/oit/faq/FAQ.cfm">FAQs</a></li>
<li><a href="/oit/Need-Help-Now.cfm">Get Help Now</a></li>
<li><a href="/oit/software/AU-Supported-Software.cfm">Software</a></li>
<li><a href="/oit/network/Network.cfm">Wired & Wireless</a></li>
</ul>
</li>
<!-- Building Nav 3: PageID 758685,UniqueID 758692, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Technology Services</a></li>
<li class="accordion_content">
<ul>
<li><a href="/library/technology/av/index.cfm">Audio Visual</a></li>
<li><a href="/technology/services/Campus-Computing-Facilities.cfm">Campus Computing Facilities</a></li>
<li><a href="https://blogs.library.american.edu/printing/" target="_blank">EagleBucks Printing</a></li>
<li><a href="/oit/hardware/index.cfm">Hardware Support</a></li>
<li><a href="/oit/hardware/IT-Asset-Management.cfm">IT Asset Management</a></li>
<li><a href="https://myau.american.edu" target="_blank">myAU.american.edu</a></li>
<li><a href="http://status.american.edu" target="_blank">System Status</a></li>
<li><a href="/oit/network/Telephony-and-Television.cfm">Telephony & Television</a></li>
</ul>
</li>
<!-- Building Nav 4: PageID 758685,UniqueID 758699, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Projects</a></li>
<li class="accordion_content">
<ul>
<li><a href="/oit/projects/index.cfm">Project Overview</a></li>
<li><a href="/oit/projects/prioritization.cfm">Project Prioritization</a></li>
</ul>
</li>
<!-- Building Nav 5: PageID 758685,UniqueID 758701, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">IT Security</a></li>
<li class="accordion_content">
<ul>
<li><a href="/oit/security/index.cfm">Security Overview</a></li>
<li><a href="/oit/security/Compliance.cfm">Compliance</a></li>
<li><a href="/oit/security/Incident-Response.cfm">Incident Response</a></li>
<li><a href="/oit/security/Patch-Management.cfm">Patch Management</a></li>
<li><a href="/oit/security/Protecting-Data.cfm">Protecting Sensitive Data</a></li>
<li><a href="/oit/security/Security-Tips.cfm">Security Solutions & Tips</a></li>
<li><a href="/oit/security/Social-Engineering.cfm">Social Engineering</a></li>
</ul>
</li>
<!-- Building Nav 6: PageID 758685,UniqueID 758704, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Training & Awareness</a></li>
<li class="accordion_content">
<ul>
<li><a href="/training/index.cfm">AU Training Opportunities</a></li>
<li><a href="/training/tech.cfm">Technical Training</a></li>
</ul>
</li>
<!-- Building Nav 7: PageID 758685,UniqueID 758712, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Policies</a></li>
<li class="accordion_content">
<ul>
<li><a href="loader.cfm?csModule=security/getfile&pageid=2609353">Computer Use & Copyright Policy</a></li>
<li><a href="loader.cfm?csModule=security/getfile&pageid=2044108">Copyright Ownership Policy</a></li>
<li><a href="loader.cfm?csModule=security/getfile&pageid=2010701">Data Breach Notification Policy</a></li>
<li><a href="/oit/policies/AU-Data-Classification-Policy.cfm">Data Classification Policy</a></li>
<li><a href="loader.cfm?csModule=security/getfile&pageid=2010763">Electronic Mass Communication Policy</a></li>
<li><a href="loader.cfm?csModule=security/getfile&pageid=2676090">Identity Theft Prevention Policy</a></li>
<li><a href="loader.cfm?csModule=security/getfile&pageid=2452111">Information Technology Security Policy</a></li>
<li><a href="/oit/policies/Records-Retention.cfm">Records Retention Policy</a></li>
<li><a href="loader.cfm?csModule=security/getfile&pageid=2912781">Responsible Use of University Web Site & Content Management System</a></li>
<li><a href="/oit/policies/Web-Copyright.cfm">Web Copyright & Privacy Statement</a></li>
</ul>
</li>
<!-- Building Nav 8: PageID 758685,UniqueID 758717, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Publications</a></li>
<li class="accordion_content">
<ul>
<li><a href="/oit/publications/OIT-Year-in-Review-2014-2015.cfm">OIT Annual Report</a></li>
<li><a href="http://w.american.edu/oit/EagleWire15-Fac_Staff.pdf" target="_blank">OIT Faculty & Staff Newsletter</a></li>
<li><a href="http://w.american.edu/oit/EagleWire2015.pdf" target="_blank">OIT Student Newsletter</a></li>
<li><a href="http://w.american.edu/oit/OITRoadmap-FINAL.pdf" target="_blank">OIT Roadmap</a></li>
</ul>
</li>
<!-- Building Nav 9: PageID 758685,UniqueID 758734, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">About OIT</a></li>
<li class="accordion_content">
<ul>
<li><a href="/directory/index.cfm">AU Faculty & Staff Directory</a></li>
<li><a href="/oit/about/staff.cfm">Meet the Staff</a></li>
<li><a href="/oit/about/cio.cfm">Office of the Vice President & CIO</a></li>
</ul>
</li>
<!-- Building Nav 10: PageID 758685,UniqueID 758738, ElementInstanceType 756671 -->
<li class="questions dotted-bottom"><a href="#">Information Specifically For</a></li>
<!-- Building Nav 11: PageID 758685,UniqueID 758739, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Alumni</a></li>
<li class="accordion_content">
<ul>
<li><a href="http://alumniassociation.american.edu" target="_blank">AU's Online Community</a></li>
<li><a href="/provost/registrar/academicservices/transcriptsinfo.cfm">Diplomas & Transcripts</a></li>
<li><a href="http://www.american.edu/oit/email/Gmail-FAQ.cfm#keepmyaddress" target="_blank">E-mail Services for New Alumni</a></li>
<li><a href="http://www.library.american.edu/about/services/alumni.html" target="_blank">Library Services</a></li>
</ul>
</li>
<!-- Building Nav 12: PageID 758685,UniqueID 758745, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Faculty</a></li>
<li class="accordion_content">
<ul>
<li><a href="/oit/network/VPN.cfm">Connecting to AU's VPN Service</a></li>
<li><a href="/oit/network/Eaglesecure-Wireless.cfm">Connecting to AU's Wireless Network</a></li>
<li><a href="/oit/accounts/Faculty-Staff-Accounts.cfm">Creating Accounts</a></li>
<li><a href="/oit/software/Microsoft-Office.cfm">Download Microsoft Office</a></li>
<li><a href="/oit/email/Faculty-Staff-Services.cfm">E-mail Services</a></li>
<li><a href="/oit/accounts/Password.cfm">Managing Your Passwords</a></li>
<li><a href="/oit/security/Protecting-Data.cfm">Protecting Sensitive Data</a></li>
<li><a href="/oit/accounts/Account-Request.cfm">Request Specialized Account</a></li>
<li><a href="/oit/network/Campus-Telephone.cfm">Using Campus Telephone Services</a></li>
<li><a href="/oit/faq/Remote-Work-FAQ.cfm">Working Remotely</a></li>
</ul>
</li>
<!-- Building Nav 13: PageID 758685,UniqueID 758772, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Parents</a></li>
<li class="accordion_content">
<ul>
<li><a href="/technology/newstudents/index.cfm">Essential IT Info for New Students</a></li>
<li><a href="http://www.american.edu/oit/software/Portal-FAQ.cfm#parents" target="_blank">Granting Parent Portal Access</a></li>
<li><a href="/oit/hardware/Recommendations.cfm">Recommendations for Buying a Computer</a></li>
</ul>
</li>
<!-- Building Nav 14: PageID 758685,UniqueID 758774, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Staff</a></li>
<li class="accordion_content">
<ul>
<li><a href="/oit/network/VPN.cfm">Connecting to AU's VPN Service</a></li>
<li><a href="/oit/network/Eaglesecure-Wireless.cfm">Connecting to AU's Wireless Network</a></li>
<li><a href="/oit/accounts/Faculty-Staff-Accounts.cfm">Creating Accounts</a></li>
<li><a href="/oit/software/Microsoft-Office.cfm">Download Microsoft Office</a></li>
<li><a href="/oit/email/Faculty-Staff-Services.cfm">E-mail Services</a></li>
<li><a href="/oit/accounts/Password.cfm">Managing Your Passwords</a></li>
<li><a href="/oit/security/Protecting-Data.cfm">Protecting Sensitive Data</a></li>
<li><a href="/oit/accounts/Account-Request.cfm">Request Specialized Account</a></li>
<li><a href="/oit/network/Campus-Telephone.cfm">Using Campus Telephone Services</a></li>
<li><a href="/oit/faq/Remote-Work-FAQ.cfm">Working Remotely</a></li>
</ul>
</li>
<!-- Building Nav 15: PageID 758685,UniqueID 758788, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Students</a></li>
<li class="accordion_content">
<ul>
<li><a href="/oit/network/Cable-TV.cfm">Cable Television</a></li>
<li><a href="/oit/network/Eaglesecure-Wireless.cfm">Connecting to AU's Wireless Network</a></li>
<li><a href="/oit/accounts/Student-Accounts.cfm">Creating Accounts</a></li>
<li><a href="/oit/email/Student-Services.cfm">E-mail Services</a></li>
<li><a href="/technology/newstudents/index.cfm">Essential IT Info for New Students</a></li>
<li><a href="http://www.american.edu/oit/software/Portal-FAQ.cfm#parents" target="_blank">Granting Parent Portal Access</a></li>
<li><a href="/oit/accounts/Password.cfm">Managing Your Passwords</a></li>
<li><a href="/oit/network/Network-FAQ.cfm">Network FAQ</a></li>
<li><a href="/oit/email/Address.cfm">Personalize Your E-mail Address</a></li>
<li><a href="/oit/software/Portal-FAQ.cfm">Portal FAQ</a></li>
</ul>
</li>
<!-- Building Nav 16: PageID 758685,UniqueID 758843, ElementInstanceType 756671 -->
<li class="accordion_toggle top"> <a href="#">Visitors</a></li>
<li class="accordion_content">
<ul>
<li><a href="/oit/accounts/Visitor-Accounts.cfm">Creating Visitor Accounts</a></li>
<li><a href="/oit/network/Eaglesecure-Wireless.cfm">Connecting to AU's Wireless Network</a></li>
<li><a href="/oit/accounts/Password.cfm">Managing Your Passwords</a></li>
</ul>
</li>
</ul>
<script type="text/javascript">
var hasAccord = $('nav-accordion-holder');  // get all links
if (hasAccord != null) {
aT = $$('#nav-accordion-holder .accordion_toggle');
var mainAccordion = new accordion('nav-accordion-holder');
findElement();
}
</script>
</div></div><a name="container2411" id="container2411"></a><a name="CP_JUMP_2411" id="CP_JUMP_2411"></a><div id="cs_control_2411" class="CS_Element_Schedule"><div  title="" id="CS_Element_container2411"></div></div><a name="custom184331" id="custom184331"></a><a name="CP_JUMP_184331" id="CP_JUMP_184331"></a><div id="cs_control_184331" class="cs_control CS_Element_Custom"></div></div></div>
</div>
<div class="column">
<div id="cs_control_1442" class="CS_Element_Schedule"><div  title="" id="CS_Element_13073_1442"><a name="customcf2397" id="customcf2397"></a><a name="CP_JUMP_2397" id="CP_JUMP_2397"></a><div id="cs_control_2397" class="cs_control CS_Element_CustomCF"><div id="CS_CCF_13073_2397">
<div class="content-container content-container-margin">
<div class="content-layout-6-column">
<div id="cs_control_2559" class="CS_Element_Schedule"><div  title="" id="CS_Element_13073_2559"><a name="custom2721" id="custom2721"></a><a name="CP_JUMP_2721" id="CP_JUMP_2721"></a><div id="cs_control_2721" class="cs_control CS_Element_Custom">
<div class="content-container content-container-margin">
<div class="article-content">
<h1>AU Web Copyright and Privacy Policy Statement</h1>
<div class="article-copy">
<br />
<h3>Copyright Statement</h3>
<p align="justify">All materials contained in this site are protected by United States copyright law. The right to download and store or output the materials on this site is granted for the user's personal, noncommercial use only, and materials may not be reproduced in any edited form. Any other reproduction, transmission, performance, display, or editing of these materials by any means mechanical or electronic without express written permission of American University is strictly prohibited. Users wishing to obtain permission to reprint or reproduce any materials appearing on this site may contact us at <a href="mailto:helpdesk@american.edu">helpdesk@american.edu</a>.</p>
<p>© 2009 American University All rights reserved. All trademarks mentioned herein belong to their respective owners.</p>
<p> </p>
<h3>Nondiscrimination Notice</h3>
<p align="justify">American University does not discriminate on the basis of race, color, religion, national origin, sex, age, marital status, personal appearance, sexual orientation, gender identity and expression, family responsibilities, political affiliation, disability, source of income, place of residence or business, or certain veteran status in its programs and activities. For information, contact the IT Help Desk at 202-885-2550 or at American University, 4400 Massachusetts Avenue, N.W., Washington, D.C. 20016.</p>
<p> </p>
<h3>Privacy Policy Statement</h3>
<p align="justify">American University is fully committed to providing an informative, useful, and entertaining website for its visitors. The university enjoys interacting with its audience and helping to provide it with pertinent information. Visitors to any American University webpage have an opportunity to contact us via e-mail. </p>
<p align="justify">The policy of American University's officially controlled and operated WWW sites is to respect and protect the privacy of its students, faculty, staff, and all other users. American University does not sell or distribute any information obtained from our visitors to a third party, under any circumstance. </p>
<p align="justify">This policy defines how American University collects information from you and how it is used. For each visitor to American University's website, the following information is collected:<br />
</p>
<blockquote>
<ul>
<li>internet addressing information of visitors to our Web pages (IP Addresses) </li>
<li>the e-mail addresses of those who communicate with us via e-mail </li>
<li>the aggregate information on what pages visitors access or visit </li>
<li>user-specific information on what pages visitors access or visit </li>
<li>information volunteered by the visitor, such as survey information </li>
<li>personal information we request for information inquiries/requests, etc.</li>
</ul>
</blockquote>
<p align="justify"><br />
There are cases where American University websites may request/or provide personal information such as userID, password, name, or address. To provide for your privacy in these instances, American University provides a secure server which transmits and receives scrambled data, which is decoded on the server side. This technology protects your information from being viewed by an outside user. </p>
<p align="justify">The collected information is used for internal university purposes and may be used to improve the content of American University's overall website. It will not be shared with other organizations to help them contact visitors. However, American University reserves the right to use the information to contact visitors for marketing purposes. </p>
<p align="justify">A small number of American University pages contain links which may take you outside of our website. Users should be aware that when you are on, for example, the American University Athletics website, and you "click" on a banner advertisement, you will be directed to another website which is beyond American University's control. These other sites may send and receive cookies, collect data, or solicit personal information and may use this information in ways American University does not in accordance with its privacy policy statement. Always be aware of whose site you are visiting. </p>
<p align="justify">As a Web user, keep in mind that whenever you give out personal information online (i.e. via message boards or chat) information can be collected and used by people you don't even know. </p>
<p align="justify">While American University strives to protect its users' personal information and privacy, it cannot guarantee the security of any information you disclose online and you do so at your own risk. American University's policy does not extend to anything that is inherent in the operation of the Internet, and therefore beyond our control, and is not to be applied in any manner contrary to applicable law or governmental regulation. </p>
<p align="justify">American University reserves the right at its discretion to make changes to this policy at any time. Feel free to check this page periodically for updates. </p>
<p>If you do not want to be contacted by us in the future, please let us know.</p>
<p>Our postal address is :</p>
<dl>
<dd>American University<br />
4400 Massachusetts Avenue, NW<br />
</dd>
<dd>WWW Operations </dd>
<dd>Office of Information Technology 
<dl>
<dt>Washington, D.C. 20016</dt>
</dl>
</dd>
</dl>
<p>We can be reached via e-mail at: <a href="mailto:helpdesk@american.edu">helpdesk@american.edu</a></p>
</div>
</div>
</div>
<div class="clear"><!-- --></div>
</div></div></div>
</div>
<div class="content-layout-3-column">
<div id="cs_control_2560" class="CS_Element_Schedule"><div  title="" id="CS_Element_13073_2560"><a name="custom4383" id="custom4383"></a><a name="CP_JUMP_4383" id="CP_JUMP_4383"></a><div id="cs_control_4383" class="cs_control CS_Element_Custom"></div><a name="custom9263" id="custom9263"></a><a name="CP_JUMP_9263" id="CP_JUMP_9263"></a><div id="cs_control_9263" class="cs_control CS_Element_Custom">
<div class="content-module">
<h3>Related Links</h3>
<!-- do we need to build a grid here? -->
<div id="cs_control_12099" class="cs_control CS_Element_LinkBar">
<ul>
<li class="nolines"><a id="CP___PAGEID=270591,AU-Data-Classification-Policy.cfm,337" href="/oit/policies/AU-Data-Classification-Policy.cfm">AU Data Classification Policy</a></li>
<li class="nolines"><a id="CP___PAGEID=17048,AU-Info-Security-Plan.cfm,337" href="/oit/policies/AU-Info-Security-Plan.cfm">AU Information Security Plan</a></li>
<li class="nolines"><a id="CP___PAGEID=13132,Records-Retention.cfm,337" href="/oit/policies/Records-Retention.cfm">AU Records Retention</a></li>
<li class="nolines"><a id="CP___PAGEID=13102,Computer-Use.cfm,337" href="/oit/policies/Computer-Use.cfm">Computer Use and Copyright Policy</a></li>
<li class="nolines"><a id="CP___PAGEID=13325,Security-Policies.cfm,337" href="/oit/policies/Security-Policies.cfm">IT Security Policies</a></li>
</ul>
</div>
</div>
</div></div></div>
</div>
<div class="clear"></div>
</div>
</div></div></div></div>
</div>
</div>
</div></div></div></div>
</div>
</div>
<div class="clear"></div>
<!--begin IMODULES scrape footer -->
<div class="reskin">
<footer>
<div id="social-media-bar">
<span>Connect With AU</span>
<ul>
<li><a href="http://www.facebook.com/AmericanUniversity"><span class="icon-facebook social-icon"></span>Facebook</a></li>
<li><a href="http://www.twitter.com/AmericanU"><span class="icon-twitter social-icon"></span>Twitter</a></li>
<li><a href="http://www.youtube.com/americanuniversity"><span class="icon-youtube social-icon"></span>YouTube</a></li>
<li><a href="https://www.linkedin.com/edu/american-university-18058"><span class="icon-linkedin social-icon"></span>LinkedIn</a></li>
<li><a href="http://instagram.com/americanuniversity"><span class="icon-instagram social-icon"></span>Instagram</a></li>
<li><a href="http://blogs.american.edu/"><span class="icon-wordpress social-icon"></span>AU Blogs</a></li>
<li><a href="/socialmedia/"><span class="icon-ausocial social-icon"></span>AU Social Media Directory</a></li>
</ul>
</div>
<div class="footer-top-reskin">
<div class="left">
<p>
<a href="/" class="home-link-reskin"><span class="icon-home"></span>Home</a>
</p>
</div>
<div class="right">
<p>
<a href="/emergency/">Emergency Preparedness</a> |
<a href="/hr/">Work at AU</a> |
<a href="/media/">Media Relations</a> |
<a href="/m/">AU Mobile</a> |
<a href="/website-feedback.cfm">Website Feedback</a>
</p>
</div>
</div>
<div class="clearfix"></div>
<div class="footer-bottom-reskin">
<div class="left">
<div>4400 Massachusetts Avenue, NW, Washington, DC 20016</div>
<div>(202) 885-1000 | <a href="/contact-au.cfm">Contact Us</a> | <a href="/aumaps/index.cfm">Maps</a></div>
</div>
<div class="right">
Copyright © 2016 American University. <a href="/oit/policies/Web-Copyright.cfm">Privacy</a> |
<a href="/policies/disclosure.cfm">Disclosure</a> |
<a href="/eeo">EEO</a>
</div>
</div>
</footer>
</div>
<!--end IMODULES scrape footer -->
<script type="text/javascript">
// pop-up login if asked
jQuery(document).ready(function($){
if( document.location.href.indexOf("autoLogin=1") > 0 )
{
$("#autoLogin").show();
$("#autoLogin").children("a").trigger("click");
$("#autoLogin").hide();
}
});
</script>
<div id="autoLogin" class="hey" style="display:none">
<a href="https://localhost/login.cfm?backURL=Login&hidePendingActions=1" title="Login" class="">Login</a>
</div><div style="display:block; clear:left; padding:0px; font-family:Verdana,Arial; font-size:10px; color:gray;"></div>
		<script type="text/javascript">
		<!--
			var jsPageContributeMode = 'read';
			var jsPageSessionContributeMode = 'read';
			
			var jsPageAuthorMode = 0;
			var jsPageEditMode = 0;

			
			if(!commonspot)
				var commonspot = {};
			commonspot.csPage = {};

			
				commonspot.csPage.url = '/oit/policies/Web-Copyright.cfm';
			

			commonspot.csPage.id = 13073;
			commonspot.csPage.title = 'Web Copyright';

			commonspot.csPage.pagetype = 0;
			commonspot.csPage.publicationState = 'Published Page';
			commonspot.csPage.siteRoot = '/';
			commonspot.csPage.subsiteRoot = '/oit/policies/';
			commonspot.csPage.mode = jsPageSessionContributeMode;
			commonspot.csPage.authorok = 0;
			commonspot.csPage.approvalstatus = 0;
			commonspot.csPage.showContainerUI = 0;
			commonspot.csPage.requestedVersionTimestamp = '';
			commonspot.csPage.userRights = {};
			commonspot.csPage.userRights.isLoggedIn = 0;
			commonspot.csPage.userRights.licensedContributor = 0;
			commonspot.csPage.userRights.read = 1;
			commonspot.csPage.userRights.author = 0;
			commonspot.csPage.userRights.edit = 0;
			commonspot.csPage.userRights.approve = 0;
			commonspot.csPage.userRights.history = 0;
			commonspot.csPage.userRights.design = 0;
			commonspot.csPage.userRights.admin = 0;
			commonspot.csPage.userRights.subsiteAdmin = 0;
			commonspot.csPage.userRights.siteAdmin = 0;
			commonspot.csPage.userRights.customerAdmin = 0;
			commonspot.csPage.userRights.serverAdmin = 0;
			commonspot.csPage.userRights.enableLogout = 0;

			commonspot.csPage.enabledMenuClasses = '';
			
			commonspot.csPage.visibleMenuClasses = '';

			
			commonspot.csPage.subsiteContext = {};
			commonspot.csPage.subsiteContext.sitetype = 1;
			commonspot.csPage.subsiteContext.siteid = 1;
			commonspot.csPage.subsiteContext.subsiteid = 337;
			commonspot.csPage.subsiteContext.siteurl = '/';
			commonspot.csPage.subsiteContext.subsiteurl = '/oit/policies/';
			commonspot.csPage.subsiteContext.sitename = 'au';
			commonspot.csPage.subsiteContext.subsitename = 'policies';
			commonspot.csPage.subsiteContext.menucontexttimestamp = '1453310147525';
			commonspot.csPage.serveradminurl = "/commonspot/admin/";
			if (parent.commonspot)
				parent.commonspot.csPage = commonspot.csPage;
			var doLviewRedirect;
			if(parent &&
						parent.commonspot &&
						parent.commonspot.lview &&
						!doLviewRedirect &&
						parent.commonspot.lightbox &&
						parent.commonspot.lightbox.stack.length == 0)
			{
			
				commonspot.csPage.showPageSubmit = false;
			
				onPageArrival = function()
				{
					parent.commonspot.lview.currentPage.onPageArrival( commonspot.csPage );
				};
				onPageUnload = function()
				{
					parent.commonspot.lview.currentPage.onPageUnload();
				};
				parent.Spry.Utils.addEventListener(window, 'load', onPageArrival, false);
				parent.Spry.Utils.addEventListener(window, 'unload', onPageUnload, false);
			}
		

		// -->
		</script>
<script type="text/javascript">
<!--
	if (typeof parent.commonspot == 'undefined' || typeof parent.commonspot.lview == 'undefined')
		loadNonDashboardFiles();
	else if (parent.commonspot && typeof newWindow == 'undefined')
	{
		var arrFiles = 
				[
					{fileName: '/commonspot/javascript/lightbox/overrides.js', fileType: 'script', fileID: 'cs_overrides'},
					{fileName: '/commonspot/javascript/lightbox/window_ref.js', fileType: 'script', fileID: 'cs_windowref'}
				];
		
		loadDashboardFiles(arrFiles);
	}	
//-->
</script>

</body></html>