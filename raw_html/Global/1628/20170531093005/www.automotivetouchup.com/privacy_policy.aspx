

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head><meta http-equiv="content-type" content="text/html; charset=utf-8" /><meta name="msvalidate.01" content="B2590B06B4BEC6D2F3DB0A450D4E6870" />
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/plugins.js"></script>
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/js/plugins/fancy-box/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="/js/plugins/fancy-box/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/child_theme/js/scripts.js"></script>
    <script type="text/javascript" src="/js/functions.js"></script>
    <script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
	<script src="https://cdn.optimizely.com/js/317536305.js"></script>


    <!-- /block -->
    <link rel="shortcut icon" href="/templates/images/favicon.ico" />
    <!-- Stylesheets
	============================================= -->

    <link rel="stylesheet" type="text/css" href="/child_theme/css/all-ie-only.css" /><link rel="stylesheet" href="/css/bootstrap.css" type="text/css" /><link rel="stylesheet" href="/css/conflict/style.css" type="text/css" /><link rel="stylesheet" href="/css/dark.css" type="text/css" /><link rel="stylesheet" href="/css/font-icons.css" type="text/css" /><link rel="stylesheet" href="/css/animate.css" type="text/css" /><link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts.css" type="text/css" /><link rel="stylesheet" href="/child_theme/style.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts/tarzananarrowbold.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts/tarzananarrowregular.css" type="text/css" /><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" /><link rel="stylesheet" href="/css/responsive.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/responsive.css" type="text/css" /><link rel="stylesheet" href="/js/plugins/fancy-box/source/jquery.fancybox.css" type="text/css" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

    <!-- Document Title
	============================================= -->
    <title>
	Privacy Policy Statement | Touch Up Paint  | AutomotiveTouchup
</title>
    

<meta name="description" content="Privacy policy for Automotivetouchup.com, Microfinish LLC at AutomotiveTouchup at AutomotiveTouchup" /><meta name="keywords" content="Privacy Policy, Automotive Touch Up Privacy, Touch Up Paint, touch up paint, touchup paint, car paint, vehicle touch up paint, vehicle touchup paint, AutomotiveTouchup, Touch Up Paint, touch up paint, touchup paint, car paint, vehicle touch up paint, vehicle touchup paint, AutomotiveTouchup" /></head>
<body class="no-transition stretched">
    
    <form method="post" action="./privacy_policy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0gS/o86U5j6jNeTiyPMLTfJ30o1CYuoEAbHzEqvxtdyD5LA9kfNl5usEG/e28HqjZZfqOHt5KScL5OvpwOO0+TRlXy8rLMvoQQ85n20DFDy3yT//6P4OFDKqqFuQIVKu5HpgSdZlZmJDgKuvcJAUoYG9+NyMUV++aeNa+IPrFkXu9d0dTbwEImyh+gqof0txUFzPHh77VQPKg+kVOZVgnY66Tdm/c9GZTPgdq+5aJwt3GnVIUrCZFaJmDxyFYvlEulJrZIEtiiStO5QTMiPYDRZJgeZc4F/krW1Rer1IKdP4F3UUvDdlxJPStQZVORwycvVrrr4T9wSXt00EbDnDyTuyksnyumSU8+hyTCEbYDhg6dF5nuyRhF8YihSYYzrf4qO0O48fG8zv6bHTSrGgUNbD6ffVVVq1YPNtAhXH9ixzPoi1ufricW8GvtArkMBDKsGa2C+ktq5GS9XioNRwWXFR9XKtIkY8bmfJQOh21aZOGIEGOXAqllG+fTIjH8oBfbo9E61sE8bUFcJKpQp2pEUqT/RmV2Oez/UbVwAOXueONiYEFaYr7EMBJxrAWugdp79jjfmB3DYmLkgga2jUeRwhVpXZqmduLALQcoY7fMhloo+VuVr2D7kqCEOd0Y6zCGQVgTGunj7ovzH8UVnCUyOpI0MXnZqaKRMj7oZ5OCYwluYdourPEfvTM3Gl0X0E2F4WOfY6bxyd4j16UWG/hon7oDOP407bXI3mIpTIk4QupJaV6gjhOZWwadgQN8PZOz3S24j9MzY3mxlB34avi1/U4doTqb/ne2+ow7xOM6LkC0Op/vGfYjnWIeKfNsbDNEERtZqnlz5Bepe2pSLIqW4DdJ5i1Q5WSB2q5keks1NjzBP6stfHGJAWoIP85byzwyYS7xS8gvMY4IJwzHxtRRQ6cxUYA8UiS6YBmxHNBWb4EGXFWWcdEb9VffnLoj50ecJtWU3l2mbGM5l6nS/0okKdIHuZ7KfUcRjaFrWocRTT+G3d4ePM+9OrRPMWGdDvnfhG0IFr6q6Wu27D0sdMpk331GvA1KMFCuyHQgUD6iV9Xks2aK2WPQfET+s3Ysl1JkQ8I5OxT7qdFlwawgYcAEyFb6O/NOlFupiSwrCVPht65PQHNOaz3GYGbJ/6LjU40iV5K+NFbDYwcGTbXuS6uXO+wY6U32TgdSbL31WhjmzMKR0UBD0dLx0uNMMIE6DQrAQL+DiE0YyUhwraoXzgvskIylTkskkrfc87/NXKS94hMYg7jnhWzcpjWXoIjYymf7JWT7PruXbwMqHuZX/OqZ01InOlpvTWOtqbChMclQ7vPfDcHyOkfQ+dH3aBNrjKOScifNyMlGE7pAatsejPwpu60YAQaL9luhocLn5OsmlDdDb27mHq6teeuMdO+1U9FXbvu8tsYMgArrdhghrJFFKIFxEvVN9ygE5S48RPoIvUlpgzE+1Z02Xuin7k8B92lphIwPkJRsKQMZtDShoJF8iPIiEGAj+B6tgEqfD3/0GUB6wH+YYTcjyBcOOzXCjLhTfe7WXxSFa0hG74XKtGwm/FvYMMS/wUEOHn6lbcuAITptcn6l7azGH+HolArSN1o3c5jbvS+wRXYv5nKz78La/Imx4vIs1N7sLr4e67AJVpe9dPeaSwtZY32hCUX52/Pruhl0wGzvT585uZrqjsQnic9nXhYzO3rZmyIVykBL2kgXezg+jCUKVWWIT1YGgoXcc0cdKu7VjOeE0F5D3QDITGReRl5/Kxpn1+ygGG3Vpi7NOIf2SiWHf27eHV1ysmWMgMf4GPZ3wTmYiXV/91hqgvrtdLrOHHqc8pZitMDaAXSC8uD/gLqsPspCMBDsXzt/kRE/ZyE7ocqpc5Yf4I2cTlwDz+Ao7y6DBWL1aMKzGArj3wd5xlLqfmqmNMLcv5U+ptoP9PFDCWjhqlK3n6rrfzP+tepNZhMrR63ph4E1ae9vAFuoEiU+NbhV9HUVLZZUFS0oMYsBx/TmhrS90Tib/7T9cPkGnD3OyZZtl4wpdNFcu3Xbn0Yrld4tg2L3r3D92vplSSAqUhOxYhu0S/4SCkXvRFQLcb/FPfig+b1bhYFiszKCCetSaWAAVkMI+XNxyCT+UChAPSr0dl2OF9OZctS5aR0OCFYeHEMfM51U/vw7MDyAxV1XfAvBbmJcElFnl320a7NQwEMV/DSKRugfOwS2SgLHmzwIbfLQ9mTbnubJYbS0KJFWHcrlzc2qDuoMGje/ApJlJPk0+E0JFyVnLJW4BTxXgcnul8S/WU6c3cHRm65nVvMVLxf83UdGvR5SahpS6n5vJQxinbgQiEc0sG+a7Em16MBeDQf7oBui7aiu5qnN9rD5iX1VxMaIxH+7m5jjm12bdyv5o44v3sJpmRTZz1iyN6iz5SigDQCQ94Zk89HXX7EsA1il7qXQ/59w9WuymULc70g1AqVHHOCZ1ItT8xLkL03ZgZHr07YmFZAqYHZWSNxFkoQ3T+TrE94bP+TbzZuX3GC1o4Tj4jSzDeLoJVb6EVlAmw/r/LVeuDwgIvhlTUn/ngLYhTrhQx6o41O17YDP9yssKDS2KVrmdQofjTZBXm/pgl31hm5+OnqFdd3s/AjC8ogGDQKNNHtJvxg7uUDXkip3G2qTi+TlxAuOVUY5TSDebqE8PQbMyIDATeVzmNf4vLRKOTJat6DXgWh0yKRxHYp2yzQMNxbss83KfbAOPwgXzZ0VuMzd0JdihiYHZtIgnUjKq03NyYtMQ+5JVKjoGFWVJAy4nV3YFMX5Wbny+AyXwn72lPU358IHXLEqCULHq2djUVS5J+5KIntejGxcMSbXzDi8RPdxwfh0limRZjsNR+8pJtNSPutH0LSSZuo/2ZGpuKOAqTtCq7lmS9dmKc+clHV+toz8gPpQw5mOUywZAZFoQb73lFKmcTUfyVlZ5CWVrUnbGf" />


<script src="/js/outermaster_external.js" type="text/javascript"></script>
<script src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yGe-MgZ6_6pvcmjBkCQMNS8-RyI640njGgMPV5RliEowHeuNCdPy0CKFe_CVqjeuPEUK-hDvdpNNyoaq0z-lCUADdhHv7eusPZaq_xADHMAocgoC7y0BxM6hg5mZQCr3LzDoER7ofeJIAI-RcznGFfI1&amp;t=ffffffffcc58dd65" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2927320D" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="zTSDZ9shzTSqKTib+0YZ7TVWgpkqLM7ZViPqf4kA/+ul2wGDEkKxxaKUrObu9VfXNR0YNsd/a4yq3C/WBI5MBlFbIfPgnUvfhr1lfhJ8UNP9j9LOgWU4ZzrLK+hAvo02w0Fnp7rrbqXPpvD6PryL/KnWkYSc0wec42zQAIiOKh6bj+zRDlMvpzBPlw7bmfplQtNGdQyHNMlKszx0q1EaedLs81Hp1xN6YeyQbGkK88+3v144dfxxxplDjjTDYOnBa2x16x9cdzqI7J4FWE/PfeWhHlaSMS+lcs81AaRXjugKtjtuFP106iVk91caO0tqJ343Rfe5nP8jz8IQwwmEBLKWIy6j6vkiI9kSK+YGsSmg7mk1ccMpJwVbj2KmyKrFl1StvD3aXZc2uBTHOzmsJUG1vzF4/fU3dYdwi0eeR2bJ0UccgnR1JgMe24EXTQfHCxZDcWHN93VbEniYzYeWQXshzNKD1W0uKfhVa4/Vfu8Jg3+4CCfO0cHATluGUfDiHxtrdVYL974agZNwhBXzJfti1tWhj9sxZ2DAPyG/fJ6t9BZMWYqub8O7REJJOuzOkQFa/ZdK1KbjyjdOWnHG7KADfwUSal7eDDkdarbHGkPstBxeL42DwDxx00onH+P0oEAJn2VCSfyNWReGXDxXr28zARmrh5eJX23YhnrbZy+nFWNZm2M/ffOdtpp1hx1MPKSFdD0XeLpm1nLkTzEgBWodc+XLG/Fe8lIUNcnnJoeqDCJxu/8Dp7Lyaiwb4k7g1q2dMxg+IzGBgf54S/aykeHpFcLQUQX/IpqriJCvP+SFhcm0Xnb9eORsN1bYyJFHwo1sukzj5Q0UY7vrDpU3Kxd8nXJ+cwhdCS/WdnW9dEFD98RfqSqxU0lNOp19IgsvE3KCumDJmsf/96slcFNfJ8yJZ1WhErf07z8OJR7ysNCvFAbufv3DqehNhakAWT8aemHF3n4ZVOGbMD/EmrdGIPjjcFBMxXqhzdC473jbipbH4CvP3q1NjHOAYlcXa+1VcumUZGIswFIZ3rZrUlfoJqtWNGFqWjNvulv13ZulDozZUZ/C7pxBzEZY9eKhKUDGI/RbNc3cFkuNERBEVyxEpzfegxzQzhld8KyiFT8A9NZRSgvn5SaiJpUxBk8O6k4ohGZHiPqjQrdYb6J0Vb554Ssukou4Nzr+dTnc9vTIeR1XrJdts5vibmivegcIJGaFHv4kBvQhPkq3KJE/fN31XWaGuWEkGClnsB+hdX3DKnwWfuMFWesLupEqQxgv38rp1Y2F/nruzFafEty/nVMlKNExKuY4GeFV1fsy+fjJtSmZEyRpquAjyWqCqX3zi5ILGQweDDz5iSuRJ3FdqrcOCvOLB3RiG1+hM1I06gXIfcZFLtR+CpNMVkiv1cfxUnAdvyN8CRtvh+JFPtg+ALp8xfiDXJWcghvOm76pTuEz+M3xYNAnsZ8ZyaM83lyV30BtdxXO21vWlHVtePf45Zh/JS+ImFDwgh2yVCjH5rKUrmfb+56gFeuHVXRBZKb+WzeVklxpUx+BN/smRFfhVau1V0RnT2sA11Ro0j4sQp83soKwSetQ+WEM6Q8Qv3YKUT8Flkdwn5NCEl09OFVi7hX8uJ5IGv5iBXOTl+PJ61HLxJlxi8uWGoryFnTMlWbERillB5DYG9upO+k3eGfbPLbK39tvTM7PbcsZSwRATk/X944ZrYAFlFkxC2hFN+/axq77B7ta7KKN6TNEjqCmsne8UBgxqnoCmErdlNC+oA0Uqgjhut8p1UMOkFE11wiUYburQZ9FYxQQJcdISVy5Tk8eE+FR1eXR1RVCK1yga2xJVDqcogy13iHBBZOZeg1jIfZK7kOCDK+eMlrcrMuS+WaEVvphe3CFotsakzseA5k2t6wxXi4DrWMqNqYunALROK6GbglCtMLatWJzde0qJh0ReDWDJ0uSGGmDCJoJs305uhw5f1O/d2t1+TqxApxeBaa7A5/KfkmQok21luFQKU5zJDZd7G1rrlK+FTsnq0KxoIrJ9HDwNkTsdXttjwgNBAdnF6XAOKIlh9HokgLInbFnSWXic1gpDb7qgQkWd6qDDfvoq9q22U6J5MqE16FsUC5MngzhQWrLAgUKCgDswE58dDJRShuF5mWvu3mF0MxWo7zM6ziM6X96TXlDR0QCPRi5fHJi0P2zN1n1WQmMR8YD66ZgT1XSZR2XWemYTCN1kA9ZFmnU4kYV4exEjLHi7S2peuoYTPycrc3jcdve12wfse6Fh7qLYbQSdfEyUV1G3x2pvy0Dczt9lF+j+C5UQYpWXbFtO9IvLo/M0IzLbLcN6ctHdhKLoz1sdvd+Bjwpo/ae/zENxnHv5ANcMOfvjSj/q0KJ/MoYzAofEz8dp4W3Kd9MDcviXfvBDgjvt2SRUBBCti0XRYoo1RYYndPEDBX5LyvZX2lNyKCyC64vU8gTQjDm/TccI3R3SvQ4ZT65lBexQ1SnKWeDtyADtI1DZ7SkZkYI7ud/cTem0oyH8DdhFdAokKFzbEu8YUjWOgcXXpJingraPYL5NiZimTJimqDJGBMUyt4M8AzWUiBfVtMT9uK3I2fVU35TQuvJpVryYx0UHeqfLad1sKrn+icz5i2pr88QDO1bD80SBeHDtG+RuF9w0ILQIH33iLBedtlcwoMXj/jJDqdWMcw5r9nWnbs0WTITFJVE4EuaeIeTEUhn5FEDcvk+hpnPA5fZSXChL4RsVP3OYnyDifU1GxSxHgcNkODL5ZOT69EaDQfNY4N17DhTy0POdat5NOwsFyECSNQfeUh+TxB3YWTPd28EXn1pfDOsEnkGLAqHkT1EtPO6nOcF2QIfAHtOqQAQzQC/VyarNYR8zDtRi8XoniY3BY1mi11AW4U1n0DaK2lYniwE/tHF3o2YkcTZNud5kI3uyWXPr+Cin16WarusRTNB98RI1VUawkWp9DlnDr5E6BukLk/TilqsoZ6XMMY2Nld7JR2BR1KkgQJEhRPgmvjPzuW4/jrx5E3Ay3hz2jYJh+oR0b5p1AFuwet92nW9W3NriyS9pd+4BqWu9u53mjyat4pb1Uh+mMmEWRnkSRfd1TXfQOv7rR5F7abm7OQWzeQEvgboCaFbRcg9fIZZWc/Ys5I+JEQaeGfKiAfGLF1y3BQr5g==" />

        

        <!-- Document Wrapper
	============================================= -->
        <div id="wrapper" class="clearfix">

            <div class="top-bar">
                <div class="container">
                    <div class="row">

                        <!-- Logo
					============================================= -->
                        <div id="logo">
                            <a href="http://www.automotivetouchup.com/" class="standard-logo" data-dark-logo="/child_theme/images/logo.png">
                                <img width="238" src="/child_theme/images/logo.png" alt="Logo" /></a>
                            <a href="http://www.automotivetouchup.com/" class="retina-logo" data-dark-logo="/child_theme/images/logo.png">
                                <img width="238" src="/child_theme/images/logo.png" alt="Logo" /></a>
                        </div>
                        <!-- #logo end -->


                        <div class="col-xs-12 text-right">
                            <ul class="list-inline social-links">
                                <li><strong>1-888-710-5192</strong></li>
                                <li>|</li>
                                <li><a href="/contact-us.aspx">Contact</a></li>
                                <li>|</li>
                                <li><a href="/check.aspx">Order Status</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Header
		============================================= -->
            <header id="header" class="transparent-header dark no-sticky">

                <div id="header-wrap" style="background-color: #455660;">

                    <div class="container clearfix dark">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>


                        <!-- Primary Navigation
					============================================= -->
                        <nav id="primary-menu">
                            <div class="top-search-content">
                                <div>
	
                                    <input name="ctl00$ctl00$SearchTextBox" type="text" id="ctl00_ctl00_SearchTextBox" class="form-control" placeholder="Type &amp; Hit Enter.." />
                                    
                                    <input type="submit" name="ctl00$ctl00$TopSearchImageButton" value="" id="TopSearchImageButton" style="display: none" />
                                
</div>
                            </div>
                            <ul>
                                <li class="mega-menu">
                                    <a href="/touch-up-paint/">
                                        <div>Choose Your Make <span class="twosep">:</span></div>
                                    </a>
                                   <div class="mega-menu-content myFirstMegaMenu style-2 clearfix">
										<ul class="mega-menu-column col-md-3">

										<li><a href="/touch-up-paint/acura/">Acura</a></li>
                                        <li><a href="/touch-up-paint/audi/">Audi</a></li>
                                        <li><a href="/touch-up-paint/bmw/">BMW</a></li>
                                        <li><a href="/touch-up-paint/buick/">Buick</a></li>
                                        <li><a href="/touch-up-paint/cadillac/">Cadillac</a></li>
                                        <li><a href="/touch-up-paint/chevrolet/">Chevrolet</a></li>
                                        <li><a href="/touch-up-paint/chrysler/">Chrysler</a></li>
                                        <li><a href="/touch-up-paint/dodge/">Dodge</a></li>
                                        <li><a href="/touch-up-paint/ford/">Ford</a></li>
										</ul>
										<ul class="mega-menu-column col-md-3">
										<li><a href="/touch-up-paint/gmc/">GMC</a></li>
                                        <li><a href="/touch-up-paint/honda/">Honda</a></li>
                                        <li><a href="/touch-up-paint/hummer/">Hummer</a></li>
                                        <li><a href="/touch-up-paint/hyundai/">Hyundai</a></li>
                                        <li><a href="/touch-up-paint/infiniti/">Infiniti</a></li>
                                        <li><a href="/touch-up-paint/jaguar/">Jaguar</a></li>
                                        <li><a href="/touch-up-paint/jeep/">Jeep</a></li>
                                        <li><a href="/touch-up-paint/kia/">Kia</a></li>
                                        <li><a href="/touch-up-paint/lexus/">Lexus</a></li>
										</ul>
										<ul class="mega-menu-column col-md-3">
										<li><a href="/touch-up-paint/land-rover/">Land Rover</a></li>
										<li><a href="/touch-up-paint/lincoln/">Lincoln</a></li>
                                        <li><a href="/touch-up-paint/mazda/">Mazda</a></li>
                                        <li><a href="/touch-up-paint/mercedes-benz/">Mercedes-Benz</a></li>
                                        <li><a href="/touch-up-paint/mini/">Mini</a></li>
                                        <li><a href="/touch-up-paint/mitsubishi/">Mitsubishi</a></li>
                                        <li><a href="/touch-up-paint/nissan/">Nissan</a></li>
                                        <li><a href="/touch-up-paint/pontiac/">Pontiac</a></li>
                                        <li><a href="/touch-up-paint/porsche/">Porsche</a></li>
										</ul>
										<ul class="mega-menu-column col-md-3">
										<li><a href="/touch-up-paint/saab/">Saab</a></li>
										<li><a href="/touch-up-paint/saturn/">Saturn</a></li>
                                        <li><a href="/touch-up-paint/scion/">Scion</a></li>
                                        <li><a href="/touch-up-paint/smart/">Smart</a></li>
                                        <li><a href="/touch-up-paint/toyota/">Toyota</a></li>
                                        <li><a href="/touch-up-paint/volkswagen/">Volkswagen</a></li>
                                        <li><a href="/touch-up-paint/volvo/">Volvo</a></li>
                                        <li><a href="/touch-up-paint/">Don't see your make?</a></li>
										</ul>
									</div>
                                </li>


                                <li class="mega-menu"><a href="/paint-code.htm">
                                    <div>Paint Codes <span class="twosep">:</span></div>
                                </a></li>

                                <li class="mega-menu col-4-menu">
                                   <a href='http://www.automotivetouchup.com/products.aspx'>
                                        <div>Products <span class="twosep">:</span></div>
                                    </a>
                                    <ul style="display: none">
                                        
                                        <li><a href='http://www.automotivetouchup.com/touch_up_paint.asp'>1/2oz Paint Bottles</a>
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_paint_matched.aspx'>1/2oz Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_paint_clearcoat.aspx'>1/2oz Clearcoat</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_primer.aspx'>1/2oz Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>1/2oz Accessories</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/index.aspx'>2oz Paint Bottles</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_basecoat.aspx'>2oz Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_clearcoat.aspx'>2oz Clearcoat </a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_primer.aspx'>2oz Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>2oz Accessories</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/paintpen.asp'>Paint Pens</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_matched.aspx'>Paint Pen Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_clearcoat.aspx'>Paint Pen Clearcoat</a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_primer.aspx'>Paint Pen Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Paint Pen Accessories</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/spray_paint.asp'>Aerosol Spray Paint</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/spray_paint_matched.aspx'>Aerosol Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_clearcoat.aspx'>Aerosol Clearcoat</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_primer.aspx'>Aerosol Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_accessories.aspx'>Aerosol Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_trim.aspx'>Black Trim Aerosol</a></li>
												<li><a href='http://www.automotivetouchup.com/spray-paint/adhesion-promoter.aspx'>Adhesion Promoter</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/auto_paint.asp'>Spray Gun Paint</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/auto_paint_basecoat.aspx'>Spray Gun Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_clearcoat.aspx'>Spray Gun Clearcoat</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_primer.aspx'>Spray Gun Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_accessories.aspx'>Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/spray-guns/all-spray-guns.aspx'>Spray Guns</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Accessories</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Touch Up Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_accessories.aspx'>Aerosol Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_accessories.aspx'>Auto Paint Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/spray-guns/all-spray-guns.aspx'>Spray Guns</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/prep_solvent.aspx'>Prep Solvent</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/rubbing_compound.aspx'>Rubbing Compound</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/surface-wipe.aspx'>Prep Wipe</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/bodyandbumper.aspx'>Body & Bumper</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/bodyandbumper.aspx'>Body & Bumper Repair</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bondo.aspx'>Bondo Body Filler</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/ultimate_bondo.aspx'>Ultimate Body Filler</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bondo-professional-gold-filler.aspx'>Gold Body Filler</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/spot_putty.aspx'>Glazing and Spot Putty</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bumper_repair_kit.aspx'>Syringe Bumper Repair</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/flexible_part_bumper_repair.aspx'>Easy Finish Part Repair</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/flexible_part_bumper_repair.aspx'>Easy Finish Bumper </a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/plastic_spreaders.aspx'>Plastic Spreaders</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper.aspx'>Sandpaper</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper-multi-pack.aspx'>Sandpaper Multi-Pack</a></li>
												
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/file_holder.aspx'>3 Position File Holder</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/sanding_block.aspx'>Rubber Sanding Block</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/sandpaper/general-abrasives.aspx'>Sandpaper</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper.aspx'>Wet Sandpaper</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper-multi-pack.aspx'>Assorted Sanding Pack</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/touch_up_sandpaper.aspx'>Mini Sandpaper</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/scuff-pad.aspx'>Scuff Pad</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/sanding_block.aspx'>Rubber Sanding Block</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/file_holder.aspx'>3 Position File Holder</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/stick-on-disc.aspx'>Stick-On DA Paper</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/safety.aspx'>Safety</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/dust_mask.aspx'>Dust Masks</a></li>
												<li><a href='http://www.automotivetouchup.com/safety/respirator.aspx'>SAS Bandit Respirator</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/respirator.aspx'>3M Respirator</a></li>
												<li><a href='http://www.automotivetouchup.com/safety/gloves.aspx'>Nitrile Gloves</a></li>
												<li><a href='http://www.automotivetouchup.com/safety/glasses.aspx'>3M Eye Protection</a></li>
											</ul>
										</li>
										<li><a href="#">Basecoat</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_paint_matched.aspx'>1/2oz Brush-In-Bottle </a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_basecoat.aspx'>2oz Brush-In-Bottle </a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_matched.aspx'>Paint Pen Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_matched.aspx'>Aerosol Spray Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_basecoat.aspx'>Paint for Spray Guns </a></li>
											</ul>
										</li>										
										<li><a href="#">Clearcoat</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_paint_clearcoat.aspx'>1/2oz Clearcoat </a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_clearcoat.aspx'>2oz Clearcoat </a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_clearcoat.aspx'>Clearcoat Paint Pen</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_clearcoat.aspx'>Clearcoat Aerosol</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_clearcoat.aspx'>Spray Gun Clearcoat </a></li>
												<li><a href='http://www.automotivetouchup.com/lacquer_clearcoat.aspx'>Aerosol Lacquer</a></li>
												<li><a href='http://www.automotivetouchup.com/spray-paint/spraymax-2k-clearcoat.aspx'>Aerosol Urethane</a></li>
												<li><a href='http://www.automotivetouchup.com/lacquer_clearcoat.aspx'>Lacquer Qt/Gal </a></li>
												<li><a href='http://www.automotivetouchup.com/urethane_clearcoat.aspx'>Urethane Qt/Gal</a></li>
											</ul>
										</li>
										<li><a href="#">Primer</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_primer.aspx'>1/2oz Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_primer.aspx'>2oz Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_primer.aspx'>Primer Paint Pen</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_primer.aspx'>Aerosol Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_primer.aspx'>Spray Gun Paint Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/lacquer_primer.aspx'>Laquer Qt/Gal</a></li>
												<li><a href='http://www.automotivetouchup.com/urethane_primer.aspx'>Urethane Qt/Gal</a></li>
											</ul>
										</li>
                                        


                                    </ul>
                                </li>

                                <li class="mega-menu"><a href="/how-to-videos/">
                                    <div>How To Videos <span class="twosep">:</span></div>
                                </a></li>

                                <li class="mega-menu"><a href="/directions.htm">
                                    <div>Directions <span class="twosep">:</span></div>
                                </a></li>

                                <li class="mega-menu"><a href="/gallery.htm">
                                    <div>Results <span class="twosep">:</span></div>
                                </a></li>
                                <li class="mega-menu"><a href="/feedback.aspx">
                                    <div>Reviews <span class="twosep">:</span></div>
                                </a></li>
                                <li class="mega-menu"><a href="/about-us.aspx">
                                    <div>About ATU <span class="twosep"></span></div>
                                </a></li>
								
								<li class="extraSpace"></li>



                            </ul>

                            <!-- Top Cart
						============================================= -->
                            <div id="top-cart">

                                <a id="ctl00_ctl00_lnkCart1" rel="nofollow" href="https://www.automotivetouchup.com/cgi-bin/ShoppingCart.aspx"><div id="ctl00_ctl00_CartUpdatePanel">
	
                                            <img src="/child_theme/images/cart.png" alt="cart-icon"><span id="ctl00_ctl00_NumItemslabel">0</span>
                                        
</div></a>


                            </div>
                            <!-- #top-cart end -->
                            <!-- search Bar
							============================================= -->
                            <div id="top-search">
                                <a href="#"></a>
                            </div>
                            <!-- search Barend -->
                        </nav>
                        <!-- #primary-menu end -->

                    </div>

                </div>



            </header>
            <!-- #header end -->

            
    
    <!-- Slider -->
    <section id="slider" class="clearfix page">
        <div class="main-slide" style="background-repeat: no-repeat;">
            <div class="container clearfix">
                <div class="row-fluid">
                    <div class="col-xs-6 col-sm-6 col-md-6"></div>
                    <div class="maincaptions col-xs-12">
                        <h1 data-caption-animate="fadeInUp" class="fadeInUp animated">PRIVACY POLICY</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Content
		============================================= -->
    <section id="form" class="clearfix">

        <div class="container clearfix">
            <div class="row clearfix">
				 <div class="col-xs-12 col-sm-9 col-sm-push-3">
					<div class="col-xs-12 text-right page-breadcrumbs"><a href="/">HOME</a> > 

                
		<!-- div continued from developSiderbar -->
             PRIVACY POLICY </div>
        <div class="col-xs-12">
            <div id="subpage">
        
        <p>
        AutomotiveTouchup.com belongs to Microfinish LLC.
        </p>
        <p>
Our postal address is
        </p>
        <p>
Microfinish LLC<br>
208 Plauche Ct.<br>
New Orleans, LA 70123<br>
We can be reached via e-mail at web@automotivetouchup.com<br>
or you can reach us toll free by phone at 1-888-710-5192
        </p>
        <p>
For each visitor to our Web page, our Web server automatically recognizes no information regarding the domain or e-mail address (It may recognize an IP address or a referring domain).
        </p>
        <p>
We collect no information on consumers who browse our Website.
        </p>
        <p>
The information we collect is used for internal review and is then discarded, not shared with other organizations for commercial purposes.
        </p>
        <p>
With respect to cookies: We use cookies to record session information, such as items that consumers add to their shopping cart.
        </p>
        <p>
If you do not want to receive e-mail from us in the future, please let us know by sending us e-mail at the above address, writing to us at the above address. We do not use email as a form of marketing.
        </p>
        <p>
If you supply us with your postal address on-line you will only receive the information for which you provided us your address. Persons who supply us with their telephone numbers on-line will only receive telephone contact from us with information regarding orders they have placed on-line.
        </p>
        <p>
With respect to Ad Servers: We do not partner with or have special relationships with any ad server companies.
        </p>
        <p>
Upon request we provide site visitors with access to transaction information (e.g., dates on which customers made purchases, amounts and types of purchases) that we maintain about them, a description of information that we maintain about them.
        </p>
        <p>
Upon request we offer visitors the ability to have inaccuracies corrected in contact information.
Consumers can have this information corrected by sending us e-mail at the above address, writing to us at the above address.
        </p>
        <p>
With respect to security: We always use industry-standard encryption technologies when transferring and receiving consumer data exchanged with our site, We have appropriate security measures in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.
        </p>
        <p>
If you feel that this site is not following its stated information policy, you may contact us at the above addresses or phone number.
        </p>
        <p>
We appreciate your comments and choose to spotlight some of our customer comments as testimonials. If your comment is selected to appear on our site, we will use only your first name, last initial, location, car/paint info, and your comment. When <a href="/feedback.aspx">submitting your comment</a>, you will have the option to opt out of having your review appear on our site.
        </p>
    </div>
           
       <!-- two more closing divs before the sidebar code starts -->


				
					</div>
				</div>
                <!-- sidebar starts here -->

                <div class="col-xs-12 col-sm-3 col-sm-pull-9 secondary-nav">
                    <span class="visible-xs hidden-sm hidden-md hidden-lg"><a class="nav-drop" data-toggle="collapse" href="#navCollapse" aria-expanded="false" aria-controls="navCollapse">MORE PRODUCTS &#8681;
                    </a></span>

                    <div class="collapse left-sidebar" id="navCollapse">
					
						<div class="mySidebarSearch">
						<h4>FIND YOUR PERFECT COLOR MATCH</h4>
						<p style="margin-bottom:0!important;">Enter your year, make, and model below to find color matched paint:</p>
						<div id="ctl00_ctl00_OuterBodyContentPlaceHolder_ColorSearchPanel" class="sidebarsection">
	
                        <div id="sidebarselectleft">
                            <span id="step1">STEP 1:</span> <span id="step2">STEP 2:</span> <span id="step3">STEP
                            3:</span>
                        </div>
                        <div id="sidebarselectright">
                            <a id="RefreshButton" href="javascript:__doPostBack(&#39;ctl00$ctl00$OuterBodyContentPlaceHolder$RefreshButton&#39;,&#39;&#39;)" style="display: none"></a>
                            <div id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterSearchUpdatePanel">
		
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterYearDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList">
			<option value="">--Year--</option>
			<option value="2018">2018</option>
			<option value="2017">2017</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option selected="selected" value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009">2009</option>
			<option value="2008">2008</option>
			<option value="2007">2007</option>
			<option value="2006">2006</option>
			<option value="2005">2005</option>
			<option value="2004">2004</option>
			<option value="2003">2003</option>
			<option value="2002">2002</option>
			<option value="2001">2001</option>
			<option value="2000">2000</option>
			<option value="1999">1999</option>
			<option value="1998">1998</option>
			<option value="1997">1997</option>
			<option value="1996">1996</option>
			<option value="1995">1995</option>
			<option value="1994">1994</option>
			<option value="1993">1993</option>
			<option value="1992">1992</option>
			<option value="1991">1991</option>
			<option value="1990">1990</option>
			<option value="1989">1989</option>
			<option value="1988">1988</option>
			<option value="1987">1987</option>
			<option value="1986">1986</option>
			<option value="1985">1985</option>
			<option value="1984">1984</option>
			<option value="1983">1983</option>
			<option value="1982">1982</option>
			<option value="1981">1981</option>
			<option value="1980">1980</option>
			<option value="1979">1979</option>
			<option value="1978">1978</option>
			<option value="1977">1977</option>
			<option value="1976">1976</option>
			<option value="1975">1975</option>
			<option value="1974">1974</option>
			<option value="1973">1973</option>
			<option value="1972">1972</option>
			<option value="1971">1971</option>
			<option value="1970">1970</option>
			<option value="1969">1969</option>
			<option value="1968">1968</option>
			<option value="1967">1967</option>
			<option value="1966">1966</option>
			<option value="1965">1965</option>
			<option value="1964">1964</option>
			<option value="1963">1963</option>
			<option value="1962">1962</option>
			<option value="1961">1961</option>
			<option value="1960">1960</option>
			<option value="1959">1959</option>
			<option value="1958">1958</option>
			<option value="1957">1957</option>
			<option value="1956">1956</option>
			<option value="1955">1955</option>
			<option value="1954">1954</option>
			<option value="1953">1953</option>
			<option value="1952">1952</option>
			<option value="1951">1951</option>
			<option value="1950">1950</option>
			<option value="1949">1949</option>
			<option value="1948">1948</option>
			<option value="1947">1947</option>
			<option value="1946">1946</option>
			<option value="1945">1945</option>
			<option value="1944">1944</option>
			<option value="1943">1943</option>
			<option value="1942">1942</option>
			<option value="1941">1941</option>
			<option value="1940">1940</option>
			<option value="1939">1939</option>
			<option value="1938">1938</option>
			<option value="1935">1935</option>

		</select>
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterMakeDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList">
			<option value="">--Make--</option>
			<option value="Acura">Acura</option>
			<option value="Alfa Romeo">Alfa Romeo</option>
			<option value="Aston Martin">Aston Martin</option>
			<option value="Audi">Audi</option>
			<option value="Bentley">Bentley</option>
			<option value="BMW">BMW</option>
			<option value="Buick">Buick</option>
			<option value="Cadillac">Cadillac</option>
			<option value="Chevrolet">Chevrolet</option>
			<option value="Chrysler">Chrysler</option>
			<option value="Daihatsu">Daihatsu</option>
			<option value="Dodge">Dodge</option>
			<option value="Ferrari">Ferrari</option>
			<option value="Fiat">Fiat</option>
			<option value="Ford">Ford</option>
			<option value="GMC">GMC</option>
			<option value="Honda">Honda</option>
			<option value="Hyundai">Hyundai</option>
			<option value="Infiniti">Infiniti</option>
			<option value="Isuzu">Isuzu</option>
			<option value="Jaguar">Jaguar</option>
			<option value="Jeep">Jeep</option>
			<option value="Kia">Kia</option>
			<option value="Lamborghini">Lamborghini</option>
			<option value="Land Rover">Land Rover</option>
			<option value="Lexus">Lexus</option>
			<option value="Lincoln">Lincoln</option>
			<option value="Lotus">Lotus</option>
			<option value="Maserati">Maserati</option>
			<option value="Mazda">Mazda</option>
			<option value="Mercedes-Benz">Mercedes-Benz</option>
			<option value="Mini">Mini</option>
			<option value="Mitsubishi">Mitsubishi</option>
			<option value="Nissan">Nissan</option>
			<option selected="selected" value="Porsche">Porsche</option>
			<option value="Rolls Royce">Rolls Royce</option>
			<option value="Scion">Scion</option>
			<option value="Smart">Smart</option>
			<option value="Subaru">Subaru</option>
			<option value="Suzuki">Suzuki</option>
			<option value="Tesla">Tesla</option>
			<option value="Toyota">Toyota</option>
			<option value="Volkswagen">Volkswagen</option>
			<option value="Volvo">Volvo</option>

		</select>
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterModelDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList">
			<option value="">--Model--</option>
			<option value="All Models">All Models</option>
			<option value="911">911</option>
			<option value="918">918</option>
			<option value="Boxster">Boxster</option>
			<option value="Carrera">Carrera</option>
			<option value="Cayenne">Cayenne</option>
			<option selected="selected" value="Cayman">Cayman</option>
			<option value="Macan">Macan</option>
			<option value="Panamera">Panamera</option>

		</select>
                                
	</div>
                            
                            
                            
                            
                            <input type="submit" name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterColorSearchButton" value="Search" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$OuterBodyContentPlaceHolder$OuterColorSearchButton&quot;, &quot;&quot;, true, &quot;ColorSearchValidationGroup&quot;, &quot;&quot;, false, false))" id="OuterColorSearchButton" class="btn btn-primary btn-center white" />
                        </div>
						<div style="clear:both;"></div>
                    
</div>
						</div>

                        <h4>START HERE</h4>
                        <ul>
                            <li><a href="/touch-up-paint/">Find Your Car’s Color</a></li>
							<li><a href="/directions.htm">How to Use Our Products</a></li>
							<li><a href="/how-to-videos/">See How it Works on Video</a></li>
							<li><a href="/paint-code.htm ">Where to Find Your Paint Code</a></li>
                        </ul>

                        <h4>TYPES OF PAINT</h4>
                        <ul>
                            <li><a href="/touch_up_paint.asp">1/2oz Paint (Brush-in-Bottle)</a></li>
							<li><a href="/store/2oz_touch-up_paints/index.aspx">2oz Paint (Brush-in-Bottle)</a></li>
							<li><a href="/paintpen.asp">Paint Pens</a></li>
							<li><a href="/spray_paint.asp">Aerosol Spray Paint</a></li>
							<li><a href="/auto_paint.asp">Auto Paint (for Spray Guns)</a></li>
                        </ul>


                        <h4>ACCESSORIES</h4>
                        <ul>
							<li><a href="/touch_up_accessories.aspx">Touch Up Paint Accessories</a></li>
							<li><a href="/spray_paint_accessories.aspx">Spray Paint Accessories</a></li>
							<li><a href="/bodyandbumper.aspx">Body and Bumper Repair</a></li>
							<li><a href="/sandpaper/general-abrasives.aspx">General Abrasives</a></li>
							<li><a href="/safety.aspx">Safety</a></li>
                        </ul>

                    </div>
                </div>
                <!-- end sidebar -->
            </div>
        </div>
    </section>





            <!-- Footer
		============================================= -->
            <footer id="footer" class="light" style="background: url('/child_theme/images/footerbg.jpg') center no-repeat; background-size: cover;">

                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 col-sm-3">
                            <h3>Contact</h3>



                            <p>
                                Microfinish LLC<br />
                                208 Plauche Ct<br />
                                New Orleans, LA 70123
                            </p>
                            <p>
                                Toll Free: 1-888-710-5192<br />
                                Phone: 1-504-818-2334<br />
                                Fax: 1-504-818-2996
                            </p>
                            <p>
                            </p>
                            <p><a href="/contact-us.aspx" class="btn btn-primary">Contact Us</a></p>
                        </div>



                        <div class="col-xs-12 col-sm-3">
                            <h3>Learn More</h3>
                            <ul>
								<li><a href="/touch-up-paint/">Choose Your Make</a></li>
                                <li><a href="/faq.htm">FAQ</a></li>
                                <li><a href="/site_map.htm">Sitemap</a></li>
                                <li><a href="/e-store_policies.aspx">Policies</a></li>
                                <li><a href="/affiliate.aspx">Affiliates</a></li>
                                <li><a href="/link_to_us.aspx">Link To Us</a></li>
                                <li><a href="/privacy_policy.aspx">Privacy Policy</a></li>
                            </ul>
                            <div class="flex-stretch">
                                <a href="https://www.facebook.com/AutomotiveTouchup/"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.youtube.com/user/Automotivetouchup"><i class="fa fa-youtube"></i></a>
                                <a href="https://plus.google.com/+AutomotivetouchupPaint"><i class="fa fa-google-plus"></i></a>
                                <a href="https://twitter.com/atu_microfinish"><i class="fa fa-twitter"></i></a>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <h3>Information</h3>
                                &copy; 2017 | AutomotiveTouchup, a Microfinish LLC Company<br />
                                <br />

                                <img width="280" src="/child_theme/images/cc.png" alt="credit cards we accept" /><br />
                                <br />

                                <img src="/child_theme/images/footerlogo.png" alt="AutomotiveTouchup Logo" />
                        </div>
                    </div>
                </div>

			 
			 
			
			<a id="RefreshButton" href="javascript:__doPostBack(&#39;ctl00$ctl00$RefreshButton&#39;,&#39;&#39;)" style="display: none"></a>
			
			<div id="content">

				<div id="contentwhite">
				</div>
				<div id="contentwhitebottom">
					
					
				</div>
				
					<div style="display: none;">
						<img height="1" width="1" style="border-style: none;" alt="ad" title="ad" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072509267/?value=0&amp;label=g1PzCMPsrQkQ0-K0_wM&amp;guid=ON&amp;script=0" />
					</div>
				
			</div>
			</footer>
        </div>

        <!-- #wrapper end -->

        <!-- Go To Top
		============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>



        <!-- Footer Scripts
	============================================= -->
        <script>
            $('.fancybox-thumbs').fancybox({


                helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });
            $(document).ready(function () {

                function searchF() {
                    var sWidth = $("#primary-menu > ul").width();
                    $(".top-search-content").css('width', sWidth);
                };
                $('#top-search a').click(function () {
                    $(".top-search-content").toggle();
                    $("#top-search a").toggleClass('on');
                });
                searchF();

                $(window).resize(function () {
                    searchF();
                });

            });

        </script>
      
<script>
$('#trigger-uploads').click(function(e){
e.preventDefault();
$(this).hide();
$('#uploads-table').show();
});
</script>

<!-- Move To Top feature -->
<script type="text/javascript">
$(window).scroll(function(){
if($(window).scrollTop() > 500){
$('#gotoTop').show();
}else{
$('#gotoTop').hide();
};
});
</script>
<script type="text/javascript">
$('#gotoTop').click(function(){
$('html, body').animate({scrollTop : 0},400);
return false;
});

</script>
<!-- / Move To Top feature -->
<!-- Mouseflow -->
<script type="text/javascript">
    var _mfq = _mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/d118fe43-3e59-4474-a834-342182950bd8.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>
<!-- /Mouseflow -->
    

<script type="text/javascript">
//<![CDATA[
$(document).ready(function () {if (!checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList','2014') || !checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList','Porsche') || !checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList','Cayman')) {findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList', ''); findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList', ''); findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList', ''); __doPostBack('ctl00$ctl00$OuterBodyContentPlaceHolder$RefreshButton', '');}});//]]>
</script>

<script src="/js/outermaster_internal.js" type="text/javascript"></script></form>

</body>
</html>
