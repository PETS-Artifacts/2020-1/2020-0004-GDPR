



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">

<head><meta http-equiv="content-type" content="text/html; charset=utf-8" /><meta name="msvalidate.01" content="B2590B06B4BEC6D2F3DB0A450D4E6870" />

    <script type="text/javascript" src="/js/jquery.js"></script>

    <script type="text/javascript" src="/js/plugins.js"></script>

    <script type="text/javascript" src="/js/jquery.cookie.js"></script>

    <script type="text/javascript" src="/js/plugins/fancy-box/lib/jquery.mousewheel-3.0.6.pack.js"></script>

    <script type="text/javascript" src="/js/plugins/fancy-box/source/jquery.fancybox.pack.js"></script>

    <script type="text/javascript" src="/child_theme/js/scripts.js"></script>

    <script type="text/javascript" src="/js/functions.js"></script>

    <script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>

	<script src="https://cdn.optimizely.com/js/317536305.js"></script>





    <!-- /block -->

    <link rel="shortcut icon" href="/templates/images/favicon.ico" />

    <!-- Stylesheets

	============================================= -->



    <link rel="stylesheet" type="text/css" href="/child_theme/css/all-ie-only.css" /><link rel="stylesheet" href="/css/bootstrap.css" type="text/css" /><link rel="stylesheet" href="/css/conflict/style.css" type="text/css" /><link rel="stylesheet" href="/css/dark.css" type="text/css" /><link rel="stylesheet" href="/css/font-icons.css" type="text/css" /><link rel="stylesheet" href="/css/animate.css" type="text/css" /><link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts.css" type="text/css" /><link rel="stylesheet" href="/child_theme/style.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts/tarzananarrowbold.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts/tarzananarrowregular.css" type="text/css" /><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" /><link rel="stylesheet" href="/css/responsive.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/responsive.css" type="text/css" /><link rel="stylesheet" href="/js/plugins/fancy-box/source/jquery.fancybox.css" type="text/css" /><meta name="viewport" content="width=device-width, initial-scale=1" />



    <!--[if lt IE 9]>

		<script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>

	<![endif]-->



    <!-- Document Title

	============================================= -->

    <title>
	Privacy Policy Statement | Touch Up Paint  | AutomotiveTouchup
</title>
    

<meta name="description" content="Privacy policy for Automotivetouchup.com, Microfinish LLC at AutomotiveTouchup at AutomotiveTouchup" /><meta name="keywords" content="Privacy Policy, Automotive Touch Up Privacy, Touch Up Paint, touch up paint, touchup paint, car paint, vehicle touch up paint, vehicle touchup paint, AutomotiveTouchup, Touch Up Paint, touch up paint, touchup paint, car paint, vehicle touch up paint, vehicle touchup paint, AutomotiveTouchup" /></head>

<body class="no-transition stretched">

    

    <form method="post" action="./privacy_policy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="q2GwIxv5dXbVUEI0VR3o4T5Pcm3QzK6UrZeAu+ZRLgwVXcl+D7g1b2qQP4RmNNcfpwcWMfq666vyEX+L2a6mkNOn8msbzka19UE7urxCelV0slZU4sxFpy1zg5/mHSOd1FLDWCRJXMl0Ivnp6SJDpgcjdQAKUNXqTimy7wHvm2blXZHaLCHNpKL7D6Vxb/y1Cg0AIzklK16WbsjcJiciIdc8JVohNR4JT/o2TjXGR/AS6RiNPmfL8AQFLDgTppItQx0ArBTl1OLUmKBMUIB9L1i05RO0q8IKkqNbYiUSigKZN2oA8im5hQW2bKfD4s60chezZQ2Z5+/MB+nhz4lDnwOkJ5Djl+p2zsLZBf3tHLf+HVNB1RIRnkpfMjhSS6NMg4XKqzjE6+8Ny6UJ0oiHwIa1BCc8Zvfj+VI29esaMatf5okYrifgTlWfzZOZjkL0QrbvLgFj+UtM1hLxurkmhu0fdzfua0u1uJz2SXQBhPBwurgWPalvyM/cG4EcJSqaoqFVGvNYuzGZlcI4MBXdlNNygq3lCV7wfBmlek2l7OUpN9XhBbxGLUgE+MlKJmYFeTLChZmr5kWjH3On8ct7i03K4sGoZQeyCkMLrBANQtqRjsBRDxqlb8bXaP3x4JsIg236Z7jwOvu7ShjCMzygo2NoBhmfYfCa4ZsQeWG/KWcqsKi+6VpJBFdTnOb/jLdupN+6qRUPVs+v7IFWSYrRwMZD3E+Xy0z6YF2uTOrz5rEdPhgW9rY5/D8repQY/A0MPwHCMac4D3sDeI24FwFE+nsM1oblQPFGaShrOYEmOC74coAu38B9f63vFToqdPIFEqrKAxPwBedfHOTOIZueZYRZ6knQfsXQhgNjatVskfgOVPuEapGH4Pk9wfdohcGGflP9Obfh46FCpYj6pWUsT7CUQQ3EDzKrTYTG+RWLgneuy0cg9XhmECcfcbZEIO5fP/K5pIINNlFKOEPYca9UnX/PcMctb+tO8oCRK4EcovEjvCYvwP93+cMnq0hjlVaN/n1nnxENmbNm2uBO8e+i3PTdrz3vy6sRBX4ld9dgyaW0SugqsSkFn2+OGrfV5QAMkXXf8q2lzTXn1A0y7baMsgvMF1gB3NIm/UCBa+HKlegqmIgphCZ0aGkOYZ0mp7N/FDZqzg07nMjgz5lTy3yRztG7gby4355h7JD++oTi7NG/kyTQpldSEQWaTbtauSZI5MvVeglzdt8LeV7dBHl2rEvBZCDTUVt2LdSoeNpXp0XHdZ0XErpKAbqgBYAIslhc/Wbb1Sj3yizUdV9xnP3e0ZwLSBRGhncUUgSjHtDwtHXjDxJkIhF0eZRqiYb21hl+2o0hXfW5EPGfk5DK4qgaI7WefjAl2Lzsaja97iPqLGLOGJWnViKut92lUZ77tDmhcgxr+qxD+4MyY6BZc9ZaUcEYK07Ch8FFb6xsu0MhpFOgJ+Jf3ml659TlX8zohYozld+uudbxDnhb8UXS4fcKPUHg/taTfiJZ0BZzohSZ9Y8d96XpPoDCm76GA+fZUCgGJk8310ucsrFueBe8fsuQNAVsvup8z5XxktwANbN/QIahC1YJb2OUU+NZvLQbtvnjGE1vRqWozMIRvy2byKM8agKP/z6AR/MszHs661AY0vvxdgJEWApQ0HazQ09nyAPeXK3jaaknHYRwkNeEpTbp5XTqmW7yqd/TUrnYxKG+5ng08Wv7LV7cshIH3fOKDJx/InzzcqpAHwGKeuHfNTdLoCQFZ3hBDgJpfDV2Y48hldcbZ96XItcFoGBOnQ2LVqwhLQZu1p8R4L5ILvE8dLEsTN35NUY5SgFJXKbDDsv19uuw5241kxYS6SG8FE8j+XXwY5jEWhb5ohOyCkSr0Me+eXEQoT4sfqNNyS/mm7cIwGY89s9SKyZPgybXR0gm3mPhfFV/l+k9Udu56fIw6kT0ezBoJzr3Rw1vr9EAFGYqpRwaYOC8z7Kf5ML7C3117bOmItp7GpCXs/unLeCR3OIGi03rrLS/5aqG3jy3kcQX9dhiCHrKAwPKNX7CvTSFSnXj2WtxylkxPFws7GXJ8wbgXpTUnStioAODlh24v1ru1eLLA9Bb/CJhFoZoGsyesPj1lSNDHI1ou3D9fACYsDMuFxI00q6Y2/zRZehrrPbEEWwiX+13yEd1B4N3W7uyJjfNx32p7pzJwLgZ71O3CioJeeAg+LjNDtAVtwp4qoGNHZBwoE192QUEQuaryOD29mEwIFZKIbt2LAiUD5Fzm8WzfQQd9uypq+VGHQqZH+IXHSY8cQDgghLKRDKbYZDAwlEIKqWyC8WzgUCs3XilARzKZxFN/D1xHtvW7ZM/IA6GiH0wnFCoFUYi5wDFlWes6r/HuEtdhjPFJvaB0qAfah5XMYSRUF7OEWWCqzbE+x1tiPI9WaFZ4WCiV8w3lTTzJL9h2b2SSWQE7WE76UVj8So071IWr3jZEjZojg0lsdmnujvUnslMJcF64ckIgeaZFM00tV2C/BCxiNGlPv8l8j10d9C03i2TVk18t4o6tmHzGT7zChWem1EojLydlZexRMjkrCVksRPOW3CGzjhaEcQ8A6Fb+pFEehSMsOamd51T3Asv1OklVUaRnGMIibPJ9pLi5BGi7c0B5sYxucXlehba1Pi7KFXtcww+u8NjKrKkimHUg6RIuS6xizK+K+UmqNtwy6FmplvrRaxc7yWt4+2wgYNKY8EqmOxCeMIf7T4yH4lkYTdGmfJXo/9lADMhM9kOoE2JDU57M1iPe+mf7sxhJ4ZYnJGA/PA0nq0ES0ENIrwY3AfIvPNluKB57U/rTFHo8mRc0YFkju+CWW8xFCTPN/XIzaov1XMKndrJkg129Yp8boKDkxGEjc4p5vktsrasRixhkH00q5XnlSyx+XGrFvblTQZ2A1qoLFRjQ7aMyeMjEJLSCqzZcc+WeMj1HHDRaH4rSCw5UTMcs1+DwyB4bLjoHCZ1V+5sXUiiqhtJKxhUvXWpHBYdOGRKL9X5XZQVlnbk3Vt3c8g7ywzlNMa5c7JdAGanoiStkrKc+76GldOjObYVZ/aOTrEaia30vlMvYsMrZuKwLyHvqw3cNyQVgfrnUtSzNpMvAu0qJRCYshL/2KhIU4WlascQX4Jyuy9hsyfxSQd2YHSpKzcomzDDpdHAxNWb8BCQHMbhDMYrxj3NhStDIAwHMwFRFSOl4vKIhRBeUK0tDe8mV5jjwFFyjaLe+FxsPxRmtcBroOgCsv2ha2VZeadPD9nUozZlsBgbO4rXTaKPOQQrp2iAalKJyeYmg+t3yiwaGO1qGv6DQVe5WLVLKWkjyBlJ9J4PgEyFZsqFNBrcdVJvRyMhgGR8J4nOaT163T/S47susdpgyV0pW0tZ+di7ic+NlrWj4YYGG5dlnIJV7575mdJC6fBzR/m8jc4rKQgRnhibFrCCzpIztDJO2GQeuCsOZXaTxx4SHG6uVxyvH4JGwMDl2v7IaeJ2cUMzw4mF4IaNPkQ2DhDyBcCUciYBhwzWu8s5kOc/vwe/Cnwni5F0P9HidzQWuCCL3Q3V/AjLXyCXuxWAQpshePr5RIW2+SzvQDae0kYgtTPyhNH4AuhwknlOgVJkXI90Q+NuG1x4svCKqZLpC6eBFDqsR9H1oIYY30400wno81h7ePS87XJF0iNEZ+41HaW9DGIbzGibhlFurrrJjrIFe/5AMSzpQ/dk1aLGhGY7QDSctSzQqI3SnFgiiORgBaKDRjClECtySRmPAqaI2Jo=" />


<script src="/js/outermaster_external.js" type="text/javascript"></script>
<script src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yGe-MgZ6_6pvcmjBkCQMNS8-RyI640njGgMPV5RliEowHeuNCdPy0CKFe_CVqjeuPEUK-hDvdpNNyoaq0z-lCUADdhHv7eusPZaq_xADHMAocgoC7y0BxM6hg5mZQCr3LzDoER7ofeJIAI-RcznGFfI1&amp;t=72fc8ae3" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2927320D" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="qeaqwRFZnvbdQUtwYejWk3RT5cmNVdPZVa3Kk74EVw4AbUrY2cn6MYkSiEIPnYmDCe5GJQ4wub75A/0+ndUWkvEhayVxJ5syXsYoBnLU4D7PR1VZLcCv7pEFLMPsjOps1PqHVwRT7jL/b31EK+FKDmxy0E1y1hSfyHGSBhvFreC1dW400jowbHMzUesMNFQ2BkVFkv/ZsMBMgrOyYesv5/bGEpvsN6mUXHC5oSh4jIZjduvbWzoenTK+x9pTIQ01mSjLZqQPZygTz6oCsLVki6uYBwdfJVnt5oMIdrqubpEf+yOziRE6D0+DUsV3Z3wO2/LehblQRQ8/bbXQbr4clHrrvh2PlTfa1yi/y0dTOcejBKdR1Z8AkjLeMmk1w1QBo4/vn9Ta2ePUKABt/1NwQtqX2kcBd4TnWJhA6Ezj6CTvFJa7iCjx1oqO3+1xdsi34EY/FuGuAffedNMxyR/l6W6N5Y2METdIHGKDAL9Yd9PVxzZTFXlKGLrdHNKFAkgCzN7BgtOQt1iJozmOgH/gqM57joXNXNz0yAXuWyU1AJcOzFKdAkw0p44C0IqF1sZ7SBMBHFskzbuOSD5Mj86Tvkl/6o/Zpn1qIJLpuawx/KhZ3kiecQxhnbo5B3Yao6iUpm6CZ4EGgHIYWYGljdA/60yyl8po64zqsRn0wIudCDh2CPvVMfGxtc4HcvNQ3t4lNz89d74o3A2obFvIc1Py1DaHArezmHhH3h9dYOWAOpKFyvNsWUr3KRSKJjFECPXqSqjd9aRSm+/nC7dWua6VVJyB2vUrxcC+ee41SfjUH4kmt9ue6nGube2YGlqYwU0tVCKws4Yh6bebJNEgLa3z2OsVWhKym+pI7huASvahPbiPJO21Y7iAkEY2tfNzhQEJ2EdAe6dsn4Gr9iq+GWYeWGH/rzwLYBa8YkuBBjb1vKhkneI+pSd8p/m3OVQ0/dpIauTtIXbb7ARUnq+rWPleRNrvS71OvktQoo1RMWGbW3xEXlBeFOderRAy6k5egz7ocoxIpMnmn/b8Dxfgs8zST0D5NKCLLEzqrKlu5lEHI68CtjySho8P4EMN7g188EuhGcjOT9LNwa0UgFqGEEcmxw9Qw+ki8h2NOD7tLRDraFEjc4LOLXCxhYmpYITQNC383by/kuW/C58mKof25vzK0T2gEGPedMuDRzFZC5dhFg5594sMWsU113oNpiZ70Jj+rL81zCATLMfaGo8ImgAQwDqibRoEgDIDsOhw73XOZuFY0q06us1ssRkWfyFImZlcKItpzFrvAXtkPfzN+TJbxmlGHOuLkBO3S0sLy1AFSTAO74gGKutkog9d2kIv4AtsJWij0asLK70h/1IyWYYJlulHtWuZ4oaQbQawbXjWduyeg9QIPTh5/xS81BY2zAJrYn9nEaiK7f1qbkC4DSpMd6wDJco/iQcaLOzsqUlGwQ8bSCPSwusHN3+9pLRuq+a6fEkvVMWjTKx9OvGWoOTlR3lEDvPeEwdxPjeWW9ebsq6XnXdc+XCl90S9uAVSp3Hf7XdMK6CHMquicMx/+LAkkRSApx2zdjXRpa8qOKNxSjXFJ50R/MO+6yucdWLgj9vQOTmmMJ+r6MBvSS5C+vmWIV7znVvu9u9Ko/ZVgBoNbrrWfc2tbXDHDWN4ljHRXNalmzek/a/zY/3OQn0hK8Gvenml9S6vbo4fptCP+dNu5QGnLpqZp72hZb+B2MvYCAwDTY9NkevaSYivKOgY+b+SwhRHhhNhSnTWVclkFLhQJNkwqGGl+gHe/BKuIQhGKGjabPTJKHhAv9v+egggCI/Z7WB83TZtI+iJ5P/yYefY9PlQ9xkQoh7iOqao38ghHF2+XKrZaLJlT8xwOYc9gw1wofRI+UPRbB4mMkCkdaIkn/bNIxuZg8Hpvuoa+5hCuEZt497TrkiZveZfaRMtTUfGcEQ3HxjK40oVc4WnhKbVEtaijMWtauA/9M8/KEdsOPYXnU20OFeWFIA0+cqsJ3JbU+E7AfKkpK//M6gujQmpfZ3mWHz67d643O7GU1n8M2S9j/YJ0vAiCsIva+xXI0eHFijepOG7h41IrbmjjNQoXEotx5PVaXHwt2ETFYxZM3e/jQ2NwFZDyWwoCUiPn6a07X+G+kZ1oARrRzk5bTM9zL8HyZpwSZKA7eykRhQ44DgkH9JI5jcWnWpIqiFSEmfNcrjdJ20HTB7CvCZsA6K1lQapfgbe1AyQYD11FDxXTrlwvfOG7ZSDCOcOj21BezZyU/Sh7nxYJAMTPOR2oX6lCRO7CZUEpSqg9E7AR4snvjCS53UVU9RqAFalliavZWrRqbQrkSaMV1nwVjrioe9FF4gsNn36t067qVbcduQZU0/0wGyYabx6acrSkO3SpvM/TAuYIJBEieX/alI5uhHLrFTNE2Ixil2Pdccdod4R1EGue5BMLtZPU/SLu1iw2vADwWTg2DJ7IHK+gInXXbciEdN80nA2xRO9Og6tdRhhjZgMCQ7Lj+c7nCa9wv5Z/o+pg78mGI9IY1cj7IZGRetxMJ4O8YhR990AdL0M83C1/7Vw0zW6m7zRcnSXLceb3vfof2QrZphGfMBMJPxr35/PoCIFIDIATA2/NlDNguyF3brr0xrshYFkzD/8JFVU2Fqr2PxdWZECNqGiDIsHxLneyjYbIw9Ncf2jRWa6m6Phzv7zI9vOE/+g+LmIrZDcYF4Ac5x9vUsXTL/eYeBLlQbVIAtqUO0rP/c/r0poUUNwmdtMwVCLYSvCTZMX1gDz+LpflikjquSBrBa+LhmyTgVvgJyXg23FVSJrIohaB04ttv1VsjZiZ6CPeOxV8A5sBRGeG2pE+fOm1GTuQVDKG3uAiGkN44TVhMHxLkzcnOSXsaOCCqo3PE1VoF2KuibyqtGptnncTgfLBLNf47CtTs7qEl8sgIAAQf7ZBdcMmS2QvZsrjTQ7M3PZuBSPks/s9wx8Bbmm6eboYR/7ZwoK3zSoVkIkJ8cA1zmMEQKVpkK/jj28WSHhXNzW9Yb9nmL5qzoOtW2JY8bPngNJ04Kk8l4Id2Hc/DI/6DVn6WDCPpORkhJ899ShC9P89y82gkGS+yU9N+8TDezQ2cJR5a55D98LYlnCAytR85C+ydvEj3CtppfqGE1+BJhN3qMmtWjOQLdHbrgl1yMmGbKgdn4AnRXEIee44kbwV520oB/hr1EzRU7wZG1i8Cc/iVG7roWPgeGtyaWnZYn7Z5GoSlhiAhYF4zB99/f9SsHpliZFgrhNkZcpE0hmgB2P8gUaCWARNft4BnMF0fzmDPSwdhwfUK+1R2YkfBi5LinE3rH9O6QKqOxH7dLmSgiKFNt9qscwL+i+AlaN97zWzsw0GZPp2K3OD6Nmj9g5wyymGUGtEXzGfZnWMx8zhc6h1paGys6lXGh/qVaMA3r4BMjkayecJ3eb/GQrMo7leJAXWrZVHSRf31CDiRShgQluSi88EELbgn6s5X8pDTz8Sfq8PcdaKDL494U4x7zCyHCEYx79POVAMFfB0VmpyERUslQi5uK5l26eaxUeDVAargraaZ7sSu0/jdCAFMJmxmtDFqxiXYTRpA9lmwyUsYjWlHp5KsKmcw57DdDpk25igCNaxv/q7cH/rTmZ4vp/S0rRDanUvUAswcjZ+G4mRTJ1xKKtfFJBhFo6oQJaRS8GYanHuXVZ0tZ40ztP8mT7ZrcShmm/z1WXx/TNpcanVzgjSVTOzEDliAaCG4VuYCnWXyijfMhdFsNkzPU=" />



        



        <!-- Document Wrapper

	============================================= -->

        <div id="wrapper" class="clearfix">



            <div class="top-bar">

                <div class="container">

                    <div class="row">



                        <!-- Logo

					============================================= -->

                        <div id="logo">

                            <a href="http://www.automotivetouchup.com/" class="standard-logo" data-dark-logo="/child_theme/images/logo.png">

                                <img width="238" src="/child_theme/images/logo.png" alt="Logo" /></a>

                            <a href="http://www.automotivetouchup.com/" class="retina-logo" data-dark-logo="/child_theme/images/logo.png">

                                <img width="238" src="/child_theme/images/logo.png" alt="Logo" /></a>

                        </div>

                        <!-- #logo end -->





                        <div class="col-xs-12 text-right">

                            <ul class="list-inline social-links">

                              <li><a href="/contact-us.aspx">CONTACT</a></li>

                              <li>|</li>

                              <li><strong><a href="tel:1-888-710-5192">1-888-710-5192</a></strong></li>

                              <li>|</li>

                              <li><a href="/check.aspx">ORDER STATUS</a></li>

                            </ul>

                        </div>

                    </div>

                </div>

            </div>





            <!-- Header

		============================================= -->

            <header id="header" class="transparent-header dark no-sticky">



                <div id="header-wrap" style="background-color: #455660;">



                    <div class="container clearfix dark">



                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>





                        <!-- Primary Navigation

					============================================= -->

                        <nav id="primary-menu">

                            <div class="top-search-content">

                                <div>
	

                                    <input name="ctl00$ctl00$SearchTextBox" type="text" id="ctl00_ctl00_SearchTextBox" class="form-control" placeholder="Type &amp; Hit Enter.." />

                                    

                                    <input type="submit" name="ctl00$ctl00$TopSearchImageButton" value="" id="TopSearchImageButton" style="display: none" />

                                
</div>

                            </div>

                            <ul>

                                <li class="mega-menu">

                                    <a href="/touch-up-paint/">

                                        <div>Choose Your Make <span class="twosep">:</span></div>

                                    </a>

                                   <div class="mega-menu-content myFirstMegaMenu style-2 clearfix">

										<ul class="mega-menu-column col-md-3">



										<li><a href="/touch-up-paint/acura/">Acura</a></li>

                                        <li><a href="/touch-up-paint/audi/">Audi</a></li>

                                        <li><a href="/touch-up-paint/bmw/">BMW</a></li>

                                        <li><a href="/touch-up-paint/buick/">Buick</a></li>

                                        <li><a href="/touch-up-paint/cadillac/">Cadillac</a></li>

                                        <li><a href="/touch-up-paint/chevrolet/">Chevrolet</a></li>

                                        <li><a href="/touch-up-paint/chrysler/">Chrysler</a></li>

                                        <li><a href="/touch-up-paint/dodge/">Dodge</a></li>

                                        <li><a href="/touch-up-paint/ford/">Ford</a></li>

										</ul>

										<ul class="mega-menu-column col-md-3">

										<li><a href="/touch-up-paint/gmc/">GMC</a></li>

                                        <li><a href="/touch-up-paint/honda/">Honda</a></li>

                                        <li><a href="/touch-up-paint/hummer/">Hummer</a></li>

                                        <li><a href="/touch-up-paint/hyundai/">Hyundai</a></li>

                                        <li><a href="/touch-up-paint/infiniti/">Infiniti</a></li>

                                        <li><a href="/touch-up-paint/jaguar/">Jaguar</a></li>

                                        <li><a href="/touch-up-paint/jeep/">Jeep</a></li>

                                        <li><a href="/touch-up-paint/kia/">Kia</a></li>

                                        <li><a href="/touch-up-paint/lexus/">Lexus</a></li>

										</ul>

										<ul class="mega-menu-column col-md-3">

										<li><a href="/touch-up-paint/land-rover/">Land Rover</a></li>

										<li><a href="/touch-up-paint/lincoln/">Lincoln</a></li>

                                        <li><a href="/touch-up-paint/mazda/">Mazda</a></li>

                                        <li><a href="/touch-up-paint/mercedes-benz/">Mercedes-Benz</a></li>

                                        <li><a href="/touch-up-paint/mini/">Mini</a></li>

                                        <li><a href="/touch-up-paint/mitsubishi/">Mitsubishi</a></li>

                                        <li><a href="/touch-up-paint/nissan/">Nissan</a></li>

                                        <li><a href="/touch-up-paint/pontiac/">Pontiac</a></li>

                                        <li><a href="/touch-up-paint/porsche/">Porsche</a></li>

										</ul>

										<ul class="mega-menu-column col-md-3">

										<li><a href="/touch-up-paint/saab/">Saab</a></li>

										<li><a href="/touch-up-paint/saturn/">Saturn</a></li>

                                        <li><a href="/touch-up-paint/scion/">Scion</a></li>

                                        <li><a href="/touch-up-paint/smart/">Smart</a></li>

                                        <li><a href="/touch-up-paint/toyota/">Toyota</a></li>

                                        <li><a href="/touch-up-paint/volkswagen/">Volkswagen</a></li>

                                        <li><a href="/touch-up-paint/volvo/">Volvo</a></li>

                                        <li><a href="/touch-up-paint/">Don't see your make?</a></li>

										</ul>

									</div>

                                </li>





                                <li class="mega-menu"><a href="/paint-code.htm">

                                    <div>Paint Codes <span class="twosep">:</span></div>

                                </a></li>



                                <li class="mega-menu col-4-menu">

                                   <a href='http://www.automotivetouchup.com/products.aspx'>

                                        <div>Products <span class="twosep">:</span></div>

                                    </a>

                                    <ul style="display: none">



                                        <li><a href='http://www.automotivetouchup.com/touch_up_paint.asp'>1/2oz Paint Bottles</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/touch_up_paint_matched.aspx'>1/2oz Basecoat</a></li>

												<li><a href='http://www.automotivetouchup.com/touch_up_paint_clearcoat.aspx'>1/2oz Clearcoat</a></li>

												<li><a href='http://www.automotivetouchup.com/touch_up_primer.aspx'>1/2oz Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>1/2oz Accessories</a></li>

											</ul>

										</li>

										<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/index.aspx'>2oz Paint Bottles</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_basecoat.aspx'>2oz Basecoat</a></li>

												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_clearcoat.aspx'>2oz Clearcoat </a></li>

												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_primer.aspx'>2oz Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>2oz Accessories</a></li>

											</ul>

										</li>

										<li><a href='http://www.automotivetouchup.com/paintpen.asp'>Paint Pens</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_matched.aspx'>Paint Pen Basecoat</a></li>

												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_clearcoat.aspx'>Paint Pen Clearcoat</a></li>

												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_primer.aspx'>Paint Pen Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Paint Pen Accessories</a></li>

											</ul>

										</li>

										<li><a href='http://www.automotivetouchup.com/spray_paint.asp'>Aerosol Spray Paint</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/spray_paint_matched.aspx'>Aerosol Basecoat</a></li>

												<li><a href='http://www.automotivetouchup.com/spray_paint_clearcoat.aspx'>Aerosol Clearcoat</a></li>

												<li><a href='http://www.automotivetouchup.com/spray_paint_primer.aspx'>Aerosol Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/spray_paint_accessories.aspx'>Aerosol Accessories</a></li>

												<li><a href='http://www.automotivetouchup.com/spray_paint_trim.aspx'>Black Trim Aerosol</a></li>

												<li><a href='http://www.automotivetouchup.com/spray-paint/adhesion-promoter.aspx'>Adhesion Promoter</a></li>

											</ul>

										</li>

										<li><a href='http://www.automotivetouchup.com/auto_paint.asp'>Spray Gun Paint</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/auto_paint_basecoat.aspx'>Spray Gun Basecoat</a></li>

												<li><a href='http://www.automotivetouchup.com/auto_paint_clearcoat.aspx'>Spray Gun Clearcoat</a></li>

												<li><a href='http://www.automotivetouchup.com/auto_paint_primer.aspx'>Spray Gun Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/auto_paint_accessories.aspx'>Accessories</a></li>

												<li><a href='http://www.automotivetouchup.com/spray-guns/all-spray-guns.aspx'>Spray Guns</a></li>

											</ul>

										</li>

										<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Accessories</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Touch Up Accessories</a></li>

												<li><a href='http://www.automotivetouchup.com/spray_paint_accessories.aspx'>Aerosol Accessories</a></li>

												<li><a href='http://www.automotivetouchup.com/auto_paint_accessories.aspx'>Auto Paint Accessories</a></li>

												<li><a href='http://www.automotivetouchup.com/spray-guns/all-spray-guns.aspx'>Spray Guns</a></li>

												<li><a href='http://www.automotivetouchup.com/store/accessories/prep_solvent.aspx'>Prep Solvent</a></li>

												<li><a href='http://www.automotivetouchup.com/store/accessories/rubbing_compound.aspx'>Rubbing Compound</a></li>

												<li><a href='http://www.automotivetouchup.com/store/accessories/surface-wipe.aspx'>Prep Wipe</a></li>

											</ul>

										</li>

										<li><a href='http://www.automotivetouchup.com/bodyandbumper.aspx'>Body & Bumper</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/bodyandbumper.aspx'>Body & Bumper Repair</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bondo.aspx'>Bondo Body Filler</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/ultimate_bondo.aspx'>Ultimate Body Filler</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bondo-professional-gold-filler.aspx'>Gold Body Filler</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/spot_putty.aspx'>Glazing and Spot Putty</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bumper_repair_kit.aspx'>Syringe Bumper Repair</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/flexible_part_bumper_repair.aspx'>Easy Finish Part Repair</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/flexible_part_bumper_repair.aspx'>Easy Finish Bumper </a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/plastic_spreaders.aspx'>Plastic Spreaders</a></li>

												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper.aspx'>Sandpaper</a></li>

												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper-multi-pack.aspx'>Sandpaper Multi-Pack</a></li>



												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/file_holder.aspx'>3 Position File Holder</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/sanding_block.aspx'>Rubber Sanding Block</a></li>

											</ul>

										</li>

										<li><a href='http://www.automotivetouchup.com/sandpaper/general-abrasives.aspx'>Sandpaper</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper.aspx'>Wet Sandpaper</a></li>

												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper-multi-pack.aspx'>Assorted Sanding Pack</a></li>

												<li><a href='http://www.automotivetouchup.com/store/accessories/touch_up_sandpaper.aspx'>Mini Sandpaper</a></li>

												<li><a href='http://www.automotivetouchup.com/sandpaper/scuff-pad.aspx'>Scuff Pad</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/sanding_block.aspx'>Rubber Sanding Block</a></li>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/file_holder.aspx'>3 Position File Holder</a></li>

											</ul>

										</li>

										<li><a href='http://www.automotivetouchup.com/safety.aspx'>Safety</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/dust_mask.aspx'>Dust Masks</a></li>

												<li><a href='http://www.automotivetouchup.com/safety/respirator.aspx'>SAS Bandit Respirator</a></li>

												<li><a href='http://www.automotivetouchup.com/store/accessories/respirator.aspx'>3M Respirator</a></li>

												<li><a href='http://www.automotivetouchup.com/safety/gloves.aspx'>Nitrile Gloves</a></li>

												<li><a href='http://www.automotivetouchup.com/safety/glasses.aspx'>3M Eye Protection</a></li>

											</ul>

										</li>

										<li><a href="#">Basecoat</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/touch_up_paint_matched.aspx'>1/2oz Brush-In-Bottle </a></li>

												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_basecoat.aspx'>2oz Brush-In-Bottle </a></li>

												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_matched.aspx'>Paint Pen Basecoat</a></li>

												<li><a href='http://www.automotivetouchup.com/spray_paint_matched.aspx'>Aerosol Spray Basecoat</a></li>

												<li><a href='http://www.automotivetouchup.com/auto_paint_basecoat.aspx'>Paint for Spray Guns </a></li>

											</ul>

										</li>

										<li><a href="#">Clearcoat</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/touch_up_paint_clearcoat.aspx'>1/2oz Clearcoat </a></li>

												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_clearcoat.aspx'>2oz Clearcoat </a></li>

												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_clearcoat.aspx'>Clearcoat Paint Pen</a></li>

												<li><a href='http://www.automotivetouchup.com/spray_paint_clearcoat.aspx'>Clearcoat Aerosol</a></li>

												<li><a href='http://www.automotivetouchup.com/auto_paint_clearcoat.aspx'>Spray Gun Clearcoat </a></li>

												<li><a href='http://www.automotivetouchup.com/lacquer_clearcoat.aspx'>Aerosol Lacquer</a></li>

												<li><a href='http://www.automotivetouchup.com/spray-paint/spraymax-2k-clearcoat.aspx'>Aerosol Urethane</a></li>

												<li><a href='http://www.automotivetouchup.com/lacquer_clearcoat.aspx'>Lacquer Qt/Gal </a></li>

												<li><a href='http://www.automotivetouchup.com/urethane_clearcoat.aspx'>Urethane Qt/Gal</a></li>

											</ul>

										</li>

										<li><a href="#">Primer</a>

											<ul>

												<li><a href='http://www.automotivetouchup.com/touch_up_primer.aspx'>1/2oz Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_primer.aspx'>2oz Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_primer.aspx'>Primer Paint Pen</a></li>

												<li><a href='http://www.automotivetouchup.com/spray_paint_primer.aspx'>Aerosol Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/auto_paint_primer.aspx'>Spray Gun Paint Primer</a></li>

												<li><a href='http://www.automotivetouchup.com/lacquer_primer.aspx'>Laquer Qt/Gal</a></li>

												<li><a href='http://www.automotivetouchup.com/urethane_primer.aspx'>Urethane Qt/Gal</a></li>

											</ul>

										</li>







                                    </ul>

                                </li>



                                <li class="mega-menu"><a href="/how-to-videos/">

                                    <div>How To Videos <span class="twosep">:</span></div>

                                </a></li>



                                <li class="mega-menu"><a href="/directions.htm">

                                    <div>Directions <span class="twosep">:</span></div>

                                </a></li>



                                <li class="mega-menu"><a href="/gallery.htm">

                                    <div>Results <span class="twosep">:</span></div>

                                </a></li>

                                <li class="mega-menu"><a href="/feedback.aspx">

                                    <div>Reviews <span class="twosep">:</span></div>

                                </a></li>

                                <li class="mega-menu"><a href="/about-us.aspx">

                                    <div>About ATU <span class="twosep"></span></div>

                                </a></li>



								<li class="extraSpace"></li>







                            </ul>



                            <!-- Top Cart

						============================================= -->

                            <div id="top-cart">



                                <a id="ctl00_ctl00_lnkCart1" rel="nofollow" href="https://www.automotivetouchup.com/cgi-bin/ShoppingCart.aspx"><div id="ctl00_ctl00_CartUpdatePanel">
	

                                            <img src="/child_theme/images/cart.png" alt="cart-icon"><span id="ctl00_ctl00_NumItemslabel">0</span>

                                        
</div></a>





                            </div>

                            <!-- #top-cart end -->

                            <!-- search Bar

							============================================= -->

                            <div id="top-search">

                                <a href="#"></a>

                            </div>

                            <!-- search Barend -->

                        </nav>

                        <!-- #primary-menu end -->



                    </div>



                </div>







            </header>

            <!-- #header end -->



            
    
    <!-- Slider -->
    <section id="slider" class="clearfix page">
        <div class="main-slide" style="background-repeat: no-repeat;">
            <div class="container clearfix">
                <div class="row-fluid">
                    <div class="col-xs-6 col-sm-6 col-md-6"></div>
                    <div class="maincaptions col-xs-12">
                        <h1 data-caption-animate="fadeInUp" class="fadeInUp animated">PRIVACY POLICY</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Content
		============================================= -->
    <section id="form" class="clearfix">

        <div class="container clearfix">
            <div class="row clearfix">
				 <div class="col-xs-12 col-sm-9 col-sm-push-3">
					<div class="col-xs-12 text-right page-breadcrumbs"><a href="/">HOME</a> >

                
		<!-- div continued from developSiderbar -->
             PRIVACY POLICY </div>
        <div class="col-xs-12">
            <div id="subpage">
        
        <p>
        AutomotiveTouchup.com belongs to Microfinish LLC.
        </p>
        <p>
Our postal address is
        </p>
        <p>
Microfinish LLC<br>
208 Plauche Ct.<br>
New Orleans, LA 70123<br>
We can be reached via e-mail at web@automotivetouchup.com<br>
or you can reach us toll free by phone at 1-888-710-5192
        </p>
        <p>
For each visitor to our Web page, our Web server automatically recognizes no information regarding the domain or e-mail address (It may recognize an IP address or a referring domain).
        </p>
        <p>
We collect no information on consumers who browse our Website.
        </p>
        <p>
The information we collect is used for internal review and is then discarded, not shared with other organizations for commercial purposes.
        </p>
        <p>
With respect to cookies: We use cookies to record session information, such as items that consumers add to their shopping cart.
        </p>
        <p>
If you do not want to receive e-mail from us in the future, please let us know by sending us e-mail at the above address, writing to us at the above address. We do not use email as a form of marketing.
        </p>
        <p>
If you supply us with your postal address on-line you will only receive the information for which you provided us your address. Persons who supply us with their telephone numbers on-line will only receive telephone contact from us with information regarding orders they have placed on-line.
        </p>
        <p>
With respect to Ad Servers: We do not partner with or have special relationships with any ad server companies.
        </p>
        <p>
Upon request we provide site visitors with access to transaction information (e.g., dates on which customers made purchases, amounts and types of purchases) that we maintain about them, a description of information that we maintain about them.
        </p>
        <p>
Upon request we offer visitors the ability to have inaccuracies corrected in contact information.
Consumers can have this information corrected by sending us e-mail at the above address, writing to us at the above address.
        </p>
        <p>
With respect to security: We always use industry-standard encryption technologies when transferring and receiving consumer data exchanged with our site, We have appropriate security measures in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.
        </p>
        <p>
If you feel that this site is not following its stated information policy, you may contact us at the above addresses or phone number.
        </p>
        <p>
We appreciate your comments and choose to spotlight some of our customer comments as testimonials. If your comment is selected to appear on our site, we will use only your first name, last initial, location, car/paint info, and your comment. When <a href="/feedback.aspx">submitting your comment</a>, you will have the option to opt out of having your review appear on our site.
        </p>
    </div>
           
       <!-- two more closing divs before the sidebar code starts -->



					</div>
				</div>
                <!-- sidebar starts here -->

                <div class="col-xs-12 col-sm-3 col-sm-pull-9 secondary-nav">
                    <span class="visible-xs hidden-sm hidden-md hidden-lg"><a class="nav-drop" data-toggle="collapse" href="#navCollapse" aria-expanded="false" aria-controls="navCollapse">MORE PRODUCTS &#8681;
                    </a></span>

                    <div class="collapse left-sidebar" id="navCollapse">

						<div class="mySidebarSearch">
						<h4>FIND YOUR PERFECT COLOR MATCH</h4>
						<p style="margin-bottom:0!important;">Enter your year, make, and model below to find color matched paint:</p>
						<div id="ctl00_ctl00_OuterBodyContentPlaceHolder_ColorSearchPanel" class="sidebarsection">
	
                        <div id="sidebarselectleft">
                            <span id="step1">STEP 1:</span> <span id="step2">STEP 2:</span> <span id="step3">STEP
                            3:</span>
                        </div>
                        <div id="sidebarselectright">
                            <a id="RefreshButton" href="javascript:__doPostBack(&#39;ctl00$ctl00$OuterBodyContentPlaceHolder$RefreshButton&#39;,&#39;&#39;)" style="display: none"></a>
                            <div id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterSearchUpdatePanel">
		
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterYearDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList">
			<option value="">--Year--</option>
			<option value="2019">2019</option>
			<option value="2018">2018</option>
			<option value="2017">2017</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option selected="selected" value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009">2009</option>
			<option value="2008">2008</option>
			<option value="2007">2007</option>
			<option value="2006">2006</option>
			<option value="2005">2005</option>
			<option value="2004">2004</option>
			<option value="2003">2003</option>
			<option value="2002">2002</option>
			<option value="2001">2001</option>
			<option value="2000">2000</option>
			<option value="1999">1999</option>
			<option value="1998">1998</option>
			<option value="1997">1997</option>
			<option value="1996">1996</option>
			<option value="1995">1995</option>
			<option value="1994">1994</option>
			<option value="1993">1993</option>
			<option value="1992">1992</option>
			<option value="1991">1991</option>
			<option value="1990">1990</option>
			<option value="1989">1989</option>
			<option value="1988">1988</option>
			<option value="1987">1987</option>
			<option value="1986">1986</option>
			<option value="1985">1985</option>
			<option value="1984">1984</option>
			<option value="1983">1983</option>
			<option value="1982">1982</option>
			<option value="1981">1981</option>
			<option value="1980">1980</option>
			<option value="1979">1979</option>
			<option value="1978">1978</option>
			<option value="1977">1977</option>
			<option value="1976">1976</option>
			<option value="1975">1975</option>
			<option value="1974">1974</option>
			<option value="1973">1973</option>
			<option value="1972">1972</option>
			<option value="1971">1971</option>
			<option value="1970">1970</option>
			<option value="1969">1969</option>
			<option value="1968">1968</option>
			<option value="1967">1967</option>
			<option value="1966">1966</option>
			<option value="1965">1965</option>
			<option value="1964">1964</option>
			<option value="1963">1963</option>
			<option value="1962">1962</option>
			<option value="1961">1961</option>
			<option value="1960">1960</option>
			<option value="1959">1959</option>
			<option value="1958">1958</option>
			<option value="1957">1957</option>
			<option value="1956">1956</option>
			<option value="1955">1955</option>
			<option value="1954">1954</option>
			<option value="1953">1953</option>
			<option value="1952">1952</option>
			<option value="1951">1951</option>
			<option value="1950">1950</option>
			<option value="1949">1949</option>
			<option value="1948">1948</option>
			<option value="1947">1947</option>
			<option value="1946">1946</option>
			<option value="1945">1945</option>
			<option value="1944">1944</option>
			<option value="1943">1943</option>
			<option value="1942">1942</option>
			<option value="1941">1941</option>
			<option value="1940">1940</option>
			<option value="1939">1939</option>
			<option value="1938">1938</option>
			<option value="1935">1935</option>

		</select>
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterMakeDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList">
			<option value="">--Make--</option>
			<option value="Acura">Acura</option>
			<option value="Alfa Romeo">Alfa Romeo</option>
			<option value="Aston Martin">Aston Martin</option>
			<option value="Audi">Audi</option>
			<option value="Bentley">Bentley</option>
			<option value="BMW">BMW</option>
			<option value="Buick">Buick</option>
			<option value="Cadillac">Cadillac</option>
			<option value="Chevrolet">Chevrolet</option>
			<option value="Chrysler">Chrysler</option>
			<option value="Daewoo">Daewoo</option>
			<option value="Daihatsu">Daihatsu</option>
			<option value="Dodge">Dodge</option>
			<option value="Ferrari">Ferrari</option>
			<option value="Fiat">Fiat</option>
			<option value="Fisker Automotive">Fisker Automotive</option>
			<option value="Ford">Ford</option>
			<option value="GMC">GMC</option>
			<option value="Harley Davidson">Harley Davidson</option>
			<option value="Holiday Rambler">Holiday Rambler</option>
			<option value="Honda">Honda</option>
			<option value="Hyundai">Hyundai</option>
			<option value="Infiniti">Infiniti</option>
			<option value="Isuzu">Isuzu</option>
			<option value="Jaguar">Jaguar</option>
			<option value="Jeep">Jeep</option>
			<option value="Kia">Kia</option>
			<option value="Lamborghini">Lamborghini</option>
			<option value="Land Rover">Land Rover</option>
			<option value="Lexus">Lexus</option>
			<option value="Lincoln">Lincoln</option>
			<option value="Lotus">Lotus</option>
			<option value="Maserati">Maserati</option>
			<option value="Maybach">Maybach</option>
			<option value="Mazda">Mazda</option>
			<option selected="selected" value="Mercedes-Benz">Mercedes-Benz</option>
			<option value="Mercury">Mercury</option>
			<option value="Mini">Mini</option>
			<option value="Mitsubishi">Mitsubishi</option>
			<option value="Nissan">Nissan</option>
			<option value="Oldsmobile">Oldsmobile</option>
			<option value="Plymouth">Plymouth</option>
			<option value="Pontiac">Pontiac</option>
			<option value="Porsche">Porsche</option>
			<option value="Rolls Royce">Rolls Royce</option>
			<option value="Saab">Saab</option>
			<option value="Scion">Scion</option>
			<option value="Smart">Smart</option>
			<option value="Subaru">Subaru</option>
			<option value="Suzuki">Suzuki</option>
			<option value="Suzuki Motorcycle">Suzuki Motorcycle</option>
			<option value="Tesla">Tesla</option>
			<option value="Toyota">Toyota</option>
			<option value="Volkswagen">Volkswagen</option>
			<option value="Volvo">Volvo</option>
			<option value="Yamaha Motorcycles">Yamaha Motorcycles</option>

		</select>
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterModelDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList">
			<option value="">--Model--</option>
			<option value="All Models">All Models</option>
			<option value="A Class">A Class</option>
			<option value="B Class">B Class</option>
			<option value="C Class">C Class</option>
			<option value="CL Class">CL Class</option>
			<option value="CLC Class">CLC Class</option>
			<option value="CLK Class">CLK Class</option>
			<option value="CLS Class">CLS Class</option>
			<option selected="selected" value="E Class">E Class</option>
			<option value="G Class">G Class</option>
			<option value="G550">G550</option>
			<option value="GL Class">GL Class</option>
			<option value="GLK Class">GLK Class</option>
			<option value="M Class">M Class</option>
			<option value="ML Class">ML Class</option>
			<option value="R Class">R Class</option>
			<option value="S Class">S Class</option>
			<option value="SL Class">SL Class</option>
			<option value="SL550">SL550</option>
			<option value="SLK Class">SLK Class</option>
			<option value="SLS Class">SLS Class</option>
			<option value="Sprinter">Sprinter</option>
			<option value="Truck">Truck</option>
			<option value="Viano">Viano</option>

		</select>
                                
	</div>
                            
                            
                            
                            
                            <input type="submit" name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterColorSearchButton" value="Search" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$OuterBodyContentPlaceHolder$OuterColorSearchButton&quot;, &quot;&quot;, true, &quot;ColorSearchValidationGroup&quot;, &quot;&quot;, false, false))" id="OuterColorSearchButton" class="btn btn-primary btn-center white" />
                        </div>
						<div style="clear:both;"></div>
                    
</div>
						</div>

                        <h4>START HERE</h4>
                        <ul>
                            <li><a href="/touch-up-paint/">Find Your Car's Color</a></li>
							<li><a href="/directions.htm">How to Use Our Products</a></li>
							<li><a href="/how-to-videos/">See How it Works on Video</a></li>
							<li><a href="/paint-code.htm ">Where to Find Your Paint Code</a></li>
                        </ul>

                        <h4>TYPES OF PAINT</h4>
                        <ul>
                            <li><a href="/touch_up_paint.asp">1/2oz Paint (Brush-in-Bottle)</a></li>
							<li><a href="/store/2oz_touch-up_paints/index.aspx">2oz Paint (Brush-in-Bottle)</a></li>
							<li><a href="/paintpen.asp">Paint Pens</a></li>
							<li><a href="/spray_paint.asp">Aerosol Spray Paint</a></li>
							<li><a href="/auto_paint.asp">Auto Paint (for Spray Guns)</a></li>
                        </ul>


                        <h4>ACCESSORIES</h4>
                        <ul>
							<li><a href="/touch_up_accessories.aspx">Touch Up Paint Accessories</a></li>
							<li><a href="/spray_paint_accessories.aspx">Spray Paint Accessories</a></li>
							<li><a href="/bodyandbumper.aspx">Body and Bumper Repair</a></li>
							<li><a href="/sandpaper/general-abrasives.aspx">General Abrasives</a></li>
							<li><a href="/safety.aspx">Safety</a></li>
                        </ul>

                    </div>
                </div>
                <!-- end sidebar -->
            </div>
        </div>
    </section>









            <!-- Footer

		============================================= -->

            <footer id="footer" class="light" style="background: url('/child_theme/images/footerbg.jpg') center no-repeat; background-size: cover;">



                <div class="container">

                    <div class="row">



                        <div class="col-xs-12 col-sm-3">

                            <h3>Contact</h3>







                            <p>

                                Microfinish LLC<br />

                                208 Plauche Ct<br />

                                New Orleans, LA 70123

                            </p>

                            <p>

                                Toll Free: 1-888-710-5192<br />

                                Phone: 1-504-818-2334<br />

                                Fax: 1-504-818-2996

                            </p>

                            <p>

                            </p>

                            <p><a href="/contact-us.aspx" class="btn btn-primary">Contact Us</a></p>

                        </div>







                        <div class="col-xs-12 col-sm-3">

                            <h3>Learn More</h3>

                            <ul>

								<li><a href="/touch-up-paint/">Choose Your Make</a></li>

                                <li><a href="/faq.htm">FAQ</a></li>

                                <li><a href="/site_map.htm">Sitemap</a></li>

                                <li><a href="/e-store_policies.aspx">Policies</a></li>

                                <li><a href="/affiliate.aspx">Affiliates</a></li>

                                <li><a href="/link_to_us.aspx">Link To Us</a></li>

                                <li><a href="/privacy_policy.aspx">Privacy Policy</a></li>

                            </ul>

                            <div class="flex-stretch">

                                <a href="https://www.facebook.com/AutomotiveTouchup/"><i class="fa fa-facebook"></i></a>

                                <a href="https://www.youtube.com/user/Automotivetouchup"><i class="fa fa-youtube"></i></a>

                                <a href="https://plus.google.com/+AutomotivetouchupPaint"><i class="fa fa-google-plus"></i></a>

                                <a href="https://twitter.com/atu_microfinish"><i class="fa fa-twitter"></i></a>

                            </div>

                        </div>



                        <div class="col-xs-12 col-sm-6">

                            <h3>Information</h3>

                                &copy; 2018 | AutomotiveTouchup, a Microfinish LLC Company<br />

                                <br />



                                <img width="280" src="/child_theme/images/cc.png" alt="credit cards we accept" /><br />

                                <br />



                                <img src="/child_theme/images/footerlogo.png" alt="AutomotiveTouchup Logo" />

                        </div>

                    </div>

                </div>



			 

			 

			

			<a id="RefreshButton" href="javascript:__doPostBack(&#39;ctl00$ctl00$RefreshButton&#39;,&#39;&#39;)" style="display: none"></a>



			<div id="content">



				<div id="contentwhite">

				</div>

				<div id="contentwhitebottom">

					

					

				</div>

				

					<div style="display: none;">

						<img height="1" width="1" style="border-style: none;" alt="ad" title="ad" src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072509267/?value=0&amp;label=g1PzCMPsrQkQ0-K0_wM&amp;guid=ON&amp;script=0" />

					</div>

				

			</div>

			</footer>

        </div>



        <!-- #wrapper end -->



        <!-- Go To Top

		============================================= -->

        <div id="gotoTop" class="icon-angle-up"></div>







        <!-- Footer Scripts

	============================================= -->

        <script>

            $('.fancybox-thumbs').fancybox({





                helpers: {

                    thumbs: {

                        width: 50,

                        height: 50

                    }

                }

            });

            $(document).ready(function () {



                function searchF() {

                    var sWidth = $("#primary-menu > ul").width();

                    $(".top-search-content").css('width', sWidth);

                };

                $('#top-search a').click(function () {

                    $(".top-search-content").toggle();

                    $("#top-search a").toggleClass('on');

                });

                searchF();



                $(window).resize(function () {

                    searchF();

                });



            });



        </script>



<script>

$('#trigger-uploads').click(function(e){

e.preventDefault();

$(this).hide();

$('#uploads-table').show();

});

</script>



<!-- Move To Top feature -->

<script type="text/javascript">

$(window).scroll(function(){

if($(window).scrollTop() > 500){

$('#gotoTop').show();

}else{

$('#gotoTop').hide();

};

});

</script>

<script type="text/javascript">

$('#gotoTop').click(function(){

$('html, body').animate({scrollTop : 0},400);

return false;

});



</script>

<!-- / Move To Top feature -->

<!-- Mouseflow -->

<script type="text/javascript">

    var _mfq = _mfq || [];

    (function() {

        var mf = document.createElement("script");

        mf.type = "text/javascript"; mf.async = true;

        mf.src = "//cdn.mouseflow.com/projects/d118fe43-3e59-4474-a834-342182950bd8.js";

        document.getElementsByTagName("head")[0].appendChild(mf);

    })();

</script>

<!-- /Mouseflow -->

    

<script type="text/javascript">
//<![CDATA[
$(document).ready(function () {if (!checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList','2011') || !checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList','Mercedes-Benz') || !checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList','E Class')) {findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList', ''); findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList', ''); findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList', ''); __doPostBack('ctl00$ctl00$OuterBodyContentPlaceHolder$RefreshButton', '');}});//]]>
</script>

<script src="/js/outermaster_internal.js" type="text/javascript"></script></form>



</body>

</html>

