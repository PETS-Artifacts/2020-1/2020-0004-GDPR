

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
<head><meta http-equiv="content-type" content="text/html; charset=utf-8" /><meta name="msvalidate.01" content="B2590B06B4BEC6D2F3DB0A450D4E6870" />
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script type="text/javascript" src="/js/plugins.js"></script>
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/js/plugins/fancy-box/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="/js/plugins/fancy-box/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/child_theme/js/scripts.js"></script>
    <script type="text/javascript" src="/js/functions.js"></script>
    <script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
	<script src="https://cdn.optimizely.com/js/317536305.js"></script>


    <!-- /block -->
    <link rel="shortcut icon" href="/templates/images/favicon.ico" />
    <!-- Stylesheets
	============================================= -->

    <link rel="stylesheet" type="text/css" href="/child_theme/css/all-ie-only.css" /><link rel="stylesheet" href="/css/bootstrap.css" type="text/css" /><link rel="stylesheet" href="/css/conflict/style.css" type="text/css" /><link rel="stylesheet" href="/css/dark.css" type="text/css" /><link rel="stylesheet" href="/css/font-icons.css" type="text/css" /><link rel="stylesheet" href="/css/animate.css" type="text/css" /><link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts.css" type="text/css" /><link rel="stylesheet" href="/child_theme/style.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts/tarzananarrowbold.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/fonts/tarzananarrowregular.css" type="text/css" /><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" /><link rel="stylesheet" href="/css/responsive.css" type="text/css" /><link rel="stylesheet" href="/child_theme/css/responsive.css" type="text/css" /><link rel="stylesheet" href="/js/plugins/fancy-box/source/jquery.fancybox.css" type="text/css" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

    <!-- Document Title
	============================================= -->
    <title>
	Privacy Policy Statement | Touch Up Paint  | AutomotiveTouchup
</title>
    

<meta name="description" content="Privacy policy for Automotivetouchup.com, Microfinish LLC at AutomotiveTouchup at AutomotiveTouchup" /><meta name="keywords" content="Privacy Policy, Automotive Touch Up Privacy, Touch Up Paint, touch up paint, touchup paint, car paint, vehicle touch up paint, vehicle touchup paint, AutomotiveTouchup, Touch Up Paint, touch up paint, touchup paint, car paint, vehicle touch up paint, vehicle touchup paint, AutomotiveTouchup" /></head>
<body class="no-transition stretched">
    
    <form method="post" action="./privacy_policy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="G63kO2qaRXRl3Wd2Y5ReuK6DM+wVUvuzExKp3ebTUl5d3oGYxLbpv/+/7fGU18se6vXP/KIP4S/+KDstqgagcA820MdQNOtV2dWP5CcRV107bAaDXNdeLc7/GGof/ExMedugCOWYAPORa2bg6ma5viqz1AkjtqIjHT/U/MHLqzu1iNthFfLG1z5rb83eaICyIUl3TERg7qRwRwsBQBd1j9O0i53M7tpN2L9RMqdqsnFEvp3jDlD3qcxXltuyiSApWrlM4AgLWsMKgqqzwn8yiN3+yD57tJCkHjL/33uuOfXjMAEIbkvfZpIPX0IwBiFIDUwmfZybd4XpJIVtHe6KmsDe2yo6iC3UBQsFiyvBYjtIWIOnHb240Ufu6KmOHNQSg9hA9pVFaGACqbN8U3sFhBmCt61sfcELA+mgl/e0otR5bQyXtuy7JEWSQfoZKXJiFZi+v08eVocbwPO/7iM7dhy5gFaXCyIxCVjL4IXiI7sk9wkFHZjCF7nsUKkr5W17fPyspR1RNCh61sKuxQ73iQEsx1N54sDGEvLS1r3bKOJesGM6UVMoDfGB3cSTkdgK5M5a2mb0N+n8lLG20tB8ekD43sWxY2pF+MFKaD0w7SvlaGlc5sNeu0fxmbtONkg1wx/YqLE5H7VPsInih+msWNAqomxVU5YenNyfNrrm7VPGlbJJvTPgTPpT4EolMA5g9TZAoYpyPIKahTx1Q362bVoZ4ix4B9t/wCkAcLHQwyl7GJDAjJHgcmY3GcU4gpmWEC/hBTtxpMHVqARGfwqCsRA7phN+myvOSsgM3epY5zjP8NgP+RVAS2F5mUj93+os9adDqqYemlANCfOjZk9jBKl1Yq29nnvgfX+h56KvUNaByLE5yoR5VO15NzCwPygF0PCxfFhPXwQS31+aojC3q1AHaZeuQRN/+pPU36nEokWaQozCIYNvSumLlgvlLnOw/beRpLTYb1o0vqau7gX4uaVBgGC6jFgc1iKvrgRSt1ykVDiQiRhPeQt0gMrz0KVZg4nJ73l5yM3L1Pu27kDMnP3R12sE3Yed7cs1UxC58nK+B7RtW3zEjBJ4Ad6dt+84rk/usZxRmFZvLWHFu1aTc/iR8sJh4j6QJBKQOAJ3H1CA64qXazW03Qcp3RMxKRcjLxmUM8dPFsVoQyzG8423xmWhuCrXhphyLPXXM39N/yq7BiY0RyPdxl+FUhmGEnPHPbI2tQcuHeJOt7o1Qn7XpneE3UevO/4++NORSHZqS7Ta7dgTg5hcV/yLbJdEjWXbr1TnVWKYAN36vyp/dRybWrF/FBmU8wD5E66LT64Lqc+3EcPEzmI/kcR35dxZ07L037H5+OAJtE/JMJj6zLsQ+ddw7avNDx0kTwO3KVWB6QLHLrumCWPJ8ZRlhcN72lGq5+R7JE+1Et1o5HmFeXK+ahC6JvI6QOz8kzMWB4VmSIQvmuwgtVMruaaNIvJunf1r/1l9QM0j681IStzncCYblW9knTx6EQHTL+z50VTi8WShAq5NV32jT7P0La/3urzZteJxY9z7zRNmqfKWOqoON90w1oC0qrTZC3NDjOeQv5X3eDr+kvhbyWBkzpTxvx7CTimF17Ib9rUZuKBhicZ9LEgQhYEK60f8T5UjeTVpzygRAT/+26tTHX2huARgoybDHSToj7IT9Fdco+921xhj4dQlEeKBDscZ6vx3uSxdghAWkmkpo0sfqa1Ue64YOnzI9s+NAhUZjNWxPcohDUX/7IXFqFjelh0R1AAaeJEIUzwbta1mdsME5wAxZRlaQgLA" />


<script src="/js/outermaster_external.js" type="text/javascript"></script>
<script src="//www.googleadservices.com/pagead/conversion.js" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yGe-MgZ6_6pvcmjBkCQMNS8-RyI640njGgMPV5RliEowHeuNCdPy0CKFe_CVqjeuPEUK-hDvdpNNyoaq0z-lCUADdhHv7eusPZaq_xADHMAocgoC7y0BxM6hg5mZQCr3LzDoER7ofeJIAI-RcznGFfI1&amp;t=ffffffffcc58dd65" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2927320D" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="8v4ZrkLyRYfwLcZHb0zto7hxBaA04ae4wolaFnYaJtkPzfyljYOyQ0TQPWgPDNKcanupTmQje1W2Vuj8Vp1e0iHpQ8emprdw9lSFqZaCUgtyM2JUOPeVrCRXCMja8G0iPr4npEyK2rg3Jf9ws2hijZc3x7z+BF3y/gI1D40lmE5dpj7BujcJ21/0RhKVyKXxOQWtA7yq8EMY2fyeCGJrskEs8xg598KVcicDj8T0rS7EbIp7Lb0W3VPl6eHn0ccKc2ovUXrnZVVnF6dm+gf4jrgq6nkxGHji3oxGoQZGu462ks7IZq1QhFKjYdegjKQ2084f6upPTpIfj/adVboAwCakJZqYPO+hH/wcXtCATJHWLYs0wXvct51E+qj44VmTHSQhWtc/SyB+PKspyx7GaB3MCw7eZRobQCbBfKunb52uWDKYSSV2JQ1C7IvH8ZgwKRoz/U+erTyviT+rWOaW+NXi/DRYPBtbsO2BsTNPa5jGnwuBkzOtsqiqEefAv6yyg2ynUIc4QOLyqjruoWdXN5jjDO6A+MRAMnlSTrggbwglNtr36EwyTSzkqwOGXj7Ky6aojEjJerWMQ7gFOvEc8C82JasFWKXYucfYE7KJyzDxvjeVUfVsxbX7Iyy3tUhs/f3VjHr1CMxQf3XHt3Nv/ZPRzmvvq/AKeuz0Xi7mDG4p5UefPm7NUFXHjyW3WiGSh46M/FHjowWvh86zh7+I3PZpxzWQcGtw2o/Fi1AtsNT8Hvbc4G0v6b/Pzr5Kuym0cOzTeyLm7bPDk6sropl8ibSEJR8GLSxHamqec7+BuRds45mh71J88W1sJze5oGCCFwDaOIfM3bAryVueZ9rc5Uu2HJ9D5MUXyyBoZT33es7FskclKfra3MyGejCiB60CnmA4qWqr3yk+JwmMmr1Ho7sJ+xfuXz3dd5SIobEb4X6hRzjLnGDEsUkfd60jnG7BCPmsqjrmfI42JD7sQ1Sq4EI32oChEnTYDiUiQzJiDLomTBp12ngeh40GQlX6kzaK4M2wTivoMH0mtzdEk/YJbWzKCO8QTcnXUgBP0/rgC7zfPRO0Tjux6X0SOa9cje6s9LYFSIRuJnrEHYYizfBtjBgJmQGE4kNdyzO3YUzv6C4AVWOHI3fR+4wDjsUl42LZT0AJFXWMYPLc44HfUigq/p4nZpS2f5sTXEArLFo1miMr1ltV4KQ599rszq6iQaTlr9P5H39OyNij1KSGb3kXD1Xtwas9BbvNWWxG+wbyh/dpLoSgddheaJQj7LZsRV6n5x2anPjvRcgRgs0aVOAPjI20vyMEMs5j4OrF+yZcTYf0qTcIfFE8IvSzvLipenEDQNTWcXZAeIdS2BeSbDU+xBQrzfsQqUycKvlxAmWXUvfDJPKvs9oS9kaRa/na6fdUovSF00c/dfSqNe23yBDNs2f8iUl1V307SviJC0nLFRnHvTTpjAmypC4j+XRmdv8JZjtZXpxVgy0QFqAYUsEQTTMczuYTFm82hkV6oihKetOMJwvRzPKCYIxQkNiNLZBaoEYFbcQS99SSNPBm4lz+GtVaDevjHXtgp72a6x+v5kLS5SGySWqNvlO6wWpue2PPQW6E8FRS9Go6EulckuA6e55kQ1QJYusiATINLiOAvk4o7lE0uAHCDISQ1WXWPGHzcYLPeMKMlGB+DBdgIgvL3jtwCglls9ueAPNMpi+BtYOZRcq+5AN2pshQ16xkZn88Ov/gcme4at2qpWA3RAwkhrwSsDn6585u4hXvv+Kt+5p0G/C/Qv2tt5Bd9SdK0TNr2CZP6EhotQJBI/gjT4kqNRiMb14zlo89hFaBw9Ht6Bn6NlaDqgMeTNsFurcda+OZv3nt31t72tPo4nGxEUxtSJPg+7FyfBRtdPxkSvLgqwmBBsKfSWQWj1Ao+jACATFucrpsod2kDAffGyFRcwwoq4P2TKjXGox3laTJTS0Lyab7cAlIVYxOn/3pjmVn1QUQcVHk6USZR69vGcRYTyYzpZqzo440jEkRkdDNi4KkXn4=" />

        

        <!-- Document Wrapper
	============================================= -->
        <div id="wrapper" class="clearfix">

            <div class="top-bar">
                <div class="container">
                    <div class="row">

                        <!-- Logo
					============================================= -->
                        <div id="logo">
                            <a href="http://www.automotivetouchup.com/" class="standard-logo" data-dark-logo="/child_theme/images/logo.png">
                                <img width="238" src="/child_theme/images/logo.png" alt="Logo" /></a>
                            <a href="http://www.automotivetouchup.com/" class="retina-logo" data-dark-logo="/child_theme/images/logo.png">
                                <img width="238" src="/child_theme/images/logo.png" alt="Logo" /></a>
                        </div>
                        <!-- #logo end -->


                        <div class="col-xs-12 text-right">
                            <ul class="list-inline social-links">
                                <li><strong>1-888-710-5192</strong></li>
                                <li>|</li>
                                <li><a href="/contact-us.aspx">Contact</a></li>
                                <li>|</li>
                                <li><a href="/check.aspx">Order Status</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Header
		============================================= -->
            <header id="header" class="transparent-header dark no-sticky">

                <div id="header-wrap" style="background-color: #455660;">

                    <div class="container clearfix dark">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>


                        <!-- Primary Navigation
					============================================= -->
                        <nav id="primary-menu">
                            <div class="top-search-content">
                                <div>
	
                                    <input name="ctl00$ctl00$SearchTextBox" type="text" id="ctl00_ctl00_SearchTextBox" class="form-control" placeholder="Type &amp; Hit Enter.." />
                                    
                                    <input type="submit" name="ctl00$ctl00$TopSearchImageButton" value="" id="TopSearchImageButton" style="display: none" />
                                
</div>
                            </div>
                            <ul>
                                <li class="mega-menu">
                                    <a href="/touch-up-paint/">
                                        <div>Choose Your Make <span class="twosep">:</span></div>
                                    </a>
                                   <div class="mega-menu-content myFirstMegaMenu style-2 clearfix">
										<ul class="mega-menu-column col-md-3">

										<li><a href="/touch-up-paint/acura/">Acura</a></li>
                                        <li><a href="/touch-up-paint/audi/">Audi</a></li>
                                        <li><a href="/touch-up-paint/bmw/">BMW</a></li>
                                        <li><a href="/touch-up-paint/buick/">Buick</a></li>
                                        <li><a href="/touch-up-paint/cadillac/">Cadillac</a></li>
                                        <li><a href="/touch-up-paint/chevrolet/">Chevrolet</a></li>
                                        <li><a href="/touch-up-paint/chrysler/">Chrysler</a></li>
                                        <li><a href="/touch-up-paint/dodge/">Dodge</a></li>
                                        <li><a href="/touch-up-paint/ford/">Ford</a></li>
										</ul>
										<ul class="mega-menu-column col-md-3">
										<li><a href="/touch-up-paint/gmc/">GMC</a></li>
                                        <li><a href="/touch-up-paint/honda/">Honda</a></li>
                                        <li><a href="/touch-up-paint/hummer/">Hummer</a></li>
                                        <li><a href="/touch-up-paint/hyundai/">Hyundai</a></li>
                                        <li><a href="/touch-up-paint/infiniti/">Infiniti</a></li>
                                        <li><a href="/touch-up-paint/jaguar/">Jaguar</a></li>
                                        <li><a href="/touch-up-paint/jeep/">Jeep</a></li>
                                        <li><a href="/touch-up-paint/kia/">Kia</a></li>
                                        <li><a href="/touch-up-paint/lexus/">Lexus</a></li>
										</ul>
										<ul class="mega-menu-column col-md-3">
										<li><a href="/touch-up-paint/land-rover/">Land Rover</a></li>
										<li><a href="/touch-up-paint/lincoln/">Lincoln</a></li>
                                        <li><a href="/touch-up-paint/mazda/">Mazda</a></li>
                                        <li><a href="/touch-up-paint/mercedes-benz/">Mercedes-Benz</a></li>
                                        <li><a href="/touch-up-paint/mini/">Mini</a></li>
                                        <li><a href="/touch-up-paint/mitsubishi/">Mitsubishi</a></li>
                                        <li><a href="/touch-up-paint/nissan/">Nissan</a></li>
                                        <li><a href="/touch-up-paint/pontiac/">Pontiac</a></li>
                                        <li><a href="/touch-up-paint/porsche/">Porsche</a></li>
										</ul>
										<ul class="mega-menu-column col-md-3">
										<li><a href="/touch-up-paint/saab/">Saab</a></li>
										<li><a href="/touch-up-paint/saturn/">Saturn</a></li>
                                        <li><a href="/touch-up-paint/scion/">Scion</a></li>
                                        <li><a href="/touch-up-paint/smart/">Smart</a></li>
                                        <li><a href="/touch-up-paint/toyota/">Toyota</a></li>
                                        <li><a href="/touch-up-paint/volkswagen/">Volkswagen</a></li>
                                        <li><a href="/touch-up-paint/volvo/">Volvo</a></li>
                                        <li><a href="/touch-up-paint/">Don't see your make?</a></li>
										</ul>
									</div>
                                </li>


                                <li class="mega-menu"><a href="/paint-code.htm">
                                    <div>Paint Codes <span class="twosep">:</span></div>
                                </a></li>

                                <li class="mega-menu col-4-menu">
                                   <a href='http://www.automotivetouchup.com/products.aspx'>
                                        <div>Products <span class="twosep">:</span></div>
                                    </a>
                                    <ul style="display: none">
                                        
                                        <li><a href='http://www.automotivetouchup.com/touch_up_paint.asp'>1/2oz Paint Bottles</a>
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_paint_matched.aspx'>1/2oz Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_paint_clearcoat.aspx'>1/2oz Clearcoat</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_primer.aspx'>1/2oz Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>1/2oz Accessories</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/index.aspx'>2oz Paint Bottles</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_basecoat.aspx'>2oz Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_clearcoat.aspx'>2oz Clearcoat </a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_primer.aspx'>2oz Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>2oz Accessories</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/paintpen.asp'>Paint Pens</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_matched.aspx'>Paint Pen Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_clearcoat.aspx'>Paint Pen Clearcoat</a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_primer.aspx'>Paint Pen Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Paint Pen Accessories</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/spray_paint.asp'>Aerosol Spray Paint</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/spray_paint_matched.aspx'>Aerosol Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_clearcoat.aspx'>Aerosol Clearcoat</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_primer.aspx'>Aerosol Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_accessories.aspx'>Aerosol Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_trim.aspx'>Black Trim Aerosol</a></li>
												<li><a href='http://www.automotivetouchup.com/spray-paint/adhesion-promoter.aspx'>Adhesion Promoter</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/auto_paint.asp'>Spray Gun Paint</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/auto_paint_basecoat.aspx'>Spray Gun Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_clearcoat.aspx'>Spray Gun Clearcoat</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_primer.aspx'>Spray Gun Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_accessories.aspx'>Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/spray-guns/all-spray-guns.aspx'>Spray Guns</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Accessories</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_accessories.aspx'>Touch Up Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_accessories.aspx'>Aerosol Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_accessories.aspx'>Auto Paint Accessories</a></li>
												<li><a href='http://www.automotivetouchup.com/spray-guns/all-spray-guns.aspx'>Spray Guns</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/prep_solvent.aspx'>Prep Solvent</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/rubbing_compound.aspx'>Rubbing Compound</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/surface-wipe.aspx'>Prep Wipe</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/bodyandbumper.aspx'>Body & Bumper</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/bodyandbumper.aspx'>Body & Bumper Repair</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bondo.aspx'>Bondo Body Filler</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/ultimate_bondo.aspx'>Ultimate Body Filler</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bondo-professional-gold-filler.aspx'>Gold Body Filler</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/spot_putty.aspx'>Glazing and Spot Putty</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/bumper_repair_kit.aspx'>Syringe Bumper Repair</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/flexible_part_bumper_repair.aspx'>Easy Finish Part Repair</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/flexible_part_bumper_repair.aspx'>Easy Finish Bumper </a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/plastic_spreaders.aspx'>Plastic Spreaders</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper.aspx'>Sandpaper</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper-multi-pack.aspx'>Sandpaper Multi-Pack</a></li>
												
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/file_holder.aspx'>3 Position File Holder</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/sanding_block.aspx'>Rubber Sanding Block</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/sandpaper/general-abrasives.aspx'>Sandpaper</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper.aspx'>Wet Sandpaper</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/wet-sandpaper-multi-pack.aspx'>Assorted Sanding Pack</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/touch_up_sandpaper.aspx'>Mini Sandpaper</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/scuff-pad.aspx'>Scuff Pad</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/sanding_block.aspx'>Rubber Sanding Block</a></li>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/file_holder.aspx'>3 Position File Holder</a></li>
												<li><a href='http://www.automotivetouchup.com/sandpaper/stick-on-disc.aspx'>Stick-On DA Paper</a></li>
											</ul>
										</li>
										<li><a href='http://www.automotivetouchup.com/safety.aspx'>Safety</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/store/bumper_and_body_repair/dust_mask.aspx'>Dust Masks</a></li>
												<li><a href='http://www.automotivetouchup.com/safety/respirator.aspx'>SAS Bandit Respirator</a></li>
												<li><a href='http://www.automotivetouchup.com/store/accessories/respirator.aspx'>3M Respirator</a></li>
												<li><a href='http://www.automotivetouchup.com/safety/gloves.aspx'>Nitrile Gloves</a></li>
												<li><a href='http://www.automotivetouchup.com/safety/glasses.aspx'>3M Eye Protection</a></li>
											</ul>
										</li>
										<li><a href="#">Basecoat</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_paint_matched.aspx'>1/2oz Brush-In-Bottle </a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_basecoat.aspx'>2oz Brush-In-Bottle </a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_matched.aspx'>Paint Pen Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_matched.aspx'>Aerosol Spray Basecoat</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_basecoat.aspx'>Paint for Spray Guns </a></li>
											</ul>
										</li>										
										<li><a href="#">Clearcoat</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_paint_clearcoat.aspx'>1/2oz Clearcoat </a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_clearcoat.aspx'>2oz Clearcoat </a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_clearcoat.aspx'>Clearcoat Paint Pen</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_clearcoat.aspx'>Clearcoat Aerosol</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_clearcoat.aspx'>Spray Gun Clearcoat </a></li>
												<li><a href='http://www.automotivetouchup.com/lacquer_clearcoat.aspx'>Aerosol Lacquer</a></li>
												<li><a href='http://www.automotivetouchup.com/urethane_clearcoat.aspx'>Aerosol Urethane</a></li>
												<li><a href='http://www.automotivetouchup.com/lacquer_clearcoat.aspx'>Lacquer Qt/Gal </a></li>
												<li><a href='http://www.automotivetouchup.com/urethane_clearcoat.aspx'>Urethane Qt/Gal</a></li>
											</ul>
										</li>
										<li><a href="#">Primer</a> 
											<ul>
												<li><a href='http://www.automotivetouchup.com/touch_up_primer.aspx'>1/2oz Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/store/2oz_touch-up_paints/2_oz_primer.aspx'>2oz Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/store/paint_pens/paint_pen_primer.aspx'>Primer Paint Pen</a></li>
												<li><a href='http://www.automotivetouchup.com/spray_paint_primer.aspx'>Aerosol Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/auto_paint_primer.aspx'>Spray Gun Paint Primer</a></li>
												<li><a href='http://www.automotivetouchup.com/lacquer_primer.aspx'>Laquer Qt/Gal</a></li>
												<li><a href='http://www.automotivetouchup.com/urethane_primer.aspx'>Urethane Qt/Gal</a></li>
											</ul>
										</li>
                                        


                                    </ul>
                                </li>

                                <li class="mega-menu"><a href="/how-to-videos/">
                                    <div>How To Videos <span class="twosep">:</span></div>
                                </a></li>

                                <li class="mega-menu"><a href="/directions.htm">
                                    <div>Directions <span class="twosep">:</span></div>
                                </a></li>

                                <li class="mega-menu"><a href="/gallery.htm">
                                    <div>Results <span class="twosep">:</span></div>
                                </a></li>
                                <li class="mega-menu"><a href="/feedback.aspx">
                                    <div>Reviews <span class="twosep">:</span></div>
                                </a></li>
                                <li class="mega-menu"><a href="/about-us.aspx">
                                    <div>About ATU <span class="twosep"></span></div>
                                </a></li>
								
								<li class="extraSpace"></li>



                            </ul>

                            <!-- Top Cart
						============================================= -->
                            <div id="top-cart">

                                <a id="ctl00_ctl00_lnkCart1" rel="nofollow" href="https://www.automotivetouchup.com/cgi-bin/ShoppingCart.aspx"><div id="ctl00_ctl00_CartUpdatePanel">
	
                                            <img src="/child_theme/images/cart.png" alt="cart-icon"><span id="ctl00_ctl00_NumItemslabel">0</span>
                                        
</div></a>


                            </div>
                            <!-- #top-cart end -->
                            <!-- search Bar
							============================================= -->
                            <div id="top-search">
                                <a href="#"></a>
                            </div>
                            <!-- search Barend -->
                        </nav>
                        <!-- #primary-menu end -->

                    </div>

                </div>



            </header>
            <!-- #header end -->

            
    
    <!-- Slider -->
    <section id="slider" class="clearfix page">
        <div class="main-slide" style="background-repeat: no-repeat;">
            <div class="container clearfix">
                <div class="row-fluid">
                    <div class="col-xs-6 col-sm-6 col-md-6"></div>
                    <div class="maincaptions col-xs-12">
                        <h1 data-caption-animate="fadeInUp" class="fadeInUp animated">PRIVACY POLICY</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Content
		============================================= -->
    <section id="form" class="clearfix">

        <div class="container clearfix">
            <div class="row clearfix">
				 <div class="col-xs-12 col-sm-9 col-sm-push-3">
					<div class="col-xs-12 text-right page-breadcrumbs"><a href="/">HOME</a> > 

                
		<!-- div continued from developSiderbar -->
             PRIVACY POLICY </div>
        <div class="col-xs-12">
            <div id="subpage">
        
        <p>
        AutomotiveTouchup.com belongs to Microfinish LLC.
        </p>
        <p>
Our postal address is
        </p>
        <p>
Microfinish LLC<br>
208 Plauche Ct.<br>
New Orleans, LA 70123<br>
We can be reached via e-mail at web@automotivetouchup.com<br>
or you can reach us toll free by phone at 1-888-710-5192
        </p>
        <p>
For each visitor to our Web page, our Web server automatically recognizes no information regarding the domain or e-mail address (It may recognize an IP address or a referring domain).
        </p>
        <p>
We collect no information on consumers who browse our Website.
        </p>
        <p>
The information we collect is used for internal review and is then discarded, not shared with other organizations for commercial purposes.
        </p>
        <p>
With respect to cookies: We use cookies to record session information, such as items that consumers add to their shopping cart.
        </p>
        <p>
If you do not want to receive e-mail from us in the future, please let us know by sending us e-mail at the above address, writing to us at the above address. We do not use email as a form of marketing.
        </p>
        <p>
If you supply us with your postal address on-line you will only receive the information for which you provided us your address. Persons who supply us with their telephone numbers on-line will only receive telephone contact from us with information regarding orders they have placed on-line.
        </p>
        <p>
With respect to Ad Servers: We do not partner with or have special relationships with any ad server companies.
        </p>
        <p>
Upon request we provide site visitors with access to transaction information (e.g., dates on which customers made purchases, amounts and types of purchases) that we maintain about them, a description of information that we maintain about them.
        </p>
        <p>
Upon request we offer visitors the ability to have inaccuracies corrected in contact information.
Consumers can have this information corrected by sending us e-mail at the above address, writing to us at the above address.
        </p>
        <p>
With respect to security: We always use industry-standard encryption technologies when transferring and receiving consumer data exchanged with our site, We have appropriate security measures in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site.
        </p>
        <p>
If you feel that this site is not following its stated information policy, you may contact us at the above addresses or phone number.
        </p>
        <p>
We appreciate your comments and choose to spotlight some of our customer comments as testimonials. If your comment is selected to appear on our site, we will use only your first name, last initial, location, car/paint info, and your comment. When <a href="/feedback.aspx">submitting your comment</a>, you will have the option to opt out of having your review appear on our site.
        </p>
    </div>
           
       <!-- two more closing divs before the sidebar code starts -->


				
					</div>
				</div>
                <!-- sidebar starts here -->

                <div class="col-xs-12 col-sm-3 col-sm-pull-9 secondary-nav">
                    <span class="visible-xs hidden-sm hidden-md hidden-lg"><a class="nav-drop" data-toggle="collapse" href="#navCollapse" aria-expanded="false" aria-controls="navCollapse">MORE PRODUCTS &#8681;
                    </a></span>

                    <div class="collapse left-sidebar" id="navCollapse">
					
						<div class="mySidebarSearch">
						<h4>FIND YOUR PERFECT COLOR MATCH</h4>
						<p style="margin-bottom:0!important;">Enter your year, make, and model below to find color matched paint:</p>
						<div id="ctl00_ctl00_OuterBodyContentPlaceHolder_ColorSearchPanel" class="sidebarsection">
	
                        <div id="sidebarselectleft">
                            <span id="step1">STEP 1:</span> <span id="step2">STEP 2:</span> <span id="step3">STEP
                            3:</span>
                        </div>
                        <div id="sidebarselectright">
                            <a id="RefreshButton" href="javascript:__doPostBack(&#39;ctl00$ctl00$OuterBodyContentPlaceHolder$RefreshButton&#39;,&#39;&#39;)" style="display: none"></a>
                            <div id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterSearchUpdatePanel">
		
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterYearDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList">
			<option selected="selected" value="">--Year--</option>
			<option value="2018">2018</option>
			<option value="2017">2017</option>
			<option value="2016">2016</option>
			<option value="2015">2015</option>
			<option value="2014">2014</option>
			<option value="2013">2013</option>
			<option value="2012">2012</option>
			<option value="2011">2011</option>
			<option value="2010">2010</option>
			<option value="2009">2009</option>
			<option value="2008">2008</option>
			<option value="2007">2007</option>
			<option value="2006">2006</option>
			<option value="2005">2005</option>
			<option value="2004">2004</option>
			<option value="2003">2003</option>
			<option value="2002">2002</option>
			<option value="2001">2001</option>
			<option value="2000">2000</option>
			<option value="1999">1999</option>
			<option value="1998">1998</option>
			<option value="1997">1997</option>
			<option value="1996">1996</option>
			<option value="1995">1995</option>
			<option value="1994">1994</option>
			<option value="1993">1993</option>
			<option value="1992">1992</option>
			<option value="1991">1991</option>
			<option value="1990">1990</option>
			<option value="1989">1989</option>
			<option value="1988">1988</option>
			<option value="1987">1987</option>
			<option value="1986">1986</option>
			<option value="1985">1985</option>
			<option value="1984">1984</option>
			<option value="1983">1983</option>
			<option value="1982">1982</option>
			<option value="1981">1981</option>
			<option value="1980">1980</option>
			<option value="1979">1979</option>
			<option value="1978">1978</option>
			<option value="1977">1977</option>
			<option value="1976">1976</option>
			<option value="1975">1975</option>
			<option value="1974">1974</option>
			<option value="1973">1973</option>
			<option value="1972">1972</option>
			<option value="1971">1971</option>
			<option value="1970">1970</option>
			<option value="1969">1969</option>
			<option value="1968">1968</option>
			<option value="1967">1967</option>
			<option value="1966">1966</option>
			<option value="1965">1965</option>
			<option value="1964">1964</option>
			<option value="1963">1963</option>
			<option value="1962">1962</option>
			<option value="1961">1961</option>
			<option value="1960">1960</option>
			<option value="1959">1959</option>
			<option value="1958">1958</option>
			<option value="1957">1957</option>
			<option value="1956">1956</option>
			<option value="1955">1955</option>
			<option value="1954">1954</option>
			<option value="1953">1953</option>
			<option value="1952">1952</option>
			<option value="1951">1951</option>
			<option value="1950">1950</option>
			<option value="1949">1949</option>
			<option value="1948">1948</option>
			<option value="1947">1947</option>
			<option value="1946">1946</option>
			<option value="1945">1945</option>
			<option value="1944">1944</option>
			<option value="1943">1943</option>
			<option value="1942">1942</option>
			<option value="1941">1941</option>
			<option value="1940">1940</option>
			<option value="1939">1939</option>
			<option value="1938">1938</option>
			<option value="1935">1935</option>

		</select>
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterMakeDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList" disabled="disabled" class="aspNetDisabled">
			<option selected="selected" value="">--Make--</option>

		</select>
                                    <select name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterModelDropDownList" id="ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList" disabled="disabled" class="aspNetDisabled">
			<option selected="selected" value="">--Model--</option>

		</select>
                                
	</div>
                            
                            
                            
                            
                            <input type="submit" name="ctl00$ctl00$OuterBodyContentPlaceHolder$OuterColorSearchButton" value="Search" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$OuterBodyContentPlaceHolder$OuterColorSearchButton&quot;, &quot;&quot;, true, &quot;ColorSearchValidationGroup&quot;, &quot;&quot;, false, false))" id="OuterColorSearchButton" class="btn btn-primary btn-center white" />
                        </div>
						<div style="clear:both;"></div>
                    
</div>
						</div>

                        <h4>START HERE</h4>
                        <ul>
                            <li><a href="/touch-up-paint/">Find Your Car’s Color</a></li>
							<li><a href="/directions.htm">How to Use Our Products</a></li>
							<li><a href="/how-to-videos/">See How it Works on Video</a></li>
							<li><a href="/paint-code.htm ">Where to Find Your Paint Code</a></li>
                        </ul>

                        <h4>TYPES OF PAINT</h4>
                        <ul>
                            <li><a href="/touch_up_paint.asp">1/2oz Paint (Brush-in-Bottle)</a></li>
							<li><a href="/store/2oz_touch-up_paints/index.aspx">2oz Paint (Brush-in-Bottle)</a></li>
							<li><a href="/paintpen.asp">Paint Pens</a></li>
							<li><a href="/spray_paint.asp">Aerosol Spray Paint</a></li>
							<li><a href="/auto_paint.asp">Auto Paint (for Spray Guns)</a></li>
                        </ul>


                        <h4>ACCESSORIES</h4>
                        <ul>
							<li><a href="/touch_up_accessories.aspx">Touch Up Paint Accessories</a></li>
							<li><a href="/spray_paint_accessories.aspx">Spray Paint Accessories</a></li>
							<li><a href="/bodyandbumper.aspx">Body and Bumper Repair</a></li>
							<li><a href="/sandpaper/general-abrasives.aspx">General Abrasives</a></li>
							<li><a href="/safety.aspx">Safety</a></li>
                        </ul>

                    </div>
                </div>
                <!-- end sidebar -->
            </div>
        </div>
    </section>





            <!-- Footer
		============================================= -->
            <footer id="footer" class="light" style="background: url('/child_theme/images/footerbg.jpg') center no-repeat; background-size: cover;">

                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 col-sm-3">
                            <h3>Contact</h3>



                            <p>
                                Microfinish LLC<br />
                                208 Plauche Ct<br />
                                New Orleans, LA 70123
                            </p>
                            <p>
                                Toll Free: 1-888-710-5192<br />
                                Phone: 1-504-818-2334<br />
                                Fax: 1-504-818-2996
                            </p>
                            <p>
                            </p>
                            <p><a href="/contact-us.aspx" class="btn btn-primary">Contact Us</a></p>
                        </div>



                        <div class="col-xs-12 col-sm-3">
                            <h3>Learn More</h3>
                            <ul>
								<li><a href="/touch-up-paint/">Choose Your Make</a></li>
                                <li><a href="/faq.htm">FAQ</a></li>
                                <li><a href="/site_map.htm">Sitemap</a></li>
                                <li><a href="/e-store_policies.aspx">Policies</a></li>
                                <li><a href="/affiliate.aspx">Affiliates</a></li>
                                <li><a href="/link_to_us.aspx">Link To Us</a></li>
                                <li><a href="/privacy_policy.aspx">Privacy Policy</a></li>
                            </ul>
                            <div class="flex-stretch">
                                <a href="https://www.facebook.com/AutomotiveTouchup/"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.youtube.com/user/Automotivetouchup"><i class="fa fa-youtube"></i></a>
                                <a href="https://plus.google.com/+AutomotivetouchupPaint"><i class="fa fa-google-plus"></i></a>
                                <a href="https://twitter.com/atu_microfinish"><i class="fa fa-twitter"></i></a>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6">
                            <h3>Information</h3>
                                &copy; 2017 | AutomotiveTouchup, a Microfinish LLC Company<br />
                                <br />

                                <img width="280" src="/child_theme/images/cc.png" alt="credit cards we accept" /><br />
                                <br />

                                <img src="/child_theme/images/footerlogo.png" alt="AutomotiveTouchup Logo" />
                        </div>
                    </div>
                </div>

			 
			 
			
			<a id="RefreshButton" href="javascript:__doPostBack(&#39;ctl00$ctl00$RefreshButton&#39;,&#39;&#39;)" style="display: none"></a>
			
			<div id="content">

				<div id="contentwhite">
				</div>
				<div id="contentwhitebottom">
					
					
				</div>
				
					<div style="display: none;">
						<img height="1" width="1" style="border-style: none;" alt="ad" title="ad" src="http://googleads.g.doubleclick.net/pagead/viewthroughconversion/1072509267/?value=0&amp;label=g1PzCMPsrQkQ0-K0_wM&amp;guid=ON&amp;script=0" />
					</div>
				
			</div>
			</footer>
        </div>

        <!-- #wrapper end -->

        <!-- Go To Top
		============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>



        <!-- Footer Scripts
	============================================= -->
        <script>
            $('.fancybox-thumbs').fancybox({


                helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });
            $(document).ready(function () {

                function searchF() {
                    var sWidth = $("#primary-menu > ul").width();
                    $(".top-search-content").css('width', sWidth);
                };
                $('#top-search a').click(function () {
                    $(".top-search-content").toggle();
                    $("#top-search a").toggleClass('on');
                });
                searchF();

                $(window).resize(function () {
                    searchF();
                });

            });

        </script>
      
<script>
$('#trigger-uploads').click(function(e){
e.preventDefault();
$(this).hide();
$('#uploads-table').show();
});
</script>

<!-- Move To Top feature -->
<script type="text/javascript">
$(window).scroll(function(){
if($(window).scrollTop() > 500){
$('#gotoTop').show();
}else{
$('#gotoTop').hide();
};
});
</script>
<script type="text/javascript">
$('#gotoTop').click(function(){
$('html, body').animate({scrollTop : 0},400);
return false;
});

</script>
<!-- / Move To Top feature -->
<!-- Mouseflow -->
<script type="text/javascript">
    var _mfq = _mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/d118fe43-3e59-4474-a834-342182950bd8.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
</script>
<!-- /Mouseflow -->
    

<script type="text/javascript">
//<![CDATA[
$(document).ready(function () {if (!checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList','') || !checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList','') || !checkDropDownValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList','')) {findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterYearDropDownList', ''); findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterMakeDropDownList', ''); findAndSetValue('ctl00_ctl00_OuterBodyContentPlaceHolder_OuterModelDropDownList', ''); __doPostBack('ctl00$ctl00$OuterBodyContentPlaceHolder$RefreshButton', '');}});//]]>
</script>

<script src="/js/outermaster_internal.js" type="text/javascript"></script></form>

</body>
</html>
