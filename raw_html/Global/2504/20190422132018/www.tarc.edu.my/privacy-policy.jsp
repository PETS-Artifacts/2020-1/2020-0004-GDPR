




























	



































 
 













 

<!DOCTYPE html>
<html lang="en" class="wide wow-animation">
  <head>
    <title>Tunku Abdul Rahman University College</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="keywords" >
    <meta name="description" >
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
   
   <link rel="icon" href="https://tarc.edu.my/images/tarIco.ico" type="image/x-icon">
    			
        <!-- bootstrap & fontawesome -->
        
        <link rel="stylesheet" href="https://tarc.edu.my/assets/css/colorbox.css">
		<link rel="stylesheet" href="https://tarc.edu.my/assets/css/bootstrap.css" />
		<link rel="stylesheet" href="https://tarc.edu.my/assets/css/font-awesome.css" />

		<!-- page specific plugin styles -->
		
        
           
         	<!--link rel="stylesheet" href="https://tarc.edu.my/assets/css/jquery-ui.custom.css" /-->
            
            <!--link rel="stylesheet" href="https://tarc.edu.my/assets/css/chosen.css" /-->
            <!--link rel="stylesheet" href="https://tarc.edu.my/assets/css/datepicker.css" /-->
            <!--link rel="stylesheet" href="https://tarc.edu.my/assets/css/bootstrap-timepicker.css" /-->
            <!--link rel="stylesheet" href="https://tarc.edu.my/assets/css/daterangepicker.css" />
            <!--link rel="stylesheet" href="https://tarc.edu.my/assets/css/bootstrap-datetimepicker.css" /-->
            <!--link rel="stylesheet" href="https://tarc.edu.my/assets/css/colorpicker.css" /-->
            
           
         
      
      
        
		<!-- text fonts -->
		<link rel="stylesheet" href="https://tarc.edu.my/assets/css/ace-fonts.css" />
        
        
 
		<!-- ace styles -->
		<link rel="stylesheet" href="https://tarc.edu.my/assets/css/ace.css" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="https://tarc.edu.my/assets/css/ace-part2.css" class="ace-main-stylesheet" />
		<![endif]-->

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="https://tarc.edu.my/assets/css/ace-ie.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="https://tarc.edu.my/assets/js/ace-extra.js"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
			<script src="https://tarc.edu.my/assets/js/html5shiv.js"></script>
			<script src="https://tarc.edu.my/assets/js/respond.js"></script>
		<![endif]-->
        
        
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,700%7CLato:300,300italic,400,400italic,700,900%7CPlayfair+Display:700italic,900">
    	<link rel="stylesheet" href="https://tarc.edu.my/assets/css/style.css">
    	
        <link rel="stylesheet" href="https://tarc.edu.my/assets/css/print.css" />
        
        
		<link rel="stylesheet" href="https://tarc.edu.my/assets/css/red.css?d=2019/04/22 21:20:19">
      	 <link rel="stylesheet" href="https://tarc.edu.my/assets/css/custom.css" /> 
        
		<!--[if lt IE 10]>
    		<div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    		<script src="https://tarc.edu.my/assets/js/html5shiv.min.js"></script>
		<![endif]-->
		   

  </head>
  <body style="">
    <div class="page">
       
      <header class="page-head">
      	<div class="rd-navbar-wrap">
  <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-clone="false" data-body-class="rd-navbar-static-smooth" data-md-stick-up-offset="60px" data-lg-stick-up-offset="60px" data-md-stick-up="true" data-lg-stick-up="true" class="rd-navbar rd-navbar-default">

  
   
   
    <div class="rd-navbar-inner" style="padding:0;">
    <div class="rd-navbar-panel">
    <button data-custom-toggle=".rd-navbar-nav-wrap" data-custom-toggle-disable-on-blur="true" class="rd-navbar-toggle"><span></span></button>
    
    <a href="https://tarc.edu.my/" class="rd-navbar-brand brand"><span style="float:left;"><img src="https://tarc.edu.my/images/tarcLogo1.png" style="height:40px;width:93.5px;" alt="logo"/></span>
      
        <div id="remark">
       
          <span><font size="2px"><strong>KOLEJ UNIVERSITI TUNKU ABDUL RAHMAN</strong></font>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://tarc.edu.my/images/TAR-UC-Chinese.png" width="85" alt="Logo"><BR>Registration No. DKU023(W)</span>
            <span>Wholly owned by the TARC Education Foundation (Co. Reg. No.: 1033820M)</span>
        </div>
     </a>
    </div>
        
    <div id="notPrint" class="topbar">
     
     <i class="fa fa-language fa-lg blue"></i>
             &nbsp;  <a href="http://tarc.edu.my/privacy-policy.jsp?cat_id=&fmenuid=&fsubid=&flang=BM">BM</a>&nbsp; |&nbsp; 
          
      
     
      	
            <i class="fa fa fa-briefcase"></i>
            <a href="https://tarc.edu.my/career-opportunities/managerial-administrative-staff/" target="_self">Career Opportunities</a>
         &nbsp;|&nbsp;	
            <i class="fa fa fa-globe"></i>
            <a href="https://tarc.edu.my/contact-us/" target="_self">Contact Us</a>
         &nbsp;|&nbsp;	
            <i class="fa fa fa-envelope-o"></i>
            <a href="https://web.tarc.edu.my/portal/gmailSelect.jsp" target="_blank">UC Email</a>
         &nbsp;|&nbsp;	
            <i class="fa https://www.tarc.edu.my/files/tarc/D51BC9C4-2632-4349-97ED-D7ED9E362F16.png"></i>
            <a href="https://web.tarc.edu.my/portal/loginSelect.jsp" target="_blank">Intranet</a>
         &nbsp;|&nbsp;	
            <i class="fa fa fa-comments-o"></i>
            <a href="https://www.tarc.edu.my/feedback.jsp" target="_blank">Feedback</a>
             
        &nbsp;
        </div>   <!-- END topbar -->
    </div>   <!-- END rd-navbar-inner -->
    
    <div id="notPrint" class="rd-navbar-inner" style="padding-top:0;padding-bottom:10px;">

      <div style="height:0;">
       </div>
      <div  class="rd-navbar-group rd-navbar-search-wrap">
        <div  class="rd-navbar-nav-wrap">
          <div class="rd-navbar-nav-inner">
            <div class="rd-navbar-search">
              <!--<form action="search-results.html" method="GET" data-search-live="rd-search-results-live" class="rd-search">-->
              <form action="https://tarc.edu.my/search-results.jsp" id="cse-search-box" method="GET" data-search-live="rd-search-results-live" class="rd-search">
                 <input type="hidden" name="cx" value="001492124851478647991:g9xtqk2kbfu">
                  <input type="hidden" name="cof" value="FORID:10">
                  <input type="hidden" name="ie" value="UTF-8">
                <div class="form-group">
                  <label for="rd-search-form-input" class="form-label">Search...</label>
                  <input id="rd-search-form-input" type="text" name="q" autocomplete="off" class="form-control">
                  <div id="rd-search-results-live" class="rd-search-results-live"></div>
                </div>
                <button type="submit" class="rd-search-submit"></button>
              </form>
              <button data-rd-navbar-toggle=".rd-navbar-search, .rd-navbar-search-wrap" class="rd-navbar-search-toggle"></button>
            </div> <!-- End rd-navbar-search -->
         
         
         
            <ul class="rd-navbar-nav">
             <li class=""><a href="#" target="_self" class="ja">	<span class="fa-stack fa-lg">  		<i class="fa fa-circle fa-stack-2x red"></i> 		<i class="fa fa-comment-o fa-stack-1x fa-inverse"></i>	</span> Why TAR UC</a><ul class="rd-navbar-dropdown" style="margin-top:0;"><!--put fmenu UL--><li><a href="https://tarc.edu.my/why-tar-uc/tar-uc-in-a-nutshell/" target="_self" style="cursor:pointer;" class="ja">TAR UC in A Nutshell</a></li><li><a href="https://tarc.edu.my/why-tar-uc/our-key-strength/" target="_self" style="cursor:pointer;" class="ja">Our Key Strength</a></li><li><a href="https://tarc.edu.my/why-tar-uc/10-seals-of-excellence/" target="_self" style="cursor:pointer;" class="ja">10 Seals of Excellence</a></li><li><a href="https://www.tarc.edu.my/tarc-uc/pictorial-milestones-events/" target="_self" style="cursor:pointer;" class="ja">Notable Milestone</a></li></ul></li><li class=""><a href="https://www.tarc.edu.my/admissions/" target="_self" class="ja">	<span class="fa-stack fa-lg">  		<i class="fa fa-circle fa-stack-2x red"></i> 		<i class="fa fa-book fa-stack-1x fa-inverse"></i>	</span> Apply & Study</a></li><li class=""><a href="#" target="_self" class="ja">	<span class="fa-stack fa-lg">  		<i class="fa fa-circle fa-stack-2x red"></i> 		<i class="fa fa-university fa-stack-1x fa-inverse"></i>	</span> Campuses</a><ul class="rd-navbar-dropdown" style="margin-top:0;"><!--put fmenu UL--><li><a href="https://www.tarc.edu.my/kl/" target="_blank" style="cursor:pointer;" class="ja">Kuala Lumpur Main Campus</a></li><li><a href="https://www.tarc.edu.my/penang/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Penang Branch Campus</a></li><li><a href="https://www.tarc.edu.my/perak/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Perak Branch Campus</a></li><li><a href="https://www.tarc.edu.my/johor/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Johor Branch Campus</a></li><li><a href="https://www.tarc.edu.my/pahang/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Pahang  Branch</a></li><li><a href="https://www.tarc.edu.my/sabah/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Sabah Branch</a></li></ul></li><li class=""><a href="#" target="_self" class="ja">	<span class="fa-stack fa-lg">  		<i class="fa fa-circle fa-stack-2x red"></i> 		<i class="fa fa-university fa-stack-1x fa-inverse"></i>	</span> FACULTY & CENTRE</a><ul class="rd-navbar-dropdown" style="margin-top:0;"><!--put fmenu UL--><li><a href="https://www.tarc.edu.my/fafb/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Faculty of Accountancy, Finance and Business</a></li><li><a href="https://www.tarc.edu.my/foas/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Faculty of Applied Sciences</a></li><li><a href="https://www.tarc.edu.my/focs/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Faculty of Computing and Information Technology</a></li><li><a href="https://www.tarc.edu.my/fobe/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Faculty of Built Environment</a></li><li><a href="https://www.tarc.edu.my/foet/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Faculty of Engineering and Technology</a></li><li><a href="https://www.tarc.edu.my/fcci/" target="_blank" style="cursor:pointer;" class="ja">Faculty of Communication and Creative Industries</a></li><li><a href="https://www.tarc.edu.my/fssh/" target="_blank" style="cursor:pointer;" class="ja">Faculty of Social Science and Humanities</a></li><li><a href="https://www.tarc.edu.my/cpus/" target="_blank" style="cursor:pointer;" class="ja">Centre for Pre-University Studies</a></li><li><a href="https://www.tarc.edu.my/cpsr/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Centre for Postgraduate Studies and Research</a></li><li><a href="https://www.tarc.edu.my/cpe/index.jsp" target="_blank" style="cursor:pointer;" class="ja">Centre for Continuing and Professional Education</a></li><li><a href="https://www.tarc.edu.my/cbiev/" target="_blank" style="cursor:pointer;" class="ja">Centre for Business Incubation and Entrepreneurial Ventures</a></li></ul></li><li class=""><a href="#" target="_self" class="ja">	<span class="fa-stack fa-lg">  		<i class="fa fa-circle fa-stack-2x red"></i> 		<i class="fa fa-graduation-cap fa-stack-1x fa-inverse"></i>	</span> Alumni</a><ul class="rd-navbar-dropdown" style="margin-top:0;"><!--put fmenu UL--><li><a href="https://web.tarc.edu.my/portal/alumni/index.jsp" target="_blank" style="cursor:pointer;" class="ja">TAR UC Alumni</a></li><li><a href="https://web.tarc.edu.my/portal/alumni/letter-of-certification.jsp" target="_blank" style="cursor:pointer;" class="ja">Application for Letter of Certification</a></li><li><a href="#" target="_blank"  style="cursor:default;" class="ja">Convocation</a><ul class="rd-navbar-dropdown" style="margin-top:0;"><!--put fmenusub UL--><li><a href="https://www.tarc.edu.my/convo/index.jsp" target="_self">TAR UC Convocation</a></li><li><a href="https://www.tarc.edu.my/convo/campbell/index.jsp" target="_self">Campbell Convocation</a></li></ul></li><li><a href="https://web.tarc.edu.my/portal/alumni/images/RollofHonours-v26.pdf" target="_blank" style="cursor:pointer;" class="ja">Contributor Roll of Honour</a></li><li><a href="#" target="_self"  style="cursor:default;" class="ja">TAR UC Education Foundation</a><ul class="rd-navbar-dropdown" style="margin-top:0;"><!--put fmenusub UL--><li><a href="https://web.tarc.edu.my/portal/donations/" target="_blank">Donation to TAR UC</a></li></ul></li><li><a href="https://web.tarc.edu.my/portal/alumni/tf-video.jsp" target="_blank" style="cursor:pointer;" class="ja">TARC Education Foundation Video</a></li><li><a href="#" target="_self"  style="cursor:default;" class="ja">Examination Services</a><ul class="rd-navbar-dropdown" style="margin-top:0;"><!--put fmenusub UL--><li><a href="https://www.tarc.edu.my/deca/content.jsp?cat_id=36B60CCA-D7B9-4149-A95C-C842B2B6F7DB" target="_self">Application for Academic Transcript / Application for Replacement Copy of Certificate (Scroll)</a></li><li><a href="https://www.tarc.edu.my/files/tarc/546DBCD2-D758-48FE-AA4C-9BF115D1BC47.pdf" target="_blank">Collection of Certificate (Scroll)</a></li></ul></li></ul></li><li class=""><a href="https://www.tarc.edu.my/library/index.jsp" target="_self" class="ja">	<span class="fa-stack fa-lg">  		<i class="fa fa-circle fa-stack-2x red"></i> 		<i class="fa fa-book fa-stack-1x fa-inverse"></i>	</span> Library</a></li><li class=""><a href="#" target="_self" class="ja">	<span class="fa-stack fa-lg">  		<i class="fa fa-circle fa-stack-2x red"></i> 		<i class="fa fa-globe fa-stack-1x fa-inverse"></i>	</span> About TAR UC</a><ul class="rd-navbar-dropdown" style="margin-top:0;"><!--put fmenu UL--><li><a href="https://tarc.edu.my/tarc-uc/president-welcome-message/" target="_self" style="cursor:pointer;" class="ja">President Welcome Message</a></li><li><a href="https://tarc.edu.my/tarc-uc/philosophy-mission-vision/" target="_self" style="cursor:pointer;" class="ja">Philosophy, Mission & Vision</a></li><li><a href="https://tarc.edu.my/tarc-uc/the-board-of-trustees/" target="_self" style="cursor:pointer;" class="ja">The Board of Trustees</a></li><li><a href="https://tarc.edu.my/tarc-uc/the-board-of-governors/" target="_self" style="cursor:pointer;" class="ja">The Board of Governors</a></li><li><a href="https://www.tarc.edu.my/staffDirectory.jsp" target="_blank" style="cursor:pointer;" class="ja">Staff Directory</a></li></ul></li>
            </ul>
            
         
            <div class="midSide">
            	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/F2870562-A317-49EA-AC29-834357B46773.png"></i>-->
                    <a href="https://tarc.edu.my/contentsub.jsp?cat_id=0AD0CD54-87D2-4527-9744-C54128A87A20&fmenuid=" target="_blank">Application</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/AB138A82-4702-4FAD-A805-D6E1AE5A8404.png"></i>-->
                    <a href="https://www.tarc.edu.my/admissions/programmes/programme-offered-a-z/undergraduate-programme/" target="_self">Undergraduate Programmes Offered</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/F15A559C-D0EB-4B40-9599-1B821FA40984.png"></i>-->
                    <a href="https://www.tarc.edu.my/admissions/programmes/programme-offered-a-z/pre-university-programme/" target="_blank">Pre-University Programme Offered</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/01BF4AB7-945B-45B6-8A0E-F15A3395483B.png"></i>-->
                    <a href="https://www.tarc.edu.my/admissions/programmes/programme-offered-a-z/postgraduate-programme/" target="_blank">Postgraduate Programme Offered</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/326BC591-5426-4BF6-A36E-4E372CE75D83.png"></i>-->
                    <a href="https://www.tarc.edu.my/dsa/content.jsp?cat_id=65E4C10A-7434-46EE-BDF5-86D6E6906C0F&fmenuid=76546648-8707-4F7C-9EFC-ED45DC35BA43" target="_self">Financial Aid</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/6921619C-5A5C-4D59-9D66-DB79E0231FD6.png"></i>-->
                    <a href="https://www.tarc.edu.my/dsa/index.jsp" target="_self">Campus life</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/530A4C64-20A4-45AB-8345-62CEF6DD4D8F.png"></i>-->
                    <a href="https://www.tarc.edu.my/news_archive.jsp?cat_id=56EBCDAD-D3D4-4D3B-8AC4-44CAF115AE1E&fmenuid=63470210-0077-4156-8326-47DE252878A7&ftype=subcontent" target="_self">Highlights</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/C0C47CFE-A52A-4249-8C90-21A222483EAA.png"></i>-->
                    <a href="https://www.tarc.edu.my/international/index.jsp" target="_self">International Students</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/FAECB7FF-1279-4ADE-A0D9-0D92B0697CAB.png"></i>-->
                    <a href="https://www.tarc.edu.my/bulletin.jsp" target="_self">Updates</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/E41C1B2A-75A8-47F1-AF51-1BF1A4D9D20E.png"></i>-->
                    <a href="https://www.jobstarc.com/" target="_blank">Jobs TARC</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/F47F38D2-AB6D-43E0-8ADF-78B3B49D9D2F.png"></i>-->
                    <a href="https://www.tarc.edu.my/cpe/" target="_blank">Centre for Continuing Professional Education</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/764954D5-5B38-44E4-9890-5CE993F6023D.png"></i>-->
                    <a href="https://www.tarc.edu.my/cbiev/" target="_self">Centre for Business Incubation & Entrepreneurial Ventures</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/DD11D925-93E1-42E1-9779-17B44D64842E.png"></i>-->
                    <a href="https://tarc.edu.my/contentsub.jsp?cat_id=76AD185A-7387-46DA-9E34-B808DAF5908F&fmenuid=" target="_self">Research & Development</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/6869F793-C663-4E4F-B037-212ED7C041D7.png"></i>-->
                    <a href="https://web.tarc.edu.my/portal/donations/" target="_blank">Donation to TAR UC</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/9678656B-338B-442E-9F66-E4A34C417D48.png"></i>-->
                    <a href="https://www.tarc.edu.my/mqa/index.jsp" target="_blank">Programmes Approval & Accreditation</a>
            <hr>	
                    <!--<i class="fa https://www.tarc.edu.my/files/tarc/94BABDCF-2EC7-4B26-9B05-5818CB90228F.png"></i>-->
                    <a href="https://web.tarc.edu.my/portal/tarucGo50Shirt/index.jsp" target="_blank">TAR UC Going 50 Special Edition T-shirt Sales</a>
            <br>
            </div>
 				
             <div class="topbarSide">
              <i class="fa fa-language"></i>
                <a href="http://tarc.edu.my/privacy-policy.jsp?cat_id=&fmenuid=&fsubid=&flang=BM">&nbsp;BM</a><br><hr>
           	
                    <i class="fa fa fa-briefcase"></i>
                    <a href="https://tarc.edu.my/content.jsp?cat_id=5B0C6615-ED65-4F4A-88BD-2E30F656588F&fmenuid=A35020BC-8B83-40E0-AEF3-0CDCD757E4E4" target="_self">Career Opportunities</a><br> 
             <hr>	
                    <i class="fa fa fa-globe"></i>
                    <a href="https://tarc.edu.my/content.jsp?cat_id=75DF4BA0-DC1A-46C8-81C3-867293E538FA&fmenuid=" target="_self">Contact Us</a><br> 
             <hr>	
                    <i class="fa fa fa-envelope-o"></i>
                    <a href="https://web.tarc.edu.my/portal/gmailSelect.jsp" target="_blank">UC Email</a><br> 
             <hr>	
                    <i class="fa https://www.tarc.edu.my/files/tarc/D51BC9C4-2632-4349-97ED-D7ED9E362F16.png"></i>
                    <a href="https://web.tarc.edu.my/portal/loginSelect.jsp" target="_blank">Intranet</a><br> 
             <hr>	
                    <i class="fa fa fa-comments-o"></i>
                    <a href="https://www.tarc.edu.my/feedback.jsp" target="_blank">Feedback</a><br> 
             
           
            </div>  <!-- END topbarSide -->
            
            
          </div> <!-- END rd-navbar-nav-inner -->
        </div>  <!-- END rd-navbar-nav-wrap -->
      </div> <!-- END rd-navbar-group rd-navbar-search-wrap -->
    </div>  <!-- END rd-navbar-inner -->
   
  
  
  </nav>
</div>  <!-- END rd-navbar-wrap -->
<script type="text/javascript" src="https://tarc.edu.my/assets/js/gs.js"></script>
       
      </header>
   
 
 
 
      <section style="background-image: url(images/bg-privacy.jpg);" class="section-30 section-sm-40 section-md-66 section-lg-bottom-90 bg-gray-dark page-title-wrap">
        <div class="shell">
          <div class="page-title">
            <h3>Privacy policy</h3>
          </div>
        </div>
      </section>

      <section class="section-90 section-sm-90 section-lg-bottom-120">
	  <div class="shell">
	   <h3 class="text-center"><span>Privacy Policy</span></h3>
	  	
        
 

    <div class="list-order"  style="font-size:13px;"> 
    
      <figure style="position: relative; width: 85%; border: 1px solid #ccc; padding: 10px;  display: inline-block; margin: 10px;  ">
 		<figcaption>
        <p>Pursuant and subject to the Malaysian Personal Data Protection Act, 2010 (PDPA, 2010), this privacy policy provides for the Tunku Abdul Rahman University College&rsquo;s (TAR UC) uses and secure any personally identifiable information that you (the User) may have given to the TAR UC during your visit of our website herein.</p>
        <p>The TAR UC is committed to ensure the personal data security of all Users as required under the laws in Malaysia generally and the PDPA, 2010 specifically.</p>
        <p>The Internet, being an open environment, necessitates that the TAR UC shall not be able to guarantee and warrant that all data collected shall not be accessed, copied, disclosed, altered or otherwise tampered with.</p>
        <p>This Privacy Policy outlines the TAR UC commitment to the safeguard and treatment of the users&rsquo; personal data pursuant to the PDPA, 2010.</p>
        <p><strong><u>Types of data that may be collected</u></strong><br>
          <br>
          Types of data collected may include (but not limited to):</p>
        <ul >
          <li>Name, title, honorific, education background and other related information</li>
          <li>Contact information such as mobile and  phone numbers, house address, email address and other contact details.</li>
          <li>Demographic information such as race, religion, gender, preferences and interests</li>
          <li>Any other information relevant to surveys and/or those related to our products and services inquiries</li>
        </ul>
        <br>
        <p><strong><u>Treatment of collected data</u></strong><br>
          <br>
          By visiting our website, the User consents that the data collected by TAR UC will be utilised to understand the needs and provide the User with better and more accurate service(s).  The data collected will be utilized in the following manner:</p>
        <ul>
          <li>Internal record keeping.</li>
          <li>For products and services offer and/or improvement</li>
          <li>TAR UC may from time to time send promotional materialson new or existing products &amp; services, special offers, events or other information that the TAR UC is of the view that you may find interesting using the contact and other details which you have provided. </li>
          <li>TAR UC may also use your information to contact you for market research and surveys purposes.</li>
          <li>Any other marketing and promotional activities that the TAR UC is of the view that you may find interesting using the details which you have provided.</li>
        </ul>
        <p>The TAR UC shall not sell, distribute, lease or otherwise disclose any data collected herein to any other third parties save and except as outlined above or permitted by the User or is required by law to do so.</p>
        <p>The User&rsquo;s personal information may be utilised by TAR UC to send promotional information about third parties which TAR UC is of the view that User may find interesting if the User allows for the same.</p>
        <p>Under no circumstances will the data collected be sold or manipulated in any way and for any other commercial reasons except as disclosed herein above.</p>
        <p><strong><u>Security</u></strong> <br>
          <br>
          The TAR UC is committed to ensuring that any personal information collected is secured. In order to prevent unauthorised access or disclosure,the TAR UC has put in place, as the present resources and knowledge so permit,  suitable and reasonable physical, electronic and processes to safeguard and secure the said information.</p>
        <p> 
          <strong><u>Cookies</u></strong> <br>
          <br>
          The TAR UC use traffic log cookies to identify which pages are being used. This allows     for data analysis about web page traffic in order to improve the website to tailor it to market needs. The information is for statistical analysis only and shall be removed from the system thereafter. A cookie shall in no way be used to access the User&rsquo;s computer or any information about the User save and except the data the User volunteered to share.</p>
        <p>The User may accept or decline cookies. Most web browsers automatically accept cookies, but may be modified to decline selected cookies. This may, however, prevent the User from taking full advantage of the website.</p>
        <p> 
          <strong><u>Links to other websites</u></strong><br>
          <br>
          The TAR UC website may contain links to other websites of interest. Please note that the TAR UC do not have any control over that other Third Party website. Therefore, The TAR UC cannot be responsible for the protection and privacy of any information which the User provided whilst visiting such sites and such sites are not governed by this privacy statement.</p>
        <p> 
          <strong><u>Controlling of personal information</u></strong><br>
          <br>
          The User may choose to restrict the collection or use of your personal information in the following ways:</p>
        <ul>
          <li>whenever the User is asked to fill in a form on the website, check the box provided to disallow consent for  the information to be provided to be used by anybody for direct marketing purposes</li>
          <li>if the User has previously agreed to TAR UC using the personal information collected, the User may change his mind at any time by writing to or emailing the same to <a href="mailto:pdpa@tarc.edu.my"> pdpa@tarc.edu.my</a></li>
        </ul>
       <p> The User may request details of personal information which the TAR UC holds about the User under the PDPA, 2010 by sending such request via email to <a href="mailto:pdpa@tarc.edu.my" target="new">pdpa@tarc.edu.my</a> An administrative fee shall be payable. In the event the User believes that any information of the User envisaged herein in the TAR UC&rsquo;s possession is incorrect, inaccurate and or incomplete, please write to or email the TAR UC as soon as possible, at the above address or <a href="mailto:pdpa@tarc.edu.my">pdpa@tarc.edu.my</a> for the TAR UC&rsquo;s immediate and prompt action.</p>
     
     
     	<p><u><strong>Privacy Notice</strong></u></p>
        <p>The Tunku Abdul Rahman University College and its Holding Company the TARC Education Foundation (collectively herein referred to as <strong>TAR UC</strong>) is committed to ensure your personally identifiable information (personal data) be treated in accordance with the present legal and regulatory demands in force in Malaysia.</p>
        <p>Whilst the TAR UC already has in place a <u>privacy policy</u> before the Malaysian Personal Data Protection Act, 2010 (PDPA, 2010) came into force; the TAR UC, nevertheless is taking further steps to ensure compliance with the PDPA, 2010.</p>
        <p>This serves as a notice to you informing how the TAR UC may be processing and treating your personal data. The TAR UC processes your personal data such as name, identity card information, passport information, title, honorific, education background, records &amp; performances and skills; contact information, mobile &amp; phone numbers, house address, email address, mobile contacts like whatsapp, twitter, google+, viber, skype, wechat, facebook and/or linkedin; demographic information such as race, religion, gender, preferences and interests and financial background AND any other information relevant to you being a student, graduate, alumnus, staff, vendor, contractor, agent, partner or affiliate (whichever is applicable) which you have in the past provided and consented to provide and/or which personal data derived and arising out of your contract, contact/ relation with TAR UC.</p>
           <p>  Please be informed that the personal data will be processed by the TAR UC for the following purposes (<strong>Purposes</strong>): 
             
             <ol style="list-style:lower-roman">
                <li>Internal record keeping and maintenance. Such process may include but not limited to updating and managing the accuracy of the TAR UC records.
              	</li>
                <li>For the TAR UC products and services offer and/or improvement. TAR UC may from time to time send promotional materials on new or existing products &amp; services, special offers, events or other information that the TAR UC is of the view that you may find interesting using the personal data which you have provided.
              	</li>
                <li>TAR UC may use your personal data to communicate with you.</li>
              	<li>TAR UC may also use your personal data to contact you (for academic or market research and surveys purposes subject always to your express consent to participate in the same).</li>
              	<li>Any other marketing and promotional activities that the TAR UC is of the view that you may find interesting using the personal data and details which you have provided.</li>
              <li>Prevention, detection and prosecution of Offences or Crimes, and compliance with legal, statutory, regulatory and contractual obligations.</li>
              <li>Maintain your academic, scholastic and disciplinary background (applicable to students, graduates and Alumnus only).</li>
              <li>Maintain your academic, scholastic, employment and disciplinary records (applicable to staff and former staff only ).</li>
              <li>Protecting TAR UC's interest and/or other ancillary and related purposes.</li>
              </ol>
          </p>
          
         <p align="justify">Further, your personal data may be disclosed to the TAR UC&rsquo;s strategic partners, professional advisers such as lawyers, accountants and auditors, governmental agencies and or vendors whether within or outside Malaysia directly or via their agents, representatives or servants for the <strong>Purposes</strong>.</p>
        <p align="justify">In certain circumstances, you may have provided the TAR UC personal data relating to others (such as spouse, parent(s), guardian(s) and/or sibling(s) for the <strong>Purposes. </strong>For thesepersonal data, you expressly warrant and represent to the TAR UC that you have permission, consent or assent from the same to provide such personal data to the TAR UC.</p>
        <p align="justify">We trust that you agree and consent to the above about how your personal data is processed by the TAR UC.</p>
        <p align="justify">You may request details of your personal data which the TAR UC holds about you under the PDPA, 2010 by sending such request via email to <a href="mailto:pdpa@tarc.edu.my">pdpa@tarc.edu.my</a> and may also request for the correction, deletion and update of your personal data in the TAR UC's possession, which is incorrect, inaccurate and or incomplete provided always that your personal data under items (vii) and (viii) above shall not be applicable to this statement herein.</p>
        <p align="justify">An administrative fee shall be payable.</p>
        <p align="justify">The TAR UC reserves the absolute right and discretion to alter, change, add, subtract or otherwise modify this notice and/or the privacy policy.</p></td>
  		
       
        </figcaption>
        </figcaption>
       </div>
       
      </section> 
	  


		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="https://tarc.edu.my/assets/js/jquery.js"></script>
		<!-- <![endif]-->

		<!--[if IE]>
<script src="https://tarc.edu.my/assets/js/jquery1x.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='https://tarc.edu.my/assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<!--checking in progress enabled by default<script src="https://tarc.edu.my/assets/js/bootstrap.js"></script>-->

		<!-- page specific plugin scripts -->

		<!-- ace scripts -->
		<script src="https://tarc.edu.my/assets/js/ace/elements.scroller.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/elements.colorpicker.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/elements.fileinput.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/elements.typeahead.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/elements.wysiwyg.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/elements.spinner.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/elements.treeview.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/elements.wizard.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/elements.aside.js"></script>

		<script src="https://tarc.edu.my/assets/js/ace/ace.ajax-content.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.touch-drag.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.sidebar.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.sidebar-scroll-1.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.submenu-hover.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.widget-box.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.settings.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.settings-rtl.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.settings-skin.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.widget-on-reload.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.searchbox-autocomplete.js"></script>

		<!-- inline scripts related to this page -->
        
        <section class="" style="padding:15px;border:1px solid #e30613;border-width:5px 0 0 0;"">
        <div class="text-sm-center">
              <div class="group-sm group-middle">
                <ul class="list-inline list-inline-reset">
                  <li><a target="_blank" href="https://www.facebook.com/tunkuabdulrahmanuniversitycollege" class="icon icon-circle icon-gray-dark-filled icon-xxs-smallest fa fa-facebook"></a></li>
                  <li><a target="_blank" href="https://www.youtube.com/channel/UCqjsPpVnwjCRT5mgAgFo1ng" class="icon icon-circle icon-gray-dark-filled icon-xxs-smallest fa fa-youtube"></a></li>
                   <li><a target="_blank" href="https://www.instagram.com/taruc_official/" class="icon icon-circle icon-gray-dark-filled icon-xxs-smallest"><img src="https://www.tarc.edu.my/images/instagram-2.png" /></a></li>
                  
                </ul>
              </div>
            </div>
      </section>
      
		<footer class="page-foot page-foot-default section-35 bg-gray-base">
        <div class="shell">
          <div class="range">
           
            
            <div class="cell-xs-12" style="padding:0px;" id="TARUCGO50">
				<div class="col-xs-12 col-sm-3 col-md-3" style="padding:0;background:#000;">
					<div class="col-xs-4"></div>
					<div class="col-xs-4 col-sm-12 col-md-12" style="padding:0;">
						<img src="https://www.tarc.edu.my/images/50th_taruc_gold.png" width="130px">
						<!--<a href="https://www.tarc.edu.my/event2019.jsp" target="_blank"><img src="https://www.tarc.edu.my/images/50th-anniversary-calendar.png" width="130px"></a>-->                
					</div>
					<div class="col-xs-4"></div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6" style="padding-top:36px;background:#000;">
					<p class="small" align="center">
						<span><a href="https://www.tarc.edu.my">TAR UC</a></span><span>&nbsp;©&nbsp;</span><span id="copyright-year">2019</span> <span>All Rights Reserved</span><br class="veil-sm"> <a id="notPrint" href="https://www.tarc.edu.my/disclaimer.jsp" class="link-primary-inverse">Disclaimer</a><span id="notPrint"> and </span><a id="notPrint" href="https://www.tarc.edu.my/privacy-policy.jsp" class="link-primary-inverse">Privacy Policy</a>
					</p>

				</div>
				<div class="col-xs-12 col-sm-3 col-md-3" style="padding:0;background:#000;">
					&nbsp;
				</div>
            <style>
#kl-countdown {
    color: #FC0; border: none; background-color: transparent; position: relative; margin-top: -42px; width: 100%;
    float: left; line-height: 15px;
	font-size:20px;
}
#kl-countdown .countdown-section { font-size: 70%; }
.kl-countdown { min-height: 68px; }

.kl-img-fw{width:100%;}

.is-countdown {
	border: 1px solid #ccc;
	background-color: #eee;
}
.countdown-rtl {
	direction: rtl;
}
.countdown-holding span {
	color: #888;
}
.countdown-row {
	clear: both;
	width: 100%;
	padding: 0px 2px;
	text-align: center;
}
.countdown-show1 .countdown-section {
	width: 98%;
}
.countdown-show2 .countdown-section {
	width: 48%;
}
.countdown-show3 .countdown-section {
	width: 32.5%;
}
.countdown-show4 .countdown-section {
	width: 24.5%;
}
.countdown-show5 .countdown-section {
	width: 19.5%;
}
.countdown-show6 .countdown-section {
	width: 16.25%;
}
.countdown-show7 .countdown-section {
	width: 14%;
}
.countdown-section {
	display: block;
	float: left;
	font-size: 75%;
	text-align: center;
}
.countdown-amount {
    font-size: 200%;
}
.countdown-period {
    display: block;
}
.countdown-descr {
	display: block;
	width: 100%;
}

@media print{
 
#TARUCGO50 {
display:none}



}
    </style>
            
                                     
           </div>
           
           
           <script type="text/javascript">
            $(document).ready(function() {
                    var AnniversaryDate = new Date(
                        parseInt('2019'), // year
                        (parseInt('03') - 1), // month
                        parseInt('17'), // day
                        parseInt('10'), // hours
                        parseInt('00'), // mins
                        parseInt('00') // seconds
					);
					
					
					function serverTime() { 
					//out.print server time
					var server = new Date("Apr 22, 2019 21:20:19 +0800"); 
					//server.setMinutes(server.getMinutes() + 5); 
					server.setSeconds(server.getSeconds() + 0); 
					return server; 
					}
			
                    $('#kl-countdown').countdown({until: AnniversaryDate, serverSync: serverTime});
                
                
            });
        </script>
			<!--<div class="cell-xs-12 text-center">
              <p class="rights small"><span><a href="https://www.tarc.edu.my">TAR UC</a></span><span>&nbsp;&#169;&nbsp;</span><span id="copyright-year">2016</span><span>All Rights Reserved</span><br class="veil-sm"><a id="notPrint" href="https://tarc.edu.my/disclaimer.jsp" class="link-primary-inverse">Disclaimer</a><span id="notPrint">and</span><a id="notPrint" href="https://tarc.edu.my/privacy-policy.jsp" class="link-primary-inverse">Privacy Policy</a>
              </p>
            </div>-->
          </div>
        </div>
      </footer>

    </div>
    <div id="form-output-global" class="snackbars"></div>
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    
    <script src="https://tarc.edu.my/assets/js/core.min.js"></script>
    <script src="https://tarc.edu.my/assets/js/script.js"></script>
		<script src="https://tarc.edu.my/assets/js/ace/ace.js"></script>
  </body>
</html>

