<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" id="vbulletin_html" xml:lang="en">
<head>
		<script type='text/javascript'>
		var googletag = googletag || {};
		googletag.cmd = googletag.cmd || [];
		(function() {
		var gads = document.createElement('script');
		gads.async = true;
		gads.type = 'text/javascript';
		var useSSL = 'https:' == document.location.protocol;
		gads.src = (useSSL ? 'https:' : 'http:') + 
		'//www.googletagservices.com/tag/js/gpt.js';
		var node = document.getElementsByTagName('script')[0];
		node.parentNode.insertBefore(gads, node);
		})();
		</script>
		<script type='text/javascript'>
		googletag.cmd.push(function() {
				googletag.defineSlot('/1004159/Header_Leaderboard_ROS', [728, 90], 'div-gpt-ad-1414604708020-0').addService(googletag.pubads());
				googletag.pubads().enableSingleRequest();
		googletag.enableServices();
		});
		</script>
<title>BabyNames.com Privacy Policy</title> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="description" content="Boy names, girl names, and unique baby names. Search Baby Names, meanings and origins. Top Most Popular Baby Names" />
<meta name="keywords" content="Baby Names, Name Meanings, Baby Boy Names, Baby Girl Names, Unique Baby Names, Name Origins, PregnancyBaby Names, Name Meanings, Baby Boy Names, Baby Girl Names, Unique Baby names, Name origins, pregnancy" />
<meta name="classification" content="Baby Names, Names, Pregnancy, Parenting" /> 
<meta name="google-site-verification" content="zd-JzxK5PfujkWXaQTSNtbHJqkRTY6gVPcD9FuUY3ZQ" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="alternate" media="only screen and (max-width: 640px)"
      href="http://m.babynames.com/about/privacy.php" >
<link rel="stylesheet" href="/_assets/menu/styles.css" />
<link rel="stylesheet" href="/_assets/css/styles.css?1913838487" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700,700italic,400italic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Oswald:400,700" rel="stylesheet" type="text/css" />
<link href="/_assets/themes/2010/bn-ui.css" rel="stylesheet" type="text/css" />
<link href="/_assets/css/screen.css" rel="stylesheet" type="text/css" media="Screen" />
<link href="/_assets/css/mobile.css" rel="stylesheet" type="text/css" media="handheld" />
<link href="/_assets/js/jquery/jquery-ui.min.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/_assets/js/jquery/external/jquery/jquery.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/_assets/js/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="/_assets/menu/script.js" ></script>
<script type="text/javascript" src="/_assets/js/bn-menu.js"></script>
<script type="text/javascript" src="/_assets/js/common.js"></script>
<script type="text/javascript" src="/_assets/js/namelist-pop.js"></script>
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="icon" href="/favicon.png" />
<link rel="icon" sizes="57x57" href="/_assets/favicon/favicon-32x32.png" />
<link rel="icon" sizes="57x57" href="/_assets/favicon/favicon-57x57.png" />
<link rel="icon" sizes="72x72" href="/_assets/favicon/favicon-72x72.png" />
<link rel="icon" sizes="76x76" href="/_assets/favicon/favicon-76x76.png" />
<link rel="icon" sizes="114x114" href="/_assets/favicon/favicon-114x114.png" />
<link rel="icon" sizes="120x120" href="/_assets/favicon/favicon-120x120.png" />
<link rel="icon" sizes="144x144" href="/_assets/favicon/favicon-144x144.png" />
<link rel="icon" sizes="152x152" href="/_assets/favicon/favicon-152x152.png" />
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="/_assets/favicon/favicon-144x144.png" />
<meta name="application-name" content="BabyNames.com" />
<script src='https://www.google.com/recaptcha/api.js'></script>
</head><body>
<!-- GOOGLE ANALYTICS -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-710528-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- GOOGLE ANALYTICS -->

<!-- COMSCORE -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "11307734" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=11307734&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- QUANTCAST -->
<script type="text/javascript">
_qoptions={
qacct:"p-28sMnkrViazWA"
};
</script>
<script type="text/javascript" src="http://edge.quantserve.com/quant.js"></script>
<noscript>
<a href="http://www.quantcast.com/p-28sMnkrViazWA" target="_blank"><img src="http://pixel.quantserve.com/pixel/p-28sMnkrViazWA.gif" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/></a>
</noscript>

<!-- COMPETE.COM -->
<script type="text/javascript">
    __compete_code = '6e2400b3e0e5f1fa24265c2742b9ac9a';
    (function () {
        var s = document.createElement('script'),
            d = document.getElementsByTagName('head')[0] ||
                document.getElementsByTagName('body')[0],
            t = 'https:' == document.location.protocol ? 
                'https://c.compete.com/bootstrap/' : 
                'http://c.compete.com/bootstrap/';
        s.src = t + __compete_code + '/bootstrap.js';
        s.type = 'text/javascript';
        s.async = 'async'; 
        if (d) { d.appendChild(s); }
    }());
</script>

<!-- ALEXA -->
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript" src="https://d31qbv1cthcecs.cloudfront.net/atrk.js"></script><script type="text/javascript">_atrk_opts = { atrk_acct: "Y3E6h1acOh002d", domain:"babynames.com"}; atrk ();</script><noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Y3E6h1acOh002d" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->


<!-- ADD THIS SOCIAL SHARING SETUP -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e4bef2600bfe229" async="async"></script>
<div id="topstrip">
   <div id="topstrip-inner">
		<div class="icon"><a href="/"><img src="/_assets/image/bn-icon.png" width="33" height="31" border="0"></a></div>
		<div class="loginbox">
					<form action="/user/login.php" method="post">
				<input type="hidden" name="auth_submit" value="1" />
				username <input id="login-email" class="top-email" type="text" name="user_email" />
				&nbsp; password <input id="login-password" class="top-pass" type="password" name="user_password" />
				<input id="login-remember" type="checkbox" name="rem" value="yes" />stay logged in
				&nbsp; <input name="submit" class="smallbutton" type="submit" value="Log in" alt="BabyNames.com Member Login" />
				<a href="/user/join.php" class="smallbutton joinbutton">Become a Member</a>
			</form>
				</div> 

		<div class="socialicons">
			<a href="http://www.facebook.com/babynamesdotcom" TARGET="_BLANK"><img src="/_assets/image/soc-facebook.png" onmouseover="this.src='/_assets/image/soc-facebook-t.png'" onmouseout="this.src='/_assets/image/soc-facebook.png'" border="0" height="33" width="33"></a>
			<a href="http://www.twitter.com/babynamesdotcom" TARGET="_BLANK"><img src="/_assets/image/soc-twitter.png" onmouseover="this.src='/_assets/image/soc-twitter-t.png'" onmouseout="this.src='/_assets/image/soc-twitter.png'" border="0" height="33" width="33"></a>
			<a href="http://www.pinterest.com/babynamesdotcom" TARGET="_BLANK"><img src="/_assets/image/soc-pinterest.png" onmouseover="this.src='/_assets/image/soc-pinterest-t.png'" onmouseout="this.src='/_assets/image/soc-pinterest.png'" border="0" height="33" width="33"></a>
			<a href="http://www.instagram.com/babynamesdotcom" TARGET="_BLANK"><img src="/_assets/image/soc-instagram.png" onmouseover="this.src='/_assets/image/soc-instagram-t.png'" onmouseout="this.src='/_assets/image/soc-instagram.png'" border="0" height="33" width="33"></a>
			<a href="http://www.youtube.com/babynamesdotcom" TARGET="_BLANK"><img src="/_assets/image/soc-youtube.png" onmouseover="this.src='/_assets/image/soc-youtube-t.png'" onmouseout="this.src='/_assets/image/soc-youtube.png'" border="0" height="33" width="33"></a>
			<a href="https://itunes.apple.com/au/app/baby-names-from-babynames.com/id509942454?mt=8&ign-mpt=uo%3D2" TARGET="_BLANK"><img src="/_assets/image/soc-apple.png" onmouseover="this.src='/_assets/image/soc-apple-t.png'" onmouseout="this.src='/_assets/image/soc-apple.png'" border="0" height="33" width="33"></a>
			<a href="https://play.google.com/store/apps/details?id=com.babynames&hl=en" TARGET="_BLANK"><img src="/_assets/image/soc-android.png" onmouseover="this.src='/_assets/image/soc-android-t.png'" onmouseout="this.src='/_assets/image/soc-android.png'" border="0" height="33" width="33"></a>
		</div>
	</div> <! -- topstrip inner -->
	<div class="clearall"></div>
</div>  <! -- topstrip -->
<div id="sitebackground">
<div id="contentwrapper">
	<div id="BNheader">
		<div class="logo"><a href="/"><img src="/_assets/logo/bnlogo_web.jpg" width="300" height="96" alt="BabyNames.com" /></a></div>
		<div class="leaderboard">
			<div id='div-gpt-ad-1414604708020-0' style='width:728px; height:90px;'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1414604708020-0'); });
</script>
</div>
    		</div>
		<div class="clearall"></div>
		<div id="searchitems">
		<form action="/Names/name-index.php" method="post" enctype="application/x-www-form-urlencoded">
			<div id="namesearch">
					<input id="btn-search" type="image" src="/_assets/image/searchimg.png" height="36" width="36" alt="search" onMouseOver="this.src='/_assets/image/searchimg_hover.png'" onMouseOut="this.src='/_assets/image/searchimg.png'" />&nbsp;<input id="standard-search" type="text" name="starts" value="Enter name or starts with" onFocus="this.value=''" onBlur="if(this.value=='')this.value='search name or starts with...'" /> 
			</div>
			<ul class="browsebyletter">
					<li><a class="letter" href="/Names/A/">a</a></li>
					<li><a class="letter" href="/Names/B/">b</a></li>
					<li><a class="letter" href="/Names/C/">c</a></li>
					<li><a class="letter" href="/Names/D/">d</a></li>
					<li><a class="letter" href="/Names/E/">e</a></li>
					<li><a class="letter" href="/Names/F/">f</a></li>
					<li><a class="letter" href="/Names/G/">g</a></li>
					<li><a class="letter" href="/Names/H/">h</a></li>
					<li><a class="letter" href="/Names/I/">i</a></li>
					<li><a class="letter" href="/Names/J/">j</a></li>
					<li><a class="letter" href="/Names/K/">k</a></li>
					<li><a class="letter" href="/Names/L/">l</a></li>
					<li><a class="letter" href="/Names/M/">m</a></li>
					<li><a class="letter" href="/Names/N/">n</a></li>
					<li><a class="letter" href="/Names/O/">o</a></li>
					<li><a class="letter" href="/Names/P/">p</a></li>
					<li><a class="letter" href="/Names/Q/">q</a></li>
					<li><a class="letter" href="/Names/R/">r</a></li>
					<li><a class="letter" href="/Names/S/">s</a></li>
					<li><a class="letter" href="/Names/T/">t</a></li>
					<li><a class="letter" href="/Names/U/">u</a></li>
					<li><a class="letter" href="/Names/V/">v</a></li>
					<li><a class="letter" href="/Names/W/">w</a></li>
					<li><a class="letter" href="/Names/X/">x</a></li>
					<li><a class="letter" href="/Names/Y/">y</a></li>
					<li><a class="letter" href="/Names/Z/">z</a></li>
			</ul>
		</form>
		</div>
	<div class="clearall"></div>
</div> <!- BNheader ->
<div id="BNnav">
<div id="cssmenu">
<ul>
   <li class="has-sub"><a href="/Names/search.php">Find Names</a>
      <ul>
         <li><a href="/Names/search.php">Advanced Search</a></li>
         <li><a href="/Names/Popular/">Most Popular Names</a></li>
         <li><a href="/Names/Popular/around-the-world.php">Top World Names</a></li>
         <li><a href="/boy-names/">Boy Names</a></li>
         <li><a href="/girl-names/">Girl Names</a></li>
      </ul>
   </li>
   <li class="has-sub"><a href="#">Name Advice</a>
      <ul>
         <li><a href="/blogs/ask-babynames/">Ask BabyNames</a></li>
         <li><a href="/boards/">Message Boards</a></li>
         <li><a href="/Names/glossary.php">Name Glossary</a></li>
         <li><a href="/baby-name-book.php">Baby Names Book</a></li>
         <li><a href="/lists/character-names.php">Tips for Writers</a></li>
      </ul>
   </li>
   <li class="has-sub"><a href="#">Celebrities</a>
      <ul>
         <li><a href="/Names/Celebrities/celebrity-baby-names.php">Celeb Baby Names</a></li>
         <li><a href="/Names/Celebrities/">Celeb Real Names</a></li>
         <li><a href="/blogs/celebrities">Celebrity Baby News</a></li>
      </ul>
   </li>
   <li class="has-sub"><a href="/lists/">Cool Name Lists</a>
		<ul>			
				<li><a href="/lists/">See All Lists</a></li>
                <li><a href="/lists/game-of-thrones.php">Game of Thrones</a></li>
                <li><a href="/lists/lord-of-the-rings.php">Lord of the Rings</a></li>
                <li><a href="/NSI/NSI_showlist.php?listid=17">Harry Potter Names</a></li>
                <li><a href="/lists/hunger-games.php">Hunger Games Names</a></li>
                <li><a href="/lists/twilight-names.php">Twilight Names</a></li>
                <li><a href="/lists/shakespeare-names.php">Shakespeare Names</a></li>
				<li><a href="/lists/valentines-names.php">Valentine's Names</a></li>
                <li><a href="/lists/presidents-names.php">President's Names</a></li>
                <li><a href="/lists/black-history-names.php">Black History Names</a></li>
                <li><a href="/lists/mother-names.php">Mother Names</a></li>
                <li><a href="/lists/father-names.php">Father Names</a></li>
                <li><a href="/lists/spooky-names.php">Spooky Names</a></li>
                <li><a href="/lists/christmas-names.php">Christmas Names</a></li>
                <li><a href="/lists/character-names.php">Character Names</a></li>
		</ul>
	</li>

   <li class="has-sub"><a href="#">Community/Fun</a>
      <ul>
         <li><a href="/boards/">Message Boards</a></li>
         <li><a href="/features/track-my-baby.php">Track My Baby</a></li>
         <li><a href="/Names/rename.php">Random Renamer</a></li>
         <li><a href="/baby-shower-games.php">Baby Shower Games</a></li>
         <li><a href="/Names/namelist_vote.php">Vote on NameLists</a></li>
         <li><a href="/shopping/">Baby Bodysuit Shop</a></li>
      </ul>
   </li>
   <li class="has-sub"><a href="/blogs/">Baby Blogs</a>
      <ul>
         <li><a href="/blogs/celebrities/">Celebrity Baby Blog</a></li>
         <li><a href="/blogs/ask-babynames/">Ask BabyNames</a></li>
         <li><a href="/blogs/baby-tech/">Baby Tech Blog</a></li>
         <li><a href="/blogs/beauty/">Moms' Beauty Blog</a></li>
         <li><a href="/blogs/books/">Kids' Book Reviews</a></li>
      </ul>
   </li>
   <li class="has-sub"><a href="/user/">My BabyNames</a>
      <ul>
	<li><a href="/user/login.php">Login</a></li>
	<li><a href="/user/join.php">Become a Member</a></li>
        <li><a href="/Names/namelist.php">My NameList</a></li>
        <li><a href="/user/viewprofile.php">My Profile</a></li>
        <li><a href="/user/mypix.php">My Face Pix</a></li>
        <li><a href="/features/track-my-baby.php">Track My Baby</a></li>
        <li><a href="/user/myprofile.php">My Settings</a></li>
      </ul>
   </li>
</ul>
</div>
</div>
<div id="BNpage"><div ID="contentfull">
<h1>Your Privacy</h1>
<P class="subhead">This site is owned and operated by BabyNames.com LLC, a California Limited Liability Company. </p>
<span class="small">rev. November 2014 </span>

						<br />

						<p class="paragraph"><strong>BabyNames.com</strong> has created this statement in order to demonstrate our firm commitment to protecting the privacy of our visitors. The following summarizes our information-gathering and dissemination practices for our website.</p>

						<h2 class="dotted">Your Personal Information</h2>
						<p class="paragraph">Demographic and profile data collected at our site is used for the demographic purposes and order processing. The <strong>BabyNames.com</strong> feedback form, order forms, personal name list and message boards require users to enter first name, last name and a valid email address. All personal and demographic information gathered through any type of product ordering process remains the property of BabyNames.com LLC and may be used to send you site newsletters and special offers from our business partner companies. 

						</p>


						<h2 class="dotted">User Profiles</h2>
						<p class="paragraph">Upon registration, BabyNames.com creates a public user profile page for you. Each part of your profile can be hidden from public view by logging in and going to MY PROFILE and then PRIVACY SETTINGS.  The following fields are set to public by default, but can be changed at any time:
							<UL>
								<LI>Your Profile Picture</li>
								<LI>First Name</li>
								<LI>Gender</li>
								<LI>Parent Type/Family Type</li>
								<LI>Interests</li>
								<LI>Online Status</li>
								<LI>Community Title & Badges</li>
								<LI># Message Board Posts</li>
								<LI>Last Message Board Activity</li>
								<LI>VIP Expire Date</li>
							</ul>

						<h2 class="dotted">Your IP Address</h2>
						<p class="paragraph"><strong>BabyNames.com</strong> records user IP addresses to help diagnose problems with our server, administer our site and identify malicious visitors on the message boards. IP addresses remain private, and are not displayed or posted on the site to any other user.</p>

						<h2 class="dotted">Links</h2>
						<p class="paragraph">This site contains advertising and links to other sites, however we assume no liability and are not responsible for any practices of such third party Web sites, including their privacy practices or content.</p>

						<h2 class="dotted">Security</h2>
						<p class="paragraph"><strong>BabyNames.com</strong> has security measures in place to protect the loss, misuse and alteration of the information by third parties. We use SSL encryption, firewall intervention, software and hardware redundancy and a comprehensive data backup schedule. Nonetheless, we cannot guarantee the protection of such information against loss, misuse or alteration.</p>

						<h2 class="dotted">Opt-In Mailing List</h2>
						<p class="paragraph">You have the option to add or remove yourself from the monthly newsletter and mailings for special discounts and offers from our partners. We take great care in making sure our users do not receive unwanted email. BabyNames.com sends a weekly newsletter to our members who have opted in to receive it. We will never opt you into our email communications without your permission and we will never sell your information to third parties. IF you receive a special offer from one of our partners, it will always come from our server, as we send them out on the advertiser's behalf. We also ensure that every email you receive from BabyNames.com contains a link to remove yourself from the mailing list. To manage your BabyNames.com email settings, go to our <a href="http://www.babynames.com/newsletter/">Newsletter Page</a>. If you are a member, you can also login and change your opt-in settings on your profile page.

						<h2 class="dotted">Children's Guidelines</h2>
						<p class="paragraph"><strong>BabyNames.com</strong> discourages users under the age of 13 from entering any personal information without consent from a parent or legal guardian. <strong>BabyNames.com</strong> does not condone posting or distribution of personally identifiable contact information of any individual, including baby's surnames. BabyNames.com does not entice (by the prospect of a special game, prize or other activity) any person to divulge more information than is needed to participate in any given activity.</p>

						<h2 class="dotted">Use of Cookies</h2>
						<p class="paragraph">Like many websites, BabyNames.com stores several cookies on your computer so that we can remember who you are when you return to the site.  If you choose to have BabyNames.com remember your login information, that information is stored within your browser software. Your password is encrypted and cannot be viewed or processed by any other person or website.  Other cookies include your BabyNames session id, so we can keep track of what names you have voted on. The Google advertising network uses a DoubleClick DART cookie in the ads served by Google on the BabyNames.com website. When users visit our site and view or click on a Google ad, a cookie may be dropped on the user's browser. The data gathered from these cookies enables Google to serve ads based on your visit to BabyNames and other sites on the Internet.  You may opt out of the use of the Doubleclick DART cookie by <a href="http://www.google.com/privacy_ads.html" target="_NEW">visiting the Google ad and content network privacy policy</a>.</p> 

						<h2 class="dotted">Third-Party Advertisers</h2>
						<p class="paragraph">BabyNames.com allows third-party companies to collect certain information when you visit our web site. These companies may utilize cookies, pixels or other technologies to collect and use non-personally identifiable information (e.g., hashed data, click stream information, browser type, time and date, subject of advertisements clicked or scrolled over) during your visits to this and other web sites in order to provide advertisements about goods and services likely to be of greater interest to you.  To learn more about this behavioral advertising practice or to opt-out of this type of advertising, you can visit the websites of the <a href="http://www.aboutads.info" target="_BLANK">Digital Advertising Alliance</a> or the <a href="http:// www.networkadvertising.org/choices" target="_BLANK">Networking Advertising Initiative</a>.
						</p>
	
						<h2 class="dotted">Removal of Personal Information</h2>
						<p class="paragraph">Users can have all their personal information, message board posts and images removed by closing their account on BabyNames.com.  To easily close your account and remove all your information, <a href="/user/removeme.php">click here</a>.  Once an account is closed by a user, their data is immediately deleted from the BabyNames.com servers and cannot be recovered.</p>

						<h2 class="dotted">How to Contact BabyNames.com</h2>
						<p class="paragraph">If you have any questions about this privacy statement, the practices of this site, or your dealings with this Web site, you may contact: <a href="mailto:support@babynames.com">support@babynames.com</a>.</p>

						<p class="paragraph">Questions regarding this statement should be directed to BabyNames.com User Support Services department:</p>
						<p class="paragraph"><strong>BabyNames.com</strong><br />User Support Services<br />PO BOX 1147<br />Oakhurst, CA 93644<br />(510) 51-NAMES (510-516-2637)<br /><a href="mailto:support@babynames.com">support@babynames.com</a></p>
</div>
	</div> <! -- BNpage -->
</div> <! -- contentwrapper -->
</div> <!-- site background -->
<div id="footer">
	<div class="footinner">
		<div class="logo"><a href="/"><img src="/_assets/image/footer-logo.png" border="0" alt="BabyNames.com"></a>
			<div class="socialfooter"><a href="http://www.facebook.com/babynamesdotcom"><img src="/_assets/image/footer-icon-facebook.png" border="0"></a>&nbsp; &nbsp;<a href="http://www.twitter.com/babynamesdotcom"><img src="/_assets/image/footer-icon-twitter.png" border="0"></a>&nbsp; &nbsp;<a href="http://www.instagram.com/babynamesdotcom"><img src="/_assets/image/footer-icon-instagram.png" border="0"></a>&nbsp; &nbsp;<a href="http://www.pinterest.com/babynamesdotcom"><img src="/_assets/image/footer-icon-pinterest.png" border="0"></a>
			</div>
		</div>
		<div class="links1">
				<a href="/about/">About BabyNames.com</a> 
				<a href="/advertise/">Advertise</a>
				<a href="/about/press.php">Press</a> 
				<a href="/about/licensing.php">Data Licensing</a>  
		</div>
		<div class="links2">	
				<a href="/about/privacy.php">Privacy Policy</a>
				<a href="/about/terms.php">Terms of Service</a> 
				<a href="/about/feedback.php">Contact Us</a>
				<a href="/usemobilesite.php">Use Mobile Site</a>
		</div>
		<div class="copyright">All site content &copy;1996-2016<BR />BabyNames.com LLC<BR />All Rights Reserved</div>
	</div> <!- footinner -->
</div> <!- BNfooter -->


<!-- DATONICS -->
<script type="text/javascript" src="http://ads.pro-market.net/ads/scripts/site-132491.js"></script>
