

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>


<script type="text/javascript" src="/static/js/analytics.js"></script>
<script type="text/javascript">archive_analytics.values.server_name="wwwb-app54.us.archive.org";archive_analytics.values.server_ms=0;</script>
<link type="text/css" rel="stylesheet" href="/static/css/banner-styles.css"/>

<title>
	Error
</title><meta name="robots" content="noindex" /></head>
<body>




  
    
<script> if (window.archive_analytics) { window.archive_analytics.values['server_name']="wwwb-app54.us.archive.org";}; </script>

<!-- Start of Record Banner output -->
<wb_div id="__wb_record_overlay_div" class="__wb_overlay">
</wb_div>

<wb_div class="__wb_record_content" id="__wb_record_content_loader">
<wb_h1 id="_wb_load_msg">Saving page now...</wb_h1>
<wb_p id="_wb_curr_url">https://www.indiastudychannel.com/errors/Error.aspx?Message=An+error+occurred.+Code%3a+66ac2612-0035-4d7d-9bb8-ae4b48996bc6%3cbr+%2f%3ePlease+try+to+access+the+feature+again+by+pressing+the+back+button+on+the+browser.</wb_p>
<wb_p>As it appears live May 26, 2019 3:30:01 AM UTC</wb_p>



<wb_div id="__wb_spinningSquaresG">
<wb_div id="__wb_spinningSquaresG_1" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_2" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_3" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_4" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_5" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_6" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_7" class="__wb_spinningSquaresG">
</wb_div>
<wb_div id="__wb_spinningSquaresG_8" class="__wb_spinningSquaresG">
</wb_div>
</wb_div>

<img id="_wb_logo" src="/static/images/logo_WM.png"/>
</wb_div>

<script type="text/javascript" src="/static/js/disclaim-element.js" ></script>
<script type="text/javascript">
  function doRedir()
  {   
    var redirUrl = "/web/20190526033001/https://www.indiastudychannel.com/errors/Error.aspx?Message=An+error+occurred.+Code%3a+66ac2612-0035-4d7d-9bb8-ae4b48996bc6%3cbr+%2f%3ePlease+try+to+access+the+feature+again+by+pressing+the+back+button+on+the+browser.";
    var textElem = document.getElementById("_wb_load_msg");
    if (textElem) {
      textElem.innerHTML = "Loading Saved Page...";
    }
    window.location.replace(redirUrl);  
  }
  
  function redirToCapture()
  {
    window.setTimeout(doRedir, 1000);
  }

  
  if ( window != window.top ) {
    
    var wmOverlay = document.getElementById("__wb_record_overlay");
    
    if (wmOverlay) {
      wmOverlay.style.display = "none";
    }
    
  } else {
    
    if (window.addEventListener) {
      window.addEventListener('load', redirToCapture, false);
    } else if (window.attachEvent) {
      window.attachEvent('onload', redirToCapture);
    }
    
    // Just in case, redir after 90 secs
    window.setTimeout(doRedir, 90000);
  }
</script>

<!-- End of Record Banner Output output -->

  




    <form method="post" action="./Error.aspx?Message=An+error+occurred.+Code%3a+66ac2612-0035-4d7d-9bb8-ae4b48996bc6%3cbr+%2f%3ePlease+try+to+access+the+feature+again+by+pressing+the+back+button+on+the+browser." id="form1">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTE2NTA3NjQwMDUPZBYCAgMPZBYCZg8PFgIeBFRleHQFlQFBbiBlcnJvciBvY2N1cnJlZC4gQ29kZTogNjZhYzI2MTItMDAzNS00ZDdkLTliYjgtYWU0YjQ4OTk2YmM2PGJyIC8+UGxlYXNlIHRyeSB0byBhY2Nlc3MgdGhlIGZlYXR1cmUgYWdhaW4gYnkgcHJlc3NpbmcgdGhlIGJhY2sgYnV0dG9uIG9uIHRoZSBicm93c2VyLmRkZMQ99k0yHoMsDNii1J9XcNlQ3tAp6SQ/VDteH4wzvS3h" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="899B89B6" />

    <br /><br /><br /><br /><br />
			<center>
				<table width="85%" cellpadding="5" cellspacing="1" bgcolor="red">
					<tr bgcolor="white">
						<td align="center">
							<b><font color="red">An error occurred.</font></b>
							<br>
							<br>
							<font color="red">
								<span id="lblMessage">An error occurred. Code: 66ac2612-0035-4d7d-9bb8-ae4b48996bc6<br />Please try to access the feature again by pressing the back button on the browser.</span>
							</font>
							<br>
							<br>
							<b><font color="darkblue">Error information has been sent to the IndiaStudyChannel.com team.
									<br>
									<br>
								 </font>
							</b>
							<br>
							If you have any comments, suggestions or questions, feel free to write to <a href='/save/http://www.indiastudychannel.com/general/Contactus.aspx'>Contact Us</a>.
							We like to hear from you.
						</td>
					</tr>
				</table>
				<br>
				<h1><a href='/save/http://www.indiastudychannel.com/'>Return to IndiaStudyChannel.com Homepage</a></h1>
			</center>
			
    </form>
</body>
</html>





<!--
     FILE ARCHIVED ON 3:30:01 May 26, 2019 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 3:30:01 May 26, 2019.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
-->
