
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head id="ctl00_ctl00_Head1"><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-PP827D4');</script><script type="text/javascript" src="../js/commonFunction.js?v=170111"></script><script>function ResolveUrl(url) { var baseUrl = '/'; if (url.indexOf('~/') === 0) { url = baseUrl + url.substring(2); } return url; }
function GetSiteToken() { return 'aJLSKcPs4RUgVnIqPrjBsORv-BiLQBaWFi82Mqc5hVL-yZQkSBTC1T1nQxwnvC1pUTKGutqWrEs-zuKa6O1eXh_5txI1:3MGhOSo0LyqDxYgg_cDCVPmLKYdxbJt3lYtZutjt6DDlPRDmN3U3lCKv6m8SREAOJ8QS4V6XPHLCbq5OYolIKXn_vyA1';}</script><script type="text/javascript" src="../js/BrowserWindowSize.js"></script><script type="text/javascript" src="../js/master.min.js?v=180523"></script><script type="text/javascript" src="../Messenger7/js/sm.js?v=170426"></script><link rel="stylesheet" type="text/css" href="/css/Elements.css?v=180531" /><title>
	Online dating privacy policy on RedHotPie
</title><meta name="description" content="Privacy policy on RedHotPie. Read the privacy policy of RedHotPie online dating community." />
<meta name="keywords" content="privacy, policy, online, dating, RedHotPie" />
<link rel="alternate" href="http://redhotpie.com" hreflang="en-us" /><link rel="alternate" href="http://redhotpie.com" hreflang="en-ca" /><link rel="alternate" href="http://redhotpie.com" hreflang="en-fr" /><link rel="alternate" href="http://redhotpie.com" hreflang="en-es" /><link rel="alternate" href="http://redhotpie.com" hreflang="en-it" /><link rel="alternate" href="http://redhotpie.com" hreflang="en-de" /><link rel="alternate" href="http://redhotpie.com" hreflang="en-nl" /><link rel="alternate" href="http://redhotpie.com" hreflang="en-be" /><link rel="alternate" href="http://redhotpie.com" hreflang="en-pt" /><link rel="alternate" href="http://redhotpie.asia" hreflang="en-jp" /><link rel="alternate" href="http://redhotpie.asia" hreflang="en-th" /><link rel="alternate" href="http://redhotpie.asia" hreflang="en-cn" /><link rel="alternate" href="http://redhotpie.asia" hreflang="en-id" /><link rel="alternate" href="http://redhotpie.asia" hreflang="en-hk" /><link rel="alternate" href="http://redhotpie.asia" hreflang="en-sg" /><link rel="alternate" href="http://redhotpie.co.uk" hreflang="en-gb" /><link rel="alternate" href="http://redhotpie.co.uk" hreflang="en-ie" /><link rel="alternate" href="http://redhotpie.com.au" hreflang="en-au" /><link rel="alternate" href="http://redhotpie.co.nz" hreflang="en-nz" />
<script type="text/javascript">
        <!--
        var bWinIdt = false;
        //-->
    </script>
<!--[if lt IE 9]>
		<script>
		    var e = ("abbr,article,aside,audio,canvas,datalist,details," +
		        "figure,footer,header,hgroup,mark,menu,meter,nav,output," +
		        "progress,section,time,video").split(',');
		    for (var i = 0; i < e.length; i++) {
		        document.createElement(e[i]);
		    }
		</script>
	    <![endif]-->
<meta property="og:site_name" content="RedHotPie" />
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/image/rhp.ico" />
<meta name="theme-color" content="#7EC34D" />
<link rel="alternate" type="application/rss+xml" href="/Feeder/rss.aspx" title="RedHotPie RSS" />
<meta name="google-site-verification" content="LklQ82w62IsAHArIJMX-IYldhuRx48dLzU3f2MiQtnQ" />
<link rel="stylesheet" type="text/css" href="/css/Generic.css?v=131105" /><script type="text/javascript" src="../js/app/app.fb.common.js"></script><link rel="stylesheet" type="text/css" href="/css/SmartHeader.css?v=170316" /><meta property="og:image" content="https://redhotpie.com.au/image/200x200-RHP-Square-Logo-fb2.jpg" /></head>
<body id="ctl00_ctl00_body">

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PP827D4"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<form name="aspnetForm" method="post" action="./PrivacyPolicy.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTExODM1NDg2NjcPZBYCZg9kFgJmD2QWAgIDD2QWAgIDD2QWAgIBD2QWCAIBDw8WAh4HVmlzaWJsZWhkZAIDD2QWAmYPDxYEHghDc3NDbGFzcwVDU0hlYWRlcldyYXBwZXIgU0hlYWRlckxvZ2dlZE91dCBTSGVhZGVyRGVza3RvcCBTSGVhZGVyTm9uUmVzcG9uc2l2ZR4EXyFTQgICZBYCAgMPZBYQAgEPDxYCHwBoZGQCDQ9kFgICAQ8PFgIeC05hdmlnYXRlVXJsBW5qYXZhc2NyaXB0OlBvcHVwV2luKCdTaWduIHVwIC8gTG9naW4nLCAnLy9yZWRob3RwaWUuY29tLmF1L1NpZ25Jbi5hc3B4P3RhYj1TaWduVXAnLCA1NjgsIDYzOCwgZmFsc2UsICd3aGl0ZScpO2RkAg8PZBYCAgEPZBYCZg8WAh4LXyFJdGVtQ291bnQCCBYQZg9kFgICAQ8WAh4FY2xhc3MFElNOYXZUYWIgU05hdkFjdGl2ZRYCZg8PFgQeB1Rvb2xUaXAFNUF1c3RyYWxpYSdzIGhvdHRlc3QgY2FzdWFsIGRhdGluZyAmIHNvY2lhbCBuZXR3b3JraW5nHwMFAn4vZBYCZg8WAh4EVGV4dAUESG9tZWQCAQ9kFgICAQ8WAh8FBQdTTmF2VGFiFgJmDw8WBB8GBRpGaW5kIGhvdCBzaW5nbGVzICYgY291cGxlcx8DBRp+L01lbWJlclNlYXJjaC8/Y2xlYXI9dHJ1ZWQWAmYPFgIfBwUGU2VhcmNoZAICD2QWAgIBDxYCHwUFB1NOYXZUYWIWAmYPDxYEHwYFPUFydGljbGVzLCBlcm90aWMgc3RvcmllcywgZGF0ZSBkb2N0b3JzLCBjb21wZXRpdGlvbnMsICYgbW9yZSEfAwUMfi9Db21tdW5pdHkvZBYCZg8WAh8HBQlDb21tdW5pdHlkAgMPZBYCAgEPFgIfBQUHU05hdlRhYhYCZg8PFgQfBgUwRGlzY3VzcyBob3QgdG9waWNzIGluIG91ciBwb3B1bGFyIG1lc3NhZ2UgYm9hcmRzHwMFD34vQWR1bHQtRm9ydW1zL2QWAmYPFgIfBwUMQWR1bHQgRm9ydW1zZAIED2QWAgIBDxYCHwUFB1NOYXZUYWIWAmYPDxYEHwYFRFZpZXcgbmF1Z2h0eSBtZW1iZXIgcGhvdG9zaG9vdHMsIHNoYXJlIHlvdXIgb3duICYgd2luIG1vbnRobHkgcHJpemVzHwMFbmphdmFzY3JpcHQ6UG9wdXBXaW4oJ1NpZ24gdXAgLyBMb2dpbicsICcvL3JlZGhvdHBpZS5jb20uYXUvU2lnbkluLmFzcHg/dGFiPVNpZ25VcCcsIDU3MCwgNjQwLCBmYWxzZSwgJ3doaXRlJyk7ZBYCZg8WAh8HBQxBbWF0ZXVyIFBpY3NkAgUPZBYCAgEPFgIfBQUHU05hdlRhYhYCZg8PFgQfBgUdUHJlLXBsYW4geW91ciBob3QgaG9saWRheSBmdW4fAwUdfi9NZW1iZXJQcm9maWxlL0RhdGVBd2F5LmFzcHhkFgJmDxYCHwcFBlRyYXZlbGQCBg9kFgICAQ8WAh8FBQdTTmF2VGFiFgJmDw8WBB8GBSZZb3VyIGxvY2FsIHBhcnRpZXMgJiBzZXh5IGV2ZW50cyBndWlkZR8DBQd+L0V2ZW50ZBYCZg8WAh8HBQZFdmVudHNkAgcPZBYCAgEPFgIfBQUHU05hdlRhYhYCZg8PFgQfBgU0SGFuZyBvdXQgd2l0aCBvdGhlciBtZW1iZXJzIGluIG91ciB0aGVtZWQgY2hhdCByb29tcx8DBQZ+L0NoYXRkFgJmDxYCHwcFC1dlYmNhbSBDaGF0ZAIRD2QWAgIBDxYCHwcFBjEwLDA4MWQCEw9kFgICAQ9kFgICAQ8WAh8HBQYxMCwwODFkAhUPZBYCZg8WAh8AZ2QCFw9kFgICAQ8PFgIfAwVuamF2YXNjcmlwdDpQb3B1cFdpbignU2lnbiB1cCAvIExvZ2luJywgJy8vcmVkaG90cGllLmNvbS5hdS9TaWduSW4uYXNweD90YWI9U2lnblVwJywgNTY4LCAzMTgsIGZhbHNlLCAnd2hpdGUnKTtkZAIbD2QWAmYPFgIfBAIIFhBmD2QWAgIBDxYCHwUFElNOYXZUYWIgU05hdkFjdGl2ZRYCZg8PFgQfBgU1QXVzdHJhbGlhJ3MgaG90dGVzdCBjYXN1YWwgZGF0aW5nICYgc29jaWFsIG5ldHdvcmtpbmcfAwUCfi9kFgJmDxYCHwcFBEhvbWVkAgEPZBYCAgEPFgIfBQUHU05hdlRhYhYCZg8PFgQfBgUaRmluZCBob3Qgc2luZ2xlcyAmIGNvdXBsZXMfAwUafi9NZW1iZXJTZWFyY2gvP2NsZWFyPXRydWVkFgJmDxYCHwcFBlNlYXJjaGQCAg9kFgICAQ8WAh8FBQdTTmF2VGFiFgJmDw8WBB8GBT1BcnRpY2xlcywgZXJvdGljIHN0b3JpZXMsIGRhdGUgZG9jdG9ycywgY29tcGV0aXRpb25zLCAmIG1vcmUhHwMFDH4vQ29tbXVuaXR5L2QWAmYPFgIfBwUJQ29tbXVuaXR5ZAIDD2QWAgIBDxYCHwUFB1NOYXZUYWIWAmYPDxYEHwYFMERpc2N1c3MgaG90IHRvcGljcyBpbiBvdXIgcG9wdWxhciBtZXNzYWdlIGJvYXJkcx8DBQ9+L0FkdWx0LUZvcnVtcy9kFgJmDxYCHwcFDEFkdWx0IEZvcnVtc2QCBA9kFgICAQ8WAh8FBQdTTmF2VGFiFgJmDw8WBB8GBURWaWV3IG5hdWdodHkgbWVtYmVyIHBob3Rvc2hvb3RzLCBzaGFyZSB5b3VyIG93biAmIHdpbiBtb250aGx5IHByaXplcx8DBW5qYXZhc2NyaXB0OlBvcHVwV2luKCdTaWduIHVwIC8gTG9naW4nLCAnLy9yZWRob3RwaWUuY29tLmF1L1NpZ25Jbi5hc3B4P3RhYj1TaWduVXAnLCA1NzAsIDY0MCwgZmFsc2UsICd3aGl0ZScpO2QWAmYPFgIfBwUMQW1hdGV1ciBQaWNzZAIFD2QWAgIBDxYCHwUFB1NOYXZUYWIWAmYPDxYEHwYFHVByZS1wbGFuIHlvdXIgaG90IGhvbGlkYXkgZnVuHwMFHX4vTWVtYmVyUHJvZmlsZS9EYXRlQXdheS5hc3B4ZBYCZg8WAh8HBQZUcmF2ZWxkAgYPZBYCAgEPFgIfBQUHU05hdlRhYhYCZg8PFgQfBgUmWW91ciBsb2NhbCBwYXJ0aWVzICYgc2V4eSBldmVudHMgZ3VpZGUfAwUHfi9FdmVudGQWAmYPFgIfBwUGRXZlbnRzZAIHD2QWAgIBDxYCHwUFB1NOYXZUYWIWAmYPDxYEHwYFNEhhbmcgb3V0IHdpdGggb3RoZXIgbWVtYmVycyBpbiBvdXIgdGhlbWVkIGNoYXQgcm9vbXMfAwUGfi9DaGF0ZBYCZg8WAh8HBQtXZWJjYW0gQ2hhdGQCCQ9kFgYCAQ9kFgZmDw8WAh8DBRt+L1N1cHBvcnQvU3VibWl0VGlja2V0LmFzcHhkZAIBDw8WBB8HBRVoZWxwQHJlZGhvdHBpZS5jb20uYXUfAwUcbWFpbHRvOmhlbHBAcmVkaG90cGllLmNvbS5hdWRkAgIPDxYEHwcFFWhlbHBAcmVkaG90cGllLmNvbS5hdR8DBRxtYWlsdG86aGVscEByZWRob3RwaWUuY29tLmF1ZGQCAw9kFgJmD2QWAmYPZBYCZg9kFgICAw8WAh8HBbQEPHN0cm9uZz48c3BhbiBzdHlsZT0iY29sb3I6ICNkNTQwMzg7Ij5SZWQ8L3NwYW4+SG90UGllPC9zdHJvbmc+IGlzIEF1c3RyYWxpYSdzIGxhcmdlc3QNCiAgICAgICAgICAgICAgICBvbmxpbmUgPGEgaHJlZj0iIyI+YWR1bHQgZGF0aW5nPC9hPiBzaXRlIGZvciBzaW5nbGVzLCBhbmQgPGEgaHJlZj0iIyI+c3dpbmdlcnM8L2E+Ow0KICAgICAgICAgICAgICAgIDxhIGhyZWY9IiMiPnNpbmdsZXMgZGF0aW5nPC9hPiBpcyBhYm91dCBtZWV0aW5nIGZyaWVuZHMsIGZpbmRpbmcgbG92ZSAmIHJlbGF0aW9uc2hpcHMuDQogICAgICAgICAgICAgICAgQWR1bHQgZGF0aW5nIGlzIGZvciB0aG9zZSBsb29raW5nIGZvciA8YSBocmVmPSIjIj5zd2luZ2VycyBwYXJ0aWVzPC9hPiBhbmQgPGEgaHJlZj0iIyI+DQogICAgICAgICAgICAgICAgICAgIHN3aW5nZXJzIGNsdWJzPC9hPiwgd2ViY2FtIGFuZCA8YSBocmVmPSIjIj5hZHVsdCBjaGF0PC9hPiwgPGEgaHJlZj0iIyI+YWR1bHQgcGVyc29uYWxzPC9hPiwNCiAgICAgICAgICAgICAgICA8YSBocmVmPSIjIj5zZXggc3RvcmllczwvYT4gYW5kIG1vcmUuZAIFD2QWBAIPD2QWAmYPFgIfAGdkAhMPDxYCHwBnZBYEAgEPDxYEHwcFFVNleGllc3QgQ3VydnkgTW9kZWxzIB8DBR1+L1RvcFRlbi9TZXhpZXN0LUN1cnZ5LU1vZGVsc2RkAgMPDxYCHwMFHX4vVG9wVGVuL1NleGllc3QtQ3VydnktTW9kZWxzZBYCZg8PFgYeCEltYWdlVXJsBSh+L2ltYWdlVXNlci9Ub3BUZW4vMjAxNC4wMV9SSFBfQ3VydnkuanBnHwYFFVNleGllc3QgQ3VydnkgTW9kZWxzIB4NQWx0ZXJuYXRlVGV4dAUcU2V4aWVzdCBDdXJ2eSBNb2RlbHMgIGJhbm5lcmRkAhEPZBYCZg8WAh8AZxYCZg9kFgICAQ9kFhACAQ9kFgJmDxYCHwQCCBYQZg9kFgICAQ9kFgJmDw8WBB8GBTVBdXN0cmFsaWEncyBob3R0ZXN0IGNhc3VhbCBkYXRpbmcgJiBzb2NpYWwgbmV0d29ya2luZx8DBQJ+L2QWAmYPFgIfBwUESG9tZWQCAQ9kFgICAQ9kFgJmDw8WBB8GBRpGaW5kIGhvdCBzaW5nbGVzICYgY291cGxlcx8DBRp+L01lbWJlclNlYXJjaC8/Y2xlYXI9dHJ1ZWQWAmYPFgIfBwUGU2VhcmNoZAICD2QWAgIBD2QWAmYPDxYEHwYFPUFydGljbGVzLCBlcm90aWMgc3RvcmllcywgZGF0ZSBkb2N0b3JzLCBjb21wZXRpdGlvbnMsICYgbW9yZSEfAwUMfi9Db21tdW5pdHkvZBYCZg8WAh8HBQlDb21tdW5pdHlkAgMPZBYCAgEPZBYCZg8PFgQfBgUwRGlzY3VzcyBob3QgdG9waWNzIGluIG91ciBwb3B1bGFyIG1lc3NhZ2UgYm9hcmRzHwMFD34vQWR1bHQtRm9ydW1zL2QWAmYPFgIfBwUMQWR1bHQgRm9ydW1zZAIED2QWAgIBD2QWAmYPDxYEHwYFRFZpZXcgbmF1Z2h0eSBtZW1iZXIgcGhvdG9zaG9vdHMsIHNoYXJlIHlvdXIgb3duICYgd2luIG1vbnRobHkgcHJpemVzHwMFbmphdmFzY3JpcHQ6UG9wdXBXaW4oJ1NpZ24gdXAgLyBMb2dpbicsICcvL3JlZGhvdHBpZS5jb20uYXUvU2lnbkluLmFzcHg/dGFiPVNpZ25VcCcsIDU3MCwgNjQwLCBmYWxzZSwgJ3doaXRlJyk7ZBYCZg8WAh8HBQxBbWF0ZXVyIFBpY3NkAgUPZBYCAgEPZBYCZg8PFgQfBgUdUHJlLXBsYW4geW91ciBob3QgaG9saWRheSBmdW4fAwUdfi9NZW1iZXJQcm9maWxlL0RhdGVBd2F5LmFzcHhkFgJmDxYCHwcFBlRyYXZlbGQCBg9kFgICAQ9kFgJmDw8WBB8GBSZZb3VyIGxvY2FsIHBhcnRpZXMgJiBzZXh5IGV2ZW50cyBndWlkZR8DBQd+L0V2ZW50ZBYCZg8WAh8HBQZFdmVudHNkAgcPZBYCAgEPZBYCZg8PFgQfBgU0SGFuZyBvdXQgd2l0aCBvdGhlciBtZW1iZXJzIGluIG91ciB0aGVtZWQgY2hhdCByb29tcx8DBQZ+L0NoYXRkFgJmDxYCHwcFC1dlYmNhbSBDaGF0ZAIDD2QWBAIFDw8WAh8AZ2RkAgkPZBYCZg8WAh8AZ2QCBQ8PFgIfAGhkZAIHDxYCHwBoZAIKDxYCHwQCBhYMZg9kFgICAQ9kFgICAQ8PFgYfBwUIc2l0ZSBtYXAfBgUIc2l0ZSBtYXAfAwUOfi9TaXRlTWFwLmFzcHhkZAIBD2QWAgIBD2QWAgIBDw8WBh8HBQx0ZXJtcyBvZiB1c2UfBgUMdGVybXMgb2YgdXNlHwMFF34vVGVybXMvVGVybXNPZlVzZS5hc3B4ZGQCAg9kFgICAQ9kFgICAQ8PFgYfBwUOcHJpdmFjeSBwb2xpY3kfBgUOcHJpdmFjeSBwb2xpY3kfAwUafi9UZXJtcy9Qcml2YWN5UG9saWN5LmFzcHhkZAIDD2QWAgIBD2QWAgIBDw8WBh8HBQtzYWZlIGRhdGluZx8GBQtzYWZlIGRhdGluZx8DBRd+L0RhdGluZ1NhZmV0eVRpcHMuYXNweGRkAgQPZBYCAgEPZBYCAgEPDxYGHwcFDHJlcG9ydCBhYnVzZR8GBQxyZXBvcnQgYWJ1c2UfAwUbfi9TdXBwb3J0L1N1Ym1pdFRpY2tldC5hc3B4FgIeA3JlbAUIbm9mb2xsb3dkAgUPZBYCAgEPZBYCAgEPDxYGHwcFC2F2b2lkIHNjYW1zHwYFC2F2b2lkIHNjYW1zHwMFT2phdmFzY3JpcHQ6UG9wdXBXaW4oJ0F2b2lkIFNjYW1zJywgJy9NZXNzYWdlL01lc3NhZ2VBdm9pZFNjYW0uYXNweCcsIDM4MCwgNTYzKTtkZAIMDw8WBB8DBRt+L1N1cHBvcnQvU3VibWl0VGlja2V0LmFzcHgfBwU3PGltZyBzcmM9Jy9JTUFHRS9pY29ucy9idXR0b25zL1N1cHBvcnQucG5nJz4gQ29udGFjdCBVc2RkAg4PFgIfBAIGFgxmD2QWAgIBD2QWAgIBDw8WCB8HBQphZmZpbGlhdGVzHwYFCmFmZmlsaWF0ZXMfAwUiaHR0cDovL2FmZmlsaWF0ZXMucmVkaG90cGllLmNvbS5hdR4GVGFyZ2V0BQZfYmxhbmtkZAIBD2QWAgIBD2QWAgIBDw8WBh8HBQljb3Jwb3JhdGUfBgUJY29ycG9yYXRlHwMFG34vQ29ycG9yYXRlSW5mb3JtYXRpb24uYXNweGRkAgIPZBYCAgEPZBYCAgEPDxYGHwcFDG1lZGlhIGNlbnRyZR8GBQxtZWRpYSBjZW50cmUfAwUSfi9NZWRpYUNlbnRlci5hc3B4ZGQCAw9kFgICAQ9kFgICAQ8PFgYfBwUMdGVzdGltb25pYWxzHwYFDHRlc3RpbW9uaWFscx8DBQ5+L1Rlc3RpbW9uaWFsL2RkAgQPZBYCAgEPZBYCAgEPDxYGHwcFCWFkdmVydGlzZR8GBQlhZHZlcnRpc2UfAwUZfi9jb250YWN0cy9BZHZlcnRpc2UuYXNweGRkAgUPZBYCAgEPZBYCAgEPDxYIHwcFDW1vYmlsZSBkYXRpbmcfBgUNbW9iaWxlIGRhdGluZx8DBRRodHRwOi8vcmhwbW9iaWxlLmNvbR8LBQZfYmxhbmtkZAIQDxBkDxYGZgIBAgICAwIEAgUWBhAFCUF1c3RyYWxpYQUBMWcQBQ5Vbml0ZWQgS2luZ2RvbQUCNzJnEAUNVW5pdGVkIFN0YXRlcwUDMjEzZxAFC05ldyBaZWFsYW5kBQEyZxAFBkNhbmFkYQUCMzRnEAUNSW50ZXJuYXRpb25hbAUCLTFnZGQYAgUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgEFQWN0bDAwJGN0bDAwJGNwaE1haW5UYWJsZSR3dWNIZWFkZXJTbWFydCR3dWNMb2dpbkJveCRjaGtSZW1lbWJlck1lBUJjdGwwMCRjdGwwMCRjcGhNYWluVGFibGUkd3VjSGVhZGVyU21hcnQkd3VjUmVhbFRpbWVJbmZvSGVhZGVyJG12SlMPD2RmZClmzzrORJCF6frd9U4bVhtyaQzl" />
</div>
<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>
<script src="/WebResource.axd?d=q3eFUn-79QZw38CXc0QXbCU0nLeg713cmHik57hKMJHqyjhZVstCbqr2IK9HM4EYoEJMoa4ww__UjiZ0iNKYIHWYaeE1&amp;t=636686942738678653" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=KpNRXaoxiUcINGANVCcW1hS7TsxMbMa4lnC61RRMmJ0vhnqgZT3OXs2OI6eg7ZSoEBhfO8b5K60HQ7g9oMI5KwOGX5Jgt2Ega3O2bVmfRBI4Plvg359RYbFUaHYz0fVCqmt-ETDP0c_5jB0S_AjO4EoaAgw1&amp;t=ffffffffab5b37cd" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=cfaJoJnQBYn_sG8t2D46mZ38hydxkyTqH9VdSHrzDLysD4uaYtA3xDzlSxtCfTAMGXj27zXYOAPJi8P5y2KjLjPRxZpt3HQVVX2cystHMQRb9pQ9aTD2Qof5Vm5fJdaGuxsXdJSDx9VBO0Xw21AERNNgRR01&amp;t=545ba255" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=f09kumCYSfbwH1Od63Y2yDbIYaTlMTG5k7fXniffyYu5JtwIX5xP8f73W2QiWpZ9550UeXxMpaKay0yhG9KxuIdR9PjhGPJhTtFO82c1jaVUCVTwCkfjMXcUefghj05WDX3K7s2SKWAPdsyRenvdUYSWkFnZ-l4Fsp3gA00xeBA4nHmF0&amp;t=545ba255" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
return true;
}
//]]>
</script>
<div>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="AAE5C345" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdABPmR580VZboa0e91BJSoHV5C896yjMMvAw5IFkZoFY1/++KsmDauZvK5W1lRJgH6zk0Amr7x56ExgjZX2jmt/6Vn7y1G494hPwCNpjwYPuMqByhPYc2J+LR+Y22XKMvQ3nqzImLiVYIdjPaYKRwkdSN/LTNiJxqAAU4KpR9jw6FSlWkxyMSagWDixWzAtCqUW+klArZ+R91L2aXWXpFltJcFiZ/AiLRG1UUU32HlIh3+npnUIqNEV5jfGUDr9rhh6ZM6MR/vdUFrfOkZEWvm/AU/2wQN+HTtM+/SIztrENNrEg7vsR8GcRKyya5wEAp1DJx7Z8wlRrwLq3Eu5CYO8yFflNMG+I0j3w9Rme5dXkLagnWNzMW/vzdaTf+D9YgnECJHxgYVueq9PPGj3CJ8GK/VO+/tnjDySXAwHYPTBKU1Jn4Smc=" />
</div>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ctl00$ScriptManager1', 'aspnetForm', [], [], [], 90, 'ctl00$ctl00');
//]]>
</script>
<div id="ctl00_ctl00_cphMainTable_wucHeaderSmart_pnlHeaderWrapper" class="SHeaderWrapper SHeaderLoggedOut SHeaderDesktop SHeaderNonResponsive">
<header id="ctl00_ctl00_cphMainTable_wucHeaderSmart_sHeader" class="SHeader">
<div class="SHeaderContainer">
<div class="SGrid">

<div class="SMainLogo">
<a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_hplMainLogo" href="../"><img id="ctl00_ctl00_cphMainTable_wucHeaderSmart_imgLogo" src="../IMAGE/Logos/RHP-LogoFlatpie.png" style="border-width:0px;" /></a>
</div>
<span class="SBurgerToggle"></span>

<span class="SMainEDI">
<img id="ctl00_ctl00_cphMainTable_wucHeaderSmart_imgHeaderRight" src="/IMAGE/Logos/logoEdi.png" style="border-width:0px;" /></span>

<nav class="SReaNav">
<ul class="SUserMenu">
<li><a href="/MemberLogin.aspx" class="LoginReveal" title="Sign in">Login</a></li>
<li>
<a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_hplSignUp2" class="ButtonHalf JoinRevealHero" href="javascript:PopupWin(&#39;Sign up / Login&#39;, &#39;//redhotpie.com.au/SignIn.aspx?tab=SignUp&#39;, 568, 638, false, &#39;white&#39;);">Sign Up</a></li>
</ul>
</nav>
</div>
</div>
<hr class="SClearboth">

<div class="SNavContainer SNavMain SBurgerContainer">
<div class="SGrid">
<nav class="SMainNav">
<ul>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl00_li" class="SNavTab SNavActive"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl00_hpl" title="Australia&#39;s hottest casual dating &amp; social networking" href="../">Home</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl01_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl01_hpl" title="Find hot singles &amp; couples" href="../MemberSearch/?clear=true">Search</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl02_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl02_hpl" title="Articles, erotic stories, date doctors, competitions, &amp; more!" href="../Community/">Community</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl03_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl03_hpl" title="Discuss hot topics in our popular message boards" href="../Adult-Forums/">Adult Forums</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl04_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl04_hpl" title="View naughty member photoshoots, share your own &amp; win monthly prizes" href="javascript:PopupWin(&#39;Sign up / Login&#39;, &#39;//redhotpie.com.au/SignIn.aspx?tab=SignUp&#39;, 570, 640, false, &#39;white&#39;);">Amateur Pics</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl05_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl05_hpl" title="Pre-plan your hot holiday fun" href="../MemberProfile/DateAway.aspx">Travel</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl06_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl06_hpl" title="Your local parties &amp; sexy events guide" href="../Event">Events</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl07_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNav_rptNav_ctl07_hpl" title="Hang out with other members in our themed chat rooms" href="../Chat">Webcam Chat</a></li>
</ul>
</nav>
</div>
</div>

<div class="SNavContainer SNavSub">
<div class="SGrid">
<nav class="SMainNav">
<a href="/MemberProfile/OnlineNow.aspx" title="Online Now" class="SNavOnlineNow"><strong>
10,081</strong> Online now</a>
<ul id="ctl00_ctl00_cphMainTable_wucHeaderSmart_ulSubMenu">
</ul>
</nav>
</div>
</div>

<div class="SLoginContainer">
<div class="SLoginContainerInner">
<a class="Close" href="javascript:void(0);"></a>
<div class="SGrid">
<div class="HeaderBox">
<h3>Member Login</h3>
</div>
<div class="FacebookBox" id="fbInLoginBox">
<a href="#" class="Facebook" onclick="PopupCenter('/Registration/JoinThroughFacebook.aspx', 'FacebookPopUp', 800, 498)">
<span>Connect with Facebook</span>
<small>
<span>None of your RHP details will
<br>
be shown on Facebook</span>
</small>
</a>
</div>
<div class="seperation"><span class="Line"></span><span class="or">or</span><span class="Line"></span></div>
<div id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucLoginBox_pnlLoginBox" class="LoginBox" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ctl00_ctl00_cphMainTable_wucHeaderSmart_wucLoginBox_lBtnLogin&#39;)">
<input name="ctl00$ctl00$cphMainTable$wucHeaderSmart$wucLoginBox$txtUsername" type="text" id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucLoginBox_txtUsername" placeholder="Username" />
<input name="ctl00$ctl00$cphMainTable$wucHeaderSmart$wucLoginBox$txtPassword" type="password" id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucLoginBox_txtPassword" placeholder="Password" />
<a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucLoginBox_lBtnLogin" class="Button" href="javascript:__doPostBack(&#39;ctl00$ctl00$cphMainTable$wucHeaderSmart$wucLoginBox$lBtnLogin&#39;,&#39;&#39;)">Login</a>
<a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucLoginBox_hplForgotPwd" class="venobox Forgot" data-type="iframe" data-overlay="rgba(0, 0, 0, 0.6)">Forgot Password?</a>
<span class="Checkbox"><input id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucLoginBox_chkRememberMe" type="checkbox" name="ctl00$ctl00$cphMainTable$wucHeaderSmart$wucLoginBox$chkRememberMe" /><label for="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucLoginBox_chkRememberMe">Remember Me</label></span>
</div>
</div>
</div>
</div>
<div class="SMobileMenuButtons">
<a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_hplSignUp" class="ButtonHalf JoinRevealMobile" href="javascript:PopupWin(&#39;Sign up / Login&#39;, &#39;//redhotpie.com.au/SignIn.aspx?tab=SignUp&#39;, 568, 318, false, &#39;white&#39;);">Sign Up</a><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_hplLoginReveal" class="ButtonHalf LoginReveal" href="../MemberLogin.aspx">Login</a>
</div>

<div class="SMobileMenu">
<div class="SMobileNav">
<div class="SMobileNavInner">
<ul>
<li class="SNavTab SNavLogin">
<a href="/MemberLogin.aspx" class="SNavHalf LoginRevealMobile">Login</a>
<a href="javascript:PopupWin('Sign up / Login', '//redhotpie.com.au/SignIn.aspx?tab=SignUp', 568, 318, false, 'white');" class="SNavHalf JoinRevealMobile" id="aLoginMobile">Sign Up</a>
</li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl00_li" class="SNavTab SNavActive"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl00_hpl" title="Australia&#39;s hottest casual dating &amp; social networking" href="../">Home</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl01_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl01_hpl" title="Find hot singles &amp; couples" href="../MemberSearch/?clear=true">Search</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl02_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl02_hpl" title="Articles, erotic stories, date doctors, competitions, &amp; more!" href="../Community/">Community</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl03_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl03_hpl" title="Discuss hot topics in our popular message boards" href="../Adult-Forums/">Adult Forums</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl04_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl04_hpl" title="View naughty member photoshoots, share your own &amp; win monthly prizes" href="javascript:PopupWin(&#39;Sign up / Login&#39;, &#39;//redhotpie.com.au/SignIn.aspx?tab=SignUp&#39;, 570, 640, false, &#39;white&#39;);">Amateur Pics</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl05_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl05_hpl" title="Pre-plan your hot holiday fun" href="../MemberProfile/DateAway.aspx">Travel</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl06_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl06_hpl" title="Your local parties &amp; sexy events guide" href="../Event">Events</a></li>
<li id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl07_li" class="SNavTab"><a id="ctl00_ctl00_cphMainTable_wucHeaderSmart_wucMainMenuNavMobile_rptNav_ctl07_hpl" title="Hang out with other members in our themed chat rooms" href="../Chat">Webcam Chat</a></li>
</ul>
<h3>Get social with us</h3>
<div class="SocialIconList">
<ul class="">
<li><a href="https://www.facebook.com/RedHotPieAu" data-original-title="facebook" class="facebook" target="_blank"></a></li>
<li><a href="https://plus.google.com/u/0/b/108692982369870483095/108692982369870483095/posts" data-original-title="Goole Plus" class="googleplus" target="_blank"></a></li>
<li><a href="https://twitter.com/redhotpie" data-original-title="twitter" class="twitter" target="_blank"></a></li>
<li><a href="https://www.youtube.com/channel/UCNlnowKVYWLVzQuXQ5GBXDw" data-original-title="youtube" class="youtube" target="_blank"></a></li>
<li><a href="https://www.tumblr.com/blog/redhotpieaustralia" data-original-title="tumblr" class="tumblr" target="_blank"></a></li>
<li><a href="https://www.pinterest.com/redhotpie/" data-original-title="pinterest" class="pinterest" target="_blank"></a></li>
<li><a href="http://instagram.com/redhotpie" data-original-title="instagram" class="instagram" target="_blank"></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="BlurBackground NavClose" style="-moz-user-select: none;"></div>
</header>
</div>
<div class="MainTable">
<div class="Content Generic">
<div class="ColumnLeft630 Terms">
<div class="Title">
<h1>Privacy Policy</h1>
</div>
<div class="Padded">
<h2 class="First">Email Address and Consent</h2>
Please ensure that your address is valid and able to accept mail. Your address will never be passed on to third parties.<br><br>
By joining RedHotPie you consent to RedHotPie emailing you. If you do not wish to receive emails, you can 'opt-out' of any or all of them by updating your preferences in ‘Email Notifications’.<br><br>
Add RedHotPie to your safe senders list (<a href="/MemberProfile/RecommendedEmailSettings.aspx" target="_blank">click here</a> for instructions)<br><br>
<h2>Our Commitment To Privacy</h2>
Your privacy is important to us. To better protect your privacy we provide this
notice explaining our online information practices and the choices you can make
about the way your information is collected and used. To make this notice easy to
find, we make it available on our homepage and at every point where personally identifiable
information may be requested.
<br />
<br />
In our online <b>ANONYMOUS</b> registration form, we ask you to provide us with
certain information such as your&nbsp;email, age and the type of relationship you
are looking for. <b>However, NONE of the personally identifiable information can be
accessed or used by the public to identify a specific individual.</b>
<br />
<br />
In the course of using our site, we may track certain information such as your IP
addresses and email addresses, phone number. Many sites automatically collect this
information. If you choose to post messages in our&nbsp;Message Boards, chat rooms,
or other message areas, we will only display such information about you as you choose
to reveal.
<br />
<br />
If you send us personal correspondence, such as emails or letters, or if other users
or third parties send us correspondence about your activities or postings on the
site, we may collect such information into a file which is only accessed by authorized
personnel. We may also collect other users' comments about you in our complaints
department.
<h2>The Way We Use Information:</h2>
We internally use personally identifiable information of our users to improve our
marketing efforts, to statistically analyze site usage, to improve our content and
product offerings as well as to customize our site's content and layout. We believe
these uses allow us to improve our site and better tailor your online experience
to meet your needs.
<br />
<br />
We may also use personally identifiable information about you to deliver information
to you that, in some cases, are targeted to your interests, such as targeted banners
and promotions.
<br />
<br />
We use personally identifiable information to resolve disputes, troubleshoot problems
and enforce our User Agreement.
<br />
<br />
We DO NOT sell or rent any personally identifiable information about you to any
third party without your permission. We do disclose information in aggregate to
advertisers and for other marketing and promotional purposes. However, in these
situations, we DO NOT disclose to these entities any information that could be used
to personally identify you. Certain information, such as your password, credit card
number, and bank account number, are not disclosed in aggregate at all.
<br />
<br />
We may use your e-mail to send you messages that you have request and to respond
to any feedback or queries about out Site or Services.
<h2>Marketing and Promotional Messages</h2>
All marketing or promotional messages sent from
redhotpie.com.au
will be only sent if you have signified your consent via the explicit opt-in process.
<h2>Opt-out / Unsubscribing</h2>
redhotpie.com.au
will not send you any messages that you have not requested. To opt-out please use
one of the following methods:
<br />
<br />
1. All messages sent to you from
RedHotPie
contain an opt-out option or a link to the unsubscribe function.
<br />
2. You can unsubscribe by logging into your account via Manage Account / Profile
screen.
<br />
3. You can at any time access the unsubscribe system to stop all messages send to
an email address (<a href="/unsubscribe.aspx" target="_blank">located here</a>).
<br />
4. You can send an email, via our help system, telling us you wish to stop all messages
(<a id="ctl00_ctl00_cphMainTable_holder1_wucPrivacy_hplSupportMessage" href="../Support/SubmitTicket.aspx" target="_blank">located here</a>
or
<a id="ctl00_ctl00_cphMainTable_holder1_wucPrivacy_lnkEmail1" href="/cdn-cgi/l/email-protection#5b333e372b1b293e3f33342f2b323e75383436753a2e" target="_blank"><span class="__cf_email__" data-cfemail="eb838e879bab998e8f83849f9b828ec5888486c58a9e">[email&#160;protected]</span></a>).
<h2>Our Commitment To Data Security</h2>
RedHotPie&nbsp;has security measures in place to protect
and prevent the loss, misuse, and alteration of the information under our control.&nbsp;RedHotPie
uses industry standard efforts to safeguard the confidentiality of your personal
identifiable information such as firewalls. While &quot;perfect security&quot; does
not exist on the Internet, our technical experts at&nbsp;RedHotPie
work hard to ensure your secure use of our services.
<h2>How You Can Access Or Correct Your Information:</h2>
You can access all your personally identifiable information that we collect online
and maintain by&nbsp;logging in to
RedHotPie
and going to &quot;Manage Account / Profile&quot; page. We use this procedure to better safeguard
your information where you are the only one who has full access and control of your
information.
<br />
<br />
RedHotPie&nbsp;Members have the following choices to modify
or delete their information from our database:<br />
<br />
1. Send an email to our customer service rep. at
<a id="ctl00_ctl00_cphMainTable_holder1_wucPrivacy_lnkEmail" href="/cdn-cgi/l/email-protection#b7dfd2dbc7f7c5d2d3dfd8c3c7ded299d4d8da99d6c2" target="_blank"><span class="__cf_email__" data-cfemail="1e767b726e5e6c7b7a76716a6e777b307d7173307f6b">[email&#160;protected]</span></a><br />
2. Log in with your password and modify/delete your profile<br />
<br />
<br />
To protect your privacy and security, we will also take reasonable steps to verify
your identity before granting access or making corrections.
<h2>Important Notice</h2>
As a responsible corporate citizen
RedHotPie
cooperates with the proper judicial processes at both the State and Federal level.&nbsp;
RedHotPie
will, upon receipt of an appropriate warrant, or other judicial order, provide information
to state and federal prosecuting authorities.<br />
</div>
</div>
<div class="ColumnRight300">
<div id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_pnlForm" class="JoinNowAd" onkeypress="javascript:return WebForm_FireDefaultButton(event, &#39;ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_btnJoinNow&#39;)">
<h2 id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_h2Tag">
Register for <strong>free</strong></h2>
<span class="Label">Username:</span>
<input name="ctl00$ctl00$cphMainTable$holder1$wucRegistrationNotLoggedIn$wucQuickRegistration2$txtUsername" type="text" maxlength="16" id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtUsername" autocomplete="off" />
<span class="Label">Password:</span>
<input name="ctl00$ctl00$cphMainTable$holder1$wucRegistrationNotLoggedIn$wucQuickRegistration2$txtPassword" type="password" maxlength="20" id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtPassword" autocomplete="off" />
<span class="Label">Confirm Password:</span>
<input name="ctl00$ctl00$cphMainTable$holder1$wucRegistrationNotLoggedIn$wucQuickRegistration2$txtPassword2" type="password" maxlength="20" id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtPassword2" />
<span class="Label">Email:</span>
<input name="ctl00$ctl00$cphMainTable$holder1$wucRegistrationNotLoggedIn$wucQuickRegistration2$txtEmail" type="text" maxlength="250" id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtEmail" />
<a onclick="return lBtnJoinRegistration_Click();" id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_btnJoinNow" class="Register" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctl00$cphMainTable$holder1$wucRegistrationNotLoggedIn$wucQuickRegistration2$btnJoinNow&quot;, &quot;&quot;, true, &quot;Registration&quot;, &quot;&quot;, false, true))"></a>
<span id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername" style="color:Red;display:none;"></span>
<span id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle" style="color:Red;display:none;"></span>
<span id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword" style="color:Red;display:none;"></span>
<span id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword" style="color:Red;display:none;"></span>
<span id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail" style="color:Red;display:none;"></span>
<span id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail" style="color:Red;display:none;"></span>
<span id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword" style="color:Red;display:none;"></span>
<input type="hidden" name="ctl00$ctl00$cphMainTable$holder1$wucRegistrationNotLoggedIn$wucQuickRegistration2$hdnAttempt" id="ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_hdnAttempt" value="0" />
</div>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript">
        <!--
            var targetWin;
            
            
            function lBtnJoinRegistration_Click() {
                var retVal;
                retVal = false;
                if (!DisplayAlertMsg('Please correct this', 'Registration')) {
                
                } 
                else if($get('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_hdnAttempt').value=="0"){
                    PopupWin('Terms Of Use', 'TermsOfUseConfirm.aspx', 407, 563); 
                }
                else {
                    retVal = true;
                }
                Page_BlockSubmit = retVal;
                return retVal;
            }

            function Fire_lBtnJoinRegistration() 
            {
                if (Page_ClientValidate('Registration')) {
                     __doPostBack('ctl00$ctl00$cphMainTable$holder1$wucRegistrationNotLoggedIn$wucQuickRegistration2$btnJoinNow','');
                }
            }
            
            function PopupCenter(pageURL, title,w,h) {
		        var left = (screen.width/2)-(w/2);
		        var top = (screen.height/2)-(h/2);
		        targetWin = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
	        }
	        
	        function DoVerification(enc){
	            targetWin.close();
	            if(top.location == self.location){
	                 PopupWin('Confirm Details', '/MemberProfile/VerifyDetail.aspx?enc='+encodeURIComponent(enc), 350, 700, false, 'white');
	            }
	            else {
	               window.location.href = '/MemberProfile/VerifyDetail.aspx?enc='+encodeURIComponent(enc); 
	            }
	        }
	        
	        function DoLogin(enc){
	            targetWin.close();
	            if(top.location == self.location){
	                 window.location.href = '/Login/Process.aspx?enc='+encodeURIComponent(enc); 
	            }
	            else {
	               window.parent.top.location.href = '/Login/Process.aspx?enc='+encodeURIComponent(enc); 
	            }
	        }
	        
	        
        -->
    </script>
<div class="Banner">
<div class="Banner TopTenAd">
<div class="TitleText"><a id="ctl00_ctl00_cphMainTable_holder1_wucAdBanner300x250_wucTopTen300x250_hplLatest" href="../TopTen/Sexiest-Curvy-Models">Sexiest Curvy Models </a></div>
<a id="ctl00_ctl00_cphMainTable_holder1_wucAdBanner300x250_wucTopTen300x250_hplImage" href="../TopTen/Sexiest-Curvy-Models"><img id="ctl00_ctl00_cphMainTable_holder1_wucAdBanner300x250_wucTopTen300x250_imgLatest" title="Sexiest Curvy Models " src="../imageUser/TopTen/2014.01_RHP_Curvy.jpg" alt="Sexiest Curvy Models  banner" style="border-width:0px;" /></a>
<div class="FooterText"><a href="/TopTen/" id="ctl00_ctl00_cphMainTable_holder1_wucAdBanner300x250_wucTopTen300x250_a1">Archive</a></div>
</div>
</div>
</div>
</div>
</div>
<div id="ctl00_ctl00_cphMainTable_wucFooterNew_pnlFooter" class="FooterResponsive Width946">
<div class="container FooterInner">
<div class="FooterFullNav">
<ul>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl00_li"><a id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl00_hpl" title="Australia&#39;s hottest casual dating &amp; social networking" href="../">Home</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl01_li"><a id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl01_hpl" title="Find hot singles &amp; couples" href="../MemberSearch/?clear=true">Search</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl02_li"><a id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl02_hpl" title="Articles, erotic stories, date doctors, competitions, &amp; more!" href="../Community/">Community</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl03_li"><a id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl03_hpl" title="Discuss hot topics in our popular message boards" href="../Adult-Forums/">Adult Forums</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl04_li"><a id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl04_hpl" title="View naughty member photoshoots, share your own &amp; win monthly prizes" href="javascript:PopupWin(&#39;Sign up / Login&#39;, &#39;//redhotpie.com.au/SignIn.aspx?tab=SignUp&#39;, 570, 640, false, &#39;white&#39;);">Amateur Pics</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl05_li"><a id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl05_hpl" title="Pre-plan your hot holiday fun" href="../MemberProfile/DateAway.aspx">Travel</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl06_li"><a id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl06_hpl" title="Your local parties &amp; sexy events guide" href="../Event">Events</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl07_li"><a id="ctl00_ctl00_cphMainTable_wucFooterNew_wucMainMenuNav_rptNav_ctl07_hpl" title="Hang out with other members in our themed chat rooms" href="../Chat">Webcam Chat</a></li>
</ul>
</div>
<div class="Banner">
</div>
<div class="col-6">
<div class="FooterOptions">
<div class="SocialNav">
<h3>Get social with us</h3>
<div class="SocialIconList">
<ul class="">
<li><a href="https://www.facebook.com/RedHotPieAu" data-original-title="facebook" class="facebook" target="_blank"></a></li>
<li><a href="https://plus.google.com/u/0/b/108692982369870483095/108692982369870483095/posts" data-original-title="Goole Plus" class="googleplus" target="_blank"></a></li>
<li><a href="https://twitter.com/redhotpie" data-original-title="twitter" class="twitter" target="_blank"></a></li>
<li><a href="https://www.youtube.com/channel/UCNlnowKVYWLVzQuXQ5GBXDw" data-original-title="youtube" class="youtube" target="_blank"></a></li>
<li><a href="https://www.tumblr.com/blog/redhotpieaustralia" data-original-title="tumblr" class="tumblr" target="_blank"></a></li>
<li><a href="https://www.pinterest.com/redhotpie/" data-original-title="pinterest" class="pinterest" target="_blank"></a></li>
<li><a href="http://instagram.com/redhotpie" data-original-title="instagram" class="instagram" target="_blank"></a></li>
</ul>
</div>
<h3>Get our
<img src="/image/logos/RHP-Mobile-Small.png">
app</h3>
<div class="SocialIconList">
<ul class="">
<li><a href="https://play.google.com/store/apps/details?id=com.messmo.redhotpie.android" data-original-title="android" class="android" target="blank"></a></li>
<li><a href="https://itunes.apple.com/app/rhpmobile/id405146852?mt=8" target="_blank" data-original-title="apple" class="apple"></a></li>
</ul>
</div>
</div>
</div>
</div>
<div class="col-6">
<div class="FooterNavBlock">
<ul class="FooterNavBlockInner">
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl00_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl00_hpl" title="site map" href="../SiteMap.aspx">site map</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl01_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl01_hpl" title="terms of use" href="TermsOfUse.aspx">terms of use</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl02_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl02_hpl" title="privacy policy" href="PrivacyPolicy.aspx">privacy policy</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl03_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl03_hpl" title="safe dating" href="../DatingSafetyTips.aspx">safe dating</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl04_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl04_hpl" title="report abuse" rel="nofollow" href="../Support/SubmitTicket.aspx">report abuse</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl05_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav1_ctl05_hpl" title="avoid scams" href="javascript:PopupWin(&#39;Avoid Scams&#39;, &#39;/Message/MessageAvoidScam.aspx&#39;, 380, 563);">avoid scams</a></li>

<li>
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_hplHelp" class="ButtonSupport" href="../Support/SubmitTicket.aspx"><img src='/IMAGE/icons/buttons/Support.png'> Contact Us</a>
<a href="#" class=""></a>
</li>
</ul>
<ul class="FooterNavBlockInner">
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl00_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl00_hpl" title="affiliates" href="http://affiliates.redhotpie.com.au" target="_blank">affiliates</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl01_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl01_hpl" title="corporate" href="../CorporateInformation.aspx">corporate</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl02_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl02_hpl" title="media centre" href="../MediaCenter.aspx">media centre</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl03_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl03_hpl" title="testimonials" href="../Testimonial/">testimonials</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl04_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl04_hpl" title="advertise" href="../contacts/Advertise.aspx">advertise</a></li>
<li id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl05_li">
<a id="ctl00_ctl00_cphMainTable_wucFooterNew_rptFooterLinkNav2_ctl05_hpl" title="mobile dating" href="http://rhpmobile.com" target="_blank">mobile dating</a></li>
<li>
<select name="ctl00$ctl00$cphMainTable$wucFooterNew$ddlCountry" id="ctl00_ctl00_cphMainTable_wucFooterNew_ddlCountry" class="CreateCountrySelect">
<option selected="selected" value="1" data-url="//redhotpie.com.au/?domCtry=au">Australia</option>
<option value="72" data-url="//redhotpie.co.uk/?domCtry=gb">United Kingdom</option>
<option value="213" data-url="//redhotpie.com/?domCtry=us">United States</option>
<option value="2" data-url="//redhotpie.co.nz/?domCtry=nz">New Zealand</option>
<option value="34" data-url="//redhotpie.com/?domCtry=ca">Canada</option>
<option value="-1" data-url="//redhotpie.com/?domCtry=intl">International</option>
</select>
</li>
</ul>
</div>
</div>
<div class="FooterTM">
&copy; 2001 - 2019 Digital Quarter Pty Ltd - All Rights Reserved<br />
The word RedHotPie and the RedHotPie logo style are trademarks of Digital Quarter Pty Ltd. <a href="/Feeder/rss.aspx" id="ctl00_ctl00_cphMainTable_wucFooterNew_wucRhpCopyright_aRss">RSS</a>
</div>
</div>
</div>
<script type="text/javascript">
            <!--
    window.addEvent("domready", function () {
        var messageArray = new Array();
        messageArray[0] = 'redhotpie.com.au';
                messageArray[1] = 'redhotpie.com';
                messageArray[2] = '//redhotpie.com/?domCtry=us';
                messageArray[3] = '213';
                createAnnouncementCountry(messageArray, 'D6-BHbfthPKYbwrCY7v3F7EcWrbXlDydPYwCAsTIpgIBjYFbbetPxU8VZ_I6zt4mfMO2M8yq22WstDXonwHA8_8vO941:1s2tEVU1RJRbnSUi9HDpNrzor28nsW2JJce8xbmD9UH_u0JNIR9TqHam14HUlDFOVR1q2LeJReTwe8rtjPFPO7DdRyg1');
            });

            //-->
        </script>
<input name="__RequestVerificationToken" type="hidden" value="a9ULjgH3vEGf2_VHDjEI6koI7xASLJlqpf7dKxL5nvd_vWNXrk5j04QDBoenfXPhXAQrwNjEY7gUx0tfNMNFqwqAmFQ1" />
<input type="hidden" name="ctl00$ctl00$hdnScreenWidth" id="ctl00_ctl00_hdnScreenWidth" value="-1" /><input type="hidden" name="ctl00$ctl00$hdnScreenHeight" id="ctl00_ctl00_hdnScreenHeight" value="-1" />
<script type="text/javascript">
                var resizeTimeout = false;
                window.addEvent('load', function () {
                    var currentWidth = parseInt($('ctl00_ctl00_hdnScreenWidth').get('value'), 10);
                    var currenHeight = parseInt($('ctl00_ctl00_hdnScreenHeight').get('value'), 10);
                    SetApiScreenSizeWidthHeight(currentWidth, currenHeight, '5Edne4UzyOquXHtujR_pUgpbXrZ9rAQubqO2yrZKkoScjaxb1nWsc1PJTX0areoLkvyi3hVOfpoXho_rh50-UwjkE581:hyBbQqZtGyNEbIu118nkckrBBk4UmCkf2cK9kaZXQWTQL4IA27pqXLSGLBDoVSQRuEUuxnKtqmjWpuWN-8HDGiBEnbg1');

                    window.addEvent('resize', function () {
                        if (resizeTimeout) resizeTimeout = window.clearTimeout(resizeTimeout);

                        resizeTimeout = setTimeout(function () { // Prevent multiple firing
                            var rcurrentWidth = parseInt($('ctl00_ctl00_hdnScreenWidth').get('value'), 10);
                            var rcurrenHeight = parseInt($('ctl00_ctl00_hdnScreenHeight').get('value'), 10);
                            SetApiScreenSizeWidthHeight(rcurrentWidth, rcurrenHeight, 's-1gb14E-G0gYujxEL8ARTMHH_Qg3bs0PsC6xB3WgenBFZte3t00ZrJ0QwAzhpgjojd6h9BvmybcCxuhB4tLZPPkqss1:Z8Rm1G9e5n41CWmPgWMF0bhQoMAX8V8FDM-TsmLh8z3TWKsWeHSq4VX9oNiojvHYJOuwIaE0MTsxr0zd-9n3pW9GH-Q1');
                        }, 1000);
                    });

                });
            </script>
<script type="text/javascript">
//<![CDATA[
var Page_Validators =  new Array(document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername"), document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle"), document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword"), document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword"), document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail"), document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail"), document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword"));
//]]>
</script>
<script type="text/javascript">
//<![CDATA[
var ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername = document.all ? document.all["ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername"] : document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername");
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername.controltovalidate = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtUsername";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername.errormessage = "You must enter a username.";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername.display = "None";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername.validationGroup = "Registration";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername.initialvalue = "";
var ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle = document.all ? document.all["ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle"] : document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle");
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle.controltovalidate = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtUsername";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle.errormessage = "Username must be between 5 and 16 characters in length, and must only contain letters, numbers and underscore. Underscores may not be used at the beginning or end of a username.";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle.display = "None";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle.validationGroup = "Registration";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle.validationexpression = "^[0-9a-zA-Z][0-9a-zA-Z_]{3,14}[0-9a-zA-Z]$";
var ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword = document.all ? document.all["ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword"] : document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword");
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword.controltovalidate = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtPassword";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword.errormessage = "You must enter a password.";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword.display = "None";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword.validationGroup = "Registration";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword.initialvalue = "";
var ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword = document.all ? document.all["ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword"] : document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword");
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword.controltovalidate = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtPassword";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword.focusOnError = "t";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword.errormessage = "Your password must be at least 6 letters long and only contain letters, numbers and underscore.";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword.display = "None";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword.validationGroup = "Registration";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword.validationexpression = "^([a-zA-Z0-9_]{6,20})$";
var ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail = document.all ? document.all["ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail"] : document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail");
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail.controltovalidate = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtEmail";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail.errormessage = "You must enter an email address.";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail.display = "None";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail.validationGroup = "Registration";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail.initialvalue = "";
var ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail = document.all ? document.all["ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail"] : document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail");
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail.controltovalidate = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtEmail";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail.errormessage = "Please input a valid email address";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail.display = "None";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail.validationGroup = "Registration";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail.validationexpression = "^([0-9a-zA-Z]([-.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";
var ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword = document.all ? document.all["ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword"] : document.getElementById("ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword");
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword.controltovalidate = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtPassword2";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword.errormessage = "Your passwords do not match.";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword.display = "None";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword.validationGroup = "Registration";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword.evaluationfunction = "CompareValidatorEvaluateIsValid";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword.controltocompare = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtPassword";
ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword.controlhookup = "ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_txtPassword";
//]]>
</script>
<script type="text/javascript">
//<![CDATA[

var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        
document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvUsername'));
}

document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionHandle'));
}

document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvPassword'));
}

document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionPassword'));
}

document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_rfvEmail'));
}

document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_RegularExpressionEmail'));
}

document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('ctl00_ctl00_cphMainTable_holder1_wucRegistrationNotLoggedIn_wucQuickRegistration2_cvdPassword'));
}
//]]>
</script>
</form>
</body>
</html>
