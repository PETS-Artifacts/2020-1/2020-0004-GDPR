<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="sr-Cyrl-CS" lang="sr-Cyrl-CS" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2017, by DNN Corporation -->
<!--*********************************************-->
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	Агенција за привредне регистре
</title><meta id="MetaDescription" name="DESCRIPTION" content="Насловна страница" /><meta id="MetaKeywords" name="KEYWORDS" content="АПР, Агенција за привредне регистре, Предузетници, Привредна друштва, Лизинг, Заложно право, Залога, Јавна гласила, Удружења, Страна удружења, Туризам, Финансијски извештаји, Бонитет, Регистрација, Регистар,DotNetNuke,DNN" /><meta id="MetaGenerator" name="GENERATOR" content="DotNetNuke " /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Resources/Search/SearchSkinObjectPreview.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Portals/0/Skins/Flex2-V-Round-Stripe/skin.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Portals/0/Containers/Flex2-Set8/container.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Portals/0/portal.css?cdv=581" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=581" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=581" type="text/javascript"></script><link rel='SHORTCUT ICON' href='/Portals/0/favico/favicon.ico?ver=2011-09-19-155652-890' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-6906308-2']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  </head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="1AIT+7dqW1+6GdSAYJZvmpPfR5RzdmmizWDjUkLQbWsrFvFu4rtR1HOQ8KQ+DOd1vATt6SDOHU5U/UkkWBiMT9kZmpyqoGwe5ltgoSPLjNlgOFP0QT0eTpW5y/SYW3pMpH6rAyS/ruW18MlpLIsNgljYAvD9sI6xlUwF+zgq8EqbSPVrP+r+cX6pSGhWK599GF7SWiivW32z/ObQppDlI4hiQRO6mu78+4P4giYF6cmJ9zQx+/l8jzPLHX7PMwZbBRHrQFKzxdPWkzRSbGWYkfjFworX19BvnYJvI8QlViku5Rh99ERa4lHl6gLCD/KJs0hrJ//idJ0MrDg5bUb0su8qUKfhR8efjYtVIX4ktbWOwSqbTaGuUPMjN6OVSBkpMvRaFqTWDqbsUOEv1Bnh/lsCJhUkKx/MIU8YiVPFXG6Bf+yBOr4rfez7yLZG2PG9/wsvejsSeAS+L3QFqvlYgpcVVw6A5nwyNo+ER572n8IRiCbaBBeaZzGU4QgjEAZNqsrYWS2yMjq12H5HprbBWzW3UkJ/2t0v6AxoEYFwB3T0vbqbcN204JWEc7IL0bCOkPa5BlTUlPc7PtPBYJ31cVgfMaTAT0hwlYDW6z5yT5g9d9/g1qRtVpzrPMsTEjGqjm3ka/d7Tet9JHs8ef8Jmtklv5Lpj1j3nT7OYckOj+JEuieGtj17B4nu/GjqlzSPm8B69g7QVvwg6kPWXpBKBvTjDuE+ZHXivTkromp0k4ImoQhSK3veD9eFKgd3jLY3aPtc8lS8NUJmWk6jwitX+63scHx6dazjwPWXXh12VoboLclNIA8SDlCZ0oXMviq+fU/tZKquk5WB1JhNhgXL7Xvv6LGYQW+AJ5XWVDSYskiX5MXkY1Hzg87zTEGvtgP2CYn4z1Fr63pgI3hcmzCeGgKE8A6sp9b4A6sjHs2UqtV4derOKpdEXOvHonKxdFEnM2DengvmVK4jGFRrdVze6+6SCVS7CTV+4IixEI6hldn6chcJllzkJvef7y0wGZA8aRv0SWaXN2FsAb0ohCp2g/W1GIApanJGBfs2SfOBQxKrMSBjHQlC4duiuLBMlC1tM/MXx41gNUe97KvTouNCK4nPJmGztIHvsC+RPKR9iXHoYWeFNWCKZ+hhHQCfOTnd9IJqF7Bm1PQJkapbW5L1TAyg7AbkYaYC+GC1yKzoSmfbDhuhc3ueejXh6TtCIC8/3woIyE7YLlx/Xg5ngfk0n6swOXJcOpFHsfO7pPIzxFqBJq/nVGPqUcygdscq88krXOw2rcAbCDPx/uap1zEMh0AM2Ga32VUuojD7pSBdkCS8FjeRgTAwvXyfSQPDCdqvgdfYV5dFccCfxvnonc/uDUpmFqHpyui5km1Hqa5tmMHyUMVhPgxmVgMtficCgDq0ZbWOXTCq4qqT8c/ZFxrldsqQ+07Oq2W94NRa8ZrPIxxhJgwFheDhQ9qobD3MbE/qmz04vaVf6fFWubeNnKQNFXOT00oXUfWhHIw1nltqRZ99GGCuThaPuejOpWeG0nebD2xePeCNsQg/Z0h51iKB+MF6m/6DRwWa+c2atAXWox4s0zpzhw5aJvqdB8xnsWPtosQX55+SHsQZAV6Iw/O184UD+732Dku/A8wyk5+F6HXdN3/8duRAMBpFFzh7fd9VS8YY0ay1Ge7yivEmlY6eRxw0Mwn0CaWK98lLkVyuTK2nMxl+TC56rkJwNikYjL/PsLwXFS83I5tK1xpZV0VbA7iRjneMpQtD+06EpnP+8ypkbGrK+nnzh8jp0QL7b6xwuUKqpFnAppOKwW5uj9eL6BeqcXunYxSOoEiB3N48g4lmHguK/u6PvhQNnexszpQov5TUH8sDF5z8lffSPPHioyXog7R8E9UNLrvzhh3jdUmgvq4Faf9XfpZ0ndSrK/DPvdVzhmYT8m0C1n+YczufS8MvDnkpYub+UosdixaYSsdFAmNWuuP97KDZIWTZN4/NuFmciYWW/Cce4EMHzHVPio9GYxROSQuQaV1qR4oSNS8NDvtPPH+28ianBzv8l6OCr9bWqV/Hyld2YDJ/kmDBSsbU8wcIauTOAsoAtAww2AN3loFhYhX5L2ARSA04AyQSVDsCj9alb3efo2R5csPw5p3buGiO2t/o8F6Lt4MN8yhVryM7UcbzUwWYGEF5/lzXtN2BvwX9JaapFIk2UEvSIiBLHiec9MD2F6bKkNywVn3IMYQvrr23fWTQpqvu86JskcXvpSGz5z2qrOa76iORwUcjWNVKV8irlJt37u2/MhYvCrx3jFIwo9QKibA0uGYlSsJgroCshgB59nE+D0TXzMloCYPeeCHqbwktdM77yWDdIgXv+f3QwzV2gzuebMDjGnrHifOVMBzJ6cqFsykXAjfsRsVHl0xIbgtO9HChFINg8bKzmMLkSO3uplzh1zcLzHVvmPCt6yDaTqOqifkwiRTzE+gturMGnbTeiyuMZoBnSQF8taiqFKxYcgtMQurqOiJaoM+05KxEMLaSyDklElgsn+TIJHx06ONURf4EimCa3UjbMamOoJi6HidqdjeQlMTvFni6+6EFQYre2zzq/Kvf+0q4/+mROp6cH6Np2i1rkxe/BihJOt7gxyIJM0YqqILXzo2aEH6WwjZwIfA++nw8ZdGZLD8KAAitTDUF2zSoW0BL8uCl/EUG8NjfCXIjxs4tmjUyMaS0j/hufzMUkhQu2JDwzeIWZfVlDgI872o9jcLl/76wmywPn/uJKLCrS5PSOwwWwq66yw5iome4caLDTuVwZuZtfKYOudz/kn42HU3Li8+IEhw87CGPzLqZg2cWN2a0oBnBDl20PDaoalEKHNxkiLihiVr1pM18LsqadbctRq/YOzxl4otafK0xx4jyQZQNXIYDoBTaVDrQovQfCPTXP3gzOcnQXH/Av2QSq5l8klniYTAV+u5nPRFvAO05d8qV6N5K07rFZ1NqHA55Q1zu9TFrtD0zGcqXmvSsVcTY7fr4Yu33OhbMuPxN5DuWZoC6vDrtugDVhhiG8eyD1I539Xjeq5INGwYNoR+Antv7MsMwFIyNgKX4jZQeDcKo/4IU+vQc2q/RUN48yWYgivcUYULGoACPw7vgnV5sVk9Ha10VSFW79ddzP9Wh7jpDeaqdJs9HKTb6TPcJEffPXzJ/52Q0TQxtFvvJruWhn5g93SPCBH3R1UJfszYuoqXCoZI/0glux9Sjez92+x2lHwepllPPaFAdKMvamKSPBQawIg41vVkAJ6cmxGMVS3NDAQnM1i61Cu4eV9qF36Pa+KhOTuXJtl4Jk6r9O843jwEIyKUrsbf//BOxOPwHM+Zb5MWeabDw66YYWtbT5liJE4BgYtd66HpM4NeyqNRfqfhQ4DImZEv/0XfjXfEzO6j1Xpfg2Fdi5XFdbGLv9pq75u2AtMz2aVAP/xpCuuRHGvyjP7tf8EfcvYvrZrY69P3LK6Yrn3dm+J62vXbxJkKXoGtu6iuEFyrVn71fjlFjR9iIQug1AnrawrfaWZuabQm00PEp03W5XRF4JFP5nXymlvqvockOi3M0vZ7dJE98RX+Yrfgvp6Mikqu8LRlEze7rs8jQjibKaSZ8eo7dxH+Fe6JAGKVGQZAHjvyLll/kqYLSHMtV5LOZmrqyDnRrGACFsXIxIEtuRd5P8h9jemcVHtlqtn/r0yx7OilEwU+rtarzNtiu6u5LUxV/lZ3SBbf2v1XQfk2QBRBU3/I3hQ3mENKJD3ymxmzVKhEu/kaPnD51iIfvbs+5G4cOE0pmA/UXZ8kH+ts5Zr/uA6VsfxEDOsdwHZl/ZRCm2wJfuS+cTRex1whSQ1rozmMNCijBcAkp+Lp0a29kbSuxOdssWFJAKd0mBgH5AHdi0Qe4Ng5zFGj0kCc+uWRnu313cmWFjNAyTVc4ThujUJgauxS9GhGh/uMjJ/XZgmbOAYG8NdZwmYucpIYxZTbiHRGgWNabaV9XTh9UCrOV0n8PBGMXBVfjDCwvOhbPa2En+F2atlVVP50UDqsHjtB2alTI/3A4KQAdjfR/+2or0TSS4vSCoawVqOK2zX5R263St0QmM3qNNfK1AoN3GkYOBrYlkLjPgfS7BLPAskorQTOG+UH3hta0RxjcIObNFsbY2oreXgRUcPxU4KhFIzf6/Wo1ux8oFilMt37tEiXEZLzK4ztIMd1I0WzQ1vq7AxmMX1aAbjLEIWolvPbuPPRAdpzAFBokg1KqOd6krHoOsJBHsbDFBeQJ/dghG2Y7SIEiTkHvdFdEuLsCoLXHHnZwOZr5wGdIY+iVIaeWiSEa8p8N55DpWSmliLLdpEmCgJlQr5PwC2YCntYb8fYbhVRACjCPU7uOyxvmtKrvQhJmwGyaJMttfWeqPe51Dvdum/D95mkiJBFXTEstl4nHdk3GVPp7FVdnSEEp5lqrL4lWF6SPuSpmW9zv1XcuC6QXDyDHHjJvYxB4O7MBmSv2Erdbw/hykInsOYwYkzzf+Rm/S4bx6oUytxA6b0sRi96j3DE7XEFuvbV6XnSSEhNMLj18/CcPiDNDKTL0JHAZlUP0PzlXKtOkikxl2TyHMY1L0K2hOc1BxZDM2LX92gmkPdkjxXVTg+VQW3CWL4jA9qpDzyPA9Opj6vs+oxQj/YYm/MHN/iwDsJKczvuz+hVho/bYf+j4jE2wEuIt4DxszrGT7pQoRdB0rsrukrjKNuVSVeACmd/LMeYehM/OXlJ/SRFXSYPMe3SJ7xMwZVeC9b1kA4bTlkFWErzsWkyeZFZe1zliKXYL85P0TBEZj1RyVPdkTcCTMi+cPh0Q/IBHw2pU0u/gOOVFjOoYZ51p5nWs0PymW/L7jbxSEifflcZv2suaFm4ShKR3AyeB5CmfPCAtr0VKXNdqcTHgnUJ64cENdKmv/7QrKyyMGz23D+Tp/kwfZoEos9p8NKk6jzTryxwc/bhA0XMVJFx6edOL2BAm5zbAVMdMCJRSO3V4DWM8M3NfwdJTHPWLuPBQ7Tt0/XBSVd91U7C2SKjXYQKW7vkqK8XASFo1porr+j3xKnEcbEiZNsnxmNFrcHqfgWadIAT4UrvazJk3XK4ScS9XfRPajUAEFN1exyN8W0h/BLDURnZYfALYnvrkP2kp9sh0zLWvwjKfDQhE7d8Ee9YcsZKXtvOAuGlAuGj5wt/zwCPBKlrsn++HKBe8obaHRSHgl3sKFBiYAz5DEdVOtwjzStblTdugzd4ErChbFHcl6J68ZTSVbNCOv1VHyhsZgQ1vNkhltT+7pYPGTkwwrecsJ2CGgCDXehfcz0MUUiFswzTR27W0ioRH45BNCzBgMLco14k39hIjmMADUGf+iumUB3OZ3prrGxcbDxWfDUss+h3Nq/jcWvEmLZUuKLAUACiQV/ZADOeJCtWdJbSuujMcuvOlPOY12Xlr0QGbb7f7fXGA5b5wYX8C1gzfasTXO/xXlJkAqf8qKy/DQqMRgrHKLGVZcWsnmyOr3lasfxeTCvg6jMUJ89NsnMGIvla0nedY3PB4Lx3WzNAARg36rRYEV20Q9ndDyeJMGWCXFo9YW/J4MAYMNG+YQ+Wzk2+0x5yVDbiMexPtHpS2eRz6MF5cXozKgqHvm9UIDsFkBNQvqRpQge/MPwNvMmI7nqk6kPo4ei1J1y7xstv2d1jY1PeqfjceiuSIu3osEhDkUSfq0XpmUt4Auu3tN1vIOBGVW4g9fKrsRMbBwfKXgjniA2zgC+jHYhBZDCwDjRR2xnw0I2punzMC/HyRF52m6d9mdHbJl3emfUVXBvugsRucQ7D8GAMI8axn7+Za3jWXfznKB4M6wL5ytCssa5Pna7fwPUAjqsWgCQETpKGDLDvFKSTSs9qNSox9tyBx5v+3NU3aSWKDU80hEgC/CiHBjXxwNKWNejNhBAX/LqRzUMPfOxCRT7A4L6oWPPnJ6P7I+p3cIYbsxmyyJFmqODHdQwB88lQM3I2CPrsbSMAfxEkX94SZbyJNvrbF7bT+5CuKvSkzW1WOqw1iuhJWDrX2eR/S6IWKvcKos40UKFBZ6/xim+gAImJLqodpjw0KdUBmqB82egbFpF+L39pdVBgxInRQg3yuKXETxxxhV60jSUKgzIR9Gzwrdm+v3JnpjStb860u3KpDexVh0O8ydZT8kKdZCHQxZIlhwxFjRMHNnM2YklFWaAgM1enYpunMb70Yn1kMgI4gUGQLD5GV/AZuxqml6ze92oFhjjOoxwNeuCd8mtSaskVvUTBUWotYY5Cun27OeGm146RxMWMyJ3mqGLPl5INvKbZ1MXjaBUkaKgQwEr6kJ72gMxVj7QmTtjXYs3P/DwjUNb9NBnLyAOvgPb6JpuwQ0lu0vuJ8w3vIl5LrNQqY6FyWG/b20FsmlQTypqrGeXTNcygInpAkGIWoNI600g6pkYN2AkgHiuMOVyPo6Vucf82n7dL4JZbWejQciAa1D3lAdvdINRJTHDJTTBsKjAc2tIphFOkJg8f85KoISn8zcxtEe+LnkGzcwUwmYApti7K6fPmZYMx0jOmpp272NZx4RS2YLnnGExqllYa4ZOnmviVAO+6Eg8xhqkA78THz3C5AQgembihYRplee+ETRmimrq+yMVLRMs/KBCY+dNLqpKfvzzZXONCE4X5lKzkx9+QINwQeaJ+7T8BHG/EJCWPbOXh2+P6uvSinLhYcc7hyqPcBxf5Myf+UdSWWvirCD91KtjYKXEGJURw+Me7C3aMSXpgCChp2neKATxlZ1vCm5Q3GcCNZQ5NDOQoysyNw1ogMOaiC1ilFiEg5meUCHBYUaG9pk2Yc2AhMS6aMumHr2nFy9oSGC2VHDVwoQ/tqgw74qCpkYCtiOpGPo5adtAep0Na6Gh0G1Sol98rvvaxBMFsxtG352Rg4ppmW4W/Y2NfZ6bAE5MmhR6jpAaA4WmsTm3pPUbk7TJGHIK4w4p2DURfBfOmzeyPlecI/Ce/mRlqMnI8o2FhowS7IBbqtugmKllBFSjRQfkmXuiFB4B6+0ykvMwEb6M0iF2PdUB5VHJ8AcfuDweXI+yhjFNsGED+u+LlfytZchuX0Vp+jb5EJbg2VhgNhJf3VAric3MN6bsDKTL/sY02KWG8yI5F523ye5uF2L0zRqi3rdhBgaJZw3Bxu3w6rzNq2JViBj9DV3QnVkzaQFAKnRTpXjAlF6YwbuBD0QKLhcYCRLECZWi55iQWNeEJhBW0cG0NeaLpsbMyxPE2L1B6cSM2aiswIf3UcmHbhNN/5Bu+X0GA8yXikdkcU5Ft0a7iNBRwLpSKnpbNNhTAmF4eMgEvrJPaY1nCStk+kCOPnsRkgIte5CDSzj/3Ql1g/hhCBiwqSVXPX2Ji/56hkgmv3mnw3mEdyFtc6reFN/inVJIfcZri+DODrOeKceuX/wWG/HF6g8h1RpOY+8CcAYyfy9BDd+FhqaozkPp1PNT9xi+uvaEHg9ryD1F7Rl5r+BvEPY+FH5J6SwUc2goH7jGn5hoXwdhJvWAXP59PLylrqkGugWczCbmXZcpwa9zTmYRUSMuvMLZ10juL7d42U6/za7alMrX3uyVm7p9yOXRLiErKjMbfe50KrpssTzoofP83FRI/2y58EXvIUmyBoraiTAnVPadPvxEfhkFrBFeekediH+8SAtbfaPcXnuJmSZetrGdRL/6OkWBu6/62ZNSobIn4gUToyH6uQ8U3RESG3kaztGjZtDem3KfckFV34rHvjvssuBgJafNo2oRzWifbn7hAWkKtClK+n/opCmqpR6s6arOUAb4cMOjw+WXbvGcaZb5yKiAxKvc9j0tY94fcLbRmSFFDz4rSJ0FNrjWH1EmMr8lqt24Di1JMee+jafkr0qQjDJCs4hMEMEvPEh9PiN0IPJdFYRo62goYcVQke7115VK4h0Nk3yhvfhK2fF23c+pwnlmIctLmItx2wLFbTg0UQef2gCEF7AryDt/zfRCLvw0/vB0crhGxnmbyKdrAe9pcBN+F5FBeSdgRkk4R2/NbPyyxZ+ITGgbXF7uDOZ8bDiv/qaP1A1j1V114kTFWQmp+nlTwBG+k5Piyv2XS+W0Wi3U7Y1tmZIdZiT0U9n5d3qF7Jqo1R53iWkUM+HK0BOm+VTXvBh1or4F5aCTWeAU/1Ql5QFTLK2e+qA9zKP90TIm94hHfQKITqZG+h2xEWjQ6JDUtyUDUbj8h2g+5kALSKP92a2GbDUKDGTqWzYXaLxpSagYNT9aQ3s4wjCoTpJsR95EfFbMIx7w8h5nM55Ex8Iu+kPgkdFtP94fhpcNlrUF+U9n4xHsWv7IC+SMPr6mlfQs9zkj4ozWmtW+ci/HOB7r9GXCW7Z5lFWA2++5gWYMxyXlFtLfdhoa+laklMjlAtspRqr7ayth3ejTrhVxLUS4ELSjxT4HObbpmN1wVcZNJOSkNl4XH3JvwJGFbb7qEIF3nniJeT3DED475sYOX7uA8uo4PmajnWyaXIy0DAV37qVyBm4p33Gi3WHRVTtt08JxMashKRxoMLINbwWt2CNalRuvaibhUfsOJOGNlakSLmhsDoJPT/cWBFoyTmtiCbhHXJ2T/RCacz+QKBQrh9Sr1a2OHtE7ifyO4IcTEKLVwlzkR3I5r04x5y9we3sgt1ggwlzjutuGjJLyGTljen8SqfK+JhQ0jSDc970yiDmlLABomAvwK3OmzFOaN6oxFtXdWl+1e5eg8+w30PVU5Pc+1T80aJgO2QzLI4p40TbDhc2GitdXV5pQtfgrn+/2urx4Sb4nmL0sw0DixRyCeTb3NmhIsPHFtSm2TuiTj3QkNF90PWf6+Rck35Ox+EHpTDraY2ZSPiHStzo/XnFbotZm27ze2MqsUem8oqw5KW2LuQ49Myj4KgFPODH7gelRad8JmcbjHEn7iq0qB2fQcVpLR8FVTFGx9HUY2C0onDo+DVufyrwCLOlnmqcpJY3yzN60Z6j4TiijCWyDVvaPa8W0mNxbBpEkTvI/aeY9Tzy/yBjFJK3r0fVVNa9VJ+uDSUEEjsODYcrQzzdxbmFOR+2NeS+wZFI5ZdLObCSgOkGJJXak0/m+MJh9MsGuBlfVEIAYDLIC9CcyAgcc2OMrq3XABRcfo9OyGfzza5Ft3Cv0OJJaLUcG1TY0sbsmztw01I+k55pVvBG8Hcto0z3+8MDP2YjJ5KkgU7rpVP+PlzJpSCnEFMyGlXVGVUxoGfXiqv0G6JEVHv/i9Are7EUeY8FBabCvSA7b9P2OG56O/18kz2twWLK1mHaFSKhrKnSBdDcl4kRSxIzaoQWS6oE9AuYaAdKZDx7klp/HwZfbwpdyA39+mz+uCVb7LgJHRZpLjo/I4f1/A/cISO+wunW+rALo+GHAX/Hd6l2/9JaNrvRBrkVdbiJqDQ4lA0phvsDC3rv4a24VhID9K4JRphM+ghg1aoLqVqMrSjLZszBy6plGJjJHSJlhuo/qR2uzE9z4axd3ELHEz9Z79P5dw4qf6WH09YNK+iusJSbEHDnRFXC6huiBIBcka7yOuuKPY6OgjLbdIZZiUvLJ14JSpo4NTXldKynHSFarfufOacuNWcR8A0MNrfQGz03/IpSMs5H9YmIjxcxVXTa3JBzSiAM33wLea56lY1Vm1gY0f/iHTEsliFb87oZ1ovVTj1RTRjuYdq0yiVjBrMKJp14h84VMk8rigyfS4lWRggjfXovofV2BIojnidhnXuHKfkwrFQ+2W9tHkr0UQamHcQX55Klpr9frgWXgYjGHx9nK7vB19C6jNIxI+MAX4CKDhJ0xR4dP9lCuymeS7McwRRazYSVQSQCg7FG8YuzvRDuZGgdNzppXqaeq0j3An8Rcbb/Q1ZwIX0OB40LZi5S0eefrJyCSbMMMrKqWw1NCd7+lgquKE607Cf6Yko4+lspQ3uu5DETLooIkXf++40Q93O5L18BaODgeN1LnW4uFlUxCnWT0mSV7/UCr8E3W+W2HVFm3BmPzNCCPD3Ivage632lsA8yASOftWEC/420aqcleaeA0ay/Xzt13guTz9ruUUaTUBkj6RqI8sdBkSXmMaaEGNhu8Jn/1uz/M124ZqBLkx024wC48HPadnTkV8tcO7XKhnK19cr1U1bfMoKYHT9NTOZNg/nf90jH/dFSrDR9YqaCDyh1JAF8iv5M047V4N7q4GZCjCwLCUSySlwREMC7oDGJZPu2grVmiqm4+KRmvAqfNDRHdNs8WzcVZR9a6Z0LoIpUTPZSSRxKEkn7wV03aXibwfU0IQWnrzvz1/2ig67H6PhGKOAs9kTfhSoyKxOMZiZgOiIV/PddV9sUkpyo6aJwEn4qEOXYwtwdcV+KBZh6zYj5WVmfkxT8tWbW40mf55N1aIjfg0nIb2W4u4nbZlBXPtD92C/63JVDYERbkRpxSLNrvivcAvwxG2c+xwQehQp+P4nmUZu0z+QX2m8LX53xqrz/8p2VYTNQodjhNngX4u9x2v9VAlXoz8G6j3Gwigf6+eVtAsruTgQsPl8smuBXTw4Bi7nGjeJPhWQY/3ou8Us70i7PBpq4Zy4kbbyJTJ3shVAe19SZehnoBYXksb6rpLFMk+7dWiRDbykV2htVIdtJmS9dGqqetolS3kB3ZoaNv14Q1oDWmBDADn+j5nlZyeIfWN4alW9mhmoghF96HKKV1YXfE7EWvTlo0mFNHH4dNirWwmPCpQaBT3JaF4vtnYDzfXos9Lq+7ySfv//c56VNDOJzkqeCu+YD34ozajOceQVZ9ruD5OYV4FUTUuvDS0XT6K82tFgf/FeqDgctuJYyDalXwUyBSJZYcz06I81zywjXSAphBEYnf9cYiaTau/cAXtEuUpbJ8JropilRZqU+V/3f47o7gT75AyX/xY33F8x3Ryu/bkQx3JeF3e1aHyJkLURulkHrV2ow5Kj8PBw7su1dDHwmxIvfBqri2DUJWlBuEeQ8laXuFMDt4PM2PqnPetnuqwBQQ83muvnhUC8vOky1oR1ZIM6OXAjSSOyarW8T3PvMBNaTPVLStkr71CiSkRPdCTcj5KHKpwlxzol3CZtElbJEGiPLfp/DyxQ39HEGREDcRoC2aMlF4eB3B7Bb0YgH0YeqsitYHIJoa+pg5V98wCgIUsn8qcRlQbVAj4Gmbga7d9N/XoKYoRzdSp2W/IW96qPRwGSM7c+9wDmDILu099/x7n56hTSLhcYJr7a+UutXuX6ztOoNhuPoYLrzv4mG3LfrysVoptnCKKDqEV5gyP2+9trU9CFDf4/OGN11uF9rBnz7Zzq3PIkdRQh7ae6gMfD/LLXZz9LW+XUpGKOQmDroD6esw35hUN+Ple7z7wb2qbmlcDEoeeAZh2h57A+zk2J3k3rdovmLLxXEGsOmk2QFS3Kx6d0Wk8jHcLddXpTGun4F33dxVwzSCDLmvH5X1wNWQYMgo6hwRV+ld5qgryrKdqtxa18nUpZ3meewBHZ9eylHykxkbSySZ8nlPq5pXf2VuqkUBRHHqjYz7P3P3S62OqEbbccIgXpeR/+hnkfaN4vcrzaWT2Zar8EuwraidylrpHsOMF7XAh71SNkTsDlBmXkQi1/qH9iLzxieTZrOA+kzLdzLmWm1CZv7F/fCgdr2cVgYNm/E0uEHjl3hbdmLdWOXDeRV0h4JtEb/+tCX1Wzez13I54Qp3jukVMjXmlNbSo3e16p3zCh32Q0/dEVYoEWxk3UCfHvBUXGNz0WHmf0mFf/D1uTKp9hiYhZ8ZQLdJJnCb2dsFTufSCzItgqqSqDqRGjgvvXfANMGzi4R3ijZCE+RVIU1hg+9re8rtcgDbOBOBSN/2rckVZP7xhueSfhv26g19s+oElglTNhmqaCm27cifHBnCO2cvFx5/1wcpd16qeUz2upUih3ttZI0wdXPiYWTw53oKAyoH1JVt52GXJwGAR24QP7R3IQHDLDJXRKuLsC8L2kMLZBz3o6YoKhCkTn4f9RoQ1k9Mj5IrwX3IOUL51YakOhs5f5yArJQZxlC/Xc6rKdq8AxP61isGD3o7qmZnyNHPY0fQ4CUKL11n+AU2unGDEruMlGAqGQvgZNOd+FhkcKYabg0okFSLf3RjgDBhuSwIF1s2wEkR5qLq8PiVLf44a7hJ2kiw==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV2aNbE0v3xAhmIgl78LJs2xV0TBqp0uo6vQSD43MG7X87rwPP41&amp;t=636765499264470882" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var __cultureInfo = {"name":"sr-Cyrl-CS","numberFormat":{"CurrencyDecimalDigits":2,"CurrencyDecimalSeparator":",","IsReadOnly":false,"CurrencyGroupSizes":[3],"NumberGroupSizes":[3],"PercentGroupSizes":[3],"CurrencyGroupSeparator":".","CurrencySymbol":"дин.","NaNSymbol":"није број","CurrencyNegativePattern":8,"NumberNegativePattern":1,"PercentPositivePattern":1,"PercentNegativePattern":1,"NegativeInfinitySymbol":"-бесконачност","NegativeSign":"-","NumberDecimalDigits":2,"NumberDecimalSeparator":",","NumberGroupSeparator":".","CurrencyPositivePattern":3,"PositiveInfinitySymbol":"+бесконачност","PositiveSign":"+","PercentDecimalDigits":2,"PercentDecimalSeparator":",","PercentGroupSeparator":".","PercentSymbol":"%","PerMilleSymbol":"‰","NativeDigits":["0","1","2","3","4","5","6","7","8","9"],"DigitSubstitution":1},"dateTimeFormat":{"AMDesignator":"","Calendar":{"MinSupportedDateTime":"\/Date(-62135596800000)\/","MaxSupportedDateTime":"\/Date(253402297199999)\/","AlgorithmType":1,"CalendarType":1,"Eras":[1],"TwoDigitYearMax":2029,"IsReadOnly":false},"DateSeparator":".","FirstDayOfWeek":1,"CalendarWeekRule":0,"FullDateTimePattern":"d. MMMM yyyy. H:mm:ss","LongDatePattern":"d. MMMM yyyy.","LongTimePattern":"H:mm:ss","MonthDayPattern":"d. MMMM","PMDesignator":"","RFC1123Pattern":"ddd, dd MMM yyyy HH\u0027:\u0027mm\u0027:\u0027ss \u0027GMT\u0027","ShortDatePattern":"d.M.yyyy.","ShortTimePattern":"H:mm","SortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss","TimeSeparator":":","UniversalSortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd HH\u0027:\u0027mm\u0027:\u0027ss\u0027Z\u0027","YearMonthPattern":"MMMM yyyy.","AbbreviatedDayNames":["нед.","пон.","ут.","ср.","чет.","пет.","суб."],"ShortestDayNames":["не","по","ут","ср","че","пе","су"],"DayNames":["недеља","понедељак","уторак","среда","четвртак","петак","субота"],"AbbreviatedMonthNames":["јан.","феб.","март","апр.","мај","јун","јул","авг.","септ.","окт.","нов.","дец.",""],"MonthNames":["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар",""],"IsReadOnly":false,"NativeCalendarName":"грегоријански календар","AbbreviatedMonthGenitiveNames":["јан.","феб.","март","апр.","мај","јун","јул","авг.","септ.","окт.","нов.","дец.",""],"MonthGenitiveNames":["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар",""]},"eras":[1,"н.е.",null,0]};//]]>
</script>

<script src="/ScriptResource.axd?d=uHIkleVeDJe71swkG8xd5vEURr7bXsMJksoCf852JUAfVA1fWCu4qAk19j2IwdllUwCO_W6706nyIVYW4QTXHqm77HR_M_G66KTg4JFFOEt2C7dFZvbabdR-beOOrhWVp-Cn7A2&amp;t=ffffffff999c3159" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Jw6tUGWnA14woTgPvOH-aA_V-3Ps9rcTdh6L5lcuhs9ykMNgm6DjPhCCH3aesZTLFqGFhHOx76ImCYZ03W-VQoV2Z0XZgCNG9ADSPgy-msteT5x5-pg2Cgfx8nWK7MfsCsU83zU9QoHYT67M0&amp;t=ffffffff999c3159" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
dnn.controls.submitComp.onsubmit();
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="Fbw+J0nuVBdMccDyM5faQqESZ+VsZRxQcRaT8VgAhJoN7Bw3nFjn2iu10iIT094uLWIWEyn8UqPEvAlV2px5b2GO79dCsb3PzoJyvOFCVVtQzMddIknuUeNbXEvDaO4pWusyaEVouvEhYsD62sqvhZI3SUw=" />
</div><script src="/js/dnn.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.xmlhttp.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.controls.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.modalpopup.js?cdv=581" type="text/javascript"></script><script src="/js/dnncore.js?cdv=581" type="text/javascript"></script><script src="/Resources/Search/SearchSkinObjectPreview.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.servicesframework.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.dom.positioning.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.controls.dnnmenu.js?cdv=581" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        
<script type="text/javascript" src="/Portals/0/Skins/Flex2-V-Round-Stripe/swfobject.js"></script>
<script type="text/javascript">
var params = { wmode: "transparent", FlashVars: "XMLpath=/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.images.xml" };
swfobject.embedSWF("/Portals/0/Skins/Flex2-V-Round-Stripe/flash/flex-900x112.swf", "FlashBannerContainer", "900", "112", "9.0.0", false, false, params);
</script>

<script type="text/javascript" src='/Portals/0/Skins/Flex2-V-Round-Stripe/drnuke-height.js'></script>
<!--[if lte ie 6]>
<script type="text/javascript">
if (typeof blankImg == 'undefined') var blankImg = '/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif';
</script>
<style type="text/css">
.trans-png, #Body, .flex-container-visibility a img { behavior: url(/Portals/0/Skins/Flex2-V-Round-Stripe/drnuke-png.htc) }
</style>
<![endif]-->
<!--[if gte IE 5]>
<link rel="stylesheet" type="text/css" media="all" href="/Portals/0/Skins/Flex2-V-Round-Stripe/css/ie.css">
<![endif]-->  
<!--[if ie 6]>
<link rel="stylesheet" type="text/css" media="all" href="/Portals/0/Skins/Flex2-V-Round-Stripe/css/ie6.css">
<![endif]-->

<div id="OuterContainer" class="EMWidth">   

<table class="EMSkinTable fullwidth" border="0" cellspacing="0" cellpadding="0">
<tr id="EMOffset1"><td class="EMSkinTL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td style="vertical-align:bottom;">
	<table class="fullwidth" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="EMSkinTL2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    <td class="EMSkinT trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
	<td class="EMSkinTR2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    </tr>
    </table>
</td>
<td class="EMSkinTR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td></tr>
<tr>
<td class="EMSkinL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td class="EMSkinM">
	<div id="ContentHeightContainer">
	<div id="InnerContainer">
    	<div style="float:left;"> 
            <div id="LogoContainer" class="EMLogoContainer">
                <a id="dnn_dnnLOGO_hypLogo" title="Agencija za privredne registre (APR)" aria-label="Agencija za privredne registre (APR)" href="http://www.apr.gov.rs/%d0%9d%d0%b0%d1%81%d0%bb%d0%be%d0%b2%d0%bd%d0%b0.aspx"></a>
            </div>
        </div>
        <div style="float:right;">
            <div id="LogoRightContainer">
                <div id="DateContainer" class="EMDateContainer">
                <span id="dnn_dnnCURRENTDATE_lblDate" class="DateToken EMFontFamily">четвртак 11. април  2019.</span>

                </div>
                
                <div id="LanguageContainer" class="EMLanguageContainer">
                <div class="language-object" >


</div>
                </div>            
            </div>
        </div>
        <div class="clear"></div>
        
        <div id="MenuBottomContainer">
        <table cellpadding="0" cellspacing="0" border="0" class="fullwidth">
        <tr><td id="MenuBarL" class="EMBaseColour6"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-bl.png" alt="" class="trans-png" /></td><td id="MenuBarM" class="EMBaseColour6"></td><td id="MenuBarR" class="EMBaseColour6"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-br.png" alt="" class="trans-png" /></td></tr>
        </table>
        </div>
          
        <div id="BannerOuterContainer">
            <div id="FlashBannerContainer">
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
            </div> 
        </div>
          
        <div id="UnderBannerContainer">
        <table cellpadding="0" cellspacing="0" class="fullwidth">
        <tr>
        <td><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/login-l.gif" width="9" height="31" alt="" /></td>
        <td class="fullwidth">
            <div id="BreadcrumbContainer" class="EMBreadcrumbContainer">
                <span class="EMBaseColour4 BreadcrumbSpan"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/breadcrumb.png" alt="" class="trans-png" /></span><span id="dnn_dnnBREADCRUMB_lblBreadCrumb" itemprop="breadcrumb" itemscope="" itemtype="https://schema.org/breadcrumb"><span itemscope itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="http://www.apr.gov.rs/Насловна.aspx" class="BreadcrumbToken EMFontFamily" itemprop="item"><span itemprop="name">Насловна</span></a><meta itemprop="position" content="1" /></span></span></span>
            </div>
            <div id="LoginContainer">
                <table border="0" cellpadding="0" cellspacing="0">
                <tr><td class="EMBaseColour5 trans-png"><div></div></td></tr>
                </table> 
            </div>
            <div id="UserContainer">
<a href="http://www.apr.gov.rs/eng/Home.aspx"><strong>English</strong></a>
                <table border="0" cellpadding="0" cellspacing="0">
		
                <tr><td ></td></tr>
                </table> 
            </div>
            
		</td>
        <td><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/login-r.gif" width="9" height="31" alt="" /></td>
        </tr>
        </table>
        </div>

		<div id="ContentContainer">
        <table border="0" cellpadding="0" cellspacing="0" class="fullwidth">
        <tr>
        <td id="MenuContainerCell">
        
            <div id="MenuContainer"><div>
	<ul class="mainmenu-menuitem EMSubMenuItemOff EMMainMenuFont" id="dnn_dnnNAV_ctldnnNAV">
		<li id="dnn_dnnNAV_ctldnnNAVctr39"><a href="http://www.apr.gov.rs/%d0%9d%d0%b0%d1%81%d0%bb%d0%be%d0%b2%d0%bd%d0%b0.aspx"><span>Насловна</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr113"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8.aspx"><span>О Агенцији</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub113">
			<li id="dnn_dnnNAV_ctldnnNAVctr285"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a0%d0%b5%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d0%b5%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Референце Агенције</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr242"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%9e%d1%81%d0%bd%d0%b8%d0%b2%d0%b0%d1%9a%d0%b5.aspx"><span>Оснивање</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr243"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%9e%d1%80%d0%b3%d0%b0%d0%bd%d0%b8%d0%b7%d0%b0%d1%86%d0%b8%d1%98%d0%b0.aspx"><span>Организација</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr244"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a3%d0%bf%d1%80%d0%b0%d0%b2%d0%bd%d0%b8%d0%be%d0%b4%d0%b1%d0%be%d1%80.aspx"><span>Управни одбор</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr245"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a1%d0%b2%d0%b8%d1%80%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8%d0%bd%d0%b0%d1%98%d0%b5%d0%b4%d0%bd%d0%be%d0%bc%d0%bc%d0%b5%d1%81%d1%82%d1%83.aspx"><span>Сви регистри на једном месту</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr353"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%98%d0%bd%d1%82%d0%b5%d1%80%d0%bd%d0%b0%d0%b4%d0%be%d0%ba%d1%83%d0%bc%d0%b5%d0%bd%d1%82%d0%b0.aspx"><span>Интерна документа</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr139"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8.aspx"><span>Регистри</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub139">
			<li id="dnn_dnnNAV_ctldnnNAVctr59"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b0%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b0.aspx"><span>Привредна друштва</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr60"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b5%d0%b4%d1%83%d0%b7%d0%b5%d1%82%d0%bd%d0%b8%d1%86%d0%b8.aspx"><span>Предузетници</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr216"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d0%b8%d0%b7%d0%b2%d0%b5%d1%88%d1%82%d0%b0%d1%98%d0%b8.aspx"><span>Финансијски извештаји</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr61"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d0%bb%d0%b8%d0%b7%d0%b8%d0%bd%d0%b3.aspx"><span>Финансијски лизинг</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr62"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%97%d0%b0%d0%bb%d0%be%d0%b6%d0%bd%d0%be%d0%bf%d1%80%d0%b0%d0%b2%d0%be.aspx"><span>Заложно право</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr116"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9c%d0%b5%d0%b4%d0%b8%d1%98%d0%b8.aspx"><span>Медији</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr117"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a3%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9a%d0%b0.aspx"><span>Удружења</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr118"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%82%d1%80%d0%b0%d0%bd%d0%b0%d1%83%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9a%d0%b0.aspx"><span>Страна удружења</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr229"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a2%d1%83%d1%80%d0%b8%d0%b7%d0%b0%d0%bc.aspx"><span>Туризам</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr258"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%82%d0%b5%d1%87%d0%b0%d1%98%d0%bd%d0%b5%d0%bc%d0%b0%d1%81%d0%b5.aspx"><span>Стечајне масе</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr260"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9c%d0%b5%d1%80%d0%b5%d0%b8%d0%bf%d0%be%d0%b4%d1%81%d1%82%d0%b8%d1%86%d0%b0%d1%98%d0%b8%d1%80%d0%b5%d0%b3%d0%b8%d0%be%d0%bd%d0%b0%d0%bb%d0%bd%d0%be%d0%b3%d1%80%d0%b0%d0%b7%d0%b2%d0%be%d1%98%d0%b0.aspx"><span>Мере и подстицаји регионалног развоја</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr262"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b5%d0%ba%d0%be%d0%bc%d0%be%d1%80%d0%b5%d0%b8%d0%bf%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bd%d0%b8%d1%88%d1%82%d0%b2%d0%b0%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%85%d0%bf%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b8%d1%85%d0%ba%d0%be%d0%bc%d0%be%d1%80%d0%b0.aspx"><span>Привредне коморе и представништва страних привредних комора</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr341"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%97%d0%b0%d0%b4%d1%83%d0%b6%d0%b1%d0%b8%d0%bd%d0%b5%d0%b8%d1%84%d0%be%d0%bd%d0%b4%d0%b0%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Задужбине и фондације</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr343"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bd%d0%b8%d1%88%d1%82%d0%b2%d0%b0%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%85%d0%b7%d0%b0%d0%b4%d1%83%d0%b6%d0%b1%d0%b8%d0%bd%d0%b0%d0%b8%d1%84%d0%be%d0%bd%d0%b4%d0%b0%d1%86%d0%b8%d1%98%d0%b0.aspx"><span>Представништва страних задужбина и фондација</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr366"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d0%bf%d0%be%d1%80%d1%82%d1%81%d0%ba%d0%b0%d1%83%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9aa,%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b0%d0%b8%d1%81%d0%b0%d0%b2%d0%b5%d0%b7%d0%b8.aspx"><span>Спортска удружењa, друштва и савези</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr377"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%83%d0%b4%d1%81%d0%ba%d0%b5%d0%b7%d0%b0%d0%b1%d1%80%d0%b0%d0%bd%d0%b5.aspx"><span>Судске забране</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr599"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b0%d0%ba%d1%82%d0%be%d1%80%d0%b8%d0%bd%d0%b3.aspx"><span>Факторинг</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr591"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d0%be%d0%bd%d1%83%d1%92%d0%b0%d1%87%d0%b8.aspx"><span>Понуђачи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr779"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a3%d0%b3%d0%be%d0%b2%d0%be%d1%80%d0%b8%d0%be%d1%84%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%80%d0%b0%d1%9a%d1%83%d0%bf%d0%be%d1%99%d0%be%d0%bf%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b5%d0%bf%d1%80%d0%be%d0%b8%d0%b7%d0%b2%d0%be%d0%b4%d1%9a%d0%b5.aspx"><span>Уговори о финансирању пољопривредне производње</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr828"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%93%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>Грађевинске дозволе</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr885"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%bc%d0%b5%d0%bd%d0%b0%d0%be%d0%b3%d1%80%d0%b0%d0%bd%d0%b8%d1%87%d0%b5%d1%9a%d0%b0%d0%bf%d1%80%d0%b0%d0%b2%d0%b0%d0%bb%d0%b8%d1%86%d0%b0.aspx"><span>Привремена ограничења права лица</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr970"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a6%d0%b5%d0%bd%d1%82%d1%80%d0%b0%d0%bb%d0%bd%d0%b0%d0%b5%d0%b2%d0%b8%d0%b4%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b0%d1%81%d1%82%d0%b2%d0%b0%d1%80%d0%bd%d0%b8%d1%85%d0%b2%d0%bb%d0%b0%d1%81%d0%bd%d0%b8%d0%ba%d0%b0.aspx"><span>Централна евиденција стварних власника</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr64"><a href="http://www.apr.gov.rs/%d0%9f%d1%80%d0%be%d0%bf%d0%b8%d1%81%d0%b8.aspx"><span>Прописи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr132"><a href="http://www.apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5.aspx"><span>Услуге</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub132">
			<li id="dnn_dnnNAV_ctldnnNAVctr427"><a href="http://www.apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d0%bf%d0%be%d0%b4%d0%b0%d1%82%d0%b0%d0%ba%d0%b0%d0%b1%d0%b5%d0%b7%d0%bd%d0%b0%d0%ba%d0%bd%d0%b0%d0%b4%d0%b5.aspx"><span>Достављање података без накнаде</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr509"><a href="http://www.apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d0%bf%d0%be%d0%b4%d0%b0%d1%82%d0%b0%d0%ba%d0%b0%d1%83%d0%b7%d0%bd%d0%b0%d0%ba%d0%bd%d0%b0%d0%b4%d1%83.aspx"><span>Достављање података уз накнаду</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr133"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83.aspx"><span>Односи с јавношћу</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub133">
			<li id="dnn_dnnNAV_ctldnnNAVctr68"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%92%d0%b5%d1%81%d1%82%d0%b8.aspx"><span>Вести</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr310"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9f%d1%80%d0%b5%d1%81%d1%81%d0%bb%d1%83%d0%b6%d0%b1%d0%b0.aspx"><span>Прес служба</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr66"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9c%d0%b5%d1%92%d1%83%d0%bd%d0%b0%d1%80%d0%be%d0%b4%d0%bd%d0%b8%d0%be%d0%b4%d0%bd%d0%be%d1%81%d0%b8.aspx"><span>Међународни односи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr67"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9a%d0%be%d0%bd%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Конференције</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr328"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%90%d0%9f%d0%a0%d0%bd%d0%b0%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b5%d0%bd%d0%b8%d0%bc%d0%bc%d1%80%d0%b5%d0%b6%d0%b0%d0%bc%d0%b0.aspx"><span>АПР на друштвеним мрежама</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr140"><a href="http://www.apr.gov.rs/%d0%90%d1%80%d1%85%d0%b8%d0%b2%d0%b0.aspx"><span>Архива</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr72"><a href="http://www.apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.aspx"><span>Инфо центар и контакти</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub72">
			<li id="dnn_dnnNAV_ctldnnNAVctr228"><a href="http://www.apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80.aspx"><span>Инфо центар</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr715"><a href="http://www.apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%9a%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.aspx"><span>Контакти</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr322"><a href="http://www.apr.gov.rs/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%90%d0%9f%d0%a0.aspx"><span>Рачуни АПР</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub322">
			<li id="dnn_dnnNAV_ctldnnNAVctr944"><a href="http://www.apr.gov.rs/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%90%d0%9f%d0%a0/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%b7%d0%b0%d1%83%d0%bf%d0%bb%d0%b0%d1%82%d0%b5.aspx"><span>Рачуни за уплате</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr943"><a href="http://www.apr.gov.rs/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%90%d0%9f%d0%a0/%d0%9f%d0%be%d0%b2%d1%80%d0%b0%d1%9b%d0%b0%d1%98%d0%b2%d0%b8%d1%88%d0%ba%d0%b0%d1%83%d0%bf%d0%bb%d0%b0%d1%9b%d0%b5%d0%bd%d0%b8%d1%85%d1%81%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%b0.aspx"><span>Повраћај вишка уплаћених средстава</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr550"><a href="http://www.apr.gov.rs/%d0%88%d0%b0%d0%b2%d0%bd%d0%b5%d0%bd%d0%b0%d0%b1%d0%b0%d0%b2%d0%ba%d0%b5.aspx"><span>Јавне набавке</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr631"><a href="http://www.apr.gov.rs/%d0%92%d0%b8%d0%b4%d0%b5%d0%be%d1%83%d0%bf%d1%83%d1%82%d1%81%d1%82%d0%b2%d0%b0.aspx"><span>Видео упутства</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr954"><a href="http://www.apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5.aspx"><span>еУслуге</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub954">
			<li id="dnn_dnnNAV_ctldnnNAVctr969"><a href="http://www.apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%b5%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b0%d1%86%d0%b8%d1%98%d0%b0%d0%be%d1%81%d0%bd%d0%b8%d0%b2%d0%b0%d1%9a%d0%b0%d1%98%d0%b5%d0%b4%d0%bd%d0%be%d1%87%d0%bb%d0%b0%d0%bd%d0%be%d0%b3%d0%94%d0%9e%d0%9e.aspx"><span>еРегистрација оснивања једночланог ДОО</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr960"><a href="http://www.apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%b5%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b0%d1%86%d0%b8%d1%98%d0%b0%d0%be%d1%81%d0%bd%d0%b8%d0%b2%d0%b0%d1%9a%d0%b0%d0%bf%d1%80%d0%b5%d0%b4%d1%83%d0%b7%d0%b5%d1%82%d0%bd%d0%b8%d0%ba%d0%b0.aspx"><span>еРегистрација оснивања предузетника</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr961"><a href="http://www.apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%b5%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d1%84%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d1%85%d0%b8%d0%b7%d0%b2%d0%b5%d1%88%d1%82%d0%b0%d1%98%d0%b0.aspx"><span>еДостављање финансијских извештаја</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr962"><a href="http://www.apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%b5%d0%93%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>еГрађевинске дозволе</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr314"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d0%be%d0%bd%d0%b0%d0%bb%d0%bd%d0%b8%d0%bf%d0%be%d1%80%d1%82%d0%b0%d0%bb.aspx"><span>Регионални портал</span></a></li>
	</ul>
</div></div>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_MenuPane" class="MenuPane DNNEmptyPane" valign="top"></td>

            </tr>
            </table>
            <div id="SearchContainer" class="EMSearchContainer">
                    <div class="SearchBoxL">
                        <div class="SearchBox EMBaseColour3">
                        <span id="dnn_dnnSEARCH_ClassicSearch">
    
    
    <span class="searchInputContainer" data-moreresults="See More Results" data-noresult="No Results Found">
        <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" aria-label="Search" autocomplete="off" placeholder="Search..." />
        <a class="dnnSearchBoxClearText" title="Clear search text"></a>
    </span>
    <a id="dnn_dnnSEARCH_cmdSearch" class="SkinObject" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/search-button.png" hspace="0" alt="Search" class="trans-png" /></a>
</span>


<script type="text/javascript">
    $(function() {
        if (typeof dnn != "undefined" && typeof dnn.searchSkinObject != "undefined") {
            var searchSkinObject = new dnn.searchSkinObject({
                delayTriggerAutoSearch : 400,
                minCharRequiredTriggerAutoSearch : 2,
                searchType: 'S',
                enableWildSearch: true,
                cultureCode: 'sr-Cyrl-CS',
                portalId: -1
                }
            );
            searchSkinObject.init();
            
            
            // attach classic search
            var siteBtn = $('#dnn_dnnSEARCH_SiteRadioButton');
            var webBtn = $('#dnn_dnnSEARCH_WebRadioButton');
            var clickHandler = function() {
                if (siteBtn.is(':checked')) searchSkinObject.settings.searchType = 'S';
                else searchSkinObject.settings.searchType = 'W';
            };
            siteBtn.on('change', clickHandler);
            webBtn.on('change', clickHandler);
            
            
        }
    });
</script>

                        </div>
                    </div>         
                </div>
        </td>
        <td id="ContentContainerCell">
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_TopPane" colspan="2" class="TopPane DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane" class="LeftPane DNNEmptyPane"></td>

            <td id="dnn_RightPane" class="RightPane DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_TopPane2" colspan="3" class="TopPane2 DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane2" class="LeftPane2 DNNEmptyPane"></td>

            <td id="dnn_ContentPane1" class="ContentPane1 DNNEmptyPane"></td>

            <td id="dnn_RightPane2" class="RightPane2 DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_ContentPane" colspan="2" class="ContentPane"><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">
<table class="flex-container-1 fullwidth">
<tr class="EMContainerColour8">
<td class="flex-container-tl-simple trans-png"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="33" width="14" alt="" /></td>
<td class="flex-container-t-simple trans-png">
	
    <div class="flex-container-title"><h1 class="EMContainerTitleFontSize8"><span id="dnn_ctr_dnnTITLE_titleLabel" class="EMContainerTitleFontColour8 EMContainerTitleFontFamily8 EMContainerTitleFontSize8">Privacy Statement</span>


</h1></div>
    <div class="flex-container-visibility"></div>
    <div class="flex-container-help"></div>
</td>
<td class="flex-container-tr-simple trans-png"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="33" width="14" alt="" /></td>
</tr>
<tr>
  <td class="flex-container-l"></td>
  <td class="flex-container-m">
    <table class="flex-container-m-table fullwidth">
    <tr><td id="dnn_ctr_ContentPane" class="flex-container-m-td flex-container-content"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Agencija za privredne registre (APR) is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Agencija za privredne registre (APR) site and governs data collection and usage.
        By using the Agencija za privredne registre (APR) site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Agencija za privredne registre (APR) also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Agencija za privredne registre (APR). This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Agencija za privredne registre (APR) for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Agencija za privredne registre (APR) site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Agencija za privredne registre (APR) public message boards,
        this information may be collected and used by others. Note: Agencija za privredne registre (APR)
        does not read any of your private online communications.</p>
        <p>Agencija za privredne registre (APR) encourages you to review the privacy statements of Web sites
        you choose to link to from Agencija za privredne registre (APR) so that you can understand how those
        Web sites collect, use and share your information. Agencija za privredne registre (APR) is not responsible
        for the privacy statements or other content on Web sites outside of the Agencija za privredne registre (APR)
        and Agencija za privredne registre (APR) family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) collects and uses your personal information to operate the Agencija za privredne registre (APR)
        Web site and deliver the services you have requested. Agencija za privredne registre (APR) also uses
        your personally identifiable information to inform you of other products or services
        available from Agencija za privredne registre (APR) and its affiliates. Agencija za privredne registre (APR) may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Agencija za privredne registre (APR) does not sell, rent or lease its customer lists to third parties.
        Agencija za privredne registre (APR) may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Agencija za privredne registre (APR)
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Agencija za privredne registre (APR), and they are required to maintain
        the confidentiality of your information.</p>
        <p>Agencija za privredne registre (APR) does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Agencija za privredne registre (APR) keeps track of the Web sites and pages our customers visit within
        Agencija za privredne registre (APR), in order to determine what Agencija za privredne registre (APR) services are
        the most popular. This data is used to deliver customized content and advertising
        within Agencija za privredne registre (APR) to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Agencija za privredne registre (APR) Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Agencija za privredne registre (APR) or the site; (b) protect and defend the rights or
        property of Agencija za privredne registre (APR); and, (c) act under exigent circumstances to protect
        the personal safety of users of Agencija za privredne registre (APR), or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Agencija za privredne registre (APR) Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Agencija za privredne registre (APR) pages, or
        register with Agencija za privredne registre (APR) site or services, a cookie helps Agencija za privredne registre (APR)
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Agencija za privredne registre (APR) Web site, the information
        you previously provided can be retrieved, so you can easily use the Agencija za privredne registre (APR)
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Agencija za privredne registre (APR) services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) secures your personal information from unauthorized access,
        use or disclosure. Agencija za privredne registre (APR) secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Agencija za privredne registre (APR) will occasionally update this Statement of Privacy to reflect
company and customer feedback. Agencija za privredne registre (APR) encourages you to periodically 
        review this Statement to be informed of how Agencija za privredne registre (APR) is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Agencija za privredne registre (APR) welcomes your comments regarding this Statement of Privacy.
        If you believe that Agencija za privredne registre (APR) has not adhered to this Statement, please
        contact Agencija za privredne registre (APR) at <a href="mailto:srogic@apr.gov.rs">srogic@apr.gov.rs</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></td>
</tr>
    <tr><td class="flex-container-m-td">
    	<div class="flex-container-action2"></div>
        <div class="flex-container-syndicate"></div>
        <div class="flex-container-print"></div>
        <div class="flex-container-settings"></div>
    </td></tr>    
    </table>
  </td>
  <td class="flex-container-r"></td>
</tr>
<tr><td class="flex-container-bl"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="12" width="14" alt="" /></td><td class="flex-container-b"></td><td class="flex-container-br"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="12" width="14" alt="" /></td></tr>
</table>
<div class="clear cont-br"></div>







</div></td>

            </tr>                
            <tr>
            <td id="dnn_ContentPane2" class="ContentPane2 DNNEmptyPane"></td>

            <td id="dnn_RightPane3" class="RightPane3 DNNEmptyPane"></td>

            </tr> 
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">        
            <tr>
            <td id="dnn_MiddlePane" colspan="3" class="MiddlePane DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane3" class="LeftPane3 DNNEmptyPane"></td>

            <td id="dnn_ContentPane3" class="ContentPane3 DNNEmptyPane"></td>

            </tr> 
            </table>  
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_BottomPane" colspan="3" class="BottomPane DNNEmptyPane"></td>

            </tr>              
            <tr>
            <td id="dnn_LeftPane4" class="LeftPane4 DNNEmptyPane"></td>

            <td id="dnn_ContentPane4" class="ContentPane4 DNNEmptyPane"></td>

            <td id="dnn_RightPane4" class="RightPane4 DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_BottomPane2" class="BottomPane2 DNNEmptyPane"></td>

            </tr>
            </table>
        </td></tr>
        </table>
        
        </div>
      </div>
      </div>        
</td>        
<td class="EMSkinR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset2">
<td class="EMSkinL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td class="EMSkinM">
    <div id="FooterContainer">
    <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
    <tr>
    <td id="dnn_FooterPane" class="FooterPane DNNEmptyPane" valign="top"></td>

    </tr>
    </table>
    </div>
</td>
<td class="EMSkinR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset3">
<td class="EMSkinBL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td>
	<table class="fullwidth" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="EMSkinBL2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    <td class="EMSkinB trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
	<td class="EMSkinBR2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    </tr>
    </table>
</td>
<td class="EMSkinBR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset4">
<td class="EMSkinBL3 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td id="FooterCell" class="EMSkinB3">
    <div id="CopyrightContainer" class="EMCopyrightContainer"><span id="dnn_dnnCOPYRIGHT_lblCopyright" class="FooterToken EMFooterFont">Copyright 2009 APR</span>
</div>
    <div id="TermsContainer" class="EMTermsContainer"><a id="dnn_dnnTERMS_hypTerms" class="FooterToken EMFooterFont" rel="nofollow" href="http://www.apr.gov.rs/terms.aspx">Terms Of Use</a></div>
    <div id="PrivacyContainer" class="EMPrivacyContainer"><a id="dnn_dnnPRIVACY_hypPrivacy" class="FooterToken EMFooterFont" rel="nofollow" href="http://www.apr.gov.rs/privacy.aspx">Privacy Statement</a></div> 
</td>
<td class="EMSkinBR3 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
</table>

</div>


        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" value="`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`39`,`dnn_dnnNAV_ctldnnNAV_json`:`{nodes:[{bcrumb:\`1\`,selected:\`1\`,id:\`39\`,key:\`39\`,txt:\`Насловна\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Насловна.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-breadcrumbactive EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,cssSel:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`113\`,key:\`113\`,txt:\`О Агенцији\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`285\`,key:\`285\`,txt:\`Референце Агенције\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/РеференцеАгенције.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`242\`,key:\`242\`,txt:\`Оснивање\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Оснивање.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`243\`,key:\`243\`,txt:\`Организација\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Организација.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`244\`,key:\`244\`,txt:\`Управни одбор\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Управниодбор.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`245\`,key:\`245\`,txt:\`Сви регистри на једном месту\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Свирегистринаједномместу.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`353\`,key:\`353\`,txt:\`Интерна документа\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Интернадокумента.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`139\`,key:\`139\`,txt:\`Регистри\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`59\`,key:\`59\`,txt:\`Привредна друштва\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Привреднадруштва.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`60\`,key:\`60\`,txt:\`Предузетници\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Предузетници.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`216\`,key:\`216\`,txt:\`Финансијски извештаји\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Финансијскиизвештаји.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`61\`,key:\`61\`,txt:\`Финансијски лизинг\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Финансијскилизинг.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`62\`,key:\`62\`,txt:\`Заложно право\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Заложноправо.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`116\`,key:\`116\`,txt:\`Медији\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Медији.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`117\`,key:\`117\`,txt:\`Удружења\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Удружења.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`118\`,key:\`118\`,txt:\`Страна удружења\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Странаудружења.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`229\`,key:\`229\`,txt:\`Туризам\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Туризам.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`258\`,key:\`258\`,txt:\`Стечајне масе\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Стечајнемасе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`260\`,key:\`260\`,txt:\`Мере и подстицаји регионалног развоја\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Мереиподстицајирегионалногразвоја.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`262\`,key:\`262\`,txt:\`Привредне коморе и представништва страних привредних комора\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Привреднекомореипредставништвастранихпривреднихкомора.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`341\`,key:\`341\`,txt:\`Задужбине и фондације\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Задужбинеифондације.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`343\`,key:\`343\`,txt:\`Представништва страних задужбина и фондација\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Представништвастранихзадужбинаифондација.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`366\`,key:\`366\`,txt:\`Спортска удружењa, друштва и савези\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Спортскаудружењa,друштваисавези.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`377\`,key:\`377\`,txt:\`Судске забране\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Судскезабране.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`599\`,key:\`599\`,txt:\`Факторинг\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Факторинг.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`591\`,key:\`591\`,txt:\`Понуђачи\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Понуђачи.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`779\`,key:\`779\`,txt:\`Уговори о финансирању пољопривредне производње\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Уговориофинансирањупољопривреднепроизводње.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`828\`,key:\`828\`,txt:\`Грађевинске дозволе\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Грађевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`885\`,key:\`885\`,txt:\`Привремена ограничења права лица\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Привременаограничењаправалица.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`970\`,key:\`970\`,txt:\`Централна евиденција стварних власника\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Централнаевиденцијастварнихвласника.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`64\`,key:\`64\`,txt:\`Прописи\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Прописи.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`132\`,key:\`132\`,txt:\`Услуге\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Услуге.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`427\`,key:\`427\`,txt:\`Достављање података без накнаде\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Услуге/Достављањеподатакабезнакнаде.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`509\`,key:\`509\`,txt:\`Достављање података уз накнаду\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Услуге/Достављањеподатакаузнакнаду.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`133\`,key:\`133\`,txt:\`Односи с јавношћу\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`68\`,key:\`68\`,txt:\`Вести\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Вести.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`310\`,key:\`310\`,txt:\`Прес служба\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Пресслужба.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`66\`,key:\`66\`,txt:\`Међународни односи\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Међународниодноси.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`67\`,key:\`67\`,txt:\`Конференције\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Конференције.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`328\`,key:\`328\`,txt:\`АПР на друштвеним мрежама\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/АПРнадруштвениммрежама.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`140\`,key:\`140\`,txt:\`Архива\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Архива.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`72\`,key:\`72\`,txt:\`Инфо центар и контакти\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Инфоцентариконтакти.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`228\`,key:\`228\`,txt:\`Инфо центар\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Инфоцентариконтакти/Инфоцентар.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`715\`,key:\`715\`,txt:\`Контакти\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Инфоцентариконтакти/Контакти.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`322\`,key:\`322\`,txt:\`Рачуни АПР\`,ca:\`3\`,url:\`http://www.apr.gov.rs/РачуниАПР.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`944\`,key:\`944\`,txt:\`Рачуни за уплате\`,ca:\`3\`,url:\`http://www.apr.gov.rs/РачуниАПР/Рачунизауплате.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`943\`,key:\`943\`,txt:\`Повраћај вишка уплаћених средстава\`,ca:\`3\`,url:\`http://www.apr.gov.rs/РачуниАПР/Повраћајвишкауплаћенихсредстава.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`550\`,key:\`550\`,txt:\`Јавне набавке\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Јавненабавке.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`631\`,key:\`631\`,txt:\`Видео упутства\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Видеоупутства.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`954\`,key:\`954\`,txt:\`еУслуге\`,ca:\`3\`,url:\`http://www.apr.gov.rs/еУслуге.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`969\`,key:\`969\`,txt:\`еРегистрација оснивања једночланог ДОО\`,ca:\`3\`,url:\`http://www.apr.gov.rs/еУслуге/еРегистрацијаоснивањаједночланогДОО.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`960\`,key:\`960\`,txt:\`еРегистрација оснивања предузетника\`,ca:\`3\`,url:\`http://www.apr.gov.rs/еУслуге/еРегистрацијаоснивањапредузетника.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`961\`,key:\`961\`,txt:\`еДостављање финансијских извештаја\`,ca:\`3\`,url:\`http://www.apr.gov.rs/еУслуге/еДостављањефинансијскихизвештаја.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`962\`,key:\`962\`,txt:\`еГрађевинске дозволе\`,ca:\`3\`,url:\`http://www.apr.gov.rs/еУслуге/еГрађевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`314\`,key:\`314\`,txt:\`Регионални портал\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регионалнипортал.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]}]}`}" />
        <input name="__RequestVerificationToken" type="hidden" value="_p5tPaewXOnzcnlWTrjlU4OQYPrnx5Dt62sy8kCgM3DNUwoj_hOMz9Fo6HJ4dceWiG-_DQ2" />
    
<script type="text/javascript">dnn.setVar('dnn_dnnNAV_ctldnnNAV_p', '{rarrowimg:\'/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png\',sysimgpath:\'/images/\',carrowimg:\'/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png\',orient:\'1\',rmode:\'2\',css:\'mainmenu-menuitem EMSubMenuItemOff EMMainMenuFont\',postback:\'__doPostBack(\\\'dnn$dnnNAV$ctldnnNAV\\\',\\\'[NODEID]~|~Click\\\')\',mcss:\'mainmenu-submenu EMSubMenuItemBGOff EMMainMenuFont EMSubMenuOpacity\',easeDir:\'0\'}');dnn.controls.initMenu($get('dnn_dnnNAV_ctldnnNAV'));</script></form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=581)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=581)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=581)--><!--CDF(Css|/Portals/0/Skins/Flex2-V-Round-Stripe/skin.css?cdv=581)--><!--CDF(Css|/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.css?cdv=581)--><!--CDF(Css|/Portals/0/Containers/Flex2-Set8/container.css?cdv=581)--><!--CDF(Css|/Portals/0/portal.css?cdv=581)--><!--CDF(Css|/Resources/Search/SearchSkinObjectPreview.css?cdv=581)--><!--CDF(Javascript|/Resources/Search/SearchSkinObjectPreview.js?cdv=581)--><!--CDF(Javascript|/js/dnn.js?cdv=581)--><!--CDF(Javascript|/js/dnn.servicesframework.js?cdv=581)--><!--CDF(Javascript|/js/dnn.dom.positioning.js?cdv=581)--><!--CDF(Javascript|/js/dnn.xmlhttp.js?cdv=581)--><!--CDF(Javascript|/js/dnn.controls.js?cdv=581)--><!--CDF(Javascript|/js/dnn.controls.js?cdv=581)--><!--CDF(Javascript|/js/dnn.controls.dnnmenu.js?cdv=581)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=581)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=581)-->
    
</body>
</html>