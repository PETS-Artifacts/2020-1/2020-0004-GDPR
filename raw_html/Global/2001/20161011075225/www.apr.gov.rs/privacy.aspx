<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="sr-Cyrl-CS" lang="sr-Cyrl-CS" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2016, by DNN Corporation -->
<!--*********************************************-->
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	Агенција за привредне регистре
</title><meta id="MetaDescription" name="DESCRIPTION" content="Насловна страница" /><meta id="MetaKeywords" name="KEYWORDS" content="АПР, Агенција за привредне регистре, Предузетници, Привредна друштва, Лизинг, Заложно право, Залога, Јавна гласила, Удружења, Страна удружења, Туризам, Финансијски извештаји, Бонитет, Регистрација, Регистар,DotNetNuke,DNN" /><meta id="MetaGenerator" name="GENERATOR" content="DotNetNuke " /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=535" type="text/css" rel="stylesheet"/><link href="/Resources/Search/SearchSkinObjectPreview.css?cdv=535" type="text/css" rel="stylesheet"/><link href="/Portals/0/Skins/Flex2-V-Round-Stripe/skin.css?cdv=535" type="text/css" rel="stylesheet"/><link href="/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.css?cdv=535" type="text/css" rel="stylesheet"/><link href="/Portals/0/Containers/Flex2-Set8/container.css?cdv=535" type="text/css" rel="stylesheet"/><link href="/Portals/0/portal.css?cdv=535" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=535" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=535" type="text/javascript"></script>     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-6906308-2']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <link rel='SHORTCUT ICON' href='/Portals/0/favico/favicon.ico?ver=2011-09-19-065652-890' type='image/x-icon' /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="T8ZVomHmb06Ru1tgJ3Bh+C0ztY46WaoQm9IbBlRcHdWbCdlY3Lj2tURgnhkmYvpAz6+InkyFpAKGzYJjxqwp8Qhd+ffAtl85x32m/oCmQlO9QeoGKByjnArNnZbu0+EtAGSeFgu3AEtfaPT3kDBKgZwJBgmc7pudQJ5kPJWN2xY5GVSoBVF+oFXtMBYy1OgQefyWWxdvLX8z2NkwJcJFxltYY4wGcxC8AURS6/pPKEKA5WYhDpGh0rB6On/FKKcGvC9CLiFXpRXclxLgdor3x2DdP9EjbvRoD/oOMvRtKjbqZYWszRT80LPW+cHqALwTMEsxLKX8lPft6whOBE9twrzGv85J2+ZERmBxGmCpmVyKRK/8uELHHujdl9SuupEsQJ4ihbSWF6yMmqg5ChzRMCHrhKSZhkwDeYdPe6RWGFcLRj93reRvy+wPIUhAzTNu/xg0oDD/92IC4DV3F88F3GCyn9k2JVZTmdjsErvivHRN8yaU2eeJx8NvsBR1oMK8dVdDPW7uwLtuWuOSBGTKKj5L/3s+IxtKXY7prit/HeZSYoky0n6YYOouuzh3TclaNCPg97fZS4uSzCzUqYy9Fdw9N20Ga+7zOJXc7ufkcaOsLtxmscQ4o7K0TkcESnGwUo6YP27KnWOsmwNOre65xlgK1ZNx4OlfplDEr5Sms695HSmlDcOMvyFeWvRb1Hwa/lj70djpKUFqXmn+kpW1IKOXEUg7WDZ4CznIybYH95w98hsZxV/YSTeWH+UTRG9iubL5qEoBUWUOO+wjLGvxD/N7/jG0Jbo/003NlXpr9AYUnOSUGz0dzVo21QXqGIeW0kEkU7LxOggEg0/TA6TYOjOvwrAS9mqpWj7Ri0nPcQnzknSrd0mNYvXZn/c9yYZrDzUWMbmZc1nUJCN+LwlfCWEkixPBtDsNkVhWpycR8qknMjqybefzg4gopfQwGw23qr67v6lsFrRHR5qfgWeNm3gu/9jADohFHJjj+QUutHttNEwJFov8XbkZ027VC9CeW/RwMpyeM0itKmSlwxDbfRVZ7IjwZ1IfDq6vArsLu4y0D9khUSAukV/3AG4YuNf7f+MQvJYJzrks5RVSSaC3c8/KmNrvRLZCIdlBvFqnHPLOG1bXcFXiIehFwSo1JWHk/rC19LqkqwmHN4hx/Z9ntS4U0HadxZm6IpLZh445xoBgg7z+O5tbhTgU1GwwdBH+YNPq1b7pPNINYO1kXsUmCtuv3BQJWGX8bZCb3FDiGK3sB+NncU3OCYJAvIjpLuUHEz3ycNKk694Cx04FX33Cbwxs2KvH4grQqTgvWOW9Ys001NPNqQR7Ny51E7vxbULhZN3LVOLfxHb2wfjO+SaBLUI59Nl4LfII4mRQkzHsMrG5Oi/eD8Ibstc9C/Jd6cKd4DMhBvIFqhc9wce6FxiHBSibhsuEo9wKAYjNi633ZpaC5wRvExtM8jWryLb9oP7sfb45tvjoH3EiahcLOTW/X7FVNOWaiwi7hX0jrf1mwII1+poZXgRgV357AJlLnPTUUlkgkw5zb1/FMFZ65EqwmBVhH8FPsXBHaYI+YlLS7pSH7vmR/+Ya11gMtoC8BUrfms3x/NbRDim1HYHtbpkGjCO8AuPCe19iVPl+1LlPVbANFA9D5C2d5AYghckRUiC/ouatLSf/YkqmBxGCW8RAHWrvITMtGzIwipyk9DNMPgIlLX5rDTCSyvV5BI/uRTL0/14Nv8o2CoTimlBM3ICbgJYCWxwyHSfPd7CRoPDJqhdODiwN/Ki+d3FhkfpQcpSCJk0DAidBGhVe9ZHsIg8wDBaGVFEAl3DhY/v9jthn8T4ywRo8ZOjAA1oBTlIlkThfo2gBSaSFiL7PrnWk7EPstD0BN9mpg8wU29uobQGzm0RajK1orE4QaHTGztsBQAHy0h92q7MdP96bO7mVwJ8nje1863Z10Dg2nHh8LfG2nZWHQUNS/3W7ev0j0xY3TYW5CvssP5GQ42MatN3sdNY/FUriqTALk1CQy1fR7yh4kWIhIr/wjZZYhE58C8DMlY8qOC1rQCUnOnARzgu9zI3YYna4yDyNIaV+CH4LYzrdt9xXJ+DzQexorbr5xeoEil9Lzzj6tyauoqlJJfgMlDBys/2dixiyhsQrvzFG5KJpXMK7YFVzepwjLT18UdqmtC0hkjFbHGa6h70zkwF8+NKWMFXmyYGro4NWedtBFD6d9cpkQ0YwoATJB98Efg4G2gUmF3qz40Dq/S9alJaqcshVFqqMkJglOVEBFiTZibfLgo/gcmfDS76kriVdekZSjco35DqRDu2P7UCPUjVusAoF0aQJXrcWEcxOEIoDa81fBPyXFkZQXUVjV3tvN/iwU481fiSJ8ha99ZnwQYiB+5hzByoLX3eniSOCPbm4rEvn9DapOaQRV/pf6SD/Fo8NLD/JovO6QG1i0YTJcgNn14kEt055EMFYYeQqmlp65ctOvtD4sxsv3KUdsrlNWfyrs3un58WVsCW+y/ewvyGE/IMn9lC4VehB1VSZcWfKtTA8vees0qqeF5xr/oZkYKiAwckBskn9ZZxyx1uXy9cF/mvMmuo5+adKuEtW+uV5pPwRVWAN7gQXysjw1w9l4vD8PwQguer80Jee7HWfnK9JiZr8bTFAc66RO8a4WgzyFFly55uhq45mZKOkk4N1hDrLmvuq8al6pK1g2P0PEap5RvullRjGwHCCxSoyr4OcTfsUqiq3vi6+G2Hyyt037EIodmx1i+M5+Nr0z5ObCjbHUQFWYWRwu+0B9KVOoO2+XNLrG7mYO8YHALudgSNnE/yaGZ1LKqx/vhxkTbKPonLYj+HRzcnS6pR4X5bZGHjgkcHXFmGmiXtkkQSnw+4AEPRWg7ou5ehb2XGr0g0u9xTVZiCmVlqqzNlzSOKQPwf1sdDV/HirVpJ7tm+evuEEUouzU7DznlskzS6Fx5AMsCXSh8qaRWc0naahw0yXbgW8/4+TJ8YLn0CEAINZhmbDSslz9HeKTcn5VlVsjD0xfy/E91lKnUqWKXWEKvNE8nZiQFIevNNtBrt8OMxSCsNdp/jeUgsN/uKYadkerSUCTYiOjriMvHEn7MQrPGMctFGuNwBeWsGl4Fg6cJ6aZnSXlUVzIm2PqrmZSs7Yn6Wm/3tEfLP91Y8gMSK+Uyc68T75Lh/3RxQpW6ZR2YEAOZ8ix9lXPec4U/tiSwA4sGyHlXv6QEwZVJ5oYVHDF7dhS/5uhknhsfJGBP44u6bo+Tw9P7NZqhSsymom36emdSBJGrlTluXubspS4chrx3yZ/lF8id164h4U3iOpg29TAjW7OlSxbNLn98qwlK3TqdCVdsrOWg5HeDeP4YL3tudWuL6cFpl55/+IgNK4aYRkBQf7xUkCAwWPpjLCg/28UH/VgXcPqXh58rBxgYfw3+VnRwyc1OS7v+cNHe/H1eC2+kZPKc1V/sBw/raulvIZ9uY70qycZuG2V8z5+5vwTZUZ0BgzrgmuzwOffbxNQ6fl09hgi4l4voDCzQrq3lPLvpaMDDGjEivmkZX0n01hqf4OXPmgEsWpLvoT1Tp8A8nJex2MWnKV0Ro5vYJrOGE377GLOZ3lJP6KPlDM91yDHqteIA0NBu7cKKU4ahrswb7n9qkRvgT96ntcKpxvfZemV1HbGBFxoLmT6WSV7sKibDnbknL1E6TLjoMhKNO2gI3P5NcxHmtKp8fyNQfrV52XuUWIX6RBa7e1TFNwcj7xMTIwk3aLi7urQEkbPu5ODPp5ZbBdoNMIvp5FzSyhACfpJxGdPUujXkgw+X+Fs5P/Cka1Hubvbk7MxcIdRrylnyaauTdijpSUwKhkr+vMZ6NFFSoi24wrGWvDuXcZZ8jwS6L8hJ+khMkLHwNFpXE5tAh+h9u9ehLjJ53Fa/oyhw7kD+p6EG3YV4yxjhZU2uw/z07qJOK5uv1lNlYPs80HuQIZY+wAcUW0xGqK0AcflzlkaGiGfNJvhPxGGkPvzqWJnu6BQFY/gXeA4AlY62e8PCeA4kRCpS/6cNzO8I3psq72ZlO7WIsDxOGyskzHYjsHr48syyvujB2zGRFXAXgAjw5GQbbPcRTHbYF9t+enFvWLl4OuRSPtJj4HnYyjEmJrroT++WQaMB7nSzGX10CUfJ+8kvFGqKzdAxWekXDroJog5sfstWRcm3PqwimH/pvxhd4vvzk+m4mIINvzx4KdLhyj6J/uZ0vrzRLOJa9hWMZaXAgF3q8zhOygUbQxMcfRy+GSPiJurjZ9/dbhr6KalBNt5nNs1tisgWe+2VmdRTYryhjTp4LEFG0tlQMMIqFV3MQq3uAit1OrytiaeLxylvj5XrAr0roxk34I5/gckBxHfVe99+RW61Eu9HTe7Q317wqQvcI7D/w5gL+HoFOS3HCBnv5bIRJL56HtUpVczaF5/74xrGmN653LALndb8BErsbZE51g9mcJgX29IbdXNn6kxR+QjLSfX/3weTCbhobmtctzALOS4ECpfrKS6o65huXYgi7WBUmlRPoXXHAWfPsAGoPY4oyxejDiWBtnHXgdZ4JJe00/EereJ5Xtjd3i+5oHMpvxMExH2Oj4SPlyFqp4/wN0UMd7C+H40kjOgNC75zOiEn0ELCWdTCj1qcD0rwRsC2xJZ35vdr2J1itWOThUK7u+LlSEElNwbseyveg73t5e+/4XMZESQvrAtZl4QWKkVGRkWfAusDEyZksLlGsDkPQ5UqCIv0iz2ZgAlxCKOkA7hKMzN0kqhJexOrjPsMz2Ecr1KGuFlQaEwXikdaLIqszCQo+8R6JZHIKN+DMTe4WOJiGpsFwGtWIBFqPOsw03CqiSzMn+pSwvjHiOUZfZ+SzgQpe84bGzJ+5aO2tpBtvei67QhVIPb2igbLAoq42MeMmaJ7tcySNvDSmUkGvAsq0Qm5lGZMNMNgialI+vhw94sa7DxFNgPfzYjIbkzS1Dr7Xnaaiuq1n7Jx8M9jhlLr1fGa1z90MzapsWi18PgUQuicZo0LJYDxlZQJeX19RoKdr8CdSQJeO7pH88U0dPn18Oe12lSqSNNflsUHlwgESvFULjo2ylVCfuSNV7GbU44nhVLv7HZLtU3uKJvLEF11oac89nxv/Z3X4ISd31FdprrRmBgVX/BDAAFUxLyh7CTbdYvhrH/Cdf6Fw5pbJ4P+aICKIaXixnYjDPo9/E+s/JTw8hCP/uSEib7UisbIknYodARp+uJUHmo03sWIRR0+AYbcU/op5rFwxjLodxJhzfBAWEcNpH8DQnuXj7OR3RqzXxreX3PgCQ+Oku+XfQIb3euYSJCULw4Q6AnNzySwOlUfCAMzkQFCVVMXey2Dw2TQMRt4rD2PWeKKbz+9vjqZPXbSqCv6OYMRkG0lKSCxXruDsiX6APxiyD2dhzDYxFfvK9jfrg9EE3mmcIndsBtBfbuhMwic+U1Fa1lcayuyMQ1USR1D4/hNEgtWMCfMDDY8vFU52F5HTehWU/ZHkqAUJXCuypiKS6u13YDVht6qjaZb7qVlsAQA4UGnHq+VwcG6S//Jwr2ILq646agQ9G9YqEHDFiN8mIiXPz7SpYpZfWzyH2ZZNXRAyZaahR4cXXSoJc40inontcivC95+wk07jnXaF2LRWXCyUikTfOtBG8VVtZy41nfY7MeDF95yE74Nvhe4vqO8vA81aI9Yi2msKB+oKWNbHZoh7attrUm2gjYlMnrZc6p1UfCwUAcVFBdzrfiTYwfnZPBL5zgrWHxNvUqsS1Is9pZujzD9PeL8GO1JCXmCT1bqFi9B1WYV1qPt0e32S/qwBd9fwzhUTIrNhCtEw+zPVixo97TmOeM/0nCR44eGIfDLpJjCR2sLgc6JnByRyA1Xuq/AWKp8cgf/mfk7JL9Qs6K8iTrrGQP7lrYNgqwygYtv9OtV2725LD1bgo6Jr99Al1TwCFWrr6WKr2uw7WU0/QvAPNMNo+kjDoY2Muz62MaA3q+KlwxUvdLdXg6fZJxf5LGrBOsWoH1HnJv9rHw0tUyjNSQYVh4gNQLSxAF7VvRsANzvg54n1Pi1w7Z36Ly46fwyxRIIYAwjc3uoq0+e7mYdF1MVxDaAJPIgZnW9Or47n9hp788GiYzXTpdYAg7j1j7h0TaiVdcanfsFDrpPQq4nlbAZE6/sxQSSswlaLAx8w4la2E3igBg3lJSR4Yvpb0DuHcc1XIx0JfEmFwf4RMHcLsd5DPOIEWy5opZuZt+j9NQpZm1DS2TW0u3Pjt8y4gUMt/OYDrlbm5DSLgDKdFxBjPp0bG1hV2s20BO1Jqo8UIaz1bMOVO2ahvq2RCJh/lUZEGos0wDEcw5jm420KpQgvBGiv0D4MTnlwPjS3PYggjjEk4eCYFM+9HuLxMxFw64zMpYvkERcl7spCUtyYuVkIhhXRpMO1yP6XXt0uYkAfN5B3yo3q8HPOWRqzBQVMRMDDanePj9H0gZMWoG1qUGdaLmD2zKlqO8dsIGeYe057TsqbS8jmd0HshoH0Jw5pMmcfl0Pr6uZUmz4o+J5RXmd+wLzyi7MG8aFz7V6qZXOcW+vsHCevordq1z2rL2m4g0lXjxjpguqeTGWAs11OLMJWDeLzjFNOlTR0cRdAMdcP1sJzpKE7w47/cdzfqC8xfbQB2zc9tLacXc8O7D3tLNFtARNLDTujamsOBgodedPvw45cLY2judFdWliWgf/ixHAewe8HUz5V7ptdaatAPHSuVcXAw31fapBB4TkzM+NGMhqikEoKwwIG9h4DyZUakZjXYY1TUz/auPEzuhc0TV4Lz/7ludfRn/IFNCHppyBShXj3KxEE6S4+mjB8QPpc8X+i4gw5d3xTN8o+MvPdI9otxU/cBPLwRyShSft0098IqYtzr8ewz09S2MDQ34aYJ9twG6s7+7017zO7mwZYCTWKT7kgd6UYcX++QL5Ba50MkmYPRsf8nKAs14UtAPheQsQ0M9qlRH23jQJ/X8rysigFxEMekOAniDrcOs+EsP9QuguCxlJRXMXo2YGSknbj21sf3rsHimLuQXyrfdhz4KYwYcRIJjs4xbCG+M/Vn9+J70pP5yKeGqDuFQNg+qyJGKVKQ7I1dpreO4iEZJbyCiZ3qLjhMOCQl3NWScKD4fldpIdRTP+u9LHkSRj9lT+J0zp0pAxplDVmjgx2PDyheYfZhHV5DtqrmR8btrVwnHUsKTq9J2/S3SU5yZJeviKz+Flw/8YXK/I8D6Vrm5hW8wBHJaaLwPA1lhu26OmuaL4FPFBk0ncw6VF1e6ZdgfnfSBnq2CzIurSSfne7Iu+2ZLAqKRakTR2IPFnEqIpvY0L5iujemzskYaXsqLK3wlsuYlIhOhc/lko6dfAcJGsEU698RqI3lVeC2/dJDpBsVyNlfxDZA1snnGt29iFryg5udYQ1WzGwrLen4A5g0qkJ4tYm3WhoSwxzEhWl+3Ze3FO3QWHWFOL4+5VP4R27kYsKyilvB6VZaEmMdw3Ee/NmOksi1YfIXa0Yfp/PjLq2lk7HqyiPrNTH3ynPr5rAfc6TxO6tHP9JqDV9vcjIGnB9bkI2h3HxzgGTW3Vzpo900oQxm5wIH5uSQCviwEuE8Ee3P5bfveML+sIqXleF2Bh4AJLkP0n53KlAEQVxW999kpw8mFrfZmEsDWvRiXRQhEiFVeH/2U7OPqKLyHk0OILvQBVLvWrLxhh9RpsTp7QteZW0Uara4OknQcJsKbiAsiz5PAYyXzTVelgczkCoDZ9cF4C3Q6fRf2K6F12cW1Lb5LxDX4ug90C3Xk1hVoFqvs3IqY+zGlNqDNmurcBVnwETPid65cjXn6Zua6cLTDHga2lYFX7PIX7ds09K9KheNP4AJhPFM1n/Zbxd/f7lGMPpGZNuPxAeMNuQ3SO4kpUOt/m47c4tjpoq9MuYuBQnByAfjxLj8rT1EUANkhbmcgR8FYmjqTmjszKFA/FTaKH3lBBrF5hpZ0if44wnc2fZzA2BYaC9BNDcONCQn64oNzjtlKcgcuBByyjlH2mUGynF+P59NSl6D6u/vIr/LWF9dt/zhwvpO7ijgroMrc1HDq83IUCdZdwZ87ynne5vQAkvZNVuUmlA4yaFp8Muqv8D/+1rKSCPdBBBFG4bjpUZN84ZRyUuekVePmLlR/jopnZ6hbISzeEB1Ch2We/uSgqAuYhA3Evm79crEZIxi3zd5UV/UmWo8wv4+bDdfMiS1lfN2yrMy2/G6gU9uMZa+F2znYsxNp/BD6ds9SpioCUOlNi+PBUvTBRKzdnMNqH5lbaasbdXEoWC2moFG9FXJPvjg+XO2JYKnffTy05nu9xjE6LZp/oQjGC8vuDGkT4b2sPtT/xwZpUNMnWY6lggewjzlM6d2QYyCmAxSniM1hVnxaABtUKYeeG9ksZcWGO1UDie1VWe69lD6rH7UXUyL9b1lmvrxfTKM/9ZhrM557jKMCTPIHoh2M9dH091mk35p3cHXrgSHzCW+8VvWhKT8rzhxONxgT/cFt5pJ19+aIg9KPyI7EcFEKDACq3nCTaoboEXLKexq2K4Hz+MnHOEB7mhXQflGwtwlL3zFuwxnCH3UWpFYpLIkxaZ6nkZrfyrosAcVCdP/0UaOHu8IEdRDHj6aMYi2wRuhVBvkR8CHTTDuV7zORYnlERTz6NkKR4z3Mmdbgx+SBoqLbwcSnuftnMrAPAdN3x96JBfP41AvqdVxaG0QqlRkqpQeCSOqSCU+MIBB+CCYUG7RJ6yT/so9rcjn45bb0o8toUTByMtAFJJtinZbCnFQN64bZ9OswZbsiF21LzBBKo5g/FWx+KLzf+4ZeB7J0pmMiJc7JQfWKlzi7dFwFzxUfF5lR8bfrjZwMnihBB5j4FoJEAEfc7MGCxB8ezSJYkGKnXYJyrnlBkSL1SQtB9Fsna+y8nim127g7jxdYwuQbE4cOkP3Isof18dNfrV6+TzLPFUnEEe/yd2ZCHOMgYXMv1WgDPJejzvWNd54ie7VLgiRcL41px/BA8Jh41nvby+u5ccFXptYIJqj4RDnhqe5XBYnV90ESvkrY+9bLInHP9sA1UEm15AFmVnqVnpl3EP9Kl2PS4iwH/cGcDDSP0rCxtB/NiyhA2cTxPDzp84p+M304O7gK+9wL8DppQfF/rfDBp5OzHSRMagBetBQNH/rLH63wukTM4h1+sx8U6yzCrW+PxTq8V8TpUIirLGBGc83CcoFw2QKOiORm+v2EvvggXbwXDCCaLWcE82QkBhJIKt+cOsd5ScvO7RanGx7BYq6gk9zLgs4d9GUOmu+wd46NWkq8gMvJubUnxJD83d/hqDXBWMa/Ir97nhjrdsmHUkFzIOeMS9lHqjyKvrNd+iRragEcyupk2w4OwjI5euDZM3+8xpKMYgxiepquSpz86dHDz9QXdpPGr2fpQ5CM6Db7+ri0tk/cQrI8mZ96/0tI6LW/knBmbIONpIPaFWphbjNUmH+gLkcLrR9/seKR68HOBpJZN84hSaY5cRvfTyKaaau4xmp/yJadDegQ2dJA7PdsMRFlAg0iU7qe7vw1ypOIpr9bpUiYJmbQWGcQrDwEzfXE7yvtzQ+najPpzsjYbOq0mb4DrAKH083IHESkcR5uns6SB0OwU2BsyJ61FTToin3OqlK26TSXvKDgft/rPsS2IC00kwZXtWqWH0tWB2hIIJEasLEnjaADyZr/U0lhHk4+hDlNBYmblWmUEGmH2ZhEE0uxTcCNY9Tz4PcVxQB0Dmv2DN/M3HwxkjOWmJ3T58yRgmIBPlcbCabQjsF0Qp/TcKy8G5GPe2cOSK48sDsszuN5TtEmYiqfqkOmc6spA4oeZL2F+/mvgKP8R3OgRiKQBe9xy8gcbxdn/jM3rtdsFHsWd4JZ/d3aFsxLMiyne5lW76QsH7nlNKhPmyYRPXIuhr+i035/L4/o0JNlm6dkGZyC44E8/dI8DvZaqsFBCbkufm2q8N3HjVEBS1tw6xOX3Qyrh3Q8pV7A1xXEY0nmFnOppWqfWySvIt4VX2dN9Uy9VooQAZUVe+Q8rFkTr7zSyFROkZtOjWFvX29RBgGA4IDia+7kr5TjS3kACTPHJMwv5yGLuNuV1Th1Ji1kLtPLzYf40B0wuPdfge273um0mO/BnxzJ/7yFJ2oR1/aFEsQ9oMhrFKm/wTUveScy5ij55inKlW4cie6lCcDtBVtGPNIdV8gk9N10IA17JETyiYKYirWXmW7dxT4P14UCPQ5JyDutivQOnVRY+YnhK/fLTswif0pNxE7Bc0ZZX6mTumWlBRbOAZoMmCDQsJtmwfxcZAS+t9oaTJ32uZe6BBCHyn9iB/+HZJlqdH34/S5JYXHqMhkbu5xK+CIhoWXNQs0LTSW2CW/4n0yNFd2oB6lbHehlF+7ffdJbD3vaPAGGmsJS+rdIznVq2wOAHVBscjDxDZcVW9OzbiB5NxZBvHs4qRwWl6MrkslKEiqvCfZ+rf5lznbIdwuEC+Q3eaGZLgfqjOIDbVjs+JMoHiCpky/rAunAt3aCcA2inzAOkijSGt0Jmz0jExRR6qk9RvQrBn4nstACDw+Y6gJYZD+Ifxb5ozEvYBuVHzyjUmhrHTrCBBlwCPSU0/dXShWWmDS9PiwzkNgACFOMU6BGWFXcGqvSGjOrWs5SB8hOaYCL91qKboXzputDXEVO7VCKGcA0Rc63dap5LM7YnJ3fiOjrtgfym+6pq5+Yho+g42miIeiHVDebtlIUJz7021AjuGQFsEmJ/ezPc8moMmY5QgiTKkBzD0HgcHKbhh4DiB5VRiKNB47DNGfubqhQPFCwOthx5NH8diz1AzBpIYFHc080JEuZQ7YiQlLVFTWqERlRsuiSei33PUeAkeDW5j2Oln2aNVT3ArYAsSOfZHkp+hWkH9OR3o+UQ8x99LgHMQS8kgGes1d4JRhoAdJe4h1IEmwizNX1srIyXNo/0PWWauUkNds7ZNZbprdJZ8uqzVK2syaw6y5zd52m1e/23baoyStZwQ5OzAWUj6Zq+3ocHA5iqfjQWcWP5juCdnIPpTUZgX4B7rCe2QAEzgB7LgDIN7zKwu08li0iX1rhriK3rKLpCZxRre4kNsL2dBN7VJnr9VAHGH7cZ9NRoWU/ZJY2U5Cvv3VLO7OplWUoTojVBHcVFa4I100ZFHTiVLy0pB+O+KBcjW4YB2+y5Ai0p2wi/UBs/taNH5goTZ5JkJrXyWr+KYm3zmI/3KpSrm+zmSeNQFDpmo8J8+hkflUXNX+9jAJ26B5f9JDstocOkTYDJpG7/nLm6aQNmu1brDecgONNTuIpJofS/AQ7i78eaeM0FSjvRjJr5dpuibThrVWFGp/m7fCq+TPnCy6KR4rP/sK7DBaRFs9xms9hkGw4WF6LOsHFiZaOIqooHppveBrT5rzC6ICdMpMdjzybj1+bojpwIuYBsKzjiZBwWBwRglf4JNJjXQPQMG/RHuNsI05A3kX9sh4G5XxNKWqmTaPEc8h8kaFkW2XvqmJzabip6T3Yl5EmYnWJYO5DzHGD4tbmWPXUUuwS+FjrJ2iEb/GN3K7t3FwRuxkNKQ9c55QXlWUqXfSMzGrBF/xeDIyaJlgj97KtHWUnz8EDAZ7zmgYlxdbJ9SCsn4La5krTsxk6vYcIFj0RCQqpLbPSGPXWh2ypT7bcM+hFbgPkazPqxIbI11bXVCiyQQdFYEXkuU6GH73gwOdxM2JkgTP2k9/Rwhn949SsPeXY27c/TZ/VA4DTSzblL7hJ/mWRiJ2K8HK5uulb/OlNRYgjSrr2uG3fDRlXzyD7+W4RktXKtPP33t6l0Wr1mgtcDlY9TH4KAliC2vQVgvIwB49XGLE3c1VzaKyg4T3ra3f7BKfSFjNPbDnTWe8bfwUHY8Ur6IHj7OouI4MWZRBrAHx8dx5lNv/4sx0kt1m7WFD9vojGqCtuUyEAR5XN4xByR8vv45R/dN/9Cr0v7OJaSAML40itrTaBaY8I6b96xG04hjojbMN10/SgKdgSEeF+L0MgwUg9H+ZbD+uyVB05W7JS4OFhQNwTIDdW/fG4hlRwJEJroxBoghLd+A+5lC2vz7BrbYiospGu/OIzLrbzk1SxUvOWr7u9g6dTrDFCnffQw==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV2aNbE0v3xAhmIgl78LJs2xV0TBqp0uo6vQSD43MG7X87rwPP41&amp;t=635875545181218729" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var __cultureInfo = {"name":"sr-Cyrl-CS","numberFormat":{"CurrencyDecimalDigits":2,"CurrencyDecimalSeparator":",","IsReadOnly":false,"CurrencyGroupSizes":[3],"NumberGroupSizes":[3],"PercentGroupSizes":[3],"CurrencyGroupSeparator":".","CurrencySymbol":"дин.","NaNSymbol":"није број","CurrencyNegativePattern":8,"NumberNegativePattern":1,"PercentPositivePattern":1,"PercentNegativePattern":1,"NegativeInfinitySymbol":"-бесконачност","NegativeSign":"-","NumberDecimalDigits":2,"NumberDecimalSeparator":",","NumberGroupSeparator":".","CurrencyPositivePattern":3,"PositiveInfinitySymbol":"+бесконачност","PositiveSign":"+","PercentDecimalDigits":2,"PercentDecimalSeparator":",","PercentGroupSeparator":".","PercentSymbol":"%","PerMilleSymbol":"‰","NativeDigits":["0","1","2","3","4","5","6","7","8","9"],"DigitSubstitution":1},"dateTimeFormat":{"AMDesignator":"","Calendar":{"MinSupportedDateTime":"\/Date(-62135596800000)\/","MaxSupportedDateTime":"\/Date(253402297199999)\/","AlgorithmType":1,"CalendarType":1,"Eras":[1],"TwoDigitYearMax":2029,"IsReadOnly":false},"DateSeparator":".","FirstDayOfWeek":1,"CalendarWeekRule":0,"FullDateTimePattern":"d. MMMM yyyy. H:mm:ss","LongDatePattern":"d. MMMM yyyy.","LongTimePattern":"H:mm:ss","MonthDayPattern":"d. MMMM","PMDesignator":"","RFC1123Pattern":"ddd, dd MMM yyyy HH\u0027:\u0027mm\u0027:\u0027ss \u0027GMT\u0027","ShortDatePattern":"d.M.yyyy.","ShortTimePattern":"H:mm","SortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss","TimeSeparator":":","UniversalSortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd HH\u0027:\u0027mm\u0027:\u0027ss\u0027Z\u0027","YearMonthPattern":"MMMM yyyy.","AbbreviatedDayNames":["нед.","пон.","ут.","ср.","чет.","пет.","суб."],"ShortestDayNames":["не","по","ут","ср","че","пе","су"],"DayNames":["недеља","понедељак","уторак","среда","четвртак","петак","субота"],"AbbreviatedMonthNames":["јан.","феб.","март","апр.","мај","јун","јул","авг.","септ.","окт.","нов.","дец.",""],"MonthNames":["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар",""],"IsReadOnly":false,"NativeCalendarName":"грегоријански календар","AbbreviatedMonthGenitiveNames":["јан.","феб.","март","апр.","мај","јун","јул","авг.","септ.","окт.","нов.","дец.",""],"MonthGenitiveNames":["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар",""]},"eras":[1,"н.е.",null,0]};//]]>
</script>

<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3asr-Cyrl-CS%3a7dd8b7c5-dd18-48e6-97c2-5a5a060b2752%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<script src="/js/dnn.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
dnn.controls.submitComp.onsubmit();
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="ewVw6kcclFzT/WegJrQkPdtJNbinYePiKMoLLU+dK2qReKiDzqosmVUa8Sl7CVPm7l0yu36jjnScpuZh192OVKu/OZcec2twSgRFkoO//FTHFuqW6JK/zvPest1sjjoiBl2h5pOAR0kbeck9dYmOKH2N6k0=" />
</div><script src="/js/dnn.js?cdv=535" type="text/javascript"></script><script src="/js/dnn.xmlhttp.js?cdv=535" type="text/javascript"></script><script src="/js/dnn.controls.js?cdv=535" type="text/javascript"></script><script src="/js/dnn.modalpopup.js?cdv=535" type="text/javascript"></script><script src="/js/dnncore.js?cdv=535" type="text/javascript"></script><script src="/Resources/Search/SearchSkinObjectPreview.js?cdv=535" type="text/javascript"></script><script src="/js/dnn.servicesframework.js?cdv=535" type="text/javascript"></script><script src="/js/dnn.dom.positioning.js?cdv=535" type="text/javascript"></script><script src="/js/dnn.controls.dnnmenu.js?cdv=535" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        
<script type="text/javascript" src="/Portals/0/Skins/Flex2-V-Round-Stripe/swfobject.js"></script>
<script type="text/javascript">
var params = { wmode: "transparent", FlashVars: "XMLpath=/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.images.xml" };
swfobject.embedSWF("/Portals/0/Skins/Flex2-V-Round-Stripe/flash/flex-900x112.swf", "FlashBannerContainer", "900", "112", "9.0.0", false, false, params);
</script>

<script type="text/javascript" src='/Portals/0/Skins/Flex2-V-Round-Stripe/drnuke-height.js'></script>
<!--[if lte ie 6]>
<script type="text/javascript">
if (typeof blankImg == 'undefined') var blankImg = '/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif';
</script>
<style type="text/css">
.trans-png, #Body, .flex-container-visibility a img { behavior: url(/Portals/0/Skins/Flex2-V-Round-Stripe/drnuke-png.htc) }
</style>
<![endif]-->
<!--[if gte IE 5]>
<link rel="stylesheet" type="text/css" media="all" href="/Portals/0/Skins/Flex2-V-Round-Stripe/css/ie.css">
<![endif]-->  
<!--[if ie 6]>
<link rel="stylesheet" type="text/css" media="all" href="/Portals/0/Skins/Flex2-V-Round-Stripe/css/ie6.css">
<![endif]-->

<div id="OuterContainer" class="EMWidth">   

<table class="EMSkinTable fullwidth" border="0" cellspacing="0" cellpadding="0">
<tr id="EMOffset1"><td class="EMSkinTL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td style="vertical-align:bottom;">
	<table class="fullwidth" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="EMSkinTL2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    <td class="EMSkinT trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
	<td class="EMSkinTR2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    </tr>
    </table>
</td>
<td class="EMSkinTR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td></tr>
<tr>
<td class="EMSkinL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td class="EMSkinM">
	<div id="ContentHeightContainer">
	<div id="InnerContainer">
    	<div style="float:left;"> 
            <div id="LogoContainer" class="EMLogoContainer">
                <a id="dnn_dnnLOGO_hypLogo" title="Agencija za privredne registre (APR)" href="http://apr.gov.rs/%d0%9d%d0%b0%d1%81%d0%bb%d0%be%d0%b2%d0%bd%d0%b0.aspx"></a>
            </div>
        </div>
        <div style="float:right;">
            <div id="LogoRightContainer">
                <div id="DateContainer" class="EMDateContainer">
                <span id="dnn_dnnCURRENTDATE_lblDate" class="DateToken EMFontFamily">уторак 11. октобар  2016.</span>

                </div>
                
                <div id="LanguageContainer" class="EMLanguageContainer">
                <div class="language-object" >


</div>
                </div>            
            </div>
        </div>
        <div class="clear"></div>
        
        <div id="MenuBottomContainer">
        <table cellpadding="0" cellspacing="0" border="0" class="fullwidth">
        <tr><td id="MenuBarL" class="EMBaseColour6"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-bl.png" alt="" class="trans-png" /></td><td id="MenuBarM" class="EMBaseColour6"></td><td id="MenuBarR" class="EMBaseColour6"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-br.png" alt="" class="trans-png" /></td></tr>
        </table>
        </div>
          
        <div id="BannerOuterContainer">
            <div id="FlashBannerContainer">
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
            </div> 
        </div>
          
        <div id="UnderBannerContainer">
        <table cellpadding="0" cellspacing="0" class="fullwidth">
        <tr>
        <td><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/login-l.gif" width="9" height="31" alt="" /></td>
        <td class="fullwidth">
            <div id="BreadcrumbContainer" class="EMBreadcrumbContainer">
                <span class="EMBaseColour4 BreadcrumbSpan"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/breadcrumb.png" alt="" class="trans-png" /></span><span id="dnn_dnnBREADCRUMB_lblBreadCrumb" itemprop="breadcrumb" itemscope="" itemtype="https://schema.org/breadcrumb"><span itemscope itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="http://apr.gov.rs/Насловна.aspx" class="BreadcrumbToken EMFontFamily" itemprop="item"><span itemprop="name">Насловна</span></a><meta itemprop="position" content="1" /></span></span></span>
            </div>
            <div id="LoginContainer">
                <table border="0" cellpadding="0" cellspacing="0">
                <tr><td class="EMBaseColour5 trans-png"><div></div></td></tr>
                </table> 
            </div>
            <div id="UserContainer">
<a href="http://apr.gov.rs/eng/Home.aspx"><strong>English</strong></a>
                <table border="0" cellpadding="0" cellspacing="0">
		
                <tr><td ></td></tr>
                </table> 
            </div>
            
		</td>
        <td><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/login-r.gif" width="9" height="31" alt="" /></td>
        </tr>
        </table>
        </div>

		<div id="ContentContainer">
        <table border="0" cellpadding="0" cellspacing="0" class="fullwidth">
        <tr>
        <td id="MenuContainerCell">
        
            <div id="MenuContainer"><div>
	<ul class="mainmenu-menuitem EMSubMenuItemOff EMMainMenuFont" id="dnn_dnnNAV_ctldnnNAV">
		<li id="dnn_dnnNAV_ctldnnNAVctr39"><a href="http://apr.gov.rs/%d0%9d%d0%b0%d1%81%d0%bb%d0%be%d0%b2%d0%bd%d0%b0.aspx"><span>Насловна</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr113"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8.aspx"><span>О Агенцији</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub113">
			<li id="dnn_dnnNAV_ctldnnNAVctr285"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a0%d0%b5%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d0%b5%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Референце Агенције</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr242"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%9e%d1%81%d0%bd%d0%b8%d0%b2%d0%b0%d1%9a%d0%b5.aspx"><span>Оснивање</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr243"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%9e%d1%80%d0%b3%d0%b0%d0%bd%d0%b8%d0%b7%d0%b0%d1%86%d0%b8%d1%98%d0%b0.aspx"><span>Организација</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr244"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a3%d0%bf%d1%80%d0%b0%d0%b2%d0%bd%d0%b8%d0%be%d0%b4%d0%b1%d0%be%d1%80.aspx"><span>Управни одбор</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr245"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a1%d0%b2%d0%b8%d1%80%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8%d0%bd%d0%b0%d1%98%d0%b5%d0%b4%d0%bd%d0%be%d0%bc%d0%bc%d0%b5%d1%81%d1%82%d1%83.aspx"><span>Сви регистри на једном месту</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr353"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%98%d0%bd%d1%82%d0%b5%d1%80%d0%bd%d0%b0%d0%b4%d0%be%d0%ba%d1%83%d0%bc%d0%b5%d0%bd%d1%82%d0%b0.aspx"><span>Интерна документа</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr139"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8.aspx"><span>Регистри</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub139">
			<li id="dnn_dnnNAV_ctldnnNAVctr59"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b0%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b0.aspx"><span>Привредна друштва</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr60"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b5%d0%b4%d1%83%d0%b7%d0%b5%d1%82%d0%bd%d0%b8%d1%86%d0%b8.aspx"><span>Предузетници</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr216"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d0%b8%d0%b7%d0%b2%d0%b5%d1%88%d1%82%d0%b0%d1%98%d0%b8.aspx"><span>Финансијски извештаји</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr61"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d0%bb%d0%b8%d0%b7%d0%b8%d0%bd%d0%b3.aspx"><span>Финансијски лизинг</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr62"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%97%d0%b0%d0%bb%d0%be%d0%b6%d0%bd%d0%be%d0%bf%d1%80%d0%b0%d0%b2%d0%be.aspx"><span>Заложно право</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr116"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9c%d0%b5%d0%b4%d0%b8%d1%98%d0%b8.aspx"><span>Медији</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr117"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a3%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9a%d0%b0.aspx"><span>Удружења</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr118"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%82%d1%80%d0%b0%d0%bd%d0%b0%d1%83%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9a%d0%b0.aspx"><span>Страна удружења</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr229"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a2%d1%83%d1%80%d0%b8%d0%b7%d0%b0%d0%bc.aspx"><span>Туризам</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr258"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%82%d0%b5%d1%87%d0%b0%d1%98%d0%bd%d0%b5%d0%bc%d0%b0%d1%81%d0%b5.aspx"><span>Стечајне масе</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr260"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9c%d0%b5%d1%80%d0%b5%d0%b8%d0%bf%d0%be%d0%b4%d1%81%d1%82%d0%b8%d1%86%d0%b0%d1%98%d0%b8%d1%80%d0%b5%d0%b3%d0%b8%d0%be%d0%bd%d0%b0%d0%bb%d0%bd%d0%be%d0%b3%d1%80%d0%b0%d0%b7%d0%b2%d0%be%d1%98%d0%b0.aspx"><span>Мере и подстицаји регионалног развоја</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr262"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b5%d0%ba%d0%be%d0%bc%d0%be%d1%80%d0%b5%d0%b8%d0%bf%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bd%d0%b8%d1%88%d1%82%d0%b2%d0%b0%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%85%d0%bf%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b8%d1%85%d0%ba%d0%be%d0%bc%d0%be%d1%80%d0%b0.aspx"><span>Привредне коморе и представништва страних привредних комора</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr341"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%97%d0%b0%d0%b4%d1%83%d0%b6%d0%b1%d0%b8%d0%bd%d0%b5%d0%b8%d1%84%d0%be%d0%bd%d0%b4%d0%b0%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Задужбине и фондације</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr343"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bd%d0%b8%d1%88%d1%82%d0%b2%d0%b0%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%85%d0%b7%d0%b0%d0%b4%d1%83%d0%b6%d0%b1%d0%b8%d0%bd%d0%b0%d0%b8%d1%84%d0%be%d0%bd%d0%b4%d0%b0%d1%86%d0%b8%d1%98%d0%b0.aspx"><span>Представништва страних задужбина и фондација</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr366"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d0%bf%d0%be%d1%80%d1%82%d1%81%d0%ba%d0%b0%d1%83%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9aa,%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b0%d0%b8%d1%81%d0%b0%d0%b2%d0%b5%d0%b7%d0%b8.aspx"><span>Спортска удружењa, друштва и савези</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr377"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%83%d0%b4%d1%81%d0%ba%d0%b5%d0%b7%d0%b0%d0%b1%d1%80%d0%b0%d0%bd%d0%b5.aspx"><span>Судске забране</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr599"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b0%d0%ba%d1%82%d0%be%d1%80%d0%b8%d0%bd%d0%b3.aspx"><span>Факторинг</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr591"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d0%be%d0%bd%d1%83%d1%92%d0%b0%d1%87%d0%b8.aspx"><span>Понуђачи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr779"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a3%d0%b3%d0%be%d0%b2%d0%be%d1%80%d0%b8%d0%be%d1%84%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%80%d0%b0%d1%9a%d1%83%d0%bf%d0%be%d1%99%d0%be%d0%bf%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b5%d0%bf%d1%80%d0%be%d0%b8%d0%b7%d0%b2%d0%be%d0%b4%d1%9a%d0%b5.aspx"><span>Уговори о финансирању пољопривредне производње</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr828"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%93%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>Грађевинске дозволе</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr885"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%bc%d0%b5%d0%bd%d0%b0%d0%be%d0%b3%d1%80%d0%b0%d0%bd%d0%b8%d1%87%d0%b5%d1%9a%d0%b0%d0%bf%d1%80%d0%b0%d0%b2%d0%b0%d0%bb%d0%b8%d1%86%d0%b0.aspx"><span>Привремена ограничења права лица</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr64"><a href="http://apr.gov.rs/%d0%9f%d1%80%d0%be%d0%bf%d0%b8%d1%81%d0%b8.aspx"><span>Прописи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr132"><a href="http://apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5.aspx"><span>Услуге</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub132">
			<li id="dnn_dnnNAV_ctldnnNAVctr427"><a href="http://apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d0%bf%d0%be%d0%b4%d0%b0%d1%82%d0%b0%d0%ba%d0%b0%d0%b1%d0%b5%d0%b7%d0%bd%d0%b0%d0%ba%d0%bd%d0%b0%d0%b4%d0%b5.aspx"><span>Достављање података без накнаде</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr509"><a href="http://apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d0%bf%d0%be%d0%b4%d0%b0%d1%82%d0%b0%d0%ba%d0%b0%d1%83%d0%b7%d0%bd%d0%b0%d0%ba%d0%bd%d0%b0%d0%b4%d1%83.aspx"><span>Достављање података уз накнаду</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr133"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83.aspx"><span>Односи с јавношћу</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub133">
			<li id="dnn_dnnNAV_ctldnnNAVctr68"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%92%d0%b5%d1%81%d1%82%d0%b8.aspx"><span>Вести</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr310"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9f%d1%80%d0%b5%d1%81%d1%81%d0%bb%d1%83%d0%b6%d0%b1%d0%b0.aspx"><span>Прес служба</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr66"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9c%d0%b5%d1%92%d1%83%d0%bd%d0%b0%d1%80%d0%be%d0%b4%d0%bd%d0%b8%d0%be%d0%b4%d0%bd%d0%be%d1%81%d0%b8.aspx"><span>Међународни односи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr67"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9a%d0%be%d0%bd%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Конференције</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr328"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%92%d0%b8%d0%b4%d0%b5%d0%be%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b8%d1%98%d0%b0.aspx"><span>Видео галерија</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr140"><a href="http://apr.gov.rs/%d0%90%d1%80%d1%85%d0%b8%d0%b2%d0%b0.aspx"><span>Архива</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr72"><a href="http://apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.aspx"><span>Инфо центар и контакти</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub72">
			<li id="dnn_dnnNAV_ctldnnNAVctr228"><a href="http://apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80.aspx"><span>Инфо центар</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr715"><a href="http://apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%9a%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.aspx"><span>Контакти</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr832"><a href="http://apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%9f%d0%be%d0%b7%d0%b8%d0%b2%d0%bd%d0%b8%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b7%d0%b0%d0%b3%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>Позивни центар за грађевинске дозволе</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr322"><a href="http://apr.gov.rs/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%b7%d0%b0%d1%83%d0%bf%d0%bb%d0%b0%d1%82%d0%b5.aspx"><span>Рачуни за уплате</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr550"><a href="http://apr.gov.rs/%d0%88%d0%b0%d0%b2%d0%bd%d0%b5%d0%bd%d0%b0%d0%b1%d0%b0%d0%b2%d0%ba%d0%b5.aspx"><span>Јавне набавке</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr631"><a href="http://apr.gov.rs/%d0%92%d0%b8%d0%b4%d0%b5%d0%be%d1%83%d0%bf%d1%83%d1%82%d1%81%d1%82%d0%b2%d0%b0.aspx"><span>Видео упутства</span></a></li>
	</ul>
</div></div>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_MenuPane" class="MenuPane DNNEmptyPane" valign="top"></td>

            </tr>
            </table>
            <div id="SearchContainer" class="EMSearchContainer">
                    <div class="SearchBoxL">
                        <div class="SearchBox EMBaseColour3">
                        <span id="dnn_dnnSEARCH_ClassicSearch">
    
    
    <span class="searchInputContainer" data-moreresults="See More Results" data-noresult="No Results Found">
        <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" autocomplete="off" placeholder="Search..." />
        <a class="dnnSearchBoxClearText" title="Clear search text"></a>
    </span>
    <a id="dnn_dnnSEARCH_cmdSearch" class="SkinObject" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/search-button.png" hspace="0" alt="Search" class="trans-png" /></a>
</span>


<script type="text/javascript">
    $(function() {
        if (typeof dnn != "undefined" && typeof dnn.searchSkinObject != "undefined") {
            var searchSkinObject = new dnn.searchSkinObject({
                delayTriggerAutoSearch : 400,
                minCharRequiredTriggerAutoSearch : 2,
                searchType: 'S',
                enableWildSearch: true,
                cultureCode: 'sr-Cyrl-CS',
                portalId: -1
                }
            );
            searchSkinObject.init();
            
            
            // attach classic search
            var siteBtn = $('#dnn_dnnSEARCH_SiteRadioButton');
            var webBtn = $('#dnn_dnnSEARCH_WebRadioButton');
            var clickHandler = function() {
                if (siteBtn.is(':checked')) searchSkinObject.settings.searchType = 'S';
                else searchSkinObject.settings.searchType = 'W';
            };
            siteBtn.on('change', clickHandler);
            webBtn.on('change', clickHandler);
            
            
        }
    });
</script>

                        </div>
                    </div>         
                </div>
        </td>
        <td id="ContentContainerCell">
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_TopPane" colspan="2" class="TopPane DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane" class="LeftPane DNNEmptyPane"></td>

            <td id="dnn_RightPane" class="RightPane DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_TopPane2" colspan="3" class="TopPane2 DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane2" class="LeftPane2 DNNEmptyPane"></td>

            <td id="dnn_ContentPane1" class="ContentPane1 DNNEmptyPane"></td>

            <td id="dnn_RightPane2" class="RightPane2 DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_ContentPane" colspan="2" class="ContentPane"><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">
<table class="flex-container-1 fullwidth">
<tr class="EMContainerColour8">
<td class="flex-container-tl-simple trans-png"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="33" width="14" alt="" /></td>
<td class="flex-container-t-simple trans-png">
	
    <div class="flex-container-title"><h1 class="EMContainerTitleFontSize8"><span id="dnn_ctr_dnnTITLE_titleLabel" class="EMContainerTitleFontColour8 EMContainerTitleFontFamily8 EMContainerTitleFontSize8">Privacy Statement</span>


</h1></div>
    <div class="flex-container-visibility"></div>
    <div class="flex-container-help"></div>
</td>
<td class="flex-container-tr-simple trans-png"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="33" width="14" alt="" /></td>
</tr>
<tr>
  <td class="flex-container-l"></td>
  <td class="flex-container-m">
    <table class="flex-container-m-table fullwidth">
    <tr><td id="dnn_ctr_ContentPane" class="flex-container-m-td flex-container-content"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Agencija za privredne registre (APR) is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Agencija za privredne registre (APR) site and governs data collection and usage.
        By using the Agencija za privredne registre (APR) site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Agencija za privredne registre (APR) also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Agencija za privredne registre (APR). This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Agencija za privredne registre (APR) for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Agencija za privredne registre (APR) site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Agencija za privredne registre (APR) public message boards,
        this information may be collected and used by others. Note: Agencija za privredne registre (APR)
        does not read any of your private online communications.</p>
        <p>Agencija za privredne registre (APR) encourages you to review the privacy statements of Web sites
        you choose to link to from Agencija za privredne registre (APR) so that you can understand how those
        Web sites collect, use and share your information. Agencija za privredne registre (APR) is not responsible
        for the privacy statements or other content on Web sites outside of the Agencija za privredne registre (APR)
        and Agencija za privredne registre (APR) family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) collects and uses your personal information to operate the Agencija za privredne registre (APR)
        Web site and deliver the services you have requested. Agencija za privredne registre (APR) also uses
        your personally identifiable information to inform you of other products or services
        available from Agencija za privredne registre (APR) and its affiliates. Agencija za privredne registre (APR) may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Agencija za privredne registre (APR) does not sell, rent or lease its customer lists to third parties.
        Agencija za privredne registre (APR) may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Agencija za privredne registre (APR)
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Agencija za privredne registre (APR), and they are required to maintain
        the confidentiality of your information.</p>
        <p>Agencija za privredne registre (APR) does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Agencija za privredne registre (APR) keeps track of the Web sites and pages our customers visit within
        Agencija za privredne registre (APR), in order to determine what Agencija za privredne registre (APR) services are
        the most popular. This data is used to deliver customized content and advertising
        within Agencija za privredne registre (APR) to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Agencija za privredne registre (APR) Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Agencija za privredne registre (APR) or the site; (b) protect and defend the rights or
        property of Agencija za privredne registre (APR); and, (c) act under exigent circumstances to protect
        the personal safety of users of Agencija za privredne registre (APR), or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Agencija za privredne registre (APR) Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Agencija za privredne registre (APR) pages, or
        register with Agencija za privredne registre (APR) site or services, a cookie helps Agencija za privredne registre (APR)
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Agencija za privredne registre (APR) Web site, the information
        you previously provided can be retrieved, so you can easily use the Agencija za privredne registre (APR)
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Agencija za privredne registre (APR) services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) secures your personal information from unauthorized access,
        use or disclosure. Agencija za privredne registre (APR) secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Agencija za privredne registre (APR) will occasionally update this Statement of Privacy to reflect
company and customer feedback. Agencija za privredne registre (APR) encourages you to periodically 
        review this Statement to be informed of how Agencija za privredne registre (APR) is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Agencija za privredne registre (APR) welcomes your comments regarding this Statement of Privacy.
        If you believe that Agencija za privredne registre (APR) has not adhered to this Statement, please
        contact Agencija za privredne registre (APR) at <a href="mailto:srogic@apr.gov.rs">srogic@apr.gov.rs</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></td>
</tr>
    <tr><td class="flex-container-m-td">
    	<div class="flex-container-action2"></div>
        <div class="flex-container-syndicate"></div>
        <div class="flex-container-print"></div>
        <div class="flex-container-settings"></div>
    </td></tr>    
    </table>
  </td>
  <td class="flex-container-r"></td>
</tr>
<tr><td class="flex-container-bl"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="12" width="14" alt="" /></td><td class="flex-container-b"></td><td class="flex-container-br"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="12" width="14" alt="" /></td></tr>
</table>
<div class="clear cont-br"></div>







</div></td>

            </tr>                
            <tr>
            <td id="dnn_ContentPane2" class="ContentPane2 DNNEmptyPane"></td>

            <td id="dnn_RightPane3" class="RightPane3 DNNEmptyPane"></td>

            </tr> 
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">        
            <tr>
            <td id="dnn_MiddlePane" colspan="3" class="MiddlePane DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane3" class="LeftPane3 DNNEmptyPane"></td>

            <td id="dnn_ContentPane3" class="ContentPane3 DNNEmptyPane"></td>

            </tr> 
            </table>  
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_BottomPane" colspan="3" class="BottomPane DNNEmptyPane"></td>

            </tr>              
            <tr>
            <td id="dnn_LeftPane4" class="LeftPane4 DNNEmptyPane"></td>

            <td id="dnn_ContentPane4" class="ContentPane4 DNNEmptyPane"></td>

            <td id="dnn_RightPane4" class="RightPane4 DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_BottomPane2" class="BottomPane2 DNNEmptyPane"></td>

            </tr>
            </table>
        </td></tr>
        </table>
        
        </div>
      </div>
      </div>        
</td>        
<td class="EMSkinR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset2">
<td class="EMSkinL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td class="EMSkinM">
    <div id="FooterContainer">
    <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
    <tr>
    <td id="dnn_FooterPane" class="FooterPane DNNEmptyPane" valign="top"></td>

    </tr>
    </table>
    </div>
</td>
<td class="EMSkinR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset3">
<td class="EMSkinBL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td>
	<table class="fullwidth" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="EMSkinBL2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    <td class="EMSkinB trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
	<td class="EMSkinBR2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    </tr>
    </table>
</td>
<td class="EMSkinBR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset4">
<td class="EMSkinBL3 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td id="FooterCell" class="EMSkinB3">
    <div id="CopyrightContainer" class="EMCopyrightContainer"><span id="dnn_dnnCOPYRIGHT_lblCopyright" class="FooterToken EMFooterFont">Copyright 2009 APR</span>
</div>
    <div id="TermsContainer" class="EMTermsContainer"><a id="dnn_dnnTERMS_hypTerms" class="FooterToken EMFooterFont" rel="nofollow" href="http://apr.gov.rs/terms.aspx">Terms Of Use</a></div>
    <div id="PrivacyContainer" class="EMPrivacyContainer"><a id="dnn_dnnPRIVACY_hypPrivacy" class="FooterToken EMFooterFont" rel="nofollow" href="http://apr.gov.rs/privacy.aspx">Privacy Statement</a></div> 
</td>
<td class="EMSkinBR3 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
</table>

</div>


        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" value="`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`39`,`dnn_dnnNAV_ctldnnNAV_json`:`{nodes:[{bcrumb:\`1\`,selected:\`1\`,id:\`39\`,key:\`39\`,txt:\`Насловна\`,ca:\`3\`,url:\`http://apr.gov.rs/Насловна.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-breadcrumbactive EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,cssSel:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`113\`,key:\`113\`,txt:\`О Агенцији\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`285\`,key:\`285\`,txt:\`Референце Агенције\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/РеференцеАгенције.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`242\`,key:\`242\`,txt:\`Оснивање\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Оснивање.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`243\`,key:\`243\`,txt:\`Организација\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Организација.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`244\`,key:\`244\`,txt:\`Управни одбор\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Управниодбор.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`245\`,key:\`245\`,txt:\`Сви регистри на једном месту\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Свирегистринаједномместу.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`353\`,key:\`353\`,txt:\`Интерна документа\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Интернадокумента.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`139\`,key:\`139\`,txt:\`Регистри\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`59\`,key:\`59\`,txt:\`Привредна друштва\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Привреднадруштва.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`60\`,key:\`60\`,txt:\`Предузетници\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Предузетници.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`216\`,key:\`216\`,txt:\`Финансијски извештаји\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Финансијскиизвештаји.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`61\`,key:\`61\`,txt:\`Финансијски лизинг\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Финансијскилизинг.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`62\`,key:\`62\`,txt:\`Заложно право\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Заложноправо.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`116\`,key:\`116\`,txt:\`Медији\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Медији.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`117\`,key:\`117\`,txt:\`Удружења\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Удружења.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`118\`,key:\`118\`,txt:\`Страна удружења\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Странаудружења.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`229\`,key:\`229\`,txt:\`Туризам\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Туризам.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`258\`,key:\`258\`,txt:\`Стечајне масе\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Стечајнемасе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`260\`,key:\`260\`,txt:\`Мере и подстицаји регионалног развоја\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Мереиподстицајирегионалногразвоја.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`262\`,key:\`262\`,txt:\`Привредне коморе и представништва страних привредних комора\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Привреднекомореипредставништвастранихпривреднихкомора.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`341\`,key:\`341\`,txt:\`Задужбине и фондације\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Задужбинеифондације.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`343\`,key:\`343\`,txt:\`Представништва страних задужбина и фондација\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Представништвастранихзадужбинаифондација.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`366\`,key:\`366\`,txt:\`Спортска удружењa, друштва и савези\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Спортскаудружењa,друштваисавези.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`377\`,key:\`377\`,txt:\`Судске забране\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Судскезабране.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`599\`,key:\`599\`,txt:\`Факторинг\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Факторинг.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`591\`,key:\`591\`,txt:\`Понуђачи\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Понуђачи.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`779\`,key:\`779\`,txt:\`Уговори о финансирању пољопривредне производње\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Уговориофинансирањупољопривреднепроизводње.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`828\`,key:\`828\`,txt:\`Грађевинске дозволе\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Грађевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`885\`,key:\`885\`,txt:\`Привремена ограничења права лица\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Привременаограничењаправалица.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`64\`,key:\`64\`,txt:\`Прописи\`,ca:\`3\`,url:\`http://apr.gov.rs/Прописи.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`132\`,key:\`132\`,txt:\`Услуге\`,ca:\`3\`,url:\`http://apr.gov.rs/Услуге.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`427\`,key:\`427\`,txt:\`Достављање података без накнаде\`,ca:\`3\`,url:\`http://apr.gov.rs/Услуге/Достављањеподатакабезнакнаде.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`509\`,key:\`509\`,txt:\`Достављање података уз накнаду\`,ca:\`3\`,url:\`http://apr.gov.rs/Услуге/Достављањеподатакаузнакнаду.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`133\`,key:\`133\`,txt:\`Односи с јавношћу\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`68\`,key:\`68\`,txt:\`Вести\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Вести.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`310\`,key:\`310\`,txt:\`Прес служба\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Пресслужба.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`66\`,key:\`66\`,txt:\`Међународни односи\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Међународниодноси.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`67\`,key:\`67\`,txt:\`Конференције\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Конференције.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`328\`,key:\`328\`,txt:\`Видео галерија\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Видеогалерија.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`140\`,key:\`140\`,txt:\`Архива\`,ca:\`3\`,url:\`http://apr.gov.rs/Архива.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`72\`,key:\`72\`,txt:\`Инфо центар и контакти\`,ca:\`3\`,url:\`http://apr.gov.rs/Инфоцентариконтакти.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`228\`,key:\`228\`,txt:\`Инфо центар\`,ca:\`3\`,url:\`http://apr.gov.rs/Инфоцентариконтакти/Инфоцентар.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`715\`,key:\`715\`,txt:\`Контакти\`,ca:\`3\`,url:\`http://apr.gov.rs/Инфоцентариконтакти/Контакти.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`832\`,key:\`832\`,txt:\`Позивни центар за грађевинске дозволе\`,ca:\`3\`,url:\`http://apr.gov.rs/Инфоцентариконтакти/Позивницентарзаграђевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`322\`,key:\`322\`,txt:\`Рачуни за уплате\`,ca:\`3\`,url:\`http://apr.gov.rs/Рачунизауплате.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`550\`,key:\`550\`,txt:\`Јавне набавке\`,ca:\`3\`,url:\`http://apr.gov.rs/Јавненабавке.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`631\`,key:\`631\`,txt:\`Видео упутства\`,ca:\`3\`,url:\`http://apr.gov.rs/Видеоупутства.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]}]}`}" />
        <input name="__RequestVerificationToken" type="hidden" value="80KTddyCdqMzdvwE_q5lOXEaGRB3YC_D79rVccxrImYPPXRKElP1nUQOjz0eg7DSCBxYnA2" />
    
<script type="text/javascript">dnn.setVar('dnn_dnnNAV_ctldnnNAV_p', '{rarrowimg:\'/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png\',sysimgpath:\'/images/\',carrowimg:\'/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png\',orient:\'1\',rmode:\'2\',css:\'mainmenu-menuitem EMSubMenuItemOff EMMainMenuFont\',postback:\'__doPostBack(\\\'dnn$dnnNAV$ctldnnNAV\\\',\\\'[NODEID]~|~Click\\\')\',mcss:\'mainmenu-submenu EMSubMenuItemBGOff EMMainMenuFont EMSubMenuOpacity\',easeDir:\'0\'}');dnn.controls.initMenu($get('dnn_dnnNAV_ctldnnNAV'));</script></form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=535)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=535)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=535)--><!--CDF(Css|/Portals/0/Skins/Flex2-V-Round-Stripe/skin.css?cdv=535)--><!--CDF(Css|/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.css?cdv=535)--><!--CDF(Css|/Portals/0/Containers/Flex2-Set8/container.css?cdv=535)--><!--CDF(Css|/Portals/0/portal.css?cdv=535)--><!--CDF(Css|/Resources/Search/SearchSkinObjectPreview.css?cdv=535)--><!--CDF(Javascript|/Resources/Search/SearchSkinObjectPreview.js?cdv=535)--><!--CDF(Javascript|/js/dnn.js?cdv=535)--><!--CDF(Javascript|/js/dnn.servicesframework.js?cdv=535)--><!--CDF(Javascript|/js/dnn.dom.positioning.js?cdv=535)--><!--CDF(Javascript|/js/dnn.xmlhttp.js?cdv=535)--><!--CDF(Javascript|/js/dnn.controls.js?cdv=535)--><!--CDF(Javascript|/js/dnn.controls.js?cdv=535)--><!--CDF(Javascript|/js/dnn.controls.dnnmenu.js?cdv=535)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=535)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=535)-->
    
</body>
</html>