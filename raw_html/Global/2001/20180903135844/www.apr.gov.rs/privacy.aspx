<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="sr-Cyrl-CS" lang="sr-Cyrl-CS" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2017, by DNN Corporation -->
<!--*********************************************-->
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	Агенција за привредне регистре
</title><meta id="MetaDescription" name="DESCRIPTION" content="Насловна страница" /><meta id="MetaKeywords" name="KEYWORDS" content="АПР, Агенција за привредне регистре, Предузетници, Привредна друштва, Лизинг, Заложно право, Залога, Јавна гласила, Удружења, Страна удружења, Туризам, Финансијски извештаји, Бонитет, Регистрација, Регистар,DotNetNuke,DNN" /><meta id="MetaGenerator" name="GENERATOR" content="DotNetNuke " /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Resources/Search/SearchSkinObjectPreview.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Portals/0/Skins/Flex2-V-Round-Stripe/skin.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Portals/0/Containers/Flex2-Set8/container.css?cdv=581" type="text/css" rel="stylesheet"/><link href="/Portals/0/portal.css?cdv=581" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=581" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=581" type="text/javascript"></script><link rel='SHORTCUT ICON' href='/Portals/0/favico/favicon.ico?ver=2011-09-19-155652-890' type='image/x-icon' />     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-6906308-2']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  </head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="6sU+zVncSydPwIb530lw8bt5/XIlqZD4CUDklvM6aU/p/xPGfoFo0EYw9o20wf94CqHoYwWblU6LlNtTOwfvuwui3rKV4dPoBlHnMHxIqAFNMKT9FYy1dpqgfZYsXYpWI0xyrjdhZYzq1ndq3kJBlnS0F1q7uNVPXPYVdrqScKOnDZftXD5If39zCmrRqRG5xq2qcZ2Xl8iNa9teprR4KhjhTGc5TY+fVh1rNqhMMkHRcxQgHmG9OyShxfvjixb7Xp53ju54yVIxEDyonNbjq5xjiOFKvGzrvJPCFuUoFvpBXB4PK3wdN25fsaJC5HzhkV2lrfi5TVFS4FVribOOsqtHpYFQ8ju12G05Q+0q+domLl8dw3+BR+2rGtepY/peSkyu9CCm3uv8MW6LQcGggtI1kvlB7AiX0axUc5XjxyM0pCGVY5o7sxNtlYsAY3HWL//S1KDBoAe6TZJH7usmyBez0vBCBpuEoiMxqHGkB0Ws9v/d9ifYnqUy7K+W/SllhEy2Aal6S06K3SSQ3IFxN97DgqZkk63wIlbD3kWLLuNRmWxtlUn+CClER9OOjpRkxAevN5ichgYKPQy4v/2oFxh0ceiTkGnavT6i9901UalC9MkSabh71L51Nndn8RoiNTbg3ptmpOpaG8lhzYS+7j2jATh8Y5UCm9TBoVOX5j6uR62RSBJM962OBzgokG7EBSgKMVs8kw9VKrLWTcvyp+ng+sx4sKH+WQElFYDlz8rvqrAUlLx8OQqA46v4yg2fVZdvhwTvzfcdaKSV2659TskUuw3dR8Trk6TT+tIZkNoCS77kgU2D+VJ3fr3UcyjwG1kMLJ13uX2QQaYCSSQiCZi2SQ8nerwAHz3MDrU3QBEz2VYRh/WibPbnIRnC8v66BKzOsdhPWAY4qWqekvCIcMOfriiqc9K/V4wx/iv3VBg32QQKRru7aiTclLQ64C43w8fxWxPkwzO5QaZDWnVQDC9KWsKL+FzoU3JlgPCpubU+iFoaNt2HgvV8+shh9dmmXKLJ/6U6qC4Bo5x06b4aKJlpFaFYjW62uor1KLO3bjOYwPS3nChIr6lTzfvkjeXg+DBQ2GPtwBsB1LH48VR8ekkF5bdrEZcCsgrJgXtvLoFL5xM2KC1j9qXRH5sMVjJbaTlqdD9y3IfFB291eY9f9fsoF1ZhG+UslSrZ173TEFkXcH7dUVH0LBdWKv01zP8zIk8q7Vich6yx/y4AxyEpyvyCK03kWI/qycUsrOD3zZWvlEs1Kya32FTLu65z+lBkTwyLXEXp1HNkPlV+udDubNCVkQFanr7kc7Vnj0IRdVW5MjWANC/J6BiwDiJgV84iR3XCeI8QfsP//tWB297nLmwIo54dDMgj888HMqRwOGR5xk3wLN4EUvA7POrDYS82B2aekPjvlc3X11EgLFFy26HrYTKTgBfus7h4ycquFhkXg9U+R+KoPKNsYaBY5e1GRWJv6uuSkCw+PU8vVgHTU4dMeIqWKzfI39p/+cz2Ur8CWhz3ubzVBO3aSR3jnWk+1iURfhSmK8XK5CP4QyBtTVvhiZKeiTf/HY9Sgr9QomTB7LEhaJXM65uNVBfCWkCp6jN/+47YM8Pz0WUlUOj+YrTBD9F1V3qY7j0jxfvwSgcvsZ4b6VKNt205myQspTHyncY8wrcumSCi56QrTbTzYBE62aMWVRblWlVRkwFwvJbRonrV3/F21OiZU7Jlk4/WU87aV8lEM/toXlvKZIz2O+xQpW9xi0bmeR4xT6aU9kNkvHt17gjZFlgy2i1NCDXI/02cLjDpQVRJJaR5J72jAOJLacPgGbyMfm+kgLDtZrMapH3f9QXT0ZKjLm70TVJWZGO6vH8vbSufe2nPvXt4fu4B/US2Rk0X+ZtqTNlv91Pk4KCHSivAvXPwYCPcHUZgb8h+4r4q1wbd8/LJ453HKWrd+L7VFk5U4dX6LXku4KVzQKsD4bFkBM80tCs9FbFmF9En+gZ7p8XLsw8abOoR6EU0Pi0vLkuKH0NSjKOTJGW90EN91ghQkZFVtWrtu28d7I5zLLSZI/Wtz4uOBJQbGPjI6bQrL+eTon6v2tULE7ZKrExvOzNPsSijhPJhJck21KnTZI7k39aEbvHsYaqoNwzVtORoDfhUYgXeOaPdSULUi5jVIZo5WSDWRvp09cuz77s3wCChmefS4CPAnttL5bbb26msYBUdgP/ayWczV+vkhzeFdyTO1UEBZ7pgfOKSRq7UnwF7mmiQ71U12ZFRbrqRnNgnMr8bIONPvf8qufcGum2tmaZgtqqQsgZhRTk26QzCAJ1uivDXuNd1zXO3SRjvu3ja3u624NyxsYvXa/nNiPaKZLNqIY8ILs+1ifFJnkUAaDHaWkZBEaxQ3lkTx+ignS15olO8aXnktBSipEsPdm/J2RATPcHlR9MjkUl7UEP8sNBIqWfVVUQ8TFrBGYac5GkBMzjkwDyW6xI5eGofGB3pmoLZQXR15YX2BfXelnl52CfKtKjILa/Ot4rrNF6j6tPwG//BpQP2H9n3lbS0JEnqENIPUASuNbTtuHh1v5infxxI3F5R+mjOUWMwKtfrE4535Mddmfb/dRdD6w38+ZMmUGcaZQkYicmC+F2oRI9x9cwT5bNgJ2C3hYgU2qS3FX2QIyfOOMq4Yhb3bKnaV+W97yCCcWQylWpGFFPIGQ+BscJ0vIaSGmSxGVsQmi4pNhm3rDoZfBpN5PYNyAB/xhKNU5Us0b4+PvQs7GgSgzwZOrseJteoZThgqqSA1HqJzTNRb8ubVp0G1RUjYF5eCMB5EVuhFLot7kxw8y2KKabj8+DLOBRyvc5zef6XaFkU15yazBjIA8D0CEB5zTZFyxgaBS7by51bRjwdjk57/5oq4aRYjXTV5mPLoqSvLcogqqhlG9gACyaK7+cBSI7RvYgoHytNSKH3jR0NT2/0F42JDjtGa9qtVakaemskLajJaW5qCaKt4JFZpy/rP+nDEbknkUoOA0ePoHgQi7EXLWFRVVcacEvDFP3QEzo/Zpf+r31SAfSIry9RqbnofFuaoOAZAsQ66SWoxKQfh3ZFSaoRGQiPUmRgG8R33FM3DXDN7vf+woP+OC7xibcrERt/udHEGSjaFZRKBwgUDO81MXpfKG0eHr26C+fqWuybVICWTrXMxgT8Mfp5tit/uT9ku2lVIm/Pxxv1v4cwmkPl5+dIZ6VZ2C72uF6OiEbtyXOEkZVzjqo/v2Wor1h/De4IhkwQeE11+iIV6yIXmcDYmvJgxDw8Tt1xXbN6FL1H7CvPNQFfVCDn1zhxFKG9fzliJZRts6GSKYhiPxk8x9ugiFHnsxUX7I1h/PfV7WNKyNOMgF6DzR2ezgE46rIVWUdXDtkzdcX2Hs/4grXZq2GU2mKLZur61KCo6lHFitayQtMkEC4KfsHamsmoOa+c9NhSwzKIJxTkT4dWo1OJfXAC/po/mfFdEhXnYFl+6pXupOhGBRN7W6SteF1BsCxrl+W4GAfrdVnscjZ9CRiZIPx2MJM/vb1ySWhru9a0kSAZQPnpQKv2KmQbgmemIYOwKg3OrKocLyOWLCXx3/9VSQsP4EAL8QQoUhU0JeQEOuXO3zpVXSPACbV3a+3G/M/Hbpav/h6cPDlCswle1KbbnZu/isTEBsiUoTOhVlMQ2L0oxymHlxh9rWWeCYjFY8vLi1r0qRN7Vy1D3nsnEaZJx619/OfAPNCtCNXRQ9uMeQ4bPf7pFH0P4tYTN83tx6j09T6eugIgT3fuP0wl3DdrDlGi82JwQey7HkrcfLMnRUmjN/X/+QDS9dmKNbwyufBGZyMEMeFKT/oH5DHrz12ditS5eTZM0+qQ/h2oBPPbmzHKW3zsdw002MEp8JJXAwtAq0hGFONiSu4efI9KBj+p+g9zual3EKv++q2ltHpbuTq1LFKWxAkqgXQZxgs00lWPevdZHdC28qQtG4nK9WIeEEs/zKwwlgYYvCeYc7VgL5G4M5XpzrcG4WR8Xk86FrmRADdc3icC7MwEVyh9JbNepdua+cZCPxukjRK0uXU6dYP5ZFy/ri6jBplriId3SkYp2DutBK5WkySfDSqSMGIIulYswQ4T7Ip9IGwrer/qH+2KzZA1MNCeGUI6esUj9eGv3XFk8akcowjYMzU3tr6dmPYgErlcezmEHqNldUZDhDZveyJW9qsED4vbg5noZHrzdP2nWgdYIVIXK4JOKswIMpUt9XIvQfCsstRDlLnMLb1VUd3P8oQFwHeQEoZwNzD9eQXKBrCXS5kkFjGVtBugUh2vIRSdbRacKXJhSOoao2xbZA02HhggjieumUy79TPzrWCFHPnF0UgJ2YgH7c57wYzxXlMQoT4A+tnVci7jS53Rr+ngkhrLR+zdJYVkCi8ahU7sPO3I8CiQYJZBvIR9w4rTOAODLXI49LwFx+vcozVp3Gga4Cr89MD2o73fliTG0hcOyI/Din7l7p3/P0BQ9NcZrSWqWNZ8GUgjgOm2LxYYsIcaXUnBuvDudncdBpGMcx/ydP/XpARlaewE7bxW4S8+IgsTbYn7HKUfC/a+R/bFFM8SFti2MPQK2Axl8yj0r+OFWSKbw0GqPGRtnSoiaFYRPLzyaQQo0pE+N/q43MGdsguewcNlk7Rm7+s/zK+2TnwLEum+cPjUkI3OwOOacCYe29OAT7FGc9SPxjqRTMiRc81LzQaGo53MxmhPIzLJQMf6oGxL3tXbdZEuJK18Gh7nPsLk9Bptl2/aOZPIfnjW/mOHVPu+5/MC0exR6wXHyARvOEC2cv228ZWXQJ5IDP6lO6JVDIzgofFN8/9qQf0SbAmPmfJ7pNUnmlvqDyh1A6m8XZzUH4YB1DDtfvEvUw2CuRBi9cbKymEF7nx/83xBqmTXFpTt3Rc/o4F62+pZQJYTAGe9BGUyc7iyRumUUPPgeZifQd4bhmj2lTYlp4umTqhQ9I2l16xdlxQu6KbWvM1amryn5EAsgMCPPdQj6FS+VzYLienmbiha0c1T+JQuQ1mQrpuwmegGQa2lNSHKj7MaijENHG80aGAxuNwrK2IJ1W0E6A7kEdgH3dKaJ9wLySr3V/jrCOIGA+wylsFRpQO/8TLYbhVaRo/4klW5QE5qoVWDHZM/7WgO54J3SFuT65fNz5err2ngxjfHHrxtMuQOULd8HWR0TJ3RUnUuOWbvaT/EXJKhAOhDdI3n7OiBuwT9AQ9qKTYnAOS0nWBaHiodptq5R/3tq90CwBfWsVKCRgTVklmnHZDri3hN2w6iNO2F0kjS5DdOJa43uT1CoQ/TRpopGSOGt5Bt3OrSknEzovBcPPx96p5yCPgqLAQghZlTTopQi6UpvTVsfvos6dTysOvt7DctmFRE5f5MNoll0DyWO6RxyJwAT6HSySvDoSdkRa4hY0cZRFC0DpfapTMsUMYbW5mrvEjt/5I6zDDKnQEGpEdEL7BQAtIDTQAjqBFblqX9sYpnkJmI/fuBQBM/efRB7gAY1jiybYUr8fMIsgwXQwLmF5zc+7dgmWCoqzd/HZVcElQgP+TfB0UnLx4JXrEC5NIA4bjiZkoZj4xwdD0Usubz/xsiiSUTtnjJQ2r6+VKW6fQoiTE6rY7qa5ICm6RwD4w/A8Fyzx82kurGl+QTljCg+FyosVp+ZeDEshHdi7xt2DiNlqODLId+1ywQ059zu1LIigk5spBszaLDNa3OGQ3a/07Zsi5ALRyqjcOtp5Gl3ydfel11D66zyTlNbqUnS3cBVnQMWxnQWbSk3amFDCRI8YQ9Ak7b+RpJikGKLrS46Zc4/5gbBMDKMnVA9rDp8EX7c1/3sppYZJn51uvbzphMMdjcOjbWWt1gXgPDarn36YpwXjzoud2P8z5M8JjEvP6XBT+2w57eKt0RgT6A69Jhfm0xV/edHYvmy1xn8pqm17Acs71qv95SBbDdxD33Qlzs/Wndl3wg7+P6QeQ0qEZ307IeqiARCDIsYMJirKlmVSWjJnhm1WTNoEsyvvGNy1I/I03rlYR5nSuBLBuon5UyQQLNihQ7oRWcGg9/jOggX7H4HzlPx5GQ6pwoEhQGbmBM0Q0P7WfX2Do+SDLR1GO629g1JlPZDTaq3J+beMRayR+YC6zjyXMGqG/DF0Pxe4VunwKgm5NyEI/etW/ainAb8wZgCWxulifGYsQ+xCh/2jJGX3qwySIlNGJ06lrryYhgec8RyH/uAdD7/gzpSJbnhOz/qywROCwwPX4EIo+VWQ9eUw5fWAGCFFEDRPK0ks47Ce/HcSTIjJhCvQaKUFQEVuDEatiRktwlsGEwcPDFgW3v416aFC8rBwwIbZuKxQ/yGrKoSNn2E6m0qFZM6XEqL0ZUeLOowD89/FQ4ZNkL5UkxP98LXI3DpYq7gEfRtWRf7hSK/eb9cuJGjCM/i2Y1Fc9IH4VSGerXI2mfXrSC1QxgdUves9kKeit1xD9yrFDt4N/ByOOtYf1Dx2fnmf1R+9XNjaTJ9Mf5vgmEuoZHN/zCHCxhZLcsgtVr/Ag5kQOWvWul7VOQNr5XGCgUfibZGrNOWvuFrcqG+S77SQTkcvo/LAxIs88xw2fLYQvJuGJnL9YT61axC7HH1FLHzWqNxmEX0OFMn1Y4HPe57YyEq+pLHDdqIzncOm7p3qBOdS3zvJaAEx0NVWgS3lDBMoVCGVIKxQu6IR7e8gp6qbhHP4GkZ91d3KUr+KEYAlLHc2KnC1uGNqdKfl9Md2btzuUuPTc60ThMNRoJfnkDD8ftfjo+9osoooelesstFEOGnnkkIZzOYd92BVEkdE2u9JKSS4h/GhzLfPJ3f9W9Jivjgpjt7hc79HBwStNZWowFEiofxB0Osci5f4wJcgpUHx6CEVrsD1m47qq979IQg/a9lhkq0Kfa8NPWBcQy0yDApeBzDEWFJ+1AotRy48O26p1wr9cJTImA16oispzeSKBzQZrmrmTGxpj5Zc8VMXeW5xUfPcibOsXdsIEBTdVBd3X457y11pz4WhTlU7fHtMgfYQa8bV645K84BG1a/AEzxoDC2pVlYTZ+KY14canKmhhV33MAoKS3IycNws8vT0XEpSOLdim1LPssaKBx/+S3APW/Z8nLC9TH+uYemPtWFtxjlDHEFRx5RUsn/hMxYoJE0s21LFosZOjfZtcqErxJ9CRApHMk3uG0QjGlb9fVsq0M04wue++Us/yX4xScgmUZXEnm7u61MzbtWFAuj+vTxPzbMaEZmbDdVsuZunig7nuz8myGHwPMXLMnkOukhC3qviNO9D0ZmKYw2lhwvy9E0GtacP2v74qXAREroZgt3aFOYbfEa2emlvYxfkfTbrL7gCyU1aCxRn5QZZbIw8EMtKEvoanV6OcbCGEa4EwhwpmmToCRZPDdTKdaVUVwJLp08VQRQ0woxTCRRpTvsibB9E9MTNfjFlZ/C9XesLJp6gVdimZjE0LPDXzrqdwrnKurqOrvkDV7U64r3RWbyOo/b/YxKtWVWNWRqwQxEGwk/zl/vrS+ScKyctY8VRQ/mKdqr5RXkpMSAVE+3Kc0zahzW+3iKuvhUhKq1c+g/g4UYKahR8E23ITCDqQuMnQeCjahpdTQITxiFQzLfeZ6poKh+83NuzsVVYUB7pB1lLP4Gx9Nas4TQJWzm/1erYdAmgu3UVtF+GgWE0fKmGfxnKONzsk6WpReWpzXCw7vsfr1PSr2ANsToTQntl5ctf6ldu3cohT41p2+MIYMhyGnlxbhCYbIhAbNNBIzHfj0r6sV1ksvNxlpbZXBs2DuCKJyz34sKGl0kudM9NgGDqlAPdgQyKtZinuVQoKtsR0+MMR/Rf7IwEGbPvkZaTB1yHEonAU6NzcoCUzfZCCh7dsYCmE1cjQ3uAOWOzf8Gb0x/vQ0JtOmGIzsvF3RDZkRau5438ZO+9AIHhMu0fXyaZ2I8/5kvWu+l4ZBZ/vXK3xQiN265yCocOy2OUohnTGhXoaGCMUL3MgoQX7efZoxMMfsdoI31rlruGl7T8v2FRoPPhqcLYPFgynyZvciUHTGynKdpAWysFsRgz7KOn5T7ad6QzF8GulRwoG+Q9DqV4w5SvLrumbEYe7Ewti97Z28jdZm0HyHmoZ7BzqSfLjuEYl/jPGmT6cIAqIOwa+LmWjYUOrz9vxHx6mX62BTH6H/leoy6GUi3vPqG61TsVWuT08Oo/kqiiWbWUWYAnFPZ3WNpGRYNe1DFGgfK4E1O5msKxnIiaYLYhWJwSgJiTHK1pYEtYJnF7gXTPoDUVEwqcBzhHs6Jqtw1zvFnpxfOLKEI6xFQSn++zPyvcwt1BoIE7Mf4ysw108DPVfTG+FiDSFafSPclsRv4GMeYF5Nmpo6BkAc0HcOmSzKgclFelb2JXDf1YEMIQ2tr1jhTyO4BhStiFVijJlYj8EY+Z/cqPmwy1fzVV7mkq1rzXKi7LeCrSFjBlNv5lLlQqfDBomUxAFuqQDELslfqpUVahG+FMmx/elr+C6HT68VZNMepCY2Nwqe9X9vvckIzMJYJ9V9mcx/UIpBHo6zQW1x0poELBfPhDGdxFmyicEi1WNJhGn1iQFpohdeLrNPXDLVFay/RSswIotjypeh6pBUT7sFZjNJWkQznagbT+uFwDGjj8sylJWdhy9PqjHBzzBTPszkKH2eny5bYcgFB3G4d/z9mCMvi0gizrxW9sBzEOH1TjKD/bjz/ydTMEnM0JYSmZoUbjwz91XZn2wYzquhh/t4CLYwpr8VrLUve0grgnXeIYLoAJReBHfvklT5TJwa1POD9jG/35SyNe+d9oKAuCdTLfReQQPiPBeD3EW4bqY4rTFgz4QIRve1PFwG4bBchAMOnjsGMDBkpuCM0dTTv99+KFZp5WwoFH7QfWMQF2ajOirES7KhHn/gYNWtjKf2mWBT9SPMJhR6qoHi3MBAdnaVWXiUmodGyFPWlqytvgBSFz+Vq3HLt2C1mhyvVCzidFf5xO2oXrQEpijAl4iy7HxU92FldPhH0hiqdNGXYflHWHWUVTmg1uk0gTXZA1sDCKS6IMvNBD83UeYuh6LLISZ1c88HT52umAqgRzsP5ftLL+1HEwCDTxNePeHaMr5VpDHckXidVOAVA2y6ewWtmuuUy4cmGk5eM62dUo1BdNPLoPLF3jCAEG2RihmxymVoWit3EPFH3PVLrRzvE8ZVfbGy2XWZPOBseG/zKHcH0e6Kb3yaYaI+8NugdYGKvuYW7qmCfblikIXN67Jwslg06k5cUNtrU/TZgY6Zrx5ezQdJC8zc068GyVuSp15hUzbX72cagxwF0TSesWvn4k4jC8+GHbmt/xZ/3jYTtYIsDI1u6J6i04Vy787P7ykVcxuXpuQsR8ZV025eWkKCKazzYrk39B/Ihf9gSK/uT1btJdLX8HxLkm0nuk2AgEUeoKuTKJ4aN+RIlYFhD/ydQVhSlpS4QZ/yofvVFfmDWxTQPa0QzqC8Pz+JfLr89jDjrq1PUdEtfXp1XYUXgrnjq9pWkr8pPYwQeL/vLH+bJvk4SL/Pg/dMxy+pubpe2Zrqtxr47AGQUxKzB7B8vZi61Q0chFaDniaKtp9chZnTqUCUz0m/tnaWLcDYCe6ZET4TGdYxrdHo5aqbXU2axwBACu1agEWVykUYAxd5f9D58TbFPhrF3WEVglTOyVaKBiiB1eeLeQMruAHMOWbEWortQlKkrH8b6FwiV9Ow2cHMG9AfM/hrGHoyNtvzIZRWEivEVGGe8MY9pOONocFk0E7bFnF5Iz5DIr0EiXWh3P4iiBnYaKvpBSMz9gLoIi9kh8wqPkzQyN89X5PXmnoVDFiMBPX5OXwedleYKwjpHNTzB68TUfKrfk+9+sI6aALY244QUXT/y8TUO8yrFKO8fn0uZ7v5F7r7auahk8TBgc9LMr/GP5t++Y4r1BuEKUhFScdhXKQOYKeqThZNhjCb1CUJkWYojSUrmMpQdqVMbEfN45qmGsQVgAG1lflM5/+PcsXU5zA54eXzlDcf8ifwvBMFFtEN/BZeUBuqVgREn7nMaA2YdUvzc0E2l2xWlF4HHzKRq9rpjHmyxESzQWV9tW0v7usmpzZ0zOkpqh45oY/IW5UISW1CPeXi+DJ7kCC/j2EnUYIHXUwhD9hHSujb7ISG6bWbUpSlfXLnCSYTYD9MPAC+WfLe6l8MpK+K7w+xscnxk66k+ZxwJbuq77bUuGYXLbouzMSSRM5ymOjhv25QPVomrDO8xDp/axTfr3hdeJUIe/MWe79p4XXl9U1hCwZ6oxXdvcxIKL+I2eUAfNOAMU3gdmcT/Ejwzi7jSYTqzSOLe4uI87cEXGfbU+n45hEEj93y9SZsmrn1nM+ENeP3V3jARK75weE7izG6uarHhDOvaNMfBCEY5Bdb0xuAo5anHmcIjt68H6plcxQLtDYDz5DeIood7SgMDPet9SH3qSGcL9yap9YK6yeEKqbtQpTHm0cSX9H8iBJe7YtkkThN1yPnnNGat7sPGccNR8ZQW7o3YlWWEKwch+nzQ77ac9cLI+jxpdQH9hty2TfmV2PQXRUMX3TvhQ0mmG8BBsokxzuYr87wdZ+qOkL1/YqqBB39JfnR/VB0R49SY5r9fhtsBYVjdwVRTBoJwLThjuMZdDnp/h2EqFMLIYm6ZAO7YQIDpFD6cveyEV8KYiGuFbb38JMCv5/OP7hxuh52kEWz4RMNZmssZTgY9B6qpaVEwYyCzikCBRgXt0alUCdJfWtAGehbwHnhrKWA+nRVlXPv1TGXAqAvH9SClKD/UHg4KfxF4SEEfV8xSVXQlOWwEepisovYlN65vrRdDvzZLfHQOiYlz7tktrQ1Bg0Lhz8RmFml5S2zLnM2GpGjB8UmKzTxn5Bbm+JlMhCUeK4LNm6XiVtxz6qWI/g+3ni8Z37PAJdNmdkM58LZ5wsIdcR4joqnZErx3LxPaX6tpCuljcj/qtHLGr+L2GufLuEz8ABGx5V5oFwrz/juMLh74nyEXV8/tJO8Iy72zyBCEpxye8W/MoPGiT54oN6OTw3SFmSyopJHb+YdsE7MA/HTmhbJrHZ1ET1fazVDHTm7tcTYrTb6hKf6KzdgvWZf5IU8orTNB5EB94Y0ktBlNWAdQ8wgWYMPb95jdEBB+Cj+J44TwZnp6aOCpPn0c8wapLktg5Ud8x4VBzL55lpQTNhiK3QQ3R2+v1IvPLL+VkBW9MRBOeDwvr0AJ8NOSTmS2V6TGEZAC3FRqZKtLNaoR7UPQRjt7MlGDeyLWqQMDGd1hN0A1dOcC0adNBy4h3052MkGuG0vO07+7AafYWzEohActWuLePj3jwU5WNpfR/vZdhOOpTEDsUbx4DYFcvs+Ths6Eu0kCakI4rAbEkMlxpAI/+dL+dIiQE4OT+y5lPuENYucoa4bwegJviTXS8N0wEiEICMOkDI4qKh/BDYa8vuM/ExsWpQjrUEsNJDltO0ulnzZpm/33gfy5el7Ld5v2ziJmzBf5Uty/PmkfRWDJz65hJ1pqy7SNk2eE3nacItegbgsIsYI2Iga5i/0ptAinHzsUcBA3qgFVgAR9XIXKfuQnvFXvzmIQYeMXjfo9KrsMOIEn253yUbyE3r1PgErzqr+sL5dTR/zYps3ikh3GsNm11C7CoswpAOEvVHR4Dh5r6HdKx62Tb4XAD/LYYXr+7nAZaoKd+lYAgSLXGIMJe1uR5WO3Q6Z+Dyp6qrMAmcyiSdMExwn3xncq+lfwmnNmRLBrx/69y+L3ITfzxj7KR0XezhyCnH/RGRMptlKh3FzBb1viXA5HG5fF+/W9WN7v44wn2BYoOYG74ca0N8hFJi/I0Qeh6EwdAncKqBspHh9Vi06qI6wmDcL/kZZsXVoqzKEieMrpCAUrYWxVtcCcGET6zmQBB4woLxi/RalLPXRcSVMDjpWh2BW6Ch8hHE0RhJB70ypWVAsvBpRiThDHbvkM6iAPaSrG5OqXBLN+yD461Y/ovOXipMHWXaohOxhAAO5s0wiBfks5JZd/vsxShuWrW06FLy6kDj9TWaVypTqeXIE7UVDAKelidQZObRrH51O0HK9i2+biRalKZHF0uuaf7n17Sbj0zeS8667qjN++cSM9Y2cwi8BaxhYnMTlnGzY7znNrA==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV2aNbE0v3xAhmIgl78LJs2xV0TBqp0uo6vQSD43MG7X87rwPP41&amp;t=636476155203255666" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var __cultureInfo = {"name":"sr-Cyrl-CS","numberFormat":{"CurrencyDecimalDigits":2,"CurrencyDecimalSeparator":",","IsReadOnly":false,"CurrencyGroupSizes":[3],"NumberGroupSizes":[3],"PercentGroupSizes":[3],"CurrencyGroupSeparator":".","CurrencySymbol":"дин.","NaNSymbol":"није број","CurrencyNegativePattern":8,"NumberNegativePattern":1,"PercentPositivePattern":1,"PercentNegativePattern":1,"NegativeInfinitySymbol":"-бесконачност","NegativeSign":"-","NumberDecimalDigits":2,"NumberDecimalSeparator":",","NumberGroupSeparator":".","CurrencyPositivePattern":3,"PositiveInfinitySymbol":"+бесконачност","PositiveSign":"+","PercentDecimalDigits":2,"PercentDecimalSeparator":",","PercentGroupSeparator":".","PercentSymbol":"%","PerMilleSymbol":"‰","NativeDigits":["0","1","2","3","4","5","6","7","8","9"],"DigitSubstitution":1},"dateTimeFormat":{"AMDesignator":"","Calendar":{"MinSupportedDateTime":"\/Date(-62135596800000)\/","MaxSupportedDateTime":"\/Date(253402297199999)\/","AlgorithmType":1,"CalendarType":1,"Eras":[1],"TwoDigitYearMax":2029,"IsReadOnly":false},"DateSeparator":".","FirstDayOfWeek":1,"CalendarWeekRule":0,"FullDateTimePattern":"d. MMMM yyyy. H:mm:ss","LongDatePattern":"d. MMMM yyyy.","LongTimePattern":"H:mm:ss","MonthDayPattern":"d. MMMM","PMDesignator":"","RFC1123Pattern":"ddd, dd MMM yyyy HH\u0027:\u0027mm\u0027:\u0027ss \u0027GMT\u0027","ShortDatePattern":"d.M.yyyy.","ShortTimePattern":"H:mm","SortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss","TimeSeparator":":","UniversalSortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd HH\u0027:\u0027mm\u0027:\u0027ss\u0027Z\u0027","YearMonthPattern":"MMMM yyyy.","AbbreviatedDayNames":["нед.","пон.","ут.","ср.","чет.","пет.","суб."],"ShortestDayNames":["не","по","ут","ср","че","пе","су"],"DayNames":["недеља","понедељак","уторак","среда","четвртак","петак","субота"],"AbbreviatedMonthNames":["јан.","феб.","март","апр.","мај","јун","јул","авг.","септ.","окт.","нов.","дец.",""],"MonthNames":["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар",""],"IsReadOnly":false,"NativeCalendarName":"грегоријански календар","AbbreviatedMonthGenitiveNames":["јан.","феб.","март","апр.","мај","јун","јул","авг.","септ.","окт.","нов.","дец.",""],"MonthGenitiveNames":["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар",""]},"eras":[1,"н.е.",null,0]};//]]>
</script>

<script src="/ScriptResource.axd?d=uHIkleVeDJe71swkG8xd5vEURr7bXsMJksoCf852JUAfVA1fWCu4qAk19j2IwdllUwCO_W6706nyIVYW4QTXHqm77HR_M_G66KTg4JFFOEt2C7dFZvbabdR-beOOrhWVp-Cn7A2&amp;t=ffffffffad4b7194" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Jw6tUGWnA14woTgPvOH-aA_V-3Ps9rcTdh6L5lcuhs9ykMNgm6DjPhCCH3aesZTLFqGFhHOx76ImCYZ03W-VQoV2Z0XZgCNG9ADSPgy-msteT5x5-pg2Cgfx8nWK7MfsCsU83zU9QoHYT67M0&amp;t=ffffffffad4b7194" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
dnn.controls.submitComp.onsubmit();
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="pFOonbhCco18MS0vRY42uB5zl+YotYhfGMvhR2zVAVTMptNmev+NDixAbVF/4S/pxXCJauVUO+HzcY3zBy/hhQCfQOhF3uVpb4yOAiuS61VAwFbaRoWSOxPZsRhEwFIW1CsF2hRSqmzdrjgHwLL2O3oD0ZM=" />
</div><script src="/js/dnn.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.xmlhttp.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.controls.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.modalpopup.js?cdv=581" type="text/javascript"></script><script src="/js/dnncore.js?cdv=581" type="text/javascript"></script><script src="/Resources/Search/SearchSkinObjectPreview.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.servicesframework.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.dom.positioning.js?cdv=581" type="text/javascript"></script><script src="/js/dnn.controls.dnnmenu.js?cdv=581" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        
<script type="text/javascript" src="/Portals/0/Skins/Flex2-V-Round-Stripe/swfobject.js"></script>
<script type="text/javascript">
var params = { wmode: "transparent", FlashVars: "XMLpath=/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.images.xml" };
swfobject.embedSWF("/Portals/0/Skins/Flex2-V-Round-Stripe/flash/flex-900x112.swf", "FlashBannerContainer", "900", "112", "9.0.0", false, false, params);
</script>

<script type="text/javascript" src='/Portals/0/Skins/Flex2-V-Round-Stripe/drnuke-height.js'></script>
<!--[if lte ie 6]>
<script type="text/javascript">
if (typeof blankImg == 'undefined') var blankImg = '/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif';
</script>
<style type="text/css">
.trans-png, #Body, .flex-container-visibility a img { behavior: url(/Portals/0/Skins/Flex2-V-Round-Stripe/drnuke-png.htc) }
</style>
<![endif]-->
<!--[if gte IE 5]>
<link rel="stylesheet" type="text/css" media="all" href="/Portals/0/Skins/Flex2-V-Round-Stripe/css/ie.css">
<![endif]-->  
<!--[if ie 6]>
<link rel="stylesheet" type="text/css" media="all" href="/Portals/0/Skins/Flex2-V-Round-Stripe/css/ie6.css">
<![endif]-->

<div id="OuterContainer" class="EMWidth">   

<table class="EMSkinTable fullwidth" border="0" cellspacing="0" cellpadding="0">
<tr id="EMOffset1"><td class="EMSkinTL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td style="vertical-align:bottom;">
	<table class="fullwidth" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="EMSkinTL2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    <td class="EMSkinT trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
	<td class="EMSkinTR2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    </tr>
    </table>
</td>
<td class="EMSkinTR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td></tr>
<tr>
<td class="EMSkinL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td class="EMSkinM">
	<div id="ContentHeightContainer">
	<div id="InnerContainer">
    	<div style="float:left;"> 
            <div id="LogoContainer" class="EMLogoContainer">
                <a id="dnn_dnnLOGO_hypLogo" title="Agencija za privredne registre (APR)" aria-label="Agencija za privredne registre (APR)" href="http://apr.gov.rs/%d0%9d%d0%b0%d1%81%d0%bb%d0%be%d0%b2%d0%bd%d0%b0.aspx"></a>
            </div>
        </div>
        <div style="float:right;">
            <div id="LogoRightContainer">
                <div id="DateContainer" class="EMDateContainer">
                <span id="dnn_dnnCURRENTDATE_lblDate" class="DateToken EMFontFamily">понедељак 03. септембар  2018.</span>

                </div>
                
                <div id="LanguageContainer" class="EMLanguageContainer">
                <div class="language-object" >


</div>
                </div>            
            </div>
        </div>
        <div class="clear"></div>
        
        <div id="MenuBottomContainer">
        <table cellpadding="0" cellspacing="0" border="0" class="fullwidth">
        <tr><td id="MenuBarL" class="EMBaseColour6"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-bl.png" alt="" class="trans-png" /></td><td id="MenuBarM" class="EMBaseColour6"></td><td id="MenuBarR" class="EMBaseColour6"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-br.png" alt="" class="trans-png" /></td></tr>
        </table>
        </div>
          
        <div id="BannerOuterContainer">
            <div id="FlashBannerContainer">
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
            </div> 
        </div>
          
        <div id="UnderBannerContainer">
        <table cellpadding="0" cellspacing="0" class="fullwidth">
        <tr>
        <td><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/login-l.gif" width="9" height="31" alt="" /></td>
        <td class="fullwidth">
            <div id="BreadcrumbContainer" class="EMBreadcrumbContainer">
                <span class="EMBaseColour4 BreadcrumbSpan"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/breadcrumb.png" alt="" class="trans-png" /></span><span id="dnn_dnnBREADCRUMB_lblBreadCrumb" itemprop="breadcrumb" itemscope="" itemtype="https://schema.org/breadcrumb"><span itemscope itemtype="http://schema.org/BreadcrumbList"><span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="http://apr.gov.rs/Насловна.aspx" class="BreadcrumbToken EMFontFamily" itemprop="item"><span itemprop="name">Насловна</span></a><meta itemprop="position" content="1" /></span></span></span>
            </div>
            <div id="LoginContainer">
                <table border="0" cellpadding="0" cellspacing="0">
                <tr><td class="EMBaseColour5 trans-png"><div></div></td></tr>
                </table> 
            </div>
            <div id="UserContainer">
<a href="http://apr.gov.rs/eng/Home.aspx"><strong>English</strong></a>
                <table border="0" cellpadding="0" cellspacing="0">
		
                <tr><td ></td></tr>
                </table> 
            </div>
            
		</td>
        <td><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/login-r.gif" width="9" height="31" alt="" /></td>
        </tr>
        </table>
        </div>

		<div id="ContentContainer">
        <table border="0" cellpadding="0" cellspacing="0" class="fullwidth">
        <tr>
        <td id="MenuContainerCell">
        
            <div id="MenuContainer"><div>
	<ul class="mainmenu-menuitem EMSubMenuItemOff EMMainMenuFont" id="dnn_dnnNAV_ctldnnNAV">
		<li id="dnn_dnnNAV_ctldnnNAVctr39"><a href="http://apr.gov.rs/%d0%9d%d0%b0%d1%81%d0%bb%d0%be%d0%b2%d0%bd%d0%b0.aspx"><span>Насловна</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr113"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8.aspx"><span>О Агенцији</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub113">
			<li id="dnn_dnnNAV_ctldnnNAVctr285"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a0%d0%b5%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d0%b5%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Референце Агенције</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr242"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%9e%d1%81%d0%bd%d0%b8%d0%b2%d0%b0%d1%9a%d0%b5.aspx"><span>Оснивање</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr243"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%9e%d1%80%d0%b3%d0%b0%d0%bd%d0%b8%d0%b7%d0%b0%d1%86%d0%b8%d1%98%d0%b0.aspx"><span>Организација</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr244"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a3%d0%bf%d1%80%d0%b0%d0%b2%d0%bd%d0%b8%d0%be%d0%b4%d0%b1%d0%be%d1%80.aspx"><span>Управни одбор</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr245"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a1%d0%b2%d0%b8%d1%80%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8%d0%bd%d0%b0%d1%98%d0%b5%d0%b4%d0%bd%d0%be%d0%bc%d0%bc%d0%b5%d1%81%d1%82%d1%83.aspx"><span>Сви регистри на једном месту</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr353"><a href="http://apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%98%d0%bd%d1%82%d0%b5%d1%80%d0%bd%d0%b0%d0%b4%d0%be%d0%ba%d1%83%d0%bc%d0%b5%d0%bd%d1%82%d0%b0.aspx"><span>Интерна документа</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr139"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8.aspx"><span>Регистри</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub139">
			<li id="dnn_dnnNAV_ctldnnNAVctr59"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b0%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b0.aspx"><span>Привредна друштва</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr60"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b5%d0%b4%d1%83%d0%b7%d0%b5%d1%82%d0%bd%d0%b8%d1%86%d0%b8.aspx"><span>Предузетници</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr216"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d0%b8%d0%b7%d0%b2%d0%b5%d1%88%d1%82%d0%b0%d1%98%d0%b8.aspx"><span>Финансијски извештаји</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr61"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d0%bb%d0%b8%d0%b7%d0%b8%d0%bd%d0%b3.aspx"><span>Финансијски лизинг</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr62"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%97%d0%b0%d0%bb%d0%be%d0%b6%d0%bd%d0%be%d0%bf%d1%80%d0%b0%d0%b2%d0%be.aspx"><span>Заложно право</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr116"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9c%d0%b5%d0%b4%d0%b8%d1%98%d0%b8.aspx"><span>Медији</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr117"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a3%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9a%d0%b0.aspx"><span>Удружења</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr118"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%82%d1%80%d0%b0%d0%bd%d0%b0%d1%83%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9a%d0%b0.aspx"><span>Страна удружења</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr229"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a2%d1%83%d1%80%d0%b8%d0%b7%d0%b0%d0%bc.aspx"><span>Туризам</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr258"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%82%d0%b5%d1%87%d0%b0%d1%98%d0%bd%d0%b5%d0%bc%d0%b0%d1%81%d0%b5.aspx"><span>Стечајне масе</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr260"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9c%d0%b5%d1%80%d0%b5%d0%b8%d0%bf%d0%be%d0%b4%d1%81%d1%82%d0%b8%d1%86%d0%b0%d1%98%d0%b8%d1%80%d0%b5%d0%b3%d0%b8%d0%be%d0%bd%d0%b0%d0%bb%d0%bd%d0%be%d0%b3%d1%80%d0%b0%d0%b7%d0%b2%d0%be%d1%98%d0%b0.aspx"><span>Мере и подстицаји регионалног развоја</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr262"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b5%d0%ba%d0%be%d0%bc%d0%be%d1%80%d0%b5%d0%b8%d0%bf%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bd%d0%b8%d1%88%d1%82%d0%b2%d0%b0%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%85%d0%bf%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b8%d1%85%d0%ba%d0%be%d0%bc%d0%be%d1%80%d0%b0.aspx"><span>Привредне коморе и представништва страних привредних комора</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr341"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%97%d0%b0%d0%b4%d1%83%d0%b6%d0%b1%d0%b8%d0%bd%d0%b5%d0%b8%d1%84%d0%be%d0%bd%d0%b4%d0%b0%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Задужбине и фондације</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr343"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bd%d0%b8%d1%88%d1%82%d0%b2%d0%b0%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%85%d0%b7%d0%b0%d0%b4%d1%83%d0%b6%d0%b1%d0%b8%d0%bd%d0%b0%d0%b8%d1%84%d0%be%d0%bd%d0%b4%d0%b0%d1%86%d0%b8%d1%98%d0%b0.aspx"><span>Представништва страних задужбина и фондација</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr366"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d0%bf%d0%be%d1%80%d1%82%d1%81%d0%ba%d0%b0%d1%83%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9aa,%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b0%d0%b8%d1%81%d0%b0%d0%b2%d0%b5%d0%b7%d0%b8.aspx"><span>Спортска удружењa, друштва и савези</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr377"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%83%d0%b4%d1%81%d0%ba%d0%b5%d0%b7%d0%b0%d0%b1%d1%80%d0%b0%d0%bd%d0%b5.aspx"><span>Судске забране</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr599"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b0%d0%ba%d1%82%d0%be%d1%80%d0%b8%d0%bd%d0%b3.aspx"><span>Факторинг</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr591"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d0%be%d0%bd%d1%83%d1%92%d0%b0%d1%87%d0%b8.aspx"><span>Понуђачи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr779"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a3%d0%b3%d0%be%d0%b2%d0%be%d1%80%d0%b8%d0%be%d1%84%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%80%d0%b0%d1%9a%d1%83%d0%bf%d0%be%d1%99%d0%be%d0%bf%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b5%d0%bf%d1%80%d0%be%d0%b8%d0%b7%d0%b2%d0%be%d0%b4%d1%9a%d0%b5.aspx"><span>Уговори о финансирању пољопривредне производње</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr828"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%93%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>Грађевинске дозволе</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr885"><a href="http://apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%bc%d0%b5%d0%bd%d0%b0%d0%be%d0%b3%d1%80%d0%b0%d0%bd%d0%b8%d1%87%d0%b5%d1%9a%d0%b0%d0%bf%d1%80%d0%b0%d0%b2%d0%b0%d0%bb%d0%b8%d1%86%d0%b0.aspx"><span>Привремена ограничења права лица</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr64"><a href="http://apr.gov.rs/%d0%9f%d1%80%d0%be%d0%bf%d0%b8%d1%81%d0%b8.aspx"><span>Прописи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr132"><a href="http://apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5.aspx"><span>Услуге</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub132">
			<li id="dnn_dnnNAV_ctldnnNAVctr427"><a href="http://apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d0%bf%d0%be%d0%b4%d0%b0%d1%82%d0%b0%d0%ba%d0%b0%d0%b1%d0%b5%d0%b7%d0%bd%d0%b0%d0%ba%d0%bd%d0%b0%d0%b4%d0%b5.aspx"><span>Достављање података без накнаде</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr509"><a href="http://apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d0%bf%d0%be%d0%b4%d0%b0%d1%82%d0%b0%d0%ba%d0%b0%d1%83%d0%b7%d0%bd%d0%b0%d0%ba%d0%bd%d0%b0%d0%b4%d1%83.aspx"><span>Достављање података уз накнаду</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr133"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83.aspx"><span>Односи с јавношћу</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub133">
			<li id="dnn_dnnNAV_ctldnnNAVctr68"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%92%d0%b5%d1%81%d1%82%d0%b8.aspx"><span>Вести</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr310"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9f%d1%80%d0%b5%d1%81%d1%81%d0%bb%d1%83%d0%b6%d0%b1%d0%b0.aspx"><span>Прес служба</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr66"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9c%d0%b5%d1%92%d1%83%d0%bd%d0%b0%d1%80%d0%be%d0%b4%d0%bd%d0%b8%d0%be%d0%b4%d0%bd%d0%be%d1%81%d0%b8.aspx"><span>Међународни односи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr67"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9a%d0%be%d0%bd%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Конференције</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr328"><a href="http://apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%92%d0%b8%d0%b4%d0%b5%d0%be%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b8%d1%98%d0%b0.aspx"><span>Видео галерија</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr140"><a href="http://apr.gov.rs/%d0%90%d1%80%d1%85%d0%b8%d0%b2%d0%b0.aspx"><span>Архива</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr72"><a href="http://apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.aspx"><span>Инфо центар и контакти</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub72">
			<li id="dnn_dnnNAV_ctldnnNAVctr228"><a href="http://apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80.aspx"><span>Инфо центар</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr715"><a href="http://apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%9a%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.aspx"><span>Контакти</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr832"><a href="http://apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%9f%d0%be%d0%b7%d0%b8%d0%b2%d0%bd%d0%b8%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b7%d0%b0%d0%b3%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>Позивни центар за грађевинске дозволе</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr322"><a href="http://apr.gov.rs/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%90%d0%9f%d0%a0.aspx"><span>Рачуни АПР</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub322">
			<li id="dnn_dnnNAV_ctldnnNAVctr944"><a href="http://apr.gov.rs/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%90%d0%9f%d0%a0/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%b7%d0%b0%d1%83%d0%bf%d0%bb%d0%b0%d1%82%d0%b5.aspx"><span>Рачуни за уплате</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr943"><a href="http://apr.gov.rs/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%90%d0%9f%d0%a0/%d0%9f%d0%be%d0%b2%d1%80%d0%b0%d1%9b%d0%b0%d1%98%d0%b2%d0%b8%d1%88%d0%ba%d0%b0%d1%83%d0%bf%d0%bb%d0%b0%d1%9b%d0%b5%d0%bd%d0%b8%d1%85%d1%81%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%b0.aspx"><span>Повраћај вишка уплаћених средстава</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr550"><a href="http://apr.gov.rs/%d0%88%d0%b0%d0%b2%d0%bd%d0%b5%d0%bd%d0%b0%d0%b1%d0%b0%d0%b2%d0%ba%d0%b5.aspx"><span>Јавне набавке</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr631"><a href="http://apr.gov.rs/%d0%92%d0%b8%d0%b4%d0%b5%d0%be%d1%83%d0%bf%d1%83%d1%82%d1%81%d1%82%d0%b2%d0%b0.aspx"><span>Видео упутства</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr954"><a href="http://apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5.aspx"><span>еУслуге</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub954">
			<li id="dnn_dnnNAV_ctldnnNAVctr960"><a href="http://apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%b5%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b0%d1%86%d0%b8%d1%98%d0%b0%d0%be%d1%81%d0%bd%d0%b8%d0%b2%d0%b0%d1%9a%d0%b0%d0%bf%d1%80%d0%b5%d0%b4%d1%83%d0%b7%d0%b5%d1%82%d0%bd%d0%b8%d0%ba%d0%b0.aspx"><span>еРегистрација оснивања предузетника</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr961"><a href="http://apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%b5%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d1%84%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d1%85%d0%b8%d0%b7%d0%b2%d0%b5%d1%88%d1%82%d0%b0%d1%98%d0%b0.aspx"><span>еДостављање финансијских извештаја</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr962"><a href="http://apr.gov.rs/%d0%b5%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%b5%d0%93%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>еГрађевинске дозволе</span></a></li>
		</ul></li>
	</ul>
</div></div>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_MenuPane" class="MenuPane DNNEmptyPane" valign="top"></td>

            </tr>
            </table>
            <div id="SearchContainer" class="EMSearchContainer">
                    <div class="SearchBoxL">
                        <div class="SearchBox EMBaseColour3">
                        <span id="dnn_dnnSEARCH_ClassicSearch">
    
    
    <span class="searchInputContainer" data-moreresults="See More Results" data-noresult="No Results Found">
        <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" aria-label="Search" autocomplete="off" placeholder="Search..." />
        <a class="dnnSearchBoxClearText" title="Clear search text"></a>
    </span>
    <a id="dnn_dnnSEARCH_cmdSearch" class="SkinObject" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/search-button.png" hspace="0" alt="Search" class="trans-png" /></a>
</span>


<script type="text/javascript">
    $(function() {
        if (typeof dnn != "undefined" && typeof dnn.searchSkinObject != "undefined") {
            var searchSkinObject = new dnn.searchSkinObject({
                delayTriggerAutoSearch : 400,
                minCharRequiredTriggerAutoSearch : 2,
                searchType: 'S',
                enableWildSearch: true,
                cultureCode: 'sr-Cyrl-CS',
                portalId: -1
                }
            );
            searchSkinObject.init();
            
            
            // attach classic search
            var siteBtn = $('#dnn_dnnSEARCH_SiteRadioButton');
            var webBtn = $('#dnn_dnnSEARCH_WebRadioButton');
            var clickHandler = function() {
                if (siteBtn.is(':checked')) searchSkinObject.settings.searchType = 'S';
                else searchSkinObject.settings.searchType = 'W';
            };
            siteBtn.on('change', clickHandler);
            webBtn.on('change', clickHandler);
            
            
        }
    });
</script>

                        </div>
                    </div>         
                </div>
        </td>
        <td id="ContentContainerCell">
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_TopPane" colspan="2" class="TopPane DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane" class="LeftPane DNNEmptyPane"></td>

            <td id="dnn_RightPane" class="RightPane DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_TopPane2" colspan="3" class="TopPane2 DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane2" class="LeftPane2 DNNEmptyPane"></td>

            <td id="dnn_ContentPane1" class="ContentPane1 DNNEmptyPane"></td>

            <td id="dnn_RightPane2" class="RightPane2 DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_ContentPane" colspan="2" class="ContentPane"><div class="DnnModule DnnModule- DnnModule--1 DnnModule-Admin">
<table class="flex-container-1 fullwidth">
<tr class="EMContainerColour8">
<td class="flex-container-tl-simple trans-png"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="33" width="14" alt="" /></td>
<td class="flex-container-t-simple trans-png">
	
    <div class="flex-container-title"><h1 class="EMContainerTitleFontSize8"><span id="dnn_ctr_dnnTITLE_titleLabel" class="EMContainerTitleFontColour8 EMContainerTitleFontFamily8 EMContainerTitleFontSize8">Privacy Statement</span>


</h1></div>
    <div class="flex-container-visibility"></div>
    <div class="flex-container-help"></div>
</td>
<td class="flex-container-tr-simple trans-png"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="33" width="14" alt="" /></td>
</tr>
<tr>
  <td class="flex-container-l"></td>
  <td class="flex-container-m">
    <table class="flex-container-m-table fullwidth">
    <tr><td id="dnn_ctr_ContentPane" class="flex-container-m-td flex-container-content"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Agencija za privredne registre (APR) is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Agencija za privredne registre (APR) site and governs data collection and usage.
        By using the Agencija za privredne registre (APR) site, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) collects personally identifiable information, such as your email
        address, name, home or work address or telephone number. Agencija za privredne registre (APR) also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Agencija za privredne registre (APR). This information can include: your IP address,
        browser type, domain names, access times and referring website addresses. This
        information is used by Agencija za privredne registre (APR) for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Agencija za privredne registre (APR) site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Agencija za privredne registre (APR) public message boards,
        this information may be collected and used by others. Note: Agencija za privredne registre (APR)
        does not read any of your private online communications.</p>
        <p>Agencija za privredne registre (APR) encourages you to review the privacy statements of Web sites
        you choose to link to from Agencija za privredne registre (APR) so that you can understand how those
        Web sites collect, use and share your information. Agencija za privredne registre (APR) is not responsible
        for the privacy statements or other content on Web sites outside of the Agencija za privredne registre (APR)
        and Agencija za privredne registre (APR) family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) collects and uses your personal information to operate the Agencija za privredne registre (APR)
        Web site and deliver the services you have requested. Agencija za privredne registre (APR) also uses
        your personally identifiable information to inform you of other products or services
        available from Agencija za privredne registre (APR) and its affiliates. Agencija za privredne registre (APR) may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Agencija za privredne registre (APR) does not sell, rent or lease its customer lists to third parties.
        Agencija za privredne registre (APR) may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Agencija za privredne registre (APR)
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Agencija za privredne registre (APR), and they are required to maintain
        the confidentiality of your information.</p>
        <p>Agencija za privredne registre (APR) does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Agencija za privredne registre (APR) keeps track of the Web sites and pages our customers visit within
        Agencija za privredne registre (APR), in order to determine what Agencija za privredne registre (APR) services are
        the most popular. This data is used to deliver customized content and advertising
        within Agencija za privredne registre (APR) to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Agencija za privredne registre (APR) Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Agencija za privredne registre (APR) or the site; (b) protect and defend the rights or
        property of Agencija za privredne registre (APR); and, (c) act under exigent circumstances to protect
        the personal safety of users of Agencija za privredne registre (APR), or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Agencija za privredne registre (APR) Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Agencija za privredne registre (APR) pages, or
        register with Agencija za privredne registre (APR) site or services, a cookie helps Agencija za privredne registre (APR)
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Agencija za privredne registre (APR) Web site, the information
        you previously provided can be retrieved, so you can easily use the Agencija za privredne registre (APR)
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Agencija za privredne registre (APR) services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) secures your personal information from unauthorized access,
        use or disclosure. Agencija za privredne registre (APR) secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Agencija za privredne registre (APR) will occasionally update this Statement of Privacy to reflect
company and customer feedback. Agencija za privredne registre (APR) encourages you to periodically 
        review this Statement to be informed of how Agencija za privredne registre (APR) is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Agencija za privredne registre (APR) welcomes your comments regarding this Statement of Privacy.
        If you believe that Agencija za privredne registre (APR) has not adhered to this Statement, please
        contact Agencija za privredne registre (APR) at <a href="mailto:srogic@apr.gov.rs">srogic@apr.gov.rs</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></td>
</tr>
    <tr><td class="flex-container-m-td">
    	<div class="flex-container-action2"></div>
        <div class="flex-container-syndicate"></div>
        <div class="flex-container-print"></div>
        <div class="flex-container-settings"></div>
    </td></tr>    
    </table>
  </td>
  <td class="flex-container-r"></td>
</tr>
<tr><td class="flex-container-bl"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="12" width="14" alt="" /></td><td class="flex-container-b"></td><td class="flex-container-br"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="12" width="14" alt="" /></td></tr>
</table>
<div class="clear cont-br"></div>







</div></td>

            </tr>                
            <tr>
            <td id="dnn_ContentPane2" class="ContentPane2 DNNEmptyPane"></td>

            <td id="dnn_RightPane3" class="RightPane3 DNNEmptyPane"></td>

            </tr> 
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">        
            <tr>
            <td id="dnn_MiddlePane" colspan="3" class="MiddlePane DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane3" class="LeftPane3 DNNEmptyPane"></td>

            <td id="dnn_ContentPane3" class="ContentPane3 DNNEmptyPane"></td>

            </tr> 
            </table>  
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_BottomPane" colspan="3" class="BottomPane DNNEmptyPane"></td>

            </tr>              
            <tr>
            <td id="dnn_LeftPane4" class="LeftPane4 DNNEmptyPane"></td>

            <td id="dnn_ContentPane4" class="ContentPane4 DNNEmptyPane"></td>

            <td id="dnn_RightPane4" class="RightPane4 DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_BottomPane2" class="BottomPane2 DNNEmptyPane"></td>

            </tr>
            </table>
        </td></tr>
        </table>
        
        </div>
      </div>
      </div>        
</td>        
<td class="EMSkinR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset2">
<td class="EMSkinL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td class="EMSkinM">
    <div id="FooterContainer">
    <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
    <tr>
    <td id="dnn_FooterPane" class="FooterPane DNNEmptyPane" valign="top"></td>

    </tr>
    </table>
    </div>
</td>
<td class="EMSkinR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset3">
<td class="EMSkinBL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td>
	<table class="fullwidth" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="EMSkinBL2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    <td class="EMSkinB trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
	<td class="EMSkinBR2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    </tr>
    </table>
</td>
<td class="EMSkinBR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset4">
<td class="EMSkinBL3 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td id="FooterCell" class="EMSkinB3">
    <div id="CopyrightContainer" class="EMCopyrightContainer"><span id="dnn_dnnCOPYRIGHT_lblCopyright" class="FooterToken EMFooterFont">Copyright 2009 APR</span>
</div>
    <div id="TermsContainer" class="EMTermsContainer"><a id="dnn_dnnTERMS_hypTerms" class="FooterToken EMFooterFont" rel="nofollow" href="http://apr.gov.rs/terms.aspx">Terms Of Use</a></div>
    <div id="PrivacyContainer" class="EMPrivacyContainer"><a id="dnn_dnnPRIVACY_hypPrivacy" class="FooterToken EMFooterFont" rel="nofollow" href="http://apr.gov.rs/privacy.aspx">Privacy Statement</a></div> 
</td>
<td class="EMSkinBR3 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
</table>

</div>


        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" value="`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`39`,`dnn_dnnNAV_ctldnnNAV_json`:`{nodes:[{bcrumb:\`1\`,selected:\`1\`,id:\`39\`,key:\`39\`,txt:\`Насловна\`,ca:\`3\`,url:\`http://apr.gov.rs/Насловна.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-breadcrumbactive EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,cssSel:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`113\`,key:\`113\`,txt:\`О Агенцији\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`285\`,key:\`285\`,txt:\`Референце Агенције\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/РеференцеАгенције.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`242\`,key:\`242\`,txt:\`Оснивање\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Оснивање.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`243\`,key:\`243\`,txt:\`Организација\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Организација.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`244\`,key:\`244\`,txt:\`Управни одбор\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Управниодбор.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`245\`,key:\`245\`,txt:\`Сви регистри на једном месту\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Свирегистринаједномместу.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`353\`,key:\`353\`,txt:\`Интерна документа\`,ca:\`3\`,url:\`http://apr.gov.rs/ОАгенцији/Интернадокумента.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`139\`,key:\`139\`,txt:\`Регистри\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`59\`,key:\`59\`,txt:\`Привредна друштва\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Привреднадруштва.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`60\`,key:\`60\`,txt:\`Предузетници\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Предузетници.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`216\`,key:\`216\`,txt:\`Финансијски извештаји\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Финансијскиизвештаји.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`61\`,key:\`61\`,txt:\`Финансијски лизинг\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Финансијскилизинг.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`62\`,key:\`62\`,txt:\`Заложно право\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Заложноправо.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`116\`,key:\`116\`,txt:\`Медији\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Медији.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`117\`,key:\`117\`,txt:\`Удружења\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Удружења.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`118\`,key:\`118\`,txt:\`Страна удружења\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Странаудружења.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`229\`,key:\`229\`,txt:\`Туризам\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Туризам.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`258\`,key:\`258\`,txt:\`Стечајне масе\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Стечајнемасе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`260\`,key:\`260\`,txt:\`Мере и подстицаји регионалног развоја\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Мереиподстицајирегионалногразвоја.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`262\`,key:\`262\`,txt:\`Привредне коморе и представништва страних привредних комора\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Привреднекомореипредставништвастранихпривреднихкомора.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`341\`,key:\`341\`,txt:\`Задужбине и фондације\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Задужбинеифондације.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`343\`,key:\`343\`,txt:\`Представништва страних задужбина и фондација\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Представништвастранихзадужбинаифондација.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`366\`,key:\`366\`,txt:\`Спортска удружењa, друштва и савези\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Спортскаудружењa,друштваисавези.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`377\`,key:\`377\`,txt:\`Судске забране\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Судскезабране.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`599\`,key:\`599\`,txt:\`Факторинг\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Факторинг.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`591\`,key:\`591\`,txt:\`Понуђачи\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Понуђачи.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`779\`,key:\`779\`,txt:\`Уговори о финансирању пољопривредне производње\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Уговориофинансирањупољопривреднепроизводње.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`828\`,key:\`828\`,txt:\`Грађевинске дозволе\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Грађевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`885\`,key:\`885\`,txt:\`Привремена ограничења права лица\`,ca:\`3\`,url:\`http://apr.gov.rs/Регистри/Привременаограничењаправалица.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`64\`,key:\`64\`,txt:\`Прописи\`,ca:\`3\`,url:\`http://apr.gov.rs/Прописи.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`132\`,key:\`132\`,txt:\`Услуге\`,ca:\`3\`,url:\`http://apr.gov.rs/Услуге.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`427\`,key:\`427\`,txt:\`Достављање података без накнаде\`,ca:\`3\`,url:\`http://apr.gov.rs/Услуге/Достављањеподатакабезнакнаде.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`509\`,key:\`509\`,txt:\`Достављање података уз накнаду\`,ca:\`3\`,url:\`http://apr.gov.rs/Услуге/Достављањеподатакаузнакнаду.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`133\`,key:\`133\`,txt:\`Односи с јавношћу\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`68\`,key:\`68\`,txt:\`Вести\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Вести.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`310\`,key:\`310\`,txt:\`Прес служба\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Пресслужба.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`66\`,key:\`66\`,txt:\`Међународни односи\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Међународниодноси.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`67\`,key:\`67\`,txt:\`Конференције\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Конференције.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`328\`,key:\`328\`,txt:\`Видео галерија\`,ca:\`3\`,url:\`http://apr.gov.rs/Односисјавношћу/Видеогалерија.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`140\`,key:\`140\`,txt:\`Архива\`,ca:\`3\`,url:\`http://apr.gov.rs/Архива.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`72\`,key:\`72\`,txt:\`Инфо центар и контакти\`,ca:\`3\`,url:\`http://apr.gov.rs/Инфоцентариконтакти.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`228\`,key:\`228\`,txt:\`Инфо центар\`,ca:\`3\`,url:\`http://apr.gov.rs/Инфоцентариконтакти/Инфоцентар.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`715\`,key:\`715\`,txt:\`Контакти\`,ca:\`3\`,url:\`http://apr.gov.rs/Инфоцентариконтакти/Контакти.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`832\`,key:\`832\`,txt:\`Позивни центар за грађевинске дозволе\`,ca:\`3\`,url:\`http://apr.gov.rs/Инфоцентариконтакти/Позивницентарзаграђевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`322\`,key:\`322\`,txt:\`Рачуни АПР\`,ca:\`3\`,url:\`http://apr.gov.rs/РачуниАПР.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`944\`,key:\`944\`,txt:\`Рачуни за уплате\`,ca:\`3\`,url:\`http://apr.gov.rs/РачуниАПР/Рачунизауплате.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`943\`,key:\`943\`,txt:\`Повраћај вишка уплаћених средстава\`,ca:\`3\`,url:\`http://apr.gov.rs/РачуниАПР/Повраћајвишкауплаћенихсредстава.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`550\`,key:\`550\`,txt:\`Јавне набавке\`,ca:\`3\`,url:\`http://apr.gov.rs/Јавненабавке.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`631\`,key:\`631\`,txt:\`Видео упутства\`,ca:\`3\`,url:\`http://apr.gov.rs/Видеоупутства.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`954\`,key:\`954\`,txt:\`еУслуге\`,ca:\`3\`,url:\`http://apr.gov.rs/еУслуге.aspx\`,rhtml:\`\\u003cspan class=\\\`item-right\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,lhtml:\`\\u003cspan class=\\\`item-left\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\\u003cspan class=\\\`item-arrow\\\`\\u003e\\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\\u003e\\u003c/span\\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`960\`,key:\`960\`,txt:\`еРегистрација оснивања предузетника\`,ca:\`3\`,url:\`http://apr.gov.rs/еУслуге/еРегистрацијаоснивањапредузетника.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`961\`,key:\`961\`,txt:\`еДостављање финансијских извештаја\`,ca:\`3\`,url:\`http://apr.gov.rs/еУслуге/еДостављањефинансијскихизвештаја.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`962\`,key:\`962\`,txt:\`еГрађевинске дозволе\`,ca:\`3\`,url:\`http://apr.gov.rs/еУслуге/еГрађевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]}]}`}" />
        <input name="__RequestVerificationToken" type="hidden" value="2xytTVDoZAmT1cwpEuIvH767Ehr0nIpgsY6PC3MY0ejItzPhehjHlitq21JG4b-vAixWzA2" />
    
<script type="text/javascript">dnn.setVar('dnn_dnnNAV_ctldnnNAV_p', '{rarrowimg:\'/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png\',sysimgpath:\'/images/\',carrowimg:\'/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png\',orient:\'1\',rmode:\'2\',css:\'mainmenu-menuitem EMSubMenuItemOff EMMainMenuFont\',postback:\'__doPostBack(\\\'dnn$dnnNAV$ctldnnNAV\\\',\\\'[NODEID]~|~Click\\\')\',mcss:\'mainmenu-submenu EMSubMenuItemBGOff EMMainMenuFont EMSubMenuOpacity\',easeDir:\'0\'}');dnn.controls.initMenu($get('dnn_dnnNAV_ctldnnNAV'));</script></form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=581)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=581)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=581)--><!--CDF(Css|/Portals/0/Skins/Flex2-V-Round-Stripe/skin.css?cdv=581)--><!--CDF(Css|/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.css?cdv=581)--><!--CDF(Css|/Portals/0/Containers/Flex2-Set8/container.css?cdv=581)--><!--CDF(Css|/Portals/0/portal.css?cdv=581)--><!--CDF(Css|/Resources/Search/SearchSkinObjectPreview.css?cdv=581)--><!--CDF(Javascript|/Resources/Search/SearchSkinObjectPreview.js?cdv=581)--><!--CDF(Javascript|/js/dnn.js?cdv=581)--><!--CDF(Javascript|/js/dnn.servicesframework.js?cdv=581)--><!--CDF(Javascript|/js/dnn.dom.positioning.js?cdv=581)--><!--CDF(Javascript|/js/dnn.xmlhttp.js?cdv=581)--><!--CDF(Javascript|/js/dnn.controls.js?cdv=581)--><!--CDF(Javascript|/js/dnn.controls.js?cdv=581)--><!--CDF(Javascript|/js/dnn.controls.dnnmenu.js?cdv=581)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=581)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=581)-->
    
</body>
</html>