<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xml:lang="sr-Cyrl-CS" lang="sr-Cyrl-CS" xmlns="http://www.w3.org/1999/xhtml">
<head id="Head">
<!--*********************************************-->
<!-- DNN Platform - http://www.dnnsoftware.com   -->
<!-- Copyright (c) 2002-2015, by DNN Corporation -->
<!--*********************************************-->
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	Агенција за привредне регистре
</title><meta id="MetaDescription" name="DESCRIPTION" content="Насловна страница" /><meta id="MetaKeywords" name="KEYWORDS" content="АПР, Агенција за привредне регистре, Предузетници, Привредна друштва, Лизинг, Заложно право, Залога, Јавна гласила, Удружења, Страна удружења, Туризам, Финансијски извештаји, Бонитет, Регистрација, Регистар,DotNetNuke,DNN" /><meta id="MetaGenerator" name="GENERATOR" content="DotNetNuke " /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Portals/_default/default.css?cdv=440" media="all" type="text/css" rel="stylesheet"/><link href="/Resources/Search/SearchSkinObjectPreview.css?cdv=440" media="all" type="text/css" rel="stylesheet"/><link href="/Portals/0/Skins/Flex2-V-Round-Stripe/skin.css?cdv=440" media="all" type="text/css" rel="stylesheet"/><link href="/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.css?cdv=440" media="all" type="text/css" rel="stylesheet"/><link href="/Portals/0/Containers/Flex2-Set8/container.css?cdv=440" media="all" type="text/css" rel="stylesheet"/><link href="/Portals/0/portal.css?cdv=440" media="all" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=440" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=440" type="text/javascript"></script>     
        
			    <script type="text/javascript">
			      var _gaq = _gaq || [];
			      _gaq.push(['_setAccount', 'UA-6906308-2']);
			      _gaq.push(['_trackPageview']);
			 
			      (function() {
				    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			      })();
			    </script>
        
		  <link rel='SHORTCUT ICON' href='/Portals/0/favico/favicon.ico?ver=2011-09-19-155652-890' type='image/x-icon' /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="dt5spc5ymR2cfTag9wOigRBXQEN/3pA0PSTui29T4rMF4ioftNDhYFypE+4K2ftvqB5pf8JYoD/W9okOU64Uu4oFTkZRcOqiQ3djmLqPtK0nVAWZkME2HuRV8GGJKYx40IJrn9LzO1sOBLpVLZdQJChtetS6qBFQ/qdNjO53N2TK1caFytmyrXB4M4Slue/errJLee+nHN6Gz1lMK7OAkmlQqudmEG8baxdDUGc4zNpiImNVMDoSBL/mNUgIMQk9f5X+6EZL27GF1LDRrEncQsZBLcQaPwgCJ+IhzAKS9hkgnV78inub8FtJ+j7UvXpDnNvfl3otrhl6pqeVC+eRsDq50r39siSr6m8pfx5XhcViGMWlBLz3uWoJYEcs8b1DAR61DemLHI2ZFZX7C76TlhaD2fPlUC+jSj8HNzVBmp7K4lTjW90nu/tjefGX/Ub1uGaxBeBPagURflCu01cLMTBBPEEtgbSFMEM8+mBgMC8nmlcR6Izz62KJeEDyc28yBPe2M/9eikDqpqAxB4VJQjYB8tvkCTfk4ldVyrGpy9l58BzkipPnFRiEx4Im6O3TWQBmgfeVq5JZ940qNy7ia5GSBMPrAJFlE5SE8GyDIUPL54qetGZrLnQuZPgwKiHqNZk7KDlI48Y7tUJ3LRDQ5W3+GgmrANQKO5i2Vmf82GolAzO6yui/341L7ioMZSJJS7BLkQMGrOKWDsrBNoII1bTDVpfP1LVt9YWZgsy/NcSTDqFHXulCa6Ph2/F1BVip0iGnC7BnoRxuNCeUCJTsZW5iY04oOZi6RQnz7WwiWYOJEmN35L7TuIwc6L6S5l2xvUP3Q8+76iUYtcRFs1XKNXcp1wdlcNMRg398mBiB1UcsgwBP6ygefVvLVT97SpEcKbBVIRX72+qaIqnqmcIcLQkjl59HaBqS6DjvEWIW+iGD+2MMKZNqr0zDEXy7ZnbLMYmN/a5Z8dxOPePwqGDd9ewp6TUnbzPzof5+0ZpCWDuGeOjOlr3DMx1wfZOrvXP1npYmoV8wzRnbtLb5GAMWYKu+NzfebyvtNqgx0lEEb+TBJO9pOKSBqOh/wAHgsFK4kNUKPsTqL6J7orKZzhNRE5/qY0+PiiTlc/TLPdwarG7ZVzLhHhgxifZs7bOoBC4+yUO5SBgR2AbL6a6wP+5hAz0qDh+oAHWKDqOgKnp5a8DQB6cgOGZsMcFxb4yRSs4UusnqlfKuz+zD0Coc7S1vipqbsJB5hgdqDEUIPkWyxXnwzAZH+dj0cVpOG6DN1VJvgZN6guFAVtY951cE50S33i4roY/QI4ef0CKjz3m3fJs28xBsqLdF426IdNLEQWSV5ib0gTtQWmV251qo4vKoDO7mILjtsSvII0M0w0Ok2Hcjdo90XtqYn7ic5NyY2RK5q+kumyjkg7W6olNmCKRK90y8j5UkZ5MkEDrto7NzECi9f/aY+/QvNxBLk0Y0qjlnL5lYr0v/VOdl2xRzQa3/8zmmgkr9p+T/V7BMq9efw19GGHYjItaS69t3wRaUXHzvIaeLUbQYVsa7Tjztlnxin9EufG6nYtonM4YqZkL+rnhQKC2KHzg4u+QZyhQT//GrlK2HgH3A2te0etxk90wd99Bp7hspIJxfR5Aceza9rvCi74BqRBCKB6JFCt57PCXMY1qSi6EaS2fk7IHDohhA33X0Fm4w8slqTiUsBSqihGblxwIgZe0S+xU/6WUgHbNYGuVI7RoD3h/u2cKM/rV9mCXt10bmsfJq9uXfAqyCAO+GVrbZvla0brgn52jsY1dwVgTvgdcLyLKgscChSyVWCbSFUJVN7sp+3qqx+1q6VeO3xfIgHN2z6yHtruEs7TpPxACxTCPWK2CHYJBno+Pxji4o1kM4EZX2RtW6ymo1f/BfsyNG3SXWPqA9rYBwSSScfo2c4eLiV91FFr4dVecxfuxHt9+n3CbW9MyhJIGRQnF5k0656L9QlglCkckvhKj6zbi70R+iBDeSbOuLrCBp9WXCGDQVxSYTvGGem3vDZ0PIPPY+yGhddGk756qC5hdRhALn1GUADPlxgc3u2M5MN4WNf3kMdq6mj66EKLT6s9Mm52LYO1DzKA5a2vlc0UxE6dPTl96G5KsiNQvHndOuvSXxaI8av3IpnuH4/4ZvGiwL0Zkgx5CdTqhXuHqbw3GeqIOx6RQAMlkyr6TPUgNqGYbcNKjExLU3mauBpPkJfk1PExiUCVSYD/OI4HLJ8F6uCC1urPRkZcZxLTQam9cUZpfZEopy3dRR4ELXmwkFUzEFbtNRrEKzbfAZBTPEuD/FL8VGhOC9hN3yqf7p3hWxGbLmFh/i5I8NpJ/xAfMXriaEVXHOBX71O8yy39MwrbzjZ69ZPI3sCTVXLVlEYQRNBlktvw59Mrhme5NbylPuIOMb6ylXFbF4YtX/3/H96E0bYLnFoiAahLiKP6ucez93XsQxCPWtb5ydrC1Ci+87xZNxSs+yN5qPnKHF1Rd+I1CqbKKTH+n1kala42tqf86lnz+IGEpAX3S5hh03nWVm+8SzBN1aN2U1C15u11x4TQpQtKnwEn88Zwc6LVWNGc8BJg5JaiycrSth1Pmw5teLgPSCzE7N7aI13Ar3JuNB39bXQt/VO8ZFo58i2ImnBX1RsUvRuNpp7BIMqJym/e/O4UF0brgkCJPIH3MQzIF+RZgv6mTzrQciY9QJ9/PijMG4P+O4MZjOkqNb+YXJMvMW9CzAy2gKRWW8muPpDmtYpmWUlyqqqel7gRLQlZt5pb7HaHIp1ztlHR0bkay1ZzEYABkclmp4EwWq9LI0uig4ExMfAFPdnEXooViZmrp2udl+HRQetrhlIHGaRmJlAlLYpQwbmbY+JHDLDbVHTjwf/2besUEQYC0/0SUnIMd+SIXvoEVaMRC1zdRdVa+PmjkLIGKtP9g+G/KYWqEfEeVw0GmPIgrfjsAJOPVs4aWmjpjsahwz3UY0oI3BhnWMMy5GnO7lqZZggqQAxctZJYRmON6rocsdKzdM/wt4mS5OEU4s2myMj24BoB3mNKLD7H5eh7pxVvY3W6vNepfgpeK99qoFV6IZz6FAnKHFy/vZiaOmiAioUceXZAtiMi6sIC4uYW8lNhWo9Ty4XEwU67iCCnLG66Q9U5wpTFTXO1OeggV68bxEAk6lYyfTUJvbzAA6KevEEeRTfQSCRuP/+0yk3g1SqVrSmqgyv86aSpSjFqdjGPxJ9YH50Rd46QQKtofglEh832he2TedZaSHBOn7mKJN6GxOLKQBDaGUrt8yyYlreMF2VlRjMzJ88Np6Gx30qfb4rWao2DAkOF80XnEs7F3YNLqNePc05DjDdUlatpockovP07RDSHJ5a+sCDNnvQt8R8AvzoTsOTMpFxpavh7LFiVt6nortjQwKeRAVd821hKVXML7TMmQ+BStRThUwgyRN6DfD+vH+wm0cds1XSn/BQXTbX0jpwSjEr892+Ug95VWs5DnHfOq3aJvxgYUAKCsmRbnmk5isfDAKU1d1o0WtWJlXeAdWBHjFwz1iM5v25hJy2Cv1O6WMOsxOPwQighAoyhUTUTYFpTdJaQXXmBRzZS+rBV4tyT1J1DOxwINe77EmCXXiqVB91BohL/cCMzDO+XK3Afe4CWPq2Me66MicQBqwNCdO0uLNjt+ptPW5DQnBJyBK05zlBvdKRbr/8eiWhQb+564rPxAVd3e0eWRWmt6mJrxh7nijShsDdVOVB9zH4nedQKNsXpZig6cO1bc1ex6NJPo6d6/os7yUGn746PmBg6H1sNNXkiLHsNsQwxS/WWHFlZo0GgqqOxZYS3eXUFR9Se3Kv/waoydFnOPBPJzo39zMoCLLxwhNmis+Fgc5hUMVOvIynolTM9reewGNCJsCKudNGhjjixMRRDZMs+uXdVG2DvBZZW0GgylieKrYAhST8yEumQhzO5BrLdRjjIKSMiSehqH/sPM67BAHuXe3ERZ+jzxxhUnOePoAiyHxwebgexmUpO96wj9+XzAfEJ2GM93c+4b7AwDOACMwizamRrg4RMS5KzsU+v8CK0VZPm73eudAqDC5nbJjAmWx73kIcvd1Wbr7vQVxUC3XiY2hu5+D2wrb5a/dXJe5vOrM/fF6v1vOkCgQDyEetMFJLqHnFgwG+vL/kj1sfcl7yuHQoDpcQhMHOdh0GmxmiRBkXx9kw4ZPMP7fAfO4ODUf6KHiJ06kO9HxOG0WYY4/wdahajWy0ThZhiMgGycGXFUfVXtYpmbpd1vTeDsy73qCJbcYcoIoqROUqlgWYupF5Hqb4aN8jrZg7kQ6m5U/8+M8Hz5wN4rYGCwM4N3ZfvTApf4sOrloXFLOAtc5LeUU7n4A0EkY42mT8ut2DVjfdxTIqUEN4MvLEwjLrU1KA5i7BnqYv6RrFldW1ahCIKv2vrjjMQQeHsnTjodFVoKVQA8crLKMAqjXh9WlSXszdYZq481cshZlIKFsT4HfHJjaNGoRbTgUXUK1D6+s+dTRc++DH0hh1nIHpsDTlkIoU1XRynSoXDQXI9XfJ4FmMtePXpM0PYV1RbQV/GMZ11H49ro2IDJN+K888tAM9m118WdRglaeY54rm8JBr4TRclm7frkucuMp2N2Om8St8y8P3vDiEbYr3O94thmuOB8cFgvDMpOCp5s/JsaPhQzEdCax2r6B7t1cNhqXKqeHJqQfgBnUZDFxDNjXEhN4b01txANwzWJ9i0bfrRZlgS/v1TUzobIRQKBPhxNplFG29dfQtUgGup8qfuqES5s45Mxce5N6f4zIr8mVYzu7wAQ7Qo+nUd9oUknxHtTQ9s8Y0vtSFQ9j+vzDKwLNu6rgSpThwbkWzc4kHhvWFPt5JxR83i354g4OR8znyT9F+Ts69bQxZSWFhluAtebSr/cgIJzzmrY7d/Sxv1Anmz7p50x3nKzerDYtv3GX8jmC/4QfmHP3kue7JKgQ9ZB/CEuH2rJKCFC0gGUS8/j/f0++aAViku6C4NHafbp9siJVEN30rbj7UpVmluoCNSpNnEmdekJOEw9QkE2W0SHTPC4SkI5RAG2HpIM1ycqsuxeCjKk/U2nHZv61OdjstMI+3dvaagLOwTEMh+PdTDvSH0DsqqBMsThxZsqscPVIZJdZQvgsS3STPwgUAtRN9gc73zKiGjeZYv/wsZzbvfKL15sTYR4V6TwcgVgJ2j1Pn5jBONczQXgrMuHS92UQ6blYpMnU24eECiBAONxSExfuU7BgFLs04w5V3DYPaOAaLjpP0J6ro3OGJDw5oWxwhbLU9RQCSg63CpFVkxAI3XhbCFN5C9T1hpt8nEnqQFOS99utXFMIj8evz6LETluu2qGwaEl75Znu1MaAdi/tQTrk23NhPXuRz0uNuwZHzNbZFZkFqH0hoiXKMTpb+UKoS1RgoxIB1AhK73BNEEObmUk2RphHDlOwuA4u15sjggjQIZTuODr1kwsIHNy6NzOFJ2PxSUneE+55cbIdbzPBjt7MBn3wMO9DMMQ3SQlrFBErcSfDX/sBeL9UHRXfEnjLvW+pF24+RZJ1WfiNpvnjHRdooxfvjL5oiFTso8eGe8/Sx5R9d0MHCJ0yn+HDnynFK4/ufJvxs3a5wHdENQUA3vWZ8smwUGUQTZzP7vvHPu+D+pD/SnGvdgh9gzf6FNZtJ0hbFeb1MuqdCq+ThmRoCzd88bGd+0cVZv9Zq7KmR2PdOjJ/kC3Qlpwvvct5e9lAt6sh2/X34ZFYcKIrvIUH43+kzCxTobYScztrVI+FGc2LOX1WNEdDUqGC3nxq7Rn23SkjFCF2sFEQjxiIWrHVnY9uyGk97fCad7WDubthpK9Kw5hiEBq7MRwG4zZbuF6J8SfSz2lZVzFNMsbTVW+JK9h2kgNcVWISXV4vwgQzmW4Mha0vkH6t+EpFEc3dBB87sRY32jGDTyhsoTsR+gV8RdK9di4HodQS2BQeQ/3uGixeMAFqAH/yVbVuOYl8jLrzUY2c7tqjplehcBK26loPvjTzWY+m9eBjLJpnLUShJUP4nKw+HMAR9WFjaxtaew3xm0G1CvaCrQ1FmU62VvYSiVk133yzX2TRig7iVmcT9wwubLaipkdXZ71qtu1IMAKLqiWesS4mCIyYKtinN5deKw7jfEeohmhiNa57OM2qokIpP0qmjCKP17xtFt8Et3W9d6kjTgu8/xehULobvTIShF39dat7mJjiIF+yAi8z7cBbT2cOBB/T45LEqMvbFWDHFcnZfC5pK9xuZpuTpIgJBorFQOn6ypjagC0vs8qayCW+unxnc0lpRS4xVJYryvwpKs6zViwJW523OsAf/Ho6c1faTh+a3SkqKQPNMfxe05c7W4j4iLy7tihPEEzp8JSEPp52CZXlTrjePUlakJAgLcPM2rytpiIKIjrXU7yfsCMMkxpPzrhh1paW7r86DdtFNJy78vd7AitQ3/TSF+HtYTylXfNmHjEgDrg6/KAIUZ83gMs4mWgV5nsl5sJrCIVA7Ktt5FclJgGydQf6PzUoQDC0QS6ziv4299h2NzTbacm8DQqnS3Uf5u2wFh/NnW2fv4vvT8z9isOHauZ101MvtzWns7PMa1jBSUY+rEQDI0YzvvdHZi/40V5AtVSXj8xY3j/Iij4CTZSUzZ3AA0wRMZli0LdznGXU9VOZZGJlTHrM/rqlUeoQL+J/bvOpqoO+UKI9WObTbZSXr338pUxuvpvvwoQR8VixYY26YKhWPrjelTfflaF52u4XvizLey0B6Vlpmloy5Xvh7U+1gNiZqx2OMfCSJNY4rypWnQYf71goSGW+4bxpd/q9y5riu5x0eww/Z63v7KIrmurhNegenL71+3FIFdWBZtH0fgZj2EoQcwB56VfzfwzUlEDV2XtEjMLjNNTpPr5Sf3c9ju+d54EJQyVsagTCTP9HQjg5YbJbngqSvoU1IDzmAl+/udgcFX2V2BQxcgXY9kA3/vJZMXldiZpeTuIRPtBTH8oE1qVLw+oFPlahlkWlzB8Rp8HRTY4EF3e509R3+taiK5JCWW3maHeQbvt6Pe2sqssDNKNZc8OmWSBm/j/Gm+86ty6PeRn0B9Xua2JWMM+XqUmphIfAtUBNVS1n8eYr8ha0eBOBRoESm8itj5mJKHhTsy30sBL/HaHyChVgznuvhvmR+mS9ScU0g2LS8y9EvuBb57OxzJIr9Wi0dqTvP17H4AhCfXtZBlDy377b9ruOWbjwBy1mjxEunWC7KSjyl0ieI6X5nSvrvpsc/H5LCyUzf9zqL0d8Di42dVszpuivR5vRp2giqo80PX9EzE/4GntPb0L4Z8cTty5R7C9d9vHrqRAMJukLW/U4rCKcLqw5HDRSCQF5MfaYYYF27/ApVZPpBdnPirZ7ov6ib+UF54nZCwtd+MSvwShwjo5mCS3qWSf6RrrScYD/TQxXRE7l13xCTn22x3csL4F3Gh2n3uVhCgL55x14oi9tZL0rcdLlpoJqz4wWdu6U+jHaRKwHkU28DulWqsfHxorI/tnOoyuoGuMjEn4QtzZ3N98dzIFpWZCSN4WwO9IZ7h9SUggiaPevIce8se7+H7uCvdi4Desoc0mqDEzhU97wYLH1V5kyfq32Fo0OJNqiaeedJOC8r/b4/IHo55f0qiig1TovD89FWq+bTGr3MRX9dTwSu0DavOpVZhbl30q1dAXVal1sMOR27wS+G3V0mTGfDR6xRF4itS68ebtw61p3478BgyDDpZPCqKQsbZFjwi2jlnURumZmOqnckCelRvvZZfHCgsSTvy8zl3OO04ZqIi77bRimpIO7tGkH6scYJjJP4yrFFt/Twthvx3I+SCMNWxmwrrmY4byRULhJCAn603NOaYvQpeLRLmsuHzLZXE/QTHzbsUmO3Gw/UxdPW3aMv6vCS4MbzuSvSX/17qgEMNFehqfHPpa8JHGgntMRFweExVMAojTIUp3U0J+9jxt3kTr31ciSl9BXf1IhfpS7TmJpfG5ZPw5nxsI/1fULlxSg53Np8QUSeULNurcGtmeLtHBBwyj7AhDEOhjYD11ucMjsjFC8IQh0l0wHXY2uEXXmjWaIZqm764No2lVxVT03LDqNjWpXcQSMecQRi5Q6MZ4Ajt6BJ/bUfrpqeXqvyYujrbatZRLuxsgq/fGqVPkKVo9VLrle7Ky6wVn6Q+XrJmuiWRMZj8M6wTo3EjeQE3KWKIrqkwygPrNjhxaloqzLyONB1qcDG1TEGk0yJiAQjBMKxGWNq+fWPan7jfO4cxPxwOJqy6KbZCf2X03VsY0GcjcjQKlvKQpvzI7T8SUKauUysqIl8kTovNEj6kAXLMp+bUWaw2f41HyF3swqDxsWhzt62STWbh+2llSbLV7SRdUOX9qPqy/iTiBMpmwRRE/GO9xEeIDWNnpc3AY7Z4GkvBUqX8blWx82wbQYxfHycVmK4DbndHi2QumQQw2N/a1SGEePeL9WvhqTHdQmNPISPawQD+NRHYaLnTS38l/stluNP2pAwPvColyA50zGL2z1aZocY5voYa6X9ifGuky6Zmb8U7HYfeyG7DHhZORnU98lAsQ050TLXjaU/jhV9dTbrTaC6n14V7fnA54L/FU7qzU1Of0NM/ZAvMNd4Zcfo7Pz4Pq6sEbxJ7r3Q4iqYvxEFsp205cypAOKDZIbXtOrGl6CP1QGRt5goD5242W2dSUJlQwbACH34SVKq/xmZ8zoffG5sorAYzzYHElCWLuY+VVU2t2IG4qmyn0CwLWwdEuT7gXC3XYsCvKrZU0e/D+1+hjrov31aoMfHfYdAJ5oyZpy5lQ9L8FYTVf4m29bMHDNepvVKQzkYHjMNod6eXNX9IgPLvl8N4Zuq3kn9Wykc4aiPifWDK8DgMLovbUwwx/6Va+RdsB77hRUxNtvpxUdDXtGOaNEG8JQRaHxivwXxqQtlHSGDS+cqVHooTDl5WYdMlPEcoq7178Yd5zZcy2OvYHxpCuyD03BzbkifpMbwCJ/jIWoe/DqpDt+xk/RasIxEEXMvf8xKrndIMpFy3dgDJqvk0Hd3LxF9WubqwU8HYxpcw8tWoucsEJE1DGfNLHyJm4U4zAaQC/MwvhGyNGc6d13j6MSsLPCbEX2OpMDyRnT4yNX+VDQgl4+i1Ny6bKp6+oLKIUgFhc3TdSMB+uk949Lz49M4RcEwf+usGN0+cwU5lYt6tg19eCIwztzpH5QwU4aSFDJDaBZuKPooNwUJtcq9lO2Q220HFc/xiCcm/XLe4/dpand8WgMHQyIKsHqhH2rDRgzfXHKLVdjF9mnAuOuUv3JZB7+GGKMFOQUVJb+bzRhAvaiutQFJfickVqg+n8QRmpqj9FuOcWeLpqQQHyUxD38fYHdoVSbUTT0LN6yNU38b676HHP+MWoEBhCiB4DxPotDKWwG4eEZ0lLBGyUXXnzksFqxp7w9rABdx6f5iQvVYSt804pf2QiQ6Rcm6m676SFsUzJOavVp3YKxG4KECax6NN+7JhapmOsyQzsg2Eqz87TyZXaQcHpIVJtgFpzuJRq5Z4RAvkbUzIilo/7ZdS+RnGc7zdYxqQk6GHCWks+tFUERxSiULwQW028dfon5fHK07FsJxnenyUfDdaFIrrbGhJqOonRGMghofssA62KRjY5aD7mpvUWjUKh0e+XwmEfzBGlxxkzGn/9EzdUpJ9/Pz0+wMVitG1RLbL/nlHW10BGYd+kDr/waNRGNu9iC934+53W0Zsxc44kJRQIlDXiSEN8zSRJ+tU+TRUNaDrxgo0RahCbTI/wwDeY0hZWKoIEELcl+vws50oESpQeAbNlsV4QA55pXpfayqjdWg43CrZYEsUrO9jiZWNUwe/PR13ZxGNsSjUHfRw/C8eiJjn9xAxc4JQW48GWFoz+weXX572X3zKbQHpxepGIpu+PkE2T5eW1dsBCZNayw+bcTWCSzs7+eyKIurbm8Cjc8YGyHKg7VS2fT5iyzyJtAPR51lUGRg+LLMjFB04Fyd62bdE2wGZkUnzY/nzsDaBBZvN1QSob2NHLrtjc1JRzQ2hpNN/p01OpzT+2+Qv1Hl4kQZYwyy0E3gmMXskG0UHjQYcV8drrTyEceRQpN/8lVxy2y+ABx/N2yCa/CeBIe52aluyXtftJYIEPbxpJcX3+4QYCpd3DEafeAKAyg7IbQhCxavqX9IZEmeqdg2NEokE236E+JI6ypF0NRufbB6qfA10hz8Ol9FSOy2X8eIpfcrRRRYSYNRPaA657ybIvrqZTe0nwx1ESoidvqnjYMAaeJysxaZFbLNq26zbPIoeBbd3sFcKzpDNqyS7DLkkoAspni0nin10QCya3bzeZQUsCcTGmpwsxN/XZvyhF63pjgR0/jcbFC9dxYxKrZMxVdPRSMAMV2jjsUB+kIuFEPC7yW3s+QsfWAlQMmqLS7BPcDNvJVLY/jsDurRiG0SRLOq6RF1WomFmyUeS1wH/v3a/oScr7LIhj24f8knWx1xzQgQI6oMF8d8rnuViLjfdZTh0jPXkTdlZ6GFlq7KQI+JWJxEZHnyZTrXq7e1xTMPsqio5zON6jH+8TkFdprpDbMTpp5cRhvwkWKCzkjUt1CFO5FMH0bN6rWCkU1iWwU5QwpeysWvq7w5b63+xE5ODUg0NRE4VnRNQ0KlC1EqxvmbSVMiwqPehyET0+aVQsXFCYCFPd/XuVpuV7PrqFK7TPwaYV0SEhYPb/LQT2oMNJQjwbVEg0YJwK1F+jnGQOrPyj4I+tueGMbotYGkWzf0EGY1Rmm0QtA/oWCiSZQAE4wj8b+dh+DbXksScFFyeI5N20a2hNM9iLbxIHtMHzY4pDss5/f/C1wG2D8vCuHjW1kw8FeJnRo0MdBu2MwfywYsIvHLSQ+EyAs0xGiy+bcms4Wqzuxi1P0ftcke7h7A4vj7U5cYuiFi7VqHhCEnfGGwNQ5cyoAEbrMenXqQcgFUhsr4RaOOAf3FYnjSIZYtTkKVMXrLBxBy4sLLjxM4VFXGMTHz1E2GpV5g1LS8HZ8S4YhgYQxFb4PjlNpEDvbIHqKqqnxORliPh8bUe+i0A315JfvoC4lRPh1JrKXRZB/1dWD93pPrn7TijeXpqIBIkw0e0Qfes5POjAc6YlcZGNeu4rKiINy+orBzX1RS+U3MGZEWuipZxLQa0bpoqi0Xxo1DTnzuA859XFsc9MOzUiR3YsDn4neZ+wvSnzGolnrsmNCKJmSt8vKkMgd7A/bWMJuCl4zgpHfVLi6+aNOldXEnsMnAoT+RxBOTfGu+YACROwmme1WR3N8e559iwzoQx1oLxM16mex9rP5KGvIwPiR4PqJsCT1nBmB+U5liqz3sl6ldimJShEDXvmLcs8uyrl+HuSW2qMXDUCzr9Y2vsYxgXGILjui+vnBBsTeDsm1c5nvmCGV02H6rrOC7bp8gDrNIHT1+e0UwGqLhaRT9RKQraE0YQaBQHn3thOcS1q5m8uRNryzTNhYFvSRxAs1G36pQwDBxxXXqpmr2hjayAZA57v+RZi3Yz2MRNDyZD/2Q1zQlbaz8KL5HfXPe1fDBR11Z6cNcU+5yR92z1QYF23fjHM8353RlXVKU33F53v26eUWqvNIzZ46axp8zCx7plVV+HY/iZGyTeP1e51rJGw5guVWhT1mM+vQbiXKf4ST89MlM6UY6a3qOhCM/BzTyM283sSPI+9clbNjd0VMrnmwWFu87OD62+RuhP9a5GidXnel5NMPEzXLrpNPWf4Gdp5Hkj1a9eMofVaS/5wPUj0sgNDnr+uis99X+fZ6pNOOp3tJF5YnzOhwIzNaiPZ+MP60a6O/PBESQk84UXM0KTW7lj4cf4R9hdZwWh+wI3RWDfJ162iJ3t6P8WLVRUklsQ9FkwyMbOIjhoKr/DCKg+cXwBezVC/wmvvIlLDrmm0PfhUXRRkX3EAKDCGSQQ1M+Tj3ZHgLYRAaUH+Wdfn58TLMSHjuC6h2tnFf8JWoJZ/OQFR08VXUDeIbwuh4ZDXzCWxJQ8SZ1gzH9Ql1khUxzakKvHmHqbu680hdL8TzAOdPoG6nbWi5iLvXOXb4ve59YTZliC+ncXz29fUm22C8HsOFAWYxvvL9QiFT/gx6rshpBDNozvHpfoFrqJgqk/rZUh8/CXRvoQ9ZRMI=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['Form'];
if (!theForm) {
    theForm = document.Form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=Vn0MwPIq9cKekaJ36HVIg-I8wRJb6JZjZm8bc0hLzbjI5_obDKM5JUfRF5-MXsIFem-86Tcp0m1QBI9A0&amp;t=635802997220000000" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var __cultureInfo = {"name":"sr-Cyrl-CS","numberFormat":{"CurrencyDecimalDigits":2,"CurrencyDecimalSeparator":",","IsReadOnly":false,"CurrencyGroupSizes":[3],"NumberGroupSizes":[3],"PercentGroupSizes":[3],"CurrencyGroupSeparator":".","CurrencySymbol":"Дин.","NaNSymbol":"NaN","CurrencyNegativePattern":8,"NumberNegativePattern":1,"PercentPositivePattern":1,"PercentNegativePattern":1,"NegativeInfinitySymbol":"-бесконачност","NegativeSign":"-","NumberDecimalDigits":2,"NumberDecimalSeparator":",","NumberGroupSeparator":".","CurrencyPositivePattern":3,"PositiveInfinitySymbol":"+бесконачност","PositiveSign":"+","PercentDecimalDigits":2,"PercentDecimalSeparator":",","PercentGroupSeparator":".","PercentSymbol":"%","PerMilleSymbol":"‰","NativeDigits":["0","1","2","3","4","5","6","7","8","9"],"DigitSubstitution":1},"dateTimeFormat":{"AMDesignator":"","Calendar":{"MinSupportedDateTime":"\/Date(-62135596800000)\/","MaxSupportedDateTime":"\/Date(253402297199999)\/","AlgorithmType":1,"CalendarType":1,"Eras":[1],"TwoDigitYearMax":2029,"IsReadOnly":false},"DateSeparator":".","FirstDayOfWeek":1,"CalendarWeekRule":0,"FullDateTimePattern":"d. MMMM yyyy H:mm:ss","LongDatePattern":"d. MMMM yyyy","LongTimePattern":"H:mm:ss","MonthDayPattern":"d. MMMM","PMDesignator":"","RFC1123Pattern":"ddd, dd MMM yyyy HH\u0027:\u0027mm\u0027:\u0027ss \u0027GMT\u0027","ShortDatePattern":"d.M.yyyy","ShortTimePattern":"H:mm","SortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd\u0027T\u0027HH\u0027:\u0027mm\u0027:\u0027ss","TimeSeparator":":","UniversalSortableDateTimePattern":"yyyy\u0027-\u0027MM\u0027-\u0027dd HH\u0027:\u0027mm\u0027:\u0027ss\u0027Z\u0027","YearMonthPattern":"MMMM yyyy","AbbreviatedDayNames":["нед","пон","уто","сре","чет","пет","суб"],"ShortestDayNames":["не","по","ут","ср","че","пе","су"],"DayNames":["недеља","понедељак","уторак","среда","четвртак","петак","субота"],"AbbreviatedMonthNames":["јан","феб","мар","апр","мај","јун","јул","авг","сеп","окт","нов","дец",""],"MonthNames":["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар",""],"IsReadOnly":false,"NativeCalendarName":"грегоријански календар","AbbreviatedMonthGenitiveNames":["јан","феб","мар","апр","мај","јун","јул","авг","сеп","окт","нов","дец",""],"MonthGenitiveNames":["јануар","фебруар","март","април","мај","јун","јул","август","септембар","октобар","новембар","децембар",""]},"eras":[1,"н.е.",null,0]};//]]>
</script>

<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3asr-Cyrl-CS%3ad28568d3-e53e-4706-928f-3765912b66ca%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<script src="/js/dnn.js" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
dnn.controls.submitComp.onsubmit();
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="aR+DFVLpZRaAnqivd4lt1TdfO/AknPlpoz5KA1sc08TkT9XMPANgDxGD9mNO8Seh+f+S26VVIPkWT9gh8ppatx4DV90kJ0ABPx8DRrMZWIK6OmhMeK3eylAymFToQYx369B0q8Q6GWDlh2X2sZ7UtEZIZ/fLWfXbAiIZfC2HhwAlJfDEihaEcV+b8JY=" />
</div><script src="/js/dnn.js?cdv=440" type="text/javascript"></script><script src="/js/dnn.xmlhttp.js?cdv=440" type="text/javascript"></script><script src="/js/dnn.controls.js?cdv=440" type="text/javascript"></script><script src="/js/dnn.modalpopup.js?cdv=440" type="text/javascript"></script><script src="/js/dnn.controls.dnnmenu.js?cdv=440" type="text/javascript"></script><script src="/js/dnn.dom.positioning.js?cdv=440" type="text/javascript"></script><script src="/js/dnncore.js?cdv=440" type="text/javascript"></script><script src="/Resources/Search/SearchSkinObjectPreview.js?cdv=440" type="text/javascript"></script><script src="/js/dnn.servicesframework.js?cdv=440" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        
<script type="text/javascript" src="/Portals/0/Skins/Flex2-V-Round-Stripe/swfobject.js"></script>
<script type="text/javascript">
var params = { wmode: "transparent", FlashVars: "XMLpath=/Portals/0/Skins/Flex2-V-Round-Stripe/Template-Flash-900x112-01.images.xml" };
swfobject.embedSWF("/Portals/0/Skins/Flex2-V-Round-Stripe/flash/flex-900x112.swf", "FlashBannerContainer", "900", "112", "9.0.0", false, false, params);
</script>

<script type="text/javascript" src='/Portals/0/Skins/Flex2-V-Round-Stripe/drnuke-height.js'></script>
<!--[if lte ie 6]>
<script type="text/javascript">
if (typeof blankImg == 'undefined') var blankImg = '/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif';
</script>
<style type="text/css">
.trans-png, #Body, .flex-container-visibility a img { behavior: url(/Portals/0/Skins/Flex2-V-Round-Stripe/drnuke-png.htc) }
</style>
<![endif]-->
<!--[if gte IE 5]>
<link rel="stylesheet" type="text/css" media="all" href="/Portals/0/Skins/Flex2-V-Round-Stripe/css/ie.css">
<![endif]-->  
<!--[if ie 6]>
<link rel="stylesheet" type="text/css" media="all" href="/Portals/0/Skins/Flex2-V-Round-Stripe/css/ie6.css">
<![endif]-->

<div id="OuterContainer" class="EMWidth">   

<table class="EMSkinTable fullwidth" border="0" cellspacing="0" cellpadding="0">
<tr id="EMOffset1"><td class="EMSkinTL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td style="vertical-align:bottom;">
	<table class="fullwidth" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="EMSkinTL2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    <td class="EMSkinT trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
	<td class="EMSkinTR2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    </tr>
    </table>
</td>
<td class="EMSkinTR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td></tr>
<tr>
<td class="EMSkinL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td class="EMSkinM">
	<div id="ContentHeightContainer">
	<div id="InnerContainer">
    	<div style="float:left;"> 
            <div id="LogoContainer" class="EMLogoContainer">
                <a id="dnn_dnnLOGO_hypLogo" title="Agencija za privredne registre (APR)" href="http://www.apr.gov.rs/%d0%9d%d0%b0%d1%81%d0%bb%d0%be%d0%b2%d0%bd%d0%b0.aspx"></a>
            </div>
        </div>
        <div style="float:right;">
            <div id="LogoRightContainer">
                <div id="DateContainer" class="EMDateContainer">
                <span id="dnn_dnnCURRENTDATE_lblDate" class="DateToken EMFontFamily">четвртак 17. март  2016.</span>

                </div>
                
                <div id="LanguageContainer" class="EMLanguageContainer">
                <div class="language-object" >


</div>
                </div>            
            </div>
        </div>
        <div class="clear"></div>
        
        <div id="MenuBottomContainer">
        <table cellpadding="0" cellspacing="0" border="0" class="fullwidth">
        <tr><td id="MenuBarL" class="EMBaseColour6"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-bl.png" alt="" class="trans-png" /></td><td id="MenuBarM" class="EMBaseColour6"></td><td id="MenuBarR" class="EMBaseColour6"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-br.png" alt="" class="trans-png" /></td></tr>
        </table>
        </div>
          
        <div id="BannerOuterContainer">
            <div id="FlashBannerContainer">
            <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
            </div> 
        </div>
          
        <div id="UnderBannerContainer">
        <table cellpadding="0" cellspacing="0" class="fullwidth">
        <tr>
        <td><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/login-l.gif" width="9" height="31" alt="" /></td>
        <td class="fullwidth">
            <div id="BreadcrumbContainer" class="EMBreadcrumbContainer">
                <span class="EMBaseColour4 BreadcrumbSpan"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/breadcrumb.png" alt="" class="trans-png" /></span><span id="dnn_dnnBREADCRUMB_lblBreadCrumb"><a href="http://www.apr.gov.rs/Насловна.aspx" class="BreadcrumbToken EMFontFamily">Насловна</a></span>

            </div>
            <div id="LoginContainer">
                <table border="0" cellpadding="0" cellspacing="0">
                <tr><td class="EMBaseColour5 trans-png"><div></div></td></tr>
                </table> 
            </div>
            <div id="UserContainer">
<a href="http://www.apr.gov.rs/eng/Home.aspx"><strong>English</strong></a>
                <table border="0" cellpadding="0" cellspacing="0">
		
                <tr><td ></td></tr>
                </table> 
            </div>
            
		</td>
        <td><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/login-r.gif" width="9" height="31" alt="" /></td>
        </tr>
        </table>
        </div>

		<div id="ContentContainer">
        <table border="0" cellpadding="0" cellspacing="0" class="fullwidth">
        <tr>
        <td id="MenuContainerCell">
        
            <div id="MenuContainer"><div>
	<ul class="mainmenu-menuitem EMSubMenuItemOff EMMainMenuFont" id="dnn_dnnNAV_ctldnnNAV">
		<li id="dnn_dnnNAV_ctldnnNAVctr39"><a href="http://www.apr.gov.rs/%d0%9d%d0%b0%d1%81%d0%bb%d0%be%d0%b2%d0%bd%d0%b0.aspx"><span>Насловна</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr113"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8.aspx"><span>О Агенцији</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub113">
			<li id="dnn_dnnNAV_ctldnnNAVctr285"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a0%d0%b5%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d0%b5%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Референце Агенције</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr242"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%9e%d1%81%d0%bd%d0%b8%d0%b2%d0%b0%d1%9a%d0%b5.aspx"><span>Оснивање</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr243"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%9e%d1%80%d0%b3%d0%b0%d0%bd%d0%b8%d0%b7%d0%b0%d1%86%d0%b8%d1%98%d0%b0.aspx"><span>Организација</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr244"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a3%d0%bf%d1%80%d0%b0%d0%b2%d0%bd%d0%b8%d0%be%d0%b4%d0%b1%d0%be%d1%80.aspx"><span>Управни одбор</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr245"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%a1%d0%b2%d0%b8%d1%80%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8%d0%bd%d0%b0%d1%98%d0%b5%d0%b4%d0%bd%d0%be%d0%bc%d0%bc%d0%b5%d1%81%d1%82%d1%83.aspx"><span>Сви регистри на једном месту</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr353"><a href="http://www.apr.gov.rs/%d0%9e%d0%90%d0%b3%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b8/%d0%98%d0%bd%d1%82%d0%b5%d1%80%d0%bd%d0%b0%d0%b4%d0%be%d0%ba%d1%83%d0%bc%d0%b5%d0%bd%d1%82%d0%b0.aspx"><span>Интерна документа</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr139"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8.aspx"><span>Регистри</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub139">
			<li id="dnn_dnnNAV_ctldnnNAVctr59"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b0%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b0.aspx"><span>Привредна друштва</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr60"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b5%d0%b4%d1%83%d0%b7%d0%b5%d1%82%d0%bd%d0%b8%d1%86%d0%b8.aspx"><span>Предузетници</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr216"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d0%b8%d0%b7%d0%b2%d0%b5%d1%88%d1%82%d0%b0%d1%98%d0%b8.aspx"><span>Финансијски извештаји</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr61"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%98%d1%81%d0%ba%d0%b8%d0%bb%d0%b8%d0%b7%d0%b8%d0%bd%d0%b3.aspx"><span>Финансијски лизинг</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr62"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%97%d0%b0%d0%bb%d0%be%d0%b6%d0%bd%d0%be%d0%bf%d1%80%d0%b0%d0%b2%d0%be.aspx"><span>Заложно право</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr116"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9c%d0%b5%d0%b4%d0%b8%d1%98%d0%b8.aspx"><span>Медији</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr117"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a3%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9a%d0%b0.aspx"><span>Удружења</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr118"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%82%d1%80%d0%b0%d0%bd%d0%b0%d1%83%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9a%d0%b0.aspx"><span>Страна удружења</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr229"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a2%d1%83%d1%80%d0%b8%d0%b7%d0%b0%d0%bc.aspx"><span>Туризам</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr258"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%82%d0%b5%d1%87%d0%b0%d1%98%d0%bd%d0%b5%d0%bc%d0%b0%d1%81%d0%b5.aspx"><span>Стечајне масе</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr260"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9c%d0%b5%d1%80%d0%b5%d0%b8%d0%bf%d0%be%d0%b4%d1%81%d1%82%d0%b8%d1%86%d0%b0%d1%98%d0%b8%d1%80%d0%b5%d0%b3%d0%b8%d0%be%d0%bd%d0%b0%d0%bb%d0%bd%d0%be%d0%b3%d1%80%d0%b0%d0%b7%d0%b2%d0%be%d1%98%d0%b0.aspx"><span>Мере и подстицаји регионалног развоја</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr262"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b5%d0%ba%d0%be%d0%bc%d0%be%d1%80%d0%b5%d0%b8%d0%bf%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bd%d0%b8%d1%88%d1%82%d0%b2%d0%b0%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%85%d0%bf%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b8%d1%85%d0%ba%d0%be%d0%bc%d0%be%d1%80%d0%b0.aspx"><span>Привредне коморе и представништва страних привредних комора</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr341"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%97%d0%b0%d0%b4%d1%83%d0%b6%d0%b1%d0%b8%d0%bd%d0%b5%d0%b8%d1%84%d0%be%d0%bd%d0%b4%d0%b0%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Задужбине и фондације</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr343"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d1%80%d0%b5%d0%b4%d1%81%d1%82%d0%b0%d0%b2%d0%bd%d0%b8%d1%88%d1%82%d0%b2%d0%b0%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%85%d0%b7%d0%b0%d0%b4%d1%83%d0%b6%d0%b1%d0%b8%d0%bd%d0%b0%d0%b8%d1%84%d0%be%d0%bd%d0%b4%d0%b0%d1%86%d0%b8%d1%98%d0%b0.aspx"><span>Представништва страних задужбина и фондација</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr366"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d0%bf%d0%be%d1%80%d1%82%d1%81%d0%ba%d0%b0%d1%83%d0%b4%d1%80%d1%83%d0%b6%d0%b5%d1%9aa,%d0%b4%d1%80%d1%83%d1%88%d1%82%d0%b2%d0%b0%d0%b8%d1%81%d0%b0%d0%b2%d0%b5%d0%b7%d0%b8.aspx"><span>Спортска удружењa, друштва и савези</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr377"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a1%d1%83%d0%b4%d1%81%d0%ba%d0%b5%d0%b7%d0%b0%d0%b1%d1%80%d0%b0%d0%bd%d0%b5.aspx"><span>Судске забране</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr599"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a4%d0%b0%d0%ba%d1%82%d0%be%d1%80%d0%b8%d0%bd%d0%b3.aspx"><span>Факторинг</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr591"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%9f%d0%be%d0%bd%d1%83%d1%92%d0%b0%d1%87%d0%b8.aspx"><span>Понуђачи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr779"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%a3%d0%b3%d0%be%d0%b2%d0%be%d1%80%d0%b8%d0%be%d1%84%d0%b8%d0%bd%d0%b0%d0%bd%d1%81%d0%b8%d1%80%d0%b0%d1%9a%d1%83%d0%bf%d0%be%d1%99%d0%be%d0%bf%d1%80%d0%b8%d0%b2%d1%80%d0%b5%d0%b4%d0%bd%d0%b5%d0%bf%d1%80%d0%be%d0%b8%d0%b7%d0%b2%d0%be%d0%b4%d1%9a%d0%b5.aspx"><span>Уговори о финансирању пољопривредне производње</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr828"><a href="http://www.apr.gov.rs/%d0%a0%d0%b5%d0%b3%d0%b8%d1%81%d1%82%d1%80%d0%b8/%d0%93%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>Грађевинске дозволе</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr64"><a href="http://www.apr.gov.rs/%d0%9f%d1%80%d0%be%d0%bf%d0%b8%d1%81%d0%b8.aspx"><span>Прописи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr132"><a href="http://www.apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5.aspx"><span>Услуге</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub132">
			<li id="dnn_dnnNAV_ctldnnNAVctr427"><a href="http://www.apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d0%bf%d0%be%d0%b4%d0%b0%d1%82%d0%b0%d0%ba%d0%b0%d0%b1%d0%b5%d0%b7%d0%bd%d0%b0%d0%ba%d0%bd%d0%b0%d0%b4%d0%b5.aspx"><span>Достављање података без накнаде</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr509"><a href="http://www.apr.gov.rs/%d0%a3%d1%81%d0%bb%d1%83%d0%b3%d0%b5/%d0%94%d0%be%d1%81%d1%82%d0%b0%d0%b2%d1%99%d0%b0%d1%9a%d0%b5%d0%bf%d0%be%d0%b4%d0%b0%d1%82%d0%b0%d0%ba%d0%b0%d1%83%d0%b7%d0%bd%d0%b0%d0%ba%d0%bd%d0%b0%d0%b4%d1%83.aspx"><span>Достављање података уз накнаду</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr133"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83.aspx"><span>Односи с јавношћу</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub133">
			<li id="dnn_dnnNAV_ctldnnNAVctr68"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%92%d0%b5%d1%81%d1%82%d0%b8.aspx"><span>Вести</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr310"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9f%d1%80%d0%b5%d1%81%d1%81%d0%bb%d1%83%d0%b6%d0%b1%d0%b0.aspx"><span>Прес служба</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr66"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9c%d0%b5%d1%92%d1%83%d0%bd%d0%b0%d1%80%d0%be%d0%b4%d0%bd%d0%b8%d0%be%d0%b4%d0%bd%d0%be%d1%81%d0%b8.aspx"><span>Међународни односи</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr67"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%9a%d0%be%d0%bd%d1%84%d0%b5%d1%80%d0%b5%d0%bd%d1%86%d0%b8%d1%98%d0%b5.aspx"><span>Конференције</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr328"><a href="http://www.apr.gov.rs/%d0%9e%d0%b4%d0%bd%d0%be%d1%81%d0%b8%d1%81%d1%98%d0%b0%d0%b2%d0%bd%d0%be%d1%88%d1%9b%d1%83/%d0%92%d0%b8%d0%b4%d0%b5%d0%be%d0%b3%d0%b0%d0%bb%d0%b5%d1%80%d0%b8%d1%98%d0%b0.aspx"><span>Видео галерија</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr140"><a href="http://www.apr.gov.rs/%d0%90%d1%80%d1%85%d0%b8%d0%b2%d0%b0.aspx"><span>Архива</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr72"><a href="http://www.apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.aspx"><span>Инфо центар и контакти</span><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png" /></a><ul id="dnn_dnnNAV_ctldnnNAVsub72">
			<li id="dnn_dnnNAV_ctldnnNAVctr228"><a href="http://www.apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80.aspx"><span>Инфо центар</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr715"><a href="http://www.apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%9a%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8.aspx"><span>Контакти</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr832"><a href="http://www.apr.gov.rs/%d0%98%d0%bd%d1%84%d0%be%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b8%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%b8/%d0%9f%d0%be%d0%b7%d0%b8%d0%b2%d0%bd%d0%b8%d1%86%d0%b5%d0%bd%d1%82%d0%b0%d1%80%d0%b7%d0%b0%d0%b3%d1%80%d0%b0%d1%92%d0%b5%d0%b2%d0%b8%d0%bd%d1%81%d0%ba%d0%b5%d0%b4%d0%be%d0%b7%d0%b2%d0%be%d0%bb%d0%b5.aspx"><span>Позивни центар за грађевинске дозволе</span></a></li>
		</ul></li><li id="dnn_dnnNAV_ctldnnNAVctr322"><a href="http://www.apr.gov.rs/%d0%a0%d0%b0%d1%87%d1%83%d0%bd%d0%b8%d0%b7%d0%b0%d1%83%d0%bf%d0%bb%d0%b0%d1%82%d0%b5.aspx"><span>Рачуни за уплате</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr550"><a href="http://www.apr.gov.rs/%d0%88%d0%b0%d0%b2%d0%bd%d0%b5%d0%bd%d0%b0%d0%b1%d0%b0%d0%b2%d0%ba%d0%b5.aspx"><span>Јавне набавке</span></a></li><li id="dnn_dnnNAV_ctldnnNAVctr631"><a href="http://www.apr.gov.rs/%d0%92%d0%b8%d0%b4%d0%b5%d0%be%d1%83%d0%bf%d1%83%d1%82%d1%81%d1%82%d0%b2%d0%b0.aspx"><span>Видео упутства</span></a></li>
	</ul>
</div></div>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_MenuPane" class="MenuPane DNNEmptyPane" valign="top"></td>

            </tr>
            </table>
            <div id="SearchContainer" class="EMSearchContainer">
                    <div class="SearchBoxL">
                        <div class="SearchBox EMBaseColour3">
                        <span id="dnn_dnnSEARCH_ClassicSearch">
    
    
    <span class="searchInputContainer" data-moreresults="See More Results" data-noresult="No Results Found">
        <input name="dnn$dnnSEARCH$txtSearch" type="text" maxlength="255" size="20" id="dnn_dnnSEARCH_txtSearch" class="NormalTextBox" autocomplete="off" placeholder="Search..." />
        <a class="dnnSearchBoxClearText"></a>
    </span>
    <a id="dnn_dnnSEARCH_cmdSearch" class="SkinObject" href="javascript:__doPostBack(&#39;dnn$dnnSEARCH$cmdSearch&#39;,&#39;&#39;)"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/search-button.png" hspace="0" alt="Search" class="trans-png" /></a>
</span>


<script type="text/javascript">
    $(function() {
        if (typeof dnn != "undefined" && typeof dnn.searchSkinObject != "undefined") {
            var searchSkinObject = new dnn.searchSkinObject({
                delayTriggerAutoSearch : 400,
                minCharRequiredTriggerAutoSearch : 2,
                searchType: 'S',
                enableWildSearch: true,
                cultureCode: 'sr-Cyrl-CS',
                portalId: -1
                }
            );
            searchSkinObject.init();
            
            
            // attach classic search
            var siteBtn = $('#dnn_dnnSEARCH_SiteRadioButton');
            var webBtn = $('#dnn_dnnSEARCH_WebRadioButton');
            var clickHandler = function() {
                if (siteBtn.is(':checked')) searchSkinObject.settings.searchType = 'S';
                else searchSkinObject.settings.searchType = 'W';
            };
            siteBtn.on('change', clickHandler);
            webBtn.on('change', clickHandler);
            
            
        }
    });
</script>

                        </div>
                    </div>         
                </div>
        </td>
        <td id="ContentContainerCell">
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_TopPane" colspan="2" class="TopPane DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane" class="LeftPane DNNEmptyPane"></td>

            <td id="dnn_RightPane" class="RightPane DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_TopPane2" colspan="3" class="TopPane2 DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane2" class="LeftPane2 DNNEmptyPane"></td>

            <td id="dnn_ContentPane1" class="ContentPane1 DNNEmptyPane"></td>

            <td id="dnn_RightPane2" class="RightPane2 DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_ContentPane" colspan="2" class="ContentPane"><div class="DnnModule DnnModule- DnnModule--1">
<table class="flex-container-1 fullwidth">
<tr class="EMContainerColour8">
<td class="flex-container-tl-simple trans-png"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="33" width="14" alt="" /></td>
<td class="flex-container-t-simple trans-png">
	<div class="flex-container-action"></div>
    <div class="flex-container-title"><h1 class="EMContainerTitleFontSize8"><span id="dnn_ctr_dnnTITLE_titleLabel" class="EMContainerTitleFontColour8 EMContainerTitleFontFamily8 EMContainerTitleFontSize8">Privacy Statement</span>


</h1></div>
    <div class="flex-container-visibility"></div>
    <div class="flex-container-help"></div>
</td>
<td class="flex-container-tr-simple trans-png"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="33" width="14" alt="" /></td>
</tr>
<tr>
  <td class="flex-container-l"></td>
  <td class="flex-container-m">
    <table class="flex-container-m-table fullwidth">
    <tr><td id="dnn_ctr_ContentPane" class="flex-container-m-td flex-container-content"><div id="dnn_ctr_ModuleContent" class="DNNModuleContent ModC">
	
<div class="dnnPrivacy dnnClear"><span id="dnn_ctr_Privacy_lblPrivacy"><div align="left">
        <p>Agencija za privredne registre (APR) is committed to protecting your privacy and developing technology
        that gives you the most powerful and safe online experience. This Statement of Privacy
        applies to the Agencija za privredne registre (APR) Web site and governs data collection and usage.
        By using the Agencija za privredne registre (APR) website, you consent to the data practices described
        in this statement.</p>
        <p><span class="SubHead">Collection of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) collects personally identifiable information, such as your e-mail
        address, name, home or work address or telephone number. Agencija za privredne registre (APR) also
        collects anonymous demographic information, which is not unique to you, such as
        your ZIP code, age, gender, preferences, interests and favorites.</p>
        <p>There is also information about your computer hardware and software that is automatically
        collected by Agencija za privredne registre (APR). This information can include: your IP address,
        browser type, domain names, access times and referring Web site addresses. This
        information is used by Agencija za privredne registre (APR) for the operation of the service, to
        maintain quality of the service, and to provide general statistics regarding use
        of the Agencija za privredne registre (APR) Web site.</p>
        <p>Please keep in mind that if you directly disclose personally identifiable information
        or personally sensitive data through Agencija za privredne registre (APR) public message boards,
        this information may be collected and used by others. Note: Agencija za privredne registre (APR)
        does not read any of your private online communications.</p>
        <p>Agencija za privredne registre (APR) encourages you to review the privacy statements of Web sites
        you choose to link to from Agencija za privredne registre (APR) so that you can understand how those
        Web sites collect, use and share your information. Agencija za privredne registre (APR) is not responsible
        for the privacy statements or other content on Web sites outside of the Agencija za privredne registre (APR)
        and Agencija za privredne registre (APR) family of Web sites.</p>
        <p><span class="SubHead">Use of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) collects and uses your personal information to operate the Agencija za privredne registre (APR)
        Web site and deliver the services you have requested. Agencija za privredne registre (APR) also uses
        your personally identifiable information to inform you of other products or services
        available from Agencija za privredne registre (APR) and its affiliates. Agencija za privredne registre (APR) may also
        contact you via surveys to conduct research about your opinion of current services
        or of potential new services that may be offered.</p>
        <p>Agencija za privredne registre (APR) does not sell, rent or lease its customer lists to third parties.
        Agencija za privredne registre (APR) may, from time to time, contact you on behalf of external business
        partners about a particular offering that may be of interest to you. In those cases,
        your unique personally identifiable information (e-mail, name, address, telephone
        number) is not transferred to the third party. In addition, Agencija za privredne registre (APR)
        may share data with trusted partners to help us perform statistical analysis, send
        you email or postal mail, provide customer support, or arrange for deliveries. All
        such third parties are prohibited from using your personal information except to
        provide these services to Agencija za privredne registre (APR), and they are required to maintain
        the confidentiality of your information.</p>
        <p>Agencija za privredne registre (APR) does not use or disclose sensitive personal information, such
        as race, religion, or political affiliations, without your explicit consent.</p>
        <p>Agencija za privredne registre (APR) keeps track of the Web sites and pages our customers visit within
        Agencija za privredne registre (APR), in order to determine what Agencija za privredne registre (APR) services are
        the most popular. This data is used to deliver customized content and advertising
        within Agencija za privredne registre (APR) to customers whose behavior indicates that they are interested
        in a particular subject area.</p>
        <p>Agencija za privredne registre (APR) Web sites will disclose your personal information, without notice,
        only if required to do so by law or in the good faith belief that such action is
        necessary to: (a) conform to the edicts of the law or comply with legal process
        served on Agencija za privredne registre (APR) or the site; (b) protect and defend the rights or
        property of Agencija za privredne registre (APR); and, (c) act under exigent circumstances to protect
        the personal safety of users of Agencija za privredne registre (APR), or the public.</p>
        <p><span class="SubHead">Use of Cookies</span></p>
        <p>The Agencija za privredne registre (APR) Web site use "cookies" to help you personalize your online
        experience. A cookie is a text file that is placed on your hard disk by a Web page
        server. Cookies cannot be used to run programs or deliver viruses to your computer.
        Cookies are uniquely assigned to you, and can only be read by a web server in the
        domain that issued the cookie to you.</p>
        <p>One of the primary purposes of cookies is to provide a convenience feature to save
        you time. The purpose of a cookie is to tell the Web server that you have returned
        to a specific page. For example, if you personalize Agencija za privredne registre (APR) pages, or
        register with Agencija za privredne registre (APR) site or services, a cookie helps Agencija za privredne registre (APR)
        to recall your specific information on subsequent visits. This simplifies the process
        of recording your personal information, such as billing addresses, shipping addresses,
        and so on. When you return to the same Agencija za privredne registre (APR) Web site, the information
        you previously provided can be retrieved, so you can easily use the Agencija za privredne registre (APR)
        features that you customized.</p>
        <p>You have the ability to accept or decline cookies. Most Web browsers automatically
        accept cookies, but you can usually modify your browser setting to decline cookies
        if you prefer. If you choose to decline cookies, you may not be able to fully experience
        the interactive features of the Agencija za privredne registre (APR) services or Web sites you visit.</p>
        <p><span class="SubHead">Security of your Personal Information</span></p>
        <p>Agencija za privredne registre (APR) secures your personal information from unauthorized access,
        use or disclosure. Agencija za privredne registre (APR) secures the personally identifiable information
        you provide on computer servers in a controlled, secure environment, protected from
        unauthorized access, use or disclosure. When personal information (such as a credit
        card number) is transmitted to other Web sites, it is protected through the use
        of encryption, such as the Secure Socket Layer (SSL) protocol.</p>
        <p><span class="SubHead">Changes to this Statement</span></p>
        <p>Agencija za privredne registre (APR) will occasionally update this Statement of Privacy to reflect
company and customer feedback. Agencija za privredne registre (APR) encourages you to periodically 
        review this Statement to be informed of how Agencija za privredne registre (APR) is protecting your
        information.</p>
        <p><span class="SubHead">Contact Information</span></p>
        <p>Agencija za privredne registre (APR) welcomes your comments regarding this Statement of Privacy.
        If you believe that Agencija za privredne registre (APR) has not adhered to this Statement, please
        contact Agencija za privredne registre (APR) at <a href="mailto:srogic@apr.gov.rs">srogic@apr.gov.rs</a>.
        We will use commercially reasonable efforts to promptly determine and remedy the
        problem.</p>
    </div></span></div>
</div></td>
</tr>
    <tr><td class="flex-container-m-td">
    	<div class="flex-container-action2"></div>
        <div class="flex-container-syndicate"></div>
        <div class="flex-container-print"></div>
        <div class="flex-container-settings"></div>
    </td></tr>    
    </table>
  </td>
  <td class="flex-container-r"></td>
</tr>
<tr><td class="flex-container-bl"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="12" width="14" alt="" /></td><td class="flex-container-b"></td><td class="flex-container-br"><img src="/Portals/0/Containers/Flex2-Set8/images/spacer.gif" height="12" width="14" alt="" /></td></tr>
</table>
<div class="clear cont-br"></div>







</div></td>

            </tr>                
            <tr>
            <td id="dnn_ContentPane2" class="ContentPane2 DNNEmptyPane"></td>

            <td id="dnn_RightPane3" class="RightPane3 DNNEmptyPane"></td>

            </tr> 
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">        
            <tr>
            <td id="dnn_MiddlePane" colspan="3" class="MiddlePane DNNEmptyPane"></td>

            </tr>
            <tr>
            <td id="dnn_LeftPane3" class="LeftPane3 DNNEmptyPane"></td>

            <td id="dnn_ContentPane3" class="ContentPane3 DNNEmptyPane"></td>

            </tr> 
            </table>  
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_BottomPane" colspan="3" class="BottomPane DNNEmptyPane"></td>

            </tr>              
            <tr>
            <td id="dnn_LeftPane4" class="LeftPane4 DNNEmptyPane"></td>

            <td id="dnn_ContentPane4" class="ContentPane4 DNNEmptyPane"></td>

            <td id="dnn_RightPane4" class="RightPane4 DNNEmptyPane"></td>

            </tr>
            </table>
            <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
            <tr>
            <td id="dnn_BottomPane2" class="BottomPane2 DNNEmptyPane"></td>

            </tr>
            </table>
        </td></tr>
        </table>
        
        </div>
      </div>
      </div>        
</td>        
<td class="EMSkinR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset2">
<td class="EMSkinL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td class="EMSkinM">
    <div id="FooterContainer">
    <table align="center" cellpadding="0" cellspacing="0" class="fullwidth">
    <tr>
    <td id="dnn_FooterPane" class="FooterPane DNNEmptyPane" valign="top"></td>

    </tr>
    </table>
    </div>
</td>
<td class="EMSkinR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset3">
<td class="EMSkinBL trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td>
	<table class="fullwidth" border="0" cellspacing="0" cellpadding="0">
	<tr>
    <td class="EMSkinBL2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    <td class="EMSkinB trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
	<td class="EMSkinBR2 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
    </tr>
    </table>
</td>
<td class="EMSkinBR trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
<tr id="EMOffset4">
<td class="EMSkinBL3 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
<td id="FooterCell" class="EMSkinB3">
    <div id="CopyrightContainer" class="EMCopyrightContainer"><span id="dnn_dnnCOPYRIGHT_lblCopyright" class="FooterToken EMFooterFont">Copyright 2009 APR</span>
</div>
    <div id="TermsContainer" class="EMTermsContainer"><a id="dnn_dnnTERMS_hypTerms" class="FooterToken EMFooterFont" rel="nofollow" href="http://www.apr.gov.rs/terms.aspx">Terms Of Use</a></div>
    <div id="PrivacyContainer" class="EMPrivacyContainer"><a id="dnn_dnnPRIVACY_hypPrivacy" class="FooterToken EMFooterFont" rel="nofollow" href="http://www.apr.gov.rs/privacy.aspx">Privacy Statement</a></div> 
</td>
<td class="EMSkinBR3 trans-png"><img src="/Portals/0/Skins/Flex2-V-Round-Stripe/images/spacer.gif" alt="" /></td>
</tr>
</table>

</div>


        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" value="`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`39`,`dnn_dnnNAV_ctldnnNAV_json`:`{nodes:[{bcrumb:\`1\`,selected:\`1\`,id:\`39\`,key:\`39\`,txt:\`Насловна\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Насловна.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-breadcrumbactive EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,cssSel:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`113\`,key:\`113\`,txt:\`О Агенцији\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`285\`,key:\`285\`,txt:\`Референце Агенције\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/РеференцеАгенције.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`242\`,key:\`242\`,txt:\`Оснивање\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Оснивање.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`243\`,key:\`243\`,txt:\`Организација\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Организација.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`244\`,key:\`244\`,txt:\`Управни одбор\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Управниодбор.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`245\`,key:\`245\`,txt:\`Сви регистри на једном месту\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Свирегистринаједномместу.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`353\`,key:\`353\`,txt:\`Интерна документа\`,ca:\`3\`,url:\`http://www.apr.gov.rs/ОАгенцији/Интернадокумента.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`139\`,key:\`139\`,txt:\`Регистри\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`59\`,key:\`59\`,txt:\`Привредна друштва\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Привреднадруштва.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`60\`,key:\`60\`,txt:\`Предузетници\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Предузетници.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`216\`,key:\`216\`,txt:\`Финансијски извештаји\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Финансијскиизвештаји.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`61\`,key:\`61\`,txt:\`Финансијски лизинг\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Финансијскилизинг.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`62\`,key:\`62\`,txt:\`Заложно право\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Заложноправо.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`116\`,key:\`116\`,txt:\`Медији\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Медији.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`117\`,key:\`117\`,txt:\`Удружења\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Удружења.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`118\`,key:\`118\`,txt:\`Страна удружења\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Странаудружења.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`229\`,key:\`229\`,txt:\`Туризам\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Туризам.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`258\`,key:\`258\`,txt:\`Стечајне масе\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Стечајнемасе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`260\`,key:\`260\`,txt:\`Мере и подстицаји регионалног развоја\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Мереиподстицајирегионалногразвоја.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`262\`,key:\`262\`,txt:\`Привредне коморе и представништва страних привредних комора\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Привреднекомореипредставништвастранихпривреднихкомора.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`341\`,key:\`341\`,txt:\`Задужбине и фондације\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Задужбинеифондације.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`343\`,key:\`343\`,txt:\`Представништва страних задужбина и фондација\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Представништвастранихзадужбинаифондација.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`366\`,key:\`366\`,txt:\`Спортска удружењa, друштва и савези\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Спортскаудружењa,друштваисавези.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`377\`,key:\`377\`,txt:\`Судске забране\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Судскезабране.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`599\`,key:\`599\`,txt:\`Факторинг\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Факторинг.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`591\`,key:\`591\`,txt:\`Понуђачи\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Понуђачи.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`779\`,key:\`779\`,txt:\`Уговори о финансирању пољопривредне производње\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Уговориофинансирањупољопривреднепроизводње.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`828\`,key:\`828\`,txt:\`Грађевинске дозволе\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Регистри/Грађевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`64\`,key:\`64\`,txt:\`Прописи\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Прописи.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`132\`,key:\`132\`,txt:\`Услуге\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Услуге.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`427\`,key:\`427\`,txt:\`Достављање података без накнаде\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Услуге/Достављањеподатакабезнакнаде.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`509\`,key:\`509\`,txt:\`Достављање података уз накнаду\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Услуге/Достављањеподатакаузнакнаду.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`133\`,key:\`133\`,txt:\`Односи с јавношћу\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`68\`,key:\`68\`,txt:\`Вести\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Вести.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`310\`,key:\`310\`,txt:\`Прес служба\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Пресслужба.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`66\`,key:\`66\`,txt:\`Међународни односи\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Међународниодноси.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`67\`,key:\`67\`,txt:\`Конференције\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Конференције.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`328\`,key:\`328\`,txt:\`Видео галерија\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Односисјавношћу/Видеогалерија.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`140\`,key:\`140\`,txt:\`Архива\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Архива.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`72\`,key:\`72\`,txt:\`Инфо центар и контакти\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Инфоцентариконтакти.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[{id:\`228\`,key:\`228\`,txt:\`Инфо центар\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Инфоцентариконтакти/Инфоцентар.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`715\`,key:\`715\`,txt:\`Контакти\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Инфоцентариконтакти/Контакти.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]},{id:\`832\`,key:\`832\`,txt:\`Позивни центар за грађевинске дозволе\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Инфоцентариконтакти/Позивницентарзаграђевинскедозволе.aspx\`,cssHover:\`submenu-menuitemsel EMSubMenuItemBGOn EMSubMenuItemOn EMMainMenuFont\`,nodes:[]}]},{id:\`322\`,key:\`322\`,txt:\`Рачуни за уплате\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Рачунизауплате.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`550\`,key:\`550\`,txt:\`Јавне набавке\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Јавненабавке.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]},{id:\`631\`,key:\`631\`,txt:\`Видео упутства\`,ca:\`3\`,url:\`http://www.apr.gov.rs/Видеоупутства.aspx\`,rhtml:\`\u003cspan class=\\\`item-right\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-r.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,lhtml:\`\u003cspan class=\\\`item-left\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-l.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\u003cspan class=\\\`item-arrow\\\`\u003e\u003cimg src=\\\`/Portals/0/Skins/Flex2-V-Round-Stripe/images/em-menu-arrow.png\\\` class=\\\`trans-png\\\`\u003e\u003c/span\u003e\`,css:\`mainmenu-idle EMMainMenuItemOff EMBaseColour1 EMMainMenuFont\`,cssHover:\`mainmenu-selected EMBaseColour2 EMMainMenuItemOn EMMainMenuFont\`,cssIcon:\` \`,nodes:[]}]}`}" />
        <input name="__RequestVerificationToken" type="hidden" value="45d6gUjekHXfDAxm8iAmjRXjNu8kqO_1FlxoYtZd999Wo_bzvslcxIbQjb7_DJxTmdUmdYLJVKcavYYVa7CQAuNfqgzvbKkf5JmUY5M9bCg3b68Jput_inaCT_g1" />
    
<script type="text/javascript">dnn.setVar('dnn_dnnNAV_ctldnnNAV_p', '{rarrowimg:\'/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png\',sysimgpath:\'/images/\',carrowimg:\'/Portals/0/Skins/Flex2-V-Round-Stripe/images/menu-arrow.png\',orient:\'1\',rmode:\'2\',css:\'mainmenu-menuitem EMSubMenuItemOff EMMainMenuFont\',postback:\'__doPostBack(\\\'dnn$dnnNAV$ctldnnNAV\\\',\\\'[NODEID]~|~Click\\\')\',mcss:\'mainmenu-submenu EMSubMenuItemBGOff EMMainMenuFont EMSubMenuOpacity\',easeDir:\'0\'}');dnn.controls.initMenu($get('dnn_dnnNAV_ctldnnNAV'));</script></form>
    
    
</body>
</html>