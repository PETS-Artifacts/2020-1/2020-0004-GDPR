<!DOCTYPE html><!--[if lt IE 7]>      <html lang="en-US" class="no-js lt-ie9 lt-ie8 lt-ie7 deprecated"> <![endif]--><!--[if IE 7]>         <html lang="en-US" class="no-js lt-ie9 lt-ie8 ie7 deprecated"> <![endif]--><!--[if IE 8]>         <html lang="en-US" class="no-js lt-ie9 ie8 deprecated"> <![endif]--><!--[if gt IE 8]><!--> <html lang="en-US" class="no-js deprecated"> <!--<![endif]--><head>
<!-- // --><script type="text/javascript">
<!--
	document.write('<script src="/__ssobj/ard.png?6664735941975913340_1-348-'+(46017*33721+15502)+'&n=1'+'">\x3C/script\x3E');
//-->
<!-- // --></script>
<script>window.__ss_load_start=new Date();</script>
<script>window.__ss_load_start=new Date();</script>

<meta charset="utf-8">
<meta name="msvalidate.01" content="0485973D244F31396772B2ACD1ED08B7" />
<script src="/view/static/js/canonicals.js"></script>
<script type="text/javascript">
/*<![CDATA[*/(function(e,f){function h(a){if(a.mode){var b=g("mobify-mode");b&&a[b]||(b=a.mode(c.ua));return a[b]}return a}function m(){function a(a){e.addEventListener(a,function(){c[a]=+new Date},!1)}e.addEventListener&&(a("DOMContentLoaded"),a("load"))}function n(){var a=new Date;a.setTime(a.getTime()+3E5);f.cookie="mobify-path=; expires="+a.toGMTString()+"; path=/";e.location.reload()}function p(){k({src:"https://preview.mobify.com/v7/"})}function g(a){if(a=f.cookie.match(RegExp("(^|; )"+a+"((=([^;]*))|(; |$))")))return a[4]||""}function l(a){f.write('<plaintext style="display:none">');setTimeout(function(){d.capturing=!0;a()},0)}function k(a,b){var e=f.getElementsByTagName("script")[0],c=f.createElement("script"),d;for(d in a)c[d]=a[d];b&&c.setAttribute("class",b);e.parentNode.insertBefore(c,e)}var d=e.Mobify={},c=d.Tag={};d.points=[+new Date];d.tagVersion=[7,0];c.ua=e.navigator.userAgent;c.getOptions=h;c.init=function(a){c.options=a;if(""!==g("mobify-path"))if(m(),a.skipPreview||"true"!=g("mobify-path")&&!/mobify-path=true/.test(e.location.hash)){var b=h(a);if(b){var d=function(){b.post&&b.post()};a=function(){b.pre&&b.pre();k({id:"mobify-js",src:b.url,onerror:n,onload:d},"mobify")};!1===b.capture?a():l(a)}}else l(p)}})(window,document);(function(){var e="//cdn.mobify.com/sites/eddiebauer-mobile/eddie-bauer-production/adaptive.min.js";var t="//cdn.mobify.com/sites/eddiebauer-tablet/eddie-bauer-production/adaptive.min.js";Mobify.Tag.init({mode:function(e){if(/ipad|android(?!.*mobile)(?!.*firefox)/i.test(e)){return"tablet"}else if(/^((?!windows\sphone).)*(ip(hone|od)|android.*(mobile)(?!.*firefox))/i.test(e)){return"smartphone"}return"desktop"},smartphone:{url:e},tablet:{url:t},desktop:{capture:false,url:"//a.mobify.com/eddiebauer-mobile/a.js"}})})();/*]]>*/
</script>
<meta name="checkoutExperience" content="new"/>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9,IE=edge,chrome=1">
<title>Privacy And Security | Company Information | Eddie Bauer</title>
<meta name="description" content="Find information about Eddie Bauer Privacy And Security in Company Information">
<meta name="robots" content="index,follow">
<link rel="shortcut icon" href="/static/img/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="/static/img/icons/apple-touch-icon-precomposed.png"><!-- 57×57px -->
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/img/icons/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/img/icons/apple-touch-icon-76x76-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/img/icons/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/img/icons/apple-touch-icon-120x120-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/img/icons/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/img/icons/apple-touch-icon-152x152-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/static/img/icons/apple-touch-icon-180x180-precomposed.png">
<link rel="icon" sizes="192x192" href="/static/img/icons/touch-icon-192x192.png">
<link rel="stylesheet" href="/static/combined/css/styles1.css">
<link rel="stylesheet" href="/static/css/main.css">
<link rel="stylesheet" href="/static/css/checkout.css">
<link rel="stylesheet" href="/static/css/account.css">
<link rel="stylesheet" href="/static/css/stylesheet-image-based.css">
<link rel="stylesheet" href="/static/css/footer/footer.css">
<link rel="stylesheet" href="/static/css/header/header.css">
<link rel="stylesheet" href="/static/css/footer/footer-light.css">
<link rel="stylesheet" href="/static/combined/css/styles2.css">
<link rel="stylesheet" href="/static/css/print.css" media="print">
<link rel="stylesheet" href="/static/css/custserv.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/static/js/jquery/jquery-1.7.2.min.js"><\/script>')</script>
<script src="/static/combined/js/script14.js"></script>
<!-- Oracle Chat -->
<script type="text/javascript" src="//eddiebauer.widget.custhelp.com/euf/rightnow/RightNow.Client.js"></script>
<script type="text/javascript">
$( document ).ready(function() {
RightNow.Client.Controller.addComponent(
{
container_element_id: "myChatLinkContainerCstSrv",
info_element_id: "myChatLinkInfoCstSrv",
link_element_id: "myChatLinkCstSrv",
instance_id: "sccl_3",
module: "ConditionalChatLink",
custom_fields: "{\"2\":\"1\"}",
type: 7
},
"//eddiebauer.widget.custhelp.com/ci/ws/get"
);
});
</script>
<style>
#myChatLinkInfoCstSrv {
height:58px !important;
margin-bottom:10px;
padding-left:-10px;
}
#myChatLinkCstSrv.rn_ChatAvailable {
height:58px;
width:214px;
margin-bottom:20px;
background-image: url("/static/img/chat-phone_live-chat.png");
}
#myChatLinkCstSrv.rn_ChatAvailable img {
visibility:hidden;
}
#myChatLinkCstSrv.rn_ChatAvailable a {
height:58px;
margin-bottom:20px;
line-height:20px;
}
#myChatLinkCstSrv.rn_ChatAvailable span {
height:25px;
visibility:hidden;
}
#myChatLinkInfoCstSrv span {
height:25px;
visibility:hidden;
}
</style>
<script>
loginLoaded = false;
loginData = '';
var rewardsTierName = '';
//search scope asynchronous call
var url = '/xhr/login-status-header.jsp';
try {
var hostname = window.location.hostname;
var pathname = window.location.pathname;
if (hostname.indexOf("ebi") > -1 && pathname.indexOf("index.jsp") > -1) {
var assocId = decodeURIComponent((new RegExp('[?|&]assocId=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
if (assocId) {
url += '?assocId=' + assocId;
}
}
} catch (err) {
console.log('error');
}
var loc = location.href;
if(loc.indexOf('DPSLogout') >= 0 && loc.indexOf('logout.jsp') < 0) {
url += '?DPSLogout=true';
}
$.ajax({
type:'get',
url:url,
cache: false,
dataType: 'html',
success: function(data) {
loginData = data;
if($('#headShoppingHolder').length) {
if(!loginLoaded) {
$('.login-bag-links').remove();
$('#header-login-cart').append(data);
$('.welcome-name-header').text($('#welcome-name-hidden').text());
}
loginLoaded = true;
} else {
$(document).ready(function() {
if(!loginLoaded) {
$('.login-bag-links').remove();
$('#header-login-cart').append(data);
$('.welcome-name-header').text($('#welcome-name-hidden').text());
}
loginLoaded = true;
});
}
rewardsTierName = $(data).filter('#rewardsTierName').val();
//console.log('load-login-bag-header :'+rewardsTierName)
loadHeaderRail("/includes/freeshipping-sitewide-rail.jsp?tierName=" + rewardsTierName, "#headContentHolder");
loadHeaderRail("/includes/headerRail.jsp?tierName=" + rewardsTierName+"&bodyClass=custserv-page customer-service company-info company-info-privacy-and-security","#header-rail");
}
});
</script>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="/pwr/engine/js/full.js"></script>
<script src="/static/js/global.js"></script>
<style type="text/css">.checkout-bag{display:none;}</style>

                    <script>var w=window;if(w.performance||w.mozPerformance||w.msPerformance||w.webkitPerformance){var d=document;AKSB=w.AKSB||{},AKSB.q=AKSB.q||[],AKSB.mark=AKSB.mark||function(e,_){AKSB.q.push(["mark",e,_||(new Date).getTime()])},AKSB.measure=AKSB.measure||function(e,_,t){AKSB.q.push(["measure",e,_,t||(new Date).getTime()])},AKSB.done=AKSB.done||function(e){AKSB.q.push(["done",e])},AKSB.mark("firstbyte",(new Date).getTime()),AKSB.prof={custid:"82127",ustr:"",originlat:"0",clientrtt:"1",ghostip:"204.237.221.159",ipv6:false,pct:"10",clientip:"207.241.225.246",requestid:"16cfb450",region:"29559",protocol:"",blver:14,akM:"a",akN:"ae",akTT:"O",akTX:"1",akTI:"16cfb450",ai:"165668",ra:"false",pmgn:"",pmgi:"",pmp:"",qc:""},function(e){var _=d.createElement("script");_.async="async",_.src=e;var t=d.getElementsByTagName("script"),t=t[t.length-1];t.parentNode.insertBefore(_,t)}(("https:"===d.location.protocol?"https:":"http:")+"//ds-aksb-a.akamaihd.net/aksb.min.js")}</script>
                    <script src="/__ssobj/core.js+ssdomvar.js+generic-adapter.js"></script></head>
<body class="custserv-page customer-service company-info company-info-privacy-and-security"><!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->
<script>
(function(a,b,c,d){
a='//tags.tiqcdn.com/utag/eddiebauer/desktopeb/prod/utag.js';
b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
})();
</script>
<div id="tealium-utag-data">
<script>

function GetQueryStringParams( name ){
name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
var regexS = "[\\?&]"+name+"=([^&#]*)",
regex = new RegExp( regexS ),
results = regex.exec( window.location.href );
if( results == null ){
return "";
} else{
return decodeURIComponent(results[1].replace(/\+/g, " "));
}
};
var pSrch = GetQueryStringParams('pSrch') || "";
var Ntt = GetQueryStringParams('Ntt') || "";
if(pSrch !=""){
var	pScrhWord = pSrch;
var pScrhPredictiveSearch = ["Yes"];
}else{
var	pScrhWord = "";
var pScrhPredictiveSearch = [""];
};
//alert("Getting to utag_data");
var utag_data = {

page_name : "Privacy And Security | Company Information",
sitespect : "27213:835599|36077:1047549",
category_id : "C22376"

}
</script>
</div>
<!--<script>
$(document).ready(function () {
var rewardsTierName="";
loadHeaderRail("/includes/freeshipping-sitewide-rail.jsp"+(rewardsTierName != ""  ? "?tierName=" + rewardsTierName : "?tierName=anonymous"), "#headContentHolder");
loadHeaderRail("/includes/headerRail.jsp?"+(rewardsTierName != ""  ? "tierName=" + rewardsTierName+"&" : "tierName=anonymous&")+"bodyClass=custserv-page customer-service company-info company-info-privacy-and-security","#header-rail");
});
</script>-->
<!-- for the acc-login.jsp alone, we are setting this flag since that page can be reached from within the checkout and its bodyclass will chage at that time -->
<div id="headerBg"></div>
<header id="pageTop" class="mainHeader" data-endeca-cartridge="headerMain3Slots">
<div id="headerMid">
<div class="header-item">
<div id="ebLogo">
<a href="/">
<img src="/static/img/logo_eb_header.svg" alt="Eddie Bauer(R). EST.1920">
</a>
</div>
</div>
<div class="header-item">
</div>
<div class="header-item">
<div id="headSearchHolder">
<form id="headSearchForm" class="clearfix" action="/includes/searchInter.jsp" method="get" autocomplete="off">
<div class="form-item right clearfix">
<input type="hidden" name="Dy" value="1" />
<input type="hidden" name="Nty" value="1" />
<input id="searchScopeValue" type="hidden" name="N" value="0" />
<input id="searchScopeValue" type="hidden" name="previousPage" value="SRC" />
<input class="left text-input" title="Search by item number or keyword" id="searchText" type="text" name="Ntt" placeholder="Search by Item # or Keyword" />
<input id="headSearchSubmit" class="button left" type="image" src="/static/img/icon_search.svg" alt="GO" />
</div>
<div id="headSearchCategoryHolder" class="left">
<a class="ut-trigger" href="#"><span id="triggerTitle">All</span><span class="downArrow">&nbsp;&#9660;</span></a>
<div id="headSearchCats" class="ut-flyout"></div>
</div>
</form>
<script type="text/javascript">
if(false){
var snapDefaultConfig = {
accountId: "5034",
authKey: "7n0es2mr3scz6ptx",
domainKey: "eddiebauer_com",
request_type: "suggest",
enableSnapSearch: false,
minAutoSuggestInputLength: "3",
delay: 250,
autoSuggestServiceUrl: "http://brm-suggest-0.brsrvr.com/api/v1/suggest/",
containerClass: "dimSearchSuggestContainer"
};
}
else{
var	searchSuggestObject = {
minAutoSuggestInputLength:"3",
autoSuggestServiceUrl:"/autosuggestjson.jsp",
collection:"/content/Shared/Auto-Suggest Panels",
searchUrl:"",
containerClass:"dimSearchSuggestContainer",
defaultImage:""
};
}
</script>
</div>
</div>
<div id="utilityNav">
<ul id="header-login-cart" class="clearfix nav-list">
</ul>
<script>
if($('#headShoppingHolder').length) {
if(!loginLoaded && loginData != '') {
$('.login-bag-links').remove();
$('#header-login-cart').append(loginData);
$('.welcome-name-header').text($('#welcome-name-hidden').text());
loginLoaded = true;
}
}
</script>
</div>
<div id="utilityNav">
<ul id="header-login-cart" class="clearfix nav-list">
<li style="padding: 0 0px; font-size : 9px">
<form id="languageSwitcher" name="languageSwitcher" action="/includes/?_DARGS=/includes/languages.jsp.languageSwitcher" method="post"><input name="_dyncharset" value="UTF-8" type="hidden"></input><input name="_dynSessConf" value="-4415807349612252502" type="hidden"></input><input id="locale" name="locale" value="en_US" type="hidden"><input name="_D:locale" value=" " type="hidden"><input name="/atg/userprofiling/ProfileFormHandler.update" value="go" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.update" value=" " type="hidden"><input name="/atg/userprofiling/ProfileFormHandler.loginErrorURL" value="login.jsp" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.loginErrorURL" value=" " type="hidden"><input name="/atg/userprofiling/ProfileFormHandler.loginSuccessURL" value="login.jsp" type="hidden"><input name="_D:/atg/userprofiling/ProfileFormHandler.loginSuccessURL" value=" " type="hidden"><input name="_DARGS" value="/includes/languages.jsp.languageSwitcher" type="hidden"></input></form></li>
</ul>
</div>
</div>
<!-- <div class="header-third-slot"> -->
<div class="header-item">
<!-- TODO change to ENDECA!!! -->
<div id="mainNav" role="navigation" aria-label="Main menu">
<ul class="nav-list" role="menubar">
<li role="menuitem">
<a id="nav-men" class="" href="/browse/men/_/N-26y3?tab=men&cm_sp=topnav-_-Men-_-main&previousPage=GNAV" aria-controls="flyout-men" aria-expanded="false" aria-owns="flyout-men" aria-haspopup="true">
Men</a>
<li role="menuitem">
<a id="nav-women" class="" href="/browse/women/_/N-278f?tab=women&cm_sp=topnav-_-Women-_-main&previousPage=GNAV" aria-controls="flyout-women" aria-expanded="false" aria-owns="flyout-women" aria-haspopup="true">
Women</a>
<li role="menuitem">
<a id="nav-outerwear" class="" href="/outerwear/?tab=outerwear&cm_sp=topnav-_-Outerwear-_-main&previousPage=GNAV" aria-controls="flyout-outerwear" aria-expanded="false" aria-owns="flyout-outerwear" aria-haspopup="true">
Outerwear</a>
<li role="menuitem">
<a id="nav-shoes" class="" href="/shoes/?tab=shoes&cm_sp=topnav-_-Shoes-_-main&previousPage=GNAV" aria-controls="flyout-shoes" aria-expanded="false" aria-owns="flyout-shoes" aria-haspopup="true">
Shoes</a>
<li role="menuitem">
<a id="nav-home" class="" href="/browse/home/_/N-275E?tab=home&cm_sp=topnav-_-Home-_-main&previousPage=GNAV" aria-controls="flyout-home" aria-expanded="false" aria-owns="flyout-home" aria-haspopup="true">
Home</a>
<li role="menuitem">
<a id="nav-bagsgear" class="" href="/browse/gear/_/N-2752?tab=gear&cm_sp=topnav-_-Bags&Gear-_-main&previousPage=GNAV" aria-controls="flyout-bagsgear" aria-expanded="false" aria-owns="flyout-bagsgear" aria-haspopup="true">
Bags & Gear</a>
<li role="menuitem">
<a id="nav-sale" class="" href="/browse/sale/_/N-1z13XM6?tab=sale&cm_sp=topnav-_-Sale-_-main&previousPage=GNAV" aria-controls="flyout-sale" aria-expanded="false" aria-owns="flyout-sale" aria-haspopup="true">
Sale</a>
<li role="menuitem">
<a id="nav-clearance" class="" href="/browse/clearance/_/N-y?isClearCat=true&tab=clearance&cm_sp=topnav-_-Clearance-_-main&previousPage=GNAV" aria-controls="flyout-clearance" aria-expanded="false" aria-owns="flyout-clearance" aria-haspopup="true">
Clearance</a>
<li role="menuitem">
<a id="nav-guidetoeb" class="" href="/guidetoeb?cm_sp=topnav-_-GUIDETOEB-_-main&previousPage=GNAV" aria-controls="flyout-guidetoeb" aria-expanded="false" aria-owns="flyout-guidetoeb" aria-haspopup="true">
GUIDE TO EB</a>
</ul>
</div>
<div id="navFlyouts">
<div id="flyout-men" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-men">
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p class="category-focus" aria-level="2">Shop by category</p></li><li><a href="/browse/outerwear/men/_/N-276b?cm_sp=sub-_-Men-_-Outerwear&currentNode=outerwear&tab=men&previousPage=GNAV">Outerwear</a></li><li><a href="/browse/fleece/men/_/N-27ap?cm_sp=sub-_-Men-_-Fleece&currentNode=fleece&tab=men&previousPage=GNAV">Fleece</a></li><li><a href="/browse/pants/men/_/N-276h?cm_sp=sub-_-Men-_-Pants&currentNode=pants&tab=men&previousPage=GNAV">Pants</a></li><li><a href="/browse/flannel/men/_/N-27ch?cm_sp=sub-_-Men-_-Flannel&currentNode=flannel&tab=men&previousPage=GNAV">Flannel</a></li><li><a href="/browse/t-shirts---polos/men/_/N-27ca?cm_sp=sub-_-Men-_-TShirtsPolos&currentNode=t-shirts---polos&tab=men&previousPage=GNAV">T-Shirts & Polos</a></li><li><a href="/browse/shirts/men/_/N-277c?cm_sp=sub-_-Men-_-Shirts&currentNode=shirts&tab=men&previousPage=GNAV">Shirts</a></li><li><a href="/browse/jeans/men/_/N-276a?cm_sp=sub-_-Men-_-Jeans&currentNode=jeans&tab=men&previousPage=GNAV">Jeans</a></li><li><a href="/browse/shorts/men/_/N-276z?cm_sp=sub-_-Men-_-Shorts&currentNode=shorts&tab=men&previousPage=GNAV">Shorts</a></li><li><a href="/browse/blazers---jackets/men/_/N-27cb?cm_sp=sub-_-Men-_-BlazersJackets&currentNode=blazers---jackets&tab=men&previousPage=GNAV">Blazers & Jackets</a></li><li><a href="/browse/sweatshirts---hoodies/men/_/N-2778?cm_sp=sub-_-Men-_-SweatshirtsHoodies&currentNode=sweatshirts---hoodies&tab=men&previousPage=GNAV">Sweatshirts & Hoodies</a></li><li><a href="/browse/sweaters/men/_/N-2774?cm_sp=sub-_-Men-_-Sweaters&currentNode=sweaters&tab=men&previousPage=GNAV">Sweaters</a></li><li><a href="/browse/sleepwear/men/_/N-2773?cm_sp=sub-_-Men-_-Sleepwear&currentNode=sleepwear&tab=men&previousPage=GNAV">Sleepwear</a></li><li><a href="/browse/baselayers/men/_/N-2767?cm_sp=sub-_-Men-_-Baselayers&currentNode=baselayers&tab=men&previousPage=GNAV">Baselayers</a></li><li><a href="/browse/shoes/men/_/N-276q?cm_sp=sub-_-Men-_-Shoes&currentNode=shoes&tab=men&previousPage=GNAV">Shoes</a></li><li><a href="/browse/accessories/men/_/N-26y4?cm_sp=sub-_-Men-_-Accessories&currentNode=accessories&tab=men&previousPage=GNAV">Accessories</a></li><style type="text/css">
.nav_list li a {white-space:nowrap;}
#customProductMainContentHolder #controls ul#icons li .hover-label {
    margin-bottom:4px;
}
.base-modal h2 {font-size:23px !important;}
.custom-jcarousel-prev,
.custom-jcarousel-next {outline:0 none !important;}
.pdp-atc-details {
    font-size: 12px;
    line-height: 14px !important;
}
#mainNav {letter-spacing:0.02em;}
.nav-list, .non-list {
    list-style-type: none !important;
    margin: -2px 0 0;
    padding: 0;
}
.certona-jcarousel-next {
    background-color: #fff;
    height: 263px;
    margin-left: 35px;
}
</style>
<script>
$("#fbOuterClose").click(function() {
    location.reload(true);
});
$( document ).ready(function() {
if (window.location.pathname == "/checkout/bag.jsp") {
    $(".certona-jcarousel-next").css("margin-left", "0px");
}

if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
    console.log("You'll only see this in Chrome");
    jQuery('.deprecated #mainNav a:link').css('font-size', '13.8px !important');
}


});
</script></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<div class="f-col left" role="columnheader">
	<ul class="nav-list" role="list">
		<li role="listitem">
			<p role="heading">
				Featured Collections</p>
		</li>
		<li>
			<a href="/browse/men/_/N-1z13xeyZ26y3?tab=men&amp;cm_sp=topnav-_-Men-_-main&amp;previousPage=GNAV&amp;fq=FSM_NewProduct:new">New Arrivals</a></li>
		<li role="listitem">
			<a href="/lookbooks/us-lookbook?previouspage=GNAV&amp;cm_sp=topnav-_-men-_-Lookbooks" role="link">Shop our Lookbooks</a></li>
		<li role="listitem">
			<a href="/lookbooks/us-landing-page#catalog/outerwear-guide-m/page/1" role="link">Outerwear Guide</a></li>
		<li role="listitem">
			<a href="/lookbooks/us-landing-page#catalog/microtherm/page/1?previouspage=GNAV&amp;cm_sp=topnav-_-men-_-microtherm_180925" role="link">Microtherm</a></li>
		<li role="listitem">
			<a href="/campaign/rain?previouspage=GNAV&amp;cm_sp=topnav-_-men-_-RainCampaign" role="link">Waterproof Rainwear</a></li>
		<li role="listitem">
			<a href="/search/a-search.jsp?sTerm=WFMens&amp;_requestid=22324&amp;previousPage=GNAV&amp;cm_sp=topnav-_-men-_-WRINKLEFREE" role="link">Wrinkle-Free Shirts &amp; Pants</a></li>
		<li role="listitem">
			<a href="/lookbooks/us-landing-page#catalog/evertherm-mens/page/1" role="link">EverTherm</a></li>
		<li role="seperator" style="line-height:0px;">
			<hr style="margin-top:7px;margin-bottom:6px;" />
		</li>
		<li role="listitem">
			<a href="/browse/tall/men/_/N-1z13yphZ26y3?isLeftNav=true&amp;tab=men&amp;previousPage=FNAV&amp;currentNode=Tall" role="link">Tall Shop</a></li>
		<li role="seperator">
			&nbsp;</li>
		<li role="listitem">
			<p role="heading">
				Specials</p>
		</li>
		<li role="listitem">
			<a href="/browse/men/_/N-1z13xm6Z26y3?Endeca_user_segments=men&amp;currentNode=Men&amp;tab=sale&amp;previousPage=GNAV" role="link">Sale</a></li>
		<li role="listitem">
			<a href="/browse/men/_/N-yZ26y3?currentNode=Men&amp;tab=clearance&amp;isClearCat=true&amp;previousPage=GNAV" role="link">Clearance</a></li>
	</ul>
</div>
<style type="text/css">
#product2_rr a:link,
    #product2_rr a:active,
    #product2_rr a:visited,
    #categoryLanding_rr a:link,
    #categoryLanding_rr a:active,
    #categoryLanding_rr a:visited,
    #nosearch2_rr a:link,
    #nosearch2_rr a:active,
    #nosearch2_rr a:visited,
    #cart2_rr a:link,
    #cart2_rr a:active,
    #cart2_rr a:visited {
        outline: 0 none;
    }</style>
<script>
var original_jquery_ajax = $.ajax;
$.ajax = function () {
    var a_fn, a_url;
    var cb = function (data, status, settings) {
        a_fn(data, status, settings);
    }
    for (var i = 0; i < arguments.length; i++)
        if (arguments[i] instanceof Object) {
            if (arguments[i].success) {
                a_fn = arguments[i].success; arguments[i].success = cb;
            }
            if (arguments[i].url) a_url = arguments[i].url;
        }
    if (typeof (arguments[0]) === "string") a_url = arguments[0];
    var aj = original_jquery_ajax.apply(null, arguments);
    var done_original = aj.done;
    aj.done = function (cb_fn) { 
        a_fn = cb_fn;
        done_original(cb);
        return aj;
    };
    
    return aj;
};
</script></ul>
</div>
<div class="f-col left">
<ul class="nav-list">
<ul class="nav-list" role="list">
	<li role="listitem">
		<p role="heading">
			Shop By Activity</p>
	</li>
</ul>
<div class="shopby-header">
	<a role="link" href="/browse/first-ascent/_/N-274k?tab=first-ascent&amp;cm_sp=topnav-_-FirstAscent-_-main&amp;previousPage=GNAV">
        <img alt="First Ascent" src="/static/img/logo_fa.svg" style="height:22px;width:121px;border:0;" /> 
    </a>
	<p>
		<a role="link" href="/browse/first-ascent/_/N-274k?tab=first-ascent&amp;cm_sp=topnav-_-FirstAscent-_-main&amp;previousPage=GNAV">Mountain Sports</a></p>
</div>
<div class="shopby-divider" role="separator">
	&nbsp;</div>
<div class="shopby-header">
	<a role="link" href="/browse/motion/_/N-27bk?tab=motion&amp;cm_sp=topnav-_-Motion-_-main&amp;previousPage=GNAV">
        <img alt="Motion" src="/static/img/logo_motion.svg" style="height:22px;width:121px;border:0;" /> </a>
	<p>
		<a role="link" href="/browse/motion/_/N-27bk?tab=motion&amp;cm_sp=topnav-_-Motion-_-main&amp;previousPage=GNAV">Training</a></p>
</div>
<div class="shopby-divider" role="separator">
	&nbsp;</div>
<div class="shopby-header">
	<a role="link" href="/browse/travex/_/N-277Y?tab=travex&amp;cm_sp=topnav-_-Travex-_-main&amp;previousPage=GNAV">
        <img alt="Travex" src="/static/img/logo_travex.svg" style="height:22px;width:121px;border:0;" /> </a>
	<p>
		<a role="link" href="/browse/travex/_/N-277Y?tab=travex&amp;cm_sp=topnav-_-Travex-_-main&amp;previousPage=GNAV">Hiking &amp; Travel</a></p>
</div>
<div class="shopby-divider" role="separator">
	&nbsp;</div>
<div class="shopby-header">
	<a role="link" href="/browse/sport-shop/_/N-277n?tab=sport-shop&amp;cm_sp=topnav-_-SportShop-_-main&amp;previousPage=GNAV">
        <img alt="Sportshop" src="/static/img/logo_sportshop.svg" style="height:22px;width:121px;border:0;" /> </a>
	<p>
		<a role="link" href="/browse/sport-shop/_/N-277n?tab=sport-shop&amp;cm_sp=topnav-_-SportShop-_-main&amp;previousPage=GNAV">Fishing &amp; Field</a></p>
</div></ul>
</div>
</div>
<div id="flyout-women" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-women">
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p class="category-focus" aria-level="2">Shop by Category</p></li><li><a href="/browse/outerwear/women/_/N-2792?cm_sp=sub-_-Women-_-Outerwear&currentNode=outerwear&tab=women&previousPage=GNAV">Outerwear</a></li><li><a href="/browse/fleece/women/_/N-27as?cm_sp=sub-_-Women-_-Fleece&currentNode=fleece&tab=women&previousPage=GNAV">Fleece</a></li><li><a href="/browse/pants---capris/women/_/N-2798?cm_sp=sub-_-Women-_-PantsCapris&currentNode=pants---capris&tab=women&previousPage=GNAV">Pants & Capris</a></li><li><a href="/browse/flannel/women/_/N-27ci?cm_sp=sub-_-Women-_-Flannel&currentNode=flannel&tab=women&previousPage=GNAV">Flannel</a></li><li><a href="/browse/tops/women/_/N-279z?cm_sp=sub-_-Women-_-Tops&currentNode=tops&tab=women&previousPage=GNAV">Tops</a></li><li><a href="/browse/t-shirts---tanks/women/_/N-27cc?cm_sp=sub-_-Women-_-TShirtsTanks&currentNode=t-shirts---tanks&tab=women&previousPage=GNAV">T-Shirts & Tanks</a></li><li><a href="/browse/jeans/women/_/N-2791?cm_sp=sub-_-Women-_-Jeans&currentNode=jeans&tab=women&previousPage=GNAV">Jeans</a></li><li><a href="/browse/shorts/women/_/N-278u?cm_sp=sub-_-Women-_-Shorts&currentNode=shorts&tab=women&previousPage=GNAV">Shorts</a></li><li><a href="/browse/dresses---skirts/women/_/N-278y?cm_sp=sub-_-Women-_-DressesSkirts&currentNode=dresses---skirts&tab=women&previousPage=GNAV">Dresses & Skirts</a></li><li><a href="/browse/blazers---jackets/women/_/N-278t?cm_sp=sub-_-Women-_-BlazersJackets&currentNode=blazers---jackets&tab=women&previousPage=GNAV">Blazers & Jackets</a></li><li><a href="/browse/sweatshirts---hoodies/women/_/N-27bg?cm_sp=sub-_-Women-_-SweatshirtsHoodies&currentNode=sweatshirts---hoodies&tab=women&previousPage=GNAV">Sweatshirts & Hoodies</a></li><li><a href="/browse/sweaters/women/_/N-27bh?cm_sp=sub-_-Women-_-Sweaters&currentNode=sweaters&tab=women&previousPage=GNAV">Sweaters</a></li><li><a href="/browse/sleepwear/women/_/N-279o?cm_sp=sub-_-Women-_-Sleepwear&currentNode=sleepwear&tab=women&previousPage=GNAV">Sleepwear</a></li><li><a href="/browse/baselayers/women/_/N-278q?cm_sp=sub-_-Women-_-Baselayers&currentNode=baselayers&tab=women&previousPage=GNAV">Baselayers</a></li><li><a href="/browse/shoes/women/_/N-279f?cm_sp=sub-_-Women-_-Shoes&currentNode=shoes&tab=women&previousPage=GNAV">Shoes</a></li><li><a href="/browse/accessories/women/_/N-278g?cm_sp=sub-_-Women-_-Accessories&currentNode=accessories&tab=women&previousPage=GNAV">Accessories</a></li></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<div class="f-col left" role="columnheader">
	<ul class="nav-list" role="list">
		<li role="listitem">
			<p role="heading">
				FEATURED COLLECTIONS</p>
		</li>
		<li>
			<a href="/browse/women/_/N-1z13xeyZ278f?tab=women&amp;cm_sp=topnav-_-Women-_-main&amp;previousPage=GNAV&amp;fq=FSM_NewProduct:new">New Arrivals</a></li>
		<li role="listitem">
			<a href="/lookbooks/us-lookbook?previouspage=GNAV&amp;cm_sp=topnav-_-women-_-Lookbooks" role="link">Shop our Lookbooks</a></li>
		<li role="listitem">
			<a href="/lookbooks/us-landing-page#catalog/outerwear-guide-w/page/1" role="link">Outerwear Guide</a></li>
		<li role="listitem">
			<a href="/lookbooks/us-landing-page#catalog/microtherm/page/1?previouspage=GNAV&amp;cm_sp=topnav-_-women-_-microtherm_180925" role="link">Microtherm</a></li>
		<li role="listitem">
			<a href="/campaign/rain?previouspage=GNAV&amp;cm_sp=topnav-_-women-_-RainCampaign" role="link">Waterproof Rainwear</a></li>
		<li role="listitem">
			<a href="/search/a-search.jsp?sTerm=WomensWrinkleFreeShirtsCollection&amp;previousPage=GNAV&amp;cm_sp=topnav-_-women-_-WRINKLEFREE_180518" role="link">Wrinkle-Free</a></li>
		<li role="listitem">
			<a href="/lookbooks/us-landing-page#catalog/evertherm-womens/page/1" role="link">EverTherm</a></li>
		<li role="seperator" style="line-height:0px;">
			<hr style="margin-top:7px;margin-bottom:6px;" />
		</li>
		<li role="listitem">
			<a href="/browse/petite/women/_/N-1z13ypcZ278f?isLeftNav=true&amp;currentNode=Petite&amp;tab=women&amp;previousPage=GNAV&amp;fq=fsm_regular_style:Petite" role="link">Petite Shop</a></li>
		<li role="listitem">
			<a href="/browse/tall/women/_/N-1z13yphZ278f?isLeftNav=true&amp;currentNode=Tall&amp;tab=women&amp;previousPage=GNAV&amp;fq=fsm_regular_style:Tall" role="link">Tall Shop</a></li>
		<li role="listitem">
			<a href="/browse/plus/women/_/N-1z13xdjZ278f?isLeftNav=true&amp;currentNode=Plus&amp;tab=women&amp;previousPage=GNAV&amp;fq=fsm_regular_style:Plus" role="link">Plus Shop</a></li>
		<li role="seperator">
			&nbsp;</li>
		<li role="listitem">
			<p role="heading">
				SPECIALS</p>
		</li>
		<li role="listitem">
			<a href="/browse/women/_/N-1z13xm6Z278f?Endeca_user_segments=women&amp;currentNode=Women&amp;tab=Sale&amp;previousPage=GNAV" role="link">Sale</a></li>
		<li role="listitem">
			<a href="/browse/women/_/N-yZ278f?currentNode=Women&amp;tab=clearance&amp;isClearCat=true&amp;previousPage=GNAV" role="link">Clearance</a></li>
	</ul>
</div></ul>
</div>
<div class="f-col left">
<ul class="nav-list">
<ul class="nav-list">
	<li>
		<p role="heading">
			Shop By Activity</p>
	</li>
</ul>
<div class="shopby-header">
	<a href="/browse/first-ascent/_/N-274k?tab=first-ascent&amp;cm_sp=topnav-_-FirstAscent-_-main&amp;previousPage=GNAV" role="link"><img alt="First Ascent" src="/static/img/logo_fa.svg" style="height:22px;width:121px;border:0;" /> </a>
	<p>
		<a href="/browse/first-ascent/_/N-274k?tab=first-ascent&amp;cm_sp=topnav-_-FirstAscent-_-main&amp;previousPage=GNAV" role="link">Mountain Sports</a></p>
</div>
<div class="shopby-divider" role="separator">
	&nbsp;</div>
<div class="shopby-header">
	<a href="/browse/motion/_/N-27bk?tab=motion&amp;cm_sp=topnav-_-Motion-_-main&amp;previousPage=GNAV" role="link"><img alt="Motion" src="/static/img/logo_motion.svg" style="height:22px;width:121px;border:0;" /> </a>
	<p>
		<a href="/browse/motion/_/N-27bk?tab=motion&amp;cm_sp=topnav-_-Motion-_-main&amp;previousPage=GNAV" role="link">Training</a></p>
</div>
<div class="shopby-divider" role="separator">
	&nbsp;</div>
<div class="shopby-header">
	<a href="/browse/travex/_/N-277Y?tab=travex&amp;cm_sp=topnav-_-Travex-_-main&amp;previousPage=GNAV" role="link"><img alt="Travex" src="/static/img/logo_travex.svg" style="height:22px;width:121px;border:0;" /> </a>
	<p>
		<a href="/browse/travex/_/N-277Y?tab=travex&amp;cm_sp=topnav-_-Travex-_-main&amp;previousPage=GNAV" role="link">Hiking &amp; Travel</a></p>
</div>
<div class="shopby-divider" role="separator">
	&nbsp;</div>
<div class="shopby-header">
	<a href="/browse/sport-shop/_/N-277n?tab=sport-shop&amp;cm_sp=topnav-_-SportShop-_-main&amp;previousPage=GNAV" role="link"><img alt="Sportshop" src="/static/img/logo_sportshop.svg" style="height:22px;width:121px;border:0;" /> </a>
	<p>
		<a href="/browse/sport-shop/_/N-277n?tab=sport-shop&amp;cm_sp=topnav-_-SportShop-_-main&amp;previousPage=GNAV" role="link">Fishing &amp; Field</a></p>
</div></ul>
</div>
</div>
<div id="flyout-outerwear" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-outerwear">
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/outerwear/men/_/N-276b?cm_sp=sub-_-men-_-outerwear&currentNode=outerwear&tab=men&previousPage=GNAV">Men</a></p></li><li><a href="/browse/jackets/outerwear/men/_/N-276d?cm_sp=sub-_-Men-_-OuterwearJackets&currentNode=jackets&tab=men&previousPage=GNAV">Jackets</a></li><li><a href="/browse/parkas/outerwear/men/_/N-27ag?cm_sp=sub-_-Men-_-OuterwearParkas&currentNode=parkas&tab=men&previousPage=GNAV">Parkas</a></li><li><a href="/browse/vests/outerwear/men/_/N-276g?cm_sp=sub-_-Men-_-OuterwearVests&currentNode=vests&tab=men&previousPage=GNAV">Vests</a></li><li><a href="/browse/trenches---coats/outerwear/men/_/N-27ah?cm_sp=sub-_-Men-_-OuterwearTrenchesCoats&currentNode=trenches---coats&tab=men&previousPage=GNAV">Trenches & Coats</a></li><li><a href="/browse/performance-fleece/outerwear/men/_/N-276f?cm_sp=sub-_-Men-_-OuterwearPerformanceFleece&currentNode=performance-fleece&tab=men&previousPage=GNAV">Performance Fleece</a></li><li><a href="/browse/pants--rain--snow--ski/outerwear/men/_/N-276e?cm_sp=sub-_-Men-_-OuterwearPantsRainSnowSki&currentNode=pants--rain--snow--ski&tab=men&previousPage=GNAV">Pants: Rain, Snow, Ski</a></li><li><a href="/browse/down-insulated/outerwear/men/_/N-27bv?cm_sp=sub-_-Men-_-OuterwearDownInsulated&currentNode=down-insulated&tab=men&previousPage=GNAV">Down Insulated</a></li><li><a href="/browse/synthetic-insulated/outerwear/men/_/N-27bw?cm_sp=sub-_-Men-_-OuterwearSyntheticInsulated&currentNode=synthetic-insulated&tab=men&previousPage=GNAV">Synthetic Insulated</a></li><li><a href="/browse/rain/outerwear/men/_/N-27bx?cm_sp=sub-_-Men-_-OuterwearRain&currentNode=rain&tab=men&previousPage=GNAV">Rain</a></li><li><a href="/browse/soft-shells/outerwear/men/_/N-27by?cm_sp=sub-_-Men-_-OuterwearSoftShells&currentNode=soft-shells&tab=men&previousPage=GNAV">Soft Shells</a></li></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/outerwear/women/_/N-2792?cm_sp=sub-_-women-_-outerwear&currentNode=outerwear&tab=women&previousPage=GNAV">Women</a></p></li><li><a href="/browse/parkas/outerwear/women/_/N-27ak?cm_sp=sub-_-Women-_-OuterwearParkas&currentNode=parkas&tab=women&previousPage=GNAV">Parkas</a></li><li><a href="/browse/jackets/outerwear/women/_/N-2794?cm_sp=sub-_-Women-_-OuterwearJackets&currentNode=jackets&tab=women&previousPage=GNAV">Jackets</a></li><li><a href="/browse/vests/outerwear/women/_/N-2797?cm_sp=sub-_-Women-_-OuterwearVests&currentNode=vests&tab=women&previousPage=GNAV">Vests</a></li><li><a href="/browse/trenches---coats/outerwear/women/_/N-27al?cm_sp=sub-_-Women-_-OuterwearTrenchesCoats&currentNode=trenches---coats&tab=women&previousPage=GNAV">Trenches & Coats</a></li><li><a href="/browse/performance-fleece/outerwear/women/_/N-2796?cm_sp=sub-_-Women-_-OuterwearPerformanceFleece&currentNode=performance-fleece&tab=women&previousPage=GNAV">Performance Fleece</a></li><li><a href="/browse/pants--rain--snow--ski/outerwear/women/_/N-2795?cm_sp=sub-_-Women-_-OuterwearPantsRainSnowSki&currentNode=pants--rain--snow--ski&tab=women&previousPage=GNAV">Pants: Rain, Snow, Ski</a></li><li><a href="/browse/synthetic-insulated/outerwear/women/_/N-27c0?cm_sp=sub-_-Women-_-OuterwearSyntheticInsulated&currentNode=synthetic-insulated&tab=women&previousPage=GNAV">Synthetic Insulated</a></li><li><a href="/browse/soft-shells/outerwear/women/_/N-27c1?cm_sp=sub-_-Women-_-OuterwearSoftShells&currentNode=soft-shells&tab=women&previousPage=GNAV">Soft Shells</a></li><li><a href="/browse/rain/outerwear/women/_/N-27c2?cm_sp=sub-_-Women-_-OuterwearRain&currentNode=rain&tab=women&previousPage=GNAV">Rain</a></li><li><a href="/browse/hard-shells/outerwear/women/_/N-27c3?cm_sp=sub-_-Women-_-OuterwearHardShells&currentNode=hard-shells&tab=women&previousPage=GNAV">Hard Shells</a></li><li><a href="/browse/down-insulated/outerwear/women/_/N-27c4?cm_sp=sub-_-Women-_-OuterwearDownInsulated&currentNode=down-insulated&tab=women&previousPage=GNAV">Down Insulated</a></li></ul>
</div>
<div class="f-col left">
<ul class="nav-list">
<li role="listitem"><p role="heading"><a role="link" href="/browse/kids/_/N-275n?tab=kids&cm_sp=topnav-_-Kids-_-main&previousPage=GNAV">Kids</a></p></li><li><a href="/browse/boys/kids/_/N-275o?cm_sp=sub-_-Kids-_-Boys&currentNode=boys&tab=kids&previousPage=GNAV">Boys</a></li><li><a href="/browse/girls/kids/_/N-275t?cm_sp=sub-_-Kids-_-Girls&currentNode=girls&tab=kids&previousPage=GNAV">Girls</a></li></ul>
</div>
</div>
<div id="flyout-shoes" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-shoes">
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/shoes/men/_/N-276q?cm_sp=sub-_-men-_-shoes&currentNode=shoes&tab=men&previousPage=GNAV">Men</a></p></li><li><a href="/browse/boots/shoes/men/_/N-276r?cm_sp=sub-_-Men-_-ShoesBoots&currentNode=boots&tab=men&previousPage=GNAV">Boots</a></li><li><a href="/browse/hiking---trekking-shoes/shoes/men/_/N-276u?cm_sp=sub-_-Men-_-ShoesHikingTrekkingShoes&currentNode=hiking---trekking-shoes&tab=men&previousPage=GNAV">Hiking & Trekking Shoes</a></li><li><a href="/browse/lace-up-shoes/shoes/men/_/N-27b4?cm_sp=sub-_-Men-_-ShoesLaceUpShoes&currentNode=lace-up-shoes&tab=men&previousPage=GNAV">Lace-Up Shoes</a></li><li><a href="/browse/sandals/shoes/men/_/N-276x?cm_sp=sub-_-Men-_-ShoesSandals&currentNode=sandals&tab=men&previousPage=GNAV">Sandals</a></li><li><a href="/browse/slippers/shoes/men/_/N-276y?cm_sp=sub-_-Men-_-ShoesSlippers&currentNode=slippers&tab=men&previousPage=GNAV">Slippers</a></li><li><a href="/browse/casual-comfort-shoes/shoes/men/_/N-276s?cm_sp=sub-_-Men-_-ShoesCasualComfortShoes&currentNode=casual-comfort-shoes&tab=men&previousPage=GNAV">Casual Comfort Shoes</a></li><li><a href="/browse/water-shoes/shoes/men/_/N-27b5?cm_sp=sub-_-Men-_-ShoesWaterShoes&currentNode=water-shoes&tab=men&previousPage=GNAV">Water Shoes</a></li></ul>
</div>
<div class="f-col left">
<ul class="nav-list">
<li role="listitem"><p role="heading"><a role="link" href="/browse/shoes/women/_/N-279f?cm_sp=sub-_-women-_-shoes&currentNode=shoes&tab=women&previousPage=GNAV">Women</a></p></li><li><a href="/browse/boots/shoes/women/_/N-279i?cm_sp=sub-_-Women-_-ShoesBoots&currentNode=boots&tab=women&previousPage=GNAV">Boots</a></li><li><a href="/browse/hiking---trekking-shoes/shoes/women/_/N-279g?cm_sp=sub-_-Women-_-ShoesHikingTrekkingShoes&currentNode=hiking---trekking-shoes&tab=women&previousPage=GNAV">Hiking & Trekking Shoes</a></li><li><a href="/browse/lace-up-shoes/shoes/women/_/N-279l?cm_sp=sub-_-Women-_-ShoesLaceUpShoes&currentNode=lace-up-shoes&tab=women&previousPage=GNAV">Lace-Up Shoes</a></li><li><a href="/browse/flats/shoes/women/_/N-27b9?cm_sp=sub-_-Women-_-ShoesFlats&currentNode=flats&tab=women&previousPage=GNAV">Flats</a></li><li><a href="/browse/sandals/shoes/women/_/N-279m?cm_sp=sub-_-Women-_-ShoesSandals&currentNode=sandals&tab=women&previousPage=GNAV">Sandals</a></li><li><a href="/browse/slippers/shoes/women/_/N-279n?cm_sp=sub-_-Women-_-ShoesSlippers&currentNode=slippers&tab=women&previousPage=GNAV">Slippers</a></li><li><a href="/browse/water-shoes/shoes/women/_/N-27ba?cm_sp=sub-_-Women-_-ShoesWaterShoes&currentNode=water-shoes&tab=women&previousPage=GNAV">Water Shoes</a></li></ul>
</div>
</div>
<div id="flyout-home" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-home">
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p class="category-focus" aria-level="2">Shop by Category</p></li><li><a href="/browse/sheets---pillowcases/home/_/N-275l?cm_sp=sub-_-Home-_-SheetsPillowcases&currentNode=sheets---pillowcases&tab=home&previousPage=GNAV">Sheets & Pillowcases</a></li><li><a href="/browse/duvet-covers---shams/home/_/N-275h?cm_sp=sub-_-Home-_-DuvetCoversShams&currentNode=duvet-covers---shams&tab=home&previousPage=GNAV">Duvet Covers & Shams</a></li><li><a href="/browse/blankets---throws/home/_/N-275f?cm_sp=sub-_-Home-_-BlanketsThrows&currentNode=blankets---throws&tab=home&previousPage=GNAV">Blankets & Throws</a></li><li><a href="/browse/comforters/home/_/N-275g?cm_sp=sub-_-Home-_-Comforters&currentNode=comforters&tab=home&previousPage=GNAV">Comforters</a></li><li><a href="/browse/pillows/home/_/N-275j?cm_sp=sub-_-Home-_-Pillows&currentNode=pillows&tab=home&previousPage=GNAV">Pillows</a></li><li><a href="/browse/mattress-pads/home/_/N-275i?cm_sp=sub-_-Home-_-MattressPads&currentNode=mattress-pads&tab=home&previousPage=GNAV">Mattress Pads</a></li></ul>
</div>
<div class="f-col left">
<ul class="nav-list">
<div class="f-col left" role="columnheader">
	<ul class="nav-list" role="list">
		<li role="listitem">
			<p role="heading">
				FEATURED COLLECTIONS</p>
		</li>
		<li role="listitem">
			<a href="/lookbooks/us-landing-page#catalog/home-bedding/page/1">Home &amp; Bedding</a></li>
		<li role="listitem">
			<a href="//eddiebaueroutdoorfurniture.com/" target="_blank">Outdoor Furniture</a></li>
		<li role="listitem">
			<a href="//www.eddiebauerfloors.com" target="_blank">Hardwood Flooring</a></li>
	</ul>
</div></ul>
</div>
</div>
<div id="flyout-bagsgear" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-bagsgear">
<div class="f-col left">
<ul class="nav-list">
<li role="listitem"><p class="category-focus" aria-level="2">Shop By Category</p></li><li><a href="/browse/backpacks/gear/_/N-2754?cm_sp=sub-_-Gear-_-Backpacks&currentNode=backpacks&tab=gear&previousPage=GNAV">Backpacks</a></li><li><a href="/browse/duffels---luggage/gear/_/N-2755?cm_sp=sub-_-Gear-_-DuffelsLuggage&currentNode=duffels---luggage&tab=gear&previousPage=GNAV">Duffels & Luggage</a></li><li><a href="/browse/messenger---laptop-bags/gear/_/N-2759?cm_sp=sub-_-Gear-_-MessengerLaptopBags&currentNode=messenger---laptop-bags&tab=gear&previousPage=GNAV">Messenger & Laptop Bags</a></li><li><a href="/browse/travel-accessories/gear/_/N-275d?cm_sp=sub-_-Gear-_-TravelAccessories&currentNode=travel-accessories&tab=gear&previousPage=GNAV">Travel Accessories</a></li><li><a href="/browse/tents/gear/_/N-275c?cm_sp=sub-_-Gear-_-Tents&currentNode=tents&tab=gear&previousPage=GNAV">Tents</a></li><li><a href="/browse/sleeping-bags/gear/_/N-275a?cm_sp=sub-_-Gear-_-SleepingBags&currentNode=sleeping-bags&tab=gear&previousPage=GNAV">Sleeping Bags</a></li><li><a href="/browse/stoves---cookware/gear/_/N-275b?cm_sp=sub-_-Gear-_-StovesCookware&currentNode=stoves---cookware&tab=gear&previousPage=GNAV">Stoves & Cookware</a></li><li><a href="/browse/hydration/gear/_/N-2756?cm_sp=sub-_-Gear-_-Hydration&currentNode=hydration&tab=gear&previousPage=GNAV">Hydration</a></li><li><a href="/browse/lighting/gear/_/N-2758?cm_sp=sub-_-Gear-_-Lighting&currentNode=lighting&tab=gear&previousPage=GNAV">Lighting</a></li><li><a href="/browse/knives---tools/gear/_/N-2757?cm_sp=sub-_-Gear-_-KnivesTools&currentNode=knives---tools&tab=gear&previousPage=GNAV">Knives & Tools</a></li><li><a href="/browse/tote-bags/gear/_/N-27bs?cm_sp=sub-_-Gear-_-ToteBags&currentNode=tote-bags&tab=gear&previousPage=GNAV">Tote Bags</a></li><li><a href="/browse/sunglasses/gear/_/N-27bu?cm_sp=sub-_-Gear-_-Sunglasses&currentNode=sunglasses&tab=gear&previousPage=GNAV">Sunglasses</a></li></ul>
</div>
</div>
<div id="flyout-sale" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-sale">
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/men/_/N-1z13xm6Z26y3?Endeca_user_segments=sale&currentNode=men&tab=sale&previousPage=GNAV">Men</a></p></li><li><a href="/browse/outerwear/men/_/N-276bZ1z13xm6?cm_sp=sub-_-Sale-_-Outerwear&currentNode=outerwear&tab=sale&previousPage=GNAV">Outerwear</a></li><li><a href="/browse/fleece/men/_/N-27apZ1z13xm6?cm_sp=sub-_-Sale-_-Fleece&currentNode=fleece&tab=sale&previousPage=GNAV">Fleece</a></li><li><a href="/browse/pants/men/_/N-276hZ1z13xm6?cm_sp=sub-_-Sale-_-Pants&currentNode=pants&tab=sale&previousPage=GNAV">Pants</a></li><li><a href="/browse/flannel/men/_/N-27chZ1z13xm6?cm_sp=sub-_-Sale-_-Flannel&currentNode=flannel&tab=sale&previousPage=GNAV">Flannel</a></li><li><a href="/browse/t-shirts---polos/men/_/N-27caZ1z13xm6?cm_sp=sub-_-Sale-_-TShirtsPolos&currentNode=t-shirts---polos&tab=sale&previousPage=GNAV">T-Shirts & Polos</a></li><li><a href="/browse/shirts/men/_/N-277cZ1z13xm6?cm_sp=sub-_-Sale-_-Shirts&currentNode=shirts&tab=sale&previousPage=GNAV">Shirts</a></li><li><a href="/browse/jeans/men/_/N-276aZ1z13xm6?cm_sp=sub-_-Sale-_-Jeans&currentNode=jeans&tab=sale&previousPage=GNAV">Jeans</a></li><li><a href="/browse/shorts/men/_/N-276zZ1z13xm6?cm_sp=sub-_-Sale-_-Shorts&currentNode=shorts&tab=sale&previousPage=GNAV">Shorts</a></li><li><a href="/browse/blazers---jackets/men/_/N-27cbZ1z13xm6?cm_sp=sub-_-Sale-_-BlazersJackets&currentNode=blazers---jackets&tab=sale&previousPage=GNAV">Blazers & Jackets</a></li><li><a href="/browse/sweatshirts---hoodies/men/_/N-2778Z1z13xm6?cm_sp=sub-_-Sale-_-SweatshirtsHoodies&currentNode=sweatshirts---hoodies&tab=sale&previousPage=GNAV">Sweatshirts & Hoodies</a></li><li><a href="/browse/sleepwear/men/_/N-2773Z1z13xm6?cm_sp=sub-_-Sale-_-Sleepwear&currentNode=sleepwear&tab=sale&previousPage=GNAV">Sleepwear</a></li><li><a href="/browse/baselayers/men/_/N-2767Z1z13xm6?cm_sp=sub-_-Sale-_-Baselayers&currentNode=baselayers&tab=sale&previousPage=GNAV">Baselayers</a></li><li><a href="/browse/shoes/men/_/N-276qZ1z13xm6?cm_sp=sub-_-Sale-_-Shoes&currentNode=shoes&tab=sale&previousPage=GNAV">Shoes</a></li><li><a href="/browse/accessories/men/_/N-26y4Z1z13xm6?cm_sp=sub-_-Sale-_-Accessories&currentNode=accessories&tab=sale&previousPage=GNAV">Accessories</a></li></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/women/_/N-1z13xm6Z278f?Endeca_user_segments=sale&currentNode=women&tab=sale&previousPage=GNAV">Women</a></p></li><li><a href="/browse/outerwear/women/_/N-2792Z1z13xm6?cm_sp=sub-_-Sale-_-Outerwear&currentNode=outerwear&tab=sale&previousPage=GNAV">Outerwear</a></li><li><a href="/browse/fleece/women/_/N-27asZ1z13xm6?cm_sp=sub-_-Sale-_-Fleece&currentNode=fleece&tab=sale&previousPage=GNAV">Fleece</a></li><li><a href="/browse/pants---capris/women/_/N-2798Z1z13xm6?cm_sp=sub-_-Sale-_-PantsCapris&currentNode=pants---capris&tab=sale&previousPage=GNAV">Pants & Capris</a></li><li><a href="/browse/flannel/women/_/N-27ciZ1z13xm6?cm_sp=sub-_-Sale-_-Flannel&currentNode=flannel&tab=sale&previousPage=GNAV">Flannel</a></li><li><a href="/browse/tops/women/_/N-279zZ1z13xm6?cm_sp=sub-_-Sale-_-Tops&currentNode=tops&tab=sale&previousPage=GNAV">Tops</a></li><li><a href="/browse/t-shirts---tanks/women/_/N-27ccZ1z13xm6?cm_sp=sub-_-Sale-_-TShirtsTanks&currentNode=t-shirts---tanks&tab=sale&previousPage=GNAV">T-Shirts & Tanks</a></li><li><a href="/browse/jeans/women/_/N-2791Z1z13xm6?cm_sp=sub-_-Sale-_-Jeans&currentNode=jeans&tab=sale&previousPage=GNAV">Jeans</a></li><li><a href="/browse/shorts/women/_/N-278uZ1z13xm6?cm_sp=sub-_-Sale-_-Shorts&currentNode=shorts&tab=sale&previousPage=GNAV">Shorts</a></li><li><a href="/browse/dresses---skirts/women/_/N-278yZ1z13xm6?cm_sp=sub-_-Sale-_-DressesSkirts&currentNode=dresses---skirts&tab=sale&previousPage=GNAV">Dresses & Skirts</a></li><li><a href="/browse/blazers---jackets/women/_/N-278tZ1z13xm6?cm_sp=sub-_-Sale-_-BlazersJackets&currentNode=blazers---jackets&tab=sale&previousPage=GNAV">Blazers & Jackets</a></li><li><a href="/browse/sweatshirts---hoodies/women/_/N-27bgZ1z13xm6?cm_sp=sub-_-Sale-_-SweatshirtsHoodies&currentNode=sweatshirts---hoodies&tab=sale&previousPage=GNAV">Sweatshirts & Hoodies</a></li><li><a href="/browse/sweaters/women/_/N-27bhZ1z13xm6?cm_sp=sub-_-Sale-_-Sweaters&currentNode=sweaters&tab=sale&previousPage=GNAV">Sweaters</a></li><li><a href="/browse/sleepwear/women/_/N-279oZ1z13xm6?cm_sp=sub-_-Sale-_-Sleepwear&currentNode=sleepwear&tab=sale&previousPage=GNAV">Sleepwear</a></li><li><a href="/browse/baselayers/women/_/N-278qZ1z13xm6?cm_sp=sub-_-Sale-_-Baselayers&currentNode=baselayers&tab=sale&previousPage=GNAV">Baselayers</a></li><li><a href="/browse/shoes/women/_/N-279fZ1z13xm6?cm_sp=sub-_-Sale-_-Shoes&currentNode=shoes&tab=sale&previousPage=GNAV">Shoes</a></li><li><a href="/browse/accessories/women/_/N-278gZ1z13xm6?cm_sp=sub-_-Sale-_-Accessories&currentNode=accessories&tab=sale&previousPage=GNAV">Accessories</a></li><style type="text/css">
hr {
    border-bottom: 1px solid #636363;
    margin-top: 5px;
    width: auto;
}
.f-cat-break {margin-top:6px !important;}</style></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/kids/_/N-1z13xm6Z275n?Endeca_user_segments=sale&currentNode=kids&tab=sale&previousPage=GNAV">Kids</a></p></li><li><a href="/browse/boys/kids/_/N-275oZ1z13xm6?cm_sp=sub-_-Sale-_-Boys&currentNode=boys&tab=sale&previousPage=GNAV">Boys</a></li><li><a href="/browse/girls/kids/_/N-275tZ1z13xm6?cm_sp=sub-_-Sale-_-Girls&currentNode=girls&tab=sale&previousPage=GNAV">Girls</a></li><li role="listitem"><p role="heading" class="f-cat-break"><a role="link" href="/browse/gear/_/N-1z13xm6Z2752?Endeca_user_segments=sale&currentNode=gear&tab=sale&previousPage=GNAV">Gear</a></p></li><li><a href="/browse/backpacks/gear/_/N-2754Z1z13xm6?cm_sp=sub-_-Sale-_-Backpacks&currentNode=backpacks&tab=sale&previousPage=GNAV">Backpacks</a></li><li><a href="/browse/duffels---luggage/gear/_/N-2755Z1z13xm6?cm_sp=sub-_-Sale-_-DuffelsLuggage&currentNode=duffels---luggage&tab=sale&previousPage=GNAV">Duffels & Luggage</a></li><li><a href="/browse/messenger---laptop-bags/gear/_/N-2759Z1z13xm6?cm_sp=sub-_-Sale-_-MessengerLaptopBags&currentNode=messenger---laptop-bags&tab=sale&previousPage=GNAV">Messenger & Laptop Bags</a></li><li><a href="/browse/travel-accessories/gear/_/N-275dZ1z13xm6?cm_sp=sub-_-Sale-_-TravelAccessories&currentNode=travel-accessories&tab=sale&previousPage=GNAV">Travel Accessories</a></li><li><a href="/browse/hydration/gear/_/N-2756Z1z13xm6?cm_sp=sub-_-Sale-_-Hydration&currentNode=hydration&tab=sale&previousPage=GNAV">Hydration</a></li><li><a href="/browse/lighting/gear/_/N-2758Z1z13xm6?cm_sp=sub-_-Sale-_-Lighting&currentNode=lighting&tab=sale&previousPage=GNAV">Lighting</a></li><li><a href="/browse/tote-bags/gear/_/N-27bsZ1z13xm6?cm_sp=sub-_-Sale-_-ToteBags&currentNode=tote-bags&tab=sale&previousPage=GNAV">Tote Bags</a></li></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/first-ascent/_/N-1z13xm6Z274k?Endeca_user_segments=sale&currentNode=first-ascent&tab=sale&previousPage=GNAV">First Ascent</a></p></li><li><a href="/browse/men/first-ascent/_/N-274lZ1z13xm6?cm_sp=sub-_-Sale-_-Men&currentNode=men&tab=sale&previousPage=GNAV">Men</a></li><li><a href="/browse/women/first-ascent/_/N-274qZ1z13xm6?cm_sp=sub-_-Sale-_-Women&currentNode=women&tab=sale&previousPage=GNAV">Women</a></li><li><a href="/browse/gear/first-ascent/_/N-274yZ1z13xm6?cm_sp=sub-_-Sale-_-Gear&currentNode=gear&tab=sale&previousPage=GNAV">Gear</a></li><li role="listitem"><p role="heading" class="f-cat-break"><a role="link" href="/browse/motion/_/N-1z13xm6Z27bk?Endeca_user_segments=sale&currentNode=motion&tab=sale&previousPage=GNAV">Motion</a></p></li><li><a href="/browse/men/motion/_/N-27blZ1z13xm6?cm_sp=sub-_-Sale-_-Men&currentNode=men&tab=sale&previousPage=GNAV">Men</a></li><li><a href="/browse/women/motion/_/N-27bmZ1z13xm6?cm_sp=sub-_-Sale-_-Women&currentNode=women&tab=sale&previousPage=GNAV">Women</a></li><li><a href="/browse/gear/motion/_/N-27bnZ1z13xm6?cm_sp=sub-_-Sale-_-Gear&currentNode=gear&tab=sale&previousPage=GNAV">Gear</a></li></ul>
</div>
<div class="f-col left">
<ul class="nav-list">
<li role="listitem"><p role="heading"><a role="link" href="/browse/sportshop/_/N-1z13xm6Z277n?Endeca_user_segments=sale&currentNode=sportshop&tab=sale&previousPage=GNAV">Sport Shop</a></p></li><li><a href="/browse/men/sport-shop/_/N-277oZ1z13xm6?cm_sp=sub-_-Sale-_-Men&currentNode=men&tab=sale&previousPage=GNAV">Men</a></li><li><a href="/browse/women/sport-shop/_/N-277rZ1z13xm6?cm_sp=sub-_-Sale-_-Women&currentNode=women&tab=sale&previousPage=GNAV">Women</a></li><li><a href="/browse/gear/sport-shop/_/N-277uZ1z13xm6?cm_sp=sub-_-Sale-_-Gear&currentNode=gear&tab=sale&previousPage=GNAV">Gear</a></li><li role="listitem"><p role="heading" class="f-cat-break"><a role="link" href="/browse/travex/_/N-1z13xm6Z277y?Endeca_user_segments=sale&currentNode=travex&tab=sale&previousPage=GNAV">Travex</a></p></li><li><a href="/browse/men/travex/_/N-277zZ1z13xm6?cm_sp=sub-_-Sale-_-Men&currentNode=men&tab=sale&previousPage=GNAV">Men</a></li><li><a href="/browse/women/travex/_/N-2785Z1z13xm6?cm_sp=sub-_-Sale-_-Women&currentNode=women&tab=sale&previousPage=GNAV">Women</a></li></ul>
</div>
</div>
<div id="flyout-clearance" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-clearance">
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/men/_/N-yZ26y3?currentNode=men&tab=clearance&isClearCat=true&previousPage=GNAV">Men</a></p></li><li><a href="/browse/outerwear/men/_/N-276bZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Outerwear&currentNode=outerwear&tab=clearance&previousPage=GNAV">Outerwear</a></li><li><a href="/browse/fleece/men/_/N-27apZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Fleece&currentNode=fleece&tab=clearance&previousPage=GNAV">Fleece</a></li><li><a href="/browse/pants/men/_/N-276hZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Pants&currentNode=pants&tab=clearance&previousPage=GNAV">Pants</a></li><li><a href="/browse/flannel/men/_/N-27chZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Flannel&currentNode=flannel&tab=clearance&previousPage=GNAV">Flannel</a></li><li><a href="/browse/t-shirts---polos/men/_/N-27caZy?isClearCat=true&cm_sp=sub-_-Clearance-_-TShirtsPolos&currentNode=t-shirts---polos&tab=clearance&previousPage=GNAV">T-Shirts & Polos</a></li><li><a href="/browse/shirts/men/_/N-277cZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Shirts&currentNode=shirts&tab=clearance&previousPage=GNAV">Shirts</a></li><li><a href="/browse/jeans/men/_/N-276aZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Jeans&currentNode=jeans&tab=clearance&previousPage=GNAV">Jeans</a></li><li><a href="/browse/shorts/men/_/N-276zZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Shorts&currentNode=shorts&tab=clearance&previousPage=GNAV">Shorts</a></li><li><a href="/browse/blazers---jackets/men/_/N-27cbZy?isClearCat=true&cm_sp=sub-_-Clearance-_-BlazersJackets&currentNode=blazers---jackets&tab=clearance&previousPage=GNAV">Blazers & Jackets</a></li><li><a href="/browse/sweatshirts---hoodies/men/_/N-2778Zy?isClearCat=true&cm_sp=sub-_-Clearance-_-SweatshirtsHoodies&currentNode=sweatshirts---hoodies&tab=clearance&previousPage=GNAV">Sweatshirts & Hoodies</a></li><li><a href="/browse/sweaters/men/_/N-2774Zy?isClearCat=true&cm_sp=sub-_-Clearance-_-Sweaters&currentNode=sweaters&tab=clearance&previousPage=GNAV">Sweaters</a></li><li><a href="/browse/sleepwear/men/_/N-2773Zy?isClearCat=true&cm_sp=sub-_-Clearance-_-Sleepwear&currentNode=sleepwear&tab=clearance&previousPage=GNAV">Sleepwear</a></li><li><a href="/browse/shoes/men/_/N-276qZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Shoes&currentNode=shoes&tab=clearance&previousPage=GNAV">Shoes</a></li><li><a href="/browse/accessories/men/_/N-26y4Zy?isClearCat=true&cm_sp=sub-_-Clearance-_-Accessories&currentNode=accessories&tab=clearance&previousPage=GNAV">Accessories</a></li></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/women/_/N-yZ278f?currentNode=women&tab=clearance&isClearCat=true&previousPage=GNAV">Women</a></p></li><li><a href="/browse/outerwear/women/_/N-2792Zy?isClearCat=true&cm_sp=sub-_-Clearance-_-Outerwear&currentNode=outerwear&tab=clearance&previousPage=GNAV">Outerwear</a></li><li><a href="/browse/fleece/women/_/N-27asZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Fleece&currentNode=fleece&tab=clearance&previousPage=GNAV">Fleece</a></li><li><a href="/browse/pants---capris/women/_/N-2798Zy?isClearCat=true&cm_sp=sub-_-Clearance-_-PantsCapris&currentNode=pants---capris&tab=clearance&previousPage=GNAV">Pants & Capris</a></li><li><a href="/browse/flannel/women/_/N-27ciZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Flannel&currentNode=flannel&tab=clearance&previousPage=GNAV">Flannel</a></li><li><a href="/browse/tops/women/_/N-279zZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Tops&currentNode=tops&tab=clearance&previousPage=GNAV">Tops</a></li><li><a href="/browse/t-shirts---tanks/women/_/N-27ccZy?isClearCat=true&cm_sp=sub-_-Clearance-_-TShirtsTanks&currentNode=t-shirts---tanks&tab=clearance&previousPage=GNAV">T-Shirts & Tanks</a></li><li><a href="/browse/jeans/women/_/N-2791Zy?isClearCat=true&cm_sp=sub-_-Clearance-_-Jeans&currentNode=jeans&tab=clearance&previousPage=GNAV">Jeans</a></li><li><a href="/browse/shorts/women/_/N-278uZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Shorts&currentNode=shorts&tab=clearance&previousPage=GNAV">Shorts</a></li><li><a href="/browse/dresses---skirts/women/_/N-278yZy?isClearCat=true&cm_sp=sub-_-Clearance-_-DressesSkirts&currentNode=dresses---skirts&tab=clearance&previousPage=GNAV">Dresses & Skirts</a></li><li><a href="/browse/blazers---jackets/women/_/N-278tZy?isClearCat=true&cm_sp=sub-_-Clearance-_-BlazersJackets&currentNode=blazers---jackets&tab=clearance&previousPage=GNAV">Blazers & Jackets</a></li><li><a href="/browse/sweatshirts---hoodies/women/_/N-27bgZy?isClearCat=true&cm_sp=sub-_-Clearance-_-SweatshirtsHoodies&currentNode=sweatshirts---hoodies&tab=clearance&previousPage=GNAV">Sweatshirts & Hoodies</a></li><li><a href="/browse/sweaters/women/_/N-27bhZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Sweaters&currentNode=sweaters&tab=clearance&previousPage=GNAV">Sweaters</a></li><li><a href="/browse/sleepwear/women/_/N-279oZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Sleepwear&currentNode=sleepwear&tab=clearance&previousPage=GNAV">Sleepwear</a></li><li><a href="/browse/baselayers/women/_/N-278qZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Baselayers&currentNode=baselayers&tab=clearance&previousPage=GNAV">Baselayers</a></li><li><a href="/browse/shoes/women/_/N-279fZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Shoes&currentNode=shoes&tab=clearance&previousPage=GNAV">Shoes</a></li><li><a href="/browse/accessories/women/_/N-278gZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Accessories&currentNode=accessories&tab=clearance&previousPage=GNAV">Accessories</a></li><li><a href="/browse/swimwear/women/_/N-279yZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Swimwear&currentNode=swimwear&tab=clearance&previousPage=GNAV">Swimwear</a></li></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/kids/_/N-yZ275n?currentNode=kids&tab=clearance&isClearCat=true&previousPage=GNAV">Kids</a></p></li><li><a href="/browse/boys/kids/_/N-275oZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Boys&currentNode=boys&tab=clearance&previousPage=GNAV">Boys</a></li><li><a href="/browse/girls/kids/_/N-275tZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Girls&currentNode=girls&tab=clearance&previousPage=GNAV">Girls</a></li></ul>
</div>
<div class="f-col left f-sep-dark">
<ul class="nav-list f-sep-light">
<li role="listitem"><p role="heading"><a role="link" href="/browse/first-ascent/_/N-yZ274K?currentNode=first-ascent&tab=clearance&isClearCat=true&previousPage=GNAV">First Ascent</a></p></li><li><a href="/browse/men/first-ascent/_/N-274lZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Men&currentNode=men&tab=clearance&previousPage=GNAV">Men</a></li><li><a href="/browse/women/first-ascent/_/N-274qZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Women&currentNode=women&tab=clearance&previousPage=GNAV">Women</a></li><li role="listitem"><p role="heading" class="f-cat-break"><a role="link" href="/browse/motion/_/N-yZ27bk?currentNode=motion&tab=clearance&isClearCat=true&previousPage=GNAV">Motion</a></p></li><li><a href="/browse/men/motion/_/N-27blZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Men&currentNode=men&tab=clearance&previousPage=GNAV">Men</a></li><li><a href="/browse/women/motion/_/N-27bmZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Women&currentNode=women&tab=clearance&previousPage=GNAV">Women</a></li></ul>
</div>
<div class="f-col left">
<ul class="nav-list">
<li role="listitem"><p role="heading"><a role="link" href="/browse/sportshop/_/N-yZ277N?currentNode=sportshop&tab=clearance&isClearCat=true&previousPage=GNAV">Sport Shop</a></p></li><li><a href="/browse/men/sport-shop/_/N-277oZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Men&currentNode=men&tab=clearance&previousPage=GNAV">Men</a></li><li><a href="/browse/women/sport-shop/_/N-277rZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Women&currentNode=women&tab=clearance&previousPage=GNAV">Women</a></li><li role="listitem"><p role="heading" class="f-cat-break"><a role="link" href="/browse/first-ascent/_/N-yZ277Y?currentNode=first-ascent&tab=clearance&isClearCat=true&previousPage=GNAV">Travex</a></p></li><li><a href="/browse/men/travex/_/N-277zZy?isClearCat=true&cm_sp=sub-_-Clearance-_-Men&currentNode=men&tab=clearance&previousPage=GNAV">Men</a></li><li><a href="/browse/women/travex/_/N-2785Zy?isClearCat=true&cm_sp=sub-_-Clearance-_-Women&currentNode=women&tab=clearance&previousPage=GNAV">Women</a></li></ul>
</div>
</div>
<div id="flyout-guidetoeb" class="nav-flyout clearfix" aria-expanded="false" aria-hidden="true" aria-labelledby="nav-guidetoeb">
<div class="f-col left">
<ul class="nav-list">
<div role="columnheader">
	<ul class="nav-list" role="list">
		<li role="listitem">
			<a href="/campaign/theguide">Our Founder</a></li>
		<li role="listitem">
			<a href="/campaign/guides">Guides &amp; Athletes</a></li>
		<li role="listitem">
			<a href="/campaign/expeditions" role="link">Expeditions</a></li>
		<li role="listitem">
			<a href="/campaign/Big-City-Mountaineers" role="link">Big City Mountaineers</a></li>
		<li role="listitem">
			<a href="/campaign/American-Hiking-Society" role="link">American Hiking Society</a></li>
		<li role="listitem">
			<a href="/campaign/theheroesproject">The Heroes Project</a></li>
		<li role="listitem">
			<a href="/campaign/americanforests">American Forests</a></li>
		<li role="listitem">
			<a href="/campaign/responsible-sourcing">Responsible Sourcing</a></li>
		<li role="listitem">
			<a href="/campaign/awards">Award Winners</a></li>
		<li role="listitem">
			<a href="//careers.eddiebauer.com/" target="_blank">Careers</a></li>
		<!--<li role="listitem">
			<a href="//blog.eddiebauer.com" target="_blank">Blog</a></li>-->
	</ul>
</div></ul>
</div>
</div>
</div>
</div>
<!-- </div> -->
</header>
<div id="headContentHolder"></div>
<div id="header-rail"></div>
<div id="end-kiosk-session" style="display: none;">
<div id="kiosk">
<p>IMPORTANT:  Please click &quot;End Session&quot; to clear your personal information. (Credit Card info is always hidden from view).</p><a id="endKioskSession" tabindex="0" class="button-link end-kiosk-session-button" href="/?_DARGS=/cartridges/HeaderMain3Slots/HeaderMain3Slots.jsp_A&_DAV=logout">End Session</a></div>
</div>
<script>
$(document).ready(function () {
try {
var hostname = window.location.hostname;
if (hostname.indexOf("kiosk") > -1) {
$( "#end-kiosk-session" ).show();
if(window.BOOMR && window.BOOMR.version){return;}
var dom,doc,where,iframe = document.createElement('iframe'),win = window;
function boomerangSaveLoadTime(e) {
win.BOOMR_onload=(e && e.timeStamp) || new Date().getTime();
}
if (win.addEventListener) {
win.addEventListener("load", boomerangSaveLoadTime, false);
} else if (win.attachEvent) {
win.attachEvent("onload", boomerangSaveLoadTime);
}
iframe.src = "javascript:false";
iframe.title = "NO CONTENT"; iframe.role="presentation";
(iframe.frameElement || iframe).style.cssText = "width:0;height:0;border:0;display:none;";
where = document.getElementsByTagName('script')[0];
where.parentNode.insertBefore(iframe, where);
try {
doc = iframe.contentWindow.document;
} catch(e) {
dom = document.domain;
iframe.src="javascript:var d=document.open();d.domain='"+dom+"';void(0);";
doc = iframe.contentWindow.document;
}
doc.open()._l = function() {
var js = this.createElement("script");
if(dom) this.domain = dom;
js.id = "boomr-if-as";
js.src = '//c.go-mpulse.net/boomerang/' +
'94AZ9-TANCQ-NZ8GC-RTNCQ-CMBN8';
BOOMR_lstart=new Date().getTime();
this.body.appendChild(js);
};
doc.write('<body onload="document._l();">');
doc.close();
} else {
if(hostname.indexOf("ebi") < 0){
$( "#end-kiosk-session" ).hide();
}
var domainName = document.domain;
var mpAppId = '';
switch(domainName){
case 'www.eddiebauer.com':
var mpAppId = 'NQ5VF-BTBHE-8LCV4-4SXP7-C4XED';
break;
case 'stg3.eddiebauer.com':
var mpAppId = 'H8N8K-49QNR-3Y7XM-H393M-F8WN3';
break;
case 'stg4.eddiebauer.com':
var mpAppId = 'H8N8K-49QNR-3Y7XM-H393M-F8WN3';
break;
case 'stg5.eddiebauer.com':
var mpAppId = 'H8N8K-49QNR-3Y7XM-H393M-F8WN3';
break;
case 'ebi.eddiebauer.com':
var mpAppId = '94AZ9-TANCQ-NZ8GC-RTNCQ-CMBN8';
break;
}
if(window.BOOMR && window.BOOMR.version){return;}
var dom,doc,where,iframe = document.createElement('iframe');
iframe.src = "javascript:false";
iframe.title = "NO CONTENT"; iframe.role="presentation";
(iframe.frameElement || iframe).style.cssText = "width:0;height:0;border:0;display:none;";
where = document.getElementsByTagName('script')[0];
where.parentNode.insertBefore(iframe, where);
try {
doc = iframe.contentWindow.document;
} catch(e) {
dom = document.domain;
iframe.src="javascript:var d=document.open();d.domain='"+dom+"';void(0);";
doc = iframe.contentWindow.document;
}
doc.open()._l = function() {
var js = this.createElement("script");
if(dom) this.domain = dom;
js.id = "boomr-if-as";
js.src = '//c.go-mpulse.net/boomerang/' + mpAppId;
BOOMR_lstart=new Date().getTime();
this.body.appendChild(js);
};
doc.write('<body onload="document._l();">');
doc.close();
}
} catch (err) {
console.log('error');
}
});
</script>
<!--
DO NOT REMOVE THIS []
Server Name: 772623-rsatgbot1.eddiebauer.com:8850
Profile: u2461679575
Order: A2474909576
mode: 
Server is alive... />
-->
<div id="siteContainer"><noscript>
<div class="response-error">
<p><strong>JavaScript is not enabled in your browser.</strong></p>
<p>You must have JavaScript enabled for the best experience on our site and to make a purchase.</p>
<p><a href="/custserv/customer-service-technical-information.jsp#javascript">Learn how to enable JavaScript in your browser</a> or call 1-800-426-8020 for assistance.</p>
</div>
</noscript>
<div id="noCookie" class="hide">
<div class="response-error">
<p><strong>Cookies are not enabled in your browser.</strong></p>
<p>You must have Cookies enabled for the best experience on our site and to make a purchase.</p>
<p><a href="/custserv/customer-service-technical-information.jsp#cookies">Learn how to enable Cookies in your browser</a> or call 1-800-426-8020 for assistance.</p>
</div>
</div>
<script>
var cookieEnabled = (navigator.cookieEnabled) ? true : false;
if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
document.cookie = "isCookie";
cookieEnabled = (document.cookie.indexOf("isCookie") > -1) ? true : false;
}
if (!cookieEnabled) {
document.getElementById("noCookie").style.display = "block";
}
</script>
<!-- Do not remove the Slick code below. This is for the Freeship Rail on each page. -->
<link rel="stylesheet" type="text/css" href="/static/css/slick-1.6.0.css"/>
<link rel="stylesheet" type="text/css" href="/static/css/slick-theme-1.6.0.css"/>
<script type="text/javascript" src="/static/js/slick-1.6.0.min.js"></script>
<!-- End Slick includes -->
<input type="hidden" id="currentSiteDefaultCountry" name="currentSiteDefaultCountry" value="US"/>
<input type="hidden" id="currentSiteDefaultLanguage" name="currentSiteDefaultLanguage" value="en"/>
<input type="hidden" id="currentSiteDefaultCurrency" name="currentSiteDefaultCurrency" value="$"/>
<input type="hidden" id="currentSiteId" name="currentSiteId" value="EBSite"/>
<div id="mainContentHolder" role="main">
<ul class="breadcrumbs">
<li><a href="/">Eddie Bauer</a> |</li>
<li><a href="/company-info/company-info-about-us.jsp">Company Info</a> |</li>
<li>Privacy & Security</li>
</ul>
<div id="baseTitle">
<h1>Privacy & Security</h1>
<p class="privacy_align">Last updated: February, 21, 2019</p>
</div>
<div class="customerService">
<div class="wrapper">
<div class="leftColumn" role="navigation" aria-labelledby="co-info-hdr">
<script>
jQuery(document).ready(function () {
var pageClasses = jQuery('body').attr('class');
var pageClassArray = pageClasses.split(" ");
var pageSelector = "company-info-";
var pageName = "";
var leftNavLi = jQuery('#leftNav li');
for (var i=0;i<pageClassArray.length;i++){
if(pageClassArray[i].indexOf(pageSelector) != -1){
pageName = "/company-info/" + pageClassArray[i] + ".jsp";
}
}
if (leftNavLi.length >0){
leftNavLi.each(function(index){
var ahref = jQuery(this).find('a').attr('href');
if (ahref.toLowerCase() == pageName.toLowerCase()){
jQuery(this).removeClass('not').addClass('hot');
// check to see if second level if so open first level
jQuery(this).parent().parent().removeClass('not').addClass('open');
}
});
//show left nav after updates have been made
$('ul.first').css({'visibility':'visible'});
}
});
</script>
<h5 id="co-info-hdr" tabindex="-1" class="header">Company Info</h5>
<ul id="leftNav" class="first">
<li class="not"><a href="/company-info/company-info-about-us.jsp">About Us</a></li>
<li class="not"><a href="/company-info/company-info-our-founder.jsp">Our Founder</a></li>
<li class="not"><a href="/company-info/company-info-affiliate-program.jsp">Affiliate Program</a></li>
<li class="not"><a href="/company-info/company-info-partners.jsp">Partners</a></li>
<li class="not"><a href="/custserv/pro-deals-application.jsp">Pro Deals</a></li>
<li class="not"><a href="/company-info/company-info-global-releaf.jsp">Global ReLeaf</a></li>
<li class="not"><a href="/company-info/company-info-privacy-and-security.jsp">Privacy & Security</a>
<ul class="second">
<li class="not"><a href="/company-info/company-info-terms-of-use.jsp">Terms of Use</a></li>
</ul>
</li>
<li class="not"><a href="/company-info/company-info-global-labor-practice.jsp">Global Labor Practice</a></li>
</ul>
</div>
<div class="centerColumn">
<div class="container">
<h3 class="first">
Overview</h3>
<p class="first">
In 1920, Eddie Bauer wrote his business creed, a statement explaining the services he'd provide for his customers. We still follow this same creed today: "To give you such outstanding quality, value, service and guarantee that we may be worthy of your high esteem." Here at Eddie Bauer, we take this promise seriously and we firmly believe in your privacy and the security of your personal information. We are committed to using any information you give us in a responsible manner. You're very important to us and we want to provide you with a safe and secure online experience.</p>
<h3>
Privacy Policy</h3>
<p class="aboveSubheader">
This Privacy Statement describes how Eddie Bauer (“we,” “us,” or “our”) collects, uses, and discloses your personal information as a data controller. This Privacy Statement applies to our website (www.eddiebauer.com), our retail location, and our services and products. To the extent permitted by applicable law, you consent to the use of your personal information as described in this Privacy Statement each time you visit our website or retail locations, or use our products or services. This Policy is part of the&nbsp;<a class="bg-link" href="company-info-terms-of-use.jsp">Terms of Use</a>
published on our website.<p class="last">
When we use the term “personal information” in this Policy, we mean information relating to an identified or identifiable natural person, such as an individual’s full name, postal address, e-mail address, telephone number, payment information, and location information. When other data (such as consumer preferences) is directly associated with such information, we also treat this other data as personal information.</p>
</div>
<ul class="bulletList">
<li><a href="#first">What information does Eddie Bauer collect about me?</a></li>
<li><a href="#second">How do you use my information?</a></li>
<li><a href="#third">To whom do you disclose my information?</a></li>
<li><a href="#fourth">Companies other than Eddie Bauer</a></li>
<li><a href="#fifth">What are "cookies" and how does Eddie Bauer use them?</a></li>
<li><a href="#sixth">Your Rights and Choices</a></li>
<li><a href="#seventh">California Privacy Rights</a></li>
<li><a href="#eight">How is my information protected?</a></li>
<li><a href="#HowtoReportFraud"> How to Report Fraud</a></li>
<li><a href="#ninth">Children's Policy</a></li>
<li><a href="#tenth">Changes to this Privacy Statement</a></li>
<li><a href="#eleventh">Contact Information</a></li>
<li><a href="#twelth"> Unsubscribe From Eddie Bauer</a></li>
</ul>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="first">
What information does Eddie Bauer collect about me?</h3>
<p class="first">
We may collect different kinds of information from or about you:</p>
<ul class="bulletedList">
<li><span style="text-decoration: underline;">Information you provide: </span> When you purchase from Eddie Bauer or request a catalog, we collect information that you volunteer: your name, email address, postal address, phone number, payment information, and other information necessary to process your order or send you the catalog. We also collect the information you provide to us when you register for "My Account," participate in a contest or sweepstakes promotion, fill out a survey or questionnaire, or contact us (for example via email, "Ask Eddie" or "Live Chat"). The information you supply to us is added to our customer database.</li>
<li><span style="text-decoration: underline;">Information about use of our website:</span> Every time you visit our website, we automatically collect your Internet service provider's address, the Web page from which you came, and a record of your activity on our site. We may gather some of this information automatically.</li>
<li><span style="text-decoration: underline;">Information about visits to retail locations:</span> We use mobile location analytics technology at our U.S. retail spaces to gain aggregate insights about customer behavior. The data collected is anonymous and works by sensing the presence of Wi-Fi enabled devices. This technology collects and hashes Media Access Control (MAC) addresses from Wi-Fi enabled devices that come in contact with our network equipment. In addition, this technology collects signal strength, time and date, the manufacturer of the device, and the name of a Wi-Fi network currently connected to the device, if applicable. A MAC address is a unique identifier assigned to every mobile device and is not the device's phone number. Collection of mobile location information only occurs when a device's Wi-Fi setting is turned on.</li>
<li><span style="text-decoration: underline;">Information from third parties:</span> We may collect information from third parties, including business partners and marketing agencies. This may include your contact information from marketing partners when we engage in joint marketing or co-branding activities, your IP address or location information from service providers to offer certain products and services relevant to you or your location, and data from your social networks to authenticate your product use with us, or that you grant permission to our products or services to access.</li>
</ul>
<p class="first">
We may combine information we collect in different ways, and combine it in your customer profile. </p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="second">
How do you use my information?</h3>
<p class="first">
Generally speaking, we use personal information to provide, improve, and develop our products and services, to communicate with you, to offer you targeted advertisements, and to protect us and our users.</p>
<ul class="bulletedList">
<li>Eddie Bauer collects, processes, and determines how to process your personal information for the following purposes: <span
style="text-decoration: underline;">Communicating with you:</span> We may use your personal information to communicate with you and process your requests, accounts, or orders, and to inform you about our policies and terms. These communications allow us to provide you the products and/or services you have requested, and relate to the relationship between you and us. Please note: In order to process your purchases we may send you a confirmation email or we may contact you by mail or telephone if we have questions about your order. If you do not want to receive such mailings or telephone calls from us, please&nbsp;<a
class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">click here</a> for contact information or call 1-800-426-8020.</li>
<li>Informing you about products or services that may be of interest: With your prior consent, we may use personal information to send you marketing communications; for example, we may periodically send you emails about new products and services, discounts, special promotions or upcoming events. If you do not wish to receive such emails in the future, please click here for contact information, or click the "unsubscribe" link on any promotional email you receive from us. Also with your prior consent, we may share your personal information with third-party partners who may send you marketing communications in relation to their products and services. If you wish to have your name and address not shared with other organizations, please&nbsp;<a
class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">contact us</a> or call 1-800-426-8020.</li>
<li><span style="text-decoration: underline;">Providing, improving, and developing our products and services: </span> We use your information to help us provide, improve, and develop our services, which is necessary for us to pursue our legitimate interest and provide you with quality services and products. The information we receive allows us to tailor our product offerings and improve your shopping experience, making shopping easier and more enjoyable. It also enables us to customize the content and layout of our pages. We also use mobile location analytics technology to develop anonymous and aggregated reporting and gain a better understanding of our customers' preferences. This information might be used, for example, to improve our store layouts or the timing of promotions and sales. If you wish to opt out of participation and delete existing mobile location data, please&nbsp;<a
class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">click here</a> or call 1-800-426-8020.</li>
<li><span style="text-decoration: underline;">Promoting safety and security:</span> We use your information to pursue our legitimate interest in helping verify accounts and user activity, as well as in promoting safety and security, such as by monitoring fraud, investigating suspicious or potentially illegal activity or violations of our terms or policies, protecting our rights, or pursuing available remedies.</li>
</ul>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="third">
To whom do you disclose my information?</h3>
<p class="first">
We make certain information available to strategic partners that work with us to provide our products and services or help us market to customers.</p>
<ul class="bulletedList">
<li><span style="text-decoration: underline;">Vendors and Service Providers:</span> In some instances, we use third party companies to help us provide our products and services to you, to assist us in managing customer information, to fulfill promotions and to communicate with you. Some of these companies are given access to some or all of the information you provide to us, and some may use cookies on our behalf. These companies are contractually restricted from using your information in any manner other than in helping us to provide you with the products and services available from Eddie Bauer.</li>
<li><span style="text-decoration: underline;">Advertising Partners and Licensees:</span> With your prior consent, we may provide your personally-identifiable information, including your name, address, email address, as well as a record of any transactions you conduct on our website or offline with us to a third-party advertising partner and its service providers in order to deliver to you banner advertisements and other advertising tailored to your interests when you are online. Our advertising partners will make the data we provide it anonymous before using it to deliver ads online. We may partner with other service providers that collect non-identifiable information about your visits to our website, your interaction with our products and services, and your visits to other websites to serve ads targeted to your interests on other websites. The information is collected through the use of cookies, web beacons, and similar technologies, and does not include your name, address, e-mail address or other personal information. To learn more about these practices or to opt out of having this information used by our third-party advertising partners to serve ads targeted to your interests, please visit the Digital Advertising Alliance by&nbsp;<a
class="bg-link" href="https://digitaladvertisingalliance.org/"
target="_blank">clicking here</a>
To opt out of the online advertisements provided directly by Eddie Bauer&nbsp;<a
class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">click here</a> If you delete your cookies, use a different browser, or buy a new computer, you will need to renew your opt-out choice. In some cases we may provide your name and address to a company licensed to distribute Eddie Bauer products, whose additional products you also may find of interest. If you do not want us to share this information with licensees, we can add your name to our "do-not-share" list. Please&nbsp;<a
class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">click here</a> for contact information.</li>
<li><span style="text-decoration: underline;">Corporate Affiliates and Corporate Business Transactions:</span> We may share information with our affiliated companies. In the event of a merger, reorganization, acquisition, joint venture, assignment, spin-off, transfer, or sale or disposition of all or any portion of our business, including in connection with any bankruptcy or similar proceedings, we may transfer any and all personal information to the relevant third party.</li>
<li><span style="text-decoration: underline;">Legal Compliance and Security:</span> In certain special cases, we may disclose your personal information when we have reason to believe that disclosing this information is necessary to identify or contact you, or bring legal action against someone who may be causing injury to you, Eddie Bauer, or someone else. We may disclose your personal information when we believe the law requires it, in response to any demand by law enforcement authorities in connection with a criminal investigation, in response to civil or administrative authorities in connection with a pending civil case, governmental authorities or administrative investigation, or in connection with an investigation conducted by Eddie Bauer.</li>
</ul>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="fourth">
Companies other than Eddie Bauer</h3>
<p class="first">
Our privacy policy does not apply to companies other than Eddie Bauer. Companies other than Eddie Bauer are sometimes identified on our website, catalog or marketing materials. If you make a purchase from another company's catalog or website, the information you provide to that company, such as your credit card number and contact information, is provided to that company and not to Eddie Bauer. Providing this information to the company enables your transaction with the other company to take place. However, you should know that the company may, if you do not advise them to the contrary, share your name and postal and email addresses with third parties, including Eddie Bauer. You should also know that these companies have their own privacy and data collection practices and are not covered by the Eddie Bauer privacy statement. We are not responsible for the privacy practices employed by those third parties, nor are we responsible for the information or content their products and services contain.</p>
<p class="first">
For more information, including privacy policies, regarding such companies please see that company's home page and click on their Information link; or review the customer service section of any catalog you may receive from that company.</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="fifth">
What are "cookies" and how does Eddie Bauer use them?</h3>
<p class="first">
A "cookie" is a small data file stored on your computer. At Eddie Bauer, we place a unique identifier in the cookie and use the cookie to connect your computer with the information we store for you in our database. Some examples of the data we store in our database includes your account preferences, items added to your shopping bag, user- specific information on pages accessed, and previous visits to our site. Through cookies, web beacons, and similar technologies, we may alert you to new areas that we think might be of interest to you when you return to our site; record past activity at our site; provide advertisements that are tailored to your interests; or, customize Web page content.</p>
<p class="first">
We do not use cookies to store any of your personal or financial information on your computer. </p>
<p class="first">
Your browser or device may offer settings that allow you to choose whether browser cookies are set and to delete them. For more information about these controls and to exercise your cookie preferences, visit your browser or device’s help material. By choosing to accept an Eddie Bauer cookie you allow us to maintain the preferences stored in your Eddie Bauer "My Account" and process your order. If you choose not to accept the cookie, you will not be able to use certain features of our website and services, including making purchases on the Eddie Bauer site.</p>
<p class="first">
If you are shopping Eddie Bauer and are having difficulty adding products to your shopping bag or completing your order, please check your privacy settings in your browser. If you do not wish to change your privacy settings to "Medium" and you are currently using the "Medium High" or "High" settings, you may choose to override your current cookie handling practices for individual sites that you specify, including&nbsp;<a href="/">eddiebauer.com.</a>
Please&nbsp;<a class="bg-link"
href="/custserv/customer-service-technical-information.jsp">click here</a>
for instructions on how to override your normal cookie handling practices. If you prefer not to change your privacy settings or override your cookie handling practices, you can, as always, place your order by phone at 1-800-426- 8020.</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="sixth">
Your Rights and Choices</h3>
<p class="first">
We take reasonable steps to ensure that your personal information is accurate, complete, and up-to-date. Depending on where you live, you may have the rights described below.</p>
<p class="first">
You have the right to access, correct, or delete the personal information that we collect. You are also entitled to object to or restrict, at any time, the further processing of your personal information. You have the right to receive your personal information in a structured and standard format.</p>
<p class="first">
To protect the privacy and the security of your personal information, we may request information from you to enable us to confirm your identity and right to access such information, as well as to search for and provide you with the personal information we maintain. There are instances where applicable laws or regulatory requirements allow or require us to refuse to provide or delete some or all of the personal information that we maintain. </p>
<p class="first">
You may&nbsp;<a class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">contact us</a>
to exercise your rights. We will respond to your request in a reasonable timeframe, and in any event in less than 30 days. You may also lodge a complaint with the competent data protection authority regarding the processing of your personal information.</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="seventh">
California Privacy Rights</h3>
<p class="first">
If you are a California resident, California law permits you to request information regarding the disclosure of your personal information to third parties for the third parties’ direct marketing purposes. You may&nbsp;<a class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">contact us</a>&nbsp;to exercise your rights. If you are a California resident under the age of 18, and you are a registered user of our products or services, California law permits you to request and obtain removal of content or information you have publicly posted. To make such a request, please&nbsp;<a class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">contact us</a>
with specific information about where the content or information is posted and attesting that you posted it. We will then make reasonable, good faith efforts to remove the post from prospective public view, or anonymize it so you cannot be individually identified, to the extent required by applicable law. Please be aware that such a request does not ensure complete or comprehensive removal of the content or information you have posted and that there may be circumstances in which the law does not require or allow removal even if requested.</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="eight">
How is my information protected?</h3>
<p class="first">
We have implemented physical, technological, and organizational safeguards and security measures designed to protect against the loss, misuse, or unauthorized access or disclosure of your personal information under our control. We also take measures to protect the personal information we disclose to third parties, for example by entering into contractual agreements stipulating the confidentiality of the information and the purposes for which it is to be used. Please be aware that, despite our efforts, no security measures are perfect or impenetrable. You can help us by protecting and keeping your passwords safe at all times, and not using the same password for different applications or sites. You also can protect yourself and help us reduce the risk of fraud by promptly notifying us if you suspect that your credit card, user name or password is lost, stolen or used without permission. In the event of a security breach, we will promptly notify you and the proper authorities if required by law.</p>
<p class="first">
We will retain your personal information for as long as it is necessary to fulfill the purposes outlined in this Privacy Statement, unless a longer retention period is required or permitted by law.</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="HowtoReportFraud">
How to Report Fraud</h3>
<p class="first">
You can help us identify attempts at fraud.</p>
<p class="first">
Eddie Bauer will never ask you for sensitive personal information in an email.</p>
<p class="first">
If you receive a suspicious email appearing to come from Eddie Bauer, such as one requesting that you confirm personal information like your credit card number, or if you have received an email implying that it has been sent by Eddie Bauer after you have Opted Out of receipt of Eddie Bauer emails, you may forward the suspicious email to&nbsp;<a href="mailto:abuse@eddiebauer.com">abuse@eddiebauer.com.</a>
</p>
<p class="first">
You should not respond directly to suspicious email. And do not click on or open any hyperlink embedded in the suspicious email.</p>
<p class="first">
There are a few common features of fraudulent emails that can help you identify them as spam. The two most frequent are:</p>
<ol class="numberedList olWrapper">
<li>receiving an email that contains mistakes such as misspelled words or grammatical errors, and</li>
<li>receiving an email that asks for your confidential information or asks you to take immediate action on your account.</li>
</ol>
<p class="first">
We encourage all of our customers to protect themselves and their information:</p>
<ol class="numberedList olWrapper">
<li>do not share your credentials or account information with any third party, and</li>
<li>make sure that your system is properly patched, running anti-virus and a personal firewall.</li>
</ol>
<p class="first">
If you have already provided information in response to a suspicious email that appeared to have been sent by Eddie Bauer, you may report the suspicious email by forwarding it to&nbsp;<a href="mailto:abuse@eddiebauer.com">abuse@eddiebauer.com.</a>
</p>
<p class="first">
For more information on how to protect yourself and your personal information, you can visit the antiphishing Working Group's website at&nbsp;<a href="https://www.antiphishing.org/resources/overview/"
target="_blank">http://www.antiphishing.org/consumer_recs.html</a>.
</p>
<p class="first">
Thank you for helping us monitor and prevent fraud.</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="ninth">
Children's Policy</h3>
<p class="first">
We do not knowingly collect, use, or disclose information from children under 18. If we learn that we have collected the personal information of a child under 18 we will take steps to delete the information as soon as possible. Please immediately&nbsp;<a class="bg-link" target="_blank"
href="/custserv/ask-eddie-email.jsp">contact us</a>
if you become aware that a child under 18 has provided us with personal information.</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="tenth">
Changes to this Privacy Statement</h3>
<p class="first">
We may periodically change this Privacy Statement and expect most changes to be minor. Any non-material changes will take effect immediately upon posting of an updated Privacy Statement. For any significant changes to the Privacy Statement, we will provide you with a prominent notice of such changes.</p>
<p class="first">
Where applicable, your continued use of our Site after the effective date of this Privacy Statement means that you accept the revised Privacy Statement. If you do not agree to the revised Privacy Statement, please refrain from using our website.</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<div class="container">
<h3 id="eleventh">
Contact Information</h3>
<p class="last">
Feel free to contact us for any reason, including a request to be added to or taken off a list, or if you have any questions or concerns. Please note: When you contact us, please provide your name and address exactly as it appears on correspondence you have received from us.</p>
<h4 id="eigth">
By Postal Mail</h4>
<p class="aboveSubheader">
Eddie Bauer Customer Service<br />
P.O. Box 7001<br />
Groveport, OH 43125</p>
<h4 id="ninth">
By Phone</h4>
<p class="aboveSubheader">
Call 24-hours a day at 1-800-426-8020</p>
<h4 id="tenth">
By Email</h4>
<p class="aboveSubheader">
<a href="mailto:CustomerCare@csc.eddiebauer.com"> CustomerCare@csc.eddiebauer.com</a>
</p>
</div>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
<h3 id="twelth">
Communications from Eddie Bauer</h3>
<p class="email-caps">
To Unsubscribe from Eddie Bauer Email</p>
<p class="last">
If for any reason you would like to unsubscribe from our email program, please follow the instructions below:</p>
<p class="bold timeToProcess">
Eddie Bauer Email</p>
<ol class="numberedList olWrapper">
<li>Simply click the Unsubscribe link provided at the bottom of<br /> every email you receive.</li>
<li>Send us an email via our&nbsp;<a
class="bg-link"
href="/custserv/ask-eddie-email.jsp?unsubscribe=true"
target='_blank'>Contact Us</a> form.</li>
</ol>
<h4>
EDDIE BAUER MAIL AND TELEMARKETING PREFERENCES</h4>
<p class="last">
If you prefer not to receive Eddie Bauer catalogs from us, please&nbsp;<a class="bg-link"
href="mailto:CustomerCare@csc.eddiebauer.com?subject=Unsubscribe Catalogs">
contact us</a>
or call 1-800-426-8020. Please include your full name and mailing address as it appears on your catalog. We will remove your name from our mailing list as soon as possible. Please recognize that you may receive another catalog before we are able to remove you. If you wish to be placed on our
&quot;
Do Not Call
&quot;
list, please let us know during the call,&nbsp;<a class="bg-link"
href="mailto:CustomerCare@csc.eddiebauer.com?subject=Please Add To Do Not Call List">
contact us</a>
or call 1-800-426-8020.</p>
<div class="returnLink">
^<a href="#" class="anchor">Return to top</a>
</div>
</div>
<div class="rightColumn">
<style>
.contactUsHead {
margin-left:-10px;
margin-bottom:0px;
}
</style>
<div class="contactUs">
<div class="container first">
<div class="section first contactUs">
<div class="contactUsHead"><img src="/static/img/chat-phone_contact-us.png" alt="Contact Us. Our customer service specialists are standing by to assist you." /></div>
</div>
<div id="myChatLinkContainerCstSrv" class="clearfix contactUsHead">
<div id="myChatLinkCstSrv">
<div id="myChatLinkInfoCstSrv"><img src="/static/img/chat-phone-live-chat_unavail.png" alt="Chat Status" /></div>
</div>
</div>
<div class="section byPhone">
<table role="presentation">
<tr>
<td><span role="presentation" class="icon"></span></td>
<td>
<div class="label">By Phone</div>
<div class="content phone" aria-label="Phone Number - 8am to 2am EST daily">
<a title="Phone Number 1-800-426-8020 - 8am to 2am EST daily"
href="tel:+1-800-426-8020">
1-800-426-8020</a>
</div>
<div class="content text">8am to 2am EST daily</div>
</td>
</tr>
</table>
</div>
<div class="section byEmail">
<table role="presentation">
<tr>
<td><span role="presentation" class="icon"></span></td>
<td>
<div class="label">By Email</div>
<div class="content text"><a class="links underline" href="/custserv/ask-eddie-email.jsp">Send us an email</a> with your questions.</div>
</td>
</tr>
</table>
</div>
<div class="section byMail">
<table role="presentation">
<tr>
<td><span role="presentation" class="icon"></span></td>
<td>
<div class="label">By Mail</div>
<div class="content address1">Eddie Bauer Customer Service</div>
<div class="content address2">P.O. Box 7001</div>
<div class="content address3">Groveport, OH 43125</div>
</td>
</tr>
</table>
</div>
</div>
</div>
<div class="safeSecureShopping">
<div class="container">
<div class="section first">
<div role="listitem" class="label">Safe & Secure Shopping</div>
<div class="content text">Shop with confidence. Your order is safe and secure.</div>
<span class="safeSecureShopping-faqs">
<a href="javascript:openFBModal('/includes/faqs/privacy.jsp','type:ajax; scrolling:no; contentScroll:false width:400; height:450;')" class="links first" title="Privacy: Opens Privacy Modal">Privacy</a>				|
<a href="javascript:openFBModal('/includes/faqs/security.jsp','type:ajax; scrolling:no; contentScroll:false width:400; height:250;')" class="links last" title="Security: Opens Security Modal">Security</a>
</span>
<div id="DigiCertClickID_Jd4GFR9i" data-language="en">
<a href="#" class="verisign-modal-link"></a>
</div>
</div>
</div>
</div>
<script type="text/javascript">
var __dcid = __dcid || [];
__dcid.push([ "DigiCertClickID_Jd4GFR9i", "13", "s", "black",
"Jd4GFR9i" ]);
(function()
{
var cid = document.createElement("script");
cid.async = true;
cid.src = "//seal.digicert.com/seals/cascade/seal.min.js";
var s = document.getElementsByTagName("script");
var ls = s[(s.length - 1)];
ls.parentNode.insertBefore(cid, ls.nextSibling);
}());
</script>
</div>
</div>
</div>
</div>
</div><footer class="main-footer main-footer-light">
<div class="main-footer-wrapper">
<div class="foot-first-wrapper">
</div>
<div class="foot-second-wrapper">
<div class="foot-col-item item-1">
<div class="foot-item foot-nav-item">
<h5>Customer Service 1-800-426-8020</h5>
<ul class="nav-list">
<li>
<a href="/custserv/customer-service-landing.jsp">Help & Support</a>
</li>
<li>
<a href="/custserv/ask-eddie-email.jsp">Contact Us</a>
</li>
<li>
<a href="/user/login.jsp?successUrl=/user/order-history.jsp">Order Status</a>
</li>
<li>
<a href="/custserv/customer-service-shipping.jsp">Shipping</a>
</li>
<li>
<a href="/custserv/customer-service-returns-and-exchanges.jsp">Returns</a>
</li>
<li>
<a href="/custserv/customer-service-gift-cards.jsp">Gift Cards</a>
</li>
<li>
<a href="/storelocator/store_locator.jsp">Store Locator</a>
</li>
<li>
<a href="/custserv/ask-eddie-email.jsp?subject=catalog">Request a Catalog</a>
</li>
<li>
<a href="/catalog/catalog-quick-order.jsp">Catalog Quick Order</a>
</li>
<li>
<a href="/user/login.jsp?successUrl=/user/wishlist.jsp">Wish List</a>
</li>
<li>
<a href="/custserv/customer-service-size-charts.jsp">Size Charts</a>
</li>
</ul>
</div>
</div>
<div class="foot-col-item item-2">
<div class="foot-item foot-nav-item">
<h5>Company Info</h5>
<ul class="nav-list">
<li>
<a href="/company-info/company-info-about-us.jsp">About Us</a>
</li>
<li>
<a href="https://careers.eddiebauer.com/?_ga=2.108495940.750893424.1540851489-1669448594.1540851489">Careers</a>
</li>
<li>
<a href="/company-info/company-info-affiliate-program.jsp">Affiliate Program</a>
</li>
<li>
<a href="/static/pdf/California_Transparency_Supply_Chain.pdf">Supply Chain Disclosure</a>
</li>
<li>
<a href="/view/shopping-index/index.html">Site Map</a>
</li>
</ul>
</div>
<div class="foot-separator"></div>
<div class="foot-item foot-nav-item">
<h5>Eddie Bauer Credit Card</h5>
<ul class="nav-list">
<li>
<a href="/custserv/customer-service-eddie-bauer-credit-cards.jsp">Learn More & Apply</a>
</li>
<li>
<a href="https://d.comenity.net/eddiebauer/">Pay My Bill</a>
</li>
</ul>
</div>
</div>
<div class="foot-col-item item-3">
<h5>ADVENTURE IN YOUR INBOX</h5>
<form id="emailSignupForm" action="#" method="GET">
<p>Get the latest news and offers.</p>
<label for="signupEmail" class="visually-hidden">Enter email address</label>
<input aria-invalid="true" aria-describedby="err-msg" type="email" id="signupEmail" name="signupEmail" data-error="" class="text-input" placeholder="Enter email address">
<button id="emailSignupSubmit" class="button">Go</button>
</form>
<div class="foot-separator"></div>
<div class="foot-item earn-rewards">
<img alt="Eddie Bauer Adventure Rewards" src="/static/img/rewards/68_footer_logo_259x41.png">
<h5 class="ft_colhead2">Live Your Adventure. Get Rewarded.</h5>
<div id="cust-member" class="cust-member">Already a Member?<a id="acc-login-footer" tabindex="0" href="/user/acc-login.jsp" aria-label="Follow this link to sign in to your account."> Sign In</a></div>
<a id="footer-register" style="width:95px;height:38px;padding-top:5px;background-color:#c2d92b;"
href="/user/acc-register.jsp" class="button-solid">JOIN NOW</a>
</div>
</div>
<div class="foot-col-item item-4">
<h5>FOLLOW EDDIE BAUER</h5>
<ul class="connect-list">
<li><a href="https://www.facebook.com/eddiebauer" title="Facebook" target="_blank">Facebook</a></li>
<li><a href="https://www.youtube.com/eddiebauer" title="YouTube" target="_blank">YouTube</a></li>
<li><a href="https://www.instagram.com/eddiebauer" title="Instagram" target="_blank">Instagram</a></li>
<li><a href="https://www.pinterest.com/eddiebauer" title="Pinterest" target="_blank">Pinterest</a></li>
<li><a href="https://www.twitter.com/eddiebauer" title="Twitter" target="_blank">Twitter</a></li>
</ul>
<div class="foot-item">
<h5>GUARANTEED FOR LIFE</h5>
<p>Every product we sell carries a lifetime warranty. If you are unhappy with it for any reason, you may return it for an exchange or refund.</p>
</div>
</div>
<div class="foot-site-toogle">
<div class="left item-1">You Are Now Shopping:</div>
<div class="left">
<img src="/static/img/site/170303_svg_check.svg" alt="active" width="16px;" height="12px;"/>
<img src="/static/img/site/170303_svg_us_flag.svg" alt="" width="36px;" height="23px;"/>US
<a title="" class="siteSwitchLayer" data-linkUrl="http://www.eddiebauer.ca/?atg.multisite.remap=false" href="#"><img src="/static/img/site/170303_svg_canadian_flag_gray_off.svg" alt="" width="36px;" height="23px;"/>CANADA
</a></div>
</div>
</div>
<a class="footTopLink old-footer-link" href="#pageTop" style="display:none;">Back to top</a>
<div class="foot-third-wrapper">
<div class="foot-item">
<div class="foot-item foot-info">
<div class="foot-tagline">Live Your Adventure<sup>&reg;</sup></div>
<div class="foot-info-wrapper clearfix">
<p class="policy-info left">©2018 Eddie Bauer LLC. All rights reserved. eddiebauer.com® is a registered trademark of Eddie Bauer Licensing Services LLC.</p>
<span class="right phone-number old-footer" style="display:none;" aria-label="Phone Number - 24 hours a day 7 days a week">
<a title="Phone Number - 24 hours a day 7 days a week" aria-label="Phone Number 1-800-426-8020 - 24 hours a day 7 days a week" href="tel:1-800-426-8020">1-800-426-8020</a>
&mdash; 24 hours a day 7 days a week
</span>
<ul class="policy-links right">
<li><a href="/company-info/company-info-terms-of-use.jsp">Terms of Use</a>
</li>
<li><a href="/company-info/company-info-privacy-and-security.jsp">Privacy Policy</a>
</li>
<li><a href="/view/shopping-index/index.html">Site Map</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</div>
</footer>
<!--
DO NOT REMOVE THIS []
Server Name: 772623-rsatgbot1.eddiebauer.com:8850
Profile: u2461679575
Order: A2474909576
mode: 
Server is alive... />
-->
<script type="text/javascript">
fbPageOptions = {
graphicsPath:"/static/js/jquery/graphics/"
};
</script>
<script src="/static/combined/js/script1.js"></script>
<script>
//var timeout="30";
//alert("timeout is " + timeout)
</script>
<script type="text/javascript">
$(document).ready(function() {
//alert("document is ready");
EBGLOBAL.sessionTimeout.durationInMinutes = 30;
EBGLOBAL.sessionTimeout.durationInMilliSeconds = EBGLOBAL.sessionTimeout.durationInMinutes * 60000;
EBGLOBAL.sessionTimeout.pages = "/checkout/bag.jsp,/checkout/login.jsp,/checkout/combinedAddress.jsp,/checkout/delivery.jsp,/checkout/payment.jsp,/checkout/review.jsp,/checkout/visa-review.jsp";
EBGLOBAL.sessionTimeout.resetTimeout();
});
</script>
<!-- @TODO: [jquery.validate.additional-methods.min.js] this may not be necessary
<script src="/static/js/vendor/jquery.validate.additional-methods.min.js"></script>
-->
<script src="/static/js/gauge.min.js"></script>
<script src="/static/js/main.js"></script>
<script type="text/javascript" src="/static/js/jquery/jquery.mask.js"></script>
<script type="text/javascript" src="/static/js/checkout.js"></script>
<script src="/static/combined/js/script6.js"></script>
<script src="/static/js/account.js"></script>
<script src="/static/js/endeca-auto-suggest.js"></script>
<script>
if(window.SS && localStorage.getItem('tealium_va')){
if(JSON.parse(localStorage.getItem('tealium_va')).badges[9733]){SS.EventTrack.metric('tealium9733');}
if(JSON.parse(localStorage.getItem('tealium_va')).badges[9680]){SS.EventTrack.metric('tealium9680');}
if(JSON.parse(localStorage.getItem('tealium_va')).badges[9682]){SS.EventTrack.metric('tealium9682');}
if(JSON.parse(localStorage.getItem('tealium_va')).badges[9725]){SS.EventTrack.metric('tealium9725');}
if(JSON.parse(localStorage.getItem('tealium_va')).badges[9801]){SS.EventTrack.metric('tealium9801');}
if(JSON.parse(localStorage.getItem('tealium_va')).badges[9803]){SS.EventTrack.metric('tealium9803');}
if(JSON.parse(localStorage.getItem('tealium_va')).badges[9805]){SS.EventTrack.metric('tealium9805');}
if(JSON.parse(localStorage.getItem('tealium_va')).badges[9807]){SS.EventTrack.metric('tealium9807');}
}
</script>

<script>if(window.SS){SS.PageTimer.time('dwell', 'pgdwell'+window.location.pathname+window.location.search, window.__ss_load_start);}</script>

<script>if(window.SS){SS.PageTimer.time('load', 'pgload'+window.location.pathname+window.location.search, window.__ss_load_start);SS.PageTimer.time('ready', 'pgready'+window.location.pathname+window.location.search, window.__ss_load_start);}</script>
</body></html>