<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="sv">
    <head>
        <!-- WARNING: Automated extraction of rates is prohibited under the Terms of Use. -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="meta" href="http://www.xe.com/labels.rdf" type="application/rdf+xml" title="ICRA labels"/>
<meta http-equiv="pics-label" content='(pics-1.1 "http://www.icra.org/pics/vocabularyv03/" l gen true for "http://xe.com" r (n 0 s 0 v 0 l 0 oa 0 ob 0 oc 0 od 0 oe 0 of 0 og 0 oh 0 c 0) gen true for "http://www.xe.com" r (n 0 s 0 v 0 l 0 oa 0 ob 0 oc 0 od 0 oe 0 of 0 og 0 oh 0 c 0))'/>
<meta name="version" content="3.43"/>
<meta http-equiv="pics-label" content='(pics-1.1 "http://www.icra.org/ratingsv02.html" l gen true for "http://www.xe.com" r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true for "http://www.xe.com" r (n 0 s 0 v 0 l 0))'/>
<meta http-equiv="pics-label" content='(pics-1.1 "http://www.icra.org/ratingsv02.html" l gen true for "http://www.xe.net" r (cz 1 lz 1 nz 1 oz 1 vz 1) "http://www.rsac.org/ratingsv01.html" l gen true for "http://www.xe.net" r (n 0 s 0 v 0 l 0))'/>
<meta http-equiv="pics-label" content='(pics-1.1 "http://www.rsac.org/ratingsv01.html" l gen true comment "RSACi North America Server" for "" on "1999.09.07T12:36-0800" r (n 0 s 0 v 0 l 0))'/>
<meta http-equiv="pics-label" content='(pics-1.1 "http://www.rsac.org/ratingsv01.html" l gen true comment "RSACi North America Server" for "http://www.xe.net/" on "1999.09.07T12:30-0800" r (n 0 s 0 v 0 l 0))'/>
<meta name="lang" content="sv"/>
<meta name="description" content="XE.com's Privacy Policy. This Policy is incorporated into, and subject to, XE's Terms of Use. If you do not agree to these terms, please do not use or access the Services."/>
<meta name="keywords" content="xe, xe currency, privacy policy, xe.com, terms, services, advertising, contact, cookies, information, secure, currency converter, xe money transfer, trademark "/>
<link rel="manifest" href="/manifest.json"/>
<link rel="icon" href="/favicon_white_180x180.ico" sizes="180x180"/>
<link rel="icon" href="/favicon_white_128x128.ico" sizes="128x128"/>
<link rel="icon" href="/favicon_white_64x64.ico" sizes="64x64"/>
<link rel="icon" href="/favicon_white_32x32.ico" sizes="32x32"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>XE - Privacy Policy</title>
        <link rel='stylesheet' href='https://www.xe.com/themes/xe/t_css/privacy_resp.7h.css' type='text/css' /><link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Noto+Sans+KR:400,700' type='text/css' />        <!--[if IE]><style> .layout { height: 0; he\ight: auto; zoom: 1; } </style><![endif]-->
        			<script type='text/javascript'>
				var e9AdSlots  = { 
				  sv_lb : {site:'xecom', adSpace:'SV', size:'728x90,468x60', noAd: '1'},
sv_rs : {site:'xecom', adSpace:'SV', size:'300x250,300x600,160x600', noAd: '1'},
ros_bs1 : {site:'XEInternal', adSpace:'HROS', size:'180x150', noAd: '1'},
ros_bs2 : {site:'XEInternal', adSpace:'HROS', size:'180x150', noAd: '1'},
ros_bs3 : {site:'XEInternal', adSpace:'LROS', size:'180x150', noAd: '1'}
				};			
			</script>		<script src='https://www.xe.com/themes/xe/t_js/toppages.6d.js' type='text/javascript'></script>			<script>
			    if (!gdprIsCookieTagAllowed("tag_GoogleAnalytics")) {
                    window['ga-disable-UA-851277-1'] = true;
                }
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
				ga('create', 'UA-851277-1', 'auto');
				ga('require', 'displayfeatures');
				ga('send', 'pageview');
				
			</script>
    </head>

    <body>
                    <script type="text/javascript">
                var utag_data = {};
            </script>
            <script type="text/javascript">
                (function(a,b,c,d){
                a='//tags.tiqcdn.com/utag/xe/main/prod/utag.js';
                b=document;c='script';d=b.createElement(c);d.src=a;d.type='text/java'+c;d.async=true;
                a=b.getElementsByTagName(c)[0];a.parentNode.insertBefore(d,a);
                })();
            </script><!-- WARNING: Automated extraction of rates is prohibited under the Terms of Use. -->
<div id="headerContainer" class="nonReactPageFix-header"></div>
        <main class="wrapper" lang=en>
            <div id="leaderB" class="bannerAdContainer">
	<div class="bannerAd" id="sv_lb">
				<script type="text/javascript">
						e9AdSlots['sv_lb']['mobileAd'] = "<a href='https://www.xe.com/sv/xemoneytransfer/?WT.seg_1=XTR-300x50-XROSINT-ADL-XX-V01-130901'><img class='center' alt='Ad' src='https://www.xe.com/themes/xe/images/300x50_xemoneytransfer.png' border='0'/></a>";
			e9AdSlots['sv_lb']['desktopAd'] = "<a href='https://www.xe.com/sv/xemoneytransfer/?WT.seg_1=XTR-728x90-XROSINT-ADL-XX-V01-130901'><img alt='Ad' src='https://www.xe.com/themes/xe/images/728x90_xemoneytransfer.png' border='0'/></a>";
			e9AdSlots['sv_lb']['mobileThreshold'] = 767;			
			enqueueAd('sv_lb');
		</script>
		<noscript>
			<div class='center'><a href='https://www.xe.com/sv/xemoneytransfer/?WT.seg_1=XTR-728x90-XROSINT-ADL-XX-V01-130901'><img alt='Ad' src='https://www.xe.com/themes/xe/images/728x90_xemoneytransfer.png' border='0'/></a></div>
		</noscript>	</div>
    <div class="leaderBbr">
            AD
	</div>
</div>            <div class="bodyContent" id="content" ontouchstart="void(0);">
                                <div class="main" id="contentL" role="main">
                    <div class='module clearfix iTools'>
			<h1>Privacy Policy</h1>
			
										<div class="pageDescriptionWrap"> 
				<span class=''>Effective as of May 25, 2018 (the "Effective Date").

<br />
<br /></span><span class=''>XE.com, XE currency mobile application and the XE travel mobile application Privacy Notice
<br />
<br /></span><p class='pageDescription'>In this Privacy Notice we explain how we collect and use your personal information that we obtain when you use our services, visit or use our websites or mobile applications or otherwise interact with us, how we share your information and the steps we take to protect your information.</p>
			</div>			<div id='' class='relatedLinks LnBxCUS'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC1'>1 - WHO WE ARE AND THE APPLICATION OF THIS PRIVACY NOTICE</span></h2>
        </div>		<div>
    	<p >This Privacy Notice applies to XE.com, Inc. (“XE”, “we”, “our” or “us”), a subsidiary of Euronet Worldwide, Inc. (“Euronet”). Further details on Euronet and the companies within the Euronet group (the “Euronet Group”) are available at: <a href='http://www.euronetworldwide.com' target="_blank">http://www.euronetworldwide.com</a>.</p><p >XE is an affiliate of HiFX Europe Limited within the Euronet Group.</p><p >We are committed to the privacy and security of your personal data. This Privacy Notice describes how we collect and use personal data, in accordance with applicable law and our standards of ethical conduct.</p><p >XE.com, Inc. at 1145 Nicholson Rd, Suite 200, Newmarket ON, L3Y 9C3 Canada will be the &ldquo;data controller&rdquo; or &ldquo;controller&rdquo; in relation to any personal data provided to us directly via email, phone, and post or via xe.com (the &ldquo;Website&rdquo;), or the XE currency mobile application or XE travel mobile application (individually and together, the &ldquo;App&rdquo;). This means that we are responsible for deciding how we will hold and use personal data about you.</p><p >The Euronet Group Data Protection Officer can be contacted:</p>			
			<ul class="faqList-cookie">
				<li>By email at: DPO@xe.com or</li><li>By post to: Euronet Data Protection Officer, Calle Cantabria, 2 28108 Alcobendas, Madrid, Spain.</li>
			</ul><p >By using or navigating the Website, the App or any product or service (including the XE Currency Data service (“XECD”); and XE Rate alerts, the XE Currency Update daily email newsletter and the XE Currency Market Analysis daily email newsletter (each an “XE Email Service” and together “XE Email Services”) offered by us through the Website and/or App (collectively, “Services”), you acknowledge that you have read, understand, and agree to be bound by this Privacy Notice. You should not provide us with any of your information if you do not agree with the terms of this Privacy Notice. This Privacy Notice is incorporated into, and subject to, XE's Terms of Use at <a href='https://www.xe.com/sv/legal/'>https://www.xe.com/legal/</a></p><p >We encourage you to review and check the Website / App regularly for any updates to this Privacy Notice. We will publish the updated version on the Website / App and by continuing to deal with us, you accept this Privacy Notice as it applies from time to time.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC2'>2 - DATA PROTECTION PRINCIPLES</span></h2>
        </div>		<div>
    	<p >&ldquo;Personal data&rdquo; means any information that enables us to identify you or the beneficiary of your transaction with us, directly or indirectly, such as name, email, address, telephone number, any form of identification number or one or more factors specific to your or your beneficiary&rsquo;s identity.</p><p >We are committed to complying with applicable data protection laws and will ensure that personal data is:</p>			
			<ul class="faqList-cookie">
				<li>Used lawfully, fairly and in a transparent way;</li><li>Collected only for valid purposes that we have clearly explained to you and not used in any way that is incompatible with those purposes;</li><li>Relevant to the purposes we have told you about and limited only to those purposes;</li><li>Accurate and kept up to date;</li><li>Kept only as long as necessary for the purposes we have told you about;</li><li>Kept securely.</li>
			</ul>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC3'>3 - WHAT PERSONAL DATA DO WE COLLECT AND HOW DO WE COLLECT IT?</span></h2>
        </div>		<div>
    	<p >We may collect personal data when you give it to us, including when you indicate that you would like to receive any of our Services, when you register with us, when you complete forms online, when you speak with us over the telephone, when you write to us, when you visit the Website or App and, in certain circumstances as set out in this Privacy Notice, when you have provided your information to a related company operating under the HiFX,  xe.com or xe brand (each an &ldquo;XE Company&rdquo; and together the &ldquo;XE Companies&rdquo;).</p><p >We may collect and process the following personal data:</p>			
			<ul class="faqList-cookie">
				<li>Personal details, such as data which may identify you. This may include your name, title, residential and/or business address, email, telephone and other contact data, signature, IP address and travel details (such as destination country or trip details) (&ldquo;Identity Personal Data&rdquo;).</li><li>If you have provided your consent for us to collect such information and not withdrawn such consent, non-identifiable GPS-based location details whilst using the Website or App (&ldquo;Location Data&rdquo;).</li><li>Information from which you may be indirectly identified, such as a client identification number or online identifier (&ldquo;Indirectly Identifiable Personal Data&rdquo;).</li>
			</ul><p >We may also receive information in connection with transactions you carry out on our Website, such as the last four digits of the payment card you used to make payment for the XECD service (as provided to us by the third party payment processor) (&ldquo;Payment Data&rdquo;).</p><p ><span style="font-weight: bold;">Cookies and similar technologies</span><br>When you use our Website or App we collect information via cookies and similar technologies, including the IP address of visitors, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform. We may use this data for the following purposes:</p>			
			<ul class="faqList-cookie">
				<li>To measure the use of our Website / App and services, including number of visits, average time spent on a website, pages viewed, page interaction data (such as scrolling, clicks, and mouse-overs), etc., and to improve the content we offer;</li><li>To administer the Website / App and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;</li><li>As part of our efforts to keep the Website / App safe and secure.</li>
			</ul><p >Due to their core role of enhancing or enabling usability or site processes, disabling cookies may prevent you from using certain parts of our Website / App. It will also mean that some features on our Website or App will not function if you do not allow cookies.</p><p >For more information please read our <a href='https://www.xe.com/sv/cookiepolicy.php'>Cookie Policy</a>.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC4'>4 - HOW WE USE YOUR PERSONAL DATA</span></h2>
        </div>		<div>
    	<p >Personal data collected through our Website or App is typically stored and processed in Canada; however, in some instances, it may be transferred, stored, and/or processed outside of Canada (see section 5 for further details).</p><p >We have summarised below the ways in which we may use your personal data and our basis for such usage:</p><style>
.policyTable td { border: 1px solid black }
</style>
<table class="policyTable">
	<tr><td>How we use personal data</td><td>Our basis for using your personal data</td></tr>
	<tr><td>Registration and administration. We may use Identity Personal Data to enable you to register with us and, once your registration with us is complete, for the administration of your account, to contact you, to update our records about you, and to respond to and process your queries and requests.</td><td>Performance of a contract / service – delivery of XE Email Services or provision of the XECD service.</td></tr>
	<tr><td>Requesting access to tools and information. You may wish to have access to certain tools and information (such as XE Email Services or XECD) made available on Our Website / App. We may collect and use Identity Personal Data for these purposes.</td><td>Performance of a contract / service -  delivery of XE Email Services or provision of the XECD service.</td></tr>
	<tr><td>Supply of our Services. We may use Identity Personal Data (and where it is collected, Payment Data)) so that we can supply you with our Services which you use or have requested.</td><td>Performance of a contract/ service    -  delivery of XE Email Services or provision of the XECD service.</td></tr>
	<tr><td>Location.  If you have given your consent for us to so and not withdrawn such consent, we may collect and use Location Data to provide you with a tailored experience on the Website or App related to your location, such as displaying the local currency in the relevant location.</td><td>Your consent for us to process your Location Data for such purposes.</td></tr>
	<tr><td>Service communications. We may use Identity Personal Data to notify you about changes or developments relating to our Services which you used or have requested.</td><td>Necessary for our legitimate interest to notify you about changes or developments relating to our products and services which you use or have requested.</td></tr>
	<tr><td>Compliance. We may use Identity Personal Data (and where it is collected, Payment Data) for compliance purposes, including the prevention and detection of crime, tax evasion or fraud.</td><td>Necessary to comply with a legal obligation.</td></tr>
	<tr><td>Telephone calls. We may monitor and record (via automated means or transcripts) our telephone calls with you (which may involve Identity Personal Data (and where it has been collected, Payment Data) and we may use any transcripts of these calls so we can be sure we understand the instructions you give us and so we have a clear record of our discussion with you.</td><td>Necessary to comply with a legal obligation. 
Performance of a contract/ service    - delivery of XE Email Services or provision of the XECD service.</td></tr>
	<tr><td>Marketing. In certain circumstances, we may use your Identity Personal Data to contact you with marketing communications in relation to the Services or the services and products of XE Companies (which may include HiFX Europe Limited).</td><td>See section 11 below.</td></tr>
        <tr><td>Profiling. Xe may combine Indirectly Identifiable Personal Data with other information generated during the use of our Services to create individual profiles for customers through automated processes.</td><td>Necessary for our legitimate interest to enhance the customer experience by allowing for better use of our services. See section 12 below.</td></tr>
</table><p ><br><span style="font-weight: bold;">Non-identifiable data</span><br>Whenever possible, we use data from which you cannot be identified directly (such as IP addresses and anonymous demographic and usage data) rather than personal data. This non-identifiable data may be used to tailor your experiences with the Services by showing content in which we think you will be interested and displaying content according to your preferences. Non-identifiable data may also be used to improve our internal processes or delivery of services.</p><p >We may use aggregate data for a variety of purposes, including analysing user behaviour and characteristics in order to measure interest in (and use of) the various portions and areas of the Services. We also may use the data collected to evaluate and improve the Services and analyse traffic to the Services.</p><p >In some circumstances we may anonymise your personal data so that it can no longer be associated with you, in which case we may use such data without further notice to you.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC5'>5 - IS DATA COLLECTED SHARED WITH OR COLLECTED BY THIRD PARTIES</span></h2>
        </div>		<div>
    	<p ><span style="font-weight: bold;">XE Companies</span><br>We may share your personal data with other XE Companies (including HiFX Europe Limited in the United Kingdom (“UK”)) in order to enable or facilitate us to provide you with any of the Services you have requested, for our or an XE Company’s compliance purposes and, where you have consented and not withdrawn your consent, for the XE Company’s direct marketing purposes (see section 11 below).</p><p ><span style="font-weight: bold;">Euronet Group</span><br>We may share your personal data with Euronet and affiliates in the Euronet Group (some of which are based outside the European Economic Area (“EEA”) and in Canada and the United States – further details are set out at the end of this section 5) for the purposes, or to enable or facilitate the purposes, set out in section 4 and 11 of this Privacy Notice. This may include sharing your personal data within the Euronet Group for compliance purposes.</p><p ><span style="font-weight: bold;">Aggregated statistical analysis</span><br>We may use statistical analysis of aggregate data to inform advertisers of aggregate user demographics and behaviour, as well as the number of users that have been exposed to or clicked on their advertising banners. We will provide only aggregate data from these analyses to third parties.</p><p ><span style="font-weight: bold;">Third party service providers</span><br><p>We may share personal data we collect with third party service providers to manage, enable or facilitate certain aspects of the Services we provide and if we do so, we will have safeguards in place with such third party service providers requiring them to protect the personal data.</p><p>We use advertising services suppliers on our Website and App, such as Exponential Interactive, Inc. (“Exponential”) and MoPub, a division of Twitter, Inc. and Twitter International Company (“MoPub”), who, along with their advertising partners, may collect and use personal data when you interact with our Website or App.  Further details are set out at section 6 below.</p></p><p ><span style="font-weight: bold;">Corporate process</span><br>We may transfer your personal data to a third party as a result of a sale, acquisition, merger, or reorganisation involving Euronet, a company within the Euronet Group, or any of their respective assets. In these circumstances, we will take reasonably appropriate steps to ensure that your information is properly protected.</p><p ><span style="font-weight: bold;">Legal and regulatory</span><br>We may also disclose your personal data in special cases if required or requested to do so by law, court order, or other governmental authority, or when we believe in good faith that disclosing this data is otherwise necessary or advisable, such as to identify, contact, or bring legal action against someone who may be causing injury to, or interfering with, our rights or property, our services, another user, or anyone else that could be harmed by such activities (for example, identify theft or fraud).</p><p ><span style="font-weight: bold;">Sharing personal data outside of Canada and the EEA</span><br>As explained above, we may share your personal data within the Euronet Group (including to XE Companies), which may involve transferring your data outside of Canada and the EEA. It is important for you to note that the laws on holding data in Canada (or any other country in which we transfer, store or process your data) may be less stringent than the laws of your country, but XE intends to adhere to the principles set forth in this Policy, unless otherwise required by applicable laws.<br>If we share personal data with third party service providers based outside of Canada or the EEA, we will ensure a level of protection and safeguarding of your personal data.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC6'>6 - ADVERTISING</span></h2>
        </div>		<div>
    	<p >Advertisements that appear on the Website or App or otherwise in the Services are generally delivered (or &quot;served&quot;) directly to you by third party advertisers. These third party advertisers have no access to the information you have provided directly to XE.</p><p >If you have provided your consent by accepting &ldquo;Targeting Cookies&rdquo; through the Website cookie consent manager or enabled &ldquo;Targeting&rdquo; and &ldquo;Location&rdquo; on the App, the advertisements that are served may be personalised to you.</p><p ><span style="font-weight: bold;">Advertising on our Website</span><br>Advertisements on our Website may be served by Exponential or Exponential’s advertising partners. </p>			
			<ul class="faqList-cookie">
				<li><p>If you have provided your consent by accepting Targeting Cookies Exponential will automatically receive your IP address. Exponential or Exponential’s advertising partners may also download cookies and similar technologies such as pixel tags/beacons and scripts downloaded to your computer (‘cookies’) to measure the effectiveness of their ads and to personalize advertising content. Doing this allows them to recognize your computer each time they send you an advertisement in order to measure the effectiveness of their ads and to personalize advertising content. In this way, they may compile information about where individuals using your computer or browser saw their advertisements and determine which advertisements were clicked.</p><p>Accordingly, if you have provided your consent by accepting Targeting Cookies, Exponential will collect and profile personal data in the form of IP address and cookies from users on the Website in order to provide targeted online advertising and ad measurement. You may change your consent decision at any time via the Website cookie consent manager.  Exponential’s collection and use of your personal data is covered by the Exponential <a href='http://exponential.com/privacy/' target="_blank">Privacy Policy</a>.</p></li><li>If you do not accept Targeting Cookies, Exponential or any other third party advertisers will not receive your IP address or download any cookies to your computer through the Website.  However, advertisements that are not specific or personalised to your or your device may still be served to you on the Website.</li>
			</ul><p ><span style="font-weight: bold;">Advertising on our App</span><br><p>Our App is integrated with MoPub.  This allows us to serve advertisements in the App and for advertisers to reach audiences that may be interested in the advertiser’s products or services and MoPub may collect and use personal data for these purposes. MoPub’s <a href='https://www.mopub.com/legal/privacy/' target="_blank">privacy policy</a> will apply to MoPub’s collection and use of personal data.</p><p>As set out in the MoPub privacy policy, MoPub is designed to avoid collecting information such as your name, address or email address but the personal data collected may enable MoPub to recognize your device over time and across apps. Depending on where you live and your privacy choices on the App, the personal data MoPub collects may include device identifiers and information, app usage information, (if you have enabled Location Services) geo-location, information about interests to make ads served more relevant and information about interactions with ads.</p></p>			
			<ul class="faqList-cookie">
				<li>If you have provided your consent by enabling &ldquo;Targeting&rdquo; and &ldquo;Location&rdquo; for the App, MoPub will collect and use the personal data summarised above and described in the MoPub privacy policy to serve you personalised advertising.</li><li>If you do not enable Targeting and Location for the App, you will not receive personalised advertisements and MoPub will not collect and use personal data for such purposes. MoPub may still collect and process certain personal data when MoPub consider they have a basis for doing so, as described in the MoPub privacy policy.</li>
			</ul><p ><span style="font-weight: bold;">Remarketing on the Website and App</span><br>If you have provided your consent by accepting Targeting Cookies on the Website or enabled Targeting on the App, the Website / App may use Google AdWords, including the remarketing and Similar Audiences features. This enables Google, through the use of cookies, to identify the fact that you have visited the Website / App, to track aspects of your usage of the Website / App and combine that with what it knows about your usage of other websites in the Google ad network. We use these services to advertise to visitors of Google ad network websites who have previously visited our Website / App or who Google deems to have shared interests with visitors of our Website / App.  Any data we collect through your use of our Website / App will be used by us in accordance with this Privacy Notice and Google’s collection and use of your personal data is covered by the <a href='http://www.google.com/privacy.html' target="_blank">Google privacy policy</a>.  You can set preferences for how Google advertises to you using the <a href='http://www.google.com/settings/ads' target="_blank"> Google Ad Preferences page</a>.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC1'>7 - HOW LONG IS YOUR PERSONAL DATA RETAINED?</span></h2>
        </div>		<div>
    	<p >Personal data is used for different purposes, and is subject to different standards and regulations. In general, personal data is retained for as long as necessary to provide you with services you request, to comply with applicable legal, accounting or reporting requirements, and to ensure that you have a reasonable opportunity to access the personal data.</p><p >To determine the appropriate retention period for personal data, we consider the applicable legal requirements, the amount, nature, and sensitivity of the personal data, the potential risk of harm from unauthorised use or disclosure of your personal data, the purposes for which we process your personal data and whether we can achieve those purposes through other means. For example:</p>			
			<ul class="faqList">
				<li>Legal and regulatory requirements. We will retain your personal data if required to comply with legal and regulatory obligations, compliance procedures and legal limitation periods. </li><li>Customer service. If you provide us with your personal data but do not have a XECD or XE Email Services account, we may (subject to any legal or regulatory considerations) retain your personal data for as long as necessary to deal with your query.</li><li>Marketing and XE Email Services. Personal data provided to us for marketing and XE Email Services purposes may be retained until you opt out or until we become aware the data is inaccurate.</li>
			</ul>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC2'>8 - IS CORRESPONDENCE THAT YOU SEND TO US SAVED?</span></h2>
        </div>		<div>
    	Yes. If you send us correspondence, including e-mails, we may retain such data along with any records of your account. We may also retain customer service correspondence and other correspondence involving you, us and any XE Company, our partners, and our suppliers.
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC3'>9 - DATA SECURITY</span></h2>
        </div>		<div>
    	<p >We are committed to maintaining the security of your personal data and have measures in place to protect against the loss, misuse, and alteration of the data under our control.</p><p >We employ modern and secure techniques to protect our systems from intrusion by unauthorised individuals, and we regularly upgrade our security as better methods become available.</p><p >Our datacentres and those of our partners utilise modern physical security measures to prevent unauthorised access to the facility. In addition, all personal data is stored in a secure location behind firewalls and other sophisticated security systems with limited (need-to-know) administrative access.</p><p >All our employees who have access to, or are associated with, the processing of personal data are contractually obligated to respect the confidentiality of your data and abide by the privacy standards we have established.</p><p >Please be aware that no security measures are perfect or impenetrable. Therefore, although we use industry standard practices to protect your privacy, we cannot (and do not) guarantee the absolute security of personal data.</p><p >The Website or App may offer chat rooms, forums, message boards, or news groups to users. It is important to remember that any information disclosed in these areas becomes public information. Accordingly, as with any public forum, you should exercise extreme caution when deciding whether to disclose your personal information.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC4'>10 - DOES THIS PRIVACY NOTICE APPLY TO OTHER WEBSITES?</span></h2>
        </div>		<div>
    	<p >No. Our Website (xe.com) and App may contain links to other Internet websites. By clicking on a third party advertising banner or certain other links, you will be redirected to such third party websites.</p><p >We are not responsible for the privacy policies of other websites or services. You should make sure that you read and understand any applicable third-party privacy policies, and you should direct any questions or concerns to the relevant third party administrators or webmasters prior to providing any personal data.</p><p >We may permit third parties to offer subscription or registration-based services promoted through our own Services. In some instances, these other services may be co-branded or use XE&#039;s trademarks under license; however, other&rsquo;s services have their own respective privacy policies. For the avoidance of doubt, &quot;XE Money Transfer&quot; is branded using XE&#039;s trade-marks, but is a service operated by our affiliate company, HiFX, and consequently &quot;XE Money Transfer&quot; is subject to HiFX&#039;s privacy policy.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC5'>11 - DIRECT MARKETING BY XE OR XE COMPANIES </span></h2>
        </div>		<div>
    	<p >We or an XE Company may sometimes contact you (by email, SMS text, letter or phone) in order to provide targeted marketing about our Services or the services of another XE Company or Euronet Group. Such marketing communications will only be sent to you if you gave your consent (when you registered for our Services or at another point) and you have not withdrawn such consent or if there is another basis to send such communications to you.</p><p >All marketing e-mails you receive from us will include specific instructions on how to unsubscribe and you may unsubscribe at any time or alternatively you can unsubscribe from marketing at any time by contacting us in writing by email at privacy@xe.com or by post to XE Privacy Office, 1145 Nicholson Rd, Suite 200, Newmarket ON, L3Y 9C3 Canada.</p><p >You should note that we are opposed to third-party spam mail activities and do not participate in such mailings, nor do we release or authorise the use of customer personal data to third parties for such purposes.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC6'>12 - PROFILING</span></h2>
        </div>		<div>
    	<p >Through automated processes we may create individual profiles for customers based on a combination of Indirectly Identifiable Personal Data and other information gathered through our customer&rsquo;s interaction with our Services.  We may use such profiles to better understand the ways in which you use our Services.  In addition, we may send personalised communications to you based on a profile (including pricing offers in relation to the Services or the services and products of Xe Companies which may include Xe Corporation, Inc.), if we have a basis to send such communications in accordance with this Privacy Notice (see Section 11 above).</p>You have the right not to be subject to profiling, and you can exercise this right by contacting us in writing at <a href='mailto:transfers.eu@xe.com'>transfers.eu@xe.com</a> or <a href='mailto:dpo@euronetworldwide.com'>dpo@euronetworldwide.com</a>.
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC1'>13 - WHAT ARE MY DATA PROTECTION RIGHTS?</span></h2>
        </div>		<div>
    	<p >In certain circumstances (for example, if you are a &ldquo;data subject&rdquo; in the EEA), and subject always to verification of your identity, you may request access to and have the opportunity to update and amend your personal data. You may also exercise any other rights you enjoy under applicable data protection laws.</p><p >Data subjects in the EEA have the right to:</p>			
			<ul class="faqList-cookie">
				<li>Request access to any personal data we hold about them (&ldquo;Subject Access Request&rdquo;) as well as related data, including the purposes for processing the personal data, the recipients or categories of recipients with whom the personal data has been shared, where possible, the period for which the personal data will be stored, the source of the personal data, and the existence of any automated decision making;</li><li>Obtain without undue delay the rectification of any inaccurate personal data we hold about them;.</li><li>Request that personal data held about them is deleted provided the personal data is not required by us, an XE Company or the Euronet Group for compliance with a legal obligation under applicable law or for the establishment, exercise or defence of a legal claim;</li><li>Under certain circumstances, prevent or restrict processing of their personal data, except to the extent processing is required for the establishment, exercise or defence of legal claims;</li><li>Under certain circumstances, request transfer of personal data directly to a third party where this is technically feasible.</li>
			</ul><p >Also, where you believe that we have not complied with our obligations under this Privacy Notice or the applicable law, you may have the right to make a complaint to a relevant Data Protection Authority or through the courts. The Canadian Data Protection Authority is the Office of the Privacy Commissioner of Canada (<a href='https://www.priv.gc.ca/en/' target="_blank">https://www.priv.gc.ca/en/</a>) and the country in which you are located is likely to have a Data Protection Authority (such as the Information Commissioner’s Office in the United Kingdom).</p><p >Although not required, we would encourage you to let us know about any complaint you might have and we will respond in line with our complaints procedure set out in section 14 of this Privacy Notice.</p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC2'>14 - PRIVACY COMPLAINTS PROCEDURE</span></h2>
        </div>		<div>
    	<p >Where you believe that we have not complied with our obligations under this Privacy Notice, or the applicable law, you may have the right to make a complaint to a relevant Data Protection Authority or through the courts. Although not required, we would encourage you to let us know about any complaint you might have and XE will respond in line with our complaints procedure &ndash; our contact details are set out in section 15 below. </p><p >We want to deal with your concerns fairly, effectively and promptly. However, some complaints are more complex than others and may take some time to investigate. </p>			
			<ul class="faqList-cookie">
				<li>We will acknowledge your complaint promptly after receiving it</li><li>We will keep you informed throughout any investigation</li>
			</ul><p >In order to assist in the speedy resolution of any complaint you may have, it&rsquo;s important that we understand your complaint fully.  Sometimes this means we may ask you to address your concerns to us in writing. This can be either by email or post to the addresses in section 15 below. We have established internal procedures for investigating any complaint, which may also involve experienced members of staff from XE Companies (including HiFX) considering or investigation the complaint. Where appropriate, the complaint will be dealt with by someone who was not directly involved in the matter which is the subject of your complaint. The member of staff will either have authority to settle your complaint or will have ready access to someone who has the authority. Our response will fully address the subject matter of your complaint and, if appropriate, will offer redress. If you phone us during our investigation and the member of staff handling your complaint is not available, then another member of our team will try to assist you.</p><p >Unless applicable data protection laws require responses within shorter timescales, we will try to resolve any privacy complaints you have within 15 business days of receiving your complaint and in exceptional circumstances, within 35 business days (and we will let you know if this is the case).</p><p >As noted above, if you are not satisfied with our reply/outcome, or otherwise with the handling of the complaint, you may have the right to lodge a claim before a relevant Data Protection Authority or the courts. </p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>		<div id='' class='relatedLinks'>
        	<h2 class='relatedLinksTitle'><span class='relatedLinksTitleBackground tClrC3'>15 - CONTACT US</span></h2>
        </div>		<div>
    	<p >If you have any questions or concerns about this Privacy Notice or our data practices, please contact us in writing by email at privacy@xe.com or by post to XE Privacy Office, 1145 Nicholson Rd, Suite 200, Newmarket, ON, L3Y 9C3 Canada. </p>
	</div>	<div class='relatedLinksList'><a class='backToTop' href='#'>Top &#9650;</a></div>
			
			
</div><div id='adProbx' >			<div class="adProAd" id="ros_bs1">
			    		<script type="text/javascript">
			
			enqueueAd('ros_bs1');
		</script>
		<noscript>
			<div class='center'><a href='https://www.xe.com/sv/xemoneytransfer/?WT.seg_1=XTR-180x150-XROSINT-XIN-XX-V01-130301'><img alt='Send Cheap Money Transfers' src='https://www.xe.com/themes/xe/images/xtr-180x150-cheapmoneytransfers-piggy.jpg' border='0'/></a></div>
		</noscript>
			</div>			<div class="adProAd" id="ros_bs2">
			    		<script type="text/javascript">
			
			enqueueAd('ros_bs2');
		</script>
		<noscript>
			<div class='center'><a href='https://www.xe.com/sv/xecurrencydata/?WT.seg_1=XDF-180x150-XROSINT-XIN-XX-V01-130301'><img alt='Get currency rates for your business' src='https://www.xe.com/themes/xe/images/xdf-180x150-currencyrates-symbols-api.png' border='0'/></a></div>
		</noscript>
			</div>			<div class="adProAdend" id="ros_bs3">
			    		<script type="text/javascript">
			
			enqueueAd('ros_bs3');
		</script>
		<noscript>
			<div class='center'><a href='https://www.xe.com/sv/apps/?WT.seg_1=XMA-180x150-XROSINT-XIN-XX-privacy.php_V01-130301'><img alt='Download our Free XE Currency App' src='https://www.xe.com/themes/xe/images/xmx-180x150-mobilexecurrencyapps.gif' border='0'/></a></div>
		</noscript>
			</div></div>                </div>
                <div class="sidebar" id="contentR">
                    <div id="ad">
	<div class="adTab">
			AD
	</div>   
	<div class="adBox" id="sv_rs">	
				<script type="text/javascript">
						e9AdSlots['sv_rs']['mobileAd'] = "<a href='https://www.xe.com/sv/xemoneytransfer/?WT.seg_1=XTR-300x50-XROSINT-ADL-XX-V01-130901'><img class='center' alt='Ad' src='https://www.xe.com/themes/xe/images/300x250_xemoneytransfer.png' border='0'/></a>";
			e9AdSlots['sv_rs']['desktopAd'] = "<a href='https://www.xe.com/sv/xemoneytransfer/?WT.seg_1=XTR-300x250-XROSINT-ADL-XX-V01-130901'><img alt='Ad' src='https://www.xe.com/themes/xe/images/300x250_xemoneytransfer.png' border='0'/></a>";
			e9AdSlots['sv_rs']['mobileThreshold'] = 850;			
			enqueueAd('sv_rs');
		</script>
		<noscript>
			<div class='center'><a href='https://www.xe.com/sv/xemoneytransfer/?WT.seg_1=XTR-300x250-XROSINT-ADL-XX-V01-130901'><img alt='Ad' src='https://www.xe.com/themes/xe/images/300x250_xemoneytransfer.png' border='0'/></a></div>
		</noscript>	</div>
</div><div class='module clearfix '>
			<h2>Live Currency Rates</h2>
			
									<table id="crLive" class="liveCurrencyRates" width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
		    	<th width="45%">Currency</th>
		        <th width="45%">Rate</th>
		        <th width="10%"></th>
			</tr>
			<tr><td><a href='https://www.xe.com/currencycharts/?from=EUR&amp;to=USD'>EUR / USD</a></td><td>1,12029</td><td class='arrow dwn'>&#9660;</td></tr><tr><td><a href='https://www.xe.com/currencycharts/?from=GBP&amp;to=EUR'>GBP / EUR</a></td><td>1,16543</td><td class='arrow up'>&#9650;</td></tr><tr><td><a href='https://www.xe.com/currencycharts/?from=USD&amp;to=JPY'>USD / JPY</a></td><td>111,383</td><td class='arrow dwn'>&#9660;</td></tr><tr><td><a href='https://www.xe.com/currencycharts/?from=GBP&amp;to=USD'>GBP / USD</a></td><td>1,30562</td><td class='arrow up'>&#9650;</td></tr><tr><td><a href='https://www.xe.com/currencycharts/?from=USD&amp;to=CHF'>USD / CHF</a></td><td>1,01712</td><td class='arrow dwn'>&#9660;</td></tr><tr><td><a href='https://www.xe.com/currencycharts/?from=USD&amp;to=CAD'>USD / CAD</a></td><td>1,34373</td><td class='arrow dwn'>&#9660;</td></tr><tr><td><a href='https://www.xe.com/currencycharts/?from=EUR&amp;to=JPY'>EUR / JPY</a></td><td>124,782</td><td class='arrow dwn'>&#9660;</td></tr><tr><td><a href='https://www.xe.com/currencycharts/?from=AUD&amp;to=USD'>AUD / USD</a></td><td>0,70188</td><td class='arrow up'>&#9650;</td></tr>
		</table>
			
			
</div><div class='module clearfix '>
			<h2>Popular Currency Profiles</h2>
			
							
	<ul id="popCurr">
		<li><div class='flagBoxPopular flagSprite fUSD'></div><a href='https://www.xe.com/sv/currency/usd-us-dollar'>USD - USA-dollar</a></li><li><div class='flagBoxPopular flagSprite fEUR'></div><a href='https://www.xe.com/sv/currency/eur-euro'>EUR - Euro</a></li><li><div class='flagBoxPopular flagSprite fGBP'></div><a href='https://www.xe.com/sv/currency/gbp-british-pound'>GBP - Brittiskt pund</a></li><li><div class='flagBoxPopular flagSprite fINR'></div><a href='https://www.xe.com/sv/currency/inr-indian-rupee'>INR - Indisk rupie</a></li><li><div class='flagBoxPopular flagSprite fAUD'></div><a href='https://www.xe.com/sv/currency/aud-australian-dollar'>AUD - Australisk dollar</a></li><li><div class='flagBoxPopular flagSprite fCAD'></div><a href='https://www.xe.com/sv/currency/cad-canadian-dollar'>CAD - Kanadensisk dollar</a></li><li><div class='flagBoxPopular flagSprite fSGD'></div><a href='https://www.xe.com/sv/currency/sgd-singapore-dollar'>SGD - Singaporiansk dollar</a></li><li><div class='flagBoxPopular flagSprite fCHF'></div><a href='https://www.xe.com/sv/currency/chf-swiss-franc'>CHF - Schweiziska franc</a></li><li><div class='flagBoxPopular flagSprite fMYR'></div><a href='https://www.xe.com/sv/currency/myr-malaysian-ringgit'>MYR - Malaysisk ringgit</a></li><li><div class='flagBoxPopular flagSprite fJPY'></div><a href='https://www.xe.com/sv/currency/jpy-japanese-yen'>JPY - Japansk yen</a></li><li><div class='flagBoxPopular flagSprite fCNY'></div><a href='https://www.xe.com/sv/currency/cny-chinese-yuan-renminbi'>CNY - Kinesisk yuan renminbi</a></li>	</ul>


			
			<div class=' modFtr'><span class='aFtrRight'><a href='/currency/'>More currencies</a></span></div>
</div><div class='module clearfix '>
			<h2>Central Bank Rates</h2>
			
							<ul class='bankRates'><li><a href='https://www.xe.com/sv/currency/jpy-japanese-yen'><span class='centralBankPopularCurrency'>JPY</span></a> −0,10 %</li>
<li><a href='https://www.xe.com/sv/currency/chf-swiss-franc'><span class='centralBankPopularCurrency'>CHF</span></a> −0,75 %</li>
<li><a href='https://www.xe.com/sv/currency/eur-euro'><span class='centralBankPopularCurrency'>EUR</span></a> 0,00 %</li>
<li><a href='https://www.xe.com/sv/currency/usd-us-dollar'><span class='centralBankPopularCurrency'>USD</span></a> 2,25 %</li>
<li><a href='https://www.xe.com/sv/currency/cad-canadian-dollar'><span class='centralBankPopularCurrency'>CAD</span></a> 1,75 %</li>
<li><a href='https://www.xe.com/sv/currency/aud-australian-dollar'><span class='centralBankPopularCurrency'>AUD</span></a> 1,50 %</li>
<li><a href='https://www.xe.com/sv/currency/nzd-new-zealand-dollar'><span class='centralBankPopularCurrency'>NZD</span></a> 1,75 %</li>
<li><a href='https://www.xe.com/sv/currency/gbp-british-pound'><span class='centralBankPopularCurrency'>GBP</span></a> 0,75 %</li>
</ul>
			
			
</div>                </div>
            </div>
                    </main>

        <div id="footerContainer" class="nonReactPageFix-footer"></div>
<script>window.pageProps = {"isMobile":false,"isTablet":false,"locale":"sv","surveyUrl":"https:\/\/www.surveymonkey.com\/r\/NL7HN8Q","forceSurveyOpenInNewTab":false,"breadcrumbs":[{"text":"Home","url":"\/"},{"text":"Privacy Policy","url":null}]};</script><script>
    const unspeedyUserAgents = /(prerender|iPhone|iPad)/i;
    const SC_DISABLE_SPEEDY = unspeedyUserAgents.test(navigator.userAgent);
</script>            <script>
                var isModernBrowser = (
                    !!window.Promise &&
                    !!Array.prototype.find &&
                    !!window.Set &&
                    !!Number.parseInt &&
                    !!Number.parseFloat
                );

                if (!isModernBrowser) {
                    var scriptElement = document.createElement('script');

                    scriptElement.async = false;
                    scriptElement.src = "https://www.xe.com/themes/xe/js/react/polyfills.856b617e57d74fe91c2c.min.js";
                    document.head.appendChild(scriptElement);
                }
            </script><script src="https://www.xe.com/themes/xe/js/react/commons.de02768974fc549469c1.min.js"></script><script src="https://www.xe.com/themes/xe/js/react/nav.a57a091e9ce9bfcf7266.min.js"></script>    </body>
</html>
