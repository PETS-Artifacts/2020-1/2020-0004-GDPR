<!--
Copyright (c) - USMLEForum.com
 Any violations of Copyright is strictly prohibited and will be prosecuted to the maximum extent.
 Modification, reproduction, or distribution of this source code in any form is NOT allowed.
-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Pragma" content="no-cache">
<title>USMLE Forum - Terms of Use & Disclaimer</title>
<link rel="stylesheet" type="text/css" href="../shared/UFMain.css">
</head>
<body topmargin="2" leftmargin="0">
<SCRIPT type=text/javascript >
DomainName = document.domain;
if (DomainName != "www.usmleforum.com")
{
	window.location = "http://www.usmleforum.com";
}
</SCRIPT>
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="92">
	<tr>
		<td width="30%">&nbsp;</td>
		<td width="70%" rowspan="3" valign="top" align="right">	<!-- Google Ad Start --->
<script type="text/javascript"><!--
google_ad_client = "ca-pub-1595373265720144";
/* 728x90 */
google_ad_slot = "7007604136";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
	<!-- Google Ad End -->
</td>
	</tr>
	<tr>
		<td width="30%" valign="top">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<a href="http://www.usmleforum.com/" target="_top" title="usmleforum.com"><img border="0" src="/images/usmleforum.gif" width="162" height="22" alt="USMLE forum"></a></td>
	</tr>
	<tr>
		<td width="30%">&nbsp;</td>
	</tr>
</table>
</center>
</div>
<div align="center">
  <center>
  <table border="1" cellpadding="0" cellspacing="0" width="500" class="Table2" bordercolor="#008000">
  <tr>
    <td width="100%">&nbsp;</td>
  </tr>
    <tr>
      <td width="100%">
<div align="center">
  <center>
<table border="0" cellpadding="0" width="100%" cellspacing="0">
  <tr>
    <td class="Title2" width="100%" align="center">USMLEForum.com Privacy Statement</td>
  </tr>
  <tr>
    <td width="100%">&nbsp;</td>
  </tr>
  <tr>
    <td class="FormText2" width="100%">
      <blockquote>     	
	<br>We are committed to protect your privacy.  You can visit the site without giving any information to us.  But, when you post or reply
    to messages, we need your information.
    <br><br>Any visitor can post message, and the database will be updated
    automatically and immediately. We have no control over any matter or data posted on these pages. Some of the links in this site lead to external sites where we have no control over those external sites.
    <br><br>We will store all your information like name, e-mail address, Network and/or IP addresse in our database.  We will use Network/IP addresses to identify in case of any misuse of the site. We will ask your name and e-mail address only to identify you as a unique user.
	<br><br>When you post or reply a message, if you have entered your e-mail address, that e-mail address along with your name will appear on the pages.  We are against to those who will collect your information from these messages.  But, we have no control or not responsible for any type of collection by such type of scams.
	<br><br>We cannot protect your name and e-mail addresses from
    spammers.  We strongly suggest you not to post email address in the forum. If you have any constraints about any posted message under your name and e-mail address,
    contact us through info@usmleforum.com and we will take appropriate action against it.  But, once in a while, we will contact you to announce important messages.
	<br><br>When you post any message or reply we will try our best to store them in a standard format.  But due to
    our technical limitations or other problems, it could be manipulated.  If you have noticed any problems like that, please inform us immediately and we will take
    necessary action.
	<br><br>The site uses cookies only to identify your name and email address,
    and for your easiness not to type your name and email address again and
    again.&nbsp; The site works without cookies also.&nbsp; The site has links and advertisements from third party.  They may use cookies and we do not have any control over them.
	<br><br>If requested by law enforcement or USMLE related organization, or our advertisers with DMCA complaints etc we will have to provide them your data including your email address, IP address, and activity.
	<br><br>Once the message posted in USMLEForum.com, that is final.  No modifications, deletions allowed.  If we find any bad messages, we may have to delete them.  Bad messages includes selling copyright material, vulgar/threatning/unlawful language.
      </blockquote>
    </td>
  </tr>
</table>
  </center>
</div>
</td>
    </tr>
  </table>
  </center>
</div>
<p>&nbsp;</p>
<p>
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td width="48%" align="right">        <!-- Google Ad Start -->
<script type="text/javascript"><!--
google_ad_client = "ca-pub-1595373265720144";
/* 300x250 */
google_ad_slot = "2295977747";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
        <!-- Google Ad End -->
</td>
	<td width="4%">&nbsp;</td><td width="48%">        <!-- Google Ad Start -->
<script type="text/javascript"><!--
google_ad_client = "ca-pub-1595373265720144";
/* 300x250 */
google_ad_slot = "2295977747";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
        <!-- Google Ad End -->
</td>
</tr>
<tr>
	<td align="center">&nbsp;</td>
</tr>
</table>
</center>
</div>
</p>
<div align="center">
<table border="1" cellpadding="0" cellspacing="0" width="450" style="border: 1 solid #808000">
<tr>
	<td>
	<FORM method=GET action='http://www.google.com/custom'  target="google_window">
	<div align="center">
	<center>
	<TABLE bgcolor='#FFFFFF'>
	<tr>
		<td nowrap='nowrap' valign='top' align='center' height='32'>
		<A HREF='http://www.google.com/'>
		<font face="Verdana" size="2" color="#0000FF">
		<IMG SRC='http://www.google.com/logos/Logo_25wht.gif' border='0' ALT='Google'></font></A></td>
	</center>
	<td>
		<font face="Verdana" size="2" color="#0000FF">
		<input type=hidden name=domains value='usmleforum.com'><INPUT TYPE=text name=q size=31 maxlength=255 value=''>
		<INPUT type=submit name=sa VALUE='Search'>
		</font>
	</td>
</tr>
<center>
<tr>
	<td>&nbsp;</td>
	<td>
		<font face="Verdana" size="2" color="#0000FF">
		<input type=radio name=sitesearch value=''>  Web
		<input type=radio name=sitesearch value='usmleforum.com' checked>USMLEforum.com
		<input type=hidden name=client value='pub-1595373265720144'>
		<input type=hidden name=forid value='1'>
		<input type=hidden name=ie value='ISO-8859-1'>
		<input type=hidden name=oe value='ISO-8859-1'>
		<input type=hidden name=cof value='GALT:#0066CC;GL:1;DIV:#999999;VLC:336633;AH:center;BGC:FFFFFF;LBGC:FF9900;ALC:0066CC;LC:0066CC;T:000000;GFNT:666666;GIMP:666666;LH:22;LW:162;L:http://www.usmleforum.com/images/usmleforum.gif;S:http://;FORID:1;'>
		<input type=hidden name=hl value='en'>
		</font>
	</td>
</tr>
</TABLE>
</center>
</div>
</FORM>
</td>
</tr>
</table>
</div>
<div align="center">
<table border="0" cellpadding="0" cellspacing="0" width="450">
<tr>
	<td align="center">&nbsp;</td>
</tr>
</table>
</div>
<p>
<div align="center">
<center>
<table border="1" width="720px" align="center" bordercolor="green">
	<tr bordercolor="#FF0000">

	<td width="120" align="center" height="25"><a href="http://www.usmleforum.com/forum/index.php?forum=1" target="_top" class="Links3">Step 1</a></td>
	<td width="120" align="center" height="25"><a href="http://www.usmleforum.com/forum/index.php?forum=2" target="_top" class="Links3">Step 2 CK</a></td>
	<td width="120" align="center" height="25"><a href="http://www.usmleforum.com/forum/index.php?forum=3" target="_top" class="Links3">Step 2 CS</a></td>
	<td width="120" align="center" height="25"><a href="http://www.usmleforum.com/forum/index.php?forum=4" target="_top" class="Links3">Matching & Residency</a></td>
	<td width="120" align="center" height="25"><a href="http://www.usmleforum.com/forum/index.php?forum=5" target="_top" class="Links3">Step 3</a></td>
	<td width="120" align="center" height="25"><a href="http://www.usmleforum.com/forum/index.php?forum=6" target="_top" class="Links3">Classifieds</a></td>	</tr>
	<tr>
		<td width="100%" colspan="6" align="center">
		<table width="100%">
		<tr>
<td width="33%" align="center" height="20"><a href="http://www.usmleforum.com/profiles" target="_top" class="Links3">Login</a></td><td width="33%" align="center" height="20"><a href=http://www.usmleforum.com/links/ target="_top" class="Links3">USMLE Links</a></td><td width="33%" align="center" height="20"><a href="http://www.usmleforum.com/" target="_top" class="Links3">Home</a></td>		</tr>
	</table>
	</td>
</tr>
</table>
</center>
</div>
</p>
<p>&nbsp;</p>
</body>
</html>
