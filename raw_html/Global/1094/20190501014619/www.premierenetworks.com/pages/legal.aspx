<!DOCTYPE html>
<html>
<head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" /><title>
	
	Legal

</title><meta name="description" /><link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" /><link href="/Content/css/app.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.js"></script>
     
    <script src="https://use.typekit.net/niq1tfu.js"></script>
	<script>try{Typekit.load({ async: true });}catch(e){}</script>
    
    <!--[if lt IE 9]>
        <script src="/Content/js/respond-1.1.0.min.js"></script>
        <script src="/Content/js/html5shiv.js"></script>
        <script src="/Content/js/html5element.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>

	<script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script src="/Content/js/global.js"></script>
	<script src="/Content/js/detectizr.min.js"></script>
    <script src="/Content/js/vendor.min.js"></script>	
    <script src="/Content/js/app.min.js"></script>	


	
	
	
			
			
		
	
	
<link href="/Content/css/premiere.css" rel="stylesheet" /><link rel="shortcut icon" href="/Content/img/favicon.ico" type="image/vnd.microsoft.icon" /></head>
<body class="iheart">
<form name="aspnetForm" method="post" action="/pages/legal.aspx" id="aspnetForm">
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="0x37EE4BD4E8044A58717DEE82ECC82B78006DC8E69F54F67C9D462B8E4A6C61369128091246701E92918D3AF9A225A96A2DB041BA8309454C3E8924A0112831EF,01 May 2019 01:46:19 -0000" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUINDExNzIwMzYPZBYCZg9kFgICAQ9kFgQCAQ9kFgQCAg9kFgJmD2QWAgIBDxYCHhNQcmV2aW91c0NvbnRyb2xNb2RlCymIAU1pY3Jvc29mdC5TaGFyZVBvaW50LldlYkNvbnRyb2xzLlNQQ29udHJvbE1vZGUsIE1pY3Jvc29mdC5TaGFyZVBvaW50LCBWZXJzaW9uPTE0LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPTcxZTliY2UxMTFlOTQyOWMBZAIKD2QWAgIHDxYCHwALKwQBZAIDD2QWBAIDD2QWAgIDD2QWAgIDDxYCHwALKwQBZAIFD2QWBAIBD2QWAgIDD2QWAgIBDxYCHwALKwQBZAIJD2QWAgIBD2QWAmYPZBYCAgEPD2QWBh4FY2xhc3MFIm1zLXNidGFibGUgbXMtc2J0YWJsZS1leCBzNC1zZWFyY2geC2NlbGxwYWRkaW5nBQEwHgtjZWxsc3BhY2luZwUBMGRk8I+pOvOEUZjaFepJI+FEZSa0W2Q=" />


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = false;var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002f", webLanguage: 1033, currentLanguage: 1033, webUIVersion:4,pageListId:"{dc39fd9a-eda2-4316-83cc-8a070d43121f}",pageItemId:221, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};//]]>
</script>
<script type="text/javascript" src="/_layouts/1033/init.js?rev=lEi61hsCxcBAfvfQNZA%2FsQ%3D%3D"></script>
<script type="text/javascript">
//<![CDATA[
document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};//]]>
</script>

<script src="/ScriptResource.axd?d=XF9h4AZdUYHN9meLY0fs3IBwaHRnW6veOJircZ6H7TFPM9grprsfWIJNdqZZn4A7VII6nBHeeZT2szBKJZQPVNXKSJt3ptNimKytkYzT4EyKRdFj0s82zfBr4G1IWgZyL_u9WfPXhWqiWuahkLb4Yb3d6yQ1&amp;t=3f4a792d" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="53C11AB5" />
			 
	<div id="viewport" class="sb-site-container">
      
      <!-- Navigation -->
            <nav id="scrolling-nav" class="navbar navbar-default" role="navigation">	
    <div class="container">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-xs-12 col-lg-10 navbar-wrap">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle sb-toggle-left" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/Default.aspx"><img src="/Content/img/pn-brand-color.png" alt="Premiere Networks"></a>
                </div>

                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <!-- nav wrapper for alignment -->
                    <div class="pull-right">
                    <ul class="nav navbar-nav nav-spy">
                        <li><a href="/Default.aspx#s2">Programming</a></li>
                        <li><a href="/Default.aspx#s3">Advertising</a></li>
                        <li><a href="/Default.aspx#s4">About Us</a></li>
                    </ul>

                    <!-- navigation engineering link component -->
                    <div class="nav-engineering-wrapper">
                        <a href="http://engineering.premiereradio.com" target="_blank">Engineering</a>
                    </div>
                    <!-- /navigation engineering link component -->

                    </div>
                    <!-- /nav wrapper for alignment -->
                </div>

            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
</nav>




                        
		

	<section id="blog-main-wrapper" class="container blog-container">
		
	
	
			<div class="article article-body">
				
				<div class="article-content">
					<div id="ctl00_PlaceHolderMain_ctl01_ctl01_label" style='display:none'>Page Content</div><div id="ctl00_PlaceHolderMain_ctl01_ctl01__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_ctl01_ctl01_label"><a name="termsofuse"></a>
<h1>Legal</h1>
<ul>
    <li><a href="#termsofuse">Terms of Use</a></li>
    <li><a href="#privacystatement">Privacy Statement</a></li>
    <li><a href="/Pages/legal-spanish.aspx">View this page in Spanish</a></li>
</ul>
<h1 id="article-title" style="text-transform:capitalize !important">Terms of Use </h1>
<h5 class="updated-date">This Agreement was last modified on February 26, 2019.</h5>
<p> </p>
<blockquote>
    <ol class="jump-to-list">
        <li><a href="#mobile-devices">Mobile devices</a></li>
        <li><a href="#changes">Changes to this agreement</a></li>
        <li><a href="#privacy">Privacy and protection of personal information</a></li>
        <li><a href="#accounts">Accounts, Security</a></li>
        <li><a href="#code-of-conduct">User code of conduct</a></li>
        <li><a href="#fees">Fees</a></li>
        <li><a href="#warranties">Disclaimer of warranties</a></li>
        <li><a href="#exceptions">Exceptions</a></li>
        <li><a href="#liability">Limitations on liability</a></li>
        <li><a href="#indemnification">Indemnification</a></li>
        <li><a href="#modification-termination">Modification/Termination by iHeartMedia, Inc.,</a></li>
        <li><a href="#links">Links</a></li>
        <li><a href="#software">Software and downloads available through this site</a></li>
        <li><a href="#export-controls">International use/U.S. export controls</a></li>
        <li><a href="#third-party">Third-party merchants</a></li>
        <li><a href="#advertisements">Advertisements, sponsorships, co-promotions and other partnerships</a></li>
        <li><a href="#events">Events</a></li>
        <li><a href="#interactive">Interactive services and user materials</a></li>
        <li><a href="#subscription">Subscription services</a></li>
        <li><a href="#premium-services">Premium services</a></li>
        <li><a href="#contests">Contests/sweepstakes</a></li>
        <li><a href="#general">General</a></li>
        <li><a href="#copyright">Copyright and trademark notice</a></li>
    </ol>
</blockquote>
<div class="article-body">
    <div class="article-body">
        <article>
            <p>
                This site or application is owned or managed by iHeartMedia, Inc. (“iHeartMedia”) and is part of the iHeartMedia family of companies,
                which includes other quality entertainment brands such as broadcast and Internet radio stations, Clear Channel Outdoor, iHeartMedia
                Radio, and the Premiere Radio Networks, each of which operates or manages one or more websites or applications (each a “iHeartMedia
                Site,” and collectively the “iHeartMedia Sites”).
            </p>
            <p>
                iHeartMedia provides this iHeartMedia Site and related services for your personal non-commercial use only and subject to your compliance
                with this Terms of Use Agreement (the “Agreement”). Please read this Agreement carefully before using this iHeartMedia Sites. Your use
                of this iHeartMedia Sites constitutes your acceptance to be bound by this Agreement without limitation, qualification or change. If at
                any time you do not accept all the terms and conditions of this Agreement, you must immediately discontinue use of this iHeartMedia
                Site. This Agreement sets forth iHeartMedia’s policies with respect to its operation of the iHeartMedia Sites. Other policies govern
                iHeartMedia’s non-Internet operations.
            </p>
            <p>
                Certain products or services offered by this and/or other iHeartMedia Sites (each a “iHeartMedia Internet Service,” and collectively
                “iHeartMedia Internet Services”), and certain areas within this and/or other iHeartMedia Sites may be governed by additional terms
                (“Additional Terms”) presented in conjunction with those products or services. You must agree to those Additional Terms before using
                those areas or iHeartMedia Internet Services. The Additional Terms and this Agreement, taken together, shall apply to your use of those
                areas or iHeartMedia Internet Services. In the event of an irreconcilable inconsistency between the Additional Terms and this Agreement,
                the Additional Terms shall control.
            </p>
            <p>
                YOU MAY NOT USE ANY IHEARTMEDIA SITE FOR ANY PURPOSE THAT IS UNLAWFUL OR PROHIBITED BY THIS AGREEMENT AND/OR ANY APPLICABLE ADDITIONAL
                TERMS. YOUR ACCESS TO ANY IHEARTMEDIA SITE MAY BE TERMINATED IMMEDIATELY IN IHEARTMEDIA’S SOLE DISCRETION, WITH OR WITHOUT NOTICE, IF
                YOU FAIL TO COMPLY WITH ANY PROVISIONS OF THIS AGREEMENT AND/OR ANY APPLICABLE ADDITIONAL TERMS, OR FOR ANY OTHER REASON, OR NO REASON.
            </p>
            <p>
                By using this iHeartMedia Site, you are representing and warranting that: (a) you are a legal resident of the United States; (b) you are
                at or above the legal age of majority in your jurisdiction of residence; (c) you own or have sufficient authorization to use the
                computer, mobile device, technology or other device you use to access this iHeartMedia Site (collectively, “Device”); and (d) you will
                access and use this iHeartMedia Site in accordance with this Agreement.
            </p>
            <p>
                Some parts of this iHeartMedia Site may contain adult content intended for people who are at or above the legal age of majority in their
                jurisdiction of residence. By viewing this adult content, you are representing that you are at or above such legal age of majority and
                that the content is acceptable to you. Filtering software is commercially available which can be used to exclude content that is not
                acceptable to you. This software may prevent the display of all or portions of the iHeartMedia Site content.
            </p>
        </article>
        <div class="jump-to-container">
            <article>
                <a class="anchor" name="mobile-devices"></a>
                <h4 class="list-title">
                    Mobile devices
                </h4>
                <div class="body-content">
                    <p>
                        If permitted or available through the applicable iHeartMedia Internet Service, to (a) upload content to this iHeartMedia Site
                        via your mobile device and/or tablet, (b) receive and reply to messages, or to access or make posts using text messaging, (c)
                        browse this iHeartMedia Site from your mobile device and/or (d) to access certain features through a mobile application you have
                        downloaded and installed on your mobile device (collectively the “Mobile Services”), you must have a mobile communications
                        subscription (or have the consent of the applicable subscriber) with a participating carrier or otherwise have access to a
                        mobile communications network for which iHeartMedia makes the iHeartMedia Internet Service available as well as any carrier
                        services necessary to download content, and pay any service fees associated with any such access (including text messaging
                        charges for each text message you send and receive on your mobile device). In addition, you must provide all equipment and
                        software necessary to connect to the iHeartMedia Internet Service, including, but not limited to, if this iHeartMedia Site
                        contains a mobile element, a mobile hand set or other mobile access device that is in working order and suitable for use in
                        connection with the iHeartMedia Internet Service and to use any part of that Service. You are responsible for ensuring that your
                        equipment and/or software does not disturb or interfere with iHeartMedia’s or this iHeartMedia Site’s operations or the
                        iHeartMedia Internet Service. Any equipment or software causing interference will be immediately disconnected from the
                        iHeartMedia Internet Service and iHeartMedia will have the right to immediately terminate this Agreement. If any upgrade in or
                        to the iHeartMedia Internet Service requires changes in your equipment or software (including the operating system for your
                        Device), you must effect these changes at your own expense. Unless explicitly stated otherwise, any new or additional features
                        that augment or enhance the current iHeartMedia Internet Service, including the release of new products and services, will be
                        subject to the terms and conditions of this Agreement. You agree to follow and comply with any applicable laws in your use of
                        the iHeartMedia Internet Service.
                    </p>
                </div>
            </article>
            <article>
                <h4 class="list-title">
                    <a class="anchor" name="changes"></a>
                    Changes to This Agreement
                </h4>
                <div class="body-content">
                    <p>
                        iHeartMedia reserves the right, in its sole discretion, to modify, alter, or otherwise change this Agreement and/or the
                        Additional Terms at any time. iHeartMedia will provide notice of such change on this iHeartMedia Site. Please review this
                        Agreement and/or Additional Terms periodically for changes. Your continued use of this iHeartMedia Site and/or iHeartMedia
                        Internet Service constitutes your acceptance and agreement to be bound by these changes without limitation, qualification or
                        change. If at any time you do not accept these changes, you must immediately discontinue use of this iHeartMedia Site and/or the
                        iHeartMedia Internet Service to which the changes may apply.
                    </p>
                </div>
            </article>
            <article>
                <h4 class="list-title">
                    <a class="anchor" name="privacy"></a>
                    Privacy and Protection of Personal Information
                </h4>
                <div class="body-content">
                    <p>iHeartMedia has developed a Privacy Statement in order to inform you of its practices with respect to the collection, use, disclosure and protection of personal information. You can find the Privacy Statement here which is incorporated into this Agreement, and by using this iHeartMedia Site you agree to the terms of the <a href="/privacy-statement">Privacy</a></p>
                </div>
            </article>
            <article>
                <a class="anchor" name="accounts"></a>
                <h4 class="list-title">
                    Accounts, Security, Passwords
                </h4>
                <div class="body-content">
                    <p>
                        If a particular iHeartMedia Site or iHeartMedia Internet Service requires you to open an account, you must complete the
                        specified registration process by providing us with current, complete, and accurate information as requested by the applicable
                        online registration form. It is your responsibility to maintain the currency, completeness, and accuracy of your registration
                        data and any loss caused by your failure to do so is your responsibility. After you have fully completed the registration form,
                        you may be asked to choose a password and a user name. It is entirely your responsibility to maintain the confidentiality of
                        your password and account. Additionally, you are entirely responsible for any and all activities that occur under your account.
                        You agree to notify iHeartMedia immediately of any unauthorized use of your account. iHeartMedia is not liable for any loss that
                        you may incur as a result of someone else using your password or account, either with or without your knowledge. You may cancel
                        your account by delivering notice in the manner provided in the Additional Terms governing the particular iHeartMedia Internet
                        Service.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="code-of-conduct"></a>
                <h4 class="list-title">
                    User Code of Conduct
                </h4>
                <div class="body-content">
                    <h5>
                        In accessing and using this iHeartMedia, Inc., Site and/or the iHeartMedia, Inc., Internet Services, you agree that you will
                        not:
                    </h5>
                    <ul>
                        <li>
                            Deliver any unsolicited advertisement, promotional materials, junk email, bulk email (also known as “spam”), chain letters,
                            surveys or contests, or solicit participation in any pyramid schemes (unless it is on a page that explicitly states that
                            such postings are allowed on that page).
                        </li>
                        <li>
                            Deliver any unlawful (according to local, state, federal, or international law or regulation) postings to or through this
                            iHeartMedia Site, or any postings which advocate illegal activity.
                        </li>
                        <li>
                            Deliver, or provide links to, any postings containing material that could be considered harmful, obscene, pornographic,
                            indecent, lewd, violent, abusive, profane, insulting, threatening, harassing, hateful or otherwise objectionable.
                        </li>
                        <li>
                            Deliver, or provide links to, any postings containing material that harasses, victimizes, degrades, or intimidates an
                            individual or group of individuals on the basis of religion, race, ethnicity, sexual orientation, gender, age, or
                            disability.
                        </li>
                        <li>Deliver, or provide links to, any postings containing defamatory, false or libelous material.</li>
                        <li>
                            Deliver any posting that infringes or violates any intellectual property or other right of any entity or person, including,
                            without limitation, copyrights, patents, trademarks, laws governing trade secrets, rights to privacy, or publicity.
                        </li>
                        <li>
                            Deliver any posting to that you do not have a right to make available under law or contractual or fiduciary relationships.
                        </li>
                        <li>
                            Impersonate another person or entity or falsely state or otherwise misrepresent your affiliation with a person or entity, or
                            adopt a false identity if the purpose of doing so is to mislead, deceive, or defraud another.
                        </li>
                        <li>Manipulate identifiers, including by forging headers, in order to disguise the origin of any posting that you deliver.</li>
                        <li>
                            Deliver any posting containing personal information, such as phone numbers, social security numbers, account numbers,
                            addresses or employer references.
                        </li>
                        <li>
                            Use this iHeartMedia service in any manner which could damage, disable, overburden, or impair or otherwise interfere with
                            the use of this iHeartMedia Site or other users' Devices, or cause damage, disruption or limit the functioning of any
                            software, hardware, or telecommunications equipment.
                        </li>
                        <li>
                            Attempt to gain unauthorized access to this iHeartMedia Site, any related website, other accounts, computer system, or
                            networks connected to this iHeartMedia, Inc., Site, through hacking, password mining, or any other means.
                        </li>
                        <li>
                            Obtain or attempt to obtain any materials or information through any means not intentionally made available through this
                            iHeartMedia Site, including harvesting or otherwise collecting information about others such as email addresses.
                        </li>
                    </ul>
                </div>
            </article>
            <article>
                <a class="anchor" name="fees"></a>
                <h4 class="list-title">
                    Fees
                </h4>
                <div class="body-content">
                    <p>
                        Except where otherwise provided, access to and use of this iHeartMedia Site and the iHeartMedia Internet Services offered
                        through it are currently available without charge. iHeartMedia reserves the right to charge a fee for access to or use of this
                        iHeartMedia Site, or any iHeartMedia Internet Service available on this iHeartMedia Site at any time in the future. Your access
                        to or use of this iHeartMedia Site before such time does not entitle you to use of this iHeartMedia Site without charge in the
                        future.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="warranties"></a>
                <h4 class="list-title">
                    Disclaimer of Warranties
                </h4>
                <div class="body-content">
                    <p>
                        YOUR USE OF, AND RELIANCE ON, ANY ADVICE OR INFORMATION OBTAINED FROM OR THROUGH THIS IHEARTMEDIA SITE AND/OR IHEARTMEDIA
                        INTERACTIVE SERVICE IS AT YOUR OWN RISK. ALL CONTENT, INCLUDING SOFTWARE, PRODUCTS, SERVICES, INFORMATION, TEXT AND RELATED
                        GRAPHICS CONTAINED WITHIN OR AVAILABLE THROUGH THIS IHEARTMEDIA SITE OR IHEARTMEDIA INTERACTIVE SERVICE ARE PROVIDED TO YOU ON
                        AN “AS IS,” “AS AVAILABLE” BASIS. IHEARTMEDIA MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, AS
                        TO THE OPERATION OF THIS IHEARTMEDIA SITE OR THE INFORMATION, CONTENT OR MATERIALS INCLUDED ON THIS IHEARTMEDIA SITE. TO THE
                        FULLEST EXTENT PERMISSIBLE PURSUANT TO APPLICABLE LAW, IHEARTMEDIA DISCLAIMS ALL REPRESENTATIONS AND WARRANTIES OF ANY KIND,
                        EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY OR SATISFACTORY QUALITY,
                        FITNESS FOR A PARTICULAR PURPOSE, WORKMANLIKE EFFORT, INFORMATIONAL CONTENT, TITLE, OR NON-INFRINGEMENT OF THE RIGHTS OF THIRD
                        PARTIES. IHEARTMEDIA DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS THAT THIS IHEARTMEDIA SITE WILL OPERATE ERROR-FREE OR
                        UNINTERRUPTED, THAT DEFECTS WILL BE CORRECTED, OR THAT THIS IHEARTMEDIA SITE AND/OR ITS SERVER WILL BE FREE OF VIRUSES AND/OR
                        OTHER HARMFUL COMPONENTS. IHEARTMEDIA DOES NOT WARRANT OR MAKE ANY REPRESENTATIONS REGARDING SUITABILITY, AVAILABILITY,
                        ACCURACY, RELIABILITY, COMPLETENESS, OR TIMELINESS OF ANY MATERIAL OF ANY KIND CONTAINED WITHIN THIS IHEARTMEDIA SITE FOR ANY
                        PURPOSE, INCLUDING SOFTWARE, PRODUCTS, SERVICES, INFORMATION, TEXT AND RELATED GRAPHICS CONTENT.
                    </p>
                    <p>
                        IHEARTMEDIA IS NOT RESPONSIBLE FOR ANY FAILURES CAUSED BY SERVER ERRORS, MISDIRECTED OR REDIRECTED TRANSMISSIONS, FAILED
                        INTERNET CONNECTIONS, INTERRUPTIONS IN THE TRANSMISSION OR RECEIPT OF TICKET ORDERS OR IHEARTMEDIA INTERACTIVE SERVICES, OR ANY
                        COMPUTER VIRUS OR OTHER TECHNICAL DEFECT, WHETHER HUMAN OR TECHNICAL IN NATURE.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="exceptions"></a>
                <h4 class="list-title">
                    Exceptions
                </h4>
                <div class="body-content">
                    <p>
                        SOME JURISDICTIONS DO NOT ALLOW THE DISCLAIMER, EXCLUSION OR LIMITATION OF CERTAIN WARRANTIES, LIABILITIES AND DAMAGES, SO SOME
                        OF THE ABOVE DISCLAIMERS, EXCLUSIONS AND LIMITATIONS MAY NOT APPLY TO YOU. IN SUCH JURISDICTIONS, IHEARTMEDIA’S LIABILITY WILL
                        BE LIMITED TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="liability"></a>
                <h4 class="list-title">
                    Limitations on Liability
                </h4>
                <div class="body-content">
                    <p>
                        In no event shall iHeartMedia, its subsidiaries, affiliates, distributors, suppliers, licensors, agents or others involved in
                        creating, sponsoring, promoting, or otherwise making available this iHeartMedia Site and its contents, be liable to any person
                        or entity whatsoever for any direct, indirect, incidental, special, compensatory, consequential, or punitive damages or any
                        damages whatsoever, including but not limited to: (i) loss of goodwill, profits, business interruption, data or other intangible
                        losses; (ii) your inability to use, unauthorized use of, performance or non-performance of this iHeartMedia Site; (iii)
                        unauthorized access to or tampering with your personal information or transmissions; (iv) the provision or failure to provide
                        any service; (v) errors or inaccuracies contained on this iHeartMedia Site or any information, software, products, services, and
                        related graphics obtained through this iHeartMedia Site; (vi) any transactions entered into through this iHeartMedia Site; (vii)
                        any property damage including damage to your Device or computer system caused by viruses or other harmful components, during or
                        on account of access to or use of this iHeartMedia Site or any site to which it provides hyperlinks; or (viii) damages otherwise
                        arising out of the use of this iHeartMedia Site and iHeartMedia Internet Services. The limitations of liability shall apply
                        regardless of the form of action, whether based on contract, tort, negligence, strict liability or otherwise, even if
                        iHeartMedia has been advised of the possibility of damage.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="indemnification"></a>
                <h4 class="list-title">
                    Indemnification
                </h4>
                <div class="body-content">
                    <p>
                        You agree to indemnify and hold harmless iHeartMedia, its subsidiaries, agents, distributors and affiliates, and their officers,
                        directors and employees from and against any and all claims, actions, demands, liabilities, costs and expenses, including,
                        without limitation, reasonable attorneys’ fees, resulting from your breach of any provision of this Agreement, the Additional
                        Terms, or any warranty you provide herein, or otherwise arising in any way out of your use of this iHeartMedia Site and any
                        related iHeartMedia Internet Service and/or software. You agree to cooperate fully with iHeartMedia in asserting any available
                        defenses in connection with a claim subject to indemnification by you under this Agreement.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="modification-termination"></a>
                <h4 class="list-title">
                    Modification/Termination by iHeartMedia, Inc.,
                </h4>
                <div class="body-content">
                    <p>
                        iHeartMedia reserves the right, in its sole discretion, to modify, suspend, or terminate this iHeartMedia Site and/or any
                        portion thereof, including any iHeartMedia Internet Service, and/or your account, password, or use of any iHeartMedia Internet
                        Service, or any portion thereof, at any time for any reason with or without notice to you.
                    </p>
                    <p>
                        Termination of your account for a iHeartMedia Internet Service removes your authorization to use the iHeartMedia Internet
                        Service. In the event of termination, you will still be bound by your obligations under this Agreement and any Additional Terms,
                        including the warranties made by you, and by the disclaimers and limitations of liability. Additionally, iHeartMedia shall not
                        be liable to you or any third party for any termination of your access to a iHeartMedia Internet Service.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="links"></a>
                <h4 class="list-title">
                    Links
                </h4>
                <div class="body-content">
                    <p>
                        This iHeartMedia Site may contain links to websites, applications or other services operated by third parties (the “Linked
                        Sites”). iHeartMedia does not monitor or control the Linked Sites and makes no representations regarding, and is not liable or
                        responsible for the accuracy, completeness, timeliness, reliability or availability of, any of the content uploaded, displayed,
                        or distributed, or products, or services available at the Linked Sites. If you choose to access any third-party site (including
                        any Linked Site), you do so at your own risk, and your use of that site is subject to its own terms of use and privacy policy,
                        which you should review. The presence of a link to a third-party site does not constitute or imply iHeartMedia’s endorsement,
                        sponsorship, or recommendation of the third party or of the content, products, or services contained on, or available through,
                        the site.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="software"></a>
                <h4 class="list-title">
                    Software and Downloads Available Through This Site
                </h4>
                <div class="body-content">
                    <p>
                        Any software that is made available to access, use, view and/or download in connection with a iHeartMedia Site or iHeartMedia
                        Internet Service (“Software”), including applications, podcasts, audio streaming, or video streaming, is owned or controlled by
                        iHeartMedia and/or licensors, affiliates and suppliers and is protected by copyright laws and international treaty provisions.
                        Your use of the Software is limited to private, non-commercial use and is governed by the terms of the end user license
                        agreement, if any, which accompanies or is included with the Software. iHeartMedia accepts no responsibility or liability in
                        connection with any Software owned or controlled by third parties.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="export-controls"></a>
                <h4 class="list-title">
                    International Use/U.S. Export Controls
                </h4>
                <div class="body-content">
                    <p>
                        Accessing materials on this iHeartMedia Site by certain persons in certain countries may not be lawful, and iHeartMedia makes no
                        representation that materials on this iHeartMedia Site are appropriate or available for use in locations outside the United
                        States. If you choose to access this iHeartMedia Site from outside the United States, you do so at your own risk and initiative,
                        and are responsible for compliance with any applicable local laws.
                    </p>
                    <p>
                        The United States controls the export of any software downloadable from this iHeartMedia, Inc., Site. No software or any other
                        materials associated with this iHeartMedia, Inc., Site may be downloaded or otherwise exported or re-exported to countries or
                        persons prohibited under export control laws, including but not limited to countries against which the United States has
                        embargoed goods, or to anyone on the U.S. Treasury Department list of Specially Designated Nationals and Blocked Persons or the
                        U.S. Commerce Department's Table of Deny Orders. You are responsible for compliance with the laws of your local jurisdiction
                        regarding the import, export, or re-export of any such materials. By using and/or downloading any such materials from a
                        iHeartMedia, Inc., Site, you represent and warrant that you are not located in, under the control of, or a national or resident
                        of any such country to which such import, export, or re-export is prohibited or are not a person or entity to which such export
                        is prohibited.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="third-party"></a>
                <h4 class="list-title">
                    Third-Party Merchants
                </h4>
                <div class="body-content">
                    <p>
                        This iHeartMedia Site may enable you to order and receive products, information and services from businesses that are not owned
                        or operated by iHeartMedia. The purchase, payment, warranty, guarantee, delivery, maintenance, and all other matters concerning
                        the merchandise, services or information, opinion or advice ordered or received from such businesses are solely between you and
                        such businesses. iHeartMedia does not endorse, warrant, or guarantee such products, information, or services, and is not liable
                        for the accuracy, completeness, or usefulness of such information or the quality of availability of such products or services.
                        iHeartMedia will not be a party to or in any way responsible for monitoring any transaction between you and third-party
                        providers of such products, services, or information, or for ensuring the confidentiality of your credit card information. Any
                        separate charges or obligations you incur in your dealings with these third parties are your responsibility and are not part of
                        the fee, if any, charged for the iHeartMedia Internet Service.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="advertisements"></a>
                <h4 class="list-title">
                    Advertisements, Sponsorships, Co-Promotions and Other Partnerships
                </h4>
                <div class="body-content">
                    <p>
                        iHeartMedia may display advertisements for the goods and services of a third party on the iHeartMedia Sites, including in
                        connection with co-promotions, sponsorships and other similar partnership arrangements. iHeartMedia does not endorse or
                        represent and is not responsible for the safety, quality, accuracy, reliability, integrity or legality of any such goods or
                        services advertised, promoted or displayed on this iHeartMedia Site.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="events"></a>
                <h4 class="list-title">
                    Events
                </h4>
                <div class="body-content">
                    <p>
                        You may be invited or asked to attend iHeartMedia-sponsored events or events held by other members and users of this iHeartMedia
                        Site which are not in any way associated with iHeartMedia at various locations throughout the United States (collectively,
                        “Events”). Your participation in any Events is at your own risk and you agree to release and hold iHeartMedia, its subsidiaries,
                        agents, distributors and affiliates, and their officers, directors and employees harmless from and against any and all claims,
                        actions, demands, liabilities, costs and expenses, including, without limitation, any injury or death to you or your minor
                        children or wards, resulting from attending the Events or participation in any activities available at the Events. You also
                        agree that we may film and record any of the Events sponsored by iHeartMedia in which you or your minor children or wards
                        participate and you hereby agree that such films and recordings shall be owned by iHeartMedia and we may use your or your minor
                        children or wards’ name, likeness, voice, performance and other activities in which you or your minor children or wards engage
                        for any advertising, promotional or other lawful purpose in any and all media now or hereafter known throughout the world in
                        perpetuity without notice, approval or compensation to you or any third party.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="interactive"></a>
                <h4 class="list-title">
                    Interactive Services and User Materials
                </h4>
                <div class="body-content">
                    <p>
                        This iHeartMedia Site may offer certain iHeartMedia Internet Services having interactive components such as bulletin boards,
                        chat rooms, blogs, and forums (collectively, “iHeartMedia Interactive Services”). Additional Terms may cover iHeartMedia
                        Internet Services, which appear on the pages where these services are available, in addition to the general terms provided
                        below. The selection of available iHeartMedia Internet Services may change from time to time in iHeartMedia’s sole discretion.
                        You may participate in the iHeartMedia Interactive Service by completing the registration form where one is provided.
                    </p>
                    <h5>
                        User Materials
                    </h5>
                    <p>
                        iHeartMedia does not control and is not responsible for any notes, messages, billboard postings, ideas, suggestions, concepts or
                        other material, or files delivered to a iHeartMedia Site by you or other users (collectively, “User Materials”). iHeartMedia is
                        not obligated to and does not regularly review, prescreen, monitor, delete, or edit User Materials. However, iHeartMedia
                        reserves the right to do so at any time in its sole discretion, for any reason or no reason, and to refuse, delete, move or edit
                        any User Materials, in whole or in part, with or without notice. iHeartMedia is not responsible or liable for damages of any
                        kind arising from any User Materials even when iHeartMedia is advised of the possibility of such damages, or from iHeartMedia’s
                        alteration or deletion of any User Materials.
                    </p>
                    <p>
                        You are solely responsible and liable for all User Materials delivered to this iHeartMedia Site using your account. Any
                        violation of these provisions can subject your iHeartMedia account to immediate termination and, possibly, further legal action.
                        You represent and warrant that you own or otherwise control any and all rights in and to the User Materials and that public
                        posting and use of the User Materials by iHeartMedia will not infringe or violate the rights of any third party in any manner.
                    </p>
                    <p>
                        By submitting, transmitting, posting, uploading, modifying or otherwise providing any User Material in connection with this
                        iHeartMedia Site and/or a iHeartMedia Interactive Service, whether solicited or unsolicited, you are granting iHeartMedia and
                        its designees a royalty-free, fully paid, non-exclusive, irrevocable, perpetual, unrestricted, worldwide license to reproduce,
                        publish, transmit, perform, display, sublicense, create derivative works from and otherwise use such User Material for any
                        purpose, including, without limitation, advertising and promotional purposes, alone or as a part of other works in any form,
                        media or technology now or hereafter known. No credit, approval or compensation is due to you for any such use of User Materials
                        you may submit. iHeartMedia also has the right, but not the obligation, to use your username (and real name, image, likeness or
                        other identifying information, if provided in connection with User Materials), city and state in connection with broadcast,
                        print, online or other use or publication of your User Materials. Please note that any User Material you submit is and will be
                        treated as non-confidential and non-proprietary as to you, unless specifically stated otherwise in our Privacy Statement.
                    </p>
                    <p>
                        The information and opinions expressed in User Materials appearing on this iHeartMedia Site are not necessarily those of
                        iHeartMedia or its content providers, advertisers, sponsors, affiliated or related entities, and iHeartMedia makes no
                        representations or warranties regarding that information or those opinions, and expressly disclaims any responsibility for User
                        Materials. iHeartMedia does not represent or guarantee the truthfulness, accuracy, or reliability of any User Materials or
                        determine whether the User Materials violate the rights of others, and iHeartMedia has no control over whether such User
                        Materials are of a nature that you or other users might find offensive, distasteful or otherwise unacceptable. You acknowledge
                        that any reliance on any User Materials submitted by other users will be at your own risk, including any reliance on the
                        accuracy, completeness or usefulness of such User Materials. You acknowledge that this iHeartMedia Site is “public,” and in
                        addition to the license granted to iHeartMedia, other users will have access to your User Materials and might copy, modify or
                        distribute them.
                    </p>
                    <p>
                        If you are aware of any User Material on this iHeartMedia Site which violates these Terms, please contact us at
                        support@iheartradio.com. Please provide as much detail as possible, including a copy of the underlying material, the location
                        where iHeartMedia may find it, and the reason such User Material should be removed. Please note that filing a complaint will not
                        guarantee its removal, iHeartMedia will only remove User Materials if iHeartMedia believes the measure is necessary, in our sole
                        discretion. To the extent any notice is based on an alleged copyright violation, please follow in the instructions set forth in
                        the section entitled “Copyright Infringement.”
                    </p>
                    <p>
                        Other than those we specifically request, we do not accept or consider unsolicited creative materials, ideas or suggestions
                        either via this iHeartMedia Site, email or other means. This is to avoid any misunderstandings if your ideas are similar to
                        those we have developed or obtained independently. However, if you do still transmit to us, via this iHeartMedia Site, email or
                        otherwise, any unsolicited communication or material, you will be deemed to have granted to us the same rights as are set out in
                        this section with respect to User Materials. Without limitation thereof, you agree that iHeartMedia, our affiliates and our
                        licensees are free to use any ideas, concepts, know-how or techniques contained in any communication you send to us for any
                        purpose whatsoever including, but not limited to, developing, manufacturing and marketing products, services and content using
                        such information, without any credit, notice, approval or compensation to you.
                    </p>
                    <h5>
                        Referral Programs and &quot;Forward to a Friend&quot; Opportunities
                    </h5>
                    <p>
                        The iHeartMedia Site may offer referral programs that permit you to submit information about other persons (each, a “Referred
                        Person”), including, without limitation, U.S.-based email addresses, mobile telephone numbers, names, street addresses and other
                        contact information so they may receive information and/or promotional offers concerning the iHeartMedia Internet Service. You
                        may only refer persons with whom you have a personal relationship. You must have obtained the consent of the Referred Person
                        prior to providing us with his or her contact information. We reserve the right to limit the number of Referred Persons you can
                        submit. We reserve the right to limit the number of transmissions to any particular Referred Person from time to time. You may
                        not withdraw the contact information you provide for a Referred Person once it has been submitted. A Referred Person must be a
                        permanent, legal resident of the continental United States, at least 18 years old (or 19 years old if a resident of Alabama or
                        Nebraska, or 21 years old if a resident of Mississippi), and be able to register for the iHeartMedia Internet Service, or
                        otherwise use the iHeartMedia Internet Service. The contact information for a Referred Person must be valid and functioning in
                        order for us to contact him or her about the iHeartMedia Internet Service. We will not be responsible for validating the contact
                        information you provide. We may elect NOT to communicate with any Referred Person and/or e-mail address if he/she/it appears to
                        be on any of our “do not contact” or “do not e-mail” lists. In addition, we reserve the right to reject the participation of any
                        Referred Person if (a) the contact information provided by you is incorrect or not valid, (b) such individual has violated any
                        provision of these terms or conditions, or (c) we determine in our sole discretion that the participation of such individual
                        might be harmful to us, this iHeartMedia Site, any iHeartMedia Internet Service, or any third party for any reason. We
                        specifically disclaim any liability for exercising such right.
                    </p>
                    <p>
                        We may, at our discretion, send you a confirmation using any means available through the iHeartMedia Internet Service, including
                        email, text and other forms of messaging, to inform you that the Referred Person has registered for the iHeartMedia Internet
                        Service. If we send the confirmation to you via the carrier service with which you have a mobile communications subscription or
                        otherwise have access, you understand you will pay any service fees associated with any such access (including text messaging
                        charges in connection with messages to your mobile device). If you misuse any referral program or otherwise engage in improper
                        behavior with respect to a referral program, as we determine in our sole discretion, we reserve the right to discontinue the
                        iHeartMedia Internet Service to you. We may from time to time offer incentives or rewards in connection with a referral program,
                        and any such incentive or reward programs shall be subject to Additional Terms which will be posted at the time such programs
                        become available and will be deemed incorporated into, and subject to, this Agreement. We reserve the right, in our sole
                        discretion, to suspend, temporarily or permanently, or cease to provide any and all referral programs without notice, reason or
                        liability.
                    </p>
                    <p>
                        If you are using the iHeartMedia Internet Service to communicate to a Referred Person (or any third party), you agree not to use
                        such iHeartMedia Internet Service to harm the Referred Person or any other third party, and/or use such iHeartMedia Internet
                        Service in violation of any applicable laws, rules or regulations or the terms and conditions of this Agreement.
                    </p>
                    <h5>
                        Voting/Rating Features
                    </h5>
                    <p>
                        For any voting/rating features that are available on this iHeartMedia Site, you must follow instructions on this iHeartMedia
                        Site to submit your votes/ratings, including any restrictions set forth with respect to limitations on voting/rating.
                        Votes/ratings received from you in excess of any stated limitation will be disqualified. Payment or other consideration in
                        exchange for votes/ratings is prohibited. Votes/ratings generated by script, macro or other automated means or any other means
                        intended to impact the integrity of the voting/rating process as determined by us may be void. iHeartMedia assumes no
                        responsibility for incorrect/inaccurate voting/rating information or for any error, omission, interruption, deletion, defect,
                        delay in operation or transmission, communications line failure, theft or destruction or unauthorized access to, or alteration
                        of, votes/ratings. We may, at our discretion, modify, terminate, or suspend the voting/rating or void any vote/rating should a
                        virus, bug, non-authorized human intervention, action of voter/rater, or other cause corrupt or impair the administration,
                        security, or fairness of the voting/rating. We reserve the right, in our sole discretion, to disqualify any individual it finds
                        to be violating these terms, tampering with the voting/rating process, or acting in an unsportsmanlike or improper manner and
                        void all associated votes/ratings. Our decisions with respect to all aspects of any voting/rating element are final and binding,
                        but not limited to, with respect to the tallying of votes/ratings and the invalidation or disqualification of any suspected
                        votes/ratings or voters/raters. You may also be given the opportunity to participate in voting/rating features in a third party
                        application or feature (such as one of our social media partners like Facebook or Twitter), in which case your participation in
                        such features will be subject to the terms and conditions governing that third party application or feature.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="subscription"></a>
                <h4 class="list-title">
                    Subscription Services
                </h4>
                <div class="body-content">
                    <p>
                        This iHeartMedia Site may offer certain iHeartMedia Subscription Services such as newsletters and Real Simple Syndication
                        (“RSS”) feeds (collectively “iHeartMedia Subscription Services”). By registering for a iHeartMedia Subscription Service, you
                        will be subject to any charges and rules set forth in the description of that service which may or may not be reflected in
                        Additional Terms.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="premium-services"></a>
                <h4 class="list-title">
                    Premium Services
                </h4>
                <div class="body-content">
                    <p>
                        SOME IHEARTMEDIA INTERNET SERVICES ON THIS IHEARTMEDIA SITE,  INCLUDING CERTAIN IHEARTMEDIA INTERACTIVE AND SUBSCRIPTION SERVICES, MAY  BE OFFERED TO YOU CONDITIONED ON YOUR PAYMENT OF A FEE (EACH, A  “IHEARTMEDIA PREMIUM SERVICE”). BY USING THE IHEARTMEDIA PREMIUM  SERVICE, YOU AGREE THAT YOU WILL BE SUBJECT TO ANY CHARGES AND RULES SET FORTH IN THE ADDITIONAL TERMS FOR THAT SERVICE, IN ADDITION TO THE GENERAL TERMS  PROVIDED BELOW, WHICH MAY INCLUDE AUTOMATIC RENEWALS AND PERIODIC RECURRING CHARGES (E.G., MONTHLY, QUARTERLY, OR ANNUALLY) TO YOUR PAYMENT METHOD UNTIL YOU CANCEL A SERVICE. YOU MAY CANCEL ONLINE BY FOLLOWING THE INSTRUCTIONS BELOW (“PAID SUBSCRIPTION SERVICE – CANCELLATION”).  YOU MAY REGISTER FOR BY COMPLETING THE APPLICABLE  REGISTRATION FORM.
                    </p>


                    <h5>Plus and All Access Service</h5>
                    <p>iHeartMedia provides various types of music streaming services:  (1)  the “Free-of-Charge Service”; (2) the “Plus Service”; (3) the “All  Access Service”; and (4) an “All Access Family Plan.” The following terms are applicable only if you purchase the Plus Service, the All Access Service, or the Family Plan (each, a “Paid Subscription  Service”). In order to obtain access to the Paid Subscription Service, you must also pay a specified, automatically recurring subscription fee (e.g., monthly, quarterly, or annually).  Further details about the Paid Subscription Service can be found at <a href="https://ondemand.iheart.com/">ondemand.iheart.com</a>.  iHeartMedia may, at its sole discretion, prospectively change the features and  services offered in any of the Paid Subscription Service at any time.  The Paid Subscription Services are available to US residents only.</p>

                    <h5>Paid Subscription Service—Billing and Fees</h5>

                    <p>Before you register for a Paid Subscription Service, iHeartMedia will  provide notice of and get your affirmative consent to any charges.  By registering for a Paid Subscription  Service (which includes “Direct Purchases” and “In-App Purchases,” as  defined below), you affirmatively consent to pay all fees and charges associated with  the Paid Subscription Service on a timely basis and are responsible for  any charges incurred by your account.  You warrant that the credit card  information that you provide is correct and is your account.  We are not  liable for any loss that you may incur as a result of someone else  using your password or account, whether with or without your knowledge.</p>
                    <p>BY REGISTERING FOR A PAID SUBSCRIPTION SERVICE AND PROVIDING YOUR  CREDIT CARD INFORMATION, YOU AUTHORIZE US (IN THE CASE OF A DIRECT  PURCHASE) OR THE APP STORE PROVIDER (IN THE CASE OF AN IN-APP PURCHASE)  TO CHARGE YOUR CREDIT CARD FOR THE PAID SUBSCRIPTION SERVICE FEE AT THE  THEN-CURRENT RATE AND ANY ADDITIONAL CHARGES (INCLUDING LATE CHARGES AND  APPLICABLE FEDERAL, STATE, OR LOCAL TAXES), AS WELL AS TO CHARGE YOUR CREDIT CARD ON AN AUTOMATIC AND RECURRING BASIS (E.G., MONTHLY, QUARTERLY, OR ANNUALLY), AS CONSENTED TO AT THE TIME OF REGISTRATION, UNTIL YOU CANCEL YOUR PAID SUBSCRIPTION SERVICE.  You may cancel online by following the instructions below (“Paid Subscription Service – Cancellation”).</p>
                    <p>If you make an in-app purchase of the Paid Subscription Service (an  “In-App Purchase”) via the applicable app store provider (e.g., Apple  App Store, Google Play, etc.) (an “App Store Provider”), then the App  Store Provider will charge your account the applicable fees or charges. Billing for In-App Purchases will be handled by the App Store Provider,  according to their terms.</p>
                    <p>If you purchase a Paid Subscription Service directly from us (a  “Direct Purchase”), we may use a third party that is not affiliated with  us to process your payments as merchant of record (“Processor”).  You  agree that this Processor is solely responsible for controlling,  handling and processing your payments.  For Direct Purchases, you will  receive a bill for the Paid Subscription Service fee based on the term  of the subscription for which you registered.  For example, if you  signed up for a monthly paid subscription, you will be billed one day  after each month on the calendar day that corresponds with the  commencement of your subscription.  If your payment date falls on a date  that does not exist in that month (e.g., the 31st day of a month), we  will bill your credit card on the last day of the applicable month.   However, you acknowledge that we reserve the right to change our billing  methods effective upon notice to you.  You may visit your member  account to update your credit card information, and you agree to  maintain a valid credit card during the subscription term.  You are  liable for any amounts that are unsettled and overdue.  We may  deactivate your member account without notice if your payment method is invalid and your payment becomes past  due.  Any delinquent payments are due in full within thirty (30) days of  the termination or cancellation of a Direct Purchase Paid Subscription  Service.</p>

                    <h5>Paid Subscription Service—Promotional Trial</h5>
                    <p>
                        iHeartMedia may offer a free or reduced price trial subscription of the
                        Plus Service or the All Access Service for a specified trial period either
                        without payment or with payment of a reduced fee (a “Trial”). To initiate
                        the Promotional Trial, you will be required to provide your credit card
                        information. By initiating the Promotional Trial, you affirmatively consent
                        to have your credit card charged automatically on a periodically recurring
                        basis if you do not cancel your subscription before the end of the
                        Promotional Trial.
                    </p>
                    <p>
                        Unless otherwise specified, Trials are limited to a single Paid
                        Subscription Service; you may not obtain more than a single Trial using the
                        same account or credit card and if you have previously used the same
                        account or credit card to obtain a Trial or Paid Subscription Service, then
                        that card will immediately be charged applicable fee upon registration.
                        iHeartMedia may, at its sole discretion and to the fullest extent permitted
                        by law, revoke or alter a Promotional Trial without notice to you and at
                        any time. Your Promotional Trial will end as soon as you purchase a Paid
                        Subscription Service.
                    </p>
                    <p>
                        BY INITIATING A PROMOTIONAL TRIAL, YOU AFFIRMATIVELY CONSENT TO BE
                        AUTOMATICALLY ENROLLED IN THE PAID SUBSCRIPTION SERVICE YOU SELECTED DURING
                        REGISTRATION, AND TO HAVE YOUR CREDIT CARD AUTOMATICALLY CHARGED THE
                        APPLICABLE FEES (INCLUDING APPLICABLE FEDERAL, STATE, OR LOCAL TAXES) ON A
                        RECURRING PERIODIC BASIS (E.G., MONTHLY, QUARTERLY, OR ANNUALLY), UNLESS
                        YOU CANCEL THE PAID SUBSCRIPTION SERVICE PRIOR TO EXPIRATION OF THE
                        PROMOTIONAL TRIAL<strong>. </strong>You may cancel online by following the
                        instructions below (“Paid Subscription Service – Cancellation”).
                    </p>
                    <h5>Paid Subscription Service—All Access Family Plan</h5>
                    <p>
                        If you purchase an All Access Family Plan, you, as the paying account
                        holder may designate up to five (5) non-paying account holders (each, a
                        “Family Account Holder”) to access the All Access Service. You will be the
                        only party responsible for paying for the All Access Family Plan and may
                        add or remove Family Account Holders from your All Access Family Plan at
                        any time. To invite a prospective Family Account Holder to participate in
                        your All Access Family Plan, you must provide the prospective Family
                        Account Holder’s email address. iHeartMedia will send an email to the
                        prospective Family Account Holder inviting them to participate in your All
                        Access Family Plan. To accept your invitation to participate in the All
                        Access Family Plan, the Family Account Holder must create an account with
                        iHeartMedia (if they do not already have one) and enter the paying account
                        holder’s billing zip code. All Family Account Holders must live in the same
                        household and be at least 13 years of age.
                    </p>
                    <h5>Paid Subscription Service—Automatic Renewal</h5>
                    <p>
                        By registering for a Paid Subscription Service, you affirmatively consent
                        to have your Paid Subscription Service automatically renewed, and to have
                        your credit card charged on a periodically recurring basis (e.g., monthly,
                        quarterly, or annually), unless you cancel your subscription before the end
                        of your current subscription term. You may cancel online by following the
                        instructions below (“Paid Subscription Service – Cancellation”).
                        Cancellation will be effective the day after the end of your current
                        subscription term. Your failure to cancel your subscription during the
                        current term will result in the automatic renewal of your subscription at
                        the then-current subscription rate and for the same term as your current
                        subscription (e.g., monthly, quarterly, yearly). As described below, if
                        iHeartMedia plans to change the subscription rate, we will provide you with
                        advance notice of the new rate. The cancellation of a Paid Subscription
                        Service will result in a downgrade of your subscription service to the
                        Free-of-Charge Service as of the end of your current subscription term.
                    </p>
                    <h5>Paid Subscription Service—Subscription Rate Changes</h5>
                    <p>
                        We may change the subscription rate from time to time. If we do, we will
                        provide you advance notice of any change in the subscription rate. If you
                        do not agree with the new subscription rate, you may cancel your
                        subscription prior to the new subscription rate taking effect. You may
                        cancel online by following the instructions below (“Paid Subscription
                        Service – Cancellation”). If you continue to use the Paid Subscription
                        Service after the subscription rate has gone into effect, you will be
                        charged the new rate until you cancel or the rate changes.
                    </p>
                    <h5>Paid Subscription Service—Data</h5>
                    <p>
                        As part of your purchase of a Paid Subscription Service, data collected
                        through the registration and payment process may be shared with our
                        applicable payment service provider. If you purchase the Paid Subscription
                        Service online, such information will be shared with Processor. If you make
                        an in-app purchase of the Paid Subscription Service, such information will
                        be shared with the applicable app store provider (e.g., Apple App Store,
                        Google Play, etc.). Your data will be handled in accordance with our
                        <a href="#privacystatement">Privacy Policy</a>.
                    </p>
                    <h5>Paid Subscription Service—Member Account and Password</h5>
                    <p>
                        You are responsible for any membership name and password that is associated
                        with your account during registration. If the Paid Subscription Service
                        does not recognize your device from a previous sign-in, you will be asked
                        for information that will help us to identify your registration. It is your
                        responsibility to maintain the confidentiality of your password, if one is
                        established. You are entirely responsible for any and all activities that
                        occur under your account, and agree to notify us immediately of any
                        unauthorized use of your account.
                    </p>
                    <h5>Paid Subscription Service—Limited to US Residents, Personal and Non-Commercial Use</h5>
                    <p>
                        The Paid Subscription Services are for US residents only. The Paid
                        Subscription Services are for your personal and non-commercial use only.
                        You may not modify, copy, distribute, transmit, display, perform,
                        reproduce, publish, license, create derivative works from, transfer, or
                        otherwise exploit the Paid Subscription Services without our prior written
                        consent.
                    </p>
                    <h5>Paid Subscription Service—Cancellation</h5>
                    <p>
                        You can cancel your Direct Purchases or a Paid Subscription Service at any
                        time by going to your online account “Settings” and following the
                        cancellation instructions; you may cancel your In-App Purchases by
                        following your App Store Provider’s instructions. In the event that you
                        cancel a Paid Subscription Service before the end of your subscription
                        period, we will not return any portion of your subscription fee provided
                        and you will be entitled to continue accessing the applicable Paid
                        Subscription Service until the end of your membership period.
                    </p>
                    <p>
                        If you violate this Agreement, we reserve the right to terminate your
                        access to a Paid Subscription Service or any portion thereof at any time,
                        without notice.
                    </p>

                    <h5>Member Account and Password</h5>
                    <p>You are responsible for any membership name and password that is associated with your account during registration. If this premium service does not recognize your device from a previous sign-in, you will be asked for information that will help us to identify your registration. It is your responsibility to maintain the confidentiality of your password, if one is established. You are entirely responsible for any and all activities that occur under your account, and agree to notify us immediately of any unauthorized use of your account.</p>



                    <h5>
                        Charges for Premium Service
                    </h5>
                    <p>
                        iHeartMedia will provide notice of any charges, or extra charges, before you register for or enter a premium area. You are
                        responsible for any charges for premium content incurred by your account. We are not liable for any loss that you may incur as a
                        result of someone else using your password or account, whether with or without your knowledge. In the event that you pay for a
                        premium service by credit card, you authorize us to charge your credit card account by registering for the service and providing
                        us with your credit card information. You warrant to us that the credit card information that you provide us is correct and is
                        your account.
                    </p>
                    <p>
                        In the event that you pay for a premium service by credit card, you authorize us to charge your credit card account by
                        registering for the service and providing us with your credit card information. You warrant to us that the credit card
                        information that you provide us is correct and is your account.
                    </p>
                    <h5>
                        Limited to Personal and Non-Commercial Use
                    </h5>
                    <p>
                        Any premium service is for your personal and non-commercial use only. You may not modify, copy, distribute, transmit, display,
                        perform, reproduce, publish, license, create derivative works from, transfer, or sell any information, software, products
                        obtained from this premium service without our prior written consent. You may inquire about obtaining written permission from us
                        to display or reproduce material from this iHeartMedia Site by writing:
                    </p>
                    <p>
                        In the event that you pay for a premium service by credit card, you authorize us to charge your credit card account by
                        registering for the service and providing us with your credit card information. You warrant to us that the credit card
                        information that you provide us is correct and is your account.
                    </p>
                    <ul class="address">
                        <li>IP Permission</li>
                        <li>Legal Department</li>
                        <li>iHeartMedia, Inc.</li>
                        <li>200 East Basse Road</li>
                        <li>San Antonio, TX 78209</li>
                        <li>By Facsimile: (210) 832-3149</li>
                        <li>By Email: <a href="mailto:IPPermission@iHeartMedia.com">IPPermission@iHeartMedia.com</a></li>
                    </ul>
                    <h5>
                        Cancellation
                    </h5>
                    <p>
                        You may cancel your membership in this premium service at any time by contacting us using the contact information provided on
                        this premium service. In the event that you have paid a fee to register on this iHeartMedia Site and you cancel before the end
                        of your membership period, we will not return any portion of your membership fee provided that you will be entitled to continue
                        accessing the applicable iHeartMedia Site until the end of your membership period.
                    </p>
                    <p>
                        We reserve the right to terminate your access to this premium service or any portion thereof at any time, without notice. Upon
                        such termination, we shall return the unused pro-rata portion of your membership fee on a 52-week pro-rated basis to you within
                        ninety (90) days of the termination of your access to this service.
                    </p>
                    <h5>
                        Service Contact
                    </h5>
                    <p>
                        You may email your requests for customer service through the contact information provided on the home page of the applicable
                        premium service.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="contests"></a>
                <h4 class="list-title">
                    Contests/Sweepstakes
                </h4>
                <div class="body-content">
                    <p>
                        Any sweepstakes, contests, games and/or promotional offers accessible on this iHeartMedia Site are governed by specific rules
                        and/or terms and conditions. By entering a sweepstakes or contests or participating in such games or promotional offers
                        available on this iHeartMedia Site, you will be subject to those rules and/or terms and conditions. It is critical that you read
                        the applicable rules and/or or terms and conditions, which are linked from the particular page or activity. To the extent of any
                        conflict between those rules and/or terms and conditions and these Terms, the rules and/or terms and conditions for the
                        sweepstakes, game or promotional offer will govern, but only to the extent of the conflict. Any sweepstakes, contests, games
                        and/or promotional offers made available or advertised on third party sites accessible from this iHeartMedia Site (such as those
                        of social media partners like Facebook and Twitter), in addition to being subject to the specific rules and/or terms and
                        conditions applicable to your participation in such feature(s) on this iHeartMedia Site, will also be subject to the rules
                        and/or terms and conditions applicable to your participation in such feature(s) on those third party sites.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="contests"></a>
                <h4 class="list-title">
                    Contests/Sweepstakes
                </h4>
                <div class="body-content">
                    <p>
                        Any sweepstakes, contests, games and/or promotional offers accessible on this iHeartMedia Site are governed by specific rules
                        and/or terms and conditions. By entering a sweepstakes or contests or participating in such games or promotional offers
                        available on this iHeartMedia Site, you will be subject to those rules and/or terms and conditions. It is critical that you read
                        the applicable rules and/or or terms and conditions, which are linked from the particular page or activity. To the extent of any
                        conflict between those rules and/or terms and conditions and these Terms, the rules and/or terms and conditions for the
                        sweepstakes, game or promotional offer will govern, but only to the extent of the conflict. Any sweepstakes, contests, games
                        and/or promotional offers made available or advertised on third party sites accessible from this iHeartMedia Site (such as those
                        of social media partners like Facebook and Twitter), in addition to being subject to the specific rules and/or terms and
                        conditions applicable to your participation in such feature(s) on this iHeartMedia Site, will also be subject to the rules
                        and/or terms and conditions applicable to your participation in such feature(s) on those third party sites.
                    </p>
                </div>
            </article>
            <article>
                <a class="anchor" name="general"></a>
                <h4 class="list-title">
                    General
                </h4>
                <div class="body-content">
                    <p>
                        This Agreement and any Additional Terms shall be governed by, construed and enforced in accordance with the laws of the State of
                        New York, as it is applied to agreements entered into and to be performed entirely within such state, without regard to conflict
                        of law principles. You agree that any and all disputes, claims and causes of action arising out of, or connected with, this
                        Agreement and/or the Additional Terms, or in connection with any matters related to this iHeartMedia Site and/or the Privacy
                        Statement, shall be resolved individually, without resort to any form of class action, exclusively in either the state or
                        Federal courts located in New York County, New York. You agree to submit to the personal jurisdiction of the courts of the State
                        of New York for any cause of action arising out of this Agreement. You agree to file any cause of action with respect to this
                        Agreement within one year after the cause of action arises. You agree that a cause of action filed after this date is barred.
                    </p>
                    <p>
                        If any provision of this Agreement, or the application thereof to any person or circumstances, is held invalid or for any
                        reason, unenforceable including, but not limited to, the warranty disclaimers and liability limitations, then such provision
                        shall be deemed superseded by a valid, enforceable provision that matches, as closely as possible, the original provision, and
                        the other provisions of this Agreement shall remain in full force and effect. The failure of either party to insist upon strict
                        performance of any provision of this Agreement shall not be construed as a waiver of any provision or right. Unless expressly
                        provided otherwise, this Agreement is the entire agreement between you and iHeartMedia with respect to the use of this
                        iHeartMedia Site and shall not be modified except in writing, signed by an authorized representative of iHeartMedia.
                    </p>
                    <p>
                        If you have any questions concerning this Agreement, you may send them by email to support@iheartradio.com. You must send any
                        official correspondence via postal mail to:
                    </p>

                    <ul class="address">
                        <li>Legal Department</li>
                        <li>ATTN: iHeartMedia Sites Terms of Use</li>
                        <li>iHeartMedia, Inc.</li>
                        <li>200 E. Basse Road</li>
                        <li>San Antonio, Texas 78209</li>
                    </ul>
                </div>
            </article>
            <article class="right">
                <a class="anchor" name="copyright"></a>
                <h4 class="list-title">
                    Copyright &amp; Trademark Notice
                </h4>
                <h5>Use of Intellectual Property</h5>
                <p>
                    The iHeartMedia Site, and all of its contents, including but not limited to articles, other text, photographs, images, illustrations,
                    graphics, video material, audio material, including musical compositions and sound recordings, software, iHeartMedia logos, titles,
                    characters, names, graphics and button icons (collectively “Intellectual Property”), are protected by copyright, trademark and other
                    laws of the United States, as well as international conventions and the laws of other countries. The Intellectual Property is owned
                    or controlled by iHeartMedia or by other parties that have provided rights thereto to iHeartMedia.
                </p>
                <p>
                    You may not, and agree that you will not, reproduce, download, license, publish, enter into a database, display, modify, create
                    derivative works from, transmit, post, distribute or perform publicly by any means, method, or process now known or later developed,
                    decompile, reverse engineer, disassemble, use on another computer-related environment, transfer or sell any Intellectual Property,
                    information, software or products obtained from or through this iHeartMedia Site, in whole or in part, without the express written
                    permission of iHeartMedia.
                </p>
                <p>
                    Other trademarks, service marks, product names and company names or logos appearing on this iHeartMedia Site that are not owned by
                    iHeartMedia may not be used without express permission from their owners.
                </p>
                <p>
                    Additionally, unless otherwise expressly permitted, websites may not link, whether by hyperlink or otherwise, to any page beyond the
                    homepage of this iHeartMedia Site, or frame this iHeartMedia Site, or any web page or material herein, nor may any entity include a
                    link to any aspect of this iHeartMedia Site in an email for commercial purposes, without the express written permission of
                    iHeartMedia. Further, unless otherwise expressly permitted, you agree not to link to iHeartMedia’s Intellectual Property so as to
                    cause you or anyone else to access iHeartMedia’s Intellectual Property other than through this iHeartMedia Site.
                </p>
                <h5>You may inquire about obtaining permission by writing:</h5>
                <ul class="address">
                    <li>IP Permission</li>
                    <li>Legal Department</li>
                    <li>iHeartMedia, Inc.</li>
                    <li>200 East Basse Road</li>
                    <li>San Antonio, TX 78209</li>
                    <li>By Facsimile: (210) 832-3149</li>
                    <li>By Email: IPPermission@iHeartMedia.com</li>
                </ul>
                <h5>Copyright Infringement</h5>
                <p>
                    iHeartMedia respects the intellectual property rights of third parties, and complies with the terms of the Digital Millennium
                    Copyright Act (DMCA) regarding such rights. By submitting any material or photographs through this iHeartMedia Site, you are granting
                    permission to have this material posted on this iHeartMedia Site, and are representing that you are the rightful owner of the
                    submitted material, and that no one else may claim rights to this material. iHeartMedia reserves the right to remove access to
                    infringing material. Such actions do not affect or modify any other rights iHeartMedia may have under law or contract. You can find
                    our procedures for providing notice of alleged copyright infringement below.
                </p>
                <h5>Procedure for Making Claim of Copyright Infringement</h5>
                <p>
                    If you believe that your work has been copied in a way that constitutes copyright infringement, you should send written notification
                    thereof, in accordance with the provisions of the Digital Millennium Copyright Act, to our Designated Agent, who can be reached as
                    follows.
                </p>
                <strong>By mail:</strong>
                <ul class="address">
                    <li>DMCA Designated Agent</li>
                    <li>c/o Legal Department</li>
                    <li>iHeartMedia, Inc.</li>
                    <li>200 East Basse Road</li>
                    <li>San Antonio, TX 78209</li>
                    <li>By Facsimile: (210) 832-3149</li>
                    <li>By Email: <a href="mailto:DMCA@iHeartMedia.com">DMCA@iHeartMedia.com</a></li>
                </ul>
                <h5>Pursuant to 17 U.S.C. § 512(c), to be effective, the Notification must include the following:</h5>
                <ol>
                    <li>
                        A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly
                        infringed.
                    </li>
                    <li>
                        Identification of the copyrighted work claimed to have been infringed, or, if multiple copyrighted works at a single online
                        site are covered by a single notification, a representative list of such works at that site.
                    </li>
                    <li>
                        Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be
                        removed or access to which is to be disabled, and information reasonably sufficient to permit iHeartMedia, Inc., to locate the
                        material.
                    </li>
                    <li>
                        Information reasonably sufficient to permit iHeartMedia, Inc., to contact the complaining party, such as an address, telephone
                        number, and, if available, an email address.
                    </li>
                    <li>
                        A statement that the complaining party has a good faith belief that use of the material in the manner complained of is not
                        authorized by the copyright owner, its agent, or the law.
                    </li>
                    <li>
                        A statement that the information in the notification is accurate, and under penalty of perjury, that the complaining party is
                        authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.
                    </li>
                </ol>
                <p>
                    This process only relates to reporting a claim of copyright infringement. Messages related to other matters will not receive a
                    response through this process.
                </p>
            </article>
            
        
</div>
<br />
<br />
<h1 id="article-title" style="text-transform:capitalize !important">Privacy Statement </h1>
<a name="privacystatement"></a>
<article class="article-body"></article>
<h5 class="updated-date">UPDATED AS OF: February 19<sup>th</sup>, 2019</h5>
<p>iHeartMedia, Inc. (&quot;iHeartMedia&quot;, &quot;we&quot;, &quot;us&quot;, or &quot;our&quot;) is committed to
    maintaining your confidence and trust as it relates to the privacy of your information. This Privacy Policy
    describes how we collect, protect, share and use your information as part of our technology platforms, including,
    our websites, interactive features and mobile Apps (&quot;Platforms&quot;) and how you may access and control its
    use in connection with our marketing communications and business activities. </p>
<div class="sub-title">QUICK GUIDE TO CONTENTS</div>
<blockquote>
    <ol class="jump-to-list">
        <li><a href="#1">INFORMATION COLLECTED ON OUR PLATFORMS</a></li>
        <li><a href="#2">HOW WE USE THE INFORMATION WE COLLECT</a></li>
        <li><a href="#3">SHARING OF INFORMATION</a></li>
        <li><a href="#4">ADVERTISING SERVICES</a></li>
        <li><a href="#5">YOUR PRIVACY RIGHTS, CHOICE AND ACCESS</a></li>
        <li><a href="#6">CHILDREN</a></li>
        <li><a href="#7">SECURITY OF YOUR INFORMATION</a></li>
        <li><a href="#8">OTHER SITES</a></li>
        <li><a href="#9">CONSENT TO PROCESSING AND TRANSFER OF INFORMATION</a></li>
        <li><a href="#10">CHANGES</a></li>
        <li><a href="#11">CONTACT US</a></li>
    </ol>
</blockquote>
<div class="jump-to-container">
    <section></section><a name="1" class="anchor"></a>
    <h4 class="list-title"><span class="number">1.</span> INFORMATION COLLECTED ON OUR PLATFORMS</h4>
    <div class="body-content">
        <p><strong>Information You Provide To Us</strong> </p>
        <p>When you use the Platforms, for example to register to create an account, update your information, sign up
            for a subscription, use certain features, access or download content, purchase items or enter into
            promotions, refer others to the Platform, interact with us via third party applications or social media
            sites, request customer support or otherwise communicate with us, we or our service providers or business
            partners may collect information that identifies you, including personal information <strong>, </strong>such
            as your name, e-mail address, telephone number, mobile phone number, credit card or debit card details,
            geographic location, mailing address, date of birth, demographic information (such as zip code, age and
            gender), photos, videos, social media account details and other information from your device. <strong><em></em></strong></p>
        <p>You may decide to provide us with another person's email or phone number so that person may be invited
            to join iHeartMedia or so that we may facilitate your communication with other people through the service.
            Local law may require you to seek the consent of your contacts to provide their personal information to
            iHeartMedia, who may use that information in accordance with the terms of this Privacy Policy. </p>
        <p>You may choose to create a personal iHeartRadio Code by enabling this feature through the settings in the
            App. The iHeartRadio Code is connected to a unique identifier that ties back to your iHeartRadio account
            and the information associated with your account (e.g., email, year of birth, gender). The iHeartRadio Code
            can be scanned to enter iHeartMedia contests, access events, and other exclusive promotions and to interact
            with our Services. We may use the iHeartRadio Code for our administering our contests, promotions, events
            and for advertising, marketing and related business purposes. We may share the iHeartRadio Code and the
            associated unique identifier (and underlying account data as well as your name and phone number), with
            third parties, including our business partners for marketing purposes and as set forth in the applicable
            sections of our Privacy Policy governing the use and sharing of such data. </p>
        <p><strong>Information Collected Automatically</strong> </p>
        <p>Whenever you visit or interact with the Platforms, we, as well as any third-party advertisers and/or service
            providers, may use a variety of technologies that automatically or passively collect information about how
            the Platforms are accessed and used (&quot;Usage Information&quot;). Usage Information may include
            information collected in the following ways: </p>
        <p><strong><em>Log Files:</em></strong> Information is logged about your use of the Platforms, including your
            browser type, operating system, activations, content you purchase, access, or download, listening times,
            the page served and the preceding page views. This statistical usage data provides us with information
            about the use of the Platforms, such as how many visitors visit a specific page on the Platforms, how long
            they stay on that page, and which hyperlinks, if any, they &quot;click&quot; on. It also provides us with
            information about your interactions with songs, playlists, audiovisual content and other iHeartMedia users,
            as well as advertising and products or services offered through or linked from the Platforms. This
            information helps us keep our Platforms fresh and interesting to our visitors and enables content to be
            tailored on and off the Platforms to a visitor's interests. </p>
        <p><strong><em>Device Identifiers:</em></strong> We or our service providers and partners may automatically
            collect your IP address or other unique identifier (&quot;Device Identifier&quot;) for the computer, mobile
            device, tablet or other device (&quot;Device&quot;) you use to access the Platforms, including the hardware
            model and mobile network information. A Device Identifier is a number such as a mobile advertising
            identifier (Android AAID or Apple IDFA) that is automatically assigned to your Device when you access a
            website or its servers or an application, and our computers identify your Device by its Device Identifier.
            We may use and share with third parties Device Identifiers to, among other things, administer the
            Platforms, help diagnose problems with our servers, analyze trends, provide attribution metrics to our
            advertisers and partners, track users' web page movements, help identify you and your shopping cart,
            and gather broad demographic information for aggregate use. </p>
        <p><strong><em>Location Information:</em></strong> We may collect or infer information about the general
            location of your Device when you access or use the Platforms; or if you choose to turn on your Bluetooth,
            Wi-Fi or other geolocation functionality when you use the App, subject to your consent, the App may
            collect, infer, and use more precise geolocation information. We may use and share your location with
            certain third-parties for analytics, attribution and advertising purposes (currently advertising use is
            restricted to iHeartMedia properties only). </p>
        <p><strong><em>Cookies; Pixel Tags:</em></strong> The technologies used on the Platforms to collect Usage
            Information, including Device Identifiers, include but are not limited to: cookies (data files placed on a
            Device when it is used to visit the Platforms), mobile analytics software and pixel tags (transparent
            graphic image, sometimes called a web beacon or tracking beacon, placed on a web page or in an email, which
            indicates that a page or email has been viewed). Cookies may also be used to associate you with social
            networking sites and, if you so choose, enable interaction between your activities on the Platforms and
            your activities on such social networking sites. We, or our vendors and partners, may place cookies or
            similar files on your Device for security purposes, to facilitate site navigation and to personalize your
            experience while visiting our Platforms or third-party sites (such as to select which ads or offers are
            most likely to appeal to you, based on your browsing behavior, interests, preferences, location, or
            demographic information), as well as for market research related purposes. Personalized content may also
            include content from the providers of goods and services that you may be able to purchase through the
            Platform or otherwise (who may pay us to show you such content). Note, however, that if a third party
            content provider asks us to show content to users with certain characteristics (for example, women ages
            18-24) or a segment of that group (for example, women ages 18-24 who are located in a particular locality)
            and you respond to that content or click on links embedded in that content, the third-party content
            provider may conclude that you have the characteristics of the audience that they are trying to reach.
            iHeartMedia does not have access to or control over cookies, web beacons, or other technologies that third
            parties who have asked us to show you content may use, and the privacy practices of these third parties are
            not covered by this Privacy Policy. Please contact these companies directly for information on their data
            collection and privacy practices. For more information on advertising using cookies and how to opt out of
            advertising cookies specifically, please see the sections on Advertising Services; and Your Privacy Rights,
            Choices, Access below. </p>
        <p>Similarly, we may use analytics platforms and other tracking technologies to help us track and analyze usage
            of our Platforms, using cookies, pixels and other tracking technologies. </p>
        <p><strong>Information Received from Other Sources</strong> </p>
        <p>We may obtain information from other sources and combine that with information we collect through the
            Platforms. For example, if you create an account using your credentials from a third-party social media
            site, we will have access to certain information from that site, such as your name, profile and account
            information, such as lists of your friends, &quot;likes&quot;, comments you have shared, groups and
            location, in accordance with the authorization procedures determined by such third-party social media site.
            Services like Facebook Connect give you the option to post information about your activities on our
            Platforms to your profile page to share with others within your network. In addition, we may receive
            information about you if other users of a third party application or website give us access to their
            profiles and you are one of their &quot;connections&quot; or information about you is otherwise accessible
            through your &quot;connections'&quot; web page, profile page, or similar page on a social networking or
            other third party website or interactive service. </p>
        <p>You may integrate your iHeartMedia account with third party applications and sites. If you do, we may
            receive similar information related to your interactions with the iHeartMedia service on the third party
            application, as well as information about your publicly available activity on the third party application
            or site. </p>
        <p>We may also receive information about you from our partners and service providers, (including, for example,
            business partners, analytics vendors, advertising networks and search information providers) which we use
            to personalize your iHeartMedia experience, to measure ad quality and responses, and to display ads that
            are more likely to be relevant to you. </p>
    </div>
    <section></section><a name="2" class="anchor"></a>
    <h4 class="list-title"><span class="number">2.</span> HOW WE USE THE INFORMATION WE COLLECT</h4>
    <div class="body-content">
        <p>We use the information we collect about and from you for a variety of business purposes such as to: respond
            to your questions and requests; provide customer service; provide you with access to certain areas and
            features of the Platforms such as your favorite stations, your musical preferences, songs you have listened
            to, shared playlists and your interaction with other users; verify your identity; communicate with you
            about your account and activities on the Platforms and, in our discretion, changes to any iHeartMedia
            policy; market research; Platform analytics and operations; advertising attribution, analytics and
            reporting; curate content, personalize advertisements, and offers that are served to you on and off the
            Platforms by us or our partners and advertisers; (including for third party products and services) made
            available on or outside the Platform for example by providing personalized or localized radio stations,
            content, advertisements, recommendations, features that match your interests, browsing behavior and
            preferences; improve the Platforms; link or combine your information with information we receive from
            others to help understand your needs and to provide you with better service and offers; communicate with
            you, either directly or through one of our partners, for marketing and promotional purposes via emails,
            newsletters, notifications, or other messages, consistent with any permissions you may have communicated to
            us; comply with license obligations; and for purposes disclosed at the time you provide your information or
            otherwise with your consent. </p>
    </div>
    <section></section><a name="3" class="anchor"></a>
    <h4 class="list-title"><span class="number">3.</span> SHARING OF INFORMATION</h4>
    <div class="body-content">
        <p>The information collected or generated through your use of the Platform may be shared by you or by us as
            described below. </p>
        <p><strong>Sharing of Information by You</strong> </p>
        <p>iHeartMedia is a social service that offers many ways to find, enjoy, and share content. Your name and/or
            username, profile picture, who you follow, your followers, and your iHeartMedia user profile will be
            publicly available. </p>
        <p><strong>Your Activities.</strong> Your activity on the Platform (e.g., music you listen to, music you
            share), playlists created or followed, and content you post to the Platform are made publicly available by
            default and can be accessed by users who follow you or view your iHeartMedia profile and will also be
            displayed publicly in third-party search engine results and may appear together with other public profile
            information, such as your playlists, picture and name and/or username. Although this information is made
            public by default, in the &quot;<u>Your Choices</u>&quot; section below, we describe the controls that you
            can use to limit the sharing of your information. </p>
        <p><strong>Third Party Platforms.</strong> If your iHeartMedia account is connected to a third party
            application or social medial platform, iHeartMedia may automatically share your activity with that service,
            including the content you use and such information may be attributed to your account on the third party
            platform and published on such service. Likewise, if you log into a third party application with your
            iHeartMedia account, that third party application may have access to certain information such as your
            iHeartMedia playlists and activity. A third party platform's use of information collected from you (or
            as authorized by you) is governed by the third party platform's privacy policies and your settings on
            the relevant service. <strong><em></em></strong></p>
        <p><strong>Sharing of Information by Us</strong> </p>
        <p>We may share aggregate user statistics, demographic information, and Usage Information with third parties.
            We may also share your information as disclosed at the time you provide your information, as set forth in
            this Privacy &amp; Cookie Notice and in the following circumstances: </p>
        <p><strong>Third Parties.</strong> In order to carry out your requests, to make various features, services and
            materials available to you through the Platforms, and to respond to your inquiries, we may share your
            information with third parties that perform functions on our behalf, such as companies that: host or
            operate our Platforms; payment processors; analyze data; provide customer service; sponsors or other third
            parties that participate in or administer our promotions. Your personal information may also be used by us
            or shared with our sponsors, partners, advertisers, advertising networks, advertising servers, and
            analytics companies or other third parties in connection with marketing, promotional, and other offers, as
            well as product information, and for such third party's advertising, analytics, planning and market
            research. These advertisers, advertising networks, advertising servers, and analytics companies use various
            technologies to collect data in order to send (or serve) relevant ads to users on our Platforms, or on
            platforms or websites operated by third parties. These technologies may include the placement of cookies or
            web beacons, the use of unique or non-unique non-personal identifiers, or the use of other technologies on
            our Platforms, which may be used to track user behavior both on and off our Platforms, to track how our
            Platforms are being used, to link various Devices you may use, build consumer profiles and to serve you
            more relevant ads. This Privacy Policy does not cover the use of various technologies by third party
            advertisers, advertising networks, advertising servers, and analytics companies. We may also share your
            information with the rights holders that license content to iHeartMedia to stream on the Platforms. </p>
        <p><strong>Your Agreement To Have Your Personal Information Shared.</strong> While on our Platforms, you may
            have the opportunity to opt-in to receive information and/or marketing offers from someone else or to
            otherwise consent to the sharing of your information with a third party. If you use the social sharing
            features of the Platforms, your information may be shared with certain of your friends or groups. If you
            agree to have your information shared, your information will be disclosed to the third party and the
            information you disclose will be subject to the privacy policy and business practices of that third party.
        </p>
        <p><strong>Business Transfers.</strong> Your information may also be used by us or shared with our subsidiaries
            and affiliates for internal reasons, primarily for business and operational purposes. As we continue to
            develop our business, we may sell or purchase assets. If another entity acquires us or all or substantially
            all of our assets, or assets related to the Platforms, your information may be disclosed to such entity as
            part of the due diligence process and will be transferred to such entity as one of the transferred assets.
            Also, if any bankruptcy or reorganization proceeding is brought by or against us, all such information may
            be considered an asset of ours and as such may be sold or transferred to third parties. </p>
        <p><strong>Legal Disclosure.</strong> iHeartMedia may transfer and disclose your information to third parties
            to comply with a legal obligation; when we believe in good faith that the law requires it; at the request
            of governmental authorities conducting an investigation; to verify or enforce our <strong><u>Terms of Use</u></strong>
            or other applicable policies; to respond to an emergency; or otherwise to protect the rights, property,
            safety, or security of third parties, visitors to our Platforms or the public. </p>
        <p><strong>Nielsen.</strong> Our Platforms may include Nielsen's proprietary measurement software which may
            allow you to contribute to market research. To learn more about the information Nielsen's software may
            collect and your choices with regard to it, please <a href="http://www.nielsen.com/digitalprivacy" target="_blank">click
                here</a>. </p>
    </div>
    <section></section><a name="4" class="anchor"></a>
    <h4 class="list-title"><span class="number">4.</span> ADVERTISING SERVICES</h4>
    <div class="body-content">When you use the Platforms, we, our service providers and advertising partners may set
        and access cookies and similar tracking technologies on your computer or Device in order to collect information
        about your online web-browsing activities over time and across different online websites and services
        (including, but not limited to, online or cloud computing services, and online applications) or mobile
        applications in order to serve you ads or other content personalized to your interests. This form of
        advertising is referred to as Interest Based Advertising (&quot;IBA&quot;). In order to personalize these ads
        or content, we or our advertising partners or service providers may connect and/or share data about you either
        to data you provided to us, e.g., your email address, or to Usage Information collected from you, such as your
        Device Identifier or IP address. The use of cookies, pixel tags, or similar technologies by these third parties
        is subject to their own privacy policies, not ours. If you do not want your information collected and used by
        us or the companies we work with for IBA purposes, you can opt out of this form of advertising by following the
        instructions in the next section. </div>
    <section></section><a name="5" class="anchor"></a>
    <h4 class="list-title"><span class="number">5.</span> YOUR PRIVACY RIGHTS, CHOICE, AND ACCESS.</h4>
    <div class="body-content">
        <p><strong>If you do not consent to the collection, use or sharing of your information in the manner described
                in this Privacy Policy, please do not provide us with such information. By providing such information,
                you are opting in to the collection, use and sharing of this information in accordance with this
                Privacy Policy. </strong></p>
        <p><em><u>Cookies, Tracking &amp; IBA: </u></em></p>
        <p>To learn how you may be able to reduce the number of cookies you receive from us or third parties, or delete
            cookies that have already been installed in your browser's cookie folder, please refer to your
            browser's help menu or other instructions related to your browser to see if you can reject or disable
            such cookies. You can also learn more about cookies by visiting <a href="http://www.allaboutcookies.org/">www.allaboutcookies.org</a>
            which includes additional useful information on cookies and how to block cookies using different types of
            browser. Removing or rejecting browser cookies does not necessarily affect third-party flash cookies used
            in connection with our Platform. To delete or disable flash cookies, please visit <a href="https://www.adobe.com/support/documentation/en/flashplayer/help/settings_manager.html" target="_blank">https://www.adobe.com/support/documentation/en/flashplayer/help/settings_manager.html
            </a>for more information. If you do disable or opt out of receiving cookies, please be aware that some
            features and services on our Platform may not work properly because we may not be able to recognize and
            associate you with your account(s). In addition, the offers we provide when you visit us may not be as
            relevant to you or tailored to your interests. </p>
        <p>As described above, we, or other parties we do business with, may place or recognize unique cookies or other
            technologies on your browser and Device when you visit our Platform to collect information about your use
            of our Platforms and your other online activities over time and across different websites and apps, and may
            use that information to serve interest-based advertisements to you as you browse the Internet. To learn
            more about such interest-based advertising, please visit: <a href="http://www.aboutads.info/choices/">http://www.aboutads.info/choices/</a>.
        </p>
        <p>There are 2 ways that you can opt out of IBA practices from certain iHeartmedia or third party ads on our
            Platforms and on third party sites and apps. One way is through the cross-industry Self-Regulatory Program
            for Online Behavioral Advertising managed by the Digital Advertising Alliance (DAA). To opt out of IBA,
            please click on the following link and follow the instructions: DAA AppChoices - <a href="http://www.aboutads.info/appchoices/">http://www.aboutads.info/appchoices/</a>
            and <a href="http://optout.aboutads.info/">http://optout.aboutads.info</a>. To opt out from the use of
            information about your online activities for IBA by NAI member companies, visit: <a href="http://www.networkadvertising.org/choices/">http://www.networkadvertising.org/choices/</a>.
        </p>
        <p>Another way to opt out of IBA is by clicking on the Advertising Options Icon featured on certain iHeartMedia
            ads on third-party websites. When clicked it (i) describes the collection and uses of data gathered at the
            relevant third-party website and (ii) provides a way for you to opt out of data collection and use by the
            third parties listed for the purposes of IBA. If you choose to opt out, our service provider will
            communicate your opt out election to the relevant third-party advertising partners and a cookie will be
            placed on your browser indicating your decision to opt out. </p>
        <p>Bear in mind that because cookies are stored by your browser, if you use different browsers on your
            computer, or multiple computers and Devices that have browsers and you do not wish to have cookies collect
            information for IBA purposes, you will need to opt out of IBA from each browser on each of the computers
            and Devices that you use. Please note that even if you opt out of IBA, you may still receive advertisements
            from us; they just won't be customized based on your web-browsing activities on third-party websites.
            Please also note that if you opt-out of IBA, we may still track your visits to the Platforms for our own
            analytics, operations and security purposes. </p>
        <p>To learn how to change your Device Identifier, please refer to your device's help menu or other
            instructions. </p>
        <p>Some browsers have a &quot;Do Not Track&quot; feature that lets you tell websites that you do not want to
            have your online activities tracked. These features are not yet uniform, so note that our systems are not
            configured to recognize Do Not Track headers or signals from some or all browsers. </p>
        <p><em><u>Data Sharing and Communications: </u></em>You may always direct us not to share your personal
            information with third parties, not to use your information to provide you with information or offers, or
            not to send you newsletters, e-mails or other communications by: </p>
        <ul>
            <li>sending us an e-mail at <a href="mailto:privacy@iheartradio.com">privacy@iheartradio.com</a>; </li>
            <li>contacting us by mail at iHeartMedia Digital Customer Service, 20880 Stone Oak Pkwy, San Antonio, TX
                78258 , or </li>
            <li>following the removal instructions in the communication that you receive. Your opt-out request will be
                processed within 30 days of the date on which we receive it. </li>
        </ul>
        <p>Please note that if you opt-out of various uses of your information the service we provide to you and
            relevant offers may be impacted. Also, If you opt out of receiving promotional offers, we may still send
            you non-promotional communications, such as those about your account or our ongoing business relations. </p>
        <p>Your California Privacy Rights: California law permits residents of California to request certain details
            about how their information is shared with third parties for direct marketing purposes. If you are a
            California resident and would like to make such a request, please contact us as described above in Data
            Sharing and Communications. </p>
        <p><em><u>Location Information: </u></em>By downloading our App or using the Platforms, you consent to our
            collection and use of your Device location information and sharing of location information with third
            parties for analytics and advertising purposes (currently advertising use is exclusively within iHeartMedia
            properties). If you initially consent to our collection of precise location information, you can
            subsequently stop the collection of precise location information at any time by going to the setting
            feature on your Device and changing your preferences. If you do so, certain features of our App may no
            longer function. You also may stop our collection of location information by following the standard
            uninstall process to remove our App from your device. </p>
        <p><em><u>Access to Your Information: </u></em>If you wish to verify, correct, or update any of your personal
            information collected through the Platforms, you may contact us at the above address or e-mail. In
            accordance with our routine record keeping, we may delete certain records that contain personal information
            you have submitted through the Platforms. We are under no obligation to store such personal information
            indefinitely and disclaim any liability arising out of, or related to, the destruction of such personal
            information. In addition, you should be aware that it is not always possible to completely remove or delete
            all of your information from our databases without some residual data because of backups and other reasons.
        </p>
    </div>
    <section></section><a name="6" class="anchor"></a>
    <h4 class="list-title"><span class="number">6.</span> CHILDREN</h4>
    <div class="body-content">
        <p>The Platform is not directed to children under 13. We do not knowingly collect, use or disclose personally
            identifiable information from anyone under 13 years of age. If we determine upon collection that a user is
            under this age, we will not use or maintain his/her Personal Information without the parent/guardian's
            consent. If we become aware that we have unknowingly collected personally identifiable information from a
            child under the age of 13, we will make reasonable efforts to delete such information from our records. </p>
    </div>
    <section></section><a name="7" class="anchor"></a>
    <h4 class="list-title"><span class="number">7.</span> SECURITY OF YOUR INFORMATION.</h4>
    <div class="body-content">
        <p>We use certain reasonable security measures to help protect your personal information. However, no
            electronic data transmission or storage of information can be guaranteed to be 100% secure. Please note
            that we cannot ensure or warrant the security of any information you transmit to us, and you use the
            Platforms and provide us with your information at your own risk. </p>
    </div>
    <section></section><a name="8" class="anchor"></a>
    <h4 class="list-title"><span class="number">8.</span> OTHER SITES.</h4>
    <div class="body-content">
        <p>The Platforms may contain links to other sites that we do not own or operate. This includes links from
            service providers, advertisers, sponsors and/or partners that may use our logo(s) as part of a co-branding
            or co-marketing agreement. We do not control, recommend or endorse and are not responsible for these sites
            or their content, products, services or privacy policies or practices. These other websites may send their
            own cookies to your Device, they may independently collect data or solicit personal Information and may or
            may not have their own published privacy policies. You should also independently assess the authenticity of
            any site which appears or claims that it is one of our Platforms (including those linked to through an
            email or social networking page). </p>
        <p>The Platforms may make available chat rooms, forums, message boards, and news groups. Remember that any
            information that you disclose in these areas becomes public information and is not subject to the
            provisions of this Privacy Policy. </p>
    </div>
    <section></section><a name="9" class="anchor"></a>
    <h4 class="list-title"><span class="number">9.</span> CONSENT TO PROCESSING AND TRANSFER OF INFORMATION.</h4>
    <div class="body-content">
        <p>The Platforms are governed by and operated in, and in accordance with the laws of, the United States, and
            are intended for the enjoyment of residents of the United States. iHeartMedia makes no representation that
            the Platforms are governed by or operated in accordance with the laws of any other nation. </p>
    </div>
    <section></section><a name="10" class="anchor"></a>
    <h4 class="list-title"><span class="number">10.</span> CHANGES</h4>
    <div class="body-content">
        <p>Any changes we may make to our Privacy Policy will be posted on this page. Please check back frequently to
            see any updates or changes to our Privacy Policy. If you do not agree or consent to these updates or
            changes, do not continue to use the Platform. </p>
    </div>
    <section></section><a name="11" class="anchor"></a>
    <h4 class="list-title"><span class="number">11.</span> CONTACT US.</h4>
    <div class="body-content">
        <p>If you have any questions or concerns about this Privacy Statement, the practices of the Platforms, or your
            experiences with the Platforms, please contact us at: </p>
        <p>Attn: Privacy Questions <br />iHeartMedia, Inc. <br />20880 Stone Oak Pkwy <br />San Antonio, TX 78258 <br />Re:
            iHeartMedia Privacy Statement <br />E-Mail: <u><a href="mailto:privacy@iheartradio.com">privacy@iheartradio.com</a></u>
        </p>
    </div>
</div>
​​​​​​​​​​​​​​​​​​​​​​​</div></div>
​​​​​​​</div>
				</div>
				
			</div>
		

	</section>
   	
   						
		
	
		
	
		
		
		<!-- Footer -->

        <div id="footer-wrap" class="section-wrap">
        <section id="footer" class="container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="section-box col-xs-12 col-lg-10">
                        <div class="row section-title-wrapper">
                            <div class="col-xs-12 col-md-12 section-footer-wrapper">
                                <div class="col-xs-12 col-md-4">
                                    <a href="/Pages/Contact.aspx" class="btn btn-primary outline right">Contact Us</a> 
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <a href="/Press" class="btn btn-primary outline">Press</a> 
                                </div>
                                <div class="col-xs-12 col-md-4">
                                    <a href="/pages/legal.aspx" class="btn btn-primary outline left">Legal</a> 
                                </div>

                            </div>
                        </div>
                    <div class="row content">
                        <div class="col-xs-12 col-md-12">
                            <p>Follow Us</p>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8 social-wrapper">
                                    <a href="https://www.facebook.com/premierenetworks" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="https://twitter.com/premierenetwork" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="https://www.linkedin.com/company/13567?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A2-3-6%2CtarId%3A1430516198712%2Ctas%3APremiere" target="_blank"><i class="fa fa-linkedin"></i></a>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <p class="copyright">&copy; 2019 Premiere Networks</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>
        </section>
        </div>
 	</div>





<!-- Off Canvas Navigation -->
<div class="sb-slidebar sb-left sb-momentum-scrolling">
  <!-- Your left Slidebar content. -->
    <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
    <ul class="nav sidebar-nav nav-spy">
        <li class="sidebar-brand">
            <a href="/Default.aspx">
               <h1>Premiere Networks</h1>
            </a>
        </li>
        <li><a href="/Default.aspx#s2">Programming</a></li>
        <li><a href="/Default.aspx#s3">Advertising</a></li>
        <li><a href="/Default.aspx#s4">About Us</a></li>
        <li><a href="http://engineering.premiereradio.com" target="_blank">Engineering</a></li>
    </ul>
</nav>
</div>

	
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002fsp.core.js?rev=7ByNlH\u00252BvcgRJg\u00252BRCctdC0w\u00253D\u00253D");</script><script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=b6\u00252FcRx1a6orhAQ\u00252FcF\u00252B0ytQ\u00253D\u00253D");</script><script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002fsp.ui.dialog.js?rev=IuXtJ2CrScK6oX4zOTTy\u00252BA\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.core.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script><script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f1033\u002fcore.js?rev=thUAOrLfyaU\u00252Fgyxy0eiMiw\u00253D\u00253D");</script>
<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;//]]>
</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002fsp.runtime.js?rev=9sKdsC9N6p2BiRk3313M7Q\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.core.js");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script><script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002fsp.js?rev=\u00252FLlKIPQWhhq9mRi404xe\u00252FA\u00253D\u00253D");RegisterSodDep("sp.js", "sp.core.js");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script><script type="text/javascript">RegisterSod("cui.js", "\u002f_layouts\u002fcui.js?rev=k\u00252B4HtUzT9\u00252B3mSycgD7gPaQ\u00253D\u00253D");</script><script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002finplview.js?rev=ZfVDYd30Z2D01DIRRl8ETA\u00253D\u00253D");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script><script type="text/javascript">RegisterSod("ribbon", "\u002f_layouts\u002fsp.ribbon.js?rev=F\u00252BUEJ66rbXzSvpf7nN69wQ\u00253D\u00253D");RegisterSodDep("ribbon", "core.js");RegisterSodDep("ribbon", "sp.core.js");RegisterSodDep("ribbon", "sp.js");RegisterSodDep("ribbon", "cui.js");RegisterSodDep("ribbon", "sp.res.resx");RegisterSodDep("ribbon", "sp.runtime.js");RegisterSodDep("ribbon", "inplview");</script><script type="text/javascript">RegisterSod("sp.publishing.resources.resx", "\u002f_layouts\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252EPublishing\u00252EResources\u0026rev=q6nxzZIVVXE5X1SPZIMD3A\u00253D\u00253D");</script><script type="text/javascript">RegisterSod("sp.ui.pub.ribbon.js", "\u002f_layouts\u002fsp.ui.pub.ribbon.js?rev=RGQSBI9Dm0E345iq\u00252FxUpHg\u00253D\u00253D");</script>
<script type="text/javascript">
//<![CDATA[

function EnsureScripts(scriptInfoList, finalFunction)
{
if (scriptInfoList.length == 0)
{
finalFunction();
}
else
{
var scriptInfo = scriptInfoList.shift();
var rest = function () { EnsureScripts(scriptInfoList, finalFunction); };
var defd;
try
{
eval('defd = typeof(' + scriptInfo[1] + ');');
}
catch (e)
{
defd = 'undefined';
}
if (scriptInfo[2])
{
EnsureScript(scriptInfo[0], defd, null);
ExecuteOrDelayUntilScriptLoaded(rest, scriptInfo[0]);
}
else
{
EnsureScript(scriptInfo[0], defd, rest);
}
}
}
function PublishingRibbonUpdateRibbon()
{
var pageManager = SP.Ribbon.PageManager.get_instance();
if (pageManager)
{
pageManager.get_commandDispatcher().executeCommand('appstatechanged', null);
}
}Sys.Application.initialize();
//]]>
</script>
</form>

</body>
</html>