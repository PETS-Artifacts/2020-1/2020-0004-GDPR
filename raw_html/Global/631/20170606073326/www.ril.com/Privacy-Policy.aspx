<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="head"><title>
	Privacy Policy :: Reliance Industries Limited
</title><meta http-equiv="content-type" content="text/html; charset=UTF-8" /> 
<meta http-equiv="pragma" content="no-cache" /> 
<meta http-equiv="content-style-type" content="text/css" /> 
<meta http-equiv="content-script-type" content="text/javascript" /> 
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
<link rel="shortcut icon" href="/App_Themes/RIL/images/generic/favicon.ico">
<link type="text/css" href="/App_Themes/RIL/css/bootstrap/bootstrap.css" rel="stylesheet">
<!-- <link type="text/css" href="/App_Themes/RIL/css/bootstrap/bootstrap-theme.css" rel="stylesheet"> -->
<link type="text/css" href="/App_Themes/RIL/css/bootstrap/bootstrap-select.min.css" rel="stylesheet">
<link type="text/css" href="/App_Themes/RIL/css/onepage-scroll.css" rel="stylesheet">
<link type="text/css" href="/App_Themes/RIL/css/ril.css" rel="stylesheet">
<link type="text/css" href="/App_Themes/RIL/css/jquery.bxslider.css" rel="stylesheet">
<link type="text/css" href="/App_Themes/RIL/css/responsive.css" rel="stylesheet">
<link type="text/css" href="/App_Themes/RIL/css/jquery.mCustomScrollbar.css" rel="stylesheet">



<!--[if lte IE 8]>
<link type="text/css" href="/App_Themes/RIL/css/ie8.css" rel="stylesheet">
<![endif]-->
<!--[if lte IE 9]>
<link type="text/css" href="/App_Themes/RIL/css/onepage-scroll-ie9.css" rel="stylesheet">
<![endif]-->

 <script src="/App_Themes/RIL/js/libs/modernizr.js" ></script>
<!--Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-63861062-2', 'auto');
  ga('send', 'pageview');

</script>
<!--End Google Analytics --> 
<link href="/favicon.ico" type="image/x-icon" rel="shortcut icon"/> 
<link href="/favicon.ico" type="image/x-icon" rel="icon"/> 
</head>
<body class="LTR ENUS ContentBody" >
    <form method="post" action="./Privacy-Policy.aspx?aliaspath=%2fPrivacy-Policy" onsubmit="javascript:return WebForm_OnSubmit();" id="form">
<div class="aspNetHidden">
<input type="hidden" name="manScript_HiddenField" id="manScript_HiddenField" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="9J/Wu0Bo54xTjh00FNFKo3reLICcRd3Ijiv36phQ2Ym/TOQHmyBbIA1I1Jx8gC7Ficw5DrBVFAVTfPz4J67FDbpYTRKhwAPOhn92n0wMT+AVK3+OTgZ9tUJIf0ix1a4kpi6bMWxLliJKTdu5AjTH5EQSzjZQHMFwqkfdVKgxII9na49bwPfwxOSto5G/XcUkJCClnTS4islkmJ7ePaIp/8QKgEGkOFwLot+XMvRT6pilp5Gf5/iUMXKQn900Pynm6uw7GrcdprFioG/qpsPAiLyF9c5tixu0sk/kW6va3RBqN269FerfLD0hdvuSdjYTH7ns20n/zqUpbPSOU53um6hlRTpI0TZvXTb6MmGlK7LYTkrFsOXZEEXJ+GfKvFbn9QS4xTFe+1tUgoBGkCOWXZU3SYHJAVkle4vpQRMlhJ99GDENYjgd/fYF3EBr981zTC3nGMqqNZEHkd2Zp8YUICo7srXZMSHoq0dIogJOtZcL6FM326c9jRUrM47+XoXVtMpbBKUp5jvme3p0BclylmQf3A/yvfZ731DZi24JIMCq4qyGW0KPUDGmVTgKcw4zp+p3jx8JXvbvFg4MqFUlPdYI+HnTxOk/pxeL/40+4qNvNWSrSbYhVPLVkdqWEqltM2sMAA0DHCjkHqoEOCzJ+FnXmWxTyZasQYjzrsV+fpeEfUcJY1HxJFVhJd2fnJ+GcP3EsTeYijGcuymxGK8NvG5V0UsMv0jAdpHwnIxbtrArNeH3nUNySLvd8UhRIY3WCwI2Wxg2oqYFqYQq3ycAGOKqRvzjrv6J1gvnZQuqmm43MwSLEG4UpM2a1VDQtG6QjYr0lvnLaSetGSdRyDBmIp5YLm4B2YRHpzZ9WPBE75v4YSwd83el4Ng/m+Sqqzz+g8seJg4MJEwMKncMaEZATqArfT+CMEEKn4/KVEFu4osBemtGhbLXEArglwLjST7k9x8Lnh/Lv9RAFojKtUwXqz5jFlo6p8eaQc0gsK9q5kro6LtULThYP2NcB5pLaQOWzzRdTmYoGn4P52XIGsBH7/FcAamyaOOjXjGGclvEgIiFDgSTQhOmzt4Qdzcsx31eP8P5M9NwjvrCbHGhfc5DLWYbhWqTVaD20VqxesonklFc6SDLysPJJydiq0+JkExfCkf1nhCNH9LSR1YVU3jFeIn0leedJAUHFPeDMchZ01gI9Wvz4/5bt6Ysr5hdFc2zF+YWwfSWIoDqTolirUMs2v/lIHzoWtQybEJF1lU1kwrxW/kAo7OiyxGikk5y4MFDXMYnYo/n+/PmJmuUDiywqG2aVo6JhpV1ASCHKBx3/3hliUBQ2bbH0AxGLzloNgMhlq1xa1meKeBJKNdovn5XwxhPqaowdo7WjQgmuGYwwpcHleDkSXOSMyhzpxhpvWUsd7IDEFVV+aqNRbiDVR9tlk5pvEU6EhMwO3Sbupb/cEHxeDYpdObQcvDf1aVHbTI8uw4Q0nAM0cNtrIWZI+V5nQnqK9UeLtmLdBdg6Q==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form'];
if (!theForm) {
    theForm = document.form;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=IEiak5QdXZs7t307cSGXsPAbv0_fAxANbkaMxxVSH7kS6G8id3_YPOJEvc7P28rv0ysNWs1bhwuUgYgxew6zABk2ISk1&amp;t=636161530540000000" type="text/javascript"></script>

<input type="hidden" name="lng" id="lng" value="en-US" />
<script type="text/javascript">
	//<![CDATA[

function PM_Postback(param) { if (window.top.HideScreenLockWarningAndSync) { window.top.HideScreenLockWarningAndSync(1080); }__doPostBack('m',param); }
function PM_Callback(param, callback, ctx) { if (window.top.HideScreenLockWarningAndSync) { window.top.HideScreenLockWarningAndSync(1080); }WebForm_DoCallback('m',param,callback,ctx,null,true); }
//]]>
</script>
<script src="/ScriptResource.axd?d=GFF6TqY0wPprSQxONX0AelToGqbcBLNKtEqgpYnMFX-3PMLiS47Wyh7gCYdqujRDRoM900IOWT-5y6Ya7T9APIbMb3ceTQPql0kK5WzXtfc9_MZGzSFOdgtyvXdyUKhTVp-jETkqZZtMuI2P626BfrnCwzM1&amp;t=ffffffffccb3b105" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=pMYqSEoLWH3jMxpox8G1KA8Kb3dFrOpiKm0s67sTj3L1YAcOptroq_XLyceqbTj63T7e8IANVtgB3VzWQUXirKcLjlxncGZ2Kc4fqYbaxOSQA9DA0&amp;t=3a1336b1" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=UDXlimVP8_fFz32o_LYQANX9Mr_i2mYkBj97C_WlRpGzi36vWSm7FuLRj3xi9u4IBxOpdD-OfGRgm7ZNnN0uGs6-RQsxlOKKFALoIRcV4kvn6rdPLsTL6wDjcjcfuugRi8bB-Q2&amp;t=3a1336b1" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=6oVD4f1Zk9RTmCpLTlbbJFVTqRa3sa8QvBRecHxVufsnW-89rqzHWkGiwV5dLYyyeJJJCka2I2ifzQ-Y6V34PA77ne_3MDlxxqP3TIv2TGXaaWxr0Q3gkgP8UbsSXEkkYRbTMQ2&amp;t=3a1336b1" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="A5343185" />
	<input type="hidden" name="__SCROLLPOSITIONX" id="__SCROLLPOSITIONX" value="0" />
	<input type="hidden" name="__SCROLLPOSITIONY" id="__SCROLLPOSITIONY" value="0" />
</div>
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('manScript', 'form', ['tp$lt$ctl01$wp_Trading$UpdatePanelStock','','tp$lt$ctl09$wp_Subscription1$upnlAjax','','tctxM',''], ['p$lt$ctl01$wp_Trading$Timer1',''], [], 90, '');
//]]>
</script>
<div id="CMSHeaderDiv">
	<!-- -->
</div>
    <div id="ctxM">

</div>
    
<header>
  <div class="container">
    <div class="top-sec">
      <div class="row">
        <figure class="logo col-lg-3 col-md-3 col-sm-3"><a href="/" title="Reliance Industries Limited"><img id="p_lt_ctl00_EditableImage_ucEditableImage_imgImage" src="/App_Themes/RIL/images/generic/RIL_Logo.png" alt="Reliance Industries Limited" />

</a></figure>
       <!--<div class="col-lg-8 col-md-8 col-sm-9">-->
          <div class="search-sec fright"><ul class="list-unstyled social-menu">
            <li><div id="p_lt_ctl01_wp_Trading_UpdatePanelStock">
	
         <div class="stock mR5 "> 
            		<ul class="trading">
            <div id="p_lt_ctl01_wp_Trading_divNSEStockQuotes" class="stockQuotes"><li><p><i class='stock-up'></i><span class='text-blue'>NSE        1,330.00</span> <small> &nbsp; (0.40%)</small></p></li></div>
                 </ul>
        </div>
        <div class="stock ">
            <ul class="trading">
                <div id="p_lt_ctl01_wp_Trading_divStockQuotes" class="stockQuotes"><li><p><i class='stock-up'></i><span class='text-blue'>BSE        1,328.90</span> <small>  &nbsp; (0.31%)</small></p></li></div>
            </ul>
        </div>
    
</div>
<span id="p_lt_ctl01_wp_Trading_Timer1" style="visibility:hidden;display:none;"></span>
</li>
            <li><input id="txtTopSearch" maxlength="40" type="text" placeholder="Search" /><!--<input class="btn btn-info search-icon btnSearch" value="" /> -->
<div class="btn btn-info search-icon btnSearch">
	&nbsp;</div>




</li>
             <li class="share"><a href="https://www.facebook.com/RelianceIndustriesLimited" target="_blank" title="Facebook"></a>



</li>
             <li class="sharetwt"><a href="https://twitter.com/flameoftruth" target="_blank" title="Twitter"></a>



</li>
             <li class="sharelnkin"><a href="https://www.linkedin.com/company/reliance" target="_blank" title="Linkedin"></a>



</li>
            <li class="shareyoutube"><a title="YouTube" href="https://www.youtube.com/user/flameoftruth2014" target="_blank"></a>



</li> 
            </ul></div>
        <!--</div>-->
      </div>
    </div>
    <div class="row">
       <nav class="navbar main-nav navbar-default ">
	<div class="navbar-header">
		<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button></div>
	<div class="navbar-collapse collapse" id="navbar">
		<ul class="nav navbar-nav">
			<li>
				<a class="dropdown-toggle" href="/TheRelianceStory.aspx">The Reliance Story </a></li>
			<li class="dropdown our-company">
				<a class="dropdown-toggle" data-close-others="false" data-delay="100" data-hover="dropdown" data-toggle="dropdown" href="javascript:;">Our Company </a>
				<ul class="dropdown-menu">
					<li>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/OurCompany/About.aspx" tabindex="-1">About </a></div>
							<div class="menu-sub-in">
								Leadership
								<ul>
									<li>
										<a href="/OurCompany/Leadership/FounderChairman.aspx">Founder Chairman</a></li>
									<li>
										<a href="/OurCompany/Leadership/Mukesh-Ambani-Chairman-and-Managing-Director.aspx">Chairman &amp; Managing Director</a></li>
									<li>
										<a href="/OurCompany/Leadership/BoardOfDirectors.aspx">Board of Directors</a></li>
									<li>
										<a href="/OurCompany/Leadership/BoardCommittees.aspx">Board Committees</a></li>
								</ul>
							</div>
						</div>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a href="/OurCompany/Manufacturing.aspx" tabindex="-1">Manufacturing Excellence</a></div>
							<div class="menu-sub-in">
								<a href="/OurCompany/R-D.aspx">Research &amp; Technology Development</a></div>
							<div class="menu-sub-in">
								<a href="/OurCompany/ProductsAndBrands.aspx">Products &amp; Brands</a></div>
							<div class="menu-sub-in">
								<span class="link"><a href="/OurCompany/CSR.aspx">Corporate Social Responsibility</a></div>
						</div>
					</li>
				</ul>
			</li>
			<li class="dropdown our-businesses">
				<a class="dropdown-toggle" data-close-others="false" data-delay="100" data-hover="dropdown" data-toggle="dropdown" href="javascript:;">Our Businesses </a>
				<ul class="dropdown-menu">
					<li>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/OurBusinesses/Exploration.aspx" tabindex="-1">Exploration &amp; Production</a></div>
							<div class="menu-sub-in">
								<a class="link" href="/OurBusinesses/PetroleumRefiningAndMarketing.aspx" tabindex="-

1">Petroleum Refining &amp; Marketing </a></div>
							<div class="menu-sub-in">
								<a class="link" href="/OurBusinesses/Global-Corporate-Security.aspx" tabindex="-1">Global Corporate Security</a>
								<ul>
									<li>
										<a href="/OurBusinesses/Global-Corporate-Security/Security-Services.aspx">Security Services</a></li>
								</ul>
							</div>
						</div>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/OurBusinesses/Petrochemicals.aspx" tabindex="-

1">Petrochemicals</a><br />
								<ul>
									<li>
										<a href="/OurBusinesses/Petrochemicals/Polymers.aspx">Polymers</a></li>
									<li>
										<a href="/OurBusinesses/Petrochemicals/Polyesters.aspx">Polyesters</a></li>
									<li>
										<a href="/OurBusinesses/Petrochemicals/Fiber-Intermediates.aspx">Fiber Intermediates</a></li>
									<li>
										<a href="/OurBusinesses/Petrochemicals/Aromatics.aspx">Aromatics</a></li>
									<li>
										<a href="/OurBusinesses/Petrochemicals/Elastomers.aspx">Elastomers</a></li>
								</ul>
							</div>
						</div>
						<div class="menu-col-0 menu-col-3">
							<div class="menu-sub-in">
								<a class="link" href="/OurBusinesses/Textiles.aspx" tabindex="-1">Textiles </a></div>
							<div class="menu-sub-in">
								<a class="link" href="/OurBusinesses/Retail.aspx" tabindex="-1">Retail </a>
								<ul>
									<li>
										<a href="/OurBusinesses/Retail-Investor-Relations.aspx">Retail Investor Relations</a></li>
								</ul>
							</div>
							<div class="menu-sub-in">
								<a class="link" href="/OurBusinesses/Jio.aspx" tabindex="-1">Jio </a><br />
								<ul>
									<li>
										<a href="/OurBusinesses/Jio/Jio-Investor-Relations.aspx">Jio Investor Relations</a></li>
									<li>
										&nbsp;</li>
								</ul>
							</div>
						</div>
					</li>
				</ul>
			</li>
			<li class="dropdown sustainability">
				<a class="dropdown-toggle" data-close-others="false" data-delay="100" data-hover="dropdown" data-toggle="dropdown" href="javascript:;">Sustainability </a>
				<ul class="dropdown-menu">
					<li>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/Sustainability/CorporateSustainability.aspx" tabindex="-1">Corporate Sustainability</a></div>
						</div>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/Sustainability/HealthSafety.aspx" tabindex="-1">Health, Safety and Environment </a></div>
						</div>
					</li>
				</ul>
			</li>
			<li>
				<a href="/OurCompany/Innovation.aspx">Innovation</a></li>
			<li class="dropdown investor-relations">
				<a class="dropdown-toggle" data-close-others="false" data-delay="100" data-hover="dropdown" data-toggle="dropdown" href="javascript:;">Investor Relations </a>
				<ul class="dropdown-menu">
					<li>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/InvestorRelations/FinancialReporting.aspx" tabindex="-1">Financial Reporting </a></div>
							<div class="menu-sub-in">
								<a class="link" href="/InvestorRelations/ShareholdersInformation.aspx" tabindex="-

1">Shareholders Information</a></div>
							<div class="menu-sub-in">
								<a class="link" href="	/InvestorRelations/Chairman-Communication.aspx" tabindex="-

1">Chairman&#39;s Communication</a></div>
							<div class="menu-sub-in">
								<a href="/InvestorRelations/Investor-Contacts.aspx">Investor Contacts</a></div>
						</div>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/InvestorRelations/GrievanceManagement.aspx" tabindex="-

1">Grievance Management</a></div>
							<div class="menu-sub-in">
								<a class="link" href="/InvestorRelations/Downloads.aspx" tabindex="-

1">Downloads</a></div>
							<div class="menu-sub-in">
								<a class="link" href="/InvestorRelations/Corporate-Announcements.aspx" tabindex="-

1">Corporate Announcements</a></div>
							<div class="menu-sub-in">
								<a class="link" href="/InvestorRelations/Notices.aspx" tabindex="-

1">Notices</a></div>
						</div>
					</li>
				</ul>
			</li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-close-others="false" data-delay="100" data-hover="dropdown" data-toggle="dropdown" href="javascript:;">Careers </a>
				<ul class="dropdown-menu">
					<li>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/Careers/WorkingatRIL.aspx" tabindex="-1">Working at RIL </a></div>
							<div class="menu-sub-in" style="display: none;">
								<a class="link" href="/Careers/RALP.aspx" tabindex="-1">Reliance Accelerated Leadership Program </a></div>
							<div class="menu-sub-in">
								<a class="link" href="/Careers/MeetourPeople.aspx" tabindex="-1">Meet our People </a></div>
						</div>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								<a class="link" href="/Careers/ApplyNow.aspx" tabindex="-1">Apply Now</a></div>
						</div>
					</li>
				</ul>
			</li>
			<li class="dropdown our-partners">
				<a class="dropdown-toggle" data-close-others="false" data-delay="100" data-hover="dropdown" data-toggle="dropdown" href="javascript:;">eB2B </a>
				<ul class="dropdown-menu">
					<li>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								For Suppliers
								<ul>
									<li>
										<a href="/eB2B/EPNotice.aspx">E&amp;P Notice for EOI</a></li>
									<li>
										<a href="http://scm.ril.com" target="_blank">eProcurement</a></li>
									<li>
										<a href="http://ebiz.ril.com" target="_blank">eBiz</a></li>
									<li>
										<a href="Http://supplierregistration.ril.com" target="_blank">Retail Vendor Registration</a></li>
								</ul>
							</div>
						</div>
						<div class="menu-col-0 menu-col-2">
							<div class="menu-sub-in">
								For Customers
								<ul>
									<li>
										<a href="http://ecommerce.ril.com" target="_blank">Internet Sales</a></li>
									<li>
										<a href="https://fiori.ril.com/sap/bc/ui5_ui5/sap/zcrmweb_lead/index.html?sap-client=586&amp;sap-ui-language=EN&amp;sap-ui-appcache=false&amp;saml2=disabled" target="_blank">Petrochemicals: New Customer Inquiry</a></li>
									<li>
										<a href="/eB2B/Bidder.aspx">Notice Inviting Offer - CBM Block SP (West) - CBM-2001/1</a></li>
								</ul>
								<div class="menu-sub-in">
									Product-Related Downloads&nbsp;
									<ul>
										<li>
											<a href="/eB2B/Polymer-Downloads.aspx">Polymers</a></li>
									</ul>
									</span>&nbsp; &nbsp;&nbsp;<br />
									&nbsp;</div>
							</div>
						</div>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</nav>
<br />





   </div>
  </div>
</header>


<section class="banner-in about-banner">
  <!--<div class="container">-->
    <div class="row">
      <div class="col-lg-12 posi-r">
        <img src="/App_Themes/RIL/images/our-company/about/banner.jpg" id="bannerimageAbout" />
        <div class="banner-sub-title">
          <h2>Infinite possibilities 



</h2><h3>Reshaping the future of India 



</h3>
        </div>
      </div>
    </div>
  <!--</div>-->
</section>
<section>
  <div class="container bg-white"> 
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumbs">
      <div class="breadcrumb pull-left"><a href="/RIL/index.aspx">Home</a><span  class="CMSBreadCrumbsCurrentItem">Privacy Policy</span>
 </div>
       <ul class="list-unstyled font-resize">
            <li> <a title="Decrease size" id="jfontsize-minus">A<sup>-</sup></a></li>
            <li> <a title="Increase size" id="jfontsize-plus" class="large">A<sup>+</sup></a></li>
            </ul>
        </div>
      </div>
    </div>   
  <div class="row">    	
    <div id="innovations">
      <section class="subLeft">
        <article>
          <h2 class="text-blue"><h2>Privacy Policy</h2>



</h2><div class="subBox textJustify">
	<p>
		Welcome to this Reliance website. We at Reliance respect the privacy of everyone who visits this website and are committed to maintaining the privacy and security of the personal information of all visitors to this website. Our policy on the collection and use of personal information and other information is outlined below.</p>
</div>





        </article>                
      </section>  
    </div>    
  </div>
  <div class="row">
    <section class="businesSep">
      <h2 class="text-blue"><h5>
	Personal Information</h5>




</h2><div class="subBox textJustify">
	<p>
		1. We do not require personal information to obtain access to most of our website.<br />
		<br />
		2. We collect personal information from our visitors on a voluntary basis. Personal information may include name, title, company, address, phone number, email address, and other relevant data. Questions or comments submitted by visitors may also include personal information.<br />
		<br />
		3. We collect and use personal information for business purposes in order :</p>
	<div>
		&nbsp;</div>
	<ul>
		<li>
			<p>
				that you may download product information, order products and take advantage of certain other features of our website.</p>
		</li>
		<li>
			<p>
				to provide information or interactive services through this website, to your e-mail address or, where you wish it to be sent by post, to your name and postal address.</p>
		</li>
		<li>
			<p>
				to seek your feedback or to contact you in relation to those services offered on the our website</p>
		</li>
		<li>
			<p>
				to process orders or applications submitted by you</p>
		</li>
		<li>
			<p>
				to administer or otherwise carry out our obligations in relation to any agreement you have with us</p>
		</li>
		<li>
			<p>
				to anticipate and resolve problems with any goods or services supplied to you</p>
		</li>
		<li>
			<p>
				to create products or services that may meet your needs.</p>
		</li>
		<li>
			<p>
				to process and respond to requests, improve our operations, and communicate with visitors about our products, services and businesses.</p>
		</li>
		<li>
			<p>
				to allow you to subscribe to our news letter</p>
		</li>
	</ul>
	<div>
		&nbsp;</div>
	<p>
		4. We will not use or share, either within Reliance or with a third party, any information collected at this page for direct marketing purposes. Because of the nature of the Internet, we may transmit the information to another country, but among Reliance and its affiliates, for purposes other than direct marketing, such as for storage, or for carrying out the processing detailed above, or because of where our servers are located, but we do not provide or use personal information to unrelated businesses for direct marketing purposes.<br />
		<br />
		5. To the extent required or permitted by law, we may also collect, use and disclose personal information in connection with security related or law enforcement investigations or in the course of cooperating with authorities or complying with legal requirements.<br />
		<br />
		6. We may also remove all the personally identifiable information and use the rest for historical, statistical or scientific purposes.<br />
		<br />
		7. If you e-mail us, you are voluntarily releasing information to us. Your e-mail address will be used by Reliance to respond to you. We will not use the information that can identify you, such as your e-mail address, for direct marketing purposes.<br />
		<br />
		8. In addition, we may have collected similar information from you in the past. By entering this website you are consenting to the terms of our information privacy policy and to our continued use of previously collected information. By submitting your personal information to us, you will be treated as having given your permission for the processing of your personal data as set out in this policy.</p>
</div>





    </section>
  </div>
  <div class="row">
    <section class="businesSep">
      <article>
        <h5>
	Non personal information</h5>
<div class="subBox textJustify">
	<p>
		1. At this web site, information sent by your web browser, may be automatically collected. This information typically includes your domain name (the site after the @ in your e-mail address). It may also contain your user name (the name before the @ in your e-mail address). Other examples of information collected by our server include the Internet protocol (IP) address used to connect the visitor&#39;s computer to the Internet, operating system and platform, the average time spent on our website, pages viewed, information searched for, access times, websites visited before and a visitor visits our website, and other relevant statistics. The amount of information sent depends on the settings you have on your web browser; please refer to your browser if you want to learn what information it sends.<br />
		<br />
		2. All such information will be used only to assist us in providing an effective service on this website. We may from time to time supply the owners or operators of third party websites from which it is possible to link to our website with information relating to the number of users linking to our website from such third party website. You cannot be identified from this information.<br />
		<br />
		3. We use the information we automatically receive from your web browser to see which pages you visit within our website, which website you visited before coming to ours, and where you go after you leave. We at Reliance can then develop statistics that are helpful to understanding how our visitors use this website. We use this information in the aggregate to measure the use of our website and to administer and improve our website. This statistical data is interpreted by Reliance in its continuing effort to present the website content that visitors are seeking in a format they find most helpful.</p>
	<br />
	<h5>
		Information placed on your computer</h5>
	<br />
	<p>
		We may store some information such as cookies on your computer when you look at our website. Cookies are pieces of information that a website transfers to the hard drive of a visitor&#39;s computer for record-keeping purposes. This information facilitates your use of our website and ensures that you do not need to re-enter your details every time you visit it. You can erase or choose to block this information from your computer if you want to; please refer to your browser settings to do so. Erasing or blocking such information may limit the range of features available to the visitor on our website. We use also use such information to provide visitors a personalised experience on our website. We may use such information to allow visitors to use the website without logging on upon returning, to auto-populate email forms, to make improvements and to better tailor our website to our visitors&#39; needs. We also use this information to verify that visitors meet the criteria required to process their requests.</p>
	<br />
	<h5>
		Security</h5>
	<br />
	<p>
		We have implemented technology and policies, with the objective of protecting your privacy from unauthorised access and improper use, and periodically review the same.</p>
	<br />
	<h5>
		Third Parties</h5>
	<br />
	<p>
		1. For your convenience, this page may contain certain hyperlinks to other Reliance pages as well as to websites outside Reliance. In addition, you may have linked to our website from another website. We cannot be responsible for the privacy policies and practices of other websites, even if you access them using links from our website. We can make no promises or guarantees regarding data collection on the hyper-linked pages and on websites that are not owned by Reliance. We recommend that you check the policy of each website you visit, or link from, and contact the owners or operators of such websites if you have any concerns or questions.</p>
	<br />
	<h5>
		Contacting us</h5>
	<br />
	<p>
		1. We aim to keep our information about you as accurate as possible. If you would like to review or change the details you have supplied us with, please contact us as set out below. If you wish to change or delete any of the personal information you have entered whilst visiting our website or if you have any questions about our privacy statement, e-mail the Data Manager at eBiz@ril.com<br />
		<br />
		2. If you are concerned about our use of your personal information, please contact us at eBiz@ril.com, with the subject line, &quot;privacy.&quot;<br />
		<br />
		3. If at any time you would like to contact us, you can do so by emailing us at eBiz@ril.com</p>
</div>





      </article>
    </section>
  </div>    
 

</div>
</section>
<section class="bg-grey-light pad-sec-md ie9footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-5 bottom-news-sec">
        <div class="row"><h4>
	Media Releases</h4>




</div><ul>
<div class="row pB10">
          <div class="col-lg-2 col-md-2 col-sm-3 bluebg">
            <span class="date">26</span> 
            <span class="month">Apr</span>
         </div>
          <div class="col-lg-9 news-content">
            <a href="/getattachment/5efebeba-dda2-46ae-a03c-467c77ecc898/RIL-partners-with-Germany’s-Resysta-to-bring-innov.aspx" target="_blank"><p>RIL partners with Germany’s Resysta to bring innovative wood alternative in [...]</p></a>
          </div>
</div><div class="row pB10">
          <div class="col-lg-2 col-md-2 col-sm-3 bluebg">
            <span class="date">24</span> 
            <span class="month">Apr</span>
         </div>
          <div class="col-lg-9 news-content">
            <a href="/getattachment/2ff2c127-d95e-4344-8986-325d018bf48d/RIL-s-Q4-(FY-2016-17)-Financial-and-Operational-Pe.aspx" target="_blank"><p>RIL's Q4 (FY 2016-17) Financial and Operational Performance</p></a>
          </div>
</div><div class="row pB10">
          <div class="col-lg-2 col-md-2 col-sm-3 bluebg">
            <span class="date">21</span> 
            <span class="month">Apr</span>
         </div>
          <div class="col-lg-9 news-content">
            <a href="/getattachment/d4ae96ad-c25e-48d6-a148-8aea401b9bcd/RELIANCE-COMMENCES-COMMERCIAL-PRODUCTION-OF-COAL-B.aspx" target="_blank"><p>RELIANCE COMMENCES COMMERCIAL PRODUCTION OF COAL BED METHANE IN SOHAGPUR</p></a>
          </div>
</div>
</ul><div align="right"><a id="p_lt_ctl09_Link_btnElem_hyperLink" href="/MediaReleases.aspx"><span id="p_lt_ctl09_Link_btnElem_lblText">More...</span></a></div><div id="p_lt_ctl09_wp_Subscription1_upnlAjax">
	
 <div class="mT20">
     <h5 class="text-grey-med">Subscribe to our news alerts</h5>
     <input name="p$lt$ctl09$wp_Subscription1$txtSubscription" type="text" value="Email Id" maxlength="40" id="p_lt_ctl09_wp_Subscription1_txtSubscription" onblur="if (this.value == &#39;&#39;) {this.value = &#39;Email Id&#39;;}" onfocus="if (this.value == &#39;Email Id&#39;) {this.value = &#39;&#39;;}" />
     <input type="submit" name="p$lt$ctl09$wp_Subscription1$btnSubmit" value="Subscribe" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;p$lt$ctl09$wp_Subscription1$btnSubmit&quot;, &quot;&quot;, true, &quot;grpSubscribe&quot;, &quot;&quot;, false, false))" id="p_lt_ctl09_wp_Subscription1_btnSubmit" class="btn btn-info" />
     <div class="clear"></div>
     <span id="p_lt_ctl09_wp_Subscription1_reqFieldSubscription" style="color:Red;display:none;">Please enter Email ID</span>
     <span id="p_lt_ctl09_wp_Subscription1_EmailValidator" style="color:Red;display:none;">Please enter valid Email ID</span>
     
     
 </div>
 
</div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3 text-center reliance-foundation">
        <figure>
	<a class="text-center" href="http://www.reliancefoundation.org" target="_blank"><img src="/App_Themes/RIL/images/generic/reliance-foundation-logo.png" alt="Reliance Foundation" /></a></figure>
<p>
	&nbsp;</p>
<span class="bottom-text mT20"><a href="http://www.reliancefoundation.org" target="_blank">Reliance Foundation works with some of the most vulnerable and marginalized communities across India... </a> </span>




      </div>
       <div class="col-lg-4 col-md-4 col-sm-4 quick-link">
        <h4>
	Quick Links</h4>
<ul class="list-styled list-arrow mB20">
	<li>
		<a href="https://www.jio.com/" target="_blank">Jio</a></li>
	<li>
		<a href="http://relianceretail.com/" target="_blank">Reliance Retail</a></li>
	<li>
		<a href="http://www.gennexthub.com/" target="_blank">GenNext Hub</a></li>
	<li>
		<a href="http://www.da-is.org" target="_blank">Dhirubhai Ambani International School</a></li>
	<li>
		<a href="http://www.mumbaiindians.com" target="_blank">Mumbai Indians</a></li>
	<li>
		<a href="/Contactus.aspx">Contact Us</a></li>
</ul>





      </div>
    </div>
  </div>
</section>
<footer class="pad-sec-sm">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-5">
	<p class="tleft">
		&copy; 2017 Reliance Industries Limited. All rights reserved.</p>
	<p class="tleft">
		Site best viewed in IExplorer 9+ | Chrome 40+ | Firefox 35+</p>
</div>
<div class="col-lg-2 col-md-2 col-sm-2 text-center ">
	<div class="socialIcon">
		&nbsp;</div>
</div>
<div class="col-lg-5 col-md-5 col-sm-5 ">
	<ul>
		<li>
			<a href="/Privacy-Policy.aspx">Privacy Policy</a></li>
		<li>
			<a href="/Legal-Notice.aspx">Legal Notice</a></li>
		<li>
			<a href="/Sitemap.aspx">Site Map</a></li>
		<li>
			<a href="mailto:ril.isoc@ril.com">Report Security Issue</a></li>
	</ul>
</div>
<br />





    </div>
  </div>
</footer>
 <div class="successMessage" id="divNotificationSuccess">
    <p>Processed successfully.</p>
  </div>
  <div class="errorMessage" id="divNotificationFailure">
    <p>Error occured.</p>
  </div>
<div class="overlay-menu"></div>
<script src="/App_Themes/RIL/js/libs/jquery-1.11.0.min.js" ></script> 
<!-- <script src="/App_Themes/RIL/js/jquery.mCustomScrollbar.concat.min.js" ></script> -->
<script src="/App_Themes/RIL/js/bootstrap/bootstrap.min.js" ></script> 
<script src="/App_Themes/RIL/js/bootstrap/bootstrap-hover-dropdown.js" ></script>
<script src="/App_Themes/RIL/js/bootstrap/bootstrap-select.min.js"></script> 
<script src="/App_Themes/RIL/js/libs/jquery.jfontsize-1.0.js"></script>
<script src="/App_Themes/RIL/js/libs/jquery.bxslider.js"></script> 
<script src="/App_Themes/RIL/js/custom.js"></script> 
<script src="/App_Themes/RIL/js/libs/jquery.onepage-scroll.js" ></script> 
<!--
<script>
     (function(jQuery){
         jQuery(window).load(function(){
           
              jQuery(".content").mCustomScrollbar();
         });
     })(jQuery);
</script>

 <script>
    jQuery(window).scroll(function () {
    myHeight = jQuery(window).scrollTop();
    if (jQuery(window).scrollTop() >= 600){

        jQuery(".certificate").addClass("search-icon");
    }
    else{
        jQuery(".certificate").removeClass("search-icon");
    }
});
   </script>
-->
<script>
jQuery(document).ready(function($){
  
 
  
  jQuery(window).on('load', function () {	
      jQuery('.selectpicker').selectpicker({
		'selectedText': 'cat'
      });
    
   
    
});
 
   jQuery("#facebookShare").click(function () {
     
      var shareUrl;
      var url = window.location.href;
      shareUrl = "https://www.facebook.com/sharer.php?u=" + url;
      window.open(shareUrl, '_blank');
    });
    
    jQuery("#twitterShare").click(function () {
      var shareUrl;
      var url = window.location.href;
      var title = document.title;
      shareUrl = "http://twitter.com/share?url=" + url + "&text=" + title;
      window.open(shareUrl, '_blank');
    });
    
    jQuery("#linkedinShare").click(function () {
      var shareUrl;
      var url = window.location.href;
      shareUrl = "http://www.linkedin.com/shareArticle?mini=true&url=" + url;
      window.open(shareUrl, '_blank');
    });
  
jQuery('.home-banner-slide').bxSlider({
		mode: 'fade',
		pager: false,
		controls:true,
        auto:true,
        autoDelay:5000
	});
  
   
		if(jQuery('body').hasClass('our-story')) {
		jQuery('html').addClass('our-head-story');
	}
	setTimeout(function(){
		jQuery(".overlay-menu").height(jQuery("body").height()-140);
		},1000);

	jQuery(".dropdown").hover(function(){
		jQuery(".overlay-menu").fadeIn(0);
		},function(){
		jQuery(".overlay-menu").fadeOut(0);
		});
      jQuery(".story-main").onepage_scroll({
		sectionContainer: "section.pages",
		// responsiveFallback: 600,
		beforeMove: function(index){
		},
		afterMove: function(index){
			if(jQuery('body').hasClass('LTR Gecko Gecko30 ENUS ContentBody our-story viewing-page-11'))
			 {
				 
				 var foot = document.getElementsByTagName('footer').item(0);
			     foot.innerHTML += '<style>footer{position:absolute;bottom:0px; display:block; background:#fff; z-index:999999; width:100%;}</style>';
			}
		}
	}); 
});
</script>
 <!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
 _paq.push(['enableLinkTracking']);
  (function() {
    var u="//webanalytics.ril.com/analytics/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 30]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<!-- End Piwik Code -->
<!--flexslider-->
<link rel="stylesheet" href="/App_Themes/RIL/css/flexslider.css">
<!--<link rel="stylesheet" href="css/demo.css">-->
<script defer src="/App_Themes/RIL/js/jquery.flexslider.js"></script>
  <script type="text/javascript">
    $(window).load(function(){
      $('.flexslider').flexslider({
        animation: "fade",
        start: function(slider){
          $('body').removeClass('loading');
        }
      });
    });
  </script>
<!--flexslider css end-->

  

    
    
<script type="text/javascript">
//<![CDATA[
var Page_Validators =  new Array(document.getElementById("p_lt_ctl09_wp_Subscription1_reqFieldSubscription"), document.getElementById("p_lt_ctl09_wp_Subscription1_EmailValidator"));
//]]>
</script>

<script type="text/javascript">
//<![CDATA[
var p_lt_ctl09_wp_Subscription1_reqFieldSubscription = document.all ? document.all["p_lt_ctl09_wp_Subscription1_reqFieldSubscription"] : document.getElementById("p_lt_ctl09_wp_Subscription1_reqFieldSubscription");
p_lt_ctl09_wp_Subscription1_reqFieldSubscription.controltovalidate = "p_lt_ctl09_wp_Subscription1_txtSubscription";
p_lt_ctl09_wp_Subscription1_reqFieldSubscription.errormessage = "Please enter Email ID";
p_lt_ctl09_wp_Subscription1_reqFieldSubscription.display = "Dynamic";
p_lt_ctl09_wp_Subscription1_reqFieldSubscription.validationGroup = "grpSubscribe";
p_lt_ctl09_wp_Subscription1_reqFieldSubscription.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
p_lt_ctl09_wp_Subscription1_reqFieldSubscription.initialvalue = "Email Id";
var p_lt_ctl09_wp_Subscription1_EmailValidator = document.all ? document.all["p_lt_ctl09_wp_Subscription1_EmailValidator"] : document.getElementById("p_lt_ctl09_wp_Subscription1_EmailValidator");
p_lt_ctl09_wp_Subscription1_EmailValidator.controltovalidate = "p_lt_ctl09_wp_Subscription1_txtSubscription";
p_lt_ctl09_wp_Subscription1_EmailValidator.errormessage = "Please enter valid Email ID";
p_lt_ctl09_wp_Subscription1_EmailValidator.display = "Dynamic";
p_lt_ctl09_wp_Subscription1_EmailValidator.validationGroup = "grpSubscribe";
p_lt_ctl09_wp_Subscription1_EmailValidator.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
p_lt_ctl09_wp_Subscription1_EmailValidator.validationexpression = "(Email Id)|\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
(function() {var fn = function() {$get("manScript_HiddenField").value = '';Sys.Application.remove_init(fn);};Sys.Application.add_init(fn);})();
WebForm_InitCallback();
var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        
theForm.oldSubmit = theForm.submit;
theForm.submit = WebForm_SaveScrollPositionSubmit;

theForm.oldOnSubmit = theForm.onsubmit;
theForm.onsubmit = WebForm_SaveScrollPositionOnSubmit;
Sys.Application.add_init(function() {
    $create(Sys.UI._Timer, {"enabled":true,"interval":900000,"uniqueID":"p$lt$ctl01$wp_Trading$Timer1"}, null, null, $get("p_lt_ctl01_wp_Trading_Timer1"));
});

document.getElementById('p_lt_ctl09_wp_Subscription1_reqFieldSubscription').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('p_lt_ctl09_wp_Subscription1_reqFieldSubscription'));
}

document.getElementById('p_lt_ctl09_wp_Subscription1_EmailValidator').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('p_lt_ctl09_wp_Subscription1_EmailValidator'));
}
//]]>
</script>
<input type="hidden" name="as_sfid" value="AAAAAAXtU7kZJREnYn5Q2AVin5w3vEt38aoT-Ovhy5sXomlspUMKbyEn8YGHeprPTehikcRywOBqP48jUwQ9Z-lcE1pKzVqWa4O6wLUWwnJEt_BQnl8X00OgvR-fnRi7Ga6cnPw=" /><input type="hidden" name="as_fid" value="503bb5a05da9d0565ea0e32b2119ba850c7bfd3b" /></form>
</body>
</html>
