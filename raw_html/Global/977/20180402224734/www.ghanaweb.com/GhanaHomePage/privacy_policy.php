<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Privacy Policy GhanaWeb</title>
<link rel="shortcut icon" href="https://cdn.ghanaweb.com/design/favicon.ico" type="image/x-icon">
<link href="https://cdn.ghanaweb.com/css/GH/v3.4.43/common.css" rel="stylesheet" type="text/css" media="screen">
<link href="https://cdn.ghanaweb.com/css/GH/v3.4.43/print.css" rel="stylesheet" type="text/css" media="print">
<link rel="apple-touch-icon" href="https://cdn.ghanaweb.com/design/logo_ghanaweb.png">
<link rel="apple-touch-icon" href="https://cdn.ghanaweb.com/design/apple-touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="72x72" href="https://cdn.ghanaweb.com/design/apple-touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="114x114" href="https://cdn.ghanaweb.com/design/apple-touch-icon-iphone4.png">
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://mobile.ghanaweb.com/GhanaHomePage/privacy_policy.php"/>

</head>
<!-- Start Alexa !! Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"Rv2Th1aon800ah", domain:"www.ghanaweb.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Rv2Th1aon800ah" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<body><div id="pagecontainer">
				<div id="logo">
			<div id="thelogo">
				<a href="https://www.ghanaweb.com/GhanaHomePage"><img src="https://cdn.ghanaweb.com/design/logo_desktop2.png" /></a>
			</div>

		


		<div id="mainnav">
			<div id="accessmenu">
				<a href="displaypreference.php?url=www.ghanaweb.com%2FGhanaHomePage%2Fprivacy_policy.php" rel="nofollow">Display options</a>
				<a href="https://mobile.ghanaweb.com/" class="last">Mobile website</a>
			</div>


			
			<div id="loginForm">
				<div id="loginFormLinks">
											<a href="https://members.ghanaweb.com/private/register.php">Sign up</a>  <a href="https://members.ghanaweb.com/private/login.php" class="logIn">Login</a>
									</div>

				<div  id="loginFormFields" style="display:none">
											<form action="https://members.ghanaweb.com/private/login.php?mode=login" method="post">
							<div id="loginem"><input type="text" name="email" id="email" size="12"/> </div>
							<div id="loginpas"> <input type="password" name="password" id="password" size="10"/></div>
							<div id="loginbtndiv"><input type="submit" value="Login" name="login" id="loginbtn" /></div>
						</form>
						<div id="newto"><a href="https://members.ghanaweb.com/private/register.php">Sign up</a> | <a href="https://members.ghanaweb.com/private/forgotten.php">forgot your password</a></div>
					

					<form action="https://members.ghanaweb.com/private/login.php?mode=login" method="post">
						<div id="loginem">
							<label>Username</label><input type="text" name="email" id="email" size="12"/>
						</div>
						<div id="loginpas">
							<label>Password</label><input type="password" name="password" id="password" size="10"/>
						</div>

						<div id="newto"><a href="https://members.ghanaweb.com/private/forgotten.php">Forgot your password</a></div>
						<div id="loginbtndiv">
							<input type="submit" value="Login" name="login" id="loginbtn" />
						</div>
					</form>
				</div>
			</div>

			
			<div id="socialMedia">
			    <a href="https://www.youtube.com/channel/UCsL03z5sLvA0t8sjwDWf_3A" class="yt" target="_blank">Youtube Icon</a>
				<a href="https://www.facebook.com/GhanaHomePage" class="fb" target="_blank">Facebook Icon</a>
				<a href="https://twitter.com/theghanaweb" class="tw" target="_blank">Twitter Icon</a>
				<!--<a href="#" class="gp">Google Plus Icon</a>-->
							</div>

			


			<div id="radioIcon"><a class="en" href="https://radio.ghanaweb.com/live-radio.test.php" target="new" title="Click here for Radio">Click here for Radio</a></div>

			<div id="topnav" class="topnav-en">
				You are here: <a href="https://www.ghanaweb.com/GhanaHomePage">Home</a> &rarr; <b>Privacy Policy</b>			</div>



      <ul id="mainnavinner" class="nav-en">
        <li class="navNews"><a href="https://www.ghanaweb.com/GhanaHomePage/NewsArchive/">News</a></li>
        <li class="navSports"><a href="https://www.ghanaweb.com/GhanaHomePage/SportsArchive/">Sports</a></li>
        <li class="navBusiness"><a href="https://www.ghanaweb.com/GhanaHomePage/business/">Business</a></li>
        <li class="navEntertainment"><a href="https://www.ghanaweb.com/GhanaHomePage/entertainment/">Entertainment</a></li>

        					<li class="navWorld"><a href="https://www.ghanaweb.com/GhanaHomePage/world/">World</a></li>
				
				<li class="navCountry"><a href="https://www.ghanaweb.com/GhanaHomePage/country_information/">Country</a></li>
        <li class="navOpinions"><a href="https://www.ghanaweb.com/GhanaHomePage/opinions/">Opinions</a></li>
        <li class="navMembers last"><a href="https://members.ghanaweb.com/">Members</a></li>
      </ul>
    </div>
  </div>


	<div id="banner">
		<div id="gw_leaderboard">
			<div id="537376500" style="width:728px;height:90px;margin:0;padding:0">
		  <noscript><iframe id="90d7d30867" name="90d7d30867" src="//ghanaweb-d.openx.net/w/1.0/afr?auid=537376500&cb=152171327660571" frameborder="0" scrolling="no" width="728" height="90"><a href="//ghanaweb-d.openx.net/w/1.0/rc?cs=90d7d30867&cb=152171327660571" ><img src="//ghanaweb-d.openx.net/w/1.0/ai?auid=537376500&cs=90d7d30867&cb=152171327660571" border="0" alt=""></a></iframe></noscript>
		</div>
		<script type="text/javascript">
		  var OX_ads = OX_ads || [];
		  OX_ads.push({
		     slot_id: "537376500",
		     auid: "537376500"
		  });
		</script>
		<script type="text/javascript" src="//ghanaweb-d.openx.net/w/1.0/jstag"></script>		</div>
		<div id="search">
			<form action="https://ghanaweb.com/GhanaHomePage/search.php" method="post">
				<input type="hidden" name="action" value="1">
				<div id="searchbox">
					<h5>Search GhanaWeb</h5>
					<input type="text" name="Search" id="searchbox1" value="" size="27"/>
				</div>
				<div class="srcType">
					<input type="radio" name="search_mode" value="general" id="searchPortal" checked="checked" /> <label for="searchPortal">Website</label>
				</div>
				<div class="srcType">
					<input type="radio" name="search_mode" value="news" id="searchNews" /> <label for="searchNews">News Archive</label>
				</div>
				<div id="searchbutton">
					<input type="submit" value="Search" id="searchbtn" />
				</div>
			</form>

		</div>
  </div>

  <div id="mainbody">

	     		     <div id="leftsection">
<div class="menuheading">GhanaWeb</div>
<ul>
	<li><a href="aboutus.php">About Us</a></li>
	<li><a href="contact.php">Contact Us</a></li>
	<li><a href="partners.php">Partners</a></li>
	<li><a href="sitemap.php">Sitemap</a></li>
</ul>
<div class="menuheading">Services</div>
<ul>
	<li><a href="accessibility.php">Accessibility</a></li>
  <li><a href="advertising.php">Advertising</a></li>
	<li><a href="rss_feeds.php">RSS Feeds</a></li>
	<li><a href="webhosting.php">Webhosting</a></li>
</ul>
<div class="menuheading">Legal Matters</div>
<ul>
	<li><a href="privacy_policy.php">Privacy Policy</a></li>
	<li><a href="disclaimer.php">Disclaimer</a></li>
</ul>

<div class="menuheading">Advertisement</div>
<div id="navigation_advert">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<script type="text/javascript"><!--
google_override_format = true;
google_ad_client = "pub-1385374189532140";
google_ad_slot = "HOMEPAGE_GOOGLE_AD_SLOT_LEFT";
google_ad_width = 170;
google_ad_height = 290;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

</div>

</div>
<div id="medsection1">
<!-- End of html generated by this->header() -->
<h1>Privacy Policy</h1>

<p>GhanaWeb and its advertisers are committed to protecting the privacy of Internet users.</p>

<h2>Collection of personally identifiable information for memberships</h2>

<p>Unless you register yourself GhanaWeb does not collect personally identifiable information about you, such as your email address, name, age, etc. 
To register yourself is optional. You can access most of the content of GhanaWeb anonymously without registering yourself.</p>

<p>You must be 18 years or older before you can register yourself.</p>

<p>Your personally identifiable information is not sold to third parties.</p>

<p>Your personally identifiable information will be used to facilitate the communication between you and other members of GhanaWeb. 
You can determine yourself how if and how much personal information you want to share with the other visitors of GhanaWeb.</p>

<h2>Collection of non personally identifiable information</h2>

<p>GhanaWeb does obtain some types of non personally identifiable information such as the ip address you use to connect to the Internet, your browser type, or the type of computer operating system you use (Macintosh or Windows, for example). This information is used to improve the website by   for example   analysing which pages are popular and which pages need to be improved. </p>

</p>The collected information is not sold to third parties.</p>

<h2>Usage of cookies by GhanaWeb</h2>

<p>GhanaWeb uses <a href="http://www.cookiecentral.com/faq/"> cookies </a> to store information about the country and city from which you are surfing. This information is used to customise the website   content as well as advertisements   according to your geo graphical location.</p>

<p>GhanaWeb uses cookies to remember you screen name when commenting on news articles and when joining the chatroom. Without these cookies you would need to re type your screen name all the time.</p>

<p>GhanaWeb uses a cookie for the Say It Loud forum to determine whether you are a registered user. Without this cookie it will not be possible to automatically give you the privileges of a registered user.</p>

<p>In all cases you can stop this usage of cookies by GhanaWeb by disabling cookies in your browser. In the help section of your browser you will also find information about how to remove existing cookies from GhanaWeb from your computer.</p>

<p>In general you can use GhanaWeb without cookies although it might reduce the user friendliness of the website.</p>


<h2>Usage of cookies by Advertisers</h2>

<p>GhanaWeb runs advertisements from various renowned advertising networks, such as <a href="http://www.valueclickmedia.com/"> Valueclick Media </a>, Google and <a href="http://www.adconion.com/"> Adconion </a>. Their usage of cookies is governed by the individual privacy policy of the advertising network in question.

<p>A good resource to learn more about your consumer privacy in relation to these advertising networks is the <a href="http://www.networkadvertising.org/"> Network Advertising Initiative (NAI) </a>. On this website you can opt out from Behavioral Advertising for all the advertising networks which are member of the NAI.</p>





<!-- Start of html generated by this->footer() -->
</div>
<div id="rightsection"><div class="rightNavSection" style="height:600px; margin-bottom:10px; clear:both;">
<div id="537376523" style="width:160px;height:600px;margin:0;padding:0">
		  <noscript><iframe id="e630eaf2b1" name="e630eaf2b1" src="//ghanaweb-d.openx.net/w/1.0/afr?auid=537376523&cb=152171327628585" frameborder="0" scrolling="no" width="160" height="600"><a href="//ghanaweb-d.openx.net/w/1.0/rc?cs=e630eaf2b1&cb=152171327628585" ><img src="//ghanaweb-d.openx.net/w/1.0/ai?auid=537376523&cs=e630eaf2b1&cb=152171327628585" border="0" alt=""></a></iframe></noscript>
		</div>
		<script type="text/javascript">
		  var OX_ads = OX_ads || [];
		  OX_ads.push({
		     slot_id: "537376523",
		     auid: "537376523"
		  });
		</script>
		<script type="text/javascript" src="//ghanaweb-d.openx.net/w/1.0/jstag"></script></div>
<iframe src="https://tech.ghanaweb.com/TheGreensWidget/widget.14.php" id=“theGreens" frameborder="0" style="float:left; height: 348px; width: 160px; padding: 0; margin-bottom: 10px; margin-top: 10px;" scrolling="no"></iframe>
<div style="clear:both;"></div><style type="text/css" media="screen">
.cpcpop {visibility:hidden;width:445px;position:absolute;top:100px;left:100px; z-index:10;}
.cpcpop .inner{width:435px;margin:5px;}
.cpcpop .inner img.cntrl {position:absolute;right:-20px; top:-20px;}
.popup-box{background:#fff;padding:10px 20px;border:10px solid #7F961C;float:left;width:480px;position:fixed;top:30%;left:50%;margin:0 0 0 -250px;z-index:100;}
.popup-box .share-top{float:left;margin-bottom:5px;width:480px;}
.popup-box h3{border:0;float:left;width:480px;margin:5px 0 7px 10px;}
.icon-email {background: url('https://cdn.ghanaweb.com/design/cpc_sprite2.jpg') no-repeat 0 0;width: 58px;height: 29px;}
.icon-phone {background: url('https://cdn.ghanaweb.com/design/cpc_sprite2.jpg') no-repeat -29px 0px;width: 58px;height: 29px;}
#cpcicon_box{width:29px; height:29px;}
</style>
<script>

function openCPCPOP(cpc_box_id) {
	document.getElementById('cpcpopupbox').style.visibility = 'visible';
	updatecpcpopup(cpc_box_id);
}

function closeCPCPOP(cpc_box_id) {
	document.getElementById('cpcpopupbox').style.visibility = 'hidden';
}

function updatecpcpopup(cpc_box_id)
{
	document.getElementById('cpcpopup_display').innerHTML='Loading...';
	var xmlhttp;
  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
 	 xmlhttp=new XMLHttpRequest();
  }
	else
  {// code for IE6, IE5
  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
	xmlhttp.onreadystatechange=function()
  {
  	if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    	//response
    	document.getElementById('cpcpopup_display').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","https://ghanaweb.com/cpcpopup.php?token="+cpc_box_id,true);
xmlhttp.send();
}
</script><div id="cpcpopupbox" class="cpcpop">
									  <div class="popup-box">
									  	<div class="inner">
									     	<a href="javascript:closeCPCPOP()"><img src="https://cdn.ghanaweb.com/design/icon_closex.png" class="cntrl" title="Close" border="0" /></a>
									     	<div id="cpcpopup_display"></div>
								    	</div>
									  </div>
									</div><script language="Javascript" type="text/javascript" src="https://cdn.ghanaweb.com/jscript/v2.13/cpc.scroll3.js"></script>
<div class="selfad" style="width:160px; overflow:hidden; position:relative; background:#9a9a9a; margin-bottom:10px"><a href="https://members.ghanaweb.com/cpc_campaign" style="font-size:14px; display:block; text-align:center; background:#9a9a9a; padding:5px 0; color:#fff"><strong>GhanaWeb</strong><br/>Self Adverts</a>
							<div class="cover" style="position:absolute; background:#9a9a9a; height:700px; width:160px; z-index:1; left:0; opacity:0; display:none; "></div><div id="scroller"><ul style="float:left; margin:0"><li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10538" ><strong>WE RENT,SEW,UNIFORMS</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1521143738.jpg" title="WE RENT,SEW,UNIFORMS" alt="WE RENT,SEW,UNIFORMS" width="54" height="54">
						  GRADUATION GOWN,CASSOCK..
0244633060</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10689" ><strong>BAD BREATH TREATMENT</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1519312465.jpg" title="BAD BREATH TREATMENT" alt="BAD BREATH TREATMENT" width="54" height="54">
						  Treat Mouth Odor Naturally, Call 0273248602...</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10553" ><strong>Thesis & Dissertation</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/no-img.jpg" title="Thesis & Dissertation" alt="Thesis & Dissertation" width="54" height="54">
						  Guidance for PhD, Masters,1st Degree, Turnitin</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10541" ><strong>WOMAN,WHEN PILLS AND </strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1521144396.jpg" title="WOMAN,WHEN PILLS AND " alt="WOMAN,WHEN PILLS AND " width="54" height="54">
						  SURGERY FAILS, TRY HERBS.+233244633060</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10580" ><strong>SUPERNATURAL POWERS.</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1521550827.jpg" title="SUPERNATURAL POWERS." alt="SUPERNATURAL POWERS." width="54" height="54">
						  LOVE,MONEY,PROTECTION,BUSIESS,LUCK,TRAVEL,ETC.</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10614" ><strong>FOCOS Dinner Dance</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1522025100.jpg" title="FOCOS Dinner Dance" alt="FOCOS Dinner Dance" width="54" height="54">
						  FOCOS dinner dance - May 26th at Hanover Manor</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10668" ><strong>Thesis & Dissertation</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1522166927.png" title="Thesis & Dissertation" alt="Thesis & Dissertation" width="54" height="54">
						  Guidance for PhD, Masters,1st Degree, Turnitin</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10692" ><strong>TREAT BAD BREATH NOW!!</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1519314324.jpg" title="TREAT BAD BREATH NOW!!" alt="TREAT BAD BREATH NOW!!" width="54" height="54">
						  Treat Mouth Odor Naturally, Call 0273248602...</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10632" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1522150744.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assistance for Master's and PhD Thesis</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10686" ><strong>BAD BREATH 100% REMEDY</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1520109649.jpg" title="BAD BREATH 100% REMEDY" alt="BAD BREATH 100% REMEDY" width="54" height="54">
						  Treat Mouth Odor Naturally, Call 0273248602...</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10626" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1522150685.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assistance for Master's and PhD Thesis</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10665" ><strong>Thesis & Dissertation</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1522166904.png" title="Thesis & Dissertation" alt="Thesis & Dissertation" width="54" height="54">
						  Guidance for PhD, Masters,1st Degree, Turnitin</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10662" ><strong>Thesis & Dissertation</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1522166826.png" title="Thesis & Dissertation" alt="Thesis & Dissertation" width="54" height="54">
						  Guidance for PhD, Masters,1st Degree, Turnitin</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10499" ><strong>ROOMS FOR SHORT STAY</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1506077716.jpg" title="ROOMS FOR SHORT STAY" alt="ROOMS FOR SHORT STAY" width="54" height="54">
						  COMFORTABLE,AFFORDABLE ROOMS FOR RENT AT LEGON</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10577" ><strong>Who Say Man No Dey</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1521543475.png" title="Who Say Man No Dey" alt="Who Say Man No Dey" width="54" height="54">
						  How Men Can Last Longer in Bed n Stop PE & ED</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10623" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1522150656.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assistance for Master's and PhD Thesis</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10550" ><strong>Thesis & Dissertation</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/no-img.jpg" title="Thesis & Dissertation" alt="Thesis & Dissertation" width="54" height="54">
						  Guidance for PhD, Masters,1st Degree, Turnitin</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10514" ><strong>Struggling 2 Conceive?</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1520610365.png" title="Struggling 2 Conceive?" alt="Struggling 2 Conceive?" width="54" height="54">
						  Infertility or Miscarriage Problems ??
</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="https://members.ghanaweb.com/cpc_campaign/redirect.php?token=10629" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="https://cdn.ghanaweb.com/cpc_campaign/1522150715.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assistance for Master's and PhD Thesis</a></li>
</ul></div>
					<a href="https://members.ghanaweb.com/cpc_campaign"><img src="https://cdn.ghanaweb.com/cpc_campaign/banner_sm9.gif" style="margin:0"></a></div>
</div>

<div id="footer">
	<ul id="footerinner">
    	<li class="fstyle"><a href="https://ghanaweb.com/GhanaHomePage/aboutus.php">About Us</a></li>
        <li class="fstyle"><a href="https://ghanaweb.com/advertise">Advertising</a></li>
        <li class="fstyle"><a rel="nofollow" href="https://ghanaweb.com/GhanaHomePage/feedback.php?url=www.ghanaweb.com/GhanaHomePage/privacy_policy.php" onclick="NewWindow('https://ghanaweb.com/GhanaHomePage/feedback.php?url=www.ghanaweb.com/GhanaHomePage/privacy_policy.php', 'feedback', '550', '450','no') ;return false;">Feedback</a></li>
        <li class="fstyle"><a href="https://ghanaweb.com/GhanaHomePage/sitemap.php">Sitemap</a></li>
        <li class="fstyle last"><a href="https://ghanaweb.com/GhanaHomePage/jobs.php">Jobs</a></li>
    </ul>
    <div id="footerrechts">
    	Copyright &copy; 1994 - 2018 GhanaWeb. All rights reserved.    </div>
</div>
</div>

</div>



</div></div>

<!-- REF: https://www.ghanaweb.com/GhanaHomePage/ ::: MBR: f9da5a09d4a1ccb38fbb3944c530e1f1 --><!--2018-04-02	22	12,13,166,171	US	22:47:34	2	1	2147	207.241.226.209
--></body>
</html>
<script language="Javascript" type="text/javascript" src="https://cdn.ghanaweb.com/jscript/v2.13/basic.js"></script>
<!-- Begin Cookie Consent --><script type="text/javascript">window.cookieconsent_options = {"message":"By continuing to use the site, you agree to the use of cookies. You can find out more by following","dismiss":"Accept Cookies","learnMore":"this link","link":"/GhanaHomePage/privacy_policy.php","theme":"light-bottom"}</script><script type="text/javascript" src="https://cdn.ghanaweb.com/jscript/v2.13/cookieconsent.min.js?v=2.0"></script><!-- End Cookie Consent plugin --><!-- Generated in 0.006 seconds -->
