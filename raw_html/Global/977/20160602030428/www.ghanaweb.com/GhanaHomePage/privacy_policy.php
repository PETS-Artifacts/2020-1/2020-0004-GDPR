<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Privacy Policy GhanaWeb</title>
<link rel="shortcut icon" href="http://cdn.ghanaweb.com/design/favicon.ico" type="image/x-icon">
<link href="http://cdn.ghanaweb.com/css/GH/v3.4.21/common.css" rel="stylesheet" type="text/css" media="screen">
<link href="http://cdn.ghanaweb.com/css/GH/v3.4.21/desktop.css" rel="stylesheet" type="text/css" media="screen">
<link href="http://cdn.ghanaweb.com/css/GH/v3.4.21/print.css" rel="stylesheet" type="text/css" media="print">
<link rel="apple-touch-icon" href="http://cdn.ghanaweb.com/design/logo_ghanaweb.png">
<link rel="apple-touch-icon" href="http://cdn.ghanaweb.com/design/apple-touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="72x72" href="http://cdn.ghanaweb.com/design/apple-touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="114x114" href="http://cdn.ghanaweb.com/design/apple-touch-icon-iphone4.png">
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://mobile.ghanaweb.com/GhanaHomePage/privacy_policy.php"/>

</head>
<!-- Start Alexa !! Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"Rv2Th1aon800ah", domain:"ghanaweb.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Rv2Th1aon800ah" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<body><div id="pagecontainer">
				<div id="logo">
			<div id="thelogo">
				<a href="http://www.ghanaweb.com/GhanaHomePage"><img src="http://cdn.ghanaweb.com/design/logo_desktop2.png?site=GH" /></a>
			</div>

		


		<div id="mainnav">
			<div id="accessmenu">
				<a href="displaypreference.php?url=www.ghanaweb.com%2FGhanaHomePage%2Fprivacy_policy.php" rel="nofollow">Display options</a>
				<a href="http://mobile.ghanaweb.com/" class="last">Mobile website</a>
			</div>


			
			<div id="loginForm">
				<div id="loginFormLinks">
											<a href="http://members.ghanaweb.com/private/register.php">Sign up</a>  <a href="http://members.ghanaweb.com/private/login.php" class="logIn">Login</a>
									</div>

				<div  id="loginFormFields" style="display:none">
											<form action="http://members.ghanaweb.com/private/login.php?mode=login" method="post">
							<div id="loginem"><input type="text" name="email" id="email" size="12"/> </div>
							<div id="loginpas"> <input type="password" name="password" id="password" size="10"/></div>
							<div id="loginbtndiv"><input type="submit" value="Login" name="login" id="loginbtn" /></div>
						</form>
						<div id="newto"><a href="http://members.ghanaweb.com/private/register.php">Sign up</a> | <a href="http://members.ghanaweb.com/private/forgotten.php">forgot your password</a></div>
					

					<form action="http://members.ghanaweb.com/private/login.php?mode=login" method="post">
						<div id="loginem">
							<label>Username</label><input type="text" name="email" id="email" size="12"/>
						</div>
						<div id="loginpas">
							<label>Password</label><input type="password" name="password" id="password" size="10"/>
						</div>

						<div id="newto"><a href="http://members.ghanaweb.com/private/forgotten.php">Forgot your password</a></div>
						<div id="loginbtndiv">
							<input type="submit" value="Login" name="login" id="loginbtn" />
						</div>
					</form>
				</div>
			</div>

			
			<div id="socialMedia">
				<a href="https://www.facebook.com/GhanaHomePage" class="fb" target="_blank">Facebook Icon</a>
				<a href="https://twitter.com/theghanaweb" class="tw" target="_blank">Twitter Icon</a>
				<!--<a href="#" class="gp">Google Plus Icon</a>-->
							</div>

			


			<div id="radioIcon"><a class="en" href="http://radio.ghanaweb.com/live-radio.test.php" target="new" title="Click here for Radio">Click here for Radio</a></div>

			<div id="topnav" class="topnav-en">
				You are here: <a href="http://www.ghanaweb.com/GhanaHomePage">Home</a> &rarr; <b>Privacy Policy</b>			</div>



      <ul id="mainnavinner" class="nav-en">
        <li class="navNews"><a href="http://www.ghanaweb.com/GhanaHomePage/NewsArchive/">News</a></li>
        <li class="navSports"><a href="http://www.ghanaweb.com/GhanaHomePage/SportsArchive/">Sports</a></li>
        <li class="navBusiness"><a href="http://www.ghanaweb.com/GhanaHomePage/business/">Business</a></li>
        <li class="navEntertainment"><a href="http://www.ghanaweb.com/GhanaHomePage/entertainment/">Entertainment</a></li>

        					<li class="navWorld"><a href="http://www.ghanaweb.com/GhanaHomePage/world/">World</a></li>
				
				<li class="navCountry"><a href="http://www.ghanaweb.com/GhanaHomePage/country_information/">Country</a></li>
        <li class="navOpinions"><a href="http://www.ghanaweb.com/GhanaHomePage/opinions/">Opinions</a></li>
        <li class="navMembers last"><a href="http://members.ghanaweb.com/">Members</a></li>
      </ul>
    </div>
  </div>


	<div id="banner">
		<div id="gw_leaderboard">
			<div id="537376500" style="width:728px;height:90px;margin:0;padding:0">
		  <noscript><iframe id="90d7d30867" name="90d7d30867" src="//ghanaweb-d.openx.net/w/1.0/afr?auid=537376500&cb=146252114547041" frameborder="0" scrolling="no" width="728" height="90"><a href="//ghanaweb-d.openx.net/w/1.0/rc?cs=90d7d30867&cb=146252114547041" ><img src="//ghanaweb-d.openx.net/w/1.0/ai?auid=537376500&cs=90d7d30867&cb=146252114547041" border="0" alt=""></a></iframe></noscript>
		</div>
		<script type="text/javascript">
		  var OX_ads = OX_ads || [];
		  OX_ads.push({
		     slot_id: "537376500",
		     auid: "537376500"
		  });
		</script>
		<script type="text/javascript" src="//ghanaweb-d.openx.net/w/1.0/jstag"></script>		</div>
		<div id="search">
			<form action="http://www.ghanaweb.com/GhanaHomePage/search.php" method="get">
				<input type="hidden" name="action" value="1">
				<div id="searchbox">
					<h5>Search GhanaWeb</h5>
					<input type="text" name="Search" id="searchbox1" value="" size="27"/>
				</div>
				<div class="srcType">
					<input type="radio" name="search_mode" value="general" id="searchPortal" checked="checked" /> <label for="searchPortal">Website</label>
				</div>
				<div class="srcType">
					<input type="radio" name="search_mode" value="news" id="searchNews" /> <label for="searchNews">News Archive</label>
				</div>
				<div id="searchbutton">
					<input type="submit" value="Search" id="searchbtn" />
				</div>
			</form>

		</div>
  </div>


  <div id="mainbody">

	     		     <div id="leftsection">
<div class="menuheading">GhanaWeb</div>
<ul>
	<li><a href="aboutus.php">About Us</a></li>
	<li><a href="contact.php">Contact Us</a></li>
	<li><a href="partners.php">Partners</a></li>
	<li><a href="sitemap.php">Sitemap</a></li>
</ul>
<div class="menuheading">Services</div>
<ul>
	<li><a href="accessibility.php">Accessibility</a></li>
	<li><a href="advertising.php">Advertising</a></li>
	<li><a href="rss_feeds.php">RSS Feeds</a></li>
	<li><a href="webhosting.php">Webhosting</a></li>
</ul>
<div class="menuheading">Legal Matters</div>
<ul>
	<li><a href="privacy_policy.php">Privacy Policy</a></li>
	<li><a href="disclaimer.php">Disclaimer</a></li>
</ul>

<div class="menuheading">Advertisement</div>
<div id="navigation_advert">
<script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<script type="text/javascript"><!--
google_override_format = true;
google_ad_client = "pub-1385374189532140";
google_ad_slot = "HOMEPAGE_GOOGLE_AD_SLOT_LEFT";
google_ad_width = 170;
google_ad_height = 290;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

</div>

</div>
<div id="medsection1">
<!-- End of html generated by this->header() -->
<h1>Privacy Policy</h1>

<p>GhanaWeb and its advertisers are committed to protecting the privacy of Internet users.</p>

<h2>Collection of personally identifiable information for memberships</h2>

<p>Unless you register yourself GhanaWeb does not collect personally identifiable information about you, such as your email address, name, age, etc. 
To register yourself is optional. You can access most of the content of GhanaWeb anonymously without registering yourself.</p>

<p>You must be 18 years or older before you can register yourself.</p>

<p>Your personally identifiable information is not sold to third parties.</p>

<p>Your personally identifiable information will be used to facilitate the communication between you and other members of GhanaWeb. 
You can determine yourself how if and how much personal information you want to share with the other visitors of GhanaWeb.</p>

<h2>Collection of non personally identifiable information</h2>

<p>GhanaWeb does obtain some types of non personally identifiable information such as the ip address you use to connect to the Internet, your browser type, or the type of computer operating system you use (Macintosh or Windows, for example). This information is used to improve the website by   for example   analysing which pages are popular and which pages need to be improved. </p>

</p>The collected information is not sold to third parties.</p>

<h2>Usage of cookies by GhanaWeb</h2>

<p>GhanaWeb uses <a href="http://www.cookiecentral.com/faq/"> cookies </a> to store information about the country and city from which you are surfing. This information is used to customise the website   content as well as advertisements   according to your geo graphical location.</p>

<p>GhanaWeb uses cookies to remember you screen name when commenting on news articles and when joining the chatroom. Without these cookies you would need to re type your screen name all the time.</p>

<p>GhanaWeb uses a cookie for the Say It Loud forum to determine whether you are a registered user. Without this cookie it will not be possible to automatically give you the privileges of a registered user.</p>

<p>In all cases you can stop this usage of cookies by GhanaWeb by disabling cookies in your browser. In the help section of your browser you will also find information about how to remove existing cookies from GhanaWeb from your computer.</p>

<p>In general you can use GhanaWeb without cookies although it might reduce the user friendliness of the website.</p>


<h2>Usage of cookies by Advertisers</h2>

<p>GhanaWeb runs advertisements from various renowned advertising networks, such as <a href="http://www.valueclickmedia.com/"> Valueclick Media </a>, Google and <a href="http://www.adconion.com/"> Adconion </a>. Their usage of cookies is governed by the individual privacy policy of the advertising network in question.

<p>A good resource to learn more about your consumer privacy in relation to these advertising networks is the <a href="http://www.networkadvertising.org/"> Network Advertising Initiative (NAI) </a>. On this website you can opt out from Behavioral Advertising for all the advertising networks which are member of the NAI.</p>





<!-- Start of html generated by this->footer() -->
</div>
<div id="rightsection"><div class="rightNavSection" style="height:600px; margin-bottom:10px;">
<div id="537376523" style="width:160px;height:600px;margin:0;padding:0">
		  <noscript><iframe id="e630eaf2b1" name="e630eaf2b1" src="//ghanaweb-d.openx.net/w/1.0/afr?auid=537376523&cb=146252114538230" frameborder="0" scrolling="no" width="160" height="600"><a href="//ghanaweb-d.openx.net/w/1.0/rc?cs=e630eaf2b1&cb=146252114538230" ><img src="//ghanaweb-d.openx.net/w/1.0/ai?auid=537376523&cs=e630eaf2b1&cb=146252114538230" border="0" alt=""></a></iframe></noscript>
		</div>
		<script type="text/javascript">
		  var OX_ads = OX_ads || [];
		  OX_ads.push({
		     slot_id: "537376523",
		     auid: "537376523"
		  });
		</script>
		<script type="text/javascript" src="//ghanaweb-d.openx.net/w/1.0/jstag"></script></div>
<style type="text/css" media="screen">
.cpcpop {visibility:hidden;width:445px;position:absolute;top:100px;left:100px; z-index:10;}
.cpcpop .inner{width:435px;margin:5px;}
.cpcpop .inner img.cntrl {position:absolute;right:-20px; top:-20px;}
.popup-box{background:#fff;padding:10px 20px;border:10px solid #7F961C;float:left;width:480px;position:fixed;top:30%;left:50%;margin:0 0 0 -250px;z-index:100;}
.popup-box .share-top{float:left;margin-bottom:5px;width:480px;}
.popup-box h3{border:0;float:left;width:480px;margin:5px 0 7px 10px;}
.icon-email {background: url('http://cdn.ghanaweb.com/design/cpc_sprite2.jpg') no-repeat 0 0;width: 58px;height: 29px;}
.icon-phone {background: url('http://cdn.ghanaweb.com/design/cpc_sprite2.jpg') no-repeat -29px 0px;width: 58px;height: 29px;}
#cpcicon_box{width:29px; height:29px;}
</style>
<script>

function openCPCPOP(cpc_box_id) {
	document.getElementById('cpcpopupbox').style.visibility = 'visible';
	updatecpcpopup(cpc_box_id);
}

function closeCPCPOP(cpc_box_id) {
	document.getElementById('cpcpopupbox').style.visibility = 'hidden';
}

function updatecpcpopup(cpc_box_id)
{
	document.getElementById('cpcpopup_display').innerHTML='Loading...';
	var xmlhttp;
  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
 	 xmlhttp=new XMLHttpRequest();
  }
	else
  {// code for IE6, IE5
  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
	xmlhttp.onreadystatechange=function()
  {
  	if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    	//response
    	document.getElementById('cpcpopup_display').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","http://www.ghanaweb.com/cpcpopup.php?token="+cpc_box_id,true);
xmlhttp.send();
}
</script><div id="cpcpopupbox" class="cpcpop">
									  <div class="popup-box">
									  	<div class="inner">
									     	<a href="javascript:closeCPCPOP()"><img src="http://cdn.ghanaweb.com/design/icon_closex.png" class="cntrl" title="Close" border="0" /></a>
									     	<div id="cpcpopup_display"></div>
								    	</div>
									  </div>
									</div><script language="Javascript" type="text/javascript" src="http://cdn.ghanaweb.com/jscript/v2.8/cpc.scroll3.js"></script>
<div class="selfad" style="width:160px; overflow:hidden; position:relative; background:#9a9a9a; margin-bottom:10px"><a href="http://members.ghanaweb.com/cpc_campaign" style="font-size:14px; display:block; text-align:center; background:#9a9a9a; padding:5px 0; color:#fff"><strong>GhanaWeb</strong><br/>Self Adverts</a>
							<div class="cover" style="position:absolute; background:#9a9a9a; height:700px; width:160px; z-index:1; left:0; opacity:0; display:none; "></div><div id="scroller"><ul style="float:left; margin:0"><li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7238" ><strong>Subscribe to swennews</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1463417589.jpg" title="Subscribe to swennews" alt="Subscribe to swennews" width="54" height="54">
						  and get all your latest news and magazines</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7335" ><strong>LOOSE WEIGHT IN 9 DAYS</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464773757.jpg" title="LOOSE WEIGHT IN 9 DAYS" alt="LOOSE WEIGHT IN 9 DAYS" width="54" height="54">
						  LOOSE WEIGHT AND FLATTEN YOUR BELLY NATURALLY</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7232" ><strong>FREE SANDWICH MAKER</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1463381997.jpg" title="FREE SANDWICH MAKER" alt="FREE SANDWICH MAKER" width="54" height="54">
						  Massive reduction on all our kitchenware</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7264" ><strong>INSTANT CASH AVENUE</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1463696322.jpg" title="INSTANT CASH AVENUE" alt="INSTANT CASH AVENUE" width="54" height="54">
						  Earn $955 - $5000 online.Sign-up for free now.</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="javascript:void(0)" onclick="openCPCPOP('7150')"><strong>Kia Sorento 2015 model</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1461774547.jpg" title="Kia Sorento 2015 model" alt="Kia Sorento 2015 model" width="54" height="54">
						  Brand new KIA Sorento going for GHS 150,000</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7296" ><strong>Earn cash distributing</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464106374.jpg" title="Earn cash distributing" alt="Earn cash distributing" width="54" height="54">
						  Wobetekpa! Gh #1 Magazine of Humour & Comics</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7336" ><strong>FIND A HOUSE IN GHANA</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464775901.jpg" title="FIND A HOUSE IN GHANA" alt="FIND A HOUSE IN GHANA" width="54" height="54">
						  YOUR SEARCH FOR A HOUSE IN GHANA ENDS HERE!</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7327" ><strong>WEIGHT LOSS EXPERTS</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://cdn.ghanaweb.com/cpc_campaign/1435055532.jpg" title="WEIGHT LOSS EXPERTS" alt="WEIGHT LOSS EXPERTS" width="54" height="54">
						   WANNA LOSE WEIGHT IN 9DAYS 0244573792</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7333" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464763872.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assisting with Masters and PhD thesis.</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7323" ><strong>MONEY WATER. TO</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464670121.jpg" title="MONEY WATER. TO" alt="MONEY WATER. TO" width="54" height="54">
						  ATTRACT CLIENTS & CUSTOMERS
+233276544355</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7272" ><strong>HouseHelps Jobs Jordan</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1463822934.jpg" title="HouseHelps Jobs Jordan" alt="HouseHelps Jobs Jordan" width="54" height="54">
						  Call 0267-177492. Salary: 1,000 per month</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7208" ><strong>LOOSE WEIGHT IN 9 DAYS</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://cdn.ghanaweb.com/cpc_campaign/1439383432.jpg" title="LOOSE WEIGHT IN 9 DAYS" alt="LOOSE WEIGHT IN 9 DAYS" width="54" height="54">
						  LOOSE WEIGHT NATURALLY   0204107076</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7334" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464763910.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assisting with Masters and PhD thesis.</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7321" ><strong>Get Website @ GH¢300</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464646251.png" title="Get Website @ GH¢300" alt="Get Website @ GH¢300" width="54" height="54">
						  Get a website 4 your business 0249155003</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7297" ><strong>Need Love Advice?</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1457346611.png" title="Need Love Advice?" alt="Need Love Advice?" width="54" height="54">
						  Get answers to your relationship issues</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7198" ><strong>ORGANIC HILLS CLINIC</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1462802387.jpg" title="ORGANIC HILLS CLINIC" alt="ORGANIC HILLS CLINIC" width="54" height="54">
						  HEALING THROUGH ORGANIC SUPER FOODS.  VISIT US</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7310" ><strong>Job Vacancy Conference</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464458012.jpg" title="Job Vacancy Conference" alt="Job Vacancy Conference" width="54" height="54">
						  Attend the Mentorpreneur Conference 2016. RSVP</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7186" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1462530067.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assisting with Masters and PhD thesis.</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7312" ><strong>Deliverance Minister</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464462461.gif" title="Deliverance Minister" alt="Deliverance Minister" width="54" height="54">
						  Become a Deliverance Minister. CLICK HERE</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=7317" ><strong>SACK YOUR BOSS!</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1464088963.jpg" title="SACK YOUR BOSS!" alt="SACK YOUR BOSS!" width="54" height="54">
						  MAKE GH? 10,000-20,000 EVERY MONTH, LIMITED!</a></li>
</ul></div>
					<a href="http://members.ghanaweb.com/cpc_campaign"><img src="http://cdn.ghanaweb.com/cpc_campaign/banner_sm9.gif" style="margin:0"></a></div>
</div>

<div id="footer">
	<ul id="footerinner">
    	<li class="fstyle"><a href="http://www.ghanaweb.com/GhanaHomePage/aboutus.php">About Us</a></li>
        <li class="fstyle"><a href="http://www.ghanaweb.com/advertise">Advertising</a></li>
        <li class="fstyle"><a rel="nofollow" href="http://www.ghanaweb.com/GhanaHomePage/feedback.php?url=www.ghanaweb.com/GhanaHomePage/privacy_policy.php" onclick="NewWindow('http://www.ghanaweb.com/GhanaHomePage/feedback.php?url=www.ghanaweb.com/GhanaHomePage/privacy_policy.php', 'feedback', '550', '450','no') ;return false;">Feedback</a></li>
        <li class="fstyle last"><a href="http://www.ghanaweb.com/GhanaHomePage/sitemap.php">Sitemap</a></li>
    </ul>
    <div id="footerrechts">
    	Copyright &copy; 1994 - 2016 GhanaWeb. All rights reserved.    </div>
</div>
</div>

</div>
<script type="text/javascript">
		   var _sf_async_config = { uid: 56378, domain: 'ghanaweb.com', useCanonical: true, sections: 'GhanaHomePage', authors:''};
		  (function() {
		    function loadChartbeat() {
		      window._sf_endpt = (new Date()).getTime();
		      var e = document.createElement('script');
		      e.setAttribute('language', 'javascript');
		      e.setAttribute('type', 'text/javascript');
		      e.setAttribute('src','//static.chartbeat.com/js/chartbeat.js');
		      document.body.appendChild(e);
		    };
		    var oldonload = window.onload;
		    window.onload = (typeof window.onload != 'function') ?
		      loadChartbeat : function() { oldonload(); loadChartbeat(); };
		  })();
		</script>

</div></div>

<!-- REF: http://www.ghanaweb.com/GhanaHomePage/ ::: MBR: fb1a15494ba595a537558dbb249d8d31 --><!--2016-06-02	03	12,13,166	US	03:04:28	2	1		207.241.229.102
--></body>
</html>
<script language="Javascript" type="text/javascript" src="http://cdn.ghanaweb.com/jscript/v2.8/basic.js"></script>
<!-- Begin Cookie Consent --><script type="text/javascript">window.cookieconsent_options = {"message":"By continuing to use the site, you agree to the use of cookies. You can find out more by following","dismiss":"Accept Cookies","learnMore":"this link","link":"/GhanaHomePage/privacy_policy.php","theme":"light-bottom"}</script><script type="text/javascript" src="http://cdn.ghanaweb.com/jscript/v2.8/cookieconsent.min.js?v=2.0"></script><!-- End Cookie Consent plugin --><!-- Generated in 0.007 seconds -->
