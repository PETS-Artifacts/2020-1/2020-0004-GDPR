<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Privacy Policy GhanaWeb</title>
<link rel="shortcut icon" href="http://cdn.ghanaweb.com/design/favicon.ico" type="image/x-icon">
<link href="http://cdn.ghanaweb.com/css/GH/v3.4.19/common.css" rel="stylesheet" type="text/css" media="screen">
<link href="http://cdn.ghanaweb.com/css/GH/v3.4.19/desktop.css" rel="stylesheet" type="text/css" media="screen">
<link href="http://cdn.ghanaweb.com/css/GH/v3.4.19/print.css" rel="stylesheet" type="text/css" media="print">
<link rel="apple-touch-icon" href="http://cdn.ghanaweb.com/design/logo_ghanaweb.png">
<link rel="apple-touch-icon" href="http://cdn.ghanaweb.com/design/apple-touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="72x72" href="http://cdn.ghanaweb.com/design/apple-touch-icon-ipad.png">
<link rel="apple-touch-icon" sizes="114x114" href="http://cdn.ghanaweb.com/design/apple-touch-icon-iphone4.png">
<script src="http://www.ghanaweb.com/banner/advert.js" type="text/javascript"></script>
<link rel="alternate" media="only screen and (max-width: 640px)" href="http://mobile.ghanaweb.com/GhanaHomePage/privacy_policy.php"/>

</head>
<!-- Start Alexa !! Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"Rv2Th1aon800ah", domain:"ghanaweb.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=Rv2Th1aon800ah" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript -->

<body><div id="pagecontainer">
				<div id="logo">
			<div id="thelogo">
				<a href="http://www.ghanaweb.com/GhanaHomePage"><img src="http://cdn.ghanaweb.com/design/logo_desktop2.png?site=GH" /></a>
			</div>

		


		<div id="mainnav">
			<div id="accessmenu">
				<a href="displaypreference.php?url=www.ghanaweb.com%2FGhanaHomePage%2Fprivacy_policy.php" rel="nofollow">Display options</a>
				<a href="http://mobile.ghanaweb.com/" class="last">Mobile website</a>
			</div>


			
			<div id="loginForm">
				<div id="loginFormLinks">
											<a href="http://members.ghanaweb.com/private/register.php">Sign up</a>  <a href="http://members.ghanaweb.com/private/login.php" class="logIn">Login</a>
									</div>

				<div  id="loginFormFields" style="display:none">
											<form action="http://members.ghanaweb.com/private/login.php?mode=login" method="post">
							<div id="loginem"><input type="text" name="email" id="email" size="12"/> </div>
							<div id="loginpas"> <input type="password" name="password" id="password" size="10"/></div>
							<div id="loginbtndiv"><input type="submit" value="Login" name="login" id="loginbtn" /></div>
						</form>
						<div id="newto"><a href="http://members.ghanaweb.com/private/register.php">Sign up</a> | <a href="http://members.ghanaweb.com/private/forgotten.php">forgot your password</a></div>
					

					<form action="http://members.ghanaweb.com/private/login.php?mode=login" method="post">
						<div id="loginem">
							<label>Username</label><input type="text" name="email" id="email" size="12"/>
						</div>
						<div id="loginpas">
							<label>Password</label><input type="password" name="password" id="password" size="10"/>
						</div>

						<div id="newto"><a href="http://members.ghanaweb.com/private/forgotten.php">Forgot your password</a></div>
						<div id="loginbtndiv">
							<input type="submit" value="Login" name="login" id="loginbtn" />
						</div>
					</form>
				</div>
			</div>

			
			<div id="socialMedia">
				<a href="https://www.facebook.com/GhanaHomePage" class="fb" target="_blank">Facebook Icon</a>
				<a href="https://twitter.com/theghanaweb" class="tw" target="_blank">Twitter Icon</a>
				<!--<a href="#" class="gp">Google Plus Icon</a>-->
							</div>

			


			<div id="radioIcon"><a class="en" href="http://radio.ghanaweb.com/live-radio.test.php" target="new" title="Click here for Radio">Click here for Radio</a></div>

			<div id="topnav" class="topnav-en">
				You are here: <a href="http://www.ghanaweb.com/GhanaHomePage">Home</a> &rarr; <b>Privacy Policy</b>			</div>



      <ul id="mainnavinner" class="nav-en">
        <li class="navNews"><a href="http://www.ghanaweb.com/GhanaHomePage/NewsArchive/">News</a></li>
        <li class="navSports"><a href="http://www.ghanaweb.com/GhanaHomePage/SportsArchive/">Sports</a></li>
        <li class="navBusiness"><a href="http://www.ghanaweb.com/GhanaHomePage/business/">Business</a></li>
        <li class="navEntertainment"><a href="http://www.ghanaweb.com/GhanaHomePage/entertainment/">Entertainment</a></li>

        					<li class="navWorld"><a href="http://www.ghanaweb.com/GhanaHomePage/world/">World</a></li>
				
				<li class="navCountry"><a href="http://www.ghanaweb.com/GhanaHomePage/country_information/">Country</a></li>
        <li class="navOpinions"><a href="http://www.ghanaweb.com/GhanaHomePage/opinions/">Opinions</a></li>
        <li class="navMembers last"><a href="http://members.ghanaweb.com/">Members</a></li>
      </ul>
    </div>
  </div>


	<div id="banner">
		<div id="gw_leaderboard">
			<div id="537376500" style="width:728px;height:90px;margin:0;padding:0">
		  <noscript><iframe id="90d7d30867" name="90d7d30867" src="http://ox-d.ghanaweb.com/w/1.0/afr?auid=537376500&cb=145328511789261" frameborder="0" scrolling="no" width="728" height="90"><a href="http://ox-d.ghanaweb.com/w/1.0/rc?cs=90d7d30867&cb=145328511789261" ><img src="http://ox-d.ghanaweb.com/w/1.0/ai?auid=537376500&cs=90d7d30867&cb=145328511789261" border="0" alt=""></a></iframe></noscript>
		</div>
		<script type="text/javascript">
		  var OX_ads = OX_ads || [];
		  OX_ads.push({
		     slot_id: "537376500",
		     auid: "537376500"
		  });
		</script>
		<script type="text/javascript" src="http://ox-d.ghanaweb.com/w/1.0/jstag"></script>		</div>
		<div id="search">
			<form action="http://www.ghanaweb.com/GhanaHomePage/search.php" method="get">
				<input type="hidden" name="action" value="1">
				<div id="searchbox">
					<h5>Search GhanaWeb</h5>
					<input type="text" name="Search" id="searchbox1" value="" size="27"/>
				</div>
				<div class="srcType">
					<input type="radio" name="search_mode" value="general" id="searchPortal" checked="checked" /> <label for="searchPortal">Website</label>
				</div>
				<div class="srcType">
					<input type="radio" name="search_mode" value="news" id="searchNews" /> <label for="searchNews">News Archive</label>
				</div>
				<div id="searchbutton">
					<input type="submit" value="Search" id="searchbtn" />
				</div>
			</form>

		</div>
  </div>


  <div id="mainbody">

	     		     <div id="leftsection">
<div class="menuheading">GhanaWeb</div>
<ul>
	<li><a href="aboutus.php">About Us</a></li>
	<li><a href="contact.php">Contact Us</a></li>
	<li><a href="partners.php">Partners</a></li>
	<li><a href="sitemap.php">Sitemap</a></li>
</ul>
<div class="menuheading">Services</div>
<ul>
	<li><a href="accessibility.php">Accessibility</a></li>
	<li><a href="advertising.php">Advertising</a></li>
	<li><a href="rss_feeds.php">RSS Feeds</a></li>
	<li><a href="webhosting.php">Webhosting</a></li>
</ul>
<div class="menuheading">Legal Matters</div>
<ul>
	<li><a href="privacy_policy.php">Privacy Policy</a></li>
	<li><a href="disclaimer.php">Disclaimer</a></li>
</ul>

<div class="menuheading">Advertisement</div>
<div id="navigation_advert">
<script async src="http://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

<script type="text/javascript"><!--
google_override_format = true;
google_ad_client = "pub-1385374189532140";
google_ad_slot = "HOMEPAGE_GOOGLE_AD_SLOT_LEFT";
google_ad_width = 170;
google_ad_height = 290;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

</div>

</div>
<div id="medsection1">
<!-- End of html generated by this->header() -->
<h1>Privacy Policy</h1>

<p>GhanaWeb and its advertisers are committed to protecting the privacy of Internet users.</p>

<h2>Collection of personally identifiable information for memberships</h2>

<p>Unless you register yourself GhanaWeb does not collect personally identifiable information about you, such as your email address, name, age, etc. 
To register yourself is optional. You can access most of the content of GhanaWeb anonymously without registering yourself.</p>

<p>You must be 18 years or older before you can register yourself.</p>

<p>Your personally identifiable information is not sold to third parties.</p>

<p>Your personally identifiable information will be used to facilitate the communication between you and other members of GhanaWeb. 
You can determine yourself how if and how much personal information you want to share with the other visitors of GhanaWeb.</p>

<h2>Collection of non personally identifiable information</h2>

<p>GhanaWeb does obtain some types of non personally identifiable information such as the ip address you use to connect to the Internet, your browser type, or the type of computer operating system you use (Macintosh or Windows, for example). This information is used to improve the website by   for example   analysing which pages are popular and which pages need to be improved. </p>

</p>The collected information is not sold to third parties.</p>

<h2>Usage of cookies by GhanaWeb</h2>

<p>GhanaWeb uses <a href="http://www.cookiecentral.com/faq/"> cookies </a> to store information about the country and city from which you are surfing. This information is used to customise the website   content as well as advertisements   according to your geo graphical location.</p>

<p>GhanaWeb uses cookies to remember you screen name when commenting on news articles and when joining the chatroom. Without these cookies you would need to re type your screen name all the time.</p>

<p>GhanaWeb uses a cookie for the Say It Loud forum to determine whether you are a registered user. Without this cookie it will not be possible to automatically give you the privileges of a registered user.</p>

<p>In all cases you can stop this usage of cookies by GhanaWeb by disabling cookies in your browser. In the help section of your browser you will also find information about how to remove existing cookies from GhanaWeb from your computer.</p>

<p>In general you can use GhanaWeb without cookies although it might reduce the user friendliness of the website.</p>


<h2>Usage of cookies by Advertisers</h2>

<p>GhanaWeb runs advertisements from various renowned advertising networks, such as <a href="http://www.valueclickmedia.com/"> Valueclick Media </a>, Google and <a href="http://www.adconion.com/"> Adconion </a>. Their usage of cookies is governed by the individual privacy policy of the advertising network in question.

<p>A good resource to learn more about your consumer privacy in relation to these advertising networks is the <a href="http://www.networkadvertising.org/"> Network Advertising Initiative (NAI) </a>. On this website you can opt out from Behavioral Advertising for all the advertising networks which are member of the NAI.</p>





<!-- Start of html generated by this->footer() -->
</div>
<div id="rightsection"><div class="rightSkyscraper">
<div id="537376523" style="width:160px;height:600px;margin:0;padding:0">
		  <noscript><iframe id="e630eaf2b1" name="e630eaf2b1" src="http://ox-d.ghanaweb.com/w/1.0/afr?auid=537376523&cb=145328511719953" frameborder="0" scrolling="no" width="160" height="600"><a href="http://ox-d.ghanaweb.com/w/1.0/rc?cs=e630eaf2b1&cb=145328511719953" ><img src="http://ox-d.ghanaweb.com/w/1.0/ai?auid=537376523&cs=e630eaf2b1&cb=145328511719953" border="0" alt=""></a></iframe></noscript>
		</div>
		<script type="text/javascript">
		  var OX_ads = OX_ads || [];
		  OX_ads.push({
		     slot_id: "537376523",
		     auid: "537376523"
		  });
		</script>
		<script type="text/javascript" src="http://ox-d.ghanaweb.com/w/1.0/jstag"></script></div>
<style type="text/css" media="screen">
.cpcpop {visibility:hidden;width:445px;position:absolute;top:100px;left:100px; z-index:10;}
.cpcpop .inner{width:435px;margin:5px;}
.cpcpop .inner img.cntrl {position:absolute;right:-20px; top:-20px;}
.popup-box{background:#fff;padding:10px 20px;border:10px solid #7F961C;float:left;width:480px;position:fixed;top:30%;left:50%;margin:0 0 0 -250px;z-index:100;}
.popup-box .share-top{float:left;margin-bottom:5px;width:480px;}
.popup-box h3{border:0;float:left;width:480px;margin:5px 0 7px 10px;}
.icon-email {background: url('http://cdn.ghanaweb.com/design/cpc_sprite2.jpg') no-repeat 0 0;width: 58px;height: 29px;}
.icon-phone {background: url('http://cdn.ghanaweb.com/design/cpc_sprite2.jpg') no-repeat -29px 0px;width: 58px;height: 29px;}
#cpcicon_box{width:29px; height:29px;}
</style>
<script>

function openCPCPOP(cpc_box_id) {
	document.getElementById('cpcpopupbox').style.visibility = 'visible';
	updatecpcpopup(cpc_box_id);
}

function closeCPCPOP(cpc_box_id) {
	document.getElementById('cpcpopupbox').style.visibility = 'hidden';
}

function updatecpcpopup(cpc_box_id)
{
	document.getElementById('cpcpopup_display').innerHTML='Loading...';
	var xmlhttp;
  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
 	 xmlhttp=new XMLHttpRequest();
  }
	else
  {// code for IE6, IE5
  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
	xmlhttp.onreadystatechange=function()
  {
  	if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    	//response
    	document.getElementById('cpcpopup_display').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","http://www.ghanaweb.com/cpcpopup.php?token="+cpc_box_id,true);
xmlhttp.send();
}
</script><div id="cpcpopupbox" class="cpcpop">
									  <div class="popup-box">
									  	<div class="inner">
									     	<a href="javascript:closeCPCPOP()"><img src="http://cdn.ghanaweb.com/design/icon_closex.png" class="cntrl" title="Close" border="0" /></a>
									     	<div id="cpcpopup_display"></div>
								    	</div>
									  </div>
									</div><script language="Javascript" type="text/javascript" src="http://cdn.ghanaweb.com/jscript/v2.8/cpc.scroll3.js"></script>
<div class="selfad" style="width:160px; overflow:hidden; position:relative; background:#9a9a9a; margin-bottom:10px"><a href="http://members.ghanaweb.com/cpc_campaign" style="font-size:14px; display:block; text-align:center; background:#9a9a9a; padding:5px 0; color:#fff"><strong>GhanaWeb</strong><br/>Self Adverts</a>
							<div class="cover" style="position:absolute; background:#9a9a9a; height:700px; width:160px; z-index:1; left:0; opacity:0.0; "></div><div id="scroller"><ul style="float:left; margin:0"><li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6829" ><strong>See the Top 11 Most</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1456116943.jpg" title="See the Top 11 Most" alt="See the Top 11 Most" width="54" height="54">
						  Mysterious Deaths of African Presidents</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6983" ><strong>AK47 SYRUP 4 PENIS</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1458944242.jpg" title="AK47 SYRUP 4 PENIS" alt="AK47 SYRUP 4 PENIS" width="54" height="54">
						  ENLARGEMENT.100% HERBAL 
0279956270</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6807" ><strong>SEXUAL WEAKNESS </strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://cdn.ghanaweb.com/cpc_campaign/1430588175.jpg" title="SEXUAL WEAKNESS " alt="SEXUAL WEAKNESS " width="54" height="54">
						  STOP PREMATURE EJACULATION 0206933738</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="javascript:void(0)" onclick="openCPCPOP('6971')"><strong>General Gardening </strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/no-img.jpg" title="General Gardening " alt="General Gardening " width="54" height="54">
						  Mowing,planting of flowers & lawns</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="javascript:void(0)" onclick="openCPCPOP('6976')"><strong>HOSTEL FOR RENT</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1458736468.jpg" title="HOSTEL FOR RENT" alt="HOSTEL FOR RENT" width="54" height="54">
						  30 Bedrooms with 207 Students yearly</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6991" ><strong>WEBSIT &PAY IN 3MONTHS</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1459249200.jpg" title="WEBSIT &PAY IN 3MONTHS" alt="WEBSIT &PAY IN 3MONTHS" width="54" height="54">
						  NEAT WEBSIT & FINISH PAYIN IN 3MONTHS</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6966" ><strong>See African SOCIALITEs</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1453412192.jpg" title="See African SOCIALITEs" alt="See African SOCIALITEs" width="54" height="54">
						  Exposing their Assets Just for FAME/CASH</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6981" ><strong>BOOST UR ASS &</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1458856489.jpg" title="BOOST UR ASS &" alt="BOOST UR ASS &" width="54" height="54">
						  BREAST WITH HERBAL TONIC 
0244229584
</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6970" ><strong>HOME PIANO/GUITAR</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1458619391.jpg" title="HOME PIANO/GUITAR" alt="HOME PIANO/GUITAR" width="54" height="54">
						  LESSON FOR KIDS.
CLICK 0268815340</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6997" ><strong>END YOUR SNORING</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1459301693.jpg" title="END YOUR SNORING" alt="END YOUR SNORING" width="54" height="54">
						  NOW AND SAVE YOUR MARRIAGE
0542992386</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="javascript:void(0)" onclick="openCPCPOP('6842')"><strong>12 BEDROOM HOUSE</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1456249468.jpg" title="12 BEDROOM HOUSE" alt="12 BEDROOM HOUSE" width="54" height="54">
						  EAST LEGON GHC 6,000,000 NEGOTIABLE</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6996" ><strong>STOP WEAK ERECTION</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://cdn.ghanaweb.com/cpc_campaign/1450122255.jpg" title="STOP WEAK ERECTION" alt="STOP WEAK ERECTION" width="54" height="54">
						  BOOST SEXUAL DRIVE & LIBIDO 0265280923</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6949" ><strong>BOREHOLE CONSTRUCTION</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://cdn.ghanaweb.com/cpc_campaign/1443613741.jpg" title="BOREHOLE CONSTRUCTION" alt="BOREHOLE CONSTRUCTION" width="54" height="54">
						  Affordable borehole water supply for all</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6917" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1457958375.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assisting with Masters and PhD thesis.</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6953" ><strong>Find Godly Partner Now</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1458394542.jpg" title="Find Godly Partner Now" alt="Find Godly Partner Now" width="54" height="54">
						  For a God-Fearing Spouse Click Here Now</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6908" ><strong>Earn up to $10,000/Wk</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1457629957.jpg" title="Earn up to $10,000/Wk" alt="Earn up to $10,000/Wk" width="54" height="54">
						  Partner Canada's biggest fashion house</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6986" ><strong>USE THE POWER </strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1459060780.jpg" title="USE THE POWER " alt="USE THE POWER " width="54" height="54">
						  STONE & LAST 2 HOURS.
CLICK. 0268815340</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6896" ><strong>Medical Support </strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1457440930.jpg" title="Medical Support " alt="Medical Support " width="54" height="54">
						  Support a stereo-tactic radio surgery </a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6919" ><strong>Thesis Writers Ghana</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1457958540.jpg" title="Thesis Writers Ghana" alt="Thesis Writers Ghana" width="54" height="54">
						  Assisting with Masters and PhD thesis.</a></li>
<li style="width:152px;float:left;position:relative;display:block; border-top:3px solid #9a9a9a; background:#FFF; border-color:#9a9a9a; padding:0 3px 3px; margin:0 0 0 1px">
<a style="float:left" href="http://members.ghanaweb.com/cpc_campaign/redirect.php?token=6968" ><strong>ODD & SHOCKING Stories</strong><br/>
						  <img style="float:left; margin:3px 5px 0 0; border:1px solid #f2f2f2;" src="http://static.ghanaweb.com/cpc_campaign/1453412058.jpg" title="ODD & SHOCKING Stories" alt="ODD & SHOCKING Stories" width="54" height="54">
						  READ STRANGE but Interesting Stories</a></li>
</ul></div>
					<a href="http://members.ghanaweb.com/cpc_campaign"><img src="http://cdn.ghanaweb.com/cpc_campaign/banner_sm9.gif" style="margin:0"></a></div>
</div>

<div id="footer">
	<ul id="footerinner">
    	<li class="fstyle"><a href="http://www.ghanaweb.com/GhanaHomePage/aboutus.php">About Us</a></li>
        <li class="fstyle"><a href="http://www.ghanaweb.com/advertise">Advertising</a></li>
        <li class="fstyle"><a rel="nofollow" href="http://www.ghanaweb.com/GhanaHomePage/feedback.php?url=www.ghanaweb.com/GhanaHomePage/privacy_policy.php" onclick="NewWindow('http://www.ghanaweb.com/GhanaHomePage/feedback.php?url=www.ghanaweb.com/GhanaHomePage/privacy_policy.php', 'feedback', '550', '450','no') ;return false;">Feedback</a></li>
        <li class="fstyle last"><a href="http://www.ghanaweb.com/GhanaHomePage/sitemap.php">Sitemap</a></li>
    </ul>
    <div id="footerrechts">
    	Copyright &copy; 1994 - 2016 GhanaWeb. All rights reserved.    </div>
</div>
</div>

</div>
<script type="text/javascript">
		   var _sf_async_config = { uid: 56378, domain: 'ghanaweb.com', useCanonical: true, sections: 'GhanaHomePage', authors:''};
		  (function() {
		    function loadChartbeat() {
		      window._sf_endpt = (new Date()).getTime();
		      var e = document.createElement('script');
		      e.setAttribute('language', 'javascript');
		      e.setAttribute('type', 'text/javascript');
		      e.setAttribute('src','//static.chartbeat.com/js/chartbeat.js');
		      document.body.appendChild(e);
		    };
		    var oldonload = window.onload;
		    window.onload = (typeof window.onload != 'function') ?
		      loadChartbeat : function() { oldonload(); loadChartbeat(); };
		  })();
		</script>

</div></div>

<!-- REF: http://www.ghanaweb.com/GhanaHomePage/NewsArchive/Some-of-my-friends-think-I-am-a-fool-Kwesi-Pratt-427382 ::: MBR: 57a759ecc4dc76249f3d853eaa691d3f --><!--2016-04-01	01	12,13,166	US	01:06:03	2	1		207.241.231.219
--></body>
</html>
<script language="Javascript" type="text/javascript" src="http://cdn.ghanaweb.com/jscript/v2.8/basic.js"></script>
<!-- Begin Cookie Consent --><script type="text/javascript">window.cookieconsent_options = {"message":"By continuing to use the site, you agree to the use of cookies. You can find out more by following","dismiss":"Accept Cookies","learnMore":"this link","link":"/GhanaHomePage/privacy_policy.php","theme":"light-bottom"}</script><script type="text/javascript" src="http://cdn.ghanaweb.com/jscript/v2.8/cookieconsent.min.js?v=2.0"></script><!-- End Cookie Consent plugin --><!-- Generated in 0.006 seconds -->
