<!DOCTYPE html><html lang="en-US"><head><meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"><title>RocketTube - Privacy Policy Statement</title><meta name="description" content="RocketTube takes your privacy seriously."><link rel="alternate" type="application/rss+xml" title="RSS - RocketTube Newest Videos" href="https://feeds.feedburner.com/Rockettube-Newest-Videos"/> <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"><link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'><link rel="stylesheet" type="text/css" href="https://small1.xygallery.com/assetsV3/css/global102517.css" media="all">
<link rel="shortcut icon" type="image/x-icon" href="https://small1.xygallery.com/favicon.ico"/> <script type="text/javascript">var _sf_startpt=(new Date()).getTime()</script> <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-103497-11', 'auto');
  ga('send', 'pageview');

</script></head><body> <header>
	<div class="topHdr">
		<div class="inner">
			<h1></h1>  <span><a href="/login.php">Login</a> <a 
href="/join.php" class="joinBtn">Create free account</a></span>
		</div>
	</div>
	<div class="inner">
		<div class="logo"><a href="/index.php">RocketTube</a>
		</div>
		<nav>
			<label for="show-menu" class="show-menu">&#xf0c9;</label>
			<input type="checkbox" id="show-menu" role="button">
			<ul id="menu">
				<li><a href="http://www.vguys.com/live/guys/?mp_code=asykh" target="_blank">Live Sex</a>
				</li>
				<li><a href="/categories.php">Categories</a>
				</li>
				<li><a href="/channels.php">Channels</a>
				</li>
				<li><a href="/search-by-tags.html">Tags</a>
				</li>
				<li><a href="/upload.php">Upload</a>
				</li>
				<li><a href="/ticket_support.php" target="_blank">Help</a>
				</li>
				<li class="mob"><a href="/login.php">Login</a>
				</li>
				<li class="mob join"><a href="/join.php">Create free account</a>
				</li>
			</ul>
		</nav>
		<div class="search">
			<form action="/searchResults.php" method="post"> <i class="fa fa-search"></i>
				<input type="text" name="q" value="What are you looking for?" onfocus="if(this.value == 'What are you looking for?') { this.value = ''; }" onBlur="if(this.value == '') { this.value = 'What are you looking for?'; }" />
				<input type="submit" name="searchBox" value="go" />
			</form>
		</div>
		<div class="srcBtn"><i class="fa fa-search"></i>
		</div>
	</div>
</header>
<div class="mainC innerPad"> <div class="breadCrumb noMar"><a href="/">Home</a> <i class="fa fa-angle-right"></i> <strong>Privacy</strong></div><div class="normalC"><div class="deTitle glblTtl">PRIVACY</div><div class="innerNmlC"><p>Your Personal Information is Safe We support the rights of adults to explore entertainment and sexuality on the Internet in a private and secure environment. As a visitor of RocketTube.com, you will have peace-of-mind knowing that any information you share privately with us will be kept strictly confidential. This includes, but is not limited to, your name, e-mail address, address, phone number, or personal information regarding the use of this site. If information is collected from you, the information will only be used to provide the products and services you've requested, to provide customer service, or to share news about our service offerings.</p><p> Your e-mail address will never be sold to other companies or used by RocketTube.com or its affiliated companies except for the purposes expressly mentioned above. If you have any questions about our privacy policy, please contact us via our on-site contact form located at support.rockettube.com.</p><p> Please note that any information you publicly reveal on your personal profile page, personal blog, personal gallery, in public message boards or chat does not fall under the above policy. This policy applies only to personally identifiable information you share privately with the operators of RocketTube.com.</p><p> Cookie Policy We use cookies to help remember you as you surf RocketTube.com. These cookies do not contain any personally identifiable information and help enhance your usage of the site. Without them, you may not be able to fully enjoy many aspects of the site such as staying logged in.</p></div></div>  <div class="topAd"> <iframe src="https://secure.vs3.com/_special/banners/LiveWebCams.php?style=responsive-live-models-0002&mp_code=asykh&service=guys&bgcolor=FFFFFF&txtcolor=000000&linkcolor=000000&num_models=50&sitekey=whitelabel&whitelabel_domain=vguys.com&target=_blank&btncolor=000099&btntxtcolor=FFFFFF&accentcolor=FFFF66" scrolling="no" width="100%" height="auto" frameborder="0"></iframe> </div>
<!-- Mobile Footer Below shoudl show on all pages of site -->

<div class="topAdMob"><ins data-revive-zoneid="12" data-revive-id="f3133930653831066a0c65b302b4c974"></ins><script async src="//revive.pixeljack.com/www/delivery/asyncjs.php"></script></div></div><footer>
	<div class="ftrLft">
		<ul>
			<li><a href="/index.php"><i class="fa fa-circle"></i> Home</a>
			</li>
			<li><a href="/terms.php"><i class="fa fa-circle"></i> Terms of Use</a>
			</li>
			<li><a href="/videos/all/page/1.html"><i class="fa fa-circle"></i> Videos</a>
			</li>
			<li><a href="/privacy.php"><i class="fa fa-circle"></i> Privacy Policy</a>
			</li>
			<li><a href="/users.php"><i class="fa fa-circle"></i> Members</a>
			</li>
			<li><a href="/dmca.php"><i class="fa fa-circle"></i> DMCA Info</a>
			</li>
			<li><a href="/upload.php"><i class="fa fa-circle"></i> Uploads</a>
			</li>
			<li><a href="/ticket_support.php" target="_blank"><i class="fa fa-circle"></i> Webmasters</a>
			</li>
			<li><a href="/advertising.php"><i class="fa fa-circle"></i> Advertising</a>
			</li>
			<li><a href="/ticket_support.php" target="_blank"><i class="fa fa-circle"></i> Contact Us</a>
			</li>
		</ul>
	</div>
	<div class="ftrRyt">
		<div class="fllwUs"><span>follow Us:</span>  <a href="feed://www.rockettube.com/rss.php"><i class="fa fa-rss"></i></a>  <a href="https://twitter.com/rockettube"><i class="fa fa-twitter"></i></a> <a href="/cdn-cgi/l/email-protection#c5fab6b0a7afa0a6b1f897aaa6aea0b1b1b0a7a0e593aca1a0aab6e3a7aaa1bcf88dace98ce5a3aab0aba1e5b1adacb6e5b2a0a7b6acb1a0e5a4aba1e5b1adaab0a2adb1e5bcaab0e5a8aca2adb1e5a9acaea0e5acb1e5eeadb1b1b5b6e0f684e0f783e0f783b2b2b2ebb7aaa6aea0b1b1b0a7a0eba6aaa8ee"><i class="fa fa-envelope"></i></a>
		</div>
		<p>&copy;
			2018 RocketTube - Labeled with ICRA and RTA. Member of ASACP and The Free Speech Coalition.
			<br /><br />18 U.S.C. 2257 Record-Keeping Requirements Compliance Statement: All models appearing on JustUsBoys.com were over 18 at the time of photography. The records for sexually explicit images are kept by the
individual producers of the images.
			<br /><br />Visit our sister site <a href="http://www.justusboys.com/">JustUsBoys</a>.
		<br />
	</div>
</footer>
<script data-cfasync="false" src="/cdn-cgi/scripts/d07b1474/cloudflare-static/email-decode.min.js"></script><script async type="text/javascript" src="https://small1.xygallery.com/assetsV3/js/tabby.min.js"></script>
<script async type="text/javascript" src="https://small1.xygallery.com/assetsV3/js/general092115.js"></script>
<script type='text/javascript'>var _sf_async_config=_sf_async_config||{};_sf_async_config.uid=5084;_sf_async_config.domain='rockettube.com';_sf_async_config.useCanonical=true;(function(){function loadChartbeat(){window._sf_endpt=(new Date()).getTime();var e=document.createElement('script');e.setAttribute('language','javascript');e.setAttribute('type','text/javascript');e.setAttribute('src','//static.chartbeat.com/js/chartbeat.js');document.body.appendChild(e);} loadChartbeat();})();</script>

<!--<script>function setCookie(e,o){var t=new Date;t.setTime(t.getTime()+86400);var i="expires="+t.toUTCString();document.cookie=e+"="+o+"; "+i+";domain=rockettube.com;path=/"}function getCookie(e){for(var o=e+"=",t=document.cookie.split(";"),i=0;i<t.length;i++){for(var n=t[i];" "==n.charAt(0);)n=n.substring(1);if(0==n.indexOf(o))return n.substring(o.length,n.length)}return""}$(document).ready(function(){$("html").mouseleave(function(){var e=getCookie("bsbpu");"1"!=e&&($("body").addClass("fixedPos"),$("footer").after('<div class="popup"><div class="popInner"><i class="closeBtn"></i><a href="https://join.brokestraightboys.com/track/MTAzNTU0LjEwMTE1LjEwOS4zMzUuMjE3LjAuMC4wLjA/join?tpl=join4"><img src="https://small1.xygallery.com/assetsV3/images/popup-75percent-2018.jpg" alt="" /></a></div></div>'),$("i.closeBtn").on("click",function(e){$("body").removeClass("fixedPos"),$(".popup").hide(),e.preventDefault()}),setCookie("bsbpu","1"))})});</script> -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
	{parsetags:'explicit'}
</script>
<script type="text/javascript">
	gapi.plusone.go();
</script>



</body></html>
