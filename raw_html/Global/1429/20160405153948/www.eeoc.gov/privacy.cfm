<!DOCTYPE html> <html lang="en"> <!-- Content Copyright Equal Employment Opportunity Commission -->
<!-- Page generated 2016-04-04 17:08:13 by CommonSpot Build 5.1.1.160 (2010-07-08 15:05:28) -->
<!-- JavaScript & DHTML Code Copyright 1998-2009 PaperThin, Inc.  All rights reserved --> <head> <title>Privacy Policy</title> <meta name="Description" id="Description" content="Privacy Policy" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="Keywords" id="Keywords" content="" />
<meta name="Generator" id="Generator" content="CommonSpot Content Server Build 5.1.1.160" />
<link rel="stylesheet" href="/style/default.css" type="text/css" />
<link rel="stylesheet" href="/style/eeoc_main.css" type="text/css" />
<style type="text/css">
	.mw { color:#000000;font-family:Verdana,Arial,Helvetica;font-weight:bold;font-size:xx-small;text-decoration:none; }
	a.mw:link	{color:#000000;font-family:Verdana,Arial,Helvetica;font-weight:bold;font-size:xx-small;text-decoration:none;}
	a.mw:visited	{color:#000000;font-family:Verdana,Arial,Helvetica;font-weight:bold;font-size:xx-small;text-decoration:none;}
	a.mw:hover	{color:#0000FF;font-family:Verdana,Arial,Helvetica;font-weight:bold;font-size:xx-small;text-decoration:none;}
</style> <script type="text/javascript">
<!--
	var gMenuControlID = 0;
	var menus_included = 0;
	var jsDlgLoader = '/loader.cfm';
	var jsSiteID = 3;
	var jsSubSiteID = 1;
	var js_gvPageID = 1369;
	var jsPageID = 1369;
	var jsPageSetID = 0;
	var jsPageType = 0;
	var jsControlsWithRenderHandlers = ",4101,4101,173417,";
	var jsDefaultRenderHandlerProps = ",,";
	var jsAuthorizedControls = ",1,2,3,4,6,7,8,9,10,11,16,18,20,21,22,23,25,26,27,28,29,30,31,34,36,37,38,39,40,41,42,43,44,45,46,47,50,51,52,53,54,4101,8218,8406,8448,8484,8514,8542,173417,191640,199899,";
	var jsCustomRenderHandlerPairs = "";
	var jsStandardRenderHandlers = "";
	var jsSiteSecurityCreateControls = 0;
	var jsShowRejectForApprover = 1;

	document.CS_StaticURL = "http://www.eeoc.gov/";
	document.CS_DynamicURL = "http://www1.eeoc.gov/";
// -->
</script>
<script type="text/javascript" src="/commonspot/javascript/browser-all.js"></script> 

<script type="text/javascript" src="/upload/external.js"></script>
<script type="text/javascript" src="/upload/niftycube.js"></script>
<script type="text/javascript">
window.onload=function(){
Nifty("div.box,div#leftmenu");
Nifty("div#centercol","top");
handleExternalLinks();
}
</script>


  <link rel="stylesheet" type="text/css" href="/upload/hover.css">
  

<!-- START sitecues implementation code -->
<script data-provider="sitecues-config" type="application/javascript">
    var sitecues = window.sitecues = window.sitecues || {};
    sitecues.config = sitecues.config || {};
    sitecues.config.palette = 'reverse-blue';
    // sitecues.config.maxRewrapZoom = 1.3;
</script>
<script data-provider="sitecues" type="application/javascript">
    // DO NOT MODIFY THIS SCRIPT WITHOUT ASSISTANCE FROM SITECUES
    var sitecues = window.sitecues = window.sitecues || {};
    sitecues.config = sitecues.config || {};
    sitecues.config.siteId = 's-0000ee0c';
    sitecues.config.scriptUrl = 'https://js.sitecues.com/l/s;id='+sitecues.config.siteId+'/js/sitecues.js';
    (function () {
        var script = document.createElement('script'),
            first  = document.getElementsByTagName('script')[0];
        script.type  = 'application/javascript';
        script.async = true;
        script.src   = sitecues.config.scriptUrl;
        first.parentNode.insertBefore(script, first);
    }());
</script>
<!-- END sitecues implementation code -->

<link rel="stylesheet" href="/commonspot/commonspot.css" type="text/css" id="cs_maincss" /> </head><body lang="en" class="CS_Document"><a name="__topdoc__"></a><script src="/commonspot/ns-resize.js" type="text/javascript"></script><script src="/commonspot/pagemode/always-include-common.js" type="text/javascript"></script><script src="/commonspot/pagemode/always-include-ns.js" type="text/javascript"></script>

  
 <div id="view">
 
  <a href="#centercol" class="skipnav" title="Skip to content">Skip to content</a></div>

  <div id="header">
  
  
      <div id="languages"><a href="/spanish/">Espa&#241;ol</a> | <a href="/languages/">Other Languages</a></div>
    
  <img src="/graphics/seal.png" height="90px" width="90px" alt="EEOC Seal" id="seal">
  <form name="gs" method="GET" action="/search/search">
  <div id="searchbox">
      	  <input type="text" name="q" id="search_input" onClick="clearSearch();" size="25" maxlength="256" placeholder="Enter search terms..." title="Enter search terms">&nbsp;<input type="submit" name="btnG"  id="search" value="Search">
  </div>
 <input type="hidden" name="entqr" value="0">
	  <input type="hidden" name="ud" value="1">
	  <input type="hidden" name="sort" value="date:D:L:d1">
	  <input type="hidden" name="output" value="xml_no_dtd">
	  <input type="hidden" name="oe" value="UTF-8"><input type="hidden" name="ie" value="UTF-8">
	  <input type="hidden" name="client" value="default_frontend">
	  <input type="hidden" name="proxystylesheet" value="default_frontend">
	  <input type="hidden" name="site" value="FullSite">
      </form>

<p id="eeoc"><a href="/index.cfm">U.S. Equal Employment<br>Opportunity Commission</a></p>
    <p style="clear: right; float: right; margin-top: -2em; margin-bottom: -1em;"><strong><a href="/connect_with_us.cfm" style="font-weight: 700; text-decoration: none; color: white; margin-bottom: 0;">CONNECT WITH US</a></strong>&nbsp;
    <a href="http://www1.eeoc.gov/eeoc/newsroom/index.cfm?xml=eeoc_press,RSS2.0full"><img src="/graphics/rss_32x32x32.png" alt="RSS News Feed" title="RSS" border="0" height="24" style="position: relative; top: 12px;"></a> 
    <a href="http://www.twitter.com/eeocnews/"><img src="/graphics/twitter_32x32x32.png" alt="Twitter" title="Twitter" border="0" height="24" style="position: relative; top: 12px;"></a> 
    <a href="http://www.twitter.com/eeocespanol/"><img src="/graphics/twitter_es_32x32x32.png" alt="Twitter en Español" title="Twitter en Español" border="0" height="24" style="position: relative; top: 12px;"></a> 
    <a href="https://www.facebook.com/USEEOC"><img src="/graphics/facebook_32x32x32.png" alt="Facebook" title="Facebook" border="0" height="24" style="position: relative; top: 12px;"></a> 
    <a href="http://www.youtube.com/user/TheEEOC"><img src="/graphics/youtube_32x32x32.png" alt="YouTube Channel" title="YouTube"  border="0" height="24" style="position: relative; top: 12px;"></a> 
    <a href="https://public.govdelivery.com/accounts/USEEOC/subscriber/new" target="_blank"><img src="/graphics/govd_white.png" alt="Get Email Updates" title="Get Email Updates" border="0" height="23" style="position: relative; top: 12px;"></a></p>



<ul class="dropdown">
	<li><a href="/">Home</a></li>
	
	<li><a href="/eeoc/">About EEOC</a>
	  <ul class="drop">
		<li><a href="/eeoc/index.cfm" class="first">Overview</a></li>
		<li><a href="/eeoc/commission.cfm">The Commission</a></li>
		<li><a href="/eeoc/meetings/index.cfm">Meetings of the Commission</a></li>
		<li><a href="/open/index.cfm">Open Government</a></li>
		<li><a href="/eeoc/newsroom/index.cfm">Newsroom</a></li>
		<li><a href="/laws/index.cfm">Laws, Regulations, Guidance &amp; MOUs</a></li>
		<li><a href="/eeoc/plan/index.cfm">Budget &amp; Performance</a></li>
		<li><a href="/eeoc/enforcement_litigation.cfm">Enforcement &amp; Litigation</a></li>
		<li><a href="/eeoc/initiatives/index.cfm">Initiatives</a></li>
		<li><a href="/eeoc/task_force/index.cfm">Task Forces</a></li>
		<li><a href="/eeoc/interagency/index.cfm">Interagency Programs</a></li>
		<li><a href="/eeoc/publications/index.cfm">Publications</a></li>
		<li><a href="/eeoc/statistics/index.cfm">Statistics</a></li>
		<li><a href="/eeoc/outreach/index.cfm">Outreach &amp; Education</a></li>
		<li><a href="/eeoc/legislative/index.cfm">Legislative Affairs</a></li>
		<li><a href="/eeoc/foia/index.cfm">FOIA &amp; Privacy Act</a></li>
		<li><a href="/eeoc/doingbusiness/index.cfm">Doing Business with EEOC</a></li>
		<li><a href="/eeoc/jobs/index.cfm">Jobs &amp; Internships</a></li>
		<li><a href="/eeoc/history/index.cfm">EEOC History</a></li>
		<li><a href="http://oig.eeoc.gov/" class="last">Office of Inspector General</a></li>
	  </ul>
    </li>

    <li><a href="/employees/">Employees &amp; Applicants</a>
	  <ul class="drop">
		<li><a href="/employees/index.cfm" class="first">Overview</a></li>
		<li><a href="/employees/coverage.cfm">Coverage</a></li>
		<li><a href="/employees/timeliness.cfm">Timeliness</a></li>
		<li><a href="/employees/charge.cfm">Filing A Charge</a>
		  <ul>
			<li><a href="/employees/howtofile.cfm">How to File</a></li>
			<li><a href="/employees/process.cfm">After You File a Charge</a></li>
			<li><a href="/employees/confidentiality.cfm">Confidentiality</a></li>
			<li><a href="/employees/mediation.cfm">Mediation</a></li>
			<li><a href="/employees/remedies.cfm">Remedies</a></li>
			<li><a href="/employees/afterfiling.cfm">Existing Charges</a></li>
			<li><a href="/employees/lawsuit.cfm">Filing a Lawsuit</a></li>
		  </ul>
		</li>
		<li><a href="/laws/types/index.cfm">Discrimination by Type</a>
		  <ul>
			<li><a href="/laws/types/age.cfm">Age</a></li>
			<li><a href="/laws/types/disability.cfm">Disability</a></li>
			<li><a href="/laws/types/equalcompensation.cfm">Equal Compensation</a></li>
			<li><a href="/laws/types/genetic.cfm">Genetic Information</a></li>
			<li><a href="/laws/types/harassment.cfm">Harassment</a></li>
			<li><a href="/laws/types/nationalorigin.cfm">National Origin</a></li>
			<li><a href="/laws/types/pregnancy.cfm">Pregnancy</a></li>
			<li><a href="/laws/types/race_color.cfm">Race/Color</a></li>
			<li><a href="/laws/types/religion.cfm">Religion</a></li>
			<li><a href="/laws/types/retaliation.cfm">Retaliation</a></li>
			<li><a href="/laws/types/sex.cfm">Sex</a></li>
			<li><a href="/laws/types/sexual_harassment.cfm">Sexual Harassment</a></li>
		  </ul>
		</li>
		<li><a href="/laws/practices/index.cfm" class="last">Prohibited Practices</a></li>
	  </ul>
	</li>
	
    <li><a href="/employers/index.cfm">Employers</a>
	  <ul class="drop">
		<li><a href="/employers/index.cfm" class="first">Overview</a></li>
		<li><a href="/employers/coverage.cfm">Coverage</a></li>
		<li><a href="/employers/process.cfm">After a Charge is Filed</a></li>
		<li><a href="/employers/resolving.cfm">Resolving a Charge</a></li>
		<li><a href="/employers/remedies.cfm">Remedies</a></li>
		<li><a href="/laws/types/index.cfm">Discrimination by Type</a></li>
		  <ul>
			<li><a href="/laws/types/age.cfm">Age</a></li>
			<li><a href="/laws/types/disability.cfm">Disability</a></li>
			<li><a href="/laws/types/equalcompensation.cfm">Equal Compensation</a></li>
			<li><a href="/laws/types/genetic.cfm">Genetic Information</a></li>
			<li><a href="/laws/types/harassment.cfm">Harassment</a></li>
			<li><a href="/laws/types/nationalorigin.cfm">National Origin</a></li>
			<li><a href="/laws/types/pregnancy.cfm">Pregnancy</a></li>
			<li><a href="/laws/types/race_color.cfm">Race/Color</a></li>
			<li><a href="/laws/types/religion.cfm">Religion</a></li>
			<li><a href="/laws/types/retaliation.cfm">Retaliation</a></li>
			<li><a href="/laws/types/sex.cfm">Sex</a></li>
			<li><a href="/laws/types/sexual_harassment.cfm">Sexual Harassment</a></li>
	      </ul>
		<li><a href="/laws/practices/index.cfm">Prohibited Practices</a></li>
		<li><a href="/employers/recordkeeping.cfm">Recordkeeping</a></li>
		<li><a href="/employers/reporting.cfm">EEO Reports/Surveys</a></li>
		<li><a href="/employers/poster.cfm">"EEO Is The Law" Poster</a></li>
		<li><a href="/eeoc/outreach/index.cfm">Training</a></li>
		<li><a href="/employers/other_issues.cfm" class="last">Other Employment Issues</a></li>
	  </ul>
    </li>
	
    <li><a href="/federal/">Federal Agencies</a>
	  <ul class="drop">
		<li><a href="/federal/index.cfm" class="first">Overview</a></li>
		<li><a href="/federal/fed_employees/index.cfm">Federal Employees &amp; Applicants</a>
	      <ul>
			<li><a href="/federal/fed_employees/complaint_overview.cfm">Federal Complaint Process</a></li>
			<li><a href="/laws/types/index.cfm">Discrimination by Type</a></li>
			<li><a href="/federal/otherprotections.cfm">Other Federal Protections</a></li>
			<li><a href="/laws/practices/index.cfm">Prohibited Practices</a></li>
	      </ul>
		</li>
		<li><a href="/federal/coordination.cfm">Federal EEO Coordination</a></li>
		<li><a href="/federal/eeo_directors.cfm">Federal Agency EEO Directors</a></li>
		<li><a href="/laws/index.cfm">Laws, Regulations, Guidance &amp; MOUs</a></li>
		<li><a href="/federal/directives/index.cfm">Management Directives &amp; Federal Sector Guidance</a></li>
		<li><a href="/federal/adr/index.cfm">Federal Sector Alternative Dispute Resolution</a></li>
		<li><a href="/federal/reports/index.cfm">Federal Sector Reports</a></li>
		<li><a href="/federal/decisions.cfm">Appellate Decisions</a></li>
		<li><a href="/federal/digest/index.cfm"><cite>Digest of EEO Law</cite></a></li>
		<li><a href="/federal/fedsep.cfm">Federal Sector EEO Portal (FedSEP)</a></li>
		<li><a href="/federal/form462/index.cfm">Form 462 Reporting</a></li>
		<li><a href="/federal/training/index.cfm" class="last">Federal Training &amp; Outreach</a></li>
	  </ul>
    </li>
	
    <li><a href="/contact/">Contact Us</a>
	  <ul class="drop">
		<li><a href="/contact/index.cfm" class="first">Contact EEOC</a></li>
		<li><a href="/field/index.cfm">Find Your Nearest Office</a></li>
		<li><a href="https://eeoc.custhelp.com/cgi-bin/eeoc.cfg/php/enduser/std_alp.php?p_sid=r9xBKEAh&p_lva=&p_sp=&p_li=" class="last">Frequently Asked Questions</a></li>
	  </ul>
    </li>

  <li>

<!-- sitecues badge -->
	<li style="float: right; border-right: none;"><a id="sitecues-badge" style="width: 110.5px; height: 17px; margin-right: 1.5em;"></a></li>

	
  </li>
 
</ul>

  </div>

  <div id="container">
  
  <div id="centercol">
  <div id="cs_control_1332" class="cs_control CS_Element_LinkBar"><a href="/" class="CS_LinkBar_Item" onmouseout=" setStatbar('');return true;" onmouseover="setStatbar('/');return true;">Home</a></div>

  <p class="pagetools" style="margin-bottom: 1em;">
  
  <a href="privacy.cfm?renderforprint=1"><img src="/graphics/print.png" alt="Print" title="Print this page"></a> 
  &nbsp; <a href="mailto:?Subject=Link from U.S. Equal Employment Opportunity Commission website&Body=Here's a link to a page on the U.S. Equal Employment Opportunity Commission website that you might be interested in...%0a%0ahttp://www.eeoc.gov/Root Site/privacy.cfm"><img src="/graphics/mail.png" alt="Email" title="Email this page"></a>&nbsp;
  <a href="http://www.addthis.com/bookmark.php" title="Share this page"><img src="/graphics/share.png" alt="Share"></a></p>
  <div id="cs_control_1333" class=""><div  class="CS_Element_Schedule"  title="" id="CS_Element_Content" >
<a name="textblocknohdr1373" id="textblocknohdr1373"></a><a name="CP_JUMP_1373" id="CP_JUMP_1373"></a><div id="cs_control_1373" class="cs_control CS_Element_Textblock"><div class="CS_Textblock_Text"><h1>Privacy Policy for the U.S. Equal Employment Opportunity Commission Web Site</h1>

<p>We collect no personal information about you when you visit this site unless you choose to provide this information to us. Our web server software, and our content management system, do collect certain information automatically, and some of this
information is made available to us. This information is outlined below.</p>

<h2>Information collected and stored automatically:</h2>

<p>If you visit our site to read or download information, the following information is collected automatically by the web server. None of this information is used to identify you.</p>

<ul>
<li>The name of Internet domain and IP address (an IP address is a number that is automatically assigned to your computer when you are connected to a network, such as the Internet). This information is used to help count the number of unique visits
made to the site;</li>

<li>The date and time that you access our site;</li>

<li>The pages you visit (which helps us to determine what people are looking for);</li>

<li>The web browser you use (which helps us to design our site to accommodate the broadest possible range of web browser software); and</li>

<li>If you followed a link to get to us, the page you linked from (which helps us to determine how people are finding us, and how we can reach more people).</li>
</ul>

<p>All of this information is used to help us make our site more useful to visitors like you. We do not track or record information about individuals.</p>

<h2>If you send us personal information:</h2>

<p>If you choose to provide us with personal information, as in e-mail with a comment or question, or by filling out a form with your personal information and submitting it to us through our web site, we use that information to respond to your
message and to help us get you the information you have requested. Electronic mail is not secure. Therefore, we suggest that you not send personal information to us via e-mail, especially social security numbers. We may share information you give us
with contractors acting on our behalf or with another government agency if your inquiry relates to that agency. EEOC does not collect or use information for commercial marketing. We do not share our e-mail with any other organizations, unless we
receive a request from an organization conducting a civil or criminal law enforcement investigation.</p>

<p>Electronically submitted information is maintained and destroyed according to the requirements of the Federal Records Act and the regulations and records retention schedules of the National Archives and Records Administration, and in some cases
may be covered by the Privacy Act and the Freedom of Information Act. A discussion of your rights under these laws can be found at <a href="http://www.usa.gov/">www.USA.gov</a>.</p>

<h2>Social Media:</h2>

<p>Please see our <a class="CP___PAGEID_189152" href="http://www.eeoc.gov/social_media_policies.cfm">comments policy and privacy notice</a> for Facebook, Twitter, YouTube and other social media.<br>
</p>

<h2>Links to other sites:</h2>

<p>We may have links to other outside web sites (both federal and non-federal) that we do not control. We are not responsible for the content or privacy policies of these sites, and users should check those policies on such sites.</p>

<h2>Use of cookies:</h2>

<p>"Cookies" are small bits of text that are either used for the duration of the session ("session cookies"), or saved on a user's hard drive in order to identify that user, or information about that user, the next time the user logs on to a web
site ("persistent cookies"). EEOC's websites do not use persistent cookies. Session specific cookies may be used to improve the user experience and for basic web metrics. These cookies expire in a very short time frame or when a browser window
closes and are permitted by current federal guidelines.</p>

<h2>Security:</h2>

<p>We maintain a variety of physical, electronic and procedural safeguards to protect the security of this web site and any personal information you provide to us. For example, this Government computer system employs software programs to monitor
network traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage. Anyone using this system expressly consents to such monitoring and is advised that if such monitoring reveals evidence of possible abuse or
criminal activity, such evidence may be provided to appropriate law enforcement officials. Unauthorized attempts to upload or change information on this server are strictly prohibited and may be punishable by law, including the Computer Fraud and
Abuse Act of 1986, and the National Information Infrastructure Protection Act of 1996.</p>

<h2>Changes to this policy:</h2>

<p>We will revise or update this policy if our practices change. You should refer back to this page often for the latest information and the effective date of any changes. If we decide to change this policy, we will post a new policy on our site and
change the date at the bottom. Changes to the policy shall not apply retroactively.</p></div></div>

</div></div>
  <!-- CONTENT GOES HERE -->


  </div> <!-- centercol -->

  </div> <!-- container -->
  <div id="footer">
    <p><strong><a href="/connect_with_us.cfm" style="font-weight: 700; text-decoration: none;">CONNECT WITH US</a></strong>&nbsp;
    <a href="http://www1.eeoc.gov/eeoc/newsroom/index.cfm?xml=eeoc_press,RSS2.0full"><img src="/graphics/rss_32x32x32.png" alt="RSS News Feed" title="RSS" border="0" height="24" style="position: relative; top: 12px;"></a> 
    <a href="http://www.twitter.com/eeocnews/"><img src="/graphics/twitter_32x32x32.png" alt="Twitter" title="Twitter" border="0" height="24"  style="position: relative; top: 12px;"></a> 
    <a href="http://www.twitter.com/eeocespanol/"><img src="/graphics/twitter_es_32x32x32.png" alt="Twitter en Español" title="Twitter en Español" border="0" height="24" style="position: relative; top: 12px;"></a> 
    <a href="https://www.facebook.com/USEEOC"><img src="/graphics/facebook_32x32x32.png" alt="Facebook" title="Facebook" border="0" height="24"  style="position: relative; top: 12px;"></a> 
    <a href="http://www.youtube.com/user/TheEEOC"><img src="/graphics/youtube_32x32x32.png" alt="YouTube Channel" title="YouTube"  border="0" height="24" style="position: relative; top: 12px;"></a> 
    <a href="https://public.govdelivery.com/accounts/USEEOC/subscriber/new" target="_blank"><img src="/graphics/govd_original.png" alt="Get Email Updates" title="Get Email Updates" border="0" height="22" style="position: relative; top: 12px;"></a></p>
  <p><a href="/privacy.cfm">Privacy Policy</a> | <a href="/disclaimer.cfm">Disclaimer</a> | <a href="http://www.usa.gov">USA.Gov</a></p>
  </div>
 </div> <!-- view -->
  
<div style="display:block; clear:left; padding:0px; font-family:Verdana,Arial; font-size:10px; color:gray;"></div><script type="text/javascript"></script><script type="text/javascript">
		<!--
			var jsPageContributeMode = 'read';
			var jsPageSessionContributeMode = 'read';
			var jsPageAuthorMode = 0;
			var jsPageEditMode = 0;
			// build commonspot.csPage, used by entrance.js and onDocumentLoaded
			if(!commonspot)
				var commonspot = {};
			commonspot.csPage = {};
			commonspot.csPage.url = location.pathname + ((location.pathname.match(/.cfm/i)) ? '' : 'index.cfm'); // use index.cfm if no filename
			commonspot.csPage.id = 1369;
			commonspot.csPage.title = 'Privacy Policy';
			commonspot.csPage.subsiteRoot = '/';
			commonspot.csPage.siteRoot = '/';
			commonspot.csPage.mode = jsPageSessionContributeMode;
			commonspot.csPage.authorok = 0;
			commonspot.csPage.showContainerUI = 0;
			commonspot.csPage.requestedVersionTimestamp = '';
			commonspot.csPage.userRights = {};
			commonspot.csPage.userRights.isLoggedIn = 0;
			commonspot.csPage.userRights.read = 1;
			commonspot.csPage.userRights.author = 0;
			commonspot.csPage.userRights.edit = 0;
			commonspot.csPage.userRights.approve = 0;
			commonspot.csPage.userRights.history = 0;
			commonspot.csPage.userRights.design = 0;
			commonspot.csPage.userRights.admin = 0;
			commonspot.csPage.userRights.subsiteAdmin = 0;
			commonspot.csPage.userRights.siteAdmin = 0;
			commonspot.csPage.userRights.userAdmin = 0;
			var doLviewRedirect;
			if(parent && parent.commonspot && parent.commonspot.lview && (!doLviewRedirect))
			{
				onPageArrival = function()
				{
					parent.commonspot.lview.currentPage.onPageArrival(commonspot.csPage.mode, commonspot.csPage.url, commonspot.csPage.id, commonspot.csPage.title, commonspot.csPage.subsiteRoot, commonspot.csPage.siteRoot);
				};
				onPageUnload = function()
				{
					parent.commonspot.lview.currentPage.onPageUnload(location.pathname + '?' + document.location.search);
				};
				parent.Spry.Utils.addEventListener(window, 'load', onPageArrival, false);
				parent.Spry.Utils.addEventListener(window, 'unload', onPageUnload, false);
			}
		
		// -->
		</script></body></html>