<!DOCTYPE html>
<html data-in_mode="full">
<head prefix="og: http://ogp.me/ns# product: http://ogp.me/ns/product#">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,t,n){function r(n){if(!t[n]){var o=t[n]={exports:{}};e[n][0].call(o.exports,function(t){var o=e[n][1][t];return r(o||t)},o,o.exports)}return t[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({1:[function(e,t,n){function r(e,t){return function(){o(e,[(new Date).getTime()].concat(a(arguments)),null,t)}}var o=e("handle"),i=e(2),a=e(3);"undefined"==typeof window.newrelic&&(newrelic=NREUM);var u=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit"],c=["addPageAction"],f="api-";i(u,function(e,t){newrelic[t]=r(f+t,"api")}),i(c,function(e,t){newrelic[t]=r(f+t)}),t.exports=newrelic,newrelic.noticeError=function(e){"string"==typeof e&&(e=new Error(e)),o("err",[e,(new Date).getTime()])}},{}],2:[function(e,t,n){function r(e,t){var n=[],r="",i=0;for(r in e)o.call(e,r)&&(n[i]=t(r,e[r]),i+=1);return n}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],3:[function(e,t,n){function r(e,t,n){t||(t=0),"undefined"==typeof n&&(n=e?e.length:0);for(var r=-1,o=n-t||0,i=Array(0>o?0:o);++r<o;)i[r]=e[t+r];return i}t.exports=r},{}],ee:[function(e,t,n){function r(){}function o(e){function t(e){return e&&e instanceof r?e:e?u(e,a,i):i()}function n(n,r,o){e&&e(n,r,o);for(var i=t(o),a=l(n),u=a.length,c=0;u>c;c++)a[c].apply(i,r);var s=f[g[n]];return s&&s.push([m,n,r,i]),i}function p(e,t){w[e]=l(e).concat(t)}function l(e){return w[e]||[]}function d(e){return s[e]=s[e]||o(n)}function v(e,t){c(e,function(e,n){t=t||"feature",g[n]=t,t in f||(f[t]=[])})}var w={},g={},m={on:p,emit:n,get:d,listeners:l,context:t,buffer:v};return m}function i(){return new r}var a="nr@context",u=e("gos"),c=e(2),f={},s={},p=t.exports=o();p.backlog=f},{}],gos:[function(e,t,n){function r(e,t,n){if(o.call(e,t))return e[t];var r=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,t,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return e[t]=r,r}var o=Object.prototype.hasOwnProperty;t.exports=r},{}],handle:[function(e,t,n){function r(e,t,n,r){o.buffer([e],r),o.emit(e,t,n)}var o=e("ee").get("handle");t.exports=r,r.ee=o},{}],id:[function(e,t,n){function r(e){var t=typeof e;return!e||"object"!==t&&"function"!==t?-1:e===window?0:a(e,i,function(){return o++})}var o=1,i="nr@id",a=e("gos");t.exports=r},{}],loader:[function(e,t,n){function r(){if(!w++){var e=v.info=NREUM.info,t=s.getElementsByTagName("script")[0];if(e&&e.licenseKey&&e.applicationID&&t){c(l,function(t,n){e[t]||(e[t]=n)});var n="https"===p.split(":")[0]||e.sslForHttp;v.proto=n?"https://":"http://",u("mark",["onload",a()],null,"api");var r=s.createElement("script");r.src=v.proto+e.agent,t.parentNode.insertBefore(r,t)}}}function o(){"complete"===s.readyState&&i()}function i(){u("mark",["domContent",a()],null,"api")}function a(){return(new Date).getTime()}var u=e("handle"),c=e(2),f=window,s=f.document;NREUM.o={ST:setTimeout,XHR:f.XMLHttpRequest,REQ:f.Request,EV:f.Event,PR:f.Promise,MO:f.MutationObserver},e(1);var p=(""+location).split("?")[0],l={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-892.min.js"},d=window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&!/CriOS/.test(navigator.userAgent),v=t.exports={offset:a(),origin:p,features:{},xhrWrappable:d};s.addEventListener?(s.addEventListener("DOMContentLoaded",i,!1),f.addEventListener("load",r,!1)):(s.attachEvent("onreadystatechange",o),f.attachEvent("onload",r)),u("mark",["firstbyte",a()],null,"api");var w=0},{}]},{},["loader"]);</script>

					<title>Privacy Policy - Canuck Audio Mart</title>
	
		<link rel="shortcut icon" href="/images/cam/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" type="text/css" href="http://cdn.staticneo.com/neoassets/cam_assets/core.1454112716.css" />

	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.2/jquery.cookie.js"></script>

	<script type="text/javascript" charset="utf-8" src="http://cdn.staticneo.com/neoassets/cam_assets/core.1450895061.js"></script>

							
		<meta name="keywords" content="canadian,used,buy,sell,audio,buy sell,used audio, used stereo, hifi,high-end,used hifi,amplifier,pre-amplifier,preamp,speakers,buy and sell,trade,marketplace,market,online,web,store,find,lowest price,bryston,canada used audio,canadian audio equipment, canadian used audio euqipment, canada,canada hometheater classified,audio equipment,classifieds,bc audio,ontario,alberta audio,alberta,vancouver classified,toronto audio,quebec audio" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<script type='text/javascript'>
		var googletag = googletag || { };
		googletag.cmd = googletag.cmd || [];
				(function() {
			var gads = document.createElement('script');
			gads.async = true;
			gads.type = 'text/javascript';
			var useSSL = 'https:' == document.location.protocol;
			gads.src = (useSSL ? 'https:' : 'http:') +
					'//www.googletagservices.com/tag/js/gpt.js';
			var node = document.getElementsByTagName('script')[0];
			node.parentNode.insertBefore(gads, node);
		})();
			</script>

	<script type='text/javascript'>
	var slot1, slot2, slot3, slot4;
	googletag.cmd.push(function() {
		
	slot1 = googletag.defineSlot('/1015505/CAM_6-2_230x60', [230, 60], 'div-gpt-ad-1325637203686-7').addService(googletag.pubads());

	if ($("td#main").width() >= 729 || $(window).width() > 1058) {
		slot2 = googletag.defineSlot('/1015505/CAM_5_728x90', [728, 90], 'div-gpt-ad-1325637203686-5').addService(googletag.pubads());
		slot3 = googletag.defineSlot('/1015505/CAM_7_728x90', [728, 90], 'div-gpt-ad-1325637203686-8').addService(googletag.pubads());
	} else {
		slot2 = googletag.defineSlot('/1015505/CAM_5_468x60', [468, 60], 'div-gpt-ad-1325637203686-5').addService(googletag.pubads());
		slot3 = googletag.defineSlot('/1015505/CAM_7_468x60', [468, 60], 'div-gpt-ad-1325637203686-8').addService(googletag.pubads());
	}

	slot4 = googletag.defineSlot('/1015505/CAM_8_120x135', [[120, 135],[145, 300]], 'div-gpt-ad-1325637203686-9').addService(googletag.pubads());
	/*googletag.defineSlot('/1015505/CAM_9_120x135', [120, 135], 'div-gpt-ad-1331870501938-0').addService(googletag.pubads());*/
	/*googletag.defineSlot('/1015505/CAM_120x32_position_10_left', [120, 32], 'div-gpt-ad-1325637203686-0').addService(googletag.pubads());*/
	/*googletag.defineSlot('/1015505/CAM_15_250x32', [250, 32], 'div-gpt-ad-1342559490398-0').addService(googletag.pubads());*/

		
	googletag.pubads().enableAsyncRendering();
	googletag.enableServices();
	});
</script>

	
	
	
<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-558035-2', 'auto');
	ga('set', 'dimension1', 'guest');
 	
	ga('send', 'pageview');	ga('create', 'UA-29520199-5', 'auto', 'blocktracker');
	ga('blocktracker.send', 'pageview');	setTimeout('ga("send", "event", "TimeOnPage-timeout", "60 secs", "");', 60000);

	var _pageload_ts = new Date().getTime();
	window.addEventListener('unload', function(e) {
		if (_pageload_ts > 0) {
			var _now = new Date().getTime();
			var diff = (_now - _pageload_ts);
			ga('send', 'timing', 'PageLifetime', window.location.pathname, diff, '', {transport: 'beacon'});
			if (diff >= 60000) {
				ga("send", "event", "TimeOnPage", "60 secs", "", '', {transport: 'beacon'});
			} else if (diff >= 30000) {
				ga("send", "event", "TimeOnPage", "30 secs", "", '', {transport: 'beacon'});
			}
		}
	}, false);
</script>
	<meta property="og:site_name" content="Canuck Audio Mart" />
	<meta property="og:title" content="Privacy Policy - Canuck Audio Mart" />
	<meta property="og:type" content="website" />
				
	<script type="text/javascript">(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=105872462774941";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
</head>

<body class="desktop cam">
<div id="fb-root"></div>

	<div id="header">
	<div id="header_left">
		<a href="http://www.canuckaudiomart.com/" title="Canuck Audio Mart - Canadian Audio/Video Classifieds"><img src="http://www.canuckaudiomart.com/images/cam/audio_video_logo.gif" class="logo" alt="Canuck Audio Mart - Canadian Audio/Video Classifieds" border="0" /></a>
		<a href="http://www.canuckaudiomart.com/" title="Canuck Audio Mart - Canadian Audio Video &amp; hometheater Classifieds"><img src="http://www.canuckaudiomart.com/images/cam/text_logo.gif" id="textlogo" alt="Canuck Audio Mart - Canada's largest online Hifi/Audio Classifieds Site!" border="0" /></a>
		<div class="free-classifieds">

		</div>
	</div>
		<div id="miniads">
		

<!-- CAM_120x32_position_10_left -->

	<div style="margin-left:25px;margin-right:10px;" class="pull-left">
		<a href="/partnerlink/aHR0cHM6Ly9wdWJhZHMuZy5kb3VibGVjbGljay5uZXQvZ2FtcGFkL2p1bXA/aXU9LzEwMTU1MDUvQ0FNXzExXzEyMHgzMiZzej0xMjB4MzImYz0yNDU1NDU=" target="_blank" rel="nofollow">
			<img src="https://pubads.g.doubleclick.net/gampad/ad?iu=/1015505/CAM_11_120x32&sz=120x32&c=245545" alt="" />
		</a>
	</div>

	
	
	

	</div>
		<div id="header_right">
		<div class="navbar guest">
			<div class="navbar-inner">
				<div class="container">
					<div class="nav-collapse">
			<ul class="nav navbar-nav">
							<li><a href="#register_modal" data-toggle="modal" onclick="$('#register_modal form input[name=do]').val('');">Register</a></li>
				<li><a href="https://www.canuckaudiomart.com/login.php">Login</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="fb-like" data-href="https://www.facebook.com/CanuckAudioMart" data-width="225" data-layout="standard" data-action="like" data-show-faces="false" data-share="false" style="display:inline-block;width:225px;margin-top:16px;height:22px;overflow:hidden;"></div>
		<a class="placead_btn" href="#register_modal" data-toggle="modal" onclick="$('#register_modal form input[name=do]').val('add ad');" title="Place an Ad on the Canadian A/V Classifieds">Place an Ad <small>Free for hobbyists!</small></a>
		
	</div>
	<div style="clear:both"></div>
</div>

<div id="navbar">
	<div class="pull-left">
		<ul id="martlist" class="nav nav-pills">
									<li class="active"><a href="http://www.canuckaudiomart.com/" target="_blank" title="audio hifi classifieds in Canada"><img src="http://www.canuckaudiomart.com/images/cam/mini_icon.png" title="Canuck Audio Mart" width="16" height="16" />Canada</a></li>
		
												<li><a href="http://www.usaudiomart.com/" target="_blank" title="audio hifi classifieds in America"><img src="http://www.canuckaudiomart.com/images/usam/mini_icon.png" title="US Audio Mart" width="16" height="16" />United States</a></li>
		
												<li><a href="http://www.ukaudiomart.com/" target="_blank" title="audio hifi classifieds in United Kingdom and Europe"><img src="http://www.canuckaudiomart.com/images/ukam/mini_icon.png" title="UK Audio Mart" width="16" height="16" />UK / Europe</a></li>
		
											<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">More in <b class="caret"></b></a>
				<ul class="dropdown-menu">
							<li><a href="http://www.aussieaudiomart.com/" target="_blank" title="audio hifi classifieds in Australia and New Zealand"><img src="http://www.canuckaudiomart.com/images/ausam/mini_icon.png" title="Aussie Audio Mart" width="16" height="16" />Australia / NZ</a></li>
		
							</ul>
			</li>
							</ul>
	</div>
	<div class="pull-right">
		<ul class="nav nav-pills">
			<li><a href="http://www.canuckaudiomart.com/forum/" title="Canadian Audio/Video &amp; Home Theater Forums">Forums</a></li>
			<li><a href="http://www.canuckaudiomart.com/gallery.php" title="Canadian Hifi/Audio, Video, &amp; Home Theater Gallery">Gallery</a></li>
					<li><a href="#register_modal" data-toggle="modal" onclick="$('#register_modal form input[name=do]').val('add ad');" title="Place an Ad on the Canadian A/V Classifieds">Place Ads</a></li>
			<li><a href="#register_modal" data-toggle="modal" onclick="$('#register_modal form input[name=do]').val('add notify');" title="Receive email notifications on new Ads in Canadian A/V classifieds">Ad Notification</a></li>
					<li><a href="http://www.canuckaudiomart.com/reviews/">Reviews</a></li>
			<li><a href="http://www.canuckaudiomart.com/dealers.php">Dealers</a></li>
			<li><a href="http://www.canuckaudiomart.com/premium.php">Premium</a></li>			<li><a href="http://www.canuckaudiomart.com/members.php">Members</a></li>
			<li><a href="http://www.canuckaudiomart.com/faq.php">FAQ</a></li>
		</ul>
	</div>
	<div class="clear"></div>
</div>



<script type="text/javascript">
	scroll_to_fix = function() {
		var $left_content = $("#left_content", "#leftmenu");
		var $left_div = $("#leftmenu");

		var $right_content = $("#right_content", "#rightmenu");
		var $right_div = $("#rightmenu");

		if ($left_div.length && $left_content.length) { // make sure all element exist
			if ($left_div.height() > $left_content.height()) {
				$left_content.scrollToFixed($left_div);
			}
		}

		if ($right_div.length && $right_content.length) {
			if ($right_div.height() > $right_content.height()) {
				$right_content.scrollToFixed($right_div);
			}
		}
	};
	$(document).ready(function(){
		$('#usam-help').bind('closed', function () {
			$.cookie('show_usam_help','false',{ expires: 3650, path: '/'})
		});
		$('#submit_spam_clear').on('click',function() {
			if (!$('#confirm_spam_clear').is(":checked")) {
				alert('You must indicate you understand you cannot mark emails as SPAM.');
				return false;
			}
		});
		$('#submit_bounce_clear').on('click',function() {
			if (!$('#confirm_bounce_clear').is(":checked")) {
				alert('You must indicate that you checked your email address is valid.');
				return false;
			}
		});
		if (!BrowserDetect.show_mobile_mode()) {
			scroll_to_fix();
		}
	});
</script>
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
				<td id="leftmenu" class="sidecontent" valign="top">
		<div id="left_content">
			<div class="precontent" style="margin:0.5em;">
				
	<div align="center">
		<!-- komoona code start -->
		
		
	</div>
	
	<!-- komoona code end -->
	<!-- CAM_1_120x135 -->
	
	<div style="width:120px; height:135px;margin-bottom:1em;">
		<a href="/partnerlink/aHR0cHM6Ly9wdWJhZHMuZy5kb3VibGVjbGljay5uZXQvZ2FtcGFkL2p1bXA/aXU9LzEwMTU1MDUvQ0FNXzFfMTIweDEzNSZzej0xMjB4MTM1JmM9MjQ1NTQ1" target="_blank" rel="nofollow">
			<img src="https://pubads.g.doubleclick.net/gampad/ad?iu=/1015505/CAM_1_120x135&sz=120x135&c=245545" alt="" />
		</a>
	</div>
	
		</div>
	
			<h3>Search A/V Classifieds</h3>
		<form class="form-inline" action="http://www.canuckaudiomart.com/search.php" style="margin-bottom: 0.5em">
			<div class="input-group">
				<input type="text" name="keywords" placeholder="Keywords, ID" class="form-control" />
				<button type="submit" class="btn btn-default"><i class="icon-search"></i></button>
			</div>
		</form>
		<ul class="list-unstyled">
			<li><a href="http://www.canuckaudiomart.com/classifieds/all/">Browse All Ads</a></li>
			<li><a href="http://www.canuckaudiomart.com/search.php">Advanced Search</a></li>
			<li><a href="http://www.canuckaudiomart.com/classifieds/all/?filter=WANTED">Wanted Ads</a></li>
		</ul>
		<h3>Our Features</h3>
		<ul class="list-unstyled">
			<li><a href="http://www.canuckaudiomart.com/brands/">Brand Catalogue</a></li>
			<li><a href="http://www.canuckaudiomart.com/reviews/">Consumer Review</a></li>
							<li><a href="http://audiomart.spreadshirt.ca/" target="_blank" rel="nofollow">CAM Merchandise</a></li>					</ul>
		<h3>See also</h3>
		<ul class="list-unstyled">
			<li><a href="http://www.mywatchmart.com/?utm_source=cam&utm_campaign=audiomartlink&utm_medium=web&utm_content=leftmenulink" target="_blank" rel="nofollow">WatchMart</a></li>
		</ul>
		<h3>Find User</h3>
		<form class="form-inline" action="http://www.canuckaudiomart.com/find.php" id="find_form">
			<input type="hidden" value="1" name="submit" />
			<div class="input-group">
				<input type="text" name="user" placeholder="Username" class="form-control" title="Find users by username" />
				<button type="submit" class="btn btn-default"><i class="icon-search"></i></button>
			</div>
		</form>
	
			<!-- Begin Banner Code -->
		<div class="precontent offset">
				<!-- CAM_2_120x135 -->
			<div style="width:120px; height:135px;margin-bottom:1em;">
		<a href="/partnerlink/aHR0cHM6Ly9wdWJhZHMuZy5kb3VibGVjbGljay5uZXQvZ2FtcGFkL2p1bXA/aXU9LzEwMTU1MDUvQ0FNXzJfMTIweDEzNSZzej0xMjB4MTM1JmM9MjQ1NTQ1" target="_blank" rel="nofollow">
		<img src="https://pubads.g.doubleclick.net/gampad/ad?iu=/1015505/CAM_2_120x135&sz=120x135&c=245545" alt="" />
		</a>
	</div>

	<div style="width:120px; height:135px;margin-bottom:1em;">
		<a href="/partnerlink/aHR0cHM6Ly9wdWJhZHMuZy5kb3VibGVjbGljay5uZXQvZ2FtcGFkL2p1bXA/aXU9LzEwMTU1MDUvQ0FNXzNfMTIweDEzNSZzej0xMjB4MTM1JmM9MjQ1NTQ1" target="_blank" rel="nofollow">
		<img src="https://pubads.g.doubleclick.net/gampad/ad?iu=/1015505/CAM_3_120x135&sz=120x135&c=245545" alt="" />
		</a>
	</div>

	<div style="width:120px; height:135px;margin-bottom:1em;">
			<a href="/partnerlink/aHR0cHM6Ly9wdWJhZHMuZy5kb3VibGVjbGljay5uZXQvZ2FtcGFkL2p1bXA/aXU9LzEwMTU1MDUvQ0FNXzRfMTIweDEzNSZzej0xMjB4MTM1JmM9MjQ1NTQ1" target="_blank" rel="nofollow">
		<img src="https://pubads.g.doubleclick.net/gampad/ad?iu=/1015505/CAM_4_120x135&sz=120x135&c=245545" alt="" />
				</a>
	</div>

	
		</div>
		<!-- End Banner Code -->
	
			<h3>Recent Classifieds</h3>
		<ul class="list-unstyled">
							<li style="word-wrap:break-word;width:150px;"><a href="http://www.canuckaudiomart.com/details/649264533-thorens-td125-with-custom-plinth/">DEALER AD: Thorens TD-125 with custom plinth</a></li>
							<li style="word-wrap:break-word;width:150px;"><a href="http://www.canuckaudiomart.com/details/649264532-reference-3a-mm-gen2-bookshelf-speakers-with-standsmade-in-france/">DEALER AD: Reference 3a MM Gen2 bookshelf speakers with stands(made in France)</a></li>
							<li style="word-wrap:break-word;width:150px;"><a href="http://www.canuckaudiomart.com/details/649264531-ortofon-2m-bronze-mm-cart/">FOR SALE: Ortofon 2M Bronze MM Cart</a></li>
							<li style="word-wrap:break-word;width:150px;"><a href="http://www.canuckaudiomart.com/details/649264530-dynaco-sca80q-integrated-amplifier/">DEALER AD: Dynaco SCA-80Q integrated amplifier</a></li>
					</ul>
	
			<h3>User Galleries</h3>
		<div align="center">
			<a href="http://www.canuckaudiomart.com/view_userimages.php?user_id=21651&amp;image_id=17749" style="text-decoration: none" title="SA-6800 amp saved from the chop shop"><div class="left-menu-gallery-multiline" style="background: url(//img.canuckaudiomart.com/uploads/19/user_21651_thumb_3dbc976f2d5617b804713a2428b3374d.jpg);">
				<span>mechstek's Gallery</span>
			</div></a>
			<a href="http://www.canuckaudiomart.com/gallery.php"><strong>See the Member Gallery</strong></a><br /></div>
		<br />
	<div id="dealeradstxt">
		<h5>DEALER ADS WELCOME</h5>
		
		<a href="http://www.canuckaudiomart.com/dealer_how_to.php">Click here for more information</a>
	</div>
</div>
<div class="sidemenu-placeholder"></div>
		</td>
				<!-- center column -->
		<td id="main" valign="top" width="*">
							<!-- BEGIN Banner Code -->
<div class="precontent top" style="height: 164px;">
	<table style="margin:0 auto 12px auto;">
		<tr>
			<td style="padding:0 8px 0 0;">
			
				<!--/* OpenX Javascript Tag v2.8.5-rc7 */-->

				


				<!-- Begin Banner Code Zone J 230x60 -->
			<!-- CAM_6-1_230x60 -->
								<div>
					<a href="/partnerlink/aHR0cHM6Ly9wdWJhZHMuZy5kb3VibGVjbGljay5uZXQvZ2FtcGFkL2p1bXA/aXU9LzEwMTU1MDUvQ0FNXzYtMV8yMzB4NjAmc3o9MjMweDYwJmM9MjQ1NTQ1" target="_blank" rel="nofollow">
						<img src="https://pubads.g.doubleclick.net/gampad/ad?iu=/1015505/CAM_6-1_230x60&sz=230x60&c=245545" alt="" />
					</a>
				</div>
							</td>
			<td>
				<!-- right hand side -->
				<!-- CAM_6-2_230x60 -->
				<div id='div-gpt-ad-1325637203686-7' style='width:230px; height:60px;'>
					<script type='text/javascript'>
						googletag.cmd.push(function() { googletag.display('div-gpt-ad-1325637203686-7'); });
					</script>
				</div>
			</td>
		</tr>
	</table>
	<!-- <br /><br />
				<a href="http://www.everestaudio.com" target="_blank"><img src="http://www.canuckaudiomart.com/partnerimages//everestaudio/newest-ani.gif" width="468" height="60" border="1" alt="everest audio" /></a> -->


	<!-- CAM_5_468x60 -->
	<div class="cam_5_648x60">
		<div id='div-gpt-ad-1325637203686-5' class="center">
			<script type='text/javascript'>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1325637203686-5'); });
			</script>
		</div>
	</div>


</div>
<!-- End Banner Code -->
				


		
			<div class="well well-small hidden-xs">Welcome to CAM, a classifieds site for used audio, hifi and stereo. It is completely <strong>free</strong> for hobbyists to post classifieds here.  <a href="register.php" data-target="#register_modal" data-toggle="modal">Register today</a> and start selling your audio gear!</div>
	

		
				<center><h2>Canuck Audio Mart's Privacy Policy</h2></center>

<p>
We here at Canuck Audio Mart and its Operators, Canuck Digital Media Inc. recognize that the relationship with our visitors is one of our most valuable assets. We therefore understand and respect the need for protecting your privacy in order to establish trust and to maintain the integrity of our company. The following is the Privacy Policy for Canuck Audio Mart (www.canuckaudiomart, "we" or "us") on the purpose, intent, use, and protection of the information you (the user of the site) provide.
</p>
<p>
Note: This policy only applies to any information disclosed while dealing directly with Canuck Audio Mart (http://www.canuckaudiomart.com/). Once you leave our web site to go to another web site, or provide information to other users of the site, we no longer assume responsibility of any information that may be collected, disclosed, and/or manipulated.
</p>

<u><strong>About the Information We Collect:</strong></u>

<p>
When you visit Canuck Audio Mart, you may provide information that is personal information (i.e. any information that can identify an individual) that you knowingly choose to disclose, and information for the World Wide Web in general. In no circumstance would you be subject to any other involuntary disclosure of information. However, in order to use certain services, you may be required to disclose certain personal or private information, but it ultimately remains at your discretion to do so or not. These certain services also may require the use of cookies and/or the propagation of session information. Cookies and sessions information help facilitate and make the visit to our site more "user friendly," but we still treat them as personal information and in accordance with this Privacy Policy.
</p>

<u><strong>Use and Disclosure of the Information We Collect:</strong></u>

<p>
Canuck Audio Mart will only use the aforementioned information to facilitate your transactions on our site and to enhance you overall experience of the site. We do NOT sell, lease, rent, or in any way make available to third parties, any information that is linkable to you individually that you provided us, without express consent from you as indicated through information forms that you voluntarily fill out.
</p>

<p>
Canuck Audio Mart will take reasonable precautionary measures to protect and safeguard all information gathered about you while using our site. Canuck Audio Mart does not, however, take any responsibility for any breach of security, accidental disclosure, theft, or tampering of your information.
</p>

<u><strong>Your Consent:</strong></u>
<p>
By using our web site (http://www.canuckaudiomart.com/), it constitutes acceptance of the details outlined in this Privacy Policy. Canuck Audio Mart reserves the right to modify this Privacy Policy at any time.
</p>

<strong><u>Privacy Contact Info:</u></strong>
<br /><br />
Canuck Digital Media Inc.<br />
P.O. Box 27531<br />
Oakridge<br />
Vancouver, BC, Canada<br />
V5Z 4M4<br />



		
		<div class="precontent">

		
									<!-- CAM_7_468x60 -->
				<div id='div-gpt-ad-1325637203686-8' style='min-height:60px;margin: 30px auto;'>
					<script type='text/javascript'>
						googletag.cmd.push(function() { googletag.display('div-gpt-ad-1325637203686-8'); });
					</script>
				</div>
								<br />
			<h3 class="free">Canuck Audio Mart is FREE!</h3>
		</div>
			
		</td>
				<!-- right column -->
		<td id="rightmenu" class="sidecontent" valign="top" width="160">
		<div id="right_content">



		<div align="center" style="margin: 4px 0;">
		<!-- CAM_8_120x135 -->
		<div id='div-gpt-ad-1325637203686-9' style='min-width:120px; min-height:135px;'>
			<script type='text/javascript'>
				googletag.cmd.push(function() { googletag.display('div-gpt-ad-1325637203686-9'); });
			</script>
		</div>

		
	</div>

	<h3>Forum Messages</h3>
	<ul class="list-unstyled">
		
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42189">USA buyers at CAM???</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42156">19 year old Toronto record seller....</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42178">Phono cable replacement</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=41814">Snap Crackle Pop</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=41986">Amp for B&amp;W 603 S3...?</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42187">Shopping for HiFi in Singapore</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42143">All Things Must Pass/Tower Records</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42194">Thorens e50 motor rebuild</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42188">Klipsch Heressy II speaker questions.</a></li>
			
					
			
			<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42183">Record company profits and the surprising cash cow</a></li>
			
			</ul>

	<h3>Announcements</h3>
	<ul class="list-unstyled">
		
					
							<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=41417">Major Changes to Premium User Program and Free User Accounts</a></li>
								
								
						</ul>

	<h3>Dealer News</h3>

	<ul class="list-unstyled">
		<li><a href="http://www.canuckaudiomart.com/classifieds/all/?filter=DEALER+AD" style="font-weight:bold;">&raquo; Browse Dealer Ads</a></li>
					<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=41307">Wolf Ear Audio. A new business specializing in tube audio!</a></li>
					<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42193">20% OFF SALE!!!</a></li>
					<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42192">Tru-Lift automatic tonearm lift</a></li>
					<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42190">Sonneteer and Gutwire at Spinners Sound Centre!</a></li>
					<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42170">LINN LP12 KLINIC</a></li>
					<li><a href="http://www.canuckaudiomart.com/forum/viewtopic.php?t=42159">Montreal Salon Audio &amp; Being Featured in Stereophile</a></li>
			</ul>


	<h3>Ad Stats</h3>
	<ul class="list-unstyled">
		<li>Last 48 hrs: <a href="http://www.canuckaudiomart.com/search.php?search=1&amp;days_ago=2&amp;show_other_marts=Y" title="Click to view ads posted in the past 2 days">371</a></li>
		<li>Active: 3158</li>
		<li>Total: 518113</li>
			</ul>
	<h3>Replies Stats</h3>
	<ul class="list-unstyled">
		<li>Last 48 hrs: 764</li>
		<li>Total: 1006503<br /><small>(since Nov 25th, 2004)</small></li>
			</ul>
	
<h3>Info</h3>
<ul class="list-unstyled">
	<li><a href="http://www.canuckaudiomart.com/advertising.php">Advertising Info</a></li>
</ul>


			<div style="margin-top:20px;">
			<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<!-- CAM_160x600 -->
			<ins class="adsbygoogle"
				 style="display:inline-block;width:160px;height:600px"
				 data-ad-client="ca-pub-9579993617424571"
				 data-ad-slot="4339428640"></ins>
			<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
			</script>
		</div>
	</div>
<div class="sidemenu-placeholder"></div>
		</td>
			</tr>
</table>


<div id="footer">
	<img src="http://www.canuckaudiomart.com/images/cam/made_in.gif" width="54" height="53" alt="Proud to be a Canadian Audio/Video and Home Theater site" border="0" id="canadian" />	<div id="footerlinks">
		<a href="https://www.facebook.com/CanuckAudioMart"><i class="icon-facebook-square icon-2x" title="Canuck Audio Mart on Facebook!"></i></a>
		<a href="https://twitter.com/CanuckAudioMart"><i class="icon-twitter-square icon-2x" title="Canuck Audio Mart on Twitter!"></i></a>
		<a href="http://www.canuckaudiomart.com/">Home</a>
		<a href="http://www.canuckaudiomart.com/aboutus.php">About Us</a>
		<a href="http://www.canuckaudiomart.com/forum/">A/V Forums</a>
		<a href="http://www.canuckaudiomart.com/advertising.php">Advertise</a>
		<a href="http://www.canuckaudiomart.com/links.php">Links</a>
		<a href="http://www.canuckaudiomart.com/scams.php">Avoid being scammed!</a>
		<a href="http://www.canuckaudiomart.com/privacy.php">Privacy Policy</a>
		<a href="http://www.canuckaudiomart.com/termsofuse.php">Terms of Use</a>
		<a href="http://www.canuckaudiomart.com/contact_us.php">Contact Us</a>
		<a href="http://www.canuckaudiomart.com/support_us.php">Support Us</a>			</div>
			<div class="pull-right" style="margin:10px 5px;">Check out our sister site <a href="http://www.mywatchmart.com/?utm_source=cam&utm_campaign=audiomartlink&utm_medium=web&utm_content=footerlink" target="_blank" title="watch classifieds updated daily" rel="nofollow">WatchMart</a>.</div>
			<div id="copyright">&copy Canuck Audio Mart. 2000-2016</div>
	
	</div>
<small>Elapsed: 0.8461</small>

<br /><br />


	

<script type="text/javascript">// <![CDATA[
	(function(){
		var test = document.createElement('div');
		test.innerHTML = '&nbsp;';
		test.className = 'adsbox';
		document.body.appendChild(test);
		window.setTimeout(function() {
			whetherdo = Math.floor((Math.random() * 100) + 1);
			if (whetherdo < 50) {
				if (test.offsetHeight === 0) {
					ga('blocktracker.send', 'event', 'Adblock', 'Unblocked','false',{ nonInteraction: true } );
				} else {
					ga('blocktracker.send', 'event', 'Adblock', 'Blocked','true',{ nonInteraction: true });

									}
			}
			test.remove();
		}, 100);
	})();
	// ]]>
</script>


<div class="modal fade" id="register_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<a class="close" data-dismiss="modal">&times;</a>
				<h3 class="modal-title">Register an account on Canuck Audio Mart</h3>
			</div>
			<div class="modal-body">
				<form action="register.php" method="get" onsubmit="if ($('input[name=do]', this).val()==''){ $('input[name=do]', this).remove(); }">
					<input type="hidden" name="do" value="" />
					<div id="stage1" class="row">
						<div class="col-md-6">
							<button type="submit" name="utype" value="dealer1" class="btn btn-warning btn-block">
								<h3 style="color: #fcfcfc;">Commercial User</h3>
								<div>$150CAD a year</div>
							</button>
														<div style="margin: 1em;">
								<h5 class="hidden-xs">What you'll get</h5>
								<ul class="hidden-xs">
									<li>A business profile</li>
									<li>Dealer listing and icon</li>
									<li><a href="http://www.canuckaudiomart.com/dealer_how_to.php">See more</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary btn-block">
								<h3 style="color: #fcfcfc;">Casual User</h3>
								<div>FREE for hobbyists!</div>
							</button>
							<div class="hidden-xs" style="margin: 1em;">
								<ul>
									<li>Free classifieds postings</li>
									<li>Access a forum of audio enthusiasts</li>
								</ul>
							</div>
						</div>
					</div>
										<div class="clearfix">
						<h3>Already a member? <small><a href="https://www.canuckaudiomart.com/login.php" class="btn btn-info btn-lg login_modal">Login now</a></small></h3>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
				$('#register_modal').modal( { show: false })
				.on('show.bs.modal', function(e) {
					var classname = $("input[name='do']", this).val();
					var cfid_action_array = ["add watchlist"];
					var userid_action_array = ["add hotlist"];
					var info = ({ action: 'set_login_session', classname: classname });
					var click_element = $(e.target);
					if (jQuery.inArray(classname, cfid_action_array) >= 0) {
						info.cfid = click_element.data('cfid');
					} else if (jQuery.inArray(classname, userid_action_array) >= 0) {
						info.friend_id = click_element.data('friend_id');
					}
					$.post("/api_handler.php", info);
				})
				.on('shown.bs.modal', function() {
					ga('send', 'pageview', '/register_modal.php');
				});
		$('.login_modal', '#register_modal').on('click', function(e) {
			e.preventDefault();
			$("h3", '#register_modal .modal-header').html("Canuck Audio Mart Account Login");
			var login_form = "<form action='https://www.canuckaudiomart.com/login.php' method='POST'>"+
					"<table class='table login_table'>"+
					"<tr><th>Username / Email address:</th><td><input type='text' name='username' value='' class='form-control input-large' /></td></tr>"+
					"<tr><th>Password:</th><td><input type='password' name='password' class='form-control input-large' /></td></tr>"+
					"<tr><th></th><td><input type='submit' name='submit' value='Login' class='btn btn-lg btn-primary'/> <small style='margin-left:2em;'><a href='http://www.canuckaudiomart.com/forgot.php'><strong>Forgot your password?</strong></a></small></td></tr>"+
					"</table>"+
					"</form>";
			$("form", "#register_modal .modal-body").replaceWith(login_form);
		});
	});
</script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"5c8bdb1280","applicationID":"437011","transactionName":"ZwZbMEUEWEsAARdeWF5MbBZeSkZKDggGVENDTFoFWRBVUwAXB15YXQJLEBgVQ1oNCwBoX0QOVUtHF19OAAEaGUdYEw==","queueTime":0,"applicationTime":857,"ttGuid":"","agentToken":"","userAttributes":"","errorBeacon":"bam.nr-data.net","agent":""}</script></body>
</html>