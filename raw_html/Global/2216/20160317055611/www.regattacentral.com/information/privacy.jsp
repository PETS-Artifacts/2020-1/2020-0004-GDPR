<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>


<title>RegattaCentral - Staff</title>






<link rel="icon" href="https://rccdn.cachefly.net/images/rc.png" type="image/png" />
<link rel="SHORTCUT ICON" href="https://rccdn.cachefly.net/images/favicon.ico" />
<link rel="icon" sizes="192x192" href="https://rccdn.cachefly.net/images/touch-icon-192x192.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="https://rccdn.cachefly.net/images/apple-touch-icon-180x180-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://rccdn.cachefly.net/images/apple-touch-icon-152x152-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://rccdn.cachefly.net/images/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://rccdn.cachefly.net/images/apple-touch-icon-120x120-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://rccdn.cachefly.net/images/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="https://rccdn.cachefly.net/images/apple-touch-icon-76x76-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://rccdn.cachefly.net/images/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="https://rccdn.cachefly.net/images/apple-touch-icon-precomposed.png">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />  



<script src="https://rccdn.cachefly.net/js/jquery-1.11.3.min.js"></script>
<script src="https://rccdn.cachefly.net/js/jquery-migrate-1.2.1.min.js"></script>
<script src="https://rccdn.cachefly.net/js/jquery-ui-1.10.4.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/css/jquery-ui-bootstrap/jquery-ui-1.10.3.custom.css" />
<script src="/wro/jqueryLibs.js"></script>
<script src="/ui/subModal/common.js"></script>
<script src="/ui/subModal/subModal.js?v=3"></script>
<link rel="stylesheet" type="text/css" href="/ui/subModal/style.css" />
<link rel="stylesheet" type="text/css" href="/ui/subModal/subModal.css?v=2" />
 <script src="https://rccdn.cachefly.net/js/tablesorter/jquery.tablesorter.combined.js"></script>
<link href="/js/tablesorter/style.css" rel="stylesheet" type="text/css" />



<script src="/wro/main.js"></script>
<link href="/wro/mainStyles.css" rel="stylesheet" type="text/css" />


<script src="https://rccdn.cachefly.net/lib/owl-carousel_1.31/owl.carousel.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/lib/owl-carousel_1.31/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/lib/owl-carousel_1.31/owl.theme.css" />


  		







<script src="https://rccdn.cachefly.net/js/moment-with-langs.min.js"></script>
<script src="https://rccdn.cachefly.net/js/moment-timezone.min.js"></script>
<script src="https://rccdn.cachefly.net/js/livestamp.min.js"></script>

<script type="text/javascript" src="https://rccdn.cachefly.net/lib/tooltipster-3.3.0/js/jquery.tooltipster.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/lib/tooltipster-3.3.0/css/tooltipster.css" />
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/lib/tooltipster-3.3.0/css/themes/tooltipster-shadow.css" />




<script>
moment.lang('en');
</script>

<!--[if IE 7]>
	<link href="/css/ie-7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 8]>
	<link href="/css/ie-8.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 9]>
	<link href="/css/ie-9.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if IE 10]>
	<link href="/css/ie-10.css" rel="stylesheet" type="text/css" />
<![endif]-->
    
<script type="text/javascript">    
	    
		  jQuery(document).ready(function() {
		   		$('.tooltip').tooltipster();
		    
		  });
</script>






<script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={xpid:"UQ8BU15XGwABVVlSAgg="};window.NREUM||(NREUM={}),__nr_require=function(t,e,n){function r(n){if(!e[n]){var o=e[n]={exports:{}};t[n][0].call(o.exports,function(e){var o=t[n][1][e];return r(o||e)},o,o.exports)}return e[n].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<n.length;o++)r(n[o]);return r}({QJf3ax:[function(t,e){function n(){}function r(t){function e(t){return t&&t instanceof n?t:t?a(t,i,o):o()}function u(n,r,o){t&&t(n,r,o);for(var i=e(o),a=l(n),s=a.length,f=0;s>f;f++)a[f].apply(i,r);var u=c[v[n]];return u&&u.push([w,n,r,i]),i}function d(t,e){m[t]=l(t).concat(e)}function l(t){return m[t]||[]}function p(t){return f[t]=f[t]||r(u)}function h(t,e){s(t,function(t,n){e=e||"feature",v[n]=e,e in c||(c[e]=[])})}var m={},v={},w={on:d,emit:u,get:p,listeners:l,context:e,buffer:h};return w}function o(){return new n}var i="nr@context",a=t("gos"),s=t(1),c={},f={},u=e.exports=r();u.backlog=c},{1:24,gos:"7eSDFh"}],ee:[function(t,e){e.exports=t("QJf3ax")},{}],3:[function(t){function e(t){try{i.console&&console.log(t)}catch(e){}}var n,r=t("ee"),o=t(1),i={};try{n=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(i.console=!0,-1!==n.indexOf("dev")&&(i.dev=!0),-1!==n.indexOf("nr_dev")&&(i.nrDev=!0))}catch(a){}i.nrDev&&r.on("internal-error",function(t){e(t.stack)}),i.dev&&r.on("fn-err",function(t,n,r){e(r.stack)}),i.dev&&(e("NR AGENT IN DEVELOPMENT MODE"),e("flags: "+o(i,function(t){return t}).join(", ")))},{1:24,ee:"QJf3ax"}],4:[function(t){function e(t,e,n,i,a){try{f?f-=1:r("err",[a||new UncaughtException(t,e,n)])}catch(c){try{r("ierr",[c,(new Date).getTime(),!0])}catch(u){}}return"function"==typeof s?s.apply(this,o(arguments)):!1}function UncaughtException(t,e,n){this.message=t||"Uncaught error with no additional information",this.sourceURL=e,this.line=n}function n(t){r("err",[t,(new Date).getTime()])}var r=t("handle"),o=t(6),i=t("ee"),a=t("loader"),s=window.onerror,c=!1,f=0;a.features.err=!0,t(5),window.onerror=e;try{throw new Error}catch(u){"stack"in u&&(t(1),t(2),"addEventListener"in window&&t(3),a.xhrWrappable&&t(4),c=!0)}i.on("fn-start",function(){c&&(f+=1)}),i.on("fn-err",function(t,e,r){c&&(this.thrown=!0,n(r))}),i.on("fn-end",function(){c&&!this.thrown&&f>0&&(f-=1)}),i.on("internal-error",function(t){r("ierr",[t,(new Date).getTime(),!0])})},{1:10,2:9,3:7,4:11,5:3,6:25,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],5:[function(t){t("loader").features.ins=!0},{loader:"G9z0Bl"}],6:[function(t){function e(){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var n=t("ee"),r=t("handle"),o=t(1),i=t(2);t("loader").features.stn=!0,t(3);var a=NREUM.o.EV;n.on("fn-start",function(t){var e=t[0];e instanceof a&&(this.bstStart=Date.now())}),n.on("fn-end",function(t,e){var n=t[0];n instanceof a&&r("bst",[n,e,this.bstStart,Date.now()])}),o.on("fn-start",function(t,e,n){this.bstStart=Date.now(),this.bstType=n}),o.on("fn-end",function(t,e){r("bstTimer",[e,this.bstStart,Date.now(),this.bstType])}),i.on("fn-start",function(){this.bstStart=Date.now()}),i.on("fn-end",function(t,e){r("bstTimer",[e,this.bstStart,Date.now(),"requestAnimationFrame"])}),n.on("pushState-start",function(){this.time=Date.now(),this.startPath=location.pathname+location.hash}),n.on("pushState-end",function(){r("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),"addEventListener"in window.performance&&(window.performance.clearResourceTimings?window.performance.addEventListener("resourcetimingbufferfull",function(){r("bstResource",[window.performance.getEntriesByType("resource")]),window.performance.clearResourceTimings()},!1):window.performance.addEventListener("webkitresourcetimingbufferfull",function(){r("bstResource",[window.performance.getEntriesByType("resource")]),window.performance.webkitClearResourceTimings()},!1)),document.addEventListener("scroll",e,!1),document.addEventListener("keypress",e,!1),document.addEventListener("click",e,!1)}},{1:10,2:9,3:8,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],7:[function(t,e){function n(t){for(var e=t;e&&!e.hasOwnProperty("addEventListener");)e=Object.getPrototypeOf(e);e&&r(e)}function r(t){a.inPlace(t,["addEventListener","removeEventListener"],"-",o)}function o(t){return t[1]}var i=t("ee").get("events"),a=t(1)(i),s=t("gos");e.exports=i,r(window),"getPrototypeOf"in Object?(n(document),n(XMLHttpRequest.prototype)):XMLHttpRequest.prototype.hasOwnProperty("addEventListener")&&r(XMLHttpRequest.prototype),i.on("addEventListener-start",function(t){if(t[1]){var e=t[1];if("function"==typeof e){var n=s(e,"nr@wrapped",function(){return a(e,"fn-",null,e.name||"anonymous")});this.wrapped=t[1]=n}else"function"==typeof e.handleEvent&&a.inPlace(e,["handleEvent"],"fn-")}}),i.on("removeEventListener-start",function(t){var e=this.wrapped;e&&(t[1]=e)})},{1:26,ee:"QJf3ax",gos:"7eSDFh"}],8:[function(t,e){var n=t("ee").get("history"),r=t(1)(n);e.exports=n,r.inPlace(window.history,["pushState","replaceState"],"-")},{1:26,ee:"QJf3ax"}],9:[function(t,e){var n=t("ee").get("raf"),r=t(1)(n);e.exports=n,r.inPlace(window,["requestAnimationFrame","mozRequestAnimationFrame","webkitRequestAnimationFrame","msRequestAnimationFrame"],"raf-"),n.on("raf-start",function(t){t[0]=r(t[0],"fn-")})},{1:26,ee:"QJf3ax"}],10:[function(t,e){function n(t,e,n){t[0]=i(t[0],"fn-",null,n)}function r(t,e,n){this.method=n,this.timerDuration="number"==typeof t[1]?t[1]:0,t[0]=i(t[0],"fn-",this,n)}var o=t("ee").get("timer"),i=t(1)(o);e.exports=o,i.inPlace(window,["setTimeout","setImmediate"],"setTimer-"),i.inPlace(window,["setInterval"],"setInterval-"),i.inPlace(window,["clearTimeout","clearImmediate"],"clearTimeout-"),o.on("setInterval-start",n),o.on("setTimer-start",r)},{1:26,ee:"QJf3ax"}],11:[function(t,e){function n(t,e){e=e||this;var n=i.context(e);e.readyState>3&&!n.resolved&&(n.resolved=!0,i.emit("xhr-resolved",[],e));try{c.inPlace(e,l,"fn-",r)}catch(o){}}function r(t,e){return e}function o(t,e){for(var n in t)e[n]=t[n];return e}var i=t("ee").get("xhr"),a=t(1),s=t(2),c=s(i),f=s(a),u=NREUM.o,d=u.XHR,l=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout","onreadystatechange"];e.exports=i,window.XMLHttpRequest=function(t){var e=new d(t);try{i.emit("new-xhr",[e],e),e.hasOwnProperty("addEventListener")&&f.inPlace(e,["addEventListener","removeEventListener"],"-",r),e.addEventListener("readystatechange",n,!1)}catch(o){try{i.emit("internal-error",[o])}catch(a){}}return e},o(d,XMLHttpRequest),XMLHttpRequest.prototype=d.prototype,c.inPlace(XMLHttpRequest.prototype,["open","send"],"-xhr-",r),i.on("send-xhr-start",n),i.on("open-xhr-start",n)},{1:7,2:26,ee:"QJf3ax"}],12:[function(t){function e(t){var e=this.params,r=this.metrics;if(!this.ended){this.ended=!0;for(var o=0;u>o;o++)t.removeEventListener(f[o],this.listener,!1);if(!e.aborted){if(r.duration=(new Date).getTime()-this.startTime,4===t.readyState){e.status=t.status;var i=n(t,this.lastSize);if(i&&(r.rxSize=i),this.sameOrigin){var s=t.getResponseHeader("X-NewRelic-App-Data");s&&(e.cat=s.split(", ").pop())}}else e.status=0;r.cbTime=this.cbTime,c.emit("xhr-done",[t],t),a("xhr",[e,r,this.startTime])}}}function n(t,e){var n=t.responseType;if("json"===n&&null!==e)return e;var o="arraybuffer"===n||"blob"===n||"json"===n?t.response:t.responseText;return r(o)}function r(t){if("string"==typeof t&&t.length)return t.length;if("object"!=typeof t)return void 0;if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if("undefined"!=typeof FormData&&t instanceof FormData)return void 0;try{return JSON.stringify(t).length}catch(e){return void 0}}function o(t,e){var n=s(e),r=t.params;r.host=n.hostname+":"+n.port,r.pathname=n.pathname,t.sameOrigin=n.sameOrigin}var i=t("loader");if(i.xhrWrappable){var a=t("handle"),s=t(2),c=t("ee"),f=["load","error","abort","timeout"],u=f.length,d=t(1),l=t(3),p=window.XMLHttpRequest;i.features.xhr=!0,t(5),t(4),c.on("new-xhr",function(t){var n=this;n.totalCbs=0,n.called=0,n.cbTime=0,n.end=e,n.ended=!1,n.xhrGuids={},n.lastSize=null,l&&(l>34||10>l)||window.opera||t.addEventListener("progress",function(t){n.lastSize=t.loaded},!1)}),c.on("open-xhr-start",function(t){this.params={method:t[0]},o(this,t[1]),this.metrics={}}),c.on("open-xhr-end",function(t,e){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&e.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),c.on("send-xhr-start",function(t,e){var n=this.metrics,o=t[0],i=this;if(n&&o){var a=r(o);a&&(n.txSize=a)}this.startTime=(new Date).getTime(),this.listener=function(t){try{"abort"===t.type&&(i.params.aborted=!0),("load"!==t.type||i.called===i.totalCbs&&(i.onloadCalled||"function"!=typeof e.onload))&&i.end(e)}catch(n){try{c.emit("internal-error",[n])}catch(r){}}};for(var s=0;u>s;s++)e.addEventListener(f[s],this.listener,!1)}),c.on("xhr-cb-time",function(t,e,n){this.cbTime+=t,e?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof n.onload||this.end(n)}),c.on("xhr-load-added",function(t,e){var n=""+d(t)+!!e;this.xhrGuids&&!this.xhrGuids[n]&&(this.xhrGuids[n]=!0,this.totalCbs+=1)}),c.on("xhr-load-removed",function(t,e){var n=""+d(t)+!!e;this.xhrGuids&&this.xhrGuids[n]&&(delete this.xhrGuids[n],this.totalCbs-=1)}),c.on("addEventListener-end",function(t,e){e instanceof p&&"load"===t[0]&&c.emit("xhr-load-added",[t[1],t[2]],e)}),c.on("removeEventListener-end",function(t,e){e instanceof p&&"load"===t[0]&&c.emit("xhr-load-removed",[t[1],t[2]],e)}),c.on("fn-start",function(t,e,n){e instanceof p&&("onload"===n&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=(new Date).getTime()))}),c.on("fn-end",function(t,e){this.xhrCbStart&&c.emit("xhr-cb-time",[(new Date).getTime()-this.xhrCbStart,this.onload,e],e)})}},{1:"XL7HBI",2:13,3:15,4:11,5:7,ee:"QJf3ax",handle:"D5DuLP",loader:"G9z0Bl"}],13:[function(t,e){e.exports=function(t){var e=document.createElement("a"),n=window.location,r={};e.href=t,r.port=e.port;var o=e.href.split("://");!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=e.hostname||n.hostname,r.pathname=e.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname);var i=!e.protocol||":"===e.protocol||e.protocol===n.protocol,a=e.hostname===document.domain&&e.port===n.port;return r.sameOrigin=i&&(!e.hostname||a),r}},{}],14:[function(t,e){function n(t,e){return function(){r(t,[(new Date).getTime()].concat(i(arguments)),null,e)}}var r=t("handle"),o=t(1),i=t(2);"undefined"==typeof window.newrelic&&(newrelic=NREUM);var a=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit"],s=["addPageAction"],c="api-";o(a,function(t,e){newrelic[e]=n(c+e,"api")}),o(s,function(t,e){newrelic[e]=n(c+e)}),e.exports=newrelic,newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),r("err",[t,(new Date).getTime()])}},{1:24,2:25,handle:"D5DuLP"}],15:[function(t,e){var n=0,r=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);r&&(n=+r[1]),e.exports=n},{}],gos:[function(t,e){e.exports=t("7eSDFh")},{}],"7eSDFh":[function(t,e){function n(t,e,n){if(r.call(t,e))return t[e];var o=n();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,e,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return t[e]=o,o}var r=Object.prototype.hasOwnProperty;e.exports=n},{}],handle:[function(t,e){e.exports=t("D5DuLP")},{}],D5DuLP:[function(t,e){function n(t,e,n,o){r.buffer([t],o),r.emit(t,e,n)}var r=t("ee").get("handle");e.exports=n,n.ee=r},{ee:"QJf3ax"}],XL7HBI:[function(t,e){function n(t){var e=typeof t;return!t||"object"!==e&&"function"!==e?-1:t===window?0:i(t,o,function(){return r++})}var r=1,o="nr@id",i=t("gos");e.exports=n},{gos:"7eSDFh"}],id:[function(t,e){e.exports=t("XL7HBI")},{}],G9z0Bl:[function(t,e){function n(){if(!h++){var t=p.info=NREUM.info,e=f.getElementsByTagName("script")[0];if(t&&t.licenseKey&&t.applicationID&&e){s(d,function(e,n){t[e]||(t[e]=n)});var n="https"===u.split(":")[0]||t.sslForHttp;p.proto=n?"https://":"http://",a("mark",["onload",i()],null,"api");var r=f.createElement("script");r.src=p.proto+t.agent,e.parentNode.insertBefore(r,e)}}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()],null,"api")}function i(){return(new Date).getTime()}var a=t("handle"),s=t(1),c=window,f=c.document;NREUM.o={ST:setTimeout,XHR:c.XMLHttpRequest,REQ:c.Request,EV:c.Event,PR:c.Promise,MO:c.MutationObserver},t(2);var u=(""+location).split("?")[0],d={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-885.min.js"},l=window.XMLHttpRequest&&XMLHttpRequest.prototype&&XMLHttpRequest.prototype.addEventListener&&!/CriOS/.test(navigator.userAgent),p=e.exports={offset:i(),origin:u,features:{},xhrWrappable:l};f.addEventListener?(f.addEventListener("DOMContentLoaded",o,!1),c.addEventListener("load",n,!1)):(f.attachEvent("onreadystatechange",r),c.attachEvent("onload",n)),a("mark",["firstbyte",i()],null,"api");var h=0},{1:24,2:14,handle:"D5DuLP"}],loader:[function(t,e){e.exports=t("G9z0Bl")},{}],24:[function(t,e){function n(t,e){var n=[],o="",i=0;for(o in t)r.call(t,o)&&(n[i]=e(o,t[o]),i+=1);return n}var r=Object.prototype.hasOwnProperty;e.exports=n},{}],25:[function(t,e){function n(t,e,n){e||(e=0),"undefined"==typeof n&&(n=t?t.length:0);for(var r=-1,o=n-e||0,i=Array(0>o?0:o);++r<o;)i[r]=t[e+r];return i}e.exports=n},{}],26:[function(t,e){function n(t){return!(t&&"function"==typeof t&&t.apply&&!t[i])}var r=t("ee"),o=t(1),i="nr@original",a=Object.prototype.hasOwnProperty,s=!1;e.exports=function(t){function e(t,e,r,a){function nrWrapper(){var n,i,s,c;try{i=this,n=o(arguments),s="function"==typeof r?r(n,i):r||{}}catch(u){d([u,"",[n,i,a],s])}f(e+"start",[n,i,a],s);try{return c=t.apply(i,n)}catch(l){throw f(e+"err",[n,i,l],s),l}finally{f(e+"end",[n,i,c],s)}}return n(t)?t:(e||(e=""),nrWrapper[i]=t,u(t,nrWrapper),nrWrapper)}function c(t,r,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<r.length;c++)s=r[c],a=t[s],n(a)||(t[s]=e(a,f?s+o:o,i,s))}function f(e,n,r){if(!s){s=!0;try{t.emit(e,n,r)}catch(o){d([o,e,n,r])}s=!1}}function u(t,e){if(Object.defineProperty&&Object.keys)try{var n=Object.keys(t);return n.forEach(function(n){Object.defineProperty(e,n,{get:function(){return t[n]},set:function(e){return t[n]=e,e}})}),e}catch(r){d([r])}for(var o in t)a.call(t,o)&&(e[o]=t[o]);return e}function d(e){try{t.emit("internal-error",e)}catch(n){}}return t||(t=r),e.inPlace=c,e.flag=i,e}},{1:25,ee:"QJf3ax"}]},{},["G9z0Bl",4,12,6,5]);</script><style type="text/css">
#content {
	max-width: 800px;
	margin-right: auto;
	margin-left: auto;
	padding-left: 0px;
	padding-right: 0px;
}

}
<!--
ol {
	margin-bottom: 0in;
	list-style: disc;
	list-style-type: disc;
}

ul {
	margin-left: 0px;
	padding-left: 15px;
	margin-bottom: 0in;
}
li {
	padding-top:10px;
}
-->
</style>
</head>
<body>
	<div id="wrapper">

		




















	
	
	
	
	
	
	
	<div class="head">
    <div>
        <div class="header-container">
            <h1><a title="RegattaCentral" href="/">RegattaCentral</a></h1>
            <nav class="main">
                <ul><li><a href="/" >Home</a></li>
                    <li><a href="/regattas" >Regattas</a></li>
                    <li><a href="/results" >Results</a></li>
                    <li><a href="/clubs" >Clubs</a></li>
                    <li><a href="/programs" >Programs</a></li>
                    <li><a href="/ltr" >Learn-To-Row</a></li>
                    <li><a href="/camps" >Camps & Clinics</a></li>
                    <li><a href="/volunteer" >Volunteer</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div>
        <div class="header-container">
            <div class="align-left">
                <!-- Search -->
                <form method="post" action="/search/" id="search_global">
                    <a href="#clear" id="search_clear"><img src="https://rccdn.cachefly.net/images/icon_search_clear.png" alt="Clear" title="Clear" width="18" height="18"/></a>
                    <input type="search" name="txt_search_global" id="txt_search_global" value="" placeholder="Search" class="disabled placeholder"/>
                    <ul id="search_global_results"></ul>
                </form>
                <!-- Social Media -->
                <div>
                    <a target="_new" href="http://www.twitter.com/regattacentral"><img border="0" src="https://rccdn.cachefly.net/images/twitter.png" width="24" height="24"></a>
                    <a target="_new" href="http://www.facebook.com/pages/RegattaCentral/"><img border="0" src="https://rccdn.cachefly.net/images/facebook.png" width="24" height="24"></a>
                </div>
            </div>
            <div class="align-right">
                <!-- Support & Settings -->
                <nav class="options">
                    <ul>
                    
					
                        <li>
                            <a href="#" class="noclick">Settings &#x25bc;</a>
                            <ul>
                            
                                <li>
                                    <span class="first">Language</span>
                                    <ul>
                                        <li><a class="" href="/?lang=de" >Deutsch</a></li>
                                        <li><a href="/?lang=en" class="active">English</a></li>
                                        <li><a href="/?lang=fr" >Français</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <span>View</span>
                                    <ul>
                                        <li><a onclick="setFixedWidth();return false;" id="view_in" href="#">Default (1024p)</a></li>
                                        <li><a onclick="setFullWidth();return false;" id="view_in"href="#">Full Width</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="/support.jsp">Support</a></li>
                    </ul>
                </nav>
                
                <!-- Login / Join -->
                <div id="loginBarWrapper">
                    <div id="loginBar">
                        <div id="loginLogout">
                            <span class="vline"></span>
                            <a href="#" id="sign_in">Login</a>
                            <a style="padding-right:3px" href="/user/newUser.jsp">Join</a>
                            <div id="sign_box">
                                <form method="post" id="signinForm" action="#">
                                
                                    <input type="hidden" name="loc" value="https://www.regattacentral.com/information/privacy.jsp?null" />
                                    
										<input type="hidden" name="tab" value="merchandise" />
										
                                    <div>    
                                        <div>
                                            <label for="usr">Username</label>
                                            <input type="text" autocorrect="off" autocapitalize="off" size="15" id="uid" maxlength="120" name="uid" />
                                            <label for="password">Password</label>
                                            <input type="password" autocorrect="off" autocapitalize="off" id="pwd" size="15" maxlength="20" name="pwd" />
                                        </div>
                                        <label style="clear:both;padding-bottom:4px;white-space: nowrap" for="rememberMe">Keep me logged in</label>
                                        <input type="checkbox" value="true" id="rememberMe" name="rememberMe" />
                                    </div>
                                    <div style="clear:both;padding:5px;text-align:center;padding-top:3px;clear:both">
                                        <input id="loginBtn" type="submit" value=" Login" />
                                        <img id="loginIco" style="display:none;vertical-align: baseline;margin-bottom:-8px" src="https://regattacentral.com/images/wait.gif" border="0"/>
                                        <a href="#" style="color:blue" id="cancelLogin">cancel</a>
                                    </div>
                                </form>
                                <div id="forgotCredentials">Forgot your <a style="color:blue" id="userNmLink" href="#">username</a> or <a style="color:blue" id="pwdResetLink" href="#">password?</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
 
        

			
			<noscript>
				<div class="error">JavaScript is required for this website.	Please edit your browser's settings and enable JavaScript to ensure	RegattaCentral functions properly</div>
			</noscript>
		

	<div style="display:none" id="userNmReminder" title="Username Reminder">
	<form id="userNmFrm">
		    <fieldset>
		 		<label for="name">Email Address</label>
		        <input id="userNmEmail" type="text" name="email" value="" maxlength="128" size="30" />
		        <input type="hidden" name="csrfToken" value="7782749284367" />
		        </fieldset>
		        </form>
	</div>
	<div style="display:none" id="pwdReset" title="Password Reset">
		<form id="pwdResetFrm">
		    <fieldset>
		        <label for="name">Username</label>
		        <input id="pwdResetUsername" type="text" name="userName" value="" maxlength="128" size="30" />
		        <input type="hidden" name="csrfToken" value="7782749284367" />
		    </fieldset>
	    </form>
	</div>


		<div id="content">

			<div style="padding-left: 15px; padding-right: 10px;padding-top:10px; float: right">
			<div id="5c75658b-489b-42b9-9355-291692f01df8"> <script type="text/javascript" src="//privacy-policy.truste.com/privacy-seal/RegattaCentral,-LLC/asc?rid=5c75658b-489b-42b9-9355-291692f01df8"></script><a href="//privacy.truste.com/privacy-seal/RegattaCentral,-LLC/validation?rid=5b39dee0-f81f-457e-8475-ab1f1f9e29cd" title="TRUSTe Privacy Certification" target="_blank"><img style="border: none" src="//privacy-policy.truste.com/privacy-seal/RegattaCentral,-LLC/seal?rid=5b39dee0-f81f-457e-8475-ab1f1f9e29cd" alt="TRUSTe Privacy Certification"/></a></div>
				<div>
					<br />
					<b>RegattaCentral Privacy Policy</b> <br />
					<b> </b> <br />
					<b>Effective Date: 10 June 2015</b> <br />
					<b> </b> <br />
					<b>Introduction</b> <br /> <br />At RegattaCentral we understand
					that your privacy is important to you and we are committed to
					complying with the applicable privacy laws in relation to all
					Personal Information and Sensitive Information that we collect from
					you. This policy describes our Privacy and Information handling
					practices. <br /> <br />By accessing and using this website
					(www.regattacentral.com) or other websites owned and operated by
					RegattaCentral, you accept and agree to RegattaCentral&#8217;s
					Privacy Policy without limitation or qualification. <br />Definitions
					of specific terms referred to in this Policy can be found under the
					heading &#8216;Glossary&#8217; at the end of this document. <br />
					<b> </b> <br />
					RegattaCentral has received TRUSTe's Privacy Seal signifying that this Privacy Policy and our practices have been reviewed for compliance with the TRUSTe program viewable on the validation page available by clicking the TRUSTe seal. The TRUSTe program covers only information that is collected through this website, www.regattacentral.com. If you have an unresolved privacy or data use concern that we have not addressed satisfactorily, please contact TRUSTe <a href="https://feedback-form.truste.com/watchdog/request">here</a>.
					<br/><br/>
					<b>Privacy Policy</b> <br />
					<b> </b> <br />This Policy outlines the following: <br /> <br />1.
					The types of Personal Information that we collect and hold. <br />2.
					How we collect Personal Information. <br />3. The purposes for
					which the information is collected. <br />4. The way in which we
					use and disclose Personal Information. <br />5. The steps we take
					to protect and keep secure the Personal Information we hold. <br />6.
					How individuals can access and correct their Personal Information
					held by us. <br />7. Mechanisms for complaint if you are unhappy
					about how we have dealt with or are dealing with your Personal
					Information. <br /> <br />
					<b>1. </b><b>The types of Personal Information that we collect
						and hold</b> <br />
					<b> </b> <br />In the normal course of business activities we will
					collect Personal Information from you and this may include
					collecting some Sensitive Information. <br /> <br />The following
					are the types of your Personal Information and Sensitive
					Information that we may collect and hold: <br /> <br />&#183; We
					collect personal details such as name, postal and email addresses,
					phone numbers, date of birth, age, gender, height and rowing
					history. <br /> <br />This may also include copies of documents
					provided to us as proof of identity. It may also include Sensitive
					Information about individuals such as health information and
					information regarding racial or ethnic origin. <br /> <br />&#183;
					If you order any products from our website, we also collect payment
					details such as credit card number and billing address. <br /> <br />&#183;
					In registering for an event or program, you may provide Personal
					Information or Sensitive Information to us for the purpose of
					attending the event or program. We use that information for that
					purpose and may provide information to you regarding our other
					services. <br /> <br />
					<b>2. </b><b>How we collect Personal Information</b> <br />
					<b> </b> <br />We will only collect Personal Information by lawful
					and fair means. Personal Information may be collected directly from
					you or your authorised representative. We collect Personal
					Information and Sensitive Information via our website, email or
					other direct contact. You may also supply your Personal Information
					to us when communicating with us via social networks and other
					online channels, e.g. through your Facebook identity. <br /> <br />We
					do not collect Personal and Sensitive information unless the
					information is reasonably necessary for our business functions or
					activities. We will obtain your consent before collecting any
					Sensitive Information. <br /> <br />When you deal with us, you will
					need to identify yourself in order for us to provide our services
					to you, as such we do not accept the use of pseudonyms. <br /> <br />
					<b>3. </b><b>The purposes for which the information is
						collected</b> <br />
					<b> </b> <br />We collect, hold and use your Personal Information
					so that we can provide our services including to: <br /> <br />&#183;
					assist you to register for events or programs with a local or
					international host organisation, governing body, club or event
					organiser for: <br /> <br />o rowing events, results or programs; <br />o
					camps or clinics; <br />o learn-to-row sessions; <br />o volunteer
					roles. <br /> <br />&#183; assist us, the host organisations,
					governing bodies, clubs or event organisers in verifying your
					identity and/or eligibility; <br /> <br />&#183; respond to you regarding the reason
					you contacted us or to answer your questions; <br /> <br />&#183;
					manage our internal operations and registration processes necessary
					to provide our services. <br /> <br />
					<b>4. </b><b>The way in which we use and disclose Personal
						Information</b> <br />
					<b> </b> <br />We will use or disclose your Personal Information
					only for the purposes for which it was collected. If you register
					for a particular event or program, we will share your information
					with the applicable host organisation, governing body, club or
					event organiser for that event or program, and with any other party
					which furthers the purpose of your registration. These parties are
					authorized to use your Personal Information only as necessary to
					provide these services to us. We will use or disclose your
					information for a secondary purpose only if you have consented or
					if you would reasonably expect us to do so or if required by law,
					such as to comply with a subpoena, or similar legal process. When
					we believe in good faith that disclosure is necessary to protect
					our rights, protect your safety or the safety of others,
					investigate fraud, or respond to a government request. If
					RegattaCentral is involved in a merger, acquisition, or sale of all
					or a portion of its assets, you will be notified via email and/or a
					prominent notice on our website of any change in ownership or uses
					of your Personal Information, as well as any choices you may have
					regarding your Personal Information. <br /> <br />We may use your
					Personal Information for the purposes of our own direct marketing,
					however we will ensure you have an ability to opt out of future
					such communications by following the unsubscribe instructions
					included in these emails or you can contact us at
					support@regattacentral.com. <br />
					<br/>
					We will also send you service related email announcements on rare occasions when it is necessary to do so. For instance, if our service is temporarily suspended for maintenance, we might send you an email. You do not have an option to opt out of these emails, which are not promotional in nature.
					 <br /> <br />
					<b><i>Disclosure to overseas recipients</i></b> <br />
					<b><i> </i></b> <br />We may disclose your Personal Information to
					overseas recipients such as host organisations, governing bodies,
					clubs or event organisers which may be located in Australia,
					Canada, England, Germany, Belgium, Italy or the United States of America
					when you have sought our assistance to participate in an event or
					program or where disclosure is required for verification purposes.
					As such, the location of these overseas recipients will depend on
					the specific events or programs to which you apply or where such
					verification is required. <br /> <br />The information disclosed to
					overseas recipients consists of any information you have given to
					us to provide the relevant services. By accepting services from us,
					you consent to us providing your Personal Information to the
					relevant overseas recipient as mentioned above and acknowledge that
					we are not responsible for ensuring their compliance with the
					applicable privacy laws. <br /> <br />
					<b>5. </b><b>The steps we take to protect and keep secure the
						Personal Information we hold</b> <br />
					<b> </b> <br />We take reasonable steps to protect the Personal
					Information we hold against interference, loss, unauthorised
					access, use, modification or disclosure, and other misuse. <br /> <br />The
					steps we take include: <br /> <br />&#183; maintenance of computer
					and network security measures such as including firewalls, use of
					password and other appropriate measures where information is held
					in electronic form; <br /> <br />&#183; regular updates to security
					systems to protect our systems from malicious software; <br /> <br />&#183;
					restriction of access to data to only those staff that need access
					to carry out our business activities; <br /> <br />&#183; training
					and ensuring that all our employees are required, as a condition of
					employment, to treat Personal Information held by us as
					confidential; <br /> <br />&#183; whenever we collect Sensitive
					information such as credit card data, that information is encrypted
					and transmitted to us in a secure way using secure socket layer
					technology (SSL). You can verify this by looking for a closed lock
					icon at the bottom of your web browser, or looking for
					&#8216;https&#8217; at the beginning of the address of the web
					page. <br /> <br />You should however, be aware that if you submit
					information to us electronically the internet is not a secure
					environment. We take reasonable steps to provide a secure channel
					for receiving information but cannot absolutely protect Personal
					Information before it reaches us.  If you have any questions about security on our website, you can contact us at support@regattacentral.com<br /> <br />We will retain your information for as long as your account is active or as needed to provide you services. When the Personal
					Information that we collect is no longer required, we will destroy,
					delete it in a secure manner, or ensure that the information is
					de-identified in accordance with our information destruction and
					de-identification policy, unless we are required by law to retain a
					copy of the Personal Information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. <br /> <br />
					<b>6. </b><b>How individuals can access and correct their
						Personal Information held by us</b> <br />
					<b> </b> <br />You have the right to seek access to any of your
					Personal Information held by us unless there is a valid reason
					under an applicable privacy law for us to withhold the information.
					<br /> <br />Valid reasons include where: <br /> <br />&#183; we
					have reason to suspect that unlawful activity or misconduct of a
					serious nature has been engaged in and giving access would be
					likely to prejudice the taking of appropriate action in relation to
					the matter; or <br /> <br />&#183; giving access would reveal
					evaluative information generated within us in connection with a
					commercially sensitive process; or <br /> <br />&#183; giving
					access would have an unreasonable impact on the privacy of other
					individuals; or <br /> <br />&#183; giving access would be
					unlawful. <br /> <br />If your personal details change, or you
					believe the information we hold about you is incorrect, incomplete
					or out-of-date, please contact us so that we can correct our
					records or you can make the change on your Account Information
					page. <br /> <br />Requests for access, deletion or correction to
					Personal Information should be addressed to the RegattaCentral
					Privacy Officer. All requests for access, deletion or correction to
					Personal Information will be responded to in writing within a
					reasonable period of time. As part of this process we will verify
					the identity of the individual requesting the information prior to
					providing access or making any changes. If access, deletion or
					correction to your Personal Information is refused we will provide
					reasons for our refusal. <br /> <br />
					<b>7. </b><b>Mechanisms for complaint if you are unhappy about
						how we have dealt with or are dealing with your Personal
						Information</b> <br />
					<b> </b> <br />We have procedures in place to deal with your
					inquiries or complaints. <br /> <br />If you have any questions
					about our policy or any complaint regarding the treatment of your
					privacy by us, please contact us at the respective addresses below.
					<br /> <br />
					<b><i>Complaints to RegattaCentral</i></b> <br />
					<u> </u> <br />
					<u>By post:</u> <br />Attn: Privacy Officer<br> RegattaCentral,
						LLC<br> 2029 Riverside Dr, Suite 201<br />Columbus, OH, 43221 <br />United
							States of America <br />
						<u> </u> <br />
						<u>By e-mail:</u> <br />support@regattacentral.com <br />
						<u> </u> <br />
						<u>By phone:</u> <br />(614) 360-2922 (US & Canada) <br />+61 2
							6100 4216 (Australia) <br /> <br />We will endeavour to respond
							to you within a reasonable time. If you are still not satisfied
							with the way your complaint is handled by us, you may be entitled
							to have your complaint reviewed under an applicable privacy law.
							<br /> <br />
						<b>8. Additional Information</b> <br />
						<b> </b>
							<ul>
								<li><i>Links to 3<sup>rd</sup> Party Sites
								</i></li>
							</ul> <br /> <br />Our website includes links to other websites whose
							privacy practices may differ from those of RegattaCentral. If you
							submit Personal Information to any of those sites, your
							information is governed by their privacy policies. We encourage
							you to carefully read the privacy policy of any website you
							visit. <br />
							<ul>
								<li><i>Collection and Use of 3<sup>rd</sup> Party Personal Information
								</i></li>
							</ul> <br /> <br />You may also provide Personal Information about
							other people, such as their name, date of birth and gender. This
							information is only used for the sole purpose of completing your
							request or for whatever reason it may have been provided. <br />
							<ul>
								<li><i>Social Media Widgets</i></li>
							</ul> <br /> <br />Our website includes Social Media Features, such as
							the Facebook Like button and Twitter button or interactive
							mini-programs that run on our website. These Features may collect
							your IP address, which page you are visiting on our website, and
							may set a cookie to enable the Feature to function properly.
							Social Media Features and Widgets are either hosted by a third
							party or hosted directly on our website. Your interactions with
							these Features are governed by the privacy policy of the company
							providing it. <br />
						<b> </b> <br />
						<b>Updates to this Policy</b> <br /> <br />We reserve the right to
							amend this Policy from time to time. Any revisions to the Policy
							will be posted on this website. If we make any material changes
							we will notify you by email (sent to the e-mail address specified
							in your account) or by means of a notice on this website prior to
							the change becoming effective. We encourage you to periodically
							review this page for the latest information on our privacy
							practices. <br /> <b><br clear=all></b> <br />
						<b>Glossary</b> <br />
						<b> </b>
							<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0>
								<tr>
									<td width=595 valign=top><br />1. <b><i>Personal
												Information </i></b>means information or an opinion about an
										identified individual, or an individual who is reasonably
										identifiable, whether the information or opinion is true or
										not, and whether the information or opinion is recorded in a
										material form or not.</td>
								</tr>
								<tr>
									<td width=595 valign=top><br />2. <b><i>RegattaCentral</i></b>
										means RegattaCentral, LLC.</td>
								</tr>
								<tr>
									<td width=595 valign=top><br />3. <b><i>Sensitive
												Information </i></b>includes information or an opinion about an
										individual&#8217;s racial or ethnic origin, political
										opinions, membership of a political association, religious
										beliefs or affiliations, philosophical beliefs, membership of
										a professional or trade association, membership of a trade
										union, sexual orientation or practices, or criminal record
										that is also personal information, or health information about
										an individual, or genetic information about an individual that
										is not otherwise health information, or biometric information
										used for the purpose of automated biometric verification or
										identification, or biometric templates.</td>
								</tr>
								<tr>
									<td width=595 valign=top><br />4. <b><i>We</i></b>, <b><i>us</i>
									</b>and<b> <i>our</i></b> refers to RegattaCentral.</td>
								</tr>
							</table> <br /> <br />
						<b>Website usage and other information collected on this
								website</b> <br /> <br />By accessing and using this website or
							other websites owned and operated by RegattaCentral, you accept
							and agree to RegattaCentral&#8217;s Privacy Policy without
							limitation or qualification. <br /> <br />We do not sell, rent or
							trade your Personal Information to any other third parties except
							if required by law or permitted under an applicable privacy law.
							<br /> <br />RegattaCentral&#8217;s Privacy Policy applies solely
							to information collected by us and may differ from the policies
							of other companies&#8217; or organisations&#8217; websites that
							you link to from this website. These third party links are made
							available to you as a convenience and you agree to use these
							links at your own risk. Please be aware that we are not
							responsible for the content of third party websites linked to our
							website nor are we responsible for the privacy policy or
							practices of any third parties whose website is linked to our
							website. We recommend that you read the privacy policy of any
							other companies&#8217; or organisations&#8217; website that you
							link to from our website. <br />
						<b> </b> <br />
						Technologies such as: cookies, beacons, tags and scripts are used by RegattaCentral and our marketing partners, partners, affiliates, and analytics/monitoring service providers. These technologies are used in analyzing trends, administering the website, identifying the number of visitors and page views (or page impressions) that occur on our website and how visitors use the website (e.g. how long these visitors (on average) spend on our website and common entry and exit points to our website). We may receive reports based on the use of these technologies by these companies on an individual as well as aggregated basis.<br/><br/>
						<b>Cookies/Tracking Technology</b> <br /> <br />Cookies are small
							data files that are sent from a website and stored in a
							user&#8217;s web browser to improve the user&#8217;s web browsing
							experience. We do not link the information we store in cookies to
							any Personal Information you submit while on our website. <br />
							<br />If you wish to disable the cookies on this website you will
							need to follow the steps required for your preferred browser but your ability to use some features or areas of our website may be limited.<br />
							
							<br />&#183; <i>Log Files</i><br/><br/>
							As is true of most websites, we gather certain information automatically and store it in log files.  This information may include internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and/or clickstream data. We do link this automatically collected data to other information we collect about you. We do this to improve services we offer, improve marketing and assist with analytics and site monitoring functinality.
							<br/>
							<br />&#183; <i>Web Beacons / Gifs</i> <br />Our
							third party advertising partner employs a software technology
							called clear gifs (a.k.a. Web Beacons/Web Bugs), that help us
							better manage content on our website by informing us what content
							is effective. Clear gifs are tiny graphics with a unique
							identifier, similar in function to cookies, and are used to track
							the online movements of Web users. In contrast to cookies, which
							are stored on a user&#8217;s computer hard drive, clear gifs are
							embedded invisibly on Web pages and are about the size of the
							period at the end of this sentence. We do not tie the information
							gathered by clear gifs to our customers&#8217; Personal
							Information. <br />
							<ul>
								<li><i>3<sup>rd</sup> Party Tracking
								</i></li>
							</ul> <br /> <br />The use of cookies by our partners, affiliates,
							tracking utility company, and service providers is not covered
							by our Privacy Policy. We do have access or control over
							these cookies. Our partners, affiliates, tracking utility
							company, and service providers may use session ID cookies and persistent
							cookies to make it easier for you to navigate our website and assist with managing our services. <br />
				</div>
			</div>
		</div>
		<!-- end #content -->
		<div class="push"></div>
	</div>
	<!-- end #wrapper -->
	







<style>
  #footerMenu li {
    display:inline;
    list-style-type:none;
    padding-left:.3em;
    margin-left:0em;
    border-left:1px solid #ccc
    }

  #footerMenu  li:first-child {
    border-left:none
    }

</style>
<div id="footer">
	<div style="padding:3px;text-align:left">
	<table width="100%">
	<tr>
	<td>
	
		RegattaCentral &#169; 1999-2016 
	</td>
	<td align="right">
		<ul id="footerMenu">
			<li><a href="/information/about.jsp">About</a></li>
			<li><a href="/information/privacy.jsp">Privacy Policy</a></li>
			<li><a href="/information/terms.jsp">Terms &amp; Conditions</a></li>
			<li><a href="/support.jsp">Help</a></li>
		</ul>
	</td>
	</tr>
	</table>
	</div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-24307524-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- version=1.49-->

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"applicationID":"3118059,3132377","applicationTime":4,"beacon":"bam.nr-data.net","queueTime":0,"licenseKey":"9974f40920","transactionName":"MVRaZEBQVktZAU0KVggecmNiHlFWXg1LDlgSWFdeHUFKUU4DWhoXDEJI","agent":"","errorBeacon":"bam.nr-data.net"}</script></body>
</html>

