<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>


<title>RegattaCentral - Privacy Policy</title>






<link rel="icon" href="https://rccdn.cachefly.net/images/rc.png" type="image/png" />
<link rel="SHORTCUT ICON" href="https://rccdn.cachefly.net/images/favicon.ico" />
<link rel="icon" sizes="192x192" href="https://rccdn.cachefly.net/images/touch-icon-192x192.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="https://rccdn.cachefly.net/images/apple-touch-icon-180x180-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://rccdn.cachefly.net/images/apple-touch-icon-152x152-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="https://rccdn.cachefly.net/images/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://rccdn.cachefly.net/images/apple-touch-icon-120x120-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="https://rccdn.cachefly.net/images/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="https://rccdn.cachefly.net/images/apple-touch-icon-76x76-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://rccdn.cachefly.net/images/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="https://rccdn.cachefly.net/images/apple-touch-icon-precomposed.png">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />  



<script src="https://rccdn.cachefly.net/js/jquery-1.11.3.min.js"></script>
<script src="https://rccdn.cachefly.net/js/jquery-migrate-1.2.1.min.js"></script>
<script src="https://rccdn.cachefly.net/js/jquery-ui-1.10.4.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/css/jquery-ui-bootstrap/jquery-ui-1.10.3.custom.css" />
<script src="/wro/jqueryLibs.js"></script>
<script src="/ui/subModal/common.js"></script>
<script src="/ui/subModal/subModal.js?v=3"></script>
<link rel="stylesheet" type="text/css" href="/ui/subModal/style.css" />
<link rel="stylesheet" type="text/css" href="/ui/subModal/subModal.css?v=2" />
 <script src="https://rccdn.cachefly.net/js/tablesorter-2.27.7/js/jquery.tablesorter.combined.js"></script>
<link href="/js/tablesorter/style.css" rel="stylesheet" type="text/css" />



<script src="/wro/main.js?v=3"></script>
<link href="/wro/mainStyles.css?v=1" rel="stylesheet" type="text/css" />
<!-- Hotjar Tracking Code for RegattaCentral -->
	<script>
	    (function(h,o,t,j,a,r){
	        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
	        h._hjSettings={hjid:1052886,hjsv:6};
	        a=o.getElementsByTagName('head')[0];
	        r=o.createElement('script');r.async=1;
	        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
	        a.appendChild(r);
	    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
		


<script src="https://rccdn.cachefly.net/lib/owl-carousel_1.31/owl.carousel.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/lib/owl-carousel_1.31/owl.carousel.css" />
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/lib/owl-carousel_1.31/owl.theme.css" />


  		







<script src="https://rccdn.cachefly.net/js/moment-with-langs.min.js"></script>
<script src="https://rccdn.cachefly.net/js/moment-timezone.min.js"></script>
<script src="https://rccdn.cachefly.net/js/livestamp.min.js"></script>

<script type="text/javascript" src="https://rccdn.cachefly.net/lib/tooltipster-3.3.0/js/jquery.tooltipster.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/lib/tooltipster-3.3.0/css/tooltipster.css" />
<link rel="stylesheet" type="text/css" href="https://rccdn.cachefly.net/lib/tooltipster-3.3.0/css/themes/tooltipster-shadow.css" />




<script>
moment.lang('en');
</script>

<!--[if IE 10]>
	<link href="/css/ie-10.css" rel="stylesheet" type="text/css" />
<![endif]-->
    
<script type="text/javascript">    
	    
		  jQuery(document).ready(function() {
		   		$('.tooltip').tooltipster();
		    
		  });
</script>






<script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={xpid:"UQ8BU15XGwABVVlSAgg="};window.NREUM||(NREUM={}),__nr_require=function(t,n,e){function r(e){if(!n[e]){var o=n[e]={exports:{}};t[e][0].call(o.exports,function(n){var o=t[e][1][n];return r(o||n)},o,o.exports)}return n[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({1:[function(t,n,e){function r(t){try{s.console&&console.log(t)}catch(n){}}var o,i=t("ee"),a=t(16),s={};try{o=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(s.console=!0,o.indexOf("dev")!==-1&&(s.dev=!0),o.indexOf("nr_dev")!==-1&&(s.nrDev=!0))}catch(c){}s.nrDev&&i.on("internal-error",function(t){r(t.stack)}),s.dev&&i.on("fn-err",function(t,n,e){r(e.stack)}),s.dev&&(r("NR AGENT IN DEVELOPMENT MODE"),r("flags: "+a(s,function(t,n){return t}).join(", ")))},{}],2:[function(t,n,e){function r(t,n,e,r,s){try{p?p-=1:o(s||new UncaughtException(t,n,e),!0)}catch(f){try{i("ierr",[f,c.now(),!0])}catch(d){}}return"function"==typeof u&&u.apply(this,a(arguments))}function UncaughtException(t,n,e){this.message=t||"Uncaught error with no additional information",this.sourceURL=n,this.line=e}function o(t,n){var e=n?null:c.now();i("err",[t,e])}var i=t("handle"),a=t(17),s=t("ee"),c=t("loader"),f=t("gos"),u=window.onerror,d=!1,l="nr@seenError",p=0;c.features.err=!0,t(1),window.onerror=r;try{throw new Error}catch(h){"stack"in h&&(t(8),t(7),"addEventListener"in window&&t(5),c.xhrWrappable&&t(9),d=!0)}s.on("fn-start",function(t,n,e){d&&(p+=1)}),s.on("fn-err",function(t,n,e){d&&!e[l]&&(f(e,l,function(){return!0}),this.thrown=!0,o(e))}),s.on("fn-end",function(){d&&!this.thrown&&p>0&&(p-=1)}),s.on("internal-error",function(t){i("ierr",[t,c.now(),!0])})},{}],3:[function(t,n,e){t("loader").features.ins=!0},{}],4:[function(t,n,e){function r(t){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var o=t("ee"),i=t("handle"),a=t(8),s=t(7),c="learResourceTimings",f="addEventListener",u="resourcetimingbufferfull",d="bstResource",l="resource",p="-start",h="-end",m="fn"+p,v="fn"+h,w="bstTimer",y="pushState",g=t("loader");g.features.stn=!0,t(6);var b=NREUM.o.EV;o.on(m,function(t,n){var e=t[0];e instanceof b&&(this.bstStart=g.now())}),o.on(v,function(t,n){var e=t[0];e instanceof b&&i("bst",[e,n,this.bstStart,g.now()])}),a.on(m,function(t,n,e){this.bstStart=g.now(),this.bstType=e}),a.on(v,function(t,n){i(w,[n,this.bstStart,g.now(),this.bstType])}),s.on(m,function(){this.bstStart=g.now()}),s.on(v,function(t,n){i(w,[n,this.bstStart,g.now(),"requestAnimationFrame"])}),o.on(y+p,function(t){this.time=g.now(),this.startPath=location.pathname+location.hash}),o.on(y+h,function(t){i("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),f in window.performance&&(window.performance["c"+c]?window.performance[f](u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["c"+c]()},!1):window.performance[f]("webkit"+u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["webkitC"+c]()},!1)),document[f]("scroll",r,{passive:!0}),document[f]("keypress",r,!1),document[f]("click",r,!1)}},{}],5:[function(t,n,e){function r(t){for(var n=t;n&&!n.hasOwnProperty(u);)n=Object.getPrototypeOf(n);n&&o(n)}function o(t){s.inPlace(t,[u,d],"-",i)}function i(t,n){return t[1]}var a=t("ee").get("events"),s=t(19)(a,!0),c=t("gos"),f=XMLHttpRequest,u="addEventListener",d="removeEventListener";n.exports=a,"getPrototypeOf"in Object?(r(document),r(window),r(f.prototype)):f.prototype.hasOwnProperty(u)&&(o(window),o(f.prototype)),a.on(u+"-start",function(t,n){var e=t[1],r=c(e,"nr@wrapped",function(){function t(){if("function"==typeof e.handleEvent)return e.handleEvent.apply(e,arguments)}var n={object:t,"function":e}[typeof e];return n?s(n,"fn-",null,n.name||"anonymous"):e});this.wrapped=t[1]=r}),a.on(d+"-start",function(t){t[1]=this.wrapped||t[1]})},{}],6:[function(t,n,e){var r=t("ee").get("history"),o=t(19)(r);n.exports=r,o.inPlace(window.history,["pushState","replaceState"],"-")},{}],7:[function(t,n,e){var r=t("ee").get("raf"),o=t(19)(r),i="equestAnimationFrame";n.exports=r,o.inPlace(window,["r"+i,"mozR"+i,"webkitR"+i,"msR"+i],"raf-"),r.on("raf-start",function(t){t[0]=o(t[0],"fn-")})},{}],8:[function(t,n,e){function r(t,n,e){t[0]=a(t[0],"fn-",null,e)}function o(t,n,e){this.method=e,this.timerDuration=isNaN(t[1])?0:+t[1],t[0]=a(t[0],"fn-",this,e)}var i=t("ee").get("timer"),a=t(19)(i),s="setTimeout",c="setInterval",f="clearTimeout",u="-start",d="-";n.exports=i,a.inPlace(window,[s,"setImmediate"],s+d),a.inPlace(window,[c],c+d),a.inPlace(window,[f,"clearImmediate"],f+d),i.on(c+u,r),i.on(s+u,o)},{}],9:[function(t,n,e){function r(t,n){d.inPlace(n,["onreadystatechange"],"fn-",s)}function o(){var t=this,n=u.context(t);t.readyState>3&&!n.resolved&&(n.resolved=!0,u.emit("xhr-resolved",[],t)),d.inPlace(t,y,"fn-",s)}function i(t){g.push(t),h&&(x?x.then(a):v?v(a):(E=-E,O.data=E))}function a(){for(var t=0;t<g.length;t++)r([],g[t]);g.length&&(g=[])}function s(t,n){return n}function c(t,n){for(var e in t)n[e]=t[e];return n}t(5);var f=t("ee"),u=f.get("xhr"),d=t(19)(u),l=NREUM.o,p=l.XHR,h=l.MO,m=l.PR,v=l.SI,w="readystatechange",y=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"],g=[];n.exports=u;var b=window.XMLHttpRequest=function(t){var n=new p(t);try{u.emit("new-xhr",[n],n),n.addEventListener(w,o,!1)}catch(e){try{u.emit("internal-error",[e])}catch(r){}}return n};if(c(p,b),b.prototype=p.prototype,d.inPlace(b.prototype,["open","send"],"-xhr-",s),u.on("send-xhr-start",function(t,n){r(t,n),i(n)}),u.on("open-xhr-start",r),h){var x=m&&m.resolve();if(!v&&!m){var E=1,O=document.createTextNode(E);new h(a).observe(O,{characterData:!0})}}else f.on("fn-end",function(t){t[0]&&t[0].type===w||a()})},{}],10:[function(t,n,e){function r(t){var n=this.params,e=this.metrics;if(!this.ended){this.ended=!0;for(var r=0;r<d;r++)t.removeEventListener(u[r],this.listener,!1);if(!n.aborted){if(e.duration=a.now()-this.startTime,4===t.readyState){n.status=t.status;var i=o(t,this.lastSize);if(i&&(e.rxSize=i),this.sameOrigin){var c=t.getResponseHeader("X-NewRelic-App-Data");c&&(n.cat=c.split(", ").pop())}}else n.status=0;e.cbTime=this.cbTime,f.emit("xhr-done",[t],t),s("xhr",[n,e,this.startTime])}}}function o(t,n){var e=t.responseType;if("json"===e&&null!==n)return n;var r="arraybuffer"===e||"blob"===e||"json"===e?t.response:t.responseText;return h(r)}function i(t,n){var e=c(n),r=t.params;r.host=e.hostname+":"+e.port,r.pathname=e.pathname,t.sameOrigin=e.sameOrigin}var a=t("loader");if(a.xhrWrappable){var s=t("handle"),c=t(11),f=t("ee"),u=["load","error","abort","timeout"],d=u.length,l=t("id"),p=t(14),h=t(13),m=window.XMLHttpRequest;a.features.xhr=!0,t(9),f.on("new-xhr",function(t){var n=this;n.totalCbs=0,n.called=0,n.cbTime=0,n.end=r,n.ended=!1,n.xhrGuids={},n.lastSize=null,p&&(p>34||p<10)||window.opera||t.addEventListener("progress",function(t){n.lastSize=t.loaded},!1)}),f.on("open-xhr-start",function(t){this.params={method:t[0]},i(this,t[1]),this.metrics={}}),f.on("open-xhr-end",function(t,n){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&n.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),f.on("send-xhr-start",function(t,n){var e=this.metrics,r=t[0],o=this;if(e&&r){var i=h(r);i&&(e.txSize=i)}this.startTime=a.now(),this.listener=function(t){try{"abort"===t.type&&(o.params.aborted=!0),("load"!==t.type||o.called===o.totalCbs&&(o.onloadCalled||"function"!=typeof n.onload))&&o.end(n)}catch(e){try{f.emit("internal-error",[e])}catch(r){}}};for(var s=0;s<d;s++)n.addEventListener(u[s],this.listener,!1)}),f.on("xhr-cb-time",function(t,n,e){this.cbTime+=t,n?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof e.onload||this.end(e)}),f.on("xhr-load-added",function(t,n){var e=""+l(t)+!!n;this.xhrGuids&&!this.xhrGuids[e]&&(this.xhrGuids[e]=!0,this.totalCbs+=1)}),f.on("xhr-load-removed",function(t,n){var e=""+l(t)+!!n;this.xhrGuids&&this.xhrGuids[e]&&(delete this.xhrGuids[e],this.totalCbs-=1)}),f.on("addEventListener-end",function(t,n){n instanceof m&&"load"===t[0]&&f.emit("xhr-load-added",[t[1],t[2]],n)}),f.on("removeEventListener-end",function(t,n){n instanceof m&&"load"===t[0]&&f.emit("xhr-load-removed",[t[1],t[2]],n)}),f.on("fn-start",function(t,n,e){n instanceof m&&("onload"===e&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=a.now()))}),f.on("fn-end",function(t,n){this.xhrCbStart&&f.emit("xhr-cb-time",[a.now()-this.xhrCbStart,this.onload,n],n)})}},{}],11:[function(t,n,e){n.exports=function(t){var n=document.createElement("a"),e=window.location,r={};n.href=t,r.port=n.port;var o=n.href.split("://");!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=n.hostname||e.hostname,r.pathname=n.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname);var i=!n.protocol||":"===n.protocol||n.protocol===e.protocol,a=n.hostname===document.domain&&n.port===e.port;return r.sameOrigin=i&&(!n.hostname||a),r}},{}],12:[function(t,n,e){function r(){}function o(t,n,e){return function(){return i(t,[f.now()].concat(s(arguments)),n?null:this,e),n?void 0:this}}var i=t("handle"),a=t(16),s=t(17),c=t("ee").get("tracer"),f=t("loader"),u=NREUM;"undefined"==typeof window.newrelic&&(newrelic=u);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],l="api-",p=l+"ixn-";a(d,function(t,n){u[n]=o(l+n,!0,"api")}),u.addPageAction=o(l+"addPageAction",!0),u.setCurrentRouteName=o(l+"routeName",!0),n.exports=newrelic,u.interaction=function(){return(new r).get()};var h=r.prototype={createTracer:function(t,n){var e={},r=this,o="function"==typeof n;return i(p+"tracer",[f.now(),t,e],r),function(){if(c.emit((o?"":"no-")+"fn-start",[f.now(),r,o],e),o)try{return n.apply(this,arguments)}catch(t){throw c.emit("fn-err",[arguments,this,t],e),t}finally{c.emit("fn-end",[f.now()],e)}}}};a("actionText,setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,n){h[n]=o(p+n)}),newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),i("err",[t,f.now()])}},{}],13:[function(t,n,e){n.exports=function(t){if("string"==typeof t&&t.length)return t.length;if("object"==typeof t){if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if(!("undefined"!=typeof FormData&&t instanceof FormData))try{return JSON.stringify(t).length}catch(n){return}}}},{}],14:[function(t,n,e){var r=0,o=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);o&&(r=+o[1]),n.exports=r},{}],15:[function(t,n,e){function r(t,n){if(!o)return!1;if(t!==o)return!1;if(!n)return!0;if(!i)return!1;for(var e=i.split("."),r=n.split("."),a=0;a<r.length;a++)if(r[a]!==e[a])return!1;return!0}var o=null,i=null,a=/Version\/(\S+)\s+Safari/;if(navigator.userAgent){var s=navigator.userAgent,c=s.match(a);c&&s.indexOf("Chrome")===-1&&s.indexOf("Chromium")===-1&&(o="Safari",i=c[1])}n.exports={agent:o,version:i,match:r}},{}],16:[function(t,n,e){function r(t,n){var e=[],r="",i=0;for(r in t)o.call(t,r)&&(e[i]=n(r,t[r]),i+=1);return e}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],17:[function(t,n,e){function r(t,n,e){n||(n=0),"undefined"==typeof e&&(e=t?t.length:0);for(var r=-1,o=e-n||0,i=Array(o<0?0:o);++r<o;)i[r]=t[n+r];return i}n.exports=r},{}],18:[function(t,n,e){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],19:[function(t,n,e){function r(t){return!(t&&t instanceof Function&&t.apply&&!t[a])}var o=t("ee"),i=t(17),a="nr@original",s=Object.prototype.hasOwnProperty,c=!1;n.exports=function(t,n){function e(t,n,e,o){function nrWrapper(){var r,a,s,c;try{a=this,r=i(arguments),s="function"==typeof e?e(r,a):e||{}}catch(f){l([f,"",[r,a,o],s])}u(n+"start",[r,a,o],s);try{return c=t.apply(a,r)}catch(d){throw u(n+"err",[r,a,d],s),d}finally{u(n+"end",[r,a,c],s)}}return r(t)?t:(n||(n=""),nrWrapper[a]=t,d(t,nrWrapper),nrWrapper)}function f(t,n,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<n.length;c++)s=n[c],a=t[s],r(a)||(t[s]=e(a,f?s+o:o,i,s))}function u(e,r,o){if(!c||n){var i=c;c=!0;try{t.emit(e,r,o,n)}catch(a){l([a,e,r,o])}c=i}}function d(t,n){if(Object.defineProperty&&Object.keys)try{var e=Object.keys(t);return e.forEach(function(e){Object.defineProperty(n,e,{get:function(){return t[e]},set:function(n){return t[e]=n,n}})}),n}catch(r){l([r])}for(var o in t)s.call(t,o)&&(n[o]=t[o]);return n}function l(n){try{t.emit("internal-error",n)}catch(e){}}return t||(t=o),e.inPlace=f,e.flag=a,e}},{}],ee:[function(t,n,e){function r(){}function o(t){function n(t){return t&&t instanceof r?t:t?c(t,s,i):i()}function e(e,r,o,i){if(!l.aborted||i){t&&t(e,r,o);for(var a=n(o),s=m(e),c=s.length,f=0;f<c;f++)s[f].apply(a,r);var d=u[g[e]];return d&&d.push([b,e,r,a]),a}}function p(t,n){y[t]=m(t).concat(n)}function h(t,n){var e=y[t];if(e)for(var r=0;r<e.length;r++)e[r]===n&&e.splice(r,1)}function m(t){return y[t]||[]}function v(t){return d[t]=d[t]||o(e)}function w(t,n){f(t,function(t,e){n=n||"feature",g[e]=n,n in u||(u[n]=[])})}var y={},g={},b={on:p,addEventListener:p,removeEventListener:h,emit:e,get:v,listeners:m,context:n,buffer:w,abort:a,aborted:!1};return b}function i(){return new r}function a(){(u.api||u.feature)&&(l.aborted=!0,u=l.backlog={})}var s="nr@context",c=t("gos"),f=t(16),u={},d={},l=n.exports=o();l.backlog=u},{}],gos:[function(t,n,e){function r(t,n,e){if(o.call(t,n))return t[n];var r=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(t,n,e){function r(t,n,e,r){o.buffer([t],r),o.emit(t,n,e)}var o=t("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(t,n,e){function r(t){var n=typeof t;return!t||"object"!==n&&"function"!==n?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");n.exports=r},{}],loader:[function(t,n,e){function r(){if(!E++){var t=x.info=NREUM.info,n=p.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&n))return u.abort();f(g,function(n,e){t[n]||(t[n]=e)}),c("mark",["onload",a()+x.offset],null,"api");var e=p.createElement("script");e.src="https://"+t.agent,n.parentNode.insertBefore(e,n)}}function o(){"complete"===p.readyState&&i()}function i(){c("mark",["domContent",a()+x.offset],null,"api")}function a(){return O.exists&&performance.now?Math.round(performance.now()):(s=Math.max((new Date).getTime(),s))-x.offset}var s=(new Date).getTime(),c=t("handle"),f=t(16),u=t("ee"),d=t(15),l=window,p=l.document,h="addEventListener",m="attachEvent",v=l.XMLHttpRequest,w=v&&v.prototype;NREUM.o={ST:setTimeout,SI:l.setImmediate,CT:clearTimeout,XHR:v,REQ:l.Request,EV:l.Event,PR:l.Promise,MO:l.MutationObserver};var y=""+location,g={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1099.min.js"},b=v&&w&&w[h]&&!/CriOS/.test(navigator.userAgent),x=n.exports={offset:s,now:a,origin:y,features:{},xhrWrappable:b,userAgent:d};t(12),p[h]?(p[h]("DOMContentLoaded",i,!1),l[h]("load",r,!1)):(p[m]("onreadystatechange",o),l[m]("onload",r)),c("mark",["firstbyte",s],null,"api");var E=0,O=t(18)},{}]},{},["loader",2,10,4,3]);</script><style type="text/css">
#content {
	max-width: 800px;
	margin-right: auto;
	margin-left: auto;
	padding-left: 0px;
	padding-right: 0px;
}

}
<!--
ol {
	margin-bottom: 0in;
	list-style: disc;
	list-style-type: disc;
}

ul {
	margin-left: 0px;
	padding-left: 15px;
	margin-bottom: 0in;
}
li {
	padding-top:10px;
}
-->
</style>
</head>
<body>
	<div id="wrapper">

		


















	
	
	
	
	<div class="head">
    <div>
        <div class="header-container">
            <h1><a title="RegattaCentral" href="/">RegattaCentral</a></h1>
            <nav class="main">
                <ul><li><a href="/" >Home</a></li>
                    <li><a href="/regattas" >Regattas</a></li>
                    <li><a href="/regattas?results" >Results</a></li>
                    <li><a href="/clubs" >Clubs</a></li>
                    <li><a href="/programs" >Programs</a></li>
                    <li><a href="/ltr" >Learn-To-Row</a></li>
                    <li><a href="/camps" >Camps & Clinics</a></li>
                    <li><a href="/volunteer" >Volunteer</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div id="searchAndAccountBar">
        <div class="header-container">
            <div class="align-left">
                <!-- Search -->
                <form method="post" action="/search/" id="search_global">
                    <a href="#clear" id="search_clear"><img src="https://rccdn.cachefly.net/images/icon_search_clear.png" alt="Clear" title="Clear" width="18" height="18"/></a>
                    <input type="search" name="txt_search_global" id="txt_search_global" value="" placeholder="Search" class="disabled placeholder"/>
                    <ul id="search_global_results"></ul>
                </form>
                <!-- Social Media -->
                <div>
                    <a target="_new" href="http://www.twitter.com/regattacentral"><img border="0" src="https://rccdn.cachefly.net/images/twitter.png" width="24" height="24"></a>
                    <a target="_new" href="http://www.facebook.com/regattacentral/"><img border="0" src="https://rccdn.cachefly.net/images/facebook.png" width="24" height="24"></a>
                </div>
            </div>
            <div class="align-right">
                <!-- Support & Settings -->
                <nav class="options">
                    <ul>
                    
					
                        <li>
                            <a href="#" class="noclick">Settings &#x25bc;</a>
                            <ul>
                            
                                <li>
                                    <span class="first">Language</span>
                                    <ul>
                                        <li><a class="" href="/?lang=de" >Deutsch</a></li>
                                        <li><a href="/?lang=en" class="active">English</a></li>
                                        <li><a href="/?lang=fr" >Français</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <span>View</span>
                                    <ul>
                                        <li><a onclick="setFixedWidth();return false;" id="view_in" href="#">Default (1024p)</a></li>
                                        <li><a onclick="setFullWidth();return false;" id="view_in"href="#">Full Width</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li><a href="/support.jsp">Support</a></li>
                    </ul>
                </nav>
                
                <!-- Login / Join -->
                <div id="loginBarWrapper">
                    <div id="loginBar">
                        <div id="loginLogout">
                            <span class="vline"></span>
                            <a style="padding-right:3px" href="/login">Login</a>
                            <a style="padding-right:3px" href="/signup">Sign Up</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
 
        

			
			<noscript>
				<div class="error">JavaScript is required for this website.	Please edit your browser's settings and enable JavaScript to ensure	RegattaCentral functions properly</div>
			</noscript>
		

	<div style="display:none" id="userNmReminder" title="Username Reminder">
	<form id="userNmFrm">
		    <fieldset>
		 		<label for="name">Email Address</label>
		        <input id="userNmEmail" type="text" name="email" value="" maxlength="128" size="30" />
		        <input type="hidden" name="csrfToken" value="8748277338866" />
		        </fieldset>
		        </form>
	</div>
	<div style="display:none" id="pwdReset" title="Password Reset">
		<form id="pwdResetFrm">
		    <fieldset>
		        <label for="name">Username</label>
		        <input id="pwdResetUsername" type="text" name="userName" value="" maxlength="128" size="30" />
		        <input type="hidden" name="csrfToken" value="8748277338866" />
		    </fieldset>
	    </form>
	</div>


		<div id="content">

			<div style="padding-left: 15px; padding-right: 10px;padding-top:10px; float: right">
			<div style="float:left"><a href="//privacy.truste.com/privacy-seal/validation?rid=5b39dee0-f81f-457e-8475-ab1f1f9e29cd" target="_blank"><img style="border: none" src="//privacy-policy.truste.com/privacy-seal/seal?rid=5b39dee0-f81f-457e-8475-ab1f1f9e29cd" alt="TRUSTe"/></a></div>
			<div style="float:right"><script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?code=91d5205414c243838d5de30b1ebbb349"></script></div>
				<div style="clear:both">
					<br />
					<b>SPay, Inc d/b/a Stack Sports Star Sports Privacy Policy</b> <br />
					<b> </b> <br />
					<b>Effective Date: 28 September 2017</b> <br />
					<b> </b> <br />
					<b>Introduction</b> <br /> <br />At SPay, Inc. (SPay) we understand
					that your privacy is important to you and we are committed to
					complying with the applicable privacy laws in relation to all
					Personal Information and Sensitive Information that we collect from
					you. This policy describes our Privacy and Information handling
					practices. <br /> <br />By accessing and using this website
					(www.regattacentral.com) or other websites owned and operated by
					SPay, you accept and agree to SPay&#8217;s
					Privacy Policy without limitation or qualification. <br />Definitions
					of specific terms referred to in this Policy can be found under the
					heading &#8216;Glossary&#8217; at the end of this document. <br />
					<b> </b> <br />
					 If you have an unresolved privacy or data use concern that we have not addressed satisfactorily, please contact our U.S.-based third party dispute resolution provider (free of charge) at <a href="https://feedback-form.truste.com/watchdog/request">https://feedback-form.truste.com/watchdog/request</a>
					<br/><br/>
					<b>Privacy Policy</b> <br />
					<b> </b> <br />This Policy outlines the following: <br /> <br />1.
					The types of Personal Information that we collect and hold. <br />2.
					How we collect Personal Information. <br />3. The purposes for
					which the information is collected. <br />4. The way in which we
					use and disclose Personal Information. <br />5. The steps we take
					to protect and keep secure the Personal Information we hold. <br />6.
					How individuals can access and correct their Personal Information
					held by us. <br />7. Mechanisms for complaint if you are unhappy
					about how we have dealt with or are dealing with your Personal
					Information. <br /> <br />
					<b>1. </b><b>The types of Personal Information that we collect
						and hold</b> <br />
					<b> </b> <br />In the normal course of business activities we will
					collect Personal Information from you and this may include
					collecting some Sensitive Information. <br /> <br />The following
					are the types of your Personal Information and Sensitive
					Information that we may collect and hold: <br /> <br />&#183; We
					collect personal details such as name, postal and email addresses,
					phone numbers, date of birth, age, gender, height and rowing
					history. <br /> <br />This may also include copies of documents
					provided to us as proof of identity. It may also include Sensitive
					Information about individuals such as health information and
					information regarding racial or ethnic origin. <br /> <br />&#183;
					If you order any products from our website, we also collect payment
					details such as credit card number and billing address. <br /> <br />&#183;
					In registering for an event or program, you may provide Personal
					Information or Sensitive Information to us for the purpose of
					attending the event or program. We use that information for that
					purpose and may provide information to you regarding our other
					services. <br /> <br />
					<b>2. </b><b>How we collect Personal Information</b> <br />
					<b> </b> <br />We will only collect Personal Information by lawful
					and fair means. Personal Information may be collected directly from
					you or your authorised representative. We collect Personal
					Information and Sensitive Information via our website, email or
					other direct contact. You may also supply your Personal Information
					to us when communicating with us via social networks and other
					online channels, e.g. through your Facebook identity. <br /> <br />We
					do not collect Personal and Sensitive information unless the
					information is reasonably necessary for our business functions or
					activities. We will obtain your consent before collecting any
					Sensitive Information. <br /> <br />When you deal with us, you will
					need to identify yourself in order for us to provide our services
					to you, as such we do not accept the use of pseudonyms. <br /> <br />
					<b>3. </b><b>The purposes for which the information is
						collected</b> <br />
					<b> </b> <br />We collect, hold and use your Personal Information
					so that we can provide our services including to: <br /> <br />&#183;
					assist you to register for events or programs with a local or
					international host organisation, governing body, club or event
					organiser for: <br /> <br />o rowing events, results or programs; <br />o
					camps or clinics; <br />o learn-to-row sessions; <br />o volunteer
					roles. <br /> <br />&#183; assist us, the host organisations,
					governing bodies, clubs or event organisers in verifying your
					identity and/or eligibility; <br /> <br />&#183; respond to you regarding the reason
					you contacted us or to answer your questions; <br /> <br />&#183;
					manage our internal operations and registration processes necessary
					to provide our services. <br /> <br />
					<b>4. </b><b>The way in which we use and disclose Personal
						Information</b> <br />
					<b> </b> <br />We will use or disclose your Personal Information
					only for the purposes for which it was collected. If you register
					for a particular event or program, we will share your information
					with the applicable host organisation, governing body, club or
					event organiser for that event or program, and with any other party
					which furthers the purpose of your registration. These parties are
					authorized to use your Personal Information only as necessary to
					provide these services to us. We will use or disclose your
					information for a secondary purpose only if you have consented or
					if you would reasonably expect us to do so or if required by law,
					such as to comply with a subpoena, or similar legal process. In certain situations, SPay, Inc. may be required to disclose Personal Information in response to lawful requests by public authorities, including to meet national security or law enforcement requirements.  If
					SPay is involved in a merger, acquisition, or sale of all
					or a portion of its assets, you will be notified via email and/or a
					prominent notice on our website of any change in ownership or uses
					of your Personal Information, as well as any choices you may have
					regarding your Personal Information. <br /> <br />We may use your
					Personal Information for the purposes of our own direct marketing,
					however we will ensure you have an ability to opt out of future
					such communications by following the unsubscribe instructions
					included in these emails or you can contact us at
					support@regattacentral.com. <br />
					<br/>
					We will also send you service related email announcements on rare occasions when it is necessary to do so. For instance, if our service is temporarily suspended for maintenance, we might send you an email. You do not have an option to opt out of these emails, which are not promotional in nature.
					 <br /> <br />
					<b><i>Disclosure to overseas recipients</i></b> <br />
					<b><i> </i></b> <br />We may disclose your Personal Information to
					overseas recipients such as host organisations, governing bodies,
					clubs or event organisers which may be located in Australia,
					Canada, England, Germany, Belgium, Italy or the United States of America
					when you have sought our assistance to participate in an event or
					program or where disclosure is required for verification purposes.
					As such, the location of these overseas recipients will depend on
					the specific events or programs to which you apply or where such
					verification is required. <br /> <br />The information disclosed to
					overseas recipients consists of any information you have given to
					us to provide the relevant services. By accepting services from us,
					you consent to us providing your Personal Information to the
					relevant overseas recipient as mentioned above and acknowledge that
					we are not responsible for ensuring their compliance with the
					applicable privacy laws. <br /> <br />
					<b>5. </b><b>The steps we take to protect and keep secure the
						Personal Information we hold</b> <br />
					<b> </b> <br />We take reasonable steps to protect the Personal
					Information we hold against interference, loss, unauthorised
					access, use, modification or disclosure, and other misuse. <br /> <br />The
					steps we take include: <br /> <br />&#183; maintenance of computer
					and network security measures such as including firewalls, use of
					password and other appropriate measures where information is held
					in electronic form; <br /> <br />&#183; regular updates to security
					systems to protect our systems from malicious software; <br /> <br />&#183;
					restriction of access to data to only those staff that need access
					to carry out our business activities; <br /> <br />&#183; training
					and ensuring that all our employees are required, as a condition of
					employment, to treat Personal Information held by us as
					confidential; <br /> <br />&#183; whenever we collect Sensitive
					information such as credit card data, that information is encrypted
					and transmitted to us in a secure way using secure socket layer
					technology (SSL). You can verify this by looking for a closed lock
					icon at the bottom of your web browser, or looking for
					&#8216;https&#8217; at the beginning of the address of the web
					page. <br /> <br />You should however, be aware that if you submit
					information to us electronically the internet is not a secure
					environment. We take reasonable steps to provide a secure channel
					for receiving information but cannot absolutely protect Personal
					Information before it reaches us.  If you have any questions about security on our website, you can contact us at support@regattacentral.com<br /> <br />We will retain your information for as long as your account is active or as needed to provide you services. When the Personal
					Information that we collect is no longer required, we will destroy,
					delete it in a secure manner, or ensure that the information is
					de-identified in accordance with our information destruction and
					de-identification policy, unless we are required by law to retain a
					copy of the Personal Information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. <br /> <br />
					<b>6. </b><b>How individuals can access and correct their
						Personal Information held by us</b> <br />
					<b> </b> <br />You have the right to seek access to any of your
					Personal Information held by us unless there is a valid reason
					under an applicable privacy law for us to withhold the information.
					<br /> <br />Valid reasons include where: <br /> <br />&#183; we
					have reason to suspect that unlawful activity or misconduct of a
					serious nature has been engaged in and giving access would be
					likely to prejudice the taking of appropriate action in relation to
					the matter; or <br /> <br />&#183; giving access would reveal
					evaluative information generated within us in connection with a
					commercially sensitive process; or <br /> <br />&#183; giving
					access would have an unreasonable impact on the privacy of other
					individuals; or <br /> <br />&#183; giving access would be
					unlawful. <br /> <br />Upon request, SPay will provide you with information about whether we hold any of your personal information. If your personal details change, or you
					believe the information we hold about you is incorrect, incomplete
					or out-of-date, please contact us so that we can correct our
					records or you can make the change on your Account Information
					page. <br /> <br />Requests for access, deletion or correction to
					Personal Information should be addressed to the SPay Inc. 
					Privacy Officer. All requests for access, deletion or correction to
					Personal Information will be responded to in writing within a
					reasonable period of time. As part of this process we will verify
					the identity of the individual requesting the information prior to
					providing access or making any changes. If access, deletion or
					correction to your Personal Information is refused we will provide
					reasons for our refusal. <br /> <br />
					<b>7. </b><b>Mechanisms for complaint if you are unhappy about
						how we have dealt with or are dealing with your Personal
						Information</b> <br />
					<b> </b> <br />We have procedures in place to deal with your
					inquiries or complaints. <br /> <br />If you have any questions
					about our policy or any complaint regarding the treatment of your
					privacy by us, please contact us at the respective addresses below.
					<br /> <br />
					<b><i>Complaints to SPay Inc. </i></b> <br />
					<u> </u> <br />
					<u>By post:</u> <br />Attn: Privacy Officer<br> SPay, Inc. re: RegattaCentral<br> 2029 Riverside Dr, Suite 201<br />Columbus, OH, 43221 <br />United
							States of America <br />
						<u> </u> <br />
						<u>By e-mail:</u> <br />support@regattacentral.com <br />
						<u> </u> <br />
						<u>By phone:</u> <br />+1 (614) 360-2922<br /><br /> <br />We will endeavour to respond
							to you within a reasonable time. If you are still not satisfied
							with the way your complaint is handled by us, you may be entitled
							to have your complaint reviewed under an applicable privacy law.
							<br /> <br />
						<b>8. Additional Information</b> <br />
						<b> </b>
							<ul>
								<li><i>Links to 3<sup>rd</sup> Party Sites
								</i></li>
							</ul> <br /> <br />Our website includes links to other websites whose
							privacy practices may differ from those of SPay, Inc. If you
							submit Personal Information to any of those sites, your
							information is governed by their privacy policies. We encourage
							you to carefully read the privacy policy of any website you
							visit. <br />
							<ul>
								<li><i>Collection and Use of 3<sup>rd</sup> Party Personal Information
								</i></li>
							</ul> <br /> <br />You may also provide Personal Information about
							other people, such as their name, date of birth and gender. This
							information is only used for the sole purpose of completing your
							request or for whatever reason it may have been provided. We do store this third party information. Individuals may request for us to delete their information by contacting us at support@regattacentral.com. <br />
							<ul>
								<li><i>Social Media Widgets</i></li>
							</ul> <br /> <br />Our website includes Social Media Features, such as
							the Facebook Like button and Twitter button or interactive
							mini-programs that run on our website. These Features may collect
							your IP address, which page you are visiting on our website, and
							may set a cookie to enable the Feature to function properly.
							Social Media Features and Widgets are either hosted by a third
							party or hosted directly on our website. Your interactions with
							these Features are governed by the privacy policy of the company
							providing it. <br />
						<b> </b> <br />
						<b>Updates to this Policy</b> <br /> <br />We reserve the right to
							amend this Policy from time to time. Any revisions to the Policy
							will be posted on this website. If we make any material changes
							we will notify you by email (sent to the e-mail address specified
							in your account) or by means of a notice on this website prior to
							the change becoming effective. We encourage you to periodically
							review this page for the latest information on our privacy
							practices. <br /> <b><br clear=all></b> <br />
						<b>Glossary</b> <br />
						<b> </b>
							<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0>
								<tr>
									<td width=595 valign=top><br />1. <b><i>Personal
												Information </i></b>means information or an opinion about an
										identified individual, or an individual who is reasonably
										identifiable, whether the information or opinion is true or
										not, and whether the information or opinion is recorded in a
										material form or not.</td>
								</tr>
								<tr>
									<td width=595 valign=top><br />2. <b><i>SPay</i></b>
										means SPay, Inc. d/b/a Stack Sports</td>
								</tr>
								<tr>
									<td width=595 valign=top><br />3. <b><i>Sensitive
												Information </i></b>includes information or an opinion about an
										individual&#8217;s racial or ethnic origin, political
										opinions, membership of a political association, religious
										beliefs or affiliations, philosophical beliefs, membership of
										a professional or trade association, membership of a trade
										union, sexual orientation or practices, or criminal record
										that is also personal information, or health information about
										an individual, or genetic information about an individual that
										is not otherwise health information, or biometric information
										used for the purpose of automated biometric verification or
										identification, or biometric templates.</td>
								</tr>
								<tr>
									<td width=595 valign=top><br />4. <b><i>We</i></b>, <b><i>us</i>
									</b>and<b> <i>our</i></b> refers to SPay.</td>
								</tr>
							</table> </br></br>
						<b>Website usage and other information collected on this
							website</b> <br /> <br />By accessing and using this website (www.regattacentral.com) owned and operated by SPay, Inc. you accept
							and agree to SPay, Inc&#8217;s Privacy Policy without
							limitation or qualification. <br /> <br />We do not sell, rent or
							trade your Personal Information to any other third parties except
							if required by law or permitted under an applicable privacy law.
							<br /> <br />SPay&#8217;s Privacy Policy applies solely
							to information collected by us and may differ from the policies
							of other companies&#8217; or organisations&#8217; websites that
							you link to from this website. These third party links are made
							available to you as a convenience and you agree to use these
							links at your own risk. Please be aware that we are not
							responsible for the content of third party websites linked to our
							website nor are we responsible for the privacy policy or
							practices of any third parties whose website is linked to our
							website. We recommend that you read the privacy policy of any
							other companies&#8217; or organisations&#8217; website that you
							link to from our website. <br />
						<b> </b> <br />
						<b>Cookies/Tracking Technology</b> <br /> <br />Cookies are small
							data files that are sent from a website and stored in a
							user&#8217;s web browser to improve the user&#8217;s web browsing
							experience. We do not link the information we store in cookies to
							any Personal Information you submit while on our website. <br />
							<br />If you wish to disable the cookies on this website you will
							need to follow the steps required for your preferred browser but your ability to use some features or areas of our website may be limited.<br />
							<br/><br/>Technologies such as: cookies, beacons, tags and scripts are used by SPay and our marketing partners, partners, affiliates, and analytics/monitoring service providers. These technologies are used in analyzing trends, administering the website, identifying the number of visitors and page views (or page impressions) that occur on our website and how visitors use the website (e.g. how long these visitors (on average) spend on our website and common entry and exit points to our website). We may receive reports based on the use of these technologies by these companies on an individual as well as aggregated basis.
							<br />&#183; <i>Log Files</i><br/><br/>
							As is true of most websites, we gather certain information automatically and store it in log files.  This information may include internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and/or clickstream data. We do link this automatically collected data to other information we collect about you. We do this to improve services we offer, improve marketing and assist with analytics and site monitoring functinality.
							<br/>
						<br />&#183; <i>Behavioral Targeting/Re-Targeting</i><br/><br/>
						We partner with a third party to [display advertising on our website or to manage our advertising on other sites]. Our third party partner may use cookies or similar technologies in order to provide you advertising based upon your browsing activities and interests. If you wish to opt out of interest-based advertising click <a href="http://preferences-mgr.truste.com/">here</a>, Please note you will continue to receive generic ads. 
						<br/><br/>
				</div>
			</div>
		</div>
		<!-- end #content -->
		<div class="push"></div>
	</div>
	<!-- end #wrapper -->
	







<style>
  #footerMenu li {
    display:inline;
    list-style-type:none;
    padding-left:.3em;
    margin-left:0em;
    border-left:1px solid #ccc
    }

  #footerMenu  li:first-child {
    border-left:none
    }

</style>
<div id="footer">
	<div style="padding:3px;text-align:left">
	<table width="100%">
	<tr>
	<td>
	
		RegattaCentral &#169; 1999-2018 
	</td>
	<td align="right">
		<ul id="footerMenu">
			<li><a href="/information/about.jsp">About</a></li>
			<li><a href="/information/jobs.jsp">Jobs</a></li>
			<li><a href="/information/press.jsp">Press</a></li>
			<li><a href="https://api.regattacentral.com">Developers</a></li>
			<li><a href="/information/privacy.jsp">Privacy Policy</a></li>
			<li><a href="/information/terms.jsp">Terms &amp; Conditions</a></li>
			<li><a href="/support.jsp">Help</a></li>
		</ul>
	</td>
	</tr>
	</table>
	</div>
</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-24307524-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- version=1.49-->

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"errorBeacon":"bam.nr-data.net","licenseKey":"9974f40920","agent":"","beacon":"bam.nr-data.net","applicationTime":37,"applicationID":"3118059,3132377","transactionName":"MVRaZEBQVktZAU0KVggecmNiHlFWXg1LDlgSWFdeHUFKUU4DWhoXDEJI","queueTime":0}</script></body>
</html>

