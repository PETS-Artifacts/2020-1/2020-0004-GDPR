
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Privacy Policy | CAMEO Chemicals | NOAA</title>
    
<link href="/stylesheets/cameo.css" media="all" rel="stylesheet" type="text/css" />
    <script src="/javascripts/cameo.js" type="text/javascript"></script>
    <meta name="author" content="NOAA Office of Response and Restoration, US GOV" />
    

	<meta name="viewport" content="width = device-width, minimum-scale = 1" />
    <link rel="apple-touch-icon" href="/images/webclips/iPhone-older.png" /> <!-- 120x120 -->
    <link rel="apple-touch-icon" sizes="180x180" href="/images/webclips/iPhone-6-Plus.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="/images/webclips/iPad-Retina.png" />
    <link rel="apple-touch-icon" sizes="167x167" href="/images/webclips/iPad-Pro.png" />
  </head>

<body>









<!-- Site banner -->
<table class="banner" cellpadding="0" cellspacing="0" width="100%">
<tr><td>
	<img src="/images/banner.jpg" width="750" height="60" 
        alt="CAMEO Chemicals" />
</td></tr>
</table>

<!-- BEGIN menu sidebar -->
<div id="sidebar">
<a href="/">Home</a>
<br />
<br />
<a href="/help/cameo_chemicals_help.htm" target="_blank">Help</a>
<br />
<br />

<img class="noborder" src="/images/navbar.gif" alt="" />
<h1>Search Chemicals</h1>
<a href="/search/simple">New Search</a><br />
<br /><img class="noborder" src="/images/navbar.gif" alt="" />
<h1>MyChemicals</h1>

<div style="margin-bottom: 1ex">
chemicals: 0




</div>

<a href="/my">View MyChemicals</a><br /><br />
<a href="/reactivity">Predict Reactivity</a><br />


<br /><img class="noborder" src="/images/navbar.gif" alt="" /><br /><br />
<a href="https://m.cameochemicals.noaa.gov/privacy">Mobile Site</a><br /><br />

<br />

<!-- IOS mobile app link (Apple App Store) -->
<div style="margin-bottom: 5px"><a href="https://itunes.apple.com/app/apple-store/id1151912682?pt=1421333&amp;ct=cameo_chemicals_website_badge&amp;mt=8" target="_blank"><img src="/images/app_store_badge.png" width="130" height="38" alt="Click on this to go to the CAMEO Chemicals page in the App Store." /></a></div>

<!-- Android mobile app link (Google Play store) -->
<div><a href="https://play.google.com/store/apps/details?id=gov.noaa.cameochemical&amp;referrer=utm_source%3Dcameo_chemicals_website%26utm_medium%3Dbadge%26anid%3Dadmob" target="_blank"><img src="/images/google_play_badge.png" width="130" height="38" alt="Click on this to go to the CAMEO Chemicals page on Google Play." /></a></div>



</div>
<!-- END menu sidebar -->


<!-- BEGIN main div & page title -->
<div class="mainbody">



<h1>Privacy Policy</h1>





<!-- BEGIN page content -->


<p>Thank you for your interest in the CAMEO Chemicals program. If you access the website or download and use the desktop application, this policy explains how we handle information we learn about you.</p>

<p>Please be assured that the privacy of our users is of utmost importance to us. <strong>We do not collect personal information that would allow us to identify you by name or contact you directly (such as your email address), unless you choose to provide that information to us voluntarily via email.</strong></p>

<h2>General Information Collected and Stored Automatically</h2>

<p>We automatically collect and store some general information about your use of CAMEO Chemicals, but the information does not identify you personally.</p>

<p>Examples of this general information include:
    <ul>
        <li>Date and time of use</li>
        <li>Internet domain (for example, "xcompany.com" or "yourschool.edu")</li>
        <li>IP address (an Internet Protocol address is a number that is automatically assigned to your computer whenever you are online)</li>
        <li>Type of internet browser and/or computer operating system used to access our program</li>
        <li>Pages viewed in our program</li>
        <li>Search terms and results in our program</li>
        <li>Substances added to MyChemicals</li>
    </ul>
</p>

<p>We use this information to help us make CAMEO Chemicals more useful for users; for example, to learn what ways people are using our search and whether they successfully find the content they're looking for.</p>

<p>This general information may be preserved indefinitely to assist with program management.</p>

<h2>Personal Information That You Voluntarily Provide by Email</h2>

<p>We do not collect personally identifiable information (such as names, email addresses, phone numbers, or addresses), unless you choose to provide it to us. If you send us an email, we use your information to respond to your request. Additionally, we may save your message in an email archive, which we use for program management purposes (for example, it is useful to see how many people asked a particular question).</p>

<p>We may share the information you give us with another government agency if your inquiry relates to that agency. In other limited circumstances, such as responses to requests from Congress and private individuals, we may be required by law to disclose information you submit.</p>

<p>By sending us personal information, you are giving consent for us to use the information for the stated purpose. We do not use information about you in any other ways (such as commercial marketing).</p>

<h2>Interaction with Children</h2>

<p>The content in CAMEO Chemicals is intended primarily for emergency responders and planners, and no content is specifically provided for children. If a child uses CAMEO Chemicals, no personally identifiable information is collected from them unless voluntarily submitted (via email) as a request for information. The information collected is used as described in the section above.</p>

<h2>Cookies</h2>

<p>"Cookies" are small bits of text that are either used for just the duration of a program visit ("session cookies") or saved on a user's computer in order to identify that user--or information about that user--the next time the user returns to a program ("persistent cookies").</p>

<p>CAMEO Chemicals uses session cookies for program navigation. The cookies exist while you’re using CAMEO Chemicals, and then are deleted when you quit the browser or close the CAMEO Chemicals program.</p>

<p>CAMEO Chemicals does not use persistent cookies or any other persistent tracking technology. However, CAMEO Chemicals does use a third-party tool (Google Analytics) that provides basic information about users using a persistent cookie, as described below.</p>

<h2>Google Analytics™</h2>

<p>CAMEO Chemicals uses Google Analytics, a third-party web measurement and customization technology as defined and organized by the federal government's Office of Management and Budget (OMB) in <a href="https://www.whitehouse.gov/sites/whitehouse.gov/files/omb/memoranda/2010/m10-22.pdf" target="_blank">Guidance for Online Use of Web Measurement and Customization Technologies (PDF)</a><img alt="external link" height="10" src="/images/external_link.png" width="12" />, known as OMB M-10-22.</p>

<p>Here's how it works: Google Analytics places a small file on your computer, commonly called a "cookie," so that it can recognize your computer if you use CAMEO Chemical in the future. This cookie does not collect personal identifying information. (This is considered a Tier 2 service in the OMB guidance.)</p>

<p><strong>Google Analytics does not collect personally identifiable information through its cookie and does not combine, match, or cross-reference CAMEO Chemicals information with any other information.</strong> Please review the <a href="https://support.google.com/analytics/answer/6004245" target="_blank">Google Analytics privacy policy</a><img alt="external link" height="10" src="/images/external_link.png" width="12" /> for additional information.</p>

<p>Users who choose to disable this statistical measurement tool will still have full access to CAMEO Chemicals. While the details vary from device to device, most can be set up to accept, reject, or request user intervention when communicating with the internet. Google Analytics uses a cookie that begins with "ga".</p>

<h2>Links to Other Sites</h2>

<p>CAMEO Chemicals includes links to external websites for your convenience. This does not constitute an official endorsement or approval of those sites by the CAMEO Chemicals developers, nor is it a guarantee of the validity of the information provided on those sites. (An internet connection is required to view these external sites.)</p>

<p>Note that once you go to an external website, you are subject to the privacy and security policies of that site. It is a good idea to review that site's privacy policy to determine how it handles information collection.</p>

<h2>Information Protection</h2>

<p>For site security purposes for the CAMEO Chemicals website and to ensure that this service remains available to all users, this government computer system employs software programs to monitor network traffic to identify unauthorized attempts to upload or change information, or otherwise cause damage. If such monitoring reveals evidence of possible abuse or criminal activity, such evidence may be provided to appropriate law enforcement officials. Unauthorized attempts to upload or change information on the CAMEO Chemicals web server are strictly prohibited and may be punishable under the Computer Fraud and Abuse Act of 1986 and the National Information Infrastructure Protection Act or other law.</p>

<h2>Your Rights Under the Privacy Act</h2>

<p><a href="https://www.justice.gov/opcl/privacy-act-1974" target="_blank">The Privacy Act of 1974</a><img alt="external link" height="10" src="/images/external_link.png" width="12" /> provides safeguards against invasion of personal privacy through the misuse of records by agencies within the Executive Branch of the federal government.</p> 

<p>The Privacy Act guarantees three primary rights for citizens of the United States and aliens who are lawfully admitted into the United States for permanent residence:</p>

<ul>
    <li>The right to see records about yourself, subject to Privacy Act exemptions;</li>
    <li>The right to request the amendment of records that are not accurate, relevant, timely or complete; and</li>
    <li>The right of individuals to be protected against unwarranted invasion of their privacy resulting from the collection, maintenance, use, and disclosure of personal information.</li>
</ul>

<p>You may make a <a href="http://www.osec.doc.gov/omo/FOIA/privacyrequest.htm" target="_blank">request to see personal information about yourself</a><img alt="external link" height="10" src="/images/external_link.png" width="12" />. Requests will be processed under both the Privacy Act and the Freedom of Information Act to ensure the greatest access to your personal records.</p>



<!-- END page content -->


<!-- BEGIN page footer -->


<div class="footer">
  <div class="sitelinks">
    <a href="/about">About</a> | 
    <a href="/privacy">Privacy Policy</a> | 
    <a href="/help/cameo_chemicals_help.htm#t=2_about%2Fcontact_us.htm" target="_blank">Contact Us</a>
    | 
    <a href="/survey">Website Satisfaction Survey</a> |
    <a href="https://m.cameochemicals.noaa.gov/privacy">Mobile Site</a>
  </div>
  <div class="meatball-container">
    <img class="meatball" src="/images/noaa-meatball.gif" alt="" />
    <div class="meatball-text">
      <p>Web site owner: 
        <a href="https://response.restoration.noaa.gov/" target="_blank">Office of Response and Restoration</a><img alt="external link" height="10" src="/images/external_link.png" width="12" />, 
        <a href="https://oceanservice.noaa.gov/" target="_blank">National Ocean Service</a><img alt="external link" height="10" src="/images/external_link.png" width="12" />,
        <a href="http://www.noaa.gov/" target="_blank">National Oceanic and Atmospheric Administration</a><img alt="external link" height="10" src="/images/external_link.png" width="12" />.
        <a href="https://www.usa.gov/" target="_blank">USA.gov</a><img alt="external link" height="10" src="/images/external_link.png" width="12" />.
      </p>
      <p>CAMEO Chemicals version 2.7.1.</p>
    </div>
  </div>
</div>

<!-- END page footer -->

</div>
<!-- END main div -->


    <script id="_fed_an_ua_tag" src="/javascripts/federated-analytics.js?agency=DOC&amp;subagency=NOAA&amp;pua=UA-50520236-6" type="text/javascript"></script>

  </body>
</html>







