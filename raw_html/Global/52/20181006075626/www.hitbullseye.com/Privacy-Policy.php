<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<title>Bulls Eye - Privacy Policy</title>
<meta name="keywords" content="privacy policy" />
<meta name="description" content="The page provides the privacy policy adopted by hitbullseye.com with regard to the collection, use, and dissemination of personal information requested from visitors to this site. Your continued use of this website constitutes acceptance of such terms and policies."/>
<meta name="author" content="Bulls Eye" />
<link rel="canonical" href="http://mba.hitbullseye.com/Privacy-Policy.php" />
<meta name="robots" content="index, follow">


<link href="https://plus.google.com/+hitbullseye/" rel="publisher" />

<link rel="shortcut icon" href="images/favicon.ico" />

<!--Bootstrap Css Starts-->
<link rel="stylesheet" type="text/css" href="css/new/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/new/bootstrap-theme.css">
<!--Bootstrap Css Ends-->

<!--Site Main Css Starts-->
<link rel="stylesheet" type="text/css" href="css/new/style.css">
<link rel="stylesheet" type="text/css" href="css/new/form.css">
<link rel="stylesheet" type="text/css" href="css/new/responsive.css">
<link rel="stylesheet" type="text/css" href="css/new/fonts.css">
<!--Site Main Ends-->

<!--Lightbox Starts Scripts-->
<link rel="stylesheet" href="css/new/lightbox.css">
<!--Lightbox Starts Ends-->
<!--Font Awesome Css Starts-->
<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
<!--Font Awesome Css Ends-->

<!-- Google Analytic Code -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-20676820-5', 'auto');
ga('send', 'pageview');
setTimeout("ga('send', 'event', 'unbounce', '10_sec')", 10000);
</script>
<!-- End Google Analytic Pixel Code -->


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '887656554597592');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=887656554597592&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->



</head>

<body id="mba-w-2">
<div class="landscape-mode-outer">
	<div class="landscape-mode text-center">
    <img src="images/device.svg" alt="Device Orientation" />
    <div class="clearfix"></div>
    <h2>Please rotate your device</h2>
    <div class="clearfix"></div>
    <p>We don't support landscape mode yet. Please go back to portrait mode for the best experience</p>
    </div>
</div>

<div class="main-content">
<!--Main Wrapper Starts-->
<div class="wrapper">

	<!--Header Starts-->
    <header>
    <div class="row">
        <div class="container">
        <div class="image-row">
            <div class="image-set">
            <!--Navigation Starts-->
            <div class="col-md-1 col-xs-1 top-navigation">
            	<a href="javascript:void(0)"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <div class="top-nav-toggle">
                    <ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="about-us.php">About</a></li>
                        <li><a href="testimonial.php">Testimonials</a></li>
                        <li><a href="courses.php">Courses</a></li>
                        <li><a href="contact.php">Contact</a></li>
                            
                        <!---<li><a target="_blank" href="//hitbullseye.com/">Home</a></li>--->
                        <li><a target="_blank" href="http://mba.hitbullseye.com/">MBA</a></li>
                        <li><a target="_blank" href="http://cpt.hitbullseye.com/">Placement Training</a></li>
                        <li><a target="_blank" href="http://grad.hitbullseye.com/courses.php#Clat-Law">Law/CLAT</a></li>
                        <li><a target="_blank" href="http://po.hitbullseye.com/">Bank PO/SSC</a></li>
                        <li><a href="http://grad.hitbullseye.com/">Careers After +2</a></li>
                        <li><a href="GRE.php">GRE</a></li>
                        <li><a href="GMAT.php">GMAT</a></li>
                        <li><a href="http://grad.hitbullseye.com/SAT-Coaching.php">SAT</a></li>
                    </ul>
                </div>
            </div>
            <!--Navigation Ends-->                              
            <!--Logo Starts-->
            <div class="col-md-3 col-sm-4 col-xs-6 main-logo">
                <a href="index.php">
                    <img src="images/bullseye-logo.svg" alt="Bulls Eye" title="Bulls Eye" class="img-responsive" />
                </a>
            </div>
            <!--Logo Ends-->
        
            <!--Sign and Sign Up Starts-->
            <div class="col-md-6 col-sm-6 col-xs-12 sign-btns pull-right text-right">
                <a class="js-open-modal btn btn-success" href="javascript:void(0)" onclick="opeloginremote();">Sign In <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                <a class="js-open-modal btn btn-success" href="javascript:void(0)" onclick="operegistrationremote();">Sign Up <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <!--Sign and Sign Up Ends-->
             
            </div>
            </div>
        </div>
    </div>
</header>
<script>
function checkNum(num)
{ 
	var w = ""; 
	var v = "+0123456789"; 
	for (i=0; i < num.value.length; i++) 
	{	
		x = num.value.charAt(i); 
		if (v.indexOf(x,0) != -1) w += x; 
	} 
	num.value = w; 
}
</script>    <!--Header Ends-->
    
    <!--Banner Starts-->
    <div class="row privacy-banner inner-banner2">
    	<div class="container">
        	<!--Navigation Starts-->
            <nav class="navbar navbar-default">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <span class="navbar-brand">MBA</span>
  </div>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navigation-left">
      <li><a href="index.php">Home</a></li>
      <li><a href="about-us.php">About</a></li>      
      <li><a href="testimonial.php">Testimonials</a></li>
      <li><a href="courses.php">Courses</a></li>
      <li><a href="contact.php">Contact</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right social-media">
      <li><a href="https://www.facebook.com/hitbullseyedotcom/" target="_blank"><img src="images/social-icons/h-fb.svg" class="img-responsive" alt="Social Media" /></a></li>
      <li><a href="https://twitter.com/hitbullseye" target="_blank"><img src="images/social-icons/h-twitter.svg" class="img-responsive" alt="Social Media" /></a></li>
      <li><a href="https://www.linkedin.com/company/bulls-eye" target="_blank"><img src="images/social-icons/h-linked-in.svg" class="img-responsive" alt="Social Media" /></a></li>
      <li><a href="https://www.instagram.com/hitbullseye/" target="_blank"><img src="images/social-icons/h-instagram.svg" class="img-responsive" alt="Social Media" /></a></li>
      <li><a href="https://plus.google.com/+hitbullseye" target="_blank"><img src="images/social-icons/h-google.svg" class="img-responsive" alt="Social Media" /></a></li>
      <li><a href="https://www.youtube.com/user/BullsEyeAdmin" target="_blank"><img src="images/social-icons/h-youtube.svg" class="img-responsive" alt="Social Media" /></a></li>
    </ul>
  </div>
</nav>
            <!--Navigation Ends-->
        </div>
        
        <div class="clearfix"></div>
        
        <div class="container">
        	<!--Banner Text Starts-->
            <div class="col-md-8 col-sm-8 col-xs-12 innerbanner-text">
            	<div class="innerbanner-heading3">Privacy <span>Policy</span></div>
            </div>
            <!--Banner Text Ends-->
        </div>
    </div>
    <!--Banner Ends-->
	
    
    <!--Courses Starts-->
    <div class="row inner-content grey-bg">
    	<div class="container">
<h3>Acceptance  of Terms</h3>
  <p class="mbottom3"> Certain products and services may from time to time be made available to you  (&quot;you&quot; or &quot;User&quot;) by Bullseye. By registering/ purchasing  any products or services through BULLSEYE website, you signify that you have read,  understood and agreed to be bound by the Terms of Sale in effect at the time of  purchase (&quot;Terms of Sale&quot;).<br>
  These Terms of  Sale are subject to change without prior written notice at any time, in  Company's sole discretion, and such changes shall apply to any purchases made  after such changes are posted to the Site. Therefore, you should review these  Terms of Sale prior to each registration/ purchase so you will understand the  terms applicable to such transaction. If you do not agree to these Terms of Sale,  do not make any purchases/ register. </p>
  
   <p class="mbottom3"><strong>PLEASE READ THESE TERMS OF SALE CAREFULLY AS THEY  CONTAIN IMPORTANT INFORMATION REGARDING YOUR LEGAL RIGHTS, REMEDIES AND  OBLIGATIONS. THESE INBULLSEYEUDE VARIOUS LIMITATIONS AND EXBULLSEYEUSIONS.</strong></p>
  
  <h3>Purchase  Qualifications; Account Security</h3>
   <p class="mbottom3">To make a purchase on BULLSEYE website, you must comply with these Terms of  Sale. You acknowledge that you are responsible for maintaining the security of,  and restricting access to, your account and password, and you agree to accept  responsibility for all purchases/ registration and other activities that occur  under your account. BULLSEYE reserves the right to refuse or cancel orders at  any time in its sole discretion.</p>
  
  <h3>Termination</h3>
  
 <p class="mbottom3">Termination by you: You may, at any point in time, choose to terminate your subscription to a service by sending us an e-mail. Such termination will not change any amounts due to us.<br>

Termination by BULLSEYE: Breach of any of the terms of this Agreement constitutes a termination of the Agreement. Any and all amounts due under this Agreement shall survive the Term of this Agreement.<br>

If you fail, or BULLSEYE suspects that you have failed, to comply with any of the provisions of this Agreement,  Bullseye including but not limited to failure to make payment of fees due, failure to provide BULLSEYE with a valid credit card or with accurate and complete Registration Data, failure to safeguard your Account information, violation of the Usage Rules or any license to the software, or infringement or other violation of third parties' rights, BULLSEYE, after notice of 07 days to rectify such failure to you, may terminate your subscription and/or your Account if such failure on your part still continues. No refunds will be issued for early Termination.</p>

<h3>Refund incase of incorrect purchase </h3>
 <p class="mbottom3">Product purchased will not be refunded or changed to another program on grounds of incorrect selection at the time of purchase.</p>

<h3>Payment Method and Terms</h3>
 <p class="mbottom3">We currently accept the following forms of payment: <br>
For offline payment: <br>
Demand draft in favour of "Bullseye." payable at <br>
For online payment:<br>
Credit cards and certain Debit cards<br>
Direct Debit of accounts with certain banks<br>
Note: By submitting an order through BULLSEYE website, you authorize the Company, or its designated payment processor, to charge the account you specify for the purchase amount.<br>
All payments are to be made in Indian Rupees.
</p> 

<h3>Delivery</h3>
 <p class="mbottom3">With respect to services purchased through the Site, you acknowledge and agree that upon making such services available to you (or to their intended authorized recipients), BULLSEYE will have fully satisfied its obligation to deliver or otherwise provide such services, regardless of any failure or inability to use such services.</p>

<h3>Product and Service Descriptions and Availability, Errors</h3>
 <p class="mbottom3">We attempt to be as accurate as possible and eliminate errors, however we do not warrant that any product, service or description, photograph, pricing or other information is accurate, complete, reliable, current, or error-free. In the event of an error, whether on the Site, in an order confirmation, in processing an order, delivering a product or service or otherwise, we reserve the right to correct such error and revise your order accordingly if necessary (including charging the correct price) or to cancel the order and refund any amount charged.</p>

<h3>PERSONAL INFORMATION, PRIVACY AND DEBULLSEYEARATION</h3>
 <p class="mbottom3">By registering/ purchasing the product/ service on BULLSEYE website, you become a Bona fide student of BULLSEYE and authorize BULLSEYE to use your name and photograph in any of its promotional communication such as newspapers, ads, leaflets etc. You will not make any claims of whatsoever nature. BULLSEYE may send you regular updates from BULLSEYE on new products, exclusive offers, institute information etc.<br>
Your privacy is important to BULLSEYE. BULLSEYE will not make your contact details available to any third party.</p>

<h3>Disclaimers</h3>
 <p class="mbottom3">Products and services are provided or sold "as-is" and the company disclaims any and all representations and warranties, whether express or implied, including without limitation implied warranties of title, merchantability, fitness for a particular purpose or non-infringement. The company cannot guarantee and does not promise any specific results from use of products or services. Company does not represent or warrant that products, services or any part thereof, are accurate, complete, reliable, current or error-free or that products or services that are downloaded from the site are free of viruses or other harmful components. Therefore, you should exercise caution in the use and downloading of any such content or materials and use industry-recognized software to detect and disinfect viruses.
Reference to any products, services, processes or other information, by trade name, trademark, manufacturer, and supplier or otherwise does not constitute or imply endorsement, sponsorship or recommendation thereof, or any affiliation therewith, by the Company.</p>


<h3>Indemnity</h3>
 <p class="mbottom3">You agree to indemnify and hold the Company, its subsidiaries and affiliates, and each of their directors, officers, agents, contractors, partners and employees, harmless from and against any loss, liability, claim, demand, damages, costs and expenses, including reasonable attorney's fees, arising out of or in connection with your purchase or use of any product or services, or any violation of this Agreement or of any law or the rights of any third party.</p>

<h3>Other</h3>
 <p class="mbottom3">These Terms of Sale, including the Terms of Use, constitute the entire agreement between you and Company regarding purchases you make on BULLSEYE, superseding any prior agreements between you and Company relating to such purchases. The failure of Company to exercise or enforce any right or provision of these Terms of Sale shall not constitute a waiver of such right or provision in that or any other instance. If any provision of this Agreement is held invalid, the remainder of this Agreement shall continue in full force and effect. If any provision of these Terms of Sale shall be deemed unlawful, void or for any reason unenforceable, then that provision shall be deemed severable from these Terms of Sale and shall not affect the validity and enforceability of any remaining provisions. In the event of a conflict between these Terms of Sale and the Terms of Use, these Terms of Sale shall be controlling.</p>

<h3>Notices</h3>
 <p class="mbottom3">BULLSEYE may send you notice with respect to Products/Services by sending an email message to the email address listed in your contact information, by sending a letter via postal mail to the contact address listed in your contact information, or by a posting on the website. Notices shall become effective immediately.</p>

<h3>Events beyond our reasonable control</h3>
 <p class="mbottom3">BULLSEYE will not be held responsible for any delay or failure to comply with its obligations under these conditions if the delay or failure arises from any cause which is beyond its reasonable control including reasons of Force Majeure which shall include any default due to an act of God, war, or threatened war, act of terrorism or threatened act of terrorism, strike, lockout individual action, fire, flood, drought, tempest or other event beyond the control of either party.</p>

<h3>Waiver</h3>
 <p class="mbottom3">If you breach these conditions and BULLSEYE takes no action, BULLSEYE will still be entitled to use its rights and remedies in any other situation where you breach these conditions.</p>


<h3>Books & Material</h3>
 <p class="mbottom3">The books & material is given to the students in several parts. Although most of this material is available to all the students who have enrolled for the course, BULLSEYE Educate reserves the right to give some exam specific portion of the material only to a select group of students. In this case, a student can be asked to submit a copy of the hall ticket of the examination as a proof of registration before he can access the material.</p>

<h3>Questions</h3>
 <p class="mbottom3">If you have any questions about these terms, the practices of the  company, or your dealings with the  company, you can contact @.com or write to: <br>
BULLSEYE Limited,<br>
Sector 8 Chandigarh<br>
India</p>

        </div>
    </div>
    <!--Courses Ends-->
    
    <!--Sign Up Starts-->
    <div class="row signup">
    	<div class="container">
        	<div class="signup-inner">
            	<ul>
                	<li>
                    	<h2>Sign Up for Free</h2>
                        <p>To get a step closer to your Dream Career.</p>
                    </li>
                    
                    <li>
                    	<a href="javascript:void(0)" onclick="operegistrationremote();" class="btn btn-lg">Start Now <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                                              
                    </li>
                </ul>
                
                <div class="clearfix"></div>
            </div>
        </div>
    </div>    <!--Sign Up Ends-->
    
    <!--Footer Starts-->
    <footer class="footer">                
        <!--Quick Links Starts-->
        <div class="row">
            <div class="quick-links container">
                <div class="f-block1">
                    <h2>Courses</h2>
                    <ul>
                        <li><a href="http://mba.hitbullseye.com" target="_blank">MBA</a></li>
                        <li><a href="http://cpt.hitbullseye.com" target="_blank">Placement Training</a></li>
                        <li><a href="http://grad.hitbullseye.com/courses.php#Clat-Law" target="_blank">LAW</a></li>
                        <li><a href="http://po.hitbullseye.com" target="_blank">Bank PO/ SSC</a></li>
                    </ul>
                    
                    <ul>
                    	<li><a href="https://grad.hitbullseye.com/">Careers After +2</a></li>
                        <li><a href="https://www.hitbullseye.com/GRE.php">GRE</a>
                        <li><a href="https://www.hitbullseye.com/GMAT.php">GMAT</a></li>
                        <li><a href="https://grad.hitbullseye.com/SAT-Coaching.php">SAT</a></li>
                        
                    </ul>
                </div>
                
                <div class="f-block2">
                    <h2>Study Zone</h2>
                    <ul>
                        <li><a href="https://www.hitbullseye.com/Quant" target="_blank">Quant</a></li>
                        <li><a href="//www.hitbullseye.com/Verbal" target="_blank">Verbal</a></li>
                        <li><a href="//www.hitbullseye.com/Reasoning" target="_blank">Reasoning</a></li>
                        <li><a href="//www.hitbullseye.com/Data" target="_blank">Data</a></li>
                    </ul>
                </div>
                
                <div class="f-block3">
                    <h2>E-Learning Portal</h2>
                    <ul>
                        <li><a href="http://gk.hitbullseye.com/" target="_blank">G.K</a></li>
                        <li><a href="http://vocabulary.hitbullseye.com/" target="_blank">Vocab Builder</a></li>
                        <li><a href="//gdpi.hitbullseye.com/" target="_blank">Interview Prep</a></li>
                        <li><a href="http://elibrary.hitbullseye.com/" target="_blank">E-Library</a></li>
                    </ul>
                </div>
                
                <div class="f-block4">
                    <h2>Company</h2>
                    <ul>
                        <li><a href="about-us.php">About</a></li>
                        <li><a href="testimonial.php">Testimonial</a></li>
                        <li><a href="courses.php">Courses</a></li>
                        <li><a href="contact.php">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--Quick Links Ends-->
        
        <!--App Download and Social Media Starts-->
        <div class="row app-social">
        	<div class="container">
            	<div class="col-md-6 col-sm-6 col-xs-12 d-app">
                	<h2>Download Now</h2><a href="https://itunes.apple.com/in/app/bulls-eye-mba-test-prep/id901960610?mt=8" target="_blank"><img src="images/appstore-logo.svg" alt="App Store" title="App Store" class="img-responsive" /></a><a href="https://play.google.com/store/apps/details?id=com.bullseye" target="_blank"><img src="images/playstore-logo.svg" alt="Play Store" title="Play Store" class="img-responsive" /></a>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 social pull-right text-right">
                	<h2>Follow Us</h2>
                    <ul>
                    	<li><a href="https://www.facebook.com/hitbullseyedotcom/" target="_blank"><img src="images/social-icons/f-fb.svg" class="img-responsive" alt="Social Media" /></a></li>
                        <li><a href="https://twitter.com/hitbullseye" target="_blank"><img src="images/social-icons/f-twitter.svg" class="img-responsive" alt="Social Media" /></a></li>
                        <li><a href="https://www.linkedin.com/company/bulls-eye" target="_blank"><img src="images/social-icons/f-linkedin.svg" class="img-responsive" alt="Social Media" /></a></li>
                        <li><a href="https://www.instagram.com/hitbullseye/" target="_blank"><img src="images/social-icons/f-instagram.svg" class="img-responsive" alt="Social Media" /></a></li>
                        <li><a href="https://plus.google.com/+hitbullseye" target="_blank"><img src="images/social-icons/f-google.svg" class="img-responsive" alt="Social Media" /></a></li>
                        <li><a href="https://www.youtube.com/user/BullsEyeAdmin" target="_blank"><img src="images/social-icons/f-youtube.svg" class="img-responsive" alt="Social Media" /></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!--App Download and Social Media Ends-->
        
        <!--Copyright Starts-->
        <div class="row copyright-privacy">        	
            <div class="container">
                <div class="col-md-6 col-sm-12 text-left copyright">Copyright © Hit Bullseye 2018  |  All Rights Reserved</div>
                <div class="col-md-6 col-sm-12 text-right privacy"><a href="Privacy-Policy.php">Privacy Policy</a><a href="refund-Policy.php">Refund Policy</a><a href="Terms-of-use.php">Terms of use</a></div>
            </div>
        </div>
        <!--Copyright Ends-->
        
        <a href="#" class="go-top btn btn-lg btn-success"><i class="fa fa-angle-double-up" aria-hidden="true"></i></a>
    </footer>
<div id="load-registration" class="load-registration" style="display:none"></div>
<!--Bootstrap Js Starts-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!--Bootstrap Js Ends-->

<script src="assets/js/jquery-1.9.1.min.js"></script>
<script src="owl-carousel/owl.carousel.min.js"></script>
<script src="js/site-script.js"></script>
<script type="text/javascript" src="js/accordion.js"></script>
<script src="testimonials/js/jquery-1.9.1.min.js"></script> 
<script src="testimonials/js/owl.carousel.js"></script>
<script type="text/javascript" src="//www.hitbullseye.com/registration/registration.js" ></script>


<script>
function getURLParameter(param){
        var pageURL = window.location.search.substring(1); //get the query string parameters without the "?"
        var URLVariables = pageURL.split('&'); //break the parameters and values attached together to an array
        for (var i = 0; i < URLVariables.length; i++) {
            var parameterName = URLVariables[i].split('='); //break the parameters from the values
            if (parameterName[0] == param) {
                return parameterName[1];
            }
        }
        return null;
    }
   function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
 
document.setCookie = function(sName,sValue)
{
    var oDate = new Date(), minutes = 43200;
    oDate.setTime(oDate.getTime() + (minutes * 60 * 1000));
    var sCookie = encodeURIComponent(sName) + '=' + encodeURIComponent(sValue) + ';expires=' + oDate.toGMTString() + ';domain=.hitbullseye.com;path=/';
    document.cookie= sCookie;
}
var source = getURLParameter('utm_source');
if (source) {
            document.setCookie('utm_source',source);
}
else{
    var source = getURLParameter('gclid'); //this will work when you are running ad through google
    if(source){
        source= "GoogleAds"
        document.setCookie('utm_source',source);
    }
}
var admitad_uid= getURLParameter('admitad_uid');

if (admitad_uid) {
            document.setCookie('admitad_uid',admitad_uid);
}

</script>



<style type="text/css">
#owl-demo .item{margin: 3px;}
#owl-demo .item img{display: block; width: 100%; height: auto;}
</style>
 <script type="text/javascript">


$(document).ready(function(){
$("#contactcenter").on("submit",function(){
	if (grecaptcha.getResponse() == ""){
    alert("please fill the captcha");
}
else
    {

    var Sname=$("#name");
	
	var Semail=$("#email");
	
	var Smobile=$("#mobile");
	
	var Scity=$("#city");
	
	var Scourse=$("#course");
	
	var Squery=$("#msg");    		

    //$("#send").replaceWith("<em>sending...</em>");		
			
			//alert(name);
			$.ajax({
					type: 'POST',
					url: 'http://www.hitbullseye.com/sendmessage_test.php',
					data: $("#contactcenter").serialize(),
					success: function(data) {
					//alert(data);
						if(data == 1) {
						          //$("#success").html("");
								//$("#contact").fadeOut("fast", function(){
								//$(this).before("<p><strong>Your query has been successfully submitted. Our representative will revert to you in next 24-48 hours.</strong></p>");
								//setTimeout("$.fancybox.close()", 50000);
							//});
                            alert("Your query has been successfully submitted. Our representative will revert to you in next 24-48 hours.");
     $('#contactcenter').trigger("reset");
      location.reload();
						}else {
							$("#success").html(data);
							
						}
					}
				});
	}
  });
  
  $("#contactcentreonline").on("submit",function(){
	if (grecaptcha.getResponse() == ""){
    alert("please fill the captcha");
}
else
    {

    var Sname=$("#name");
		var Semail=$("#email");
		var Smobile=$("#mobile");
		var Scity=$("#city");
		var Scourse=$("#course");
		var Squery=$("#msg");
		var urlpath = "SITE_PATH";
				$("#send").replaceWith("<em>sending...</em>");
			$.ajax({
				type: 'POST',
				url: '/sendmessage_test.php',
				data: $("#contactcentreonline").serialize(),
				success: function(data) {
					if(data == 1) {
						//$("#contact").fadeOut("fast", function(){
							//$(this).before("<p><strong>Your query has been successfully submitted. Our representative will revert to you in next 24-48 hours.</strong></p>");
						//	$("#myModalLabel").hide();
						//});
                        alert("Your query has been successfully submitted. Our representative will revert to you in next 24-48 hours.");
     $('#contactcentreonline').trigger("reset");
      location.reload();
					}
				}
			});
		return false;
	}
  });

	
	});

</script>    <!--Footer Ends-->
</div>
<!--Main Wrapper Ends-->


</div>
</body>
</html>