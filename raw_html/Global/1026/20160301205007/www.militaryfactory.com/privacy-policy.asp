
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1" />
<link rel="stylesheet" href="/common/2016/global4.css" type="text/css" />
<link rel="author" href="https://plus.google.com/113924982996480362000"/>
<link rel="image_src" href="http://www.militaryfactory.com/aircraft/imgs/northamerican-rockwell-ov10-bronco.jpg" />
<title>Military Factory Privacy Policy</title>
<meta name="description" content="The privacy policy page for MilitaryFactory.com." />
<meta property="og:title" content="MilitaryFactory.com Privacy Policy" /><!-- 95 Character Limit -->
<meta property="og:type" content="article" />
<meta property="og:image" content="http://www.militaryfactory.com/aircraft/imgs/northamerican-rockwell-ov10-bronco.jpg" />
<meta property="og:url" content="http://www.militaryfactory.com/privacy-policy.asp" />
<meta property="og:description" content="The provacy policy page for MilitaryFactory.com." /><!-- 297 Character Limit -->
<!-- Google Analytics START -->
<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-864198-1', 'militaryfactory.com'); ga('send', 'pageview');</script>
<!-- Google Analytics END -->
<!-- !!!!!!!!!!!!!!!!!!!!!!GOOGLEPLUS!!!!!!!!!!!!!!!!!!!! -->
<script type="text/javascript">(function() {var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true; po.src = 'https://apis.google.com/js/plusone.js'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);})();</script>
<!-- !!!!!!!!!!!!!!!!!!!!!!GOOGLEPLUS!!!!!!!!!!!!!!!!!!!! -->

<!-- !!!!!!!!!!!!!!!!!!!!!!PAGE-LEVEL ADS!!!!!!!!!!!!!!!!!!!! -->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>(adsbygoogle = window.adsbygoogle || []).push({google_ad_client: "ca-pub-5604186322601931",enable_page_level_ads: true});</script>
<!-- !!!!!!!!!!!!!!!!!!!!!!PAGE-LEVEL ADS!!!!!!!!!!!!!!!!!!!! -->
</head>
<body>
<!-- google_ad_section_start -->
<!-- !!!!!!!!!!!!!!!!!!!!!!HEADER!!!!!!!!!!!!!!!!!!!!!! -->
<div style="width:100%; background-color:#000; padding:0.25%;">
<div style="max-width:1200px; margin-left:auto; margin-right:auto;">
<div id="logoImgHolder"><a href="http://www.militaryfactory.com/" title="Military Weapons"><img src="/imgs/design/88x47-mf-logo.png" width="88" height="47" border="0" /></a></div>
<div id="searchBlockHolder">
<script>(function() {var cx = 'partner-pub-5604186322601931:1661576606'; var gcse = document.createElement('script'); gcse.type = 'text/javascript'; gcse.async = true; gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(gcse, s);})();</script><gcse:searchbox-only></gcse:searchbox-only>
</div><!-- Search Form Container END -->
</div>
</div>

<div id="MainNavHolder">
<div style="margin-left:auto; margin-right:auto; max-width:1200px;">
<a href="/military_pay_scale.asp" class="newMainNavLinks" title="Military Pay Scale"><div style="width:100%; max-width:15%; text-align:center; box-sizing:border-box; padding-top:10px; padding-bottom:10px; position:relative; display:inline-block;">Military Pay</div></a>
<a href="/ranks/index.asp" class="newMainNavLinks" title="Military Ranks"><div style="width:100%; max-width:16%; text-align:center; box-sizing:border-box; padding-top:10px; padding-bottom:10px; position:relative; display:inline-block;">Military Ranks</div></a>
<a href="/aircraft/index.asp" class="newMainNavLinks" title="Military Aircraft"><div style="width:100%; max-width:180px; text-align:center; box-sizing:border-box; padding-top:10px; padding-bottom:10px; position:relative; display:inline-block;">Aircraft</div></a>
<a href="/smallarms/index.asp" class="newMainNavLinks" title="Guns and Equipment"><div style="width:100%; max-width:16%; text-align:center; box-sizing:border-box; padding-top:10px; padding-bottom:10px; position:relative; display:inline-block;">Small Arms</div></a>
<a href="/armor/index.asp" class="newMainNavLinks" title="Military Land Weapons"><div style="width:100%; max-width:16%; text-align:center; box-sizing:border-box; padding-top:10px; padding-bottom:10px; position:relative; display:inline-block;">Land Systems</div></a>
<a href="/ships/index.asp" class="newMainNavLinks" title="Naval Warships and Submarines"><div style="width:100%; max-width:16%; text-align:center; box-sizing:border-box; padding-top:10px; padding-bottom:10px; position:relative; display:inline-block;">Navy Ships</div></a>
</div>
</div>

<div id="hdrLdrbrdAdHolder">
<!--Leaderboard Ad Block START-->
<div id="tribalLdrbrd728"></div>
<!--Leaderboard Ad Block END-->
</div>
<!-- !!!!!!!!!!!!!!!!!!!!!!HEADER!!!!!!!!!!!!!!!!!!!!!! -->
<!-- !!!!!!!!!!!!!!!!!!!!!!BACKGROUND IMAGE!!!!!!!!!!!!!!!!!!!!!! -->
<div id="bgWrapper"><!-- Article BG Big Pic Wrapper START -->
<div id="bgImage" style="background-image:url(/aircraft/imgs/northamerican-rockwell-ov10-bronco.jpg);"><!-- Article BG Big Pic START -->
<div id="bgTab"><!-- Article Title Tab Container START -->
<div id="bgTitleContainer">
<header>
<div id="bgFlagContainer"><img src="/imgs/design/40x22-mf-logo.png" alt="Military Factory" width="40" height="22" border="0" /></div>
<h1><span class="textDkBlue">Military Factory Site Privacy Policy</span></h1>
</header>
</div>
</div> <!-- Article Title Tab Container END -->
</div><!-- Article BG Big Pic END -->        
</div><!-- Article BG Big Pic Contatiner END -->
<!-- !!!!!!!!!!!!!!!!!!!!!!BACKGROUND IMAGE!!!!!!!!!!!!!!!!!!!!!! -->
<!-- !!!!!!!!!!!!!!!!!!!!!!PAGE AUTHOR and DATE!!!!!!!!!!!!!!!!!! -->
<div id="articleTiles">
<span class="textMedium1 textLtGray">Authored By <span class="textBold textDkBlue">Staff Writer</span></span>
<span class="textMedium1 textLtGray"> | Last Updated: 12/17/2015</span>		
</div>
<!-- !!!!!!!!!!!!!!!!!!!!!! PAGE AUTHOR and DATE !!!!!!!!!!!!!!!!!! -->
<!-- !!!!!!!!!!!!!!!!!!!!!! PAGE CAPTION !!!!!!!!!!!!!!!!!!!!!!!!!! -->
<div id="articleTiles"><h2>Because your privacy is important to us.</h2><br><br>
<!-- !!!!!!!!!!!!!!!!!!!!!! SOCIALMEDIA !!!!!!!!!!!!!!!!!!!!!!!!!! -->
<!-- Facebook --><iframe src="http://www.facebook.com/plugins/like.php?href=http://www.militaryfactory.com/privacy-policy.asp&amp;layout=button_count&amp;show_faces=true&amp;width=100&amp;action=like&amp;font=verdana&amp;colorscheme=light&amp;height=25" scrolling="no" frameborder="0" style="border:none; width:100px; height:25px; float:left;" allowTransparency="true"></iframe><!-- Facebook -->   
<!-- Google Plus --><div class="g-plusone" data-size="medium"></div><!-- Google Plus -->
<!-- !!!!!!!!!!!!!!!!!!!!!! SOCIALMEDIA !!!!!!!!!!!!!!!!!!!!!!!!!! -->
<br><br>
</div>
<!-- !!!!!!!!!!!!!!!!!!!!!! PAGE CAPTION !!!!!!!!!!!!!!!!!!!!!!!!!! -->
<!-- !!!!!!!!!!!!!!!!!!!!!! CONTENT !!!!!!!!!!!!!!!!!!!!!! -->
<div id="wrapper"><!-- wrapper START -->
<div id="sidebar-left"><!-- Sidebar-Left START -->
<!-- !!!!!!!!!!!!!!!!!!!!!!SIDEBAR LEFT CONTENT!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
<!-- !!!!!!!!!!!!!!!!!!!!!!ADBLOCK 1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
<div id="adsenseColumn300">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- MF Sitewide Gen Rect RESP: 300x250: 12/5/13 -->
<ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-5604186322601931" data-ad-slot="7528691001" data-ad-format="auto"></ins>
<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
</div>
<!-- !!!!!!!!!!!!!!!!!!!!!!ADBLOCK 1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
<!-- !!!!!!!!!!!!!!!!!!!!!!RECENT ARTICLES!!!!!!!!!!!!!!!!!!!!!! -->
<div id="genericColumn300" style="margin-top:3%;"><!-- Recent Articles Container START -->
<li style="list-style-type:none; line-height:250%; text-align:left; padding:1%; background-color:#efefef;"><span class="textBold textLtGray">New on MilitaryFactory.com:</span></li>
<!------------- Enter New Records Under This Line - keep this list to five records for best formatting ----------------->

<li style="list-style-type:none; line-height:175%; text-align:left; border-bottom:1px solid #ccc; padding:2%;"><a href="/aircraft/detail.asp?aircraft_id=1524" title="History, specs and pictures of the British Supermarine B.12-36 heavy bomber prototype" class="textMedium1 BaseGrayLinks" style="text-decoration:none;">A German air raid ended the prospects of this early-war British heavy bomber by Supermarine</a></li>
<!--RECORD-->

<li style="list-style-type:none; line-height:175%; text-align:left; border-bottom:1px solid #ccc; padding:2%;"><a href="/ships/detail.asp?ship_id=HMS-Belleisle" title="History, specs and pictures of the British Navy ironclad warship HMS Belleisle" class="textMedium1 BaseGrayLinks" style="text-decoration:none;">HMS Belleisle was originally laid down for the Ottoman Navy - but then requisitioned by the Royal Navy</a></li>
<!--RECORD-->

<li style="list-style-type:none; line-height:175%; text-align:left; border-bottom:1px solid #ccc; padding:2%;"><a href="/armor/detail.asp?armor_id=874" title="History, specs and pictures of the Russian Army KAMAZ Typhoon MRAP vehicle" class="textMedium1 BaseGrayLinks" style="text-decoration:none;">The KAMAZ Typhoon MRAP wheeled vehicle forms part of the new-look Russian Army</a></li>
<!--RECORD-->

<li style="list-style-type:none; line-height:175%; text-align:left; border-bottom:1px solid #ccc; padding:2%;"><a href="/aircraft/detail.asp?aircraft_id=1528" title="History, specs and pictures of the Indian HAL HTT-40 basic trainer aircraft" class="textMedium1 BaseGrayLinks" style="text-decoration:none;">The HAL HTT-40 is set to become a standard basic trainer of the Indian Air Force</a></li>
<!--RECORD-->

<li style="list-style-type:none; line-height:175%; text-align:left; border-bottom:1px solid #ccc; padding:2%;"><a href="/ships/detail.asp?ship_id=sang-o-attack-submarine-north-korea" title="History, specs and pictures of the North Korean Sang-O class attack submarine" class="textMedium1 BaseGrayLinks" style="text-decoration:none;">A large portion of the North Korean submarine force is made up of the relatively compact Sang-O-class</a></li>
<!--RECORD-->

</div><!-- Recent Articles Container END -->
<!-- !!!!!!!!!!!!!!!!!!!!!!RECENT ARTICLES!!!!!!!!!!!!!!!!!!!!!! -->
<!-- !!!!!!!!!!!!!!!!!!!!!!SIDEBAR LEFT CONTENT!!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
</div><!-- Sidebar-Left END -->
<div id="content-right"><!-- Content-Right START -->
<span class="textMedium1 textDkGray">
<p><span class="textBold">Stat Tracking</span><br>
  For your information,  the following are some examples of the information we collect during a typical visit to our website:</p><br>
• Browser Type and Resolution<br />
• Operating System<br />
• IP Address<br />
• Entry Page, Pages Visited in Order of Visit and Exit Page<br />
• Inbound Links from whence you came to our website (if coming from another website's link)</p><br>
<p>For a real-time example of some of what is tracked and what isn't, you can always visit our publicly-displayed traffic stats information <a href="http://extremetracking.com/open;unique?login=m1l1fact" target="_blank">here</a>.</p><br>
<p><span class="textBold">3rd Party Advertisements - Google Adsense</span><br>
&nbsp;Some of   these advertisers may use technology such as cookies when they advertise on our site, which will also send these advertisers (such as Google through the Google AdSense program) information including your IP address, your ISP, the browser you used to visit our site, and sometimes whether you have the Flash player installed or not.&nbsp; This is generally used for geotargeting purposes (showing New York real estate ads to someone in New York, for example) or showing certain   ads based on specific sites visited (such as showing cooking ads to someone who frequents cooking sites).</p><br>
<p>In the case of Google Adsense ads, this ad server uses the DoubleClick DART cookie. As per the Google website, it is explained as such:</p><br>
<p>• Google, as a third party vendor, uses cookies to serve ads on this website (www.MilitaryFactory.com).<br />
• Google's use of the DART cookie enables it to serve ads to your users based on their visit to your sites and other sites on the Internet.<br />
• Users may opt out of the use of the DART cookie by visiting the <A href="http://www.google.com/privacy_ads.html" target="_blank">Google ad and content network privacy policy</A>.</p><br>
<p>You can chose to disable or selectively turn off  cookies or third-party cookies in your browser settings, or by managing preferences in programs such as Norton Internet Security.</p><br>
<p><span class="textBold">3rd Party Advertisements - TribalFusion, BurstMedia</span><br>
We use third-party advertising companies to serve ads when you visit our Web site. These companies may use aggregated information (not including your name, address, email address or telephone number) about your visits to this and other Web sites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, click <a href="http://www.networkadvertising.org/managing/opt_out.asp" target="_blank" rel="nofollow">here</a>.</p><br />
<p><span class="textBold">Affiliate Partner - Quinstreet</span><br>
We are partnered with Quinstreet to deliver education-based results through their proprietary search engine. These ads appear across our site as interactive forms with user-initiated drop-down selected options or clearly marked banner advertising. Use of these forms is entirely at your discretion, though particularly useful to those seeking more information from institutions about programs currently offered or military benefits to enrollees. MilitaryFactory.com does NOT record any of the information you deliberately send to these schools requesting additional information.</p>
</span>
<!-- !!!!!!!!!!!!!!!!!!!!!! PAGE NAVIGATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
<nav>
<div style="width:100%; display:inline-block; position:relative; margin-top:2%; padding:1%;">
<div id="navLeftCol"> <!-- Left Column Container START -->
<span class="textMedium">
<span class="textBold">MAIN:</span>
<p>&bull; <a href="/military_pay_scale.asp" class="BaseBlueLinks" title="Military Pay Scale Chart">2016 Military Pay Scale Chart</a><br />
&bull; <a href="/ranks/index.asp" class="BaseBlueLinks" title="Military Ranks">Military Ranks</a><br />
&bull; <a href="/medals/index.asp" class="BaseBlueLinks" title="U.S. Military Medals Listing">U.S. Military Medals &amp; Ribbons</a><br />
&bull; <a href="/aircraft/index.asp" class="BaseBlueLinks" title="Military Aircraft">Aircraft</a> / <a href="/aircraft/military-helicopters.asp" class="BaseBlueLinks" title="Military Helicopters">Helicopters</a><br />
&bull; <a href="/smallarms/index.asp" class="BaseBlueLinks" title="Military Guns">Small Arms</a><br />
&bull; <a href="/armor/main-battle-tanks.asp" class="BaseBlueLinks" title="Main Battle Tanks">Tanks</a> / <a href="/armor/index.asp" class="BaseBlueLinks" title="Military Vehicles and Artillery">Armored Vehicles</a><br>
&bull; <a href="/ships/index.asp" class="BaseBlueLinks" title="Navy Ships"> Ships / Submarines</a><br />
&bull; <a href="/worldwar2/weapons.asp" class="BaseBlueLinks" title="Listing of all WW2 Weapons">WW2 Weapons</a><br />
&bull; <a href="/world-war-1/weapons.asp" class="BaseBlueLinks" title="Listing of all WW1 Weapons">WW1 Weapons</a>
</p>
</span>
</div> <!-- Left Column Container END -->            
<div id="navRightCol"> <!-- Right Column Categories Container START -->
<span class="textMedium">
<span class="textBold">RESOURCES:</span><br />
<p>&bull; <a href="/american_war_deaths.asp" class="BaseBlueLinks" title="American War Deaths Through History">American War Deaths</a><br />
&bull; <a href="/military_alphabet_code.asp" class="BaseBlueLinks" title="Military Alphabet Code">Military Alphabet Code</a><br />
&bull; <a href="/dictionary/military-dictionary.asp" class="BaseBlueLinks" title="Military Dictionary">Military Dictionary</a><br />
&bull; <a href="/conversioncalculators/index.asp" class="BaseBlueLinks" title="Conversion Calculators">Conversion Calculators</a><br />
&bull; <a href="/battles/french_military_victories.asp" class="BaseBlueLinks" title="French Military Victories">French Military Victories</a><br />
&bull; <a href="/5-star-generals.asp" class="BaseBlueLinks" title="5 Star Generals">5-Star Generals</a><br />
&bull; <a href="/glossary.asp" class="BaseBlueLinks" title="Military Acronyms Glossary">Military Acronyms</a><br />
&bull; <a href="/military_map_symbols.asp" class="BaseBlueLinks" title="Military Map Symbols">Military Map Symbols</a><br />
&bull; <a href="/smallarms/ballistics-chart.asp" class="BaseBlueLinks" title="Ammunition Ballistics Chart">Ballistics Chart</a>
</p>
</span> 
</div><!-- Close right column -->
</div><!-- Encompass all footer -->
</nav>
<!-- !!!!!!!!!!!!!!!!!!!!!! PAGE NAVIGATION !!!!!!!!!!!!!!!!!!!!!!!!!!!!! -->
</div><!-- Content-Right END -->
</div><!-- wrapper END -->  
<!-- !!!!!!!!!!!!!!!!!!!!!! CONTENT !!!!!!!!!!!!!!!!!!!!!! -->
<!-- !!!!!!!!!!!!!!!!!!!!!! FOOTER !!!!!!!!!!!!!!!!!!!!!! -->
<footer>

<div style="width:100%; display:inline-block; position:relative; background-color:#efefef; margin-top:1%;">
<div style="width:98%; max-width:1200px; padding:1%; text-align:center; margin-left:auto; margin-right:auto; border:thin solid #CCC; margin-top:1%; margin-bottom:1%;">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script><!-- MF Matched Content -->
<ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-5604186322601931" data-ad-slot="1964569402" data-ad-format="autorelaxed"></ins>
<script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
</div>
</div>

<div style="width:100%; display:inline-block; background-color:#000000;"><!-- All FOOTER Container START -->
<div style="width:100%;max-width:1200px;text-align:left;padding:1%; margin-left:auto; margin-right:auto;"><!-- Red Area Container START -->
<p><span class="textMedium1 textWhite"><a href="/disclaimer.asp" class="WhiteLinksFooter" title="Military Factory Site Disclaimer">Site Disclaimer</a> &nbsp;|&nbsp; <a href="/privacy-policy.asp" class="WhiteLinksFooter" title="Military Factory Privacy Policy">Privacy Policy</a> &nbsp;|&nbsp; <a href="/sitemap.asp" title="Military Factory Site Map" class="WhiteLinksFooter">Site Map</a></span></p><br>
<p><span class="textLighterGray textMedium1">&copy;2016 <a href="http://www.militaryfactory.com" class="WhiteLinksFooter" title="Military Weapons">www.MilitaryFactory.com</a> &bull; Content &copy;2003-2016 MilitaryFactory.com &bull; All Rights Reserved &bull; Site Contact Email: militaryfactory at gmail dot com. The "Military Factory" name and MilitaryFactory.com logo are registered &reg; trademarks and protected by all applicable domestic and international intellectual property laws. Material presented throughout this website is for historical and entertainment value and should <span class="textItalics">not</span> to be construed as usable for hardware restoration, maintenance or general operation. Please consult manufacturers for such information. Use of images originating from the US DoD, UK MoD, and other similar government intitutions should not be taken as an endorsement of this site and/or its presented content.</span></p><br />
<span class="textMedium1 textWhite textBold">On Facebook:</span>
<a href="http://www.facebook.com/pages/Military-Factory/201817485855" target="_blank" title="Military Factory on Facebook"><img src="/imgs/design/mf-network.png" alt="Military Factory Facebook Logo" width="20" height="20" border="0" /></a></p><br />
<!-- ExtremeTracker START -->
<a href="http://extremetracking.com/open?login=m1l1fact"
target="_top"><img src="http://t1.extreme-dm.com/i.gif"
alt="eXTReMe Tracker"
name="EXim" width="41" height="38" border="0" id="EXim" /></a>
<script type="text/javascript" language="JavaScript1.2"><!--
EXs=screen;EXw=EXs.width;navigator.appName!="Netscape"?
EXb=EXs.colorDepth:EXb=EXs.pixelDepth;//-->
</script>
<script type="text/javascript"><!--
var EXlogin='m1l1fact' // Login
var EXvsrv='s10' // VServer
navigator.javaEnabled()==1?EXjv="y":EXjv="n"; EXd=document;EXw?"":EXw="na";EXb?"":EXb="na"; EXd.write("<img src=http://e1.extreme-dm.com","/"+EXvsrv+".g?login="+EXlogin+"&amp;","jv="+EXjv+"&amp;j=y&amp;srw="+EXw+"&amp;srb="+EXb+"&amp;","l="+escape(EXd.referrer)+" height=1 width=1>");//-->
</script>
<noscript><img height="1" width="1" alt="" src="http://e1.extreme-dm.com/s10.g?login=m1l1fact&amp;j=n&amp;jv=n"/></noscript>
<!-- ExtremeTracker END -->
</div><!-- Red Area Container END -->
</div><!-- All FOOTER Container END -->
</footer>
<!--Ad Code Holder Tribal START-->
<div style="display:none">
<div id="adsource-0">
<script type="text/javascript"><!--
e9 = new Object(); e9.size = "970x250,728x90"; e9.addBlockingCategories="Adult,Flashing,dieting,Survey,Violence,Pop-up,preroll,Sweepstakes,Gambling,Tobacco,Pop-under,Full-page,overlay,Audio,Unicast,tag-based-skins,Floating,Warning,Alcohol";
//--></script>
<script type="text/javascript" src="http://tags.expo9.exponential.com/tags/MilitaryFactory/ROS/tags.js"></script>
</div>
</div>
<script type="text/javascript">
source = document.getElementById("adsource-0");
placeholder = document.getElementById("tribalLdrbrd728");
placeholder.appendChild(source);
</script>
<!--Ad Code Holder Tribal END-->
<!-- !!!!!!!!!!!!!!!!!!!!!! FOOTER !!!!!!!!!!!!!!!!!!!!!! -->
<!-- google_ad_section_end -->
</body>
</html>