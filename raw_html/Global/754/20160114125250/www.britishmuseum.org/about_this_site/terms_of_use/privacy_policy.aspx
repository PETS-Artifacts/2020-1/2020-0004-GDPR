<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html lang="en-GB" xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	

British Museum - Privacy policy
</title><link href="/WebResource.axd?d=9SIonGbKeFVKuKsR_reirFktlYIIvlsRzc0rYI78h5uGJubjr5Gu1szq_pN7EXWmvegrJTmqH2vzyf_dhQ-I_UAyXIzm-Wxv3p4FHxzjSMgNMDm8qUyr1635tGEo3CFJxFWq-Dt6kspCudR5nNQ7MNEqVgI1&amp;t=634383934428827289" type="text/css" rel="stylesheet" /><base href="http://www.britishmuseum.org/" /><meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info = {"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"24faa59c77","applicationID":"6028717","transactionName":"NF1aZBYAV0FZWkwNDg0XeWM0Tl1XXlhNCBVNWUtAHA==","queueTime":0,"applicationTime":261,"ttGuid":"8D5089FA525D502E","agent":"js-agent.newrelic.com/nr-768.min.js"}</script>
<script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({QJf3ax:[function(e,n){function t(e){function n(n,t,a){e&&e(n,t,a),a||(a={});for(var u=c(n),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,t);return s}function a(e,n){f[e]=c(e).concat(n)}function c(e){return f[e]||[]}function u(){return t(n)}var f={};return{on:a,emit:n,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=e("gos");n.exports=t()},{gos:"7eSDFh"}],ee:[function(e,n){n.exports=e("QJf3ax")},{}],3:[function(e,n){function t(e){return function(){r(e,[(new Date).getTime()].concat(i(arguments)))}}var r=e("handle"),o=e(1),i=e(2);"undefined"==typeof window.newrelic&&(newrelic=window.NREUM);var a=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit","noticeError"];o(a,function(e,n){window.NREUM[n]=t("api-"+n)}),n.exports=window.NREUM},{1:12,2:13,handle:"D5DuLP"}],gos:[function(e,n){n.exports=e("7eSDFh")},{}],"7eSDFh":[function(e,n){function t(e,n,t){if(r.call(e,n))return e[n];var o=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return e[n]=o,o}var r=Object.prototype.hasOwnProperty;n.exports=t},{}],D5DuLP:[function(e,n){function t(e,n,t){return r.listeners(e).length?r.emit(e,n,t):void(r.q&&(r.q[e]||(r.q[e]=[]),r.q[e].push(n)))}var r=e("ee").create();n.exports=t,t.ee=r,r.q={}},{ee:"QJf3ax"}],handle:[function(e,n){n.exports=e("D5DuLP")},{}],XL7HBI:[function(e,n){function t(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:i(e,o,function(){return r++})}var r=1,o="nr@id",i=e("gos");n.exports=t},{gos:"7eSDFh"}],id:[function(e,n){n.exports=e("XL7HBI")},{}],G9z0Bl:[function(e,n){function t(){var e=d.info=NREUM.info,n=f.getElementsByTagName("script")[0];if(e&&e.licenseKey&&e.applicationID&&n){c(p,function(n,t){n in e||(e[n]=t)});var t="https"===s.split(":")[0]||e.sslForHttp;d.proto=t?"https://":"http://",a("mark",["onload",i()]);var r=f.createElement("script");r.src=d.proto+e.agent,n.parentNode.insertBefore(r,n)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=e("handle"),c=e(1),u=window,f=u.document;e(2);var s=(""+location).split("?")[0],p={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-768.min.js"},d=n.exports={offset:i(),origin:s,features:{}};f.addEventListener?(f.addEventListener("DOMContentLoaded",o,!1),u.addEventListener("load",t,!1)):(f.attachEvent("onreadystatechange",r),u.attachEvent("onload",t)),a("mark",["firstbyte",i()])},{1:12,2:3,handle:"D5DuLP"}],loader:[function(e,n){n.exports=e("G9z0Bl")},{}],12:[function(e,n){function t(e,n){var t=[],o="",i=0;for(o in e)r.call(e,o)&&(t[i]=n(o,e[o]),i+=1);return t}var r=Object.prototype.hasOwnProperty;n.exports=t},{}],13:[function(e,n){function t(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(0>o?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=t},{}]},{},["G9z0Bl"]);</script>
<link rel="stylesheet" href="/templates/default/css/system/stylesheet.css?tick=2012.11.15-1636" type="text/css" media="screen" /><link rel="stylesheet" href="/templates/default/css/system/print.css?tick=2012.11.15-1636" type="text/css" media="print" /><link rel="stylesheet" href="/templates/default/css/Home_Page.css" type="text/css" media="screen" />


<!--[if IE]>
	<link rel="stylesheet" href="templates/default/css/system/ie_all.css?tick=@{buildDate}" type="text/css" />
	<immCore:Null></immCore:Null>
<![endif]-->
<!--[if IE 8]>
	<link rel="stylesheet" href="templates/default/css/system/ie_8.css?tick=@{buildDate}" type="text/css" />
<![endif]-->
<!--[if IE 7]>
	<link rel="stylesheet" href="templates/default/css/system/ie_7.css?tick=@{buildDate}" type="text/css" />
<![endif]-->
<!--[if lt IE 7]>
	<link rel="stylesheet" href="templates/default/css/system/ie_lt_7.css?tick=@{buildDate}" type="text/css" />
<![endif]-->



<script src="/javascripts/v2/setup.js?tick=2015-03-20-1115" type="text/javascript"></script>



<!-- OpenSearch test providers -->
<link rel="search" type="application/opensearchdescription+xml" title="British Museum" href="/OpenSearchBMSite.xml" />
<link rel="search" type="application/opensearchdescription+xml" title="British Museum collection database" href="/OpenSearchCollection.xml" />


<link rel="Shortcut Icon" href="/favicon.ico" />


<!-- Google Analytics -->
<script type="text/javascript">
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-1005261-1', 'auto'); 
ga('require', 'linkid', {'levels': 5});   
ga('send', 'pageview');   

ga('require', 'linker'); 
ga('linker:autoLink', ['britishmuseumshoponline.org']);

</script>

<!-- End Google Analytics -->
<meta name="GENERATOR" content="Immediacy .NET CMS - Licensed To : British Museum" /><meta name="keywords" content="privacy, policy, data protection, information, confidentiality" /><meta name="description" content="Privacy policy for the British Museum website" /></head>
<!-- Generates body tag with section class -->
<body class="Aboutthissite">
	
		<div id="wrapper">
		
			<p class="skipLinkList"><a href="/about_this_site/terms_of_use/privacy_policy.aspx#mainContent" class="skipLink">Skip to content</a></p>				

		    
	<script type="text/javascript">
	$(document).ready(function(){
		$.getJSON("http://www2.britishmuseumshoponline.org/services/GetCookiePolicyValue/?callback=?",function(data) {
			//console.log("cookiesPolicy= " + data.cookiesPolicy);		
			if (data.cookiesPolicy != 1) { 
				$("#cookiePrompt").css("display","block"); // show the message
				$(".cookie-continue").live("click",function(e){  // set up close button
					e.preventDefault();
					$("#cookiePrompt").slideUp(300);
				});	
			}
		});
	});
	</script>

	<div id="cookiePromptWrapper" class="headerPromptWrapper">
		<div id="cookiePrompt" class="headerPrompt" style="display:none;">  
			<div class="grid_12">			
				<div class="grid_11 block alpha">			
					<p>The British Museum uses cookies to ensure you have the best browsing experience 
					and to help us improve the site.<br />
					By continuing to browse the site you are agreeing to 
					our use of cookies. 
					<a href="about_this_site/footer_components/cookies.aspx">Find out more</a>
					</p>
				</div>
				<div class="grid_1 block omega">
					<div class="cta smallButton black margin8pxT " style="float:right;"><a href="#" class="cookie-continue" title="close">x</a></div>
				</div>
			</div>
			
		</div>
	</div>


<div id="headerWrapper">

<!-- div#headerBar - open -->
    <div id="headerBar">
        <div id="headerLogo">
            <p><a href="/?ref=header" title="Home">The British Museum</a></p>
            <img src="images/bmlogo_print.png" class="printOnly" alt="" />
        </div>
        
        <!-- Global Menu -->
        <div id="headerMenu">
            
                    <ul class="clearfix">
                        
                
                    <li>
                        <div>
                        <a href="/visiting.aspx" id="Header1" title="Visiting"><span></span>Visiting</a>
                        </div>
                    </li>
                

                    <li>
                        <div>
                        <a href="/membership.aspx" id="Header1030" title="Membership"><span></span>Membership</a>
                        </div>
                    </li>
                

                    <li class="current">
                        <div>
                        <a href="/whats_on.aspx" id="Header3" title="What's on"><span></span>What's on</a>
                        </div>
                    </li>
                

                    <li>
                        <div>
                        <a href="/support_us.aspx" id="Header1014" title="Support us"><span></span>Support us</a>
                        </div>
                    </li>
                

                    <li>
                        <div>
                        <a href="/research.aspx" id="Header13" title="Research"><span></span>Research</a>
                        </div>
                    </li>
                

                    <li>
                        <div>
                        <a href="/blog.aspx" id="Header33363" title="Blog"><span></span>Blog</a>
                        </div>
                    </li>
                

                    <li>
                        <div>
                        <a href="/learning.aspx" id="Header20" title="Learning"><span></span>Learning</a>
                        </div>
                    </li>
                

                    <li>
                        <div>
                        <a href="/shop.aspx" id="Header25" title="Shop"><span></span>Shop</a>
                        </div>
                    </li>
                

                    <li>
                        <div>
                        <a href="/about_us.aspx" id="Header22" title="About us"><span></span>About us</a>
                        </div>
                    </li>
                

                    </ul>
                
        </div>
        
        <!-- Site search -->
        <form action='' id='NullForm'><div class='immediacyFix'> </div></form>
        <form action='search_results.aspx' method='get' id='generalSearch'>
        <div id="headerSearch">
            <div class="search">
                <label for="siteWideSearch" title="Search this site"><span>Search:</span></label>
                <input id="siteWideSearch" class="txtBoxSmall marginRightSmall" type="text" name="searchText" />
                <button type="submit" class="headerImageButton" title="Search the website" value="Go"><span>Search the website</span></button>
                <div class="searchOptions">
                    <a class="searchOptionsToggle" href="#">More search options</a>
                    <ul>
                        <li><a href="search_results.aspx" class="selected">Search the website<span></span></a></li>
                        <li><a href="research/search_the_collection_database/search_results.aspx">Search the collection<span></span></a></li>
                        <li><a href="http://britishmuseumshoponline.org/page/search">Search the shop<span></span></a></li>
                    </ul>	
                </div>
            </div>
        </div>
        </form>
        
    </div>
<!-- div#headerBar - close -->
</div>

		
		
		
		
            <div id="subMenuWrapper">
                <div id="primaryMenu">
                    <div class="navigation horizontal section hideFirstChild">
                        
                                <ul>
                            
                                <li class="deepLink"><a href="/default.aspx" title="Home">Home</a></li>
                            

                                <li class="deepLink"><a href="/about_this_site.aspx" title="About this site">About this site</a></li>
                            

                                <li class="deepLink"><a href="/about_this_site/terms_of_use.aspx" title="Terms of use">Terms of use</a></li>
                            

                                <li class="active"><a href="/about_this_site/terms_of_use/privacy_policy.aspx" title="Privacy policy">Privacy policy</a></li>
                            

                                
                                </ul>
                            
                    </div>
                </div>
            </div>
		
		
		
	
			
			<div class="container">
	
				<div class="column span-7">
					
					<div id="leftNav">
						

<h2 class="Aboutthissite"><a href="/about_this_site.aspx" alt="Go to the About this site landing page."><span></span>About this site</a></h2>

        <ul class="cornerBottomSmall">
    
        <li>
            <span><a href="/about_this_site/about_this_site.aspx" title="About this site">About this site</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/contact.aspx" title="Contact">Contact</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/site_map.aspx" title="Site map">Site map</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use.aspx" title="Terms of use">Terms of use</a></span>
            
    

        <ul>
    
        <li class="active">
            <span><a href="/about_this_site/terms_of_use/privacy_policy.aspx" title="Privacy policy">Privacy policy</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/copyright_and_permissions.aspx" title="Copyright and permissions">Copyright and permissions</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/terms_of_use_for_membership.aspx" title="Terms of use for Membership">Terms of use for Membership</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/terms_of_use_for_tickets.aspx" title="Terms of use for tickets">Terms of use for tickets</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/terms_of_use_for_shop_online.aspx" title="Terms of use for shop online">Terms of use for shop online</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/terms_of_use_for_filming.aspx" title="Terms of use for filming">Terms of use for filming</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/data_protection_statement.aspx" title="Data protection statement">Data protection statement</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/cookies.aspx" title="Cookies">Cookies</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/school_bookings.aspx" title="School bookings">School bookings</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/terms_of_use_for_museumcraft.aspx" title="Terms of use for Museumcraft">Terms of use for Museumcraft</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/terms_of_use/social_media_code_of_conduct.aspx" title="Social media code of conduct">Social media code of conduct</a></span>
            </li>
    

                    </ul>
                </li>
    
        <li>
            <span><a href="/about_this_site/faqs.aspx" title="FAQs">FAQs</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/registered_addresses.aspx" title="Registered addresses">Registered addresses</a></span>
            </li>
    

        <li>
            <span><a href="/about_this_site/using_the_british_museum_logo.aspx" title="Using the British Museum logo">Using the British Museum logo</a></span>
            </li>
    

        </ul>
    

					</div> 
					
					
					
					
					
				</div>
	
				<div class="column span-24 last" id="mainContent">
					
					
<h1>Privacy (part of Terms of use)</h1>
<p>5.1&nbsp;We are committed to protecting the privacy and the
confidentiality of the personal information of visitors to our
Website. We undertake to ensure that all personal information in
our possession is processed in accordance with the principles of
the Data Protection Act 1998. Our Data Protection Policies may be
inspected in the <a title="Inspect our data protection policies in the governance section" href="/about_us/management/museum_governance.aspx"><strong>governance section</strong></a>.</p>
<p>5.2&nbsp;We collect personal information (such as your name,
contact details, credit card information) that you supply to us.
Your information is collected when you request information from us,
contact us, make a booking with us, purchase items from us,
including Membership and etickets,&nbsp;or make&nbsp;donation to
us. We will update your information whenever we get the opportunity
to keep it current, accurate and complete.</p>
<p>5.3&nbsp;Any information you provide when registering with us,
buying Membership, etickets&nbsp;and other products will be used
for British Museum, British Museum Friends and British Museum
Company purposes only. From time to time we may send you
information from other organisations which we think may be of
interest to you. We may also disclose your information to companies
who act as "data processors" on our behalf, some of whom may be
outside the UK/EEA.</p>
<p>5.4&nbsp;You may indicate your preference for receiving direct
marketing by telephone or e-mail from us or any partner
organisations with whom we may work. You will be given the
opportunity on every communication we send you to indicate that you
no longer wish to receive our direct marketing material. You may,
in addition, indicate your preference regarding receiving third
party direct marketing material. Once properly notified by you, we
will take steps to stop using your information in this way.</p>
<p>5.5 The credit card information you give for any online
transaction is used solely for the purpose of processing that
transaction.</p>
<p>5.6&nbsp;You have the right to ask in writing for a copy of the
information we hold about you (for which we may charge a fee) and
to correct any inaccuracies in your information.</p>
<p><a class="document pdf" href="/pdf/Data%20Protection%20Code%20of%20Practice.pdf"><span class="icon">&nbsp;</span>Data Protection Code of Practice</a></p>
					
				</div>
	
			</div>
			<!-- end div.container -->
	<!--</form>-->
	        

<!-- div.footer - open -->
        <div id="footer">
            <div id="footer-inner" class="clearfix"> 
            
                <div class="grid_8 alpha">
                
                    <div class="enewsletter">
                        <h3 class="type14px plain margin0pxAll">Enewsletter sign up</h3>


                        <form action="/about_this_site/terms_of_use//BritishMuseum/" method="post" id="enewsNullForm-footer"></form>
                        <form method="post"  class="margin16pxB grid_8 alpha omega" id="signupform-footer" action="http://www.pages04.net/thebritishmuseumcompany/Welcome/Master_OptIn"  >
							<input type="text" name="Email" id="email" value="Enter email" />
							<input type="submit" class="button" value="Sign up" />
							<input type="hidden" name="optin_giftshop" id="control_COLUMN106" value="Yes" />
							<input type="hidden" name="optin_whatson" id="control_COLUMN105" value="Yes" />
							<input type="hidden" name="source" id="control_COLUMN46" value="3" />
							<input type="hidden" name="formSourceName" value="StandardForm" />
						</form>
                    </div>    
                    <div class="shareHomepage clear">           
                    <h3 class="type14px plain margin0pxAll pad12pxT">Follow the British Museum</h3>
                    <ul class="followLinks">
                        <li class="facebook"><a href="http://www.facebook.com/britishmuseum" title="Facebook"><span>Facebook</span></a></li>
                        <li class="twitter"><a href="http://twitter.com/britishmuseum" title="Twitter"><span>Twitter</span></a></li>
                        <li class="youtube"><a href="http://www.youtube.com/user/britishmuseum" title="YouTube"><span>YouTube</span></a></li>
                        <li class="flickr"><a href="http://www.flickr.com/photos/britishmuseum" title="Flickr"><span>Flickr</span></a></li>
                        <li class="blog"><a href="http://blog.britishmuseum.org/feed" title="Blog"><span>Blog</span></a></li>
                        
                    </ul>
                    <div class="twitterFollow">
                    
                    
                    </div>
                    

                    </div>
                    
                </div>
                <div class="grid_4 omega">
                    
                        
                                <ul class="footerLinks">
                                
                            
                                <li><a href="/about_this_site/footer_components/contact_us.aspx" title="Contact us">Contact us</a></li>
                            

                                <li><a href="/about_this_site/footer_components/site_map.aspx" title="Site map">Site map</a></li>
                            

                                <li><a href="/about_this_site/footer_components/terms_of_use.aspx" title="Terms of use">Terms of use</a></li>
                            

                                <li><a href="/about_this_site/footer_components/cookies.aspx" title="Cookies">Cookies</a></li>
                            

                                <li><a href="/about_this_site/footer_components/faqs.aspx" title="FAQs">FAQs</a></li>
                            

                                    <li class="externalSites language"><a href="http://www.lvyou168.cn/travel/uk/Britishmuseum/index.html" title="Go to the Chinese version of the British Museum website">Chinese site <span lang="ch">&#20013;&#25991;</span></a></li>
                                    <li class="language"><a href="http://arabic.britishmuseum.org/" title="Go to the Arabic version of the British Museum website">Arabic site <span lang="ar">&#1575;&#1604;&#1606;&#1587;&#1582;&#1577; &#1575;&#1604;&#1593;&#1585;&#1576;&#1610;&#1577;</span></a></li> 
                                    <li><a href="http://www.finds.org.uk/" title="Portable Antiquities Scheme">Portable Antiquities Scheme</a></li>
                                </ul>
                            
                    
                    
                    <div class="grid_4 alpha margin32pxT pad36pxT">
                        <span id="copyright">&copy; Trustees of the British Museum</span>
                    </div>
                </div>
              
            </div>
        </div>
        <!-- div.footer - close -->
			
		</div>
		<!-- end div#wrapper -->
		

    

<!-- Google Analytics moved to HEAD -->

<!-- AKA marketing analytics -->

<script type="text/javascript">
var versaTag = {};
    versaTag.id = "258";
    versaTag.sync = 0;
    versaTag.dispType = "js";
    versaTag.ptcl = "HTTPS";
    versaTag.bsUrl = "bs.serving-sys.com/BurstingPipe";
    versaTag.activityParams = {"OrderID":"","Session":"","Value":"","productid":"","productinfo":"","tp_Quantity":""};
    versaTag.retargetParams = {}; //Static retargeting tags parameters.
    versaTag.dynamicRetargetParams = {}; //Dynamic retargeting tags parameters.
    versaTag.conditionalParams = {}; //Third party tags conditional parameters.    
</script>

<script id="ebOneTagUrlId" src="https://secure-ds.serving-sys.com/SemiCachedScripts/ebOneTag.js" type="text/javascript"></script>

<noscript>
<iframe src="https://bs.serving-sys.com/BurstingPipe?
cn=ot&amp;
onetagid=258&amp;
ns=1&amp;
activityValues=$$
Value=[Value]0&amp;
OrderID=[OrderID]0&amp;
ProductID=[ProductID]&amp;
ProductInfo=[ProductInfo]&amp;
tp_Quantity=[Quantity]&amp;$$&amp;
retargetingValues=$$&amp;
dynamicRetargetingValues=$$&amp;
acp=$$$$&amp;"
style="display:none;width:0px;height:0px"></iframe>
</noscript>


<script type="text/javascript" src="/javascripts/v2/core/adobeAnalytics.js"></script>


</body>
</html>
