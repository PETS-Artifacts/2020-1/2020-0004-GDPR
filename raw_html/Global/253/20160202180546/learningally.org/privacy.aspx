<!DOCTYPE html>
<html  lang="en-US">
<head id="Head"><meta http-equiv="X-UA-Compatible" content="IE=edge" /><title>
	Learning Ally - Together It's Possible > Privacy
</title><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" /><meta content="text/javascript" http-equiv="Content-Script-Type" /><meta content="text/css" http-equiv="Content-Style-Type" /><meta id="MetaDescription" name="DESCRIPTION" content="Learning Ally - Together It&#39;s Possible" /><meta id="MetaKeywords" name="KEYWORDS" content="Learning Ally, books, audiobooks, dyslexia" /><meta id="MetaCopyright" name="COPYRIGHT" content="Copyright 2016 by Learning Ally" /><meta id="MetaAuthor" name="AUTHOR" content="Learning Ally - Together It&#39;s Possible" /><meta name="RESOURCE-TYPE" content="DOCUMENT" /><meta name="DISTRIBUTION" content="GLOBAL" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><meta name="REVISIT-AFTER" content="1 DAYS" /><meta name="RATING" content="GENERAL" /><meta http-equiv="PAGE-ENTER" content="RevealTrans(Duration=0,Transition=1)" /><style id="StylePlaceholder" type="text/css"></style><link href="/portals/_default/default.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/desktopmodules/html/module.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/bootstrap3.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/learningAlly.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/bootstrap-accessibility.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/theme.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/parents.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/buyflow.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/nav.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/kendo.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/custom.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/font-awesome.css?cdv=832" type="text/css" rel="stylesheet"/><link href="/portals/6/portal.css?cdv=832" type="text/css" rel="stylesheet"/><script src="/resources/libraries/jquery/01_09_01/jquery.js?cdv=832" type="text/javascript"></script><script src="/resources/libraries/jquery-migrate/01_02_01/jquery-migrate.js?cdv=832" type="text/javascript"></script><script src="/resources/libraries/jquery-ui/01_10_03/jquery-ui.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/knockout.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/knockout.validation.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/bootstrap.min.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/bootstrap-accessibility.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/verimail.jquery.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/ui.multiselect.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/commonUI.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/kendo.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/parents.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/buyflow.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/fixLogoSize.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/common.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/kendoInitializers.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/jquery.blockUI.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/floating-footer.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/topfixedmenu.js?cdv=832" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/mainmenu.js?cdv=832" type="text/javascript"></script>

          <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-22081078-53']);
            _gaq.push(['_setDomainName', '.learningally.org']);
			_gaq.push(['_setAllowLinker', true]);
            

            _gaq.push(['_trackPageview']);

            (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
			
function recordOutboundLink(link, category, action, label) {
  //_gat._getTrackerByName()._trackEvent(category, action, label);
  _gaq.push(['_trackEvent', category, action, label]); 
  setTimeout('document.location = "' + link.href + '"', 100);
}
          </script>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" /></head>
<body id="Body">
    
    <form method="post" action="/privacy.aspx" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="a5EuWsLVGAvWOfEHd/T2ofDU8zTWBkPl3nbsMbTOpeDvGTYQA3qYW4vrZ6jHkYq5gmdCWb7VNqgkJy45+GnotUOr+gdxNhSN9AC2kpfm+bDLFgIDXkpFHW4Q8PpUwRDffNnOcTaTaZT55EMI1CNfpjUtHv0wVtrrCPuSMTpxOyzLfeBNooI0dv93T81GA5F6uvtZbFBjhKH8EIKu5i6qZj3hd+1GxqQBiaFvvXF/0e8ZCJYkmy5SqQ/sZiay7maa3WO+Lsmtd9avXujwRHxFdnTGcPtCtL6ocH2oW6gX2dhfipvSyj9w/2QE5srQ23uL+UdyeO6Zxtgdgg8lsGCpWdD5/2BKfhrjZKlRF2KJ+Cll6aIBAwWEYR5+TVnzBBiNqsv43sOaDujFwonFcbX+C8qyjQZK2j1pZGwnaWFeF3qYTNnAFgbphh0eV++BYg1OnIxEa6l7aXc9Nx3sx31jp3sy+tZTWJB1VlTvrHn2SsFkmkn7lrJHpr+FnsJ25QizB3COa/Z4F7ylV62T3uelUkdhpJ9P2yirHGv0IdLF+6JkOWb66au1JotudCIWkP6Q7Y+AaYfGdle0ESKNc3nFBzKe8X2/71MCPaACI3mEW3uKYB3QSwfrN3KogqFz7ePNzOq+mvaqpHKRLVYo0WWPRwYJwLqEhiWulA2OeFSKHSkEAw8aDhurSqgJ9lks88gS8upAzYksxyaPo/vjVTBpbi1v3JRyvsvvKwbCqCSB/lh0Ye8TF/yxhIUI39CfXsPHFjo1IlBcAtJA1DmCtJAZA1yh0oZqq1Ezpha2Y+CRgHL+NoyxihVhHqroS0avfVF0zw74T8MtSb74N86Obdi1bD9m3CnG2cZxCgW8oimGs48y60mr6pcsEILz7KS7Cao3uwkPYKv8zTwUjVscJUl5wV9MwapBk0VkUbfzdpUXyUehjLiJ7xUt49i/VB0HCACwtfGLhFT3X+OTzzYqbcvxxjNjGCr7ULLkP9akBOkBD8W4PDrKkSs53dBMr2dASDxKboNWaeI4hLmDVBvEGYW5qT7SJ2k972RhWfeXkQ9URqQ2Da0tL9PpAJTQRt+Wh3DgK7hI2IGqkQwSZXXtlgg/1agXT+TsXV3dg5BxXRuvuLOrm/kt635beaBf88k9uO2UJ4GYp4QnT8ljvju/K9y4S0LTvajAuaaRYx3lKXbz8jMKNox4LMhx2DMmINE03f51AAbc75BDA+BnBhJ84JFd1oHrBiWlawPI+KLrr7+5OHdyHHPJQyS0e2CPm7xFFyIw8nmd2KDmcOHsWuR5eCmisb7+JqPB32mX1fbyqsQlisJXoqR6Mk+ExQH5vof1Lh5fMgGieik/9lGGlyd/SnaiHwDHlRP3dX4TTjwpJT4zTIZtH1wJsUX+/R0BmLZkWO76Orhrih4t2uyaBmiZYpR/4GVQd9WTbTnoU7fU9lfJ407ZdUJ1ZJAkDhZDbk0L3uaoSolo4LDbxg9U3pzCIL07Dd2qKNjfcrwjGa7iwVzAivvCILQGCRoI+Ye+AtANt9/zeV8OKNh3cMzYFII0aHQZ4E/ROBqohteySnLdXYPDTFTnCWAsLNqn+gc9RvZlWS6lXymq07YQruJUjK6Aitz637OuhE08B627Bq8JS8lSq3WEELidS9NVpWQ3LDTcWy60OlwkHLsoFExCmeM2u1ejjjO2TPcnXv4z0/6ItUm8/VGFJOuUfZOgADINdHYzMssDxwOwuWo/+bUxC1oT1Huxj6Js9GByiqN8g5UN1RhWRSp0AjXtA/uFN42jOd/j0wFZEC0WxVAHP1g7T7dDbxszwFKKvvJX9XP1NSI4k8/NHDL8KmLYijPkeiIbyu/XOHHA8VqawZ52qvC94+HdaUVH7oysnA99D82QMz2ct0Qpoub2XWqMPkkcTuGD70vtcOgiMzUhB3w+e0izIwJ7a0s/lYB6azSAUVn35lMkge2NCsOfP/Mx4sugb/+/LhI8PzEGB/TbN2Ts6puMIA799jn0nn2sB4Q6tQsTZDUUoMxehUbs7RkBoGS1XqtTIumkDLHyC/IdCQ+/JzWp1eZ7dv0wpMMom9XShSjRB0EEoOwOv0ZtzTBNY+xocS/AsmN+kYVqNLIghn6MjB+J28dd41hucNwfM2rkp7bxXjO8IRMWCH5xKGHOoPWT+rNgU48PD6N3f6uinxvzj7RMUBAFOPbc9cLMsKWXhQqWbDcE7g5f438htsq2pYlUV3kXsbgZoeDGs+dJsO4WF5PahXfpd8QlYJv0S4QINoVAA/qgSoebfVKvTjHLRBfn/ubDCHvLzmRQpnSNL3Wl+AfK3LPpWAjr8BeixcEzlkj2JcHzfsurTSS3sRZrn4qV1ODUOWx2No4UTIN/Joiqcm9463fEZee5vNSOu4uuqf5+al7i6/nFNShuRljtqkmmoxh0GBw5ry8mbjuSwu2G70j6RiPMpcR/24Zt/JPZxQc1CQw+cGFO8fO/QeUDb40Lpu+Nv2y0Su/G9jvCbAYoh/yXXyaRL1kwWyip6TypfdjvcWRNOZNNT57EIpMjOlI2horcp9JUgSfOKm/xGYGnkjaEDtPDBRfeqMG3BsEDaXtjumlI3GePEuwhKtOC180Jrf9lpJFVYGFw4ZdLG9ghG0DGL8B+3jt2DAeJW1yL3o+mSPxu1Y7R24dJJAv4l1mySTlVtYz5PvfRHSgxgFVk2nAjSKP5Ta5xPq+vsI6ej7V4AjpEmBU5nGbOzqHs24+4KOL113m4kPnxWN1x96K9ivatAcelS6nJkF7HR6rw84k1Nigr/22D+CI67r+Bgfx9mpwBuh5mLGmtrjQQIDGjvuxvvutaVVAzqSTfh5e6a8olEzXCZ+w3MDfOJCjT5SSn8E2Hk0wlKOR/MiJdatyYHXVTjG1m61kTKo+dgmlGwuD0GRK3PmmzEiQOkp1DSJ5/3aEv3fRc3m/c2QtaKzBynJFSq7V82BtyfsHVgDICHveaGvMkuVHs8g6/miKPpvrx9MvP9Ph9lmgiq1RG+by62xkzwLpE8qBp2CkxuXu5cDCJqqmtXMFRMfN1DZPo+w4i4d5p9yuNOSqPF5asYFhRgIqq0gzyDLjaa7SjltXNgoATovM2TYSRKS1kKACzOO6lMz/hSw4YBqFwY5dCpbRxkRznYF/tRLgVM3ob+zZYzpRtcC6yGt3g6Yb55Y/H/IhLFPE/ffpkY24mylCLwiCN7orE9hFoX6xdlxHkj7ILWdJRWOUiz6uI1uKuzg73" />
</div>


<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3ad28568d3-e53e-4706-928f-3765912b66ca%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="RGNZiFmV8DCcVl03bk3WPB+B6uf1uM6LiQUZkTfjAwaNqetRDWAUSyEh9cYMCb5mqOEYz8kYwlJYdhllky9lObj7v41FZZ/yE/XGHjY7L5RFI1GPnUmyqYCvdpzzSUGBkr59JplO9q6XzR0p" />
</div><script src="/js/dnn.js?cdv=832" type="text/javascript"></script><script src="/js/dnn.modalpopup.js?cdv=832" type="text/javascript"></script><script src="/js/dnn.servicesframework.js?cdv=832" type="text/javascript"></script><script src="/js/dnncore.js?cdv=832" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        <!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/learningAlly.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/bootstrap3.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/bootstrap-accessibility.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/theme.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/parents.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/buyflow.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/nav.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/kendo.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/custom.css?cdv=832)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/font-awesome.css?cdv=832)-->

<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/knockout.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/knockout.validation.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/bootstrap.min.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/bootstrap-accessibility.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/verimail.jquery.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/ui.multiselect.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/commonUI.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/kendo.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/parents.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/buyflow.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/common.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/fixLogoSize.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/kendoInitializers.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/jquery.blockUI.js?cdv=832)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/floating-footer.js?cdv=832)-->



<div class="site-wrapper dashboard-home">
    
<nav class="audience-nav navbar navbar-default" role="navigation" id="top-fixed-menu">
    <div class="container-fluid">
    <div class="container">
        <span class="sr-only">Audience Navigation</span>
            <ul class='list-unstyled list-inline '><li class='haschild'><a href='http://www.learningally.org/AboutUs.aspx'><span>About Us</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Parents/ParentHome.aspx'><span>Parents</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Educators/EducatorHome.aspx'><span>Educators</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Adults.aspx'><span>Adults</span></a></li>
<li><a href='http://www.learningally.org/BrowseAudiobooks.aspx'><span>Browse Audiobooks</span></a></li>
<li><a href='https://supportus.learningally.org/checkout/donation?eid=55156'><span>Donate</span></a></li>
<li><a href='http://www.learningally.org/AboutUs/Join.aspx'><span>Join</span></a></li>
</ul>




    </div>
    </div>
</nav>
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/topfixedmenu.js?cdv=832)-->

	<!-- Floating Footer Promotion for year end giving
    <div class="promo--footer opaque">
        <div class="row p-tb-8 promo--footer__container"> 
         <a href="#close" class="pull-right promo--footer__close">X</a>
        <div class="col-xs-12 col-sm-7 col-sm-offset-1 col-md-7 col-md-offset-1 col-lg-6 col-lg-offset-2"> 
            <h2 class="promo--donation__h1 m-t-8">Give the Gift of Reading</h2> 
            <p>A foundation of support. A lifetime of confident learning.</p> 
        </div> 
        <div class="col-xs-12 col-sm-3 col-lg-3"> 
            <div class="promo--footer__cta"> 
            <a href="https://supportus.learningally.org/checkout/donation?eid=55156/" class="btn btn-default btn--footer-cta m-t-15"  onclick="recordOutboundLink(this, 'donate', 'click', 'donate-promo')" >Give Now</a> 
            </div> 
        	</div> 
        </div> 
    </div> -->
    <div class="container">
        <div class="row header">
            <div class="nav-responsive">
                <div class="col-xs-6 col-sm-4 col-md-4 nav-logo">

                    <a id="dnn_dnnLOGO_hypLogo" title="Learning Ally - Together It&#39;s Possible" href="http://www.learningally.org/Home.aspx"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/6/Logo.png" alt="Learning Ally - Together It&#39;s Possible" /></a>
                </div><!-- /col -->

                <div class="col-xs-6 col-sm-8 col-md-8 pull-right nav-btns">
                    <div class="row">
                        <div class="col-sm-12">
       	                
<div class="navbar-header pull-right">	
	<div class="navbar-btns visible-xs">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-search">
		<span class="sr-only">Toggle search</span>
		<i class="fa fa-search fa-lg" style="padding-right:0px"></i>
		</button>
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-menu">
		<span class="sr-only">Toggle navigation</span>
		<i class="fa fa-bars fa-lg" style="padding-right:0px"></i>
		</button>
	</div><!-- navbar-btns -->
	
    
			<div class="visible-xs navbar-login-xs">
				<a href="/login.aspx">Login</a>
			</div><!-- navbar-login-xs -->

			<div class="hidden-xs navbar-login">
				<a class="btn btn-primary btn-sm pull-right login" href="http://www.learningally.org/login.aspx">Login</a>
			</div><!-- navbar-login -->
                      									
</div>
				
       	                

<ul class="hidden-xs links-secondary list-unstyled list-inline pull-right" id="donate-menu">
   <li><a href="http://www.learningally.org/blog" title="Learninga Ally Blog" onclick="recordOutboundLink(this, 'wordpress', 'blog', 'Blog-TopMenu')">Blog</a></li>
    <li><a href="https://go.learningally.org/help-and-support" title="Help and Support"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'help-header')">Help</a></li>
    <li><a href="tel:+18002214792">(800) 221-4792</a></li>
    <li><a href="http://www.learningally.org/volunteer" title="Volunteer" onclick="recordOutboundLink(this, 'wordpress', 'volunteer', 'volunteer-header')">Volunteer</a></li>
    <li><a href="https://supportus.learningally.org/checkout/donation?eid=55156/" title="Donate"  target="_blank" onclick="recordOutboundLink('https://supportus.learningally.org/checkout/donation?eid=55156/', 'classy', 'donate', 'Donate-TopMenu')">Donate</a></li>
    <li><a class="btn btn-sm btn-cta" id ="btnSupport" href="http://www.learningally.org/SupportUs" title="Support Us">Support Us</a></li>

</ul>

                        </div><!-- /col -->
                    </div><!-- /row -->
       	         
                    
<script type="text/javascript">

    window.MainPortal = 'http://teacherally.learningally.org';
    window.MemberPortal = 'http://www.learningally.org';

    $(document).ready(function () {
        $("#search").keypress(function (e) {
            if (e.keyCode == 13 || e.which == 13) {
                $("#search-btn").click();
                e.preventDefault();
            }

        });

        $("#search-btn").click(function (e) {
            e.preventDefault();
            var notPlaceholder = ($("#search").data('placeholder') == null || $("#search").val() != $("#search").data('placeholder').placeholder);
            if ($("#search").val() != "" && notPlaceholder) {
                location.href = "./search.aspx?q=" + $("#search").val();
            } else {
                alert("Please enter at least one search term.");
            }
        });

        $("#search-audiobooks").keypress(function (e) {
            if (e.keyCode == 13 || e.which == 13) {
                $("#searchblock .btn").click();
                e.preventDefault();
            }

        });

        $("#searchblock .btn").click(function (e) {
            e.preventDefault();
            if ($("#search-audiobooks").val() != "") {
                location.href = "./search.aspx?q=" + $("#search-audiobooks").val();
            } else {
                alert("Please enter at least one search term.");
            }
        });
    });
    
</script>
<div class="row header-search">
    <div class="hidden-xs col-sm-10 col-md-9 pull-right">
    <div class="navbar-form navbar-right" role="search">
        <div class="input-group">
        <label class="sr-only" for="search">Search</label>
        <input type="search" placeholder="Search for Audiobooks, Help, Resources, etc." class="form-control" id="search" name="search" size="45">
        <span class="input-group-btn">
            <button class="btn btn-default search-btn" type="submit" value="Search" id="search-btn"><i class="fa fa-search"></i><span class="sr-only">Submit</span></button>
        </span>
        </div><!-- /input-group -->
    </div>
    </div><!-- /col -->
</div><!-- /row -->

              
                </div><!-- /col -->

                <div class="col-xs-12">
                
                    
<script type="text/javascript">
    
    $(document).ready(function () {
        $("#search-mobile").keypress(function (e) {
            if (e.keyCode == 13 || e.which == 13) {
                $("#search-btn-mobile").click();
                e.preventDefault();
            }

        });

        $("#search-btn-mobile").click(function (e) {
            e.preventDefault();
           // var notPlaceholder = ($("#search").data('placeholder') == null || $("#search").val() != $("#search").data('placeholder').placeholder);
            if ($("#search-mobile").val() != "" ) {
                location.href = "./search.aspx?q=" + $("#search-mobile").val();
            } else {
                alert("Please enter at least one search term.");
            }
        });

    });
    
</script>

<nav class="navbar navbar-inverse" role="navigation">
    

        <div class="collapse navbar-collapse" id="navbar-collapse-menu" style="height: auto;">
			<ul class='nav navbar-nav navbar-justified '><li class='haschild'><a href='http://www.learningally.org/AboutUs.aspx'><span>About Us</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Parents/ParentHome.aspx'><span>Parents</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Educators/EducatorHome.aspx'><span>Educators</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Adults.aspx'><span>Adults</span></a></li>
<li><a href='http://www.learningally.org/BrowseAudiobooks.aspx'><span>Browse Audiobooks</span></a></li>
<li><a href='https://supportus.learningally.org/checkout/donation?eid=55156'><span>Donate</span></a></li>
<li><a href='http://www.learningally.org/AboutUs/Join.aspx'><span>Join</span></a></li>
</ul>




           <ul class="nav__ul--donate visible-xs">
            <li class="nav__donate">
             <a href="http://www.learningally.org/SupportUs" class="open">Support Us</a>
            </li>
            </ul>
        </div>
    

      <div class="collapse navbar-collapse" id="navbar-collapse-search">
        <div class="navbar-form" role="search">
          <div class="input-group">
            <label class="sr-only" for="search-mobile">Search</label>
            <input type="search" placeholder="Search for Audiobooks, Help, Resources, etc." class="form-control" id="search-mobile" name="search">
            <span class="input-group-btn">
              <button class="btn btn-default search-btn" type="submit" value="Search" id="search-btn-mobile"><i class="fa fa-search"></i><span class="sr-only">Submit</span></button>
            </span>
          </div><!-- /input-group -->
        </div>
      </div><!-- /navbar-collapse-search -->
</nav>
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/mainmenu.js?cdv=832)-->

                </div><!-- /col -->
            </div><!-- /nav-responsive -->
        </div><!-- /header -->                        
        <!--Start main content area-->
        <div class="teacher-body">
            <span id="dnn_dnnBreadcrumb_lblBreadCrumb"><ol class="breadcrumb"><li><a href="http://www.learningally.org/Privacy.aspx" class="active">Privacy</a></li></ol></span>
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/breadcrumbs.js?cdv=832)-->
        	    <div id="dnn_LeftPane" class="leftpane DNNEmptyPane"></div>
                <div id="dnn_ContentPane" class="contentpane"><div class="DnnModule DnnModule-DNN_HTML DnnModule-2541"><a name="2541"></a>
<div class="NO-Title conwrap">
	<div class="con-aciton"></div>
	<div id="dnn_ctr2541_ContentPane"><!-- Start_Module_2541 --><div id="dnn_ctr2541_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	
<div id="dnn_ctr2541_HtmlModule_lblContent" class="Normal">
	<h1>Learning Ally Member Privacy Policy</h1>
<p>
<strong>Learning Ally, Inc. <br />
20 Roszel Rd<br />
Princeton, NJ 08540
</strong></p>
<p>
We can be reached via e-mail at <a class="ApplyClass" href="mailto:privacy@learningally.org">Privacy</a>.
</p>
<p>
Here at Learning Ally, we care deeply about privacy. We do not sell personally identifiable information (PII), nor do we use PII from education records to market to students.
</p>
<p>
Learning Ally Member Privacy Policy has been developed as an extension of our commitment to combine the finest quality products and services with the highest level of integrity in dealing with our Members. The Policy guides how we collect, store and use information about Members and Prospects, and it will be periodically assessed against new technologies, business practices and our Members' changing needs.
Learning Ally may, from time to time, modify this Member Privacy Policy to reflect legal, technological and other developments.
</p>
<p>
Please note that Privacy Notices advising Members and Prospects as to the specific uses that are being made of their Personally-Identifiable Information by specific Business Units within Learning Ally may also be modified from time to time to reflect changes in business practices. In this event, those changes will appear in the relevant Privacy Notices.
</p>
<p>
Learning Ally will also comply with applicable U.S. laws that impose additional responsibilities on Learning Ally beyond those stated in this Policy in connection with the treatment of Personally Identifiable Information about its Members and Prospects.
</p>
<p>
Our Policy recognizes two kinds of personal data that deserve varying levels of protection. Personally Identifiable Information includes, for example, e-mail addresses, billing information, employment status and data that tracks user activity on a Web site or online services. A subset of that category, Sensitive Data, deserves additional safeguards. Sensitive Data includes, by way of example, Social Security numbers, personal financial data (such as credit card information) and information about specific medical conditions.
</p>
<p>
</p>
<h2>Learning Ally is committed to using all reasonable efforts to abide by the following Policy:
</h2>
<p>
</p>
<h3>Collection.</h3>
<p> Personally-Identifiable Information will only be collected to the extent that Learning Ally deems reasonably necessary to serve a legitimate business purpose. Please be aware that if you disclose Personally-Identifiable Information on Web site message boards or chat areas, that information may be collected and used by third parties without our knowledge and may result in unsolicited communications from third parties. Such activities are beyond the control of Learning Ally.
</p>
<h3>Notification:</h3>
<p> Members and Prospects will be notified (usually at the point where Personally-Identifiable Information is collected) as to the uses the Business Unit may make of the Personally-Identifiable Information collected from them.
</p>
<p>
</p>
<h3>Security:</h3>
<p> Appropriate safeguards will be implemented in an effort to ensure the security, integrity and privacy of Personally-Identifiable Information about our Members and Prospects.
</p>
<p>
</p>
<h3>Access:</h3>
<p> Review and Correction. Procedures have been developed through which Members and Prospects can, upon request, review and correct the Personally-Identifiable Information that has been collected from them by Learning Ally. These procedures may not, however, compromise the security, integrity and privacy of either Learning Ally Education own proprietary databases or databases licensed from third party companies. Sensitive Data. The collection and use of Sensitive Data carries with it special obligations and responsibilities in order to maintain the data's security, integrity and privacy.
</p>
<p>
Sensitive Data will not be rented or otherwise made available for External Distribution outside Learning. Please note that our Web sites may contain links to other Web Sites that have their own privacy policies and practices. While Learning Ally will employ commercially appropriate procedures to help ensure that your information is only used for authorized purposes as described above, Learning Ally cannot make any guarantees with respect to the actions or policies of such third parties.
</p>
<p>
</p>
<h3>Children:</h3>
<p> Collection and use of information from children in the U.S. under the age of thirteen will be made in compliance with the Children's Online Privacy Protection Act of 1998. Specifically, Learning Ally Education will:
</p>
<p>
Provide a parent or guardian, upon request, with the means to review the Personally-Identifiable Information collected from his or her child;
</p>
<p>
Provide a parent or guardian with the opportunity to prevent the further use of Personally-Identifiable Information that has already been collected, or the future collection of Personally-Identifiable Information, from that child;
</p>
<p>
Limit collection of Personally-Identifiable Information for a child's online participation in a game, prize offer or other activities to Personally-Identifiable Information that is reasonably necessary to participate in the activity; and Establish procedures to protect the confidentiality, security and integrity of the Personally-Identifiable Information collected from children.
</p>
<h3>Privacy Policy&nbsp;</h3>
<p>
The Learning Ally Member Privacy Policy has been developed out of respect for the privacy preferences and choices of our Members and Prospects. We have established procedures to ensure that every reasonable effort is made to address your concerns. Each Business Unit within Learning Ally has a designated manager to implement our Member Privacy Policy for its respective Members and Prospects.</p>
</div>






</div><!-- End_Module_2541 --></div>
	<div class="conft">
		<div></div>
	</div>
</div>



</div></div>
                <div id="dnn_RightPane" class="rightpane DNNEmptyPane"></div>
            <div class="row">
				<div id="dnn_BottomLeftPane" class="bottomleftpane DNNEmptyPane"></div>
				<div id="dnn_BottomRightPane" class="bottomrightpane DNNEmptyPane"></div>
			</div>
        </div><!--End teacher-body-->
    </div> 
    
    <!--End container-->
</div> <!--End site-wrapper-->
       	                

<script>(function (w, d, t, r, u) {
var f, n, i; w[u] = w[u] || [], f = function () {
var o =
{ ti: "4077640" }
    ; o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
}, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function ()
{ var s = this.readyState; s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null) }
, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
})(window, document, "script", "//bat.bing.com/bat.js", "uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4077640&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

<footer class="site-footer" role="contentinfo">
   <div class="container">
      <div class="row">
	    <div class="col-sm-4">		
		    <div class="row">
		      <div class="col-xs-6">
		        <h4>About Us</h4>        
		  		    <nav>
		  		      <ul class="list-unstyled">			
		   			    <li><a href="http://www.learningally.org/AboutUs/WhoWeAre.aspx" title="Who We Are">Who We Are&nbsp;»</a></li>			
		   			    <li><a href="http://www.learningally.org/AboutUs/WhatWeDo.aspx" title="What We Do">What We Do&nbsp;»</a></li>	
						<li><a href="http://www.learningally.org/AboutUs/successstories.aspx" title="Success Stories">Success Stories&nbsp;»</a></li>	
		   			    <li><a href="http://www.learningally.org/AboutUs/leadership" title="Leadership">Leadership&nbsp;»</a></li>			
		   			    <li><a href="https://naa.learningally.org" title="Scholarships"  onclick="recordOutboundLink(this, 'wordpress', 'NAA', 'NAA-footer)">Scholarships&nbsp;»</a></li>			
		   			    <li><a href="http://www.learningally.org/AboutUs/Careers.aspx" title="Careers">Careers&nbsp;»</a></li>
						<li><a href="http://www.learningally.org/AboutUs/PressRoom.aspx" title="Press">Press&nbsp;»</a></li>
		   	      </ul>
		   	    </nav> 
		       </div><!-- /col -->    
		       <div class="col-xs-6">
		   	    <h4>Get Involved</h4>       
		  		    <nav>
		  		      <ul class="list-unstyled">			
							    <li><a href="http://www.learningally.org/volunteer" title="Volunteer"  onclick="recordOutboundLink(this, 'wordpress', 'volunteer', 'volunteer-footer)">Volunteer&nbsp;»</a></li>
                             <li><a href="https://supportus.learningally.org/checkout/donation?eid=55156/" title="Donate"  onclick="recordOutboundLink(this, 'classy', 'donate', 'Donate-footer)">Donate&nbsp;»</a></li>
							    <li><a href="http://www.learningally.org/SupportUs" title="Support Us">Support Us&nbsp;»</a></li>
							    <li><a href="http://www.learningally.org/AboutUs/Partners.aspx" title="Partners">Partners&nbsp;»</a></li>
							    <li><a href="https://go.learningally.org/parents/learning-ally-approach/dyslexia-resources/find-dyslexia-specialist/" title="Specialists" target="_blank"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'find-a-specialist-footer)">Specialists&nbsp;»</a></li>
		           </ul>
		         </nav>             
		       </div><!-- /col -->    
		    </div><!-- /row -->
		
	    </div><!-- /col --> 
	    <div class="col-sm-4">		
		    <div class="row">		 
		       <div class="col-xs-6">
				    <h4>Stay in Touch</h4>        
				    <nav>
					    <ul class="list-unstyled">
                        <li><a href="http://www.learningally.org/AboutUs/StayinTouch.aspx" title="Stay in Touch">Stay in Touch&nbsp;»</a></li>
						    <li><a href="https://go.learningally.org/news-events/blog/" title="Blog"  onclick="recordOutboundLink(this, 'wordpress', 'blog', 'blog-footer')">Blog&nbsp;»</a></li>
		 				    <li><a href="https://go.learningally.org/contact-us/" title="Contact Us"   onclick="recordOutboundLink(this, 'wordpress', 'blog', 'ContactUs-footer')">Contact Us&nbsp;»</a></li>
						 </ul>
				      <ul class="list-unstyled social-links">			
		 				    <li class="clearfix"><i class="facebook"></i><a target="_blank" href="http://www.facebook.com/LearningAlly.org" onclick="recordOutboundLink(this, 'social', 'facebook', 'facebook-footer')">Facebook</a></li>
		       	    <li class="clearfix"><i class="twitter"></i><a target="_blank" href="http://twitter.com/Learning_Ally" onclick="recordOutboundLink(this, 'social', 'twitter', 'twitter-footer')">Twitter</a></li>
		       	    <li class="clearfix"><i class="linkedin"></i><a target="_blank" href="http://www.linkedin.com/groups?home=&amp;gid=2644842&amp;trk=anet_ug_hm" onclick="recordOutboundLink(this, 'social', 'linkedin', 'linked-in-footer')">LinkedIn</a></li>
		       	    <li class="clearfix"><i class="youtube"></i><a target="_blank" href="http://www.youtube.com/learningally" onclick="recordOutboundLink(this, 'social', 'youtube', 'youtube-footer')">YouTube</a></li>
		       	    <li class="clearfix"><i class="google"></i><a target="_blank" href="https://plus.google.com/108255844779026447464/posts"  onclick="recordOutboundLink(this, 'social', 'googleplus', 'google-plus-footer')">Google+</a></li>
                      <li class="clearfix"><i class="instagram"></i><a target="_blank" href="http://instagram.com/Learning_Ally"  onclick="recordOutboundLink(this, 'social', 'click', 'instagram-footer')">Instagram</a>
                      </li>
		            	    <li class="clearfix"><i class="pinterest"></i><a target="_blank" href="http://pinterest.com/LearningAlly/"  onclick="recordOutboundLink(this, 'social', 'click', 'pinterest-footer)">Pinterest</a></li>
		 	  	    </ul>
		 		    </nav>
			    </div><!-- /col -->    
		      <div class="col-xs-6">
		        <h4>Helpful Tools</h4>        
		 		    <nav>
		 		      <ul class="list-unstyled">			
						    <li><a href="https://go.learningally.org/parents/dyslexia-screener/" title="Dyslexia Quiz" onclick="recordOutboundLink(this, 'wordpress', 'quiz', 'DyslexiaQuiz-footer')">Dyslexia Quiz&nbsp;»</a></li>
						    <li><a href="https://go.learningally.org/parents/learning-ally-approach/dyslexia-resources/reading-tutor-network/" title="Find a Tutor"  onclick="recordOutboundLink(this, 'wordpress', 'tutors', 'find-a-tutor-footer')">Find a Tutor&nbsp;»</a></li>
						    <li><a href="https://go.learningally.org/parents/learning-ally-approach/dyslexia-resources/find-dyslexia-specialist/" title="Find a Specialist" onclick="recordOutboundLink(this, 'wordpress', 'specialists', 'find-a-specialist-footer')">Find a Specialist&nbsp;»</a></li>
		  				    <li><a href="https://go.learningally.org/questions/" title="Ask a Parent" onclick="recordOutboundLink(this, 'wordpress', 'parents', 'ask-a-parent-footer')">Ask a Parent&nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/BrowseAudiobooks.aspx" title="Browse Audiobooks">Browse Audiobooks&nbsp;»</a></li>
		  	      </ul>
		  	    </nav> 
			    </div><!-- /col -->
		    </div><!-- /row -->
		
	    </div><!-- /col --> 
	    <div class="col-sm-4">		
		    <div class="row">    		
			    <div class="col-xs-6">
		   	    <h4>Support</h4>        
		  		    <nav>
		  		      <ul class="list-unstyled">			
		   			    <li><a href="https://go.learningally.org/help-and-support/" title="Help"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'help-footer)">Help&nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/IndividualTOS.aspx" title="Terms of Service">Terms of Service&nbsp;»</a></li>
		   	      </ul>
		   	    </nav>
			    </div><!-- /col -->    
			    <div class="col-xs-6">        
		   	    <h4>Join Us</h4>        
		  	    <nav>
		  		    <ul class="list-unstyled">			
		   			    <li><a href="http://www.learningally.org/Parents/Join.aspx" title="Parent">Parent&nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/Adults/Join.aspx" title="Adult Learners">Adults &nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/Educators.aspx" title="Educator">Educators&nbsp;»</a></li>
		   	      </ul>
		   	    </nav>  
		 	    </div><!-- /col -->
			    <div class="col-xs-12 app-badges">
		  	    <nav>
		  		    <ul class="list-unstyled list-inline">			
		   			    <li><a href="https://itunes.apple.com/us/app/rfb-d-audio/id418888450" target="_blank"><img alt="Available on the App Store (iTunes)" class="img-responsive" src="/Portals/_default/Skins/LearningAllyBootstrap3/images/theme/badge-apple-app.png" /></a></li>
			          <li><a href="https://play.google.com/store/apps/details?id=org.learningally.learningallyaudioandroid&amp;hl=en" target="_blank"><img alt="Android App on Google Play" class="img-responsive" src="/Portals/_default/Skins/LearningAllyBootstrap3/images/theme/badge-android-app.png" /></a></li>
		   	      </ul>
		   	    </nav>  
		 	    </div><!-- /col -->
		    </div><!-- /row -->
	    </div><!-- /col -->		
    </div><!-- /row --> 

    <div class="row">
      <div class="col-xs-12 copyright">
           <p><span id="dnn_laFooter_dnnCOPYRIGHT_lblCopyright" class="copyright">Copyright 2016 by Learning Ally</span>
</p>
        <ul class="list-unstyled list-inline">			
  		    <li><a href="http://www.learningally.org/privacy.aspx" title="Privacy">Privacy&nbsp;</a></li>
  		    <li><a href="http://www.learningally.org/accessibility.aspx" title="Accessibility">Accessibility&nbsp;</a></li>
  		    <li><a href="https://go.learningally.org/la-sitemap/" title="Site Map"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'SiteMap-footer)">Site Map&nbsp;</a></li>
  		    <li><a href="https://supportus.learningally.org/checkout/donation?eid=55156/" title="Donate" onclick="recordOutboundLink(this, 'outbound', 'click', 'Donate-footer)">Donate&nbsp;</a></li>
  		    <li><a href="https://go.learningally.org/contact-us/" title="Contact Us"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'ContactUs-footer)">Contact Us&nbsp;</a></li>
  		    <li><a href="http://www.learningally.org/AboutUs/Join.aspx" title="Join">Join&nbsp;</a></li>
  	    </ul>
      </div>
    </div>
</div>
</footer>
<div id="freezerIcon" class="modal-backdrop fade in" style="opacity: 0.3; z-index: 8000; display: none;"> 
    <div style="position:absolute; left:50%; top:50%; margin-left:-33px; margin-top:-33px; width:66px; height:66px; ">
        <img width="66px" height="66px" src="/Portals/_default/Skins/LearningAllyBootstrap3/images/ajax-loader.gif" alt="Processing...Please Wait" />
    </div>
</div>	
        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" value="`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`2008`}" />
        <script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/breadcrumbs.js?cdv=832" type="text/javascript"></script>
    
<script type="text/javascript" src="/Resources/Shared/scripts/initWidgets.js" ></script></form>
    
    
</body>
</html>