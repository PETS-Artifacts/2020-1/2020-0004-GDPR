<!DOCTYPE html>
<html  lang="en-US">
<head id="Head"><meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta name="REVISIT-AFTER" content="1 DAYS" />
<meta name="RATING" content="GENERAL" />
<meta name="RESOURCE-TYPE" content="DOCUMENT" />
<meta content="text/javascript" http-equiv="Content-Script-Type" />
<meta content="text/css" http-equiv="Content-Style-Type" />
<title>
	Learning Ally - Together It's Possible > Privacy
</title><meta id="MetaDescription" name="DESCRIPTION" content="Learning Ally - Together It&#39;s Possible" /><meta id="MetaKeywords" name="KEYWORDS" content="Learning Ally, books, audiobooks, dyslexia" /><meta id="MetaRobots" name="ROBOTS" content="INDEX, FOLLOW" /><link href="/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/bootstrap.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/learningAlly.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/bootstrap-accessibility.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/theme.min.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/parents.min.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/buyflow.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/nav.min.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/custom.min.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/font-awesome.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/refresh.css?cdv=1082" type="text/css" rel="stylesheet"/><link href="/Portals/6/portal.css?cdv=1082" type="text/css" rel="stylesheet"/><script src="/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=1082" type="text/javascript"></script><script src="/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=1082" type="text/javascript"></script><script src="/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/knockout.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/knockout.validation.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/bootstrap.min.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/bootstrap-accessibility.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/verimail.jquery.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/ui.multiselect.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/commonUI.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/parents.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/buyflow.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/common.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/fixLogoSize.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/jquery.blockUI.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/floating-footer.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/topfixedmenu.js?cdv=1082" type="text/javascript"></script><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/mainmenu.js?cdv=1082" type="text/javascript"></script><script type='text/javascript'>
//<![CDATA[
    var analytics_page_startTime = (new Date()).getTime();
//]]>
</script>


          <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-22081078-53']);
            _gaq.push(['_setDomainName', '.learningally.org']);
			_gaq.push(['_setAllowLinker', true]);
            

            _gaq.push(['_trackPageview']);

            (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
			
function recordOutboundLink(link, category, action, label) {
  //_gat._getTrackerByName()._trackEvent(category, action, label);
  _gaq.push(['_trackEvent', category, action, label]); 
  setTimeout('document.location = "' + link.href + '"', 100);
}
          </script>

        <meta name="viewport" content="width=device-width, initial-scale=1.0" /></head>
<body id="Body">
    
    <form method="post" action="/Privacy" id="Form" enctype="multipart/form-data">
<div class="aspNetHidden">
<input type="hidden" name="StylesheetManager_TSSM" id="StylesheetManager_TSSM" value="" />
<input type="hidden" name="ScriptManager_TSM" id="ScriptManager_TSM" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="IuC9dGVObjQngLpC8b8B0Kv+wjgZkWRSkW/p31LuPh/ScWZ0zS0tOnnGv7cFA1ug/FviRc+yolvO7sVAGzDoA/SS1XHVKbWZ90Zp2Bl/AY7dPT+Pv47NR6h7KV5INajXcV/zZvVcrXA7jYMCNry0vs/y7VYcraWbe8VmIMArRNVMfPjq6FB/IxtCVJZpsHlAK0PA9ShGreJp2Wjwg554Ks1/ymvAddCgYue12S1Lh9Uhd6+V+GKsxyEVp4I5e7F4HGsI5oEJCP8E5HkW5KcvHHODSycoKIMxOxO4MShJJKVo4XV5yQSGSyyYy20lklL/pa3qO0335dah3UuG3YUfyHpuX6yvfyuYpThJvczc8nVtNEDDkA04461jMieAlkh8MJz5GZz5Wu9MjFmcuV2ONnf36sPCMZ6SNCYsSzs8CQtjFsB8ClQvkrCy23ju+XDKaVXj8/gh59+aBZ4P/d+lHLACPdbCQPwoKLxRu2d9xX/TSMdC6WRUhz0vH/FudMolnR4fUc20aZvnV1yhBlSn8CtuX/19dYCiVAadbscYDKZYSw50KJIxWFJr5T31SyhrYRI7ZvieWXPnYIhsQLsQxLk1rmou6QuBLJLoifboJ6oLDqD8spw7g54NAax7LRi57gPMO6mPE3DDJ5JYdHxTw6kKAty5es/ysaeFdSDf9+xNJChnb5Ei6+uyXkMF3JK+3cIfB1ox/ZhhGQnNNwwbKhPQ/mYWvVbL5zKrHUI6KP0OXewOvjDVRzV2fV24cgATLrjVGYwJ4WafWxeFcBCYw87rMLeNMU8vQzAwmafGQYSETpwFU11x7Ofbg6mi50XKGDdSQlJtUTU5jCWcAICUugdvQ4vG0UBqiWN+F0fY63cL/w6SxpvYHpyNpW8mcsP3sOerTRPOJyPFIXEkamOjre/MN+k0zzQtE6rFkDRpqiYGBwQ2dvB+REosEDw2gBn3Pg35KaV0QBoFX/JkqUkBIEy10QjkyzYX616KGNH0fksltnDqf7KkmUwogBuqPb7l+ThtK0iUoO9da6mctb9CrSWXw1LExQgKlV5rmnk0XT+LZXTb4o7Ai9uOYqZTvsRY/d7pvvLSNgut/BUEpQXWHdDXo0hSuQcQjm+TevUUKc2c44uSHFoG2nbOOu88y3R/WQ4/8vl213YVz8XffOqsy5MF4cgkjYwaWWxrUiEG9YZBqTeCOhIgcyPfbfX+TXGt1G3DgICZ+B6Ooqs/Skeu0WFTRfi5waF9c7xUyR+4eZM+ZBgi0RMUuVJW8qXERTSY0tC+H94Ly17d06F/Or5/g93V1uwgpsuWonMd7rERgPpfEWwlblr1u8wLsl0FsoHKbdLDUcM5X6MdvrhY9XGzi1CZmMt5tPOlHrWMsgNgdiiFfUHHhyS0qe+EEtPgX+dIKylemArmjg8bqbENAj2iquPOkjFkfYdXGY98rDTHjWS6g9v5ElChMBh8u6Z8puOjadY2ha+r70d4ZuRudLhNheJ/QRGVnjoAIGvqUjS7IDa3MQd7Ic4eQf67UvApt/ujjXn/uyZtfZrnJcEzSkszFTrA4wMDzCbimSKqPAGPEpkBGQfwttn7WH8fXtRt/PxqNEm5F5HDdCPTNHJZO0Kr0IhTbPtGb+gBybjioAtkES7A5ssAQeA7MyxkqyeEY55uF9vhM8sLnVET4SxRXuakrN3ixgfQ1LSFp4/dKoB+e7/9Owho+nv2uXQwmaG7SZ6o/+eg5E2EsRZNEXsb0slDDwsPM3DQaDJjM5m5qK3mpchyO2gIY/hor2izJmTorV9zEnyE/z+T2SRgGeAC6VT741ilLGVuoOe4VIxb2SUOlBW7aRxmDPOIX8IcqxCQUmZO+GJADu1OJjfbzm/OqicOrgaxQyBWPN+XC14kefGrifMFPPFScx2hUO//9cMp6H14XxdtgfGohhGEw+pFtgzTeOXGfWEvvYBJsOYPFMFY0nUGr1jpE/qu+DGhP+baeT9a4uXyxhrgws4lbPdhEkPJxNFUfYL19PPx2eMYzp+W1sLi0OwVPjub/3caFyerJs9HZswYZKF/UN2Lyr/cb6dcf6YgPGZSupSTDZpU+Jw8kbH6s5GLuYtKCqcREusGqCJimkhI2C/PhidXVHv3L/Yi7sSbI9LevVuTYeXGV60E13PBvSjniVXHXQRxGrbYDbKQ/T+pEKj7/KAJYQmWyKvydi/QZ/xQWkxah6XFFK2/ATJe5SKjUKTqD4KcndMoKZ6I5ZJW8h5nBapMfnE/HRXnb60KSv2kn+SNlTY9P6922wTZWf4HZGaR5DPahgrG2iNz6pjGMNrN3ubNp77DdsgjrTIQcfda5MpXSA6iPQCEoQXa3HYDEhDebUZqfS1FFcTQj9JwoTgKvkSO149lQRpuWvgR6/+mvh4fI86Mj23bCS8au8uubfLSLOKF2CfUw8GCQ2XRLr3aBniM5iCMFEQlMOb3mZUV2jqsdi5BsnoDOcMqN9uhzUV3sIGXXrc6FGIdwj7t4yhVjQbTKESCrJs67CUGGMv+TKjFYsG57NOQZ2tg5sffneIHijQ95sYrORzAiE2TGZ/HcGXPvdWBnx0SKrwOHio=" />
</div>


<script src="/Telerik.Web.UI.WebResource.axd?_TSM_HiddenField_=ScriptManager_TSM&amp;compress=1&amp;_TSM_CombinedScripts_=%3b%3bSystem.Web.Extensions%2c+Version%3d4.0.0.0%2c+Culture%3dneutral%2c+PublicKeyToken%3d31bf3856ad364e35%3aen-US%3ad28568d3-e53e-4706-928f-3765912b66ca%3aea597d4b%3ab25378d2" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="CA0B0334" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="lxMXJJPjdrF3c84N4qeYhtXr6DrRF9acUGlKPdOT/CxH6nwMhSNZIQ++o3dA3m+QlWOAERiXAnoU42nbkRhdQ+x0muBgiW6j3wS/jJWsr8/vUkH1" />
</div><script src="/js/dnn.js?cdv=1082" type="text/javascript"></script><script src="/js/dnn.modalpopup.js?cdv=1082" type="text/javascript"></script><script src="/js/dnncore.js?cdv=1082" type="text/javascript"></script><script src="/DesktopModules/DNNCorp/EvoqContentLibrary/ClientScripts/dnn.analytics.Injected.js?cdv=1082" type="text/javascript"></script><script src="/js/dnn.servicesframework.js?cdv=1082" type="text/javascript"></script><script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager', 'Form', [], [], [], 90, '');
//]]>
</script>

        
        
        <!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/learningAlly.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/bootstrap.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/bootstrap-accessibility.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/theme.min.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/parents.min.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/buyflow.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/nav.min.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/custom.min.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/font-awesome.css?cdv=1082)-->
<!--CDF(Css|/Portals/_default/Skins/LearningAllyBootstrap3/stylesheets/refresh.css?cdv=1082)-->

<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/knockout.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/knockout.validation.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/bootstrap.min.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/bootstrap-accessibility.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/verimail.jquery.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/ui.multiselect.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/commonUI.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/parents.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/buyflow.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/common.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/fixLogoSize.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/jquery.blockUI.js?cdv=1082)-->
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/floating-footer.js?cdv=1082)-->



<div class="site-wrapper dashboard-home">
    
<nav class="audience-nav navbar navbar-default" role="navigation" aria-label="Audience Navigation" id="top-fixed-menu">
    <div class="container-fluid">
    <div class="container">
        <span class="sr-only">Audience Navigation</span>
            <ul class='list-unstyled list-inline '><li class='haschild'><a href='http://www.learningally.org/About-Us'><span>About Us</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Parents/Parent-Home'><span>Parents</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Educators/Educator-Home'><span>Educators</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/College-Adults'><span>Adults</span></a></li>
<li><a href='http://www.learningally.org/Browse-Audiobooks'><span>Browse Audiobooks</span></a></li>
<li><a href='https://supportus.learningally.org/checkout/donation?eid=55156'><span>Donate</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Get-Involved'><span>Get Involved</span></a></li>
<li><a href='http://www.learningally.org/About-Us/Join'><span>Join</span></a></li>
</ul>




    </div>
    </div>
</nav>
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/topfixedmenu.js?cdv=1082)-->

	 <div class="container">
        <div class="row header">
            <div class="nav-responsive">
                <div class="col-xs-6 col-sm-4 col-md-4 nav-logo"  role="banner">

                    <a id="dnn_dnnLOGO_hypLogo" title="Learning Ally - Together It&#39;s Possible" href="http://www.learningally.org/"><img id="dnn_dnnLOGO_imgLogo" src="/Portals/6/Logo.png?ver=2014-12-14-154648-423" alt="Learning Ally - Together It&#39;s Possible" /></a>
                </div><!-- /col -->

                <div class="col-xs-6 col-sm-8 col-md-8 pull-right nav-btns">
                    <div class="row">
                        <div class="col-sm-12" role="navigation" aria-label="User tools menu">
       	                
<div class="navbar-header pull-right">	
	<div class="navbar-btns visible-xs">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-search">
		<span class="sr-only">Toggle search</span>
		<i class="fa fa-search fa-lg" style="padding-right:0px"></i>
		</button>
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-menu">
		<span class="sr-only">Toggle navigation</span>
		<i class="fa fa-bars fa-lg" style="padding-right:0px"></i>
		</button>
	</div><!-- navbar-btns -->
	
    
			<div class="visible-xs navbar-login-xs">
				<a href="/login.aspx">Login</a>
			</div><!-- navbar-login-xs -->

			<div class="hidden-xs navbar-login">
				<a class="btn btn-primary btn-sm pull-right login" href="http://www.learningally.org/login.aspx">Login</a>
			</div><!-- navbar-login -->
                      									
</div>
				
       	                

<ul class="hidden-xs links-secondary list-unstyled list-inline pull-right" id="donate-menu">
    <!--
    <li><a href="http://www.learningally.org/blog" title="Learninga Ally Blog" onclick="recordOutboundLink(this, 'wordpress', 'blog', 'Blog-TopMenu')">Blog</a></li>
    -->
    <li><a href="https://go.learningally.org/help-and-support" title="Help and Support"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'help-header')">Help</a></li>
    <li><a href="tel:+18002214792" title="phone: (800) 221-4792">(800) 221-4792</a></li>
    <li><a href="http://www.learningally.org/volunteer" title="Volunteer" onclick="recordOutboundLink(this, 'wordpress', 'volunteer', 'volunteer-header')">Volunteer</a></li>
    <li><a href="https://supportus.learningally.org/checkout/donation?eid=55156/" title="Donate"  onclick="recordOutboundLink(this, 'classy', 'donate', 'Donate-TopMenu')">Donate</a></li>
    <li><a class="btn btn-sm btn-cta" id ="btnSupport" href="http://www.learningally.org/SupportUs" title="Support Us">Support Us</a></li>

</ul>

                        </div><!-- /col -->
                    </div><!-- /row -->
       	         
                    
<script type="text/javascript">

    window.MainPortal = 'http://teacherally.learningally.org';
    window.MemberPortal = 'http://www.learningally.org';

    $(document).ready(function () {
        $("#search").keypress(function (e) {
            if (e.keyCode == 13 || e.which == 13) {
                $("#search-btn").click();
                e.preventDefault();
            }

        });

        $("#search-btn").click(function (e) {
            e.preventDefault();
            var notPlaceholder = ($("#search").data('placeholder') == null || $("#search").val() != $("#search").data('placeholder').placeholder);
            if ($("#search").val() != "" && notPlaceholder) {
                location.href = "/search.aspx?q=" + $("#search").val();
            } else {
                alert("Please enter at least one search term.");
            }
        });

        $("#search-audiobooks").keypress(function (e) {
            if (e.keyCode == 13 || e.which == 13) {
                $("#searchblock .btn").click();
                e.preventDefault();
            }

        });

        $("#searchblock .btn").click(function (e) {
            e.preventDefault();
            if ($("#search-audiobooks").val() != "") {
                location.href = "/search.aspx?q=" + $("#search-audiobooks").val();
            } else {
                alert("Please enter at least one search term.");
            }
        });
    });
    
</script>
<div class="row header-search">
    <div class="hidden-xs col-sm-10 col-md-9 pull-right">
    <div class="navbar-form navbar-right" role="search">
        <div class="input-group">
        <label class="sr-only" for="search">Search</label>
        <input type="search" placeholder="Search for Audiobooks, Help, Resources, etc." class="form-control" id="search" name="search" size="45">
        <span class="input-group-btn">
            <button class="btn btn-default search-btn" type="submit" value="Search" id="search-btn"><i class="fa fa-search"></i><span class="sr-only">Submit</span></button>
        </span>
        </div><!-- /input-group -->
    </div>
    </div><!-- /col -->
</div><!-- /row -->

              
                </div><!-- /col -->

                <div class="col-xs-12">
                
                    
<script type="text/javascript">
    
    $(document).ready(function () {
        $("#search-mobile").keypress(function (e) {
            if (e.keyCode == 13 || e.which == 13) {
                $("#search-btn-mobile").click();
                e.preventDefault();
            }

        });

        $("#search-btn-mobile").click(function (e) {
            e.preventDefault();
           // var notPlaceholder = ($("#search").data('placeholder') == null || $("#search").val() != $("#search").data('placeholder').placeholder);
            if ($("#search-mobile").val() != "" ) {
                location.href = "/search.aspx?q=" + $("#search-mobile").val();
            } else {
                alert("Please enter at least one search term.");
            }
        });

    });
    
</script>

<nav class="navbar navbar-inverse" role="navigation" aria-label="Main Menu">
    

        <div class="collapse navbar-collapse" id="navbar-collapse-menu" style="height: auto;">
			<ul class='nav navbar-nav navbar-justified '><li class='haschild'><a href='http://www.learningally.org/About-Us'><span>About Us</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Parents/Parent-Home'><span>Parents</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Educators/Educator-Home'><span>Educators</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/College-Adults'><span>Adults</span></a></li>
<li><a href='http://www.learningally.org/Browse-Audiobooks'><span>Browse Audiobooks</span></a></li>
<li><a href='https://supportus.learningally.org/checkout/donation?eid=55156'><span>Donate</span></a></li>
<li class='haschild'><a href='http://www.learningally.org/Get-Involved'><span>Get Involved</span></a></li>
<li><a href='http://www.learningally.org/About-Us/Join'><span>Join</span></a></li>
</ul>




           <ul class="nav__ul--donate visible-xs">
            <li class="nav__donate">
             <a href="http://www.learningally.org/SupportUs" class="open">Support Us</a>
            </li>
            </ul>
        </div>
    

      <div class="collapse navbar-collapse" id="navbar-collapse-search">
        <div class="navbar-form" role="search">
          <div class="input-group">
            <label class="sr-only" for="search-mobile">Search</label>
            <input type="search" placeholder="Search for Audiobooks, Help, Resources, etc." class="form-control" id="search-mobile" name="search">
            <span class="input-group-btn">
              <button class="btn btn-default search-btn" type="submit" value="Search" id="search-btn-mobile"><i class="fa fa-search"></i><span class="sr-only">Submit</span></button>
            </span>
          </div><!-- /input-group -->
        </div>
      </div><!-- /navbar-collapse-search -->
</nav>
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/mainmenu.js?cdv=1082)-->

                </div><!-- /col -->
            </div><!-- /nav-responsive -->
        </div><!-- /header -->                        
        <!--Start main content area-->
        <div class="teacher-body" role="main" >
            <span id="dnn_dnnBreadcrumb_lblBreadCrumb"><ol class="breadcrumb"><li><a href="http://www.learningally.org/Privacy" class="active">Privacy</a></li></ol></span>
<!--CDF(Javascript|/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/breadcrumbs.js?cdv=1082)-->
        	    <div id="dnn_LeftPane" class="leftpane DNNEmptyPane"></div>
                <div id="dnn_ContentPane" class="contentpane"><div class="DnnModule DnnModule-DNN_HTML DnnModule-2541 DnnVersionableControl"><a name="2541"></a>

<!-- <div id="container-block" class="SpacingBottom responsive"> -->           
    <div id="dnn_ctr2541_ContentPane"><!-- Start_Module_2541 --><div id="dnn_ctr2541_ModuleContent" class="DNNModuleContent ModDNNHTMLC">
	<div id="dnn_ctr2541_HtmlModule_lblContent"><h1>Learning Ally Member Privacy Policy</h1>
<p>
<strong>Learning Ally, Inc. <br />
20 Roszel Rd<br />
Princeton, NJ 08540
</strong></p>
<p>
We can be reached via e-mail at <a class="ApplyClass" href="mailto:privacy@learningally.org">Privacy</a>.
</p>
<p>
Here at Learning Ally, we care deeply about privacy. We do not sell personally identifiable information (PII), nor do we use PII from education records to market to students.
</p>
<p>
Learning Ally Member Privacy Policy has been developed as an extension of our commitment to combine the finest quality products and services with the highest level of integrity in dealing with our Members. The Policy guides how we collect, store and use information about Members and Prospects, and it will be periodically assessed against new technologies, business practices and our Members' changing needs.
Learning Ally may, from time to time, modify this Member Privacy Policy to reflect legal, technological and other developments.
</p>
<p>
Please note that Privacy Notices advising Members and Prospects as to the specific uses that are being made of their Personally-Identifiable Information by specific Business Units within Learning Ally may also be modified from time to time to reflect changes in business practices. In this event, those changes will appear in the relevant Privacy Notices.
</p>
<p>
Learning Ally will also comply with applicable U.S. laws that impose additional responsibilities on Learning Ally beyond those stated in this Policy in connection with the treatment of Personally Identifiable Information about its Members and Prospects.
</p>
<p>
Our Policy recognizes two kinds of personal data that deserve varying levels of protection. Personally Identifiable Information includes, for example, e-mail addresses, billing information, employment status and data that tracks user activity on a Web site or online services. A subset of that category, Sensitive Data, deserves additional safeguards. Sensitive Data includes, by way of example, Social Security numbers, personal financial data (such as credit card information) and information about specific medical conditions.
</p>
<p>
</p>
<h2>Learning Ally is committed to using all reasonable efforts to abide by the following Policy:
</h2>
<p>
</p>
<h3>Collection.</h3>
<p> Personally-Identifiable Information will only be collected to the extent that Learning Ally deems reasonably necessary to serve a legitimate business purpose. Please be aware that if you disclose Personally-Identifiable Information on Web site message boards or chat areas, that information may be collected and used by third parties without our knowledge and may result in unsolicited communications from third parties. Such activities are beyond the control of Learning Ally.
</p>
<h3>Notification:</h3>
<p> Members and Prospects will be notified (usually at the point where Personally-Identifiable Information is collected) as to the uses the Business Unit may make of the Personally-Identifiable Information collected from them.
</p>
<p>
</p>
<h3>Security:</h3>
<p> Appropriate safeguards will be implemented in an effort to ensure the security, integrity and privacy of Personally-Identifiable Information about our Members and Prospects.
</p>
<p>
</p>
<h3>Access:</h3>
<p> Review and Correction. Procedures have been developed through which Members and Prospects can, upon request, review and correct the Personally-Identifiable Information that has been collected from them by Learning Ally. These procedures may not, however, compromise the security, integrity and privacy of either Learning Ally Education own proprietary databases or databases licensed from third party companies. Sensitive Data. The collection and use of Sensitive Data carries with it special obligations and responsibilities in order to maintain the data's security, integrity and privacy.
</p>
<p>
Sensitive Data will not be rented or otherwise made available for External Distribution outside Learning. Please note that our Web sites may contain links to other Web Sites that have their own privacy policies and practices. While Learning Ally will employ commercially appropriate procedures to help ensure that your information is only used for authorized purposes as described above, Learning Ally cannot make any guarantees with respect to the actions or policies of such third parties.
</p>
<p>
</p>
<h3>Children:</h3>
<p> Collection and use of information from children in the U.S. under the age of thirteen will be made in compliance with the Children's Online Privacy Protection Act of 1998. Specifically, Learning Ally Education will:
</p>
<p>
Provide a parent or guardian, upon request, with the means to review the Personally-Identifiable Information collected from his or her child;
</p>
<p>
Provide a parent or guardian with the opportunity to prevent the further use of Personally-Identifiable Information that has already been collected, or the future collection of Personally-Identifiable Information, from that child;
</p>
<p>
Limit collection of Personally-Identifiable Information for a child's online participation in a game, prize offer or other activities to Personally-Identifiable Information that is reasonably necessary to participate in the activity; and Establish procedures to protect the confidentiality, security and integrity of the Personally-Identifiable Information collected from children.
</p>
<h3>Privacy Policy&nbsp;</h3>
<p>
The Learning Ally Member Privacy Policy has been developed out of respect for the privacy preferences and choices of our Members and Prospects. We have established procedures to ensure that every reasonable effort is made to address your concerns. Each Business Unit within Learning Ally has a designated manager to implement our Member Privacy Policy for its respective Members and Prospects.</p></div>








</div><!-- End_Module_2541 --></div>
	<div class="clear"></div>
<!-- </div> -->
</div></div>
                <div id="dnn_RightPane" class="rightpane DNNEmptyPane"></div>
            <div class="row">
				<div id="dnn_BottomLeftPane" class="bottomleftpane DNNEmptyPane"></div>
				<div id="dnn_BottomRightPane" class="bottomrightpane DNNEmptyPane"></div>
			</div>
        </div><!--End teacher-body-->
    </div> 
    
    <!--End container-->
</div> <!--End site-wrapper-->
       	                

<script>(function (w, d, t, r, u) {
var f, n, i; w[u] = w[u] || [], f = function () {
var o =
{ ti: "4077640" }
    ; o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad")
}, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function ()
{ var s = this.readyState; s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null) }
, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i)
})(window, document, "script", "//bat.bing.com/bat.js", "uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=4077640&Ver=2" height="0" width="0" alt="bing-seo" style="display:none; visibility: hidden;" /></noscript>

<footer class="site-footer" role="contentinfo">
   <div class="container">
      <div class="row">
	    <div class="col-sm-4">		
		    <div class="row">
		      <div class="col-xs-6">
		        <h3>About Us</h3>        
		  		    <nav role="navigation" aria-label="Footer Navigation. About Us">
		  		      <ul class="list-unstyled">			
		   			    <li><a href="http://www.learningally.org/AboutUs/WhoWeAre.aspx" title="Who We Are">Who We Are&nbsp;»</a></li>			
		   			    <li><a href="http://www.learningally.org/AboutUs/WhatWeDo.aspx" title="What We Do">What We Do&nbsp;»</a></li>	
						<li><a href="http://www.learningally.org/AboutUs/successstories.aspx" title="Success Stories">Success Stories&nbsp;»</a></li>	
		   			    <li><a href="http://www.learningally.org/AboutUs/leadership" title="Leadership">Leadership&nbsp;»</a></li>			
		   			    <li><a href="http://www.learningally.org/NAA" title="Scholarships">Scholarships&nbsp;»</a></li>			
		   			    <li><a href="http://www.learningally.org/AboutUs/Careers.aspx" title="Careers">Careers&nbsp;»</a></li>
						<li><a href="http://www.learningally.org/AboutUs/PressRoom.aspx" title="Press">Press&nbsp;»</a></li>
		   	      </ul>
		   	    </nav> 
		       </div><!-- /col -->    
		       <div class="col-xs-6">
		   	    <h3>Get Involved</h3>       
		  		    <nav role="navigation" aria-label="Footer Navigation. Get Involved">
		  		      <ul class="list-unstyled">			
							  <li><a href="http://www.learningally.org/volunteer" title="Volunteer">Volunteer&nbsp;»</a></li>
                              <li><a href="http://www.learningally.org/Volunteer/StudioLocations.aspx" title="Studio Locations">Studio Locations&nbsp;»</a></li>
                            <li><a href="https://supportus.learningally.org/checkout/donation?eid=55156/" title="Donate"  onclick="recordOutboundLink(this, 'classy', 'donate', 'Donate-footer)">Donate&nbsp;»</a></li>
							    <li><a href="http://www.learningally.org/SupportUs" title="Support Us">Support Us&nbsp;»</a></li>
							    <li><a href="http://www.learningally.org/AboutUs/Partners.aspx" title="Partners">Partners&nbsp;»</a></li>
							    <li><a href="https://go.learningally.org/parents/learning-ally-approach/dyslexia-resources/find-dyslexia-specialist/" title="Specialists"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'find-a-specialist-footer)">Specialists&nbsp;»</a></li>
		           </ul>
		         </nav>             
		       </div><!-- /col -->    
		    </div><!-- /row -->
		
	    </div><!-- /col --> 
	    <div class="col-sm-4">		
		    <div class="row">		 
		       <div class="col-xs-6">
				    <h3>Stay in Touch</h3>        
				    <nav role="navigation" aria-label="Footer Navigation. Stay in Touch">
					    <ul class="list-unstyled">
                        <li><a href="http://www.learningally.org/AboutUs/StayinTouch.aspx" title="Stay in Touch">Stay in Touch&nbsp;»</a></li>
						    <li><a href="https://go.learningally.org/news-events/blog/" title="Blog"  onclick="recordOutboundLink(this, 'wordpress', 'blog', 'blog-footer')">Blog&nbsp;»</a></li>
		 				    <li><a href="https://go.learningally.org/contact-us/" title="Contact Us"   onclick="recordOutboundLink(this, 'wordpress', 'blog', 'ContactUs-footer')">Contact Us&nbsp;»</a></li>
						 </ul>
				      <ul class="list-unstyled social-links">			
		 		   <li class="clearfix"><i class="facebook"></i><a title="Facebook page. Opens in a new window" target="_blank" href="http://www.facebook.com/LearningAlly.org" onclick="recordOutboundLink(this, 'social', 'facebook', 'facebook-footer')">Facebook</a></li>
		       	    <li class="clearfix"><i class="twitter"></i><a title="Twitter. Opens in a new window" target="_blank" href="http://twitter.com/Learning_Ally" onclick="recordOutboundLink(this, 'social', 'twitter', 'twitter-footer')">Twitter</a></li>
		       	    <li class="clearfix"><i class="linkedin"></i><a title="LinkedIn. Opens in a new window" target="_blank" href="http://www.linkedin.com/groups?home=&amp;gid=2644842&amp;trk=anet_ug_hm" onclick="recordOutboundLink(this, 'social', 'linkedin', 'linked-in-footer')">LinkedIn</a></li>
		       	    <li class="clearfix"><i class="youtube"></i><a  title="Youtube. Opens in a new window" target="_blank" href="http://www.youtube.com/learningally" onclick="recordOutboundLink(this, 'social', 'youtube', 'youtube-footer')">YouTube</a></li>
		       	    <li class="clearfix"><i class="google"></i><a title="Google Plus page. Opens in a new window" target="_blank" href="https://plus.google.com/108255844779026447464/posts"  onclick="recordOutboundLink(this, 'social', 'googleplus', 'google-plus-footer')">Google+</a></li>
                      <li class="clearfix"><i class="instagram"></i><a title="Instagram. Opens in a new window" target="_blank" href="http://instagram.com/Learning_Ally"  onclick="recordOutboundLink(this, 'social', 'click', 'instagram-footer')">Instagram</a>
                      </li>
		            	    <li class="clearfix"><i class="pinterest"></i><a title="Pinterest. Opens in a new window" target="_blank" href="http://pinterest.com/LearningAlly/"  onclick="recordOutboundLink(this, 'social', 'click', 'pinterest-footer)">Pinterest</a></li>
		 	  	    </ul>
		 		    </nav>
			    </div><!-- /col -->    
		      <div class="col-xs-6">
		        <h3>Helpful Tools</h3>        
		 		    <nav role="navigation" aria-label="Footer Navigation. Helpful Tools">
		 		      <ul class="list-unstyled">			
						    <li><a href="https://go.learningally.org/parents/dyslexia-screener/" title="Dyslexia Quiz" onclick="recordOutboundLink(this, 'wordpress', 'quiz', 'DyslexiaQuiz-footer')">Dyslexia Quiz&nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/Parents/DyslexiaResources/TutorNetwork.aspx" title="Find a Tutor">Find a Tutor&nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/Parents/DyslexiaResources/AllResources/specialistListing.aspx" title="Find a Specialist">Find a Specialist&nbsp;»</a></li>
		  				    <li><a href="http://www.learningally.org/parents/parentchat.aspx" title="Ask a Parent">Ask a Parent&nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/BrowseAudiobooks.aspx" title="Browse Audiobooks">Browse Audiobooks&nbsp;»</a></li>
		  	      </ul>
		  	    </nav> 
			    </div><!-- /col -->
		    </div><!-- /row -->
		
	    </div><!-- /col --> 
	    <div class="col-sm-4">		
		    <div class="row">    		
			    <div class="col-xs-6">
		   	    <h3>Support</h3>        
		  		    <nav role="navigation" aria-label="Footer Navigation. Support">
		  		      <ul class="list-unstyled">			
		   			    <li><a href="http://www.learningally.org/help.aspx" title="Help">Help&nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/IndividualTOS.aspx" title="Terms of Service">Terms of Service&nbsp;»</a></li>
		   	      </ul>
		   	    </nav>
			    </div><!-- /col -->    
			    <div class="col-xs-6">        
		   	    <h3>Join Us</h3>        
		  	    <nav role="navigation" aria-label="Footer Navigation. Join Us">
		  		    <ul class="list-unstyled">			
		   			    <li><a href="http://www.learningally.org/Parents/Join.aspx" title="Parent">Parent&nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/Adults/Join.aspx" title="Adult Learners">Adults &nbsp;»</a></li>
						    <li><a href="http://www.learningally.org/Educators.aspx" title="Educator">Educators&nbsp;»</a></li>
		   	      </ul>
		   	    </nav>  
		 	    </div><!-- /col -->
			    <div class="col-xs-12 app-badges">
		  	    <nav role="navigation" aria-label="Footer.  Application and Google Store">
		  		    <ul class="list-unstyled list-inline">			
		   			    <li><a href="https://itunes.apple.com/us/app/rfb-d-audio/id418888450" title="App Store. Opens in a new window" target="_blank"><img alt="Available on the App Store (iTunes)" class="img-responsive" src="/Portals/_default/Skins/LearningAllyBootstrap3/images/theme/badge-apple-app.png" /></a></li>
			          <li><a href="https://play.google.com/store/apps/details?id=org.learningally.learningallyaudioandroid&amp;hl=en" title="Google Play. Opens in a new window" target="_blank"><img alt="Android App on Google Play" class="img-responsive" src="/Portals/_default/Skins/LearningAllyBootstrap3/images/theme/badge-android-app.png" /></a></li>
		   	      </ul>
		   	    </nav>  
		 	    </div><!-- /col -->
		    </div><!-- /row -->
	    </div><!-- /col -->		
    </div><!-- /row --> 

    <div class="row">
      <div class="col-xs-12 copyright" role="navigation" aria-label="Footer. Copyright and links.">
           <p><span id="dnn_laFooter_dnnCOPYRIGHT_lblCopyright" class="copyright">Copyright 2016 by Learning Ally</span>
</p>
        <ul class="list-unstyled list-inline">			
  		    <li><a href="http://www.learningally.org/privacy.aspx" title="Privacy">Privacy&nbsp;</a></li>
  		    <li><a href="http://www.learningally.org/accessibility.aspx" title="Accessibility">Accessibility&nbsp;</a></li>
  		    <li><a href="https://go.learningally.org/la-sitemap/" title="Site Map"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'SiteMap-footer)">Site Map&nbsp;</a></li>
  		    <li><a href="https://supportus.learningally.org/checkout/donation?eid=55156/" title="Donations" onclick="recordOutboundLink(this, 'outbound', 'click', 'Donate-footer)">Support Us&nbsp;</a></li>
  		    <li><a href="https://go.learningally.org/contact-us/" title="Contact Us"  onclick="recordOutboundLink(this, 'wordpress', 'click', 'ContactUs-footer)">Contact Us&nbsp;</a></li>
  		    <li><a href="http://www.learningally.org/AboutUs/Join.aspx" title="Join">Join&nbsp;</a></li>
  	    </ul>
      </div>
    </div>
</div>
</footer>
<div id="freezerIcon" class="modal-backdrop fade in" style="opacity: 0.3; z-index: 8000; display: none;"> 
    <div style="position:absolute; left:50%; top:50%; margin-left:-33px; margin-top:-33px; width:66px; height:66px; ">
        <img width="66px" height="66px" src="/Portals/_default/Skins/LearningAllyBootstrap3/images/ajax-loader.gif" alt="Processing...Please Wait" />
    </div>
</div>	
        <input name="ScrollTop" type="hidden" id="ScrollTop" />
        <input name="__dnnVariable" type="hidden" id="__dnnVariable" autocomplete="off" value="`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`2008`,`evoq_TabId`:`2008`,`evoq_PageLanguage`:`en-US`,`evoq_ContentItemId`:`-1`,`evoq_UrlReferrer`:``,`evoq_UrlPath`:`http%3a%2f%2fwww.learningally.org%2fPrivacy`,`evoq_UrlQuery`:`%3fTabId%3d2008%26language%3den-US`,`evoq_ContentItemReferrer`:`-1`,`evoq_PersonalizedUrlReferrer`:`-1`,`evoq_DisableAnalytics`:`False`}" />
        <input name="__RequestVerificationToken" type="hidden" value="2NsGPNfdqV_n91Sj93fxiXY8brGDpO9xT83rZVC1LnEoBZ9UhFxgM0pgX8Xi2oOzdwvh1w2" /><script src="/Portals/_default/Skins/LearningAllyBootstrap3/javascripts/breadcrumbs.js?cdv=1082" type="text/javascript"></script>
    
<script type='text/javascript'>
//<![CDATA[
(function() {

  var analyticsSettings;
  var initAnalyticsSettings = function (e) {
    analyticsSettings = {
      pageStart: analytics_page_startTime,
      servicesFramework: $.ServicesFramework(-1)
    };
  };
  if(window.addEventListener) {
    window.addEventListener('load', initAnalyticsSettings, false);
  } else {
    window.attachEvent('onload', initAnalyticsSettings);
  };

  var eventName = (window.onpagehide || window.onpagehide === null) ? 'pagehide' : 'unload';
  if (window.addEventListener) {
    window.addEventListener(eventName, function (e) {
      if (analyticsSettings) {
        analyticsSettings.eventSource = 'pagehide';
        if(dnn && dnn.analytics) { dnn.analytics.clientUnloading(e,analyticsSettings); }
      }
    }, false);
  } else {
    window.attachEvent('on' + eventName, function (e) {
      if (analyticsSettings) {
        analyticsSettings.eventSource = 'pagehide';
        if(dnn && dnn.analytics) { dnn.analytics.clientUnloading(e,analyticsSettings); }
      }
    });
  }

})();
//]]>
</script>
</form>
    <!--CDF(Javascript|/js/dnncore.js?cdv=1082)--><!--CDF(Javascript|/js/dnn.modalpopup.js?cdv=1082)--><!--CDF(Css|/Resources/Shared/stylesheets/dnndefault/7.0.0/default.css?cdv=1082)--><!--CDF(Css|/Portals/6/portal.css?cdv=1082)--><!--CDF(Javascript|/DesktopModules/DNNCorp/EvoqContentLibrary/ClientScripts/dnn.analytics.Injected.js?cdv=1082)--><!--CDF(Javascript|/js/dnn.js?cdv=1082)--><!--CDF(Javascript|/js/dnn.servicesframework.js?cdv=1082)--><!--CDF(Javascript|/Resources/libraries/jQuery/01_09_01/jquery.js?cdv=1082)--><!--CDF(Javascript|/Resources/libraries/jQuery-Migrate/01_02_01/jquery-migrate.js?cdv=1082)--><!--CDF(Javascript|/Resources/libraries/jQuery-UI/01_11_03/jquery-ui.js?cdv=1082)-->
    
</body>
</html>