
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:og="http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<html lang="en">

<head>

	<meta charset="utf-8">
	<title>BookBrowse.com's privacy statement</title>
	<meta name="description" content="BookBrowse.com's privacy statement">
	<meta name="author" content="BookBrowse">
	
		
	<meta name="verify-v1" content="nk8F1oMygMpT0ekiLzIuurj97ciVOxiP5OHk9DNpdFg=" />						
	<meta name="ROBOTS" content="ALL, INDEX" />
	<meta name="copyright" content="Copyright (c) BookBrowse" />
	<meta name="rating" content="general" />
	
	
	<meta name="viewport" content="width=device-width, initial-scale=1">

	
	
	
	<meta property="og:title" content="BookBrowse.com's privacy statement"/>
	<meta property="og:type" content="book"/>
	<meta property="og:image" content="https://www.bookbrowse.com/https://www.bookbrowse.com/site/images/bblogo-big.gif"/>
	<meta property="og:site_name" content="BookBrowse.com"/>
	<meta property="og:description" content="BookBrowse.com's privacy statement"/>
	<link rel="image_src" href="https://www.bookbrowse.com/site/images/bblogo-big.gif" / ><!--formatted-->
        
	<!-- Favicon -->
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	
	<!-- Stylesheets -->
	<link href='https://fonts.googleapis.com/css?family=Rufina:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/site/style/bibliophile.css">
	<link rel="stylesheet" href="/site/style/tagcloud.css"> 
	<link rel="stylesheet" href="/site/style/style.css">
	<link rel="stylesheet" media="print" title="pageturner" href="/site/style/printable.css" /> 
	
	
	
	
	
	<link rel="alternate" type="application/rss+xml" href="https://bookbrowse.com/rss/newest_reader_reviews.rss"  	 title="Bookbrowse: Most Recent Recommended Reader Reviews." />
	<link rel="alternate" type="application/rss+xml" href="https://bookbrowse.com/rss/book_news.rss" 				 title="Bookbrowse News: The latest news on authors, publishers, and book-related topics." />
	<link rel="alternate" type="application/rss+xml" href="https://bookbrowse.com/rss/bookbrowse_free_newsletter.rss" title="Bookbrowse Free Newsletter: Highlights of books coming soon, and in-depth profiles of selected books and authors." />
	<link rel="alternate" type="application/rss+xml" href="https://bookbrowse.com/rss/member_ezines.rss" 			 title="Bookbrowse Member ezine: Get detailed information on forthcoming books, in-depth book reviews, reading guides, and author interviews." />
	<link rel="alternate" type="application/rss+xml" href="https://www.bookbrowse.com/blogs/editor/rss.cfm?mode=full" title="BookBrowse Editor's Blog" />
	
	
	<link rel="apple-touch-icon" href="/site/images/mobile/apple-touch-icon.png" />
	<link rel="apple-touch-icon" href="/site/images/mobile/touch-icon-ipad.png" 		 sizes="72x72" />
	<link rel="apple-touch-icon" href="/site/images/mobile/apple-touch-icon-72x72.png" 	 sizes="72x72" />
	<link rel="apple-touch-icon" href="/site/images/mobile/touch-icon-iphone4.png" 		 sizes="114x114" />
	<link rel="apple-touch-icon" href="/site/images/mobile/apple-touch-icon-114x114.png" sizes="114x114" />
	<link rel="apple-touch-icon" href="/site/images/mobile/apple-touch-icon-144x144.png" sizes="144x144">
	<link rel="apple-touch-icon-precomposed" href="/site/images/mobile/apple-touch-icon-precomposed.png"> 
	<!-- Tile icon for Win8 (144x144 + tile color) -->
	<meta name="msapplication-TileImage" content="/site/images/mobile/apple-touch-icon-144x144.png">
	<meta name="msapplication-TileColor" content="#e5e1ba">
	<!-- and now for android --->
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" href="/site/images/mobile/apple-touch-icon.png">

	<!-- JavaScript -->
	<!--[if lt IE 9]><script src="/site/js/ie9.js">IE7_PNG_SUFFIX=".png";</script><![endif]-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script> 
	<script src="/site/js/jquery.bxslider.min.js"></script>
	<script src="/site/js/custom.js"></script>
	<script src="/site/js/jquery.hashchange.min.js"></script>
	<script src="/site/js/jquery.easytabs.min.js"></script>
	<script src="/site/js/swfobject.js"></script>		

</head>
<body> 
<!-- header -->
<header class="header">
	<div class="wrapper">
		
            <a href="https://www.bookbrowse.com" class="logo"><img src="/site/images/bblogo.gif" alt="BookBrowse Logo" /></a>
			<ul>
				<li><a href="https://www.bookbrowse.com/join/" title="Join">Join</a></li>
                <li><a href="https://www.bookbrowse.com/gift_certificate/" title="Member Login">Gift</a></li>
                <li><a href="https://www.bookbrowse.com/login/" title="Member Login">Member Login</a></li>
                <li><a href="https://www.bookbrowse.com/library" title="Library Member Login">Library Login</a></li>
			</ul>
			<a href="/lib"><img src="/images/awards/mla-gold-score-logo-small.jpg" style="width: 100px; float: right; margin-top: 10px;" alt="MLA Gold Award Site" title="BookBrowse Wins Gold!"></a>
        
		
	</div>
</header>
<!-- /header -->
<!-- main menu -->



<nav class="main_menu main_menu_head">
	<div class="wrapper">
		<div class="menu_trigger"></div>
		<ul>
			<li><a href="https://www.bookbrowse.com">Home</a></li>
			<li class="ezine"><a href="https://www.bookbrowse.com/mag/"><i>The Review</i></a></li>
			<li class="submenu">
				<a href="https://www.bookbrowse.com/current_books/">What's New</a>
				<ul>
					<li><a href="https://www.bookbrowse.com/arc/arc_reviews/">Members Recommend</a></li>
					<li><a href="https://www.bookbrowse.com/coming_soon/">Publishing Soon</a></li>
					<li><a href="https://www.bookbrowse.com/browse/index.cfm/category_number/56/new-in-hardcover/">Featured This Week</a></li>
					<li><a href="https://www.bookbrowse.com/browse/index.cfm/category_number/60/recommended-hardcovers/">New in Hardcover</a></li>
					<li><a href="https://www.bookbrowse.com/browse/index.cfm/category_number/61/recommended-paperbacks/">New in Paperback</a></li>
					<li><a href="https://www.bookbrowse.com/news/">Book News</a></li>
					
				</ul>
			</li>
			<li class="submenu">
				<a href="https://www.bookbrowse.com/category/">Find Books</a>
				<ul>
					
					<li><a href="https://www.bookbrowse.com/category/index.cfm/tc/1">Fiction</a></li>
					<li><a href="https://www.bookbrowse.com/category/index.cfm/tc/2">Non-Fiction</a></li>
                    <li><a href="https://www.bookbrowse.com/category/index.cfm/tc/3">Young Adults</a></li>
                    <li><a href="https://www.bookbrowse.com/category/index.cfm//tc/tags">Books by Theme</a></li>
					
					<li><a href="https://www.bookbrowse.com/category/index.cfm/tc/favorites">Favorites</a></li>
					<li><a href="https://www.bookbrowse.com/category/index.cfm/tc/awards">Award Winners</a></li>
					<li><a href="https://www.bookbrowse.com/jumper">Surprise Me!</a></li>
                   
				</ul>
			</li>
			
            
            <li><a href="https://www.bookbrowse.com/read-alikes/">Read-Alikes</a></li>
            
             <li class="submenu">
				<a href="https://www.bookbrowse.com/bookclubs">Book Clubs</a>
				<ul>
					<li><a href="https://www.bookbrowse.com/reading_guides/genre/">Reading Guides</a></li>
					<li><a href="https://www.bookbrowse.com/onlinebookclub/">Discuss Books</a></li>
                    <li><a href="https://www.bookbrowse.com/bookclubs/advice/">Advice</a></li>
                    <li><a href="https://www.bookbrowse.com/wp/">Book Club Report</a></li>
					
				</ul>
			</li>
                  
            <li><a href="https://www.bookbrowse.com/authors/">Authors</a></li>
			
            
            
          

			
            <li><a href="https://www.bookbrowse.com/blogs/editor/">Blog</a></li>
             
           <li class="submenu">
				<a href="https://www.bookbrowse.com/fun/">Free and Fun</a>
				<ul>
					<li><a href="https://www.bookbrowse.com/arc/">First Impressions</a></li>
					                  
				</ul>
			</li>
            <li><a href="https://www.bookbrowse.com/about/">About</a></li>
            
		</ul>
	</div>
</nav>

<!-- /main menu --> 
<div class="search_wrap">
	<div class="wrapper">
		<!-- search_block -->
		<div class="search_block">
			<form action="/search/index.cfm" method="post" id="search">
				<p>
				<span>Search:</span>
				<input type="checkbox" id="title" name="s_title" class="g" checked="checked" value="1"/><label for="title">Title</label>
				<input type="checkbox" id="author" name="s_author" class="g" checked="checked" value="1"/><label for="author">Author</label>
				<input type="checkbox" id="isbn" name="s_isbn" class="g"  value="1"/><label for="isbn">ISBN</label>
				<input type="text" id="sstring" name="search_string" value="Enter title, author or isbn" />
				<input type="submit" value="Go!" />
				</p>
			</form>	
		</div>
		<!-- /search_block -->
	</div>



</div>


<script>
    $(document).ready(function(){
        $('input.g[name!="s_isbn"]').click(function(e){
            $('input.g[name="s_isbn"]').prop('checked',false);
        });
        $('input.g[name="s_isbn"]').click(function(e){
            $('input.g[name!="s_isbn"]').prop('checked',false);
        });
    });
</script>


	
	
	<div class="container">
		<div class="wrapper">
		<div class="left_column">
	<h1></h1>
	<div class="top_block">
		<div><h2>BookBrowse Privacy Statement</h2>
		<p>BookBrowse.com has created this privacy statement in order to demonstrate our
	firm commitment to privacy. The following discloses the information gathering
	and dissemination practices for this website.</p></div>
	</div>
	
	<div class="pinkblock">
	<h4>Personal Information</h4>
	<p>
	BookBrowse.com does not collect any personal information on you unless you provide it. E.g. if you signup for our free newsletter or become a member we will store your email address and other basic contact information (first name for the free newsletter and name and mailing address for the membership list).  
	<br><br>
We use a third-party email host for maiings, so the name and email address of those on our mailing lists are also securely stored at our email host, Constant Contact. 
	<br><br>
	Email addresses collected when people enter BookBrowse giveaways, quizzes or take part in surveys are used only to select the winning names and are deleted within a few weeks of the winners having claimed their prizes.
	<br><br>
	We make every effort to protect BookBrowse from malicious attack - but if we were hacked, members' credit card information would not be compromised because we do not store any financial information on BookBrowse at all (it is passed directly to our payment processor, Paypal). 
	<br><br>
    Publishers usually mail the books that people win in book giveaways, or are assigned to read for First Impressions or the Book Club. When we send a mailing list to the publisher to fulfil it is with clear instructions that the list is to be used only to mail the specific book to the specific people listed, and under no circumstance is to be stored for future use.  
    <br><br>
	<i><b>We have never and will never sell, give-away or rent our mailing list</b></i>.<br />
    The only time we would ever do so would be with your express permission or if ordered by a court of law. 
      <br /><br /> 
	<a href="https://www.bookbrowse.com/information/index.cfm?fuseaction=security">More about internet security</a> in general and BookBrowse's specific security.

    
	</p><br />
	
	
	<h4>Tell-A-Friend</h4>
	<p>
	Visitors sometimes wish to tell a friend about a particular page at BookBrowse, or send themselves a reminder.  For this reason, like many other websites, BookBrowse provides a convenient 'tell-a-friend' tool that allows you to send a personal message to a friend telling them about BookBrowse.  
	<br><br>
	Email addresses are used only to send this one-time message, and are not stored or recorded by BookBrowse in anyway.
	
	</p><br />
    
    
    	<h4>"My Reading List"</h4>
	<p>
Members have the option of creating and storing book lists on BookBrowse - for example books they have read and want to read. Members can also record their personal thoughts on these books. At this time, member reading lists can only be viewed by the individual member when that member is logged in. They are not available to the general public to view at all. Members have requested that BookBrowse provide the ability for lists to be made public - either so that the member can provide access to specific people or so that their lists are freely available to all internet users. If BookBrowse were to do this in the future, the default setting would be that a list remains private unless an individual member specifically opts to make his or her list(s) public. 
	</p><br />
	
	<h4>Information Automatically Logged</h4>
	<p>
	We collect IP (internet protocol) addresses to help diagnose problems with our server and to administer our Web site.  IP addresses are also used to gather broad demographic information such as the percentage of visitors by country. 
	<br><br>
	Because IP addresses are usually shared by many people, BookBrowse (like the majority of websites) uses a 'cookie' in order to know how many individuals come to the site.  When you visit us, BookBrowse's server sends a tiny file (cookie) to your computer containing a sequence of letter & numbers. This is stored on your hard disk in a special folder for cookies.  
	<br><br>
	If you visit BookBrowse again, your computer will check its files to see if a cookie matches, and if so send this little file back to BookBrowse, so that you are recognized as a returning visitor.  This helps us calculate how many people visit in total and when they visit, but it <i>does not</i> give us any personal information about you at all.  
	<br><br>This is what the contents of a typical BookBrowse cookie file looks like:
	<br>CFID 212442 bookbrowse.com/1536 3546759168 29693834<br><br>
	If you're interested in knowing more about cookies we recommend <a href="http://computer.howstuffworks.com/cookie1.htm" title="howstuffworks.com">this site</a>.
	</p><br />
	

	<h4>Information Provided by Third Parties</h4>
	<p>
    <i>Third Party Content</i>
	<br><br>
	BookBrowse contains a considerable amount of information provided by third parties, including, but not limited to, advertising, excerpts, reviews, interviews, book summaries and text links.  As such, content at BookBrowse.com does not necessarily represent the views of the owners of BookBrowse.com and is not endorsed by BookBrowse.com.
	<br><br>
	
	<i>Third Party Links</i>
	<br><br>
	This site contains links to other sites, including but not limited to, banner advertising and text links. BookBrowse.com is not responsible for the privacy practices or the content of any third party sites, and a link to a third party site should not be seen as an endorsement by BookBrowse of that site.
	<br><br>
	
	<i>Advertising</i>
	<br><br>From time to time we use third-party advertising companies to serve ads on BookBrowse. These companies use cookies to track your visits to this and other websites in order to provide advertisements that may be of interest to you. The information about you does not include personal details such as your name or address, it is simply a cookie (as explained in the previous section) that tracks general traffic movements. If you would like more information about this practice, or you would like to opt out from having your data collected by some advertising networks, please <a href="http://www.networkadvertising.org/optout_nonppii.asp" title="opt out of ads">click here</a>.
	<br><br>
    
  
    
    	<h4>Contacting BookBrowse.com</h4>
	<p>
	If you have any questions about this privacy statement, the practices of this site, or your dealings with this website, please <a href="https://www.bookbrowse.com/contactus" title="contact us">contact us</a>.
	</p><br />
    
    
  
	

	
	<p class="smallertextright">Last updated January 2014</pr>
	
	</div>
</div> <!-- right_column -->
<div class="right_column">






	<div class="member_title">
		<span>Member Benefits</span>
		<div class="members_advantages_block">
			<p class="clear"><a href="https://www.bookbrowse.com/join" class="button red">Join Now!</a></p>
			<div class="clear">
				<h4>Check the advantages!<br />Just $10 for 3 months or $35 for a year</h4>
					
				<ul>
					<li>
						<ul>
							<li>&nbsp;</li>
							<li>FREE</li>
							<li>MEMBER</li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Range of media reviews for each book</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Excerpts of all featured books</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Author bios, interviews and pronunciations</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Browse by genre</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Book club discussions</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Book club advice and reading guides</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>BookBrowse reviews and "beyond the book" back-stories</li>
							<li>&nbsp;</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Reviews of notable books ahead of publication</li>
							<li>&nbsp;</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Free books to read and review (US Only)</li>
							<li>&nbsp;</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Browse for the best books by time period, setting & theme</li>
							<li>&nbsp;</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>Read-alike suggestions for thousands of books and authors</li>
							<li>&nbsp;</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
					<li>
						<ul>
							<li>'My Reading List" to keep track of your books</li>
							<li>&nbsp;</li>
							<li><img src="/site/images/icons/check2.png" alt="" /></li>
						</ul>
					</li>
				</ul>	
			</div>
		</div>	
	</div>




<div class="newsletter_block">
		<h4>Free Newsletters</h4>
		<p>Reviews, previews, articles, giveaways, book club resources & much more!</p>
		<p><a href="/mailman##top" class="button red">Subscribe</a></p>
	</div>




<div class="editors_choice border clear">
	
    <h4>Editor's Choice</h4>
     
	<ul>
		
		<li>
			<figure>
				<a href="https://www.bookbrowse.com/mag/reviews/index.cfm/book_number/3372/while-the-city-slept" title="While the City Slept">
                <img src="/images/jackets/9780670015719.jpg" alt="Book Jacket: While the City Slept" />
	            </a>
				<figcaption>
					<strong><a class="newgreen" href="https://www.bookbrowse.com/mag/reviews/index.cfm/book_number/3372/while-the-city-slept" title="While the City Slept">While the City Slept</a></strong>
					<br>by Eli Sanders
					<div class="spacer"></div>
					It would be hard to say who exhibited the greater degree of madness: the deeply troubled and ...
				</figcaption>	
			</figure>	
		</li>
		
		<li>
			<figure>
				<a href="https://www.bookbrowse.com/mag/reviews/index.cfm/book_number/3363/dog-run-moon" title="Dog Run Moon">
                <img src="/images/jackets/9780812993776.jpg" alt="Book Jacket: Dog Run Moon" />
	            </a>
				<figcaption>
					<strong><a class="newgreen" href="https://www.bookbrowse.com/mag/reviews/index.cfm/book_number/3363/dog-run-moon" title="Dog Run Moon">Dog Run Moon</a></strong>
					<br>by Callan Wink
					<div class="spacer"></div>
					A Montana fly-fishing guide near Yellowstone National Park, Callan Wink has clearly impressed the ...
				</figcaption>	
			</figure>	
		</li>
		
		<li>
			<figure>
				<a href="https://www.bookbrowse.com/mag/reviews/index.cfm/book_number/3360/the-age-of-genius" title="The Age of Genius">
                <img src="/images/jackets/9781620403440.jpg" alt="Book Jacket: The Age of Genius" />
	            </a>
				<figcaption>
					<strong><a class="newgreen" href="https://www.bookbrowse.com/mag/reviews/index.cfm/book_number/3360/the-age-of-genius" title="The Age of Genius">The Age of Genius</a></strong>
					<br>by A.C. Grayling
					<div class="spacer"></div>
					Why do you think you think the way you think?<br><br>

That rather flippant question conveys a genuinely ...
				</figcaption>	
			</figure>	
		</li>
		
	</ul>
    
</div>


    
<div class="display_right"> 
				<A HREF="/jump/redir.cfm?loc=856" TARGET="_top" rel="nofollow" id="display">
				<IMG SRC="/display/Shelter.gif" ALT="Shelter" BORDER="0" ALIGN="absmiddle" width="300px" height="250px"></A>
			
	
	<div class="spacer"></div>
</div>



<div class="brownblock swirl">
	<div class="db">
		<div class="obcbook">
			<h5>Book Discussions</h5>
			<div class="spacer"></div>
			
				<figure>
					<a href="https://www.bookbrowse.com/bb_briefs/detail/index.cfm/ezine_preview_number/10161/black-river" title="Black River">
		            <img src="/images/previews_images/9780544570238.jpg" alt="Book Jacket">
		            
		            </a>
					<figcaption>
						<strong>Black River</strong><br />by S. M. Hulse
						<div class="spacer"></div>
						
                        <p>Four starred reviews for this debut that will turn readers' hearts inside out.</p>
                        <a href="https://www.bookbrowse.com/bb_briefs/detail/index.cfm/ezine_preview_number/10161/black-river" title="Black River">About the book</a> 
		                <br>
						<a href="https://www.bookbrowse.com/booktalk/threads.cfm?forumid=1C82B1F6-0346-2A52-9B11BF599439ADE9">Join the discussion!</a>
					</figcaption>	
				</figure>
				<div class="clearer"></div>
				
			
		</div>
	</div>
</div>



<div class="display_right"> 
<A HREF="https://ad.doubleclick.net/ddm/jump/N115602.109082BOOKBROWSE/B9413733.127897910;abr=!ie4;abr=!ie5;sz=300x250;ord=1458161869151?">

<IMG SRC="https://ad.doubleclick.net/ddm/ad/N115602.109082BOOKBROWSE/B9413733.127897910;abr=!ie4;abr=!ie5;sz=300x250;ord=1458161869151?" BORDER=0 WIDTH=300 HEIGHT=250 ALT="Advertisement"></A>
</div>


<div class="first_imp_reviews clear">
	<h4>First Impressions</h4>
	<ul>
		
		<li>
			<figure>
				<a href="https://www.bookbrowse.com/bb_briefs/detail/index.cfm/ezine_preview_number/11103/the-last-confession-of-thomas-hawkins" title="The Last Confession of Thomas Hawkins">
                <img src="/images/previews_images/0544639685.jpg" alt="Book Jacket">
                
                </a>
				<figcaption>
					<p><strong>The Last Confession of Thomas Hawkins</strong><br />by Antonia Hodgson</p>
					<div class="rating"> <div id="fourstarh"></div>
        </div>
					<p>Pitch-perfect suspense craftily constructed adds up to a winning mystery set in 18th century England.</p>
					
					
					<p><a href="https://www.bookbrowse.com/arc/arc_reviews/detail/index.cfm/arc_number/598/the-last-confession-of-thomas-hawkins">Read Member Reviews</a></p>
				</figcaption>	
			</figure>	
		</li>
		
		<li>
			<figure>
				<a href="https://www.bookbrowse.com/bb_briefs/detail/index.cfm/ezine_preview_number/11072/shelter" title="Shelter">
                <img src="/images/previews_images/1250075610.jpg" alt="Book Jacket">
                
                </a>
				<figcaption>
					<p><strong>Shelter</strong><br />by Jung Yun</p>
					<div class="rating"> <div id="fourstarh"></div>
        </div>
					<p>A masterfully crafted debut novel in the tradition of <i>House of Sand and Fog</i>.</p>
					
					
					<p><a href="https://www.bookbrowse.com/arc/arc_reviews/detail/index.cfm/arc_number/592/shelter">Read Member Reviews</a></p>
				</figcaption>	
			</figure>	
		</li>
		
	</ul>	
    <p>Members review books pre-publication. Read their opinions in <a href="/arc/arc_reviews/">First Impressions</a></p>
</div> <!-- win_book -->

<div class="info_block win_book float_right_block">
		<h5>Win this book!</h5>
		
		<img src="/images/previews_images/140006953X.jpg" alt="Win Georgia" />
		<p><strong><i>Georgia: A Novel</i></strong></p>
        <p>A novel is as magical and provocative as Georgia O'Keeffe's lush paintings.</p>
		<p><a href="https://www.bookbrowse.com/giveaway/" class="button">Enter</a></p>
		
	</div>

<!-- /win_book --> <div class="info_block word_play">
	<h5>Word Play</h5>
	
			
			<p><strong>Solve this clue:</strong></p>
			
			<p class="clear"><a href="https://www.bookbrowse.com/wordplay/" class="button blue">Y Can't G H A</a></p>
			
			<p>and be entered to win..</p>
		
</div> 
	<div class="quote_block">
		<div>
			
            <h1><span>Books that&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><br /><i>entertain,<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;engage</i><br />&nbsp;& enlighten<br /></h1>
            <p>Visitors can view some of BookBrowse for free. Full access is for members only.</p>
			<p><a class="button float_left" href="/join">Join Today!</a></p>
		</div>	
	</div>






	

</div>	
<!-- /right_column --> <!-- quote_block3 -->

	<div class="quote_block quote_block3">
		<div>
			<div class="desc">
				<p><i>Your guide to</i><span>exceptional&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;books</p>
				<p>BookBrowse seeks out and recommends books that we believe to be best in class. Books that will whisk you to faraway places and times, that will expand your mind and challenge you -- the kinds of books you just can't wait to tell your friends about. </p>
			</div>
			<div class="social">
				
					<h5>Newsletter</h5>
					<p>Subscribe to receive some of our best reviews, "beyond the book" articles, book club info & giveaways by email.</p>
					<form method="post" action="/mailman/index.cfm#top">
					<p><input type="text" name="ea"/><input type="submit" value="Subscribe" class="button red" /></p>
					</form>
					
				<ul>
					<li><a id="facebook" class="addthis_button_facebook_follow" addthis:userid="bookbrowse"><img src="/images/spacer.gif"></a></li>
					<li><a id="twitter" class="addthis_button_twitter_follow" addthis:userid="bookbrowse"><img src="/images/spacer.gif"></a></li>
					<li><a id="pinterest" class="addthis_button_pinterest_follow" addthis:userid="bookbrowse"><img src="/images/spacer.gif"></a></li>
					<li><a id="rss" class="addthis_button_rss_follow" addthis:url="http://www.bookbrowse.com/rss/"><img src="/images/spacer.gif"></a></li>
					<li><a id="email"href="/mailman" title="Subscribe"><img src="/images/spacer.gif"></a></li>
				</ul>	
			</div>
			<div class="clear">&nbsp;</div>
		</div>
	</div>

<!-- /quote_block3 -->
		</div>
	</div>
	
	<!-- footer menu -->

<nav class="main_menu">
	<div class="wrapper">
		<ul>
			<li><a href="https://www.bookbrowse.com/information/index.cfm/fuseaction/editorial_submissions">Book Submissions</a></li>
			<li><a href="https://www.bookbrowse.com/information/index.cfm/fuseaction/advertisers">Advertising</a></li>
			<li><a href="https://www.bookbrowse.com/lib">Library Subscriptions</a></li>
			<li><a href="https://www.bookbrowse.com/reviewers/index.cfm/fuseaction/apply">Reviewing for BookBrowse</a></li>
            <li><a href="https://www.bookbrowse.com/apps/">Apps</a></li>
            <li><a href="https://www.bookbrowse.com/contactus/">Contact Us</a></li>
		</ul>
	</div>
</nav>
<!-- /footer menu -->

<!-- footer -->
<footer class="footer">
	<div class="wrapper">
		<ul>
			<li>
				<ul>
					<li><a href="https://www.bookbrowse.com">Home</a></li>
					<li><a href="https://www.bookbrowse.com/mag/">Magazine</a></li>
					<li>
						<a href="https://www.bookbrowse.com/current_books/">What's New</a>
						<ul>
							<li><a href="https://www.bookbrowse.com/arc/arc_reviews/">Members Recommend</a></li>
							<li><a href="https://www.bookbrowse.com/coming_soon/">Publishing Soon</a></li>
							<li><a href="https://www.bookbrowse.com/browse/index.cfm/category_number/56/new-in-hardcover/">Featured This Week</a></li>
							<li><a href="https://www.bookbrowse.com/browse/index.cfm/category_number/60/recommended-hardcovers/">New in Hardcover</a></li>
							<li><a href="https://www.bookbrowse.com/browse/index.cfm/category_number/61/recommended-paperbacks/">New in Paperback</a></li>
							<li><a href="https://www.bookbrowse.com/news/">Book News</a></li>
						</ul>	
					</li>
				</ul>	
			</li>
			<li>
            
                  
				<ul>
					<li><a href="https://www.bookbrowse.com/read-alikes/">Read-Alikes</a></li>
					<li>
						<a href="https://www.bookbrowse.com/category/">Find a Book</a>
						<ul>
					<li><a href="https://www.bookbrowse.com/category/index.cfm/tc/1" title="Fiction">Fiction</a></li>
					<li><a href="https://www.bookbrowse.com/category/index.cfm/tc/2" title="Non-Fiction">Non-Fiction</a></li>
                    <li><a href="https://www.bookbrowse.com/category/index.cfm/tc/3" title="Young Adults">Young Adults</a></li>
                    <li><a href="https://www.bookbrowse.com/category/index.cfm//tc/tags" title="Tag Cloud">Books by Theme</a></li>
					
					<li><a href="https://www.bookbrowse.com/category/index.cfm/tc/favorites" title="Favorites">Favorites</a></li>
					<li><a href="https://www.bookbrowse.com/category/index.cfm/tc/awards" title="Award Winners">Award Winners</a></li>
					<li><a href="https://www.bookbrowse.com/jumper" title="Surprise Me!">Surprise Me!</a></li>
						</ul>	
					</li>
				</ul>	
			</li>
			<li>
				<ul>
					<li>
						<a href="https://www.bookbrowse.com/authors/">Authors</a>
						<ul>
							<li><a href="https://www.bookbrowse.com/author_interviews/">Interviews</a></li>
							<li><a href="https://www.bookbrowse.com/authors/allauthors/">Biographies</a></li>
							<li><a href="https://www.bookbrowse.com/authors/author_pronunciations/">Pronunciation Guide</a></li>
							<li><a href="https://www.bookbrowse.com/author_websites/">Author Websites</a></li>
						</ul>	
					</li>
                    
                    
                    
					<li>
						<a href="https://www.bookbrowse.com/bookclubs">Book Clubs</a>
						<ul>
							<li><a href="https://www.bookbrowse.com/booktalk/">Discuss Book</a></li>
							<li><a href="https://www.bookbrowse.com/featured-bookclubs/archives/">Book Club Q&As</a></li>
							<li><a href="https://www.bookbrowse.com/reading_guides/genre/">Reading Guides</a></li>
							<li><a href="https://www.bookbrowse.com/bookclubs/advice">Advice</a></li>
						</ul>	
					</li>
				</ul>	
			</li>
			<li>
            
   
            
				<ul>
					<li><a href="https://www.bookbrowse.com/blogs/editor/">Blog</a></li>
					<li>
						<a href="https://www.bookbrowse.com/fun/">Free and Fun</a>
						<ul>
							<li><a href="https://www.bookbrowse.com/arc/">First Impressions</a></li>
							<li><a href="https://www.bookbrowse.com/wordplay/">Wordplay</a></li>
							<li><a href="https://www.bookbrowse.com/giveaway/">Book Giveaway</a></li>
							<li><a href="https://www.bookbrowse.com/bb_poll/">Polls</a></li>
							<li><a href="https://www.bookbrowse.com/quotes">Literary Quotes</a></li>
							<li><a href="https://www.bookbrowse.com/quiz">Personality Quiz</a></li>
                            <li><a href="https://www.bookbrowse.com/apps">BookBrowse Apps</a></li>
						</ul>	
					</li>
				</ul>	
			</li>
			<li>
				<ul>
					<li><a href="https://www.bookbrowse.com/about">About</a>
						<ul>
							<li><a href="https://www.bookbrowse.com/information/index.cfm/fuseaction/about">About Us</a></li>
							<li><a href="https://www.bookbrowse.com/contactus">Contact Us</a></li>
							
								<li><a href="https://www.bookbrowse.com/join/">Become a Member</a></li>
							
                            <li><a href="https://www.bookbrowse.com/gift_certificate/">Gift Memberships</a></li>
							<li><a href="https://www.bookbrowse.com/apps/">Apps</a></li>
                            <li><a href="https://www.bookbrowse.com/information/index.cfm/fuseaction/faqs/">FAQs</a></li>
                            <li><a href="https://www.bookbrowse.com/reviewers/">Meet Us</a></li>
							<li><a href="https://www.bookbrowse.com/information/index.cfm/fuseaction/people_say/">People Say</a></li>
							<li><a href="https://www.bookbrowse.com/press_info/">Press Info</a></li>
							<li><a href="https://www.bookbrowse.com/contactus/">Contact Us</a></li>
						</ul>	
					<li><a href="https://www.bookbrowse.com/information/index.cfm/fuseaction/privacy">Privacy Statement</a></li>
					<li><a href="javascript:OpenWindow('https://www.bookbrowse.com/more_info/index.cfm/fuseaction/terms','Terms',450,500)">Terms of Service</a></li>
				</ul>	
			</li>
		</ul>
	</div>
</footer>
<!-- /footer -->

<!-- footer copy -->
<footer class="footer_copy">
	<div class="wrapper">
		<p>&copy; BookBrowse LLC 1997-2016. All rights reserved. Information at BookBrowse.com is published with the permission of the copyright holder or their agent.<br />
		It is forbidden to copy anything for publication elsewhere without written permission from the copyright holder.</p>
        
        <br/><br />

	</div>
</footer>
<!-- /footer copy -->
<!-- Start Quantcast tag -->

<!-- End Quantcast tag -->




<!-- old Google Analytics tag 
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3368487-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
 End old Google Analytics tag -->

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3368487-1', 'bookbrowse.com');
  ga('require', 'linkid', 'linkid.js');
  ga('send', 'pageview');

</script>
<!-- End Google Analytics -->


</body>
</html> 





<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({appId: '176762499038251', status: true, cookie: true,
             xfbml: true});
  };

  // Load the SDK's source Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));
</script>




<script>
function OpenTellAFriend(a) {
    var maxheight = 400;
    maxheight = screen.height;
    if (maxheight > 660) {
      maxheight = 660;
    } else {
      maxheight = maxheight - 100;
    }
    OpenWindow('/tell_a_friend/index.cfm?url='+a,'tellafriend','600',maxheight);
  }
</script>

<script src="/js/openwindow.js"></script>				


<script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=bookbrowse">
	var addthis_config = {"data_track_addressbar":false};
	var addthis_share = {url_transforms : {clean: true, remove: ['fb_xd_fragment'] } };
</script>


