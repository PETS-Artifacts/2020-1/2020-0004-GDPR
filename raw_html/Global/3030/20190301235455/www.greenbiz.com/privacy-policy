<!DOCTYPE html>
<html lang="en" dir="ltr" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#" xmlns:article="http://ogp.me/ns/article#" xmlns:book="http://ogp.me/ns/book#" xmlns:profile="http://ogp.me/ns/profile#" xmlns:video="http://ogp.me/ns/video#" xmlns:product="http://ogp.me/ns/product#">
<head profile="http://www.w3.org/1999/xhtml/vocab">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<script type="text/javascript">dataLayer = [{"entityType":"node","entityBundle":"gbz_content_page","entityId":"100048","entityLabel":"GreenBiz Group Privacy Policy","entityLanguage":"und","entityTnid":"0","entityVid":"207072","entityName":"GreenBiz Editors","entityUid":"47","entityCreated":"1389385320","entityStatus":"1","drupalLanguage":"en","userUid":0}];</script>
<link rel="shortcut icon" href="/sites/all/themes/greenbiz/favicon/favicon.ico" type="image/vnd.microsoft.icon">
<link rel="apple-touch-icon" href="/sites/all/themes/greenbiz/favicon/apple-touch-icon.png">
<link rel="icon" href="/sites/all/themes/greenbiz/favicon/favicon.png">
<!--[if IE]><link rel="shortcut icon" href="/sites/all/themes/greenbiz/favicon/favicon.ico"><![endif]-->

<meta name="msapplication-TileColor" content="#f00">
<meta name="msapplication-TileImage" content="/sites/all/themes/greenbiz/favicon/tileicon.png">
<meta name="description" content="Effective August 5, 2015We built GreenBiz (http://greenbiz.com, or “GreenBiz” or the “Website” or the “Site”) to help share news, information, and opinion on green business practices. This Privacy Policy (the “Policy”) is designed to help you understand how we collect and use personal information, and how we handle privacy issues." />
<link rel="canonical" href="https://www.greenbiz.com/privacy-policy" />
<meta name="generator" content="Drupal 7 (http://drupal.org)" />
<meta name="rights" content="© 2019 GreenBiz Group" />
<link rel="shortlink" href="https://www.greenbiz.com/node/100048" />
<meta property="fb:admins" content="GreenBiz" />
<meta property="og:site_name" content="GreenBiz" />
<meta property="og:type" content="article" />
<meta property="og:title" content="GreenBiz Group Privacy Policy" />
<meta property="og:url" content="https://www.greenbiz.com/privacy-policy" />
<meta property="og:description" content="Effective August 5, 2015We built GreenBiz (http://greenbiz.com, or “GreenBiz” or the “Website” or the “Site”) to help share news, information, and opinion on green business practices. This Privacy Policy (the “Policy”) is designed to help you understand how we collect and use personal information, and how we handle privacy issues." />
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@greenbiz" />
<meta name="twitter:creator" content="@greenbiz" />
<meta name="twitter:site:id" content="15942016" />
<meta name="twitter:title" content="GreenBiz Group Privacy Policy" />
<meta name="twitter:url" content="https://www.greenbiz.com/privacy-policy" />
<meta name="twitter:description" content="Effective August 5, 2015We built GreenBiz (http://greenbiz.com, or “GreenBiz” or the “Website” or the “Site”) to help share news, information, and opinion on green business practices. This Privacy" />
<meta name="dcterms.title" content="GreenBiz Group Privacy Policy" />
<meta name="dcterms.creator" content="GreenBiz Editors" />
<meta name="dcterms.description" content="Effective August 5, 2015We built GreenBiz (http://greenbiz.com, or “GreenBiz” or the “Website” or the “Site”) to help share news, information, and opinion on green business practices. This Privacy Policy (the “Policy”) is designed to help you understand how we collect and use personal information, and how we handle privacy issues." />
<meta name="dcterms.contributor" content="https://www.greenbiz.com/users/greenbiz-staff" />
<meta name="dcterms.publisher" content="GreenBiz Group Inc." />
<meta name="dcterms.date" content="2014-01-10T12:22-08:00" />
<meta name="dcterms.type" content="Text" />
<meta name="dcterms.format" content="text/html" />
<meta name="dcterms.identifier" content="https://www.greenbiz.com/privacy-policy" />
<meta name="dcterms.rights" content="© 2019 GreenBiz Group" />

<meta content="width=device-width" name="viewport" />
<meta http-equiv="cleartype" content="on" />
<title>GreenBiz Group Privacy Policy | GreenBiz</title>
<link type="text/css" rel="stylesheet" href="https://www.greenbiz.com/sites/default/files/css/css_kShW4RPmRstZ3SpIC-ZvVGNFVAi0WEMuCnI0ZkYIaFw.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.greenbiz.com/sites/default/files/css/css_ntCz7udh66prM85dlibL5cSl16uR5mFkmPYLsA2b56k.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.greenbiz.com/sites/default/files/css/css_VUmaJT9n6nWPCDZAn2yCH0o4t6TGmRY9UGKO_pLq11o.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.greenbiz.com/sites/default/files/css/css_MzPNibo56O3ooq5bT8ygN4m_UIOEDq_7sLuopErqR3c.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.greenbiz.com/sites/default/files/css/css_pKfOTUhE0APf05HQskVzBKxRR6oXVrBiA9Sbo81Ns8M.css" media="all" />
<link type="text/css" rel="stylesheet" href="https://www.greenbiz.com/sites/default/files/css/css_lufOSdy7ix6xpC0ITNhJgri0U8KjSigBIi3FMcRn1z4.css" media="screen" />

<script type="text/javascript">
<!--//--><![CDATA[//><!--
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0];var j=d.createElement(s);var dl=l!='dataLayer'?'&l='+l:'';j.type='text/javascript';j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl+'';j.async=true;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MD8PC4J');
//--><!]]>
</script>
<script type="text/javascript" src="https://www.greenbiz.com/sites/default/files/js/js_RwPo9pdWYsPo6QBG9K_qLfMHycqTPCC-VbqDpX-PEPg.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
googletag.slots = googletag.slots || {};
//--><!]]>
</script>
<script type="text/javascript" src="https://www.googletagservices.com/tag/js/gpt.js"></script>
<script type="text/javascript" src="https://www.greenbiz.com/sites/default/files/js/js_GdOZkAUImFuNDqKcbp--_lngcclDc06RA4kNgTxv7Os.js"></script>
<script type="text/javascript" src="https://www.greenbiz.com/sites/default/files/js/js_CwA9uD2kYGc1ubylea-Ul8iN5PWYS40GWBA4ue9Bo4w.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
document.createElement( "picture" );
//--><!]]>
</script>
<script type="text/javascript" src="https://www.greenbiz.com/sites/default/files/js/js_qRPb9xAJz7ePOmYUM2rF8MaWSmc65txkC8QXsjrpip4.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
googletag.cmd.push(function() {
  googletag.pubads().enableAsyncRendering();
  googletag.pubads().enableSingleRequest();
  googletag.pubads().collapseEmptyDivs();
  googletag.pubads().disableInitialLoad();
  googletag.pubads().setTargeting("path", ["node","100048"]);
});

googletag.enableServices();
//--><!]]>
</script>
<script type="text/javascript" src="https://www.greenbiz.com/sites/default/files/js/js_zezREeoMrPFe86tAnYSvMD5c_KFDbzJg1ZpCK39eGWw.js"></script>
<script type="text/javascript" src="https://www.greenbiz.com/sites/default/files/js/js_WM-JfCh0jdxdZ03wA21UpmrT1wTWme9kwvNYY6ywzOg.js"></script>
<script type="text/javascript">
<!--//--><![CDATA[//><!--
jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"greenbiz","theme_token":"Sxyt2SRIEyd5fQm9VEPzSxNswr_AuVqQJhtQnHC_-Pw","css":{"modules\/system\/system.base.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"misc\/ui\/jquery.ui.theme.css":1,"misc\/ui\/jquery.ui.accordion.css":1,"profiles\/panopoly\/modules\/contrib\/date\/date_api\/date.css":1,"profiles\/panopoly\/modules\/contrib\/date\/date_popup\/themes\/datepicker.1.7.css":1,"modules\/field\/theme\/field.css":1,"sites\/all\/modules\/custom\/gbz_layouts\/gbz_layouts.css":1,"modules\/node\/node.css":1,"sites\/all\/modules\/contrib\/picture\/picture_wysiwyg.css":1,"profiles\/panopoly\/modules\/contrib\/radix_layouts\/radix_layouts.css":1,"modules\/user\/user.css":1,"profiles\/panopoly\/modules\/contrib\/views\/css\/views.css":1,"profiles\/panopoly\/modules\/contrib\/caption_filter\/caption-filter.css":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/plain\/colorbox_style.css":1,"profiles\/panopoly\/modules\/contrib\/ctools\/css\/ctools.css":1,"profiles\/panopoly\/modules\/contrib\/panels\/css\/panels.css":1,"sites\/all\/themes\/greenbiz\/layouts\/page\/two-col-content-first\/two-col-content-first.css":1,"https:\/\/netdna.bootstrapcdn.com\/font-awesome\/4.1.0\/css\/font-awesome.min.css":1,"sites\/all\/themes\/greenbiz\/css\/admin.css":1,"sites\/default\/files\/css\/follow.css":1,"sites\/all\/themes\/kalatheme\/css\/tweaks.css":1,"sites\/all\/themes\/greenbiz\/css\/main.css":1},"js":{"sites\/all\/modules\/contrib\/picture\/picturefill2\/picturefill.min.js":1,"sites\/all\/modules\/contrib\/picture\/picture.min.js":1,"profiles\/panopoly\/modules\/panopoly\/panopoly_widgets\/panopoly-widgets.js":1,"profiles\/panopoly\/modules\/panopoly\/panopoly_widgets\/panopoly-widgets-spotlight.js":1,"0":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/jquery\/1.12\/jquery.min.js":1,"misc\/jquery.once.js":1,"1":1,"https:\/\/www.googletagservices.com\/tag\/js\/gpt.js":1,"misc\/drupal.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/ui\/external\/jquery.cookie.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.core.min.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.widget.min.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.tabs.min.js":1,"sites\/all\/modules\/contrib\/jquery_update\/replace\/ui\/ui\/minified\/jquery.ui.accordion.min.js":1,"2":1,"sites\/all\/modules\/features\/gbz_ads\/js\/gbz_ads_render.js":1,"sites\/all\/modules\/contrib\/marketo_ma\/js\/marketo_ma.js":1,"sites\/all\/modules\/contrib\/media_colorbox\/media_colorbox.js":1,"sites\/all\/modules\/custom\/toggle_summary\/toggle-summary.min.js":1,"profiles\/panopoly\/modules\/contrib\/caption_filter\/js\/caption-filter.js":1,"3":1,"sites\/all\/libraries\/colorbox\/jquery.colorbox-min.js":1,"sites\/all\/modules\/contrib\/colorbox\/js\/colorbox.js":1,"sites\/all\/modules\/contrib\/colorbox\/styles\/plain\/colorbox_style.js":1,"sites\/all\/modules\/contrib\/colorbox\/js\/colorbox_load.js":1,"profiles\/panopoly\/libraries\/jquery.imagesloaded\/jquery.imagesloaded.min.js":1,"sites\/all\/modules\/contrib\/datalayer\/datalayer.js":1,"sites\/all\/themes\/greenbiz\/js\/dist\/greenbiz-polyfills.pkg.min.js":1,"sites\/all\/themes\/greenbiz\/js\/dist\/greenbiz.pkg.js":1}},"colorbox":{"opacity":"0.85","current":"{current} of {total}","previous":"\u00ab Prev","next":"Next \u00bb","close":"Close","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"CToolsModal":{"modalSize":{"type":"scale","width":".9","height":".9","addWidth":0,"addHeight":0,"contentRight":25,"contentBottom":75},"modalOptions":{"opacity":".55","background-color":"#FFF"},"animationSpeed":"fast","modalTheme":"CToolsModalDialog","throbberTheme":"CToolsModalThrobber"},"panopoly_magic":{"pane_add_preview_mode":"automatic"},"marketo_ma":{"track":true,"key":"211-NJY-165","library":"\/\/munchkin.marketo.net\/munchkin.js"},"dataLayer":{"languages":{"en":{"language":"en","name":"English","native":"English","direction":0,"enabled":1,"plurals":0,"formula":"","domain":"","prefix":"","weight":0,"javascript":""}}}});
//--><!]]>
</script>

</head>
<body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-100048 node-type-gbz-content-page region-content">
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MD8PC4J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <div id="page-wrapper"><div id="page">

<header class='navbar navbar-default gbz-header-main'>
<div class='header-main-container clearfix'>
<h1 id="logo" class="brand navbar-brand">
<a href="/" title="Home" rel="home">
<img class="img-responsive" aria-hidden="true" src="https://www.greenbiz.com/sites/all/themes/greenbiz/logo.png" alt="GreenBiz Group Privacy Policy" width="239px" height="44px"></span>
<span class="sr-only element-invisible">Home</span>
</a>
</h1>
<nav id="nav-container" role="navigation">
<h2 class="sr-only">Main menu</h2><ul class="nav navbar-nav links clearfix" id="main-menu-links" role="menubar"><li class="mid-4116 first" role="presentation"><a href="/insights" class="first leaf" role="menuitem">Insights</a></li>
<li class="mid-4117" role="presentation"><a href="/events" class="leaf" role="menuitem">Events</a></li>
<li class="mid-2115 last" role="presentation"><a href="/video" class="last leaf" role="menuitem">Videos</a></li>
</ul> <h2 class="sr-only">Secondary menu</h2><ul id="secondary-menu-links" class="nav navbar-nav secondary-links collapse navbar-collapse"><li class="menu-4312 first " role="presentation" id="" style=""><a href="/collections/circular-economy">Circular Economy</a></li>
<li class="menu-1737" role="presentation"><a href="/collections/energy-climate" title="">Energy</a></li>
<li class="menu-4310" role="presentation"><a href="/collections/transportation">Transportation</a></li>
<li class="menu-4306" role="presentation"><a href="/collections/sustainability">Sustainability</a></li>
<li class="menu-4311" role="presentation"><a href="/collections/supply-chain">Supply Chain</a></li>
<li class="menu-4308 " role="presentation" id="" style=""><a href="/collections/cities">Cities</a></li>
<li class="menu-4307" role="presentation"><a href="/collections/buildings-facilities">Buildings</a></li>
<li class="menu-4309 last" role="presentation"><a href="/collections/water">Water</a></li>
</ul> </nav>
<nav id="gbz-header-icons" class="gbz-header-icons">
<div class="social drawer">
<a class="actuator">
<span aria-hidden="true"></span>
<span class="sr-only">Greenbiz on Social Media</span>
</a>
<div class='follow-links clearfix site'><div class='follow-link-wrapper follow-link-wrapper-twitter'><a href="http://new.greenbiz.com/twitter" class="" title="Follow GreenBiz on Twitter"><span class="fa fa-twitter" aria-hidden="true"></span><span class="sr-only">Twitter</span></a>
</div><div class='follow-link-wrapper follow-link-wrapper-facebook'><a href="https://www.facebook.com/GreenBiz" class="" title="Follow GreenBiz on Facebook"><span class="fa fa-facebook" aria-hidden="true"></span><span class="sr-only">Facebook</span></a>
</div><div class='follow-link-wrapper follow-link-wrapper-googleplus'><a href="https://plus.google.com/" class="" title="Follow GreenBiz on Google+"><span class="fa fa-google-plus" aria-hidden="true"></span><span class="sr-only">Google+</span></a>
</div><div class='follow-link-wrapper follow-link-wrapper-linkedin'><a href="https://www.linkedin.com/groups?gid=664267" class="" title="Follow GreenBiz on LinkedIn"><span class="fa fa-linkedin" aria-hidden="true"></span><span class="sr-only">LinkedIn</span></a>
</div><div class='follow-link-wrapper follow-link-wrapper-this-site'><a href="/rss.xml" class="" title="Follow GreenBiz on This site (RSS)"><span class="fa fa-rss" aria-hidden="true"></span><span class="sr-only">This site (RSS)</span></a>
</div></div> </div>
<a href="/search" class="actuator" id="search-actuator-sm">
<span aria-hidden="true"></span>
 <span class="sr-only">Search</span>
</a>
<div id="gbz-search-drawer" class="search drawer" data-submitonclose="true">
<div id="gbz-search-container">
<form action="/search" method="get" id="greenbiz-search-form" accept-charset="UTF-8" role="form"><div><div class="form-item form-type-textfield form-item-query form-group"><input class="form-control form-text fa-on" type="text" id="edit-query" name="query" value="" size="20" maxlength="128" placeholder="Enter search terms…"></div><input name="" type="submit" id="edit-submit" value="Search" class="form-submit btn btn-default btn-primary" placeholder="Enter search terms…"></div></form> </div>
<a href="/search" class="actuator">
<span aria-hidden="true"></span>
<span class="sr-only">Search</span>
</a>
</div>
</nav>
<button id="hamburger-menu" class="navbar-toggle" type="button" data-toggle="collapse" data-target="#secondary-menu-links">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
</header>

<div id="main-wrapper" class="clearfix"><div id="main" class="clearfix">
<div id="content" class="container">
<a id="main-content"></a>
<div class="region region-content">
<div id="block-system-main" class="block block-system">
<div class="content">
<section class="content-main layout-two-col-content-first panel-display">
<div class="columns-content-main columns-two-col-content-first clearfix">
<section class="gbz-content-main">
<div class="panel-panel ">
<div class="panel-panel-inner">
<div class="panel-pane pane-node-title">
<div class="pane-content">
<h1>GreenBiz Group Privacy Policy</h1>
</div>
</div>
<div class="panel-pane pane-entity-field pane-node-body">
<div class="pane-content">
<div class="field field-name-body field-type-text-with-summary field-label-hidden"><div class="field-items"><div class="field-item even"><p><em>Effective August 5, 2015</em></p><p>We built GreenBiz (<a href="http://greenbiz.com">http://greenbiz.com</a>, or “GreenBiz” or the “Website” or the “Site”) to help share news, information, and opinion on green business practices. This Privacy Policy (the “Policy”) is designed to help you understand how we collect and use personal information, and how we handle privacy issues.</p><h2>Changes to our Privacy Policy</h2><p>We reserve the right to modify this Policy at any time without notice, so please review it frequently. The date at the top of this page indicates when this Policy was last revised. This Policy applies to all information that we have about you, and to your account with us. If we decide to change this Policy, we will post those changes to this privacy statement and other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. If we make material changes to this Policy, we will notify you here, by email, or by means of a prominent notice when you log into the Site.</p><h2>The Information We Collect</h2><p>When you visit GreenBiz, you provide us with two types of information: A) personal information (such as your name and email address) that you have voluntarily disclosed to us, and B) non-personally identifiable information that is generated when you interact with the Website.</p><h3>A) Personal Information</h3><p>When you register with GreenBiz, you provide us with your name, e-mail address, and time zone. GreenBiz stores this information to track the success of our referral program. We may occasionally use your name and email address to send you notifications regarding new services offered by GreenBiz that we think you may find valuable. We will send you our newsletter if you opt to receive this communication from GreenBiz. If you wish to unsubscribe from promotional emails including newsletters, you may do so by following the unsubscribe instructions included in each promotional email. GreenBiz may send you service-related announcements from time to time through the general operation of the service. GreenBiz may use information in your profile without identifying you as an individual to third parties. We do this for purposes such as personalizing advertisements and promotions. We believe this benefits you because it allows you to know about products and services that are complementary to the services provided by GreenBiz and that may be of interest to you.</p><h4>Testimonials</h4><p>From time to time, we may post testimonials on our Website. We receive permission to post testimonials that include personally identifiable information prior to posting.</p><h4>Surveys</h4><p>From time to time, we may provide you the opportunity to participate in surveys on our Website. If you participate, we may request certain personally identifiable information from you. Participation in these surveys is completely voluntary and you therefore have a choice whether or not to disclose this information. The requested information typically includes contact information (such as name and business address), and demographic information (such as zip code). We use this information to monitor site traffic or personalize the Website.</p><h4>Service Providers</h4><p>We use third parties to provide various services on our behalf such as an email service provider and credit card processor. When you sign up to receive our newsletter we will share your email address with our email service provider. When you subscribe to one of our services on the site, we use a credit card processor to store your credit card details in a secure location and to charge your credit card when payments are due. Third parties are prohibited from using your personally identifiable information for any other purpose including their own marketing.</p><h4>Legal Disclaimer</h4><p>We reserve the right to disclose your personally identifiable information as required by law and when we believe that disclosure is necessary to protect our rights and/or to comply with a judicial proceeding, court order, or legal process served on our Website.</p><h3>B) Non-Personally Identifiable Information / Cookies</h3><p>When you enter GreenBiz, we collect non-personally-identifiable information including IP address, profile information, aggregate user data, and browser type, from users and visitors to the Website. This data is used to manage the Website, track usage and improve the Website services. This non-personally-identifiable information may be shared with third parties to provide more relevant services and advertisements to members. User IP addresses are recorded for security and monitoring purposes. In addition, we store certain information from your browser using “cookies.” A cookie is a piece of data stored on the user’s computer tied to information about the user. We use session ID cookies to store visitors’ preferences and to record session information. These cookies terminate once the user closes the browser. By default, we use a persistent cookie that stores your login ID (but not your password) to make it easier for you to login when you come back to GreenBiz. You may be able to configure your browser to accept or reject all or some cookies, or notify you when a cookie is set and each browser is different, so check the “Help” menu of your browser to learn how to change your cookie preferences. However, you must enable cookies from GreenBiz in order to use most functions on the Website. Our cookies are not tied to personally identifiable information. Please note that GreenBiz allows third-party advertisers that are presenting advertisements on some of our pages to set and access their cookies on your computer, and that by using GreenBiz, you consent to the setting and accessing of such cookies on your computer. Advertisers use of cookies is subject to their own privacy policies, not this Policy.</p><h2>Third Party Advertising</h2><p>Ads appearing on this Website may be delivered to users by GreenBiz or one of our Web advertising partners. Our Web advertising partners may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement. In this way, ad servers may compile information about where you, or others who are using your computer, saw their advertisements and determine which ads are clicked on. This information allows an ad network to deliver targeted advertisements that they believe will be of most interest to you. This Policy covers the use of cookies by GreenBiz only and does not cover the use of cookies by any advertisers. We have no access or control of these third party cookies.</p><h2>Links to Other Sites</h2><p>This Website contains links to other sites that are not owned or controlled by GreenBiz. Please be aware that we, GreenBiz, are not responsible for the privacy practices of such other sites. We encourage you to be aware when you leave our site and to read the privacy statements of each and every website that collects personally identifiable information. This privacy statement applies only to information collected by this Website.</p><h2>Changing or Removing Information</h2><p>Access and control over most personal information on GreenBiz is readily available through the profile editing tools. You may modify or delete any of your profile information at any time by logging into your account. Information will be updated as soon as our technology allows. Individuals who wish to deactivate their GreenBiz account may do so on the Account page. Removed information may persist in backup copies for a reasonable period of time but will not be generally available. You can also request GreenBiz remove your account, by sending an email to <a href="/cdn-cgi/l/email-protection#066f686069466174636368646f7c2865696b"><span class="__cf_email__" data-cfemail="4920272f26092e3b2c2c272b2033672a2624">[email&#160;protected]</span></a>.</p><h2>Security</h2><p>GreenBiz takes appropriate precautions to protect our users’ information. Your account information is located on a secured server behind a firewall. When we collect sensitive information such as your credit card, we use SSL encryption to protect this data. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its absolute security. Because email and instant messaging are not recognized as secure communications, we request that you not send private information to us by e-mail or instant messaging services.</p><h2>How to Contact Us</h2><p>Should you have any questions or concerns about the security on our Website or these privacy policies, please email us at <a href="/cdn-cgi/l/email-protection#5d34333b321d3a2f3838333f3427733e3230"><span class="__cf_email__" data-cfemail="b5dcdbd3daf5d2c7d0d0dbd7dccf9bd6dad8">[email&#160;protected]</span></a> or contact us at the following address: Attention: Client Services, GreenBiz Group, 350 Frank H. Ogawa Plaza, Suite 800, Oakland, CA, 94612.</p></div></div></div> </div>
</div>
<div class="panel-pane pane-node-links link-wrapper">
<div class="pane-content">
</div>
</div>
</div>
</div>
</section>
<aside class="sidebars" id="sidebars">
<div class="panel-panel">
<div class="panel-panel-inner">
</div>
</div>
</aside>
</div>
</section>
</div>
</div>
</div>
</div> 
</div></div> 

<footer class='section gbz-footer-main' id='footerlower' role='contentinfo'>
<div class="footer-links-primary">
<div class='container'>
<div class='row'>
<nav role="navigation">
<h2 class="element-invisible">Footer menu 1</h2><ul class="nav nav-pills nav-stacked" role="menubar"><li class="mid-2474 first" role="presentation"><a href="/about-us" class="first leaf" role="menuitem">About Us</a></li>
<li class="mid-2458" role="presentation"><a href="/our-team" class="leaf" role="menuitem">GreenBiz Team</a></li>
<li class="mid-50235" role="presentation"><a href="/editorial-team" title="" class="leaf" role="menuitem">Editorial Team</a></li>
<li class="mid-4345" role="presentation"><a href="/media-kit" title="Request a media kit" class="leaf" role="menuitem">Media Kit</a></li>
<li class="mid-46539 last" role="presentation"><a href="/contact-us" class="last leaf" role="menuitem">Contact Us</a></li>
</ul> </nav>
<nav role="navigation">
<h2 class="element-invisible">Footer menu 2</h2><ul class="nav nav-pills nav-stacked" role="menubar"><li class="mid-2462 first" role="presentation"><a href="/executive-network" title="Executive Network" class="first leaf" role="menuitem">Executive Network</a></li>
<li class="mid-2470" role="presentation"><a href="/reports" class="leaf" role="menuitem">Research Reports</a></li>
<li class="mid-2469" role="presentation"><a href="/white-papers" class="leaf" role="menuitem">White Papers</a></li>
<li class="mid-4536 last" role="presentation"><a href="https://www.greenbiz.com/events/webcasts" title="Upcoming and archived GreenBiz webcasts" class="last leaf" role="menuitem">Webcasts</a></li>
</ul> </nav>
<nav role="navigation">
<h2 class="element-invisible">Footer menu 3</h2><ul class="nav nav-pills nav-stacked" role="menubar"><li class="mid-49345 first" role="presentation"><a href="https://www.greenbiz.com/newsletters-subscribe" class="first leaf" role="menuitem">Newsletters</a></li>
<li class="mid-50234" role="presentation"><a href="https://www.greenbiz.com/350" class="leaf" role="menuitem">GreenBiz 350 Podcast</a></li>
<li class="mid-52909" role="presentation"><a href="https://www.greenbiz.com/greenbiz-careers" class="leaf" role="menuitem">GreenBiz Careers</a></li>
<li class="mid-26034 last" role="presentation"><a href="http://jobs.greenbiz.com" class="last leaf" role="menuitem">Jobs</a></li>
</ul> </nav>
<div class="social_links">
<div class='follow-links clearfix site'><div class='follow-link-wrapper follow-link-wrapper-twitter'><a href="http://new.greenbiz.com/twitter" class="" title="Follow GreenBiz on Twitter"><span class="fa fa-twitter" aria-hidden="true"></span><span class="sr-only">Twitter</span></a>
</div><div class='follow-link-wrapper follow-link-wrapper-facebook'><a href="https://www.facebook.com/GreenBiz" class="" title="Follow GreenBiz on Facebook"><span class="fa fa-facebook" aria-hidden="true"></span><span class="sr-only">Facebook</span></a>
</div><div class='follow-link-wrapper follow-link-wrapper-googleplus'><a href="https://plus.google.com/" class="" title="Follow GreenBiz on Google+"><span class="fa fa-google-plus" aria-hidden="true"></span><span class="sr-only">Google+</span></a>
</div><div class='follow-link-wrapper follow-link-wrapper-linkedin'><a href="https://www.linkedin.com/groups?gid=664267" class="" title="Follow GreenBiz on LinkedIn"><span class="fa fa-linkedin" aria-hidden="true"></span><span class="sr-only">LinkedIn</span></a>
</div><div class='follow-link-wrapper follow-link-wrapper-this-site'><a href="/rss.xml" class="" title="Follow GreenBiz on This site (RSS)"><span class="fa fa-rss" aria-hidden="true"></span><span class="sr-only">This site (RSS)</span></a>
</div></div> </div>
</div>
</div>
</div>
<div class="footer-links-secondary">
<div class='container clearfix'>
<div class="secondary-links col-md-7">
<h2 class="element-invisible">Secondary Footer menu</h2><ul class="footer-links list-unstyled list-inline" role="menubar"><li class="mid-2464 first last active" role="presentation"><a href="/privacy-policy" class="first last leaf active" role="menuitem">Privacy Policy</a></li>
</ul> </div>
<div class="col-xs-12 col-md-5">
<div class="footer-message">
&copy; 2019 GreenBiz Group Inc. GREENBIZ&reg; and GREENBIZ.COM&reg; is a registered trademark of <a href="http://www.greenbizgroup.com/" target="_blank">GreenBiz Group Inc</a>. </div>
</div>
</div>
</div>
</footer>
</div></div> 
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript" src="https://www.greenbiz.com/sites/default/files/js/js_7Ukqb3ierdBEL0eowfOKzTkNu-Le97OPm-UqTS5NENU.js"></script>
<script type="text/javascript" src="https://www.greenbiz.com/sites/default/files/js/js_29qYXJz8NLGg8Aomg-RZPjJcj9yEdEst1BMZ9gZbs-4.js"></script>

</body>
</html>
