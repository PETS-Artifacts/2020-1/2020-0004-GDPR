<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>eUttaranchal - Privacy Policy</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="keywords" content="eUttaranchal, Uttaranchal Community, euttranchal, euttranchal.com, About eUttaranchal">
<meta name="description" content="Privacy Policy of eUttaranchal. Updated Terms of Use and Privacy Policy.">
<script src='https://www.google.com/recaptcha/api.js'></script>
<style>
.content{border:none !important; box-shadow:none !important;}
</style>
<script src="https://assets.euttaranchal.com/_js/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
<link rel="canonical" href="https://www.euttaranchal.com/about_us/privacy.php" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto:300,400,500,700|Roboto+Condensed:400,300|Raleway:300,400,700|Quicksand:300,500,700" rel="stylesheet" type="text/css">
<meta name=viewport content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/tourism/assets/bootstrap.min.css">
<script src="/tourism/assets/bootstrap.min.js"></script>
<link href="/tourism/assets/style.css?v=22.11.2018" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://assets.euttaranchal.com/_js/bootstrap-datepicker.css">
<script type="text/javascript" src="https://assets.euttaranchal.com/_js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://assets.euttaranchal.com/_js/jquery.unveil.js"></script>
</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" rightmargin="0" bottommargin="0">

<header>
		<nav class="navbar navbar-default" id="global-sitetop-nav">
				  <div class="container">
					<div class="navbar-header">
					   <button type="button" class="navbar-toggle collapsed" data-toggle="slide-collapse" data-target="#slide-navbar-collapse" aria-expanded="false" id="global-menu-trigger">
						<span class="sr-only">Toggle</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					  <a class="navbar-brand" href="https://www.euttaranchal.com" alt="eUttaranchal" title="eUttaranchal Home"><img src="https://www.euttaranchal.com/_imgs/eua-logo-small.png" /></a>
					</div>
					
					<div id="slide-navbar-collapse" class="navbar-collapse collapse">
					  <ul class="nav navbar-nav navbar-right">
						<li class="border-bottom visible-sm visible-xs text-center mobile-menu-logo"><img src="https://www.euttaranchal.com/_imgs/eua-logo-small.png" /> <i class="fa fa-window-close" id="close-global-nav"></i></li>
						<li class="border-bottom"><a href="https://www.euttaranchal.com/matrimonials/">Matrimonial</a></li>
						<li class="dropdown border-bottom">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Uttarakhand Tourism <i class="fa fa-angle-down"></i></a>
						  <ul class="dropdown-menu">
							<li><a href="/tourism/hill-stations-in-uttarakhand.php">Hill Stations</a></li>
							<li><a href="/tourism/wildlife-tours-uttarakhand.php">Wildlife</a></li>
							<li><a href="/tourism/trekking-tours-uttarakhand.php">Trekking</a></li>
							<li><a href="/tourism/pilgrimage-tours-uttarakhand.php">Pilgrimage</a></li>
							<li><a href="http://chardham.euttaranchal.com" target="_blank">Char Dham</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="/hotels/" target="_blank">Hotels</a></li>
							<li><a href="/packages/" target="_blank">Tour Packages</a></li>
						  </ul>
						</li>
						<li class="border-bottom"><a href="https://www.euttaranchal.com/education/">Education</a></li>
						<li class="border-bottom"><a href="https://www.euttaranchal.com/culture/">Culture</a></li>
						<li class="border-bottom"><a href="https://www.euttaranchal.com/uttarakhand/">Uttarakhand</a></li>
						<li class="border-bottom"><a href="https://govt-jobs.euttaranchal.com">Govt Jobs</a></li>
						<li class="border-bottom"><a href="https://www.euttaranchal.com/about_us/contact_us.php">Contact</a></li>
					  </ul>
					</div><!--/.nav-collapse -->
				  </div>
		</nav>
		<div class="menu-overlay"></div>
</header>
<div class="wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-9 white-fill">
			  <div class="content">
				  <h1 class="heading1">Privacy Policy</h1>
				  <p>The Privacy Policies laid down below are general and applicable throughout the website http://www.euttaranchal.com</p>
				  <p>To read the specific <em><strong><a href="http://www.euttaranchal.com/matrimonials/register/privacy-policy.php">Privacy Policies for our Matrimonial Service</a></strong></em>, please <a href="http://www.euttaranchal.com/matrimonials/register/privacy-policy.php">click here</a></p>
				  <p class="heading3"><strong>Information Collection and Use</strong></p>
				  <ul>
                    <li> eUttaranchal collects personal information when you register with eUttaranchal  or its network partners, when you use eUttaranchal services, when you visit  eUttaranchal pages and when you enter promotions. eUttaranchal may combine  information about you that we have with information we obtain from business  partners or other companies. <br>
                        <br>
                    </li>
				    <li> When  you register we ask for information such as your name, email address, birth  date, gender, zip code, occupation, industry, and personal interests. For some  financial products and services we may also ask for your address and  information about your assets. Once you register with eUttaranchal and sign in  to our services, you are not anonymous to us. <br>
                        <br>
                    </li>
				    <li> eUttaranchal  automatically receives and records information on our server logs from your  browser, including your IP address, eUttaranchal cookie information, and the  page you request. <br>
                        <br>
                    </li>
				    <li> eUttaranchal  uses information for the following general purposes: to customize the  advertising and content you see, fulfill your requests for products and  services, improve our services, contact you, conduct research, and provide  anonymous reporting for internal and external clients.</li>
			    </ul>
				  <h3 class="heading3"> Information Sharing and Disclosure</h3>
				  <ul type="disc">
                    <li>eUttaranchal does not rent,       sell, or share personal information about you with other people or       nonaffiliated companies except to provide products or services you've       requested, when we have your permission, or under the following       circumstances: </li>
				    <ul type="circle">
                      <li>eUttaranchal has partnered with trusted agents and parties for various services. Your personal details may be shared with our trusted partners who may contact you to provide products or services you've       requested at eUttaranchal. </li>
				      <li>We believe it is        necessary to share information in order to investigate, prevent, or take        action regarding illegal activities, suspected fraud, situations        involving potential threats to the physical safety of any person,        violations of eUttaranchal&rsquo;s terms of use, or as otherwise required by        law. </li>
				      <li>We transfer        information about you if eUttaranchal is acquired by or merged with        another company. In this event, eUttaranchal! will notify you before information        about you is transferred and becomes subject to a different privacy        policy.<br>
                          <br>
                      </li>
			        </ul>
				    <li>eUttaranchal displays       targeted advertisements based on personal information. Advertisers       (including ad serving companies) may assume that people who interact with,       view, or click on targeted ads meet the targeting criteria.<br>
                        <br>
                    </li>
				    <li>eUttaranchal does not        provide any personal information to the advertiser when you interact with        or view a targeted ad. However, by interacting with or viewing an ad you        are consenting to the possibility that the advertiser will make the        assumption that you meet the targeting criteria used to display the ad. </li>
			    </ul>
				  <h3 class="heading3">Cookies</h3>
				  <ul type="disc">
                    <li>eUttaranchal may set and       access  cookies on your computer. </li>
				    <li>eUttaranchal lets other       companies/advertisers that show advertisements on some of our pages set       and access their cookies on your computer. Other companies' use of their       cookies is subject to their own privacy policies, not this one.       Advertisers or other companies do not have access to eUttaranchal's       cookies. </li>
			    </ul>
				  <h3 class="heading3">Your Account Information  and Preferences</h3>
				  <p><strong>General</strong></p>
				  <ul type="disc">
                    <li>You can edit your       eUttaranchal Account Information at any time. </li>
				    <li>We reserve the right to send       you certain communications relating to the eUttaranchal service, such as       service announcements, administrative messages and the Newsletter, that       are considered part of your eUttaranchal account, without offering you the       opportunity to opt-out of receiving them. </li>
			    </ul>
				  <p class="heading3"><strong>Changes to this Privacy Policy</strong></p>
				  <ul type="disc">
                    <li>eUttaranchal may update this       policy at any time. We will notify you about significant changes in the       way we treat personal information by sending a notice to the primary email       address specified in your eUttaranchal account or by placing a prominent       notice on our site. </li>
			    </ul>
				  <p>This website (http://www.euttaranchal.com) is the owned and operated by <strong>DeeBee Web Services Pvt Ltd</strong>. </p>
				  <p>Should you have further queries about our privacy policy, please contact our  support team at <a href="mailto:care@eutn.in">care@eutn.in</a></p>
			  </div>
			</div><!--#col-9 //-->
			
			<div class="col-md-3">
				<div class="content white-fill">
					<ul class="list-unstyled">
						<li><a href="/about_us/">About Us</a></li>
						<li><a href="/about_us/euttaranchal-goals.php">Goals & Objectives</a></li>
						<li><a href="/about_us/careers.php">Jobs</a></li>
						<li><a href="/about_us/internship.php">Internship</a></li>
						<li><a href="/about_us/advertise.php">Advertise</a></li>
						<li><a href="/about_us/contact_us.php">Contact</a></li>
					</ul>				</div>
			</div><!--#col-3 //-->
		</div><!--#row //-->
	</div>
	
</div>

<div class="clear min-height"></div>

<script language="javascript">
$('[data-toggle="slide-collapse"]').on('click', function() {
  $navMenuCont = $($(this).data('target'));
  $navMenuCont.animate({
	'width': 'toggle'
  }, 350);
  $(".menu-overlay").fadeIn(500);

});
$(".menu-overlay").click(function(event) {
  $("#global-menu-trigger").trigger("click");
  $(".menu-overlay").fadeOut(500);
});

$("#close-global-nav").click(function(event) {
  $("#global-menu-trigger").trigger("click");
  $(".menu-overlay").fadeOut(500);
});


$('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
    	next = $(this).siblings(':first');
  	}
    
    next.children(':first-child').clone().appendTo($(this));
  }
});

</script>

<div class="wrap global-footer">
	<div class="container">
		
		<div class="row footer-row">
			<div class="col-md-3">
				<div id="footer-logo"><a href="https://www.euttaranchal.com"><img src="/tourism/_imgs/logo-white-s.png" class="img-responsive" border="0" title="eUttaranchal" alt="eUttaranchal" /></a></div>
				<ul class="list-unstyled">
					<li><a href="https://chardham.euttaranchal.com" target="_blank">Char Dham Yatra</a></li>
					<li><a href="https://www.sacredyatra.com" target="_blank">Sacred Yatra</a></li>
					<li><a href="https://kumbh-mela.euttaranchal.com" target="_blank">Kumbh Mela</a></li>
					<li><a href="https://www.chopta.in" target="_blank">Chopta Tungnath</a></li>
					<li><a href="https://www.euttarakhand.com" target="_blank">Uttarakhand Stories</a></li>
					<li><a href="https://www.culturaltrends.in/product-cat/books/travel" target="_blank">Uttarakhand Travel Books</a></li>
				</ul>
			</div>
			
			<div class="col-md-3">
				<h4>eUttaranchal</h4>
				<ul class="list-unstyled">
					
					<li><a href="https://www.euttaranchal.com">Uttarakhand</a></li>
					<li><a href="https://www.euttaranchal.com/tourism/">Uttarakhand Tourism</a></li>
					<li><a href="https://www.euttaranchal.com/packages/">Uttarakhand Tour Packages</a></li>
					<li><a href="https://www.euttaranchal.com/hotels/">Uttarakhand Hotels</a></li>
					<li><a href="https://news.euttaranchal.com">Uttarakhand News</a></li>
					<li><a href="https://www.euttaranchal.com/about_us/">About Us</a></li>
					</ul>
			</div>
			
			<div class="col-md-3">
				<h4>Customer Service</h4>
				<ul class="list-unstyled">
					<li><a href="https://www.euttaranchal.com/travel-bookings-terms-conditions">Return, Cancellation & Refund</a></li>
					<li><a href="https://www.euttaranchal.com/about_us/privacy.php">Privacy Policy</a></li>
					<li><a href="https://www.euttaranchal.com/about_us/tos.php">Terms of Use</a></li>
					<li><a href="/tourism/contact.php">Contact Us</a></li>
				</ul>
			</div>
			
			<div class="col-md-3">
				<h4>Business Connect</h4>
				<ul class="list-unstyled">
					<li><a href="/tourism/contact.php">Contact Us</a></li>
					<li><a href="/tourism/contact.php?s=list-package">List your Package</a></li>
					<li><a href="https://www.euttaranchal.com/hotels/add-a-hotel.php">List your Hotel</a></li>
					<li><a href="/tourism/contact.php?s=feedback">Send Feedback</a></li>
				</ul>
			</div>
			
		</div>
	</div>
</div>


<div class="wrap copyright-container">
	<div class="container">
		
		<div class="row">
				<div class="text-center copyright col-md-12">eUttaranchal is promoting <a href="https://www.euttaranchal.com/tourism/">Travel and Tourism of Uttarakhand</a> since 2003. Our motto is "Marketing Uttarakhand Worldwide."</div>
		</div>
		
		<div class="row">
		  <div class="col-md-12 text-center">
				<div class="social-icons">
					<ul class="list-unstyled list-inline">
						<li><a href="https://www.facebook.com/euttaranchal" target="_blank" class="fb"></a></li>
						<li><a href="https://www.instagram.com/euttaranchal/" target="_blank" class="insta"></a></li>
						<li><a href="https://twitter.com/uttarakhand" target="_blank" class="twitter"></a></li>
						<li><a href="https://plus.google.com/+euttaranchal" target="_blank" class="gplus"></a></li>
						<li><a href="https://www.youtube.com/c/euttaranchal" target="_blank" class="ytube"></a></li>
					</ul>
				</div>
			 	
				<div class="copyright">All rights reserved. &copy; 2018-2019 <a href="https://www.euttaranchal.com">eUttaranchal.com</a><br />
</div>

<div class="bottom-desc">eUttaranchal.com is <strong>NOT</strong> an Official website of Government of Uttarakhand (Uttaranchal) neither is it associated with Uttarakhand Tourism Board or <a href="https://www.euttaranchal.com/tourism/kmvn.php">KMVN</a> or <a href="https://www.euttaranchal.com/tourism/gmvn.php">GMVN</a>. Besides <a href="https://www.euttaranchal.com/matrimonials/">Uttarakhand Matrimonial</a> and <a href="https://www.euttaranchal.com/tourism/">Uttarakhand Tourism</a> services, eUttaranchal is also a mini encyclopedia of Uttarakhand providing information about <a href="https://www.euttaranchal.com/culture/">Culture</a>, Tourism, <a href="https://www.euttaranchal.com/education">Education</a>, Politics and <a href="https://news.euttaranchal.com">News</a> about <a href="https://www.euttaranchal.com">Uttarakhand</a>.</div>
			</div>
		</div>
	</div>
</div>


<script language="javascript">
    $('#date-picker').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
		startDate: "today",
        todayHighlight: true
    });
	
	$('#date-picker-sidebar').datepicker({
        format: "dd/mm/yyyy",
        autoclose: true,
		startDate: "today",
        todayHighlight: true
    });
	
	$("a.inpage-link").on('click', function(e) {

	   // prevent default anchor click behavior
	   e.preventDefault();
	
	   // store hash
	   var hash = this.hash;
	
	   // animate
	   $('html, body').animate({
		   scrollTop: $(hash).offset().top
		 }, 300, function(){
	
		   // when done, add hash to url
		   // (default click behaviour)
		   window.location.hash = hash;
		 });

	});
	
	//Last Child Selector
	$(".suggest-box li:last-child").addClass("no-border-bottom");
	
	
$(document).ready(function(){	
	$(".pts-photo img, #list-attractions-bottom img, #list-attractions-inline img, #featured-photo-sidebar-lazyload img, .featured-photo img, .dt-photo img, .pic-container-dlist img, .ht-photo img, .p-gallery img").unveil(200, function() {
	  $(this).load(function() {
		this.style.opacity = 1;
	  });
	});
});
</script>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-66141-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-66141-1');
</script>
</body>
</html>
