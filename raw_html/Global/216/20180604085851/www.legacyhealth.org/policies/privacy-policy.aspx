
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <!-- Start Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || []; w[l].push({
                    'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
                }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                        '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-NSSFDB');</script>
    <!-- End Google Tag Manager -->
    
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="language" content="English">
        <title>
            Privacy Policy</title>
        
        <meta name="description" content="">
        <meta name="keywords" content="" />
        <meta property="og:title" content="Privacy Policy" />
        <meta property="og:description" content="" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="" />
        <meta property="og:url" content="http://www.legacyhealth.org/policies/privacy-policy.aspx" />
        <meta property="og:site_name" content="Legacy Health" />

        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        


        <link rel="stylesheet" href="/media/css/style.css?v=7.2.7">

        <link rel="stylesheet" type="text/css" media="screen and (max-width: 944px)" href="/media/css/break1.css?v=7.2.7" />
        <link rel="stylesheet" type="text/css" media="screen and (max-width: 707px)" href="/media/css/break2.css?v=7.2.7" />
        <link rel="stylesheet" type="text/css" media="screen and (max-width: 452px)" href="/media/css/break3.css?v=7.2.7" />
        <!--[if IE]><link rel="stylesheet" type="text/css" href="/media/css/fontsIE.css?v=7.2.7" /><![endif]-->
        <!--[if !IE]><link rel="stylesheet" type="text/css" href="/media/css/fonts.css?v=7.2.7" /><<![endif]-->

        <link rel="stylesheet" type="text/css" href="/media/css/tmp-orbit.css?v=7.2.7" media="screen" />
        <link rel="stylesheet" type="text/css" href="/media/css/tmp-podcast.css?v=7.2.7" media="screen" />

        <link rel="stylesheet" type="text/css" href="/media/css/print.css?v=7.2.7" media="print" />
        <link rel="stylesheet" media="screen" type="text/css" href="/media/css/styles.css?v=7.2.7" />

        <link rel="canonical" href="http://www.legacyhealth.org/policies/privacy-policy.aspx" />


        <script src="/media/js/libs/modernizr-2.5.3.min.js"></script>

        
        <script>window.twttr = (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                t = window.twttr || {};
                if (d.getElementById(id)) return t;
                js = d.createElement(s);
                js.id = id;
                js.src = "https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);

                t._e = [];
                t.ready = function (f) {
                    t._e.push(f);
                };

                return t;
            }(document, "script", "twitter-wjs"));</script>
        

        <!-- <PageMap>
        <DataObject type="thumbnail"></DataObject>
        </PageMap> -->
    <title>

</title></head>
<body>

    <!-- Should always be located before Google Tag Manager code -->
    <script>
     
    </script>

    
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-NSSFDB"
            height="0" width="0" style="display: none; visibility: hidden"></iframe>
    </noscript>

    <div id="fb-root"></div>
    <script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=182774688449398";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <form method="post" action="/policies/privacy-policy.aspx" id="fForm">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="+BZgzCtlXK44HSw00F8BTsM0de9C/FEYBJ8jbZhIs3NzMvvj3cRfwco5smblZoJaFarGBVS4P4S0w1hUk1rMBYDUUHy1BD/9omgmYxpr6apMbzbNlalWJIGvhcifYiEMcYAe+lO2gUlIsjPXK1OXtKjrzCG+7b4qncezjIKTE6U/r31PS2Q3G78g+GMHpu3akYwqhMO8iuDl0CYrCeU3ZJRZHiF7mJB8FyY1KWG8kWlUIPAy+HsEprDUPonycFCfy5DmHVjTxl/DnPti3upwA8rQQ7yEyeWvifAXJlFOZz8Elln6Gmui1Q3T3DhD4qkCtZlnGSmLWbk4UuOHwBjtQokv0Y0rva5v6Qdzb0yY00iD0eLnXpnPjFsUirpm852IQSspP+Ofqp7fg2Cc3gRmkbrX5U0/cpLYmShLFCMmy4KxpF4GftV58zt4cfMLpW56aRel/Yngb4xvbmHJ9rRP5hJAtM0LA/1rKZWzXNGj1Ihyud95YB6iGtCoPx11zr1GDNftB1UpzFBNUvAPSLxhvg3tTafuGFaGhhFYYlGoAozKTMYZFaWRqa2la9ozAdUwgt7Y79urlYNlTY7KHdhkcAKoXXxKf9KhY+qhFaT3+UWuXqj9FaabkKhlM5wLD5UjXFrfb8R2y0lrld07UYEYjw8YdsKbC1Xyj31M9HikNP/sAYQm8WTXwspVRmAXAzaoBTirJMrdiuSeZBJAWprlDq4fw84s3om0ECNisr4mj+bry9xot5Ja9rolE2/AAQqgsE0yK1L6gVFI9TQAK65myBCIKnMMOOcpyeiQZ8JVDDiqqAc91of/Tc6FxLBtuBodM2kcKEHiAYBOFKRthvuyGkvdkPdKpXIOH/JINLTDXkUr37d4VG+ekQ90cQDSVbbet0Iiqgb6chHD7UFm8A0fbEiBZ4DG+SsKqc/U+pX+z8arUejtNi8JRVTeivj0OHFRjyzPf4em/NrsRo6Rc3zQbEfpBSBWU2IJcgA8IcQc0F/gplYFEXDFhSChNvD+yZ3z4Rt2EAkfgsvtUQSvLCXX74azyA0J1QgZaH7WfDtqI3lhcPkCdp1E8CwUfU5NL2CwLsV/vFmeiVmLgNZ+tkpc393kW89dvCDmNkE/5T8kZd+njsK02Rc7xQRpu2cgRNW6F+BjClAumT6ADsYDsGQauAKraTIkHCWKZTs7SMdSB3cYfaMTKtYY4ywPbOWS5yaV/8sEobgA7KjqEKdnb+iwhMOS8wjSMocd++z8+GNkPQ3yN/KgZZWx3Wer7ItLDJkhuNkgrY+HKO93Pb8WUSxmJXgGLQWPGem9puPRhj+QPe3I5oJwdq09JeO4oX8cKwTw+wzjoBNXyNvH/sTQlDnm377Y+rwKskJyms/EIvSXaymyDbOE6eUQvN8sKSKh6w1Y+G60pF0TEOSMFkW6tuY+Sm2ROJiLXQsDepHazGt3yyP7B6WxvOsm9vdVb+bUd9QQ31uy9JzqAEm44TiWG6RYusWsiy9pg5GQJ5mOpg9BN5WiRk+zI7SYG8B0D279IAFEfJyE2mJcq9K/WU3MEvorftc8jS+Y5gJ77nqDCkzoLYUhbMybEZXB515+6hb17jwvaxnWBhg2fAOx+qzLmceBwAuF5ocomKlprV98GeREkt+4v/STkUqvMjPOsHmMJwYRUonGgOw8IWsYJEfBz0ek4VmWp86htw11B6auwhgIulSN79IrlcKjQ9FZoMLDjHj5wYtygvpSD6BjwUf3zp68A20Rxhly9EyklTAgElm2VLdXltAxmhh/eY3nQ3ucbFSnNo+oVotmWghjy8rJvY8X3e1w/UxKkJ7ZuLvO6Y9rTvsZGwYCiX4oHBODDZ8rURw9em5+u2vDUXzxErHncBXbREe2CNO/Bqfh3ofGg/RyiXULBfxnrRl8JJZbjLCmvzFZaHlzc9J87xR2yke6MX4vrqFdYCMzRvWWWEXKT8HMXYMLb4P9KqdlGzF0Qi3guBFSFya2SdBLyFx0KXlS7jlfnqQ+dOxHAbKoSGdMP9ldl/4G1wtIBMtDO9j4VZ0E9pjbhsfZfRuiM+4XwNeGhJJXc/Vcvd/DGNgWnhtgyYXDJhsRcXKbe7m+AgukL4iIvKNvDyZ4SlU53lged88WEhTY5m9QgnD0TGRH7ygv/yskQpU5Qg9fD5dn2wuc4Y6cVVIVK43m70bBcCHw+TWzzFqEnr0YPnGuCZ1sNBWpC3VWtsUwBnREZvvNKSHMocvM4WnZBkcGwkKhACbg/GVNgd2W6Oez3K2H2ebEhgrDYzKuSz/FmPjQremgdypqsW2l7o6SK+9Bh+5EOvK+s0vZfmhQa+NYUd60DEf3rxe4vAoJb0N+GmKFYJbDXBZKTakvl4pBWe9NlrkFqEtzk9OU6K8c6lk8oG3Webf17PpSSHXHDNm2TkNUs0le5A99xPqZjI+OzTxto03mKi1wlVcR6X5ygo+5LSbhsYkeMxlSU3iDOvS6BxsfTvFShHoNvaoWFP3Rj7O8aHKYGryy3qziQESwzPF6l7y2flzgsCofTvCoslNaaIihdsJHFqzyWFRTyRN+hF6qkbemBT2TyryZjsl9pp3V3TbZ9d1dm/traZFBOVdPncAzidIFcUcCzvDZUBis7URqY+eI7oWPi5betwlU9AoIZxZQkP0b+VIn2X9o8jP5WKO7BWS4ci3zzZR+lSkM53PQ7CFGRO0untzCwme7u0KOkA6TusAVFdcDgW7OudoFuPjgh/AvEGDEnGiQDIT6GasVro2xS9Ngw2vLvObfckgyr6u6XJCaMnqFhSRy8lFihKjLOX/1VIEr34vhA+2BFBGT0Ui5BE0/3ESizaF4acNBJmQ9S5rB87MV0KO13OwEBOKj2v17sUdFkY6m5znYvOgDSObRfOER3U5ieCVMSLa6cSY1CmsfaJNSdXQaxbnnKf5Aj9+T5Bj/LHVKo2axL7Y0FTSTS88bfUdifOAZaqa13SFncyYAZ7Ome5otWL/6hovuJ5hx9gurJcP+af+hD3XhtlYWG57Zb/P4N2y2WNsjFc56pHEeyNYrFSzjvavwka7IJIk/Kfjb/HvD5DnjnmcLeKTvK1lYgOrC72IfK33n9zTTNX0Jxxaxedwileq3bak9VZPvs5B7DyUqbJl5Ujk5T3dWn4tzXxvdk8TRhK0xHR8lYlJWcLN6Gaw/iTqNujr2YrQJnYWL1GzVD3fhj3i+SOFFC3zaIP8J1KtZIluYm6pcu8m0eInZBYNwC6Xy8+U0lOFj2hIbf1MeVT8+8e1vyGLEoS3SdVaEQr/kuuPDSykyZYO0rDH6OsZ9YhGbgFdzJtAbD9KWFbgHqS+J/f729bbqmqyXYr+7liQtnxEi+8IlOeGJKCb5zaAQUhBdW0H4Gq3BV/Gpw/qytiUEUSfwITWb+xdx920dZrIZYuZRUacVaPBb2L18gjg1Em8zNaVBHf7CvBPkOIFLarVsHU6fkRhfxMTSn+lqnJD+PuzYcVJnj6UYWQ7nADDlz1CAom57w9cmAzQiJ2SAHmh0HhVwnohoH8S29S8P5napCosM/gjCA9dQNSwYnKEMIVb7gvU7844LUrHppfXbDWaEHFf1c9OvcmXwWhSlog8uu8jFGs2v1cN1y/X4u3RmTb3xVHGVhIwojtA7curN5KuFTd4Ssvjztn12x0HaHvBuUK8ljgUJ5K/DTCqMLcEl2IRFjjje99j1u2zOBqDVXofrnbN1xbkkTgPGDhbptxhEJoG9ZO6KslLM4tvsma/kA54KKm1DD8C+WfGERS1oJGK4yzVEE9EDnyda67d0HIu0nj5bPySG4/X0Hya9jP/XMWDdB/fO7Z2q8I9ydrh0K5Xa0xdUSkw+jeCetpA43e1d4FROdLOtWetfX9mczKker8rNY+YIyfv+5XAChm0ULUqCINXuWzmc10EMnuQr+Jq/qWaoyIGQLN2jT6Uy8U3f17xDBnXqH16g+dBH7XMMGt+ck3xnh+2qN0QX+0XKUfGO4ckATkfKN9GxBCXeFYZ3vDRwjUT6D3hDWvNq/2YdvxOani4t5SY/n//9U17eYX4wd0WnuBYnue/guV/NORP5UwuHEmltXg0UVZOZhCw2qf8WtgRCB4INAn1XiRN3AOFU1nZvZ/ArefCBsld1N2t9a72c7v/vzOGoP3eMbZcf4d8PTge+GXNplQJWSD55ysafmyE1ZJOBsAq9uuoYmeabk0Ba1eInydVslDXR1PxTA7SzdEvzOgxGR8Wd610QA0u8vDwH8mJU3TxzEbkcAGzG0o4F2gnHHunXDjb3fLqyB9dlV32J5vUOIsrZCHM43ToSvZRMrMUE90D9UXdRfwERfjRpa7LZh6qjFpNFq0hyDWREnPtC8XEZCHZ6GK+BhkHJspGr6s9TYOjMYD/ZV8IEDdJl58S6J6sAbBh8C/pAhi4bTZFdm2AXec94ph9kjltA/Bl09VVhG0nZL/xouUOzdU9O8lynoFedvr1ud7jPiA2x1J/6vCmCl7OIK8wL2R82ZvYJYwx5llhgh8BoxUqLIQJEedikeI/bumEcTnfCqEbe5BsYtOHRiIfEM/hmtV6sxJ0SSVf4KY/D6JFMzNP1X8R8jW1Fwj+Ji4RevehEcuxwI4GD8rbq3CcfmxVnx9E+ZYPoqPEcn463EimUCZI9xrvHexaufPR4jWfn7Ca4qyZbTRf+A4JGHtdYoWFWyyLnuZsqdu/UVIJrPdUjrqejsimJZQl3mGlf5wW2cAxMpiXHPHTryW1sS0t7gXbJAXQLn6fYhLVflltTDCvbMDBmYCV5jDkEYZDxxhYRto/DU6l9UVVGTl1V74pm9x6FimRSY8jxIWQh1B8zu+30W5K6EyHWd6UWbjLkBqp2GbdGZHm+J7f97ETLNaGyfopAB6fR0iCEXj8uqM7+SNwiKpy4LBzdwmeRgjdqBWBc2N+MZGSGTk+UB5xc8PIEcZomN7QuykdUDRmxZSv6MilJhmArp/PM/PMYbuAObb+VczvmjxoYbZ0f1DFvRpXlfpmc1tbQCr8UoA7xnkJKX6k36HD7W22G94480ICtOa6LvGzaHGj+A0klbbehY7fXWheByGeYTHHABppRIrLTBh3c9CaYkWM=" />
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="EB5877AD" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="xvR69/Be+6rwPJ6kY9yiXVlqbNzT1zZ2+Q3aEZ+sdDmwlg/aShZI4ILn/LCU8w+gxXtRaSRspVHP87GSC/UTk9ECw375lArDJ4QvSsHFT45vRdNTjxvMIeYJK/uSmPXcRVks6qjAE5YpBM8ZkOHn6NBA/hj0DB90S0eu93hKXxqMjyYw1ivpUmsTUK0Teyr3" />
</div>
        <a name="top"></a>
        
<header class="top-header">
    <div class="w12 mobileFullWidth">       
        <div id="headerTop">
			<div class="container legacy-health-redesign-2015">
                <div class="row header-row-form-container">
                    <div class="col-xs-6 col-sm-5 col-md-7">
						<a href="/" class="headerLogo">
                            <img src="/-/media/Images/Logos/Header.png?la=en" alt="Legacy Health" />
						</a>
					</div>
                    
                    <div class="col-xs-2 col-sm-4 col-md-3 search-container">					
                        <button id="bMobileSearch" type="button" class="btn btn-default button-container search-toggle show-on-mobile">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                        </button>
					</div>

					<div class="col-xs-4 col-sm-3 col-md-2 my-health-container">
						<div class="my-health-toggle">
							<a id="ctl03_rLinks_hLink_0" data-title="My Health" class="my-health-cta">MyHealth <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></a>
							<div class="my-health-form-container">
                                <p>Manage your account, request prescriptions, set up appointments &amp; more.</p>
                                <a class="more-stories-button" href="https://myhealth.lhs.org/myhealth/" target="_blank">LOG IN</a>
								<div class="row">
									<div class="col-xs-6">
                                        <span>Don&#39;t have an account</span>
									</div>
									<div class="col-xs-6 my-health-create-account-link">
                                        <a class="my-health-link" href="https://myhealth.lhs.org/myhealth/accesscheck.asp" target="_blank">CREATE AN ACCOUNT ></a>
									</div>
								</div>
							</div>
						</div>
                        <a class="contact-us-link hide-on-mobile" href="/contact-us.aspx">Contact Us</a>
					</div>

					<div class="input-group search-input-group">
	                    <input name="ctl04$tSearch" type="text" id="ctl04_tSearch" class="form-control search-input" placeholder="Search" />  
						<span class="input-group-btn">						
                            <button onclick="__doPostBack('ctl04$bSearch','')" id="ctl04_bSearch" class="btn btn-default button-container search-button autoenter">
                                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            </button>                            
						</span>
					</div>
                </div>
			</div>
            <div id="menuButtons">
				<div class="inner closedResBtn closed" id="menuButton">
					<span class="text">Menu</span>
					<span class="overlay"></span>
				</div>
			</div>
        </div>
    </div>
    <div id="headerMenus">
        <div class="w12">            
            <ul id="megaMenu">
                
                        <li id="ctl04_rMegaMenuHeadings_lMegamenu_0" data-sub-mega-menu-id="subMegaMenu0">
                            <span class="shadow"></span>
                            <span class="overlap"></span>
                            <a id="ctl04_rMegaMenuHeadings_hLink_0" class="ga-mega-menu mega-menu-label" href="/find-a-provider.aspx" data-title="find-a-provider">Find a provider</a>
                        </li>
                    
                        <li id="ctl04_rMegaMenuHeadings_lMegamenu_1" data-sub-mega-menu-id="subMegaMenu1">
                            <span class="shadow"></span>
                            <span class="overlap"></span>
                            <a id="ctl04_rMegaMenuHeadings_hLink_1" class="ga-mega-menu mega-menu-label" href="/locations.aspx" data-title="locations">Our Locations</a>
                        </li>
                    
                        <li id="ctl04_rMegaMenuHeadings_lMegamenu_2" data-sub-mega-menu-id="subMegaMenu2">
                            <span class="shadow"></span>
                            <span class="overlap"></span>
                            <a id="ctl04_rMegaMenuHeadings_hLink_2" class="ga-mega-menu mega-menu-label" href="/health-services-and-information.aspx" data-title="health-services-and-information">Health Services &amp; Information</a>
                        </li>
                    
                        <li id="ctl04_rMegaMenuHeadings_lMegamenu_3" data-sub-mega-menu-id="subMegaMenu3">
                            <span class="shadow"></span>
                            <span class="overlap"></span>
                            <a id="ctl04_rMegaMenuHeadings_hLink_3" class="ga-mega-menu mega-menu-label" href="/for-patients-and-visitors.aspx" data-title="for-patients-and-visitors">For Patients &amp; Visitors</a>
                        </li>
                    
                        <li id="ctl04_rMegaMenuHeadings_lMegamenu_4" data-sub-mega-menu-id="subMegaMenu4">
                            <span class="shadow"></span>
                            <span class="overlap"></span>
                            <a id="ctl04_rMegaMenuHeadings_hLink_4" class="ga-mega-menu mega-menu-label" href="/giving-and-support.aspx" data-title="giving-and-support">Giving &amp; Support</a>
                        </li>
                    
                        <li id="ctl04_rMegaMenuHeadings_lMegamenu_5" data-sub-mega-menu-id="subMegaMenu5">
                            <span class="shadow"></span>
                            <span class="overlap"></span>
                            <a id="ctl04_rMegaMenuHeadings_hLink_5" class="ga-mega-menu mega-menu-label" href="/our-legacy.aspx" data-title="our-legacy">Our Legacy</a>
                        </li>
                    
                        <li id="ctl04_rMegaMenuHeadings_lMegamenu_6" data-sub-mega-menu-id="subMegaMenu6">
                            <span class="shadow"></span>
                            <span class="overlap"></span>
                            <a id="ctl04_rMegaMenuHeadings_hLink_6" class="ga-mega-menu mega-menu-label" href="/for-health-professionals.aspx" data-title="for-health-professionals">For Health Professionals</a>
                        </li>
                    
            </ul>            
        </div>
    </div>
    <div id="subMegaMenus">
        
                <div id="subMegaMenu0" class="subMegaMenu">
	
                    <div class="subMegaMenuBody clearfix">
                        
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_0_hLink_0" class="ga-mega-menu" href="/find-a-provider.aspx" data-title="provider-information">Provider Information</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_0_rSubNavigation_0_hLink_0" class="ga-mega-menu" href="/find-a-provider.aspx" data-title="search-by-last-name">Search by Last Name</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_0_rSubNavigation_0_hLink_1" class="ga-mega-menu" href="/find-a-provider.aspx" data-title="search-by-location">Search by Location</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_0_rSubNavigation_0_hLink_2" class="ga-mega-menu" href="/find-a-provider.aspx" data-title="search-by-keyword">Search by Keyword</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                    </div>
                
</div>
            
                <div id="subMegaMenu1" class="subMegaMenu">
	
                    <div class="subMegaMenuBody clearfix">
                        
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_1_hLink_0" class="ga-mega-menu" href="/locations/hospitals.aspx" data-title="hospitals">Hospitals</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_0_hLink_0" class="ga-mega-menu" href="/locations/hospitals/legacy-emanuel-medical-center.aspx" data-title="legacy-emanuel-medical-center">Legacy Emanuel Medical Center</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_0_hLink_1" class="ga-mega-menu" href="/locations/hospitals/legacy-good-samaritan-medical-center.aspx" data-title="legacy-good-samaritan-medical-center">Legacy Good Samaritan Medical Center</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_0_hLink_2" class="ga-mega-menu" href="/locations/hospitals/legacy-meridian-park-medical-center.aspx" data-title="legacy-meridian-park-medical-center">Legacy Meridian Park Medical Center </a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_0_hLink_3" class="ga-mega-menu" href="/locations/hospitals/legacy-mount-hood-medical-center.aspx" data-title="legacy-mount-hood-medical-center">Legacy Mount Hood Medical Center</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_0_hLink_4" class="ga-mega-menu" href="/locations/hospitals/legacy-salmon-creek-medical-center.aspx" data-title="legacy-salmon-creek-medical-center">Legacy Salmon Creek Medical Center</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_0_hLink_5" class="ga-mega-menu" href="/locations/hospitals/legacy-silverton-medical-center.aspx" data-title="legacy-silverton-medical-center">Legacy Silverton Medical Center</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_0_hLink_6" class="ga-mega-menu" href="/locations/hospitals/randall-childrens-hospital-at-legacy-emanuel.aspx" data-title="randall-childrens-hospital-at-legacy-emanuel">Randall Children&#39;s Hospital at Legacy Emanuel </a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_1_hLink_1" class="ga-mega-menu" href="/locations/clinics.aspx" data-title="clinics">Clinics</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_1_hLink_0" class="ga-mega-menu" href="/locations/clinics/primary-care-clinics.aspx" data-title="primary-care-clinics">Primary Care Clinics</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_1_hLink_1" class="ga-mega-menu" href="/locations/clinics/specialty-clinics.aspx" data-title="specialty-clinics">Specialty Clinics</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_1_hLink_2" class="ga-mega-menu" href="/locations/clinics/urgent-care.aspx" data-title="urgent-care">Urgent Care </a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_1_hLink_2" class="ga-mega-menu" href="/locations/other-locations.aspx" data-title="other-locations">Other Legacy Locations</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_2_hLink_0" class="ga-mega-menu" href="/locations/other-locations/laboratory-patient-centers.aspx" data-title="laboratory-patient-centers">Laboratory Patient Centers</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_2_hLink_1" class="ga-mega-menu" href="/locations/other-locations/drug-testing-sites.aspx" data-title="drug-testing-sites">Drug Testing Sites</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_2_hLink_2" class="ga-mega-menu" href="/locations/other-locations/imaging-locations.aspx" data-title="imaging-locations">Imaging Locations</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_2_hLink_3" class="ga-mega-menu" href="/locations/other-locations/physical-therapy-and-rehab-locations.aspx" data-title="physical-therapy-and-rehab-locations">Physical Therapy &amp; Rehab Locations</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_2_hLink_4" class="ga-mega-menu" href="/locations/other-locations/hopewell-house-hospice.aspx" data-title="hopewell-house-hospice">Hopewell House Hospice </a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_2_hLink_5" class="ga-mega-menu" href="/locations/other-locations/business-health-services.aspx" data-title="business-health-services">Business Health Services</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_1_rSubNavigation_2_hLink_6" class="ga-mega-menu" href="/locations/other-locations/wellspring-conference-center.aspx" data-title="wellspring-conference-center">Wellspring Conference Center</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                    </div>
                
</div>
            
                <div id="subMegaMenu2" class="subMegaMenu">
	
                    <div class="subMegaMenuBody clearfix">
                        
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_2_hLink_0" class="ga-mega-menu" href="/health-services-and-information/health-services.aspx" data-title="health-services">Health Services </a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_0_hLink_0" class="ga-mega-menu" href="/health-services-and-information/health-services/for-adults-a-z.aspx" data-title="for-adults-a-z">For Adults A-Z</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_0_hLink_1" class="ga-mega-menu" href="/health-services-and-information/health-services/for-children-a-z.aspx" data-title="for-children-a-z">For Children A-Z</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_2_hLink_1" class="ga-mega-menu" href="/health-services-and-information/wellness-and-prevention.aspx" data-title="wellness-and-prevention">Wellness and Prevention</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_1_hLink_0" class="ga-mega-menu" href="/health-services-and-information/wellness-and-prevention/adult-health-screenings.aspx" data-title="adult-health-screenings">Adult Health Screenings</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_1_hLink_1" class="ga-mega-menu" href="/health-services-and-information/wellness-and-prevention/adult-immunization-schedule.aspx" data-title="adult-immunization-schedule">Adult Immunization Schedule</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_1_hLink_2" class="ga-mega-menu" href="/health-services-and-information/wellness-and-prevention/flu-info.aspx" data-title="flu-info">Flu Info</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_1_hLink_3" class="ga-mega-menu" href="/health-services-and-information/wellness-and-prevention/fitmonth-play-for-an-hour-a-day.aspx" data-title="fitmonth-play-for-an-hour-a-day">Kids: Let&#39;s get moving</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_1_hLink_4" class="ga-mega-menu" href="/health-services-and-information/wellness-and-prevention/trying-to-quit-smoking.aspx" data-title="trying-to-quit-smoking">Trying to Quit Smoking?</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_1_hLink_5" class="ga-mega-menu" href="/health-services-and-information/wellness-and-prevention/your-primary-care-physician.aspx" data-title="your-primary-care-physician">Your Primary Care Physician</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_2_hLink_2" class="ga-mega-menu" href="/health-services-and-information/trauma-nurses-talk-tough-tntt.aspx" data-title="trauma-nurses-talk-tough-tntt">Trauma Nurses Talk Tough - TNTT</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_2_hLink_0" class="ga-mega-menu" href="/health-services-and-information/trauma-nurses-talk-tough-tntt/content-modules.aspx" data-title="content-modules"></a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_2_hLink_1" class="ga-mega-menu" href="/health-services-and-information/trauma-nurses-talk-tough-tntt/court-ordered-classes.aspx" data-title="court-ordered-classes">Court-ordered classes</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_2_hLink_2" class="ga-mega-menu" href="/health-services-and-information/trauma-nurses-talk-tough-tntt/family-education-programs.aspx" data-title="family-education-programs">Family Education Programs</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_2_hLink_3" class="ga-mega-menu" href="/health-services-and-information/trauma-nurses-talk-tough-tntt/school-programs.aspx" data-title="school-programs">School Programs</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_2_hLink_3" class="ga-mega-menu" href="/health-services-and-information/health-resources.aspx" data-title="health-resources">Health Resources</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_3_hLink_0" class="ga-mega-menu" href="/health-services-and-information/health-resources/phone-apps.aspx" data-title="phone-apps">Phone Apps</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_2_hLink_4" class="pMMHeader" href="/health-services-and-information/health-library.aspx" data-title="health-library">Health Library</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_4_hLink_0" class="pMMSubHeader" href="http://legacyhealth.staywellsolutionsonline.com/" data-title="sub-header" target="_blank">Browse topics. Watch videos. Get recipes. Take quizzes. Read articles.</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_2_rSubNavigation_4_hLink_1" class="pMMCTA" href="http://legacyhealth.staywellsolutionsonline.com/" data-title="explore-cta" target="_blank">Explore</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                    </div>
                
</div>
            
                <div id="subMegaMenu3" class="subMegaMenu">
	
                    <div class="subMegaMenuBody clearfix">
                        
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_3_hLink_0" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information.aspx" data-title="patient-information">Patient Information</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_0" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/admission-patient-access.aspx" data-title="admission-patient-access">Admission/Patient access</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_1" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/your-hospital-care.aspx" data-title="your-hospital-care">Your Hospital Care</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_2" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/patient-and-family-engagement.aspx" data-title="patient-and-family-engagement">Patient and Family Engagement</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_3" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/patient-privacy.aspx" data-title="patient-privacy">Patient Privacy</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_4" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/discharge-information.aspx" data-title="discharge-information">Discharge Information</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_5" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/billing-information.aspx" data-title="billing-information">Billing Information</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_6" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/get-medical-records.aspx" data-title="get-medical-records">How to order medical records</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_7" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/interpreter-services.aspx" data-title="interpreter-services">Interpreter Services</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_0_hLink_8" class="ga-mega-menu" href="/for-patients-and-visitors/patient-information/servicio-de-interpretes-medicos.aspx" data-title="servicio-de-interpretes-medicos">Servicio de int&#233;rpretes m&#233;dicos</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_3_hLink_1" class="ga-mega-menu" href="/for-patients-and-visitors/visitor-information.aspx" data-title="visitor-information">Visitor Information</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_1_hLink_0" class="ga-mega-menu" href="/for-patients-and-visitors/visitor-information/parking.aspx" data-title="parking">Parking</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_1_hLink_1" class="ga-mega-menu" href="/for-patients-and-visitors/visitor-information/gift-shops.aspx" data-title="gift-shops">Gift Shops</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_1_hLink_2" class="ga-mega-menu" href="/for-patients-and-visitors/visitor-information/lodging.aspx" data-title="lodging">Lodging</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_1_hLink_3" class="ga-mega-menu" href="/for-patients-and-visitors/visitor-information/pharmacies.aspx" data-title="pharmacies">Pharmacies</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_1_hLink_4" class="ga-mega-menu" href="/for-patients-and-visitors/visitor-information/tobacco-faq.aspx" data-title="tobacco-faq">Tobacco FAQ</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_3_hLink_2" class="ga-mega-menu" href="/for-patients-and-visitors/general-information.aspx" data-title="general-information">General Information</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_2_hLink_0" class="ga-mega-menu" href="/for-patients-and-visitors/general-information/volunteering.aspx" data-title="volunteering">Volunteering</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_2_hLink_1" class="ga-mega-menu" href="/for-patients-and-visitors/general-information/health-exchange.aspx" data-title="health-exchange">Health exchange</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_2_hLink_2" class="ga-mega-menu" href="/for-patients-and-visitors/general-information/rights-and-responsibilities.aspx" data-title="rights-and-responsibilities">Rights and Responsibilities</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_2_hLink_3" class="ga-mega-menu" href="/for-patients-and-visitors/general-information/spiritual-care.aspx" data-title="spiritual-care">Spiritual Care</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_2_hLink_4" class="ga-mega-menu" href="/for-patients-and-visitors/general-information/clinical-ethics-program.aspx" data-title="clinical-ethics-program">Clinical Ethics Program</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_2_hLink_5" class="ga-mega-menu" href="/for-patients-and-visitors/general-information/coded-and-anonymous-genetics-research.aspx" data-title="coded-and-anonymous-genetics-research">Coded and Anonymous Genetics Research</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_3_rSubNavigation_2_hLink_6" class="ga-mega-menu" href="/for-patients-and-visitors/general-information/medicare-important-notice.aspx" data-title="medicare-important-notice">Medicare - Important Notice</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                    </div>
                
</div>
            
                <div id="subMegaMenu4" class="subMegaMenu">
	
                    <div class="subMegaMenuBody clearfix">
                        
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_4_hLink_0" class="pMMHeader" href="/giving-and-support.aspx" data-title="donate-to-legacy">Donate to Legacy</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_0_hLink_0" class="pMMSubHeader" href="/giving-and-support.aspx" data-title="giving-sub-header">Support the region&#39;s largest nonprofit, locally owned health care provider</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_0_hLink_1" class="pMMCTA" href="/giving-and-support.aspx" data-title="explore-giving">Explore Giving</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_4_hLink_1" class="ga-mega-menu" href="/giving-and-support/giving-to-legacy.aspx" data-title="giving-to-legacy">Giving to Legacy</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_1_hLink_0" class="ga-mega-menu" href="/giving-and-support/giving-to-legacy/ways-to-give.aspx" data-title="ways-to-give">Ways to Give</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_1_hLink_1" class="ga-mega-menu" href="/giving-and-support/giving-to-legacy/care-champions.aspx" data-title="care-champions">Honor Excellent Care</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_1_hLink_2" class="ga-mega-menu" href="http://legacyhealth.planmygift.org/" data-title="planned-giving" target="_blank">Gift Planning</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_1_hLink_3" class="ga-mega-menu" href="/giving-and-support/giving-to-legacy/gifts-of-toys-and-other-items.aspx" data-title="gifts-of-toys-and-other-items">Gifts of Toys and Other Items</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_4_hLink_2" class="ga-mega-menu" href="/giving-and-support/related-resources.aspx" data-title="related-resources">Related Resources</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_2_hLink_0" class="ga-mega-menu" href="/giving-and-support/related-resources/giving-news-and-publications.aspx" data-title="giving-news-and-publications">Giving Publications</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_2_hLink_1" class="ga-mega-menu" href="/giving-and-support/related-resources/foundation-events.aspx" data-title="foundation-events">Foundation Events</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_2_hLink_2" class="ga-mega-menu" href="/for-patients-and-visitors/general-information/volunteering.aspx" data-title="volunteering-opportunities">Volunteer Opportunities</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_4_rSubNavigation_2_hLink_3" class="ga-mega-menu" href="/giving-and-support/related-resources/who-we-are.aspx" data-title="who-we-are">Who we are</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                    </div>
                
</div>
            
                <div id="subMegaMenu5" class="subMegaMenu">
	
                    <div class="subMegaMenuBody clearfix">
                        
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_5_hLink_0" class="ga-mega-menu" href="/our-legacy/about-legacy.aspx" data-title="about-legacy">About Legacy</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_0" class="ga-mega-menu" href="/our-legacy/about-legacy/accreditation.aspx" data-title="accreditation">Accreditation</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_1" class="ga-mega-menu" href="/our-legacy/about-legacy/awards-and-honors.aspx" data-title="awards-and-honors">Awards and honors</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_2" class="ga-mega-menu" href="/our-legacy/about-legacy/community-report.aspx" data-title="community-report">Community report</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_3" class="ga-mega-menu" href="/our-legacy/about-legacy/legacy-hospitalists.aspx" data-title="legacy-hospitalists">Legacy Hospitalists</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_4" class="ga-mega-menu" href="/our-legacy/about-legacy/legacy-medical-group.aspx" data-title="legacy-medical-group">Legacy Medical Group</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_5" class="ga-mega-menu" href="/our-legacy/about-legacy/legacy-nursing.aspx" data-title="legacy-nursing">Legacy Nursing</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_6" class="ga-mega-menu" href="/our-legacy/about-legacy/locations.aspx" data-title="locations">Locations</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_7" class="ga-mega-menu" href="/our-legacy/about-legacy/medical-homes.aspx" data-title="medical-homes">Medical Homes</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_8" class="ga-mega-menu" href="/our-legacy/about-legacy/our-mission.aspx" data-title="our-mission">Our Mission</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_9" class="ga-mega-menu" href="/our-legacy/about-legacy/pacificsource.aspx" data-title="pacificsource">PacificSource</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_0_hLink_10" class="ga-mega-menu" href="/our-legacy/about-legacy/this-is-my-legacy.aspx" data-title="this-is-my-legacy">This is my Legacy</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_5_hLink_1" class="ga-mega-menu" href="/our-legacy/legacy-values.aspx" data-title="legacy-values">Legacy Values</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_1_hLink_0" class="ga-mega-menu" href="/our-legacy/legacy-values/diversity.aspx" data-title="diversity">Diversity</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_1_hLink_1" class="ga-mega-menu" href="/our-legacy/legacy-values/health-literacy.aspx" data-title="health-literacy">Health Literacy</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_1_hLink_2" class="ga-mega-menu" href="/our-legacy/legacy-values/in-the-community.aspx" data-title="in-the-community">In the Community</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_1_hLink_3" class="ga-mega-menu" href="/our-legacy/legacy-values/quality-and-patient-safety.aspx" data-title="quality-and-patient-safety">Quality and Patient Safety</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_1_hLink_4" class="ga-mega-menu" href="/our-legacy/legacy-values/sustainability.aspx" data-title="sustainability">Sustainability</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_5_hLink_2" class="ga-mega-menu" href="/our-legacy/stay-connected.aspx" data-title="stay-connected">Stay Connected</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_2_hLink_0" class="ga-mega-menu" href="/our-legacy/stay-connected/newsroom.aspx" data-title="newsroom">Newsroom</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_2_hLink_1" class="ga-mega-menu" href="/our-legacy/stay-connected/story-center.aspx" data-title="story-center">Story Center</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_2_hLink_2" class="ga-mega-menu" href="/our-legacy/stay-connected/contact-us.aspx" data-title="contact-us">Contact Us</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_2_hLink_3" class="ga-mega-menu" href="/our-legacy/stay-connected/multimedia-center.aspx" data-title="multimedia-center">Multimedia Center</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_2_hLink_4" class="ga-mega-menu" href="/our-legacy/stay-connected/patient-stories.aspx" data-title="patient-stories">Stories from our patients</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_2_hLink_5" class="ga-mega-menu" href="/our-legacy/stay-connected/legacy-image-library.aspx" data-title="legacy-image-library">Legacy Image Library</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_5_hLink_3" class="ga-mega-menu" href="/our-legacy/doing-business-with.aspx" data-title="doing-business-with">Doing business with Legacy Health</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_3_hLink_0" class="ga-mega-menu" href="/our-legacy/doing-business-with/building-a-partnership.aspx" data-title="building-a-partnership">Building Supplier Partnerships</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_3_hLink_1" class="ga-mega-menu" href="/our-legacy/doing-business-with/conducting-business-with.aspx" data-title="conducting-business-with">Conducting Business </a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_3_hLink_2" class="ga-mega-menu" href="/our-legacy/doing-business-with/pharmacy-services.aspx" data-title="pharmacy-services">Pharmacy Services</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_5_rSubNavigation_3_hLink_3" class="ga-mega-menu" href="/our-legacy/doing-business-with/randall-childrens-hospital-art-donation.aspx" data-title="randall-childrens-hospital-art-donation">Randall Children’s Hospital art</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                    </div>
                
</div>
            
                <div id="subMegaMenu6" class="subMegaMenu">
	
                    <div class="subMegaMenuBody clearfix">
                        
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_6_hLink_0" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient.aspx" data-title="refer-a-patient">Refer a patient</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_0" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/one-call-consult-and-transfer.aspx" data-title="one-call-consult-and-transfer">One Call Consult &amp; Transfer</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_1" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/kids-team-mobile-icu.aspx" data-title="kids-team-mobile-icu">KIDS Team mobile ICU</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_2" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/lmg-providers-and-services.aspx" data-title="lmg-providers-and-services">Legacy Medical Group Providers and Services</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_3" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/pain-management-center-info-for-referring-providers.aspx" data-title="pain-management-center-info-for-referring-providers">Legacy Pain Management Center - for providers</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_4" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/ECMO-for-providers.aspx" data-title="ECMO-for-providers">ECMO for Providers</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_5" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/imaging.aspx" data-title="imaging">Imaging</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_6" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/laboratory-services.aspx" data-title="laboratory-services">Laboratory Services</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_7" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/physical-therapy-and-rehab.aspx" data-title="physical-therapy-and-rehab">Physical therapy &amp; rehab</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_0_hLink_8" class="ga-mega-menu" href="/for-health-professionals/refer-a-patient/referral-forms.aspx" data-title="referral-forms">Referral Forms</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_6_hLink_1" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers.aspx" data-title="tools-and-resources-for-providers">Tools &amp; Resources for Providers</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_0" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/epic.aspx" data-title="epic">Epic</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_1" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/fuji-synapse-mobility-phone-app.aspx" data-title="fuji-synapse-mobility-phone-app">Fuji Synapse Mobility Phone App</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_2" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/legacy-access.aspx" data-title="legacy-access">Legacy Access</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_3" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/legacy-connect.aspx" data-title="legacy-connect">Legacy Connect</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_4" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/legacy-health-partners.aspx" data-title="legacy-health-partners">Legacy Health Partners</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_5" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/library-services.aspx" data-title="library-services">Library Resources &amp; Services</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_6" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/medical-staff-credentialing.aspx" data-title="medical-staff-credentialing">Medical staff credentialing and privileging </a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_7" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/mobile-med.aspx" data-title="mobile-med">Mobile Med</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_8" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/provider-profiles.aspx" data-title="provider-profiles">Provider Profile System</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_1_hLink_9" class="ga-mega-menu" href="/for-health-professionals/tools-and-resources-for-providers/physician-liaisons.aspx" data-title="physician-liaisons">Physician Liaisons</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_6_hLink_2" class="ga-mega-menu" href="/for-health-professionals/legacy-research-institute.aspx" data-title="legacy-research-institute">Legacy Research Institute</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_2_hLink_0" class="ga-mega-menu" href="/for-health-professionals/legacy-research-institute/Departments.aspx" data-title="Departments">Departments/Programs</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_2_hLink_1" class="ga-mega-menu" href="/for-health-professionals/legacy-research-institute/Scientists.aspx" data-title="Scientists">Scientists</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_2_hLink_2" class="ga-mega-menu" href="/for-health-professionals/legacy-research-institute/search-for-clinical-trials.aspx" data-title="search-for-clinical-trials">Clinical Trials</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_2_hLink_3" class="ga-mega-menu" href="/for-health-professionals/legacy-research-institute/Services.aspx" data-title="Services">Research Services</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_2_hLink_4" class="ga-mega-menu" href="/for-health-professionals/legacy-research-institute/SeminarSeries.aspx" data-title="SeminarSeries">Seminar Series</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_2_hLink_5" class="ga-mega-menu" href="/for-health-professionals/legacy-research-institute/SummerStudents.aspx" data-title="SummerStudents">Summer Student Program</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_2_hLink_6" class="ga-mega-menu" href="/for-health-professionals/legacy-research-institute/Support.aspx" data-title="Support">Support Our Research</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_6_hLink_3" class="ga-mega-menu" href="/for-health-professionals/education-for-health-professionals.aspx" data-title="education-for-health-professionals">Education for Health Professionals</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_3_hLink_0" class="ga-mega-menu" href="/for-health-professionals/education-for-health-professionals/pedinet.aspx" data-title="pedinet">PediNet</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_3_hLink_1" class="ga-mega-menu" href="/for-health-professionals/education-for-health-professionals/pharmacy-residency-program.aspx" data-title="pharmacy-residency-program">Pharmacy Residency Programs</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_3_hLink_2" class="ga-mega-menu" href="/for-health-professionals/education-for-health-professionals/clinical-pastoral-education.aspx" data-title="clinical-pastoral-education">Clinical Pastoral Education</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_3_hLink_3" class="ga-mega-menu" href="/for-health-professionals/education-for-health-professionals/conferences.aspx" data-title="conferences">Conferences</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_3_hLink_4" class="ga-mega-menu" href="/for-health-professionals/education-for-health-professionals/continuing-medical-education.aspx" data-title="continuing-medical-education">Continuing Medical Education</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_3_hLink_5" class="ga-mega-menu" href="/for-health-professionals/education-for-health-professionals/continuing-nursing-education.aspx" data-title="continuing-nursing-education">Continuing Nursing Education</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_3_hLink_6" class="ga-mega-menu" href="/for-health-professionals/education-for-health-professionals/graduate-medical-education.aspx" data-title="graduate-medical-education">Graduate Medical Education</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                                <div class="subMegaMenuColumn">
                                    <ul class="secondary">
                                        <li class="secondary">
                                            <a id="ctl04_rSubNavigation_rSubNavigation_6_hLink_4" class="ga-mega-menu" href="/for-health-professionals/careers.aspx" data-title="careers">Careers</a>
                                            
                                                <ul class="tertiary">
                                                    
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_4_hLink_0" class="ga-mega-menu" href="/for-health-professionals/careers/content-modules.aspx" data-title="content-modules"></a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_4_hLink_1" class="ga-mega-menu" href="/for-health-professionals/careers/life-at-legacy.aspx" data-title="life-at-legacy">Life at Legacy</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_4_hLink_2" class="ga-mega-menu" href="/for-health-professionals/careers/career-areas.aspx" data-title="career-areas">Career areas</a></li>
                                                        
                                                            <li class="tertiary">
                                                                <a id="ctl04_rSubNavigation_rSubNavigation_6_rSubNavigation_4_hLink_3" class="ga-mega-menu" href="/for-health-professionals/careers/hiring-questions.aspx" data-title="hiring-questions">Hiring questions</a></li>
                                                        
                                                    <li class="tertiary subMegaMenuExpand"><span>
                                                        see more mega-menu-label
                                                    </span></li>
                                                </ul>
                                            
                                        </li>
                                    </ul>
                                </div>
                            
                    </div>
                
</div>
            
    </div>
</header>
<div id="megaMenuSiteFade" class="hidden"></div>

        <div id="pageContent" class="container" role="main">
	

            
            <div class="row">
                <div class="eleven columns">
                    
	<div id="breadcrumbWrap" style="font-size: 11px; ">
		
				<a id="ctl05_rBreadcrumbs_hLink_0" href="/">Home</a>
				
			<span class="breadcrumb_between">:</span>
				<a id="ctl05_rBreadcrumbs_hLink_1" href="/policies.aspx">Policies</a>
				
			<span class="breadcrumb_between">:</span>
				
				Privacy policy
			
	</div>

                </div>
                <div class="one columns">
                    <a href="#" class="addThis shareLink">
                        Share
                    </a>
                </div>
            </div>
            
<div id="header-banner" class="row clearfix">
	<div class="twelve columns">
		
	</div>
</div>
            <div class="row clearfix auto-enter-boundary">
                
<aside id="sidenav-column" class="three columns">
	<h2>Policies</h2>
	<nav id="sidenav">
		<ul>
			
					<li id="body_0_rItems_lLink_0"><a id="body_0_rItems_hLink_0" href="/policies/inclement-weather.aspx">Weather update</a></li>
				
					<li id="body_0_rItems_lLink_1" class="current"><a id="body_0_rItems_hLink_1" href="/policies/privacy-policy.aspx">Privacy Policy</a></li>
				
					<li id="body_0_rItems_lLink_2"><a id="body_0_rItems_hLink_2" href="/policies/terms-of-use.aspx">Terms of Use</a></li>
				
		</ul>
	</nav>
</aside><div id="main-content" class="nine mobile-twelve mobile-first-child columns">
		
	<article>
		<header>
			<h1>Privacy policy</h1>
			
		</header>
		
        

		<p class="p1">We provide this notice to explain our online information practices, and&nbsp;make it available from every page of our web site. By visiting Legacy Health (Legacy) at www.legacyhealth.org, you are accepting the practices described in this Privacy Statement.</p>
<h3 class="p2">Information we collect</h3>
<p class="p1">Two types of information are collected:</p>
<ul class="ul1">
    <li class="li3">Personal or "individually identifiable" information that you provide to us </li>
    <li class="li3">Standard web server/visitor traffic information, commonly referred to as "aggregate information," regarding overall website traffic patterns. Normally, web servers collect this type of basic information as part of their web log processes. We do not report on individual user sessions. </li>
</ul>
<h3 class="p2">Individually identifiable information&nbsp;and how we use it</h3>
<p class="p1">There are instances when Legacy collects individually identifiable information from its users. Examples include: employment applications, information requests and charitable contributions. Individually identifiable user information we may collect includes:</p>
<ul class="ul1">
    <li class="li3">First and last name, street address, city, state, zip code, telephone number, email address, and subject of inquiry (i.e. information requests). </li>
</ul>
<p class="p1">&nbsp;</p>
<p class="p1">In the case of contributions:</p>
<ul class="ul1">
    <li class="li3">We process standard credit card information (card number, card type, expiration date) via a secure, encrypted Secure Sockets Layer session. SSL is the proven standard for secure web messaging transactions. We maintain records of all financial transactions.</li>
    <li class="li3">In the case of tribute (honorarium or memorial) contributions made to a Legacy hospice program, a personalized acknowledgement letter is sent to a designated recipient, usually a loved one of the deceased or the honoree. Therefore, we will request the name, address, city, state and zip code for your designated acknowledgee. This information is only used for the purpose of sending the acknowledgement.</li>
</ul>
<h3 class="p2">Donation information</h3>
<p class="p1">We maintain records of all contributions to Legacy's affiliated foundations. All information related to such contributions, including all personal information is private and confidential. Additionally, all information is stored in a secure location accessible only by authorized staff. Legacy may contact you with further communications.</p>
<p class="p1">You can choose to opt out from:</p>
<ul class="ul1">
    <li class="li3">Fundraising activities, communications and solicitations </li>
    <li class="li3">Any contact from Legacy </li>
</ul>
<p class="p1">&nbsp;</p>
<p class="p1">If you wish to opt out or restrict the use of your contact information, you may let us know by:</p>
<ul class="ul1">
    <li class="li3">Contacting Legacy Foundations by emailing <a href="mailto:giving@lhs.org">giving@lhs.org</a> or writing:<br />
    <ul class="ul2">
        <dd>Legacy Foundations </dd>
        <dd>P.O. Box 4484 </dd>
        <dd>Portland, OR 97208-4484 </dd>
        <dd>(Please indicate which Legacy foundation(s) you want to opt out from financial contribution activities.) </dd>
        <dd><br />
        </dd>
    </ul>
    </li>
</ul>
<ul class="ul1">
    <li class="li3">Emailing Legacy from our website. Use the "Contact Us" link (found at the bottom of every page&nbsp;of our website) </li>
</ul>
<h3 class="p2">Aggregate information</h3>
<p class="p1">In addition to information provided by visitors to our website, we use the normal Internet tracking tools associated with standard Internet protocols and Web based systems. This system information is typically stored in log files and the information is used for aggregate reporting. Aggregate reporting includes: total number of visitors, most visited sections, peak traffic times, etc. Log files are simply transaction records web servers maintain. Those logs are used for recording information, such as:</p>
<ul class="ul1">
    <li class="li3">service provider IP addresses </li>
    <li class="li3">browser versions </li>
    <li class="li3">referring websites </li>
    <li class="li3">search terms used </li>
    <li class="li3">average number of pages requested </li>
    <li class="li3">average duration of visit </li>
    <li class="li3">total visitor traffic </li>
</ul>
<p class="p1">&nbsp;</p>
<p class="p1">We use aggregate information gathered from your visit to better design our website. This information supplies us with a broad picture of how people use our website in order to help the management of our systems and to better serve our audience.</p>
<h3 class="p2">Our commitment to data security</h3>
<p class="p1">To prevent unauthorized access, maintain data accuracy, and ensure the correct use of information, we strive to maintain physical, electronic, and administrative safeguards to secure the information we collect online. This includes the use of the Secure Sockets Layer for processing contributions securely.</p>
<h3 class="p2">Links to other sites</h3>
<p class="p1">Legacy has links to other websites that are not under its control, and Legacy is not responsible for the contents of any linked website, or any link contained in a linked website, or any changes or updates to such websites. The inclusion of any link does not imply endorsement by Legacy of that website.</p>
<p class="p1">This Privacy Statement applies only to the Legacy website. Please be aware that Legacy is not responsible for the privacy practices of other websites. We encourage you to read the privacy statements of every website that requests personal information from you.</p>
<p class="p1">Legacy reserves the right to change our privacy policies at any time. This Privacy Statement will be kept up-to-date and clearly posted on our website.</p>
<p class="p1">&nbsp;</p>

        <!-- Extended Page Fields -->

        

        

        

        

        

        

        <!-- End of Extended Page Fields -->
        
		
        
        
        
		
        
	</article>

	</div>


            </div>
        
</div>
        
<footer id="pageFooter" class="clearfix">
	<div role="main" class="container">
  		<div class="row">
			<div id="branding-column" class="three mobile-six columns">
				<a id="ctl09_hLogo" class="headerLogo" href="/"><img src="/-/media/Images/Logos/Footer.png?h=33&amp;la=en&amp;w=78" alt="Legacy Health" width="78" height="33" /></a>
				<p class="copyright"></p>
				<div class="footerNewsletterForm clearfix auto-enter-boundary">
					<input name="ctl09$tEmail" type="text" id="ctl09_tEmail" class="emailSignup" data-label="Sign up for emails" />
					<input type="submit" name="ctl09$bEmail" value="" id="ctl09_bEmail" class="footerEmailSubmit autoenter" />
				</div>
				<nav>
					<ul>
						<li>
							<a id="ctl09_rPrimaryFooter_hLink_0" href="/policies/terms-of-use.aspx" class="ga-footer-link" data-title="Terms of Use">Terms of Use</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a id="ctl09_rPrimaryFooter_hLink_1" href="/policies/privacy-policy.aspx" class="ga-footer-link" data-title="Privacy Policy">Privacy Policy</a>
						</li>
						
								<li><a id="ctl09_rSecondaryFooter_hLink_0" href="/contact-us/phone-numbers.aspx" class="ga-footer-link" data-title="See our list of phone numbers">See our list of phone numbers</a></li>
							
								<li><a id="ctl09_rSecondaryFooter_hLink_1" href="/contact-us/email-us.aspx" class="ga-footer-link" data-title="Email us">Email us</a></li>
							
								<li><a id="ctl09_rSecondaryFooter_hLink_2" href="/for-patients-and-visitors/general-information/rights-and-responsibilities/notice-of-nondiscrimination-and-accessibility.aspx" class="ga-footer-link" data-title="Notice of Nondiscrimination | Accessibility">Notice of Nondiscrimination | Accessibility</a></li>
							
					</ul>
				</nav>
			</div>
			
					<div id="link-column-one" class="three mobile-six columns">
	
						<div>
							<div class="f-border"></div>
							<h2>Resources</h2>
							<nav>
								<ul>
									
											<li><a id="ctl09_rFooterMenus_rSubNavigation_0_hLink_0" href="/for-health-professionals/careers.aspx" class="ga-footer-link" data-title="Careers">Careers</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_0_hLink_1" href="/for-patients-and-visitors/general-information/volunteering.aspx" class="ga-footer-link" data-title="Volunteering">Volunteering</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_0_hLink_2" href="/for-patients-and-visitors/patient-information/get-medical-records.aspx" class="ga-footer-link" data-title="How to order medical records">How to order medical records</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_0_hLink_3" href="https://myhealth.lhs.org/myhealth/" class="ga-footer-link" data-title="Log in to MyHealth" target="_blank (new window)">Log in to MyHealth</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_0_hLink_4" href="/for-patients-and-visitors/patient-information/billing-information/pay-your-bill.aspx" class="ga-footer-link" data-title="Paying your bill online">Paying your bill online</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_0_hLink_5" href="http://www.legacyhealthevents.org" class="ga-footer-link" data-title="Classes|Events|Tours" target="_blank (new window)">Classes|Events|Tours</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_0_hLink_6" href="/find-a-provider/insurance-faqs.aspx" class="ga-footer-link" data-title="Insurance FAQs">Insurance FAQs</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_0_hLink_7" href="/our-legacy/about-legacy.aspx" class="ga-footer-link" data-title="About Legacy">About Legacy</a></li>
										
								</ul>
							</nav>
						</div>
					
</div>
				
					<div id="link-column-two" class="three mobile-six columns mobile-first-child">
	
						<div>
							<div class="f-border"></div>
							<h2>Popular Links</h2>
							<nav>
								<ul>
									
											<li><a id="ctl09_rFooterMenus_rSubNavigation_1_hLink_0" href="/for-health-professionals/education-for-health-professionals.aspx" class="ga-footer-link" data-title="Education for Health Professionals">Education for Health Professionals</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_1_hLink_1" href="/for-health-professionals/refer-a-patient/laboratory-services.aspx" class="ga-footer-link" data-title="Laboratory Services">Laboratory Services</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_1_hLink_2" href="/giving-and-support/giving-to-legacy/ways-to-give.aspx" class="ga-footer-link" data-title="Ways to Give">Ways to Give</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_1_hLink_3" href="/for-health-professionals/refer-a-patient.aspx" class="ga-footer-link" data-title="Refer a patient">Refer a patient</a></li>
										
											<li><a id="ctl09_rFooterMenus_rSubNavigation_1_hLink_4" href="/for-health-professionals/legacy-research-institute/search-for-clinical-trials.aspx" class="ga-footer-link" data-title="Clinical Trials">Clinical Trials</a></li>
										
								</ul>
							</nav>
						</div>
					
</div>
				
			<div id="contact-column" class="three mobile-six columns clearfix">
				<div>
					<div class="f-border"></div>
					<h2>Connect With Us</h2>
					
							<div id="f-facebook">

</div>
							<p class="f-indent">
								
										<a id="ctl09_rSocialNetworks_rSocialLinks_0_hLink_0" href="http://www.facebook.com/legacyhealth" class="ga-footer-link" data-title="Legacy Health" target="_blank">Legacy Health</a>
									
										<a id="ctl09_rSocialNetworks_rSocialLinks_0_hLink_1" href="http://www.facebook.com/legacyweight" class="ga-footer-link" data-title="Legacy Weight and Diabetes Institute" target="_blank">Legacy Weight and Diabetes Institute</a>
									
										<a id="ctl09_rSocialNetworks_rSocialLinks_0_hLink_2" href="http://www.facebook.com/legacychildrens" class="ga-footer-link" data-title="Randall Children&#39;s Hospital" target="_blank">Randall Children&#39;s Hospital</a>
									
							</p>
						
							<div id="f-twitter">

</div>
							<p class="f-indent">
								
										<a id="ctl09_rSocialNetworks_rSocialLinks_1_hLink_0" href="https://twitter.com/OurLegacyHealth/" class="ga-footer-link" data-title="Legacy Health" target="_blank">Legacy Health</a>
									
										<a id="ctl09_rSocialNetworks_rSocialLinks_1_hLink_1" href="https://twitter.com/randallchildren" class="ga-footer-link" data-title="Randall Children&#39;s Hospital" target="_blank">Randall Children&#39;s Hospital</a>
									
							</p>
						
							<div id="f-youtube">

</div>
							<p class="f-indent">
								
										<a id="ctl09_rSocialNetworks_rSocialLinks_2_hLink_0" href="http://www.youtube.com/user/OurLegacyHealth" class="ga-footer-link" data-title="Watch our YouTube videos" target="_blank">Watch our YouTube videos</a>
									
							</p>
						
				</div>
			</div>
		</div>
  </div>
</footer>
    </form>

    

    <!-- Page-specific 3rd party markup -->
    
    
    
    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/media/js/libs/jquery-1.7.2.min.js"><\/script>')</script>
    <script src="http://s7.addthis.com/js/200/addthis_widget.js"></script>

    <!-- Location Map -->
    <!--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>-->

    <!-- scripts concatenated and minified via build script -->
    <script src="/media/js/libs/featherlight.min.js"></script>
    <script src="/media/js/plugins.js?v=7.2.7"></script>
    <script src="/media/js/libs/chosen.jquery.js"></script>
    <script src="/media/js/libs/slick.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="/media/js/script.js?v=7.2.7"></script>
    <script src="/media/js/autoenter.js?v=7.2.7"></script>
    <!-- end scripts -->

    <!-- Asynchronous Google Analytics snippet. Change UA-XXXXX-X to be your site's ID. mathiasbynens.be/notes/async-analytics-snippet -->
    <script>
        
       
        

        //var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
        //_gaq.push(['_require', 'inpage_linkid', pluginUrl]);

        (function (d, t) {
            var g = d.createElement(t), s = d.getElementsByTagName(t)[0];
            g.src = ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g, s)
        }(document, 'script'));

        // fonts.com tracking
        window.onload = function () {
            var mtiTracking = document.createElement('link');
            mtiTracking.type = 'text/css';
            mtiTracking.rel = 'stylesheet';
            mtiTracking.href = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//fast.fonts.com/t/1.css';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(mtiTracking);
        }
    </script>

    <!-- Legacy Healthcare Systems's SmartPixel -->
    <script type="text/javascript">
        adroll_adv_id = "OGDGSRXV4BBQBEJIVLUN5R";
        adroll_pix_id = "P3Y7RAWX2JELXDEORXZX4L";
        (function () {
            var oldonload = window.onload;
            window.onload = function () {
                __adroll_loaded = true;
                var scr = document.createElement("script");
                var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
                scr.setAttribute('async', 'true');
                scr.type = "text/javascript";
                scr.src = host + "/j/roundtrip.js";
                ((document.getElementsByTagName('head') || [null])[0] ||
                 document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
                if (oldonload) { oldonload() }
            };
        }());
    </script>
    
</body>
</html>
