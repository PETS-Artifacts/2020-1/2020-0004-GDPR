


<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Money Instructor.com - Worksheets and Lesson Plans for Teachers</title>

<script>
var locate = window.location
var cmfrom = document.referrer

function get_cookie(Name) {
  var search = Name + "="
  var returnvalue = "";
  if (document.cookie.length > 0) {
    offset = document.cookie.indexOf(search)
    if (offset != -1) { // if cookie exists
      offset += search.length
      // set index of beginning of value
      end = document.cookie.indexOf(";", offset);
      // set index of end of cookie value
      if (end == -1)
         end = document.cookie.length;
      returnvalue=unescape(document.cookie.substring(offset, end))
      }
   }
  return returnvalue;
}

function checkgot(){
if (get_cookie('refer')==''){
document.cookie="refer=PAGEPRIV"+cmfrom + ";path=/;";
}
}

checkgot()

</script>

<style fprolloverstyle>
A:hover {color: red; font-weight: bold}
.style1 {
	font-family: Arial;
	font-size: 12px;
}
</style>

</head>

<body bgcolor="#FFFFFF">


<div align="center">
  <center>
  <TABLE border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111">
				<TR><TD colspan="7">
                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1">
                    <tr>
                      <td width="34%"><a href="http://www.moneyinstructor.com/">
                  <img src="../images/moneytitle3.jpg" border="0" width="240" height="64"></a></td>
                      <td width="63%" align="right" valign="bottom">&nbsp;</td>
                      <td width="3%" align="left">&nbsp;
      </td>
                    </tr>
                  </table>
                  </TD></TR>
				<TR>
					<TD><a href="http://www.moneyinstructor.com/">
                    <img src="../images/newsite/new/menu1.JPG" border="0" width="102" height="33" alt="Money Instructor Home"></a></TD>
					<TD><a href="../skills.asp">
                    <img src="../images/newsite/new/menu_money.JPG" border="0"  width="125" height="33" alt="Basic Money Skills Lessons"></a></TD>
					<TD><a href="../earnspend.asp">
                    <img src="../images/newsite/new/menu_earnspend.JPG" border="0" width="98" height="33" alt="Earning and Spending Money Lessons"></a></TD>
					<TD><a href="../spendsave.asp">
                    <img src="../images/newsite/new/menu_saveinvest.JPG" border="0" width="104" height="33" alt="Saving and Investing Money Lessons"></a></TD>
					<TD><a href="../interactive.asp">
                    <img src="../images/newsite/new/menu_interactive.JPG" border="0" width="106" height="33" alt="Interactive Money Lessons"></a></TD>
					<TD><a href="../suggestions.asp">
                    <img src="../images/newsite/new/menu_suggestions.JPG" border="0" width="127" height="33"  alt="Suggestions and Requests"></a></TD>
					<TD bgcolor="#E6E4D8"><IMG src="../images/newsite/spacer.gif" width="104" height="1" width="104" height="1"></TD>
				</TR>
		</TABLE></center>
</div>  




<p align="center" style="margin-top: 0; margin-bottom: 0">&nbsp;</p>

<p align="center" style="margin-top: 0; margin-bottom: 0">&nbsp;</p>

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1">
  <tr>
    <td width="14%">&nbsp;</td>
    <td width="68%">
    <p style="margin-top: 0; margin-bottom: 0"><b>
    <font face="Arial" size="5" color="#008000">Privacy Policy</font></b></p>
    <p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
    <p class="style1" style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
    <p class="style1">&nbsp;</p>
    <p>Money Instructor (MI) recognizes the importance of protecting the privacy of your personal information, and we have prepared this Privacy Policy to provide you with information about our privacy practices. This Privacy Policy applies when you use our websites, register for any of our sites, register for any of our events, and/or subscribe to any of our subscription services.</p>
    <p>This Privacy Policy has been updated to reflect compliance with the European Union&rsquo;s General Data Protection and Regulation (&ldquo;GDPR&rdquo;) requirements.</p>
    <p>Summary<br>
      We collect your data only to provide you with access to our content, events, and other programs. We do not share your data with anyone else without your explicit consent to do so. We respect your right to privacy. If you wish to unsubscribe from the Services and/or to have us remove your data from our systems, we will do so.</p>
    <p>Consenting to our collection and use of your data is optional, but if you do not provide consent, we cannot provide you with the Services.</p>
    <p>Personal Information We May Collect<br>
      For the purpose of this Privacy Policy, &ldquo;Personal Information&rdquo; means any information relating to an identified or identifiable individual. We obtain Personal Information relating to you from various sources described below.</p>
    <p>Where applicable, we indicate whether and why you must provide us with your Personal Information, as well as the consequences of failing to do so. If you do not provide Personal Information when requested, you may not be able to benefit from our Service if that information is necessary to provide you with the service or if we are legally required to collect it.</p>
    <p>Personal Information Provided by You<br>
      While using our Services, you will be given the opportunity to sign up for newsletters and submit information to access content.</p>
    <p>When you do that, we may collect the following information from you: full name, email address, location, mailing address, billing address or payment information such as credit/debit card info.</p>
    <p>We do not store credit/debit card information. This information is kept with our payment processing services to complete a transaction you requested.</p>
    <p>Personal Information Automatically Obtained from Your Interactions with the Service</p>
    <p><br>
      Log Data<br>
      When you use our Service, our servers automatically record information that your browser sends whenever you visit a website (&ldquo;Log Data&rdquo;). This Log Data may include information such as your IP address, browser type or the domain from which you are visiting, the web-pages you visit, the search terms you use, and any advertisements on which you click.</p>
    <p>Cookies and Similar Technologies<br>
      Like many websites, we also use &ldquo;cookie&rdquo; technology to collect additional website usage data and to improve the Site and our Service. A cookie is a small data file that we transfer to your computer&rsquo;s hard disk. A session cookie enables certain features of the Site and our service and is deleted from your computer when you disconnect from or leave the Site. A persistent cookie remains after you close your browser and may be used by your browser on subsequent visits to the Site. Persistent cookies can be removed by following your web browser help file directions. Most Internet browsers automatically accept cookies. Buffer may use both session cookies and persistent cookies to better understand how you interact with the Site and our Service, to monitor aggregate usage by our users and web traffic routing on the Site, and to improve the Site and our Service.</p>
    <p>We may also automatically record certain information from your device by using various types of technology, including &ldquo;clear gifs&rdquo; or &ldquo;web beacons.&rdquo; This automatically collected information may include your IP address or other device address or ID, web browser and/or device type, the web pages or sites that you visit just before or just after you use the Service, the pages or other content you view or otherwise interact with on the Service, and the dates and times that you visit, access, or use the Service. We also may use these technologies to collect information regarding your interaction with email messages, such as whether you opened, clicked on, or forwarded a message, to the extent permitted under applicable law.</p>
    <p>You can instruct your browser, by editing its options, to stop accepting cookies or to prompt you before accepting a cookie from the websites you visit. Please note that if you delete, or choose not to accept, cookies from the Service, you may not be able to utilize the features of the Service to their fullest potential.</p>
    <p>Do Not Track<br>
      We do not process or respond to web browsers&rsquo; &ldquo;do not track&rdquo; signals or other similar transmissions that indicate a request to disable online tracking of users who use our Service.</p>
    <p>Third Party Web Beacons and Third Party Buttons<br>
      We may display third-party content on the Service, including third-party advertising. Third-party content may use cookies, web beacons, or other mechanisms for obtaining data in connection with your viewing of the third party content on the Service. Additionally, we may implement third party buttons, such as Facebook &ldquo;share&rdquo; buttons, that may function as web beacons even when you do not interact with the button. Information collected through third-party web beacons and buttons is collected directly by these third parties, not by MI. Please consult such third party&rsquo;s data collection, use, and disclosure policies for more information.</p>
    <p>Links to Other Websites<br>
      Our Site contains links to other websites. The fact that we link to a website is not an endorsement, authorization or representation of our affiliation with that third party. We do not exercise control over third party websites. These other websites may place their own cookies or other files on your computer, collect data or solicit Personal Information from you. Other sites follow different rules regarding the use or disclosure of the Personal Information you submit to them. We are not responsible for the content, privacy and security practices, and policies of third-party sites or services to which links or access are provided through the Service. We encourage you to read the privacy policies or statements of the other websites you visit.</p>
    <p>How do we use information we collect?<br>
      To Provide and Manage the Services You Request<br>
      This includes, for example, sending you electronic newsletters and enabling you to participate in the features provided by the Services. It also may include providing personalized content and recommendations to you. Through such features, we are able to bring you information and content tailored to your individual interests and needs.</p>
    <p>To Contact You<br>
      We may periodically contact you with information about the Services and our affiliates and partners.</p>
    <p>Consent<br>
      We may otherwise use your information with your consent or at your direction.</p>
    <p>How do we share information?<br>
      The following provides information about entities with which we may share information. Our practices vary depending on the type of information.</p>
    <p>Service Providers<br>
      We may share information with vendors providing contractual services to us, such as hosting vendors.</p>
    <p>Other Parties When Required by Law or As Necessary to Protect Our Users and Services<br>
      We may share your personal information as we believe is necessary or appropriate to protect, enforce, or defend the legal rights, privacy, safety, or property of the Services, our employees or agents or users or to comply with applicable law or legal process, including responding to requests from public and government authorities.</p>
    <p>Other Parties in Connection With a Corporate Transaction<br>
      We reserve the right to transfer any information we have about you in the event that we sell or transfer all or a portion of our business or assets to a third party, such as in the event of a merger, acquisition, or in connection with a bankruptcy reorganization.</p>
    <p>Otherwise With Your Consent or At Your Direction<br>
      In addition to the sharing described in this Privacy Policy, we may share information about you with third parties whenever you consent to or direct such sharing.</p>
    <p>Do we provide links to third-party sites?<br>
      Our Services may link to third-party websites and services that are outside our control. We are not responsible for the security or privacy of any information collected by other websites or other services. You should exercise caution and review the privacy statements applicable to the third-party websites and services you use.</p>
    <p>Accessing or updating your information and other privacy choices available to you<br>
      You may modify your subscription and other personally identifiable information we have obtained as a result of your use of the Services by sending an email using our contact us link.</p>
    <p>Data Security<br>
      We have in place physical, electronic and managerial procedures to protect the information we collect online. However, as effective as these measures are, no security system is impenetrable. We cannot guarantee the security of our database, nor can we guarantee that the information you supply will not be intercepted while being transmitted to us over the Internet.</p>
    <p>How to ask us to delete your data<br>
      You can unsubscribe from our communications at any time by selecting the &ldquo;unsubscribe&rdquo; button in the footer of our emails. If you would like us to delete your information entirely, or request to see the information we store about you, please send an email to the support email address associated with the website in question with the subject line &ldquo;Data Removal Request&rdquo;.</p>
    <p>Absent a request to unsubscribe and/or delete your data, your continued use of the Services gives us permission to manage and use your data as described in this Privacy Policy.</p>
    <p>Other Important Information<br>
      Children&rsquo;s Privacy<br>
      We do not knowingly collect any personal information from children under the age of 13 without adult consent, unless permitted by law. If we learn that a child under the age of 13 has provided us with personal information, we will delete it in accordance with applicable law.</p>
    <p>Changes to Our Privacy Policy<br>
      We may modify this Privacy Policy from time to time. We will notify you of changes by posting changes here, or by other appropriate means. Any changes to the Privacy Policy will become effective when the updated policy is posted on the Services. Your use of the Services or your provision of personal information to use the Services following such changes indicates your acceptance of the revised Privacy Policy.</p>
    <p class="style1">&nbsp;</p>
    <p class="style1"><font face="Arial" size="2">We 
      take your privacy seriously.&nbsp; With the exception of any anonymous information which may be collected from third-party advertisers (see below), Money Instructor is the sole owner of 
      all information collected from this site, and no information is sold or 
      shared by us with others.</font></p>
    <p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
    <p class="style1" style="margin-top: 0; margin-bottom: 0"><font face="Arial" size="2">You 
    must register to access the content of this web site.&nbsp; All information 
    registered with us will never be sold or shared with others.&nbsp; This 
    includes your email address and other information.&nbsp; Information you 
    provide to us is used only by MoneyInstructor.com to better offer content 
    and services to our members, troubleshooting errors, etc.&nbsp; We may also 
    from time to time email our members about services offered by 
    Money Instructor, or other relevant information. Log 
    files and cookies may be used to help administer the site, and track content 
    demand.</font></p>
    <p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
    <p class="style1" style="margin-top: 0; margin-bottom: 0"><font face="Arial" size="2">The 
    site may have links to other web sites.&nbsp; Be aware that we are not 
    responsible for the content of these other web sites.&nbsp; These may 
    include other educational sites, advertisers, etc.&nbsp; These other web 
    sites may use cookies and collect personal information.&nbsp; Again, we 
    never 
    share or sell information from our members to these other sites.&nbsp; Check 
    the privacy policy of those web sites before deciding to give them any 
    personal information.</font></p>
    <p class="style1" style="margin-bottom: 0">We  allow third-party companies to serve ads and/or collect certain anonymous  information when you visit our web site. These companies may use non-personally  identifiable information (e.g., click stream information, browser type, time  and date, subject of advertisements clicked or scrolled over) during your  visits to this and other Web sites in order to provide advertisements about  goods and services likely to be of greater interest to you. These companies  typically use a cookie or third party web beacon to collect this information.  To learn more about this behavioral advertising practice or to opt-out of this  type of advertising, you can visit <a href="http://www.networkadvertising.org/managing/opt_out.asp" rel="nofollow">networkadvertising.org</a>.</p>
    <p class="style1" style="margin-bottom: 0"><font face="Arial" size="2">If 
    you use our site, you consent to the collection and use of information as 
    described above.&nbsp; If we make any changes to our Privacy statement, we 
    will post it on our web site.</font></p>
    <p style="margin-top: 0; margin-bottom: 0">&nbsp;</p>
    <p style="margin-top: 0; margin-bottom: 0"></td>
    <td width="18%">&nbsp;</td>
  </tr>
</table>
    <p style="margin-top: 0; margin-bottom: 0" align="center"><b>
    <font size="4" face="Arial">
    <a href="http://www.moneyinstructor.com/">Click here</a> to go 
    to the <a href="http://www.moneyinstructor.com/">Money Instructor</a> home page</font></b><p style="margin-top: 0; margin-bottom: 0"><p style="margin-top: 0; margin-bottom: 0"><p style="margin-top: 0; margin-bottom: 0"></body></html>