

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-CA" xml:lang="en-CA" data-ng-app='core'>

<head id="ctl00_Head1"><title>
	Privacy Policy - Best Buy Canada
</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="canonical" href="http://www.bestbuy.ca/en-CA/help/privacy-policy/hc1139.aspx"/>    
<link id="ctl00_favicon" type="image/x-icon" href="../../../favicon.ico" rel="icon" />
<link type="text/css" media="all" rel="stylesheet" href="/scripts/core-assets/styles/ng-dialog.min.css?v=2.36" /><link type="text/css" media="all" rel="stylesheet" href="/scripts/core-assets/styles/marketplace.min.css?v=2.36" /><link type="text/css" media="all" rel="stylesheet" href="/css/default.css?v=2.36" /><link type="text/css" media="all" rel="stylesheet" href="/css/style.css?v=2.36" /><link type="text/css" media="all" rel="stylesheet" href="/css/bootstrap.css?v=2.36" /><link type="text/css" media="all" rel="stylesheet" href="/scripts/core-assets/styles/core.min.css?v=2.36" />


	<!--[if gte IE 8]>
	    <link type="text/css" rel="stylesheet" media="all" href="../../../css/ie/ie.css?v=2.36" /><link type="text/css" media="all" rel="stylesheet" href="/scripts/core-assets/styles/ie-lte9.min.css" />
	<![endif]-->

	<!--[if lt IE 9]>
		<script src="/jmvc/libraries/files/html5.js" type="text/javascript"></script>
	<![endif]-->


	


<script src="//d2wy8f7a9ursnm.cloudfront.net/bugsnag-2.min.js" data-apikey="2822bdd50d07b652a768e3bf029bed08"></script>


    <script> Bugsnag.releaseStage = 'prod-prod'; </script>

        <script src='//assets.adobedtm.com/04ab3340cf329abcea56acabf66ac369fdd317b9/satelliteLib-c1b195160f65264d33779a173088b1b148531931.js'></script>
	

    <script src="http://static.bbycastatic.ca/jmvc/steal/steal.production.js"></script>
    <script src="http://static.bbycastatic.ca/jmvc/libraries/libraries.min.js?v=2.36"></script>
    <script src="http://static.bbycastatic.ca/scripts/core-assets/scripts/core-vendor.min.js?v=2.36"></script>
    <script src="http://static.bbycastatic.ca/scripts/modernizr/bby-modernizr.min.js?v=2.36"></script>
<link href="../../../css/csr.css" type="text/css" rel="stylesheet" /><link href="/css/common.css" type="text/css" rel="stylesheet" /><link href="/css/specialfeatures.css" type="text/css" rel="stylesheet" /></head>
<body>
	<form name="aspnetForm" method="post" action="/en-CA/help/privacy-policy/hc1139.aspx" id="aspnetForm">
<div>
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="FxmEGGfOR86mUOh+Jwgt7y+nXW4SqzIdTFZx941GXhfbyWGxS1Z9eIpDBWHhChudwEyNFahiT8hBYxM7qJOGkJoTuELMwdR0O5YfKHFPH4AyZV22HDHxlRAnGrQmayRXr3fANigWXP1pd4oMY19a5BqaOfS1badmJbswgR73md9YP8QWMZb+ibtC/fLPE3t31n1We6yNMWFkmXFWr09D5Gg+8XFy6Yg+7uCCznPzfZKqdARodASZvHrO4L1EniRTM36FyXJKOgf1/Cd9CtWoo5w+6Ywd/FP87VfMKnnA59sIIHIZGOtS8G0BXmzfgXKcyCmUf5oQ3GTbjdnicsBA1Kf2eDRUjTob7w4IF3Xah7mY11Hd7vU/NORTXhKXDr6xc5qG9C7jMryfk01viFJXWhSzHZUdiOONI395DfeTWF/cNrk9EJmVC0WOUuEI5UYfOS0sqcMgCgiFiZvClNToAT9RVbRybvekjtBpBYVzm+fPsKEnfKeunedvHUmoFsklJdRjI4VX3mE9sqGych5IpoUpry+7eLSdj2a+1j/YtWbGgonFiXa0TNL6xeVSosvTYDC8T8NComhTvCotiwVi6EbviKCbLaZAmqhQTFJMDt4SPm/vH1wmLp3fCyBNT6XaM/AdG4lFusJILs8D9z38Gq4tUYk=" />
</div>


<script src="/config.ashx?lang=en-CA" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=aE9PDZQYdHjWo46qOH-AjtTpt3J8fxtrqYBZm41Os3RKDSZR0WTIIxpJi-Qp8zJ7MpKca9V9XDPw5fXMGmfSAUGonWcKb3wG7MMGR0WpcAQlSG9rCgDQ4vcPgqxsU80hDOuaBpiCwrqJYU5xzgcK0r0FvGLFfAKoL1eDND4EN_YMCdTf0&amp;t=2e2045e2" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(Sys) === 'undefined') throw new Error('ASP.NET Ajax client-side framework failed to load.');
//]]>
</script>

<script src="http://static.bbycastatic.ca/jmvc/ui/ui.min.js?v=2.36" type="text/javascript"></script>
<script src="http://static.bbycastatic.ca/jmvc/core/core.min.js?v=2.36" type="text/javascript"></script>
<script src="http://static.bbycastatic.ca/jmvc/legacy/legacy.min.js?v=2.36" type="text/javascript"></script>
<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="F0945A61" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="NgMx6EnM6rRg9d2l+GunqwwFLPu7YCvDwpe9WJuCAQIeUk4Jh17+y4P9l6pxO7uwxZiC9+xO8aOIk3Q6ixzfTpcQnOd1onH+brSFUJ7rdpAvkAbLhy6yf0llUdenFkBoECc9+AQ4BJ6xty0n98QK9yQhU6k=" />
</div>

	

	<div id="page">
		
 

		

		<div class="adsplash-glb-header remove-empty"></div>

		

		

		
		

		<div id="pagecontent" class="master-content-wrap">
			
    
    
    <div class="toolbar-container"> 
        

<div class="breadcrumb-container clearfix">
    <div class="breadcrumb-row">
        <span id="ctl00_CP_Breadcrumb_SiteMapPath" class="breadcrumb" vocab="http://schema.org/" typeof="BreadcrumbList"><a href="#ctl00_CP_Breadcrumb_SiteMapPath_SkipLink"><img alt="Skip Navigation Links" height="0" width="0" src="/WebResource.axd?d=THQBzoq3MnzkY77NG-QeQu9gLDl1kzIWg-ZlXZ1W-qcr1zkfxKvL-d4-lN2DJ_OP7fGUFYj8u2iBQgpUK_gLVp62pBU1&amp;t=635588408026805809" style="border-width:0px;" /></a><span>
                <span property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage"
                    href='/en-CA/home.aspx?path=f9af28b84da40a62feed69e743160ef6en99'>
                  <span property="name">Home</span></a>
                <meta property="position" content='1'>
                </span>
            </span><span> : </span><span>
                <span property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage"
                    href='/en-CA/help/helpcentre/helpcentre.aspx?path=3b88ece438a998d7d2627c280b2a2c0fen07'>
                  <span property="name">HelpCentre</span></a>
                <meta property="position" content='2'>
                </span>
            </span><span> : </span><span>
                <span property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage"
                    href='/en-CA/help/policies/policies.aspx?path=e169c1d73e5cba143a05c1669ee89683en07'>
                  <span property="name">Policies</span></a>
                <meta property="position" content='3'>
                </span>
            </span><span> : </span><span class="breadcrumb-current">
                Privacy Policy
                <span property="itemListElement" typeof="ListItem">
                   <span property="item" typeof="WebPage">
                   <span class="hide" property="name">
                       Privacy Policy
                   </span>
                   </span>
                   <meta property="position" content='4'>            
                </span>
	        </span><a id="ctl00_CP_Breadcrumb_SiteMapPath_SkipLink"></a></span>
    </div>
</div>
        

<div class="main-toolbar">
    <div>
        <a id="ctl00_CP_ucCommonLinks_HypEmailFriend" class="email" href="/en-CA/secure/emailfriend.aspx?path=ff4051529f27c95126f54715bb6ced0aen99&amp;ET=Privacy%20Policy%20-%20Best%20Buy%20Canada">Email a Friend</a>
        <a id="ctl00_CP_ucCommonLinks_HypPrint" class="print" href="javascript:window.print();">Print</a>
        <a id="ctl00_CP_ucCommonLinks_HypBookmark" class="bookmark" href="javascript:AddCurrentPageToBookmark();">Bookmark</a>    
    </div>
</div>

    </div>
    <div class="toolbar-clear clear"></div>
    
<div class="clear2"></div>


    <div id="contentleft3">    
        
    <div class="help-detail-images">
        
    </div>

        


<div class="help-detail-content">
    <div class="lnkbackto"><a id="ctl00_CP_HelpDetailsUC1_lnkBackTo" href="javascript:history.go(-1);"></a></div>
    <h1>Privacy Policy</h1>

    

    <div class="content-pmessage">
        
                    
                    
                <p><a name="top"></a><a href="#privacy">Our Privacy Commitment</a></p>
<p><a href="#infocollect">What Information does Best Buy Collect?</a></p>
<p><a href="#infoused">How is Your Personal Information Used?</a></p>
<p><a href="#yourconsent">Your Consent</a></p>
<p><a href="#infodirectlyfromyou">Information Best Buy Collects Directly from You</a></p>
<p><a href="#infocollectauto">Information Best Buy Collects Automatically</a></p>
<p><a href="#dataonreturn">Data on Returned Product</a></p>
<p><a href="#supplementinginfo">Supplementing Information</a></p>
<p><a href="#shareyourinfo">Does Best Buy Share Your Information?</a></p>
<p><a href="#protectyourinfo">How does Best Buy Protect Your Information?</a></p>
<p><a href="#verifyinfo">How Can You Verify Your Information?</a></p>
<p><a href="#inforemoved">When is Your Information Removed?</a></p>
<p><a href="#contactbby">Contacting Best Buy</a></p>
<p><a href="#whois">Who is Best Buy?</a></p>
<p><a href="#policychange">Will this Privacy Policy Change?</a></p>
<p> </p>
<p><strong><a name="privacy"><strong>Our Privacy Commitment</strong></a><br />
</strong>At Best Buy we know how important it is to protect your personal information. Whether you are shopping with us online or in person we want to make every customer experience safe and secure. In keeping with that goal, we have developed this Privacy Policy to explain our practices for the collection, use, and disclosure of your personal information.</p>
<p>This Policy applies to our retail stores, our Web Sites, and all other channels in which Best Buy collects and uses your personal information.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="infocollect"><strong>What Information does Best Buy Collect?</strong></a><br />
</strong>At Best Buy, we collect two types of information: personal information and non-personal information. The types of information we collect depend on the nature of your interaction with us.</p>
<p><strong>Personal information</strong><br />
Personal information is information that can identify an individual or information about an identifiable individual. We may collect personal information such as:</p>
<ul>
    <li>your contact information - e.g., name, postal address, telephone number, and email address; </li>
    <li>your personal preferences - e.g., product wish lists, language preferences, and marketing consent; and </li>
    <li>your transaction information - e.g., products purchased, method of payment, amount paid, and credit or debit card number. </li>
</ul>
<p><strong>Non-personal information<br />
</strong>Non-personal information does not identify you as an individual. For example:</p>
<ul>
    <li>we may ask you for your postal code to assist us with planning our flyer distribution; </li>
    <li>we may collect certain non-personal data when you visit our Web Sites, such as the type of browser you are using or the referring URL; or </li>
    <li>we may collect and summarize customer information in a non-personal, aggregate format for statistical and research purposes. We may, for example, summarize our data to determine that a certain percentage of contest entrants are male, aged 18 to 25. </li>
</ul>
<p>All references to "Web Sites" in this Privacy Policy mean www.bestbuy.ca, www.geeksquad.ca, or other Best Buy provided Web sites.</p>
<p>You may choose not to provide us with your personal information. However, if you make this choice we may not be able to provide you with the product, service, or information that you requested. For example, we can only offer you a Best Buy credit card if you have provided us with certain financial information.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="infoused"><strong>How is Your Personal Information Used?</strong></a><br />
</strong>Best Buy may use your personal information for a number of different business purposes, for example to:</p>
<ul>
    <li>fulfill orders and requests for products, services or information; </li>
    <li>process returns, exchanges and layaway requests; </li>
    <li>detect and protect against fraud and error; </li>
    <li>establish and manage your accounts with us; </li>
    <li>track and confirm online orders; </li>
    <li>deliver or install products; </li>
    <li>manage our Reward Zone program; </li>
    <li>process Best Buy credit card applications; </li>
    <li>track and analyze your purchases and preferences to better understand your product and service needs and eligibility; </li>
    <li>market and advertise products and services; </li>
    <li>communicate things like special events, promotions and surveys; </li>
    <li>tailor our online content or in-store offerings to you based on your interactions with us and your preferences; </li>
    <li>conduct research and analysis; </li>
</ul>
<ul>
    <li>operate, evaluate and improve our business; and </li>
    <li>for other purposes as described in this Privacy Policy. </li>
</ul>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="yourconsent"><strong>Your Consent</strong></a><br />
</strong>Best Buy will collect, use, or disclose your personal information only with your knowledge and consent, except where required or permitted by law. When you choose to provide us with your personal information you consent to the use of your personal information as identified in this Privacy Policy and as may be further identified at the time of collection.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="infodirectlyfromyou"><strong>Information Best Buy Collects Directly from You</strong></a><br />
</strong>Listed here are some further examples of the ways that we collect personal information directly from you and how we use it.</p>
<p style="margin-left: 30px;"><strong>In-Store Purchases<br />
</strong>When you purchase a Best Buy product or service, you may need to provide us with contact and payment information (such as credit card information) so that we can process your request or complete your transaction. Examples where we need contact information include delivery services, product servicing, in-home installations, and extended warranty coverage.</p>
<p style="margin-left: 30px;"><strong>Returns and Exchanges<br />
</strong>Our goal is to ensure that you are completely satisfied with your purchase and to this end, Best Buy offers a Return Policy. For Best Buy to continue to offer our Return Policy, we ask for personal information such as your name, address, and telephone number for the purposes of detecting and preventing fraud and error, or to communicate with you regarding your return or exchange, if necessary. You may also be asked to produce photo ID for verification purposes only.</p>
<p style="margin-left: 30px;"><strong>Applying for the Best Buy Credit Card<br />
</strong>When you apply for a Best Buy credit card, certain contact and financial information is collected and reviewed by our credit card provider, JPMorgan Chase &amp; Co. ("Chase"), to approve, establish and maintain credit card accounts. Chase has its own privacy policy, available at <a href="https://www.chase.com/online/canada/privacy-en.htm">https://www.chase.com/online/canada/privacy-en.htm</a>, which governs its relationship with you. We also encourage you to read their privacy statement provided with your credit card application.</p>
<p style="margin-left: 30px;"><strong>ReClaim Insurance Replacement<br />
</strong>When working with our ReClaim Consultants to replace goods that are covered under your insurance policy, we will need some basic information, such as your contact information, insurance claim number, and the name of your claim adjuster, so we can process your quote in a timely manner and bill your insurance company directly for you. </p>
<p style="margin-left: 30px;"><strong>Purchase Follow-Up and Surveys<br />
</strong>Following a purchase from some of our departments we may send you a follow-up letter to thank you for your business, or we may contact you to ensure that you are completely satisfied with the delivery, setup, or installation of your new product.</p>
<p style="margin-left: 30px;">We may use contact information collected in-store, online, or via our call centres to conduct occasional surveys and other customer research. These surveys are entirely voluntary and you may easily decline to participate. </p>
<p style="margin-left: 30px;"><strong>Our Web Sites<br />
</strong>Our Web Sites can be browsed anonymously. We use your browsing history and personal information to display interest-based advertising that is relevant to you. We place interest-based advertisements on our website as well as other third-party sites. The browsing history we use is collected by us and by third parties on our website. We may also share your anonymous browsing history from our website with third parties to provide relevant ads. They may combine this information with assumptions based on the other websites you have visited in order to tailor ads to you.</p>
<p style="margin-left: 30px;">To engage in certain special features or functions of our Web Sites, or to order products and services from our Web Sites, you may be asked to provide certain personal information, such as your name, email address, postal address, telephone number, or credit card information. We use this information to create your account, contact and correspond with you about your order, respond to your inquiries, and monitor functions of our Web Sites that you choose to use, such as Customer Reviews.</p>
<p style="margin-left: 30px;">If you choose to ship an order to a third party, you represent and warrant that you have obtained all necessary consents from the third party to permit Best Buy to collect, use, and disclose the third party’s personal information for the purposes of fraud prevention and processing and shipping the order.  </p>
<p style="margin-left: 30px;"><strong>Customer Reviews<br />
</strong>The Customer Review functionality is provided as a rating system that offers you the chance to share your thoughts on a product. These reviews are accessible to all Web Site visitors so please use discretion when posting information to them and do not post personal information.</p>
<p style="margin-left: 30px;"><strong>Sign-Up for Newsletters or Updates<br />
</strong>Best Buy offers a variety of newsletters and promotional communications. You may choose to stop receiving these communications at any time. Each marketing or promotional email we send to you will include a link or other method to quickly and easily unsubscribe and decline further similar emails. </p>
<p style="margin-left: 30px;"><strong>Participate in a Contest or Promotion<br />
</strong>From time to time, we may run contests or promotions. If you participate, you may be asked for contact information as well as additional optional survey information (for example, product preferences). Personal information from contest entries will be used only to administer the contest and contact you if you win. For research and analysis purposes, we may also summarize contest survey information in a manner that no longer identifies individual participants. All contests and promotions are subject to this Privacy Policy and the rules that will be available with each particular campaign. Some contests are conducted by third parties and will be subject to the third party’s policies.</p>
<p style="margin-left: 30px;"><strong>Email a Friend<br />
</strong>While visiting our Web Sites you may choose to use the "Email a Friend" tool which allows you to inform a friend about products of interest to you. The information you provide will be used to send a one-time email to your friend, inviting them to our Web Sites. We will not add your friend to a mailing list and will not send them further email unless they choose to receive such communications from us. When using the "Email a Friend" tool you are contacting your friend directly and as such you acknowledge that you are responsible for this correspondence and not Best Buy. If you provide us with personal information concerning another individual (e.g., email address), you represent and warrant that you have obtained all necessary consents from that individual to enable us to use that personal information for the identified purpose.</p>
<p style="margin-left: 30px;"><strong>Reward Zone Membership<br />
</strong>Loyalty and rewards programs, such as Reward Zone, include a component where we ask for your contact information so we can administer the program. Your membership application information, information about your transactions and redemptions, and any other information you provide from time to time will be used to administer your membership, communicate with you about your membership, and to better understand your preferences and eligibility for Best Buy’s products and services. Membership in Reward Zone will result in certain membership-related communications (such as notification of reward certificates, statement updates, and program updates). On occasion, and with your consent, Best Buy may also use this information to provide you with additional personalized marketing, notifications of sales and special events, and other exclusive member-only offers. If you do not wish to receive these additional special offers and notifications, you may unsubscribe at any time.</p>
<p style="margin-left: 30px;"><strong>Subscription Services and Activations<br />
</strong>Best Buy may offer activation of certain products and services like cellular plans, satellite services, or Internet services. These activations may require that certain contact and financial information be collected and provided to the respective service providers as part of the activation process. For example, a cellular carrier may need to perform a credit check prior to activation so a government-issued ID number and your credit card number may be provided to the carrier for that purpose.</p>
<p style="margin-left: 30px;"><strong>Call Centres<br />
</strong>Best Buy operates customer support call centres to accept and address your questions, concerns, or complaints. When you contact our call centres, we may ask for personal information to help us respond to your inquiry or to verify your identity. For example if you have a question about the status of a recent online order, we will ask for personal information to verify that you are the account holder. We may also monitor or record your telephone discussions with our customer support representatives for training purposes and to ensure service quality.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="infocollectauto"><strong>Information Best Buy Collects Automatically</strong></a><br />
</strong>In some cases, we automatically collect certain information.</p>
<p style="margin-left: 30px;"><strong>Our Web Sites<br />
</strong>When using our Web Sites, we may collect the Internet Protocol (IP) address of your computer, the IP address of your Internet Service Provider, the date and time you access our Web Sites, the Internet address of the Web site from which you linked directly to our Web Site, the operating system you are using, the sections of the Web Site you visit, the Web Site pages read and images viewed, and the content you download from the Web Site. This information is used for Web Site and system administration purposes and to improve the Web Sites.</p>
<p style="margin-left: 60px;"><strong>Cookies<br />
</strong>The Web Site uses "cookies", a technology that installs a small amount of information on a Web Site user's computer to permit the Web Site to recognize future visits using that computer. Cookies enhance the convenience and use of the Web Site. For example, the information provided through cookies is used to recognize you as a previous user of the Web Site (so you do not have to enter your language preference every time), to offer personalized Web page content and information for your use, to track your activity at the Web Site, to respond to your needs, and to otherwise facilitate your Web Site experience. You may choose to decline cookies if your browser permits, but doing so may affect your use of the Web Site and your ability to access certain features of the Web Site or engage in transactions through the Web Site.</p>
<p style="margin-left: 60px;"><strong>Web Site Tracking Information<br />
</strong>We may use web beacons or pixel tags to compile tracking information reports regarding Web Site user demographics, Web Site traffic patterns, and Web Site purchases. We may then provide these reports to advertisers and others. None of the tracking information in these reports can be connected to the identities or other personal information of individual users. For our own research purposes we may link tracking information with personal information voluntarily provided by Web Site users. Once such a link is made, all of the linked information is treated as personal information and will be used and disclosed only in accordance with this Privacy Policy.</p>
<p style="margin-left: 60px;"><strong>Links to Other Web Sites<br />
</strong>Our Web Site may contain links to other Web sites or Internet resources which are provided solely for your convenience and information. When you click on one of those links you are contacting another Web site or Internet resource. Best Buy has no responsibility or liability for, or control over, those other Web sites or Internet resources or their collection, use and disclosure of your personal information. We encourage you to read the privacy policies of those other Web sites to learn how they collect and use your personal information.</p>
<p style="margin-left: 30px;"><strong>In-Store</strong></p>
<p style="margin-left: 60px;"><strong>Video Surveillance<br />
</strong>Best Buy retail stores are under video surveillance for safety and loss prevention purposes. The recorded images of our customers are viewed only when required. There may also be occasion where video footage is used for general demographic or traffic flow analysis.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="dataonreturn"><strong>Data on Returned Product</strong></a><br />
</strong>When returning products which may contain stored or recorded personal information, like computers, PDAs, digital cameras, etc., you are responsible for deleting or removing all personal information and media from your product before you return it. Best Buy is not responsible for any use by a third party of personal data or media left behind on a returned item.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="supplementinginfo"><strong>Supplementing Information</strong></a><br />
</strong>From time to time we may supplement information you give us with information from other sources, such as information validating your address or other available information you have provided us. This is to help us maintain the accuracy of the information we collect and to help us provide better service.</p>
<p><a href="#top" name="top">Top</a></p>
<p><a name="shareyourinfo"><strong>Does Best Buy Share Your Information?<br />
</strong></a>Best Buy does not sell or rent our customers’ personal information to any other party. However, in the normal course of business we may share some of your personal information within our corporate family and with third parties acting on our behalf or as permitted or required by law.</p>
<p>Some of these operations may result in personal information collected by Best Buy being stored or processed outside of Canada and, as a result, your personal information may be accessible to law enforcement and regulatory authorities in accordance with the law of these foreign jurisdictions.</p>
<p style="margin-left: 30px;"><strong>Service Providers<br />
</strong>Best Buy may use third parties to manage one of more aspects of our business operations, including the processing or handing of personal information. When we do use an outside company, we use contractual or other appropriate means to ensure that your personal information is used in a manner that is consistent with this Privacy Policy.</p>
<p style="margin-left: 30px;">For example, we may share personal information with third parties to perform services on our behalf such as: fulfilling online orders, processing non-cash payments, sending marketing communications, servicing products, conducting research surveys, verifying and validating information that you have provided to us, delivering products, and providing customer support services.</p>
<p style="margin-left: 30px;">Best Buy has taken precautions to prevent the fraudulent use of your information on our Web Sites. Best Buy has adopted industry standard authentication procedures to ensure your protection while shopping with us online. This includes, but is not limited to, contacting financial institutions or credit reporting agencies to ensure the authenticity of your credit card and to validate your credit card billing information.</p>
<p style="margin-left: 30px;"><strong>Information Shared Amongst Best Buy, its Corporate Parents, Affiliates, Subsidiaries and Divisions<br />
</strong>Best Buy may share personal information gathered on our Web Sites with Best Buy retail stores (and vice versa) and amongst our corporate parents, subsidiaries, affiliates or divisions for internal business purposes in accordance with this Privacy Policy. If Best Buy shares your information within its corporate family, we will ensure that your information continues to be used only in accordance with this Privacy Policy and your expressed choices.</p>
<p style="margin-left: 30px;"><strong>Product Safety Recalls<br />
</strong>In the event we receive notice from a manufacturer of a product safety recall, we may provide your contact information, limited to name, address, and telephone number, to the manufacturer so that they may notify you of the recall and supply details of any replacement or repair programs. This transfer or your contact information will occur only for recalls that may impact your personal safety or impair the functionality of the product you purchased from us.</p>
<p style="margin-left: 30px;"><strong>Sale or Transfer of all or part of Best Buy<br />
</strong>Any information we have about you may be transferred or disclosed to a purchaser or prospective purchaser in the event of a sale, assignment, or other transfer of all or a portion of our business or assets. Should such a transfer occur, we will use reasonable efforts to try to ensure that the transferee uses your information in a manner that is consistent with this Privacy Policy.</p>
<p style="margin-left: 30px;"><strong>Legal Disclosure<br />
</strong>Best Buy may disclose your information as permitted or required by law. For example, we may be compelled to release information by a court of law or other person or entity with jurisdiction to compel production of such information. If we have reasonable grounds to believe information could be useful in the investigation of improper or unlawful activity, we may disclose information to law enforcement agencies or other appropriate investigative bodies.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="protectyourinfo"><strong>How does Best Buy Protect Your Information?</strong></a><br />
</strong>The security of your personal information is a high priority for Best Buy. We maintain appropriate safeguards and current security standards to protect your personal information, whether recorded on paper or captured electronically, against unauthorized access, disclosure, or misuse. For example, electronic records are stored in secure, limited-access servers; we employ technological tools like firewalls and passwords; and we ensure our employees are trained on the importance of maintaining the security and confidentiality of personal information.</p>
<p style="margin-left: 30px;"><strong>SSL Technology<br />
</strong>Our Web Sites use encryption technology, such as Secure Sockets Layer ("SSL"), to protect your personal information during data transport. SSL encrypts ordering information such as your name, address, and credit card number.</p>
<p style="margin-left: 30px;"><strong>Cybertrust Certification<br />
</strong>Our Web Sites have received a Cybertrust certificate to confirm that we have met with industry standard security protocols. For your online security, always verify you are on an authentic Best Buy Web Site by clicking on the Cybertrust logo located in the lower right footer of our homepage or by using the certificate validation tools provided in your Web browser.</p>
<p style="margin-left: 30px;"><strong>Choosing a Password<br />
</strong>When you create an online account on our Web Sites, you need to select a personal password. To maximize your level of protection, you should choose at least 6 characters including a combination of both letters and numbers. You are solely responsible for maintaining the secrecy of your password and any account information. Best Buy will never send an unsolicited communication asking you for your password or requesting that you update your account credentials. To learn more about email fraud, please <a href="http://www.bestbuy.ca/en-CA/help/how-can-i-learn-to-protect-myself-from-email-fraud/hc1146.aspx?path=58cd06432d03774f76f46c5982c096aeen06&amp;HelpCategoryId=Shopping%20Online&amp;CategoryName=Shopping%20Online&amp;ReferringPageTitle=Help%20Customer%20Service">click here.</a></p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="verifyinfo"><strong>How Can You Verify Your Information?</strong></a><br />
</strong>You may check your information to verify, update, or correct it. If you created an account on one of our Web Sites, you can access and change your online account profile yourself. You can also ask to review any of the information that we have retained, how we have used it, and to whom we have disclosed it at any time by contacting us as indicated below under the heading "Contacting Best Buy". Subject to certain exceptions prescribed by law, and provided we can authenticate your identity, you will be given reasonable access to your personal information, and will be entitled to challenge the accuracy and completeness of the information and to have it amended as appropriate.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="inforemoved"><strong>When is Your Information Removed?</strong></a><br />
</strong>We keep your information only as long as we need it for legitimate business purposes and to meet any legal requirements. We have retention standards that meet these parameters. We destroy your information when it is no longer needed, or we remove your personally identifiable information to render it anonymous.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="contactbby"><strong>Contacting Best Buy</strong></a><br />
</strong>Best Buy is responsible for all personal information under its control. Our Privacy Manager is accountable for Best Buy's compliance with the principles described here. If you have any questions, concerns or complaints about the privacy practices of our organization please contact us at the following:</p>
<p style="margin-left: 30px;">Email: <a href="https://www-ssl.bestbuy.ca/en-CA/secure/view-privacy-inquiry.aspx">Click here</a> (please read the Important Reminder below)<br />
Telephone: 1-866-237-8289<br />
Mail: Best Buy Canada Ltd.<br />
Attention: Privacy Manager<br />
8800 Glenlyon Parkway<br />
Burnaby, BC, V5J 5K3</p>
<p>We will respond to your request or investigate your concern as quickly as we can, but no later than 30 days from the date of receipt of your query.</p>
<p><strong>Important Reminder about email communications:<br />
</strong>Please remember that email sent over the public Internet is not secure. If you send an email directly to us from your own email account the contents will not be encrypted. We strongly recommend that you do not send sensitive information (like a credit card number) to us via unencrypted email. Best Buy is not responsible for any transmission by you of any personal information over the public Internet.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="whois"><strong>Who is Best Buy?</strong></a><br />
</strong>Best Buy, bestbuy.ca, Geek Squad, and geeksquad.ca are divisions of Burnaby-based Best Buy Canada Ltd., a wholly-owned subsidiary of Best Buy Co., Inc.</p>
<p><a href="#top" name="top">Top</a></p>
<p><strong><a name="policychange"><strong>Will this Privacy Policy Change? Last Update: June 20, 201</strong></a>4<br />
</strong>To accommodate changes in our services, changes in technology, and legal developments, this Privacy Policy may change over time without notice to you. We may add, change, or remove portions of the Privacy Policy when we feel it is appropriate to do so. We encourage you to review our Privacy Policy periodically. Each time you submit personal information or use our services you agree to be bound by the then current terms of the Privacy Policy. Whenever we update the Privacy Policy we will change the date to indicate when the changes were made.</p>
<p><a href="#top" name="top">Top</a></p>
            
            
       
        
            <p class="learnmore">
            Learn more about <a id="ctl00_CP_HelpDetailsUC1_hlCategoryNameLink" class="strong" href="/en-CA/help/policies/policies.aspx?path=e169c1d73e5cba143a05c1669ee89683en07">Policies</a></p>
        
        
    </div>
</div>
    

        <div class="clear"></div>
    </div>
 
    <div id="contentright3">
        <div id="ctl00_CP_PanelRight">
	       
	        

            <div class="std-bottommargin"><a id="ctl00_CP_hlContactUs" title="Contact Us" class="std-bottommargin" href="/en-CA/secure/contact-us.aspx?path=6f0efb11924efa3b38c5b7f41d16664een99">Contact Us</a></div>
        
</div> 
         
    </div>
    <div class="clear"></div>
    

            <div bby-global-overlay=""></div>   
            
            <div class="clear"></div>
            
        </div>
        <div class="fdn core-utilities core-components margin-xl-top">
                <div class="page-feedback-container border-enabled">
                    <div class="row">
                        <div class="small-4 columns small-centered">
                            <div class="page-feedback background-white">
                                <div class="page-feedback-link-container">
                                <a href="#" class="accent-blue" data-feedback="generic">Do you have feedback about this page?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    


<!-- Begin Small Enhancement Utils -->
	<script type="text/javascript" src="http://static.bbycastatic.ca/scripts/se-modules/_utils/dist/modal.min.js?v=2.31"></script>
	<script type="text/javascript" src="http://static.bbycastatic.ca/scripts/se-modules/_utils/dist/cookie.min.js?v=2.31"></script>
<!-- End Small Enhancement Utils -->

<!-- Begin ConfirmIt -->

	<script type="text/javascript" src="http://static.bbycastatic.ca/scripts/se-modules/confirmit/dist/confirmit.min.js?v=2.31"></script>
    <script>$.confirmIt().init();</script>

<!-- End ConfirmIt -->

<!-- Begin OpinionLab -->


<div id="UserFeedback_tab" class="UserFeedback_tab_right" data-feedback="generic">
	<span>
		<a href="#" class="opinionlab-sidebar">
            Rate This Page
		</a>
	</span>
</div>
<!--[if IE 8]>
  <script type="text/javascript">
    // this is only executed for IE 8
    // because css media query is not supported by that browser
     
    (function(){
        var toggleSidebar = function(){
            if ($( window ).width() < 1010) {
                $('#UserFeedback_tab').hide();
            } else {
                $('#UserFeedback_tab').show();
            }

        };
        $(document).ready(toggleSidebar);
        $(window).resize(toggleSidebar);
        
    
    
    })();
    
          
  </script>
<![endif]-->

    <script type="text/javascript" src="http://static.bbycastatic.ca/scripts/se-modules/_vendors/opinionlab/oo_engine.min.js?v=2.31"></script>
	<script type="text/javascript" src="http://static.bbycastatic.ca/scripts/se-modules/opinionlab/dist/opinionlab.min.js?v=2.31"></script>
    <script>$.opinionLab().init();</script>
 
<!-- End OpinionLab -->


		

		

	</div>

	<input type="hidden" name="ctl00$hidSelectedProductIds" id="ctl00_hidSelectedProductIds" />
	<input type="hidden" name="ctl00$hidSelectedProductCount" id="ctl00_hidSelectedProductCount" value="0" />
	<input type="hidden" name="ctl00$hidNotEnoughProductsMessage" id="ctl00_hidNotEnoughProductsMessage" value="Sorry, you must select at least (2) products to compare." />
	<input type="hidden" name="ctl00$hidTooManyProductsMessage" id="ctl00_hidTooManyProductsMessage" value="The maximum number of products you can compare is {0}. Please try again" />
	<input type="hidden" name="ctl00$hidMaxComparableProducts" id="ctl00_hidMaxComparableProducts" value="5" />
	<input type="hidden" name="ctl00$hidMinComparableProducts" id="ctl00_hidMinComparableProducts" value="2" />

    <!-- NGA Scripts-->
    <script src="http://static.bbycastatic.ca/scripts/core-assets/scripts/core-app.min.js?v=2.36"></script>

    
    <!-- endinject -->


    <!--NGA Core Scripts END-->

	<!--Selected product IDs for compare-->
	<input type="hidden" id="hidSelectedProductIds_ID" name="hidSelectedProductIds_ID" value="" />
	<!--Selected product count for compare-->
	<input type="hidden" id="hidSelectedProductCount_ID" name="hidSelectedProductCount_ID" value="" />
	<!--Not enough products selected message-->
	<input type="hidden" id="hidNotEnoughProductsMessage_ID" name="hidNotEnoughProductsMessage_ID" value="" />
	<!--Too many products selected message-->
	<input type="hidden" id="hidTooManyProductsMessage_ID" name="hidTooManyProductsMessage_ID" value="" />
	<!--Maximum products for comparison-->
	<input type="hidden" id="hidMaxComparableProducts_ID" name="hidMaxComparableProducts_ID" value="" />
	<!--Minimum products for comparison-->
	<input type="hidden" id="hidMinComparableProducts_ID" name="hidMinComparableProducts_ID" value="" />

	



<script type="text/javascript" src="http://static.bbycastatic.ca/scripts/vendor/equal-height.min.js"></script>
<script type="text/javascript" src="http://static.bbycastatic.ca/scripts/theme-layout/theme-layout-init.js"></script>


<!-- Begin comScore Tag -->
<script type="text/javascript">

  if(!window.config.isKiosk) {
    steal(
  		(document.location.protocol == 'https:' ? 'https://sb' : 'http://b') + '.scorecardresearch.com/beacon.js'
  	).then(function(){
  			COMSCORE.beacon({ c1: 2, c2: 6034726, c3: '', c4: 'www.bestbuy.ca' + '\x2fhelpcustomerservice\x2fhelpdetails.aspx\x3fLang\x3den-CA\x26HelpTitleId\x3dhc1139', c5: '', c6: '', c15: '' });
  	});
  }
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/b?c1=2&c2=6034726&c3=&c4=www.bestbuy.ca%2fhelpcustomerservice%2fhelpdetails.aspx%3fLang%3den-CA%26HelpTitleId%3dhc1139&c5=&c6=&c15=&cv=1.3&cj=1" style="display:none" width="0" height="0" alt="" />
</noscript>
<!-- End comScore Tag -->
	

<script type="text/javascript">
//<![CDATA[
$get('hidSelectedProductIds_ID').value = 'ctl00$hidSelectedProductIds';$get('hidMaxComparableProducts_ID').value = 'ctl00$hidMaxComparableProducts';$get('hidNotEnoughProductsMessage_ID').value = 'ctl00$hidNotEnoughProductsMessage';$get('hidSelectedProductCount_ID').value = 'ctl00$hidSelectedProductCount';$get('hidTooManyProductsMessage_ID').value = 'ctl00$hidTooManyProductsMessage';$get('hidMinComparableProducts_ID').value = 'ctl00$hidMinComparableProducts';//]]>
</script>
<script type="text/javascript">
    var data_layer = {"section":"Main","template":null,"subTemplate":null,"breadcrumb2":"HelpCentre","breadcrumb3":"Policies","breadcrumb4":"Privacy Policy","breadcrumb5":null,"breadcrumb6":null,"language":"English","recognized":"Anonymous","pageIdentifier":"Privacy Policy","storeID":null,"catID":null,"prodRating":null,"numProdReviews":null,"prodID":[],"prodPrice":[],"prodQty":[],"prodSavings":[],"prodDiscount":[],"prodTotalSavings":[],"prodName":[],"prodShippingPrice":[],"productBundle":[],"bundlePrice":[],"bundleQty":[],"bundleName":[],"bundleSavings":[],"bundleDiscount":[],"bundleTotalSavings":[],"orderLevelDiscount":null,"productType":[],"skuType":[],"daysToDeliver":[],"brandStore":null,"brand":null,"findingMethod":null,"event":"faqView","paymentType":null,"fulfillmentType":[],"promoCode":[],"searchTerm":null,"searchTermFailed":null,"searchType":null,"numSearchResults":null,"faqTitle":"Privacy Policy","postalCode":null,"errorCode":[],"sellerID":null,"sellerName":null,"vendorType":null,"marketplace":null,"rzID":null,"purchaseID":null,"loginID":null,"userPostalCode":null,"userCity":null,"userProvince":null,"collectionValues":null,"collectionName":null};
</script>

<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>

    
        <script type="text/javascript" src="http://static.bbycastatic.ca/jmvc/vendor/moment/moment.min.js?v=2.36"></script>
        <script type="text/javascript">_satellite.pageBottom();</script>
    

</body>
</html>
