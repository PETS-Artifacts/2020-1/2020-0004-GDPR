

<!--- START Page Layout--->


<!DOCTYPE HTML>
<html xmlns:fb="http://ogp.me/ns/fb#" lang="en-US" charset="UTF-8">
<head>




 




<title>Privacy Statement | soapcentral.com</title>  





<!-- // START / Style Sheets \\ -->
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900' rel='stylesheet' type='text/css'>
	<link rel="Shortcut Icon" type="image/x-icon" href="http://soapcentral.com/images/favicon.ico">
	<link rel="stylesheet" href="/ss/ss_ix_2017_170522.css" type="text/css">	
	<link rel="stylesheet" href="/ss/ss_news_2016.css" type="text/css">	
	<link rel="stylesheet" href="/ss/ss_nav_2017.css" type="text/css">	
		
<!-- // START / Style Sheets \\ -->

	
	<script language="JavaScript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js" type="text/javascript"></script><script type="text/javascript">	var _rys = jQuery.noConflict(); _rys("document").ready(function(){ _rys(window).scroll(function () { if (_rys(this).scrollTop() > 87) { _rys('.nav_blue').addClass("f-nav"); } else { _rys('.nav_blue').removeClass("f-nav"); } }); });</script>
	<!-- END Style Sheet -->		
	

<!-- // START / META Tags \\ -->
	
	
		
	<meta name="viewport" content ="width=device-width,initial-scale=1,user-scalable=yes" />
	<meta name="publication" content="soapcentral.com" />
	
	<meta name="title" content="Privacy Statement" />
	
	
	<meta property="og:url" content="http://soapcentral.com/soapcentral/privacy.php" />
	<meta property="og:title" content="Privacy Statement" />
	<meta property="og:description" content="A soapcentral.com section devoted to soapcentral.com, featuring daily recaps dating back to 1996, scoops and spoilers, Two Scoops commentary, character and actor biographies, message boards, contests, games, and the latest news from the world of soaps, plus much more." />
    <meta property="og:type" content="article" data-meta-updatable/>
	<meta property="og:image" content="http://soapcentral.com/soapcentral/images/rect/logo.jpg" />	
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:image:width" content="720" />
	<meta property="og:image:height" content="420" />
	<meta property="og:site_name" content="soapcentral.com" />

	<meta property="fb:app_id" content="100557517607" />
		
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@soapcentral">	
	<meta name="twitter:creator" content="@DanJKroll">	
	<meta name="twitter:title" content="Privacy Statement" />
	<meta name="twitter:url" content="http://soapcentral.com/soapcentral/privacy.php" />
	<meta name="twitter:image" content="http://soapcentral.com/soapcentral/images/rect/logo.jpg" />
	<meta name="twitter:description" content="A soapcentral.com section devoted to soapcentral.com, featuring daily recaps dating back to 1996, scoops and spoilers, Two Scoops commentary, character and actor biographies, message boards, contests, games, and the latest news from the world of soaps, plus much more." />

	
		
	<meta http-equiv="refresh" content="2500" />	<meta name="Copyright" content="Copyright (c) 1995-2017 soapcentral.com" />
<!-- // START / META Tags \\ -->
	
	

<!-- // START / Javascript \\ -->
	
	<!-- START Advertisement / Reskin | Trigger: Gorilla Nation -->
	<script src="http://tags.evolvemediallc.com/websites/evolve_tags/14739"></script>
	<!-- END Advertisement / Reskin | Trigger: Gorilla Nation -->


	<!-- START Advertisement / Mobile Overlay Google -->
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<script>
	  (adsbygoogle = window.adsbygoogle || []).push({
	    google_ad_client: "ca-pub-9262634086419500",
	    enable_page_level_ads: true
	  });
	</script>
	<!-- END Advertisement / Mobile Overlay Google -->


	<!--- START // Google Tracker --->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-7167992-1', 'auto');
  ga('send', 'pageview');
</script>
	<!--- END // Google Tracker --->


	<!--- START // Taboola --->
	<script type="text/javascript">
  window._taboola = window._taboola || [];
  _taboola.push({article:'auto'});
  !function (e, f, u, i) {
    if (!document.getElementById(i)){
      e.async = 1;
      e.src = u;
      e.id = i;
      f.parentNode.insertBefore(e, f);
    }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/soapcentral/loader.js',
  'tb_loader_script');
	</script>
	<!--- END // Taboola --->
	
	
	<script language="JavaScript" src="/js/js.js" type="text/javascript" async="async"></script>	

<!-- // END / Javascript \\ -->

</head>
	
		
<body style="align:center; background:#fefefe; margin:0px auto; z-index: 3000">
<a name="top"></a>


<!-- /// Header | Logo and Social Media \\\ -->
<div style="width:100%; background:#fff; background-image:url('http://soapcentral.com/images/header/bg/bg_soc.gif'); text-align:center; margin:0px auto">
<div class="page_header">
<img id="sc_logo" src="/images/header/logo_sc_2017.gif" title="Soap Central"><a href="http://facebook.com/soapcentral" target="_new"><img src="/images/icons/circ_fb.gif" id="socialmedia"  title="Facebook"></a><a href="http://instagram.com/thesoapcentral" target="_new"><img src="/images/icons/circ_ig.gif" id="socialmedia"  title="Instagram @thesoapcentral"></a><a href="http://twitter.com/soapcentral" target="_new"><img src="/images/icons/circ_twitter.gif" id="socialmedia"  title="Twitter @soapcentral"></a><a href="https://www.youtube.com/channel/UC9IgOACIYO3pnqzlN8Cx7Sg" target="_new"><img src="/images/icons/circ_youtube.gif" id="socialmedia"  title="YouTube"></a></div>
</div>
<!-- /// Header | Logo and Social Media \\\ -->



<!-- /// Header | Navigation: Blue Options Menu \\\ -->
<script src="/mobile/ddmenu/ddmenu/ddmenu.js" type="text/javascript"></script>
<div style="width:100%; height:auto; background:#004e75">
<div id="page_nav_main" style="margin:0px auto">
<nav id="ddmenu">
    <div class="menu-icon"></div>
    <ul>
        <li class="full-width">
            <span class="top-heading">HOME/SOAPS</span>
            <!--- <i class="caret"></i> --->
            <div class="dropdown">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Home/Current Soaps</h3></li>
                        <li><a href="/soapcentral/index.php"><b>soap</b>central<b>.com</b> homepage</a></li>
                        <li><a href="/bb/index.php">The Bold and the Beautiful</a></li>
                        <li><a href="/days/index.php">Days of our Lives</a></li>
                        <li><a href="/gh/index.php">General Hospital</a></li>
                        <li><a href="/yr/index.php">The Young and the Restless</a></li>		
                    </ul>
                    <ul class="column mayHide">
      					<li><br /><a href="/soapcentral/thesoaps.php"><img src="/soapcentral/images/rect/sm/logo_soaps.jpg" style="width:240px; height:140px"/></a></li>
                    </ul>
                    <ul class="column">
					    <li><h3>Other Soaps</h3></li>
                        <li><a href="/amc/index.php">All My Children</a></li>
                        <li><a href="/aw/index.php">Another World</a></li>
                        <li><a href="/atwt/index.php">As the World Turns</a></li>
                        <li><a href="/gl/index.php">Guiding Light</a></li>
                        <li><a href="/oltl/index.php">One Life to Live</a></li>
                        <li><a href="/ps/index.php">Passions</a></li>
                        <li><a href="/pc/index.php">Port Charles</a></li>
                        <li><a href="/sb/index.php">Sunset Beach</a></li>
				    </ul>
                </div>
            </div>
        </li>
        <li class="full-width">
            <a class="top-heading" href="/soapcentral/recaps.php">RECAPS</a>
            <!--- <i class="caret"></i> --->
            <div class="dropdown">
                <div class="dd-inner">
                    <ul class="column">
                       	<li><h3>Current Daily Recaps</h3></li>
                        <li><a href="/soapcentral/recaps/2017/170515.php">Quick catch up weekly summary</a></li>
						<li><a href="/bb/recaps.php">The Bold and the Beautiful</a></li>
						<li><a href="/days/recaps.php">Days of our Lives</a></li>
						<li><a href="/gh/recaps.php">General Hospital</a></li>
						<li><a href="/yr/recaps.php">The Young and the Restless</a></li>						
                    </ul>
                    <ul class="column">
                        <li><h3>Archived Recaps</h3></li>
            	        <li><a href="/amc/archives.php">All My Children</a></li>
                	    <li><a href="/aw/archives.php">Another World</a></li>
						<li><a href="/atwt/archives.php">As the World Turns</a></li>
						<li><a href="/gl/archives.php">Guiding Light</a></li>
    	                <li><a href="/oltl/archives.php">One Life to Live</a></li>
						<li><a href="/ps/archives.php">Passions</a></li>
						<li><a href="/pc/archives.php">Port Charles</a></li>
						<li><a href="/sb/archives.php">Sunset Beach</a></li>		
					</ul>			
                </div>
            </div>
        </li>
        <li class="full-width">
            <a class="top-heading" href="/soapcentral/scoop.php">SCOOP</a>
            <!--- <i class="caret"></i> --->
            <div class="dropdown">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Previews, teasers, and spoilers</h3></li>
						<li><a href="/bb/scoop.php">The Bold and the Beautiful</a></li>
						<li><a href="/days/scoop.php">Days of our Lives</a></li>
						<li><a href="/gh/scoop.php">General Hospital</a></li>
						<li><a href="/yr/scoop.php">The Young and the Restless</a></li>		
                    </ul>
                    <ul class="column mayHide">
      										
                       <li><a href="/soapcentral/scoop.php"><img src="/soapcentral/recaps/2017/images/170522.jpg" style="width:240px; height:140px"/></a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="full-width">
            <a class="top-heading" href="/soapcentral/twoscoops.php">TWO SCOOPS</a>
			<!--- <i class="caret"></i> --->
            <div class="dropdown">
                <div class="dd-inner">
                    <ul class="column">
                       	<li><h3>Two Scoops Commentary</h3></li>
						<li><a href="/bb/content/scoop/twoscoops/2017/170522.php">The Bold and the Beautiful</a></li>
						<li><a href="/days/content/scoop/twoscoops/2017/170522.php">Days of our Lives</a></li>
						<li><a href="/gh/content/scoop/twoscoops/2017/170522.php">General Hospital</a></li>
						<li><a href="/yr/content/scoop/twoscoops/2017/170522.php">The Young and the Restless</a></li>				
                    </ul>
                    <ul class="column mayHide">
      										
                       <li><br /><a href="/soapcentral/twoscoops.php"><img src="/soapcentral/content/scoop/twoscoops/2017/170515.jpg" style="width:240px; height:140px"/></a></li>
                    </ul>
					<ul class="column">
                        <li><h3>Archived Commentary</h3></li>
   						<li><a href="http://soapcentral.com/amc/content/scoop/twoscoops/index.php">All My Children</a></li>
						<li><a href="http://soapcentral.com/atwt/content/scoop/twoscoops/index.php">As the World Turns</a></li>
						<li><a href="http://soapcentral.com/gl/content/scoop/twoscoops/index.php">Guiding Light</a></li>
						<li><a href="http://soapcentral.com/oltl/content/scoop/twoscoops/index.php">One Life to Live</a></li>
						<li><a href="http://soapcentral.com/ps/content/scoop/twoscoops/index.php">Passions</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="full-width">
			<a class="top-heading" href="http://boards.soapcentral.com" target="_new">BOARDS/CONNECT</a>
			<!--- <i class="caret"></i> --->
            <div class="dropdown">
                <div class="dd-inner">
                    <ul class="column">
                    <li><h3>Message Boards</h3></li>
                    <li><a href="http://boards.soapcentral.com/" target="_new">Visit Message Boards</a></li>
                    <li><a href="http://boards.soapcentral.com/blog.php" target="_new">Read/Create a soap blog</a></li>										
                    <li><a href="http://boards.soapcentral.com/register.php" target="_new">Register/Sign up (It's free!)</a></li>
                    <li><a href="http://boards.soapcentral.com/login.php?do=lostpw" target="_new">Lost password?</a></li>
					<li><a href="http://boards.soapcentral.com/usercp.php" target="_new">Email changed?</a></li>
                    </ul>
					<ul class="column">
                    <li><h3>soapcentral.com Social Media</h3></li>
					<li><img src="/images/header/logo_fb.gif" width="15" height="15" align="left" alt=""> &nbsp; <a href="http://facebook.com/soapcentral" target="_new">Facebook</a></li>
                    <li><img src="/images/header/logo_twitter.gif" width="15" height="15" align="left" alt=""> &nbsp; <a href="http://twitter.com/soapcentral" target="_new">Twitter (@soapcentral)</a></li>										
                    <li><img src="/images/header/logo_instagram.gif" width="15" height="15" align="left" alt=""> &nbsp; <a href="http://instagram.com/thesoapcentral" target="_new">Instagram (@thesoapcentral)</a></li>
                    <li><img src="/images/header/logo_periscope.gif" width="15" height="15" align="left" alt=""> &nbsp; <a href="http://periscope.tv/soapcentral" target="_new">Periscope</a></li>
                    <li><img src="/images/header/logo_snapchat.gif" width="15" height="15" align="left" alt=""> &nbsp; <a href="http://snapchat.com/soapcentral" target="_new">Snapchat</a></li>
					<li><img src="/images/header/logo_youtube.gif" width="15" height="15" align="left" alt=""> &nbsp; <a href="http://youtube.com/soapcentral" target="_new">Youtube</a></li>
					</ul>
                </div>
            </div>
        </li>					
		
        <li class="full-width">
			<a class="top-heading" href="/emmys/" target="_new">EMMYS</a>
			<!--- <i class="caret"></i> --->
            <div class="dropdown">
                <div class="dd-inner">
                    <ul class="column">
                    	<li><h3>2017 Daytime Emmys</h3></li>						
	                    <li><a href="/emmys/index.php">Full coverage</a></li>
	                    <li><a href="/emmys/news/2017/0430-emmy_coverage_video.php">Red carpet interviews</a></li>
<!--- 						<li><a href="/emmys/news/2016/0404-emmy_coverage_clips_01.php">Photos, livestreams, and more</a></li> --->
						<li><a href="/emmys/news/2017/0410-emmy_coverage_clips_01.php">Watch the Emmy reels</a></li>
						<li><a href="/emmys/news/2017/0424-emmy_predictions.php">Our predictions</a></li>
	 					<li><a href="/emmys/news/2017/0322-emmy_nominations_reaction.php">Reaction from the nominees</a></li>
	                    <li><a href="/emmys/news/2017/0322-emmy_nominations.php">The nominees</a></li>
                    </ul>
					<ul class="column">
						<li><br /><a href="/emmys/index.php"><img src="/emmys/images/rect/2017/logo_sm.jpg" style="width:240px; height:140px"/></a></li>
                    </ul>
					<ul class="column">
                    	<li><h3>Daytime Emmys Archive</h3></li>						
	                    <li><a href="/emmys/archives/index.php">Emmy Archive<br>(1971 to Present)</a></li>
	                    <li><a href="/emmys/archives/index.php">Winners by category</a></li>
						<li><a href="/emmys/news/2016/0501-emmy_coverage.php">2016 Emmy coverage</a></li>
	                    <li><a href="/emmys/news/2015/0426-emmy_coverage.php">2015 Emmy coverage</a></li>
                    </ul>
				</div>
            </div>
        </li>				
		
        <li class="no-sub"><a class="top-heading" href="/soapcentral/feedback.php">FEEDBACK</a></li>
		
        <li class="full-width">
            <a class="top-heading" href="/search/index.php">SEARCH</a>
            <!--- <i class="caret"></i> --->
            <div class="dropdown">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Search the Site</h3></li>
                        <li><form action="http://soapcentral.com/search/index.php" id="cse-search-box" STYLE="margin: 0px; padding: 0px;">
	<input type="hidden" name="cx" value="002945430885157946737:tzhwpytgxbg" />
	<input type="hidden" name="cof" value="FORID:11" />
	<input type="hidden" name="ie" value="UTF-8" />
	<input type="text" name="q" size="25" />
	<input type="image" SRC="http://soapcentral.com/images/icons/search_button.gif" align="absmiddle" BORDER="0" ALT="Submit Form" name="sa" value="Search" />
	</form>
	<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=cse-search-box&lang=en"></script></li>
                    </ul>
                </div>
            </div>
        </li>
		
				
    </ul>
</nav>
</div>
</div><!-- /// Header | Navigation: Blue Options Menu \\\ -->
	


<!-- /// Header | Navigation: Soaps \\\ -->
<div style="width:100%; height:auto; background:#8c315a">
<div id="page_nav_soaps">
<font id="nav_soaps_first"><a href="/soapcentral/index.php"><b>soap</b>central<b>.com</b></a></font>
<font id="nav_soaps"><a href="/bb/index.php">The Bold and the Beautiful</a></font>
<font id="nav_soaps"><a href="/days/index.php">Days of our Lives</a></font>
<font id="nav_soaps"><a href="/gh/index.php">General Hospital</a></font>
<font id="nav_soaps"><a href="/yr/index.php">The Young and the Restless</a></font>
<font id="nav_soaps_last"><a href="/soapcentral/thesoaps.php">Other Soaps</a></font>
</div>
</div>
<!-- /// Header | Navigation: Soaps \\\ -->



<!-- /// Header | Late-Breaking News \\\ -->
<div style="width:100%; margin-bottom:15px; background:#e07e88">
<div id="page_nav_lbn">
<a href="/bb/news/2017/0522-courtney_hope_interview.php">How B&B and GH stars started dating</a> |
<a href="/gh/news/2017/0517-finola_hughes_shocker.php">The secret an actress kept from everyone</a> |
<a href="/days/news/2017/0518-bill_hayes_world_by_the_tail.php">Watch FREE documentary about a daytime legend</a> |
<!--- <a href="/days/news/2017/00503-alison_sweeney_on_return.php">Alison Sweeney previews DAYS return</a> --->
<!--- <a href="/gh/news/2017/0501-roger_howarth_contract_talks.php">Roger Howarth out at GH?</a> --->
</div> 

<div id="page_nav_lbn">
<a href="/days/news/2017/0515-arianne_zucker_ladies_lake_interview.php">INTERVIEW: Ari Zucker talks DAYS exit, new gig</a> |
<a href="/gh/news/2017/0512-bradford_anderson_back_as_spinelli.php">Spinelli returning to GH</a> | 
<a href="/emmys/news/2017/0430-emmy_coverage_video.php">EXCLUSIVE VIDEO: Our Red Carpet interviews</a> | 
<!--- <a href="/emmys/index.php">2017 Daytime Emmys coverage</a>  --->
</div>
</div>



<div id="page_nav_lbn_02">
<font id="week">Week of May 22</font>: SCOOP &nbsp; <a href="/bb/content/scoop/spoilers/2017/170522.php">B&B</a> <a href="/days/content/scoop/spoilers/2017/170522.php">DAYS</a> <a href="/gh/content/scoop/spoilers/2017/170522.php">GH</a> <a href="/yr/content/scoop/spoilers/2017/170522.php">Y&R</a> 
&nbsp; &nbsp; | &nbsp; &nbsp; TWO SCOOPS COMMENTARY &nbsp; <a href="/bb/content/scoop/twoscoops/2017/170522.php">B&B</a> <a href="/days/content/scoop/twoscoops/2017/170522.php">DAYS</a> <a href="/gh/content/scoop/twoscoops/2017/170522.php">GH</a> <a href="/yr/content/scoop/twoscoops/2017/170522.php">Y&R</a>  
&nbsp; &nbsp; | &nbsp; &nbsp;<!--- <font id="week">Week of May 1</font>: ---> LAST WEEK'S RECAPS &nbsp; <a href="/bb/recaps/2017/170515.php">B&B</a> <a href="/days/recaps/2017/170515.php">DAYS</a> <a href="/gh/recaps/2017/170515.php">GH</a> <a href="/yr/recaps/2017/170515.php">Y&R</a>
<!--- &nbsp; &nbsp; | &nbsp; &nbsp; <font id="week">Week of Apr 24</font>:  TWO SCOOPS &nbsp; <a href="/bb/content/scoop/twoscoops/2017/170515.php">B&B</a> <a href="/days/content/scoop/twoscoops/2017/170515.php">DAYS</a> <a href="/gh/content/scoop/twoscoops/2017/170515.php">GH</a> <a href="/yr/content/scoop/twoscoops/2017/170515.php">Y&R</a> --->
</div>
</div><!-- /// Header | Late-Breaking News \\\ -->



<!-- /// Advertisement | 728x90 \\\ -->
<div id="page_ad_top"><!-- ROS 728x90 [Top] [javascript] -->
<script type="text/javascript">
var rnd = window.rnd || Math.floor(Math.random()*10e6);
var pid209754 = window.pid209754 || rnd;
var plc209754 = window.plc209754 || 0;
var abkw = window.abkw || '';
var absrc = 'http://ab168312.adbutler-chargino.com/adserve/;ID=168312;size=728x250;setID=209754;type=js;sw='+screen.width+';sh='+screen.height+';spr='+window.devicePixelRatio+';kw='+abkw+';pid='+pid209754+';place='+(plc209754++)+';rnd='+rnd+';click=CLICK_MACRO_PLACEHOLDER';
document.write('<scr'+'ipt src="'+absrc+'" type="text/javascript"></scr'+'ipt>');
</script></div>
<!-- /// Advertisement | 728x90 \\\ -->


<!-- ////// START News Content: Left Side \\\\\\ --->



<div id="page_size">

<div id="page_size_left">

<!--- END Page Layout--->


<!--- START Content Area --->
<div id="ft_profilename" style="color: #069; border-bottom: 1px solid #000">ABOUT</font>&nbsp;&nbsp;&nbsp;&nbsp;</td><td bgcolor=#99aaff>&nbsp;&nbsp;<font class="blocks">OUR PRIVACY STATEMENT</font>&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
<p>


Thank you for visiting the <b>soap</b>central<b>.com</b> site. Your privacy is important to us. In an effort to better protect your privacy, we provide this notice explaining our online information practices of collecting and disseminating information.
<br><br><br><br>


<font id="bold_plusone" color="#006699">THE INFORMATION WE COLLECT</font>
<br>Throughout the <b>soap</b>central<b>.com</b> site, you can enter contests, expression your opinions on message boards and forums, vote in polls, or subscribe to any or all of our newsletters. In order to access these features we ask that you supply us with personally identifiable information such as name, email address, and information about your interests in and use of various products, programs and services.
<br><br>
At some areas of the <b>soap</b>central<b>.com</b> site, you can submit information about other people. For example, if you choose to send a Soap Card (an electronic postcard), you might submit the recipient's name and email address. In this situation, the recipient's email address is required.
<br><br>
The <b>soap</b>central<b>.com</b> message boards feature an option that allows individual users to determine if they would like to share (display) their email address with other board visitors. This decision is up to you, the user, who will have the option to post your email address on and or all messages that you post. By default, the "Do Not Display My email Address" has been selected for all posters.
<br><br>
We also may collect certain non-personally identifiable information when you visit many of our web pages such as the type of browser you are using (e.g., Netscape, Internet Explorer), the type of operating system you are using, (e.g., Windows '95 or Mac OS) and the domain name of your Internet service provider                (e.g., America Online, Earthlink).
<br><br>
We use third-party advertising companies to serve ads when you visit our Web site.  The third-party advertising technology that we use on this Web site uses information derived from your visits to this site to target advertising within this site.  In addition, our advertisers may use other third-party advertising technology to target advertising on this site and other sites.  In the course of advertisements being served to you, a unique third-party cookie or cookies may be placed your browser.  In addition, we use Web beacons, provided by third-party advertising companies, to help manage and optimize our online advertising.  Web beacons enable us to recognize a browser's cookie when a browser visits this site, to learn which banner ads bring users to our Web site and to deliver more relevant advertising.  To "opt-out," please click here. http://www.doubleclick.com/privacy/index.aspx. 

<br><br><br><br>


<font id="bold_plusone" color="#006699">HOW WE USE THE INFORMATION WE COLLECT</font>
<br>First and foremost, the information we collect is used to enhance all of our visitor's experiences on the <b>soap</b>central<b>.com</b> web site. 
<br><br>
Another key aspect of collecting information, such as name and email address, is so that we can respond to the users of our site when they have technical problems or questions about a feature or other aspect of the web site.
<br><br>
We use the information that you provide about others to enable us to send them your gifts or cards. From time to time, we also may use this information to offer our products, programs, or services to them. 
<br><br>
The information we collect in connection with our online forums and communities is used to provide an interactive experience. We use this information to facilitate participation in these online forums and communities and, from time to time, to offer you products, programs, or services.
<br><br>
On occasion, we may use the non-personally identifiable information that we collect to improve the performance, design and content of our site. This information may also enable us to personalize your visits to the site and provide you with content and information that most interests you. 
<br><br>
By collecting non-personally identifiable information, we are also able to determine which features of the site are most visited. This information may be used to determine which features to enhance or remove from the site. We also may disclose such information to a third-party for advertising purposes.
<br><br>
We will disclose information we maintain when required to do so by law, for example, in response to a court order or a subpoena. We also may disclose such information in response to a law enforcement agency's request.
<br><br>
Representatives, agents and contractors of <b>soap</b>central<b>.com</b>  who have access to personally identifiable information are required to protect this information in a matter that is consistent with this Privacy Notice. For example, these parties may not use this information for any purpose other than to carry out the services they are performing for <b>soap</b>central<b>.com</b>.
<br><br><br><br>

<font id="bold_plusone" color="#006699">THIRD-PARTY AND SPONSOR SITES</font>
<br>Some areas of <b>soap</b>central<b>.com</b> contain links to other Internet sites whose information practices may be different their ours. Visitors should consult the other sites' privacy notices as we have no control over information that is submitted to, or collected by, these third parties.
<br><br>
<b>soap</b>central<b>.com</b>  sometimes may offer contests, sweepstakes, or promotions that are sponsored by or co-sponsored with identified third parties. By virtue of their sponsorship, these third parties may obtain personally identifiable information that visitors voluntarily submit to participate in the contest,   sweepstakes, or promotion. <b>soap</b>central<b>.com</b> has no control over the third-party sponsors' use of this information. <b>soap</b>central<b>.com</b> will notify you at the time of requesting personally identifiable information if third-party sponsors will obtain such information.
<br><br><br><br>


<font id="bold_plusone" color="#006699">COOKIES</font>
<br>Cookies are text files we place in your computer's browser to store  your preferences. Cookies, by themselves, do not tell us your e-mail address or other personally identifiable information unless you choose to provide this information to us by, for example, registering for one of our contests, the message board or other features that require the use of an email address.
<br><br>
We use cookies to understand site usage and to improve the content and offerings on our sites. For example, we may use cookies to personalize your experience at our web pages (e.g. to recognize you by name when you return to our site), save your password in password-protected areas, and enable you to
use shopping carts on our sites. We also may use cookies to offer you products, programs, or services.
<br><br><br><br>

<font id="bold_plusone" color="#006699">COMMITTED TO SECURITY</font>
<br>We have put in place appropriate physical, electronic, and managerial procedures to safeguard and help prevent unauthorized access, maintain data security, and correctly use the information we collect online.
<br><br><br><br>



<font id="bold_plusone" color="#006699">WHAT TO DO IF YOUR INFORMATION NEEDS TO BE UPDATED OR CORRECTED</font>
<br>Instructions on how to edit any of your personally identifiable information is available at the point of collection. Should you need to correct or edit any of your personally identifiable information, you may contact us by <a href="/soapcentral/feedback.php?feedback=Privacy">Feedback</a>.  For verification purposes please include your first name, last name, e-mail address and the password you use on the <b>soap</b>central<b>.com</b> site.
<br><br><br><br>

<font id="bold_plusone" color="#006699">HOW TO CONTACT US</font>
<br>If you have any questions or concerns about the <b>soap</b>central<b>.com</b> Privacy Policy or its implementation you may contact us by <a href="/soapcentral/feedback.php?feedback=Privacy">Feedback</a>.

<br><br>
</div></font>





<!--- START Footer --->

</td>
	</tr>
</table>
</td><td width="5" background="/images/page_bg_right.jpg"><img src="/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
</tr></table>


</td>
</tr>
</table>



</td>
</tr>
</table>




<!--- START Second Third // Advertisement and In Other News  --->
<!--- START Second Third // Advertisement and In Other News  --->


<table width="1000px" height="8px" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="5px" background="http://soapcentral.com/images/page_bg_left.jpg"><img src="http://soapcentral.com/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
		<td width="990px" align="center" bgcolor="#ffffff">
<td width="5" background="http://soapcentral.com/images/page_bg_right.jpg"><img src="http://soapcentral.com/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
</tr>
</table>



<table width="1000px" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="5px" background="http://soapcentral.com/images/page_bg_left.jpg"><img src="http://soapcentral.com/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
		<td width="990px" align="center" bgcolor="#ffffff">
		
<!--- START In Other News  --->
<!--- START In Other News  --->

<table width="970" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" class="newsothersoaps"> 

<tr><td width="980" height="15" bgcolor="#006699" colspan="9" class="newsothersoaps"><font class="hline">&nbsp;&nbsp;Other stories making news on soap</font><font class="hline-nonbold">central</font><font class="hline">.com</font></td></tr>
<tr><td width="980" height="5" colspan="9"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="1"></td></tr>

<tr>
<td width="7"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td>
<td width="230" valign="top">

<!--- START AMC Headlines --->
<table width="230" border="0" cellpadding="0" cellspacing="0">
<tr><td width="230" colspan="2"><font class="soapname">All My Children</font></td></tr>
<tr><td width="230" height="1" bgcolor="#000000" colspan="2"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="230"></td></tr>
<tr><td width="10" height="5" bgcolor="#880000"><img src="http://soapcentral.com/images/spacer.gif" width="10"></td><td width="220" height="5" bgcolor="#eeeeee" align="left"><font class="opt">&nbsp;<a href="http://soapcentral.com/amc/index.php">FrontPage</a> <font color="#888888">|</font> <a href="http://soapcentral.com/amc/recaps.php">Recaps</a> <font color="#888888">|</font> <a href="http://soapcentral.com/amc/scoop.php">Scoop</a> <font color="#888888">|</font> <a href="http://boards.soapcentral.com/forumdisplay.php?f=3" target="_boards"">Board</a></font></td></tr>
<!--- <tr><td colspan="2"><img src="http://soapcentral.com/images/spacer.gif" width="1" height="5"></td></tr> --->
<tr><td colspan="2"><div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0511-jennifer_bassey_to_gh.php">AMC alum Jennifer Bassey joins GH</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0510-anna_alex_devane.php">Who's Alex Devane? Your ultimate guide to Anna's twin sister</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0426-mark_consuelos_riverdale.php">Mark Consuelos cast on Riverdale, but series may be forced to drop him</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0424-michael_b_jordan_fahrenheit_451.php">AMC alum Michael B. Jordan cast in HBO film Fahrenheit 451</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0418-clifton_james_obit.php">AMC and Dallas actor Clifton James dead at 96</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0407-mark_consuelos_night_shift.php">AMC's Mark Consuelos lands major Night Shift role</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0317-amanda_seyfried_mischa_barton_xxx_scandal.php">Two AMC alums fight release of explicit images</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0316-eva_larue_mom_cancer.php">Eva LaRue opens up about her mom's battle with cancer</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0310-kelly_ripa_makes_history.php">AMC alum Kelly Ripa makes TV history</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0310-finn_wittrock_caa.php">AMC alum Finn Wittrock signs with CAA</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0306-melissa_claire_egan_mystery_project.php">Melissa Claire Egan working on mystery project</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/content/scoop/twoscoops/2017/170220.php">Let's talk about those AMC revival rumors</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0224-josh_duhamel_unsolved_biggie_2pac.php">Josh Duhamel lands Tupac-Biggie murder pilot Unsolved</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0220-amc_crossovers_for_gh.php">Exec hints AMC characters headed to GH</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0213-vincent_irizarry_out.php">Vincent Irizarry out at Days of our Lives</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0215-jacob_young_las_vegas.php">B&B's Jacob Young hits Vegas for his first musical concert</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0206-amc_netflix.php">Is AMC headed to Netflix?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0208-richard_hatch_obit.php">AMC's Richard Hatch dead at 71</a></div>	
</td></tr>
</table>
<!--- END AMC Headlines --->


</td><td width="12"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td>
<td width="230" valign="top">

<!--- START ATWT Headlines --->
<table width="230" border="0" cellpadding="0" cellspacing="0">
<tr><td width="230" colspan="2"><font class="soapname">As the World Turns</font></td></tr>
<tr><td width="230" height="1" bgcolor="#000000" colspan="2"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="230"></td></tr>
<tr><td width="10" height="5" bgcolor="#2e434f"><img src="http://soapcentral.com/images/spacer.gif" width="10"></td><td width="220" height="5" bgcolor="#eeeeee" align="left"><font class="opt">&nbsp;<a href="http://soapcentral.com/atwt/index.php">FrontPage</a> <font color="#888888">|</font> <a href="http://soapcentral.com/atwt/recaps.php">Recaps</a> <font color="#888888">|</font> <a href="http://soapcentral.com/atwt/scoop.php">Scoop</a> <font color="#888888">|</font> <a href="http://boards.soapcentral.com/forumdisplay.php?f=4" target="_boards"">Board</a></font></td></tr>
<!--- <tr><td colspan="2"><img src="http://soapcentral.com/images/spacer.gif" width="1" height="5"></td></tr> --->
<tr><td colspan="2"><div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0315-jennifer_landon_joins_days_report.php">Jennifer Landon joins DAYS</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/atwt/news/2017/0303-actress_nearly_fired_over_coffee.php">ATWT actress nearly fired for lousy coffee pouring skills</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2017/0209-cbs_daytime_class_photo.php">WATCH: CBS Daytime gathers 100+ stars for �class photo�</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/atwt/news/2017/0120-courtney_simon_wga.php">WGA East to honor soap opera writer and actress Courtney Simon</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0112-amanda_seyfriend_pregnant.php">ATWT alum Amanda Seyfriend expecting first baby</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2016/1224-cady_mcclain_michelle_stafford_single_mom.php">Cady McClain guests on Single Mom A Go-Go</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2016/1110-will_chase_nashville.php">ATWT alum Will Chase confirms return to Nashville</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2016/1028-daniel_cosgrove_exit_interview.php">INTERVIEW: Daniel Cosgrove dishes on Project Dad and his DAYS exit</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/atwt/news/2016/1024-martha_byrne_in_the_car_with_blossom_and_len.php">ATWT alum Martha Byrne returns to the stage</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2016/1021-daniel_cosgrove_out_lands_new_job.php">Daniel Cosgrove exits DAYS; lands primetime comedy</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2016/0929-cbs_daytime_crossover_30_years.php">CBS Daytime celebrates 30 years at #1 with crossover episodes, Paley Center exhibit</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2016/0927-mcclain_seeing_is_believing.php">Cady McClain's new project on amazing women</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2016/0903-harmon_kay.php">Emmy winner Lesli Kay back to B&B</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2016/0901-devious_maids.php">Cancellation Shocker: Lifetime pulls the plug on Devious Maids</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2016/0805-mcclain_message.php">Can't Miss: Cady McClain shares beautiful message with fans</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/atwt/news/2016/0706-eastsiders.php">Soap stars' critically acclaimed web series, Eastsiders, hits Netflix</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2016/0603-dawson.php">PHOTO: Catch the first glimpse of ATWT alum Trent Dawson as GH's Huxley</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2016/0324-evans_emmynom.php">DAYS Mary Beth Evans celebrates very first (and second!) Emmy nomination</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2016/0209-buchanan_return.php">Jensen Buchanan returning as Y&R's Judge Elise Moxley</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2015/1101-marston.php">Nathaniel Marston critically injured in car accident</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2015/1008-mcclain_birthday.php">Cady McClain's birthday surprise</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/atwt/news/2015/0918-hubbard_anacostia.php">Iconic soap opera actress Elizabeth Hubbard joins Emmy-winning series</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/atwt/news/2015/0223-moore_oscar.php">Former soap star takes home the Oscar for Best Actress</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/atwt/news/2014/1006-chase_cpd.php">Bailey Chase lands multi-episode Chicago P.D. role</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="http://soapcentral.com/atwt/news/2014/0329-eastsiders_comic.php">Van Hansis web series gets comic treatment</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="http://soapcentral.com/yr/news/2014/0225-mcclain_watros.php">ATWT vets McClain, Lindstrom wed</a></div>

</td></tr>
</table>
<!--- END ATWT Headlines --->



</td><td width="12"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td>
<td width="230" valign="top">


<!--- START BB Headlines --->
<table width="230" border="0" cellpadding="0" cellspacing="0">
<tr><td width="230" colspan="2"><font class="soapname">The Bold and the Beautiful</font></td></tr>
<tr><td width="230" height="1" bgcolor="#000000" colspan="2"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="230"></td></tr>
<tr><td width="10" height="5" bgcolor="#cc6666"><img src="http://soapcentral.com/images/spacer.gif" width="10"></td><td width="220" height="5" bgcolor="#eeeeee" align="left"><font class="opt">&nbsp;<a href="http://soapcentral.com/bb/index.php">FrontPage</a> <font color="#888888">|</font> <a href="http://soapcentral.com/bb/recaps.php">Recaps</a> <font color="#888888">|</font> <a href="http://soapcentral.com/bb/scoop.php">Scoop</a> <font color="#888888">|</font> <a href="http://boards.soapcentral.com/forumdisplay.php?f=5" target="_boards"">Board</a></font></td></tr>
<!--- <tr><td colspan="2"><img src="http://soapcentral.com/images/spacer.gif" width="1" height="5"></td></tr> --->
<tr><td colspan="2">
<div class="topstory">
<div class="shell"><div class="boxes"><div id="img"><a href="/bb/news/2017/0522-courtney_hope_interview.php"><img src="/bb/images/rect/hope_courtney.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#c66">THE BOLD AND THE BEAUTIFUL</font></div><div class="headline"><a href="/bb/news/2017/0522-courtney_hope_interview.php">INTERVIEW: Courtney Hope reveals how she prepared to play B&B's Sally</a></div></div></div>
</div>


<div class="shell"><div class="boxes"><div class="img"><a href="/bb/news/2017/0511-linsey_godfrey_bb_status.php"><img src="/bb/images/rect/godfrey_linsey.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0511-linsey_godfrey_bb_status.php">Linsey Godfrey reveals she's NOT returning to B&B</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/bb/content/scoop/twoscoops/2017/170522.php"><img id="img" src="/bb/content/scoop/twoscoops/2017/170522_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#c66">THE BOLD AND THE BEAUTIFUL</font></div><div class="headline"><a href="/bb/content/scoop/twoscoops/2017/170522.php">TWO SCOOPS: Do you agree with Brooke's choice of groom?</a></div></div></div>

<div class="shell" style="display:none"><div class="boxes"><div class="img"><a href="/bb/content/scoop/spoilers/2017/170522.php"><img id="img" src="/bb/content/scoop/spoilers/2017/170522_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#c66">THE BOLD AND THE BEAUTIFUL</font></div><div class="headline"><a href="/bb/content/scoop/spoilers/2017/170522.php">B&B SPOILER ALERT: Sally is overwhelmed with guilt by what Thomas has given up</a></div></div></div>

<div class="shell" style="display:none"><div class="boxes"><div class="img"><a href="/soc/recaps/2017/170515.php"><img id="img" src="/bb/recaps/2017/images/170515_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#c66">THE BOLD AND THE BEAUTIFUL</font></div><div class="headline"><a href="/soc/recaps/2017/170515.php">LAST WEEK ON B&B: Brooke followed her heart and married Bill</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/bb/news/2017/0519-karla_mosley_lawrence_saint_victor_wedlocked.php"><img src="/bb/news/2017/0519-karla_mosley_lawrence_saint_victor_wedlocked_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#c66">THE BOLD AND THE BEAUTIFUL</font></div><div class="headline"><a href="/bb/news/2017/0519-karla_mosley_lawrence_saint_victor_wedlocked.php">B&B's Lawrence Saint-Victor and Karla Mosley resurrect Wed-Locked</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0512-kyle_lowder_ladies_lake_interview.php">INTERVIEW: Kyle Lowder opens up about the making of Ladies of the Lake</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/emmys/news/2017/0430-video_bell.php">VIDEO: Brad Bell on why B&B is so popular -- and he takes on a dare on the Red Carpet</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/emmys/news/2017/0430-emmy_coverage_video.php">VIDEO: Exclusive Daytime Emmys Red Carpet interviews with B&B stars</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2017/0504-wga_strike_averted.php">Writers strike averted after last minute deal</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0327-karla_mosley_truth_awards.php">Karla Mosley honors groundbreaking trans model Tracey Norman</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0323-unsolved_bold_mysteries.php">The 30 Unsolved Mysteries of The Bold and the Beautiful</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0320-bradley_bell_on_bb_airtime.php">B&B exec explains method behind character airtime madness</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0321-kim_matula_fox_la_to_vegas.php">Kim Matula (ex-Hope) books FOX's LA to Vegas</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0313-patrika_darbo_days_return.php">Patrika Darbo headed back to DAYS</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2017/0320-yr_bb_casting_call_winners.php">CBS announces Y&R and B&B casting call winners</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0127-australia_location_shoot.php">B&B in Australia for �the most glamorous location shoot" in soap history</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0307-i_am_bold_and_beautiful_contest.php">Enter to win B&B's 'I am Bold and Beautiful' sweepstakes</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0304-30th_people_special.php">People releases special B&B collector's edition</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0302-darin_brooks_two_broke_girls.php">B&B's Darin Brooks to guest star on 2 Broke Girls</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0221-susan_flannery_fosters.php">Is Susan Flannery headed back to B&B?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0217-john_mccook_interview.php">John McCook on having to �suck it up� when he feels �malaise about the show�</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/content/scoop/twoscoops/2017/170220.php">TWO SCOOPS: Do you <i>like</i> Quinn and Ridge as a couple?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0217-katherine_kelly_lang_house_sale.php">PHOTOS: Katherine Kelly Lang selling her LA home</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2017/0216-nsfd_shorty.php">CBS Daytime needs your help!</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/content/scoop/twoscoops/2017/170213.php">OPINION: Is Spectra 2.0 as good as the original?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0210-courtney_grosbeck_coco_spectra.php">B&B's Spectra family to grow again</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/content/scoop/twoscoops/2017/170206.php">Are you loving the resurrection of Spectra Fashions?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2017/0209-cbs_daytime_class_photo.php">WATCH: CBS Daytime gathers 100+ stars for �class photo�</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0207-anna_maria_horsford_marla_gibbs_sitcom.php">Anna Maria Horsford, Marla Gibbs land ABC comedy pilot</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0208-jennifer_finnigan_salvation.php">B&B alum Jennifer Finnigan lands plum CBS role</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/bb/news/2017/0131-the_rise_of_spectra.php">The return of Spectra Fashions (and Sally!)</a></div></div></div>

</td></tr>
</table>
<!--- END BB Headlines --->


</td><td width="12"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td>
<td width="230" valign="top">

<!--- START DAYS Headlines --->
<table width="230" border="0" cellpadding="0" cellspacing="0">
<tr><td width="230" colspan="2"><font class="soapname">Days of our Lives</font></td></tr>
<tr><td width="230" height="1" bgcolor="#000000" colspan="2"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="230"></td></tr>
<tr><td width="10" height="5" bgcolor="#000066"><img src="http://soapcentral.com/images/spacer.gif" width="10"></td><td width="220" height="5" bgcolor="#eeeeee" align="left"><font class="opt">&nbsp;<a href="http://soapcentral.com/days/index.php">FrontPage</a> <font color="#888888">|</font> <a href="http://soapcentral.com/days/recaps.php">Recaps</a> <font color="#888888">|</font> <a href="http://soapcentral.com/days/scoop.php">Scoop</a> <font color="#888888">|</font> <a href="http://boards.soapcentral.com/forumdisplay.php?f=6" target="_boards"">Board</a></font></td></tr>
<!--- <tr><td colspan="2"><img src="http://soapcentral.com/images/spacer.gif" width="1" height="5"></td></tr> --->
<tr><td colspan="2"><div class="topstory">
<div class="shell"><div class="boxes"><div id="img"><a href="/days/news/2017/0518-bill_hayes_world_by_the_tail.php"><img src="/days/news/2017/0518-bill_hayes_world_by_the_tail.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#009">DAYS OF OUR LIVES</font></div><div class="headline"><a href="/days/news/2017/0518-bill_hayes_world_by_the_tail.php">WATCH: DAYS' Bill Hayes releases free documentary about his magical life</a></div></div></div>
</div>

<div class="shell"><div class="boxes"><div class="img"><a href="/days/content/scoop/twoscoops/2017/170522.php"><img id="img" src="/days/content/scoop/twoscoops/2017/170522_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#009">DAYS OF OUR LIVES</font></div><div class="headline"><a href="/days/content/scoop/twoscoops/2017/170522.php">TWO SCOOPS: Love -- the kind you clean up with a mop and bucket</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/days/news/2017/0508-chandler_massey_returning.php"><img id="img" src="/days/images/rect/massey_chandler.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#009">DAYS OF OUR LIVES</font></div><div class="headline"><a href="/days/news/2017/0508-chandler_massey_returning.php">Emmy winner Chandler Massey is returning to Days of our Liv</a></div></div></div>

<div class="shell" style="display:none"><div class="boxes"><div class="img"><a href="/days/content/scoop/spoilers/2017/170522.php"><img id="img" src="/days/content/scoop/spoilers/2017/170522_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#009">DAYS OF OUR LIVES</font></div><div class="headline"><a href="/days/content/scoop/spoilers/2017/170522.php">DAYS SPOILER ALERT: A deadly week in Salem</a></div></div></div>

<div class="shell" style="display:none"><div class="boxes"><div class="img"><a href="/soc/recaps/2017/170515.php"><img id="img" src="/days/recaps/2017/images/170515_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#009">DAYS OF OUR LIVES</font></div><div class="headline"><a href="/soc/recaps/2017/170515.php">LAST WEEK ON DAYS: Deimos' plan started to fall apart</a></div></div></div>

<!--- <div class="shell"><div class="boxes"><div class="img"><a href="/days/news/2017/0518-bill_hayes_world_by_the_tail.php"><img src="/days/news/2017/0518-bill_hayes_world_by_the_tail_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#006">DAYS OF OUR LIVES</font></div><div class="headline"><a href="/days/news/2017/0518-bill_hayes_world_by_the_tail.php">WATCH: DAYS' Bill Hayes releases free documentary about his magical life</a></div></div></div> --->

<div class="shell"><div class="boxes"><div class="img"><a href="/days/news/2017/0515-ken_corday_walk_of_fame.php"><img src="/days/news/2017/0515-ken_corday_walk_of_fame_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#006">DAYS OF OUR LIVES</font></div><div class="headline"><a href="/days/news/2017/0515-ken_corday_walk_of_fame.php">WATCH: DAYS' EP Ken Corday receives star on Hollywood Walk of Fame</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0508-guy_wilson_on_will_return.php">Guy Wilson comments on DAYS bringing back Will</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0511-morgan_fairchild_angelica.php">Morgan Fairchild heading to DAYS</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0512-kyle_lowder_ladies_lake_interview.php">INTERVIEW: Kyle Lowder opens up about the making of Ladies of the Lake</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/emmys/news/2017/0430-video_adams_keegan_pettis.php">Olivia Keegan, Kyler Pettis, and Lucas Adams answer "the hardest question" ever on the Red Carpet</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/emmys/news/2017/0430-emmy_coverage_video.php">VIDEO: Exclusive Daytime Emmys Red Carpet interviews with DAYS stars</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0503-alison_sweeney_on_return.php">Alison Sweeney dishes on her �troublemaking� return to DAYS</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2017/0504-wga_strike_averted.php">Writers strike averted after last minute deal</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/emmys/news/2017/0428-emmy_coverage_creative.php">DAYS scores most wins at Creative Arts ceremony</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0422-kate_mansi_returns_to_days.php">Kate Mansi returns to DAYS</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0424-tionne_watkins_tboz_back_days.php">TLC's T-Boz back at DAYS</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/content/scoop/twoscoops/2017/170410.php">Is there anyone without a motive to kill Deimos?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0331-a_martinez_exiting.php">A Martinez says goodbye to Days of our Lives</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0407-days_stars_seriously_sexy_music_video.php">DAYS stars turn up the heat in seriously sexy music video</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0325-billy_flynn_emmy_nom_reaction.php">INTERVIEW: Billy Flynn got an Emmy nomination and went to... Target?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0328-james_lastovic_emmy_nom_reaction.php">INTERVIEW: James Lastovic on the Emmy question he <i>knew</i> we'd ask</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0323-vincent_irizarry_emmy_nom_reaction.php">INTERVIEW: Vincent Irizarry on his Emmy nom and future in daytime</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0323-kate_mansi_emmy_nom_reaction.php">INTERVIEW: DAYS' Kate Mansi discusses Emmy nomination</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0315-arianne_zucker_normal_job.php">Ari Zucker reveals her post-DAYS plans</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0301-arianne_zucker_out.php">DAYS shocker: Arianne Zucker to exit</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0315-jennifer_landon_joins_days_report.php">Jennifer Landon joins DAYS</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0313-nadia_bjorlin_pregnant_again.php">Nadia Bjorlin pregnant with second baby</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0313-patrika_darbo_days_return.php">Patrika Darbo jumping from B&B back to DAYS</a></div></div></div>
</td></tr>
</table>
<!--- END DAYS Headlines --->

</td><td width="7"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td></tr>
<tr><td colspan="9" width="980" height="20"><img src="http://soapcentral.com/images/spacer.gif"></td></tr>


<tr>
<td width="7"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td>
<td width="230" valign="top">

<!--- START GH Headlines --->
<table width="230" border="0" cellpadding="0" cellspacing="0">
<tr><td width="230" colspan="2"><font class="soapname">General Hospital</font></td></tr>
<tr><td width="230" height="1" bgcolor="#000000" colspan="2"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="230"></td></tr>
<tr><td width="10" height="5" bgcolor="#aa4411"><img src="http://soapcentral.com/images/spacer.gif" width="10"></td><td width="220" height="5" bgcolor="#eeeeee" align="left"><font class="opt">&nbsp;<a href="http://soapcentral.com/gh/index.php">FrontPage</a> <font color="#888888">|</font> <a href="http://soapcentral.com/gh/recaps.php">Recaps</a> <font color="#888888">|</font> <a href="http://soapcentral.com/gh/scoop.php">Scoop</a> <font color="#888888">|</font> <a href="http://boards.soapcentral.com/forumdisplay.php?f=7" target="_boards"">Board</a></font></td></tr>
<!--- <tr><td colspan="2"><img src="http://soapcentral.com/images/spacer.gif" width="1" height="5"></td></tr> --->
<tr><td colspan="2"><div class="topstory">
<div class="shell"><div class="boxes"><div id="img"><a href="/gh/news/2017/0517-finola_hughes_shocker.php"><img src="/gh/recaps/2017/images/170516.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/gh/news/2017/0517-finola_hughes_shocker.php">GH co-stars didn't even know Finola Hughes was masquerading as Alex</a></div></div></div>
</div>

<div class="shell"><div class="boxes"><div class="img"><a href="/gh/news/2017/0522-kathleen_gati_grandson.php"><img src="/gh/images/rect/gati_kathleen.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/gh/news/2017/0522-kathleen_gati_grandson.php">Kathleen Gati celebrates a brand new role: grandmother</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/yr/news/2017/0519-eric_braeden_michelle_stafford_single_mom.php"><img src="/yr/news/2017/0519-eric_braeden_michelle_stafford_single_mom_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#803">THE YOUNG AND THE RESTLESS</font></div><div class="headline"><a href="/yr/news/2017/0519-eric_braeden_michelle_stafford_single_mom.php">Michelle Stafford reunites with Eric Braeden on Single Mom A Go-Go</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/gh/news/2017/0522-anthony_montgomery_miles_away.php"><img src="/gh/news/2017/0522-anthony_montgomery_miles_away_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/gh/news/2017/0522-anthony_montgomery_miles_away.php">Anthony Montgomery dishes on his passion project, Miles Away</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/gh/content/scoop/twoscoops/2017/170522.php"><img id="img" src="/gh/content/scoop/twoscoops/2017/170522_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/gh/content/scoop/twoscoops/2017/170522.php">TWO SCOOPS: Could Alex ever become your favorite Devane?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/gh/news/2017/0512-roger_howarth_contract_update.php"><img id="img" src="/gh/images/rect/howarth_roger.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/gh/news/2017/0512-roger_howarth_contract_update.php">RELIEF! Roger Howarth signs new contract at GH</a></div></div></div>

<div class="shell" style="display:none"><div class="boxes"><div class="img"><a href="/gh/content/scoop/spoilers/2017/170522.php"><img id="img" src="/gh/content/scoop/spoilers/2017/170522_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/gh/content/scoop/spoilers/2017/170522.php">GH SPOILER ALERT: Ava makes things worse for herself</a></div></div></div>

<div class="shell" style="display:none"><div class="boxes"><div class="img"><a href="/soc/recaps/2017/170515.php"><img id="img" src="/gh/recaps/2017/images/170519_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/soc/recaps/2017/170515.php">LAST WEEK ON GH: Anna and Alex Devane finally came face to face</a></div></div></div>

<!--- <div class="shell"><div class="boxes"><div class="img"><a href="/gh/news/2017/0517-finola_hughes_shocker.php"><img src="/gh/recaps/2017/images/170516_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/gh/news/2017/0517-finola_hughes_shocker.php">GH co-stars didn't even know Finola Hughes was masquerading as Alex</a></div></div></div> --->

<div class="shell"><div class="boxes"><div class="img"><a href="/gh/news/2017/0512-bradford_anderson_back_as_spinelli.php"><img src="/gh/images/rect/sm/anderson_bradford.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#a41">GENERAL HOSPITAL</font></div><div class="headline"><a href="/gh/news/2017/0512-bradford_anderson_back_as_spinelli.php">Bradford Anderson reprising role as Spinelli</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0510-anna_alex_devane.php">Who's Alex Devane? Your ultimate guide to Anna's twin sister</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0512-jason_thompson_baby_two.php">GH alum Jason Thompson, wife expecting their second child</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0511-jennifer_bassey_to_gh.php">AMC alum Jennifer Bassey joins GH</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0509-bryan_craig_to_yr.php">Is two-time Emmy-winning GH alum Bryan Craig joining Y&R?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0508-nursesball_preview_mario_lopez.php">ABC previews GH's Nurses Ball; Mario Lopez to co-host red carpet festivities</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0507-sebastian_roche_movie.php">GH alum Sebastian Roch� lands new film</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/emmys/news/2017/0430-emmy_coverage_video.php">VIDEO: Exclusive Daytime Emmys Red Carpet interviews with GH stars</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/emmys/news/2017/0428-emmy_coverage_creative.php">GH Hairstyling team wins Creative Arts Emmy</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0410-helen_hunt_gh_crush.php">Helen Hunt reveals which GH star she was �madly in love� with</a></div></div></div>


<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0408-ricky_martin_american_crime_story.php">Ricky Martin joins American Crime Story as Versace's lover</a></div></div></div>
</td></tr>
</table>
<!--- END GH Headlines --->


</td><td width="12"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td>
<td width="230" valign="top">

<!--- START OLTL Headlines --->
<table width="230" border="0" cellpadding="0" cellspacing="0">
<tr><td width="230" colspan="2"><font class="soapname">One Life to Live</font></td></tr>
<tr><td width="230" height="1" bgcolor="#000000" colspan="2"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="230"></td></tr>
<tr><td width="10" height="5" bgcolor="#ff6600"><img src="http://soapcentral.com/images/spacer.gif" width="10"></td><td width="220" height="5" bgcolor="#eeeeee" align="left"><font class="opt">&nbsp;<a href="http://soapcentral.com/oltl/index.php">FrontPage</a> <font color="#888888">|</font> <a href="http://soapcentral.com/bb/recaps.php">Recaps</a> <font color="#888888">|</font> <a href="http://soapcentral.com/oltl/scoop.php">Scoop</a> <font color="#888888">|</font> <a href="http://boards.soapcentral.com/forumdisplay.php?f=9" target="_boards"">Board</a></font></td></tr>
<!--- <tr><td colspan="2"><img src="http://soapcentral.com/images/spacer.gif" width="1" height="5"></td></tr> --->
<tr><td colspan="2"><div class="shell"><div class="boxes"><div class="img"><a href="/yr/news/2017/0522-gina_tognoni_interview_strong_women.php"><img src="/yr/news/2017/0522-gina_tognoni_interview_strong_women_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#803">THE YOUNG AND THE RESTLESS</font></div><div class="headline"><a href="/yr/news/2017/0522-gina_tognoni_interview_strong_women.php">OLTL alum Gina Tognoni opens up on the challenge of portraying a strong woman</a></div></div></div>

<div class="news_list_headline_box"><a href="/gh/news/2017/0512-roger_howarth_contract_update.php"><img class="news_list_headline_img" src="/gh/images/rect/howarth_roger.jpg"></a><div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0512-roger_howarth_contract_update.php">RELIEF! Roger Howarth signs new contract at GH</a></div></div>	


<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2017/0419-jessica_morris_ladies_of_lake_interview.php">INTERVIEW: OLTL alum Jessica Morris on competition, nasty habits and Ladies of the Lake</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2017/0308-bree_williamson_mommys_little_boy.php">Bree Williamson stars in Lifetime's Mommy's Little Boy</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0206-tonja_walker_wraps_gh.php">Tonja Walker wraps up her General Hospital stint</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0131-hillary_b_smith_nora.php">Hillary B. Smith reviving Nora Buchanan on GH</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2017/0127-tika_sumpter_engaged.php">OLTL's Tika Sumpter engaged</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0123-ron_carlivati_new_head_writer.php">Emmy winner Ron Carlivati named head writer of Days of our Lives</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2017/0116-robin_strasser_addiction.php">Robin Strasser opens up about her drug addiction, OLTL exit</a></div>	


<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2017/0117-kassie_depaiva_eddie_alderson_update.php">HEALTH UPDATE: �Great news� for OLTL's Eddie Alderson</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0113-jensen_ackles_danneel_harris_twins.php">OLTL's Danneel Harris introduces her new twins</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2017/0109-abc_on_amc_oltl_rights.php">What the rights back, what will ABC do with OLTL?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0106-trevor_stjohn_mariahs_dad.php">RUMOR ALERT: OLTL alum Trevor St. John headed to Y&R?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2016/1230-tuc_watkins_retake.php">WATCH: OLTL's Tuc Watkins stars in LGBT drama Retake</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2016/1111-claire_labine_obit.php">Emmy-winning soap writer Claire Labine passes away</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2016/1111-robert_vaughn_obit.php">OLTL alum Robert Vaughn passes away</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2016/0928-agnes_nixon_death.php">AMC/OLTL creator Agnes Nixon has died</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/amc/news/2016/0928-agnes_nixon_death_reaction.php">Daytime reacts to the passing of legendary soap creator Agnes Nixon</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2016/0906-lozano_narcos.php">Netflix renews Narcos, starring OLTL's Florencia Lozano, for two more seasons</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/oltl/news/2016/0831-goldsberry_lion-king.php">OLTL alum Ren�e Elise Goldsberry lands Disney role</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2016/0901-abc_daytime_shakeup.php">ABC head of daytime OUT</a></div>	

</td></tr>
</table>
<!--- END OLTL Headlines --->



</td><td width="12"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td>
<td width="230" valign="top">


<!--- START YR Headlines --->
<table width="230" border="0" cellpadding="0" cellspacing="0">
<tr><td width="230" colspan="2"><font class="soapname">The Young and the Restless</font></td></tr>
<tr><td width="230" height="1" bgcolor="#000000" colspan="2"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="230"></td></tr>
<tr><td width="10" height="5" bgcolor="#880033"><img src="http://soapcentral.com/images/spacer.gif" width="10"></td><td width="220" height="5" bgcolor="#eeeeee" align="left"><font class="opt">&nbsp;<a href="http://soapcentral.com/yr/index.php">FrontPage</a> <font color="#888888">|</font> <a href="http://soapcentral.com/yr/recaps.php">Recaps</a> <font color="#888888">|</font> <a href="http://soapcentral.com/yr/scoop.php">Scoop</a> <font color="#888888">|</font> <a href="http://boards.soapcentral.com/forumdisplay.php?f=11" target="_boards"">Board</a></font></td></tr>
<!--- <tr><td colspan="2"><img src="http://soapcentral.com/images/spacer.gif" width="1" height="5"></td></tr> --->
<tr><td colspan="2"><div class="topstory">
<div class="shell"><div class="boxes"><div id="img"><a href="/yr/news/2017/0522-gina_tognoni_interview_strong_women.php"><img src="/yr/news/2017/0522-gina_tognoni_interview_strong_women_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#803">THE YOUNG AND THE RESTLESS</font></div><div class="headline"><a href="/yr/news/2017/0522-gina_tognoni_interview_strong_women.php">Gina Tognoni opens up on the challenge of portraying a strong woman</a></div></div></div>
</div>

<div class="shell"><div class="boxes"><div class="img"><a href="/yr/news/2017/0512-jason_thompson_baby_two.php"><img src="/yr/images/rect/thompson_jason.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#803">THE YOUNG AND THE RESTLESS</font></div><div class="headline"><a href="/yr/news/2017/0512-jason_thompson_baby_two.php">Y&R's Jason Thompson, wife expecting their second child</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/yr/news/2017/0519-eric_braeden_michelle_stafford_single_mom.php"><img src="/yr/news/2017/0519-eric_braeden_michelle_stafford_single_mom_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#803">THE YOUNG AND THE RESTLESS</font></div><div class="headline"><a href="/yr/news/2017/0519-eric_braeden_michelle_stafford_single_mom.php">Eric Braeden reunites with Michelle Stafford on Single Mom A Go-Go</a></div></div></div>

<div class="shell" style="display:none"><div class="boxes"><div class="img"><a href="/yr/content/scoop/spoilers/2017/170522.php"><img id="img" src="/yr/content/scoop/spoilers/2017/170522_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#803">THE YOUNG AND THE RESTLESS</font></div><div class="headline"><a href="/yr/content/scoop/spoilers/2017/170522.php">Y&R SPOILER ALERT: Hilary sets out on a path of destruction</a></div></div></div>

<div class="shell" style="display:none"><div class="boxes"><div class="img"><a href="/soc/recaps/2017/170515.php"><img id="img" src="/yr/recaps/2017/images/170515_sm.jpg"></a></div><div class="soap_name_box"><font id="text" style="background-color:#803">THE YOUNG AND THE RESTLESS</font></div><div class="headline"><a href="/soc/recaps/2017/170515.php">LAST WEEK ON Y&R: Ashley and Jack struggled with how to handle Dina's return</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/yr/news/2017/0516-sally_sussman_midnight_return.php"><img src="/yr/news/2017/0516-sally_sussman_midnight_return_sm.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#803">THE YOUNG AND THE RESTLESS</font></div><div class="headline"><a href="/yr/news/2017/0516-sally_sussman_midnight_return.php">Y&R writer Sally Sussman's fascinating film Midnight Return now available</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0414-casting_lexie_stevenson_noah_alexander_gerry.php">Y&R's Mattie and Charlie recast and SORAS'd</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0504-greg_rikaart_out.php">Greg Rikaart "will be saying goodbye" to Y&R</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/gh/news/2017/0509-bryan_craig_to_yr.php">Is two-time Emmy-winning GH alum Bryan Craig joining Y&R?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/emmys/news/2017/0430-emmy_coverage_video.php">VIDEO: Exclusive Daytime Emmys Red Carpet interviews with Y&R stars</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0406-eileen_davidson_preview.php">Eileen Davidson teases �amazing� story for the Abbotts</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0405-eric_braeden_grand_marshal_boxing_parade.php">Y&R's Eric Braeden gets pretty cool honor</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0404-abhi_sinha_interview.php">INTERVIEW: Getting to know Y&R's Abhi Sinha</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0404-david_faustino_reprising_howard.php">David Faustino returning to Y&R</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0403-kelly_sullivan_emmy_nom_reaction.php">INTERVIEW: Guess who Kelly Sullivan first told about her Emmy nom?</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0403-hunter_king_emmy_nom_reaction.php">INTERVIEW: Y&R's Hunter King reacts to Emmy nomination</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0331-julie_chen_la_story.php">Julie Chen to guest on Y&R as the show heads to Hollywood</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0311-elizabeth_hendrickson_exiting.php">REPORT: Elizabeth Hendrickson taken off contract, will exit CBS soap</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/soapcentral/news/2017/0320-yr_bb_casting_call_winners.php">CBS announces Y&R and B&B casting call winners</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/days/news/2017/0315-jennifer_landon_joins_days_report.php">Y&R alum Jennifer Landon joins DAYS</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0308-marla_adams_returning.php">Y&R's Marla Adams returns</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0309-sofia_pernas_for_god_and_country.php">Sofia Pernas lands major role in NBC drama</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0306-melissa_claire_egan_mystery_project.php">Melissa Claire Egan working on mystery project</a></div></div></div>

<div class="shell"><div class="boxes"><div class="img"><a href="/soapcentral/index.php"><img src="/soapcentral/images/rect/sm/logo.jpg" id="img"></a></div><div class="soap_name_box"><font id="background-color:#069">SOAP CENTRAL</font></div><div class="headline"><a href="/yr/news/2017/0308-erika_girardi_returning.php">Real Housewives star Erika Girardi back to Y&R</a></div></div></div>

</td></tr>
</table>
<!--- END YR Headlines --->


</td><td width="12"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td>
<td width="230" valign="top">

<!--- START YR Headlines --->
<table width="230" border="0" cellpadding="0" cellspacing="0">
<tr><td width="230" colspan="2"><font class="soapname">Other Soap News</font></td></tr>
<tr><td width="230" height="1" bgcolor="#000000" colspan="2"><img src="http://soapcentral.com/images/spacer.gif" height="1" width="230"></td></tr>
<tr><td width="10" height="5" bgcolor="#006699"><img src="http://soapcentral.com/images/spacer.gif" width="10"></td><td width="220" height="5" bgcolor="#eeeeee" align="left"><font class="opt">&nbsp;<a href="http://soapcentral.com/soapcentral/thesoaps.php">FrontPage</a> <font color="#888888">|</font> <a href="http://soapcentral.com/soapcentral/recaps.php">Recaps</a> <font color="#888888">|</font> <a href="http://soapcentral.com/soapcentral/scoop.php">Scoop</a> <font color="#888888">|</font> <a href="http://boards.soapcentral.com/index.php" target="_boards"">Board</a></font></td></tr>
<!--- <tr><td colspan="2"><img src="http://soapcentral.com/images/spacer.gif" width="1" height="5"></td></tr> --->
<tr><td colspan="2"></td></tr>
</table>
<!--- END YR Headlines --->

</td><td width="7"><img src="http://soapcentral.com/images/spacer.gif" width="1" height=1></td></tr>
<tr><td colspan="9" width="980" height="10"><img src="http://soapcentral.com/images/spacer.gif"></td></tr>

</table>
</td>

<td width="5" background="http://soapcentral.com/images/page_bg_right.jpg"><img src="http://soapcentral.com/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
</tr>
</table>


<!--- END In Other News  --->
<!--- END In Other News  --->


<table width="1000px" height="8px" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="5px" background="http://soapcentral.com/images/page_bg_left.jpg"><img src="http://soapcentral.com/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
		<td width="990px" align="center" bgcolor="#ffffff">
<td width="5" background="http://soapcentral.com/images/page_bg_right.jpg"><img src="http://soapcentral.com/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
</tr>
</table>




<!--- END Second Third // Advertisement and In Other News  --->
<!--- END Second Third // Advertisement and In Other News  --->




<!--- START /// Footer --->
<!--- START /// Footer --->


<!--- START Page Footer // Copyright Info and Links --->
<table width="1000" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="5" background="/images/page_bg_left.jpg"><img src="/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
		<td width="990">

			<table id="copright_stripe" width="990" height="30" border="0" cellpadding="0" cellspacing="0">	
				<tr>
		<td height="30" bgcolor="#8C315A" align=left valign=middle>&nbsp;&nbsp;<font class="footer"><font color="#ffffff">&nbsp;&nbsp;&copy; 1997-2017 <b>soap</b>central<b>.com</b></font></td><td bgcolor="#8C315A" align="right" valign=middle><font class="footer-link"><font color="#C69CBD">|</font> <a href="/soapcentral/index.php">Home</a> <font class="footer-link"><font color="#C69CBD">|</font> <a href="/soapcentral/feedback.php">Feedback</a> <font class="footer-link"><font color="#C69CBD">|</font> <a href="/soapcentral/advertising.php?from=ml">Advertising Information</a> <font class="footer-link"><font color="#C69CBD">|</font> <a href="/soapcentral/privacy.php">Privacy Policy</a> <font class="footer-link"><font color="#C69CBD">|</font> <a href="/soapcentral/terms.php">Terms of Use</a> <font class="footer-link"><font color="#C69CBD">|</font> <a href="#top">Top</a> <font class="footer-link"><font color="#C69CBD">|</font>&nbsp;&nbsp;</font></font></td>
				</tr>
			</table>

		</td>
		<td width="5" background="/images/page_bg_right.jpg"><img src="/images/spacer.gif" alt="" border="0" height="1" width="5"></td>
	</tr>
</table>
<!--- END Page Footer // Copyright Info and Links --->
<br><br>



<!--- START // Footer: Google Tracker --->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-7167992-1");
pageTracker._trackPageview();
} catch(err) {}</script>
<!--- END // Footer: Google Tracker --->


<!--- START // Footer: Bottom Ads --->
<br>

<!--- START Advertisement // 468x60 | Advertising.com and Her Moment --->
<table width="1000" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<!--- START Advertisement // 468x60 | Advertising.com --->		
		<td width="330" height="250" align="center"><script type="text/javascript">
  if(typeof(gnm_ord)=='undefined') gnm_ord=Math.random()*10000000000000000; if(typeof(gnm_tile) == 'undefined') gnm_tile=1;
  document.write('<scr'+'ipt src="http://n4403ad.doubleclick.net/adj/gn.soapcentral.com/ros;sect=ros;mtfInline=true;sz=300x250;tile='+(gnm_tile++)+';ord='+gnm_ord+'?" type="text/javascript"></scr'+'ipt>');
</script></td>

		<!--- END Advertisement // 468x60 | Advertising.com --->		
		<td width="340" height="250" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-9262634086419500";
/* 300x250, created 12/1/09 */
google_ad_slot = "1233024923";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
		<!--- END Advertisement // 468x60 | Advertising.com --->		
		
				
		<!--- START Advertisement // 468x60 | Her Moment --->		
		<td width="330" height="250" align="center" bgcolor="#006699">
		<script language="javascript"><!--
document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtechus.com/addyn/3.0/10028.1/3546692/0/170/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp=[group];misc='+new Date().getTime()+'"></scri'+'pt>');
//-->
</script><noscript><a href="http://adserver.adtechus.com/adlink/3.0/10028.1/3546692/0/170/ADTECH;loc=300;key=key1+key2+key3+key4;grp=[group]" target="_blank"><img src="http://adserver.adtechus.com/adserv/3.0/10028.1/3546692/0/170/ADTECH;loc=300;key=key1+key2+key3+key4;grp=[group]" border="0" width="300" height="250"></a></noscript></td>
		<!--- END Advertisement // 468x60 | Her Moment --->		
	</tr>
</table>
<!--- END Advertisement // 468x60 | Advertising.com and Her Moment --->


<script type='text/javascript'>
var dc_AdLinkColor = 'green' ;
var dc_PublisherID = 3437 ;
var dc_isBoldActive= 'no';
var dc_open_new_win='yes'
</script>
<script type='text/javascript' src='http://kona.kontera.com/javascript/lib/KonaLibInline.js'>
</script> <!--- START // Footer: Bottom Ads --->
<br><br>


<!--- END /// Footer --->
<!--- END /// Footer --->






<!--- <script type="text/javascript">
    var infolink_pid = 32160;
    var infolink_wsid = 26;
</script>
<script type="text/javascript" src="http://resources.infolinks.com/js/infolinks_main.js"></script> --->


</body>
</html><!--- END Footer --->
