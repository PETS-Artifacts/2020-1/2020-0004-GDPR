

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	cinema.com.my: Privacy Policy 
</title><meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<!-- Unique Data for each category -->
	<meta name="keywords" content="cinema, online, showtime, movie, film, filem, contest, now, showing, coming, soon, review, news, features, interviews, gallery, gsc, tgv, mbo, lfs, mall, cineplex, plex, plaza, trailers" /><meta name="description" content="Cinema Online Malaysia's Favourite Movie Site" />

    <!-- Google DFP script start -->
	<script type='text/javascript'>
	    (function () {
	        var useSSL = 'https:' == document.location.protocol;
	        var src = (useSSL ? 'https:' : 'http:') +
	'//www.googletagservices.com/tag/js/gpt.js';
	        document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
	    })();
	</script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" /><link rel="stylesheet" href="_plugins/menu2/css/menu.css" type="text/css" />
	<!-- Google DFP tags -->
	<script type='text/javascript'>
	googletag.defineSlot('/55577451/COMY_logo', [50, 60], 'div-gpt-ad-1412224214767-2').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_privacy_sb', [728, 90], 'div-gpt-ad-1408719234135-32').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_privacy_halfpg', [300, 600], 'div-gpt-ad-1414141487831-0').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_footer_lrec1', [336, 280], 'div-gpt-ad-1409216776650-0').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_footer_lrec2', [336, 280], 'div-gpt-ad-1409216776650-1').addService(googletag.pubads());
	googletag.pubads().enableSyncRendering();
	googletag.pubads().enableSingleRequest();
	googletag.enableServices();
	</script>	
	<!-- Google DFP script end --> 

	
	<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "14621247" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=14621247&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->	
<link href="App_Themes/Default/Controls.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default/dateStyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default/ItemStyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default/Sections.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default/StyleSheet.css" type="text/css" rel="stylesheet" /></head>
<body>
    <form name="aspnetForm" method="post" action="privacy_policy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKLTg5ODU3OTcyOQ9kFgJmD2QWAgIDD2QWAgIFD2QWCAIBDxQrAAIPFgQeC18hRGF0YUJvdW5kZx4LXyFJdGVtQ291bnQCNWRkFgJmD2QWCAIBD2QWHAIBD2QWAmYPFQHUATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkNpdHkgQ2VudGVyPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01MSI+VEdWIC0gPGk+U3VyaWEgS0xDQzwvaT48L2E+PC9kaXY+ZAICD2QWAmYPFQFoPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTY3NCI+VEdWIC0gPGk+U3Vud2F5IFB1dHJhPC9pPjwvYT48L2Rpdj5kAgMPZBYCZg8VAWY8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MjEiPkdTQyAtIDxpPlBhdmlsaW9uIEtMPC9pPjwvYT48L2Rpdj5kAgQPZBYCZg8VAW88ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MTciPkdTQyAtIDxpPkJlcmpheWEgVGltZXMgU3F1YXJlPC9pPjwvYT48L2Rpdj5kAgUPZBYCZg8VAWs8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjUyIj5HU0MgLSA8aT5RdWlsbCBDaXR5IE1hbGw8L2k+PC9hPjwvZGl2PmQCBg9kFgJmDxUBZjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NjAiPkdTQyAtIDxpPk51IFNlbnRyYWw8L2k+PC9hPjwvZGl2PmQCBw9kFgJmDxUBZDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01OTciPkxGUyAtIDxpPkNvbGlzZXVtPC9pPjwvYT48L2Rpdj5kAggPZBYCZg8VAdQBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+S3VhbGEgTHVtcHVyPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NzEiPlRHViAtIDxpPkFFT04gQVUyPC9pPjwvYT48L2Rpdj5kAgkPZBYCZg8VAWU8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTY4Ij5NQk8gLSA8aT5WaXZhIEhvbWU8L2k+PC9hPjwvZGl2PmQCCg9kFgJmDxUB3gE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5PbGQgS2xhbmcgUm9hZDwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MjIiPkdTQyAtIDxpPlNpZ25hdHVyZSBHYXJkZW5zPC9pPjwvYT48L2Rpdj5kAgsPZBYCZg8VAWQ8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MSI+R1NDIC0gPGk+TWlkIFZhbGxleTwvaT48L2E+PC9kaXY+ZAIMD2QWAmYPFQHTATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkNoZXJhczwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjAiPlRHViAtIDxpPkNoZXJhcyBTZWxhdGFuPC9pPjwvYT48L2Rpdj5kAg0PZBYCZg8VAWY8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTc1Ij5UR1YgLSA8aT4xIFNoYW1lbGluPC9pPjwvYT48L2Rpdj5kAg4PZBYCZg8VAWo8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjQzIj5UR1YgLSA8aT5DaGVyYXMgU2VudHJhbDwvaT48L2E+PC9kaXY+ZAICD2QWHAIBD2QWAmYPFQFoPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTMiPkdTQyAtIDxpPkNoZXJhcyBMZWlzdXJlPC9pPjwvYT48L2Rpdj5kAgIPZBYCZg8VAdYBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U2hhaCBBbGFtPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01NzYiPk1CTyAtIDxpPlNwYWNlIFU4IE1hbGw8L2k+PC9hPjwvZGl2PmQCAw9kFgJmDxUBazxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01OTEiPkdTQyAtIDxpPlNldGlhIENpdHkgTWFsbDwvaT48L2E+PC9kaXY+ZAIED2QWAmYPFQHTATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPlN1bndheTwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTMiPlRHViAtIDxpPlN1bndheSBQeXJhbWlkPC9pPjwvYT48L2Rpdj5kAgUPZBYCZg8VAdMBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U3ViYW5nPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01NzMiPk1CTyAtIDxpPlN1YmFuZyBQYXJhZGU8L2k+PC9hPjwvZGl2PmQCBg9kFgJmDxUBZDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD00Ij5HU0MgLSA8aT5TdW1taXQgVVNKPC9pPjwvYT48L2Rpdj5kAgcPZBYCZg8VAW88ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjMzIj5QcmVtaXVtLVggLSA8aT5PbmUgQ2l0eSBNYWxsPC9pPjwvYT48L2Rpdj5kAggPZBYCZg8VAdsBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+S290YSBEYW1hbnNhcmE8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTY0NCI+VEdWIC0gPGk+RW5jb3JwIFN0cmFuZDwvaT48L2E+PC9kaXY+ZAIJD2QWAmYPFQHTATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPlBldGFsaW5nIEpheWE8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU0Ij5UR1YgLSA8aT4xIFV0YW1hPC9pPjwvYT48L2Rpdj5kAgoPZBYCZg8VAXA8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjU3Ij5UR1YgLSA8aT5KYXlhIFNob3BwaW5nIENlbnRyZTwvaT48L2E+PC9kaXY+ZAILD2QWAmYPFQFmPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYwMCI+TUJPIC0gPGk+Q2l0dGEgTWFsbDwvaT48L2E+PC9kaXY+ZAIMD2QWAmYPFQFiPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTE4Ij5HU0MgLSA8aT4xIFV0YW1hPC9pPjwvYT48L2Rpdj5kAg0PZBYCZg8VAWo8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTI5Ij5HU0MgLSA8aT5Ucm9waWNhbmEgQ2l0eTwvaT48L2E+PC9kaXY+ZAIOD2QWAmYPFQFpPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU5MCI+R1NDIC0gPGk+UGFyYWRpZ20gTWFsbDwvaT48L2E+PC9kaXY+ZAIDD2QWHAIBD2QWAmYPFQFtPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTc2Ij5tbUNpbmVwbGV4ZXMgLSA8aT5EYW1hbnNhcmE8L2k+PC9hPjwvZGl2PmQCAg9kFgJmDxUBZDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0xMTEiPkxGUyAtIDxpPlBKIFN0YXRlPC9pPjwvYT48L2Rpdj5kAgMPZBYCZg8VAdEBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+UHVjaG9uZzwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTk5Ij5UR1YgLSA8aT5TZXRpYSBXYWxrPC9pPjwvYT48L2Rpdj5kAgQPZBYCZg8VAWI8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NyI+R1NDIC0gPGk+SU9JIE1hbGw8L2k+PC9hPjwvZGl2PmQCBQ9kFgJmDxUB2AE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5LYWphbmc8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTE1NSI+TEZTIC0gPGk+TWV0cm8gUGxhemEgS2FqYW5nPC9pPjwvYT48L2Rpdj5kAgYPZBYCZg8VAc4BPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+S2xhbmc8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU1Ij5UR1YgLSA8aT5CdWtpdCBSYWphPC9pPjwvYT48L2Rpdj5kAgcPZBYCZg8VAWc8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjIiPlRHViAtIDxpPkJ1a2l0IFRpbmdnaTwvaT48L2E+PC9kaXY+ZAIID2QWAmYPFQFpPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU1MiI+TUJPIC0gPGk+SGFyYm91ciBQbGFjZTwvaT48L2E+PC9kaXY+ZAIJD2QWAmYPFQFoPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTY2OSI+R1NDIC0gPGk+S2xhbmcgUGFyYWRlPC9pPjwvYT48L2Rpdj5kAgoPZBYCZg8VAWU8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MTEwIj5MRlMgLSA8aT5TcmkgSW50YW48L2k+PC9hPjwvZGl2PmQCCw9kFgJmDxUBbDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01NjkiPkxGUyAtIDxpPlNoYXcgQ2VudHJlcG9pbnQ8L2k+PC9hPjwvZGl2PmQCDA9kFgJmDxUB0gE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5TZXJpIEtlbWJhbmdhbjwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTIiPlRHViAtIDxpPk1pbmVzPC9pPjwvYT48L2Rpdj5kAg0PZBYCZg8VAdMBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+QW1wYW5nPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0xMjQiPk1CTyAtIDxpPkdhbGF4eSBBbXBhbmc8L2k+PC9hPjwvZGl2PmQCDg9kFgJmDxUBbDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02MDMiPk9uZSBDaW5lbWFzIC0gPGk+U3BlY3RydW08L2k+PC9hPjwvZGl2PmQCBA9kFhYCAQ9kFgJmDxUB2AE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5TZWxheWFuZzwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MTE0Ij5MRlMgLSA8aT5DYXBpdG9sIFNlbGF5YW5nPC9pPjwvYT48L2Rpdj5kAgIPZBYCZg8VAdUBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U2VudHVsPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0yMTAiPkxGUyAtIDxpPlNlbnR1bCBDaW5lcGxleDwvaT48L2E+PC9kaXY+ZAIDD2QWAmYPFQHLATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPktlcG9uZzwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTkiPlRHViAtIDxpPktlcG9uZzwvaT48L2E+PC9kaXY+ZAIED2QWAmYPFQFlPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYwOSI+TUJPIC0gPGk+QnJlbSBNYWxsPC9pPjwvYT48L2Rpdj5kAgUPZBYCZg8VAW88ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTU4Ij5NQk8gLSA8aT5LZXBvbmcgVmlsbGFnZSBNYWxsPC9pPjwvYT48L2Rpdj5kAgYPZBYCZg8VAdcBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U2V0YXBhazwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTgxIj5NQk8gLSA8aT5LTCBGZXN0aXZhbCBDaXR5PC9pPjwvYT48L2Rpdj5kAgcPZBYCZg8VAdYBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+V2FuZ3NhIE1hanU8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU0MSI+VEdWIC0gPGk+V2FuZ3NhIFdhbGs8L2k+PC9hPjwvZGl2PmQCCA9kFgJmDxUB2wE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5EZXNhIFBldGFsaW5nPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01MzgiPk1CTyAtIDxpPlNwYXJrIENpbmVwbGV4PC9pPjwvYT48L2Rpdj5kAgkPZBYCZg8VAdoBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+UHV0cmFqYXlhPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0yMyI+R1NDIC0gPGk+QWxhbWFuZGEgUHV0cmFqYXlhPC9pPjwvYT48L2Rpdj5kAgoPZBYCZg8VAWk8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjYyIj5HU0MgLSA8aT5JT0kgQ2l0eSBNYWxsPC9pPjwvYT48L2Rpdj5kAgsPZBYCZg8VAc8BPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+Q3liZXJqYXlhPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NzAiPlRHViAtIDxpPkRQdWx6ZTwvaT48L2E+PC9kaXY+ZAIDDw9kDxAWAWYWARYCHg5QYXJhbWV0ZXJWYWx1ZWQWAQIDZGQCBQ8UKwACDxYEHwBnHwECUWRkFgJmD2QWCAIBD2QWLAIBD2QWAmYPFQHMATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPlJhd2FuZzwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTg5Ij5UR1YgLSA8aT5SYXdhbmc8L2k+PC9hPjwvZGl2PmQCAg9kFgJmDxUB1QE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5LdWFsYSBTZWxhbmdvcjwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTU3Ij5MRlMgLSA8aT4xIFBsYXphPC9pPjwvYT48L2Rpdj5kAgMPZBYCZg8VAdQBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+R2VvcmdldG93bjwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTY2Ij5UR1YgLSA8aT4xc3QgQXZlbnVlPC9pPjwvYT48L2Rpdj5kAgQPZBYCZg8VAWo8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjQxIj5UR1YgLSA8aT5HdXJuZXkgUGFyYWdvbjwvaT48L2E+PC9kaXY+ZAIFD2QWAmYPFQFnPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTEyIj5HU0MgLSA8aT5HdXJuZXkgUGxhemE8L2k+PC9hPjwvZGl2PmQCBg9kFgJmDxUBaDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01ODIiPkxGUyAtIDxpPlByYW5naW4gTWFsbDwvaT48L2E+PC9kaXY+ZAIHD2QWAmYPFQHhATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPlNlYmVyYW5nIFBlcmFpPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NTMiPlRHViAtIDxpPkFFT04gQnVraXQgTWVydGFqYW08L2k+PC9hPjwvZGl2PmQCCA9kFgJmDxUBazxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01MDYiPlRHViAtIDxpPlBlcmRhIENpdHkgTWFsbDwvaT48L2E+PC9kaXY+ZAIJD2QWAmYPFQFoPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTE5MCI+TWVnYSBDJ3BsZXggLSA8aT5QcmFpPC9pPjwvYT48L2Rpdj5kAgoPZBYCZg8VAWo8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjM1Ij5NZWdhIEMncGxleCAtIDxpPkJlcnRhbTwvaT48L2E+PC9kaXY+ZAILD2QWAmYPFQHTATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkJheWFuIExlcGFzPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0xOSI+R1NDIC0gPGk+UXVlZW5zYmF5PC9pPjwvYT48L2Rpdj5kAgwPZBYCZg8VAWg8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MTEyIj5MRlMgLSA8aT5CdWtpdCBKYW1idWw8L2k+PC9hPjwvZGl2PmQCDQ9kFgJmDxUB1gE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5CdXR0ZXJ3b3J0aDwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MTEzIj5MRlMgLSA8aT5CdXR0ZXJ3b3J0aDwvaT48L2E+PC9kaXY+ZAIOD2QWAmYPFQHbATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPlNlYmVyYW5nIEpheWE8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTIwIj5HU0MgLSA8aT5TdW53YXkgQ2Fybml2YWw8L2k+PC9hPjwvZGl2PmQCDw9kFgJmDxUB2wE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5TdW5nYWkgUGV0YW5pPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NzMiPk1CTyAtIDxpPkNlbnRyYWwgU3F1YXJlPC9pPjwvYT48L2Rpdj5kAhAPZBYCZg8VAWk8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTk2Ij5HU0MgLSA8aT5BbWFuamF5YSBNYWxsPC9pPjwvYT48L2Rpdj5kAhEPZBYCZg8VAXM8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTQwIj5HcmFuZCBDaW5lcGxleCAtIDxpPlZpbGxhZ2UgTWFsbDwvaT48L2E+PC9kaXY+ZAISD2QWAmYPFQHYATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkFsb3IgU2V0YXI8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYwNyI+TUJPIC0gPGk+QWxvciBTdGFyIE1hbGw8L2k+PC9hPjwvZGl2PmQCEw9kFgJmDxUBaDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NzYiPkdTQyAtIDxpPkFtYW4gQ2VudHJhbDwvaT48L2E+PC9kaXY+ZAIUD2QWAmYPFQFmPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTE1NiI+TEZTIC0gPGk+Q2l0eSBQbGF6YTwvaT48L2E+PC9kaXY+ZAIVD2QWAmYPFQHfATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkxhbmdrYXdpPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0xOTEiPk1lZ2EgQydwbGV4IC0gPGk+TGFuZ2thd2kgUGFyYWRlPC9pPjwvYT48L2Rpdj5kAhYPZBYCZg8VAd0BPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+Sml0cmE8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTExNyI+Sml0cmEgTWFsbCBDJ3BsZXggLSA8aT5KaXRyYSBNYWxsPC9pPjwvYT48L2Rpdj5kAgIPZBYsAgEPZBYCZg8VAdsBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+S3VsaW08L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYxMiI+TUJPIC0gPGk+S3VsaW0gTGFuZG1hcmsgQ2VudHJhbDwvaT48L2E+PC9kaXY+ZAICD2QWAmYPFQHOATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPklwb2g8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU5NCI+VEdWIC0gPGk+U3RhdGlvbiAxODwvaT48L2E+PC9kaXY+ZAIDD2QWAmYPFQFlPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU2Ij5UR1YgLSA8aT5LaW50YSBDaXR5PC9pPjwvYT48L2Rpdj5kAgQPZBYCZg8VAWc8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjYxIj5HU0MgLSA8aT5JcG9oIFBhcmFkZTwvaT48L2E+PC9kaXY+ZAIFD2QWAmYPFQFrPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTUzNyI+TEZTIC0gPGk+U2VyaSBLaW50YSBJcG9oPC9pPjwvYT48L2Rpdj5kAgYPZBYCZg8VAd8BPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+UGFyaXQgQnVudGFyPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NTkiPkxGUyAtIDxpPktlcmlhbiBTZW50cmFsIE1hbGw8L2k+PC9hPjwvZGl2PmQCBw9kFgJmDxUBzAE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5LYW1wYXI8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYwNiI+TEZTIC0gPGk+S2FtcGFyPC9pPjwvYT48L2Rpdj5kAggPZBYCZg8VAd0BPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U2VyaSBNYW5qdW5nPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NTQiPlRHViAtIDxpPkFFT04gU2VyaSBNYW5qdW5nPC9pPjwvYT48L2Rpdj5kAgkPZBYCZg8VAdoBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U2VyaSBJc2thbmRhcjwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjM0Ij5MRlMgLSA8aT5TZXJpIElza2FuZGFyPC9pPjwvYT48L2Rpdj5kAgoPZBYCZg8VAdABPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U2l0aWF3YW48L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYxMyI+TUJPIC0gPGk+U2l0aWF3YW48L2k+PC9hPjwvZGl2PmQCCw9kFgJmDxUB0wE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5UYWlwaW5nPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NzUiPlRHViAtIDxpPkFlb24gVGFpcGluZzwvaT48L2E+PC9kaXY+ZAIMD2QWAmYPFQFrPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYxNCI+TUJPIC0gPGk+VGFpcGluZyBTZW50cmFsPC9pPjwvYT48L2Rpdj5kAg0PZBYCZg8VAdYBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+VGVsdWsgSW50YW48L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYxNSI+TUJPIC0gPGk+VGVsdWsgSW50YW48L2k+PC9hPjwvZGl2PmQCDg9kFgJmDxUB0gE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5NYWxhY2NhPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0xMjYiPk1CTyAtIDxpPk1lbGFrYSBNYWxsPC9pPjwvYT48L2Rpdj5kAg8PZBYCZg8VAXE8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTcwIj5HU0MgLSA8aT5BRU9OIEJhbmRhcmF5YSBNZWxha2E8L2k+PC9hPjwvZGl2PmQCEA9kFgJmDxUBbDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01MDIiPkdTQyAtIDxpPkRhdGFyYW4gUGFobGF3YW48L2k+PC9hPjwvZGl2PmQCEQ9kFgJmDxUBajxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01MzMiPkxGUyAtIDxpPk1haGtvdGEgUGFyYWRlPC9pPjwvYT48L2Rpdj5kAhIPZBYCZg8VAdEBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U2VyZW1iYW48L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU3Ij5UR1YgLSA8aT5TZXJlbWJhbiAyPC9pPjwvYT48L2Rpdj5kAhMPZBYCZg8VAWY8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTE2Ij5NQk8gLSA8aT5UZXJtaW5hbCAyPC9pPjwvYT48L2Rpdj5kAhQPZBYCZg8VAWU8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjQ3Ij5HU0MgLSA8aT5QYWxtIE1hbGw8L2k+PC9hPjwvZGl2PmQCFQ9kFgJmDxUBZDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02Ij5HU0MgLSA8aT5UZXJtaW5hbCAxPC9pPjwvYT48L2Rpdj5kAhYPZBYCZg8VAWQ8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTM2Ij5MRlMgLSA8aT5LTSBQbGF6YTwvaT48L2E+PC9kaXY+ZAIDD2QWLAIBD2QWAmYPFQHXATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkJhaGF1PC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01MjgiPkxGUyAtIDxpPktpYXJhIFNxdWFyZSBCYWhhdTwvaT48L2E+PC9kaXY+ZAICD2QWAmYPFQHWATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkt1YW50YW48L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTEwIj5HU0MgLSA8aT5CZXJqYXlhIE1lZ2FtYWxsPC9pPjwvYT48L2Rpdj5kAgMPZBYCZg8VAWs8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTMyIj5HU0MgLSA8aT5FYXN0IENvYXN0IE1hbGw8L2k+PC9hPjwvZGl2PmQCBA9kFgJmDxUB1QE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5Kb2hvciBCYWhydTwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTgiPlRHViAtIDxpPlRlYnJhdSBDaXR5PC9pPjwvYT48L2Rpdj5kAgUPZBYCZg8VAWc8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTg4Ij5UR1YgLSA8aT5CdWtpdCBJbmRhaDwvaT48L2E+PC9kaXY+ZAIGD2QWAmYPFQFqPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTY1NiI+VEdWIC0gPGk+QUVPTiBLdWxhaWpheWE8L2k+PC9hPjwvZGl2PmQCBw9kFgJmDxUBYjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01MzAiPk1CTyAtIDxpPlUgTWFsbDwvaT48L2E+PC9kaXY+ZAIID2QWAmYPFQFkPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU2NSI+TUJPIC0gPGk+S1NMIENpdHk8L2k+PC9hPjwvZGl2PmQCCQ9kFgJmDxUBbzxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD03NCI+bW1DaW5lcGxleGVzIC0gPGk+Q2l0eSBTcXVhcmU8L2k+PC9hPjwvZGl2PmQCCg9kFgJmDxUBZjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD05MyI+TEZTIC0gPGk+QnJvYWR3YXkgSkI8L2k+PC9hPjwvZGl2PmQCCw9kFgJmDxUBbjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0xMDIiPkxGUyAtIDxpPlBsYXphIFRhc2VrIFNrdWRhaTwvaT48L2E+PC9kaXY+ZAIMD2QWAmYPFQFpPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTUzOSI+TEZTIC0gPGk+U2t1ZGFpIFBhcmFkZTwvaT48L2E+PC9kaXY+ZAIND2QWAmYPFQFqPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU3NCI+TEZTIC0gPGk+SU9JIE1hbGwgS3VsYWk8L2k+PC9hPjwvZGl2PmQCDg9kFgJmDxUBazxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD01NDIiPlN1cGVyc3RhciAtIDxpPlVsdSBUaXJhbTwvaT48L2E+PC9kaXY+ZAIPD2QWAmYPFQHUATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPk1hc2FpPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02ODAiPlN1cGVyc3RhciAtIDxpPlNlcmkgQWxhbTwvaT48L2E+PC9kaXY+ZAIQD2QWAmYPFQHZATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkJhdHUgUGFoYXQ8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYwOCI+TUJPIC0gPGk+QmF0dSBQYWhhdCBNYWxsPC9pPjwvYT48L2Rpdj5kAhEPZBYCZg8VAWY8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTIxIj5NQk8gLSA8aT5TcXVhcmUgT25lPC9pPjwvYT48L2Rpdj5kAhIPZBYCZg8VAdABPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+U2VnYW1hdDwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTc3Ij5MRlMgLSA8aT4xIFNlZ2FtYXQ8L2k+PC9hPjwvZGl2PmQCEw9kFgJmDxUB0QE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5LbHVhbmc8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYxMCI+TUJPIC0gPGk+S2x1YW5nIE1hbGw8L2k+PC9hPjwvZGl2PmQCFA9kFgJmDxUB2AE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5Lb3RhIFRpbmdnaTwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTk1Ij5NQk8gLSA8aT5IZXJpdGFnZSBNYWxsPC9pPjwvYT48L2Rpdj5kAhUPZBYCZg8VAdQBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+VGVyZW5nZ2FudTwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTU5Ij5UR1YgLSA8aT5NZXNyYSBNYWxsPC9pPjwvYT48L2Rpdj5kAhYPZBYCZg8VAdoBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+TWVudGFrYWI8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU4NSI+R1NDIC0gPGk+TWVudGFrYWIgU3RhciBNYWxsPC9pPjwvYT48L2Rpdj5kAgQPZBYeAgEPZBYCZg8VAdIBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+S3VjaGluZzwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTMxIj5NQk8gLSA8aT5TcHJpbmcgTWFsbDwvaT48L2E+PC9kaXY+ZAICD2QWAmYPFQFjPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTYxNiI+R1NDIC0gPGk+Q2l0eU9ORTwvaT48L2E+PC9kaXY+ZAIDD2QWAmYPFQFtPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTEzMSI+TEZTIC0gPGk+Uml2ZXJzaWRlIEt1Y2hpbmc8L2k+PC9hPjwvZGl2PmQCBA9kFgJmDxUBZzxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02NDIiPkxGUyAtIDxpPlN1bW1lciBNYWxsPC9pPjwvYT48L2Rpdj5kAgUPZBYCZg8VAeABPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+S290YSBLaW5hYmFsdTwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjY2Ij5NQk8gLSA8aT5JbWFnbyBLb3RhIEtpbmFiYWx1PC9pPjwvYT48L2Rpdj5kAgYPZBYCZg8VAWc8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTQ0Ij5HU0MgLSA8aT5TdXJpYSBTYWJhaDwvaT48L2E+PC9kaXY+ZAIHD2QWAmYPFQFjPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTI2MyI+R1NDIC0gPGk+MUJvcm5lbzwvaT48L2E+PC9kaXY+ZAIID2QWAmYPFQFxPGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTU0MyI+TWVnYWxvbmcgQydwbGV4IC0gPGk+UGVuYW1wYW5nPC9pPjwvYT48L2Rpdj5kAgkPZBYCZg8VAXA8ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9MTgwIj5Hcm93YmFsbCAtIDxpPkNlbnRlciBQb2ludCBLSzwvaT48L2E+PC9kaXY+ZAIKD2QWAmYPFQHYATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPlNhbmRha2FuPC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0xNDYiPlN0YXIgQydwbGV4IC0gPGk+U2FuZGFrYW48L2k+PC9hPjwvZGl2PmQCCw9kFgJmDxUB3QE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5UYXdhdTwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NTkyIj5FYXN0ZXJuIEMncGxleCAtIDxpPkVhc3Rlcm4gUGxhemE8L2k+PC9hPjwvZGl2PmQCDA9kFgJmDxUB0AE8ZGl2IHN0eWxlPSJQYWRkaW5nOiAycHggMnB4IDJweCAycHg7IGJhY2tncm91bmQ6IEdyZXk7IGNvbG9yOiBXaGl0ZTsgZm9udC1zaXplOiAxMnB4Ij48Yj5TaWJ1PC9iPjwvZGl2PjxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD0xNDIiPlN0YXIgQydwbGV4IC0gPGk+U2lidTwvaT48L2E+PC9kaXY+ZAIND2QWAmYPFQHWATxkaXYgc3R5bGU9IlBhZGRpbmc6IDJweCAycHggMnB4IDJweDsgYmFja2dyb3VuZDogR3JleTsgY29sb3I6IFdoaXRlOyBmb250LXNpemU6IDEycHgiPjxiPkJpbnR1bHU8L2I+PC9kaXY+PGRpdiBzdHlsZT0iZm9udC1zaXplOiAxMnB4Ij48YSBocmVmPSIvc2hvd3RpbWVzL2NpbmVtYS5hc3B4P0lkPTE0NCI+U3RhciBDJ3BsZXggLSA8aT5CaW50dWx1PC9pPjwvYT48L2Rpdj5kAg4PZBYCZg8VAdYBPGRpdiBzdHlsZT0iUGFkZGluZzogMnB4IDJweCAycHggMnB4OyBiYWNrZ3JvdW5kOiBHcmV5OyBjb2xvcjogV2hpdGU7IGZvbnQtc2l6ZTogMTJweCI+PGI+TWlyaTwvYj48L2Rpdj48ZGl2IHN0eWxlPSJmb250LXNpemU6IDEycHgiPjxhIGhyZWY9Ii9zaG93dGltZXMvY2luZW1hLmFzcHg/SWQ9NjU1Ij5UR1YgLSA8aT5JbXBlcmlhbCBDaXR5IE1hbGw8L2k+PC9hPjwvZGl2PmQCDw9kFgJmDxUBbDxkaXYgc3R5bGU9ImZvbnQtc2l6ZTogMTJweCI+PGEgaHJlZj0iL3Nob3d0aW1lcy9jaW5lbWEuYXNweD9JZD02MDUiPkdTQyAtIDxpPkJpbnRhbmcgTWVnYW1hbGw8L2k+PC9hPjwvZGl2PmQCBw8PZA8QFgFmFgEWAh8CZBYBAgNkZBgCBR1jdGwwMCRDaW5lbWFzX0xpc3QxJExpc3RWaWV3MQ8UKwAKZGRkAhZkZGQ8KwBRAAJRZGQFHWN0bDAwJENpbmVtYXNfTGlzdDEkbHZDaW5lbWFzDxQrAApkZGQCDmRkZDwrADUAAjVkZGp2R0FG9qgFbIUQUEr5VnxTlsH1" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2927320D" />
<input type="hidden" name="__PREVIOUSPAGE" id="__PREVIOUSPAGE" value="D_oE3-oOMB9eJKwFw1W02OsJMTKAHKQ3EfR5JhyhqHEMgoJKakuzsAx0RrdkYVFZ9HEYmEUrAi5h0Donz6a3VBhlq3E1" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWCgLU/O5WAurU3NwMArSCjfIIAuSVnpUDAu+sreYEAsOFz+8GAv/4mNcMApDSqOgOAueZ6K0DArfnz8IDeVp7DUfgJBvi0/BE7fCqtRk4J50=" />
    <div id="master">
        <!-- BEGIN EFFECTIVE MEASURE CODE -->
        <!-- COPYRIGHT EFFECTIVE MEASURE -->
        <script type="text/javascript">
            (function () {
                var em = document.createElement('script'); em.type = 'text/javascript';
                em.async = true;
                em.src = ('https:' == document.location.protocol ? 'https://my-ssl' :
                'http://my-cdn') + '.effectivemeasure.net/em.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(em, s);
            })();
        
        </script>
        <noscript>
            <img src="//my.effectivemeasure.net/em_image" alt="" style="position: absolute; left: -5px;" />
        </noscript>
        <!--END EFFECTIVE MEASURE CODE -->        
        <div id="header">
            <div style="width: 91%; display: table-cell; border:none; vertical-align: middle;">                                                                       
                <table class="tbl_standard">
                    <tr style="vertical-align: middle;">
                        <td>English|<a href="http://www.cinema.com.my/bm/default.aspx" target="_blank">B. Malaysia</a> &nbsp;&nbsp;&nbsp;Like Us!</td>
                        <td><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fcinemaonline&amp;width=47&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20&amp;appId=583362251764516" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:47px; height:20px;" allowTransparency="true"></iframe></td>
                        <td><input name="ctl00$Search1$q" type="text" maxlength="512" id="ctl00_Search1_q" />&nbsp;
<input type="submit" name="ctl00$Search1$sa" value="Search" id="ctl00_Search1_sa" />                
<input type="hidden" name="ctl00$Search1$cx" id="ctl00_Search1_cx" value="013128572172917708998:wkt14jbgegg" />
<input type="hidden" name="ctl00$Search1$cof" id="ctl00_Search1_cof" value="FORID:11" /></td>
                    </tr>
                </table>
            </div>                                        
            

<link rel="stylesheet" href="/members/css/style.css" type="text/css" />

<script src="/members/js/login.js" type="text/javascript"></script>
<div id="loginContainer">
    
            <a href="#" id="loginButton"><span>Login</span><em></em></a>
            <table id="ctl00_ctrl_login1_lvLogin_Login1" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td>
                    <div id="loginBox">
                        <div id="loginForm">
                            <fieldset id="body">
                                <fieldset>
                                    <input name="ctl00$ctrl_login1$lvLogin$Login1$UserName" type="text" id="ctl00_ctrl_login1_lvLogin_Login1_UserName" placeholder="Username" />
                                    
                                </fieldset>
                                <fieldset>
                                    <input name="ctl00$ctrl_login1$lvLogin$Login1$Password" type="password" id="ctl00_ctrl_login1_lvLogin_Login1_Password" placeholder="Password" />
                                    
                                </fieldset>
                                <input type="submit" name="ctl00$ctrl_login1$lvLogin$Login1$btnSubmit" value="Log In" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$btnSubmit&quot;, &quot;&quot;, true, &quot;login&quot;, &quot;&quot;, false, false))" id="ctl00_ctrl_login1_lvLogin_Login1_btnSubmit" />
                                                              
                            </fieldset>
                            <span>
                                <a id="ctl00_ctrl_login1_lvLogin_Login1_lbtnRegister" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$lbtnRegister&quot;, &quot;&quot;, false, &quot;&quot;, &quot;members/registration.aspx&quot;, false, true))">Register</a>&nbsp;|&nbsp;<a id="ctl00_ctrl_login1_lvLogin_Login1_lbtnForgotPassword" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$lbtnForgotPassword&quot;, &quot;&quot;, false, &quot;&quot;, &quot;members/forgot_password.aspx&quot;, false, true))">Forgot your password?</a>
                            </span>
                        </div>
                    </div>
                </td>
	</tr>
</table>            
        
</div>

            
        </div>
                
        <div id="logo">
            <table style="width: 1065px;">
                <tr>
                    <td style="width: 235px">
                        <table style="width:234px">
                            <tr>
                                <td>
									
	<!-- COMY_logo -->
	<div id='div-gpt-ad-1412224214767-2' style='width: 50px; height: 60px;'>
		<script type='text/javascript'>
			googletag.display('div-gpt-ad-1412224214767-2');
		</script>
	</div>

                                </td>
                                <td>
                                    <!-- CINEMA ONLINE text -->
                                    <a href="http://www.cinema.com.my/default.aspx" style="text-decoration: none"><font style="letter-spacing: -1px;
                                        font-family: Arial; font-size: 25px; font-weight: bold; color: #2F3193; text-decoration: none;
                                        text-transform: none;">Cinema</font><font style="letter-spacing: -0.5px; font-family: 'Times New Roman'; font-size: 25px; font-weight: bolder; color: #AA1C20; text-decoration: none; text-transform: none;"> Online</font>
                                        <br />
                                    </a><font face="Arial" size="2" color="#000000"><i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...your movie partner</i></font>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 830px;" align="right">
                        
	<!-- COMY_privacy_sb -->
	<div id='div-gpt-ad-1408719234135-32' style='width:728px; height:90px;'>
		<script type='text/javascript'>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1408719234135-32'); });
		</script>
	</div>

                    </td>
                </tr>
            </table>
        </div>

        <div>
            <ul id="menu">
                <li><a href="/default.aspx">Home</a></li>
                <li><a href="/movies/nowshowing.aspx">Now Showing</a></li>
                <li><a href="/movies/comingsoon.aspx">Coming
                    Soon</a></li>
                <li><a href="/articles/news.aspx">News</a></li>
                <li><a href="/articles/gallery.aspx">Gallery</a></li>
                <li><a href="/articles/features.aspx">Features</a></li>
                <li><a href="/articles/interviews.aspx">Interviews</a></li>
                <li><a href="/trailers/trailers.aspx">Trailers</a></li>                
                <li><a href="/articles/events.aspx">Events</a></li>
                <li><a href="/contests/contests.aspx">Contests</a></li>
                <li class="menu_right"><a href="#"><img alt="" style="padding-top: 5px" src="/_images/DropDownArrow.png" width="15" /></a>
                    
<div class="dropdown_5columns align_right"><!-- Begin 5 columns container -->
    
            <div class="section_header" style="margin-bottom: 10px">
                Klang Valley
            </div>            
            
            <div class="col_1">
                <div>
                    
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>City Center</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=51">TGV - <i>Suria KLCC</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=674">TGV - <i>Sunway Putra</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=21">GSC - <i>Pavilion KL</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=17">GSC - <i>Berjaya Times Square</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=652">GSC - <i>Quill City Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=660">GSC - <i>Nu Sentral</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=597">LFS - <i>Coliseum</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kuala Lumpur</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=671">TGV - <i>AEON AU2</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=568">MBO - <i>Viva Home</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Old Klang Road</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=22">GSC - <i>Signature Gardens</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=1">GSC - <i>Mid Valley</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Cheras</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=60">TGV - <i>Cheras Selatan</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=575">TGV - <i>1 Shamelin</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=643">TGV - <i>Cheras Sentral</i></a></div>
        
                </div>
            </div>
        
            <div class="col_1">
                <div>
                    
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=3">GSC - <i>Cheras Leisure</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Shah Alam</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=576">MBO - <i>Space U8 Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=591">GSC - <i>Setia City Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Sunway</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=53">TGV - <i>Sunway Pyramid</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Subang</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=573">MBO - <i>Subang Parade</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=4">GSC - <i>Summit USJ</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=633">Premium-X - <i>One City Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kota Damansara</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=644">TGV - <i>Encorp Strand</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Petaling Jaya</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=54">TGV - <i>1 Utama</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=657">TGV - <i>Jaya Shopping Centre</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=600">MBO - <i>Citta Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=18">GSC - <i>1 Utama</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=529">GSC - <i>Tropicana City</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=590">GSC - <i>Paradigm Mall</i></a></div>
        
                </div>
            </div>
        
            <div class="col_1">
                <div>
                    
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=76">mmCineplexes - <i>Damansara</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=111">LFS - <i>PJ State</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Puchong</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=599">TGV - <i>Setia Walk</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=7">GSC - <i>IOI Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kajang</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=155">LFS - <i>Metro Plaza Kajang</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Klang</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=55">TGV - <i>Bukit Raja</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=62">TGV - <i>Bukit Tinggi</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=552">MBO - <i>Harbour Place</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=669">GSC - <i>Klang Parade</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=110">LFS - <i>Sri Intan</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=569">LFS - <i>Shaw Centrepoint</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Seri Kembangan</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=52">TGV - <i>Mines</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Ampang</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=124">MBO - <i>Galaxy Ampang</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=603">One Cinemas - <i>Spectrum</i></a></div>
        
                </div>
            </div>
        
            <div class="col_1">
                <div>
                    
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Selayang</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=114">LFS - <i>Capitol Selayang</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Sentul</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=210">LFS - <i>Sentul Cineplex</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kepong</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=59">TGV - <i>Kepong</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=609">MBO - <i>Brem Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=558">MBO - <i>Kepong Village Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Setapak</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=581">MBO - <i>KL Festival City</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Wangsa Maju</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=541">TGV - <i>Wangsa Walk</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Desa Petaling</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=538">MBO - <i>Spark Cineplex</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Putrajaya</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=23">GSC - <i>Alamanda Putrajaya</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=662">GSC - <i>IOI City Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Cyberjaya</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=670">TGV - <i>DPulze</i></a></div>
        
                </div>
            </div>
                    
        

    

<div class="col">

            <div class="section_header" style="margin-bottom: 10px">
                Nationwide
            </div>            
            
            <div class="col_1">
                <div>
                    
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Rawang</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=589">TGV - <i>Rawang</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kuala Selangor</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=557">LFS - <i>1 Plaza</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Georgetown</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=566">TGV - <i>1st Avenue</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=641">TGV - <i>Gurney Paragon</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=12">GSC - <i>Gurney Plaza</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=582">LFS - <i>Prangin Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Seberang Perai</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=653">TGV - <i>AEON Bukit Mertajam</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=506">TGV - <i>Perda City Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=190">Mega C'plex - <i>Prai</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=635">Mega C'plex - <i>Bertam</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Bayan Lepas</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=19">GSC - <i>Queensbay</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=112">LFS - <i>Bukit Jambul</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Butterworth</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=113">LFS - <i>Butterworth</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Seberang Jaya</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=20">GSC - <i>Sunway Carnival</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Sungai Petani</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=673">MBO - <i>Central Square</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=596">GSC - <i>Amanjaya Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=540">Grand Cineplex - <i>Village Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Alor Setar</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=607">MBO - <i>Alor Star Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=676">GSC - <i>Aman Central</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=156">LFS - <i>City Plaza</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Langkawi</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=191">Mega C'plex - <i>Langkawi Parade</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Jitra</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=117">Jitra Mall C'plex - <i>Jitra Mall</i></a></div>
        
                </div>
            </div>
        
            <div class="col_1">
                <div>
                    
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kulim</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=612">MBO - <i>Kulim Landmark Central</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Ipoh</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=594">TGV - <i>Station 18</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=56">TGV - <i>Kinta City</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=661">GSC - <i>Ipoh Parade</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=537">LFS - <i>Seri Kinta Ipoh</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Parit Buntar</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=659">LFS - <i>Kerian Sentral Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kampar</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=606">LFS - <i>Kampar</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Seri Manjung</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=654">TGV - <i>AEON Seri Manjung</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Seri Iskandar</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=634">LFS - <i>Seri Iskandar</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Sitiawan</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=613">MBO - <i>Sitiawan</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Taiping</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=675">TGV - <i>Aeon Taiping</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=614">MBO - <i>Taiping Sentral</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Teluk Intan</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=615">MBO - <i>Teluk Intan</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Malacca</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=126">MBO - <i>Melaka Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=570">GSC - <i>AEON Bandaraya Melaka</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=502">GSC - <i>Dataran Pahlawan</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=533">LFS - <i>Mahkota Parade</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Seremban</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=57">TGV - <i>Seremban 2</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=516">MBO - <i>Terminal 2</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=647">GSC - <i>Palm Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=6">GSC - <i>Terminal 1</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=536">LFS - <i>KM Plaza</i></a></div>
        
                </div>
            </div>
        
            <div class="col_1">
                <div>
                    
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Bahau</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=528">LFS - <i>Kiara Square Bahau</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kuantan</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=10">GSC - <i>Berjaya Megamall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=532">GSC - <i>East Coast Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Johor Bahru</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=58">TGV - <i>Tebrau City</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=588">TGV - <i>Bukit Indah</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=656">TGV - <i>AEON Kulaijaya</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=530">MBO - <i>U Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=565">MBO - <i>KSL City</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=74">mmCineplexes - <i>City Square</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=93">LFS - <i>Broadway JB</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=102">LFS - <i>Plaza Tasek Skudai</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=539">LFS - <i>Skudai Parade</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=574">LFS - <i>IOI Mall Kulai</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=542">Superstar - <i>Ulu Tiram</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Masai</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=680">Superstar - <i>Seri Alam</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Batu Pahat</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=608">MBO - <i>Batu Pahat Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=521">MBO - <i>Square One</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Segamat</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=577">LFS - <i>1 Segamat</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kluang</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=610">MBO - <i>Kluang Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kota Tinggi</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=595">MBO - <i>Heritage Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Terengganu</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=559">TGV - <i>Mesra Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Mentakab</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=585">GSC - <i>Mentakab Star Mall</i></a></div>
        
                </div>
            </div>
        
            <div class="col_1">
                <div>
                    
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kuching</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=531">MBO - <i>Spring Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=616">GSC - <i>CityONE</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=131">LFS - <i>Riverside Kuching</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=642">LFS - <i>Summer Mall</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Kota Kinabalu</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=666">MBO - <i>Imago Kota Kinabalu</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=544">GSC - <i>Suria Sabah</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=263">GSC - <i>1Borneo</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=543">Megalong C'plex - <i>Penampang</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=180">Growball - <i>Center Point KK</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Sandakan</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=146">Star C'plex - <i>Sandakan</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Tawau</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=592">Eastern C'plex - <i>Eastern Plaza</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Sibu</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=142">Star C'plex - <i>Sibu</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Bintulu</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=144">Star C'plex - <i>Bintulu</i></a></div>
        
            <div style="Padding: 2px 2px 2px 2px; background: Grey; color: White; font-size: 12px"><b>Miri</b></div><div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=655">TGV - <i>Imperial City Mall</i></a></div>
        
            <div style="font-size: 12px"><a href="/showtimes/cinema.aspx?Id=605">GSC - <i>Bintang Megamall</i></a></div>
        
                </div>
            </div>
                    
        

    

</div>
</div>
                </li>
                <li class="menu_right">
                    <a href="/cinemas/cinemalist.aspx">Showtimes</a>
                </li>                                 
            </ul>                                                 
        </div>
        
        <div id="content">
            
			
            
    <table style="width: 1065px">
        <tr>
            <td style="width: 735px; vertical-align: top;">
                <div class="main_section">
                    <div class="section_header">
                        Privacy Policy
                    </div>
                    <div class="section_content">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <br />
                                    <p>
                                        This Privacy Policy governs the manner in which Cinema Online collects, uses, maintains
                                        and discloses information collected from users (each, a "User") of the <a href="http://www.cinema.com.my">
                                            cinema.com.my</a> website ("Site"). This privacy policy applies to the Site
                                        and all products and services offered by Cinema Online.</p>
                                    <span class="red-title">Personal identification information</span>                                        
                                    <p>
                                        We may collect personal identification information from Users in a variety of ways,
                                        including, but not limited to, when Users visit our site, register on the site,
                                        subscribe to the newsletter, respond to a survey, fill out a form, and in connection
                                        with other activities, services, features or resources we make available on our
                                        Site. Users may be asked for, as appropriate, name, email address, mailing address,
                                        phone number, and NRIC. Users may, however, visit our Site anonymously. We will
                                        collect personal identification information from Users only if they voluntarily
                                        submit such information to us. Users can always refuse to supply personally identification
                                        information, except that it may prevent them from engaging in certain Site related
                                        activities.</p>
                                    <span class="red-title">Non-personal identification information</span>                                        
                                    <p>
                                        We may collect non-personal identification information about Users whenever they
                                        interact with our Site. Non-personal identification information may include the
                                        browser name, the type of computer and technical information about Users means of
                                        connection to our Site, such as the operating system and the Internet service providers
                                        utilized and other similar information.</p>
                                    <span class="red-title">Web browser cookies</span>                                        
                                    <p>
                                        Our Site may use "cookies" (such as Google Analytics cookie and DoubleClick cookie)
                                        to enhance User experience. User's web browser places cookies on their hard drive
                                        for record-keeping purposes and sometimes to track information about them. User
                                        may choose to set their web browser to refuse cookies, or to alert you when cookies
                                        are being sent. If they do so, note that some parts of the Site may not function
                                        properly.</p>
                                    <span class="red-title">How we use collected information</span>                                        
                                    <p>
                                        Cinema Online may collect and use Users personal information for the following purposes:<br>
                                        <ul>
                                            <li><i>To improve customer service</i><br />
                                                Information you provide helps us respond to your customer service requests and support
                                                needs more efficiently.</li>
                                            <li><i>To personalize user experience</i><br />
                                                We may use information in the aggregate to understand how our Users as a group use
                                                the services and resources provided on our Site.</li>
                                            <li><i>To improve our Site</i><br />
                                                We may use feedback you provide to improve our products and services.</li>
                                            <li><i>To run a promotion, contest, survey or other Site feature</i><br />
                                                To send Users information they agreed to receive about topics we think will be of
                                                interest to them.</li>
                                            <li><i>To send periodic emails</i><br />
                                                We may use the email address to respond to their inquiries, questions, and/or other
                                                requests. If User decides to opt-in to our mailing list, they will receive emails
                                                that may include company news, updates, or related service information, etc. If
                                                at any time the User would like to unsubscribe from receiving future emails, we
                                                include detailed unsubscribe instructions at the bottom of each email.</li>
                                        </ul>
                                    </p>
                                    <span class="red-title">How we protect your information</span>                                        
                                    <p>
                                        We adopt appropriate data collection, storage and processing practices and security
                                        measures to protect against unauthorized access, alteration, disclosure or destruction
                                        of your personal information, username, password, and data stored on our Site.</p>
                                    <span class="red-title">Sharing your personal information</span>                                        
                                    <p>
                                        We do not sell, trade, or rent Users personal identification information to others.
                                        We may share generic aggregated demographic information not linked to any personal
                                        identification information regarding visitors and users with our business partners,
                                        trusted affiliates and advertisers for the purposes outlined above. We may use third
                                        party service providers to help us operate our business and the Site or administer
                                        activities on our behalf, such as sending out newsletters or surveys. We may share
                                        your information with these third parties for those limited purposes provided that
                                        you have given us your permission.</p>
                                    <span class="red-title">Third party websites</span>                                        
                                    <p>
                                        Users may find advertising or other content on our Site that link to the sites and
                                        services of our partners, suppliers, advertisers, sponsors, licensors and other
                                        third parties. We do not control the content or links that appear on these sites
                                        and are not responsible for the practices employed by websites linked to or from
                                        our Site. In addition, these sites or services, including their content and links,
                                        may be constantly changing. These sites and services may have their own privacy
                                        policies and customer service policies. Browsing and interaction on any other website,
                                        including websites which have a link to our Site, is subject to that website's own
                                        terms and policies.</p>
                                    <span class="red-title">Advertising</span>                                        
                                    <p>
                                        Ads appearing on our site may be delivered to Users by advertising partners, who
                                        may set cookies. These cookies allow the ad server to recognize your computer each
                                        time they send you an online advertisement to compile non personal identification
                                        information about you or others who use your computer. This information allows ad
                                        networks to, among other things, deliver targeted advertisements that they believe
                                        will be of most interest to you. This privacy policy does not cover the use of cookies
                                        by any advertisers.</p>
                                    <span class="red-title">Google Adsense</span>                                        
                                    <p>
                                        Some of the ads may be served by Google. Google's use of the DART cookie enables
                                        it to serve ads to Users based on their visit to our Site and other sites on the
                                        Internet. DART uses "non personally identifiable information" and does NOT track
                                        personal information about you, such as your name, email address, physical address,
                                        etc. You may opt out of the use of the DART cookie by visiting the Google <a href="http://www.google.com/privacy_ads.html" target="_blank">
                                            ad and content network privacy policy</a>.</p>
                                    <span class="red-title">Google Analytics</span>                                        
                                    <p>
                                        Some features implemented are based on Display Advertising for Google Analytics
                                        Demographics and Interest Reporting. Using the <a href="http://www.google.com/settings/ads"
                                            target="_blank">Ads Settings</a>, Users can opt-out of Google Analytics for
                                        Display Advertising and customize Google Display Network ads.<br>
                                        <br>
                                        Users may also check out Google Analytics' <a href="http://tools.google.com/dlpage/gaoptout/"
                                            target="_blank">currently available opt-outs</a> for the web.<br>
                                        <br>
                                        The data gathered from Google's Interest-based advertising or 3rd-party audience
                                        data (such as age, gender, and interests) with Google Analytics shall be used to
                                        understand our Users better in order to improve our products and services.</p>
                                    <span class="red-title">Changes to this privacy policy</span>                                        
                                    <p>
                                        Cinema Online has the discretion to update this privacy policy at any time. When
                                        we do, we will revise the updated date at the bottom of this page. We encourage
                                        Users to frequently check this page for any changes to stay informed about how we
                                        are helping to protect the personal information we collect. You acknowledge and
                                        agree that it is your responsibility to review this privacy policy periodically
                                        and become aware of modifications.</p>
                                    <span class="red-title">Your acceptance of these terms</span>                                        
                                    <p>
                                        By using this Site, you signify your acceptance of this policy. If you do not agree
                                        to this policy, you may write in to request for a deletion or edit of your personal
                                        information. Your continued use of the Site following the posting of changes to
                                        this policy will be deemed your acceptance of those changes.</p>
                                    <span class="red-title">E.T. phone number</span>                                        
                                    <p>
                                        If you have any questions about this Privacy Policy, the practices of this site,
                                        or your dealings with this site, please contact us at:<br />
                                        <br />
                                        Cinema Online Sdn. Bhd.<br />
                                        No: 7A, Jalan SS4D/2,<br />
                                        People’s Park,<br />
                                        47301 Petaling Jaya,<br />
                                        Selangor, Malaysia.<br />
                                        <br>
                                        Tel: +603 7805 1527 (Office hours only, as stated below)<br>
                                        Email: marketing@cinemaonline.asia<br />
                                        <br />
                                        Office Hours: Monday – Friday (closed on public holidays), 9:30am - 6:00pm<br />
                                        <br />
                                        This document was last updated on December 27, 2013<br />
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
             <td style="width: 5px">
                &nbsp;
            </td>
            <td style="width: 325px; vertical-align: top;">
                <div class="main_section_halfpage">
					<!-- COMY_privacy_halfpg -->
					<div id='div-gpt-ad-1414141487831-0' style='width:300px; height:600px; margin: 0 auto; padding-top: 3px;'>
					<script type='text/javascript'>
					googletag.display('div-gpt-ad-1414141487831-0');
					</script>
					</div>
				</div>
            </td>
        </tr>
    </table>

	<table style="width: 1065px">
		<tr>
			<td style="width: 365px;">
				<!-- COMY_footer_lrec1 -->
				<div id='div-gpt-ad-1409216776650-0' style='width:336px; height:280px; margin: 0 auto;'>
				<script type='text/javascript'>
					googletag.display('div-gpt-ad-1409216776650-0');
				</script>
				</div>
			</td>
			<td style="width: 5px">
				&nbsp;
			</td>
			<td style="width: 365px;">
				<!-- COMY_footer_lrec2 -->
				<div id='div-gpt-ad-1409216776650-1' style='width:336px; height:280px; margin: 0 auto;'>
				<script type='text/javascript'>
					googletag.display('div-gpt-ad-1409216776650-1');
				</script>
				</div>
			</td>
			<td style="width: 5px">
				&nbsp;
			</td>
			<td style="width: 325px;" valign="top">
				&nbsp;
			</td>
		</tr>
	</table>

        </div>
        <br /><br />               
        <div id="footer">                   
            <a href="/advertise/advertise.aspx">Advertise with Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/about_us.aspx">About Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/contact_us.aspx">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/faq.aspx">FAQ</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="http://www.thehive.asia/" target="_blank">TheHive.asia</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/privacy_policy.aspx">Privacy Policy</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/vacancy.aspx">Vacancy</a>
            <hr />
            <span>
                © 1999 - 2016
                All content copyright of Cinema Online and their respective owners<br />
                e-mail: <a href="mailto:marketing@cinemaonline.asia" target="_top">marketing@cinemaonline.asia</a> for inquiries.
            </span>
        </div>        
    </div>    
    </form>

    
			
	
	

    <!-- Google Analytics code -->
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-6252059-1', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');

	</script> 
	
	<!-- Innity Container Tag -->
<script type="text/javascript" charset="UTF-8">(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src=("https:"==d.location.protocol? "https://ssl-avd.innity.net":"http://avd.innity.net")+"/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "61", "5594d5c21c51b1ee4d7abea2");</script>
<!-- End Innity Container Tag -->

</body>
</html>
