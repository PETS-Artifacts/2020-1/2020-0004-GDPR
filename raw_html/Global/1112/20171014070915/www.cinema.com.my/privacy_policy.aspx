


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	cinema.com.my: Privacy Policy 
</title><meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <!-- FB Instant articles -->
    <meta property="fb:pages" content="186350986277" />

	<!-- Unique Data for each category -->
	<meta name="keywords" content="cinema, online, showtime, movie, film, filem, contest, now, showing, coming, soon, review, news, features, interviews, gallery, gsc, tgv, mbo, lfs, mall, cineplex, plex, plaza, trailers" /><meta name="description" content="Cinema Online Malaysia's Favourite Movie Site" /><link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
    <!-- Google DFP script start -->
	<script type='text/javascript'>
	    (function () {
	        var useSSL = 'https:' == document.location.protocol;
	        var src = (useSSL ? 'https:' : 'http:') +
	'//www.googletagservices.com/tag/js/gpt.js';
	        document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
	    })();
	</script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
          
    <link rel="stylesheet" href="_plugins/menu2/css/menu_v2.css" type="text/css" />
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" /> 
	
	<script type="text/javascript" src="../_plugins/Colorbox/js/jquery.colorbox.js"></script>
    <link rel="stylesheet" href="_plugins/Colorbox/css/colorbox.css" type="text/css" />  
	
    <script type="text/javascript">


        function FavouriteBox(userid) {

            //$(function () {
            //    $.colorbox({ iframe: true, innerWidth: 795, scrolling: false, href: '../Usr_Ctrl/Members/favMovies.aspx?id=' + userid });
            //    return false;
            //});

            if (document.cookie.indexOf('visited=true') === -1) {
                var expires = new Date();
                expires.setTime(expires.getTime() + (120 * 60 * 1000));
                document.cookie = "visited=true; expires=" + expires.toUTCString();
                //$.colorbox({ height: '100%', scrolling: false, href: '../Usr_Ctrl/Members/favMovies.aspx?id=' + userid });
                $.colorbox({ iframe: true, innerWidth: 795, scrolling: false, href: '../Usr_Ctrl/Members/favMovies.aspx?id=' + userid });
                //ShowPopup("testing");
            };


        };

	</script>

	<!-- Po.st Lite code -->
	<script type="text/javascript">
		(function () {
			var s = document.createElement('script');
			s.type = 'text/javascript';
			s.async = true;
			s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
			  + '.po.st/static/v4/post-widget.js#publisherKey=fgiltlv0pvisqip084ec';
			var x = document.getElementsByTagName('script')[0];
			x.parentNode.insertBefore(s, x);
		 })();
	</script>
    
    <!-- Google DFP tags -->
	<script type='text/javascript'>
	googletag.defineSlot('/55577451/COMY_logo', [50, 60], 'div-gpt-ad-1412224214767-2').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_privacy_sb', [728, 90], 'div-gpt-ad-1408719234135-32').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_privacy_halfpg', [300, 600], 'div-gpt-ad-1414141487831-0').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_footer_lrec1', [336, 280], 'div-gpt-ad-1409216776650-0').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_footer_lrec2', [336, 280], 'div-gpt-ad-1409216776650-1').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_ros_footer_rec', [300, 250], 'div-gpt-ad-1470016231071-4').addService(googletag.pubads());
	googletag.pubads().enableSyncRendering();
	googletag.pubads().enableSingleRequest();
	googletag.enableServices();
	</script>	
	<!-- Google DFP script end --> 
	<link rel="stylesheet" href="../_plugins/stickyad/css/stickyadmovies.css" type="text/css" />       
	<script type="text/javascript" src="../_plugins/stickyad/js/jquery.stickyadmovies.js"></script>

	
	<!-- Begin comScore Tag -->
	<script>
	  var _comscore = _comscore || [];
	  _comscore.push({ c1: "2", c2: "14621247" });
	  (function() {
		var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
		el.parentNode.insertBefore(s, el);
	  })();
	</script>
	<noscript>
	  <img src="http://b.scorecardresearch.com/p?c1=2&c2=14621247&cv=2.0&cj=1" />
	</noscript>
	<!-- End comScore Tag -->	
	
	<!-- Google Analytics code -->
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-6252059-1', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');
	</script> 
	
	<!-- Innity Container Tag -->
		<script type="text/javascript" charset="UTF-8">(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src=("https:"==d.location.protocol? "https://ssl-avd.innity.net":"http://avd.innity.net")+"/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "61", "5594d5c21c51b1ee4d7abea2");</script>
	<!-- End Innity Container Tag -->
<link href="App_Themes/Default_v2/Controls.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/dateStyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/ItemStyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/MasterStyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/Sections.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/StyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/StyleSheet_v2.css" type="text/css" rel="stylesheet" /></head>
<body>
    <form name="aspnetForm" method="post" action="privacy_policy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEyODk5NTE0ODEPZBYCZg9kFgICAw9kFgICCA9kFgICAQ9kFgICAQ9kFgJmD2QWCAIBDxAPFgYeDURhdGFUZXh0RmllbGQFCGFyZWFOYW1lHg5EYXRhVmFsdWVGaWVsZAUGYXJlYUlkHgtfIURhdGFCb3VuZGdkEBU6FlNlbGVjdCBBcmVhIC8gQWxsIEFyZWELQ2l0eSBDZW50ZXIMS3VhbGEgTHVtcHVyDk9sZCBLbGFuZyBSb2FkBkNoZXJhcw1EZXNhIFBldGFsaW5nBkFtcGFuZwtXYW5nc2EgTWFqdQ1QZXRhbGluZyBKYXlhBlN1bndheQZTdWJhbmcOS290YSBEYW1hbnNhcmEJU2hhaCBBbGFtB1B1Y2hvbmcFS2xhbmcGS2FqYW5nDlNlcmkgS2VtYmFuZ2FuBlJhd2FuZwZLZXBvbmcHU2V0YXBhawlQdXRyYWpheWEJQ3liZXJqYXlhDkt1YWxhIFNlbGFuZ29yCkdlb3JnZXRvd24OU2ViZXJhbmcgUGVyYWkLQmF5YW4gTGVwYXMLQnV0dGVyd29ydGgNU2ViZXJhbmcgSmF5YQ1TdW5nYWkgUGV0YW5pCkFsb3IgU2V0YXIITGFuZ2thd2kFSml0cmEFS3VsaW0ESXBvaAxQYXJpdCBCdW50YXIGS2FtcGFyDFNlcmkgTWFuanVuZw1TZXJpIElza2FuZGFyCFNpdGlhd2FuB1RhaXBpbmcLVGVsdWsgSW50YW4HTWFsYWNjYQhTZXJlbWJhbgVCYWhhdQdLdWFudGFuC0pvaG9yIEJhaHJ1BU1hc2FpCkJhdHUgUGFoYXQHU2VnYW1hdAZLbHVhbmcLS290YSBUaW5nZ2kKVGVyZW5nZ2FudQhNZW50YWthYgdLdWNoaW5nDUtvdGEgS2luYWJhbHUIU2FuZGFrYW4FVGF3YXUETWlyaRU6ATACNTECNTYCNTgCMTECNTcCNDcCNTUCNDICNDMCMzYCNDUCMzUCNDQCMzgCMzkCNDACNDYCNTMCNTQCNTkCODQCNDECMTYCMTkCMTcCMTgCMjABOQE2ATgBNwIxMAIyMQI2MwIyMgI2NAIyNAIyMwIyNQIyNwIxMgIxMwIxNAIxNQExAjkzATMBNAE1ATICNDkCNjECMzECMjgCMjkCMzACMzQUKwM6Z2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCAw8QDxYGHwAFB2Npbk5hbWUfAQUFY2luSWQfAmdkEBWNARtTZWxlY3QgQ2luZW1hIC8gQWxsIENpbmVtYXMQVEdWIC0gU3VyaWEgS0xDQxJUR1YgLSBTdW53YXkgUHV0cmELVEdWIC0gTWluZXMUVEdWIC0gU3Vud2F5IFB5cmFtaWQNVEdWIC0gMSBVdGFtYRNUR1YgLSBFbmNvcnAgU3RyYW5kGlRHViAtIEpheWEgU2hvcHBpbmcgQ2VudHJlElRHViAtIEFFT04gS2xlYmFuZwxUR1YgLSBLZXBvbmcUVEdWIC0gQ2hlcmFzIFNlbGF0YW4VVEdWIC0gU3Vud2F5IFZlbG9jaXR5EFRHViAtIDEgU2hhbWVsaW4UVEdWIC0gQ2hlcmFzIFNlbnRyYWwRVEdWIC0gV2FuZ3NhIFdhbGsOVEdWIC0gQUVPTiBBVTIWVEdWIC0gRFB1bHplIEN5YmVyamF5YRBUR1YgLSBTZXRpYSBXYWxrDFRHViAtIFJhd2FuZxBUR1YgLSBCdWtpdCBSYWphElRHViAtIEJ1a2l0IFRpbmdnaSBUR1YgLSBTZXJlbWJhbiAyIFNob3BwaW5nIENlbnRyZRBUR1YgLSAxc3QgQXZlbnVlGVRHViAtIEd1cm5leSBQYXJhZ29uIE1hbGwZVEdWIC0gQUVPTiBCdWtpdCBNZXJ0YWphbRBUR1YgLSBTdGF0aW9uIDE4EFRHViAtIEtpbnRhIENpdHkSVEdWIC0gQWVvbiBUYWlwaW5nF1RHViAtIEFFT04gU2VyaSBNYW5qdW5nEVRHViAtIFRlYnJhdSBDaXR5EVRHViAtIEJ1a2l0IEluZGFoFFRHViAtIEFFT04gS3VsYWlqYXlhEFRHViAtIE1lc3JhIE1hbGwYVEdWIC0gSW1wZXJpYWwgQ2l0eSBNYWxsF1RHViAtIFZpdmFjaXR5IE1lZ2FtYWxsD01CTyAtIFZpdmEgSG9tZRVNQk8gLSBHYWxheHkgQ2luZXBsZXgTTUJPIC0gU3ViYW5nIFBhcmFkZRBNQk8gLSBDaXR0YSBNYWxsE01CTyAtIFN0YXJsaW5nIE1hbGwTTUJPIC0gU3BhY2UgVTggTWFsbA9NQk8gLSBCcmVtIE1hbGwZTUJPIC0gS2Vwb25nIFZpbGxhZ2UgTWFsbBVNQk8gLSBTZXRhcGFrIENlbnRyYWwUTUJPIC0gU3BhcmsgQ2luZXBsZXgTTUJPIC0gSGFyYm91ciBQbGFjZRxNQk8gLSBLdWxpbSBMYW5kbWFyayBDZW50cmFsFU1CTyAtIFRhaXBpbmcgU2VudHJhbBRNQk8gLSBDZW50cmFsIFNxdWFyZRFNQk8gLSBUZWx1ayBJbnRhbhFNQk8gLSBNZWxha2EgTWFsbBNNQk8gLSBFbGVtZW50cyBNYWxsGU1CTyAtIFRlcm1pbmFsIDIgU2VyZW1iYW4XTUJPIC0gS3VhbnRhbiBDaXR5IE1hbGwMTUJPIC0gVSBNYWxsDk1CTyAtIEtTTCBDaXR5EE1CTyAtIFNxdWFyZSBPbmURTUJPIC0gS2x1YW5nIE1hbGwTTUJPIC0gSGVyaXRhZ2UgTWFsbBFNQk8gLSBTcHJpbmcgTWFsbBlNQk8gLSBJbWFnbyBLb3RhIEtpbmFiYWx1F0dTQyAtIFNpZ25hdHVyZSBHYXJkZW5zEEdTQyAtIE1pZCBWYWxsZXkTR1NDIC0gTWVsYXdhdGkgTWFsbBFHU0MgLSBQYXZpbGlvbiBLTA1HU0MgLSAxIFV0YW1hFEdTQyAtIFRyb3BpY2FuYSBDaXR5E0dTQyAtIFBhcmFkaWdtIE1hbGwVR1NDIC0gU2V0aWEgQ2l0eSBNYWxsEkdTQyAtIEtsYW5nIFBhcmFkZRpHU0MgLSBCZXJqYXlhIFRpbWVzIFNxdWFyZRVHU0MgLSBRdWlsbCBDaXR5IE1hbGwQR1NDIC0gTnUgU2VudHJhbBhHU0MgLSBBbGFtYW5kYSBQdXRyYWpheWETR1NDIC0gSU9JIENpdHkgTWFsbBlHU0MgLSBDaGVyYXMgTGVpc3VyZSBNYWxsDEdTQyAtIE15VG93bhBHU0MgLSBTdW1taXQgVVNKDkdTQyAtIElPSSBNYWxsEUdTQyAtIElwb2ggUGFyYWRlEkdTQyAtIEFtYW4gQ2VudHJhbBNHU0MgLSBBbWFuamF5YSBNYWxsEkdTQyAtIEd1cm5leSBQbGF6YQ9HU0MgLSBRdWVlbnNiYXkVR1NDIC0gU3Vud2F5IENhcm5pdmFsD0dTQyAtIFBhbG0gTWFsbBBHU0MgLSBUZXJtaW5hbCAxG0dTQyAtIEFFT04gQmFuZGFyYXlhIE1lbGFrYRZHU0MgLSBEYXRhcmFuIFBhaGxhd2FuFkdTQyAtIEJlcmpheWEgTWVnYW1hbGwVR1NDIC0gRWFzdCBDb2FzdCBNYWxsGEdTQyAtIE1lbnRha2FiIFN0YXIgTWFsbBFHU0MgLSBTdXJpYSBTYWJhaA1HU0MgLSAxQm9ybmVvDUdTQyAtIENpdHlPTkUWR1NDIC0gQmludGFuZyBNZWdhbWFsbBdMRlMgLSBDb2xpc2V1bSBDaW5lcGxleBdMRlMgLSBTdGF0ZSBDaW5lcGxleCBQShhMRlMgLSBNZXRybyBQbGF6YSBLYWphbmcVTEZTIC0gU3JpIEludGFuIEtsYW5nDkxGUyAtIEtNIFBsYXphEUxGUyAtIEJ1dHRlcndvcnRoFUxGUyAtIFNlcmkgS2ludGEgSXBvaA5MRlMgLSBTaXRpYXdhbhtMRlMgLSBQbGF6YSBUYXNlayBTa3VkYWkgSkITTEZTIC0gU2t1ZGFpIFBhcmFkZRtMRlMgLSBIYXJib3VyIE1hbGwgU2FuZGFrYW4QTEZTIC0gUEIgU2VudHJhbBtDaXR5IEMncGxleCAtIENpdHkgQ2luZXBsZXgeRWFzdGVybiBDJ3BsZXggLSBFYXN0ZXJuIFBsYXphHUdyYW5kIENpbmVwbGV4IC0gVmlsbGFnZSBNYWxsG0dyb3diYWxsIC0gR3Jvd2JhbGwgQ2luZW1heB5KaXRyYSBNYWxsIEMncGxleCAtIEppdHJhIE1hbGwRS1BJIC0gQnJvYWR3YXkgSkISS1BJIC0gUGVybGluZyBNYWxsJ01lZ2EgQydwbGV4IC0gTWVnYW1hbGwgUGluYW5nIChMZXZlbCA0KR1NZWdhIEMncGxleCAtIExhbmdrYXdpIFBhcmFkZRRNZWdhIEMncGxleCAtIEJlcnRhbSlNZWdhbG9uZyBDJ3BsZXggLSBNZWdhbG9uZyBNYWxsIFBlbmFtcGFuZxhtbUNpbmVwbGV4ZXMgLSBEYW1hbnNhcmEabW1DaW5lcGxleGVzIC0gQ2l0eSBTcXVhcmUfbW1DaW5lcGxleGVzIC0gU2hhdyBDZW50cmVwb2ludCVtbUNpbmVwbGV4ZXMgLSAxIFBsYXphIEt1YWxhIFNlbGFuZ29yIW1tQ2luZXBsZXhlcyAtIEtpYXJhIFNxdWFyZSBCYWhhdR1tbUNpbmVwbGV4ZXMgLSBNYWhrb3RhIFBhcmFkZSJtbUNpbmVwbGV4ZXMgLSBCdWtpdCBKYW1idWwgUGVuYW5nG21tQ2luZXBsZXhlcyAtIFByYW5naW4gTWFsbBVtbUNpbmVwbGV4ZXMgLSBLYW1wYXIibW1DaW5lcGxleGVzIC0gS2VyaWFuIFNlbnRyYWwgTWFsbBxtbUNpbmVwbGV4ZXMgLSBTZXJpIElza2FuZGFyF21tQ2luZXBsZXhlcyAtIElPSSBNYWxsGG1tQ2luZXBsZXhlcyAtIDEgU2VnYW1hdCBtbUNpbmVwbGV4ZXMgLSBSaXZlcnNpZGUgS3VjaGluZxptbUNpbmVwbGV4ZXMgLSBTdW1tZXIgTWFsbB5tbUNpbmVwbGV4ZXMgLSBSaXZlcmZyb250IENpdHkdT25lIENpbmVtYXMgLSBTcGVjdHJ1bSBBbXBhbmcZUHJlbWl1bS1YIC0gT25lIENpdHkgTWFsbCJTdXBlcnN0YXIgLSBUb2RheSdzIE1hbGwgVWx1IFRpcmFtJFN1cGVyc3RhciAtIFRvZGF5J3MgTWFya2V0IFNlcmkgQWxhbRpUU1IgLSBJUkRLTCBNYWxsIFNoYWggQWxhbRZQYXJhZ29uIC0gVGFpcGluZyBNYWxsFY0BATACNTEDNjc0AjUyAjUzAjU0AzY0NAM2NTcDNjg5AjU5AjYwAzY5MwM1NzUDNjQzAzU0MQM2NzEDNjcwAzU5OQM1ODkCNTUCNjICNTcDNTY2AzY0MQM2NTMDNTk0AjU2AzY3NQM2NTQCNTgDNTg4AzY1NgM1NTkDNjU1AzY4OAM1NjgDMTI0AzU3MwM2MDADNjk3AzU3NgM2MDkDNTU4AzU4MQM1MzgDNTUyAzYxMgM2MTQDNjczAzYxNQMxMjYDNjk0AzUxNgM3MDEDNTMwAzU2NQM1MjEDNjEwAzU5NQM1MzEDNjY2AjIyATEDNzAwAjIxAjE4AzUyOQM1OTADNTkxAzY2OQIxNwM2NTIDNjYwAjIzAzY2MgEzAzY5OQE0ATcDNjYxAzY3NgM1OTYCMTICMTkCMjADNjQ3ATYDNTcwAzUwMgIxMAM1MzIDNTg1AzU0NAMyNjMDNjE2AzYwNQM1OTcDMTExAzE1NQMxMTADNTM2AzExMwM1MzcDNjk4AzEwMgM1MzkDNjgyAzY5NQM2ODYDNTkyAzU0MAMxODADMTE3AzY4NAM2ODcDMTkwAzE5MQM2MzUDNTQzAjc2Ajc0AzcwNQM3MTYDNzEwAzcwOQM3MTQDNzA3AzcxMgM3MTEDNzA2AzcxMwM3MTUDNzA4AzcwNAM3MTcDNjAzAzYzMwM1NDIDNjgwAzY5MAM2OTIUKwONAWdnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCBQ8QDxYGHwAFB21vdk5hbWUfAQUFbW92SWQfAmdkEBU1GVNlbGVjdCBNb3ZpZSAvIEFsbCBNb3ZpZXMbS2luZ3NtYW46IFRoZSBHb2xkZW4gQ2lyY2xlEUJsYWRlIFJ1bm5lciAyMDQ5DUFtZXJpY2FuIE1hZGUIR2Vvc3Rvcm0WVGhlIExlZ28gTmluamFnbyBNb3ZpZQZDYXJzIDMYVG9tYmlydW86IFBlbnVuZ2d1IFJpbWJhAkl0EUJpc2lrIFBhZGEgTGFuZ2l0DFNodXR0bGUgTGlmZQdNb3RoZXIhDVRoZSBGb3JlaWduZXIWTWlkbmlnaHQgRGluZXIgMiAoSkZGKRhUc3VraWppIFdvbmRlcmxhbmQgKEpGRikiVGhlIElycmVndWxhciBBdCBNYWdpYyBIaWdoIFNjaG9vbAtUdXJuIEFyb3VuZBJDaGFzaW5nIFRoZSBEcmFnb24PVGhlIENydWNpZml4aW9uC0VzY2FwZSBSb29tGExldCBNZSBFYXQgWW91ciBQYW5jcmVhcxlCbGFkZSBSdW5uZXIgMjA0OSAoRC1CT1gpFkJsYWRlIFJ1bm5lciAyMDQ5ICgzRCkZQmxhZGUgUnVubmVyIDIwNDkgKEFUTU9TKRBHZW9zdG9ybSAoRC1CT1gpCEthcnVwcGFuE0knbSBOb3QgQSBUZXJyb3Jpc3QSQWJhbmcgTG9uZyBGYWRpbCAyEUFtZXJpY2FuIEFzc2Fzc2luE0FubmFiZWxsZTogQ3JlYXRpb24SVGhlIFNvbiBPZiBCaWdmb290C1Rva3lvIEdob3VsGU15IExpdHRsZSBQb255OiBUaGUgTW92aWUHU2VycGVudAhTdHJvbmdlchBNaWRuaWdodCBSdW5uZXJzBENoZWYrTXkgTGl0dGxlIFBvbnk6IFRoZSBNb3ZpZSAoRmFtaWx5IEZyaWVuZGx5KShUaGUgTGVnbyBOaW5qYWdvIE1vdmllIChGYW1pbHkgRnJpZW5kbHkpG0JsYWRlIFJ1bm5lciAyMDQ5IChJTUFYIDNEKRJHZW9zdG9ybSAoSU1BWCAzRCkNUGVhY2UgQnJlYWtlchdWYXNhbnRoYSBWaWxsYXMgMTA6NDVQTRRIYXJhIEhhcmEgTWFoYWRldmFraQ1CYXlhbWEgSXJ1a2t1GUthIEthIEthIEFhYmF0aGluIEFyaWt1cmkGU3B5ZGVyGEJsYWRlIFJ1bm5lciAyMDQ5IChJTUFYKRJHZW9zdG9ybSAoSU1BWCAyRCkISnVkd2FhIDIeVGhlIEZhcm06IEVuIFZlZXR0dSBUaG90dGF0aGlsH0JsYWRlIFJ1bm5lciAyMDQ5IChBVE1PUyBELUJPWCkeQ2VyaXRhLUNlcml0YSBEaWRpIEFuZCBGcmllbmRzFTUBMAsxMTE2NS4yMjA1MAsxMTE4Ny4yMjA5MwsxMTA1OS4yMjEwOQsxMTM2Ni4yMjUzNQsxMTM3NC4yMjU0NgsxMTE4My4yMjY1NgsxMTYzOC4yMzE2NAsxMTU3NS4yMzMxNgsxMTg2OC4yMzY0NQsxMjAwMy4yNDAxNgsxMjE4Ny4yNDQwMAsxMjIwNS4yNDQ0MAsxMjI3NS4yNDYxMQsxMTM0Ny4yNDYxNAsxMjQ1Ni4yNDk3MQsxMjU1NC4yNTE5MAsxMjUzOC4yNTQ5NwsxMjYyMy4yNTUwNQsxMjM1MS4yNTU4NAsxMjc0MS4yNTU4NQsxMTE4Ny4yNTY1OQsxMTE4Ny4yNTY2OQsxMTE4Ny4yNTY3MQsxMTM2Ni4yNTc0NgsxMjgwNS4yNTYyNQsxMjA2Ny4yNDE1NQsxMjM2MC4yNTA4NgsxMTc1MC4yMzQwNwsxMTM2OC4yMjUzOAsxMTkyNS4yMzc5MAsxMjE0NC4yNDU1MgsxMTYxNi4yNTAxNAsxMjU3Mi4yNTIyNAsxMjQxMS4yNTIzMQsxMjQxMi4yNTU1OQsxMjgxNi4yNTY0NwsxMTYxNi4yNTc1MgsxMTM3NC4yNTYyNwsxMTE4Ny4yNTY1MQsxMTM2Ni4yNTY4MwsxMjc3My4yNTU4MwsxMjc5OS4yNTYxNgsxMjgwNy4yNTYzMgsxMjg1MS4yNTcyMAsxMjg3My4yNTc1NQsxMjc3Ny4yNTU5MAsxMTE4Ny4yNTc1MAsxMTM2Ni4yNTc2NAsxMjc1MS4yNTU0MQsxMTEzMC4yMTk2OQsxMTE4Ny4yNTY3NgsxMjE5MC4yNTc1MxQrAzVnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCBw8QDxYGHwAFC3Nob3dEYXRlU3RyHwEFCHNob3dEYXRlHwJnZBAVKBMxNC1PY3QtMTcsIFNhdHVyZGF5ETE1LU9jdC0xNywgU3VuZGF5ETE2LU9jdC0xNywgTW9uZGF5EjE3LU9jdC0xNywgVHVlc2RheRQxOC1PY3QtMTcsIFdlZG5lc2RheRMxOS1PY3QtMTcsIFRodXJzZGF5ETIwLU9jdC0xNywgRnJpZGF5EzIxLU9jdC0xNywgU2F0dXJkYXkRMjItT2N0LTE3LCBTdW5kYXkRMjMtT2N0LTE3LCBNb25kYXkSMjQtT2N0LTE3LCBUdWVzZGF5FDI1LU9jdC0xNywgV2VkbmVzZGF5EzI2LU9jdC0xNywgVGh1cnNkYXkRMjctT2N0LTE3LCBGcmlkYXkTMjgtT2N0LTE3LCBTYXR1cmRheREyOS1PY3QtMTcsIFN1bmRheREzMC1PY3QtMTcsIE1vbmRheRIzMS1PY3QtMTcsIFR1ZXNkYXkUMDEtTm92LTE3LCBXZWRuZXNkYXkTMDItTm92LTE3LCBUaHVyc2RheREwMy1Ob3YtMTcsIEZyaWRheRMwNC1Ob3YtMTcsIFNhdHVyZGF5ETA1LU5vdi0xNywgU3VuZGF5ETA2LU5vdi0xNywgTW9uZGF5EjA3LU5vdi0xNywgVHVlc2RheRQwOC1Ob3YtMTcsIFdlZG5lc2RheRIxNC1Ob3YtMTcsIFR1ZXNkYXkTMTYtTm92LTE3LCBUaHVyc2RheRExNy1Ob3YtMTcsIEZyaWRheRMxOC1Ob3YtMTcsIFNhdHVyZGF5ETE5LU5vdi0xNywgU3VuZGF5ETIwLU5vdi0xNywgTW9uZGF5EjIxLU5vdi0xNywgVHVlc2RheRQyMi1Ob3YtMTcsIFdlZG5lc2RheRMzMC1Ob3YtMTcsIFRodXJzZGF5ETAxLURlYy0xNywgRnJpZGF5EzAyLURlYy0xNywgU2F0dXJkYXkRMDMtRGVjLTE3LCBTdW5kYXkRMDQtRGVjLTE3LCBNb25kYXkUMDYtRGVjLTE3LCBXZWRuZXNkYXkVKAgxMC8xNC8xNwgxMC8xNS8xNwgxMC8xNi8xNwgxMC8xNy8xNwgxMC8xOC8xNwgxMC8xOS8xNwgxMC8yMC8xNwgxMC8yMS8xNwgxMC8yMi8xNwgxMC8yMy8xNwgxMC8yNC8xNwgxMC8yNS8xNwgxMC8yNi8xNwgxMC8yNy8xNwgxMC8yOC8xNwgxMC8yOS8xNwgxMC8zMC8xNwgxMC8zMS8xNwcxMS8xLzE3BzExLzIvMTcHMTEvMy8xNwcxMS80LzE3BzExLzUvMTcHMTEvNi8xNwcxMS83LzE3BzExLzgvMTcIMTEvMTQvMTcIMTEvMTYvMTcIMTEvMTcvMTcIMTEvMTgvMTcIMTEvMTkvMTcIMTEvMjAvMTcIMTEvMjEvMTcIMTEvMjIvMTcIMTEvMzAvMTcHMTIvMS8xNwcxMi8yLzE3BzEyLzMvMTcHMTIvNC8xNwcxMi82LzE3FCsDKGdnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2cWAWZkZATVpZZw1+j+wktMHPKJmRw0MvGw" />


<script src="/ScriptResource.axd?d=-6PURz02oQuKlkAEimPEMnGROvbMNBXbWvEdQK7KP6PDEGjR5A-haiEM53_6bA17UeaXZdQe2r_cBt3YHoyuBlWW6aR_iRA-MxvRlnn7UBR5dmSi8Hr71sZ0zKGH8CKAOyZ2t3tse3WMo3tozwDH6pWIE1284QsdKJ9pi1DlbIkmgZyf0&amp;t=ffffffffbcb9b94a" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(Sys) === 'undefined') throw new Error('ASP.NET Ajax client-side framework failed to load.');
//]]>
</script>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2927320D" />
<input type="hidden" name="__PREVIOUSPAGE" id="__PREVIOUSPAGE" value="Co2a2GXMfL6cupotct6qS8fFQPmbNrkKTTnxQSjDjBY3gnETbDD0oeaGPNsleHosVxT7h6eA0wmdB18uAMcPOGScV2A1" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWrwICxtXCkAMC6tTc3AwCtIKN8ggC5JWelQMC76yt5gQCw4XP7wYC//iY1wwCkNKo6A4C55norQMCt+fPwgMCs9251gUCqN311QUCqN3B1QUCqN2Z1gUCrN311QUCqN3d1QUCr93d1QUCqN3F1QUCr93x1QUCr93N1QUCrt3B1QUCr93F1QUCrt3F1QUCr93J1QUCrt2Z1gUCrt2V1gUCr9351QUCr93B1QUCqN3N1QUCqN3J1QUCqN2V1gUCu93J1QUCr9311QUCrN3B1QUCrN2V1gUCrN3d1QUCrN2Z1gUCrd351QUCtN251gUCqd251gUCu9251gUCqt251gUCrN351QUCrd311QUCqd3N1QUCrd3x1QUCqd3J1QUCrd3J1QUCrd3N1QUCrd3F1QUCrd3d1QUCrN3x1QUCrN3N1QUCrN3J1QUCrN3F1QUCrN251gUCtN3N1QUCrt251gUCr9251gUCqN251gUCrd251gUCr92V1gUCqd311QUCrt311QUCrd2Z1gUCrd2V1gUCrt351QUCrt3J1QUCp6SXzg0CvKTbzQ0Cp5jVrgUCvKTfzQ0CvKTjzQ0CvKTnzQ0Cp5jJrgUC1sX37gMC0IbhhQUCvKS7zg0CvaTXzQ0CwqK/2ggCm/e7hA8CwqLr2QgC99ym7wQCiNyy7wQCk+XMmQoC34blhQUC34bhhQUCvKTrzQ0CvaTfzQ0CvKTzzQ0C8O6VmQkCiNym7wQCwqLv2QgCppidrwUCvKTvzQ0CnPe7hA8Cp5jNrgUCvKS3zg0C+q+DsAsC8e6RmQkC34aVhQUCnPezhA8C+6+DsAsC+q+7sAsCopjBrgUCwaL32QgCk+WwmgoC1sXH7gMC8O6ZmQkC0IaBhQUC+q+3sAsC99z27wQC+q+vsAsC7MuIxA4C7cv4xA4Cp5i9rwUCwqL32QgCnPejhA8C/O6FmQkCp5idrwUC8O6BmQkCidyW7wQCkuW8mgoCm/e3hA8C99ye7wQCk+W0mgoCm/eDhA8C99yi7wQC8e6VmQkCuaTfzQ0CuKSXzg0ClOWwmgoCuaTbzQ0CuKS3zg0C34aJhQUCkuWUmgoC99z67wQC0IaZhQUCuKTzzQ0C7cuIxA4Ck+XImQoCuaTjzQ0C7cuMxA4CuqSXzg0C0IblhQUCu6SXzg0CvqSXzg0CiNyu7wQC8e6ZmQkC8O7hmQkCuKTfzQ0CuKS7zg0CuaTXzQ0C1sXz7gMCvaSXzg0CkuXMmQoC7Mv0xA4CuKTXzQ0C7MuAxA4Cm/f/hA8CppjJrgUCzqLz2QgC8e6BmQkCnPefhA8C1cXH7gMC89ya7wQCh/ezhA8CnuW0mgoC8O6JmQkCzaLf2QgC1cXv7gMC+6+HsAsC6Mv0xA4C34aNhQUC7cvUxA4CnPeDhA8C8e7dmQkC7MvYxA4CkuXAmQoCnuWQmgoC0cXn7gMCp5iZrwUC1sXD7gMCnuWUmgoC89z67wQCnPerhA8CwaLr2QgCvqTvzQ0CvqTnzQ0CnfefhA8C8u6BmQkClOW0mgoC0YaBhQUCuJi9rwUC18Xj7gMC7sv4xA4Cidya7wQC8u79mQkCw6Lf2QgCnfejhA8C/K+jsAsCuJi5rwUC18Xn7gMCwqLb2QgCwqLn2QgC7MuExA4Ck+WQmgoCk+WUmgoC7cvYxA4C24WFng8C85HnoAgC+6TzzQgCxvau1AUCzP+c2AYC4cGasw4C4KuYowsC9Znr1wwCjsfL+gUC3sHS0AUCtb7ZjQYCqPic7g0C67Tomw8Cn9qs2AcCuP7UQgLH5aSWDQKJ3IrHBgKuxq+xAgKH5rDyBgKElOepDQKoham6BQKTr8SMCwKTr8CMCwLLlqujBAKhwfqjCgK+09m5CgLExbj7DwL4xo6ECgL73sLpCQLr5MTOBgKL5d72CgKmjqXhBwLOn5/BDAK4tt9wAp658okBAtj069MHAuuC2b8FAuuG3MAPAtmo/J0HAsuW06IEAq/3/5kFAvOzj/QEAszY8oIEAvfG8q8FAoeC4cwHAvLHjoEJAsb+2bMGAq348M0OAuuUt44OAv/Bm8oFAuGslI8PAuSqhOMKAt2l/74OAovU/LkPAozU/LkPAo3U/LkPAo7U/LkPArfU/LkPAojU/LkPAo/U6PkPAoDU6PkPAoHU6PkPAoLU6PkPAovU6PkPAozU6PkPAo3U6PkPAo7U6PkPArfU6PkPAojU6PkPAo/U1NULAoDU1NULApjN4/gMApjN97gMApjNi/kLApjNn7kLApjNs/kKApjNx7kKApjN2/kJApjNr7sBAovU+MQPAo3U+MQPAo7U+MQPArfU+MQPAojU+MQPAo/U5AQCgNTkBAKB1OQEAo/U0NALApjN3/sMApjN87sMApjNh/gLApjNm7gLApjNw7gKAqXNhocLIInbzRKfe7wJ+YjUn9YFLhFRgkI=" />
    
    <div id="master" class="shadow">
        <!-- BEGIN EFFECTIVE MEASURE CODE -->
        <!-- COPYRIGHT EFFECTIVE MEASURE -->
        <script type="text/javascript">
            (function () {
                var em = document.createElement('script'); em.type = 'text/javascript';
                em.async = true;
                em.src = ('https:' == document.location.protocol ? 'https://my-ssl' :
                'http://my-cdn') + '.effectivemeasure.net/em.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(em, s);
            })(); 
        </script>
        <noscript>
            <img src="//my.effectivemeasure.net/em_image" alt="" style="position: absolute; left: -5px;" />
        </noscript>
        <!--END EFFECTIVE MEASURE CODE -->  
         
        <div id="header">
            <div style="width: 91%; display: table-cell; border:none; vertical-align: middle;">                                                                       
                <table class="tbl_standard">
                    <tr style="vertical-align: middle;">
                        <td>English|<a href="http://www.cinema.com.my/bm/default.aspx" target="_blank">B. Malaysia</a> &nbsp;&nbsp;&nbsp;Like Us!</td>
                        <td><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fcinemaonline&amp;width=47&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20&amp;appId=583362251764516" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:47px; height:20px;" allowTransparency="true"></iframe></td>
                        <td><input name="ctl00$Search1$q" type="text" maxlength="512" id="ctl00_Search1_q" />&nbsp;
<input type="submit" name="ctl00$Search1$sa" value="Search" id="ctl00_Search1_sa" />                
<input type="hidden" name="ctl00$Search1$cx" id="ctl00_Search1_cx" value="013128572172917708998:wkt14jbgegg" />
<input type="hidden" name="ctl00$Search1$cof" id="ctl00_Search1_cof" value="FORID:11" /></td>
                    </tr>
                </table>
            </div>                                        
            


<script src="/members/js/login.js" type="text/javascript"></script>
<div id="loginContainer">
    
            <a href="#" id="loginButton"><span>Login</span><em></em></a>
            <table id="ctl00_ctrl_login1_lvLogin_Login1" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td>
                    <div id="loginBox">
                        <div id="loginForm">
                            <fieldset id="body">
                                <fieldset>
                                    <input name="ctl00$ctrl_login1$lvLogin$Login1$UserName" type="text" id="ctl00_ctrl_login1_lvLogin_Login1_UserName" placeholder="Username" />
                                    
                                </fieldset>
                                <fieldset>
                                    <input name="ctl00$ctrl_login1$lvLogin$Login1$Password" type="password" id="ctl00_ctrl_login1_lvLogin_Login1_Password" placeholder="Password" />
                                    
                                </fieldset>
                                <input type="submit" name="ctl00$ctrl_login1$lvLogin$Login1$btnSubmit" value="Log In" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$btnSubmit&quot;, &quot;&quot;, true, &quot;login&quot;, &quot;&quot;, false, false))" id="ctl00_ctrl_login1_lvLogin_Login1_btnSubmit" />
                                                              
                            </fieldset>
                            <span>
                                <a id="ctl00_ctrl_login1_lvLogin_Login1_lbtnRegister" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$lbtnRegister&quot;, &quot;&quot;, false, &quot;&quot;, &quot;members/registration.aspx&quot;, false, true))">Register</a>&nbsp;|&nbsp;<a id="ctl00_ctrl_login1_lvLogin_Login1_lbtnForgotPassword" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$lbtnForgotPassword&quot;, &quot;&quot;, false, &quot;&quot;, &quot;members/forgot_password.aspx&quot;, false, true))">Forgot your password?</a>
                            </span>
                        </div>
                    </div>
                </td>
	</tr>
</table>            
        
</div>

            
        </div>
                
        <div id="logo">
            <table style="width: 1065px;">
                <tr>
                    <td style="width: 280px">
                        <table style="width:100%">
                            <tr>
                                <td>
									
    <!-- COMY_logo -->
	<div id='div-gpt-ad-1412224214767-2' style='width: 50px; height: 60px;'>
		<script type='text/javascript'>
			googletag.display('div-gpt-ad-1412224214767-2');
		</script>
	</div>

                                </td>
                                <td>
                                    <!-- CINEMA ONLINE text -->
                                    <a href="http://www.cinema.com.my/default.aspx" style="text-decoration: none">
                                        <font style="letter-spacing: -1px; font-family: Arial; font-size: 35px; font-weight: bold; color: #2F3193; text-decoration: none;
                                        text-transform: none;">Cinema</font><font style="letter-spacing: -0.5px; font-family: 'Times New Roman'; font-size: 36px; font-weight: bolder; color: #AA1C20; text-decoration: none; text-transform: none;"> Online</font>
                                        <br />
                                    </a><font style="font-family: Arial;font-size:11px;color:#000000">&nbsp;&nbsp;<i>Malaysia's Favourite Movie Site</i></font>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 785px;" align="right">
                        
    <!-- COMY_privacy_sb -->
	<div id='div-gpt-ad-1408719234135-32' style='width:728px; height:90px;'>
		<script type='text/javascript'>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1408719234135-32'); });
		</script>
	</div>

                    </td>
                </tr>
            </table>
        </div>

        <div id="nav_menu_wrap">
            
            <nav >
                <ul>
                    <li><a href="/default.aspx">Home</a></li>
                    <li><a href="/movies/nowshowing.aspx">Now Showing</a></li>
                    <li><a href="/movies/comingsoon.aspx">Coming Soon</a></li>
                    <li><a href="/articles/news.aspx">News</a></li>
                    <li><a href="/articles/features.aspx">Features</a></li>
                    <li><a href="/trailers/trailers.aspx">Trailers</a></li>
                    <li><a href="/contests/contests.aspx">Contests</a></li>
                    <li><a href="/moviefreak/moviefreak.aspx">Movie Freak</a></li>
                    <li><a href="/filmfestivals/default.aspx">Film Festivals</a></li>
                    <li><a href="/cinemas/cinemalist.aspx">Showtimes</a> </li>
                </ul>
            </nav>
        </div>
        
        <div id="content">
            
			
            
    <table style="width: 1065px">
        <tr>
            <td style="width: 735px; vertical-align: top;">
                <div class="main_section">
                    <div class="section_header">
                        Privacy Policy
                    </div>
                    <div class="section_content">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <br />
                                    <p>
                                        This Privacy Policy governs the manner in which Cinema Online collects, uses, maintains
                                        and discloses information collected from users (each, a "User") of the <a href="http://www.cinema.com.my">
                                            cinema.com.my</a> website ("Site"). This privacy policy applies to the Site
                                        and all products and services offered by Cinema Online.</p>
                                    <span class="red-title">Personal identification information</span>                                        
                                    <p>
                                        We may collect personal identification information from Users in a variety of ways,
                                        including, but not limited to, when Users visit our site, register on the site,
                                        subscribe to the newsletter, respond to a survey, fill out a form, and in connection
                                        with other activities, services, features or resources we make available on our
                                        Site. Users may be asked for, as appropriate, name, email address, mailing address,
                                        phone number, and NRIC. Users may, however, visit our Site anonymously. We will
                                        collect personal identification information from Users only if they voluntarily
                                        submit such information to us. Users can always refuse to supply personally identification
                                        information, except that it may prevent them from engaging in certain Site related
                                        activities.</p>
                                    <span class="red-title">Non-personal identification information</span>                                        
                                    <p>
                                        We may collect non-personal identification information about Users whenever they
                                        interact with our Site. Non-personal identification information may include the
                                        browser name, the type of computer and technical information about Users means of
                                        connection to our Site, such as the operating system and the Internet service providers
                                        utilized and other similar information.</p>
                                    <span class="red-title">Web browser cookies</span>                                        
                                    <p>
                                        Our Site may use "cookies" (such as Google Analytics cookie and DoubleClick cookie)
                                        to enhance User experience. User's web browser places cookies on their hard drive
                                        for record-keeping purposes and sometimes to track information about them. User
                                        may choose to set their web browser to refuse cookies, or to alert you when cookies
                                        are being sent. If they do so, note that some parts of the Site may not function
                                        properly.</p>
                                    <span class="red-title">How we use collected information</span>                                        
                                    <p>
                                        Cinema Online may collect and use Users personal information for the following purposes:<br>
                                        <ul>
                                            <li><i>To improve customer service</i><br />
                                                Information you provide helps us respond to your customer service requests and support
                                                needs more efficiently.</li>
                                            <li><i>To personalize user experience</i><br />
                                                We may use information in the aggregate to understand how our Users as a group use
                                                the services and resources provided on our Site.</li>
                                            <li><i>To improve our Site</i><br />
                                                We may use feedback you provide to improve our products and services.</li>
                                            <li><i>To run a promotion, contest, survey or other Site feature</i><br />
                                                To send Users information they agreed to receive about topics we think will be of
                                                interest to them.</li>
                                            <li><i>To send periodic emails</i><br />
                                                We may use the email address to respond to their inquiries, questions, and/or other
                                                requests. If User decides to opt-in to our mailing list, they will receive emails
                                                that may include company news, updates, or related service information, etc. If
                                                at any time the User would like to unsubscribe from receiving future emails, we
                                                include detailed unsubscribe instructions at the bottom of each email.</li>
                                        </ul>
                                    </p>
                                    <span class="red-title">How we protect your information</span>                                        
                                    <p>
                                        We adopt appropriate data collection, storage and processing practices and security
                                        measures to protect against unauthorized access, alteration, disclosure or destruction
                                        of your personal information, username, password, and data stored on our Site.</p>
                                    <span class="red-title">Sharing your personal information</span>                                        
                                    <p>
                                        We do not sell, trade, or rent Users personal identification information to others.
                                        We may share generic aggregated demographic information not linked to any personal
                                        identification information regarding visitors and users with our business partners,
                                        trusted affiliates and advertisers for the purposes outlined above. We may use third
                                        party service providers to help us operate our business and the Site or administer
                                        activities on our behalf, such as sending out newsletters or surveys. We may share
                                        your information with these third parties for those limited purposes provided that
                                        you have given us your permission.</p>
                                    <span class="red-title">Third party websites</span>                                        
                                    <p>
                                        Users may find advertising or other content on our Site that link to the sites and
                                        services of our partners, suppliers, advertisers, sponsors, licensors and other
                                        third parties. We do not control the content or links that appear on these sites
                                        and are not responsible for the practices employed by websites linked to or from
                                        our Site. In addition, these sites or services, including their content and links,
                                        may be constantly changing. These sites and services may have their own privacy
                                        policies and customer service policies. Browsing and interaction on any other website,
                                        including websites which have a link to our Site, is subject to that website's own
                                        terms and policies.</p>
                                    <span class="red-title">Advertising</span>                                        
                                    <p>
                                        Ads appearing on our site may be delivered to Users by advertising partners, who
                                        may set cookies. These cookies allow the ad server to recognize your computer each
                                        time they send you an online advertisement to compile non personal identification
                                        information about you or others who use your computer. This information allows ad
                                        networks to, among other things, deliver targeted advertisements that they believe
                                        will be of most interest to you. This privacy policy does not cover the use of cookies
                                        by any advertisers.</p>
                                    <span class="red-title">Google Adsense</span>                                        
                                    <p>
                                        Some of the ads may be served by Google. Google's use of the DART cookie enables
                                        it to serve ads to Users based on their visit to our Site and other sites on the
                                        Internet. DART uses "non personally identifiable information" and does NOT track
                                        personal information about you, such as your name, email address, physical address,
                                        etc. You may opt out of the use of the DART cookie by visiting the Google <a href="http://www.google.com/privacy_ads.html" target="_blank">
                                            ad and content network privacy policy</a>.</p>
                                    <span class="red-title">Google Analytics</span>                                        
                                    <p>
                                        Some features implemented are based on Display Advertising for Google Analytics
                                        Demographics and Interest Reporting. Using the <a href="http://www.google.com/settings/ads"
                                            target="_blank">Ads Settings</a>, Users can opt-out of Google Analytics for
                                        Display Advertising and customize Google Display Network ads.<br>
                                        <br>
                                        Users may also check out Google Analytics' <a href="http://tools.google.com/dlpage/gaoptout/"
                                            target="_blank">currently available opt-outs</a> for the web.<br>
                                        <br>
                                        The data gathered from Google's Personalized advertising (formerly known as interest-based advertising) or 3rd-party audience
                                        data (such as age, gender, and interests) with Google Analytics shall be used to
                                        understand our Users better in order to improve our products and services.</p>
                                    <span class="red-title">Changes to this privacy policy</span>                                        
                                    <p>
                                        Cinema Online has the discretion to update this privacy policy at any time. When
                                        we do, we will revise the updated date at the bottom of this page. We encourage
                                        Users to frequently check this page for any changes to stay informed about how we
                                        are helping to protect the personal information we collect. You acknowledge and
                                        agree that it is your responsibility to review this privacy policy periodically
                                        and become aware of modifications.</p>
                                    <span class="red-title">Your acceptance of these terms</span>                                        
                                    <p>
                                        By using this Site, you signify your acceptance of this policy. If you do not agree
                                        to this policy, you may write in to request for a deletion or edit of your personal
                                        information. Your continued use of the Site following the posting of changes to
                                        this policy will be deemed your acceptance of those changes.</p>
                                    <span class="red-title">E.T. phone number</span>                                        
                                    <p>
                                        If you have any questions about this Privacy Policy, the practices of this site,
                                        or your dealings with this site, please contact us at:<br />
                                        <br />
                                        Cinema Online Sdn. Bhd.<br />
                                        No: 7A, Jalan SS4D/2,<br />
                                        People’s Park,<br />
                                        47301 Petaling Jaya,<br />
                                        Selangor, Malaysia.<br />
                                        <br>
                                        Tel: +603 7805 1527 (Office hours only, as stated below)<br>
                                        Email: marketing@cinemaonline.asia<br />
                                        <br />
                                        Office Hours: Monday – Friday (closed on public holidays), 9:30am - 6:00pm<br />
                                        <br />
                                        This document was last updated on 7 April  2017<br />
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
             <td style="width: 5px">
                &nbsp;
            </td>
            <td style="width: 325px; vertical-align: top;">
                
<style>
    .styled-select
    {
        width: 100%;
        height: 34px;
        overflow: hidden;
        border: 1px solid #ccc;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    
    .ddlframe
    {
        background-color: #a6a6a6;
        padding: 5px 5px 5px 5px;
        margin-bottom: 5px;
    }
    
    .submit
    {
        border: 1px solid #563d7c;
        border-radius: 5px;
        color: white;
        padding: 5px 10px 5px 25px;
        background: url(https://i.stack.imgur.com/jDCI4.png) left 3px top 5px no-repeat #563d7c;
    }
    
    
</style>

<div class="main_section sidebar_bg2">
    <div class="section_header sidebar_header">
        Showtimes
    </div>
    <div>
        <div id="ctl00_cpContent_ShowtimesBox1_ajxUpdatePanel">
	
                <div class="ddlframe">
                    <select name="ctl00$cpContent$ShowtimesBox1$ddlArea" id="ctl00_cpContent_ShowtimesBox1_ddlArea" class="styled-select">
		<option selected="selected" value="0">Select Area / All Area</option>
		<option value="51">City Center</option>
		<option value="56">Kuala Lumpur</option>
		<option value="58">Old Klang Road</option>
		<option value="11">Cheras</option>
		<option value="57">Desa Petaling</option>
		<option value="47">Ampang</option>
		<option value="55">Wangsa Maju</option>
		<option value="42">Petaling Jaya</option>
		<option value="43">Sunway</option>
		<option value="36">Subang</option>
		<option value="45">Kota Damansara</option>
		<option value="35">Shah Alam</option>
		<option value="44">Puchong</option>
		<option value="38">Klang</option>
		<option value="39">Kajang</option>
		<option value="40">Seri Kembangan</option>
		<option value="46">Rawang</option>
		<option value="53">Kepong</option>
		<option value="54">Setapak</option>
		<option value="59">Putrajaya</option>
		<option value="84">Cyberjaya</option>
		<option value="41">Kuala Selangor</option>
		<option value="16">Georgetown</option>
		<option value="19">Seberang Perai</option>
		<option value="17">Bayan Lepas</option>
		<option value="18">Butterworth</option>
		<option value="20">Seberang Jaya</option>
		<option value="9">Sungai Petani</option>
		<option value="6">Alor Setar</option>
		<option value="8">Langkawi</option>
		<option value="7">Jitra</option>
		<option value="10">Kulim</option>
		<option value="21">Ipoh</option>
		<option value="63">Parit Buntar</option>
		<option value="22">Kampar</option>
		<option value="64">Seri Manjung</option>
		<option value="24">Seri Iskandar</option>
		<option value="23">Sitiawan</option>
		<option value="25">Taiping</option>
		<option value="27">Teluk Intan</option>
		<option value="12">Malacca</option>
		<option value="13">Seremban</option>
		<option value="14">Bahau</option>
		<option value="15">Kuantan</option>
		<option value="1">Johor Bahru</option>
		<option value="93">Masai</option>
		<option value="3">Batu Pahat</option>
		<option value="4">Segamat</option>
		<option value="5">Kluang</option>
		<option value="2">Kota Tinggi</option>
		<option value="49">Terengganu</option>
		<option value="61">Mentakab</option>
		<option value="31">Kuching</option>
		<option value="28">Kota Kinabalu</option>
		<option value="29">Sandakan</option>
		<option value="30">Tawau</option>
		<option value="34">Miri</option>

	</select>
                </div>
                <div class="ddlframe">
                    <select name="ctl00$cpContent$ShowtimesBox1$ddlCinemas" id="ctl00_cpContent_ShowtimesBox1_ddlCinemas" class="styled-select">
		<option selected="selected" value="0">Select Cinema / All Cinemas</option>
		<option value="51">TGV - Suria KLCC</option>
		<option value="674">TGV - Sunway Putra</option>
		<option value="52">TGV - Mines</option>
		<option value="53">TGV - Sunway Pyramid</option>
		<option value="54">TGV - 1 Utama</option>
		<option value="644">TGV - Encorp Strand</option>
		<option value="657">TGV - Jaya Shopping Centre</option>
		<option value="689">TGV - AEON Klebang</option>
		<option value="59">TGV - Kepong</option>
		<option value="60">TGV - Cheras Selatan</option>
		<option value="693">TGV - Sunway Velocity</option>
		<option value="575">TGV - 1 Shamelin</option>
		<option value="643">TGV - Cheras Sentral</option>
		<option value="541">TGV - Wangsa Walk</option>
		<option value="671">TGV - AEON AU2</option>
		<option value="670">TGV - DPulze Cyberjaya</option>
		<option value="599">TGV - Setia Walk</option>
		<option value="589">TGV - Rawang</option>
		<option value="55">TGV - Bukit Raja</option>
		<option value="62">TGV - Bukit Tinggi</option>
		<option value="57">TGV - Seremban 2 Shopping Centre</option>
		<option value="566">TGV - 1st Avenue</option>
		<option value="641">TGV - Gurney Paragon Mall</option>
		<option value="653">TGV - AEON Bukit Mertajam</option>
		<option value="594">TGV - Station 18</option>
		<option value="56">TGV - Kinta City</option>
		<option value="675">TGV - Aeon Taiping</option>
		<option value="654">TGV - AEON Seri Manjung</option>
		<option value="58">TGV - Tebrau City</option>
		<option value="588">TGV - Bukit Indah</option>
		<option value="656">TGV - AEON Kulaijaya</option>
		<option value="559">TGV - Mesra Mall</option>
		<option value="655">TGV - Imperial City Mall</option>
		<option value="688">TGV - Vivacity Megamall</option>
		<option value="568">MBO - Viva Home</option>
		<option value="124">MBO - Galaxy Cineplex</option>
		<option value="573">MBO - Subang Parade</option>
		<option value="600">MBO - Citta Mall</option>
		<option value="697">MBO - Starling Mall</option>
		<option value="576">MBO - Space U8 Mall</option>
		<option value="609">MBO - Brem Mall</option>
		<option value="558">MBO - Kepong Village Mall</option>
		<option value="581">MBO - Setapak Central</option>
		<option value="538">MBO - Spark Cineplex</option>
		<option value="552">MBO - Harbour Place</option>
		<option value="612">MBO - Kulim Landmark Central</option>
		<option value="614">MBO - Taiping Sentral</option>
		<option value="673">MBO - Central Square</option>
		<option value="615">MBO - Teluk Intan</option>
		<option value="126">MBO - Melaka Mall</option>
		<option value="694">MBO - Elements Mall</option>
		<option value="516">MBO - Terminal 2 Seremban</option>
		<option value="701">MBO - Kuantan City Mall</option>
		<option value="530">MBO - U Mall</option>
		<option value="565">MBO - KSL City</option>
		<option value="521">MBO - Square One</option>
		<option value="610">MBO - Kluang Mall</option>
		<option value="595">MBO - Heritage Mall</option>
		<option value="531">MBO - Spring Mall</option>
		<option value="666">MBO - Imago Kota Kinabalu</option>
		<option value="22">GSC - Signature Gardens</option>
		<option value="1">GSC - Mid Valley</option>
		<option value="700">GSC - Melawati Mall</option>
		<option value="21">GSC - Pavilion KL</option>
		<option value="18">GSC - 1 Utama</option>
		<option value="529">GSC - Tropicana City</option>
		<option value="590">GSC - Paradigm Mall</option>
		<option value="591">GSC - Setia City Mall</option>
		<option value="669">GSC - Klang Parade</option>
		<option value="17">GSC - Berjaya Times Square</option>
		<option value="652">GSC - Quill City Mall</option>
		<option value="660">GSC - Nu Sentral</option>
		<option value="23">GSC - Alamanda Putrajaya</option>
		<option value="662">GSC - IOI City Mall</option>
		<option value="3">GSC - Cheras Leisure Mall</option>
		<option value="699">GSC - MyTown</option>
		<option value="4">GSC - Summit USJ</option>
		<option value="7">GSC - IOI Mall</option>
		<option value="661">GSC - Ipoh Parade</option>
		<option value="676">GSC - Aman Central</option>
		<option value="596">GSC - Amanjaya Mall</option>
		<option value="12">GSC - Gurney Plaza</option>
		<option value="19">GSC - Queensbay</option>
		<option value="20">GSC - Sunway Carnival</option>
		<option value="647">GSC - Palm Mall</option>
		<option value="6">GSC - Terminal 1</option>
		<option value="570">GSC - AEON Bandaraya Melaka</option>
		<option value="502">GSC - Dataran Pahlawan</option>
		<option value="10">GSC - Berjaya Megamall</option>
		<option value="532">GSC - East Coast Mall</option>
		<option value="585">GSC - Mentakab Star Mall</option>
		<option value="544">GSC - Suria Sabah</option>
		<option value="263">GSC - 1Borneo</option>
		<option value="616">GSC - CityONE</option>
		<option value="605">GSC - Bintang Megamall</option>
		<option value="597">LFS - Coliseum Cineplex</option>
		<option value="111">LFS - State Cineplex PJ</option>
		<option value="155">LFS - Metro Plaza Kajang</option>
		<option value="110">LFS - Sri Intan Klang</option>
		<option value="536">LFS - KM Plaza</option>
		<option value="113">LFS - Butterworth</option>
		<option value="537">LFS - Seri Kinta Ipoh</option>
		<option value="698">LFS - Sitiawan</option>
		<option value="102">LFS - Plaza Tasek Skudai JB</option>
		<option value="539">LFS - Skudai Parade</option>
		<option value="682">LFS - Harbour Mall Sandakan</option>
		<option value="695">LFS - PB Sentral</option>
		<option value="686">City C'plex - City Cineplex</option>
		<option value="592">Eastern C'plex - Eastern Plaza</option>
		<option value="540">Grand Cineplex - Village Mall</option>
		<option value="180">Growball - Growball Cinemax</option>
		<option value="117">Jitra Mall C'plex - Jitra Mall</option>
		<option value="684">KPI - Broadway JB</option>
		<option value="687">KPI - Perling Mall</option>
		<option value="190">Mega C'plex - Megamall Pinang (Level 4)</option>
		<option value="191">Mega C'plex - Langkawi Parade</option>
		<option value="635">Mega C'plex - Bertam</option>
		<option value="543">Megalong C'plex - Megalong Mall Penampang</option>
		<option value="76">mmCineplexes - Damansara</option>
		<option value="74">mmCineplexes - City Square</option>
		<option value="705">mmCineplexes - Shaw Centrepoint</option>
		<option value="716">mmCineplexes - 1 Plaza Kuala Selangor</option>
		<option value="710">mmCineplexes - Kiara Square Bahau</option>
		<option value="709">mmCineplexes - Mahkota Parade</option>
		<option value="714">mmCineplexes - Bukit Jambul Penang</option>
		<option value="707">mmCineplexes - Prangin Mall</option>
		<option value="712">mmCineplexes - Kampar</option>
		<option value="711">mmCineplexes - Kerian Sentral Mall</option>
		<option value="706">mmCineplexes - Seri Iskandar</option>
		<option value="713">mmCineplexes - IOI Mall</option>
		<option value="715">mmCineplexes - 1 Segamat</option>
		<option value="708">mmCineplexes - Riverside Kuching</option>
		<option value="704">mmCineplexes - Summer Mall</option>
		<option value="717">mmCineplexes - Riverfront City</option>
		<option value="603">One Cinemas - Spectrum Ampang</option>
		<option value="633">Premium-X - One City Mall</option>
		<option value="542">Superstar - Today's Mall Ulu Tiram</option>
		<option value="680">Superstar - Today's Market Seri Alam</option>
		<option value="690">TSR - IRDKL Mall Shah Alam</option>
		<option value="692">Paragon - Taiping Mall</option>

	</select>
                </div>
                <div class="ddlframe">
                    <select name="ctl00$cpContent$ShowtimesBox1$ddlMovies" id="ctl00_cpContent_ShowtimesBox1_ddlMovies" class="styled-select">
		<option selected="selected" value="0">Select Movie / All Movies</option>
		<option value="11165.22050">Kingsman: The Golden Circle</option>
		<option value="11187.22093">Blade Runner 2049</option>
		<option value="11059.22109">American Made</option>
		<option value="11366.22535">Geostorm</option>
		<option value="11374.22546">The Lego Ninjago Movie</option>
		<option value="11183.22656">Cars 3</option>
		<option value="11638.23164">Tombiruo: Penunggu Rimba</option>
		<option value="11575.23316">It</option>
		<option value="11868.23645">Bisik Pada Langit</option>
		<option value="12003.24016">Shuttle Life</option>
		<option value="12187.24400">Mother!</option>
		<option value="12205.24440">The Foreigner</option>
		<option value="12275.24611">Midnight Diner 2 (JFF)</option>
		<option value="11347.24614">Tsukiji Wonderland (JFF)</option>
		<option value="12456.24971">The Irregular At Magic High School</option>
		<option value="12554.25190">Turn Around</option>
		<option value="12538.25497">Chasing The Dragon</option>
		<option value="12623.25505">The Crucifixion</option>
		<option value="12351.25584">Escape Room</option>
		<option value="12741.25585">Let Me Eat Your Pancreas</option>
		<option value="11187.25659">Blade Runner 2049 (D-BOX)</option>
		<option value="11187.25669">Blade Runner 2049 (3D)</option>
		<option value="11187.25671">Blade Runner 2049 (ATMOS)</option>
		<option value="11366.25746">Geostorm (D-BOX)</option>
		<option value="12805.25625">Karuppan</option>
		<option value="12067.24155">I'm Not A Terrorist</option>
		<option value="12360.25086">Abang Long Fadil 2</option>
		<option value="11750.23407">American Assassin</option>
		<option value="11368.22538">Annabelle: Creation</option>
		<option value="11925.23790">The Son Of Bigfoot</option>
		<option value="12144.24552">Tokyo Ghoul</option>
		<option value="11616.25014">My Little Pony: The Movie</option>
		<option value="12572.25224">Serpent</option>
		<option value="12411.25231">Stronger</option>
		<option value="12412.25559">Midnight Runners</option>
		<option value="12816.25647">Chef</option>
		<option value="11616.25752">My Little Pony: The Movie (Family Friendly)</option>
		<option value="11374.25627">The Lego Ninjago Movie (Family Friendly)</option>
		<option value="11187.25651">Blade Runner 2049 (IMAX 3D)</option>
		<option value="11366.25683">Geostorm (IMAX 3D)</option>
		<option value="12773.25583">Peace Breaker</option>
		<option value="12799.25616">Vasantha Villas 10:45PM</option>
		<option value="12807.25632">Hara Hara Mahadevaki</option>
		<option value="12851.25720">Bayama Irukku</option>
		<option value="12873.25755">Ka Ka Ka Aabathin Arikuri</option>
		<option value="12777.25590">Spyder</option>
		<option value="11187.25750">Blade Runner 2049 (IMAX)</option>
		<option value="11366.25764">Geostorm (IMAX 2D)</option>
		<option value="12751.25541">Judwaa 2</option>
		<option value="11130.21969">The Farm: En Veettu Thottathil</option>
		<option value="11187.25676">Blade Runner 2049 (ATMOS D-BOX)</option>
		<option value="12190.25753">Cerita-Cerita Didi And Friends</option>

	</select>
                </div>
                <div class="ddlframe">
                    <select name="ctl00$cpContent$ShowtimesBox1$ddlDates" id="ctl00_cpContent_ShowtimesBox1_ddlDates" class="styled-select">
		<option selected="selected" value="10/14/17">14-Oct-17, Saturday</option>
		<option value="10/15/17">15-Oct-17, Sunday</option>
		<option value="10/16/17">16-Oct-17, Monday</option>
		<option value="10/17/17">17-Oct-17, Tuesday</option>
		<option value="10/18/17">18-Oct-17, Wednesday</option>
		<option value="10/19/17">19-Oct-17, Thursday</option>
		<option value="10/20/17">20-Oct-17, Friday</option>
		<option value="10/21/17">21-Oct-17, Saturday</option>
		<option value="10/22/17">22-Oct-17, Sunday</option>
		<option value="10/23/17">23-Oct-17, Monday</option>
		<option value="10/24/17">24-Oct-17, Tuesday</option>
		<option value="10/25/17">25-Oct-17, Wednesday</option>
		<option value="10/26/17">26-Oct-17, Thursday</option>
		<option value="10/27/17">27-Oct-17, Friday</option>
		<option value="10/28/17">28-Oct-17, Saturday</option>
		<option value="10/29/17">29-Oct-17, Sunday</option>
		<option value="10/30/17">30-Oct-17, Monday</option>
		<option value="10/31/17">31-Oct-17, Tuesday</option>
		<option value="11/1/17">01-Nov-17, Wednesday</option>
		<option value="11/2/17">02-Nov-17, Thursday</option>
		<option value="11/3/17">03-Nov-17, Friday</option>
		<option value="11/4/17">04-Nov-17, Saturday</option>
		<option value="11/5/17">05-Nov-17, Sunday</option>
		<option value="11/6/17">06-Nov-17, Monday</option>
		<option value="11/7/17">07-Nov-17, Tuesday</option>
		<option value="11/8/17">08-Nov-17, Wednesday</option>
		<option value="11/14/17">14-Nov-17, Tuesday</option>
		<option value="11/16/17">16-Nov-17, Thursday</option>
		<option value="11/17/17">17-Nov-17, Friday</option>
		<option value="11/18/17">18-Nov-17, Saturday</option>
		<option value="11/19/17">19-Nov-17, Sunday</option>
		<option value="11/20/17">20-Nov-17, Monday</option>
		<option value="11/21/17">21-Nov-17, Tuesday</option>
		<option value="11/22/17">22-Nov-17, Wednesday</option>
		<option value="11/30/17">30-Nov-17, Thursday</option>
		<option value="12/1/17">01-Dec-17, Friday</option>
		<option value="12/2/17">02-Dec-17, Saturday</option>
		<option value="12/3/17">03-Dec-17, Sunday</option>
		<option value="12/4/17">04-Dec-17, Monday</option>
		<option value="12/6/17">06-Dec-17, Wednesday</option>

	</select>
                </div>
                <div>
                    
                    <input type="submit" name="ctl00$cpContent$ShowtimesBox1$btnSearch" value="Search" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$cpContent$ShowtimesBox1$btnSearch&quot;, &quot;&quot;, true, &quot;search&quot;, &quot;&quot;, false, false))" id="ctl00_cpContent_ShowtimesBox1_btnSearch" class="btn-default" />
                </div>
            
</div>
    </div>
</div>
                <div class="main_section_halfpage">
					<div class="not-sticky" style="display:hide" />
					<!-- COMY_privacy_halfpg -->
					<div class="sticky" id='div-gpt-ad-1414141487831-0' style='width:300px; height:600px; padding-top: 3px;'>
					<script type='text/javascript'>
					googletag.display('div-gpt-ad-1414141487831-0');
					</script>
					</div>
				</div>
            </td>
        </tr>
    </table>

	<table style="width: 1065px">
		<tr>
			<td style="width: 365px;">
				<!-- COMY_footer_lrec1 -->
				<div class="sticky-footer" id='div-gpt-ad-1409216776650-0' style='width:336px; height:280px; margin: 0 auto;'>
				<script type='text/javascript'>
					googletag.display('div-gpt-ad-1409216776650-0');
				</script>
				</div>
			</td>
			<td style="width: 5px">
				&nbsp;
			</td>
			<td style="width: 365px;">
				<!-- COMY_footer_lrec2 -->
				<div id='div-gpt-ad-1409216776650-1' style='width:336px; height:280px; margin: 0 auto;'>
				<script type='text/javascript'>
					googletag.display('div-gpt-ad-1409216776650-1');
				</script>
				</div>
			</td>
			<td style="width: 5px">
				&nbsp;
			</td>
			<td style="width: 325px;" valign="top">
				<!-- /55577451/COMY_ros_footer_rec -->
				<div id='div-gpt-ad-1470016231071-4' style='height:250px; width:300px; margin: 0 auto;'>
				<script>
				googletag.display('div-gpt-ad-1470016231071-4');
				</script>
				</div>
			</td>
		</tr>
	</table>

        </div>
        <br /><br />               
        <div id="footer">                   
            <a href="/advertise/advertise.aspx">Advertise with Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/about_us.aspx">About Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/contact_us.aspx">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/faq.aspx">FAQ</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="http://www.thehive.asia/" target="_blank">TheHive.asia</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/privacy_policy.aspx">Privacy Policy</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/vacancy.aspx">Vacancy</a>
            <hr />
            <span>
                © 1999 - 2017
                All content copyright of Cinema Online and their respective owners<br />
                e-mail: <a href="mailto:marketing@cinemaonline.asia" target="_top">marketing@cinemaonline.asia</a> for inquiries. 
            </span>
        </div>        
    </div>    
    

<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>

    
			
	
		

</body>
</html>
