

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	cinema.com.my: Privacy Policy 
</title><meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <!-- FB Instant articles -->
    <meta property="fb:pages" content="186350986277" />

	<!-- Unique Data for each category -->
	<meta name="keywords" content="cinema, online, showtime, movie, film, filem, contest, now, showing, coming, soon, review, news, features, interviews, gallery, gsc, tgv, mbo, lfs, mall, cineplex, plex, plaza, trailers" /><meta name="description" content="Cinema Online Malaysia's Favourite Movie Site" /><link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
    <!-- Google DFP script start -->
	<script type='text/javascript'>
	    (function () {
	        var useSSL = 'https:' == document.location.protocol;
	        var src = (useSSL ? 'https:' : 'http:') +
	'//www.googletagservices.com/tag/js/gpt.js';
	        document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
	    })();
	</script>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
          
    <link rel="stylesheet" href="_plugins/menu2/css/menu_v2.css" type="text/css" />
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" /> 
	
	<script type="text/javascript" src="../_plugins/Colorbox/js/jquery.colorbox.js"></script>
    <link rel="stylesheet" href="_plugins/Colorbox/css/colorbox.css" type="text/css" />  
	
    <script type="text/javascript">


        function FavouriteBox(userid) {

            //$(function () {
            //    $.colorbox({ iframe: true, innerWidth: 795, scrolling: false, href: '../Usr_Ctrl/Members/favMovies.aspx?id=' + userid });
            //    return false;
            //});

            if (document.cookie.indexOf('visited=true') === -1) {
                var expires = new Date();
                expires.setTime(expires.getTime() + (120 * 60 * 1000));
                document.cookie = "visited=true; expires=" + expires.toUTCString();
                //$.colorbox({ height: '100%', scrolling: false, href: '../Usr_Ctrl/Members/favMovies.aspx?id=' + userid });
                $.colorbox({ iframe: true, innerWidth: 795, scrolling: false, href: '../Usr_Ctrl/Members/favMovies.aspx?id=' + userid });
                //ShowPopup("testing");
            };


        };

	</script>

	<!-- Po.st Lite code -->
	<script type="text/javascript">
		(function () {
			var s = document.createElement('script');
			s.type = 'text/javascript';
			s.async = true;
			s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
			  + '.po.st/static/v4/post-widget.js#publisherKey=fgiltlv0pvisqip084ec';
			var x = document.getElementsByTagName('script')[0];
			x.parentNode.insertBefore(s, x);
		 })();
	</script>
    
    <!-- Google DFP tags -->
	<script type='text/javascript'>
	googletag.defineSlot('/55577451/COMY_logo', [50, 60], 'div-gpt-ad-1412224214767-2').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_privacy_sb', [728, 90], 'div-gpt-ad-1408719234135-32').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_privacy_halfpg', [300, 600], 'div-gpt-ad-1414141487831-0').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_footer_lrec1', [336, 280], 'div-gpt-ad-1409216776650-0').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_footer_lrec2', [336, 280], 'div-gpt-ad-1409216776650-1').addService(googletag.pubads());
	googletag.defineSlot('/55577451/COMY_ros_footer_rec', [300, 250], 'div-gpt-ad-1470016231071-4').addService(googletag.pubads());
	googletag.pubads().enableSyncRendering();
	googletag.pubads().enableSingleRequest();
	googletag.enableServices();
	</script>	
	<!-- Google DFP script end --> 
	<link rel="stylesheet" href="../_plugins/stickyad/css/stickyadmovies.css" type="text/css" />       
	<script type="text/javascript" src="../_plugins/stickyad/js/jquery.stickyadmovies.js"></script>

	
	<!-- Begin comScore Tag -->
	<script>
	  var _comscore = _comscore || [];
	  _comscore.push({ c1: "2", c2: "14621247" });
	  (function() {
		var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
		el.parentNode.insertBefore(s, el);
	  })();
	</script>
	<noscript>
	  <img src="http://b.scorecardresearch.com/p?c1=2&c2=14621247&cv=2.0&cj=1" />
	</noscript>
	<!-- End comScore Tag -->	
	
	<!-- Google Analytics code -->
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-6252059-1', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');
	</script> 
	
	<!-- Innity Container Tag -->
		<script type="text/javascript" charset="UTF-8">(function(w,d,s,i,c){var f=d.createElement(s);f.type="text/javascript";f.async=true;f.src=("https:"==d.location.protocol? "https://ssl-avd.innity.net":"http://avd.innity.net")+"/"+i+"/container_"+c+".js";var g=d.getElementsByTagName(s)[0];g.parentNode.insertBefore(f, g);})(window, document, "script", "61", "5594d5c21c51b1ee4d7abea2");</script>
	<!-- End Innity Container Tag -->
<link href="App_Themes/Default_v2/Controls.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/dateStyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/ItemStyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/MasterStyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/Sections.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/StyleSheet.css" type="text/css" rel="stylesheet" /><link href="App_Themes/Default_v2/StyleSheet_v2.css" type="text/css" rel="stylesheet" /></head>
<body>
    <form name="aspnetForm" method="post" action="privacy_policy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwULLTEyODk5NTE0ODEPZBYCZg9kFgICAw9kFgQCCA9kFgICAQ9kFgICAQ9kFgJmD2QWCAIBDxAPFgYeDURhdGFUZXh0RmllbGQFCGFyZWFOYW1lHg5EYXRhVmFsdWVGaWVsZAUGYXJlYUlkHgtfIURhdGFCb3VuZGdkEBU7FlNlbGVjdCBBcmVhIC8gQWxsIEFyZWELQ2l0eSBDZW50ZXIMS3VhbGEgTHVtcHVyDk9sZCBLbGFuZyBSb2FkBkNoZXJhcw1EZXNhIFBldGFsaW5nBkFtcGFuZwtXYW5nc2EgTWFqdQ1QZXRhbGluZyBKYXlhBlN1bndheQZTdWJhbmcOS290YSBEYW1hbnNhcmEJU2hhaCBBbGFtB1B1Y2hvbmcFS2xhbmcGS2FqYW5nDlNlcmkgS2VtYmFuZ2FuCFNlbGF5YW5nBlJhd2FuZwZLZXBvbmcHU2V0YXBhawlQdXRyYWpheWEJQ3liZXJqYXlhDkt1YWxhIFNlbGFuZ29yCkdlb3JnZXRvd24OU2ViZXJhbmcgUGVyYWkLQmF5YW4gTGVwYXMLQnV0dGVyd29ydGgNU2ViZXJhbmcgSmF5YQ1TdW5nYWkgUGV0YW5pCkFsb3IgU2V0YXIITGFuZ2thd2kFSml0cmEFS3VsaW0ESXBvaAxQYXJpdCBCdW50YXIGS2FtcGFyDFNlcmkgTWFuanVuZw1TZXJpIElza2FuZGFyCFNpdGlhd2FuB1RhaXBpbmcLVGVsdWsgSW50YW4HTWFsYWNjYQhTZXJlbWJhbgVCYWhhdQdLdWFudGFuC0pvaG9yIEJhaHJ1BU1hc2FpCkJhdHUgUGFoYXQHU2VnYW1hdAZLbHVhbmcLS290YSBUaW5nZ2kKVGVyZW5nZ2FudQhNZW50YWthYgdLdWNoaW5nDUtvdGEgS2luYWJhbHUIU2FuZGFrYW4FVGF3YXUETWlyaRU7ATACNTECNTYCNTgCMTECNTcCNDcCNTUCNDICNDMCMzYCNDUCMzUCNDQCMzgCMzkCNDACNDgCNDYCNTMCNTQCNTkCODQCNDECMTYCMTkCMTcCMTgCMjABOQE2ATgBNwIxMAIyMQI2MwIyMgI2NAIyNAIyMwIyNQIyNwIxMgIxMwIxNAIxNQExAjkzATMBNAE1ATICNDkCNjECMzECMjgCMjkCMzACMzQUKwM7Z2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2cWAWZkAgMPEA8WBh8ABQdjaW5OYW1lHwEFBWNpbklkHwJnZBAVjgEbU2VsZWN0IENpbmVtYSAvIEFsbCBDaW5lbWFzD01CTyAtIFZpdmEgSG9tZRVNQk8gLSBHYWxheHkgQ2luZXBsZXgTTUJPIC0gU3ViYW5nIFBhcmFkZRBNQk8gLSBDaXR0YSBNYWxsE01CTyAtIFN0YXJsaW5nIE1hbGwTTUJPIC0gU3BhY2UgVTggTWFsbA9NQk8gLSBCcmVtIE1hbGwZTUJPIC0gS2Vwb25nIFZpbGxhZ2UgTWFsbBVNQk8gLSBTZXRhcGFrIENlbnRyYWwUTUJPIC0gU3BhcmsgQ2luZXBsZXgTTUJPIC0gSGFyYm91ciBQbGFjZRxNQk8gLSBLdWxpbSBMYW5kbWFyayBDZW50cmFsFU1CTyAtIFRhaXBpbmcgU2VudHJhbBRNQk8gLSBBbG9yIFN0YXIgTWFsbBRNQk8gLSBDZW50cmFsIFNxdWFyZRFNQk8gLSBUZWx1ayBJbnRhbhVNQk8gLSBCYXR1IFBhaGF0IE1hbGwRTUJPIC0gTWVsYWthIE1hbGwTTUJPIC0gRWxlbWVudHMgTWFsbBlNQk8gLSBUZXJtaW5hbCAyIFNlcmVtYmFuDE1CTyAtIFUgTWFsbA5NQk8gLSBLU0wgQ2l0eRBNQk8gLSBTcXVhcmUgT25lEU1CTyAtIEtsdWFuZyBNYWxsE01CTyAtIEhlcml0YWdlIE1hbGwRTUJPIC0gU3ByaW5nIE1hbGwZTUJPIC0gSW1hZ28gS290YSBLaW5hYmFsdRdHU0MgLSBTaWduYXR1cmUgR2FyZGVucxBHU0MgLSBNaWQgVmFsbGV5E0dTQyAtIE1lbGF3YXRpIE1hbGwRR1NDIC0gUGF2aWxpb24gS0wNR1NDIC0gMSBVdGFtYRRHU0MgLSBUcm9waWNhbmEgQ2l0eRNHU0MgLSBQYXJhZGlnbSBNYWxsFUdTQyAtIFNldGlhIENpdHkgTWFsbBJHU0MgLSBLbGFuZyBQYXJhZGUaR1NDIC0gQmVyamF5YSBUaW1lcyBTcXVhcmUVR1NDIC0gUXVpbGwgQ2l0eSBNYWxsEEdTQyAtIE51IFNlbnRyYWwYR1NDIC0gQWxhbWFuZGEgUHV0cmFqYXlhE0dTQyAtIElPSSBDaXR5IE1hbGwZR1NDIC0gQ2hlcmFzIExlaXN1cmUgTWFsbAxHU0MgLSBNeVRvd24QR1NDIC0gU3VtbWl0IFVTSg5HU0MgLSBJT0kgTWFsbBFHU0MgLSBJcG9oIFBhcmFkZRJHU0MgLSBBbWFuIENlbnRyYWwTR1NDIC0gQW1hbmpheWEgTWFsbBJHU0MgLSBHdXJuZXkgUGxhemEPR1NDIC0gUXVlZW5zYmF5FUdTQyAtIFN1bndheSBDYXJuaXZhbA9HU0MgLSBQYWxtIE1hbGwQR1NDIC0gVGVybWluYWwgMRtHU0MgLSBBRU9OIEJhbmRhcmF5YSBNZWxha2EWR1NDIC0gRGF0YXJhbiBQYWhsYXdhbhZHU0MgLSBCZXJqYXlhIE1lZ2FtYWxsFUdTQyAtIEVhc3QgQ29hc3QgTWFsbBhHU0MgLSBNZW50YWthYiBTdGFyIE1hbGwRR1NDIC0gU3VyaWEgU2FiYWgNR1NDIC0gMUJvcm5lbw1HU0MgLSBDaXR5T05FFkdTQyAtIEJpbnRhbmcgTWVnYW1hbGwXTEZTIC0gQ29saXNldW0gQ2luZXBsZXgWTEZTIC0gQ2FwaXRvbCBTZWxheWFuZxdMRlMgLSBTdGF0ZSBDaW5lcGxleCBQShhMRlMgLSBNZXRybyBQbGF6YSBLYWphbmcVTEZTIC0gU3JpIEludGFuIEtsYW5nFkxGUyAtIFNoYXcgQ2VudHJlcG9pbnQcTEZTIC0gMSBQbGF6YSBLdWFsYSBTZWxhbmdvcg5MRlMgLSBLTSBQbGF6YRhMRlMgLSBLaWFyYSBTcXVhcmUgQmFoYXUUTEZTIC0gTWFoa290YSBQYXJhZGUZTEZTIC0gQnVraXQgSmFtYnVsIFBlbmFuZxJMRlMgLSBQcmFuZ2luIE1hbGwRTEZTIC0gQnV0dGVyd29ydGgVTEZTIC0gU2VyaSBLaW50YSBJcG9oDkxGUyAtIFNpdGlhd2FuDExGUyAtIEthbXBhchlMRlMgLSBLZXJpYW4gU2VudHJhbCBNYWxsE0xGUyAtIFNlcmkgSXNrYW5kYXIbTEZTIC0gUGxhemEgVGFzZWsgU2t1ZGFpIEpCE0xGUyAtIFNrdWRhaSBQYXJhZGUOTEZTIC0gSU9JIE1hbGwPTEZTIC0gMSBTZWdhbWF0F0xGUyAtIFJpdmVyc2lkZSBLdWNoaW5nEUxGUyAtIFN1bW1lciBNYWxsG0xGUyAtIEhhcmJvdXIgTWFsbCBTYW5kYWthbhBMRlMgLSBQQiBTZW50cmFsEFRHViAtIFN1cmlhIEtMQ0MSVEdWIC0gU3Vud2F5IFB1dHJhC1RHViAtIE1pbmVzFFRHViAtIFN1bndheSBQeXJhbWlkDVRHViAtIDEgVXRhbWETVEdWIC0gRW5jb3JwIFN0cmFuZBpUR1YgLSBKYXlhIFNob3BwaW5nIENlbnRyZRJUR1YgLSBBRU9OIEtsZWJhbmcMVEdWIC0gS2Vwb25nFFRHViAtIENoZXJhcyBTZWxhdGFuFVRHViAtIFN1bndheSBWZWxvY2l0eRBUR1YgLSAxIFNoYW1lbGluFFRHViAtIENoZXJhcyBTZW50cmFsEVRHViAtIFdhbmdzYSBXYWxrDlRHViAtIEFFT04gQVUyFlRHViAtIERQdWx6ZSBDeWJlcmpheWEQVEdWIC0gU2V0aWEgV2FsawxUR1YgLSBSYXdhbmcQVEdWIC0gQnVraXQgUmFqYRJUR1YgLSBCdWtpdCBUaW5nZ2kgVEdWIC0gU2VyZW1iYW4gMiBTaG9wcGluZyBDZW50cmUQVEdWIC0gMXN0IEF2ZW51ZRlUR1YgLSBHdXJuZXkgUGFyYWdvbiBNYWxsGVRHViAtIEFFT04gQnVraXQgTWVydGFqYW0QVEdWIC0gU3RhdGlvbiAxOBBUR1YgLSBLaW50YSBDaXR5ElRHViAtIEFlb24gVGFpcGluZxdUR1YgLSBBRU9OIFNlcmkgTWFuanVuZxFUR1YgLSBUZWJyYXUgQ2l0eRFUR1YgLSBCdWtpdCBJbmRhaBRUR1YgLSBBRU9OIEt1bGFpamF5YRBUR1YgLSBNZXNyYSBNYWxsGFRHViAtIEltcGVyaWFsIENpdHkgTWFsbBdUR1YgLSBWaXZhY2l0eSBNZWdhbWFsbBtDaXR5IEMncGxleCAtIENpdHkgQ2luZXBsZXgeRWFzdGVybiBDJ3BsZXggLSBFYXN0ZXJuIFBsYXphHUdyYW5kIENpbmVwbGV4IC0gVmlsbGFnZSBNYWxsG0dyb3diYWxsIC0gR3Jvd2JhbGwgQ2luZW1heB5KaXRyYSBNYWxsIEMncGxleCAtIEppdHJhIE1hbGwRS1BJIC0gQnJvYWR3YXkgSkISS1BJIC0gUGVybGluZyBNYWxsJ01lZ2EgQydwbGV4IC0gTWVnYW1hbGwgUGluYW5nIChMZXZlbCA0KR1NZWdhIEMncGxleCAtIExhbmdrYXdpIFBhcmFkZRRNZWdhIEMncGxleCAtIEJlcnRhbSlNZWdhbG9uZyBDJ3BsZXggLSBNZWdhbG9uZyBNYWxsIFBlbmFtcGFuZxhtbUNpbmVwbGV4ZXMgLSBEYW1hbnNhcmEabW1DaW5lcGxleGVzIC0gQ2l0eSBTcXVhcmUdT25lIENpbmVtYXMgLSBTcGVjdHJ1bSBBbXBhbmcZUHJlbWl1bS1YIC0gT25lIENpdHkgTWFsbCJTdXBlcnN0YXIgLSBUb2RheSdzIE1hbGwgVWx1IFRpcmFtJFN1cGVyc3RhciAtIFRvZGF5J3MgTWFya2V0IFNlcmkgQWxhbRpUU1IgLSBJUkRLTCBNYWxsIFNoYWggQWxhbRZQYXJhZ29uIC0gVGFpcGluZyBNYWxsFY4BATADNTY4AzEyNAM1NzMDNjAwAzY5NwM1NzYDNjA5AzU1OAM1ODEDNTM4AzU1MgM2MTIDNjE0AzYwNwM2NzMDNjE1AzYwOAMxMjYDNjk0AzUxNgM1MzADNTY1AzUyMQM2MTADNTk1AzUzMQM2NjYCMjIBMQM3MDACMjECMTgDNTI5AzU5MAM1OTEDNjY5AjE3AzY1MgM2NjACMjMDNjYyATMDNjk5ATQBNwM2NjEDNjc2AzU5NgIxMgIxOQIyMAM2NDcBNgM1NzADNTAyAjEwAzUzMgM1ODUDNTQ0AzI2MwM2MTYDNjA1AzU5NwMxMTQDMTExAzE1NQMxMTADNTY5AzU1NwM1MzYDNTI4AzUzMwMxMTIDNTgyAzExMwM1MzcDNjk4AzYwNgM2NTkDNjM0AzEwMgM1MzkDNTc0AzU3NwMxMzEDNjQyAzY4MgM2OTUCNTEDNjc0AjUyAjUzAjU0AzY0NAM2NTcDNjg5AjU5AjYwAzY5MwM1NzUDNjQzAzU0MQM2NzEDNjcwAzU5OQM1ODkCNTUCNjICNTcDNTY2AzY0MQM2NTMDNTk0AjU2AzY3NQM2NTQCNTgDNTg4AzY1NgM1NTkDNjU1AzY4OAM2ODYDNTkyAzU0MAMxODADMTE3AzY4NAM2ODcDMTkwAzE5MQM2MzUDNTQzAjc2Ajc0AzYwMwM2MzMDNTQyAzY4MAM2OTADNjkyFCsDjgFnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnFgFmZAIFDxAPFgYfAAUHbW92TmFtZR8BBQVtb3ZJZB8CZ2QQFUIZU2VsZWN0IE1vdmllIC8gQWxsIE1vdmllcw5UaGUgRGFyayBUb3dlcgVCYWx1bhZTcGlkZXItTWFuOiBIb21lY29taW5nC0JhYnkgRHJpdmVyD1RoZSBFbW9qaSBNb3ZpZR5XYXIgRm9yIFRoZSBQbGFuZXQgT2YgVGhlIEFwZXMdVHJhbnNmb3JtZXJzOiBUaGUgTGFzdCBLbmlnaHQTQW5uYWJlbGxlOiBDcmVhdGlvbgdEdW5raXJrFlRoZSBIaXRtYW4ncyBCb2R5Z3VhcmQZQW1pdHl2aWxsZTogVGhlIEF3YWtlbmluZx5UaGUgTnV0IEpvYiAyOiBOdXR0eSBCeSBOYXR1cmUMVGhlIFNhbGVzbWFuBlJldm9sdBVUaGUgQmF0dGxlc2hpcCBJc2xhbmQKQmFkIEdlbml1cw9UaGUgQWR2ZW50dXJlcnMWRmFpcnkgVGFpbDogRHJhZ29uIENyeRRNb24gTW9uIE1vbiBNb25zdGVycw5Xb2xmIFdhcnJpb3IgMgtHaG9zdCBIb3VzZRtBbm5hYmVsbGU6IENyZWF0aW9uIChBVE1PUykhQW5uYWJlbGxlOiBDcmVhdGlvbiAoQVRNT1MgRC1CT1gpFlZlbGFpaWxsYSBQYXR0YWRoYXJpIDIWVGhlIERhcmsgVG93ZXIgKEFUTU9TKStWYWxlcmlhbiBBbmQgVGhlIENpdHkgT2YgQSBUaG91c2FuZCBQbGFuZXRzE0phYiBIYXJyeSBNZXQgU2VqYWwNVklQIDI6IExhbGthchRUaGUgRW1vamkgTW92aWUgKDNEKQlNdWJhcmFrYW4RQmFyZWlsbHkgS2kgQmFyZmkVVG9pbGV0OiBFayBQcmVtIEthdGhhMFRoZSBOdXQgSm9iIDI6IE51dHR5IEJ5IE5hdHVyZSAoRmFtaWx5IEZyaWVuZGx5KSlKYWlsYW5na3VuZzogRGF0YW5nIEdlbmRvbmcgUHVsYW5nIEJvcG9uZwxMb2dhbiAoSU1BWCkeQmVhdXR5IEFuZCBUaGUgQmVhc3QgKElNQVggM0QpDkR1bmtpcmsgKElNQVgpEE9uY2UgVXBvbiBBIFRpbWUoeFh4OiBUaGUgUmV0dXJuIE9mIFhhbmRlciBDYWdlIChJTUFYIDNEKRxLb25nOiBTa3VsbCBJc2xhbmQgKElNQVggM0QpHEZhc3QgQW5kIEZ1cmlvdXMgOCAoSU1BWCAzRCkWV29uZGVyIFdvbWFuIChJTUFYIDNEKQxWaWtyYW0gVmVkaGEbUG9kaHV2YWdhIEVuIE1hbmFzdSBUaGFuZ2FtCFRhcmFtYW5pG1BlbmN1cmkgSGF0aSBNci4gQ2luZGVyZWxsYQlPdmVyZHJpdmUNQXRvbWljIEJsb25kZSxUaGUgT3NpcmlzIENoaWxkOiBTY2llbmNlIEZpY3Rpb24gVm9sdW1lIE9uZQpUaGUgRG9sbCAyME1hcnZlbCdzIEd1YXJkaWFucyBPZiBUaGUgR2FsYXh5IFZvbC4yIChJTUFYIDNEKQ9NZWVzYXlhIE11cnVra3U1UGlyYXRlcyBPZiBUaGUgQ2FyaWJiZWFuOiBTYWxhemFyJ3MgUmV2ZW5nZSAoSU1BWCAzRCksQmF0bWFuIFYgU3VwZXJtYW46IERhd24gT2YgSnVzdGljZSAoSU1BWCAzRCktTWFydmVsJ3MgQ2FwdGFpbiBBbWVyaWNhOiBDaXZpbCBXYXIgKElNQVggM0QpK1BpcmF0ZXMgT2YgVGhlIENhcmliYmVhbjogU2FsYXphcidzIFJldmVuZ2UYU211cmZzOiBUaGUgTG9zdCBWaWxsYWdlEEFiYW5nIExvbmcgRmFkaWwmTWFydmVsJ3MgR3VhcmRpYW5zIE9mIFRoZSBHYWxheHkgVm9sLjIgS2luZyBBcnRodXI6IExlZ2VuZCBPZiBUaGUgU3dvcmQMV29uZGVyIFdvbWFuCkogUmV2b2x1c2kbQmFhaHViYWxpIDI6IFRoZSBDb25jbHVzaW9uLk1hcnZlbCdzIEd1YXJkaWFucyBPZiBUaGUgR2FsYXh5IFZvbC4yIChBVE1PUykONzcgSGVhcnRicmVha3MVQgEwCjY5NTYuMTI4MzQKOTQ1My4xODE3OAsxMDM3NS4yMDMwOQsxMDg0MC4yMTI5MwsxMTEzMi4yMTk3MQsxMDI5Mi4yMjA1MQsxMTI0My4yMjMwOAsxMTM2OC4yMjUzOAsxMTM3My4yMjU0NQsxMTY5Ni4yMzI4MwsxMTYwNC4yMzU0OAsxMjA5NS4yNDIxMQsxMjEwMy4yNDU3NQsxMjM3MS4yNDc4NwsxMjI3NC4yNDg0MAsxMjI0Mi4yNDkyNAsxMjQ0MS4yNDkzMgsxMjQwMC4yNDk3MAsxMjQwNy4yNDk3NwsxMjQ4Ni4yNTA0MwsxMjM4Mi4yNTIzMwsxMTM2OC4yNTMxMAsxMTM2OC4yNTMyNAsxMjYzMS4yNTMyNQo2OTU2LjI1Mjc5CzExMDU2LjIyNTM0CzEyNTAxLjI1MDg0CzEyNjUyLjI1MzU3CzExMTMyLjI1Mzc0CzEyNTc3LjI1MjM0CzEyNjA0LjI1MjgzCzEyNTk0LjI1MzU2CzEyMDk1LjI1MzQ4CzEyNjI1LjI1MzExCzEwMzM1LjI0MjI4CzEwNTUzLjI0MzIzCzExMzczLjI1MTk4CzEyMjY4LjI1MjgyCzExMzMzLjIzOTgxCzExMjYwLjI0MjU4CzExMDYzLjI0Mzc3CzExMzY5LjI0ODIyCzEyNDk1LjI1MTA3CzEyNjQyLjI1MzQ1CzEyNjYzLjI1Mzc4CzEyNTk5LjI1MjcyCjkyMjIuMTc2MTMLMTE5MTQuMjQzMjELMTI0MzguMjQ5MTYLMTIzOTQuMjQ4NDELMTAzNDIuMjQ1OTgLMTI1MTYuMjUxMjMKODk0Ni4yNDc5Ngo5NzcxLjIyMzQzCjk3MDIuMjI0NDkKODk0Ni4xNzAwNwo5NDg1LjE4MjMyCjk1NjcuMTg0NjELMTAzNDIuMjAyMDULMTA4NjYuMjE2MjYLMTEzNjkuMjI1MzkLMTEzNzAuMjI1NDALMTIyMzMuMjQ1MDELMTAzNDIuMjQ2MTYLMTIzMjQuMjQ3MDQUKwNCZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnFgFmZAIHDxAPFgYfAAULc2hvd0RhdGVTdHIfAQUIc2hvd0RhdGUfAmdkEBULETIwLUF1Zy0xNywgU3VuZGF5ETIxLUF1Zy0xNywgTW9uZGF5EjIyLUF1Zy0xNywgVHVlc2RheRQyMy1BdWctMTcsIFdlZG5lc2RheRMyNC1BdWctMTcsIFRodXJzZGF5ETI1LUF1Zy0xNywgRnJpZGF5EzI2LUF1Zy0xNywgU2F0dXJkYXkRMjctQXVnLTE3LCBTdW5kYXkRMjgtQXVnLTE3LCBNb25kYXkSMjktQXVnLTE3LCBUdWVzZGF5FDMwLUF1Zy0xNywgV2VkbmVzZGF5FQsHOC8yMC8xNwc4LzIxLzE3BzgvMjIvMTcHOC8yMy8xNwc4LzI0LzE3BzgvMjUvMTcHOC8yNi8xNwc4LzI3LzE3BzgvMjgvMTcHOC8yOS8xNwc4LzMwLzE3FCsDC2dnZ2dnZ2dnZ2dnFgFmZAIJD2QWAmYPDxYCHgRUZXh0BQU4NTMyMGRkZNQzIBR2GFDUUz6DQpVgAQMjitj8" />


<script src="/ScriptResource.axd?d=-6PURz02oQuKlkAEimPEMnGROvbMNBXbWvEdQK7KP6PDEGjR5A-haiEM53_6bA17UeaXZdQe2r_cBt3YHoyuBlWW6aR_iRA-MxvRlnn7UBR5dmSi8Hr71sZ0zKGH8CKAOyZ2t3tse3WMo3tozwDH6pWIE1284QsdKJ9pi1DlbIkmgZyf0&amp;t=ffffffffbcb9b94a" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(Sys) === 'undefined') throw new Error('ASP.NET Ajax client-side framework failed to load.');
//]]>
</script>

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2927320D" />
<input type="hidden" name="__PREVIOUSPAGE" id="__PREVIOUSPAGE" value="Co2a2GXMfL6cupotct6qS8fFQPmbNrkKTTnxQSjDjBY3gnETbDD0oeaGPNsleHosVxT7h6eA0wmdB18uAMcPOGScV2A1" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWoQICzcqjkw8C6tTc3AwCtIKN8ggC5JWelQMC76yt5gQCw4XP7wYC//iY1wwCkNKo6A4C55norQMCt+fPwgMCs9251gUCqN311QUCqN3B1QUCqN2Z1gUCrN311QUCqN3d1QUCr93d1QUCqN3F1QUCr93x1QUCr93N1QUCrt3B1QUCr93F1QUCrt3F1QUCr93J1QUCrt2Z1gUCrt2V1gUCr9351QUCr92Z1gUCr93B1QUCqN3N1QUCqN3J1QUCqN2V1gUCu93J1QUCr9311QUCrN3B1QUCrN2V1gUCrN3d1QUCrN2Z1gUCrd351QUCtN251gUCqd251gUCu9251gUCqt251gUCrN351QUCrd311QUCqd3N1QUCrd3x1QUCqd3J1QUCrd3J1QUCrd3N1QUCrd3F1QUCrd3d1QUCrN3x1QUCrN3N1QUCrN3J1QUCrN3F1QUCrN251gUCtN3N1QUCrt251gUCr9251gUCqN251gUCrd251gUCr92V1gUCqd311QUCrt311QUCrd2Z1gUCrd2V1gUCrt351QUCrt3J1QUCp6SXzg0C+q+7sAsCopjBrgUCwaL32QgCk+WwmgoC1sXH7gMC8O6ZmQkC0IaBhQUC+q+3sAsC99z27wQC+q+vsAsC7MuIxA4C7cv4xA4Cp5i9rwUC1sXj7gMCwqL32QgCnPejhA8C+6+jsAsC/O6FmQkCp5idrwUC8O6BmQkCkuW8mgoCm/e3hA8C99ye7wQCk+W0mgoCm/eDhA8C99yi7wQC8e6VmQkCuaTfzQ0CuKSXzg0ClOWwmgoCuaTbzQ0CuKS3zg0C34aJhQUCkuWUmgoC99z67wQC0IaZhQUCuKTzzQ0C7cuIxA4Ck+XImQoCuaTjzQ0C7cuMxA4CuqSXzg0C0IblhQUCu6SXzg0CvqSXzg0CiNyu7wQC8e6ZmQkC8O7hmQkCuKTfzQ0CuKS7zg0CuaTXzQ0C1sXz7gMCvaSXzg0CkuXMmQoC7Mv0xA4CuKTXzQ0C7MuAxA4Cm/f/hA8CppjJrgUCzqLz2QgC8e6BmQkCnPefhA8C1cXH7gMCopi9rwUC89ya7wQCh/ezhA8CnuW0mgoC34aZhQUC1cX37gMC8O6JmQkC+q+rsAsCwaLn2QgC6Mv4xA4C7MvUxA4CzaLf2QgC1cXv7gMC+6+HsAsC8e79mQkC0IaVhQUCp5jFrgUC6Mv0xA4C34aNhQUCppjVrgUC1cX/7gMC89yi7wQC7cuExA4C7cvUxA4CnPeDhA8CvKTbzQ0Cp5jVrgUCvKTfzQ0CvKTjzQ0CvKTnzQ0Cp5jJrgUC1sX37gMC0IbhhQUCvKS7zg0CvaTXzQ0CwqK/2ggCm/e7hA8CwqLr2QgC99ym7wQCiNyy7wQCk+XMmQoC34blhQUC34bhhQUCvKTrzQ0CvaTfzQ0CvKTzzQ0C8O6VmQkCiNym7wQCwqLv2QgCppidrwUCvKTvzQ0CnPe7hA8Cp5jNrgUCvKS3zg0C+q+DsAsC8e6RmQkC34aVhQUCnPezhA8C+6+DsAsC8e7dmQkC7MvYxA4CkuXAmQoCnuWQmgoC0cXn7gMCp5iZrwUC1sXD7gMCnuWUmgoC89z67wQCnPerhA8CwaLr2QgCvqTvzQ0CvqTnzQ0CwqLb2QgCwqLn2QgC7MuExA4Ck+WQmgoCk+WUmgoC7cvYxA4C24WFng8C7NKjkQkC67+E5w8Crc/Wyg4Ctamwhg8C35f/pQgCpZyOnA8C3vzq/A8C6+TEzgYCpf/8qAQCwenVsAgCmaWG3AcCrv2G4wcCxLKySgK3qJS+BwKtrfbiCQKn7rhdAv6AmJAGAsPXvO4PAqex7fkGArXq1bIKAszimZENAr29otkGAqmWp44OAsfm1MMBApmWtUICxqym4AECspjBqQcCzZzLzg4C2IWknAUCn7HDcAKH6PX7AgKArrWpBQKSoLriAwK/xb2nDgKuz46gCgLDy9S8DAKC+szeCgLtnvqIAwLSsOqdBALHlenoBwKA6KjlAwLj7+3OAgL5sfnuAQKg5qiJAgLX0Li0DQLy9LT0DQLJqoCUAgLLk9XlBQKlqfO2DwLKs+oMAoTNzuQJAvHjgoUIAun2odEHAr2X0+4OAqbc7sIGAsXZ2/wOAp6MkMYIAuD1sKcJAuKVl9gDAqH/8OYBAqPO4tkIAsuw1ugKAoecso4MApephJkMAoz+1KwFAs7h7IEJAs7hgM4IAs7hlI4IAs7hqM4HAs7hvI4HAs7h0M4GAs7h5I4GAs7h+M4FAs7hzIANAs7h4MAMAsPRxMIIAqXNhocLhl/7ivQ6GW8X5w36zwQ/cZekens=" />
    
    <div id="master" class="shadow">
        <!-- BEGIN EFFECTIVE MEASURE CODE -->
        <!-- COPYRIGHT EFFECTIVE MEASURE -->
        <script type="text/javascript">
            (function () {
                var em = document.createElement('script'); em.type = 'text/javascript';
                em.async = true;
                em.src = ('https:' == document.location.protocol ? 'https://my-ssl' :
                'http://my-cdn') + '.effectivemeasure.net/em.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(em, s);
            })(); 
        </script>
        <noscript>
            <img src="//my.effectivemeasure.net/em_image" alt="" style="position: absolute; left: -5px;" />
        </noscript>
        <!--END EFFECTIVE MEASURE CODE -->  
         
        <div id="header">
            <div style="width: 91%; display: table-cell; border:none; vertical-align: middle;">                                                                       
                <table class="tbl_standard">
                    <tr style="vertical-align: middle;">
                        <td>English|<a href="http://www.cinema.com.my/bm/default.aspx" target="_blank">B. Malaysia</a> &nbsp;&nbsp;&nbsp;Like Us!</td>
                        <td><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fcinemaonline&amp;width=47&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=20&amp;appId=583362251764516" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:47px; height:20px;" allowTransparency="true"></iframe></td>
                        <td><input name="ctl00$Search1$q" type="text" maxlength="512" id="ctl00_Search1_q" />&nbsp;
<input type="submit" name="ctl00$Search1$sa" value="Search" id="ctl00_Search1_sa" />                
<input type="hidden" name="ctl00$Search1$cx" id="ctl00_Search1_cx" value="013128572172917708998:wkt14jbgegg" />
<input type="hidden" name="ctl00$Search1$cof" id="ctl00_Search1_cof" value="FORID:11" /></td>
                    </tr>
                </table>
            </div>                                        
            


<script src="/members/js/login.js" type="text/javascript"></script>
<div id="loginContainer">
    
            <a href="#" id="loginButton"><span>Login</span><em></em></a>
            <table id="ctl00_ctrl_login1_lvLogin_Login1" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td>
                    <div id="loginBox">
                        <div id="loginForm">
                            <fieldset id="body">
                                <fieldset>
                                    <input name="ctl00$ctrl_login1$lvLogin$Login1$UserName" type="text" id="ctl00_ctrl_login1_lvLogin_Login1_UserName" placeholder="Username" />
                                    
                                </fieldset>
                                <fieldset>
                                    <input name="ctl00$ctrl_login1$lvLogin$Login1$Password" type="password" id="ctl00_ctrl_login1_lvLogin_Login1_Password" placeholder="Password" />
                                    
                                </fieldset>
                                <input type="submit" name="ctl00$ctrl_login1$lvLogin$Login1$btnSubmit" value="Log In" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$btnSubmit&quot;, &quot;&quot;, true, &quot;login&quot;, &quot;&quot;, false, false))" id="ctl00_ctrl_login1_lvLogin_Login1_btnSubmit" />
                                                              
                            </fieldset>
                            <span>
                                <a id="ctl00_ctrl_login1_lvLogin_Login1_lbtnRegister" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$lbtnRegister&quot;, &quot;&quot;, false, &quot;&quot;, &quot;members/registration.aspx&quot;, false, true))">Register</a>&nbsp;|&nbsp;<a id="ctl00_ctrl_login1_lvLogin_Login1_lbtnForgotPassword" href="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$ctrl_login1$lvLogin$Login1$lbtnForgotPassword&quot;, &quot;&quot;, false, &quot;&quot;, &quot;members/forgot_password.aspx&quot;, false, true))">Forgot your password?</a>
                            </span>
                        </div>
                    </div>
                </td>
	</tr>
</table>            
        
</div>

            
        </div>
                
        <div id="logo">
            <table style="width: 1065px;">
                <tr>
                    <td style="width: 280px">
                        <table style="width:100%">
                            <tr>
                                <td>
									
    <!-- COMY_logo -->
	<div id='div-gpt-ad-1412224214767-2' style='width: 50px; height: 60px;'>
		<script type='text/javascript'>
			googletag.display('div-gpt-ad-1412224214767-2');
		</script>
	</div>

                                </td>
                                <td>
                                    <!-- CINEMA ONLINE text -->
                                    <a href="http://www.cinema.com.my/default.aspx" style="text-decoration: none">
                                        <font style="letter-spacing: -1px; font-family: Arial; font-size: 35px; font-weight: bold; color: #2F3193; text-decoration: none;
                                        text-transform: none;">Cinema</font><font style="letter-spacing: -0.5px; font-family: 'Times New Roman'; font-size: 36px; font-weight: bolder; color: #AA1C20; text-decoration: none; text-transform: none;"> Online</font>
                                        <br />
                                    </a><font style="font-family: Arial;font-size:11px;color:#000000">&nbsp;&nbsp;<i>Malaysia's Favourite Movie Site</i></font>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 785px;" align="right">
                        
    <!-- COMY_privacy_sb -->
	<div id='div-gpt-ad-1408719234135-32' style='width:728px; height:90px;'>
		<script type='text/javascript'>
			googletag.cmd.push(function() { googletag.display('div-gpt-ad-1408719234135-32'); });
		</script>
	</div>

                    </td>
                </tr>
            </table>
        </div>

        <div id="nav_menu_wrap">
            
            <nav >
                <ul>
                    <li><a href="/default.aspx">Home</a></li>
                    <li><a href="/movies/nowshowing.aspx">Now Showing</a></li>
                    <li><a href="/movies/comingsoon.aspx">Coming Soon</a></li>
                    <li><a href="/articles/news.aspx">News</a></li>
                    <li><a href="/articles/features.aspx">Features</a></li>
                    <li><a href="/trailers/trailers.aspx">Trailers</a></li>
                    <li><a href="/contests/contests.aspx">Contests</a></li>
                    <li><a href="/moviefreak/moviefreak.aspx">Movie Freak</a></li>
                    <li><a href="/filmfestivals/default.aspx">Film Festivals</a></li>
                    <li><a href="/cinemas/cinemalist.aspx">Showtimes</a> </li>
                </ul>
            </nav>
        </div>
        
        <div id="content">
            
			
            
    <table style="width: 1065px">
        <tr>
            <td style="width: 735px; vertical-align: top;">
                <div class="main_section">
                    <div class="section_header">
                        Privacy Policy
                    </div>
                    <div class="section_content">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td>
                                    <br />
                                    <p>
                                        This Privacy Policy governs the manner in which Cinema Online collects, uses, maintains
                                        and discloses information collected from users (each, a "User") of the <a href="http://www.cinema.com.my">
                                            cinema.com.my</a> website ("Site"). This privacy policy applies to the Site
                                        and all products and services offered by Cinema Online.</p>
                                    <span class="red-title">Personal identification information</span>                                        
                                    <p>
                                        We may collect personal identification information from Users in a variety of ways,
                                        including, but not limited to, when Users visit our site, register on the site,
                                        subscribe to the newsletter, respond to a survey, fill out a form, and in connection
                                        with other activities, services, features or resources we make available on our
                                        Site. Users may be asked for, as appropriate, name, email address, mailing address,
                                        phone number, and NRIC. Users may, however, visit our Site anonymously. We will
                                        collect personal identification information from Users only if they voluntarily
                                        submit such information to us. Users can always refuse to supply personally identification
                                        information, except that it may prevent them from engaging in certain Site related
                                        activities.</p>
                                    <span class="red-title">Non-personal identification information</span>                                        
                                    <p>
                                        We may collect non-personal identification information about Users whenever they
                                        interact with our Site. Non-personal identification information may include the
                                        browser name, the type of computer and technical information about Users means of
                                        connection to our Site, such as the operating system and the Internet service providers
                                        utilized and other similar information.</p>
                                    <span class="red-title">Web browser cookies</span>                                        
                                    <p>
                                        Our Site may use "cookies" (such as Google Analytics cookie and DoubleClick cookie)
                                        to enhance User experience. User's web browser places cookies on their hard drive
                                        for record-keeping purposes and sometimes to track information about them. User
                                        may choose to set their web browser to refuse cookies, or to alert you when cookies
                                        are being sent. If they do so, note that some parts of the Site may not function
                                        properly.</p>
                                    <span class="red-title">How we use collected information</span>                                        
                                    <p>
                                        Cinema Online may collect and use Users personal information for the following purposes:<br>
                                        <ul>
                                            <li><i>To improve customer service</i><br />
                                                Information you provide helps us respond to your customer service requests and support
                                                needs more efficiently.</li>
                                            <li><i>To personalize user experience</i><br />
                                                We may use information in the aggregate to understand how our Users as a group use
                                                the services and resources provided on our Site.</li>
                                            <li><i>To improve our Site</i><br />
                                                We may use feedback you provide to improve our products and services.</li>
                                            <li><i>To run a promotion, contest, survey or other Site feature</i><br />
                                                To send Users information they agreed to receive about topics we think will be of
                                                interest to them.</li>
                                            <li><i>To send periodic emails</i><br />
                                                We may use the email address to respond to their inquiries, questions, and/or other
                                                requests. If User decides to opt-in to our mailing list, they will receive emails
                                                that may include company news, updates, or related service information, etc. If
                                                at any time the User would like to unsubscribe from receiving future emails, we
                                                include detailed unsubscribe instructions at the bottom of each email.</li>
                                        </ul>
                                    </p>
                                    <span class="red-title">How we protect your information</span>                                        
                                    <p>
                                        We adopt appropriate data collection, storage and processing practices and security
                                        measures to protect against unauthorized access, alteration, disclosure or destruction
                                        of your personal information, username, password, and data stored on our Site.</p>
                                    <span class="red-title">Sharing your personal information</span>                                        
                                    <p>
                                        We do not sell, trade, or rent Users personal identification information to others.
                                        We may share generic aggregated demographic information not linked to any personal
                                        identification information regarding visitors and users with our business partners,
                                        trusted affiliates and advertisers for the purposes outlined above. We may use third
                                        party service providers to help us operate our business and the Site or administer
                                        activities on our behalf, such as sending out newsletters or surveys. We may share
                                        your information with these third parties for those limited purposes provided that
                                        you have given us your permission.</p>
                                    <span class="red-title">Third party websites</span>                                        
                                    <p>
                                        Users may find advertising or other content on our Site that link to the sites and
                                        services of our partners, suppliers, advertisers, sponsors, licensors and other
                                        third parties. We do not control the content or links that appear on these sites
                                        and are not responsible for the practices employed by websites linked to or from
                                        our Site. In addition, these sites or services, including their content and links,
                                        may be constantly changing. These sites and services may have their own privacy
                                        policies and customer service policies. Browsing and interaction on any other website,
                                        including websites which have a link to our Site, is subject to that website's own
                                        terms and policies.</p>
                                    <span class="red-title">Advertising</span>                                        
                                    <p>
                                        Ads appearing on our site may be delivered to Users by advertising partners, who
                                        may set cookies. These cookies allow the ad server to recognize your computer each
                                        time they send you an online advertisement to compile non personal identification
                                        information about you or others who use your computer. This information allows ad
                                        networks to, among other things, deliver targeted advertisements that they believe
                                        will be of most interest to you. This privacy policy does not cover the use of cookies
                                        by any advertisers.</p>
                                    <span class="red-title">Google Adsense</span>                                        
                                    <p>
                                        Some of the ads may be served by Google. Google's use of the DART cookie enables
                                        it to serve ads to Users based on their visit to our Site and other sites on the
                                        Internet. DART uses "non personally identifiable information" and does NOT track
                                        personal information about you, such as your name, email address, physical address,
                                        etc. You may opt out of the use of the DART cookie by visiting the Google <a href="http://www.google.com/privacy_ads.html" target="_blank">
                                            ad and content network privacy policy</a>.</p>
                                    <span class="red-title">Google Analytics</span>                                        
                                    <p>
                                        Some features implemented are based on Display Advertising for Google Analytics
                                        Demographics and Interest Reporting. Using the <a href="http://www.google.com/settings/ads"
                                            target="_blank">Ads Settings</a>, Users can opt-out of Google Analytics for
                                        Display Advertising and customize Google Display Network ads.<br>
                                        <br>
                                        Users may also check out Google Analytics' <a href="http://tools.google.com/dlpage/gaoptout/"
                                            target="_blank">currently available opt-outs</a> for the web.<br>
                                        <br>
                                        The data gathered from Google's Personalized advertising (formerly known as interest-based advertising) or 3rd-party audience
                                        data (such as age, gender, and interests) with Google Analytics shall be used to
                                        understand our Users better in order to improve our products and services.</p>
                                    <span class="red-title">Changes to this privacy policy</span>                                        
                                    <p>
                                        Cinema Online has the discretion to update this privacy policy at any time. When
                                        we do, we will revise the updated date at the bottom of this page. We encourage
                                        Users to frequently check this page for any changes to stay informed about how we
                                        are helping to protect the personal information we collect. You acknowledge and
                                        agree that it is your responsibility to review this privacy policy periodically
                                        and become aware of modifications.</p>
                                    <span class="red-title">Your acceptance of these terms</span>                                        
                                    <p>
                                        By using this Site, you signify your acceptance of this policy. If you do not agree
                                        to this policy, you may write in to request for a deletion or edit of your personal
                                        information. Your continued use of the Site following the posting of changes to
                                        this policy will be deemed your acceptance of those changes.</p>
                                    <span class="red-title">E.T. phone number</span>                                        
                                    <p>
                                        If you have any questions about this Privacy Policy, the practices of this site,
                                        or your dealings with this site, please contact us at:<br />
                                        <br />
                                        Cinema Online Sdn. Bhd.<br />
                                        No: 7A, Jalan SS4D/2,<br />
                                        People’s Park,<br />
                                        47301 Petaling Jaya,<br />
                                        Selangor, Malaysia.<br />
                                        <br>
                                        Tel: +603 7805 1527 (Office hours only, as stated below)<br>
                                        Email: marketing@cinemaonline.asia<br />
                                        <br />
                                        Office Hours: Monday – Friday (closed on public holidays), 9:30am - 6:00pm<br />
                                        <br />
                                        This document was last updated on 7 April  2017<br />
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
             <td style="width: 5px">
                &nbsp;
            </td>
            <td style="width: 325px; vertical-align: top;">
                
<style>
    .styled-select
    {
        width: 100%;
        height: 34px;
        overflow: hidden;
        border: 1px solid #ccc;
        margin-top: 5px;
        margin-bottom: 5px;
    }
    
    .ddlframe
    {
        background-color: #a6a6a6;
        padding: 5px 5px 5px 5px;
        margin-bottom: 5px;
    }
    
    .submit
    {
        border: 1px solid #563d7c;
        border-radius: 5px;
        color: white;
        padding: 5px 10px 5px 25px;
        background: url(https://i.stack.imgur.com/jDCI4.png) left 3px top 5px no-repeat #563d7c;
    }
    
    
</style>

<div class="main_section sidebar_bg2">
    <div class="section_header sidebar_header">
        Showtimes
    </div>
    <div>
        <div id="ctl00_cpContent_ShowtimesBox1_ajxUpdatePanel">
	
                <div class="ddlframe">
                    <select name="ctl00$cpContent$ShowtimesBox1$ddlArea" id="ctl00_cpContent_ShowtimesBox1_ddlArea" class="styled-select">
		<option selected="selected" value="0">Select Area / All Area</option>
		<option value="51">City Center</option>
		<option value="56">Kuala Lumpur</option>
		<option value="58">Old Klang Road</option>
		<option value="11">Cheras</option>
		<option value="57">Desa Petaling</option>
		<option value="47">Ampang</option>
		<option value="55">Wangsa Maju</option>
		<option value="42">Petaling Jaya</option>
		<option value="43">Sunway</option>
		<option value="36">Subang</option>
		<option value="45">Kota Damansara</option>
		<option value="35">Shah Alam</option>
		<option value="44">Puchong</option>
		<option value="38">Klang</option>
		<option value="39">Kajang</option>
		<option value="40">Seri Kembangan</option>
		<option value="48">Selayang</option>
		<option value="46">Rawang</option>
		<option value="53">Kepong</option>
		<option value="54">Setapak</option>
		<option value="59">Putrajaya</option>
		<option value="84">Cyberjaya</option>
		<option value="41">Kuala Selangor</option>
		<option value="16">Georgetown</option>
		<option value="19">Seberang Perai</option>
		<option value="17">Bayan Lepas</option>
		<option value="18">Butterworth</option>
		<option value="20">Seberang Jaya</option>
		<option value="9">Sungai Petani</option>
		<option value="6">Alor Setar</option>
		<option value="8">Langkawi</option>
		<option value="7">Jitra</option>
		<option value="10">Kulim</option>
		<option value="21">Ipoh</option>
		<option value="63">Parit Buntar</option>
		<option value="22">Kampar</option>
		<option value="64">Seri Manjung</option>
		<option value="24">Seri Iskandar</option>
		<option value="23">Sitiawan</option>
		<option value="25">Taiping</option>
		<option value="27">Teluk Intan</option>
		<option value="12">Malacca</option>
		<option value="13">Seremban</option>
		<option value="14">Bahau</option>
		<option value="15">Kuantan</option>
		<option value="1">Johor Bahru</option>
		<option value="93">Masai</option>
		<option value="3">Batu Pahat</option>
		<option value="4">Segamat</option>
		<option value="5">Kluang</option>
		<option value="2">Kota Tinggi</option>
		<option value="49">Terengganu</option>
		<option value="61">Mentakab</option>
		<option value="31">Kuching</option>
		<option value="28">Kota Kinabalu</option>
		<option value="29">Sandakan</option>
		<option value="30">Tawau</option>
		<option value="34">Miri</option>

	</select>
                </div>
                <div class="ddlframe">
                    <select name="ctl00$cpContent$ShowtimesBox1$ddlCinemas" id="ctl00_cpContent_ShowtimesBox1_ddlCinemas" class="styled-select">
		<option selected="selected" value="0">Select Cinema / All Cinemas</option>
		<option value="568">MBO - Viva Home</option>
		<option value="124">MBO - Galaxy Cineplex</option>
		<option value="573">MBO - Subang Parade</option>
		<option value="600">MBO - Citta Mall</option>
		<option value="697">MBO - Starling Mall</option>
		<option value="576">MBO - Space U8 Mall</option>
		<option value="609">MBO - Brem Mall</option>
		<option value="558">MBO - Kepong Village Mall</option>
		<option value="581">MBO - Setapak Central</option>
		<option value="538">MBO - Spark Cineplex</option>
		<option value="552">MBO - Harbour Place</option>
		<option value="612">MBO - Kulim Landmark Central</option>
		<option value="614">MBO - Taiping Sentral</option>
		<option value="607">MBO - Alor Star Mall</option>
		<option value="673">MBO - Central Square</option>
		<option value="615">MBO - Teluk Intan</option>
		<option value="608">MBO - Batu Pahat Mall</option>
		<option value="126">MBO - Melaka Mall</option>
		<option value="694">MBO - Elements Mall</option>
		<option value="516">MBO - Terminal 2 Seremban</option>
		<option value="530">MBO - U Mall</option>
		<option value="565">MBO - KSL City</option>
		<option value="521">MBO - Square One</option>
		<option value="610">MBO - Kluang Mall</option>
		<option value="595">MBO - Heritage Mall</option>
		<option value="531">MBO - Spring Mall</option>
		<option value="666">MBO - Imago Kota Kinabalu</option>
		<option value="22">GSC - Signature Gardens</option>
		<option value="1">GSC - Mid Valley</option>
		<option value="700">GSC - Melawati Mall</option>
		<option value="21">GSC - Pavilion KL</option>
		<option value="18">GSC - 1 Utama</option>
		<option value="529">GSC - Tropicana City</option>
		<option value="590">GSC - Paradigm Mall</option>
		<option value="591">GSC - Setia City Mall</option>
		<option value="669">GSC - Klang Parade</option>
		<option value="17">GSC - Berjaya Times Square</option>
		<option value="652">GSC - Quill City Mall</option>
		<option value="660">GSC - Nu Sentral</option>
		<option value="23">GSC - Alamanda Putrajaya</option>
		<option value="662">GSC - IOI City Mall</option>
		<option value="3">GSC - Cheras Leisure Mall</option>
		<option value="699">GSC - MyTown</option>
		<option value="4">GSC - Summit USJ</option>
		<option value="7">GSC - IOI Mall</option>
		<option value="661">GSC - Ipoh Parade</option>
		<option value="676">GSC - Aman Central</option>
		<option value="596">GSC - Amanjaya Mall</option>
		<option value="12">GSC - Gurney Plaza</option>
		<option value="19">GSC - Queensbay</option>
		<option value="20">GSC - Sunway Carnival</option>
		<option value="647">GSC - Palm Mall</option>
		<option value="6">GSC - Terminal 1</option>
		<option value="570">GSC - AEON Bandaraya Melaka</option>
		<option value="502">GSC - Dataran Pahlawan</option>
		<option value="10">GSC - Berjaya Megamall</option>
		<option value="532">GSC - East Coast Mall</option>
		<option value="585">GSC - Mentakab Star Mall</option>
		<option value="544">GSC - Suria Sabah</option>
		<option value="263">GSC - 1Borneo</option>
		<option value="616">GSC - CityONE</option>
		<option value="605">GSC - Bintang Megamall</option>
		<option value="597">LFS - Coliseum Cineplex</option>
		<option value="114">LFS - Capitol Selayang</option>
		<option value="111">LFS - State Cineplex PJ</option>
		<option value="155">LFS - Metro Plaza Kajang</option>
		<option value="110">LFS - Sri Intan Klang</option>
		<option value="569">LFS - Shaw Centrepoint</option>
		<option value="557">LFS - 1 Plaza Kuala Selangor</option>
		<option value="536">LFS - KM Plaza</option>
		<option value="528">LFS - Kiara Square Bahau</option>
		<option value="533">LFS - Mahkota Parade</option>
		<option value="112">LFS - Bukit Jambul Penang</option>
		<option value="582">LFS - Prangin Mall</option>
		<option value="113">LFS - Butterworth</option>
		<option value="537">LFS - Seri Kinta Ipoh</option>
		<option value="698">LFS - Sitiawan</option>
		<option value="606">LFS - Kampar</option>
		<option value="659">LFS - Kerian Sentral Mall</option>
		<option value="634">LFS - Seri Iskandar</option>
		<option value="102">LFS - Plaza Tasek Skudai JB</option>
		<option value="539">LFS - Skudai Parade</option>
		<option value="574">LFS - IOI Mall</option>
		<option value="577">LFS - 1 Segamat</option>
		<option value="131">LFS - Riverside Kuching</option>
		<option value="642">LFS - Summer Mall</option>
		<option value="682">LFS - Harbour Mall Sandakan</option>
		<option value="695">LFS - PB Sentral</option>
		<option value="51">TGV - Suria KLCC</option>
		<option value="674">TGV - Sunway Putra</option>
		<option value="52">TGV - Mines</option>
		<option value="53">TGV - Sunway Pyramid</option>
		<option value="54">TGV - 1 Utama</option>
		<option value="644">TGV - Encorp Strand</option>
		<option value="657">TGV - Jaya Shopping Centre</option>
		<option value="689">TGV - AEON Klebang</option>
		<option value="59">TGV - Kepong</option>
		<option value="60">TGV - Cheras Selatan</option>
		<option value="693">TGV - Sunway Velocity</option>
		<option value="575">TGV - 1 Shamelin</option>
		<option value="643">TGV - Cheras Sentral</option>
		<option value="541">TGV - Wangsa Walk</option>
		<option value="671">TGV - AEON AU2</option>
		<option value="670">TGV - DPulze Cyberjaya</option>
		<option value="599">TGV - Setia Walk</option>
		<option value="589">TGV - Rawang</option>
		<option value="55">TGV - Bukit Raja</option>
		<option value="62">TGV - Bukit Tinggi</option>
		<option value="57">TGV - Seremban 2 Shopping Centre</option>
		<option value="566">TGV - 1st Avenue</option>
		<option value="641">TGV - Gurney Paragon Mall</option>
		<option value="653">TGV - AEON Bukit Mertajam</option>
		<option value="594">TGV - Station 18</option>
		<option value="56">TGV - Kinta City</option>
		<option value="675">TGV - Aeon Taiping</option>
		<option value="654">TGV - AEON Seri Manjung</option>
		<option value="58">TGV - Tebrau City</option>
		<option value="588">TGV - Bukit Indah</option>
		<option value="656">TGV - AEON Kulaijaya</option>
		<option value="559">TGV - Mesra Mall</option>
		<option value="655">TGV - Imperial City Mall</option>
		<option value="688">TGV - Vivacity Megamall</option>
		<option value="686">City C'plex - City Cineplex</option>
		<option value="592">Eastern C'plex - Eastern Plaza</option>
		<option value="540">Grand Cineplex - Village Mall</option>
		<option value="180">Growball - Growball Cinemax</option>
		<option value="117">Jitra Mall C'plex - Jitra Mall</option>
		<option value="684">KPI - Broadway JB</option>
		<option value="687">KPI - Perling Mall</option>
		<option value="190">Mega C'plex - Megamall Pinang (Level 4)</option>
		<option value="191">Mega C'plex - Langkawi Parade</option>
		<option value="635">Mega C'plex - Bertam</option>
		<option value="543">Megalong C'plex - Megalong Mall Penampang</option>
		<option value="76">mmCineplexes - Damansara</option>
		<option value="74">mmCineplexes - City Square</option>
		<option value="603">One Cinemas - Spectrum Ampang</option>
		<option value="633">Premium-X - One City Mall</option>
		<option value="542">Superstar - Today's Mall Ulu Tiram</option>
		<option value="680">Superstar - Today's Market Seri Alam</option>
		<option value="690">TSR - IRDKL Mall Shah Alam</option>
		<option value="692">Paragon - Taiping Mall</option>

	</select>
                </div>
                <div class="ddlframe">
                    <select name="ctl00$cpContent$ShowtimesBox1$ddlMovies" id="ctl00_cpContent_ShowtimesBox1_ddlMovies" class="styled-select">
		<option selected="selected" value="0">Select Movie / All Movies</option>
		<option value="6956.12834">The Dark Tower</option>
		<option value="9453.18178">Balun</option>
		<option value="10375.20309">Spider-Man: Homecoming</option>
		<option value="10840.21293">Baby Driver</option>
		<option value="11132.21971">The Emoji Movie</option>
		<option value="10292.22051">War For The Planet Of The Apes</option>
		<option value="11243.22308">Transformers: The Last Knight</option>
		<option value="11368.22538">Annabelle: Creation</option>
		<option value="11373.22545">Dunkirk</option>
		<option value="11696.23283">The Hitman's Bodyguard</option>
		<option value="11604.23548">Amityville: The Awakening</option>
		<option value="12095.24211">The Nut Job 2: Nutty By Nature</option>
		<option value="12103.24575">The Salesman</option>
		<option value="12371.24787">Revolt</option>
		<option value="12274.24840">The Battleship Island</option>
		<option value="12242.24924">Bad Genius</option>
		<option value="12441.24932">The Adventurers</option>
		<option value="12400.24970">Fairy Tail: Dragon Cry</option>
		<option value="12407.24977">Mon Mon Mon Monsters</option>
		<option value="12486.25043">Wolf Warrior 2</option>
		<option value="12382.25233">Ghost House</option>
		<option value="11368.25310">Annabelle: Creation (ATMOS)</option>
		<option value="11368.25324">Annabelle: Creation (ATMOS D-BOX)</option>
		<option value="12631.25325">Velaiilla Pattadhari 2</option>
		<option value="6956.25279">The Dark Tower (ATMOS)</option>
		<option value="11056.22534">Valerian And The City Of A Thousand Planets</option>
		<option value="12501.25084">Jab Harry Met Sejal</option>
		<option value="12652.25357">VIP 2: Lalkar</option>
		<option value="11132.25374">The Emoji Movie (3D)</option>
		<option value="12577.25234">Mubarakan</option>
		<option value="12604.25283">Bareilly Ki Barfi</option>
		<option value="12594.25356">Toilet: Ek Prem Katha</option>
		<option value="12095.25348">The Nut Job 2: Nutty By Nature (Family Friendly)</option>
		<option value="12625.25311">Jailangkung: Datang Gendong Pulang Bopong</option>
		<option value="10335.24228">Logan (IMAX)</option>
		<option value="10553.24323">Beauty And The Beast (IMAX 3D)</option>
		<option value="11373.25198">Dunkirk (IMAX)</option>
		<option value="12268.25282">Once Upon A Time</option>
		<option value="11333.23981">xXx: The Return Of Xander Cage (IMAX 3D)</option>
		<option value="11260.24258">Kong: Skull Island (IMAX 3D)</option>
		<option value="11063.24377">Fast And Furious 8 (IMAX 3D)</option>
		<option value="11369.24822">Wonder Woman (IMAX 3D)</option>
		<option value="12495.25107">Vikram Vedha</option>
		<option value="12642.25345">Podhuvaga En Manasu Thangam</option>
		<option value="12663.25378">Taramani</option>
		<option value="12599.25272">Pencuri Hati Mr. Cinderella</option>
		<option value="9222.17613">Overdrive</option>
		<option value="11914.24321">Atomic Blonde</option>
		<option value="12438.24916">The Osiris Child: Science Fiction Volume One</option>
		<option value="12394.24841">The Doll 2</option>
		<option value="10342.24598">Marvel's Guardians Of The Galaxy Vol.2 (IMAX 3D)</option>
		<option value="12516.25123">Meesaya Murukku</option>
		<option value="8946.24796">Pirates Of The Caribbean: Salazar's Revenge (IMAX 3D)</option>
		<option value="9771.22343">Batman V Superman: Dawn Of Justice (IMAX 3D)</option>
		<option value="9702.22449">Marvel's Captain America: Civil War (IMAX 3D)</option>
		<option value="8946.17007">Pirates Of The Caribbean: Salazar's Revenge</option>
		<option value="9485.18232">Smurfs: The Lost Village</option>
		<option value="9567.18461">Abang Long Fadil</option>
		<option value="10342.20205">Marvel's Guardians Of The Galaxy Vol.2</option>
		<option value="10866.21626">King Arthur: Legend Of The Sword</option>
		<option value="11369.22539">Wonder Woman</option>
		<option value="11370.22540">J Revolusi</option>
		<option value="12233.24501">Baahubali 2: The Conclusion</option>
		<option value="10342.24616">Marvel's Guardians Of The Galaxy Vol.2 (ATMOS)</option>
		<option value="12324.24704">77 Heartbreaks</option>

	</select>
                </div>
                <div class="ddlframe">
                    <select name="ctl00$cpContent$ShowtimesBox1$ddlDates" id="ctl00_cpContent_ShowtimesBox1_ddlDates" class="styled-select">
		<option selected="selected" value="8/20/17">20-Aug-17, Sunday</option>
		<option value="8/21/17">21-Aug-17, Monday</option>
		<option value="8/22/17">22-Aug-17, Tuesday</option>
		<option value="8/23/17">23-Aug-17, Wednesday</option>
		<option value="8/24/17">24-Aug-17, Thursday</option>
		<option value="8/25/17">25-Aug-17, Friday</option>
		<option value="8/26/17">26-Aug-17, Saturday</option>
		<option value="8/27/17">27-Aug-17, Sunday</option>
		<option value="8/28/17">28-Aug-17, Monday</option>
		<option value="8/29/17">29-Aug-17, Tuesday</option>
		<option value="8/30/17">30-Aug-17, Wednesday</option>

	</select>
                </div>
                <div>
                    
                    <input type="submit" name="ctl00$cpContent$ShowtimesBox1$btnSearch" value="Search" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;ctl00$cpContent$ShowtimesBox1$btnSearch&quot;, &quot;&quot;, true, &quot;search&quot;, &quot;&quot;, false, false))" id="ctl00_cpContent_ShowtimesBox1_btnSearch" class="btn-default" />
                </div>
            
</div>
    </div>
</div>
                <div class="main_section_halfpage">
					<div class="not-sticky" style="display:hide" />
					<!-- COMY_privacy_halfpg -->
					<div class="sticky" id='div-gpt-ad-1414141487831-0' style='width:300px; height:600px; padding-top: 3px;'>
					<script type='text/javascript'>
					googletag.display('div-gpt-ad-1414141487831-0');
					</script>
					</div>
				</div>
            </td>
        </tr>
    </table>

	<table style="width: 1065px">
		<tr>
			<td style="width: 365px;">
				<!-- COMY_footer_lrec1 -->
				<div class="sticky-footer" id='div-gpt-ad-1409216776650-0' style='width:336px; height:280px; margin: 0 auto;'>
				<script type='text/javascript'>
					googletag.display('div-gpt-ad-1409216776650-0');
				</script>
				</div>
			</td>
			<td style="width: 5px">
				&nbsp;
			</td>
			<td style="width: 365px;">
				<!-- COMY_footer_lrec2 -->
				<div id='div-gpt-ad-1409216776650-1' style='width:336px; height:280px; margin: 0 auto;'>
				<script type='text/javascript'>
					googletag.display('div-gpt-ad-1409216776650-1');
				</script>
				</div>
			</td>
			<td style="width: 5px">
				&nbsp;
			</td>
			<td style="width: 325px;" valign="top">
				<!-- /55577451/COMY_ros_footer_rec -->
				<div id='div-gpt-ad-1470016231071-4' style='height:250px; width:300px; margin: 0 auto;'>
				<script>
				googletag.display('div-gpt-ad-1470016231071-4');
				</script>
				</div>
			</td>
		</tr>
	</table>

        </div>
        <br /><br />               
        <div id="footer">                   
            <a href="/advertise/advertise.aspx">Advertise with Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/about_us.aspx">About Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/contact_us.aspx">Contact Us</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/faq.aspx">FAQ</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="http://www.thehive.asia/" target="_blank">TheHive.asia</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/privacy_policy.aspx">Privacy Policy</a>&nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="/vacancy.aspx">Vacancy</a>
            <hr />
            <span>
                © 1999 - 2017
                All content copyright of Cinema Online and their respective owners<br />
                e-mail: <a href="mailto:marketing@cinemaonline.asia" target="_top">marketing@cinemaonline.asia</a> for inquiries. <span id="ctl00_testing1_lblFigure"><font color="White">85320</font></span>
            </span>
        </div>        
    </div>    
    

<script type="text/javascript">
//<![CDATA[
Sys.Application.initialize();
//]]>
</script>
</form>

    
			
	
		

</body>
</html>
