<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta name="description" content="At IRONMAN.com, we are committed to safeguarding your privacy online. We want to assure our users that IRONMAN.com will not willfully disclose any specific individual information about you to any third party unless you give us your express permission to do so. " />
    <meta name="keywords" content="" />
    <meta name="ROBOTS" content="NOYDIR" />
    <meta name="ROBOTS" content="NOODP" />
    <meta name="p:domain_verify" content="b20f42b0bafc1bfc2f559241022a8c9d" >
    <meta name="google-site-verification" content="C0SboseTtSt6vFpE98Yq1Ur2IVGgnaB9bRkIqHGDNFw" />
    <meta property="twitter:account_id" content="1510104590" />

    <meta property="fb:admins" content="49102455,584780102,748220376,1134223497,100000369144587,100000475753312"/>
    

    
    
    

    <meta property="og:title" content=""/>
    <meta property="og:type" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:site_name" content="IRONMAN.com"/>
    <meta property="og:description" content=""/>

    <title>Privacy Policy - IRONMAN Official Site | IRONMAN triathlon 140.6 & 70.3</title>

    <link rel="canonical" href="http://www.ironman.com/triathlon/privacy-policy.aspx" />

    <link rel="icon" type="image/png" href="http://www.ironman.com/media/favicon.png" />
    <link rel="shortcut icon" href="http://www.ironman.com/media/favicon.ico" />

    
<link rel="stylesheet" type="text/css" href="http://www.ironman.com/includes/cssbin/mini/ironman.0.css" media="all" />
    <link rel="stylesheet" type="text/css" href="/includes/cssbin/ironman.min.css?v=17" media="all" />

  	

    <!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="http://www.ironman.com/includes/cssbin/ie8.css" media="all" />
	<![endif]-->

	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="http://www.ironman.com/includes/cssbin/ie7.css" media="all" />
	<![endif]-->

    <link href='http://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css'>
    

<script type="text/javascript" src="http://www.ironman.com/includes/jsbin/mini/ironman.2223920852082.js"></script>
    





    <!-- need this for all of the handlers used by the new IronFan section -->
    <script type="text/javascript">
        var raceStartTime = "";
        var raceStartTimeZone = "";
        var raceStartTimeZoneAbbreviated = "";
        var timezoneOffset = "";
        var subEventId = "";
        var domainServicesSecure = "https://services.ironman.com";
        var domainServicesCached = "http://services-cdn.ironman.com";
        

        if (LiveMap === undefined) {
            var LiveMap = {};
        }
        if (LiveMap.vars === undefined) {
            LiveMap.vars = {};
        }
        LiveMap.vars.debugHandlebars = false;
    </script>

    <script type="text/javascript">
        var shareURL = "http%3a%2f%2fwww.ironman.com%2ftriathlon%2fprivacy-policy.aspx";
        var shareTitle = "Privacy+Policy";
        var ord = Math.random() * 10000000000000000;
        ord = Math.round(ord);
        var dst = "False";
        var availableLanguages = [];
        var pageId = "5880dc60164046b089c417733fe8b9bd";
        var currentLanguage = "en";
        var directories = document.location.pathname.split("/");
        if (directories.length > 1) {
            if ((directories[1].length === 2) || ((directories[1].length === 5) && (directories[1].charAt(2) === "-"))) {
                currentLanguage = directories[1];
            }
        }
        //console.log("dir- " + directories);
        //console.log("lang- " + currentLanguage);
        var trackerUrl = "";
    </script>

    <script type="text/javascript">
        if (trackerUrl !== 'undefined' && trackerUrl.length > 0) {
            $(window).load(function() {
                initAjaxPopUp(trackerUrl);
            });
        }
    </script>

    <script type="text/javascript">
        var ga_accountID, ga_domain;
        var host = location.host;

        if (host == "beta.ironman.com" || host == "ironman.com" || host == "www.ironman.com" || host == "ap.ironman.com" || host == "eu.ironman.com" || host == "m.ironman.com" || host == "m.ap.ironman.com" || host == "m.eu.ironman.com") {
            ga_accountID = 'UA-213900-2';
            ga_domain = 'ironman.com';
        } else {
            ga_accountID = 'UA-34708665-1';
            ga_domain = 'digitaria.com';
        }

        // for testing only, remove from production
        //if (typeof console.log != "undefined") {
        //console.log("account id= " + ga_accountID);
        //}

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', ga_accountID]);
        _gaq.push(['_setDomainName', ga_domain]);

        // Updated custom variable tracking for Double Click data fields
        _gaq.push(['_setCustomVar', 1, 'scTemplate', 'Text Detail', 3]);
_gaq.push(['_setCustomVar', 2, 'scEvent', '', 3]);
_gaq.push(['_setCustomVar', 3, 'scTag', "", 3]);
_gaq.push(['_setCustomVar', 4, 'Article Byline', '', 3]);


        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>

    <!-- Start of Neustar Real User Measurements code -->
    <script type="text/javascript">
        ns_rum = {};
        var t_pagestart = new Date().getTime(), t_pageend;
        ns_rum.init = function () {
            t_pageend = new Date().getTime();
            var s = document.createElement ('script'); s.id = 'rum';
            s.type = 'text/javascript'; s.src = 'https://d2lo2tipcl3aii.cloudfront.net/C620CFB87B594673BEA261FF02820DF9/neustar.beacon.js';
            document.getElementsByTagName('head')[0].appendChild(s);
        }
        window.addEventListener ? window.addEventListener('load', ns_rum.init, false) : window.attachEvent ? window.attachEvent('onload', ns_rum.init) : false;

        ns_rum.errors = [];
        ns_rum.ogErrorHandler = window.onerror;
        window.onerror = function rumErrorHandler(msg, url, line){
            ns_rum.errors.push({'url': url, 'line': line, 'msg': msg});
            if(ns_rum.ogErrorHandler){ return ns_rum.ogErrorHandler(msg, url, line); }
            return false;
        }
    </script>
    <!-- End of Neustar Real User Measurements code -->

    
        <!-- Start: Double Click Ad Code -->
        <script type='text/javascript'>
            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            (function () {
                var gads = document.createElement('script');
                gads.async = true;
                gads.type = 'text/javascript';
                var useSSL = 'https:' == document.location.protocol;
                gads.src = (useSSL ? 'https:' : 'http:') +
                    '//www.googletagservices.com/tag/js/gpt.js';
                var node = document.getElementsByTagName('script')[0];
                node.parentNode.insertBefore(gads, node);
            })();
        </script>

        <script type='text/javascript'>
            googletag.cmd.push(function () {
                googletag.defineSlot('/4606835/IM_300x250', [300, 250], 'div-gpt-ad-2064498911').addService(googletag.pubads());

                googletag.pubads().collapseEmptyDivs();
                googletag.pubads().enableSingleRequest();
                googletag.pubads().setTargeting("url", "http://www.ironman.com/triathlon/privacy-policy.aspx");
googletag.pubads().setTargeting("scTemplate", "Text Detail");
googletag.pubads().setTargeting("adUnit", "");

                googletag.enableServices();
            });
        </script>
        <!-- End: Double Click Ad Code -->
    
    
    <!--
      IronFan - EasyXDM
      =================
    -->
    
    <!-- This should be changed so that it points to the minified version before use in production.-->
    <script type="text/javascript" src="/Includes/JsBin/easyXDM-2.4.19.3/easyXDM.min.js"></script>
    <script type="text/javascript">
        // Update to point to your copy
        easyXDM.DomHelper.requiresJSON("/includes/jsbin/easyXDM-2.4.19.3/json2.js");
    </script>
</head>

<body class="layout3 articleDetail">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KK6278"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>    (function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({ 'gtm.start':
                new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                        '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-KK6278');</script>
<!-- End Google Tag Manager -->




    <!-- Begin: eventCoverage -->
    <div class="eventCoverage">
        <div class="eventCoverageWrapper clear">
            <h3>Event Coverage</h3>
            <ul class="eventCoverageLocation">
                <li><a href="http://www.ironman.com/triathlon/coverage/detail.aspx?race=buenosaires70.3&y=2016">Buenos Aires</a><div class="expand"> IRONMAN 70.3 Buenos Aires</div></li><li><a href="http://www.ironman.com/triathlon/coverage/detail.aspx?race=subicbay70.3&y=2016">Subic Bay Philippines</a><div class="expand">Century Tuna IRONMAN 70.3 Subic Bay Philippines</div></li><li><a href="http://www.ironman.com/triathlon/coverage/detail.aspx?race=newzealand&y=2016">New Zealand</a><div class="expand">Kellogg's Nutri-Grain IRONMAN New Zealand</div></li>
            </ul>
        </div>
    </div>
    <!-- End: eventCoverage -->


<!-- Start: Header -->
<div id="headerWrapper">
    <header class="masthead clear">

        <!-- Site Logo -->
        <div id="siteLogo">
            <a href="http://www.ironman.com/" title="IRONMAN Official Site | IRONMAN triathlon 140.6 & 70.3">IRONMAN Official Site | IRONMAN triathlon 140.6 & 70.3</a>
        </div>
        

        <!-- Header Search Form -->
        <form id="siteSearchForm" name="searchForm" method="get" action="http://www.ironman.com/search/site.aspx">
            <label for="siteSearch">Search</label>
			<input type="text" name="q" onfocus="this.value=''" value="Search..." id="siteSearch">
			<button id="siteSearchBtn" type="submit">Submit</button>
        </form>

        <nav class="siteNav">
			<ul>

<li class=" first withDropdown"><a href="http://www.ironman.com/events/triathlon-races.aspx">Races</a>
<div class="subNavWrapper">    <div class="subNavWrapperInner"><a href="http://www.ironman.com/triathlon/news/ironman-world-championship.aspx" title=""><img class="colImage" src="http://www.ironman.com/~/media/a422a9a52a1c40a8bdeeb4f5c6836765/races%20navimg%202.jpg?w=342&h=194" width="342" height="194"  title="" alt="" /></a>

<div class="col">
    
	<a href="http://www.ironman.com/events/triathlon-races.aspx" title="All Races" class="colLabel">All Races</a>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon/coverage/live.aspx" class="liveHeaderCss first">Live Now!</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?d=ironman">IRONMAN</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?d=ironman+70.3">IRONMAN 70.3</a></li><li><a href="http://www.ironman.com/triathlon/events/championships.aspx">Championships</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?d=ironkids">IRONKIDS</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?d=iron girl">Iron Girl</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?d=multisports">MultiSport Festivals</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?d=5i50">5150 and International</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<p class="colLabel">Locations</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/events/triathlon-races.aspx?l=north+america" class="first">North America</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?l=south+america">South America</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?l=europe">Europe</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?l=middle+east">Middle East</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?l=africa">Africa</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?l=asia">Asia</a></li><li><a href="http://www.ironman.com/events/triathlon-races.aspx?l=australia">Australia</a></li><li><a href="http://ap.ironman.com/events/triathlon-races.aspx?l=australia" class="last">New Zealand</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<p class="colLabel">Programs</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon/organizations/triclubs.aspx" class="first">TriClub Program</a></li><li><a href="http://www.ironman.com/triathlon/events/vip-experience.aspx">VIP Experience</a></li><li><a href="http://www.ironman.com/triathlon/events/charity-entries.aspx">Charity Partner Entries</a></li><li><a href="http://www.ironman.com/triathlon/organizations/all-world-athlete.aspx">All Word Athlete Program</a></li><li><a href="http://www.ironman.com/triathlon/organizations/ironman-xc.aspx">Executive Challenge</a></li><li><a href="http://www.ironman.com/triathlon/events/americas/ironman/world-championship/register/ironman-legacy-program.aspx">Kona Legacy Program</a></li><li><a href="http://www.ironman.com/full-refund-plan.aspx">Refund Plan</a></li><li><a href="http://www.ironman.com/triathlon/events/north-american-ironman-transfer-pilot-program.aspx" class="last">Transfer Program</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<p class="colLabel">Initiatives</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon/organizations/women-for-tri.aspx" class="first">Women for Tri</a></li><li><a href="http://www.ironmanfoundation.org" target="_blank">Foundation</a></li><li><a href="http://www.ironman.com/triathlon/events/swimsmart-initiative.aspx">SwimSmart</a></li><li><a href="http://www.ironman.com/triathlon/organizations/anti-doping.aspx" class="last">Anti-Doping</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<a href="http://www.ironman.com/triathlon/history.aspx" title="History" class="colLabel">History</a>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon/history.aspx" class="first">Our History</a></li><li><a href="http://www.ironman.com/triathlon/history/hall-of-fame.aspx" class="last">Hall of Fame</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>
    </div></div>
</li>

<li class=" withDropdown"><a href="http://www.ironman.com/triathlon/coverage/past.aspx">Results</a>
<div class="subNavWrapper">    <div class="subNavWrapperInner"><a href="http://www.ironman.com/" title=""><img class="colImage" src="http://www.ironman.com/~/media/7a3fb5a528604aed9e4f8e9fbee67a6b/results%20navimg%201.jpg?w=342&h=194" width="342" height="194"  title="" alt="" /></a>

<div class="col">
    
	<p class="colLabel">Age Group Athletes</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon/coverage/past.aspx" class="first">Recent Results</a></li><li><a href="http://www.ironman.com/triathlon/organizations/all-world-athlete/agr-and-all-world-athletes.aspx">Rankings</a></li><li><a href="http://www.ironman.com/triathlon/organizations/all-world-athlete.aspx">All World Athlete Program</a></li><li><a href="http://www.ironman.com/triathlon/triathlon-rankings/triclub-ranking-overview.aspx" class="last">Tri Club Rankings</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<p class="colLabel">Professional Athletes</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon-news/race-news.aspx" class="first">Race News</a></li><li><a href="http://www.ironman.com/triathlon/triathlon-rankings/points-system.aspx">Rankings</a></li><li><a href="http://www.ironman.com/triathlon/organizations/pro-membership.aspx" class="last">Membership</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<p class="colLabel">Coverage</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon/coverage/live.aspx" class="liveHeaderCss first">Live Now!</a></li><li><a href="http://www.ironman.com/triathlon/coverage/past.aspx">Past Coverage</a></li><li><a href="http://www.ironman.com/triathlon-news/race-news.aspx" class="last">Race News</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>
    </div></div>
</li>

<li class="mlife withDropdown"><a href="http://www.ironman.com/triathlon-news/ironman-life.aspx">Life</a>
<div class="subNavWrapper">    <div class="subNavWrapperInner"><a href="http://home.trainingpeaks.com/products/trainingplans/affiliates/ironman/official-ironman-training-plans" title="" target="_blank"><img class="colImage" src="http://www.ironman.com/~/media/bfe2dc93cc904ef48fdf551ef82224f8/tp%20ironman%20trainingplans%20banner%20final%20red.jpeg?w=342&h=285" width="342" height="285"  title="" alt="" /></a>

<div class="col">
    
	<a href="http://www.ironman.com/triathlon-news/ironman-life.aspx" title="News and Features" class="colLabel">News and Features</a>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon-news/headlines.aspx" class="first">News</a></li><li><a href="http://www.ironman.com/triathlon-news/race-news.aspx">Race News</a></li><li><a href="http://www.ironman.com/triathlon-news/profiles.aspx">Profiles</a></li><li><a href="http://www.ironman.com/triathlon-news/inspiration.aspx" class="last">Inspiration</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<a href="http://www.ironman.com/triathlon/getting-started.aspx" title="Get Started" class="colLabel">Get Started</a>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon-news/ironman-101.aspx" class="first">IRONMAN 101</a></li><li><a href="http://www.ironman.com/triathlon/organizations/triclubs.aspx">TriClubs</a></li><li><a href="http://www.ironman.com/ironman-faq.aspx">FAQ</a></li><li><a href="http://www.ironman.com/rules-and-regulations.aspx" class="last">Rules</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<a href="http://www.ironman.com/triathlon-news/training.aspx" title="Reach Your Goals" class="colLabel">Reach Your Goals</a>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon-news/training.aspx" class="first">Training</a></li><li><a href="http://www.ironman.com/triathlon-news/nutrition.aspx">Nutrition</a></li><li><a href="http://home.trainingpeaks.com/products/trainingplans/affiliates/ironman/official-ironman-training-plans" target="_blank" class="last">Training Plans</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<a href="http://www.ironman.com/triathlon/social-community/from-the-fans.aspx#/axzz3yU3ZBl5o" title="Social Community" class="colLabel">Social Community</a>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon/social-community/from-the-fans.aspx" class="first">From the Fans</a></li><li><a href="http://www.ironman.com/triathlon/social-community/facebook.aspx">Facebook</a></li><li><a href="http://www.ironman.com/triathlon/social-community/twitter.aspx">Twitter</a></li><li><a href="http://www.ironman.com/triathlon/social-community/instagram.aspx">Instagram</a></li><li><a href="http://www.ironman.com/triathlon/social-community/pinterest.aspx" class="last">Pinterest</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>
    </div></div>
</li>

<li class=" withDropdown"><a href="https://university.ironman.com/#sthash.bhyg3cpx.dpbs">IRONMAN U</a>
<div class="subNavWrapper">    <div class="subNavWrapperInner"><img class="colImage" src="http://www.ironman.com/~/media/a5cb50535c8947ed9fa04b4f23ebaa3e/imunavigation%20graphic.jpg?w=342&h=194" width="342" height="194"  title="" alt="" />

<div class="col">
    
	<p class="colLabel">Coaches</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/triathlon/ironman-u.aspx" class="first">About</a></li><li><a href="https://university.ironman.com/content/coaching-certification-course#sthash.grfwkylv.dpbs">Coaching Certification</a></li><li><a href="https://university.ironman.com/content/ironman-coaches-association#sthash.znbfrxtv.dpbs" class="last">Coaches Association</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<p class="colLabel">Athletes</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="https://university.ironman.com/content/train-smart#sthash.9ox6gj5h.dpbs" class="first">Athlete Education</a></li><li><a href="http://www.ironman.com/triathlon/ironman-u/startnow" class="last">Find a Coach</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>
    </div></div>
</li>

<li class=" withDropdown"><a href="http://www.ironman.com/partners.aspx">Partners</a>
<div class="subNavWrapper">    <div class="subNavWrapperInner"><img class="colImage" src="http://www.ironman.com/~/media/fd67425cab18407ba4e81f3206b05ca7/image005.jpg?w=342&h=192" width="342" height="192"  title="Katherine Kelly Lang (54) from The Bold and the Beautiful rejoices." alt="Katherine Kelly Lang (54) from The Bold and the Beautiful rejoices." />

<div class="col">
    
	<p class="colLabel">IRONMAN Partners</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironman.com/partners/ironman-north-american-series.aspx" class="first">IRONMAN North American Series</a></li><li><a href="http://www.ironman.com/partners/ironman-u.s.-series.aspx">IRONMAN U.S. Series</a></li><li><a href="http://www.ironman.com/partners/ironman-70.3-u.s.-series.aspx">IRONMAN 70.3 U.S. Series</a></li><li><a href="http://www.ironman.com/partners/ironman-canadian-series.aspx">IRONMAN Canadian Series</a></li><li><a href="http://www.ironman.com/partners/ironman-charity-partners.aspx" class="last">IRONMAN Charity Partners</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>
    </div></div>
</li>

<li class=" last withDropdown"><a href="http://www.ironmanstore.com?utm_source=ironman+site&utm_medium=nav+bar&utm_campaign=shop+button" target="_blank">Shop</a>
<div class="subNavWrapper">    <div class="subNavWrapperInner"><a href="http://www.ironmanstore.com/zoot-apparel.html?&utm_source=ironman+site&utm_medium=nav+image&utm_campaign=zoot+preorder" title=""><img class="colImage" src="http://www.ironman.com/~/media/a227cc49c93c405cb2055511fd86ad10/zoot%20im%20dropdown%202.jpg?w=342&h=209" width="342" height="209"  title="" alt="" /></a>

<div class="col">
    
	<p class="colLabel">Official IRONMAN Gear</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironmanstore.com/event-discount.html?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=event" target="_blank" class="first">Event Gear</a></li><li><a href="http://www.ironmanstore.com/finisher.html?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=finisher">Finisher</a></li><li><a href="http://www.ironmanstore.com/training-essentials.html?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=training">Training</a></li><li><a href="http://www.ironmanstore.com/kona-dvds.html?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=dvds" class="last">DVDs</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>


<div class="col">
    
	<p class="colLabel">Free Shipping on Orders Over $100</p>
	<!-- Begin: sub nav links -->
	<ul>
		<li><a href="http://www.ironmanstore.com/trucker-hats.html?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=hats+visors" class="first">Hats and Visors</a></li><li><a href="http://www.ironmanstore.com/ironman-bags-and-backpacks.html?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=bags">Bags</a></li><li><a href="http://www.ironmanstore.com/special-offer.html?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=sale">Sale</a></li><li><a href="http://www.ironmanstore.com/vip-packages.html?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=vip">VIP Packages</a></li><li><a href="http://www.ironmanstore.com/borderfree/selector/?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=border+free" class="last">International Athletes - Click Here</a></li>
	</ul>
	<!-- End: sub nav links -->
</div>
    </div></div>
</li></ul>
        </nav>

        <div id="utilityMenu">
            <div id="utilityMenuInner">
                <div class="controls">
                    <div id="utilityHandle" class="clear">
                        <a class="accountLink login" href="/triathlon/registration/sign-in.aspx">Login</a>
                        <a class="accountLink register" href="/triathlon/registration/sign-up.aspx">Register</a>
                        <a class="accountLink my-account" href="/triathlon/account/account-settings.aspx">My Account</a>
                        <a class="accountLink logout" href="https://services.ironman.com/handlers/registration/logout.ajax?s=www.ironman.com">Log Out</a>
                        <a class="regionLink" href="#">Region<span class="icon"></span></a>
                        <div class="regionDropdown">
                            <ul>
                                <li class="category"><a href="http://www.ironman.com" class="active">North America</a></li>

<li class="category"><a href="http://eu.ironman.com">Europe/Africa</a></li>

<li class="category"><a href="http://ap.ironman.com">Asia-Pacific</a></li>

<li class="category"><a href=""></a></li>

<li class="category"><a href=""></a></li>

<li class="category"><a href=""></a></li>


                            </ul>
                        </div>
                    </div>
                    <div id="socialNav">
                        <a href="http://www.facebook.com/Ironmantri" title="facebook" target="_blank" class="facebookLink ss-icon">&#xF610;</a>
		                <a href="http://twitter.com/ironmantri" title="Twitter" target="_blank" class="twitterLink ss-icon">&#xF611;</a>
                        <a href="http://www.youtube.com/ironmantriathlon" title="YouTube" target="_blank" class="youtubeLink ss-icon">&#xF630;</a>
                        <a href="http://www.ironman.com/triathlon/subscribe.aspx" title="Email" class="emailLink ss-icon">&#x2709;</a>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>
<!-- End: Header -->

<!-- Begin: Wrapper -->
<div id="wrapper" class="">

    <div id="backgroundArea">
        
    

    </div>

    <div id="backgroundImage">
        
        
    

    </div>

    <!-- Begin: header content area -->
    
    

<header class="pagehead">
    <h2 class="">Privacy Policy</h2>
    
</header>

    <!-- End: header content area -->

    <div id="wrapperInner">

        <!-- Begin: main content area -->
        
	
	
    <div id="navCol" class="navRail">
		

    </div>
    

    <div id="mainContentColExtra" class="extra clear">
		
		

	</div>
	<!-- Start: main content area -->
	<div id="mainContentColWrap" class="clear">
		<div id="mainInnerColWrap">
			<div id="mainContentCol1" class="">
				

			</div>
			<div id="mainContentCol2" class="">
				

			</div>
			<div id="mainContentCol3" class="rail3">
				

			</div>
			<div id="mainContentCol4" class="rail4">
				



<div class="moduleWrap genericDesc" style="border-bottom: none;">
    <div class="moduleContentOuter">
        <div class="moduleContentInner clear">
            <section>
                <header><h1>Privacy Policy</h1></header>
                <p>At IRONMAN.com, we are committed to safeguarding your privacy online. We want to assure our users that IRONMAN.com will not willfully disclose any specific individual information about you to any third party unless you give us your express permission to do so. </p><p>At IRONMAN.com, we are committed to safeguarding your privacy online. We want to assure our users that IRONMAN.com will not willfully disclose any specific individual information about you to any third party unless you give us your express permission to do so.&nbsp;</p>
<p>IRONMAN.com does not solicit any personally identifiable information from children under the age of 13. Accordingly, children under 13 years old may be restricted from participating in certain activities and transactions on IRONMAN.com (including the Ironmanstore.com on IRONMAN.com at store.IRONMAN.com). IRONMAN.com works with (and may provide links to) third party sites that offer features described on IRONMAN.com (e.g., official fantasy games, chatrooms, message boards). Such third parties may permit children under the age of 13 to register for these features on their sites in accordance with their respective privacy policies. However, IRONMAN.com does not knowingly obtain, retain or use information from anyone who indicates that they are under 13 years old. If you would like more information about IRONMAN.com's policy for collecting information from children, please read our Note to Parents below.</p>
<p>Please read this Privacy Policy ("Policy") for an explanation of how we will treat your personal information. By using IRONMAN.com, including any of the Official Ironman Race Websites and the Ironmanstore.com on IRONMAN.com, you consent to the collection and use of personal information as outlined in this Policy. If you do not agree to this Policy, please do not use our site. If we make any material changes to the Policy, those changes will be posted here so that you are always aware of what personal information is collected, how that information is used, and under what circumstances that information may be disclosed. Since this Policy may change from time to time, you should check back periodically. Your continued use of IRONMAN.com following the posting of changes to the Policy will signal your acceptance of such changes.&nbsp;</p>
<p>If you have any questions or comments regarding this Policy, please contact <script type="text/javascript">
<!--
var emailStart = 'ben';
var emailEnd = 'IRONMAN.com';
document.write(emailStart + "@" + emailEnd);
//--> </script></p>
<p><strong>TABLE OF CONTENTS</strong></p>
<p><strong>1. </strong><strong>What personally identifiable information of yours or third party personally identifiable information is collected from you through the site;</strong></p>
<p><strong>2. </strong><strong>The organization collecting the information; </strong></p>
<p><strong>3. </strong><strong>How the information is used; </strong></p>
<p><strong>4. </strong><strong>With whom the information may be shared; </strong></p>
<p><strong>5. </strong><strong>What choices are available to you regarding collection, use and distribution of the information; </strong></p>
<p><strong>6. </strong><strong>The kind of security procedures that are in place to protect the loss, misuse or alteration of <g class="gr_ gr_323 gr-alert gr_gramm Grammar only-ins replaceWithoutSep" id="323" data-gr-id="323">information</g> under our control. </strong></p>
<p><strong>1. What personally identifiable information of yours or third party personally identifiable information is collected from you through the site </strong><br />
<br />
As stated above, IRONMAN.com does not solicit any personally identifiable information from children under the age of 13. Please review our Note to Parents, if you have any questions regarding our Policy with respect to children. <br />
<br />
IRONMAN.com does not collect personally identifiable information through the site unless you provide that information during registration for various features on the site (e.g., IRONMAN.com personalization, newsletters, purchases on the Ironmanstore.com on IRONMAN.com, Sweepstakes etc.). Information required to participate in such features may vary but will typically include your first and last name; address including city, state, zip code and country; complete date of birth; and e-mail address; and, in the case of making purchases, a credit card number and expiration date. You may also be requested to provide a username and password for certain features. IRONMAN.com will maintain all of the personal information submitted by you in secured servers behind corporate firewalls.<br />
<br />
<strong>2. The organization collecting the information</strong><br />
<br />
IRONMAN.com is operated by World Triathlon Corporation. World Triathlon Corporation is located at 43309 US HWY 19 N, Tarpon Springs, Florida 34689. <script type="text/javascript">
<!--
var front = '<a href="mai';
var textStart = 'ben';
var textMiddle = '@';
var textEnd = 'IRONMAN.com';
var emailStart = 'ben';
var emailEnd = 'ironmanlive.com';
var end = '">';
document.write(front + "lto:" + emailStart + "@" + emailEnd + end + textStart + textMiddle + textEnd + "</a>");
//--> </script> for more information. You can also learn more about any of the 17 Qualifying Ironman Races by referring to their respective Official Web Sites located on IRONMAN.com. <br />
<br />
<strong>3. How the information is used</strong> <br />
<br />
IRONMAN.com may use <g class="gr_ gr_351 gr-alert gr_gramm Grammar only-ins replaceWithoutSep" id="351" data-gr-id="351">information</g> provided at registration or information obtained through third parties to offer <g class="gr_ gr_352 gr-alert gr_spell ContextualSpelling multiReplace" id="352" data-gr-id="352">better targeted</g> products and services to you. We use targeting to customize and enhance your online experience on the site. The use of information provided at registration or obtained through third parties will not be provided to advertisers or partners, except in aggregate <g class="gr_ gr_350 gr-alert gr_gramm Punctuation only-del replaceWithoutSep" id="350" data-gr-id="350">form,</g> unless you have consented to such use at registration. <br />
<br />
IRONMAN.com uses "cookie" technology. Cookies are pieces of information transferred to your hard drive for record-keeping purposes. Such technology is an industry standard and is used by most major Web sites. IRONMAN.com will never use cookies to retrieve information from your computer that was not originally sent in a cookie. In order to effectively use the site and some of its features (e.g.,&nbsp; Advertising personalization, Ironmanstore.com on IRONMAN.com), your browser must accept cookies. <br />
<br />
Users should be aware that IRONMAN.com cannot control the use of cookies or the resulting information by advertisers or third parties hosting data for IRONMAN.com. If a user does not want information collected through the use of cookies, there is a simple procedure in most browsers that <g class="gr_ gr_339 gr-alert gr_gramm Grammar multiReplace" id="339" data-gr-id="339">allows</g> the user to deny or accept cookies; however, users should note that cookies may be necessary to provide the user with certain features available on IRONMAN.com. <br />
<br />
IRONMAN.com tracks certain general usage pattern information, including what pages people visit and how long they have been registered users. IRONMAN.com may log users' IP addresses (the group of four numbers separated by periods, such as 123.45.64.89, that identifies a particular host computer) for the purposes of system administration, site tracking and reporting of aggregate usage information to our marketing and product development teams. <br />
<br />
From time to time, IRONMAN.com may solicit your opinion to determine your preferences and dislikes in order to provide you with a better product. These surveys are always optional to the user and your decision not to participate in a survey will not affect your status as an IRONMAN.com user. Should you choose to participate, any information you give us may be used in aggregate form for marketing purposes, and to enhance your overall experience on our site. <br />
<br />
<strong>4. With whom the information may be shared</strong> <br />
<br />
Once again, IRONMAN.com will not provide any personally identifiable information, other than to its affiliates, about you to advertisers or other partners without your consent. As stated above, IRONMAN.com may, however, provide advertisers and/or other partners with anonymous aggregated information about our users to illustrate trends and patterns. All third parties with whom we share such information will have independent privacy and data collection practices and IRONMAN.com has no responsibility or liability for their policies or actions.<br />
<br />
We will never give out any information linking your real name to your screen name unless you authorize us to do so, or unless required to do so by law. <br />
<br />
IRONMAN.com has several partners that can offer valuable products and services to our members. From time to time, IRONMAN.com may give or sell a list from our database of users to these third party vendors. These lists will consist of only those users that have "opted in" to receiving special offers and promotions during the initial registration process. IRONMAN.com will use commercially reasonable efforts to ensure that every marketing offer sent to an IRONMAN.com user as a result of the provision of such information contains a link for that user to no longer receive such offers. In addition, we may share your information with a company that acquires all or substantially all of IRONMAN.com's assets or in a merger or acquisition, but only if such company agrees to abide by this Privacy Policy.<br />
<br />
While IRONMAN.com is careful in choosing its partners and advertisers, we are only responsible for content on our own site. All third parties associated with IRONMAN.com (including, but not limited to, advertisers, sponsors and other Internet sites) with whom we share information have independent privacy and data collection practices. IRONMAN.com does not control this collection of information and is neither responsible nor liable for their independent policies or actions. <br />
<br />
IRONMAN.com uses DoubleClick, a reputable third party vendor, to serve the advertisements that you see on the pages of our site. If you would like to know more about the information gathering practices and "opt-out" procedures of our third party ad server, please <a href="http://www.doubleclick.net/us/corporate/privacy/">Click Here</a>. <br />
<br />
<strong>5. What choices are available to you regarding collection, use and distribution of the information</strong> <br />
<br />
You are not required to receive any mailings that you do not wish to receive. During the registration process, you will have the option to sign up to receive newsletters and selected sponsor offers and discounts. You will not receive such information if you do not select such options. If, after you choose to receive such information, you wish to discontinue receiving these items, you may change your subscription preferences by following the instructions provided within the e-mails received.</p>
<p><strong>6. The kind of security procedures that are in place to protect the loss, misuse or alteration of information under our control</strong> <br />
<br />
IRONMAN.com has security measures in place to protect against the loss, misuse and alteration of the information under its control. The servers for IRONMAN.com are stored in a physically secure and climate controlled off-site facility. Beyond the physical protection of the servers, we protect against data loss by regularly backing up data, performing consistency checks with independently recorded log files to prevent tampering, and restricting access to information on our servers via several user authentication technologies. Our site uses SSL or "Secure Sockets Layer", an industry standard security protocol. When you place an order on the site and your credit card information is transmitted, you will connect with our secure server. SSL sends your browser information that encrypts your order, changing all the information that you send back into a code that is extremely difficult to decipher. <br />
<br />
Only those employees needed to carry out the business functions will have access to information on individual IRONMAN.com customers. We train each such employee with respect to the IRONMAN.com Privacy Policy, and those violating these policies will be subject to disciplinary action. <br />
<br />
It is important to remember that whatever you transmit or disclose online can be collected and used by others or unlawfully intercepted by third parties. No data transmission over the Internet can be guaranteed to be 100% secure. While we strive to protect your personal information, we cannot warrant the security of any information you transmit to us. In addition, we may disclose information to law enforcement or other government officials if we determine such disclosure to be appropriate, or if we are compelled to disclose it by court order or operation of law. <br />
<br />
Any information voluntarily disclosed by users and posted on our site (on message boards and/or in chat areas) may be viewed and used by others. If users post personal information that is accessible to the public online, the user does so at his or her own risk. Users accept that they may receive unsolicited messages from other parties in return.&nbsp;</p>
<p><strong>LAST UPDATED: September 7, 2001</strong><br />
Copyright 2001 World Triathlon Corporation.&nbsp; All rights reserved. <br />
<em>Use of this site signifies your acceptance of the TERMS OF USE.</em> <br />
<br />
IRONMAN.com NOTES TO PARENTS</p>
<p>It is IRONMAN.com's policy not to solicit any personally identifiable information from children. A number of interactive features on IRONMAN.com require registration to participate (e.g., e-mail newsletters, Sweepstakes, etc&hellip;. All registrants will be required to enter their full name, age, zip code and e-mail address. In some cases, users will be required to select a special user name to be used as an identifier for the feature and must also enter a password and a secret hint that will help us verify their identity if they forget their password. If IRONMAN.com determines that a person is under the age of 13, based on the information provided during the registration process, that person will not be permitted to register for features requiring the submission of personal information, in accordance with the Children's Online Privacy Protection Act of 1998. Instead, when a child under 13 years old tries to register for such features on IRONMAN.com, he or she will be directed to content which does not involve the submission of personal information. Any personally identifiable information that the child may have attempted to enter during the registration process will not be collected or maintained by IRONMAN.com. Children who indicate that they are under 13 years old will not be able to make credit card purchases or participate in interactive features on IRONMAN.com that involve the collection and retention of personal information (e.g., entering a sweepstakes). <br />
<br />
IRONMAN.com works with (and may provide links to) third party sites that offer features described on IRONMAN.com. Such third parties may permit children under the age of 13 to register for these features on their sites in accordance with their respective privacy policies. Registration for these features is pursuant to the privacy policies of the third parties operating the websites on which registration occurs. For more information, please review the privacy policies of each respective third party site. IRONMAN.com does not knowingly obtain, retain or use information from anyone who indicates that they are under 13 years old.<br />
<br />
</p>
            </section>
        </div>
    </div>
</div>



			</div>
			<div id="mainContentCol5" class="rail5">
				
<div class="moduleWrap adModule adModule300x250">
    <div class="moduleContentOuter">          
        <div class="moduleContentInner clear">
            
        	<!-- IM_300x250 --><div id='div-gpt-ad-2064498911' style='width:300px; height:250px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-2064498911'); });</script></div>

		</div>
	</div>
</div>

<div class="moduleWrap tabbedNewsList">
    <div class="moduleContentOuter">          
        <div class="moduleContentInner clear">
        	<div class="moduleTabs">
        		<div class="moduleTabsTabs">
        			<ul>
	        			<li class="first">
	        				<a href="#mostViewed" class="active">Most Viewed Articles</a>
	        			</li>
	        			<li class="last">
	        				<a href="#recentNews">Most Recent Articles</a>
	        			</li>
	        		</ul>
        		</div>
				<aside id="mostViewed" class="moduleTabsContent">
					<ul class="articleList">
                        
<li class="first">	
	<a class="imageLink" href="http://www.ironman.com/triathlon/news/articles/2013/05/six-months-to-ironman-a-basic-training-program.aspx" ><img src="http://www.ironman.com/~/media/2657bbd9c7764ef19d72f509fb3603a5/1305sixmonthstoironman%20740.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon/news/articles/2013/05/six-months-to-ironman-a-basic-training-program.aspx" class="articleLink" title="Go to IRONMAN 101: A Six-Month Training Plan" >
	    <h3>
	        IRONMAN 101: A Six-Month Training Plan

	    </h3>
	</a>
    <p><time datetime="2013-05-28" pubdate="pubdate">May 28th 2013</time> - BY John Newsom</p>    
</li>

<li class="">	
	<a class="imageLink" href="http://www.ironman.com/triathlon-news/articles/2013/06/6-rules-of-endurance-nutrition.aspx" ><img src="http://www.ironman.com/~/media/ad7ffd70370b44a68ffa96d879d16083/nutrition%20rules.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon-news/articles/2013/06/6-rules-of-endurance-nutrition.aspx" class="articleLink" title="Go to 6 Nutrition Rules for Endurance Athletes" >
	    <h3>
	        6 Nutrition Rules for Endurance Athletes

	    </h3>
	</a>
    <p><time datetime="2013-06-12" pubdate="pubdate">June 12th 2013</time> - BY Jennifer Ward</p>    
</li>

<li class="">	
	<a class="imageLink" href="http://www.ironman.com/triathlon/news/articles/2014/10/10-things-not-to-say-to-a-triathlete.aspx" ><img src="http://www.ironman.com/~/media/381e0bc7c1d54a0780e92ead9d90ec4d/1410%2010thingsnottosay%20740.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon/news/articles/2014/10/10-things-not-to-say-to-a-triathlete.aspx" class="articleLink" title="Go to 10 Things Not to Say to a Triathlete" >
	    <h3>
	        10 Things Not to Say to a Triathlete

	    </h3>
	</a>
    <p><time datetime="2014-10-28" pubdate="pubdate">October 28th 2014</time> - BY Lisa Dolbear</p>    
</li>

<li class="">	
	<a class="imageLink" href="http://www.ironman.com/triathlon/news/articles/2014/06/ironman-awa-program-benefits.aspx" ><img src="http://www.ironman.com/~/media/cecd4e39af574bfa80376a3a91f07b26/awaswim%20740.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon/news/articles/2014/06/ironman-awa-program-benefits.aspx" class="articleLink" title="Go to All World Athlete Program: What's in it for You?" >
	    <h3>
	        All World Athlete Program: What's in it for You?

	    </h3>
	</a>
    <p><time datetime="2014-06-17" pubdate="pubdate">June 17th 2014</time></p>    
</li>

<li class="last">	
	<a class="imageLink" href="http://www.ironman.com/triathlon/news/articles/2015/07/five-years-of-kona-anywhere.aspx" ><img src="http://www.ironman.com/~/media/8db8d0e501154b06a1055b6cc53e632c/1506%20vimeo%20740.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon/news/articles/2015/07/five-years-of-kona-anywhere.aspx" class="articleLink" title="Go to Five Years of Kona Anywhere" >
	    <h3>
	        Five Years of Kona Anywhere

	    </h3>
	</a>
    <p><time datetime="2015-07-07" pubdate="pubdate">July 7th 2015</time></p>    
</li>

					</ul>
				</aside>
				<aside id="recentNews" class="moduleTabsContent">
					<ul class="articleList">
					    
<li class="first">	
	<a class="imageLink" href="http://www.ironman.com/triathlon/news/articles/2016/03/ironman-new-zealand-recap.aspx" ><img src="http://www.ironman.com/~/media/89932a3b652240c493e9417c001d8972/cameron%20brown%20wins%20imnz%202016.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon/news/articles/2016/03/ironman-new-zealand-recap.aspx" class="articleLink" title="Go to Brown and Kessler Bag Records in Taupo" >
	    <h3>
	        Brown and Kessler Bag Records in Taupo

	    </h3>
	</a>
    <p><time datetime="2016-03-05" pubdate="pubdate">March 5th 2016</time></p>    
</li>

<li class="">	
	<a class="imageLink" href="http://www.ironman.com/press-releases/2016/ironman-70.3-canada.aspx" ><img src="http://www.ironman.com/~/media/f8defcf50bed4a8682ac09be714c5400/1601%20im703can%20announce%20740.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/press-releases/2016/ironman-70.3-canada.aspx" class="articleLink" title="Go to Whistler, British Columbia Added to 2016 Ironman 70.3 Series" >
	    <h3>
	        Whistler, British Columbia Added to 2016 Ironman 70.3 Series

	    </h3>
	</a>
    <p><time datetime="2016-03-04" pubdate="pubdate">March 4th 2016</time></p>    
</li>

<li class="">	
	<a class="imageLink" href="http://www.ironman.com/triathlon/news/articles/2016/03/70.3-canada-announcement.aspx" ><img src="http://www.ironman.com/~/media/f8defcf50bed4a8682ac09be714c5400/1601%20im703can%20announce%20740.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon/news/articles/2016/03/70.3-canada-announcement.aspx" class="articleLink" title="Go to IRONMAN Adds New Event in Whistler" >
	    <h3>
	        IRONMAN Adds New Event in Whistler

	    </h3>
	</a>
    <p><time datetime="2016-03-04" pubdate="pubdate">March 4th 2016</time></p>    
</li>

<li class="">	
	<a class="imageLink" href="http://www.ironman.com/triathlon/news/articles/2016/03/an-adventure-awaits-in-china-xiamen.aspx" ><img src="http://www.ironman.com/~/media/8346cf43e3024e878cfa710eb3fd1c38/1603%20gulangyu%20xiamen%20travel.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon/news/articles/2016/03/an-adventure-awaits-in-china-xiamen.aspx" class="articleLink" title="Go to An Adventure Awaits in China: Xiamen" >
	    <h3>
	        An Adventure Awaits in China: Xiamen

	    </h3>
	</a>
    <p><time datetime="2016-03-04" pubdate="pubdate">March 4th 2016</time></p>    
</li>

<li class="last">	
	<a class="imageLink" href="http://www.ironman.com/triathlon/news/articles/2016/03/the-check-in-03.04.aspx" ><img src="http://www.ironman.com/~/media/f5b85ea4e5fa42ceac1b069cdb6d1a9a/1603%20ci%20imnz%20pr%202.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" /></a>
    <a href="http://www.ironman.com/triathlon/news/articles/2016/03/the-check-in-03.04.aspx" class="articleLink" title="Go to The Foster Grant Check-In: IRONMAN Season Kicks Off in New Zealand" >
	    <h3>
	        The Foster Grant Check-In: IRONMAN Season Kicks Off in New Zealand

	    </h3>
	</a>
    <p><time datetime="2016-03-04" pubdate="pubdate">March 4th 2016</time></p>    
</li>

					</ul>
				</aside>
			</div>
		</div>
	</div>
</div>





<div class="moduleWrap listAside eventListAside">
    <div class="moduleContentOuter">          
        <div class="moduleContentInner clear">
			<aside>
				<header>
					<h2>Latest Articles</h2>
				</header>
				<ul class="eventList">
					<li class="first clear"><a href="http://www.ironman.com/triathlon/news/articles/2016/03/ironman-new-zealand-recap.aspx" class="imageLink"><img src="http://www.ironman.com/~/media/89932a3b652240c493e9417c001d8972/cameron%20brown%20wins%20imnz%202016.jpg?w=75&h=50&c=1" width="75" height="50" /></a><a href="http://www.ironman.com/triathlon/news/articles/2016/03/ironman-new-zealand-recap.aspx" class="eventLink"><h3><span>Brown and Kessler Bag Records in Taupo</span></h3></a><time datetime="03/05/2016 00:21:00" pubdate="pubdate">March 5th 2016</time></li><li class="clear"><a href="http://www.ironman.com/press-releases/2016/ironman-70.3-canada.aspx" class="imageLink"><img src="http://www.ironman.com/~/media/f8defcf50bed4a8682ac09be714c5400/1601%20im703can%20announce%20740.jpg?w=75&h=50&c=1" width="75" height="50" /></a><a href="http://www.ironman.com/press-releases/2016/ironman-70.3-canada.aspx" class="eventLink"><h3><span>Whistler, British Columbia Added to 2016 Ironman 70.3 Series</span></h3></a><time datetime="March 4, 2016" pubdate="pubdate">March 4th 2016</time></li><li class="clear"><a href="http://www.ironman.com/triathlon/news/articles/2016/03/70.3-canada-announcement.aspx" class="imageLink"><img src="http://www.ironman.com/~/media/f8defcf50bed4a8682ac09be714c5400/1601%20im703can%20announce%20740.jpg?w=75&h=50&c=1" width="75" height="50" /></a><a href="http://www.ironman.com/triathlon/news/articles/2016/03/70.3-canada-announcement.aspx" class="eventLink"><h3><span>IRONMAN Adds New Event in Whistler</span></h3></a><time datetime="03/04/2016 13:34:00" pubdate="pubdate">March 4th 2016</time></li><li class="clear"><a href="http://www.ironman.com/triathlon/news/articles/2016/03/an-adventure-awaits-in-china-xiamen.aspx" class="imageLink"><img src="http://www.ironman.com/~/media/8346cf43e3024e878cfa710eb3fd1c38/1603%20gulangyu%20xiamen%20travel.jpg?w=75&h=50&c=1" width="75" height="50" /></a><a href="http://www.ironman.com/triathlon/news/articles/2016/03/an-adventure-awaits-in-china-xiamen.aspx" class="eventLink"><h3><span>An Adventure Awaits in China: Xiamen</span></h3></a><time datetime="03/04/2016 08:41:00" pubdate="pubdate">March 4th 2016</time></li><li class="last clear"><a href="http://www.ironman.com/triathlon/news/articles/2016/03/the-check-in-03.04.aspx" class="imageLink"><img src="http://www.ironman.com/~/media/f5b85ea4e5fa42ceac1b069cdb6d1a9a/1603%20ci%20imnz%20pr%202.jpg?w=75&h=50&c=1" width="75" height="50" /></a><a href="http://www.ironman.com/triathlon/news/articles/2016/03/the-check-in-03.04.aspx" class="eventLink"><h3><span>The Foster Grant Check-In: IRONMAN Season Kicks Off in New Zealand</span></h3></a><time datetime="03/04/2016 08:41:00" pubdate="pubdate">March 4th 2016</time></li>
				</ul>
			</aside>
            
		</div>
	</div>
</div>

<div class="moduleWrap listAside eventListAside">
    <div class="moduleContentOuter">          
        <div class="moduleContentInner clear">
			<aside>
				<header>
					<h2>Latest Events</h2>
				</header>
				<ul class="eventList">
                    
<li class="first clear">
	<a href="http://www.ironman.com/triathlon/events/americas/ironman-70.3/buenos-aires.aspx" class="imageLink" >
		<img src="http://www.ironman.com/~/media/599093862f624c24b954316f9d8a76c1/buenos%20aires%202.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" />
	</a>
	<a href="http://www.ironman.com/triathlon/events/americas/ironman-70.3/buenos-aires.aspx" class="eventLink" >
		<h3><span>IRONMAN 70.3 Buenos Aires</span>Buenos Aires, Argentina</h3>
	</a>
	<time datetime="" pubdate="pubdate"></time>
</li>
<li class=" clear">
	<a href="http://www.ironman.com/triathlon/events/americas/multisports/ironman-western-canada.aspx" class="imageLink" >
		<img src="http://www.ironman.com/~/media/2ebf8ece34694d4b88582e0bf48f3208/bouldertriseries%20racecard%20thumbnail%20230x150.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" />
	</a>
	<a href="http://www.ironman.com/triathlon/events/americas/multisports/ironman-western-canada.aspx" class="eventLink" >
		<h3><span>IRONMAN Western Canada Series</span>Canada</h3>
	</a>
	<time datetime="" pubdate="pubdate"></time>
</li>
<li class=" clear">
	<a href="http://www.ironman.com/triathlon/events/asiapac/ironman-70.3/geelong.aspx" class="imageLink" >
		<img src="http://www.ironman.com/~/media/2c360a7cb0a44dba99a426a2d5c816a2/geelong%20tourism.jpg?w=75&h=50&c=1" width="75" height="50"  title="Geelong Waterfront" alt="Geelong Waterfront" />
	</a>
	<a href="http://www.ironman.com/triathlon/events/asiapac/ironman-70.3/geelong.aspx" class="eventLink" >
		<h3><span>IRONMAN 70.3 Geelong</span>Geelong, Victoria</h3>
	</a>
	<time datetime="02/07/2016 00:00:00" pubdate="pubdate">February 7th 2016</time>
</li>
<li class=" clear">
	<a href="http://www.ironman.com/triathlon/events/asiapac/ironman-70.3/geelong/ironkids.aspx" class="imageLink" >
		<img src="http://www.ironman.com/~/media/6217b2adbb10443caee4b3b0cdc37680/im703geelong%204.jpg?w=75&h=50&c=1" width="75" height="50"  title="IRONKIDS" alt="IRONKIDS" />
	</a>
	<a href="http://www.ironman.com/triathlon/events/asiapac/ironman-70.3/geelong/ironkids.aspx" class="eventLink" >
		<h3><span>IRONKIDS Geelong</span>Geelong, Victoria, Australia</h3>
	</a>
	<time datetime="02/06/2016 00:46:00" pubdate="pubdate">February 6th 2016</time>
</li>
<li class="last clear">
	<a href="http://www.ironman.com/triathlon/events/americas/ironman-70.3/panama.aspx" class="imageLink" >
		<img src="http://www.ironman.com/~/media/431404429fb542438642b4807319c001/panama%20703%203%201600.jpg?w=75&h=50&c=1" width="75" height="50"  title="" alt="" />
	</a>
	<a href="http://www.ironman.com/triathlon/events/americas/ironman-70.3/panama.aspx" class="eventLink" >
		<h3><span>IRONMAN 70.3 Pan American Pro Championship Panama</span>Panama</h3>
	</a>
	<time datetime="01/31/2016 00:00:00" pubdate="pubdate">January 31st 2016</time>
</li>
				</ul>
			</aside>
		</div>
	</div>
</div>

			</div>
            <div id="mainContentCol6" class="rail6">
				

			</div>
		</div>
	</div>
    <div id="mainContentColExtraBottom" class="extraBottom clear">
		
	</div>

        <!-- End: main content area -->

    </div>

</div>
<!-- End: Wrapper -->
<div class="bottomBg"></div>
<!-- Begin: Footer -->
<footer>
    <div id="footerInner">
        <div class="footerNav clear">
            

<dl>
    <dt><a href="http://www.ironman.com/events/triathlon-races.aspx" title="Races">Races</a></dt>
    
<dt><a href="http://www.ironman.com/events/triathlon-races.aspx" title="All Races">All Races</a></dt><dd><a class="liveFooterCss" href="http://www.ironman.com/triathlon/coverage/live.aspx" title="Live Coverage" target="_self">Live Coverage</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?d=ironman" title="IRONMAN" target="_self">IRONMAN</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?d=ironman+70.3" title="IRONMAN 70.3" target="_self">IRONMAN 70.3</a></dd>
<dd><a href="http://www.ironman.com/triathlon/events/championships.aspx" title="Championships" target="_self">Championships</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?d=ironkids" title="IRONKIDS" target="_self">IRONKIDS</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?d=5i50" title="5150 and International" target="_self">5150 and International</a></dd>
<dd><a href="http://www.ironman.com/triathlon/events/north-american-ironman-transfer-pilot-program.aspx" title="North American Transfer Program" target="_self">North American Transfer Program</a></dd>
<dd><a href="http://www.ironman.com/full-refund-plan.aspx" title="Refund Plan" target="_self">Refund Plan</a></dd>
<dt>Locations</dt><dd><a href="http://www.ironman.com/events/triathlon-races.aspx?l=north+america" title="North America" target="_self">North America</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?l=south+america" title="South America" target="_self">South America</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?l=europe" title="Europe" target="_self">Europe</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?l=africa" title="Africa" target="_self">Africa</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?l=asia" title="Asia" target="_self">Asia</a></dd>
<dd><a href="http://www.ironman.com/events/triathlon-races.aspx?l=australia" title="Australia" target="_self">Australia</a></dd>


</dl>

<dl>
    <dt>Results</dt>
    
<dt>Age Group Athletes</dt><dd><a href="http://www.ironman.com/triathlon/coverage/live.aspx" title="Recent Results" target="_self">Recent Results</a></dd>
<dd><a href="http://www.ironman.com/triathlon/organizations/all-world-athlete/agr-and-all-world-athletes.aspx" title="Rankings" target="_self">Rankings</a></dd>
<dd><a href="http://www.ironman.com/triathlon/organizations/all-world-athlete.aspx" title="All World Athletes" target="_self">All World Athletes</a></dd>
<dd><a href="http://www.ironman.com/triathlon-news/profiles.aspx" title="Profiles" target="_self">Profiles</a></dd>
<dt>Pro Athletes</dt><dd><a href="http://www.ironman.com/triathlon/organizations/pro-membership.aspx" title="Pro Membership" target="_self">Pro Membership</a></dd>
<dd><a href="http://www.ironman.com/triathlon/triathlon-rankings/points-system.aspx" title="Rankings" target="_self">Rankings</a></dd>
<dd><a href="http://www.ironman.com/triathlon-news/race-news.aspx" title="Race News" target="_self">Race News</a></dd>
<dt>Coverage</dt><dd><a class="liveFooterCss" href="http://www.ironman.com/triathlon/coverage/live.aspx" title="Live Now" target="_self">Live Now</a></dd>
<dd><a href="http://www.ironman.com/triathlon/coverage/past.aspx" title="Past Coverage" target="_self">Past Coverage</a></dd>
<dd><a href="http://www.ironman.com/triathlon-news/race-news.aspx" title="Race News" target="_self">Race News</a></dd>


</dl>

<dl>
    <dt><a href="http://www.ironman.com/triathlon-news/training.aspx" title="Training">Training</a></dt>
    
<dt><a href="http://www.ironman.com/triathlon/getting-started.aspx" title="Get Started">Get Started</a></dt><dd><a href="http://www.ironman.com/triathlon-news/ironman-101.aspx" title="IRONMAN 101" target="_self">IRONMAN 101</a></dd>
<dd><a href="http://www.ironman.com/rules-and-regulations.aspx" title="Rules" target="_self">Rules</a></dd>
<dd><a href="http://www.ironman.com/ironman-faq.aspx" title="FAQ" target="_self">FAQ</a></dd>
<dt>Reach Your Goals</dt><dd><a href="http://www.ironman.com/triathlon-news/training.aspx" title="Training" target="_self">Training</a></dd>
<dd><a href="http://www.ironman.com/triathlon-news/nutrition.aspx" title="Nutrition" target="_self">Nutrition</a></dd>
<dd><a href="http://www.ironman.com/triathlon-news/inspiration.aspx" title="Inspiration" target="_self">Inspiration</a></dd>


</dl>

<dl>
    <dt><a href="http://www.ironman.com/triathlon-news/headlines.aspx" title="News">News</a></dt>
    
<dt><a href="http://www.ironman.com/triathlon-news/headlines.aspx" title="All News">All News</a></dt><dd><a href="http://www.ironman.com/triathlon-news/race-news.aspx" title="Race News" target="_self">Race News</a></dd>
<dd><a href="http://www.ironman.com/triathlon-news/training.aspx" title="Training" target="_self">Training</a></dd>
<dd><a href="http://www.ironman.com/triathlon-news/nutrition.aspx" title="Nutrition" target="_self">Nutrition</a></dd>
<dd><a href="http://www.ironman.com/triathlon-news/inspiration.aspx" title="Inspiration" target="_self">Inspiration</a></dd>
<dt>Social Community</dt>

</dl>

<dl class="last">
    <dt class="headerCss"><a href="http://ironmanstore.com/?utm_source=ironman.com&utm_medium=nav+bar&utm_campaign=shop+button" title="Shop">Shop</a></dt>
    <dd><a href="http://www.ironmanstore.com/shop?_event=66&limit=40?utm_source=ironman+site&utm_medium=nav+drop+down&utm_campaign=event+world+championship+gear" title="Official World Championship Gear" target="_blank">Official World Championship Gear</a></dd>
<dd><a href="http://ironmanstore.com/event-50-off.html?utm_source=ironman.com&utm_medium=footer&utm_campaign=event" title="Event Gear" target="_blank">Event Gear</a></dd>
<dd><a href="http://www.ironmanstore.com/finisher.html?utm_source=ironman+site&utm_medium=footer+link&utm_campaign=finisher" title="Finisher" target="_self">Finisher</a></dd>
<dd><a href="http://www.ironmanstore.com/sugoi-deals.html?&utm_source=ironman+site&utm_medium=footer+link&utm_campaign=training" title="Training" target="_self">Training</a></dd>
<dd><a href="http://www.ironmanstore.com/special-offer.html?&utm_source=ironman+site&utm_medium=footer+link&utm_campaign=sale" title="Sale" target="_self">Sale</a></dd>

<dt class="headerCss ">Programs</dt><dd><a href="http://www.ironman.com/triathlon/organizations/triclubs.aspx" title="TriClub Program" target="_self">TriClub Program</a></dd>
<dd><a href="http://www.ironman.com/triathlon/events/vip-experience.aspx" title="VIP Experience" target="_self">VIP Experience</a></dd>
<dd><a href="http://www.ironman.com/triathlon/organizations/all-world-athlete.aspx" title="All World Athlete Program" target="_self">All World Athlete Program</a></dd>
<dd><a href="http://www.ironman.com/triathlon/events/americas/ironman/world-championship/register/ironman-legacy-program.aspx" title="Kona Legacy Program" target="_self">Kona Legacy Program</a></dd>
<dd><a href="http://www.ironman.com/triathlon/organizations/anti-doping.aspx" title="Anti-Doping" target="_self">Anti-Doping</a></dd>
<dd><a href="http://www.ironman.com/triathlon/organizations/women-for-tri.aspx" title="Women for Tri" target="_self">Women for Tri</a></dd>
<dd><a href="http://www.ironman.com/triathlon/news/articles/2014/09/hof/hall-of-fame-main.aspx" title="Hall of Fame" target="_self">Hall of Fame</a></dd>
<dd><a href="http://ironmanfoundation.org" title="Foundation" target="_self">Foundation</a></dd>


</dl>
            <dl class="corporate clear">
<dt>Corporate:</dt><dd><a href="http://www.ironman.com/triathlon/organizations/media.aspx" title="Press Information">Press Information</a></dd>
<dd><a href="http://ironman.teamworkonline.com/teamwork/jobs/default.cfm" title="Jobs">Jobs</a></dd>
<dd></dd>
<dd><a href="http://www.ironman.com/triathlon/legal-faq.aspx" title="Intellectual Property Usage">Intellectual Property Usage</a></dd>
<dd><a href="http://www.ironman.com/triathlon/privacy-policy.aspx" title="Privacy Policy">Privacy Policy</a></dd>
<dd><a href="http://www.ironman.com/triathlon/contact-us.aspx" title="Contact">Contact</a></dd>
<dd><a href="http://feeds.ironman.com/ironman/topstories" title="RSS" target="_blank">RSS</a></dd>
</dl>
            
        </div>
        <div class="footerRight">
            <p class="subscribeLabel">Get News & Race Updates <span>Be the first to know what's happening </br> in the world of IRONMAN.</span></p>
		    <a href="/triathlon/subscribe.aspx" title="Email" class="signupBtn" data-ga-event="newsletterClickThrough">Signup for Email Updates</a>
            <a href="http://www.facebook.com/Ironmantri" title="facebook" target="_blank" class="facebookLink"><span class="ss-icon">&#xF610;</span></a>
            <a href="http://twitter.com/ironmantri" title="Twitter" target="_blank" class="twitterLink"><span class="ss-icon">&#xF611;</span></a>
            <a href="http://www.youtube.com/ironmantriathlon" title="YouTube" target="_blank" class="youtubeLink"><span class="ss-icon">&#xF630;</span></a>
            <a href="http://www.mirumagency.com" target="_blank" class="digiLink">
				<img src="http://www.ironman.com/media/built_by_mirum.png" alt="Site Built By Mirum" width="75" height="33"/>
			</a>
        </div>
    </div>
    <div class="footerCopyright">
        <div class="footerCopyrightInner">
            <p>Copyright&copy; 2001-2016 World Triathlon Corporation (WTC). All Rights Reserved. IRONMAN&reg;, IRONMAN TRIATHLON&reg;, M-DOT&reg;, IRONMANLIVE.com&reg;, IRONMAN.COM&trade; and 70.3&reg; are trademarks of WTC.<br/>Any use of these marks without the express written consent of WTC is prohibited.</p>
        </div>
    </div>
</footer>
<!-- End: Footer -->

<script type="text/javascript">
    jCoreInit("http%3a%2f%2fwww.ironman.com%2ftriathlon%2fprivacy-policy.aspx", "Privacy+Policy", "5880dc60-1640-46b0-89c4-17733fe8b9bd", []);
</script>

<!-- Start IRONMAN Quantcast tag -->
<script type="text/javascript">
    qoptions={
        qacct:"p-3fRk5b9IqK8fo"
    };
</script>
<script type="text/javascript" src="http://edge.quantserve.com/quant.js"></script>
<noscript>
<img src="http://pixel.quantserve.com/pixel/p-3fRk5b9IqK8fo.gif" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
</noscript>
<!-- End IRONMAN Quantcast tag -->
<!-- Begin comScore Tag -->
<!-- <script>
    document.write(unescape("%3Cscript src='" + (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js' %3E%3C/script%3E"));
</script>
<script>
    COMSCORE.beacon({
        c1:2,
        c2:3005619,
        c3:"",
        c4:"",
        c5:"",
        c6:"",
        c15:""
    });
</script>
<noscript>
<img src="http://b.scorecardresearch.com/p?c1=2&c2=3005619&c3=&c4=&c5=&c6=&c15=&cj=1" />
</noscript>-->
<!-- End comScore Tag -->

<script type="text/javascript">tyntVariables = {"ap":"Originally from:"};</script> <script type="text/javascript" src="http://tcr.tynt.com/javascripts/Tracer.js?user=bakJX-LzSr34Enadbi-bpO&b=1"></script>
</body>
</html>
