
<!DOCTYPE html>
<!--[if lt IE 7]>       <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>          <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>          <html class="no-js lt-ie10 lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>          <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->  <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
    <!--[if (IE)&(lt IE 9) ]>
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
	<![endif]-->
	<!--[if (IE)&(gt IE 8) ]>
			<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<![endif]-->

    <meta charset="utf-8">

    <!-- IE9 gets old FE pipeline, concatenating these scripts breaks IE9 -->
    <!--[if !IE]><!--><link rel="stylesheet" href="/site/dist/site-css.min.css"><!--<![endif]-->
    <!--[if (IE 9)|(lt IE 9) ]>
        <link href="/site/styles/generic/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="/site/styles/generic/base.css">
        <link rel="stylesheet" href="/site/dist/site-css-old.min.css">
        <!--<![endif]-->


    <script src="/site/javascript/custom.modernizr.js"></script>

	<link rel="alternate" type="application/rss+xml" title="RSS" href="/rss/news">
    <link rel="search" type="application/opensearchdescription+xml" title="Fordham" href="/site/scripts/opensearch.php">
    <link rel="shortcut icon" type="image/x-icon" href="/site/favicon.ico">

        <link rel="stylesheet" href="/site/styles/standard.css">

    <!-- load fonts for IE9+ and all others - IE8 doesnt like @font-face declrations with selectivizr -->
    <!--[if (!IE)|(gt IE 8) ]><!--><link rel="stylesheet" type="text/css" href="/site/styles/generic/font.css" /><!--<![endif]-->

    <!--[if lt IE 9]>
      <link rel="stylesheet" href="/site/styles/generic/ie8.css">
      <script src="/site/javascript/fordham/html5shiv.js"></script>
      <script src="/site/javascript/fordham/nwmatcher-1.2.5-min.js"></script>
      <script src="/site/javascript/fordham/selectivizr-1.0.3b.js"></script>
      <script src="/site/javascript/fordham/respond.min.js"></script>
    <![endif]-->


    <link rel="stylesheet" href="/site/custom_css/fordham_widg_supp.css">
        <link rel="canonical" href="https://www.fordham.edu/info/21366/policies/8331/privacy_policy/1">
<meta name="google-site-verification" content="fF4NKO9ovCXsPNko7fU-GSgc8gH2fHKzwFZqIZtpsWY" />        <meta name="author" content="Fordham">
        <meta name="generator" content="http://www.jadu.net">
        <meta name="revisit-after" content="2 days">
        <meta name="robots" content="index,follow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<!-- XHTML Template -->

	<meta http-equiv="content-language" content="en" />
	<!-- Dublin Core Metadata -->
	<meta name="DC.creator" lang="en" content="Chen Michele " />
	<meta name="DC.date.created" scheme="DCTERMS.W3CDTF" lang="en" content="2016-01-11" />
	<meta name="DC.date.modified" scheme="DCTERMS.W3CDTF" lang="en" content="2016-01-11" />
	<meta name="DC.date.valid" scheme="DCTERMS.W3CDTF" lang="en" content="1970-01-01" />
	<meta name="DC.date.expired" scheme="DCTERMS.W3CDTF" lang="en" content="1970-01-01" />
	<meta name="DC.description" lang="en" content="Information about Privacy Policy" />
	<meta name="DC.format" lang="en" scheme="DCTERMS.IMT" content="text/html" />
	<meta name="DC.identifier" scheme="DCTERMS.URI" content="https://www.fordham.edu/site/scripts/documents_info.php?categoryID=21366&amp;documentID=8331" />
	<meta name="DC.language" scheme="DCTERMS.ISO639-1" content="en" />
	<meta name="DC.publisher" lang="en" content="Fordham University" />
	<meta name="DC.rights.copyright" lang="en" content="Fordham University" />
	<meta name="DC.title" lang="en" content="Privacy Policy" />
	<meta name="DC.coverage" lang="en" content="UK" />
	<meta name="DC.subject" lang="en" content="Policies" />
        <meta name="description" content="Information about Privacy Policy">
        <meta name="keywords" content="documents, consultations, policies, information, plans, performance, objectives, facts and figures, strategy, Fordham University, Rams, undergraduate, graduate, college, school, higher  education, bachelor, master, mba, business, law, lawyer, social service,   education, religion, Jesuit, scholarship, financial aid, liberal arts">
        <script type="text/javascript" src="/site/javascript/swfobject.js"></script>
        <title>Privacy Policy | Privacy Policy | Fordham</title>
    <!--include files for Cookie Consent Code-->
   <link rel="stylesheet" type="text/css" href="https://assets.fordham.edu/cookieconsent/cookieconsent.min.css" />
<script src="https://assets.fordham.edu/cookieconsent/cookieconsent.min.js"></script>

</head>
    <body class="inner-page">
    <div class="off-canvas-wrap">
<!-- googleoff: index -->
<header class="sticky header">
<a class="skip-nav" href="#main-fordham-content" tabindex="1">Skip to main content</a>
<div class="top-navigation-wrapper">
    <div class="menu-tab">
        <button class="menu-tab__link" data-a11y-toggle="top-navigation">
            <span class="icon icon-chevron-down"></span>
            <span class="menu-tab__label">Navigate Fordham</span>
        </button>
    </div>
    <div id="top-navigation" class="top-navigation">
        <div class="top-navigation__inner">
		<ul  class="top-navigation__list top-navigation__list--secondary" data-nav-menu="audience_menu">
<li  class="top-navigation__item">
		<a href="/homepage/1022/info_for_current_students" class="top-navigation__link">
			Current Students		</a>
</li><li  class="top-navigation__item">
		<a href="/homepage/1035/info_for_faculty_and_staff" class="top-navigation__link">
			Faculty and Staff		</a>
</li><li  class="top-navigation__item">
		<a href="/homepage/1024/info_for_parents" class="top-navigation__link">
			Parents		</a>
</li><li  class="top-navigation__item">
		<a href="http://forever.fordham.edu/" class="top-navigation__link">
			Alumni		</a>
</li>		</ul>
		<ul  class="top-navigation__list top-navigation__list--tertiary" data-nav-menu="verb_menu">
<li  class="top-navigation__item">
		<a href="/info/25446/degrees_and_programs" class="top-navigation__link">
			Find a Degree		</a>
</li><li  class="top-navigation__item">
		<a href="https://411.fordham.edu/" class="top-navigation__link">
			Find People		</a>
</li><li  class="top-navigation__item">
		<a href="/apply" class="top-navigation__link">
			Apply		</a>
</li><li  class="top-navigation__item">
		<a href="/info/20530/visit" class="top-navigation__link">
			Visit		</a>
</li><li  class="top-navigation__item">
		<a href="https://securelb.imodules.com/s/1362/rd/index.aspx?sid=1362&amp;gid=1&amp;pgid=603&amp;cid=1881" class="top-navigation__link">
			Give		</a>
</li>		</ul>
            <ul class="top-navigation__list top-navigation__login">
                <noscript>
                <li class="top-navigation__item"><a class="top-navigation__link" href="https://loginp.fordham.edu/cas/login?service=https%3A%2F%2Fwww.fordham.edu%2Finfo%2F21366%2Fpolicies%2F8331%2Fprivacy_policy" title="Login to My Fordham">Log in to My.Fordham</a></li>
                </noscript>
            </ul>
        </div>
    </div>
    <div class="top-bar universal sticky no-flipper" data-topbar=""><!--
        --><div class="header__logo"><!--
        --><!--
            --><a href="https://www.fordham.edu"><img src="/site/styles/img/logo-universal.png" alt="Fordham"></a><!--
        --><!--
            --></div><div class="top-bar__right-side">		<ul  class="desktop-navigation" data-nav-close-button="off" data-nav-submenu-trigger="hover" data-nav-menu="top_navigation">
<li  data-nav-menuitem="42" class="desktop-navigation__item">
		<a href="/info/20057/about" class="desktop-navigation__link">
			About		</a>
</li><li  data-nav-menuitem="16834" class="desktop-navigation__item">
		<a href="https://www.fordham.edu/info/20001/academics" class="desktop-navigation__link">
			Academics		</a>
</li><li  data-nav-menuitem="44" class="desktop-navigation__item">
		<a href="/info/20000/admissions_and_aid" class="desktop-navigation__link">
			Admissions and Aid		</a>
</li><li  data-nav-menuitem="270562" class="desktop-navigation__item">
		<a href="/info/21405/research" class="desktop-navigation__link">
			Research		</a>
</li><li  data-nav-menuitem="45" class="desktop-navigation__item">
		<a href="/info/20015/student_life" class="desktop-navigation__link">
			Student Life		</a>
</li><li  data-nav-menuitem="46" class="desktop-navigation__item">
		<a href="/info/20071/resources" class="desktop-navigation__link">
			Resources		</a>
</li><li  data-nav-menuitem="40" class="desktop-navigation__item">
		<a href="https://www.fordham.edu/info/21386/athletics" class="desktop-navigation__link">
			Athletics		</a>
</li>		</ul>
<!--
        --></div><!--
        --><div class="top-bar-section">
            <div class="top-bar__toggle-wrapper">
                <a class="top-bar__toggle-item top-bar__toggle-item--hamburger" href="#mobile-navigation" data-a11y-toggle="mobile-navigation" aria-label="Open site navigation menu">
                    <span class="icon icon-menu"></span>
                    <span class="top-bar__toggle-label">Menu</span>
                </a>
                <a class="top-bar__toggle-item toggle-search-site" href="#global-search" aria-label="Open site search">
                    <span class="icon icon-search"></span>
                    <span class="top-bar__toggle-label">Search</span>
                </a>
            </div>
        </div><!--
    --></div>
</div>
<form action="https://www.fordham.edu/_search/s/search.html" method="get" class="search-form" id="global-search">
    <div class="small-12 columns">
        <label for="search-site-hidden" class="visually-hidden">Search this site</label>
        <input type="text" id="search-site-hidden" size="18" maxlength="40" name="query" placeholder="Enter Keywords">
        <input type="hidden" name="collection" value="fordham-search">
        <input type="submit" value="Search">
        <a href="#" class="close-search site" title="Close Search">X</a>
    </div>
</form>
</header>
<!-- googleon: index -->


    <div class="row twelve-hun-max main-wrap">
    <div class="small-12 columns">
    <div class="row">
<!-- googleoff: index -->
        <aside>
        
        
        <div class="grandparentnav">
            <a href="" title="" class="show-for-small in-section">In This Section</a>
		<ul  class="sidenav parentnav" data-nav-menu="left_hand_navigation">
<li  data-nav-active="1" class="childmav">
		<a href="/info/21366/policies">
			Policies		</a>
			<ul><li  data-nav-active="active" class="childmav">
		<span>
			Privacy Policy		</span>
</li></ul>
		</li>		</ul>
        </div>
    <div class="additional-move">
    </div>
        </aside>
<!-- googleon: index -->
    <!-- <div class="main universal" role="main"> -->
    <div class="main universal">
<!-- googleoff: all -->
        <nav class="breadcrumbs">
                <a href="/" rel="home">Home</a><a href="/info/20071/resources">Resources</a><a href="/info/21366/policies">Policies</a><a href="#" class="current">Privacy Policy</a>        </nav>
<!-- googleon: all -->
        <section class="content">
         <h1 class="title"  id="main-fordham-content">Privacy Policy</h1>
            <div class="editor clearfix">
<p>Version 1.1</p>

<h2>Purpose</h2>

<p>The University is committed to protecting and respecting your privacy. The purpose of this policy is to inform you how the University governs the collection and use of information collected on websites and Fordham Digital Properties (FDP) maintained and operated by the University including, but not limited to mobile applications.</p>

<h2>Scope</h2>

<p>This policy, and all policies referenced herein, shall apply to all members of the University community including faculty, students, administrative officials, staff, alumni, authorized guests, delegates, and independent contractors (the &quot;User(s)&quot; or &quot;you&quot;) who use, access, or otherwise employ, locally or remotely, the University&#39;s IT Resources, whether individually controlled, shared, stand-alone, or networked.</p>

<h2>Policy Statement</h2>

<ul>
	<li>The University is not responsible for the content, privacy, or security practices of any non-Fordham Digital Property.</li>
	<li>The University uses reasonable organizational, technical, and administrative measures to protect personal data collected and under our control against loss, misuse, or alteration. However, the University cannot provide an absolute guarantee to the security of any information you transmit through FDP because no method of transmission or electronic storage is 100% secure.</li>
	<li>Fordham collects personally identifiable information (e.g., email address, name, or phone number) which you expressly and voluntarily provide to the University through FDP to register for events and conferences or to join a mailing list.
	<ul>
		<li>The University may use that information to fulfill your request for our information, services or communicate information that may be of interest to you.</li>
	</ul>
	</li>
	<li>The University does not sell, trade, or share your information with third parties unless one or more of the following conditions apply:
	<ul>
		<li>You expressly consent to share the information;</li>
		<li>In response to subpoenas, court orders, as required by applicable law, or other legal processes;</li>
		<li>When a business need arises, the University may share alumni email addresses with service providers acting on the University&#39;s behalf to conduct outreach, development, and advertising campaigns, who have agreed to protect the confidentiality of the data.</li>
		<li>Any alumnus who does not wish to have their email address shared with a service provider should contact <a href="mailto:privacy@fordham.edu">privacy@fordham.edu</a>.</li>
	</ul>
	</li>
	<li>The University limits the information collected to only the minimum information required to complete a visitor&#39;s request. At your request, the University may remove any contact information you provided from its database by contacting <a href="mailto:privacy@fordham.edu">privacy@fordham.edu</a>.</li>
	<li>The University may automatically aggregate visitor information of FDP unless you take steps to browse the internet anonymously.
	<ul>
		<li>Personal data automatically aggregated may be used to improve the content of our websites, analyze trends, investigate security concerns, and identify system performance or problem areas.</li>
	</ul>
	</li>
	<li>The University uses server logs to automatically collect information concerning the internet connection and general information about visits to our websites.</li>
	<li>The information the University collects is based on IP addresses.
	<ul>
		<li>An IP address is the location of a computer or network and is not linked to your specific email address in any of the log reports or other material compiled by the University.</li>
		<li>The University does not associate your IP address with any other information held by Fordham that can specifically identify you unless we are required to do so by law.</li>
	</ul>
	</li>
	<li>The University may utilize online identification technologies such as cookies, web beacons, or log files to automatically track web browsing patterns.
	<ul>
		<li>The University&#39;s use of cookies does not involve the collection of personally identifiable information.</li>
		<li>You may set your browser to prevent the use of cookies; however, doing so may prevent you from using specific functions of our websites.</li>
	</ul>
	</li>
	<li>The University may use web analytics services such as the service currently provided to Fordham by Google <a href="#FN1">[1]</a>, Inc. Google Analytics&trade; and other similar services use cookies to help us analyze how visitors use our sites.
	<ul>
		<li>The information generated by the cookies about your use of the website includes your IP address, and this information is transmitted to and stored by the services.</li>
		<li>The information may be shared with third-parties where required to do so by law, or where such third parties process the information on Google&#39;s or a similar service&#39;s behalf.</li>
		<li>The University does not associate your IP address with any other data held by Google and other similar services. By using this website, you consent to the processing of data about you in this manner and for the purposes set out above.</li>
	</ul>
	</li>
	<li>When you use certain FDP, the University may use location technologies, such as Global Positioning System (GPS), radio-frequency identification (RFID), or near-field communication (NFC) to determine your current location or enable specific automated responses when a mobile device enters or leaves particular areas.
	<ul>
		<li>The University does not share your current location with third-parties unless required by applicable law or other legal processes (e.g., subpoenas or court orders).</li>
		<li>If you do not want to share your location information, you can turn off the location services for the mobile application located in your account settings or your mobile phone settings or within the mobile application. However, doing so may prevent you from using specific functions of the application.</li>
	</ul>
	</li>
	<li>FDP may link to other websites. You are no longer subject to this Privacy Policy when you leave an FDP.</li>
	<li>FDP reference to any specific service, company, or organization does not constitute the University&#39;s endorsement or recommendation.</li>
	<li>The University may contract with third-party vendors to place our advertisements or other communications on non-Fordham websites and digital properties.
	<ul>
		<li>Fordham and the third-party vendors may use cookies and web beacons to target ads or other communications based on your prior visit(s) to FDP and assess the effectiveness of our campaigns.</li>
		<li>If you would like more information about these practices, go to <a href="http://www.aboutads.info/">www.aboutads.info</a> <a href="#FN2">[2]</a>. &nbsp;</li>
	</ul>
	</li>
</ul>

<h2>Definitions</h2>

<p><strong>Cookies and web beacons</strong> are small files that may be transferred to your computer or mobile device through web browsing software to evaluate your use of websites, compiling reports on website activity, and providing other services relating to internet usage.</p>

<p><strong>IT Resources</strong> include computing, networking, communications, application, and telecommunications systems, infrastructure, hardware, software, data, databases, personnel, procedures, physical facilities, cloud-based vendors, Software as a Service (SaaS) vendors, and any related materials and services.</p>

<h2>Related Policies and Procedures</h2>

<ul>
	<li>Logging Standards Policy</li>
	<li>Internet Usage Policy</li>
</ul>

<h2>Implementation Information</h2>

<table>
	<tbody>
		<tr>
			<td>Review Frequency:</td>
			<td>Annual</td>
		</tr>
		<tr>
			<td>Responsible Person:</td>
			<td>Office of Legal Counsel</td>
		</tr>
		<tr>
			<td>Approved By:</td>
			<td>Office of Legal Counsel</td>
		</tr>
		<tr>
			<td>Approval Date:</td>
			<td>May 22, 2018</td>
		</tr>
	</tbody>
</table>

<h2>Revision History</h2>

<table>
	<tbody>
		<tr>
			<td><strong>Version:</strong></td>
			<td><strong>Date:</strong></td>
			<td><strong>Description:</strong></td>
		</tr>
		<tr>
			<td>1.0</td>
			<td>01/11/2016</td>
			<td>Initial Policy</td>
		</tr>
		<tr>
			<td>
			<p>1.1</p>
			</td>
			<td>05/22/2018</td>
			<td>Policy rewritten to meet new data privacy requirements</td>
		</tr>
	</tbody>
</table>

<h2>Policy Disclaimer Statement</h2>

<p>Fordham University retains the right to review this policy periodically and make changes as deemed necessary. Willful failure to adhere to published policy or protocol may be met with University sanctions.</p>

<div>
<hr class="text_align_left" style="width: 33%; size: 1px;" /></div>

<p>[1] Visit the following pages for more information on Google Analytics&#39; <a href="https://www.google.com/intl/en/policies/terms/">terms of use</a> and Google&#39;s <a href="https://www.google.com/policies/privacy/">privacy practices</a>.</p>

<p>[2] To learn about your choices in connection with these practices on the particular device on which you are accessing this policy, please visit <a href="http://www.networkadvertising.org/managing/opt_out.asp%20">http://www.networkadvertising.org/managing/opt_out.asp</a> and <a href="http://www.aboutads.info/">http://www.aboutads.info/</a> to opt out in desktop and mobile web browsers.</p>

                </div>

        </section>
        </div>
        </div>
        </div>
        </div>
<!-- googleoff: index -->
        <footer class="footer">
            <div class="row twelve-hun-max">
                <div class="small-12 columns no-lr-pad-large">
                    <div class="small-12 large-9 column no-l-pad">
                        <div class="small-12 large-5 column no-lr-pad logo"><a href="/" title=""><img src="/site/styles/img/logo-white.png" title="" alt="Fordham University"></a> </div>
                        <div class="small-12 large-8 column slogan"><span>New York is my campus. Fordham is my school.<sup>TM</sup></span></div>
                        <ul class="social inline no-list-style small-12 column no-l-pad">
				<li><a href="/socialmedia" title="Twitter"><em class="fa fa-twitter"></em><span class="hidden">Twitter</span></a></li>
                                <li><a href="/socialmedia" title="Facebook"><em class="fa fa-facebook"></em><span class="hidden">Facebook</span></a></li>
                                <li><a href="/socialmedia" title="Linkedin"><em class="fa fa-linkedin"></em><span class="hidden">Linkedin</span></a></li>
                                <li><a href="/socialmedia" title="Youtube"><em class="fa fa-youtube"></em><span class="hidden">Youtube</span></a></li>
                                <li><a href="/socialmedia" title="Instagram"><em class="fa fa-instagram"></em><span class="hidden">Instagram</span></a></li>
                        </ul>
                    </div>
                    <div class="small-12 medium-12 large-3 max-250 column list-links">
                        <a href="/contact_us" title="Contact Fordham University">Contact Us</a>
                        <a href="/maps_and_directions" title="Fordham Maps and Directions">Maps and Directions</a>
                        <a href="/info/22823/discrimination" title="Fordham Nondiscrimination Policy">Nondiscrimination Policy</a>
                    </div>
                </div>

            </div>
            <div class="copyright show-for-small">&copy; 2018 Fordham University</div>
           <!-- <a href="#top" class="back-to-top"><em class="fa fa-angle-up"></em></a> -->
        </footer>
<!-- googleon: index -->
    </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="/site/javascript/fordham/foundation.custom.js"></script>
        <script src="/site/javascript/almond.min.js"></script>
        <script src="/site/javascript/util.min.js"></script>
	<script type="text/javascript" src="/site/custom_scripts/jadu.nav.Menus.js"></script>
        <script src="/site/dist/site.min.js"></script>
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NGK35S "height="0" width="0" style="display:none;visibility:hidden" title="empty" role="presentation" tabindex="-1"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-NGK35S');</script><script type="text/javascript">/*<![CDATA[*/(function() {var sz = document.createElement('script'); sz.type = 'text/javascript'; sz.async = true;sz.src = '//us2.siteimprove.com/js/siteanalyze_66356605.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sz, s);})();/*]]>*/</script><script type="text/javascript">/* <![CDATA[ */var google_conversion_id = 1050634373;var google_custom_params = window.google_tag_params;var google_remarketing_only = true;/* ]]> */</script><script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script><noscript><div style="display:inline;"><img height="1" width="1" style="border­style:none;" alt=""src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1050634373/?value=0&guid=ON&script=0"/></div></noscript>
    </body>
</html>