<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <title>AusGamers Privacy Policy - AusGamers.com</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href='//fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Archivo+Narrow:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="/res/css/stylesheet.3.7.8.min.css" />
<link rel="stylesheet" type="text/css" href="/res/css/ausgamers.3.7.8.min.css" />
<link rel="stylesheet" type="text/css" href="/res/css/legacy.3.7.8.min.css" />

<link rel="stylesheet" type="text/css" href="/server-rentals/css/stylesheet.css" />

<script type="text/javascript" src="/res/static/javascript/jquery-1.11.1.min.js"></script>
<script>jQuery.noConflict();</script>

<link rel="publisher" href="https://plus.google.com/108853910544340068267/" />
<link rel="search" type="application/opensearchdescription+xml" href="/search/osdd.xml" />
<link rel="shortcut icon" href="/res/static/images/ag-logo-new-32x32_3.png">

<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type='text/javascript'>
googletag.cmd.push(function() {
googletag.pubads().collapseEmptyDivs(true);
var skinMapping = googletag.sizeMapping().addSize([970,0],[[1290,800],[1902,1053]]).build();
var leaderboardMapping = googletag.sizeMapping().addSize([768,0],[728,90]).addSize([0,0],[300,250]).build();
var rectMapping = googletag.sizeMapping().addSize([1290,0],[300,250]).addSize([960,0],[300,250]).addSize([0,0],[300,250]).build();
var resizeTimer;
function resizer() {
    googletag.pubads().refresh(window.responsive_ads);
}
if ('undefined' != typeof window.innerWidth && 'undefined' != typeof Array.prototype.forEach)
{
    window.breakPoints = [0,768,970];
    window.breakPoints.forEach(function(value) {
        if(window.innerWidth >= value) {
            window.breakPoint = value;
        }
    });
    jQuery(window).on('resize', function()
    {
		var deviceWidth = window.innerWidth;
        var bp = 0;
        window.breakPoints.forEach(function(value) {
            if(deviceWidth >= value) {
                bp = value;
            }
        });
        if (bp != window.breakPoint) {
            window.breakPoint = bp;
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(resizer, 250);
        }
    });
}


window.header_leaderboard = googletag.defineSlot('/68110269/AG_Header_Leaderboard', [[300, 250], [728, 90]], 'div-gpt-ad-1409576370107-2').addService(googletag.pubads()).defineSizeMapping(leaderboardMapping);
window.footer_leaderboard = googletag.defineSlot('/68110269/AG_Footer_Leaderboard', [[300, 250], [728, 90]], 'div-gpt-ad-1409576370107-0').addService(googletag.pubads()).defineSizeMapping(leaderboardMapping);
window.general_rect = googletag.defineSlot('/68110269/AG_General_Rect', [300, 250], 'div-gpt-ad-1409576370107-1').addService(googletag.pubads());
window.sidebar_rect = googletag.defineSlot('/68110269/AG_Sidebar_Rect', [300, 250], 'div-gpt-ad-1409576370107-8').addService(googletag.pubads()).defineSizeMapping(rectMapping);;
window.skin = googletag.defineSlot('/68110269/AG_Skin_Full', [[1902,1053],[1290, 800],[1400, 800]], 'div-gpt-ad-1409838830137-0').addService(googletag.pubads()).defineSizeMapping(skinMapping);
window.responsive_ads = [window.header_leaderboard, window.footer_leaderboard, window.skin];



            googletag.defineSlot('/68110269/AG_OTP', [500, 250], 'div-gpt-ad-1414477457716-1').addService(googletag.pubads());
            

googletag.pubads().enableSingleRequest();
googletag.enableServices();
});
</script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<meta name="title" content="">
<meta name="description" content="AusGamers - Australia's largest online gaming resource! All the latest news, demos and files, as well as an active community and plenty of free services!">
<meta name="robots" content="">
<meta name="keywords" content="files, game files, gaming files, game patches, counterstrike patches, counter-strike patches, counter-strike, half-life, game demos, demos, game upgrades, gaming files, free demos, online gaming, australian gaming, computer gaming, australian computer gaming, free downloads, free demos, free files">
</head>
<body class="ag anonymous">
<noscript>
    <div class="WarningMessage">
        We have detected that you do not currently have JavaScript enabled.<br/><br/>
        AusGamers requires that you enable JavaScript to use this site correctly.
    </div>
</noscript>
<header id="page-header" >
    <div id="main-nav-container">
        <nav id="main-nav">
            <button id="toggle-nav"><i class="fa fa-bars"></i></button>
            <a href="http://www.ausgamers.com" class="brand">
                <div class="logo"></div>
                <span class="text">AusGamers</span>
            </a>
            <ul class="nav">
                                <li><a href="/news/" >News</a>
                                    </li>
                                <li><a href="/reviews/" >Reviews</a>
                                    </li>
                                <li><a href="/features/" >Features</a>
                                    </li>
                                <li><a href="/esports/" >eSports</a>
                                    </li>
                                <li><a href="/files" >Downloads</a>
                                    </li>
                                <li><a href="/forums/" >Forums</a>
                                    </li>
                                <li id="nav-search">
                    <form method="get" class="searchbox" action="/search/">
                        <input name="search" type="search" placeholder="search" class="search search-input"><button><i class="fa fa-search search-icon"></i></button>
                    </form>
                </li>
            </ul>
        </nav>
    </div>

    <div class="top">
                <div class="headerAd">
                <div id='div-gpt-ad-1409576370107-2'>
                <script type='text/javascript'>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1409576370107-2'); });
                </script>
                </div></div>
    </div>

    <div class="infobar">
        <ul class="right ul logged-out">
    <li class="logout">
        <a class="login-button" href="/account/login.php" data-modal-href=">" >
            <i class="fa fa-lg fa-sign-in"></i> Sign In
        </a>
    </li>
    <li class="register">
        <a class="register-button" href="/account/join.php" data-modal-href=">">
            <i class="fa fa-lg fa-user"></i> Register
        </a>
    </li>
</ul><ul class="left ul breadcrumbs">
    <li><a href="http://www.ausgamers.com"><i class="fa fa-lg fa-home"></i> AusGamers</a></li>
    <li>&raquo; <a href="http://www.ausgamers.com">Privacy Policy</a></li>
    </ul>
    </div>

</header><div id="page" >
<div id="topFeatures">
<div class="feature">
    <div class="feature-block">
        <img src="https://www.ausgamers.com/gameres/7121/images/695/TCTD2_Keyart_Gold_GC_180821_930am_CET_1534794281.jpg" class="content center-h">
        <div class="overlay hover">
            Where we talk about the three Cs: the camera, the control and the combat.         </div>
        <a href="https://www.ausgamers.com/features/read/3613595" class="link"></a>
    </div>
    <a href="https://www.ausgamers.com/features/read/3613595" class="title">We Chat to The Division 2's Game Director David Kennedy</a>
</div>
<div class="feature">
    <div class="feature-block">
        <img src="https://www.ausgamers.com/gameres/7261/images/695/crackdown3review009.jpg" class="content center-h">
        <div class="overlay hover">
            An explosion several years in the making.        </div>
        <a href="https://www.ausgamers.com/games/crackdown-3/review/" class="link"></a>
    </div>
    <a href="https://www.ausgamers.com/games/crackdown-3/review/" class="title">Crackdown 3 Review - Boom! Shake The Room</a>
</div>
<div class="feature">
    <div class="feature-block">
        <img src="https://www.ausgamers.com/gameres/7253/images/695/metorexodusreview019.jpg" class="content center-h">
        <div class="overlay hover">
            An astonishing achievement, this post-apocalyptic shooter offers an experience that lives up to its ambition and promise, offering challenge and surprise in equal measure.         </div>
        <a href="https://www.ausgamers.com/games/metro-exodus/review/" class="link"></a>
    </div>
    <a href="https://www.ausgamers.com/games/metro-exodus/review/" class="title">Metro Exodus Review - You Won't Fall Out of This Carriage</a>
</div>
<div class="feature">
    <div class="feature-block">
        <img src="https://www.ausgamers.com/gameres/7245/images/695/FCND_screen_preview_StealthTakedown_190123_6pmCET_1547798678.jpg" class="content center-h">
        <div class="overlay hover">
            We play through the neon colours of Far Cry New Dawn, the direct standalone sequel to the events of Far Cry 5        </div>
        <a href="https://www.ausgamers.com/games/far-cry-new-dawn/review/" class="link"></a>
    </div>
    <a href="https://www.ausgamers.com/games/far-cry-new-dawn/review/" class="title">Far Cry New Dawn Review - A New Dawn, A Nuke Day</a>
</div>
</div><div class="main">
    <style type="text/css">

H1	{
	margin-bottom:10px;
}
H2	{
	margin-bottom:0px;
	Font-size:15px;
}

H3	{
	font-style:italic;
	font-weight:bold;
	font-size:14px;
}

P	{
margin-top:5px;
}
</style>
<H1>Privacy Policy</H1>
<H3>AusGamers Pty Ltd</H3>

<H2>General</H2>
<P>AusGamers Pty Ltd ('AusGamers') is committed to protecting the personal information it collects from you.  However, AusGamers is not required to comply with the provisions of the Privacy Act 1988 (Cth) and while not purporting to be a statement of compliance with that Act, this document sets out how AusGamers will manage your personal information.</P>

<h2>Collection</H2>
<P>AusGamers may collect and hold personal information about you, including your name, gender, address, phone number, email address, bank account details, credit card details or other information relevant to providing you with the services you are seeking.</P>

<P>The personal information that AusGamers collects and holds about you, depends on your interaction with AusGamers.  Generally, AusGamers will collect and hold your personal information for the purpose of:</P>

<UL>
 <LI>providing AusGamers's services to you;
 <LI>providing you with information about other services that AusGamers offers that may be of interest to you;
 <LI>facilitating AusGamers's internal business operations, including the fulfilment of any legal requirements; and
 <LI>analysing AusGamers's services and customer needs with a view to developing new or improved services.
</UL>

<P>Personal information will generally be collected by AusGamers directly from you through standard application and registration forms or your other interactions with AusGamers.  There may, however, be some instances where personal information about you will be collected indirectly because it is unreasonable or impractical to collect personal information directly from you.  We will usually notify you about these instances in advance, or where that is not possible, as soon as reasonably practicable after the information has been collected.</P>
<P>If you access AusGamers's web site, AusGamers may collect additional personal information about you in the form of your IP address (that is, the single numerical address for your computer on the internet, which consists of four consecutive numbers ranging between 0 and 255) or domain name (that is, the textual address for your location on the internet which corresponds to your IP address, which the internet computers can read).</P>
<P>Also, AusGamers's web site uses cookies.  A cookie is a message given by a web server (that is, a computer that delivers or serves up pages on the world wide web such as www.AusGamers.com.au) to a web browser (that is, software, such as Microsoft Internet Explorer, used to locate and display pages on the world wide web).  The web browser stores the message in a text file.  The message is then sent back to the web server each time the web browser requests a page from the web server.</P>
<P>The main purpose of cookies is to identify users and to prepare customised web pages for them.  When you enter a web site using cookies, you may be asked to fill out a form providing such information as your name and interests.  This information is packaged into a cookie and sent to your web browser, which stores it for later use.  The next time you go to the same web site, your web browser will send the cookie to the web server.  The web server can then use this information to present you with customised web pages.  So, for example, instead of seeing just a generic welcome page, you might see a welcome page with your name on it.</P>
<P>Cookies do not identify you personally, but they may link back to a database record about you.  If you register on AusGamers's web site, AusGamers will then link your cookie back to your personal information details.</P>
<P>AusGamers uses cookies to monitor usage of AusGamers's web site and to create a personal record of when you visit AusGamers's web site and what pages you view so that AusGamers may serve you more effectively.</P>
<P>If the personal information you provide to AusGamers is incomplete or inaccurate, AusGamers may be unable to administer your investments smoothly or provide you with the services you are seeking.</P>

<h2>Use and disclosure</H2>
<P>Generally, AusGamers only uses or discloses personal information about you for the purposes for which it was collected (as set out above).  However, AusGamers may also disclose personal information about you to:</P>
<UL>
 <LI>financial institutions, who process any payments you make to AusGamers; and
 <LI>service providers, who assist AusGamers in operating its business, however, these service providers are required to comply with AusGamers's privacy policy in how they handle your personal information.
</UL>

<h2>Security</H2>
<P>AusGamers may store your personal information in different ways, including in paper and in electronic form.  The security of your personal information is important to AusGamers.  AusGamers takes all reasonable measures to ensure that your personal information is stored safely to protect it from misuse, loss, unauthorised access, modification or disclosure, including electronic and physical security measures.</P>

<h2>Access</H2>
You may access personal information AusGamers holds about you, upon making a written request.  AusGamers will endeavour to acknowledge your request within 14 days of its receipt and to provide you with access to the information requested within 30 days.  AusGamers may charge you a reasonable fee for processing your request.
<P>AusGamers may decline a request for access to personal information in the limited circumstances permitted by the Privacy Act.</P>
<P>If, upon receiving access to your personal information or at any other time, you believe the personal information AusGamers holds about you is inaccurate, incomplete or out of date, please notify AusGamers immediately.  AusGamers will take reasonable steps to correct the information so that it is accurate, complete and up to date.  If AusGamers disagrees with you about whether your personal information is inaccurate, incomplete or out of date, and you ask AusGamers to associate with your personal information a statement claiming that your personal information is inaccurate, incomplete or out of date, AusGamers will take reasonable steps to do so.</P>

<H2>Links to other web sites</H2>
<P>AusGamers's web site may contain links to other web sites.  AusGamers is not responsible for the privacy practices of linked web sites and so linked web sites are not subject to AusGamers's privacy policies and proceay in which AusGamers handles your personal information, please contact AusGamers's privacy officer at:
<BR><BR>Street address:	Suite S04/34 Coonan Street, Indooroopilly, Queensland,Australia, 4068 
<BR>Postal address:	Suite S04/34 Coonan Street, Indooroopilly, Queensland,Australia, 4068 
<BR>Email address:	contact AT ausgamers [dot] com
<BR>Telephone:	+61-7-3721-5000 
<BR>Facsimile:	+61-7-3721-5001
</P>

<H2>More information</H2>
<P>For more information about privacy in general, you can visit the Federal Privacy Commissioner's web site 
at <a href="http://www.privacy.gov.au">http://www.privacy.gov.au</a>.</P>

<h2>Advertising</h2>
<p>
Cookies are used by Third Party vendors to serve ads on our website, including Google AdSense and DoubleClick.
<br>You can opt out interest-based ads served from Google by visiting <a href="http://www.google.com/ads/preferences/">http://www.google.com/ads/preferences/</a>.
</p></div>
<div class="sidebar">
    		<div id="" style="  ">
			<div id="GreenModule" style="  ">
				<div class="Header" title="Follow Us">
										<span class="agn-header gray">Follow Us</span>
														</div>
				<div class="Content" style=" padding:6px 0px 6px 3px;">
					                <div class="follow-us">
        <ul class="social-tiles">
            <li><a href="http://www.facebook.com/ausgamers" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="http://twitter.com/ausgamers" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://plus.google.com/+ausgamers/posts" class="google-plus"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="http://www.ausgamers.com/rss/news.php" class="rss-feed"><i class="fa fa-rss"></i></a></li>
        </ul>
        </div>
						</div>
			</div>
		</div>
			<div id="" style="  ">
			<div id="GreenModule" style="  ">
				<div class="Header" title="Latest Reviews">
					<a href="/reviews/index.php">					<span class="agn-header gray">Latest Reviews</span>
										</a>				</div>
				<div class="Content" style=" ">
					                <ul class="mediabox-list" style="font-size: 16px;">
                                        <li class="mediabox review">
                        <div class="feature-block thumb">
                            <img src="http://www.ausgamers.com/gameres/7261/images/l-portrait.jpg" class="content center-h">
                            <a href="http://www.ausgamers.com/reviews/read.php/3613747" class="link"></a>
                        </div>
                        <div class="info">
                            <a href="http://www.ausgamers.com/reviews/read.php/3613747" class="title">Crackdown 3</a>
                        </div>
                    </li>
                                        <li class="mediabox review">
                        <div class="feature-block thumb">
                            <img src="https://www.ausgamers.com/gameres/7245/images/l-portrait.jpg?1550142178" class="content center-h">
                            <a href="http://www.ausgamers.com/reviews/read.php/3613575" class="link"></a>
                        </div>
                        <div class="info">
                            <a href="http://www.ausgamers.com/reviews/read.php/3613575" class="title">Far Cry New Dawn</a>
                        </div>
                    </li>
                                        <li class="mediabox review">
                        <div class="feature-block thumb">
                            <img src="http://www.ausgamers.com/gameres/7253/images/l-portrait.jpg" class="content center-h">
                            <a href="http://www.ausgamers.com/reviews/read.php/3613539" class="link"></a>
                        </div>
                        <div class="info">
                            <a href="http://www.ausgamers.com/reviews/read.php/3613539" class="title">Metro Exodus</a>
                        </div>
                    </li>
                                        <li class="mediabox review">
                        <div class="feature-block thumb">
                            <img src="http://www.ausgamers.com/gameres/7259/images/l-portrait.jpg" class="content center-h">
                            <a href="http://www.ausgamers.com/reviews/read.php/3613489" class="link"></a>
                        </div>
                        <div class="info">
                            <a href="http://www.ausgamers.com/reviews/read.php/3613489" class="title">Wargroove</a>
                        </div>
                    </li>
                                    </ul>
                <div class='More'><a href='/reviews/'>More...</a></div>				</div>
			</div>
		</div>
			<div id="" style="  ">
			<div id="GreenModule" style="  ">
				<div class="Header" title="News Feed">
					<a href="/news/index.php">					<span class="agn-header gray">News Feed</span>
										</a>				</div>
				<div class="Content" style=" ">
					<ul class="mediabox-list">
                        <li class="mediabox news">
                            <div class="feature-block thumb">
                                <img src="http://www.ausgamers.com/gameres/7237/images/695/quake2raytracing.jpg" class="content center-v">
                            </div>
                            <div class="info">
                                <a href="http://www.ausgamers.com/news/read/3612799/quake-ii-mod-makes-full-use-of-nvidia-rtx-2080-tis-ray-tracing-features" class="title">Quake II Mod Makes Full Use of NVIDIA RTX 2080 Ti's Ray-Tracing Features</a>
                            </div>
                        </li>
                        

                        <li class="mediabox news">
                            <div class="feature-block thumb">
                                <img src="http://www.ausgamers.com/gameres/7027/images/695/BigBashBoom_Console_Maxwell.jpg" class="content center-v">
                            </div>
                            <div class="info">
                                <a href="http://www.ausgamers.com/news/read/3611707/arcade-cricket-game-big-bash-boom-out-now-for-pc-and-consoles" class="title">Arcade Cricket Game Big Bash Boom Out Now for PC and Consoles</a>
                            </div>
                        </li>
                        

                        <li class="mediabox news">
                            <div class="feature-block thumb">
                                <img src="http://www.ausgamers.com/gameres/7027/images/695/dota_2.jpg" class="content center-v">
                            </div>
                            <div class="info">
                                <a href="http://www.ausgamers.com/news/read/3612397/valve-reveals-steams-best-of-2018-most-played-top-sellers-top-new-releases-and-more" class="title">Valve Reveals Steam's Best of 2018: Most Played, Top Sellers, Top New Releases and More</a>
                            </div>
                        </li>
                        

                        <li class="mediabox news">
                            <div class="feature-block thumb">
                                <img src="https://www.ausgamers.com/gameres/7027/images/695/intel-gpu.jpg" class="content center-v">
                            </div>
                            <div class="info">
                                <a href="http://www.ausgamers.com/news/read/3613261/intel-teases-its-discrete-graphics-card-in-new-video" class="title">Intel Teases its Discrete Graphics Card in New Video</a>
                            </div>
                        </li>
                        

                        <li class="mediabox news">
                            <div class="feature-block thumb">
                                <img src="http://www.ausgamers.com/gameres/7119/images/695/fallout76beta019.jpg" class="content center-v">
                            </div>
                            <div class="info">
                                <a href="http://www.ausgamers.com/news/read/3612879/bethesda-responds-to-the-non-rumour-that-fallout-76-is-going-free-to-play" class="title">Bethesda Responds to the Non-Rumour that Fallout 76 is Going Free-to-Play</a>
                            </div>
                        </li>
                        
</ul>				</div>
			</div>
		</div>
			<div id="" style="  ">
			<div id="GreenModule" style="  ">
				<div class="Header" title="Advertisement">
										<span class="agn-header gray">Advertisement</span>
														</div>
				<div class="Content" style=" ">
					        <div class="sidebar-ad" style="margin-bottom: 18px;">
            
            <div id='div-gpt-ad-1409576370107-8' style='display: inline-block;'>
            <script type='text/javascript'>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1409576370107-8'); });
            </script>
            </div>        </div>
        				</div>
			</div>
		</div>
			<div id="" style="  ">
			<div id="GreenModule" style="  ">
				<div class="Header" title="Latest Threads">
					<a href="/forums/general/index.php">					<span class="agn-header gray">Latest Threads</span>
										</a>				</div>
				<div class="Content" style=" ">
					        <ul class="mediabox-list">
                        <li class="mediabox forums">
                <div class="feature-block thumb">
                                    </div>
                <div class="info">
                    <a class="title" href="http://www.ausgamers.com/forums/general/thread.php/3597845?#lastpost">Political Thread 3</a>
                    <br>
                    <span class="author">Vash</span> <time class="time">33 minutes ago</time>
                </div>
            </li>
                        <li class="mediabox forums">
                <div class="feature-block thumb">
                                    </div>
                <div class="info">
                    <a class="title" href="http://www.ausgamers.com/forums/general/thread.php/3613909?#lastpost">Reggie out Bowser in - Nintendo NA president retires :(</a>
                    <br>
                    <span class="author">notgreazy</span> <time class="time">7 hours ago</time>
                </div>
            </li>
                        <li class="mediabox forums">
                <div class="feature-block thumb">
                                    </div>
                <div class="info">
                    <a class="title" href="http://www.ausgamers.com/forums/general/thread.php/3613791?#lastpost">Nothing on this site about Tarkov</a>
                    <br>
                    <span class="author">Vash</span> <time class="time">1 day ago</time>
                </div>
            </li>
                        <li class="mediabox forums">
                <div class="feature-block thumb">
                                        <img src="//www.ausgamers.com/images/avatars/3740.png" class="content center-h">
                                    </div>
                <div class="info">
                    <a class="title" href="http://www.ausgamers.com/forums/general/thread.php/3612957?#lastpost">Long time no see! </a>
                    <br>
                    <span class="author">Hogfather</span> <time class="time">2 days ago</time>
                </div>
            </li>
                        <li class="mediabox forums">
                <div class="feature-block thumb">
                                    </div>
                <div class="info">
                    <a class="title" href="http://www.ausgamers.com/forums/general/thread.php/3613171?#lastpost">Veganism</a>
                    <br>
                    <span class="author">Ickus</span> <time class="time">4 days ago</time>
                </div>
            </li>
                    </ul>
        				</div>
			</div>
		</div>
			<div id="" style="  ">
			<div id="GreenModule" style="  ">
				<div class="Header" title="Log In">
										<span class="agn-header gray">Log In</span>
														</div>
				<div class="Content" style=" ">
							<a link="loginbox"></a>
					<form method="post" action="https://www.ausgamers.com/account/login.php">
				<input type="hidden" name="goto_url" value="/privacy.php">
				<input type="hidden" name="goto_vars" value="">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr><td><b>User:&nbsp;&nbsp;</b></td><td><input type="text" name="login_username" style="margin-bottom:6px;" /></td></tr>
					<tr><td><b>Pass:&nbsp;&nbsp;</b></td><td><input type="password" name="login_password" style="margin-bottom:4px;" /></td></tr>
					<tr><td></td><td><input type='submit' value="LOGIN" class="ButtonLink"   style="color:#000000;" /></td></tr>
				</table>
				</form>
				<div style="line-height:14px;">
					<div style="padding-top:5px;"><a href="/account/join.php">Create Account</a> | <a href="/account/forgotten.php">Lost your password?</a></div>
				</div>
						</div>
			</div>
		</div>
	</div>
<div id="bottom">
        <div class="bottom-ad">
        
                <div id='div-gpt-ad-1409576370107-0'>
                <script type='text/javascript'>
                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1409576370107-0'); });
                </script>
                </div>    </div>
    
    <div class="top-content">

    <div class="section news">
        <h2 class="section-title">News</h2>

        <div class="main-feature">
            <div class="feature-block ratio16x9">
                <img src="https://www.ausgamers.com/gameres/7027/images/695/dota_2.jpg" class="content center-h">
                <a href="http://www.ausgamers.com/news/read/3612397/valve-reveals-steams-best-of-2018-most-played-top-sellers-top-new-releases-and-more" class="link"></a>
            </div>
            <a href="http://www.ausgamers.com/news/read/3612397/valve-reveals-steams-best-of-2018-most-played-top-sellers-top-new-releases-and-more"><span class="title">Valve Reveals Steam's Best of 2018: Most Played, Top Sellers, Top New Releases and More</span></a>
        </div>

        <ul class="mediabox-list">
                            <li class="mediabox feature">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com/gameres/7237/images/695/quake2raytracing.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/news/read/3612799/quake-ii-mod-makes-full-use-of-nvidia-rtx-2080-tis-ray-tracing-features" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/news/read/3612799/quake-ii-mod-makes-full-use-of-nvidia-rtx-2080-tis-ray-tracing-features" class="title">Quake II Mod Makes Full Use of NVIDIA RTX 2080 Ti's Ray-Tracing Features</a><br>
                    </div>
                </li>
                            <li class="mediabox feature">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com/gameres/7027/images/695/BigBashBoom_Console_Maxwell.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/news/read/3611707/arcade-cricket-game-big-bash-boom-out-now-for-pc-and-consoles" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/news/read/3611707/arcade-cricket-game-big-bash-boom-out-now-for-pc-and-consoles" class="title">Arcade Cricket Game Big Bash Boom Out Now for PC and Consoles</a><br>
                    </div>
                </li>
                            <li class="mediabox feature">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com/gameres/7027/images/695/intel-gpu.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/news/read/3613261/intel-teases-its-discrete-graphics-card-in-new-video" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/news/read/3613261/intel-teases-its-discrete-graphics-card-in-new-video" class="title">Intel Teases its Discrete Graphics Card in New Video</a><br>
                    </div>
                </li>
                    </ul>
    </div>



        <div class="section features">
            <h2 class="section-title">Features</h2>

            <div class="main-feature">
                <div class="feature-block ratio16x9">
                    <img src="https://www.ausgamers.com/gameres/7253/images/695/metorexodusreview021.jpg" class="content center-h">
                    <a href="http://www.ausgamers.com/features/read/3613731" class="link"></a>
                </div>
                <a href="http://www.ausgamers.com/features/read/3613731">
                    <span class="title">Metro Exodus and Real-Time Ray-Tracing - A Glimpse Into the Future</span>
                </a>
            </div>

            <ul class="mediabox-list">
                                <li class="mediabox feature">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com/gameres/7121/images/695/TCTD2_Keyart_Gold_GC_180821_930am_CET_1534794281.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/features/read/3613595" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/features/read/3613595" class="title">Interview: Tom Clancy's The Division 2's Game Director, David Kennedy, Tells All</a><br>
                    </div>
                </li>
                                <li class="mediabox feature">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com/gameres/7121/images/695/Tom_Clancy%E2%80%99s_The_Division_2_-_Private_Beta_2.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/features/read/3613441" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/features/read/3613441" class="title">Rebuilding the Endgame – Hands-On with The Division 2’s Private Beta </a><br>
                    </div>
                </li>
                                <li class="mediabox feature">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com/gameres/6919/images/695/reddead2wildlife005.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/features/read/3613379" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/features/read/3613379" class="title">Practice Makes Perfect - Levelling Up Skills and Keeping Fit Both In-Game and Out</a><br>
                    </div>
                </li>
                            </ul>

        </div><!-- /.news -->

        <div class="section reviews" style="margin-right: 0;">
            <h2 class="section-title">Reviews</h2>
                        <div class="main-feature">
                <div class="feature-block ratio16x9">
                    <img src="https://www.ausgamers.com//gameres/7261/images/695/crackdown3review006.jpg" class="content center-h">
                    <a href="http://www.ausgamers.com/reviews/read.php/3613747" class="link"></a>
                </div>
                <a href="http://www.ausgamers.com/reviews/read.php/3613747">
                    <h3 class="title">Crackdown 3</h3>
                </a>
            </div>
            
            <ul class="mediabox-list">
                <li class="mediabox review">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com//gameres/7245/images/695/14-02-2019_8-06-07_PM-ebnezlx1.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/reviews/read.php/3613575" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/reviews/read.php/3613575" class="title">Far Cry New Dawn</a><br>
                    </div>
                </li>
                <li class="mediabox review">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com//gameres/7253/images/695/metorexodusreview004.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/reviews/read.php/3613539" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/reviews/read.php/3613539" class="title">Metro Exodus</a><br>
                    </div>
                </li>
                <li class="mediabox review">
                    <div class="feature-block thumb">
                        <img src="https://www.ausgamers.com//gameres/7259/images/695/wargroovereview001.jpg" class="content center-v">
                        <a href="http://www.ausgamers.com/reviews/read.php/3613489" class="link"></a>
                    </div>
                    <div class="info">
                        <a href="http://www.ausgamers.com/reviews/read.php/3613489" class="title">Wargroove</a><br>
                    </div>
                </li>
            </ul>
        </div><!-- /.reviews -->

    </div>
</div></div>
<footer id="page-footer">
    <div class="footer-links">
        <div class="link-block">
            <ul>
                <li><b>Content</b></li>
                <li><a href="/news/">News</a></li>
                <li><a href="/reviews/">Reviews</a></li>
                <li><a href="/features/">Features</a></li>
                <li><a href="/files">Downloads</a></li>
            </ul>
        </div>
        <div class="link-block">
            <ul>
                <li><b>Forums</b></li>
                <li><a href="/forums">Forums</a></li>
                <li><a href="/forums/general/index.php">General Chat</a></li>
                <li><a href="/forums/games/index.php">Games Discussion</a></li>
                <li><a href="/forums/media/index.php">Movies, TV and Music</a></li>
                <li><a href="/forums/hardware/index.php">Hardware</a></li>
            </ul>
        </div>
        <div class="link-block">
            <ul>
                <li><b>Server Rentals</b></li>
                <li><a href="/server-rentals">Rent a Server</a></li>
                <li><a href="/server-rentals/help.php">Help & FAQs</a></li>
                <li><a href="/forums/gsr/index.php">Community Forum</a></li>
                <li><a href="/contact/?info=Rentals">Contact Support</a></li>
            </ul>
        </div>
        <div class="link-block">
            <ul>
                <li><b>Social</b></li>
                <li><a href="http://facebook.com/ausgamers">Facebook</a></li>
                <li><a href="http://twitter.com/ausgamers">Twitter</a></li>
                <li><a href="http://plus.google.com/+ausgamers">Google+</a></li>
                <li><a href="/rss/news.php">RSS Feed</a></li>
            </ul>
        </div>
    </div>
    <div class="footer-bot">
        <ul class="links-list">
            <li><a href="/about">About Us</a></li>
            <li><a href="/about/staff">Staff</a></li>
            <li><a href="/contact">Contact Us</a></li>
            <li><a href="/contact/advertise">Advertise with Us</a></li>
            <li><a href="/privacy.php">Privacy Policy</a></li>
        </ul>
        <div class="copyright">
            <img src="//www.ausgamers.com/res/static/images/ag-logo-new-shade.png" style="height: 32px; float: left; margin-right: 5px;">
            &copy; 2001-2019 AusGamers&trade; Pty Ltd
            <br>
            ACN 093 772 242
        </div>
    </div>
</footer>
<script type="text/javascript" src="/res/js/ausgamers.jquery.js"></script>
<link rel="stylesheet" href="/res/css/font-awesome.4.1.0.min.css">
<script type="text/javascript" src="/res/static/javascript/moo.1.8.1.js"></script>
<script type="text/javascript" src="/res/static/javascript/medialoader.1.9.4.1.js"></script>
<script type="text/javascript" src="/res/static/javascript/functions.1.9.4.1.js"></script>
<script type="text/javascript" src="/res/static/javascript/swfobject.1.9.4.1.js"></script>
<script type="text/javascript" src="/res/static/javascript/flv.1.9.4.1.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="/res/js/ie.js"></script>
<![endif]-->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-197654-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<!-- BEGIN CPMSTAR TWO-SIDED ASYNCHRONOUS SITESKIN CODE 20130626 -->
<script type="text/javascript">
var cpmstar_siteskin_settings = {
pid: 60247, // poolid
centerWidth: "", // page width
fixed: "1",
topPos: "158px",
zIndex: 1
};
if ("undefined" != typeof window.innerWidth && "undefined" != typeof window.breakPoint)
{
    console.log("windowBP", window.breakPoint);
	switch(window.breakPoint) {
		case 0:
		case 768:
		break;

		case 970:
			cpmstar_siteskin_settings.centerWidth = "970px";
		break;
	}
}
if ("" != cpmstar_siteskin_settings.centerWidth) {
(function(){var t=document.createElement("script");t.type="text/javascript";t.async=1;t.src=(document.location.protocol == "https:"?"//server":"//cdn") + ".cpmstar.com/cached/js/siteskin_v100.pack.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(t,s);})();
}
</script>
<!-- END CPMSTAR SITESKIN CODE -->
<!--
    AusGamers Design by Daniel Lock and Katie Browne, with special thanks to Daniel Roy and Brendan Cosman.
    Development by Daniel Lock, Aaron Hohns and David Harrison.
-->
</body>
</html>
