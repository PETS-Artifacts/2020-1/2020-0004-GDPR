<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="aXDUyCu4JYXUDdpSokrwvDh6LV/ATxAf15MmhZ+vyzHBLU6Z/0cjHsXhWxn7usrkQpTwbALbRab0Hz244orA4mNnFDQJsGp3a6ZhDiy1v0wMU/M2TIyCm3Z7650Zn0L/LX2mZw7x33hRHq+NgxHUJYYUQgorqS98e2IJzg6heYoBgduGQyVLFh4m545KVh1+0aHuYbU/gtwKm1BOwyy+cQjQ0lwEEBRONLVIfBSPCzIXDMgnlJ90/BFywSZRKpqLH92iZGz5/JqNugHgJBrntNCONlerx7O82eYigvZIuBfl2YTWMxLbYvyJMIJPtD6vtsdP54fEjnzwDibJ+rNLgr+5fcc6nFD9Bp/1t3l7IjQuNqbBByeLnJTM0RGZGTWHBZ24iAtoN4YBCw0YVkORPAL/RhgD+5O8r8VT9GCKavWhbEmhcALnZVW/zhA8aDXNUBXejQw3i4UDOgMBLYkan2VlwZt4cZpDY8zrvGtvinoEmRK6zsZawLQtgUJWJIrYhEfv6705rANPl1XwLPqn1+A8Fp+21v1pKjLVByAiZkAyUsVp2j9pw+v05ABWET6os/uU9UYTO76ZwBvmMFMID2oElDX71Cs9pMUUvSGSxVgV13Qi29niuWvN+YgQcaGAmIi6eM2zggDt0XRlHaBHOPmH17ezXHOFRwApCem9oLz/VR5yZ3r8yCSLrZVYtgKiSKMndusJdoU8FSxQ4uPT7AcSc38sS2iv9k1tOD68/rPX6+NzsGjPe0dwJzjuIOLxiy8D2bkyOjRrN8FjU0HY7O7HBcnMaEv5KlC0s5WPm2fIyiPpPiFnoPXnNejpSHujEsZYbj0Fps07vHVJjr79bljC2sT+m7ozb5Zbu4dIEDBcCpzAqj1yGAK/e8eFWAkFmjLcA2kjsbwbkwx9sbkWIe0KHmvLYS2LOqhQiN4a8jdw1qI48fXIYtlNil0Q9R5c3wsPVAwNSBaoz0EzZxhJBGE7yaShv1maO/38JfS9gWFh5U2BnD1KvqEhaDIj7rgUdwcAVc3gSd5kpZOXx61nn7HYRI7ma1yfJ1qm5eVc4hB8NA35+fYubfJR+WLxphmhDUEO9XZRQqfGu7ud5r8ZYwbsH6EC6kFpi09kkt8CY/L8+jmOvc17/JS5v5bFDWR8EjZw4Td9pvuuICKGbNbx8iWHUN+SialqoEquWNEsZ/VdieDCJiTM6S151wLY7QGIAlX7ti7UUUztI9AYmlApZzSlvwdoZZaYRzt+KOyP5g2+WYPNFLZmaeGUiWMcSEXP4vOXartaXxj0nZf4x6rUaYKAwCzCs+y2Iv6A2x7HtbXPU6J6CzM84WXgwlLCzA0lDrr2qku33/gV1XxqAh7d5nM6uJjirxh0dWBDR2C4hFplobJi7fJZt6mecSdtaqY7HXEeoupnF8ZJ2vUP9/6I9CQz699osLQtk+11TME/l1cNGLmnqNVkdManfEZ6jXE4fjNHjjr/QYKKbYvbROwj7C3JxfULjpKX/MO6Jz29IdfHMoj0m3g6OpoquGd7GI54dXuywzxo3tEzBkUqFsHEErKdfHU9hzu5d6Edp1huTsQEwwJXn4CSN6y9yFRtFlaCZTm6OoL05Czb9FYP8eVbsukBY8BWGWIGGoEmL49LsLIzAtnl4aj+Qb1FUKBCkw+Q1GJsSzMmMOvs0Hohs6DVAtqMS9OPVjIWew8cavAbRqkvJgDIF/HlBeoEZXnbJ/SCa+yrQXYWnyu9XZ58rVp1m5dTRCOFKE4l/An+3ng23XNTlV9lt/BMgKHKZGG60IXbbWGl1Fo/KryNyKDHudl1kP0zbzEkJXGqQ61A/1Dp22JgAOtrojYlZHS4TDPMbgUTR0PPPenFUsd7g8KLZm/59ZKO4rAuuo6poWb5UuGLTgZ7jtBnIUdgVoPAgmoODA4XoIsJaDgVd844p0ZSanlyH2wiY32njykas61HNztzvC/NNzc8j80kF2w5qMNWCFSut3NXAaUM1Fh5eGhbwOgVGVdh+6jjpx03Dtzf9CL4V6npe4MU+8c/u2FmVEDVtytAjodVk/oyosl/IVY++RyiYB9IacE14lqTGJtFPojcQl5fK209XKfVGAx4esmcu9G4BKICh+d874xZ2ke5SaPdYtf5CmQaRchKm2qLE6+DiutXHstKxab6hQs+Hgjd7qG7b9hwebmQxFLBaZZVCwEdcpe9yTVyOYRCd2p90c45D8Gl8QSteGBHAYJ5M02tOUb/S4QiiVnyLP37yVHt7QZ8tn/2iJYD4a+zMaqkO/LIT6rpGJmMv0e2QFlUxyGFOUlBnISfnKu+gYRelKf7ftjB9oE2pcJp1AciX6e8ChUQZMt6712cjcmNBV5xhOp0xx4Tthpel7LRe7tyaF/n3h+E68YfUL0E1eHCdBBcvGctIZPEM0CrQlePzeHOEqJK0reiT5nZkTK0m+9239N6LFsMYO/BcS4QKjBim5jswgcmg4AdpHTWQTTUQeAvj9nM/y7NH89naUejkSqrHA8HmtiaEzN+Y1Y3ndMXfSGFfJjUNvLoLkS5zxCSKAkg6w6iHB7KTbvX7fzuBOeXOgNhSfDlgaQTQvO4JxfRdv5rww7TyLYf+86vSBzHpWXbMPykVCvZDWjVTfL7noszJB6v8UUGevS/8uUi5w2mXGQMIyqLE3TiKVNkgbhsqwHC4U/jQcXVDd01OPqvPXRzoTJeWdERqlSLtD4ZKeU5br9tXgv1L8PR7IojaWf4QvKCiUAiSdAxGzb5BNLW1Cwqrkwmlc5mky9tdZEZWEFCp1AHlTS9gTKbpIDXac6p+hQS5OyJyonw4Y94viDcBwIWDV80he39MSuojRGEZjmA1XKDOf5vv7u8/mZoEgU85LJq3NyQeKv+/rcCB1r7MK6HLsxLBavWxTNRoqdwEz3PhpPahkkXjBKBdS4+O6U3IbLcuHLmZIfJdlL6T3XeXURz7uWibFBhPKUvdKhH0Wk+lRKPqa4ctJWtyjOHCZR2sfXFis0sxhOpJVNjlOV7/gb69mgyMswPCDxnz01Z1Fqkjf/B0IY4ifFboth7Pp0bDuJWzlCebFb5m2+67yVmeLQ36MvI3UJxMRioWhJ6w8Z6/2MsjMLxs0VImVKomsq0OVX9yAiloeXxmUk8dHQnn8N6fcNNYAL7ky7/V38K4eCk1ORRa6LjvLs3QVS4EnxN+ab3ryXkvWH7ZcsERMgx/FbCTaURYjweMh69mysWjwhBpzSGFVhYdJYa2SYe7twJe/BXWPzJz0NSO8JeUEl0l9i3pOkj4jIPUR+FDfTkFLpzXwy9IH2GZJbezp7RHD5wpJKX8xRji2fiUaKQkGl3XtDob2ZvqElSVM3BC6Z6zzKL5WEano/WdpRoyKwH43DFQNoQ+3u/zE1/kHGdsTy6C52q0nqE1PbPsnEIwas8ZVQSVJvdOc1vQkxorypkra5//Z/YPqVesbsilWnZBsjvRbtHbkoFN0GEjgHiZVaAboMn3EDTFYickiqpH6hdFc2QlfR6UuCdTvLqprYRLyKTgzH8+l1cbJwhcYT6APR7jShpAD4dgs5lhXVFBEqFDYkOnEVuF9HZr1iFqDbHqKoNYK4RV0jKVu7mm/4nhVcdfcJ8Cb+M2T2JQRq07nN8fk44Mz5iGu726qGiBqehCqQa2+FzBnTbcLVy03L66RTSLT7nhrFPmjr834e41MyhXTHoVGX/8MAeJ/9dOSTiBuy3GtPtkn/SpB1ytD/COUPPMkmEYyfGqjAD1BpZiGHtdUd0LFZl/Yjz3ii9m/Q7/iKgdUGKnW5GmFxMwxGCcTZR+f08XrPz2GHZRHn28C61uBDsFFKvYjOsrX4t3FKbigjdd3n/d5xmQEqjQJ3deRslCZlQAVn5u1oQNeT8fZ2PJXpWB29HVJfJRA3/6KWvQd1D3vN+6yOIkkvF8rI1pega9hNkjgwUJEVYapVmurKJuRNrOEVJg4hxjjXJaWe/GnSoyYRkLJRGAi2rzyGwZcZxZCy1NEx3r91AbDEoicg3jekpKWf6M96OZyt6eSmvlT45cHcr+qpNpMrBBxLOE22c1v4a/Raa+HI9uaraj1/kgRsGFuLPTqsTBtljuP863OFluoO6+CTRsS7gk4Ul+4J+A6NgRuPwf+0kJ6XkW/YkZmxWdQ1qSErcQNK84kOzFg5HXdcDVn3TBjmqU/yngJjRRSWna/4wPk4QIw9h9AZDwSkVYg2/6EY7MHQMlFaY+GolzY3zXh2UnrrxVhDRWEtRDOlFpnZSfqQd0jDDgxtSMwoibV9lFR0TM7aINx2Jlnwz87AQhwfZeru2CjPYWjb+uWCFhKJoB2MVfasS6KE5C1mfc6kr+pwgcz+D8ZnpfEGqE633JF8Uw8bt3zCy+zE2CmGe2DHnS3wCzsrfWw7xd9AU6v/6eHqI6UdlKUvqmoV6OvFvI64IgOLXopd0yo730/rUwYE0a2ku9frdfjN2EW3/N6U2f5SWES+1DqEoVCEqGnYkAkku/BF/yRCO+bxo1DtWCEhkm1rUAVLqiGpOpyoAyHGngimhxcqUEuRSPiPNGqOR7EGS2o4iHOFf+zCsgNbyrdhG0tpbq4hfQqCadLfclTD4xjTO35FUxdYkrltNZP98atWTa+T7vCTiQy+TCEZwSkiKu3OKPhw/wdXFL2B8Xr/tNh/PTnX5wWb/pDd37k5yLzA7qguW+feOqBx+mhqBGHSwJ9IfkYdpI11xWjJJLuS1uZTpMbAQfne19qTpnicbcVi6qFOy/Veb9qw0dLNdeAm7yi5qTCHy8evWOz4waivGwPRkTbEE0oQqdxadfqnFLrq4MVqKbBFD1l6wgleYSxuXBIJAfGriNUJpEgUYykAgvP3LgaELNLCQZE+B91kSEkypigdu6foKzjefcNvl54IR9fYzTbqOFd/wJOXNpgGwvUkQpUER2d2IlihAcYq6rgIgSI/Jhw0rLz7OnPdjP6H0fb7j+xPzHoW3PEy5GXDChWGFeULphC+dmXoKrdiLWywL+L34dSnd8bN3MX+GzV8ZIvr8WT31zggO+IDNaaBVWgXyXz5+RfMsgyGR9FnfVBIZ5bParVChoE95yO58/pM5ceB7wCe4VkH7ayu/rQJGxUybxaeOoxhf67o/Dq+A6peg5BiHTuEais4HEXJhpH873LGOWVjE4v6UgI6lwq17GEPfKEssZZRymwSL+5lqsp6NLlAbpY+sQHLlWCPuYpfA6up40clEjaN7WlttpuWzjYXc33SLgwRXQeojIcI2jqJs1p+OguzseeUpIP7USYvga7/IDJ2tBg7+/40TLNxh+fBt+x986nzBJVCZwn+oLNVq4Mz1krNxCBW8kRlIi73MK9lsy8UivDMhSDB6QbDDaAgCP0pHjpsL4tPcARtDfI+l0uDBE7BCqziXoSZYAy/CNmfcLhliEmiQkIE4ayER9P2aHhKKriqIC42XomQ+stKf2MdnQoTljgyo9BjmwKZHy9nWTEYcfCCzIsg+zXrcvAtN1IuckzVlNCPtq+vrTC6Af++wri6dUU2Hz7c/a5tJLrwYzQ1flKP63erf5hsoW7yMP2ysKQmYifuvcqnKij3hUJkvF0DshMHx8GmPn901ewKb6g2x4mp9V+wG6nh+/WSnFefTRC8lTtPWo3VV8dxN6p9qkn62EZWiHTrNgBbDaVG4HbCyJi5rHyI5vgsR4HxOdYFx6u+HoVJ+1q21JiLnuP0Fta1lLjOatujxONeEmuQVcalNPGx0QymzFdb5GcVBjLZAQgZkoNqOgKLQzm5FWYmV9hSm2GJfaCfD+L9nRO0CIvJjGXs6pIZ+0K3YXTwndV4Xv6IyjV2zd9iR3vDCannzW5Iw8aGhVqQKYvOezfFHydTSHFdlUqohbIEfS0NNB7u4WYPXjlHcLOrTYIJSJOgMtA4GEfjCfjeLvwBnAgFVPkqZ3x5eWVluCC0j1zBiU+JvQ+aCJlVDkcBRIZDiFiOdII1tdoyFJTMpwcIgKgQEVCSta1MW0tuzzS8EsZ6HqI6nFDYupMZ9IA57kXBV/39kMni/8nUNZPm+2gSYL4ITIewz4h47gZGJvBREK6sizu9aU0lCOqPXBQ4DCO54ikmnJcwIHCVNIQLVOEVNFIdcsX5AqdNwEWHZbazDjdo3P1lF+6g29EImdpsPrlCrJWKqTmFTPzT+nAGnLYsQuEAL1vDw79VwRhX4kTIGqG4ibvLZXgT362GqgV6QCDYRs6PL38UYcLxsArIINAoqx/hvs6apsMnrwuZvyHppnqCuwCvlfOGKrp6FQUEQcMO8CHXThUcXgAWckpNj5L9+wRMg0Yjn+7FZygE+4RcpJzj3icsN7gb8NUqskmERvdZivpKXCmZuQ8c+vMlnyMGbLzrDuaBMfPE45U48IZqSW9r9ss11eZXmiwf7dDqeZkwwlDAHcHVBx432GN/fKRhsOXKiHuA7Ro/RsnOZVbbBnWtfMnn2SRGO3754IGsol2uJb2PPRw6PpU0NxTgF5kVbhsaBiC28rQpi8+KhhZjsPSln6GW2LKmxRhoey7piXvYamR/Al2UA0onHOBE7ybRRCeuBb4ABIWRKpGNsXHkmoPOvr2SVNie4o3+FaFmM4I+gfQudHZftqRJbAriq1w+MrriM6vGMbhcK//2uKWfRp+1QaqJOqHm1tP+Rgod8pReRL79T0w02IbF8MaRO1p3VO6ohYzwSRvrdJOhpl2Y6Iq7iC4OTauUIh61XdxQ676RW0f9RCxWhbMvgVxc19ZABixHlewhHYx28zznQTc6JQVsQ04kqPh2M4iXNPuM0QpNM8YWQ2pd8IBZm6zIp2jnflB+9awMBifhi+T9Esyex4NiKag8WYxzFyxYSY2cJpPyUgYsgkzwq02CohQdefR1Igfn5Lvy9L0oPa2s0Fl6DlBHVbx69f0eFMccGRqm/VCGURoHXchJsdYYm1FnovCDIVTHTEG9ATxQfAmdDJ74XbFy1u0RxTHk72Btw1EdSkMeHtBa+GEQhgfSPkUyhHp58o4YfUk87DNrKWhXCNVVGXpx3zfXBJmLUbaQS8KyPWbLhjkjR2Y4OM3pAOkoL1GgGrvqlfYRystRf0geMeshri0tdcAxRSETjzLS+dT3CWop3kBCB7396VlwiFrK64n5C8Y81oE3WuiWwIZnldHPNbEHc8LdaBBy7/vSptB8jtMtFHaoYy1DAjxp2aT5LqtCWBTubRleKb+ijKvN7wWdINlchm37TEvEKdH5lMIQzwF0kTuhBORU7A9xUGf0BkJMps0ou618mvDM0crJfvo0Qid89EArwEGZnGT0NN10z2SDGAjr6RnNCIpc4A2W4z+5ZohzyKKURnda1403LomG1hJCWNs7PNvm/v/fdga0DElI936vMJutoSBr2OAwWbH5AzrNBdgTk9VbAyNB/OUN/7wngluRY9TlJJxK+Uq9gIwNkDVUWcZtUt0pUfK1DKfRq5tB0QZjjjkeZFkNCxvLM1hykocDbO61z7SGXil19UbW5zo9EPhMy9esuktBzCgUafJ6LYjibN8IBE7ryMWxGZxbRoavtGyqHjXDjJt064rLTwPbhybUqFg7mpaAvtgb9hsiTlz6uzvFeGNbORrESh2F4AEHJrsqgofiQVfKAYoojDrqWkfjK96Uxm2gNC7fko4jzV+3XDMBl891C3tMiIqiL39pZsDf+/uKHnkh3Fi6XWMVXwuMW2LfgIDSKvqo/zXH506ZzGc3y6GCZCe6wGJKlRyu+L9koVcPk6zBy7MxaokWdAVknnqheMHp2WEhjyzR/qc6xb/yo4SPx7zLGKflrcccJJtT0MRpb19J94kqr+pjUpjyz30VCnkrRKQ/TWtbDQUH7c9CZyRvt6OenGECCox9IDRPUnjtGeasST3vbIXiQ/qSY3OIFLU/C9YsilZw/z2HVqSNLweuXT+CbDCzTSCZjiV90rrwhZy3COsK6S0e9KRP0nloulBTBzipgdVAqgNk2xGQNmw4e9MazGwEWKsvLEr923S0AlW8OK3/9q60D8Eo7tjRlvUKPN7ESpKh0dwAsyh2Nk/NV1aihJayu5XJvoQfZuNYJUqGRGg3vtsVsZdFhdpt7wwhm83nVlfNNxeaFqxi5fRYZpibKcj9MVl+WA+JRnQi3uQt5TrISD2tdJQlonR0euJ0pv9n2Rm36LMb66R56Qo6yct5sZdWuc4+VZTqr0JDta08ayCQGr3M5n0uu0dCpfB/6L4Wj+n6tcjtCQOT3ZB2hAb32waRcQW0PoCIuxGVB2UA0k3/1JfDlt6rBi9yJuYgxg4c7/OkVkkvX5ZbedbA1DKqsJGzkTDPxot9MElme+MhZv1xDFDtjy5nyxIgVtGDkE/6nZvSHyohNQUiUpBwzcFU5rBwXJUx7NoD2HXegLXp4nIfVdjZW0CH3MCQ4r1hPkwN7F7flDVcWguMKaiy0yjVJDOLJP7u4A639kdqcEcg6yz8oOVj6JR7ds5FZh8NWYQ9E5OmlXYIn5bONXkvATjgOWieDZ9mdlpFROlRUUfb21hkR9Aks1wftXFXw3lE4lEztDm9AYLOzVNUK+qu/lNmzCuCvQtD+mMtKzimCssZzjDy8g6H2liRssFMV4nmJXPqOR50yUz9MCwTGQLkAC3mQLxGZp/0sOBOaZ0a8GuS7K8RLPnwgp4j0drEJbs3achimc0Z9tOg4rrT+vnOrrtUTzRD7RG0adVIGjoTsexRtmqEEHPkfS35OpMcMe4zqAnFNtC2jcCaTp/8Ztsos0nZH8WPq/ObGRkMN908HVeGjYjmermwD1k7MyhqVhDjyH4QXKwmLCk+yFrf9t8AMMSfuLuDfJMVeEJk2t5NYBk7bSyAEpK7q22LeHk4lEwYImFBWyjhpyBlpVyW77Wx6iEqfE8LTvRY5Fyk3oQ1W/Qnk6JYniNMXJlbUknNWDvuKkF57AqOe3jWBlbdmumd0ULg1NxRHRg+MHtb2N5KcOL71NnMMQZPKcA6kXE5M28ta+ffb6j3jt1MvW1mKOgpyLFC8N88dBhmh805SV2v8IS1UJn3oyiBw/Rr/9laQefoIY+oO0gp+cs/JkREMfLLarXcoPeleuyyNyq5PzeuDOMzgqiH8whSX7o6FLwVrdIxC+oIEniCyXLtK9e3iu+suEqTjpOJcYtJKEfaYDjUcMt5ibubKMIJQLitICg510FaeHAihXmfpw76ij6gEi36BXQEFpmYGo895VcDmbl8VGjx349pNVTmkmSBDjg4RagqLkdQGin+IzdeHbv31gBaihiS3Y4RaPYSXrTo9HNIy8nLR7vXtr9HMB5ilatJ9UmVOCXtcXo4KGKYoeNUIFf52L9596bnkWZnDwsrafGiUuXZ0AVNjqpC3XOKBjDiz9gHFL7xEGhaXcuf31XHE/PxBMANCsOCKuKZuJEohfYiYL0MSvy3i/kpRz7BtJRPLaE0j+mMcQqd15SnayzO/gmPf/xy1h4cVeAZc2PUuRuMkxloHIOXW2K1UE6S6CMLSUYiAbVb0ZGoU8yNSEhXzNrrCP9FTYG5A6AoBKKyFJukrxuhJs4wHnTZGx32l7jhpSN/ZzUxTyTimFk6mQq0kjQP2VyUg4fEDNrENABh6CS8rQVlvZsNgrlYCK9pLidiL1YFSxLbZgnDFWDw+NeIspfYo5roq9Qubq3be4naASwnmjAmtDvFP1gHyqUa+hJmA00Q8pZ5PS5LDjRuOBTkZ5uq+dYMpqodPI3PfkgCi7YDHbY6t5zDp9pZF1cC1WMeAHpWqeWqkvEnWKGRdEI8SU6FF2XOC4fdacH1UWkCc47KLnDY/BlJB95oVCy8aanGhhjA+yWOnXWH7y67DQ3xPH5UJTP6xTu17gr6l/xlcfdAFKL3kesjPzHB1Dg9LZ9qCgBzHHpcDsJ1a4juTltpt591jEKyg6rJKTGsTUELGt7JL13hOrKwJ/WUrC10513cn/KtDnDyUhnTUN4dbN3RHFWZfM+DBjf5UT/2ujQ8KPDNFSlL1QySUDAoyPmpodbeoZQtVsDaVsvq9lAwYtIOwYhhQ4hhhEdEHcbs+7dDAlbQMoM0Zb8cm9ykWFYjdlmLPqO+tEbGiwS+1Q1x/Sru9GwvMbFDiI6duKqoFEiSFsLNqC2XLPudS+g8TL9zSc5cvhlMU3GiewZF5/BLAPFJ3WBLk/ZAZXPLGf83WTT2fHkfKFIfmlcjPNc7PTJXwlb378mv3n7/Xi6GwAvEf2vZFZ8BOM1GSuG7W971Q7hbzT564AHB2kbbHZFuovBurkfOe0saEWMQP3PajNbvorEBtC1ggNDxjGAUlN474ZkCOBQ0/fRUjpGH7KdxBpnvtsdkGGEWW1eKli4hYmKOSuNBKTR5Q5x4P1j1UFf9XZgFEtj2chFqyWXrjHjZq19xVVd11kNB35sWIMY9kNpl6a0kN1fGPCEG7S3OBosD8KGJ11BvzEqU/rsK0vPtj/wBu+Y02IderraGdK1uz8F5ojk5C74aiOOe7MX7PRReKYeqieMeKEsd2H2eTsE613/X0244/M3/ZrKewSb/fEcp/2S6pGC5YAawJl71lnLELo8GBDIyNjCLBk7hmwF95FLWoWovMpywMkXeSZ/wGfsrLEkbtnPyP0TI1cpnOoptNIxTO92HsFK7vxvp2l/mv1xqicI/0fVlVmwEJTcSqBPDmkJqYBptBIQU5A77Q8sqGHHqnWDigj1OB5Zi+abbOXcPvW44Dj5QC+iLxeH/3laomoWZJS8HhQpVdwGdwUTJapoVdKF++jHMQ5M6M52Zrpt4jw492nPYaHASTeMy+NyyRCuAvdNOInLdeLjAvzOeAqtSU2s+ICOWQlhhfkiqKjEN5gfcyizIUeo+F+fB2q48/DCcgxEnLW2p3nwhhYfoQzrObSUi1e9k0S/YAlrL2ENejcqrQnGV47cZXCqRo7cEXQcuoq5f2qTCSsGhe0Hw62dHxwSZw4v7QimQfTSeBNh0g1o19keCeOfM3K0Ol0sm6i34ub6ioBKghI80lAFTDLpQMDbMl1q/kMdeIySHbFMgnkQvpVU/xBzVbMC9JacktxqiVicF7ZieK0GgKNO8+dWxsZN36910Sm0QSejzyQs235fCQ2GLxLXBxUL3ruxXODMZ7td7kEdLNdteEUfPDJ3GUHh6UOXv8MQt/g8S6S6T1SzjTeoZF1c9VGGoRYRmhyiWjqC8+seyfJsr9OfZiPfdXDnt56bdqTjz5efUMuyPqaZsHObCaOB7qzT6Uqbrpi/QUPkmEgqYlOMrpW1Yq2Bb86PMp2tkvrjrVRT78eKM6HryY97r9RzjL/nuuOWXDsFDoLzSh2fDMN5Ip0BFsIFIvES1zXU4PkYypxC+elQIubPgL0usvmro0sGwQtI4RhFBc6QMKc8vOEUhGBPW4cpVtblxY/2Z1Db2W/o8T9JEhMJ2Bimtc1k9p2m0D91UKQEnRNmg9PfGlMO/KeJvAN924NUlXeRpIjq57zWTARZaWqW8M5TPZPz746EuYqcjxfgDQIH/YLgTzPEQATbaY/ly1X7r20uV0w31QTZ3lQy+QV17uFvqt5Y2aqPKUVoVfEGYB9mDAO8Tnx8MenMn4GUaUbP7YK14Dkmjf4WJJYBUXUxyWvaGaSOe4sSlmlrelbtUXicQLeDJAwjTY0JLMKlembFTBOHdPKQSjgYalCWsknFTxe4wMW65oeBHWAW/XHBGGW3grkbarrU59g/2AYcjYvswI707dbzXzkcAXL0NXBTdbCaC9rVXrQ0tzyN9qwaJ30VqGdLIt7in0sYN9K3ReZRWxP4p56clSpUHjCiSrDLjvwQUHf054xx6El1P7LPkqcmOGXSQ1aitf2YWXUBqeqeCyB1CTkr5VvtTbfVc14CusSiYGQ4vLSbhqcsRsGKYxu99Q7mhb62wM7hPQBAa4inggwwSV3J2sQmlUgGDfwT1CVFGh8KFZftJULgSpqYRsX3H/DnjGhRpUIIb5INtUOh81PgPHUCDT8T3FOGKFWxwuTYiAArB0AK3TjOp4qygyt52Qx8vnvUnKfsENqHYJD2G9YulV33TLPqasEEEY9BQgElbl2z0ir4MxLLM2w7ja303q4Qavr3Z0qMAsQZ9lWiLU298396kgMFmDPT4aoZ1MJE+MWw0UGU8eRpYS9xxgg913t3LPCxBbW2PvuP6/SYer/J2Gaqk1wk65V0maCUuwkAQJA8CuL5rrgboGofyLJje14aiqwRAsIL4kZOSxJfRTao8elnE2eTggkmRt3BP/nNZ70fcrPVQHmCaQIFDku/qwd4GJjBb+eP5ZywaGWFOSKRF/NqfdtmaWXhZ6Cw8WiEUr4vW/kCMpFanQBaxEtPSdBr9Jd066d5KTgPLPakqMZL4AFib63BXrNaqTdXWkTXBPF6gQf1Sa38Zr/1Ri2bfUkkSbxwptP/tuhpG+OsNoSiGc4zonBjJnmysZWXCi0wCAkmYIqaU+VMd/lcCqkV9M0BUuuYQmXjbt/0sYuB7iqHMskiHnULpQHwh7CUt/bkSauwKMRUf2tI2WSXmbENJCdaQM2BAjMfnp/9BcKzI0t6CykNZydmb9dWGE70pVYOklSNrafcM/XZoTpeD903iWhcBlwdvw2yl/SUYnftlgkD0jCTxEigB5/rRYPHyBRujLAormadalw7EdACo0bmbA1esbpb8IyZ8Q7yyRx4ZXZQ3HYJwaIpyfQg/FDx3y/DdRY90wbfu+cwYKanwhaXHsH+xs02Kmzb++WR2GbVIF7QHVYl56VIXdYMGdKcu28WFUV0DZyTdeaD2crfXnDDXxda4/lDFu/+ygEEiWGgact75QYDE06xchgzUOGXCTr9H/HJwGOsdf0aRVkDAqwMaBufrgIwE3O+i+eu97bqovJ3VJQhKsxHmX3XLC5IOIYTZAEsFqmWq5y3nQ2OCAKU79y2WrLN1CkRw/7YfMtrznihku3dUqbQ/HQ/BdlV2BpRQncOKFEJm5bHjEZ8X3FPiE40DLPXYGaFPHBd2gs8N9Z72m1mIsKygT66tqZgfQDej6whCFmHF6MVWjc8blcdLrxgtfhx3wV/HTQa101PYAexx+nFonJ+elmctKJ7molx4nZ8J/zuBElE25l0XsfE+EGVfDvxK97cLJ22d5K3DKrNoovTzfrvaabaLiLN4VjHtYvUVKCJit2YPvvjIFl/xhB+7fj4//VzSpuUnuENbzAktXxmWbSbLXL5xVSUWF3L8hW3r765hVVS6PxjMQIW/m9YcWwlh3v0M/" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="AIDKY5tMq+D4SAUXS/u6m7ToVzSi1gopHoLE7GtKXq1F3BGT3Ff2aEqd42PBrLz9lrdS0fEk7faHjRauhiwPN+O0QCh/g4NShJGcivgaPk2VUPx7zxeJWIVYu0gbv0zkvqGvzE0SJeMhLe2rnkzcscFOLEn5u8R13si1Q5oF+Jmq+mTp1oXnqypj82Ws8gCV7DGTFrx/6XVsGuYbdxlIxbH1VJCVVQa5HwVMCSpPT7PbM+L+jxkZetw7ST4WsZteDwTIp70xh93SD5f2FentGEL+dVPksyhoPX3nrbq0wOxpuMvg9ZzgSOP1mSGYiPovKhn8M7wAEctpUYLNUQnlWg==" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width:260px;">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="#">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/eartquakesecure.aspx">Dos and Dont  during earthquake</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/externaltenders.aspx">External Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=grsGMsqnclzsclld">Kochi Metro Tenders</a></li><li><a href="/tender/Default.aspx?cid=3AGNZpFcFwwlld">Lucknow Metro Works</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender/Default.aspx?cid=yGEt7G92BVAlld">Noida-Greater noida works</a></li><li><a href="/saleofscrap.aspx">Details of sale of scrap by DMRC</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/advertiserlist.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/DMRCprocurement.aspx">DMRC Procurement Manual</a></li><li><a href="/tender/Default.aspx?cid=vlVO3VPqpMAlld">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="/otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/otherdocuments/VigilanceMnual.pdf">Vigilance Manual</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>
















  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">79750889</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
