<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yE5EhmYbkWms/dQ/LHBMxVqMyfb8eW/eEqs2VxNp/R4CDmk5QXXtgqSZ3iSpRkI8jV3bngSFF7m1NQHdprSUsSTdRtZcRp/POkuKTllNbJeeNv+HsdOZ7NtM6HQ5fAwFVDLqiMIYT2O6jrb3jdpN9G6uqqSVg1xiRhCuwfi+WZ7DcliAz+diSspOUvybP5yYs3UhE5gKgOHHaKjr3z2IUwC3IC/+WpH5FQQtw8upF+STxnbDHWyHgo3B9A/oi9G4H6CbLYjBSYNs3IXUUgWNDUs7wwQsGrZpx6hToiJUo8/HA1zx78F3xw5ECY1XnhyYW9XcKa21kxNYuIqAqUjN6+rx0bmeE3y1rnulVL/vkbcINye1+ehkr8ekdXtaox3Wfw/AgkYJuCTEw7gIYnKQ+6EHbTRugUsQKR8n2TLm6QBprqG1EsBdu0R8klDOLVMWnXHJtLih/3VAQP+WD1U9FxLHJSmGntX/o1qGGIYPyDsNLjFRP3N7H3ih11Px127hhE1U6ER0U9w8A0xlwxsXqmvvnPLoN/cilnp6KESArVYzIfHlGR3Kk/uPP3V/ep0lOH2G+bQ/gx/Al2aVIKI3ehi4ohNPJLHW4z23RzdSE71YGoYpxOQ2HrMjTZ4SeEs9pPb2PwfiHun+nbh4lY6rFG+AZ9I98F5almnk12ozmPU1CoLvxmIe0Mk6ByJJMThfpIkHmwkl3yjb/hpGTXEGj4lXaNe8gb5uhkN9lsqN6RFDbY/GgwLPGpOYMBpM9kXyH2ZihGYOG36OceOr2UMwh5mysaA+2EJ4OtFbuy7qBLF7Ba2GMQor2jEL2LjkGTXB87MhZp2rUd7JEwwEgEFkaxY0+3IPBnRNxUQMZ99tVRJI0aSDqV8RXar1Y3bAG9LMebO11dOx9Ky6qzM9e1B5I9aURgTCeC9pps+u59QJnwQ6Pc6JbYbICchWcV2B8A5GLVNJpLKSQXU8nmuQZUbGPNAPj0m7YDAv/TRBl30aQrOACOqsikubsnyUR1trampPiklGNCU+f8RYruZl2wrRsOqY4G2o2ZRutYvrHrax/ILF9UQObhPcSwp/pZTR7D/wQEbpz2rJ/DjqvaYaUMPhyxBfP3EP7oGm/4x/ueMBhmP54bXG6tcptLnszN53Pt/xYdQqeufjIUmACMOTMuiULOp1Kl/Bomwki4o3E3n0HsLBF2Pxd8RB68SXT8tlMaEhLQsbh1BCSrMtopPVrWu2UW3YmYuVNlRuGT4KGiKKJnEeCNQWuugDhpZzvhMdU2EvhRLqW6WgtC3Ki/zL09LUK7zDwcP+JLceBfwG1qgqLNSiOdWj8OXFqEM3izwMp8S6B7lqqF7S/u8hjapIxYDo/KOUHCN9X4MaP7KH0fzZ0ko5T0PKhikl5J4kE3QXHj3NUiI5p07Fkn7QMYD8tt8NG1KjJoXpusw1pX0n1kLBpeNTPBg6LnK88iY95Gfvns4bSglJNY4G2IBMYr+LWMsyL0w1xhhYRTB0hlaes9QmCKJdEBRb8OMOQ5n1HTXy1rWeetqojvPrll7GkR2om51wsltTsliYyh7gcpU+agM5JtKbdLGdbjt6BurBR47L/OdRne1WR7z5HJ9FYoiaCNLo74cdtqCK97X9MnWKlTlaA3WwmOTXDv0q2DzAM3bzW9KmtCk2dSWX0On1zr12thgDEz9qsCps2UIbSe0cEUC4VGZUTDirUoF0FIa2l8genoFyI+0EylRMHHb6XFJha63/vxFOWPb39jiZY3wdy0hWd3hJ5el3lkHF0czUDITNb6LeBSW6fla8S/nqjzOS4rqp+aTGZeHagt1ym2rV2dIjxoZqmJmRkaWDN8nLxq7ww4bmbKait5wYmOc8G138fQm+DvUeylTEhuEpZG4VY809aBtYsEN56zw32raDtaVNnxDbxLTY5J5B2Jt5hUaZn1SC57b4wdu+IdtKOa3k+MBjaZJHwtdCNPOYT8hlsKIVAW0FeR68pGVdu8kh1xLtZOksCu6X4+B+Z5V37fW+8xEZkQ+I8kE+2j057zVN/y/fydWbm58uzaqAHJKCUiDFjtAmExT+uzTSdlb6bs/m9687r5uCFuEeIkqvGVztkyQTB2PqluiCxzFMareg60cKOJ8ashQIT0cSpARU6shVBUI8Qjq/d8o5GttsixEaftu84bNlU1rQZ8bgEqh1+9/hgf1hzQOJSUmqlhmtWtz9gNektsFc2G/pvgpASPOUrj88+QmXUYsO8L2exEX4+cfImya9OqJtsEaziHCqUEsuzQkmL7LjDyACy5GdBgiV+F3l6Yq5J+K4SlhkQYGQA7EaSbe+Nu3hAJRM2Eoyg6hDU/sXzp9tS/S1mO2+1tfNwL3aZnluLl+g6RvKJ0r4Swjrokjfas0R5/NcQvKBvC3YQ0vOZIMIcvuDX10W9HQSt3nkWpsHSVjOX2lKCWUf0VFD+QV6k2C6lvr+qE/6dSx3XXMGmxwQ12B4hc4MnI5nGUK5NuKXES82VfQ2Qvw2xF0uz/Ig20d6MGIBv92PkbPheOX0z3IyoaKVS6TyFTp5mjsc2TLxWSG9CKOVn8Ilxu/3gKcho5mp3DZD2SS4mzhARhzBZENORFM8eJhj0VEoPH3h+b1bdBJrDjbIrnTQfL28gkzWlqbIdVU5EVYHO1rklhF9KDQ1NEL1vRurJRS+M2+nY7l0haXb092/8VYIhHkko/0CZOftQdvbEWVP9ebW8ClX8oTPsOUzhnA9cpFhZPKsP4khkG56A/gWpuzM8d5tCiA3RzBl9Ie0Yp5jHXhnlvk2WPLOlUwvbi9swuLg2oPw5pm+cSdaRt9QVkVWGWjyqppWyv7keKI4D2suKERmhzQu7Jn0caN/+HmwQP/lXGuODoN90HwFfFtVGlO6l5f7qyfZ/VJfw+opuesJQqFbx1nwWs5PWVQjEpcxh0XUMUw651iDK3cNXcnNSPUacipVtQi41aLrAG1FRXA2N49GBa9FKF8b1YJvxF8PKXPCoVe6mwERGto5REGKii7qjKUs5EZ0SxHWi9yKJLnGfXK8rDgjzVMbz3dHBcBjP5lhAYsWNmiJX1R8MYoVb0E8v1woz6ROEXrsork1yapAPpJkeOAxGW5aZ/tN9RdHHEvYwUr6kOMyv0kiMn8ZYAl1uzptszZsj92kBGzvZqRMmcrKioX0WNdIq92DikgS3u+gsuqKQ17tTiRasF4VyVUP8YqKzvK8342CYwWz/+6ANX6DECC2VtdJ6UpH2XC/ET82naJqUZVxOMivDLXX4k8g1ZUoStE5Jc1k2L3LRObHUB2tmCR6k6jNrA2eI1hiptWhliZgOjkbPtab8JJMTqdIrnUYvyP8/PYSEgdXZ1VcFw3DtM4kxLbA2d+tqUeWt8s3OuXuRsmKEVC6O4GaYon/fEAyxsY1Ix6dC2KiN06P9YiWoyyqegJJ9SHBChEd9LQZu58FH6SdAb9YB+3KIdpFzDI8z7nrQKjDJ/cmzFt3cAiBqndnoKRTv7dz8xBfm/KVAmTVasWuMA2WfGb7QBN/ChApxQ4iyakFY8FagGDoCVuvaOqmnDnqN1+CyAtw8JhfMskHyGEZ+Fh1ObjMEL1QorvjiyP1URv9NcRVKs+6iBaOGiGrOoVsus+KmRDi2/mIKzRtu21KaFi9gPyihCMHrHlik12hgrk1n0exJ6hSx/bTbfs7SvNJNRgt7/cXg+NKHro5tsD9olgJzVeJl6qboXq70dbICWCXrbpSoHlQImqymQuYAJ8C5+LhhUutTxRooB8jEBR3G2SDo5qMbd68Nd/NqwSEQFmPETWY8qgoxu0E8P2dJWhbo0kCJGS11sOKfckG2Vn8HJ10cMXjqMuWMrIXoZM05zcelYvZhRFPj8uSRxopSrwNxTHvl1PwPLIppyu/QNnJH+qt6cqd1QDyoTdbKsntKnGoj3VqmNiiQf72ZE3QaXfvTJyFJBpDKaeuwBETwunyhx992/H9Zo78f5EO7aPnrgNSGa8jPzXXofvCkaxyoGgzZYxg9W5OXn2+4yPSa+259RA9HqBJaqHvQfAGnzXHVK00u3EygHWKrudZ9z0R4qY0oBYuBv/61gfzp/AFiQl0p6u7rnqdmWsHtK/FkPSAXPwYHU0CCrH9ZPyhmkVBQeNFuoCvOYXsIskVS87I5qz5by/nN8gizFKHnyEeBMCHEssVrYzDzC/4+ViHW7KyL/kCPpcA1UJLoYEkZYzGboB1B7gBuzapkWNCfH9OupCRHZ7aZA/MvWa5B7kBkKy2EsTdyQGciaEapwopPQ2WsqoK9IcpV/yMMi1jbeg/Un5+Djc6IA6iJSTmqQ2CTSv/f4zfXNa2ab2OZOCfdPptpt2NfO9a1VEWGAxV9lcEC3B9gm2LqzO0PV9SphB4ElSbXD0udFvpXb3meLtOClVvY4EjZ3wO19zC/NB7qK1F/m6LUPuBiZemtsH2qcuXieRQ86iZv+n/abO9Xne+v2EnhDI9zp3epTddFcotRwN02z+3mh4rqMCC2PzSWNW6/Jfj2x3TU2O40WrBpMEqqvbkS76Tp7QwACnI/EwM38+uBLKBnkIQvZUZYQuWOChBOld/GiNhe5X0b9gV/q8MRXSnmMhxk5HRTGAEhfIxWo2kfqcC2VUekh865DBz5MbCV/wgo07ljTIjwkwUfx4JeF4Dh1rUSdMnNJuYPuUIluYbgSF7gN2ibERChtKQ5lwcPuFRu96EURME4xkT2PHXO7olxiDh7jNPnPZ/7CYcaFq1vMqW9Q/KKzD6qWGfjWKw5IUzkAdU/+wwpxYwk+DAmU6uPayfCjMgQdc2osDtt/8cifov3hnM34HfQJwy5P0zxfzTiQY9V0VfQcIB9TD+K+1L/bH9+lyoixBQASa2Xy2mu+gbVgdudwqv443E5HMsjdssdy5PKCFtdoi6Kf5fRIG5Wi6Tmms+ZA95TeFQnq0LBam5lKOexulIEyAqoIPHIW2G5OdYAPwARfh6QfjJ8byJIl2hUxbCERjJBhwW54Pmp+xV5Yi/0MujQpmqGgodEg06SuJaVUxNQyvvUmix2GTshj845nghKBCTcfVg7jPp365J0pT45Bw5rwtg8qnJrPfkf4U1t/eL7mqbct53Kt4nU50i+FTKq09PJh9dlJW4uAZbQCmm7AcLvBiJbU/LylOLUFuNz4gbO9lFj63DmPQEZ/GU6FfI+mOavwvd8L7FltgDBYbb05CSrLFC4c+vmqBzq4Jcxl5SuLzewS8ZFIQeAg8TbmodO5sWrqfz7/WK5CPjv7VBxU1NnlcGgd4GH8O0RL3WvjbHhWRRVJw1r+jav7/bZLzu84G+bj9aOOJgACQ48Fow00HON8buGmSB6sjWoXX4SItSsW0m7ZPAHLZqjUin1D0FMmQqubS9Wzf5lIti+Na0sruIzNFx5uFuEL2hiYRr4A4GULAvOVmWpvvLWzG614T1pUbdCk/n6qlajEJ9D+pM1nqCUYtIdk9hBCyXD6bNi2phx98b1iXWX0YIL9weeFUNp1AhxpN4urruSGBrsxYTq2iskAP3kcXbmaqM4YAm7pgfTnX8T8qpRf/LnZShFnDJTKc/iMxJZHZ+7WaWN/BPDFrxgQ87AYyUTg062IizqrnqxlI1NFvJyrhmsfNTSYbbJIuo0YzL62ZjvXHepcRZl0mztXyyL8GjAt1/1vQvjTT+p5hEWWcyJQs2Cl6QY1r6km6rxSjHsAyVwHIari01+SN7RjKBsYSXq+yK9474gvJ5O49d95cel2e/apcgoSwXO8B9k7o8W1CGUJ41b/O6LATIIo0eS8Gob28oQvODpadPn4xUv9QY8Qk3aQ4K7CJMG378NDqG7jcQjYDN3m4FKOC5zXjNG1q/ARSr9W9xx1g7W0RpS5Kp7dHA+xMxoSqfo//TTnF+Ch6TSp2kxZXNBiu4Z8x2iiguoESMVUpllJ4dfLz80XwXfJKK4jQHdyZIuLxcXsruW56nYHUJDARCd/LhKtHX8uAosdYje4keD6E9PbpR/FdPwzJHnqBebLmkN4zWgGMcWcXfe6Qlb0svjXHGW8dDncMQoTVg8ijacTPyTRPbjeuJZeI/Au9743ErQgUWq6Y+VWGXs8nNppe6Z9jHaZUgAjb4Zl2M4d+gfAWc7q9xPGQY5k784phHskfjkDSQsADQlwc4mNv3upx9aY3hR33y+HswGAWp5Lmu3OPjexSOytoWqKr3wc5ms01hX1zp7a2Q1wV/IHjwCsYDA+es3mDfjWeXP9b2uBkdWFXhPrX4LaNbp0VDakQab2Q8GIwyIKCr6VaNquJyW6mLw5N5Pdc4yHuz1may4N9G1j9i7AnMjvzkgdaEfaKlji0/oXTdi3Dsozq3xerbBVThpalHBx0a3veAkIVbkGGFRHrQi8BWDeQFawlMUm7qtI1G5yBru6OpqXcsveaFzSMGvlefKojw100WM5FA3BWR+brcYZJ6rwWn6H9SF6PdceGCzztcIG9u1TwBr826XqGVfqUEapC0xfmvDXHV2itFE4DOZKthlKhL9uovuX79yXyqRoiWQ42VWpS7hrLFX1N0jqEquQXBaM/ScdSw0q5TXS3Kc2DpeBOBTwwWJh9B7u5DxlALOv57ug/BhLRsvABL/M/zATW7couQCYJvqFFx80Q8Up4cN1/CY5FHPWomSkDyLm648FynpiN7elmejdbM7NTw2eq/lh7n81FOe4rZaSpm7b05ZNAQtyhfkJV8tIeQHOhEOQm2+hLf9fakIHzXvZeKawFyLFzDeyZ3d6L4KYn4bNBwqnei5O//cYMURpvTSjTrd4mB1Moyq5UgXZ6z0jbJ7+CQf2iZnjskxaqatAuzpibgTld6wRDF0Mhdgej5pLReg+WfQd8mZERb+xJaZg0jkDIMXolHjw2KJWqs2+P7nJptcOd5b/S5bJXqQwzTJEREmVZi1usDBYkixsDKZSgE3vsvVHxp+FvyJ0w0LohM7VC9wrbneZSEco6UkZXShuEVidLYZHYQ7B+Ap/AEXq+mGg9vQdL/USY/NtyWAoKtFflNj4lQaEiZ3KKUC3zDXu6J7zfRf5DWeyTtXe95C0fnlPYZMtZ/o21CrJzxx4+v0MTgXA9UVccV/RwsiyEoAFn+mH7p/IiRqO3wABUgKUDpZSL1L9UY3STVK5VPy/qQeAfaXZVmbChg6MzYRllh4oJpgQg520cq7QBqoFTCIcGDVom6qLuwOoOO5ROXuSNtAFR2AyEFMg7P3S3WIk+XKF0i5hOd09V9t5XRyVCz+oyApRJVBKeYNdXnp8XYU3OlIyk58y/pQMutlMSfvMC7JaXpTi/NgMizBd+RFVVOTJoLJCnY5b7LJ125wEfjlJ+iT7UuuHKMh9le8/uo54gz2lnsfyJhpjsv6MKgIBn8pLaxCdoWcz5N8HqrK0U5L7Qa2fE+MMJm0PA+QsZtRVAUoBo6ygjzGuiWYoe4RvYKcH/2gnEuEbVrks1u9MINzlA8g9cySwT1Y50FYe1r0fUZptrn820JRLwd0VyAlrNp8lpFQFdwFTl7BEhqLpjD/X27VKGbpF9TPJEJ0oaNxmCzCGZJ/5RUBz5cpGZv9qvCQ0rc93alD37B3Ed7LcaJaHaWAdKXybqfJI8jGG9CyzdCf468NUNch7JmviRqV1rA93AkmZvDBFqOrb4GaOhSVS7IqJ/hl48cOn7TiZexAjXfK9Wnj+W4mxfxZvDWxg5eMgk2SbJkwPEk+gMRUT/caVpLJpbYH9bHRGfPtqd2OAk31eOmWCL8UZn+fPx5hboiDFUnMA+1SfR4ZESxbdvgvymForAPvHBMLnwqftN6CfGRhYosoKFlJhRg62eEZtSkjhTlSeECQhnln1/nS9NyUXWH9smN4JTTWrByCu/Rdb0GZAJ+mft6bEvGTzLmnkHamkXv4D2cFqYF0XjdVITHmlpMTsxh8Z6coTEMWj9VRpm/v+7Gqac40FTndaeUg44IVj2gbYoRYXa+U7nwJsRw6PWopTxtGatQ+KDiqy8zMEUGYGld4Ox+4XgqxlXt0DU1WECsbTdE+HDVrr4UToavX2KXzVqY9rMmu31bQDlPDRxNU3YiB9BCVXk0bX1hVdxx5EB7lOCk4N263EQeTIKjmzSZuZeKs38ri3l3cCV89PspH7L6ZsTyYCLNDqlEmg7J1h1PP2bsfKn6HlNjYnmnjXCDrk3pEUTG7Owp9pxufZ91F0puJaPkL970bTA6bdzEnBVK2q6YALGqwTRfTXTYpuXLIU74brdPFTV3osvCFQSk+aNI6iSiI2kXNEp+W87NDPfKhV9PddVI8OJh57wUzu31KBTbDtlod3znGTo0Dk8MzXRTiugxI4MZo/4V8tIFrBP6KxrRipoi4LIEuthaoDfeuzi6Zuok9VjLMtpD75gPEJ+hDeOA2OL//tllUrL+RLFsOlzgHzAvkMP6EGDy8EcyjpPdyQSU1VvGzfBA6hHK2CF2fJvKsBkJArget/coEkJ6DZ4VoQzkN7575CVnDDOEjJZM44amq2DF5HC1831atd73XvCr1QTIvk9RNXKZz/2ZtzUCf2blf/kvWeFTVsI/r0ClyUL2nBDh7ZyIb67TVqfQvPVr+Fz4svDyli2LU2WhssuQ9GbjwXGE9CDiW1Q80jovaW+Vrx3UktK2nQdTyecteaqivUu5oXeDKnBIeyvx4TOvt9X9r6vVLPBiWEZaRGTBi0WzLpXO4HwNaLIvNOP0spGkhwdy7NctM22yQqZ91FpyZz9C0cXZZS+Hyp+TVjuwRffA2IgGcOKCnAH6MWtiP30wlsAm8bGfh21EncfDMpKz0qfGeDeYp7d6x62MrtwMj9LsUvxjUMxEp8RGnLzUK8vSDCLRwmA5gGcRL9RuvyUx9HoMcSynHUFdxWnoclsYO1N8Inqd49Tm+wOtUTptHI3twWfJy9wsttja88vYIbOaCbGCF5irPSykGmnseBZ6uLtSIVUhbyaYE0Nj+LMWt6aY9fRSHATcL5rFIs5wb4vX/hQBnAHOnS8jHmcmTnRP1A4DFwHbw0QaY0QaLIScfWzZu+RPvHlgj79/0ThRCYFBaevo/W+QTpiW/6M0D/pHvL7JGBBn4LVlOqMLC46SyJAB5OGvXoNna+L8eshfFsl5OnpMtOI2cGae7lVJJmEUCCLnh5BPiWIo1vNLR9RwyBYMEhlA6ahsJ8XOgoTC/NijMlrhZrbQStbePxhV/h7P1HmMn+yGyiFlPAZasdfKqpB0CzqtidUrs/55qm8/m5n9+VQ4hYFMAYS7HgSmGRJYQgoyx9IUBoqWncmFqmXaJKAECrCV3EgpW1oqoRSvowVhHLi8axM6kd15IfkuyBq3DqgcE1QQHOPsSaV3tyTBt/v2UESwZTlyIhD+TEjBLHOcCqa9tG+vQnBoejOmuQUXjxAF+0MQKb7dm1mBswZV5KiQTaZxDjeE2ZsoOzZNspPgsPy5r5OOyZJMOXf5zgBLQfj+z8fPKwudSwRJ77n2080baxh7l1l/9x6v2fpt5vBacrBZrtseYbnuvpf0i4Iy6ALPRF/fwGdeVxiK1r2uyN78bgSlJiBvb279+6mHzEV6b71gctw2WaoY79VxPnl5omTLlsGsZSl4O4dY1KXFEnw/dVBQtahIdRiL+OrkqEhioGyFbc0gNIbYUPVPeuPtj6LPWVoafQb8hnx25zjUav3skvQJ0C4lx1n93hB12pgPbCm/vnm30tCG39DhnFI6BnFczTZ9og24upchpsh39rGZ5/X7hzmC3CpzPbH15UiEh2QM4TCKk1Wrt+0QNmDK/L2CyvRr3R6WqrVN3/wSuXbPzixTHauBPMOr3YE58Jb0fyGHi3Fr/mdM+g++9uZ0QsQVFo4KfmPmM2LuE3k9WMy9LbONsIj5BTH2/8ofLKvV0rt9YHswEaqLgexlgJq6xk+K/jJ6U2UmjvFrJxJs/7DDoMIFavX8jrMGUk7fNtdLV2j7E4pKrr7h4IDrDo2Hpq7YAHCPmJW/VGZY1HaTeSoJ1xemysAjcZB28bQXHzYhn9fEw9aL7Q90H0LDGBJNFnBjP/cVjlfNEsM9KKo/j/g79xrxIk1BDwhWunzkZHQ8+Kmpb6nTt4phsqH2kxYUS7fCjbm6EPcFe+u2aemeiqwV8JSXhBv8wOETaCEOE4eiiEG1I5fMoc7+f2wPKsVsywXVdiopd+GKCBG+tPmtEqKC2kKlV7WSxvbtft7VgxydGlmGMcLfFTUcEigzbc3sHgo5Y3v9BsJIPo+Hu7bsbVfVv50cCPqyeFza9+V8w1dgajShXI6GnPM2JR7Sw/3C3VD6oBx3RFbAAfG59dOecIjAkGzFn+xP0i7qJk/eDQxi4il8VJgKX2S9ZiCmBR5nTY03YqRvi+kN7JHJD2NWq+Y8UqleNfRWHECSLKTPy8CGuwFFG3YGDDT7r3l7uG0bsKrRB+PHKmtuvXwTp8F7EpfB/hTp8rAADa0zRdHEW00MBN/kC9iwKUE/agJSTsYxB+qMjVPblTyFS9vNs52hbwigrZgZx3cL12lnm8+MKfXgPgwsCtAqbKcx5nu10ATS21nSo3ur2rjOyS8IoBzfL3F7dlEMfsYE6fHMj7VVWbtUYc0X+AGV7CudcdfDw8NhN7nwp4zMfbdn9wIXrYJFI61DrlaRtyaTd7mJmPeE6/oLTMeLAGcGaNjytZZfA1KA9hmWE2gLJJH6urrZbLvMzW9XDCRmPmHahOFN1NLG0YpFFk1NJ6N9llVUcx4wS+ozz/ez6rD9AYBzE10dTWB+GCP/cuiQi5Rdr2ENcbmGW8kw+B4nvOqQwGSGU/2YiLINnyXFCuzwAVnhvPXfv6CCfGlgQjesRCdVaViHxy/iY/40vGrkps1bYRQkgX8E/6s9nrrZp9/iAzsNuTnGOpiGBnw6LsKE3gPoxB8z5Fbh/f7Vb9q8svoc2dnseWWGQViOBoPz7XSXfr5JUAavtbzzs2SQxFUrbN0kRL5mzBFG56iBqcGvesAxA/ZrXKFJpa/IouUM0S+kOqinJehfGKAWQO2zIVC0cBtpcpF9lIbQLNKR855fpC4jpQBfnMvBghOk0BMtxebDTmFrKOU6ceeMunTbc+nsD9e+tyy4ORFc3GsWuz4NnIeDB5AT5b+BNnQkLF4e/h1RxnWTHV3iE3s4/KZGwPC2lE82awDoMbbHqkrIkkgMYJ4Jo47itnf32YF4Q3HgYFkIsEYHnml6+T9GReabj/3P2zvLoEtp9bj3AS7xbB8//cA61aLp7Uff33+ZlK5LdEJ58EscLd9MPx4aPgAR7/drYGC0Clh8utREZu73QVoMpJT8Hj9LvkAc9GNJ/z/eDTC6KdweLq8tK05XKEHpXSHSoWr3Yo9OCX4CudY1JgCp+t4BZPPBiJk0AJHG0PTP88ByPstX2p4AbNu6AA2z+/6GLGZMPVc7d/UQ/po/6kfgUEf6tWxBS2u33Coy/aLLfnwoB3OFa7YU9ED/mZeVqtxXqFPAyulZ/GQJn8yoB7DkxhoYKOzsBk38VdI2Cj3VcbPi0KxCELVDGO7HTwf0g8dNpZx1S/3CCYilS9YhpGGn1Dy5oxVfeUzZOYa58ah7h8wY/iK1+YlCk6eoVLoPwyBjE5PS3SA2ad2fKfb8WJt/lSYBFxSLE7DqI/BVV6yQydUlBUS8DHkWU7nAjvmisDLR1HwbgQYyM/rD54+uHX6+wroARxQcaf13Rgh883dD7IBLRJF6MGlwWGPB0yw6AgYgyaRKVqCuv5gW7+cbs7kWVaemXHxi/bCACIjkPyDdKVpSY/xV2D8Cp9W2A0i8O1fLCuuNDsReGrph7BgQC1cIiLHY7701uaD04u10HwGDdxgyjnxSjmEIicZXm8fI6YlcIPHLiAsxrnnR/V4ZSXRIXY/ud3jJmKr0gmT5PYp60+7Wg3q7VQNCExWz4LaSimuE1LpcvLUGYYbwrJQWW+lscb0F+cOYj7/agrvcKDmAfFAboYkvVpuw1U2hndhUuC45lLRqBhz1LnLw9DpaPsMirbBEn45pS8bBxm9uZ11X6M0GfH2O7gBW2gL2p5aPb0ufqiqU4nqY0f5yE5tZWcSWF2VR+ga1vH0TpwyHLOUeO76pjlpkKDMjs0R3gP/wTvfwIUSE3V1Z8jHveVWFelH35FdOjOUvKfiyzzk7fHE9DFlxlS8xJ5ZMPz7Gn91/Px5qUESSo7VfyymhB1hwnoyPAqJ0Em92R4rteU7t81SqoTwoS6zCQieEbTIA/f2o7rRgt2MXZAvccP+z7r66uQ5tlbaWOc6a8GFvYpL+SDxWD0kxf0ZCSNG+lkP7KBheJjigSkn2XsPgVuvlziWpjTaiJN/ZsoSDFOfls4+SvVXNrQBpombuTYVrItXGD8UIUB6Zh/LvWS2GzUluSrUdlX3LEj541BBem37nTaNxuqCkRsfumtKnCKqjU5kfGnAT+9GG0f6kThLtlhKhOrvBIo/cMsfXbzRrrSORksjXZd6bfqhUmLxwSCABes1ARAA48wlMobG3D6btDzkHsAwlqIOCzoTkD5Aa0P2gQMiLW6GnYVA1xA5WwnbW6sNVvIcVJf78EakqwyB90qJXA4UwQksZu7Q9OLUG86XyXTV4N23QoaGvHkAMwaVMafXfLK41x6emTmFhuruWovpOfA27ts3VCZ4vVY/2xGca10jWctVwqCfmaIrjIDnQpDqgOUTD/7XtQwNn8xAMOyqmWBPyrGTAkoVmdz0uR20cCuco2Enx4f0ScWTQ2mzCuD9AzvorJlUYwNh3ZLgfElMYzw3yRK/rDtsPYulQeFI0OEFH9/weIde/ROuQGh+8d3XH23Z8Kpp4cZB1RuTVcsZVWQ/PUBn16IGL5aAHbnq71qNX8H5cA75rGr+yoM34nJfK3DJ0vHVl0fMbImCOFAVZzktw9BauFcUhWXL0VIYTFj1WH3RsNjWft75p2ejWg5pzZHQn9HA0nLmaeSugDU9Dv2peC3wC1yIzR4gjbtERBasci+/jKSG8AAph3whfK+XyULYAVV5OSxQ2ckdF9s9TnLpoddWZ9S8V9GltU8zu8mXE+glcmZz47eWB+qQr2EtFk7yqaVcjspffjtZRJ6jAY/JwaqP595S/UNYJN86yNlw9wqowSaESWZ6rXxCjHY472pR6qSmF+ySaeE7hb9wNF3LHLpFURm8IYLn3CQU0wM8fLHERGNrC5eAmrJEvmmGv7bIdaPQ50nlDOgJAZgAX+hp/Y36IgGTaobOgD2Zsjp9COA4IccWNtJsBJ8nzestzENsMp2fA2HZ9zzBBVwj/JVFPwm5Jo1P580R+poU8D4DWQihNa2u++cRnN2h22LuNtrOP1QJ6mRpIUaxRCAsgqr6x/YmNfS5CS5aRfnKSDRmHH3TJlFBO5yNZueO6AWHGWqZbaq3Az2ckI73FIkn4dv41knbBLCYbBWKFlH609FPwXFT6QTyFQeF5WFVgKWYe7wY+FvYeOp0Htg==" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="BJWQ08wdrtzVzgom/pHTkPsZqCa3uEoRSjlSCqhykutHhsv04oG+4IDNGvC4xT7/Bx4z3b5K0DH/UVgvHMyKYyNz4tM+K5rispdPTtQAmydMBZIvTokS6xvoc+H4PeDwGBKKEF+6MrEZLiUJkcUuvQ7BI/5A2cqxdrVbhy5JVrFnpcyz0Nzn3qCiAB4QP4YNGnBVaJiscaeTgYFlucidauxSd8RQbNQ7OoywIrI5VRHyDC/PoqG6Me753b8UI4cCBKDGL3dpmBbr1mzKPD5MuGKfViNkV21ZvcSwU3AvsLPryYumtz7y9Kno/Y1DJ8FMVFN3EuHX22n0RfCuDLMfIA==" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width:260px;">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="#">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/eartquakesecure.aspx">Dos and Dont  during earthquake</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/externaltenders.aspx">External Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=grsGMsqnclzsclld">Kochi Metro Tenders</a></li><li><a href="/tender/Default.aspx?cid=3AGNZpFcFwwlld">Lucknow Metro Works</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender/Default.aspx?cid=yGEt7G92BVAlld">Noida-Greater noida works</a></li><li><a href="/saleofscrap.aspx">Details of sale of scrap by DMRC</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/advertiserlist.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/DMRCprocurement.aspx">DMRC Procurement Manual</a></li><li><a href="/tender/Default.aspx?cid=vlVO3VPqpMAlld">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="/otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/otherdocuments/VigilanceMnual.pdf">Vigilance Manual</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>
















  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">81537766</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
