<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="vnePU6VDFY7RMcGcxHdPNsWealmnnC/MGJCj55E2KX82c9Zy+5VxiBvKrv9vLC/9AFZvB9bOcoCkKZtpCJ4CFXup5/Z28UZIChw65dUiI/xV+5JqTqrLhSrIOkJF26nhbXUPScUdpf8nSG9+2ZIP3A0YOLO1nyd4eUbmXwl0BgG7vZ/ZOjtQyalzqBlcuSAuTEwhP9iibX+NtULVzdtNAXCtcuB+rqgCv+TYAZNHHxRCHXlMztjUjoz0JiIJYAIYUpOyhL+M4e7mllsF44spOiOKkADydud69S3yGjH7LN+GQpo2GQO4vsrvCg/K1nMwc8XubZml59o3We2ilLJi5Gd7FuCXTO1zYvcU7Hm3y7KX4y1lxBY6MYfuLHAM597/o34oR7g99SDlV4/a/htlHGYvosnUkl3Cv+B0lrQ/a5n+R/lmck4uLDKDJYsDprtetB8elgXtI6c50mCgs8dHzvnVFgmzS072rOeUHo1hCJ3NszrzCGklIJItoeuGBw4LP/frDm8wTgooZPWyXMPWe8HPUgt9y96SWTCtUGan5Zcr569NAA+475m+Tu/DW0EOjV5fT5KFBtCNNqavcEnj2rbz0NJB/TerWDPoSQsxGg8Bj5dYNyWEPimEGMFUFe+CrVMPGJ9OgNAz8cOYpFcnR7t7sW0tdfA90WOIczK+yDfOlFbunrUSj45znQc3WJGl3B9M/p2Y7hKiD3rbLIdyWMDLUgmsucAMRwUaXT5KD3BpK3xBkKWMSmdtGY1Z1bMUoWZ8dUhzubXD3Ce8ghzuCS+DgF9vyZftL6uV1RNZh36FeI6uL+DmVMQtEfFLOh2f6cib1eWLNaIF7XwIaDuPu3mqwqNLe/cCTNCpF6G3zRTvf+Tl1fH0FwGI7r3Pi2y4lN/vRXBG2Ds4Zw7xIpvap9zctB36Y3QMXMLAkqfSmpWTwEMfM116oVC9YJs1JA5uxK2E5uNIy5hjgssYagjrRJN2zQ5G+gkDVvny+bfZHVTjFjBazO/FrRyb2JYJ4T7AD7RR6sLu8nZl+hXuOchJWZwrI/kmIgASQscWha7njUm0LuCzkVEAwzErfJskD+XyUuJVm9EsrtjXLXgNdlkRkPwhcHWn4Ymp9xCqIo7RHKRvk9/LtWij3dFBCBqhDIAwLyq1Ljad0vEoVutzbtNYLoOPKZIlcBWE31cTXOol5vnfAe8oZMHvOOwPGvH3iaFbFEcwrs7+B8ps6at4tZrefVrwAHlH+b7xxcRNYefqEn04GCwauRDC5lLx4VVfxXmKOwb7jIeJk3a6Qfq06eTYrKFGntV0Zu6mgE41wQoUzko4UA8ig79O4p9z1idAi2Cur1A9lRlVVaWwABGbqc1tlG5yZqq0njF8niEazU0kbmrf/WoeMawRqd7/SmfxWwb3em8eIsZlqRhDnMWcNopnfNQlW7bchz3nLnWecvMEphvyu+k1GVDXsExcRg0KBnRovH/ONAP/sm93T85XZGgv7a93Y+xAu2lMa75ONnMxWPLbV32BanLih9xfUp3/khXGicKhNnK8w+FWQAMdYBNZPPWT8uPNC7H9CWSb08rHWXwYsfMxcA3kO/3INcAceUuvPqRKR9OiK/if7oTvZ4HaXbOwE54xp3YD62G+PWLseIOTrP/PfDenOeiOH4aloeLEogXs1nsSWdz2EySFBDuNEq+LDQ9V2+Yr1M/8Qolrc0mscjDOZxl5f9MLpOjav6dLIhnMrj+HEtGrb1vCmESr1YDn2WaJ03k9RTyAJF4AHhWluCj+Nq8mjyd8l7FGl5dKbZtB6iTy2u6GY1eu7cSLAvOaTsewe2RCzFlrgOIg/DzG/pbkcKPTDYlrn29K2L15cCIz2PEhahmciCqtQXvWI92Gg2Q5ylhyefnwY8d7rc34mmDJ01JiDrxZtH6GLXBwixW85WQTK+bXEw7jkrJZecIX5Wa9FwGIKdmx5V1iT/F7KTcQ8gbEwQ3TwHp4ArKEfPTCcDF+QEJd8qvOU2uoPMuusdi5WFYMMmBW4GNfngZnyngnY73GCY3FyHTubMaEqaP6Bk7VZTsXZoKW9Y9/EOPoZ6163izakv+eT7ykEyfSOITPjg9VRICsVZpJxJqTZenX2cshyYJbt/pAVnorO6xguFy83Fe560o8/xJAntsNpdrClgW+diU+RH9TPsk9ex0mIVenPwEQNQPUV/WzBgSDDJzhlAAIQF6AF+ShpBi9JyIJRLpsZx0e0weEGFXpJDQk6r/XBzL/vbnTrkbVXtkIjYPymwUoU9O2eN0SUPPdMY6VuN9X1c2OFGObc43Vgx6jGFtpwepplVQumeQTN4Ep2XUSTSxxVgxWjaSdcJpBd/qEZe84MjCw+vydvk+qviNnyaYpAZUomZC918MbLNqfBfYW/+5ig3TSQJL3SLLMKw24zxpRwrQnDeRKUcM1u7WkVR7i6kjhRpJQYWfsXLhxtI1R5tYj3ivttviC1T+KYEJZNZ9NZZqEOliTo7sUAbMas2g+vfKWqoDStbPhTGs8ykKTOynW9YsqrWhbryPuLHf5vbdekaCZ2NYVD7a5RPxpoVG477Ghf2lfDC1tlu/eh2o6MbEwR3ITl50VDeBL5iuFTUlelWDC/TxNBU3qlADt09D6UO86dQOOwJa9x2hYzUdHIrgpUAa3D4NPxo+ZFtWmCQqnCZIFnIBCVNN9QF8saAfVN2YVGl4wnwlcqbSR8jGhtDyuwNjY//sFjXD6n7+v3JbIr06RxcKmfBNQqmgJamEZ0ghIEhK5TDe7iORTSTClYbUbRTs+zIqEvxCuIs8rNgXlbAfRtwIgxQVbqa3iS0eGHCeEJnLzuoA/VKjXaQJjrFZK1DMZhYmwunk7zaRnqbSaf1tnc02IEFALlfKaVpi/tPt8rUK9D/7ZgkK2VSroKaIXo1VYvFR1mjLzIFW0k8ABQugYrssfS3ugximy7vYaC3PHdpJs08dDqXmgt2gjkTGeYjrS7e1TObmD3V1G6fwoZxdPKY3KmM/tNzTqDVjeyYU/wh7lfW0uDwdOyCFbLADJUXjexBcetBYTAiM+DlbNLUhlC/R8aNFwBxUKt/bllCWsqjk7a7WETGpBAz03oFk1mzcDlQ6wSuHay9iM++zD9krA0R1eAhEAkYW9hz+DzM17XFza987Tm+lZpJsND4Uuov5NfqJY232Doliq3UUBixm+eZlBBKSzdypojlEgbBtGWpxvH2Qk3tiNwWYlFtZWbsJTpLyt0q1snfNrNLLr/2QmCVi6dx5KJ+xYgJT5k+FGqVnyw/ka1rJJQcVzGKYW0xy50lXM3hqDKm7iwLUTF4Q4tssGxTYJpujwA27jOJIbih0hrYI5d9QjwDtspPkiOv7j4fRra8lOJQpNhKQ6O0NnBA16TLXSsfILiqmO/FoKy6Hl3aIScBH/HHzBNNMkh4LsO6uYQzjQ1ApolH9D5dDDfQyICr1V+zO+MDU1fBoG1s25Ge6sDT+7j1/H3kLI8kaROr8iERLbe6UUqMuZaAPfFFV0vZuCjOZBVqSRhI+4WeRjYICJtc5GTNSpq4J3zi17qEQjrtKPVpiqpUXhlEYHSud9eRWRP2V44XlJG4tiG4JlfNPXlVG2jyfnwT6w+4QWA8IMISOfFx3HvfN0bsxoyqsVBoPmzzYd43glL5mRiLXYKSNHBIcBRgn6HxN4WmT+cVwonoXkb/mFh1F1kVA/gQZjEuoTQOHXMbK4kdtbkX7C2FA7B9EKADieRBHSXg1kVQ0k+Hl3M9eZNlWmzO6hwPCeodud2baNamOm9HR/jwceaH5W3zBTDV+fV7TbqhQi7+XfSDb1T5hIxPiP7tF5gXdv4Nfx5SUoWyAPBHvJdptMl5DBOVe0RyBm+7BIfU++yVea6OrmBPPEhonew+YdMUVAwkYAH+x+xpLFAUabYSX92E4nI+12RvOVZDIL4q+rDncU5vS/4xTEBIWuUg4M5VcVYTdPWFR5XSAQCex89gueDTNsnHZJXB/Dk3+RlH3RaY3awrXLbo1ZJohT6aDkzSO6x2rUyqzeGOvSffdXs1zR64LEPp+n0P+fF/6+odG9LO0BeHs3slcq+LlC3Eg4s9TdKQybsvDzaUtNh5XP6rAE8sQmQ4bPdsGkWcDX4YSvUyoBZZJ9c72CiRyFfszwNGLCnAbNyXCVuwj2t6Wb+Fdkvit2Oe821oCZXR47Co+JtH2WlAUZxxUoJBKQdRull/cQensBjCVUig+8llZ4BLiFg/oO6zi29l/m0wwCMY2i/h5giNZJRvf25VSiY5/GLUX89II9N6mdkXlt9qldF4fM1DSnPA1W13YgLf+F87S8T+YpxQ0dORLxgMAF4G3p/IHWeOzM/i5aomnecad+8aeyXis745y7TOZUdzoTEkD3pMWH1h1SgWZBfaYzoWOEsItMcpMRqSKp/pYZQvQmW49a05yGZF6zd8H9Z58xuno3F4yojQlZ1iz8EdJvKH3RqluliOYLp3opatR6yKV+bZxqtGvykteXwv6JPSa1R46RXa4VMiLmxEyKVunekH3rKRyy/ni+8qcYtew50AGxtkKu2bB2QHNnBIbwE4B3D2qCMG7E9OQSesCSt09ZZscJz2uTlQcygLej9YOHaoI1mjUnPlizGY8WpILJGdJS14Muhft7xsnvICd5zey4STyopZSBmtxQIbbde89wNPyD9DjaVG+mRVX0NFA9iBE0zdSSfsfp8RKwj0kH9lVXXBUQESHgkDAZ80gSFEtPXsY84k1siXLHcsktgnpOR4xgfiQaHBzMkL5au9y4AF4xmUERBoRZnCUOdS50Rd/xm/7dmLm2xiobCdUFXitbsJmmYBIZwSRb7ghw6O478E4XwKLqlupx1JiM0cPxxGbE/1Tt2lchNLWHE5uyXmKYSsRoNHyyMfLSgedA/zLq0707s83ukKy5a1BG+efNXmIcm15SrriNmA4U7a3VvETJnWPxZiAq8m2VFqqOkPTCK745Iv8PB8/3e7j/9nFKmXXX5koPVMuB1NbD9y3klIxj98Ht0VMbCMoNNiAq+OAUgN7aGcTuOk14z6g92mWl4SvYzeru+aVRjDMcjJ1ytIYVChXTAYNZsWAD0rjGOH0vXgjwONyj+d6F5MvdRQq3ALXO2bP2HqsMnziqGftPYdIA/8v+v+4WygfzrIJc5Op5AtBFZCcAv5YUWWPgpxv0aW4ZfuBwUio8h6lScxWusVgnLft3hFLTTV5ZQSebyYz0ohPZYTYOsvvsg+r49FJdOwu/nL0VuL702oIwtOnjtv8QFEQceJzRpji0i6cZr/HwuxWf7WmBUhPgY6eaEAXVvtl59/cUgI0lUfMCBR9rBCyvVTSY5LgmsT3nasHNnqs/rlxAU/SCqMLDztjTxYYqrH6AddCv8pe1HpdWm3dlgRIabsXhKpKw9LKmz0WF/dHh5jPcdcel1UHL+9MIrwdvd3nREFTgHmh+bIc47VT8/417aPztP4hF1xyn/8DfsQQpPiofSaHN+55J9ulXG6e5GPe2N4ejQ9KH/EtEdUHSvI+D7ERXr34Htnmo0qNrO0vQL6EHsBE7gjoWb74EYtgzXxvq4TJVR5+yyXofg+HOOPScn+vvIVcuJELDCDD0oGo6DTv/U3qgvWK/XixataJRTNe6GJ2kVtxd5QH+1ChgbrOS9y6D3Y4tSQw1YTWlYVXqBtVb1NMTcZ08R5GiW8/9JpZZgjrXyFR68Sop7eKuIDqJ8GoD3Lnk4F5Xq3TI8ertAN5mvOynpOzzBKpC+Lje7/5y7JzWZo6lPCacYFRkvE83NxcKjObsYbc4HOsyMKZlhRwoh4oM+k2mDkmeweoanBBxAjSIBnyr/BqIoPrUsZmosxai5rrb47loju3ePlcj6RIvK59S9o6ENXnlDbxBmkj5+pjvJFiyLhpqcUHEfUpFPSxGzjt0ccJsac9UAbWvRkLF2St6uSMV+UHLcBNkT5009WbQBW/u2yBQbBPqsrFqfnfOM7YK1o5ezpVwcPP67Dad6mCAaqslBR3x3DJrBDi8Y9u0yChHm17XwWYZMx+/RqIhi/pdNDOyVDw2r2EyIIk0JZOOAMb6IeP5AG5IHFn6Zi+a8nzdvRyV04jYnVePTAX6Q/V6j5B7fcPkxVFtrPRxhfELwzeUxffcqIoZMbM/U+QeSXghZb2FtO+1Ze9L+R1dlEAGu3RZa63SqGPkzXw1NcGXsbbH7Fe3yLlzXHAFRk77mQ9T01zw4UJe1N95IM1A6j03BPYkT6MBdGlC0+fw8PQoITlWb76eFwkUrmuTlS9TNUVrEhFjabr3FC8DhJ6gIWa17/xsAD3r3/jLHu6LZYOsjCPwq86mOE1wopFpnaH9bO5MU0Uvvn8WOuvdd1YfDBywWLvCMxcPQSXUHMlr6PNFHaMxh0H/41cHE1gYY+mszK6JnBIAgdkbUzp7FinHiGQLBqAfzmubcZEo7QvzkSTSmD/1J/LbCiVXQt9ZtqxW6/gVh7wHJI+rdfAhDsfuaMKzVirmxsx3CDZgQP/174C68daMlwlflUUeBZ7kHXXbIntcAvsh1df8dAQ9eDxy4uXdsIaR+X5OJRM1LY405hUGrpX9PL2iJrG2JqcHqy9qgAivaNlwakMANsYy95ZNvzA+cmPXcERS9cIIB0dRZTXZWmsCg5dVR+TWD/BU3//xVjTxQNyW702YwsuKiMUyrN5oxbNQFTk2uAcgFLzPouezkK/HNrVZv/B/LElQzStl14Tlp+1xpKRI+5wDyhZG/ToAxmqrBoN3+9/oWLk+82ZoLOHH9nmWCIFN6E1CBdFx1Z2S3jwpLH5KzpY/+xk63g/z8MUxDwSoJvrC1B+1n417xtiYSSzQkGoo7JPGMZNtZ3dFL9yaMbs0G7/CjGHcnotkBvAybVHqHp93cxtZCMjsl1sDMsNfCYe8ZTx+aG/Zl5Sm7I+kd5cYu+A+RqB/fKmF1JhfuBBlgvszyE4Mk98FZPwN4BZieC4V5dU5P8Kty2eCDkuQGNBv+q8Qi9FtP4mQAyJ2YDhjbbqNrxiVyUIEc4Gxa44aMWVJa0fFAn+Cj+8UTaqo3U+SLBcxIY5OGtq+LbVc/7ihi+YDFsvjcojO5oCC3TH0ifZ/U7he6b84GsLElQ8nU8Yb9PNZq4h01na75YkCjxrEKo+KR/71f93Ud8ssMZqPb+5Y8Edasce5lDDf75RaCWkXoGCkDde8n5YRL/bfPEN92eiNCgPcB2HntHV2AtlZwVsCMV78rXzXRDUV0ST/lHp0DjRUsfqK8OIhG2raerDg+r91gjMJ4rLxuEWt9i1V95oLThuAcBnfF423KfC9X3T3DQlDMPo7NBAXhG2fT5rVecEj51rKhtp1yY5+rF0u+hp1aNh3V2qiVPb5oS9nBQTMkB+ciGDpc7JqRW1edKmw29GzgxjZvTXpT4fcHjXzVeEehFi1YLVDLpjfF0KmqvMUcaxkRNz3jJqrU7t2YLmhqy7c0Fu9ySPMjv+YGi9ORiFVApDNwHXHCHULSnvs09az2Uqkgrul7196uwQO4dOtXb3UZHEkwbWQHJP/sSMha3cpdZj4Kaj6Hc1cVPddhDxj6t54ogDjkujY55H0EEi+qAta+8YeMP7tBIGe2ZFKJAQLmcIGcJy6ln+b72+KYv3IsGPLBecsEAvUgSx0XdIFTYFLRaPUBRDbo9rviLNAIgXLPrChMeKDBeWWfwouMq0Jg7rHz7luY7EjKLBT/Q2AwN27ilFdxOEHh9yQWGyZM/kuP3zZs5mmTArprOcPaVf8zySuIGTgUXM6sFuaRdCBy7dv4cRHWY6qKlN63IBgMLCHNJK4U5+MZ2egSRNzW6KLF/vgJbtS2NDClWztTXl2GeGEg2wKuDYB1aeffFqjgjb+BTEKK4zCzcQPPvow46oNVzsJNIwui054J8wtReGnLTd3kuhJhkNgpw7Kk67zlEjfMr5+YmBHwTZgGlM4Kix10Vx26lSBYXr/RGX+e9AJN38IkPdvcuYRXLavq2vsOdK7YQMRS5GIWmOrX/FLDFqOROtPIrcryo92Y3V/5ILZfeaVxOVRD+5TBtT8R/dVarPd4lUPnEVHMeytQhp8Xo+EHLs8E7i+YKdqKZbhpFsRLnCgKswnt27AuFscKIwJ2SQqNWBLZpeTkBPB++CPCRTZDn2GnYAjAZ7vOWWMsZRtyUiQJC4YsO7Il1HnzVb9SlbYUzBaF/ZVAiyo7h9sq9jL66J4KGfrdmzOS3vbhVhf5uvtF4IhyNfK91oIDGN/i3t/gRfTjkwu5RBUIboWsuGJxVxlpkROmTWAsir8h7eklln1jAGMOHnFczYJIF5Ki0y3G9ZMdJT+7Nd/4GoYSpIXs3dupj3vORzQReqHhz4eIFp2CdxlkZpDAwbplnkIb+POiQirhRmeXUfSENFV/T1Qfsg23u0mD/Bh2h03j9wNy/XfEahHmCEXNpQ5fsLzsdKQ31j7KfgEjpRDNorXoy74mqJH3lpoqU0Igr1gLEn8jZCyeR8oB1ZIn4fA/M+PbrnCb4CQMSBmns2j4i7i14Q+hYyMWO22j3S1TSjZZts1hQzNZL1IyG0yv46b9crf2HQ50X7UFlrdn04m0Ql6fxpNABBJdBpOxyqh05kxyIRoJsfy0lYFOZI4Cr+qY/pHJuCNXxUj4fn6tP4qyrU7e1fO0NCDPuLv+WRwTuoTwDwc7xxLj1W3kAil0ah/wxNvUYoJa2tM2dINAd4N11UHuRlzbeMP2z/VYLSa5rlKcKhLmD6A6QrUW4TaOQkEZ1iCN1qMnXoQ4E32V9BPcWWxWpwpghqfFv33sq+FoT/Ex2b2vd7MCRWKVCiC9QY6xRhuGgRFplZxtgKo6IPHHmT7SVIGnDSdKovyKhNTGD+2xnT/7KluS1pS4Kmqet0FpwMBjRKu+w/5GWpeqJ5nfok7BGcAAM+2E17yStnxYj7+P5nda01tymh28vQD21Dw7M+Hr7aw66P36W/xUYT/TDWco9f6cecMu9nuk9uO9DAa4dLkDmJCoE/gQz235YIIw2H0dMlWeqfTRXdUqIT5Wpxmg/kOMfiYiFYRsz4s1i0ze9ITHbfiOIYD4sTsTgaiWXwFQ8svH9ZzTNbT2QV18JWG5wxGRWaaKXhwJjOxdNi283Snq37FwpjLt3jVKI22tqrV2wKT2xWw1+3MXHVucW8cDXl50LD0ILzFQWX9SKWXN0XQtxaJzz/hlmpAX16MQF1aT2mpfFwsKgSali7h8+zyRqpXeM8E/LzvZf7ONj/jkUvJWrrckdbtEpBaSZc8lGPYjeTf4/Ye/yEpHDjxFv4t4oCuRx7ajiFPtK3f1YfPSt0nZI+V+tbTW3Emv26/FfhS+OHq7i98SQP1q0rALIp3Cqx5ViFOMIJ5WggStPKO9OSEFYr1PkHb+wAkm31jZkBlnw2ng9lsxBFIc0pJytVl9dIk2lQUuDnBXiwj/KgS1daNGy/LVTaPIT1vr++jDLqy5AS7LPRcXNJwcavdYGPa6umjMkB0O1BA1bH4Al+LUeQNXppPZlFVZSb5EGO6cgjxukoCeXPjU7S1nEoaJFmYyiuVP143PW8AcRiH3UzOImtCj5RnnkVpCeKztbOQYrvhWZ8QQftRtA+WzMAISaKwj33yMYlSTOhB9EwQ7N/yPl/6KhxV8dV2pym0w9k3dA/jNUf14xmDnTgN7Cm8HQXsl8bgRYKlcyz7iwX3coJpDUTZb69hlSYSnrEhn8QoekVyzGFY4YwKxmBamI8pl886ETAJftCoiiUHmNRrMidtWN1thPHVT8PD9fOt2QXLHRUQ" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="Td6JVbFyjENhWAGQxiHNJmGvNzL70a2TzV5/SJPBghMimR0Um8sf2ehzYQWED5fwTfl+PCJ44akJ40bvPS889kxM6123gAKdyvlrWUDgaMRY5VwWST6Psc7t9VtJllASwmAA/unTZi34g9hCV6z1Xw7Ohy7ZdVEo1XZaHp3tKPeCQw8fpZbn1aRPUYzxxhJ+4SZQlCnt1V6I5mX8242Rb6q4zmQoTybeLwn7AErrO0v4zBta7QC/qNeGNdXHbkZtWWIgNUxrF4LTYCcfke945KHQ7i14GG3Z+Mt0X/RZDiMZDKhbU14tTQRK8TbdCNO+" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul style="width: 520px!important;" class="pass-menu">
		
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility_new25418.aspx">Parking & Bicycle Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/MiscellaneousDefault.aspx">Miscellaneous</a></li><li><a href="/otherdocuments/900/DMRCDirectoryforwebsiteandapp_8518.pdf">Metro Stations Contact Numbers</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/generalcondition.aspx">GCC & Other Information</a></li><li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=nclsis35ZWYbHIIlld">Phase lV</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/DMRCstore_department.aspx">Store Department</a></li><li><a href="/DelhiOutsideTender.aspx">Tender for Projects Outside Delhi</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/vigilancegallery.aspx">Vigilance awareness Week 2017</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>

<style>
.nav li ul.pass-menu li
{
	width: 50% !important;float:left;
}
.nav li ul.pass-menu li a
{
	width: 100% !important;
}
</style>














  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/11/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">101899836</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
