<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="s6VIrcv2xB9U8w/zEDmuRbdV371y1PNn/KjMyf07kMGwXCGtzR4taeh3WCLQV/5SHbn39VS0iovpnRrYUGwOW9CRsCMalCVY6sADQRlg5a+p/QZqgxpfzTyQnDTRyV3wWQSdxWCagzTCRkYvTqi3mAAsG1yL2xW8F9aJZiJGNMMlQyFeLVq28S731SXy1E5ak3RuJxdE9IZg4F8yFlSeNRLRmELufkCPJdPjVG+ff0MO72/oUbbphupScd3bkNo7z5ZImaTtKPTYg3I6nxE6oHyNa/B6uebZn97+YPOsdNstWm5nmmasxUGzF9zZhREl7CfwhaD65fGDOmN6u3Zc+JZrtk02T+4+2g967++QUVH8KolD8YahsFblH/r407Ik513AsoWpHhTjbAa0dhG+IeYKip0EyDrX+ybubq8Bxmj0FNL2VF+W8xrXblNsYidSaJOeqa3/oSTjdq/qUCH64czWQ2kr6IP41bRGfd3ocZvq1PGLrUd8YVlqFDNDSWaiBhMEYvM937EooQyrhae6aOYD1pSVJrHxP86vIUp+rBIi7qrrGrZWfa7Fivpd37EJC0p51y5SOHYQx+NCi/U7xf0OAzAnbGnkU0xWUr1bxjfgmd6hbW0YXNMX3xQqV/IwieIDDWRU3WY4xKm8DBrrjEWIBeVGBl71eDBOQ8P7MWQnj01sb9OAQmJb//Nh0QXckwRc8oH/CF9pizEycYAKjAUD/zzLM20CxLFXpHBrky+pUYvZLh5nyLtjkKKApoY2odGy7rx98JCR+OiA9CxnPkHUbu2DtdfoJsVOjT2vUf8Vznyv3YRh1eqT8MLVRQ5Q4XYA7b0sgHqNuiIjQl2t/aFLJh4MdrLhKajYPpM693YghadXWR8GVSndzmR2liX8ZAeet2MdLMhVRWM7pqaznRZUM/wB1Clo0zqWqhhiBhSy3KNaOgsUk2IoI3YJPKKYptj4lS8bu47RRPZ7lgo0vShzJCw0q1IFBszhTnYJxovuEpwtVjapl1A4Dvj1XRrUQ11aov7Drv3fPamQvp2UilmzjZz5XOqbUJFSfcFYC2FXV3VmEhNckBjcLg/e5kHTZzJLMc3qDkjIn9oRyuxdPsEIJgqKpfDyfxDSvZxIoECtYgSU2QBnBQBsz6DXcxnPgabnONWS6UiXN//BUVgzBvxuoeLkwpXFOK80ZpZC7uPUvwlyoqBU0lb8lS0ovrepOHTKVyGhNpp4yHNYfScK2ymRhAOao1vBqSXRBxL7mN3GbOlWKL8BJ4+0EhV1J25eWGTKN2XpbDzb6IiNU7VQ41NwHwQHSGPaBTaT79+/YRzptPsq0bR5W2p+/cb0A3qyHeEqXNizXg4wFyXqLCNnd532BxBNdv+QhuXetq+dItji2z41z7VcqxY9Zbn5G1VFAdq1lZB232tloUcPkoh4UG9BcSIiC9p/41b//txq69dvI+Tiqlp0xsXGrZNUvWZ0eCE5DIYEqyStlOrbXjOrj9tMB6Jl8Wfai93kYSgEYS/VteqqxcaBtNgEYq2ZbfBWBe/o2Ztg/tSCzKkHQvknz2hfoY+wf7rP751VurrNbjHrkvjYeZkzTU3dVdxFJkzyFER5WZWjOBrjKARhzDE/mFeOMK5UnQ0HU1+z3kltDWZvrk3yq34fFTT8oP4USvWdueIA30AjeVMD+DvzG0UxPGmaIOLsA+p8Szt0I4ud+KdUXxc6Qog3hQRQT8rdxU5gRjNtLv/MHYOsyhZMyHOqZ9/8NbuIG8pZ/ELWFHgzQqMGQIJBs/4GzxsOP3mZzC1KhIbye9y4LkBqKVAoxuB/+kcNnSCC5fGLR6hPaAwDadtvFwo3NckdWeb8+w+q6YOVRbYToMYrVFt5syxah/eIkC8biUgsx2QyW+x0RcU1lezqS/ClA6MsdX71FuAlQ6ICFeqghoW7caghpFUKWUVk/tinA488J0TGZZg3nFzx4sK6fBdjKyIedhmimZnPJVtrAIkxNi4nNBfH5UkAOs9DFRDf4s1HDC/zgi93ZahHINmXzkcQUj/Fuv+h5uBbycEDedG5qwr2nteA1dyTh3vt+Jsp0ezC3XoS7MVpMUIhuFmREsYSk8dm6jol9357hij/ZR4+C49PxHpWXEGgabxzEk4k98nFvb70Id4hZTQ9lixKhuy3V2MBTNkVPqQ9CPXS7oKn/W/ylZYxdfDpsc+B7pcaCXvSj8QSrtrd6qOFtTEM5mvLYzW5duoOx6jxwcUixJqNcv+d4LUR3tAJ2wSeNftcKCl966G0hYIbG3UirTIDYemP7ipRZpdSsyam7qsrFFmobBsXX6qAxH7WF4ZgQksDTfv97lHDWJq2dAbVmg0OFehpc98QBOk2J/2oxMZ1bVv7/qV7ElUZQsUU39XBlHb6LWFCiv/4o3nBKRkLn7DGsFNiMZP21PwYctvBjhwTq05gb9+ekHesip9l2reofW4i98/SmvDslhm248vn/V/cwSUARszqgZolMvePGDteJtG1FVZzo2lntiqUmPoEBQqCWtUhXTU8s1i5bXaYCtcCKu4/c5UfD3dvIPATaJM3mReWZJJ0k1Nedb/AtZ8p2ySVTinhq1R2Tbv+WzyRdddt0qSYHmT7iGeIKmwBUsEK3RqWiapJMonA63gH+NtBN/shX/MQKkq7jjCNl3HiXosORKBaGjEOf1kLWWaY26ld8CjrvTsfMxdZCwHLwCyFzLoHkxmi4Ba0b8GzXJMzXej97gHL3BfZEOnBaAu8tfCIaWE5kM5T/IT0kI6JRd7NfQvGTrDDE8mo3Q++sHX66fLjsiJq2FWmijqe+aqgbVzLnncolVGhsx+7SjWRAe2C8mrQTsEUJ5T9jC6WT6StUc/9N7sE/be9bQomOkiIp+njd4cw9NlKnOfGO40XJTacBINJX/z50CXZ+SzN1pnjpqWO/1XbdWbDjMNCSyVMVsuldryY05LdWjuFKJv1iqo8MJWEHeYj0YKOyb727Eq7Q6xEKa0nDOlVNwccFTVuAVIrp7U3CRgS6vYKA2EcD9AXZ1+aev3zTPgtQSG4F6qx9Axs9QDinp2G/msst9w10ckGj/b62yDYizh4cqS1Mtg3f4TZsMgYGANB5/+7bNOR/KRgUn1gp0I4+7xwuYRyyqvw/WHlNsiQ5UbtKStoyUe25GgjvFam8PHlxfYhwh985GtomZLUHRLlRyL9iU2Zusj50YrUz/S29rswoHojQhstXVt4v+Cb2OPmtrhMv5tcCny5cGHDtmFOs8ZKZXYFy7FEywUxVUmvFq2PGjmchXnXGNtxG0dC/gyi1OI4Ll/9EFdjdeKkZN4vOnXL2YxBpWXN+bE2dHwHj7p7mh8PQSWZnU3jyQUHNuniREpLHfPpQj4qqmWZtQbR4yGnKy6AkDfaj196Y53UK4vy1woM04CjjSjHbQL/AVOGe6gx27KOtx9EZ0E6TkoQeJdEi4myi78x1vxr3sNU44hXiRjsVoTnLO3lhmBJEPg3Kb8AWWePhuCcP+f3nT9CwmY3Z45ybfPFOV6K5AgDT198tQe0s55N6tYKn/VGxqSo5zVqNFUXrS9rOjGJgTcCQ2GQlpxCafojuRdQ0pB0vRfn5Dl1CGTi8L6fIkpKMAY1rkLG0utnB0cjZp3Lf0EY/P9+Efdljn+t6KfJJK+vcuX0eX4UtguMFURvvpdEzatj2d3Wv4WCfJzSafQJjjIKP9xTVfUVbxv0EwOpF3FxdCWYwZP3LDJFqp2ZhCw5Dj8+uYfOGP+J0tGS1WHw99dEO3u81AcgVxoa0M+V0df53JlPW/jh/t1r8VLOKoBPNSKLezJPRhyg8dUADci5m6G/kuNNFaRhCbCSDyP0HEsLnfWBP16flffmXa7AoTu4HIVOI5bn3N6QIlRC5sQ6SF5KeRrC/o3pGwbvpbK7Wwx++pXuy5A6ef5SEv9xXVsG1zJnITv5mzS9OrPevvWbIrsdPdBW5GMSry875pbfPqJtGjDcCaFkBZqSYbwb3GlhZbGPQMppO01WD4kMkdkmr3oWLb3OD2swYctuv58TPvfCWa30asYd8vFP1QuyBqA+2F+qiRvEx9CxbL+Z7Bx7+Az0ps0Upbje3fY/Q55nUvB18MTE+1hj7gkjRDeC4+J/9B7rAhLt0/S1h+sK49z+YebRIXifdEgQZBWKl/zei4VhEX5OnDvdWJ2FFcuTF+wEEehq4M7BqZm1pZubd8DcA+5F8Y5KYanVx0i/Cj6wef+BKekYKB3KBy9gjXaquUCExcaUYoR07ZpvLDaZfqVdxft/lMD8KAB1SQSZk9RNyFrP+LQmEh0v3ZKeIIJxFvZN0d1IpSoZyII1oTZRTtC6YQS9/hppV9rJuhbqhzciIrFnUTDxMRBKH7OcpJ3b/WomHkK/AhGMQnKXghn/2lsHSFwedDsV4gotnZIH+xy1HwqdxjiPERXxW4cZa6SVpjZ1EbgNCrUAUCH43NeWclPe0K99xqLQUNIVyyMnpk+jKh1fSon+tadElS4ykGcwOxHxWQX8w1N0Uqb8caPpsyuK+a3WBK+JzFQYD96Wa2M06hvBqEwopUabCokzmmwOM+3ZnOZNCEWWHCnLEfLExRmaTWzr0vdXxu0IH92S6KFUMy6UsA5BpEp4gClnrm538upbZ33V+5wwWfWaOcymw10CqlBqOoAgTWuVlpFtRrDes7VZRMXa1uiwghuH0AlEP0o7uKXaol8yT3Jlsm1qH5OPFFj3O7rBCFyxtrVHTdE+q5BUe06oD5OKjSsRYq6rxyYkXf3rel43YQcnyE0YhtKoB607FpGwSaaikwe4nW0S9ZQ4RCSE4F+s6nWm+jRlxyfp06XIs8AnL74B37XZv8Yqfr3kSS8oYR78EY+dKx98EhOgP3n9T9CCJ8fCH2/Z9JYVfnEYyNgkgPq0k0Qb530jYJ4UMe1uDlF705u20BRAo89Y3HvYdvc+/NRWcQqI9/rLjpGDHHfP4jyxqg/+GcxcdLvFFqbibMcT49g4YAjvhG9TagcmRH7V6+ctcnlJGPB6EQWhivRgRYl7JAjCEWo11PuFS/T55jo0IL8bo5/oSv4VHjlOdLNyo9H9FwIvt+vx3owp+zoHH8GDnwWk68oarhy3vvzuYLwejAhb9W/nB0dHNzJ2kusqNDy/rpqcLx1a1Ery6ud4QqIsBFBUOI2W+IAEf+0oP561KjXuz2an16Jjfq3/zcIiu9EE9PeKDKGrDSxWAL19y/fN9J/nh3UYaN+OBUq+CnuSz01f3SqGvBZVZ+zAJ+Cl3orwBIX9ZKGgQS+l0EQlNg+qw6n3pd02Nm8+ZivO0CkRfH6SXCVFEgkjxljRIIXjYMnToouVq1FU8+RatIXelydNU+ZGXtMyOGkorwTwl8kTpz0Nk/V/YlhTue7ZraOY56/DrL/AymdN6nPE91yDXV7TvDcMjpcWeXWcmoddzTQ4IEZIjwE0OW/Ipoy6gKQzHU8XSXyuWNcOE0pw1Q7vLRD8hRru7d0kl2qBMUqvr9S3udAwP4iVIGHKgHfPFj1l8/S5/6oAL/X0weLFN3eUn5seOFITLXn7UBN0wNSStafOfJJ64vug46PILezOeprJurePygMurCztRR73XsHip0m2nSvXsPolIDindJvb4IMKyb0qR9R7ZFIEuCpWgSCbu52aedV9ctHeEyNw7elgWmc7CgZiCibWc+DWtycYl1da3Kj5ieynlriQXZ3tFVsPFR6DzubIqE1r5PsiZwA7HtsboPzJXc2CBieXZQXgM9aDqPlG2YE8lfO7s3m/X0760YpOHhCXqbtnKdEYUtC+LQ6zynR/C7Dil3GVffgQsHLEkroeaBVnsJmxLMGnRDzho+liwPZMgTLMuzWxes7LnlEkHaFsAB+QcliLnE3JdQGVgK8t0wQ6FH/FBnwwud75Vb1J0pKqjAflxO2YtS7nwZ3M6GuYLB+eCmiAl7ZJ1ISeVxwWwuPztvgC7urI04amBBLji2GDH3cPqjK4Zu23FfRG6ciKGFxdYUTN/2pVHYeJ0Iy9SjGj9pMTNa0WFwZhAEasadVSjSYa60pFISfzwez9FKIIUBHuc637U8TTf8pTEqh4VBUXHdVe+WypPsNM+TpszsbKYhr6aYz9jnUmL3aKvtx1/ENStdYZahJ3bDqqqVgHywP9ROPbAk8QRcTtEeV5n3XvV7N0hJc44deyR+U+5yIgwOAp+50xFjVITDLsf7HwGhDj1rdFaIspAJo+KvkKavGbdbv714whbsZ4Li3qe9ZdjmQ4ZYOUx0jd1qJk9E91aFXf6UM0A4XeplcyGEUxgAWyWO4+FAp/emwLKsc2YoX1fK196icv43IFC9q5Xo8O6I/b7pWSTFSWYl4iKXapT6gKNSQLItO11zfnnrmrXwqP87gQhuZYfU9Bd0fkiOP03wAXhNJeYlAypCwcZNAUjul/eZFjQ6PYHNdv70zHJqwB1L75ROzJirzyubqXt4eqYfAYjXjsxjw66p7t5dAP/mIgwdEGFcE63KyPPgBgvwhOyaE/5S2WyEgFjrNDQQXeqRWVY+finmmZzgig4WmS3rlif8eci9excN8GSTU/WuzKYiSUU+44cXks/TLffj5xtd7TdTCFVUqljjHNFJRRIQJ1EOJlcr+hExt+QxvUhhE5QRT02PQRFLY0wnAxNmkcHL8L0nF2JnKaqtKqZ3DS6MwWcSbNENolBkZbQGzfT/Vk+55F5VkWauJgDt/2sXnxSSWwgF/lZpfbhH7l+1iotKM1vIChLe2suG83YC36Orxua7wC6C+8hJpPhLtJrkOUt93/vCrAkYMt9OueOSPM+4yVcq5bShsWsxAJGNm1qHR3Is2Fug4T7WSIVK7rMiG8AOR7EkzWpGG4huMAJcMsTPUjEbQ7g9vqsfR+i8p89mTiS3d7L4TZ6V8SZGD0sBN+F1rEAfFkDvbiLc66+ozWQm9idOIQYE5kk6QhMlGsjpT1CObH77xHJFE7P2NHYy55kDxbO4vWO2jyP9MLUnFQi80nGwIPja//rSLoTt7KQJ42wNWh5Zy3BTyWRm8T+YLegX61ljUr8BnwW5pIGQlZZIl1I+ivaWbVzZPg2mE5u9jch0q8lkUrApm/bJfpXQRpEXorhOcbIKHXujfxxzidJFPmQjt9DOyTqaKzD6g8/dPc5XZI7EMpTirrHuQqvnX0EMbtvNwhxKqjuTPgslCGgL1r2j7J3ZpA1jiYXlaY3c3SkJ3UlYW2PxBDX/Okwb+LBUQhTPm6fPrnLNqhWfYFMtRqSae/Ejavj15Mwy2yPksVbUpGRH7blDWV54gi1cdpUz+LRKFIaH8bzzMQQdnSmEZVZRJb8p+mSGAm3RCToY73KHrza5jl6eO/Pq+I4AiJV7NLv4mNsBvi4AYuMGPeNkth0+SmPVfvQOUfnX6hfGPEn21j5DGGeQZS6CdaRMoJ6w5vFLnscckB2EJEqe0IuXDOW4juKmJ2ELJZR21V7tFqlSbeSIyzN3Sav68BpKu+s8plqnwiKkZSNKxNwDqRXWfT0mONR3R84mGqqxX1PsR0qHpjBFpOvDe4le60AbO3h+K0toSZC1atLLDM5A0z3pQ31mpSQbKU5+3naoZcjC+CIoh7I1FY1ZbUUnDqO4m0ie63Faq/Q+R87foPGtK04/PKHs8BCWScKpH6udJmKhu4QwBpFlOaPBcA28ptqF3pv9mSb4xCaWiHF7wVIDgSOQfd3bNgZg73ZIoM/H++kqCRRzVQMG2jNedHJMGtNV1GNvOtyyGUzTKGSupKDSqzsqrPiz64jshTJaSp3CVu7b65PJdgOebA2WGC57rigVPOWmbrydu3fOEooiUWokzNq9cQTPs6ApKNMePgZmk0rSI334bVb/JWGRwoFkzEZCbnKJddV94DHuLzyKgyZ/+qYDF5laL3ib4lHIBtzPWpcK82NQ3gMPOOxrz1JgFAITwE9sQXDMyhSK7P2UKYqirDwsbVY28i87tY3hvR/9xICQQFZiuFWY6ZrR4YKza2oNtSqqyITG0/qXXMMHBubL4zN+29j9CZs2TpyfE7TpDNPCXKXFLCTKvRIOXPlNRXvtjuVlrNV3zuG7Vf7eRffg/f+3XcSC9EdpE9a/IjW5aomkGPjvQNul5C+3Wst15KuZ5bVujz8xQNRY82Mdjbzj6MubQThBagLHPv0ZpRkd4j5P6714i0eR1B9yQJ02/cvoExG5adHVwurUI7u79FMkii07hv7OSrVs77LKJZqslI7is5AyT6sHcsNyi+RfDJapcUdoyMZv9Lm7WdYBIuWmJRascvaNGzZj2QjswydJZjkipMML3UwMtogoTJpFh/lwgdlszYI7dnB8BqXaKoqdfHsviH/yTGCMBoOEFhKyntOJTIsy9iyQNDcB6rc3IFYBzSPaLCInHAczruFWZTrigwh3w4h+050xXE6fok/PQ116GrGOicrP0eYUu+yqLAlVm68dKUo1x6t+RYdurULOyNykgDRNLNxq4KCSK58T96AbmjPt+Gq5rn5Z1uXFafhYWghVzjVIKMHmpU38PJVm7ZNSv7MErNtlXadYspbOxUNNGSQUDAjIIWZK/PmQjYbBde4Y0RBgt6o7vozkHcyz9GnK9xUPzkTjzyEPtg7SkXoOG1sU6vkIKvbTUD2hSZ5nsWt7iyI+FYU3jUu8Vp4xIae4PnUwOAbikeQ0T4n4MrnKWCbctDm7nTspZtxg/Slj4QTWgGqahuN2niQWHOOE3VU04M3c/rC3gxkxL74W2+YY02P8kXwYCEn5eo6d1vm1QknirS4OLwQfSE7OhdnTpJSvzMwD0xYX1dKCLW9PSPnjxf4uptYsNYhsXKhONSrcJOEXN240pD4EclLS6uJtp3aCSXg5vCumxkkMk1ZtoTItexpkTNux+MfnIWIHpip8SftaFINkdedkX9eMIgovZW9mBJ1iydbykJrbbOEfKPLP89DM1EF1Ik0UCz5ghVmd4d3brqEBEioZTJ60MxNVc30iVdzjHk7wxszCeqR6bx/NsJffMXQ1em6MuKKZafgCYkMyHO8AHfeGMDhXfptlyQmA3hjuAuX4YafGtFiJjGj/Dwt/lOWUS5siJRg0W3EWWk1CJh3IoQ/EfDsGEdgECN1xuig2uEcfDtn8XZtL83uwhy0hNSTXrHbszv0Zf+tAPf3WQQyk34+4k96lKRtIGPzH54eclvH4r7rBXuLhayLmB9pfi3dp/nLwM1wSo/x/Eu7vDLZT3f0pPJBUdJiYvpkjJ2HRH4qL3O7TtLnBLKKDX9muLteeFN+sv+d32M31h2isQAnmwZ/dMHiKLHwdUDzB1dztX1DThBB/BOyBnyVtvW4PNsCkzOXb6RRxjmXqOEPj0MOFlCE/zpalNz3QQ2UZGuVMhwl2I/Z399L2FKOUABaQ/Zn99G5YvjBLCDaeRKjf0TAZw61wYBAg9w7FtmS1Yuebfbb1EgvOXMaaYtQ/k40tfTfdRwl4Du3ROkCUlfDWwdzY5nixidsF4KnGXOkMMiJ9koaJ2sfx3ukvG8y/zOzXEISMCxqyS/kQ3MPOI75c36+2pOUSz7cO7JLk+hdhV/rz4EcpX3FoxJdXit0/KXnf5wizfMgERatS/hYkRhWurCUc9ARzZxL4WvTOFbbcmEwnWNhCh6HeoC2jwU6L6LQxSPq1dw/XkdE2ArFHbL5DBQCi3V+VA+il17GCQOzr5dwC5+8o0LuJf2Lr9pEtzV2JUJX0qNA6uXZRO4SPzrXQdGnhOrQLROcUWq5eevtpvr7KCJLsCtnltcmKGPN7P8Fxq0CfIpqUZw7FYExaiI9Be25LH8+xJqKnjlzDsh1zM9mByf6WI+/YnXeMROI9QF+RSb7GqCb0BT3cn8JoUbfvVBHEOUESZt/tx4L4tQfMBMDCKp+r5N136MWnq5AhFP08jEjDJDQYwrffIo7lYeBrkob7KfybuM0vh/LZaXl0xCYvK8p1fEgznHTS8ERE61f/uVSC7LeBxX9cKuxAlqZdakVenICdYj9Pqq62ulpZDvlisKLWWxKDhhaFMsmKyC+Kk0LInszGHrRDaI/MgGpXFmRdc+oP/FQTukx5uY4+dU4nuILVOckuk+wRxq2bqRA4dfsez1wLiCxhmXFiVNCSTsEjbBqWEcnmFskPKWr8vO+P5p/QpkUtgoFIebBnnXs2gkZf7EiEwAIdFNRQHJdre/a21CanEG/xpDzTPwnIfo9D9itlffiNhYGtwj97C50TC8ASVS5TFRsFo+MAbZBFwpDFphPYWO+FBHQXP4f3ZBjSlXG2kg5GoI0P0HeMQW2LyiOUoNbBBFse/X5YWmuyuBPE5P3DYxtLRJWHTtGmE795UAOkZ9vl2NVDK921+iXOGABV1XelORVp+1Qscb2RIoVhoJ2vi3g7u3McZyllm6zsVSOWKd1U/Se5+D66tDgfk4Jswjh5mL0uFvl5TZ3gzchFikn0aPBN9IyTrqZvp4atYeEQM7MAELK6/VOTDs70krDOHhidAFZHLaTKzUjpl1sVsUr8iLDcT0uuFh8NbZ8wGq6TTKc+VQWm4iRhLCPzOmDxNEq3PX6fQA4sfEiQiAGuzrsLIxWhPm3gJL19I+XqckEZnTDjatOnF6D2PYPbUiqsL3kDPWOiGHA4K+XhPbwbX+EZyp5i0sOvU42d6snRLr0io63aQHKkPqPLRTYtdhAQ3EL1+Rf1AHeSW7xcxgP6OpHmM0aXH61B7caF33ao97x25zoRdAeZ3APGaPSMabcSELln4QKzEczLuIwSY0oEGFwxwRdG98m8pkKBjQPfek4ohSzAMNKmsKDQNyGaubTp9J++iKvjHEzLsfKjDC1NcOVpXqPveygvxBNlFD3mOaqR6gLROHbEtoTYzBwQ6a1zDhB/jOjgzYQ4YIVgHjgWTx5lUaOlW44Dm849d6C0JlemchVkw1iYLjyx7kAGzOLqGlSK1H09n0ZIZMGLflN0I6UxDFwugdSd6WVPgTmmEhaycpZ1+DkZKRt7Yn4m3OvchnULDRR2Qz++e2wFS2le4619nomp6E6iyTwU9hF1xGZBc1W4cNGoYJsoqElVLn3f+vrR+4lnOwTlTSfBTeryo6YKgC5kAzlpApTHgUva2/RSpCa6mOfQ0ZbbOxFX3Ey40h8JrzDkvh3q3aPlqD5l+7EWZvU80DiVrfOw7NniuSAo5vkMkbszCNfcjQH5Li1B1XFw59a/7hTk6wKwJNR9VdfR3izDNLHTOlW8ipxXBPz5Qx8nYHx3Z7Hi0dZrjgVk0y0EBJxR9KOwJqlbMgb4gnL6rqXd8SJTItZwEsHzsQZlaMkWolGwXWJ1ldGnWmjApk3KCS6ii1xLOTRYKzCQ1S+2So/uOh+hlrtxiLOQgTHiBjhSbV3onzK4IDMYr4vY+0S+L0+K6H2GZBD1DV/taN3KDRPdd1bnm0g/mdRiwDlpPPxbgj+FJdpsRQY52jmeO/EdDkATZlQ+6fQnuNWtproIT2Jc1Bp8x6bZdrERJ1gh7Wa6b9XIK9OQVenRJcJGzOgANgi1Wh8Ytfo+m+uyZ/ifmk9g8ujiVCLgCPUDG0V3WY8RIZXdqKRo4Eao7MdF8aMvKpTcDNT4nkqQmMjKgI2mPCAE+6NyApn4tyH0Vc7Yc09BvwrtZIzKN48J88RAhUeCOrQoOkIMloau4aFKmb8dpXoiEvRn/Xw60WPyP/giWVb5iql4NP5UFJRF+00L2+kbAgzUOn6TfoVewMhCWwE/73T9PuVu3G/6cox28CcmzhrOWcJ6qt2bd4NRgakxvWUWW9gOJhRMJcR3lpJFnKlAnaPvG8vPsCUqF5Wd/Fzga0AWxvtRshC8OwUS8HYQ04IQi/AR+4u2LZbX8uCc3zBX/hI/31fejo3lr3EyQ28skEyRuPA/J/majear2qufdvArkfv6+tKK4cl/Wj7ltRQovSzPbz6wFvb2Gn+AjeBT8QEKfvtqiJaGikltmtFoU1DFOiNOzohVkLOOrO9+wXZUPdxVuhcD6XssR33ppHjhd0Ec1saGP2ZYrQhJN/zRFefIWowKUVcEHMHy9vHO8gx9eCfJx5pVQYQoiNXG1PkKw7/Cdqoe6qhTRFiQOfUWLxf4xw3/RmFNsI0lMARM5VP2rPJnkeYUFEJ58TDmgdqKq7rdhlRIr/nHr9ojUuPqnjSSa8zY11q4gNa9Z10Cd6looJj5QY0oOsWZ2uBcBktgEF11RlsjT0gSTLcWsG0BqG46BkX22O2MwavIxNglJazhrfkfkdihnWH9fEHN7tu5O0Konc9EtKEvJgTrMejzmKRJVcb6pv+HjeW/+VkhzE/xFTjjCyt4OGG70OsliueK1yJaT5o5vji69MuwidFHt0lJhb9QwBHN6Lq3351Bug09MVhb3GUBVSHGPJpYV+ft5B0mW3fo3YrLGQjSkoVLSBnacOMs3c50rm3KTpwpRm7XdwShQV5vqWI5vANplovF3ii1ZvMbQW/OEW+oNymljrT79TAmqGGszokPSYswOXJwLRhtRJ0YTcaA/pEe6dJsI5Vm4y9WLAe3lDEXIUd3USL943F6SgwZaakVh8DzDnEj21VTQ7siTqSF9VhTlqquZ7L8N2YIXWB2cZx4cJLpwu+aICc+4oxrGi0/idBWx5IfHaGG+hLkRC030MA/s8LLLWlC///ESpPI12Y8DeRA4z72uRNtpSzLGe8M3+85Y0X5Hoj2ev9pQpzghzwY6beu+L9p9KbPMeshg0JCQBUHw8dvarJZCf6y0aHfX0lGmLiCLJOO9pd44xOKFG6pEsCAtJ5a/87BK7ONbWwFkONs26hpAy5uOPo3Nwt0rUKzORudZhGhApExgf2u+dZBY5XVSYD4RZ6w4+DxkCKzIlWF9xHvEzK1U7ZusWtUejvWcgHuHcUJJ5iSVnRqj0J8SbBM80kahmC7WxpM3BbRZ7S5m5FDRhfO0Civ9KWMUz43u/UpE7sHwjHDSgPF/In6+36AEUntEnZF8L/LAcTchgjfpD/w1RZGW1lTTdr1fmaFLgG3eRefvUsAX0IToO3g1GRpe6CZfhVx6tt0ubhTiWGqxigNTpPMM1BFzI1RkzGGW9T+IBDCl6GY+vFCXBrWp7W+PV/bl405yruZKRwbFY5Fn6mX48jDeamI7PszMOMCMGHrKF1Mxguehtrm7a4MzNRndJCVUllpIoMwIy8JTjd+v/odJToiAh/GoGOXbllrwWm+0SL5VW3MxA3IXZaGkQGoJa5/2bE4ndDB8y8Iy37wy+q8aFPjcpvWZKMenBF6j8np/X64DFt4xc4ixo2/2C7VgQ+fgHLaIIVynfKQ4RIS2cZQKIZ4pXaRJDruvcU8JebFSAcJw9pmdFnOx+1skvjXtWxBYeNG5ghYkeryJzxqDU2V1+km97yQap+vHrjvdOvDZnHfUvqfDKY2NFOedECET/by4/zEGFxd07bBp6MEz4JowS0Xwx3DhM" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="i8JffsRdhTAK7uvpvDBCXaPeQJRlnhDY2jMjP83TiSJjIqnHc51bW9Yo5vvkZ5PQBYpKnWhe2OBI3sieblCF7lyi/zSEM30wIsItdop8QpgETJ/V/99HNOr/G8/J3e53pi4tUrYSIMxptuwB5l9wjmtvXZ1WBhR4KG4JYD20KZ3ztA0XMeCrJoIF1HKvbyRkHQlvGpCevLwdG4MxIdzptPi0tGtv2VtJB+7uNc8QpMx94ZbL67y+wEjHTNYtjADNrJQdqHgcKGiO3L83sJ4oM6deakt82+KwJerYTk3sLvV11VPtGvVqU1ZXdr7ZKGff0aL7cvnmCu9GEFmrpG8nXw==" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width:260px;">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="#">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/eartquakesecure.aspx">Dos and Dont  during earthquake</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/externaltenders.aspx">External Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=grsGMsqnclzsclld">Kochi Metro Tenders</a></li><li><a href="/tender/Default.aspx?cid=3AGNZpFcFwwlld">Lucknow Metro Works</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender/Default.aspx?cid=yGEt7G92BVAlld">Noida-Greater noida works</a></li><li><a href="/saleofscrap.aspx">Details of sale of scrap by DMRC</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/advertiserlist.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/Outdoor_advertisement_details.aspx">Outdoor advertising locations</a></li><li><a href="/DMRCprocurement.aspx">DMRC Procurement Manual</a></li><li><a href="/tender/Default.aspx?cid=vlVO3VPqpMAlld">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="/otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/otherdocuments/VigilanceMnual.pdf">Vigilance Manual</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>
















  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">72688139</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
