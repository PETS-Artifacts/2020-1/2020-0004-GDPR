<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMTk0MTcwNDkyD2QWAmYPZBYCAgIPZBYKZg8PFgQeCENzc0NsYXNzZR4EXyFTQgICZGQCAQ8PFgQfAAUPbGFuZ3VhZ2VfYWN0aXZlHwECAmRkAgQPZBYMZg8WAh4LXyFJdGVtQ291bnQCDRYaZg9kFgZmDxUBGy9hYm91dF91cy5hc3B4I0ludHJvZHVjdGlvbmQCAQ8VAQ/gpKrgpLDgpL/gpJrgpK9kAgIPFQEMSW50cm9kdWN0aW9uZAIBD2QWBmYPFQEUL1RvcF9tYW5hZ2VtZW50LmFzcHhkAgEPFQEl4KS24KWA4KSw4KWN4KS3IOCkquCljeCksOCkrOCkguCkp+CkqGQCAg8VAQ5Ub3AgTWFuYWdlbWVudGQCAg9kFgZmDxUBEy9ETVJDLXBvbGljaWVzLmFzcHhkAgEPFQEu4KSh4KWA4KSP4KSu4KSG4KSw4KS44KWAIOCkqOClgOCkpOCkv+Ckr+CkvuCkgmQCAg8VAQ1ETVJDIFBvbGljaWVzZAIDD2QWBmYPFQEJL0NTUi5hc3B4ZAIBDxUBPuCkqOCkv+Ckl+CkruCkv+CkpCDgpLjgpL7gpK7gpL7gpJzgpL/gpJUg4KSm4KS+4KSv4KS/4KSk4KWN4KS1ZAICDxUBH0NvcnBvcmF0ZSBTb2NpYWwgUmVzcG9uc2liaWxpdHlkAgQPZBYGZg8VARUvZ3JlZW5pbml0aWF0aXZlLmFzcHhkAgEPFQEf4KS54KSw4KWA4KSk4KS/4KSu4KS+IOCkquCkueCksmQCAg8VARBHcmVlbiBJbml0aWF0aXZlZAIFD2QWBmYPFQETL2FubnVhbF9yZXBvcnQuYXNweGQCAQ8VASvgpLXgpL7gpLDgpY3gpLfgpL/gpJUg4KSw4KS/4KSq4KWL4KSw4KWN4KSfZAICDxUBDUFubnVhbCBSZXBvcnRkAgYPZBYGZg8VARAvRE1SQy1Bd2FyZC5hc3B4ZAIBDxUBMeCkoeClgOCkj+CkruCkhuCksOCkuOClgCDgpKrgpYHgpLDgpLjgpY3gpJXgpL7gpLBkAgIPFQELRE1SQyBBd2FyZHNkAgcPZBYGZg8VARIvdXNlZnVsX2xpbmtzLmFzcHhkAgEPFQEo4KSJ4KSq4KSv4KWL4KSX4KWAIOCkleCkoeCkvOCkv+Ckr+CkvuCkgWQCAg8VAQxVc2VmdWwgTGlua3NkAggPZBYGZg8VARAvbWVtb3JhbmR1bS5hc3B4ZAIBDxUBX+CknOCljeCknuCkvuCkquCkqCDgpJTgpLAg4KSh4KWA4KSP4KSu4KSG4KSw4KS44KWAIOCkleClhyDgpLjgpILgpJfgpK4g4KSF4KSo4KWB4KSa4KWN4KSb4KWH4KSmZAICDxUBLk1lbW9yYW5kdW0gYW5kIEFydGljbGVzIG9mIEFzc29jaWF0aW9uIG9mIERNUkNkAgkPZBYGZg8VARIvYW5udWFscmV0dXJuLmFzcHhkAgEPFQE74KS14KS+4KSw4KWN4KS34KS/4KSVIOCkuOCkguCkquCkpOCljeCkpOCkvyDgpLXgpL/gpLXgpLDgpKNkAgIPFQEWQW5udWFsIFByb3BlcnR5IFJldHVybmQCCg9kFgZmDxUBES9zdXN0YWlibGl0eS5hc3B4ZAIBDxUBN+CkqOCkv+CksOCkguCkpOCksOCkpOCkviDgpLDgpL/gpKrgpYvgpLDgpY3gpJ8gIDIwMTQtMTVkAgIPFQEdU3VzdGFpbmFiaWxpdHkgUmVwb3J0IDIwMTQtMTVkAgsPZBYGZg8VAQ8vZG1yY2dyZWVuLmFzcHhkAgEPFQGdAeCkueCksOClhyDgpJTgpLAg4KS44KWN4KS14KSa4KWN4KSbIOCkruCkvuCkuCDgpLDgpYjgpKrgpL/gpKEg4KSf4KWN4KSw4KS+4KSC4KSc4KS/4KSfIOCkuOCkv+CkuOCljeCkn+CkriDgpK7gpYfgpIIg4KSh4KWA4KSP4KSu4KSG4KSw4KS44KWAIOCkleClgCDgpKrgpLngpLJkAgIPFQEkRE1SQyBsZWFkZXJzIGluIGdyZWVuIGFuZCBjbGVhbiBNUlRTZAIMD2QWBmYPFQETL2NpaW1ldHJvcGhvdG8uYXNweGQCAQ8VAV7gpJfgpY3gpLDgpYDgpKgg4KSu4KWH4KSf4KWN4KSw4KWLIOCkquCljeCksOCko+CkvuCksuClgCDgpLjgpK7gpY3gpK7gpYfgpLLgpKgg4KSX4KWI4KSy4KSw4KWAZAICDxUBJUdyZWVuIE1ldHJvIFN5c3RlbSBDb25mZXJlbmNlIEdhbGxlcnlkAgEPFgIfAgIGFgxmD2QWCGYPFQEUL3Byb2plY3RwcmVzZW50LmFzcHhkAgEPFQEr4KS14KSw4KWN4KSk4KSu4KS+4KSoIOCkqOClh+Ckn+CkteCksOCljeCklWQCAg8VAQ9QcmVzZW50IE5ldHdvcmtkAgQPFgIfAmZkAgEPZBYIZg8VAQ8vQ29ycmlkb3JzLmFzcHhkAgEPFQEk4KSr4KWH4KScIDMg4KSq4KSw4KS/4KSv4KWL4KSc4KSo4KS+ZAICDxUBEVBoYXNlIElJSSBQcm9qZWN0ZAIEDxYCHwJmZAICD2QWCGYPFQEXL3VuZGVyY29uc3RydWN0aW9uLmFzcHhkAgEPFQEr4KSt4KS14KS/4KS34KWN4KSvIOCkquCksOCkv+Ckr+Cli+CknOCkqOCkvmQCAg8VAQ5GdXR1cmUgUHJvamVjdGQCBA8WAh8CZmQCAw9kFghmDxUBDS9mdW5kaW5nLmFzcHhkAgEPFQEl4KSo4KS/4KSn4KS/IOCkteCljeCkr+CkteCkuOCljeCkpeCkvmQCAg8VAQdGdW5kaW5nZAIEDxYCHwJmZAIED2QWCGYPFQEoL3Byb2plY3RzdXBkYXRlL2NvbnN1bHRhbmN5X3Byb2plY3QuYXNweGQCAQ8VAS7gpKrgpLDgpL7gpK7gpLDgpY3gpLYg4KSq4KSw4KS/4KSv4KWL4KSc4KSo4KS+ZAICDxUBE0NvbnN1bHRhbmN5IFByb2plY3RkAgQPFgIfAmZkAgUPZBYIZg8VAQEjZAIBDxUBD+CkteCkv+CkteCkv+Ckp2QCAg8VAQ1NaXNjZWxsYW5lb3VzZAIEDxYCHwICBBYIZg9kFgZmDxUBIy9wcm9qZWN0c3VwZGF0ZS9laWFfcmVwb3J0bGluay5hc3B4ZAIBDxUBIuCkiOCkhuCkiOCkjyDgpLDgpL/gpKrgpYvgpLDgpY3gpJ9kAgIPFQELRUlBIFJlcG9ydHNkAgEPZBYGZg8VASEvUmVsb2NhdGlvbmFuZHJlaGFiaWxpdGF0aW9uLmFzcHhkAgEPFQFO4KSq4KWB4KSo4KSw4KWN4KS14KS+4KS4IOCklOCksCDgpKrgpYHgpKjgpLDgpY3gpLjgpY3gpKXgpL7gpKrgpKgg4KSo4KWA4KSk4KS/ZAICDxUBIFJlbG9jYXRpb24vUmVoYWJpbGl0YXRpb24gUG9saWN5ZAICD2QWBmYPFQEaL1JhaW4tV2F0ZXItSGFydmVzdGluZy5wZGZkAgEPFQEW4KSc4KSyIOCkuOCkguCkmuCkr+CkqGQCAg8VARVSYWluIFdhdGVyIEhhcnZlc3RpbmdkAgMPZBYGZg8VARcvQWlycG9ydF9hZ3JlZW1lbnQuYXNweGQCAQ8VAXTgpI/gpK/gpLDgpKrgpYvgpLDgpY3gpJ8g4KSP4KSV4KWN4KS44KSq4KWN4KSw4KWH4KS4IOCksuCkvuCkh+CkqCDgpJXgpYcg4KSV4KSC4KS44KWH4KS44KS/4KSv4KSo4KS+4KSwIOCkleCksOCkvuCksGQCAg8VATBDb25jZXNzaW9uYWlyZSBBZ3JlZW1lbnQgb2YgQWlycG9ydCBFeHByZXNzIExpbmVkAgIPFgIfAgIVFipmD2QWBmYPFQEOL1pvb21fTWFwLmFzcHhkAgEPFQEu4KSo4KWH4KSf4KS14KSw4KWN4KSVIOCkruCkvuCkqOCkmuCkv+CkpOCljeCksGQCAg8VAQtOZXR3b3JrIE1hcGQCAQ9kFgZmDxUBES9tZXRyby1mYXJlcy5hc3B4ZAIBDxUBP+CksOCkvuCkuOCljeCkpOCkviDgpKjgpL/gpK/gpYvgpJzgpJUg4KSU4KSwIOCkleCkv+CksOCkvuCkr+Clh2QCAg8VARlKb3VybmV5IFBsYW5uZXIgYW5kIEZhcmVzZAICD2QWBmYPFQEVL3BhcmtpbmdmYWNpbGl0eS5hc3B4ZAIBDxUBMuCkquCkvuCksOCljeCkleCkv+CkguCklyDgpJXgpYAg4KS44KWB4KS14KS/4KSn4KS+ZAICDxUBElBhcmtpbmcgRmFjaWxpdGllc2QCAw9kFgZmDxUBFy9zdGF0aW9uZmFjaWxpdGllcy5hc3B4ZAIBDxUBK+CkuOCljeCkn+Clh+CktuCkqCDgpLjgpYHgpLXgpL/gpKfgpL7gpI/gpIJkAgIPFQESU3RhdGlvbiBGYWNpbGl0aWVzZAIED2QWBmYPFQEBI2QCAQ8VASvgpLjgpL7gpIfgpJXgpL/gpLIg4KS44KWB4KS14KS/4KSn4KS+4KSP4KSCZAICDxUBEkJpY3ljbGUgRmFjaWxpdGllc2QCBQ9kFgZmDxUBASNkAgEPFQEl4KS24KWM4KSa4KS+4KSy4KSvIOCkuOClgeCkteCkv+Ckp+CkvmQCAg8VARFUb2lsZXQgRmFjaWxpdGllc2QCBg9kFgZmDxUBES93b21lbnNhZmV0eS5hc3B4ZAIBDxUBVeCkruCkueCkv+CksuCkviDgpK/gpL7gpKTgpY3gpLDgpL/gpK/gpYvgpIIg4KSV4KWHIOCksuCkv+CkjyDgpLjgpYHgpLXgpL/gpKfgpL7gpI/gpIJkAgIPFQEfRmFjaWxpdGllcyBmb3IgV29tZW4gcGFzc2VuZ2Vyc2QCBw9kFgZmDxUBFS9kaWZmZXJlbnRseWFibGUuYXNweGQCAQ8VAVvgpLXgpL/gpJXgpLLgpL7gpILgpJcg4KSv4KS+4KSk4KWN4KSw4KS/4KSv4KWL4KSCIOCkleClhyDgpLLgpL/gpI8g4KS44KWB4KS14KS/4KSn4KS+4KSP4KSCZAICDxUBK0ZhY2lsaXRpZXMgZm9yIGRpZmZlcmVudGx5IGFibGVkIHBhc3NlbmdlcnNkAggPZBYGZg8VAQ8vZmVlZGVyYnVzLmFzcHhkAgEPFQEZ4KSr4KWA4KSh4KSwIOCkrOCkuOClh+CkgmQCAg8VAQxGZWVkZXIgQnVzZXNkAgkPZBYGZg8VARgvQWlycG9ydEV4cHJlc3NMaW5lLmFzcHhkAgEPFQFB4KSP4KSv4KSw4KSq4KWL4KSw4KWN4KSfIOCkj+CkleCljeCkuOCkquCljeCksOClh+CkuCDgpLLgpL7gpIfgpKhkAgIPFQEUQWlycG9ydCBFeHByZXNzIExpbmVkAgoPZBYGZg8VARAvbW9iaWxlLWFwcC5hc3B4ZAIBDxUBVOCkpuCkv+CksuCljeCksuClgCDgpK7gpYfgpJ/gpY3gpLDgpYsg4KSu4KWL4KSs4KS+4KSH4KSyIOCkj+CkquCljeCksuClgOCkleClh+CktuCkqGQCAg8VAR5EZWxoaSBNZXRybyBNb2JpbGUgQXBwbGljYXRpb25kAgsPZBYGZg8VARIvZmlyc3R0aW1pbmdzLmFzcHhkAgEPFQFP4KSq4KWN4KSw4KSl4KSuIOCklOCksCDgpIXgpILgpKTgpL/gpK4g4KSu4KWH4KSf4KWN4KSw4KWLIOCkn+CkvuCkh+CkruCkv+CkguCkl2QCAg8VARlGaXJzdCAmIExhc3QgTWV0cm8gVGltaW5nZAIMD2QWBmYPFQEQL2xvc3RfZm91bmQuYXNweGQCAQ8VARvgpJbgpYvgpK/gpL4gLSDgpKrgpL7gpK/gpL5kAgIPFQEMTG9zdCAmIEZvdW5kZAIND2QWBmYPFQEWL01ldHJvX1NlY3VyaXRpZXMuYXNweGQCAQ8VAUzgpK7gpYfgpJ/gpY3gpLDgpYsg4KS44KWB4KSw4KSV4KWN4KS34KS+IOCklOCksCDgpKrgpYHgpLLgpL/gpLgg4KSl4KS+4KSo4KWHZAICDxUBIE1ldHJvIFNlY3VyaXR5ICYgUG9saWNlIFN0YXRpb25zZAIOD2QWBmYPFQEPZm9ybV9ndWlkZS5hc3B4ZAIBDxUBhAHgpIbgpIngpJ/gpLjgpYvgpLDgpY3gpLgg4KSV4KSw4KWN4KSu4KSa4KS+4KSw4KS/4KSv4KWL4KSCIOCkleClhyDgpLLgpL/gpI8g4KSr4KS+4KSw4KWN4KSuIOCklOCksCDgpKbgpL/gpLbgpL7gpKjgpL/gpLDgpY3gpKbgpYfgpLZkAgIPFQErRm9ybXMgJiBHdWlkZWxpbmVzIGZvciBPdXRzb3VyY2VkIEVtcGxveWVlc2QCDw9kFgZmDxUBFS9jb21tdXRlcnNfZ3VpZGUuYXNweGQCAQ8VAR/gpK/gpL7gpKTgpY3gpLDgpYAg4KSX4KS+4KSH4KShZAICDxUBD0NvbW11dGVycyBHdWlkZWQCEA9kFgZmDxUBES9BVE1fZGV0YWlscy5hc3B4ZAIBDxUBH+Ckj+Ckn+ClgOCkj+CkriDgpLXgpL/gpLXgpLDgpKNkAgIPFQELQVRNIERldGFpbHNkAhEPZBYGZg8VAQ4vZmVlZGJhY2suYXNweGQCAQ8VARLgpKvgpYDgpKHgpKzgpYjgpJVkAgIPFQEIRmVlZGJhY2tkAhIPZBYGZg8VAQEjZAIBDxUBLOCkruClh+Ckn+CljeCksOCliyDgpK7gpL/gpKTgpY3gpLAg4KSq4KS54KSyZAICDxUBFk1ldHJvIE1pdHJhIEluaXRpYXRpdmVkAhMPZBYGZg8VAQkvRkFRLmFzcHhkAgEPFQEo4KS44KS+4KSu4KS+4KSo4KWN4KSvIOCkquCljeCksOCktuCljeCkqGQCAg8VAQNGQVFkAhQPZBYGZg8VASgvb3RoZXJkb2N1bWVudHMvRUFSVEhRVUFLRWd1aWRlbGluZXMucGRmZAIBDxUBa+CkreClguCkmuCkvuCksiDgpJXgpYcg4KSm4KWM4KSw4KS+4KSoIOCkleCksOCkqOClhyDgpJTgpLAg4KSoIOCkleCksOCkqOClhyDgpK/gpYvgpJfgpY3gpK8g4KSs4KS+4KSk4KWH4KSCZAICDxUBIURvJ3MgYW5kIERvbnQncyBkdXJpbmcgZWFydGhxdWFrZWQCAw8WAh8CAgMWBmYPZBYGZg8VAQsvbWVkaWEuYXNweGQCAQ8VASLgpLjgpK7gpL7gpJrgpL7gpLDgpYvgpIIg4KSu4KWH4KSCZAICDxUBC0luIHRoZSBOZXdzZAIBD2QWBmYPFQEUL3ByZXNzX3JlbGVhc2VzLmFzcHhkAgEPFQEr4KSq4KWN4KSw4KWH4KS4IOCkteCkv+CknOCljeCknuCkquCljeCkpOCkv2QCAg8VAQ5QcmVzcyBSZWxlYXNlc2QCAg9kFgZmDxUBEi9uZXdzYW5hbHlzaXMuYXNweGQCAQ8VASvgpLjgpK7gpL7gpJrgpL7gpLAg4KS14KS/4KS24KWN4KSy4KWH4KS34KSjZAICDxUBDU5ld3MgQW5hbHlzaXNkAgQPFgIfAgIWFixmD2QWBmYPFQENL3RlbmRlcnMuYXNweGQCAQ8VAVvgpLjgpILgpKrgpKTgpY3gpKTgpL8g4KS14KS/4KSV4KS+4KS4IOCkpOCkpeCkviDgpLjgpILgpKrgpKTgpY3gpKTgpL8g4KS14KWN4KSv4KS+4KSq4KS+4KSwZAICDxUBKFByb3BlcnR5IERldmVsb3BtZW50ICYgUHJvcGVydHkgQnVzaW5lc3NkAgEPZBYGZg8VARMvdGVuZGVyc19jaXZpbC5hc3B4ZAIBDxUBD+CkuOCkv+CkteCkv+CksmQCAg8VAQVDaXZpbGQCAg9kFgZmDxUBGC90ZW5kZXJzX2VsZWN0cmljYWwuYXNweGQCAQ8VARXgpLXgpL/gpKbgpY3gpK/gpYHgpKRkAgIPFQEKRWxlY3RyaWNhbGQCAw9kFgZmDxUBEy90ZW5kZXJzX21zbG5zLmFzcHhkAgEPFQEP4KS14KS/4KS14KS/4KSnZAICDxUBDU1pc2NlbGxhbmVvdXNkAgQPZBYGZg8VAREvdGVuZGVyc19zbnQuYXNweGQCAQ8VATLgpLjgpL/gpJfgpKjgpLIg4KSP4KS14KSCIOCkpuClguCksOCkuOCkguCkmuCkvuCksGQCAg8VARRTaWduYWxsaW5nICYgVGVsZWNvbWQCBQ9kFgZmDxUBCy9UcmFjay5hc3B4ZAIBDxUBEOCksOClh+CksiDgpKrgpKVkAgIPFQEFVHJhY2tkAgYPZBYGZg8VAREvS29jaGlfTWV0cm8uYXNweGQCAQ8VAT7gpJXgpYvgpJrgpY3gpJrgpL8g4KSu4KWH4KSf4KWN4KSw4KWLIOCkqOCkv+CkteCkv+CkpuCkvuCkj+CkgmQCAg8VARNLb2NoaSBNZXRybyBUZW5kZXJzZAIHD2QWBmYPFQEaL3RlbmRlcl9MdWNrbm93X01ldHJvLmFzcHhkAgEPFQFF4KSy4KSW4KSo4KSKIOCkruClh+Ckn+CljeCksOCliyDgpKjgpL/gpLDgpY3gpK7gpL7gpKMg4KSV4KS+4KSw4KWN4KSvZAICDxUBE0x1Y2tub3cgTWV0cm8gV29ya3NkAggPZBYGZg8VARAvdGVuZGVyc19ycy5hc3B4ZAIBDxUBFuCkmuCksiDgpLjgpY3gpJ/gpYngpJVkAgIPFQENUm9sbGluZyBTdG9ja2QCCQ9kFgZmDxUBFy9tYWpvci1jb250cmFjdG9ycy5hc3B4ZAIBDxUBH+CkrOClnOClhyDgpKDgpYfgpJXgpYfgpKbgpL7gpLBkAgIPFQERTWFqb3IgQ29udHJhY3RvcnNkAgoPZBYGZg8VAR8vdGVuZGVyX25vaWRhX0dyZWF0ZXJub2lkYS5hc3B4ZAIBDxUBWOCkqOCli+Ckj+CkoeCkvi3gpJfgpY3gpLDgpYfgpJ/gpLAg4KSo4KWL4KSP4KSh4KS+IOCkqOCkv+CksOCljeCkruCkvuCkoyDgpJXgpL7gpLDgpY3gpK9kAgIPFQEZTm9pZGEtR3JlYXRlciBub2lkYSB3b3Jrc2QCCw9kFgZmDxUBFS9hd2FyZGVkLXRlbmRlcnMuYXNweGQCAQ8VAR/gpKjgpL/gpLXgpL/gpKbgpL4g4KSg4KWH4KSV4KWHZAICDxUBF0xpc3Qgb2YgVGVuZGVycyBBd2FyZGVkZAIMD2QWBmYPFQEoL290aGVyZG9jdW1lbnRzL0NvcHlMaXN0b2ZJbnNpZGVTdG5zLnBkZmQCAQ8VAS/gpLXgpL/gpJzgpY3gpJ7gpL7gpKrgpJXgpYsg4KSV4KWAIOCkuOClguCkmuClgGQCAg8VARBBZHZlcnRpc2VycyBMaXN0ZAIND2QWBmYPFQESL0tpb3NrLVBvbGljeS5hc3B4ZAIBDxUBH+CkleCkv+Ckk+CkuOCljeCklSDgpKjgpYDgpKTgpL9kAgIPFQEMS2lvc2sgUG9saWN5ZAIOD2QWBmYPFQEjL091dGRvb3JfYWR2ZXJ0aXNlbWVudF9kZXRhaWxzLmFzcHhkAgEPFQFB4KSs4KS+4KS54KWN4KSw4KSvIOCkteCkv+CknOCljeCknuCkvuCkquCklSDgpIXgpLXgpLjgpY3gpKXgpL7gpKhkAgIPFQEdT3V0ZG9vciBhZHZlcnRpc2luZyBsb2NhdGlvbnNkAg8PZBYGZg8VASZvdGhlcmRvY3VtZW50cy9ETVJDcHJvY3VyZW1lbnQyMDE0LnBkZmQCAQ8VATjgpKHgpYDgpI/gpK7gpIbgpLDgpLjgpYAg4KSW4KSw4KWA4KSmIOCkruCliOCkqOClgeCkheCksmQCAg8VARdETVJDIFByb2N1cmVtZW50IE1hbnVhbGQCEA9kFgZmDxUBFS9LZXJhbGFfbW9ub3JhaWwuYXNweGQCAQ8VASLgpJXgpYfgpLDgpLIg4KSu4KWL4KSo4KWL4KSw4KWH4KSyZAICDxUBD0tlcmFsYSBtb25vcmFpbGQCEQ9kFgZmDxUBES9ibGFja2xpc3RlZC5hc3B4ZAIBDxUBOuCkquCljeCksOCkpOCkv+CkrOCkguCkp+Ckv+CkpCDgpI/gpJzgpYfgpILgpLjgpL/gpK/gpL7gpIJkAgIPFQEeQmxhY2tsaXN0IC9EZWJhcnJlZCAvU3VzcGVuZGVkZAISD2QWBmYPFQEnL3RlbmRlci9EZWZhdWx0LmFzcHg/Y2lkPVFFc2FEUFlKWldRbGxkZAIBDxUBR+CkteCkv+CknOCkr+CkteCkvuCkoeCkvOCkviDgpK7gpYfgpJ/gpY3gpLDgpYsg4KSq4KSw4KS/4KSv4KWL4KSc4KSo4KS+ZAICDxUBGFZpamF5YXdhZGEgTWV0cm8gUHJvamVjdGQCEw9kFgZmDxUBE2NvbW1lcmNpYWx3YWxrLmFzcHhkAgEPFQGKAeCkteClieCklS3gpIfgpKgt4KSG4KSn4KS+4KSwIOCkquCksCDgpJzgpKjgpKrgpKUg4KSu4KWH4KSf4KWN4KSw4KWLIOCkruClh+CkgiDgpLXgpL7gpKPgpL/gpJzgpY3gpK/gpL/gpJUg4KSw4KS/4KSV4KWN4KSkIOCkuOCljeCkpeCkvuCkqGQCAg8VATNDb21tZXJjaWFsIHNwYWNlcyBhdCBKYW5wYXRoIG1ldHJvIGF0IHdhbGstaW4tYmFzaXNkAhQPZBYGZg8VAScvdGVuZGVyL0RlZmF1bHQuYXNweD9jaWQ9S0JKOW9hR2J5aDRsbGRkAgEPFQE74KSu4KWB4KSC4KSs4KSIIOCkruClh+Ckn+CljeCksOCliyDgpKrgpLDgpL/gpK/gpYvgpJzgpKjgpL5kAgIPFQEUTXVtYmFpIE1ldHJvIFByb2plY3RkAhUPZBYGZg8VASZvdGhlcmRvY3VtZW50cy9FTVBBTkVMTUVOVGFnZW5jaWVzLnBkZmQCAQ8VAVLgpLXgpL/gpJzgpY3gpJ7gpL7gpKrgpKgg4KSP4KSc4KWH4KSC4KS44KS/4KSv4KWL4KSCIDIwMTYtMjAyMSDgpJXgpL4g4KSq4KWI4KSo4KSyZAICDxUBLUVtcGFuZWxtZW50IG9mIEFkdmVydGlzaW5nIEFnZW5jaWVzIDIwMTYtMjAyMWQCBQ8WAh8CAgYWDGYPZBYGZg8VAQ8vdmlnaWxhbmNlLmFzcHhkAgEPFQFM4KSh4KWA4KSP4KSu4KSG4KSw4KS44KWAIOCkuOCkpOCksOCljeCkleCkpOCkviDgpJXgpYcg4KSs4KS+4KSw4KWHIOCkruClh+CkgmQCAg8VARRBYm91dCBETVJDIFZpZ2lsYW5jZWQCAQ9kFgZmDxUBGS92aWdpbGFuY2UvZnVuY3Rpb25zLmFzcHhkAgEPFQEv4KS44KWA4KS14KWA4KSTIOCkleClhyDgpKrgpY3gpLDgpJXgpL7gpLDgpY3gpK9kAgIPFQEURnVuY3Rpb25zIG9mIHRoZSBDVk9kAgIPZBYGZg8VARovdmlnaWxhbmNlL2NvbXBsYWludHMuYXNweGQCAQ8VAS7gpLjgpKTgpLDgpY3gpJXgpKTgpL4g4KS24KS/4KSV4KS+4KSv4KSk4KWH4KSCZAICDxUBFFZpZ2lsYW5jZSBDb21wbGFpbnRzZAIDD2QWBmYPFQEaL3ZpZ2lsYW5jZS9jb250YWN0LXVzLmFzcHhkAgEPFQEs4KS54KSu4KS44KWHIOCkuOCkguCkquCksOCljeCklSDgpJXgpLDgpYfgpIJkAgIPFQEKQ29udGFjdCBVc2QCBA9kFgZmDxUBGy92aWdpbGFuY2UvbWVkaWEtZm9jdXMuYXNweGQCAQ8VAR/gpK7gpYDgpKHgpL/gpK/gpL4g4KSr4KWL4KSV4KS4ZAICDxUBC01lZGlhIEZvY3VzZAIFD2QWBmYPFQEgL3ZpZ2lsYW5jZS9WaWdpbGFuY2VfQm9va2xldC5wZGZkAgEPFQFk4KSu4KWH4KSf4KWN4KSw4KWLIOCkh+CkguCknOClgOCkqOCkv+Ckr+CksOCljeCkuCDgpJXgpYcg4KSy4KS/4KSPIOCkpuCkv+CktuCkvuCkqOCkv+CksOCljeCkpuClh+CktmQCAg8VAR5HdWlkZWxpbmVzIGZvciBNZXRybyBFbmdpbmVlcnNkAgUPFgIeB1Zpc2libGVoZAIHD2QWCGYPDxYCHgtOYXZpZ2F0ZVVybAUsL0NoZXF1ZXNEb2N1bWVudHMvNjcyUmVhZHlDaGVxdWVzRGV0YWlscy5wZGYWAh4Hb25jbGljawUmcmV0dXJuIEZ1bkhpdEluY3JlYXNlKCc2NzInLCdDaGVxdWUnKTtkAgEPDxYCHgRUZXh0BQg1NTk5OTQxM2RkAgIPDxYCHwQFLC9DaGVxdWVzRG9jdW1lbnRzLzY3MlJlYWR5Q2hlcXVlc0RldGFpbHMucGRmFgIfBQUmcmV0dXJuIEZ1bkhpdEluY3JlYXNlKCc2NzInLCdDaGVxdWUnKTtkAgMPDxYCHwYFCDU1OTk5NDEzZGRkOfcOw+b84V3QARsCNyzTtPaY2zFZ+XVleOQt8If7KJI=" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAtEnwGPPO2GNDDkML/eAGf6UaBnHZtG0Wz7XGrIHyxAcTxor8nKYVLONgQ8pi8TxzPj7dallA1vzavOzYqdPeuHFciQ52/r/TRcebO7vDkOyT5+w4zmgwxZF5onDl2qYbqf4pvCZG5+MWRbqSr0ywww82Hzab4I+GHhOTYg4pmK+zx0Vpc25Dg46nOnD//jqWuvLUrjuIXoY9GFaXY/V1ulxAgGLzw2RaaEJ/dk5qex9GzcH9/MrvusWvVukrZc/1NbpTwj3PSIm3zSfPk7B8Zs" />
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/greeninitiative.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report 2014-15</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width:260px;">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="#">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/otherdocuments/EARTHQUAKEguidelines.pdf">Do's and Dont's during earthquake</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/tenders.aspx">Property Development & Property Business</a></li><li><a href="/tenders_civil.aspx">Civil</a></li><li><a href="/tenders_electrical.aspx">Electrical</a></li><li><a href="/tenders_mslns.aspx">Miscellaneous</a></li><li><a href="/tenders_snt.aspx">Signalling & Telecom</a></li><li><a href="/Track.aspx">Track</a></li><li><a href="/Kochi_Metro.aspx">Kochi Metro Tenders</a></li><li><a href="/tender_Lucknow_Metro.aspx">Lucknow Metro Works</a></li><li><a href="/tenders_rs.aspx">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender_noida_Greaternoida.aspx">Noida-Greater noida works</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/otherdocuments/CopyListofInsideStns.pdf">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/Outdoor_advertisement_details.aspx">Outdoor advertising locations</a></li><li><a href="otherdocuments/DMRCprocurement2014.pdf">DMRC Procurement Manual</a></li><li><a href="/Kerala_monorail.aspx">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="commercialwalk.aspx">Commercial spaces at Janpath metro at walk-in-basis</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    <li><a href="/rightto_infoact/"><b>RTI</b></a>
        
    </li>
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE (FOR NOC) New</b></a></li>
         

</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>
















  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at <a href="feedback@delhimetrorail.com">feedback@delhimetrorail.com </a> <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>

                    <li>
                        <a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;672&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/672ReadyChequesDetails.pdf" target="_blank">READY CHEQUE FOR DISTRIBUTION</a></li>


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">55999413</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2014. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="images/WIM.png"></a></span>
				
				<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Powered by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
