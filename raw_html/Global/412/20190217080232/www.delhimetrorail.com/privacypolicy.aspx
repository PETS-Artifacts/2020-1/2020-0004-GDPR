<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_ibtnFind')" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="KLflwFVE2rq7kpztmAhpROvYMcTZps57gjAoGBjgi3Hs15AWbaeNQHNpgxhxQm7G8NJoHHCywukZcs6G1Akl4WDTI8iCsTCzKBrNBb6LjcaHApKbu3CZjbe8aMRQJs82Uak7ZEUrKtwrZJwxmmKPI4Uq5sSsYUPj8aQ5SyYmnIVHOeL5ocTAZWBIDRRZh/c7ObAw5K+gKrM4zEWjvTurePIW3S5V26QRp7R1cQfK2u5f/6Bw2XxzgVw0Qhf2OdBTlAlO27vOBX6wDPcf/qDYPUEjAhR1YT0sk6q8Z5GcZ8mIPLWW88Davz7q8CAKPO9Hta4GPCq3W/elffwmsvui4+YlDwVmydypRq50trf7kI1KebayxvaFaQODjVlWAoMHbUvDOJ/7v9sUVSzNUg0b6XXeQ1nNJN2dFiPc2rP/6tQdlYWi4wvmG7tA0JYP1962SO6i+mKh1zXMa8p5sh3ipKSz21wj+LEjJN/Vdxe06WSc+NJW/a/eZ5huA/d1zzTlgvRblFcptUgElOLlHe/HJyzQZJ3WndpwOTODTyX2AECjfCQp/g8U3muwrJDEGfxe0HKQhIS6IPLZ1bCf6zjd3WCsketSXh39eMvittv7dVqalEmCpoLmdxI+Kln4W3VxeVXQazVto5wpFqwNwIsCcqkQp+Lop/gKvfRbUB/GNzOMrl2JlDt3GDOS4PxnpKar64N2ADiH+4IRSnRoa3IrXLQmgSBT+D6pOkmlCRnT4CmMg2eHt5Xk8cHxAdIsFSIxhlVWVODONWVffFdR7AsQP+x9gbigBzLroXyN0+fMYnpqqcmm6LqkHO/XLq4XEhmvwo9bUiZk4oiUsYfTGKAz28Q1/CqMjTtTZw8ORx7B093PnDKDmsNz/n4kdfKT17Ll4tKFloH8P4SIsV2StofKcsd3zDdP/0TDr7Xz3/0u5JHpiabIG26zREDK1fgEwoPrp7cs4h+NZZx17cwTMNqfEjxBtOjtfD+TSZmAOf50PRiaQ/ql9P6CcO+BvJxlGj7ADhwYlKf/EnHG4IKAVfsH3fjL/R5k5/5UfYLZXdBbE7q1HcU9hyV/WuuX8RgkUTBCQK5lniaSUkVhcSAW4SS0P2k/KzPlJSebVlOeHfnlcViHDn6dn9oSaeZmollwbWXIgw+zMmpUTBqpFn4kN63qb2Zcw8zaAhm0yrdOonE4lzRPkpuTh2GWewLIGfNUve3b2GJ+sP3jqobKYnxxSJezJ1k5wgVeNzsp0vXSyT8b2MHedn5Gs98RLdSyoFd6UyxIgmKQ0z2SCi9yhwN6W460nqmE4a7ywGdCRyZLN5YDCi6Scg3KRrscQqPQqKbfqlpfhjD1za+21NQKOZ7qHLp6SXUlxb9YTY1SHZl0DYiiXIuGwugSaqbQ1C7wf4sTaVlfqO3Puunn8rBf2ie21S2fVJBLyT5KkJeel7Sonq+FI0Js73yI/ThTrvrHs+R+dRZYxEfK5J4J6zCgrbG6TcDaFj/6adJx107lh9jBXcYRljyIgZFRmiR5SGrto18d7d8wE3bEsIzhDy1pZIZSdQWRNN6OE0YP94SCeCH+OEjL7EyiCA1H626hBbTdTM6cqsWtxvFO2Y+wxYAcI3C7J8sABrnVh8MPN55PKPDZ2XfoDCVBCVKlnWP/M2EV7nOzdeomldOYzaNixZPG41517oeoawsXJkymt7I63BM3egQRtM2DGxFjYqCBkCyWYv8n1CCGbbsK+hNNortd3/xVpOa1hWK2GEPP5YF/hUFX4VCpiaqR+w9GnrZOGHEyksS0tQzDIrWUV+QBjdcLkk9b1mWCSW3tkQxPVrUY8au8x4tZ6JceEuwWtOcYWtXNd4M+k7mc9OqRB6nDCl7Pc5siZ5y9gvweXTRH5OXyWUk3iWPpn9dFVUl6lbGJ3pSJQIees7zC/DxArcG8r4X0Ad+2njtkl23to9/SYtVeJim4fs6b+C0M/DMq/WH4PqzLRPx5ad37+dv80VJTgfkyFWjUpZncYKhD406V6ZPgkXx55oEL0dRg9kzFuW/2653nOg18RRJkIjCcMbXUEnkygOpSSG2LZL0nHcQrbMMWqkuPeZR1inzknHsIf9viPi13LrgZbctaUwuosHrJpznvG/malLMSjeZ75DgCwb03vm6aE++JEmIN3ytu26/cVVV+P2D3PYnxNS9gghe7ap1cgKV4IbCaUbtvJoXvFz53Uh/mjGJx/zdNRPOtaUFMWZoUs9fQi0J5Gq9+vzcMM6JJwmFgwyXaBTUqtgjaaFDjJUjllrY8xw2RLF6hcK2Qb+/PLUdVyb1jsnbja2yjGauvb+pU3j1Jkxu04sBPbEwrG45llNm8LC0gBRGoiEUh1HXTc50oTmYNm3BWQAmGvtRj6xqT3zBDmWIuOJWZ6LxqUCvBPVUWb0/XcJ8RykIx8n6KgUd73AjWMk9UwKmYJyq/HY+AqvJPnkJ2N43ad/E4qzpJcYp9KOPOLoGi5Mff9Ir62r7CeMul0FicvH+WR5SOXu4z9f1MSB5oUnaad0MEuEdV2fSYqXCZAB2lMNhFOPIS/HQi8UftmdfQIomkH+0ZFYtLkzQNUYw7njxlmd0oBGYQnFrDHp0fKd9e8infMHBRdzIveL4dhCClgf+c7Z+okz7X67km3mYAhk+wRSYZRVP/XvkUsoH4AITk0VBHmnaRKoe9cVCUhBN0EqnGXt2F6m+3qAsfu32GRmmKx/7Imd9dBEIRJj+mM4ljP09PSyt/CYYw573xosdFkSpbX72qFKrP0zaP+YXV/S1W6slpc+/VTMNXoYJzSYCSqzaXzqKddJrKoL9brT19A8Z8G0kmPrOeBeez9LA6hMf+dEAzdRjBhT1iRZIMdxMdbCLTLfsA7DQ8SivcjMx0BTCNb/KmIGiixx4nURyodXOleJwqIvP0e+pcSy32n22Kdv0wHnK6jBk3p6DflmTy/XmxPf+IYn84zRLRmHbvkmvUd9oOMC5RiK5BcR5tawCIv8XRwahWMgAicgOVDEbR7qKOgfK87A55RTj8feZ5Zq7tgIBP7pEgCrcDXh+gmDdllDa1XoIgTeyh3V5u1tLLipxWa8CT7E5w1otbksaSzNBRc0COooh/mb8uYkHUu8vZZ1YfyqVLiH2B+aV5zUVfHPuV8KPokQR5Db2hp20OVrKYJuXDYGpcK4RBVSE9hcrs9LTz9EhFNc4bXHfLq7mrx/dmDce/oUN5uMdqvVotjAZBmwaLkFDvlak2N1sQ7hyeVfHRpLZTS3bIpwR4r+em4kuNTItkAaYoc/Pju8bq4mu2zHbsEatzmJqT7wsoigjnR9ng5M54oPyPyW2mNwMy03XwOGY3sJ0Q4S6Oij1hg75DAX1Z5aRMKajSDxXhhjuzFkOBB/KSJ+Vty7ZABX4L6dJBLijh1jZdDK2Acsts/s82uH9WvH66INxu2cizUjG8OcnUpKnobw4ZID3Omd9jtwkQI20dvuI+J4JLXivj6DvC0pIxe6X9LfS5TOqvEm/0TFXDQk3W1TKhvFHVvKmgt9IjK2lLoR1s/WkB9IQaRutIVumCVa+smzY5PIxD0+LTsXrmQk45sviuiWZi5mqaiz9zYe3kwabGetb3RI8ySBCIbK7mP84kI/i41GKEYmGSnfqx/KwVltmHYbImfHGmhkrTBziRoXAUFCTYqUeV1Y1Dx4G8iWXIs+v88OvtCl9wuprNIIt/NRba9iSZfbGiO1oUPdJF/cFZ7MtOb4ssRTn76gi3mMaVD6xE41DCXnZ3GeszagLPPj+3oupNQpivkxWu9dl4SDzJYnOpF3e3cnnd64+9gz02TtNNl5+jPrPRtYWfKEXtM/XTRRoRCkP5o5O0BkUpoKf4JRLFtxEVqTEtfCsd3g08FMkWcOVWh2hIyHIpwJkMtbMiieJnJ/vvjnaksH/rr7OI0hoHN+drmf3V9r74JEXN08hp9yqIJO/19e6ZJZEGPc4egQiF/pBmh+cgLLHidBXVK4TtfGShYovLd9273NbChm105aukCmGUQ9zTghmFmMAruvTZzg4JbtcJGxeeex+Z7+SUVK8Q+ebY9cppp624oPWiGydJSc9il/02jbgECBsE9xTNFiLCVH9D/aZH2kQXbAh2/mYwXHSiNqACTB1oCkqLBtlzN22k5hQI1b2JGBnFHpS81NUXYmUGPQZLp99GtBji1YiCKx0VyLB32C8k+FQW3u8l/ICNASFfMMGQMYuzIRvQ8OHxvTV7Btz584OqYAET+CuLoNxSzId6zf+hlCZWM0WsMeaHPMyyxNgA4C9IL9D4ex8J3Qh11TwZpsZ/Eg/Iopf+r4oTWv++1xmhmPbJxA2fC4ONagVuhgxEwPlGbDxvmeiv9hmURQQXO0m0bKHMN19prq48ryyfWZfOKAqW/tDt5s4i48o/5YPtoUllCZ40wuxM9Jnd5avyBTfsytqydXsju9ge/LT59D8Sv/0QCe0pmX4pY/AvCWFSnCMIhZ+RAw5foPo24hcsEQHYzZn4DzjvYzPGZPU2g5sNbjUVz713laoYDb4dSm22NIycCtYKFLeYwIy1DDn8pmfK9kDM1iaNLUt8bo7bfzPpsF+ua2P/DjHL/R6Umm7LqkRklPq3il5Q+3nzM4pNC2rO6Lo8U/3MvhxK1wwMZc0fo2WlUL75MhmML3mFMic5OAlVAAvemjJ4Rzezm4uVPFZvsOv2OYrYyoZzXQ/7NLQtWBWiudoqmYmArwHuWVV0julYL4DCngppzPgPQqrPL4P395crrudJ/qikgAS4BePrifwiifO74ATmyR7t4MJfEjyIOscu2BicR1iyYBhNtS3gUiSKkYiB8WPgAXjyaodPCePIDVGPnQuCkaOlwnAWAniI5zuEcQ53G4S2Ew5945VDSI2HHpbeHxLPxk50af+9vRYajOu3C2xq5RMzSrCHmJbr8WzoOYty6sdna+e5n5diPFnwwTZvYWjs6gfZ0uBRkF2ebVhgRfegSKBeqrdiNUmh9nUG/uuiG6173mmzhi0NGh7d5fFTHy8Crh4p6pOKIwmZE/NMBTIfkSXRDyovESXFe0O1gioT6ph8vMCQNZSHAt4HeWNkCGkySF8VMUuZF4u+kKLNkBva76uEzvnbeazM65em6B1V7UU7RXwRGsBqRV9QW87jjYAzN3Qjx0ds34WrnFCFHq3Cw75x6nABNlGBPFZj7YXoC4yOTJQk7HSlHsle1R5PJQgev7hU/z4Kg/0V6C8VJo6/hy0wXxvEbW25Ls4Sm6AhpVopUqPOwMJm0CzAG9TwEyvrpPjda8NYg5XI+uPAZazMExV1oSXaT7NFZ1crv+JSu+RPEbSaC52KlsoK6wTfI4xSBwf6S0LRQfdr2kLUh+eavGuzOLmI/0mDoRTnBR/ue+7owspEMAnoLfwy6WAHtyvjtGcJcL7ZMnYzmOOcAIR0Vx3/ODHZYrjSWVirbYSeNOrOAPGx8MEw9R2ESk3Hts9AglgBVvjkH3OCYH86tEr0DXkzU7+cRW8UU0UncSKH0ZKL5G9yIscpqF/uSufjjfjdUtrSGCmyOshxEfbvOQA/8LFc64kxIrQMmady67FqdJNNOHfvt+sP1Xwg+4+GHqgt5hgcKw+vVmWRzI607+hrbj9xxF3Z+T/1wzWdiDpqwzIxrlXuYE4otc/jWZLB1fJUcVoQmTvRoICPJUcdgovG6aXiwS1aRSDPB6T3SXDuxtaVguBomnVbP+vdoPM4KsiUqDpDQY4gtSEUqy9Ev53AOeJvPgLOOBA85BaoWCk5B5qMOJtFtGkx2IEtjsIQz6O62R5ue4rKHa4hKqhwAx4dwMdTq/ug1RGKQBvhcnOuEa5iUHLE6DApl9DNYqgQU/ve4ZAi6GK2iTGdpNaxHuwwcr1USDtkB40prN0zYqa4LRC29NGF2QsxUFlrr+x1OrMaVBp7kPEnDOaHrvl0S0qeB0K2O06DufLBVajTMbzDb45ukOt+uOHHPkSnEQqZBJS2bBBQWDWld6PSmLZqIoC16CGbyE7MuYGT5LCb1f/y503rxxUiNzDp8jCNozR6RRvTgP7UKBkfUt4fHhJNNeBYhG5pfaNA77iUpZrgGuUWg5DO8L4FrRKYBzR39CiPKzGEUFNWaufKT74msfsxCOxSDW12LNPlhaXiJQtVaznshuFXifCXWEpqlpdDmz/mWIs8WZZaN4+WCLvJJjH1IXi9x8WHz5r647fEPRxh0M5br4i2hMIIipEyWTUAx6ZpHGNxQlhkpm4T9B25jFEA9zg8RR+ZcVq0l9SiNuX2GkBkdriWTgq14PhANwl32j7eQ5sp5FBY5yiH8OIUJIDOCZQsM4UL/yNYfe+TsZ0aKp04PQdUVK/dDLoBKP61oDAAZyriTwp2TXsD9Gockb7bx2Z58KlAR8TjjE3Zt1OFqk6e8bKtlPpB9gC36gT7kn8jCk1jLHQo5xh3i1A70MlSQYyII7x4gxeX/re9EEmRXbpFEgY9ceBpC7uNoMgdmtA4xgPs86RG+WueEBUqwSZlefVSuoeg78ZXJB0As7RBg+8brAMZ2WkzvFmGEJzcJKHnwVo26nRmix83KTss8V/ESFcWxfs5XsPl6h6EIc5/TvatuaRWAy5PD4bTk65ZHmPDrkpscRzd7Dmy2hQwJsa6azNfSI3PSoc6ykq0Pg/5fPYhKzNWLSiADYtM6FUnAi0z8kYRfMiTWOWVblAbhCm3C3pHZX8yGc9JgFZmFqrGoTf8jepjr8T0/PIv30z68wNDFw9CWUdFI3LjrqGxDpXw7HjLeE1B/gbDO/kzEHvrsODkKae+i6SSV5hKDxwPGHLvH5OOHMW5XbbezoOqItA8AbYZtHR4o2bdq5g6k10YJQO6w0XPMXO0WtxISyO/LFYzH6fofvC6idxcAoMAHaB4tCokeiqzcbxly0Bvufgn5yVbNpLykpYO9Eaa8L06JqlqrGlvczCcdA0ETtNvYsPJg3xwXdXLvyMwnJ8IWVlYFpIhnp4a2nPgLJX/G22f6l1KxteYNHWFwzNUUMKmQv5BT0my8r6+PNtdqoBoddDIwA+olh0v+3qAEdn8d18UioL2QwE8MLCEDVUtMeXfL73FMk62ty/GFC70KMMTG3j8IDj1ykrQxok+i8QsSbHT5so6JEIhAevwUmNfSN5eiM347AtIE3UEtqryrozKCZP2ecB5DY3OW/Z0W7G4EARVNz0jzHGOhLdrGMsdL1I7g5FdY0Q7YkTX4a/JyctQk80yxg5rYgoV1tn00UsUBncJstefJaY/+BEUlRvyxqB+XoBy8Dn0tAKW9wzn2y6MJS2cU2YH1gzY4anS40A76m49vXEfhZxcbkBa9jte0R801L5fpu5zhzXv/KLRUb3z5jx2KP2T1WubB/3VU3wPtQx6is4hXhDv1PRc/ya2Aplkq38cZDKST3y8TxzMp2k5FJSqGPseChkRFuzlBnwSqqcCng+Yf847R0FcXUYbSzRwa42yH8o7Cv24g4/8wIGJQZXE+aKh6rAuzDNrZ3/nmKgD54rq2n25m2X9HKtEOuKsWNUE9PTdhlQ2U04ill7DrgQ6rMdA3MI59mbZ8MX+q6pmvnzq3lsqM4DwzuZZXjJhTjkI5nAmpafOEVHzq8Pce0EZ90vf36ligVUw6TfRxZdAW+3mggJdXvco6B1wvOpCxMShvVEFG6GNGwuk2hEGONzG7Z9JPrs+2uV23cfY+fQfSFvSWXDxLDjvE1qeqCl5Eqk2GWvBMmKty+Zu8m0GQm+/zFubuVCWPUiJ0Wdb6Fp+Y/UftTCPVAk2G9JH3An3Xc/Cjp0MJGObjkbIJLUu/h6hMG7qXVuvHzP0VtuVsbcaQQOXVx4VrPiD07Etahvhwz//adcbMiWhU1PDR0D3ntjwQ/QHikRvosWvprUxFy5V093vJ8OmrmyQS19cHpHpb6gWGrtqlfcf0RmQWDGhrMIroiv7jyaF8zNUVDoulbuX2wkLZeaqRS0D0jbv1tsMPerq882lzMHDRCfeGGjwINJLvEXqq2vF/Iu9FADRBniDYz0LDFKBX2jSL5IRqncrdTaYbOt3CNDzAnQBt8e+CnOpwmf1qSogd0bNY3lWJMsj9pFf2sTwuBEIB1okJFvOFKyeT58xQQFqzFuhMOTeZgMXLNYFs2mwQ09ZcH0PHUr5+iD9yaXrpp0KXXPMo3GLXKK9wllLH8+UaQN1v8gZl1kLK4zOYGGDjjDb46MRFT3n6tx9fCSnhE2UlO9f7ZXIySpbUTVAKDKaBLQ9/hUDLdYs3lAKKXKzeyqMSjFKCs8watwyflxlp5/MxBPmIlchb9EdNYTK1Vbyak3sM39HDLZUmfOebyTRQ5wuf+W8iqHhmAni1ut22uI/QxonkMIymuuY/cU6GJpHbb7Gnz97eJhc0iGP2w0vHULSesM8jtNvngPwFF2VFbXoks6/i81EEFEX+Pe4L4zytUshAreznRVOYZNKD2nyd31pepQPnQoZxZQqw5IOSA4kHzQgzB5XtDif+I9exUsaj17KwMmUV8hKBBHTWK36OMkKTjJTwo+XqZ1a8bh2MM0wSoPrQ9Z3U1/d9Adp6+UulA595iTFW/omGcuT77KU/zEMcYwQjo/fyfpjvcG7dVCBD+X6JJGI+VRFYa0Km5SFTlqeKnLZTxsJMYhBDlign3KOf7CbYWtzWIukEPHaC5ui6dVDc9Waedq4l4IT2utuj8rADVQ9/wvzw61S4X4piOr3VTKoziUcTZman4kP7wg5jC5gV3rtgoodBp/MlA+0sWWa74lqA+oXm71eo93/LP5HapJaKE1n8ZFyZAYI2GckoDDPlJEQ2ML1m43VUV2VayTmr8W1VwjWOTEwBlG1Xi1k/erVy11MRR1brISke1fuYRreyORI2RpCjM24t8nKYseZt1eFnTJkBwKeHxp0bsIS6Hw50vFioX+RJb2mXsDQS6GFRkodobxeAxij1qJp8kvXimHyjrC5rnK1KCRb5NQ7wD5xy91Z0RpVCUd4widGThniQb/nWAb9agcEC1gXLQFehMYQO+RkHqdlVDaIZ0cWd2qiAZ5gXWEUA/mCAkHSmVB5JPTezBRzhkbEXZxtOHFPmiU1+nxSBDC+5ZdOtPdJXp9eSmLNFkzmiUt9YU4Q1m9/TwZknM4U6XISdL1qmn9W9xPqtwa29RSgQO0xyb6mSczNAEQWbL91vj7jCXesIN3JjZPCpsS4WDY0tEPX0jEqhjr6mOj2prB3/nW9m3h5FMskZ14xsKDH910tm8lorPcePh+XQDJ+ZyWFSbhb6RKL2qx+LFL+DJsdv6HnpKuUN+gqOztqvbpmUTgvGLklT951cytBrNuS1hCpX0n6t5x+yo3TJlaOvq2aYvWwXl9QwT8fe2mKv9t0MhHf1zCV6MZYPNXD1Yqt9pD0WYrW+CXW6pMKAHNQMORLjakmXwGxIEiXVwL5RCbqkTeNc7/rhA/jaAt2IMST0nVmzmp3ipJf8CTksXprFLkFRGBGeVH3WhGr/ZiJ4054YsKMajn6rRkqRN1oUgzHWUxIa28y70/bYBsXdx25psdQP6U5BbCH8VlG25vk/fjWgO1gXCdT7B0EnQ0gp1o=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=wq1seoLbYo791RQSSn6NSVK-67iJEJwazTkvP1TQpR_9yGbWA9Jc0NQlspnVXCub0eE6LTM6FMckSyDi-vhnYhg9UB8UY4XV0fs5prWYdD01&amp;t=635793215982061497" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ePCCHQpCHv5F33XcQdsUoy5f9tnkOoQXKllB6tFJm5ASM8qJmhAqc3FVznjELXCSmeOF8w05XFywPvPutUgHcoezQhDp7seqBnjKA-UbBL8o-Xdp3Yf4iNGB3sD6ZK6iunAQ1bR-7oaexhfzID4OwpgxIwtVEG71Vo7ilOV7EA3_NJrCul7JTWBQ-2nhrMJP0&amp;t=72e85ccd" type="text/javascript"></script>
<script src="/WebResource.axd?d=K2TryOXIxCuOZdIV0x1rzlHKX2EanrtImP12nVxqgDeuZu109iBbQRTgluA3wov73HCHC_DzeRUR6RtXvVz_UmOc4TcYD15KGHRKZCSs1-41&amp;t=635793215982061497" type="text/javascript"></script>
<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="2SYsy5jBE64fGjNGHjyPQVYdr9+BcifLWzoEdVpH3AfwPBNbXWSfScmiNoqodM6J7r9cygrvMJ6EEsTAbFflxqvuGcJzNfnZecp6s/kacA5baQcRa9gG6a6ekHfsdmJBp6mQ/b1jG0LrtXhTUOzk+JynlyaJ1Z8KPIX2H1R8d6lunPKJw/SqKUubbypmJ32BmIV6SpgF8AST5dVpXvZqcuX2TLc314caNd+a0tlJkNeUPyjMM45uTL6PuA9z7ygorAsKyo0BdSvKyGxOO+euMDcSJKNzgPv3zNLIOlgPWoVxWTT+Ea6Hkg6osWJ5RuJm" />
</div>
     <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$script1', 'aspnetForm', [], [], [], 90, 'ctl00');
//]]>
</script>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul style="width: 520px;" class="pass-menu">
		
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width: 520px;" class="pass-menu">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking & Bicycle Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/MiscellaneousDefault.aspx">Miscellaneous</a></li><li><a href="/otherdocuments/900/directory_24818.pdf">Metro Stations Contact Numbers</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width: 520px;" class="pass-menu">
            <li><a href="/generalcondition.aspx">GCC & Other Information</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=nclsis35ZWYbHIIlld">Phase lV</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/DMRCstore_department.aspx">Store Department</a></li><li><a href="/DelhiOutsideTender.aspx">Tender for Projects Outside Delhi</a></li><li><a href="workcertificate.aspx">Work Experience Certificates</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/vigilancegallery.aspx">Vigilance awareness Week 2018</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>

<style>
.nav li ul.pass-menu li
{
	width: 50% !important;float:left;
}
.nav li ul.pass-menu li a
{
	width: 100% !important;
}
</style>














  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/11/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit" style="color:White;">119311153</span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    

<script type="text/javascript">
//<![CDATA[
WebForm_AutoFocus('ibtnFind');//]]>
</script>
</form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
