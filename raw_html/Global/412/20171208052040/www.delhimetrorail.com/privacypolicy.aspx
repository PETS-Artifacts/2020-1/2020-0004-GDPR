<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="WSu5ktBhyX6ZhoFKI6srwJe2M0xAHP3Ajh02CuK4mZC/zu05pkejUYCj81XooHpC6V6w62kZSeLzw3F5/KL0MrwPD0WHAl6Eso1iBubqfR49Nx886U5s3YdRqMDVQsxGryQqM0gJnBRPBBbAYLso8UDpwM+T8ut3A+VyydetVXdWWDpaC2ihcnUOsU4BJSPs1WoMPNWcP+C7rX21ZBv2Mo3HcwqxP91koe4j+4FiwfFAqH1IiGL4Twb+KxcFMy2ehnz0dLVZ4K9Sv7TFqWbVX3FNzuOYKo5/ZK2MRtbUzw+wyCE9lzitMKOIk77Nc/L4rBtOUe+xqsHWqi3wHrswgFDRoVDfqaCFAVCgjgxBtRyy9+/KwcUTlhxjN7Q5mQBSNQmHu+zbzyxcDhgHDEBCQWbVmOHPkCNq1h+26zEbPB3BAFDSH2shiykAe2iPVG6FMjdGwl0fA3rmpsJG4+Px37ln8/nD/htfZi8fXKm7iIFueMGBnQ8t3pfQDXTY6DKSnhxOZFmTspcP8aCwrGxBpFZHvxaw1sXEXxo+V20mig0NnJ3ZnZsPXMmufeLCw3AZbF20IxzlJNTkFGkbmeAxWUcxhL7Xz5oDjkCdFAZfRI5+DnMPhWG5Ux4JUcIbDQLgdW/iYDltJW26m+sAexsyM+GeHrrLU+5EkTnFla6iLN0dUTNHqPJ7dB4vOOAEd1PoB220DUtjmBD1uFkAYGAAQ0do7xqjzd5serLv9JY6RAFcYDhFCd02LA2V5dpX99M7vMlo6IEpMYm2a3anm1fyVhs3BaRjNbLPn2MvT8nh5mhQHTah2XaW879dUnS7G4iFua2g3o+eP4FmQM8//VnNKtYZoNqvMEmHx3cGT+uRab5dkWVGblnxrmhXNb2D8F6cSUucYLLNfA0c7YIItD9/DWQNL3tWNvHUC3iBsBi07zX2NMtFwuBJlJqUZQ2F1WxX2HSJIFyOkiXrAdVqhRwsJkVvk7j8yUBT9xbPhPQJJcnKb0F3uMApW2fYZp5JlqEocIo9T9gCCSkzkuZHtUxV06svP0Cc4ZOEtbNtpU7nTavWpDVnXcI8YzoPEC4feYS1yHsVPJw7u5LxRZZY3EePa5b1hFJpTxKd66npn+uTfvpIGyljRwq1tTwlZHNz9BXJ8rFLQuC927ZMIx3lAXMpEFJ+qx9eWusSV0xDikibCDsdGDl1/7aaiPfo6MuUrtQ48e42DVYdsmpbLHIq6alr8NHL1zV3e3VRaj2DycXPLLxEZulhusDaLwaYQQZZuhVAuDTle43HZSz3ITPmcQ1mjZu/UDkr68mtud06oll6vkWjLWYkGbOEPoABO6CMAxpx4bIW0YaYPo7UFfcurSJMxVi/vc8E2obIIxymtgd9POKFNg+Pvhafw5EHyhsaflWTrltBKUQgxt/rJRIBRag12BLDpIXyc0QJqAdWsrlBkZTkxVUGgXPX9PvAnWPZ50QlpRgc9tTmWhGpXCD7mz4zVuOE5nQFffCX++A+mxCgNgTCiyNeOBO90barzqq+o5SLLUJzGEOYkIHDbmuweVwYoOPuqxpVstqegiDjSQTHuDOckfiYj0R3uxs3XQu9DnMzIO+8xqSCK1LZYLMxzTdLrnOl2Y3c2ilc9bcNU2ZirDN81RHjbKe3neuCLpT0+9DgQS4XyC7n9iQxjM+m+ZOV6aEksOTP3F2LXEuPttIpbiN3w2B0pk0iWw+QmU+c3akYcX+YyFaQm0WOms1vVsFrvjnxz3cyk3lakVuwntw75X7eb32OoBKofgwhRHiqpq4aOc8l8N4GKzTQtHK5thYg3RjL/AAo+IFyC6rPoDjIvoQqWKrcm+LC/1yh1E0Sw9sGvn0vZFhIKrbW1HoGblUAfcQJWQvtFsi+lLJiIAsT3OPf5fk/rNIs0bi790UlCYUh9WHBEubLjJRqSgUJMC0AdAY3cREbytB0wKQVxQlrQWCTDiRCI+D75bTaNBocIbepPDxcHOnwZ62hNHDvE2e7EcmhLbZPyc99Tfn8CB/QoC4C7SKt9HO5xST+94VX0DXmR51YeTkc/0W8o8b7KZypG1fglzPHA1j5JC41/dFFnKS4D5IItZNRPlbXTxeXLKBvZN5Rq/FmpSvCRqS6Ovx4fXLmodIiyteXjJj1UbI2Ue8aORa/vJjhRHiSCeGQMePPCyKdD4pZFIDnr7rysVeoVQTirOD/1ZCEkC5ZQf93//1cBpojG0MaGpPJGTPKmvHFnxv+bxyNxPwrRo63lb8QVDgllGdlCrI9NZb5i5fgIHpMwdbKLecXIlEYVdlsN4ErRxDu/8wTkibTzNWyeOG2hOENKhcMVFvrbPWCZWy62f7WzruNt0iB7wpcguVeFkLtY690dmDjm9bJnexSchi2cN2y0gvJhxYozajvzeP3HSDyIITdvRR/DBxksPEI4Nr5QdFt7vpZ6nVDb4slXC2GA8HTiD59kmr+aS5qXZDmv9KRDUFuJuIG4rqulzBo2npsgSWXSXeNS/FhcYt0wTZf2HbpgR7hDZ9xN+8DezWdHDagkOrh5ycYTECC7oc5YXHeCR7HWerdGxjNTro+d0se4QKa/kfjdJsrNO5GjYcckiJfsf5WfLVrSv9uKx8aT1bovn+oMgz1cYB41n81qYUmjJqhpxdFbKnx8QHZNay3IITIFR7Z1gJh9+Vb7U0DOUdKoRqF9GT/5WeHb2el3TXXw9O9SWhoON6/kXoUlPgaXOZIerKvsV5EIoi3ULC5v3kDMsNEAjG2yZGXeTjk0O/5jIGJmP1ePgVBQaHfZHz52iCBjLt7Fhiw/rlkFCCfaokVISVYn41Lt60KNtcCXk4OEBXGyqaQ5n5k9Zj/KX4gc9DTvmIItba/WbIHAieTEjlCzotAdZCHwOnHCDF+xUF/LMmtGZIkoyUO2rxmn8LiFyWBTVjEIUgQvH5naTMsB0N45ajVQ/HiDuiKwGxq3zr6e8+d3eAuFEN6IdvQGK/gBggkGuKZqVqpRCbX0Rilig+oXPsO8OTk+6f8ikP6A137U4MZnflGKWEX2uQIcD/x/rJwjBJoLxXkeWLeacu0wPYFDIKwN+A9h4ROUAhYU60azd8nAjxYCov058nCacRzDGbEkXWu2xoAc1nowx/cydUChs2Zr1dsNXndYWA5VjuyEveK74HiCTwygw/UvUATqCly2EvgAMPICVvaF9YhfCXSxiN94gClQfTA7KsGEB50wbBVPp6Y5yy9uxh/6spT2HKKHc3z+LFxXIfP4Ief+JYjP/dY1pFg+oG0FsMIo4jxvT6p4tAi+N4+cskbGnTMlF1TVFcNQmlhs/OneEkq5qGtWSPwsuNebiRnZkZ+jxN52AaRpoWcyEfmkg8SI6KXuS3xslGZi6VJPgKLateu+4rHwhgOLQCrPpDHCKJsz8+j0A4M0xvj75bzca1+ahc58mr506NRqoDxXqnsCRVtEFnDAnMgDutZD9D2X1ENTTajysl1tfco9DhlQkb/4M97oYAQ/n4xNake8iYCmldviH13IjuCDh7rJWHKXFlwFA6Jbeh3ndQMB+I0UViN0T4q79EPv18yiUFNDMXdMgtBIDObxlHU9rxxhGtNwMfDSk2QZC+eKRiqlxxNHa1+JdXPXRIAt96YyLFo/mlrpgkno7FN2Zfd4A068g2Pr5rjmdYc+c2FtnVv/0i6mrtndXRmIfu/i8UMrXzk8b5UVejcfzBi/SBpn3XP9bi2Ct+ToxbyPQhdXjyFsL8w3+esV0cYZBiLjhfJIRS34aqhG7NqhKhVgSuFCl+sYIbSKLIV8hz186wz89HDbOb/DXiDR52C6nDlogyGZdQuZfrYOBql2gtsd8E3S0W/tu8/4u20mOUOPkHm1bdHK5MVRhsKd99Sm59CrDrCV0pN8LcN811rBqoGGrcXkXGxFl4Q04nWsZ92jbHxIHxJIuI7n/oaSMKj3VkYWpcJr4QPcvY0ok9LEiU+jTxDzEeUQUBdrPLPQFzMnKA/QlBRy5OTfQpuX4UdArOZYaMnbR6VWX8kUjjyJDU0joomy4BpfOxqNSvgVY6FDve8l1iqeXGKnKoWK22p7sqY2oUhQas5BWmmntO06POYJcsCnhcgs7zjY71YfMkcAUC7FOCYjeWfHeZuW0wuH7xy4cKLq5crHrAwRm2EeDPOqu3AGznPMGXI5p8HBXltXZqhuDMWdXyJoCjFuapl60wsFVvlAHlkR5mCIbyMfxN4k+4NKJk5XagC/BTjrTqBmKZ1Jd1tgOQNH28NfpAFtsZAJ/B3hJazc+0IU9KSQFJCeWil8tS3oY33rvzs20HfNQy1TD3hlGHQzFG/S6m/4Ia3sWnEPcYPRVyLwuY1IzGsmsIZAR6KnBnyuEcRee8WEe1d71q/05hkQBjzBUqAEDu7CSB5E6T2K66crAuXi6tPeBG3Z5oZGPFXTpk3x3eMzSa0HjwT2AF9FY2tisr3gKkP5oGjF4l1VpTPKZC0uWXrOUkRqzqHKBDf+v1oaa8k9cv2k6zNr5vTOFwhyRiRq4o7rsGBrhec6JcCIAKloCHOk39AXzJD6nSu7QmwT3jAJdFHVKC2ySOgaJce8RtJttkqEqoWNw04xdPLPGIl8LzTmEjoilG89OIMYHUW+oBggMrMCZbjNQ+QbkVOwtuUtgyRBvkPanl5whAcG1T6BUtR2M11XjOaeKzji3fWkJwVor43u8IBee9RJWOtwK2Wfa1vwZ3AvSwuQduYBwd3gTZNsXpWWE/EXWHnx/4QEDwnr+vSEwqmaV15xRTLnWTCmDDmSRbca1gVUFn19RJSZ8fVRZn2Fbs3Pwdjo9X9POh86Q2JIpI5tsVxWQEJphRKXGXsU7QN8ByT8RcGSUNtVqP4Mp/Y9WCtiVCZ+JWFYSwLZRA2jRNAafz24I5aJSr65QNojXQLuX8HA4ql4X0drrK60EDbQdcGzBrAtJIiD57S4At4w14bcji+0pAI3ZrQRokGpx1wijp2HhBS+hgVdLx5bz/qCWtgW3PKwIy0s1Pt/2OvH/p8PzWLgRZuvRALnCsPZkMTROflaKcEasuw6JNSjTgGXQZK39DGkT5Cv4mUo5KGHdn7C9IVzmGUs2U4YMYRcoTt6KYY4Sxi0AjhHJlyOe1vXzfFsvQ+jaTc3jFokFuNq/dqOVPS2h90o5qpjZWWVGDOFYjKllFsb6Rdxhin0zFu8WuCRBe+pDu0xi+oEgOU1Zb6B2SaARNlowHPch8l4QooI36GV4zDvhdZXcdw5KKR8M/VPc5KYdop+EgeMi7H5uezABGSpELsHMJSSf/9l7r3+/sHPXM6NZB6E7k1BQwAE5ygQGeaHBQqJxx1w9dygt3vSBxX0OLo53gR4vtLi0HsiiQqQxjbjanrtJzFQGe+FoHMxs6u1+YylKbEbcbIb64oxVgond8Dc6VAx6ycKebw0Z+UVvbW75AGOqtTW9WJEa/x6PzsJO43I6lQYDXvDnuntOztRoaj3Waeoc07Bj8DsqhxLSPsuQnX+GhYL7x3o3UVghkQNd2AB7IDOZS+/grhlqF1A5L6c4IuAkeIpNv/UBh3ul1aYVjVzgez8N1FHWbsSpTEGLzjOwFAT4ufY7tWwyl8iKjCudiebmiuGxNHfr6iP53h/stRVaBbgNdlDIWTSUuhpMcQfPnFCEeT6xrt2SoaJlH7ukOUr9JIjEgMESm/gJ0afIm6nE61c36pLUIbqZ+eC4M3m3VuxR+kLw/6KMioIXvCm2PnJqHlL/ravhXE9IXCgUIDncpu8trrhkG2K2PsFS9ZkvIPY+ri1gcEX25NNCZd3hPpkApErMNRFaG2KCCvin/13RatkxjLOE//1yaSGQXloDMdJ7jTJ5VEu1ZwLUvZ2R5VDyaj8nsWZ1DTU0Dfw5MLkeOfFxM5IPZJ41AF+hHHcTBpePG8k+xvKBwsmVextnmj7HLoUhsRXNOyhub7pxY1WdoSuJO3cUiqhHkZQoz0FnNsHy/1lQDJ05MpyRw3Hw044Hu+rBdNZPpQX9jPqVVQ92RY1/z3gmWCqYSuOZP3X9Z+LbIuQAOMOE1Mt3oOZpXRFb3x2+mKcNTRxGtBPnb7q3DZz1HyzJh/zlEIpxmHMXsflsTdD63tS7DZQ/gHCXa/ZQU3G15b6PVJxV/eTCSzWqtTiPVFuQl3NCAegU1fkrJZRaHoM8kiAF8El1hjnExZ2hsG1m9Mu4M/L0f/0IzyxlSd3gF8zU4G7NgUdps7bodFb/O3PspaGL0U1pcisXZf4zhCyD4Jy+1w1ml7cgMR4oJqTpypJPMehLmrLltLb3pg6RX+HZJtaJK9EsMRtCAI84eR864Ubt2DuPxQtK7GyGZQCYwvp8c1BVipB9Tw1xNkqs51P++New7q8rrz3efpHSJjz9rvNZ7rvJeIE+lSc4HL08mmLXdvHP7MCwjZC05u5U4WVJCHJxeTe6gz8MXiZS5IImJpbYF2YJ+CMGxrsooeK3YTn2Wi9Wj3lq1M9DIM1BrvQoxTKr76rUIlvBU6HmiXxqSJad9M4WWPSXhI/9d2AEypqtFPb2tvdWI0DI5RGuLOk14m6i8LJmd7WSpw+k0AbYHJtLJhQPK/SqP9JOWthm4/1zoYFvQrkdzbKUDzzJ2K397u2O+p32tkLPJ3Owz2KUZm5BxD0KyewUi2HnVvkEstAe2DDqUQKHOiDvdIKmMTQ4r3LJVZAwhneYz7+6iB44KevCt2m4eu0EQAT2Xrm5D+Pem9jEGLjjMJXzehNIHgTNcGXjAHKTD8Ig+2Gh1n2y3LNJ5oPwZgFCVUfwVOceNe2pJRkFn/P1st23ROYEV1NDhT1HpKcFbr9/BJDiQiJPXdp0NhI5VQLdrxjr37TXDLAdwlmypQqfLIt5KR8XoFviSoaNdLCQzxdUEMJvnTmzA+rL0TKw3LuwuV076EiX5ZwyXRBcKRwq0vxEc8+aAivn7ilPUV9FjEQtlUDfDZ0Utjnkux6fyl+FDUXXxnn6OKh7SPw9/+8mP8qoHy8Ez7zKQzw/Rro/qRWxED2GuSHNHLiBVscXLkpfZcEFGF7M+G/o1RVKhKtZ7FAVyI2p7AgHossLnxHRgPzbjgzhYqpU8VArIMKa5HOhV+/8Mf8emtz3zViD5gAz57pjjKI5kGqepGl16NarNut950QyB6Ey5X/XmM81glcMcfqtRrO2ai+QjiTvJ1Sg1AmEwhv+SkIbSoEul2TQ208GccB6+Hq844fMxN3JGijWnzYGwC7/nZ23AyUq+v8yODz8PHpNeWdNMKjwfDRS+Elv9wK39KqPtvNh0JTRS0dATM4WNzOkttGWMJuEvIXbC/zoEMj1weyiymYvLuqONgqvWQLCAnncHh6iJdj9iOtRqiz6nghWY+2Xnuh1//6smB2Sj89pA/5+XuQZ9KH1GOk/6UlE33rqq4olwdqNgEiKwKz6sOBRpw8P9CSCgCVJ1hMcNu3aD7WfjJP+LEBqpU/dv4b9dZHQzQOMD095ygv+qNSncfr2e/V8avvrvg5UOr0+hGEcUJi+61zmwPHnF8GB5MBQxmxP8QZsSAWuMKGyN0LWpwgZV1QOGpwLX24yoXWGHDC0UYAfhNbaabXMn0Sr0rHPJvNG2gkST4Nzk8Dnj7XRL/OYWGYR+7YIW7m7178NKdldvlI7kyKpXl6Y9u1EDzr/gKH9CAB3gIRO7BgTsWgfr0r4sTo0V9lScloBQeXr5iv1vlL5fxv6BZJhZ5U8bS7SZTl955T4D8+/tAozsaKtLrb3l2/CzvDySfhqGbDn1NUKmqmt+Vr25SW9uNgXEaOuZM/X1tuOE83CeoCdS/OExR3y/Eve1ubK3UrTUhgr6r8vuYrcHwRsSrEx+OzmdYF6wacN0oqZHfeheVr+CEhIMygYyAddtFxI3WDOcNYOebODPL8pJ9iIaMeRYCQzgLIumoLqUsOdHSngSdJCjt0/CAS/QRMi2B8htVEXLtO6YMEGlILlisBEra1kFF8rZzb6ZurF2JDGp3xRfkOIqcmr5vv3/UD2muB25jMC8laFGoth+fn8H02wJqTG1XIOQZ1Bh1o8HI5iU3m8CWMYJ6hiieJ5dyiOVgtajKsjVKFlxB71if8yy4qXrEhMrJ9ekIrEbcAsfuJMfwCfpm/N2zV0ig8TfWUweoLdHl2gH7Rj0mGkNkWjstkFU4TviiDYVqf2svvAY4htVYMoplBk8rcBDNfYNgteXsnxIZOSmkn2ZZy09KSy6NrRJhSOM1GpAzF7+yvAsg2mwH5HfRU4rFOUVbr2i9/kDUEdKUowUnF/ZSx2JU/jAeD/5y96Dgty8vjKdQsM40Ngp6r+1dd4A/6/p4uQjrQZvjE2LDBRPYqHx5Z1neSI5L7MghR4o62jRiHKDIXPj0irNLz1XL32eJKsIvvGceJkmoFRfnAJuOiH5LH4+Ax1WuqQUHkAeWqCatbCZwKv9CPSe3W4oODF89I/VuwnLcx3EBlrjZjTwgs1a549E0l/30ju1EjxSdPDz/0whgW2HcJGlVblVXX0uexbAJ/CaHUo18wbO2PFYSACcl3G1V+ohYYEEAsHBxckHYPWfA9qachzPB0knOGCAZVqYvmlMtfX4LUIEaX+xPBNdjI/Z1rQKP0iUIgUvC+pOGuzcKkid3va7pKx57vnFCYaDvZjQcF8nA6EYS1uENI+1SVTBaIlIgnuyKwcIER3Fsdfi8RgEgQOkqPbImcoTru9/lzlK9MhjUchEVneYLAHenT671nB/I1mXJwhmhTqY9JAa16/Q2Zhys2acvQ8UveKr6PyYfhfXCOOcTw6gv4s6WcgrF7v+LT1HMOMwftz5p+reig5FSAPBGWjX7+Kv8NTXALdIuCAii4SC+5KdJtJpHOgamZ0XBqi3NpI5cE1VSfRlTdAG5Lh8DoXXwjBweY9iu6wk00Fz4Y0EE1zG87YkPAN4J7xipA7xwXa1Cml3gNHY2HYoojZ8EORCOPAIAQJ64aes6X4hfYRLO1FueGRmdrok45T808FJQpnMHYjAbnQcsNuFP9FJEAGoOPwZgo64LryQcAzbV9MXhmRlig5+NdFm+nBEeNajqboMuRHuMGjvAgZNBTXcHG/Eo3XutcE14rz4IkmlqCru4MzLcSe4/v7rRsjm0gYBTgMD+j5JnnlppeJzYi150RkXT6JIACCrmxn0L/7CUs1mFZAqfGA7/hkv8Wk9U/MmFiGcweTcQFrfW/QQFckHKEoClusx/wysEYFk/c2e5udLMMnFqdR0Wo0cFwKMBRomB+1cxwVey7iLGhRY881lBON0DV9zc9ejmsbs2lBMF6TnCOfzxfC7H5ZeZovBeVDeugk0zztOttEwYGFNL0gVnXgE1kG0QdJ1//FUWPXXDm3n/9iDpmEhfjKR2Ouf6AIsZrYQ4p+W9jgYHSK3cOAqmxZjQczrjpOqvsaWREcsZHfh68hLEeWq8WGWXN56uvleUgi148hIxhgXWhxEcBeMF+83g3BuKhffo+TvKqD6bDUASwL7zii24iYDtQadcQLgcpr/VdyLaHFLgsZaAXkAOG6JLBgEAncrvyN5mfBOv8G3y1P40krgBJ4db9ypWN31b/A3da5G5j12uhLOJXgPypHsjtPsM2xa0dBbqq9CDYiAfpUZHbeYSg+2WPl7Kkx3pft9TRx9t+cK8pEntFutlDi72SiNKEXSkmcQNCwn/+GdJyEfS4jgrINrGry/+RyLGbshaR6nMbWiJFVv25dinTeseuSM1cRUwCz4rfZwvhpUfTNgsGHyTdnXDyR9iYm5YiLmHwE9SH74XLrnYWO+YuwxzcuBbnCUupRT6rLJQO/fEL+g7Y7yuzHiqmT2BbrAZfvUHH13nmptqITAasRx8OdtmHL7IHJvsUIdntPbjXb8ZUohGBY2lJ2ubwOs1kuy1zHrLjawqmQgTXSe+YOK3jFLdFvbcvWhCnJSLXEzQKxFcxcL86tdEIzBMp/j8/NJrNMkyWJEhF+GXwiE90cUgtqFiVQSDO46TrglDqJ52Yw3RRrffKeOKn+JBzWmmym+MNDWYA/yth/Ln+o81+bhzTJ4UBUHazlLsHDWJGGHb9V0UwEOWdM7Q97NUIZ4p/Rxmqy1DBqnckGJF/O+xrevV/r9nuwXe9w8sEDr9Ncr/AlR2W6IX6aVtfIFBSqwVQVcx4zQJzJEapSoyiszZYdeJifrRILB6VqZKY14dJZ3BTJrWIt0sgUBOosOxP3DEP5HC6IvV2mb2k3mrieVJp4vjSYJOLafq/v60eZudgZWEen1tgG7RgkaodtiSSXVRl0jhT20wxZiJnbZgTXWfWeS4TTKMwn11jlUGjbaLNwi2eHd8Qh8VC1njdB5ULHKJpFxRVSOO+W0lmHyiTg6Kj/FO1C7qO4yoY4OdWnIKgOoNJVshiJFVWUAbfdcG18H2/d2VHjs4F9IywV/qXS7/TNo9E9RQb4NZsKDbVJeshaILJZnOCVwte2kFRjo8EawtkkRSxGfSHlcydR1yYL2Frk6ln+VcP8BfbMbaAFI3d86k1MrMy8h2b2ym3n28fneHEvPC+VJYQV9aSG2QrepW1EpTJs7znobGRJpuH1anXhoIfT/fQvJlRJSb5Y+ktgWQtKY7Eh3ulP6X5a9gpH/KbrD6djK02tgQ3fZaJ9XlQ1sR3sxzMYrdRJYWmpbl72Wq/L/+sLdBG8LeYdoa5IqKHTJ69Yh9iFFGdq5GLRic2o1kReYZdiRhzW+viyGWLXFoNuVOGq6ZeQgxaff53v/K7Mz6QbFUtWtSy/XTELVYHD6MzE6uQg7mCRi8YFck+K4fzSTG4v/y5wrYyWMYg7ZOSsTafWF74+j5Td5YHdCR9Is5JzRLvS9LCKqNxoBwVOBbGIiAVZ2IZE5y+06d5XIX0f4fIfER+XmmpFyDExf+TtYCS433hFO5x4bH2CBXHKW/RVRTHa08hBTVayDvlYzvycw0aNB84ljxiC3neZxuoaXM9s6g8t8mCLLTyhf4igynEIDEnt2ueeV+dOKHN2ACe/pFGsM383QZqjJTOynfuNwGqhZNAbcpW73SL29pjYWr1e754TSAbRJ8kuzsO7dwfUf3scU278sXBeo8FiXSQrC+J4Q962vN4vuNFNOxU5k76/fhMW3r7SL4vPlril8AaUlA1eLJYaAHF5J145IqTAD83Q+R1WhtK2hrQabcARxfefuX4eNPfCHNZLUf7QqfEmAQcoOJTqXrzB2WVkHE0P4u+vjFcTsCz6V0fKvQMkzwGjVEp9fa+Oc5bVF6bItmpbeBnAkBZlg3yP3Gf+rG4UIsqKpUnzHTAyl1gUp6t80WZSph1oXkZEXD0RrGMqMui3paf7r8qOlCBMDrDhl9eI3swP/J+dIrT2sjshxnQTaS4z+09QgpXhjQn8lVdm6Jdi0Wg0Sb0vNBKFKpUpVvvsEwtfc8mEB6/9MurWL8UYmKdMKaeYqJnVv7RMAJofutDVhrZeXu1yDv6q1w24QwGT30Zn4ptTC33u2gAO13lyOuepBsyDStHDAHT2qvS5fEa3yqP19fbunaDFd4CA45RBXu+3XsXRQPjogXERcBWGxDg0eKuvUre5Ki9SnKTrFQgC+NMcHbPUsh9/OzONLJ0ENe+8a86VxqgUUVV2Ov8t6kDTCgS/TX1kxaoavqC4zDCJUZEhrsNTmnXYaxMrDaoR+7Mzm3VSMsF4frjHC85Rm3Qe22f3vAEKXnzx07kzEO7CpK7NcNCEcvh9mr5OgvXU+qwtib82WKUyLPVQvoLtV9Hjy6VzfrWyV5tSgP5btvZv3P8i2cTelPDhjq30lLtGa7KkcdPpE1vXbdeLhOqQmQG3W1zUFtOgpKXmQRHsSww7U2s1rKNEXnMYU1Zlquom1DtGW1B76JGWMgz5Ou2iaS7jyHeOCYKcHgINdgeFOECmCjKTR/JAJFdgqXxOFRpE1YHachgjRsD8uaYbITuqJaFGCe7ACJUJImse9gAqIZYPRaJ0swAcqeJTrnJThANSdzEI/9nZljODogfRWNtitsfIVaGq1HLEHew6Vz6Yw97nNaexhsuoXUdA24UAN5gseXu+u1zdEdZrKwVJPlJ1WCSADh0j7z+/I3YjhmzoxT6eKjdyRxjj1LLwVJzrN00TCMhfR7KolB6NCm+AQIR2s5ElQKhEJDLEgHNBmHA1IwJ+rn5K9DMVJRefs7NRe702+pRPl92rl/AtCWKWppflujHKDFc3/dIw7Wwy8rtuHnBfJn86yQnG67qxGt29TC7ZgYQVr03sBfFC0ZnxG4rxDmxEuVKvDwuIvcx/AYZ4H2vESZF+CHB7HP1xGCkvG/5/rnOq7jsRl4PxJ/gdJzzysCjo1fnkIArFD+0ELhlh9oJXg5U/UgFgfHkZR5FdePVrv3UWnFqnw63umZjZcZz9iY/z3xI4+/SeaQFzYGYJ20InSiKe5OfSzBre+eoDDGg5s/vWXMPT24yYGQbqrICjBuvz9LMfOTd915c8JzMdPEwegylXY/pHUrqhWIrEqB5yAI+CqJfC1hHKdTlUh8m1/VsSYrjO4Czy2MDC8FB+D8TnSwqBthQiMAT4HPAjBg9bUIwVNU3REol5MO5Tph0r6IM51+yncO17KYKvPmuN6V+r5NKSDG/iB/0/Fx+lAQ1iDqoyVzvcl28HKXqVrFJuZwr6fkAf4UD1jjrPlGt2cuF8vq+8SD2ykexjJjofhaj5uMV7jJNhj9h9hULpagGTNqgoKAhDkKS6r4jxAofukEcvKhRsIG6wXnYFKfxNet4sNopdOwGhOHWj49IVhqJjFJz/BcpQVFs7GumpCF1w1b/SuckQAuO+q5nPU+7B7CVF7GH8yyHg976TlXdlGFARpndnwWbWAbWjA3IbBT/3Egz3nSE9D/njB7w7D0m1k5/Kf7NGUe5XpMo0PRRWXlZfBFHE2paM87sMxzXzuZI3QPJZqxZTDss1wEeG2m8eVQu+K+2hktg9t7W6Cx7vqWrXawYH1QvSBJ/zRpYZtPnkHEdzeLxO6HK/b1uIeWpJGnUB5Y0k3EE78dWB3OQxFo4+RBjhCO9HnppRhIdhDRazcYz/sugipjwx0gEmk/jnfAC58bEUEIyStXZ/3olDoAyCocc4/q9JJJeY9qcHw/xDhtx5a8u1ULBVV4pRexVenjE030SHtHlOzV3OBpU1Ne//N4TB2LU8rVjqYWWOmqNZ0dbhf+yvnTSjZZiQOWuQnldGY/ylzcunWtp9C0ZoS7JLoaqc53Tmm2dyex5Q435BhR7sbNPNKG2lQP2NC1qbxPR/LNftApKGWziUQwUtjp3mmukHgzeRy6HCodi+1TvDZa2RCcalP1JJz3eSYVdIWQ9Af0AOaRZrhHz6ZmcRdw1TlpT4Tp5ubFdoVdpic/m3APHxSz/vaVUpRjRJglCWQlUs9ESMltsIupix3s25uIpbeiUe7Lw2/wKLSbwB0SH0sR000rSAGQRhaTNh+YpHbVi3g+uqBt4qqUZYgVkCCF4DkQnyec8OA5gmre+eWMB5sJC+0v11av+WtAALj8OKcsdY9/mYJFgKd/7ntIAwKo1QJDVhDWUkmqOuHLu3PAz38Y1IlvFUrxDkTW+l1z2s3YfqVaGKxXJ+nWmLJjGYWUNHfQfiFunMVJVYvwgE6Qwts2L5DK0QMCkeQbUC/JRh7tRfI/xk2nWqq18Px3IMmGpz62DUxhQGp0z0BlMgU=" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="nbo9ITZZet2E59h3ZDpaZhW5E4gNcwMZwRM9/qU4PpQCflV8VOu69vT1DyaFo2Ta7EPgACLhHVJL8esWnuiLhGSxkKQaMyZEUsso/41KAjeqQbdYK1ofjE2MCgRMgjTNSFz3a11nwWNTAcD5zXk2Vw4pmIpbn/MR7+bUVn2I/5/WuZUyVxsEXTuY6JG9AAjexB8DJvcaOA5nZ9nEQYWfQ89Cgt9iVmmkn+6HWIg5jNMfKjwhDIkRZKJXvr1hpgUQxlftoVBCF82y9zJlRFP5bOxyCAc5a0rIy3FrpDfLMDjXugAyIjd81ElYuMbuU7nzKMEdU3/g2WFD3NlEkEmb0A==" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="#">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/eartquakesecure.aspx">Dos and Dont  during earthquake</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/otherdocuments/GCC_up_to_CS_9_89.pdf">General Conditions of Contract</a></li><li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/externaltenders.aspx">External Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=grsGMsqnclzsclld">Kochi Metro Tenders</a></li><li><a href="/tender/Default.aspx?cid=3AGNZpFcFwwlld">Lucknow Metro Works</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender/Default.aspx?cid=yGEt7G92BVAlld">Noida-Greater noida works</a></li><li><a href="/saleofscrap.aspx">Details of sale of scrap by DMRC</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/advertiserlist.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/DMRCprocurement.aspx">DMRC Procurement Manual</a></li><li><a href="/tender/Default.aspx?cid=vlVO3VPqpMAlld">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="/otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/otherdocuments/VigilanceMnual.pdf">Vigilance Manual</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>

<style>
.nav li ul.pass-menu li
{
	width: 50% !important;float:left;
}
.nav li ul.pass-menu li a
{
	width: 100% !important;
}
</style>














  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">87732051</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
