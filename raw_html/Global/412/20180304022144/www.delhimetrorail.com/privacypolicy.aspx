<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ffJjAbiOiLwgCPobI2F9MhlCVWu2QCktlgQKfMGjNrgRBs580aB2LT3lrAzZtrZ3uIm63aFUXhd8rrZkgfWEhunDtl3Y0QU8D1ChPIgs80zz8cOrThxOF6s45Jq0hPecocCc+MsCcIIpkci0HPxa+fnQfNJvSbMmcnTfn5/bckQ3PIFlL4vSdqsOzR3ksC43xzXPlmLVcjU4/5oPr8GsBUvbeNbSsLjE1OkunnD56w+gfyQm2Y1vSMEtE+jkRRuoays9ONpE6iqVbKpRZmkRzR6UfZD04a7ZGMc16qh0Ylg8+pkaHSh2yYYzOfXlXvhQfvBQT8HujFgS58KK2xoQXKDB3/9fJNLvv+Vov3nPmUiDjecDYjvuqvWvV6Vgl7w7VjVuJo+IMd6WpfhP61mY620XC6fO3QZMsI9f8sBmveSTwWQqXY9yXic86BL9m2rixNF82yXIBJTVPezNFmzhg7oYjGLIRKvIorCm3tp5uY1DS+Fu61c0hLuAuMww4mUtNWakmmidzo6t0G0xflDbOf28BLj931Tf+yNEdpT1V6K+DbS/bXm2nQdwE7HlQ+5jJxOIvAXvBNhoBObyCz+cSjtM5HPAPmzhooGP/beExCLsn91aVnZxJXjTrxI1dRdytmCQ3ohE/+i9HXlmk6tj+KBUEKt/xh46P24ldtDRF6FAErcWnemOJoY8GTmtfGmwoFVdV4DtJdScUer8+JdmpEDoIE+OfeGqT9efOv6B1YVMFs9NGrKDvEf0/iop6SMe4SWIfxR8h//UXCmD2qaHf/f8g+AiETMo+3HbUt+JRdDp3d2N1E5q3pGFOcjC8Ly4Yu9gS8tscBPYpchoJwkIdnN96hfdJvtBq3I0cZ2pJIsOAjsgV2xlqsm46j06+8X+hluMv1u9ZOmqI2RggAAJGNAeqbARtMDhpGikSAR+enCuoiLyqIecm4fla7E0iY8qiDStOeceMEasebwlg/DmG+7rVrSerKhzRkj5q4PjvongYTvZFW+4gpV9R3Iklzrlaz4j8W8Ll+7RU3/N/Z+iZugkcZNAIxNDG+cbVgwIAuRZi2Wy16K+Pep9Ntr00ivIaQtSvKOg7aVEhHyjOb1x2le/+PGDAmcJNjomTHwq00nHzRMitwg/mfRwJDKz5tus86tYGzpqi8fqCIwoy/kW+4wRAjOtLqmNBqbY+soioGW53KPhyxlM0Aa7IS4r/QfEo4L92JwufCX8hdX6Oz3ZPHo+47yQC2GspTfSV+XzHo6ahqAWB4D93xb+1ZhRLhw33KBJRlaNoU44+eP/f9CJYJIgXDLoiRQmdn2SlpMUjJMml/lJYILYLhFpCf1ca/vQSaJLRCJW6Xkpi2qSmgoylKo/wv6wnomG8x9MJHdyfVNwy1+IaBhQc8LEYjJ2gX8xfNbS60pMYDKSnzyL6QxceVn5IejeRxC+g3cge25x+/CUXeddU6PlNM9rkALgwTVMR1O734McV964yB2TxduMvRJbtSbfvF428ecEG33kBlB2YO/grdA4/0jBITpqwpCY3k0Vgfe/+V1935oVoPAYmXhrmz/lJ0DBZjXIw1q6p3WuaUo99QtwZtxcipFYX0ZpA6cNmL0IWOVXjPl0BJI09gXHmSCOKvkTdUJ87fq564qpi4dn4H2PuCpLh5GjqGJvR3AQSZIeW6Z/lmZJsvvuw6oGj4NdrIv/5JFVoXlr7Mfo6NbO0cNmkVjexqtDMWsCMQnE3hfGJYMWIcbIcQwoGLAC0RELLnfexL5BQteS6AAMY28yzaT0mgjGEfjNU37ZzR9qSzS45x34moNSKzhmrS1w8/+b934qa5e1veMU1dLn/uSlD2o9hQqDGKKTApD5Er7oFN5PvbFDacrTCSQkv9mxP0c/3qYLaCuxsNootGra2PNkVtdyFTfscndtAP0JZkgmjHYQzYbWTHM330lCXHHW1SAXVOC65L6SbG3/srev7/xBMjylKB2IH4zl6+lTr4Y5erP/RNviTGAo/QPY3n66vBT3/DwCph3z5DLG1TXKgGZPwgYMpT5xH8UMMUCygdXzb9OIrIioUxByn0Ql3rcCOFdm1IX00f56acc4EGsISGTcYgqYiALveWBSJP44hu2BS35sKfweryocV/TlfZLCJIqtUenCLGJj5rFaQvlvQNjqBI4qIZirG02wwuP7nuUeZUYAzNNaLH88WCZb98PbNla86V7zt2pg/14EjHwnIYm/lVkX9tV1wyqbdQ4eofnsI8ys6+91bjh1dIKexaALPqSoBfIxdurR7LWBARBh9JRurLaqqQNUWo+M/Xkdu4JZfm0FaidbX/6hsuPgOV8EAyw/AJKRlYhzXqRE5n1lkXC16b7hAjmjIewy7mEvaewl1HCueJWoQb5b1LScAsWVLWFOCCVdCoswc4RKgpDpVF+lwHfhw3Bj4xndgZgEaXGP2keZOKJ/0LTDnQQMHC4S7TWUrpTuffzTkGDuA9nXXyheXUYVEhafvpXh1ZCyHTUxsIPVVIyG2+19WJZPY+qHLR1cSnrSFup/ZS2dlKrpc5W4GJU7xoOR9JNY6FI2voT6EZ58Jf8rKTdugctGI6H4lSOkdBun3j1BtLqZHv01ZrFfN26Ho1G/cH7rqI63389O3AoPfUAqQqTy2OZ6HsxC8jOZ7WjQronEVnnjcYsCxXLI8UflpCZYmEVbWBcI0WXjpEq/rnkTOY2uo2zt2emZQkjmM3CSPHlulkcg28vTwF8ILBb/sOCmZqX6IcD5f4EXaWxf15XJfWHIKotZZUH3toMafy9ilqREzpOlK5bosjoya/OcT2X/IVCjj/vtMGGeZ5gi8dT0ZA4F8BqKjHoIOpyFWIBIvwApe2aPodliHG+F6vkt6w5O+wEG+a1cMVA/6U87HNSRrtlHZaYOQ4uxCCTcnKeDcOgrh+r4m37DwfpxjfP6L1lwHJoW/yf+20l2xke2rReHDLpKk9GGgTP0AL3/M+7TXY8xXDOfcJCT6YNq03zsmKVdbk9vAetpnwIJJyv2WwN4OF5WJlHfbscREFfIsbFpVhOqo2D0Mzx1BFogZ0/1EQgkXg7nJFwUJaFDUa1LJSFQEo8qiyIp5AOaAekr2CsDzFkxXSoBFM7MzmZWRghrv5JiXwXCMQ1CNs0CPB9BV4kJxd+P7nrDllQAC/QG8HarYZX86nopIygmkIGMxf546Iqh+blk8jMFW1idd8mRhlYKrlYuG9mprpgrOtKd7xRFIC+OOSsMHRdYAhDvrNv0Cx21dAVCcYPd6OHkSTOtlm5m6FQjIVjMU0/j3UTy/S5crGQG/xbujv5QeOIEQDJb/0tw8ozCwJYd8yrSS1G0TWI0MAbvo761VFGUpaQXA2augXnbgMM/zEpj5MhX+e9LzzxMI0svlvTqmTodlqmHyirH0adymznrlGWZ5cntpQ/nb150ytWAi6Zx4RyNpE9ZZdSqueg5hpRYhMCggD0jE/aYpjJZIxlv+FPUMXKiynSFoXyo5GYWuoTZGOcYyvYqim/B7Baqr2cd652stVS+NZ1eYq4IOj+7ADPxCA8brqUEKECBWhTf4Qw4Mm125slHQLBlaOwedUwIoavb73angqGZMMD6KQI2dhln0lM85Cwogo6yK55ow1jZ+cD3nu97akvZgnvpg9YkqWQ1hRgsf5WmUfY525GjLaJ4YDaseMjztp/DluK1DcNkpJgzOSAhztm29RCg1UK2yS8jf/sgtdq4XZ0T8QQJvX2T6F79gEt/hn8qWPQ0HhXsg4mP/jggiLMT0hVJ0wGcQpOEGZEj0E6SayYFRcgt5/+/QSHMbAOG3utSEgcaFBgvnQyisHZ/TlaziC5bnm+SksqHEAc/iBT9QhZdsXTzWVSCWlpcWNBhFN5+e4yrqRMgcqBjvsa+dOdnRPOdwrDvA7Eud4F/ajH+Npy2YSbxtjF0QgVEdPLG1jRMaj8JsEjGo7ILFbDNFJqrK2aujzGnYOCy6LQgPeQyhIheGPs0nujj0q9RmgyTMLwot/+1B3Y8+QYuM3LsJlwVxK17FrcuxiN/sp95Pk9wPOmlGS6VZpqHxp9sE9Av0kBj/IkUdSpsQgey9KcgW3crdUpv/QcalNxvtBMczAXwIvUtETWS/3GnifIwM1bp5YmOdJsdPJJSQG98LWotCVxmqYT1/g/iL+7h2FIu3IlcBXfEQTw+TR3igs5B1ZUx5TGdAuRtPpMZw8EqEWZfrc/0ME3KcbxcjMUZwJ67Yo7v3yLlEKLrfoT8nv3IWLei3bDCKx5d4G/qrIDphgs9oUSDm8T2raNh/BZra0WPwIxACa1OWTd8DdOfWUX5CuNbfj+1bbiImC+gxqHuvPT++ETd5Urw+BHd0F7HNoO0S6X+qXRzC9PoWe+sMHpjcjM5m6+kB2rqg826XIidSGo8K2rt/Egu+YjcTt2fEcjJLfTxl+wprYpKiS0tfF1/qFtbTobrQK9ueJmhhtuKWclTE8rETrq1iBpKErV9fomMbGyqmYWsLq+0V9+txRv3YLJvwgPRYSMqNpWSSPB2YSYmWDiiWe4rNvrus9RoeZAtHuCUXBTZFaN3Vvenu5HTe8IlpQBcSmSR0eRHm2EdPQeKRL8cOm5yec01NSBcNhZRj+t6qzKqLRI+TNPbsa70zZHXSRwnA+7bO1gLIB1EpTyyiw4cnqtK6uY/g1/J/tfqGUUhIPbL6N+BToJ5HgXYX8a+SqtL+TuCnWAKS7Q1bDs/bUbf/uq7UfUEDdCiLWVAg8wniCwMVnf1l6Azyk/vxJhhAV0RHgw2tpjU3ZVcQ3MO0i4tYJkFkb4sf9Ow1T4Vs6RSXnDgtG3ehqMk+kismWDhxGvLzTGDwtRZ3/GfBiKLJO2lGoXsG5Si99cqkjP8k3jeGtYGc/Je+gG6f6ZEJ7oX25dB7GVChAwHiNbbIY9Gvf2BTJ413svSN6jItMCYDUC3rh5GusmrRQAfSgXI5tobrjILBqQRz8Ldphl2n31aLkkemt1j0St2JHOMySD5RS2FTYWW0DkeI5tGtmY0sVD685pn+uQ3EoOkbbxhCxvv1B3wJAG8zFIiqnbqDFRIt1Vl1PkNM+nk4G3vl1Hwgi5dbzOMCP2DwPC0H716KVWYz+5/m9RrDjrKyP3HmXyjkjqnHaQs2UOzOB7SpSK63YJdV6xf7AKuj8aJxvRpigbSJopElREYxbEBXfI4oZPkOvmmHVOqNSWt2+4tBYpx9gN0qfWF7i8NHtk4OE9fcBqZw3hkZMfrHFcuLRLVI9x95Z9xvAMjEwjChNPC6ja6iFIh1uVUE8MARKSIGsve6kwVS0QEPzCd1tgXx+OcHK4fzgQK4vXjqidu89CyXQIMprV2MlC8FdxYLlLOAKPfNNmbRPHxreyds4G6Z3NLLsx2xt8OpmUv3T7QA9cH5kYd4rrXnGBhbkjwKwqQb28WOI56aXocPqvf2+/uOxqtNJNuO+6aHrK6/MC/omGqqtRyVK/Y+DYjL1RlY9hPzT9yvjfI0J3W7w8ey/fbWGJUa49xY7cdLkXt9oRmc+uspBxhzTB4YQoPuYQt3fxPMH4ritf91+bqSEEmCXBsG0KqMsfC0uDvBRwmrAjUJl0bNO2FPyN2qsEjmxp1LI0n+yAAR6UAkdqTilP7JfLAyGDUXEscXDZJrtOxstKPG/ghr+HMsnw+CBPxPuNLmgwjenj58QRBP1brbQpKOPHELqlSV4bXsWHgYgWwL5Kc05Xfvd4r6oX92IOaYCJFxPa0sXeGYzosIEVR4KrZlAonUYBx6Un0xV0y3AR6CKPMGHTlsyQzvL3gr2Y5Zik0KD1iUcY5S2V6SoDoRJme58+ez6vRxM+x7gMe9sxogjCwpgxlzLc0CzTC/9ndzeujQNdW9hHVDTTA6qigZ3CBkXmuKgPhhaFeROQYzCknfSJOWYHTkMHHFVUX/M97coeLLUk7xuxBipFM3xFdFKZ3fFUB3xXe5HuWIw33oSbizqBxVzy2V8EQQ16aSWI1/3P6CDy9mI29VkdFEzc3WAffa6YL5xic9GtNDTrMcFR/mTsiJW6kb52xqPXI6J8W4KCs0vghz9yaXimXVGr4Brk3rw41MPqa588Ly2FaUGdtMMd8+QaQK/FSnapP3OmY6ucxsKNovUN5Zq5mhbRO92G9VjlcGqfWcJ7ygATyPoI4hYH0GTbMfA4Ermv1HFIf3ANl2t8FA16Bpxa9ZgP6zcxh6gzlJjj7nrOL0L7Q3p3uP6rTgHjW9C7N7/5+lkvB2tz7eoaM/gM7t3cZFn7y0EBUbdop/E7HkyIiaLmsxoBWee0UL6nfUriHAR1xI3Pd02yLAeLCYz3/7PMA25pVnI7hFWA1iuXk80St4r2wNERWabmA+MVaJn4BJ+45iRu9d8P10CgJjJPWjKNoesXmBxXRZqZ5iAdgHCcWYlRD/hCEToX+ujfP93lsHV5mG+TwACI7EoVwrxapmOvC0mcTjhS00C0CFLtMr6l2lJ8Ser7Wks0DvJbU2JAF5KhKMPZy8JJfmzIfaeXGUltualx013lqCUFr2PgpdjuSva1vSlxeJFtWXkdBrP1PKQw1CV8a7czaIkE4taGmb1+hxnCfhqJYQixScAsit+YJJVHlYeYi23CCxkJGoM5bNz0txnOUjx8LMe2qhjEEQc4slDaBVL9EjpJBuxuq64Fayc1Rkx8D9mRUnRKUouu1xta6u9+pT0dxZRGXbTQCbijvwJlrRhqxz9fQzm9nI/an/HV2KX0zf7L5A42w7tqMZJ96VhOR1GMIsyFULo4Zt/8OVUrsUZkyWANVd8/BY48cu5MwQU9bY9AbFaa6vrWIvZrRYVHWV3qcDi7886wL1dwcQsw9+jcPXktlAyCfN7PIRPVaNrRAoXHcX7akQUvjIRPLvAY6i8OXxdkKsJW+A+zb/CDGhhlPek0NzJJ9bKTxmySEW9A9VsJIivK5syZD5ydbkrLRJlo9Qg5aaGRPuQi33DudqPTiamy11GncNMetuFe9aM2JPjV/db/L98bIPkUrnsPsUrqEcb5PZqzuyDNkOsmlbfuV7lDgqJn0ZkcKNp3/NR5RixuWHczjMG+bavp6tRYd7C1/7/G1Wcb0uoNfV05EQ7DPV3uNAcySgUaikFVpf8tUdD/6V+oAbqHYmTChAEHMY9zYwFuDY446H/h0BLnUnnYfZ6tRVpWdLnMhGB4Rz6lWZGtnlmuN+tWzb4bgUSkHsOKl0le/cz35oEEBF9HSL4Ybo31VTeSXjfGA2tk/v3w7024amog56iOXj/fYw//L0HFN5KufDhaerr3U/2k05v6Qw7hbafgVxf3qZODT1+bQjg27BMRQeTOdYG5W9aSFJSZezE9qc9qHkXDE87P875K71ngTu+nIkCNQVOYM37qXtMbygQjeYC+cQdwtwkLfw2leh56gXPYHesDIlgyzrEsbMS/5UScKxBNpKPY5d/Li2IlUCuyjNHSCvLyKXN4yX2QeUYCbW0HmEsnHSA40NAsOPXyBF1i5Dv2aqz9y3YgyW1Y3dKjoIydUf8IEgCWbkO0mrNvTnjYVf36/Zt9al7VuTIGgg/RlKTvMJNS8HDuFe6tdXHRWpRW24791lbmQqf6ET6WIvrLOhA0JDi6pWT1m0rutvDI9glt+NdAwKFupVDjjz4tEMkv4x1YcAzVvoOHl7zmk+6tSX/4zAqDAp5wk0itR6Wez6b/ygPpOhrxVth8CTtlA1k+fKuXYg5ExXrSZYW8/vrGOmxU65qvItESadXAolqXX0XU6ir0lOuvghLcVONnhMao/2Mr+WVE3pdHQPWhLUPFCkduaGdSsgDGhMZ+eohuNCjY3n3quroKm0eTkhySEroKBn4xaY0bDK5pFohsXU06s2H84Q3NQ2bcunzAJpPQOATz1XU3/aO97W2e2LhPK8fBFKUCk94+FgerWuDrGXLe8hRpuHgLsLI/gMYZyJ6YfatC8Dd/g5XDq/Y9aycWV8tUbfyH+ZPXrR1LxS2wzyvsuVrwCNdqJjUM/OoiSBY3DyDrV1KqfmNXcvyISAe6QHL1MNYCuO+m0SAYUzbfHLWT2D5Q/0swLCQvCGD3gwVwMdQBxnxwXP2u/sDACD5PUzelw8zMS9W/XcnZez40cVCi3qKMKcTcUHQfUsJKFKGJSmjOKI2gKsrGkQ3jQrXaF6w9VD67t5NB6AFylVpaRjlIOp2tWeoeXCOjQEtH4D0hwi041DWk7yrw7P9e2N8P4z7Dh9pA5tNFQe+Xt/ZMvwMLtOFY3lKmWfYKS5j5izPSEh+GkrEwii/NhCxP8rptSEx9SBwbM01tqWneK2rHPyGDt6Dh3a4ZVWRMRQu2lQrnawtcgFo5Gj+DT2OUnA0eUTf3zjrL3RkeZ9HiMt7am6zN68FYfLc9Xp4bGzCySb9pbJVHRqX6K3OrjLcvHbqXup9L0UY65ElLNrCoC7bZwt98e/mIpiSW0EAp3Sgtt5wdwz/ROTWgj/rEeFJcX0FNLQNkUExzpl68LFkibVfiXbpE7620AEfIPtjClJOKSw32T19blFYkpDDCazEkBq+TsttcmG+MsCCAfqp/ydm3E0E5x4BJf9Qzt5HAWHNo/nYGqy27HGnmaEPdGzS9amrMJJ/4/tjONc3KdR/5kpeTjf9+IA36yD/0CX52DjpQqPyWRheKsI3bVa9L9OYEdmlWzL5ZhQq9OGg88Z2s9JCqNxZupyOMLgIf+DU2jCjS5p3TXE4gRCvfA7UtDSpKZTFcFwsfaVChboDkYEidJhrKIs1zuSd3P5351Dm3LBmE0Jgpd65C0v1OFMVti9Kmy9EudDIDgcDt/7Wj16ctpORVaXuKfg/LiB27aMZJD8x9OoAroSvHjtQJtilFgzsj+Y854qPAnGgpb826wMF+lvYfnmRfLgmbl/JEiH45U5ovgaOW5rUTw3Ovs6ugD2LSbFCpzVQmuqENN0IUcjFTczNDHjGEzLsrNwADDgBFbw7qoSJUFt6UQsnOfkwc+HSO/W8LWGzMZ61bPty4sBYsDMm5UYIIDx1comcA7TUxU1P3OxRLC9RSYzMbsKlPH4iwW6awhNSKqtDcjD8iiRoc9mCRW5wCLakxfp3BR0hNBo3n30+06dNjaq2Vc7EROjaBUiErenkYprUvjm5QsXnKvm1niRbx0mHIM4nvbFi18pAEtb0mJ+vH/+3LpJPD5m5WAha/PZYK6N0R3M9QDgMywPboEK2EKoeCSv9k9KwrTyoHrNR0Ff/xHnTvIYrUO9uLl+cPkG8GhhlSUAD2742i6Nuo/rWTw/CWPFDbsMKlUBiQHI4/wzkRMf7hBYoTOuQyUSfAzeFdkG3rhFo7bxRujFbQW0oWKPvzlpNHKjS/TEpBdGh3SJRMMHNfOxPxDp4ixgyPS1mqmisp4xJ8qGnAD9bnRQ4RTDq4hMVygNX3B85aUhKtzgwtQeooVRy1bTfZe8KXvR17mBxsre1zr03ebwMdUwZH3fiZbvup5WDysEXZRuj/kIM+4Ey7HynxQ9HhaN8Tf731NYhC6qQ/qSPR+phldWRRcxazmtZz9GL30LJZvBS7HuPD3HoI3q1ybkbp3Fdd4MKOiJNBwHXYP3P4zI6xGBMS3LbLOBjXt0WBa9F4oP1xVW41GZtZNtCYp3z1T2aA/cs5nMeGjC45RWtTMLJeewPBnp3WLhs5m/UvVYeAfRrM7SDdApYF8Loldz36bRDFJxxdpxJ4Cvj1PHlnme4KpcmNiFK8HDgtzAIIBctuH9tAuU7Z2M9bJcBcdxLF9GCVmXmVl4L6EKMj59nZOIl5ka12uVvG3GjRRil3aLJ/nfeIaVDAyCLFywlaStAZMrSUutQ5sJ7wfgFMOurb7OsU0vOvNm+tTF8ZCbzWxYh+QdGVfwQ3MTX/+sYMMSXwXOjJbOQZkT3x54/mUKhLYW8Msqsw5zYdu0Ghd3srtXbaCGfto8/LZPGkN0cg+eRhiDxY3kDsRs1H/jnJiyHpWSzLggVa1sr0Chr8d6NVC6MQ49khw3J1Z1kkrYCZuBrCajPEyDwKaSmrT7mwIKe6Ov8npgzNlT0kYyNsHqHj9b2duRy97w34wdDQSNVhwkl8FYydrQlNfGh0by9ti1xwyPrrUiY0N9BmfEGKqRMrrVCY0YRGtuNR3ifp0PJTFJIeJQ5e8VZ80cm0y31UiSEYbEl2dedEWR2byAkaXc3OzH3YEHVXDLwpGasU4cHcitcBvMm5jRYr6CkR3CrmKG/fYrJKmC6TFHSh8baNvT1raWLWiCbM+8XxUmH6Yyzd3f8l1+AsZC0TXobd6hur3eQ8QdmrMmYSWX4YnWzeKtBn2lJv6739YattkMteKrIm2+jNgiPpgweEGWrbFZLqHM30Lggca64w/nkPb4NoF9XgK5wus2fjPOuKozr4aRSbKYIKXFTqrUQPyz/6MZ7DEWBTRybmJWzpyjAxhSUrnw0VA9gy7wpJfl3XvBw4jeAnYteqbmM+cF8q7liUfUm1le8971W3k6IChVUvmIpwuaMIyIxyqoOR9Wll2Cr68SP82SAroqqN44dFiCQNJJudgbIzk2lVxdwv9KQhFwCS/lQNk+BbC4GVZHaWpRGeEGsi7pb88nWnJRPGQ1D7So+RWL4hoIbiyadoqM4DelZK6FXaeO1gNyfyy+vm+/jc6QWPDHuuyc2oj8aDD8UoJ9Dt1MdgFVRql3CJaBUKqCDQZt7o43Xf1lpM85kBbBvD1l1JQx77B+3kSjEHDEars5babZyeGVnBu4azW//k6di/mvDRfcL1S4nwb8x/OpuQ7omdWRVlvo7uWyRTkzEytkIRyQqlVYYjgTW+p9ddFWBHkzByQRZWNwbsO5hNhJlPgqLkQCqORxDDAXMvQO5EqieZtKaLDj9g/JM6CFzzq2el6+X7N7V3pwdubKhXSXySpNliXTMqVT4Jzmjdfilo1baHZze5pE/Il8hBvFx6Yx00NFbavnOLZtkPMkIJ6aJPl782X4F7FMVMInIUG8g4M4b1s9Z813vc0vy3lVWczU2d07MQVbhHrEjuLAvDwlKkt43MoE6Ys86CWgCpkpdV5ohYQS3r0tcMojLy9RALul+P8kiKMdj+nSxR75YlBsv6q4nJy3d9bRhgjQqtTvrY0bqzwtD2poEPBjbrnTP6wxb82XOi/MfPG5h0mktimF8UyozDB0m69vESyK4nV84V5hF41DiQyEKsBYjQ4RMV9ZmIP/JhuW7xfAqoDwlygmDUENfiLD48uc9hUXUjIBHvaPi2uInJIrklIuTJx6NMkkgTyTL4F4hb9E9wogL0vZkG+G8CKOuISIWGh8KpksKrInLcWOxSvJ+jGc6HPiCMqhkPRDG9uYU0ufIr17IcyxHubBtbWG3l4PLxx4UjBtIYruwebd1NiOjvan//JdIrKsdF+Jfpg0NTAm5GLgAGpPliNOmQsTr5I8kAAW/W4XFHZLWNJ/m68W3cuA62nP67GGM4rAYOHQKI1Sb6Az2ccBEvcKAbmFhkBQgLhitnzMO13PBdIBvO1pR2aenBXp0JORxrlZ6qbF8kG4c43N2DvfRiFhC8fo9RoQ3fskH/xOAUMVpUf2VHGHAUcPU3Csx3c8JdM9A8pygKehmImDmyGNA+uJM3YHVqMmDQYRNRRY625pNP5LewChzZcCpvI66E5SSSwZx6tSjdNPmgllvX1jUPy9lQ5DKUFPf78FedEqUA2BR0YZlBlrvYbFgwB5UhK/lVkcBMziiYk07jPUF4bayPlMt5AKNkaMq2JT9xEf9Knhhd0l+f0n20BEvqgtejkmXPX0qFXkWMVbWbJyQJXMa4FUTPIWpZRg8vbDj5o1C+P78t6Il6CZHtijvbT8ILSYqsxLqEe8sYbvHUNCJa5qLqfNVSREAYJ7MWLs1qh/GGI5HDgilyVH3ZMLxHosPN9z9NOQNqx/piID4vRrm2I1i5WFXgHLhSDUjLnhZklvu4C8ldT7+solESUrN+DbkhRf17IB3mXqPeRLfz1in9K7VcEC7aJCVw6PEFg/EMHreOnsF8+NrgsdrBehgprZN2/9eVT0Mrhrrtaw14OH1hhzNuL+2aKt/Pl2LpdOw9Sf/lpm2EB+iZTUxjpg40c6s7mvsoel4R2aitjteW7GRv+Y+9pwdbFeqIzQJhKg9j0CCGubB+C3RHvXRmdJJf6b+m0pyB1666+zGVSORZwbq273S+fz8FyDHuVGg3n6lEgz1Y8VweLpiGUEKkSeIOGYilqdpKfgBJHPLJqHz+WAO6I3UI+0fu7715Vz/PTjMm3lGqvMH1o1oBWfFTNPfW+xeV86lKg5e5+NxypFSRUlysKiJFhFaujaOaDh7WkEf5e9VXnePtsy+CmACWs1BzKlIyzAZoyfedya++XB8a+w6ayfMs2EBhS47gMxmdocUsJFxmY992gFUp/fLIXbE8KfC0AU47ZLNbfc7M68COC6LrlljdtkBxtuGK1+gj+CfNI4NaXYEs5Vw+ZDHTSlGWNlp8tuPhRc6Yi3lj584YV8u7G11xh2MO8LAO0L0yrueGLv120N3U0/0PICsUvF1AK43wF3htwysLl+jyrGjGLOMYuC/w2/1CsH3wFEhsLHa8yttAFHH6FAxCyDC3lAoyf09i2/eYlahaOrRtckqx4nfyKfTgUlZQXZfPuBmHWNXHFa4OcjT068a+pCzmhXYWls+cPDT1SuUxstByvtbkv+fowrSCrzxz0uYoCVTKab3LQcNVpCcYU8olZCec1S9UuJ/KtcuodvSh4I6ryk5A+Zl2xz7ARYxp46B6GzBVw2+1uMS+qWfv0dbgqbVYAXXWovNdCDzdMhbAlLhoNhEOVWQp8l9690q77ITTX9HdJg9xYcmag/xMhhMfHt7NQNStb7tgx8AYDsnka91CEGjrO4pNNVN+rj8B3xXttwoAE0ZuIe2Sgab7M+7KibgDFwzvE+MgIfwLD6zhxHerl+IRshrRtc5BYdUeIOygapalcENnJwGYPrgUCOr86NMCn1KoH+nnlChYUJCV9YbwAQUCs/Ob/jl3RaFok98uhuTmlEHNmCB6/RdKe5FV219hfj3PnFe0zUm0PeerUTCp0xjodQhkGwXaJ7Mo/u169MMfVPl5jyq5GjYQKh9cx5QYvsI1KF6RFswi+1eIMhtduZP8YhLNgsx2Dw5ufVjwfPvfYKCObXoi7XE/rw29kGctfwx0aoEshOGODjU+uqZNxuO4CpWi2sW4o1vFdaIio9zzb3mdke/bYKIznUDAQqWiS++HqiTYrfETfRsYuyJreXfohczpsFWYQYBv1cajh0YjlhgBBwHh+wXHzCLe2N8MB1qcHcxI7RNiVnb6kLCngzacIfFHf0S4/sI2+6KQQvsNuPCIOox61ci65LvwH/KmKHXW7KyBGmtek1L3CUYFX/xAwLb9jM5UdAGU+i3lQzMKBFQ6Av44omL4LQTn+CBc9j96xobSxaGhw7R9vQr0u4F9K8L7YzAUkUzMeWLLQd+wOCHx67YT1Gv3w1jxZP5Uw9CVgsWQn3SC13f/PzoRepLTQlWpazfSj775v3MMu4g/F3cRqf+8sS62UG5ov+0PDFAO70CVmpY8VvcYxj8ljcRV63UAnA86Ghh0qQ8dlP1L2Bc0/du/Tb/4NkQdktRFBMINp8aym8qn+3Qb9vzrcqtUDFRVBATN6gRYAuGlz1sRkzec+G6ikF3enzHeSeTwE+/AxcVnrUuOhPexNYgzS6vCAWUSQ6uynTw3jJSmvw8vrmnxHCFariJ/fzF/i4kdMH0MyWBrDB+DHAcJXZpZjDtNGPHoNOyqfzXakS+m+b9wT/pdbIxdaRPUo5HJf7u4v8PbjSAv8oMqrq1PsTufaQEw2LAim82Em0CX/5qXut+bz/+DAJqZQgYE9K2uyb03o5BDEgnqZcHYYhXPk/4B0I/xQ1TVOuYyynJEjUYREWec57c61CJfdU1oC09ZXtsV+REVPuWhgbzoNapYJwLSt3eIuj3U5pcDLPW3mfZNuV84NlVuZ2Zut0zPZ80dCmpNVIb87B4bQI7rSi9p9rLLc4khHJH8QMWmmqItGxg444Q==" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="CbSn6bAxOL8GyEGHl3R1VaUF45o+rBwVP2DmmCDIm2axcH3Vq4aRQ2NRvNqLjo50cLmoJm2ckoRuhlGzxdBlaquF1/+PaBUc5Bq09r/O2ObSTUpFs+7zb9d/GscemA+xb0/SYrROu1nYv+iIVCykEIKaYjtHZVs1IwtQQZojBjXn9Dzt3H4QV6iR1HlxbJEDwVZQg2r4XYpHjuXnkn+XYiCBjnLO11/FOhCP3mVkRTIY4PRKsQGpBGIu3nx/2a3oMb7yRUkFIBhVpFIWOHey8p8894sLVmIsmbQ9aWJLCTjJeDqT7G9l6UDRiohoGetJg4F1EiuZuqYN2PYDThBv8Q==" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="otherdocuments/CYCLE_SHARING1.pdf">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/eartquakesecure.aspx">Dos and Dont  during earthquake</a></li><li><a href="/otherdocuments/800/BusRoutes_25118.pdf">Bus Routes Operated with Common Mobility Card</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/General_condition_of_contract.aspx">General Conditions of Contract</a></li><li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/externaltenders.aspx">External Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=nclsis35ZWYbHIIlld">Phase lV</a></li><li><a href="/tender/Default.aspx?cid=grsGMsqnclzsclld">Kochi Metro Tenders</a></li><li><a href="/tender/Default.aspx?cid=3AGNZpFcFwwlld">Lucknow Metro Works</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender/Default.aspx?cid=yGEt7G92BVAlld">Noida-Greater noida works</a></li><li><a href="/DMRCstore_department.aspx">Store Department</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/advertiserlist.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/DMRCprocurement.aspx">DMRC Procurement Manual</a></li><li><a href="/tender/Default.aspx?cid=vlVO3VPqpMAlld">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="/otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/otherdocuments/VigilanceMnual.pdf">Vigilance Manual</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>

<style>
.nav li ul.pass-menu li
{
	width: 50% !important;float:left;
}
.nav li ul.pass-menu li a
{
	width: 100% !important;
}
</style>














  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">93825312</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
