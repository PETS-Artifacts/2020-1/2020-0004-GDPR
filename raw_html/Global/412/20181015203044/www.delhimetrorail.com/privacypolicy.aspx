<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="jN4TrS8Kq5EgkduscqjmfUUVdziIILNpDp3loWQUq0bf57nY4Vam1VXk5IFOmvN3wsUrK8BGy/dj0lSJJ59C9H7Mk9CRF2PeAd+z9ePi0RQsbx6eOnPYmBWvEePxmxszb+oGq5r45Y9+9+SasHwXLwN/pKoJl1/y/I1Ov9Ya+J7fBEz4DNCiHQaoubrDuN2cNSD9wD/SoSfMrXL5PPM9wtqOblSG3B1gihZVf+Hq4aZjlwMuCrfOsLHRw1442BuvQ4ggJZUhNi4TPWgLKQbV+sS80PKhLPzuZ77zQIriCSw6Wobu65QXjfZwYrMmLMptMRCUCcZa0dcm9hLaK/+k7qdmFjKIZroFke+i1/8l1SMefq7ttPU6NcuCLkd2BDeW4JtET0Eg1TPr2ti+yOFtqENzyYOJ9DdoqC0zvXYD17J6SgGqrXbSXK3e+cP9t8Gq5JljNkaDfPUYgNP6vTSfM5AXAaYhEGZkSIfKSPtZBFaAfgoRHRsPD5UOlAHOKONGDaK15lwT0x07VnfT8BKOC/IaNOc+6Q4jYaN2VmbmSGBRhthVFN3YF4NP4qeQTK24OFYTdCm2xLmU5mVTPrgtmKUK6ztkf5izgzsyGNcWXHeO91i30HEUMwVAgBgmhbCWxm9Zg22Fg1aXT/B4qRhpE4/EJL1qPFuNp8FAnpy4mW0393etWT3h9+7SSlnwXS/IPUyYWHxFJBYEXblQRVJCiAi4fNgqGBmDdwvRGjEcddLMDSnvCRvrSszOPXvCTaqOC3qvkj0CbrfDXeMWvTH1U/hvcS4DWQWX1FLb5iulVVzq1W8wcPivd5PnkSXKqKFV9zISSeITTz+0ljSW9M+FBF+EqAR3wSxzSkNPEKZ9aLKycjg+1EepP+IECTRbWoxlxQaaEc/JR45DC47hup7mfK/bjjUN2tV4nZVKJu32JQjExqoVH9hj5ax82p5NRF/PYTxMavekCpzfNQkEDURMg8S6rsu1WKKWeWlcrhtROSRl2m4KAGKvhKZcz4ZyfjTBoeQiP5rNDvTcSrbXLZU5bfMTCJcn4Fw1UASluvXLt8CE1il1GSJjmfDCKYEj0M9WrRfxVL72IV1QTV8GNaQx3VKSGi+vkr5rNQ3oHuDMSVgXtvrIl8rq+I3Jf0FZDewF0CEV3VfoncNTIl4FxwwJ3fr/v1d0ZUT5goirSaboTn5r9Y/arrAxPzjJwrCSvLJaJK9Vi9Q9fKYbdfJcIOlBlsRkZPHwbDHuCxe90jfxnY0hzOfp4pl8MFZOrd2ScnqN+sHpOIbHX5OQnBIMG/GcLrjAlNXtfOp84TXKJqIbNrj3wpUNlEfVFCrYbvA8Wwe4DQ61zhKpIzmupXA9d4e5V2xSOHeIpot1upGyrGtBeUuwXSyAn0+WoxZofE8XjdTLfhZMSCRe02ZV4lfkPYZpfUxPShQOh/m4q0Ka0h+Ih7ll2i/I7TT0AIQwGo/sh99C6ZrBECDMSDBtPmnLifIUwQ+Ty+wGBjVKospVGXa3GsPlcB3l1fLXQhRQNWCkyxWRK303P5KrZF/A5nUVg7/4OdUMqbCG+uK6gBrz2O5lBRSYd/wSGtPecPjoTCjkrsbu9gC2wB+51XWwVHnGa3Clg/+pMDc09e9wF+8s9Opn/mb6qM80rz6Wn2hIU8bQ1/QpRUsJ2ipPwNbw24rJF34ZiPWx1j/qa3gQEEWr8m+FRq6KDLKiFqDdeogYBQoBlhMKv4sitY/CHcF6AGWq4ei9W7kPSpL/Pp09AfEigfh9BrZJyTvMeTQm2WfwwyoRqTL7/I7Q7DcovQfxY7wv+DHjG0Q4UPZmkjE6qylXd2KWT2L1S3gYCpVYiiZgMMyNcw4sd3nIg9jKFyXVz0Bh+1HvCvZV5GIowR6b0Ddh5C82l30skFNqmVBoRDNShJZ43MfdRsyVBrKdXej5p7AryDRRZ0Bbu4tGHshjQUH0TfIiwqcQg/GC6FnQQxFE3RDTjukf9SeighaJF+1YWEbHxB6AreP9yjks2iQeae7vDZN3Jlh8IgDmYVbWAVAt2Q9hSN9ACa26/7FAWQggl+q0ewOqkH5O9dnOJ+uxh1tepXSVluWe+68g1i2uaBPwFrkyNSVoRdhTwDcAPvVAklgF+HTB53fI4iRrXDvkmDqCJdLI5hWkhKDoJUDk89mFSFC5MrVkbleIi8vpBvhf0MLS3RQaYU7mNfSGRxuC2BUZcn7eJvZ++pMOLJkpuuOAO6filEJYFKt4xqQwBkKQRE0j2+w1PWRzGnViVYDPLHP9lLOranji44UrLn3OwOW8FrXd1yqJv//R0h8SQZU7Zc5Oic4+TM8oafPoVA0+RgqOPVG+yEcJRURya4YUwlIhePY5mb2N2Or5bxYmO31DsitYUbIB8xN/vyBVrCAP67wtc1d8SVknXTvkRe919vSpZaA0gPJ34QOcYNJS3auVPji6ScZwqXaLM5sT5M5a1uxOTN2fZHpe9WMT8/x1imJ5I91qqHTam01AOLA3RbcwI9HTJG4Uq6w9Mo0FeseuMs1OG4T7DbBZrtkvybfrGUoYQpEW1WWFibaHSrBqe1xLy+foj1BUxRkwUH70KNUasPqyLXF6y9vJJNZCR8h8blRDMIdoMtSFKyf0bPUwyZHBNCoQAqrj3135X5oaTcsPwtHUjLkZsb/f3T1JGzIQanrX1bp/qN6S8FMtvFaXqBwGf59BKNmkDqo67FB2ml4nLXy8n83Q5/MH9xA9i7iIR1WBQMSX05mHrhmN481VhS6MlasUQ1Mx9lLm6WxbSZ6ocDMO07JxvGyFJjVwXTJcWJyBMZX9VMVaAbOHTCIwV3qYvXvcJRjbaFUK3itj4VXdaNHIZ8yb+3nm5qP4+qUIsOBcrwEj5cTEgm9Fq1v/EO9hsXs62DHaQCgHuJj2ob0ue4qOMjgT8GBP7SznV0BWtehnTu0/7gMh7WlpiDX5uis5GUm/QV8WQmGKeKvLLeXzCvHRwQWoP/30q2x4d9bQabj57KXCNqrtjP8Z9YLdGpew8xRVCrjZHKp8wsjk2onAtMV8YN4mdMnwly7UvLgEerl0vujzkVGKgU9DWTpOQGDTJubI4KI73A7Qdy8TDFMvGDfX0V3NqLZwDUUEhHhbVj0r39wwWpgAOSDfzHBgeUjpp8Wob3wd1dPVLvmrN5unOesdke2MRfNCY1AfZtDIXyDALutoBrIELk/u6K/5mH1TOJHJdvnm2Th6c9R/bISs9CPxedGK1XjrX+cSYbDlhWuQDMZ/Q48C9Bip8BhkYP5QT7SfY0/uE+2YfKieA1zzZ1Q/wBT+uCUnN4kYKr5RL6aOGs3vQFvAh8onPEhuT/UBjIxUqGkM/SjnaxO1+3Po3CNftcTMoNHkIor8AlWXewxtv67gkzQqJqr8oy3PMO0R55QKPXLBdamd41qOv2LPCwx3Pdh1S8Ql4P3NOrzergWCANExrXsdLP9d9s1162C6sgqbkZS0R4P9TRKtv+oDac8ddPIGFcLOPFgfxKGqaJE+haZC2H1qNXvZIzCoHJeWdmi0oiACWvkQnd2rZA1wOkjiGJaGCP4eFSGWl+zE65QRKu7ukNBtPUKjYCj9N1pW7QeTlj0Hl/krOcllnat5Ft7WJCB5kHI1Iyi41A1zGaFW8JzQxkpb9uTPTWLnHpRnwSREcHwAWOE6Qki+LvQZK5ZgQSCEIdfnBYGiCs6fZFtKjAO4oeCIcnZ2rvURbwCmgaOstGPQFzM1mGt0cvCiaMP7A2Lkk9FEk8Kxr6RtdGRN6BiZEsOfts4aJK188z3ZNLzM+19VNNybSpR9YWeFx+YQzEasrBPX80NT9j+hzc1XpIiSg2c6qfWa/VwhVj53qezEmxKErJuO95XJ5fQn4DbYa8/RRbprCt66SWbB9SsblMONfqIBc3ym6Ij/OF2h8UOdxtrS1mm7bkNlr/NuR/GtGT4twmXxKZ33vTGUZzFT0/ZQXrv1C8QCCz234HCEl9a0MxRB1VPgRSjBtY6h791KOL6wkOyfSQoZ9JoouON5GZcfvoAiDz6cQzV9N2cF6WhlfMvy9iokdagykCC0BAlSASQ2d2aLh+KZu7bV7pCL9PUAReozYgGnKSABAhsl+1cGjDbnymrjrakBqzsCONxQ3g/KfofGGPcrumhyjWxG9ti9KImCxXBC6EZOfoO2oaGftRrktDKO8tywrUZ5LhNciDFoaJk53Gdr0PvUGuyHNL+yN17rCCJNh26Kjg/l3h9gMAjKAmDw3t0TMHx7/BhMgnTNy9N2z7rsfXB4wfz5zyUte2NNTgFK8ODVwvdCuoFUULsZq2MR9xJzNfxnJ51HC2ZFMN0FRoNmMP4yVIs/4yPBaBuVJPJ+BYOpowpBkiH3HVgg847cWbI+hqnJeZyJDmE47eqWsPoXYQhLX66KiKHqAMgNUWYAWHJ/plRELK3ksRMaomCintRC3+DebLxRZcLgaZOYG4g3btCesz+X5JCwhUwksKuYjz5ht8Vco2g7960A/vGrXAqgKo9Vkfta82J4Uu0879tjX5DyzAkFCXKAyy0UuvZoA+BHBjQNJeYTm27pL7avvMThaY1XkPQ0tYN04n3S/XHNpna4qeTTrc4gCIyFz5/gqVVNVFyyLSzgNmDua5oOlUTuoDCO0GcbhOesIdAmUgZOeATROUA36kaS9UiuhD9ZpC4979xzl+QX3BwPvPiap0wbWfwiWHGEPxWgav7YY6uybybvcTcToIfJgmqOvT/jha6cFtHZmBUjLwXyipMF2GJgvMyEWgu3xxx1OMaKosjyL37OzDPhLEaQlX4HF8ZdJx6Pyi7Jj/wYmLpwSVzrFwtsSj2DGHvUZk4bJkDeCHXiuaM9zGILKyU1UlUKrgrEEBuB317jcPUHlZW1DAayuKWnZaJKwk/Kw+bfudkKfjhUAWJyLu7NN/i9gC6Q4g7/Qv76SI41NbZTN+oVXEm32/nd41ghY1d9C9gHfB7v8UdIL6FInu/vmzEH6nTCKJ5GOnlNP05vrf3cR6Km9Y52FgPgPhJQAs0JjzcQkXWKFwBGrdyKio00sEresy7+f4F2Cnv+ne1L0G0q93KvEpucHhAPkasMPEzp2wnX7i9MYz7nkA/NX3fJvyCi5Hloy0Jom0yFi5oFViZ+KQpBlZb4HkBXkaSHwsNfMVZsRGyzhQ3R5leIKNeY/zFqroIxaCkrqkaQOiomEAFwjY2aqi6PDYLD+XRqsEUc65YasVHsAur0XcBPRT5dwfBDFXI/tSkyd2jxk9O8N3Y46WZIoz30ufXExdt+c1DvqtemSYoNndK8cDLJFwXaUKhn8OJ5ANQKtBqXN+ga2X8grAtAiZdHTYOf1DXwAbiiSJd1eDa1OfkAJeamZKwZoutiftmZ4H9E43LZTuGOkBqQT2gxGm/ZX9fOejEe8fVXOprDOxwXRKyDT2sMs7TSbzfCFzOIV1wP2VdcU9KV3ssCHBbIVdy4zEL+OCZcitUqQSPohdZa2CTugsYE5YC+Ry2yyMPfgjHVxGol6/QCFChX9AMWS2KUNidiJX5aMQ9QHmIU/hA1Ek3U+jO8Y/g2JeQQs8opG6xw1FNG2DplGzs8dwKKMOaFL1x8bTP50QG7+VjKOzLRhgSxD9SkzC9Yv1k6Lq/j5bjMn+goW/HNNhEyXe/sFN4CNWHd/DCKUjeOYpE+u7jvqdSUYkIliF9U72BXxVOSAK2y+uExXPECNThwXPi+rmEdJzl9NjQo2rlnef4aDgByPB6Pr0qCNnQPnbtoD5URPyBACNsXW7Jk11t401/M77+BaDLVePAL4wRAyBFACGjEeFWEy1UZdYZvdPbRMnIan6lzm23wZHh880KkweBm+ANxNorouj2yMpPOQiEkKt/tUCsFuWNhoPZ1am/xWQ4ICDtQ5qyKJ5aIw4t+ok/Da5ZK8Nk/t7TJd8wwKmGZFRcqMnsp6+IWpTdcHW9JTjpvd+Y6DaxWfq8OsbJDtoOvLdO4CHTmntqQYuZFoTI2CpvMtZEb+7X3QRAFIG4QAfvfuK4PkpoO1y9/VQt7zQW6xbbM8rKrwJxlV+k4WwPy8N0lKfXBfVpoaYKNuFO4n6FRaOBc+rpjrj2S3ESAb095jebppgQmjRJ2tuZDtBXYnygoHaIodaCPQL28vIpyQvnmdF+19Ryo5pu0hdpTrFPqSQE0QVnBs+94z8yYpTrGxE9j/4WmmcegC+ipVP5EIwm45yBeTsuT5i074iQfzWlNNsciZNpqDJfW01k15DYLbXbV0orqC0F0aNOy7IuRjgC2DYjLCIkqNbEiBG6jBu6mcZuIB5ncMVTykC7HeLAXJ9Q7VrwkxOZjUXpZwdaWirp0EP/EZoYVzxaGzxPsmIftn8Q8G1N5BTTAnia/OVCI4jQ1c2ddaYkhkfc1OkqVPnpyCptNs/fVmVllif2gvpMaWhP3DdOCLMLvABClnkSVoNi6m2pgG+Ti5pfT9jJGkIs4T0n+dKIpy9r2VpzFkCQOxsdp0eLvyPblh50mvl08AApfG8aXsWlKFb3reVisUpV2tgLLpuy990r3+EVZapxWvTnoLB3dGpteY1K6MkB6GcwynJK0Pi9d165BaNZNc6z8m7YQYKkNAyHAjUneWdQ/v39SOjVuUkOnSMRcyue7UwmEhOrG5MtM+Dd5P1zBfwXMbIn9AhmA4Kqu0iXBJdQetaTZtYa3+iAZSzjS5PURaZ99ci52mVTq1l/TkaRf4VGlHWQN8UDfMhw0uj2ntubmDgpSWFkqg8HxHErTsvzauGzYoDe6P/HThVbsUvb0hoZP9CEKT6CPAEep2wuhMM1kudytQLWFEftzJOyNFwPs/uFnAdRIGN7grTTJ409LuG+iOUSZHOTEJamHPA2pgOaWttFXiHyDm+UJA5WzX7CH4lvKoj7bz4g0YwG+1ttCWpduL+I8vV1SijGSJYyHhlSZ2oIolDcMgrHVmQaYX0yJ5jlfcnqld4E9CN+Q/CQ2NDubSXG1jpMzOpHhueOeiss8yBZy8tMf98/mdGcCGxxN9m9PHj1dXZR1Jlm7D5G+jtwawChdVpZOmcgNcS9Lec7NMDVVuyN7GPaRd2RKF3he+D4iCeOu6L4Cqhq2aWQ/vvQWeG+b2Pc8/iKeNkvv4OMMw3+rKI0ExhSC3zGcgg2GR1NOdFjdxhczzEXuuwNdlyJ/Q+UOH2MzQ5keEdxCdRYEXvRCJF9qyQQkXJb7CUi7hiEA8sdOBfc3EL7wcb4LKVEAxyguLwxdT+cH8oN1TY3+NOF6qGkYMiGluFzgviY3Q+SxczN5pADFCUw3iOZGYjXix/5/3vdvgXRzW+zHe9jPL0m6sJ2QADnlCTiO1PB008fKIzJCUJiwonPeOpjFT+slvuzq/T0D5a8r89xIj3JONAXUd15SoX/YQpLl3hvuK8l/bIYqvMH47si6/iRyesUxEnxm3SWuiLgvcVcix2eo+xzClFRXnDqqhdBS+xo8WkSuZ7ttl1nvve+Abi65w4XNqfO9vPgIBnhNBISLr9BT8qpZfjkClWnIdMpG+ftLnW3k032ZlTjAz+3ev4fCAd9Gat8firSycOaMPTgdQlcmvnHxow0dWZI0H3D0zL12MNq45PUnuybIfahpaFCKJovMDL5bEVPA5CSf71wi41QHlpPQnZYqN2QRaLzuH9z8Q8oliwaN1A8bx86+Q2204Hv1Gol2vT0tkhuqwTVIgryklCPVR5SAhvaXopqh/324TOshg3fQUEkXiYo73D0/bbrimBK/Qomtni39j57rGuy0qqJ2XZYrFGyMCYY27H1dW1wCuCFmUs3VIKImJ1wddDWbE0IzCz7sJj2xXtZJzv3kQTGq34DD2WI8wsbLYc1POR3Y8pMWYOn+kJL3Vx++qTZZiyzqiOUevG/hR7Z3dDJ6vdutzj7YWgILwJHYQc2W3hgiG68puKBJ2w0USePo6kXJQg9pGXNK7qvK5+2d7vvixMW5M8d0/8xAG+CPPDomnKL+zPwE3FfM8/x9X+hRe0LK3+Flz+m1+V6ogVdl6sj0cfomkzlA2zZoO1Iq64ghTSxSnrrKq/UgRg/xsOxTnF3CyXpd9TMWySndUR58iPy/Fn6BvDM1yVhcjYx7zZVSOgfC6grE0eWaGJukdcbAXGQ6fFIrmzLFtaLu8fn7auhXTO67HYqretH2iqLzVK6JIVogVEyawAdN72CEBmelnu/H2+KARwQjMaFlmM7V0Bi2l1ueVoKm7ZNNs3MiM8HwcIuYsPsDgwsz1gzDHzQ8/8kQ94egK4w/vIpzsaAIZ1oSApQrHWGUbmKIM0Tm4HMTk8bZwhh/sEc7gejuH4l7W2UsaMo83s0/Rfm+DFzVDxbxWpGmRDjtA0kDl9PMi9+XjRik7kqvstC0ZQSBFq5O22BNRqugC0qCBsgQJJ0ilwNjDS2mWjCjxrGFjUzymEIA1S9H+nfwgLAyb98eV9zTNsykakwy79pYluonHCrGcjU2tWNXuQ5Vs8BzPnsxktZEh1vira8obk5skz1PCz64H/eL9z+BiaDRISAaP2+6WN4huY3hR2vz/hZcHjIvcskT6B0slUIDpz6nwUojULm6JyWqx1k4PA5W+RupaFyfg7mNuGTt0wrwaBb/lEmMxO5lPsnSFTt+Tr3trrxqJlq/8WjBng5XPNEGHJ+EnwtuLPAJMQXgmNXHhPOfqhV2vTrkpkUUn0VZxaPabrouO9LhWD2cPaK2tXjgNlw+j8fe4nj3/13oE9BuzzLOGR9PdVPSdHcuwwNWkK1LSljUuBDPjJULKVHLhINPzai7Rn2ZAR2TXum4PM0lq6T8YsO1tb3+bqdATnIegCa+quIQHrrvsJ6Kh39iwkr129/QokeRx55foQDozxJfIv5aLvo/sKAgj1iaDg0BwldbvHiIlr5pcqCuH1EdH5WCgVjTdY7X06fmCwn9L/PSMexvzDg0clYS1F+GDA1LSHjd8nemPmlR3uyi28rvzXh7k41Z9jr5X81KG+RIGCtWYqYjhf4/yiFnBJVT26MQw1AepwZziXAADgRKtZqD1jFpe3EfMJhdoIu/1Qk6XqdOeP/iqihGIg6vGbynx6EEhcXp2dKk9H+DxHXPvmmG9ga0Y/cFsFSNA2H7aOlBuj7qR3ACpUJKC+U1TUkY7Eco93ux5pxQ4TksPlQMXf6hFpYnAaGRx42LVX3t1r+y29Sv7tzAdiicFwLx5H7SSL6xVO2acVkMQKfQlpsasrkze1/Bq4wDaBnvO3dCda3JKRJ2CMkfdgGYQbw39oLDslLufdTdd17i8Zjm4aK6gz2u1fDBeqkTF1k5tBX4+6FkK0fUwCUutk2SSVFffSphJ23haFW8Z4nIQ0zUCRqrv7OGSYX1M/WDqbO97M96fu2d0jeiL6J4h8GuQUOlAb4xJK3yLDhBJdoOUKcQwkvK1Jbkm3Jec6B/Q4JJ3Jy+inyvsaN432spPAUCv226D5i2SdD+e6aVrKMKmFQ96cKNi4+jJnFxjVRH6cuvcAIpjxcprdM0MKLqRRFntyZFGVooHPVTjBQZbixYg01OHCF6UgcHGkWmnXAFAMfMRl7+pfRYGHkeUZq0w2uQdduZ54eAe50l+BU9g2zxKjeNwqx3VXQVJTltqwsxAWRx9PwQeOabV0rh7psph18D8iXMkkELfuhx7W/i9kHbehNPjHyvVMNazl7/c1bTPPlkBTvf2NADhafh+muQywiRFr5COWpcAPfQK9ALy5AsY7TP/iI8xsd76LAyDpz7gK7ntsd+xngCgyGDaokvVNQ7YnnsxD8wHjG6uT5bCxFUpoSz6KK0" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="RALuC6ebPhKfFjqr4zSzqObTN/OTA4xxrgcD65hX035uOdNYgmegOBEDTdqIoj+DV9o8QXV6nx1Wg+uAtSylsbXCBy8wiQmNrJzKkWi9asLqFnXdIxSNpzdOwO52feV1Ov3GuNNOf1kU99BrF55ltwswnO8KTooqDwG3TXiAK3czFYB5wAe55gBWgVswriRZs09LaBt93DERdHIslejhG911obw7TLSxVlfrUBJiJjl/K1defmDLDSAB7eVBcOQOn9mk8VbFua3XXoUxkzOyNjxQMEzobkNXWCjTPvj0OazInbMZSyuJOVCV/jvLYbtw" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul style="width: 520px!important;" class="pass-menu">
		
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking & Bicycle Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/MiscellaneousDefault.aspx">Miscellaneous</a></li><li><a href="/otherdocuments/900/DMRCDirectoryforwebsiteandapp_8518.pdf">Metro Stations Contact Numbers</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/generalcondition.aspx">GCC & Other Information</a></li><li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=nclsis35ZWYbHIIlld">Phase lV</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/DMRCstore_department.aspx">Store Department</a></li><li><a href="/DelhiOutsideTender.aspx">Tender for Projects Outside Delhi</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/vigilancegallery.aspx">Vigilance awareness Week 2017</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>

<style>
.nav li ul.pass-menu li
{
	width: 50% !important;float:left;
}
.nav li ul.pass-menu li a
{
	width: 100% !important;
}
</style>














  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/11/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">111623175</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
