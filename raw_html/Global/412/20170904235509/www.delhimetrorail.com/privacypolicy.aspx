<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="yHSz4aXWcEbWufuzfENyOzwlpR3EV5ZkT/C9h/KanCPfo4R/Dkg0gIJARcbBlB0RERsB+Rsf0WRpFsMZ0/ZDHAbjDyL2NgTzcoEmSNZQxaZsMQMu6fzR/WXVtd4sB9tyQmro8id0/N1J7DvjMZvou+z/AgABSFrGhgt7ypz0PMGJdSYh1zlyvKUBZU6bK9anns1r6jHConFXQuvkw3RYc9sfXc5sEPk87OozD6lTxk01/SYC+TQ+4yiCZx/cFCAryVT1r81KC7m2k+qbXwaTPYqzhI2bz5bVcRhKAEcf4XK+7rCccnlsHYi2Q/l3FDzoa76wbhayLGEHPs/FEr5TfqjNT1x48s3nSvrD4CMNuaYkam9uDMlOpMBfn8JhayLIbJBI8N4NYD6cA94/uWCaWbrhacIYPcmSFRF9zgUUR7GkHMJiMtpLEuc5+H8gKTCk2q0yYyb6qcqgLaZtzTGm5wXKz3/wl3Uw9PlLWCJJIip7kfaMUy1jYzJ4qh+72XHJBvZ4acdY2Q3g2NR5WIEC4k7AozxgdUrQxdRAsTVrlnbwEGIOGq/MHIBqJZqB7FVSnYvXXxd/ON4anPeLJRs1xHgmuoxFQF6CFY7XHjhwoT+q5g6QWO3tGSATWYfFnjd8XCbo2p1zLY6+XqFPveHKyavg9XaCruKiNY6WnjwRui08FV3QupYwCKnoXzAU/B8JqQx5vDy37+PqsHVUgz6bRjY+5SzhpPtXUl6N00ZYW4Jc4BM8fZZg3Tpv3uCnzbtQIrlL6Rn/FWpbaxuma/qYlsLLvLEpimAm5vNatfE7WPbozUcUVO9aqwtGasb++SrQfSVBS/Abmv8kZABuxPtWU8L57YsNk1Ncw3Z1SfxYdbmhyDZIenKFE6+f3CmefsiMQ6dPFmy6Um2V3OgJt7SSY/ZA0o1mG50cEzDuuCn1E6YKLz5SFoXCxJCAr271mq9v/B4PUOXNDn/YtN1zx+hIPrvQW0TC5uRygBscRY5H+xQcEdGOBRHe2t4Zk7FSjKhnXhFdWh9xdFuo7n/ynoJFupwrpojPB90aNKN2bz0xL9vlL3XmjiTqI410S1eci4ywGYATeiyMWdVnfOMqnmrSFuNQHRujpS/isycxtZ1E3CmRDjrLouWu4Fx+0Zd0FyhmFAlk3lT/xDoflfzm0pIwca9SAjjXbbcvVe4J+F+BF9wScyLzuxIoFYkUSYUiDZr5IIn6mIkebDThrzRoRhY1rVMTKGs0Igi6VkXzSkTNfmq7sBUgDKUzZg3Aj4Tz/mYsfYC9WZ1AOa2QR/qozo2TQtLIs0TaY9/4RTMFACcasGt2k9NJYE3bdKllmKmWlgA1SjRSByEh8hfWTuW9qDjkNDOzowxYuHuzb3UwkpPByM6SWj/zVS2LmNYbwI4NGaKi2+HKtB2XtJyoTX/1SqZ3iQ5vadevcIT6PoG/o8x57nTcPXt3dKeQGj0DIsxgjRIFUXH9+CQDr2YVsxxBBV7+bDGr/kaTSXw7D29TbQY83+fmi78/zEKRDxDyj7DWN/qgXgFE5DQaCNfLe9r7kzvW4kR6E56istC+D+JWziDX6Wyg2kI7cvcEdPCyDdD779Qgwbx8aP+KhsIr6ZtauHXlC5JWfrq9gBi6ZtrAXF5/qmudsgMETA4U0VP/zNw4Sm6utWRPGQpGdTG5tPc8EitqdoVaZHinEZyon/UofOIBPbzpgW+yM77yVfmgcD8pkfUl/XsXgm8n46h4i6AW7xyo2ZvcbpfrQMzsZUqLjDoY7mdX/SksDDHobmHMwBC3VgNsl+za9x03M8InDePgnKkErO8C+sjRtC+gXqG/Kbs8gJj78DlM30uk4Mq7YI+XeOinGI2p0gaVAumYigQizHN/XRRl+OVHlCRz1okhlQm++2I5dIW3KhE9/2su2zR4HcBdXymS//9xm1rmriqPkDCXTwtGmMdxVNBCCn80fXHVoWsva8N4G1gGo/+QilgvmMkeSIQ8eKPY17S/ORzQvHiQbFVC/xbYhjiFnjw5HQ1/176i6dTe/gKdDPfrzhRoPyOIDuj1NgIU0lnKAsTDIpow1dF93QSWqEwnIy68xCnm2TqFoT4/jJg9OvnTEvSfIhQtjX3q4F4u4YT2BNi82mGw7RNJQhHOC3Gy+ULuVNp9REDGNsaspJiQBSg8Rf+kvtLclL2uAn1tvSrX1Yt+PH9xNdtOIOxunqa1fvpiH2uAxK9B7Xsb8IlD4qFz6OZTLvy/Vxm2bDdJkvedJWDSAIgWUczBeOqOOe6B4BN0K4HxbnewJHAnzu55iE9vzPqZ9QAMfFx8lXM780ypE8oXLfOGHSJAynmwdPjiK5A/lmRPU9oXMkdSZKKe8/23Ev9TMnPPTLmDuNc9qofP0bis2Eiy0EstujFE1C53Xg/ucMwOEsypel7TtgBibD4U2RA5VMF4yhpFgTWosSb1YAKWr9vLhH2PCg1M8tFVeQtTD7X7ZO+0GBw74YOripXNdSWQfaBpIG3AqgfNLXgef+tzjfw93y1030sfA8GW13oImSr6lGgLbwESn9B5K8VL+U8WuMZrVhMunYz0TOkZboC8FRab0wuY4wXU7wbcxcXnRJRhnZbvaDPs75K/UUrADkVOvJIqawUJp0EFrCBmEN+LwLECOERP8gREgq/oHOhaTfdm/9gFGk7vL8nUrZBt3PeZfU1uXSvyi7Vm3wwYszz62/inr7tIjKH6WsuhaLtP98bK9i4NdDfgil/xp1U6fshtMKgzLG+Sq+qnp96x1tENyoXshmluUsphkWBl18R0ASRkvQ9c9ZJRSqu2flgzjPU1jchognMyevgqgguB1b24u8wwKYZcffWkpR5IsGlfYlEzcFxDuZKLJ+dyJ6XuV0zmR+gqpbXC8POq9L/SoX6Msq2i/9vfQ1dXsU6VbQoOT3BXf5gzUAQ59WfMLCercFqiGvzCgDgoZoU3NE/HQcYk1C/odJF5spnDLsblzFfJi9FH+93iAmW8LTOBqqZCe1M0dp+KCaDOCYPNi30X/WrGeSovUsKytsTFwvccuvztZVJcb3Vq+djp/8WPtmkTDnaUzAVKJsClhtYo20HIpzazVw3CgZwiManglzab08gtLMnPle3Y2Bt7nYp60sWCv1JYtUM2WalLTAOROAqpWWhejR3OW6GWEyYdH+c5R4o33TjdFeQBdiXdQYySkyAfdUm34xDRd5lpGOepAM2mp0zX2Tssr9pSR1j24vmQkJj9O7izi+VGjRl0MQOhyEPSSruu1O4Q5vps5YIU1ZHYUw4zN3WxAttpu2BkkYBP28t5h3532IdEy3tIsoYk7C7H+Ji3CrPYydhkCLh4jGq+qUWF8G69APN9Gsvaoy3PewTohRAaVTZdeJQP16jrI1D0sztHmKDkzUmu+2ws2M0u2bhkrrwXbxrnvioSmpyFQ9N83Gy4XyIRm4TYofnIktoMtVdKriNtMg8tg2J7k8UQcNpJ4jF26Kl729/BAO7bbY//8y2vRnAuoPxn9Vkuhzxa+5gEZvDuNjytcCD3OoeWvVQ/6FcD1Hkr6KrO0+6RL7yjoK4+7hBmNagaOV9OpbVbN+IC/ySQGufH7oorsItP5+V8v4lG4c9hoJyOrQjA1gx1elL0L00ESNjBkigVGo56F9WJ9AE45B//NBPP85pXXToVQDtCp4fOL0RzU4Ml72VsIuaB9AWXByXo0E/fbMgZljjb6Xww3Io6gtiIgGN9s/r2pms7ilWA4U8HaiVVf9w/t06kRbngiyf6A4E5msLlHcCnxvVIzQgQ1I06ueplOqGTSnQRM81WHbUjyVQoPlyIIK0Gyjr0kJv3p4vAOYgfJdFihQoMG814/GtZXUBTztYijM35CcNqdBstPn9A98444yVMC6ZW6S09pH24jH//ujxczKcGlcjQH3mMIdayXsYxrg4JguHZqMVqozufHT3T0SZzkERlvM7ctBqiIHItGE9rZ5GA/xiwM/fKQQu3YEhoYhU7wWjigZx6N48utmzJnulNGJ20OBsjP42v7TPMy/35HXg88Anht+KEMXNHmyoX9og+Ejf8AAbMXEtA7Mx3mEa1yFVZUooDffBRyyDYygqKKRSQ+FWAWEuBfVdC+AqqZ+qymmS3KFa8PUQM2Y3VDlyZ6DRJm8JB+NkttfGQxbZXjNEQGGvwOMukiN4fShCZA1eRQC2GDzRex0y4Dh5t0Pgw9hLAJ0nilLsNRMjMLJZfTAVIoGgxfiNefXb0GnwsdXnrhxsqGuazwBCUBjwnrv82MJTm0LcD/wHIw7L2vHmsMJfiQ8PuskqIapuqttW3Sj1udD4DCHe+wXKrXvWt5a33Z0jVEZeymcdoUJiJhmCeYaK2k6qJEITUjw6zhF6Lpuw3S7fN2ukT/hYxq7dQuPSFrt1yb8NWCeDtnB+6q4XgPCSBvcAeGtgtsnHe9Hrg/U5oDVy5JwPnphxEe8eKqDlYgckYQsEVPzki/hw0zijbwfYvheK7OVrrozsSymFkjgB8/a5+DOMY7VtumxuxS1RaA3WDtQNvG8g9kgYG0tMVY/LdnLzQtQB4CG9p7X1qPTiWMTWLT68z2C+f7Xf8IxzSjuqFDmzGdTpoPyhsDzIJgzylJUWLxK9yUX2RuEsTAgvPLD41FAbSQPPCax3osC2gyiuCOaY5Xb6OLyNsvnzbrKDfcVJNkscgFwupVzl280fFWZ1QEbtNw4Pv/r991+B/ucs9vNttiZ2730+6Yzd8v6D4Q8bi1FJylEYAbsLOHELpKq+KEuFu0Dd+dteilwFPZEUQb2OQ7Yy/K51UwxF1FG0r+0p4BAzgeVYIVzd+rRzRBhkuJ6Uk5tr3rX5OftFNZaFyWxE1Le/jYX1NP36491d4Jjpc/1BEuhjQkKEZTvNM01kgHStBcap9134eTUJhHAGhybTalqOTg2lvRDeaNqn6zMGucjohqBgy5xJnmnJt3jnQzVIkWQtAoDSCQ6uGO3qtD0gq6EpSvglyvxWEwJas6Dmqe6Cfdt6iCcJS946U4+lHzxRfFgm7t1BLE/j00twFTymMgid6E07GKLswajHOKMaWpJgg37Heo3cqKjT5ixAuzyP0pWwcsnCmqpD5A72EvgGYEqgGr1FSNG/f9C0ScK3hPBinbIy7koIUYPQXHd6id9vO2L64tGHuugKzj7tqoDPVzcAjLNIVPSioCQbVwxnKo3nwyCGVp5wGdo7MfVWNaJ5nb5gXa83IIxQQyRKUyKU4AdNcVcBYwl0e485BVbCwNerdiyTwLEVvy8hRjrpAx9xkvlORdPGgRELMfGCTwspQmvpgR5R9lmezXctLXrScU673bZHcRbyBOq6QjBhO9yMb0pkUcPYVFk81bWpyIv29e+2jJ99fIsnHfIXaxegXtskHGYPwNO0qFHebWGwTdqivcYnPZk6Uvs4GhAm9GSBE5dA027nXpUACdC0Fr/oosGvUQ6omYSvIDYp34ZluSWb88ZE4ilNbbKKfmGBFhAdpEo1fZy/W4PhJ7pKrgT6BqeUT5JsAQ4q1EcueH0q7sM+NgS0ZCxH0eHG/J0tT5xYWXpDeizdEB4XQ98bmAc7HEuyUH8W3pCtvoXC0teWA7U7xQm2Z7pn6tk+33NR0yhRKCR3Lxc6e9pOrSJP5IAqPO/mVhlggMVfECtG3osknBqYKg+cekcl6M0iQHysx19kejSgzxgy/v8ExBKUerZhQTOwphcMiZEuKrem0Hpv4NamiI4WvADZyHrhow07hVQ7XS9KYGRwKdDaNdes62G7yDSsjofqWlry09YK3pgDOpDBWSIL2+ai86bXd8tzBXkmWhEJVrPuC2sfL8+en2eBOqyXFF1eC/g909AMC7WPiJ/bAD4c/3RFIsWJaaweZMlSCo31GRVRvMtz/uE3Z0QzF90Ib5H9lIegmusTeYnzH2vhBNxQUurZbBEpqOKlE7LAzB7ARAXymri/LBXOFkTYumCuVnB8LsMEZ7XHuZp6dCS6bLHyniajHlHFiQ3freyI89kNEcgKf7m6RLhX+As29pRZllGSkEnrHaP/YEiJqLivs4aPaq7oVT3vxiMvvHU147UVC8U9/Aoq1zPqDR3k5rUUxIvqh531YbbwWNI9KqpYAI2Ve6g1WhB9q4YAKw7a4Lq/qbSCC0uY17IRaW2IVP00izpcteKl2pm9h1AuSMPEub0+fMjsp/ePNqd/vzV5AM6yUEAn6WX+/QVX/0h90cU9/15PsxFIQzUc7eiGyoiJzynymgtR2PsbPaLDTCASlORNpIYo4qbmumc2Mrom7qtdiKsDA/iQkixE9vNVT8oOTput8+dz4eEFrKEq2+5+HDufvwF431Y4e1rlMEnxSo62vBg+e7xECI8mmdRLp6rP6G4kYI4cur7PpjGJPwlIafsUx1wnHiGRzmOu7c6PSmSMQm9FI9/efh2YKj193sUyc1BhAh+BKRXhDqTvIDJcAymvG0qQEU+Idr4sMWs8NpPWXjDkM6JFsI6bGwTv7ZO3L8JTGCbwN5WkNwZ3rdSE8LdCuTt6hxOURljbxb9KTqAUtHoglEFm9CxKQ45keSbL4ygP7JaKh/dO1loOybdtbWCA+t1VAc+ONm0w3gB9zpJYnBlZHWUa84CMdVX1kCe+AHj+Vv7kcH1pima17PPxTZwWk8aKQJ1EvE9eFsJB+S5Pi0PVSHYJiwyhUqXmQ8bg8riaLfKTpGzxIu3C3lcCI5W9bmUsWjIqUvq53XLlUMYdJ/Gz5ZvdKoIHMvZOByTWwE+Ih5sHKHkBWTRFdZaet9LanxqJk/bCIp8SyYjuD20UN+4ok9RhL2wwMY+RFYBkEQpcx4KMkGSOmdKE8xS1zfU38+sYHKREm4Iu1IsvIWLGbTw2bD2Uh5VGJDwy9v5WFnpBIOzqydCxYlv3mikESdTm7+jAoO0iFnMY8yIxgOspG73aQ1/50AAsFSRJRodepgWRXFjzLwwr0b6oTnsBBqzhpDX5G6zKu16yhV5jVl/i05tuARRWl+SGPe2kE6MTcfSMdWeQwJMgBRJRxYWAIPnLVc9qGJsK0UpH+7ZIHdggfp1BQpWh4euL460Rsm5gddKsecarSxw0qC/dUe31S0AYban90DklVP+lnf1tnoKlDaU1dKqpk9U4g2YZauLN8W3wI+jB/hUs6dnanz9TSPcK3R3CCwXPRLRvR7r5VNTFBBkFiS2PDXQh80N9eSJPgSJyECBDVBFYH/SQWHxNuLn47EMFiphEP8b69b/WJPguVfY6llIdLvyX3MMWy/8NFnfGXADU/MHug+oa4lF/Fcg7ms+6M+8NAH1w7qu9+8ivmGA2KKPYPNWqFR4AmblZZWNeF8xyYyFiboPzN5ZEAzcyHSiT+gTB2HF0MhdoECIxCHK/AynaecoyL0n8dZFYUDgpTeaBMIFouopBEmxGOQ321CKrF7ntIHlSoCakm60FmHkhdNnAxKsEYsjE0kAfpt+3UjAEjMNsPCJGNVyIByn6yxL8SYG4BAL2aBEQJ5CJStMt2Io+G1iKbcLLgwoCGD8RN4wwThrZCpmY5XLxM5vGsbEeoCQMv+kOVbocW0NLFc0MtDtOvfZr1xh32BDeGKn2fiUrTlXYZUm9O0dd+dj3/GhejdPe5DsRh5hyf0k+FXRzFWVJn6grNuMdT3PogJMowE9HfpXWy/uh4GYglRnqlyCG8Vov1tsKVgMvc7CdGCI+BfJT50RVp0vbSNyWcwnxCg7JrUsuDUGUSuTIEnShS5nD6WUCpL/dQ20ps0yBEutdIqVvB+O7W6uVaysovOKnl0YTyS+8CGjzQ/VE4ziU4W/cw0cKNs6KWevPnfFBKwMaNAL2nYBC3YEVc45jtBTobIGvzPUGLoXh1FwzKB/O9JRdJnOfV2ser1yqik8kFOOVPB/tGaz1z88ZwV9VY1z/4rmVRgOtyY4VrK+EI6CqP9nktoPJCHkzd9yp7Zz0gHH5vCszHvvzfwjRRjBAwNjgnx12Ibno/59sjAbRtKTx3s8LtHZea9uQWH1S0qHPhQUjgNDydv6iJ77DF95OYMFKaDJTNSxux95ZfQW8UwJvQCjJrFXORbtaPV0Uu4wZwbNC/XSE2oE/cGRXFeT28U8AjKqsrGCtJaMBkWHkjjCME9MUGZjqnCqAGJi7ERhSr9hnXsnSfRW619WzRSRjObI5L3/EuSdwsvUp6OUd+iOv8Gx9LnmGjeBWAD1fp3SG/H9cqsUKcF+pxQW4XSf+2r6ibRQ9l403IGuvLTZxHLgpeUtcQU5RfghFFc0/6KcNUao/271VkLa53jIJzYiCxsPCeUT1xFSYvDC/NP2QPk7eKs6wVzZAdkoKAs9RuurvzxzXcInxazjW+RtkyWP7/Yc9rZAHofYciXhgCcOiNtKc7yreL3TYzZdIQmfEEsc9MrlUWTHqk7TJeh4PX/2bFHErYf/ys86uU6J6yJhK0mN+z5p114fJ4NwqpG98AUDg4Nvhd05h9l43IMFIm1AasCdviFYvVMYsvBcgDxqfKVZC+xsr9qRwqWe9D9/EDDTI2Luw6R9IYpIBhHI7kH0nzcxjsiWPSmp3f5Twvxi4z+nr8BCr/WRk/mcM1lhKOV6wQa4X1oBIXc9A5NUsMqtMd7iylABYWwWb7qaEVvCHTJwTfLfuwjfI4SDIo6+5v1ezcUWBZa2HvsSEAn6NeFmaK83uwyz85jIO3JcknuCF8WTAyvbrY3MC61l/UZv8WiNzyYic1WlhgHATvaGPg0hoOeJR5u6n1rSOWn7qBw0GA8kjSfsP5kPDpi0H+laSvMCM9llq/otg695HedhW+P8/KOikV7n5peRsd85EYAlTxr61KneJzBrKLano/qrpCY2YDICola8BLUdJNaUZKQKISyergcT6nAFg+RQznVjLlAzFWXnOKVPcF+qUv4/qLQbW9lwJBLh7w5HWEIIanTJ0X+SMPVmba+kbtFj1BtJlgaxKmpnrB5kIpgKflYHS0A7Uu8np5FJcYjcFG4Fu1uoYoUxQk/REgdojkFVZMVAdn7x6XRd/nSbMa2WRluURyIyIb7tZPltE8c0M1j+8eIrt5ii9enIJEdvCQopRwCQs0+5IU7uhvD8XOIUIy5KlLEz5cg4Y8iPPAb8iI3m1A+SwFdzdeYDttknYRtTvFeMahx60rHxCU37WCZMPvkXBEWukbqXyVXdWvGL57Y4PvV3RE9TU5iF6UaOmiygVgZxc71AjCR55Mn4+fEytc0JtxQTGJB4IWk9gwAq/wuRzjG+lzgZdR498x4nUuo3q7U+C9b6ZVdHjjvJUIsk1m2qymGZ8k+xCExESJ00gpAhWWs8B+pfEJY/5Ep54WmKP5vbZWtetzBXull300evi5D9HVoYu0GXPC6xWwLSJ5DtzpLq/U9zW2o03C6EtuWJw/PftxoQdH8KCK5HIdrf8ynDhqCZS2mTCwcK+7bS1tz6T8/gy334NiNkspV+k0YErh2GPyC025rVAVdtEElE+XR2Q0Vu5zXY3P+aHaGbyV8stQFr+uzX9uyF8l6Tam+iEAlAm2/N6ulEv7oK28lQ9faPwWopw17OleP+0knqPf0eF4V6mQ093SiNdtDeI4tAMymroK2ZtvD2wj1zjTjLLorH6CIn4nVvx5BvVEqx38EKWvWsD6LWUKVmCmMERb5jogolFgBDCeZaFJzyfpWeIlOrlZRwCAeFl68Gl3z3aVv1UqXa8y6yjysinffaxvWuHT4Nx1AoB6+eP/52Y1S6PXkTSk3+/VONwHfPk652TYfFyutjOkJhbTSjENkLp2Wv0Zp5ij6JGUkz8QpfnzP9eaxqS/SGu4Cc0ozOBcsUGyWMTf0/XJwzLV8avxH87v8TZsty5rhyLZMUZqq/ggynR1AhKVq1ybYVdKNeI47PtNB+I5Vs4v7XNjxFVTD/d1lXeGbCQd2ZmtYKFKDIjd51zerd0FfRumUQtRyhrs+fEOrl61QCzVOzR8eKkR9Z4+ZTjruVDpECzbvZlAVYiveI8FV2hw3YdLdNMLBRhZaLhXmuDG+DRzk2Qn+i2eyaCplQBiyXEwYKJX01ZzQLHpzPqkWO0kSBEmnQpfsFdlB89qtgC+KCXOJV+KTq86Rhu9rpqXWSSvtaPkpcy/e0P0W/rMYMF10Qn5mTBwNyqTWmtcytNXMso0Gyzz0rJ3+4REAkQoPK//zVIijsTCtSzA4E2bzc/q6+5IqVDpe6iOiTdet8YYzfFzfV+l6LR2P/z+gA+7k4rU53p+gnYRaHgd7TmQUg1XIyaAikInn+rbnmFtDPklLD2xJ3p6bAD3winE1xC2GqCLyU5e1M0TouV14muQc2qzqMmWxMuWRe9W6f+U8ZASMQm/PL6PZINPGgW01MODgmKFbum6SqHPLQjmAwJt3Bd2VlpRxl97uQysL5GcbkiVBYxq5Xm+kv5VbV3i4fq90+NkH/rQX7oTViaRqGAGWcK1OFzBx9wmjqDQeh9oIyToQaRhWTJcjqWNSPr69c4Qwjrh1RbO0dZbIQXYdOZDVBNizkFF3oQe5MNyw7oGZ+AmVhYQtN9Gm9wfgHJ5A6mf80EWWGC1FhvprB5SV0rLR8abQLqYcz5DXNUmE5KwU7fnYJz77seNxYK0OOtQ1qvCnQWL1OhHO+t9loQj6HhMZ/IK20nmvzcVO//QzNfkdrADy9vA6JoBMYoun81e0e4Cuc/+dUwqNbAkaon9LAvIHSkLh01tvHjOxBsxJh/8rqUMNgsSHY0x0ep2BRLJrR4xjCShIvz3p+9/vsUH34y+eFzzQzxds5nvGBdd02xhXS8Q7ukmKwkn6XtbgtJI8kDMab6ebCJTEEYs+Kg/bcTbyBfFQqG2NRQUPTABrEinaSJRRhSpESO5etsY05B+uHkTFasEmOBhxD15TlokLJZ64h1GNNpFkdFH950+FYfNmzzpT6tZz5hu6CEwaDNEya/GKVcLnJScX0x29HjZZdVHlkXNWOEhrSCENHwPR29b2zW0WgSvQ7jFf+f9C8SyOWsM6pblyOWWFu0mM5QsC9EbAuYdH8XqbmlfWH9UN1BaeYr+ca1fYwjFb1dFU5y5BTrSOVlxHD18+dVWezvrrD1IGkrlFwARnppF+DoG0CxIe61HI5EAsn7/gKvgYgKPqAFy93t9KFPX/9//CFfVDE2G3mq9gtbsRm1i3vBl2qJjip4SgUZ8SrJoC0IiIa/y9iwNIbyV3/DjRatCbg/kkwhhEuFNe2cC5ZJ0jHwsBEv8yDUmOQ74oTLjilkHuKC2AdMLJObm2jQHJvZiItEfZZni5fgdOy9cJvrVrSyqARchDWOivuXHLoUyOZCTHeqvqAhm917T+iPBapSIzPaGAcEtLmX9bN2lZxlYlXvPwZ51F0nZoqLSIOX2kiRvxzE2nX2vQ1FcJ24LY1qTZJkVZA2CEjrq1O5pY/0OX1+eeYb9hrzoRf8doW1LZfmqLDg3gN2o991jikLAIIFJWqcb7Q7CJQmVleUOjNhFJFomHscMT1PCMeyuK6RvDUcVzi7QQaycljIBACwcN8RF0Sc1KGsQpM/zPo1AmSHZM+GO8huvHrzcdw/7YkDxZbSl0oBMyc3o5zSxpnJJ8tQfqiG1pKxyHFnaEkyAJwRcaz2DrDIC1zsBETa/snSoYOCYtr+Y/zhrxGR4025nyERaGV8mKRRjkh+8DyIOBxEU2j0v4Gz9lT6i+8axbG1K/D3acUjalloCFpSzGywA8jhIISAfCSPYrlXUHeHNGbPhWbjgP5WzjCLOrOFTmsHjt8yyoLJtTclbqbTieVm0M/AH7yZENNIqvbFmdHSO2b90QxRfoduf27FtiiuqKxq0dmNtqWl+AJx+qiXgCcptqVFnWLBFCmlhB7kDqPZSRus2io+UwHk2prDbJSfPVIf0mVPJF6bCUpaD26ly6B0rKSKvE8WEa3qVB2NkhG+v7x5kVG+nRFbzDRputzgza6zQhQDyK4dbvkDb9VjP1lXuaqN2q5KAv/ZzSNUp+GiW5PWP2TrtfT73menOLvqgO91uqv2y881fpl1YPcd8F2z13QLIKA8tB97U+chvixapiznb9ROqMVA1gHO7LRd1E4oAmRzmWRJzQcY+JOPDRpUalikfs0pVlQgTvetOC1AcdL35+c3nzR0clYMuCTypi8FWB+t/dWRCU61Viubh1SMJqFqAd3rOhgrpYFYMP3evrqpwSt9TN/PdWTA67gjhmCuXg+84fjubZPPTAO4FoQ9e5GzBrH5InLKfZqEritVpIB/dXDTLjTsNgQZZN4YzgSnRK8r7yLah+eShKbELiVZRdpDt8sExhsdsiU6BVOmw2WB9CdALgJ14kR6VR/txCSMQF5B5VWFCOCtPWfOihL6zjy97RWs2nge1WCNFoNKO/7w5w22LvYQMKI0OZcT7MWL2NEEMFF6IoCfBQrUSEYdfgVc0PzJx6zjPKzguxsI1zfHu9Ownwvy4+fPq4KD2fIC3N54ICpI58oudzghWKhMKitiki0kRl87vGJtWpn75+16UJAZkp+vMzypjvUVHRLqbFixpdUFtVecnyzftXUHyP6z2xoREJXo3BfCMWn/c2huTF/MZHj/ChDeB8Dp/gqMfyA59royOJPURr9xI6VlJ3Ru0LNqyYMKIvg646mwTgL8NVQx0cRHZedDSs1x0/8byVg9+GhktMj1U0tYjFlTFXwzUGFdu1jQUcMUgb2aSklCONe+PdmK1tYuwTXlU70+7kgwpWuCscohASFYs0dz5Sgap1aqV4Pzx0YORWx4w8JWd5i/MHKP9Tse8j7XyWBFkhGeMFObiGIGDu8Ry/bfeEL6XOp6VE12JX05WDfaDw4ADMjiT5BhT6QhTfBgPg7g+1stHeFjDxl6v7KAfnCfiGwm03h61mlrdnF5aT4kTv6pwQ+OuDhDTP9bxs8LTotl0ljRvrvPaBk0Iiz2STpbCyjgPsLyhH0TtOYTFM1FUi6qiiXj20kD4g6wPmCD5VBEa1oTv6mox+qnJMquSYN5Dk0fqGZvpgbB7unO3JXDnmEVwcjP41O/OgnXq8+8hSoWYAYI/PXT3S/yagAYJQc/iDiXzRDqZxOlcDVqRLDOZgg0uVY8lSQ+i6w2nzJ7QTjfDhR5dXxFFOMbWevIYJy3SVQqb8w2mrYyMrV49bfiAgKBX8x9PwKv4GP3I7dUJlu5W/5HF7I5b+Iw9muWTdWVehGWYW+6z3Vdch+k1r4rrdYHaar5qYwNX4izKQbUk8K+D5ksfZG1PkQeZRl5X6/wQBma0COZUvOintvBob5YbOabvrRLdNvm4nHIaAkM+qHJ0Hie+jqvDxxXXQ9n6KmFcAQieF8cHKzGdISM6Y9BYkMOsO1OYWLoUuPHzMxm29WGLR5emD9u4JQS05l8ZMecSrVmrta0lW+TCWQ+p6r7eNt3LXz1fxouAS/O+74EEfF5vKvNEnipYrmIEC85w2muPYzfjNl7XE0r82TYMddM5BtxNPBLfj0fbFg==" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="owvzZ53nNrwYxZqdDer3GtSxGWwthEvV393XJQ27/5qEneTQ2scCviCI9zU/8VDvp/TBjNr/MMcRoH/denqez7EP16lNioODgqg/zY8MAVRQDvig4BV/W/Wwsu2D8tYM5nxFbUVtaPxfvegG8d6I4+l+jsxJxHKCUa0Yg2UMX+Ze3E4kOukpRHXGMhofA/qHs8HBfefO7jmvFzldtQbyvghf11iXTd+OFIs9oBmkRNmdwEzhKez9E+VO0S5Yc23/qF5/q9UdQppIKGXINKt/Dz3yYYa/JZv2Qj+uanQljIKOdKxLa+kDQYM3PWb6Pp/OAoANeHhubaQjWXSpvBeGkw==" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width:260px;">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="#">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/eartquakesecure.aspx">Dos and Dont  during earthquake</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/externaltenders.aspx">External Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=grsGMsqnclzsclld">Kochi Metro Tenders</a></li><li><a href="/tender/Default.aspx?cid=3AGNZpFcFwwlld">Lucknow Metro Works</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender/Default.aspx?cid=yGEt7G92BVAlld">Noida-Greater noida works</a></li><li><a href="/saleofscrap.aspx">Details of sale of scrap by DMRC</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/advertiserlist.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/DMRCprocurement.aspx">DMRC Procurement Manual</a></li><li><a href="/tender/Default.aspx?cid=vlVO3VPqpMAlld">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="/otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/otherdocuments/VigilanceMnual.pdf">Vigilance Manual</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>
















  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">83094793</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
