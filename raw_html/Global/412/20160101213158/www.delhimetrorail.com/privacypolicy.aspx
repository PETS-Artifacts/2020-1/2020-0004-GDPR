<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJMTk0MTcwNDkyD2QWAmYPZBYCAgIPZBYKAgEPDxYEHghDc3NDbGFzc2UeBF8hU0ICAmRkAgMPDxYEHwAFD2xhbmd1YWdlX2FjdGl2ZR8BAgJkZAIJD2QWDGYPFgIeC18hSXRlbUNvdW50AgsWFmYPZBYGZg8VARsvYWJvdXRfdXMuYXNweCNJbnRyb2R1Y3Rpb25kAgEPFQEP4KSq4KSw4KS/4KSa4KSvZAICDxUBDEludHJvZHVjdGlvbmQCAQ9kFgZmDxUBFC9Ub3BfbWFuYWdlbWVudC5hc3B4ZAIBDxUBJeCktuClgOCksOCljeCktyDgpKrgpY3gpLDgpKzgpILgpKfgpKhkAgIPFQEOVG9wIE1hbmFnZW1lbnRkAgIPZBYGZg8VARMvRE1SQy1wb2xpY2llcy5hc3B4ZAIBDxUBLuCkoeClgOCkj+CkruCkhuCksOCkuOClgCDgpKjgpYDgpKTgpL/gpK/gpL7gpIJkAgIPFQENRE1SQyBQb2xpY2llc2QCAw9kFgZmDxUBIi9vdGhlcmRvY3VtZW50cy9DU1JQb2xpY3kwMTMxNS5wZGZkAgEPFQE+4KSo4KS/4KSX4KSu4KS/4KSkIOCkuOCkvuCkruCkvuCknOCkv+CklSDgpKbgpL7gpK/gpL/gpKTgpY3gpLVkAgIPFQEfQ29ycG9yYXRlIFNvY2lhbCBSZXNwb25zaWJpbGl0eWQCBA9kFgZmDxUBFS9ncmVlbmluaXRpYXRpdmUuYXNweGQCAQ8VAR/gpLngpLDgpYDgpKTgpL/gpK7gpL4g4KSq4KS54KSyZAICDxUBEEdyZWVuIEluaXRpYXRpdmVkAgUPZBYGZg8VARMvYW5udWFsX3JlcG9ydC5hc3B4ZAIBDxUBK+CkteCkvuCksOCljeCkt+Ckv+CklSDgpLDgpL/gpKrgpYvgpLDgpY3gpJ9kAgIPFQENQW5udWFsIFJlcG9ydGQCBg9kFgZmDxUBEC9ETVJDLUF3YXJkLmFzcHhkAgEPFQEx4KSh4KWA4KSP4KSu4KSG4KSw4KS44KWAIOCkquClgeCksOCkuOCljeCkleCkvuCksGQCAg8VAQtETVJDIEF3YXJkc2QCBw9kFgZmDxUBEi91c2VmdWxfbGlua3MuYXNweGQCAQ8VASjgpIngpKrgpK/gpYvgpJfgpYAg4KSV4KSh4KS84KS/4KSv4KS+4KSBZAICDxUBDFVzZWZ1bCBMaW5rc2QCCA9kFgZmDxUBEC9tZW1vcmFuZHVtLmFzcHhkAgEPFQFf4KSc4KWN4KSe4KS+4KSq4KSoIOCklOCksCDgpKHgpYDgpI/gpK7gpIbgpLDgpLjgpYAg4KSV4KWHIOCkuOCkguCkl+CkriDgpIXgpKjgpYHgpJrgpY3gpJvgpYfgpKZkAgIPFQEuTWVtb3JhbmR1bSBhbmQgQXJ0aWNsZXMgb2YgQXNzb2NpYXRpb24gb2YgRE1SQ2QCCQ9kFgZmDxUBEi9hbm51YWxyZXR1cm4uYXNweGQCAQ8VATvgpLXgpL7gpLDgpY3gpLfgpL/gpJUg4KS44KSC4KSq4KSk4KWN4KSk4KS/IOCkteCkv+CkteCksOCko2QCAg8VARZBbm51YWwgUHJvcGVydHkgUmV0dXJuZAIKD2QWBmYPFQEjL290aGVyZG9jdW1lbnRzL0RNUkNSZXBvcnRmaW5hbC5wZGZkAgEPFQE24KSo4KS/4KSw4KSC4KSk4KSw4KSk4KS+IOCksOCkv+CkquCli+CksOCljeCknyAyMDEzLTE0ZAICDxUBHVN1c3RhaW5hYmlsaXR5IFJlcG9ydCAyMDEzLTE0ZAIBDxYCHwICBhYMZg9kFghmDxUBFC9wcm9qZWN0cHJlc2VudC5hc3B4ZAIBDxUBK+CkteCksOCljeCkpOCkruCkvuCkqCDgpKjgpYfgpJ/gpLXgpLDgpY3gpJVkAgIPFQEPUHJlc2VudCBOZXR3b3JrZAIEDxYCHwJmZAIBD2QWCGYPFQEPL0NvcnJpZG9ycy5hc3B4ZAIBDxUBJOCkq+Clh+CknCAzIOCkquCksOCkv+Ckr+Cli+CknOCkqOCkvmQCAg8VARFQaGFzZSBJSUkgUHJvamVjdGQCBA8WAh8CZmQCAg9kFghmDxUBFy91bmRlcmNvbnN0cnVjdGlvbi5hc3B4ZAIBDxUBK+CkreCkteCkv+Ckt+CljeCkryDgpKrgpLDgpL/gpK/gpYvgpJzgpKjgpL5kAgIPFQEORnV0dXJlIFByb2plY3RkAgQPFgIfAmZkAgMPZBYIZg8VAQ0vZnVuZGluZy5hc3B4ZAIBDxUBJeCkqOCkv+Ckp+CkvyDgpLXgpY3gpK/gpLXgpLjgpY3gpKXgpL5kAgIPFQEHRnVuZGluZ2QCBA8WAh8CZmQCBA9kFghmDxUBKC9wcm9qZWN0c3VwZGF0ZS9jb25zdWx0YW5jeV9wcm9qZWN0LmFzcHhkAgEPFQEu4KSq4KSw4KS+4KSu4KSw4KWN4KS2IOCkquCksOCkv+Ckr+Cli+CknOCkqOCkvmQCAg8VARNDb25zdWx0YW5jeSBQcm9qZWN0ZAIEDxYCHwJmZAIFD2QWCGYPFQEBI2QCAQ8VAQ/gpLXgpL/gpLXgpL/gpKdkAgIPFQENTWlzY2VsbGFuZW91c2QCBA8WAh8CAgQWCGYPZBYGZg8VASMvcHJvamVjdHN1cGRhdGUvZWlhX3JlcG9ydGxpbmsuYXNweGQCAQ8VASLgpIjgpIbgpIjgpI8g4KSw4KS/4KSq4KWL4KSw4KWN4KSfZAICDxUBC0VJQSBSZXBvcnRzZAIBD2QWBmYPFQEhL1JlbG9jYXRpb25hbmRyZWhhYmlsaXRhdGlvbi5hc3B4ZAIBDxUBTuCkquClgeCkqOCksOCljeCkteCkvuCkuCDgpJTgpLAg4KSq4KWB4KSo4KSw4KWN4KS44KWN4KSl4KS+4KSq4KSoIOCkqOClgOCkpOCkv2QCAg8VASBSZWxvY2F0aW9uL1JlaGFiaWxpdGF0aW9uIFBvbGljeWQCAg9kFgZmDxUBGi9SYWluLVdhdGVyLUhhcnZlc3RpbmcucGRmZAIBDxUBFuCknOCksiDgpLjgpILgpJrgpK/gpKhkAgIPFQEVUmFpbiBXYXRlciBIYXJ2ZXN0aW5nZAIDD2QWBmYPFQEXL0FpcnBvcnRfYWdyZWVtZW50LmFzcHhkAgEPFQF04KSP4KSv4KSw4KSq4KWL4KSw4KWN4KSfIOCkj+CkleCljeCkuOCkquCljeCksOClh+CkuCDgpLLgpL7gpIfgpKgg4KSV4KWHIOCkleCkguCkuOClh+CkuOCkv+Ckr+CkqOCkvuCksCDgpJXgpLDgpL7gpLBkAgIPFQEwQ29uY2Vzc2lvbmFpcmUgQWdyZWVtZW50IG9mIEFpcnBvcnQgRXhwcmVzcyBMaW5lZAICDxYCHwICFRYqZg9kFgZmDxUBDi9ab29tX01hcC5hc3B4ZAIBDxUBLuCkqOClh+Ckn+CkteCksOCljeCklSDgpK7gpL7gpKjgpJrgpL/gpKTgpY3gpLBkAgIPFQELTmV0d29yayBNYXBkAgEPZBYGZg8VAREvbWV0cm8tZmFyZXMuYXNweGQCAQ8VAT/gpLDgpL7gpLjgpY3gpKTgpL4g4KSo4KS/4KSv4KWL4KSc4KSVIOCklOCksCDgpJXgpL/gpLDgpL7gpK/gpYdkAgIPFQEZSm91cm5leSBQbGFubmVyIGFuZCBGYXJlc2QCAg9kFgZmDxUBFS9wYXJraW5nZmFjaWxpdHkuYXNweGQCAQ8VATLgpKrgpL7gpLDgpY3gpJXgpL/gpILgpJcg4KSV4KWAIOCkuOClgeCkteCkv+Ckp+CkvmQCAg8VARJQYXJraW5nIEZhY2lsaXRpZXNkAgMPZBYGZg8VARcvc3RhdGlvbmZhY2lsaXRpZXMuYXNweGQCAQ8VASvgpLjgpY3gpJ/gpYfgpLbgpKgg4KS44KWB4KS14KS/4KSn4KS+4KSP4KSCZAICDxUBElN0YXRpb24gRmFjaWxpdGllc2QCBA9kFgZmDxUBFS9iaWN5Y2xlZmFjaWxpdHkuYXNweGQCAQ8VASvgpLjgpL7gpIfgpJXgpL/gpLIg4KS44KWB4KS14KS/4KSn4KS+4KSP4KSCZAICDxUBEkJpY3ljbGUgRmFjaWxpdGllc2QCBQ9kFgZmDxUBFC9Ub2lsZXRmYWNpbGl0eS5hc3B4ZAIBDxUBJeCktuCljOCkmuCkvuCksuCkryDgpLjgpYHgpLXgpL/gpKfgpL5kAgIPFQERVG9pbGV0IEZhY2lsaXRpZXNkAgYPZBYGZg8VAREvd29tZW5zYWZldHkuYXNweGQCAQ8VAVXgpK7gpLngpL/gpLLgpL4g4KSv4KS+4KSk4KWN4KSw4KS/4KSv4KWL4KSCIOCkleClhyDgpLLgpL/gpI8g4KS44KWB4KS14KS/4KSn4KS+4KSP4KSCZAICDxUBH0ZhY2lsaXRpZXMgZm9yIFdvbWVuIHBhc3NlbmdlcnNkAgcPZBYGZg8VARUvZGlmZmVyZW50bHlhYmxlLmFzcHhkAgEPFQFb4KS14KS/4KSV4KSy4KS+4KSC4KSXIOCkr+CkvuCkpOCljeCksOCkv+Ckr+Cli+CkgiDgpJXgpYcg4KSy4KS/4KSPIOCkuOClgeCkteCkv+Ckp+CkvuCkj+CkgmQCAg8VAStGYWNpbGl0aWVzIGZvciBkaWZmZXJlbnRseSBhYmxlZCBwYXNzZW5nZXJzZAIID2QWBmYPFQEPL2ZlZWRlcmJ1cy5hc3B4ZAIBDxUBGeCkq+ClgOCkoeCksCDgpKzgpLjgpYfgpIJkAgIPFQEMRmVlZGVyIEJ1c2VzZAIJD2QWBmYPFQEaL0FpcnBvcnQtRXhwcmVzcy1MaW5lLmFzcHhkAgEPFQFB4KSP4KSv4KSw4KSq4KWL4KSw4KWN4KSfIOCkj+CkleCljeCkuOCkquCljeCksOClh+CkuCDgpLLgpL7gpIfgpKhkAgIPFQEUQWlycG9ydCBFeHByZXNzIExpbmVkAgoPZBYGZg8VARAvbW9iaWxlLWFwcC5hc3B4ZAIBDxUBVOCkpuCkv+CksuCljeCksuClgCDgpK7gpYfgpJ/gpY3gpLDgpYsg4KSu4KWL4KSs4KS+4KSH4KSyIOCkj+CkquCljeCksuClgOCkleClh+CktuCkqGQCAg8VAR5EZWxoaSBNZXRybyBNb2JpbGUgQXBwbGljYXRpb25kAgsPZBYGZg8VARkvVHJhaW5UaW1lL1RyYWluVGltZS5hc3B4ZAIBDxUBT+CkquCljeCksOCkpeCkriDgpJTgpLAg4KSF4KSC4KSk4KS/4KSuIOCkruClh+Ckn+CljeCksOCliyDgpJ/gpL7gpIfgpK7gpL/gpILgpJdkAgIPFQEZRmlyc3QgJiBMYXN0IE1ldHJvIFRpbWluZ2QCDA9kFgZmDxUBEC9sb3N0X2ZvdW5kLmFzcHhkAgEPFQEb4KSW4KWL4KSv4KS+IC0g4KSq4KS+4KSv4KS+ZAICDxUBDExvc3QgJiBGb3VuZGQCDQ9kFgZmDxUBFi9NZXRyb19TZWN1cml0aWVzLmFzcHhkAgEPFQFM4KSu4KWH4KSf4KWN4KSw4KWLIOCkuOClgeCksOCkleCljeCkt+CkviDgpJTgpLAg4KSq4KWB4KSy4KS/4KS4IOCkpeCkvuCkqOClh2QCAg8VASBNZXRybyBTZWN1cml0eSAmIFBvbGljZSBTdGF0aW9uc2QCDg9kFgZmDxUBD2Zvcm1fZ3VpZGUuYXNweGQCAQ8VAYQB4KSG4KSJ4KSf4KS44KWL4KSw4KWN4KS4IOCkleCksOCljeCkruCkmuCkvuCksOCkv+Ckr+Cli+CkgiDgpJXgpYcg4KSy4KS/4KSPIOCkq+CkvuCksOCljeCkriDgpJTgpLAg4KSm4KS/4KS24KS+4KSo4KS/4KSw4KWN4KSm4KWH4KS2ZAICDxUBK0Zvcm1zICYgR3VpZGVsaW5lcyBmb3IgT3V0c291cmNlZCBFbXBsb3llZXNkAg8PZBYGZg8VARUvY29tbXV0ZXJzX2d1aWRlLmFzcHhkAgEPFQEf4KSv4KS+4KSk4KWN4KSw4KWAIOCkl+CkvuCkh+CkoWQCAg8VAQ9Db21tdXRlcnMgR3VpZGVkAhAPZBYGZg8VAREvQVRNX2RldGFpbHMuYXNweGQCAQ8VAR/gpI/gpJ/gpYDgpI/gpK4g4KS14KS/4KS14KSw4KSjZAICDxUBC0FUTSBEZXRhaWxzZAIRD2QWBmYPFQEOL2ZlZWRiYWNrLmFzcHhkAgEPFQES4KSr4KWA4KSh4KSs4KWI4KSVZAICDxUBCEZlZWRiYWNrZAISD2QWBmYPFQElb3RoZXJkb2N1bWVudHMvTUVUUk9fTUlUUkFfUE9MSUNZLnBkZmQCAQ8VASzgpK7gpYfgpJ/gpY3gpLDgpYsg4KSu4KS/4KSk4KWN4KSwIOCkquCkueCksmQCAg8VARZNZXRybyBNaXRyYSBJbml0aWF0aXZlZAITD2QWBmYPFQEJL0ZBUS5hc3B4ZAIBDxUBKOCkuOCkvuCkruCkvuCkqOCljeCkryDgpKrgpY3gpLDgpLbgpY3gpKhkAgIPFQEDRkFRZAIUD2QWBmYPFQEnb3RoZXJkb2N1bWVudHMvRUFSVEhRVUFLRWd1aWRlbGluZXMucGRmZAIBDxUBa+CkreClguCkmuCkvuCksiDgpJXgpYcg4KSm4KWM4KSw4KS+4KSoIOCkleCksOCkqOClhyDgpJTgpLAg4KSoIOCkleCksOCkqOClhyDgpK/gpYvgpJfgpY3gpK8g4KSs4KS+4KSk4KWH4KSCZAICDxUBIURvJ3MgYW5kIERvbnQncyBkdXJpbmcgZWFydGhxdWFrZWQCAw8WAh8CAgMWBmYPZBYGZg8VAQsvbWVkaWEuYXNweGQCAQ8VASLgpLjgpK7gpL7gpJrgpL7gpLDgpYvgpIIg4KSu4KWH4KSCZAICDxUBC0luIHRoZSBOZXdzZAIBD2QWBmYPFQEUL3ByZXNzX3JlbGVhc2VzLmFzcHhkAgEPFQEr4KSq4KWN4KSw4KWH4KS4IOCkteCkv+CknOCljeCknuCkquCljeCkpOCkv2QCAg8VAQ5QcmVzcyBSZWxlYXNlc2QCAg9kFgZmDxUBEy9uZXdzYW5hbHlzaXNzLmFzcHhkAgEPFQEr4KS44KSu4KS+4KSa4KS+4KSwIOCkteCkv+CktuCljeCksuClh+Ckt+Cko2QCAg8VAQ1OZXdzIEFuYWx5c2lzZAIEDxYCHwICFBYoZg9kFgZmDxUBDS90ZW5kZXJzLmFzcHhkAgEPFQFb4KS44KSC4KSq4KSk4KWN4KSk4KS/IOCkteCkv+CkleCkvuCkuCDgpKTgpKXgpL4g4KS44KSC4KSq4KSk4KWN4KSk4KS/IOCkteCljeCkr+CkvuCkquCkvuCksGQCAg8VAShQcm9wZXJ0eSBEZXZlbG9wbWVudCAmIFByb3BlcnR5IEJ1c2luZXNzZAIBD2QWBmYPFQETL3RlbmRlcnNfY2l2aWwuYXNweGQCAQ8VAQ/gpLjgpL/gpLXgpL/gpLJkAgIPFQEFQ2l2aWxkAgIPZBYGZg8VARgvdGVuZGVyc19lbGVjdHJpY2FsLmFzcHhkAgEPFQEV4KS14KS/4KSm4KWN4KSv4KWB4KSkZAICDxUBCkVsZWN0cmljYWxkAgMPZBYGZg8VARMvdGVuZGVyc19tc2xucy5hc3B4ZAIBDxUBD+CkteCkv+CkteCkv+Ckp2QCAg8VAQ1NaXNjZWxsYW5lb3VzZAIED2QWBmYPFQERL3RlbmRlcnNfc250LmFzcHhkAgEPFQEy4KS44KS/4KSX4KSo4KSyIOCkj+CkteCkgiDgpKbgpYLgpLDgpLjgpILgpJrgpL7gpLBkAgIPFQEUU2lnbmFsbGluZyAmIFRlbGVjb21kAgUPZBYGZg8VAQsvVHJhY2suYXNweGQCAQ8VARDgpLDgpYfgpLIg4KSq4KSlZAICDxUBBVRyYWNrZAIGD2QWBmYPFQERL0tvY2hpX01ldHJvLmFzcHhkAgEPFQE+4KSV4KWL4KSa4KWN4KSa4KS/IOCkruClh+Ckn+CljeCksOCliyDgpKjgpL/gpLXgpL/gpKbgpL7gpI/gpIJkAgIPFQETS29jaGkgTWV0cm8gVGVuZGVyc2QCBw9kFgZmDxUBGi90ZW5kZXJfTHVja25vd19NZXRyby5hc3B4ZAIBDxUBReCksuCkluCkqOCkiiDgpK7gpYfgpJ/gpY3gpLDgpYsg4KSo4KS/4KSw4KWN4KSu4KS+4KSjIOCkleCkvuCksOCljeCkr2QCAg8VARNMdWNrbm93IE1ldHJvIFdvcmtzZAIID2QWBmYPFQEQL3RlbmRlcnNfcnMuYXNweGQCAQ8VARbgpJrgpLIg4KS44KWN4KSf4KWJ4KSVZAICDxUBDVJvbGxpbmcgU3RvY2tkAgkPZBYGZg8VARcvbWFqb3ItY29udHJhY3RvcnMuYXNweGQCAQ8VAR/gpKzgpZzgpYcg4KSg4KWH4KSV4KWH4KSm4KS+4KSwZAICDxUBEU1ham9yIENvbnRyYWN0b3JzZAIKD2QWBmYPFQEfL3RlbmRlcl9ub2lkYV9HcmVhdGVybm9pZGEuYXNweGQCAQ8VAVjgpKjgpYvgpI/gpKHgpL4t4KSX4KWN4KSw4KWH4KSf4KSwIOCkqOCli+Ckj+CkoeCkviDgpKjgpL/gpLDgpY3gpK7gpL7gpKMg4KSV4KS+4KSw4KWN4KSvZAICDxUBGU5vaWRhLUdyZWF0ZXIgbm9pZGEgd29ya3NkAgsPZBYGZg8VARUvYXdhcmRlZC10ZW5kZXJzLmFzcHhkAgEPFQEf4KSo4KS/4KS14KS/4KSm4KS+IOCkoOClh+CkleClh2QCAg8VARdMaXN0IG9mIFRlbmRlcnMgQXdhcmRlZGQCDA9kFgZmDxUBDi9BZHZfTGlzdC5hc3B4ZAIBDxUBL+CkteCkv+CknOCljeCknuCkvuCkquCkleCliyDgpJXgpYAg4KS44KWC4KSa4KWAZAICDxUBEEFkdmVydGlzZXJzIExpc3RkAg0PZBYGZg8VARIvS2lvc2stUG9saWN5LmFzcHhkAgEPFQEf4KSV4KS/4KST4KS44KWN4KSVIOCkqOClgOCkpOCkv2QCAg8VAQxLaW9zayBQb2xpY3lkAg4PZBYGZg8VASMvT3V0ZG9vcl9hZHZlcnRpc2VtZW50X2RldGFpbHMuYXNweGQCAQ8VAUHgpKzgpL7gpLngpY3gpLDgpK8g4KS14KS/4KSc4KWN4KSe4KS+4KSq4KSVIOCkheCkteCkuOCljeCkpeCkvuCkqGQCAg8VAR1PdXRkb29yIGFkdmVydGlzaW5nIGxvY2F0aW9uc2QCDw9kFgZmDxUBMS9UZW5kZXJEb2N1bWVudHMvRE1SQy1Qcm9jdXJlbWVudC1NYW51YWwtMjAxMi5wZGZkAgEPFQE+4KSh4KWA4KSP4KSu4KSG4KSw4KS44KWAIOCkluCksOClgOCkpiDgpKjgpL/gpK/gpK7gpL7gpLXgpLLgpYBkAgIPFQEXRE1SQyBQcm9jdXJlbWVudCBNYW51YWxkAhAPZBYGZg8VARUvS2VyYWxhX21vbm9yYWlsLmFzcHhkAgEPFQEi4KSV4KWH4KSw4KSyIOCkruCli+CkqOCli+CksOClh+CksmQCAg8VAQ9LZXJhbGEgbW9ub3JhaWxkAhEPZBYGZg8VAR1vdGhlcmRvY3VtZW50cy9WYWNhbnRzaG9wLnBkZmQCAQ8VASVETVJDIHNob3BzIGF2YWlsYWJsZSBvbiB3YWxrLWluIGJhc2lzZAICDxUBJURNUkMgc2hvcHMgYXZhaWxhYmxlIG9uIHdhbGstaW4gYmFzaXNkAhIPZBYGZg8VARYvVmlqYXlhd2FkYV9tZXRyby5hc3B4ZAIBDxUBGFZpamF5YXdhZGEgTWV0cm8gUHJvamVjdGQCAg8VARhWaWpheWF3YWRhIE1ldHJvIFByb2plY3RkAhMPZBYGZg8VARNjb21tZXJjaWFsd2Fsay5hc3B4ZAIBDxUBM0NvbW1lcmNpYWwgc3BhY2VzIGF0IEphbnBhdGggbWV0cm8gYXQgd2Fsay1pbi1iYXNpc2QCAg8VATNDb21tZXJjaWFsIHNwYWNlcyBhdCBKYW5wYXRoIG1ldHJvIGF0IHdhbGstaW4tYmFzaXNkAgUPFgIfAgIGFgxmD2QWBmYPFQEPL3ZpZ2lsYW5jZS5hc3B4ZAIBDxUBTOCkoeClgOCkj+CkruCkhuCksOCkuOClgCDgpLjgpKTgpLDgpY3gpJXgpKTgpL4g4KSV4KWHIOCkrOCkvuCksOClhyDgpK7gpYfgpIJkAgIPFQEUQWJvdXQgRE1SQyBWaWdpbGFuY2VkAgEPZBYGZg8VARkvdmlnaWxhbmNlL2Z1bmN0aW9ucy5hc3B4ZAIBDxUBL+CkuOClgOCkteClgOCkkyDgpJXgpYcg4KSq4KWN4KSw4KSV4KS+4KSw4KWN4KSvZAICDxUBFEZ1bmN0aW9ucyBvZiB0aGUgQ1ZPZAICD2QWBmYPFQEaL3ZpZ2lsYW5jZS9jb21wbGFpbnRzLmFzcHhkAgEPFQEu4KS44KSk4KSw4KWN4KSV4KSk4KS+IOCktuCkv+CkleCkvuCkr+CkpOClh+CkgmQCAg8VARRWaWdpbGFuY2UgQ29tcGxhaW50c2QCAw9kFgZmDxUBGi92aWdpbGFuY2UvY29udGFjdC11cy5hc3B4ZAIBDxUBLOCkueCkruCkuOClhyDgpLjgpILgpKrgpLDgpY3gpJUg4KSV4KSw4KWH4KSCZAICDxUBCkNvbnRhY3QgVXNkAgQPZBYGZg8VARsvdmlnaWxhbmNlL21lZGlhLWZvY3VzLmFzcHhkAgEPFQEf4KSu4KWA4KSh4KS/4KSv4KS+IOCkq+Cli+CkleCkuGQCAg8VAQtNZWRpYSBGb2N1c2QCBQ9kFgZmDxUBIC92aWdpbGFuY2UvVmlnaWxhbmNlX0Jvb2tsZXQucGRmZAIBDxUBZOCkruClh+Ckn+CljeCksOCliyDgpIfgpILgpJzgpYDgpKjgpL/gpK/gpLDgpY3gpLgg4KSV4KWHIOCksuCkv+CkjyDgpKbgpL/gpLbgpL7gpKjgpL/gpLDgpY3gpKbgpYfgpLZkAgIPFQEeR3VpZGVsaW5lcyBmb3IgTWV0cm8gRW5naW5lZXJzZAILDxYCHgdWaXNpYmxlaGQCDw9kFghmDw8WAh4LTmF2aWdhdGVVcmwFLC9DaGVxdWVzRG9jdW1lbnRzLzY0NFJlYWR5Q2hlcXVlc0RldGFpbHMucGRmFgIeB29uY2xpY2sFJnJldHVybiBGdW5IaXRJbmNyZWFzZSgnNjQ0JywnQ2hlcXVlJyk7ZAIBDw8WAh4EVGV4dAUINDgxNjk1MTZkZAICDw8WAh8EBSwvQ2hlcXVlc0RvY3VtZW50cy82NDRSZWFkeUNoZXF1ZXNEZXRhaWxzLnBkZhYCHwUFJnJldHVybiBGdW5IaXRJbmNyZWFzZSgnNjQ0JywnQ2hlcXVlJyk7ZAIDDw8WAh8GBQg0ODE2OTUxNmRkZAnOYAz7PzlJZls43IjBrujcVE01CIs6D/gaZvKMwqcE" />

<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAAuCPWvKTe3Y4nfSbCWmAlhdUaBnHZtG0Wz7XGrIHyxAcTxor8nKYVLONgQ8pi8TxzPj7dallA1vzavOzYqdPeuHFciQ52/r/TRcebO7vDkOyT5+w4zmgwxZF5onDl2qYbqf4pvCZG5+MWRbqSr0ywww82Hzab4I+GHhOTYg4pmK+zx0Vpc25Dg46nOnD//jqWuvLUrjuIXoY9GFaXY/V1ulxAgGLzw2RaaEJ/dk5qex9MhYZ5GD5ldIoHoyqT/A3ExUScPmT2U4IGdgdoPh3vLR" />
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" width="229" height="101" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                            <img src="/images/call.jpg" width="156" height="71">
                        </div>

                        <div class="lag_main">
                            <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/otherdocuments/CSRPolicy01315.pdf">Corporate Social Responsibility</a></li><li><a href="/greeninitiative.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/otherdocuments/DMRCReportfinal.pdf">Sustainability Report 2013-14</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width:260px;">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="/bicyclefacility.aspx">Bicycle Facilities</a></li><li><a href="/Toiletfacility.aspx">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/Airport-Express-Line.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/TrainTime/TrainTime.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="otherdocuments/METRO_MITRA_POLICY.pdf">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="otherdocuments/EARTHQUAKEguidelines.pdf">Do's and Dont's during earthquake</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysiss.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/tenders.aspx">Property Development & Property Business</a></li><li><a href="/tenders_civil.aspx">Civil</a></li><li><a href="/tenders_electrical.aspx">Electrical</a></li><li><a href="/tenders_mslns.aspx">Miscellaneous</a></li><li><a href="/tenders_snt.aspx">Signalling & Telecom</a></li><li><a href="/Track.aspx">Track</a></li><li><a href="/Kochi_Metro.aspx">Kochi Metro Tenders</a></li><li><a href="/tender_Lucknow_Metro.aspx">Lucknow Metro Works</a></li><li><a href="/tenders_rs.aspx">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender_noida_Greaternoida.aspx">Noida-Greater noida works</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/Adv_List.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/Outdoor_advertisement_details.aspx">Outdoor advertising locations</a></li><li><a href="/TenderDocuments/DMRC-Procurement-Manual-2012.pdf">DMRC Procurement Manual</a></li><li><a href="/Kerala_monorail.aspx">Kerala monorail</a></li><li><a href="otherdocuments/Vacantshop.pdf">DMRC shops available on walk-in basis</a></li><li><a href="/Vijayawada_metro.aspx">Vijayawada Metro Project</a></li><li><a href="commercialwalk.aspx">Commercial spaces at Janpath metro at walk-in-basis</a></li>
        </ul>
    </li>
    
    <li><a href="/rightto_infoact/"><b>RTI</b></a>
        
    </li>
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li>
        </ul>
    </li>
    

    <li><a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a>
         
    </li>
	 <!--<li><a href="/feedback.aspx"><b>FEEDBACK</b></a></li> -->
         

</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>
















  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	
    <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at <a href="feedback@delhimetrorail.com">feedback@delhimetrorail.com </a> <br/><br/>







</p>
 
    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>

                    <li>
                        <a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;644&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/644ReadyChequesDetails.pdf" target="_blank">READY CHEQUE FOR DISTRIBUTION</a></li>


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">48169516</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2014. All right are reserved.</span>
           
            <br />
                <br />
                <br />
                <br />
                <span style="color: #aaa9a9; margin-top: 8px; display: block;">Powered by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a></span>

            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
