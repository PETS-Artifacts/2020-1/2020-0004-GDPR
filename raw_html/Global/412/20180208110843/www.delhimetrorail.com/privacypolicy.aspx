<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="gN4PGMUpzvrQE07GjXvYE4Z8O5N90omuf9czK3XF+JPeJgmCmCKqmekilWuaPwP+0lExBceVAG8D21g+DCxGBSVImuATGsEyocM9QIsUb/mIxD8UM/+aPOZJGAgOu8LoTPW2riCKRhlQnnRKDjpkADO+UTDLQd5/7P9dspVdU/mYTW4QNjxzWRFdyOK37Ufk4yNdWLBWdw9H9EA5qWXnmgYMGy8oKDUhCUQWfBor5iRvsNdnDxM56bRAmpBP4X+w/4NkiLvP68+2eV+IBTrEMJFJVl8kRtWS0915c8oQwKYyILHNhLT50vu18Ky1anwGhTf6CN6UHspof93RMDGklcP5cxW9fMDsECCWEhPoe6iKHbo89QbglsC4F36U0yMyOSD0wrDJoWvkYFu8rIolj3hnRWiXL6mx2bOmvtGW9780oFGX85vOLY4uYc+A4qHR8V15WylJDa4xsnVvMLfAWOsXSP03ZtkDP8xeBlTfJXt2oK9L8vo7jN9k14ntkBMKD26QgK/dpe4vtWmqbOVntTdWNkz2ymJRDe2TUpvWCY4ZcZmJWt0/tSh2gSwyxEJHAu1CnACl5BMPiF5U/9VPvVrDUFqoLKH8770c6mwsr2l18nkky4ggaMPDktgJWCOSzjednnl6sGGFr9+RLmiSeDf3hPZA2QbFIoUS65u8wPzcVGxdFE3BGJj2sdTJhPYPyz3sKKRj83PWlOINpdy92Fw3kgMD8Thrpvixy1tWOCszy+ay1ZdpFc+1u2TaZWm1oIA0x6kQK92D5i0yGYgppyaTaf+LHasGRKM/IPcoYl/kGMgptDWg7dD12+GM+lv3rgHyBnSZISHFoAdLbTkVILq6RF16AFcBV6I9j7YWUS9omtfs0jNC4Z2uyRuGQHKzJaqgNKucyJFZwcov9u0b6oXLjDxZaiOjhcU6izJBkBBPNfbh/LvWbs299FKN+Op86WJE5+3slmm/Q8a9+Ewvlmr8Y/aCHJrT3Jmi3JY/PXLnMAXBIiRntCB16IK80oqWtYKzgEit7oytMN6sjJ1uPfMMOko/zdT3wnuMxguVw0MSQMxvJPssptyJ5BfijeSRVl8FmLJaXeafEjbmB7aIhMtyx98THF2YFdTgicaAiM53C4M4UiuHlAZNlOf+kgbFIJOonXfe9BC1Q/iCrP8ER3eIV+zpXUEj59yn+K4KsGtOVIxWUGruS0t8NFBTrFwtXiHJOloWNoPSw5Gsw0xezp1iNJ86b4EUwcFwOaKCTm2C0lAzh35t8c9k1m6Da+FRIylSCwUhoeY/K7NC07+0CeJOTzA1odAkvD8s9O+SlyGpCn7IEqwAyw3JNOsODXJQMXJgDlNc4keAtOgIeyXTbXC0XE34NRt/uU/wBTyNUBW/DOEW7HA0qMtuw0/3mxUIHy6uMbfPDhVtezDpDSNjrku+ZVCBbYkktwKXUogOr6x7s7I7Xih5clDjR1R/D0ZL2WbJVn5+gYlWBn89Lu1pJjRHrbHaBGTp6UtXunNOAQ9xk9awbwP0LYE9Fvd26pwOkFdZ7ZE+gIh3d5TNYbPdR8lbyFOhPb0EUZLGQ0lkOmDG+dTpJKwSJR268+jc5uBAOSM1sAMXjOFlDGyKEflSaS5UBXctgcld08YCd/k/FYlPRcZ0le089BxV7VHuEPjT19HlOh3PrM/IUyJ65eYkB7m6KCMcb1qlC7GGIayf1STdqwpQR2q6u2ngoY6gDAs7z0IBQPmcBIsHHLqy/uekHRnr47gEWzOPuwyxmYeLsJkCt8jftmMl0Irs6v2IStbaIZuMXhs4eOOpm4l5xo1/B+VIocuXzHn20kJHz2dD/Hazk4ARPSA8bbZ9052pL8pwKL3Sm8q3QyjSjdfiLYl9EyXCDrn6it0VSYYz924gEDPokq5qAZSXToVphaK4XXYl+/xrtqZBdiGymyzhLc9AakpHhu1nys4JlDJa1lsXkfny2PROxPVShpB3j3Z5xyVuhGvjaOd12CGHKWueqgfRFsZMo/FardgPmpDwK6nH3r42bo/n3ncS1PgwpVCYTTFGnSo1Vm7CUmg7Z/t8+EViFzcRFIdOkZvqauzGSTlq1ZWo5R7Ap3+gyzFuWb8CwjO+62+dTOSrJUHm3vmV4H7+He2J80bJ/2BbWZR/1lAIdne25hktfyrwwcLlLSOnYnE96UqxKmVv8sYa46vgZfCq+b5q4yyCfKR3R9+UqvH6XeOsg66rNrcFvC0UIsdr1TOiZpPSpWQQPGK4Rar/C8q+yKry0ZBovnGSLuBBPySzmRDN4lYaLRbHAMxWW/yOisqYFRqBpe5it/dxrUF9pEv/2kZcxOyAShUtkVBclZ9xelnb3tEcmBmjeX0HCAH1rOPD/z+sf3qqqnMIsZ2vR4vpBdzvNUVgapgPNvlbRdZWH3pDsFnZI9r62pgf7ani3bn9kOZ7RwJI9qIz4oO9NqqUOWVHXVHY8hvzliwKK0dRUVeprlRa+j9C+4iyn0WkhjGCEUjLikwRI//DqtukPI950IviLAH3ozBIvzlbVtrN5bR+7JdsN1bGgBgPvY6KLN/G++F4bBfi/fDy7BZZf8fpjyiU9mS+EhHL+vMGm+RF3EljvvhKM5mnlMBwJ4vwsckMvEFf7J8GvFb4T7NMvrDlB37gzkFACHsw1ykc045NGiBZTAUYKRHv8vos/bi3DeBuz9wqyBA73griDQXs9HuYIn1h+6/PnT0fzkOCdSeppApjLPfo95DZGv/Ij72QAwaiGE/mkD4gitMkIao35D4ZXLnI3oyEzVY3YH6Lkx95MUOTneslphe2pCzew8UAzqYP5P4ORwFBKpzuV2W2Sytm6QTCQW2EUW6/wk8OBVlO1lyLKhtFK4PlISsPHt/FhYsEk6ZBuoH0kBO7f+7GKOZoQw1vTP3dYIvxAlOeGG+5LFeYf9oI6OxJghIyTgarn+yFcwsHn1giap1FJgssJwtmlLiGvGLkYL5RoCRUJSYxHidCeaY4KTXodY0XmjwOLJHz+pOgKxzh4+1rdfo50OrGKx4emWzJ2wG7GDtdELwElkBIo6Lz4QvtyPTFS/nkte3lFRhNsa64L9jJ/wNRmWySRnV0hjyKa/N8F8cnGBqXRCODZ53C09WrWpb7wec6LS0P28o6LZ3ggndR2ZSpFvlEGGFelkdPBZZsjB3laTX59kMDWvh2qx9jri4JeAXMvRS+AG2+vpp+4Z0jzokT98WQ86GzfrTexjZujgSf+DHzt+c6Ai8vJFSMPpL+UCbQwS9zREMMX3QYNHkg0x7fAmyEoPIJJVDDtu2PaYO0EXy5F7CPXd3u+g9yemRiXA9y25BHK+UF7e1fKhJz2cSBdQiyA/CRa1QStsjyuc9L6ivsDUaeaPvRu8I9/uCZSBk3LHzuVFKY7RhQ+uKPFHoimVfOE4lv8ZAsh5aG5t7jAnpXUDtMMESfnLgUhPujoFbFyjjHXnG+ECBaLgDIOi8C1jh/IpxFPBYvW1ypWOo5N8Ys9IGwz+r4N3BHvXyf+gSIGcMN4iu5mwDA3ZY0+4T2pnJRGRwMFE1RxxFJAtXOyCDKYrxKeYS3o7CEG0UFi1sRjlJUPZ0t2iMR6rZVoLkFtThXPlb+s5OgsUfdig9kbM/rWpLoDokucJ5aM4mE4VpNir/SI6vOVQVlqc1TK5q64sRcttqHoeQN0ugcdX44oL7q6D4cMiqB5G17na36JMsO/jkmDF3pglreCvaVnvtPdjjWuE8QyDjKDdBqNG+IK/Dl0f3LbNU+1iLTUNIqSTzH+nbOSmstYHHku05X1vnQ9rIn6dA545ppS34f5lO3pDifJCMYDUg1oTPjzGoOztfpHdR5K33NTJOhtqdF6hhcA6/VNS/T7YGapzduAn7EnZaY/Q/+D2x7VKkb8fcz8A2ygeql1suEL4uasDzUqtfnselQZKsfaZcQ3tjZwLDrTbxtaiF8NqYoTaNIGoMZiLJVgdaUnSPjPJ03ND2NT8QWwAWfUEONL+7/wX4R4AP8p+1xOHx24xx+Lmm4Bd8kn7+rQ51wZ0WXfIQ6iDJarTlARUvw/KhyaWNdsarr9jRDQ6dUBpcxVanA9VVMH+cVtGGZcUYJESDoK2RTCfu2clQg4TG8sZfDVpc5xveTMvrD147frqRSHtfz94osQJfpxH3j0rGgyH48kxRSE7e4YwaqiXJpchzrEDpkwwlhRKrJbhHqyAIPMyU+I/XZb4s4k6H8DSSBuMLSa1etA7P1nF5z7Pah1BBhl59L9Qw/UjByb9r1HAHoFerbjbXo+LoWPgYRLMARhVqX5oOBdBwRMthjffTwsZkugmDc8QrVWUVgZ6hdOqVTLHoDbAn02Cvo8lXHz8w6TyODGOqwHqFcY1ZUw87/+A9yAkHSiLnPquwQyB3dT2j7FJrbDMcAVpAxVPQFnGX6DbpmGMS9XakyEvL2rEvVxHr2Nzu67I6vaxb0vpyovvcx+29Bi1Von1SA8Qnq72YoH3WJBZgr3q2mwjrSmzKXQeCETCREOQXuKGxNf4VzD9tl5XFAa1IVMve3v0eAug/eTY0quE1eRzoueZ1m5HhCyLIqls5fFu3i1KoZPtHp52Qdit2ehSwlrMdMp7jsE2zhLQ3Su2wUP0L17DPV1wDK/yKz+cf2ieQHfs6qadjKy09S+R4rTpQw+48kw2lQQqDMVtKjdyANQA1Wd7NNTlqsGNMiTbiKMaT61vY0d8+MRUV3ShNfNHGOY6jXV5T/2TDynAsF4Zu1wkAepdDJKIw0bFgHumA7x8zyATNcvcydyhbIIQfnAFbbTBX7fifm/FoekVu7MKuArnBryxUqQSCmzBadd1hQ0YGxc2wNyF1W5gRySvtccnFgjlyXwwVAQyfisf2WtTlIDRoc2zvScKZaPL3W+Vvb989nE0jFYhqw3gFBxlivhmfgQCIWfJf68tZ5oUeKfKRO5wbEFHG1tunJ7iozI5/UZwR0yyz80eLP3H2gTh3KeQ7eDugklfD9daF6k5pIBUpdu7yDPS1ShCDkTm2RIX7LEYGSEbEvLMC8bzkTn2qUPV7Dw0sxRRx3KkT/ns5o2jpz/jT8R1hcDHwurFuDEj76vpWiXKS6LAnO5qZdDJBGFLXPPNCFpGm1E1FctXxZGowS+eHMy5cTwj7jdTCNDNMNYqzh7bFcMh7lyZsMAoT/RAA46OKjRahLi/Zx+3IG1+YAdvefPZtItMBlwoelj2nUY1v0DdeJOr3Txt3pDCjS65yXiM6lUi8XJcOV4UIhRbVBArlHjDFQYClFDUwsDGAdJ+THmzySZ+dkMq/hoyK/reWvRlqjsBeZBsQGDl68MKL3e+kHsNUq9hEnyu7SVCA6g9DxvJCXDyZmLmpqbOAcf1VE49BkZRM4wRjytfThD9BcxmoHlU6uqOQj+JQd2teU2WXP4U3hHjDDPVov5VRWdwFlMvsiavCnc/Qed7FiXRj6rw6/w84+22/O1OBsaWlQj+vK4LVhQN07tk12zpnVoQswNVaXwltvxkgfGpyd/X3Yrzh4exX18OEvSJOdnj86ibkd0dhsEwDJjB9ATDdhzVp7BJsx/I7FesTz8L1ezaBlvX7Fulr8ceI53ftIrwU2WjO9jEvU2mf0uMmeQ/IF0swSvZbkNEmH2rdDpWqmsHkcJANBHavnH0ARaWwutJBr7GzpSiq/YGMvhT7DCbr3QzVtILl2GeTH7bn47E1KjaDO/jdEVPIzUsgHSCpR72AP4Bpfw7xjJedE4agJnLpES55E2zfa3F8A8WQXh1INw1lv2QKG6xp1qDwhQX4rtPf+NZ6OL+qJc3xSRfaSeY+AVYtWIJpwrHud93QQghCGjcAZNwmfvpePrrVdUD/p0lhkpsh4vSv/EW5BxkAaNMdIJHReWLwqsn2+pkyXsAJYnEOF0PQtT0Kv6VMIRIGyjBFlumB36lVrcFL9uv+IpzWda8h+OvekIABDvyQhNGMgkoYjczdmfjrEz1ZuodJKb+/YPGXOhbfuJ3vYGJOc0i8TGx3XyFQ8MAzzJDJx6zfh+5Q6zTcSWUNCPzSNkgtD1ZudHO8gvWQx+QERfoGPJ3xSc9vSS361CJ9ee2/xbK6XLRa/pdDmERe67R3iubNyE8xlY5cU+CqY9Z076izt0VhKleLvhr2ouDWlGF9mByijEfYwPVZA6whvzwxwuz71OJfDuvi+vWoBcXPIh0IPiBIHE/zJUdgzQO+B8KZQuTGMUvkKZKcmcu+xaszDzrFE+udReel/9QHwq+OuDEPsL2zzCqSLyiiHWLTthL4JaJvd4szknSHppNczWMblFliFjlRpaQaSiBjO1nagp2TqBBGV0jvkY+c/VvjHqfVqbCqkXUGbfEarErHV2OxDNxuxEqC9aXqr4KMWZl2w+wlC3m3DLAjvoqauThmI/9CFVsPXwykuYUtrtSE36XvZ1rLHBEZkBAE1jy31tyC1rLlG5NiscYyL10uiW5HTOB9ZeuG4iPFns9sH59vENNiDYn3CaeJajqI73svchGbT+2WAb7iX2LB3sxkNyvOOWg6Twwe4Qx/FPJIDEgV4zLXOQDxAVuWyKHwzT7RsKK5YaUTuSA/IJt/IDXw56UHw1mccRY7iOv/grS/j8uCE2jRSOCBv+ygElNSMqRmC8aYZCE58xEdUx+WtCGs0IHQX3zmJDWuWbJwfm03x0eFkHIWhojbGTtEYnMl2ZgW3cat9iWKKVYe+aUvEQs2+f0QOEvXSz4sa1Zi9/yuRO/P0ZDpEmZm2kqComi7hv7mn5pdqJO/YXWayKbsUVkg76imcvvA62c6/RCCpaBwfsDEOMgBqg90bcONhaspQWBBfe1ZArftuQJNEEnT2gznUL8displ3OhZN+F+mDc8hYURufEZbww4aZMNTjGW7flSZSfkyMh85UIazYVw3ak1pyTLXhEzclUakh/r4ezlCOEq+MuP26x9psUiZ8jTm1bDmYr5v87sR+JYrPPsbvQLunvknDV4JX1exJ/dvys7RPSh2e9V5tMxiZYwT4HSJmIKs9E5vnvDp6yyiLgho32To9lFnj3WGn8ab6xFPVfi0zild0W7VNb1TU06a5xY+YzXiePSu/RdFKfmYu8UKgK4qDnGYFi94Ueb/WVVlROxMeWKVk/mWmIQburxDYOqzUiXyY1FpiwzkQYEKa2gfj/TdwfDDZtIv0n2xzTYeYaoxWGY8TE2qo4gS/pAPN4vwXT7KC+gL8jtoAn8GKjA1h9AsmbeykYUdc2vK7w1zteu03cBZ/tPl44hJEaSQDfzr38nDy6Ktt7Os8lYTSlg+tQp0kw+D9he4+jNf0XpoNymv8mVuUc7fp8VqGWmomTudk9xinfHGmlAQQ+TlzK6Y4L2IM+XXJu5fIivwaIhVEOhh5tMfWf0LajgBIEOWVBD/MMXSNM/byW421e/MRTg0EsYtlwWet2v8jFY9BdYLjVtp8LuHAE3EdgZkx3pbAxQbcBWDtAv0uL+vtENKeH9BvpLMvIYtTdkwe6pQ/sbdaKerQlIFD72DuWHZgOodnrJeWsBronW5O23JW6sgtaUOFmpIj4XEjKFyUUOFWWW+/7/dQWY6sFpNfmYBit4Ttkj5LMp/X06JjKoCeXx+YBfsDDrjNq97uUEmRkECla2x8a1wriNNL0zQSIF/3ZQ0a6gPEnMr6Qfs673jMBDpUMsRWekEwthiOsy+FnCJm7t8EhvxVpuEnQ0gNrZTLBwICac8/FwLp0ue/6q581ouW1xbHg4g0m0mZIuJqax+4wqctMKcGmaBRBpcRoMUe34KIU6QqpglOtACY8JwPvnnzHwUwu+9Na650K3Q7ejVuvtkkTW9cLwIAH0Kw8YhFcckEAhNIXi1jNibeLS6YReUTjMG0D0YlV4pVfc+QFRYhIJHnYe2qt6AmT+5inY2rgc897vDPWcCLlVGOD5GF3G+6xGOYESTWeiChXvm54i+0MEJtbx6eRm30pK4wDzqdDmHQzRvm8XMy6aUVaDuzFW5EgGdI4U99+pMH/jlaAFuHPaV98HAg2pHRHWzKgDxD7oxkwVokiPQGhMrR8v5B+svUJPADhra/uZ4h+W4AwyKJ1qBLCrhIqu3K+NGL6+sSzCkxAl1Mx2PYAI2Zy425NS8RmrL/I9AuDf4kc7MvSo1HG04irSUnIG2ys/AXhQpgQ1trUkLkLIMyVAeNI5nlZfdMYkM5Xm8AQAlzvzVq94NkCeUBC2KCWxrApCB5k2WbqvZKnFzNhS3zMcdIV0Vry+7rqQ4BN79rRegErIQfBOXF7fsvXX66yElEoK8eXYj9SVEmiOGMFwE7TiblKlP8G/dNhXqLX5hvTn+G1JlxREo3GVx/SzUyY08K6zpBIli0vSrlJ3D/7hiq3bW1ZrhZaV1esTzi5EZtifPnXcwR8AiqXz3UK3i1edyyUmIfoLb4Dv1P1OISP+s/lhF6CWKfSU60JPEQBzOHJA3E80fnA6gjU8gnByLjLCpm8houzVblDKwT342MZgnQnAFcadsF0nUtVVv/MO0T3YCfrBZoITALrhKV1NExZm5duFc35cqBdfRDuw+b6NZ8WLo/b5z3RaE7jeANjOLT+0Tns7v2q6+8m2wbToNnB4SImc2VAXtGOOWSOmuG6iA9SzqvMjRk5zJePBAMA0Yop/GsseXzgejd7lSiUaGC8YTtUHx4m0AV0yN81AbOj5sSH7MuwJ2+bx6R3jDnsu6c2eKdNmuFYsTypAXidGrf0XPB/DtLxoMn9HHdxyLnupPwkv01asL52cGLJVD7luTs9KZs//7VO7UWa45NNIp/Ii2c38RCS76lsmd1ojpwfsFJhNc7x3FiQkldbKzfZ3tDo33ufzeI5fd6m6XWUu+9DaYQyFuWl9+WWSBR+91VDird/R7hILFL/+ecCtVaM9DDLyPksuQd7Pcj1dnphWX64mOSIbdFQ2JrJQcUGXN0YKAdQZOQBySxMhY3huqXTXea0mqLF0ECrDeV0kvXRlX9nlZWjOXTpYbn2aUh4jCeM9K4jGBqAjNyCYGhiOHV66LsrzsOBt1Pv4p8gQBwxk+UjOiRj6L0qwAK8ZkzRJRZuisDsIQFmSohdh379x3Mrx7sOwcqt6YbwcywPVvqlmz1DOnMKao+ZC7/cnXihuUBwAvyKhJR40XXz1qkuKChmvtT9A65cflnfY2LuhMiCxKo1OJ1jak2hFvQK8FX3PKNycJN3iThDfodhdkbY0fr1Z3ievUuAzpsU4O3tznCklxnwiWPGt3uubS+iXyGF3usdHI0EPesblkeLKIlomSnKNy/a03DgPQJnHADTJ+z7KHloCIyevZXwv1CzpZFWgZeAQ6xPyomk6eJRoGyIZ7C/2MIkKLUIGV5BAqiNfhrIOBTCINB+gYtARVBGmInBE8Ki2WkH3deLnNglXA4kSH5yseDE80/ZIvFvxdbcUgl/A5bsrarDO95vU+jArb8dh9f3/as4CXecdZ5UwtX8P2/T3ukWuLzvDK+gskr+YKSxUQ9SWTjyYgVo05rF+5RGqOPgVLN9v9cAnIQfPG+2iVwMiiUqsRwDfqObHcjmuYXjYnXLhxwyPpES2ua10mGkmca+1A0GB+qM62bRU4uh1XrGTvUNdL4jX+RIwROHif27uulk8y0EEB7X+21R8KByVCsOpdQCVnWzaiOLepFq94kcbtrM5eQrJtM6Yn3V0fs7Ok4W2p0pFQ8EQF+7VTr1Mmrya0khDxNf7db4HclwdVaTxtjMc0DfH+m9IInRrqcPNY27RrI0zE5264PVVaKLpmCNtdbG5Too19lTWOBM9tml75tcEaEaVwtnl5wde7kfKt4v2aq2NI9XsVzgPAd5ieEQ86cpcRCfpxDvIk6JtqiNDu/5xdZgd/8V+81vNqsVxRFZ1NI3yqrcpWLLeMxKn5IMkCLGiI+w02sAmj7vEyJVODuUtUeCQEGjNB5nMmIFNxXpgmKxnTOrDOIEgSGv13nfFIebFdwvN5lF3hg3bLDvsEgjvq2aOzvMMf0N6+DuLOtc/vt+taXYRPXYT0MY7giOprqRLUzvbQ6e68tfN2fEFEEo2DgrZlYea+J6TJS3WkTRulyrtxPXUVbgtUT85pFoo17QBgZYPeLlEVFz1StuBalju9YUVfsYDuszNfdYEz1FZzqRKbbGAXJuNP/zFUuxeTTamtkBiZgcA/GWuKPk+hKYdEAqkGkHWivnThY1z+yBnsvFNJY1Ya4db4kQBZ66TVC2nmHfZb2z8AYHYUoqWlLktaRGXyMK7F1qRHjyLfNFf1T5ezL8p+XjK1zsFjSke4V7XKnjER0C6OLBIizEsV3FTg6kVYNMZaotpKtvI0+kNdpPyfMJjJT88PL1OzoEUVioZcHHOnZMewx5l6Z95pwDZQ5CtR30UWG9OjVe3Y9se/7nccYBaK9GVRHKSfUKsvf+yx6yXCoRUu5qeQNu62UfLwkeNlwYHZ4+rlqxeiGlr31AnWdvSjCe2i++tieI95Xch2BwZskn5yuOTNa0DcExA/YYA8RtYBsnexOSS4y0O1Q9BhLl02+whxb+wszcbWmrF2RZGZsQfxgKDXtQROHmZA3MkigIyYSh0npQJtX499Yt0YgQ4h3HiILZWhDAaVRLm5dxztjRm5oiDXwaNccaDeVpDvsMjuhF+vJhzvFi+mxwqXxub0c/8Po5rQ4XbJJ3njCOqoqLbWybugWzOZ9TCehtDiETnzSrVKj+wdMasaXZzLBGP4v1oWuoU90eTd7OzV/U5oO3jzA8I9UKxKQ7fJ5TzQlWi5f36ucuBzSuySwZJ8eSgr5BPk0n8SCWD9O++wyQPTzbbvqW3t2AdUxn5FngKUkT6bVhZ3fstBuabAF92T9KUpDVc5MklsVUCvdo2anRQEpfvJfEHmRxRj5mk3P4FX5wsCDM1363Q4Be/l587hADWzoHhBsVpogJxeMguM8nZFcfufR2AxXk1YYeGFjgLx8YpPlrsFDGtqng+hOS2fQfoKpxmeKlEhDwH37kVjTouBmV9saBe6qDCyNvxiZYDnWNCWUqoadl6xXZhFBQ01wKVqauZ+Fx+ZUL842M/BZRAeTt+efzh0tS7eshC2kq3Mm3L8xRyVC/7+rQGc4d3CJ22GNCVm+YaUaRPysxOVkRnrZSD01uk94MPTJN7JnxtYYWKok5gp06kCbFMgpfgaY7LcmlNyt6nL6sJtzPMjngChysQs4jxiZHmagVnx7nBm6kY+Yt/iF4q4wQUJZZJX0XTKQ5XBcvDlBm7u93CWlsEE6SKFBAMroLQES3xTZq1WdhEERAdhPHqT+L2nXB3QeaogI6sirIXkxMFxdbqDGotHakD7qVpEbCUY0n90nGLMp/sMMFiKsM1FRyt6PanIMrefPDr766bzwzOvMukVo/Xu68mwrFoXXJ6wPG228tfpyBqNM18ZffzOz1d5NriFn2Z1rAYwA85nsxzc/DqXHg5P47MDxzpMphOWcBeqeKgwdRqc6m4qHnbHSPO15JI5iYOJejVM94QEu/q20Es/ZpqMtc36uEsfVbNukRLCM9dtwsejySdM3jgR+bSctM6E1Gfjs4FvCHDtYXKp4IBuRrBD5aPW/LAFiM8xbx30VrWs3nALj4uQuGInzGmldv1GlmKs2I9c6GUuppLrK9ct46FNDT36FCB0PcR8ocfVjvWM7ptOjS6ItyY4ZcMN0VZKzAV6FLm9XLhBeDEujmQMqhaVV2S2plY1YLKcndglGWMtyPKf+VJiP7R544lQqkHZsCbAZ80SdTDPvI+I7OQKqObcnGwdZmvu27twXjhy8pCPfEtzsL0eoGeCOKaPSQfY6o1nNUgSNmwY3lBK/tnXUAnksNuBmbmYTfgtS8T15OGkgJ9xU8s9WfnAlMJT2ATdGWfCaIS58A+ybRend0J7Y3QTodUkYcMzliZgp027qM9XjPTm42XDEUFwa0K5WrJLw/Sc7MN/rPb51wZ/0ubgSAIlUhe+1uej4zxToyr0485jdIs2NZiacFKhfvS+6sA+AfVZkTbmOvArQJ4R1Y+n8YxcoVL5OXRXpirOdUBeYfKxJ33MVq928LsyZRoTTxekvzcOEobh1DOZdB4UOtPuXcTtDD7ZZFWYBUkiLuSis29MEa5c4z3MYDQOjcMZVRYLcuV1maNPjEBe+CGhsZ6leD0bWIkpdmrm3r8w9BnjfFAEdBG7z+dcDVb8iBC/8SoWUdZeyot5h0OTsWW8sHYkwlJ9ZlOc3wdEkybqAj1DQAk1Ivzf5B4Ms/jaGm304lUyJWI7HpvI79GPqXZH11vCctzvGY5sb5lDY3eS0JN3BjRuBxaTjfffai6ICD/nhrRsIbEV3Gtrbus3X/8FRVcq7ExAy1R2+AN2Q/c/aD1eRoTU2dknDkGdByiip9EvmbPCiuFYwFcKQgFBBrc0NiqEjnrIz3iw8GPqcgWtVRsoYHuO4fHeO29GOTXl+nNuxZA+7KsXu0y7H3WGkoCw93rW+gk7ADX0s9zifgx6Xtcuh0+iQ55o+u+jZHywgEEwA80D61PBPivxaTC2ViXAXXzUSTKUpEz22gdM1kTns9Zj/QK1/nL+ivtrEMg5h/Lfmx1I82SQdY3sgHVzrOY/dDta7tdMW1Vru/+AsQrTpXkKz94hGPlaMwWfKFPiut7RZkf8p6LvwWHaOQx5SjGOBhVeN06BExADBKC8UIxYba/4TrhcX7jO8wU5avhWujKf4uL3hbYnrsBEZz41NezXEusHhoA7Ye/w6djyyLpSoxV97iDewmPIqim+PQFWdhScadzSYLG+qLfDdDNpCRcFjF08r5mv3i8LVPzD3VUCJR6if82E8zI6lYcgJ9NCMwV4tYHwHko0klE8NIAmsv4GgjDXAV6Y0Hss2hpQPPI0X7EQEFNQJBYVOk3rlBYlFD2vsl2CSQpYfvun0CCvJZflfPweKUPza3h6s4m9AzAvnWp/hwV18EDS4HFfA5Rux0GLwTjMPz9c2rjuZizRcUWXqRgWs2wgNbAadV1xM6XX6co180VmVo5w8Bh5rVfJ7+9SikyUlVHmOjscjjx+DnA0sqxElIV/N3g6sWgzt9YflseyCz/m4vsRQolPgBPLE14yhssv8GLue3gGjkP0qJg66fAiMpbIrY0IAsu3yxMWaZjYaosNjUXI8wxMvj0HntgYZUOjJWdVOsOEPu6aHmpDWLpXMGDA+do/eC4mcRit388A1ZRLK5aG/OU+XfoldRnD+8T4ucZLIQlyU7XMevp6uu/pQ3WIhfn3g12JGhGyeDVs8SROSlQvuJjDoWXAiMvg06L+tYdNaB9KtVXD8uSkyXVK/WCghLa5KusXd4UoVVQVvwbZYjmErRjKYclLNTf8kkyAk9qmLBbN3yxc6pa16HBydZmj2q8+23FbMO9aj0KxIURKrvGr2YMMflNlCg/0uta8Y+7whuGFd5ef389PRpm1LEZdbsogu28jK9Huem6JgUuLULsSrIK2ppkj8NbEGqDlTdsMSttK+bLCYHNOQssbcDnBvhznS/OxfMwHbrjyN/LGtK6TfZ317NBAFCx1X2+Hhteih8JfC9L3amRPIH3WsQZ/6DD4jwgeC3X6ZWYYEDtUOJZfr2YPI+53JQpNFbLl0ocsNE5hDmN3+kVvXBobyEDi1ondFCIgk34ITzT3dBxLRU74QkQskGWj40C0AODbmaf36+nU67o73/HMasNu7wiChlHKYIXoGiaWgCS8AFGgkorYkM7MRK4L83I+29qOnybX76wvDe0Ip2ruRHqOJtMy45mdVoVGNAYTxb6pbCFTJCT6tVUB+IT7x0h4OuEehp1pNdkz2YutgDghKbJjY9y0daj/snBCuRMhjuU1vqnfp4AV8a0w1ouHtG079gEHnj9lJxNu1u5iaRF8v2+ykffa1o5ebtvDWxn1I0Sz+pXWNzyMWRhaiH7GQMJf3+IvQKbrMULgglIBT4Ghd9nIxAsF6KbxP+RiALiDCh3+yptzl6oNgIJlZ1h5Jl4PVGVswyaBbgzvn3YM4gVGl/hcOzo=" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="FtaUkc5IG2A/seTexnA+Ml5p+gm5Xrk4mfQ24WhtH+Cp+WWs5I+Ac1fbLdeQgqN9lTCbE+jIoMR82NpR9HMn11A0l82lG/BsxrHS+68WVhTx2AREYgfXYXk0vb/gt7Nj82hBbhSYHoNZjg6xW1dWR+eyKbF6I/FlPVFX3poWjAlJjBmuimL3Zo1ElcTOAOs+sipbdxnlng6iyg3HDkgzG6HH5PSoow5c9Wh9YjRMQYyE6uzCNDWgGX6FtdVwxt7NqRNM0Wi4Ih7TEhdsa2SK8EIVUeyhw4ssn3C4gRChiuRa0GYQVdIMI8VsCbcC77J0NhDjIlQeoZuNs0dpkkz22A==" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="#">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/eartquakesecure.aspx">Dos and Dont  during earthquake</a></li><li><a href="/otherdocuments/800/BusRoutes_25118.pdf">Bus Routes Operated with Common Mobility Card</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/General_condition_of_contract.aspx">General Conditions of Contract</a></li><li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/externaltenders.aspx">External Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=nclsis35ZWYbHIIlld">Phase lV</a></li><li><a href="/tender/Default.aspx?cid=grsGMsqnclzsclld">Kochi Metro Tenders</a></li><li><a href="/tender/Default.aspx?cid=3AGNZpFcFwwlld">Lucknow Metro Works</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender/Default.aspx?cid=yGEt7G92BVAlld">Noida-Greater noida works</a></li><li><a href="/DMRCstore_department.aspx">Store Department</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/advertiserlist.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/DMRCprocurement.aspx">DMRC Procurement Manual</a></li><li><a href="/tender/Default.aspx?cid=vlVO3VPqpMAlld">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="/otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/otherdocuments/VigilanceMnual.pdf">Vigilance Manual</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>

<style>
.nav li ul.pass-menu li
{
	width: 50% !important;float:left;
}
.nav li ul.pass-menu li a
{
	width: 100% !important;
}
</style>














  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">91599212</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
