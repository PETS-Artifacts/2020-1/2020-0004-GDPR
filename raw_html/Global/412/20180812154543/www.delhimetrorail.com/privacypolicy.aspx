<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" id="aspnetForm">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="ulp/Qg8DSA3y1XhFVJTTkjjZTHNkJM7vOoyDE9rz6yo3sxJtgZjvuJOrFB8rn2EObG72cF1JOgU/uQMCxQpMXUakhOXukbe60OCcPDk+8wac5FQhfYJKwjuZWOr+oz/+Tt3Asj99sS0LVnYZwrB7ZFHQ7ecKf5ewWm1wGzi0rDPA/X7qrb0rt2Y+zgOpDwfjlOWzoDZq7N97Uir/xe2Nn4LPZ5RTWKbCBJvzqfqPywJZyflJf2jOk68XiHn0JS7HOT5AFDQH8xWz2AaDt9KySWVfv9bX+GqnKXNaABZ+6bNn7zwe/VPA0AZ8v58wEFJmtbJdCSR47Z1oYDk7o0rOxtRtTWA/MwiklPAmBxr89/w163ZdQcKt3kGcZKXJFRTkwNWrxihnv4Z8by5SnGfxrAGp1PfOIy7R7QgLev1Yg5LeRjNW2pdLFdvJpC1kfC/baQ5XnZkq3Tn4ZO0QvgO30PI68bv3FeGi0VI+lIhdPyNvXlESvcs8cahV+RxN744BuUqunhxVcCB8EadRaQBz8k9RAAxmrm7rQmrAN/BWeCkTN2HWIMF4++DlEs8fXSUmWuDMOtJp+SaJcByCOhDYKf60HhZNzl/n8vHSezYvGZi4uJTwQsW1a+GB3J3nZ/ugJhakKVLKIB4x3ag8ogpE/MnLq3p8z5B+8PWaRriW4YD1XDPyFsxHMK3LY7Ni/PN3H4rB5vwRohRqj8Ghez51H6nbqPtAFYHDWQGhG8TLypPv5JUhXeCSHdX4pLSj11tRevthjgzTZH/lkuffk4CbKyc5XKMEN8V81gc71BL8a/zBA5VPjZ8HOXH0uxltSv7oxiTMK5ag6GzGpKGXs4d69ndC4QqB5J4M4jr7+vDbw7St5klwhRcW9BHKiLIJKUdK/pMqeSu6KJXrvHIWdmeYPx6f34QSzfpzSLnB/XBSKkW9Ad8wBZOJXaBZ7gY/o9+3xx836Khx6O9064nRkJQmRcEbKSyE56eia6PmCtfbmIInNjRTkSyomS9CBKWxym5IlxkjZ2du/bx6jKOVEL0Lma7Jv1p/9duR2v6NzRsa8zqlf59RHCE4HickZgFp1l3JaSQsclox13fpiYgzFkIBc4ulLdVdkOpdfLQ0O/zikaSbhCmeUF3/yflkGdiJld5DPxMh69TVTHi38t5nA0FwrL0fbidnirnnrifL4+/LEvdhLE89rMkX9b2VYjY0GWh8eL1T9IKI3tOX2gtI1sTK4e9czrWqW3h1Z8B/4nTzCKIAofsKII/XF/9Qa/uv3FzUrQ9863jtiqX57b+yA47oXOPgRy+ZH6Aw36vkobmyPMHcPvmSgDjJv85S3wSA8NMzAVoM5r6Qw/K0KBqIhrx9DHzX4jqXmpQorhLPCJA6hpfzenTc5k/py51LqVKFos3XNtuK7Sljj9/8aedfV1c2SbUg6bMj6SrpOCVE34NK1PlJyW7RO2vxJsa1eDUua0n83FxfMsrT3QWcdTnzf4KVSObaCZvkuWHiUf51qeSVACTZuIxv3qsjfR8cXQrVYT6MDBnktaxDVfMiByJ3bP4O/g85YHowKHTM5Ao5tNhWnUhosMPDIm5FfaBZ5AJeIqtWJlVptXz9F/ZuaYY5bKGNKEylw60NITcNtaePJRNNYecvnnSfDvWxrcKKsmDrp9VXjVLmoDPi/NNSv89nrCiXvUTABlMxixbq/wi716WB2P87H2p8EnzUsOWNQLWDkgXFSAENZJBiUm9GjsPl7mN3Aplo65TUKT4KqnOWHl6hUVCKGOr3XW3KmDq64HRHXBHlJ9J72L5QkYMN23A4pw5pdVqIoUILdes6T0qS11lZDHFp67wJp/PrKd0oyQceeC1cA9chF2oec0INcZPFK9BABXCAmc6NA7oWpUGqnhBzO5zqGE5e/lgNicySaVQFCh4aNvhKkT7nRn4LyL3RQ1CWMjtxN4zP3KeUIi0UgJfEUvLIqJutougqrYRdgfZgvO2lOkfo9CVrbi9DOUKLMu6PbzuRvnt1jDAFcCz/601Mna/sAEx9VkWjQfC7aqNq34Vrz7fRB03RgQVUnYs10AleSVyBv3xnld5gkYZsifuLmKt41EPo/7HH5vpQwsV/mLSK7ACqJS6amMwWmR3/O4maCucGiUefMNYZHA5D3WIqm4Rz/XNdDdvy86dXiECgf+wMjCRO94HrFQW/tDfSXSiqfKeq94bgTCw0mRmEvxYV1+cRF/G0bXShX3pGGE/DqMmZBLVJ53Ozp4uWmvyAqI6d2QI5nRrb/aqYXWux/t/KukpKVyuQ/MHeqZYOzmaqE54REuRmBhpmm0b8zXiF49agSPkZBgjR7Fo/qtPkRimxFjph07+QYbDiHNk6MPSSfi8Ax3Mpo0+AWXKZhI3e1RdvPo1QCG2f3aYFIWXYfELIxfWVzViGDuQiviyVhvHLSkJIOwWrh9KBJSP+aXJUxgcw9s4X9rOgLZC9ym8l/t5zYitjaHRD1yts4S4CU1pFFhsGKLL0kEA1Vw5EyI9YKb9UwQ2LOo1xAO4f0VbuBWbfP446RohRW6zZXXHwtg4Nj1Nob+ZywKv6xOHu9+YSCrCTJT2xoDTXZKOUGgN1O83siAWunhfJqidH7NjiLREvFSvRMV24y1v9TjjwURXKwJztzVKSXAzRVWYxgqPJX29ncIAkz/Jab9N0YtXMGhuVrDQBtR9811zCK+m5DrTawuMDfIvlwXHpxhKUmZFZDHYx+oaJBbMNFhkkKWYp7/HZBFgzlBMZcDnzNChUa7kfJjOns0cg6z8C+JCvSr0zDWrD4WxgOIishR8zSplnlyjHGymgxIt3EzNjkRfjHJhSQ+lg5R/xv44+18yTjwGJCn4hJS5st4Ybw8MJ3LARVGoZ+BDvFkM+QCPsh+hyXCnZaAIpMTwVU0FjcHIOUqhMJQ5yf1ty9ftQMGlTsRwtkEMAOsMSWUQJ5kmLnUXP7R6gNyP72GYfo+VGRu6j/ofuGmdWG+VwIk5P99mUig3s0Qz0s9WQyR5ulwfqf23S6gfKtcQFqCO6GnOuPlbv0yP3Q9aQaFWSyoJ550BdbmXGK2Y68rY+yd898uraqsHgUBdzpbmJGon+bnxMTjzZVfKLZC3kax8abLqZQsoSN0MgVKnb+cSCPivkbRY/KGGwJ1uXz8TLbvilZWsTCgoQIfXhcP6JvHDxV3Ed2+ELYkVxNKCXOcmPll4oB4beuSnZp4eZoCGjkdYtSaeHQnMaBRf04QrNKqJrphlBUqcp80rtCItvTJmmiCCMLMTuY52uSX9YXfOF1Yn2Qo+VaGXO/r8Eu6RlFZcJQc+6PDiY/hhcNlgnCBKSzpLysgrtONdnCCDgMyz16k1x5+uHsUfQ7HIg6IGUkzxnACU/0XBNiYBNoNHhJm9lBa2IYKG5cbJic6YupcLMDrnN7gyVaJ9PIaz2TbxfEfZ14L96SheFCadT1tlt0KQ30ySR6kDLETFWH55ZnYTqBAFrGE7xmxEqilQZIpbuXV/o3tPfnFse7TMXGI5Je07V+N2AI+k19W+KX8dXIOqbJKGnX6I2JV876pu7pFOc9WQWLsNXNWM/5Z1DUsKzH3ypg2XS0yMu580WmxcOEHYdibuAOZ6bhivh+mbutTuLIjmVlPuL1sAuCA+EbkTVFC5cf1Ag854MWUTKJ4QsVUl/AMtRAeDpsNoYenprvN7N97U6h0mxhm+d0SrRlKv24Oa6xuC4HLlFwaZFJRCuehlrZCVlM+taxR+DR3lXQrBvG+kXebm2DJF/NBJSvaYqxodR35yTNq7qdCYDpqRaQKnWtljHk+7l3q4Uwl8BLigR1si1yRzinBkhdrhgOyK0aXl/z5fSeMH+v+Gm8zz0tqEEOR/QsczrpCJTXKxOQ3nFjFAWlLlJebklN7PtVZUa8yYS0T+bjMaVkVyQdpRC4BzH5sTJSd5X0oPzHwWii0Lqp8KBd9JhITdVZ/UdfjQL/AF7QPr4EN8vUuVy3tU9Uamb6o0/kv0jt7D6Y3jseBnXYWPMd8dzEv5c8+OZFOnJtQWggGeIM51bZ8yQPuZfZlSnQ0gB00eu7QX55YdNmAOyadil1CrahcExTeHKKSW0HR9B2wZ+Dl4eSquWmNqjRphqYWYGBrWufw/ZOsTXYzgZ0dw1hS81+WW8fHFna0TBMcFWFqo0yOCtqNWma13dopZsGGcDUkQhuzqXqSkFs5K87uNObzmw6JCnTdxxTyItNFbgdRXxvGkJygHzQK+kKNO3xuOSIHfymZ58fDBdfwb6NWDOpWnleKf9vL3sNSDyTxeI+cMpWwksBUd6EKrBnHQuLbYyDnqF27xn4WioQQgo/vh3XTEnTUdaIfyqoPgF0tB1iH75GDc8ug9GX4722cpt3Q++ig+SH0Hdiv1S1T79q/PjGIZP1JG4LQf3FeZV1NufJolvApkoUfEW3A81GDX/2zdWACJyYkotK2S1VWM1q1Ihg8HhcZkt+KPG2WTS4VX+NpVvASBZtmo7NJR8G0SfVqRr+J9otn30/2agmVeXCGeV2vy08a+DQxUtmSq/qW2ziri6LUCxdGaGQ2Vhajz2mgWY9XdeJRWKO+KjCI4/m3r7tagAz00gcnISGpFRvGQ0B4P10H5OK8jDVlx/PMnV2rKpGc8eGjz3nLYXsvO/n+b7xzhoU85O3hmxqp2SwXsmf2w5DQd1aN3e5j+RxEbKwzmQaRHFrvEDDlY8oR5Q6wCGLF7XUUiue54uHPOlxJ62TH76D6JafHoKCKqp/90ktnQFSqzeo9Is4CYvdn1KSBNehZjOhRad2TjT9pSYgLTQY51aErVe3WkVWpGk5ykJPre1nkOiQ/SjrF5UaFHndD9lAT/7x/e7zzPliH9xwZy6hQbYP4bdVCnXHjHNa5umfjHSUG1aw1A8He5M51Wmc7n3kuc8hF3S4Q2RqL4xjcz46Y3P/kQHC31h4HeJK/nv+mMz9wgHYAQb6JPyNfp+BRs88f3rlJ1VE8CcP4qgXc6vM8veDoErRslRRM5F9eZXItG6EZU01pqIuJuahdkyltSmjYIqeLY68DPJTvMjjOj37vdLukcLsZkZLjvoI+LGry7y7kBzF2VAbqndcKSQ2VN3qEiDChM21NvlwxzIbktj0pgFmWOm1m9tiszkz5I/kvZxhNTeA8clOBivV3TsCQpvnO/5RN3Y5kjNklytWv3w6en4bHKioLPDUkXPDE6tqN5XgSUgwyvKV0RCnMcbG32hnE7d5U/YKKUqRdjftoSSGHeQ5LwfqavhDbPWrELueeENlgEHj3l4Lb2AzCbvfehxlcTCjT80+y8DDkkc5HN6AWgiyvpStdiGrsSW0PX+s4r6BsLi75GC7p4TaVsByp4eIZZuHk9AR01hVAYBzwaZy0egnNwl2tEXlAjHYp6vr9KghkXtW7mNhQ5F6vJb8cUTvqLDb2n15v9eZwmVEPeO6EHwQzZLrVseBXLmUGt4xu1jCdsi8J/CVNILAtaI3j9uy4tdBGL2F7v9zl9JTSH/b4WvBpBc1+fgP00EQ5I3g7jqeQ+JztyExkmJgIuEAAL+dln+72UrrXYZU+AGdjyYDc+q8eHGIA/La01Aw0C+YRY3V96eoFBhXPO53rEQe8B0oDVJEG+I1U8pqXeAZR2CZYWLIfznbiKXXRT2z2JXOb976ap0Klvg9NryfZ15pfZ/w3XIQ0Z9m8PNzI7oyJZKJucLTch9nN0dbsbJBNNuxZiv+n4YAPioFRF2dwwvrGfBczC8gZpECfjSxdBHoBI4g6jAQPkg5CDzWfcKUIBE09OvxCdVI7uLpQ22pOCTtYhLD5wbwrWYTGM9Tkfd8stYdmEmHwSyKsCkQlstvxkaAep6dib4JgJLLpildsnWB0qZD+YTXQdhSCm+yPdtH7XH5vyhHgJ21l4WTqRzgBVhIlGmCvMJItHIhieiay6Cq9MlUUzSaQc7r2I565SrDnloEY7sYZjQJFJP8m9D8zdFp0lvGNf4x7caS2d9ivkwP2/95q2SELGKB6A6jQOLq92Vv3ReZwv4JQxOGdtpSDJlo55GOz1xyzti2LoYDldtbcS+cL4TD+/zdbogc/kuJV9fe3IBvlMUmfu0F+/UlUWBQVr66CGlcPiRyLs8PfShKQ7hOBIynl8nAJxdjoaDob6xYFwiyVUqPwN4Tp+eKZUKnFEv0jU5m+2o+vK8TPPUW8UHUUCy7NZO57gPKtYCFIQ0PTa0PShjGr2630QmXDcIUr6dLEYKZE2V9uYAizSfdVaWEXV/9iG21Bp8yQz/GxaYvXEXWBDaEoFCZ3nqPxzekS6bTQUxAKMDJ2VPADPLeHDFohRc9GNhueEc3re4D6T282VEvkS70C1FsYe2spa3gU6bA9yq60utxE+1LAxGdY+9ocjkMGVqINmIAAgeyZP4CWhn5FNh/Q91BDY3kn81Qj5WNdNZ9V7PTg1yqKEg4U/i2rSHAwxH8d78toydxfP7ftqOzR3OuRE+L0RduKRC7qY0YAZllDIX2xvKuiL0JhncaC4XpKPRQueHfJKGtvVbOZClHdfTIlnpSgowUm+j15Mls/g23Exu451pPQRWl+P1HZGyZ/wj1Vrkgu0nbwOQ0/UstJ+Wf2Yb3+eO+HQE/9lKjQyhvtzAFuz698DkGm0hIhzcWMEPnA68Plp8ny3P1oH3ji+6DzuReWSvzsvm3tS7hSKyCaH8q7TtzFbK1CYIz58sP600m68Si40tVjAqjjZt8L3IBfyAiwAQZaL9MAgVRsSMYDUfzXfKMzO6s9e2K9THAQSvNrQh/XdWM/0Mk3Onlco+SBEfcQR3sDKilBSZgaaMlHjNG44DwuDXdwjyEyVXMhwrQ6pr8i1EapdQvYYJIxmeZzpPcxPZhjZlTxhbWMAZLNwVR8eK8sajO6cNn+SIoPzkep9Vt0ylwgr2KDAXl3RHZyf3MwRMNDajbGoCXLj9WL3xZVCmBdTjBprnrPcflhUDpDDdg+whDU2oQX87xoWEb8er5An0DBRQf9RqJ1D/d0eyQwxkxxzarsvnVNtbiYBo3+NuoY2ObgFHROGroAPBCvEUlOR3h9qqdznvQsBqFKjZgaxKa6FhYKg3+y/TQjYMUKyXVUabKQBm4qJd11Ib4drpMsXZYgXCuiVmYJ4cs52dYWonUUGGI1Qcg94OD47L+XIUNb/ifGuMZhvCDLvznJKkIm1tkif0mpQRcrMiovHi4Ugwzr/nWUsa9N7mh+CIobUEcymKvvhS722VoC0i7RJ9KKG1fW63iHrSQN268c2zl8lHS+nCl+W6fzrpmcizGcGwiuGg5hcwyB8Q8u6RDq2cIRjzmphw2FizNEJjqAMGH7n5vvpYkP4WbsjK2YEWSb1w10CNwVfNfaf2gm/6x4RtVc8aeNOA85PYM8u8evS8gq9XeNo8MiKUS57wF4oWXwWSO2tcsc9w59FWRqxa0vHNwsEc5rd9StniIy+Vs8HydcVGSmol6aFZs7z/8GbfSsEbkRyDjp0mpNOFeLkbCP6AnHZ23Qls1fXxgIWp8NVkaNJdk7mhI46gh6HkIMXu4fGurbfbqm4FBm6eeLXDlzShiLCSVH1K9K5kPLq1wqxEiSv9GvH28dySwxVqu9iUzN7UboOGvmch6LtvpmVUDPNbO6KGF1MJdXJz2n3HxMIc5DclINqzLZsmrrjsS5UQvM+fdBPp3UMIPU1y36LZ/DQywqGjvH5L7cSVn9UVRluDvA55kAHWf81MnCq/2eMLD0UgTwTEL3SY5ubvAxANqq51M4dBPWIl/uD2jC2Tty/TdcYE1zdDcwZqYWE1GOSWsx7ORaD6I81F0UW8qzGB+mTGZwSFqQjChy+cnWVizc+ZkrBjPqLd9ZcSA9nvhzB7+XPL8lKpMEwb+H0q1yiSg/2Bh6maiyxkEhiu5dHQUpsG+f/WpOXk398FxvjiGsayS6oPPmSAcSQUuAUuiX9Z3Em38Wuh5RDQV3WxpwR2LA+1C5A9Tw59eUMDeF0LXLJLFbrnApdHG9IpkqR78ThhKo8MAh6s1SUbFKvmUc63z+t0H/+Psr+qIN+oScMCtGiQCGfiJ32AYcL5jzOLZMRc/votpzKnJcP9drLrCQt887sZXkakCdZwrx5JXU7coiWVmzhsfoCH80EGn8MtSCqRUZoElwfvu/N9qymO/Tmtr7Qr5oQpKe7IQs0W+gRq7OZkd32C2CCRopyghayB8kZN6cbMdmhyZyUVQ23S/K3eo4bjkMOh9jKDUlAjAIDNK2oJ0lk3A9aobQC7RQDfyfVIxv2Lavze/hOsNuYSZZyvX7CrtYwOfEmyCHjSHdheqFKSn5572iOR8Sa9B2CRaf5QcTquI/E5nuiGTCj7kWi5RyfAnvmocESmPXA+VcnD//KMUE91s9XDxTvrUrHVo/B3IvRPbtjycyoubdt0XEVI2MsgWW95h1uCIGXcWlq7sq3r2kqDJJ/jHlC8uf7FH4/GBBOLWkVHBjKiu0za8PV1sMd6764ZLjrnC1iMsB44NCc8Tj64Emrb6zZ0uq4Mo7HmMgG+euuKVbVz1ODCuraD4qkIZNcexQajJy2QVv4rcNSFCTCHudLazQRVVKSXi+1UMDhN8K0VZ9b5/9KbHCy8+l9Dl24a5hBwIf3W+/A2fO/BERM9K7JBbiHbzph41Cj5kFFxuH+6XqI0f7C/Epm/lIywJMHL984B0fNWOi5L09+E9HwJLs0FmPTmJZO6L64kGK2cBTJjD6MZYthS7jVUuXYR28WQanK2Z2N+m+Xr/mf2Dv1ngZUW24s0j8GQ1yeN3T3i+j6gg31hO/RpBtLOBWVV2zxAGwP9oFwJvmfzCqpvF97xGyZ4Ot+rdxh68SSvZoZSpJLtkqfSYnyekI3z4pkYy9SCyhoTCo2s3tfhg6zBoV4dAfAzfRr8jNPTvmCOFQX+SEyuNZcp4BAD+twINiwzqk6DTiFBbWip0IkbQAe7C/U2iF9OQ73/LGfZ9480LUH+E61dSmk3BH2sws5FIy8NBDMDbR6+fQAQp6UA8GbN7USMVMOQPUuibtZbLK9SihqshzO2foyW1zIXrDMLqX3FKj15p67MqC7Ob6FHsxZBrKz355P9uf9oWx2ZLzZNcrRhdaEQb8TQYHmog+Rf8kGTxhpiGIrCNkycRnOIvW2VCMpyjjX/QcCu7HmapyIL2YgoSwpZVe3Y1z9TMyPKPL/xp9xC/Ux58XG9BP/J8YMlMnuL75LkGwzresyt5aFAZGk/hYoekhK/zI+tFzj7rnVXzpEji5p+Mero/MQ+8zl+QEBN03u85cRrEoASnHYhCvhAcSJUkxGAfxHue7nrw+tyiPj+kBh1oidVbfMQ61loP4x87BHfHTrvpc1Nnj1VMIucC4QEILXzPeCCPKg4UqsjuvjCaNIzvamg0pjHeUjmW1dfYR102UBHvKczl7mZKL4lFEu8zidvp6tBxK7G3GWNPTInpeaI9x+ucwUX+5R4z7wbiK5usAPYujXfbvfIXO0He75aaZ5wzdpIXcxWBz+R+lbOB3HZF3MAkV1K27hI00gSxj0OfCmasdi2dJ3XqToNun7OBLoVkYEFqJjbogdpa5N2GuoEQC0vlOoUr8J8tosXyaEh3S1wCABFb5A3YHf//MQjjnBl0RV4C/5cZ5yL2lE8W7PM8M8Db+A5yELBrNyc9CVybPLvqN4ie4mleHdmSx5ONf4va4KGbmD2Ad1X4F0+7Swc3Vt03vRjWWYOPAOIPSTR+tg4V4EUA/U5QEcuu22yrTC4CsYbrNtx70eq8qcqtaS/Qo5QkQ0PH34V3BzYmAE5/v4p" />


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="h10FwjwUuWI/bUvwaxiinRUZm9Av6oQS5s5u7Cjz9aCEfLLY1mHakNikfJy82KM6p1jgBj67+7gJ5zBUovBLY3emGiJJZ1aJGgks6l1C3yA3TNa6mVxcP7ecpS76EfbxUX/DOGjJKaghTtiXtkDW4nSFSjRiwqJyhAIzDZ7AG6k7DE0xr95OJPgUqXi9H21HCk6CsMWWqIulYMuZRGAko7Ani7DTSmuGKx/OLJHU8K826UNo9PW69tV2lCUqaagmRgAD1//gZeaTQb0GoHywgHZzwMI+9J/cMvrcr1W7fGxu7xSb55EgoruaRrTBUjG+" />
     
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul style="width: 520px!important;" class="pass-menu">
		
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking & Bicycle Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/MiscellaneousDefault.aspx">Miscellaneous</a></li><li><a href="/otherdocuments/900/DMRCDirectoryforwebsiteandapp_8518.pdf">Metro Stations Contact Numbers</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width: 520px!important;" class="pass-menu">
            <li><a href="/generalcondition.aspx">GCC & Other Information</a></li><li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=nclsis35ZWYbHIIlld">Phase lV</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/DMRCstore_department.aspx">Store Department</a></li><li><a href="/DelhiOutsideTender.aspx">Tender for Projects Outside Delhi</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/vigilancegallery.aspx">Vigilance awareness Week 2017</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>

<style>
.nav li ul.pass-menu li
{
	width: 50% !important;float:left;
}
.nav li ul.pass-menu li a
{
	width: 100% !important;
}
</style>














  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/11/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit"><font color="White">107342916</font></span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    </form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
