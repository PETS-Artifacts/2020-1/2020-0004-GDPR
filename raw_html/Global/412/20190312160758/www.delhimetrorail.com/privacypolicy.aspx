<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_ibtnFind')" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="8VHO1RF5d+RmZo/b+UbGubhhml7jA5bjXlnsXYgN/C4uTSQbxDVkj9RK7Jh9Pf0yD+1/cLuzOiUfCkn4dhHaVzHM0fjBk3o9HhAwSrJFTSr/K9pidrSRgOlDQAfk2Qkt2QOVyj7206jr9HYC8JlAD23Q9ee2vxryicgWfGtZWlSFENcYSnFxBi9l976zVqDGshatmEBon1moOmPil8VHWBgZTjEWsjWim15z8JRd/1N4C81xdnybok3x78b+Dccf0bxtRlw0ownjXI0FmAgUQBa9NHrr9lX1E0uj0jdXen6Eh4zHC87ZBEHcaOnZ3p2SY+YsDOJy9v1OXZqSuwKtBIAvnKAURTwE+yNEwnoPq64eyGw3nLTkhga2noMnfbpYcQYeGXXyTOxSE9qLhqONdunffDp6WGk7FvWna5m+O0E9NCP/K7J2go0k3R0jjvxlR/gdSjIMbYYmtGmUPfkgh5vS9LuFTVaWYQLBSRHBKeuD6YVOUyax9QCsstqLj+Jyy/fWN/z2D0ZmdrduHicBFu4D2gB83gYtThwq+Y99I7SkrpjcdvD6VTEUfXn2DZyBHB2JHfnC1J4M0Qs9Eijx+rTbgeWqGXLYnOX0FKHZHXV6T5rps3+Bzi2iM0ZRw4YOe/F3Dotc6RzuyvmMYb0JJpW9UPia+ZAnoOpMjCPzIUZJ3YF0SIZDQFwteprc6K7LgkVTPc9V1aN2aOnDJUqdL11mLRPkV9OyG+Q/5/O0nKbdtBUx+dzuUYEf375hojPoU94mXcn05hfriHiDtugMzOFkN7xUbbMS+aFA8/Mqg+JRVKDe0hvM+3i8ZHtpf8lltE72R5lucBSUzIVb/UhOWcxPpPQaWTs2+5TWbvcAKoL6GVKDh0DGFvEO0GSxvImmroUu5lT3AkYgq414ZACjMnlmaZ03kLBH1bVMM7QXbSE9Fr4NEvbapZ4t54SoGwvKjJROso9et364fRRTtFEe1HUck5iDBms/pEtUmaepuCGsRqn/Cexwvc5jVf1QtZRE3CBwdZjRtikcv5nsuJZ3tIc55zFSM+7efWwjZv8WbEfArwYekctJBN2oHsiJBAk+Tll2xg0LonrMjM520qOI1O/5aFY58pU5wzE6GNPu6eDywPlmR35qU8ldqZacz4h9kBi5eGtqV5meMMGqzCy9qy4hzCZMnSa1RraCasqVG64hnWorH54iEEDz7HaYQE+5n9aEgbTh5BaQappLR0h+SRGnlnvi26XOZfKzmNYv7rxkKwZWJK5R9ifRDRWvkN62jtGRRTKoLBSnVU4JIXih9H2eKXP5RUZ5mt0WSboRCw5INCpko1Z4KRxa2dkhKS73tok3ccWv/cL5h0LobeoNmCMqyJbxls2oAVGJHESSijGBN72fz78OSr9YRn0vmKfRUv8fvb/K9DRhwzJ5AOMvoGXiTaZw4Zm/IUOA8MPQfKuvmXlQHW7naEQ6BcQni0hmfRPwSab8p4XPdXTPhedseZ6aAv6FzCoVHXWtHZovPC//FhYyZy/crppMLhhTd3NV5ntAhe8A6vUlDM0q6Fq5UTNh1zU8PM3SpZYWOoeV+bYFQoJMQrjuQ9GzD10fU2apcKsipoHZb7AkBnmQzWV0OItcPBfnW1d4sIMW9fcatWvGg2qPGKG/iakMTwO5J7ySchFbRgUtXzd99gs6vq3ZD8oGtCKd4Vl0BLHKJTSVdRrGpNezvrtLPL9Kqjf0vv8RyYc9DpIEW74B5FQhWr5PgAaZHFjIpA7tdbyfz8o1Nr9E4HEtci619Rn7vpUweW59l8QITPV5KvW9tX+8EhelarLwvXSbNz9Zy1g62mz4h81LQceuG2V5WO40jO8ZOowUyb8IpcoihCwV3kFJ5ozF20yXDsx1rzUtKhzLuKFURhcrTCeDhLxjgJaHCfyHHHtQnG0445RcAGLZoGRR3YhjEfRrlFA/FW6UhfwkYW2pqRAkHJZyiJwFSBK2zqaH6P1/LDMBjDu6MPn7sNdz+YA5HE/4NELjLHBGp3aMm1soxVSzsLVTKgmY8WwVksm2hD1pdMeUYpqX/h0G8qbZaHI5AwqDh+xSf2Nyc5f8+E+f0Rvy/pnkLptriUBm0p7E94OxfLHDrXC9Jv80f910S3AMleJpfF0n9z2517OQOcFxi5OKDwoERPzosWRBNpAhr+U86j1kBK+DLZPqAdrutRZxfKrevqQHUvLLmPcqEzVlsHw6Jl6ffI5IPAigbFsB417EHMmMuqCJb9te0lKqM+OVgPJndChIZavclr+m4EOBQzsN5DMqdncuXIwBru8IWJlq5pqrWW/v1rMwwtDbVAM64F0dQ95auaCse1q3Frfmz3G1YOKUIzyvLWrTE4HIhw+z1IewBLx8QoHLkaQ+n3/ilnQ+X7MiZxQe2WBjjs24HOhaaUNJAjfM6BLf4KsPlBaG6MkWfryaQjhtQv5cWfvARQIT9K9XmBo4NeakbM4ysga3wbzileEuwb0UNGZdUoCdl8CSboch3X80J0qizYcm4ikvEfK7qXLAnr5cEGZcC/4Ueq556hp3aStYJuNmVdF7MqIU3bN6QwUR6DPjcSpSIW6iYG+v7V8ip4DT+uh+NM9gH7yDwjSmeNqkJXGfZYsF6GbeMIMTciOJv9w3Q23az2d5IhvrPEUbxFcWEqZ08svu/wXevP8MnzB1Ys01NvtuWj9omankczPAhJVKNv7wWTepH4t0xYcYjKaNh0cJHR86J4lfuetq8QQ4EdxTf7Wy4VIGcLh/jp9ZuF2n3LOuTxfWe1tKN4ly4TQcSqNpSvff3noVwWKShpUO9JxlMMch63khG0CQoDXtByml8drR8jjG6U9mL0+FP90V2cTSUskUMJuXDviz/EOebt6t5FUC7zbtJJ9dQnWjvMQvVTysdcSvtCaL2sW5VPLR3hO5HjOVx136KI72veVvkz8jiTndB/I4923qTuv7XXjEdBDEz1PIiGMeGlFWwr/QIKdrse0JAd1FRh1LGVVmTju5RXqUV2sexMAp7ISugV81HbRe4vfFrrkWx4/LGal25BggaR4BYXB4V12eRsPubLV1igfHNe2fLBBeQl6U5fEB379SiIs3CK0tAuEuhKqOETShr2PRAoWp/okHSeEoHR0XrVmX8Rss5PtTzQZWULVZAr8njBJIsNW0pWG6rRfRADPkQd0X1eFCmiY/+HCnvmI/PNEJQjyamS+ZBp6dKJCIVDZSpI/5t8g4g1NZk8woZW6OT9bOyiYbdrtaQXalmP+yzJ8g8j/CM2jbzDbhEvEl+Rk2RZEjSEMv5aAUDQaPSFPIqmK8Iwh5c84OIqputRgRTDgky4b3SwwDmW0DtCAJVfw+f9GNoUD4bZjaXF+qnr/kjbmnPK1rBG6pbfCBKWqBQXeH2SbXpCsrjMIefrRUOZ3xoGnX0oUBNBDnSN5PuWsg92QGpH7yFfcvjLyevrpp6se/CUZekSxwkngvHfyUQdJnfJZ33yjyB3W6s0qi1HVQgGla3HMEK/QFwe07k13csWHjx2IP064SA5z0baSw0ghR7QSp59TKJMlr1oP3TjAdHycrwBCZ9nXrxmZE5iXK0sx3FTnHgtLe4BuONkXJCzkWjkXUPPkor0/RZ5a24Andtv7HtbvViaO6IHs2NUMTdqoRrT51tC+UfSTpInswe9ZKDW1G5aKJ3z1iWKcxuWA86Bn1Cac7lortZGJe/Q3rR+0Q+7jl1uLNnS6ZQmixtaUXvXFJplxCG/xqAxcpCYs0BhBMBEYMrmq4TvS8CqNW7R1Z+RXXut6qt2ZjE5nIe1VkIQ7Y4W+e1104leN2XB9dv/H4nv8FiTbAagpo3g9jO1RWvfHMm5qZ/u1DvfZgu97O0/oXXuTJyCNDRvPgR63xdyHNjwtIUElynVOjvpjcQLQBF/hAiBmd/mwYFfyNAdC/cdd4yRcDOhLcdxa3ulYh5omN9x7t9bdp684XBrLNvCMUoS2Hwi+xd3ExSZbNR+U0GG1RpNnG0ymrPN5mQXflw+MsNv8QBitt585AmrtI/nG0kRjsFstH2WDxIXsBqTQaJfoZeg57v/V77Il1169hOfORcJ9bFIQVF7WUBfJeaYAMPcHxiFk01G00EzMD2YVJ0mxIK+4nnVeJSa4ov/RgK7FyEWFlnktxgPkrVDP55hS95n2BgtBf4ssF67qDROLvY1QP2zUzxjTDzRFD8g6QvIK0+shIA7jS5626hrjPgHG+syiobDK7oP0yGVGFSbbkcIp253soMMCDrMucnE1mE7zcWm8+ibKtc2H0Q1luFmyz9eC33hI2Tfi3/zQxd0BU5LuXkrAPCmQV3obAgXcW05qfbAD7naS3uYYI2Na2oNyJM+sI4L5gISOSUggQmEz0z42xW7NBUvpX1IlRS4Za3ZDeu+sSZxGmBRGT+Gl69TrO+3mudmW47EdOuJJxbYTBLjYQgW60bpsOtW1NCRg7zkGfXpiB7hrD5svhxS/C6jmUGeJ3Zx0ESUqsRX7gQUu79QQKeOg12Gei3KbfoGXn9r95xR7Z7pDdL0MaP5859a94824i6xhUO9stHUWXkgr+vms778oC264vyvgQoq35MZNVCkvyAWx9dZDmQ61poryH4Ga7bG2yFr1JGXQbxygBsdquxy1lay05tc3pwkTG/kcgDhD0/ZwyWFfTiLdIjYL6KUXD5igPOT8eAA/pMHDkeotvvMx5pYEO2HGhwWlTKYewgdEEwVvkTNxKQ0ayT7vlti86k8uqAW+c0njEtBhbaJYyQYqYrF0h2kDw5wqYjIKvMa5RvQvENPn1PwddT/sr/qLILDcNBRH1AHgWJvCBm4uikd/XS6YN3qz4xHQWW9OqvyzJTWpufQxZz0POBfYTl/O8qtqI/sOmpcB2GLUuGPJo97Hf1nA7lhbRy2HDrTyJwRSt+F1a6DFPLTbB2HqK6IN2UZRMsD0DvCFfGxK4Z8CANwEXauObvb4GNnRX5LVtQxHwYUWu4lDgmngcyMx5R16wdqvnpSrWNY8kjV2mZni8XJW+ddBUld1RqBQVcuk1HkINvtquX/OKh8abL8dBWifQvvnEikgERcf/3bvkK4XcVR8uI2Y0J5MqpXXyx6OE96b8HVuKZrVWoZn/wAOUyJF3CJ7nH0u+MSI0cKUkTDdClpKdGYb3m+7KTg/IZVttXkh/nWBA4ywi4hDd62pAx/mcHiVFSJS76EI3q869RrtCCwlRUhOEKN+/Fx/UWs94/2hNavTYCjDtvsxYv5lKlnVGND4RMIoX28FTtrMxdSedytRWkExqrU5IonYeaxzSWp51q3gnpOLrZsRGWs6hOGKbqwtfhmwOeZ1B1+d/f9umDfcG70nolZVSp6QOOLImTEDiJLaAdEfR2vz7s532SXwpANOAKojwqtmQQN9wgo0ijsvxMz3iAYbWXS3z/WJ7jfcb9j3iGumCZCQS3xhAKrUzdNNwzpCbyIgk4nIdF3Ojqvdbw4rFS4WxRdH8537WNllPgXV5+h408fd0Kv6Lp69DFqsLz4+tUYXpJJ4dpmwLNurfKpkkuPFw260Z8Pj6GeXGqjnzELbctQ38bCLEdXv9bsk0eWRQTgcyGOTYtpkOX0ZMwS33QAv2C95k6MoJw5cOxR7ApEQuUvgo32a9m3Hd4hUMgYM24t1ItB9v5SUHAObYFjW0QeP27GTVa8qMerKwobMssBcHKPZwnwa/Vr7iZwZWYcWSz98aVevTHqb5iQNpPA8Yet4Q0RcwYgqOnMhpJBvA4/aN/YneOGhsQxNjwPzEGoMdSFDi6FpFE/h+XmXhSkrUFiMU9wMa+rVh3G/ACc18guSsG7bVtCDJ9a2n67mQjaTEiQ3bGoCHbYZ0HxjFE6RuuFqEX7yPz4Twg+Orz0ssiPzrJc2aDsPL6E/SdwyEcROyzbItj39eyjpo4lxi3QK/FLQIc5W18vY0RDYR+LOdSLn09RhT5vc7gM4qz2J9Tn29JhG2t+RQOF3pUrGMaZp8vmMgxXXwpXUE2WHWpGSwWlNVw+d+nb6zKIWgRPq9izSqtKJXGb0koBsgno/wk7cWtOkzK7NM5ZJ9fapyyrH5k9t1rz7GJ5NbElp9ySBBPVVvPUihUy8uLKB9PNVv68Tae0aheIFePX7YIjBI/KC+ioTzTC4q+vbKm08+YizWTsGZyKfPHVOALk31aLXTFej+CGk2Uj8X/Xbt50QTy2XePATu7fw6rtSngJk2Iwy6nbgL5lSm3CTy55FmJTlUlPVzYnV3uOFnNWOj2k4B0QYl1duKTnNOsJaWiKj0+b5cFjpxY4jM4EkCRD5tAS9AF7F4c/qTH+kOnvbmwHhq0XqkQSBW6usZGR0R6A5KpT/fYuETOUokGV6GDZGpdL1nIU2P7d8oiQF/22BWrZPtNndeOdwhgi5PjQzKQPQdzCm44843YVfjmSQF/NkjiMNxDuXdDh0eFSStb9pSMMJR3E3t6b6W7p1jm0RqAotQ/ePrcvLuM6D2pFIK5ViJ6hbk7j3RNuZ5qCiYkDmo9Ps4r5FhRLdeHwManFsiEZ2c40wCVvqBza0NhCxpEoqOFs+LEx+RgwfqrivsNcXsApapQXIqabfakEhWa0BsQn7nfYvnKy0Shq3iNsP9I5mwr9Qj3rbmD5YfHmL7Ou6wRUTQigl/bcGQ+XKlapgQok1jjRHe9w3vJmc62yoJoSv+wd/lEEfZCFvZlvJXJR4vLUlJWiyoQDXEmPEffq1t5sto6xf2WzQ12t0DgDzNb40CICTzI0YpUYjOB9Gf/mpI4baMs5THRy2iXzLORH289xKR2JmhOP08U6h3aSgQoFyP74i3V14IXuY1mhCfu6xAx2HKObqtSexzCLLKzFH2NCbmAVBa07aTJr/IwB1BbYsdnzOWRxWQREETWs/UCH5MUAERdIpCpYEiGsuERSMkSPkHpqdNqx4nEzJpqJSq779LHLrbE/5qANA3eMCQrvxChSH/XtzZQPTE80PjupwZE+4cYzjz0sAGLfLEgvClqFp9jVosMcKBWXpMk4UjQeWiLbv1nB4RacKSwroEUKnVGOyYsNY9WFPu7Xfw6/RbFVGqM7zwA189AYZ04F1aLhcWiik2iPxlWPecOUP1i70sXUf3Qd0s3MzIkAOK7cPSbvh7h+8v84Vpg0heuS0IlttGGkn56hr4plGiBABJyBspTxHxwdEAhL1qmo5p1zyWQZIZ5diex6+EzWlOXB2GSIC2m50Lggsw59++QsczKAZZjkgw6zBpvxMVYb/D2KXtnchtJ0x0QnLWOX5takg1n8JZrRy97yu4PRLC1ZDFnlhsOY4Nvkh9CPpj+JNEquixnZO2wKV/I3FJyEIwJXCBzkixn7xlhmgJSCu7ObQ6+uxvhEyLtJf+oLfsskTYBv2czqas7aCGqrdTrfDDdX4QUcWxqt/vFikLLex5G+k8ULG37IO6c09c6r8n29lhBMOdnvvht2TPV7171ITk7LIeYQpY6wSKXfIfT36MZ0+2kIJXV5Wl8jcTCk2hNjqpvme9/bKMZBjPTxYTRV3nrb8S3Q944CFnGY0QwUjPMffmVN2ZFvwykGVHjseJdI7LEKdrs1rnGenu1kO7nlvbrAcBzMV0OXk+bB7Zoa2+gHxDpQO6LpJRqRrQNAi2OHsL0oguZSd94hCAT04A2r0sslEvaM22dF6T3svWO4+D8q/BFraI9iIbsuNz0HdcXDE6ECGXFJ68DxpCDZ2+BX8sgW97+Kf0Icj1KCrG7b0abnJ0RTA1yo7fqA2vLqDZ/dwCMzCkdCWpfltI+ijKRhrdy4cOoKX4aGK6SdF5ZBkGjhz2YOJ5cNJNNEtnYLD/SFCDbj1euqhsTpx+NtlzwTR5Hwtb4+pEIhrIgoR8ULGE6GWND4lL7ucHy/BLCjTB3r4CTz1fsYCJ4GdtYmO2yEhW9J7xo5Rm3iMOAJsE+5GcREOVDrOgovHHyUY6osx6tp9W2PpbzGrledQZOI0eJNhaIovQlMZQpzHQHCBlLptEIAbISiP9bI8kjpRr3Od3XJbN7i+GP5Zl931Wj7Db9aCJbR8YCsB74qvSPUmJv/S8tkLvUZOuLoNtLugmemwXcAmgERFeZq+hAMMcsJD08BUwK1xiN4ccOc+S0Sz54KmECp88UelfGeAltFqf79WfTHN+79h4EnbxRtECGTcCPcx5QcKSIJAMJQHJVgzpEwvWCJTi8EaG6kkdSu+pFzGGi87veScvoTOVwSR/VorxHKQY2r5qIS9UTQjXVpm6T2G1i0eI/0xPbE7mUAWG+JgGVsRFbdfgiFZoiZXXEhmpPJKcMPjgPY9Ryr+uI+IgMl+YQYLf7ag41OhEGSj2pGeC9wga3+7WHsGKHdro9dHGZoQ1RDrZktMRm8yDwEIOkrYZQtKtrewa3SmUhJNjWFsTWODh4U24Lk3bMUb0BwQ9ubcuuBWP8OflfuauNOwe3vEBaAdUnF4LKD9KRGbnrUK94Km+THDMj1+v5CqvYHrzuug6R0pfAdMncek2ZRzFd0OeBJJT+jOaHsSHFKJanYKSwsS5k5S4MNKUPHc492Dro/nTLcxqSEBMdeNjMW5qgaDcncgXGlwlrGSes3+QGX2mBYNoGY4cCy5YC55H+a9uxz40LqkO6kfxX8r9LFTq0R9ak614CcONfs8IpskhAEwIhpRdsqIvzdGCTK3HZ4YPF9BIJv6zrCmaUi3ZUsmhPZWDiRsJcscJGoxDSbGR3ACFMVeawULL+lBbyORIkVNIQe++qHtT38kxcdHw7LL6hoSyn/G8fs83cn+eGNvnDTkx20RO/u2IprSZ18pHKs521HfLbV7/ZsjBaecLmhk4CKfBy22PS2LGN/PKTkLSNbP4bk/cHXs/9aQXCzh4Kj27tGK0Yi3xbFcKFPnWAKTZ8dh4WNkP1bdcWpLEbjMxwyKkJewQwRWI/c7aIPGVaNa4x6fX/D3AwB1snmew2e2SYnT8vtP6di063kT2UJgN4SZWLqczbWxWR35qdq5dg/srLv057ippDNZNeWx3sX4aQRVNh05i5DbK0xXFB+a3L6XFYAPofvtcIMifZgevAlO1rVIGGMMd8M1PV03dQEzLUXD6gTOxywWYMiynyuxVdPjPSkkSUPOL4V38mRZCsVavyWMAuryaogwr0P+3B5MbwNNoSXaSPoEs4mVv7/6eu3EJrt/kcOc8QqoKmMIxFYG9lh/xxucWJzcVOJC789zNTTZqYFAegwhK3gtNb9WhZfPYlAtapfjwYoD7FKywSdG0jzop09f57rBTKGcy2xLeJTzmg5Ys+0O6rhy7r3L90ngVTz8ilvjTq8dDKTU44Kra4SnC3aBznfHKoXI8q/6NNU+WQa0D1ZdWHZiBGE4UZAT19h1Ob6/YsUnrzhFpw0zN7/0bX7hZpSjASs7HapAoz9bVQFEA5XeuYTMpek+YJt5e2AXnLyCq2vTIdWB4OCIED45T0TTgkEO8D98TwPgiE75+LRgu84IqVCnsuV+BqUbJviTO4igMSxrVkYP867bH2ucxys/w6c13JEvF0frObY4tgxazMdaYcuXst65P39qwSqP9OfLw8dQxpze445h1rpHW2l1LbcU=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=wq1seoLbYo791RQSSn6NSVK-67iJEJwazTkvP1TQpR_9yGbWA9Jc0NQlspnVXCub0eE6LTM6FMckSyDi-vhnYhg9UB8UY4XV0fs5prWYdD01&amp;t=635793215982061497" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ePCCHQpCHv5F33XcQdsUoy5f9tnkOoQXKllB6tFJm5ASM8qJmhAqc3FVznjELXCSmeOF8w05XFywPvPutUgHcoezQhDp7seqBnjKA-UbBL8o-Xdp3Yf4iNGB3sD6ZK6iunAQ1bR-7oaexhfzID4OwpgxIwtVEG71Vo7ilOV7EA3_NJrCul7JTWBQ-2nhrMJP0&amp;t=72e85ccd" type="text/javascript"></script>
<script src="/WebResource.axd?d=K2TryOXIxCuOZdIV0x1rzlHKX2EanrtImP12nVxqgDeuZu109iBbQRTgluA3wov73HCHC_DzeRUR6RtXvVz_UmOc4TcYD15KGHRKZCSs1-41&amp;t=635793215982061497" type="text/javascript"></script>
<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="6xnyd1qOAy5LnAfRrDUQJX8Y4tbaVLODQNodgSrfwF0y5vTcE4G/NVZQatU72Jr0HRdxCkRb5PDjp6UIZ+DgUH11BbpmeSBWL01BJcYmKzBmMutc8D+PyQvqoGI2sNAEsECFI5Qz5aBy/97M5l/bIo8RJGDkGibsAjAq+Bv6M0yqGgpViaZR2dNPSh0jEEFpIbpFGgQw5e88yb+BrNPzxSU7xSSEHYrC4Y5srhiGsJ2v+eZcJojAb9cWnPczAG+UXS/tkUxCUKC41U6ITQ5qrCmzueBPia8tFWt6WXX1mYn4AeUVG81g6so6hcwDgOyt" />
</div>
     <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$script1', 'aspnetForm', [], [], [], 90, 'ctl00');
//]]>
</script>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul style="width: 520px;" class="pass-menu">
		
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/fare_fixation.aspx">Fare Fixation Committee Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width: 520px;" class="pass-menu">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking & Bicycle Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/MiscellaneousDefault.aspx">Miscellaneous</a></li><li><a href="/otherdocuments/900/directory_24818.pdf">Metro Stations Contact Numbers</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width: 520px;" class="pass-menu">
            <li><a href="/generalcondition.aspx">GCC & Other Information</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=nclsis35ZWYbHIIlld">Phase lV</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/DMRCstore_department.aspx">Store Department</a></li><li><a href="/DelhiOutsideTender.aspx">Tender for Projects Outside Delhi</a></li><li><a href="workcertificate.aspx">Work Experience Certificates</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/vigilancegallery.aspx">Vigilance awareness Week 2018</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>

<style>
.nav li ul.pass-menu li
{
	width: 50% !important;float:left;
}
.nav li ul.pass-menu li a
{
	width: 100% !important;
}
</style>














  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/11/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <!--<li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>-->
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit" style="color:White;">120892739</span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<!--<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a>--><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span><br /><br />
<b>Follow us :</b> <a href="https://www.facebook.com/officialdmrc" target="_blank" style="color: #ce1a1b;"><img src="/images/download.png"></a> <a href="https://twitter.com/officialdmrc" target="_blank" style="color: #ce1a1b;"><img src="/images/download1.png"></a></span>		
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    

<script type="text/javascript">
//<![CDATA[
WebForm_AutoFocus('ibtnFind');//]]>
</script>
</form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
