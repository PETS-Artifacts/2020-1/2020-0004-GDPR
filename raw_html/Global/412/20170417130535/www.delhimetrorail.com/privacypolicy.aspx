<head><link rel="shortcut icon" href="/images/favicon.ico" /><link type="text/css" rel="Stylesheet" href="nav_1/style.css" /><link type="text/css" rel="Stylesheet" href="css/style.css" /><link type="text/css" rel="Stylesheet" href="css/menu.css" />
    
    <script type="text/javascript" src="/js/popup.js"></script>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/script.js"></script>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" /><link href="css/style_new.css" rel="stylesheet" /><link rel="stylesheet" href="css/menu.css" type="text/css" media="screen" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <link type="text/plain" rel="author" href="Master/humans.txt" /><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" /><link rel="icon" href="/favicon.ico" type="image/x-icon" />
    <title>DMRC : Privacy Policy</title>
	
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Delhi Metro Rail Corporation Ltd. | ANNUAL REPORT</title>
<meta name="Description" content="Unique feature of Delhi Metro is its integration with other modes of public transport, enabling the commuters to conveniently interchange from one mode to another. To increase ridership of Delhi Metro, feeder buses for metro stations are Operating. In short, Delhi Metro is a trendsetter for such systems in other cities of the country and in the South Asian region. "/>
<meta name="Classification" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Keywords" content="delhi metro,dnrc,delhi metro rail corporation, dmrc career, dmrc website, dmrc route map, metro map, metro station information, metro timings, delhi map, metro jobs, dmrc jobs"/>
<meta name="Language" content="English"/>
<meta http-equiv="Expires" content="never"/>
<meta http-equiv="CACHE-CONTROL" content="PUBLIC"/>
<meta name="Copyright" content="Delhi Metro Rail Corporation Ltd."/>
<meta name="Designer" content="Netcommlabs Pvt Ltd."/>
<meta name="Revisit-After" content="7 days"/>
<meta name="distribution" content="Global"/>
<meta name="Robots" content="INDEX,FOLLOW"/>
<meta name="city" content="Delhi"/>
<meta name="country" content="India"/>
	
<title>

</title></head>

<body>
    <form name="aspnetForm" method="post" action="./privacypolicy.aspx" onkeypress="javascript:return WebForm_FireDefaultButton(event, 'ctl00_ibtnFind')" id="aspnetForm">
<div>
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="5onU8w5NDqqJQ4KvpjG5ZtFw0Yq6vbwQz/3ZAlI3vgOReA509fYtwGi0nDY2TZsTzFKIQDYiHqgFTXBAEqx9o16dV/vnnS9zM2h0KPZeyLXn4JEsVrxbauZYuvFzZaTQId+C0Fb7kSrqkM9LtDk2OGmKCO3fBS8CFXQM++hWECYkziz3QcwEnMFgXjC3wZloZTPWYcqLY6OAe7IrYLrSJ89Qczd4og4dIlk6zq9OohT3HMhmoGLkjv1QA1oNviR1i2l58Qr8IjBgNwuet5VxbOJmpSHGT5bFd9/HV2smlAIFlK9bQZ7ql+JPspc6UZhbB4UgJlq7ejSeRrNVgPDzxzJamHkrUdgMWlVJ74GXaC6YEhLto1/mrpynQInYrYpAxnVBM18hITYkb91Ru+C4kpzezduFsBy7CmHnI/AkDYTZ+eromSzrcOWgLBZ++nApZYug0NyK7ZaOUiyITLLh8U5FLTLQOaQVCJISeXHE1vviTp5UTXFfa+0IBfvoYFsmy+A2zT/9nUzB19QoOVTpxDjI/pkcDzgRKsDxZZParJdVPBrPT4zJctoJD855lXGFRnvfIDKFP6Vv8oAZdf2b84py7DBdUvgJkcS3RQWWQfVXL8ig7t/khEYVv48NmMOJika0Xf8LWlBvHdf+iWoUpAG0hUTH2ToTwg5ggo3aM7aGmr2WWMwQZcaY8KF3obVa8733rl6wp4Iio7q2B9+8VejZ0G2M/Iq+Amc21C0aIoLLSdBFVLS1TYUFNApfiBYtLv2gB78owy9dXLLLc/oey2emk2o6d0nkf5kyTb5nsxIE2oLvSjW5ZoNAWrJwUJFR4oE+iiOE0NZPelIP8/gY1uU2y+oz51oBbOZ67anEbddIlJopUoHqO9J2rlDYEfzTvUleTnfdviQs5rPndyQiwffpIAg8MaxCSoNjNT8kPZjdzNqOehcRtIkdC+1vGrbqe3iOCUW9ia6ILwsaGLEYDOHDtCnqlPzk3eFyFJywptOTZEM3i1rgLJGTHAV9Pi26dujbnSd9MEt1WgDUmlIyEV1EVJbWbAp51VI5bttWUsK4q9S59WRStehQc78TwnTzHRR/FmhvsDIrznVj96SR3x+R1cZAaWQYNMAO++XaG4849d7uWtJ1mKosIygQmaT8nhNO5xqPkh/+Ca5URq7Whr1NtbgPMtJA4o6GEtT1JweFdAA1mFCW7A1fDFqYeeeuaP02UZ9NCg7hgFRvWL1pPMI3foTtm1lBZMXsPnPmJwTUaezin0hQx6yDWxaxYDu2bZNJMLII8VQMaRHZF2n5dGXMXk8gZc9us4XDnFaJJqJ78jAkTo7/vSZ2ki2eP5lgWwcQY7Ba2/DR9GVlSMk+I94gJI9wQ1oKOJB6TjCM8lrB+TRZT4UNcC1NZkDedRsP+8Y600l9Sf90fxtbSzwg5TCgtDzlL0PkgG/mbjJfqokuHqriWDwTDwuL94odpGxGqaEMo7iiBiwsWUBMiA226P4BupzuCrOcFYgGIhV7Jck5Aa/VDw4QL/lF5KIxSP3xj0/jZD+mdpvOM+aWcB9ESfPgYVkQTdzWkRliOAmbgE8pYl/OjkXw0s/54emoZLNYHiO1oTOzXS+PtaP8Yun7YDJI0Je0tgGMEYYswLPmq932+9uE/x3IOulBq/nFyb7yyydCTTzLbePBlNQ1YbIj9VSUiBWh3rB9sIjDVZSz623gCnHvKDI+EEQIUTurtdc5wTENrk2nzeXc926TxlJwLtocvgY4t7S4J/5ig8uDUj0DHlUD/Z8HdZiAwFDHWvKWzPYG9GcyyVKnKBF7FWqXmJSZSMYoCyWOCcTJ68XEmByT41wX2OfTyDfDd16kj5gVEuq+xlIZ+Ke8TAMuKAfm5zfX46I4EzaiBFe1yjumWuki4aEm4WaBf8rlPjwM9/WCR5EMvLa04mRvFDXDgykCWWNkcvm7vlfgwVrSX1AAhulRXz8D4kTjp/p2TcLwDgCPEi3XebtudPIJttfjYfcnYSa/+uyiSSzStrnGuCnX3g8iJ2B4sakqc5dBCuyOap3FRBK5iPFXJwTIjjcYu2cQqkdxFL/btJUMIRLOwklyaY6LFLSXWNjMjZ9vl2Z7X1GpUvX4B67vwtek03dfLFtNRjEXnrOKRGJO6h2cWznnmt0WLNZLoKGTCWV/TKrEJfraBZU0j/4LkMaYIRZYjWqjigAgsRSOFyIXktTUXQkj+9YOV1ANobmCZ3xr2vDWiUcRNOvfc4rhuMKPfBRDwpF+UB5sjQbE60EsmUXFB9QwK5fWCSHa2TED+oUvVo0zABQQ0vgBPZC1rhnz/x9naefLTafRSjez5GE7ZhMpvs64fOSeJQYquh0dpX73yx/QHADUKMhWar/fOkX+9Ggk12Jk4NHeKVwFwjJrVo6kZKA4T2pM/7hHLGWXHHOK2GRo1lN3Vt50E6AVuHaipIiyA3e6fzeSMo7WXaa73UI2nC7X8zIfOAHyi+8WrpqVXVh0wZAqfs3uelgFbmIY6pFfIcjgVTwMgckc/T/JZmKdzmnqvCKn7P9q3L/r/3ZorwfEUOHHhR4kjcGuNeFujiIxKmDxxv42DwlDbZPzBzOal91hGKHeRXcj3TL9PPLCx2o0N2OUxQVaKegl8uyQ9A+gStCnd5tK8CSWoYWR+QTa//0NVLedSUyN7cHIIMd+f3Ws3nAuUDpekftpk9DmsF3F0WLwo+rkrUtoK6RfR6m0xtL2aPQA937/QByulYqlLQZcDozAONGISabeQqSRj4nmMvt4JJnqBlQlnufIPKTGmywlz1OzDMlH3NVL5hiTZGkQxHWFSinDRbIp0PvIdZD3jqrPFVFZNX/NO+fduCeP5pKXP8Wi91ct0yr7Bw7FASAmpz3LrCYhgDA3g0nlmREk8hOkRdsYhx21Dicvf+UJS+wgQjXzCogEWWj1eicQ1c2fL5pZN9PTb3L/nyFRnR7osMrtF8WMZYau91zmuqo2MFKxw5lTAjU0yW2/xW1DMa1/exbZZtjb3ogB3dmTkG2CfKGjQCHtGdMXHxPjt2Lplliq8a8/uTpqleL6CzH0jHX2MfMI+ug9HAQsX7A329tJWQyJoewvjJt4tX4lvGvO5gGsggjhzz4B005jZ0TnajHyiEGMXMdKuQkT3tRCckZsEqDx8Ubi191QtNSuSfpk51Spw/malQ73RhL515P56CbPJDEt/VD1ZfkGL68ZXZ2m5yo8Gb/YUdM6cYk5PYclZAJ/CEsm6vIBYnftT3E/e9ezMr82OAWUV4iy6N6CNm5MSyn+eNMo7XD5NaQ4Y6UT24RKOJMGC1PYYyHmgqs6OKGQbsGSoCt8nofZ60QYSzLUz2rkhOib0CNJHa/S55T+5a1+xsSy1Rpi1aqiVNZ52lZv7KHIHUDijlPp7Fah5jfmiG9lxyPBw0BmHPaF7pnrQLgTJ1s8dIuD/CwlScn4nzb/ua4PHYAd7v/8+vs+zEm0raE+h1tt8EbNsqGbZUKRYCdvlUpMOm7oomNs8wBxJ+cTCq2vNC4UH5VV91DnGL5k+trTkaeFs0l49uxoP8PefyJmf8DQClFJ3b5i3fZM3ZwHGoIt4vJyaG9Idm5jLfpxMRygRZiykN9Y0uYH58hhh6vunu5twrSdc/FXlAzH/DKhG4MOop2VzePu6bT7K5maNKJ/CbxYARfcTXL40JqKQ4T+SGPP8pLhQE/Ws8x7LqEqlhgA7LhLZIWlPFbEqBqTYFf3dX9AnHJgjHbBfE7YED9RVWN5o6zcyDBkdAlsG4G8yk8aY/KhMJ2x1x3pKFKGXj2gfjPIah1JZF1iR4xWySctsgAmUC65vbCzmkbNcA7tbOPEm+zKmiWnugPZ/djSibVcAEaTHmzzLZoTpvbYuBDZAJNhFCkJxxH3ACAIAGZ/CyJwbK3JS/vIoXfUmpNayfX1ov7VZRwvkDTk8EwGQPv9aroiTl8mrve4EOMn8IRGBbWAJLL9ujLzF3602aSMkLhxPzeKdUNQRjYTXd8pW7NiesPjZWIYOJUoEuorLP+sv6CVgEVJiUKfpaaBPPMrHp8GHQG/PyvurrQY4cZUNfdOTbSrr7wTSiPxwmW+mZ+EboSxIGBL+WV+NXvCuZwLjm8zl4Ec9XQex8cdrtyn6/ntZ6UZB15/dC97DBp53W+UM/tLvavxTYbTXJuToMv7nHNDuwLbYJYO2LIjYJ/t/aoRBm6gU+j2K9Oq18P3sK0BnS9p+hM7Na5IbILhCNpeo3dujgF/U1KvydjL1y5eDSQvdi5B4md+2hRxoTrL3tm+rEOxRE7nwMlVPXYw5NcTFEwclmitDA8Ji6F5SMBxfS4oA7dm32yXGZU41aC/rEpvEfHIONx49XtaEUCOAyQA5GLwjSehJFJ+7qagnK+zVvM+2+0FEJsKovIES3YqOJ3yly9NbjI3Odmi0xprh5GTFC7dtWrbiKDl3jWD8r6NQkPiKqymfDNhMDbN5NeREsexFilRAFZWV7m3SFANnWnC5ZO8OpesnmHY1owtgrGqCKFgFeKbspFtDZsqAfO/GwYJqzcqFetKZqU49mcqzXiJdepQd8mPLKQzwg74pvJ0Xy9DL8SFKC0k9LnazMN76zz2btKjvM6WaKjtHfDoRSnZlP2ybCO+06XuFCKjYYgJaPUPoLmV/ljB1NkjJ4mzLEhkWym/FcHTxRetu6nHOaS2rX1WCVrMNjQXWLxNDBSngpHeKrTMVsMQNaoZ9pXvSTcnqPw3xeNijGWYdygKYnpOP3TySpMrZS0NMrTnJkVJpO/D2Ha2F558mBD8XinjuPFGzBlRm+hYLmgcwQ/C2ojnXAGpn07zyqIUvT3ool+bPvnDQguKcmMY2i2jgfM1kApdYrpqhkHjH9rTin7Tx93ow3xLIatfpiMF5RMyAIWA1s7FesZk2g9wAcjIkgs5z/pQSMYY1/XncxTEG4qzizRrQqUKFKWFdxSArhKFK8UOTEnkcxXCsylo3Zvesb/6YL6lkP/wK3QpIJ0qCHemgiYIuItDRQU/qToe0y/0W0TgvHkwLax78wGf16HfFXlB0kDu71cmP0zg+AfpE+J4+Y5unMqAfm/1HO2L7vnuGI/UyR7YQUTVCVGtO/R+ISF6EFSsvTXUkLNTP7v/TtfL6vs0Nd1H1J5Rj1d2osPFqJU/zAExGz63YUC8ly7/0X3ik7T97TaSgF3orgoY9rNkveKWMKt05J09MzTe+ry16BQDCwWntOt13685A8QNeDg3CT7uqTCfPJamg/uFYG7pbqtRzG1W4Wk5dU29bJGNZWw1IACkW5x+xGrS+JfQR+VBzXG4uPN3tMvf2gPyGc5eoKLz18HkKHCHm3GEJ/X1/HJ6shGOjQM6kNcgz7r6yj8BQ3vS6k1gvMZQaHXgv6ZnqtE3aSmsDCqPFYSkb6ULr7pKG39oZaHuXwrVAk9AKY4299Xj/R6vYTKHQJHba9cdfKb0heD6Af2ZhHrWk1WTJTR3Gr9sEnmWMnLz5Y0Kj4GK/fLS9SsCSl7Iru7zKCt6hNpUF6HUlhgAbdQ1FHpN3f4G3oKpACvjbA/R1gh528rHb9oZMbsm/lFoaDIME4HnY9AvYSOiSF+uQDeD9YDSLFaFPGe56BKIpg7EVEJn/l+Ndwx815gMqMv3RVWJm21Bw6NHQ6b+KAwwBV3UQwd0i7tPD0+dnFQFH9D6xws/tVckpOcCymyNJoV+3EbkXhcPgfwhnK+OQzJKzzRNx3dnjts44zvDyB7aoG3HNfBzXzU8p0LElnUtxRHLxMMVQhQWJEhJhlsxUrslKRBiRSevozEOYTcFJ1s2PmQNZhKyE8ZIDDgVmu0M/YJZhIPqVXCS50sjWQqi0SCEA03I8JYs/u2dfv/O43CznqtrH+wRdCK5Uie4now4rR2R4X/jZeBMIue9AL7anY0Et4CNQhqy8ZZNzNS8yykGh8SaMRAbU4rGiWif3zFBI+dR4cWYjRNmsnWXd6E98BBDo1jbyQVSipVq/4Cqiamv0uB4OwgXf8swXD3LwZgK8jo7zJiOerv4l17yYXqnHuTXdv6xHR37pas1twVqsdAsxKoJ+QrQs64f9iBvLeqtTmWSn+Fzw37OzmUE9FL0lgY4MXKQXv5L7wLl4F/O8SZVmAPoXI7AMubvPjR8wJWLC108I2hpu+wEr/lSCEO6Kr9yKN9Q1B3b9dgY5YdHmWUkc9ZDHUB4lTVTqitOGEs53UgOaVw+1ZNl99YkN/eVUWvmaS5lMHeITyheQJm6dCdLAlLH/fHegguGXJ+yb40cTlSW/GyKdM9f/ZVyQ8nV9j08HTBDSqv4Go4eh2oLgajEp6RTII8h71jZADT9Bcz6I+PPDzhrDIrLJdmVyj/s9xeFXPCJq7U3rK/6DWVBsNdiV95z0ykoxio0RbNk73FdWFltbI3AtNiEiE9f4GCOoZSrGlwUPEdKxP9awRQRdMotV0thm3/BVurKj3wxuS6VOALwf83OaxP/PLxv7k+ENXZMV8nHVu9VQm2UU+8Sl+0L3TKfwb/lu45DPm7uL1YR5gtJt+nCzD+3YdCYnW5RE0zbsuubJ/eldR1EMAAvdhSIhO5WhqxB17t2Uwj2WXHfRok0MKreapXsmSzNTH3gHfjtkpXYq6tUJGdGbItwO+dIEy7/yQ4Q8CoYPwyl7+4MTq19kISCNvUC++dW41IkWAvWnv/uz8gOY+6UQBiej3DE+Gl5mozrR6J1bn5ba7x/W1wE4U02ZBpunk4YsFPpzu0EQnvSIewnyRiHEAnmgZ4gliLenzyRF+7NsvmB+b+Y8jCbFUbYGRAKSm57SxzppTOLqcMx8ZRI/345hyz8jK/7IrxIly2RarI29enPdA3tD9iwc9mVlz4yP4lFsalr4W22WoKQxCXeC9H6kUA86bOfoP97F7TjdlqczGbRG+IZaCOcx3IGAy9Ney8be4UsK/p1vQAmzA2lkfNN/iIXiSzZnA4gBxQ09bFjMYCBnJNjXIO694qnoMxUBmpQYcAcesN8+KSSZ4hpTVYhubA6gT/oEEmKPXx1FoOyFM38p7vkg5VPpgdTcmRcZWyjdEHYquwFdk9lRXDRgp6OInZpqwFX2d8jdqwBx5MCgOiK8lJij7WrST2Kb2RP7gfJHAVRRr2IyipQx5Tpd2peYvlG//HCpcgqi4R0akoR8VfEce/XBjsXYcjUJzjXP6YJ8gzDjGInkDd5K+c0WgKoXdOiXb2VoxWYRytkNpRc4bKZ3s1DwodLTxVPcqyoH7OKVtg2vyffehyE7ozsmQAYE2njOpCCra2Lc5Wtk8+eFYg8QaX671heG532OP723HiZvWuqFV9mdxKTs0aPMZpuW7yygJQ2h9IDcfwLz0D/6jCRoggHHXHbjOLdFSQj7gcz7eVY7j1sSRebcQWFjwFxiwwOil+nBYFBtYDacjbQW7/ET4uFbBK6Q5LqlaGc79jZXASMIDvb24wg9SaTDywY3myZveTqPLblKWyE+pef77W2dSsMJa2UqjUbnQWtZ4lAr0UAfTIzd626WwNfdZ0dqmGC8wjQE7ERtKI+c+J6ZggaqQreSNPjyjEfUm9Vf7VPWhANoAFUAN9EB6g44A+FLnf0keQ3/DvzuV7gU+anpBSo42KR8HdPtkiDHHJSarzvc19EUmU84RXe/9GZrV4upnGA2A1u718hkAfJPDsbsWleplZnHbKIdZI+oFg0JyUMZdI1Vut4yU+3Uj3apOBIEReZHkWvgkUbGnKgHH9T7qtlXDSISX5842A66CHj7fH9fJZvm3cF4SwjmfiyLM5wqxE3EuUZCpogMVY9/bXzeXW1nkfXkCw6+iPIGNlITwPgSrGjgeeYteBvYu818q0+3I47IzeQ1FyDf2b8nQY0D1BVIOFU7Bep2b1sH/mQv0oEh7iotNlxhRt0UlG70inbB3QTqGpFyjoPwlQoIgQUedVuZZbGEAiRijoyvFQI3pKnVRfrxO/WluKa6U7+CtUrdDlNEQu4yWXY8PtP7pr6W3MVs80rHuaCwQAAo6Tol7iux6wQ+nUzbEIiXY7UkCtZkIbMSpBiMxcEg6EKWADhTevSMYnMEinY0bkXnQ9Ds/kHvlgezwx3wC/QnGvdCIQKjPBWH6ZLO07rdCZluucHsrmDCLxjoIWzhpSqeblHHS55vEkJCQ/oZ2tQMHAvByZdRTxjt7Cn+HiaVFTUBAWDqSic0q77BhhYyFMrfF/kQEO6ksGH7s7Q6TFmtaLxrm1rnU/PYCzpFlr/gsMRHEgkcJm1lGgjPoT3BhyJleSzE8PnosBpZKkwgg/pbZB8U4yiUvIFigMzRCaCZSLCLpvLkU7JJ4clVzdkg4KwMMMOsMR0m7Vg+3StOft5uj4s09TQKXKZZ6Ra4151xqULEx+3Yr8hL5BuE+81lPZf7k5cqi+0AGauPyHrZxnPmJ7ARPDOWStsuBR15nCMcsB0aTgm0v/S8GA2/ckJpttwoyh8TYJNy7j4ITAAYkmaYmL13o1a5uxGSTLzXCk+p6ObN7xJv0qf7xhRoQRL3vmqe+i0tMQNqETyeI6JmozPDPqOSMcBG45ijyxDJF15I6I6QgHyyg/MB3MgOmgAepiit64CtCCoi7vkmRKPjt1VvmBbn/xmaTGVeKd56bAzgK+itLrph9GnUpCMZVbKPOHPBjOjWLQDZgYzK/TKlfEYsuG+lkpVxjLcXPeTS6fADmD4SRCNCmP+0Fi3B75TARrXn9lXP+L6523FI30r6iyWcuSL3KsrhP+cxaBSUeh2ZAhNZfdYH3f71nJ4kvYlFgHSAFxStua3m6OcnBNfyDg1SQ84NoHCChpQ3605wdwlVL2Mk6YK6/E26IRFm0+9sBkVNI2XEaMuk/9CEEu/dC5Dt8PxmGDgT2/Xo2co4xD5pYgf6iJYiiCEcOhwAag+BG4JFkGNp/0ny5t4/bMCLaZ+dqQMcwDfe9p3WpCNWVm9tyKQNAvPU+36PTouYV20qo3LGOp8P83YiJOoyp3ETRaU/UMbdteURICtLoyLTaKFaDT/tAhqRFfwdZ9kQEbd/kJ7qv5NGPm+iiV1K+V0WSC3AYz/eUekgjQuibUHfYF86aeK4BnwWTFxjIC/3lfN9loB8Ae/j/24SLUZfkJiNNW2IUrQHSN9NcrNke0MgVAJ06j99jQimTbFAHTgIAiCT3LmCHpuBv6lusoXGjzSMpbOY5gilG0/AQLsTZg2l9A2IRxu7CtXre5QOJocqgFEjxu8iSSNgCdFbdA2D/qulW1WFDqZl2YSbdQ48qrtKyBhDW63K306do8aXTDaLYSk39pqpHvfHIM2GwT7StAwATZi0cvq9WBdJXovvsy6buso4uf6/fX3//f9MihaT8DdIC+rWA6XtZEZSudDvWxxIcw+QnAZ6DDFLhS1CyHCuexdK3sonGQBOr0J8CKh86SqPdSBL+vAkiVJgdLxXA8vAfDfhxzlRctXlENPoD8+0PpZ0rHjWN8gXBlpBRlip2eQv6vZIH3VhQ2wtwZ/zuDlqtQ98rE8zy8CzzJ/3kYFFEOrWm5KLx7SarUXgxenYhYlnYPk3NFWag/7oJj1MgG3XJdefYpbUI6SDypzFSp3VQUTFN1EhOZ38Xkkp58lxygzvzhsg6jonSyHMd/1cbpEGSVqJmo/hMvTV2m6U0MNNZF6A4XYdHDbTV1Haru8BIcTK9DQr/pqgqoCPl6dUoyZejRq1BYuVeEI5OAghtIf5jzN1JcNQOKmMxL1x08yFsx5Pe23v7g7B51fhovRa+ZASVc0xC5q+aX+aeNw+qm/cDvbvYs8bPAF2+zdOAj8XsiKhG94MeVfABH5tgvllwIuOhiBOfIhv0HkC0U0lBw+UmdDM8pr54jDORSQ3pBa6bucHAnZgGh5kYvNSQEzSE0C64OdUbSfkMKAR+xWijLj5VNIYm0+L+kC0++BsYHx9wf+kYOkYT3CEL+JV2PoN+C3612pQgGcJgJfpx4+n2ZmES2mGIGvuANAN77NVInog1dcYoDSDAmFcj/TlZ6pihiUTZgZRycIJ2RQnWG+C3X6/G/t0WMziiOkIOxiqfWOVR8ffGA5m3hvsRWaELisBO+PKpMSl7gS1DLKvXVB22aa9qUwH0UZeLY8DyZa0Wg31Iy0YWKRJ5+rWvMo0DBRmMxjD8vV4Lc0TjkSHvkxxfC8TpxfEboIGFID5hwDcICGHcJYrzgoVn0oE2bE+5SIVtDBA7oH5OHIN5wjHu879fAwRVeihS4lrgLo8gKrV0ht8WJ5nMzZMvWRlSps/oBuFnjAV58/GlBbrSHO65UGJfp3NbkszFnHuqiB5098pOIOHFWd/tTLDAuJ8fobI/YoMI3atoyLmwMcDHG+2YwrfK5fBUGmA0AyicpcFWiVsF8e5vGyvqH3Rt9ndjUhEi0wIGJIy1/TLxxjXrK3tHW01c0KzBXBf1ktOoFSeqtzRN7CzNKUm2Iu8sIGodyqqqfhEbImPJA1j3xi3m4kY4wcA+b6MMPjDyEnPWK3vUnzKYMcd5FL+I95YZOegAwWcdOjX92CR4t/gip9e4mhTbGqOtWcPCOtkQgGYj/SMyLVUj7+tvo2066/SlXcUpvnAXS+53Fn+NQHaCKWFjAEpM1ZYmS6CPpZ7hCIyNYau5ganb+i02AfuEU6AIzxyInKfI0jX+troNlh1QuMeyuAUVz1/qDXmDbaw4pcwKb1+aarTjTTKA75MmyvwWAO0y6VDFpvQ0IxyUfFeetVr68HpqZVkXLS8oM4qVnvYWOJkgxbHB+mf2zEo/Pp0uvs3XzJ0wC/e8AgvPsbx3HpiwKUs3XFboqWacez3ASntEfmBhZaXVAle5OX72ieRIkGK7UMv4vJxeU4lRWI6Zxzr5n+jj70d5818yp67Y0B27wOGJX6wNG/qm9nXGBNJO9Kj/HzyCaIdvh54zSoynFYZXQMAqNsDqeuj246VPPxR0NuVW3M+ZipHzQY1GZvnGOb39srRGmhW/9wbI1fWegYsK0+WlNy5qo57PabUD9Z3cHb06cOBKs6vHlhRWmKcKKO7ZXXKi1gx5jm7wzhD9dGsAZgNmx0PVdY58YEfUN+/aOBp6z80aPR+sj7HTU5AGx9tPQonmGO/aA7zD4HpjJ7oZdc8rx5qxyl9YnYzDmLnq8HAtqfsL2OFlkz12pfsT+pp+bLwQuZXSClRNpvy1XQfPG73QrRnWwDDKMuE24ix6kV2mfsC6/A9zxObs4W0/8VF73nDEUL05AWx+KQ7BvmRjRZv/apVUhS853CjkZZB5Fl83xiUSiqsnMoQZZBekAalg1R7iozA+DPfkBbCFWHeJ+3jCglHR5tBFsfp+ycKAdPI7fgP4wr9gDEAeuXAY1iI+zJ3cNtPsSFxCiZlXlP2DG2uuppw7qHd6SaIA/fITNyhdKjac9dZc9dW3FkVnHI0FPkrRs7vvyuoIkqu9X5VitGZbW9R+EywLLz5CBdzgqePngxl/AqxCZgHhKOzhhycbSdLW/nwNwwSrPaE4DoSTatgzOgV2OOQi+86fDeffw0Ih+c//BqzmmwOefye0T648NwWB3f53XsoKSfUUZuHUMhg2hNQ3Fs5jds93FqwNnBHgqZSxK781ASmlhE68eV8cVueiBrdFzEXzMtheDFJINNm6lZUcyxd9VVgiZuyvAtlo0MNhP54G23RHIsHXL1XPUAAImnC/Kcx5u8JnszAMqN1Yj6SYai0J6BP251I/iSexQR9CiBzCS472G+WToqyK/uv98MnMJhKVXREVMFWifA3unWgoMkB9MQ8MgCpZ38mzcXZuaeZRgkaKsiHf4Yr4O7mGtd6uSzGYlOL+u97hyXJBSLsPE9As1UdgNWE+zB25FAi5oE9yvY2eoKWq17Z9rrohjTqXKzNn8qLifaSjYIfCEdgcYjqdByqMVf8z65KUTmC1OsJ4DP9sx/m1/Edbx2JaBTCKfDHoF8HXnb8/DGOqJkqLAH/GFszinL2MoDNGHmsZI5Fgm+VmK2mrInAzrp/Hwegr9pKW6vT9Hwh+ToEYnpsl4atl2gAlAUqJuD64hHWdVT08tFzKrl9CqsKH5SwLC/uIfbyeSn59oz6kIlXNcMq72V5pSZQyIBJFbCfyZ/ake9IxXUYklov4BwCf0c4e8ze8d+EZQ0XsByDl3K1JD6v6avUoHJuWJc4j0DtDOFq9a0FSffGCyLPOe75BrQ2mAGUUHmdasZeMvfr1YiGoboTvYe5IM3vlR45QE/lbpgkxTpg0EaTczM59mzdzBWPEgwokpQ3PApdQ+Hv6UnkM7yxMQBM11Nc1CyZ0vHwqM6cz/9zvpdKRByeM4NKqKqsxaAdTsTTqJfLR24jGs2EMPFmQXGHlJgNwwzv16WBgpOt7kVrpYD1/U9Pa5faTQ6Qhdq/sts1BsoUrxseMbzQFwSDQISp49bE1KBF9aXV9FG+Nzaaq5qL1Vn6fqn3o2L079BBRJJw1aKFbvFVhx047aXRQftlMNa+ciLSBR9rZRkfW7BkJb5rjEqrjcpWn7OMUquRtOia/ai4yCU4eCKI+KJv8XnQE2R/fqIDurnQbhyS6fKNGUjsUTI2u2JQFrF4LAWYNiv+n1gytL3QvFVOm3RzqS/m/Nv8KoSPLWrNMVXGtzxwbEKgjNjYF88HpyVQBcLjuXq6vVBbuD54DF+QfCFRRGuRyKP09MCPE2KDvrfgYwJOhjeNezH8ddPpSmDjq2n+Bit4wqnRMcOD5rOJYG3j2BvDRiU+LQnFy+qgr+gfr/3haFDdmFBQc7JQNT78NbEX5z1G/70TJyNO370gmhG6+uMhtW7pAfFFieYLJEHbbnMXV/56RYfg5nUpRJr9VJRs+DYmr1wjWUM4JCobyJ3EIA+kiJb2hstCT1Wj6uJkHoSNsEe3jWu62BQNQ9JHsqrnVITt8OYDuILfGAavkEc2mdPgxZsXFIh6r2Sojf/tWX8XoLPfIRtc0D001Ryhp5+sq+P3yOQxFdr1phgfKBHiSuaGiJD1DRq1GFcbUudhIfhRbcjeWhT8fr4pi3OXvWme9KHO1difiVsGzLwCThK+zkNsv36zFZ3mA9P8F5NGrxAlq8o+ApkhGckc3kQY4W/vOTYK8ZMUEhudS+PbHuMiDQp+gd9ZNt+Z4fzqqGiuNsKxvf2zOhANKNjLKmR5bpL80VmJSUm3QTdU9KcNekMu/GMdpZ8FgZliXZ8lG1Fc1sZseTuiRfZCBoG/ZBCBEHMXiCk+xuCXsmZdCxfS9MkOrcgdK3629R9orbkWFfRxGuAxhpWx6ealojDi6zIIdDCcZ4TTXcpV06L6s6PVTNQggpDZDlUIQJ03r7Q9bc0hVYf8UohYQRqHi7AGAEliD2Gs8R8hOvJt4N5BzZ8HyWujiswi6fWquHRPR0x7G0ftGOhPCDCUOPYDrIQsLpYMGgIv5oycgRBkLhu9zhzotIA+LhjFMRNNu3QI3qSZINtpRXS" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=wq1seoLbYo791RQSSn6NSVK-67iJEJwazTkvP1TQpR_9yGbWA9Jc0NQlspnVXCub0eE6LTM6FMckSyDi-vhnYhg9UB8UY4XV0fs5prWYdD01&amp;t=635793215982061497" type="text/javascript"></script>


<script src="/ScriptResource.axd?d=S0uXnpj946ypgcCTcQGRp3xZUGeTlo5ygiMf70nTgU26rh7UyTT-kqFSLMJrVwsYi9JjE9M0uBK8twDxrdqG0lNFAPHwOaeMka9IswnFMYa4I2vYkduzHaMkHsClo3cBNNRu2s4qw40OWyIeDC3PmTE6gaU4dSfaM5FAnKwVx5k1&amp;t=72e85ccd" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=ePCCHQpCHv5F33XcQdsUoy5f9tnkOoQXKllB6tFJm5ASM8qJmhAqc3FVznjELXCSmeOF8w05XFywPvPutUgHcoezQhDp7seqBnjKA-UbBL8o-Xdp3Yf4iNGB3sD6ZK6iunAQ1bR-7oaexhfzID4OwpgxIwtVEG71Vo7ilOV7EA3_NJrCul7JTWBQ-2nhrMJP0&amp;t=72e85ccd" type="text/javascript"></script>
<script src="/WebResource.axd?d=K2TryOXIxCuOZdIV0x1rzlHKX2EanrtImP12nVxqgDeuZu109iBbQRTgluA3wov73HCHC_DzeRUR6RtXvVz_UmOc4TcYD15KGHRKZCSs1-41&amp;t=635793215982061497" type="text/javascript"></script>
<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="7650BFE2" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="gOEb45uR3iEdm7C1YsqUI3iqMEeyt0zOTP92F75i3OozhgbhTwXzOeKh5yqqmM8qkwpfPplRisdpz5olV5OtbyZfWcUP+ArzfZ1gqhuq0RM6+hbLNyLblGgSiJJR4Owo3ylQanCxwmOBKvHWqWnrv+UpTCbXNoEcF0+ltOIcWqQf5f6dZyx76UCXqumsjq9hLgXOjgfF4Jy57FO+CnZqrJ9ygUBxiSkMzRunysddXTiMsk85Fkana+8k3VCY7j1wYXreGMMi8kT9aYIWkR/fjkI1yA/elAVvf69LDewEy77an7qyNDL9t6SONbqed3VjR2hQdQv5yPkWtlX4oGEjhQ==" />
</div>
     <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$script1', 'aspnetForm', [], [], [], 90, 'ctl00');
//]]>
</script>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 logo_1">
                    <a href="/default.aspx">
                        <img src="/images/logo.jpg" alt="Logo" border="0"></a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="socila_icons">

                        <div class="toll_free_call">
                        
    
                         <img src="/images/call.jpg" width="156" height="71">
                     
    
                    </div>

                        <div class="lag_main">
                        
    
                         <img src="/images/language.jpg" width="84" height="45" style="float: left;">
                     
    
							
                            <div class="language_setting">
                                <a id="ctl00_ibtnHindi" href="javascript:__doPostBack(&#39;ctl00$ibtnHindi&#39;,&#39;&#39;)">हिन्दी</a>
                                &nbsp;|&nbsp;
                                <a id="ctl00_ibtnEnglish" class="language_active" href="javascript:__doPostBack(&#39;ctl00$ibtnEnglish&#39;,&#39;&#39;)">English</a></div>
                        </div>

                        <div style="float: left;">

                            <div class="search_pos">
                                <div id="demo-b">
                                    <input name="ctl00$txtSearch" id="ctl00_txtSearch" type="search" placeholder="Search" />
                                </div>
                            </div>

                        </div>

                        <div class="change_srch">
                            <input type="submit" name="ctl00$ibtnFind" value="" id="ctl00_ibtnFind" type="search1" placeholder="Search" />
                        </div>

                    </div>
                </div>
            </div>
        </div>





        




<div class="new_nav_1">

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="toggleMenu" href="#" style="display: none; color:#FFF; text-decoration:none; width:100%; font-size:16px;">Navigate to..  <img src="images/mobi_icon.png" style="float:right;"/></a>
                             <ul class="nav" style="display: block;">
	<li>
		<a href="/about_us.aspx"><b>ABOUT US</b></a>
        <ul>
            <li><a href="/about_us.aspx#Introduction">Introduction</a></li><li><a href="/Top_management.aspx">Top Management</a></li><li><a href="/DMRC-policies.aspx">DMRC Policies</a></li><li><a href="/CSR.aspx">Corporate Social Responsibility</a></li><li><a href="/enviroment.aspx">Green Initiative</a></li><li><a href="/annual_report.aspx">Annual Report</a></li><li><a href="/DMRC-Award.aspx">DMRC Awards</a></li><li><a href="/useful_links.aspx">Useful Links</a></li><li><a href="/memorandum.aspx">Memorandum and Articles of Association of DMRC</a></li><li><a href="/annualreturn.aspx">Annual Property Return</a></li><li><a href="/sustaiblity.aspx">Sustainability Report</a></li><li><a href="/dmrcgreen.aspx">DMRC leaders in green and clean MRTS</a></li><li><a href="/ciimetrophoto.aspx">Green Metro System Conference Gallery</a></li>
        </ul>
        
        
	</li>
      <li><a href="/project_updates.aspx"><b>PROJECTS</b></a>
        <ul style="width:200px;">
            <li><a href="/projectpresent.aspx">Present Network</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl00$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl00_hdnID" value="114" />
                    <ul>
                   </ul>
            </li><li><a href="/Corridors.aspx">Phase III Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl01$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl01_hdnID" value="87" />
                    <ul>
                   </ul>
            </li><li><a href="/underconstruction.aspx">Future Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl02$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl02_hdnID" value="115" />
                    <ul>
                   </ul>
            </li><li><a href="/funding.aspx">Funding</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl03$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl03_hdnID" value="116" />
                    <ul>
                   </ul>
            </li><li><a href="/projectsupdate/consultancy_project.aspx">Consultancy Project</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl04$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl04_hdnID" value="7" />
                    <ul>
                   </ul>
            </li><li><a href="#">Miscellaneous</a>
                   <input type="hidden" name="ctl00$headerMenu$rptProUpdate$ctl05$hdnID" id="ctl00_headerMenu_rptProUpdate_ctl05_hdnID" value="113" />
                    <ul>
                   <li><a href="/projectsupdate/eia_reportlink.aspx">EIA Reports</a></li><li><a href="/Relocationandrehabilitation.aspx">Relocation/Rehabilitation Policy</a></li><li><a href="/Rain-Water-Harvesting.pdf">Rain Water Harvesting</a></li><li><a href="/Airport_agreement.aspx">Concessionaire Agreement of Airport Express Line</a></li></ul>
            </li>
           
        </ul>
    </li>
   
	 <li><a href="#"><b>PASSENGER INFORMATION</b></a>
    <ul style="width:260px;">
            <li><a href="/Zoom_Map.aspx">Network Map</a></li><li><a href="/metro-fares.aspx">Journey Planner and Fares</a></li><li><a href="/parkingfacility.aspx">Parking Facilities</a></li><li><a href="/stationfacilities.aspx">Station Facilities</a></li><li><a href="#">Bicycle Facilities</a></li><li><a href="#">Toilet Facilities</a></li><li><a href="/womensafety.aspx">Facilities for Women passengers</a></li><li><a href="/differentlyable.aspx">Facilities for differently abled passengers</a></li><li><a href="/feederbus.aspx">Feeder Buses</a></li><li><a href="/AirportExpressLine.aspx">Airport Express Line</a></li><li><a href="/mobile-app.aspx">Delhi Metro Mobile Application</a></li><li><a href="/firsttimings.aspx">First & Last Metro Timing</a></li><li><a href="/lost_found.aspx">Lost & Found</a></li><li><a href="/Metro_Securities.aspx">Metro Security & Police Stations</a></li><li><a href="form_guide.aspx">Forms & Guidelines for Outsourced Employees</a></li><li><a href="/commuters_guide.aspx">Commuters Guide</a></li><li><a href="/ATM_details.aspx">ATM Details</a></li><li><a href="/feedback.aspx">Feedback</a></li><li><a href="#">Metro Mitra Initiative</a></li><li><a href="/FAQ.aspx">FAQ</a></li><li><a href="/eartquakesecure.aspx">Dos and Dont  during earthquake</a></li>
        </ul>
    </li>
    <li><a href="/knowyourmetrovideo.aspx" target="_blank"  data-tooltip="Informative Short films on Security, Commuter behavior and
Safety"><b>VIDEOS<img src="/images/vdo-in.png" align="absmiddle" / style="margin-left:5px;"></b></a></li>  
   
    
    <li><a href="/media.aspx"><b>MEDIA</b></a>
        <ul>
            <li><a href="/media.aspx">In the News</a></li><li><a href="/press_releases.aspx">Press Releases</a></li><li><a href="/newsanalysis.aspx">News Analysis</a></li>
        </ul>
    </li>
    
    <li><a href="/career.aspx"><b>CAREERS</b></a>
        
    </li>
    
    <li><a href="/tenders.aspx"><b>TENDERS</b></a>
        <ul style="width:250px;">
            <li><a href="/latest_tender.aspx">Latest Tenders</a></li><li><a href="/externaltenders.aspx">External Tenders</a></li><li><a href="/tender/Default.aspx?cid=Gy9kTd80D98lld">Property Development & Property Business</a></li><li><a href="/tender/Default.aspx?cid=IT8bnclZM5cBAlld">Civil</a></li><li><a href="/tender/Default.aspx?cid=AxYp2sisiBEmklld">Electrical</a></li><li><a href="/tender/Default.aspx?cid=FzMnclfd2o1oMlld">Miscellaneous</a></li><li><a href="/tender/Default.aspx?cid=N6bP2JEr4WMlld">Signalling & Telecom</a></li><li><a href="/tender/Default.aspx?cid=NYB7R4FBBJQlld">Track</a></li><li><a href="/tender/Default.aspx?cid=grsGMsqnclzsclld">Kochi Metro Tenders</a></li><li><a href="/tender/Default.aspx?cid=3AGNZpFcFwwlld">Lucknow Metro Works</a></li><li><a href="/tender/Default.aspx?cid=PmI6leadWTIlld">Rolling Stock</a></li><li><a href="/major-contractors.aspx">Major Contractors</a></li><li><a href="/tender/Default.aspx?cid=yGEt7G92BVAlld">Noida-Greater noida works</a></li><li><a href="/saleofscrap.aspx">Details of sale of scrap by DMRC</a></li><li><a href="/awarded-tenders.aspx">List of Tenders Awarded</a></li><li><a href="/advertiserlist.aspx">Advertisers List</a></li><li><a href="/Kiosk-Policy.aspx">Kiosk Policy</a></li><li><a href="/Outdoor_advertisement_details.aspx">Outdoor advertising locations</a></li><li><a href="/DMRCprocurement.aspx">DMRC Procurement Manual</a></li><li><a href="/tender/Default.aspx?cid=vlVO3VPqpMAlld">Kerala monorail</a></li><li><a href="/blacklisted.aspx">Blacklist /Debarred /Suspended</a></li><li><a href="/tender/Default.aspx?cid=QEsaDPYJZWQlld">Vijayawada Metro Project</a></li><li><a href="/tender/Default.aspx?cid=KBJ9oaGbyh4lld">Mumbai Metro Project</a></li><li><a href="/otherdocuments/EMPANELMENTagencies.pdf">Empanelment of Advertising Agencies 2016-2021</a></li>
        </ul>
    </li>
    
    
    
    <li><a href="/vigilance.aspx"><b>VIGILANCE</b></a>
        <ul>
            <li><a href="/vigilance.aspx">About DMRC Vigilance</a></li><li><a href="/vigilance/functions.aspx">Functions of the CVO</a></li><li><a href="/vigilance/complaints.aspx">Vigilance Complaints</a></li><li><a href="/vigilance/contact-us.aspx">Contact Us</a></li><li><a href="/vigilance/media-focus.aspx">Media Focus</a></li><li><a href="/vigilance/Vigilance_Booklet.pdf">Guidelines for Metro Engineers</a></li><li><a href="/otherdocuments/VigilanceMnual.pdf">Vigilance Manual</a></li>
        </ul>
    </li>
    

    <li>
    
    <a href="/Training-Institute/"><b>TRAINING INSTITUTE</b></a> 
         
    </li>
	 <li><a href="/disclamier-influence.aspx"><b>INFLUENCE ZONE(FOR NOC) New</b></a></li>
         <li><a href="/rightto_infoact/"><b>RTI online</b></a>
        
    </li>



</ul>
                    <!-- /.navbar-collapse -->

                </div>

            </div>
        </div>
    </div>
















  
  
  
  
  
  
  
  
  
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        closetimer = 0;
        if ($("#nav")) {
            $("#nav b").mouseover(function () {
                clearTimeout(closetimer);
                if (this.className.indexOf("clicked") != -1) {
                    $(this).parent().next().slideUp(500);
                    $(this).removeClass("clicked");
                }
                else {
                    $("#nav b").removeClass();
                    $(this).addClass("clicked");
                    $("#nav ul:visible").slideUp(500);
                    $(this).parent().next().slideDown(500);
                }
                return false;
            });
            $("#nav").mouseover(function () {
                clearTimeout(closetimer);
            });
            $("#nav").mouseout(function () {
                closetimer = window.setTimeout(function () {
                    $("#nav ul:visible").slideUp(500);
                    $("#nav b").removeClass("clicked");
                }, 1000);
            });
        }
    });
</script>





        <div class="main" style="border: solid 0px #000; padding-top: 8px;">
            <div class="main-in">



                <div class="content">
                    <div class="content-in" style="background: url(/images/md.jpg) repeat-y;">
                        <div>
                            <img src="/images/tp.jpg" alt="#" /></div>
                        <div class="inner-cnt">
                            
                            <div class="inner-cnt-in">
                                <div>
                                    <img src="/images/top.jpg" alt="#" /></div>
                                <div class="inner-content">
                                    
<div class="inner-content-lft">
    <div class="inner-content-hdn">
        <img src="/images/hdn-lft.jpg" alt="#" align="left"/>
        <img src="/images/hdn-rgt.jpg" alt="#" align="right"/>
        <h1 id="ctl00_InnerContent_h1Title">Privacy Policy</h1>
    </div>
    
    
    
	 <p>Delhi Metro Rail Corporation Limited is absolutely committed to maintain the trust and confidence of visitors to the website <i>(i.e http://www.delhimetrorail.com). </i>DMRC will not sell, rent, publish or trade any user's personal information to any individual or agency/organisation under any circumstances.<br/><br/>
The authorised controller of this website is Delhi Metro Rail Corporation Ltd. having its registered office at Metro Bhawan, Fire Brigade Lane, Barakhamba Road, New Delhi - 110001. The information contained in this website has been prepared solely for the purpose of providing information about DMRC in public interest.<br/><br/>
Users may access or/and download the information or material located on <a href="www.delhimetrorail.com">www.delhimetrorail.com</a> only for personal and non-commercial use. The unauthorized use/ illegal use of the material available on the website is strictly prohibited.<br/><br/>
DMRC may track domain names, IP addresses and browser types of people who visit this website. This information may be used to track aggregate traffic patterns and preparations of internal audit reports. Such information is not correlated with any personal information and it is not shared with anyone or anywhere.<br/><br/>
This site may have some links on the site which may lead to servers maintained by third parties over whom Delhi Metro Rail Corporation Ltd has no control. DMRC accepts no responsibility or liability of any sort for any of the material contained on third party servers.<br/><br/>
DMRC's web portal uses some social media networks such as Facebook, Youtube etc. to share our information/videos for better convenience of the commuters and general public at large. DMRC do not claim any responsibility nor shall any claim stand against DMRC if any error/inconvenience occurs while browsing it or any objection put on it by these site hosting organisation/agencies at any time.<br/><br/>
DMRC without any prior permission do not permit anyone to load/hyperlink any page of the website to any other website. DMRC also do not permit any app/blog or any social media pages to be hyperlinked to DMRC's website or any page of the website.<br/><br/>
By accessing this website the user agrees that DMRC will not be liable for any direct or indirect loss, of any nature whatsoever, arising from the use or inability to use website and from the material contained in this website or in any link thereof. The use of the website or any incidental issue that may arise shall be strictly governed by the laws in force in India and by using this website the user submits himself/herself to the exclusively jurisdiction of the courts in Delhi/New Delhi.<br/><br/>
The DMRC website uses cookies to allow the user to toggle between Hindi and English. The same is used to enhance the user's experience and not to collect any personal information.
The copyright of the material contained in this website exclusively belongs to and is solely vested with DMRC. The access to this website does not license the user to reproduce or transmit or store anywhere or disseminate the contents of any part thereof in any manner.<br/><br/>
DMRC reserves its rights to change or amend its privacy policy as per the management policy norms or Government of India guidelines from time to time.<br/><br/>
If at any time any user would like to contact www.delhimetrorail.com about the privacy policy with an aim to contribute to better quality of service of this website, then he/she may do so by emailing the Administrator at feedback[at]delhimetrorail[dot]com <br/><br/>

    
    
</div>


<script type="text/javascript" language="javascript">
	areport=function() {
		IE = document.all;
		if (IE)
			alert('Right-Click on the link and choose <Save Target as ..> option'); 
		else
			alert('Right-Click on the link and choose <Save Link as ..> option'); 
	}
</script>

<div style="float:left; height:180px; width:100%;"></div> 

                                </div>
                                <div>
                                    <img src="/images/bottom.jpg" alt="#" /></div>
                            </div>

                        </div>
                        <div>
                            <img src="/images/btm.jpg" alt="#" /></div>
                    </div>
                </div>
            </div>
        </div>




        

<footer>
    <div class="container">
        <div class="row">
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Contact Information</h1>
                Metro Bhawan
                Fire Brigade Lane, Barakhamba Road,<br />
                New Delhi - 110001, India
                    <br />
                Board No. - 011-23417910/12<br />
                <br />




            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick links</h1>
                <ul>
                    <li><a href="/Training-Institute/mdp.html" target="_blank"><b>MANAGEMENT DEVELOPMENT PROGRAM</b></a></li>
                    <li><a href="/photo-gallery.aspx" target="_blank">PHOTO GALLERY</a></li>
                    <li><a href="http://pgportal.gov.in/" target="_blank">PUBLIC GRIEVANCES</a></li>
                    <li><a href="/metro-citizen-forum.aspx" target="_blank">METRO CITIZENS FORUM</a></li>
                    <li><a href="/Public_notification.aspx">PUBLIC NOTICE</a> </li>
                    <li><a href="/lost_found.aspx">LOST & FOUND</a> </li>
                    <li><a href="/feedback.aspx">FEEDBACK</a> </li>
		<li><a id="ctl00_footerMenu_hplChequesEng" onclick="return FunHitIncrease(&#39;702&#39;,&#39;Cheque&#39;);" href="/ChequesDocuments/702ReadyChequesDetails.pdf" target="_blank">DETAILS OF PENDING CHEQUES</a></li>
                    


                    
                    <!-- <li><a href="/faqs.aspx">FAQs</a> |</li>-->
                    <li><a href="/contact_us.aspx">CONTACT US</a></li>
                    <li><a href="/disclamier.aspx">DISCLAIMERS</a></li>
                    <!--<li><a href="/privacy_policy.aspx">PRIVACY POLICY</a></li>-->
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <h1>Quick Contact</h1>


                <strong style="color: #016a88;">DMRC Helpline nos.</strong><br>
                <span class="call_no">155370<!--22561231--></span><br>
                <br>
                <p style="border: 1px solid red; width: 70%; background-color: #BD1111;"><b style="color: White;">&nbsp;&nbsp;Hit Counter : &nbsp;&nbsp;</b><span id="ctl00_footerMenu_lblHit" style="color:White;">74432609</span></p>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <span style="color:#aaa9a9; margin-top: 8px; display: block;"><a href="/privacypolicy.aspx" target="_blank" style="color: #016a88;">Terms of use | Privacy Policy</a><br />
                © 2017. All right are reserved.
			
				</span>
           			
<span style="margin-top: 10px; display: block;"> <a href="http://india.gov.in" target="_blank"><img src="/images/indiagov.jpg"></a></span>

				 <span style="margin-top: 14px; display: block;"> <a href="webinformationmanager.aspx" target="_blank"><img src="/images/WIM.png"></a></span>
				
				<span style="color: #aaa9a9; margin-top: 123px; margin-left:2px;display: block;">Developed by: <a href="http://www.netcommlabs.com" target="_blank" style="color: #aaa9a9;">Netcomm Labs</a><br/>
Hosted By :<a href="http://www.nic.in/" target="_blank" style="color: #ce1a1b;">National Informatics Centre</a></span>
				
				
				
				
				
				
				
            </div>


            
        </div>
    </div>
</footer>























    

<script type="text/javascript">
//<![CDATA[
WebForm_AutoFocus('ibtnFind');//]]>
</script>
</form>

    <script type="text/javascript" language="javascript">
        function SearchTextBlur(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "खोजें...";
                    return false;
                }
            }
            else {
                if (txtSearch.value.replace(/^\s+|\s+$/g, '') == "") {
                    txtSearch.value = "Search...";
                    return false;
                }
            }
        }

        function SearchTextFocus(clientID, lan) {
            var txtSearch = document.getElementById(clientID);
            if (lan == "1") {
                if (txtSearch.value == "खोजें...") {
                    txtSearch.value = "";
                    return false;
                }
            }
            else {
                if (txtSearch.value == "Search...") {
                    txtSearch.value = "";
                    return false;
                }
            }
        }
    </script>

    <script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
    </script>
    <script type="text/javascript">
        _uacct = "UA-102628-11";
        urchinTracker();
    </script>


    <script type="text/javascript" src="../nav_1/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../nav_1/script.js"></script>



</body>
</html>
