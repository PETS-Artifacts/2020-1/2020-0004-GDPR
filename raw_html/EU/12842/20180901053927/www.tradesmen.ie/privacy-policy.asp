

<html>
<head>
<title>Privacy Policy</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="robots" content="noindex,nofollow">
	<meta name="viewport" content="maximum-scale=1, width=device-width, user-scalable=yes" />
	<link rel="stylesheet" href="css.css" type="text/css">
	<link rel="stylesheet" href="new_style.css" type="text/css">
	<link rel="stylesheet" href="responsive.css" type="text/css">
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/jquery.placeholder.1.3.min.js"></script>
<link href="dist/cookies-message.min.css" rel="stylesheet">
<script src="dist/cookies-message.min.js"></script>
 <script>

  $(document).ready(function() {

    $.CookiesMessage({
      messageText: "By using Tradesmen.ie we assume you agree to our <a href='http://www.tradesmen.ie/cookie-policy.asp'>use of cookies</a> to enhance your experience..",
      messageBg: "#6C7FA0",								// Message box background color
      messageColor: "#FFFFFF",						// Message box text color
      messageLinkColor: "#F0FFAA",				// Message box links color
      closeEnable: true,									// Show the close icon
      closeColor: "#444444",							// Close icon color
      closeBgColor: "",						// Close icon background color
      acceptEnable: true,									// Show the Accept button
      acceptText: "I Accept",				// Accept button text
      infoEnable: false,										// Show the More Info button							// More Info button text
      infoUrl: "#",												// More Info button URL
      cookieExpire: 180										// Cookie expire time (days)
    });

  });

</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-20558430-1']);
  _gaq.push(['_setDomainName', 'tradesmen.ie']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>


<body bgcolor="C6D5E7" leftmargin="0" topmargin="10" marginwidth="0" marginheight="0">
<div align="center">
<table class="table-width" border="0" cellspacing="0" cellpadding="0">

  <tr valign="top"> 
    <td width="2"><img src="http://www.tradesmen.ie/images/topleft.gif" width="2" height="3"></td>
    <td background="http://www.tradesmen.ie/images/topcentre.gif" height="3"><img src="http://www.tradesmen.ie/images/trans.gif" height="3" width="1"></td>
    <td width="2"><img src="http://www.tradesmen.ie/images/topright.gif" width="2" height="3"></td>
  </tr>
</table>
<table class="table-width" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td background="http://www.tradesmen.ie/images/left.gif" width="2"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
    <td bgcolor="ffffff">
      <table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">

        <tr>
          <td class="img_1" width="14%">
		  
		<img src="http://www.tradesmen.ie/logo/SER729678tradesmenie_logo.gif">
		  
		  
		  </td>
          <td width="78%"> 
            <div align="center">
              <h1 class="H1"> Get up to 4 quotes from Rated Tradesmen!</h1>
            </div>

          </td>
          <td class="img_2" width="8%" valign="bottom"> 
            <div align="right"> 
              
              <img src="http://www.tradesmen.ie/logo/SER729678tradesmen_workman.jpg"> 
              
            </div>
          </td>
        </tr>
      </table>
    </td>
    <td background="http://www.tradesmen.ie/images/right.gif" width="2"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>

  </tr>
  <tr> 
    <td background="http://www.tradesmen.ie/images/left.gif" width="2" height="1"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
    <td bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
    <td background="http://www.tradesmen.ie/images/right.gif" width="2" height="1"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td background="http://www.tradesmen.ie/images/left.gif" width="2"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
	
	
	   <td bgcolor="6C7FA0">

        <table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr>
            <td class="banner">
              <div align="right">
			  Get up to 4 quotes from Rated Tradesmen!
			  </div>
            </td>
          </tr>
        </table>

    </td>
    <td background="http://www.tradesmen.ie/images/right.gif" width="2"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td background="http://www.tradesmen.ie/images/left.gif" width="2" height="10"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
    <td bgcolor="ffffff" height="10"> </td>
    <td background="http://www.tradesmen.ie/images/right.gif" width="2" height="10"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>

</table> <table class="new-content table-width" border="0" cellspacing="0" cellpadding="0" bgcolor="ffffff">
    <tr valign="top"> 
      <td background="images/left.gif" width="2"><img src="images/trans.gif" width="1" height="1"></td>
      <td class="new-left-td" width="168"> 
<!--**start left**-->
<table width="100%" border="0" cellspacing="0" cellpadding="0"> 
  <tr bgcolor="9C9C9C"> 
    <td width="22"><img src="http://www.tradesmen.ie/images/arrow.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td class="heading"> 
      <font class="heading-text" color="#FFFFFF">Main Menu</font> </td>
  </tr>
  <tr> 
    <td><img src="http://www.tradesmen.ie/images/trans.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td><a href="http://www.tradesmen.ie/index.asp" class="navlink">Home</a></td>
  </tr>
  <tr> 
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr> 
    <td><img src="http://www.tradesmen.ie/images/trans.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td><a href="http://www.tradesmen.ie/getquote.asp" class="navlink">Get quotes for your Job </a></td>
  </tr>
  <tr> 
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr> 
    <td><img src="http://www.tradesmen.ie/images/trans.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td><a href="http://www.tradesmen.ie/register.asp" class="navlink">Register as a tradesman </a></td>
  </tr>
  <tr> 
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr> 
    <td><img src="http://www.tradesmen.ie/images/trans.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td><a href="http://www.tradesmen.ie/browse-jobs.html" class="navlink">Browse 
      Latest Jobs</a></td>
  </tr>
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr> 
    <td><img src="http://www.tradesmen.ie/images/trans.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td><a href="http://www.tradesmen.ie/trades" class="navlink">Browse Trades by Category</a></td>
  </tr>
  <tr> 
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr> 
    <td><img src="http://www.tradesmen.ie/images/trans.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td><a href="http://blog.tradesmen.ie" class="navlink" rel="nofollow">Blog</a></td>
  </tr>
  <tr> 
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr> 
    <td><img src="http://www.tradesmen.ie/images/trans.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td><a href="http://www.tradesmen.ie/contactus.asp" class="navlink">Contact us / FAQs</a></td>
  </tr>
  <tr> 
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  
   <tr> 
    <td><img src="http://www.tradesmen.ie/images/trans.gif" width="14" height="14" vspace="4" hspace="4"></td>
    <td><a href="https://www.tradesmen.ie/tlogin.asp" class="navlink">Tradesman login</a></td>
  </tr>
  <tr> 
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
</table>


<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" bgcolor="E5E5E5">
  <tr> 
    <td> 
      <div align="center"><BR>
        <b><font color="868686">What people said about</font><br>
        <img src="http://www.tradesmen.ie/images/arrows.gif"><u><font color="1A599E">Trades</font><font color="616161">men.ie</font></u></b> 
        <br>
        <br>
      </div>
    </td>
  </tr>
</table>
<table width="100%" cellpadding="0" cellspacing="0" border="0">
  <tr> 
    <td colspan="2" bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
  <tr> 
    <td> 
      <link rel="Stylesheet" type="text/css" href="http://www.tradesmen.ie/global.css?status=0&httpStatusCode=OK?v=220"  />
<!--<script type="text/javascript" language="javascript" src="http://www.tradesmen.ie/js/query.js"></script>-->
<script type="text/javascript" language="javascript" src="http://www.tradesmen.ie/js/utils.js?v=220"></script>
<script type="text/javascript" language="javascript" src="http://www.tradesmen.ie/js/bmin.js?v=220"></script>
    
            <div id="container" class="sidebar"> 
            
                
    
            <ul id="TestimonialsShowing">
            <li>
                <span class="testimonialQuote testimonialInfo">&#147;Great service & great tradesman - would use both again.&#148; </span>
               
                <span class="JA_testimonialWho"><br>
                    <span class="testimonialName testimonialInfo">Linda Murphy</span> <br>
                    <span class="testimonialLocation testimonialInfo">Clondalkin, Dublin 22</span>
                </span>
            </li>
            </ul>
        
<br>
  <table bgcolor="E5E6E6" width="100%" cellpadding="2" cellspacing="2" border="0">
    <tr><td>            
	<div align="right" class="trades-next-last">
	&lt;&lt; 
	<a id="lastTestimonial" class="lastCycler noexit" href="javascript:;">Last</a> 
	|
    <a id="nextTestimonial" class="nextCycler noexit"  href="javascript:;">Next</a> 
			&gt;&gt;</div> </td></tr></table>

        <ul id="TestimonialsOthers" class="TM_hiddenElement">
        <li>
            <span class="testimonialQuote">&#147;Great service & great tradesman - would use both again.&#148;  </span><br>
            <span class="testimonialName">Linda Murphy</span> <br>
            <span class="testimonialLocation">Clondalkin, Dublin 22</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;I would certainly use Tradesmen.ie again and recommend it to others. A great service but little known to the public.You should promote yourselves more.&#148;  </span><br>
            <span class="testimonialName">Taragh O'Kelly</span> <br>
            <span class="testimonialLocation">Terenure, Dublin 6W</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;Excellent service .... second time using it. Love the way you can see the feedback on previous jobs.&#148;  </span><br>
            <span class="testimonialName">Eavan Clancy</span> <br>
            <span class="testimonialLocation">Celbridge, Kildare</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;Really liked the experience on your site and already recommended it to all my colleagues who were interested in the work I was doing at home.&#148;  </span><br>
            <span class="testimonialName">Hinde Lamrani</span> <br>
            <span class="testimonialLocation">Ashtown, Dublin 15</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;Yes I posted an ad. I was surprised at the speed of return and the names and contacts of four people both texted and online so quickly.&#148;  </span><br>
            <span class="testimonialName">Ann Byrne</span> <br>
            <span class="testimonialLocation">Bray, Wicklow</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;I am only sorry that I didn't know of this website before as I found the response and service excellent. I have already recommended to family and friends.&#148;  </span><br>
            <span class="testimonialName">Sharon Kennedy</span> <br>
            <span class="testimonialLocation">Clane, Kildare</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;Great Service, I have recommended it to people and regularly use the service.&#148;  </span><br>
            <span class="testimonialName">Lidia Wright</span> <br>
            <span class="testimonialLocation">Clonsilla, Dublin 15</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;Excellent service and fast response to my query. Will definitely use as my main source for tradesmen.&#148;  </span><br>
            <span class="testimonialName">William Donnelly</span> <br>
            <span class="testimonialLocation">Glasnevin, Dublin 11</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;This was a service that I had not realised was there until I google searched. I have to pay you the highest compliments for the way you run this service. From the initial responses by text and e mail through to your follow up after the work was done. I would without hesitation use this service again if my needs arise and would highly recommend your service.&#148;  </span><br>
            <span class="testimonialName">Stephen O'Brien</span> <br>
            <span class="testimonialLocation">Marino, Dublin 3</span>
        </li>
    
        <li>
            <span class="testimonialQuote">&#147;I already have recommended Tradesmen.ie and will continue to do so.&#148;  </span><br>
            <span class="testimonialName">Teresa McCarthy</span> <br>
            <span class="testimonialLocation">Leixslip, Co. Kildare</span>
        </li>
        <li>
            <span class="testimonialQuote">&#147;I found your site very useful and will definitely use it in the
future and recommend it to others.&#148; </span><br>
            <span class="testimonialName">Kelly McCabe</span> <br>
            <span class="testimonialLocation">Dublin 8</span>
        </li>
        <li>
            <span class="testimonialQuote">&#147;Excellent website and fantastic service.&#148; </span><br>
            <span class="testimonialName">Jane Burns</span> <br>
            <span class="testimonialLocation">Dunboyne, Co. Meath</span>
        </li>
        <li>
            <span class="testimonialQuote">&#147;Excellent service, I had responses almost immediately would definitely recommend the website to anyone who is looking for some work to be done. 10/10!&#148;  </span><br>
            <span class="testimonialName">Jacqui Synnott</span> <br>
			<span class="testimonialLocation">Dublin 3</span>
        </li>
    
        </ul>
                  </div>

    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
  <tr> 
    <td> 
      
      &nbsp; 
      
    </td>
  </tr>
</table>

<!--**end left**--> </td><td width="1" background="images/vertdot.gif"><img src="images/trans.gif" width="1" height="1"></td><td> 
<div align="center">  <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
<tr valign="top"> <td width="66%"> <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
<tr> <td height="20"><img src="images/trans.gif" width="1" height="15"></td></tr> 
<tr> 
                    <td><img src="images/trans.gif" width="20"><span class="Header">Privacy 
                      Policy </span> <img src="images/trans.gif" width="10"></td>
                  </tr> <tr> <td height="1" background="images/hordot.gif"><img src="images/trans.gif" width="1" height="1"></td></tr> 
</table>
                
                <table width="100%" border="0" cellspacing="5" cellpadding="5" align="center">
                  <tr> 
                    <td> 
                      <p>We are committed to protecting your privacy and we take 
                        our responsibility regarding the security of your information 
                        very seriously. We will try to be clear and transparent 
                        about the information we are collecting and what we will 
                        do with that information. This Policy was last updated 
                        on 15 June 2018. The previous Privacy Policy can be seen 
                        <a href="page.asp?cid=1346">here<br>
                        </a><br>
                        <b>Contents</b><br>
                        <a href="#ThisPolicy">This Policy</a><br>
                        <a href="#CollectionOfPersonalData">Collection of Personal 
                        Data</a><br>
                        <a href="#CreationofPersonalData">Creation of Personal 
                        Data</a><br>
                        <a href="#CategoriesofPersonalDatawemayProcess">Categories 
                        of Personal Data we may Process</a><br>
                        <a href="#LawfulbasisforProcessingPersonalData">Lawful 
                        basis for Processing Personal Data</a><br>
                        <a href="#SensitivePersonalData">Sensitive Personal Data</a><br>
                        <a href="#PurposesforwhichwemayProcessyourPersonalData">Purposes 
                        for which we may Process your Personal Data</a><br>
                        <a href="#DisclosureofPersonalDatatothirdparties">Disclosure 
                        of Personal Data to third parties</a><br>
                        <a href="#InternationaltransferofPersonalData">International 
                        transfer of Personal Data</a><br>
                        <a href="#Datasecurity">Data security</a><br>
                        <a href="#Dataaccuracy">Data accuracy</a><br>
                        <a href="#Dataminimisation">Data minimisation</a><br>
                        <a href="#Dataretention">Data retention</a><br>
                        <a href="#Yourlegalrights">Your legal rights</a><br>
                        <a href="#Cookiesandsimilartechnologies">Cookies and similar 
                        technologies</a><br>
                        <a href="#TermsofUse">Terms of Use</a><br>
                        <a href="#Directmarketing">Direct marketing</a><br>
                        <a href="#Contactdetails">Contact details</a><br>
                        <a href="#Definitions">Definitions</a></p>
                      <p><b>This Policy<a name="ThisPolicy"></a> </b><br>
                        This policy explains how we may process your personal 
                        data. This policy may be amended or updated from time 
                        to time, so please check it regularly for updates. <br>
                      </p>
                      <p>This policy is issued by Barrowvale Technology Ltd. T/A 
                        Tradesmen.ie , its subsidiaries and its affiliates and 
                        is addressed to individuals outside our organisation with 
                        whom we interact, including customers, visitors to our 
                        web sites, users of our Apps, and other users of our services.<br>
                      </p>
                      <p>The services available at http://www.Tradesmen.ie are 
                        provided by Barrowvale Technology Ltd T/A Tradesmen.ie. 
                        (&quot;Tradesmen.ie &quot;, &quot;we&quot;, &quot;us&quot; 
                        or &quot;our&quot;), company registration number 330063, 
                        having its registered office at Jamestown, Ballybrittas, 
                        Co. Laois, Ireland. The term Service Provider refers to 
                        any person who provides a service of any kind, including 
                        their subcontractors, agents and employees. The term Service 
                        Buyer refers to any person looking for a service to be 
                        provided. The terms &quot;You&quot;, &quot;Your&quot;, 
                        &quot;User&quot;, or &quot;Users&quot; refer to anyone 
                        accessing our services or web site for any reason.<br>
                      </p>
                      <p>For the purposes of this policy, Barrowvale Technology 
                        Ltd. is the Data Controller. This policy covers the information 
                        practices relating to our company web sites and services 
                        offered now or in the future.<br>
                      </p>
                      <p><b>This policy may be amended or updated from time to 
                        time to reflect changes in our practices with respect 
                        to the processing of personal data, or changes in Irish 
                        or European Union law. We encourage you to read this policy 
                        carefully, and to regularly check this page to review 
                        any changes we might make in accordance with the terms 
                        of this policy. <br>
                        </b> </p>
                      <p><b>Collection of Personal Data </b><a name="CollectionOfPersonalData"></a><br>
                      </p>
                      <p>We may collect personal data about you from the following 
                        sources:<br>
                      </p>
                      <ul>
                        <li>Data you provide: We may obtain your personal data 
                          when you provide it to us e.g., where you contact us 
                          via text message, email or telephone, or by any other 
                          means, or when you provide us with your business card.<br>
                        </li>
                        <li>Relationship data: We may collect or obtain your personal 
                          data in the ordinary course of our relationship with 
                          you e.g., if you purchase a service from us.<br>
                        </li>
                        <li>Data you make public: We may collect or obtain your 
                          personal data that you manifestly choose to make public, 
                          including via social media e.g., we may collect information 
                          from your social media profile(s), if you make a public 
                          post about us.<br>
                        </li>
                        <li>App data: We may collect or obtain your personal data 
                          when you download or use any of our Apps.<br>
                        </li>
                        <li>Web site data: We may collect or obtain your personal 
                          data when you visit our web sites or use any features 
                          or resources available on or through a web site.<br>
                        </li>
                        <li>Registration details: We may collect or obtain your 
                          personal data when you use, or register to use, any 
                          of our web sites, Apps, or services.<br>
                        </li>
                        <li>Content and advertising information: If you choose 
                          to interact with any third-party content or advertising 
                          on a web site or in an App, we may receive personal 
                          data about you from the relevant third-party.<br>
                        </li>
                        <li>Third party information: We may collect or obtain 
                          your personal data from third-parties who provide it 
                          to us e.g., credit reference agencies; law enforcement 
                          authorities; etc. <br>
                        </li>
                      </ul>
                      <p><b>Creation of Personal Data</b><a name="CreationofPersonalData"></a><br>
                      </p>
                      <p>We may also create personal data about you, such as records 
                        of your interactions with us, details of jobs you have 
                        posted, leads you have requested and purchase history 
                        for internal administration purposes and analysis. </p>
                      <p><b>Categories of Personal Data we may Process </b><a name="CategoriesofPersonalDatawemayProcess"></a><br>
                      </p>
                      <p>We may process the following categories of personal data 
                        about you: <br>
                      </p>
                      <ul>
                        <li> Personal details: given name(s), preferred name, 
                          photograph;<br>
                        </li>
                        <li> Demographic information: salutation, gender, date 
                          of birth or age, nationality, job title, industry, occupation, 
                          skill sets, language preferences;<br>
                        </li>
                        <li> Contact details: postal address, telephone number(s), 
                          email address, details of your public social media profile(s);<br>
                        </li>
                        <li> Consent records: records of any consents you may 
                          have given, the date and time, the means by which we 
                          obtained your consent;<br>
                        </li>
                        <li> Service Provider Activity: records of leads purchased, 
                          references requested and received, reviews posted, reviews 
                          received, feedback provided;<br>
                        </li>
                        <li> Service Buyer Activity: records of jobs posted, references 
                          posted, reviews posted, reviews received, feedback provided, 
                          jobs featured;<br>
                        </li>
                        <li> Purchase details: records of purchases, prices paid;<br>
                        </li>
                        <li> Payment details: invoice records, payment records, 
                          billing address, payment method, bank account number, 
                          IBAN details, SWIFT details, payment amount, payment 
                          date, records of cheques;<br>
                        </li>
                        <li> Identity Verification and Fraud Prevention information: 
                          documents regarding identity and standing, corporate 
                          and financial information, credit history information, 
                          insurance information, tax registration and/or VAT Number 
                          and bank account information among others;<br>
                        </li>
                        <li> Data relating to our web sites and Apps: device type 
                          used, operating system, browser type, browser settings, 
                          IP address, language settings, dates and times of connecting 
                          to a web site, App usage statistics, App settings; dates 
                          and times of connecting to an App, location and other 
                          technical communications data (some of which may constitute 
                          personal data), username, password, security login details, 
                          usage data, aggregate statistical information;<br>
                        </li>
                        <li> Employer details (where you interact with us in your 
                          capacity as an employee in relation to business correspondence): 
                          the contact information of your employer (including 
                          name, address, telephone number and email address) to 
                          the extent relevant;<br>
                        </li>
                        <li> Content and advertising data: Analytics which record 
                          your interactions with our online advertising and content, 
                          records of advertising and content displayed on web 
                          site pages or App screens displayed to you, and any 
                          interaction you may have had with such content or advertising, 
                          including mouse clicks, any forms you complete and any 
                          touchscreen interactions;<br>
                        </li>
                        <li> Views and opinions: any views and opinions that you 
                          choose to send to us, or publicly post about us on social 
                          media platforms. </li>
                      </ul>
                      <p>If you give us personal information about someone else 
                        such as a reference, you must do so only with that person's 
                        authorisation. You should inform them how we collect, 
                        use, disclose, and retain their personal information according 
                        to our privacy notice.</p>
                      <p><b>Lawful basis for Processing Personal Data </b><a name="LawfulbasisforProcessingPersonalData"></a><br>
                      </p>
                      <p>In processing your personal data in connection with the 
                        purposes set out in this policy, we may rely on one or 
                        more of the following legal bases, depending on the circumstances: 
                        <br>
                      </p>
                      <ul>
                        <li> Consent: We may process your personal data where 
                          we have obtained your prior, express consent to the 
                          processing (this legal basis is only used in relation 
                          to processing that is entirely voluntary - it is not 
                          used for data processing that is necessary, legal or 
                          obligatory in any way)<br>
                        </li>
                        <li>Contractual necessity: We may process your personal 
                          data where the processing is necessary in connection 
                          with any contract that you may enter into with us; <br>
                        </li>
                        <li>Compliance with applicable law: We may process your 
                          personal data where the processing is required by applicable 
                          law<br>
                        </li>
                        <li>Vital interests: We may process your personal data 
                          where the processing is necessary to protect the vital 
                          interests of any individual<br>
                        </li>
                        <li>Legitimate interests: We may process your personal 
                          data where we have a legitimate interest in carrying 
                          out the processing for the purpose of managing, operating 
                          or promoting our business, and that legitimate interest 
                          is not overridden by your interests, fundamental rights, 
                          or freedoms. <br>
                        </li>
                      </ul>
                      <p><b>Sensitive Personal Data </b><a name="SensitivePersonalData"></a><br>
                      </p>
                      <p>We do not seek to collect or otherwise process your sensitive 
                        personal data in the ordinary course of our business. 
                        Where it becomes necessary to process your sensitive personal 
                        data for any reason, we rely on one of the following legal 
                        bases: <br>
                      </p>
                      <ul>
                        <li>Compliance with applicable law: We may process your 
                          sensitive personal data where the processing is required 
                          or permitted by applicable law e.g. to comply with our 
                          diversity reporting obligations<br>
                        </li>
                        <li>Detection and prevention of crime: We may process 
                          your sensitive personal data where the processing is 
                          necessary for the detection or prevention of crime, 
                          including the prevention of fraud.<br>
                        </li>
                        <li>Establishment, exercise or defence of legal rights: 
                          We may process your sensitive personal data where the 
                          processing is necessary for the establishment, exercise 
                          or defence of legal rights.<br>
                        </li>
                        <li>Consent: We may process your sensitive personal data 
                          where we have, in accordance with applicable law, obtained 
                          your prior, express consent prior to processing your 
                          sensitive personal data. This legal basis is only used 
                          in relation to processing that is entirely voluntary 
                          - it is not used for data processing that is necessary, 
                          legal or obligatory in any way. <br>
                        </li>
                      </ul>
                      <p><b>Purposes for which we may Process your Personal Data 
                        </b><a name="PurposesforwhichwemayProcessyourPersonalData"></a><br>
                      </p>
                      <p>The purposes for which we may process personal data, 
                        subject to applicable law, include: <br>
                      </p>
                      <ul>
                        <li> Provision of services to you: if you are a Service 
                          Buyer we use your personal data to communicate with 
                          you about the job you posted and your request to receive 
                          quotes, to provide your contact information, details 
                          of jobs posted and reviews you have received if any 
                          to a maximum of four service providers who are interested 
                          in giving you a quote, to provide tips on how to stay 
                          safe, understand how our service works and have a good 
                          experience using our service, to process payments where 
                          you have purchased other services such as featuring 
                          your job; if you are a Service Provider we use your 
                          personal data to process payments and to offer you leads 
                          that match the trades and counties you have selected 
                          in your profile and to pass on your contact details 
                          and references and reviews received to the Service Buyer 
                          when you purchase a lead; we use your personal data 
                          to determine if you posted a job or registered as a 
                          Service Provider in the past and to check that you were 
                          not suspended from using our service for breach of our 
                          User Agreement or for any other reason;<br>
                        </li>
                        <li> Communications: we use your personal data to communicate 
                          with you in relation to the services you have requested 
                          via different channels (including via email, telephone, 
                          text message, social media, post or in person);<br>
                        </li>
                        <li> Communications and IT operations: we may process 
                          your personal data for management of our communications 
                          systems, operation of IT security systems and IT security 
                          audits;<br>
                        </li>
                        <li> Health and safety: we may process your personal data 
                          for health and safety assessments, record keeping, compliance 
                          with related legal obligations<br>
                        </li>
                        <li> Financial management: we may process your personal 
                          data for sales, finance, corporate audit, vendor management<br>
                        </li>
                        <li> Surveys &amp; Feedback: we use your personal data 
                          to engage with you for the purposes of obtaining your 
                          views on our services and users with whom you have engaged 
                          for quality control purposes and in order to protect 
                          the security of our users, ourselves, and others. For 
                          example, if you are a Service Buyer we use your personal 
                          information to request feedback on a job you have posted 
                          or if you are a Service Provider we use your personal 
                          information to request feedback on a job you have carried 
                          out.<br>
                        </li>
                        <li> Security: we may process your personal data in order 
                          to protect the security of our users, ourselves, and 
                          others. For example, if you visit our premises we may 
                          process your personal data by keeping records of visits 
                          and CCTV recordings; if you access our services online 
                          we may process your personal data through the use of 
                          login records and access details; if you are a Service 
                          Provider we may request identity verification and fraud 
                          prevention information including documents regarding 
                          identity and standing, corporate and financial information, 
                          credit history information, insurance information, tax 
                          registration and/or VAT Number and bank account information 
                          among others; if you provide us with false or inaccurate 
                          information and we suspect or detect fraud then we may 
                          forward this information to the relevant fraud or crime 
                          prevention bodies;<br>
                        </li>
                        <li> Investigations: we may process your personal data 
                          for the purpose of detecting, investigating and preventing 
                          breaches of policy, criminal offences in accordance 
                          with applicable law;<br>
                        </li>
                        <li> Legal proceedings: we may process your personal data 
                          for the purpose of establishing, exercising and defending 
                          legal rights<br>
                        </li>
                        <li> Legal compliance: we may process your personal data 
                          for compliance with our legal and regulatory obligations 
                          under applicable law<br>
                        </li>
                        <li> Improving our web sites, Apps, services: we may process 
                          your personal data for the purpose of identifying issues 
                          with our web sites, our Apps, or our services, planning 
                          improvements to our web sites, our Apps, or our services, 
                          notifying you of changes to any of our web sites, our 
                          Apps, or our services, creating new web sites, Apps, 
                          or services <br>
                        </li>
                        <li> Newsletters and marketing information: we use your 
                          personal information to send newsletters or marketing 
                          information to you (where you have given consent)</li>
                      </ul>
                      <p><b>Disclosure of Personal Data to third parties </b><a name="DisclosureofPersonalDatatothirdparties"></a><br>
                      </p>
                      <p>If you are a Service Buyer and you post a job on our 
                        website we will provide the contact details you have submitted 
                        to a maximum of four service providers who are interested 
                        in quoting for your job so that they can contact you and 
                        give you a quote. The jobs you have posted and any reviews 
                        you posted or received may be published on our website 
                        or sent to prospective Service Providers under a username 
                        or shortened anonymised version of your name which may 
                        give service providers more confidence in quoting for 
                        your job. If you provide your consent we will provide 
                        your contact details to a trusted third party call centre 
                        to get feedback on your experience using our service for 
                        quality control purposes.<br>
                      </p>
                      <p>If you are a Service Provider we will provide your contact 
                        details to a service buyer if you are interested in quoting 
                        for their job and if certain criteria are met. We may 
                        need to share personal information with trusted third 
                        party service providers for identity verification and 
                        fraud prevention. In the event of a dispute between a 
                        Service Provider and a Service Buyer we reserve the right 
                        to share any other information that we hold in relation 
                        to a service provider if we feel that it may help to resolve 
                        the matter in accordance with applicable law.<br>
                      </p>
                      <p>We may disclose your personal data to other entities 
                        within the company group, for legitimate business purposes, 
                        including operating our web sites and our Apps, and providing 
                        services to you, in accordance with applicable law.<br>
                      </p>
                      <p>In addition, we may disclose your personal data to: <br>
                      </p>
                      <ul>
                        <li>legal and regulatory authorities, upon request, or 
                          for the purposes of reporting any actual or suspected 
                          breach of applicable law or regulation<br>
                        </li>
                        <li>outside professional advisors, accountants, auditors, 
                          or lawyers, subject to binding contractual obligations 
                          of confidentiality<br>
                        </li>
                        <li>third party processors, e.g. payment services providers, 
                          data centres; etc., located anywhere in the world, subject 
                          to the requirements noted below.<br>
                        </li>
                        <li>any relevant party, law enforcement agency or court, 
                          to the extent necessary for the establishment, exercise 
                          or defence of legal rights<br>
                        </li>
                        <li>any relevant party for the purposes of prevention, 
                          investigation, detection or prosecution of criminal 
                          offences or the execution of criminal penalties, including 
                          safeguarding against and the prevention of threats to 
                          public security<br>
                        </li>
                        <li>any relevant third-party acquirer(s), in the event 
                          that we sell or transfer all or any relevant portion 
                          of our business or assets, including in the event of 
                          a reorganisation, dissolution or liquidation.</li>
                        <li>any relevant third-party where we acquire a product 
                          or business<br>
                        </li>
                        <li>any relevant third-party provider, where our web sites 
                          and our Apps use third-party advertising, plugins or 
                          content. If you choose to interact with any such advertising, 
                          plugins or content, your personal data may be shared 
                          with the relevant third-party provider. We recommend 
                          that you review that third party's privacy policy before 
                          interacting with its advertising, plugins or content. 
                          <br>
                        </li>
                      </ul>
                      <p>If we engage a third-party processor to process your 
                        personal data, the processor will be subject to binding 
                        contractual obligations to: <br>
                      </p>
                      <ul>
                        <li> only process the personal data in accordance with 
                          our prior written instructions;</li>
                        <li>use measures to protect the confidentiality and security 
                          of the Personal Data; together with any additional requirements 
                          under applicable law. <br>
                        </li>
                      </ul>
                      <p><b>International transfer of Personal Data </b><a name="InternationaltransferofPersonalData"></a><br>
                      </p>
                      <p>Because of the international nature of our business, 
                        we may need to transfer your personal data within the 
                        company or group, and to third-parties as noted in the 
                        'Disclosures' section above, in connection with the purposes 
                        set out in this policy. For this reason, we may transfer 
                        your personal data to other countries that may have different 
                        laws and data protection compliance requirements to those 
                        that apply in the country in which you are located.<br>
                      </p>
                      <p>Where we transfer your personal data from the EEA to 
                        recipients located outside the EEA who are not in Adequate 
                        Jurisdictions, we do so based on standard contractual 
                        clauses. You may request a copy of our standard contractual 
                        clauses using the contact details provided.<br>
                      </p>
                      <p>Please note that when you transfer any personal data 
                        directly to a company entity established outside the EEA, 
                        we are not responsible for that transfer of your personal 
                        data. We will nevertheless process your personal data, 
                        from the point at which we receive the data, in accordance 
                        with the provisions of this Privacy Policy. <br>
                      </p>
                      <p><b>Data security</b><a name="Datasecurity"></a><br>
                      </p>
                      <p>We have implemented appropriate technical and organisational 
                        security measures designed to protect your personal data 
                        against accidental or unlawful destruction, loss, alteration, 
                        unauthorised disclosure, unauthorised access, and other 
                        unlawful or unauthorised forms of processing, in accordance 
                        with applicable Irish and European Union law. <br>
                      </p>
                      <p>As the internet is an open system, the transmission of 
                        information via the internet is not completely secure. 
                        Although we will implement all reasonable measures to 
                        protect your personal data, we cannot guarantee the security 
                        of your data transmitted to us using the internet - any 
                        such transmission is at your own risk and you are responsible 
                        for ensuring that any personal data that you send to us 
                        are sent securely. <br>
                      </p>
                      <p>It is important for you to protect against unauthorised 
                        access to your password and to your computers, devices, 
                        and applications. Be sure to sign off when you finish 
                        using a shared device or computer.<br>
                        <br>
                        <b> Data accuracy</b><a name="Dataaccuracy"></a><br>
                      </p>
                      <p>We take every reasonable step to ensure that: <br>
                      </p>
                      <ul>
                        <li>your personal data that we process is accurate and 
                          kept up to date<br>
                        </li>
                        <li>any of your personal data that we process which you 
                          inform us is inaccurate will be erased or rectified 
                          as per your instruction<br>
                        </li>
                      </ul>
                      <p><b>Data minimisation </b><a name="Dataminimisation"></a><br>
                      </p>
                      <p>We take every reasonable step to ensure that your personal 
                        data which we process is limited to the personal data 
                        reasonably necessary in connection with the purposes set 
                        out in this policy or as required to provide you services 
                        or access to our web sites and/or Apps. <br>
                      </p>
                      <p><b>Data retention </b><a name="Dataretention"></a><br>
                      </p>
                      <p>We take every reasonable step to ensure that your personal 
                        data is only processed for the minimum period necessary 
                        for the purposes set out in this policy. <br>
                      </p>
                      <p>The criteria for determining the duration for which we 
                        will keep your personal data are as follows:<br>
                      </p>
                      <p>We will retain copies of your personal data in a form 
                        that permits identification only for as long as is necessary 
                        in connection with the purposes set out in this policy 
                        unless applicable law requires a longer retention period. 
                        We may retain your personal data for the duration of any 
                        period necessary to establish, exercise or defend any 
                        legal rights. <br>
                      </p>
                      <p><b>Your legal rights </b><a name="Yourlegalrights"></a><br>
                      </p>
                      <p>Subject to applicable law, you may have several rights 
                        regarding the processing of your relevant personal data, 
                        including: <br>
                      </p>
                      <ul>
                        <li>the right not to provide your personal data to us. 
                          However, please note that we may be unable to provide 
                          you with the full benefit of our web sites, Apps or 
                          services.<br>
                        </li>
                        <li>if you do not provide us with your personal data, 
                          we may not be able to process your orders without the 
                          necessary details.<br>
                        </li>
                        <li>the right to request access to, or copies of, your 
                          relevant personal data, together with information regarding 
                          the nature, processing and disclosure of those relevant 
                          personal data<br>
                        </li>
                        <li>the right to request rectification of any inaccuracies 
                          in your relevant personal data<br>
                        </li>
                        <li>the right to request, on legitimate grounds: - the 
                          erasure of your relevant personal data, restriction 
                          of processing of your relevant personal data<br>
                        </li>
                        <li>the right to object, on legitimate grounds, to the 
                          processing of your relevant personal data by us or on 
                          our behalf<br>
                        </li>
                        <li>the right to have certain relevant personal data transferred 
                          to another Data Controller, in a structured, commonly 
                          used and machine-readable format, to the extent applicable<br>
                        </li>
                        <li>where we process your relevant personal data based 
                          on your consent, the right to withdraw that consent, 
                          noting that such withdrawal does not affect the lawfulness 
                          of any processing performed prior to the date on which 
                          we receive notice of such withdrawal, and does not prevent 
                          the processing of your personal data in reliance upon 
                          any other available legal bases.<br>
                        </li>
                        <li>the right to lodge complaints with the Data Protection 
                          Commissioner regarding the processing of your relevant 
                          personal data by us or on our behalf. <br>
                        </li>
                      </ul>
                      <p>This does not affect your statutory rights. <br>
                      </p>
                      <p>To exercise one or more of these rights, or to ask a 
                        question about these rights or any other provision of 
                        this policy, or about our processing of your personal 
                        data, please use the contact details provided below.<br>
                      </p>
                      <p>Please note that: -<br>
                      </p>
                      <ul>
                        <li>we may require proof of your identity before we can 
                          give effect to these rights</li>
                        <li>where your request requires the establishment of additional 
                          facts, e.g., a determination of whether any Processing 
                          is non-compliant with applicable law, we will investigate 
                          your request reasonably promptly, before deciding on 
                          what action to take. </li>
                      </ul>
                      <p><br>
                        <b> Cookies and similar technologies </b><a name="Cookiesandsimilartechnologies"></a><br>
                      </p>
                      <p>When you visit a web site or use an App, Cookies may 
                        be placed onto your device, or Cookies already on your 
                        device may be read. This should always be subject to obtaining 
                        your consent, where required, in accordance with applicable 
                        law.<br>
                      </p>
                      <p>We use Cookies to record information about your device, 
                        your browser and, in some cases, your preferences and 
                        browsing habits. We may process your personal data through 
                        Cookies and similar technologies, in accordance with our 
                        <a href="cookie-policy.asp">Cookie Policy</a>. <br>
                      </p>
                      <p>Our web pages may contain electronic images (also known 
                        as web beacons, single-pixel gifs or transparent graphic 
                        images). This technology helps us to deliver cookies on 
                        our sites, count users who have visited our sites, deliver 
                        services, and analyse the effectiveness of our promotional 
                        campaigns. We may also include web beacons in our marketing 
                        or newsletter email messages to determine whether an email 
                        is opened or if links are clicked. Web beacons are also 
                        used to deliver interest-based advertising. You can find 
                        out more about interest-based advertising <a href="https://www.bbb.org/en/us/article/news-releases/16953-what-you-need-to-know-about-interest-based-advertising" target="_blank">here</a><br>
                      </p>
                      <p><b>Terms of Use </b><a name="TermsofUse"></a><br>
                      </p>
                      <p>All use of our Web sites, our Apps, and/or services is 
                        subject to our <a href="agreement.asp">User Agreement</a>. 
                        We recommend that you review our User Agreement regularly, 
                        to review any changes, we might make from time to time. 
                        <br>
                      </p>
                      <p> <b>Direct marketing </b><a name="Directmarketing"></a><br>
                      </p>
                      <p>We may process your personal data to contact you via 
                        email, telephone, direct mail or other communication formats 
                        to provide you with information regarding services that 
                        may be of interest to you.<br>
                      </p>
                      <p>If we provide services to you, we may send information 
                        to you regarding our services, upcoming promotions and 
                        other information that may be of interest to you, using 
                        the contact details that you have provided to us and always 
                        in compliance with applicable law. <br>
                      </p>
                      <p>You may unsubscribe from our promotional email list at 
                        any time by following the unsubscribe instructions included 
                        in every promotional email we send.<br>
                      </p>
                      <p>We will not send you promotional emails from a list you 
                        have selected to be unsubscribed from, but we may continue 
                        to contact you to the extent necessary for the purposes 
                        of any services you have requested or from additional 
                        lists you have signed up under. <br>
                        <br>
                        <b>Contact details </b><a name="Contactdetails"></a><br>
                      </p>
                      <p>You may contact us about your direct marketing preference 
                        by on our contact form here<br>
                      </p>
                      <p>If you wish to be taken off our contact list for direct 
                        marketing, or if you have any comments, questions or concerns 
                        about any of the information in this Policy, or any other 
                        issues relating to the Processing of Personal Data carried 
                        out by us, or on our behalf, please contact us by email 
                        on our contact form <a href="contactform.asp">here</a> 
                        or post to the following address: <br>
                      </p>
                      <p>The Data Protection Officer, Barrowvale Technology Ltd. 
                        T/A Tradesmen.ie, Jamestown, Ballybrittas, Portlaoise, 
                        Co. Laois, Ireland. <br>
                        Phone +353 57 86 26113 <br>
                        <br>
                        <b>Definitions </b><a name="Definitions"></a><br>
                      </p>
                      <p> <b>App</b> <br>
                        means any application made available by us (including 
                        where we make such applications available via third party 
                        stores or marketplaces, or by any other means). <br>
                        <b>Adequate Jurisdiction</b><br>
                        a jurisdiction that has been formally designated by the 
                        European Commission as providing an adequate level of 
                        protection for Personal Data. <br>
                        <b>Cookie</b><br>
                        means a small file that is placed on your device when 
                        you visit a website (including our Web sites). In this 
                        Policy, a reference to a &quot;Cookie&quot; includes analogous 
                        technologies such as web beacons and clear GIFs. <br>
                        <b>Controller</b><br>
                        means the entity that decides how and why Personal Data 
                        are Processed. In many jurisdictions, the Controller has 
                        primary responsibility for complying with applicable data 
                        protection laws. <br>
                        <b>Data Protection Authority</b><br>
                        means an independent public authority that is legally 
                        tasked with overseeing compliance with applicable data 
                        protection laws. <br>
                        <b>EEA</b> <br>
                        means the European Economic Area. <br>
                        <b>Personal Data</b><br>
                        means information that is about any individual, or from 
                        which any individual is directly or indirectly identifiable, 
                        by reference to an identifier such as a name, an identification 
                        number, location data, an online identifier or to one 
                        or more factors specific to the physical, physiological, 
                        genetic, mental, economic, cultural or social identity 
                        of that individual. <br>
                        <b>Process, Processing or Processed</b><br>
                        means anything that is done with any Personal Data, whether 
                        or not by automated means, such as collection, recording, 
                        organisation, structuring, storage, adaptation or alteration, 
                        retrieval, consultation, use, disclosure by transmission, 
                        dissemination or otherwise making available, alignment 
                        or combination, restriction, erasure or destruction. <br>
                        <b>Data Processor</b><br>
                        means any person or entity that processes personal data 
                        on behalf of the Data Controller (other than employees 
                        of the Data Controller). <br>
                        <b>Relevant Personal Data</b><br>
                        means personal data in respect of which we are the Data 
                        Controller. It expressly does not include personal data 
                        of which we are not the Data Controller. <br>
                        <b>Sensitive Personal Data</b><br>
                        means personal data about race or ethnicity, political 
                        opinions, religious or philosophical beliefs, trade union 
                        membership, physical or mental health, sexual life, any 
                        actual or alleged criminal offences or penalties, national 
                        identification number, or any other information that may 
                        be deemed to be sensitive under applicable law. <br>
                        <b>Standard Contractual Clauses</b><br>
                        means template transfer clauses adopted by the European 
                        Commission or adopted by a Data Protection Authority and 
                        approved by the European Commission. <br>
                        <b>Site</b><br>
                        means any website operated, or maintained, by us or on 
                        our behalf.<br>
                      </p>
                      <div></div>
                      <p align="CENTER">---------------------------------------------------------------------------------------------------------------------</p>
                      <p>&nbsp;</p>
                    </td>
                  </tr>
                </table>
                <p><BR>
                  
                </p>
                </td></tr> </table><br><br> 
 </div></td><td background="images/right.gif" width="2"><img src="images/trans.gif" width="1" height="1"></td></tr> 
</table><table class="table-width" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td background="http://www.tradesmen.ie/images/left.gif" width="2" height="1"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
    <td bgcolor="000000"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
    <td background="http://www.tradesmen.ie/images/right.gif" width="2" height="1"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
  <tr>
    <td background="http://www.tradesmen.ie/images/left.gif" width="2"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
	
    <td bgcolor="6C7FA0">

        <table width="100%" border="0" cellspacing="1" cellpadding="1">
          <tr>
            
          <td class="bot"> 
            <div align="center"> 
              
              <a href="http://www.tradesmen.ie/agreement.asp" class="botlink">User Agreement</a> 
              
              <img src="http://www.tradesmen.ie/images/trans.gif" width="10" height="1"> |     <img src="http://www.tradesmen.ie/images/trans.gif" width="10" height="1">
              
              <a href="http://www.tradesmen.ie/privacy-policy.asp" class="botlink">Privacy Policy</a>  |     <img src="http://www.tradesmen.ie/images/trans.gif" width="10" height="1">
			  
			  <a href="http://www.tradesmen.ie/cookie-policy.asp" class="botlink">Cookie Policy</a>  |     <img src="http://www.tradesmen.ie/images/trans.gif" width="10" height="1">

              <a href="http://www.tradesmen.ie/links.asp" class="botlink">Links</a>|     <img src="http://www.tradesmen.ie/images/trans.gif" width="10" height="1">

              <a href="http://www.tradesmen.ie/page.asp?cid=1344" class="botlink">About us</a>
              
            </div>
          </td>

          </tr>
        </table>
    </td>
    <td background="http://www.tradesmen.ie/images/right.gif" width="2"><img src="http://www.tradesmen.ie/images/trans.gif" width="1" height="1"></td>
  </tr>
</table>

<!-- Google Code for Remarketing tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1062868545;
var google_conversion_label = "3vLICLXhngMQwazo-gM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1062868545/?value=0&amp;label=3vLICLXhngMQwazo-gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>



<table class="table-width" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top"> 
    <td width="2"><img src="http://www.tradesmen.ie/images/botleft.gif" width="2" height="4"></td>

    <td background="http://www.tradesmen.ie/images/botcentre.gif" height="4"><img src="http://www.tradesmen.ie/images/trans.gif" height="4" width="1"></td>
    <td width="2"><img src="http://www.tradesmen.ie/images/botright.gif" width="2" height="4"></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td> <div align="center">Developed by Barrowvale Technology Ltd. Copyright &copy; Barrowvale Technology 
              Ltd 2008-2016. All rights reserved. </div></td>
  </tr>

  
</table>  
</div>
</body>
</html>
