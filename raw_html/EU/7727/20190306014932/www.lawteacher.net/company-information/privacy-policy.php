<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="canonical" href="https://www.lawteacher.net/company-information/privacy-policy.php">
<title>LawTeacher - Privacy Policy</title>
<meta name="description" content="Law Teacher Privacy Policy - Find out more about how Law Teacher protect your privacy as a registered data controller.">
<meta name="sloc.o1" content="NYK">
<link rel="preload" href="/css/main.css?exp=1551444281" as="style">
<link rel="preload" href="/images/law-teacher.png" as="image">
<link rel="shortcut icon" href="/favicon.ico">
<link href="/css/main.css?exp=1551444281" rel="stylesheet" type="text/css">
<meta name="theme-color" content="#005dc8">
<meta property="og:type" content="article">
<meta itemprop="og:headline" content="LawTeacher - Privacy Policy">
<meta itemprop="og:description" content="Law Teacher Privacy Policy - Find out more about how Law Teacher protect your privacy as a registered data controller.">
<meta property="og:image" content="https://www.lawteacher.net/images/lawteacher-social.jpg">
<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','gau');
        gau('create', 'UA-127492820-2', 'auto');
        gau('require', 'linkid', 'linkid.js');
                gau('set', 'anonymizeIp', true);
        gau('send', 'pageview');
    </script>
</head>
<body>
<div class="bg-primary d-none d-lg-block g-account-bar">
<ul class="container d-flex list-unstyled align-items-center mb-0 justify-content-end">
<li><a class="btn btn-sm btn-link text-offers" href="/discounts-offers/"><i class="fas mr-1 fa-heart fa-fw"></i> Offers</a></li>
<li><a href="/company-information/fair-use-policy.php" class="text-white btn btn-sm btn-link"><i class="fas fa-exclamation-triangle mr-1"></i> Fair Use Policy</a></li>
<li><a href="https://support.lawteacher.net" class="text-white btn btn-sm btn-link"><i class="fas fa-question-circle mr-1"></i> Help Centre</a></li>
<li class="notification position-relative">
<button class="btn btn-sm btn-link text-white"><i class="fas fa-bell mr-1 position-relative"></i> Notifications</button>
<div class="feed hidden position-absolute bg-light text-left border">Loading...</div>
</li>
<li><a href="/myaccount/" class="text-white btn btn-sm btn-link"><i class="fas fa-user-alt mr-1"></i> Sign In</a></li>
</ul>
</div>
<header class="g-header bg-primary">
<div class="container py-2 pt-md-1">
<div class="g-brand d-flex align-items-center justify-content-between text-white">
<button class="btn bg-primary text-white d-lg-none g-nav-toggle mr-3" aria-label="Menu"><i class="fa fa-bars"></i></button>
<a href="/" title="Go to the LawTeacher homepage" class="d-flex text-white text-nowrap text-center text-lg-left mr-auto mr-lg-0 logo">
<img src="/images/law-teacher.png" width="97" height="44" alt="LawTeacher logo" class="mr-2 align-self-center">
<div class="align-self-center d-none d-sm-block">
<span class="h4 brand-name">LawTeacher</span>
<span class="small text-uppercase d-none d-lg-block">The law essay professionals</span>
</div>
</a>
<form method="get" action="/search.php" class="g-search w-100 mx-5 d-none d-lg-block">
<div class="input-group">
<input type="search" name="q" class="form-control" placeholder="Search" aria-label="Search" required>
<div class="input-group-append">
<button class="btn bg-white" type="submit"><i class="fa fa-search"></i></button>
</div>
</div>
</form>
<div class="g-contact text-right text-nowrap d-none d-lg-block">
<span class="h4"><i class="fa fa-phone mr-1" data-fa-transform="flip-h"></i> 0115 966 7966</span>
</div>
<a class="btn bg-success text-white d-lg-none mr-2" href="/order/">Place <span class="d-none d-sm-inline">an</span> Order</a>
<a class="btn bg-primary text-white d-lg-none" href="/myaccount/" aria-label="Login to account"><i class="fa fa-user"></i></a>
</div>
</div>
<nav class="g-nav-mobile bg-white hider d-lg-none">
<div class="d-flex align-items-center justify-content-between closer border-bottom">
<button class="btn bg-light g-nav-sub-toggle rounded-0" aria-label="Menu"><i class="fa fa-chevron-left"></i></button>
<button class="btn bg-light g-nav-toggle rounded-0" aria-label="Menu"><i class="fa fa-times"></i></button>
</div>
<ul class="list-unstyled g-nav-main mt-1">
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/">Home</a></li>
<li class="border-bottom">
<a class="btn btn-link btn-block text-left" href="/services/">Writing Services <i class="fa fa-angle-right"></i></a>
<ul class="list-unstyled d-none">
<li class="border-bottom pb-2"><strong>Essay Services</strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/law-essay-writing.php">Law Essay Writing Service</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/law-assignment-writing-service.php">Law Assignment Writing Service</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/law-coursework-writing-service.php">Law Coursework Writing Service</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/skeleton-answer-service.php">Essay Outline/Plan Service</a></li>
<li class="border-bottom py-2"><strong>Dissertation Services</strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/law-dissertation-writing-service.php">Dissertation Writing Service</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/law-dissertation-proposal-writing-service.php">Dissertation Proposal Service</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/dissertation-topic-and-titles-service.php">Topics with Titles Service</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/literature-review-service.php">Literature Review Service</a></li>
<li class="border-bottom py-2"><strong>Other Services</strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/law-marking-service.php">Marking Services</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/law-report-writing-service.php">Report Writing Service</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/law-powerpoint-presentation-service.php">PowerPoint Presentation Service</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/exam-revision-service.php">Exam Revision Service</a></li>
<li class="border-bottom py-2"><strong>Service Examples</strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/services/samples/">Samples of Our Work</a></li>
</ul>
</li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/guarantees/">Guarantees</a></li>
<li class="border-bottom">
<a class="btn btn-link btn-block text-left" href="/free-resources/">Free Resources <i class="fa fa-angle-right"></i></a>
<ul class="list-unstyled d-none">
<li class="border-bottom pb-2"><strong>Essays</strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/free-law-essays/">Free Law Essays</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/example-essays/">Example Law Essays</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/problem-question-examples/">Problem Question Examples</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/free-law-coursework/">Example Law Coursework</a></li>
<li class="border-bottom py-2"><strong>Dissertations</strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/free-law-dissertations/">Full Law Dissertation Examples</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/dissertation-title-examples/">Law Dissertation Title Examples</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/law-dissertation-topics/">Law Dissertation Topic Examples</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/free-dissertation-proposals/">Law Dissertation Proposals</a></li>
<li class="border-bottom py-2"><strong>Law Help Guides</strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/law-help/essay/">Essay Writing Guide</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/law-help/dissertation/">Dissertation Writing Guide</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/law-help/coursework/">Coursework Writing Guide</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/llm/">Masters / LL.M Guides</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/law-help/BPTC/">BPTC Guide</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/law-help/lpc/">LPC Guide</a></li>
<li class="border-bottom py-2"><strong>Modules <span class="badge badge-success small">NEW</span></strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/modules/contract-law/">Contract Law</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/modules/criminal-law/">Criminal Law</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/modules/land-law/">Land Law</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/modules/public-law/">Public Law</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/modules/tort-law/">Tort Law</a></li>
<li class="border-bottom py-2"><strong>Referencing</strong></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/oscola-referencing/">OSCOLA Referencing</a></li>
</ul>
</li>
<li class="border-bottom">
<a class="btn btn-link btn-block text-left" href="/company-information/">About Us <i class="fa fa-angle-right"></i></a>
<ul class="list-unstyled d-none">
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/company-information/about-law-teacher.php">About LawTeacher</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/guarantees/">Our Guarantees</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/quality/">Our Quality Procedures</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/company-information/customer-testimonials.php">Customer Reviews</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/company-information/fair-use-policy.php">Fair Use Policy</a></li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/contact/">Contact LawTeacher</a></li>
</ul>
</li>
<li class="border-bottom"><a class="btn btn-link btn-block text-left" href="/company-information/fair-use-policy.php"> Fair Use Policy</a></li>
<li class="px-2"><a href="/order/?country=2" class="btn btn-success btn-block mt-3">Order Now</a></li>
</ul>
</nav> <nav class="g-nav py-1 d-none d-lg-block bg-white">
<div class="container">
<ul class="d-lg-flex list-unstyled align-items-center mb-0 g-nav-main">
<li class="d-none d-lg-block"><a class="btn btn-link pl-0" href="/"><i class="fa fa-home mr-1"></i> LawTeacher</a></li>
<li>
<a class="btn btn-link" href="/services/">Writing Services</a>
<div class="g-dropdown position-absolute bg-light py-4">
<div class="container">
<div class="row">
<div class="col-3">
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Essay Services</h3></li>
<li><a href="/services/law-essay-writing.php">Law Essay Writing Service</a></li>
<li><a href="/services/law-assignment-writing-service.php">Law Assignment Writing Service</a></li>
<li><a href="/services/law-coursework-writing-service.php">Law Coursework Writing Service</a></li>
<li><a href="/services/skeleton-answer-service.php">Essay Outline/Plan Service</a></li>
</ul>
</div>
<div class="col-3">
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Dissertation Services</h3></li>
<li><a href="/services/law-dissertation-writing-service.php">Dissertation Writing Service</a></li>
<li><a href="/services/law-dissertation-proposal-writing-service.php">Dissertation Proposal Service</a></li>
<li><a href="/services/dissertation-topic-and-titles-service.php">Topics with Titles Service</a></li>
<li><a href="/services/literature-review-service.php">Literature Review Service</a></li>
</ul>
</div>
<div class="col-3">
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Other Services</h3></li>
<li><a href="/services/law-marking-service.php">Marking Services</a></li>
<li><a href="/services/law-report-writing-service.php">Report Writing Service</a></li>
<li><a href="/services/law-powerpoint-presentation-service.php">PowerPoint Presentation Service</a></li>
<li><a href="/services/exam-revision-service.php">Exam Revision Service</a></li>
 </ul>
</div>
<div class="col-3">
<a href="/services/samples/">
<figure class="bg-white border">
<img class="lazy w-100 loaded" src="/images/homepage/link-boxes/placeholder.png" data-src="/images/homepage/link-boxes/legal-library.jpg" alt="A library of essays">
<figcaption class="text-center p-2">
Samples of Our Work
</figcaption>
</figure>
</a>
</div>
</div>
</div>
</div>
</li>
<li><a class="btn btn-link" href="/guarantees/">Guarantees</a></li>
<li>
<a class="btn btn-link" href="/free-resources/">Free Resources</a>
<div class="g-dropdown position-absolute bg-light py-4">
<div class="container">
<div class="row">
<div class="col-3">
<ul class="list-unstyled mb-3">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Essays</h3></li>
<li><a href="/free-law-essays/">Free Law Essays</a></li>
<li><a href="/example-essays/">Example Law Essays</a></li>
<li><a href="/problem-question-examples/">Problem Question Examples</a></li>
<li><a href="/free-law-coursework/">Example Law Coursework</a></li>
</ul>
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Summaries</h3></li>
<li><a href="/cases/">Case Summaries</a></li>
<li><a href="/acts/">Act Summaries</a></li>
</ul>
</div>
<div class="col-3">
<ul class="list-unstyled mb-3">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Dissertations</h3></li>
<li><a href="/free-law-dissertations/">Full Law Dissertation Examples</a></li>
<li><a href="/dissertation-title-examples/">Law Dissertation Title Examples</a></li>
<li><a href="/law-dissertation-topics/">Law Dissertation Topic Examples</a></li>
<li><a href="/free-dissertation-proposals/">Law Dissertation Proposals</a></li>
</ul>
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Referencing</h3></li>
<li><a href="/oscola-referencing/">OSCOLA Referencing</a></li>
</ul>
</div>
<div class="col-3">
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Law Help Guides</h3></li>
<li><a href="/law-help/essay/">Essay Writing Guide</a></li>
<li><a href="/law-help/dissertation/">Dissertation Writing Guide</a></li>
<li><a href="/law-help/coursework/">Coursework Writing Guide</a></li>
<li><a href="/llm/">Masters / LL.M Guides</a></li>
<li><a href="/law-help/BPTC/">BPTC Guide</a></li>
<li><a href="/law-help/lpc/">LPC Guide</a></li>
</ul>
</div>
<div class="col-3">
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Modules <span class="badge badge-success small">NEW</span></h3></li>
<li><a href="/modules/contract-law/">Contract Law</a></li>
<li><a href="/modules/criminal-law/">Criminal Law</a></li>
<li><a href="/modules/land-law/">Land Law</a></li>
<li><a href="/modules/public-law/">Public Law</a></li>
<li><a href="/modules/tort-law/">Tort Law</a></li>
</ul>
</div>
</div>
</div>
</div>
</li>
<li>
<a class="btn btn-link" href="/company-information/">About Us</a>
<div class="g-dropdown position-absolute bg-light py-4">
<div class="container">
<div class="row">
<div class="col-3">
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">About</h3></li>
<li><a href="/company-information/about-law-teacher.php">About LawTeacher</a></li>
<li><a href="/guarantees/">Our Guarantees</a></li>
<li><a href="/quality/">Our Quality Procedures</a></li>
<li><a href="/company-information/customer-testimonials.php">Customer Reviews</a></li>
<li><a href="/company-information/fair-use-policy.php">Fair Use Policy</a></li>
</ul>
</div>
<div class="col-3">
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Company Information</h3></li>
<li><a href="https://support.lawteacher.net/hc/en-us/">FAQs</a></li>
<li><a href="/company-information/is-this-cheating.php">Is This Cheating?</a></li>
<li><a href="/freelance-writing-jobs.php">Write for LawTeacher</a></li>
<li><a href="/discounts-offers/">Special Offers</a></li>
</ul>
</div>
<div class="col-3">
<ul class="list-unstyled">
<li><h3 class="h5 text-uppercase border-bottom pb-1">Contact</h3></li>
<li><a href="/contact/">Contact LawTeacher</a></li>
</ul>
</div>
<div class="col-3">
<a href="/myaccount/">
<figure class="bg-white border">
<img class="lazy w-100 loaded" src="/images/homepage/link-boxes/placeholder.png" data-src="/images/homepage/link-boxes/sign-in.jpg" alt="A secure vault">
<figcaption class="text-center p-2">
Account Login
</figcaption>
</figure>
</a>
</div>
</div>
</div>
</div>
</li>
<li class="ml-auto"><a href="/order/?country=2&amp;product=1" class="btn btn-info btn-block px-5 py-1" data-track-name="Header Order Now">Order Now</a></li>
</ul>
</div>
</nav>
</header>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-label="Modal" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
<div class="modal-content l">
<div class="modal-body text-center">
Loading
</div>
</div>
<div class="modal-content c d-none">
<div class="modal-body">
</div>
</div>
</div>
</div>
<main>
<div class="container company-info">
<article class="mt-5">
<h1>Privacy Policy</h1>
<p>Law Teacher is a website belonging to All Answers Limited, a UK registered company (No. 4964706) and registered data controller (No. Z1821391). We can be contacted using the details given at the bottom of this page.</p>
<p>This privacy policy tells you what to expect when we collect and process your personal data, the rights that are available to you and how you can exercise them.</p>
<h2>What we do with your data</h2>
<p>We will never sell your personal data for any reason, though sometimes it will be necessary to transfer your data to a third party to be processed. Your data is processed for certain specific reasons:</p>
<h3>Essential data</h3>
<p>To create an online account so you can manage and download your orders, we generate a unique identifier and use your email address as a username.</p>
<p>So that we can get in touch with you about your order, we need to know your name, email address, phone number and any messages you have sent through your online account.</p>
<p>On the order form, we ask for your country of study. This is because we are required to know the country in which you are downloading your order for VAT purposes.</p>
<p>To process your order properly, we also need to keep details about the assignment you are asking us to complete as well as any files you have uploaded to your order. You should try to remove all personal data from anything you upload, but we will do our best to anonymise it before passing it on to our writers.</p>
<p>Sometimes, when a file is too large to upload to our database, we will ask you to use a service called WeTransfer to send it to us. These files remain on WeTransfer's servers for 28 days, after which they are automatically deleted. You can read WeTransfer's privacy policy <a href="https://wetransfer.com/legal/privacy" rel="nofollow noopener" target="_blank">here</a>.</p>
<h3>Marketing</h3>
<p>If you have explicitly consented to receive direct marketing from us, or if you have ordered from us in the past, we will sometimes use your name and email address to send you offers and promotions about the services we provide. We do this using a service called Mailchimp. You can read Mailchimp's privacy policy <a href="https://mailchimp.com/legal/privacy/" rel="nofollow noopener" target="_blank">here</a>.</p>
<p>Similarly, we might also use your name and phone number to text you about offers and promotions that we are currently running. We do this using a service called Esendex. You can read Esendex's privacy policy <a href="https://www.esendex.co.uk/privacy-policy" rel="nofollow noopener" target="_blank">here</a>.</p>
<p>We will only ever call you about an existing service you have asked us to provide or to talk about something important related to your account.</p>
<h3>Feedback</h3>
<p>After we have completed your order, we pass your name, email address, order number and localisation data, to a company called Reviews.co.uk, who provide an independent product review service. They will then email you, asking for feedback on your order, and any feedback will be posted publicly on the <a rel="nofollow noopener" target="_blank" href="https://www.reviews.co.uk/company-reviews/store/ukessays-com">Reviews.co.uk website</a>. You can read Reviews.co.uk's privacy policy <a rel="nofollow noopener" target="_blank" href="https://www.reviews.co.uk/front/user-privacy-policy">here</a>.</p>
<p>Sometimes we use a service called Survey Monkey to administer surveys so that we can gather feedback from our customers and improve the services we provide. Survey Monkey use your IP address to log responses to surveys so that we know how many people have completed them. In order to encourage people to provide feedback, we might offer a prize draw for respondents. If so, we will ask for an email address to be provided in the survey, but this is entirely optional and is only used to contact the winner of the prize. You can read Survey Monkey's privacy policy <a href="https://www.surveymonkey.co.uk/mp/legal/privacy-policy/" rel="nofollow noopener" target="_blank">here</a>.</p>
<h3>Other purposes</h3>
<p>Sometimes, we offer coupons to individual customers for use with future orders. To ensure only the intended recipient of the coupon can use it, we store their email address alongside the coupon details. These addresses are not used for anything other than the administration of coupons.</p>
<p>When you place an order, our system stores your IP address. This address is used to work out your time zone so that we can call you during hours that are convenient for you. It is also used to identify when a customer is using multiple accounts to place their orders.</p>
<p>For payments over a certain amount, we ask for documents to prove you are the registered account holder. This is so that we can prevent fraudulent transactions, such as people paying with stolen card details.</p>
<p>In order to establish the facts in case of a legal dispute, we archive anything that might be relevant to the negotiation or performance of a contract (for example, messages or order instructions). Access to this archived data is strictly controlled so that only a few privileged employees can see it, and it is only ever used for the specific purpose of supporting a legal case. Some of this data may be shared with our legal counsel in the unlikely event of a dispute.</p>
<h3>Cookies</h3>
<p>Our websites set cookies in your browser to improve your experience and to make everything run smoothly. For more information, please read our cookie policy.</p>
<h2>Your rights</h2>
<p>By law, you have a number of rights available to you when you give us your personal data. You can ask for access to any of your personal data that we hold; you can ask us to correct any inaccuracies in your personal data; you can ask us to delete your personal data where there is no longer any good reason for us to have it; you can object to the processing of your data in some cases; and, where you have given consent, you are able to withdraw that consent at any time. Under certain circumstances, such as if the data is material to legal proceedings, you can also ask us to temporarily stop processing your personal data. Where your data is automatically processed, you can also ask for a copy of your data in a machine-readable format.</p>
<p>If you would like to exercise any of your rights listed above, please send your request to our data protection officer at <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="7e1a0e113e1f12121f100d091b0c0d501d11500b1550">[email&#160;protected]</a> After we have received proof of your identity, we will respond to your request within one calendar month.</p>
<p>If you are not happy with how we have handled your personal data, you have the right to lodge a complaint with the Information Commissioner's Office.</p>
<h2>How we keep your data safe</h2>
<h3>Security measures</h3>
<p>We use a variety of technical and organisational measures to make sure your personal data is stored and transmitted securely. For example, we make diligent use of encryption whenever personal data is being transmitted to a third party and we make regular backups in case the original copy of your data is lost. All of our core database functionality, including most of your personal data, is processed using Amazon Web Services (AWS), who are a cloud computing provider. AWS are certified to be ISO 27001 compliant, which is the gold standard in information security, so you can rest assured that your personal data is secure and protected.</p>
<p>If you communicate with us by email over the Internet, you should be aware that the nature of the Internet is such that unencrypted communication may not be secure and may pass through several different countries en route to us. Please do not email us with confidential or sensitive information such as your credit card details. We cannot accept responsibility for unauthorised access to your information that is outside our control.</p>
<h3>Data retention policy</h3>
<p>We only hold on to your data for as long as we need it to fulfil one of the purposes it was originally collected for, such as to provide a service, to gather feedback or to comply with a legal obligation. This means that retention policies for your data can differ considerably depending on the context and the way it is used. When deciding on a retention policy for any type of data, we use the following criteria:</p>
<ul>
<li><strong>How long is the data needed for?</strong> - For example, if you have not logged in to your account for several years, we will remove you from our mailing lists.</li>
<li><strong>What did we say we would do with the data when we collected it?</strong> - If the purposes for which we originally collected the data are no longer relevant, there is no need to keep it.</li>
<li><strong>Do we have a legal obligation to keep the data?</strong> - For example, financial information needs to be kept for a minimum amount of time for tax reasons.</li>
<li><strong>Does holding the data carry an inherent risk?</strong> - In the unlikely event of a data breach, we want to ensure that nothing sensitive is stolen. We will therefore have reduced retention periods for data we consider to be sensitive.</li>
</ul>
<p>However, in general, we securely archive most of your account data after four years of inactivity (see 'Other purposes' above) and then permanently delete all of your personal data after six years of inactivity.</p>
<h3>Transfers to countries outside the EEA</h3>
<p>Some of the third parties we use to process your data are based in countries which do not have the same standard of data protection laws that EU citizens enjoy. In these cases, we have made sure there are safeguards in place to protect your rights and interests. For example, Survey Monkey and Mailchimp are both located in the United States, but we have made sure that they both comply with the EU-U.S. Privacy Shield Framework, which guarantees the same level of protection.</p>
<h3>Children</h3>
<p>If you are 13 or under, you must have permission from a parent or guardian before you give us your personal information. If we find that we have received information from you without the appropriate consent, we reserve the right to cancel all transactions and services and remove all personal data that you have supplied. You will be able to re-submit the information when you have the required permission.</p>
<h3>Links to other websites</h3>
<p>Our websites may contain links to external third-party websites. We are not responsible for the privacy and security of these websites unless they belong to All Answers Limited. This privacy policy applies only to Law Teacher.</p><script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script> </article>
</div>
</main>
<footer class="g-footer py-4 py-md-5 text-white" data-track="Footer">
<div class="container">
<div class="g-footer-last-call px-5 shadow py-4 rounded d-sm-flex justify-content-between align-items-center my-3 my-sm-5 text-center text-sm-left">
<h3 class="mb-4 mb-sm-0">Ready to get started?</h3>
<a href="/order/?country=2&amp;product=1" class="btn bg-white text-dark btn-lg" data-track-name="Last call CTA">Start your order</a>
</div>
<div class="row py-0 py-md-5">
<div class="col-sm-6 col-md-4 order-4 order-md-0">
<div class="d-flex align-items-center mb-4" data-track-name="AllAnswers logo">
<img src="/images/icons/all-answers.png" alt="All Answers Ltd" height="61" class="mr-2">
<h6 class="mb-0">
Part of All Answers Ltd
</h6>
</div>
<a href="https://www.facebook.com/LawTeacherNet" target="_blank" rel="nofollow noopener" aria-label="Facebook" class="mr-5">
<i class="fab fa-2x fa-facebook-f"></i>
</a>
<a href="https://twitter.com/LawTeacherNet" target="_blank" rel="nofollow noopener" aria-label="LinkedIn" class="mr-5">
<i class="fab fa-2x fa-twitter"></i>
</a>
<a href="https://plus.google.com/+LawteacherNetUK" target="_blank" rel="nofollow noopener" aria-label="Google Plus">
<i class="fab fa-2x fa-google-plus-g"></i>
</a>
</div>
<div class="col-sm-6 col-md-3">
<h5 class="text-uppercase h6 mt-3 mb-4">Services</h5>
<ul class="list-unstyled">
<li class="my-3"><a href="/services/law-essay-writing.php">Law Essay Writing Service</a></li>
<li class="my-3"><a href="/services/law-dissertation-writing-service.php">Law Dissertation Writing Service</a></li>
<li class="my-3"><a href="/services/law-assignment-writing-service.php">Law Assignment Writing Service</a></li>
<li class="my-3"><a href="/services/">All Law Services</a></li>
</ul>
</div>
<div class="col-sm-6 col-md-3">
<h5 class="text-uppercase h6 mt-3 mb-4">Useful Resources</h5>
<ul class="list-unstyled">
<li class="my-3"><a href="/free-law-essays/">Law Essays</a></li>
<li class="my-3"><a href="/cases/">Case Summaries</a></li>
<li class="my-3"><a href="/acts/">Act Summaries</a></li>
<li class="my-3"><a href="/problem-question-examples/">Problem Questions</a></li>
<li class="my-3"><a href="/oscola-referencing/">OSCOLA Referencing Tool </a></li>
<li class="my-3"><a href="/llm/">LLM Resources </a></li>
<li class="my-3"><a href="/law-help/">Law Help </a></li>
<li class="my-3"><a href="/modules/">Study Modules</a></li>
</ul>
</div>
<div class="col-sm-6 col-md-2">
<h5 class="text-uppercase h6 mt-3 mb-4">Company</h5>
<ul class="list-unstyled">
<li class="my-3"><a href="/company-information/about-law-teacher.php">About</a></li>
<li class="my-3"><a href="/company-information/fair-use-policy.php">Fair Use Policy</a></li>
<li class="my-3"><a href="/company-information/complaints-policy.php">Complaints</a></li>
<li class="my-3"><a href="https://support.lawteacher.net/hc/en-us">Help Centre</a></li>
<li class="my-3"><a href="/freelance-writing-jobs.php">Become a Researcher</a></li>
</ul>
</div>
</div>
<p class="small mb-0 legal my-5">Copyright &copy; 2003 - 2019 - LawTeacher is a trading name of All Answers Ltd, a company registered in England and Wales. Company Registration No: 4964706. VAT Registration No: 842417633. Registered Data Controller No: Z1821391. Registered office: Venture House, Cross Street, Arnold, Nottingham, Nottinghamshire, NG5 7PJ.</p>
<ul class="list-unstyled d-sm-flex my-5 small legal">
<li class="mr-3 mb-3 mb-sm-0"><a href="/company-information/privacy-policy.php">Privacy Policy</a></li>
<li class="mr-3 mb-3 mb-sm-0"><a href="/company-information/terms-and-conditions.php">Terms &amp; Conditions</a></li>
<li><a href="/company-information/cookie-policy.php">Cookies</a></li>
</ul>
</div>
</footer>

<div class="g-service-reveal reveal bg-success pt p-3 text-center text-white" data-reveal="global-desktop" data-offset="600" data-track="Service reveal (b-2)">
<div class="container position-relative">
<button type="button" class="close text-white" aria-label="Close" data-track-name="Close">
<span aria-hidden="true">&times;</span>
</button>
<h3><a href="/discounts-offers/" class="text-white"><u>Current Offers</u></a></h3>
</div>
</div>

<script src="/js/bundle.js?exp=1551365229"></script>
<script defer src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5901b28903299bb6"></script>
</body>
</html>
