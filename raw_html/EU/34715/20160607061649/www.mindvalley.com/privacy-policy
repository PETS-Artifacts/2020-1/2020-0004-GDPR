<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Welcome to Mindvalley. We incubate and grow businesses that innovate on transformational education, dedicated to unleashing humanity's greatest potential." /><meta property="og:title" content="Mindvalley: Be Extraordinary" />
    <meta property="og:type" content="website" />
    <meta name="author" content="Denys Loveiko">
    <meta property="og:site_name" content="Mindvalley" />
    <meta property="og:image" content="http://www.mindvalley.com/assets/images/facebook.jpg" />
    <meta property="og:description" content="Welcome to Mindvalley. We incubate and grow businesses that innovate on transformational education, dedicated to unleashing humanity's greatest potential." />
    <meta property="og:url" content="http://www.mindvalley.com/" />
    <meta property="fb:app_id" content="260105907385613" /> 

    <title>Welcome to Mindvalley</title>

    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/page.min.css" />
    <link rel="stylesheet" type="text/css" href="css/queries.css" />
    
    
    <link rel="apple-touch-icon" sizes="57x57" href="imgs/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="imgs/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="imgs/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="imgs/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="imgs/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="imgs/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="imgs/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="imgs/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="imgs/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="imgs/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="imgs/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="imgs/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="imgs/favicon/favicon-16x16.png">
    <link rel="manifest" href="imgs/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="imgs/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
 
    <!--[if IE]>
        <script type="text/javascript">
             var console = { log: function() {} };
        </script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    

    <script type="text/javascript" src="js/page.min.js"></script>
    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            

            /*
			* Preloader intialization
			*/
            
            setTimeout(function(){
                preload.addClass('active-preload');
            }, 1400);
            
            setTimeout(function(){ 
                intro_overlay.addClass('overlay-active');
            }, 1650);
            
            setTimeout(function(){
                blackout.fadeOut(900);
                explore_arrow.addClass('arrow-explore-active');
            }, 1800);
            
            setTimeout(function(){
                title_first_line.addClass('active-title-first');
                title_second_line.addClass('active-title-second');
                title_line.addClass('active-line');
                title_txt_cta.addClass('active-cta');
                desc_text.addClass('decsription-short-text-active');                    
                explore_arrow.removeClass('arrow-explore-active');
                blackout.fadeOut(400);
                social.addClass('social-active');
            }, 2200);
            
            setTimeout(function(){
                logo_body.addClass('logo-body-active');
            }, 2400);
            
            
            var intro_overlay = $('.overlay'),  
                explore_block = $(".explore-block"),
                explore_arrow = $('.arrow-explore'),
                title_cta = $('.CoverTitles-cta'),
                title_line = $('.CoverSubtitle-line'),
                title_txt_cta = $('.CoverTitles-cta-text'),
                core = $('#core'),
                go_next = $('#arrow-next'),
                go_prev = $('#arrow-prev'),
                title_first_line = $('.CoverTitles-textLine-inner-first'),
                title_second_line = $('.CoverTitles-textLine-inner-second'),
                desc_text = $('.decsription-short-text'),
                logo_body = $('.logo-body'),
                preload = $('#preloader'),
                blackout = $('.overlay-black'),
                section_blackout = $('.section-blackout'),
                social = $('.social li'),
                fb = $('#fb'),
                twitter = $('#twitter'),
                insta = $('#insta'),
                youtube = $('#youtube'),
                teleport = $('.overlay-translate'),
                burger_icon = $('#nav-icon1'),
                accordion = $('.accordion'),
                menu_list = $('.submenu li'),
                menu_page = $('.accordion .link'),
                section = $('.section-img');
            
            
            /*
			* Explore button intialization
			*/
            
                $(function() {
	               var Accordion = function(el, multiple) {
		           this.el = el || {};
		           this.multiple = multiple || false;

		          // Variables privadas
		          var links = this.el.find('.link');
		          // Evento
		          links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
                  }

	           Accordion.prototype.dropdown = function(e) {
		          var $el = e.data.el;
			      $this = $(this),
			      $next = $this.next();

		          $next.slideToggle();
		          $this.parent().toggleClass('open');

		          if (!e.data.multiple) {
			         $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
		          };
	           }	

	               var accordion = new Accordion($('#accordion'), false);
              });
    
   /* Burger Menu Icon */ 
    
  
             burger_icon.click(function(){
		          $(this).toggleClass('open');
                    accordion.toggleClass('accordion-active');
     
	         });
    
             menu_list.click(function(){
		         burger_icon.toggleClass('open');
                 setTimeout(function(){
                    accordion.toggleClass('accordion-active');
                 }, 1000);
	         });
            
    /* Explore block */ 
            
                explore_block.mouseenter(function(){
                    explore_arrow.addClass('arrow-explore-hover');
                    title_cta.addClass('CoverTitles-cta-hover');
                    title_line.addClass('hover-line');
                });
            
                explore_block.mouseleave(function(){
                    explore_arrow.removeClass('arrow-explore-hover');
                    title_cta.removeClass('CoverTitles-cta-hover');
                    title_line.removeClass('hover-line');
                });
                   
    /* Prev Next buttons */ 
            
                go_next.click(function(){
                    $.fn.fullpage.moveSectionDown();
                });
    
                go_prev.click(function(){
                    $.fn.fullpage.moveSectionUp();
                });
            
            
                        
            fb.click(function() {
                window.open('https://www.facebook.com/mindvalley', '_blank');
            });
            
            twitter.click(function() {
                window.open('https://twitter.com/mindvalley', '_blank');
            });
            
            insta.click(function() {
                window.open('https://www.instagram.com/mindvalley', '_blank');
            });
            
            youtube.click(function() {
                window.open('https://www.youtube.com/user/mindvalley', '_blank');
            });
            
            
            /*
			* Particles
			*/   
            
                   
particlesJS("particles-js", {
  "particles": {
    "number": {
      "value": 80,
      "density": {
        "enable": true,
        "value_area":1200
      }
    },
    "color": {
      "value": ["#aa73ff", "#f8c210", "#83d238", "#33b1f8"]
    },
    
    "shape": {
      "type": "circle",
      "stroke": {
        "width": 0,
        "color": "#fff"
      },
      "polygon": {
        "nb_sides": 5
      },
      "image": {
        "src": "",
        "width": 100,
        "height": 100
      }
    },
    "opacity": {
      "value": 1,
      "random": false,
      "anim": {
        "enable": false,
        "speed": 0.5,
        "opacity_min": 0.1,
        "sync": false
      }
    },
    "size": {
      "value": 3,
      "random": true,
      "anim": {
        "enable": false,
        "speed": 20,
        "size_min": 0.3,
        "sync": false
      }
    },
    "line_linked": {
      "enable": true,
      "distance": 120,
      "color": "#919191",
      "opacity": 0.4,
      "width": 1
    },
  },
  "interactivity": {
    "detect_on": "canvas",
    "events": {
      "onhover": {
        "enable": true,
        "mode": "grab"
      },
      "onclick": {
        "enable": false
      },
      "resize": true
    },
    "modes": {
      "grab": {
        "distance": 140,
        "line_linked": {
          "opacity": 1
        }
      },
      "bubble": {
        "distance": 400,
        "size": 40,
        "duration": 2,
        "opacity": 8,
        "speed": 1
      },
      "repulse": {
        "distance": 200,
        "duration": 0.4
      },
      "push": {
        "particles_nb": 4
      },
      "remove": {
        "particles_nb": 2
      }
    }
  },
  "retina_detect": true
});
    
            /*
			 * Core
			 */   
            
			$('#core').fullpage({
                sectionsColor: ['#333', '#333', '#333', '#333', '#333'],
				anchors: ['about', 'culture', 'vishen', 'book', 'team'],
                scrollingSpeed: 850,
                menu: '.submenu',
                
                afterRender: function(){
					
                },
                
                
                afterLoad: function(anchorLink, index){
                    title_first_line.addClass('active-title-first');
                    title_second_line.addClass('active-title-second');
                    desc_text.addClass('decsription-short-text-active');
                    section_blackout.addClass('section-blackout-active');
                    
                        
                    section.removeClass('section-active');
                    
                    setTimeout(function(){ 
                        social.addClass('social-active');
                    }, 300);
                    
                    
                    if(index == 1){
                    }
                    
                    if(index == 2){
                    }
                    
                    if(index == 3){
                    }
                    
                    if(index == 4){
                    }
                    
                    if(index == 5){
                    } 
                    
                },
                
                onLeave: function(index, nextIndex, direction){
                    title_first_line.removeClass('active-title-first');
                    title_second_line.removeClass('active-title-second');
                    desc_text.removeClass('decsription-short-text-active');
                    section_blackout.removeClass('section-blackout-active');
                    social.removeClass('social-active');
                    section.addClass('section-active');
                
                }

                
			});
            
            
            
        });
    </script>

</head>
<body>
    

    
    
<div class="overlay"></div>
<div id="preloader"></div>
    
 <div class="overlay-black"></div>    
    
    <a href="index.html" class="logo-body orange"></a>

<div id="nav-icon1">
  <span></span>
  <span></span>
  <span></span>
</div>


    
	<!-- Contenedor -->
	<ul id="accordion" class="accordion">
        
        <div id="particles-js"></div>

		<li class="default open">
			<div class="link home"><i class="fa fa-paint-brush"></i>Home<i class="fa fa-chevron-down"></i></div>
			<ul class="submenu">
				<li data-menuanchor="about"><a href="#about">About</a></li>
				<li data-menuanchor="culture"><a href="#culture">Culture</a></li>
				<li data-menuanchor="vishen"><a href="#vishen">Vishen</a></li>
				<li data-menuanchor="book"><a href="#book">The Book</a></li>
                <li data-menuanchor="team"><a href="#team">The Team</a></li>
			</ul>
		</li>
		<li>
			<div class="link"><i class="fa fa-code"></i>Businesses<i class="fa fa-chevron-down"></i></div>
			<ul class="submenu">
				<li><a href="businesses.html#mva">Mindvalley Academy</a></li>
				<li><a href="businesses.html#apps">Apps & Platforms</a></li>
				<li><a href="businesses.html#events">Events</a></li>
                <li><a href="businesses.html#international">International</a></li>
                <li><a href="http://evercoach.com" target="_blank">Evercoach</a></li>
			</ul>
		</li>
		<li>
            <div class="link"><i class="fa fa-code"></i>
                <a href="http://www.mindvalley.com/blog/">News</a>
                <i class="fa fa-chevron-down"></i>
            </div>
		</li>
        <li>
            <div class="link"><i class="fa fa-code"></i>
                <a href="http://careers.mindvalley.com/">Careers</a>
                <i class="fa fa-chevron-down"></i>
            </div>
		</li>
		<li><div class="link"><i class="fa fa-globe"></i>Contact<i class="fa fa-chevron-down"></i></div>
		    <ul class="submenu">
				<li><a href="contact.html#cs">Customer Support</a></li>
				<li><a href="contact.html#aff">Affiliate Partnerships</a></li>
			</ul>
		</li>
	</ul>
 
    
    <div class="middle-nav">
        <button id="arrow-next">
            <span>Next</span>
        </button>
              
        <button id="arrow-prev">
            <span>Prev</span>
        </button>      
    </div>
    
    
<div id="core">
    
    <div class="section">
        
        <div class="section-img" id="about-section"></div>
        
          <div class="CoverTitles-text">
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-first">Welcome to</div>
                </div>
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-second">Mindvalley</div>
                </div> 
            </div>
              
    <div class="decsription-short-text">
        <p>We design learning experiences and publish teachings by the best authors 
            in transformational education for 3 million students worldwide.
        </p>
                  
            <ul class="social">
                  <li style="cursor:default">Follow Us:</li>
                 <li id="fb"></li>
                 <li id="twitter"></li>
                 <li id="insta"></li>
                 <li id="youtube"></li>
              </ul>     
    </div>  
        
        <div class="explore-block">
            <div class="CoverSubtitle-line" style="background-color:#ffce00;"></div>
              <div class="CoverTitles-cta">
                  <a href="http://www.mindvalley.com/about.html" class="CoverTitles-cta-text">Learn More</a>
              </div>
              <a href="http://www.mindvalley.com/about.html"><div class="arrow-explore"></div></a>
        </div>
        
        <div class="section-blackout"></div>
        
    </div>
    
    
    
    <div class="section">
        
        <div class="section-img" id="culture-section"></div>
        
          <div class="CoverTitles-text">
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-first">Invest in</div>
                </div>
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-second">Happy</div>
                </div> 
            </div>
              
    <div class="decsription-short-text">
        <p>Our best products are our employees – discover how our multi award-winning company culture drives our productivity and our growth.
        </p>
                  
            <ul class="social">
                  <li style="cursor:default">Follow Us:</li>
                 <li id="fb"></li>
                 <li id="twitter"></li>
                 <li id="insta"></li>
                 <li id="youtube"></li>
              </ul>     
    </div>  
        
        <div class="explore-block">
            <div class="CoverSubtitle-line" style="background-color:#ffce00;"></div>
              <div class="CoverTitles-cta">
                  <a href="http://www.mindvalley.com/culture.html" class="CoverTitles-cta-text">Learn More</a>
              </div>
              <a href="http://www.mindvalley.com/culture.html"><div class="arrow-explore"></div></a>
        </div>
        
        <div class="section-blackout"></div>
        
    </div>
    
    <div class="section">
        
        <div class="section-img" id="vishen-section"></div>
            
          <div class="CoverTitles-text">
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-first">Meet</div>
                </div>
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-second">Vishen</div>
                </div> 
            </div>
              
    <div class="decsription-short-text">
        <p>Get to know the CEO and Founder of Mindvalley.
        </p>
                  
            <ul class="social">
                  <li style="cursor:default">Follow Us:</li>
                 <li id="fb"></li>
                 <li id="twitter"></li>
                 <li id="insta"></li>
                 <li id="youtube"></li>
              </ul>     
    </div>  
        
        <div class="explore-block">
            <div class="CoverSubtitle-line" style="background-color:#ffce00;"></div>
              <div class="CoverTitles-cta">
                  <a href="http://www.mindvalley.com/vishen.html" class="CoverTitles-cta-text">Learn More</a>
              </div>
              <a href="http://www.mindvalley.com/vishen.html"><div class="arrow-explore"></div></a>
        </div>
        
        <div class="section-blackout"></div>
        
    </div>
    
    <div class="section">
        
        <div class="section-img" id="book-section"></div>
        
          <div class="CoverTitles-text">
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-first">Discover</div>
                </div>
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-second">The Book</div>
                </div> 
            </div>
              
    <div class="decsription-short-text">
        <p>A book that teaches you to question your reality and redefine life and success on your own terms.
        </p>
                  
            <ul class="social">
                  <li style="cursor:default">Follow Us:</li>
                 <li id="fb"></li>
                 <li id="twitter"></li>
                 <li id="insta"></li>
                 <li id="youtube"></li>
              </ul>     
    </div>  
        
        <div class="explore-block">
            <div class="CoverSubtitle-line" style="background-color:#ffce00;"></div>
              <div class="CoverTitles-cta">
                  <a href="http://www.mindvalley.com/book.html" class="CoverTitles-cta-text">Learn More</a>
              </div>
              <a href="http://www.mindvalley.com/book.html"><div class="arrow-explore"></div></a>
        </div>
        
        <div class="section-blackout"></div>
        
    </div>
   
    <div class="section">
        
        <div class="section-img" id="team-section"></div>
            
          <div class="CoverTitles-text">
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-first">40 Nationalities</div>
                </div>
                <div class="CoverTitles-textLine-outer">
                    <div class="CoverTitles-textLine-inner-second">1 Mission</div>
                </div> 
            </div>
              
    <div class="decsription-short-text">
        <p>Our unique recruitment policy has resulted in a diverse and international tribe, collectively dedicated to hacking education worldwide.
        </p>
                  
            <ul class="social">
                  <li style="cursor:default">Follow Us:</li>
                 <li id="fb"></li>
                 <li id="twitter"></li>
                 <li id="insta"></li>
                 <li id="youtube"></li>
              </ul>     
    </div>  
        
        <div class="explore-block">
            <div class="CoverSubtitle-line" style="background-color:#ffce00;"></div>
              <div class="CoverTitles-cta">
                  <a href="http://www.mindvalley.com/team.html" class="CoverTitles-cta-text">Learn More</a>
              </div>
              <a href="http://www.mindvalley.com/team.html"><div class="arrow-explore"></div></a>
        </div>
        
        <div class="section-blackout"></div>
        
    </div>
    
       
    
</div>
    
  

</body>
</html>
<!-- Localized -->