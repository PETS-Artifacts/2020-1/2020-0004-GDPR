<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-language" content="en" />
<title>Privacy Policy - ICGP Web Site</title>
<meta name="generator" content="SpeckCMS" />
<meta name="description" content="Privacy Policy - ICGP Web Site" />
<meta name="keywords" content="Privacy Policy - ICGP Web Site" />
<link rel="stylesheet" type="text/css" href="/stylesheets/main.css" />
<link rel="stylesheet" type="text/css" media="print" href="/stylesheets/print3.css" />
<link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon" />
<script type="text/javascript" src="/speck/javascripts/prototype.js"></script>
<script type="text/javascript" src="/speck/javascripts/scriptaculous.js"></script>
<script type="text/javascript" src="/speck/javascripts/lightbox.js"></script>
<link rel="stylesheet" href="/speck/stylesheets/lightbox.css" type="text/css" media="screen" />

</head>
<body>


<div id="icgp3_site" class="kw_privacy_policy sec_privacy_policy template_articles layout_two_columns">
	<div id="icgp3_wrapper">
		
		
		
		
		<div id="icgp3_header">
			
			<div id="icgp3_hr" style="margin-bottom:25px;width:390px;">
				<div style="margin-bottom:25px;height:28px;line-height:28px">
					
						<a id="icgp3_social" href="/go/my_icgp/login"><img src="/images/btn_login.png" /></a>
					
					<ul id="icgp3_links" style="float:right;height:28px;line-height:28px;padding-right:4px">
					<li style="margin-bottom:0"><a href="/go/about">About ICGP</a></li>
						<li style="margin-bottom:0"><a href="/go/find_a_gp">Find a GP</a></li>
						<li style="margin-bottom:0"><a href="/go/about/contact_us">Contact Us</a></li>
						<li style="margin-bottom:0"><a href="/go/my_icgp">My ICGP</a></li>
					</ul>
				</div>
				<form style="clear:right;border:none;margin:0 0 0 60px;padding:0" id="icgp3_search" method="get" action="https://www.icgp.ie/go/search">
					<input type="hidden" name="r" value="privacy_policy">
					<input type="image" src="/images/btn_search.png" width="70" height="23" alt="Search" />
					<input type="text" style="border-radius:4px;border:1px white solid;width:240px;margin-right:0" size="20" name="q" id="frm_icgp_q" style="width:225px" placeholder="Search the ICGP Website" />
				</form>
			</div>
			
			<a id="icgp3_logo" href="/"><img src="/images/logo.png" width="131" height="64" alt="ICGP Logo" /></a>
			<img src="/images/icgp_multilang.png" width="330" height="60" id="icgp3_text" alt="Irish College of General Practitioners" />
			
			<ul id="icgp_mainnav">
				<li id="mainnav_become_a_gp"><a href="/go/become_a_gp">GP Training</a></li>
				<li id="mainnav_membership"><a href="/go/membership">Membership</a></li>
				<li id="mainnav_in_the_practice"><a href="/go/in_the_practice">In Practice</a></li>
				<li id="mainnav_education"><a href="/go/courses">Education</a></li>
				<li id="mainnav_pcs"><a href="/go/pcs">Prof Competence</a></li>
				<li id="mainnav_research"><a href="/go/research">Research</a></li>
				<li id="mainnav_library"><a href="/go/library">Library</a></li>
				<li id="mainnav_events" class="lastchild"><a href="/go/search?w=events">Events</a></li>
			</ul>

		</div>
		
			<div id="icgp3_breadcrumbs">
				You are here: 
				
			<a title="Home" href="/">Home</a>
			 &gt; 
			
			<a class="selected" title="Privacy Policy" href="/go/privacy_policy">Privacy Policy</a>
			
			</div>
			
			<div id="icgp3_left">
				
			</div>
			
		<div id="icgp3_content" class="twocol">
            
			
<!-- SPPAGE BEGIN PRIVACY_POLICY -->

	<div id="content_right">
	
	</div>
	<div id="content_left">
	<div id="header_blurb">
            <h1>ICGP Privacy Policy</h1>
<p>Your privacy is very important to us, which is why we make sure you are always in control of what we do with your personal information.</p>
<p>We fully respect your right to privacy and to protection of your personal data. Any personal information which you provide to us will be treated strictly in accordance with the Data Protection Acts 1988 to 2018 and GDPR. We ask that you read this privacy statement ('Statement') carefully as it contains important information as to how we use your personal data.</p>
<h1><br />
General Information</h1>
<p>ICGP Company Ltd Limited owns and is responsible for the content of this site and a number of websites operated by groups within the Irish College of General Practitioners. Unless stated otherwise, this Privacy Statement applies only to the websites controlled by ICGP Company Ltd. Any site belonging to ICGP Company Ltd may have additional privacy statements that relate to their content, practices and use of personal information on each specific site. When visiting any of the ICGP Company Ltd websites, it is recommended that users consult the individual privacy statements available.</p>
<h1><br />
The Information We Collect About You</h1>
<p>We collect information to provide you with a better experience of using our website and applications and to facilitate our audiences, and visitors to the ICGP website (see the "How will ICGP use my information?" section below). In order to provide you with access to certain services or data within our websites and applications, or to our services and ICGP facilities we may require that you provide certain personal information. This information may include:</p>
<ul>
    <li>Your name, postal address, telephone number, fax number, e-mail address, subscriber name or screen name, and/or method of payment of membership/examination/course fees;</li>
    <li>Personal data related to your professional status, your MCN number, your job title, your practice location, your area of practice, if you are a trainee or if you contact us with a query about your membership, course or library information;&#160;</li>
    <li>Personal and contact details you give ICGP when subscribing to receive general information from ICGP (for example name, role and email address);</li>
    <li>Information we may collect via cookies or similar technology stored on your device about how you use our website</li>
    <li>Your IP address;</li>
    <li>Information from our web servers about online activity</li>
    <li>Information you provide if you report a problem with our website or services</li>
</ul>
We will collect and process your personal information in accordance with this Notice.<br />
<br />
If you make an enquiry via our website, on the telephone or via email, or via any third party, we may collect the information you provide to us, together with any information provided by that third party. We may monitor telephone and written correspondence with you to train our staff and monitor our services.<br />
<p>&#160;</p>
<h2>Your Data Protection Rights</h2>
<p>You have a number of rights which you may exercise in respect of your personal data. If you wish to exercise any of these rights, please email <a href="http://DPO@ICGP.ie" target="_blank">DPO@ICGP.ie</a>&#160;detailing the right that you wish to exercise together with official documentation confirming your identity such as passport, driving licence. We must respond to you without delay and in any event within one month (subject to limited extensions). You are entitled to lodge a complaint with the Data Protection Commissioner if you are dissatisfied with our response when you chose to exercise any of your rights below. The following are a synopsis of your data protection rights.&#160;</p>
<p class="note">Please note we are allowed to refuse your request in certain limited circumstances. If this arises we will inform you.</p>
<p><b>Your rights Under GDPR</b></p>
<p>You are legally guaranteed your rights as below:</p>
<p><b>The Right to be Informed</b><b><br />
</b>You must be made aware that your information will be processed for purposes you are aware of before it can be processed.</p>
<p><b>The Right of Access</b><br />
You may request a copy of all personal data we hold on you in our role as data controller. If you have account, you can access this information via your profile. If you do not have an account, you can do this by sending us an email to <a href="mailto:DPO@ICGP.ie">DPO@ICGP.ie</a></p>
<p>As well as your personal data, we'll provide you with:</p>
<ol>
    <li>the purposes of the processing;</li>
    <li>the categories of personal data concerned;</li>
    <li>the recipients or categories of recipient to whom the personal data have been or will be disclosed;</li>
    <li>the envisaged period for which the personal data will be stored;</li>
    <li>your rights, as laid out in this policy;</li>
    <li>the right to lodge a complaint with a supervisory authority;</li>
    <li>the source of the data</li>
    <li>any automated decision-making in use on your data, including the logic involved, as well as the significance and the envisaged consequences</li>
</ol>
<p><b>The Right to Rectification</b></p>
<p>You have the right to have your data rectified, for example, if it is incorrect or incomplete. You can exercise this right by logging in to your profile. If you do not have an account or wish to amend other data, you can do this by sending an email to <a href="mailto:DPO@ICGP.ie">DPO@ICGP.ie</a></p>
<p><b>The Right to Erasure</b><b><br />
</b>You may exercise your right to withdraw consent or to request erasure of personal data by logging into your profile and deleting it. If you do not have an account or wish to delete specific data, you can do this by sending an email to <a href="mailto:DPO@ICGP.ie">DPO@ICGP.ie</a>.<b> </b>Please note that the deletion may not affect an ongoing recruitment process where the employer has already received the data as part of the recruitment process.</p>
<p><b>The Right to Restrict Processing</b></p>
<p>You can request that we keep your data but do not process it further. You can do this by sending an email to <a href="mailto:DPO@ICGP.ie">DPO@ICGP.ie</a>.<b><br />
</b></p>
<p><b>The Right to Data Portability</b></p>
<p>You can request a copy of the information you've provided to us in a commonly used format. You can exercise this right by logging in to your profile or by sending an email to <a href="mailto:DPO@ICGP.ie">DPO@ICGP.ie</a>.</p>
<p><b>The Right to Object</b><b><br />
</b>You have the right to object to the use of your data for direct marketing purposes and profiling, should such profiling take place in the scope of direct marketing or our legitimate interest.</p>
<b>Rights in Relation to Automated Decision Making and Profiling<br />
</b>You have the right to not be subject to entirely automatic profiling algorithms. If any such algorithms are used, you have the right to know details of their operation. You can request information about any automated profiling algorithms by sending email to&#160;<a href="mailto:DPO@ICGP.ie">DPO@ICGP.ie</a>.<b><br />
<br />
</b>
<p>&#160;</p>
<h2>Discussion Groups and Forums</h2>
<p>Users are reminded that discussion groups and forums are for member's discussion. Any personal information supplied by users during these discussions is widely accessible to members of the ICGP. ICGP Company Ltd is not responsible for the content posted by users of these services.</p>
<h2><br />
Links to Other Websites</h2>
<p>This website contains links to other sites that are completely unrelated to ICGP Company Ltd. This privacy statement does not apply to these sites nor are we responsible for the content and practices of these websites.</p>
<h1><br />
Security of Your Personal Data</h1>
<p>Any personal information which the ICGP collects about you will be treated with the highest standards of security and confidentiality, strictly in accordance with the Data Protection Acts, 1988 to 2018 and GDPR.<br />
&#160;</p>
<h3>Retention and Use of Your Personal Data</h3>
<p>Use of your personal data &ndash; why and for how long:</p>
<p>We will only process any personal data which you provide to us in accordance with the purpose for which it was provided. If you are an ICGP member or Trainee we will use your personal data to provide ICGP services to you in accordance with any agreements entered into between us. For this purposes, we will store your personal data in our Client Relationship Management System ("CRM"); in accordance with the legitimate interests of the College to serve all members and to promote ICGP we may send you information by e-mail from time to time about our services that might be of interest to you, updates, white papers and events. Due to the longevity of the life of many of the services that we provide, unless notified otherwise by you we retain your personal data for your length of membership as per our Retention Policy and delete the personal data after this period has expired.</p>
<h1><br />
Sensitive Information</h1>
<p>Where we collect sensitive information for example in connection with examinations or to facilitate event administration it is provided to us by you or on your behalf voluntarily. By giving us this information, you agree that we may use such information as set out in this Notice.&#160;</p>
<p>&#160;</p>
<h1>People who work with the ICGP</h1>
<h2>The information we collect about you</h2>
<p>This section applies to individuals (other than employees) who work with the ICGP in any capacity.&#160; It covers examiners, assessors, auditors, clinicians, other advisers, members of editorial panels, representatives, and any business dealing with the ICGP as a Company or Contractor.</p>
<p>We collect your information in connection with the work you do with the ICGP and any services that you provide to the ICGP, including the recruitment/tendering/procurement process and the management of the contract and your work or services.</p>
<p>&#160;This information may include:</p>
<ul>
    <li>Your name, postal address, telephone      number, fax number, e-mail address;</li>
    <li>The information you provide in      any application form;</li>
    <li>The information you provide or      record in any electronic portfolio or documents submitted by you in      support of any application;</li>
    <li>information you provide when you      register for events and services;</li>
    <li>Your bank details and      other tax details;</li>
    <li>payment details</li>
    <li>We may also obtain information from other sources of data, including referees. We will only accept and use information about you from reputable organisations who have either obtained your permission to share your information with us or who have collated information about you from publicly available sources</li>
</ul>
<p>We will collect and process your personal information in accordance with this Notice.</p>
<ul>
    <li>We will delete the information once the agreement between us terminates and it is not to be renewed.</li>
</ul>
<h2>Candidates</h2>
<h3>Use of your personal data &ndash; why and for how long:</h3>
<p>We will only use the personal data provided only for the purpose of determining whether the candidate will become a GP Trainee. We will process the CVs and personal data provided at application stage and recorded in interview notes to take steps at your request prior to entering into GP Training. We will retain this information for the duration of your training and subsequent membership of the ICGP.<br />
<br />
If you are not called for interview, or your application is unsuccessful, your Personal Data will be deleted after 12 months.</p>
<h3>Who do we share your personal data with?</h3>
<p>CVs, applications and interview notes are shared with the HSE and assessor's/interview panels and relevant processing staff only.</p>
<h2><br />
<b>Cookie Policy</b></h2>
<p>Cookies are small pieces of information, stored in simple text files, placed on your computer or internet enabled device by a web site. Typically, it is used to ease navigation through the Website. Cookies can be read by the web site on your subsequent visits. In addition, the information stored in a cookie may relate to your browsing habits on the web page, or a unique identification number so that the web site can remember you on your return visit. By using our website, you are agreeing to our use of cookies.<br />
<br />
The ICGP website does not use 3<sup>rd</sup> party cookies, we only use First Party cookies for the following purposes:</p>
<ul>
    <li><strong>user&#8209;input cookies</strong> (session-id) such as first&#8209;party cookies to keep track of the user's input when filling online forms, shopping carts, etc., for the duration of a session or persistent cookies limited to a few hours in some cases</li>
    <li><strong>authentication</strong>&#160;cookies, to identify the user once the user has logged in, for the duration of a session</li>
    <li><strong>user&#8209;centric security</strong>&#160;cookies, used to detect authentication abuses, for a limited persistent duration<b> </b>    <br />
    &#160;</li>
</ul>
<h3><b>Changes to this Notice</b></h3>
<p>We may change this Notice from time to time in order to reflect changes in the law and/or our privacy practices.&#160; We will update the date at the top of the Notice accordingly. We encourage you to check this Notice online for changes.</p>
            </div></div>
<!-- SPPAGE END PRIVACY_POLICY -->


		</div>
        
		<div id="icgp3_footer" style="width:968px;margin:0 auto">
			<div style="float:left;color:#e5e5e5"><a href="http://www.ionic.ie/" style="padding:0;color:#e5e5e5;text-decoration:none" title="Web Technology by Ionic">web design</a> by <a href="http://www.ionic.ie/" style="padding:0;color:#e5e5e5;text-decoration:none" title="Web Design by Ionic">ionic</a></div>
			<a href="/go/useful_links">Useful Links</a> | <a href="/go/site_map">Site Map</a> | <a href="/go/privacy_policy">Privacy Policy</a> | <a href="/go/terms_and_conditions">Terms and Conditions</a> | <a href="/go/about">About ICGP</a> |
			<a href="https://www.facebook.com/ICGPs/"><img align="middle" src="/images/lg_fb.png" alt="Visit our Facebook page" /></a>
			<a href="https://twitter.com/ICGPnews"><img align="middle" src="/images/lg_tw.png" alt="Visit our Twitter page" /></a>
		</div>
	</div>
</div>





	
	
	<script type="text/javascript">
		function icgp3_eucookies_close() {
			document.getElementById("icgp3_eucookies").remove();
		}
	</script>
	<div id="icgp3_eucookies">
		<div id="icgp3_eucookies_cont">
			<a href="/go/cookies" id="icgp3_eucookies_more">Learn More</a>
			To give you the best possible experience, our sites use cookies. Continuing with cookies enabled means you're OK with this. Click <q>Learn More</q> for more information about our cookies, and how to disable them.
			<a href="javascript:void(0)" title="Accept cookie use" id="icgp3_eucookies_hide" onclick="icgp3_eucookies_close()">&times;</a>
		</div>
	</div>
	<style type="text/css">
		#icgp3_eucookies { position: fixed; bottom: 20px; left: 0; width: 100%; font-size: 12px; line-height: 20px; }
		#icgp3_eucookies_cont { margin: 0 auto; max-width: 680px; background: #f0f0f0; color: black; padding: 10px 25px 10px 10px; border: 1px #e0e0e0 solid; position: relative; box-shadow: 0 0 10px 10px white; border-radius: 2px; }
		#icgp3_eucookies_more { background: #ddf0f3; float: right; display: block; padding: 0 10px; line-height: 38px; border: 1px #cde0e3 solid; text-decoration: none; color: #093f76; margin-left: 6px; }
		#icgp3_eucookies_hide { position: absolute; top: 0; right: 4px; color: #dd0000; text-decoration: none; font-size: 16px; }
		#icgp3_eucookies_hide:hover { color: #ff4444; }
		#icgp3_eucookies_more:hover { background-color: #e0f5f8; }
		
			@media only screen and (max-width: 680px) { #icgp3_eucookies_more { margin-top: 10px; } }
		
	</style>
	
	
	
	





</body>
</html>
