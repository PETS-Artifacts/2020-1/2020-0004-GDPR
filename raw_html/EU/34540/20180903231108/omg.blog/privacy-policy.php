<!DOCTYPE html>
<html class="no-js" lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="fb:pages" content="103388343052560" />
<link rel="profile" href="https://gmpg.org/xfn/11" />
<title>Privacy Policy | !! omg blog !! [the original, since 2003]</title>

<link rel="canonical" href="https://omg.blog/privacy-policy.php" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Privacy Policy | !! omg blog !! [the original, since 2003]" />
<meta property="og:description" content="Who we are Our website address is: https://omg.blog. What personal data we collect and why we collect it Comments When visitors leave comments on the..." />
<meta property="og:url" content="https://omg.blog/privacy-policy.php" />
<meta property="og:site_name" content="!! omg blog !! [the original, since 2003]" />
<meta property="article:publisher" content="https://www.facebook.com/omgdotblog/" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="Who we are Our website address is: https://omg.blog. What personal data we collect and why we collect it Comments When visitors leave comments on the..." />
<meta name="twitter:title" content="Privacy Policy | !! omg blog !! [the original, since 2003]" />
<meta name="twitter:site" content="@omgblog" />
<meta name="twitter:image" content="https://omg.blog/wp-content/uploads/2016/12/omg-default-featured-thumb.png" />
<meta name="twitter:creator" content="@omgblog" />
<script type='application/ld+json'>{"@context":"https:\/\/schema.org","@type":"Organization","url":"https:\/\/omg.blog\/","sameAs":["https:\/\/www.facebook.com\/omgdotblog\/","https:\/\/instagram.com\/omgblog\/","https:\/\/www.youtube.com\/channel\/UCdAN9rslo6Sc1KuE8YLD0Eg","https:\/\/www.pinterest.com\/omgblog\/","https:\/\/twitter.com\/omgblog"],"@id":"https:\/\/omg.blog\/#organization","name":"!! omg blog !!","logo":"https:\/\/omg.blog\/wp-content\/uploads\/2018\/05\/OMG-logo-final-blk.jpg"}</script>

<link rel='dns-prefetch' href='//www.google.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="!! omg blog !! [the original, since 2003] &raquo; Feed" href="https://omg.blog/feed/" />
<link rel="alternate" type="application/rss+xml" title="!! omg blog !! [the original, since 2003] &raquo; Comments Feed" href="https://omg.blog/comments/feed/" />
<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/omg.blog\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='yarppWidgetCss-css' href='https://omg.blog/wp-content/plugins/yet-another-related-posts-plugin/style/widget.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='bc_category_widget_style-css' href='https://omg.blog/wp-content/plugins/category-widget/assets/css/bc_cw_style.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='spacexchimp_p005-bootstrap-tooltip-css-css' href='https://omg.blog/wp-content/plugins/social-media-buttons-toolbar/inc/lib/bootstrap-tooltip/bootstrap-tooltip.css?ver=4.41' type='text/css' media='all' />
<link rel='stylesheet' id='spacexchimp_p005-frontend-css-css' href='https://omg.blog/wp-content/plugins/social-media-buttons-toolbar/inc/css/frontend.css?ver=4.41' type='text/css' media='all' />
<style id='spacexchimp_p005-frontend-css-inline-css' type='text/css'>

                    .sxc-follow-buttons {
                        text-align: center !important;
                    }
                    .sxc-follow-buttons li img {
                        width: 36px !important;
                        height: 36px !important;
                        margin: 1.5px !important;
                    }
                  
</style>
<link rel='stylesheet' id='mh-newsdesk-parent-style-css' href='https://omg.blog/wp-content/themes/mh_newsdesk/style.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='mh-newsdesk-child-style-css' href='https://omg.blog/wp-content/themes/omgblog/style.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='mh-style-css' href='https://omg.blog/wp-content/themes/omgblog/style.css?ver=1.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='mh-font-awesome-css' href='https://omg.blog/wp-content/themes/mh_newsdesk/includes/font-awesome.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='mh-google-fonts-css' href='https://fonts.googleapis.com/css?family=PT+Serif:300,400,400italic,600,700%7cOswald:300,400,400italic,600,700' type='text/css' media='all' />
<link rel='stylesheet' id='video-js-css' href='https://omg.blog/wp-content/plugins/video-embed-thumbnail-generator/video-js/video-js.css?ver=5.20.2' type='text/css' media='all' />
<link rel='stylesheet' id='video-js-kg-skin-css' href='https://omg.blog/wp-content/plugins/video-embed-thumbnail-generator/video-js/kg-video-js-skin.css?ver=4.6.20' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css' href='https://omg.blog/wp-includes/css/dashicons.min.css?ver=4.9.8' type='text/css' media='all' />
<link rel='stylesheet' id='kgvid_video_styles-css' href='https://omg.blog/wp-content/plugins/video-embed-thumbnail-generator/css/kgvid_styles.css?ver=4.6.20' type='text/css' media='all' />
<script type='text/javascript' src='https://omg.blog/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://omg.blog/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://omg.blog/wp-content/plugins/social-media-buttons-toolbar/inc/lib/bootstrap-tooltip/bootstrap-tooltip.js?ver=4.41'></script>
<script type='text/javascript' src='https://omg.blog/wp-content/themes/mh_newsdesk/js/scripts.js?ver=4.9.8'></script>
<link rel='https://api.w.org/' href='https://omg.blog/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://omg.blog/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://omg.blog/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.9.8" />
<link rel='shortlink' href='https://omg.blog/?p=56816' />
<link rel="alternate" type="application/json+oembed" href="https://omg.blog/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fomg.blog%2Fprivacy-policy.php" />
<link rel="alternate" type="text/xml+oembed" href="https://omg.blog/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fomg.blog%2Fprivacy-policy.php&#038;format=xml" />
<meta property="fb:pages" content="" />
<style type="text/css">
    	    		.social-nav a:hover, .logo-title, .entry-content a, a:hover, .entry-meta .entry-meta-author, .entry-meta a, .comment-info, blockquote, .pagination a:hover .pagelink { color: #f7277f; }
			.main-nav li:hover, .slicknav_menu, .ticker-title, .breadcrumb a, .breadcrumb .bc-text, .button span, .widget-title span, input[type=submit], table th, .comment-section-title .comment-count, #cancel-comment-reply-link:hover, .pagination .current, .pagination .pagelink { background: #f7277f; }
			blockquote, input[type=text]:hover, input[type=email]:hover, input[type=tel]:hover, input[type=url]:hover, textarea:hover { border-color: #f7277f; }
    	    				.widget-title, .pagination a.page-numbers:hover, .dots:hover, .pagination a:hover .pagelink, .comment-section-title { background: #222222; }
			.commentlist .depth-1, .commentlist .bypostauthor .avatar { border-color: #222222; }
    	    				.main-nav, .main-nav ul li:hover > ul, .mh-footer { background: #639eff; }
    	    				.header-top, .header-nav ul li:hover > ul, .footer-ad-wrap, .footer-1, .footer-2, .footer-3, .footer-bottom { background: #000000; }
			.social-nav a { color: #000000; }
    	    				.footer-widgets .widget-title { background: #0000ff; }
    	    		</style>
<!--[if lt IE 9]>
<script src="https://omg.blog/wp-content/themes/mh_newsdesk/js/css3-mediaqueries.js"></script>
<![endif]-->
<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<style type="text/css">.grecaptcha-badge{margin-top: 25px;}</style><link rel="icon" href="https://omg.blog/wp-content/uploads/2016/12/cropped-omg-social-square-800-32x32.jpg" sizes="32x32" />
<link rel="icon" href="https://omg.blog/wp-content/uploads/2016/12/cropped-omg-social-square-800-192x192.jpg" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://omg.blog/wp-content/uploads/2016/12/cropped-omg-social-square-800-180x180.jpg" />
<meta name="msapplication-TileImage" content="https://omg.blog/wp-content/uploads/2016/12/cropped-omg-social-square-800-270x270.jpg" />

<meta name="verify-v1" content="X1TLRoJMQBPPojASpVQhrId0rnmBMGcURqskZ1bJerc=" />
<meta name="google-site-verification" content="u_qvi7cfqy5xxxCGmPrG5dL6u3_uQjr4KrVFvt4qyvg" />
<meta name="y_key" content="7dd310d18ec1fea9" />

<link rel="shortcut icon" href="//omg.blog/favicon.png" />

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-255852-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-255852-1');
</script>


<script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
<script>
	  var googletag = googletag || {};
	  googletag.cmd = googletag.cmd || [];
	</script>
<script>
	    googletag.cmd.push(function() {
	    var mapleader =  googletag.sizeMapping().
	            addSize([1023, 1], [728, 90]).
	            addSize([767, 1], [728,90]).
	            addSize([400, 1], [[320, 50], [320, 100]]).
	            addSize([300, 1], [[320, 50], [320, 100]]).
	            build();
	
	    var maprecmulti1 =  googletag.sizeMapping().
	            addSize([1023, 1], [[300, 250], [300, 600]]).
	            addSize([767, 1], [[300, 250], [300, 600]]).
	            addSize([400, 1], [300, 250]).
	            addSize([300, 1], [300, 250]).
	            build();
	
	    var maprecmulti2 =  googletag.sizeMapping().
	            addSize([1023, 1], [[300, 250], [300, 600], [160,600]]).
	            addSize([767, 1], [[300, 250], [300, 600], [160,600]]).
	            addSize([400, 1], [300, 250]).
	            addSize([300, 1], [300, 250]).
	            build();
	
	   	googletag.defineOutOfPageSlot('/1026291/OMGBlog_Interstitial', 'Interstitial').addService(googletag.pubads());
		googletag.defineSlot('/1026291/OMGBlog_Leader_top', [[320,50], [320,100], [728, 90]], 'leader_top').defineSizeMapping(mapleader).addService(googletag.pubads());
		googletag.defineSlot('/1026291/OMGBlog_Rec1', [[300, 250], [300, 600]], 'rectangle1').defineSizeMapping(maprecmulti1).addService(googletag.pubads());
		googletag.defineSlot('/1026291/OMGBlog_Rec2', [300, 250], 'rectangle2').addService(googletag.pubads());
		googletag.defineSlot('/1026291/OMGBlog_Rec3', [[300, 250], [300, 600], [160,600]], 'rectangle3').defineSizeMapping(maprecmulti2).addService(googletag.pubads());
		googletag.defineSlot('/1026291/OMGBlog_Leader_bottom', [[320,50], [320,100], [728, 90]], 'leader_bottom').defineSizeMapping(mapleader).addService(googletag.pubads());
		googletag.pubads().enableSingleRequest();
	    	googletag.pubads().collapseEmptyDivs();
		googletag.enableServices();
		});
		</script>


<script>
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	
	fbq('init', '292534477780573');
	fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
	src="https://www.facebook.com/tr?id=292534477780573&ev=PageView&noscript=1"
	/></noscript>



<script type="text/javascript">
	  //              DO NOT IMPLEMENT                //  
	  //       this code through the following        //
	  //                                              //
	  //   Floodlight Pixel Manager                   //
	  //   DCM Pixel Manager                          //
	  //   Any system that places code in an iframe   //
	    (function () {
	        var s = document.createElement('script');
	        s.type = 'text/javascript';
	        s.async = true;
	        s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
	          + '.po.st/static/v4/post-widget.js#publisherKey=t0qs9jcpk01jo34pv8dm';
	        var x = document.getElementsByTagName('script')[0];
	        x.parentNode.insertBefore(s, x);
	     })();
	</script>


</head>
<body class="page-template-default page page-id-56816 mh-right-sb mh-loop-layout1" itemscope="itemscope" itemtype="http://schema.org/WebPage">

<div id="mh-wrapper">
<header class="mh-header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
<div class="header-wrap clearfix">
<div class="mh-col mh-1-3 header-logo">
<a href="https://omg.blog/" title="!! omg blog !! [the original, since 2003]" rel="home">
<div class="logo-wrap" role="banner">
<img src="https://omg.blog/wp-content/uploads/2016/12/omg-header-logo.jpg" height="90" width="439" alt="!! omg blog !! [the original, since 2003]" />
</div>
</a>
</div>
<aside id="enhancedtextwidget-6" class="mh-col mh-2-3 widget_text enhanced-text-widget"><div class="header-ad"><div class="textwidget widget-text">

<div id='Interstitial'>
<script>
		googletag.cmd.push(function() { googletag.display('Interstitial'); });
	</script>
</div>

<div id='leader_top'>
<script>
		googletag.cmd.push(function() { googletag.display('leader_top'); });
	</script>
</div>
</div></div></aside> </div>
<div class="header-menu clearfix">
<nav class="main-nav clearfix" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
<div class="menu-main-menu-container"><ul id="menu-main-menu" class="menu"><li id="menu-item-25617" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25617"><a href="/musical/">music</a></li>
<li id="menu-item-25618" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25618"><a href="/funny/">LOL</a></li>
<li id="menu-item-25619" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25619"><a href="/homophilic">LGBT</a></li>
<li id="menu-item-25627" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25627"><a href="/fashionistic/">fashion</a></li>
<li id="menu-item-51048" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-51048"><a href="/werk-appropriate/">drag</a></li>
<li id="menu-item-25623" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25623"><a href="/cute/">cute</a></li>
<li id="menu-item-25625" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25625"><a href="/celebrated/">gossip</a></li>
<li id="menu-item-25621" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25621"><a href="/filmongous/">film</a></li>
<li id="menu-item-25622" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25622"><a href="/televisual/">tv</a></li>
<li id="menu-item-25624" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25624"><a href="/unfortunate/">fail</a></li>
<li id="menu-item-25620" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25620"><a href="/politicious/">poli</a></li>
<li id="menu-item-25626" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25626"><a href="/artumnal/">art</a></li>
<li id="menu-item-25628" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25628"><a href="/candy/">candy</a></li>
<li id="menu-item-58993" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-58993"><a href="/qa/">Q&#038;A</a></li>
</ul></div> </nav>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
</header><div class="mh-section mh-group">
<div id="main-content" class="mh-content" role="main" itemprop="mainContentOfPage"><article id="post-56816" class="post-56816 page type-page status-publish has-post-thumbnail hentry">
<header class="entry-header">
<h1 class="entry-title page-title">
Privacy Policy </h1>
</header>
<div class="entry-content clearfix">
<h2>Who we are</h2>
<p>Our website address is: https://omg.blog.</p>
<h2>What personal data we collect and why we collect it</h2>
<h3>Comments</h3>
<p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p>
<p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available <a href="https://automattic.com/privacy/">here</a>. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p>
<h3>Media</h3>
<p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p>
<h3>Email address and contact info</h3>
<p>!! omg blog !! may ask you to register and provide information that personally identifies you, including when you register as a subscriber to receive e-mail newsletters or respond to a contest or marketing campaign. The information gathered is used to enhance the user experience on our site and to identify frequent visitors. Information collected will not be sold, distributed or otherwise disseminated to outside sources. We may use your personal information for two primary purposes:</a></p>
<p><font color="#F7277F">&raquo; </font> To alert you to new published blog entries and updates to comment threads<br />
<font color="#F7277F">&raquo; </font> To coordinate the delivery of contest prizes</p>
<p>If you do not wish to receive such mailings, please let us know by clicking the “unsubscribe” link in the email or by contacting us at subscribe [at] omg [dot] blog. We will be sure your name is removed from our list.</p>
<h3>Cookies</h3>
<p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>
<p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p>
<p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p>
<h3>Embedded content from other websites</h3>
<p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p>
<p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.</p>
<h3>Analytics</h3>
<p>We use <a href="https://policies.google.com/privacy">Google Analytics</a>, <a href="https://chartbeat.com/privacy/">Chartbeat</a>, and <a href="https://statcounter.com/about/cookies/">Statcounter</a> to keep track of important information about our visitors. This includes but is not limited to: What website you came from to get here, how long you stay for, and what kind of computer you’re using. If you like, you can opt out of Google Analytics tracking <a href="https://www.google.com/privacy_ads.html">here</a>.</p>
<h2>Who we share your data with</h2>
<p>We share visitor data with audience measurement firms <a href="https://www.quantcast.com/privacy/">Quantcast</a> and <a href="https://www.comscore.com/About-comScore/Privacy-Policy">Comscore</a> so that the value of our ad inventory is clear to potential advertisers.</p>
<p>We use third-party advertising companies to serve the ads displayed on our website. These companies may use aggregated information (not including your name, address, email or phone number) about your visits to this and other websites in order to provide advertisements about goods and services of interest to you. If you would like more information about this practice and to know your choices about not having this information used by these companies, please click <a href="http://optout.networkadvertising.org">here</a>.</p>
<h2>How long we retain your data</h2>
<p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p>
<p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p>
<h2>What rights you have over your data</h2>
<p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p>
<h2>Where we send your data</h2>
<p>We collect information about visitors who comment for the Akismet anti-spam service, which typically includes the commenter&#8217;s IP address, user agent, referrer, and Site URL (along with other information directly provided by the commenter such as their name, username, email address, and the comment itself).</p>
<p></p>
<h2>Disclaimers</h2>
<p>!! omg blog !! is protected by federal copyright law and material published in print or online may not be reproduced in whole or part without the permission of the publishers. !! omg blog !! assumes no responsibility for unsolicited materials submitted for publication. !! omg blog !! is also not responsible for claims made by advertisers or materials provided by advertisers or their agents.</p>
<h2>Contact Us</h2>
<p>If you have questions or comments regarding !! omg blog !! privacy practices, please write to publisher [at] omg [dot] blog.</p>
</div>
</article> </div>
<aside class="mh-sidebar" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
<div id="enhancedtextwidget-2" class="sb-widget clearfix widget_text enhanced-text-widget"><div class="textwidget widget-text">

<div id='rectangle1'>
<script>
		googletag.cmd.push(function() { googletag.display('rectangle1'); });
	</script>
</div>
</div></div><div id="search-3" class="sb-widget clearfix widget_search"><form role="search" method="get" class="search-form" action="https://omg.blog/">
<label>
<span class="screen-reader-text">Search for:</span>
<input type="search" class="search-field" placeholder="Search &hellip;" value="" name="s" />
</label>
<input type="submit" class="search-submit" value="Search" />
</form></div><div id="buffercode_category_widget_info-2" class="sb-widget clearfix buffercode_category_widget_info"><h4 class="widget-title"><span>OMG Q&#038;A Series</span></h4><select onchange="window.location.href = this.options[this.selectedIndex].value"><option value="" selected>Select a Q&A:</option><option value="https://omg.blog/qa-cody-critcheloe-ssion/">OMG, a Q&A with Cody Critcheloe of SSION</option><option value="https://omg.blog/omg-qa-tracey-thorn/">OMG, a Q&A with Tracey Thorn</option><option value="https://omg.blog/omg-qa-partner/">OMG, a Q&A with Partner</option><option value="https://omg.blog/omg-qa-macy-rodman/">OMG, a Q&A with Macy Rodman</option><option value="https://omg.blog/42435/">OMG, a Q&A with Jess Chen</option><option value="https://omg.blog/omg-qa-lizzo/">OMG, a Q&A with Lizzo</option><option value="https://omg.blog/omg-qa-hayley-kiyoko/">OMG, a Q&A with Hayley Kiyoko</option><option value="https://omg.blog/omg_a_qa_jimmy_somerville/">OMG, a Q&A with Jimmy Somerville</option><option value="https://omg.blog/omg_a_qa_with_chris_garneau/">OMG, a Q&A with Chris Garneau</option><option value="https://omg.blog/omg_a_qa_with_the_julie_ruins/">OMG, a Q&A with Kenny Mellman of The Julie Ruin</option><option value="https://omg.blog/omg_a_qa_with_alison_moyet/">OMG, a Q&A with Alison Moyet</option><option value="https://omg.blog/omg_a_qa_hard_ton/">OMG, a Q&A with Hard Ton</option><option value="https://omg.blog/omg_oh_dough_you_didnt_qa_cake/">OMG, a Q&A with Cakes Da Killa</option><option value="https://omg.blog/omg_a_qa_with_jessie_ware/">OMG, a Q&A with Jessie Ware</option><option value="https://omg.blog/omg_qa_shaun_j_wright/">OMG, a Q&A with Shaun J. Wright</option><option value="https://omg.blog/omg_qa_christeene/">OMG, a Q&A with CHRISTEENE</option><option value="https://omg.blog/omg_a_q_a_with_rye_rye/">OMG, a Q&A with Rye Rye</option><option value="https://omg.blog/omg_another_qa_with_perfume_ge/">OMG, another Q&A with Perfume Genius</option><option value="https://omg.blog/omg_a_qa_with_erasures_vince_c/">OMG, a Q&A with Vince Clarke of Erasure</option><option value="https://omg.blog/omg_a_qa_with_seth_bogart_from/">OMG, a Q&A with Seth Bogart of Hunx and His Punx</option><option value="https://omg.blog/omg_a_qa_with_wolfram/">OMG, a Q&A with Wolfram</option><option value="https://omg.blog/omg_the_omg_interview_liz_phai/">OMG, a Q&A: Liz Phair</option><option value="https://omg.blog/omg_a_qa_ladyfag_interviews_ca/">OMG, a Q&A: Ladyfag interviews Casey Spooner</option><option value="https://omg.blog/omg_a_qa_with_perfume_genius/">OMG, a Q&A with Perfume Genius</option><option value="https://omg.blog/omg_a_qa_with_andy_butler_from/">OMG, a Q&A with Andrew Butler of Hercules and Love Affair</option><option value="https://omg.blog/omg_a_qa_with_pop_sensation_robyn/">OMG, a Q&A with Robyn</option><option value="https://omg.blog/omg_a_qa_with_chris_willis/">OMG, a Q&A with Chris Willis</option><option value="https://omg.blog/omg_a_qa_with_scissor_sisters/">OMG, a Q&A with Ana Matronic of Scissor Sisters</option><option value="https://omg.blog/omg_a_qa_with_new_orleans_boun/">OMG, a Q&A with Big Freedia</option><option value="https://omg.blog/omg_a_qa_with_honey_soundsyste/">OMG, a Q&A with Ken Vulsion of Honey Soundsystem</option><option value="https://omg.blog/omg_a_qa_with_alison_goldfrapp/">OMG, a Q&A with Alison Goldfrapp</option><option value="https://omg.blog/omg_a_qa_vv_brown/">OMG, a Q&A with V V Brown</option><option value="https://omg.blog/omg_she_speaks_joan_rivers_1/">OMG, a Q&A with Joan Rivers</option><option value="https://omg.blog/omg_a_qa_owen_pallett/">OMG, a Q&A with Owen Pallett</option><option value="https://omg.blog/omg_a_qa_salems_john_holland/">OMG, a Q&A with John Holland of Salem</option><option value="https://omg.blog/omg_a_qa_fever_ray/">OMG, a Q&A with Fever Ray</option><option value="https://omg.blog/omg_a_qa_mr_hudson/">OMG, a Q&A with Mr. Hudson</option><option value="https://omg.blog/omg_a_qa_little_boots/">OMG, a Q&A with Little Boots</option><option value="https://omg.blog/omg_a_qa_gloria_jones/">OMG, a Q&A with Gloria Jones</option><option value="https://omg.blog/omg_interview_simon_rex/">OMG, a Q&A with Simon Rex</option><option value="https://omg.blog/omg_a_qa_kid_cudi/">OMG, a Q&A with Kid Cudi</option><option value="https://omg.blog/omg_a_qa_grizzly_bears_ed_dros/">OMG, a Q&A with Grizzly Bear's Ed Droste</option><option value="https://omg.blog/omg_a_qa_kim_ann_foxman/">OMG, a Q&A with Kim Ann Foxman</option><option value="https://omg.blog/omg_a_qa_la_roux/">OMG, a Q&A with La Roux</option><option value="https://omg.blog/omg_a_qa_james_murphy/">OMG, a Q&A with James Murphy of LCD Soundsystem</option><option value="https://omg.blog/omg_a_qa_labelle/">OMG, a Q&A with LaBelle</option><option value="https://omg.blog/omg_a_qa_roisin_murphy/">OMG, a Q&A with Róisín Murphy</option><option value="https://omg.blog/omg_a_qa_lady_gaga/">OMG, a Q&A with Lady Gaga</option><option value="https://omg.blog/omg_a_qa_bruce_labruce/">OMG, a Q&A with Bruce LaBruce</option><option value="https://omg.blog/omg_a_qa_ebony_bones/">OMG, a Q&A with Ebony Bones</option><option value="https://omg.blog/omg_interview_debbie_harry/">OMG, a Q&A with Deborah Harry</option><option value="https://omg.blog/omg_interview_vito_roccoforte/">OMG, a Q&A with Vito Roccoforte of The Rapture</option><option value="https://omg.blog/interview_jeremy_dawson_of_the/">OMG, a Q&A with Jeremy Dawson of Shiny Toy Guns</option></select></div><div id="enhancedtextwidget-3" class="sb-widget clearfix widget_text enhanced-text-widget"><h4 class="widget-title"><span>omg bits</span></h4><div class="textwidget widget-text">

<div class="pubexchange_module" id="pubexchange_rail_grid_1" data-pubexchange-module-id="2123"></div>

</div></div><div id="tagcloud-2" class="sb-widget clearfix widget_tagcloud"><h4 class="widget-title"><span>omg trending</span></h4><div class='custom-tagcloud'><a href="https://omg.blog/tag/actor/" class="tag-cloud-link tag-link-1092 tag-link-position-1" style="font-size: 12.2pt;" aria-label="actors (175 items)">actors</a> <a href="https://omg.blog/tag/american-idol/" class="tag-cloud-link tag-link-8188 tag-link-position-2" style="font-size: 8.8pt;" aria-label="American Idol (81 items)">American Idol</a> <a href="https://omg.blog/tag/anderson-cooper/" class="tag-cloud-link tag-link-4167 tag-link-position-3" style="font-size: 9.1pt;" aria-label="Anderson Cooper (86 items)">Anderson Cooper</a> <a href="https://omg.blog/tag/beyonce/" class="tag-cloud-link tag-link-1793 tag-link-position-4" style="font-size: 12.4pt;" aria-label="Beyoncé (185 items)">Beyoncé</a> <a href="https://omg.blog/tag/big-brother/" class="tag-cloud-link tag-link-692 tag-link-position-5" style="font-size: 9.4pt;" aria-label="Big Brother (93 items)">Big Brother</a> <a href="https://omg.blog/tag/britney-spears/" class="tag-cloud-link tag-link-565 tag-link-position-6" style="font-size: 12.9pt;" aria-label="Britney Spears (210 items)">Britney Spears</a> <a href="https://omg.blog/tag/civil-rights/" class="tag-cloud-link tag-link-3017 tag-link-position-7" style="font-size: 10.6pt;" aria-label="civil rights (122 items)">civil rights</a> <a href="https://omg.blog/tag/comedy/" class="tag-cloud-link tag-link-645 tag-link-position-8" style="font-size: 8.8pt;" aria-label="comedy (80 items)">comedy</a> <a href="https://omg.blog/tag/dating-apps/" class="tag-cloud-link tag-link-26563 tag-link-position-9" style="font-size: 8.6pt;" aria-label="dating apps (76 items)">dating apps</a> <a href="https://omg.blog/tag/drag-race/" class="tag-cloud-link tag-link-1624 tag-link-position-10" style="font-size: 10.1pt;" aria-label="Drag Race (109 items)">Drag Race</a> <a href="https://omg.blog/tag/homophobia/" class="tag-cloud-link tag-link-374 tag-link-position-11" style="font-size: 10.2pt;" aria-label="homophobia (112 items)">homophobia</a> <a href="https://omg.blog/tag/horror/" class="tag-cloud-link tag-link-164 tag-link-position-12" style="font-size: 8.3pt;" aria-label="horror (71 items)">horror</a> <a href="https://omg.blog/tag/instagrammer/" class="tag-cloud-link tag-link-26350 tag-link-position-13" style="font-size: 8.8pt;" aria-label="Instagrammers (81 items)">Instagrammers</a> <a href="https://omg.blog/tag/james-franco/" class="tag-cloud-link tag-link-302 tag-link-position-14" style="font-size: 9.7pt;" aria-label="James Franco (100 items)">James Franco</a> <a href="https://omg.blog/tag/justin-bieber/" class="tag-cloud-link tag-link-791 tag-link-position-15" style="font-size: 11.1pt;" aria-label="Justin Bieber (138 items)">Justin Bieber</a> <a href="https://omg.blog/tag/kanye-west/" class="tag-cloud-link tag-link-93 tag-link-position-16" style="font-size: 9.9pt;" aria-label="Kanye West (104 items)">Kanye West</a> <a href="https://omg.blog/tag/katy-perry/" class="tag-cloud-link tag-link-579 tag-link-position-17" style="font-size: 8.3pt;" aria-label="Katy Perry (72 items)">Katy Perry</a> <a href="https://omg.blog/tag/kim-kardashian/" class="tag-cloud-link tag-link-191 tag-link-position-18" style="font-size: 10.4pt;" aria-label="Kim Kardashian (117 items)">Kim Kardashian</a> <a href="https://omg.blog/tag/kitties/" class="tag-cloud-link tag-link-1046 tag-link-position-19" style="font-size: 11.3pt;" aria-label="kitties (142 items)">kitties</a> <a href="https://omg.blog/tag/lady-gaga/" class="tag-cloud-link tag-link-207 tag-link-position-20" style="font-size: 12.7pt;" aria-label="Lady Gaga (197 items)">Lady Gaga</a> <a href="https://omg.blog/tag/lindsay-lohan/" class="tag-cloud-link tag-link-197 tag-link-position-21" style="font-size: 10pt;" aria-label="Lindsay Lohan (106 items)">Lindsay Lohan</a> <a href="https://omg.blog/tag/madonna/" class="tag-cloud-link tag-link-189 tag-link-position-22" style="font-size: 14.3pt;" aria-label="Madonna (290 items)">Madonna</a> <a href="https://omg.blog/tag/magazine-covers/" class="tag-cloud-link tag-link-25853 tag-link-position-23" style="font-size: 10.4pt;" aria-label="magazine covers (116 items)">magazine covers</a> <a href="https://omg.blog/tag/mariah-carey/" class="tag-cloud-link tag-link-1958 tag-link-position-24" style="font-size: 10.4pt;" aria-label="Mariah Carey (117 items)">Mariah Carey</a> <a href="https://omg.blog/tag/miley-cyrus/" class="tag-cloud-link tag-link-237 tag-link-position-25" style="font-size: 10.8pt;" aria-label="Miley Cyrus (129 items)">Miley Cyrus</a> <a href="https://omg.blog/tag/model/" class="tag-cloud-link tag-link-669 tag-link-position-26" style="font-size: 10.9pt;" aria-label="models (130 items)">models</a> <a href="https://omg.blog/tag/naked/" class="tag-cloud-link tag-link-28163 tag-link-position-27" style="font-size: 8.5pt;" aria-label="naked (75 items)">naked</a> <a href="https://omg.blog/tag/nude/" class="tag-cloud-link tag-link-28168 tag-link-position-28" style="font-size: 8.5pt;" aria-label="nude (74 items)">nude</a> <a href="https://omg.blog/tag/obama/" class="tag-cloud-link tag-link-1331 tag-link-position-29" style="font-size: 8.1pt;" aria-label="Obama (68 items)">Obama</a> <a href="https://omg.blog/tag/olympics/" class="tag-cloud-link tag-link-518 tag-link-position-30" style="font-size: 8.2pt;" aria-label="Olympics (70 items)">Olympics</a> <a href="https://omg.blog/tag/oprah/" class="tag-cloud-link tag-link-2249 tag-link-position-31" style="font-size: 10.1pt;" aria-label="Oprah (109 items)">Oprah</a> <a href="https://omg.blog/tag/oscars/" class="tag-cloud-link tag-link-2248 tag-link-position-32" style="font-size: 9pt;" aria-label="Oscars (85 items)">Oscars</a> <a href="https://omg.blog/tag/real-housewives/" class="tag-cloud-link tag-link-4225 tag-link-position-33" style="font-size: 9.1pt;" aria-label="Real Housewives (87 items)">Real Housewives</a> <a href="https://omg.blog/tag/reality-tv/" class="tag-cloud-link tag-link-2908 tag-link-position-34" style="font-size: 17pt;" aria-label="reality TV (541 items)">reality TV</a> <a href="https://omg.blog/tag/rihanna/" class="tag-cloud-link tag-link-580 tag-link-position-35" style="font-size: 12pt;" aria-label="Rihanna (168 items)">Rihanna</a> <a href="https://omg.blog/tag/robert-pattinson/" class="tag-cloud-link tag-link-3252 tag-link-position-36" style="font-size: 9.1pt;" aria-label="Robert Pattinson (87 items)">Robert Pattinson</a> <a href="https://omg.blog/tag/rugby/" class="tag-cloud-link tag-link-3547 tag-link-position-37" style="font-size: 8pt;" aria-label="rugby (67 items)">rugby</a> <a href="https://omg.blog/tag/rupaul/" class="tag-cloud-link tag-link-709 tag-link-position-38" style="font-size: 9.8pt;" aria-label="RuPaul (102 items)">RuPaul</a> <a href="https://omg.blog/tag/russia/" class="tag-cloud-link tag-link-517 tag-link-position-39" style="font-size: 8.5pt;" aria-label="Russia (74 items)">Russia</a> <a href="https://omg.blog/tag/sports/" class="tag-cloud-link tag-link-2058 tag-link-position-40" style="font-size: 12.3pt;" aria-label="sports (179 items)">sports</a> <a href="https://omg.blog/tag/star-wars/" class="tag-cloud-link tag-link-1236 tag-link-position-41" style="font-size: 8pt;" aria-label="Star Wars (67 items)">Star Wars</a> <a href="https://omg.blog/tag/taylor-swift/" class="tag-cloud-link tag-link-1666 tag-link-position-42" style="font-size: 8.9pt;" aria-label="Taylor Swift (82 items)">Taylor Swift</a> <a href="https://omg.blog/tag/transgender/" class="tag-cloud-link tag-link-590 tag-link-position-43" style="font-size: 11.3pt;" aria-label="transgender (144 items)">transgender</a> <a href="https://omg.blog/tag/trump/" class="tag-cloud-link tag-link-4316 tag-link-position-44" style="font-size: 12.6pt;" aria-label="Trump (196 items)">Trump</a> <a href="https://omg.blog/tag/zac-efron/" class="tag-cloud-link tag-link-305 tag-link-position-45" style="font-size: 10.5pt;" aria-label="Zac Efron (118 items)">Zac Efron</a></div></div><div id="enhancedtextwidget-4" class="sb-widget clearfix widget_text enhanced-text-widget"><div class="textwidget widget-text">

<div id='rectangle3'>
<script>
	googletag.cmd.push(function() { googletag.display('rectangle3'); });
	</script>
</div>
</div></div><div id="recent-comments-2" class="sb-widget clearfix widget_recent_comments"><h4 class="widget-title"><span>Recent Comments</span></h4><ul id="recentcomments"><li class="recentcomments"><span class="comment-author-link">Daniel Lachowski</span> on <a href="https://omg.blog/omg-hes-naked-reality-star-morten-kjeldsen-from-ex-on-the-beach-denmark/#comment-242611">OMG, he&#8217;s naked: Reality star Morten Kjeldsen from &#8216;EX ON THE BEACH: DENMARK&#8217;</a></li><li class="recentcomments"><span class="comment-author-link">scotshot</span> on <a href="https://omg.blog/omg-quote-of-the-day-anti-lgbtq-hypocrite-alex-jones-responds-to-the-trans-adult-site-seen-on-his-phone-during-broadcast/#comment-242610">OMG, quote of the day: Anti-trans InfoWars host Alex Jones explains why trans adult site was on his phone during broadcast</a></li><li class="recentcomments"><span class="comment-author-link">krinkles</span> on <a href="https://omg.blog/omg-watch-busy-phillips-tries-to-recreate-lohans-iconic-mykonos-party-dance/#comment-242609">OMG, WATCH: Busy Phillips tries to recreate Lohan&#8217;s iconic Mykonos party dance</a></li><li class="recentcomments"><span class="comment-author-link">PretenderNX01</span> on <a href="https://omg.blog/omg-his-butt-john-krasinski-in-series-jack-ryan/#comment-242608">OMG, his butt: John Krasinski in series &#8216;Jack Ryan&#8217;</a></li><li class="recentcomments"><span class="comment-author-link">HG</span> on <a href="https://omg.blog/omg-hes-naked-reality-star-morten-kjeldsen-from-ex-on-the-beach-denmark/#comment-242607">OMG, he&#8217;s naked: Reality star Morten Kjeldsen from &#8216;EX ON THE BEACH: DENMARK&#8217;</a></li></ul></div><div id="categories-5" class="sb-widget clearfix widget_categories"><h4 class="widget-title"><span>Categories</span></h4><form action="https://omg.blog" method="get"><label class="screen-reader-text" for="cat">Categories</label><select name='cat' id='cat' class='postform'>
<option value='-1'>Select Category</option>
<option class="level-0" value="15">Artumnal</option>
<option class="level-0" value="119">Camp</option>
<option class="level-0" value="25">Canadien</option>
<option class="level-0" value="22">Candy</option>
<option class="level-0" value="18">Celebrated</option>
<option class="level-0" value="20">Consensual</option>
<option class="level-0" value="23">Cute</option>
<option class="level-0" value="12">Dangerous</option>
<option class="level-0" value="13">Delicious</option>
<option class="level-0" value="27">Fashionistic</option>
<option class="level-0" value="26">Faux</option>
<option class="level-0" value="4">Filmongous</option>
<option class="level-0" value="19">Filthy</option>
<option class="level-0" value="9">Funny</option>
<option class="level-0" value="11">Gamely</option>
<option class="level-0" value="14">Geeky</option>
<option class="level-0" value="773">Hey Boo</option>
<option class="level-0" value="6">Homophilic</option>
<option class="level-0" value="24">Lesbionic</option>
<option class="level-0" value="17">Literary</option>
<option class="level-0" value="44">Movie Memories</option>
<option class="level-0" value="37">mpFree</option>
<option class="level-0" value="8">Musical</option>
<option class="level-0" value="3">New Yorkical</option>
<option class="level-0" value="5">Politicious</option>
<option class="level-0" value="28">Pop</option>
<option class="level-0" value="5967">Q&amp;A</option>
<option class="level-0" value="16">Revolutionary</option>
<option class="level-0" value="87">Social</option>
<option class="level-0" value="4411">Sponsorial</option>
<option class="level-0" value="135">Sporty</option>
<option class="level-0" value="7">Televisual</option>
<option class="level-0" value="28414">Theatrical</option>
<option class="level-0" value="942">Time Machine</option>
<option class="level-0" value="1">Uncategorized</option>
<option class="level-0" value="21">Unfortunate</option>
<option class="level-0" value="2">Webbish</option>
<option class="level-0" value="78">Werk Appropriate</option>
<option class="level-0" value="10">Worldly</option>
</select>
</form>
<script type='text/javascript'>
/* <![CDATA[ */
(function() {
	var dropdown = document.getElementById( "cat" );
	function onCatChange() {
		if ( dropdown.options[ dropdown.selectedIndex ].value > 0 ) {
			dropdown.parentNode.submit();
		}
	}
	dropdown.onchange = onCatChange;
})();
/* ]]> */
</script>
</div><div id="archives-4" class="sb-widget clearfix widget_archive"><h4 class="widget-title"><span>Archives</span></h4> <label class="screen-reader-text" for="archives-dropdown-4">Archives</label>
<select id="archives-dropdown-4" name="archive-dropdown" onchange='document.location.href=this.options[this.selectedIndex].value;'>
<option value="">Select Month</option>
<option value='https://omg.blog/2018/09/'> September 2018 </option>
<option value='https://omg.blog/2018/08/'> August 2018 </option>
<option value='https://omg.blog/2018/07/'> July 2018 </option>
<option value='https://omg.blog/2018/06/'> June 2018 </option>
<option value='https://omg.blog/2018/05/'> May 2018 </option>
<option value='https://omg.blog/2018/04/'> April 2018 </option>
<option value='https://omg.blog/2018/03/'> March 2018 </option>
<option value='https://omg.blog/2018/02/'> February 2018 </option>
<option value='https://omg.blog/2018/01/'> January 2018 </option>
<option value='https://omg.blog/2017/12/'> December 2017 </option>
<option value='https://omg.blog/2017/11/'> November 2017 </option>
<option value='https://omg.blog/2017/10/'> October 2017 </option>
<option value='https://omg.blog/2017/09/'> September 2017 </option>
<option value='https://omg.blog/2017/08/'> August 2017 </option>
<option value='https://omg.blog/2017/07/'> July 2017 </option>
<option value='https://omg.blog/2017/06/'> June 2017 </option>
<option value='https://omg.blog/2017/05/'> May 2017 </option>
<option value='https://omg.blog/2017/04/'> April 2017 </option>
<option value='https://omg.blog/2017/03/'> March 2017 </option>
<option value='https://omg.blog/2017/02/'> February 2017 </option>
<option value='https://omg.blog/2017/01/'> January 2017 </option>
<option value='https://omg.blog/2016/12/'> December 2016 </option>
<option value='https://omg.blog/2016/11/'> November 2016 </option>
<option value='https://omg.blog/2016/10/'> October 2016 </option>
<option value='https://omg.blog/2016/09/'> September 2016 </option>
<option value='https://omg.blog/2016/08/'> August 2016 </option>
<option value='https://omg.blog/2016/07/'> July 2016 </option>
<option value='https://omg.blog/2016/06/'> June 2016 </option>
<option value='https://omg.blog/2016/05/'> May 2016 </option>
<option value='https://omg.blog/2016/04/'> April 2016 </option>
<option value='https://omg.blog/2016/03/'> March 2016 </option>
<option value='https://omg.blog/2016/02/'> February 2016 </option>
<option value='https://omg.blog/2016/01/'> January 2016 </option>
<option value='https://omg.blog/2015/12/'> December 2015 </option>
<option value='https://omg.blog/2015/11/'> November 2015 </option>
<option value='https://omg.blog/2015/10/'> October 2015 </option>
<option value='https://omg.blog/2015/09/'> September 2015 </option>
<option value='https://omg.blog/2015/08/'> August 2015 </option>
<option value='https://omg.blog/2015/07/'> July 2015 </option>
<option value='https://omg.blog/2015/06/'> June 2015 </option>
<option value='https://omg.blog/2015/05/'> May 2015 </option>
<option value='https://omg.blog/2015/04/'> April 2015 </option>
<option value='https://omg.blog/2015/03/'> March 2015 </option>
<option value='https://omg.blog/2015/02/'> February 2015 </option>
<option value='https://omg.blog/2015/01/'> January 2015 </option>
<option value='https://omg.blog/2014/12/'> December 2014 </option>
<option value='https://omg.blog/2014/11/'> November 2014 </option>
<option value='https://omg.blog/2014/10/'> October 2014 </option>
<option value='https://omg.blog/2014/09/'> September 2014 </option>
<option value='https://omg.blog/2014/08/'> August 2014 </option>
<option value='https://omg.blog/2014/07/'> July 2014 </option>
<option value='https://omg.blog/2014/06/'> June 2014 </option>
<option value='https://omg.blog/2014/05/'> May 2014 </option>
<option value='https://omg.blog/2014/04/'> April 2014 </option>
<option value='https://omg.blog/2014/03/'> March 2014 </option>
<option value='https://omg.blog/2014/02/'> February 2014 </option>
<option value='https://omg.blog/2014/01/'> January 2014 </option>
<option value='https://omg.blog/2013/12/'> December 2013 </option>
<option value='https://omg.blog/2013/11/'> November 2013 </option>
<option value='https://omg.blog/2013/10/'> October 2013 </option>
<option value='https://omg.blog/2013/09/'> September 2013 </option>
<option value='https://omg.blog/2013/08/'> August 2013 </option>
<option value='https://omg.blog/2013/07/'> July 2013 </option>
<option value='https://omg.blog/2013/06/'> June 2013 </option>
<option value='https://omg.blog/2013/05/'> May 2013 </option>
<option value='https://omg.blog/2013/04/'> April 2013 </option>
<option value='https://omg.blog/2013/03/'> March 2013 </option>
<option value='https://omg.blog/2013/02/'> February 2013 </option>
<option value='https://omg.blog/2013/01/'> January 2013 </option>
<option value='https://omg.blog/2012/12/'> December 2012 </option>
<option value='https://omg.blog/2012/11/'> November 2012 </option>
<option value='https://omg.blog/2012/10/'> October 2012 </option>
<option value='https://omg.blog/2012/09/'> September 2012 </option>
<option value='https://omg.blog/2012/08/'> August 2012 </option>
<option value='https://omg.blog/2012/07/'> July 2012 </option>
<option value='https://omg.blog/2012/06/'> June 2012 </option>
<option value='https://omg.blog/2012/05/'> May 2012 </option>
<option value='https://omg.blog/2012/04/'> April 2012 </option>
<option value='https://omg.blog/2012/03/'> March 2012 </option>
<option value='https://omg.blog/2012/02/'> February 2012 </option>
<option value='https://omg.blog/2012/01/'> January 2012 </option>
<option value='https://omg.blog/2011/12/'> December 2011 </option>
<option value='https://omg.blog/2011/11/'> November 2011 </option>
<option value='https://omg.blog/2011/10/'> October 2011 </option>
<option value='https://omg.blog/2011/09/'> September 2011 </option>
<option value='https://omg.blog/2011/08/'> August 2011 </option>
<option value='https://omg.blog/2011/07/'> July 2011 </option>
<option value='https://omg.blog/2011/06/'> June 2011 </option>
<option value='https://omg.blog/2011/05/'> May 2011 </option>
<option value='https://omg.blog/2011/04/'> April 2011 </option>
<option value='https://omg.blog/2011/03/'> March 2011 </option>
<option value='https://omg.blog/2011/02/'> February 2011 </option>
<option value='https://omg.blog/2011/01/'> January 2011 </option>
<option value='https://omg.blog/2010/12/'> December 2010 </option>
<option value='https://omg.blog/2010/11/'> November 2010 </option>
<option value='https://omg.blog/2010/10/'> October 2010 </option>
<option value='https://omg.blog/2010/09/'> September 2010 </option>
<option value='https://omg.blog/2010/08/'> August 2010 </option>
<option value='https://omg.blog/2010/07/'> July 2010 </option>
<option value='https://omg.blog/2010/06/'> June 2010 </option>
<option value='https://omg.blog/2010/05/'> May 2010 </option>
<option value='https://omg.blog/2010/04/'> April 2010 </option>
<option value='https://omg.blog/2010/03/'> March 2010 </option>
<option value='https://omg.blog/2010/02/'> February 2010 </option>
<option value='https://omg.blog/2010/01/'> January 2010 </option>
<option value='https://omg.blog/2009/12/'> December 2009 </option>
<option value='https://omg.blog/2009/11/'> November 2009 </option>
<option value='https://omg.blog/2009/10/'> October 2009 </option>
<option value='https://omg.blog/2009/09/'> September 2009 </option>
<option value='https://omg.blog/2009/08/'> August 2009 </option>
<option value='https://omg.blog/2009/07/'> July 2009 </option>
<option value='https://omg.blog/2009/06/'> June 2009 </option>
<option value='https://omg.blog/2009/05/'> May 2009 </option>
<option value='https://omg.blog/2009/04/'> April 2009 </option>
<option value='https://omg.blog/2009/03/'> March 2009 </option>
<option value='https://omg.blog/2009/02/'> February 2009 </option>
<option value='https://omg.blog/2009/01/'> January 2009 </option>
<option value='https://omg.blog/2008/12/'> December 2008 </option>
<option value='https://omg.blog/2008/11/'> November 2008 </option>
<option value='https://omg.blog/2008/10/'> October 2008 </option>
<option value='https://omg.blog/2008/09/'> September 2008 </option>
<option value='https://omg.blog/2008/08/'> August 2008 </option>
<option value='https://omg.blog/2008/07/'> July 2008 </option>
<option value='https://omg.blog/2008/06/'> June 2008 </option>
<option value='https://omg.blog/2008/05/'> May 2008 </option>
<option value='https://omg.blog/2008/04/'> April 2008 </option>
<option value='https://omg.blog/2008/03/'> March 2008 </option>
<option value='https://omg.blog/2008/02/'> February 2008 </option>
<option value='https://omg.blog/2008/01/'> January 2008 </option>
<option value='https://omg.blog/2007/12/'> December 2007 </option>
<option value='https://omg.blog/2007/11/'> November 2007 </option>
<option value='https://omg.blog/2007/10/'> October 2007 </option>
<option value='https://omg.blog/2007/09/'> September 2007 </option>
<option value='https://omg.blog/2007/08/'> August 2007 </option>
<option value='https://omg.blog/2007/07/'> July 2007 </option>
<option value='https://omg.blog/2007/06/'> June 2007 </option>
<option value='https://omg.blog/2007/05/'> May 2007 </option>
<option value='https://omg.blog/2007/04/'> April 2007 </option>
<option value='https://omg.blog/2007/03/'> March 2007 </option>
<option value='https://omg.blog/2007/02/'> February 2007 </option>
<option value='https://omg.blog/2007/01/'> January 2007 </option>
<option value='https://omg.blog/2006/12/'> December 2006 </option>
<option value='https://omg.blog/2006/11/'> November 2006 </option>
<option value='https://omg.blog/2006/10/'> October 2006 </option>
<option value='https://omg.blog/2006/09/'> September 2006 </option>
<option value='https://omg.blog/2006/08/'> August 2006 </option>
<option value='https://omg.blog/2006/07/'> July 2006 </option>
<option value='https://omg.blog/2006/06/'> June 2006 </option>
<option value='https://omg.blog/2006/05/'> May 2006 </option>
<option value='https://omg.blog/2006/04/'> April 2006 </option>
<option value='https://omg.blog/2006/03/'> March 2006 </option>
<option value='https://omg.blog/2006/02/'> February 2006 </option>
<option value='https://omg.blog/2006/01/'> January 2006 </option>
<option value='https://omg.blog/2005/12/'> December 2005 </option>
<option value='https://omg.blog/2005/11/'> November 2005 </option>
<option value='https://omg.blog/2005/10/'> October 2005 </option>
<option value='https://omg.blog/2005/09/'> September 2005 </option>
<option value='https://omg.blog/2005/08/'> August 2005 </option>
<option value='https://omg.blog/2005/07/'> July 2005 </option>
<option value='https://omg.blog/2005/06/'> June 2005 </option>
<option value='https://omg.blog/2005/05/'> May 2005 </option>
<option value='https://omg.blog/2005/04/'> April 2005 </option>
<option value='https://omg.blog/2005/03/'> March 2005 </option>
<option value='https://omg.blog/2005/02/'> February 2005 </option>
<option value='https://omg.blog/2005/01/'> January 2005 </option>
<option value='https://omg.blog/2004/12/'> December 2004 </option>
<option value='https://omg.blog/2004/11/'> November 2004 </option>
<option value='https://omg.blog/2004/10/'> October 2004 </option>
<option value='https://omg.blog/2004/09/'> September 2004 </option>
<option value='https://omg.blog/2004/08/'> August 2004 </option>
<option value='https://omg.blog/2004/07/'> July 2004 </option>
<option value='https://omg.blog/2004/06/'> June 2004 </option>
<option value='https://omg.blog/2004/05/'> May 2004 </option>
<option value='https://omg.blog/2004/04/'> April 2004 </option>
<option value='https://omg.blog/2004/03/'> March 2004 </option>
<option value='https://omg.blog/2004/02/'> February 2004 </option>
<option value='https://omg.blog/2004/01/'> January 2004 </option>
<option value='https://omg.blog/2003/12/'> December 2003 </option>
<option value='https://omg.blog/2003/11/'> November 2003 </option>
<option value='https://omg.blog/2003/10/'> October 2003 </option>
<option value='https://omg.blog/2003/09/'> September 2003 </option>
<option value='https://omg.blog/2003/08/'> August 2003 </option>
<option value='https://omg.blog/2003/07/'> July 2003 </option>
</select>
</div><div id="enhancedtextwidget-5" class="sb-widget clearfix widget_text enhanced-text-widget"><div class="textwidget widget-text"> 

<div class="module-creative-commons module">
<div class="module-content">
<a href="//creativecommons.org/licenses/by-nc-nd/3.0/us/" target="_blank"><img alt="Creative Commons License" border=0 src="https://omg.blog/wp-content/themes/assets/creative-commons-button.png" /></a><br />
This weblog is licensed under a <a href="//creativecommons.org/licenses/by-nc-nd/3.0/us/" target="_blank">Creative Commons License</a>.
</div>
</div>

<br>

<div class="sidebar-policy">
<font color="#F7277F">&raquo; </font><a href="https://omg.blog/privacy-policy.php">Privacy Policy</a><font color="#F7277F"> &laquo;</font>
<br />
<font color="#F7277F">&raquo; </font><a href="https://omg.blog/omg_its_our_submission_policy/">Submission Policy</a><font color="#F7277F"> &laquo;</font>
<br />
<font color="#F7277F">&raquo; </font><a href="https://omg.blog/blogroll.php">Blogroll</a><font color="#F7277F"> &laquo;</font>
<br /><br />
</div>
<b>Disclaimers:</b> There is material on this blog that is inappropriate for those under the age of 18. If that is you, please leave the site.<br /><br />What you read here may or may not be true. We do not claim credit for any images, except where noted. Please <a href="/cdn-cgi/l/email-protection#a6d6d3c4cacfd5cec3d486dde7f2db86c9cbc186dde2e9f2db86c4cac9c1">let us know</a> if we use your image and you would like it removed.</div></div></aside></div>
</div>
<footer class="mh-footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
<div id="enhancedtextwidget-9" class="footer-ad-wrap widget_text enhanced-text-widget"><div class="textwidget widget-text">

<div id='leader_bottom'>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>
		googletag.cmd.push(function() { googletag.display('leader_bottom'); });
	</script>
</div>
</div></div> <div class="wrapper-inner clearfix">
</div>
<div class="footer-bottom">
<div class="wrapper-inner clearfix">
<div class="copyright-wrap">
<p class="copyright">
&copy; 2003-2018 !! omg blog !! | Developed by <a target="_blank" title="Charleston, SC - Website Development and Design" href="http://sitesbycoop.com">Sites By Coop</a> | <a href="/cdn-cgi/l/email-protection#24454057644b49430a46484b43">ADVERTISE WITH US</a> | <a href="/cdn-cgi/l/email-protection#84f0edf4f7c4ebe9e3aae6e8ebe3">SEND US A TIP</a>
</p>
</div>
</div>
</div>
</footer>
<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type='text/javascript'>
var renderInvisibleReCaptcha = function() {

    for (var i = 0; i < document.forms.length; ++i) {
        var form = document.forms[i];
        var holder = form.querySelector('.inv-recaptcha-holder');

        if (null === holder) continue;
		holder.innerHTML = '';

         (function(frm){
			var cf7SubmitElm = frm.querySelector('.wpcf7-submit');
            var holderId = grecaptcha.render(holder,{
                'sitekey': '6Le-Yz8UAAAAAM3xoXYoqwxPoZ6stMNLDwVVQPJ_', 'size': 'invisible', 'badge' : 'inline',
                'callback' : function (recaptchaToken) {
					if((null !== cf7SubmitElm) && (typeof jQuery != 'undefined')){jQuery(frm).submit();grecaptcha.reset(holderId);return;}
					 HTMLFormElement.prototype.submit.call(frm);
                },
                'expired-callback' : function(){grecaptcha.reset(holderId);}
            });

			if(null !== cf7SubmitElm && (typeof jQuery != 'undefined') ){
				jQuery(cf7SubmitElm).off('click').on('click', function(clickEvt){
					clickEvt.preventDefault();
					grecaptcha.execute(holderId);
				});
			}
			else
			{
				frm.onsubmit = function (evt){evt.preventDefault();grecaptcha.execute(holderId);};
			}


        })(form);
    }
};
</script>
<script type='text/javascript' async defer src='https://www.google.com/recaptcha/api.js?onload=renderInvisibleReCaptcha&#038;render=explicit'></script>
<script type='text/javascript' src='https://omg.blog/wp-content/themes/omgblog/js/scripts.js?ver=1'></script>
<script type='text/javascript' src='https://omg.blog/wp-includes/js/wp-embed.min.js?ver=4.9.8'></script>

<script type="text/javascript">
var _qevents = _qevents || [];

(function() {
var elem = document.createElement('script'); elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") +
".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt); })();
</script>
<script type="text/javascript">
_qevents.push([{
qacct:"p-93b6WeVYdHXoc", // Value of the Network operator's account ID, this stays static across sites
labels:"asktell" // Label attributed to network operators Quantcast Publisher profile
}, { qacct:"p-89e5PRr9xUpng", // Individual blog owner's account ID
labels:"omgblog" // Label attribtued to individual owner's Quantcast Publisher profile
}]);
</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel?a.1=p-93b6WeVYdHXoc&a.2=p-89e5PRr9xUpng" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>



<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "7584020" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="https://sb.scorecardresearch.com/p?c1=2&c2=7584020&cv=2.0&cj=1" />
</noscript>


<script>(function(w, d, s, id) {
	  w.PUBX=w.PUBX || {pub: "omgblog", discover: true, lazy: false};
	  var js, pjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id; js.async = true;
	  js.src = "https://main.pubexchange.com/loader.min.js";
	  pjs.parentNode.insertBefore(js, pjs);
	}(window, document, "script", "pubexchange-jssdk"));</script>


<script type="text/javascript" src='https://amara.org/embedder-iframe'>
</script>

<div id="amzn-assoc-ad-674bc608-3eac-43f1-9a3b-56f1d45c701f"></div><script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=674bc608-3eac-43f1-9a3b-56f1d45c701f"></script>


<script type="text/javascript" language="javascript">
	var sc_project=1354515; 
	var sc_invisible=1; 
	var sc_partition=12; 
	var sc_security="be1f701f"; 
	</script>
<script type="text/javascript" language="javascript" src="//www.statcounter.com/counter/counter.js"></script><noscript><a href="//www.statcounter.com/" target="_blank"><img  src="//c13.statcounter.com/counter.php?sc_project=1354515&amp;java=0&amp;security=be1f701f&amp;invisible=1" alt="hit counter" border="0"></a> </noscript>


<script>
var Tynt=Tynt||[];Tynt.push('cRoSBYPX4r37yZadbi-bpO');
(function(){var h,s=document.createElement('script');
s.src=(window.location.protocol==='https:'?
'https':'http')+'://cdn.tynt.com/ti.js';
h=document.getElementsByTagName('script')[0];
h.parentNode.insertBefore(s,h);})();
</script>


<script type="text/javascript">
    var _sf_async_config = _sf_async_config || {};
    /** CONFIGURATION START **/
    _sf_async_config.uid = 769;
    _sf_async_config.domain = 'omg.blog';
    _sf_async_config.useCanonical = true;
    /** CONFIGURATION END **/
    (function() {
        function loadChartbeat() {
            var e = document.createElement('script');
            e.setAttribute('language', 'javascript');
            e.setAttribute('type', 'text/javascript');
            e.setAttribute('src', '//static.chartbeat.com/js/chartbeat.js');
            document.body.appendChild(e);
        }
        var oldonload = window.onload;
        window.onload = (typeof window.onload != 'function') ?
            loadChartbeat : function() {
                oldonload();
                loadChartbeat();
            };
    })();
</script>


<script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/803b16a204f4a43026a99e0dc/ff34e84b4b2f5056a4ebd0a96.js");</script>




</body>
</html>