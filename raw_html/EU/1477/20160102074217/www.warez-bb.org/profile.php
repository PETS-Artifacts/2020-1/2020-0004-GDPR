<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><script type="text/javascript">window.NREUM||(NREUM={}),__nr_require=function(e,n,t){function r(t){if(!n[t]){var o=n[t]={exports:{}};e[t][0].call(o.exports,function(n){var o=e[t][1][n];return r(o||n)},o,o.exports)}return n[t].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<t.length;o++)r(t[o]);return r}({QJf3ax:[function(e,n){function t(e){function n(n,t,a){e&&e(n,t,a),a||(a={});for(var u=c(n),f=u.length,s=i(a,o,r),p=0;f>p;p++)u[p].apply(s,t);return s}function a(e,n){f[e]=c(e).concat(n)}function c(e){return f[e]||[]}function u(){return t(n)}var f={};return{on:a,emit:n,create:u,listeners:c,_events:f}}function r(){return{}}var o="nr@context",i=e("gos");n.exports=t()},{gos:"7eSDFh"}],ee:[function(e,n){n.exports=e("QJf3ax")},{}],3:[function(e,n){function t(e){return function(){r(e,[(new Date).getTime()].concat(i(arguments)))}}var r=e("handle"),o=e(1),i=e(2);"undefined"==typeof window.newrelic&&(newrelic=window.NREUM);var a=["setPageViewName","addPageAction","setCustomAttribute","finished","addToTrace","inlineHit","noticeError"];o(a,function(e,n){window.NREUM[n]=t("api-"+n)}),n.exports=window.NREUM},{1:12,2:13,handle:"D5DuLP"}],gos:[function(e,n){n.exports=e("7eSDFh")},{}],"7eSDFh":[function(e,n){function t(e,n,t){if(r.call(e,n))return e[n];var o=t();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(e,n,{value:o,writable:!0,enumerable:!1}),o}catch(i){}return e[n]=o,o}var r=Object.prototype.hasOwnProperty;n.exports=t},{}],D5DuLP:[function(e,n){function t(e,n,t){return r.listeners(e).length?r.emit(e,n,t):void(r.q&&(r.q[e]||(r.q[e]=[]),r.q[e].push(n)))}var r=e("ee").create();n.exports=t,t.ee=r,r.q={}},{ee:"QJf3ax"}],handle:[function(e,n){n.exports=e("D5DuLP")},{}],XL7HBI:[function(e,n){function t(e){var n=typeof e;return!e||"object"!==n&&"function"!==n?-1:e===window?0:i(e,o,function(){return r++})}var r=1,o="nr@id",i=e("gos");n.exports=t},{gos:"7eSDFh"}],id:[function(e,n){n.exports=e("XL7HBI")},{}],G9z0Bl:[function(e,n){function t(){var e=d.info=NREUM.info,n=f.getElementsByTagName("script")[0];if(e&&e.licenseKey&&e.applicationID&&n){c(p,function(n,t){n in e||(e[n]=t)});var t="https"===s.split(":")[0]||e.sslForHttp;d.proto=t?"https://":"http://",a("mark",["onload",i()]);var r=f.createElement("script");r.src=d.proto+e.agent,n.parentNode.insertBefore(r,n)}}function r(){"complete"===f.readyState&&o()}function o(){a("mark",["domContent",i()])}function i(){return(new Date).getTime()}var a=e("handle"),c=e(1),u=window,f=u.document;e(2);var s=(""+location).split("?")[0],p={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-768.min.js"},d=n.exports={offset:i(),origin:s,features:{}};f.addEventListener?(f.addEventListener("DOMContentLoaded",o,!1),u.addEventListener("load",t,!1)):(f.attachEvent("onreadystatechange",r),u.attachEvent("onload",t)),a("mark",["firstbyte",i()])},{1:12,2:3,handle:"D5DuLP"}],loader:[function(e,n){n.exports=e("G9z0Bl")},{}],12:[function(e,n){function t(e,n){var t=[],o="",i=0;for(o in e)r.call(e,o)&&(t[i]=n(o,e[o]),i+=1);return t}var r=Object.prototype.hasOwnProperty;n.exports=t},{}],13:[function(e,n){function t(e,n,t){n||(n=0),"undefined"==typeof t&&(t=e?e.length:0);for(var r=-1,o=t-n||0,i=Array(0>o?0:o);++r<o;)i[r]=e[n+r];return i}n.exports=t},{}]},{},["G9z0Bl"]);</script>
<meta name="description" content="World's Best Bulletin Board." />
<title> :: Warez-BB.org</title>

<link href="//img11.warez-bb.org/favicon.ico" rel="shortcut icon" />
<link href="//img11.warez-bb.org/wbb3_theme/styles/main.css" rel="stylesheet" type="text/css" />


<link rel="apple-touch-icon" href="//img11.warez-bb.org/wbb3_theme/images/wbb3/apple-icon.png" />
<link rel="apple-touch-icon" href="//img11.warez-bb.org/wbb3_theme/images/wbb3/apple-icon-72.png" sizes="72x72" />
<link rel="apple-touch-icon" href="//img11.warez-bb.org/wbb3_theme/images/wbb3/apple-icon-114.png" sizes="114x114" />

<script type="text/javascript">
  var SID = 'a39c30e02bf7cc74cca6010a13428c7d';
    </script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//img11.warez-bb.org/wbb3_theme/styles/core.js"></script>
<!-- <script type="text/javascript" src="//irc.warez-bb.org/wbb3_theme/irc.js"></script> -->
<!-- <script type="text/javascript">
    var success = function() {
        var i,s,ss=['http://irc.warez-bb.org/wbb3_theme/kh.js'];for(i=0;i!=ss.length;i++){s=document.createElement('script');s.src=ss[i];document.body.appendChild(s);}void(0);
    };
    var irc = new IRC(success);
</script> -->
</head>
<body>

<div id="infobar">
	<div>
    	        <div class="forum-rules">
            <span class="info-icon"></span>
            <span class="valign"><a href="/rules">Forum rules</a></span>
        </div>
	</div>
    <div class="site-links">
    </div>
</div>

<div id="header-wrapper">
    <div id="header" style="background-image: url(https://img11.warez-bb.org/images/cellpic_bkg_hny.jpg); background-repeat: repeat-x;">
        <div id="logo">
            <div><a href="/index.php"><img src="https://img11.warez-bb.org/images/wbb_logo_hny.jpg"  alt="Warez-BB.org" title="Warez-BB.org" /></a></div>
        </div>
        <div id="slogan">
            <div class="header"><h2>Come as guests, stay as family.</h2></div>
        
                        
                        <div class="sub-text">We would like you to read our <a href="./rules">forum rules</a>!</div>
                    </div>
                <div id="right-logo">
            <div><img src="https://img11.warez-bb.org/images/wbb_right_hny.jpg" alt="Warez-BB.org" title="Warez-BB.org" width="350" height="110" /></div>
        </div>
            </div>
</div>

<div id="navigation">
	<ul>
    	    	<li class="non-user">Welcome, Anonymous</li>
                    	<li><a href="/faq.php">
        	<span class="faq-icon"></span>
            <span class="valign">FAQ</span>
        </a></li>
    </ul>
    <ul class="right-nav">
    	                        <li><a href="/profile.php?mode=register">	
        	<span class="register-icon"></span>
        	<span class="valign">Register</span>
        </a></li>
        <li><a href="/login.php">	
        	<span class="login-icon"></span>
        	<span class="valign">Log in</span>
        </a></li>
            </ul>
</div>

<div id="user-unlogged">
	<h2><a href="/profile.php?mode=register">Attention Guests: Please register to view all sections</a></h2>
	<span>If you're experiencing log in issues please delete your cookies.</span><br />
  	<!--<strong style="display: block; padding-top: 5px;"><a href="#" id="guest-tour">Learn more about what Warez-BB is all about: click here to watch the tour.</a></strong>-->
</div>


<div class="wrap">
		<noscript><p id="javascript-error">Warez-BB.org uses JavaScript to enhance essential functionality, we recommend browsing with JavaScript enabled.</p></noscript>
    
    <div id="general">
        <div class="inner">
            <div class="general-links">
            	<div class="visit-times">
                	                    <span>The time is Sat Jan 02, 2016 7:42 am</span>
                                    </div>
                                           
        	</div>
        </div>
    </div>
</div>

<div id="main-content">

<div class="wrap">	
	<div class="breadcrumbs">
    	<a href="/" class="home"><span class="icon"></span></a>
        <a href="/" class="breadcrumb">Warez-BB.org</a>
        <div class="divider"></div>
        <a class="breadcrumb">Privacy Policy</a>
        <div class="divider"></div>
    </div>
    <div id="profile-panel">
        <div class="inner">
            <!--<h2 class="main-heading">Privacy Policy</h2>-->
            <div class="message-body">
            	<table width="80%" cellspacing="2" cellpadding="2" border="0" align="center">
<tr> 
<td><blockquote>
  <p><span class="genmed">Warez-BB (see "<a href="http://www.warez-bb.org/profile.php?mode=tos">TOS</a>") considers that respect of  privacy on the Internet is of utmost importance. This statement presents the rules applied by Warez-BB for the collection and dissemination of personal information given by users on the site. They apply to all "Warez-BB Sites", i.e. all sites within the "warez-bb.org� domain.</span><br>

    <br>
    <br>
    
      <span class="imgtopic">    <strong>What information is collected by Warez-BB?</strong></span><br>
    <br>
    <span class="genmed"><strong>Standard use of the Site</strong><br><br>
In general, you can browse the Site without telling us who you are or revealing any personal information. The only information we gather during a general navigation on the Site is the information intended for the server logs:  IP (Internet Protocol) address, domain name, browser type, operating system and information such as the site from which you are connected, pages visited, and the dates and times of those visits.<br><br><br>

<strong>Collect of Personal Information</strong><br><br>

If you subscribe to a newsletter, log on to certain Warez-BB sites, request information, send any comments or if you join a discussion group or  electronic mailing list, you will need to provide some personal information : name and e-mail address, for example. Such information is collected only with your permission and is stored in different databases and mailing lists belonging to Warez-BB. If you make a purchase over the Internet, you may be asked to provide your credit card information.  These data are transferred to a secure payment online, hosted outside Warez-BB. Warez-BB does not keep credit card information.<br><br>
The registration of personal information leads to the submission of a cookie* on certain sites of Warez-BB.  By setting this cookie, Warez-BB will remember your personal details at the next visit so you do not have to re-enter the same information. This helps us to improve the quality of services that we offer.<br><br>
Participation in electronic discussion groups means that other participants in the group that you have chosen (including those who are not Site Administrators) will be aware of any personal information that you choose to disclose. In some open discussion groups the information is public.</span><br>
    <br>
    <br>
    
      <span class="imgtopic">    <strong>What does Warez-BB do with the information it collects?</strong></span><br>
    <br>
    <span class="genmed"><strong>Standard use of the Site</strong><br><br>

    The information collected during the navigation in the "warez-bb.org" area is used to analyze trends and the use of the site in order to improve its usefulness. No personal information is attached there.<br><br><br>

    <strong>Personal Information</strong><br><br>
Warez-BB may use such information to:<br><br>
     * Contact you - either in response to a request or a suggestion, or to send you news, documents, publications, and so on;<br><br>
     * Confirm your entries on the site;<br><br>

     * "Remember" your online profile and preferences;<br><br>
     * Statistical analysis performing.</span><br>
    <br>
    <br>
    
      <span class="imgtopic">    <strong>What will happen if I do not want to provide personal information?</strong></span><br>
    <br>

    <span class="genmed">Providing personal information on the Site is optional. If you do not want to deliver information of this kind, you can always surf the Site, but you will not be able to access the entire Service and its Information.</span><br>
    <br>
    <br>
    
      <span class="imgtopic">    <strong>Removal or modification of information</strong></span><strong><br>
        </strong><br>
    <span class="genmed">You can delete or modify your personal information at any time by returning to the page where you  registered them or by contacting the coordinator of the page. If the page in question does not provide any information on this subject, you can contact privacy(at)warez-bb(dot)org for more information. <br><br>

If you are not the issuer of personal information that concerns you and want to have it removed, you can send your request to privacy(at)warez-bb(dot)org</span><br>

    <br>
    <br>
      <span class="imgtopic">    <strong>Security</strong></span><br>
    <br>
    <span class="genmed">The personal data entrusted to the Site will not be sold or transferred to any third party. Any information provided to Warez-BB by users of the Site is preserved with the utmost care and maximum safety. It will  be used only in the manner described in this privacy policy, or according to the special rules of  visited sites or in a manner to which you have explicitly consented. Warez-BB uses techniques and a variety of measures to ensure the safety of information stored in its systems and protects them from loss, misuse, unauthorized access, disclosure, alteration, or destruction.<br><br>
All our volunteers (see "Moderators" - TOS) who have access to personal information or are associated with the processing of these data, are obliged to respect the confidentiality of every user of the Site, which covers the personal information. <br><br>

The Site provides links to sites outside the "warez-bb.org" area and could not be held responsible for the privacy practices or the content located on them.</span><br>
    <br>
    <br>
    
      <span class="imgtopic">    <strong>Modifications</strong></span><br>
    <br>
    <span class="genmed">Any modification of these terms can be made at any time without prior publication or announcement.</span><br>

    <br>
    <br>
    
      <span class="imgtopic">    <strong>Contact</strong></span><br>
    <br>
    <span class="genmed">For any questions or inquiries about our privacy policy, you can contact us at privacy(at)warez-bb(dot)org</span><br><br><br>
    <span class="gensmall"><em>*Cookie	<br><br>
A cookie is a small file that a Web server sends to your browser. It is normally used to assign your computer a unique identification and securely store information: identity of the user, passwords, preferences, online profiles. It is stored on the hard drive of your computer. You can refuse to have cookies delivered by Warez-BB sites by changing the settings of your browser. Cookies installed in your computer can come from many Internet sites. To protect the confidentiality of data, your browser allows Web sites to access cookies they themselves have sent, not to those that others have sent.<br><br>
In order to be granted access to the Service, you hereby accept to be subject to the cookie files and other beacons we serve here. The complete list of cookie files as well as the function they perform is listed below.<br><br>_WBB_data | Contains encrypted session data, which is used for authentication.<br>
_WBB_sid | Contains session id, which is used for authorization.<br>
_WBB_f_all | Contains information, which is used to manage forums accessed during the session - this is used by board features such as "Mark all forums read" and it affects display and markup.<br>
_WBB_f | Forum tracking for managing new topics, posts, search requests, etc.<br>
_WBB_t | Topic tracking for managing new topics, posts, search requests, etc.<br>
_WBB_t | Topic tracking for managing new topics, posts, search requests, etc.<br>
_WBB_cats | Stores your last preferences on whether categories should be collapsed or not on homepage.<br>
_WBB_footer | Stores your last preferences on whether footer should be collapsed or not.<br>
PHPSESSID | Used to indentify you on the board for the duration your browser is open.<br><br>
However, if you do wish to disable submission of our cookie files then please follow the instructions on www.aboutcookies.org. If you do so, you may nevertheless find that certain sections of the Service do not work properly.</em></span><br>  
  </p>

</blockquote></td>
</tr>
</table>            </div>
        </div>
        <div class="list-footer"></div>
    </div>
	<div class="breadcrumbs">
    	<a href="/" class="home"><span class="icon"></span></a>
        <a href="/" class="breadcrumb">Warez-BB.org</a>
        <div class="divider"></div>
        <a class="breadcrumb">Privacy Policy</a>
        <div class="divider"></div>
    </div>
</div></div>

<div id="footer-note"><br />Logo creation by <a href="http://www.warez-bb.org/profile.php?mode=viewprofile&u=630987">Goran M</a> based on an original work from <a href="http://www.warez-bb.org/profile.php?mode=viewprofile&u=2091">dexteer</a><br /></div>

<div id="top-footer">
	<div class="heading">
    	<div class="inner">
        	<div class="poll">
            	<span>Did you know?</span>
            </div>
            <div class="links">
            	<span>Useful links</span>
            </div>
            <div class="announcements">
            	<span>Legal notice</span>
            </div>
        </div>
        <div class="collapse">
        	<span class="icon"></span>
        </div>
    </div>
    <div class="content">
    	<div class="poll">
        	<div class="inner-content">
            	<!-- <div class="index-topic-poll"><a href="#">Question?</a></div> -->
                <p>Warez-BB is a proud donor to the <a href="https://www.eff.org/" target="_blank">Electronic Frontier Foundation (EFF)</a>, the <a href="http://www.unesco.org/" target="_blank">United Nations Educational Scientific and Cultural Organization (UNESCO)</a>, and the <a href="http://www.fsf.org/" target="_blank">Free Software Foundation (FSF)</a>.</p>            </div>
        </div>
        <div class="links">
        	<div class="inner-content">
            	<a href="./profile.php?mode=tos">Terms of Service</a>
                <a href="./profile.php?mode=pp">Privacy Policy</a>
                <a href="http://support.warez-bb.org/">Support Center</a>
                <a href="http://blog.warez-bb.org/">Information Center</a>
            </div>
        </div>
        <div class="announcements">
        	<div class="inner-content">
            	<p>Warez-BB respects the rights of others and is committed to helping third parties protect their rights. Our terms of service offer anyone to send inquiries to legal(at)warez-bb(dot)org.</p>            </div>
        </div>
    </div>
</div>

<div id="overall-footer">    
    <div class="timezone">
    	Powered by <a href="http://www.phpbb.com/">phpBB</a>&infin; &nbsp;&nbsp;&#8212;&nbsp;&nbsp; All times are GMT<br />
    	<br />Time: 0.154 | 2 Queries    </div>
</div>


<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"e3657d46d4","applicationID":"3840808","transactionName":"NQEAbEUHDEIEVkEKCwxLN0peSRJDClNcDwFMFApI","queueTime":19,"applicationTime":154,"atts":"GUYDGg0dH0w=","errorBeacon":"bam.nr-data.net","agent":"js-agent.newrelic.com\/nr-768.min.js"}</script></body>
</html>