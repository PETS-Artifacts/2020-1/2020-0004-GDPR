<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en"> 
<head>

	
	<meta http-equiv="content-type" content="text/html" />
	<meta http-equiv="content-language" content="en" />

	
	<title>Privacy Policy Agreement - AuPair.com</title>
	<meta name="title" content="Privacy Policy Agreement - AuPair.com" />
	<meta name="description" content="Privacy Policy" />
	<meta name="keywords" content="Privacy Policy" />
	<meta name="Distribution" content="Global" />

	<link rel="alternate" hreflang="pt" href= "https://www.aupair.com/pt/p-política-de-privacidade.php" /><meta http-equiv="content-type" content="text/html; charset=UTF-8" />

	

			<link href="/css/style6New.css" rel="stylesheet" type="text/css" />
		
			<link href="/pics/content.css" rel="stylesheet" type="text/css" />
			<link href="/pics/contentNew.css" rel="stylesheet" type="text/css" />
		
	<meta name="viewport" content="width=device-width" />		
</head>


<body>


<div class="header">

		<div class="header_left">
		<a href="/" title="">
			<img height="30" src="/images/logo-new.jpg" alt="Hire Au Pair/Nanny or senior caregiver" title="Au Pairs/Nannies and senior caregivers" />
		</a>
	</div>

		<div class="header_right ">
		
<script type="text/javascript">
	function inputFocus(i)
	{
	    if(i.value==i.defaultValue){ i.value="";}
	}
	function inputBlur(i)
	{
	    if(i.value==""){ i.value=i.defaultValue;}
	}
</script>

<form name="frmLogin" method="post" action="/login-submit.php">

	<div style="float:left;">
		<input name="userNameF" tabindex="1" type="text" value="Email" onfocus="inputFocus(this)" onblur="inputBlur(this)" title="Email" />
		<span>
			<a href="/forgot_password.php" title="Forgot password">?&nbsp;</a>
		</span>
	</div>

	<div style="float:left">
		<input name="passwordF" tabindex="2" type="password" value="Password" onfocus="inputFocus(this)" onblur="inputBlur(this)"  />
		<span>
			<a href="/forgot_password.php" title="Forgot password">Forgot?</a>
		</span>
	</div>

	<input type="hidden" name="hidSubmit" value="1" />
	<input type="submit" tabindex="3" value="Login" align="left" class="button" />

			<a class="button1" href="/en/p-login.php" title="Click here to register">Register</a>
	
</form>	</div>
</div>


<div class="menu_bg" >
	<div class="menu_container">
		

<div class="topNav">
	<div class="searchBox">
	<form action="https://www.aupair.com/pageSearch.php" method="post">
		<input type="text" name="pageKeyword" value="" placeholder="about Au Pair" style=""/>
		<input type="submit" name="search" value="" style="" />
	</form>
</div>
	<label for="menuIcon1">
		<a></a>
		<a></a>
		<a></a>
	</label>
	<input type="checkbox" name="menuIcon1" id="menuIcon1" value="1"/>

	<ul>
		<li>
			<a>Services</a>
			<ul>
				<li><a href="/en/aupair-agencies/index.php" title="View list of Au Pair Agencies providing Care Services in your area">Au Pair Agencies</a></li>

					
				<li><a href="/en/p-au-pair-programs.php" title="">Au Pair Programs</a></li>

					
				<li><a href="/en/p-au-pair-wiki.php" title="">Au Pair Wiki</a></li>


								<li><a href="/en/p-faq.php" title="">FAQ</a></li>									
								<li><a href="/en/p-plans.php" title="">Premium Membership</a></li>													

								<li><a href="/en/p-contact-us.php" title="">Contact us</a></li>	


			</ul>
		</li>	
			
			<li>
				<a>Families</a>
				<ul>
										<li><a href="/find_aupair.php" title="Look for your Au Pair/Nanny with an advanced search">Find your Au Pair</a></li>
												<li>
								<a href="/family_register.php?language=en">Register as family</a>
							</li>	
										<li><a href="/find_aupair.php?quick_search=search&amp;home_country=210&amp;gender=2&amp;language=en" title="Look for Au Pairs and Nannies that are currently living in USA">Au Pair from USA</a></li>
					<li>
						<a href="/en/country-wise/hire-au-pair-nanny-country-wise.php" title="Look for Au Pairs and Nannies by the countries they currently live in">
							Search by country						</a>
					</li>
										<li><a href="/en/p-family-info.php" title="">Host Family in 7 steps</a></li>
					
								<li>
									<a href="/en/p-plans.php" title="">Premium Membership</a>
								</li>
											</ul>
			</li>
			
			<li>
				<a style="padding-left:5px;padding-right:5px">Au Pairs</a>
				<ul>
										<li><a href="/find_family.php">Find a Family</a></li>
												<li>
								<a href="/aupair_register.php?language=en">Register as Au Pair</a>
							</li>
										<li><a href="/find_family.php?quick_search=search&amp;countryQ=210&amp;language=en" title="Au Pair and Nanny jobs available in USA">Au Pair Jobs in USA</a></li>
										<li><a href="/en/p-au-pair-info.php" title="">Au Pair in 7 steps</a></li>
					
								<li>
									<a href="/en/p-plans.php" title="">Premium Membership</a>
								</li>	
											</ul>
			</li>		
			</ul>
</div>	</div>
</div>


<style>

	.bannerContent
	{
		width:1000px;
		margin:100px auto 0px auto;
		padding:0px 0px;
		text-align:center;
		font-size:30px;
		color:#fff;
		font-family:century gothic;
		text-shadow: 2px 2px #444;
		text-transform:uppercase;
	}

	.bannerContent h2
	{
		font-size:100px;
		color:#fff;
		letter-spacing:30px;	
		margin:0px;
		padding:0px;	
	 	text-shadow: 2px 2px #444;
	}

</style>

<div class="banner_container1"  >
	<div class="bannerContent">
			</div>
</div>







<div class="container">

	
<div class="flag ">
		<ul>	
		<li>
			<a class="default" >
								<img alt="" src="/language/english_en_2.gif" width="18" height="14" />
				EN			</a>
		<ul>

	
						<li>
							<a title="Português" href="/pt/p-política-de-privacidade.php" style="font-size:16px">Português</a> 
						</li>
						</ul>
	</li>
	</ul>
</div>
	<div class="middle_content rightBar">
<div class="breadcrumb"  >	
	<a href="/">Home</a> <a style="color:#000;border-right:0px solid #000" title="Privacy Policy Agreement." href="/en/p-privacy-policy.php">Privacy Policy</a></div>

<h1>Privacy Policy Agreement.</h1>




			<div class="pageContent">
				<div>
	<br />
	AuPair.com highly values the protection of your privacy and security.&nbsp;</div>
<div>
	&nbsp;</div>
<div>
	AuPair.com respects the legislation concerning privacy and personal data protection. AuPair.com abides by the governing laws of the Federal Republic of Germany and the European Union legislation.</div>
<div>
	&nbsp;</div>
<div>
	This Privacy Policy provides the user with all the measures taken to protect his/her privacy. Hereinafter are described, for the users and the visitors of the AuPair.com website, the type of data that are collected, the reasons and the ways in which the data are treated and processed.In the moment you use the services of AuPair.com and give your personal information, you accept and agree to the rules formulated in this Privacy Policy agreement.</div>
<div>
	&nbsp;</div>
<div>
	If a registered user no longer wishes to use the services of AuPair.com, he/she always has the right to revoke his/her consent for the use of personal data in accordance with the law. In that case, AuPair.com asks for a written statement through an email. The mail will be sent by the address: info [at] aupair [dot] com. Consequently, all collected personal data will be permanently deleted and it will not be possible for any user/visitor/third party to recollect them from the system.</div>
<h3>
	&nbsp;</h3>
<h3>
	Collection, processing and the use of personal data</h3>
<div>
	In order to run the provided services by AuPair.com it is necessary to collect and process personal information. Compulsory fields are indicated through (*) and are necessary for the actual functioning of AuPair.com.</div>
<div>
	&nbsp;</div>
<div>
	The registration form on AuPair.com provides the basis for an exchange. Therefore, it is extremely useful to be as precise as possible when creating the user&#39;s profile.</div>
<div>
	&nbsp;</div>
<div>
	With every visit to the page AuPair.com usage data will be transmitted by the Internet browser and log files (server log files)will be stored: the retrieved Web page, Message Name of successful retrieval, file, browser type and version, date and time of access, amount of data transferred, the operating system of the user, IP address, referrer URL (page you visited before) and the requesting provider.</div>
<div>
	&nbsp;</div>
<div>
	In order to register as a host family and use the services, AuPair.com asks for:</div>
<ol>
	<li>
		Email</li>
	<li>
		First name</li>
	<li>
		Last name</li>
	<li>
		Address</li>
</ol>
<div>
	Relevant information about the family (for example number of family members, languages spoken, occupation of the parents, nationality, religion, children age and sex, family age group)</div>
<div>
	Other information that is useful for the research through AuPair.com</div>
<div>
	&nbsp;</div>
<div>
	With the registration, the user gives relevant information to other users and all website visitors. This visible information includes but is not limited to: first name, images (uploaded photos), description text, location (state, town), family details (how many members, children age and sex, religion, occupation, family age group, nationality) and matching criteria for au pairs.</div>
<div>
	&nbsp;</div>
<div>
	In order to register as an Au Pair and use the services, AuPair.com asks for:</div>
<ol>
	<li>
		Email</li>
	<li>
		First name</li>
	<li>
		Last name</li>
	<li>
		Address</li>
	<li>
		Date of birth</li>
	<li>
		Nationality</li>
	<li>
		Occupation</li>
	<li>
		Other information useful for research through AuPair.com</li>
</ol>
<div>
	With the registration, the user gives relevant information to other users and all website visitors. This visible information includes but is not limited to: first name, images (uploaded photos), description text, location (State, Town), other relevant details such as marital status, family members, occupation, spoken languages, desired family criteria, health status/allergies/special diets, education, driving license, religion, height.</div>
<div>
	&nbsp;</div>
<div>
	In order to register as an au pair agency and use the services, AuPair.com asks for:</div>
<ol>
	<li>
		Email</li>
	<li>
		Agency name</li>
	<li>
		Name of the agency representative</li>
	<li>
		Address</li>
</ol>
With the registration, the user gives relevant information for research to other users and all website visitors. This visible information includes agency name, address, website address, phone number, a brief description of the agency.
<div>
	&nbsp;</div>
<div>
	The email address you provide when you register on our website is strictly confidential and will remain hidden from all the other users at all times; your email will never be visible. The Email contact will be visible only to AuPair.com employees in order to guarantee security and protection to all users.</div>
<ol>
	<li>
		The complete name will be visible for premium members.</li>
	<li>
		The complete address will be visible for premium members if the user has not restricted the information in the profile settings.&nbsp;</li>
	<li>
		Phone contact details will be automatically visible for premium users in the list of favorites. However, the user can either change the settings into visible for all premium members or entirely restrict the visibility.&nbsp;</li>
	<li>
		The moment a premium member agency is classified by AuPair.com as trustworthy, it gets access to the contact details provided by users.</li>
</ol>
<div>
	Once a visitor registers on AuPair.com that person allows AuPair.com to collect, process and use his/her personal data. Both AuPair.com, other users and premium membership users are allowed to contact the user through the internal messaging system.</div>
<div>
	&nbsp;</div>
<div>
	Au Pair.com reserves the right to contact any member at any time regarding any membership-related problems or questions.</div>
<div>
	&nbsp;</div>
<h3>
	Disclosure of personal data to third parties</h3>
<div>
	To maintain the quality of our services, data that contain personal contents will only be disclosed to third parties if they constitute a potential arrangement for a job. Every user gives his/her consent to this disclosure in advance through the registration process.</div>
<div>
	In the case of fee-based services, the data required by the user to perform the payment will be transmitted to the payment operators (listed in our Payment Options) with the exclusive purpose of completing the payment. Each user gives consent to this data transmission, for further information it will be necessary to read the Privacy Policy agreements of the payment operators.</div>
<div>
	With the exception of these two cases, no data will be disclosed to third parties.</div>
<div>
	&nbsp;</div>
<h3>
	Data Retention</h3>
<div>
	Our service will retain the information that was provided by users as long as the account exists on our platform. The data might be used to comply with our legal obligations, resolve and ensure a safe environment for our users. Users of AuPair.com have at all times the right to modify or delete the personal data that is stored on the website.</div>
<div>
	Right of Information</div>
<div>
	&nbsp;</div>
<div>
	As a user of our services, you have the right to ask for information regarding your personal data at any time. The information requested can be provided through email or postal service.<br />
	&nbsp;</div>
<h3>
	Anti-Spam Policy</h3>
<div>
	AuPair.com does not send any commercial emails or any form of spam. If you received a spam message which you believe to be a scam, please contact our support team immediately.&nbsp;</div>
<h3>
	Use of analysis programs</h3>
<div>
	The Aupair.com website uses Google Analytics, a web analytics&rsquo; service provided by Google Inc. (&quot;Google&quot;).</div>
<div>
	The Google Analytics&rsquo; service uses &quot;Cookies&quot;, text files that are stored on your computer, to help analyze how users use the website.</div>
<div>
	&nbsp;</div>
<div>
	All information generated by cookies (including your IP address) will be transmitted to Google servers located in the United States of America and stored there.</div>
<div>
	&nbsp;</div>
<div>
	Google will use this information to evaluate and analyze the website and internet usage and create reports based on the collected information.<br />
	Google may also, according to the law, transfer this information to third parties that are evaluating the information on Google&rsquo;s behalf.<br />
	Google will not associate your IP address with any other data held by Google in conjunction.<br />
	You may refuse the Cookies by selecting the appropriate settings on your browser; however, we point out that in this case you may not be able to use all the features of this website appropriately.<br />
	By using this website, you agree to the processing of data by Google as described above and for the purposes set out.</div>
<div>
	&nbsp;</div>
<h3>
	Using Social Plugins</h3>
<div>
	&nbsp;</div>
<div>
	AuPair.com uses various social media platforms (&quot;plug-ins&quot;) and social networks (e.g. Facebook, Instagram, Google+). The Plug-ins are marked with a logo and are designed to connect the users&#39; browsers directly to the servers of the social networks.</div>
<div>
	The processing and use of data by the Social and the related rights and users&acute; settings are set out in the privacy policies of the respective platforms (eg. Facebook, Google+).</div>
<div>
	By creating a profile on AuPair.com the user agrees to the fact that his/her profile, without the mention of his/her name and contact details, may appear on eg. Facebook or Google+.</div>
<div>
	If the user does not agree, in order to avoid the collection of the data through Plug-ins and Social Networks, the user needs to log out from the relevant websites before he/she visits AuPair.com.</div>
<div>
	&nbsp;</div>
<h3>
	Links to other websites</h3>
<div>
	AuPair.com features links to websites that are not controlled by our team. All the information presented on other websites is not the responsibility of AuPair.com. In addition, this website is not liable for the privacy policies or material presented on such websites.&nbsp;</div>
<div>
	&nbsp;</div>
<h3>
	Evaluation of User&#39;s behavior</h3>
<div>
	Through the registration on Aupair.com the IP address and the time of the registration will be saved. AuPair.com database will automatically save the time and place of all the users&rsquo; logins.</div>
<div>
	Through the registration the user allows AuPair.com to treat the personal data for necessary analysis and research in order to maintain the high standards of quality and safety of the offered services. This allows AuPair.com to identify and block scams or inappropriate behavior. However, AuPair.com denies any responsibility for inappropriate contents present on the website.</div>
<div>
	&nbsp;</div>
<h3>
	Current state of the art</h3>
<div>
	&nbsp;</div>
<div>
	The user is aware that at the current state of the technology data protection for data transmission through Internet is not yet fully guaranteed. In particular, emails or messages sent through social media or apps cannot guarantee that the content is fully protected. &nbsp;The user understands and accepts that the transmission of data through the Internet is done at his/her own risk. AuPair.com is not responsible for the content exchanged through third party apps or other websites.&nbsp;</div>
<div>
	&nbsp;</div>

			</div>
		<br/><br/><br/><br/>		<div class="online_box1">
			<h4>
				Online Host Families			</h4>







		<div class="box">

			<div class="onlineImageBoxContainer">
	
				<img class="flagOnImg" src="/images/nationalityImages/67.jpg" style="box-shadow:1px 0px 2px #5593CB;" alt="German Au Pair Host Family" title="German Au Pair Host Family" />

				<div class="onlineImageBox imageBoxContainer" style="border:2px solid #5593CB;">
					<div class="onlineImageBox" style="margin-left:0px;">
						<a target="_blank" href="/en/job/erftstadt-germany-951688.php">
							<img src="/aupair-family-thumbs/082017/family-951688-1544200.jpg" border="0" alt="Saskias Family from Germany" title="Saskias Family offering an Au Pair job" style="width:100%;" />
						</a>
					</div>
				</div>
			</div>

			<p>
				<a target="_blank" href="/en/job/erftstadt-germany-951688.php">
					<span class="boxHeading" style="background:#5593CB;">
						Saskia					</span>
				</a>

				<a target="_blank" href="/en/job/erftstadt-germany-951688.php" title="View Detail Profile of family Saskia offering Au Pair job in Erftstadt city of Nordrhein-Westfalen, Germany for  10-12 months" class="boxButton">
					Au Pair Job in <b>Erftstadt, Germany</b> for <b>10-12 months</b>				</a>
			</p>
		</div>
	



		<div class="box">

			<div class="onlineImageBoxContainer">
	
				<img class="flagOnImg" src="/images/nationalityImages/84.jpg" style="box-shadow:1px 0px 2px #AC120A;" alt="Italian Au Pair Host Family" title="Italian Au Pair Host Family" />

				<div class="onlineImageBox imageBoxContainer" style="border:2px solid #AC120A;">
					<div class="onlineImageBox" style="margin-left:0px;">
						<a target="_blank" href="/en/job/hard-bregenz--austria-885705.php">
							<img src="/aupair-family-thumbs/032017/family-885705-1423600.JPG" border="0" alt="Del Regnos Family from Germany" title="Del Regnos Family offering an Au Pair job" style="width:100%;" />
						</a>
					</div>
				</div>
			</div>

			<p>
				<a target="_blank" href="/en/job/hard-bregenz--austria-885705.php">
					<span class="boxHeading" style="background:#AC120A;">
						Del Regno					</span>
				</a>

				<a target="_blank" href="/en/job/hard-bregenz--austria-885705.php" title="View Detail Profile of family Del Regno offering Au Pair job in Scheidegg city of Bayern, Germany for  10-12 months" class="boxButton">
					Au Pair Job in <b>Scheidegg, Germany</b> for <b>10-12 months</b>				</a>
			</p>
		</div>
	



		<div class="box">

			<div class="onlineImageBoxContainer">
	
				<img class="flagOnImg" src="/images/nationalityImages/62.jpg" style="box-shadow:1px 0px 2px #1E4161;" alt="French Au Pair Host Family" title="French Au Pair Host Family" />

				<div class="onlineImageBox imageBoxContainer" style="border:2px solid #1E4161;">
					<div class="onlineImageBox" style="margin-left:0px;">
						<a target="_blank" href="/en/job/cavalaire-sur-mer-france-929666.php">
							<img src="/aupair-family-thumbs/062017/family-929666-1505866.JPG" border="0" alt="Emmanuels Family from France" title="Emmanuels Family offering an Au Pair job" style="height:100%;" />
						</a>
					</div>
				</div>
			</div>

			<p>
				<a target="_blank" href="/en/job/cavalaire-sur-mer-france-929666.php">
					<span class="boxHeading" style="background:#1E4161;">
						Emmanuel					</span>
				</a>

				<a target="_blank" href="/en/job/cavalaire-sur-mer-france-929666.php" title="View Detail Profile of family Emmanuel offering Au Pair job in Cavalaire sur mer city of Paca, France for  10-12 months" class="boxButton">
					Au Pair Job in <b>Cavalaire sur mer, France</b> for <b>10-12 months</b>				</a>
			</p>
		</div>
	



		<div class="box">

			<div class="onlineImageBoxContainer">
	
				<img class="flagOnImg" src="/images/nationalityImages/155.jpg" style="box-shadow:1px 0px 2px #F3423A;" alt="Spanish Au Pair Host Family" title="Spanish Au Pair Host Family" />

				<div class="onlineImageBox imageBoxContainer" style="border:2px solid #F3423A;">
					<div class="onlineImageBox" style="margin-left:0px;">
						<a target="_blank" href="/en/job/tomares-spain-817304.php">
							<img src="/aupair-family-thumbs/052017/family-817304-1493306.jpg" border="0" alt="virginias Family from Spain" title="virginias Family offering an Au Pair job" style="width:100%;" />
						</a>
					</div>
				</div>
			</div>

			<p>
				<a target="_blank" href="/en/job/tomares-spain-817304.php">
					<span class="boxHeading" style="background:#F3423A;">
						Virginia					</span>
				</a>

				<a target="_blank" href="/en/job/tomares-spain-817304.php" title="View Detail Profile of family virginia offering Au Pair job in Tomares city of Sevilla, Spain for  10-12 months" class="boxButton">
					Au Pair Job in <b>Tomares, Spain</b> for <b>10-12 months</b>				</a>
			</p>
		</div>
	
</div>
<br/><br/>
<div class="online_box1">
<h4>
	Online Users</h4>
		<div class="box">
			<div class="onlineImageBoxContainer">
				<img class="flagOnImg" src="/images/nationalityImages/62.jpg" style="box-shadow:1px 0px 2px #5593CB;" alt="French Au Pair" title="French Au Pair" />

				<div class="onlineImageBox imageBoxContainer" style="border:2px solid #5593CB;">
					<div class="onlineImageBox">
						<a href="/en/hire/sousse-tunisia-532881.php">
							<img src="/aupair-family-thumbs/102014/aupair-532881-796966.jpg" border="0" alt="Au Pair &amp; Senior Caregiver  Lola" title="Lola, French, searching for a Au Pair &amp; Senior Caregiver job" style="width:100%;" />
						</a>
					</div>
				</div>
			</div>
			<p>
				<a href="/en/hire/sousse-tunisia-532881.php" title="View profile details of Lola, French Au Pair &amp; Senior Caregiver currently in France">
					<span class="boxHeading" style="background:#5593CB;">
						Lola, 24					</span>
				</a>

				<a target="_blank" href="/en/hire/sousse-tunisia-532881.php" title="View profile details of Lola, French Au Pair &amp; Senior Caregiver currently in France" class="boxButton">
					24 y.o. French Lecture, équitation, musique etc currently in France searching for Au Pair &amp; Senior Caregiver jobs in Belgium, Canada, France, Switzerland, USA				</a>
			</p>
		</div>
			<div class="box">
			<div class="onlineImageBoxContainer">
				<img class="flagOnImg" src="/images/nationalityImages/134.jpg" style="box-shadow:1px 0px 2px #AC120A;" alt="Polish Au Pair" title="Polish Au Pair" />

				<div class="onlineImageBox imageBoxContainer" style="border:2px solid #AC120A;">
					<div class="onlineImageBox">
						<a href="/en/hire/chobanin-poland-718073.php">
							<img src="/aupair-family-thumbs/082016/aupair-718073-1261740.jpg" border="0" alt="Au Pair &amp; Nanny  Klaudia" title="Klaudia, Polish, searching for a Au Pair &amp; Nanny job" style="width:100%;" />
						</a>
					</div>
				</div>
			</div>
			<p>
				<a href="/en/hire/chobanin-poland-718073.php" title="View profile details of Klaudia, Polish Au Pair &amp; Nanny currently in Poland">
					<span class="boxHeading" style="background:#AC120A;">
						Klaudia, 21					</span>
				</a>

				<a target="_blank" href="/en/hire/chobanin-poland-718073.php" title="View profile details of Klaudia, Polish Au Pair &amp; Nanny currently in Poland" class="boxButton">
					21 y.o. Polish Graduate (high school) currently in Poland searching for Au Pair &amp; Nanny jobs in Australia, Spain, Turkey, USA, Greece				</a>
			</p>
		</div>
			<div class="box">
			<div class="onlineImageBoxContainer">
				<img class="flagOnImg" src="/images/nationalityImages/105.jpg" style="box-shadow:1px 0px 2px #1E4161;" alt="Malian Au Pair" title="Malian Au Pair" />

				<div class="onlineImageBox imageBoxContainer" style="border:2px solid #1E4161;">
					<div class="onlineImageBox">
						<a href="/en/hire/london-united-kingdom-859062.php">
							<img src="/aupair-family-thumbs/012017/aupair-859062-1375148.PNG" border="0" alt="Au Pair  Aminata Moussa" title="Aminata Moussa, Malian, searching for a Au Pair job" style="width:100%;" />
						</a>
					</div>
				</div>
			</div>
			<p>
				<a href="/en/hire/london-united-kingdom-859062.php" title="View profile details of Aminata Moussa, Malian Au Pair currently in United Kingdom">
					<span class="boxHeading" style="background:#1E4161;">
						Aminata Moussa, 22					</span>
				</a>

				<a target="_blank" href="/en/hire/london-united-kingdom-859062.php" title="View profile details of Aminata Moussa, Malian Au Pair currently in United Kingdom" class="boxButton">
					22 y.o. Malian Student currently in United Kingdom searching for Au Pair jobs in Mali, Morocco, United Kingdom, USA				</a>
			</p>
		</div>
			<div class="box">
			<div class="onlineImageBoxContainer">
				<img class="flagOnImg" src="/images/nationalityImages/172.jpg" style="box-shadow:1px 0px 2px #F3423A;" alt="Turkish Au Pair" title="Turkish Au Pair" />

				<div class="onlineImageBox imageBoxContainer" style="border:2px solid #F3423A;">
					<div class="onlineImageBox">
						<a href="/en/hire/kocaeli-turkey-952691.php">
							<img src="/aupair-family-thumbs/082017/aupair-952691-1545919.JPG" border="0" alt="Au Pair  Elif" title="Elif, Turkish, searching for a Au Pair job" style="height:100%;" />
						</a>
					</div>
				</div>
			</div>
			<p>
				<a href="/en/hire/kocaeli-turkey-952691.php" title="View profile details of Elif, Turkish Au Pair currently in Turkey">
					<span class="boxHeading" style="background:#F3423A;">
						Elif, 18					</span>
				</a>

				<a target="_blank" href="/en/hire/kocaeli-turkey-952691.php" title="View profile details of Elif, Turkish Au Pair currently in Turkey" class="boxButton">
					18 y.o. Turkish Student currently in Turkey searching for Au Pair jobs in Australia, New Zealand, Pakistan, USA				</a>
			</p>
		</div>
	</div>	

	

	</div>

		

</div>










		<div class="proAupair" style="top:80px;left:15%">
			<a href="/display_banner.php?id=4" target="_blank">	
				<img src="/banner_ads/proAupairNew.png"  alt="Pro Aupair"  />
			</a>
		</div>



<div class="footer_language_container">
	<div class="footer_language">
		<b style="color:#fff;">English</b>
					| <a title="Deutsch" href="/de/p-datenschutz.php" style="font-size:14px; color:#5593cb;">
						&nbsp;Deutsch&nbsp;
					</a> 
				
					| <a title="Polski" href="/pl/p-polityka-prywatnosci.php" style="font-size:14px; color:#5593cb;">
						&nbsp;Polski&nbsp;
					</a> 
				
					| <a title="Español" href="/es/p-politica-privacidad.php" style="font-size:14px; color:#5593cb;">
						&nbsp;Español&nbsp;
					</a> 
				
					| <a title="Português" href="/pt/p-política-de-privacidade.php" style="font-size:14px; color:#5593cb;">
						&nbsp;Português&nbsp;
					</a> 
				
					| <a title="Italiano" href="/it/p-privacy-policy.php" style="font-size:14px; color:#5593cb;">
						&nbsp;Italiano&nbsp;
					</a> 
				
					| <a title="Pусский" href="/ru/p-politika-konfidencialnosti.php" style="font-size:14px; color:#5593cb;">
						&nbsp;Pусский&nbsp;
					</a> 
				
					| <a title="中文" href="/cn/p-privacy-policy.php" style="font-size:14px; color:#5593cb;">
						&nbsp;中文&nbsp;
					</a> 
				
					| <a title="Français" href="/fr/p-privacy-policy.php" style="font-size:14px; color:#5593cb;">
						&nbsp;Français&nbsp;
					</a> 
					</div>
</div>


<div class="footer_container">
	<div class="footer">
		<div class="nav">
			<h4>Au Pair Wiki</h4>
			<a title="Download Au Pair contract template - AuPair.com" href="/en/p-contract.php">Agreement</a><a title="About Au Pair program - Definition " href="/en/p-concept.php">Au Pair Definition</a><a title="Au Pair wages depending on the host country's regulations" href="/en/p-wages.php">Au Pair wages</a><a title="Au Pair tips before traveling abroad - Aupair.com" href="/en/p-arrival-host-country.php">Before going abroad</a><a title="Prove your experience with an Au Pair certificate" href="/en/p-certificate.php">Certificate</a><a title="Au Pair cost - how much should I invest to become an Au Pair?" href="/en/p-aupair-cost.php">Costs for Au Pairs</a><a title="How much does the Au Pair program cost? - AuPair.com" href="/en/p-cost.php">Costs for Host Family</a><a title="Tips for creating an attractive Au Pair profile on Aupair.com" href="/en/p-profile.php">Create your profile</a><a title="Au Pair responsibilities and duties: what should Au Pair do?" href="/en/p-duties.php">Duties &amp; Responsibilities</a><a title="Au Pair interview: tips to prepare for Skype talk" href="/en/p-skype-interview.php">Interview</a><a title="Au Pair host family letter: tips to make a nice impression" href="/en/p-hostfamily-letter.php">Letter to Au Pair / Family</a><a title="Male Au Pair: can a boy become an Au Pair? - AuPair.com" href="/en/p-male.php">Male Au Pair</a><a title="Au Pair vs nanny: what is the difference between them?" href="/en/p-nanny.php">Nanny vs. Au Pair</a><a title="Au Pair age requirements in different countries - AuPair.com" href="/en/p-required-age.php">Required age</a><a title="How to become Au Pair - what are the requirements?" href="/en/p-requirements.php">Requirements Au Pair</a><a title="Au Pair salary: pocket money standard in different countries" href="/en/p-pocket-money-salary.php">Salary</a><a title="Au Pair fraud cases - how to protect yourself from scammers? " href="/en/p-scam-fraud.php">Scams</a><a title="Visa requirements for the Au Pair program - AuPair.com" href="/en/p-visa.php">Visa how to get</a>	
			<a href="/en/p-au-pair-wiki.php">....</a>
		</div>

	
		
		<div class="nav">
			<h4 style="color:#f3423a;">For Au Pairs</h4>
			<a title="Au Pair in Australia: what to know before applying?" href="/en/p-au-pair-australia.php">Australia</a><a title="Au Pair in Austria: What to know before applying?" href="/en/p-au-pair-austria.php">Austria</a><a title="Au Pair in Belgium: All you need to know about experience" href="/en/p-au-pair-belgium.php">Belgium</a><a title="Au Pair in Canada: what to know before applying?" href="/en/p-au-pair-canada.php">Canada</a><a title="Au Pair in China: everything you need to know" href="/en/p-au-pair-china.php">China</a><a title="Au Pair in Denmark: spend a year in the happiest country" href="/en/p-au-pair-denmark.php">Denmark</a><a title="Au Pair England / UK: live abroad and learn English in UK" href="/en/p-au-pair-england-uk.php">England / UK</a><a title="Au Pair in Finland: everything about this experience" href="/en/p-au-pair-finland.php">Finland</a><a title="Au Pair in France: learn French while living abroad" href="/en/p-au-pair-france.php">France</a><a title="Au Pair Germany: live abroad and learn German" href="/en/p-au-pair-germany.php">Germany</a><a title="Au Pair Ireland: Information about this host country" href="/en/p-au-pair-ireland.php">Ireland</a><a title="Au Pair Italy: Information about this host country" href="/en/p-au-pair-italy.php">Italy</a><a title="Au Pair Luxembourg: Information about this host country" href="/en/p-au-pair-luxembourg.php">Luxembourg</a><a title="Au Pair in the Netherlands: everything about the program" href="/en/p-au-pair-netherlands.php">Netherlands</a><a title="Au Pair New Zealand: improve English while living abroad" href="/en/p-au-pair-new-zealand.php">New Zealand</a><a title="Au Pair in Norway: what to know before applying?" href="/en/p-au-pair-norway.php">Norway</a><a title="Au Pair in Spain - learn Spanish abroad while living in Spain" href="/en/p-au-pair-spain.php">Spain</a><a title="Au Pair in Sweden - spend a year in Northern Europe" href="/en/p-au-pair-sweden.php">Sweden</a><a title="Au Pair in Switzerland - discover 3 cultures in one country" href="/en/p-au-pair-switzerland.php">Switzerland</a><a title="Au Pair in Turkey: what to know before applying?" href="/en/p-au-pair-turkey.php">Turkey</a><a title="Au Pair USA: Experience America! Join the Au Pair program! " href="/en/p-au-pair-usa-america.php">USA</a>		</div>

		
				
		<div class="nav">
			<h4 style="color:#f3423a;">For Host Families</h4>
			<a title="Au Pair Australia - Information for Host Families" href="/en/p-australia.php">Australia</a><a title="Au Pair program in Austria - How to host an Au Pair?" href="/en/p-austria.php">Austria</a><a title="Au Pair program in Belgium - Information for Host Families" href="/en/p-belgium.php">Belgium</a><a title="Au Pair program in Canada - find an Au Pair for your family" href="/en/p-canada.php">Canada</a><a title="Au Pair program in China - Information for Host Families" href="/en/p-china.php">China</a><a title="Au Pair in Denmark - Information for Host Families" href="/en/p-denmark.php">Denmark</a><a title="Host an Au Pair in the UK/England - program information" href="/en/p-england-uk.php">England / UK</a><a title="Host an Au Pair in Finland - information for Host Families" href="/en/p-finland.php">Finland</a><a title="Host an Au Pair in France - Information for French families" href="/en/p-france.php">France</a><a title="Host an Au Pair in Germany - Information for German Families" href="/en/p-germany.php">Germany</a><a title="Host an Au Pair in Ireland - Information for Irish Families " href="/en/p-ireland.php">Ireland</a><a title="How to host an Au Pair in Italy? Guide for Host Families" href="/en/p-italy.php">Italy</a><a title="Au Pair Program in Luxembourg for Host Families" href="/en/p-luxembourg.php">Luxembourg</a><a title="Au Pair Netherlands - Information for Dutch Host Families" href="/en/p-netherlands.php">Netherlands</a><a title="Au Pair New Zealand - Information for Host Families" href="/en/p-new-zealand.php">New Zealand </a><a title="Au Pair Norway - Information for the Host Families" href="/en/p-norway.php">Norway</a><a title="Au Pair Spain - Information for Spanish Host Families" href="/en/p-spain.php">Spain</a><a title="Au Pair Sweden - Information for Swedish Host Families" href="/en/p-sweden.php">Sweden</a><a title="Au Pair Switzerland - Information for Host Families" href="/en/p-switzerland.php">Switzerland</a><a title="Au Pair Program in Turkey for Host Families" href="/en/p-turkey.php">Turkey</a><a title="Au Pair Program in USA for Host Families" href="/en/p-usa.php">USA</a>		</div>

						<div class="nav" style="color:#f3423a;">
					<h4 style="color:#f3423a;">Host Families in</h4>
					<a title="Au Pair, nanny and care jobs in Spain" href="/find_family.php?quick_search=search&amp;countryQ=184&amp;language=en">Spain</a><a title="Au Pair, nanny and care jobs in Italy" href="/find_family.php?quick_search=search&amp;countryQ=99&amp;language=en">Italy</a><a title="Au Pair, nanny and care jobs in USA" href="/find_family.php?quick_search=search&amp;countryQ=210&amp;language=en">USA</a><a title="Au Pair, nanny and care jobs in Germany" href="/find_family.php?quick_search=search&amp;countryQ=76&amp;language=en">Germany</a><a title="Au Pair, nanny and care jobs in United Kingdom" href="/find_family.php?quick_search=search&amp;countryQ=208&amp;language=en">United Kingdom</a><a title="Au Pair, nanny and care jobs in France" href="/find_family.php?quick_search=search&amp;countryQ=70&amp;language=en">France</a><a title="Au Pair, nanny and care jobs in Australia" href="/find_family.php?quick_search=search&amp;countryQ=13&amp;language=en">Australia</a><a title="Au Pair, nanny and care jobs in Switzerland" href="/find_family.php?quick_search=search&amp;countryQ=191&amp;language=en">Switzerland</a><a title="Au Pair, nanny and care jobs in Turkey" href="/find_family.php?quick_search=search&amp;countryQ=202&amp;language=en">Turkey</a><a title="Au Pair, nanny and care jobs in Canada" href="/find_family.php?quick_search=search&amp;countryQ=36&amp;language=en">Canada</a><a title="Au Pair, nanny and care jobs in Ireland" href="/find_family.php?quick_search=search&amp;countryQ=97&amp;language=en">Ireland</a><a title="Au Pair, nanny and care jobs in China" href="/find_family.php?quick_search=search&amp;countryQ=42&amp;language=en">China</a><a title="Au Pair, nanny and care jobs in Austria" href="/find_family.php?quick_search=search&amp;countryQ=12&amp;language=en">Austria</a><a title="Au Pair, nanny and care jobs in Belgium" href="/find_family.php?quick_search=search&amp;countryQ=20&amp;language=en">Belgium</a><a title="Au Pair, nanny and care jobs in Netherlands" href="/find_family.php?quick_search=search&amp;countryQ=142&amp;language=en">Netherlands</a>				
				</div>
			
		
		<div class="nav" style="border:0px solid #ccc" >
			<h4 style="color:#f3423a;">Au Pairs in</h4>
			<a title="Hire Au pair, Nanny and Senior Caregiver from Spain" href="/find_aupair.php?quick_search=search&amp;home_country=184&amp;gender=2&amp;language=en">Spain</a><a title="Hire Au pair, Nanny and Senior Caregiver from Italy" href="/find_aupair.php?quick_search=search&amp;home_country=99&amp;gender=2&amp;language=en">Italy</a><a title="Hire Au pair, Nanny and Senior Caregiver from USA" href="/find_aupair.php?quick_search=search&amp;home_country=210&amp;gender=2&amp;language=en">USA</a><a title="Hire Au pair, Nanny and Senior Caregiver from Germany" href="/find_aupair.php?quick_search=search&amp;home_country=76&amp;gender=2&amp;language=en">Germany</a><a title="Hire Au pair, Nanny and Senior Caregiver from United Kingdom" href="/find_aupair.php?quick_search=search&amp;home_country=208&amp;gender=2&amp;language=en">United Kingdom</a><a title="Hire Au pair, Nanny and Senior Caregiver from France" href="/find_aupair.php?quick_search=search&amp;home_country=70&amp;gender=2&amp;language=en">France</a><a title="Hire Au pair, Nanny and Senior Caregiver from Australia" href="/find_aupair.php?quick_search=search&amp;home_country=13&amp;gender=2&amp;language=en">Australia</a><a title="Hire Au pair, Nanny and Senior Caregiver from Switzerland" href="/find_aupair.php?quick_search=search&amp;home_country=191&amp;gender=2&amp;language=en">Switzerland</a><br/>	

					
	
			<h4 style="color:#f3423a;">Au Pair Agencies</h4>
			<a title="Au pair, Nanny, child and Senior Caregiver agencies in Spain" href="/en/aupair-agencies/spain.php">Spain</a><a title="Au pair, Nanny, child and Senior Caregiver agencies in Italy" href="/en/aupair-agencies/italy.php">Italy</a><a title="Au pair, Nanny, child and Senior Caregiver agencies in USA" href="/en/aupair-agencies/usa.php">USA</a><a title="Au pair, Nanny, child and Senior Caregiver agencies in Germany" href="/en/aupair-agencies/germany.php">Germany</a><a title="Au pair, Nanny, child and Senior Caregiver agencies in United Kingdom" href="/en/aupair-agencies/united-kingdom.php">United Kingdom</a><a title="Au pair, Nanny, child and Senior Caregiver agencies in France" href="/en/aupair-agencies/france.php">France</a><a title="Au pair, Nanny, child and Senior Caregiver agencies in Australia" href="/en/aupair-agencies/australia.php">Australia</a><a title="Au pair, Nanny, child and Senior Caregiver agencies in Switzerland" href="/en/aupair-agencies/switzerland.php">Switzerland</a>	
		</div>

	</div>
</div>

<div class="footer_container1">
	<div class="footer1">
		
					<a title="About AuPair .com - AuPair.com" href="/en/p-about-aupair-com.php">About AuPair .com</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
					<a title="Do not hesitate to contact your AuPair.com Team" href="/en/p-contact-us.php">Contact us</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
					<a title="AuPair.com - Our users' most frequent questions" href="/en/p-faq.php">FAQ</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
					<a title="Become Premium and send personalized messages - AuPair.com" href="/en/p-plans.php">Premium Membership</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
					<a title="Privacy Policy Agreement - AuPair.com" href="/en/p-privacy-policy.php">Privacy Policy</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
					<a title="Terms and Conditions - AuPair.com" href="/en/p-terms-conditions.php">Terms and Conditions</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
					<a title="Legal Information - AuPair.com" href="/en/p-legal-information.php">Legal Information</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;
					<a title="Internship for students at AuPair.com" href="/en/p-job.php">Jobs</a>
						
	</div>
</div>









<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-534554-2']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type =
'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' :
'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(ga, s);
  })();

</script>









</body>
</html>