


<!doctype html>
<!--[if lte IE 8]>  <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]>  <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]--> 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
<link rel="canonical" href="https://www.adp.com/privacy.aspx"/>
<meta property="og:title" content="Privacy"/>

<title>
	Privacy
</title>
    <!-- Data Layer for GTM -->
<script>
    dataLayer = [{
        'ipAddress': '207.241.231.163'
    }];
</script>
<!-- End Data Layer for GTM -->
    
<!--Main Include Start-->
<link rel="alternate" href="https://www.adp.com" hreflang="en-us" />
<link rel="alternate" href="http://www.adppayroll.com.au" hreflang="en-au" />
<link rel="alternate" href="http://www.adp.com.br" hreflang="pt-br" />
<link rel="alternate" href="http://www.adp.ca/en-ca/default.aspx" hreflang="en-au" />
<link rel="alternate" href="http://www.adp.ca/fr-ca/canada-home.aspx" hreflang="fr-au" />
<link rel="alternate" href="http://www.adp.cl" hreflang="es-cl" />
<link rel="alternate" href="http://www.es-adp.com" hreflang="es-es" />
<link rel="alternate" href="http://www.adpchina.com/english" hreflang="en-cn" />
<link rel="alternate" href="http://www.adpchina.com" hreflang="zh-cn" />
<link rel="alternate" href="http://www.fr.adp.com" hreflang="fr-fr" />
<link rel="alternate" href="http://www.adp.in" hreflang="en-in" />
<link rel="alternate" href="http://www.it-adp.com" hreflang="it-it" />
<link rel="alternate" href="http://www.de-adp.com" hreflang="de-de" />
<link rel="alternate" href="http://www.adp.nl" hreflang="nl-nl" />
<link rel="alternate" href="http://www.adp.pl" hreflang="pl-pl" />
<link rel="alternate" href="http://www.adp.ch" hreflang="de-se" />
<link rel="alternate" href="https://www.fr.adp.ch/" hreflang="fr-se" />
<link rel="alternate" href="https://www.en.adp.ch/" hreflang="en-se" />
<link rel="alternate" href="http://www.adp.co.uk" hreflang="en-gb" />

<link rel="stylesheet" href="/ADP%20USA%202015/css/common-header-preset.min.css" type="text/css" />

<!--[if IE]>
<script type="text/javascript" src="/ADP%20USA%202015/js/PIE.js" nobundle="nobundle"></script>
<![endif]-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KH3TMH');</script>
<!-- End Google Tag Manager -->
<!-- Start BounceX tag. Deploy at the beginning of document head. -->

        <script>
     var ScriptBounceXExists=true;
        (function(d) {
            var e = d.createElement('script');
            e.src = d.location.protocol + '//tag.bounceexchange.com/2098/i.js';
            e.async = true;
            d.getElementsByTagName("head")[0].appendChild(e);
        }(document));
        </script>

<!-- End BounceX Tag-->
<!--Main Include end, Secondary Include Start-->

<link rel='stylesheet' href='/ADP%20USA%202015/css/sitemap.css' type='text/css' />
<!--Secondary Include End-->
                    <script>var w=window;if(w.performance||w.mozPerformance||w.msPerformance||w.webkitPerformance){var d=document;AKSB=w.AKSB||{},AKSB.q=AKSB.q||[],AKSB.mark=AKSB.mark||function(e,_){AKSB.q.push(["mark",e,_||(new Date).getTime()])},AKSB.measure=AKSB.measure||function(e,_,t){AKSB.q.push(["measure",e,_,t||(new Date).getTime()])},AKSB.done=AKSB.done||function(e){AKSB.q.push(["done",e])},AKSB.mark("firstbyte",(new Date).getTime()),AKSB.prof={custid:"267330",ustr:"",originlat:"0",clientrtt:"3",ghostip:"23.61.195.165",ipv6:false,pct:"10",clientip:"207.241.231.163",requestid:"5abeed",region:"17497",protocol:"",blver:13,akM:"x",akN:"ae",akTT:"O",akTX:"1",akTI:"5abeed",ai:"191013",ra:"true",pmgn:"ADPRUM",pmgi:"",pmp:"",qc:""},function(e){var _=d.createElement("script");_.async="async",_.src=e;var t=d.getElementsByTagName("script"),t=t[t.length-1];t.parentNode.insertBefore(_,t)}(("https:"===d.location.protocol?"https:":"http:")+"//ds-aksb-a.akamaihd.net/aksb.min.js")}</script>
                    </head>
<body >
    <script language='JavaScript' type='text/javascript' src='/js/s_code.js'></script>
<script language='JavaScript' type='text/javascript'>

/*You may give each page an identifying name, server, and channel on the next lines.*/

s.eVar2="English United States";
s.eVar4="Corporate";
s.eVar9="Main Category Page";
s.prop9="Main Category Page";
s.prop6="Privacy";
s.eVar3="Employer Services";
s.prop3="Employer Services";
s.prop4="Corporate";
s.eVar1="United States";
s.pageName="us|corp|es|privacy";
s.prop2="English United States";
s.server="Production";
s.prop1="United States";
s.eVar6="Privacy";
s.eVar27="21";
s.eVar29="etbmmo5q3rgq3wt3bi2qu2jl";


/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/

var s_code=s.t();if(s_code)document.write(s_code)//-->

</script><script language='JavaScript' type='text/javascript'></script><noscript><a href='http://www.omniture.com' title='Web Analytics'><img src='http://adpglobalnewsite08dev.112.2O7.net/b/ss/adpglobalnewsite08dev/1/H.17--NS/0' height='1' width='1' border='0' alt=''></a></noscript>
<!-- DO NOT REMOVE -->
<!-- End SiteCatalyst code version: H.17. -->

    <form method="post" action="/privacy.aspx" id="ctl02">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="cpb/SuHfF3n107K2yfWFgEw1awhAIP+ftodd1Poi24Hk6zSpImIbwOtEW9O6KlqDvWHDw+CVoh7ZzQwxN//dJP+YO9a0lB0tsSXwlsjey2CWvXlOS7uN+AbhcXfnbGfBaJJH3i2om7pNIKp19ykGjYhBlTt5rvzxgeQQf0UD1RowLifr9m+TZKgnvDBCi3eEsx/WW5wLuX7wJp6p9owHxNRINIERWCUv86Epf9dBWcv9nvt48G5uLKYqEAoXaSFyMYx1G95BGEKaYQtiyU5G1hVvbSBnysHIEyGNT9CC7lSeCjcSL4ssu1tdz9clfPifjR34N2gqyfF9Me6XSHQ9Pz7MbLijeUmB2s9Wy5QUXVgK1Qy9icYCq9WPpR8bCQofgagHOXRs9tIfSjh5WKgYjsshVKxDO0QIwmHIzhKLnMF+yd/vtGWro5/yiSmC0D1e4L5HmKPcQy6yB7U/E4VrT7/cYPmns6Ofet/9Nj143G4gIDeK8stdSNilfMIXnPmpGswI8yC9ZwgKs4iNBX3OaF+xnM0M5dGZfC0+ivpDgkQiWBFVvQ6b7LQ+AWnI6F20HUxc6Le2DZ+O3HijhYytGZSU6Y5rHRPhKCwbKER6LrLk0WycZ3Y7120eoGW09v5ma1aXreDWVj0u3GkW24Q2Cb4V4TfTVyUusMTjPtl+UUy139eqNRs0ZFuUey9MXp9VvdAbuf17oHlFtXbGC7H30eIRe2Z0YWRMaHUeN4iCxWQIksiQisIEDkWF9+SJRIrPudbHb7IXUiuysOSOxxwJjKTc1drNT6kwIOv/FnRbobum5pg7fW3h1ILe8lMyJM+1iuztDE/KbqE2lBXWmTrRqiw4vsR1jaFzwuTE5HjGJ3RH+1daa5RIHk6nZyLtZzQBK5mREopVqYMlPwBzB/3WX3vSQgnpzfeL37E7ZzNB8Bsaec8rFTykjrzYV6bXZ0Mt3s9T4/Oyz5VPaKKmV3l6f8uB/DofAnmPFypWvQFca+FQrEbLqZO2VQNHB4xJBS5ZwBINabbzt53o5eJV4onXN7gLCAER85o9y7vh5oqwuVkzyaWDfUOuxpz4rUr7Cyt3Q/fxwxNEsUED4LsKHMWAIKVRd89Pd+tbR8i0itFDmt1AiG/mxj2xZLBNIcIC7BtGVIHkVbVSCbb/d5NclaeqI1Dnla4tLYxgpylsC6W+bX1z+Covc+icsQ2c2eNmIpBrsubMaUXowGUgbdJxcHelGnU+k68VtS9w0oHIP32iFWHoxCPf7wPsR6UmsaAy1uZ4xKlrHCfklYAm2AbS6i/gwVbUzdYmrKxzB9xRVGo4v9EgBBMvPalqFUcW9sHLTsrT+qbdaNaVzsJpIokJGDyhgwjOdo5L/q8ZZ5oORywFHzen2JiVguaY+q014yazdlbLw4oWs66Lz1OM0vSr2Gt09dK/lJpaHeO+zJo4w4HxI/9ihx0Vz76PDqZ3B7hxVKY+jWjV7kV7l4/orTSNoTN4mbM3+TQEz0ymI/TDnQ1vZbbTB7AtADpoSORZheVWg3Hp2rDN16dcIm7JXonfzJBOBCOH+5g8Br6200qvhoe6sK/AbYIVKBSnBkYZH47YoHbDKcykF1i3e4Miq1GizdshMUbCJu0PFEK9R9jk36hs3L1g5Kn9j3ccBq9RxSaqdcMcuXbZFGKvS5dSQowaZFlRKOCPZT2yrLs5UXjZ7+r1qVgChCFBZHV2vGwPSINEe66bXGWDa6rSIEoYMSUPWpGu330H18HNQXS7Yt1uiUfvEQsdcMw/QS0Pi0jTBNMjT/9e714mJVh2luc4BePwNxYR11AB5JoHxsvb9Ikk5MDXfoSCXM1ms2gU7CcoWknfsFBYIrUT28Sb8SKVYTWWsAIxGiUngnucuWKAvPIVaWgSmqAokz9Rl+6f+infUr6IPs6Dey/rYnIVe53hXrPK8BxCAarI001RIWqc+c12AnfayWJAbbZaS/RWRWz04x34wNlDje/cv9WSWydB4PViXp3Ut+UtsCv2/p3dU24xdJ5NGKXiWAEAJ+3mUFyN7XL4iWo6R02w29YTFdc2Q6p+B3at9R613hb5Q4Fse1D0TRBGa97Pyl9Q2STs9yc/NfGSC991FlKxpLLaE8P3FEGci0dcqMdgENjEOw3jryRv0c+E6Ruk7IMNMdAGKxT54FF0Xux557twkukXP6I6QnbAT5bO2kI+j6hc4nwp7PWqpiQ2ksIDz9RHMeHE1Ea+8TdXYQrnszhxH06mcbeFuIeCajY7IvGjk462YkK2r4JcrUQaLk7Y0VGC+DpExeZVFH+TDRxSnFCzWE1NkjSnJ64NfNxf42lNB5+cteAwUrapXS269SVmqOFpa7qtqXquXqqO7qKZmD5egKAY6L1Cb7VIJ+sQcbEY2+EZd/nbz6XNb1A0b2NJyk1QVrIxplqn+u9iDLbDpz4B8dzYl+PngZ19KMdh9SwEwrxUWGX1NzQ6fpJ7F2GgVKGjdsVOtnlhm0Dd8tLaKvTLLmtUCjN276jwOkubx+5fm6h5ps8JbDBCDBc65JheybGQwRR2g65BKtsF8KFzGZlVRTiA4ZyZCXOwJIxnnTgGLtQ424OK7bep79AibS+4qxDcJCxZ8Kat6os7Qzz5jcUhHEJiypwqGzuyw0NItC5wXXqhseJw/U2NVLknmFq+AaKZzSJTT1RuFovJXSkgQvrwevgefdWQv6F11pxpaINGUCWsJhj/wakQZutWKdvy6xUrn9hXt5UX4gctsnbxGNhRkD4u1WbVkSkPTX5hKu4ws/+ntA0zCyLf4+OYm+vbsO+cfCWlsLlR/w7lZumIzExBGpj9ubEcxRjiyB+tIIrPD8pZJEVZbGob6jtF0XazW7N6XKtDmzU7dUe5yofnksWaAdZwuPrL3ZS/c2K2jpl26fHptklkGvEphpIn+elcpCdDHSPd6yvuYw7jGMgmcBBzL89OA26VkMg8T9FWaH1DsxdG1gLOcjS8bfyRUQnpRrxQdV+zXIXE7DzPNtOHhDmO7lvzjeTQmsbxQTlFnD0VMNBJlfDUcZFdKDR1nEC9eK7A4C25mFFZtqM6/qQ4Du7gTC+yliHlRPJreFWw5Iq/4rtdxRxQo6I41FDB9niP7mtH8TXFlMzrXmHazTB7eYsSYvaUD6Uskonoz0pGT4nrVdnZNaK8pdk4F8Gv9OjDe7ROB13EGsckAbVTvpDb7RgJxjktr5abXkRSl3DBM5ohStEaXI5jyz9p1zIUoCLuPUZPxqwzEBakJwSD3A9TaVMbjaCpAccCm5GCh2z9dFlyPqQZSpJps2YSAnwmhLig7yeqIfbtdhuM+3Z2DXpWJraeBrgKXP8RMLhvRwqH9AqKV7J2vxHJ89BaJKFNpmK5Kmyxb8n4x9RQChfJCHVWVghA/ZpTwIt/7PcsBzEslENFC5dM4hUda3UwA5KE+8+gWPyMSdsm5EoA66hE4REj5HIT0WND7t/+e0D4fnFvcrC38GTx9Kls378MYlGHdDc32mbiP6NMQJHwWydI/Tl3WWY9MyZVtwF0Exm8jz5jJjVNx6qgEyPHi0iOhNj4TmLo4qJCUaP8uLeaDT5dkqJ/FcvccgvhNL4P+tFUvRr2ECcfGGathcpRY0n/cK2OwoMois8tur611GX5nFciY8S3SKQfZHESoV7X5T4oVAslTrPXBUz4Budd2PxdmXCgNBUSodMxT/y+rzCaN4yTyCI6PbKiUvw+jSoAUnesA/3DVqsTOiFAT1Fj9o72jCZ4xGKnF+xQ5fBJTfX0o24SOaZ4S/YFDFeXnU2B3lK9fnmMywXFcMtzIq4JSvA6H5kFFXulQWt16XrSj6w/M1wk+IvGxhSU+mmaxMYtwcluPfWUiDMifLu+AC5YVa/FYCj+gMV3eAXc0HWaQedGnA1s0ZlivdYYTDlrILET7zN+PYtq12IQk0Y1MMY8jW2OeugEGcaASRH+EHDMm0pZq92hTIucgaAmzS2tVHG/1/eHUEmVePx4i5MGwxo7LG4JiYc0Dfzys4y5fYXTifAoq5pR/QFTvSSdLGtEcAI0sbsDSXQuxmUEF1J29mDQn3PHMYkjTz59U3zDMw7U59GczqV5trI8t4FZ2TujA4YyHh/tO6n4XU8q1eSL2hzlMWRow8Iia58xrS2JVCriyLANF93mx0/BgeT5j/AJ4FTjnhH8VtLQ5PzzkmTfJQKvt4FYZEWsz15dvsW/2uCr4UyHvWYnoWwv1YJruQyHWsxBNBtuBGsQ22go+bum8eWVdvho4QVdQHV4fM8GDeGD0ddVxSZojqqb5Bzhit3xQN2aYomO0q2oN1IxB24OtikC/Bziy0PLipMK2xO3B7wgzn5aTDnQg1U+pxONCYIh0hLYRFZHjDqfrXjjg/ixLn4lfGPeRreFRERoSGOA4ob/wmkk4VvID+DYcvTGhkALwSG02gBYl6cOzLbD1C3bLotjC6ZT4drLEQO+fkWzgpcspe45OuSRK6odHARe6A7qHhxphRvs6cSjKU3E5HT9wU09POwp1BJHE0Y0GBT2J8p2hDJ3zN8aKe9Dthr2yJwTtWduSFHwZjSYLi/Bm0f1yOsHmbBYMPt9I7u4mvCoZdE/jYJL1x2QJ+OeRefPRK7HBnFpnl43CZYwM1aeaxdk7yHokXqSzOfGTwVREJrBEUwQ95tOCgCB7++WF14o4L5q6zRs8dmlvImNHVS5AvPI9I2j/ygvlLmFS5nQVvQXNIGd0lVcBtZ9S+enxxoTzmYJttfg7ltLpz0T4x7+FR1/UcMkkPxlrKM0rCFe78vfxMTl0VvzhFGYUZhdxKpXEkBOuELMzCDQMoa5gQWIQxo4uOtYkDh8gW9w+InBtdS7MK8RDxWrOVEVVpyuVJo2cuz7uCnohVKre3G2eLfb/7jOQmb5CHKYmpyLMheyryWiig0wNfMxsvZfJBlSrvRZ7XmYn6Na71eW0NpJmSL6T0xpkWsVmJXEP6AMBHMZ8bkYIe5t00i0k4sH0X083gRSbo0foZJMkrmOC+KjKSg1l9TXV5Y/SiOqlL3EDwVZiGS+H2DFXw4yPWk7nkqg4jDcoitP8mLqMiwf1VRsz/+hTct0EXoTaVJyhvmk0b/jf5YM58T3l86guHTyMWESMP4E70O9r+apN1eGVtSx0Y33YidOR3QAS2KSQRWuMP7xAaYfDpxXPXmfXzKj8G8HSVzABSvMOLcq210GB2oqaFV22fcDvcX+7xDRczFyJTLXrO+Wz2Ul5/r0sQYSmqUMb440dDo6YJia+JD4hEqHOwjybHH+jtKtzOtbCJiR6gk5brGlOChgWCVHFWgOoQa+clLBP7Ze7XkuYOQpscU7mNvwAX4yG95Kr52m53ty27N4TU5JGSXl2f6U7gA6Yz3WQVm/tJhXYdA/DQt4HQo91Wd57mKcRGHenDM508Ef2o4kOPjZtdEugKjSUWpJNMePH6fc0kvsNhyBIO+in60poeZv+qEKdViqFBLygTOtN2jKN+FY3+4NLuL8E1TRXzID43lcxkSLF6lDu4KvAS5hAUkqHy4M6wTycv7K2z1+nzL6g7uD6CpIqlJjiEu3qp3KJB3dSn2VSXgBL/lhGU+CCltAZJEDJmmGlxFAAcojkT/aCqFCKoP1KduzLZRIy5TEL3a50HKCvwj6gLBF8udo3dP2k6ROha97TdOkuhq9UkJMq31IhZ0+biwc+YTIbo1RqY67gTLMZHavMgf55pt81bzSV/llffSVAPqSgd5GZsh0k2RAg0Unw5aEB9irhEAwKxJdZ/nyjSHGQD1wpRhyXG/H8GrFxE0dJ8joLR0pigfTSyeENsfi+JLxDx5y7lXb13vL7Wq025HqgfO+q5yeoEixqySZK1F0XnM3HKgNUGgZtmQn6S8l8gmfU5/sta92D8L73AYgtOXecO+4kJyiFyD7ECgeEddGcDhIDRnkshlPF6WvaBVinPQ4bf/9+EV6Ozgf/rqVO/AEV73a/IEna9kmEfroiofCujITRGaVfzEDJdLyjCuEbBL5YoJfDNVmD7yUdPS2iW95detwrDv59qbTJilwfimK4xHQJSuqXFg62I2IQcTgK01A0dZkvIkRTWbLM4xGx9l4TXsYMWvzkdM7+rYDGLtmVrslevz0rMLiOK2xiNGBUBsOJ7I3Qp8V8s4LhUPWnJXzit7zRd+mh+vKH10c1LSImHMBM1LZRirxJqQQRH0MtDuYGO1nyRiiANJn4FYiFb14O4h5iddTwJMDfRO+7uV11LhHJ5N/jprppTc7/7IkfcFe31GnhOohi9qpnuX7HHYV6Y16EY0zs/5QpL4=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['ctl02'];
if (!theForm) {
    theForm = document.ctl02;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>



<script src="/ScriptResource.axd?d=mkHh3H7FZVas5l3AY3JDjCg9qnZ6Lb2wLV1_rWuEpvfvI6N4qrkOmEYq3hTdpPoCSuUzVMnmNchfaIeSBYNNoviW0uarjeloz4pIJf5mircEvCJMsOI5UUcr7AA1im6aeltFYcyDISqK1tAaMG6J4feLMZ7vWOUm24QyiaD0xbgdIhWSUyA2BPDPY4SMr4arWD0xqqB8KTbjdqWP3adi9gnthkSJe1_HAV9wcja_EvKFX-Gs0" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B9C9B541" />
</div>
       
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager1', 'ctl02', [], [], [], 90, '');
//]]>
</script>
 
   
    
<aside class="adp-site-menu-2015 side-bar adp-nav-2015">
    <a class="close-site-menu" href="javascript:void(0);">
        <img src="/ADP%20USA%202015/images/GlobalNav/close_x.png" />
    </a>
    <nav>
                
    
    
    
    
    <ul class='main-link bu-nav'>
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_0_hlItemLink_0" href="/solutions/small-business.aspx">Small Business<span>1 – 49 employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_1_hlItemLink_1" href="/solutions/midsized-business.aspx">Midsized Business<span>50 – 999 employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_2_hlItemLink_2" href="/solutions/large-business.aspx">Large Business<span>1,000+ employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_3_hlItemLink_3" href="/solutions/multinational-business.aspx">Multinational Business<span>of any size</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_4_hlItemLink_4" href="/partners.aspx">Partner Solutions<span>Accountants, advisors, and more</span></a>
        
    

        </li>
        
        </ul>
    
    
        
    
    
    
    
    <ul class='corp-nav'>
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_0_hlItemLink_0" href="/">Home</a>
        
    

        </li>
        
        
        <li class='NoHighlight' >
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_1_hlItemLink_1" href="/solutions.aspx">Services & Products</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_2_hlItemLink_2" href="/why-adp.aspx">Why ADP</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_3_hlItemLink_3" href="/tools-and-resources.aspx">Insights & Resources</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_4_hlItemLink_4" href="/contact-us/customer-service.aspx">Contact & Support</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c054c16391087_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_5_hlItemLink_5" href="/logins.aspx">User Logins</a>
        
    

        </li>
        
        </ul>
    
    

    
    </nav>
</aside>


<header class="adp-site-menu-2015 gray-bar adp-nav-2015">
    <nav class="clearfix nav-container">
        
        <a class="open-site-menu" href="javascript:void(0);"><img alt="" src="/ADP%20USA%202015/images/GlobalNav/icon_hamburger.png" />Site Menu</a>
        
        <ul class="corp-links">
                    
    <li class='region-selector' >
    

        <a id="_c054c16391087_ctl01_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0"><span class="flag-icon flag-icon-us"></span> United States<ul class="region-options">
    <p class="region-group">ADP Websites</p>
    <li class="region-option">
    <a href="http://www.adp-ar.com" class="region-link"><span class="flag-icon flag-icon-ar"></span> Argentina </a> </li>
    <li class="region-option">
    <a href="http://www.adppayroll.com.au/" class="region-link"><span class="flag-icon flag-icon-au"></span> Australia </a> </li>
    <li class="region-option">
    <a href="http://www.adp.com.br/" class="region-link"><span class="flag-icon flag-icon-br"></span> Brazil </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ca/en-ca/default.aspx" class="region-link"><span class="flag-icon flag-icon-ca"></span> Canada (English) </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ca/fr-ca/default.aspx" class="region-link"><span class="flag-icon flag-icon-ca"></span> Canada (French) </a> </li>
    <li class="region-option">
    <a href="http://adp.cl" class="region-link"><span class="flag-icon flag-icon-cl"></span>Chile </a> </li>
    <li class="region-option">
    <a href="http://www.adpchina.com/" class="region-link"><span class="flag-icon flag-icon-cn"></span> China </a> </li>
    <li class="region-option">
    <a href="http://www.fr.adp.com/" class="region-link"><span class="flag-icon flag-icon-fr"></span> France </a> </li>
    <li class="region-option">
    <a href="http://www.de-adp.com/" class="region-link"><span class="flag-icon flag-icon-de"></span> Germany </a> </li>
   <li class="region-option">
    <a href="http://www.adp.com.hk/" class="region-link"><span class="flag-icon flag-icon-hk"></span> Hong Kong </a> </li>       
    <li class="region-option">
    <a href="http://www.adp.in/" class="region-link"><span class="flag-icon flag-icon-in"></span> India </a> </li>
    <li class="region-option">
    <a href="http://www.it-adp.com/" class="region-link"><span class="flag-icon flag-icon-it"></span> Italy </a> </li>
    <li class="region-option">
    <a href="http://www.adp.nl/" class="region-link"><span class="flag-icon flag-icon-nl"></span> Netherlands </a> </li>
    <li class="region-option">
    <a href="http://www.adp.pe/" class="region-link"><span class="flag-icon flag-icon-pe"></span> Peru </a> </li>
     <li class="region-option">
    <a href="http://www.adp.ph/" class="region-link"><span class="flag-icon flag-icon-ph"></span> Philippines</a> </li>
    <li class="region-option">
    <a href="http://www.adp.pl/" class="region-link"><span class="flag-icon flag-icon-pl"></span> Poland </a> </li>
    <li class="region-option">
    <a href="http://www.adp.sg/" class="region-link"><span class="flag-icon flag-icon-sg"></span> Singapore </a> </li>
    <li class="region-option">
    <a href="http://www.es-adp.com/" class="region-link"><span class="flag-icon flag-icon-es"></span> Spain </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ch/" class="region-link"><span class="flag-icon flag-icon-ch"></span> Switzerland </a> </li>
    <li class="region-option">
    <a href="http://www.adp.co.uk/" class="region-link"><span class="flag-icon flag-icon-gb"></span> United Kingdom </a> </li>
    <li class="region-option">
    <a href="https://www.adp.com/" class="region-link current-selection"><span class="flag-icon flag-icon-us"></span> United States*</a> </li>
    <li>
    <p class="foot-note">*Corporate Headquarters</p>
    </li>
    <p class="region-group">ADP Worldwide</p>
    <p>We provide payroll and HR services in more than 104 countries.</p>
    <a class="btn" href="/worldwide.aspx">View All Locations</a>
</ul></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl01_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/worldwide.aspx">Worldwide Locations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl01_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/about-adp.aspx">About ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl01_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="http://investors.adp.com/">Investor Relations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl01_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="http://mediacenter.adp.com/">Media Center</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl01_ctl00_rptStickyMenu_adpMenuItem_5_hlItemLink_5" href="/careers.aspx">Careers</a>
        
    

    
    
    
    
    </li>
        
    <li class='utility-search' >
    

        <a id="_c054c16391087_ctl01_ctl00_rptStickyMenu_adpMenuItem_6_hlItemLink_6" href="/site-search.aspx">Search <span class="search-icon"></span></a>
        
    

    
    
    
    
    </li>

    
        </ul>
        <div class="global-login-search">
            <div class="global-search">    
                <input type="image" alt="Search" src="/ADP%20USA%202015/images/GlobalNav/icon_search.png" id="site-search-box">    
                <input type="text" placeholder="Search" id="site-search">
            </div>
  
            <div class="login-wrap">
                <a class="btn btn-adp-red" href="/logins.aspx" id="user-logins">User Logins</a>
    
                <!-- Unsegmented -->
                <div class="login-flyout">
                    <div id="divRecentLogins"></div>
                    
                    <ul>
                        <li class="title">Popular Logins</li>          
                                
    <li>
    

        <a id="_c054c16391087_ctl01_ctl01_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/logins/adp-portal.aspx">ADP Portal</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl01_ctl01_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/logins/my-adp.aspx">My ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl01_ctl01_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/logins/run-powered-by-adp.aspx">RUN Powered By ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl01_ctl01_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/logins/ezlabormanager.aspx">ezLaborManager</a>
        
    

    
    
    
    
    </li>

    
                    </ul>
                    <a href="/logins.aspx">View All User Logins</a>
                </div>
            </div>
        </div>
    </nav>
</header>
    

<header class="adp-nav-2015 corp-nav">
    <div class="nav-container">
        <a class="adp-logo" href="/">
<img alt="" src="/-/media/USA-2015/Global-Header/logo_ADP_red.ashx?la=en&amp;hash=62C692F1D05DA5BDB8982DC63E16E9077954C906" />
</a>
        <nav class="nav-links">
            <ul>
                        
    <li class='submenu-trigger' >
    

        <a id="_c054c16391087_ctl02_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/solutions.aspx"><span>Services & Products</span></a>
        <div id="services-products" class="flyout-menu">
<div class="segments">
<p>Solutions by Business Size</p>
<ul class="bus">
    <li>
    <a href="/solutions/small-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_SmallBusiness.ashx?la=en&amp;hash=FE476C539A79B9D58A4DA491893D102351B8AC4D" />
    <p class="name arrow-link">Small Business</p>
    <p class="range">1 &ndash; 49 employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/midsized-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_MidsizedBusiness.ashx?la=en&amp;hash=E7A0310BDF33BADC50283F20D50E4FE69D55151F" />
    <p class="name arrow-link">Midsized Business</p>
    <p class="range">50 &ndash; 999 employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/large-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_LargeBusiness.ashx?la=en&amp;hash=E6C57034D093D200757DBF55BF8978E2AA7F2EFE" />
    <p class="name arrow-link">Large Business</p>
    <p class="range">1,000+ employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/multinational-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_MultinationalBusiness.ashx?la=en&amp;hash=07FC4D8C4F3E8ADC2F3DDC96DB7E3BCDF0818095" />
    <p class="name arrow-link">Multinational Business</p>
    <p class="range">of any size</p>
    </a>
    </li>
</ul>
</div>
<div class="link-lists clearfix">
<nav class="sub-list">
<p >What We Do</p>
<ul>
    <li><a href="/solutions/services/human-capital-management.aspx">Human Capital Management</a></li>
    <li><a href="/solutions/services/payroll-services.aspx">Payroll Services</a></li>
    <li><a href="/solutions/services/talent-management.aspx">Talent Management</a></li>
    <li><a href="/solutions/services/human-resources-management.aspx">HR Management</a></li>
    <li><a href="/solutions/services/health-care-reform-management.aspx">Affordable Care Act</a></li>
    <li><a href="/solutions/services/benefits-administration.aspx">Benefits Administration</a></li>
    <li><a href="/solutions/services/time-and-attendance.aspx">Time &amp; Attendance</a></li>
    <li><a href="/solutions/services/hr-business-process-outsourcing.aspx">HR Business Process Outsourcing (HR BPO)</a></li>
    <li><a href="/solutions/services/professional-employer-organization.aspx">Professional Employer Organization (PEO)</a></li>
    <li><a href="/solutions/employer-services/retirement-services.aspx">Retirement Plans</a></li>
    <li><a href="/solutions/employer-services/insurance-services.aspx">Insurance Plans</a></li>
    <li><a href="/solutions/employer-services/tax-and-compliance.aspx">Tax &amp; Compliance</a></li>
    <li><a href="/solutions/services/payment-services.aspx">Payment Solutions</a></li>
</ul>
</nav>
<nav class="sub-list">
<p class="arrow-link"><a href="/our-products.aspx">Our Premier Products</a></p>
<ul>
<li>Explore our <a href="/our-products.aspx">full range of payroll and HR solutions</a>, and discover what's right for you.</li>
</ul>
</nav>

</div>
</div>
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl02_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/partners.aspx"><span>Partner Solutions</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl02_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/why-adp.aspx"><span>Why ADP</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl02_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/tools-and-resources.aspx"><span>Insights</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c054c16391087_ctl02_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/contact-us/customer-service.aspx"><span>Contact & Support</span></a>
        
    

    
    
    
    
    </li>

    
            </ul>
        </nav>
    </div>
</header>

 
    
<div id="ADP_651578EE-7D98-4EEB-99F7-6D7FC542F4D6" data-source-title='Privacy Thin Contact Hero'  data-source-id='5CA9C26A-AF1E-4F27-928C-D157628DA48B'    class='hero-wrap thin-hero-wrap bg-image bg-image'  style='background-image:url("/-/media/USA-2015/Thin-Heroes/bkgd_hero_privacy-terms.ashx");'  >
    <section  class='thin-hero' >
        <h1>ADP Online Privacy</h1>
        
        
    </section>
</div>
<div id="ADP_C2699CF7-50C4-4074-B013-00052C53117E" data-source-title='Additional Privacy Text'  data-source-id='9911B708-5B11-4D80-93FE-AA23E9C133FA'    class='section-wrap pad-top-40'  >
    <section  class='width970 clearfix' >
        <!-- <p>
For information on how ADP protects your personal information online, please see the <a href="/privacy/privacy-statement.aspx">ADP Online Privacy Statement</a>.
</p> -->

        
        
    </section>
</div>
<div id="ADP_9052E5A5-A9FC-4AC1-A66B-E3BA7DFD35D2"   class='section-wrap pad-btm-100' >
    <section id="sitemap-root"  class='width970 clearfix privacystatement' >
    
    </section>
</div>
    
	<!-- Footer Area -->
    

<footer class="adp-nav-2015 global-footer">
  <div class="nav-container">
    <nav class="bu-footer-nav">
      <ul>
                
    <li>
    

        <a id="_13a007e88f1da8a_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/solutions/small-business.aspx">Small Business<span>1 – 49 employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/solutions/midsized-business.aspx">Midsized Business<span>50 – 999 employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/solutions/large-business.aspx">Large Business<span>1,000+ employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/solutions/multinational-business.aspx">Multinational Business<span>of any size</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/partners.aspx">Partner Solutions<span>Accountants & more</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl00_rptStickyMenu_adpMenuItem_5_hlItemLink_5" href="/why-adp.aspx">Why ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl00_rptStickyMenu_adpMenuItem_6_hlItemLink_6" href="/tools-and-resources.aspx">Insights</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl00_rptStickyMenu_adpMenuItem_7_hlItemLink_7" href="/contact-us/customer-service.aspx">Contact & Support</a>
        
    

    
    
    
    
    </li>

    
      </ul>
    </nav>

    <nav class="corp-footer-nav clearfix">
      <div class="adp-logo">
        <a href="/">
            <img src="/-/media/USA-2015/Global-Footer/logo_ADP_white.ashx?h=29&amp;la=en&amp;w=64&amp;hash=CCF13E9152A6CB1DE432BDD08D0CEB5DA63CE56B" alt="ADP" />
        </a>
      </div>
      <ul>
                
    <li>
    

        <a id="_13a007e88f1da8a_ctl01_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/about-adp.aspx">About ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl01_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/worldwide.aspx">Worldwide Locations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl01_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="http://investors.adp.com/">Investor Relations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl01_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="http://mediacenter.adp.com/">Media Center</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_13a007e88f1da8a_ctl01_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/careers.aspx">Careers</a>
        
    

    
    
    
    
    </li>

    
      </ul>
      <div class="legal">
        <style type="text/css">
.adp-nav-2015.global-footer nav.corp-footer-nav {  width: 70%; }
</style>

<!-- <p>ADP and the ADP logo are registered trademarks of ADP, LLC. <br />Copyright © <script type="text/javascript">var d=new Date(); document.write(d.getFullYear());</script> ADP, LLC ALL RIGHTS RESERVED.</p> -->

<p>ADP, the ADP logo and ADP A more human resource are registered trademarks of ADP, LLC.  <br />All other marks are the property of their respective owners.  Copyright © <script type="text/javascript">var d=new Date(); document.write(d.getFullYear());</script> ADP, LLC.</p>

<!-- <p>Copyright © ADP, LLC ALL RIGHTS RESERVED.
<a href="/privacy.aspx">Privacy</a>
<a href="/legal.aspx">Terms</a>
<a href="/site-map.aspx">Site Map</a>
</p> -->

<p>
<a href="/privacy.aspx" style="margin-left:0;">Privacy</a>
<a href="/legal.aspx">Terms</a>
<a href="/site-map.aspx">Site Map</a>
<a href="/modern-slavery-statement.aspx">Modern Slavery Statement</a>
</p>
      </div>
    </nav>

    <div class="right-panel">
      <p class="sales"><span>Sales:</span> <span class="tel">800-225-5237</span></p>

<a href="/connect-with-adp.aspx" class="social"><span>Social@ADP</span> <img alt="" style="width: 13px; height: 13px;" src="/-/media/USA-2015/Global-Footer/SocialADP_LinkedIn.ashx?la=en&amp;hash=A74727738486FA5362680DC99A82C9E32AEAF2A0"> <img alt="" style="width: 15px; height: 10px; margin-top: -2px;" src="/-/media/USA-2015/Global-Footer/SocialADP_YouTube.ashx?la=en&amp;hash=CF3874E1EEFE2B21B594D42D7B4EC8CFA7F179DF" > <img alt="" style="width: 15px; height: 12px;" src="/-/media/USA-2015/Global-Footer/SocialADP_Twitter.ashx?la=en&amp;hash=D881BD2C45EA41752BE8B3073E3018B2572ACC36"> </a>
<a href="/logins.aspx" class="user-logins btn btn-adp-red">User Logins</a>
    </div>
  </div>
</footer>
  
    <!-- end Footer Area --> 
   
    
<!--Main Include Start-->
<!-- 3rd Party -->
<script type="text/javascript" src="/elqNow/Global/elqCfg.js"></script>

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1018590256;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1018590256/?value=0&amp;guid=ON&amp;script=0"/>
</noscript>

<!--stop unwanted window scrolling behavior-->
<script type="text/javascript">
    window.scrollTo = function () { }
</script>

<script src="//cdn.optimizely.com/js/198303326.js"></script>
<!-- <script src="//cdn.optimizely.com/js/8167740194.js"></script> -->

<!-- 1st Party -->
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "https://www.adp.com/",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.adp.com/site-search.aspx?q={search_term}",
        "query-input": "required name=search_term"
    }
}
</script>

<script src="/ADP%20USA%202015/js/common-footer-preset.min.js"></script>
<!--Main Include end, Secondary Include Start-->

<script type='text/javascript'>var settings = {
  "XmlUrl": "https://www.adp.com/privacy/intl/Intl_Privacy1.xml",
  "StartNodeXpath": "/Privacy/PrivacyStatements/English",
  "DisplayTitle": "1"
};</script><script type='text/javascript' src='/ADP%20USA%202015/js/privacystatements.js'></script>
<script type='text/javascript' src='/adp%20usa%202015/js/sitemap.js'></script>
<!--Secondary Include End-->    
    
    </form>
</body>
</html>
