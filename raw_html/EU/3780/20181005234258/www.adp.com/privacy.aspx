


<!doctype html>
<!--[if lte IE 8]>  <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]>  <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]--> 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="gdpr, general data protection regulation, bcr, binding corporate rules">
    <meta name="description" content="ADP has adopted Binding Corporate Rules (BCR) for processing Client employee data and business contact data. ADP has implemented BCR for processing personal data of ADP Associates. These BCRs serve as the basis for our Global Privacy Program. We have implemented a Global Privacy Policy that is applicable to all ADP Associates worldwide, enabling us to comply with the commitments we’ve made in our BCRs.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
<link rel="canonical" href="https://www.adp.com/privacy.aspx"/>
<meta property="og:title" content="Privacy"/>
<meta property="og:description" content="ADP has adopted Binding Corporate Rules (BCR) for processing Client employee data and business contact data. ADP has implemented BCR for processing personal data of ADP Associates. These BCRs serve as the basis for our Global Privacy Program. We have implemented a Global Privacy Policy that is applicable to all ADP Associates worldwide, enabling us to comply with the commitments we’ve made in our BCRs."/>

<title>
	Privacy
</title>
    <!-- Data Layer for GTM -->
<script>
    dataLayer = [{
        'continent': 'NA'
    }];
</script>
<!-- End Data Layer for GTM -->
    
<!--Main Include Start-->
<link rel="alternate" href="https://www.adp.com" hreflang="en-us" />
<link rel="alternate" href="http://www.adppayroll.com.au" hreflang="en-au" />
<link rel="alternate" href="http://www.adp.com.br" hreflang="pt-br" />
<link rel="alternate" href="http://www.adp.ca/en-ca/default.aspx" hreflang="en-au" />
<link rel="alternate" href="http://www.adp.ca/fr-ca/canada-home.aspx" hreflang="fr-au" />
<link rel="alternate" href="http://www.adp.cl" hreflang="es-cl" />
<link rel="alternate" href="http://www.es-adp.com" hreflang="es-es" />
<link rel="alternate" href="http://www.adpchina.com/english" hreflang="en-cn" />
<link rel="alternate" href="http://www.adpchina.com" hreflang="zh-cn" />
<link rel="alternate" href="http://www.fr.adp.com" hreflang="fr-fr" />
<link rel="alternate" href="http://www.adp.in" hreflang="en-in" />
<link rel="alternate" href="http://www.it-adp.com" hreflang="it-it" />
<link rel="alternate" href="http://www.de-adp.com" hreflang="de-de" />
<link rel="alternate" href="http://www.adp.nl" hreflang="nl-nl" />
<link rel="alternate" href="http://www.adp.pl" hreflang="pl-pl" />
<link rel="alternate" href="http://www.adp.ch" hreflang="de-se" />
<link rel="alternate" href="https://www.fr.adp.ch/" hreflang="fr-se" />
<link rel="alternate" href="https://www.en.adp.ch/" hreflang="en-se" />
<link rel="alternate" href="http://www.adp.co.uk" hreflang="en-gb" />

<link rel="stylesheet" href="/ADP%20USA%202015/css/common-header-preset.min.css" type="text/css" />

<!--[if IE]>
<script type="text/javascript" src="/ADP%20USA%202015/js/PIE.js" nobundle="nobundle"></script>
<![endif]-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MMKNHG5');</script>
<!-- End Google Tag Manager -->
<!--Main Include end, Secondary Include Start-->

<link rel='stylesheet' href='/ADP%20USA%202015/css/sitemap.css' type='text/css' />
<!--Secondary Include End-->
    <!-- GDPR Placeholder -->
    
        <!-- Start BounceX tag. Deploy at the beginning of document head. -->

        <script>
            var ScriptBounceXExists = true;
            (function (d) {
                var e = d.createElement('script');
                e.src = d.location.protocol + '//tag.bounceexchange.com/2098/i.js';
                e.async = true;
                d.getElementsByTagName("head")[0].appendChild(e);
            }(document));
        </script>

        <!-- End BounceX Tag-->
<script>(window.BOOMR_mq=window.BOOMR_mq||[]).push(["addVar",{"rua.upush":"false","rua.cpush":"false","rua.upre":"false","rua.cpre":"false","rua.uprl":"false","rua.cprl":"false","rua.cprf":"false","rua.trans":"","rua.cook":"false","rua.ims":"false"}]);</script>
<script>!function(){function o(n,i){if(n&&i)for(var r in i)i.hasOwnProperty(r)&&(void 0===n[r]?n[r]=i[r]:n[r].constructor===Object&&i[r].constructor===Object?o(n[r],i[r]):n[r]=i[r])}try{var n=decodeURIComponent("");if(n.length>0&&window.JSON&&"function"==typeof window.JSON.parse){var i=JSON.parse(n);void 0!==window.BOOMR_config?o(window.BOOMR_config,i):window.BOOMR_config=i}}catch(o){window.console&&"function"==typeof window.console.error&&console.error("mPulse: Could not parse configuration",o)}}();</script>
<script>!function(e){function a(a){if(a&&a.data&&a.data.boomr_mq)e.BOOMR_mq=e.BOOMR_mq||[],e.BOOMR_mq.push(a.data.boomr_mq)}var t="https://s.go-mpulse.net/boomerang/";if("False"=="True")e.BOOMR_config=e.BOOMR_config||{},e.BOOMR_config.PageParams=e.BOOMR_config.PageParams||{},e.BOOMR_config.PageParams.pci=!0,t="https://s2.go-mpulse.net/boomerang/";if(function(){function a(a){e.BOOMR_onload=a&&a.timeStamp||(new Date).getTime()}if(!e.BOOMR||!e.BOOMR.version&&!e.BOOMR.snippetExecuted){e.BOOMR=e.BOOMR||{},e.BOOMR.snippetExecuted=!0;var n,i,r,o=document.createElement("iframe");if(e.addEventListener)e.addEventListener("load",a,!1);else if(e.attachEvent)e.attachEvent("onload",a);o.src="javascript:void(0)",o.title="",o.role="presentation",(o.frameElement||o).style.cssText="width:0;height:0;border:0;display:none;",r=document.getElementsByTagName("script")[0],r.parentNode.insertBefore(o,r);try{i=o.contentWindow.document}catch(e){n=document.domain,o.src="javascript:var d=document.open();d.domain='"+n+"';void(0);",i=o.contentWindow.document}i.open()._l=function(){var e=this.createElement("script");if(n)this.domain=n;e.id="boomr-if-as",e.src=t+"U8C9Z-MYMDK-82UJX-NBAVA-AC4LA",BOOMR_lstart=(new Date).getTime(),this.body.appendChild(e)},i.write("<bo"+'dy onload="document._l();">'),i.close()}}(),"".length>0)if(e&&"performance"in e&&e.performance&&"function"==typeof e.performance.setResourceTimingBufferSize)e.performance.setResourceTimingBufferSize();if(function(){if(BOOMR=e.BOOMR||{},BOOMR.plugins=BOOMR.plugins||{},!BOOMR.plugins.AK){var a={i:!1,av:function(a){var t="http.initiator";if(a&&(!a[t]||"spa_hard"===a[t])){var n="false"=="true"?1:0,i="",r=void 0!==e.aFeoApplied?1:0;if(BOOMR.addVar({"ak.v":13,"ak.cp":"267330","ak.ai":"191013","ak.ol":"0","ak.cr":3,"ak.ipv":4,"ak.proto":"","ak.rid":"3d9776","ak.r":17497,"ak.a2":n,"ak.m":"x","ak.n":"essl","ak.bpcip":"207.241.231.0","ak.cport":39388,"ak.gh":"23.61.195.165","ak.quicv":"","ak.tlsv":"tls1.2","ak.0rtt":"","ak.csrc":"-","ak.acc":"reno","ak.feo":r}),""!==i)BOOMR.addVar("ak.ruds",i)}},rv:function(){var e=["ak.bpcip","ak.cport","ak.cr","ak.csrc","ak.gh","ak.ipv","ak.m","ak.n","ak.ol","ak.proto","ak.quicv","ak.tlsv","ak.0rtt","ak.r","ak.acc"];BOOMR.removeVar(e)}};BOOMR.plugins.AK={init:function(){if(!a.i){var e=BOOMR.subscribe;e("before_beacon",a.av,null,null),e("onbeacon",a.rv,null,null),a.i=!0}return this},is_complete:function(){return!0}}}}(),e.addEventListener)e.addEventListener("message",a);var n=e.navigator;if(n&&"serviceWorker"in n&&n.serviceWorker.addEventListener)n.serviceWorker.addEventListener("message",a)}(window);</script>
                    <script>var w=window;if(w.performance||w.mozPerformance||w.msPerformance||w.webkitPerformance){var d=document;AKSB=w.AKSB||{},AKSB.q=AKSB.q||[],AKSB.mark=AKSB.mark||function(e,_){AKSB.q.push(["mark",e,_||(new Date).getTime()])},AKSB.measure=AKSB.measure||function(e,_,t){AKSB.q.push(["measure",e,_,t||(new Date).getTime()])},AKSB.done=AKSB.done||function(e){AKSB.q.push(["done",e])},AKSB.mark("firstbyte",(new Date).getTime()),AKSB.prof={custid:"267330",ustr:"",originlat:"0",clientrtt:"3",ghostip:"23.61.195.165",ipv6:false,pct:"10",clientip:"207.241.231.163",requestid:"3d9776",region:"17497",protocol:"",blver:14,akM:"x",akN:"ae",akTT:"O",akTX:"1",akTI:"3d9776",ai:"191013",ra:"false",pmgn:"ADPRUM",pmgi:"",pmp:"",qc:""},function(e){var _=d.createElement("script");_.async="async",_.src=e;var t=d.getElementsByTagName("script"),t=t[t.length-1];t.parentNode.insertBefore(_,t)}(("https:"===d.location.protocol?"https:":"http:")+"//ds-aksb-a.akamaihd.net/aksb.min.js")}</script>
                    </head>
<body >

<script language='JavaScript' type='text/javascript' src='/js/s_code.js'></script>
<script language='JavaScript' type='text/javascript'>

/*You may give each page an identifying name, server, and channel on the next lines.*/

s.eVar2="English United States";
s.eVar4="Corporate";
s.eVar9="Main Category Page";
s.prop9="Main Category Page";
s.prop6="Privacy";
s.eVar3="Employer Services";
s.prop3="Employer Services";
s.prop4="Corporate";
s.eVar1="United States";
s.pageName="us|corp|es|privacy";
s.prop2="English United States";
s.server="Production";
s.prop1="United States";
s.eVar6="Privacy";
s.eVar27="19";
s.eVar29="rahdfkjxa2vvw1swqdfg5qlg";


/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/

var s_code=s.t();if(s_code)document.write(s_code)//-->

</script><script language='JavaScript' type='text/javascript'></script><noscript><a href='http://www.omniture.com' title='Web Analytics'><img src='http://adpglobalnewsite08dev.112.2O7.net/b/ss/adpglobalnewsite08dev/1/H.17--NS/0' height='1' width='1' border='0' alt=''></a></noscript>
<!-- DO NOT REMOVE -->
<!-- End SiteCatalyst code version: H.17. -->

    <form method="post" action="/privacy.aspx" id="ctl02">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="H7Jf67J0pm453kcBXHJcOULzLv4wHFmY2fFC5C7KQ2XFfDZKJ6f3hBxhggANJG2GLjqgG5kzHOeW/0y90Ov+rwePRHuxuqATzKUcU3+9CsFftizIU46xn1rO4jxnE5PjRtMso7PlBLfhA3VuaTMI/Z2ItzDpvBX4HuSTgLqf1nh/3ITQ+l5Ch/ymaD+1xqVVGvopSV8VNaCuQ6xXRft9JLbBFYhN9ggS/z6Xmc3s/P6+iGlm7Mdd9GTO01P7odGBJA1Z/MjK1HNCnWeMnigTuuw+0yQ/GIWyFmWnQFujEiGu1XXG7nX+0W5RdfLCAidIU2Gpay1vbKPeeJsI9PsRfenXoF6crJdYus6RFOFdZmRlN16Q5KjKJroK/4j8tmkSEu9iVTIHtXM8VU4SaihZLj2OEly9wX0ORkDb7G35ElIWgRXk4EPs+hm46BQRMcEyS64+BA8owe/R9Jx7+iaXXfjQjq/7yYsycaZDWIhWnlJKZyjj7xQMSdRJrecFTY1XEZ3zZ0bS+2CTEK3Mfo3c8iELJqPog12LoIsoGQInXhcsivXzW5pkjIAqW7gDTUA9yvrCtfcggk5vVu7XcpKrx5pPcg8GZt9ftW6fD8ZINuG+/BUzPtlMe9Cy9Nu9J7218mfC9Fz5rSF8NEiIyx8qPUY9J2M6rOJQd4gjplwY2+BvFw0VH/TZP8Fbzf4yWFbenYKt8lVp5TLRDKpbH1Iph3hO8GYeINzpDCe/gZPik4uMrdwF2kkurf+TzMzjTm5VsWbfqY7i2Q8Nrrk9oByGF+k3IqJenagl/uxhMi7CNL2hrbJseKvZdv+k1h33ppNUs4przcemQAaLgJu7XgcgJU4newfTnZEKtQ9mJNy46Z2PSFMyXGN4OVBoSwvIjCKSZU/uGa8uZgYicb7fsSY1VkUKoG37LffOq5h0+7YZWdCLplelL8rgIV2KlQv3DesVY2wVb9w44THYKvfbT4P80RP0wGybmLoG9dsAeYZvM915H4aYuQgvgRi0rGGXTAy4bQrcwmBw8TxCd/B9kKL1anUtoot5J0HuP9FhpXXIN+Af9dC+oarjnSQ1iToPIghtfV+7TKlw68T/dkGjp3E6Nsum0/t0MV55ssXFmc3YvmHuVOlu3OY32Sht2K/SBj+5IOy3xXaGcfssZUC/OA9RECsHIOmYxqo5cOA0CVowqq6yixFKpo0jzduPYUewST7kgtY3dpZVb9fa7fj8iYt998ggn8tOI91SlZOyVmSSa6k45DBN4J5xoOoscDSWUSlBsEcb155+W90fBbkOC6tVrdWdCXm/8FFOCITJLSuAqbDPUc7d9h6v3+8Rh0Q4PPkf/dFvOHqK48zLy0/M7b2ARaOjSUDMDK2CV+I/yy80rKxbivrPP9+O1wYKTmu62laXn/CqRaMs6LIzIT4C7qb4m+WRSpLm500XL98k2U6Zaq+QdsI31+Qm35tbQmtuWdXCfV/tDosr/PQuLAXqbVhIeKw49i67nHCqP0SUxTuvQVD3bUO5bVJm+KDP3LW14m2a8qr7qlB/KfLpU0iQzOW4ZEPpyqDN1MpnWSlm1tGyjFaQBmwLmjKXPCsxqGgpv2oPPJ5eryxNSE1CZLecjUrxR3q1YvxFehE6IfA3xE0O9V2EiDlDFVXpXtvPJEh4ILCwA6kG7fcRbULf8LRHaTk6t4y02+ww8iHfc/GHVKDzjp6vjulhfcHEhkCnejpat/Spi565u/5LCzN32EQcvzQqQ/Nkz3vNNXQ2mhYeEA8yIYTEtH7EQgnxAPMxtMr4D/+36ujqxHIEEVTTbcBvVtpS06MhbYiej9l4ojIuMal8Op4CBwaRCnmwquxTUn5lKDGnLPcv94w9Ia9PoQkm92JmN45c8azm6mNfONvPWd8ttMANCWCTqI1b7ri1+dduvkRl8rFoTKGLVOpjwEBo0QI4ysK9t4imoMBpzxz/o5DtLaa4ureUPit/eivF0wcSXMiQM+N7jjesO9c4ypUcVRZfBmfPGof7ewSde08N4BEohcdc3sxBkniKDDSpbejD9xsQCAc46vC9kZO0aicsOZ7rUvl8EFvZI9bq6SF9QahpNH6abyjuM1++/CX2WoSWDQeZjKgJEcxz/DQu0n6l3LFDa3cVjS0ddD9OsQJWhooe7OKCXg1lyr9smqlg/7C23KC8IT3s1LA5wUFeyHuuVqZmw0unqIpgO76dwh22K9u671j825fSGBQmS1JQ0JBA309pjFhb/h3gKpBXwEUSwrVF5EBbKaRGZMa3V9Eht0J2dpCko0eH6cZG9lf8t8dVwQMhT5+fnIQBIip6GhroQDCKrzAeoRWYEQD3k1I7QCwqN1fuxWd0I5/nICca+6p2oEIShJUQ9Ps7crCZW9PdHhWDBG0C+rIO3AdaDswIo3/Bjunmc3t5sZrU1J/DlmjfhxsRqEL3TSKcFhS+Fu1vTY60Miv+v9Ym3ucOBq7TZyE+7qTAng5spXpIAJQYHzipLeRWZHUK1m1mwkuzVkgqmMXH7yZQZoUnXEODqTSkynw/vPtcx8Ltj7HAH2nrWqSSJJPBNoPguBAfT+/jTLYxkbsk8tfvC+zn7jsGd5cF8H9GGUKm4ZoFRVFXGHQRQrOL7qlIeaHnC4/idBmE/hAYH84GYGZfOH8EZGwxXYuJXz9d1nhX3MG+aU420Q3Q/EBm5yTxWlc+BJIf+/EsfXy1GIoNbWy6Ze5t1PVubltSfmPen3lzRs1nZyhAcn7ll1xdn1PLkWPncUdnWLdiTbpZnPdThx9GZqpCxhEDz3egOIZpe69aIC+dHUcZ4GTTOCDuLW+vbemzGmedqRbcdj90NwHVhPo6jtqnsd3rnUsdo3rctiuk4u5D25LPh1nHzLkVHW3B3wysdHD6uAsTc4QvxpXkuChiRCJm9g3G4G9LB0Hay5fpnk3W3DdYA72tdkKQAIhpVMsvIlqzpwy09bV8A8rOzcuKmqLmPWDzpapRg7RmT69/dlHRukvlMFDRUpVEeSguZBi8ADtRuke8GwlZg/niAmDIZaHv3FlsPFL73S43hfwopcTX2CYtQhJOLaYUmNG+G7uyhxlC/NJJgzCNiZc8AJx++mAfXEjFQ16+M5qIHd9r0UdYUUVgSjf9VqgvMeFrhUKUb5LG0SYlMQhjj6RtlCy6fvxM+bPCMcxUugEMsnZ2ZkIB+s71sm5O30fnpt5TmO/yx+FY9t8Fqe1kvO2aQ3L4Sx+X72pWfNGiVmu/aM73lXpXcerFrhw33NtKIeXNmh8atGjRaL/Q8t25zoua2gzyPZL7/nvawM5Zo4rh3VcRqb3MA8biQIJBV71ciPUTdbRPA8orflMUpu+aCbnL0KmCvtHbUtaABI1IkaQYs13T+J2cosXvQTfAnav+gm0M5JIApqF/hvlGsoOWs2GHyDKaZtyA1Pk3igMLgDa5qSnu5IE0ptSGCHueiW3do/uCeL/3e6Y1f9kfYPd912w/rgthQpDkLsQmP9PMW2TjL8CGiqww/FO83HW50lKUbk3SgFyN0+9kOljAreVfnwU6glVZA7D/1foJjebw2gDIkZtdxLtPFoc68VHNeYm+zoFU2/w+lzdSA64zdcFJlOor3WM9z4t+/yolWoiMhjDlzMi1reTufTuD9PC0X+fwFJpvl4LPawX0jVTSY3qRwZKx9R8CNOhyDcIiAZ8WB3vgmR0IxVmX8oh7b3RlSyKq+z2baXEvpvOvfFCCLuZ86LpSvQ/C99O7OWFxStN89j63Drpk/65naVuAvO8zUpIYiHsE/bqZA4JmQGXJJxK+TaLF2xtgxd5nWHZvxjwtA+VUz7pOD1SuTY0EPTK5OczRalOlYi+8wjQ9aO70kIeMZAGAGAz/8v1pEH3JI65hgjhyOMk5Paf0yPQWH13SfEavpxNBc/yERImFilvkiA0bXtIIs/PMLc00TKUU4TSou4YEBXTNQjositxgPPsVnrQ4bFqNtIqVgykps9LTzEo+Z+mMfe+zV01aYkCo2nm3h/yjht3imd00D9E5dMdvXvFsBQF8kiAWH7pZByHPVe0HYG0dVTuugZIh497ZGM49ixbN7RqmT0weqGBwEtik5YyOHJWiJOqMui7Hkf7AOGx8R9EeFdOm9s53Pmn4ZqdRg2/pC0LncrDf/9bDS6sAcsPAJxHlXV0DsZ1kJ0dq6p8mkf5395kfagyvA4uuePIYgVwqnjIlSj5TWank/s8Zk7wpU59bMWY189e57OWwaGUOr54TUGdPWG88VtLr5ojGnNedqbQdxU/JHnpLcQ4DVsQIfBzNv7mwgxOa+kyBMZKh/Sqbz3/Y+Hc+1iZOd5PrsJalg3uXmIkh/IAUyx/ofj5lcxriNz3J32dexgFKRtixBvlBSTKnISHIq+v1SQ7t1ticaC91bl2aoZySP5CVo3TVk/L28RcbUTvYWDTcwT6K5rVotNYmXPmkXO57Ps7kbxZes9pDhCt192C0ELWz8YGKjoT7qML+JL97dEXnXEnN0kGRirXMgSr1GNUeSxveknMhlVz3LxDAB8Q3hyUOCJL1zaGzINv0HdInnii3CNmmlC+yi8pGYRHpGuZB30b3Ge/zieOjWKkJczLrVCe8Px8hDHfziw9EX8CBNw9+g/PrKQTfGC/2b/LipJcVt3zcB/lV60mz31OEPXWJcAM69oDq3YI290KdDQBxjC6KxlkY9QMfHvscSH7xwLvCS8jhhgMZjBfBjFVB2YK6f2+qQiWvsW1vMO4MEZ97CT4YBzV13XiGB+9H2sgMfdwEBDGFEmCVIy2ccrnwXBgWeiL2eYZJ2y4NpEIOCKgUqGZyC7LYZdtJoLaxiLOsNV1Btux4iCduCAWOJlvOfewAxmiUdBsqpCf2JAv0S38Z0dAlEguQC6oNU3+v7Vr30vXYCTg3iJ8Z8SFKHX1E1OjYgaOwCPJ5JYjvYXOJo2p8mtPCJ6oSMu/E/5F0RvSU0tLajpc7//1+SRgj/qqpXHNnOEZJW9xdC9y2ap0lZb+clIsEm2yWC7+YU2v5nZ0yUXf4Mgcln0mK886po0gppFmePP3bdOHE8U4galXR4o/8aZZWKAZqrcyvYj2UNW/PzrU4HzY3lEOHW69DubdKsbFMvMRs8ZdXYIX7OjPDDqIm14NmTczjiwocCaqMAHoVboqas2qCgUIc3TnhI1QkxFbVDkqWGw1IOos8CgZMeoQDxX1SWQU9tr3XSesspfb3iV8FacGtv/b2USjKGa2BNtU1RmE2XpBikFLh8r+tT/1ziXiDwrteMWuIUV+ezt8t22lXuou6FJBTqtAxgW3RFuDrORXGm0JOP0fnbmW8U+d1EZ3ff7x3WybLMIuVZRFqDwcA3V6IjJYv39bzKEYDyrc7rxZURy6sINeMAU01v9EhZHbUek/F2RX7mY1/MMC5tfTQWh9Fg1e02dZ2m4qcP6uVxawQn+oQHOlxs8C2rcRMQkfjbKW5CkSYFcYotuY=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['ctl02'];
if (!theForm) {
    theForm = document.ctl02;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>



<script src="/ScriptResource.axd?d=mkHh3H7FZVas5l3AY3JDjCg9qnZ6Lb2wLV1_rWuEpvfvI6N4qrkOmEYq3hTdpPoCSuUzVMnmNchfaIeSBYNNoviW0uarjeloz4pIJf5mircEvCJMsOI5UUcr7AA1im6aeltFYcyDISqK1tAaMG6J4feLMZ7vWOUm24QyiaD0xbgdIhWSUyA2BPDPY4SMr4arWD0xqqB8KTbjdqWP3adi9gnthkSJe1_HAV9wcja_EvKFX-Gs0" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B9C9B541" />
</div>
       
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager1', 'ctl02', [], [], [], 90, '');
//]]>
</script>
 
   
    
<aside class="adp-site-menu-2015 side-bar adp-nav-2015">
    <a class="close-site-menu" href="javascript:void(0);">
        <img src="/ADP%20USA%202015/images/GlobalNav/close_x.png" />
    </a>
    <nav>
                
    
    
    
    
    <ul class='main-link bu-nav'>
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_0_hlItemLink_0" href="/solutions/small-business.aspx">Small Business<span>1 – 49 employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_1_hlItemLink_1" href="/solutions/midsized-business.aspx">Midsized Business<span>50 – 999 employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_2_hlItemLink_2" href="/solutions/large-business.aspx">Large Business<span>1,000+ employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_3_hlItemLink_3" href="/solutions/multinational-business.aspx">Multinational Business<span>of any size</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_4_hlItemLink_4" href="/partners.aspx">Partner Solutions<span>Accountants, advisors, and more</span></a>
        
    

        </li>
        
        </ul>
    
    
        
    
    
    
    
    <ul class='corp-nav'>
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_0_hlItemLink_0" href="/">Home</a>
        
    

        </li>
        
        
        <li class='NoHighlight' >
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_1_hlItemLink_1" href="/solutions.aspx">Services & Products</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_2_hlItemLink_2" href="/why-adp.aspx">Why ADP</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_3_hlItemLink_3" href="/tools-and-resources.aspx">Insights & Resources</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_4_hlItemLink_4" href="/contact-us/customer-service.aspx">Contact & Support</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_c15e591130c283_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_5_hlItemLink_5" href="/logins.aspx">User Logins</a>
        
    

        </li>
        
        </ul>
    
    

    
    </nav>
</aside>


<header class="adp-site-menu-2015 gray-bar adp-nav-2015">
    <nav class="clearfix nav-container">
        
        <a class="open-site-menu" href="javascript:void(0);"><img alt="" src="/ADP%20USA%202015/images/GlobalNav/icon_hamburger.png" />Site Menu</a>
        
        <ul class="corp-links">
                    
    <li class='region-selector' >
    

        <a id="_c15e591130c283_ctl01_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0"><span class="flag-icon flag-icon-us"></span> United States<ul class="region-options">
    <p class="region-group">ADP Websites</p>
    <li class="region-option">
    <a href="http://www.adp-ar.com" class="region-link"><span class="flag-icon flag-icon-ar"></span> Argentina </a> </li>
    <li class="region-option">
    <a href="http://www.adppayroll.com.au/" class="region-link"><span class="flag-icon flag-icon-au"></span> Australia </a> </li>
    <li class="region-option">
    <a href="http://www.adp.com.br/" class="region-link"><span class="flag-icon flag-icon-br"></span> Brazil </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ca/en-ca/default.aspx" class="region-link"><span class="flag-icon flag-icon-ca"></span> Canada (English) </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ca/fr-ca/default.aspx" class="region-link"><span class="flag-icon flag-icon-ca"></span> Canada (French) </a> </li>
    <li class="region-option">
    <a href="http://adp.cl" class="region-link"><span class="flag-icon flag-icon-cl"></span>Chile </a> </li>
    <li class="region-option">
    <a href="http://www.adpchina.com/" class="region-link"><span class="flag-icon flag-icon-cn"></span> China </a> </li>
    <li class="region-option">
    <a href="http://www.fr.adp.com/" class="region-link"><span class="flag-icon flag-icon-fr"></span> France </a> </li>
    <li class="region-option">
    <a href="http://www.de-adp.com/" class="region-link"><span class="flag-icon flag-icon-de"></span> Germany </a> </li>
   <li class="region-option">
    <a href="http://www.adp.com.hk/" class="region-link"><span class="flag-icon flag-icon-hk"></span> Hong Kong </a> </li>       
    <li class="region-option">
    <a href="http://www.adp.in/" class="region-link"><span class="flag-icon flag-icon-in"></span> India </a> </li>
    <li class="region-option">
    <a href="http://www.it-adp.com/" class="region-link"><span class="flag-icon flag-icon-it"></span> Italy </a> </li>
    <li class="region-option">
    <a href="http://www.adp.nl/" class="region-link"><span class="flag-icon flag-icon-nl"></span> Netherlands </a> </li>
    <li class="region-option">
    <a href="http://www.adp.pe/" class="region-link"><span class="flag-icon flag-icon-pe"></span> Peru </a> </li>
     <li class="region-option">
    <a href="http://www.adp.ph/" class="region-link"><span class="flag-icon flag-icon-ph"></span> Philippines</a> </li>
    <li class="region-option">
    <a href="http://www.adp.pl/" class="region-link"><span class="flag-icon flag-icon-pl"></span> Poland </a> </li>
    <li class="region-option">
    <a href="http://www.adp.sg/" class="region-link"><span class="flag-icon flag-icon-sg"></span> Singapore </a> </li>
    <li class="region-option">
    <a href="http://www.es-adp.com/" class="region-link"><span class="flag-icon flag-icon-es"></span> Spain </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ch/" class="region-link"><span class="flag-icon flag-icon-ch"></span> Switzerland </a> </li>
    <li class="region-option">
    <a href="http://www.adp.co.uk/" class="region-link"><span class="flag-icon flag-icon-gb"></span> United Kingdom </a> </li>
    <li class="region-option">
    <a href="https://www.adp.com/" class="region-link current-selection"><span class="flag-icon flag-icon-us"></span> United States*</a> </li>
    <li>
    <p class="foot-note">*Corporate Headquarters</p>
    </li>
    <p class="region-group">ADP Worldwide</p>
    <p>We provide payroll and HR services in 113 countries.</p>
    <a class="btn" href="/worldwide.aspx">View All Locations</a>
</ul></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/worldwide.aspx">Worldwide Locations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/about-adp.aspx">About ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="http://investors.adp.com/">Investor Relations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="http://mediacenter.adp.com/">Media Center</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl00_rptStickyMenu_adpMenuItem_5_hlItemLink_5" href="/careers.aspx">Careers</a>
        
    

    
    
    
    
    </li>
        
    <li class='utility-search' >
    

        <a id="_c15e591130c283_ctl01_ctl00_rptStickyMenu_adpMenuItem_6_hlItemLink_6" href="/site-search.aspx">Search <span class="search-icon"></span></a>
        
    

    
    
    
    
    </li>

    
        </ul>
        <div class="global-login-search">
            <div class="global-search">    
                <input type="image" alt="Search" src="/ADP%20USA%202015/images/GlobalNav/icon_search.png" id="site-search-box">    
                <input type="text" placeholder="Search" id="site-search">
            </div>
  
            <div class="login-wrap">
                <a class="btn btn-adp-red" href="/logins.aspx" id="user-logins">User Logins</a>
    
                <!-- Unsegmented -->
                <div class="login-flyout">
                    <div id="divRecentLogins"></div>
                    
                    <ul>
                        <li class="title">Popular Logins</li>          
                                
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl01_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/logins/adp-portal.aspx">ADP Portal</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl01_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/logins/my-adp.aspx">My ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl01_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/logins/run-powered-by-adp.aspx">RUN Powered By ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl01_ctl01_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/logins/ezlabormanager.aspx">ezLaborManager</a>
        
    

    
    
    
    
    </li>

    
                    </ul>
                    <a href="/logins.aspx">View All User Logins</a>
                </div>
            </div>
        </div>
    </nav>
</header>
    

<header class="adp-nav-2015 corp-nav">
    <div class="nav-container">
        <a class="adp-logo" href="/">
<img alt="" src="/-/media/USA-2015/Global-Header/logo_ADP_red.ashx?la=en&amp;hash=62C692F1D05DA5BDB8982DC63E16E9077954C906" />
</a>
        <nav class="nav-links">
            <ul>
                        
    <li class='submenu-trigger' >
    

        <a id="_c15e591130c283_ctl02_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/solutions.aspx"><span>Services & Products</span></a>
        <div id="services-products" class="flyout-menu">
<div class="segments">
<p>Solutions by Business Size</p>
<ul class="bus">
    <li>
    <a href="/solutions/small-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_SmallBusiness.ashx?la=en&amp;hash=FE476C539A79B9D58A4DA491893D102351B8AC4D" />
    <p class="name arrow-link">Small Business</p>
    <p class="range">1 &ndash; 49 employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/midsized-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_MidsizedBusiness.ashx?la=en&amp;hash=E7A0310BDF33BADC50283F20D50E4FE69D55151F" />
    <p class="name arrow-link">Midsized Business</p>
    <p class="range">50 &ndash; 999 employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/large-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_LargeBusiness.ashx?la=en&amp;hash=E6C57034D093D200757DBF55BF8978E2AA7F2EFE" />
    <p class="name arrow-link">Large Business</p>
    <p class="range">1,000+ employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/multinational-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_MultinationalBusiness.ashx?la=en&amp;hash=07FC4D8C4F3E8ADC2F3DDC96DB7E3BCDF0818095" />
    <p class="name arrow-link">Multinational Business</p>
    <p class="range">of any size</p>
    </a>
    </li>
</ul>
</div>
<div class="link-lists clearfix">
<nav class="sub-list">
<p >What We Do</p>
<ul>
    <li><a href="/solutions/services/human-capital-management.aspx">Human Capital Management</a></li>
    <li><a href="/solutions/services/payroll-services.aspx">Payroll Services</a></li>
    <li><a href="/solutions/services/talent-management.aspx">Talent Management</a></li>
    <li><a href="/solutions/services/human-resources-management.aspx">HR Management</a></li>
    <li><a href="/solutions/services/health-care-reform-management.aspx">Affordable Care Act</a></li>
    <li><a href="/solutions/services/benefits-administration.aspx">Benefits Administration</a></li>
    <li><a href="/solutions/services/time-and-attendance.aspx">Time &amp; Attendance</a></li>
    <li><a href="/solutions/services/hr-business-process-outsourcing.aspx">HR Business Process Outsourcing (HR BPO)</a></li>
    <li><a href="/solutions/services/professional-employer-organization.aspx">Professional Employer Organization (PEO)</a></li>
    <li><a href="/solutions/employer-services/retirement-services.aspx">Retirement Plans</a></li>
    <li><a href="/solutions/employer-services/insurance-services.aspx">Insurance Plans</a></li>
    <li><a href="/solutions/employer-services/tax-and-compliance.aspx">Tax &amp; Compliance</a></li>
    <li><a href="/solutions/services/payment-services.aspx">Payment Solutions</a></li>
</ul>
</nav>
<nav class="sub-list">
<p class="arrow-link"><a href="/our-products.aspx">Our Premier Products</a></p>
<ul>
<li>Explore our <a href="/our-products.aspx">full range of payroll and HR solutions</a>, and discover what's right for you.</li>
</ul>
</nav>

</div>
</div>
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl02_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/partners.aspx"><span>Partner Solutions</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl02_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/why-adp.aspx"><span>Why ADP</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl02_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/tools-and-resources.aspx"><span>Insights</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_c15e591130c283_ctl02_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/contact-us/customer-service.aspx"><span>Contact & Support</span></a>
        
    

    
    
    
    
    </li>

    
            </ul>
        </nav>
    </div>
</header>

 
    
<div id="ADP_651578EE-7D98-4EEB-99F7-6D7FC542F4D6" data-source-title='Privacy Thin Contact Hero'  data-source-id='5CA9C26A-AF1E-4F27-928C-D157628DA48B'    class='hero-wrap thin-hero-wrap bg-image bg-image'  style='background-image:url("/-/media/USA-2015/Thin-Heroes/bkgd_hero_privacy-terms.ashx");'  >
    <section  class='thin-hero' >
        <h1>Privacy at ADP</h1>
        
        
    </section>
</div>
<div id="ADP_9052E5A5-A9FC-4AC1-A66B-E3BA7DFD35D2"   class='section-wrap pad-btm-100' >
    <section id="sitemap-root"  class='width970 clearfix privacystatement' >
    
    </section>
</div>
    
	<!-- Footer Area -->
    

<footer class="adp-nav-2015 global-footer">
  <div class="nav-container">
    <nav class="bu-footer-nav">
      <ul>
                
    <li>
    

        <a id="_afb6769e451ce6_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/solutions/small-business.aspx">Small Business<span>1 – 49 employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/solutions/midsized-business.aspx">Midsized Business<span>50 – 999 employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/solutions/large-business.aspx">Large Business<span>1,000+ employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/solutions/multinational-business.aspx">Multinational Business<span>of any size</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/partners.aspx">Partner Solutions<span>Accountants & more</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl00_rptStickyMenu_adpMenuItem_5_hlItemLink_5" href="/why-adp.aspx">Why ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl00_rptStickyMenu_adpMenuItem_6_hlItemLink_6" href="/tools-and-resources.aspx">Insights</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl00_rptStickyMenu_adpMenuItem_7_hlItemLink_7" href="/contact-us/customer-service.aspx">Contact & Support</a>
        
    

    
    
    
    
    </li>

    
      </ul>
    </nav>

    <nav class="corp-footer-nav clearfix">
      <div class="adp-logo">
        <a href="/">
            <img src="/-/media/USA-2015/Global-Footer/logo_ADP_white.ashx?h=29&amp;la=en&amp;w=64&amp;hash=CCF13E9152A6CB1DE432BDD08D0CEB5DA63CE56B" alt="ADP" />
        </a>
      </div>
      <ul>
                
    <li>
    

        <a id="_afb6769e451ce6_ctl01_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/about-adp.aspx">About ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl01_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/worldwide.aspx">Worldwide Locations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl01_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="http://investors.adp.com/">Investor Relations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl01_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="http://mediacenter.adp.com/">Media Center</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_afb6769e451ce6_ctl01_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/careers.aspx">Careers</a>
        
    

    
    
    
    
    </li>

    
      </ul>
      <div class="legal">
        <style type="text/css">
.adp-nav-2015.global-footer nav.corp-footer-nav {  width: 70%; }
</style>

<!-- <p>ADP and the ADP logo are registered trademarks of ADP, LLC. <br />Copyright © <script type="text/javascript">var d=new Date(); document.write(d.getFullYear());</script> ADP, LLC ALL RIGHTS RESERVED.</p> -->

<p>ADP, the ADP logo and ADP A more human resource are registered trademarks of ADP, LLC.  <br />All other marks are the property of their respective owners.  Copyright © <script type="text/javascript">var d=new Date(); document.write(d.getFullYear());</script> ADP, LLC.</p>

<!-- <p>Copyright © ADP, LLC ALL RIGHTS RESERVED.
<a href="/privacy.aspx">Privacy</a>
<a href="/legal.aspx">Terms</a>
<a href="/site-map.aspx">Site Map</a>
</p> -->

<p>
<a href="/privacy.aspx" style="margin-left:0;">Privacy</a>
<a href="/legal.aspx">Terms</a>
<a href="/site-map.aspx">Site Map</a>
<a href="/modern-slavery-statement.aspx">Modern Slavery Statement</a>
</p>
      </div>
    </nav>

    <div class="right-panel">
      <p class="sales"><span>Sales:</span> <span class="tel">800-225-5237</span></p>

<a href="/connect-with-adp.aspx" class="social"><span>Social@ADP</span> <img alt="" style="width: 13px; height: 13px;" src="/-/media/USA-2015/Global-Footer/SocialADP_LinkedIn.ashx?la=en&amp;hash=A74727738486FA5362680DC99A82C9E32AEAF2A0"> <img alt="" style="width: 15px; height: 10px; margin-top: -2px;" src="/-/media/USA-2015/Global-Footer/SocialADP_YouTube.ashx?la=en&amp;hash=CF3874E1EEFE2B21B594D42D7B4EC8CFA7F179DF" > <img alt="" style="width: 15px; height: 12px;" src="/-/media/USA-2015/Global-Footer/SocialADP_Twitter.ashx?la=en&amp;hash=D881BD2C45EA41752BE8B3073E3018B2572ACC36"> </a>
<a href="/logins.aspx" class="user-logins btn btn-adp-red">User Logins</a>
    </div>
  </div>
</footer>
  
    <!-- end Footer Area --> 

                <script type="text/javascript" src="/elqNow/Global/elqCfg.js"></script>            <!-- Google Code for Remarketing Tag -->            <script type="text/javascript">                /* <![CDATA[ */                var google_conversion_id = 1018590256;                var google_custom_params = window.google_tag_params;                var google_remarketing_only = true;                /* ]]> */            </script>            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>            <noscript>            <img height ="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1018590256/?value=0&amp;guid=ON&amp;script=0"/>            </noscript>            <script src="//cdn.optimizely.com/js/198303326.js" ></script> 
    
<!--Main Include Start-->
<!--stop unwanted window scrolling behavior-->
<script type="text/javascript"></script>
<script type="text/javascript">window.scrollTo = function () { return true; }</script>

<!-- 1st Party -->
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "https://www.adp.com/",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.adp.com/site-search.aspx?q={search_term}",
        "query-input": "required name=search_term"
    }
}
</script>

<script src="/ADP%20USA%202015/js/common-footer-preset.min.js"></script>

<!--Main Include end, Secondary Include Start-->

<script type='text/javascript'>var settings = {
  "XmlUrl": "https://www.adp.com/privacy/intl/Intl_Privacy.xml",
  "StartNodeXpath": "/Privacy/PrivacyStatements/English",
  "DisplayTitle": "1"
};</script><script type='text/javascript' src='/ADP%20USA%202015/js/privacystatements.js'></script>
<script type='text/javascript' src='/adp%20usa%202015/js/sitemap.js'></script>
<!--Secondary Include End-->    


    </form>
</body>
</html>
