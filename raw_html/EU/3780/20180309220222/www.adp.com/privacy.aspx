


<!doctype html>
<!--[if lte IE 8]>  <html class="no-js ie8" lang="en"> <![endif]-->
<!--[if IE 9]>  <html class="no-js ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]--> 
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="googlebot" content="index, follow">
<link rel="canonical" href="https://www.adp.com/privacy.aspx"/>
<meta property="og:title" content="Privacy"/>

<title>
	Privacy
</title>
    <!-- Data Layer for GTM -->
<script>
    dataLayer = [{
        'ipAddress': '207.241.225.53'
    }];
</script>
<!-- End Data Layer for GTM -->
    
<!--Main Include Start-->
<link rel="alternate" href="https://www.adp.com" hreflang="en-us" />
<link rel="alternate" href="http://www.adppayroll.com.au" hreflang="en-au" />
<link rel="alternate" href="http://www.adp.com.br" hreflang="pt-br" />
<link rel="alternate" href="http://www.adp.ca/en-ca/default.aspx" hreflang="en-au" />
<link rel="alternate" href="http://www.adp.ca/fr-ca/canada-home.aspx" hreflang="fr-au" />
<link rel="alternate" href="http://www.adp.cl" hreflang="es-cl" />
<link rel="alternate" href="http://www.es-adp.com" hreflang="es-es" />
<link rel="alternate" href="http://www.adpchina.com/english" hreflang="en-cn" />
<link rel="alternate" href="http://www.adpchina.com" hreflang="zh-cn" />
<link rel="alternate" href="http://www.fr.adp.com" hreflang="fr-fr" />
<link rel="alternate" href="http://www.adp.in" hreflang="en-in" />
<link rel="alternate" href="http://www.it-adp.com" hreflang="it-it" />
<link rel="alternate" href="http://www.de-adp.com" hreflang="de-de" />
<link rel="alternate" href="http://www.adp.nl" hreflang="nl-nl" />
<link rel="alternate" href="http://www.adp.pl" hreflang="pl-pl" />
<link rel="alternate" href="http://www.adp.ch" hreflang="de-se" />
<link rel="alternate" href="https://www.fr.adp.ch/" hreflang="fr-se" />
<link rel="alternate" href="https://www.en.adp.ch/" hreflang="en-se" />
<link rel="alternate" href="http://www.adp.co.uk" hreflang="en-gb" />

<link rel="stylesheet" href="/ADP%20USA%202015/css/common-header-preset.min.css" type="text/css" />

<!--[if IE]>
<script type="text/javascript" src="/ADP%20USA%202015/js/PIE.js" nobundle="nobundle"></script>
<![endif]-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KH3TMH');</script>
<!-- End Google Tag Manager -->
<!-- Start BounceX tag. Deploy at the beginning of document head. -->

        <script>
     var ScriptBounceXExists=true;
        (function(d) {
            var e = d.createElement('script');
            e.src = d.location.protocol + '//tag.bounceexchange.com/2098/i.js';
            e.async = true;
            d.getElementsByTagName("head")[0].appendChild(e);
        }(document));
        </script>

<!-- End BounceX Tag-->
<!--Main Include end, Secondary Include Start-->

<link rel='stylesheet' href='/ADP%20USA%202015/css/sitemap.css' type='text/css' />
<!--Secondary Include End-->
                    <script>var w=window;if(w.performance||w.mozPerformance||w.msPerformance||w.webkitPerformance){var d=document;AKSB=w.AKSB||{},AKSB.q=AKSB.q||[],AKSB.mark=AKSB.mark||function(e,_){AKSB.q.push(["mark",e,_||(new Date).getTime()])},AKSB.measure=AKSB.measure||function(e,_,t){AKSB.q.push(["measure",e,_,t||(new Date).getTime()])},AKSB.done=AKSB.done||function(e){AKSB.q.push(["done",e])},AKSB.mark("firstbyte",(new Date).getTime()),AKSB.prof={custid:"267330",ustr:"cookiepresent",originlat:"0",clientrtt:"4",ghostip:"23.61.195.174",ipv6:false,pct:"10",clientip:"207.241.225.53",requestid:"cd4f79",region:"17497",protocol:"",blver:13,akM:"x",akN:"ae",akTT:"O",akTX:"1",akTI:"cd4f79",ai:"191013",ra:"true",pmgn:"ADPRUM",pmgi:"",pmp:"",qc:""},function(e){var _=d.createElement("script");_.async="async",_.src=e;var t=d.getElementsByTagName("script"),t=t[t.length-1];t.parentNode.insertBefore(_,t)}(("https:"===d.location.protocol?"https:":"http:")+"//ds-aksb-a.akamaihd.net/aksb.min.js")}</script>
                    </head>
<body >
    <script language='JavaScript' type='text/javascript' src='/js/s_code.js'></script>
<script language='JavaScript' type='text/javascript'>

/*You may give each page an identifying name, server, and channel on the next lines.*/

s.eVar2="English United States";
s.eVar4="Corporate";
s.eVar9="Main Category Page";
s.prop9="Main Category Page";
s.prop6="Privacy";
s.eVar3="Employer Services";
s.prop3="Employer Services";
s.prop4="Corporate";
s.eVar1="United States";
s.pageName="us|corp|es|privacy";
s.prop2="English United States";
s.server="Production";
s.prop1="United States";
s.eVar6="Privacy";
s.eVar27="17";
s.eVar29="hl0bp50zyvp0dys41uyntckh";


/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/

var s_code=s.t();if(s_code)document.write(s_code)//-->

</script><script language='JavaScript' type='text/javascript'></script><noscript><a href='http://www.omniture.com' title='Web Analytics'><img src='http://adpglobalnewsite08dev.112.2O7.net/b/ss/adpglobalnewsite08dev/1/H.17--NS/0' height='1' width='1' border='0' alt=''></a></noscript>
<!-- DO NOT REMOVE -->
<!-- End SiteCatalyst code version: H.17. -->

    <form method="post" action="/privacy.aspx" id="ctl02">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="MZcMHZzeRoWC2qAUqxgIV65rvyppWd9poLv0w0rrUMcTSZme6iOsy4sXNIsKjrx4bFVBZvHNR5x54N12PS7dFNUntSqUKGPys5jOgDGXBYxEwrHqrKAemh85Bhsp+LQCY6kd7e9RI1j5aifMq4vroVx7IA3xpuPloX0+tvfQKPDCXnO7bYOAoob4fHWVLUoULCg0Af0ncRsN1ahCiZNy8KS0dOHoOJrfl/2Wwiv84JqoJBW+oD9QQ457yGfLy7cnzS4VfyX4jK8sFpKGpq6E4CW+mSvQGzCuBlv3nYsqcpkVUWv1S3E1ro8tSA4S/TkYZyUqtkpwNevsBsUwz+UqJd1Z4cLBqIwQdHkYNjpKLB1g4vZ+h7taQaeRZDk5cejXUhyn6wUWG+Cln2p45QhUhIiF8GuDaMnRqM1n40cPKY9Ri9H2RAPvcBpCNXiCYS+pCnc+Gzu8q4Ia/gDbu/joSngU45uwIW/GGMMjUfskCp5eBy9hmQopB7UKBdXyPfWTw8HI6iU01j828Y26ZjTH870lZCglJY7m8a1O28AuGesjrOAv1a68mawPVN+IsPnEJmy4iYPGxXbnG0XIu3qCo41B9nQErkyy/qS1WBMcXQ7JLeCIp9mbUHMMSY160AnmfQ4VxCDf+KO/LBUSk5xRurAKRwarq/vU46gDO3a+K1tdpOyTRG6CivgG2qbTXD0HitjuCBsi2UpD3eM4OaMlcHuH5T+genryVzTyhJHUTrIBG30egMWjMKPJdcFfOYO5M+Q8n4Yqn2/TwIiEoeOxonnP9RTIHCXl9NXoKJ2a5kGjKgyhWOcnKrDrOss9jOUkmHwgkGCBrRdmfpRVKsM/VXe87duYPCYNy+NTQecbzeG1A4euXa7WWhX3y5OMlqPyq7TIC6xE2WhFJ1CruiRrnXQnySjaF03zA6Hs45ZxYPjlq7+6LahY4rBtUL6yg88kaO/nTUVI1FbL6ox4jHB+iPegmsP32nwlox1ZtR5aEAqIL4NhemFIRFKMHx5qMkrFPHN6xfSEfvE5ejCFRWbcd0QJQb7f636DfeBc6BgLO5whvnblfSebjiFUa+O6HQI7KJ1fNS1yHWVi2UycdnULYNdebkN37bQCcaxD5AUg393rQHsID8+Nt3U2G0AVGc8KSN6hOgPpoUrdXbFeWKdx/F0gGDuqNxUN9DUd9c+7MtfdjoUqnjbSgYqJ8JRd2JEkeeZDNZmMjQL0IUwii6srFdsXoiTgYWySSFhEY2m4XZcQmJVA7ZZdSSksQ0GWuSWC3xenacqplJ0diI/aiVXaH1/BtaShPmMaygv6dxK2WShxGCe2EFxPjGiFpJieR0p/4oWCDM2IvvIL0s1uas6eV1Cl23rmvm4F/OI3LFdYiFpSD+00MgEhkJ5ty6jQ+ZhJjOkYjcdc6e7ql7rUNZK2/VxLzhhfhVqZKsFKiWX3fyPirnQGl3ZuRmviLhHPpSTmMfQaLl0z8j/udeuidPcY+ZsS34S6Y/Zuz8fqLme35DEe3P+QjGtq+s9pq1bElYsVmCbokdSkTY+TT2TkG8Nl4tqGJuKuHR7rFkHpbYHK0gURCIy4ZAh1ViSex36jjwdB8RwsGm1xA8ZVokMfi+FPhawiBJ3UvcMxUwqjpYNYloQXkp5ZjaEa0lkxeIKTgr40glyA9gAaKm28wXx3HFr8WQ8JBHx5I7APU5NO3ZTLzuQ9CZkLf08/tjF0rRat+kZG0QMMLol4BHphrLtlZKSlFBpnR9VpVU+YcIVXzb4uLm2YisRDiJT59VoLwnZvt/1g8cN+v4FFpopMl8KcCRZS4mIyVTWxCqe5Q3C80lYHFWYKHRgeC6HJcfRB3eJ+OBPTwErQrhQQp53QsleMUhGDy2t64RZYMoDm1XD07QWBhg/PsgVJp/802g7PNNR71kt2TYbUcVJCfYxpBqLdzWzddkz03vrN/JffF8bbixN2Abzb6ibploAxJQjQfLZqr42ipG5FhyKHzEzq5mZXJp/eVg0vSS/Wd7ehMpKnqU8hxtT83g82pmv73oZeioxoYbrqyHBHqji0/Ws0Z+3zVOqkZwm3+sL9H9kNei3r2MEbfyAj2np2K4Anb6OgBLIIHxgq+IBFTozWbrM1z5Bs26gPu1V2XbmoCket6z30Jt/tm/BT6GybYEt6nM6MhdHAMrNh5fmux+VDbteazWrj28ut/ZN1vWnGIL0N/nFypaUdf90+KlTPrmSR8kFaAHBzzrjifOuMf4qrV6S1LRzIL5XolDZHve1TafxL068JojlNU3G6JU9v7LPISCtqUKHG5f0OWWLPBXz4Ek0RtMtPEMDjOHZ3Z9ETrrSMVXC0tsKfHWEIAYpjp1pgzjNj2fXy8GYa9QaySQm3FbIhHnu1VrcM+Hr8UbQxm6qAVGmhjfAuk2S2k7vNelYFeIFmmP5C6DcVQSsq8WAcBfpfISmIff6BKvh/YDFh+qoKIjqmuOZAeXrmMi0yzN2sqoc4RtPO+HwwAdx1Z21g1IkXV0fRc0+umg+zB+oas2K1AAlofA9TPu9mP0PqlcYu6o3U8DziZgDFBJyfE57A0ZWXpNO4CH22Krd+gFAIj86m2fwWHS8/MYXp7ehpObnlS1hKU0jyeW8tp5+6ib9Zgo7vQ4ICXilTIiCJn/S1o+mWJwFDuX7Ez3wYsdbK/mmcrk+7ByjV1Ezx3KbNccUH133AM60j14D/X0nCca/aEHctN0IEFWEBgOZ3/Me5drlSYicO/qA3CRHtdqy9qUCoc8/b0s+/7QCBkp14dRPdzB9vVul7oKZbRmqaAgUt4byQgq5lMWjyi+6fP+lmWPacJdSURATJInSiofhvoCN6kCBfkdSO0dViJUb8DT9rl3tQ+3EVMdDK5kJHGR7+rCFa3YtkhnF6k2TB5yqaXH6X21N3gW+004Sq7sOKEBtkiFOUtz4kEaKqac+DQDHsJrYcmAtzBgtgaTWMl+GmUhq5N9hQwFnIqOZVKkVHrzTt3cbeDYBCYqADHfCtyLEfeSu9KBf8yynButMKxPt7xM3VhQc65Ar7tcx6sJgyLMa7dWxCFPXd6VfGQ6yS9LGR3sKET06yvyVAwB4bas602AjEEDJQpcdu/bBpjZp+lHZYFqsfqrslo3ZNtoSMtHVt3OqfMtBw+L4FIjKuhzKjlx5EiReVjuHaZO2udOvPiDPjH9bCPwjA2yzclcKo6MJS2dSEP73A6SJmm6EcvzJqcnqCLvIMgHPhVFlR2rNHvthHlpZwFdULxr29qC5YfdR8Q40aS44ifINGYZu1wKMdMnIVgjyar+P00AfkJ/O6WtrhNxUhLZU2GxR3OfTfdxfanWhGgdafQdHCOculxp2vtABS218u73D7+goGv4QFisdepzlYQHju2W3z2l3ocqbqsh5rVquTzENpnVkqRsFTP539IBYZLQHnZVx+d68P2n4vO5WttucVJJkA3Z55rmKVXwI1R/uXU+zVkUlRgEZEaY1Ye3q/9VmnFsSLd81+kpvepp8PFpdg0YluVhKGVqL11SUmgcbGAdcSWNHJfo4xAc9HLup82mBJOAGgj8AG6zNRMdc4AAWJ18hyEQsot9VGxrs9UcjTcNKCV1I0QUCs+HUWc8XEI19gefMb9Mbjyz7HA9H3ghN/DvMYQ2OZrc5DQrh+4Ut+MyMQk/yE1UX5Degli1sW0mE+QqvH5amR1+L/24JNeNjpwSZZonaqy9T8upPXjrYpvJA3oTAPFDzSQvnfTDEd2KQitI5NiGNKjNV14Yris9mhBP1g/+eDAdTmWjS+B8eVrXh+JBdqQVGe/2UdaYxdCTDKLELe2u8vtAsWi9r6emeBoYSpqC/skEjnI2FmaiRyiJp6TlvWkXO+Yryh9bdxSSC/AzYkXC8c0hISEkUSjlCTMz+MoAG+6+8J52QLWS/pAPnlqzRpM1fHZq0NOaWGHQpkNC/1+QAsXzBtbO4ehE9wDw8zYqIGeqFhjsCx0dTdsFK3TtQWoIcEh7nfAF6rFZHtBYMXwko0fF3eHHgx9n0uelyLrgpq9BB+pjE7U/zq8cImpotwht7EKRD+GB9OQX+X6KZRm5JjApeP+c1eDWhBsIFfCxgQTTAMFFgZt1YDURWW51JIS7wXhRo9lXqw8D3H17gCxbqH8yiyS1u3EWXa4SNLai5bRGCHXKICk10bHXur1CvvsTGCaUiVQNFpj/kcTGSB4aKeM/D+ozespqWS4HWui6E/dQRpIQ2iZMfgNJISmSDUXT1H0NbFktiN0EdE9auRLtDwYQiCJaV0IE1A5UANAB/rqIJEJD1gRP8Uppa+TqYJP+oc2+3F1ahUXETY9NZtA+zQW8TgCktBphR8Q/dkdY6c9y8xSFI2FlXL1aFprNHbHp3SHO7Q9QznZhjlnhJAPiKJiHHjkhPuJ7T2iRy+d5a1wkD9/v+87ttTmcu6AdIPQwCyCUquqM23gZURLRaFt9CfCe5/6iy5rmsmNQhFHOqj7WbL9xWhzU1ZIABAr57SQGdI9waxwC7q1D1KnhgMlc6/vVrKjmfCJgHxIHRftTyfgtmLdfSW6pTGh/djI1EHtK0GqqUDhuIg1tVKowLLz36pmPKlHu2bJzYfPk6MXfd6J6Rssewikr+gUsHFmmP9KVTHFq0Vi0Cxkj1nCI+UN651GGQaflYSFl78h1OrHSQTznZP24TNe26x1UgVEwvY+euTnwfJsLvjCLpRCdg5z30nKMWTiSo6C3yG7CvZr01XpPgGgYxlpzTbJ8OBJwEGWWigYCzSGUTsiJtOiAQ0yoGDjpV/sjsRUmkW8mPlsSENQK1Z2TycSDvOaB0mg9sls9jJlN9UZqMzhtOZWytIOiuK9soYTjFEjQp5V0NqgJA7sGz8B3ieNGwtWfbxPdC9xlXZrm44w0aTZhRHEPUkLn6dnw9Rm3lihqUTa/k8Dl6BcoJbc6vGPwgK/ZcnuiDCc3OfphXjorho9ZhDIZHA8AFFU0mOOTsI8m+u8O6sdaAQOVGjz4wUvNmv5UdD+1LH1j4/GPWRE4qZiRW/ga7P1Fq+3lckvRCvdX6wzdpnWyEdqMZoF+ehO2yd/ZqkperlBkZGi65/4gt3CtDjKYceUHigAFnx5E5dLpDd/1PYpLDsDn+cNqgHFmxMc9Zh0YKuFr4zBc98xfqgOJ48cAUfnWbSSnDngZ+N4g3gMVBA9UCJUHrq+UhDc+FhS1vySyRvrq0BLaLSrXhzmeTBgYshzP5/CkdCLfqExT0LdpqKIAE6Vq3GdO+QQVMX0VZ5boB3mtAZoOinJfRc3Rq5qZ8Tbv39u42UoR4yHTq0RYkvrBxvV/CP8jrhElQ1M1eD5Y0sZC5d1eeiUTaG6yFFq76OuJJyv4Wio+5M0OZeP0w+08DvMIWnWLBrQJSwKq5CDtarknNH3cfVsd+13MHJ0+7CIwCRG1WUf8U9kH7HJpY4ipZmJsYkB7N9rZoAdfJ/zm2xlr6/xFz9ys456pydy/O/d70fslAO09QWmF6s8+pUeAACJN8j3EiFj9KOBtE9mN6XAkHxfcnWoX73dL377SHU5buwpP4vjVhGkQutSv1uMenop8wHn4V81ykEDOzv/KA5SLvMHJVwhEEqgpcA3zwV3i7LKivH7g61UhMKH30XtrHZigANzkcfpwteqyDTiSkWPtHaGBgBrByA7MmYqMp1X6yuy2alZlQueBEi4zEz5s5PFIDqwYZ8WUdsSbQg5wE8+pQ6Iz/nq+so2EIA0U6VGelyNpmyc1RTRXAB5gZgdARNuhLvjCw6StWZvYftgXP+3cu8hu9ysyhONThtXmw8AJd5lysmA5XN2maGBLRB8kksYmfU74NRUpCkobcQf7m7P6GekcUMm8vMxZDAM6vSVCgyFK3LDCCP+Wf4r+Qh0ndtWMjND3dB/fihQA+PKW2W/zIwtiaJesXzeH9xqTzWUu1uDDvMkkMdOsu5ZnHl1fIYGdxAqSjKztDeYSwleKy7hWaa6nAzbiImCewLQc+3z1oHYq5frDQgR968sGNcTvk+FmtVVeXgHSmmV3lSVZgeIQcloN/+LX/P1wshan4Jrq1bx9WKyquORq5rk/9ZEWnVvJJN1nFrzIGVvarKmrRU20nR/TwvV6vK6ayKxRycL5FDa83/u9TarZEGemluxBnBrKaa13gijgpi+qOtYDpzAckfUvI1WP25PK4f2XHXp8pvVqXLvONKbS1gIEN41i/6Ooem3G/bN2g4/bPaB7IwLUn1DQ2EBvLsf5MOl84P/o6VPM0RAnVocquHEUOoTc/9jEKjNJwEX/RG9WUcg9EUpLxgB/H7dputixKX4zgtAzUX0L33PJ6BZrW5Ms7dCMhyGr3U0CWViImZcNi45pY6rkb1DaRhCZxD/hkPsyAsxeNi6E3VOjU8x0t0eNHZ8gmmj3YHNjZNk4lJWx3+8dplVpJpmHviiaIhN0xFvcqhGK2K2CV4WgKMdYEPv5e03l/ZU6+Q795//2sZOqbnsVvipBpIbUxDqMPMSjBvr24DrGHUfJbd19wlBgPVKNQup6+NQPvto2WJB7qjIZyIvGJ96tVGwJKds62Wn3GhfEwijLggyJCtZpzrj7unWg9DL2J/z6to4SmvZgbKMX1pMw7q9kaGXEnxvHxSqSYqFaAgOFZpHoB5C2wBTTp6ybEyASpxMIc/O9iQdzsj6lJJBYJ0uL8yHFVwsIwANQJQ5YCJ5IHYw9JjutsXq4qtClaUx4fI0xGxTFsIbAgfNm8F8jznZi+X1fLQdF2D/b7SI9f2MHNkcPnJ1G6gXPv51y9VelMvCdltA4SOVmoSj9GZ8QBWWrNdtiQ+TAmuOqBqAyrn8e7JsaqdZo9XUGZSX90abQEq2iw3olaeciMicUnHtvvkKMGPJMko+cS7xoA3hMzwsFMDOMm851iOaxxiNDs5vX5WWsoecdqkF9flyAIVpmEFGL0ixg17/j9oCw5OJ8zSDaJO9C3uB3/c7XHkXsVJNaKyCuuC3SEcQTB5+mUZVBUbdegmACnIJfWo/cKT+OL16Qix60hnH0Yb32yyOzsSHspwr/GwubCxtpidAKZxbbr6Og6nht62qtkmoZaTpsmvb2+I23MRpkCK3zSw160fgz2BqgetUZrReA6EEv/3jGCiYntPRRCnHjb/R11Fu5xbpYR7/Nob13dbJX+Lyi4MrqY9+/xeXblq4i0pwvuhZmYlfwgZhvQN96u+94jp5FHLgpL8RN8abkk8NO6WnADHOaWhjMD4ERKcqc9Ypxep+lQNudl0YswydO4FsEWiZqOWa03vzdaujVfIRzZUGLr8xPej1wAVcx5qsmRhW5dZLktuuSFy0BUvIDfELLWjhtEqROoWQ8MWC5ZxuMp2LQA3z7usL6HFhXDU1KyzU6FVk3clRMQb6qlpGSeT0jH9U2LyztjJCXc6XeY5wowNEJU328vMTImZ8sCj/GD36Sesbnr807gjkkxPEOsEOxsrwe+Uabgqt/AxSG4uOwhDzpvxdarGQEY/v1KLBDhIG/tJdMJkdAdZEdFF+S3zRvGYt4+AUC+0UYcnTN9aL1at3bj4e4hI84eWi6wwmvJ76om2mrAbMWIgYQ3nM6hPzGPcj90OQpuX4YzJ72CuCzG0CAPizMHzRUQah6V/ynBb3tlIjepy3xyAOntaMXkyKIcxTcy9nuQL1iONyTv55w6Bdsj1LloqvYP4EINUtUkN+u0amMmIe01xvOD3L2EYpK++pH9NaDlW4lBXd3BfB3g1vNDuyTCQvXV3b/M+brX+PEZ7W8RyQVLGDpTDZeyBZDX721SfH9/zB0JMPRIj8dVccYFPZe2MAinzQ5x/Khtsl0lQ4iNo7lvtWFI/tfVmH823MO6sW0PAawk25IDxXmAo4XvDc1eclwmcCnkxtDvXp5nHWLmx8mrG142RQmOHbiiXT35o5Y6w+FOxRgh/U9j1Zr2N+wbfH+Hq/cB3IlFygeXI19+piRF8aJkUak0440jqe8iwzv+sqtf11JB+x1cS6H2BtoTzuKoRaVs158VdAkRNT1g7E8kgei0J67zl2UrviUrD5jMHKXtbU2t+3Tp+Gz5S3TEdfyxdFRi0eWFcy/78m1SVMkMWeY+6vcdB6ibWxcH+Ls3F7yXO2sQoO4q4ZPvvg1WdAnV1HD6ieXXERt/g7gYAZBdFe+5W4skrex1t7a8ra5xdkx9rLrMZng8TKxM/gCqkUIvij/BllIDZl/5gU1/TqLk3Q4GvopepzmgDnbvXURDgkbwoUZdwI2LVBktHcSQ/oPEl/PFE7C/MZk6L1WDuMpRm4A++aNoW3UvpGUrrs972smwfYnIbinGbpjMs7G2UQZk7rdObRYsYfFAQOM4mYusLcjfdlEJaZNFHCUo/qF0iVMWhaVHl1oxJrEmodyo7pMPtyFsDknccChKp2oKlbiLt9u3m9qcrVLONHm88fnsRDcxFAcwrmbSKiZ9Ylvp5rqQg96gwebMSfcviXPf+3rCFWr9yv02tRSwd16aXKJKdsNhqaRcyfE4r514URfQ5EzbnrzlDMhAzsQemCOsTdspYrxkdfob3h1h22nZ4ir7JrCJ5+LlKxlPTZCQ3T8ph4tilzdZpuOTFtHzhSR8vmnpL01qWOkJuhLydGTwqEQkrypa8MujUFzQSH3d/1sVFlVq/GGyWXXVPeUp15Bg2f3TUwHXrxvAiCLiieVzCERa8xbegZ7iLJlCPrvU1qaeAM94lgdtCpB6uQhbuC1HT/C/thQ/bynl6MTk3nPF26pfL83pzmwyw7Qsqu52KjQbGZppJhzRSubZjaW8gLqnhEGD3yTOB+d06TyB7Z0rEd71eone+zsgmkgpRy5+VMRdFCRannT76ynjTl04gdm4Mikf7BYtQ6YZ2pk6RycYWYZ/VugWrBUD/XS+bus2250hVvqLyqwDY/YAiQlBQ16+RUZeXu/FoEMBlpq35ggG/twtNOXDs8yE0xNC4bpBhDxow8k4MQc79ATX7QdzYZDpMWqjFZR5HFqccOv77DmT4oJY7PROY13KpLEiLkpYQ+kQxOIiYqQ+JbMvLpHOI7HyBuInJu+osdRdnH7ZbtR4E7hoYiKchWLWATThA/11RK0A5IhlNb3q/8irglP1kW8FiJDvqSnWTWh8lgzlT21pc57P2KsdA54R3XUkesBmg3adKm+Ow8U0FVSTJjL1ixECvWhEhM/q8oZiUEpBESWOqj9JDyK5ChT7NVyJ5GjXSJjDJV7SybM3Nu3mInyJ35LO1jzWgNn9Tn26erk21YfdDALj1QcBqzZUuQ/zabFt1/Q3KbUZRZP3KGyopRgrAoPm7lg/PoTgayYKsQ6qyzzOSUGQbjeGkhHI4oUV5bh99v3XPWzC7K0ttCaV0qkKn6jqxVkhSdqxMgSnX9P75aVsy837ypzmb4x+8vLx9YhwXM8h9aM4XS/K0dIbdzToGI9+SDcZ6HGUET3+Pq2bcF8aE5fNts+yu8tqnTzDkCJohZOePfxw2J4ceLblPJzWyuy2iULxJ4VPnDK4cTHPQPMOr94SERHdOYYhyqKbTIJGiGRGCdzH2CS1kGeMs/dGdJ1vzvvpBVFUQv5z7voFlbwOYBnsWOmKR99L2ENa1YYi03S8M1iQpMHRYdB9GqdiDlYNzmKv4LQfbXecebirrHekgH/YJoukBhfXRB/giUDysKh1+0p4V+aUanFpxhjPyuLh79t74fTom/HN5JO9Na/Ij2vw4zq9whmQTE2PIR3Hvo/nQDzu8vdL5wePqSgv9ck7HHCvZIH05qJ56WcjrSIbslc90WdhD7gxUgsjjWssXHV25lRVQjreyZkiyktXGSOGyrSy+12ylHVA1YOk8VznaHbTUEizsP6xESMVuG+cugk6fLoQFWS3IZX9IirojhFzb+9tP9lN6Som+SAT2zzipXo9Q7ui7ox3fDft7JsQO2hxEZDtR/PHuHRkHGbQr4jLmp0B3OZRqF6+KEAgGClXKnkAVqhyWE64RsmZnVok9OQEKRQ9EGcPHKtjugcwo0U5hxcZIX3n5sCfFkpUf26Rr8ZkRC4UU3KRFZGmLruQLLBpK7kmXf7uQD4e/xw1/URtS4XejxY5MvOXLvmP8m+ebLAsKUZV0lthbG1Iqk4Zx+WUJVyMqGzs77eSekLz8PSA57qh3/LJpXsO93xIvd0QC97RLY8KZ6cZtx2DJUlZlFMJyytuGivSxONaig0kymYhTjmCMDLqkDDsuxu80iRImAjB3vIZc5IlbiYVae0EvsLdZIRFafVys2nfjYmPwT3Nr7T5X48gDQ8IJVPnjFYlTDybZxQhgDWduI7O9D1ukQSyDto0VbVe/ayXMfux5krzTWi1B2M5RtFuEKk15Ezmn68xOqe/t6yCZYy3TwwfjjoCZ061LhUGuNaI6qiQOtwLEZgsw+0zUPCI/l/9I1k8fsXreI9t/Plw2H5MtTSR3ym19UXQQkXLUhseJdTdfqple/KS6yy8784YXHgLveWAWaDky8hmevf8LVMSeBvWDSFLHdVZl8bd2+aKkfnI8VdXR/AC0ZL6YWquss4rec76awXO8Cyw+tmU7WnUdTtBLGP3ZV+28bHRxWZ3mLqrIdqxzmyqAzR1z6ttAVyMVnl3IKinoP/jZ13a5oTWFm1FfZ5pPUftAaIyJf/OSnHImft+/nk3q/qxxYsLqGnLYTIafd/lfLxp2UdnB29AvRBFEKZMIX4jvRzrkUGFCFqlBRbSsX3eO/IBHioi6z6NhNUGh2EXsSWMzlczRWvJlb8eazRAmSkWp2Un6Sf3fbhbDhdPhyl/wlhQa0pupXGa8nN9JZOO4qmM4cA8xsg6yAvqgCArdpDFn3nN0lusGMqrmEnk6yleHDShEXBWOvwfKOjT/mc0upgn+yDas0Y5WkFiwOkXm3GPG2b9AiQrs81J2RrB6uI2IBPkjcO7f6OY9q6NsODt5y9K8LXFZxlHABGeAGyA1CZiBsUdKKq+S27Ir3s0ppylrI52oy0zjK5pZLW1intGG3HR3eI1Pm5YOAVseot+uBKUhbERqajFJxDDnVhr0SaCkVSjHdESLPLu++v744BVWUmFCpt6KkUI4ZLkh782xKuCVZm8o5xUgeZDt8RT/Zoq3Ne5Hjl0VoIgNC0XlzeRm1xgdUajOjY+X+oJIvOWCiVt6OGdD9AM6VjvVhbt2pldDtGFligoRs/3RGkYAL9oYMcdymvFebD6H4Se5ZjUGP/7jWDCIRX9NW7gus/G95f12KktRlN1LlUH4fH1W8J4UK3kUFOrUXIGCG24oMk6PnC1J1O9S5mvJkJI00/daW0FA/RY+u61Nk1nwru04ZC+9Bf+SrGGM8dcDxRHGiF1M7IMYp+psxfc1D8c2WztB+M442DMutE4VDn0afW9QuXjY1jI3Ve47RrlWZbdf2eAiaVLWn0V3pgtKd6cgYXfLSoXawzN5xcWcvlrSpa1JJmGSi3v7lXmSjH+EErUSSTRHPHSJJipk6r83OLDICFNXNATWd1PwnSueF1D8uL04m9HUDalNNvUA8mo4X16mcwesLpkwZnAODKsZsagfXiEAFIUcZUY8R+el8HSIHu5sSawmV57b+DaBLntRFowY0IFc82341TQHDOctJJnYQpH+439Sg8+bjvLDzxr1nXf+rtDsMYoY1icXZcc2s5Sib0//IPJ3F+bDZXkFhqptpVOz2M8B1AgKCA1Hfb3h5Y6sUo+0PtE9j0qPze+XDpNTSSvXgoKn2nFgl4ph6++ghYKh0TGp5VbZ/xZmmGjkAqg0X0Nl2rY1ZRZ8/v0OkhZ+OfDG/rzVj/mdNkmRtFS905Ert/QWpwjz0nHIfmNoP30Fn8WLM5Gs7bqZEEbFsDkS/gZIuqBcVJwPAUIovg5zZanSFzILAPGAIp1ja0nkKTnHw0j8Ij3rXC2xbXBUik25Xjk6aQSu/2fla4CStPU2rwHrJESrymC64n9Ldqq5hZsBOO2i6Tl8HEQrrtdic/unLu22atZm7qVrJEcLTdvTqF2C7mD9GBnpwwup+bqvcRAni0ONXb5s/8JwJea69VN/ruZ+vTUr/CZ3K0JQV9IK2e3udBlCtXPKspvz5ekmthldP3F6MmE4nK/bt0jLZZv/TYCudOZbB7Blr79SYIT1Es608QFdreXIJlrk1q9RB87/nCIMsglMPK/7mC7O1K1pkV6HNXNHLnJxr4XSV0k3OzpeaLDn2JWZ3vJ3TqGoaYC0Z730wRdjwIC5l2eWlO39oGbaUxYseRG1nL8v8gcRSDj4ADG/yhLOQKP1+vR7jcqN5op08ZRbk/becAMN6ulMKR8GvoAQyL1/ufCZY2OcvPoEKH+0ft/6f93ACuvWO4C3JtUhvilRGmhh7WGSic4va6fa5ooM2CZ0ChTgjkddoMjuFxVSBasX3jRyIjZwoCW+iIX53dRsB/oUTKstQr4XxcMg3GIILAdJIo/qwbfPpPPl47Kf81e58GGKJW9otwMlkD28USnZmi/jksylgsaDMSbgDBc1cjoHOje0a9SjNeDzjdFOFr/eTcIn17qoROndXaIQZQ7ewKpNsMhdSp2ItKzYEbRL+Ngg0wUfpajy2lp+YX9t9ZrIuA0B8ZfBigN6Mons4i1ZYi7BfUcTJfpprBG4essiJCOdRh2cMOQuIgrptCyYfeYGPO9yLl2hbP5YBn3cQ3PxI/8gqzee7ncxDcxBFZ77DVDJVylo2D2QWAJFpJA5wWPfzsKjanjudLHNy5ChC3DGg8rI8LSz6g78f0rKf1cUWsReNdyAjhXA54doQ7BIooJ1+4hMLUKyhzVe1M+Yx5+DhosHTXo9Z4sXQPemIvJZu0F/zwtpTYHJX+GruqnZAqChvnnsTgX4ZGcZZHaZZV2UL85EdJ7O5gkMc4Z1gJP/hrVBEZLmstRUmbFl3xG7H6PyBC/xPwg5knvwwveGwPVp6p8mpMCZkJM32LtCwO+IDfB2suy09S99PrnITl6ZA0jIBLDn5hHF14+1DAyJ1idKetgVtG9pSaWk0ccFSK2n6jq0ZYeIOp+3piWcxh2rT2spFZa9R5GW12erakQKLtZ5xSWeBX0whk5infRiSdjdQOgM+YIhP7KqmpoVJIkm+Fy3JUoT9gTK+APspYxw8wI48Rv3AhZzWAmVokUWAKOa3QzLwZ2CSEukJsPz47UhKjzmAVD5Pw935w9k9WIhlV4tDn2utlvv4BRCF7ZuVQhSkIYGDmp6HvZE76uKpazE9WHfv7t9EZhNNmrL3MQSQbuSPjs1tkb/qjtJc6v1a/sW1ccoxVOgRl1PYm7KOrGsW6r4Z8ZcSVpO3H0pgXjVSGrWnWLuoGvsuZqxRojVoEjRN1rQr+ePq8Py9umR36DmRbcosk0wHBi959KGvnBqMag4C8btsRJ6Gvj9WyQ8BGqYurUpGzCjllKDM3Ld9xWcZDUBO84RAdZnhEh8WAHUrvs/ipe8DtgNoz8Q1R2McdAmjt8764UHIRM1pYL0wLlCNktKfMZwFdZlekUNhXgWeVKlhu0S/X/IE6z3VudZi+8wrRBkdS463bP23+PYh/4IdbSYpEgfrJBAuTe4PvsT2IQzxlZ43tbJScdGoITH4gzq5CsHz9y7FwWfLmXFULo8SKd3ykTTm+OGg2OVUDij3ZZiDPAioFtwvxzh9cEhnQhZ7OkCspGEnaEzJnyoftv9ykc7XZB3x4gfzFQSFUJfQim+UbNT6VnnUylPaaxbRKJOD6w+jULnsgeP1Vo4R/xNve1KFKlMycmjgC8DInAhuMIi9bHHLTXobTHn4dI1sGDaARRULED505PG7jUKWR9DarOl03v0wB46EcAclCny58Z0c8PiWgg40rcmnP3xL5O2NEqgGp+QOLBTYku5iqfmp9xVAD8e4fh0P7afunvKWOv47WKYRT8AQEOzyupsfqH5cu1zdI+DyX4afuJfgFkzd+T7tpntM/M1ikKZeqWbeW1/gisOiFz/QASIvX64jGGT8/xhQ0EP6SYngHZvwHIy10IrXfdhwVjbBB1JgGprQCq8WV6agvsEQUR5nJqC5cn4ULocAZW5cye+zlTeXI95M5DhEprS7zITKp1WG+2YKnv/CYB5fNK1SnJwAb1Nm6cTEeUVcgaJwy3JT5QNaBkgowZ338FIrX92UtnJyeqnihcD3mGKMGgIsJBZGts8EgcYjzNVfbaXuhrOhJMF/Gy2D56OwWVNXKtmqgV+6NjJzMzY11RWnE7M7GyFOYL5DMdaSqCgmZdb53lWIgcWj7f5iI7qrdTkzKslcCIMuHMdeCyvBYmxaOhUG35J6G40V+jj4NFuiiRR/tYBHUWpt59TuBkaHV6fH2D2yLxLK4LRuzbBvks1NCTHbHBmEuW1h8tRlgK4MMNc5oXOrPLrmGMqQJ4ENcyG5woe0fgpEcLVwHRxksiGoKQyYzeOxlPtpIyCF4XiKlHMA3Ee9YAvilvXz3/YNmw/3LxIcVtxv8wQ4F+taeFa1o3GMva/L+pdaVTy02tC2K+p059IlEKJvPgEsRr/Wl4e2kuJwHstchqx8qYD+L6OpPFR865eybt0nbiXpt7nFp4luMTMOi0RITSZ73K8tYC+++bW2DfruiZOkG41HdJD48A19Sac9sVounDadVwHqLL+vMT9ztLH184841I/3yUKGNkug3lqQzdXh7pboH6cxs98gznBhZw8N5WD1JVFGEcdUnr8AtYWZmhUyGGu14RpYVgXul9qTInceG5HDoIqViRL2D3U4KrA/DEyuIWJJ9k35kH8Dhv+eGe+fQPp/1+RYK803/E1lm41Oy1ahYSEwKxdJSpLdHBq36myRqTw6kgccU8IOjYt3+Meax0x5UZlaw5hXQQffpexZgrUVLl20B5XdwnFjypRIosC4qGobTePe5KvZDRKJpYJszdn9aCRN829zHb6DjUymfaePq8TJI1f5wVJtx/UWPdST/jmZHDMxafDRpViFtDQhphm15IDJNHgXnigiVwFvDpWKvBqd2kGbCtrDra5mk0On2tYv0ToP7xtu8SxP++zx2w1Sp882L4Hx7uet6ft86ZhVH4kh45rcCuKS8GX9II7Li43SW2KkwVPrQtfLsLP1yQVjNxgz2plm+Aw2FF2qfJEGXkNsxQZDIYwdnc07VoFF2TMY/dYrONNCRd9E6ewgYaaeWLgB+NUB8ZGDYiC4TGNe6QW4MO6+nE8Z3aav3/GZjUGnZ+YFORdfhZKAjf1G5uBYH2yA0Ux+ssDwV7JPI3YSc2HskEX0KCi40/FI1wENwsI2YKznobkzRCHDYzPyPLhT7771E/Y51OPFbmO91dkM2Oqk5I8PRaXt/q1JHhuqveim9ZbZ7gMImfZkWhBnRCQpCTbV6gex08tE+LII0rnKbPcqoiHG2IBi+jGYosOwEevip5D29mJC8pGmoLc/cndclA9xZNfCBRDsGymkSc2j8cFFHEwFNPmiU2aCTe0wRjodX7513mEwYwmJRHI858rQNgqT0jx0uWHXWdedYzo4ZjSpwdmCjiF8FVU58kDCDqMwmeQTPEkMneU23KfmDgBwdK9gtcMJod5CbKExBMgFDTReLScV7xKb78rDSOuujcLBCwGZmwBtzBFMxJtkBe2naZnkjikVIvzb5NZfpTwx45fqZUnPpXmLKu3xCyp++albGFvAjVWIonwgs1WjyRjIUZdAPgP7Cfc8cOjzaBbaC9KDz8PhrCjReB0KFtCQGA97uug6PAN9WB7zy0yWg6SfxTvioQP4buL69zp8LjEgnvoEBci0OtXOuQAnKWrfSWUc/sL7Wxa+MABszx88N3u8hLqiaNyQ0QkV6aXKC0g6ZY8HW/AGFFUofs79xb0en5cy2XoHE0Zeez5XCkoYFtttAxRsUn4Ubv8PI7OsPIK4dCXQN1p3MThJqDbbsSqmIIa5ulnnHLB3Z68eRt2LgLUFdwQcpAvmHYoezXLgpVMeRz04QaWcMppZESmwH/t1n86LYWLNWXryWSdYeNkG4jTEZRYeOLt6oee7rZrgFRgCb7KWjw/nV4zajf3OApEubHq6/i5bTT518qmP7UoPDXG+W1Xeg5djb2YpRspVkWDTm4ga9VFlzisZoXepEqeyuKQe4XQPQ8QYJ4cQEA9IgFsIgJVb4VRXJcVhD1/GWQ6oEysz3HBBa6aOsDIbD6n3CvSe9lFLACx2sH4gbAL5VRD4/tKc9Yhs2CXmf2N0XbMTo2NOTrtDdKeq/Zxf2MYkTE1exqliDe2P72T79I26QL0xMvE0tZ6WVfWYjJEwACpa6XBVb9eyey/Bf4jI15rQ97GyIRq/daQfGdDgKYE95+sIQTDpLf3E0+o0hKUKjsFnYxL2GgBhBOOcnn6o/rt2kOoiPtkIHPmDopgh3AI0lygRDZIPNlgSCai784Hov+K3P/l1PrD5xrwYWpk+ljEnupN1bhMS6Hs0HUBNtJCBmnBbS9P5tooEf+5PDwhZkxZGL1C3rmwbRTs+7EcE24cefGwLVSsL5V9znLhz47V7APsHOuDn82cMWy3OFvMKDUNwQy51nyH3On7vllXeg7GWMa0MVOhLz4yVgIdNV6YbEl00UOBvLIGYD6HWz2U5CAvX0W+Kmf6+ksXNaAXKTmSwg32aaV7KQJIbf8IkQVc6HhtbXK3aQL1RfF947DL5whv61DdcXmaEEwQtKxoNANvq5yWKXi2Hz/jVkQkuJi3YYqsKmNxJ6b5GcHnRj/uG5rLn2xjG8qhAUL98/aPkoCUJobvmwGWS6M9be/Fpo2qn86T0yw2ybJWQ5IL0tc/BlMnqA5qUo4G36Dw2zEWHi7YKWDe9qUPVeDAErl0uSTbyaTPs4ewDwVzo+mCe2R0fOdjRfOyu8VEzFP/LPG5l7q8cn02imjIGIP/xxTDGEO9xecVji8Gi1/764c3XAlZNxDLRSBUxncfz8b4Gn/cx8OeIIbCIewJi7Yhszt/Q/P93dEnkiGjtG4hydMM3bKOdGiSgDuvhsgJJvnH9B1L5uodaUWCa8/A2j5R+OcylzxRsFlc3nRBHI5joubzZ9J1DyjxtvfHvJSP/pgFdfM/P9z+0lTJkLnxQoD+vLSls9Eemn9YGp/stoj9HNX7eR9jQcrFYz5bajp8SnTQmJQ6BWiEp5ip722JSUze9TyWiDzUxWKStdKdL6uHoUxEGobZVcLozFqkZroLFA2i3esoLOFjf4w9KgnTTOebAX1673v/wlyp8lllqYt/viz60USYv4TbDtRSi4cv5/5b9rAaVmSJj6KSygvG4U3x63UtqJ9Fa8BAilO4v1xT79wt140MZtWpKZhAIPkQDo5VBo+8zVp+Zl68xQvXF0v3iB4NFDlRqhTTfyWw2tRC6wd6+QIW556HpHJAXfeoh4D+dE6O/MtpLvv7jPoYrUOFqSp/gQUg+tQIsIHFTfXuvWROCMCgI6xbyjLUpkytreJ3ZKTplHkf2DzELvnYHl2KXpebQVwMn75iJfYCojmRWxGQB4U7+e6SkgtF+n1l6wooUKrcKq3wflbgYoC+iehVE3j1UT4lgXvcreEtkx0BqAtBJmkjzjE++wDBtpYyF8laBtzvq8DXUtG2RkdouCvV6Mgo++T7JsWQfMPdqGrtLffWFYaq6HsUK1gibIEZIUqMG50YAS0jjsC/XDFjhX5K3ssIdW+h6+gBuYCBk1f/7ArE+uozgviD4mRzj1255UPaWBCD6s9rkDtxC0kPlI+JD8XA/PL5YmlW2fn7gB7YsaJVprPos/hU+1Weq6tUfUmzz7Ee/hPbHClXqd9P+t48YMUv3SC3nmddC8ZczVXbt11R3PLN+ZfIbwuy3r+a/XidIAaWHwx+5oFD333Z5ATxTawsRs4z/6ymZY2gSIOvTtHh3bXRI1NuHu+Q1maGMJtvcEvBpjCeL3nSSUfXv9q2LaAy+d0DkVRxOKSWHulPRDoAGYz2/hhKL2WlisVw+9Zav1+C9scZpCViV/Dg6iOK43z6L8nUPvJ2tb6oq1N0Kjmm4agzFokhkmkI4PNOC3HyBW4WIfD2xC7U27HsAYOKaC3HXC0ulQmQgDQAOhIwKQoNX7oD5/yDxqoHvye+Gz480Wz1LQ3tdvyW74lKei4VZCIJwOYVriHh3GVvzkQ/vvAVCOkWq6gGGFNXNQlPZ5bZf7PVw9g9cQsY40LRceC+g1crclBWbTFQN/sfrDPKcdj56B884D+qDNVdQe4udqMQDKju10MOf4+l58OFdQQtF2DpKHczHQ6KoRuup3ZzqtfZxi56RIqZyTX21jQNqoSJz9fd9Y8e9Cztu7zy/91Y3zGU5RJ/WwW/zA9VzcQM/8dln6dnQy+riqWg758FGH2U3lq0Aqoh+i++umtwTGTMSHw9/3bMcTiFM89c9vBTxODQkZfHkcpG4AuOgsZj0FwrzNdKmKSZknAksdp3x2VxHkX1D8l9kxxACur2gV5pIiHCb/s3Un8obvNx/W0e4wBTDlFIQCM8rXA7b9/QLjMHoqre5hvfYeVHM0m96aYBXE2fzQNldfgxF6rZWjs9DhD2HeUsDsk98YFeUalgb1OSeeE/MORwa1i6P4CVHYUdHkT9NBGkhrG8HSTQ0UDJ62IjZKbrtTIy1tQxRqvmvFlouVeN4rRPaStaIdGFxvovAvsSFVFbkDJzVlA2Q4bugdZhPdhMYOP1CusnMrDfM0Wjr8somADSfCWOUJHGd49dq53hUC6v76CDGhepGmnB6Ed7j8I9peQ2PzUqRrZ8WH7XG/pc+fYmu5czpJzBvL83mwd2pW/cswiOIpWMh6N3z9ppfUQlfbW3O4z7equlR4DEPUHt+TyMFtBmfJlKQN029OW0I0DnQ124xNXfVd1ES0Qj2TlXtdPlAdXSMCUbHZpLWrdPMrb5A9iG3VRRaBxByxEDqBMJ52AHD3gx+jQfgs0ve+vdUDO3rTK/+lMIPtD2uJ29UV/eig8xE1kVwikdAVGCMMztt9Xt2J3Yur/MD9v6VQ39G9L3AlvwEKmyh9dWLzvhhUUnXbeZnyhCdTNiJ6rOVoghtk3MbIumbLUSikVb9bbP73C6XDVvn0LJVatINwpQeRm6VthHo+cAWxyh67nih1+waAG5cvZpCIab/107jX45HIBlGWEtH8CARvMQYYkJMWAw6eZfetiTKDro++TqNHRttQsxfTP/M+sN9jcDury/wUZwIr7LHoHtm0cl9G8B7TcakRMyhB941zAIG3SWFLOCwQA+BfN02Uhoy2RRy4YHqlg8KvcRUY3K7IkHfQa0wu/l4bpyC06cTosjNj2t/iXUmXOpYgYuexTkZYleGRWW8huhBRxrduL4Xyn17rwvZTgajXV/06n8t3XNxViuZqtGIeInq/uRFyIRSvrXj6DqxfrkLyXGAFvqxXA7sAk8OACl8BRUeu2sow13u3AIeb2vCKs/4qjNYpebS2yuhbb+kT0bHejartTtAdrEsxlVi5BnyTAt/YJSFpI6zunR/jiAyKNOUlgNcYJj8BNdwPOBV0tnosYUVwFv7R2MxlQu+fRp6iSjFQz7prnYBGwa3rOrCWJfnps+kKjvpe3vVseIF2lsqr45EW8xw8SY8gJ1M7Y+YcSSKsGzPmql74u8Vi9qjRXfIRTErAhGddksa+3WAWPOuUdukzw/cap2yxR4s4gHqdn6N72q/hjEn3iPz3IzD0VoydaDsIW2cUG3lEwbTl2Xb+hEFYB6LPa4ZD8eInoBUUirO7dPTcZJ7VOifU/j+0XltDpkdQ0dOFpSRfXaiIjDbY6R7fqQpT23QbBWo1EbhUD33wANY7QQwndghNBGA6wZBDdbYUHyPm7doKj18w0adNrpiyRWYza9jJ0rp8aWZGzGLxt7/WasauJa4Hk8juV/k6QkAtmEKFnB+HDdB11inqO5J1a10mL9o1hAyHuGOn7loeQJhfRNADCBS90o2dEVEY7CCeuqGS5t5SstmBvdZ6q7wJAliB7n+qQeVt+OHcg1BAqQkPR7W0mykjTwgge7E5g3He6d9M+Ef9tE6pB/hyaCh05LqbfJD1Rs0KDzUzFpbhodxGZ4vErOwdm0HJd+x0aSp4iFYPHiHNil93D+rC22CLlL3pm57aNxVrr6MWRPfDSKyIyTdsV7m5PGmb4R/+E2gwRhvYi+0uiq9uFFTUduDNIjKJ0LFZ55us3h0cGJoa59tB5NyuqDV7iGsZ2KeKpEDOXYfplsTOUoivoxiNpS5xBnD0UVNk24WogaU+ynVOcNz3HixfcDnc6TvK+KwvySoMKyU+qe2kHPlVw8hjVj6suxlR3Y/7vXKC8bTAK7HsluDYzhi671OKM8919q7khVj+Kq0mJMdSU9iEHtky2vqzN4yvWVGIyG+hnLzhW8z+TACAWvh7sBYjiuFjIMCXJzqawH/idEVKA2B1BCxYp3uKKhjNiKoAX0FXSvtHuiV3tEqon4Sy+FfJxl4zY6jo33RyVwMdKajSgYA0d82IMSzqdntqYTjNlc7pifqJkfot1e0lPXcBcBNq+qHiag6mkWflQv89wgMbEVckbz+MrrnZQvEceEs+nigkMYsiKmA7F3W5IfBqA4DKUxCG9g0VNLhyyPaErt3jJLDRl58IJWytnlC3wutT5HeOBmWZYoQKSCN6+6j/RXDRJkyINhTjNn7uhoclcojfV1f4lD+sFriM4EESfewV1C+eIjMft051YFFhdI8nPQC8n0dxkNBK2oQYPx04GVDjcDPJFsR+TANa1fuQ2BbMbaXyGshtfgstu3IYBCJwArxBVbllahX12IgESVnoa39OSAPOymP2zN9QS9lFf/0Z3GeEqDsDNvT0zCG+gxfIrsXzfNm3Ud28Npisnd68U8w6zs7pgqr0hRC375SOEa+yYAvy82aUDcvIYQQwqANzQO3ALes78KLsJlK/Fkjx8n/l0/KeSf8Yt5gKiV8BwuF6yZ5HS0+CBocqdBAdTzTgOHLbNmdFfvBnbmHii1FuD0lg+J9jqYyDx0940U3e//eLmLCHHe9nrhuszrwBkUZrbQNsMpTMr9+kqpsCpAvXniBC9+whnm6YJAncTdIB/SWSHtUYvQWm/ecX2MYeJnorD621/G2ROnKewHVcMqvE5kmM3Nst0xm5BbEF04ZU18d4beU58XfDc0qNV+dx7YEE/Wcp8q4e5S+u7g0RbkZdakfJXjZZlNqOMBSSvezNevYCUu9tS5hOT33wmgVHJLKhBpuZo74T9ZGf2RfBpwEOP0uN+I3ehzRxM2Tkps3hzFPY+npS0/pBlDpBxhvz7AZopvA+/65eC7ikVSBQX3sYgUdqM8Tg6//HwK5okToelgvz9nCHYqFFDD5ONX7D7fvDmTGN1bMivcGhUgQyRo/n3x7jyDNdMqwBhYahYyRMaxiyQL+vN+a5MH1cXBqWVay96R5iSFIK9yvvYk4/klVjvHV6QxHLdqTr4l4ExAq52JiKg6uCFQ/eqHmrSNPCXvVWGJdGv6J+vBXThSxhGHYBiqJu4GVCN5vyIF6txQr8MsPS+MkRRgAvpfR9/oa3a78cx0rkwvne/1jqMom8ktxXpFjdn/AAe40NJYZ38pVTjmi4REgwLI2DYsWsM9WWckxGZ5LlgBDTE9lYNHLw7WxaiFbMccNI/ABWcT4jh3kkZtOhN3Xc2LnXhIqlkhEBWJnHAf7pDC+NJsk1mW4E7NAUiovXWG0Z/nFV29/QWf5uJ4s6tXxtWBS7hc/YEKOe6k3yiOTbII1T1uYNH+JiHseCHNZF7Hp6WRqmPx+Eo7MBxnUMMgndIs9bWUssxG0O2soczZqVHTYtPVIPjAB3MqKn6Zk6xQBAC5/zAK5SpLbZQjl17pwgBdbKV8RxIBl2Z4fhcHsWfpLOsnvXmG+tsmVvV220WpqJ41P/FdXbbF0e/YW5lAHi0MD+TwxJxWyoWjeEDLHezvT7A0nVkf8MaN5pwFWtJP+AHgxscx1q16Mi8acqNMxsQnl+7tOr0a+O/FKj8F1aBKOpt1AwAVxYfF9FvT5AI1gCBmyUayLkwsg73fRKJknBOvgVYGBqlv5xFuVSiKCHP3aFHd2toMYO1LOTRc36oc96M5WgAF1jS2z/o62VJajHBV6vgxugk1a9cdrawJmn+huCVbDyqSLWXo7/0h/wITkWHvYugxj2c3GqJASNsN3G9V3GP+hBZkrQ6PsERrLCQ1HI6QH6rD6kEDO7jIZp8wVCOQ2Q8ntWL8TqQETODftGTluTbzK1iZcQyPB5PUHITkPTXwlvUl/2UDv2CTVQMpCSevE2Y8dxh8+WJdf2JmZWznASIOscf1qCVsm6UHywn1GzptjFHFmpQUi88PnQ/vMouivPhi7RTRtJGJzRpaSNbrWf6yOsVn/IQSz7QL6nNbNyxOnlLj4SpNijhw+71edaMh1Y3sG3ZD9SmOZBQyzGbSEDn3AEz5uPGZvhlPgYGpyqlxfUX9cPCK3tdCvUtMml1/v3CSmHAbriaGJw2CMJD4wKKjilVNrng4v5vck942goc5qXLmHAeVCS8v3eU2pClx8/UCjeezoG/Nvm/Lhd2nOKEprUjYrLOmhsRteIOXSVpQVTURNJTnwtmPIQQZM6X7JAL9/h1bw53JA0btG6KRgErW/6e7ATSeU0MD2zgAQ6HcU6T24Abc3iPDv6MFu/dtsbHWO1dVdX2KL3Tzt8jsk0P0rr5drG/zG98KY3OYr5DdNOKsgzKhCCegvms4S2b6iq0ESWALMi0sooWNGL5sd+IvQoHfBkwHXr/Q9Lp5op/ykCMZQXjkOgiBhKj6+aDB4NEvzVu/nZYkQ9bXb8ughjXAStrT3NKI007NICRlA8NI0h67u4/ycPoK7xpb0cHOJ+M0V3hr3dpN2j1LwCxKzA7FVlcybJvzahweRLc8m9t2TwTevNNFpUP9egRlJdFd49tu9s0O8wA66dez+9Oy117ZEvhoUAAczfzn0KxHRWn9t6KENgqV37s7zwMtUyccRpMqMitged8QtBrXcP1QjjeMDwJjegi+D/IjaDKH4MgF1npKdLIrmP5mPAUWsHA1M4qn2WijfcLfEgf8ROUKLzXsHJHFa3XrIR8QN08Lt6B9Q9FLqNfazj6tR5rAP6111GF1uLzFD8FOkpE2tQVER0aOla/nw4RxidHEybxsZ2htnr4WS3Gr7mTAbfEG8LFNwDeLn6YfUD9wajr/eYI3v6W5rcHX5AtZkMnFq7YbPPzB45hHKBlTpAkeTd6ewLOnTzVQSmfwxrYVV6lr3l8TCARGSLAG/T2O0a4GspKTqo/W4zUfKNiJqShVKkR+TYf+Pn/XZ0IhCuP+Ke9R4ScqdOibKUXbLsvjOZ8GC/ul9iLWE3CkExJ+eLKPXsESj7yCFuJXP1iDBOrf/HnGNv9SxX+kMOOuAz1yi0Sp4X9e60Df41PXDDzhfM+CASk6mG+GMpOD8DXlVSPBGrMouwPr6LQ/PlS+LcKYjMyVXDbRt2V/NGTjTj9ZWc8HcT/loIby5vzu56JVbVuemaukgiLv946xkXChAFqBgEMaqum5AE9hTPQ4ms7WV/EcdWOPTORCTdOMMbcCC4j5CRf5VidbtSXGeqtpe0qyeR0YQ4fEKYWnRSJUvxQYuptWrYatEUt9iOE9ifzF7Em+wG5aP6j5IlbDyr9NAhC8/29mbOiOkkWGXA4XDScXBJPjekaVMyvzM8e0Vb0kiAg0S00jpWIxFmK90ZCOP16xmcpKAR46eT8EoX08al6CU2lqr6wdNe2KAid/E68OzVTOGx800dnxWN4CtPRvmMx+6KbhVNkYfbeNDJL+GuG/IjydQm410FSEh4XBAd45oELaw4Gqia/VRFwSALByQti9eP/dCJ4zheS9AcSGjpK7PRXjrM8StAb0wK/9mmOrDJY9RfCIhNb6OjCvV/VmWbOOBu/m9Pjgh6cCitkxO/i8dLA+Hx8YofAWvuUMjZbtrlDiYXnbWRtCzXuJ6+JnT9c9wgh4Fc0jHIX4fvfac/O/W5E1MdxboRE6lXwNs7u81s+AmBSBXtqyprbuTWtYkhIzDy1CoqfUm6eSTqNU6JUTAYp0CPlFl5HZZZo/ox4RmZnBpI+0kdn5sQ3VXtvGha50MBsKKsQjwIWFBsJ/qpPIH6NCSGFN9Kwfz96+1oVlmPTwHCdXNzgHd03SCRrTmPc3+RRyWCvriAHNPzosYKg+AEL+TGxn/g/FN9kyYebX9CCXpLA6myRXYprKAuXdfks91nJUZGBmhSnWWNtwkz1uoo5OgpDx//E3D4gpqAAbMIWWybMoL1YVXNOXlwAAjEsIZwxCHgzvcPJ+ym5JGwJYJ3h2GhYvFO8ym9C0YdkasrzXZxMrK2kYmNM6bMWiFo6iosGky7SQyumUTiMEgtY9Dn5IBuCMW2p4Pmwv7nYgMygm2lUlbTiDWu7NK+9U2hyzDaf/FN6/RNHDTgYpsrA/vdbwNTdNYgXt5MKm072xHJKxxbKppI51JRJJBCIYEv0aRYYfUewk1wHSzaW4CZtAYf/Vk1TijkBspFU7TAelMKFq02fNzi61k7eS30q8w2Fvk/X/hDiAhLY73wgnJixs2Rob3O5DPY4SfjAzyAF8AMPDsu50VHlDl93w4EREyPHXZBCFsvkOpmo+8ZLtdGatPdtYjsD8R5XmZhgnW6cuU+MR1MnJU7fIjsbhO+W3EUBXoj0c5OZU+ZuC4yLhZyjy4tUCwy3uk49zXrCnEBxPMMvFgpw8cAac0+6DgquQk/WnjP1Y+l+vXKR8YIukAEkulProgyjJAwMkaGPxvyorlwPyjj5RWe/BZx3QdYMSHSzoht4EVO8MNOfzrBNs5wJA6VLav7ZwW2aISRc4H6hY9YLwiW13xLhzk8sfz/HQyLd4hlPgjESqVNEhz7BRRipZhq/3Ak/xXDFe0/4H0LcTHI/d67o4j83NsEVEXqkhXPihTbadTXmGDi8e8gm/oeCA73294G4nafBCRT04y7WVqHef0DwHNBblxQ5s9nsxzhiNNpAVzV2pUFGLORLTPB8DZMVrhdr/H17igLkzVrElAs+06WlMqwZxP1qXYGVp4w98rrMEA9xKOzFIYP8zEX6Np4YjkmPammgI/fk3I6uX4rcFNM7LzoGwFZebxpvXYqffkU4ijRRYg+drJv6G4k0CNbTh7yFaXo7MQ8m2zHLR0yQbUauKrW/mIuJf/0dmfX1vNafpPjbyJ16HG0MIU06cmJhhWyKd1th4ZqIChjXyty8vamDqAj0f3M4o/Kil+822Kk0SOtLR8eVeiThc1kUd/oB2Sq24dccpSuTecdxZJ13iH1I9wTcBROecTO4yRcmsThv8nJ44lT7LpcGUHFuEcqqZS/5yJMkGWK9uXfL1KsqYytnd7icjqjkmWBL5iHu+nAvnR5fD2IvNV22bstixFyTkUG1jmtaTqkkaHb72VvoKQpj4wBSLFhHQcRz/SvxWxr9s77vCGlagwrUzWMc+LIPyPRXNPuhrB4/mc9f2hj88p7cX+BPAAqmu7igs0A0BAlCywZc+ZHFuVMJy3+61XBwpmd8T3CzESPY63VX/uGdYHrPqQgdozl/XBfC0AOysQ/jKHTTpt89j58at1YV/dABIdycj3KYojF6EOJm7qK6Z3joiOP7F5xIYe17QVdlkfid/ttJ916/Ri89L2EIFX3/uKDy9bhWbqEjVRuqhy1eARMRu6Y5+b7rxtH/+qllVt+MjtWUvvjZQFEm/n3oRTH1uRIaVDWd9Jwv+fjbIhKZzI0T5iMFlAOQwufoeNk3QWXXTdCHYHc4wZgxJezFrvdPl5VaWbfuDm0aW9b8f0SRQjllJb4l1bQ4vy6nbK5szFIFXh7QuBqsmvWw7uIoVHaTh9wzrCUFOQk0okKO6GTfCvBzRUlzeBLF2U+ZUwETpRnIy91oJ48Pk2Psl78ssxjD69wn/j7M+Lq3QfXkLMjfNKmPKA7WKR/5c765FmICb8rJowgJh9ZYHUlz3Z8YmKV0to76w/78xUM112o0ZUSU/uzKtnc4JBTy0yEPNg1vdkDiKUS4rNoei8HiqJmiB9daImnOJz6pP4eUYjk/7mwW6EGnETnNDylnAIckC7vGCxXDV7SMbkCqSe4RIcPxAxOcHgcH4EOydsap1JD3BHbDqPOQR0BI+BWNDiHyvR7PCUXgJBNUZ+MJeWCL/Go7YPKfc92Ou7KL3FOaziix6F4K869bLYwNpDRTiWk4YjBjGCtMsPrTfd2j/M0nKJrjHE5SFRE448KgSSpoOmM5pLzviHHyfsVHWOKRRIQxhfv+SlHf4OBnMc+SEZhVj/SZ5PTA2LwDbL20xS5lGi48djLBL5pZcgrGqT7HlBJS1kH2TTigS6KoHrft68M7ZCTtRc6BiuyqcW2mRMFHrR27Lu6N/OEHSIfZkD1umG4F+N7Db/ysDjJJezQuQLQEfYI+wvcF1txV9L/bFSJmXnvLhnnaBhnzi8rZtW24Ci2DshmXbMHKBALOMQVPM3QS3HB6aB65zGvfndweoLLWGdI6CwTMrFqHoAHCOMxGtOuo+nSUOQayVTifst17cep127XS9EbI0QkTmVeSttWBINAQpzsWEBAuhq1XhAIzWXJtPtO34408u8e/mO/2Brj/dnKA0FULLRrMvCgSqXDswIsLcAxO84qDlbO2BKYuXCKwiZOGtD+JQU1KyDBEJyFijguT6+970HUSxddjvRQ5v+09H4YZ26xb6v5F0+uw6yU4+8IePwB/R6bIS2NiXdLe0a3POzL1AAAy/MVZ8BOAdZcgzblhz0QIkcxENudBSvBaEdLa1ASBZd+jnJBdKdSRZIqy5kHBOY0Rjl7RKhBFEcXyzrjWGNb35PmcK3DzUjejyrK8TAQcmIfs0OWGRoyF3jAj3avH1CDpVOz2tJdICyIBJthnJicMR4HshA8SW2QeL0TOmnGW2XYtJlzchCGILLQQYTLJ9H04PJF5r92wyK2XbrckS8IfSj8RX7IkwuLORHMIdT8QLSUFiyGq5cfUTGfnoQAUbhoRCRERb02me9uakJXWDADzf4/M9FC3NQ0lEPUer8Pw8m5lBTO/9zrpUjbfBtH5RlP4ii3XfvXKVH9cH7QSsxjFurSj2H6AZzc8NP+9nkX5U+kSGz5VxSBiZatXY8DFbuZMmiGHaMt4LO3AiUCMFw27MBbr0TUKP5qdkownhFwZUVtoMlg+V2YVEN7pdk0vhf/xS/yFTv+7MtMVWZKDu5VaLX2pHXMvqxSpggXnPKO5oDBLq7bg56yM/c0SfOaF8uE5UiitYIzzg+R7syHI5fegBhFrX48XGpN6gxOnr1F9jwbPP9Do18XyZHw/n5vB6x/gepXKZR1hlqBfTrh22cSMZPHU0+P7Rd8VqYhIaDsJ5AxXJpg5YxyfqYBuA9JgB8Ci2rV0rhlUEv15v0jWzBuybh2ijiPyq+m++r83ZXrvhkc7bJxGOGqCqJLrlj1LWZfDy2pJOFWwrogVnfSpl7Q53yiUnr98wvdNaYOyreDdxUdV2lG/N3gHcqP4tUJ33UzLig3zWe0+CNYrfX9NA8WSVaN8I0drR7Un97Q5adEnI3ImxkDTfr3+Q/xTiBOvOrhl239yFGmILGj+xVG+2PF2f8iennVnNvW1fzPWo8x9DbyDl5RACw6nvF3SpCFchGEEEbFyfPW7wDBbfiTZaYuCP6eRGPKyx4DbjGO0d3ukUIJSd9vmpAvqe1xwDy+CjpYvM4JVtd3lnRi2CcRkfkiKcWrb2c3goicaOptTfDSBilL5kgL7QWVbK5EZjxr6xmDYuH1rbJc3DnbXlaJV7wFQrhfu/m0uLfR5dSioOjcrkyVZZfP9uXdvjPfhB529tX1lVSryywMisziywpfWS5+TT7kb+MfmZo6/eWDfOgOTz+IQxi1BGOuFFiS9eHj0jaCDDU3ajPo5Zyx/QlAwjfZLgj1JjKRg2NDYeYr477kTuj8JuE3iLof6AwOdltmu1D3Sb1eJx/w==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['ctl02'];
if (!theForm) {
    theForm = document.ctl02;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>



<script src="/ScriptResource.axd?d=mkHh3H7FZVas5l3AY3JDjCg9qnZ6Lb2wLV1_rWuEpvfvI6N4qrkOmEYq3hTdpPoCSuUzVMnmNchfaIeSBYNNoviW0uarjeloz4pIJf5mircEvCJMsOI5UUcr7AA1im6aeltFYcyDISqK1tAaMG6J4feLMZ7vWOUm24QyiaD0xbgdIhWSUyA2BPDPY4SMr4arWD0xqqB8KTbjdqWP3adi9gnthkSJe1_HAV9wcja_EvKFX-Gs0" type="text/javascript"></script>
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="B9C9B541" />
</div>
       
<script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager1', 'ctl02', [], [], [], 90, '');
//]]>
</script>
 
   
    
<aside class="adp-site-menu-2015 side-bar adp-nav-2015">
    <a class="close-site-menu" href="javascript:void(0);">
        <img src="/ADP%20USA%202015/images/GlobalNav/close_x.png" />
    </a>
    <nav>
                
    
    
    
    
    <ul class='main-link bu-nav'>
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_0_hlItemLink_0" href="/solutions/small-business.aspx">Small Business<span>1 – 49 employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_1_hlItemLink_1" href="/solutions/midsized-business.aspx">Midsized Business<span>50 – 999 employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_2_hlItemLink_2" href="/solutions/large-business.aspx">Large Business<span>1,000+ employees</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_3_hlItemLink_3" href="/solutions/multinational-business.aspx">Multinational Business<span>of any size</span></a>
        
    

        </li>
        
        
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_0_adpInnerMenuItem_4_hlItemLink_4" href="/partners.aspx">Partner Solutions<span>Accountants, advisors, and more</span></a>
        
    

        </li>
        
        </ul>
    
    
        
    
    
    
    
    <ul class='corp-nav'>
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_0_hlItemLink_0" href="/">Home</a>
        
    

        </li>
        
        
        <li class='NoHighlight' >
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_1_hlItemLink_1" href="/solutions.aspx">Services & Products</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_2_hlItemLink_2" href="/why-adp.aspx">Why ADP</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_3_hlItemLink_3" href="/tools-and-resources.aspx">Insights & Resources</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_4_hlItemLink_4" href="/contact-us/customer-service.aspx">Contact & Support</a>
        
    

        </li>
        
        
        <li>
        

        <a id="_fc7f4297fcf53c_ctl00_ctl00_rptStickyMenu_rptMenu_1_adpInnerMenuItem_5_hlItemLink_5" href="/logins.aspx">User Logins</a>
        
    

        </li>
        
        </ul>
    
    

    
    </nav>
</aside>


<header class="adp-site-menu-2015 gray-bar adp-nav-2015">
    <nav class="clearfix nav-container">
        
        <a class="open-site-menu" href="javascript:void(0);"><img alt="" src="/ADP%20USA%202015/images/GlobalNav/icon_hamburger.png" />Site Menu</a>
        
        <ul class="corp-links">
                    
    <li class='region-selector' >
    

        <a id="_fc7f4297fcf53c_ctl01_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0"><span class="flag-icon flag-icon-us"></span> United States<ul class="region-options">
    <p class="region-group">ADP Websites</p>
    <li class="region-option">
    <a href="http://www.adp-ar.com" class="region-link"><span class="flag-icon flag-icon-ar"></span> Argentina </a> </li>
    <li class="region-option">
    <a href="http://www.adppayroll.com.au/" class="region-link"><span class="flag-icon flag-icon-au"></span> Australia </a> </li>
    <li class="region-option">
    <a href="http://www.adp.com.br/" class="region-link"><span class="flag-icon flag-icon-br"></span> Brazil </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ca/en-ca/default.aspx" class="region-link"><span class="flag-icon flag-icon-ca"></span> Canada (English) </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ca/fr-ca/default.aspx" class="region-link"><span class="flag-icon flag-icon-ca"></span> Canada (French) </a> </li>
    <li class="region-option">
    <a href="http://adp.cl" class="region-link"><span class="flag-icon flag-icon-cl"></span>Chile </a> </li>
    <li class="region-option">
    <a href="http://www.adpchina.com/" class="region-link"><span class="flag-icon flag-icon-cn"></span> China </a> </li>
    <li class="region-option">
    <a href="http://www.fr.adp.com/" class="region-link"><span class="flag-icon flag-icon-fr"></span> France </a> </li>
    <li class="region-option">
    <a href="http://www.de-adp.com/" class="region-link"><span class="flag-icon flag-icon-de"></span> Germany </a> </li>
   <li class="region-option">
    <a href="http://www.adp.com.hk/" class="region-link"><span class="flag-icon flag-icon-hk"></span> Hong Kong </a> </li>       
    <li class="region-option">
    <a href="http://www.adp.in/" class="region-link"><span class="flag-icon flag-icon-in"></span> India </a> </li>
    <li class="region-option">
    <a href="http://www.it-adp.com/" class="region-link"><span class="flag-icon flag-icon-it"></span> Italy </a> </li>
    <li class="region-option">
    <a href="http://www.adp.nl/" class="region-link"><span class="flag-icon flag-icon-nl"></span> Netherlands </a> </li>
    <li class="region-option">
    <a href="http://www.adp.pe/" class="region-link"><span class="flag-icon flag-icon-pe"></span> Peru </a> </li>
     <li class="region-option">
    <a href="http://www.adp.ph/" class="region-link"><span class="flag-icon flag-icon-ph"></span> Philippines</a> </li>
    <li class="region-option">
    <a href="http://www.adp.pl/" class="region-link"><span class="flag-icon flag-icon-pl"></span> Poland </a> </li>
    <li class="region-option">
    <a href="http://www.adp.sg/" class="region-link"><span class="flag-icon flag-icon-sg"></span> Singapore </a> </li>
    <li class="region-option">
    <a href="http://www.es-adp.com/" class="region-link"><span class="flag-icon flag-icon-es"></span> Spain </a> </li>
    <li class="region-option">
    <a href="http://www.adp.ch/" class="region-link"><span class="flag-icon flag-icon-ch"></span> Switzerland </a> </li>
    <li class="region-option">
    <a href="http://www.adp.co.uk/" class="region-link"><span class="flag-icon flag-icon-gb"></span> United Kingdom </a> </li>
    <li class="region-option">
    <a href="https://www.adp.com/" class="region-link current-selection"><span class="flag-icon flag-icon-us"></span> United States*</a> </li>
    <li>
    <p class="foot-note">*Corporate Headquarters</p>
    </li>
    <p class="region-group">ADP Worldwide</p>
    <p>We provide payroll and HR services in more than 104 countries.</p>
    <a class="btn" href="/worldwide.aspx">View All Locations</a>
</ul></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/worldwide.aspx">Worldwide Locations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/about-adp.aspx">About ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="http://investors.adp.com/">Investor Relations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="http://mediacenter.adp.com/">Media Center</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl00_rptStickyMenu_adpMenuItem_5_hlItemLink_5" href="/careers.aspx">Careers</a>
        
    

    
    
    
    
    </li>
        
    <li class='utility-search' >
    

        <a id="_fc7f4297fcf53c_ctl01_ctl00_rptStickyMenu_adpMenuItem_6_hlItemLink_6" href="/site-search.aspx">Search <span class="search-icon"></span></a>
        
    

    
    
    
    
    </li>

    
        </ul>
        <div class="global-login-search">
            <div class="global-search">    
                <input type="image" alt="Search" src="/ADP%20USA%202015/images/GlobalNav/icon_search.png" id="site-search-box">    
                <input type="text" placeholder="Search" id="site-search">
            </div>
  
            <div class="login-wrap">
                <a class="btn btn-adp-red" href="/logins.aspx" id="user-logins">User Logins</a>
    
                <!-- Unsegmented -->
                <div class="login-flyout">
                    <div id="divRecentLogins"></div>
                    
                    <ul>
                        <li class="title">Popular Logins</li>          
                                
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl01_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/logins/adp-portal.aspx">ADP Portal</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl01_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/logins/my-adp.aspx">My ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl01_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/logins/run-powered-by-adp.aspx">RUN Powered By ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl01_ctl01_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/logins/ezlabormanager.aspx">ezLaborManager</a>
        
    

    
    
    
    
    </li>

    
                    </ul>
                    <a href="/logins.aspx">View All User Logins</a>
                </div>
            </div>
        </div>
    </nav>
</header>
    

<header class="adp-nav-2015 corp-nav">
    <div class="nav-container">
        <a class="adp-logo" href="/">
<img alt="" src="/-/media/USA-2015/Global-Header/logo_ADP_red.ashx?la=en&amp;hash=62C692F1D05DA5BDB8982DC63E16E9077954C906" />
</a>
        <nav class="nav-links">
            <ul>
                        
    <li class='submenu-trigger' >
    

        <a id="_fc7f4297fcf53c_ctl02_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/solutions.aspx"><span>Services & Products</span></a>
        <div id="services-products" class="flyout-menu">
<div class="segments">
<p>Solutions by Business Size</p>
<ul class="bus">
    <li>
    <a href="/solutions/small-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_SmallBusiness.ashx?la=en&amp;hash=FE476C539A79B9D58A4DA491893D102351B8AC4D" />
    <p class="name arrow-link">Small Business</p>
    <p class="range">1 &ndash; 49 employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/midsized-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_MidsizedBusiness.ashx?la=en&amp;hash=E7A0310BDF33BADC50283F20D50E4FE69D55151F" />
    <p class="name arrow-link">Midsized Business</p>
    <p class="range">50 &ndash; 999 employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/large-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_LargeBusiness.ashx?la=en&amp;hash=E6C57034D093D200757DBF55BF8978E2AA7F2EFE" />
    <p class="name arrow-link">Large Business</p>
    <p class="range">1,000+ employees</p>
    </a>
    </li>
    <li>
    <a href="/solutions/multinational-business.aspx"><img alt="" src="/-/media/USA-2015/Global-Header/icon_MultinationalBusiness.ashx?la=en&amp;hash=07FC4D8C4F3E8ADC2F3DDC96DB7E3BCDF0818095" />
    <p class="name arrow-link">Multinational Business</p>
    <p class="range">of any size</p>
    </a>
    </li>
</ul>
</div>
<div class="link-lists clearfix">
<nav class="sub-list">
<p >What We Do</p>
<ul>
    <li><a href="/solutions/services/human-capital-management.aspx">Human Capital Management</a></li>
    <li><a href="/solutions/services/payroll-services.aspx">Payroll Services</a></li>
    <li><a href="/solutions/services/talent-management.aspx">Talent Management</a></li>
    <li><a href="/solutions/services/human-resources-management.aspx">HR Management</a></li>
    <li><a href="/solutions/services/health-care-reform-management.aspx">Affordable Care Act</a></li>
    <li><a href="/solutions/services/benefits-administration.aspx">Benefits Administration</a></li>
    <li><a href="/solutions/services/time-and-attendance.aspx">Time &amp; Attendance</a></li>
    <li><a href="/solutions/services/hr-business-process-outsourcing.aspx">HR Business Process Outsourcing (HR BPO)</a></li>
    <li><a href="/solutions/services/professional-employer-organization.aspx">Professional Employer Organization (PEO)</a></li>
    <li><a href="/solutions/employer-services/retirement-services.aspx">Retirement Plans</a></li>
    <li><a href="/solutions/employer-services/insurance-services.aspx">Insurance Plans</a></li>
    <li><a href="/solutions/employer-services/tax-and-compliance.aspx">Tax &amp; Compliance</a></li>
    <li><a href="/solutions/services/payment-services.aspx">Payment Solutions</a></li>
</ul>
</nav>
<nav class="sub-list">
<p class="arrow-link"><a href="/our-products.aspx">Our Premier Products</a></p>
<ul>
<li>Explore our <a href="/our-products.aspx">full range of payroll and HR solutions</a>, and discover what's right for you.</li>
</ul>
</nav>

</div>
</div>
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl02_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/partners.aspx"><span>Partner Solutions</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl02_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/why-adp.aspx"><span>Why ADP</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl02_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/tools-and-resources.aspx"><span>Insights</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_fc7f4297fcf53c_ctl02_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/contact-us/customer-service.aspx"><span>Contact & Support</span></a>
        
    

    
    
    
    
    </li>

    
            </ul>
        </nav>
    </div>
</header>

 
    
<div id="ADP_651578EE-7D98-4EEB-99F7-6D7FC542F4D6" data-source-title='Privacy Thin Contact Hero'  data-source-id='5CA9C26A-AF1E-4F27-928C-D157628DA48B'    class='hero-wrap thin-hero-wrap bg-image bg-image'  style='background-image:url("/-/media/USA-2015/Thin-Heroes/bkgd_hero_privacy-terms.ashx");'  >
    <section  class='thin-hero' >
        <h1>ADP Online Privacy</h1>
        
        
    </section>
</div>
<div id="ADP_C2699CF7-50C4-4074-B013-00052C53117E" data-source-title='Additional Privacy Text'  data-source-id='9911B708-5B11-4D80-93FE-AA23E9C133FA'    class='section-wrap pad-top-40'  >
    <section  class='width970 clearfix' >
        <!-- <p>
For information on how ADP protects your personal information online, please see the <a href="/privacy/privacy-statement.aspx">ADP Online Privacy Statement</a>.
</p> -->

        
        
    </section>
</div>
<div id="ADP_9052E5A5-A9FC-4AC1-A66B-E3BA7DFD35D2"   class='section-wrap pad-btm-100' >
    <section id="sitemap-root"  class='width970 clearfix privacystatement' >
    
    </section>
</div>
    
	<!-- Footer Area -->
    

<footer class="adp-nav-2015 global-footer">
  <div class="nav-container">
    <nav class="bu-footer-nav">
      <ul>
                
    <li>
    

        <a id="_10aee027f9b1c0d_ctl00_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/solutions/small-business.aspx">Small Business<span>1 – 49 employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl00_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/solutions/midsized-business.aspx">Midsized Business<span>50 – 999 employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl00_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="/solutions/large-business.aspx">Large Business<span>1,000+ employees</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl00_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="/solutions/multinational-business.aspx">Multinational Business<span>of any size</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl00_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/partners.aspx">Partner Solutions<span>Accountants & more</span></a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl00_rptStickyMenu_adpMenuItem_5_hlItemLink_5" href="/why-adp.aspx">Why ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl00_rptStickyMenu_adpMenuItem_6_hlItemLink_6" href="/tools-and-resources.aspx">Insights</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl00_rptStickyMenu_adpMenuItem_7_hlItemLink_7" href="/contact-us/customer-service.aspx">Contact & Support</a>
        
    

    
    
    
    
    </li>

    
      </ul>
    </nav>

    <nav class="corp-footer-nav clearfix">
      <div class="adp-logo">
        <a href="/">
            <img src="/-/media/USA-2015/Global-Footer/logo_ADP_white.ashx?h=29&amp;la=en&amp;w=64&amp;hash=CCF13E9152A6CB1DE432BDD08D0CEB5DA63CE56B" alt="ADP" />
        </a>
      </div>
      <ul>
                
    <li>
    

        <a id="_10aee027f9b1c0d_ctl01_rptStickyMenu_adpMenuItem_0_hlItemLink_0" href="/about-adp.aspx">About ADP</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl01_rptStickyMenu_adpMenuItem_1_hlItemLink_1" href="/worldwide.aspx">Worldwide Locations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl01_rptStickyMenu_adpMenuItem_2_hlItemLink_2" href="http://investors.adp.com/">Investor Relations</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl01_rptStickyMenu_adpMenuItem_3_hlItemLink_3" href="http://mediacenter.adp.com/">Media Center</a>
        
    

    
    
    
    
    </li>
        
    <li>
    

        <a id="_10aee027f9b1c0d_ctl01_rptStickyMenu_adpMenuItem_4_hlItemLink_4" href="/careers.aspx">Careers</a>
        
    

    
    
    
    
    </li>

    
      </ul>
      <div class="legal">
        <style type="text/css">
.adp-nav-2015.global-footer nav.corp-footer-nav {  width: 70%; }
</style>

<!-- <p>ADP and the ADP logo are registered trademarks of ADP, LLC. <br />Copyright © <script type="text/javascript">var d=new Date(); document.write(d.getFullYear());</script> ADP, LLC ALL RIGHTS RESERVED.</p> -->

<p>ADP, the ADP logo and ADP A more human resource are registered trademarks of ADP, LLC.  <br />All other marks are the property of their respective owners.  Copyright © <script type="text/javascript">var d=new Date(); document.write(d.getFullYear());</script> ADP, LLC.</p>

<!-- <p>Copyright © ADP, LLC ALL RIGHTS RESERVED.
<a href="/privacy.aspx">Privacy</a>
<a href="/legal.aspx">Terms</a>
<a href="/site-map.aspx">Site Map</a>
</p> -->

<p>
<a href="/privacy.aspx" style="margin-left:0;">Privacy</a>
<a href="/legal.aspx">Terms</a>
<a href="/site-map.aspx">Site Map</a>
<a href="/modern-slavery-statement.aspx">Modern Slavery Statement</a>
</p>
      </div>
    </nav>

    <div class="right-panel">
      <p class="sales"><span>Sales:</span> <span class="tel">800-225-5237</span></p>

<a href="/connect-with-adp.aspx" class="social"><span>Social@ADP</span> <img alt="" style="width: 13px; height: 13px;" src="/-/media/USA-2015/Global-Footer/SocialADP_LinkedIn.ashx?la=en&amp;hash=A74727738486FA5362680DC99A82C9E32AEAF2A0"> <img alt="" style="width: 15px; height: 10px; margin-top: -2px;" src="/-/media/USA-2015/Global-Footer/SocialADP_YouTube.ashx?la=en&amp;hash=CF3874E1EEFE2B21B594D42D7B4EC8CFA7F179DF" > <img alt="" style="width: 15px; height: 12px;" src="/-/media/USA-2015/Global-Footer/SocialADP_Twitter.ashx?la=en&amp;hash=D881BD2C45EA41752BE8B3073E3018B2572ACC36"> </a>
<a href="/logins.aspx" class="user-logins btn btn-adp-red">User Logins</a>
    </div>
  </div>
</footer>
  
    <!-- end Footer Area --> 
   
    
<!--Main Include Start-->
<!-- 3rd Party -->
<script type="text/javascript" src="/elqNow/Global/elqCfg.js"></script>

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 1018590256;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>

<noscript>
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1018590256/?value=0&amp;guid=ON&amp;script=0"/>
</noscript>

<!--stop unwanted window scrolling behavior-->
<script type="text/javascript">
    window.scrollTo = function () { }
</script>

<script src="//cdn.optimizely.com/js/198303326.js"></script>
<!-- <script src="//cdn.optimizely.com/js/8167740194.js"></script> -->

<!-- 1st Party -->
<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "WebSite",
    "url": "https://www.adp.com/",
    "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.adp.com/site-search.aspx?q={search_term}",
        "query-input": "required name=search_term"
    }
}
</script>

<script src="/ADP%20USA%202015/js/common-footer-preset.min.js"></script>
<!--Main Include end, Secondary Include Start-->

<script type='text/javascript'>var settings = {
  "XmlUrl": "https://www.adp.com/privacy/intl/Intl_Privacy1.xml",
  "StartNodeXpath": "/Privacy/PrivacyStatements/English",
  "DisplayTitle": "1"
};</script><script type='text/javascript' src='/ADP%20USA%202015/js/privacystatements.js'></script>
<script type='text/javascript' src='/adp%20usa%202015/js/sitemap.js'></script>
<!--Secondary Include End-->    
    
    </form>
</body>
</html>
