<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<style type="text/css">
@import "styl.css";
</style>
<title>Planetarium Football Star – Become a World Star Player!</title>
<META HTTP-EQUIV="Expires" CONTENT="Mon, 06 Jan 1990 00:00:01 GMT">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="language" content="en">
<meta name="keywords" content="football, online, games, game, mmo, massively multiplayer online, sport, soccer, free, manager, play, virtual, football manager online, social football game, become a star, football star, online football manager">
<meta name="description" content="Football Star is a free online football simulator where you are the player! Join a club, set up trainings, interact with your team mates, give interviews, become the best football player in the world!">

<script	type="text/javascript">

  
</script>
</head>
<body>
  <meta name="google-site-verification" content="ZokmOn0IQwi1CTeEM8zvQm8paSkK1odHUZugZsxQ0y4" />
<link rel="shortcut icon" href="favicon.ico">
<script>
   if (top === self) {
        if (window.location.host === "fb.footstar.org") {
        	//alert(window.location.pathname + window.location.search + window.location.hash);
            top.location.href = "https://apps.facebook.com/pgfootstar" + window.location.pathname + window.location.search + window.location.hash;
        }
    }
</script>


<style>
body{
	background: #29435c url(img/stadium02.jpg) fixed center 15% no-repeat;
	background-position: center top; 
	font-family: Verdana,Arial,Helvetica,sans-serif;
	font-size:11px;
	color:#000;
	margin: -1px 0;
}
</style>

<script type="text/javascript" src="include_files/query1.5.2.min.js"></script>
<script type="text/javascript" src="include_files/jquery-ui.1.8.11.min.js"></script>

<script type="text/javascript" src="include_files/jquery.functions.min.js"></script>

<script type="text/javascript" src="include_files/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="include_files/promise.js"></script>
<script type="text/javascript">window.google_analytics_uacct = "UA-747112-1";</script>
<script language="JavaScript" type="text/javascript">
  //GOOGLE ANALYTICS
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-747112-1']);
  _gaq.push(['_setDomainName', '.footstar.org']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_setCustomVar',
      1,                // This custom var is set to slot #1.  Required parameter.
      'Pagante',    // The name of the custom variable.  Required parameter.
      '',        // The value of the custom variable.  Required parameter.
                        //  (possible values might be Free, Bronze, Gold, and Platinum)
      1                 // Sets the scope to visitor-level.  Optional parameter.
 ]); 
 
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  
var digital = new Date( "Jan, 01 2018 00:01:40");
$(document).ready(function() {
	
        var destination = $('#main_table').offset();
        //console.log($('#main_table').width());
        $('#banner_lateral_right_small').css({left: $('#main_table').width() + destination.left + 10});
        $('#banner_lateral_left').css({left: destination.left - $('#banner_lateral_left').width() - 10});
	
});
</script>
<style>
#nav_top_bar {
	z-index:5000;
	position:fixed;
	top:0%;
	left:50%;
	margin-left:-450px;
}
#main_table {
	margin-top:23px;
	
		margin-top: 0\9;
	
	position:relative;
	width:900px;
}

.pgames-bar {	
	top: 0;
	left: 0;
	z-index: 1000;
	height: 25px;
	width: 100%;
	position: fixed;
	position: inherit\9;
	background: rgb(255,255,255); /* Old browsers */  
	background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(167,228,232,1) 100%); /* FF3.6+ */  
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(100%,rgba(167,228,232,1))); /* Chrome,Safari4+ */  
	background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(167,228,232,1) 100%); /* Chrome10+,Safari5.1+ */  
	background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(167,228,232,1) 100%); /* Opera 11.10+ */  
	background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(167,228,232,1) 100%); /* IE10+ */  
	background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(167,228,232,1) 100%); /* W3C */  
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#a7e4e8',GradientType=0 ); /* IE6-9 */;
}

.link-pgames{
	width: 185px;
        /*display: inline-block;*/
        float: left;
        line-height: 25px;
        text-indent: -10000px;
}

.pgames-container {
	max-width: 775px;
	width: 80%;
	min-width: 340px;
	background: url('/img/logo_pgames.png') left center no-repeat;margin: 0 auto;height: 100%;
}

.pgames-bar ul{
	
	margin: 0 0 0 10px;
	padding: 0;
	/*display: inline-block;*/
 	float: left;
        line-height: 25px;
        /*        margin: 0 auto;
                width: 200px;*/
        /*height: 200px;*/
        z-index: 2000;
}

.pgames-bar ul li {
 	display: inline-block;
        color: #0B6699;
        width: 65px;
        text-align: center;
        position: relative;
        cursor: pointer;
         z-index: 2000;
         background: url('/img/1.png');
         margin: 0 1px;
}

.pgames-bar ul li:hover .game-details{ display: block; }
.pgames-bar ul li:hover{ border-left: 1px solid;border-right: 1px solid;box-shadow: 0 0 5px 1px; margin: 0;}

.game-details{
  	width: 300px;
        background-color: #102E3F;
        padding: 10px;
        border: 2px solid #ABE5EA;
        margin-top: -3px;
        border-radius: 0 0 3px 3px;
        position: absolute;
        -webkit-box-shadow: 0 6px 5px 1px black;
        -moz-box-shadow: 0 6px 5px 1px black;
        box-shadow: 0 6px 5px 1px black;
/*        left: 0;
        top:20px;*/
/*top: 20px;*/
        display: none;
        z-index: 2001;
        right: 0;
}

.game-img {
  	float: left; 
        height: 114px;
        width: 167px;
        border: 1px solid #2C6A8E;
}

.bar-game-info {
        float: right;
        width: 120px;
        line-height: 15px;
        padding-top: 50px;
        text-align: center;
}

.bar-game-info p {color: #90DDE2;font-size: 11px;margin: 0;}

.bar-bitclubs-logo {background: url('/img/logo_bitclubs.png') top center no-repeat;}
.bar-bitclubs {background: url('/img/img_bitclubs.png') no-repeat;}

.bar-footstar-logo {background: url('/img/logo_footstar.png') top center no-repeat;}
.bar-footstar {background: url('/img/img_footstar.png') no-repeat;}

.bar-pmanager-logo {background: url('/img/logo_pmanager.png') top center no-repeat;padding-top: 30px;}
.bar-pmanager {background: url('/img/img_pmanager.png') no-repeat;}

.bar-tribalbands-logo {background: url('/img/logo_tribalbands.png') top center no-repeat;}
.bar-tribalbands {background: url('/img/img_tribalbands.png') no-repeat;}

.orange {
	background: #FFB700;
	background: -moz-linear-gradient(top, rgba(255, 183, 0, 1) 0%, rgba(255, 140, 0, 1) 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255, 183, 0, 1)), color-stop(100%,rgba(255, 140, 0, 1)));
	background: -webkit-linear-gradient(top, rgba(255, 183, 0, 1) 0%,rgba(255, 140, 0, 1) 100%);
	background: -o-linear-gradient(top, rgba(255, 183, 0, 1) 0%,rgba(255, 140, 0, 1) 100%);
	background: -ms-linear-gradient(top, rgba(255, 183, 0, 1) 0%,rgba(255, 140, 0, 1) 100%);
	background: linear-gradient(to bottom, rgba(255, 183, 0, 1) 0%,rgba(255, 140, 0, 1) 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffb700', endColorstr='#ff8c00',GradientType=0 );
	border: 1px solid #E59500;
}

.button, .btn-border {
	border-radius: 2px;
	box-shadow: inset rgba(255, 255, 255, 0.3) 1px 1px 0;
}
.button, .button-bevel {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: white;
	text-decoration: none;
	display: inline-block;
	text-align: center;
	padding: 7px 20px 9px;
	margin: .5em .5em .5em 0;
	cursor: pointer;
	text-shadow: 0 1px 1px rgba(0, 0, 0, 0.4);
	-webkit-transition: 0.1s linear;
	-moz-transition: 0.1s linear;
	-ms-transition: 0.1s linear;
	-o-transition: 0.1s linear;
	transition: 0.1s linear;
}
</style>
<!--[if IE 7]>
<style>
#nav_top_bar {
	position:static;
	top:0%;
	left:00%;
	margin-left:0px;
}
#main_table {
	margin-top:0px;
	position:relative;
	width:900px;
}
</style>
<![endif]-->	

<!-- START PGAMES BAR -->

	<div class="pgames-bar">
	    <div class="pgames-container">
		<a href="http://planetariumgames.com" class="link-pgames">Planetarium Games</a>
		<ul>
		    <li>
			<img src="/img/icon_pmanager.png" style="border: 0;-ms-interpolation-mode: bicubic;vertical-align: middle;padding: 5px;">
			<div class="game-details" style="margin-top: -1px;">
			    <div class="game-img bar-pmanager"></div>
			    <div class="bar-game-info bar-pmanager-logo">
				<p>The game of smart Football Management.</p>
				<div class="button orange" style="padding: 3px 10px 4px;"><a style="text-decoration: none;color: white;" href="http://www.pmanager.org/registar.asp?action=1&amp;f=2&amp;ref=0">Play now!</a></div>
			    </div>
			</div>       
		    </li>
		    <li>
			<img src="/img/icon_bitclubs.png" style="border: 0;-ms-interpolation-mode: bicubic;vertical-align: middle;padding: 6px 5px;">
			<div class="game-details">
			    <div class="game-img bar-bitclubs"></div>
			    <div class="bar-game-info bar-bitclubs-logo">
				<p>Build your own sports empire and be boss.</p>
				<div class="button orange" style="padding: 3px 10px 4px;"><a style="text-decoration: none;color: white;" href="http://bitclubs.com">Play now!</a></div>
			    </div>
			</div>
		    </li>
		    </li>
		    <!--<li>
			 <span style='font-weight: bold;color: red;'>NEW!</span>
			<img src="http://planetariumgames.com/images/pgamesbar/icon_tribalbands.png" style="border: 0;-ms-interpolation-mode: bicubic;vertical-align: middle;padding: 6px 5px;">
			<div class="game-details">
			    <div class="game-img bar-tribalbands"></div>
			    <div class="bar-game-info bar-tribalbands-logo">
				<p>Gather a band and lead your music tribe!</p>
				<div class="button orange" style="padding: 3px 10px 4px;"><a style="text-decoration: none;color: white;" href="http://tribal-bands.com">Play now!</a></div>
			    </div>
			</div>
		    </li>-->
		</ul>
	    </div>
	</div>

<!-- END PGAMES BAR -->


<!-- MAIN TABLE -->
<div style='width:900px;margin:0 auto;position:relative;-moz-border-radius:0 1px;'>
<table border="0" align=center cellpadding="0" cellspacing="0" id='main_table'>	
  <tr>
   
   		<td style="background-repeat:no-repeat;" background="img/intro_header1.png" width="900" height="104">
   			<table width=100% cellpadding="0" cellspacing="0">
   				
   					<tr><td width='450'><a href=default.asp><img border=0 src=img/intro_logo.png width=309 class=img-logo></a></td><td valign='top'>
				<div id='btn-fb' style='border-color:black;border-top:0px;margin-left:273px;margin-top:0px;padding:5px;width:140px;font-size:10px;float:left;-moz-border-radius-topleft: 0px;-moz-border-radius-topright: 0px;-moz-border-radius-bottomright: 3px;-moz-border-radius-bottomleft: 3px;-webkit-border-radius: 0px 0px 3px 3px;border-radius: 0px 0px 3px 3px;' onClick="parent.location='https://www.facebook.com/v2.10/dialog/oauth?client_id=172369456166690&redirect_uri=https://www.footstar.org/facebook_login.asp&scope=email'"><div id='btn-bfbt' style='top:5px;'></div>Login with Facebook</div> 
				<!--<div class='btn-login' style='border-color:black;border-top:0px;border-left:0px;right:0px;margin-top:0px;width:80px;font-size:10px;float:left;text-align:center;padding:7px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 0px;-moz-border-radius-bottomright: 3px;-moz-border-radius-bottomleft: 0px;-webkit-border-radius: 0px 0px 3px 0px;border-radius: 0px 0px 3px 0px;' id='join_button'>Login</div>-->
						
								
						
					<div id='login_box' style='display:none;padding:5px;position:absolute;right:24px;top:30px;background-color:#e6eaed;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;border:1px solid #666;color:#666;'>
					<form method="post" id='login_form' action="default.asp?action=login">
					<table width="60%" cellpadding='0' border="0" style='padding-top:10px;color:black;font-size:11px;'>
					  <tr>
						<td>User</td>
						<td>Password</td>
						</tr>
						<tr>
						<td><input style='border:1px solid black;width:120px;height:15px;font-size:12px;' type="text" name="utilizador" id="utilizador" maxlength=50 size="15" onkeydown="pwd_focus(event)"></td>
						<td><input style='border:1px solid black;width:120px;height:15px;font-size:12px;' type="password" name="password" id="password" maxlength=50 size="15" onkeydown="sub_focus(event)"></td>
						<td>
						<input id='loginSub' name='loginSub' type=submit value='Login'>
						</td>
					  </tr>
					  <tr><td>&nbsp;</td><td colspan='2'><a style='color:#666;'href='https://www.footstar.org/registar.asp?action=3'>Lost your Password?</a></td></tr>
						</table>
					</form>
					<div>
			</td></tr>
   				
   			</table>
   		</td>
   	 
 </tr>
</table>
</div>

<table border="0" align=center cellpadding="0" cellspacing="0" width="900">
	<tr><td style="background-repeat:no-repeat;" background="img/intro_header2.png" width="900" height="29" valign='bottom'>
<!-- Menu de Topo -->
<div id='globalnav'></div></td></tr>
</table>

<table border="0" align=center cellpadding="0" cellspacing="0" width="900"><tr>
   
  </tr>
  <tr> 
  	<td style='background-color:#e9ecee;' width='900'>
  
  		<div id='subnav' style='height:34px;line-height:34px;padding-right:10px;border-left:1px #000 solid;border-right:1px #000 solid;width:888px;text-align:right;'><b>Players: 12.465 - Online now: 56 - New today: 0</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id='mudar_idioma'><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=EN' SELECTED>English</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=PT' >Português</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=ES' >Español</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=FR' >Français</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=NL' >Nederlands</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=BR' >Português, Brasil</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=IT' >Italiano</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=DK' >Dansk</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=RO' >Româna</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=LT' >Lietuviu</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=HE' >עברית</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=RU' >Русский</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=HU' >Magyar</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=CA' >Catalan</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=FI' >Suomi</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=PL' >Polski</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=NO' >Norsk</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=TR' >Türkçe</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=BG' >Български</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=SE' >Svenska</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=AL' >Deutsch</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=HR' >Hrvatski</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=GR' >Ελληνικά</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=BE' >Vlaams</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=SI' >Slovenščina</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=SK' >Slovenčina</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=CN' >中文(简体)</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=BA' >Bosanski</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=GA' >Galego</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=UA' >Українська мова</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=ARA' >العربية</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=CZ' >čeština</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=ID' >Bahasa Indonesia</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=ARG' >Español, Argentina</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=BY' >Беларускі</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=UY' >Español, Uruguay</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=VE' >Español, Venezuela</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=CL' >Español, Chile</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=CO' >Español, Colombia</option><option value='https://www.footstar.org/default.asp?action=idioma&linguagem=MX' >Español, México</option></select></div><script>$("#mudar_idioma").sortOptions();</script>
		
		
   </td>
  </tr>
</table>
  <table border="0" align=center cellpadding="0" cellspacing="0" width="900">
  <tr>
   
		<td style='position:static;background-color:white;border-left:1px solid black;border-right:1px solid black;width:900px;min-height:414px; max-width: 900px;' valign=top>


<div style='border:0px solid black;position:absolute;'><div id='forum_loader' style='display:none;margin:5px;margin-left:300px;color:#666;border:0px dotted #666;'><img src='img/17-0.gif'>&nbsp;<b>Loading...</b></div></div>



<script>
$(function() {		
	
	$interactions_ttip = $("#interactions_ttip");
	
	$interactions_ttip.tooltip({	
		tip: '#new_ints',
		position: "bottom center",
		//left: 680,
		//top: 168,
		
		offset:[-18,-175],
		
		delay:250,
		predelay:250,
		events: {
			interactions: 'click, mouseout'
		}
	});
	
	$interactions_ttip.live("click", function(){	
		$.ajax({
			type: "GET",
			async: true,
			url: "meu_inicio.asp",
			data: "read=1",
			complete: function(){
				$("#interactions_ttip").attr("src", "img/button-wall-grey.png");
				$("#number_ints").css("display", "none");
			}
		});
	});
});
</script>

      <table width="98%" cellpadding="0" cellspacing="0" border=0 align=center>
          <tr>
                      <td class="arial" valign="top" style='padding-top:25px;'>
               <!-- Conteudo -->

				<table width=90% align=center class=comentarios><tr><td>

<h1>
            Privacy statement</h1>
        <p>
            Planetarium Games Ltd, a company with its location registered at Porto, Portugal, publisher of the website Footstar.org and the Footstar app (available from the Apple App store and the Google Play store) has created this privacy statement in order to comply with the prevailing data protection laws and demonstrate our firm commitment to privacy.
        </p>
        <p>
            <table>
                <tr>
                    <td>
                        <h2>1.</h2>
                    </td>
                    <td>
                        <h2>Footstar</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>1.1.</b>
                    </td>
                    <td>
                        Footstar is a data controller within the meaning of the General Data Protection Regulation. Further details about us and how to contact us appear below.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>1.2.</b>
                    </td>
                    <td>
                        We collect personal data from you when you contact us, use our website or apps, when you make purchases in our shop, and when you provide us with your personal data directly.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>1.3.</b>
                    </td>
                    <td>
                        We have legal obligations to you to inform you of:
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    The personal data which we collect from you
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    How we use your personal data
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    The purposes for which we process your personal data
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d.
                                </td>
                                <td>
                                    The legal basis for processing your personal data
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    e.
                                </td>
                                <td>
                                    Who we share your personal data with
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    f.
                                </td>
                                <td>
                                    Where we store your personal data, and what protections we have in place for it
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    g.
                                </td>
                                <td>
                                    Where we send your personal data, and
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    h.
                                </td>
                                <td>
                                    where applicable
                                    <table>
                                        <tr>
                                            <td>
                                                i.
                                            </td>
                                            <td>
                                                the existence of automated decision-making and profiling (the logic, the significance and the envisaged consequences)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                ii.
                                            </td>
                                            <td>
                                                whether the data subject is obliged to provide the personal data and of the possible consequences of failure to provide such data;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                iii.
                                            </td>
                                            <td>
                                                whether the provision of personal data is a statutory or contractual requirement, or a requirement necessary to enter into a contract,
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    i.
                                </td>
                                <td>
                                    the periods for which we retain your personal data
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    j.
                                </td>
                                <td>
                                    your rights under the General Data Protection Regulation
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>1.4.</b>
                    </td>
                    <td>
                        We set this information out below for you.
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>2.</h2>
                    </td>
                    <td>
                        <h2>How we process your Personal Data</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>2.1.</b>
                    </td>
                    <td>
                        Personal data is information that identifies you as a person, either directly or indirectly.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>2.2.</b>
                    </td>
                    <td>
                        It includes personal data
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    that has been submitted to our system by you (such as name and e-mail when signing up,
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    about you contained in messages sent through our contact forms or to our e-mail accounts), and
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    data that has been created or logged automatically by our systems (by registering the IP address, app version or browser signature you use during your visits to our site and app.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>2.3.</b>
                    </td>
                    <td>
                        When we process your personal data, we:
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    do so lawfully, fairly and in a transparent manner
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    collect it for specified, explicit and legitimate purposes and not further processed in a manner that is incompatible with those purposes
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    limit our collection to what is adequate and relevant to what is necessary in relation to the purposes for which they are processed
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d.
                                </td>
                                <td>
                                    take reasonable steps to keep it accurate and up to date having regard to the purposes for which they are processed
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    e.
                                </td>
                                <td>
                                    keep it in a form where we can identify you for no longer than is necessary, for the purposes for which it is processed
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    f.
                                </td>
                                <td>
                                    process it in a manner that ensures appropriate security of the personal data. This includes protection against unauthorised or unlawful processing and against accidental loss, destruction or damage. We use appropriate technical or organisational measures to do so.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <h2>3.</h2>
                    </td>
                    <td>
                        <h2>Personal data collected</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>3.1.</b>
                    </td>
                    <td>
                        The types of personal data we may collect from you when you contact us, register and use our game and apps, or visit our website includes:
                        <table border="1">
                            <tr>
                                <td style="padding: 5px;">
                                    Category of Personal Data
                                </td>
                                <td style="padding: 5px;">
                                    Personal Data
                                </td>
        <td style="padding: 5px;">
                                    Reason for Collection/Retention Period
                                </td>
                            </tr>
<tr>
                                <td style="padding: 5px;">
                                    Registration Data
                                </td>
                                <td style="padding: 5px;">
                                    Email address, country of residence, login name, age as at registration, alias and password.
                                </td>
<td style="padding: 5px;">
                                    This data is required to open and maintain an account. It is always retained, also if your account turns inactive, to facilitate a new team application under the same account in the future. It will be erased if you explicitly withdraw your consent.
                                </td>
                            </tr>

                           
                            <tr>
                                <td style="padding: 5px;">
                                    User Profile
                                </td>
                                <td style="padding: 5px;">
                                    Your manager presentation text, favourite football team, team logo. 
        <td style="padding: 5px;">
                                    This data is optional. It is also stored until you withdraw your consent. 
                             
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;">
                                    Purchase Data
                                </td>
                                <td style="padding: 5px;">
                                    Credit card number, address, bank account identifiers and PayPal payment details are passed directly to our payment processors when using the Footstar shop. This data is required to make any purchase. We do not actually receive them.
                                </td>
  <td style="padding: 5px;">
                                    This data is required to make any purchase. We do not store credit card information ourselves, but we do keep transaction data on file (type of order placed, amount spent, refunds) for a period of at least 10 years for legal and accounting reasons.
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;">
                                    Information automatically logged
                                </td>
                                <td style="padding: 5px;">
                                    IP address, login times, browser signature, Supporter status. 
<td style="padding: 5px;">
                                    This data is necessary for us to maintain legitimate business interests such as to serve ads, perform website analytics, and carry out fraud prevention. It is automatically deleted after 365 days.
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>3.2.</b>
                    </td>
                    <td>
                        Please help us keep your personal data up to date and let us know when it changes.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>3.3.</b>
                    </td>
                    <td>
                        Also, game data: Any information created by the Footstar game engine is non-personal information. This includes match orders placed, matches played, match statistics, players scouted, nicknames given, transfers made, achievements earned, titles won, and many other things. It also includes team names. This kind of data is out of the scope of privacy regulations. If you exercise your right to be forgotten, this data will be anonymised.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>4.</h2>
                    </td>
                    <td>
                        <h2>Why we collect personal data</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>4.1.</b>
                    </td>
                    <td>
                        We need to collect personal data from you to communicate with you, to be able to offer you the services you request, and to be able to verify your age.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>4.2.</b>
                    </td>
                    <td>
                        Without your personal data
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    our website would not function properly;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    we could not provide you with our game;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    we could not respond to you if you have any questions;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d.
                                </td>
                                <td>
                                    we could not comply with our legal obligations;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    e.
                                </td>
                                <td>
                                    we could not maintain our legitimate business operations;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <b>4.3.</b>
                    </td>
                    <td>
                        As a user in Footstar you are able to communicate with others through many communication channels. You then have the opportunity to also share information about yourself.
                    </td>
                </tr> <tr>
                    <td>
                        <b>4.4.</b>
                    </td>
                    <td>
                        Information sent by way of e-mail, chat messages, contact forms or internal Footstar messages to Footstar representatives or game volunteers. It will be treated with confidentiality and deleted on your request.
                    </td>
                </tr> <tr>
                    <td>
                        <b>4.5.</b>
                    </td>
                    <td>
                       Personal data shared through public settings, such as a Footstar press release, blog posts, guestbook messages or forum posts, can be shared and stored by anyone and is outside Footstar control. 
                    </td>
                </tr> <tr>
                    <td>
                        <b>4.6.</b>
                    </td>
                    <td>
                        Information posted to the public parts of Footstar (forums, guestbooks, press releases) is never deleted automatically. It is an important part of the game history. If you exercise your right to erase your personal data, your forum posts will only be anonymised.
                    </td>
                </tr>
<tr>
                    <td>
                        <b>4.7.</b>
                    </td>
                    <td>
                        We do not knowingly supply our services to those under 13 years of age. We will require parental consent of those that are between the ages of 13 and 16.
                    </td>
                </tr>

<tr>
                    <td>
                        <h2>5.</h2>
                    </td>
                    <td>
                        <h2>Where do we obtain your personal data</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>5.1.</b>
                    </td>
                    <td>
                        We primarily obtain personal data about you directly from you, when you register and through your continued use of the website;
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>5.2.</b>
                    </td>
                    <td>
                        Other users of Footstar may also post information relating to you on our forums, in guestbooks, press announcements, the Footstar messaging system or in the Footstar chat. Such information could contain personal details
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>6.</h2>
                    </td>
                    <td>
                        <h2>How we use your personal data</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>6.1.</b>
                    </td>
                    <td>
                        We process your personal data for the following purposes, where we have a legal basis to do so:
                        <table border="1">
                            <tr>
                                <td style="padding: 5px;">
                                    a.
                                </td>
                                <td style="padding: 5px;">
                                    Communications
                                </td>
                                <td style="padding: 5px;">
                                    - Verify your identity and age<br />
                                    - Contact you when necessary <br />
                                - Understand your requests and needs <br />
                                - Seek and understand your views, opinions or comments <br />
                                - Notify you of changes to our services, staff, and other role holders <br />
                                - Send you communications which have been requested <br />
                                - Send you communications required by law <br />
                                - Communicate with others about your queries, rights, entitlements
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;">
                                    b.
                                </td>
                                <td style="padding: 5px;">
                                    Gaming
                                </td>
                                <td style="padding: 5px;">
                                    - Allow others to communicate you on our gaming platform<br />
                                    - Display information about you and your teams, for instance, your team status which may be visible on your profile;<br />
                                    - Personalise the gaming experience <br />
                                    - Allow you to share information about your use of the platform through a third-party social media service, if you connect your account through that service (which you can turn off at any time) and to provide you with better recommendations<br />
                                    - Monitor and analyse trends, usage, and activities in connection with our Platform
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;">
                                    c.
                                </td>
                                <td style="padding: 5px;">
                                    Advertising
                                </td>
                                <td style="padding: 5px;">
                                    - Provide content, features, or sponsorships that match member profiles or interests;<br />
                                    - Facilitate contests and other promotions;<br />
                                    - Combine with information that we collect for the purposes described in this Privacy Policy; and<br />
                                    - Carry out any other purposes described to you at the time that we collected the information
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;">
                                    d.
                                </td>
                                <td style="padding: 5px;">
                                    Operations
                                </td>
                                <td style="padding: 5px;">
                                    - Deliver our services to our customers<br />
                                    - Administer our agreements with customers<br />
                                    - Management and resource planning<br />
                                    - Process payments<br />
                                    - Host chat rooms, forums, message boards and news groups <br />
                                    - Manage the performance of our gaming platform<br />
                                    - Send you offers you may be interested in<br />
                                    - Invite you to participate in surveys<br />
                                    - Update you on your performance in our games<br />
                                    - Inform you of our system status and downtime<br />
                                    - Send you security updates on our systems
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 5px;">
                                    e.
                                </td>
                                <td style="padding: 5px;">
                                    Administration
                                </td>
                                <td style="padding: 5px;">
                                    - Maintain our own accounts and records<br />
                                - Audit our accounts and records
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>6.2.</b>
                    </td>
                    <td>
                        We use your personal data for the following reasons:
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    You have given us your consent in writing for the purposes for which you provided your personal data
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    We need to take steps to form a contract with you and perform a contract with you
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    We need to comply with a legal obligation other than with you;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d.
                                </td>
                                <td>
                                    We or someone else has a legitimate interest other than where your interests or fundamental rights and freedoms would be overridden.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>6.3</b>
                    </td>
                    <td>
                        We believe we have legitimate interests to process your personal data to:
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    Administer contracts with you and others
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    Enable us to meet our legal and statutory obligations to you and others
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    Ensure compliance with our policies and procedures
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d.
                                </td>
                                <td>
                                    Conduct data protection assessments and audits
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    e.
                                </td>
                                <td>
                                    Prevent, detect, investigate and address fraud, corruption and misconduct by you and others
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    f.
                                </td>
                                <td>
                                    Ensure network and information security, including preventing unauthorised access to our computer and electronic communications systems and preventing malicious software distribution
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    g.
                                </td>
                                <td>
                                    Value and sell our business
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>6.4.</b>
                    </td>
                    <td>
                        We will only use your personal information for the purposes for which we collected it, unless we reasonably consider that we need to use it for another reason and that reason is compatible with the original purpose.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>6.5.</b>
                    </td>
                    <td>
                        Please note that we may process your personal information without your knowledge or consent, in compliance with the above rules, where this is required or permitted by law.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>7.</h2>
                    </td>
                    <td>
                        <h2>Data Sharing</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.1.</b>
                    </td>
                    <td>
                        We have put in place appropriate security measures to prevent your personal information from being accidentally lost, used or accessed in an unauthorised way, altered or disclosed.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.2.</b>
                    </td>
                    <td>
                        Other game users are able to view your User Profile data as part of the playing the game and any participation you have in our forums or chat rooms.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.3.</b>
                    </td>
                    <td>
                        We otherwise limit access to your personal information to those employees, agents, contractors and other third parties who need to know. They will only process your personal information on our instructions and they are subject to a duty of confidentiality. When we do send them personal data or make it available to them, we minimise it. In many cases, we are able to pseudonymise it and sometimes anonymise it so that they do not tell who you are.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.4.</b>
                    </td>
                    <td>
                        They are:
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    Super-Moderators and Moderators. Information shown by us to our volunteers about other users is anonymized and does thus not constitute personal data. However, all volunteers enter into an agreement to protect personal data about users if any such data would be offered to them by users through contact forms, forums or e-mail.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    Payment processors. (Super-Rewards, PayPal) 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    Our accountants and auditors
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d.
                                </td>
                                <td>
                                    Legal advisors
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    e.
                                </td>
                                <td>
                                    Website hosting infrastructure companies
                                </td>
                            </tr>
                             <tr>
                                <td>
                                    f.
                                </td>
                                <td>
                                    Cloud storage providers 
                                </td>
                            </tr><tr>
                                <td>
                                    g.
                                </td>
                                <td>
                                    Advertising networks
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><i>Data Breaches</i></td>
                </tr>
                <tr>
                    <td>
                        <b>7.5.</b>
                    </td>
                    <td>
                        We have put in place procedures to deal with any suspected data security breach and will notify you and any applicable regulator of a suspected breach where we are legally required to do so.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.6.</b>
                    </td>
                    <td>
                        We also may have cause to share your personal information with:
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    computer systems service providers, including IT security personnel
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    regulatory authorities.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.7.</b>
                    </td>
                    <td>
                        We only permit them to process your personal data for specified purposes and in accordance with our instructions.
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><i>Marketing</i></td>
                </tr>
                <tr>
                    <td>
                        <b>7.8.</b>
                    </td>
                    <td>
                        We strive to provide you with choices regarding certain personal data uses, particularly around marketing and advertising. We have established the following personal data control mechanisms.
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><i>Promotional Offers from Us</i></td>
                </tr>
                <tr>
                    <td>
                        <b>7.9.</b>
                    </td>
                    <td>
                        We may use your Contact Details, User Profile to form a view on what we think you may want or need, or what may be of interest to you. This is how we decide which products, services and offers may be relevant for you.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.10.</b>
                    </td>
                    <td>
                        You will receive marketing communications from us if you have requested information from us or purchased products or services from us or if you provided us with your details when you entered a competition or registered for a promotion and, in each case, you have not opted out of receiving that marketing.
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><i>Third Party Marketing</i></td>
                </tr>
                <tr>
                    <td>
                        <b>7.11.</b>
                    </td>
                    <td>
                        We will get your express opt-in consent before we share your personal data with any company for marketing purposes, even if we do not promote that as an usual practice.
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><i>Opting Out</i></td>
                </tr>
                <tr>
                    <td>
                        <b>7.12.</b>
                    </td>
                    <td>
                        You can ask us to stop sending you marketing messages at any time by selecting options in your online profile [or emailing us]. We include opt-out links on all marketing messages.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.13.</b>
                    </td>
                    <td>
                        Where you opt out of receiving these marketing messages, this will not apply to personal data provided to us as a result of a product/service purchase, warranty registration, product/service experience or other transactions.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>7.14.</b>
                    </td>
                    <td>
                        There are some messages that you are not able to opt-out of, such as security messages.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>8.</h2>
                    </td>
                    <td>
                        <h2>Automated Decision Making</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>8.1.</b>
                    </td>
                    <td>
                        Automated decision-making takes place when an electronic system uses personal information to make a decision without human intervention.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>8.2.</b>
                    </td>
                    <td>
                        We do not automate decision making in a way that effects your legal rights or has legal consequences for you.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>8.3.</b>
                    </td>
                    <td>
                        Where we are allowed to use automated decision-making:
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    where we have notified you of the decision and given you 21 days to request a reconsideration.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    where it is necessary to perform the contract with you and appropriate measures are in place to safeguard your rights.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    in limited circumstances, with your explicit written consent and where appropriate measures are in place to safeguard your rights.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>8.4.</b>
                    </td>
                    <td>
                        You will not be subject to decisions that will have a significant impact on you based solely on automated decision-making, unless we have a lawful basis for doing so and we have notified you.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>9.</h2>
                    </td>
                    <td>
                        <h2>Data Storage</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>9.1.</b>
                    </td>
                    <td>
                        We don't transfer your personal data outside the EEA.
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><i>Access to our Services from Outside the EEA</i></td>
                </tr>
                <tr>
                    <td>
                        <b>9.2.</b>
                    </td>
                    <td>
                        When you access your account, only your game alias is visible to other users. Your alias is part of your public profile on our gaming platform.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>9.3.</b>
                    </td>
                    <td>
                        If you are outside the EEA and wish to use our services, you will be in a country that has data protection laws that are different to the laws of the EEA. Those countries may not be as protective as those in the EEA. We have taken appropriate safeguards to require that your information will remain protected in accordance with this Privacy policy.
                    </td>
                </tr><tr>
                    <td>
                        <b>9.4.</b>
                    </td>
                    <td>
                        If you do not want your name visible to other users, please choose a game alias which is not your name.
                    </td>
                </tr>

<tr>
                    <td>
                        <h2>10.</h2>
                    </td>
                    <td>
                        <h2>Data Security</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>10.1.</b>
                    </td>
                    <td>
                        We have put in place appropriate security measures to prevent your personal information from being accidentally lost, used or accessed in an unauthorised way, altered or disclosed. We limit access to your personal information to those employees, agents, contractors and other third parties who need to know. They will only process your personal information on our instructions and they are subject to a duty of confidentiality.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>10.2.</b>
                    </td>
                    <td>
                        We store all information that you provide to us on secure servers.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>10.3.</b>
                    </td>
                    <td>
                        We train employees regarding our data privacy policies and procedures, and permit authorised employees to access information on a need to know basis, as required for their role.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>10.4.</b>
                    </td>
                    <td>
                        We use firewalls designed to protect against intruders and test for network vulnerabilities.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>10.5.</b>
                    </td>
                    <td>
                        Where you have a password which enables you to use our services you are responsible for keeping this password complex, secure, and confidential. If you would like to update or change your password, you may select the 'Forgot your password?' link on the login page. You will be sent an email that allows you to reset your password.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>10.6.</b>
                    </td>
                    <td>
                        However, no method of transmission over the internet or method of electronic storage is completely secure.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>10.7.</b>
                    </td>
                    <td>
                        We have put in place procedures to deal with any suspected data security breach and will notify you and any applicable regulator of a suspected breach where we are legally required to do so.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>11.</h2>
                    </td>
                    <td>
                        <h2>Retention of your Data</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>11.1.</b>
                    </td>
                    <td>
                        We will only retain your personal information for as long as necessary to fulfil the purposes we collected it for, including for the purposes of satisfying any legal, accounting, or reporting requirements.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>11.2.</b>
                    </td>
                    <td>
                        To determine the appropriate retention period for personal data, we consider the amount, nature, and sensitivity of the personal data, the potential risk of harm from unauthorised use or disclosure of your personal data, the purposes for which we process your personal data and whether we can achieve those purposes through other means, and the applicable legal requirements.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>11.3.</b>
                    </td>
                    <td>
                        By law we have to keep basic information about our customers (including contact, name and transaction data) for six years after they cease being customers for tax purposes.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>11.4.</b>
                    </td>
                    <td>
                        In some circumstances we may anonymise your personal information so that it can no longer be associated with you, in which case we may use such information without further notice to you. We will retain and securely destroy your personal information (as the case may be) in accordance with applicable laws and regulations.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>11.5.</b>
                    </td>
                    <td>
                        In some circumstances we may anonymise your personal information so that it can no longer be associated with you, in which case we may use such information without further notice to you. Once you are no longer an employee, worker or contractor of the company we will retain and securely destroy your personal information in accordance with our data retention policy.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>12.</h2>
                    </td>
                    <td>
                        <h2>Rights of access, correction, erasure, and restriction</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>12.1.</b>
                    </td>
                    <td>
                        You have a series of rights under the General Data Protection Regulation.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>12.2.</b>
                    </td>
                    <td>
                        Not all apply in all circumstances. Under certain circumstances, you have the right to:
                        <table>
                            <tr>
                                <td>
                                    a.
                                </td>
                                <td>
                                    Request access to your personal information (commonly known as a "data subject access request"). This enables you to receive a copy of the personal information we hold about you and to check that we are lawfully processing it.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    b.
                                </td>
                                <td>
                                    Request correction of the personal information that we hold about you. This enables you to have any incomplete or inaccurate information we hold about you corrected.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    c.
                                </td>
                                <td>
                                    Request erasure of your personal information. This enables you to ask us to delete or remove personal information where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your personal information where you have exercised your right to object to processing (see below).
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    d.
                                </td>
                                <td>
                                    Object to processing of your personal information where we are relying on a legitimate interest (or those of a third party) and there is something about your particular situation which makes you want to object to processing on this ground. You also have the right to object where we are processing your personal information for direct marketing purposes.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    e.
                                </td>
                                <td>
                                    Request the restriction of processing of your personal information. This enables you to ask us to suspend the processing of personal information about you, for example if you want us to establish its accuracy or the reason for processing it.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    f.
                                </td>
                                <td>
                                    Request the transfer of your personal information to another party.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>12.3.</b>
                    </td>
                    <td>
                        If you would like to exercise any of these rights, please contact us using the details below. We will need to verify your identity before we are able to release any personal data to you.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>13.</h2>
                    </td>
                    <td>
                        <h2>Withdrawal of Consent</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>13.1.</b>
                    </td>
                    <td>
                        Where you may have provided your consent for us to process your personal data and/or transfer your personal data for a specific purpose, you can withdraw it at any time.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>13.2.</b>
                    </td>
                    <td>
                        To withdraw your consent, please contact us using the e-mail <a href="mailto:info@planetariumgames.com">info@planetariumgames.com</a>, or by visiting the <a href='https://www.footstar.org/contactos.asp'>Contact Us</a> page on Footstar. Once we have received notification that you have withdrawn your consent, we will no longer process your information for the purpose or purposes you originally agreed to, unless we have another legitimate basis for doing so in law.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>14.</h2>
                    </td>
                    <td>
                        <h2>About Us</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>14.1.</b>
                    </td>
                    <td>
                        Planetarium Games Ltd, a company with its location registered at Porto, Portugal.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>14.2.</b>
                    </td>
                    <td>
                        Planetarium Games is responsible for the personal data we hold and use.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>14.3.</b>
                    </td>
                    <td>
                        If you are dissatisfied with the way we process your personal data, you have the right to complain to the <b>Portugueses National Data Protection Commissioner</b>.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>14.4.</b>
                    </td>
                    <td>
                        They may be contacted at <b>https://www.cnpd.pt/bin/duvidas/queixas_frm.aspx</b>.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>14.5.</b>
                    </td>
                    <td>
                        We would prefer to try and resolve any difficulties between us and make them right before you approach the CNPD. Please consider contacting us in the first instance.
                    </td>
                </tr>
                <tr>
                    <td>
                        <h2>15.</h2>
                    </td>
                    <td>
                        <h2>Changes to this Privacy Statement</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>15.1.</b>
                    </td>
                    <td>
                        We may need to change this privacy policy from time to time.
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>15.2.</b>
                    </td>
                    <td>
                        We will communicate any changes to the privacy policy on our website or by e-mail.
                    </td>
                </tr>
           </table>


        </td></tr></table>

			</td>
            <td class="arial" valign="top" width="22%" align='right'>
               <!-- Info Box -->
			   <script>
function confirmSubmit_forum(tipo, mensagem, urll){
if (tipo == 1) var agree=confirm(mensagem);
if (tipo == 2) var agree=confirm("Confirm?");
if (agree)
	location.href=urll;
}		
</script>
<table width='100%'><tr><td align='center' id='banner_info_box'></td></tr></table>
<div style='min-height:300px;'></div>
            </td>
          </tr>
        </table>

	<br>

   </td>

  </tr>
  <tr>
   <td height=19 background="img/img_but.jpg">
<table width=100% style='border-bottom: 1px solid #000; height: 30px;'>
<tr><td class=comentarios align=center valign=middle width='95%'><small><a href=manual.asp?section=1><font color=#FFFFCC><u>Game Rules</u></font></a><font color=#FFFFCC>&nbsp;|&nbsp;</font><a href=tos.asp><font color=#FFFFCC><u>Terms of Service</u></font></a><font color=#FFFFCC>&nbsp;|&nbsp;</font><a href=privacy_policy.asp><font color=#FFFFCC><u>Privacy Policy</u></font></a><font color=#FFFFCC>&nbsp;|&nbsp;</font><a href='contactos.asp'><font color=#FFFFCC><u>Contact us</u></font></a><font color=#FFFFCC>&nbsp;|&nbsp; Copyright &#169; 2018 <a href=https://www.footstar.org/ target=_blank><font color=#FFFFCC><u>Footstar</u></font></a>. All Rights Reserved - Page generated in 0,05 seconds&nbsp; &nbsp;</font></small></td>
<td></td></tr></table>
	
</td></tr></table>

<script>
 $(function(){	
	$(".btn-login").click(function(event) {
		$("#login_box").toggle();
	});
});	
</script>	
</td>

  </tr>
</table>
</body>
</html>
