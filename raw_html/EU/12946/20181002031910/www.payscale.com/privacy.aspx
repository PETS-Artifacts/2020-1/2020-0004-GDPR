

<!DOCTYPE html >
<html >

<head id="ctl00_m_htmlHeader"><title>
	Privacy Policy
</title>
    <script src="https://cdn-payscale.com/resources/2018.0924.1535.420/js/Cookies.js"></script>
    

<link rel="preconnect" href="//cdn-payscale.com" />
<link rel="preconnect" href="//dev.visualwebsiteoptimizer.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="preconnect" href="https://www.google-analytics.com">
<link rel="preconnect" href="//www.googletagservices.com">
<link rel="preconnect" href="//www.googletagmanager.com">

<meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta http-equiv="x-ua-compatible" content="IE=edge" />
<meta property="fb:app_id" content="237709899768126"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"><link rel="apple-touch-icon" href="https://cdn-payscale.com/images/favicon/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="https://cdn-payscale.com/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="https://cdn-payscale.com/images/favicon/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="https://cdn-payscale.com/images/favicon/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="https://cdn-payscale.com/images/favicon/favicon-16x16.png" sizes="16x16">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,700,900" />
    <style>.pxl-navbar{background:#464249;box-sizing:border-box;font-family:Roboto,Open Sans,sans-serif;margin:0;height:70px;padding:0 20px;position:relative;width:100%;z-index:3;-webkit-font-smoothing:antialiased}.pxl-navbar a{text-decoration:none}.pxl-navbar--transparent{background:transparent}.pxl-navbar *,.pxl-navbar :after{font-family:Roboto,Open Sans,sans-serif;box-sizing:border-box}.pxl-navbar .pxl-navbar-nav>li{list-style:none;margin-bottom:0}.pxl-navbar .pxl-navbar-nav>li>a{color:#fff;text-decoration:none;font-size:14px;font-weight:700}.pxl-navbar .pxl-navbar-content{display:inline-block}.pxl-navbar .pxl-navbar-branding{display:inline-block;left:0;padding:20px 30px 20px 0;vertical-align:middle}.pxl-navbar .pxl-navbar-subnav{display:none}.pxl-navbar .pxl-navbar-subnav a{font-size:14px;font-weight:700}.pxl-navbar .pxl-navbar-subnav a:hover{text-decoration:none}.pxl-navbar .pxl-navbar-item{display:inline-block;font-size:14px;font-weight:700;cursor:pointer}.pxl-navbar .pxl-navbar-item:hover a{color:#fed9a3}.pxl-navbar .pxl-navbar-mobile-menu{color:#fff;display:none}.pxl-navbar .pxl-navbar-right{float:right;vertical-align:middle;padding:25px 0;line-height:20px}.pxl-navbar--white{background:#fff}.pxl-navbar--white .pxl-navbar-mobile-menu,.pxl-navbar--white .pxl-navbar-nav>li>a{color:#302c2c}.pxl-body-navbar{overflow:hidden}@media (min-width:992px){.pxl-navbar .pxl-navbar-nav{display:inline-block;list-style:none;margin:0;padding:0}.pxl-navbar-content{vertical-align:top;padding:25px 0;line-height:0}.pxl-navbar-content>ul{line-height:1}.pxl-navbar-item{line-height:20px;padding:0 30px 0 0}.pxl-navbar-item:last-child{padding:0}.pxl-navbar-collapse{display:none}.pxl-navbar-item.pxl-navbar-parent>.pxl-navbar-subnav{display:inline-block;height:0;left:0;margin-top:20px;overflow-y:hidden;position:absolute;top:47px;width:100%;transition:height 0ms ease .35s;-webkit-transition:height 0ms ease .35s}.pxl-navbar-item.pxl-navbar-parent>.pxl-navbar-subnav>ul{background-color:#625d6a;list-style:none;padding:20px 20px 20px 140px;margin:0}.pxl-navbar-item.pxl-navbar-parent>.pxl-navbar-subnav>ul>li{display:inline-block;font-size:14px;margin:0 40px 0 0}.pxl-navbar-item.pxl-navbar-parent>.pxl-navbar-subnav>ul>li:last-child{margin:0}.pxl-navbar-item.pxl-navbar-parent>.pxl-navbar-subnav>ul>li>a{color:#fff}.pxl-navbar-item.pxl-navbar-parent>a{position:relative}.pxl-navbar-item.pxl-navbar-parent>a:after{border-bottom:15px solid transparent;border-left:15px solid transparent;border-right:15px solid transparent;content:"";height:0;left:50%;margin:0 0 0 -15px;position:absolute;top:30px;width:0;transition:border-bottom 0ms ease .35s;-webkit-transition:border-bottom 0ms ease .35s}.pxl-navbar-item.pxl-navbar-parent:hover>a:after{border-bottom:15px solid #625d6a;transition:border-bottom 0ms ease .35s;-webkit-transition:border-bottom 0ms ease .35s}.pxl-navbar-item.pxl-navbar-parent:hover>.pxl-navbar-subnav{height:150px}.pxl-navbar--white .pxl-navbar-item.pxl-navbar-parent>.pxl-navbar-subnav>ul{background-color:#302c2c}.pxl-navbar--white .pxl-navbar-item.pxl-navbar-parent:hover>a:after{border-bottom-color:#302c2c}}@media (min-width:992px){.pxl-navbar{padding:0 60px}}@media (max-width:991px){.pxl-navbar.active{background:rgba(0,0,0,.7);height:100%;min-height:100vh;overflow-y:scroll;position:fixed;top:0}.pxl-navbar.active .pxl-navbar-mobile-menu{color:transparent!important;height:100vh;left:0;position:fixed;top:0;width:15%;transition:background .3s ease .2s;-webkit-transition:background .3s ease .2s}.pxl-navbar.active .pxl-navbar-mobile-menu:after{color:#fff;content:"\D7";font-size:30px;font-weight:400;left:0;position:absolute;text-align:center;width:100%}.pxl-navbar.active .pxl-navbar-branding{left:15%;position:relative;z-index:2;transition:left .8s ease;-webkit-transition:left .8s ease}.pxl-navbar.active .pxl-navbar-content{width:85%;right:0;transition:right .6s ease;-webkit-transition:right .6s ease}.pxl-navbar.active .pxl-navbar-right{color:#000!important;display:block;float:none;padding:25px 0;position:absolute;right:25px;top:0;z-index:2}.pxl-navbar-nav,.pxl-navbar-right{display:none}.pxl-navbar-mobile-menu{cursor:pointer;display:block!important;float:right;font-size:16px;font-weight:700;line-height:30px;padding:20px 0;transition:none;-webkit-transition:none}.pxl-navbar-content{background-color:#fff;box-shadow:0 2px 4px rgba(0,0,0,.5);color:#302c2c;display:block;float:right;margin:0;min-height:100vh;overflow-x:hidden;overflow-y:visible;padding:70px 0 0;position:absolute;right:-85%;top:0;width:0;z-index:1;transition:right .6s ease-out,width .6s ease-out;-webkit-transition:right .6s ease-out,width .6s ease-out}.pxl-navbar-nav{margin:0}.pxl-navbar-nav .pxl-navbar-item{display:block;line-height:1;margin:0;width:100%;padding:0}.pxl-navbar-nav .pxl-navbar-item>a{border-left:1px solid #c3c3ca;border-right:1px solid #c3c3ca;border-top:1px solid #c3c3ca;color:#302c2c;display:block;line-height:1;padding:25px;position:relative;width:100%}.pxl-navbar-nav .pxl-navbar-item:last-child>a{border-bottom:1px solid #c3c3ca}.pxl-navbar-nav .pxl-navbar-collapse{display:block}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent>a:after{display:none}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent.active>.pxl-navbar-collapse:after{content:"\2013"!important}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav{background:transparent;display:block;height:0;line-height:1;overflow:hidden;width:100%;position:relative;top:0;left:0;margin-top:0}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav>ul{list-style:none;margin:0;padding:0}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav>ul>li{display:block;float:left;margin:0;padding-right:30px;width:50%}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav>ul>li:nth-child(2n){padding-right:0}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav>ul>li>a{color:#fff;display:block;height:65px}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent.active .pxl-navbar-subnav{background-color:#302c2c;height:auto;padding:30px 25px 0}.pxl-navbar-nav{display:block;padding:0;width:100%}.pxl-navbar-nav .pxl-navbar-item>a{color:#302c2c!important}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent{position:relative}.pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent>.pxl-navbar-collapse:after{border-left:1px solid #c3c3ca;color:#302c2c;content:"+";font-size:30px;font-weight:300;height:64px;padding:18px 25px 25px;position:absolute;right:0;top:0;width:70px}}.pxl-navbar--mobile.pxl-navbar.active{background:rgba(0,0,0,.7);height:100%;min-height:100vh;overflow-y:scroll;position:fixed;top:0}.pxl-navbar--mobile.pxl-navbar.active .pxl-navbar-mobile-menu{color:transparent!important;height:100vh;left:0;position:fixed;top:0;width:15%;transition:background .3s ease .2s;-webkit-transition:background .3s ease .2s}.pxl-navbar--mobile.pxl-navbar.active .pxl-navbar-mobile-menu:after{color:#fff;content:"\D7";font-size:30px;font-weight:400;left:0;position:absolute;text-align:center;width:100%}.pxl-navbar--mobile.pxl-navbar.active .pxl-navbar-branding{left:15%;position:relative;z-index:2;transition:left .8s ease;-webkit-transition:left .8s ease}.pxl-navbar--mobile.pxl-navbar.active .pxl-navbar-content{width:85%;right:0;transition:right .6s ease;-webkit-transition:right .6s ease}.pxl-navbar--mobile.pxl-navbar.active .pxl-navbar-right{color:#000!important;display:block;float:none;padding:25px 0;position:absolute;right:25px;top:0;z-index:2}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav,.pxl-navbar--mobile.pxl-navbar .pxl-navbar-right{display:none}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-mobile-menu{cursor:pointer;display:block!important;float:right;font-size:16px;font-weight:700;line-height:30px;padding:20px 0;transition:none;-webkit-transition:none}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-content{background-color:#fff;box-shadow:0 2px 4px rgba(0,0,0,.5);color:#302c2c;display:block;float:right;margin:0;min-height:100vh;overflow-x:hidden;overflow-y:visible;padding:70px 0 0;position:absolute;right:-85%;top:0;width:0;z-index:1;transition:right .6s ease-out,width .6s ease-out;-webkit-transition:right .6s ease-out,width .6s ease-out}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav{margin:0}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item{display:block;line-height:1;margin:0;width:100%;padding:0}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item>a{border-left:1px solid #c3c3ca;border-right:1px solid #c3c3ca;border-top:1px solid #c3c3ca;color:#302c2c;display:block;line-height:1;padding:25px;position:relative;width:100%}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item:last-child>a{border-bottom:1px solid #c3c3ca}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-collapse{display:block}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent>a:after{display:none}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent.active>.pxl-navbar-collapse:after{content:"\2013"!important}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav{background:transparent;display:block;height:0;line-height:1;overflow:hidden;width:100%;position:relative;top:0;left:0;margin-top:0}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav>ul{list-style:none;margin:0;padding:0}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav>ul>li{display:block;float:left;margin:0;padding-right:30px;width:50%}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav>ul>li:nth-child(2n){padding-right:0}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent .pxl-navbar-subnav>ul>li>a{color:#fff;display:block;height:65px}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent.active .pxl-navbar-subnav{background-color:#302c2c;height:auto;padding:30px 25px 0}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav{display:block;padding:0;width:100%}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item>a{color:#302c2c!important}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent{position:relative}.pxl-navbar--mobile.pxl-navbar .pxl-navbar-nav .pxl-navbar-item.pxl-navbar-parent>.pxl-navbar-collapse:after{border-left:1px solid #c3c3ca;color:#302c2c;content:"+";font-size:30px;font-weight:300;height:64px;padding:18px 25px 25px;position:absolute;right:0;top:0;width:70px}.pxl-navbar .pxl-navbar__logo{height:30px}.pxl-navbar .pxl-navbar__logo *,.pxl-navbar .pxl-navbar__logo .bottom-dot,.pxl-navbar .pxl-navbar__logo .top-dot{fill:#fff}.pxl-navbar.active .pxl-navbar__logo *{fill:#302c2c}.pxl-navbar.active .pxl-navbar__logo .top-dot{fill:#b42e34}.pxl-navbar.active .pxl-navbar__logo .bottom-dot{fill:#807f83}.pxl-navbar #ps-navbar-login{border:1px solid #fff;cursor:pointer;font-size:14px;font-weight:700;line-height:20px;margin-left:40px;padding:8px 16px}.pxl-navbar #ps-navbar-login,.pxl-navbar #ps-navbar-login:hover{color:#fff;text-decoration:none}.pxl-navbar #ps-navbar-logout{text-align:center;margin:20px}.pxl-navbar #ps-navbar-phone{color:#fff;font-size:14px;font-weight:400;line-height:20px}@media (max-width:991px){.pxl-navbar #ps-navbar-phone{display:none}}.pxl-navbar #ps-navbar-business-dashboard{border:1px solid #fff;padding:8px 16px;margin-right:15px}.pxl-navbar #ps-navbar-business-dashboard,.pxl-navbar #ps-navbar-myaccount>li>a{color:#fff;font-size:14px;font-weight:700}.pxl-navbar #ps-navbar-business-dashboard:active,.pxl-navbar #ps-navbar-business-dashboard:hover,.pxl-navbar #ps-navbar-myaccount>li>a:active,.pxl-navbar #ps-navbar-myaccount>li>a:hover{color:#fff;text-decoration:none}.pxl-navbar #ps-navbar-myaccount>li>a:hover{color:#fed9a3}@media (max-width:991px){.pxl-navbar #ps-navbar-business-dashboard,.pxl-navbar #ps-navbar-phone{display:none}}@media (min-width:992px){.pxl-navbar #ps-navbar-logout{display:none}}.pxl-navbar .hidden{display:none!important}@media (max-width:991px){.pxl-navbar.active #ps-navbar-business-dashboard,.pxl-navbar.active #ps-navbar-business-dashboard:active,.pxl-navbar.active #ps-navbar-business-dashboard:hover,.pxl-navbar.active #ps-navbar-myaccount>li>a,.pxl-navbar.active #ps-navbar-myaccount>li>a:active,.pxl-navbar.active #ps-navbar-myaccount>li>a:hover{color:#302c2c}.pxl-navbar.active #ps-navbar-login,.pxl-navbar.active #ps-navbar-login:active,.pxl-navbar.active #ps-navbar-login:hover{color:#302c2c;border-color:#302c2c}}.pxl-navbar .business-nav>.pxl-navbar-subnav>ul>li>a:hover,.pxl-navbar .data-nav>.pxl-navbar-subnav>ul>li>a:hover,.pxl-navbar .personal-nav>.pxl-navbar-subnav>ul>li>a:hover{color:#fed9a3}.pxl-navbar #ps-navbar-myaccount{display:inline-block;margin:0;padding:0}.pxl-navbar #ps-navbar-myaccount>li{margin:0}.pxl-navbar #ps-navbar-myaccount .pxl-navbar-subnav>ul{padding:20px;text-align:right;right:0}.pxl-navbar #ps-navbar-myaccount .pxl-navbar-subnav>ul>li>a:hover{color:#fed9a3}.pxl-navbar--white .pxl-navbar-item:hover a{color:#8b355b}.pxl-navbar--white #ps-navbar-business-dashboard,.pxl-navbar--white #ps-navbar-business-dashboard:hover,.pxl-navbar--white #ps-navbar-login,.pxl-navbar--white #ps-navbar-login:hover,.pxl-navbar--white #ps-navbar-myaccount>li>a,.pxl-navbar--white #ps-navbar-myaccount>li>a:hover{color:#302c2c}.pxl-navbar--white #ps-navbar-business-dashboard,.pxl-navbar--white #ps-navbar-login{border-color:#302c2c}.pxl-navbar--white .pxl-navbar__logo *{fill:#302c2c}.pxl-navbar--white .pxl-navbar__logo .top-dot{fill:#b42e34}.pxl-navbar--white .pxl-navbar__logo .bottom-dot{fill:#807f83}</style>
    <style>.pxl-footer{width:100%;display:block;background-color:#302c2c}.pxl-footer a{text-decoration:none}.pxl-footer *{font-family:Roboto,Open Sans,sans-serif;font-weight:400}.pxl-footer>ul>li.pxl-navbar-item>a{color:#feeec5;font-size:12px;line-height:16px}.pxl-footer>ul>li.pxl-navbar-item>a:hover{color:#f7a154}.pxl-footer-content{padding:25px 30px}.pxl-footer-logos{float:left}.pxl-footer-logos .logo *{fill:#fff}.pxl-footer-social-logos{margin-top:15px;float:right;width:100%;display:block;text-align:right}.pxl-footer-social-logos a>img{margin-left:8px;margin-right:8px}.pxl-footer-social-logos .pxl-social-logo:hover path{fill:#f7a154}.pxl-footer-links{vertical-align:middle;text-align:right;float:right}.pxl-footer-links>span.pxl-footer-research-links{display:block;padding-top:10px;color:#feeec5;font-size:12px;font-weight:400}.pxl-footer-links>span.pxl-footer-research-links a.pxl-footer-strong{font-weight:700}.pxl-footer-links span{display:inline-block}.pxl-footer-links span a{color:#feeec5;font-size:12px;font-weight:400;line-height:16px}.pxl-footer-links span a:hover{color:#f7a154;text-decoration:none}.pxl-footer-link-item{margin-left:8px;margin-right:8px}.pxl-footer-clear{clear:both}.pxl-footer-bottom{background-color:#000;padding:15px 30px;overflow:hidden}@media (max-width:767px){.pxl-footer-bottom{padding:15px 20px}}.pxl-footer-bottom .pxl-footer-copyright{color:#fff;font-size:11px}.pxl-footer-bottom .pxl-footer-privacy{display:inline-block;float:right}@media (max-width:767px){.pxl-footer-bottom .pxl-footer-privacy{display:block;float:none;text-align:center}}.pxl-footer-bottom .pxl-footer-privacy ul{margin:0;padding:0;list-style-type:none}.pxl-footer-bottom .pxl-footer-privacy ul>li{display:inline-block;margin-right:30px}.pxl-footer-bottom .pxl-footer-privacy ul>li:last-child{margin-right:0}.pxl-footer-bottom .pxl-footer-privacy .pxl-privacy-item>a{color:#fff;font-size:11px;font-weight:400}.pxl-footer-bottom .pxl-footer-privacy .pxl-privacy-item>a:hover{color:#f7a154;text-decoration:none}@media (min-width:768px){.pxl-footer-copyright,.pxl-footer-links{display:inline-block}.pxl-footer-links{vertical-align:middle;width:58%}.pxl-footer-link-item{margin-left:16px;margin-right:0}.pxl-footer-logos{width:42%;display:inline-block}}@media (max-width:767px){.pxl-footer-copyright{display:none}.pxl-footer-links{display:block;margin:0 auto;text-align:center;margin-bottom:15px;float:none}.pxl-footer-social-logos{margin-top:15px;text-align:center}.pxl-footer-logos{width:100%;display:block;text-align:center}}</style>
<meta name="description" content="Excellent analysis comparing your job profile to the salary and compensation packages of people whose skills and experiences match yours." /><link rel="stylesheet" type="text/css" href="https://cdn-payscale.com/resources/2018.0924.1535.420/css/main.css"/><link rel="stylesheet" type="text/css" href="https://cdn-payscale.com/resources/2018.0924.1535.420/css/payscale_v2.css"/><script>
var dataLayer = [{"cacheControl":"private","origin":"payscale"}];
</script>
<link rel='search' type='application/opensearchdescription+xml' title='PayScale Salary Search' href='/opensearch.xml' /><script src="https://cdn-payscale.com/resources/2018.0924.1535.420/js/lib/loadcss/1.2.1/loadCSS.js" defer></script><script src="https://cdn-payscale.com/resources/2018.0924.1535.420/js/lib/loadcss/1.2.1/cssrelpreload.js" defer></script><script src="https://cdn-payscale.com/content/lib/jquery/jquery-2.2.4.min.js" ></script><script src="https://cdn-payscale.com/resources/2018.0924.1535.420/js/PayScale.js" defer></script><script src="https://cdn-payscale.com/resources/2018.0924.1535.420/js/iw.js" defer></script><script src="https://cdn-payscale.com/resources/2018.0924.1535.420/js/fieldhelp.js" defer></script></head>
<body style="margin: 0; padding: 0">

<!-- Google Tag Manager -->
<noscript><iframe src='//www.googletagmanager.com/ns.html?id=GTM-P3CVWR'
height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P3CVWR');</script><!-- End Google Tag Manager -->






<span id="pageTop"></span>

<style>
  .dashboardButton {
    background-color: #a5d649;
    border: 1px solid #9bcf3e;
    border-radius: 3px;
    color: #fff;
    cursor: pointer;
    font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
    font-size: 13px;
    line-height: 18px;
    margin: 0;
    padding: 6px 12px;
  }

  .dashboardButton:hover {
    background-color: #8fc330;
    border-color: #7aa629;
  }

  .insightBarRowWrap {
    max-width: 1000px;
    margin: 0 auto;
  }

  .insightBarColWrap {
    margin: 0 15px;
    padding: 7px 0 8px;
    text-align: right;
  }

  .insightBarCopyWrap {
    color: #fff;
    display: inline-block;
    font-size: 18px;
    position: relative;
    top: 2px;
  }
</style>

<div class="container-fluid hidden-xs insight-bar hidden" style="background-color: #2d2d2e;">
  <div class="insightBarRowWrap">
    <div class="insightBarColWrap">
      <div class="insightBarCopyWrap">
        Welcome back! Go to your
      </div> <a href="http://insight.payscale.com/" target="" style="margin-left: 3px;">
        <input type="button" class="dashboardButton" value="Dashboard" />
      </a>
    </div>
  </div>
</div>


<nav id="navbar" class="pxl-navbar  pxl-navbar--dark "><div id="navbar-mobile-menu" class="pxl-navbar-mobile-menu">Menu</div><div class="pxl-navbar-branding"><a href="/"><svg xmlns="http://www.w3.org/2000/svg" id="payscale-logo" class="pxl-navbar__logo" viewBox="-1051.5 522.2 490 163"><path d="M-923.9 534.9c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1c6.6 0 12.1 5.4 12.1 12.1" class="top-dot"></path><path d="M-923.9 603.9c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1c6.6 0 12.1 5.4 12.1 12.1" class="bottom-dot"></path><path d="M-923.9 569.4c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1c6.6.1 12.1 5.5 12.1 12.1" class="top-dot"></path><path d="M-923.9 638.4c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1c6.6 0 12.1 5.4 12.1 12.1m0 34.4c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1c6.6 0 12.1 5.4 12.1 12.1m-34.6-68.9c0 6.7-5.4 12.1-12.1 12.1-6.7 0-12.1-5.4-12.1-12.1s5.4-12.1 12.1-12.1c6.8 0 12.1 5.4 12.1 12.1m0 34.5c0 6.7-5.4 12.1-12.1 12.1-6.7 0-12.1-5.4-12.1-12.1s5.4-12.1 12.1-12.1c6.8 0 12.1 5.4 12.1 12.1m0 34.4c0 6.7-5.4 12.1-12.1 12.1-6.7 0-12.1-5.4-12.1-12.1s5.4-12.1 12.1-12.1c6.8 0 12.1 5.4 12.1 12.1m-34.4-34.4c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1 12.1 5.4 12.1 12.1m0 34.4c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1c6.7 0 12.1 5.4 12.1 12.1m-34.4-68.9c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1c6.7 0 12.1 5.4 12.1 12.1m0 34.5c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1 12.1 5.4 12.1 12.1m0 34.4c0 6.7-5.4 12.1-12.1 12.1s-12.1-5.4-12.1-12.1 5.4-12.1 12.1-12.1c6.7 0 12.1 5.4 12.1 12.1" class="bottom-dot"></path><path d="M-891.4 569.7h18.4c8.6 0 15.8.6 20.8 6.6 3.1 3.8 3.6 8.1 3.6 11.1 0 6.9-2.9 12.8-7.7 15.9-4.4 2.9-10.4 2.9-13.5 2.9h-8.7v23.2h-13v-59.7h.1zm12.7 26.9h5.1c4.2 0 11.8 0 11.8-9.1 0-8.1-6.8-8.1-10.5-8.1h-6.5v17.2m57.6 32.8c-.4-2.5-.4-4.4-.4-5.7-4.7 6.7-10.2 6.7-12.4 6.7-6 0-8.7-1.7-10.4-3.3-2.5-2.5-3.6-5.9-3.6-9.2s1.2-9.4 8.8-13c5.1-2.3 12.4-2.3 16.5-2.3 0-2.8-.1-4.1-.6-5.5-1.1-3.1-4.3-3.5-5.5-3.5-1.7 0-3.6.7-4.7 2.3-1 1.4-1 2.8-1 3.7h-12c.1-2.3.4-6.7 4.7-9.8 3.8-2.8 8.7-3.3 12.7-3.3 3.5 0 10.8.6 14.9 5.4 2.9 3.3 2.9 8.4 2.9 10.3l.1 16.7c0 3.6.2 7.1 1 10.5h-11.1zm-15.1-12.9c0 2.8 1.9 6 6 6 2.3 0 4.1-1.1 5.3-2.4 2.2-2.6 2.3-5.9 2.3-10.3-7.9-.7-13.6 1.7-13.6 6.7m41.3-28.8l9.2 28.9 9.3-28.9h12.2l-15.8 41-5 16.6h-12.3l5.3-15.8-16.1-41.8h13.2m45.6 22.6c0 2.9-.1 11.4 10.9 11.4 6.9 0 11.4-3.3 11.4-8.4 0-5.3-3.7-6.2-11.4-8-14.2-3.6-22-7.8-22-18.1 0-9.8 7.1-18.5 23.7-18.5 6.2 0 13.5 1.4 18.1 7.1 3.3 4.3 3.3 9.1 3.3 11.1h-12.3c-.1-2-.8-9.1-9.7-9.1-6 0-9.8 3.2-9.8 8.1 0 5.5 4.4 6.6 13 8.7 10.3 2.5 20.6 5.3 20.6 17 0 10.8-9 19.5-25.5 19.5-23 0-23.1-14.7-23.2-20.6h12.9m64.5-8.1c-.1-1.4-.7-7.7-5.4-7.7-5.7 0-6.5 8.7-6.5 13.3 0 2.3.2 9.8 3.1 12.8 1.2 1.2 2.4 1.4 3.3 1.4 1.3 0 5.1-.6 5.6-8h12.1c-.1 3.2-.6 8.3-5.4 12.3-3 2.6-6.7 3.9-12 3.9-5.5 0-10.4-1.2-14.2-5.5-3.7-4.2-5.3-9.8-5.3-16.5 0-19 14-21.6 19.5-21.6 7.8 0 17 4.3 17 15.7h-12m42.4 26.9c-.5-2.5-.5-4.4-.5-5.7-4.7 6.7-10.2 6.7-12.4 6.7-6.1 0-8.7-1.7-10.4-3.3-2.5-2.5-3.6-5.9-3.6-9.2s1.2-9.4 8.8-13c5.1-2.3 12.4-2.3 16.5-2.3 0-2.8-.1-4.1-.6-5.5-1.1-3.1-4.3-3.5-5.5-3.5-1.8 0-3.6.7-4.7 2.3-1 1.4-1 2.8-1 3.7h-12c.1-2.3.4-6.7 4.7-9.8 3.8-2.8 8.7-3.3 12.7-3.3 3.5 0 10.8.6 14.9 5.4 2.8 3.3 2.9 8.4 2.9 10.3l.1 16.7c0 3.6.2 7.1 1 10.5h-11zm-14.9-12.9c0 2.8 1.9 6 6 6 2.3 0 4.1-1.1 5.3-2.4 2.2-2.6 2.3-5.9 2.3-10.3-8-.7-13.6 1.7-13.6 6.7m33.8-46.9h12.4v59.8h-12.4v-59.8m30.9 42c0 2.8 0 11.1 8.5 11.1 3.1 0 6.1-1.2 7.3-5.7h11.1c-.1 2-.6 5.5-3.9 9.1-3.1 3.2-8.5 4.9-14.6 4.9-3.6 0-10.3-.6-14.7-5.3-3.8-3.9-5.4-9.6-5.4-16.3 0-6.8 1.7-14.8 8.7-19.7 3.2-2.2 7.2-3.5 11.7-3.5 6 0 13 2.3 16.9 10 2.8 5.7 2.6 11.8 2.5 15.3h-28.1v.1zm15.4-7.7c0-1.9-.1-9.3-7.3-9.3-5.4 0-7.7 4.4-7.9 9.3h15.2"></path></svg></a></div><div class="pxl-navbar-content"><ul class="pxl-navbar-nav"><li class="pxl-navbar-item pxl-navbar-parent personal-nav active"><a href="/salary">For You</a><div class="pxl-navbar-collapse"></div><div class="pxl-navbar-subnav"><ul><li><a href="/wizards/choose.aspx?tk=navlink-ps-header">Start Survey</a></li><li><a href="/mypayscale.aspx">Your Salary</a></li><li><a href="/salary-negotiation-guide">Salary Negotiation Guide</a></li><li><a href="/research/US/Country=United_States/Salary">Career Research</a></li><li><a href="/career-news">Career Advice</a></li><li><a href="/job-search-engine">Find Jobs</a></li><li><a href="/events?type=b2c">Events</a></li></ul></div></li><li class="pxl-navbar-item pxl-navbar-parent business-nav active"><a href="/hr">For Your Business</a><div class="pxl-navbar-collapse"></div><div class="pxl-navbar-subnav"><ul><li><a href="http://resources.payscale.com/hr-request-a-demo.html">Get a Demo</a></li><li><a href="/price-a-job?src=header">Price a Job</a></li><li><a href="/hr/compensation-software">Products</a></li><li><a href="/hr/customers">Customers</a></li><li><a href="/compensation-today">Compensation Today</a></li><li><a href="/events?type=b2b">Events</a></li></ul></div></li><li class="pxl-navbar-item pxl-navbar-parent data-nav active"><a href="/data">Compensation Research</a><div class="pxl-navbar-collapse"></div><div class="pxl-navbar-subnav"><ul><li><a href="/college-salary-report">College Salary Report</a></li><li><a href="/cbpr">Comp Best Practices</a></li><li><a href="/data/gender-pay-gap">Gender Pay Gap</a></li><li><a href="/payscale-index">PayScale Index</a></li><li><a href="/data/salary-history">Salary History</a></li></ul></div></li></ul><div id="ps-navbar-logout" class="hidden"><a href="/logout.aspx">Logout</a></div></div><div class="pxl-navbar-right"><span id="ps-navbar-phone" class="hidden">1-888-219-0327</span><a href="https://insight.payscale.com" id="ps-navbar-business-dashboard" class="hidden">Business Dashboard</a><ul id="ps-navbar-myaccount" class="hidden"><li class="pxl-navbar-item pxl-navbar-parent"><a href="/myaccount.aspx">My Account</a><div class="pxl-navbar-subnav"><ul><li><a href="/logout.aspx">Logout</a></li><li><a href="/myaccount.aspx">My Account</a></li><li><a href="/mypayscale.aspx">My Salary Reports</a></li></ul></div></li></ul><a id="ps-navbar-login" href="/login.aspx" class="">Log In</a></div></nav>
<div class="greyBarArea">
				
	<div>
		<table id="ctl00_mainContentTable" cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td id="ctl00_mainContentCell" align="left" valign="top" width="100%" class="fullContentArea">
				    
					
    <div class="container" style="padding-bottom: 15px">

        <h1>Privacy Policy</h1>
        <p>
            Here at PayScale, Inc. ("PayScale," "we," "our," or "us") we care about being transparent with our visitors and users.
            Effective as of May 25, 2018, we have updated our Privacy Policy to clarify (in plain English and with a lot of bullets) what information we collect and how we
            use that information when you visit our Websites (defined herein) and use our products or services (collectively, "Services").
            For purposes of this Privacy Policy, "Website" collectively refers to payscale.com and other websites that we operate or that link to this Privacy Policy. 
            The previous version of this Privacy Policy can be found <a href="/privacy-archive">here</a>. 
        </p>

        <p>
            We comply with the EU-U.S. Privacy Shield Framework and the Swiss-U.S. Privacy Shield Framework (collectively, "Privacy Shield") as set forth by the U.S. Department
            of Commerce regarding the collection, use, and retention of personal information transferred from the European Union and Switzerland to the U.S., respectively.
            To learn about how we comply with the Privacy Shield principles, go <a href="https://cdn-payscale.com/content/legal/privacyshield.pdf">here</a>. 
            You can learn more about Privacy Shield and view our certification <a rel="nofollow" href="https://www.privacyshield.gov/">here</a>.
        </p>

        <h2>Why and how do we collect information?</h2>
        <p>Our main purpose for collecting data is to provide you the Website, Services, and features that best meet your compensation information and management needs.  We collect information through various ways.  Below are the most common circumstances of how we collect information: </p>
        <ul>
            <li><strong>Signing up for a PayScale account:</strong> If you sign-up for a PayScale account, we will ask for your email address and to create a password.  After you create a PayScale account and log-in to our Services, you are no longer anonymous to us.</li>
            <li><strong>Completing a salary survey:</strong>  If you complete a survey, we will ask for detailed job and compensation information that's used to generate your customized salary report and certain portions of that data will be included in our anonymous and aggregated salary database called Crowdsourced Data.  You can choose to take a survey anonymously. </li>
            <li><strong>Browsing our Website:</strong> We automatically receive and record information on our server logs from your browser, including your IP address, PayScale cookie information, and the page you requested.</li>
            <li><strong>Requesting a Demo:</strong>If you request a demo from us, you will be asked to provide us your contact information and relevant information about your business, so we can contact you for scheduling and demo to you the appropriate Service based on your business structure and needs.  That information is stored in our internal systems to allow us to manage our relationship with you. </li>
            <li><strong>From businesses that buy our Services:</strong> We may receive data such as names, job title, and salary information from your employer if your employer has purchased our compensation management Services for businesses.  If your employer is a PayScale customer, you can learn from your employer about what information it shares with us, which will vary based on the Service. </li>
        </ul>
        <p>Still have questions?  Keep reading. </p>
        <h2>What information do we collect?</h2>
        <p>Below are examples of the types of information we collect to deliver our Website and Services and operate our business.  The information collected will vary depending on what Website or Service you visit or use and how you choose to use the Website or Service.</p>
        <ul>
            <li>Contact information such as name and email address when you sign-up for a PayScale account, request a demo, or contact us for support;</li>
            <li>Education, skills, job title, compensation data, and other information you choose to provide when you do a salary survey;</li>
            <li>If you are an employer using a purchased Service, your employees' names, job titles, job descriptions, salary, and other compensation-related information you choose to provide;</li>
            <li>Billing information if a Service requires payment;</li>
            <li>Resume and profile information from a third-party service such as LinkedIn or Indeed if you apply for a job posting or refer a friend to a job posting;</li>
            <li>Social media or email information if you choose to connect our Website or Services with a third-party social media or email provider;</li>
            <li>Usage data, such as information collected by cookies about the pages viewed, links clicked, and other actions taken when accessing our Website or Services;</li>
            <li>Activities, interactions, transactional information, and other computer and connection information (such as IP address) relating to use of our Website and Services; and</li>
            <li>Other information provided by you or by your employer who has purchased a Service from us that's required to use a Website or Service.</li>
        </ul>
        <h2>How do we use the information we collect?</h2>
        <p>PayScale uses information for the following purposes:</p>
        <ul>
            <li>to provide the Website and Services to you, your employer (if they bought a Service from us), and our other users;</li>
            <li>aggregate data and average compensation information to generate personalized compensation reports for our users and an anonymous crowdsourced salary database;</li>
            <li>to provide information about our Services, offers, and events;</li>
            <li>to personalize the content and experience on our Websites and Services;</li>
            <li>to secure the Website and Services, and prevent abuse or fraud;</li>
            <li>to communicate with our partners regarding business matters;</li>
            <li>undertake related tasks for legitimate business purposes; and</li>
            <li>other purposes disclosed at the time of collection.</li>
        </ul>

        <p>Below are common situations describing how we use collected information:</p>
        <ul>
            <li><strong>Develop Crowdsourced Data:</strong>  If you choose to complete a salary profile through our Website, the data you input will be used to develop an anonymous and aggregated salary database we call Crowdsourced Data.  You can learn more about our methodology <a href="https://www.payscale.com/about/methodology">here.</a></li>
            <li><strong>Service communications:</strong>  Service-related announcements will be sent to you when necessary.  For instance, we might send you an email if a Service is temporarily suspended for maintenance.  You may not opt-out of these communications because they're not promotional in nature.</li>
            <li><strong>Promotional messages:</strong>  You may receive marketing communications from us about our Services, practical guidance about salaries and careers, or our events.  For example, if you sign-up for a PayScale account but do not wish to receive any marketing emails from us, you can subscribe or unsubscribe from receiving certain types of emails through the email settings of your PayScale account.  You may also opt-out of receiving these communications by clicking the unsubscribe link provided in each communication and following the instructions provided.</li>
            <li><strong>Comments to posts:</strong>  If you post a comment, any information submitted may be removed by us in our discretion, but we do not guarantee that any information you choose to post will be removed.  Information you post may be viewed, collected, or used by other users or visitors, and could be used to send you unsolicited messages.  We are not responsible for the information you or other users or visitors choose to post on a Website or Service.</li>
            <li><strong>Alumni surveys:</strong>  If you provide information to us through an alumni survey at the invitation of your alma mater, we may provide non-personally identifiable information in its raw form or in the aggregate to your alma mater for research and analysis.  Your personal data such as your name and email address will not be shared.</li>
        </ul>
        <h2>When do we share information?</h2>

        <p>We occasionally share information collected from individuals or their employers with the following:</p>
        <ul>
            <li>subsidiaries, affiliates, employees, contractors, and third-party service providers who access, use, store, or process relevant data on our behalf to provide the Website and Services;</li>
            <li>third-party social media, email, or other similar providers, such as LinkedIn, Facebook, and Twitter, where a user consents and agrees to the third-party's privacy policy and terms of use.</li>
            <li>distributors and resellers to fulfill Service and information requests and to provide our customers and prospective customers with information about us and our Services;</li>
            <li>partners or other third-parties with whom we jointly offer related services; and</li>
            <li>other companies if we consider, experience, or prepare for a business transaction, such as a merger with or an acquisition by another company, receive funding, or sell all or a portion of our assets.</li>
        </ul>

        <p>Here are some common situations describing when and why we share information:</p>
        <ul>
            <li><strong>Other PayScale users:</strong>  We share non-personally identifiable information in an anonymous and de-identified form with other PayScale users and our customers.  For example, you may see an anonymous profile that includes the job title, years of experience, and salary information of another PayScale user with a similar profile as you.</li>
            <li><strong>Service providers:</strong>  We use third-party products and services for a variety of reasons, such as for data analytics, marketing, accounting, and legal services.  If we share information with a service provider, the information shared is limited to what's relevant to such provider's service, such provider's use of the information must follow our obligations under this Privacy Policy or our agreement with you, such provider must agree to appropriate confidentiality obligations in our contracts with them, and we are responsible for such provider's failure to do so unless we can show we are not responsible.</li>
            <li><strong>Sharing Website content through social media or email:</strong>  If you share content from a Website or Service to others through a third-party operated social media or email service (including, through Facebook, LinkedIn, Instagram, or Twitter plugins), you understand that we track when our content is shared and that your sharing is subject to the terms of use and privacy policy of the third-party provider of such service.</li>
            <li><strong>Sale of our business or bankruptcy:</strong>  If we sell our assets to or merge with another company or file for bankruptcy, information we receive from you from our Website and Services is a PayScale asset and may be transferred.  If another company buys us or a part of our business, we will have the right to continue to use your data and information in the manner set out in this Privacy Policy unless you agree otherwise.</li>
            <li><strong>Legal Disclosures:</strong>  We reserve the right to disclose your personal data and other information solely to the extent required by applicable laws and when we believe disclosure is necessary to protect our rights (including, but not limited to, intellectual property rights) or to comply with our legal obligations, a judicial proceeding, court order, or other legal process.</li>
        </ul>

        <h2>Does PayScale use cookies and similar technologies?</h2>

        <p>
            Yes, we do. 
            Cookies and similar technologies help us deliver our Website and Services so that they work correctly and are delivered in a way that is valuable to our visitors and users.
            Cookies are small text files that are automatically placed on your computer or mobile device when you visit a web site. They are stored by your Internet browser.
            Cookies contain basic information about your Internet use, but they do not normally identify you personally.
            Your browser sends these cookies back to a web site every time you revisit it, so it can recognize your computer or mobile device and personalize your experience.
            To learn about the types of cookies we use and why we use them, go <a href="/cookie-policy">here</a>.
            If you want to learn more about cookies, or how to control, disable, or delete them, check out <a rel="nofollow" href="http://www.aboutcookies.org/">www.aboutcookies.org</a>.
            For Google Analytics Advertising Features, you can opt-out through Google Ads Settings.  In addition, there are various popular products that provide privacy plugins such as Ghostery and AdBlock Plus.  Note that if you choose to delete or disable any cookies, our Website or Services may not function properly and your access to some parts of our Website or Services may be degraded or restricted.
        </p>

        <h2>Are you located outside of the United States?</h2>

        <p>
            PayScale is located and headquartered in the United States (U.S.) and our Website and Services are hosted in the U.S.
            If you are visiting or providing information from a country outside of the U.S. with laws governing data protection, collection, or use that may differ from U.S. laws,
            note that you are transferring your data to the U.S., which may have different data protection laws.
            You can learn more about our data protection measures and our commitment to comply with applicable data protection laws <a href="https://www.payscale.com/data-protection">here</a>.
        </p>

        <p>By providing us your personal data through your use of a Website or Service, you consent to:</p>
        <ul>
            <li>use of your personal data as described in this Privacy Policy;</li>
            <li>your personal data being transferred to the U.S.;</li>
            <li>the access, use, storage, and other processing of your personal data in the U.S.; and </li>
            <li>jurisdiction for any data issues shall be settled in the courts located in King County, in the State of Washington, United States of America.</li>
        </ul>

        <h2>Why do I see a message that says, "Data withheld for Privacy"?</h2>

        <p>
            When you view a salary report, you may see a message that says, "Data withheld for privacy" (or something like it).
            This message is not removed until the data in our system meets applicable legal guidelines (such as the Antitrust Safety Zone established by the U.S. Department of Justice and
            Federal Trade Commission you can learn about <a rel="nofollow" href="https://www.justice.gov/atr/statements-antitrust-enforcement-policyin-health-care#CONTNUM_49">here</a>.  To protect the privacy and rights of individuals or a company,
            we only display data that has been rendered anonymous by removing various combinations of information, such as presenting an average or aggregate of multiple data points,
            limiting the base number of employers in any analysis to five or more, and pulling back from a local search to a broader geographic area. 
        </p>

        <h2>Can I change my account Information and preferences?</h2>

        <p>Yes, you can edit your PayScale account information and preferences at any time.  If your information changes, you may delete, correct, or update it by making the change in your PayScale account settings on your own.  We retain the personal data you provide while your PayScale account is in existence or as required to provide you the Website and Services. </p>

        <h2>How do I deactivate my PayScale account? </h2>

        <p>If you wish to deactivate your PayScale account, you may do so by emailing our support team at cs@payscale.com.  After your PayScale account has been deactivated, you will no longer be able to access your PayScale account, but we will continue to retain certain portions of your data even after you have closed your PayScale account if reasonably necessary to comply with our legal obligations, meet regulatory requirements, resolve disputes, maintain security, prevent fraud and abuse, enforce our agreement with you, or fulfill your request to "unsubscribe" from further messages from us.  Also, even after your PayScale account is closed, we retain de-personalized information you have provided such as de-personalized information included in our anonymized and aggregated salary database. </p>

        <h2>What if I purchased a business product from PayScale?</h2>

        <p>
            We offer various Services for use by businesses to help them manage the compensation of their employees and attract and retain talent.
            You can learn more about our compensation products for businesses <a href="https://www.payscale.com/hr">here</a>.  Our Services for businesses are generally governed by a separate agreement.
            If there's a conflict between this Privacy Policy and the terms of that agreement regarding the business Service,
            the terms of that agreement will control.  When a business evaluates, purchases, or subscribes to our business Services, or obtains support for such Services,
            we collect data and information to provide and improve the Service, create improved user experiences, and operate our business.
        </p>

        <h2>Can PayScale make changes to this Privacy Policy?</h2>

        <p>Yes, we may make changes to this Privacy Policy to align with our operations and evolving laws.  If we make changes to this Privacy Policy, we will post those changes here and other appropriate places.  We reserve the right to modify this Privacy Policy at any time, so please review it regularly.  If we make significant changes, we will notify you here, by email, or by other reasonable means.  Your continued use of our Website and Services constitutes your agreement to this Privacy Policy and any updates to this Privacy Policy.</p>

        <h2>Have additional questions?</h2>

        <p>If you have a privacy question or concern, you can contact us at:</p>

        <address>
            PayScale, Inc.<br>
            1000 1st Avenue South<br>
            Seattle, Washington 98134<br>
            Attention:  Legal Department<br>
            Email: legal@payscale.com<br>
        </address>

    </div>

				</td>
	</tr>
</table>
	
	</div>
				
</div>

<footer id="pxl-footer" class="pxl-footer"><div class="pxl-footer-content"><div class="pxl-footer-links"><span class="pxl-footer-link-item"><a href="/about">About Us</a></span><span class="pxl-footer-link-item"><a href="/about/press-releases">Press Center</a></span><span class="pxl-footer-link-item"><a href="/about/jobs">Work With Us</a></span><span class="pxl-footer-link-item"><a href="/about/methodology">Methodology</a></span><span class="pxl-footer-link-item"><a href="/about/contact">Contact Us</a></span><span class="pxl-footer-research-links"><a href="/research/US/Country=United_States/Salary" class="pxl-footer-strong">Research Careers:</a><a href="/research/US/Job"> Jobs</a>, <a href="/research/US/Employer">Companies</a>,<a href="/research/US/Degree"> Degrees</a>,<a href="/index"> All</a></span></div><div class="pxl-footer-logos"><img src="//cdn-payscale.com/content/Payscale_Icon_White.svg"/></div><div class="pxl-footer-social-logos"><a href="https://www.facebook.com/PayScale/" rel="nofollow"><img src="//cdn-payscale.com/content/Facebook_Icon.svg" height="15px"/></a><a href="https://www.linkedin.com/company/payscale-inc-" rel="nofollow"><img src="//cdn-payscale.com/content/LinkedIn_Icon.svg" height="15px"/></a><a href="https://twitter.com/payscale" rel="nofollow"><img src="//cdn-payscale.com/content/Twitter_Icon.svg" height="15px"/></a><a href="https://www.instagram.com/payscale/" rel="nofollow"><img src="//cdn-payscale.com/content/Instagram_Icon.svg" height="15px"/></a></div><div class="pxl-footer-clear"></div></div><div class="pxl-footer-bottom"><div class="pxl-footer-copyright">Â© 2018 PayScale, Inc. All rights reserved.</div><div class="pxl-footer-privacy"><ul><li class="pxl-privacy-item"><a href="/data-protection">Data Protection</a></li><li class="pxl-privacy-item"><a href="/privacy.aspx">Privacy Policy</a></li><li class="pxl-privacy-item"><a href="/license">Terms of Use</a></li></ul></div></div></footer>
<script></script>



<div id="pageBottom"></div>

<script>
    
</script>
<script type="text/javascript">
!function(e){function t(s){if(n[s])return n[s].exports;var a=n[s]={i:s,l:!1,exports:{}};return e[s].call(a.exports,a,a.exports,t),a.l=!0,a.exports}var n={};t.m=e,t.c=n,t.i=function(e){return e},t.d=function(e,n,s){t.o(e,n)||Object.defineProperty(e,n,{configurable:!1,enumerable:!0,get:s})},t.n=function(e){var n=e&&e.__esModule?function(){return e.default}:function(){return e};return t.d(n,"a",n),n},t.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},t.p="",t(t.s=49)}({49:function(e,t,n){"use strict";!function(e){function t(e,t){return e&&e.classList&&e.classList.value&&-1!==e.classList.value.indexOf(t)}var n=new XMLHttpRequest,s="/WebServices/UserContext.asmx/GetUserContext";"undefined"!=typeof payscaleHost&&(s=""+payscaleHost+s),n.open("POST",s,!0),n.setRequestHeader("Content-Type","application/json; charset=utf-8"),n.onload=function(){if(n.status>=200&&n.status<400){var e=JSON.parse(n.responseText).d,t=document.cookie.split("; "),s=t.some(function(e){var t=e.split("=");return"accept-cookie"===t[0]&&("yes"===t[1]||"na"===t[1])});e.loggedIn&&(document.getElementById("ps-navbar-phone").classList.add("hidden"),document.getElementById("ps-navbar-login").classList.add("hidden"),document.getElementById("ps-navbar-myaccount").classList.remove("hidden"),1!==e.accountTypeGeneral&&document.getElementById("ps-navbar-business-dashboard").classList.remove("hidden")),s&&(e.loggedIn?document.cookie="isLoggedIn=true":document.cookie="isLoggedIn=false",e.hasConsumerProfile?document.cookie="hasReport=true":document.cookie="hasReport=false")}},n.send(),e&&2===e.pageType&&document.getElementById("ps-navbar-phone").classList.remove("hidden"),document.getElementById("navbar-mobile-menu").addEventListener("click",function(){var e=document.getElementById("navbar");t(e,"active")?(e.classList.remove("active"),document.body.classList.remove("pxl-body-navbar")):(e.classList.add("active"),document.body.classList.add("pxl-body-navbar"))});for(var a=document.getElementsByClassName("pxl-navbar-collapse"),o=0;o<a.length;o++){a[o].addEventListener("click",function(e){var n=this.parentElement;t(document.getElementById("navbar"),"active")&&(t(n,"active")?n.classList.remove("active"):n.classList.add("active"))})}}(window)}});
</script>
</body>
</html>
