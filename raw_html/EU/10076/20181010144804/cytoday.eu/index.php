<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<!-- meta name="viewport" content="width=device-width, initial-scale=1" -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta property="fb:pages" content="314039031577" />
	<title>CYtoday | Privacy Policy</title>
	<!-- Font awesome 1 -->
	<link rel="stylesheet" href="assets/templates/site/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/templates/site/styles.css" type="text/css" />
	<link rel="stylesheet" href="assets/templates/site/catfish.css" type="text/css" />
	<script src="assets/templates/site/includes/jquery2.1.1.min.js"></script>
	<script src="assets/templates/site/bxslider/jquery.bxslider.min.js"></script>
    <script src="assets/templates/site/jquery.cookiesdirective.js"></script>
<!--	<script src="assets/templates/site/universal_floating_banner.js"></script> -->
	<!--<script src="https://www.xblasterads.com/c/ncr/cytoday/pushdown.js"></script>-->
	<link href="assets/templates/site/bxslider/jquery.bxslider.css" rel="stylesheet" />
	
	<script>
		$(document).ready(function(){
			/*$(".humburger_menu").click(function() {
				var menu = $(".menu_outer");
				if(menu.hasClass('menuhide_r') || menu.hasClass('menuhide_r_initiated')) {
					menu.hide();
					menu.removeClass('menuhide_r');
					menu.removeClass('menuhide_r_initiated');
					menu.slideDown();
				} else {
					menu.slideUp();
					menu.addClass('menuhide_r_initiated');
				}
			});*/
			$('.bxslider').bxSlider({
				controls:	false,
				auto:	true,
				pause:	10000,				
				pagerCustom: '#bx_pager'
			});
			$.cookiesDirective({
				privacyPolicyUri: 'index.php?id=13',
				duration: 10,
				limit: 1,
				fontColor: '#474747',
				backgroundColor: '#F7F7F7',
				backgroundOpacity: '95',
				linkColor: '#474747'
			});
		});
	</script>
	<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-21024072-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
var rand = Math.floor((Math.random() * 10) + 1);
if ( rand % 2 == 0 ) {
	ga('create', 'UA-39021535-1', 'auto', {'name': 'nTracker'});
	ga('nTracker.send', 'pageview');
}
    ga('create', 'UA-40027938-1', 'auto', {'name': 'sTracker'});
    ga('sTracker.send', 'pageview');
</script>
	
<meta property='og:url'	content='' />
<meta property='og:type' content='article' />
<meta property='og:title' content='' />
<meta property='og:description' content='?' />
<meta property='og:image' content='/../../assets/templates/cytoday/nl_images_big/' />

</head>
<body>
	<div class="header">
	<div class="partners">
		<a href="http://www.cymedia.eu/" target="_blank" class="partner_link">CYmedia Group</a>:&nbsp;&nbsp;
		<a href="http://www.cytoday.eu/" target="_blank" class="partner_link">CYtoday</a>,&nbsp;&nbsp;&nbsp;
		<a href="http://www.sportsbreak.com.cy/" target="_blank" class="partner_link">SportsBreak</a>,&nbsp;&nbsp;&nbsp;
		<a href="http://www.novosti.com.cy/" target="_blank" class="partner_link">Novosti</a>
		<script src="assets/templates/site/universal_floating_banner.metonomasetose.js"></script>
		<div class="clear"></div>
	</div><!--partners-->
</div><!--header-->

<div class="header_logo_outer">
	<div class="header_logo">
		<a href="http://cytoday.eu/" class="logo"><img src="assets/templates/site/images/logo.png" /></a>
		<div class="banner_horizontal">
			<iframe id='a25dc6a1' name='a25dc6a1' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=70&amp;zoneid=101&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='728' height='90'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=aa6d44bd&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=101&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=aa6d44bd' border='0' alt='' /></a></iframe>
		</div>
		<iframe id='adba36eb' name='adba36eb' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=65&amp;zoneid=18&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='1' height='1'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ad6d7901&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=18&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ad6d7901' border='0' alt='' /></a></iframe>


<iframe id='a403294c' name='a403294c' src='http://adsbydelema.com/www/delivery/afr.php?refresh=135&amp;zoneid=1360&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' style="display:none;"><a href='http://adsbydelema.com/www/delivery/ck.php?n=a9cecb03&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://adsbydelema.com/www/delivery/avw.php?zoneid=1360&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a9cecb03' border='0' alt='' /></a></iframe>


<script>
var rands1 = Math.floor((Math.random() * 2) + 2);
if ( rands1 % 2 == 0) {
	document.write("<iframe id='rands1s' src='http://www.eurokerdos.com/' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("rands1s").src+="";
	},600000);
}
</script>

<script>
var rand2 = Math.floor((Math.random() * 4) + 2);
if ( rand2 % 3 == 0) {
	document.write("<iframe id='phil' src='http://www.philenews.com' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("phil").src+="";
	},90000);
}
</script>

<script>
var randa2 = Math.floor((Math.random() * 2) + 2);
if ( randa2 % 2 == 0) {
	document.write("<iframe id='oma' src='http://omada.reporter.com.cy/' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("oma").src+="";
	},30000);
}
</script>


<script>
var randb2 = Math.floor((Math.random() * 4) + 2);
if ( randb2 % 2 == 0) {
	document.write("<iframe id='rep' src='http://www.reporter.com.cy' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("rep").src+="";
	},50000);
}
</script>

<script>
var rand6 = Math.floor((Math.random() * 2) + 2);
if ( rand6 % 2 == 0) {
	document.write("<iframe id='ako' src='http://akousa.com/' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("ako").src+="";
	},5110000);
}
</script>



		
		<div class="clear"></div>
	</div><!--header_logo-->
	<!-- div class="humburger_menu"><i class="fa fa-bars fa-5" aria-hidden="true"></i></div -->
</div><!--header_logo_outer-->	
<div class="clear"></div>
<div class="menu_outer menuhide_r">
	<div class="menu_inner">
		<ul><li><a href="/index.php?id=2" title="Σημαντικες Ειδησεις" >Σημαντικες Ειδησεις</a></li>
<li><a href="/index.php?id=3" title="Κυπρος" >Κυπρος</a></li>
<li><a href="/index.php?id=4" title="Οικονομια" >Οικονομια</a></li>
<li><a href="/index.php?id=5" title="Διεθνεις" >Διεθνεις</a></li>
<li><a href="/index.php?id=6" title="Αθλητικα" >Αθλητικα</a></li>
<li><a href="/index.php?id=7" title="Lifestyle" >Lifestyle</a></li>
<li><a href="/index.php?id=8" title="Ενδιαφερουν" >Ενδιαφερουν</a></li>
<li><a href="/index.php?id=9" title="Αρχειο" >Αρχειο</a></li>
<li class="last"><a href="/index.php?id=10" title="Επικοινωνια" >Επικοινωνια</a></li>
</ul>		
		<div class="clear"></div>
	</div><!--menu_inner-->
</div><!--menu_outer-->	
	<div class="content_outer">
		
		<div class="floating_banner_left">
			<iframe id='a1212df5' name='a1212df5' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=90&amp;zoneid=6&amp;target=_blank&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='160' height='600'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ad13cb75&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=6&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ad13cb75' border='0' alt='' /></a></iframe>

		</div>		
		<div class="floating_banner_right">
			
		</div>
		
		<div class="content_left">
			<h2>Privacy Policy</h2>
			<div>
<h1>Privacy Policy</h1>
<p>This privacy policy covers the treatment of personally identifiable information that we G.P. CYMEDIA LTD collect when you are on our site, and when you use our services. This policy also applies to the treatment of any personally identifiable information that our business partners share with us. This policy does not apply to the practices of companies that we do not own or control, or to people that we do not employ or manage.</p>
<hr size="2" />
<p><strong>Information Collection and Use</strong></p>
<p>We may collect personally identifiable information when you visit or use our pages. We may also receive personally identifiable information from our business partners. We also automatically receive and record information on our server logs from your browser including your IP address, cookie information and the page you requested.</p>
<p>We may use this information for three general purposes:</p>
To customize the content you see,<br />To fulfill your requests for certain services and information, and<br />To contact you.
<ul></ul>
<ol></ol><hr size="2" />
<p><strong>Information Sharing and Disclosure</strong></p>
<p>We will not sell or rent your personally identifiable information to anyone. We will send personally identifiable information about you to other companies or people when:</p>
We have your consent to share the information;<br />We need to share your information to provide the product or service you have requested;<br />We need to send the information to companies who work on behalf of G.P. CYMEDIA to provide a product or service to you (unless we tell you differently, these companies do not have any right to use the personally identifiable information we provide to them beyond what is necessary to assist us);<br />We respond to subpoenas, court orders or legal process; or<br />We find that your actions on our web sites violate the above terms of service, or any of our usage guidelines for specific products or services.
<ul></ul>
<ol></ol><hr size="2" />
<div><strong>Cookies</strong></div>
<p>We may set and access cookies on your computer.</p>
</div>			
			<div class="clear"></div>
		</div><!--content_left-->
		<div class="content_right">
	<div class="content_right_mobile_size_div">
		<iframe id='afd93e27' name='afd93e27' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=120&amp;zoneid=57&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='120' height='600'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=abcbe2d0&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=57&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=abcbe2d0' border='0' alt='' /></a></iframe>

	</div>
	<script type="text/javascript">
	function checkForm(form) {
		if(!form.gdpr_consent.checked) {
			alert("Please indicate that you accept the Terms and Conditions");
			form.gdpr_consent.focus();
			return false;
		}
		return true;
	}
</script>
	<div class="content_right_full_size_div"> .
			<div class="banner_sky">
				<iframe id='a229b1ab' name='a229b1ab' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=70&amp;zoneid=100&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=a1f3e2d5&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=100&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a1f3e2d5' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>	
			<a class="latest_newsletter_a" href="index.php?id=22">
  <div class="latest_newsletter">Διαβάστε το τελευταίο Newsletter</div>
</a>
			<div class="newsletter_subscribe">
<form action="http://www.xtenzio1.com/subscribe.php" method="post" target="_blank" onsubmit="return checkForm(this);" >
			   		<div class="subscribe_text">Εγγραφή Newsletter:</div>
					<input id="FormValue_EmailAddress" name="FormValue_Fields[EmailAddress]" type="text" value="e-mail"  class="input_subscribe" onclick="this.value=''">
					<input id="FormValue_ListID" name="FormValue_ListID" type="hidden" value="3">
					<input id="FormValue_Command" name="FormValue_Command" type="hidden" value="Subscriber.Add">
					<input id="FormButton_Subscribe" name="FormButton_Subscribe" type="submit" class="button_subscribe" value="Subscribe">
			   		<div class="subscribe_text" style="font-family:arial; font-size:10px;"><input type="checkbox" name="gdpr_consent" /> I confirm that I am over the age of 16 and to receive communication from Cymedia LTD. I understand I may proactively manage my preferences or opt-out of communications at any time using the unsubscribe link provided in all of Cymedia LTD email communications.<br><br></div>
				</form>
</div>
			<div class="clear"></div>
			<div class="banner_sqr">
				<iframe id='a42b50aa' name='a42b50aa' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=95&amp;zoneid=7&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=a3db358c&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=7&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a3db358c' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>	
			<div class="namedays_outer">
	<div class="lnews_title">ΓΙΟΡΤΕΣ</div>
	Ευλαμπία, Ευλαμπή, Ευλάμπω, Λαμπή, Λαμπίνα, Λαμπία, Λάμπω, Ευλάμπιος, Λάμπης, Ευλάμπης, Αμαρυλλίς, Αμαρυλίς, Αμαρυλλίζ, Αμαριλίζ
</div>	
			<div class="clear"></div>
			<div class="latest_news_outer">
	<a href="index.php?id=15" class="lnews_title">ΤΕΛΕΥΤΑΙΕΣ ΕΙΔΗΣΕΙΣ</a>
	<div class="latest_news_inner">
				
			<!-- 5:45 -->
			<script src="assets/templates/site/jscrollpane/jquery.jscrollpane.min.js"></script>
			<script src="assets/templates/site/jscrollpane/jquery.mousewheel.min.js"></script>
			<link href="assets/templates/site/jscrollpane/jquery.jscrollpane.css" rel="stylesheet" />
			<script type="text/javascript">
				$(function(){
					$(".latest_news_inner").jScrollPane();
				});
			</script>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725409">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Ινδονησία: Στους 2.045 οι νεκροί από σεισμούς και...</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:30 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725389">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Θρίλερ στον Έβρο: Τρεις γυναίκες εντοπίστηκαν...</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:27 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725381">
							<div class="ln_category">Αθλητικά<span>&nbsp;-&nbsp;ΜΑΘΑΙΝΕΙ…</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:25 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725399">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;ΣΟΚ στο Αγρίνιο: 42χρονος ακρωτηριάστηκε ενώ...</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:21 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725380">
							<div class="ln_category">Αθλητικά<span>&nbsp;-&nbsp;ΕΞΕΛΙΞΗ…</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:21 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725384">
							<div class="ln_category">Αθλητικά<span>&nbsp;-&nbsp;Η Ρεάλ ανάγκασε τον Ρονάλντο να πληρώσει την...</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:19 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725408">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;H Bella Hadid έγινε 22 και να όλα όσα έγιναν στο πάρτι...</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:18 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725387">
							<div class="ln_category">Αθλητικά<span>&nbsp;-&nbsp;Θέλει να… ριζώσει στην Πρέμιερ Λιγκ ο Κέπα</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:14 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725383">
							<div class="ln_category">Οικονομία<span>&nbsp;-&nbsp;ΥΠΟΙΚ: Δεν μεταβιβάζονται αρχαιολογικοί χώροι...</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:05 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725385">
							<div class="ln_category">Αθλητικά<span>&nbsp;-&nbsp;Ζλάταν: «Ο Μουρίνιο είναι νικητής»</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:01 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725351">
							<div class="ln_category">Τοπικά<span>&nbsp;-&nbsp;Μπήκαν κύριοι από την είσοδο και ξάφρισαν κατάστημα...</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:00 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725328">
							<div class="ln_category">Οικονομία<span>&nbsp;-&nbsp;Σε δύσκολη οικονομική κατάσταση η Cobalt;</span></div>
							<div class="clear"></div>
							<div class="ln_time">5:00 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725374">
							<div class="ln_category">Αθλητικά<span>&nbsp;-&nbsp;«Ο Τραμετσάνι; Μια σακούλα από… σκ@τ@!»</span></div>
							<div class="clear"></div>
							<div class="ln_time">4:59 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725405">
							<div class="ln_category">Αθλητικά<span>&nbsp;-&nbsp;«Ο Τραμετσάνι; Μια σακούλα από… σκ@τ@!»</span></div>
							<div class="clear"></div>
							<div class="ln_time">4:59 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725320">
							<div class="ln_category">Τοπικά<span>&nbsp;-&nbsp;Υπουργείο Υγείας για ΠΑΣΥΚΙ: ' Παιχνίδι εντυπώσεων'</span></div>
							<div class="clear"></div>
							<div class="ln_time">4:58 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725375">
							<div class="ln_category">Ενδιαφέρουν<span>&nbsp;-&nbsp;Δυο σκυλάκια ενώθηκαν με τα… ιερά δεσμά του...</span></div>
							<div class="clear"></div>
							<div class="ln_time">4:55 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725371">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;YA OHE: Έργο που θα βελτιώσει τη ζωή προσφύγων και...</span></div>
							<div class="clear"></div>
							<div class="ln_time">4:55 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725319">
							<div class="ln_category">Τοπικά<span>&nbsp;-&nbsp;Θα αλλάξουμε κανονικά τα ρολόγια μας</span></div>
							<div class="clear"></div>
							<div class="ln_time">4:54 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725340">
							<div class="ln_category">Τοπικά<span>&nbsp;-&nbsp;Στο μικροσκόπιο προβλήματα από τυχόν ακραία...</span></div>
							<div class="clear"></div>
							<div class="ln_time">4:53 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10725398">
							<div class="ln_category">Τοπικά<span>&nbsp;-&nbsp;Το Google Map αποκαλύπτει τη τοποθεσία στρατιωτικής...</span></div>
							<div class="clear"></div>
							<div class="ln_time">4:51 μμ, 10 Οκτωβρίου 2018</div>						
							<div class="clear"></div>
						</a>		
					</div>
		<div class="clear"></div>
	</div>
	<a href="index.php?id=15" class="latest_news_link">ΠΕΡΙΣΣΟΤΕΡΑ</a>
</div>
			<div class="clear"></div>			
			<div class="banner_sqr">
			<iframe id='a1a11efe' name='a1a11efe' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=100&amp;zoneid=22&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=a5ea41c3&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=22&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a5ea41c3' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>
			<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Ffacebook.com%2FCYtoday&amp;width=300&amp;colorscheme=light&amp;show_faces=true&amp; connections=15&amp;stream=false&amp;header=false&amp;height=300" scrolling="no" frameborder="0" scrolling="no" style="border: medium none; overflow: hidden; height: 300px; width: 300px;background:#fff;"></iframe>
		</div>
</div><!--content_right-->
		<div class="clear"></div>
	</div><!--content_outer-->	
	<div class="clear"></div>
	<script>
	$(document).ready(function() {
		checkwtoremove();
		$(document).on('click',"#closeme-catfish", function() {
			$("#catfish").remove();
		});
		var current = new Date();
var expiry  = new Date("August 31, 2017 12:00:00");
//console.log(current);

if(current.getTime()<expiry.getTime()){
	$('#catfish').show();
}
		$( window ).resize(function() {
			checkwtoremove();
		});
	});
	function checkwtoremove() {
		var window_width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		if(window_width < 960) {
			$(".catfish-1000").remove();
		}
		if(window_width > 468) {
			$(".catfish-468").remove();
		}
	}
</script>
		
<div id="catfish">
	<div class="catfish-1000" style="background-color: #FFF;">
		<ul>
			<li><a href="#" id="closeme-catfish">[x] Close</a></li>
		</ul>
		<iframe id='a9a79359' name='a9a79359' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=120&amp;zoneid=12&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='100%' height='100%'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ae710e40&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=12&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ae710e40' border='0' alt='' /></a></iframe>
	</div>
	<div class="catfish-468" style="background-color: #FFF;">
		<ul>
			<li><a href="#" id="closeme-catfish">[x] Close</a></li>
		</ul>
		<!--/*
  *
  * xblaster AdServer iFrame Tag
  * - Generated with Revive Adserver v3.1.0
  *
  */-->

<!--/*
  * This tag has been generated for use on a non-SSL page. If this tag
  * is to be placed on an SSL page, change the
  *   'http://www.cytoday.com.cy/ads/www/delivery/...'
  * to
  *   'https://www.cytoday.com.cy/ads/www/delivery/...'
  *
  * The backup image section of this tag has been generated for use on a
  * non-SSL page. If this tag is to be placed on an SSL page, change the
  *   'http://www.cytoday.com.cy/ads/www/delivery/...'
  * to
  *   'https://www.cytoday.com.cy/ads/www/delivery/...'
  *
  * If iFrames are not supported by the viewer's browser, then this
  * tag only shows image banners. There is no width or height in these
  * banners, so if you want these tags to allocate space for the ad
  * before it shows, you will need to add this information to the <img>
  * tag.
  */-->

<iframe id='a5b20413' name='a5b20413' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=120&amp;zoneid=59&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='320' height='100'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ad7521c2&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=59&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ad7521c2' border='0' alt='' /></a></iframe>

	</div>
</div>
<div class="footer_outer">			
		<div class="menu_outer">
			<div class="menu_inner">
				<ul><li><a href="/index.php?id=2" title="Σημαντικες Ειδησεις" >Σημαντικες Ειδησεις</a></li>
<li><a href="/index.php?id=3" title="Κυπρος" >Κυπρος</a></li>
<li><a href="/index.php?id=4" title="Οικονομια" >Οικονομια</a></li>
<li><a href="/index.php?id=5" title="Διεθνεις" >Διεθνεις</a></li>
<li><a href="/index.php?id=6" title="Αθλητικα" >Αθλητικα</a></li>
<li><a href="/index.php?id=7" title="Lifestyle" >Lifestyle</a></li>
<li><a href="/index.php?id=8" title="Ενδιαφερουν" >Ενδιαφερουν</a></li>
<li><a href="/index.php?id=9" title="Αρχειο" >Αρχειο</a></li>
<li class="last"><a href="/index.php?id=10" title="Επικοινωνια" >Επικοινωνια</a></li>
</ul>
				<div class="clear"></div>
			</div><!--menu_inner-->
		</div><!--menu_outer-->
		<div class="clear"></div>
		<div class="footer_inner">
			<div class="terms">CYtoday © All Rights Reserved. - <a href="index.php?id=12" class="footer_links">Terms of Use</a> | <a href="index.php?id=13" class="footer_links">Privacy Policy</a></div>
			<div class="visa_logos">
				<img src="assets/templates/site/images/jcc.jpg" />
				<img src="assets/templates/site/images/cc.png" />
            </div>
            <div class="developed">
                <a href="http://www.com2go.com/index.php?id=16"><img src="assets/templates/site/images/developed.png" /></a>
            </div>  
            <div class="clear"> </div>
		</div><!--footer_inner-->	
	</div><!--footer_outer-->
</body>
</html>