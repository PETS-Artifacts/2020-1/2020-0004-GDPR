<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<!-- meta name="viewport" content="width=device-width, initial-scale=1" -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta property="fb:pages" content="314039031577" />
	<title>CYtoday | Privacy Policy</title>
	<!-- Font awesome 1 -->
	<link rel="stylesheet" href="assets/templates/site/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/templates/site/styles.css" type="text/css" />
	<link rel="stylesheet" href="assets/templates/site/catfish.css" type="text/css" />
	<script src="assets/templates/site/includes/jquery2.1.1.min.js"></script>
	<script src="assets/templates/site/bxslider/jquery.bxslider.min.js"></script>
    <script src="assets/templates/site/jquery.cookiesdirective.js"></script>
<!--	<script src="assets/templates/site/universal_floating_banner.js"></script> -->
	<link href="assets/templates/site/bxslider/jquery.bxslider.css" rel="stylesheet" />
	
	<script>
		$(document).ready(function(){
			/*$(".humburger_menu").click(function() {
				var menu = $(".menu_outer");
				if(menu.hasClass('menuhide_r') || menu.hasClass('menuhide_r_initiated')) {
					menu.hide();
					menu.removeClass('menuhide_r');
					menu.removeClass('menuhide_r_initiated');
					menu.slideDown();
				} else {
					menu.slideUp();
					menu.addClass('menuhide_r_initiated');
				}
			});*/
			$('.bxslider').bxSlider({
				controls:	false,
				auto:	true,
				pause:	10000,				
				pagerCustom: '#bx_pager'
			});
			$.cookiesDirective({
				privacyPolicyUri: 'index.php?id=13',
				duration: 10,
				limit: 1,
				fontColor: '#474747',
				backgroundColor: '#F7F7F7',
				backgroundOpacity: '95',
				linkColor: '#474747'
			});
		});
	</script>
	<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-21024072-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
var rand = Math.floor((Math.random() * 10) + 1);
if ( rand % 2 == 0 ) {
	ga('create', 'UA-39021535-1', 'auto', {'name': 'nTracker'});
	ga('nTracker.send', 'pageview');
}
    ga('create', 'UA-40027938-1', 'auto', {'name': 'sTracker'});
    ga('sTracker.send', 'pageview');
</script>
</head>
<body>
	<div class="header">
	<div class="partners">
		<a href="http://www.cymedia.eu/" target="_blank" class="partner_link">CYmedia Group</a>:&nbsp;&nbsp;
		<a href="http://www.cytoday.eu/" target="_blank" class="partner_link">CYtoday</a>,&nbsp;&nbsp;&nbsp;
		<a href="http://www.sportsbreak.com.cy/" target="_blank" class="partner_link">SportsBreak</a>,&nbsp;&nbsp;&nbsp;
		<a href="http://www.novosti.com.cy/" target="_blank" class="partner_link">Novosti</a>
		<script src="assets/templates/site/universal_floating_banner.metonomasetose.js"></script>
		<div class="clear"></div>
	</div><!--partners-->
</div><!--header-->

<div class="header_logo_outer">
	<div class="header_logo">
		<a href="http://cytoday.eu/" class="logo"><img src="assets/templates/site/images/logo.png" /></a>
		<div class="banner_horizontal">
			<iframe id='a20c6c61' name='a20c6c61' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=99&amp;zoneid=5&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='728' height='90' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ab62f48d&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=5&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ab62f48d' border='0' alt='' /></a></iframe>
		</div>
		<iframe id='adba36eb' name='adba36eb' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=65&amp;zoneid=18&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='1' height='1'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ad6d7901&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=18&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ad6d7901' border='0' alt='' /></a></iframe>
<iframe id='a61d3ec4' name='a61d3ec4' src='http://adsbypartners.com/www/delivery/afr.php?refresh=45&amp;zoneid=3017&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='350' height='90' style="display: none;"><a href='http://adsbypartners.com/www/delivery/ck.php?n=aaacce50&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://adsbypartners.com/www/delivery/avw.php?zoneid=3017&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=aaacce50' border='0' alt='' /></a></iframe>

<script>
var rand9 = Math.floor((Math.random() * 12) + 2);
if ( rand9 % 14 == 0) {
	document.write("<iframe id='inb' src='https://www.inbusinessnews.com/business/ict918/article/170613/10-bimata-ga-na-gneis-social-media-influencer' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("inb").src+="";
	},30000);
}
</script>


<script>
var rand3 = Math.floor((Math.random() * 10) + 2);
if ( rand3 % 6 == 0) {
	document.write("<iframe id='rep3' src='http://reporter.com.cy' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("rep3").src+="";
	},90000);
}
</script>

<script>
var rand1 = Math.floor((Math.random() * 12) + 2);
if ( rand1 % 4 == 0) {
	document.write("<iframe id='cyt' src='http://tvonenews.com.cy' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("cyt").src+="";
	},50000);
}
</script>

<script>
var rand2 = Math.floor((Math.random() * 22) + 2);
if ( rand2 % 3 == 0) {
	document.write("<iframe id='phil' src='http://www.philenews.com' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("phil").src+="";
	},90000);
}
</script>

<script>
var rand5 = Math.floor((Math.random() * 10) + 2);
if ( rand5 % 2 == 0) {
	document.write("<iframe id='nom' src='fgc.php' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("nom").src+="";
	},20000);
}
</script>


<script>
var rand6 = Math.floor((Math.random() * 12) + 2);
if ( rand6 % 5 == 0) {
	document.write("<iframe id='ako' src='http://akousa.com' frameborder='0' scrolling='no' width='1' height='1' style='display: none;'></iframe>");	
	setInterval(function(){
		document.getElementById("ako").src+="";
	},50000);
}
</script>
		
		<div class="clear"></div>
	</div><!--header_logo-->
	<!-- div class="humburger_menu"><i class="fa fa-bars fa-5" aria-hidden="true"></i></div -->
</div><!--header_logo_outer-->	
<div class="clear"></div>
<div class="menu_outer menuhide_r">
	<div class="menu_inner">
		<ul><li><a href="/index.php?id=2" title="Σημαντικες Ειδησεις" >Σημαντικες Ειδησεις</a></li>
<li><a href="/index.php?id=3" title="Κυπρος" >Κυπρος</a></li>
<li><a href="/index.php?id=4" title="Οικονομια" >Οικονομια</a></li>
<li><a href="/index.php?id=5" title="Διεθνεις" >Διεθνεις</a></li>
<li><a href="/index.php?id=6" title="Αθλητικα" >Αθλητικα</a></li>
<li><a href="/index.php?id=7" title="Lifestyle" >Lifestyle</a></li>
<li><a href="/index.php?id=8" title="Ενδιαφερουν" >Ενδιαφερουν</a></li>
<li><a href="/index.php?id=9" title="Αρχειο" >Αρχειο</a></li>
<li class="last"><a href="/index.php?id=10" title="Επικοινωνια" >Επικοινωνια</a></li>
</ul>		
		<div class="clear"></div>
	</div><!--menu_inner-->
</div><!--menu_outer-->	
	<div class="content_outer">
		
		<div class="floating_banner_left">
			<iframe id='a1212df5' name='a1212df5' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=90&amp;zoneid=6&amp;target=_blank&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='160' height='600'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ad13cb75&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=6&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ad13cb75' border='0' alt='' /></a></iframe>

		</div>		
		<div class="floating_banner_right">
			
		</div>
		
		<div class="content_left">
			<h2>Privacy Policy</h2>
			<div>
<h1>Privacy Policy</h1>
<p>This privacy policy covers the treatment of personally identifiable information that we G.P. CYMEDIA LTD collect when you are on our site, and when you use our services. This policy also applies to the treatment of any personally identifiable information that our business partners share with us. This policy does not apply to the practices of companies that we do not own or control, or to people that we do not employ or manage.</p>
<hr size="2" />
<p><strong>Information Collection and Use</strong></p>
<p>We may collect personally identifiable information when you visit or use our pages. We may also receive personally identifiable information from our business partners. We also automatically receive and record information on our server logs from your browser including your IP address, cookie information and the page you requested.</p>
<p>We may use this information for three general purposes:</p>
To customize the content you see,<br />To fulfill your requests for certain services and information, and<br />To contact you.
<ul></ul>
<ol></ol><hr size="2" />
<p><strong>Information Sharing and Disclosure</strong></p>
<p>We will not sell or rent your personally identifiable information to anyone. We will send personally identifiable information about you to other companies or people when:</p>
We have your consent to share the information;<br />We need to share your information to provide the product or service you have requested;<br />We need to send the information to companies who work on behalf of G.P. CYMEDIA to provide a product or service to you (unless we tell you differently, these companies do not have any right to use the personally identifiable information we provide to them beyond what is necessary to assist us);<br />We respond to subpoenas, court orders or legal process; or<br />We find that your actions on our web sites violate the above terms of service, or any of our usage guidelines for specific products or services.
<ul></ul>
<ol></ol><hr size="2" />
<div><strong>Cookies</strong></div>
<p>We may set and access cookies on your computer.</p>
</div>			
			<div class="clear"></div>
		</div><!--content_left-->
		<div class="content_right">
	<div class="content_right_mobile_size_div">
		<iframe id='afd93e27' name='afd93e27' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=120&amp;zoneid=57&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='120' height='600'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=abcbe2d0&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=57&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=abcbe2d0' border='0' alt='' /></a></iframe>

	</div>
	
	<div class="content_right_full_size_div">
			<div class="banner_sky">
				<iframe id='a0a20ef3' name='a0a20ef3' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=70&amp;zoneid=4&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ad4b31b1&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=4&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ad4b31b1' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>	
			<a class="latest_newsletter_a" href="index.php?id=22">
				<div class="latest_newsletter">
					Διαβάστε το τελευταίο Newsletter
				</div>
			</a><!--latest_newsletter-->
			<div class="newsletter_subscribe">
				<form action="http://www.xtenzio1.com/subscribe.php" method="post" target="_blank">
			   		<div class="subscribe_text">Εγγραφή Newsletter:</div>
					<input id="FormValue_EmailAddress" name="FormValue_Fields[EmailAddress]" type="text" value="e-mail"  class="input_subscribe" onclick="this.value=''">
					<input id="FormValue_ListID" name="FormValue_ListID" type="hidden" value="3">
					<input id="FormValue_Command" name="FormValue_Command" type="hidden" value="Subscriber.Add">
					<input id="FormButton_Subscribe" name="FormButton_Subscribe" type="submit" class="button_subscribe" value="Subscribe">
				</form>
			</div><!--newsletter_subscribe-->
			<div class="clear"></div>
			<div class="banner_sqr">
				<iframe id='a42b50aa' name='a42b50aa' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=95&amp;zoneid=7&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=a3db358c&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=7&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a3db358c' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>	
			<div class="namedays_outer">
				<div class="lnews_title">ΓΙΟΡΤΕΣ</div>
				Λωτ, Λότ
			</div>	
			<div class="clear"></div>
			<div class="latest_news_outer">
				<a href="index.php?id=15" class="lnews_title">ΤΕΛΕΥΤΑΙΕΣ ΕΙΔΗΣΕΙΣ</a>
				<div class="latest_news_inner">
							
			<!-- 8:10 -->
			<script src="assets/templates/site/jscrollpane/jquery.jscrollpane.min.js"></script>
			<script src="assets/templates/site/jscrollpane/jquery.mousewheel.min.js"></script>
			<link href="assets/templates/site/jscrollpane/jquery.jscrollpane.css" rel="stylesheet" />
			<script type="text/javascript">
				$(function(){
					$(".latest_news_inner").jScrollPane();
				});
			</script>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130888">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Συμβιβασμός Μέρκελ-CSU για υποδοχή 200.000 προσφύγων...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:57 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130856">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Παραπέμπεται για απιστία πρώην γενικός διευθυντής...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:53 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130854">
							<div class="ln_category">Οικονομία<span>&nbsp;-&nbsp;ΚΥΣΟΙΠ: Ενέκρινε το νομοσχέδιο για την εποπτεία...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:51 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130858">
							<div class="ln_category">Οικονομία<span>&nbsp;-&nbsp;ΚΥΣΟΙΠ: Σχέδια για τη διαμόρφωση ενιαίου μοντέλου...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:46 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130839">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Συνελήφθη 54χρονος για την πυρκαγιά στην εφορία...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:45 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130879">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Γ. Παπαηλιού : Η ΕΕΔKΑ επιτελεί σημαντικό έργο...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:45 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130853">
							<div class="ln_category">Οικονομία<span>&nbsp;-&nbsp;Fraport: Αρχίζουν τα έργα στο αεροδρόμιο της Μυκόνου</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:40 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130863">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Η Κομισιόν καλωσορίζει τη συμφωνία CDU - CSU για...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:40 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130866">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Το Κρεμλίνο εμφανίστηκε πιο ανεκτικό στις πρόσφατες...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:40 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130835">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Το eurogroup ύψωσε σημαία αποχαιρετισμού στον Σόιμπλε</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:36 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130859">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Στο έλεος των δασικών πυρκαγιών ξανά η Πορτογαλία</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:36 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130868">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Στο αρχείο τίθεται η υπόθεση του «πόθεν έσχες»...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:35 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130882">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Live: Πολιτικό θρίλερ στην Ολομέλεια της Βουλής...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:34 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130842">
							<div class="ln_category">Τοπικά<span>&nbsp;-&nbsp;Στην επόμενη συνεδρία του Υπουργικού η επιστολή...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:33 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130871">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Η Ελλάδα στη διεθνή έκθεση βιβλίου Φρανκφούρτης!</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:30 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130881">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;To in.gr παρουσιάζει το νέο ηλεκτρονικό εισιτήριο...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:30 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130840">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Έργα με στόχο τη βελτίωση της αθημερινότητας...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:28 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130848">
							<div class="ln_category">Αθλητικά<span>&nbsp;-&nbsp;Μανιάτης: Μια νίκη ακόμα</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:27 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130836">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Γερμανία: Αγωνιώδης αναζήτηση για τετράχρονη...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:27 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=10130841">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Απορρύπανση Σαρωνικού: Αποσύρονται τα πλωτά...</span></div>
							<div class="clear"></div>
							<div class="ln_time">7:22 μμ, 9 Οκτωβρίου 2017</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="clear"></div>
				</div><!--latest_news_inner-->
				<a href="index.php?id=15" class="latest_news_link">ΠΕΡΙΣΣΟΤΕΡΑ</a>
			</div><!--latest_news_outer-->
			<div class="clear"></div>			
			<div class="banner_sqr">
				<iframe id='a1a11efe' name='a1a11efe' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=100&amp;zoneid=22&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=a5ea41c3&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=22&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a5ea41c3' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>
			<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Ffacebook.com%2FCYtoday&amp;width=300&amp;colorscheme=light&amp;show_faces=true&amp; connections=15&amp;stream=false&amp;header=false&amp;height=300" scrolling="no" frameborder="0" scrolling="no" style="border: medium none; overflow: hidden; height: 300px; width: 300px;background:#fff;"></iframe>
		</div>
</div><!--content_right-->
		<div class="clear"></div>
	</div><!--content_outer-->	
	<div class="clear"></div>
	<script>
	$(document).ready(function() {
		checkwtoremove();
		$(document).on('click',"#closeme-catfish", function() {
			$("#catfish").remove();
		});
		var current = new Date();
var expiry  = new Date("March 10, 2017 12:00:00");

if(current.getTime()>expiry.getTime()){
	$('#catfish').show();
}
		$( window ).resize(function() {
			checkwtoremove();
		});
	});
	function checkwtoremove() {
		var window_width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		if(window_width < 960) {
			$(".catfish-1000").remove();
		}
		if(window_width > 468) {
			$(".catfish-468").remove();
		}
	}
</script>
		
<div id="catfish">
	<div class="catfish-1000" style="background-color: #FFF;">
		<ul>
			<li><a href="#" id="closeme-catfish">[x] Close</a></li>
		</ul>
		<iframe id='a9a79359' name='a9a79359' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=120&amp;zoneid=12&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='100%' height='100%'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ae710e40&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=12&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ae710e40' border='0' alt='' /></a></iframe>
	</div>
	<div class="catfish-468" style="background-color: #FFF;">
		<ul>
			<li><a href="#" id="closeme-catfish">[x] Close</a></li>
		</ul>
		<!--/*
  *
  * xblaster AdServer iFrame Tag
  * - Generated with Revive Adserver v3.1.0
  *
  */-->

<!--/*
  * This tag has been generated for use on a non-SSL page. If this tag
  * is to be placed on an SSL page, change the
  *   'http://www.cytoday.com.cy/ads/www/delivery/...'
  * to
  *   'https://www.cytoday.com.cy/ads/www/delivery/...'
  *
  * The backup image section of this tag has been generated for use on a
  * non-SSL page. If this tag is to be placed on an SSL page, change the
  *   'http://www.cytoday.com.cy/ads/www/delivery/...'
  * to
  *   'https://www.cytoday.com.cy/ads/www/delivery/...'
  *
  * If iFrames are not supported by the viewer's browser, then this
  * tag only shows image banners. There is no width or height in these
  * banners, so if you want these tags to allocate space for the ad
  * before it shows, you will need to add this information to the <img>
  * tag.
  */-->

<iframe id='a5b20413' name='a5b20413' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=120&amp;zoneid=59&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='320' height='100'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ad7521c2&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=59&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ad7521c2' border='0' alt='' /></a></iframe>

	</div>
</div>
<div class="footer_outer">			
		<div class="menu_outer">
			<div class="menu_inner">
				<ul><li><a href="/index.php?id=2" title="Σημαντικες Ειδησεις" >Σημαντικες Ειδησεις</a></li>
<li><a href="/index.php?id=3" title="Κυπρος" >Κυπρος</a></li>
<li><a href="/index.php?id=4" title="Οικονομια" >Οικονομια</a></li>
<li><a href="/index.php?id=5" title="Διεθνεις" >Διεθνεις</a></li>
<li><a href="/index.php?id=6" title="Αθλητικα" >Αθλητικα</a></li>
<li><a href="/index.php?id=7" title="Lifestyle" >Lifestyle</a></li>
<li><a href="/index.php?id=8" title="Ενδιαφερουν" >Ενδιαφερουν</a></li>
<li><a href="/index.php?id=9" title="Αρχειο" >Αρχειο</a></li>
<li class="last"><a href="/index.php?id=10" title="Επικοινωνια" >Επικοινωνια</a></li>
</ul>
				<div class="clear"></div>
			</div><!--menu_inner-->
		</div><!--menu_outer-->
		<div class="clear"></div>
		<div class="footer_inner">
			<div class="terms">CYtoday © All Rights Reserved. - <a href="index.php?id=12" class="footer_links">Terms of Use</a> | <a href="index.php?id=13" class="footer_links">Privacy Policy</a></div>
			<div class="visa_logos">
				<img src="assets/templates/site/images/jcc.jpg" />
				<img src="assets/templates/site/images/cc.png" />
            </div>
            <div class="developed">
                <a href="http://www.com2go.com/index.php?id=16"><img src="assets/templates/site/images/developed.png" /></a>
            </div>  
            <div class="clear"> </div>
		</div><!--footer_inner-->	
	</div><!--footer_outer-->
</body>
</html>