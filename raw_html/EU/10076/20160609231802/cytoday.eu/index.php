<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta property="fb:pages" content="314039031577" />
	<title>CYtoday | Privacy Policy</title>
	<link rel="stylesheet" href="assets/templates/site/styles.css" type="text/css" />
	<script src="assets/templates/site/includes/jquery2.1.1.min.js"></script>
	<script src="assets/templates/site/bxslider/jquery.bxslider.min.js"></script>
    <script src="assets/templates/site/jquery.cookiesdirective.js"></script>
	<link href="assets/templates/site/bxslider/jquery.bxslider.css" rel="stylesheet" />
	<script>
		$(document).ready(function(){
			$('.bxslider').bxSlider({
				controls:	false,
				auto:	true,
				pause:	10000,				
				pagerCustom: '#bx_pager'
			});
			$.cookiesDirective({
				privacyPolicyUri: 'index.php?id=13',
				duration: 10,
				limit: 1,
				fontColor: '#474747',
				backgroundColor: '#F7F7F7',
				backgroundOpacity: '95',
				linkColor: '#474747'
			});
		});
	</script>
	<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-21024072-1', 'auto');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
var rand = Math.floor((Math.random() * 10) + 1);
if ( rand % 2 == 0 ) {
	ga('create', 'UA-39021535-1', 'auto', {'name': 'nTracker'});
	ga('nTracker.send', 'pageview');
}
    ga('create', 'UA-40027938-1', 'auto', {'name': 'sTracker'});
    ga('sTracker.send', 'pageview');
</script>
</head>
<body>
	<div class="header">
	<div class="partners">
		<a href="http://www.cymedia.eu/" target="_blank" class="partner_link">CYmedia Group</a>:&nbsp;&nbsp;
		<a href="http://www.cytoday.eu/" target="_blank" class="partner_link">CYtoday</a>,&nbsp;&nbsp;&nbsp;
		<a href="http://www.sportsbreak.com.cy/" target="_blank" class="partner_link">SportsBreak</a>,&nbsp;&nbsp;&nbsp;
		<a href="http://www.novosti.com.cy/" target="_blank" class="partner_link">Novosti</a>
		<div class="clear"></div>
	</div><!--partners-->
</div><!--header-->

<div class="header_logo_outer">
	<div class="header_logo">
		<a href="http://cytoday.eu/" class="logo"><img src="assets/templates/site/images/logo.png" /></a>
		<div class="banner_horizontal">
			<iframe id='a20c6c61' name='a20c6c61' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=99&amp;zoneid=5&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='728' height='90' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ab62f48d&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=5&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ab62f48d' border='0' alt='' /></a></iframe>
		</div>
		<iframe id='af4a39be' name='af4a39be' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=1450&amp;zoneid=29&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' style="display:none;"><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=aa3656c7&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=29&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=aa3656c7' border='0' alt='' /></a></iframe>
<iframe id='aaff8e09' name='aaff8e09' src='http://www.xblasterads.com/www/delivery/afr.php?refresh=92&amp;zoneid=1803&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' style="display:none;"><a href='http://www.xblasterads.com/www/delivery/ck.php?n=af2996a4&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.xblasterads.com/www/delivery/avw.php?zoneid=1803&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=af2996a4' border='0' alt='' /></a></iframe>
		<div class="clear"></div>
	</div><!--header_logo-->
</div><!--header_logo_outer-->	
<div class="clear"></div>
<div class="menu_outer">
	<div class="menu_inner">
		<ul><li><a href="/index.php?id=2" title="Σημαντικες Ειδησεις" >Σημαντικες Ειδησεις</a></li>
<li><a href="/index.php?id=3" title="Κυπρος" >Κυπρος</a></li>
<li><a href="/index.php?id=4" title="Οικονομια" >Οικονομια</a></li>
<li><a href="/index.php?id=5" title="Διεθνεις" >Διεθνεις</a></li>
<li><a href="/index.php?id=6" title="Αθλητικα" >Αθλητικα</a></li>
<li><a href="/index.php?id=7" title="Lifestyle" >Lifestyle</a></li>
<li><a href="/index.php?id=8" title="Ενδιαφερουν" >Ενδιαφερουν</a></li>
<li><a href="/index.php?id=9" title="Αρχειο" >Αρχειο</a></li>
<li><a href="/index.php?id=10" title="Επικοινωνια" >Επικοινωνια</a></li>
<li class="last"><a href="/index.php?id=11" title="Ζωδια" >Ζωδια</a></li>
</ul>		
		<div class="clear"></div>
	</div><!--menu_inner-->
</div><!--menu_outer-->	
	<div class="content_outer">
		
		<div class="floating_banner_left">
			<iframe id='aa0ddf46' name='aa0ddf46' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=90&amp;zoneid=6&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='160' height='600' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=afe63fcc&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=6&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=afe63fcc' border='0' alt='' /></a></iframe>

		</div>		
		<div class="floating_banner_right">
			
		</div>
		
		<div class="content_left">
			<h2>Privacy Policy</h2>
			<div>
<h1>Privacy Policy</h1>
<p>This privacy policy covers the treatment of personally identifiable information that we G.P. CYMEDIA LTD collect when you are on our site, and when you use our services. This policy also applies to the treatment of any personally identifiable information that our business partners share with us. This policy does not apply to the practices of companies that we do not own or control, or to people that we do not employ or manage.</p>
<hr size="2" />
<p><strong>Information Collection and Use</strong></p>
<p>We may collect personally identifiable information when you visit or use our pages. We may also receive personally identifiable information from our business partners. We also automatically receive and record information on our server logs from your browser including your IP address, cookie information and the page you requested.</p>
<p>We may use this information for three general purposes:</p>
To customize the content you see,<br />To fulfill your requests for certain services and information, and<br />To contact you.
<ul></ul>
<ol></ol><hr size="2" />
<p><strong>Information Sharing and Disclosure</strong></p>
<p>We will not sell or rent your personally identifiable information to anyone. We will send personally identifiable information about you to other companies or people when:</p>
We have your consent to share the information;<br />We need to share your information to provide the product or service you have requested;<br />We need to send the information to companies who work on behalf of G.P. CYMEDIA to provide a product or service to you (unless we tell you differently, these companies do not have any right to use the personally identifiable information we provide to them beyond what is necessary to assist us);<br />We respond to subpoenas, court orders or legal process; or<br />We find that your actions on our web sites violate the above terms of service, or any of our usage guidelines for specific products or services.
<ul></ul>
<ol></ol><hr size="2" />
<div><strong>Cookies</strong></div>
<p>We may set and access cookies on your computer.</p>
</div>			
			<div class="clear"></div>
		</div><!--content_left-->
		<div class="content_right">
			<div class="banner_sky">
				<iframe id='a0a20ef3' name='a0a20ef3' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=90&amp;zoneid=4&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=ad4b31b1&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=4&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=ad4b31b1' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>	
			<a class="latest_newsletter_a" href="index.php?id=22">
				<div class="latest_newsletter">
					Διαβάστε το τελευταίο Newsletter
				</div>
			</a><!--latest_newsletter-->
			<div class="newsletter_subscribe">
				<form action="http://www.xtenzio1.com/subscribe.php" method="post" target="_blank">
			   		<div class="subscribe_text">Εγγραφή Newsletter:</div>
					<input id="FormValue_EmailAddress" name="FormValue_Fields[EmailAddress]" type="text" value="e-mail"  class="input_subscribe" onclick="this.value=''">
					<input id="FormValue_ListID" name="FormValue_ListID" type="hidden" value="3">
					<input id="FormValue_Command" name="FormValue_Command" type="hidden" value="Subscriber.Add">
					<input id="FormButton_Subscribe" name="FormButton_Subscribe" type="submit" class="button_subscribe" value="Subscribe">
				</form>
			</div><!--newsletter_subscribe-->
			<div class="clear"></div>
			<div class="banner_sqr">
				<iframe id='a42b50aa' name='a42b50aa' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=95&amp;zoneid=7&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=a3db358c&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=7&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a3db358c' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>	
			<div class="namedays_outer">
				<div class="lnews_title">ΓΙΟΡΤΕΣ</div>
				Δεν υπάρχει γνωστή γιορτή σήμερα.
			</div>	
			<div class="clear"></div>
			<div class="latest_news_outer">
				<a href="index.php?id=15" class="lnews_title">ΤΕΛΕΥΤΑΙΕΣ ΕΙΔΗΣΕΙΣ</a>
				<div class="latest_news_inner">
							
			<!-- 2:15 -->
			<script src="assets/templates/site/jscrollpane/jquery.jscrollpane.min.js"></script>
			<script src="assets/templates/site/jscrollpane/jquery.mousewheel.min.js"></script>
			<link href="assets/templates/site/jscrollpane/jquery.jscrollpane.css" rel="stylesheet" />
			<script type="text/javascript">
				$(function(){
					$(".latest_news_inner").jScrollPane();
				});
			</script>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574885">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Η άγνωστη σχέση της Κατερίνας Ζαρίφη με τον Γιώργο...</span></div>
							<div class="clear"></div>
							<div class="ln_time">1:44 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574882">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp; Ενημέρωση για το Πρόγραμμα χρηματοδότησης για...</span></div>
							<div class="clear"></div>
							<div class="ln_time">1:32 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574881">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Όλγα Λαφαζάνη: «Δεν λέω μεγάλα λόγια γιατί ποτέ...</span></div>
							<div class="clear"></div>
							<div class="ln_time">1:28 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574883">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Κώστας Μαρτάκης: Τι λέει για την τελευταία νύχτα...</span></div>
							<div class="clear"></div>
							<div class="ln_time">1:25 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574884">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Διασώθηκαν 3.000 μετανάστες στη Μεσόγειο</span></div>
							<div class="clear"></div>
							<div class="ln_time">1:14 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574880">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Πήρε φωτιά το σπίτι Ελληνίδας τραγουδίστριας:...</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:59 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574879">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Justin Bieber: Στη Σαντορίνη το βιντεοκλίπ του νέου...</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:44 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574842">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Θάνος Τζάνης: «Προτιμώ μια άσχημη αλήθεια από...</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:43 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574856">
							<div class="ln_category">Οικονομία<span>&nbsp;-&nbsp;10,000 άτομα στα γενέθλια της ΚΕΑΝΙΤΑ!</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:40 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574854">
							<div class="ln_category">Οικονομία<span>&nbsp;-&nbsp;Το «Deal»έρχεται στον ALPHA ΚΥΠΡΟΥ!</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:38 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574861">
							<div class="ln_category">Οικονομία<span>&nbsp;-&nbsp;Ο Armin Van Buuren θα απογειώσει ξανά το κοινό της Κύπρου</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:38 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574864">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Κρήτη: Με σακάτεψε στο ξύλο ο πατέρας της- Δεν...</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:32 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574877">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Γαλλία: Συνάντηση κυβέρνησης με το συνδικάτο...</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:25 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574844">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Κρατάει χρόνια αυτή η κολώνια</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:15 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574840">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Έδειραν ΠΑΣΙΓΝΩΣΤΟ τραγουδιστή στο γήπεδο: Καρέ-καρέ...</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:06 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574878">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Richard Gere: Τρελά ερωτευμένος με τη νεαρή σύντροφό...</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:05 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574845">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Το λευκό που ξεχωρίζει... στη Μύκονο</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:03 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574843">
							<div class="ln_category">Life Style<span>&nbsp;-&nbsp;Ημερήσιες προβλέψεις για όλα τα ζώδια για την...</span></div>
							<div class="clear"></div>
							<div class="ln_time">12:00 πμ, 10 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574874">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Πέραν των 3.000 μεταναστών διασώθηκαν στη Μεσόγειο...</span></div>
							<div class="clear"></div>
							<div class="ln_time">11:51 μμ, 9 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="latest_news">
						<a href="index.php?id=17&nid=9574855">
							<div class="ln_category">Διεθνή<span>&nbsp;-&nbsp;Τι δείχνουν τα πρώτα στοιχεία για την Porsche του...</span></div>
							<div class="clear"></div>
							<div class="ln_time">11:51 μμ, 9 Ιουνίου 2016</div>						
							<div class="clear"></div>
						</a>		
					</div>
					<div class="clear"></div>
				</div><!--latest_news_inner-->
				<a href="index.php?id=15" class="latest_news_link">ΠΕΡΙΣΣΟΤΕΡΑ</a>
			</div><!--latest_news_outer-->
			<div class="clear"></div>			
			<div class="banner_sqr">
				<iframe id='a1a11efe' name='a1a11efe' src='http://www.cytoday.com.cy/ads/www/delivery/afr.php?refresh=100&amp;zoneid=22&amp;cb=INSERT_RANDOM_NUMBER_HERE' frameborder='0' scrolling='no' width='300' height='250' allowtransparency='true'><a href='http://www.cytoday.com.cy/ads/www/delivery/ck.php?n=a5ea41c3&amp;cb=INSERT_RANDOM_NUMBER_HERE' target='_blank'><img src='http://www.cytoday.com.cy/ads/www/delivery/avw.php?zoneid=22&amp;cb=INSERT_RANDOM_NUMBER_HERE&amp;n=a5ea41c3' border='0' alt='' /></a></iframe>
			</div>
			<div class="clear"></div>
			<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Ffacebook.com%2FCYtoday&amp;width=300&amp;colorscheme=light&amp;show_faces=true&amp; connections=15&amp;stream=false&amp;header=false&amp;height=300" scrolling="no" frameborder="0" scrolling="no" style="border: medium none; overflow: hidden; height: 300px; width: 300px;background:#fff;"></iframe>
		</div><!--content_right-->
		<div class="clear"></div>
	</div><!--content_outer-->	
	<div class="clear"></div>
	<div class="footer_outer">			
		<div class="menu_outer">
			<div class="menu_inner">
				<ul><li><a href="/index.php?id=2" title="Σημαντικες Ειδησεις" >Σημαντικες Ειδησεις</a></li>
<li><a href="/index.php?id=3" title="Κυπρος" >Κυπρος</a></li>
<li><a href="/index.php?id=4" title="Οικονομια" >Οικονομια</a></li>
<li><a href="/index.php?id=5" title="Διεθνεις" >Διεθνεις</a></li>
<li><a href="/index.php?id=6" title="Αθλητικα" >Αθλητικα</a></li>
<li><a href="/index.php?id=7" title="Lifestyle" >Lifestyle</a></li>
<li><a href="/index.php?id=8" title="Ενδιαφερουν" >Ενδιαφερουν</a></li>
<li><a href="/index.php?id=9" title="Αρχειο" >Αρχειο</a></li>
<li><a href="/index.php?id=10" title="Επικοινωνια" >Επικοινωνια</a></li>
<li class="last"><a href="/index.php?id=11" title="Ζωδια" >Ζωδια</a></li>
</ul>
				<div class="clear"></div>
			</div><!--menu_inner-->
		</div><!--menu_outer-->
		<div class="clear"></div>
		<div class="footer_inner">
			<div class="terms">CYtoday © All Rights Reserved. - <a href="index.php?id=12" class="footer_links">Terms of Use</a> | <a href="index.php?id=13" class="footer_links">Privacy Policy</a></div>
			<div class="visa_logos">
				<img src="assets/templates/site/images/jcc.jpg" />
				<img src="assets/templates/site/images/cc.png" />
            </div>
            <div class="developed">
                <a href="http://www.com2go.com/index.php?id=16"><img src="assets/templates/site/images/developed.png" /></a>
            </div>                
            <div class="clear"> </div>
		</div><!--footer_inner-->	
	</div><!--footer_outer-->
</body>
</html>