<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
         <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Privacy Notice</title><meta name="KEYWORDS" content="Privacy Notice"><meta name="DESCRIPTION" content="Privacy Notice"><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><meta property="fb:pages" content="54445956713"><meta name="viewport" content="width=device-width, initial-scale=1"> <link href="https://www.lds.org/services/platform/v3/assets/main.d2070829.css" rel="stylesheet" type="text/css"> <meta property="fb:pages" content="18523396549"> <meta property="fb:pages" content="120694114630885"> <script src="https://abn.ldschurch.org/static/js/optimize.js"> </script>
        <!-- <PageMap>
        <DataObject type='document'><Attribute name='title'>Privacy Notice (Updated 2018-09-01)</Attribute><Attribute name='description'>Privacy Notice</Attribute><Attribute name='subject'>Privacy Notice</Attribute><Attribute name='collection'></Attribute></DataObject>
    </PageMap> -->
        <meta property="og:title" content="Privacy Notice"><meta property="og:url" content="https://www.lds.org/legal/privacy-notice?lang=eng&amp;country=go"><meta property="og:description" content="Privacy Notice"><meta property="og:image" content="http://www.lds.org/bc/content/ldsorg/content/images/facebook/lds_Fb_defaultThm.jpg"><meta property="fb:admins" content="683545112,100001195566656"><meta property="og:type" content="non_profit">
        
        <link rel="shortcut icon" href="//edge.ldscdn.org/cdn2/common/images/logos/favicon-lds-1.ico"><link rel="stylesheet" type="text/css" media="all" href="//edge.ldscdn.org/cdn2/csp/ldsorg/css/common/lds-old.css" class="pf-fix-me"><script type="text/javascript" src="//edge.ldscdn.org/cdn2/common/scripts/jquery/1.7.1/jquery.min.js"> </script><script src="//edge.ldscdn.org/cdn2/csp/ldsorg/script/common/lds.js"> </script>
        <link rel="stylesheet" type="text/css" media="all" href="//edge.ldscdn.org/cdn2/csp/ldsorg/css/common/content.css">
        <link rel="stylesheet" type="text/css" media="all" href="//edge.ldscdn.org/cdn2/csp/ldsorg/css/common/article.css">
        
    </head>

    <body class="tmpl-basic-article-wide pf-responsive  generic-theme   lang-eng">
        <div id="platform">
            <script src="//assets.adobedtm.com/05c94072b1046aef0dc9b195809b4d2429a0d30a/satelliteLib-a42cf03bb084dfc1c72d9fa7178dccbd338041d2.js"> </script><header id="PFmainHeader" class="LDSORG"> <div id="PFheadSpace"> <a href="/" class="PFhomeLink" id="PFmainLogo"> <picture class="LDSMainlogo"> </picture> </a> <div id="PFprefBox"></div> </div> <div id="PFnavContainer" data-breadcrumbs="false"> <div id="PFmobileNavControls"> <span id="PFmobileNavButton"></span> <span id="PFmobileBreadcrumb"></span> </div> <span id="PFsearchIcon"> <svg style="width:24px;height:24px" xmlns="http://www.w3.org/2000/svg"><path d="M21.07 19.65l-5-5a7.5 7.5 0 1 0-1.41 1.41l5 5a1 1 0 0 0 1.41-1.41zm-6.72-5.3a6 6 0 1 1 0-8.49 6 6 0 0 1 0 8.49z"></path></svg> </span> <nav id="PFmainNav"> <ul id="PFmainNavLinks"></ul> </nav> <div id="PFoverlay"></div> </div> <div id="pf-betaToggleBar"> <div class="pf-toggleWrapper"> <span>New Experience</span> <label class="pf-toggle"> <input type="checkbox" id="GLOBetaToggle"> <span class="pf-slider round"></span> </label> </div> </div> <div id="PFsearchContainer"> <form id="PFsearchForm"> <input type="text" autocomplete="off" id="PFsearchBox" form="PFsearchForm" inputmode="text" list="PFsearchSuggest"> <div id="PFsubmitSearch"> <span class="PFsearchIcon"> <svg style="width:24px;height:24px" xmlns="http://www.w3.org/2000/svg"><path fill="#fff" d="M21.07 19.65l-5-5a7.5 7.5 0 1 0-1.41 1.41l5 5a1 1 0 0 0 1.41-1.41zm-6.72-5.3a6 6 0 1 1 0-8.49 6 6 0 0 1 0 8.49z"></path></svg> </span> <span id="PFsubmitSearchText"></span> </div> <select id="PFsearchSuggest"></select> </form> </div> </header><script type="text/javascript">var cdnPath = "//edge.ldscdn.org/cdn2";</script>
            <div id="platform-canvas">
                <div id="platform-canvas-content" role="main" class="lang-eng">
                    <div id="content" class="two-col"><div id="bottom-gradient">
                        
                        <div id="details">
                            <ul class="filed-under">
            <li>
                        <a href="javascript:void(0);" class="home" style="cursor:default;">Home</a>
                    </li><li>
                        <a href="https://www.lds.org/?lang=eng&amp;country=go">LDS.org</a>
                     </li><li>
                        <span class="active">Privacy Notice</span>
                     </li>
        </ul>
                            
                            <h1>Privacy Notice (Updated 2018-09-01)</h1>

                            <hr>
                        </div>
                        <div id="primary">
                            
                            
<p>In this Notice, we, The Church of Jesus Christ of Latter-day Saints and its entities, provide you with information about how we process your personal data. When referring to "us" or "we" or "our" we mean the Church and Church entities.</p>
<p><strong>1. Who controls your personal data?</strong></p>
<p>The Church of Jesus Christ of Latter-day Saints acts through its representatives and Church entities when we process your personal data. When you share personal data with us, you are sharing it with the Church through Church entities.</p>
<p><em>a. The Church</em>: The Church of Jesus Christ of Latter-day Saints (the "<strong>Church</strong>") is a community of people who believe the same religious doctrine, practice the same religious rites and ordinances, and are governed by ecclesiastical principles. Local units of the Church, such as wards, branches, stakes, districts, missions, and areas, are merely subdivisions of this community of believers and are not separate legal entities; limited exceptions apply.</p>
<p><em>b. Church entities</em>: To satisfy temporal affairs needs and fulfill other purposes of the Church, a legal structure composed of various distinct legal entities assists the Church throughout the world. These distinct legal entities (herein "<strong>Church entities</strong>") are legally separate from the Church. The Corporation of the Presiding Bishop of The Church of Jesus Christ of Latter-day Saints ("<strong>CPB</strong>"), together with one or more other Church entities, processes your personal data as responsible joint data controllers. Some Church entities may receive personal data as data processors to provide services to those Church entities that act as data controllers. At your request, we will provide you with information as to which Church entities that process your personal data.</p>
<p><strong>2.  What personal data do we collect?</strong></p>
<p>We collect personal data that (a) you actively submit to us, (b) we record, and (c) we obtain from third parties. We may process your personal data with or without automatic means, including collection, recording, organization, structuring, storage, adaptation or alteration, retrieval, consultation, use, disclosure by transmission, dissemination or otherwise making available, alignment or combination, restriction, erasure, or destruction of your personal data.</p>
<p>a.  <em>Actively submitted data</em>. You submit personal data to us when you join the Church, seek Church ordinances, request Church materials, request access to Church tools or services, or engage in other interactions or communications with the Church. When you interact with the Church, we generally process name, birth date, birthplace, telephone number, email address, physical address, photo, gender, donation/payment information, and so on. You may provide us with additional information to participate at your own initiative in surveys, contests, or other activities or events. Participation in surveys, contests, and similar activities is optional. If you do not wish to participate in, or provide personal data in connection with, such activities, this will not affect your membership status or ability to use available Church tools or services. In each such case you will know what personal data you provide to us because you actively and voluntarily submit the data.</p>
<p>b. <em>Data we obtain from third parties and passively submitted data</em>. When local law permits, you may submit personal data, including contact information about someone other than yourself (in other words, a third party) to allow us to make contact with that person, make a delivery, or otherwise meet your request. When providing personal data about anyone other than yourself, you must first obtain the other person’s informed consent if his or her consent is legally required.</p>
<p>If you or someone else has provided us with your personal contact information and you would like to request that we do not contact you further, please follow the unsubscribe or opt-out procedures provided on the specific site, newsletter, email notification, or please contact us at <a href="mailto:DataPrivacyOfficer@ldschurch.org">DataPrivacyOfficer@ldschurch.org</a>.</p>
<p>We may process and make publicly available personal data obtained from published sources. We may process and publish living information in compliance with applicable local laws.</p>
<p>Your location-based information may be collected by some mobile applications for the purpose of helping you find the nearest temple or meetinghouse location or for a similar reason. You can edit the settings on your device to opt out of location-based services.</p>
<p>When you visit any of our resources, our servers (using log files or filtering systems) may collect information that your web browser sends any time you visit a website. This information may include but is not limited to your Internet Protocol address (IP address), browser type, operating system, language preference, any referring web page you were visiting before you came to our site, the date and time of each visitor request, information you search for on our resources, and other information collected by cookies or similar technologies. Please read our Cookie Policy posted on each of our mobile applications and websites to learn more about cookies and similar technologies and how we use them.</p>
<p><strong>3. For what purposes do we process personal data?</strong></p>
<p>We process personal data for ecclesiastical, genealogy, humanitarian, social welfare, missionary, teaching, and other operational and administrative purposes.</p>
<p>We use personal data to provide ecclesiastical and other related services to fulfill the mission of the Church. We may use personal data to (a) contact you or others, (b) create and maintain membership records, (c) fulfill requests you make, (d) seek your voluntary feedback, (e) customize features or content on our tools or services, (f) evaluate eligibility to participate in temple and other ordinances, missionary service, volunteer or leadership positions, or (g) administer Church religious education, welfare, or other Church programs. In this context, the legal basis for our processing of your personal data is either the necessity to perform contractual and other obligations that we have towards you or carrying out of our legitimate activities as a church.</p>
<p>We may also use your data to comply with applicable laws and exercise legal rights as the basis for our data processing.</p>
<p>We may also use your personal data for internal purposes, including auditing, data analysis, system troubleshooting, and research. In these cases, we base our processing on legitimate interests in performing the activities of the Church.</p>
<p><strong>4. With whom do we share personal data?</strong></p>
<p>We share your personal data with other parties in the following circumstances:</p>
<p><em>a. Third-party providers.</em> We may provide personal data to third parties for their processing in performing functions on our behalf as data processors (for example, payment processing, maintenance, security, data analysis, hosting, measurement services, data-driven social media messaging, surveys, and so on). In such instances, in accordance with this Notice and applicable laws, the providers will be contractually required to protect personal data from additional processing (including for marketing purposes) and from transfer.</p>
<p><em>b. Church entities.</em> We may transfer personal data to any Church entity to accomplish Church purposes.</p>
<p>If you are a member of the Church, your general membership information and any optional information you may choose to provide (for example, email address and photo) may be shared with Church members in your ward or branch and stake or district as necessary for the Church purposes listed above. Some of your information may also be viewable on a limited and restricted basis on our internet resources, including LDS.org. You may opt out of sharing information or limit the optional information you share by modifying your profile preferences on individual resources.</p>
<p><em>c. Legal requirements.</em> We may access and disclose your personal data, posts, journal entries, online chats, personal notes, content, or other submissions to any resource if we have a good-faith belief that doing so is required by a subpoena, by a judicial or administrative order, or is otherwise required by law. Additionally, we may disclose your personal information and other information: as required by law or to exercise or defend legal rights; to take precautions against liability; to protect the rights, property, or safety of the resource of any individual or of the general public; to maintain and protect the security and integrity of our services or infrastructure; to protect ourselves and our services from fraudulent, abusive, or unlawful uses; to investigate and defend ourselves against third-party claims or allegations; or to assist government law enforcement agencies.</p>
<p><strong>5. Where do we store personal data?</strong></p>
<p>We may store your personal data in data centers in the United States, in cloud storage solutions, or on the premises of Church entities. To ensure the adequacy of protection of data that we transfer to other jurisdictions, we have concluded data transfer and processing agreements between the respective Church entities and their service providers, which agreements include Standard Contractual Clauses approved by the European Commission in compliance with EU law (which you may be entitled to review if you contact us as suggested at the end of this Notice).</p>
<p><strong>6. How do we secure personal data?</strong></p>
<p>We use technical and organizational measures to protect the personal data we receive against loss, misuse, and unauthorized alteration and to protect its confidentiality. We regularly review our security procedures and consider appropriate new security technology and methods. We also use current encryption technology to encrypt the transmission of data on our log-in pages. However, because we cannot guarantee the complete security of these encryption technologies, please use caution when submitting personal data online.</p>
<p><strong>7. How long do we retain personal data?</strong></p>
<p>We retain collected personal data, including information collected via mobile applications and other submissions, for a reasonable period of time to fulfill the processing purposes mentioned above. We then archive it for time periods required or necessitated by law or legal considerations. When archival is no longer required, we delete personal data from our records, with the exception of limited historical profile information, general genealogy records, and personal information retained as part of a permanent genealogical, membership, or Church historical record.</p>
<p><strong>8. How can you access and correct your personal data?</strong></p>
<p>We endeavor to maintain the accuracy of personal data and rely on you to ensure your personal data is complete and accurate. You may request access to your personal data and verify, correct, or rectify (including update) it, and block your personal data through your website-specific registration, through your profile, or through your LDS Account, as applicable.</p>
<p>If you are a Church member, some of your personal data may be updated only by making changes to your Church membership record. Such changes must be made by contacting the clerk in your Church unit and requesting the change.</p>
<p>You may contact us to exercise additional statutory rights such as data portability, objection in accordance with applicable laws, restriction of processing, and erasure of your personal data. You also have the right to lodge a complaint with a supervisory authority.</p>
<p>If you experience problems with modifying or updating your personal data, you can contact us as suggested at the end of this Notice.</p>
<p><strong>9. Effective date and amendments.</strong></p>
<p>This Notice is effective September 1, 2018 and may be amended from time to time.</p>
<p><strong>10. Contact us.</strong></p>
<p>We have a global chief data protection officer who can answer questions about data privacy or security issues. Inquiries concerning this Notice or the security of personal data that we process may be sent through email, fax, or mail:</p>
<p style="margin-left:.5in;">Email:             <a href="mailto:DataPrivacyOfficer@ldschurch.org">DataPrivacyOfficer@ldschurch.org</a><br>
Fax:                 +1-801-240-1187<br>
Address:         Data Privacy Office<br>
                        50 E. North Temple Street<br>
                        Salt Lake City, UT 84150-0013<br>
                        USA</p>
<p> </p>
<p> </p>

                        </div>
                        <div id="secondary" role="complementary"><div id="top-shadow"><div id="bottom-shadow">
                            <div>
                                <ul class="tools">
                                    <li>
                                        <a class="print" href="#print" title="Print">
                                            Print
                                        </a>
                                    </li>
                                    <li>
                                        <a class="share show rfalse" href="#" onclick="return false">Share<span class="tool-arrow"> </span></a><ul class="share-list">
            <li><a class="shareemail" href="https://www.lds.org/secure/email?lang=eng&amp;cid=email-shared&amp;country=go&amp;link=/legal/privacy-notice&amp;title=Privacy+Notice" title="E-mail"> E-mail </a></li>
            <li><a class="twitter" href="http://twitter.com?status=https%3A%2F%2Fwww.lds.org%2Flegal%2Fprivacy-notice%3Flang%3Deng%26country%3Dgo%26cid%3Dtwitter-shared" target="_blank">Twitter</a></li>
            <li><a class="facebook" href="http://www.facebook.com/sharer.php?u=https%3a//www.lds.org/legal/privacy-notice%3flang%3deng%26country%3dgo%26cid%3dfacebook-shared&amp;t=Privacy+Notice" target="_blank">Facebook</a></li>
            
        </ul>
                                    </li>
                                </ul>
                            </div>
                            
                        </div></div></div><!-- end #secondary -->
                        <div id="beta-feedback">
            <span> </span>
            <a href="https://www.lds.org/tools/feedback?lang=eng&amp;country=go&amp;note=Privacy Notice&amp;id=11340">Do you have feedback?<img src="//edge.ldscdn.org/cdn2/csp/ldsorg/images/common/arrow-open-4x7-fff.png" alt=""></a>
        </div>
                    </div></div><!-- end #content -->
                </div>

            </div><!-- end #platform-canvas -->
            
            
            <footer id="PFmainFooter" class="LDSORGFooter"> <div id="PFlinkBlock"> <div id="PFstandardLinks"></div> <div id="PFsocialLinks"></div> </div> <div id="PFfooterLogo"> <a class="PFhomeLink" href="/"> <picture class="LDSMainlogo"> </picture> </a> </div> <div id="PFfooterLegal"></div> <script src="https://www.lds.org/services/platform/v3/assets/app.e31bb0bc.js" type="application/javascript"> </script>  <script>"undefined"==typeof utag_data&&(utag_data={}),function(e,t,a,n){e="https://edge.ldscdn.org/cdn2/csp/ldsorg/scripts/tracking.js",a="script",(n=(t=document).createElement(a)).src=e,n.type="text/java"+a,n.async=!0,(e=t.getElementsByTagName(a)[0]).parentNode.insertBefore(n,e)}();</script> <link href="https://www.lds.org/services/platform/v3/assets/_footer.125ddf5a.css" rel="stylesheet" type="text/css"> <script src="https://edge.ldscdn.org/cdn2/csp/ldsorg/script/common/lds.js"> </script> <div data-text="health-check"> </div> </footer><script type="text/javascript">
        var channel = "lds.org:legal";
        var pageName = "lds.org:legal:privacy notice";
        var locale = "eng-go";
    </script><!-- SiteCatalyst code --><script type="text/javascript"><!--
        /* Give each page an identifying name and channel on the next lines. */
            var LDS = LDS || { };
            LDS.track = LDS.track || { };
            LDS.track.pageName = pageName;
            LDS.track.channel = channel;
            LDS.track.prop35 = locale;
            if (typeof(searchType) !== "undefined" && typeof(searchResults) !== "undefined") {
                LDS.track.eVar53=searchType;
                LDS.track.eVar56=searchResults;
                LDS.track.events="event36";
            }
            try {
                LDS.track.prop3 = searchText;
            } catch (e) { }
            try { _satellite.pageBottom(); } catch (e) { };//-->
        </script>
        </div><!-- end #platform -->

        <script type="text/javascript" src="//edge.ldscdn.org/cdn2/csp/ldsorg/script/common/content.js"> </script>
    </body>
</html>