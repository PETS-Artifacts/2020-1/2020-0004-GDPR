<!DOCTYPE html>
<html lang="en" itemscope>
  <head>
    <title>Parsec | Privacy Policy</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="View our privacy policy to understand how we collect, use and handle your information when you use our websites, software and services (&#34;Services&#34;).">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@parsecteam">
    <meta name="twitter:title" content="Parsec | Privacy Policy">
    <meta name="twitter:description" content="View our privacy policy to understand how we collect, use and handle your information when you use our websites, software and services (&#34;Services&#34;).">
    <meta name="twitter:creator" content="">
    <meta name="twitter:image:src" content="https://www.parsec.tv/img/parsec-social.jpg">
    <meta name="twitter:player" content="">
    <meta property="og:url" content="https://parsec.tv">
    <meta property="og:title" content="Parsec | Privacy Policy">
    <meta property="og:type" content="website">
    <meta property="og:description" content="View our privacy policy to understand how we collect, use and handle your information when you use our websites, software and services (&#34;Services&#34;).">
    <meta property="og:site_name" content="Parsec | Privacy Policy">
    <meta property="og:image" content="https://www.parsec.tv/img/parsec-social.jpg">
    <meta property="fb:admins" content="248520192163731">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="">
    <meta property="og:locale" content="">
    <meta property="og:audio" content="">
    <meta property="og:video" content="">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="stylesheet" href="/css/fonts.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.6.0/clipboard.min.js"></script>
    <script>window.segment_write_key = "nX9N53x3MzlGnwqBSCmeYT0ErGWqkdZj";
    </script>
    <script type="text/javascript" src="/js/segment.js"></script>
    <script type="text/javascript" src="/js/remodal.js"></script>
    <script type="text/javascript" src="/js/parsec.js"></script>
    <script type="text/javascript" src="//cdn.optimizely.com/js/8556070732.js"></script>
  </head>
  <body class="marketing">
    <noscript>
      <iframe src="//www.googletagmanager.com/ns.html?id=GTM-PTKRK7" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PTKRK7');
    </script>

<div class="wrapper-main"><section class="wrapper-header header--fixed">
  <div class="wrapper-inner">
    <header class="header--dark"><a href="/" class="logo"><img src="/img/logo.png" alt="Parsec Logo" class="logo__img marketing"/></a><ul class="nav">
  <li class="nav__item hide--mobile"><a href="/technology" class="nav__item__link">Our Technology</a>
  </li>
  <li class="nav__item hide--mobile"><a href="/downloads" class="nav__item__link">Download Now</a>
  </li>
  <li class="nav__item"><a href="https://blog.parsec.tv" class="nav__item__link">Blog</a>
  </li>
  <li class="nav__item"><a href="/login" class="nav__item__link">Log In</a>
  </li>
  <li class="nav__item nav__item__btn"><a href="/signup" class="btn--brand-2">Start Playing</a>
  </li>
</ul>
    </header>
  </div>
</section>
<section class="section--static section--border margin-top--xlarge">
  <div class="wrapper-inner wrapper-inner--small">
    <h1 class="headline--border">Privacy Policy</h1>
    <div class="margin-bottom--large">
      <p>Thanks for using Parsec! Here we describe how we collect, use and handle your information when you use our websites, software and services ("Services").</p>
    </div>
    <div class="margin-bottom--large">
      <p><strong>WHAT &amp; WHY</strong>
      </p>
      <p>We collect and use the following information to provide, improve and protect our Services:</p>
      <p><strong>Account.</strong> We collect, and associate with your account, information like your name, email address, phone number, payment info, and physical address.
      </p>
      <p><strong>Services.</strong> When you use our Services, we transmit video from one computer and your interactions on another computer across the internet. You will be transmitting private information when using our services.	We may track which software you use when using our Services.	If you give us access to your contacts, we'll store those contacts on our servers for you to use. This will make it easy for you to do things like share your computer and invite others to use the Services.
      </p>
      <p><strong>Usage.</strong> We collect information from and about the devices you use to access the Services. This includes things like IP addresses, the type of browser and device you use, the web page you visited before coming to our sites, and identifiers associated with your devices. Your devices (depending on their settings) may also transmit location information to the Services.
      </p>
      <p><strong>Cookies and other technologies.</strong> We use technologies like cookies and pixel tags to provide, improve, protect and promote our Services. For example, cookies help us with things like remembering your username for your next visit, understanding how you are interacting with our Services, and improving them based on that information. You can set your browser to not accept cookies, but this may limit your ability to use the Services.
      </p>
    </div>
    <div class="margin-bottom--large">
      <p><strong>HOW WE USE INFORMATION COLLECTED</strong>
      </p>
      <p>Parsec uses the information we collect for our own general business purposes, which may include:</p>
      <ul>
        <li>To help us analyze, research, report on and improve upon the Services we provide;</li>
        <li>To fulfill your requests and/or orders for certain products, services and support;</li>
        <li>To market our Services, to you and keep you up to date on our latest product updates, announcements, and special offers. You may opt out of receiving these communications at any time.</li>
      </ul>
    </div>
    <div class="margin-bottom--large">
      <p><strong>WITH WHOM</strong>
      </p>
      <p>We may share information as discussed below, but we won't sell it to advertisers or other third-parties.</p>
      <p><strong>Others working for Parsec.</strong> Parsec uses certain trusted third parties to help us provide, improve, protect, and promote our Services. These third parties will access your information only to perform tasks on our behalf and in compliance with this Privacy Policy.
      </p>
      <p><strong>Other users.</strong> Our Services display information like your name and email address to other users in places like your user profile and sharing notifications. Certain features let you make additional information available to other users.
      </p>
      <p><strong>Law &amp; Order.</strong> We may disclose your information to third parties if we determine that such disclosure is reasonably necessary to (a) comply with the law; (b) protect any person from death or serious bodily injury; (c) prevent fraud or abuse of Parsec or our users; or (d) protect Parsec’s property rights.
      </p>
    </div>
    <div class="margin-bottom--large">
      <p><strong>HOW</strong>
      </p>
      <p><strong>Retention.</strong> We'll retain information you store on our Services for as long as we need it to provide you the Services. If you delete your account, we'll also delete this information. But please note: (1) there might be some latency in deleting this information from our servers and back-up storage; and (2) we may retain this information if necessary to comply with our legal obligations, resolve disputes, or enforce our agreements.
      </p>
    </div>
    <div class="margin-bottom--large">
      <p><strong>WHERE</strong>
      </p>
      <p><strong>Around the world.</strong> To provide you with the Services, we may store, process and transmit information in the United States and locations around the world - including those outside your country. Information may also be stored locally on the devices you use to access the Services.
      </p>
    </div>
    <div class="margin-bottom--large">
      <p><strong>CHANGES</strong>
      </p>
      <p>If we are involved in a reorganization, merger, acquisition or sale of our assets, your information may be transferred as part of that deal. We will notify you (for example, via a message to the email address associated with your account) of any such deal and outline your choices in that event.</p>
      <p>We may revise this Privacy Policy from time to time, and will post the most current version on our website. If a revision meaningfully reduces your rights, we will notify you.</p>
    </div>
    <div class="margin-bottom--large">
      <p><strong>CONTACT</strong>
      </p>
      <p>Have questions or concerns about Parsec, our Services and privacy? Contact us at <a href="mailto:legal@parsec.tv" target="_blank">legal@parsec.tv</a>
      </p>
    </div>
    <div class="margin-bottom--large">
      <p>Last Updated: May 24, 2016</p>
    </div>
  </div>
</section>
</div><section class="section__footer">
  <div class="wrapper-inner">
    <footer><a href="/"><img src="/img/logo-mark.png" alt="Parsec Footer Logo" class="footer__logo"/></a>
      <ul class="footer">
        <li class="footer__item"><a href="/downloads">Downloads</a>
        </li>
        <li class="footer__item"><a href="/faq">FAQ</a>
        </li>
        <li class="footer__item"><a href="/terms">Terms</a>
        </li>
        <li class="footer__item"><a href="/privacy-policy">Privacy Policy</a>
        </li>
        <li class="footer__item"><a href="https://blog.parsec.tv" target="_blank">Blog</a>
        </li>
        <li class="footer__item"><a href="/developer-projects" target="_blank">Projects</a>
        </li>
        <li class="footer__item"><a href="/about">About</a>
        </li>
        <li class="footer__item"><a href="/about#jobs">Jobs</a>
        </li>
        <li class="footer__item"><a href="mailto:founders@parsec.tv" target="_blank">Contact Us</a>
        </li>
        <li class="footer__item"><a href="https://www.facebook.com/parsec.tv/" target="_blank"><i class="icon--right ion-social-facebook"></i></a><a href="https://twitter.com/ParsecTeam" target="_blank"><i class="icon--right ion-social-twitter"></i></a>
        </li>
      </ul><a href="https://discord.gg/cQjEGFy" target="_blank" class="footer__text"><img src="/img/discord-logo.png" alt="Discord Logo"/>Join us on Discord</a>
      <div class="footer">
        <p class="copyright">Copyright Parsec Cloud, Inc. 2016-2017
        </p>
      </div>
    </footer>
  </div>
</section>
    <script>analytics.ready(function() {
	analytics.page();
});
    </script>


    <script>var DEBUG = false;
    </script>

    <script>init_floating_header();</script>
  </body>
</html>