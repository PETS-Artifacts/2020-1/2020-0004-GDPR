﻿
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->

	<link rel="stylesheet" type="text/css" href="/inc/minify_css.php?50">
	
	
	<base href="https://www.affiliaxe.com/">	
    
	<script type="text/javascript">
		var base_url = 'https://www.affiliaxe.com/';
	</script>
		
		
	<title>Affiliate Marketing, Best Affiliate Programs &amp; Top Affiliate Network | affiliaXe</title>
    <meta name="description" content="affiliaXe - The leading global affiliate network! Affiliate marketing for Desktop &amp; Mobile. Get access to the world&#8217;s best affiliate programs, CPA offers &amp; performance marketing campaigns.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="affiliate marketing, affiliate program, affiliate network, performance marketing, affiliate, CPA marketing, performance network, best affiliate programs, top affiliate programs, best affiliate network, top affiliate network, global affiliate network, desktop affiliate, mobile affiliate, affiliate offers, affiliate campaigns"/>
	<script>
		var request_uri = '/privacypolicy.php';
			</script>
	<style>
		/* cyrillic-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 300;
		  src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTa-j2U0lmluP9RWlSytm3ho.woff2) format('woff2');
		  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
		}
		/* cyrillic */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 300;
		  src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTZX5f-9o1vgP2EXwfjgl7AY.woff2) format('woff2');
		  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		}
		/* greek-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 300;
		  src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTRWV49_lSm1NYrwo-zkhivY.woff2) format('woff2');
		  unicode-range: U+1F00-1FFF;
		}
		/* greek */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 300;
		  src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTaaRobkAwv3vxw3jMhVENGA.woff2) format('woff2');
		  unicode-range: U+0370-03FF;
		}
		/* vietnamese */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 300;
		  src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTf8zf_FOSsgRmwsS7Aa9k2w.woff2) format('woff2');
		  unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
		}
		/* latin-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 300;
		  src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTT0LW-43aMEzIO6XUTLjad8.woff2) format('woff2');
		  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
		}
		/* latin */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 300;
		  src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTegdm0LZdjqr5-oayXSOefg.woff2) format('woff2');
		  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
		}
		/* cyrillic-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/K88pR3goAWT7BTt32Z01mxJtnKITppOI_IvcXXDNrsc.woff2) format('woff2');
		  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
		}
		/* cyrillic */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/RjgO7rYTmqiVp7vzi-Q5URJtnKITppOI_IvcXXDNrsc.woff2) format('woff2');
		  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		}
		/* greek-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/LWCjsQkB6EMdfHrEVqA1KRJtnKITppOI_IvcXXDNrsc.woff2) format('woff2');
		  unicode-range: U+1F00-1FFF;
		}
		/* greek */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/xozscpT2726on7jbcb_pAhJtnKITppOI_IvcXXDNrsc.woff2) format('woff2');
		  unicode-range: U+0370-03FF;
		}
		/* vietnamese */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/59ZRklaO5bWGqF5A9baEERJtnKITppOI_IvcXXDNrsc.woff2) format('woff2');
		  unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
		}
		/* latin-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/u-WUoqrET9fUeobQW7jkRRJtnKITppOI_IvcXXDNrsc.woff2) format('woff2');
		  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
		}
		/* latin */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 400;
		  src: local('Open Sans'), local('OpenSans'), url(https://fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3VtXRa8TVwTICgirnJhmVJw.woff2) format('woff2');
		  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
		}
		/* cyrillic-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 600;
		  src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSq-j2U0lmluP9RWlSytm3ho.woff2) format('woff2');
		  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
		}
		/* cyrillic */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 600;
		  src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSpX5f-9o1vgP2EXwfjgl7AY.woff2) format('woff2');
		  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		}
		/* greek-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 600;
		  src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNShWV49_lSm1NYrwo-zkhivY.woff2) format('woff2');
		  unicode-range: U+1F00-1FFF;
		}
		/* greek */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 600;
		  src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSqaRobkAwv3vxw3jMhVENGA.woff2) format('woff2');
		  unicode-range: U+0370-03FF;
		}
		/* vietnamese */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 600;
		  src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSv8zf_FOSsgRmwsS7Aa9k2w.woff2) format('woff2');
		  unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
		}
		/* latin-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 600;
		  src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSj0LW-43aMEzIO6XUTLjad8.woff2) format('woff2');
		  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
		}
		/* latin */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 600;
		  src: local('Open Sans Semibold'), local('OpenSans-Semibold'), url(https://fonts.gstatic.com/s/opensans/v13/MTP_ySUJH_bn48VBG8sNSugdm0LZdjqr5-oayXSOefg.woff2) format('woff2');
		  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
		}
		/* cyrillic-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzK-j2U0lmluP9RWlSytm3ho.woff2) format('woff2');
		  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
		}
		/* cyrillic */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzJX5f-9o1vgP2EXwfjgl7AY.woff2) format('woff2');
		  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		}
		/* greek-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzBWV49_lSm1NYrwo-zkhivY.woff2) format('woff2');
		  unicode-range: U+1F00-1FFF;
		}
		/* greek */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzKaRobkAwv3vxw3jMhVENGA.woff2) format('woff2');
		  unicode-range: U+0370-03FF;
		}
		/* vietnamese */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzP8zf_FOSsgRmwsS7Aa9k2w.woff2) format('woff2');
		  unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
		}
		/* latin-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzD0LW-43aMEzIO6XUTLjad8.woff2) format('woff2');
		  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
		}
		/* latin */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 700;
		  src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v13/k3k702ZOKiLJc3WVjuplzOgdm0LZdjqr5-oayXSOefg.woff2) format('woff2');
		  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
		}
		/* cyrillic-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 800;
		  src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url(https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hq-j2U0lmluP9RWlSytm3ho.woff2) format('woff2');
		  unicode-range: U+0460-052F, U+20B4, U+2DE0-2DFF, U+A640-A69F;
		}
		/* cyrillic */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 800;
		  src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url(https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hpX5f-9o1vgP2EXwfjgl7AY.woff2) format('woff2');
		  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
		}
		/* greek-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 800;
		  src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url(https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hhWV49_lSm1NYrwo-zkhivY.woff2) format('woff2');
		  unicode-range: U+1F00-1FFF;
		}
		/* greek */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 800;
		  src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url(https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hqaRobkAwv3vxw3jMhVENGA.woff2) format('woff2');
		  unicode-range: U+0370-03FF;
		}
		/* vietnamese */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 800;
		  src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url(https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hv8zf_FOSsgRmwsS7Aa9k2w.woff2) format('woff2');
		  unicode-range: U+0102-0103, U+1EA0-1EF9, U+20AB;
		}
		/* latin-ext */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 800;
		  src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url(https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hj0LW-43aMEzIO6XUTLjad8.woff2) format('woff2');
		  unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
		}
		/* latin */
		@font-face {
		  font-family: 'Open Sans';
		  font-style: normal;
		  font-weight: 800;
		  src: local('Open Sans Extrabold'), local('OpenSans-Extrabold'), url(https://fonts.gstatic.com/s/opensans/v13/EInbV5DfGHOiMmvb1Xr-hugdm0LZdjqr5-oayXSOefg.woff2) format('woff2');
		  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
		}
		#best_affiliate_programs {
			margin: 70px auto 0;
		}
		.best_affiliate_programs_text_box {
			background-color: #FF5A00;
			color: #fff;
			text-align: center;
			font-size: 30px;
			padding: 20px 0;
			margin-bottom:30px;
		}
		.best_affiliate_programs_cat_wrapper {
			max-width: 1700px;
			margin: 0 auto;
			text-align: center;
		}
		.city_big_trial_bg.mobile_only {
			display:none;
		}
		.best_affiliate_programs_cat{
			width: 271px;
			text-align: left;
			line-height: 20px;
			margin: 0 auto 10px;
			display: inline-block;
			font-size: 14px;
			padding-left: 5px;
		}
		.best_affiliate_programs_cat a {
			text-decoration: none;
			display: block;
			line-height: 18px;
			padding: 5px 0;
			color: #000;
			font-weight: 500;
			cursor: pointer;
			-webkit-transition: background 0.2s linear;
			-moz-transition: background 0.2s linear;
			-ms-transition: background 0.2s linear;
			-o-transition: background 0.2s linear;
			transition: background 0.2s linear;
		}
		.best_affiliate_programs_cat a:hover {
			text-decoration: underline;
		}
		@media (max-width:565px){
			#best_affiliate_programs {
				margin-top:0;
			}
			.city_big_trial_bg {
				display:none;
			}.city_big_trial_bg.mobile_only {
				display:block;
			}
			.best_affiliate_programs_cat br {
				display:none;
			}
			.best_affiliate_programs_cat {
				width:100%;
				padding:0 15px;
			}
			.best_affiliate_programs_text_box {
				position:relative;
				cursor: pointer;
				font-size:22px;
				margin-bottom:-15px;
			}
			.best_affiliate_programs_text_box:before {
				content: "";
				display: block;
				position: absolute;
				width: 12px;
				height: 12px;
				top: 35px;
				right: 20px;
				margin-top: -3px;
				border-right: 2px solid #fff;
				border-bottom: 2px solid #fff;
				-webkit-transform: rotate(-45deg);
				-ms-transform: rotate(-45deg);
				transform: rotate(-45deg);
				transition: -webkit-transform .3s;
				transition: transform .3s;
			}
			.best_affiliate_programs_opened.best_affiliate_programs_text_box:before{
				-webkit-transform: rotate(45deg);
				-ms-transform: rotate(45deg);
				transform: rotate(45deg);
			}
			.best_affiliate_programs_cat_wrapper {
				display:none;
				margin-top:25px;
			}
		}
		@media (max-width:450px){
			.best_affiliate_programs_text_box {
				font-size:16px;
			}
			.best_affiliate_programs_text_box:before {
				top: 29px;
				right: 13px;
			}
		}
	</style>
</head>
<body class="inner_page no_header_script" data-spy="scroll" data-offset="100" id="sTop"
      onload="MM_preloadImages('/images/logos/alibaba1.png','/images/logos/booking1.png','/images/logos/quibids1.png','/images/logos/aliexpress1.png','/images/logos/dx1.png','/images/logos/gameforge1.png','/images/logos/glassesusa1.png','/images/logos/playtika1.png','/images/logos/egentic1.png','/images/logos/agoda1.png','/images/logos/r2games1.png','/images/logos/plus5002.png','/images/logos/zoosk2.png','/images/logos/avon2.png','/images/logos/toluna2.png','/images/logos/mackeeper2.png','/images/logos/toluna2.png','/images/logos/goodgame1.png','/images/logos/mobogenie1.png','/images/logos/plarium1.png','/images/logos/travian1.png','/images/logos/wargamingnet1.png','/images/logos/innogames1.png','/images/logos/hotels2.png','/images/logos/ubisoft2.png','/images/logos/riot1.png','/images/play2.png')">
<!-- Start of modals --><div class="cd-user-modal">	<div class="cube cd-user-modal-container">		<div id="cd-login">			<div class="theX">				<a href="#">					<span class="site_sprite small_x"></span>				</a>			</div>			<div class="theLogo">				<img class="logoImg" src="/img/login_logo.jpg" />			</div>			<div class="theFields">				<form action="https://partners.affiliaxe.com/" method="post" class="cd-form">					<input name="_method" type="hidden" value="POST" />					<input name="data[User][email]" placeholder="E-mail" class="myFields element enve" type="text" />					<input name="data[User][password]" type="password" class="myFields element padder passw" placeholder="Password" />				</form>			</div>			<div class="theBtnHolder">				<div class="theBtn">					<a href="#" target="_blank">Login</a>				</div>			</div>			<div class="lineText">New to affiliaXe? <strong><a href="/New-Affiliate-Signup.php">Get started here</a></strong></div>		</div>		<div id="cd-contact">			<div class="cubeContact">				<div class="theX">					<a href="#">						<span class="site_sprite small_x"></span>					</a>				</div>				<div class="theLogo">					<img class="logoImg" src="/img/login_logo.jpg" />				</div>				<div class="theFieldsContact">					<div class="theName"><strong>Name:</strong> <span class="contact_name"></span></div>					<div class="theEmail"><strong>Email:</strong> <span class="contact_email"></span></div>					<div class="theMessage">						<strong>							<span>Message: </span>						</strong>						<br />						<span class="contact_message"></span>      					</div>				</div>				<div class="captchaHolder">					<div class="g-recaptcha" id="g-recaptcha"></div> 				</div>				<div class="theBtnHolderContact">					<div class="theBtnContact">						<a href="" disable>Send</a>					</div>				</div>			</div>		</div>		<div id="cd-phone_msg">			<div class="cubePhone">				<div class="theX">					<a href="#">						<span class="site_sprite small_x"></span>					</a>				</div>				<h2><img src="/img/phone_msg_image.png">Attention!</h2>				<span class="modal_text">Please make sure you enter a valid phone number where you can be reached. Phone verification is required for account activation. We won't approve your account without calling and talking to you first on the Phone, NOT on Skype or eMail.</span>				<div class="theBtnHolderContact phone_button_wrapper clearfix">					<span class="orange_btn close_modal_button">Ok, got it!</span>				</div>			</div>		</div>	</div></div><!-- End of modals --><div class="page-wrap" id="topPage">
    <nav id="my-menu" class="oblurlay">
        <div id="sidr" class="sidr left">
            <div class="whitelogo">
                <img src="/img/logo_main_white.png" style="max-width:190px; height:auto;"/>
            </div>
            <ul id="menuUl">
                <li class="lang_mobile">
					<a class="linkNoSub">Language</a>
					<select class="lang_mobile_select">
						<option value=""></option>
						<option value="en">English</option>
						<option value="ar">العربية</option>
						<option value="id">Bahasa Indonesia</option>
						<option value="my">Bahasa Malaysia</option>
						<option value="cz">Česká</option>
						<option value="cn">繁體中文</option>
						<option value="dk">Dansk</option>
						<option value="de">Deutsch</option>
						<option value="fr">Français</option>
						<option value="he">עברית</option>
						<option value="it">Italiano</option>
						<option value="hu">Magyar</option>
						<option value="nl">Nederlands</option>
						<option value="no">Norsk</option>
						<option value="jp">日本語</option>
						<option value="kr">한국어</option>
						<option value="pl">Polski</option>
						<option value="pt">Português</option>
						<option value="ro">Română</option>
						<option value="es">Español</option>
						<option value="fi">Suomi</option>
						<option value="ru">Русский</option>
						<option value="se">Svenska</option>
						<option value="th">ภาษาไทย</option>
					</select>
				</li>
				<div class="centered lang_mobile_hr">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
				<li>
					<a class="linkNoSub" href="/#topPage">Home</a>
				</li>
                <div class="centered">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
                <li>
					<a class="linkNoSub" href="/what_is_affiliate_marketing.php">What is affiliate marketing?</a>
				</li>
				<div class="centered">
					<hr class="faded">
					<hr class="faded2">
				</div>
				<li>
					<a class="menuHeading" href="/best-affiliate-programs.php">Best Affiliate Programs</a>
				</li>
                <div class="centered">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
                <li>
					<a class="menuHeading" href="/aboutus.php">About Us</a>
                    <i>
						<a class="menuSub" href="/aboutus.php">Who We Are</a>
					</i>
                </li>
                <div class="centered">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
				<li>
					<a class="menuHeading" href="/#publishers">Affiliate</a>
                    <i>
						<a class="menuSub" href="/#publishers">Maximize Traffic Monetization</a>
					</i>
                </li>
                <div class="centered">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
                <li>
					<a class="menuHeading" href="/#advertisers">Advertiser</a>
                    <i>
						<a class="menuSub" href="/#advertisers">Aquire &amp; Engage Customers</a>
					</i>
                </li>
                <div class="centered">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
                <li>
					<a href="/blog" target="_blank" class="menuHeading">Blog</a>
                    <i>
						<a class="menuSub" href="/blog" target="_blank">Keep Up To Date with affiliaXe</a>
					</i>
                </li>
                <div class="centered">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
                <li>
					<a href="/faq.php" class="menuHeading">FAQ</a>
                    <i>
						<a href="/faq.php" class="menuSub">Your Questions Answered</a>
					</i>
                </li>
                <div class="centered">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
                <li>
					<a class="menuHeading" href="http://jobs.affiliaxe.com/" target="_blank">Job Openings</a>
                    <i>
						<a class="menuSub" href="http://jobs.affiliaxe.com/" target="_blank">Join Our Team</a>
					</i>
                </li>
                <div class="centered">
                    <hr class="faded">
                    <hr class="faded2">
                </div>
                <li>
					<a class="linkNoSub" href="#footersignup">Contact Us</a>
                </li>
            </ul>
            <div class="certi">
                <div class="iab"><span class="site_sprite iab_sprite"></span></div>
                <div class="pma"><img src="/img/pma_menu.png"/></div>
            </div>
        </div>
    </nav>
    <!-- fixed top navigation &amp; logo -->
    <header>
        <div id="nav_container" class="scroll_div blurrMe" style="z-index:9999;">
            <div id="mynewmenu" class="clearfix">
                <!--<a id="simple-menu" href="#sidr"><span>MENU</span>-->
                <a id="simple-menu" href="#my-menu" class="clearfix"><span>MENU</span>

                    <div id="menu_humburger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
            </div>
            <div id="mylogo">
                <a href="/"><img class="mylogoimg" src="/img/logo_main.png"/></a>
            </div>
            <div id="mysignbtns">
                <ul class="nav navbar-nav navbar-right">
                    <li>
						<div class="lang_menu">
							<img src="/img/lang_menu/en_menu.png?50" />
							<span>&#x25BC;</span>
							<div id="lang_holder">
								<div class="arrow-up"></div>
								<div class="flagsContainer">
									<div class="languagesHeading">All Languages</div>
									<div class="flagsLeft">
										<ul class="flagList">
											<li class="defaultLanguage" title="English">
												<a href="/privacypolicy.php?lang=en">
												<div class="theFlag"><span class="flag_sprite flag_en"></span></div>
													<div class="countryName">English</div>
												</a>
											</li>
											<li  title="العربية">
												<a href="/privacypolicy.php?lang=ar">
													<div class="theFlag"><span class="flag_sprite flag_ar"></span></div>
													<div class="countryName">العربية</div>
												</a>
											</li>
											<li  title="Bahasa Indonesia">
												<a href="/privacypolicy.php?lang=id">
													<div class="theFlag"><span class="flag_sprite flag_id"></span></div>
													<div class="countryName">Bahasa Indonesia</div>
												</a>
											</li>
											<li  title="Bahasa Malaysia">
												<a href="/privacypolicy.php?lang=my">
													<div class="theFlag"><span class="flag_sprite flag_my"></span></div>
													<div class="countryName">Bahasa Malaysia</div>
												</a>
											</li>
											<li  title="Česká">
												<a href="/privacypolicy.php?lang=cz">
													<div class="theFlag"><span class="flag_sprite flag_cz"></span></div>
													<div class="countryName">Česká</div>
												</a>
											</li>
											<li  title="繁體中文">
												<a href="/privacypolicy.php?lang=cn">
													<div class="theFlag"><span class="flag_sprite flag_cn"></span></div>
													<div class="countryName">繁體中文</div>
												</a>
											</li>
											<li  title="Dansk">
												<a href="/privacypolicy.php?lang=dk">
													<div class="theFlag"><span class="flag_sprite flag_dk"></span></div>
													<div class="countryName">Dansk</div>
												</a>
											</li>
											<li  class="noBorderBottom" title="Deutsch">
												<a href="/privacypolicy.php?lang=de">
													<div class="theFlag"><span class="flag_sprite flag_de"></span></div>
													<div class="countryName">Deutsch</div>
												</a>
											</li>
										</ul>
									</div>
									<div class="flagsCenter">
										<ul class="flagList">
											<li  title="Français">
												<a href="/privacypolicy.php?lang=fr">
													<div class="theFlag"><span class="flag_sprite flag_fr"></span></div>
													<div class="countryName">Français</div>
												</a>
											</li>
											<li  title="עברית">
												<a href="/privacypolicy.php?lang=he">
													<div class="theFlag"><span class="flag_sprite flag_he"></span></div>
													<div class="countryName">עברית</div>
												</a>
											</li>
											<li  title="Italiano">
												<a href="/privacypolicy.php?lang=it">
													<div class="theFlag"><span class="flag_sprite flag_it"></span></div>
													<div class="countryName">Italiano</div>
												</a>
											</li>
											<li  title="Magyar">
												<a href="/privacypolicy.php?lang=hu">
													<div class="theFlag"><span class="flag_sprite flag_hu"></span></div>
													<div class="countryName">Magyar</div>
												</a>
											</li>
											<li  title="Nederlands">
												<a href="/privacypolicy.php?lang=nl">
													<div class="theFlag"><span class="flag_sprite flag_nl"></span></div>
													<div class="countryName">Nederlands</div>
												</a>
											</li>
											<li  title="Norsk">
												<a href="/privacypolicy.php?lang=no">
													<div class="theFlag"><span class="flag_sprite flag_no"></span></div>
													<div class="countryName">Norsk</div>
												</a>
											</li>
											<li  title="日本語">
												<a href="/privacypolicy.php?lang=jp">
													<div class="theFlag"><span class="flag_sprite flag_jp"></span></div>
													<div class="countryName">日本語</div>
												</a>
											</li>
											<li  class="noBorderBottom" title="한국어">
												<a href="/privacypolicy.php?lang=kr">
													<div class="theFlag"><span class="flag_sprite flag_kr"></span></div>
													<div class="countryName">한국어</div>
												</a>
											</li>
										 </ul>
									</div>
									<div class="flagsRight">
										<ul class="flagList">
											<li  title="Polski">
												<a href="/privacypolicy.php?lang=pl">	
													<div class="theFlag"><span class="flag_sprite flag_pl"></span></div>
													<div class="countryName">Polski</div>
												</a>
											</li>
											<li  title="Português">
												<a href="/privacypolicy.php?lang=pt">
													<div class="theFlag"><span class="flag_sprite flag_pt"></span></div>
													<div class="countryName">Português</div>
												</a>
											</li>
											<li  title="Română">
												<a href="/privacypolicy.php?lang=ro">
													<div class="theFlag"><span class="flag_sprite flag_ro"></span></div>
													<div class="countryName">Română</div>
												</a>
											</li>
											<li  title="Español">
												<a href="/privacypolicy.php?lang=es">
													<div class="theFlag"><span class="flag_sprite flag_es"></span></div>
													<div class="countryName">Español</div>
												</a>
											</li>
											<li  title="Suomi">
												<a href="/privacypolicy.php?lang=fi">
													<div class="theFlag"><span class="flag_sprite flag_fi"></span></div>
													<div class="countryName">Suomi</div>
												</a>
											</li>
											<li  title="Русский">
												<a href="/privacypolicy.php?lang=ru">
													<div class="theFlag"><span class="flag_sprite flag_ru"></span></div>
													<div class="countryName">Русский</div>
												</a>
											</li>
											<li  title="Svenska">
												<a href="/privacypolicy.php?lang=se">
													<div class="theFlag"><span class="flag_sprite flag_se"></span></div>
													<div class="countryName">Svenska</div>
												</a>
											</li>
											<li  class="noBorderBottom" title="ภาษาไทย">
												<a href="/privacypolicy.php?lang=th">
													<div class="theFlag"><span class="flag_sprite flag_th"></span></div>
													<div class="countryName">ภาษาไทย</div>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li id="signupbtn">
						<a href="/New-Affiliate-Signup.php" class="outer_link" style="display:block;">Get Started </a>
					</li>
                    <li id="signinbtn">
						<a href="#">Sign In</a>
					</li>
                </ul>
            </div>
        </div>
    </header>    <!-- Slider -->
    <section class="blurrMe" style="background-image: url(img/web_bg.jpg);">
    	<div class="centeredContainer">
       	  <div class="externalHeading">affiliaXe Privacy Policy</div>
       	  	<div class="privacy_content">
				<div>
					<strong>Introduction</strong>
				</div>
				<div>
					Thanks for visiting affiliaXe's Privacy Policy, updated and in sync with the new data protection regulation, in effect from May 25th, 2018.
					<br><br>
					This Privacy Policy Agreement between affiliaXe LTD (collectively “We,” “Us, or affiliaXe”) and the User ("You") was created to govern the way affiliaXe collects, uses, processes and disclose information collected from users of affiliaXe.com (“Website”). This Privacy Policy applies to the website, mobile applications, products and services offered by affiliaxe. By visiting the Website, you accept the terms and conditions contained in this Privacy Policy. Users of the Website include consumers who visit the Site, affiliates, and advertisers who work with affiliaXe via the Site and provide us with personal information.
				</div>
				<br>
				<div>
					<strong>About affiliaXe</strong>
				</div>
				<div>
					As an affiliate network, affiliaXe provides services via the Internet to both Publishers and Advertisers. The Website acts as an intermediary agent between a person, entity, affiliate or its agent ("Publisher") and an "Advertiser" (another person, company, or entity).
					<br><br>
					A Publisher is operating Website(s) (internet domain, or a portion of a domain), and other promotional methods to drive traffic to another Website or Website content. The Publisher may earn financial compensation ("Payouts") for "Transactions" (actions made by Visitors as defined by the Advertiser). The "Visitor" (any person or entity that is not the Publisher, the Publisher's agent, or the advertiser) is making an action (registration, purchase, signup, etc.) which is referred to the Publisher through an Internet connection ("Link") to a Website operated by the Advertiser. The Advertiser compensates the Publisher on actions, by this Agreement and the Program Payout specifications.
				</div>
				<br>
				<div>
					<strong>Which personal data we collect</strong>
				</div>
				<div>
					<h3>Website Users</h3>
					We collect data from you when you register to the Website. We consider personal data to be, but not limited to:
					<ul>
						<li>Full name</li>
						<li>Title</li>
						<li>Email address</li>
						<li>Mailing address</li>
						<li>Phone number</li>
					</ul>
				</div>
				<br>
				<div>
					For partners who work with us on a monthly payment basis, we may also ask for the additional identification details to be compliant with international and local KYC (Know Your Customer) regulations.
					<br><br>
					We may also collect non-personal identification information about users whenever they interact with our Site. The non-personal identification information may include:
					<ul>
						<li>Browser name</li>
						<li>Computer type and technical information about your connection to affiliaXe.com, such as the operating system, Internet service providers utilized and other similar information</li>
						<li>Unique mobile device ID number if you access our services via a mobile applications</li>
						<li>Details of your visits to the Website including, but not limited to, traffic data, location data, weblogs and other communication data, the site that referred you to our site and the resources that you access.</li>
					</ul>
				</div>
				<br>
				<div>
					You can always refuse to supply personal information, however, please consider that not providing personal information may prevent you from conducting any Site related activities and our being able to provide certain services to you and to interact with you as a Site user, such as in an affiliate or advertiser relationship with us.
				</div>
				<br>
				<div>
					<strong>Website Visitors</strong>
				</div>
				<div>
					For visitors to the Site (non-affiliates and non-advertisers) who do not share any information with us, we may collect information such as IP address, browser name, computer and technical connection information to our Website, mobile device type, network and unique ID. We do not, however, collect the e-mail address of such visitors to our Site.
				</div>
				<br>
				<div>
					<strong>Data processing &amp; use</strong>
				</div>
				<br>
				<div>
					We use external platforms to process and maintain the Personal Information of the User. These platforms act as data processors for the personal data for which we are the data controller.
					<br><br>
					The data processors have taken reasonable technical and organizational measures to protect against the information being accidentally or illegally destroyed, becoming lost or deteriorating, and to protect against the information being disclosed to unauthorized persons, being misused, or in other ways being processed in violation of data protection laws.
					<br><br>
					We may store personal data, and any other kind of data strictly in compliance with applicable international and local data protection and privacy legislation and our contractual duties and will prevent unauthorized access to data to the best of our abilities.
					<br><br>
					Provided you have given your consent or have not objected – where legal provisions allow so – we may use your personal data for advertising. You may withdraw your consent at any time and ask us to delete your Personal Information. When you register to affiliaXe and yet to click 'Submit,' your personal information will be retained for 30 days. Once you click 'Submit', your personal information will be automatically processed by third-party platforms as stated in the Data Controller and Processor section above.	
				</div>
				<br>
				<div>
					<strong>Why We Collect Personal Data</strong>
					Any of the information we collect from you may be used in one of the following ways:
				</div>
				<div>
					<ul>
						<li>We must process personal information for proving the identity of our users.</li>
						<li>We may process personal information to measure the performance of our users</li>
						<li>We may process personal information to create customized performance reports.</li>
						<li>We may process personal information to determine when Users respond to ads.</li>
						<li>We may process information to personalize user experience on affiliaXe.com, respond to inquiries and provide users with relevant content.</li>
						<li>Your contact information is used for, but not limited to, sending periodic administrative emails, updates and notifications regarding your account, promotional emails & further notifications relating your activity, and any changes to our terms, conditions, and policies.</li>
						<li>We may process your information to process transactions.</li>
						<li>We may use your information to improve and enhance the effectiveness of affiliaXe and services.</li>
						<li>We may use cookies, pixel tags and other tracking technologies to measure and improve our services and develop features and functionalities on our sites, apps, and APIs.</li>
						<li>We may use IP Addresses for administration and security purposes, such as calculating usage levels of our site, app, and API, helping diagnose server problems, and detecting fraud and spam behaviour.</li>
					</ul>
					Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent.
				</div>
				<br>
				<div>
					<strong>How we protect your personal data</strong>
				</div>
				<div>
					We implement a variety of security measures to keep your personal information safe when you enter, submit, or access your personal information.
					<ul>
						<li>Our website is scanned on a regular basis for security holes and known vulnerabilities to make your visit and contributions to our site as safe as possible. We employ various security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information.</li>
						<li>Secured database - Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems and are required to keep the information confidential.</li>
						<li>SSL encryption - All sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</li>
					</ul>
				</div>
				<br>
				<div>
					<strong>Data Collection Technologies</strong>
				</div>
				<h3>'Cookies'</h3>
				<div>
					We may use 'cookies' to enhance your experience. We use 'Cookies' to store your preferences for future visits, record information and user-specific information, such as what pages users access or visit. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow). Cookies enable the sites or service provider systems to recognize your browser, capture and remember certain information. You may choose to turn off 'cookies,' however, if you do, some features will be disabled - features which make your site experience more efficient and without them affiliaXe may not function properly.
				</div>
				<h3>Clear GIFs, pixel tags, and other technologies</h3>
				<div>
					Our service providers or we may use clear GIFs (also known as web beacons, web bugs or pixel tags), in connection with our Site to track the actions of Website users and visitors to our Site, help us manage content, and compile statistics about Site usage.
					<br><br>
					Our service providers or we may also use clear GIFs in HTML emails to our users, to help us track e-mail response rates, identify when our emails are viewed, and track whether our emails are forwarded.
				</div>
				<h3>Clear GIFs, pixel tags, and other technologies</h3>
				<div>
					Our service providers or we may use clear GIFs (also known as web beacons, web bugs or pixel tags), in connection with our Site to track the actions of Website users and visitors to our Site, help us manage content, and compile statistics about Site usage.
					<br><br>
					Our service providers or we may also use clear GIFs in HTML emails to our users, to help us track e-mail response rates, identify when our emails are viewed, and track whether our emails are forwarded.
				</div>
				<br>
				<div>
					<strong>You are the data controller</strong>
				</div>
				<div>
					You (i.e., the user) act as the data controller of information and content you choose to disclose on the Website. You control all data you change on your profile settings and data disclosed on your profile on Social Networks, which is the consequence of the connection of your profile on the Website with your profile on the Social Network.
				</div>
				<br>
				<div>
					<strong>Website links</strong>
				</div>
				<div>
					Reviewmaster.com contains links to other websites. Note that by clicking such links you will be transferred to other websites for which ReviewMaster is no longer the data controller. We thus recommend that you always read the personal data policy of these websites, as their procedure for collecting and processing personal data may be different from ours.
				</div>
				<br>
				<div>
					<strong>Data Processor</strong>	
				</div>
				<div>
					ReviewMaster utilizes an external company to maintain all the technical operation of the Website. This company is the data processor of the personal data for which ReviewMaster is the data controller.	
				</div>
				<div>
					By accepting this Privacy Policy, you accept that ReviewMaster allows all data for which you have control of to be processed by the same data processor.
					<br><br>
					The data processor for ReviewMaster has access to your data to ensure that the level of security required follows necessary safety measures against information being accidentally or illegally destroyed, lost or deteriorated. The personal data processor solely adheres to ReviewMaster instructions. By accepting these terms and conditions, you authorize ReviewMaster to give such instructions to the data processor.	
				</div>
				<br>
				<div>
					<strong>Security of Your Personal Information</strong>	
				</div>	
				<div>
					We implement a variety of security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information.
					<br><br>
					Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems and are required to keep the information confidential. Also, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.	
				</div>
				<br>
				<div>
					<strong>Your Rights</strong>
				</div>
				<div>
					Your principal rights under the data protection law are:
				</div>
				<ol type="a">
					<li>The right to visit the Website anonymously.</li>
					<li>The right to access your personal information.</li>
					<li>The right to sustain and delete your account, request that we stop using your data where it is no longer necessary.</li>
					<li>The right to download your personal information as appears on ReviewMaster.</li>
					<li>The right to unsubscribe from administrational notifications, newsletters, marketing content. If you do not wish to receive any emails from us, you can simply click on the unsubscribe hyperlink or change your email settings in your profile.</li>
					<li>The right to request information. By contacting support@reviewmaster.com, you may request information and details which we hold about you and the purposes for which it is being held.</li>
					<li>The right to request a correction. You have the right to request for corrections, deletion or blocking of your data stored by ReviewMaster. Once your request complies, you will be notified as to whether the action is completed and to what extent.</li>
					<li>The right to data security and integrity. We take appropriate technical and organizational measures to protect personal data against accidental or unlawful destruction, loss, alteration, unauthorized use, disclosure or access, in particular where the processing involves the transmission of data over a data processor, and against all other unlawful forms of processing and misuse.</li>
					<li>The right to opt out. Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt-Out page or by using the Google Analytics Opt-Out Browser Add-on.</li>
				</ol>
				<br>
				<div>
					<strong>Data Accessing, Corrections, Retention, and Deletion</strong>
				</div>
				<br>
				<div>
					<strong>Data Access</strong>
				</div>
				<div>
					At any point, you have the right to access your personal information to ensure its accuracy and relevancy for the purposes for which ReviewMaster have collected it.
				</div>
				<br>
				<div>
					<strong>Data Correction</strong>
				</div> 
				<div>
					You may correct/update all personal data for which you act as the controller, i.e., all data you change in your profile settings (Title/Position, Location, Biography, Email Settings). We hold the right to view and approve your changes and correction before appearing on our website.
				</div>
				<br>
				<div>
					<strong>Data Retention</strong>
				</div> 
				<div>
					We will retain your information for as long as your account is active or as needed to provide you ReviewMaster services.	
				</div>
				<br>
				<div>
					<strong>Data Deletion</strong>
				</div> 
				<div>
					Please notice that once you choose to delete your account, your personal information and other information including photos, videos, reviews, and other information submitted by the user or collected on the Website will not be retained.
					<br><br>
					Please note that reviews, compliments, and reports may not be deleted by the user once submitted on the Website. If you wish to access, amend, correct, remove, or limit the use of your personal information, contact us at support@reviewmaster.com.
				</div>
				<br>
				<div>
					<strong>Scope</strong>
				</div>
				<div>
					We act as a service provider to operators of other websites and applications to help them drive customer reviews. To the extent that we collect information – which may include your name, e-address, purchase information, and information from social media about you.
					<br><br>
					We do renounce obligation for privacy practices of websites or applications that are operated or owned by third parties, even if they are linked from and through the Site or Services. The information practices of such third-party websites or applications are governed by the privacy policies determined on those third-party websites or applications.	
				</div>
				<div>
					<strong>Data Collection Technologies</strong>
				</div>
				<br>
				<div>
					<strong>'Cookies'</strong>
				</div>
				<div>
					Our service providers or we may use cookies or a similar technology to track visitor activity on our Site.
					<br><br>
					You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since each browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.
					<br><br>
					If you turn cookies off, some features will be disabled - features which make your site experience more efficient and without them ReviewMaster may not function properly.	
				</div>
				<br>
				<div>
					<strong>Clear GIFs, pixel tags, and other technologies</strong>
				</div>
				<div>
					Our service providers or we may use clear GIFs (also known as web beacons, web bugs or pixel tags), in connection with our Site to track the actions of Website users and visitors to our Site, help us manage content, and compile statistics about Site usage.
					<br><br>
					Our service providers or we may also use clear GIFs in HTML emails to our users, to help us track e-mail response rates, identify when our emails are viewed, and track whether our emails are forwarded.	
				</div>
				<br>
				<div>
					<strong>Third-party analytics</strong>
				</div>
				<div>
					We use service providers, such as Google Analytics, to evaluate the use of our Site and Services. Our service providers or we use automated devices and applications to evaluate the use of our Site and Services. Our service providers or we use these tools to help us improve our Site, Services, performance, and user experiences.	
				</div>
				<br>		
				<div>
					<strong>Social plug-ins</strong>
				</div>
				<div>
					ReviewMaster provides you with social plug-ins from various social networks. If you choose to interact with a social network, your activity on our website will also be made available to social networks, such as Facebook or Twitter. If you share any information collected by the Website and submitted by you on the Website via a social plug-in, this information will be transferred to the social network site. 
					<br><br>
					You at this moment agree that ReviewMaster is no longer in charge of the data transferred via social plug-ins. We advise reading the privacy policies of those social networks for data collections and security information.	
				</div>
				<br>
				<div>
					<strong>Re-targeting technologies</strong>
				</div>
				<div>
					Our website utilizes re-targeting technologies that enable us to advertise directly to our visitors and users from our partners’ websites. Re-targeting technologies analyze your cookies and display advertisement based on your past surfing behavior. We believe that the display of a personalized, interest-based advertising is more beneficial for our website users. 
					<br><br>
					We also work with other companies who use tracking technologies to serve ads on our behalf across the Internet. These companies may collect information about your visits to our websites or (mobile) applications and your interaction with our communications, including advertising.	
				</div>
				<br>
				<div>
					<strong>Our disclosure of your personal data</strong>
				</div>
				<div>
					We consider your information to be a vital part of our relationship with you. Therefore, we do not sell, trade, or otherwise transfer your personal information to outside parties without your consent or as otherwise specified in this Privacy Policy.
					<br><br>
					Here are the following circumstances in which we may share your information with certain third parties:
					<ul>
						<li>We may transfer your information with your consent to third-parties</li>
						<li>We may process your personal information with trusted third-parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential.</li>
						<li>Following the Law, we may disclose your information to third parties if we determine that such disclosure is reasonably necessary to comply with the law, protect our rights or prevent fraud or abuse of affiliaXe, our users, and advertisers.</li>
						<li>We may also share aggregated or non-personally identifiable information with our partners or others for business purposes.</li>
					</ul>		
				</div>
				<br>
				<div>
					<h3>Links to third-party websites</h3>
					<div>Occasionally, at our discretion, we may include or offer third party products or services. These third-party sites have separate and independent privacy policies. We, therefore, have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites. We highly advise you to read the Privacy Policy and Terms & Conditions of such third-parties before any use.</div>
				</div>
				<br>
				<div>
					<strong>Your Rights</strong>
					Your principal rights under the data protection law are:
				</div>
				<div>
					<ul type="a">
						<li>The right to visit the Website anonymously.</li>
						<li>The right to access your personal information. You can click on the “My Account” button to access your data.</li>
						<li>The right to change your email settings under My Account or unsubscribe.</li>
						<li>The right to edit your account settings and personal information.</li>
						<li>The right to request an account and Personal Data deletion.</li>
					</ul>
				</div>
				<br>
				<div>
					<strong>Opt-Out</strong>
				</div>
				<div>
					Because you have submitted your email address upon registration, we may send periodic emails with information on new products, new offers, notifications, services or upcoming events. If you do not wish to receive emails from us, you may change your email settings under My Account or unsubscribe.
					<br><br>
					If you supply us your postal address, we may send you periodic mailings with information on new products and services or upcoming events. If you do not wish to receive such mailings, please let us know by calling us at the number provided below, writing to us at the below address, or e-mailing us at partners@affiliaxe.com.
					<br><br>
					Persons who supply us with their telephone numbers may receive telephone contact from us with information regarding new products and services or upcoming events. If you do not wish to receive such telephone calls, please let us know by calling us at the number provided below, writing to us at the below address, or e-mailing us at partners@affiliaxe.com.
				</div>
				<br>
				<div>
					<strong>How Does affiliaXe Comply with GDPR Regulations</strong>
				</div>
				<div>
					affiliaXe serves as a data controller under the GDPR and adheres to the principles and rules of the GDPR.
					<br><br>
					Under GDPR, affiliaXe takes the following measures about the personal data of EU residents:
					<ul>
						<li>Implements privacy by design and default principles.</li>
						<li>Ensures limited data retention protocols.</li>
						<li>Adheres to incident response norms.</li>
						<li>Allows for appropriate third-party audits.</li>
						<li>Ensures data is only transferred internationally on a lawful basis.</li>
						<li>Only contracts with sub-processors with adequate written commitments to data processing.</li>
						<li>Respects subject data rights and ensure that a copy of Personal Data is available.</li>
						<li>Does not process, sell or trade sensitive personal data, and provides data processing agreements to all appropriate users and partners.</li>
					</ul>
				</div>
				<br>
				<div>
					<strong>EU-U.S. Privacy Shield</strong>
				</div>
				<div>
					affiliaXe LTD. fully respect the Personal Information we receive from our customers in the U.S, EU, and Switzerland. The company complies with the U.S.-EU Privacy Shield Framework as set forth by the U.S. Department of Commerce regarding the collection, use, and retention of personal information transferred from the European Union to the United States. If there is any conflict between the terms of this privacy policy and the Privacy Shield Principles, the Privacy Shield Principles shall govern. To learn more about Privacy Shield, please visit www.privacyshield.gov.
				</div>
				<br>
				<div>
					<strong>COPPA (Children Online Privacy Protection Act)</strong>
				</div>
				<div>
					Under the Children’s Online Privacy Protection Act (COPPA), our Website, Products and Services are not intended for children under 13 years of age or the relevant age of majority, and we do not knowingly collect personal information from children under that age. If you are under 13 years of age or the age of majority, you are not permitted to create an account on affiliaXe and submit any personal information to us. If you are the parent or legal guardian of a child under 13 years old and you believe we have received personal information from your child, please contact us at partners@affiliaxe.com
				</div>
				<br>
				<div>
					<strong>CAN-SPAM Act</strong>
				</div>
				<div>
					The CAN-SPAM Act law sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.
					<br><br>
					We collect your email address to send information, respond to inquiries, and other requests or questions.
					<br><br>
					To be by CAN-SPAM, we agree to the following:
					<ul>
						<li>Not use false or misleading subjects or email addresses.</li>
						<li>Include the physical address of our business or site headquarters.</li>
						<li>Monitor third-party email marketing services for compliance, if one is used.</li>
						<li>Honor opt-out/unsubscribe requests quickly.</li>
						<li>Allow users to change email settings via their Account Settings.</li>
					</ul>
				</div>
				<br>
				<div>
					<strong>Online Privacy Policy Only</strong>
				</div>
				<div>
					This online privacy policy applies only to information collected through our website and not to information collected offline.
				</div>
				<br>
				<div>
					<strong>Changes to our Privacy Policy</strong>
				</div>
				<div>
					We reserve the right to change this Privacy Policy from time to time. Once we do, we will revise the updated date at the bottom of the page. We may notify you of such changes and updates via email or post them on this site. We highly advise to re-visit our Privacy Policy page to remain informed on how we collect and protect your personal information.
				</div>
				<br>
				<div>
					<strong>Contacting Us</strong>
				</div>
				<div>
					By using our site, you signify your consent to this privacy policy. If you do not agree to this policy, please do not use our site.
				</div>
				<br>
				<div>
					<strong>Your Consent</strong>
				</div>
				<div>
					If you have any questions or concerns regarding our privacy policy or that you believe affiliaXe LTD has failed to follow its Principles regarding your data, please contact us, and we will do our best to resolve any such disputes or complaints promptly.
				</div>
				<br>
				<div>
					<strong>Data Protection Authority:</strong>
				</div>
				<div>
					<strong>GDPR Officer</strong><br>
					reut@affiliaxe.com<br><br>
					<strong>affiliaXe Support</strong><br>
					<a href="mailto:partners@affiliaxe.com">mailto:partners@affiliaxe.com</a>
				</div>
				<br>
				<div>
					<strong>affiliaXe LTD.</strong>
				</div>
				<div>
					<a href="http://www.affiliaXe.com/">http://www.affiliaXe.com/</a><br>
					90 Begin Road<br>
					Tel Aviv, Israel 68037, Israel
				</div>
				</div>
			</div>
		</div>    
    </section>
    <!-- End Slider -->
				
		<!-- Main Content -->
		<div class="container_inner" style="margin-top:15px;">
			<article class="city_big_trial_bg">
                <img src="img/city_big_trial.png?50" alt="affiliaXe global affiliate network">
            </article>
            <footer>
                <div class="fullspan dark" id="p6">
                    <div class="container_footer">
                        <div class="row top_footer"><!-- end .col --><!-- end .col -->
                            <div class="col-md-4 visit footer_logo">
                                <img src="/img/logo_main.png" style="max-width:200px; height:auto;   padding-top: 22px;" alt="affiliaXe Affiliate Network">
                            </div>
                            <!-- end .col -->
                            <div class="col-md-4 working social-mid">
                                <ul class="socialist">
                                    <li style="font-size:18px; color:#fff;">Follow Us</li>
                                    <li>
                                        <a href="http://www.facebook.com/affiliaxe" target="_blank">
                                            <img src="/img/facebook.png"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.twitter.com/affiliaxe" target="_blank">
                                            <img src="/img/twitter.png"/>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://www.linkedin.com/company/3805985" target="_blank">
                                            <img src="/img/linkedin.png"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- end .col -->
                            <div class="col-md-4 contact">
                                <div class="activepublishers">
                                    <span style="color:#FFF;">19,755</span>
                                    <span class="grayfootertext">Active Publishers</span>
                                </div>
                                <div class="liveoffers">
									<span>
										<a href="http://status.affiliaxe.com" target="_blank">
											<span class="site_sprite circleBlink"></span>
										</a>
									</span>
									<span class="grayfootertext serversLink">
										<a href="http://status.affiliaxe.com" target="_blank" style="color:#8e8e8e;">
											Servers Online										</a>
									</span>
								</div>
                            </div>
                            <!-- end .col -->
                        </div>
                        <hr class="footerhr">
                        <hr class="footerhr2">
                        <div class="row bottom_footer"><!-- end .col --><!-- end .col -->
                            <div class="col-md-4 working">
                                <h4>Site Map</h4>
                                <p class="sitemap" style="line-height:22px;">
                                    <a href="/aboutus.php" class="outer_link">> &nbsp; About Us</a><br/>
                                    <a href="/#publishers" class="outer_link">> &nbsp; Affiliate</a><br/>
                                    <a href="/#advertisers" class="outer_link">> &nbsp; Advertiser</a><br/>
                                    <a href="/best-affiliate-programs.php">> &nbsp; Offers / Campaigns</a><br/>
                                    <a href="/blog" target="_blank">> &nbsp; Blog</a><br/>
									<a href="/faq.php" class="outer_link">> &nbsp; FAQ</a><br/>
                                    <a href="http://jobs.affiliaxe.com/" target="_blank">> &nbsp; Job Openings</a><br/>
                                    <a href="http://www.reviewmaster.com" target="_blank" title="ReviewMaster - Website Reviews">> &nbsp; Website Reviews</a><br/>
                                    <a href="/privacypolicy.php">> &nbsp; Privacy Policy</a>
                                </p>
                            </div>
                            <!-- end .col -->
                            <div class="col-md-4 visit">
                                <h4>affiliaXe Headquarters</h4>
                                <div id="telephone" class="site_sprite">&nbsp;</div>
                                <div id="telnumber"> &nbsp;<a href="tel:+97235463113">+(972) 3 5463113</a></div>
                                <br>
                                <p>90 Begin Road <br>
                                    2nd Floor<br>
                                    Tel Aviv 68037<br>
                                    Israel<br>
                            </div>
                            <!-- end .col -->
                            <div class="col-md-4 contact">
                                <h4>Contact Us</h4>
                                <div id="footersignup">
                                    <form method="post" id="footer_contact_form" class="contactform">
										<input type="text" name="firstname" id="firstname" class="input" placeholder="First Name" style="width:48.75%;"/>
										<input type="text" name="lasttname" id="lasttname" class="input" placeholder="Last Name" style="width:48.75%; float:right;"/>
										<input type="text" name="emailcontact" id="emailcontact" class="input" placeholder="Email" />
										<textarea name="message" id="message" class="textarea" placeholder="Message"></textarea>
										<div style="margin:4px auto 9px; text-align:center;">
											<input type="submit" name="sendmessage" id="sendmessage" class="submit sendmessage" value="Send" />
										</div>
									</form>
                                </div>
                            </div>
                            <!-- end .col -->
                        </div>
                    </div>
                    <!-- end .container -->
                </div>
                <!-- end .fullspan -->
                <!-- Footer Inner -->
                <div class="container">
                    <div class="row" style="padding-bottom:20px;">
                    </div>
                </div>
                <!-- Footer Copyright -->
                <div class="fullspan copy">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="footerleft">&copy; 2010-2018 affiliaXe Ltd.</div>
                                <div class="footerright">ALL RIGHTS RESERVED</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Footer Copyright -->
            </footer>
            <a href="" class="back-to-top" style="display: inline;">
                <i class="fa fa-chevron-circle-up"></i>
            </a>
        </div>
    </div>
</div>

	<script src="/js/jquery-2.1.3.min.js?50"></script>
	<script data-pace-options='{ "document": false }' src="/js/pace.min.js?50"></script>
	<script src="/js/combined.js?50"></script>
	<script src="/js/fastclick.js?50"></script>
	<script src="/js/main.js?50"></script>
	<script src="/js/custom.js?50"></script>
<script type="text/javascript">
	/*Pre-load images scripts*/
	function MM_swapImgRestore() { /*v3.0*/
		var i, x, a = document.MM_sr;
		for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
	}
	function MM_preloadImages() { /*v3.0*/
		var d = document;
		if (d.images) {
			if (!d.MM_p) d.MM_p = new Array();
			var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
			for (i = 0; i < a.length; i++)
				if (a[i].indexOf("#") != 0) {
					d.MM_p[j] = new Image;
					d.MM_p[j++].src = a[i];
				}
		}
	}

	function MM_findObj(n, d) { /*v4.01*/
		var p, i, x;
		if (!d) d = document;
		if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
			d = parent.frames[n.substring(p + 1)].document;
			n = n.substring(0, p);
		}
		if (!(x = d[n]) && d.all) x = d.all[n];
		for (i = 0; !x && i < d.forms.length; i++) x = d.forms[i][n];
		for (i = 0; !x && d.layers && i < d.layers.length; i++) x = MM_findObj(n, d.layers[i].document);
		if (!x && d.getElementById) x = d.getElementById(n);
		return x;
	}

	function MM_swapImage() { /*v3.0*/
		var i, j = 0, x, a = MM_swapImage.arguments;
		document.MM_sr = new Array;
		for (i = 0; i < (a.length - 2); i += 3)
			if ((x = MM_findObj(a[i])) != null) {
				document.MM_sr[j++] = x;
				if (!x.oSrc) x.oSrc = x.src;
				x.src = a[i + 2];
			}
	}
</script>
<script type="text/javascript">
	window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
	heap.load("3786476009");
</script>
<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-21687670-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '913710691999207');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=913710691999207&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script type="text/javascript">
	adroll_adv_id = "NJIBF4FPQVALBH23E7CJSO";
	adroll_pix_id = "3J35IBDHFJBQZBPYA6USAG";
	(function () {
	var oldonload = window.onload;
	window.onload = function(){
	   __adroll_loaded=true;
	   var scr = document.createElement("script");
	   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
	   scr.setAttribute('async', 'true');
	   scr.type = "text/javascript";
	   scr.src = host + "/j/roundtrip.js";
	   ((document.getElementsByTagName('head') || [null])[0] ||
		document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
	   if(oldonload){oldonload()}};
	}());
</script>
</body>
</html>