<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="This privacy policy document describes in detail the types
		of personal information is collected and recorded by audiotrimmer.com and how we use it.">
    
    <link rel="icon" href="../../images/favicon.ico">
	<link rel="icon" type="image/png" sizes="32x32" href="../../images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../images/favicon-16x16.png">
	<link rel="canonical" href="http://audiotrimmer.com/privacy-policy.php" />
    <title>Privacy Policy | Audio Trimmer</title>
	<meta property="og:title" content="Privacy Policy | Audio Trimmer">
		<meta property="og:site_name" content="Main Website">
		<meta property="og:description" content="This privacy policy document describes in detail the types
		of personal information is collected and recorded by audiotrimmer.com and how we use it.">
		<meta property="og:url" content="http://audiotrimmer.com/privacy-policy.php">
		<meta property="og:type" content="Website">
		<meta property="og:locale" content="en_US">
		<meta property="og:image" content="http://audiotrimmer.com/images/audiotrimmer.jpg">
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:widgets:csp" content="on">
		<meta name="twitter:url" content="http://audiotrimmer.com/privacy-policy.php">
		<meta name="twitter:description" content="This privacy policy document describes in detail the types
		of personal information is collected and recorded by audiotrimmer.com and how we use it.">
		<meta name="twitter:title" content="Privacy Policy | Audio Trimmer">
		<meta name="twitter:site" content="Main Website">
		<meta name="twitter:image" content="http://audiotrimmer.com/images/audiotrimmer.jpg">
    <!-- Bootstrap core CSS -->
    <link href="../../css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="../../css/style3.css" rel="stylesheet">
	
    
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
	
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-73759507-1', 'auto');
  ga('send', 'pageview');

</script>	    
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="http://audiotrimmer.com"><img src="../../images/logo.png" alt="Online Audio Trimmer"/></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
			<li><a href="http://audiotrimmer.com">AUDIO TRIMMER</a></li>
			<li><a href="http://audiotrimmer.com/online-mp3-converter/">MP3 CONVERTER</a></li>
			<li><a href="http://audiotrimmer.com/audio-speed-changer/">AUDIO SPEED CHANGER</a></li>
			</ul>
          <!-- Twitter -->
<a href="http://twitter.com/home?status=Trim your audio files directly on your browser http://audiotrimmer.com" title="Share on Twitter" target="_blank" class="btn btn-twitter pull-right margin5"><i class="fa fa-twitter"></i></a>
 <!-- Facebook -->
<a href="https://www.facebook.com/sharer/sharer.php?u=http://audiotrimmer.com" title="Share on Facebook" target="_blank" class="btn btn-facebook pull-right margin5"><i class="fa fa-facebook"></i></a>
<!-- Google+ -->
<a href="https://plus.google.com/share?url=http://audiotrimmer.com" title="Share on Google+" target="_blank" class="btn btn-googleplus pull-right margin5"><i class="fa fa-google-plus"></i></a>
<!-- StumbleUpon -->
<a href="http://www.stumbleupon.com/submit?url=http://audiotrimmer.com" title="Share on StumbleUpon" target="_blank" data-placement="top" class="btn btn-stumbleupon pull-right margin5"><i class="fa fa-stumbleupon"></i> </a>
<!-- LinkedIn --> 
<a href="http://www.linkedin.com/shareArticle?mini=true&url=http://audiotrimmer.com&title=Online Mp3 Trimmer" title="Share on LinkedIn" target="_blank" class="btn btn-linkedin pull-right margin5"><i class="fa fa-linkedin"></i></a>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

 <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <h1>Privacy Policy</h1>
		<p>At audiotrimmer.com we consider the privacy of our visitors to be extremely important. This privacy policy document describes in detail the types
		of personal information is collected and recorded by audiotrimmer.com and how we use it.</p>
		<h2>Uploaded Content</h2>
		<p>Audio Trimmer helps its users trim their audio files online. The uploaded audio content is stored in temporary folders on our servers to perform
		these operations. Files uploaded to Audio Trimmer are automatically deleted from our servers at regular intervals. We don't store or save your content permanently
		and don't make your content publicly available in any form.</p>
         <h2>Log Files</h2>
		 <p>Like many other Web sites, audiotrimmer.com makes use of log files. These files merely include raw visitor traffic to the site. The information inside the log files includes internet protocol (IP) addresses, browser
		 type, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and possibly the number of clicks. This information is used to analyze
		 trends, administer the site, track user's movement around the site, and gather demographic information. IP addresses, and other such information are
		 not linked to any information that is personally identifiable. </p>
		 <h2>Cookies and Web Beacons</h2>
		 <p>Audio Trimmer doesn't use cookies to collect personal information although our third party partners, such as Google Adsense and Google Analytics, might 
		 use cookies to record general data about your visit, such as your IP address, user agent, country, etc. Our third party partners:</p>
		 <h3>DoubleClick DART Cookie</h3>
		 <ul>
		 <li>Google, as a third party vendor, uses cookies to serve ads on audiotrimmer.com.</li>
		 <li>Google's use of the DART cookie enables it to serve ads to our site's visitors based upon their visit to audiotrimmer.com and other sites on the Internet. </li>
		 <li>Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at the following URL 
		 - <a href="http://www.google.com/privacy_ads.html" title="Opt out of the Dart Cookie">http://www.google.com/privacy_ads.html</a></li>
		 </ul>
		 <h3>Google Analytics</h3>
		 <p>Audio Trimmer uses Google Analytics as a third party service to record and analyse the traffic to audiotrimmer.com. Google Analytics basically records
		 data about your visit to Audio Trimmer, such as, your IP address, user agent, location, duration of your visit, pages you browsed etc. These information doesn't
		 include personally identifying data.</p>
		 <p>For more info about the cookies Google use, please visit <a href="http://www.google.com/policies/technologies/ads/" target="_blank">http://www.google.com/policies/technologies/ads/</a>
		 <h2>Consent</h2>
		 <p>By using our website, you hereby consent to our privacy policy and agree to its terms. We may update this Privacy Policy from time to time as we update the functionality
		 and services provided on Audio Trimmer. It is your responsibility to check this page regularly to ensure that you are happy with any changes.</p>
      </div>

      <hr>

      <footer>
		<div class="dropup pull-right margin10">
		  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			Language: English
			<span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
			<li><a href="/tr/gizlilik.php">Türkçe</a></li>
			<li><a href="/privacy-policy.php">English</a></li>
		 </ul>
		</div>
		<p class="pull-right margin10"><a href="privacy-policy.php">Privacy Policy</a>
        <p>&copy; 2016 AudioTrimmer.com</p>
		
      </footer>
    </div> <!-- /container -->

			

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="../../js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
