<!DOCTYPE html>
	<html>
		<head lang="en">

			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

			<title>Privacy Policy - TV Tropes</title>
			<meta name="description" content="" />

	    	    <link rel="shortcut icon" href="https://static.tvtropes.org/img/icons/favicon.ico" type="image/x-icon" />

	    	    <meta name="twitter:card" content="summary_large_image" />
	    <meta name="twitter:site" content="@tvtropes" />
	    <meta name="twitter:owner" content="@tvtropes" />
	    <meta name="twitter:title" content="Privacy Policy - TV Tropes" />
	    <meta name="twitter:description" content="" />
	    <meta name="twitter:image:src" content="https://static.tvtropes.org/logo_blue_small.png" />

	    	    <meta property="og:site_name" content="TV Tropes" />
	    <meta property="og:locale" content="en_US" />
	    <meta property="article:publisher" content="https://www.facebook.com/tvtropes" />
			<meta property="og:title" content="Privacy Policy" />
			<meta property="og:type" content="" />
			<meta property="og:url" content="https://tvtropes.org/pmwiki/privacypolicy.php" />
			<meta property="og:image" content="https://static.tvtropes.org/logo_blue_small.png" />
			<meta property="og:description" content="" />

				    <link rel="apple-touch-icon" sizes="57x57" href="https://static.tvtropes.org/img/icons/apple-icon-57x57.png" type="image/png">
	    <link rel="apple-touch-icon" sizes="60x60" href="https://static.tvtropes.org/img/icons/apple-icon-60x60.png" type="image/png">
	    <link rel="apple-touch-icon" sizes="72x72" href="https://static.tvtropes.org/img/icons/apple-icon-72x72.png" type="image/png">
	    <link rel="apple-touch-icon" sizes="76x76" href="https://static.tvtropes.org/img/icons/apple-icon-76x76.png" type="image/png">
	    <link rel="apple-touch-icon" sizes="114x114" href="https://static.tvtropes.org/img/icons/apple-icon-114x114.png" type="image/png">
	    <link rel="apple-touch-icon" sizes="120x120" href="https://static.tvtropes.org/img/icons/apple-icon-120x120.png" type="image/png">
	    <link rel="apple-touch-icon" sizes="144x144" href="https://static.tvtropes.org/img/icons/apple-icon-144x144.png" type="image/png">
	    <link rel="apple-touch-icon" sizes="152x152" href="https://static.tvtropes.org/img/icons/apple-icon-152x152.png" type="image/png">
	    <link rel="apple-touch-icon" sizes="180x180" href="https://static.tvtropes.org/img/icons/apple-icon-180x180.png" type="image/png">
	    <link rel="icon" sizes="16x16" href="https://static.tvtropes.org/img/icons/favicon-16x16.png" type="image/png">
	    <link rel="icon" sizes="32x32" href="https://static.tvtropes.org/img/icons/favicon-32x32.png" type="image/png">
	    <link rel="icon" sizes="96x96" href="https://static.tvtropes.org/img/icons/favicon-96x96.png" type="image/png">
	    <link rel="icon" sizes="192x192" href="https://static.tvtropes.org/img/icons/favicon-192x192.png" type="image/png">

	    	    

						<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1">


	    


						

													
                                        <script type="text/javascript" src="https://d1il9t8pu4dsoj.cloudfront.net/script.js"></script>
            
                                    <script type='text/javascript'>
              window.special_ops =  {
			    member : 'no',
			    isolated : 0,
			    tags : ['unknown']
              };
            </script>

			
				<script>
				var propertag = propertag || {};
				propertag.cmd = propertag.cmd || [];

				(function() {
				 var pm = document.createElement('script');
				 pm.async = true; pm.type = 'text/javascript';
				 var is_ssl = 'https:' == document.location.protocol;
				 pm.src = (is_ssl ? 'https:' : 'http:') + '//global.proper.io/tvtropes.min.js';
				 var node = document.getElementsByTagName('script')[0];
				 node.parentNode.insertBefore(pm, node);
				})();
				</script>


						

		    			<link rel="stylesheet" href="https://static.tvtropes.org/tropecons/style.css">
			<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
			<link rel="stylesheet" href="https://static.tvtropes.org/design/css/icomoon.css?rev=182" >
						<link rel="stylesheet" href="https://static.tvtropes.org/design/css/main.css?rev=182" />

			
						
						
						
				    	

						<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> -->

			<!--[if IE]>
        		<script>
            		(function(){if(!/*@cc_on!@*/0)return;var e = "abbr,article,aside,audio,bb,canvas,datagrid,datalist,details,dialog,eventsource,figure,footer,header,hgroup,mark,menu,meter,nav,output,progress,section,time,video".split(','),i=e.length;while(i--){document.createElement(e[i])}})();
        		</script>
    		<![endif]-->


			<script type="text/javascript" src="https://static.tvtropes.org/design/js/jquery-3.2.1.min.js"></script>
						<script type="text/javascript" src="https://static.tvtropes.org/design/js/ads.js?rev=1535887165" ></script>
			<script type="text/javascript" src="https://static.tvtropes.org/design/js/stickykit-min.js" ></script>

							<script type="text/javascript" src="https://static.tvtropes.org/design/js/main.min.js?rev=182" ></script>
			
			
	    				
						
						

			
						
				<!--   Google Contribute AB code  -->
                <script src="https://contributor.google.com/scripts/49180a037d37f789/loader.js"></script>
                <script>(function () {
                        'use strict';
                        var g = "function" == typeof Object.defineProperties ? Object.defineProperty : function (a, b, c) {
                                a != Array.prototype && a != Object.prototype && (a[b] = c.value)
                            },
                            k = "undefined" != typeof window && window === this ? this : "undefined" != typeof global && null != global ? global : this,
                            aa = function (a) {
                                if (a) {
                                    for (var b = k, c = ["Array", "from"], d = 0; d < c.length - 1; d++) {
                                        var e = c[d];
                                        e in b || (b[e] = {});
                                        b = b[e]
                                    }
                                    c = c[c.length - 1];
                                    d = b[c];
                                    a = a(d);
                                    a != d && null != a && g(b, c, {configurable: !0, writable: !0, value: a})
                                }
                            }, l = function () {
                                l = function () {
                                };
                                k.Symbol || (k.Symbol =
                                    ba)
                            }, ba = function () {
                                var a = 0;
                                return function (b) {
                                    return "jscomp_symbol_" + (b || "") + a++
                                }
                            }(), n = function () {
                                l();
                                var a = k.Symbol.iterator;
                                a || (a = k.Symbol.iterator = k.Symbol("iterator"));
                                "function" != typeof Array.prototype[a] && g(Array.prototype, a, {
                                    configurable: !0,
                                    writable: !0,
                                    value: function () {
                                        return ca(this)
                                    }
                                });
                                n = function () {
                                }
                            }, ca = function (a) {
                                var b = 0;
                                return da(function () {
                                    return b < a.length ? {done: !1, value: a[b++]} : {done: !0}
                                })
                            }, da = function (a) {
                                n();
                                a = {next: a};
                                a[k.Symbol.iterator] = function () {
                                    return this
                                };
                                return a
                            };
                        aa(function (a) {
                            return a ? a : function (a, c, d) {
                                n();
                                c = null != c ? c : function (a) {
                                    return a
                                };
                                var b = [], f = a[Symbol.iterator];
                                if ("function" == typeof f) for (a = f.call(a); !(f = a.next()).done;) b.push(c.call(d, f.value)); else {
                                    f = a.length;
                                    for (var h = 0; h < f; h++) b.push(c.call(d, a[h]))
                                }
                                return b
                            }
                        });
                        var p = this, ea = /^[\w+/_-]+[=]{0,2}$/, q = null, r = function (a) {
                            var b = typeof a;
                            if ("object" == b) if (a) {
                                if (a instanceof Array) return "array";
                                if (a instanceof Object) return b;
                                var c = Object.prototype.toString.call(a);
                                if ("[object Window]" == c) return "object";
                                if ("[object Array]" == c || "number" == typeof a.length && "undefined" != typeof a.splice && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("splice")) return "array";
                                if ("[object Function]" == c || "undefined" != typeof a.call && "undefined" != typeof a.propertyIsEnumerable &&
                                    !a.propertyIsEnumerable("call")) return "function"
                            } else return "null"; else if ("function" == b && "undefined" == typeof a.call) return "object";
                            return b
                        }, fa = Date.now || function () {
                            return +new Date
                        }, t = function (a, b) {
                            function c() {
                            }

                            c.prototype = b.prototype;
                            a.o = b.prototype;
                            a.prototype = new c;
                            a.prototype.constructor = a;
                            a.j = function (a, c, f) {
                                for (var d = Array(arguments.length - 2), e = 2; e < arguments.length; e++) d[e - 2] = arguments[e];
                                return b.prototype[c].apply(a, d)
                            }
                        };
                        var ha = String.prototype.trim ? function (a) {
                            return a.trim()
                        } : function (a) {
                            return /^[\s\xa0]*([\s\S]*?)[\s\xa0]*$/.exec(a)[1]
                        };
                        var u;
                        a:{
                            var v = p.navigator;
                            if (v) {
                                var w = v.userAgent;
                                if (w) {
                                    u = w;
                                    break a
                                }
                            }
                            u = ""
                        }
                        var x = function (a) {
                            return -1 != u.indexOf(a)
                        };
                        var ia = x("Opera"), ja = x("Trident") || x("MSIE"),
                            ka = x("Gecko") && !(-1 != u.toLowerCase().indexOf("webkit") && !x("Edge")) && !(x("Trident") || x("MSIE")) && !x("Edge"),
                            la = -1 != u.toLowerCase().indexOf("webkit") && !x("Edge");
                        var A = function () {
                            this.a = "";
                            this.c = z
                        }, ma = function (a) {
                            var b = B(7);
                            b = C(b);
                            b = D(b);
                            var c = /\?/.test(b) ? "&" : "?", d;
                            for (d in a) for (var e = "array" == r(a[d]) ? a[d] : [a[d]], f = 0; f < e.length; f++) null != e[f] && (b += c + encodeURIComponent(d) + "=" + encodeURIComponent(String(e[f])), c = "&");
                            return E(b)
                        }, D = function (a) {
                            return a instanceof A && a.constructor === A && a.c === z ? a.a : "type_error:TrustedResourceUrl"
                        }, z = {}, E = function (a) {
                            var b = new A;
                            b.a = a;
                            return b
                        };
                        var na = function (a, b) {
                            a.src = D(b);
                            if (null === q) {
                                a:{
                                    if ((b = p.document.querySelector("script[nonce]")) && (b = b.nonce || b.getAttribute("nonce")) && ea.test(b)) break a;
                                    b = null
                                }
                                q = b || ""
                            }
                            if (b = q) a.nonce = b
                        };
                        var F = function (a) {
                            this.a = a || p.document || document
                        };
                        var G = function (a, b, c, d, e) {
                            try {
                                var f = a.a, h = a.a.createElement("SCRIPT");
                                h.async = !0;
                                na(h, b);
                                f.head.appendChild(h);
                                h.addEventListener("load", function () {
                                    d();
                                    f.head.removeChild(h)
                                });
                                h.addEventListener("error", function () {
                                    0 < c ? G(a, b, c - 1, d, e) : e();
                                    f.head.removeChild(h)
                                })
                            } catch (m) {
                                e()
                            }
                        };
                        var H = function (a) {
                            this.c = -1;
                            this.a = a
                        }, I = function (a, b, c) {
                            try {
                                var d = b.children;
                                if (5 > c) for (var e = d.length - 1; 0 <= e; --e) I(a, d[e], c + 1);
                                0 != c && (b.hidden = !0, b.parentElement.removeChild(b))
                            } catch (f) {
                            }
                        }, oa = function (a, b, c, d, e) {
                            /Mobi/i.test(u) || J(a.a, c) || G(new F(a.a), d, 2, function () {
                                p[e] || K(a, b, c)
                            }, function () {
                                return K(a, b, c)
                            })
                        }, K = function (a, b, c) {
                            if (!J(a.a, c)) {
                                var d = new F(a.a), e = d.a.body;
                                if (e) {
                                    I(a, e, 0);
                                    Array.from(e.attributes).forEach(function (a) {
                                        e.removeAttributeNode(a)
                                    });
                                    var f = d.a.createElement("IFRAME");
                                    f.src =
                                        D(c);
                                    f.style.width = "100%";
                                    f.style.height = "100%";
                                    f.style.background = "white";
                                    f.style.border = "none";
                                    f.style.zIndex = 2147483647;
                                    e.appendChild(f);
                                    d = d.a.createElement("STYLE");
                                    d.textContent = "body, html { margin: 0; padding: 0; height: 100%; overflow: hidden; }";
                                    a.a.head.appendChild(d)
                                } else a.c = p.setTimeout(function () {
                                    return K(a, b, c)
                                }, 0)
                            }
                        }, J = function (a, b) {
                            a = a.getElementsByTagName("IFRAME");
                            return a.length ? (a = a[0], b = D(b), !(!a.src || a.src != b)) : !1
                        };
                        H.prototype.cancel = function () {
                            p.clearTimeout(this.c)
                        };
                        var L = x("Safari") && !((x("Chrome") || x("CriOS")) && !x("Edge") || x("Coast") || x("Opera") || x("Edge") || x("Silk") || x("Android")) && !(x("iPhone") && !x("iPod") && !x("iPad") || x("iPad") || x("iPod"));
                        var M = null, N = null, O = null, pa = ka || la && !L || ia,
                            qa = pa || "function" == typeof p.btoa, ra = pa || !L && !ja && "function" == typeof p.atob,
                            ta = function (a, b) {
                                sa();
                                b = b ? O : M;
                                for (var c = [], d = 0; d < a.length; d += 3) {
                                    var e = a[d], f = d + 1 < a.length, h = f ? a[d + 1] : 0, m = d + 2 < a.length,
                                        y = m ? a[d + 2] : 0, Da = e >> 2;
                                    e = (e & 3) << 4 | h >> 4;
                                    h = (h & 15) << 2 | y >> 6;
                                    y &= 63;
                                    m || (y = 64, f || (h = 64));
                                    c.push(b[Da], b[e], b[h], b[y])
                                }
                                return c.join("")
                            }, P = function (a) {
                                if (qa) a = p.btoa(a); else {
                                    for (var b = [], c = 0, d = 0; d < a.length; d++) {
                                        var e = a.charCodeAt(d);
                                        255 < e && (b[c++] = e & 255, e >>= 8);
                                        b[c++] = e
                                    }
                                    a = ta(b,
                                        void 0)
                                }
                                return a
                            }, va = function (a) {
                                if (ra) return p.atob(a);
                                var b = "";
                                ua(a, function (a) {
                                    b += String.fromCharCode(a)
                                });
                                return b
                            }, ua = function (a, b) {
                                function c(b) {
                                    for (; d < a.length;) {
                                        var c = a.charAt(d++), e = N[c];
                                        if (null != e) return e;
                                        if (!/^[\s\xa0]*$/.test(c)) throw Error("Unknown base64 encoding at char: " + c);
                                    }
                                    return b
                                }

                                sa();
                                for (var d = 0; ;) {
                                    var e = c(-1), f = c(0), h = c(64), m = c(64);
                                    if (64 === m && -1 === e) break;
                                    b(e << 2 | f >> 4);
                                    64 != h && (b(f << 4 & 240 | h >> 2), 64 != m && b(h << 6 & 192 | m))
                                }
                            }, sa = function () {
                                if (!M) {
                                    M = {};
                                    N = {};
                                    O = {};
                                    for (var a = 0; 65 > a; a++) M[a] =
                                        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(a), N[M[a]] = a, O[a] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.".charAt(a), 62 <= a && (N["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.".charAt(a)] = a)
                                }
                            };
                        var Q = function () {
                        }, wa = "function" == typeof Uint8Array, R = function (a, b) {
                            a.f = null;
                            b || (b = []);
                            a.m = void 0;
                            a.g = -1;
                            a.a = b;
                            a:{
                                if (a.a.length) {
                                    b = a.a.length - 1;
                                    var c = a.a[b];
                                    if (c && "object" == typeof c && "array" != r(c) && !(wa && c instanceof Uint8Array)) {
                                        a.h = b - a.g;
                                        a.c = c;
                                        break a
                                    }
                                }
                                a.h = Number.MAX_VALUE
                            }
                            a.l = {}
                        }, xa = [], S = function (a, b) {
                            if (b < a.h) {
                                b += a.g;
                                var c = a.a[b];
                                return c === xa ? a.a[b] = [] : c
                            }
                            if (a.c) return c = a.c[b], c === xa ? a.c[b] = [] : c
                        }, ya = function (a, b, c) {
                            if (b < a.h) a.a[b + a.g] = c; else {
                                var d = a.h + a.g;
                                a.a[d] || (a.c = a.a[d] = {});
                                a.c[b] = c
                            }
                        }, B =
                            function (a) {
                                var b = T, c = za;
                                b.f || (b.f = {});
                                if (!b.f[a]) {
                                    var d = S(b, a);
                                    d && (b.f[a] = new c(d))
                                }
                                return b.f[a]
                            };
                        Q.prototype.i = wa ? function () {
                            var a = Uint8Array.prototype.toJSON;
                            Uint8Array.prototype.toJSON = function () {
                                return ta(this)
                            };
                            try {
                                return JSON.stringify(this.a && this.a, Aa)
                            } finally {
                                Uint8Array.prototype.toJSON = a
                            }
                        } : function () {
                            return JSON.stringify(this.a && this.a, Aa)
                        };
                        var Aa = function (a, b) {
                            return "number" != typeof b || !isNaN(b) && Infinity !== b && -Infinity !== b ? b : String(b)
                        }, Ca = function (a) {
                            return new Ba(a ? JSON.parse(a) : null)
                        };
                        Q.prototype.toString = function () {
                            return this.a.toString()
                        };
                        var Ea = function (a) {
                            R(this, a)
                        };
                        t(Ea, Q);
                        var Fa = function (a, b) {
                            this.a = a;
                            this.c = b
                        };
                        Fa.prototype.challenge = function () {
                            var a = "a" + P(String(this.a)), b = P(String(this.a ^ this.c));
                            return "function" == r(p[a]) ? p[a]() == b : !1
                        };
                        var Ga = function () {
                            var a = U, b = new Ea;
                            ya(b, 1, a.a);
                            ya(b, 2, a.c);
                            a = P(b.i());
                            return P(a)
                        }, Ha = function () {
                            var a = S(T, 2) || -1;
                            return new Fa(1E3 * a * Math.random() | 0, 1E3 * a * Math.random() | 0)
                        };
                        var V = function (a) {
                            this.a = a || {cookie: ""}
                        };
                        V.prototype.set = function (a, b, c, d, e, f) {
                            if (/[;=\s]/.test(a)) throw Error('Invalid cookie name "' + a + '"');
                            if (/[;\r\n]/.test(b)) throw Error('Invalid cookie value "' + b + '"');
                            void 0 !== c || (c = -1);
                            e = e ? ";domain=" + e : "";
                            d = d ? ";path=" + d : "";
                            f = f ? ";secure" : "";
                            c = 0 > c ? "" : 0 == c ? ";expires=" + (new Date(1970, 1, 1)).toUTCString() : ";expires=" + (new Date(fa() + 1E3 * c)).toUTCString();
                            this.a.cookie = a + "=" + b + e + d + c + f
                        };
                        V.prototype.get = function (a, b) {
                            for (var c = a + "=", d = (this.a.cookie || "").split(";"), e = 0, f; e < d.length; e++) {
                                f = ha(d[e]);
                                if (0 == f.lastIndexOf(c, 0)) return f.substr(c.length);
                                if (f == a) return ""
                            }
                            return b
                        };
                        var za = function (a) {
                            R(this, a)
                        };
                        t(za, Q);
                        var Ba = function (a) {
                            R(this, a)
                        };
                        t(Ba, Q);
                        var C = function (a) {
                            a = S(a, 4) || "";
                            return E(a)
                        };
                        var W = !1, Ia, X, T, U, Y, Z = function (a) {
                            if (null != B(6)) {
                                var b = X.a;
                                var c = C(B(6)), d = C(B(10)), e = S(T, 5);
                                b = new H(b);
                                oa(b, a, c, d, e);
                                Y = b
                            }
                        }, Ja = function () {
                            G(X, Ia, S(T, 4) || 0, function () {
                                U.challenge() ? W = !0 : Z(0)
                            }, function () {
                                Z(1)
                            })
                        };
                        (function (a, b) {
                            a = void 0 === a ? p.document : a;
                            try {
                                T = Ca(va(void 0 === b ? "WyI0OTE4MGEwMzdkMzdmNzg5IiwtODk3NDY3MCwyLG51bGwsIk5Ea3hPREJoTURNM1pETTNaamM0T1FcdTAwM2RcdTAwM2QiLFtudWxsLG51bGwsbnVsbCwiaHR0cHM6Ly9wLncuMGVtbS5jb20vcy9lcnI/dFx1MDAzZEFBZ3VWOHktMVR4WnJFWU56WHlHa29xSndCNGtqS1R6UWRhMFp3MlJhQVZXZEY2dWJtR1laTUdFMDlQRjN3TFY0ZTJoMnhXZXR6UGNrYnBfTWVFX2xjUWJQMWNuTjVFUHB3XHUwMDNkXHUwMDNkIl0KLFtudWxsLG51bGwsbnVsbCwiaHR0cHM6Ly82enk5eXFlMWV3LmNvbS9mL0FHU0tXeFhuaDREaVoxa1V4cW9MTTI5dTl5YWVhU21MNjhUWGdUM0F6aEtKMHlEV091VkVpSjNBTmRLMm9ydzQzN3JNZk5mZTF4c200Y182eFZiNyJdCiwxMDAwMCwibjJpMm1kIixbbnVsbCxudWxsLG51bGwsImh0dHBzOi8vd3d3LmdzdGF0aWMuY29tLzBlbW4vZi9wLzQ5MTgwYTAzN2QzN2Y3ODkuanM/dXNxcFx1MDAzZENCSSJdCl0K" : b));
                                X = new F(a);
                                U = Ha();
                                b = {};
                                b.x = Ga();
                                var c = S(T, 9);
                                if (c) {
                                    var d = (new V(a)).get(c, "");
                                    d && (b.b = d)
                                }
                                Ia = ma(b);
                                Ja();
                                p.setTimeout(function () {
                                    W ? Y && Y.cancel() : Z(3)
                                }, S(T, 8))
                            } catch (e) {
                                W ? Y && Y.cancel() : Z(2)
                            }
                        })();
                    }).call(this);
                </script>
			
		</head>
 <body class="">

        <i id="user-prefs"></i>
    <script>updateUserPrefs();</script>

        
    <div id="fb-root"></div>

    <div id="modal-box"></div>

    <!--   Google Contribute AB message  -->
    <div id="m2vmzt"></div>

    <script type="text/javascript" src="https://static.tvtropes.org/design/js/facebook-sdk.js"></script>
    <header id="main-header-bar" class="headroom-element ">
        <div id="main-header-bar-inner">

            <span id="header-spacer-left" class="header-spacer"></span>

            <a href="#mobile-menu" id="main-mobile-toggle" class="mobile-menu-toggle-button tablet-on"><span></span><span></span><span></span></a>

            <a href="/" id="main-header-logoButton" class="no-dev"></a>

            <span id="header-spacer-right" class="header-spacer"></span>

            <nav id="main-header-nav" class="tablet-off">
                <a href="/pmwiki/pmwiki.php/Main/Tropes">Tropes</a>
                <a href="/pmwiki/pmwiki.php/Main/Media">Media</a>
                <a href="/pmwiki/browse.php" class="nav-browse">Browse</a>
                <a href="/pmwiki/index_report.php">Indexes</a>
                <a href="/pmwiki/topics.php">Forums</a>
            </nav>

            <div id="main-header-bar-right">

                                <div id="signup-login-box" class="font-xs mobile-off">
                    <a href="/pmwiki/login.php" class="hover-underline bold" data-modal-target="signup">Join</a>
                    <a href="/pmwiki/login.php" class="hover-underline bold" data-modal-target="login">Login</a>
                </div>
                
                                <div id="signup-login-mobileToggle" class="mobile-on inline">
                    <a href="/pmwiki/login.php" data-modal-target="login"><i class="fa fa-user"></i></a>
                </div>
                
                <div id="search-mobileToggle" class="mobile-on inline">
                    <a href="#search-toggle" class="mobile-search-toggle"><i class="fa fa-search"></i></a>
                </div>

                <div id="search-box">
                    <form class="search" action="/pmwiki/search_result.php">
                        <input type="text" name="q" class="search-box" placeholder="Search" value="" required>
                        <input type="submit" class="submit-button" value="&#xf002;" />
                                                <input type="hidden" name="search_type" value="article">
                        <input type="hidden" name="page_type" value="all">
                                                <input type="hidden" name="cx" value="partner-pub-6610802604051523:amzitfn8e7v">
                        <input type="hidden" name="cof" value="FORID:10">
                        <input type="hidden" name="ie" value="ISO-8859-1">
                        <input name="siteurl" type="hidden" value="">
                        <input name="ref" type="hidden" value="">
                        <input name="ss" type="hidden" value="">
                    </form>
                    <a href="#close-search" class="mobile-on mobile-search-toggle close-x"><i class="fa fa-close"></i></a>
                </div>

                <div id="random-box">
                    <a href="/pmwiki/pmwiki.php/Main/BraidsOfAction" class="button-random-trope" rel="nofollow" onclick="ga('send', 'event', 'button', 'click', 'random trope');"></a>
                    <a href="/pmwiki/pmwiki.php/Manga/TenYoriMoHoshiYoriMo" class="button-random-media" rel="nofollow" onclick="ga('send', 'event', 'button', 'click', 'random media');"></a>
                </div>

            </div>

        </div>

        <div id="mobile-menu" class="tablet-on"><div class="mobile-menu-options">

    <div class="nav-wrapper">
        <a href="/pmwiki/pmwiki.php/Main/Tropes" class="xl">Tropes</a>
        <a href="/pmwiki/pmwiki.php/Main/Media" class="xl">Media</a>
        <a href="/pmwiki/browse.php" class="xl">Browse</a>
        <a href="/pmwiki/index_report.php" class="xl">Indexes</a>
        <a href="/pmwiki/topics.php" class="xl">Forums</a>

        <a href="/pmwiki/query.php?type=att">Ask The Tropers</a>
        <a href="/pmwiki/query.php?type=tf">Trope Finder</a>
        <a href="/pmwiki/query.php?type=ykts">You Know That Show...</a>
        <a href="/pmwiki/tlp_activity.php">Trope Launch Pad</a>
        
        <a href="#tools" data-click-toggle="active">Tools <i class="fa fa-chevron-down"></i></a>
        <div class="tools-dropdown mobile-dropdown-linkList">
            <a href="/pmwiki/cutlist.php">Cut List</a>
            <a href="/pmwiki/changes.php">New Edits</a>
            <a href="/pmwiki/recent_edit_reasons.php">Edit Reasons</a>
            <a href="/pmwiki/launches.php">Launches</a>
            <a href="/pmwiki/img_list.php">Images List</a>
            <a href="/pmwiki/crown_activity.php">Crowner Activity</a>
            <a href="/pmwiki/no_types.php">Un-typed Pages</a>
            <a href="/pmwiki/page_type_audit.php">Recent Page Type Changes</a>
        </div>

        <a href="#hq" data-click-toggle="active">Tropes HQ <i class="fa fa-chevron-down"></i></a>

        <div class="tools-dropdown mobile-dropdown-linkList">
            <a href="/pmwiki/about.php">About Us</a>
            <a href="/pmwiki/contact.php">Contact Us</a>
            <a href="mailto:advertising@proper.io">Advertise</a>
        </div>

        <div class="toggle-switches">

            <ul class="mobile-menu display-toggles">
                <li>Show Spoilers <div id="mobile-toggle-showspoilers" class="display-toggle show-spoilers"></div></li>
                <li>Night Vision <div id="mobile-toggle-nightvision" class="display-toggle night-vision"></div></li>
                <li>Sticky Header <div id="mobile-toggle-stickyheader" class="display-toggle sticky-header"></div></li>
                <li>Highlight Links <div id="mobile-toggle-highlightlinks" class="display-toggle highlight-links"></div></li>
            </ul>
            <script>updateMobilePrefs();</script>

        </div>

    </div>

</div>
</div>

    </header>

    <div id="header-ad-wrapper" class="ad">
    <div id="header-ad">
    
        <div class="ad-size-970x90 atf_banner">
             
<div class='proper-ad-unit '>
  <div id='proper-ad-tvtropes_main_1'> <script>propertag.cmd.push(function() { proper_display('tvtropes_main_1'); });</script> </div>
</div>        </div>

        </div>
</div>

<div id="main-container">

    
    <div id="action-bar-top" class="action-bar mobile-off">

        <div class="action-bar-right">
            <p>Follow TV Tropes</p>
            <a href="https://www.facebook.com/TVTropes" class="button-fb">
                <i class="fa fa-facebook"></i></a>
            <a href="https://www.twitter.com/TVTropes" class="button-tw">
                <i class="fa fa-twitter"></i></a>
            <a href="https://www.reddit.com/r/TVTropes" class="button-re">
                <i class="fa fa-reddit-alien"></i></a>
        </div>
        
                <nav class="actions-wrapper" itemscope itemtype="http://schema.org/SiteNavigationElement" style="opacity: 0">
        <ul class="page-actions ss-delay">

        <li class="link-about"><a href="/pmwiki/about.php">
        <i class="fa fa-info-circle"></i> About</a></li>
    <li class="link-about"><a href="/pmwiki/policy.php">
        <i class="fa fa-pied-piper-pp"></i> Privacy Policy</a></li>
    <li class="link-about"><a href="/pmwiki/dmca.php">
        <i class="fa fa-legal"></i> DMCA Notice</a></li>
    <li class="link-about"><a href="/pmwiki/contact.php">
        <i class="fa fa-envelope"></i> Contact</a></li>

    
    </ul>
        </nav>
        
        <div class="WikiWordModalStub"></div>
        <div class="ImgUploadModalStub" data-page-type="PrivacyPolicy"></div>
        
        <div class="login-alert" style="display: none;">
            You need to <a href="/pmwiki/login.php" style="color:#21A0E8">login</a> to do this. <a href="/pmwiki/login.php?tab=register_account" style="color:#21A0E8">Get Known</a> if you don't have an account
        </div>
        
    </div>
    
    <div id="main-content" class="page-PrivacyPolicy ">

        
        
        <div id="main-entry" class="with-sidebar">

        
<h1 itemprop="headline" class="entry-title">Privacy Policy</h1>

<a href="#mobile-actions-toggle" id="mobile-actionbar-toggle" class="mobile-actionbar-toggle mobile-on" data-click-toggle="active" >
<p class="tiny-off">Go To</p><span></span><span></span><span></span><i class="fa fa-pencil"></i></a>
<nav id="mobile-actions-bar" class="mobile-actions-wrapper mobile-on"></nav>
<div id="main-article" class="article-content" itemprop="mainContentOfPage">

    <p>Thank you for visiting TVTropes. This privacy policy tells you how we use personal 
        information collected at this wiki. Please read this privacy 
        policy before using the wiki or submitting any personal information. 
        By using the wiki, you are accepting the practices described 
        in this privacy policy. These practices may be changed, but 
        any changes will be posted and changes will only apply to 
        activities and information on a going forward, not retroactive 
        basis. You are encouraged to review the privacy policy whenever 
        you visit the wiki to make sure that you understand how any 
        personal information you provide will be used.
    </p>

    <div class="note">
        <span class="title">Note : </span>the privacy practices set forth in 
        this privacy policy are for this <a href="https://tvtropes.org" class="Content">wiki</a> 
        only. If you link to other wikis, please review the privacy 
        policies posted at those wikis.
    </div>

    <h3 class="section-title">Cookie/Tracking Technology</h3>
    <p>
        The wiki uses cookie and tracking technology.
        Cookie and tracking technology are 
        useful for gathering information such as browser type and 
        operating system, tracking the number of visitors to the wiki, 
        and understanding how visitors use the wiki. Cookies can also 
        help customize the wiki for visitors. Personal information 
        cannot be collected via cookies and other tracking technology, 
        however, if you previously provided personally identifiable 
        information, cookies may be tied to such information. Aggregate 
        cookie and tracking information may be shared with third parties.
    </p>

    <h3 class="section-title">What is the DoubleClick DART cookie?</h3>
                            
    <p>
        The <a href="http://www.doubleclick.com/privacy/faq.aspx">DoubleClick DART cookie</a> is used by Google in the ads served on the websites of its partners, such as websites displaying AdSense ads or participating in Google certified ad networks. When users visit a partner's website and either view or click on an ad, a cookie may be dropped on that end user's browser. The data gathered from these cookies will be used to help better serve and manage ads on the publisher's site(s) and across the web.</p>
         <ul>
            <li>Third party vendors, including Google, use cookies to serve ads based on a user's prior visits to the wiki.</li>
            <li>Google's use of the DART cookie enables it and its partners to serve ads based on visits to the wiki and/or other sites on the Internet.</li>
            <li>Users may opt out of the use of the DART cookie by visiting the <a href="http://www.google.com/privacy_ads.html">advertising opt-out page</a>.</li> 
        </ul>
    </p>

    <h3 class="section-title">Distribution of Information</h3>
    <p>
        We may share information with governmental agencies or other 
        companies assisting us in fraud prevention or investigation. 
        We may do so when: (1) permitted or required by law; or, (2) 
        trying to protect against or prevent actual or potential fraud 
        or unauthorized transactions; or, (3) investigating fraud 
        which has already taken place. 
    </p>

    <h3 class="section-title">Commitment to Data Security</h3>
    <p>
        Your personally identifiable information is kept secure. Only 
        authorized employees, agents and contractors (who have agreed 
        to keep information secure and confidential) have access to 
        this information. All emails and newsletters from this wiki 
        allow you to opt out of further mailings. 
    </p>

    <h3 class="section-title">Privacy Contact Information</h3>
    <p>
        If you have any questions, concerns, or comments about our 
        privacy policy you may contact us using the information below:
        <br>
        By e-mail: dr at tvtropes.org
        <br>
      We reserve the right to make changes to this policy. Any changes to this policy will be posted.
    </p>
       
</div>
        
        </div>

                
                        <div id="main-content-sidebar">
<div class="sidebar-item quick-links" itemtype="http://schema.org/SiteNavigationElement">

    <a href="/pmwiki/query.php?type=att">Ask The Tropers</a>
    <a href="/pmwiki/query.php?type=tf">Trope Finder</a>
    <a href="/pmwiki/query.php?type=ykts">You Know That Show...</a>
    <a href="/pmwiki/tlp_activity.php">Trope Launch Pad</a>
    <a href="/pmwiki/review_activity.php">Reviews</a>
    <a href="/pmwiki/lbs.php">Live Blogs</a>

</div>



<div class="sidebar-item display-options">
    <p class="sidebar-item-title" data-title="Display">Display</p>
    <ul class="sidebar display-toggles">
        <li>Show Spoilers <div id="sidebar-toggle-showspoilers" class="display-toggle show-spoilers"></div></li>
        <li>Night Vision <div id="sidebar-toggle-nightvision" class="display-toggle night-vision"></div></li>
        <li>Sticky Header <div id="sidebar-toggle-stickyheader" class="display-toggle sticky-header"></div></li>
        <li>Wide Load <div id="sidebar-toggle-wideload" class="display-toggle wide-load"></div></li>
    </ul>
    <script>updateDesktopPrefs();</script>
</div>



<div class="sidebar-item ad sb-ad-unit">
    
<div class='proper-ad-unit '>
  <div id='proper-ad-tvtropes_side_1'> <script>propertag.cmd.push(function() { proper_display('tvtropes_side_1'); });</script> </div>
</div></div>


















<div class="sidebar-item">

    <p class="sidebar-item-title" data-title="Crucial Browsing">Crucial Browsing</p>

    <ul class="padded font-s" itemscope itemtype="http://schema.org/SiteNavigationElement">

        <li><a href="javascript:void(0);" data-click-toggle="active">Genre</a>
            <ul>
                <li><a href='/pmwiki/pmwiki.php/Main/ActionAdventureTropes' title='Main/ActionAdventureTropes'>Action Adventure</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/ComedyTropes' title='Main/ComedyTropes'>Comedy</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/CommercialsTropes' title='Main/CommercialsTropes'>Commercials</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/CrimeAndPunishmentTropes' title='Main/CrimeAndPunishmentTropes'>Crime &amp; Punishment</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/DramaTropes' title='Main/DramaTropes'>Drama</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/HorrorTropes' title='Main/HorrorTropes'>Horror</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/LoveTropes' title='Main/LoveTropes'>Love</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/NewsTropes' title='Main/NewsTropes'>News</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/ProfessionalWrestling' title='Main/ProfessionalWrestling'>Professional Wrestling</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/SpeculativeFictionTropes' title='Main/SpeculativeFictionTropes'>Speculative Fiction</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/SportsStoryTropes' title='Main/SportsStoryTropes'>Sports Story</a></li>
                <li><a href='/pmwiki/pmwiki.php/Main/WarTropes' title='Main/WarTropes'>War</a></li>
            </ul>
        </li>

        <li><a href="javascript:void(0);" data-click-toggle="active">Media</a>
            <ul>
                <li><a href="/pmwiki/pmwiki.php/Main/Media" title="Main/Media">All Media</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/AnimationTropes" title="Main/AnimationTropes">Animation (Western)</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Anime" title="Main/Anime">Anime</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/ComicBookTropes" title="Main/ComicBookTropes">Comic Book</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/FanFic" title="FanFic/FanFics">Fan Fics</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Film" title="Main/Film">Film</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/GameTropes" title="Main/GameTropes">Game</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Literature" title="Main/Literature">Literature</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/MusicAndSoundEffects" title="Main/MusicAndSoundEffects">Music And Sound Effects</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/NewMediaTropes" title="Main/NewMediaTropes">New Media</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/PrintMediaTropes" title="Main/PrintMediaTropes">Print Media</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Radio" title="Main/Radio">Radio</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/SequentialArt" title="Main/SequentialArt">Sequential Art</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/TabletopGames" title="Main/TabletopGames">Tabletop Games</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Television" title="Main/Television">Television</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Theater" title="Main/Theater">Theater</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/VideogameTropes" title="Main/VideogameTropes">Videogame</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Webcomics" title="Main/Webcomics">Webcomics</a></li>
            </ul>
        </li>

        <li><a href="javascript:void(0);" data-click-toggle="active">Narrative</a>
            <ul>
                <li><a href="/pmwiki/pmwiki.php/Main/UniversalTropes" title="Main/UniversalTropes">Universal</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/AppliedPhlebotinum" title="Main/AppliedPhlebotinum">Applied Phlebotinum</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/CharacterizationTropes" title="Main/CharacterizationTropes">Characterization</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Characters" title="Main/Characters">Characters</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/CharactersAsDevice" title="Main/CharactersAsDevice">Characters As Device</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Dialogue" title="Main/Dialogue">Dialogue</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Motifs" title="Main/Motifs">Motifs</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/NarrativeDevices" title="Main/NarrativeDevices">Narrative Devices</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Paratext" title="Main/Paratext">Paratext</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Plots" title="Main/Plots">Plots</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Settings" title="Main/Settings">Settings</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Spectacle" title="Main/Spectacle">Spectacle</a></li>
            </ul>
        </li>

        <li><a href="javascript:void(0);" data-click-toggle="active">Other Categories</a>
            <ul>
                <li><a href="/pmwiki/pmwiki.php/Main/BritishTellyTropes" title="Main/BritishTellyTropes">British Telly</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/TheContributors" title="Main/TheContributors">The Contributors</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/CreatorSpeak" title="Main/CreatorSpeak">Creator Speak</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Creators" title="Main/Creators">Creators</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/DerivativeWorks" title="Main/DerivativeWorks">Derivative Works</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/LanguageTropes" title="Main/LanguageTropes">Language</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/LawsAndFormulas" title="Main/LawsAndFormulas">Laws And Formulas</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/ShowBusiness" title="Main/ShowBusiness">Show Business</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/SplitPersonalityTropes" title="Main/SplitPersonalityTropes">Split Personality</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/StockRoom" title="Main/StockRoom">Stock Room</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/TropeTropes" title="Main/TropeTropes">Trope</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/Tropes" title="Main/Tropes">Tropes</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/TruthAndLies" title="Main/TruthAndLies">Truth And Lies</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/TruthInTelevision" title="Main/TruthInTelevision">Truth In Television</a></li>
            </ul>
        </li>

        <li><a href="javascript:void(0);" data-click-toggle="active">Topical Tropes</a>
            <ul>
                <li><a href="/pmwiki/pmwiki.php/Main/BetrayalTropes" title="Main/BetrayalTropes">Betrayal</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/CensorshipTropes" title="Main/CensorshipTropes">Censorship</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/CombatTropes" title="Main/CombatTropes">Combat</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/DeathTropes" title="Main/DeathTropes">Death</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/FamilyTropes" title="Main/FamilyTropes">Family</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/FateAndProphecyTropes" title="Main/FateAndProphecyTropes">Fate And Prophecy</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/FoodTropes" title="Main/FoodTropes">Food</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/HolidayTropes" title="Main/HolidayTropes">Holiday</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/MemoryTropes" title="Main/MemoryTropes">Memory</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/MoneyTropes" title="Main/MoneyTropes">Money</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/MoralityTropes" title="Main/MoralityTropes">Morality</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/PoliticsTropes" title="Main/PoliticsTropes">Politics</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/ReligionTropes" title="Main/ReligionTropes">Religion</a></li>
                <li><a href="/pmwiki/pmwiki.php/Main/SchoolTropes" title="Main/SchoolTropes">School</a></li>
            </ul>
        </li>

    </ul>
</div>




    <div class="sidebar-item sb-ad-unit">
        <div class="sidebar-section">
            <div class="square_ad ad-size-300x600 ad-section text-center">
            
<div class='proper-ad-unit '>
  <div id='proper-ad-tvtropes_side_2'> <script>propertag.cmd.push(function() { proper_display('tvtropes_side_2'); });</script> </div>
</div>            </div>
        </div>
    </div>




<div class="sidebar-item showcase">

    <p class="sidebar-item-title" data-title="Community&nbsp;Showcase">Community Showcase <a href="/pmwiki/showcase.php" class="bubble float-right hover-blue">More</a></p>

    <p class="community-showcase">

        
        <a href="https://sharetv.com/shows/echo_chamber" target="_blank" onclick="trackOutboundLink('https://sharetv.com/shows/echo_chamber');" >
            <img src="https://static.tvtropes.org/images/communityShowcase-echochamber.jpg" alt=""></a>

        <a href="http://www.charactour.com/home" target="_blank" onclick="trackOutboundLink('http://www.charactour.com/home');" >
            <img src="https://static.tvtropes.org/images/communityShowcase-charactour.jpg" alt=""></a>

        <a href="/pmwiki/pmwiki.php/Webcomic/TwistedTropes");">
            <img src="https://static.tvtropes.org/img/howlandsc-side.jpg" alt=""></a>

    </p>

</div>




    <div id="stick-cont"  class="sidebar-item sb-ad-unit">
        <div id="stick-bar" class="sidebar-section">
            <div class="square_ad ad-size-300x600 ad-section text-center">
            
<div class='proper-ad-unit '>
  <div id='proper-ad-tvtropes_side_3'> <script>propertag.cmd.push(function() { proper_display('tvtropes_side_3'); });</script> </div>
</div>            </div>
        </div>
    </div>
</div>
        
    </div>

    <div id="action-bar-bottom" class="action-bar tablet-off">
        <a href="#top-of-page" class="scroll-to-top">Top</a>
    </div>

</div>

<div id="footer-ad-wrapper" class="ad">
    <div id="footer-ad">
        <div class="ad-size-970x90-alt btf_banner">
        
<div class='proper-ad-unit '>
  <div id='proper-ad-tvtropes_main_2'> <script>propertag.cmd.push(function() { proper_display('tvtropes_main_2'); });</script> </div>
</div>        </div>
    </div>
</div>          
<div class='proper-ad-unit ad-sticky'>
  <div id='proper-ad-tvtropes_sticky_1'> <script>propertag.cmd.push(function() { proper_display('tvtropes_sticky_1'); });</script> </div>
</div>
<footer id="main-footer">
    <div id="main-footer-inner">


        <div class="footer-left">

            <a href="/" class="img-link"><img src="https://static.tvtropes.org/img/tvtropes-footer-logo.png" alt="TV Tropes" title="TV Tropes" /></a>

            <form action="index.html" id="cse-search-box-mobile" class="navbar-form newsletter-signup validate modal-replies" name="" role="" data-ajax-get="/ajax/subscribe_email.php">

                <button class="btn-submit newsletter-signup-submit-button" type="submit" id="subscribe-btn"><i class="fa fa-paper-plane"></i></button>
                <input id="subscription-email" type="text" class="form-control" name="q" size="31" placeholder="Subscribe" value="" validate-type="email">

            </form>

            <ul class="social-buttons">
               <li><a class="btn fb" target="_blank" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-facebook']);" href="https://www.facebook.com/tvtropes"><i class="fa fa-facebook"></i></a></li>
               <li><a class="btn tw" target="_blank" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-twitter']);" href="https://www.twitter.com/tvtropes"><i class="fa fa-twitter"></i></a> </li>
                              <li><a class="btn rd" target="_blank" onclick="_gaq.push(['_trackEvent', 'btn-social-icon', 'click', 'btn-reddit']);" href="https://www.reddit.com/r/tvtropes"><i class="fa fa-reddit-alien"></i></a></li>
                           </ul>

        </div>

        <hr/>

        <ul class="footer-menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
            <li><h4 class="footer-menu-header">TVTropes</h4></li>
            <li><a href="/pmwiki/pmwiki.php/Main/Administrivia">About TVTropes</a></li>
            <li><a href="/pmwiki/pmwiki.php/Administrivia/TheGoalsOfTVTropes">TVTropes Goals</a></li>
            <li><a href="/pmwiki/pmwiki.php/Administrivia/TheTropingCode">Troping Code</a></li>
            <li><a href="/pmwiki/pmwiki.php/Administrivia/TVTropesCustoms">TVTropes Customs</a></li>
            <li><a href="/pmwiki/pmwiki.php/JustForFun/TropesOfLegend">Tropes of Legend</a></li>
                    </ul>

        <hr/>

        <ul class="footer-menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
            <li><h4 class="footer-menu-header">Community</h4></li>
            <li><a href="/pmwiki/query.php?type=att">Ask The Tropers</a></li>
            <li><a href="/pmwiki/tlp_activity.php">Trope Launch Pad</a></li>
            <li><a href="/pmwiki/query.php?type=tf">Trope Finder</a></li>
            <li><a href="/pmwiki/query.php?type=ykts">You Know That Show</a></li>
            <li><a href="/pmwiki/lbs.php">Live Blogs</a></li>
            <li><a href="/pmwiki/review_activity.php">Reviews</a></li>
            <li><a href="/pmwiki/topics.php">Forum</a></li>
        </ul>

        <hr/>

        <ul class="footer-menu" itemscope itemtype="http://schema.org/SiteNavigationElement">
            <li><h4 class="footer-menu-header">Tropes HQ</h4></li>
            <li><a href="/pmwiki/about.php">About Us</a></li>
                        <li><a href="/pmwiki/contact.php">Contact Us</a></li>
            <li><a href="/pmwiki/dmca.php">DMCA Notice</a></li>
        </ul>

    </div>

    <div id="desktop-on-mobile-toggle" class="text-center gutter-top gutter-bottom tablet-on">
      <a href="/pmwiki/switchDeviceCss.php?mobileVersion=1" rel="nofollow">Switch to <span class="txt-desktop">Desktop</span><span class="txt-mobile">Mobile</span> Version</a>
    </div>

    <div class="legal">

        <p>TVTropes is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. <br>Permissions beyond the scope of this license may be available from <a xmlns:cc="http://creativecommons.org/ns#" href="mailto:thestaff@tvtropes.org" rel="cc:morePermissions"> thestaff@tvtropes.org</a>. <a href="/pmwiki/privacypolicy.php">Privacy Policy</a></p>

    </div>

</footer>

<script type="text/javascript" src="https://static.tvtropes.org/design/js/priority-nav.min.js"></script>
<script type="text/javascript" src="https://static.tvtropes.org/design/js/head-room.min.js"></script>




<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3821842-1', 'auto');
  ga('send', 'pageview');

</script>


  <script type="text/javascript">
    ga('send', 'event', 'tags', 'unknown', {'nonInteraction': 1});

    if (typeof(valid_user) == "undefined") ga('send', 'event', 'ads', 'load', 'blocked', {'nonInteraction': 1});
    else { 
      ga('send', 'event', 'ads', 'load', 'unblocked', {'nonInteraction': 1}); 
          }
  </script>



</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=750572384991117&amp;ev=PixelInitialized" /></noscript>


<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1020390763;
var google_conversion_label = "VHnFCP6P4FYQ69rH5gM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<div class="bottom-iframe" style="display:none">
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
</div>
<noscript>
<div style="display:none;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1020390763/?value=1.00&amp;currency_code=USD&amp;label=VHnFCP6P4FYQ69rH5gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


<!-- comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "18986967" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="http://b.scorecardresearch.com/p?c1=2&c2=18986967&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->

<!-- Quantcast Tag -->
<script type="text/javascript">
var _qevents = _qevents || [];

(function() {
var elem = document.createElement('script');
elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
elem.async = true;
elem.type = "text/javascript";
var scpt = document.getElementsByTagName('script')[0];
scpt.parentNode.insertBefore(elem, scpt);
})();

_qevents.push({
qacct:"p-rxFAP9KpQMJkj"
});
</script>


<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-rxFAP9KpQMJkj.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<!-- End Quantcast tag -->


</body>
</html>
