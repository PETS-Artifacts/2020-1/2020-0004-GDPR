<!DOCTYPE html><html lang="en-us"><head><meta name="generator" content="React Static" /><title data-react-helmet="true">Privacy Policy</title><meta data-react-helmet="true" http-equiv="Content-Security-Policy" content="default-src &#x27;none&#x27;; style-src &#x27;self&#x27; &#x27;unsafe-inline&#x27;; font-src &#x27;self&#x27; *.zopim.com data:; script-src &#x27;self&#x27; *.google-analytics.com *.googletagmanager.com *.zopim.com &#x27;unsafe-eval&#x27; *.google.com *.google.ca *.gstatic.com *.googleadservices.com googleads.g.doubleclick.net *.bing.com *.redditstatic.com/; img-src &#x27;self&#x27; *.google-analytics.com *.g.doubleclick.net *.zopim.com data: *.zopim.io data: *.google.com *.google.ca *.gstatic.com *.bing.com *.reddit.com; connect-src &#x27;self&#x27; wss://*.zopim.com http://v2.zopim.com; frame-src *.zopim.com *.google.com *.google.ca; media-src *.zopim.com" /><meta data-react-helmet="true" property="og:title" content="Privacy Policy" /><meta data-react-helmet="true" name="description" content="Kraken is the number one rated cryptocurrency exchange for privacy and security. Learn more by reading our privacy policy now." /><meta data-react-helmet="true" property="og:description" content="Kraken is the number one rated cryptocurrency exchange for privacy and security. Learn more by reading our privacy policy now." /><meta data-react-helmet="true" property="og:image" content="https://www.kraken.com/_assets/files/1970-01/kraken-social.jpg" /><link rel="preload" as="script" href="/js2/templates/05f41b83.d3cbc2d1.min.js" /><link rel="preload" as="script" href="/js2/templates/05f41b83.a82cb648.min.js" /><link rel="preload" as="script" href="/js2/templates/05f41b83.de932601.min.js" /><link rel="preload" as="script" href="/js2/templates/05f41b83.65355e27.min.js" /><link rel="preload" as="script" href="/js2/templates/05f41b83.ad3c2804.min.js" /><link rel="preload" as="script" href="/js2/main.05f41b83.min.js" /><link rel="preload" as="style" href="/css2/main.f639ceb8.min.css" /><link rel="stylesheet" href="/css2/main.f639ceb8.min.css" /><link data-react-helmet="true" rel="icon" type="image/x-icon" href="/img/favicon.ico?v=2" /><link data-react-helmet="true" rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico?v=2" /><link data-react-helmet="true" rel="alternate" href="https://www.kraken.com/legal/privacy" hrefLang="x-default" /><link data-react-helmet="true" rel="alternate" href="https://www.kraken.com/en-us/legal/privacy" hrefLang="en-us" /><link data-react-helmet="true" rel="alternate" href="https://www.kraken.com/en-gb/legal/privacy" hrefLang="en-gb" /><link data-react-helmet="true" rel="alternate" href="https://www.kraken.com/es-es/legal/privacy" hrefLang="es-ES" /><link data-react-helmet="true" rel="alternate" href="https://www.kraken.com/zh-cn/legal/privacy" hrefLang="zh-CN" /><link data-react-helmet="true" rel="alternate" href="https://www.kraken.com/ja-jp/legal/privacy" hrefLang="ja-jp" /><link data-react-helmet="true" rel="canonical" href="https://www.kraken.com/legal/privacy" /><script data-react-helmet="true" async="" src="https://www.googletagmanager.com/gtag/js?id=UA-45972273-1"></script><meta charSet="UTF-8" /><meta name="viewport" content="width=device-width, initial-scale=1" /><script src="/js2/routes/routeInfo_752f1f49b82c20010a1894156c0876d1.min.js"></script></head><body><div id="root"><div class="content"> <header class="kraken-header"><a href="/en-us/" class="header-logo" to="/en-us/">Kraken</a><nav class="top-nav"><ul class="primary"><li><a href="/en-us/features" to="/en-us/features">Features</a><ul class="hidden-submenu"><li><div><span class="subnav-header">Getting Started</span><ul><li><a href="/en-us/features/security" to="/en-us/features/security">Security</a></li><li><a href="/en-us/features/liquidity" to="/en-us/features/liquidity">Liquidity</a></li><li><a href="/en-us/features/fee-schedule" to="/en-us/features/fee-schedule">Fee Structure</a></li><li><a href="/en-us/features/24-7-support" to="/en-us/features/24-7-support">24/7 Support</a></li><li><a href="/en-us/features/funding-options" to="/en-us/features/funding-options">Funding Options</a></li></ul></div><div><span class="subnav-header">Advanced</span><ul><li><a href="/en-us/features/margin-trading" to="/en-us/features/margin-trading">Margin Trading</a></li><li><a href="/en-us/features/futures" to="/en-us/features/futures">Futures</a></li><li><a href="/en-us/features/otc-exchange" to="/en-us/features/otc-exchange">OTC</a></li><li><a href="/en-us/features/indices" to="/en-us/features/indices">Indices</a></li><li><a href="/en-us/features/account-management" to="/en-us/features/account-management">Account Management</a></li><li><a href="/en-us/cryptowatch" to="/en-us/cryptowatch">Cryptowatch</a></li></ul></div></li></ul></li><li><a href="/redirect?url=https%3A%2F%2Ftrade.kraken.com%2Fmarkets" target="_blank" rel="noopener" to="https://trade.kraken.com/markets">Prices</a></li><li><a href="/en-us/learn/podcast" to="/en-us/learn/podcast">Podcast</a></li><li><a href="/redirect?url=https%3A%2F%2Fsupport.kraken.com%2Fhc%2Fen-us" target="_blank" rel="noopener" to="https://support.kraken.com/hc/en-us">Support</a></li><li><a href="/en-us/institutions" to="/en-us/institutions">Institutions</a></li></ul><ul class="secondary"><li><a href="/login" class="kraken-cta inverted" location="Top Nav" to="/login">Sign In</a></li><li><a href="/signup" class="kraken-cta" location="Top Nav" to="/signup">Create Account</a></li></ul><span class="header-bk"></span><span class="header-extension"></span></nav></header><article><div class="kraken-components"><div class="kraken-component"><div class="kraken-hero-internal"><div class="kraken-content-wrapper"><div class="copy"><p class="subheader">Legal</p><h1><p><strong>Privacy Policy</strong></p>
</h1></div></div><div class="illustration"></div></div></div><div class="kraken-component"><div class="kraken-article-body"><div class="kraken-content-wrapper"><div class="body"><ol><li>
<h2>Scope</h2>
<p>This Privacy Notice explains what you can expect from us and what we need from you in relation to your personal data. Please read this carefully as this Notice is legally binding when you use our Kraken Exchange Services, as defined below.</p>
<p>This Privacy Notice applies to any Kraken Exchange Services, as defined below, regardless of how you access or use them, including through mobile devices.</p>
<p>We may amend this Privacy Notice at any time by posting the amended version on this site including the effective date of the amended version. We will announce any material changes to this Privacy Notice via email.</p>
</li>
<li>
<h2>Definitions</h2>
<ol><li>
<p>As used herein, the following terms are defined as follows:</p>
<ol><li>
<p>“Digital Asset” is a digital representation of value (also referred to as “cryptocurrency,” “virtual currency,” “digital currency,” “crypto token,” “crypto asset,” or “digital commodity”), such as bitcoin, XRP or ether, which is based on the cryptographic protocol of a computer network that may be (i) centralized or decentralized, (ii) closed or open-source, and (iii) used as a medium of exchange and/or store of value.</p>
</li>
<li>
<p>“Kraken Account” means a user-accessible account offered via the Kraken Exchange Services where Digital Assets are stored by Payward.</p>
</li>
<li>
<p>“Kraken Exchange Services” means Kraken-branded websites, applications, services, or tools operated by Payward group companies.</p>
</li>
<li>
<p>“Payward,” “Kraken,” “We,” and “Us” refers to Payward, Inc. and its wholly owned subsidiaries (also referred to collectively as &quot;Payward,&quot; &quot;Kraken,&quot; &quot;we,&quot; or &quot;us&quot;).</p>
</li>
<li>
<p>“Personal Information” or “Personal Data” or “your data” refers to any information relating to you, as an identified or identifiable natural person, including your name, an identification number, location data, or an online identifier or to one or more factors specific to the physical, economic, cultural or social identity of you as a natural person.</p>
</li>
</ol></li>
</ol></li>
<li>
<h2>Your data controller</h2>
<p>Payward provides the Kraken Exchange Services through local operating entities that are subsidiaries of Payward, Inc.<br />
You are contracting with one Payward group company, as follows:</p>
<ul><li>
<p>If you reside in a country within the European Union, you are contracting with Payward Ltd., 6th Floor, One London Wall, London, EC2Y 5EB.</p>
</li>
<li>
<p>If you reside in Japan, you are contracting with Payward KK Nibancho 9-3, Chiyoda-ku, Tokyo 102-0084, Japan</p>
</li>
<li>
<p>If you reside in the United States, you are contracting with Payward Ventures Inc., 237 Kearny Street #102, San Francisco, CA 94108.</p>
</li>
<li>
<p>If you reside in the rest of the world, you are contracting with Payward Pte. Ltd., 8 Tomasello Boulevard, #15-04, Suntec Tower Three, Singapore 038988.<br />
The Payward company you are contracting with is your data controller, and is responsible for the collection, use, disclosure, retention and protection of your personal information in accordance with our global privacy standards, this Privacy Notice, as well as any applicable national laws. The Payward group companies use encryption to protect your information and store decryption keys in separate systems. Data controllers process and retain your personal information on our servers where our data centers are located, including the European Union, Japan, Singapore, the United States of America and elsewhere in the world.<br />
Where we have a legal obligation to do so, we have appointed data protection officers (DPOs) to be responsible for Payward’s privacy program for the respective data controllers. If you have any questions about how we protect or use your Personal Information, you can contact our DPOs by email at <a href="mailto:support@kraken.com" to="mailto:support@kraken.com">support@kraken.com</a>.</p>
</li>
</ul></li>
<li>
<h2>How do we protect personal information?</h2>
<p>We are serious about guarding the privacy of your Personal Information and work hard to protect Payward and you from unauthorized access to, or unauthorized alteration, disclosure, or destruction of Personal Information we collect and store. Measures we take include encryption with SSL; the availability of two-factor authentication at your discretion, and periodic review of our Personal Information collection, storage, and processing practices. We restrict access of your Personal Information only to those Payward employees, affiliates, and subcontractors who have a legitimate business need for accessing such information. We continuously educate and train our employees about the importance of confidentiality and privacy of customer information. We maintain physical, electronic, and procedural safeguards that comply with the relevant laws and regulations to protect your Personal Information from unauthorized access.</p>
<p>Unfortunately, despite best practices and technical safeguards, the transmission of information via the internet is not completely secure. Although we do our best to protect your Personal Data, we cannot guarantee the security of your Personal Data during transmission, and any acts of transmission are at your own risk.</p>
</li>
<li>
<h2>Information we may collect about you</h2>
<ol><li>
<p>Information you give us.</p>
<ul><li>
<p>Full legal name;</p>
</li>
<li>
<p>Home address, including country of residence;</p>
</li>
<li>
<p>Email address;</p>
</li>
<li>
<p>Mobile phone number;</p>
</li>
<li>
<p>Date of birth;</p>
</li>
<li>
<p>Proof of identity (e.g., driver’s license, passport, or government-issued<br />
identity card);</p>
</li>
<li>
<p>U.S. Social Security Number or any comparable identification number issued by a government;</p>
</li>
<li>
<p>Other Personal Information or commercial and/or identification information – Whatever information we, in our sole discretion, deem necessary to comply with our legal obligations under various anti-money laundering (AML) obligations, such as under the European Union’s 4th AML Directive and the U.S. Bank Secrecy Act (BSA).</p>
</li>
</ul></li>
<li>
<p>Information we collect about you automatically.</p>
<ul><li>
<p>Location Information – Information that is automatically collected via analytics systems providers to determine your location, including your IP address and/or domain name and any external page that referred you to us, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system, and platform;</p>
</li>
<li>
<p>Log Information – Information that is generated by your use of Kraken Exchange Services that is automatically collected and stored in our server logs. This may include, but is not limited to, device-specific information, location information, system activity and any internal and external information related to pages that you visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our Website or App (including date and time; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page;</p>
</li>
<li>
<p>Account Information – Information that is generated by your Kraken account activity including, but not limited to, instructions regarding funding and disbursement, orders, trades, and account balances;</p>
</li>
<li>
<p>Correspondence – Information that you provide to us in written or oral correspondence, including opening a Kraken account, and with respect to ongoing customer support; and logging of phone numbers used to contact us.</p>
</li>
</ul></li>
<li>
<p>Information we receive about you from other sources.<br />
We may receive information about you if you use any of the other websites we operate or the other services we provide. We are also working closely with third parties and may receive information about you from them. For example:</p>
<ul><li>
<p>The banks you use to transfer money to us will provide us with your basic personal information, such as your name and address, as well as your financial information such as your bank account details;</p>
</li>
<li>
<p>Business partners may provide us with your name and address, as well as financial information;</p>
</li>
<li>
<p>Advertising networks, analytics providers and search information providers may provide us with anonymized or de-identified information about you, such as confirming how you found our website;</p>
</li>
<li>
<p>Credit reference agencies do not provide us with any personal information about you, but may be used to corroborate the information you have provided to us.</p>
</li>
</ul></li>
<li>
<p>General Provisions</p>
<p>Personal Information you provide during the registration process may be retained, even if your registration is left incomplete or abandoned. If you are located within the EEA, this Information will not be retained without your consent.</p>
In providing the personal data of any individual (other than yourself) to us during your use of the Kraken Exchange Services, you promise that you have obtained consent from such individual to disclose his/her personal data to us, as well his/her consent to our collection, use and disclosure of such personal data for the purposes set out in this Privacy Notice.
<p> </p>
</li>
</ol></li>
<li>
<h2>Uses of personal data</h2>
<p>Below we set forth in a table format, a description of all the ways we use your personal data as stated above, and which of the legal bases we rely on to do so. We have also identified what our legitimate interests are, where appropriate.</p>
<p>Note that we may process your personal data for more than one lawful ground depending on the specific purpose for which we are using your data. Please contact us at <a href="mailto:support@kraken.com" to="mailto:support@kraken.com">support@kraken.com</a> if you need details about the specific legal ground we are relying on to process your personal data where more than one ground has been set out in the table below.<br /><br />
 </p>
<div class="kraken-table scroll"><table cellPadding="1" cellSpacing="1" style="width:900px"><thead><tr><th scope="col">What We Use Your Personal Information For</th><th scope="col">Type of Information</th><th scope="col">Our Reasons</th><th scope="col">Our Legitimate Interests</th></tr></thead><tbody><tr><td><p>To provide the Kraken Exchange Services:</p><ul><li>To carry out our contractual obligations relating to you; including the fulfillment of your orders and trades, the operation of your Kraken account and the provision of our support services.</li></ul></td><td><p>Submitted Information</p></td><td><p>Fulfilling contracts</p><p>Our legitimate interests</p><p>Our legal obligation</p></td><td>Being efficient about how we fulfill our legal and contractual duties.<br />Our commercial interest in providing you with a good service.<br />Complying with regulations that apply to us.</td></tr><tr><td><ul><li><p>To comply with legal and/or regulatory requirements; including, but not limited to anti-money laundering requirements.</p></li></ul></td><td><p>Submitted Information</p><p>Device Information</p><p>Third Party Information</p></td><td><p>Our legitimate interests</p><p>Our legal obligation</p></td><td>Complying with regulations that apply to us.</td></tr><tr><td><p>To help Payward to market and provide services that interest you:</p><ul><li><p>To provide you with information about new functionality, features, or services we offer related to, or similar to, the Kraken Exchange Services;</p></li><li><p>To provide you with information about new functionality, features or services we feel may interest you;</p></li><li><p>To make suggestions and recommendations to you and other users of Kraken Exchange Services;</p></li><li><p>To process applications for Kraken Accounts services, including making decisions about whether to agree to approve any applications.</p></li></ul></td><td><p>Submitted Information</p><p>Device Information</p></td><td><p>Our legitimate interests</p></td><td><p>Developing products and services for existing customers and any applicable fees in relation to them.</p><p>Defining types of customers for new products or services.</p><p>Being efficient about how we fulfill our legal and contractual duties.</p><p>Seeking your consent when we need it to contact you.</p></td></tr><tr><td><p>To keep the Kraken Exchange Services available and properly functioning;</p><p>To administer internal operations necessary to Kraken Exchange Services, including troubleshooting, data analysis, testing, research, statistical, and survey purposes;</p><p>To notify you about changes to our service, changes to our terms and policies, and as part of our efforts to keep the Kraken Exchange Services, safe and secure.</p></td><td><p>Submitted Information</p><p>Device Information</p></td><td><p>Fulfilling contracts</p><p>Our legitimate interests</p><p>Our legal obligation</p></td><td><p>Being efficient about how we fulfill our legal and contractual duties.</p><p>Complying with regulations that apply to us.</p></td></tr></tbody></table></div><p>Definitions for Table:</p>
<ul><li>
<p>Legitimate Interest: this means an interest of ours as a business to continue to innovate and improve the Kraken Exchange Services and offer the most secure experience possible. We make sure to consider and balance any potential impact on you (both positive and negative) and your rights before we process your personal data for our legitimate interests. We do not use your personal data for activities where our interests are overridden by the impact on you (unless we have your consent or are otherwise required or permitted to by law).</p>
</li>
<li>
<p>Fulfilling a Contract: this means processing your data where it is necessary for the performance of a contract to which you are a party or to take steps at your request before entering into such a contract.</p>
</li>
<li>
<p>Our Legal Obligation: this means processing your personal data where it is necessary for compliance with a legal or regulatory obligation that we are subject to.</p>
</li>
</ul></li>
<li>
<h2>Disclosure of your personal information</h2>
<ol><li>
<p>We may share your personal information with selected third parties including:</p>
<ul><li>
<p>Affiliates, operational and business partners, suppliers and sub-contractors for the performance and execution of any contract we enter into with them or you;</p>
</li>
<li>
<p>Analytics and search engine providers that assist us in the improvement and optimization of our site; and</p>
</li>
<li>
<p>Our group entities or subsidiaries.</p>
</li>
</ul></li>
<li>
<p>We may disclose your personal information to third parties when:</p>
<ul><li>
<p>We sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets in order to continue the services for which you have contracted;</p>
</li>
<li>
<p>We are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our Terms of Service and other applicable agreements; or to protect the rights, property, or safety of Payward, our customers, or others. This includes exchanging information with other companies and organizations for the purposes of fraud protection and credit risk reduction;</p>
</li>
<li>
<p>We conduct or cooperate in investigations of fraud or other illegal activity whenever we believe it is reasonable and appropriate to do so;</p>
</li>
<li>
<p>We seek to prevent and detect fraud and crime;</p>
</li>
<li>
<p>We receive a lawful subpoena, warrant, court order, or as otherwise required by law;</p>
</li>
<li>
<p>We seek to protect Payward from financial and insurance risks;</p>
</li>
<li>
<p>We seek to recover debt or in relation to your insolvency; and</p>
</li>
<li>
<p>We seek information necessary to maintain or develop customer relationships, services and systems.</p>
</li>
</ul></li>
<li>
<p>We do not maintain a published list of all of the third parties with whom we share your personal information with, as this would be heavily dependent on your specific use of our Services. However, if you would like further information about who we have shared your personal information with, or to be provided with a list specific to you, you can request this by writing to <a href="mailto:support@kraken.com" to="mailto:support@kraken.com">support@kraken.com</a>.</p>
</li>
</ol></li>
<li>
<h2>Where we store your personal data</h2>
<p>Our operations are supported by a network of computers, servers, and other infrastructure and information technology, including, but not limited to, third-party service providers. We and our third-party service providers store and process your personal data in the European Union, Japan, Singapore, the United States of America, and elsewhere in the world.</p>
</li>
<li>
<h2>Where we store your personal data</h2>
<p>In order to provide our Services to you, it is sometimes necessary for us to transfer your data to the third parties outlined in section 7.1 that are based outside of the European Economic Area (&quot;EEA&quot;). In these cases, we ensure that both ourselves and our partners take adequate and appropriate technical, physical, and organizational security measures to protect your data. We also ensure we have appropriate contractual protections in place with these third parties.</p>
<p>By using Payward Services, you consent to your Personal Data being transferred to other countries, including countries that have different data protection rules than your country. In all such transfers, we will protect your personal information as described in this Privacy Notice and in a manner consistent with the principles of the EU-US and Swiss-US Privacy Shield framework. For more information, please contact us at <a href="mailto:support@kraken.com" to="mailto:support@kraken.com">support@kraken.com</a>.</p>
</li>
<li>
<h2>Profiling and automated decision making</h2>
<p>We may in some instances use your personal data, such as your country of residence and trade history, in order to better address your needs. For example, if you frequently trade in a specific virtual currency, we may use this information to inform you of new product updates or features that may be useful for you. When we do this, we take all necessary measures to ensure that your privacy and security are protected, and we only use anonymous or de-identified data.</p>
<p>We may use Automated Decision Making (ADM) in order to improve your experience, or to help fight financial crime. For example, so that we can provide you with a fast and efficient service, we may use ADM to verify your identity documents, or to confirm the accuracy of the information you have provided to us. None of our ADM processes have a legal effect on you.</p>
</li>
<li>
<h2>Privacy when using digital assets and blockchains</h2>
<p>Your funding of bitcoin, XRP, ether, and other Digital Assets, may be recorded on a public blockchain. Public blockchains are distributed ledgers, intended to immutably record transactions across wide networks of computer systems. Many blockchains are open to forensic analysis which can lead to deanonymization and the unintentional revelation of private financial information, especially when blockchain data is combined with other data.<br />
Because blockchains are decentralized or third-party networks which are not controlled or operated by Payward or its affiliates, we are not able to erase, modify, or alter personal data from such networks.</p>
</li>
<li>
<h2>Data Retention</h2>
<p>As Payward and its affiliates are subject to various legal, compliance and reporting obligations, including BSA or national AML requirements (as defined above), we are required by law to store some of your personal and transactional data beyond the closure of your account with us. Your data is only accessed internally on a need to know basis, and it will only be accessed or processed if absolutely necessary. We will delete data that is no longer required by any relevant law or jurisdiction in which we operate.</p>
</li>
<li>
<h2>Cookies</h2>
<p>When you use Kraken Exchange Services, we may make use of the standard practice of placing tiny data files called cookies, flash cookies, pixel tags, or other tracking tools (herein, “Cookies”) on your computer or other devices used to visit Kraken Exchange Services. We use Cookies to (i) help us recognize you as a customer, collect information about your use of Kraken Exchange Services to better customize our services and content for you, and to collect information about your computer or other access devices as part to ensure our compliance with our BSA and AML obligations and (ii) ensure that your account security has not been compromised by detecting irregular or suspicious account activities.</p>
<p>We use both session and persistent Cookies. Session Cookies expire when you log out of your account or close your browser. Persistent Cookies remain on your computer or mobile device until deleted or otherwise expire. Most web browsers are set to accept Cookies by default. You are free to decline most of our session Cookies if your browser or browser add-on permits but choosing to remove or disable our Cookies may interfere with your use and functionality of the Kraken Exchange Services.</p>
</li>
<li>
<h2>Your choices</h2>
<ul><li>
<p>Email Communications – Subject to applicable laws and regulations, we may from time to time send communications promoting services, products, facilities, or activities to you using information collected from you. When you create a Kraken Account you are agreeing to opt-in to all communications from us. We will provide you with an opportunity to opt-out of newsletter communications.. We will never provide your Personal Information to third parties for direct marketing or other unrelated purposes without your written consent. Unfortunately, we are unable to allow you to opt-out of the emails we send you for certain legal or administrative purposes.</p>
</li>
<li>
<p>Cookies – You may decline session cookies if your browser or browser add-on permits but choosing to remove or disable our cookies may interfere with your use and functionality of the Kraken Exchange Services.</p>
</li>
<li>
<p>Right of Rectification– You have the right to correct any personal information We hold on you that is inaccurate, incorrect, or out of date.</p>
</li>
<li>
<p>Right of Erasure – You have the right to ask us to delete your data when it is no longer necessary, or no longer subject to a legal obligation to which Payward is subject to.</p>
</li>
<li>
<p>Portability – You may have the right to transfer certain of your personal data between data controllers, for example, to move your account details from one online platform to another.</p>
</li>
<li>
<p>Location Information – You may be able to stop the collection of location information through your device settings or by following the standard uninstall process to remove our applications from your device; however, because such data is used by us to meet legal requirements, as well as for ongoing fraud and risk monitoring purposes, choosing to remove or disable location services may interfere with your use and functionality of the Kraken Exchange Services.</p>
</li>
</ul></li>
<li>
<h2>Access to Information</h2>
<p>Subject to applicable laws, you may have the right to access information we hold about you. Your right of access can be exercised in accordance with the relevant data protection legislation. For further information, please contact <a href="mailto:support@kraken.com" to="mailto:support@kraken.com">support@kraken.com</a>.</p>
</li>
<li>
<h2>Changes to this Privacy Notice</h2>
<p>Any changes we may make to our Privacy Notice will be posted on this page and, where appropriate, notified to you by email. Please check back frequently to see any updates or changes to our Privacy Notice.</p>
</li>
<li>
<h2>Changes to this Privacy Notice</h2>
<p>Kraken Exchange Services are not directed to persons under the age of 18, hereinafter “Minors” and we do not knowingly collect personal information from Minors. If we learn that we have inadvertently gathered personal information from a Minor, we will take legally permissible measures to remove that information from our records. Payward will require the user to close his or her account and will not allow the use of Payward Services. If you are a parent or guardian of a Minor, and you become aware that a Minor has provided personal information to Payward, please contact us at <a href="mailto:support@kraken.com" to="mailto:support@kraken.com">support@kraken.com</a> and you may request to exercise your applicable access, rectification, cancellation, and/or objection rights.</p>
</li>
<li>
<h2>Contact</h2>
<p>Any questions, comments and requests regarding this Privacy Notice are welcomed and should be addressed to <a href="mailto:support@kraken.com" to="mailto:support@kraken.com">support@kraken.com</a>.</p>
<p>If you feel that we have not addressed your questions or concerns adequately, you may contact your data controller.</p>
<p><strong>For residents of the European Union (United Kingdom):</strong><br />
The Information Commissioner’s Office<br />
Wycliffe House, Water Ln<br />
Wilmslow SK9 5AF, UK</p>
<p><strong>For residents of Japan:</strong><br />
Personal Information Protection Commission<br />
Kasumigaseki Common Gate West Tower 32nd Floor, <br />
3-2-1, Kasumigaseki, Chiyoda-ku, <br />
Tokyo, 100-0013, Japan</p>
<p><strong>For residents of the United States:</strong><br />
The Federal Trade Commission <br />
600 Pennsylvania Avenue, NW <br />
Washington, DC 20580</p>
<p><strong>For residents of the rest of the world (Singapore):</strong><br />
Personal Data Protection Commission <br />
10 Pasir Panjang Road, <br />
#03-01 Mapletree Business City Singapore 117438 </p>
</li>
</ol></div></div></div></div></div></article><footer class="kraken-footer"><div class="kraken-content-wrapper"><div class="footer-menus"><div class="footer-sign-in"><p>Take your crypto to the next level with Kraken.</p><div class="signin-2"><a href="/sign-up" class="kraken-cta" location="Footer" to="/sign-up">Sign Up</a><a href="/sign-in" class="kraken-cta inverted" location="Footer" to="/sign-in">Sign In</a></div></div><ul class="footer-primary"><li><span class="menu-title">Features</span><ul><li><a href="/en-us/features/24-7-support" to="/en-us/features/24-7-support">24/7 Support</a></li><li><a href="/en-us/features/account-management" to="/en-us/features/account-management">Account Management</a></li><li><a href="/features/api" to="/features/api">API</a></li><li><a href="/en-us/features/security/bug-bounty" to="/en-us/features/security/bug-bounty">Bug Bounty</a></li><li><a href="/en-us/features/fee-schedule" to="/en-us/features/fee-schedule">Fee Schedule</a></li><li><a href="/en-us/features/funding-options" to="/en-us/features/funding-options">Funding Options</a></li><li><a href="/en-us/features/futures" to="/en-us/features/futures">Futures</a></li><li><a href="/en-us/features/indices" to="/en-us/features/indices">Indices</a></li><li><a href="/en-us/features/liquidity" to="/en-us/features/liquidity">Liquidity</a></li><li><a href="/en-us/features/margin-trading" to="/en-us/features/margin-trading">Margin Trading</a></li><li><a href="/en-us/features/otc-exchange" to="/en-us/features/otc-exchange">OTC</a></li><li><a href="/en-us/features/security/pgp" to="/en-us/features/security/pgp">PGP Key</a></li><li><a href="/en-us/proof-of-reserves-audit" to="/en-us/proof-of-reserves-audit">Proof of Reserves</a></li><li><a href="/en-us/features/security" to="/en-us/features/security">Security</a></li><li><a href="/redirect?url=https%3A%2F%2Fsupport.kraken.com%2Fhc%2Fen-us" target="_blank" rel="noopener" to="https://support.kraken.com/hc/en-us">Support</a></li><li><a href="/features/websocket-api" to="/features/websocket-api">WebSockets</a></li></ul></li><li><span class="menu-title">Prices</span><ul><li><a href="/en-us/cryptowatch" to="/en-us/cryptowatch">Cryptowatch</a></li><li><a href="/redirect?url=https%3A%2F%2Ftrade.kraken.com%2Fmarkets" target="_blank" rel="noopener" to="https://trade.kraken.com/markets">Prices Overview</a></li></ul></li><li><span class="menu-title">Learn</span><ul><li><a href="/redirect?url=https%3A%2F%2Fblog.kraken.com%2F" target="_blank" rel="noopener" to="https://blog.kraken.com/">Blog</a></li><li><a href="/en-us/institutions" to="/en-us/institutions">Institutions</a></li><li><a href="/en-us/learn/podcast" to="/en-us/learn/podcast">Podcast</a></li></ul></li><li><span class="menu-title">About</span><ul><li><a href="/en-us/why-kraken" to="/en-us/why-kraken">Why Kraken</a></li><li><a href="/redirect?url=https%3A%2F%2Fjobs.lever.co%2Fkraken" target="_blank" rel="noopener" to="https://jobs.lever.co/kraken">Careers</a></li><li><a href="/redirect?url=https%3A%2F%2Fsupport.kraken.com%2Fhc%2Fen-us%2Frequests%2Fnew" target="_blank" rel="noopener" to="https://support.kraken.com/hc/en-us/requests/new">Contact</a></li><li><a href="/en-us/press" to="/en-us/press">Press</a></li></ul></li></ul></div><div class="footer-bottom"><div class="logo-wrapper"><a href="/" class="footer-logo" to="/">Kraken</a></div><ul class="social-wrapper"><li><a href="/redirect?url=https%3A%2F%2Fwww.facebook.com%2FKrakenFX%2F" target="_blank" rel="noopener" class="icon-facebook" to="https://www.facebook.com/KrakenFX/" data-social="Facebook" title="Facebook"></a></li><li><a href="/redirect?url=https%3A%2F%2Ftwitter.com%2Fkrakenfx" target="_blank" rel="noopener" class="icon-twitter" to="https://twitter.com/krakenfx" data-social="Twitter" title="Twitter"></a></li><li><a href="/redirect?url=https%3A%2F%2Fwww.linkedin.com%2Fcompany%2Fkraken-exchange" target="_blank" rel="noopener" class="icon-linkedin" to="https://www.linkedin.com/company/kraken-exchange" data-social="Linkedin" title="Linkedin"></a></li></ul><div class="copyright-notice"><p>© 2011 - 2019 Payward, Inc.</p><ul class="legal-links"><li><a href="/en-us/legal/privacy" to="/en-us/legal/privacy">Privacy Policy</a></li><li><a href="/en-us/legal" to="/en-us/legal">Terms of Service</a></li></ul></div><div class="language-switcher-wrap"><form><div class="current-language"><label for="curlang">Language</label></div><div class="switcher-menu"><select id="curlang"><option selected="" data-lang="en-us" value="en-us">U.S. English</option><option data-lang="en-gb" value="en-gb">British English</option><option data-lang="es-ES" value="es-ES">Español</option><option data-lang="zh-CN" value="zh-CN">中文</option><option data-lang="ja-jp" value="ja-jp">日本語</option></select></div><span style="display:none">/</span></form></div></div></div></footer><div class="kraken-cookie-warning"><div class="warning-container"><div class="warning-text">We use cookies, k?</div><div class="warning-controls"><button class="kraken-cta accept">Accept</button><a class="kraken-cta reject" href="/legal/privacy/">Privacy Policy</a></div></div></div></div></div><script defer="" type="text/javascript" src="/js2/templates/05f41b83.d3cbc2d1.min.js"></script><script defer="" type="text/javascript" src="/js2/templates/05f41b83.a82cb648.min.js"></script><script defer="" type="text/javascript" src="/js2/templates/05f41b83.de932601.min.js"></script><script defer="" type="text/javascript" src="/js2/templates/05f41b83.65355e27.min.js"></script><script defer="" type="text/javascript" src="/js2/templates/05f41b83.ad3c2804.min.js"></script><script defer="" type="text/javascript" src="/js2/main.05f41b83.min.js"></script></body></html>