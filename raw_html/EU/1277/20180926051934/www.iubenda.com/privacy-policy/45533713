<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html id="iubenda_policy" class="iubenda_fixed_policy">
<head>



  <title>Privacy Policy of All3DP</title>

	<meta content="All3DP collects some Personal Data from its Users." name="description" />
	<meta content="privacy policy" name="keywords" />

  <meta http-equiv="Content-Language" content="en" />
  <meta name="robots" content="noindex, follow">

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="date" content="2018-06-06">
	<meta http-equiv="last-modified" content="2018-09-06">
	<meta property="og:title" content="Privacy Policy of All3DP" />
	<meta property="og:type" content="website" />
	<meta property="og:image" content="https://www.iubenda.com/images/site/fb-image.png" />
	<meta property="og:site_name" content="iubenda" />
	<meta property="fb:app_id" content="190131204371223"/>
	<link rel="image_src" href="https://www.iubenda.com/images/site/fb-image.png"/>

  <meta name="viewport" content="width=device-width">

  <link href="/assets/privacy_policy-9c48f81f3e4ab1916759292b967b4c60.css" media="screen" rel="stylesheet" type="text/css" />
  <script src="/assets/privacy_policy-474d8657e20b2d06a569c0471da4c0c6.js" type="text/javascript"></script>

</head>
<body>

  

  <div id="wbars_all">
	  <div class="iub_container iub_base_container">
	    <div id="wbars">
	      <div class="iub_content simple_pp">

	        
          
<div class="iub_header">

    <h1>Privacy Policy of <strong>All3DP</strong></h1>

      <p>
        All3DP collects some Personal Data from its Users.
      </p>
</div> <!-- /header -->
	        
          



    <h2 id="purposes_data">Personal Data collected for the following purposes and using the following services:</h2>

    <ul class="for_boxes cf">

      <li class="one_line_col">
        <ul class="for_boxes">


            <li>

              <div class="iconed policyicon_purpose_7">

                <h3>Advertising</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Google AdSense</h3>
                      <p>Personal Data: Cookies and Usage Data</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_5">

                <h3>Analytics</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Google Analytics, Alexa Metrics and MixPanel</h3>
                      <p>Personal Data: Cookies and Usage Data</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_34">

                <h3>Backup saving and management</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Vaultpress</h3>
                      <p>Personal Data: various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_27">

                <h3>Commercial affiliation</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Banggood Affiliates Program, Amazon Affiliation, AliExpress Affiliate and CJ Affiliate by Conversant</h3>
                      <p>Personal Data: Cookies and Usage Data</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_10">

                <h3>Contacting the User</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Contact form</h3>
                      <p>Personal Data: email address, first name and last name</p>
                    </li>


                    <li>
                      <h3>Mailing list or newsletter</h3>
                      <p>Personal Data: email address</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_13">

                <h3>Content commenting</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Disqus</h3>
                      <p>Personal Data: Cookies, Usage Data and various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_22">

                <h3>Content performance and features testing (A/B testing)</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Google Website Optimizer</h3>
                      <p>Personal Data: Cookies and Usage Data</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_49">

                <h3>Data transfer outside the EU</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Data transfer from the EU and/or Switzerland to the U.S based on Privacy Shield, Data transfer to countries that guarantee European standards, Other legal basis for Data transfer abroad, Data transfer abroad based on standard contractual clauses and Data transfer abroad based on consent</h3>
                      <p>Personal Data: various types of Data</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_25">

                <h3>Displaying content from external platforms</h3>


                <ul class="unstyled">




                    <li>
                      <h3>YouTube video widget</h3>
                      <p>Personal Data: Cookies and Usage Data</p>
                    </li>


                    <li>
                      <h3>Google Fonts</h3>
                      <p>Personal Data: Usage Data and various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_21">

                <h3>Handling payments</h3>


                <ul class="unstyled">




                    <li>
                      <h3>PayPal and Stripe</h3>
                      <p>Personal Data: various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_23">

                <h3>Heat mapping and session recording</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Hotjar Heat Maps & Recordings</h3>
                      <p>Personal Data: Cookies, Usage Data and various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_42">

                <h3>Hosting and backend infrastructure</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Algolia, Amazon Web Services (AWS), mLab and Cloudinary</h3>
                      <p>Personal Data: various types of Data as specified in the privacy policy of the service</p>
                    </li>


                    <li>
                      <h3>iubenda Consent Solution</h3>
                      <p>Personal Data: Data communicated while using the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_30">

                <h3>Infrastructure monitoring</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Pingdom</h3>
                      <p>Personal Data: Cookies and Usage Data</p>
                    </li>


                    <li>
                      <h3>Sentry and New Relic</h3>
                      <p>Personal Data: various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_26">

                <h3>Interaction with external social networks and platforms</h3>


                <ul class="unstyled">




                    <li>
                      <h3>PayPal button and widgets</h3>
                      <p>Personal Data: Cookies and Usage Data</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_17">

                <h3>Location-based interactions</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Non-continuous geolocation and Geolocation</h3>
                      <p>Personal Data: geographic position</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_18">

                <h3>Managing contacts and sending messages</h3>


                <ul class="unstyled">




                    <li>
                      <h3>MailChimp</h3>
                      <p>Personal Data: email address</p>
                    </li>


                    <li>
                      <h3>OneSignal</h3>
                      <p>Personal Data: Cookies, email address, geographic position, language, unique device identifiers for advertising (Google Advertiser ID or IDFA, for example), Usage Data and various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_31">

                <h3>Managing landing and invitation pages</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Unbounce</h3>
                      <p>Personal Data: Cookies, email address and Usage Data</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_48">

                <h3>Tag Management</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Google Tag Manager</h3>
                      <p>Personal Data: Cookies and Usage Data</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_29">

                <h3>Traffic optimization and distribution</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Cloudflare</h3>
                      <p>Personal Data: Cookies and various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

          
            <li>

              <div class="iconed policyicon_purpose_35">

                <h3>User database management</h3>


                <ul class="unstyled">




                    <li>
                      <h3>Intercom</h3>
                      <p>Personal Data: Cookies, email address, Usage Data and various types of Data as specified in the privacy policy of the service</p>
                    </li>


                    <li>
                      <h3>Pipedrive</h3>
                      <p>Personal Data: various types of Data as specified in the privacy policy of the service</p>
                    </li>


                </ul>

              </div>

            </li>

           

        </ul>
      </li>

    </ul>
   

 


	<h2 id="further_data">Further information about Personal Data</h2>
	<ul class="for_boxes cf">
	  <li class="one_line_col wide">
	    <ul class="for_boxes">
                <li>
                  <div class="iconed icon_general">
                    <h3>Kinsta</h3>
                    <p class="more_one">Hosting of all3dp.com and m.all3dp.com is managed by Kinsta. This includes website hosting, backups, web database, file storage, APIs, and log files. Kinsta’s privacy policy can be found here: https://kinsta.com/privacy-policy/</p>
                  </div>
                </li>
                <li>
                  <div class="iconed icon_general">
                    <h3>Push notifications</h3>
                    <p class="more_one">All3DP may send push notifications to the User.<br/></p>
                  </div>
                </li>
                <li>
                  <div class="iconed icon_general">
                    <h3>Selling goods and services online</h3>
                    <p class="more_one">The Personal Data collected are used to provide the User with services or to sell goods, including payment and possible delivery.<br/><br/>The Personal Data collected to complete the payment may include the credit card, the bank account used for the transfer, or any other means of payment envisaged. The kind of Data collected by All3DP depends on the payment system used.<br/></p>
                  </div>
                </li>
	    </ul>
	  </li>
	</ul>


	<h2 id="contact_information">Contact information</h2>
	<ul class="for_boxes cf">
	  <li class="one_line_col">
	    <ul class="for_boxes">
        <li>
          <div class="iconed icon_owner">
            <h3>Owner and Data Controller</h3>
                <p>All3DP GmbH, Ridlerstr. 31, 80339 Germany</p>
                <p><b>Owner contact email:</b> contact@all3dp.com</p>
          </div>
        </li>
	    </ul>
	  </li>
	</ul>


           
	
          
<div class="iub_footer">

  <p>
    Latest update: July 27, 2018
  </p>

	 
		<p>
			<a href="//www.iubenda.com" title="iubenda - Privacy Policy generator">iubenda</a> hosts this content and only collects
			<a href="//www.iubenda.com/privacy-policy/877900">the Personal Data strictly necessary</a> for it to be provided
		</p>

                <a href="//www.iubenda.com/privacy-policy/45533713/legal" title="Show the complete Privacy Policy" class="show_comp_link iframe-preserve an-preserve">Show the complete Privacy Policy</a>

</div> <!-- /footer -->

<script type="text/javascript">
  function tryFunc(fName,args){
    if(typeof window[fName]==='function'){
      window[fName](args);
    }else{
      if(args){
        /* default behaviour is link */
        if(args.href){
          /* default link is target='_blank' */
          window.open(args.href)
        }
      }
    }
    return false; /* inhibit default behaviour */
  }
</script>


	
	      </div> <!-- /content -->
	    </div> <!-- /wbars -->
	
	
	  </div> <!-- /container base_container -->
	</div> <!-- /wbars_wrapper -->
 

  <script type="text/javascript">

    var privacyPolicy = new PrivacyPolicy({
      id:1267086,
      noBrand:true
    })

    $(document).ready(function() {

      privacyPolicy.start();

			$(".expand-content").hide();
			$(".expand").addClass("collapsed");
			$(".expand .expand-click").click(function () {
				$(this).parents(".expand").toggleClass("collapsed");
				$(this).parents(".expand").toggleClass("expanded");
				$(this).parents(".expand-item").toggleClass("hover");
				$(this).children('.icon-17').toggleClass("icon-expand");
				$(this).children('.icon-17').toggleClass("icon-collapse");
				$(this).parents('.expand').children('.expand-content').slideToggle("fast");
			});

    });

  </script>

</body>
</html>
