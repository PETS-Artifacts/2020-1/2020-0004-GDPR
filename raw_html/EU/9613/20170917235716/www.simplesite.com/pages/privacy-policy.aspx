
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
<head id="frame_Head1"><title>
	Privacy policy | SimpleSite
</title><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="keywords" content="privacy, privacy policy">
	<meta name="description" content="By using SimpleSite's Website Service, you declare that you have read, understood and accepted our Privacy policy.">
	<meta name="google" value="notranslate">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="alternate" hreflang="da" href="http://da.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="en" href="http://www.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="nb" href="http://no.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="sv" href="http://sv.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="nl" href="http://nl.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="fr" href="http://fr.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="it" href="http://it.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="es" href="http://es.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="fi" href="http://fi.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="de" href="http://de.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="pt" href="http://pt.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="ru" href="http://ru.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="id" href="http://id.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="tr" href="http://tr.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="pl" href="http://pl.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="cs" href="http://cs.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="ms" href="http://ms.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="el" href="http://el.simplesite.com/pages/privacy-policy.aspx" /> 
    <link rel="alternate" hreflang="ar" href="http://ar.simplesite.com/pages/privacy-policy.aspx" /> 
    
	<meta http-equiv="imagetoolbar" content="no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"><script type="text/javascript">window.NREUM||(NREUM={});NREUM.info = {"beacon":"bam.nr-data.net","errorBeacon":"bam.nr-data.net","licenseKey":"17c3efee35","applicationID":"29916044","transactionName":"YgZQN0RZWkRYW0RbWltMczBmF0RWXl1DHUVHCkQCVUEZR1ZUWVFMGwJBE04=","queueTime":0,"applicationTime":19,"agent":"","atts":""}</script><script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={xpid:"VQUPWFVWDhACXVhTAQEDUg=="};window.NREUM||(NREUM={}),__nr_require=function(t,n,e){function r(e){if(!n[e]){var o=n[e]={exports:{}};t[e][0].call(o.exports,function(n){var o=t[e][1][n];return r(o||n)},o,o.exports)}return n[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({1:[function(t,n,e){function r(t){try{s.console&&console.log(t)}catch(n){}}var o,i=t("ee"),a=t(15),s={};try{o=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(s.console=!0,o.indexOf("dev")!==-1&&(s.dev=!0),o.indexOf("nr_dev")!==-1&&(s.nrDev=!0))}catch(c){}s.nrDev&&i.on("internal-error",function(t){r(t.stack)}),s.dev&&i.on("fn-err",function(t,n,e){r(e.stack)}),s.dev&&(r("NR AGENT IN DEVELOPMENT MODE"),r("flags: "+a(s,function(t,n){return t}).join(", ")))},{}],2:[function(t,n,e){function r(t,n,e,r,o){try{d?d-=1:i("err",[o||new UncaughtException(t,n,e)])}catch(s){try{i("ierr",[s,c.now(),!0])}catch(u){}}return"function"==typeof f&&f.apply(this,a(arguments))}function UncaughtException(t,n,e){this.message=t||"Uncaught error with no additional information",this.sourceURL=n,this.line=e}function o(t){i("err",[t,c.now()])}var i=t("handle"),a=t(16),s=t("ee"),c=t("loader"),f=window.onerror,u=!1,d=0;c.features.err=!0,t(1),window.onerror=r;try{throw new Error}catch(l){"stack"in l&&(t(8),t(7),"addEventListener"in window&&t(5),c.xhrWrappable&&t(9),u=!0)}s.on("fn-start",function(t,n,e){u&&(d+=1)}),s.on("fn-err",function(t,n,e){u&&(this.thrown=!0,o(e))}),s.on("fn-end",function(){u&&!this.thrown&&d>0&&(d-=1)}),s.on("internal-error",function(t){i("ierr",[t,c.now(),!0])})},{}],3:[function(t,n,e){t("loader").features.ins=!0},{}],4:[function(t,n,e){function r(t){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var o=t("ee"),i=t("handle"),a=t(8),s=t(7),c="learResourceTimings",f="addEventListener",u="resourcetimingbufferfull",d="bstResource",l="resource",p="-start",h="-end",m="fn"+p,w="fn"+h,v="bstTimer",y="pushState",g=t("loader");g.features.stn=!0,t(6);var b=NREUM.o.EV;o.on(m,function(t,n){var e=t[0];e instanceof b&&(this.bstStart=g.now())}),o.on(w,function(t,n){var e=t[0];e instanceof b&&i("bst",[e,n,this.bstStart,g.now()])}),a.on(m,function(t,n,e){this.bstStart=g.now(),this.bstType=e}),a.on(w,function(t,n){i(v,[n,this.bstStart,g.now(),this.bstType])}),s.on(m,function(){this.bstStart=g.now()}),s.on(w,function(t,n){i(v,[n,this.bstStart,g.now(),"requestAnimationFrame"])}),o.on(y+p,function(t){this.time=g.now(),this.startPath=location.pathname+location.hash}),o.on(y+h,function(t){i("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),f in window.performance&&(window.performance["c"+c]?window.performance[f](u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["c"+c]()},!1):window.performance[f]("webkit"+u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["webkitC"+c]()},!1)),document[f]("scroll",r,{passive:!0}),document[f]("keypress",r,!1),document[f]("click",r,!1)}},{}],5:[function(t,n,e){function r(t){for(var n=t;n&&!n.hasOwnProperty(u);)n=Object.getPrototypeOf(n);n&&o(n)}function o(t){s.inPlace(t,[u,d],"-",i)}function i(t,n){return t[1]}var a=t("ee").get("events"),s=t(18)(a,!0),c=t("gos"),f=XMLHttpRequest,u="addEventListener",d="removeEventListener";n.exports=a,"getPrototypeOf"in Object?(r(document),r(window),r(f.prototype)):f.prototype.hasOwnProperty(u)&&(o(window),o(f.prototype)),a.on(u+"-start",function(t,n){var e=t[1],r=c(e,"nr@wrapped",function(){function t(){if("function"==typeof e.handleEvent)return e.handleEvent.apply(e,arguments)}var n={object:t,"function":e}[typeof e];return n?s(n,"fn-",null,n.name||"anonymous"):e});this.wrapped=t[1]=r}),a.on(d+"-start",function(t){t[1]=this.wrapped||t[1]})},{}],6:[function(t,n,e){var r=t("ee").get("history"),o=t(18)(r);n.exports=r,o.inPlace(window.history,["pushState","replaceState"],"-")},{}],7:[function(t,n,e){var r=t("ee").get("raf"),o=t(18)(r),i="equestAnimationFrame";n.exports=r,o.inPlace(window,["r"+i,"mozR"+i,"webkitR"+i,"msR"+i],"raf-"),r.on("raf-start",function(t){t[0]=o(t[0],"fn-")})},{}],8:[function(t,n,e){function r(t,n,e){t[0]=a(t[0],"fn-",null,e)}function o(t,n,e){this.method=e,this.timerDuration=isNaN(t[1])?0:+t[1],t[0]=a(t[0],"fn-",this,e)}var i=t("ee").get("timer"),a=t(18)(i),s="setTimeout",c="setInterval",f="clearTimeout",u="-start",d="-";n.exports=i,a.inPlace(window,[s,"setImmediate"],s+d),a.inPlace(window,[c],c+d),a.inPlace(window,[f,"clearImmediate"],f+d),i.on(c+u,r),i.on(s+u,o)},{}],9:[function(t,n,e){function r(t,n){d.inPlace(n,["onreadystatechange"],"fn-",s)}function o(){var t=this,n=u.context(t);t.readyState>3&&!n.resolved&&(n.resolved=!0,u.emit("xhr-resolved",[],t)),d.inPlace(t,y,"fn-",s)}function i(t){g.push(t),h&&(x?x.then(a):w?w(a):(E=-E,O.data=E))}function a(){for(var t=0;t<g.length;t++)r([],g[t]);g.length&&(g=[])}function s(t,n){return n}function c(t,n){for(var e in t)n[e]=t[e];return n}t(5);var f=t("ee"),u=f.get("xhr"),d=t(18)(u),l=NREUM.o,p=l.XHR,h=l.MO,m=l.PR,w=l.SI,v="readystatechange",y=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"],g=[];n.exports=u;var b=window.XMLHttpRequest=function(t){var n=new p(t);try{u.emit("new-xhr",[n],n),n.addEventListener(v,o,!1)}catch(e){try{u.emit("internal-error",[e])}catch(r){}}return n};if(c(p,b),b.prototype=p.prototype,d.inPlace(b.prototype,["open","send"],"-xhr-",s),u.on("send-xhr-start",function(t,n){r(t,n),i(n)}),u.on("open-xhr-start",r),h){var x=m&&m.resolve();if(!w&&!m){var E=1,O=document.createTextNode(E);new h(a).observe(O,{characterData:!0})}}else f.on("fn-end",function(t){t[0]&&t[0].type===v||a()})},{}],10:[function(t,n,e){function r(t){var n=this.params,e=this.metrics;if(!this.ended){this.ended=!0;for(var r=0;r<d;r++)t.removeEventListener(u[r],this.listener,!1);if(!n.aborted){if(e.duration=a.now()-this.startTime,4===t.readyState){n.status=t.status;var i=o(t,this.lastSize);if(i&&(e.rxSize=i),this.sameOrigin){var c=t.getResponseHeader("X-NewRelic-App-Data");c&&(n.cat=c.split(", ").pop())}}else n.status=0;e.cbTime=this.cbTime,f.emit("xhr-done",[t],t),s("xhr",[n,e,this.startTime])}}}function o(t,n){var e=t.responseType;if("json"===e&&null!==n)return n;var r="arraybuffer"===e||"blob"===e||"json"===e?t.response:t.responseText;return h(r)}function i(t,n){var e=c(n),r=t.params;r.host=e.hostname+":"+e.port,r.pathname=e.pathname,t.sameOrigin=e.sameOrigin}var a=t("loader");if(a.xhrWrappable){var s=t("handle"),c=t(11),f=t("ee"),u=["load","error","abort","timeout"],d=u.length,l=t("id"),p=t(14),h=t(13),m=window.XMLHttpRequest;a.features.xhr=!0,t(9),f.on("new-xhr",function(t){var n=this;n.totalCbs=0,n.called=0,n.cbTime=0,n.end=r,n.ended=!1,n.xhrGuids={},n.lastSize=null,p&&(p>34||p<10)||window.opera||t.addEventListener("progress",function(t){n.lastSize=t.loaded},!1)}),f.on("open-xhr-start",function(t){this.params={method:t[0]},i(this,t[1]),this.metrics={}}),f.on("open-xhr-end",function(t,n){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&n.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),f.on("send-xhr-start",function(t,n){var e=this.metrics,r=t[0],o=this;if(e&&r){var i=h(r);i&&(e.txSize=i)}this.startTime=a.now(),this.listener=function(t){try{"abort"===t.type&&(o.params.aborted=!0),("load"!==t.type||o.called===o.totalCbs&&(o.onloadCalled||"function"!=typeof n.onload))&&o.end(n)}catch(e){try{f.emit("internal-error",[e])}catch(r){}}};for(var s=0;s<d;s++)n.addEventListener(u[s],this.listener,!1)}),f.on("xhr-cb-time",function(t,n,e){this.cbTime+=t,n?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof e.onload||this.end(e)}),f.on("xhr-load-added",function(t,n){var e=""+l(t)+!!n;this.xhrGuids&&!this.xhrGuids[e]&&(this.xhrGuids[e]=!0,this.totalCbs+=1)}),f.on("xhr-load-removed",function(t,n){var e=""+l(t)+!!n;this.xhrGuids&&this.xhrGuids[e]&&(delete this.xhrGuids[e],this.totalCbs-=1)}),f.on("addEventListener-end",function(t,n){n instanceof m&&"load"===t[0]&&f.emit("xhr-load-added",[t[1],t[2]],n)}),f.on("removeEventListener-end",function(t,n){n instanceof m&&"load"===t[0]&&f.emit("xhr-load-removed",[t[1],t[2]],n)}),f.on("fn-start",function(t,n,e){n instanceof m&&("onload"===e&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=a.now()))}),f.on("fn-end",function(t,n){this.xhrCbStart&&f.emit("xhr-cb-time",[a.now()-this.xhrCbStart,this.onload,n],n)})}},{}],11:[function(t,n,e){n.exports=function(t){var n=document.createElement("a"),e=window.location,r={};n.href=t,r.port=n.port;var o=n.href.split("://");!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=n.hostname||e.hostname,r.pathname=n.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname);var i=!n.protocol||":"===n.protocol||n.protocol===e.protocol,a=n.hostname===document.domain&&n.port===e.port;return r.sameOrigin=i&&(!n.hostname||a),r}},{}],12:[function(t,n,e){function r(){}function o(t,n,e){return function(){return i(t,[f.now()].concat(s(arguments)),n?null:this,e),n?void 0:this}}var i=t("handle"),a=t(15),s=t(16),c=t("ee").get("tracer"),f=t("loader"),u=NREUM;"undefined"==typeof window.newrelic&&(newrelic=u);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],l="api-",p=l+"ixn-";a(d,function(t,n){u[n]=o(l+n,!0,"api")}),u.addPageAction=o(l+"addPageAction",!0),u.setCurrentRouteName=o(l+"routeName",!0),n.exports=newrelic,u.interaction=function(){return(new r).get()};var h=r.prototype={createTracer:function(t,n){var e={},r=this,o="function"==typeof n;return i(p+"tracer",[f.now(),t,e],r),function(){if(c.emit((o?"":"no-")+"fn-start",[f.now(),r,o],e),o)try{return n.apply(this,arguments)}finally{c.emit("fn-end",[f.now()],e)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,n){h[n]=o(p+n)}),newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),i("err",[t,f.now()])}},{}],13:[function(t,n,e){n.exports=function(t){if("string"==typeof t&&t.length)return t.length;if("object"==typeof t){if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if(!("undefined"!=typeof FormData&&t instanceof FormData))try{return JSON.stringify(t).length}catch(n){return}}}},{}],14:[function(t,n,e){var r=0,o=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);o&&(r=+o[1]),n.exports=r},{}],15:[function(t,n,e){function r(t,n){var e=[],r="",i=0;for(r in t)o.call(t,r)&&(e[i]=n(r,t[r]),i+=1);return e}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],16:[function(t,n,e){function r(t,n,e){n||(n=0),"undefined"==typeof e&&(e=t?t.length:0);for(var r=-1,o=e-n||0,i=Array(o<0?0:o);++r<o;)i[r]=t[n+r];return i}n.exports=r},{}],17:[function(t,n,e){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],18:[function(t,n,e){function r(t){return!(t&&t instanceof Function&&t.apply&&!t[a])}var o=t("ee"),i=t(16),a="nr@original",s=Object.prototype.hasOwnProperty,c=!1;n.exports=function(t,n){function e(t,n,e,o){function nrWrapper(){var r,a,s,c;try{a=this,r=i(arguments),s="function"==typeof e?e(r,a):e||{}}catch(f){l([f,"",[r,a,o],s])}u(n+"start",[r,a,o],s);try{return c=t.apply(a,r)}catch(d){throw u(n+"err",[r,a,d],s),d}finally{u(n+"end",[r,a,c],s)}}return r(t)?t:(n||(n=""),nrWrapper[a]=t,d(t,nrWrapper),nrWrapper)}function f(t,n,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<n.length;c++)s=n[c],a=t[s],r(a)||(t[s]=e(a,f?s+o:o,i,s))}function u(e,r,o){if(!c||n){var i=c;c=!0;try{t.emit(e,r,o,n)}catch(a){l([a,e,r,o])}c=i}}function d(t,n){if(Object.defineProperty&&Object.keys)try{var e=Object.keys(t);return e.forEach(function(e){Object.defineProperty(n,e,{get:function(){return t[e]},set:function(n){return t[e]=n,n}})}),n}catch(r){l([r])}for(var o in t)s.call(t,o)&&(n[o]=t[o]);return n}function l(n){try{t.emit("internal-error",n)}catch(e){}}return t||(t=o),e.inPlace=f,e.flag=a,e}},{}],ee:[function(t,n,e){function r(){}function o(t){function n(t){return t&&t instanceof r?t:t?c(t,s,i):i()}function e(e,r,o,i){if(!l.aborted||i){t&&t(e,r,o);for(var a=n(o),s=h(e),c=s.length,f=0;f<c;f++)s[f].apply(a,r);var d=u[y[e]];return d&&d.push([g,e,r,a]),a}}function p(t,n){v[t]=h(t).concat(n)}function h(t){return v[t]||[]}function m(t){return d[t]=d[t]||o(e)}function w(t,n){f(t,function(t,e){n=n||"feature",y[e]=n,n in u||(u[n]=[])})}var v={},y={},g={on:p,emit:e,get:m,listeners:h,context:n,buffer:w,abort:a,aborted:!1};return g}function i(){return new r}function a(){(u.api||u.feature)&&(l.aborted=!0,u=l.backlog={})}var s="nr@context",c=t("gos"),f=t(15),u={},d={},l=n.exports=o();l.backlog=u},{}],gos:[function(t,n,e){function r(t,n,e){if(o.call(t,n))return t[n];var r=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(t,n,e){function r(t,n,e,r){o.buffer([t],r),o.emit(t,n,e)}var o=t("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(t,n,e){function r(t){var n=typeof t;return!t||"object"!==n&&"function"!==n?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");n.exports=r},{}],loader:[function(t,n,e){function r(){if(!x++){var t=b.info=NREUM.info,n=l.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&n))return u.abort();f(y,function(n,e){t[n]||(t[n]=e)}),c("mark",["onload",a()+b.offset],null,"api");var e=l.createElement("script");e.src="https://"+t.agent,n.parentNode.insertBefore(e,n)}}function o(){"complete"===l.readyState&&i()}function i(){c("mark",["domContent",a()+b.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(s=Math.max((new Date).getTime(),s))-b.offset}var s=(new Date).getTime(),c=t("handle"),f=t(15),u=t("ee"),d=window,l=d.document,p="addEventListener",h="attachEvent",m=d.XMLHttpRequest,w=m&&m.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:m,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1044.min.js"},g=m&&w&&w[p]&&!/CriOS/.test(navigator.userAgent),b=n.exports={offset:s,now:a,origin:v,features:{},xhrWrappable:g};t(12),l[p]?(l[p]("DOMContentLoaded",i,!1),d[p]("load",r,!1)):(l[h]("onreadystatechange",o),d[h]("onload",r)),c("mark",["firstbyte",s],null,"api");var x=0,E=t(17)},{}]},{},["loader",2,10,4,3]);</script>
    <link rel="canonical" href="http://www.simplesite.com/pages/privacy-policy.aspx" />
<link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
<link rel="manifest" href="/manifest.json">
<link rel="yandex-tableau-widget" href="/yandex-browser-manifest.json">


<link rel="stylesheet" type="text/css" media="all" href="/userpages/pages/CssForwarder.aspx?location=root&amp;style=frontpage.css&amp;version=1.0.0" />
<link rel="stylesheet" type="text/css" media="all" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=latin,latin-ext" />
<link rel="stylesheet" type="text/css" media="all" href="/c/css/cookiebar/jquery.cookiebar.css?_v=aece76b4b02c589719d276a7f15988be" />
<link rel="stylesheet" type="text/css" media="all" href="/c/css/seoblog.css?_v=d8c252ce19c7e3c7d0ed82cda6d22e45" />
<link rel="stylesheet" type="text/css" media="all" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="/c/less/subpage.css?_v=93a77825a5268c938a7efbf5ade8b46a" />
<link rel="stylesheet" type="text/css" media="all" href="/c/less/globalResponsive.css?_v=74527db7672c579daed4ec40b2e42f70" />
<!--[if lt IE 10]>
<link rel="stylesheet" type="text/css" media="all" href="/c/less/ie.css?_v=26434e37247d0aef0bf25406d85ee757" />
<![endif]-->

<script type="text/javascript" src="/c/js/jquery-1.10.2.min.js?_v=bdce12c949e78d570c8d44e9c2b23508"></script>
<script type="text/javascript" src="/c/js/jquery-migrate-1.2.1.min.js?_v=eb05d8d73b5b13d8d84308a4751ece96"></script>
<script type="text/javascript" src="/c/js/bootstrap/bootstrap-3-3-2.min.js?_v=046ba2b5f4cff7d2eaaa1af55caa9fd8"></script>
<script type="text/javascript" src="/c/js/global.js?_v=c8f3ae7cb2617166a7009f6d3db3df32"></script>
<script type="text/javascript" src="/c/js/cookiebar/jquery.cookiebar.js?_v=8538b21d9ddca75bbef283c278b118e4"></script>


</head>
<body>
    <form method="post" action="./privacy-policy.aspx" id="aspnetForm">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUJNjQ0MzI0MzgyDxYCHhNWYWxpZGF0ZVJlcXVlc3RNb2RlAgEWAmYPZBYEZg9kFgICAw9kFgICCA8PFgIeB1Zpc2libGVoZGQCAQ9kFgYCAQ9kFgZmDxYCHgtfIUl0ZW1Db3VudAIFFgpmD2QWAmYPFQMrLy93d3cuc2ltcGxlc2l0ZS5jb20vcGFnZXMvc3RhcnR3aXphcmQuYXNweAtzdGFydHdpemFyZBtNYWtlIGEgZnJlZSB3ZWJzaXRlIG9yIGJsb2dkAgEPZBYCZg8VAzAvL3d3dy5zaW1wbGVzaXRlLmNvbS9wYWdlcy9jdXN0b21lci1zZXJ2aWNlLmFzcHgPY3VzdG9tZXJzZXJ2aWNlEEN1c3RvbWVyIFNlcnZpY2VkAgIPZBYCZg8VAy0vL3d3dy5zaW1wbGVzaXRlLmNvbS9nby9jbXMvZmVhdHVyZXMvZmVhdHVyZXMIZmVhdHVyZXMLVGhlIFByb2R1Y3RkAgMPZBYCZg8VAzEvL3d3dy5zaW1wbGVzaXRlLmNvbS9nby9jbXMvaG93aXR3b3Jrcy9ob3dpdHdvcmtzCmhvd2l0d29ya3MMSG93IGl0IHdvcmtzZAIED2QWAmYPFQMbaHR0cDovL2Jsb2cuc2ltcGxlc2l0ZS5jb20vBGJsb2cIT3VyIEJsb2dkAgEPFgIfAgIBFgJmD2QWAmYPFQMaPGkgY2xhc3M9ImZhIGZhLWxvY2siPjwvaT4tLy93d3cuc2ltcGxlc2l0ZS5jb20vcGFnZXMvc2VydmljZS1sb2dpbi5hc3B4BkxvZyBpbmQCAg9kFgQCAQ8WAh8BZxYIAgEPDxYCHgRUZXh0BQdFbmdsaXNoZGQCAw8WAh8CAgYWDGYPZBYCZg8VAi0vL21zLnNpbXBsZXNpdGUuY29tL3BhZ2VzL3ByaXZhY3ktcG9saWN5LmFzcHgNQmFoYXNhIE1lbGF5dWQCAQ9kFgJmDxUCLi8vd3d3LnNpbXBsZXNpdGUuY29tL3BhZ2VzL3ByaXZhY3ktcG9saWN5LmFzcHguRW5nbGlzaCZuYnNwOyZuYnNwOzxpIGNsYXNzPSJmYSBmYS1jaGVjayI+PC9pPmQCAg9kFgJmDxUCLS8vZnIuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweAlGcmFuw6dhaXNkAgMPZBYCZg8VAi0vL25vLnNpbXBsZXNpdGUuY29tL3BhZ2VzL3ByaXZhY3ktcG9saWN5LmFzcHgNTm9yc2sgQm9rbcOlbGQCBA9kFgJmDxUCLS8vcHQuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweApQb3J0dWd1w6pzZAIFD2QWAmYPFQItLy9lbC5zaW1wbGVzaXRlLmNvbS9wYWdlcy9wcml2YWN5LXBvbGljeS5hc3B4EM6VzrvOu863zr3Ouc66zqxkAgUPFgIfAgIGFgxmD2QWAmYPFQItLy9jcy5zaW1wbGVzaXRlLmNvbS9wYWdlcy9wcml2YWN5LXBvbGljeS5hc3B4CcSMZcWhdGluYWQCAQ9kFgJmDxUCLS8vZGUuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweAdEZXV0c2NoZAICD2QWAmYPFQItLy9pZC5zaW1wbGVzaXRlLmNvbS9wYWdlcy9wcml2YWN5LXBvbGljeS5hc3B4CUluZG9uZXNpYWQCAw9kFgJmDxUCLS8vbmwuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweApOZWRlcmxhbmRzZAIED2QWAmYPFQItLy9maS5zaW1wbGVzaXRlLmNvbS9wYWdlcy9wcml2YWN5LXBvbGljeS5hc3B4BVN1b21pZAIFD2QWAmYPFQItLy90ci5zaW1wbGVzaXRlLmNvbS9wYWdlcy9wcml2YWN5LXBvbGljeS5hc3B4CFTDvHJrw6dlZAIHDxYCHwICBhYMZg9kFgJmDxUCLS8vZGEuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweAVEYW5za2QCAQ9kFgJmDxUCLS8vZXMuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweAhFc3Bhw7FvbGQCAg9kFgJmDxUCLS8vaXQuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweAhJdGFsaWFub2QCAw9kFgJmDxUCLS8vcGwuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweAZQb2xza2lkAgQPZBYCZg8VAi0vL3N2LnNpbXBsZXNpdGUuY29tL3BhZ2VzL3ByaXZhY3ktcG9saWN5LmFzcHgHU3ZlbnNrYWQCBQ9kFgJmDxUCLS8vcnUuc2ltcGxlc2l0ZS5jb20vcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweA7QoNGD0YHRgdC60LjQuWQCAw9kFgICAQ8PFgIfAwUHRW5nbGlzaGRkAgUPZBYCZg9kFgICAQ9kFgICAQ9kFgRmD2QWAmYPFgIfAgIFFgpmD2QWAmYPFQQAHC9wYWdlcy9jdXN0b21lci1zZXJ2aWNlLmFzcHgJaS0yNCBpXzEzEEN1c3RvbWVyIFNlcnZpY2VkAgEPZBYCZg8VBAAPL3BhZ2VzL2ZhcS5hc3B4CWktMjQgaV8xMgNGQVFkAgIPZBYCZg8VBAAYL3BhZ2VzL3Rlcm1zLW9mLXVzZS5hc3B4CWktMjQgaV8xMQxUZXJtcyBvZiBVc2VkAgMPZBYCZg8VBA8gY2xhc3M9ImFjdGl2ZSIaL3BhZ2VzL3ByaXZhY3ktcG9saWN5LmFzcHgJaS0yNCBpXzE0DlByaXZhY3kgUG9saWN5ZAIED2QWAmYPFQQAHC9wYWdlcy9naWZ0LWNlcnRpZmljYXRlLmFzcHgJaS0yNCBpXzA5EEdpZnQgQ2VydGlmaWNhdGVkAgEPZBYCZg8WAh8CAgUWCmYPZBYCZg8VBAAcL3BhZ2VzL2N1c3RvbWVyLXNlcnZpY2UuYXNweAlpLTQwIGlfMTMQQ3VzdG9tZXIgU2VydmljZWQCAQ9kFgJmDxUEAA8vcGFnZXMvZmFxLmFzcHgJaS00MCBpXzEyA0ZBUWQCAg9kFgJmDxUEABgvcGFnZXMvdGVybXMtb2YtdXNlLmFzcHgJaS00MCBpXzExDFRlcm1zIG9mIFVzZWQCAw9kFgJmDxUEDyBjbGFzcz0iYWN0aXZlIhovcGFnZXMvcHJpdmFjeS1wb2xpY3kuYXNweAlpLTQwIGlfMTQOUHJpdmFjeSBQb2xpY3lkAgQPZBYCZg8VBAAcL3BhZ2VzL2dpZnQtY2VydGlmaWNhdGUuYXNweAlpLTQwIGlfMDkQR2lmdCBDZXJ0aWZpY2F0ZWQCCQ9kFgRmDxYCHwICBhYMZg9kFgJmDxUCKy8vd3d3LnNpbXBsZXNpdGUuY29tL3BhZ2VzL3N0YXJ0d2l6YXJkLmFzcHgbTWFrZSBhIGZyZWUgd2Vic2l0ZSBvciBibG9nZAIBD2QWAmYPFQIwLy93d3cuc2ltcGxlc2l0ZS5jb20vcGFnZXMvY3VzdG9tZXItc2VydmljZS5hc3B4EEN1c3RvbWVyIFNlcnZpY2VkAgIPZBYCZg8VAi0vL3d3dy5zaW1wbGVzaXRlLmNvbS9nby9jbXMvZmVhdHVyZXMvZmVhdHVyZXMLVGhlIFByb2R1Y3RkAgMPZBYCZg8VAjEvL3d3dy5zaW1wbGVzaXRlLmNvbS9nby9jbXMvaG93aXR3b3Jrcy9ob3dpdHdvcmtzDEhvdyBpdCB3b3Jrc2QCBA9kFgJmDxUCG2h0dHA6Ly9ibG9nLnNpbXBsZXNpdGUuY29tLwhPdXIgQmxvZ2QCBQ9kFgJmDxUCAABkAgEPFgIfAgIGFgxmD2QWAmYPFQIUL3BhZ2VzL2Fib3V0LXVzLmFzcHgQQWJvdXQgU2ltcGxlU2l0ZWQCAQ9kFgJmDxUCEy9wYWdlcy9jYXJlZXJzLmFzcHgPV29ya2luZyB3aXRoIHVzZAICD2QWAmYPFQIcL2dvL2Ntcy9jb3Jwb3JhdGUvbWFuYWdlbWVudApNYW5hZ2VtZW50ZAIDD2QWAmYPFQIWL3BhZ2VzL2FmZmlsaWF0ZXMuYXNweAtQYXJ0bmVyc2hpcGQCBA9kFgJmDxUCFi9nby9jbXMvY29ycG9yYXRlL25ld3METmV3c2QCBQ9kFgJmDxUCFy9nby9jbXMvY29ycG9yYXRlL3ByZXNzBVByZXNzZGQXY8FyThCpXLIeolWaqkdaMdOZ+Q==" />
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="2B6C059C" />
</div>
        <div id="wrapper">
            

<div id="header" class="red-line">        
    <div class="container">
        <div class="headercontainer">
          <div id="logo"><a href="http://www.simplesite.com/">SimpleSite.com</a></div>
          <div id="main-menu"  class="hidden-phone">
            <ul class="menu">
                
                    <li><a href="//www.simplesite.com/pages/startwizard.aspx" id="startwizard">Make a free website or blog</a></li>
                
                    <li><a href="//www.simplesite.com/pages/customer-service.aspx" id="customerservice">Customer Service</a></li>
                
                    <li><a href="//www.simplesite.com/go/cms/features/features" id="features">The Product</a></li>
                
                    <li><a href="//www.simplesite.com/go/cms/howitworks/howitworks" id="howitworks">How it works</a></li>
                
                    <li><a href="http://blog.simplesite.com/" id="blog">Our Blog</a></li>
                
            </ul>
          </div>
          <div id="sub-menu">
            <ul class="menu right" style="float:left;">
               
                        <li><i class="fa fa-lock"></i><a href="//www.simplesite.com/pages/service-login.aspx">Log in </a></li>            
                     
            </ul>                  
            

<style>
    .fa-lock {
        padding-right: 10px;
    }

    .fa-check {
        color: #50ae55;
    }

    .caret {
        padding-bottom: 10px;
        color: #262626;
    }

    .lang-selector {
        color: #393939;
        float: right;
        margin-bottom: -5px;
    }

        .lang-selector .nav > li > .multi-column::after {
            top: -5px;
        }

    li.dropdown.open > #currentLang {
        border: 1px solid #e3e3e3;
        background-color: #f2f2f2;
        margin: -1px;
    }

    .multi-column li > a:hover, .multi-column li > a:focus, .dropdown-submenu:hover > a {
        background-image: linear-gradient(to bottom, #f2f2f2, #F1E9E8);
    }

    .multi-column {
        min-width: 500px;
        padding-left: 20px;
        margin-top: 10px;
        font-size: 15px;
        border-radius: 5px;
        font-family: "Open Sans", sans-serif;
    }

    .column-drop {
        float: left;
        width: 33.33333333%;
        position: relative;
        min-height: 1px;
        box-sizing: border-box;
    }

    .multi-column-dropdown {
        list-style: none;
        margin: 0px;
    }

        .multi-column-dropdown li a:hover {
            text-decoration: none;
            color: #262626;
            background-color: #f5f5f5;
        }

    #sub-menu {
        float: left;
        clear: none;
        max-width: 350px;
        height: 50px;
    }

    #lang {
        padding-left: 20px;
    }

    #currentLang {
        padding-bottom: 13px;
        padding-top: 15px;
        font-size: 15px;
        font-family: "Open Sans", sans-serif;
        color: #393939;
    }

    @media (max-width: 768px) {
        #currentLang {
            padding-top: 20px;
            padding-bottom: 8px;
        }

        .multi-column {
            min-width: 400px;
            padding-left: 0px;
        }
    }

    @media (min-width: 768px) and (max-width: 979px) {
        .navbar .nav > li > a {
            padding-left: 5px;
            padding-right: 5px;
        }

        #header .container ul.menu li a {
            padding-right: 10px;
        }
    }

    @media (max-width: 480px) {
        .lang-selector {
            /*display: none;*/
        }

            .lang-selector .row {
                min-width: 200px;
            }

            .lang-selector .column-drop {
                width: 100%;
            }
    }
</style>


    <div class="navbar lang-selector">
        <ul class="nav">
            <li class="dropdown">
                <a href="#" id="currentLang" class="dropdown-toggle" data-toggle="dropdown">
                    <span id="frame_siteHeader_usrStandardSiteHeaderLangSelector_lblCurrentLang">English</span>
                    <b class="caret"></b></a>
                <ul class="row dropdown-menu pull-right multi-column">
                    <li class="column-drop">
                        <ul class="multi-column-dropdown">
                            
                                    <li><a id="lang" href="//ms.simplesite.com/pages/privacy-policy.aspx">Bahasa Melayu</a></li>
                                
                                    <li><a id="lang" href="//www.simplesite.com/pages/privacy-policy.aspx">English&nbsp;&nbsp;<i class="fa fa-check"></i></a></li>
                                
                                    <li><a id="lang" href="//fr.simplesite.com/pages/privacy-policy.aspx">Français</a></li>
                                
                                    <li><a id="lang" href="//no.simplesite.com/pages/privacy-policy.aspx">Norsk Bokmål</a></li>
                                
                                    <li><a id="lang" href="//pt.simplesite.com/pages/privacy-policy.aspx">Português</a></li>
                                
                                    <li><a id="lang" href="//el.simplesite.com/pages/privacy-policy.aspx">Ελληνικά</a></li>
                                
                        </ul>
                    </li>
                    <li class="column-drop">
                        <ul class="multi-column-dropdown">
                            
                                    <li><a id="lang" href="//cs.simplesite.com/pages/privacy-policy.aspx">Čeština</a></li>
                                
                                    <li><a id="lang" href="//de.simplesite.com/pages/privacy-policy.aspx">Deutsch</a></li>
                                
                                    <li><a id="lang" href="//id.simplesite.com/pages/privacy-policy.aspx">Indonesia</a></li>
                                
                                    <li><a id="lang" href="//nl.simplesite.com/pages/privacy-policy.aspx">Nederlands</a></li>
                                
                                    <li><a id="lang" href="//fi.simplesite.com/pages/privacy-policy.aspx">Suomi</a></li>
                                
                                    <li><a id="lang" href="//tr.simplesite.com/pages/privacy-policy.aspx">Türkçe</a></li>
                                
                        </ul>
                    </li>
                    <li class="column-drop">
                        <ul class="multi-column-dropdown">
                            
                                    <li><a id="lang" href="//da.simplesite.com/pages/privacy-policy.aspx">Dansk</a></li>
                                
                                    <li><a id="lang" href="//es.simplesite.com/pages/privacy-policy.aspx">Español</a></li>
                                
                                    <li><a id="lang" href="//it.simplesite.com/pages/privacy-policy.aspx">Italiano</a></li>
                                
                                    <li><a id="lang" href="//pl.simplesite.com/pages/privacy-policy.aspx">Polski</a></li>
                                
                                    <li><a id="lang" href="//sv.simplesite.com/pages/privacy-policy.aspx">Svenska</a></li>
                                
                                    <li><a id="lang" href="//ru.simplesite.com/pages/privacy-policy.aspx">Русский</a></li>
                                
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>




                     
          </div>
        </div> 
    </div>
</div>
            
            
       
<div id="body">
    
    <div id="menu-mini" class="visible-phone">
      <div class="container">
        <a href="#" class="select-page collapsed" data-toggle="collapse" data-target="#demo">Select page<b class="caret"></b></a>
        <div id="demo" class="collapse">
            
<ul role="menu" aria-labelledby="dLabel" class="">
    
            <li><a href="/pages/customer-service.aspx"><i class="i-24 i_13"></i>Customer Service</a></li>
        
            <li><a href="/pages/faq.aspx"><i class="i-24 i_12"></i>FAQ</a></li>
        
            <li><a href="/pages/terms-of-use.aspx"><i class="i-24 i_11"></i>Terms of Use</a></li>
        
            <li class="active"><a href="/pages/privacy-policy.aspx"><i class="i-24 i_14"></i>Privacy Policy</a></li>
        
            <li><a href="/pages/gift-certificate.aspx"><i class="i-24 i_09"></i>Gift Certificate</a></li>
        
</ul>
        </div>
      </div>
    </div>
    <div id="tab-menu" class="sub" style="padding-bottom: 25px;">
      <div class="container">
        <div class="row">
          <div class="span4 hidden-phone">
            
<ul id="tab-menu_tabs" class="font-mini">
    
            <li><a href="/pages/customer-service.aspx"><i class="i-40 i_13"></i>Customer Service</a></li>
        
            <li><a href="/pages/faq.aspx"><i class="i-40 i_12"></i>FAQ</a></li>
        
            <li><a href="/pages/terms-of-use.aspx"><i class="i-40 i_11"></i>Terms of Use</a></li>
        
            <li class="active"><a href="/pages/privacy-policy.aspx"><i class="i-40 i_14"></i>Privacy Policy</a></li>
        
            <li><a href="/pages/gift-certificate.aspx"><i class="i-40 i_09"></i>Gift Certificate</a></li>
        
</ul>
          </div>
          <div class="span8">
            <div class="tab-content">
              <div class="tab-pane active" id="tab_01">
                <h4 style="color: #b72026;"><i class="i-32 i-40 i_14 visible-phone"></i>Privacy Policy</h4>
                <div class="image"><img src="/cms/common/images/How_It_Works/Photo-How-to.jpg" alt="our terms and conditions"></div>
          <div><h1>Privacy <strong>policy</strong></strong></h1> 
                <p class="first"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="content-block">
      <div class="container">
        <div class="group">
          <div class="row-fluid">
            <div class="block-content">
            <p>This privacy policy has been written to better serve those who are concerned with how their personal information is being used by SimpleSite. This information can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how SimpleSite collects, uses, protects or otherwise handles your personal information in accordance with our website. </p>

<h3>What personal information do we collect from the people that visit SimpleSite? </h3>

<p>When registering for a website or purchasing a website subscription on our site, as appropriate, you may be asked to enter your name, email address, mailing address, credit card information or other details to help you along with your experience. </p>

<h3>When do we collect information? </h3>

<p>We collect information from you when you register on our site or enter information to buy or extend a SimpleSite subscription on our site. </p>

<h3>How do we use your information? </h3>

<p>We may use the information we collect from you when you register, make a purchase, respond to marketing communication, browse the website, or use certain other site features in the following ways:
<ul><li>To improve our website in order to better serve you.</li>
<li>To allow us to better service you in responding to your customer service requests.</li>
<li>To quickly process your transactions.</li>
<li>To send periodic emails regarding your order or other products and services. </li></ul></p>

<h3>How do we protect visitor information? </h3>

<p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our website as safe as possible. </p>
<p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems. These individuals are required to keep this information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. </p>

<p>We implement a variety of security measures when a user purchases a subscription, submits, or accesses their information to maintain the safety of your personal information. </p>

<p>All transactions are processed through a gateway provider and are not stored or processed on our servers. </p>

<h3>Do we use 'cookies'? </h3>

<p>Yes. Cookies are small pieces of data sent from a website and stored in your web browser while you are browsing that website. Every time the website is loaded, the browser sends the cookie back to the server to notify the website of your previous activity. They are used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. Cookies also help compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future. </p>

<p> SimpleSite uses cookies to:
<ul><li>Understand and save user's preferences for future visits.</li>
<li>Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third party services that track this information on our behalf. </li></ul></p>

If you disable cookies, some features will be disabled. Please note this may affect the user experience that makes your site experience more efficient, such as our log-in function. </p>

<h3>Third party disclosure</h3>

<p>Third parties are used to facilitate our business, such as to host the service at a co-location facility for servers and to process payments for SimpleSite’a products and services. </p>

<p>We do not sell your personal information to third parties, but we may retrieve, save, share and make any use of the information for analysis or statistics, all of which is compatible with providing our services to you. All personal information is handled in accordance with local data and privacy laws. SimpleSite makes use of Google Analytics; their privacy policy can be found <a href="http://www.google.com/analytics/privacyoverview.html/">here</a>. </p>

<p>Personally identifiable information may be shared with third parties when they offer directly relevant services for your SimpleSite website, such as the registration and hosting of your domain name. SimpleSite makes use of Ascio Technologies Inc. for the hosting of your domain name; their privacy policy can be found <a href="http://www.ascio.com/privacy-policy">here</a>. </p>

<p>Non-personally identifiable visitor information may also be provided to other parties for marketing, advertising, or other uses. </p>

<h3>Third party links</h3>

<p>Occasionally, at our discretion, we may include links to third party services on our website. These third party sites have separate and independent privacy policies. SimpleSite therefore has no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites should you deem this necessary. </p>

<h3>ICANN regulations regarding domain registration</h3>

<p><a href="https://www.icann.org/registrar-reports/accredited-list.html" target="_blank">ICANN-Accredited Registrars</a></p>

<p><a href="https://www.icann.org/resources/pages/educational-2012-02-25-en" target="_blank">Registrant Educational Materials</a></p>

<p><a href="https://www.icann.org/resources/pages/benefits-2013-09-16-en" target="_blank">Registrants' Benefits and Responsibilities</a></p>

<p>Policies, Registration Agreements and Terms for all types of domains can be found at <a href="http://www.ascio.com/policies" target="_blank">www.ascio.com/policies</a></p>

<h3>Contacting Us</h3>

<p>If there are any questions regarding this privacy policy you may contact us using the information below. </p>
<p> <strong>SimpleSite ApS</strong></br>
Toldbodgade 31, 3. tv</br>
1253 Copenhagen</br>
Denmark</br>
VAT.no: 27 97 24 54</br>
Email: <a href="mailto:customerservice@simplesite.com">customerservice@simplesite.com</a></br>
Fax (+45) 33 32 17 14 </p>



    		</div>
          </div>
        </div>
      </div>
    </div>
</div>
    
            
            <div id="footer-placeholder"></div>
            
        </div>
        
<div id="footer">    
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="span12">
          <div class="row">
            <div class="span8">
              <div class="row">
                <div class="span3">
                  <div class="top-block hidden-phone">
                    <h4>Build your website</h4>
                    <ul>
                    
                            <li><a href="//www.simplesite.com/pages/startwizard.aspx">Make a free website or blog</a></li>
                        
                            <li><a href="//www.simplesite.com/pages/customer-service.aspx">Customer Service</a></li>
                        
                            <li><a href="//www.simplesite.com/go/cms/features/features">The Product</a></li>
                        
                            <li><a href="//www.simplesite.com/go/cms/howitworks/howitworks">How it works</a></li>
                        
                            <li><a href="http://blog.simplesite.com/">Our Blog</a></li>
                        
                            <li><a href=""></a></li>
                        
                    </ul>
                  </div>
                </div>
                <div class="span3">
                  <div class="top-block">
                    <h4>About SimpleSite</h4>
                    <ul>
                    
                            <li><a href="/pages/about-us.aspx">About SimpleSite</a></li>
                        
                            <li><a href="/pages/careers.aspx">Working with us</a></li>
                        
                            <li><a href="/go/cms/corporate/management">Management</a></li>
                        
                            <li><a href="/pages/affiliates.aspx">Partnership</a></li>
                        
                            <li><a href="/go/cms/corporate/news">News</a></li>
                        
                            <li><a href="/go/cms/corporate/press">Press</a></li>
                        
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="span4 hidden-phone footerLeftDiv">
              <div class="right-block">
                <a href="/pages/startwizard.aspx" class="btn btn-large btn-block">Create a free website</a>
                <ul>
                  <li><a href="/pages/startwizard.aspx"></a></li> 
                </ul>
              </div>
            </div>
            <a class="redherring-logo"><img src="/cms/common/frontpage/red_herring_logo_global_110x110.gif"></a>
            <a class="nordicgrowthhackers-logo" href="http://nordicgrowthhackers.com/" target="_blank"><img src="/cms/common/frontpage/logo-footer-simplesite.png"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
    </form>
    <!-- GTM dataLayer -->
<script>
    dataLayer =  [{"SiteVer":"US","Language":"en","Culture":"en-US","Instrumentation":"UnDef","PreOrFre":"UnDef","RecognizedCustomer":"False","Market":"US","MainOrUser":"MainSite"}];
</script>
<!-- End GTM dataLayer -->
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-2MMH"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-2MMH');</script>
<!-- End Google Tag Manager -->
</body>
</html>
