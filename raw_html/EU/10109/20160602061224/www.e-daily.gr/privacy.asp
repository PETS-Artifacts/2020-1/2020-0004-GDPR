
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>E-Daily.gr Πολιτική Απορρήτου</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="charset" content="utf-8"/>
<meta http-equiv="Refresh" content="900"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta content="Πολιτική Απορρήτου" name="Description"/>
<meta content="" name="keywords"/>
<meta property="og:title" content="E-Daily.gr Πολιτική Απορρήτου"/>
<meta property="og:description" content=""/>
<meta property="og:site_name" content="E-Daily.gr"/>
<meta property="og:type" content="Website"/>
<meta property="og:image" content="http://www.e-daily.gr/_img/fblogo.png"/>
<meta property="fb:app_id" content="1590239487856378"/>
<meta property="og:url" content="http://www.e-daily.gr"/>
<link rel="canonical" href="http://www.e-daily.gr"/>
<link rel="stylesheet" href="/_css/post.css"/>
<link rel="stylesheet" href="/_js/scrollbar/jquery.mCustomScrollbar.css"/>
<meta name="apple-itunes-app" content="app-id=776881885">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="/_ico/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/_ico/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/_ico/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/_ico/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/_ico/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/_ico/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/_ico/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/_ico/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/_ico/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/_ico/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/_ico/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/_ico/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/_ico/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/_ico/manifest.json">
<link rel="mask-icon" href="/_ico/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/_ico/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/_ico/mstile-144x144.png">
<meta name="msapplication-config" content="/_ico/browserconfig.xml">
<meta name="theme-color" content="#ff0004">
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,400italic|Roboto+Condensed:300,400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href="/_css/style.css" rel="stylesheet" type="text/css"/>
<link href="/_css/navigation.css" rel="stylesheet" type="text/css"/>
<link href="/_js/menu/mgmenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/mobilemenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/structure.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/_img/forecastfont/weather.css"/>
<link rel="stylesheet" href="/_img/edailyicons/css/edaily.css">
<link rel="stylesheet" href="/_img/edailyicons/css/animation.css"><!--[if IE 7]><link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->
<link rel="stylesheet" type="text/css" href="/_js/mmenu/jquery.ma.infinitypush.css"/>
<script language="javaScript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script language="javaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/1.0.1/lazysizes.min.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/autocomplete/jquery.autocomplete.min.js"></script>
<script language="javaScript" type="text/javascript" src="http://www.e-radio.gr/cache/mediadata_1.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.popupWindow.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.socialstats.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/menu/mgmenu_plugins.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/menu/mgmenu.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/mmenu/jquery.ma.infinitypush.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/tools_all.js"></script>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
<script type='text/javascript'>
if(document.location.protocol=='http:'){
var Tynt=Tynt||[];Tynt.push('d91U86Bp0r5zpGrkHcnlKl');
Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false,"domain":"e-daily.gr"};
(function(){var h,s=document.createElement('script');s.src='http://cdn.tynt.com/ti.js';
h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();}
</script>
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"http://www.e-daily.gr/privacy.asp","theme":"light-bottom"};
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dt/common/DAC.js"></script>
<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dac/1370.1/w1122524.js"></script>
<script type="text/javascript" id="ean-native-embed-tag" src="//cdn.elasticad.net/native/serve/js/nativeEmbed.gz.js"></script>
<script src="/_js/tabs/jquery.easytabs.js" type="text/javascript"></script>
<script src="/_js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body>
<script type="text/javascript">
var nuggprof ="";
var nuggrid= encodeURIComponent(top.location.href);
var nuggtg= encodeURIComponent("ContentCategory");
document.write('<scr'+'ipt type=\"text/javascript\" src=\"http://adweb.nuggad.net/rc?nuggn=1230610253&nuggsid=113401978&nuggrid='+nuggrid+'&nuggtg='+nuggtg+'"><\/scr'+'ipt>');
</script>
<div id="wrapper">
<nav id="mobilemenu_pad">
<ul class="sections">
<li><a href="/news">Ειδήσεις</a>
<li><a href="/viral">Viral</a>
<li><a href="/quiz">Quiz</a>
<li><a href="/life">Life</a>
<li><a href="/themata">Θέματα</a>
<li><a href="/entertainment">Ψυχαγωγία</a>
<li><a href="/agenda">E-Agenda</a>
</ul>
<ul class="sections badges">
<li><a href="/omg"><img src="/_img/feeds/101.png" border="0"/> OMG</a>
<li><a href="/lol"><img src="/_img/feeds/106.png" border="0"/> LOL</a>
<li><a href="/woman"><img src="/_img/feeds/112.png" border="0"/> Woman</a>
<li><a href="/alist"><img src="/_img/feeds/91.png" border="0"/> A-List</a>
<li><a href="/cute"><img src="/_img/feeds/109.png" border="0"/> Cute</a>
<li><a href="/retro"><img src="/_img/feeds/110.png" border="0"/> Retro</a>
<li><a href="/juicy"><img src="/_img/feeds/104.png" border="0"/> Juicy</a>
<li><a href="/lgbt"><img src="/_img/feeds/55.png" border="0"/> LGBT</a>
<li><a href="/geeky"><img src="/_img/feeds/108.png" border="0"/> Geeky</a>
<li><a href="/win"><img src="/_img/feeds/111.png" border="0"/> Win</a>
<li><a href="/summer"><img src="/_img/feeds/15.png" border="0"/> Summer</a>
<li><a href="/xmas"><img src="/_img/feeds/14.png" border="0"/> Xmas</a>
<li><a href="/summer"><img src="/_img/feeds/12.png" border="0"/> Easter</a>
</ul>
<ul class="sections topics">
<li><a href="/food">Food</a>
<li><a href="/bodymind">Body+Mind</a>
<li><a href="/travel">Ταξίδια</a>
<li><a href="/style">Style</a>
<li><a href="/spiti">Σπίτι</a>
<li><a href="/family">Family</a>
<li><a href="/sxeseis">Σχέσεις</a>
</ul>
<ul class="sections topics">
<li><a href="/agenda">Agenda</a>
<li><a href="/music">Μουσική</a>
<li><a href="/theater">Θέατρο</a>
<li><a href="/arts">Εικαστικά</a>
<li><a href="/cinema">Σινεμά</a>
<li><a href="/nightlife">Νάιτ Λάιφ</a>
<li><a href="/city">Πόλη</a>
</ul>
<ul class="sections topics">
<li><a href="/weather">Καιρός</a>
</ul>
<div class="social">
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</nav>
<div id="main">
<form id="skinform"></form>
<div id="spider"></div>
<div id="topHeaderWarper">
<div id="topHeader">
<div id="topHeaderA">
<a href="/" class="mainlogo"><img src="/_img/edailygr_beta_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία"/></a>
</div>
<div id="topHeaderB" class="feedIcons">
<a href="/win"><img src="/_img/feeds/111.png" border="0" alt="Win Feed"/></a>
<a href="/woman"><img src="/_img/feeds/112.png" border="0" alt="Woman Feed"/></a>
<a href="/omg"><img src="/_img/feeds/101.png" border="0" alt="OMG Feed"/></a>
<a href="/lol"><img src="/_img/feeds/106.png" border="0" alt="LOL Feed"/></a>
<a href="/juicy"><img src="/_img/feeds/104.png" border="0" alt="Juicy Feed"/></a>
<a href="/alist"><img src="/_img/feeds/91.png" border="0" alt="A-List Feed"/></a>
<a href="/cute"><img src="/_img/feeds/109.png" border="0" alt="Cute Feed"/></a>
<a href="/retro"><img src="/_img/feeds/110.png" border="0" alt="Retro Feed"/></a>
<a href="/lgbt"><img src="/_img/feeds/55.png" border="0" alt="LGBT Feed"/></a>
</div>
</div>
</div>
<div id="topHeaderWarper2">
<div id="mainmenu" class="mgmenu_container">
<ul class="mgmenu">
<li class="mgmenu_button"></li> 
<div id="mainmenu_logo"><a href="/"><img src="/_img/edailygr_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία" height="33"/></a></div>
<li><span class="mgmenu_drop"><a href="/news">Ειδήσεις</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/news/58169/o-karanikas-kai-syntonisths-episthmonwn-gia-thn-aktinovolia-apo-kinhta" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΙΤΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ο Καρανίκας και «συντονιστής επιστημόνων» για την ακτινοβολία από κινητά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/karanikas24.jpg" alt="Ο Καρανίκας και «συντονιστής επιστημόνων» για την ακτινοβολία από κινητά" border="0" class="cropimg lazyload">
</a>
<a href="/news/58168/sygklonistiko-h-stigmh-ths-katarreyshs-pentaorofoy-ktirioy-sth-sivhria-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Συγκλονιστικό: Η στιγμή της κατάρρευσης πενταόροφου κτιρίου στη Σιβηρία</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ktiriokatarrei.jpg" alt="Συγκλονιστικό: Η στιγμή της κατάρρευσης πενταόροφου κτιρίου στη Σιβηρία" border="0" class="cropimg lazyload">
</a>
<a href="/news/58166/katatethhkan-oi-tropologies-deite-tis-allages-gia-ekas-enfia-kai-enstoloys" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΟΙΚΟΝΟΜΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Κατατέθηκαν οι τροπολογίες: Δείτε τις αλλαγές για ΕΚΑΣ, ΕΝΦΙΑ και ένστολους</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tropologiesekas.jpg" alt="Κατατέθηκαν οι τροπολογίες: Δείτε τις αλλαγές για ΕΚΑΣ, ΕΝΦΙΑ και ένστολους" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3">
<div class="str8">
<a href="/news" class="more">ΠΕΡΙΣΣΟΤΕΡΕΣ ΕΙΔΗΣΕΙΣ</a>
<a href="/news/58165/parathyraki-amnhsteyshs-ypoyrgwn-me-offshore-xwris-poinikes-eythynes" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΙΤΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">«Παραθυράκι» αμνήστευσης υπουργών με offshore χωρίς ποινικές ευθύνες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tsiprasmilaei.jpg" alt="«Παραθυράκι» αμνήστευσης υπουργών με offshore χωρίς ποινικές ευθύνες" border="0" class="cropimg lazyload">
</a>
<a href="/news/58164/tritokosmikes-eikones-kata-th-dianomh-trofimwn-sth-deth-me-lipothymies-kai-entash" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τριτοκοσμικές εικόνες κατά τη διανομή τροφίμων στη ΔΕΘ με λιποθυμίες και ένταση</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dikaiouxoidethsitisi.jpg" alt="Τριτοκοσμικές εικόνες κατά τη διανομή τροφίμων στη ΔΕΘ με λιποθυμίες και ένταση" border="0" class="cropimg lazyload">
</a>
<a href="/news/58163/xaos-sto-hotspot-ths-morias-metanastes-evalan-fwtia-meta-apo-kavga-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Χάος στο hotspot της Μόριας: Μετανάστες έβαλαν φωτιά μετά από καβγά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/lesvokauga.jpg" alt="Χάος στο hotspot της Μόριας: Μετανάστες έβαλαν φωτιά μετά από καβγά" border="0" class="cropimg lazyload">
</a>
<a href="/news/58162/megales-ekplhkseis-gia-tis-vaseis-toy-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Μεγάλες εκπλήξεις για τις βάσεις του 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/panelinies2016.jpg" alt="Μεγάλες εκπλήξεις για τις βάσεις του 2016" border="0" class="cropimg lazyload">
</a>
<a href="/news/58161/voles-zwhs-kata-tsipra-h-tropologia-gia-tis-offshore-eksyphretei-politikoys-filoys" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΙΤΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Βολές Ζωής κατά Τσίπρα: Η τροπολογία για τις offshore εξυπηρετεί πολιτικούς φίλους</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zoikonstatn.jpg" alt="Βολές Ζωής κατά Τσίπρα: Η τροπολογία για τις offshore εξυπηρετεί πολιτικούς φίλους" border="0" class="cropimg lazyload">
</a>
</div>
</div>
<div class="col_12 inlineMenu">
<a href="/news">ΤΕΛΕΥΤΑΙΑ ΝΕΑ</a>
<a href="/ellada">ΕΛΛΑΔΑ</a>
<a href="/kosmos">ΚΟΣΜΟΣ</a>
<a href="/politiki">ΠΟΛΙΤΙΚΗ</a>
<a href="/oikonomia">ΟΙΚΟΝΟΜΙΑ</a>
<a href="/sports">ΑΘΛΗΤΙΣΜΟΣ</a>
<a href="/epistimi">ΕΠΙΣΤΗΜΗ</a>
<a href="/politismos">ΠΟΛΙΤΙΣΜΟΣ</a>
<a href="http://feeds.feedburner.com/eradioblog" target="_blank">RSS FEED</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/themata">Θέματα</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/themata/58167/ti-doyleia-kanei-shmera-o-prwtos-twn-prwtwn-stis-panelladikes-toy-1990-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Τι δουλειά κάνει σήμερα ο πρώτος των πρώτων στις Πανελλαδικές του 1990;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/psistariasroiter.jpg" alt="Τι δουλειά κάνει σήμερα ο πρώτος των πρώτων στις Πανελλαδικές του 1990;" border="0" class="cropimg lazyload">
</a>
<a href="/themata/58101/to-mple-kommati-ths-gomolastixas-exei-allh-xrhsh-apo-ayth-poy-pisteyame" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΑΦΙΕΡΩΜΑΤΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το μπλε κομμάτι της γομολάστιχας έχει άλλη χρήση από αυτή που πιστεύαμε</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sbistra.jpg" alt="Το μπλε κομμάτι της γομολάστιχας έχει άλλη χρήση από αυτή που πιστεύαμε" border="0" class="cropimg lazyload">
</a>
<a href="/themata/58133/sto-eswteriko-ths-masonikhs-stoas-ths-athhnas-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Στο εσωτερικό της μασονικής στοάς της Αθήνας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tektoniki_stoa_athinas_main.jpg" alt="Στο εσωτερικό της μασονικής στοάς της Αθήνας" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΑ ΘΕΜΑΤΑ</a>
<a href="/themata/58100/san-shmera-o-panathhnaikos-ston-teliko-toy-kypelloy-prwtathlhtriwn-pics-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΙΣΤΟΡΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-picture"></span></div>
<div class="menuItem_title">Σαν σήμερα: Ο Παναθηναϊκός στον τελικό του κυπέλλου Πρωταθλητριών</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/panathinaikos_wembley_main.jpg" alt="Σαν σήμερα: Ο Παναθηναϊκός στον τελικό του κυπέλλου Πρωταθλητριών" border="0" class="cropimg lazyload">
</a>
<a href="/themata/57666/ekanan-kariera-me-ksenes-fwnes-to-skandalo-poy-sygklonise-th-pop-moysikh-to-1990-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Έκαναν καριέρα με ξένες φωνές: Το σκάνδαλο που συγκλόνισε τη ποπ μουσική το 1990</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/millivanilli.jpg" alt="Έκαναν καριέρα με ξένες φωνές: Το σκάνδαλο που συγκλόνισε τη ποπ μουσική το 1990" border="0" class="cropimg lazyload">
</a>
<a href="/themata/57914/8-merh-ston-kosmo-poy-apagoreyetai-na-pane-gynaikes-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">8 μέρη στον κόσμο που απαγορεύεται να πάνε γυναίκες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/No-women-sign-014.jpg" alt="8 μέρη στον κόσμο που απαγορεύεται να πάνε γυναίκες" border="0" class="cropimg lazyload">
</a>
<a href="/themata/58125/h-limnh-poy-moiazei-me-tsixlofoyska-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΕΡΙΒΑΛΛΟΝ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Η λίμνη που μοιάζει με τσιχλόφουσκα!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/p1ak1p289ar10k7s60p1t7gpb5d_944x449.jpg" alt="Η λίμνη που μοιάζει με τσιχλόφουσκα!" border="0" class="cropimg lazyload">
</a>
<a href="/themata/57883/giati-ta-agalmata-twn-arxaiwn-ellhnwn-eixan-mikro-peos" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΙΣΤΟΡΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιατί τα αγάλματα των Αρχαίων Ελλήνων είχαν μικρό πeoς;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/greekagam1.jpg" alt="Γιατί τα αγάλματα των Αρχαίων Ελλήνων είχαν μικρό πeoς;" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
<a href="/afieromata">ΑΦΙΕΡΩΜΑΤΑ</a>
<a href="/eikones">ΩΡΑΙΕΣ ΕΙΚΟΝΕΣ</a>
<a href="/istorika">ΙΣΤΟΡΙΚΑ</a>
<a href="/gay-lesbian">LGBT</a>
<a href="/tech">ΤΕΧΝΟΛΟΓΙΑ</a>
<a href="/periballon">ΠΕΡΙΒΑΛΛΟΝ</a>
<a href="/architect">ΑΡΧΙΤΕΚΤΟΝΙΚΗ</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/viral">Viral</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/viral/58115/otan-h-logikh-den-leitoyrgei-kai-h-patenta-einai-monodromos-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΧΙΟΥΜΟΡ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Όταν η λογική δεν λειτουργεί και η πατέντα είναι μονόδρομος</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/patentes_main.jpg" alt="Όταν η λογική δεν λειτουργεί και η πατέντα είναι μονόδρομος" border="0" class="cropimg lazyload">
</a>
<a href="/viral/57919/10-apo-tis-pio-tromaktikes-pthseis-olwn-twn-epoxwn-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">10 από τις πιο τρομακτικές πτήσεις όλων των εποχών</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/planes-892432.jpg" alt="10 από τις πιο τρομακτικές πτήσεις όλων των εποχών" border="0" class="cropimg lazyload">
</a>
<a href="/viral/57738/h-pio-seksi-gymnastria-toy-instagram-den-fovatai-na-deiksei-ta-kallh-ths-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>SEXY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Η πιο σeξι γυμνάστρια του Ιnstagram δεν φοβάται να δείξει τα κάλλη της</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/casidaviscover2.jpg" alt="Η πιο σeξι γυμνάστρια του Ιnstagram δεν φοβάται να δείξει τα κάλλη της" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΟ VIRAL</a>
<a href="/viral/58098/sharon-stone-epeteios-24-etwn-apo-th-thrylikh-skhnh-sto-basic-instinct-pics-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΑΦΙΕΡΩΜΑΤΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-picture"></span></div>
<div class="menuItem_title">Sharon Stone: Επέτειος 24 ετών από τη θρυλική σκηνή στο Basic Instinct</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/saron5.jpg" alt="Sharon Stone: Επέτειος 24 ετών από τη θρυλική σκηνή στο Basic Instinct" border="0" class="cropimg lazyload">
</a>
<a href="/viral/58086/oi-panemorfes-syntrofoi-podosfairistwn-poy-tha-vrethoyn-sto-euro-2016-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>SEXY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Οι πανέμορφες σύντροφοι ποδοσφαιριστών που θα βρεθούν στο EURO 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/wags_main.jpg" alt="Οι πανέμορφες σύντροφοι ποδοσφαιριστών  που θα βρεθούν στο EURO 2016" border="0" class="cropimg lazyload">
</a>
<a href="/viral/58102/athhna-wnash-thelei-na-valei-telos-sth-zwh-ths-symfwna-me-dhmosieymata-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Αθηνά Ωνάση: Θέλει να βάλει τέλος στη ζωή της σύμφωνα με δημοσιεύματα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/athina-onasi-34535.jpg" alt="Αθηνά Ωνάση: Θέλει να βάλει τέλος στη ζωή της σύμφωνα με δημοσιεύματα" border="0" class="cropimg lazyload">
</a>
<a href="/viral/58092/fwnes-kai-apeiles-apo-paroysiastria-giati-den-thn-evalan-prwto-trapezi-ston-royva-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Φωνές και απειλές από παρουσιάστρια γιατί δεν την έβαλαν πρώτο τραπέζι στον Ρουβά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/rouvas-akrotiri-1.jpg" alt="Φωνές και απειλές από παρουσιάστρια γιατί δεν την έβαλαν πρώτο τραπέζι στον Ρουβά" border="0" class="cropimg lazyload">
</a>
<a href="/viral/58061/poia-anerxomenh-ellhnida-tragoydistria-kseperase-vandh-kai-vissh-sto-youtube-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Ποια ανερχόμενη Ελληνίδα τραγουδίστρια ξεπέρασε Βανδή και Βίσση στο Υoutube</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/malou42.jpg" alt="Ποια ανερχόμενη Ελληνίδα τραγουδίστρια ξεπέρασε Βανδή και Βίσση στο Υoutube" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
<a href="/omg">OMG</a>
<a href="/lol">LOL</a>
<a href="/tabloid">TABLOID</a>
<a href="/sexy">SEXY</a>
<a href="/zoa">ΖΩΑ</a>
<a href="/media">MEDIA</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/quiz">Κουίζ</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/quiz/58146/mporeite-na-deite-to-krymmeno-gramma-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείτε να δείτε το κρυμμένο γράμμα!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/f14945da-ced4-4446-8cb7-2baae1999fe1.jpg" alt="Μπορείτε να δείτε το κρυμμένο γράμμα!" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/58118/poy-kryvetai-h-arkoyda-sthn-fwtografia-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Που κρύβεται η αρκούδα στην φωτογραφία;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/find_bear_main.jpg" alt="Που κρύβεται η αρκούδα στην φωτογραφία;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/57915/vres-thn-tainia-apo-thn-fwtografia-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Βρες την ταινία από την φωτογραφία!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/3.jpg" alt="Βρες την ταινία από την φωτογραφία!" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΑ QUIZ</a>
<a href="http://www.e-daily.gr/quiz/40339/poio-ellhniko-sirial-perigrafei-kalytera-th-zwh-soy-quiz" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ποιο ελληνικό σίριαλ περιγράφει καλύτερα τη ζωή σου;</div>
<img data-src="http://cdn.e-daily.gr/repository/places/sirial2.jpg" alt="Ποιο ελληνικό σίριαλ περιγράφει καλύτερα τη ζωή σου;" border="0" class="cropimg lazyload">
</a>
<a href="http://www.e-daily.gr/quiz/57974/poso-kala-thymasai-ta-pagwta-ths-paidikhs-soy-hlikias-quiz" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Πόσο καλά θυμάσαι τα παγωτά της παιδικής σου ηλικίας;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mpananae2.jpg" alt="Πόσο καλά θυμάσαι τα παγωτά της παιδικής σου ηλικίας;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/57937/posoys-kykloys-vlepeis-sthn-eikona-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Πόσους κύκλους βλέπεις στην εικόνα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/3ad81836-9498-4d7d-b8ab-dc11da6afb31.jpg" alt="Πόσους κύκλους βλέπεις στην εικόνα;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/57881/poio-magio-protimate-vote" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΨΗΦΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ποιο μαγιό προτιμάτε;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/magio8.jpg" alt="Ποιο μαγιό προτιμάτε;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/57560/mporeite-na-perasete-to-test-twn-koykidwn-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείτε να περάσετε το τεστ των κουκίδων;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dotetst.jpg" alt="Μπορείτε να περάσετε το τεστ των κουκίδων;" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/life">Life</a></span>
<div class="dropdown_fullwidth mgmenu_tabs">
<div class="mgmenu_width">
<ul class="mgmenu_tabs_nav">
<li><a href="/life" rel="section2_0" class="current">ΤΕΛΕΥΤΑΙΑ θΕΜΑΤΑ</a></li>
<li><a href="/food" rel="section2_1">FOOD</a></li>
<li><a href="/body-mind" rel="section2_2">BODY+MIND</a></li>
<li><a href="/spiti" rel="section2_3">ΣΠΙΤΙ</a></li>
<li><a href="/travel" rel="section2_4">ΤΑΞΙΔΙΑ</a></li>
<li><a href="/style" rel="section2_5">STYLE</a></li>
<li><a href="/family" rel="section2_6">FAMILY</a></li>
<li><a href="/sxeseis" rel="section2_7">ΣΧΕΣΕΙΣ</a></li>
<li><a href="/zodia" rel="section2_8">ΖΩΔΙΑ</a></li>
</ul>
<div class="mgmenu_tabs_panels">
<div id="section2_0" class="menuPosts">
<a href="http://www.e-daily.gr/life/58005/neos-megalos-diagwnismos-kerdiste-ena-taksidi-sthn-skiatho-kerdiste" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Νέος Μεγάλος Διαγωνισμός: Κερδίστε ένα ταξίδι στην Σκιάθο! </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/1.jpg" alt="Νέος Μεγάλος Διαγωνισμός: Κερδίστε ένα ταξίδι στην Σκιάθο! " border="0" class="cropimg lazyload">
</a>
<a href="/life/58113/psaksame-kai-vrhkame-ta-pio-omorfa-sorts-se-top-prosfores-pics-list" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-th-list"></span></div>
<div class="menuItem_title">Ψάξαμε και βρήκαμε τα πιο όμορφα σορτς σε τοπ προσφορές!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sortskaivermoudes.jpg" alt="Ψάξαμε και βρήκαμε τα πιο όμορφα σορτς σε τοπ προσφορές!" border="0" class="cropimg lazyload">
</a>
<a href="/life/58114/idees-gia-4-makaronades-aples-grhgores-kai-pentanostimes-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Ιδέες για 4 μακαρονάδες απλές, γρήγορες και πεντανόστιμες!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/13176536_1758400297745945_1941067436_n-720x600.jpg" alt="Ιδέες για 4 μακαρονάδες απλές, γρήγορες και πεντανόστιμες!" border="0" class="cropimg lazyload">
</a>
<a href="/life/56924/skiathos-arxontikh-kosmikh-me-yperoxa-kataprasina-nera-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Σκιάθος: Αρχοντική, κοσμική με υπέροχα, καταπράσινα νερά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/skiathosoldtown.jpg" alt="Σκιάθος: Αρχοντική, κοσμική με υπέροχα, καταπράσινα νερά" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_1" class="menuPosts mgmenu_tabs_hide">
<a href="/life/57028/ksereis-na-ypologizeis-poio-exei-perissoteres-thermides-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ξέρεις να υπολογίζεις; Ποιο έχει περισσότερες θερμίδες;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/howmanycalories2.jpg" alt="Ξέρεις να υπολογίζεις; Ποιο έχει περισσότερες θερμίδες;" border="0" class="cropimg lazyload">
</a>
<a href="/life/57771/se-kername-geyma-sto-top-xane-pics-kerdiste" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Σε κερνάμε γεύμα στο Τοπ Χανέ! </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tophane21.jpg" alt="Σε κερνάμε γεύμα στο Τοπ Χανέ! " border="0" class="cropimg lazyload">
</a>
<a href="/life/57143/trofes-poy-den-lhgoyn-pote" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τροφές που δεν λήγουν ποτέ!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/meli.jpg" alt="Τροφές που δεν λήγουν ποτέ!" border="0" class="cropimg lazyload">
</a>
<a href="/life/57778/champions-league-brhkame-tis-top-prosfores-pizza-gia-ton-teliko-shmera-list" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-th-list"></span></div>
<div class="menuItem_title">Champions League: Bρήκαμε τις top προσφορές pizza για τον τελικό σήμερα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/2.jpg" alt="Champions League: Bρήκαμε τις top προσφορές pizza για τον τελικό σήμερα" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_2" class="menuPosts mgmenu_tabs_hide">
<a href="http://go.linkwi.se/z/11955-1/CD15753/?" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Γνώρισε την επιδερμίδα σου & κέρδισε τα προϊόντα που σου ταιριάζουν!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/skinexpert45.jpg" alt="Γνώρισε την επιδερμίδα σου & κέρδισε τα προϊόντα που σου ταιριάζουν!" border="0" class="cropimg lazyload">
</a>
<a href="/life/57865/giati-myrizei-h-anasa-mas-otan-ksypname-to-prwi" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιατί μυρίζει η ανάσα μας όταν ξυπνάμε το πρωί</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/wakeuptaste.jpg" alt="Γιατί μυρίζει η ανάσα μας όταν ξυπνάμε το πρωί" border="0" class="cropimg lazyload">
</a>
<a href="/life/57895/therapeia-me-gala-gia-ta-mallia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Θεραπεία με γάλα για τα μαλλιά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/hairmask.jpg" alt="Θεραπεία με γάλα για τα μαλλιά" border="0" class="cropimg lazyload">
</a>
<a href="/life/57673/kante-to-proswpo-sas-na-lampsei-me-merikes-xrhsimes-symvoyles-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Κάντε το πρόσωπό σας να λάμψει με μερικές χρήσιμες συμβουλές</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/beautycanva.jpg" alt="Κάντε το πρόσωπό σας να λάμψει με μερικές χρήσιμες συμβουλές" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_3" class="menuPosts mgmenu_tabs_hide">
<a href="/life/57161/lets-go-green-loyloydia-ths-anoikshs-gia-to-mpalkoni-kai-ton-khpo-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Let`s go green: Λουλούδια της άνοιξης για το μπαλκόνι και τον κήπο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mpalkonikopologio.jpg" alt="Let`s go green: Λουλούδια της άνοιξης για το μπαλκόνι και τον κήπο" border="0" class="cropimg lazyload">
</a>
<a href="/life/57600/h-agnwsth-xrhsh-ths-floydas-ths-mpananas-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Η άγνωστη χρήση της φλούδας της μπανάνας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/banana-1.jpg" alt="Η άγνωστη χρήση της φλούδας της μπανάνας" border="0" class="cropimg lazyload">
</a>
<a href="/life/57414/25-idees-gia-na-diakosmhsete-to-mpalkoni-sas-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">25 ιδέες για να διακοσμήσετε το μπαλκόνι σας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/balcony-decorating-ideas-coverimage.jpg" alt="25 ιδέες για να διακοσμήσετε το μπαλκόνι σας" border="0" class="cropimg lazyload">
</a>
<a href="/life/56502/pws-na-eksafanisete-paneykola-kai-aneksoda-tis-gratsoynies-apo-ta-ksylina-epipla-sas-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Πως να εξαφανίσετε πανεύκολα και ανέξοδα τις γρατσουνιές από τα ξύλινα έπιπλα σας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sealwood1.jpg" alt="Πως να εξαφανίσετε πανεύκολα και ανέξοδα τις γρατσουνιές από τα ξύλινα έπιπλα σας" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_4" class="menuPosts mgmenu_tabs_hide">
<a href="http://www.e-daily.gr/life/57938/brhkame-tis-kalyteres-prosfores-gia-taksidi-sthn-italia-pics-list" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-th-list"></span></div>
<div class="menuItem_title">Bρήκαμε τις καλύτερες προσφορές για ταξίδι στην Ιταλία!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/italia-napoli3.jpg" alt="Bρήκαμε τις καλύτερες προσφορές για ταξίδι στην Ιταλία!" border="0" class="cropimg lazyload">
</a>
<a href="/life/57936/ekplhktikes-paralies-poy-moiazoyn-pseytikes-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Εκπληκτικές παραλίες που μοιάζουν ψεύτικες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paralies-3546346324.jpg" alt="Εκπληκτικές παραλίες που μοιάζουν ψεύτικες" border="0" class="cropimg lazyload">
</a>
<a href="/life/57505/islandia-h-xwra-ths-fwtias-kai-toy-pagoy-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Ισλανδία: Η χώρα της φωτιάς και του πάγου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/iceland_fire_ice.jpg" alt="Ισλανδία: Η χώρα της φωτιάς και του πάγου" border="0" class="cropimg lazyload">
</a>
<a href="/life/57392/ta-trena-sto-parisi-metatrepontai-se-kinhta-moyseia-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Τα τρένα στο Παρίσι μετατρέπονται σε κινητά μουσεία</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paris_metro_main.jpg" alt="Τα τρένα στο Παρίσι μετατρέπονται σε κινητά μουσεία" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_5" class="menuPosts mgmenu_tabs_hide">
<a href="/life/57994/neos-diagwnismos-h-nadeen-mporei-na-zwgrafisei-paramythia-sta-sandalia-soy-pics-kerdiste" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Νέος Διαγωνισμός: H Nadeen μπορεί να ζωγραφίσει παραμύθια στα σανδάλια σου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/SANDALIANADDEEN.jpg" alt="Νέος Διαγωνισμός: H Nadeen μπορεί να ζωγραφίσει παραμύθια στα σανδάλια σου" border="0" class="cropimg lazyload">
</a>
<a href="/life/57751/ta-pio-polyxrwma-stylata-kai-oikonomika-aksesoyar-gia-ayto-to-kalokairi" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα πιο πολύχρωμα, στυλάτα και οικονομικά αξεσουάρ για αυτό το καλοκαίρι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/canvacover3.jpg" alt="Τα πιο πολύχρωμα, στυλάτα και οικονομικά αξεσουάρ για αυτό το καλοκαίρι" border="0" class="cropimg lazyload">
</a>
<a href="http://go.linkwi.se/z/11656-1/CD15753/" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Κέρδισε εκπτώσεις και δώρα έκπληξη από την Maybelline!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/maybellinedora.jpg" alt="Κέρδισε εκπτώσεις και δώρα έκπληξη από την Maybelline!" border="0" class="cropimg lazyload">
</a>
<a href="/life/57775/ekptwseis-epwnyma-foremata-poy-tha-foras-olo-to-kalokairi-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Εκπτώσεις! Eπώνυμα φορέματα που θα φοράς όλο το καλοκαίρι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dresses2.jpg" alt="Εκπτώσεις! Eπώνυμα φορέματα που θα φοράς όλο το καλοκαίρι" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_6" class="menuPosts mgmenu_tabs_hide">
<a href="/life/58007/8-stantar-ekfraseis-poy-lene-oi-goneis-sta-paidia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">8 στάνταρ εκφράσεις που λένε οι γονείς στα παιδιά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paidia_goneis_1.jpg" alt="8 στάνταρ εκφράσεις που λένε οι γονείς στα παιδιά" border="0" class="cropimg lazyload">
</a>
<a href="/life/57951/mexri-poia-hlikia-epitrepetai-na-vlepei-to-paidi-gumnoys-toys-goneis" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Μέχρι ποια ηλικία επιτρέπεται να βλέπει το παιδί γuμνούς τους γονείς</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/goneis.jpg" alt="Μέχρι ποια ηλικία επιτρέπεται να βλέπει το παιδί γuμνούς τους γονείς" border="0" class="cropimg lazyload">
</a>
<a href="/life/25717/ti-theloyn-pragmatika-oi-gynaikes-gia-dwro-sth-giorth-ths-mhteras-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Tι θέλουν πραγματικά οι γυναίκες για δώρο, στη Γιορτή της Μητέρας </div>
<img data-src="http://cdn.e-daily.gr/repository/2014/mothersday2.jpg" alt="Tι θέλουν πραγματικά οι γυναίκες για δώρο, στη Γιορτή της Μητέρας " border="0" class="cropimg lazyload">
</a>
<a href="/life/7238/sofa-logia-gia-tis-mhteres" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Σοφά λόγια για τις μητέρες</div>
<img data-src="http://cdn.e-daily.gr/repository/2012/mother_day.jpg" alt="Σοφά λόγια για τις μητέρες" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_7" class="menuPosts mgmenu_tabs_hide">
<a href="/life/58154/enas-stoys-deka-enhlikoys-exei-seksoyalikh-empeiria-me-atomo-toy-idioy-fyloy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ένας στους δέκα ενήλικους έχει σeξουαλική εμπειρία με άτομο του ίδιου φύλου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/c472235833b7faac1d987c54bcd85f3c.jpg" alt="Ένας στους δέκα ενήλικους έχει σeξουαλική εμπειρία με άτομο του ίδιου φύλου" border="0" class="cropimg lazyload">
</a>
<a href="/life/57590/proedros-andrologikoy-institoytoy-gennhthhkame-apistoi-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Πρόεδρος Ανδρολογικού Ινστιτούτου: Γεννηθήκαμε άπιστοι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/proedros-konstantinidis-343.jpg" alt="Πρόεδρος Ανδρολογικού Ινστιτούτου: Γεννηθήκαμε άπιστοι" border="0" class="cropimg lazyload">
</a>
<a href="/life/57477/giati-to-na-eiste-monoi-sas-odhgei-se-kalyteres-sxeseis-sto-mellon" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιατί το να είστε μόνοι σας οδηγεί σε καλύτερες σχέσεις στο μέλλον;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/relationshio90.jpg" alt="Γιατί το να είστε μόνοι σας οδηγεί σε καλύτερες σχέσεις στο μέλλον;" border="0" class="cropimg lazyload">
</a>
<a href="/life/57356/oi-5-shmantikoteres-symvoyles-gia-mia-eytyxismenh-sxesh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Οι 5 σημαντικότερες συμβουλές για μια ευτυχισμένη σχέση</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zeugari3.jpeg" alt="Οι 5 σημαντικότερες συμβουλές για μια ευτυχισμένη σχέση" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_8" class="menuPosts mgmenu_tabs_hide">
<a href="/life/57760/zwdia-evdomadas-30-maioy-5-ioynioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ζώδια Εβδομάδας: 30 Mαίου - 5 Ιουνίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zodiapicnic.jpg" alt="Ζώδια Εβδομάδας: 30 Mαίου - 5 Ιουνίου" border="0" class="cropimg lazyload">
</a>
<a href="/viral/57381/eswpoyxa-ti-aresei-stoys-antres-kathe-zwdioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Εσώpουχα: Τι αρέσει στους άντρες κάθε ζωδίου;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/underwear34.jpg" alt="Εσώpουχα: Τι αρέσει στους άντρες κάθε ζωδίου;" border="0" class="cropimg lazyload">
</a>
<a href="/life/57238/zwdia-evdomadas-23-29-maioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ζώδια εβδομάδας: 23 - 29 Μαίου </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tropicalzo.jpg" alt="Ζώδια εβδομάδας: 23 - 29 Μαίου  " border="0" class="cropimg lazyload">
</a>
<a href="/life/56674/zwdia-evdomadas-16-22-maioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ζώδια Εβδομάδας: 16 - 22 Μαίου </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/fridaflower.jpg" alt="Ζώδια Εβδομάδας: 16 - 22 Μαίου " border="0" class="cropimg lazyload">
</a>
</div>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/entertainment">Ψυχαγωγία</a></span>
<div class="dropdown_fullwidth mgmenu_tabs">
<div class="mgmenu_width">
<ul class="mgmenu_tabs_nav">
<li><a href="/agenda" rel="section5_1" class="current">ATZENTA ΣΗΜΕΡΑ</a></li>
<li><a href="/music" rel="section5_2">ΜΟΥΣΙΚΗ</a></li>
<li><a href="/cinema" rel="section5_3">ΣΙΝΕΜΑ</a></li>
<li><a href="/theater" rel="section5_4">ΘΕΑΤΡΟ+ΧΟΡΟΣ</a></li>
<li><a href="/arts" rel="section5_5">ΕΙΚΑΣΤΙΚΑ</a></li>
<li><a href="/city" rel="section5_6">ΠΟΛΗ</a></li>
<li><a href="/nightlife" rel="section5_7">ΝΑΪΤ ΛΑΪΦ</a></li>
</ul>
<div class="mgmenu_tabs_panels">
<div id="section5_1" class="menuPosts">
<a href="/entertainment/58142/panagiwths-velianiths-protzekt-atlas" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Παναγιώτης Βελιανίτης: ​πρότζεκτ Άτλας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/panagiotisvelianitis.jpg" alt="Παναγιώτης Βελιανίτης: ​πρότζεκτ Άτλας" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58141/moysikes-ston-khpo-toy-megaroy-moysikhs-athhnwn" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Μουσικές στον Κήπο του Μεγάρου Μουσικής Αθηνών</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/megarokipos2016.jpg" alt="Μουσικές στον Κήπο του Μεγάρου Μουσικής Αθηνών" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58140/to-koritsi-mesa-sto-spiti-apo-to-theatro-toy-allote" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το Κορίτσι μέσα στο Σπίτι από το Θέατρο του `Αλλοτε</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tokoritsimesastospiti.jpg" alt="Το Κορίτσι μέσα στο Σπίτι από το Θέατρο του `Αλλοτε" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58139/sterna-art-project-2016-prwtokollo-peiramatikhs-ekpaideyshs-sth-nisyro" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Sterna Art Project 2016: Πρωτόκολλο Πειραματικής Εκπαίδευσης στη Νίσυρο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/experimentaleducationprotocol.jpg" alt="Sterna Art Project 2016: Πρωτόκολλο Πειραματικής Εκπαίδευσης στη Νίσυρο" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_2" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/58134/tinos-world-music-festival-2016-fwnes-toy-kosmoy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Tinos World Music Festival 2016: Φωνές του κόσμου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tinosworldmusicfestival.jpg" alt="Tinos World Music Festival 2016: Φωνές του κόσμου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58132/fougarock-festival-2" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Fougarock Festival 2</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/fougarofest2.jpg" alt="Fougarock Festival 2" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58120/stella-live-nteibint-single-release-party" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Σtella Live + Nteibint Single Release Party</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/stellalive2016.jpg" alt="Σtella Live + Nteibint Single Release Party" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58117/sssh-silent-movies-special-screening-glykia-symmoria-sthn-taratsa-toy-mpaios" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Sssh! Silent Movies special screening – Γλυκιά Συμμορία στην ταράτσα του Μπάιος</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/glikiasimmoria.jpg" alt="Sssh! Silent Movies special screening – Γλυκιά Συμμορία στην ταράτσα του Μπάιος" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_3" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/58085/parthenwn-film-festival" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Parthenώn Film Festival</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/parthenonfest.jpg" alt="Parthenώn Film Festival" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58035/7o-festival-vwvoy-kinhmatografoy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">7ο Φεστιβάλ Βωβού Κινηματογράφου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/7ofestivalvovoukinimatografou.jpg" alt="7ο Φεστιβάλ Βωβού Κινηματογράφου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/57904/to-cine-manto-mykonos-epistrefei-kai-fetos-apo-tis-3-ioynioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το Cine Manto Mykonos επιστρέφει και φέτος από τις 3 Ιουνίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/cinemantoARVANITAKIDELOS.jpg" alt="Το Cine Manto Mykonos επιστρέφει και φέτος από τις 3 Ιουνίου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/57690/tainies-ths-evdomadas-russell-crowe-ryan-gosling-h-alliws-ta-agoria-ksenyxtane-mona-h-dyo-dyo-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Ταινίες της Εβδομάδας: Russell Crowe + Ryan Gosling (ή αλλιώς τα αγόρια ξενυχτάνε μόνα ή δυο-δυο)</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/theniceguys.jpg" alt="Ταινίες της Εβδομάδας: Russell Crowe + Ryan Gosling (ή αλλιώς τα αγόρια ξενυχτάνε μόνα ή δυο-δυο)" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_4" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/58135/vii-diethnes-politistiko-festival-loyloydia-ths-rwsias" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΧΟΡΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">VII Διεθνές πολιτιστικό φεστιβάλ «Λουλούδια της Ρωσίας»</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/louloudiarwsiasfest16.jpg" alt="VII Διεθνές πολιτιστικό φεστιβάλ «Λουλούδια της Ρωσίας»" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58116/sync-apo-to-egw-sto-emeis-mia-parastash-xoroy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΧΟΡΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Sync: Από το εγώ στο εμείς - Μία παράσταση χορού</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/syncbios.jpg" alt="Sync: Από το εγώ στο εμείς - Μία παράσταση χορού" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58110/01-project-sth-sofita" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΧΟΡΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">01 PROJECT - Στη σοφίτα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/stagedoor104.jpg" alt="01 PROJECT - Στη σοφίτα" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58036/detach-the-dark-manifesto" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Detach: ​The Dark Manifesto</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/detachdarkmanifesto.jpg" alt="Detach: ​The Dark Manifesto" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_5" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/58138/to-atopos-cvc-kai-to-sterna-art-project-filoksenoyn-ton-andrey-bogush-sth-nisyro" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το Atopos cvc και το Sterna Art Project φιλοξενούν τον Andrey Bogush στη Νίσυρο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/atopossterna.jpg" alt="Το Atopos cvc και το Sterna Art Project φιλοξενούν τον Andrey Bogush στη Νίσυρο" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58137/mathhmata-kwphlasias-ekthesh-toy-nikoy-papadopoyloy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Μαθήματα Κωπηλασίας: έκθεση του Νίκου Παπαδόπουλου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mathimatakopilasias1.jpg" alt="Μαθήματα Κωπηλασίας: έκθεση του Νίκου Παπαδόπουλου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58119/atomikh-ekthesh-toy-petroy-nikoltsoy-sto-six-dogs" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ατομική έκθεση του Πέτρου Νικόλτσου στο Six d.o.g.s.</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/nikoltsos2016.jpg" alt="Ατομική έκθεση του Πέτρου Νικόλτσου στο Six d.o.g.s." border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58099/h-zwh-mesa-apo-to-vlemma-twn-spoydastwn-toy-iek-akmh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η ζωή μέσα από το βλέμμα των σπουδαστών του ΙΕΚ ΑΚΜΗ</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ekthesi_iek.jpg" alt="Η ζωή μέσα από το βλέμμα των σπουδαστών του ΙΕΚ ΑΚΜΗ" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_6" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/58112/hoop-it-xoyla-parti-sthn-taratsa" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Hoop it! Χούλα Πάρτι στην ταράτσα </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/hoopitjune.jpg" alt="Hoop it! Χούλα Πάρτι στην ταράτσα " border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58096/waterdrop-festival" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Waterdrop Festival </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/waterdrop3.jpg" alt="Waterdrop Festival " border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/58022/1o-diethnes-synedrio-europe-in-discourse-identity-diversity-borders" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">1ο Διεθνές Συνέδριο: “Europe in Discourse: Identity, Diversity, Borders”. </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/europeindiscourse.jpg" alt="1ο Διεθνές Συνέδριο: “Europe in Discourse: Identity, Diversity, Borders”. " border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/57910/to-back-to-athens4-epistrefei-sthn-istorikh-plateia-eksarxeiwn" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το Back to Athens/4 επιστρέφει στην ιστορική πλατεία Εξαρχείων</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/backtoathens4.jpg" alt="Το Back to Athens/4 επιστρέφει στην ιστορική πλατεία Εξαρχείων" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_7" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/57900/bananaboys-godrag" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">BananaBoys goDRAG!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/bananaboysgodrag.jpg" alt="BananaBoys goDRAG!" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/57583/vomv-when-performance-artists-invade-the-party" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΝΑΙΤ ΛΑΙΦ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">ΒΟΜΒ: when performance artists invade the party</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/bombromantso.jpg" alt="ΒΟΜΒ: when performance artists invade the party" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/57492/eleanor-friedberger-live-band-opening-act-apo-ton-leyterh-moymtzh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΞΕΝΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Eleanor Friedberger Live Band + opening act από τον Λευτέρη Μουμτζή</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/eleanorfriedberger.jpg" alt="Eleanor Friedberger Live Band + opening act από τον Λευτέρη Μουμτζή" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/57104/trashformers-all-stars-12-years-anniversary" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΝΑΙΤ ΛΑΙΦ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Trashformers All Stars - 12 years anniversary!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/trashformers2016.jpg" alt="Trashformers All Stars - 12 years anniversary!" border="0" class="cropimg lazyload">
</a>
</div>
</div>
</div>
</div>
</li> 
<li class="dropdown_custom"><span class="mgmenu_drop"><a href="/weather" id="weatherMenu"></a></span>
<div class="dropdown_container mgmenu_smallwidth" id="weatherMenuPad"></div>
</li>
</ul>
<a href="/agenda" class="mgmenu_left nextwebchannels"><img src="/_img/eagendagr_logo.png" height="20"/></a>
<a href="http://www.e-radio.gr" target="_blank" class="mgmenu_left nextwebchannels"><img src="/_img/eradiogr_logo.png" height="20"/></a>
<div class="right_item">
<div class="right_item" id="tabSearch" style="padding:12px 10px 14px 8px;"><div class="icon-search"></div></div>
<div class="right_item noactive" style="padding:0">
<a class="icon-twitter-bird right_item twitter" href="http://www.twitter.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px;"></a>
<a class="icon-instagram right_item instagram" href="http://www.instagram.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px;"></a>
<a class="icon-facebook right_item facebook" href="http://www.facebook.com/edailygr" target="_blank" style="padding:12px 0 14px 10px;"></a>
</div>
<div class="right_item noactive feedIcons showOnSmallMenu">
<a href="/retro"><img alt="Retro" src="/_img/feeds/110.png" border="0"/></a>
<a href="/omg"><img alt="OMG" src="/_img/feeds/101.png" border="0"/></a>
<a href="/lol"><img alt="LOL" src="/_img/feeds/106.png" border="0"/></a>
<a href="/juicy"><img alt="Juicy" src="/_img/feeds/104.png" border="0"/></a>
<a href="/alist"><img alt="A-List" src="/_img/feeds/91.png" border="0"/></a>
<a href="/cute"><img alt="Cute" src="/_img/feeds/109.png" border="0"/></a>
<a href="/geeky"><img alt="Geeky" src="/_img/feeds/108.png" border="0"/></a>
</div>
</div>
</div>
<div class="mgmenu_fullwidth"></div>
</div>
<div id="topHeaderMobile" style="text-align:center;">
<div class="ma-infinitypush-button"><span></span><span></span><span></span><span></span></div>
<a href="/"><img src="/_img/mobilelogo.png" width="136" height="48" alt="E-Daily" border="0"/></a>
<div class="right_item" id="tabSearchMobile" style="padding:12px; font-size: 18px;"><div class="icon-search"></div></div>
</div>
<div id="tabSearchMenu">
<form name="bigsearchboxform" id="bigsearchboxform" class="mainArea" autocomplete="off" method="post" action="/search/" onsubmit="javascript:submitsearch()">
<input type="search" name="q" id="q" placeholder="Αναζητήστε 57214 θέματα, 996 ραδιόφωνα" value="" alt="Αναζητήστε 57214 θέματα, 996 ραδιόφωνα"/>
<a name="bigsearchbox_Cancel" id="bigsearchbox_Cancel" href="javascript:hidesearch()" class="icon-cancel-circle"></a>
</form>
</div>
<div class="navibar">
</div>
<div id="topHeader3">
<center>
<script type="text/javascript">
( function() {
    var info = getTagInfo();
    var alias = 'edhomebillboard_display';
        alias = alias !== '' ? 'alias=' + ( alias.split("_") ? alias.split("_")[ 0 ] : alias ) + '_' + info.alias : '';
    document.write('<scr'+'ipt language="javascript1.1" src="http://' + info.domain + '.adtech.de/addyn/3.0/1370.1/4713596/0/-1/ADTECH;' + alias + ';loc=100;target=_blank;key=;grp=' + info.adgroupid + ';misc=' + new Date().getTime() + '"></scri'+'pt>');
})();</script>
</center>
</div>
<div class="mainArea">
<div class="mainAreaA">
<h1 class="postTitle">Privacy Policy</h1>
<div class="postBody">
<p>Welcome to E-Daily. This Privacy Policy is effective for E-Daily.gr, E-Daily Mobile Apps (&ldquo;E-Daily&rdquo;, &ldquo;we&rdquo;, &ldquo;us&rdquo; and/or &ldquo;our&rdquo;) and is incorporated by reference into the E-Daily Terms of Service and End User License Agreement. <br/>
The purpose of this Privacy Policy is to inform users of the E-Daily Service (as defined below) about the information that we collect through the E-Daily Service, how we use that information and the ways users can control how that information is used or shared. The entire policy can be found below. For information about users&rsquo; obligations and use of the E-Daily Service, please see our Terms of Service and End User License Agreement. </p>
<p> We will continue to evaluate this Privacy Policy and update it accordingly when there is a change to our business practices, our users&rsquo; needs, and to comply with regulatory and legal requirements. Please check this page periodically for updates. If we make any material changes to this Privacy Policy, we will post the updated Privacy Policy here and notify you by email or by means of a notice on the Site (as defined below) and/or the Applications (as defined below). </p>
<p><strong>The types of information we collect</strong><br/>
When you interact with the E-Daily.gr, we may collect Personal Information (as defined below) and other information from you as described below. <br/>
&ldquo;Personal Information&rdquo; means information that alone or when in connection with other information may be used to readily identify, contact, or locate you, such as: name, address, email address or phone number. We do not consider personally identifiable information to include information that has been anonymized so that it does not allow a third party to easily identify a specific individual. </p>
<p><strong>Information you give us </strong><br/>
To enable you to enjoy certain features of the E-Daily Service, we collect certain types of information, including Personal Information, during your interactions with the E-Daily Service. By using the E-Daily Service, you are authorizing us to gather, parse, and retain data related to the provision of the E-Daily Service. </p>
<p> For example, we collect information, including Personal Information, when you: </p>
<ul type="disc">
<li>Register to use our E-Daily Service;</li>
<li>Login with social networking credentials; </li>
<li>Participate in polls, contests, surveys or other features of the E-Daily Service, or respond to offers or advertisements on the E-Daily Service; </li>
<li>Communicate with us; and</li>
<li>Sign up to receive email newsletters.</li>
</ul>
<p>When you register to use the E-Daily Service, we ask you to provide certain information, which includes your name, email address, birth year, gender, zip code and/or other geographic information, as well as a password for your account. By voluntarily providing us with Personal Information or other information, you are consenting to our use of it in accordance with this Privacy Policy. If you provide any Personal Information or other information, you acknowledge and agree that such Personal Information may be transferred from your current location to our offices and servers and the authorized third parties. </p>
<p><strong>Automatic Data Collection</strong><br/>
<em>Listening and Usage Activity</em>: We keep track of your listening and usage activity. We collect information about stations, favorites, artists, channels, conversations and tracks you have listened to or in which you have expressed an interest. <br/>
<em>Social connect information</em>: When you choose to connect your social media account to your account profile, we collect Personal Information from that social media website. For example, when you connect your Facebook account, we may collect the Personal Information you have made publicly available in Facebook, such as your name, profile picture, cover photo, username, gender, friend networks, age range, locale, friend list, and any other information you have made public. We may also obtain other non-public information from the social network website with your permission. </p>
<p><em>Information about your computer or device</em>: We may also collect information about the computer, mobile or other devices you use to access the E-Daily Service. For example, our servers receive and record information about your computer and browser, including potentially your IP address, device ID, operating system, browser type, and other software or hardware information. If you access the E-Daily Service from a mobile or other device, unless you register, we may assign a unique device identifier for that device. </p>
<p><em>Location-based information</em>: We do not collect your zip code, postal code, city or country unless you choose to provide this information to us at registration. Your general geo-location is collected at regular intervals if you use the E-Daily Service on your mobile device or through the Site. We collect this information to provide content and services that are customized for your geographic area. </p>
<p><em>Tracking technology</em>: We use what are commonly called &quot;cookies&quot; on our Site. A cookie is a piece of information that the computer that hosts our Site gives to your browser when you access the Site. These cookies help us improve the E-Daily Service, provide additional functionality to the Site and help us analyze Site usage more accurately. For instance, our Site may set a cookie on your browser that allows you to access the Site without needing to remember and then enter a password more than once during a visit to the Site. We may also be able to capture other data such as searches conducted and results, date, time, and connection speed. In all cases in which we use cookies, we will not collect Personal Information except with your permission. On most web browsers, you will find a &ldquo;help&rdquo; section on the toolbar. Please refer to this section for information on how to receive notification when you are receiving a new cookie and how to turn cookies off. We recommend that you leave cookies turned on because they allow you to take advantage of some of the Site&rsquo;s features. Third parties, including advertisers and third-party advertising companies, whose products or services are accessible or advertised via the E-Daily Service may also use cookies, and we advise you to check their privacy policies for information about their cookies and other privacy practices. </p>
<p><strong>Our use of the information we collect</strong><br/>
We use the information, including Personal Information, that we collect for the following purposes: </p>
<ul type="disc">
<li>To personalize and customize the advertising and the content you see.</li>
<li>To improve and enhance the user experience. We use tracking information to determine how well each page, station and/or channel performs overall based on aggregate user demographics and traffic patterns to those pages, stations and channels. This helps us build a better service for you.</li>
<li>To fulfill your requests for certain products and services, such as sending out electronic newsletters and enabling you to participate in polls, contests, and boards.</li>
<li>To send you information about topics or content that we think will be of interest to you based on your preferences, habits or geographic location.</li>
<li>To send you information about the latest developments and features on our E-Daily Service.</li>
<li>To provide technical support.</li>
</ul>
<p>We may anonymize and aggregate data collected through the E-Daily Service and use it for any purpose. </p>
<p><strong>How the information we collect is shared</strong><br/>
We are not in the business of selling, renting or sharing Personal Information about you with other people or nonaffiliated companies for their direct marketing purposes. We consider this information to be a vital part of our relationship with you. There are, however, certain circumstances in which we may share your information, including Personal Information, with certain third parties without further notice to you, as set forth below: </p>
<ul type="disc">
<li>Information you allow us to share when you register with us, or through a subsequent affirmative election.</li>
<li>Information used when we hire or partner with third parties to provide business-related services on our behalf, such as mailing information, maintaining databases, processing payments, sweepstakes management and prize fulfillment, data processing, analytics, personalization and customization of advertising, customer/support services and other products or services that we choose to make available to our registered users. When we employ another company to perform a function of this nature, we only provide them with the information that they need to perform their specific function.</li>
<li>We may share, transfer or sell your information in connection with a merger, acquisition, financing due diligence, reorganization, bankruptcy, receivership, sale of company assets, or transition of service to another provider. We cannot control how such entities may use or disclose such information.</li>
<li>We may share information with our subsidiaries and affiliates for purposes consistent with this Privacy Policy.</li>
<li>The E-Daily Service may allow you to share information, including Personal Information, with social networking websites, such as Facebook. We do not share your Personal Information with them unless you direct the E-Daily Service to share it. Their use of the information will be governed by their privacy policies, and you may be able to modify your privacy setting on their websites.</li>
<li>We may share your information if required to do so by law or in the good faith belief that such action is necessary in order to (i) comply with a legal obligation, (ii) protect or defend our legal rights or property as well as those of our business partners, employees, agents and contractors (including enforcement of our agreements); (iii) act in urgent circumstances to protect the personal safety of users or the public; (iv) protect against fraud or risk management purposes; or (v) protect against legal liability.</li>
</ul>
<p>When you register and create a profile through the E-Daily Service, other users of the E-Daily Service may be able to see your profile information, including your screen name, profile photo and any content that you post to the E-Daily Service. We recommend that you guard your anonymity and sensitive information. We are not responsible for the privacy practices of the other users who will view and use the information you post. </p>
<p><strong>Non-Identifiable or Aggregated Data</strong><br/>
We also receive and store certain non-personally identifiable information. Such information, which is collected passively using various technologies, cannot presently be used to specifically identify you. We may store such information itself or such information may be included in databases owned and maintained by our affiliates, agents or service providers. We may use such information and pool it with other information to track, for example, the total number of visitors to the E-Daily Service, the number of visitors to each page of our Site, the domain names of our visitors' Internet service providers, and how our users use and interact with the E-Daily Service. Also, in an ongoing effort to better understand and serve the users of the E-Daily Service, we often conduct research on our customer demographics, interests and behavior based on the personal information and other information provided to us. This research may be compiled and analyzed on an aggregate basis. We may share this non-identifiable and aggregate data with our affiliates, agents and business partners, but this type of non-identifiable and aggregate information does not identify you personally. We may also disclose aggregated user statistics in order to describe our services to current and prospective business partners and to other third parties for other lawful purposes. <br/>
The use and disclosure of this non-personally identifiable information is not subject to restrictions under this Privacy Policy. </p>
<p><strong>Your Choices</strong><br/>
You can visit the Site without providing any personal information. If you choose not to provide any personal information, you may not be able to use certain features of our E-Daily Service. </p>
<p><strong>Exclusions</strong><br/>
This Privacy Policy does not apply to any personal data collected by E-Daily other than personal data collected through the E-Daily Service. This Privacy Policy shall not apply to any unsolicited information you provide to us through the E-Daily Service or through any other means. This includes, but is not limited to, information posted to any public areas of the E-Daily Service, such as bulletin boards (collectively, &ldquo;Public Areas&rdquo;), any ideas for new products or modifications to existing products, and other unsolicited submissions (collectively, &ldquo;Unsolicited Information&rdquo;). All Unsolicited Information shall be deemed to be non-confidential and we shall be free to reproduce, use, disclose, distribute and exploit such Unsolicited Information without limitation or attribution. </p>
<p><strong>How we protect your information</strong><br/>
We have taken reasonable steps to protect the information you provide via the E-Daily Service from loss, misuse, and unauthorized access, disclosure, alteration, or destruction. However, no Internet, email or other electronic transmission is ever fully secure or error free, so you should take special care in deciding what information you send to us in this way. We do not accept liability for unintentional disclosure. <br/>
By using the E-Daily Service or providing Personal Information to us, you agree that we may communicate with you electronically regarding security, privacy, and administrative issues relating to your use of the E-Daily Service. If we learn of a security system&rsquo;s breach, we may attempt to notify you electronically by posting a notice on the Site and/or Applications or sending an email to you. </p>
<p><strong>Consent to processing</strong><br/>
If you use the E-Daily Service outside of the European Union, you fully understand and unambiguously consent to the transfer of your information, including Personal Information, to, and the collection and processing of such information in Greece and other countries or territories of the European Union. The laws on holding such information in European Union may vary and be less stringent than laws of your state or country. </p>
<p><strong>Children</strong><br/>
We do not knowingly collect Personal Information from children under the age of 13. If you are under the age of 13, please do not submit any Personal Information through the E-Daily Service. We encourage parents and legal guardians to monitor their children&rsquo;s Internet usage and to help enforce our Privacy Policy by instructing their children never to provide Personal Information through the E-Daily Service without their permission. If you have reason to believe that a child under the age of 13 has provided Personal Information to us through our E-Daily, please contact us, and we will endeavor to delete that information from our databases. </p>
<p><strong>Links to Other Web Sites</strong><br/>
This Privacy Policy applies only to the E-Daily Service. The E-Daily Service may contain links to other web sites not operated or controlled by us (the &ldquo;Third Party Sites&rdquo;). The policies and procedures we described here do not apply to the Third Party Sites. The links from the E-Daily Service do not imply that we endorse or have reviewed the Third Party Sites. We suggest contacting those sites directly for information on their privacy policies. </p>
<p><strong>What are my privacy options and how can I change my settings?</strong><br/>
<em>To modify your registration information:</em> Once you have registered, you can change your registration information (including your email address and password). <br/>
<em>Opt out:</em> Each email communication we send you will contain instructions permitting you to &quot;opt-out&quot; of receiving future marketing communications. In addition, if at any time you wish not to receive any future communications or you wish to have your name deleted from our email mailing lists, please contact us as indicated below. Note however, that certain communications, such as push notifications, require you to change the settings on your device if at any time you wish not to receive any future communications. Note that on occasion we send service-related announcements, such as disruption notifications, and you may not be able to opt-out of these communications. </p>
<p><strong>English language version shall control</strong><br/>
The English language version of this Privacy Policy is the version that governs your use of the E-Daily Service and in the event of any conflict between the English language version and a translated version, the English language version will control.</p>
<p> To print out this Statement, you may visit www.e-daily.gr/privacy.asp</p>
</div>
</div>
<div class="mainArea2">
<center>
<script language="javascript">
<!--
if (window.adgroupid == undefined) {
                window.adgroupid = Math.round(Math.random() * 1000);
}
document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1370/5799491/0/170/ADTECH;loc=100;target=_blank;key=;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
//-->
</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1370/5799491/0/170/ADTECH;loc=300;key=" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1370/5799491/0/170/ADTECH;loc=300;key=" border="0" width="300" height="250"></a></noscript>
</center>
<br>
<script language="javascript">
$().ready(function() {
    $("#todayTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  updateHash: false
	});
/*	$("#tabs-hl").mCustomScrollbar({
		theme:"dark-thick"
	});
*/
	$("#tabs-recent").mCustomScrollbar({
		theme:"dark-thick"
	});
});

</script>
<div class="postsRedHeader">διαβάστε σήμερα</div>
<div id="todayTabs" class="tab-container">
<ul class="etabs">
<li class="tab"><a href="#tabs-hl">ΕΠΙΛΟΓΕΣ</a></li>
<li class="tab"><a href="#tabs-recent">ΠΡΟΣΦΑΤΑ</a></li>
</ul>
<div id="tabs-hl" class="postEntries">
<a href="/themata/58167/ti-doyleia-kanei-shmera-o-prwtos-twn-prwtwn-stis-panelladikes-toy-1990-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/psistariasroiter.jpg" alt="Τι δουλειά κάνει σήμερα ο πρώτος των πρώτων στις Πανελλαδικές του 1990;" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">08:50</span></div>
<h3>Τι δουλειά κάνει σήμερα ο πρώτος των πρώτων στις Πανελλαδικές του 1990;</h3>
<h4>Τον βρήκε ο Αντώνης Σρόιτερ</h4>
</a>
<a href="/news/58166/katatethhkan-oi-tropologies-deite-tis-allages-gia-ekas-enfia-kai-enstoloys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tropologiesekas.jpg" alt="Κατατέθηκαν οι τροπολογίες: Δείτε τις αλλαγές για ΕΚΑΣ, ΕΝΦΙΑ και ένστολους" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span><span class="when">08:43</span></div>
<h3>Κατατέθηκαν οι τροπολογίες: Δείτε τις αλλαγές για ΕΚΑΣ, ΕΝΦΙΑ και ένστολους</h3>
<h4>Κατατέθηκαν αργά το βράδυ της Τετάρτης στη Βουλή</h4>
</a>
<a href="/news/58165/parathyraki-amnhsteyshs-ypoyrgwn-me-offshore-xwris-poinikes-eythynes" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tsiprasmilaei.jpg" alt="«Παραθυράκι» αμνήστευσης υπουργών με offshore χωρίς ποινικές ευθύνες" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">08:33</span></div>
<h3>«Παραθυράκι» αμνήστευσης υπουργών με offshore χωρίς ποινικές ευθύνες</h3>
<h4>Με τη νέα ρύθμιση </h4>
</a>
<a href="/news/58164/tritokosmikes-eikones-kata-th-dianomh-trofimwn-sth-deth-me-lipothymies-kai-entash" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/dikaiouxoidethsitisi.jpg" alt="Τριτοκοσμικές εικόνες κατά τη διανομή τροφίμων στη ΔΕΘ με λιποθυμίες και ένταση" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">08:27</span></div>
<h3>Τριτοκοσμικές εικόνες κατά τη διανομή τροφίμων στη ΔΕΘ με λιποθυμίες και ένταση</h3>
<h4>Σε απόγνωση εκατοντάδες άνθρωποι </h4>
</a>
<a href="/news/58163/xaos-sto-hotspot-ths-morias-metanastes-evalan-fwtia-meta-apo-kavga-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/lesvokauga.jpg" alt="Χάος στο hotspot της Μόριας: Μετανάστες έβαλαν φωτιά μετά από καβγά" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">08:23</span></div>
<h3>Χάος στο hotspot της Μόριας: Μετανάστες έβαλαν φωτιά μετά από καβγά</h3>
<h4>Τουλάχιστον έξι οι τραυματίες</h4>
</a>
<a href="/news/58162/megales-ekplhkseis-gia-tis-vaseis-toy-2016" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/panelinies2016.jpg" alt="Μεγάλες εκπλήξεις για τις βάσεις του 2016" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">08:20</span></div>
<h3>Μεγάλες εκπλήξεις για τις βάσεις του 2016</h3>
<h4>Χαμηλώνει ο πήχης εισαγωγής σε δεκάδες σχολές</h4>
</a>
<a href="/news/58161/voles-zwhs-kata-tsipra-h-tropologia-gia-tis-offshore-eksyphretei-politikoys-filoys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/zoikonstatn.jpg" alt="Βολές Ζωής κατά Τσίπρα: Η τροπολογία για τις offshore εξυπηρετεί πολιτικούς φίλους" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">08:14</span></div>
<h3>Βολές Ζωής κατά Τσίπρα: Η τροπολογία για τις offshore εξυπηρετεί πολιτικούς φίλους</h3>
<h4>Σκληρή κριτική στους πρώην συντρόφους της</h4>
</a>
<a href="/news/58160/nea-ypoxwrhsh-gia-ta-eidika-misthologia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tsakalotos34.jpg" alt="Νέα υποχώρηση για τα ειδικά μισθολόγια" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span><span class="when">08:04</span></div>
<h3>Νέα υποχώρηση για τα ειδικά μισθολόγια</h3>
<h4>Πιο βαθιά ο «κόφτης» καθώς θα υπολογίζεται και η τυχόν απώλεια εσόδων</h4>
</a>
</div>
<div id="tabs-recent" class="postEntries noimg" style="height:600px; overflow:hidden">
<a href="/themata/58167/ti-doyleia-kanei-shmera-o-prwtos-twn-prwtwn-stis-panelladikes-toy-1990-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/psistariasroiter.jpg" alt="Τι δουλειά κάνει σήμερα ο πρώτος των πρώτων στις Πανελλαδικές του 1990;" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">08:50</span></div>
<h3>Τι δουλειά κάνει σήμερα ο πρώτος των πρώτων στις Πανελλαδικές του 1990;</h3>
<h4>Τον βρήκε ο Αντώνης Σρόιτερ</h4>
</a>
<a href="/news/58166/katatethhkan-oi-tropologies-deite-tis-allages-gia-ekas-enfia-kai-enstoloys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tropologiesekas.jpg" alt="Κατατέθηκαν οι τροπολογίες: Δείτε τις αλλαγές για ΕΚΑΣ, ΕΝΦΙΑ και ένστολους" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span><span class="when">08:43</span></div>
<h3>Κατατέθηκαν οι τροπολογίες: Δείτε τις αλλαγές για ΕΚΑΣ, ΕΝΦΙΑ και ένστολους</h3>
<h4>Κατατέθηκαν αργά το βράδυ της Τετάρτης στη Βουλή</h4>
</a>
<a href="/news/58165/parathyraki-amnhsteyshs-ypoyrgwn-me-offshore-xwris-poinikes-eythynes" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tsiprasmilaei.jpg" alt="«Παραθυράκι» αμνήστευσης υπουργών με offshore χωρίς ποινικές ευθύνες" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">08:33</span></div>
<h3>«Παραθυράκι» αμνήστευσης υπουργών με offshore χωρίς ποινικές ευθύνες</h3>
<h4>Με τη νέα ρύθμιση </h4>
</a>
<a href="/news/58162/megales-ekplhkseis-gia-tis-vaseis-toy-2016" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/panelinies2016.jpg" alt="Μεγάλες εκπλήξεις για τις βάσεις του 2016" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">08:20</span></div>
<h3>Μεγάλες εκπλήξεις για τις βάσεις του 2016</h3>
<h4>Χαμηλώνει ο πήχης εισαγωγής σε δεκάδες σχολές</h4>
</a>
<a href="/themata/58101/to-mple-kommati-ths-gomolastixas-exei-allh-xrhsh-apo-ayth-poy-pisteyame" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/sbistra.jpg" alt="Το μπλε κομμάτι της γομολάστιχας έχει άλλη χρήση από αυτή που πιστεύαμε" border="0" class="lazyload">
</div>
<div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span><span class="when">00:35</span></div>
<h3>Το μπλε κομμάτι της γομολάστιχας έχει άλλη χρήση από αυτή που πιστεύαμε</h3>
<h4>Καταρρίπτεται ένας μύθος</h4>
</a>
<a href="/viral/57738/h-pio-seksi-gymnastria-toy-instagram-den-fovatai-na-deiksei-ta-kallh-ths-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/casidaviscover2.jpg" alt="Η πιο σeξι γυμνάστρια του Ιnstagram δεν φοβάται να δείξει τα κάλλη της" border="0" class="lazyload">
</div>
<div><span class="category">SEXY</span><span class="when">00:35</span></div>
<h3>Η πιο σeξι γυμνάστρια του Ιnstagram δεν φοβάται να δείξει τα κάλλη της</h3>
<h4>Οι άντρες λατρεύουν την Casi Davis</h4>
</a>
<a href="/viral/58115/otan-h-logikh-den-leitoyrgei-kai-h-patenta-einai-monodromos-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/patentes_main.jpg" alt="Όταν η λογική δεν λειτουργεί και η πατέντα είναι μονόδρομος" border="0" class="lazyload">
</div>
<div><span class="category">ΧΙΟΥΜΟΡ</span><span class="when">00:35</span></div>
<h3>Όταν η λογική δεν λειτουργεί και η πατέντα είναι μονόδρομος</h3>
<h4>Κοιτάξτε τι κάνουν οι άνθρωποι για να μην βγουν από την βολή τους!</h4>
</a>
<a href="/viral/57919/10-apo-tis-pio-tromaktikes-pthseis-olwn-twn-epoxwn-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/planes-892432.jpg" alt="10 από τις πιο τρομακτικές πτήσεις όλων των εποχών" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">00:35</span></div>
<h3>10 από τις πιο τρομακτικές πτήσεις όλων των εποχών</h3>
<h4>Πτήσεις που οι επιβάτες δεν θα ξεχάσουν ποτέ</h4>
</a>
<a href="http://www.e-daily.gr/entertainment/58068/ta-festival-toy-kalokairioy-se-olh-thn-ellada-list" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-th-list"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/collageagendafest.jpg" alt="E-Agenda: Τα φεστιβάλ του καλοκαιριού σε όλη την Ελλάδα " border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΗ</span><span class="when">00:35</span></div>
<h3>E-Agenda: Τα φεστιβάλ του καλοκαιριού σε όλη την Ελλάδα </h3>
<h4>Σε πόλεις, σε χωριά και σε νησιά - ανανεώνεται διαρκώς!</h4>
</a>
<a href="/entertainment/57925/den-eiste-paralia-me-ayth-thn-playlist-tha-vretheite-list" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-th-list"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ipodBeach.jpg" alt="Δεν είστε παραλία; Με αυτή την playlist, θα βρεθείτε!" border="0" class="lazyload">
</div>
<div><span class="category">MUSIC LISTS</span><span class="when">00:35</span></div>
<h3>Δεν είστε παραλία; Με αυτή την playlist, θα βρεθείτε!</h3>
<h4>Ό,τι κι αν κάνετε αυτή η λίστα θα σας χαλαρώσει</h4>
</a>
<a href="/life/58113/psaksame-kai-vrhkame-ta-pio-omorfa-sorts-se-top-prosfores-pics-list" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-th-list"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sortskaivermoudes.jpg" alt="Ψάξαμε και βρήκαμε τα πιο όμορφα σορτς σε τοπ προσφορές!" border="0" class="lazyload">
</div>
<div><span class="category">STYLE</span><span class="when">00:35</span></div>
<h3>Ψάξαμε και βρήκαμε τα πιο όμορφα σορτς σε τοπ προσφορές!</h3>
<h4>Καθημερινά, βραδινά και σπορ μέχρι και -70%!!! </h4>
</a>
<a href="/quiz/58146/mporeite-na-deite-to-krymmeno-gramma-quiz" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-doc-text"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/f14945da-ced4-4446-8cb7-2baae1999fe1.jpg" alt="Μπορείτε να δείτε το κρυμμένο γράμμα!" border="0" class="lazyload">
</div>
<div><span class=alert>QUIZ</span><span class="category">BODY+MIND</span><span class="when">00:35</span></div>
<h3>Μπορείτε να δείτε το κρυμμένο γράμμα!</h3>
<h4>Μόνο για αυτούς που έχουν καλή όραση!!!</h4>
</a>
<a href="/viral/58098/sharon-stone-epeteios-24-etwn-apo-th-thrylikh-skhnh-sto-basic-instinct-pics-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/saron5.jpg" alt="Sharon Stone: Επέτειος 24 ετών από τη θρυλική σκηνή στο Basic Instinct" border="0" class="lazyload">
</div>
<div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span><span class="when">00:35</span></div>
<h3>Sharon Stone: Επέτειος 24 ετών από τη θρυλική σκηνή στο Basic Instinct</h3>
<h4>Συνεχίζει να σαγηνεύει το φακό</h4>
</a>
<a href="/quiz/58118/poy-kryvetai-h-arkoyda-sthn-fwtografia-quiz" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-doc-text"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/find_bear_main.jpg" alt="Που κρύβεται η αρκούδα στην φωτογραφία;" border="0" class="lazyload">
</div>
<div><span class=alert>QUIZ</span><span class="category">ΖΩΑ</span><span class="when">00:35</span></div>
<h3>Που κρύβεται η αρκούδα στην φωτογραφία;</h3>
<h4>Μπορείτε να την βρείτε;</h4>
</a>
<a href="/life/58114/idees-gia-4-makaronades-aples-grhgores-kai-pentanostimes-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/13176536_1758400297745945_1941067436_n-720x600.jpg" alt="Ιδέες για 4 μακαρονάδες απλές, γρήγορες και πεντανόστιμες!" border="0" class="lazyload">
</div>
<div><span class="category">FOOD</span><span class="when">00:35</span></div>
<h3>Ιδέες για 4 μακαρονάδες απλές, γρήγορες και πεντανόστιμες!</h3>
<h4>Δοκιμάστε να τις φτιάξετε όλες</h4>
</a>
<a href="/themata/57666/ekanan-kariera-me-ksenes-fwnes-to-skandalo-poy-sygklonise-th-pop-moysikh-to-1990-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/millivanilli.jpg" alt="Έκαναν καριέρα με ξένες φωνές: Το σκάνδαλο που συγκλόνισε τη ποπ μουσική το 1990" border="0" class="lazyload">
</div>
<div><span class="category">ΜΟΥΣΙΚΗ</span><span class="when">00:35</span></div>
<h3>Έκαναν καριέρα με ξένες φωνές: Το σκάνδαλο που συγκλόνισε τη ποπ μουσική το 1990</h3>
<h4>Η κωμικοτραγική ιστορία των Milli Vanilli</h4>
</a>
<a href="/themata/57914/8-merh-ston-kosmo-poy-apagoreyetai-na-pane-gynaikes-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/No-women-sign-014.jpg" alt="8 μέρη στον κόσμο που απαγορεύεται να πάνε γυναίκες" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">00:35</span></div>
<h3>8 μέρη στον κόσμο που απαγορεύεται να πάνε γυναίκες</h3>
<h4>Το Άγιο Όρος δεν είναι το μόνο άβατο στην Γη</h4>
</a>
<a href="/themata/58125/h-limnh-poy-moiazei-me-tsixolfoyska-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/p1ak1p289ar10k7s60p1t7gpb5d_944x449.jpg" alt="Η λίμνη που μοιάζει με τσιχόλφουσκα!" border="0" class="lazyload">
</div>
<div><span class="category">ΠΕΡΙΒΑΛΛΟΝ</span><span class="when">00:35</span></div>
<h3>Η λίμνη που μοιάζει με τσιχόλφουσκα!</h3>
<h4>Τόσο ροζ, που δείχνει ψεύτικη!</h4>
</a>
<a href="/entertainment/58141/moysikes-ston-khpo-toy-megaroy-moysikhs-athhnwn" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/megarokipos2016.jpg" alt="Μουσικές στον Κήπο του Μεγάρου Μουσικής Αθηνών" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΗ</span><span class="when">00:35</span></div>
<h3>Μουσικές στον Κήπο του Μεγάρου Μουσικής Αθηνών</h3>
<h4>Μέγαρο Μουσικής - 11/6/2016 - 13/7/2016</h4>
</a>
<a href="/entertainment/58140/to-koritsi-mesa-sto-spiti-apo-to-theatro-toy-allote" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tokoritsimesastospiti.jpg" alt="Το Κορίτσι μέσα στο Σπίτι από το Θέατρο του `Αλλοτε" border="0" class="lazyload">
</div>
<div><span class="category">ΘΕΑΤΡΟ</span><span class="when">00:35</span></div>
<h3>Το Κορίτσι μέσα στο Σπίτι από το Θέατρο του `Αλλοτε</h3>
<h4>Μπενσουσάν Χαν - 13/6/2016 - 15/6/2016</h4>
</a>
</div>
</div>
<br>
<center>
<div id="5799386"></div>
</center>
<script>
ADTECH.config.placements[5799386] = {params: { alias: '', key:'',target: '_blank' }};
ADTECH.enqueueAd(5799386);
ADTECH.executeQueue();
</script>
<br>
<script>
$().ready(function() {
    $("#topTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  defaultTab: "li#ttab-themata",
	  updateHash: false
	});
/*
	$("#tabs-news").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-viral").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-life").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-themata").mCustomScrollbar({
		theme:"dark-thick"
	});
*/	
});

</script>
<div class="postsRedHeader">δημοφιλέστερα</div>
<div id="topTabs" class="tab-container">
<ul class="etabs">
<li class="tab" id="ttab-news"><a href="#tabs-news">ΕΙΔΗΣΕΙΣ</a></li>
<li class="tab" id="ttab-themata"><a href="#tabs-themata">θΕΜΑΤΑ</a></li>
<li class="tab" id="ttab-life"><a href="#tabs-life">LIFE</a></li>
<li class="tab" id="ttab-viral"><a href="#tabs-viral">VIRAL</a></li>
</ul>
<div id="tabs-news" class="postEntries top">
<a href="/news/58158/daskala-emeine-egkyos-apo-13xrono-mathhth-ths-alla-thn-sthrizoyn-oi-dikoi-toy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/daskalitsa.jpg" alt="Δασκάλα έμεινε έγκυος από 13χρονο μαθητή της αλλά την... στηρίζουν οι δικοί του" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Δασκάλα έμεινε έγκυος από 13χρονο μαθητή της αλλά την... στηρίζουν οι δικοί του</h3>
<h4>Μία απίστευτη ιστορία</h4>
</a>
<a href="/news/58121/kryfos-ekstra-foros-se-mhteres-anhlikwn" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/anilika.jpg" alt="Κρυφός έξτρα φόρος σε μητέρες ανηλίκων" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span></div>
<h3>Κρυφός έξτρα φόρος σε μητέρες ανηλίκων</h3>
<h4>Δείτε τι ισχύει πλέον</h4>
</a>
<a href="/news/58126/xamos-sthn-voylh-sthn-syzhthsh-gia-tis-offshore-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/vouli_xamos_1.jpg" alt="Χαμός στην Βουλή στην συζήτηση για τις offshore" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span></div>
<h3>Χαμός στην Βουλή στην συζήτηση για τις offshore</h3>
<h4>Η ομιλιά του Σταυρου Θεοδωράκη έδωσε αφορμή να «ανάψουν τα αίματα»</h4>
</a>
<a href="/news/58153/sthn-kypro-meiwnoyn-sto-miso-ton-foro-akinhtwn" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/kypros-3464327.jpg" alt="Στην Κύπρο μειώνουν στο μισό τον φόρο ακινήτων" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Στην Κύπρο μειώνουν στο μισό τον φόρο ακινήτων</h3>
<h4>Όπως ανακοίνωσε ο υπουργός Οικονομικών της χώρας</h4>
</a>
<a href="/news/58148/to-brexit-perase-mprosta-sta-gkalop" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/brexit-45343.jpg" alt="Το Brexit πέρασε μπροστά στα γκάλοπ" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Το Brexit πέρασε μπροστά στα γκάλοπ</h3>
<h4>Στις 23 Ιουνίου οι Βρετανοί καλούνται στις κάλπες για να απαντήσουν «ναι» ή «όχι» στην ΕΕ</h4>
</a>
<a href="/news/58157/dyo-nekroi-apo-pyrovolismoys-sto-ucla-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ucla.jpg" alt="Δύο νεκροί από πυροβολισμούς στο UCLA" border="0" class="lazyload">
</div>
<div><span class="category">ΕΙΔΗΣΕΙΣ</span></div>
<h3>Δύο νεκροί από πυροβολισμούς στο UCLA</h3>
<h4>Η πανεπιστημιούπολη παραμένει αποκλεισμένη</h4>
</a>
<a href="/news/58111/egyptair-vrethhke-to-mayro-koyti-ths-moiraias-pthshs" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/egyptair_1.jpg" alt="EgyptAir: Βρέθηκε το μαύρο κουτί της μοιραίας πτήσης" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>EgyptAir: Βρέθηκε το μαύρο κουτί της μοιραίας πτήσης</h3>
<h4>Εντοπίστηκαν σήματα από τις Αιγυπτιακές αρχές</h4>
</a>
<a href="/news/58057/erxontai-nefwseis-kai-vroxes" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/nefoseis.jpg" alt="Έρχονται νεφώσεις και βροχές " border="0" class="lazyload">
</div>
<div><span class="category">ΚΑΙΡΟΣ</span></div>
<h3>Έρχονται νεφώσεις και βροχές </h3>
<h4>Οι προγνώσεις της ΕΜΥ</h4>
</a>
</div>
<div id="tabs-themata" class="postEntries top">
<a href="/themata/57883/giati-ta-agalmata-twn-arxaiwn-ellhnwn-eixan-mikro-peos" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/greekagam1.jpg" alt="Γιατί τα αγάλματα των Αρχαίων Ελλήνων είχαν μικρό πeoς;" border="0" class="lazyload">
</div>
<div><span class="category">ΙΣΤΟΡΙΚΑ</span></div>
<h3>Γιατί τα αγάλματα των Αρχαίων Ελλήνων είχαν μικρό πeoς;</h3>
<h4>Κι όμως υπάρχει απάντηση</h4>
</a>
<a href="/themata/57944/etsi-ezhsa-me-153-eyrw-gia-mia-evdomada-xwris-na-moy-leipsei-tipota-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/portofoli.jpg" alt="Έτσι έζησα με 15,3 ευρώ για μια εβδομάδα, χωρίς να μου λείψει τίποτα" border="0" class="lazyload">
</div>
<div><span class="category">ΣΠΙΤΙ</span></div>
<h3>Έτσι έζησα με 15,3 ευρώ για μια εβδομάδα, χωρίς να μου λείψει τίποτα</h3>
<h4>Ημερολόγιο ενός μπατίρη</h4>
</a>
<a href="/themata/57521/pente-merh-poy-se-plhrwnoyn-gia-na-zhseis-ekei-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/5-Places-That-PAY-You-To-Live-There.jpg" alt="Πέντε μέρη που σε πληρώνουν για να ζήσεις εκεί" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span></div>
<h3>Πέντε μέρη που σε πληρώνουν για να ζήσεις εκεί</h3>
<h4>Έχουν μεγάλη ανάγκη από κατοίκους και δεν τσιγκουνεύονται!</h4>
</a>
<a href="/themata/57488/h-xwra-poy-kanenas-eyrwpaios-de-thelei-na-zhsei" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/ellada-doryforiki-foto.jpg" alt="Η χώρα που κανένας Ευρωπαίος δε θέλει να ζήσει" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Η χώρα που κανένας Ευρωπαίος δε θέλει να ζήσει</h3>
<h4>Και σε ποια χώρα θα ήθελαν να μεταναστεύσουν;</h4>
</a>
<a href="/themata/56963/deite-pws-htan-h-syria-prin-ton-polemo-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/suriapolemos.jpg" alt="Δείτε πώς ήταν η Συρία πριν τον πόλεμο" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Δείτε πώς ήταν η Συρία πριν τον πόλεμο</h3>
<h4>Σπάνια ομορφιά που χάθηκε για πάντα</h4>
</a>
<a href="/themata/58101/to-mple-kommati-ths-gomolastixas-exei-allh-xrhsh-apo-ayth-poy-pisteyame" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/sbistra.jpg" alt="Το μπλε κομμάτι της γομολάστιχας έχει άλλη χρήση από αυτή που πιστεύαμε" border="0" class="lazyload">
</div>
<div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span></div>
<h3>Το μπλε κομμάτι της γομολάστιχας έχει άλλη χρήση από αυτή που πιστεύαμε</h3>
<h4>Καταρρίπτεται ένας μύθος</h4>
</a>
<a href="/themata/46413/top-15-oi-xeiroteroi-efialtes-zwntaneyoyn-sto-nero-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2015/photosswimming16.jpg" alt="Top 15: Οι χειρότεροι εφιάλτες ζωντανεύουν στο νερό!" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΑ</span></div>
<h3>Top 15: Οι χειρότεροι εφιάλτες ζωντανεύουν στο νερό!</h3>
<h4>Είστε έτοιμοι να δείτε τι κρύβει ο βυθός;</h4>
</a>
<a href="/themata/57096/ayto-to-spiti-noikiazetai-1-lira-to-xrono-alla-yparxei-enas-oros-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/oualia_home_0.jpg" alt="Αυτό το σπίτι νοικιάζεται 1 λίρα το χρόνο αλλά υπάρχει ένας όρος" border="0" class="lazyload">
</div>
<div><span class="category">ΑΡΧΙΤΕΚΤΟΝΙΚΗ</span></div>
<h3>Αυτό το σπίτι νοικιάζεται 1 λίρα το χρόνο αλλά υπάρχει ένας όρος</h3>
<h4>Διαβάστε ποιο είναι το τίμημα</h4>
</a>
</div>
<div id="tabs-life" class="postEntries top">
<a href="http://www.e-daily.gr/life/57566/diagwnismos-vinge-project-oneiremena-magio-kai-tsantes-katw-apo-ton-hlio-pics-kerdiste" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-ticket"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/vinge6.jpg" alt="Οι νικητές του διαγωνισμού: Ονειρεμένα μαγιό και τσάντες κάτω από τον ήλιο" border="0" class="lazyload">
</div>
<div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">STYLE</span></div>
<h3>Οι νικητές του διαγωνισμού: Ονειρεμένα μαγιό και τσάντες κάτω από τον ήλιο</h3>
<h4>Κερδίστε ένα μαγιό Vinge Project για το καλοκαίρι!</h4>
</a>
<a href="/life/57994/neos-diagwnismos-h-nadeen-mporei-na-zwgrafisei-paramythia-sta-sandalia-soy-pics-kerdiste" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-ticket"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/SANDALIANADDEEN.jpg" alt="Νέος Διαγωνισμός: H Nadeen μπορεί να ζωγραφίσει παραμύθια στα σανδάλια σου" border="0" class="lazyload">
</div>
<div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">STYLE</span></div>
<h3>Νέος Διαγωνισμός: H Nadeen μπορεί να ζωγραφίσει παραμύθια στα σανδάλια σου</h3>
<h4>Κερδίστε ένα ζευγάρι σανδάλια custom made με ό,τι σχέδιο θέλετε!</h4>
</a>
<a href="/life/57143/trofes-poy-den-lhgoyn-pote" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/meli.jpg" alt="Τροφές που δεν λήγουν ποτέ!" border="0" class="lazyload">
</div>
<div><span class="category">FOOD</span></div>
<h3>Τροφές που δεν λήγουν ποτέ!</h3>
<h4>Μη τις πετάξετε από τo ντουλάπι σας</h4>
</a>
<a href="http://www.e-daily.gr/life/58005/neos-megalos-diagwnismos-kerdiste-ena-taksidi-sthn-skiatho-kerdiste" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-ticket"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/1.jpg" alt="Νέος Μεγάλος Διαγωνισμός: Κερδίστε ένα ταξίδι στην Σκιάθο! " border="0" class="lazyload">
</div>
<div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">ΤΑΞΙΔΙΑ</span></div>
<h3>Νέος Μεγάλος Διαγωνισμός: Κερδίστε ένα ταξίδι στην Σκιάθο! </h3>
<h4>Περιλαμβάνει τα ακτοπλοϊκά εισιτήρια, την διαμονή και τα οδοιπορικά!!</h4>
</a>
<a href="/life/57951/mexri-poia-hlikia-epitrepetai-na-vlepei-to-paidi-gumnoys-toys-goneis" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/goneis.jpg" alt="Μέχρι ποια ηλικία επιτρέπεται να βλέπει το παιδί γuμνούς τους γονείς" border="0" class="lazyload">
</div>
<div><span class="category">FAMILY</span></div>
<h3>Μέχρι ποια ηλικία επιτρέπεται να βλέπει το παιδί γuμνούς τους γονείς</h3>
<h4>Εμπιστευτείτε το ένστικτό σας, λένε οι παιδοψυχολόγοι</h4>
</a>
<a href="/life/57649/h-metamorfwsh-mias-ekklhsias-poy-egine-spiti-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/EKKLISIASPITI.jpg" alt="Η μεταμόρφωση μιας εκκλησίας που έγινε σπίτι! " border="0" class="lazyload">
</div>
<div><span class="category">ΑΡΧΙΤΕΚΤΟΝΙΚΗ</span></div>
<h3>Η μεταμόρφωση μιας εκκλησίας που έγινε σπίτι! </h3>
<h4>Δείτε βήμα προς βήμα την αλλαγή</h4>
</a>
<a href="/life/57760/zwdia-evdomadas-30-maioy-5-ioynioy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/zodiapicnic.jpg" alt="Ζώδια Εβδομάδας: 30 Mαίου - 5 Ιουνίου" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΔΙΑ</span></div>
<h3>Ζώδια Εβδομάδας: 30 Mαίου - 5 Ιουνίου</h3>
<h4>Τι λένε τα άστρα για τις ημέρες που έρχονται;</h4>
</a>
<a href="http://www.e-daily.gr/life/57938/brhkame-tis-kalyteres-prosfores-gia-taksidi-sthn-italia-pics-list" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-th-list"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/italia-napoli3.jpg" alt="Bρήκαμε τις καλύτερες προσφορές για ταξίδι στην Ιταλία!" border="0" class="lazyload">
</div>
<div><span class="category">ΤΑΞΙΔΙΑ</span></div>
<h3>Bρήκαμε τις καλύτερες προσφορές για ταξίδι στην Ιταλία!</h3>
<h4>Τα φθηνότερα εισιτήρια και ξενοδοχεία για Νάπολη και Ρώμη!!</h4>
</a>
</div>
<div id="tabs-viral" class="postEntries top">
<a href="http://www.e-daily.gr/quiz/40339/poio-ellhniko-sirial-perigrafei-kalytera-th-zwh-soy-quiz" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-doc-text"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/places/sirial2.jpg" alt="Ποιο ελληνικό σίριαλ περιγράφει καλύτερα τη ζωή σου;" border="0" class="lazyload">
</div>
<div><span class=alert>QUIZ</span><span class="category">MEDIA</span></div>
<h3>Ποιο ελληνικό σίριαλ περιγράφει καλύτερα τη ζωή σου;</h3>
<h4>Ανάμεσα σε αυτά που άφησαν εποχή</h4>
</a>
<a href="/viral/57973/kypria-ekdikhthhke-ton-prwhn-ths-kai-postare-ta-epwtika-toys-vinteo-sto-facebook" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/aimiliamark.jpg" alt="Κύπρια εκδικήθηκε τον πρώην της και πόσταρε τα εpωτικά τους βίντεο στο Facebook" border="0" class="lazyload">
</div>
<div><span class="category">SEXY</span></div>
<h3>Κύπρια εκδικήθηκε τον πρώην της και πόσταρε τα εpωτικά τους βίντεο στο Facebook</h3>
<h4>Το δημοσιοποίησε για να τον πλήξει σε προσωπικό και επαγγελματικό επίπεδο</h4>
</a>
<a href="/viral/57886/deite-poy-aynanizotan-toyrkos-odhgos-lewforeioy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/aunan.jpg" alt="Δείτε που aυνανιζόταν Τούρκος οδηγός λεωφορείου" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Δείτε που aυνανιζόταν Τούρκος οδηγός λεωφορείου</h3>
<h4>Απίστευτο περιστατικό στη γειτονική χώρα</h4>
</a>
<a href="/viral/58061/poia-anerxomenh-ellhnida-tragoydistria-kseperase-vandh-kai-vissh-sto-youtube-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/malou42.jpg" alt="Ποια ανερχόμενη Ελληνίδα τραγουδίστρια ξεπέρασε Βανδή και Βίσση στο Υoutube" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Ποια ανερχόμενη Ελληνίδα τραγουδίστρια ξεπέρασε Βανδή και Βίσση στο Υoutube</h3>
<h4>Το κορίτσι των 17.000.000 προβολών</h4>
</a>
<a href="/viral/57965/poios-podosfairisths-efage-prostimo-miso-ekat-eyrw-gia-troxaia-paravash" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/polizei8.jpg" alt="Ποιος ποδοσφαιριστής έφαγε πρόστιμο μισό εκατ. ευρώ για τροχαία παράβαση" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span></div>
<h3>Ποιος ποδοσφαιριστής έφαγε πρόστιμο μισό εκατ. ευρώ για τροχαία παράβαση</h3>
<h4>Κι όμως!</h4>
</a>
<a href="/viral/58011/pws-kanei-enas-ellhnas-kamaki-sthn-agglia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/greek_kamaki_1.jpg" alt="Πως κάνει ένας Έλληνας καμάκι στην Αγγλία;" border="0" class="lazyload">
</div>
<div><span class="category">ΧΙΟΥΜΟΡ</span></div>
<h3>Πως κάνει ένας Έλληνας καμάκι στην Αγγλία;</h3>
<h4>Το αθάνατο ελληνικό καμάκι πάει... εξωτερικό</h4>
</a>
<a href="http://www.e-daily.gr/viral/56488/top-10-epika-ksekatiniasmata-sthn-ellhnikh-thleorash-se-ena-vinteo-toy-e-dailygr-video" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/aspatsina34.jpg" alt="TOP 10: Επικά ξεκατινιάσματα στην ελληνική τηλεόραση σε ένα βίντεο του E-Daily.gr" border="0" class="lazyload">
</div>
<div><span class="category">MEDIA</span></div>
<h3>TOP 10: Επικά ξεκατινιάσματα στην ελληνική τηλεόραση σε ένα βίντεο του E-Daily.gr</h3>
<h4>Τα θυμάστε;</h4>
</a>
<a href="/viral/58043/kavgas-sto-namaste-h-epithesh-toy-gkanoy-ston-goymenidh-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Ganos-Goumenidis.jpg" alt="Καβγάς στο NaMaSte: Η επίθεση του Γκάνου στον Γουμενίδη" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Καβγάς στο NaMaSte: Η επίθεση του Γκάνου στον Γουμενίδη</h3>
<h4>Άναυδη η Ναταλία Γερμανού</h4>
</a>
</div>
</div>
</div>
</div>
<div id="footerBgWide">
<center>
<div id="footerMain">
<div class="footer hideOnSmallScreen" style="width:12%;">
<span class="footer_optionsHeader"><a href="/news">Ειδήσεις</a></span><br>
<span class="footer_options">
<a href="/news">Τελευταία νέα</a>
<a href="/ellada">Ελλάδα</a>
<a href="/kosmos">Κόσμος</a>
<a href="/politiki">Πολιτική</a>
<a href="/oikonomia">Οικονομία</a>
<a href="/athlitismos">Αθλητισμός</a>
<a href="/epistimi">Επιστήμη</a>
<a href="/politismos">Πολιτισμός</a>
</span>
</div>
<div class="footer hideOnSmallScreen" style="width:12%;">
<div class="footer_optionsHeader"><a href="/themata">Θέματα</a></div><br>
<div class="footer_optionsHeader"><a href="/quiz">Κουίζ</a></div><br>
<div class="footer_optionsHeader"><a href="/viral">Viral</a></div><br>
<div class="footer_optionsHeader"><a href="/lol">LOL</a></div>
<div class="footer_optionsHeader"><a href="/omg">OMG</a></div>
<div class="footer_optionsHeader"><a href="/juicy">JUICY</a></div>
<div class="footer_optionsHeader"><a href="/alist">A-LIST</a></div>
<div class="footer_optionsHeader"><a href="/geeky">GEEKY</a></div>
<div class="footer_optionsHeader"><a href="/cute">CUTE</a></div>
<div class="footer_optionsHeader"><a href="/retro">RETRO</a></div>
</div>
<div class="footer hideOnSmallScreen" style="width:12%;">
<span class="footer_optionsHeader"><a href="/life">Life</a></span><br>
<span class="footer_options">
<a href="/food">Food</a>
<a href="/bodymind">Body+Mind</a>
<a href="/travel">Ταξίδια</a>
<a href="/style">Style</a>
<a href="/spiti">Σπίτι</a>
<a href="/family">Family</a>
<a href="/sxeseis">Σχέσεις</a>
</span><br>
</div>
<div class="footer hideOnSmallScreen" style="width:18%;">
<div class="footer_optionsHeader"><a href="/entertainment">Ψυχαγωγία</a></div><br>
<span class="footer_options">
<a href="/agenda">Agenda</a>
<a href="/wintickets">Κερδίστε Προσκλήσεις</a><br>
<a href="/music">Μουσική</a>
<a href="/theater">Θέατρο+Χορός</a>
<a href="/arts">Εικαστικά</a>
<a href="/cinema">Σινεμά</a>
<a href="/nightlife">Νάιτ Λάιφ</a>
<a href="/city">Πόλη</a>
</span>
</div>
<div class="footer fullOnSmallScreen" style="width:32%;">
<div class="footer_optionsHeader">Newsletter</div><br>
<style type="text/css">#mc-embedded-subscribe-form{margin:0;padding:0}#mc_embed_signup{display:table;clear:both;text-align:left;line-height:normal;}#mc_embed_signup .mc-field-group{display:table;margin:10px 0 10px 0;padding:0;width:100%}#mc_embed_signup li{display:block;float:left;margin-top:12px;width:33.333%;padding:0;font-weight:400;font-size:1.25em;}#mc_embed_signup .checkbox{width:22px;height:22px;border:1px solid #e3e3e3;background:#fff;margin:0;padding:0}#mc_embed_signup .checkbox:before{content:"";display:block;width:23px;height:23px;background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 0 0 no-repeat;}#mc_embed_signup .checkbox:checked:before{background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') -32px 0 no-repeat;}#mc_embed_signup label{margin-left:5px;padding-top:-5px;}#mc_embed_signup .email{font-weight:normal;height:45px;width:55%;float:left;display:inline-block;background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 10px -22px no-repeat;color:#000;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;padding:0 0 0 46px;border:1px solid #d9d9d9}#mc_embed_signup .email.focus{color:#000;}#mc_embed_signup .email.placeholder{color:#e3e3e3;}#mc_embed_signup .submit-button{background:#0079ff;text-align:center;float:left;width:30%;height:47px;border:none;font-size:14px;text-transform:uppercase;color:#FFF;letter-spacing:1px;cursor:pointer;display:inline-block;padding-top:6px;margin-left:3px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;}#mc_embed_signup .submit-button:hover{background:#2e78b3;text-decoration:none;}#mc_embed_signup div.mce_inline_error{background-color:#fff;color:#804040;}.clearfix{padding:0;margin:0}</style>
<div id="mc_embed_signup" class="clearfix">
<form action="//nextweb.us10.list-manage.com/subscribe/post?u=037b671af872ebf3eb1ceac4a&amp;id=5ab11026ce" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div class="clearfix">Εγραφείτε στα newsletters του E-Daily.gr για να μαθαίνετε τα καλύτερα στο email σας:</div>
<ul class="mc-field-group input-group">
<li>
<input type="checkbox" class="checkbox" value="1" name="group[1109][1]" id="mce-group[1109]-1109-0" checked="checked">
<label for="mce-group[1109]-1109-0">Ειδήσεις</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="2" name="group[1109][2]" id="mce-group[1109]-1109-1" checked="checked">
<label for="mce-group[1109]-1109-1">Viral</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="4" name="group[1109][4]" id="mce-group[1109]-1109-2" checked="checked">
<label for="mce-group[1109]-1109-2">Life</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="8" name="group[1109][8]" id="mce-group[1109]-1109-3" checked="checked">
<label for="mce-group[1109]-1109-3">E-Agenda</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="16" name="group[1109][16]" id="mce-group[1109]-1109-4" checked="checked">
<label for="mce-group[1109]-1109-4">E-Weekly</label>
</li>
<li></li>
</ul>
<div class="mc-field-group" style="margin-top:25px;">
<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Συμπληρώστε το email σας">
<input type="submit" class="submit-button" value="ΕΓΓΡΑΦΗ" name="subscribe" id="mc-embedded-subscribe">
</div>
</form>
<div id="mce-responses" class="clear">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>
<div style="position: absolute; left: -5000px;"><input type="text" name="b_037b671af872ebf3eb1ceac4a_5ab11026ce" tabindex="-1" value=""></div>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email'; /*
 * Translated default messages for the $ validation plugin.
 * Locale: EL
 */
$.extend($.validator.messages, {
	required: "<span class=asterisk></span> Αυτό το πεδίο είναι υποχρεωτικό.",
	remote: "<span class=asterisk></span> Παρακαλώ διορθώστε αυτό το πεδίο.",
	email: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη διεύθυνση email.",
	url: "<span class=asterisk></span> Παρακαλώ εισάγετε ένα έγκυρο URL.",
	date: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία.",
	dateISO: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία (ISO).",
	number: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό.",
	digits: "<span class=asterisk></span> Παρακαλώ εισάγετε μόνο αριθμητικά ψηφία.",
	creditcard: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό πιστωτικής κάρτας.",
	equalTo: "<span class=asterisk></span> Παρακαλώ εισάγετε την ίδια τιμή ξανά.",
	accept: "<span class=asterisk></span> Παρακαλώ εισάγετε μια τιμή με έγκυρη επέκταση αρχείου.",
	maxlength: $.validator.format("<span class=asterisk></span> Παρακαλώ εισάγετε μέχρι και {0} χαρακτήρες."),
	minlength: $.validator.format("Παρακαλώ εισάγετε τουλάχιστον {0} χαρακτήρες."),
	rangelength: $.validator.format("Παρακαλώ εισάγετε μια τιμή με μήκος μεταξύ {0} και {1} χαρακτήρων."),
	range: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεταξύ {0} και {1}."),
	max: $.validator.format("Παρακαλώ εισάγετε μια τιμή μικρότερη ή ίση του {0}."),
	min: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεγαλύτερη ή ίση του {0}.")
});}(jQuery));var $mcj = jQuery.noConflict(true);</script>
 
</div>
<div class="footer footerRight footer_options fullOnSmallScreen" style="width:14%; text-align: right">
<div class="footer_optionsHeader">Follow us</div><br>
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<span>Facebook</span>
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<span>Twitter</span>
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<span>Google+</span>
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<span>Instagram</span>
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<span>YouTube</span>
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<span>RSS</span>
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</div>
</center>
</div>
<center>
<div id="footerMain">
<div class="footer footer21">
<a href="javascript:openContact();">Επικοινωνία</a>&nbsp;&nbsp;&nbsp;
<a href="javascript:openContact();">Διαφήμιση</a>&nbsp;&nbsp;&nbsp;
<a href="/rules.asp">Όροι Διαγωνισμών</a>&nbsp;&nbsp;&nbsp;
<a href="/privacy.asp">Privacy</a>&nbsp;&nbsp;&nbsp;
<a href="/terms.asp">Terms of use</a>
<br>
<a href="http://www.e-radio.gr" target="_blank">E-Radio Hellas </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.e-radio.com.cy" target="_blank">E-Radio Cyprus </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.e-radio.co.uk" target="_blank">E-Radio UK </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.onestreaming.com" target="_blank">One Streaming </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.arionradio.com" target="_blank">Arion Radio </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.athensparty.com" target="_blank">Athens Party </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.akous.gr" target="_blank">Akous </a> &nbsp;&nbsp;&nbsp;
<br>
© 2015 All Right Reserved. "E-DAILY" is a registered trademark
</div>
<div class="footer footer22">
<a href="http://www.nextweb.gr" target="_blank"><img src="/_img/nextweb.png" border="0" alt="Nextweb"></a>
</div>
</div>
</center>
</div>
</div>
<script type='text/javascript'>
$().ready(function() {
	
	if( $('#topHeaderMobile').length ){

		$('#mobilemenu_pad').infinitypush({
			openingspeed: 300,
			closingspeed: 300
		});
		
		/*
		$('#mobilemenu_icon').click(function(){
			
			$(this).toggleClass('open');
			//slideout.toggle();
			
		});
	
		slideout.on('open', function() {
			if ($('#mobilemenu_icon').hasClass('open')==false){$('#mobilemenu_icon').addClass('open')}
		});
		slideout.on('close', function() {
			if ($('#mobilemenu_icon').hasClass('open')==true){$('#mobilemenu_icon').removeClass('open')}
	
		});
		*/
	}

});
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-551bf8914936f63f" async="async"></script>
<script type="text/javascript">

	<!-- Google -->
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-504700-32', 'auto');
  ga('send', 'pageview');

	<!-- Tynt -->
	if(document.location.protocol=='http:'){
	 var Tynt=Tynt||[];Tynt.push('aFq4livo0r4yzYacwqm_6r');Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false};
	 (function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
	}
	
	<!-- Quantcast Tag -->
	var _qevents = _qevents || [];
	
	(function() {
	var elem = document.createElement('script');
	elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
	elem.async = true;
	elem.type = "text/javascript";
	var scpt = document.getElementsByTagName('script')[0];
	scpt.parentNode.insertBefore(elem, scpt);
	})();
	
	_qevents.push({
	qacct:"p-49SuwTaH2w2BM"
	});

</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-49SuwTaH2w2BM.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6032644253807', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6032644253807&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1"/></noscript>
 
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3125699,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('http://s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="http://www.histats.com" target="_blank"><img src="http://sstatic1.histats.com/0.gif?3125699&101" alt="web hit counter" border="0"></a></noscript>
<script type="text/javascript">
( function() {
    var info = getTagInfo();
    var alias = 'edhomeskin_display';
        alias = alias !== '' ? 'alias=' + ( alias.split("_") ? alias.split("_")[ 0 ] : alias ) + '_' + info.alias : '';
    document.write('<scr'+'ipt language="javascript1.1" src="http://' + info.domain + '.adtech.de/addyn/3.0/1370.1/4713596/0/-1/ADTECH;' + alias + ';loc=100;target=_blank;key=;grp=' + info.adgroupid + ';misc=' + new Date().getTime() + '"></scri'+'pt>');
})();</script>
<noscript>Please enable scripting languages in your browser.</noscript>
</body>
</html>
