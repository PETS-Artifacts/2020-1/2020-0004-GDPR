
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>E-Daily.gr Πολιτική Απορρήτου</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="charset" content="utf-8"/>
<meta http-equiv="Refresh" content="900"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta content="Πολιτική Απορρήτου" name="Description"/>
<meta content="" name="keywords"/>
<meta property="og:title" content="E-Daily.gr Πολιτική Απορρήτου"/>
<meta property="og:description" content=""/>
<meta property="og:site_name" content="E-Daily.gr"/>
<meta property="og:type" content="Website"/>
<meta property="og:image" content="http://www.e-daily.gr/_img/fblogo.png"/>
<meta property="fb:app_id" content="1590239487856378"/>
<meta property="og:url" content="http://www.e-daily.gr"/>
<link rel="canonical" href="http://www.e-daily.gr"/>
<link rel="stylesheet" href="/_css/post.css"/>
<link rel="stylesheet" href="/_js/scrollbar/jquery.mCustomScrollbar.css"/>
<meta name="apple-itunes-app" content="app-id=776881885">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="/_ico/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/_ico/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/_ico/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/_ico/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/_ico/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/_ico/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/_ico/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/_ico/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/_ico/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/_ico/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/_ico/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/_ico/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/_ico/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/_ico/manifest.json">
<link rel="mask-icon" href="/_ico/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/_ico/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/_ico/mstile-144x144.png">
<meta name="msapplication-config" content="/_ico/browserconfig.xml">
<meta name="theme-color" content="#ff0004">
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,400italic|Roboto+Condensed:300,400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href="/_css/style.css" rel="stylesheet" type="text/css"/>
<link href="/_css/navigation.css" rel="stylesheet" type="text/css"/>
<link href="/_js/menu/mgmenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/mobilemenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/structure.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/_img/forecastfont/weather.css"/>
<link rel="stylesheet" href="/_img/edailyicons/css/edaily.css">
<link rel="stylesheet" href="/_img/edailyicons/css/animation.css"><!--[if IE 7]><link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->
<link rel="stylesheet" type="text/css" href="/_js/mmenu/jquery.ma.infinitypush.css"/>
<script language="javaScript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script language="javaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/1.0.1/lazysizes.min.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/autocomplete/jquery.autocomplete.min.js"></script>
<script language="javaScript" type="text/javascript" src="http://www.e-radio.gr/cache/mediadata_1.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.popupWindow.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.socialstats.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/menu/mgmenu_plugins.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/menu/mgmenu.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/mmenu/jquery.ma.infinitypush.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/tools_all.js"></script>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
<script type='text/javascript'>
if(document.location.protocol=='http:'){
var Tynt=Tynt||[];Tynt.push('d91U86Bp0r5zpGrkHcnlKl');
Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false,"domain":"e-daily.gr"};
(function(){var h,s=document.createElement('script');s.src='http://cdn.tynt.com/ti.js';
h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();}
</script>
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"http://www.e-daily.gr/privacy.asp","theme":"light-bottom"};
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dt/common/DAC.js"></script>
<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dac/1370.1/w1122524.js"></script>
<script type="text/javascript" id="ean-native-embed-tag" src="//cdn.elasticad.net/native/serve/js/nativeEmbed.gz.js"></script>
<script src="/_js/tabs/jquery.easytabs.js" type="text/javascript"></script>
<script src="/_js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body>
<script type='text/javascript'>
  var nuggprof = '';
  var nuggtg = encodeURIComponent('__CONTENT_TAG__');
  var nuggrid = encodeURIComponent(top.location.href);
  document.write('<scr'+'ipt type="text/javascript" src="//adweb.nuggad.net/rc?nuggn=1230610253&nuggsid=1022102324&nuggtg='+nuggtg+'&nuggrid='+nuggrid+'"></scr'+'ipt>');
</script>
<div id="wrapper">
<nav id="mobilemenu_pad">
<ul class="sections">
<li><a href="/news">Ειδήσεις</a>
<li><a href="/viral">Viral</a>
<li><a href="/quiz">Quiz</a>
<li><a href="/life">Life</a>
<li><a href="/themata">Θέματα</a>
<li><a href="/entertainment">Ψυχαγωγία</a>
<li><a href="/agenda">E-Agenda</a>
</ul>
<ul class="sections badges">
<li><a href="/omg"><img src="/_img/feeds/101.png" border="0"/> OMG</a>
<li><a href="/lol"><img src="/_img/feeds/106.png" border="0"/> LOL</a>
<li><a href="/woman"><img src="/_img/feeds/112.png" border="0"/> Woman</a>
<li><a href="/alist"><img src="/_img/feeds/91.png" border="0"/> A-List</a>
<li><a href="/cute"><img src="/_img/feeds/109.png" border="0"/> Cute</a>
<li><a href="/retro"><img src="/_img/feeds/110.png" border="0"/> Retro</a>
<li><a href="/juicy"><img src="/_img/feeds/104.png" border="0"/> Juicy</a>
<li><a href="/lgbt"><img src="/_img/feeds/55.png" border="0"/> LGBT</a>
<li><a href="/geeky"><img src="/_img/feeds/108.png" border="0"/> Geeky</a>
<li><a href="/win"><img src="/_img/feeds/111.png" border="0"/> Win</a>
<li><a href="/summer"><img src="/_img/feeds/15.png" border="0"/> Summer</a>
<li><a href="/xmas"><img src="/_img/feeds/14.png" border="0"/> Xmas</a>
<li><a href="/summer"><img src="/_img/feeds/12.png" border="0"/> Easter</a>
</ul>
<ul class="sections topics">
<li><a href="/food">Food</a>
<li><a href="/bodymind">Body+Mind</a>
<li><a href="/travel">Ταξίδια</a>
<li><a href="/style">Style</a>
<li><a href="/spiti">Σπίτι</a>
<li><a href="/family">Family</a>
<li><a href="/sxeseis">Σχέσεις</a>
</ul>
<ul class="sections topics">
<li><a href="/agenda">Agenda</a>
<li><a href="/music">Μουσική</a>
<li><a href="/theater">Θέατρο</a>
<li><a href="/arts">Εικαστικά</a>
<li><a href="/cinema">Σινεμά</a>
<li><a href="/nightlife">Νάιτ Λάιφ</a>
<li><a href="/city">Πόλη</a>
</ul>
<ul class="sections topics">
<li><a href="/weather">Καιρός</a>
</ul>
<div class="social">
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</nav>
<div id="main">
<form id="skinform"></form>
<div id="spider"></div>
<div id="topHeaderWarper">
<div id="topHeader">
<div id="topHeaderA">
<a href="/" class="mainlogo"><img src="/_img/edailygr_beta_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία"/></a>
</div>
<div id="topHeaderB" class="feedIcons">
<a href="/win"><img src="/_img/feeds/111.png" border="0" alt="Win Feed"/></a>
<a href="/summer"><img src="/_img/feeds/15.png" border="0" alt="Summer Feed"/></a>
<a href="/woman"><img src="/_img/feeds/112.png" border="0" alt="Woman Feed"/></a>
<a href="/omg"><img src="/_img/feeds/101.png" border="0" alt="OMG Feed"/></a>
<a href="/lol"><img src="/_img/feeds/106.png" border="0" alt="LOL Feed"/></a>
<a href="/juicy"><img src="/_img/feeds/104.png" border="0" alt="Juicy Feed"/></a>
<a href="/alist"><img src="/_img/feeds/91.png" border="0" alt="A-List Feed"/></a>
<a href="/retro"><img src="/_img/feeds/110.png" border="0" alt="Retro Feed"/></a>
<a href="/lgbt"><img src="/_img/feeds/55.png" border="0" alt="LGBT Feed"/></a>
</div>
</div>
</div>
<div id="topHeaderWarper2">
<div id="mainmenu" class="mgmenu_container">
<ul class="mgmenu">
<li class="mgmenu_button"></li> 
<div id="mainmenu_logo"><a href="/"><img src="/_img/edailygr_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία" height="33"/></a></div>
<li><span class="mgmenu_drop"><a href="/news">Ειδήσεις</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/news/63843/h-prwth-dhmoskophsh-meta-tis-thleoptikes-adeies" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΙΤΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η πρώτη δημοσκόπηση μετά τις τηλεοπτικές άδειες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ekloges-kalpi.jpg" alt="Η πρώτη δημοσκόπηση μετά τις τηλεοπτικές άδειες" border="0" class="cropimg lazyload">
</a>
<a href="/news/63842/fwtografies-sok-apo-ton-sovaro-traymatismoy-fwtografoy-sthn-klhrwsh-ths-football-league" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΑΘΛΗΤΙΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Φωτογραφίες-σοκ από τον σοβαρό τραυματισμού φωτογράφου στην κλήρωση της Football League</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Koukouloforoi_league_0.jpg" alt="Φωτογραφίες-σοκ από τον σοβαρό τραυματισμού φωτογράφου στην κλήρωση της Football League" border="0" class="cropimg lazyload">
</a>
<a href="/news/63797/poioi-dhmosiografoi-pane-sto-neo-kanali-toy-marinakh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>MEDIA</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ποιοι δημοσιογράφοι πάνε στο νέο κανάλι του Μαρινάκη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/marinakhs_kanali_11.jpg" alt="Ποιοι δημοσιογράφοι πάνε στο νέο κανάλι του Μαρινάκη" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3">
<div class="str8">
<a href="/news" class="more">ΠΕΡΙΣΣΟΤΕΡΕΣ ΕΙΔΗΣΕΙΣ</a>
<a href="/news/63838/entopisthke-se-komhth-to-rompotaki-philae" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΠΙΣΤΗΜΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Εντοπίστηκε σε κομήτη το ρομποτάκι Philae</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/philae-9394.jpg" alt="Εντοπίστηκε σε κομήτη το ρομποτάκι Philae" border="0" class="cropimg lazyload">
</a>
<a href="/news/63840/lamia-xamos-gia-to-onoma-se-vaftisia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Λαμία: Χαμός για το όνομα σε βαφτίσια</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/baptisi600_818087889.jpg" alt="Λαμία: Χαμός για το όνομα σε βαφτίσια" border="0" class="cropimg lazyload">
</a>
<a href="/news/63841/14xronos-odhgos-traymatise-mwro-me-agrotiko" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">14χρονος οδηγός τραυμάτισε μωρό με αγροτικό</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/koi-agrotiko-fillipiada1.jpg" alt="14χρονος οδηγός τραυμάτισε μωρό με αγροτικό" border="0" class="cropimg lazyload">
</a>
<a href="/news/63837/40000-toyristes-exase-h-lesvos-to-fetino-kalokairi" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">40.000 τουρίστες «έχασε» η Λέσβος το φετινό καλοκαίρι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Lesvos_IMG_2809_web.jpeg" alt="40.000 τουρίστες «έχασε» η Λέσβος το φετινό καλοκαίρι" border="0" class="cropimg lazyload">
</a>
<a href="/news/63831/h-apanthsh-ths-ieras-synodoy-gia-thn-katarghsh-ths-proseyxhs-sta-sxoleia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η απάντηση της Ιεράς Συνόδου για την κατάργηση της προσευχής στα σχολεία</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/prosefxh_1.jpg" alt="Η απάντηση της Ιεράς Συνόδου για την κατάργηση της προσευχής στα σχολεία" border="0" class="cropimg lazyload">
</a>
</div>
</div>
<div class="col_12 inlineMenu">
<a href="/news">ΤΕΛΕΥΤΑΙΑ ΝΕΑ</a>
<a href="/ellada">ΕΛΛΑΔΑ</a>
<a href="/kosmos">ΚΟΣΜΟΣ</a>
<a href="/politiki">ΠΟΛΙΤΙΚΗ</a>
<a href="/oikonomia">ΟΙΚΟΝΟΜΙΑ</a>
<a href="/sports">ΑΘΛΗΤΙΣΜΟΣ</a>
<a href="/epistimi">ΕΠΙΣΤΗΜΗ</a>
<a href="/politismos">ΠΟΛΙΤΙΣΜΟΣ</a>
<a href="http://feeds.feedburner.com/eradioblog" target="_blank">RSS FEED</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/themata">Θέματα</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="http://www.e-daily.gr/themata/63825/ta-35-must-toy-foithth-sthn-athhna-apo-to-e-dailygr" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΑΦΙΕΡΩΜΑΤΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα 35 must του φοιτητή στην Αθήνα από το E-Daily.gr</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/athina-83883.jpg" alt="Τα 35 must του φοιτητή στην Αθήνα από το E-Daily.gr" border="0" class="cropimg lazyload">
</a>
<a href="/themata/63811/san-shmera-ta-shmantikotera-gegonota-ths-6hs-septemvrioy-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΙΣΤΟΡΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Σαν σήμερα: Τα σημαντικότερα γεγονότα της 6ης Σεπτεμβρίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/6th_september.jpg" alt="Σαν σήμερα: Τα σημαντικότερα γεγονότα της 6ης Σεπτεμβρίου" border="0" class="cropimg lazyload">
</a>
<a href="/themata/63700/ta-paidia-sth-syria-den-stamatoyn-na-einai-paidia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα παιδιά στη Συρία δεν σταματούν να είναι παιδιά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/pisisyria.jpg" alt="Τα παιδιά στη Συρία δεν σταματούν να είναι παιδιά" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΑ ΘΕΜΑΤΑ</a>
<a href="/themata/63699/h-mama-poy-asprizei-ta-paidia-ths-h-egkyos-iepodoylh-kai-alla-ekswfrenika-sthn-amerikanikh-tv-video-list" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-th-list"></span></div>
<div class="menuItem_title">H μαμά που ασπρίζει τα παιδιά της, η έγκυος ιεpόδουλη και άλλα εξωφρενικά στην αμερικανική tv</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tyrabanksshow2.jpg" alt="H μαμά που ασπρίζει τα παιδιά της, η έγκυος ιεpόδουλη και άλλα εξωφρενικά στην αμερικανική tv" border="0" class="cropimg lazyload">
</a>
<a href="http://www.e-daily.gr/themata/63634/top-5-xeirotera-logotypa-se-ena-vinteo-toy-e-dailygr-video" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=category>DESIGN</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Top 5 χειρότερα λογότυπα σε ένα βίντεο του E-Daily.gr</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/failedlogosmain.png" alt="Top 5 χειρότερα λογότυπα σε ένα βίντεο του E-Daily.gr" border="0" class="cropimg lazyload">
</a>
<a href="/themata/63516/h-megalyterh-apeilh-gia-thn-anthrwpothta" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΕΡΙΒΑΛΛΟΝ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η μεγαλύτερη απειλή για την ανθρωπότητα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/klimalagi.jpg" alt="Η μεγαλύτερη απειλή για την ανθρωπότητα" border="0" class="cropimg lazyload">
</a>
<a href="/themata/63454/giati-to-prwi-prepei-na-pineis-nero-kai-oxi-kafe-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Γιατί το πρωί πρέπει να πίνεις νερό και όχι καφέ</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/morning_water_1.jpg" alt="Γιατί το πρωί πρέπει να πίνεις νερό και όχι καφέ" border="0" class="cropimg lazyload">
</a>
<a href="http://go.linkwi.se/z/10628-12/CD15753/?" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Κερδίστε 10€ δωροεπιταγή για τις αγορές σας σε επώνυμα παπούτσια και ρούχα! </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/spartoo2.png" alt="Κερδίστε 10€ δωροεπιταγή για τις αγορές σας σε επώνυμα παπούτσια και ρούχα!  " border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
<a href="/afieromata">ΑΦΙΕΡΩΜΑΤΑ</a>
<a href="/eikones">ΩΡΑΙΕΣ ΕΙΚΟΝΕΣ</a>
<a href="/istorika">ΙΣΤΟΡΙΚΑ</a>
<a href="/gay-lesbian">LGBT</a>
<a href="/tech">ΤΕΧΝΟΛΟΓΙΑ</a>
<a href="/periballon">ΠΕΡΙΒΑΛΛΟΝ</a>
<a href="/architect">ΑΡΧΙΤΕΚΤΟΝΙΚΗ</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/viral">Viral</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/viral/63207/diashmoi-poy-dhlwsan-bisexual" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Διάσημοι που δήλωσαν bisexual</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/loi.jpg" alt="Διάσημοι που δήλωσαν bisexual" border="0" class="cropimg lazyload">
</a>
<a href="/viral/63761/deite-ton-giannh-xaroylh-prin-apo-13-xronia-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Δείτε τον Γιάννη Χαρούλη πριν από 13 χρόνια!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/xaroulis_13_0.jpg" alt="Δείτε τον Γιάννη Χαρούλη πριν από 13 χρόνια!" border="0" class="cropimg lazyload">
</a>
<a href="/viral/63839/me-fylakish-apeileitai-montelo-poy-postare-fwtografia-gumnhs-hlikiwmenhs-pics-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-picture"></span></div>
<div class="menuItem_title">Με φυλάκιση απειλείται μοντέλο που πόσταρε φωτογραφία γuμνής ηλικιωμένης</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/model-pkamf.jpg" alt="Με φυλάκιση απειλείται μοντέλο που πόσταρε φωτογραφία γuμνής ηλικιωμένης" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΟ VIRAL</a>
<a href="/viral/63822/h-rita-ora-sxedon-gumnh-kai-me-anoixta-podia-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>SEXY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Η Rita Ora σχεδόν γuμνή και με... ανοιχτά πόδια!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/rita_ora_321.jpg" alt="Η Rita Ora σχεδόν γuμνή και με... ανοιχτά πόδια!" border="0" class="cropimg lazyload">
</a>
<a href="/viral/63834/aytos-einai-o-titlos-ths-neas-meshmerianhs-ekpomphs-ths-xristinas-lampirh-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Αυτός είναι ο τίτλος της νέας μεσημεριανής εκπομπής της Χριστίνας Λαμπίρη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/xristina-labiri-people-127.jpg" alt="Αυτός είναι ο τίτλος της νέας μεσημεριανής εκπομπής της Χριστίνας Λαμπίρη" border="0" class="cropimg lazyload">
</a>
<a href="/viral/63820/oyres-thn-nyxta-sthn-khfisias-gia-na-graftoyn-se-gymnasthrio-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Ουρές την νύχτα στην Κηφισίας για να γραφτούν σε γυμναστήριο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/gymnasthrio_1_1.jpg" alt="Ουρές την νύχτα στην Κηφισίας για να γραφτούν σε γυμναστήριο" border="0" class="cropimg lazyload">
</a>
<a href="/viral/63818/o-pythwnas-poy-se-tromazei-akomh-kai-apo-thn-eikona-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Ο πύθωνας που σε τρομάζει ακόμη και από την εικόνα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/pithonas_afriki1.jpg" alt="Ο πύθωνας που σε τρομάζει ακόμη και από την εικόνα" border="0" class="cropimg lazyload">
</a>
<a href="/viral/63813/fwtografia-adeiazoyn-to-grafeio-ths-tremh-sto-mega-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>MEDIA</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Φωτογραφία: Αδειάζουν το γραφείο της Τρέμη στο Mega</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tremi0mega.jpg" alt="Φωτογραφία: Αδειάζουν το γραφείο της Τρέμη στο Mega" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
<a href="/omg">OMG</a>
<a href="/lol">LOL</a>
<a href="/tabloid">TABLOID</a>
<a href="/sexy">SEXY</a>
<a href="/zoa">ΖΩΑ</a>
<a href="/media">MEDIA</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/quiz">Κουίζ</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/quiz/63789/vreite-ton-titlo-toy-tragoydioy-apo-enan-mono-stixo-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Βρείτε τον τίτλο του τραγουδιού από έναν μόνο στίχο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/rouvas8.jpg" alt="Βρείτε τον τίτλο του τραγουδιού από έναν μόνο στίχο" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/63625/mporeite-na-vreite-poio-faghto-exei-tis-perissoteres-thermides-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείτε να βρείτε ποιο φαγητό έχει τις περισσότερες θερμίδες; </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/foodvsfood.jpg" alt="Μπορείτε να βρείτε ποιο φαγητό έχει τις περισσότερες θερμίδες; " border="0" class="cropimg lazyload">
</a>
<a href="/quiz/63546/mporeite-na-vreite-to-pio-akrivo-forema-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείτε να βρείτε το πιο ακριβό φόρεμα;;;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/foremaquizmain.png" alt="Μπορείτε να βρείτε το πιο ακριβό φόρεμα;;;" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΑ QUIZ</a>
<a href="/quiz/63559/poia-ellhnikh-polh-vlepeis-sthn-eikona-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ποία ελληνική πόλη βλέπεις στην εικόνα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/limni_kastorias_39646.jpg" alt="Ποία ελληνική πόλη βλέπεις στην εικόνα;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/63446/mporeis-na-vreis-poso-kostizoyn-aytes-oi-treles-syskeyes-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείς να βρεις πόσο κοστίζουν αυτές οι τρελές συσκευές;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/enhanced-4110-1470843832-3.jpg" alt="Μπορείς να βρεις πόσο κοστίζουν αυτές οι τρελές συσκευές;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/63539/einai-nwris-gia-na-poyme-kalo-xeimwna-vote" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΨΗΦΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Είναι νωρίς για να πούμε «καλό χειμώνα»;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/SUMMERWINTER.jpg" alt="Είναι νωρίς για να πούμε «καλό χειμώνα»;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/63480/posa-krymmena-panta-yparxoyn-sthn-eikona-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Πόσα κρυμμένα πάντα υπάρχουν στην εικόνα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/c0828860-2ff7-418d-a1f5-e436f79dec79.jpg" alt="Πόσα κρυμμένα πάντα υπάρχουν στην εικόνα;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/63287/to-pio-dyskolo-quiz-me-diafores-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Το πιο δύσκολο quiz με διαφορές!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/find_idea.png" alt="Το πιο δύσκολο quiz με διαφορές!" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/life">Life</a></span>
<div class="dropdown_fullwidth mgmenu_tabs">
<div class="mgmenu_width">
<ul class="mgmenu_tabs_nav">
<li><a href="/life" rel="section2_0" class="current">ΤΕΛΕΥΤΑΙΑ θΕΜΑΤΑ</a></li>
<li><a href="/food" rel="section2_1">FOOD</a></li>
<li><a href="/body-mind" rel="section2_2">BODY+MIND</a></li>
<li><a href="/spiti" rel="section2_3">ΣΠΙΤΙ</a></li>
<li><a href="/travel" rel="section2_4">ΤΑΞΙΔΙΑ</a></li>
<li><a href="/style" rel="section2_5">STYLE</a></li>
<li><a href="/family" rel="section2_6">FAMILY</a></li>
<li><a href="/sxeseis" rel="section2_7">ΣΧΕΣΕΙΣ</a></li>
<li><a href="/zodia" rel="section2_8">ΖΩΔΙΑ</a></li>
</ul>
<div class="mgmenu_tabs_panels">
<div id="section2_0" class="menuPosts">
<a href="https://go.linkwi.se/z/10632-0/CD15753/?lnkurl=http%3A%2F%2Fwww.brandsgalaxy.gr%2Fcampaigns%2Fshoe-heaven%2F" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Shoe Haven με εκπτώσεις έως και 70%</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/shoeheaven.png" alt="Shoe Haven με εκπτώσεις έως και 70%" border="0" class="cropimg lazyload">
</a>
<a href="/life/63806/sta-5-kalytera-ksenodoxeia-toy-2016-vrisketai-kai-ena-ellhniko-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Στα 5 καλύτερα ξενοδοχεία του 2016 βρίσκεται και ένα ελληνικό!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/umaid.jpg" alt="Στα 5 καλύτερα ξενοδοχεία του 2016 βρίσκεται και ένα ελληνικό!" border="0" class="cropimg lazyload">
</a>
<a href="/life/63804/ti-na-dialeksw-patata-h-ryzi" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Tι να διαλέξω: Πατάτα ή ρύζι;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/patatbraste.jpg" alt="Tι να διαλέξω: Πατάτα ή ρύζι;" border="0" class="cropimg lazyload">
</a>
<a href="/life/63784/ta-zwdia-shmera-6-septemvrioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 6 Σεπτεμβρίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/kisskiss2.jpg" alt="Τα ζώδια σήμερα: 6 Σεπτεμβρίου" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_1" class="menuPosts mgmenu_tabs_hide">
<a href="/life/63521/pleon-oi-thganhtes-patates-eginan-epidorpio-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Πλέον οι τηγανητές πατάτες έγιναν επιδόρπιο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/patatesepidor.jpg" alt="Πλέον οι τηγανητές πατάτες έγιναν επιδόρπιο" border="0" class="cropimg lazyload">
</a>
<a href="/life/63461/einai-eykolo-kai-sik-h-nea-moda-to-banana-sushi-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Είναι εύκολο και σικ: H νέα μόδα, το banana sushi!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/bananasushi2.jpg" alt="Είναι εύκολο και σικ: H νέα μόδα, το banana sushi!" border="0" class="cropimg lazyload">
</a>
<a href="/life/63124/ta-tylixta-faghta-ksenwn-xwrwn-poy-aksizei-na-dokimasete-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Τα τυλιχτά φαγητά ξένων χωρών που αξίζει να δοκιμάσετε</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dumpling2.jpg" alt="Τα τυλιχτά φαγητά ξένων χωρών που αξίζει να δοκιμάσετε" border="0" class="cropimg lazyload">
</a>
<a href="/life/63206/anatroph-twra-ta-3-pio-parekshghmena-trofima" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Aνατροπή τώρα! Τα 3 πιο παρεξηγημένα τρόφιμα </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/pareksigimenatrofima.jpg" alt="Aνατροπή τώρα! Τα 3 πιο παρεξηγημένα τρόφιμα " border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_2" class="menuPosts mgmenu_tabs_hide">
<a href="/life/62252/giati-dipsame-meta-to-faghto" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιατί διψάμε μετά το φαγητό;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/thirstysalty.jpg" alt="Γιατί διψάμε μετά το φαγητό;" border="0" class="cropimg lazyload">
</a>
<a href="http://go.linkwi.se/z/11774-1/CD15753/?" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το «μαγικό» διαιτητικό χάπι που σας βοηθάει να αδυνατίσετε, χωρίς δίαιτα!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/11droz.png" alt="Το «μαγικό» διαιτητικό χάπι που σας βοηθάει να αδυνατίσετε, χωρίς δίαιτα!" border="0" class="cropimg lazyload">
</a>
<a href="/life/63367/krema-apotrixwshs-me-monimo-apotelesma" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Κρέμα αποτρίχωσης με μόνιμο αποτέλεσμα!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dellegs.jpg" alt="Κρέμα αποτρίχωσης με μόνιμο αποτέλεσμα!" border="0" class="cropimg lazyload">
</a>
<a href="/life/63723/6-trofes-poy-yposxontai-kalyterh-stytikh-leitoyrgia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">6 τροφές που υπόσχονται καλύτερη στυτική λειτουργία</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Man-looking-inside-his-shorts.jpg" alt="6 τροφές που υπόσχονται καλύτερη στυτική λειτουργία" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_3" class="menuPosts mgmenu_tabs_hide">
<a href="/life/63343/ti-loyloydia-na-fytepsw-gia-to-fthinopwro" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τι λουλούδια να φυτέψω για το φθινόπωρο;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/autumnlouloudia.jpg" alt="Τι λουλούδια να φυτέψω για το φθινόπωρο;" border="0" class="cropimg lazyload">
</a>
<a href="/life/62087/6-tropoi-na-kanete-liftingk-sthn-koyzina-sas-xwris-mpatzet" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">6 τρόποι να κάνετε λίφτινγκ στην κουζίνα σας χωρίς μπάτζετ</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/kouzin.jpg" alt="6 τρόποι να κάνετε λίφτινγκ στην κουζίνα σας χωρίς μπάτζετ" border="0" class="cropimg lazyload">
</a>
<a href="/life/61833/pws-na-eksafanisete-tis-myges-apo-to-spiti-sas" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Πώς να εξαφανίσετε τις μύγες από το σπίτι σας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/myges_spiti.jpg" alt="Πώς να εξαφανίσετε τις μύγες από το σπίτι σας" border="0" class="cropimg lazyload">
</a>
<a href="/life/61119/gia-poio-logo-yparxoyn-oi-kampyles-poy-proeksexoyn-sto-karotsaki-toy-supermarket-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Για ποιο λόγο υπάρχουν οι καμπύλες που προεξέχουν στο καροτσάκι του supermarket;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sueporoia1.jpg" alt="Για ποιο λόγο υπάρχουν οι καμπύλες που προεξέχουν στο καροτσάκι του supermarket;" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_4" class="menuPosts mgmenu_tabs_hide">
<a href="/life/56846/oi-pio-asynhthistes-aeroporikes-etaireies-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Οι πιο ασυνήθιστες αεροπορικές εταιρείες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/unusual-airlines-11.jpg" alt="Οι πιο ασυνήθιστες αεροπορικές εταιρείες" border="0" class="cropimg lazyload">
</a>
<a href="https://go.linkwi.se/z/73-0/CD15753/?lnkurl=https%3A%2F%2Fel.aegeanair.com%2Forganoste%2Fprosfores-diagonismoi%2Fcyclades%2F" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Μήλος, Πάρος, Νάξος και Σύρος από 19€!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mulosphotomain.jpg" alt="Μήλος, Πάρος, Νάξος και Σύρος από 19€!" border="0" class="cropimg lazyload">
</a>
<a href="/life/63616/h-ellada-prwth-stis-kalyteres-xwres-toy-planhth" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η Ελλάδα πρώτη στις καλύτερες χώρες του πλανήτη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ellada-main.jpg" alt="Η Ελλάδα πρώτη στις καλύτερες χώρες του πλανήτη" border="0" class="cropimg lazyload">
</a>
<a href="http://go.linkwi.se/z/177-119/CD15753/?" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Μεγάλος Διαγωνισμός: 3 πανέμορφα ξενοδοχεία σας περιμένουν!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/summer-holidays.jpg" alt="Μεγάλος Διαγωνισμός: 3 πανέμορφα ξενοδοχεία σας περιμένουν!" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_5" class="menuPosts mgmenu_tabs_hide">
<a href="/life/63003/ta-wraiotera-aksesoyar-kai-epipla-ftiaxnontai-apo-ksylo-kerdiste" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Τα ωραιότερα αξεσουάρ και έπιπλα φτιάχνονται από ξύλο </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/trigonis4.jpg" alt="Τα ωραιότερα αξεσουάρ και έπιπλα φτιάχνονται από ξύλο " border="0" class="cropimg lazyload">
</a>
<a href="/life/63690/to-pio-akrivo-zeygari-papoytsia-ston-kosmo-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Το πιο ακριβό ζευγάρι παπούτσια στον κόσμο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/shoesexp.jpg" alt="Το πιο ακριβό ζευγάρι παπούτσια στον κόσμο" border="0" class="cropimg lazyload">
</a>
<a href="/life/63419/oi-nees-taseis-toy-makigiaz-gia-th-nea-sezon-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Οι νέες τάσεις του μακιγιάζ για τη νέα σεζόν</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/smokey_eye_makeup3.jpg" alt="Οι νέες τάσεις του μακιγιάζ για τη νέα σεζόν" border="0" class="cropimg lazyload">
</a>
<a href="/life/61638/pws-na-vgaineis-teleies-fwtografies" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Πως να βγαίνεις τέλειες φωτογραφίες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sumboules.jpg" alt="Πως να βγαίνεις τέλειες φωτογραφίες" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_6" class="menuPosts mgmenu_tabs_hide">
<a href="/life/63633/giati-ta-koyfeta-stis-mpomponieres-exoyn-mono-arithmo" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιατί τα κουφέτα στις μπομπονιέρες έχουν μονό αριθμό</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/koufeta1.jpg" alt="Γιατί τα κουφέτα στις μπομπονιέρες έχουν μονό αριθμό" border="0" class="cropimg lazyload">
</a>
<a href="/life/61207/ekleisan-ta-sxoleia-gia-kalokairi-kai-twra-ti" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Έκλεισαν τα σχολεία για καλοκαίρι: Και τώρα τι;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/boredchild.jpg" alt="Έκλεισαν τα σχολεία για καλοκαίρι: Και τώρα τι;" border="0" class="cropimg lazyload">
</a>
<a href="/life/60160/ta-mikra-paidia-xeirizontai-ta-smartphones-kalytera-apo-to-piroyni" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα μικρά παιδιά χειρίζονται τα Smartphones καλύτερα από το πιρούνι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paidiatexnologia.jpg" alt="Τα μικρά παιδιά χειρίζονται τα Smartphones καλύτερα από το πιρούνι" border="0" class="cropimg lazyload">
</a>
<a href="/life/60378/xwris-farmako-yparxoyn-kai-alloi-tropoi-na-skotwsete-tis-pseires" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Χωρίς φάρμακο: Υπάρχουν και άλλοι τρόποι να σκοτώσετε τις ψείρες!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/psires24.jpg" alt="Χωρίς φάρμακο: Υπάρχουν και άλλοι τρόποι να σκοτώσετε τις ψείρες!" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_7" class="menuPosts mgmenu_tabs_hide">
<a href="/life/62967/pente-lathh-poy-kanoyn-oi-antres-sto-krevati" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Πέντε λάθη που κάνουν οι άντρες στο κρεβάτι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zeugari_krevati3.jpg" alt="Πέντε λάθη που κάνουν οι άντρες στο κρεβάτι" border="0" class="cropimg lazyload">
</a>
<a href="/life/63130/giati-ta-diazygia-einai-perissotera-ton-aygoysto-kai-ton-martio" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιατί τα διαζύγια είναι περισσότερα τον Αύγουστο και τον Μάρτιο;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/gamos.jpg" alt="Γιατί τα διαζύγια είναι περισσότερα τον Αύγουστο και τον Μάρτιο;" border="0" class="cropimg lazyload">
</a>
<a href="/life/63073/5-typoi-andrwn-poy-prepei-na-apofygeis" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">5 τύποι ανδρών που πρέπει να αποφύγεις</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/antr.jpg" alt="5 τύποι ανδρών που πρέπει να αποφύγεις" border="0" class="cropimg lazyload">
</a>
<a href="/life/62611/ti-einai-epiteloys-aytos-o-andras-palaias-kophs" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τι είναι επιτέλους αυτός ο «Άνδρας Παλαιάς Κοπής»;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/andras_palaias_kophs_1.jpg" alt="Τι είναι επιτέλους αυτός ο «Άνδρας Παλαιάς Κοπής»;" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_8" class="menuPosts mgmenu_tabs_hide">
<a href="/life/63597/ta-zwdia-shmera-5-septemvrioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 5 Σεπτεμβρίου </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/psarakia2.jpg" alt="Τα ζώδια σήμερα: 5 Σεπτεμβρίου " border="0" class="cropimg lazyload">
</a>
<a href="/life/63595/ta-zwdia-shmera-4-septemvrioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 4 Σεπτεμβρίου </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zodiaeikonitsa33.jpg" alt="Τα ζώδια σήμερα: 4 Σεπτεμβρίου " border="0" class="cropimg lazyload">
</a>
<a href="/life/63593/ta-zwdia-shmera-3-septemvrioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 3 Σεπτεμβρίου </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zodiaeikonidia2.jpg" alt="Τα ζώδια σήμερα: 3 Σεπτεμβρίου " border="0" class="cropimg lazyload">
</a>
<a href="/life/63591/ta-zwdia-shmera-2-septemvrioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 2 Σεπτεμβρίου </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zodiaeikonidia.jpg" alt="Τα ζώδια σήμερα: 2 Σεπτεμβρίου " border="0" class="cropimg lazyload">
</a>
</div>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/entertainment">Ψυχαγωγία</a></span>
<div class="dropdown_fullwidth mgmenu_tabs">
<div class="mgmenu_width">
<ul class="mgmenu_tabs_nav">
<li><a href="/agenda" rel="section5_1" class="current">ATZENTA ΣΗΜΕΡΑ</a></li>
<li><a href="/music" rel="section5_2">ΜΟΥΣΙΚΗ</a></li>
<li><a href="/cinema" rel="section5_3">ΣΙΝΕΜΑ</a></li>
<li><a href="/theater" rel="section5_4">ΘΕΑΤΡΟ+ΧΟΡΟΣ</a></li>
<li><a href="/arts" rel="section5_5">ΕΙΚΑΣΤΙΚΑ</a></li>
<li><a href="/city" rel="section5_6">ΠΟΛΗ</a></li>
<li><a href="/nightlife" rel="section5_7">ΝΑΪΤ ΛΑΪΦ</a></li>
</ul>
<div class="mgmenu_tabs_panels">
<div id="section5_1" class="menuPosts">
<a href="/entertainment/63830/9o-diethnes-festival-theatrikh-sezon-agias-petroypolhs" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">9ο Διεθνές Φεστιβάλ «Θεατρική σεζόν Αγίας Πετρούπολης»</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/agiapetroupoliethniko.jpg" alt="9ο Διεθνές Φεστιβάλ «Θεατρική σεζόν Αγίας Πετρούπολης»" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63829/h-oresteia-toy-aisxyloy-se-skhn-giannh-xoyvarda-sto-hrwdeio" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η Ορέστεια του Αισχύλου σε σκην. Γιάννη Χουβαρδά στο Ηρώδειο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/oresteiahrwdeio.jpg" alt="Η Ορέστεια του Αισχύλου σε σκην. Γιάννη Χουβαρδά στο Ηρώδειο" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63827/out-topias-performans-kai-ypaithriosdhmosios-xwros-sto-moyseio-mpenakh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">[OUT] TOPIAS - Περφόρμανς και Υπαίθριος/Δημόσιος Χώρος στο Μουσείο Μπενάκη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/outopias.jpg" alt="[OUT] TOPIAS - Περφόρμανς και Υπαίθριος/Δημόσιος Χώρος στο Μουσείο Μπενάκη" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63826/h-nostalgos-toy-papadiamanth-ksana-sto-theatro-alkmhnh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η Νοσταλγός του Παπαδιαμάντη ξανά στο Θέατρο Αλκμήνη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/inostalgos16.jpg" alt="Η Νοσταλγός του Παπαδιαμάντη ξανά στο Θέατρο Αλκμήνη" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_2" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/63821/oi-encardia-live-sto-festival-vyrwna" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Οι Encardia Live στο Φεστιβάλ Βυρώνα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/encardia16.jpg" alt="Οι Encardia Live στο Φεστιβάλ Βυρώνα" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63704/tsakelectric-live" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Tsakelectric Live</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tsakelecrticlive.jpg" alt="Tsakelectric Live" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63701/evolution-4-years-anniversary" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Evolution / 4 Years Anniversary</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/evolution4.jpg" alt="Evolution / 4 Years Anniversary" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63697/were-loud-fest-2016-thessaloniki-to-athens" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">We`re Loud Fest 2016: Thessaloniki to Athens!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/weareloudfest16.jpg" alt="We`re Loud Fest 2016: Thessaloniki to Athens!" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_3" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/62542/hellas-filmbox-roadshow" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Hellas Filmbox Roadshow</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/berlinroadshow.jpg" alt="Hellas Filmbox Roadshow" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/62289/oi-tainies-ths-evdomadas-aygoystiatikes-premieres-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Οι ταινίες της εβδομάδας: Αυγουστιάτικες πρεμιέρες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/eyeinthesky11.jpg" alt="Οι ταινίες της εβδομάδας: Αυγουστιάτικες πρεμιέρες" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/62075/1o-festival-kinhmatografoy-peiraia-gia-tainies-mikroy-mhkoys" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">1ο Φεστιβάλ Κινηματογράφου Πειραιά για Ταινίες Μικρού Μήκους</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/festkinhmatogrpeiraia.jpg" alt="1ο Φεστιβάλ Κινηματογράφου Πειραιά για Ταινίες Μικρού Μήκους" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/62009/3o-taratsa-international-film-festival" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">3o Taratsa International Film Festival</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/taratsa2016.jpg" alt="3o Taratsa International Film Festival" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_4" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/63823/tunnel-of-oppression-toy-dhmhtrh-plavoykoy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Tunnel of Oppression, του Δημήτρη Πλαβούκου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tunnelofopression.jpg" alt="Tunnel of Oppression, του Δημήτρη Πλαβούκου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63703/young-lear-ths-iolhs-andreadh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Young Lear, της Ιόλης Ανδρεάδη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/younglear1.jpg" alt="Young Lear, της Ιόλης Ανδρεάδη" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63541/to-nhpagwgeio-ksana-sto-mpagklantes-gia-periorismeno-arithmo-parastasewn" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΧΟΡΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το Νηπαγωγείο ξανά στο Μπαγκλαντές για περιορισμένο αριθμό παραστάσεων</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/nipiagogeiompagklantes.jpg" alt="Το Νηπαγωγείο ξανά στο Μπαγκλαντές για περιορισμένο αριθμό παραστάσεων" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63538/orlanto-ths-virtzinia-goylf" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ορλάντο, της Βιρτζίνια Γουλφ</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/orlantoskrow.jpg" alt="Ορλάντο, της Βιρτζίνια Γουλφ" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_5" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/63537/oi-aythadeies-toy-hlioy-eikastikh-ekthesh-me-erga-toy-leonardo-cremonini" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Οι Αυθάδειες του Ήλιου, εικαστική έκθεση με έργα του Leonardo Cremonini</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/oiauthadeiestouiliou.jpg" alt="Οι Αυθάδειες του Ήλιου, εικαστική έκθεση με έργα του Leonardo Cremonini" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63443/nothing-remains-the-same-ekthesh-ths-chris-roboras-sthn-gkaleri-7" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Nothing remains the same: έκθεση της Chris Roboras στην γκαλερί 7</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/nothingremainsthesame.jpg" alt="Nothing remains the same: έκθεση της Chris Roboras στην γκαλερί 7" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63391/oti-fwteino-exei-na-pei-tha-prepei-na-meinei-eikasia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">«Ό,τι φωτεινό έχει να πει, θα πρέπει να μείνει εικασία»</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/otifoteino.jpg" alt="«Ό,τι φωτεινό έχει να πει, θα πρέπει να μείνει εικασία»" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/62923/odyssia-festival-2016-parallhles-draseis" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Odyssia Festival 2016: Παράλληλες δράσεις</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/odyssiaart.jpg" alt="Odyssia Festival 2016: Παράλληλες δράσεις" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_6" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/63618/e-agenda-evdomadas-festival-pik-nik-kai-moysikes" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">E-Agenda Εβδομάδας: Φεστιβάλ, πικ νικ και μουσικές</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/pinikim.jpg" alt="E-Agenda Εβδομάδας: Φεστιβάλ, πικ νικ και μουσικές" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63536/istories-mponzai-gia-to-shmeio-mhden-epeteiako-logotexniko-afierwma" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ιστορίες Μπονζάι για το Σημείο Μηδέν: Επετειακό λογοτεχνικό αφιέρωμα </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/istoriesmponzai.jpg" alt="Ιστορίες Μπονζάι για το Σημείο Μηδέν: Επετειακό λογοτεχνικό αφιέρωμα " border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63390/syros-greatings-festival-gastronomias" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Syros GrEATings: φεστιβάλ γαστρονομίας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/syrosgreatings.jpg" alt="Syros GrEATings: φεστιβάλ γαστρονομίας" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/63377/fantasticon-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">ΦantastiCon 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Fantasticon2016.jpg" alt="ΦantastiCon 2016" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_7" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/62370/monkey-bar-kai-partyneverends-paroysiazoyn-ton-culoe-de-song" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΝΑΙΤ ΛΑΙΦ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Monkey Bar και Partyneverends παρουσιάζουν τον Culoe De Song</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/culoe_poster.jpg" alt="Monkey Bar και Partyneverends παρουσιάζουν τον Culoe De Song" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/62148/party-katw-apo-thn-aygoystiatikh-panselhno-sto-myrtari" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΝΑΙΤ ΛΑΙΦ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Πάρτυ κάτω από την αυγουστιάτικη πανσέληνο στο Μυρτάρι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/myrtari16.jpg" alt="Πάρτυ κάτω από την αυγουστιάτικη πανσέληνο στο Μυρτάρι" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/62010/plissken-festival-winter-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΞΕΝΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Plissken Festival // Winter 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/plisskenwinter2016.jpg" alt="Plissken Festival // Winter 2016" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/61736/party-me-ton-pepper-966" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΝΑΙΤ ΛΑΙΦ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Πάρτυ με τον Pepper 96.6</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/AFISmatola.jpg" alt="Πάρτυ με τον Pepper 96.6" border="0" class="cropimg lazyload">
</a>
</div>
</div>
</div>
</div>
</li> 
</ul>
<a href="/agenda" class="mgmenu_left nextwebchannels"><img src="/_img/eagendagr_logo.png" height="20"/></a>
<a href="http://www.e-radio.gr" target="_blank" class="mgmenu_left nextwebchannels"><img src="/_img/eradiogr_logo.png" height="20"/></a>
<div class="right_item">
<div class="right_item" id="tabSearch" style="padding:12px 10px 14px 8px;"><div class="icon-search"></div></div>
<div class="right_item noactive" style="padding:0">
<a class="icon-twitter-bird right_item twitter" href="http://www.twitter.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px;"></a>
<a class="icon-instagram right_item instagram" href="http://www.instagram.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px;"></a>
<a class="icon-facebook right_item facebook" href="http://www.facebook.com/edailygr" target="_blank" style="padding:12px 0 14px 10px;"></a>
</div>
<div class="right_item noactive feedIcons showOnSmallMenu">
<a href="/retro"><img alt="Retro" src="/_img/feeds/110.png" border="0"/></a>
<a href="/omg"><img alt="OMG" src="/_img/feeds/101.png" border="0"/></a>
<a href="/lol"><img alt="LOL" src="/_img/feeds/106.png" border="0"/></a>
<a href="/juicy"><img alt="Juicy" src="/_img/feeds/104.png" border="0"/></a>
<a href="/alist"><img alt="A-List" src="/_img/feeds/91.png" border="0"/></a>
<a href="/cute"><img alt="Cute" src="/_img/feeds/109.png" border="0"/></a>
<a href="/geeky"><img alt="Geeky" src="/_img/feeds/108.png" border="0"/></a>
</div>
</div>
</div>
<div class="mgmenu_fullwidth"></div>
</div>
<div id="topHeaderMobile" style="text-align:center;">
<div class="ma-infinitypush-button"><span></span><span></span><span></span><span></span></div>
<a href="/"><img src="/_img/mobilelogo.png" width="136" height="48" alt="E-Daily" border="0"/></a>
<div class="right_item" id="tabSearchMobile" style="padding:12px; font-size: 18px;"><div class="icon-search"></div></div>
</div>
<div id="tabSearchMenu">
<form name="bigsearchboxform" id="bigsearchboxform" class="mainArea" autocomplete="off" method="post" action="/search/" onsubmit="javascript:submitsearch()">
<input type="search" name="q" id="q" placeholder="Αναζητήστε 62853 θέματα, 1008 ραδιόφωνα" value="" alt="Αναζητήστε 62853 θέματα, 1008 ραδιόφωνα"/>
<a name="bigsearchbox_Cancel" id="bigsearchbox_Cancel" href="javascript:hidesearch()" class="icon-cancel-circle"></a>
</form>
</div>
<div class="navibar">
</div>
<div id="topHeader3">
<center>
<script type="text/javascript">
( function() {
    var info = getTagInfo();
    var alias = 'edhomebillboard_display';
        alias = alias !== '' ? 'alias=' + ( alias.split("_") ? alias.split("_")[ 0 ] : alias ) + '_' + info.alias : '';
    document.write('<scr'+'ipt language="javascript1.1" src="http://' + info.domain + '.adtech.de/addyn/3.0/1370.1/4713596/0/-1/ADTECH;' + alias + ';loc=100;target=_blank;key=;grp=' + info.adgroupid + ';misc=' + new Date().getTime() + '"></scri'+'pt>');
})();</script>
</center>
</div>
<div class="mainArea">
<div class="mainAreaA">
<h1 class="postTitle">Privacy Policy</h1>
<div class="postBody">
<p>Welcome to E-Daily. This Privacy Policy is effective for E-Daily.gr, E-Daily Mobile Apps (&ldquo;E-Daily&rdquo;, &ldquo;we&rdquo;, &ldquo;us&rdquo; and/or &ldquo;our&rdquo;) and is incorporated by reference into the E-Daily Terms of Service and End User License Agreement. <br/>
The purpose of this Privacy Policy is to inform users of the E-Daily Service (as defined below) about the information that we collect through the E-Daily Service, how we use that information and the ways users can control how that information is used or shared. The entire policy can be found below. For information about users&rsquo; obligations and use of the E-Daily Service, please see our Terms of Service and End User License Agreement. </p>
<p> We will continue to evaluate this Privacy Policy and update it accordingly when there is a change to our business practices, our users&rsquo; needs, and to comply with regulatory and legal requirements. Please check this page periodically for updates. If we make any material changes to this Privacy Policy, we will post the updated Privacy Policy here and notify you by email or by means of a notice on the Site (as defined below) and/or the Applications (as defined below). </p>
<p><strong>The types of information we collect</strong><br/>
When you interact with the E-Daily.gr, we may collect Personal Information (as defined below) and other information from you as described below. <br/>
&ldquo;Personal Information&rdquo; means information that alone or when in connection with other information may be used to readily identify, contact, or locate you, such as: name, address, email address or phone number. We do not consider personally identifiable information to include information that has been anonymized so that it does not allow a third party to easily identify a specific individual. </p>
<p><strong>Information you give us </strong><br/>
To enable you to enjoy certain features of the E-Daily Service, we collect certain types of information, including Personal Information, during your interactions with the E-Daily Service. By using the E-Daily Service, you are authorizing us to gather, parse, and retain data related to the provision of the E-Daily Service. </p>
<p> For example, we collect information, including Personal Information, when you: </p>
<ul type="disc">
<li>Register to use our E-Daily Service;</li>
<li>Login with social networking credentials; </li>
<li>Participate in polls, contests, surveys or other features of the E-Daily Service, or respond to offers or advertisements on the E-Daily Service; </li>
<li>Communicate with us; and</li>
<li>Sign up to receive email newsletters.</li>
</ul>
<p>When you register to use the E-Daily Service, we ask you to provide certain information, which includes your name, email address, birth year, gender, zip code and/or other geographic information, as well as a password for your account. By voluntarily providing us with Personal Information or other information, you are consenting to our use of it in accordance with this Privacy Policy. If you provide any Personal Information or other information, you acknowledge and agree that such Personal Information may be transferred from your current location to our offices and servers and the authorized third parties. </p>
<p><strong>Automatic Data Collection</strong><br/>
<em>Listening and Usage Activity</em>: We keep track of your listening and usage activity. We collect information about stations, favorites, artists, channels, conversations and tracks you have listened to or in which you have expressed an interest. <br/>
<em>Social connect information</em>: When you choose to connect your social media account to your account profile, we collect Personal Information from that social media website. For example, when you connect your Facebook account, we may collect the Personal Information you have made publicly available in Facebook, such as your name, profile picture, cover photo, username, gender, friend networks, age range, locale, friend list, and any other information you have made public. We may also obtain other non-public information from the social network website with your permission. </p>
<p><em>Information about your computer or device</em>: We may also collect information about the computer, mobile or other devices you use to access the E-Daily Service. For example, our servers receive and record information about your computer and browser, including potentially your IP address, device ID, operating system, browser type, and other software or hardware information. If you access the E-Daily Service from a mobile or other device, unless you register, we may assign a unique device identifier for that device. </p>
<p><em>Location-based information</em>: We do not collect your zip code, postal code, city or country unless you choose to provide this information to us at registration. Your general geo-location is collected at regular intervals if you use the E-Daily Service on your mobile device or through the Site. We collect this information to provide content and services that are customized for your geographic area. </p>
<p><em>Tracking technology</em>: We use what are commonly called &quot;cookies&quot; on our Site. A cookie is a piece of information that the computer that hosts our Site gives to your browser when you access the Site. These cookies help us improve the E-Daily Service, provide additional functionality to the Site and help us analyze Site usage more accurately. For instance, our Site may set a cookie on your browser that allows you to access the Site without needing to remember and then enter a password more than once during a visit to the Site. We may also be able to capture other data such as searches conducted and results, date, time, and connection speed. In all cases in which we use cookies, we will not collect Personal Information except with your permission. On most web browsers, you will find a &ldquo;help&rdquo; section on the toolbar. Please refer to this section for information on how to receive notification when you are receiving a new cookie and how to turn cookies off. We recommend that you leave cookies turned on because they allow you to take advantage of some of the Site&rsquo;s features. Third parties, including advertisers and third-party advertising companies, whose products or services are accessible or advertised via the E-Daily Service may also use cookies, and we advise you to check their privacy policies for information about their cookies and other privacy practices. </p>
<p><strong>Our use of the information we collect</strong><br/>
We use the information, including Personal Information, that we collect for the following purposes: </p>
<ul type="disc">
<li>To personalize and customize the advertising and the content you see.</li>
<li>To improve and enhance the user experience. We use tracking information to determine how well each page, station and/or channel performs overall based on aggregate user demographics and traffic patterns to those pages, stations and channels. This helps us build a better service for you.</li>
<li>To fulfill your requests for certain products and services, such as sending out electronic newsletters and enabling you to participate in polls, contests, and boards.</li>
<li>To send you information about topics or content that we think will be of interest to you based on your preferences, habits or geographic location.</li>
<li>To send you information about the latest developments and features on our E-Daily Service.</li>
<li>To provide technical support.</li>
</ul>
<p>We may anonymize and aggregate data collected through the E-Daily Service and use it for any purpose. </p>
<p><strong>How the information we collect is shared</strong><br/>
We are not in the business of selling, renting or sharing Personal Information about you with other people or nonaffiliated companies for their direct marketing purposes. We consider this information to be a vital part of our relationship with you. There are, however, certain circumstances in which we may share your information, including Personal Information, with certain third parties without further notice to you, as set forth below: </p>
<ul type="disc">
<li>Information you allow us to share when you register with us, or through a subsequent affirmative election.</li>
<li>Information used when we hire or partner with third parties to provide business-related services on our behalf, such as mailing information, maintaining databases, processing payments, sweepstakes management and prize fulfillment, data processing, analytics, personalization and customization of advertising, customer/support services and other products or services that we choose to make available to our registered users. When we employ another company to perform a function of this nature, we only provide them with the information that they need to perform their specific function.</li>
<li>We may share, transfer or sell your information in connection with a merger, acquisition, financing due diligence, reorganization, bankruptcy, receivership, sale of company assets, or transition of service to another provider. We cannot control how such entities may use or disclose such information.</li>
<li>We may share information with our subsidiaries and affiliates for purposes consistent with this Privacy Policy.</li>
<li>The E-Daily Service may allow you to share information, including Personal Information, with social networking websites, such as Facebook. We do not share your Personal Information with them unless you direct the E-Daily Service to share it. Their use of the information will be governed by their privacy policies, and you may be able to modify your privacy setting on their websites.</li>
<li>We may share your information if required to do so by law or in the good faith belief that such action is necessary in order to (i) comply with a legal obligation, (ii) protect or defend our legal rights or property as well as those of our business partners, employees, agents and contractors (including enforcement of our agreements); (iii) act in urgent circumstances to protect the personal safety of users or the public; (iv) protect against fraud or risk management purposes; or (v) protect against legal liability.</li>
</ul>
<p>When you register and create a profile through the E-Daily Service, other users of the E-Daily Service may be able to see your profile information, including your screen name, profile photo and any content that you post to the E-Daily Service. We recommend that you guard your anonymity and sensitive information. We are not responsible for the privacy practices of the other users who will view and use the information you post. </p>
<p><strong>Non-Identifiable or Aggregated Data</strong><br/>
We also receive and store certain non-personally identifiable information. Such information, which is collected passively using various technologies, cannot presently be used to specifically identify you. We may store such information itself or such information may be included in databases owned and maintained by our affiliates, agents or service providers. We may use such information and pool it with other information to track, for example, the total number of visitors to the E-Daily Service, the number of visitors to each page of our Site, the domain names of our visitors' Internet service providers, and how our users use and interact with the E-Daily Service. Also, in an ongoing effort to better understand and serve the users of the E-Daily Service, we often conduct research on our customer demographics, interests and behavior based on the personal information and other information provided to us. This research may be compiled and analyzed on an aggregate basis. We may share this non-identifiable and aggregate data with our affiliates, agents and business partners, but this type of non-identifiable and aggregate information does not identify you personally. We may also disclose aggregated user statistics in order to describe our services to current and prospective business partners and to other third parties for other lawful purposes. <br/>
The use and disclosure of this non-personally identifiable information is not subject to restrictions under this Privacy Policy. </p>
<p><strong>Your Choices</strong><br/>
You can visit the Site without providing any personal information. If you choose not to provide any personal information, you may not be able to use certain features of our E-Daily Service. </p>
<p><strong>Exclusions</strong><br/>
This Privacy Policy does not apply to any personal data collected by E-Daily other than personal data collected through the E-Daily Service. This Privacy Policy shall not apply to any unsolicited information you provide to us through the E-Daily Service or through any other means. This includes, but is not limited to, information posted to any public areas of the E-Daily Service, such as bulletin boards (collectively, &ldquo;Public Areas&rdquo;), any ideas for new products or modifications to existing products, and other unsolicited submissions (collectively, &ldquo;Unsolicited Information&rdquo;). All Unsolicited Information shall be deemed to be non-confidential and we shall be free to reproduce, use, disclose, distribute and exploit such Unsolicited Information without limitation or attribution. </p>
<p><strong>How we protect your information</strong><br/>
We have taken reasonable steps to protect the information you provide via the E-Daily Service from loss, misuse, and unauthorized access, disclosure, alteration, or destruction. However, no Internet, email or other electronic transmission is ever fully secure or error free, so you should take special care in deciding what information you send to us in this way. We do not accept liability for unintentional disclosure. <br/>
By using the E-Daily Service or providing Personal Information to us, you agree that we may communicate with you electronically regarding security, privacy, and administrative issues relating to your use of the E-Daily Service. If we learn of a security system&rsquo;s breach, we may attempt to notify you electronically by posting a notice on the Site and/or Applications or sending an email to you. </p>
<p><strong>Consent to processing</strong><br/>
If you use the E-Daily Service outside of the European Union, you fully understand and unambiguously consent to the transfer of your information, including Personal Information, to, and the collection and processing of such information in Greece and other countries or territories of the European Union. The laws on holding such information in European Union may vary and be less stringent than laws of your state or country. </p>
<p><strong>Children</strong><br/>
We do not knowingly collect Personal Information from children under the age of 13. If you are under the age of 13, please do not submit any Personal Information through the E-Daily Service. We encourage parents and legal guardians to monitor their children&rsquo;s Internet usage and to help enforce our Privacy Policy by instructing their children never to provide Personal Information through the E-Daily Service without their permission. If you have reason to believe that a child under the age of 13 has provided Personal Information to us through our E-Daily, please contact us, and we will endeavor to delete that information from our databases. </p>
<p><strong>Links to Other Web Sites</strong><br/>
This Privacy Policy applies only to the E-Daily Service. The E-Daily Service may contain links to other web sites not operated or controlled by us (the &ldquo;Third Party Sites&rdquo;). The policies and procedures we described here do not apply to the Third Party Sites. The links from the E-Daily Service do not imply that we endorse or have reviewed the Third Party Sites. We suggest contacting those sites directly for information on their privacy policies. </p>
<p><strong>What are my privacy options and how can I change my settings?</strong><br/>
<em>To modify your registration information:</em> Once you have registered, you can change your registration information (including your email address and password). <br/>
<em>Opt out:</em> Each email communication we send you will contain instructions permitting you to &quot;opt-out&quot; of receiving future marketing communications. In addition, if at any time you wish not to receive any future communications or you wish to have your name deleted from our email mailing lists, please contact us as indicated below. Note however, that certain communications, such as push notifications, require you to change the settings on your device if at any time you wish not to receive any future communications. Note that on occasion we send service-related announcements, such as disruption notifications, and you may not be able to opt-out of these communications. </p>
<p><strong>English language version shall control</strong><br/>
The English language version of this Privacy Policy is the version that governs your use of the E-Daily Service and in the event of any conflict between the English language version and a translated version, the English language version will control.</p>
<p> To print out this Statement, you may visit www.e-daily.gr/privacy.asp</p>
</div>
</div>
<div class="mainArea2">
<center>
<script language="javascript">
<!--
if (window.adgroupid == undefined) {
                window.adgroupid = Math.round(Math.random() * 1000);
}
document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1370/5799491/0/170/ADTECH;loc=100;target=_blank;key=;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
//-->
</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1370/5799491/0/170/ADTECH;loc=300;key=" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1370/5799491/0/170/ADTECH;loc=300;key=" border="0" width="300" height="250"></a></noscript>
</center>
<br>
<script language="javascript">
$().ready(function() {
    $("#todayTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  updateHash: false
	});
/*	$("#tabs-hl").mCustomScrollbar({
		theme:"dark-thick"
	});
*/
	$("#tabs-recent").mCustomScrollbar({
		theme:"dark-thick"
	});
});

</script>
<div class="postsRedHeader">διαβάστε σήμερα</div>
<div id="todayTabs" class="tab-container">
<ul class="etabs">
<li class="tab"><a href="#tabs-hl">ΕΠΙΛΟΓΕΣ</a></li>
<li class="tab"><a href="#tabs-recent">ΠΡΟΣΦΑΤΑ</a></li>
</ul>
<div id="tabs-hl" class="postEntries">
<a href="http://www.e-daily.gr/themata/63825/ta-35-must-toy-foithth-sthn-athhna-apo-to-e-dailygr" target="_blank">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/athina-83883.jpg" alt="Τα 35 must του φοιτητή στην Αθήνα από το E-Daily.gr" border="0" class="lazyload">
</div>
<div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span><span class="when">00:18</span></div>
<h3>Τα 35 must του φοιτητή στην Αθήνα από το E-Daily.gr</h3>
<h4>Πέρασες στην πρωτεύουσα; Good for you</h4>
</a>
<a href="/themata/63700/ta-paidia-sth-syria-den-stamatoyn-na-einai-paidia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/pisisyria.jpg" alt="Τα παιδιά στη Συρία δεν σταματούν να είναι παιδιά" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">00:18</span></div>
<h3>Τα παιδιά στη Συρία δεν σταματούν να είναι παιδιά</h3>
<h4>Μετατρέπουν τη φρίκη του πολέμου σε παιχνίδι</h4>
</a>
<a href="http://www.e-daily.gr/themata/63634/top-5-xeirotera-logotypa-se-ena-vinteo-toy-e-dailygr-video" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/failedlogosmain.png" alt="Top 5 χειρότερα λογότυπα σε ένα βίντεο του E-Daily.gr" border="0" class="lazyload">
</div>
<div><span class="category">DESIGN</span><span class="when">16:02</span></div>
<h3>Top 5 χειρότερα λογότυπα σε ένα βίντεο του E-Daily.gr</h3>
<h4>Πιο ελληνικό λογότυπο είναι στην πεντάδα;</h4>
</a>
<a href="/quiz/63789/vreite-ton-titlo-toy-tragoydioy-apo-enan-mono-stixo-quiz" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-doc-text"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/rouvas8.jpg" alt="Βρείτε τον τίτλο του τραγουδιού από έναν μόνο στίχο" border="0" class="lazyload">
</div>
<div><span class=alert>QUIZ</span><span class="category">ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span><span class="when">00:18</span></div>
<h3>Βρείτε τον τίτλο του τραγουδιού από έναν μόνο στίχο</h3>
<h4>Greek Edition</h4>
</a>
<a href="/quiz/63625/mporeite-na-vreite-poio-faghto-exei-tis-perissoteres-thermides-quiz" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-doc-text"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/foodvsfood.jpg" alt="Μπορείτε να βρείτε ποιο φαγητό έχει τις περισσότερες θερμίδες; " border="0" class="lazyload">
</div>
<div><span class=alert>QUIZ</span><span class="category">FOOD</span><span class="when">00:18</span></div>
<h3>Μπορείτε να βρείτε ποιο φαγητό έχει τις περισσότερες θερμίδες; </h3>
<h4>Θα εκπλαγείτε!</h4>
</a>
<a href="/viral/63207/diashmoi-poy-dhlwsan-bisexual" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/loi.jpg" alt="Διάσημοι που δήλωσαν bisexual" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">00:18</span></div>
<h3>Διάσημοι που δήλωσαν bisexual</h3>
<h4>Ο έρως φύλο δεν κοιτά</h4>
</a>
<a href="/news/63843/h-prwth-dhmoskophsh-meta-tis-thleoptikes-adeies" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/ekloges-kalpi.jpg" alt="Η πρώτη δημοσκόπηση μετά τις τηλεοπτικές άδειες" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">00:02</span></div>
<h3>Η πρώτη δημοσκόπηση μετά τις τηλεοπτικές άδειες</h3>
<h4>Δημοσκόπηση του ΠΑ.ΜΑΚ. </h4>
</a>
<a href="/life/63806/sta-5-kalytera-ksenodoxeia-toy-2016-vrisketai-kai-ena-ellhniko-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/umaid.jpg" alt="Στα 5 καλύτερα ξενοδοχεία του 2016 βρίσκεται και ένα ελληνικό!" border="0" class="lazyload">
</div>
<div><span class="category">ΤΑΞΙΔΙΑ</span><span class="when">00:18</span></div>
<h3>Στα 5 καλύτερα ξενοδοχεία του 2016 βρίσκεται και ένα ελληνικό!</h3>
<h4>Δείτε τα</h4>
</a>
</div>
<div id="tabs-recent" class="postEntries noimg" style="height:600px; overflow:hidden">
<a href="/news/63842/fwtografies-sok-apo-ton-sovaro-traymatismoy-fwtografoy-sthn-klhrwsh-ths-football-league" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/Koukouloforoi_league_0.jpg" alt="Φωτογραφίες-σοκ από τον σοβαρό τραυματισμού φωτογράφου στην κλήρωση της Football League" border="0" class="lazyload">
</div>
<div><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span><span class="when">00:18</span></div>
<h3>Φωτογραφίες-σοκ από τον σοβαρό τραυματισμού φωτογράφου στην κλήρωση της Football League</h3>
<h4>Εισέβαλαν κουκουλοφόροι</h4>
</a>
<a href="https://go.linkwi.se/z/10632-0/CD15753/?lnkurl=http%3A%2F%2Fwww.brandsgalaxy.gr%2Fcampaigns%2Fshoe-heaven%2F" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-ticket"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/shoeheaven.png" alt="Shoe Haven με εκπτώσεις έως και 70%" border="0" class="lazyload">
</div>
<div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">STYLE</span><span class="when">00:18</span></div>
<h3>Shoe Haven με εκπτώσεις έως και 70%</h3>
<h4>JLO, Paris Hilton, Boss, Schutz και άλλα!</h4>
</a>
<a href="http://www.e-daily.gr/themata/63825/ta-35-must-toy-foithth-sthn-athhna-apo-to-e-dailygr" target="_blank">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/athina-83883.jpg" alt="Τα 35 must του φοιτητή στην Αθήνα από το E-Daily.gr" border="0" class="lazyload">
</div>
<div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span><span class="when">00:18</span></div>
<h3>Τα 35 must του φοιτητή στην Αθήνα από το E-Daily.gr</h3>
<h4>Πέρασες στην πρωτεύουσα; Good for you</h4>
</a>
<a href="/themata/63811/san-shmera-ta-shmantikotera-gegonota-ths-6hs-septemvrioy-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/6th_september.jpg" alt="Σαν σήμερα: Τα σημαντικότερα γεγονότα της 6ης Σεπτεμβρίου" border="0" class="lazyload">
</div>
<div><span class="category">ΙΣΤΟΡΙΚΑ</span><span class="when">00:18</span></div>
<h3>Σαν σήμερα: Τα σημαντικότερα γεγονότα της 6ης Σεπτεμβρίου</h3>
<h4>Τι συνέβη σαν σήμερα στην ιστορία</h4>
</a>
<a href="/life/63806/sta-5-kalytera-ksenodoxeia-toy-2016-vrisketai-kai-ena-ellhniko-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/umaid.jpg" alt="Στα 5 καλύτερα ξενοδοχεία του 2016 βρίσκεται και ένα ελληνικό!" border="0" class="lazyload">
</div>
<div><span class="category">ΤΑΞΙΔΙΑ</span><span class="when">00:18</span></div>
<h3>Στα 5 καλύτερα ξενοδοχεία του 2016 βρίσκεται και ένα ελληνικό!</h3>
<h4>Δείτε τα</h4>
</a>
<a href="/life/63804/ti-na-dialeksw-patata-h-ryzi" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/patatbraste.jpg" alt="Tι να διαλέξω: Πατάτα ή ρύζι;" border="0" class="lazyload">
</div>
<div><span class="category">FOOD</span><span class="when">00:18</span></div>
<h3>Tι να διαλέξω: Πατάτα ή ρύζι;</h3>
<h4>Πριν μαγειρέψεις διάβασε το</h4>
</a>
<a href="/quiz/63789/vreite-ton-titlo-toy-tragoydioy-apo-enan-mono-stixo-quiz" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-doc-text"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/rouvas8.jpg" alt="Βρείτε τον τίτλο του τραγουδιού από έναν μόνο στίχο" border="0" class="lazyload">
</div>
<div><span class=alert>QUIZ</span><span class="category">ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span><span class="when">00:18</span></div>
<h3>Βρείτε τον τίτλο του τραγουδιού από έναν μόνο στίχο</h3>
<h4>Greek Edition</h4>
</a>
<a href="/life/63784/ta-zwdia-shmera-6-septemvrioy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/kisskiss2.jpg" alt="Τα ζώδια σήμερα: 6 Σεπτεμβρίου" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΔΙΑ</span><span class="when">00:18</span></div>
<h3>Τα ζώδια σήμερα: 6 Σεπτεμβρίου</h3>
<h4>Αστρολογικές προβλέψεις κάθε μέρα από το E-Daily.gr</h4>
</a>
<a href="/themata/63700/ta-paidia-sth-syria-den-stamatoyn-na-einai-paidia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/pisisyria.jpg" alt="Τα παιδιά στη Συρία δεν σταματούν να είναι παιδιά" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">00:18</span></div>
<h3>Τα παιδιά στη Συρία δεν σταματούν να είναι παιδιά</h3>
<h4>Μετατρέπουν τη φρίκη του πολέμου σε παιχνίδι</h4>
</a>
<a href="/quiz/63625/mporeite-na-vreite-poio-faghto-exei-tis-perissoteres-thermides-quiz" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-doc-text"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/foodvsfood.jpg" alt="Μπορείτε να βρείτε ποιο φαγητό έχει τις περισσότερες θερμίδες; " border="0" class="lazyload">
</div>
<div><span class=alert>QUIZ</span><span class="category">FOOD</span><span class="when">00:18</span></div>
<h3>Μπορείτε να βρείτε ποιο φαγητό έχει τις περισσότερες θερμίδες; </h3>
<h4>Θα εκπλαγείτε!</h4>
</a>
<a href="/themata/63516/h-megalyterh-apeilh-gia-thn-anthrwpothta" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/klimalagi.jpg" alt="Η μεγαλύτερη απειλή για την ανθρωπότητα" border="0" class="lazyload">
</div>
<div><span class="category">ΠΕΡΙΒΑΛΛΟΝ</span><span class="when">00:18</span></div>
<h3>Η μεγαλύτερη απειλή για την ανθρωπότητα</h3>
<h4>Η κλιματική αλλαγή απειλεί ανθρώπινες ζωές</h4>
</a>
<a href="/viral/63207/diashmoi-poy-dhlwsan-bisexual" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/loi.jpg" alt="Διάσημοι που δήλωσαν bisexual" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">00:18</span></div>
<h3>Διάσημοι που δήλωσαν bisexual</h3>
<h4>Ο έρως φύλο δεν κοιτά</h4>
</a>
<a href="/entertainment/63830/9o-diethnes-festival-theatrikh-sezon-agias-petroypolhs" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/agiapetroupoliethniko.jpg" alt="9ο Διεθνές Φεστιβάλ «Θεατρική σεζόν Αγίας Πετρούπολης»" border="0" class="lazyload">
</div>
<div><span class="category">ΦΕΣΤΙΒΑΛ</span><span class="when">00:18</span></div>
<h3>9ο Διεθνές Φεστιβάλ «Θεατρική σεζόν Αγίας Πετρούπολης»</h3>
<h4>Εθνικό Θέατρο - 19/9/2016 - 22/9/2016</h4>
</a>
<a href="/entertainment/63827/out-topias-performans-kai-ypaithriosdhmosios-xwros-sto-moyseio-mpenakh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/outopias.jpg" alt="[OUT] TOPIAS - Περφόρμανς και Υπαίθριος/Δημόσιος Χώρος στο Μουσείο Μπενάκη" border="0" class="lazyload">
</div>
<div><span class="category">ΕΙΚΑΣΤΙΚΑ</span><span class="when">00:18</span></div>
<h3>[OUT] TOPIAS - Περφόρμανς και Υπαίθριος/Δημόσιος Χώρος στο Μουσείο Μπενάκη</h3>
<h4>Μουσείο Μπενάκη - 19/9/2016 - 20/11/2016</h4>
</a>
<a href="/entertainment/63826/h-nostalgos-toy-papadiamanth-ksana-sto-theatro-alkmhnh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/inostalgos16.jpg" alt="Η Νοσταλγός του Παπαδιαμάντη ξανά στο Θέατρο Αλκμήνη" border="0" class="lazyload">
</div>
<div><span class="category">ΘΕΑΤΡΟ</span><span class="when">00:18</span></div>
<h3>Η Νοσταλγός του Παπαδιαμάντη ξανά στο Θέατρο Αλκμήνη</h3>
<h4>Θέατρο Αλκμήνη - 26/9/2016 - 22/11/2016</h4>
</a>
<a href="/entertainment/63823/tunnel-of-oppression-toy-dhmhtrh-plavoykoy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tunnelofopression.jpg" alt="Tunnel of Oppression, του Δημήτρη Πλαβούκου" border="0" class="lazyload">
</div>
<div><span class="category">ΦΕΣΤΙΒΑΛ</span><span class="when">00:18</span></div>
<h3>Tunnel of Oppression, του Δημήτρη Πλαβούκου</h3>
<h4>Vault Theatre Plus - 29/9/2016 - 30/9/2016</h4>
</a>
<a href="/entertainment/63821/oi-encardia-live-sto-festival-vyrwna" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/encardia16.jpg" alt="Οι Encardia Live στο Φεστιβάλ Βυρώνα" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span><span class="when">00:18</span></div>
<h3>Οι Encardia Live στο Φεστιβάλ Βυρώνα</h3>
<h4>Θέατρο Βράχων Μελίνα Μερκούρη - 14/9/2016</h4>
</a>
<a href="/entertainment/63829/h-oresteia-toy-aisxyloy-se-skhn-giannh-xoyvarda-sto-hrwdeio" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/oresteiahrwdeio.jpg" alt="Η Ορέστεια του Αισχύλου σε σκην. Γιάννη Χουβαρδά στο Ηρώδειο" border="0" class="lazyload">
</div>
<div><span class="category">ΘΕΑΤΡΟ</span><span class="when">17:34</span></div>
<h3>Η Ορέστεια του Αισχύλου σε σκην. Γιάννη Χουβαρδά στο Ηρώδειο</h3>
<h4>Ηρώδειο - 7/9/2016</h4>
</a>
<a href="http://go.linkwi.se/z/10628-12/CD15753/?" target="_blank">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-ticket"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/spartoo2.png" alt="Κερδίστε 10€ δωροεπιταγή για τις αγορές σας σε επώνυμα παπούτσια και ρούχα!  " border="0" class="lazyload">
</div>
<div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span><span class="when">00:37</span></div>
<h3>Κερδίστε 10€ δωροεπιταγή για τις αγορές σας σε επώνυμα παπούτσια και ρούχα! </h3>
<h4>Nike, Converse, Addidas και άλλα!</h4>
</a>
<a href="/themata/63699/h-mama-poy-asprizei-ta-paidia-ths-h-egkyos-iepodoylh-kai-alla-ekswfrenika-sthn-amerikanikh-tv-video-list" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-th-list"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tyrabanksshow2.jpg" alt="H μαμά που ασπρίζει τα παιδιά της, η έγκυος ιεpόδουλη και άλλα εξωφρενικά στην αμερικανική tv" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">13:32</span></div>
<h3>H μαμά που ασπρίζει τα παιδιά της, η έγκυος ιεpόδουλη και άλλα εξωφρενικά στην αμερικανική tv</h3>
<h4>Απίστευτες ιστορίες στην εκπομπή της Tyra Banks</h4>
</a>
</div>
</div>
<br>
<center>
<div id="5799386"></div>
</center>
<script>
ADTECH.config.placements[5799386] = {params: { alias: '', key:'',target: '_blank' }};
ADTECH.enqueueAd(5799386);
ADTECH.executeQueue();
</script>
<br>
<script>
$().ready(function() {
    $("#topTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  defaultTab: "li#ttab-themata",
	  updateHash: false
	});
/*
	$("#tabs-news").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-viral").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-life").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-themata").mCustomScrollbar({
		theme:"dark-thick"
	});
*/	
});

</script>
<div class="postsRedHeader">δημοφιλέστερα</div>
<div id="topTabs" class="tab-container">
<ul class="etabs">
<li class="tab" id="ttab-news"><a href="#tabs-news">ΕΙΔΗΣΕΙΣ</a></li>
<li class="tab" id="ttab-themata"><a href="#tabs-themata">θΕΜΑΤΑ</a></li>
<li class="tab" id="ttab-life"><a href="#tabs-life">LIFE</a></li>
<li class="tab" id="ttab-viral"><a href="#tabs-viral">VIRAL</a></li>
</ul>
<div id="tabs-news" class="postEntries top">
<a href="/news/63840/lamia-xamos-gia-to-onoma-se-vaftisia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/baptisi600_818087889.jpg" alt="Λαμία: Χαμός για το όνομα σε βαφτίσια" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Λαμία: Χαμός για το όνομα σε βαφτίσια</h3>
<h4>Ο παππούς ξεσήκωσε την εκκλησία</h4>
</a>
<a href="/news/63797/poioi-dhmosiografoi-pane-sto-neo-kanali-toy-marinakh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/marinakhs_kanali_11.jpg" alt="Ποιοι δημοσιογράφοι πάνε στο νέο κανάλι του Μαρινάκη" border="0" class="lazyload">
</div>
<div><span class="category">MEDIA</span></div>
<h3>Ποιοι δημοσιογράφοι πάνε στο νέο κανάλι του Μαρινάκη</h3>
<h4>Γνωστά ονόματα - Ποιος θα έχει πρωταγωνιστικό ρόλο</h4>
</a>
<a href="/news/63765/efyge-apo-th-zwh-o-dhmosiografos-aleksandros-velios-h-sygklonistikh-teleytaia-praksh-toy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/alexbelios.jpg" alt="«Έφυγε» από τη ζωή ο δημοσιογράφος Αλέξανδρος Βέλιος - Η συγκλονιστική τελευταία πράξη του" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>«Έφυγε» από τη ζωή ο δημοσιογράφος Αλέξανδρος Βέλιος - Η συγκλονιστική τελευταία πράξη του</h3>
<h4>Κατέφυγε στη λύση της «μη υποβοηθούμενης ευθανασίας», στο σπίτι του στο Γέρακα</h4>
</a>
<a href="/news/63841/14xronos-odhgos-traymatise-mwro-me-agrotiko" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/koi-agrotiko-fillipiada1.jpg" alt="14χρονος οδηγός τραυμάτισε μωρό με αγροτικό" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>14χρονος οδηγός τραυμάτισε μωρό με αγροτικό</h3>
<h4>Ο ανήλικος παρέσυρε το ενός έτους κοριτσάκι</h4>
</a>
<a href="/news/63828/lamia-phge-sto-gamo-ths-me-eikosi-lewforeia-pics-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/lamia_gamos_main.jpg" alt="Λαμία: Πήγε στο γάμο της με είκοσι λεωφορεία" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Λαμία: Πήγε στο γάμο της με είκοσι λεωφορεία</h3>
<h4>Ένας αλλιώτικος και ασυνήθιστος γάμος</h4>
</a>
<a href="/news/63842/fwtografies-sok-apo-ton-sovaro-traymatismoy-fwtografoy-sthn-klhrwsh-ths-football-league" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/Koukouloforoi_league_0.jpg" alt="Φωτογραφίες-σοκ από τον σοβαρό τραυματισμού φωτογράφου στην κλήρωση της Football League" border="0" class="lazyload">
</div>
<div><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span></div>
<h3>Φωτογραφίες-σοκ από τον σοβαρό τραυματισμού φωτογράφου στην κλήρωση της Football League</h3>
<h4>Εισέβαλαν κουκουλοφόροι</h4>
</a>
<a href="/news/63831/h-apanthsh-ths-ieras-synodoy-gia-thn-katarghsh-ths-proseyxhs-sta-sxoleia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/prosefxh_1.jpg" alt="Η απάντηση της Ιεράς Συνόδου για την κατάργηση της προσευχής στα σχολεία" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Η απάντηση της Ιεράς Συνόδου για την κατάργηση της προσευχής στα σχολεία</h3>
<h4>Με αφορμή δημοσιεύματα που το αναφέρουν </h4>
</a>
<a href="/news/63805/h-epikoinwnia-toy-velioy-me-politiko-arxhgo-ligo-prin-pethanei" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/velios-93821.jpg" alt="Η επικοινωνία του Βέλιου με πολιτικό αρχηγό λίγο πριν πεθάνει" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Η επικοινωνία του Βέλιου με πολιτικό αρχηγό λίγο πριν πεθάνει</h3>
<h4>Τι του ζήτησε</h4>
</a>
</div>
<div id="tabs-themata" class="postEntries top">
<a href="/themata/63662/poios-einai-o-xrhstos-kalogritsas-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/kalo.jpg" alt="Ποιος είναι ο Χρήστος Καλογρίτσας" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Ποιος είναι ο Χρήστος Καλογρίτσας</h3>
<h4>30 χρόνια στα media</h4>
</a>
<a href="/themata/63711/ena-omorfo-paramythi-me-ena-adiko-telos-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/anggelopouloutra.jpg" alt="Ένα όμορφο παραμύθι, με ένα άδικο τέλος" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Ένα όμορφο παραμύθι, με ένα άδικο τέλος</h3>
<h4>Ο γάμος,η οικογένεια το οδυνηρό τέλος</h4>
</a>
<a href="/themata/62880/mega-to-teleytaio-antio-ligo-prin-pesei-h-aylaia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/mega_channel_12345.jpg" alt="Mega: Το τελευταίο αντίο λίγο πρίν πέσει η αυλαία" border="0" class="lazyload">
</div>
<div><span class="category">MEDIA</span></div>
<h3>Mega: Το τελευταίο αντίο λίγο πρίν πέσει η αυλαία</h3>
<h4>Ύστερα από τον οριστικό αποκλεισμό του από τη διαδικασία διεκδίκησης τηλεοπτικής άδειας</h4>
</a>
<a href="/themata/63348/pethane-apo-royfhgma-poy-toy-ekane-h-kopela-toy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/mexirouf.jpg" alt="Πέθανε από ρούφηγμα που του έκανε η κοπέλα του" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Πέθανε από ρούφηγμα που του έκανε η κοπέλα του</h3>
<h4>Δείτε τι του προκάλεσε</h4>
</a>
<a href="/themata/62247/se-ayth-th-xwra-oi-gynaikes-pantreyontai-h-mia-thn-allh-kai-exoyn-antres-epastes" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tanzaniawomen.jpg" alt="Σε αυτή τη χώρα οι γυναίκες παντρεύονται η μία την άλλη και έχουν άντρες εpαστές" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Σε αυτή τη χώρα οι γυναίκες παντρεύονται η μία την άλλη και έχουν άντρες εpαστές</h3>
<h4>Είδαν και απόειδαν, και έδωσαν τη λύση</h4>
</a>
<a href="/themata/62991/o-25xronos-vrykolakas-poy-zhta-apo-ton-kosmo-na-ton-apodextei" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/brikolakascoffin.jpg" alt="Ο 25χρονος βρυκόλακας που ζητά από τον κόσμο να τον αποδεχτεί" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span></div>
<h3>Ο 25χρονος βρυκόλακας που ζητά από τον κόσμο να τον αποδεχτεί</h3>
<h4>Το μόνο που θέλει είναι ίση μεταχείριση σαν όλους τους ανθρώπους</h4>
</a>
<a href="/themata/62995/apo-astegos-narkomanhsxrysos-olympionikhs-sta-35-toy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/ervin.jpg" alt="Από άστεγος ναρκομανής...χρυσός ολυμπιονίκης στα 35 του!" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Από άστεγος ναρκομανής...χρυσός ολυμπιονίκης στα 35 του!</h3>
<h4>Τα καλύτερα σενάρια τα γράφει η ζωή</h4>
</a>
<a href="/themata/63528/h-ellhnida-athlhtria-poy-kerdise-1-xryso-4-argyra-kai-ena-xalkino-metallio" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tsakonamaria.jpg" alt="H Ελληνίδα αθλήτρια που κέρδισε 1 χρυσό, 4 αργυρά και ένα χάλκινο μετάλλιο!" border="0" class="lazyload">
</div>
<div><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span></div>
<h3>H Ελληνίδα αθλήτρια που κέρδισε 1 χρυσό, 4 αργυρά και ένα χάλκινο μετάλλιο!</h3>
<h4>Η δύναμη της θέλησης της Μαρίας Τσάκωνα</h4>
</a>
</div>
<div id="tabs-life" class="postEntries top">
<a href="/life/63616/h-ellada-prwth-stis-kalyteres-xwres-toy-planhth" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/ellada-main.jpg" alt="Η Ελλάδα πρώτη στις καλύτερες χώρες του πλανήτη" border="0" class="lazyload">
</div>
<div><span class="category">ΤΑΞΙΔΙΑ</span></div>
<h3>Η Ελλάδα πρώτη στις καλύτερες χώρες του πλανήτη</h3>
<h4>Όπως την ψήφισαν οι αναγνώστες του Conde Nast Traveller</h4>
</a>
<a href="/life/63633/giati-ta-koyfeta-stis-mpomponieres-exoyn-mono-arithmo" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/koufeta1.jpg" alt="Γιατί τα κουφέτα στις μπομπονιέρες έχουν μονό αριθμό" border="0" class="lazyload">
</div>
<div><span class="category">FAMILY</span></div>
<h3>Γιατί τα κουφέτα στις μπομπονιέρες έχουν μονό αριθμό</h3>
<h4>Τι συμβολίζει το έθιμο</h4>
</a>
<a href="/life/63641/oi-anthrwpoi-me-ta-wraiotera-matia-ston-kosmo-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/eyes-93939.jpg" alt="Οι άνθρωποι με τα ωραιότερα μάτια στον κόσμο" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span></div>
<h3>Οι άνθρωποι με τα ωραιότερα μάτια στον κόσμο</h3>
<h4>Μια ματιά τους μόνο φτάνει</h4>
</a>
<a href="/life/63003/ta-wraiotera-aksesoyar-kai-epipla-ftiaxnontai-apo-ksylo-kerdiste" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-ticket"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/trigonis4.jpg" alt="Τα ωραιότερα αξεσουάρ και έπιπλα φτιάχνονται από ξύλο " border="0" class="lazyload">
</div>
<div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">STYLE</span></div>
<h3>Τα ωραιότερα αξεσουάρ και έπιπλα φτιάχνονται από ξύλο </h3>
<h4>Ioannis Trigonis Woodworks</h4>
</a>
<a href="/life/63723/6-trofes-poy-yposxontai-kalyterh-stytikh-leitoyrgia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/Man-looking-inside-his-shorts.jpg" alt="6 τροφές που υπόσχονται καλύτερη στυτική λειτουργία" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span></div>
<h3>6 τροφές που υπόσχονται καλύτερη στυτική λειτουργία</h3>
<h4>Για να μην αδυνατεί το σώμα, όταν θέλει το μυαλό</h4>
</a>
<a href="/life/63591/ta-zwdia-shmera-2-septemvrioy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/zodiaeikonidia.jpg" alt="Τα ζώδια σήμερα: 2 Σεπτεμβρίου " border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΔΙΑ</span></div>
<h3>Τα ζώδια σήμερα: 2 Σεπτεμβρίου </h3>
<h4>Αστρολογικές προβλέψεις κάθε μέρα από το E-Daily.gr</h4>
</a>
<a href="/life/62471/o-skylos-poy-exei-myrisei-550-periptwseis-karkinoy-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/skylos_karkinos_0.jpg" alt="Ο σκύλος που έχει μυρίσει 550 περιπτώσεις καρκίνου" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span></div>
<h3>Ο σκύλος που έχει μυρίσει 550 περιπτώσεις καρκίνου</h3>
<h4>Έσωσε τη ζωή ακόμη και του ιδιοκτήτη της</h4>
</a>
<a href="/life/63343/ti-loyloydia-na-fytepsw-gia-to-fthinopwro" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/autumnlouloudia.jpg" alt="Τι λουλούδια να φυτέψω για το φθινόπωρο;" border="0" class="lazyload">
</div>
<div><span class="category">ΣΠΙΤΙ</span></div>
<h3>Τι λουλούδια να φυτέψω για το φθινόπωρο;</h3>
<h4>Ιδέες για φυτέματα και όμορφα μπαλκόνια</h4>
</a>
</div>
<div id="tabs-viral" class="postEntries top">
<a href="/viral/63747/sygklonistikh-h-eikona-toy-hthopoioy-giwrgoy-vasileioy-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/vasileioy.jpg" alt="Συγκλονιστική η εικόνα του ηθοποιού Γιώργου Βασιλείου" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Συγκλονιστική η εικόνα του ηθοποιού Γιώργου Βασιλείου</h3>
<h4>«Καλό φθινόπωρο σε όλους τους αγαπημένους φίλους...» αναφέρει στο μήνυμά του</h4>
</a>
<a href="/viral/51206/ti-fwnaze-h-21xronh-sto-troxaio-toy-pantelidh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/pantelidistroxaio.jpg" alt="Τι φώναζε η 21χρονη στο τροχαίο του Παντελίδη" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Τι φώναζε η 21χρονη στο τροχαίο του Παντελίδη</h3>
<h4>Την ώρα του απεγκλωβισμού της</h4>
</a>
<a href="/viral/63813/fwtografia-adeiazoyn-to-grafeio-ths-tremh-sto-mega-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tremi0mega.jpg" alt="Φωτογραφία: Αδειάζουν το γραφείο της Τρέμη στο Mega" border="0" class="lazyload">
</div>
<div><span class="category">MEDIA</span></div>
<h3>Φωτογραφία: Αδειάζουν το γραφείο της Τρέμη στο Mega</h3>
<h4>Το κανάλι φαίνεται να είναι κοντά στο κλείσιμο του</h4>
</a>
<a href="/viral/63760/o-antwnhs-remos-sthn-prwth-emfanish-toy-prin-apo-24-xronia-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/remos_antonis-42.jpg" alt="Ο Αντώνης Ρέμος στην πρώτη εμφάνισή του πριν από 24 χρόνια" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Ο Αντώνης Ρέμος στην πρώτη εμφάνισή του πριν από 24 χρόνια</h3>
<h4>Αγνώριστος</h4>
</a>
<a href="/viral/63619/nearos-vrhke-ton-teleio-tropo-gia-na-koimatai-mesa-sthn-taksh-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/schoolprank.jpg" alt="Νεαρός βρήκε τον τέλειο τρόπο για να... κοιμάται μέσα στην τάξη" border="0" class="lazyload">
</div>
<div><span class="category">ΧΙΟΥΜΟΡ</span></div>
<h3>Νεαρός βρήκε τον τέλειο τρόπο για να... κοιμάται μέσα στην τάξη</h3>
<h4>Μπορείτε να το προσπαθήσετε και εσείς, αρκεί να μη ροχαλίζετε</h4>
</a>
<a href="/viral/52177/deite-pws-einai-shmera-h-barbra-streisand-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Streisand_0.jpg" alt="Δείτε πώς είναι σήμερα η Barbra Streisand" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Δείτε πώς είναι σήμερα η Barbra Streisand</h3>
<h4>Δημοσίευση σε κοινωνικά δίκτυα</h4>
</a>
<a href="/viral/63513/mporei-mia-sata-na-meinei-ston-aera-an-th-fysoyn-anemisthres-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/saita2.jpg" alt="Μπορεί μια σαΐτα να μείνει στον αέρα αν τη φυσούν ανεμιστήρες;" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span></div>
<h3>Μπορεί μια σαΐτα να μείνει στον αέρα αν τη φυσούν ανεμιστήρες;</h3>
<h4>Το βίντεο που σάρωσε</h4>
</a>
<a href="/viral/63774/h-courtney-stodden-kykloforei-me-koykla-mwro-meta-thn-apovolh-ths-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/psuxiygeia.jpg" alt="Η Courtney Stodden κυκλοφορεί με κούκλα μωρό μετά την αποβολή της" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Η Courtney Stodden κυκλοφορεί με κούκλα μωρό μετά την αποβολή της</h3>
<h4>Με ξανθιά περούκα και καπέλο καλύπτει το ξυρισμένο της κεφάλι</h4>
</a>
</div>
</div>
</div>
</div>
<div id="footerBgWide">
<center>
<div id="footerMain">
<div class="footer hideOnSmallScreen" style="width:12%;">
<span class="footer_optionsHeader"><a href="/news">Ειδήσεις</a></span><br>
<span class="footer_options">
<a href="/news">Τελευταία νέα</a>
<a href="/ellada">Ελλάδα</a>
<a href="/kosmos">Κόσμος</a>
<a href="/politiki">Πολιτική</a>
<a href="/oikonomia">Οικονομία</a>
<a href="/athlitismos">Αθλητισμός</a>
<a href="/epistimi">Επιστήμη</a>
<a href="/politismos">Πολιτισμός</a>
</span>
</div>
<div class="footer hideOnSmallScreen" style="width:12%;">
<div class="footer_optionsHeader"><a href="/themata">Θέματα</a></div><br>
<div class="footer_optionsHeader"><a href="/quiz">Κουίζ</a></div><br>
<div class="footer_optionsHeader"><a href="/viral">Viral</a></div><br>
<div class="footer_optionsHeader"><a href="/lol">LOL</a></div>
<div class="footer_optionsHeader"><a href="/omg">OMG</a></div>
<div class="footer_optionsHeader"><a href="/juicy">JUICY</a></div>
<div class="footer_optionsHeader"><a href="/alist">A-LIST</a></div>
<div class="footer_optionsHeader"><a href="/geeky">GEEKY</a></div>
<div class="footer_optionsHeader"><a href="/cute">CUTE</a></div>
<div class="footer_optionsHeader"><a href="/retro">RETRO</a></div>
</div>
<div class="footer hideOnSmallScreen" style="width:12%;">
<span class="footer_optionsHeader"><a href="/life">Life</a></span><br>
<span class="footer_options">
<a href="/food">Food</a>
<a href="/bodymind">Body+Mind</a>
<a href="/travel">Ταξίδια</a>
<a href="/style">Style</a>
<a href="/spiti">Σπίτι</a>
<a href="/family">Family</a>
<a href="/sxeseis">Σχέσεις</a>
</span><br>
</div>
<div class="footer hideOnSmallScreen" style="width:18%;">
<div class="footer_optionsHeader"><a href="/entertainment">Ψυχαγωγία</a></div><br>
<span class="footer_options">
<a href="/agenda">Agenda</a>
<a href="/wintickets">Κερδίστε Προσκλήσεις</a><br>
<a href="/music">Μουσική</a>
<a href="/theater">Θέατρο+Χορός</a>
<a href="/arts">Εικαστικά</a>
<a href="/cinema">Σινεμά</a>
<a href="/nightlife">Νάιτ Λάιφ</a>
<a href="/city">Πόλη</a>
</span>
</div>
<div class="footer fullOnSmallScreen" style="width:32%;">
<div class="footer_optionsHeader">Newsletter</div><br>
<style type="text/css">#mc-embedded-subscribe-form{margin:0;padding:0}#mc_embed_signup{display:table;clear:both;text-align:left;line-height:normal;}#mc_embed_signup .mc-field-group{display:table;margin:10px 0 10px 0;padding:0;width:100%}#mc_embed_signup li{display:block;float:left;margin-top:12px;width:33.333%;padding:0;font-weight:400;font-size:1.25em;}#mc_embed_signup .checkbox{width:22px;height:22px;border:1px solid #e3e3e3;background:#fff;margin:0;padding:0}#mc_embed_signup .checkbox:before{content:"";display:block;width:23px;height:23px;background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 0 0 no-repeat;}#mc_embed_signup .checkbox:checked:before{background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') -32px 0 no-repeat;}#mc_embed_signup label{margin-left:5px;padding-top:-5px;}#mc_embed_signup .email{font-weight:normal;height:45px;width:55%;float:left;display:inline-block;background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 10px -22px no-repeat;color:#000;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;padding:0 0 0 46px;border:1px solid #d9d9d9}#mc_embed_signup .email.focus{color:#000;}#mc_embed_signup .email.placeholder{color:#e3e3e3;}#mc_embed_signup .submit-button{background:#0079ff;text-align:center;float:left;width:30%;height:47px;border:none;font-size:14px;text-transform:uppercase;color:#FFF;letter-spacing:1px;cursor:pointer;display:inline-block;padding-top:6px;margin-left:3px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;}#mc_embed_signup .submit-button:hover{background:#2e78b3;text-decoration:none;}#mc_embed_signup div.mce_inline_error{background-color:#fff;color:#804040;}.clearfix{padding:0;margin:0}</style>
<div id="mc_embed_signup" class="clearfix">
<form action="//nextweb.us10.list-manage.com/subscribe/post?u=037b671af872ebf3eb1ceac4a&amp;id=5ab11026ce" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div class="clearfix">Εγραφείτε στα newsletters του E-Daily.gr για να μαθαίνετε τα καλύτερα στο email σας:</div>
<ul class="mc-field-group input-group">
<li>
<input type="checkbox" class="checkbox" value="1" name="group[1109][1]" id="mce-group[1109]-1109-0" checked="checked">
<label for="mce-group[1109]-1109-0">Ειδήσεις</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="2" name="group[1109][2]" id="mce-group[1109]-1109-1" checked="checked">
<label for="mce-group[1109]-1109-1">Viral</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="4" name="group[1109][4]" id="mce-group[1109]-1109-2" checked="checked">
<label for="mce-group[1109]-1109-2">Life</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="8" name="group[1109][8]" id="mce-group[1109]-1109-3" checked="checked">
<label for="mce-group[1109]-1109-3">E-Agenda</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="16" name="group[1109][16]" id="mce-group[1109]-1109-4" checked="checked">
<label for="mce-group[1109]-1109-4">E-Weekly</label>
</li>
<li></li>
</ul>
<div class="mc-field-group" style="margin-top:25px;">
<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Συμπληρώστε το email σας">
<input type="submit" class="submit-button" value="ΕΓΓΡΑΦΗ" name="subscribe" id="mc-embedded-subscribe">
</div>
</form>
<div id="mce-responses" class="clear">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>
<div style="position: absolute; left: -5000px;"><input type="text" name="b_037b671af872ebf3eb1ceac4a_5ab11026ce" tabindex="-1" value=""></div>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email'; /*
 * Translated default messages for the $ validation plugin.
 * Locale: EL
 */
$.extend($.validator.messages, {
	required: "<span class=asterisk></span> Αυτό το πεδίο είναι υποχρεωτικό.",
	remote: "<span class=asterisk></span> Παρακαλώ διορθώστε αυτό το πεδίο.",
	email: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη διεύθυνση email.",
	url: "<span class=asterisk></span> Παρακαλώ εισάγετε ένα έγκυρο URL.",
	date: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία.",
	dateISO: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία (ISO).",
	number: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό.",
	digits: "<span class=asterisk></span> Παρακαλώ εισάγετε μόνο αριθμητικά ψηφία.",
	creditcard: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό πιστωτικής κάρτας.",
	equalTo: "<span class=asterisk></span> Παρακαλώ εισάγετε την ίδια τιμή ξανά.",
	accept: "<span class=asterisk></span> Παρακαλώ εισάγετε μια τιμή με έγκυρη επέκταση αρχείου.",
	maxlength: $.validator.format("<span class=asterisk></span> Παρακαλώ εισάγετε μέχρι και {0} χαρακτήρες."),
	minlength: $.validator.format("Παρακαλώ εισάγετε τουλάχιστον {0} χαρακτήρες."),
	rangelength: $.validator.format("Παρακαλώ εισάγετε μια τιμή με μήκος μεταξύ {0} και {1} χαρακτήρων."),
	range: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεταξύ {0} και {1}."),
	max: $.validator.format("Παρακαλώ εισάγετε μια τιμή μικρότερη ή ίση του {0}."),
	min: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεγαλύτερη ή ίση του {0}.")
});}(jQuery));var $mcj = jQuery.noConflict(true);</script>
 
</div>
<div class="footer footerRight footer_options fullOnSmallScreen" style="width:14%; text-align: right">
<div class="footer_optionsHeader">Follow us</div><br>
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<span>Facebook</span>
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<span>Twitter</span>
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<span>Google+</span>
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<span>Instagram</span>
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<span>YouTube</span>
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<span>RSS</span>
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</div>
</center>
</div>
<center>
<div id="footerMain">
<div class="footer footer21">
<a href="javascript:openContact();">Επικοινωνία</a>&nbsp;&nbsp;&nbsp;
<a href="javascript:openContact();">Διαφήμιση</a>&nbsp;&nbsp;&nbsp;
<a href="/rules.asp">Όροι Διαγωνισμών</a>&nbsp;&nbsp;&nbsp;
<a href="/privacy.asp">Privacy</a>&nbsp;&nbsp;&nbsp;
<a href="/terms.asp">Terms of use</a>
<br>
<a href="http://www.e-radio.gr" target="_blank">E-Radio Hellas </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.e-radio.com.cy" target="_blank">E-Radio Cyprus </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.e-radio.co.uk" target="_blank">E-Radio UK </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.onestreaming.com" target="_blank">One Streaming </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.arionradio.com" target="_blank">Arion Radio </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.athensparty.com" target="_blank">Athens Party </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.akous.gr" target="_blank">Akous </a> &nbsp;&nbsp;&nbsp;
<br>
© 2015 All Right Reserved. "E-DAILY" is a registered trademark
</div>
<div class="footer footer22">
<a href="http://www.nextweb.gr" target="_blank"><img src="/_img/nextweb.png" border="0" alt="Nextweb"></a>
</div>
</div>
</center>
</div>
</div>
<script type='text/javascript'>
$().ready(function() {
	
	if( $('#topHeaderMobile').length ){

		$('#mobilemenu_pad').infinitypush({
			openingspeed: 300,
			closingspeed: 300
		});
		
		/*
		$('#mobilemenu_icon').click(function(){
			
			$(this).toggleClass('open');
			//slideout.toggle();
			
		});
	
		slideout.on('open', function() {
			if ($('#mobilemenu_icon').hasClass('open')==false){$('#mobilemenu_icon').addClass('open')}
		});
		slideout.on('close', function() {
			if ($('#mobilemenu_icon').hasClass('open')==true){$('#mobilemenu_icon').removeClass('open')}
	
		});
		*/
	}

});
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-551bf8914936f63f" async="async"></script>
<script type="text/javascript">

	<!-- Google -->
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-504700-32', 'auto');
  ga('send', 'pageview');

	<!-- Tynt -->
	if(document.location.protocol=='http:'){
	 var Tynt=Tynt||[];Tynt.push('aFq4livo0r4yzYacwqm_6r');Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false};
	 (function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
	}
	
	<!-- Quantcast Tag -->
	var _qevents = _qevents || [];
	
	(function() {
	var elem = document.createElement('script');
	elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
	elem.async = true;
	elem.type = "text/javascript";
	var scpt = document.getElementsByTagName('script')[0];
	scpt.parentNode.insertBefore(elem, scpt);
	})();
	
	_qevents.push({
	qacct:"p-49SuwTaH2w2BM"
	});

</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-49SuwTaH2w2BM.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6032644253807', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6032644253807&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1"/></noscript>
 
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3125699,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('http://s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="http://www.histats.com" target="_blank"><img src="http://sstatic1.histats.com/0.gif?3125699&101" alt="web hit counter" border="0"></a></noscript>
<script src="//fast.wistia.net/labs/fresh-url/v1.js" async></script>
<script type="text/javascript">
( function() {
    var info = getTagInfo();
    var alias = 'edhomeskin_display';
        alias = alias !== '' ? 'alias=' + ( alias.split("_") ? alias.split("_")[ 0 ] : alias ) + '_' + info.alias : '';
    document.write('<scr'+'ipt language="javascript1.1" src="http://' + info.domain + '.adtech.de/addyn/3.0/1370.1/4713596/0/-1/ADTECH;' + alias + ';loc=100;target=_blank;key=;grp=' + info.adgroupid + ';misc=' + new Date().getTime() + '"></scri'+'pt>');
})();</script>
<noscript>Please enable scripting languages in your browser.</noscript>
</body>
</html>
