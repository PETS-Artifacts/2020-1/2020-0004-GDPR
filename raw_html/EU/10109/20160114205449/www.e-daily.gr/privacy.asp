
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>E-Daily.gr Πολιτική Απορρήτου</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="charset" content="utf-8"/>
<meta http-equiv="Refresh" content="900"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta content="Πολιτική Απορρήτου" name="Description"/>
<meta content="" name="keywords"/>
<meta property="og:title" content="E-Daily.gr Πολιτική Απορρήτου"/>
<meta property="og:description" content=""/>
<meta property="og:site_name" content="E-Daily.gr"/>
<meta property="og:type" content="Website"/>
<meta property="og:image" content="http://www.e-daily.gr/_img/fblogo.png"/>
<meta property="fb:app_id" content="1590239487856378"/>
<meta property="og:url" content="http://www.e-daily.gr"/>
<link rel="canonical" href="http://www.e-daily.gr"/>
<link rel="stylesheet" href="/_css/post.css"/>
<link rel="stylesheet" href="/_js/scrollbar/jquery.mCustomScrollbar.css"/>
<meta name="apple-itunes-app" content="app-id=776881885">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="/_ico/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/_ico/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/_ico/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/_ico/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/_ico/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/_ico/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/_ico/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/_ico/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/_ico/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/_ico/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/_ico/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/_ico/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/_ico/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/_ico/manifest.json">
<link rel="mask-icon" href="/_ico/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/_ico/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/_ico/mstile-144x144.png">
<meta name="msapplication-config" content="/_ico/browserconfig.xml">
<meta name="theme-color" content="#ff0004">
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,400italic|Roboto+Condensed:300,400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href="/_css/style.css" rel="stylesheet" type="text/css"/>
<link href="/_css/navigation.css" rel="stylesheet" type="text/css"/>
<link href="/_js/menu/mgmenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/mobilemenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/structure.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/_img/forecastfont/weather.css"/>
<link rel="stylesheet" href="/_img/edailyicons/css/edaily.css">
<link rel="stylesheet" href="/_img/edailyicons/css/animation.css"><!--[if IE 7]><link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->
<link rel="stylesheet" type="text/css" href="/_js/mmenu/jquery.ma.infinitypush.css"/>
<script language="javaScript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script language="javaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/1.0.1/lazysizes.min.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/autocomplete/jquery.autocomplete.min.js"></script>
<script language="javaScript" type="text/javascript" src="http://www.e-radio.gr/cache/mediadata_1.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.popupWindow.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.socialstats.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/menu/mgmenu_plugins.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/menu/mgmenu.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/mmenu/jquery.ma.infinitypush.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/tools_all.js"></script>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
<script type='text/javascript'>
if(document.location.protocol=='http:'){
var Tynt=Tynt||[];Tynt.push('d91U86Bp0r5zpGrkHcnlKl');
Tynt.i={"ap":"�����������:","aw":"15","st":true,"su":false,"domain":"e-daily.gr"};
(function(){var h,s=document.createElement('script');s.src='http://cdn.tynt.com/ti.js';
h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();}
</script>
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"http://www.e-daily.gr/privacy.asp","theme":"light-bottom"};
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<script src="/_js/tabs/jquery.easytabs.js" type="text/javascript"></script>
<script src="/_js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body>
<script type="text/javascript">
var nuggprof ="";
var nuggrid= encodeURIComponent(top.location.href);
var nuggtg= encodeURIComponent("ContentCategory");
document.write('<scr'+'ipt type=\"text/javascript\" src=\"http://adweb.nuggad.net/rc?nuggn=1230610253&nuggsid=113401978&nuggrid='+nuggrid+'&nuggtg='+nuggtg+'"><\/scr'+'ipt>');
</script>
<div id="wrapper">
<nav id="mobilemenu_pad">
<ul class="sections">
<li><a href="/news">Ειδήσεις</a>
<li><a href="/viral">Viral</a>
<li><a href="/quiz">Quiz</a>
<li><a href="/life">Life</a>
<li><a href="/themata">Θέματα</a>
<li><a href="/entertainment">Ψυχαγωγία</a>
<li><a href="/agenda">E-Agenda</a>
</ul>
<ul class="sections badges">
<li><a href="/omg"><img src="/_img/feeds/omg.png" border="0"/> OMG</a>
<li><a href="/lol"><img src="/_img/feeds/lol.png" border="0"/> LOL</a>
<li><a href="/juicy"><img src="/_img/feeds/juicy.png" border="0"/> Juicy</a>
<li><a href="/alist"><img src="/_img/feeds/a-list.png" border="0"/> A-List</a>
<li><a href="/cute"><img src="/_img/feeds/cute.png" border="0"/> Cute</a>
<li><a href="/retro"><img src="/_img/feeds/retro.png" border="0"/> Retro</a>
<li><a href="/geeky"><img src="/_img/feeds/geeky.png" border="0"/> Geeky</a>
</ul>
<ul class="sections topics">
<li><a href="/food">Food</a>
<li><a href="/bodymind">Body+Mind</a>
<li><a href="/travel">Ταξίδια</a>
<li><a href="/style">Style</a>
<li><a href="/spiti">Σπίτι</a>
<li><a href="/family">Family</a>
<li><a href="/sxeseis">Σχέσεις</a>
</ul>
<ul class="sections topics">
<li><a href="/agenda">Agenda</a>
<li><a href="/music">Μουσική</a>
<li><a href="/theater">Θέατρο</a>
<li><a href="/arts">Εικαστικά</a>
<li><a href="/cinema">Σινεμά</a>
<li><a href="/nightlife">Νάιτ Λάιφ</a>
<li><a href="/city">Πόλη</a>
</ul>
<ul class="sections topics">
<li><a href="/weather">Καιρός</a>
</ul>
<div class="social">
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</nav>
<div id="main">
<form id="skinform"></form>
<div id="spider"></div>
<div id="topHeaderWarper">
<div id="topHeader">
<div id="topHeaderA">
<a href="/" class="mainlogo"><img src="/_img/edailygr_beta_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία"/></a>
</div>
<div id="topHeaderB" class="feedIcons">
<a href="/omg"><img src="/_img/feeds/omg.png" border="0" alt="OMG Feed"/></a>
<a href="/lol"><img src="/_img/feeds/lol.png" border="0" alt="LOL Feed"/></a>
<a href="/juicy"><img src="/_img/feeds/juicy.png" border="0" alt="Juicy Feed"/></a>
<a href="/retro"><img src="/_img/feeds/retro.png" border="0" alt="Retro Feed"/></a>
<a href="/alist"><img src="/_img/feeds/a-list.png" border="0" alt="A-List Feed"/></a>
<a href="/cute"><img src="/_img/feeds/cute.png" border="0" alt="Cute Feed"/></a>
<a href="/geeky"><img src="/_img/feeds/geeky.png" border="0" alt="Geeky Feed"/></a>
</div>
</div>
</div>
<div id="topHeaderWarper2">
<div id="mainmenu" class="mgmenu_container">
<ul class="mgmenu">
<li class="mgmenu_button"></li> 
<div id="mainmenu_logo"><a href="/"><img src="/_img/edailygr_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία" height="33"/></a></div>
<li><span class="mgmenu_drop"><a href="/news">Ειδήσεις</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/news/48908/isis-aphgagan-600-paidia-gia-na-ta-kanoyn-vomvistes-aytoktonias" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">ISIS: Απήγαγαν 600 παιδιά για να τα κάνουν βομβιστές αυτοκτονίας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paidia-isis-12.jpg" alt="ISIS: Απήγαγαν 600 παιδιά για να τα κάνουν βομβιστές αυτοκτονίας" border="0" class="cropimg lazyload">
</a>
<a href="/news/48907/h-fwfh-diagrafei-ton-grhgorako" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΙΤΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η Φώφη διαγράφει τον Γρηγοράκο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/grigorakos1.jpg" alt="Η Φώφη διαγράφει τον Γρηγοράκο" border="0" class="cropimg lazyload">
</a>
<a href="/news/48904/synelhfthh-ypopshfios-voyleyths-toy-syriza-gia-dwrolhpsia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΟΙΚΟΝΟΜΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Συνελήφθη υποψήφιος βουλευτής του ΣΥΡΙΖΑ για δωροληψία</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/500euro.jpg" alt="Συνελήφθη υποψήφιος βουλευτής του ΣΥΡΙΖΑ για δωροληψία" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3">
<div class="str8">
<a href="/news" class="more">ΠΕΡΙΣΣΟΤΕΡΕΣ ΕΙΔΗΣΕΙΣ</a>
<a href="/news/48897/ntaiselmploym-egine-sklhrh-doyleia-gia-to-asfalistiko" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΟΙΚΟΝΟΜΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ντάισελμπλουμ: Έγινε σκληρή δουλειά για το ασφαλιστικό</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/geroun.jpg" alt="Ντάισελμπλουμ: Έγινε σκληρή δουλειά για το ασφαλιστικό" border="0" class="cropimg lazyload">
</a>
<a href="/news/48892/epifanhs-giatros-ekspepmatwse-se-proswpo-asthenoys" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Επιφανής γιατρός εκσπεpμάτωσε σε πρόσωπο ασθενούς</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Mount_Sinai.jpg" alt="Επιφανής γιατρός εκσπεpμάτωσε σε πρόσωπο ασθενούς" border="0" class="cropimg lazyload">
</a>
<a href="/news/48891/to-islamiko-kratos-pisw-apo-thn-epithesh-sthn-tzakarta" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το Ισλαμικό Κράτος πίσω από την επίθεση στην Τζακάρτα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/indonisia_isis.jpg" alt="Το Ισλαμικό Κράτος πίσω από την επίθεση στην Τζακάρτα" border="0" class="cropimg lazyload">
</a>
<a href="/news/48890/me-apoklish-22-dis-eyrw-sta-esoda-ekleise-to-2015" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΟΙΚΟΝΟΜΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Με απόκλιση 2,2 δισ. ευρώ στα έσοδα έκλεισε το 2015</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/xrimata-xartonomismata-euro.jpg" alt="Με απόκλιση 2,2 δισ. ευρώ στα έσοδα έκλεισε το 2015" border="0" class="cropimg lazyload">
</a>
<a href="/news/48888/pethane-o-kathhghths-sneip" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Πέθανε ο «Καθηγητής Σνέιπ»</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/AlanRickman.jpg" alt="Πέθανε ο «Καθηγητής Σνέιπ»" border="0" class="cropimg lazyload">
</a>
</div>
</div>
<div class="col_12 inlineMenu">
<a href="/news">ΤΕΛΕΥΤΑΙΑ ΝΕΑ</a>
<a href="/ellada">ΕΛΛΑΔΑ</a>
<a href="/kosmos">ΚΟΣΜΟΣ</a>
<a href="/politiki">ΠΟΛΙΤΙΚΗ</a>
<a href="/oikonomia">ΟΙΚΟΝΟΜΙΑ</a>
<a href="/athlitismos">ΑΘΛΗΤΙΣΜΟΣ</a>
<a href="/epistimi">ΕΠΙΣΤΗΜΗ</a>
<a href="/politismos">ΠΟΛΙΤΙΣΜΟΣ</a>
<a href="http://feeds.feedburner.com/eradioblog" target="_blank">RSS FEED</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/themata">Θέματα</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/themata/48846/anakoinwthhkan-oi-ypopshfiotes-gia-ta-xrysa-vatomoyra-list" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-th-list"></span></div>
<div class="menuItem_title">Ανακοινώθηκαν οι υποψηφιότες για τα Χρυσά Βατόμουρα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/berries.jpg" alt="Ανακοινώθηκαν οι υποψηφιότες για τα Χρυσά Βατόμουρα" border="0" class="cropimg lazyload">
</a>
<a href="/themata/48702/enas-mystikos-kwdikos-ths-apple-gia-ola-ta-iphone" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΕΧΝΟΛΟΓΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ένας μυστικός κωδικός της Apple για όλα τα iPhone</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/iphones.jpg" alt="Ένας μυστικός κωδικός της Apple για όλα τα iPhone" border="0" class="cropimg lazyload">
</a>
<a href="/themata/48864/kathhghth-agglikwn-eixe-o-alekshs-tsipras" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΙΤΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Kαθηγητή αγγλικών είχε ο Αλέξης Τσίπρας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/clinton-tsipras-12.jpg" alt="Kαθηγητή αγγλικών είχε ο Αλέξης Τσίπρας" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΑ ΘΕΜΑΤΑ</a>
<a href="/themata/48841/mpar-sthn-aystria-apagoreyei-thn-eisodo-se-aitoyntes-asylo" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Μπαρ στην Αυστρία απαγορεύει την είσοδο σε αιτούντες άσυλο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/baraustria.jpg" alt="Μπαρ στην Αυστρία απαγορεύει την είσοδο σε αιτούντες άσυλο" border="0" class="cropimg lazyload">
</a>
<a href="/themata/48814/h-daily-mail-grafei-gia-thn-apisteyth-istoria-diaswshs-toy-ellhna-billy-pics-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-picture"></span></div>
<div class="menuItem_title">Η Daily Mail γράφει για την απίστευτη ιστορία διάσωσης του «Έλληνα» Billy</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/billy8.jpg" alt="Η Daily Mail γράφει για την απίστευτη ιστορία διάσωσης του «Έλληνα» Billy" border="0" class="cropimg lazyload">
</a>
<a href="/themata/48759/kaneis-den-agorazei-ayto-to-spiti-manteyete-giati-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Κανείς δεν αγοράζει αυτό το σπίτι: Mαντεύετε γιατί;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/silencelambs.jpg" alt="Κανείς δεν αγοράζει αυτό το σπίτι: Mαντεύετε γιατί;" border="0" class="cropimg lazyload">
</a>
<a href="/themata/48807/h-eidhsh-poy-odhghse-se-100-ekat-allhlepidraseis-sto-facebook" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>MEDIA</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η είδηση που οδήγησε σε 100 εκατ. αλληλεπιδράσεις στο Facebook </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Facebook.jpg" alt="Η είδηση που οδήγησε σε 100 εκατ. αλληλεπιδράσεις στο Facebook " border="0" class="cropimg lazyload">
</a>
<a href="/themata/48820/oi-pilotoi-se-olo-ton-kosmo-exoyn-ksexasei-na-petoyn-xeirokinhta" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Οι πιλότοι σε όλο τον κόσμο έχουν ξεχάσει να πετούν χειροκίνητα!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/cockpit-12.jpg" alt="Οι πιλότοι σε όλο τον κόσμο έχουν ξεχάσει να πετούν χειροκίνητα!" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
<a href="/afieromata">ΑΦΙΕΡΩΜΑΤΑ</a>
<a href="/eikones">ΩΡΑΙΕΣ ΕΙΚΟΝΕΣ</a>
<a href="/istorika">ΙΣΤΟΡΙΚΑ</a>
<a href="/gay-lesbian">LGBT</a>
<a href="/tech">ΤΕΧΝΟΛΟΓΙΑ</a>
<a href="/periballon">ΠΕΡΙΒΑΛΛΟΝ</a>
<a href="/architect">ΑΡΧΙΤΕΚΤΟΝΙΚΗ</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/viral">Viral</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/viral/48909/poso-xronwn-egine-h-maria-kavogiannh-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Πόσο χρονών έγινε η Μαρία Καβογιάννη;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/maria-kavogianni-people-131-1.jpg" alt="Πόσο χρονών έγινε η Μαρία Καβογιάννη;" border="0" class="cropimg lazyload">
</a>
<a href="/viral/48894/anakoinwthhkan-oi-ypopshfiothtes-gia-ta-vraveia-oskar-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ανακοινώθηκαν οι υποψηφιότητες για τα βραβεία Όσκαρ 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/oscar.jpg" alt="Ανακοινώθηκαν οι υποψηφιότητες για τα βραβεία Όσκαρ 2016" border="0" class="cropimg lazyload">
</a>
<a href="/viral/48870/o-liagkas-fetes-ekdikhsh-se-ekswfyllo-me-resital-retoys-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Ο Λιάγκας φέτες: Εκδίκηση σε εξώφυλλο με ρεσιτάλ ρετούς</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Liagkasre-main.jpg" alt="Ο Λιάγκας φέτες: Εκδίκηση σε εξώφυλλο με ρεσιτάλ ρετούς" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΟ VIRAL</a>
<a href="/viral/48881/ellhnofreneia-o-tsolias-fwtografhthhke-ws-raxhl-makrh-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Ελληνοφρένεια: Ο Τσολιάς φωτογραφήθηκε ως Ραχήλ Μακρή!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tsolias-raxil-main.jpg" alt="Ελληνοφρένεια: Ο Τσολιάς φωτογραφήθηκε ως Ραχήλ Μακρή!" border="0" class="cropimg lazyload">
</a>
<a href="/viral/48849/agria-epithesh-koygia-kata-lazopoyloy-gnwrizoyme-ti-proswpikh-idiwtikh-zwh-exei-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>MEDIA</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Αγρια επίθεση Κούγια κατά Λαζόπουλου: «Γνωρίζουμε τι προσωπική ιδιωτική ζωή έχει...» </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/kougias3.jpg" alt="Αγρια επίθεση Κούγια κατά Λαζόπουλου: «Γνωρίζουμε τι προσωπική ιδιωτική ζωή έχει...»  " border="0" class="cropimg lazyload">
</a>
<a href="/viral/48877/den-kseroyn-ti-toys-ginetai-sth-sxolh-dhmosias-dioikhshs" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Δεν ξέρουν τι τους γίνεται στη Σχολή Δημόσιας Διοίκησης</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/gkafa_main.jpg" alt="Δεν ξέρουν τι τους γίνεται στη Σχολή Δημόσιας Διοίκησης" border="0" class="cropimg lazyload">
</a>
<a href="/viral/48873/to-forema-poy-ksepoylhse-se-58-lepta-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">To φόρεμα που ξεπούλησε σε 58 λεπτά! </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/foremaobama.jpg" alt="To φόρεμα που ξεπούλησε σε 58 λεπτά! " border="0" class="cropimg lazyload">
</a>
<a href="/viral/48851/ena-mikro-skylaki-etrepse-toys-diarrhktes-se-fygh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ένα μικρό σκυλάκι έτρεψε τους διαρρήκτες σε φυγή</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dog_little.jpg" alt="Ένα μικρό σκυλάκι έτρεψε τους διαρρήκτες σε φυγή" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
<a href="/omg">OMG</a>
<a href="/lol">LOL</a>
<a href="/tabloid">TABLOID</a>
<a href="/sexy">SEXY</a>
<a href="/zoa">ΖΩΑ</a>
<a href="/media">MEDIA</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/quiz">Κουίζ</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/quiz/47817/mporeis-na-mantepseis-poswn-xronwn-einai-oi-star-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείς να μαντέψεις πόσων χρονών είναι οι σταρ;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ariana_grande1.jpg" alt="Μπορείς να μαντέψεις πόσων χρονών είναι οι σταρ;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/48407/ti-pitsa-eisai-symfwna-me-ton-xarakthra-soy-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Τι πίτσα είσαι, σύμφωνα με τον χαρακτήρα σου;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/pizzayes.jpg" alt="Τι πίτσα είσαι, σύμφωνα με τον χαρακτήρα σου;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/48411/poia-einai-h-kalyterh-makaronada-vote" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΨΗΦΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ποια είναι η καλύτερη μακαρονάδα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/pastafork.jpg" alt="Ποια είναι η καλύτερη μακαρονάδα;" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΑ QUIZ</a>
<a href="/quiz/48341/tha-kerdizes-sto-den-exw-logia-tsekare-tis-gnwseis-soy-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Θα κέρδιζες στο «Δεν έχω λόγια»; Τσέκαρε τις γνώσεις σου!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/kapoutzidis3344.jpg" alt="Θα κέρδιζες στο «Δεν έχω λόγια»; Τσέκαρε τις γνώσεις σου!" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/48185/poses-hmeres-ths-zwh-sas-tha-spatalhsete-doyleyontas-katharizontas-h-kanontas-seks-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Πόσες ημέρες της ζωή σας θα σπαταλήσετε δουλεύοντας, καθαρίζοντας ή κάνοντας σeξ</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/timespent.jpg" alt="Πόσες ημέρες της ζωή σας θα σπαταλήσετε δουλεύοντας, καθαρίζοντας ή κάνοντας σeξ" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/48238/poios-theleis-na-ekproswphsei-thn-ellada-sthn-eurovision-vote" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΨΗΦΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ποίος θέλεις να εκπροσωπήσει την Ελλάδα στην Eurovision;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paparizou-greek-flag.jpg" alt="Ποίος θέλεις να εκπροσωπήσει την Ελλάδα στην Eurovision;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/48151/vres-thn-amerikanikh-polh-apo-th-fwtografia-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Βρες την αμερικανική πόλη από τη φωτογραφία</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/SAN-FRANCISCO-34.jpg" alt="Βρες την αμερικανική πόλη από τη φωτογραφία" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/47946/deite-thn-eikona-kai-elegkste-thn-parathrhtikothta-sas-apantwntas-stis-erwthseis-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Δείτε την εικόνα και ελέγξτε την παρατηρητικότητά σας απαντώντας στις ερωτήσεις</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/zagadka-main.jpg" alt="Δείτε την εικόνα και ελέγξτε την παρατηρητικότητά σας απαντώντας στις ερωτήσεις" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/life">Life</a></span>
<div class="dropdown_fullwidth mgmenu_tabs">
<div class="mgmenu_width">
<ul class="mgmenu_tabs_nav">
<li><a href="/life" rel="section2_0" class="current">ΤΕΛΕΥΤΑΙΑ θΕΜΑΤΑ</a></li>
<li><a href="/food" rel="section2_1">FOOD</a></li>
<li><a href="/body-mind" rel="section2_2">BODY+MIND</a></li>
<li><a href="/spiti" rel="section2_3">ΣΠΙΤΙ</a></li>
<li><a href="/travel" rel="section2_4">ΤΑΞΙΔΙΑ</a></li>
<li><a href="/style" rel="section2_5">STYLE</a></li>
<li><a href="/family" rel="section2_6">FAMILY</a></li>
<li><a href="/sxeseis" rel="section2_7">ΣΧΕΣΕΙΣ</a></li>
<li><a href="/zodia" rel="section2_8">ΖΩΔΙΑ</a></li>
</ul>
<div class="mgmenu_tabs_panels">
<div id="section2_0" class="menuPosts">
<a href="/life/48902/h-griph-protimaei-toys-antres" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η γρίπη προτιμάει τους άντρες!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/man-flu.jpg" alt="Η γρίπη προτιμάει τους άντρες!" border="0" class="cropimg lazyload">
</a>
<a href="/life/48661/to-test-ths-paralias-poy-leei-polla-gia-thn-proswpikothta-sas-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Το τεστ της παραλίας... που λέει πολλά για την προσωπικότητά σας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paralia3.jpg" alt="Το τεστ της παραλίας... που λέει πολλά για την προσωπικότητά σας" border="0" class="cropimg lazyload">
</a>
<a href="/life/48821/ypallhloi-ksenodoxeiwn-apokalyptoyn-ta-vromika-mystika-toys" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Υπάλληλοι ξενοδοχείων αποκαλύπτουν τα... βρόμικα μυστικά τους</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/hotel-room.jpg" alt="Υπάλληλοι ξενοδοχείων αποκαλύπτουν τα... βρόμικα μυστικά τους" border="0" class="cropimg lazyload">
</a>
<a href="/life/48539/10-faghta-poy-tha-sas-steiloyn-sthn-toyaleta" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">10 φαγητά που θα σας στείλουν στην τουαλέτα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/eatbaby4.jpg" alt="10 φαγητά που θα σας στείλουν στην τουαλέτα" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_1" class="menuPosts mgmenu_tabs_hide">
<a href="/life/48813/o-gumnos-sef-moirazetai-tis-syntages-toy-kai-pali-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">O γuμνός σεφ μοιράζεται τις συνταγές του και πάλι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/naked-chef-23.jpg" alt="O γuμνός σεφ μοιράζεται τις συνταγές του και πάλι" border="0" class="cropimg lazyload">
</a>
<a href="/life/48717/oktw-apo-ta-pio-paraksena-rofhmata-kafe-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Οκτώ από τα πιο παράξενα ροφήματα καφέ</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/59.jpg" alt="Οκτώ από τα πιο παράξενα ροφήματα καφέ" border="0" class="cropimg lazyload">
</a>
<a href="/life/48544/ellhniko-tsizkeik-me-glyko-kydwni-kai-giaoyrti" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ελληνικό τσιζκέικ με γλυκό κυδώνι και γιαούρτι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/cheesecakekidoni.jpg" alt="Ελληνικό τσιζκέικ με γλυκό κυδώνι και γιαούρτι" border="0" class="cropimg lazyload">
</a>
<a href="/life/48234/to-koykoynari-o-kshros-karpos-twn-eortwn-kai-oxi-mono" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το κουκουνάρι, ο ξηρός καρπός των εορτών και όχι μόνο...</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/koukounari.jpg" alt="Το κουκουνάρι, ο ξηρός καρπός των εορτών και όχι μόνο..." border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_2" class="menuPosts mgmenu_tabs_hide">
<a href="/life/48536/mono-to-1-twn-gynaikwn-den-kanoyn-ayta-ta-lathh-me-to-derma-toys" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Μόνο το 1% των γυναικών δεν κάνουν αυτά τα λάθη με το δέρμα τους</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/facecare2.jpg" alt="Μόνο το 1% των γυναικών δεν κάνουν αυτά τα λάθη με το δέρμα τους" border="0" class="cropimg lazyload">
</a>
<a href="/life/48535/oi-diaites-ths-neas-xronias-dialekste-poia-sas-tairiazei" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Οι δίαιτες της νέας χρονιάς: Διαλέξτε ποια σας ταιριάζει</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dietwight.jpg" alt="Οι δίαιτες της νέας χρονιάς: Διαλέξτε ποια σας ταιριάζει" border="0" class="cropimg lazyload">
</a>
<a href="/life/48340/10-spanies-nosoi-kai-pathhseis-poy-den-gnwrizate-pws-yparxoyn-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">10 σπάνιες νόσοι και παθήσεις που δεν γνωρίζατε πως υπάρχουν</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/asthenies-32.jpg" alt="10 σπάνιες νόσοι και παθήσεις που δεν γνωρίζατε πως υπάρχουν" border="0" class="cropimg lazyload">
</a>
<a href="/life/48634/pagkosmio-sok-gia-ton-io-zika-kindynos-gia-egkyoys-kai-emvrya-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Παγκόσμιο σοκ για τον ιό Ζίκα - Κίνδυνος για εγκύους και έμβρυα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zika.jpg" alt="Παγκόσμιο σοκ για τον ιό Ζίκα - Κίνδυνος για εγκύους και έμβρυα" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_3" class="menuPosts mgmenu_tabs_hide">
<a href="/life/48139/ksestoliste-shmera-idees-kai-symvoyles" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ξεστολίστε σήμερα: Ιδέες και συμβουλές</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/storagexmas.jpg" alt="Ξεστολίστε σήμερα: Ιδέες και συμβουλές" border="0" class="cropimg lazyload">
</a>
<a href="/life/48141/ayto-to-krevati-ftiaxthke-gia-na-koimasai-mazi-me-to-katoikidio-soy-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Αυτό το κρεβάτι φτιάχτηκε για να κοιμάσαι μαζί με το κατοικίδιό σου!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/pet-bed-inside-mattress-colchao-inteligente-postural-19.jpg" alt="Αυτό το κρεβάτι φτιάχτηκε για να κοιμάσαι μαζί με το κατοικίδιό σου!" border="0" class="cropimg lazyload">
</a>
<a href="/life/48288/to-netflix-diathesimo-pleon-kai-sthn-ellada" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το Netflix διαθέσιμο πλέον και στην Ελλάδα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/netflix.jpg" alt="Το Netflix διαθέσιμο πλέον και στην Ελλάδα" border="0" class="cropimg lazyload">
</a>
<a href="/life/47943/10-xrhseis-ths-ladokollas-poy-den-mporoysate-na-fantasteite" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">10 χρήσεις της λαδόκολλας που δεν μπορούσατε να φανταστείτε</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/ladokolla-12.jpg" alt="10 χρήσεις της λαδόκολλας που δεν μπορούσατε να φανταστείτε" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_4" class="menuPosts mgmenu_tabs_hide">
<a href="/life/48425/skyacht-one-to-aeroskafos-thalamhgos-gia-kroisoys-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Skyacht One: Το αεροσκάφος-θαλαμηγός για κροίσους</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/skyacht-one-1.jpg" alt="Skyacht One: Το αεροσκάφος-θαλαμηγός για κροίσους" border="0" class="cropimg lazyload">
</a>
<a href="/life/48412/to-megalh-vretania-sth-lista-me-ta-6-pio-kompsa-ksenodoxeia-toy-kosmoy-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Το «Μεγάλη Βρετανία» στη λίστα με τα 6 πιο κομψά ξενοδοχεία του κόσμου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/hotels_6.jpg" alt="Το «Μεγάλη Βρετανία» στη λίστα με τα 6 πιο κομψά ξενοδοχεία του κόσμου" border="0" class="cropimg lazyload">
</a>
<a href="/life/48219/h-telegraph-mas-leei-poy-na-pame-gia-faghto-sthn-ellada-pics-list" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-th-list"></span></div>
<div class="menuItem_title">Η Telegraph μας λέει πού να πάμε για φαγητό στην Ελλάδα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tel_food_main.jpg" alt="Η Telegraph μας λέει πού να πάμε για φαγητό στην Ελλάδα" border="0" class="cropimg lazyload">
</a>
<a href="/life/48404/thessalonikh-stoys-52-koryfaioys-taksidiwtikoys-proorismoys-toy-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Θεσσαλονίκη: Στους 52 κορυφαίους ταξιδιωτικούς προορισμούς του 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/thessaloniki-pyrgos-23.jpg" alt="Θεσσαλονίκη: Στους 52 κορυφαίους ταξιδιωτικούς προορισμούς του 2016" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_5" class="menuPosts mgmenu_tabs_hide">
<a href="/life/48135/to-makry-malli-einai-vareto-ayto-einai-to-koyrema-ths-xronias-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Το μακρύ μαλλί είναι βαρετό: Αυτό είναι το κούρεμα της χρονιάς</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/lob_malli_0.jpg" alt="Το μακρύ μαλλί είναι βαρετό: Αυτό είναι το κούρεμα της χρονιάς" border="0" class="cropimg lazyload">
</a>
<a href="/life/47921/oi-allages-ths-modas-se-antres-kai-gynaikes-ta-teleytaia-100-xronia-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Οι αλλαγές της μόδας σε άντρες και γυναίκες τα τελευταία 100 χρόνια</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/moda100xronia.jpg" alt="Οι αλλαγές της μόδας σε άντρες και γυναίκες τα τελευταία 100 χρόνια" border="0" class="cropimg lazyload">
</a>
<a href="/life/40345/ellhnas-mparmperhs-trelainei-toys-germanoys" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Έλληνας μπαρμπέρης τρελαίνει τους Γερμανούς</div>
<img data-src="http://cdn.e-daily.gr/repository/places/velios.jpg" alt="Έλληνας μπαρμπέρης τρελαίνει τους Γερμανούς" border="0" class="cropimg lazyload">
</a>
<a href="/life/41076/to-troxoforo-palati-twn-3-ekat-eyrw-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Το τροχοφόρο παλάτι των 3 εκατ. ευρώ!</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/troxoforopalati.jpg" alt="Το τροχοφόρο παλάτι των 3 εκατ. ευρώ!" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_6" class="menuPosts mgmenu_tabs_hide">
<a href="/life/48397/ereyna-ta-paidia-poy-pernoyn-perissotero-xrono-me-ton-patera-toys-exoyn-ypshlotero-iq" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Έρευνα: Tα παιδιά που περνούν περισσότερο χρόνο με τον πατέρα τους έχουν υψηλότερο IQ</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/father_daughter.jpg" alt="Έρευνα: Tα παιδιά που περνούν περισσότερο χρόνο με τον πατέρα τους έχουν υψηλότερο IQ" border="0" class="cropimg lazyload">
</a>
<a href="/life/47467/to-pio-glyko-kswtiko-kai-o-theotrelos-mpampas-toy-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Το πιο γλυκό ξωτικό και ο θεότρελος μπαμπάς του</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/elfxmas.jpg" alt="Το πιο γλυκό ξωτικό και ο θεότρελος μπαμπάς του" border="0" class="cropimg lazyload">
</a>
<a href="/life/47045/ekanan-dwro-stis-treis-kores-toys-enan-aderfo-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Έκαναν δώρο στις τρεις κόρες τους έναν... αδερφό</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/sistersreaction2.jpg" alt="Έκαναν δώρο στις τρεις κόρες τους έναν... αδερφό" border="0" class="cropimg lazyload">
</a>
<a href="/life/47386/riceballbaby-zoylhkse-to-mwro-soy-ki-esy-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">#RiceBallBaby: Ζούληξε το μωρό σου κι εσύ</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/zouligmenamorakia5.jpg" alt="#RiceBallBaby: Ζούληξε το μωρό σου κι εσύ" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_7" class="menuPosts mgmenu_tabs_hide">
<a href="/life/48810/pws-tha-glitwsei-mia-gynaika-ton-gamo-ths" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Πως θα γλιτώσει μια γυναίκα τον γάμο της</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/apistia1.jpg" alt="Πως θα γλιτώσει μια γυναίκα τον γάμο της" border="0" class="cropimg lazyload">
</a>
<a href="/life/47809/oi-5-fraseis-poy-den-prepei-na-peis-sth-nea-soy-doyleia-gia-na-mhn-deixneis-apeiros" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Οι 5 φράσεις που δεν πρέπει να πεις στη νέα σου δουλειά, για να μην δείχνεις άπειρος</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/kopela_douleia1.jpg" alt="Οι 5 φράσεις που δεν πρέπει να πεις στη νέα σου δουλειά, για να μην δείχνεις άπειρος" border="0" class="cropimg lazyload">
</a>
<a href="/life/38687/pws-na-ekneyrisete-thn-kopela-sas-mesa-se-66-deyterolepta-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙς</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Πώς να εκνευρίσετε την κοπέλα σας μέσα σε 66 δευτερόλεπτα</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/ekneurizeiskopela.jpg" alt="Πώς να εκνευρίσετε την κοπέλα σας μέσα σε 66 δευτερόλεπτα" border="0" class="cropimg lazyload">
</a>
<a href="/life/47681/giati-ta-zeygaria-tsakwnontai-ta-xristoygenna" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιατί τα ζευγάρια τσακώνονται τα Χριστούγεννα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/holidayfight.jpg" alt="Γιατί τα ζευγάρια τσακώνονται τα Χριστούγεννα;" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_8" class="menuPosts mgmenu_tabs_hide">
<a href="/life/48346/zwdia-evdomadas-11-17-ianoyarioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ζώδια Εβδομάδας: 11-17 Ιανουαρίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ZODIA43.jpg" alt="Ζώδια Εβδομάδας: 11-17 Ιανουαρίου" border="0" class="cropimg lazyload">
</a>
<a href="/viral/48409/zwdia-pws-tha-peraseis-paraskeyh-kai-savvatokyriako" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ζώδια: Πώς θα περάσεις Παρασκευή και Σαββατοκύριακο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Zodiac-Signs-Silhouettes.jpg" alt="Ζώδια: Πώς θα περάσεις Παρασκευή και Σαββατοκύριακο" border="0" class="cropimg lazyload">
</a>
<a href="/life/48076/ethsies-astrologikes-provlepseis-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ετήσιες αστρολογικές προβλέψεις 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/astrology2016.jpg" alt="Ετήσιες αστρολογικές προβλέψεις 2016" border="0" class="cropimg lazyload">
</a>
<a href="/life/47866/ti-allages-prepei-na-kanoyn-ta-zwdia-to-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τι αλλαγές πρέπει να κάνουν τα ζώδια το 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/zodia23.jpg" alt="Τι αλλαγές πρέπει να κάνουν τα ζώδια το 2016" border="0" class="cropimg lazyload">
</a>
</div>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/entertainment">Ψυχαγωγία</a></span>
<div class="dropdown_fullwidth mgmenu_tabs">
<div class="mgmenu_width">
<ul class="mgmenu_tabs_nav">
<li><a href="/agenda" rel="section5_1" class="current">ATZENTA ΣΗΜΕΡΑ</a></li>
<li><a href="/music" rel="section5_2">ΜΟΥΣΙΚΗ</a></li>
<li><a href="/cinema" rel="section5_3">ΣΙΝΕΜΑ</a></li>
<li><a href="/theater" rel="section5_4">ΘΕΑΤΡΟ+ΧΟΡΟΣ</a></li>
<li><a href="/arts" rel="section5_5">ΕΙΚΑΣΤΙΚΑ</a></li>
<li><a href="/city" rel="section5_6">ΠΟΛΗ</a></li>
<li><a href="/nightlife" rel="section5_7">ΝΑΪΤ ΛΑΪΦ</a></li>
</ul>
<div class="mgmenu_tabs_panels">
<div id="section5_1" class="menuPosts">
<a href="/entertainment/48875/oi-tainies-ths-evdomadas-apo-ton-notia-sthn-islandikh-ypaithro-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Οι ταινίες της εβδομάδας: Από τον «Νοτιά» στην ισλανδική ύπαιθρο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/NOTIAS.jpg" alt="Οι ταινίες της εβδομάδας: Από τον «Νοτιά» στην ισλανδική ύπαιθρο" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48798/makmpeth-toy-oyilliam-saiksphr-apo-to-kthve-sto-theatro-etaireias-makedonikwn-spoydwn" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Μάκμπεθ, του Ουίλλιαμ Σαίξπηρ από το ΚΘΒΕ στο Θέατρο Εταιρείας Μακεδονικών Σπουδών</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/macbeth.jpg" alt="Μάκμπεθ, του Ουίλλιαμ Σαίξπηρ από το ΚΘΒΕ στο Θέατρο Εταιρείας Μακεδονικών Σπουδών" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48796/charlie-winston-live" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Charlie Winston Live</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/charliewinston.jpg" alt="Charlie Winston Live" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48795/straight-ahead-jazz-apo-ton-mhna-aleksiadh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Straight Ahead Jazz από τον Μηνά Αλεξιάδη </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/straightaheadjaz.jpg" alt="Straight Ahead Jazz από τον Μηνά Αλεξιάδη " border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_2" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/48791/h-maria-farantoyrh-kai-o-giwrgos-ntalaras-synantoyn-th-symfwnikh-orxhstra" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η Μαρία Φαραντούρη και ο Γιώργος Νταλάρας συναντούν τη Συμφωνική Ορχήστρα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/farantourintalaras.jpg" alt="Η Μαρία Φαραντούρη και ο Γιώργος Νταλάρας συναντούν τη Συμφωνική Ορχήστρα" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48790/giwrgos-ntalaras-miltos-pasxalidhs-sth-skhnh-toy-passport-kerameikos" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιώργος Νταλάρας - Μίλτος Πασχαλίδης στη σκηνή του Passport Κεραμεικός.</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ntalaraspasxalidis.jpg" alt="Γιώργος Νταλάρας - Μίλτος Πασχαλίδης στη σκηνή του Passport Κεραμεικός." border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48788/o-oresths-ntantos-epistrefei-meta-apo-xronia-ston-stayro-toy-notoy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">O Ορέστης Ντάντος επιστρέφει μετά από χρόνια στον Σταυρό του Νότου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/orestisntantos.jpg" alt="O Ορέστης Ντάντος επιστρέφει μετά από χρόνια στον Σταυρό του Νότου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48787/sleep-party-people-live" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Sleep Party People Live</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sleeppartypeople.jpg" alt="Sleep Party People Live" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_3" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/48540/to-festival-outview-tha-einai-paron-sto-hellas-filmbox-berlin" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το φεστιβάλ Outview θα είναι παρόν στο Hellas Filmbox Berlin</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/OUTVIEWfilmbox.jpg" alt="Το φεστιβάλ Outview θα είναι παρόν στο Hellas Filmbox Berlin" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48307/tainies-ths-evdomadas-highlight-stoys-mishtoys-oktw-toy-tarantino-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Ταινίες της εβδομάδας: Highlight στους «Μισητούς Οκτώ» του Tarantino</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/hateful8.jpg" alt="Ταινίες της εβδομάδας: Highlight στους «Μισητούς Οκτώ» του Tarantino" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48229/oi-pente-megales-premieres-toy-ianoyarioy-video-list" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-th-list"></span></div>
<div class="menuItem_title">Οι πέντε μεγάλες πρεμιέρες του Ιανουαρίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/5_movies.jpeg" alt="Οι πέντε μεγάλες πρεμιέρες του Ιανουαρίου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/47388/tainies-ths-evdomadas-papakaliaths-klp-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Ταινίες της εβδομάδας: Παπακαλιάτης κλπ</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/enasalloskosmos.jpg" alt="Ταινίες της εβδομάδας: Παπακαλιάτης κλπ" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_4" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/48789/o-fovos-toy-stefan-tsvaix-sto-neo-theatro-vasilakoy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ο Φόβος, του Στέφαν Τσβάιχ στο Νέο Θέατρο Βασιλάκου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ofovos.jpg" alt="Ο Φόβος, του Στέφαν Τσβάιχ στο Νέο Θέατρο Βασιλάκου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48779/sth-siwph-toy-dhmhtrh-lentzoy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Στη σιωπή, του Δημήτρη Λέντζου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/stisiopi.jpg" alt="Στη σιωπή, του Δημήτρη Λέντζου" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48776/o-pateras-toy-aygoystoy-strintmpergk-se-mia-prwtognwrh-ekdoxh-apo-ton-grhgorh-xatzakh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ο Πατέρας του Αύγουστου Στρίντμπεργκ σε μία πρωτόγνωρη εκδοχή από τον Γρηγόρη Χατζάκη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paterasstrindberg.jpg" alt="Ο Πατέρας του Αύγουστου Στρίντμπεργκ σε μία πρωτόγνωρη εκδοχή από τον Γρηγόρη Χατζάκη" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/44307/ta-pshla-voyna-toy-zaxaria-papantwnioy-sto-theatro-akropol-se-skhn-vasilh-mayrogewrgioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΙΔΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα Ψηλά Βουνά, του Ζαχαρία Παπαντωνίου στο Θέατρο Ακροπόλ σε σκην. Βασίλη Μαυρογεωργίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/tapsilavouna15.jpg" alt="Τα Ψηλά Βουνά, του Ζαχαρία Παπαντωνίου  στο Θέατρο Ακροπόλ σε σκην. Βασίλη Μαυρογεωργίου" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_5" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/48794/an-h-zwh-htan-klwsth-ekthesh-ths-giwtas-andriakaina" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Αν η ζωή ήταν κλωστή: έκθεση της Γιώτας Ανδριάκαινα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/anizoiitanklosti.jpg" alt="Αν η ζωή ήταν κλωστή: έκθεση της Γιώτας Ανδριάκαινα" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48772/energy-athens-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Energy Athens 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/energyathens.jpg" alt="Energy Athens 2016" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48691/to-la-town-ths-cao-fei-sthn-state-of-concept-gallery" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το La Town της Cao Fei στην State of Concept Gallery </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/latown.jpg" alt="Το La Town της Cao Fei  στην State of Concept Gallery " border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48547/das-schlo223-ekthesh-toy-mpamph-panagiannh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Das Schlo&#223;: έκθεση του Μπάμπη Παναγιάννη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dasschloss.jpg" alt="Das Schlo&#223;: έκθεση του Μπάμπη Παναγιάννη" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_6" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/48792/6o-athens-video-dance-project" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΧΟΡΟς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">6ο Athens Video Dance Project</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/6oathensvideodance.jpg" alt="6ο Athens Video Dance Project" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48768/through-the-hoop" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΧΟΡΟς</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Through the hoop</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/hoopdanceflow.jpg" alt="Through the hoop" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/48663/not-another-networking-event-no-computers-allowed-8211-its-party-taim" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Not Another Networking Event: No Computers Allowed &#8211; ΙΤΣ ΠΑΡΤΥ ΤΑΪΜ </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/notanothernetworking.jpg" alt="Not Another Networking Event: No Computers Allowed &#8211; ΙΤΣ ΠΑΡΤΥ ΤΑΪΜ " border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/47569/xristoygenna-gia-ta-paidia-parea-me-to-1o-festival-paramythioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Χριστούγεννα για τα παιδιά παρέα με το 1ο Φεστιβάλ Παραμυθιού</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/festparamyth.jpg" alt="Χριστούγεννα για τα παιδιά παρέα με το 1ο Φεστιβάλ Παραμυθιού" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_7" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/48126/heretic-presents-the-kids-are-alright-vol-4-east-meets-west" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΝΑΙΤ ΛΑΙΦ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Heretic presents: THE KIDS ARE ALRIGHT vol. 4: EAST meets WEST</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/redaxes.jpg" alt="Heretic presents: THE KIDS ARE ALRIGHT vol. 4: EAST meets WEST" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/39221/giannhs-parios-melina-aslanidoy-kai-stavento-ton-ianoyario-sthn-iera-odo" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιάννης Πάριος, Μελίνα Ασλανίδου και STAVΕΝΤΟ τον Ιανουάριο στην Ιερά Οδό!</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/giannisparios1.jpg" alt="Γιάννης Πάριος, Μελίνα Ασλανίδου και STAVΕΝΤΟ τον Ιανουάριο στην Ιερά Οδό!" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/47699/blend-minotaur-minotaur-productions-present-deborah-de-luca-nakadia-live" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΞΕΝΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Blend + Minotaur-Minotaur Productions Present Deborah de Luca + Nakadia Live</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/deborahdeluca.jpg" alt="Blend + Minotaur-Minotaur Productions Present Deborah de Luca + Nakadia Live" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/47696/the-day-after-with-delta-funktionen-nl" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΞΕΝΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">the DAY AFTER with Delta Funktionen (NL)</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/deltafunctionen.jpg" alt="the DAY AFTER with Delta Funktionen (NL)" border="0" class="cropimg lazyload">
</a>
</div>
</div>
</div>
</div>
</li> 
<li class="dropdown_custom"><span class="mgmenu_drop"><a href="/weather" id="weatherMenu"></a></span>
<div class="dropdown_container mgmenu_smallwidth" id="weatherMenuPad"></div>
</li>
</ul>
<a href="/agenda" class="mgmenu_left nextwebchannels"><img src="/_img/eagendagr_logo.png" height="20"/></a>
<a href="http://www.e-radio.gr" target="_blank" class="mgmenu_left nextwebchannels"><img src="/_img/eradiogr_logo.png" height="20"/></a>
<div class="right_item">
<div class="right_item" id="tabSearch" style="padding:12px 10px 14px 8px;"><div class="icon-search"></div></div>
<div class="right_item noactive" style="padding:0">
<a class="icon-twitter-bird right_item twitter" href="http://www.twitter.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px;"></a>
<a class="icon-instagram right_item instagram" href="http://www.instagram.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px;"></a>
<a class="icon-facebook right_item facebook" href="http://www.facebook.com/edailygr" target="_blank" style="padding:12px 0 14px 10px;"></a>
</div>
<div class="right_item noactive feedIcons showOnSmallMenu">
<a href="/retro"><img alt="Retro" src="/_img/feeds/retro.png" border="0"/></a>
<a href="/omg"><img alt="OMG" src="/_img/feeds/omg.png" border="0"/></a>
<a href="/lol"><img alt="LOL" src="/_img/feeds/lol.png" border="0"/></a>
<a href="/juicy"><img alt="Juicy" src="/_img/feeds/juicy.png" border="0"/></a>
<a href="/alist"><img alt="A-List" src="/_img/feeds/a-list.png" border="0"/></a>
<a href="/cute"><img alt="Cute" src="/_img/feeds/cute.png" border="0"/></a>
<a href="/geeky"><img alt="Geeky" src="/_img/feeds/geeky.png" border="0"/></a>
</div>
</div>
</div>
<div class="mgmenu_fullwidth"></div>
</div>
<div id="topHeaderMobile" style="text-align:center;">
<div class="ma-infinitypush-button"><span></span><span></span><span></span><span></span></div>
<a href="/"><img src="/_img/mobilelogo.png" width="136" height="48" alt="E-Daily" border="0"/></a>
<div class="right_item" id="tabSearchMobile" style="padding:12px; font-size: 18px;"><div class="icon-search"></div></div>
</div>
<div id="tabSearchMenu">
<form name="bigsearchboxform" id="bigsearchboxform" class="mainArea" autocomplete="off" method="post" action="/search/" onsubmit="javascript:submitsearch()">
<input type="search" name="q" id="q" placeholder="Αναζητήστε 48106 θέματα, 969 ραδιόφωνα" value="" alt="Αναζητήστε 48106 θέματα, 969 ραδιόφωνα"/>
<a name="bigsearchbox_Cancel" id="bigsearchbox_Cancel" href="javascript:hidesearch()" class="icon-cancel-circle"></a>
</form>
</div>
<div class="navibar">
</div>
<div id="topHeader3">
<center>
<script type="text/javascript">
( function() {
    var info = getTagInfo();
    var alias = 'edhomebillboard_display';
        alias = alias !== '' ? 'alias=' + ( alias.split("_") ? alias.split("_")[ 0 ] : alias ) + '_' + info.alias : '';
    document.write('<scr'+'ipt language="javascript1.1" src="http://' + info.domain + '.adtech.de/addyn/3.0/1370.1/4713596/0/-1/ADTECH;' + alias + ';loc=100;target=_blank;key=;grp=' + info.adgroupid + ';misc=' + new Date().getTime() + '"></scri'+'pt>');
})();</script>
</center>
</div>
<div class="mainArea">
<div class="mainAreaA">
<h1 class="postTitle">Privacy Policy</h1>
<div class="postBody">
<p>Welcome to E-Daily. This Privacy Policy is effective for E-Daily.gr, E-Daily Mobile Apps (&ldquo;E-Daily&rdquo;, &ldquo;we&rdquo;, &ldquo;us&rdquo; and/or &ldquo;our&rdquo;) and is incorporated by reference into the E-Daily Terms of Service and End User License Agreement. <br/>
The purpose of this Privacy Policy is to inform users of the E-Daily Service (as defined below) about the information that we collect through the E-Daily Service, how we use that information and the ways users can control how that information is used or shared. The entire policy can be found below. For information about users&rsquo; obligations and use of the E-Daily Service, please see our Terms of Service and End User License Agreement. </p>
<p> We will continue to evaluate this Privacy Policy and update it accordingly when there is a change to our business practices, our users&rsquo; needs, and to comply with regulatory and legal requirements. Please check this page periodically for updates. If we make any material changes to this Privacy Policy, we will post the updated Privacy Policy here and notify you by email or by means of a notice on the Site (as defined below) and/or the Applications (as defined below). </p>
<p><strong>The types of information we collect</strong><br/>
When you interact with the E-Daily.gr, we may collect Personal Information (as defined below) and other information from you as described below. <br/>
&ldquo;Personal Information&rdquo; means information that alone or when in connection with other information may be used to readily identify, contact, or locate you, such as: name, address, email address or phone number. We do not consider personally identifiable information to include information that has been anonymized so that it does not allow a third party to easily identify a specific individual. </p>
<p><strong>Information you give us </strong><br/>
To enable you to enjoy certain features of the E-Daily Service, we collect certain types of information, including Personal Information, during your interactions with the E-Daily Service. By using the E-Daily Service, you are authorizing us to gather, parse, and retain data related to the provision of the E-Daily Service. </p>
<p> For example, we collect information, including Personal Information, when you: </p>
<ul type="disc">
<li>Register to use our E-Daily Service;</li>
<li>Login with social networking credentials; </li>
<li>Participate in polls, contests, surveys or other features of the E-Daily Service, or respond to offers or advertisements on the E-Daily Service; </li>
<li>Communicate with us; and</li>
<li>Sign up to receive email newsletters.</li>
</ul>
<p>When you register to use the E-Daily Service, we ask you to provide certain information, which includes your name, email address, birth year, gender, zip code and/or other geographic information, as well as a password for your account. By voluntarily providing us with Personal Information or other information, you are consenting to our use of it in accordance with this Privacy Policy. If you provide any Personal Information or other information, you acknowledge and agree that such Personal Information may be transferred from your current location to our offices and servers and the authorized third parties. </p>
<p><strong>Automatic Data Collection</strong><br/>
<em>Listening and Usage Activity</em>: We keep track of your listening and usage activity. We collect information about stations, favorites, artists, channels, conversations and tracks you have listened to or in which you have expressed an interest. <br/>
<em>Social connect information</em>: When you choose to connect your social media account to your account profile, we collect Personal Information from that social media website. For example, when you connect your Facebook account, we may collect the Personal Information you have made publicly available in Facebook, such as your name, profile picture, cover photo, username, gender, friend networks, age range, locale, friend list, and any other information you have made public. We may also obtain other non-public information from the social network website with your permission. </p>
<p><em>Information about your computer or device</em>: We may also collect information about the computer, mobile or other devices you use to access the E-Daily Service. For example, our servers receive and record information about your computer and browser, including potentially your IP address, device ID, operating system, browser type, and other software or hardware information. If you access the E-Daily Service from a mobile or other device, unless you register, we may assign a unique device identifier for that device. </p>
<p><em>Location-based information</em>: We do not collect your zip code, postal code, city or country unless you choose to provide this information to us at registration. Your general geo-location is collected at regular intervals if you use the E-Daily Service on your mobile device or through the Site. We collect this information to provide content and services that are customized for your geographic area. </p>
<p><em>Tracking technology</em>: We use what are commonly called &quot;cookies&quot; on our Site. A cookie is a piece of information that the computer that hosts our Site gives to your browser when you access the Site. These cookies help us improve the E-Daily Service, provide additional functionality to the Site and help us analyze Site usage more accurately. For instance, our Site may set a cookie on your browser that allows you to access the Site without needing to remember and then enter a password more than once during a visit to the Site. We may also be able to capture other data such as searches conducted and results, date, time, and connection speed. In all cases in which we use cookies, we will not collect Personal Information except with your permission. On most web browsers, you will find a &ldquo;help&rdquo; section on the toolbar. Please refer to this section for information on how to receive notification when you are receiving a new cookie and how to turn cookies off. We recommend that you leave cookies turned on because they allow you to take advantage of some of the Site&rsquo;s features. Third parties, including advertisers and third-party advertising companies, whose products or services are accessible or advertised via the E-Daily Service may also use cookies, and we advise you to check their privacy policies for information about their cookies and other privacy practices. </p>
<p><strong>Our use of the information we collect</strong><br/>
We use the information, including Personal Information, that we collect for the following purposes: </p>
<ul type="disc">
<li>To personalize and customize the advertising and the content you see.</li>
<li>To improve and enhance the user experience. We use tracking information to determine how well each page, station and/or channel performs overall based on aggregate user demographics and traffic patterns to those pages, stations and channels. This helps us build a better service for you.</li>
<li>To fulfill your requests for certain products and services, such as sending out electronic newsletters and enabling you to participate in polls, contests, and boards.</li>
<li>To send you information about topics or content that we think will be of interest to you based on your preferences, habits or geographic location.</li>
<li>To send you information about the latest developments and features on our E-Daily Service.</li>
<li>To provide technical support.</li>
</ul>
<p>We may anonymize and aggregate data collected through the E-Daily Service and use it for any purpose. </p>
<p><strong>How the information we collect is shared</strong><br/>
We are not in the business of selling, renting or sharing Personal Information about you with other people or nonaffiliated companies for their direct marketing purposes. We consider this information to be a vital part of our relationship with you. There are, however, certain circumstances in which we may share your information, including Personal Information, with certain third parties without further notice to you, as set forth below: </p>
<ul type="disc">
<li>Information you allow us to share when you register with us, or through a subsequent affirmative election.</li>
<li>Information used when we hire or partner with third parties to provide business-related services on our behalf, such as mailing information, maintaining databases, processing payments, sweepstakes management and prize fulfillment, data processing, analytics, personalization and customization of advertising, customer/support services and other products or services that we choose to make available to our registered users. When we employ another company to perform a function of this nature, we only provide them with the information that they need to perform their specific function.</li>
<li>We may share, transfer or sell your information in connection with a merger, acquisition, financing due diligence, reorganization, bankruptcy, receivership, sale of company assets, or transition of service to another provider. We cannot control how such entities may use or disclose such information.</li>
<li>We may share information with our subsidiaries and affiliates for purposes consistent with this Privacy Policy.</li>
<li>The E-Daily Service may allow you to share information, including Personal Information, with social networking websites, such as Facebook. We do not share your Personal Information with them unless you direct the E-Daily Service to share it. Their use of the information will be governed by their privacy policies, and you may be able to modify your privacy setting on their websites.</li>
<li>We may share your information if required to do so by law or in the good faith belief that such action is necessary in order to (i) comply with a legal obligation, (ii) protect or defend our legal rights or property as well as those of our business partners, employees, agents and contractors (including enforcement of our agreements); (iii) act in urgent circumstances to protect the personal safety of users or the public; (iv) protect against fraud or risk management purposes; or (v) protect against legal liability.</li>
</ul>
<p>When you register and create a profile through the E-Daily Service, other users of the E-Daily Service may be able to see your profile information, including your screen name, profile photo and any content that you post to the E-Daily Service. We recommend that you guard your anonymity and sensitive information. We are not responsible for the privacy practices of the other users who will view and use the information you post. </p>
<p><strong>Non-Identifiable or Aggregated Data</strong><br/>
We also receive and store certain non-personally identifiable information. Such information, which is collected passively using various technologies, cannot presently be used to specifically identify you. We may store such information itself or such information may be included in databases owned and maintained by our affiliates, agents or service providers. We may use such information and pool it with other information to track, for example, the total number of visitors to the E-Daily Service, the number of visitors to each page of our Site, the domain names of our visitors' Internet service providers, and how our users use and interact with the E-Daily Service. Also, in an ongoing effort to better understand and serve the users of the E-Daily Service, we often conduct research on our customer demographics, interests and behavior based on the personal information and other information provided to us. This research may be compiled and analyzed on an aggregate basis. We may share this non-identifiable and aggregate data with our affiliates, agents and business partners, but this type of non-identifiable and aggregate information does not identify you personally. We may also disclose aggregated user statistics in order to describe our services to current and prospective business partners and to other third parties for other lawful purposes. <br/>
The use and disclosure of this non-personally identifiable information is not subject to restrictions under this Privacy Policy. </p>
<p><strong>Your Choices</strong><br/>
You can visit the Site without providing any personal information. If you choose not to provide any personal information, you may not be able to use certain features of our E-Daily Service. </p>
<p><strong>Exclusions</strong><br/>
This Privacy Policy does not apply to any personal data collected by E-Daily other than personal data collected through the E-Daily Service. This Privacy Policy shall not apply to any unsolicited information you provide to us through the E-Daily Service or through any other means. This includes, but is not limited to, information posted to any public areas of the E-Daily Service, such as bulletin boards (collectively, &ldquo;Public Areas&rdquo;), any ideas for new products or modifications to existing products, and other unsolicited submissions (collectively, &ldquo;Unsolicited Information&rdquo;). All Unsolicited Information shall be deemed to be non-confidential and we shall be free to reproduce, use, disclose, distribute and exploit such Unsolicited Information without limitation or attribution. </p>
<p><strong>How we protect your information</strong><br/>
We have taken reasonable steps to protect the information you provide via the E-Daily Service from loss, misuse, and unauthorized access, disclosure, alteration, or destruction. However, no Internet, email or other electronic transmission is ever fully secure or error free, so you should take special care in deciding what information you send to us in this way. We do not accept liability for unintentional disclosure. <br/>
By using the E-Daily Service or providing Personal Information to us, you agree that we may communicate with you electronically regarding security, privacy, and administrative issues relating to your use of the E-Daily Service. If we learn of a security system&rsquo;s breach, we may attempt to notify you electronically by posting a notice on the Site and/or Applications or sending an email to you. </p>
<p><strong>Consent to processing</strong><br/>
If you use the E-Daily Service outside of the European Union, you fully understand and unambiguously consent to the transfer of your information, including Personal Information, to, and the collection and processing of such information in Greece and other countries or territories of the European Union. The laws on holding such information in European Union may vary and be less stringent than laws of your state or country. </p>
<p><strong>Children</strong><br/>
We do not knowingly collect Personal Information from children under the age of 13. If you are under the age of 13, please do not submit any Personal Information through the E-Daily Service. We encourage parents and legal guardians to monitor their children&rsquo;s Internet usage and to help enforce our Privacy Policy by instructing their children never to provide Personal Information through the E-Daily Service without their permission. If you have reason to believe that a child under the age of 13 has provided Personal Information to us through our E-Daily, please contact us, and we will endeavor to delete that information from our databases. </p>
<p><strong>Links to Other Web Sites</strong><br/>
This Privacy Policy applies only to the E-Daily Service. The E-Daily Service may contain links to other web sites not operated or controlled by us (the &ldquo;Third Party Sites&rdquo;). The policies and procedures we described here do not apply to the Third Party Sites. The links from the E-Daily Service do not imply that we endorse or have reviewed the Third Party Sites. We suggest contacting those sites directly for information on their privacy policies. </p>
<p><strong>What are my privacy options and how can I change my settings?</strong><br/>
<em>To modify your registration information:</em> Once you have registered, you can change your registration information (including your email address and password). <br/>
<em>Opt out:</em> Each email communication we send you will contain instructions permitting you to &quot;opt-out&quot; of receiving future marketing communications. In addition, if at any time you wish not to receive any future communications or you wish to have your name deleted from our email mailing lists, please contact us as indicated below. Note however, that certain communications, such as push notifications, require you to change the settings on your device if at any time you wish not to receive any future communications. Note that on occasion we send service-related announcements, such as disruption notifications, and you may not be able to opt-out of these communications. </p>
<p><strong>English language version shall control</strong><br/>
The English language version of this Privacy Policy is the version that governs your use of the E-Daily Service and in the event of any conflict between the English language version and a translated version, the English language version will control.</p>
<p> To print out this Statement, you may visit www.e-daily.gr/privacy.asp</p>
</div>
</div>
<div class="mainArea2">
<center>
<script language="javascript">
<!--
if (window.adgroupid == undefined) {
                window.adgroupid = Math.round(Math.random() * 1000);
}
document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1370/5799491/0/170/ADTECH;loc=100;target=_blank;key=;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
//-->
</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1370/5799491/0/170/ADTECH;loc=300;key=" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1370/5799491/0/170/ADTECH;loc=300;key=" border="0" width="300" height="250"></a></noscript>
</center>
<br>
<script language="javascript">
$().ready(function() {
    $("#todayTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  updateHash: false
	});
/*	$("#tabs-hl").mCustomScrollbar({
		theme:"dark-thick"
	});
*/
	$("#tabs-recent").mCustomScrollbar({
		theme:"dark-thick"
	});
});

</script>
<div class="postsRedHeader">διαβάστε σήμερα</div>
<div id="todayTabs" class="tab-container">
<ul class="etabs">
<li class="tab"><a href="#tabs-hl">ΕΠΙΛΟΓΕΣ</a></li>
<li class="tab"><a href="#tabs-recent">ΠΡΟΣΦΑΤΑ</a></li>
</ul>
<div id="tabs-hl" class="postEntries">
<a href="/news/48908/isis-aphgagan-600-paidia-gia-na-ta-kanoyn-vomvistes-aytoktonias" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/paidia-isis-12.jpg" alt="ISIS: Απήγαγαν 600 παιδιά για να τα κάνουν βομβιστές αυτοκτονίας" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span><span class="when">19:06</span></div>
<h3>ISIS: Απήγαγαν 600 παιδιά για να τα κάνουν βομβιστές αυτοκτονίας</h3>
<h4>Τα «κουτάβια» του χαλιφάτου</h4>
</a>
<a href="/news/48907/h-fwfh-diagrafei-ton-grhgorako" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/grigorakos1.jpg" alt="Η Φώφη διαγράφει τον Γρηγοράκο" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">18:26</span></div>
<h3>Η Φώφη διαγράφει τον Γρηγοράκο</h3>
<h4>Μέσα στις επόμενες ώρες</h4>
</a>
<a href="/news/48904/synelhfthh-ypopshfios-voyleyths-toy-syriza-gia-dwrolhpsia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/500euro.jpg" alt="Συνελήφθη υποψήφιος βουλευτής του ΣΥΡΙΖΑ για δωροληψία" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span><span class="when">17:57</span></div>
<h3>Συνελήφθη υποψήφιος βουλευτής του ΣΥΡΙΖΑ για δωροληψία</h3>
<h4>Νε προσημειωμένα χαρτονομίσματα</h4>
</a>
<a href="/life/48902/h-griph-protimaei-toys-antres" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/man-flu.jpg" alt="Η γρίπη προτιμάει τους άντρες!" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span><span class="when">17:42</span></div>
<h3>Η γρίπη προτιμάει τους άντρες!</h3>
<h4>Oι γυναίκες μπορούν να διαχειριστούν καλύτερα τα συμπτώματα μιας γρίπης λόγω ορμονών</h4>
</a>
<a href="/news/48897/ntaiselmploym-egine-sklhrh-doyleia-gia-to-asfalistiko" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/geroun.jpg" alt="Ντάισελμπλουμ: Έγινε σκληρή δουλειά για το ασφαλιστικό" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span><span class="when">16:57</span></div>
<h3>Ντάισελμπλουμ: Έγινε σκληρή δουλειά για το ασφαλιστικό</h3>
<h4>«Η Ελλάδα αποδέχτηκε τη συμμετοχή του ΔΝΤ στο πρόγραμμα»</h4>
</a>
<a href="/viral/48894/anakoinwthhkan-oi-ypopshfiothtes-gia-ta-vraveia-oskar-2016" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/oscar.jpg" alt="Ανακοινώθηκαν οι υποψηφιότητες για τα βραβεία Όσκαρ 2016" border="0" class="lazyload">
</div>
<div><span class="category">ΣΙΝΕΜΑ</span><span class="when">16:17</span></div>
<h3>Ανακοινώθηκαν οι υποψηφιότητες για τα βραβεία Όσκαρ 2016</h3>
<h4>Διαβάστε την πλήρη λίστα των υποψηφίων</h4>
</a>
<a href="/news/48892/epifanhs-giatros-ekspepmatwse-se-proswpo-asthenoys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/Mount_Sinai.jpg" alt="Επιφανής γιατρός εκσπεpμάτωσε σε πρόσωπο ασθενούς" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span><span class="when">16:00</span></div>
<h3>Επιφανής γιατρός εκσπεpμάτωσε σε πρόσωπο ασθενούς</h3>
<h4>Η 22χρονη τον κατηγορεί για σeξουαλική επίθεση</h4>
</a>
<a href="/news/48890/me-apoklish-22-dis-eyrw-sta-esoda-ekleise-to-2015" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/xrimata-xartonomismata-euro.jpg" alt="Με απόκλιση 2,2 δισ. ευρώ στα έσοδα έκλεισε το 2015" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span><span class="when">15:53</span></div>
<h3>Με απόκλιση 2,2 δισ. ευρώ στα έσοδα έκλεισε το 2015</h3>
<h4>Τι δείχνουν τα καθαρά στοιχεία του προϋπολογισμού </h4>
</a>
</div>
<div id="tabs-recent" class="postEntries noimg" style="height:600px; overflow:hidden">
<a href="/viral/48909/poso-xronwn-egine-h-maria-kavogiannh-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/maria-kavogianni-people-131-1.jpg" alt="Πόσο χρονών έγινε η Μαρία Καβογιάννη;" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span><span class="when">19:43</span></div>
<h3>Πόσο χρονών έγινε η Μαρία Καβογιάννη;</h3>
<h4>Τα γενέθλια της γιόρτασε η αγαπημένη ηθοποιός</h4>
</a>
<a href="/news/48908/isis-aphgagan-600-paidia-gia-na-ta-kanoyn-vomvistes-aytoktonias" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/paidia-isis-12.jpg" alt="ISIS: Απήγαγαν 600 παιδιά για να τα κάνουν βομβιστές αυτοκτονίας" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span><span class="when">19:06</span></div>
<h3>ISIS: Απήγαγαν 600 παιδιά για να τα κάνουν βομβιστές αυτοκτονίας</h3>
<h4>Τα «κουτάβια» του χαλιφάτου</h4>
</a>
<a href="/news/48904/synelhfthh-ypopshfios-voyleyths-toy-syriza-gia-dwrolhpsia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/500euro.jpg" alt="Συνελήφθη υποψήφιος βουλευτής του ΣΥΡΙΖΑ για δωροληψία" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span><span class="when">17:57</span></div>
<h3>Συνελήφθη υποψήφιος βουλευτής του ΣΥΡΙΖΑ για δωροληψία</h3>
<h4>Νε προσημειωμένα χαρτονομίσματα</h4>
</a>
<a href="/life/48902/h-griph-protimaei-toys-antres" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/man-flu.jpg" alt="Η γρίπη προτιμάει τους άντρες!" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span><span class="when">17:42</span></div>
<h3>Η γρίπη προτιμάει τους άντρες!</h3>
<h4>Oι γυναίκες μπορούν να διαχειριστούν καλύτερα τα συμπτώματα μιας γρίπης λόγω ορμονών</h4>
</a>
<a href="/viral/48894/anakoinwthhkan-oi-ypopshfiothtes-gia-ta-vraveia-oskar-2016" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/oscar.jpg" alt="Ανακοινώθηκαν οι υποψηφιότητες για τα βραβεία Όσκαρ 2016" border="0" class="lazyload">
</div>
<div><span class="category">ΣΙΝΕΜΑ</span><span class="when">16:17</span></div>
<h3>Ανακοινώθηκαν οι υποψηφιότητες για τα βραβεία Όσκαρ 2016</h3>
<h4>Διαβάστε την πλήρη λίστα των υποψηφίων</h4>
</a>
<a href="/news/48892/epifanhs-giatros-ekspepmatwse-se-proswpo-asthenoys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/Mount_Sinai.jpg" alt="Επιφανής γιατρός εκσπεpμάτωσε σε πρόσωπο ασθενούς" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span><span class="when">16:00</span></div>
<h3>Επιφανής γιατρός εκσπεpμάτωσε σε πρόσωπο ασθενούς</h3>
<h4>Η 22χρονη τον κατηγορεί για σeξουαλική επίθεση</h4>
</a>
<a href="/sections/48889/deite-enan-kainoyrgio-tropo-poy-pairnoyn-mati-oi-iapwnes" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/un_burera.jpg" alt="Δείτε έναν καινούργιο τρόπο που παίρνουν μάτι οι Ιάπωνες" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">15:48</span></div>
<h3>Δείτε έναν καινούργιο τρόπο που παίρνουν μάτι οι Ιάπωνες</h3>
<h4>Οι ομπρέλες μπανιστηριού κάνουν θραύση</h4>
</a>
<a href="/news/48888/pethane-o-kathhghths-sneip" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/AlanRickman.jpg" alt="Πέθανε ο «Καθηγητής Σνέιπ»" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span><span class="when">15:25</span></div>
<h3>Πέθανε ο «Καθηγητής Σνέιπ»</h3>
<h4>Έφυγε σε ηλικία 69 ετών ο σπουδαίος Alan Rickman, χτυπημένος από τον καρκίνο</h4>
</a>
<a href="/news/48882/to-porisma-toy-iatrodikasth-gia-to-foniko-sthn-kozanh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/velvento_main.jpg" alt="Το πόρισμα του ιατροδικαστή για το φονικό στην Κοζάνη" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">14:02</span></div>
<h3>Το πόρισμα του ιατροδικαστή για το φονικό στην Κοζάνη</h3>
<h4>Κάταγμα στο κρανίο έδειξε η νεκροψία</h4>
</a>
<a href="/news/48878/sta-xnaria-ths-volkswagen-kai-h-renault" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/renault.jpg" alt="Στα χνάρια της Volkswagen και η Renault;" border="0" class="lazyload">
</div>
<div><span class="category">ΟΙΚΟΝΟΜΙΑ</span><span class="when">13:31</span></div>
<h3>Στα χνάρια της Volkswagen και η Renault;</h3>
<h4>Έφοδος των γαλλικών αρχών στα γραφεία της αυτοκινητοβιομηχανίας</h4>
</a>
<a href="/viral/48877/den-kseroyn-ti-toys-ginetai-sth-sxolh-dhmosias-dioikhshs" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/gkafa_main.jpg" alt="Δεν ξέρουν τι τους γίνεται στη Σχολή Δημόσιας Διοίκησης" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">13:26</span></div>
<h3>Δεν ξέρουν τι τους γίνεται στη Σχολή Δημόσιας Διοίκησης</h3>
<h4>Μπέρδεψαν τον εφευρέτη του τεστ ΠΑΠ με τον μπασκεμπολίστα!</h4>
</a>
<a href="/entertainment/48875/oi-tainies-ths-evdomadas-apo-ton-notia-sthn-islandikh-ypaithro-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/NOTIAS.jpg" alt="Οι ταινίες της εβδομάδας: Από τον «Νοτιά» στην ισλανδική ύπαιθρο" border="0" class="lazyload">
</div>
<div><span class="category">ΣΙΝΕΜΑ</span><span class="when">13:23</span></div>
<h3>Οι ταινίες της εβδομάδας: Από τον «Νοτιά» στην ισλανδική ύπαιθρο</h3>
<h4>Αθήνα - Θεσσαλονίκη</h4>
</a>
<a href="/entertainment/48875/oi-tainies-ths-evdomadas-apo-ton-notia-sthn-islandikh-ypaithro-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/NOTIAS.jpg" alt="Οι ταινίες της εβδομάδας: Από τον «Νοτιά» στην ισλανδική ύπαιθρο" border="0" class="lazyload">
</div>
<div><span class="category">ΣΙΝΕΜΑ</span><span class="when">13:23</span></div>
<h3>Οι ταινίες της εβδομάδας: Από τον «Νοτιά» στην ισλανδική ύπαιθρο</h3>
<h4>Αθήνα - Θεσσαλονίκη</h4>
</a>
<a href="/viral/48873/to-forema-poy-ksepoylhse-se-58-lepta-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/foremaobama.jpg" alt="To φόρεμα που ξεπούλησε σε 58 λεπτά! " border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">13:14</span></div>
<h3>To φόρεμα που ξεπούλησε σε 58 λεπτά! </h3>
<h4>Ποια το φόρεσε και το προμόταρε άψογα</h4>
</a>
<a href="/viral/48870/o-liagkas-fetes-ekdikhsh-se-ekswfyllo-me-resital-retoys-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Liagkasre-main.jpg" alt="Ο Λιάγκας φέτες: Εκδίκηση σε εξώφυλλο με ρεσιτάλ ρετούς" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span><span class="when">13:08</span></div>
<h3>Ο Λιάγκας φέτες: Εκδίκηση σε εξώφυλλο με ρεσιτάλ ρετούς</h3>
<h4>Το Down Town απαντά στα επικριτικά σχόλια του Λιάγκα για τη φωτογράφηση της Βίκυς Χατζηβασιλείου</h4>
</a>
<a href="/news/48850/ikea-prolhptikh-anaklhsh-proiontos-kai-plhrhs-apozhmiwsh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/IKEA-label.jpg" alt="IKEA: Προληπτική ανάκληση προϊόντος και πλήρης αποζημίωση" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">12:24</span></div>
<h3>IKEA: Προληπτική ανάκληση προϊόντος και πλήρης αποζημίωση</h3>
<h4>Δίχως να έχει αναφερθεί οποιοδήποτε περιστατικό</h4>
</a>
<a href="/viral/48849/agria-epithesh-koygia-kata-lazopoyloy-gnwrizoyme-ti-proswpikh-idiwtikh-zwh-exei-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/kougias3.jpg" alt="Αγρια επίθεση Κούγια κατά Λαζόπουλου: «Γνωρίζουμε τι προσωπική ιδιωτική ζωή έχει...»  " border="0" class="lazyload">
</div>
<div><span class="category">MEDIA</span><span class="when">11:59</span></div>
<h3>Αγρια επίθεση Κούγια κατά Λαζόπουλου: «Γνωρίζουμε τι προσωπική ιδιωτική ζωή έχει...» </h3>
<h4>Ο γνωστός ποινικολόγος ήταν καλεσμένος στο «The 2night Show»</h4>
</a>
<a href="/themata/48846/anakoinwthhkan-oi-ypopshfiotes-gia-ta-xrysa-vatomoyra-list" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-th-list"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/berries.jpg" alt="Ανακοινώθηκαν οι υποψηφιότες για τα Χρυσά Βατόμουρα" border="0" class="lazyload">
</div>
<div><span class="category">ΣΙΝΕΜΑ</span><span class="when">11:20</span></div>
<h3>Ανακοινώθηκαν οι υποψηφιότες για τα Χρυσά Βατόμουρα</h3>
<h4>Περισσότερες υποψηφιότητες το «Fifty Shades of Grey»</h4>
</a>
<a href="/viral/48844/skoypise-to-xymeno-gala-me-to-kefali-ths-syzygoy-toy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/milkfloor.jpg" alt="Σκούπισε το χυμένο γάλα με... το κεφάλι της συζύγου του!" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">11:00</span></div>
<h3>Σκούπισε το χυμένο γάλα με... το κεφάλι της συζύγου του!</h3>
<h4>Έκανε το κεφάλι της γυναίκας του... σφουγγαρίστρα</h4>
</a>
<a href="/news/48833/h-stigmh-ths-syllhpshs-toy-syzygoktonoy-sto-xwrafi-opoy-ethapse-th-gynaika-toy-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sizigos_sylipsi_kozani1.jpg" alt="Η στιγμή της σύλληψης του συζυγοκτόνου στο χωράφι όπου έθαψε τη γυναίκα του" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">08:50</span></div>
<h3>Η στιγμή της σύλληψης του συζυγοκτόνου στο χωράφι όπου έθαψε τη γυναίκα του</h3>
<h4>Στην Κοζάνη</h4>
</a>
</div>
</div>
<br>
<center>
<script language="javascript">
<!--
if (window.adgroupid == undefined) {
                window.adgroupid = Math.round(Math.random() * 1000);
}
document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1370/5799492/0/170/ADTECH;loc=100;target=_blank;key=;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
//-->
</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1370/5799492/0/170/ADTECH;loc=300;key=" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1370/5799492/0/170/ADTECH;loc=300;key=" border="0" width="300" height="250"></a></noscript>
</center>
<br>
<script>
$().ready(function() {
    $("#topTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  defaultTab: "li#ttab-themata",
	  updateHash: false
	});
/*
	$("#tabs-news").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-viral").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-life").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-themata").mCustomScrollbar({
		theme:"dark-thick"
	});
*/	
});

</script>
<div class="postsRedHeader">δημοφιλέστερα</div>
<div id="topTabs" class="tab-container">
<ul class="etabs">
<li class="tab" id="ttab-news"><a href="#tabs-news">ΕΙΔΗΣΕΙΣ</a></li>
<li class="tab" id="ttab-themata"><a href="#tabs-themata">θΕΜΑΤΑ</a></li>
<li class="tab" id="ttab-life"><a href="#tabs-life">LIFE</a></li>
<li class="tab" id="ttab-viral"><a href="#tabs-viral">VIRAL</a></li>
</ul>
<div id="tabs-news" class="postEntries top">
<a href="/news/48833/h-stigmh-ths-syllhpshs-toy-syzygoktonoy-sto-xwrafi-opoy-ethapse-th-gynaika-toy-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sizigos_sylipsi_kozani1.jpg" alt="Η στιγμή της σύλληψης του συζυγοκτόνου στο χωράφι όπου έθαψε τη γυναίκα του" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Η στιγμή της σύλληψης του συζυγοκτόνου στο χωράφι όπου έθαψε τη γυναίκα του</h3>
<h4>Στην Κοζάνη</h4>
</a>
<a href="/news/48809/kozanh-epnikse-thn-37xronh-syzygo-toy-kai-meta-thn-ethapse-se-xwrafi" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/kozani.jpg" alt="Κοζάνη: Έπνιξε την 37χρονη σύζυγό του και μετά την έθαψε σε χωράφι" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Κοζάνη: Έπνιξε την 37χρονη σύζυγό του και μετά την έθαψε σε χωράφι</h3>
<h4>Ομολόγησε ο δράστης</h4>
</a>
<a href="/news/48882/to-porisma-toy-iatrodikasth-gia-to-foniko-sthn-kozanh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/velvento_main.jpg" alt="Το πόρισμα του ιατροδικαστή για το φονικό στην Κοζάνη" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Το πόρισμα του ιατροδικαστή για το φονικό στην Κοζάνη</h3>
<h4>Κάταγμα στο κρανίο έδειξε η νεκροψία</h4>
</a>
<a href="/news/48892/epifanhs-giatros-ekspepmatwse-se-proswpo-asthenoys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/Mount_Sinai.jpg" alt="Επιφανής γιατρός εκσπεpμάτωσε σε πρόσωπο ασθενούς" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span></div>
<h3>Επιφανής γιατρός εκσπεpμάτωσε σε πρόσωπο ασθενούς</h3>
<h4>Η 22χρονη τον κατηγορεί για σeξουαλική επίθεση</h4>
</a>
<a href="/news/48850/ikea-prolhptikh-anaklhsh-proiontos-kai-plhrhs-apozhmiwsh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/IKEA-label.jpg" alt="IKEA: Προληπτική ανάκληση προϊόντος και πλήρης αποζημίωση" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>IKEA: Προληπτική ανάκληση προϊόντος και πλήρης αποζημίωση</h3>
<h4>Δίχως να έχει αναφερθεί οποιοδήποτε περιστατικό</h4>
</a>
<a href="/news/48838/astynomikos-eswse-egkateleimmeno-vrefos-thhlazontas-to-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/astunomikosmwro3.jpg" alt="Αστυνομικός έσωσε εγκατελειμμένο βρέφος θηλάζοντάς το" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span></div>
<h3>Αστυνομικός έσωσε εγκατελειμμένο βρέφος θηλάζοντάς το</h3>
<h4>Η μητέρα του το εγκατέλειψε σε δάσος λίγες ώρες μετά τη γέννησή του</h4>
</a>
<a href="/news/48781/kanivaloi-pagideysan-zeygari-toyristwn-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/03_13020245_0e0923_2638748a.jpg" alt="Κανίβαλοι παγίδευσαν ζευγάρι τουριστών" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span></div>
<h3>Κανίβαλοι παγίδευσαν ζευγάρι τουριστών</h3>
<h4>Έζησαν έναν εφιάλτη όταν συνάντησαν την άγρια φυλή στην Παπούα</h4>
</a>
<a href="/news/48837/synelhfthh-kai-o-petheros-ths-anthhs" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/petheros2.jpg" alt="Συνελήφθη και ο πεθερός της Ανθής" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Συνελήφθη και ο πεθερός της Ανθής</h3>
<h4>Για παράνομη οπλοκατοχή</h4>
</a>
</div>
<div id="tabs-themata" class="postEntries top">
<a href="/themata/48689/to-ksespasma-toy-sakh-arnaoytogloy-gia-ton-xionia-poy-erxetai" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/sakis_arnaou.jpg" alt="Το ξέσπασμα του Σάκη Αρναούτογλου για τον... «χιονιά» που έρχεται" border="0" class="lazyload">
</div>
<div><span class="category">ΚΑΙΡΟς</span></div>
<h3>Το ξέσπασμα του Σάκη Αρναούτογλου για τον... «χιονιά» που έρχεται</h3>
<h4>Διαβάστε την ανάρτησή του στο Facebook</h4>
</a>
<a href="/themata/48625/david-bowie-pantremenos-me-thn-wraioterh-gynaika-ston-kosmo-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/bowie_gynaika_0.jpg" alt="David Bowie: Παντρεμένος με την ωραιότερη γυναίκα στον κόσμο" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>David Bowie: Παντρεμένος με την ωραιότερη γυναίκα στον κόσμο</h3>
<h4>Ποια είναι η Σομαλή Iman</h4>
</a>
<a href="/themata/48417/sto-eswteriko-enos-spitioy-300-etwn-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/casa_300_anos_1.jpg" alt="Στο εσωτερικό ενός σπιτιού 300 ετών" border="0" class="lazyload">
</div>
<div><span class="category">ΑΡΧΙΤΕΚΤΟΝΙΚΗ</span></div>
<h3>Στο εσωτερικό ενός σπιτιού 300 ετών</h3>
<h4>Μοναδική γοητεία και σύγχρονη αισθητική</h4>
</a>
<a href="/themata/48440/ekplhktiko-vinteo-deite-to-tsernompil-opws-einai-shmera-30-xronia-meta-to-atyxhma-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tsernompil.jpg" alt="Εκπληκτικό βίντεο: Δείτε το Τσερνομπίλ όπως είναι σήμερα 30 χρόνια μετά το ατύχημα" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟς</span></div>
<h3>Εκπληκτικό βίντεο: Δείτε το Τσερνομπίλ όπως είναι σήμερα 30 χρόνια μετά το ατύχημα</h3>
<h4>Χτίζεται μία τεράστια αψίδα προστασίας από την ραδιενέργεια</h4>
</a>
<a href="/themata/48451/asygkrathtos-o-manoy-tsao-me-seksi-xadia-apoxairethse-thn-agaphmenh-toy-klelia-renesh-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tsao_renesi_0.jpg" alt="Ασυγκράτητος ο Μάνου Τσάο: Με σeξι «χάδια» αποχαιρέτησε την αγαπημένη του Κλέλια Ρένεση " border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Ασυγκράτητος ο Μάνου Τσάο: Με σeξι «χάδια» αποχαιρέτησε την αγαπημένη του Κλέλια Ρένεση </h3>
<h4>Μαζί στο αεροδρόμιο</h4>
</a>
<a href="/themata/48324/pws-htan-h-seksoyalikh-zwh-twn-gynaikwn-sthn-arxaia-ellada" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/gunaikaantrasarxaiaellada.jpg" alt="Πώς ήταν η σeξουαλική ζωή των γυναικών στην Αρχαία Ελλάδα;" border="0" class="lazyload">
</div>
<div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span></div>
<h3>Πώς ήταν η σeξουαλική ζωή των γυναικών στην Αρχαία Ελλάδα;</h3>
<h4>Τι αποκαλύπτουν οι πηγές</h4>
</a>
<a href="/themata/48420/pws-leitoyrgei-to-sympan-symfwna-me-tis-thewries-toy-stephen-hawking-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/stephenhawkins.jpg" alt="Πώς λειτουργεί το σύμπαν, σύμφωνα με τις θεωρίες του Stephen Hawking" border="0" class="lazyload">
</div>
<div><span class="category">ΕΠΙΣΤΗΜΗ</span></div>
<h3>Πώς λειτουργεί το σύμπαν, σύμφωνα με τις θεωρίες του Stephen Hawking</h3>
<h4>Ένα χαριτωμένο animation για τους κοινούς θνητούς</h4>
</a>
<a href="/themata/48594/oi-7-pio-perierges-apagoreyseis-se-xwres-toy-kosmoy-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/saudiwoman.jpg" alt="Οι 7 πιο περίεργες απαγορεύσεις σε χώρες του κόσμου" border="0" class="lazyload">
</div>
<div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span></div>
<h3>Οι 7 πιο περίεργες απαγορεύσεις σε χώρες του κόσμου</h3>
<h4>Κανόνες που ξαφνιάζουν</h4>
</a>
</div>
<div id="tabs-life" class="postEntries top">
<a href="/life/48661/to-test-ths-paralias-poy-leei-polla-gia-thn-proswpikothta-sas-quiz" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-doc-text"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/paralia3.jpg" alt="Το τεστ της παραλίας... που λέει πολλά για την προσωπικότητά σας" border="0" class="lazyload">
</div>
<div><span class=alert>QUIZ</span><span class="category">BODY+MIND</span></div>
<h3>Το τεστ της παραλίας... που λέει πολλά για την προσωπικότητά σας</h3>
<h4>Μερικές ερωτήσεις και θα το ανακαλύψετε</h4>
</a>
<a href="/life/48821/ypallhloi-ksenodoxeiwn-apokalyptoyn-ta-vromika-mystika-toys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/hotel-room.jpg" alt="Υπάλληλοι ξενοδοχείων αποκαλύπτουν τα... βρόμικα μυστικά τους" border="0" class="lazyload">
</div>
<div><span class="category">ΤΑΞΙΔΙΑ</span></div>
<h3>Υπάλληλοι ξενοδοχείων αποκαλύπτουν τα... βρόμικα μυστικά τους</h3>
<h4>Σεντόνια που δεν αλλάζονται και κουβέρτες που δεν πλένονται ποτέ!</h4>
</a>
<a href="/life/48346/zwdia-evdomadas-11-17-ianoyarioy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/ZODIA43.jpg" alt="Ζώδια Εβδομάδας: 11-17 Ιανουαρίου" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΔΙΑ</span></div>
<h3>Ζώδια Εβδομάδας: 11-17 Ιανουαρίου</h3>
<h4>Τι λένε τα άστρα</h4>
</a>
<a href="/life/48539/10-faghta-poy-tha-sas-steiloyn-sthn-toyaleta" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/eatbaby4.jpg" alt="10 φαγητά που θα σας στείλουν στην τουαλέτα" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span></div>
<h3>10 φαγητά που θα σας στείλουν στην τουαλέτα</h3>
<h4>Η φύση έχει τα καλύτερα φάρμακα</h4>
</a>
<a href="/life/48340/10-spanies-nosoi-kai-pathhseis-poy-den-gnwrizate-pws-yparxoyn-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/asthenies-32.jpg" alt="10 σπάνιες νόσοι και παθήσεις που δεν γνωρίζατε πως υπάρχουν" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span></div>
<h3>10 σπάνιες νόσοι και παθήσεις που δεν γνωρίζατε πως υπάρχουν</h3>
<h4>Εξαιρετικά σπάνιες και δυστυχώς ανίατες</h4>
</a>
<a href="/life/48696/perioxh-ths-athhnas-stis-kalyteres-toy-kosmoy-poia-einai" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/koukaki.jpg" alt="Περιοχή της Αθήνας στις καλύτερες του κόσμου - Ποια είναι;" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΗ</span></div>
<h3>Περιοχή της Αθήνας στις καλύτερες του κόσμου - Ποια είναι;</h3>
<h4>Μπορεί να προκαλεί έκπληξη, αλλά μια συνοικία της Αθήνας είναι από τις καλύτερες στον κόσμο</h4>
</a>
<a href="/life/48634/pagkosmio-sok-gia-ton-io-zika-kindynos-gia-egkyoys-kai-emvrya-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zika.jpg" alt="Παγκόσμιο σοκ για τον ιό Ζίκα - Κίνδυνος για εγκύους και έμβρυα" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span></div>
<h3>Παγκόσμιο σοκ για τον ιό Ζίκα - Κίνδυνος για εγκύους και έμβρυα</h3>
<h4>Yπεύθυνος για τις γεννήσεις παιδιών με μικροκεφαλία</h4>
</a>
<a href="/life/48535/oi-diaites-ths-neas-xronias-dialekste-poia-sas-tairiazei" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/dietwight.jpg" alt="Οι δίαιτες της νέας χρονιάς: Διαλέξτε ποια σας ταιριάζει" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span></div>
<h3>Οι δίαιτες της νέας χρονιάς: Διαλέξτε ποια σας ταιριάζει</h3>
<h4>Πάρτε έμπνευση και... καλή αρχή!</h4>
</a>
</div>
<div id="tabs-viral" class="postEntries top">
<a href="/viral/48719/h-evelina-papoylia-agnwristh-me-agoristiko-koyrema-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/evelina_papoulia.jpg" alt="Η Εβελίνα Παπούλια: Αγνώριστη με αγορίστικο κούρεμα" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Η Εβελίνα Παπούλια: Αγνώριστη με αγορίστικο κούρεμα</h3>
<h4>Τέλος το μακρύ μαλλί</h4>
</a>
<a href="/viral/48766/frenitida-sthn-khfisia-gia-ta-sniker-toy-kanye-west-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/main_kanye.jpeg" alt="Φρενίτιδα στην Κηφισιά για τα σνίκερ του Kanye West" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Φρενίτιδα στην Κηφισιά για τα σνίκερ του Kanye West</h3>
<h4>Μέχρι και σε sleeping bags έξω από το κατάστημα «Simple Caractere» της Κηφισιάς κοιμήθηκαν επίδοξοι αγοραστές</h4>
</a>
<a href="/viral/48630/ksanaxtyphse-h-katara-toy-ramsei-petyxe-gkol-pethane-kai-o-bowie" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/ramsei_goal_nekros.jpg" alt="Ξαναχτύπησε η κατάρα του Ράμσεϊ: Πέτυχε γκολ, πέθανε και ο Bowie!" border="0" class="lazyload">
</div>
<div><span class="category">ΑΘΛΗΤΙΣΜΟς</span></div>
<h3>Ξαναχτύπησε η κατάρα του Ράμσεϊ: Πέτυχε γκολ, πέθανε και ο Bowie!</h3>
<h4>Μοιάζει με μακάβριο ανέκδοτο, αλλά δεν είναι...</h4>
</a>
<a href="/viral/48710/prosfeygei-sth-dikaiosynh-o-patoylhs-gia-ayto-to-vinteo-sto-radio-arvyla" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/zeugospatouli.jpg" alt="Προσφεύγει στη Δικαιοσύνη ο Πατούλης για αυτό το βίντεο στο «Ράδιο Αρβύλα»" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Προσφεύγει στη Δικαιοσύνη ο Πατούλης για αυτό το βίντεο στο «Ράδιο Αρβύλα»</h3>
<h4>Οργισμένος ο δήμαρχος Αμαρουσίου</h4>
</a>
<a href="/viral/48823/stixoyrgos-gia-gnwsth-tragoydistria-kai-twra-tha-niwsei-kala-poy-o-diskos-ths-exei-ton-apoylhto-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-camera-outline"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tragoudistria-xxx.jpg" alt="Στιχουργός για γνωστή τραγουδίστρια: Και τώρα θα νιώσει καλά που ο δίσκος της έχει τον απούλητο" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Στιχουργός για γνωστή τραγουδίστρια: Και τώρα θα νιώσει καλά που ο δίσκος της έχει τον απούλητο</h3>
<h4>«Θα πάρει στα χέρια της την κάλπικη πλατίνα και θα χαμογελάσει για τα περιοδικά»</h4>
</a>
<a href="/viral/48819/h-elenh-menegakh-mperdeythke-kai-milhse-on-air-ston-makh-pantzopoylo-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/eleni-menegaki-makis-ekp.jpg" alt="Η Ελένη Μενεγάκη μπερδεύτηκε και μίλησε on air στον Μάκη Παντζόπουλο" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Η Ελένη Μενεγάκη μπερδεύτηκε και μίλησε on air στον Μάκη Παντζόπουλο</h3>
<h4>Την ώρα της μαγειρικής μάλλον το μυαλό της ήταν αλλού</h4>
</a>
<a href="/viral/48599/metakomise-se-neo-diamerisma-kai-anakalypse-mystikh-krypth-kai-ypogeia-mpoyntroymia-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play-circled2"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dungeon-12.jpg" alt="Μετακόμισε σε νέο διαμέρισμα και ανακάλυψε μυστική κρύπτη και υπόγεια μπουντρούμια" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span></div>
<h3>Μετακόμισε σε νέο διαμέρισμα και ανακάλυψε μυστική κρύπτη και υπόγεια μπουντρούμια</h3>
<h4>Τα παλιά σπίτια στη Βρετανία είναι γεμάτα εκπλήξεις</h4>
</a>
<a href="/viral/48681/prostimo-20000-eyrw-sto-mega-gia-th-fwtografia-me-thn-hlikiwmenh-sto-atm" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/tvshow_meganews.jpg" alt="Πρόστιμο 20.000 ευρώ στο MEGA για τη φωτογραφία με την ηλικιωμένη στο ΑΤΜ" border="0" class="lazyload">
</div>
<div><span class="category">MEDIA</span></div>
<h3>Πρόστιμο 20.000 ευρώ στο MEGA για τη φωτογραφία με την ηλικιωμένη στο ΑΤΜ</h3>
<h4>Είχε προβληθεί ως εικόνα από την Ελλάδα</h4>
</a>
</div>
</div>
</div>
</div>
<div id="footerBgWide">
<center>
<div id="footerMain">
<div class="footer hideOnSmallScreen" style="width:12%;">
<span class="footer_optionsHeader"><a href="/news">Ειδήσεις</a></span><br>
<span class="footer_options">
<a href="/news">Τελευταία νέα</a>
<a href="/ellada">Ελλάδα</a>
<a href="/kosmos">Κόσμος</a>
<a href="/politiki">Πολιτική</a>
<a href="/oikonomia">Οικονομία</a>
<a href="/athlitismos">Αθλητισμός</a>
<a href="/epistimi">Επιστήμη</a>
<a href="/politismos">Πολιτισμός</a>
</span>
</div>
<div class="footer hideOnSmallScreen" style="width:12%;">
<div class="footer_optionsHeader"><a href="/themata">Θέματα</a></div><br>
<div class="footer_optionsHeader"><a href="/quiz">Κουίζ</a></div><br>
<div class="footer_optionsHeader"><a href="/viral">Viral</a></div><br>
<div class="footer_optionsHeader"><a href="/lol">LOL</a></div>
<div class="footer_optionsHeader"><a href="/omg">OMG</a></div>
<div class="footer_optionsHeader"><a href="/juicy">JUICY</a></div>
<div class="footer_optionsHeader"><a href="/alist">A-LIST</a></div>
<div class="footer_optionsHeader"><a href="/geeky">GEEKY</a></div>
<div class="footer_optionsHeader"><a href="/cute">CUTE</a></div>
<div class="footer_optionsHeader"><a href="/retro">RETRO</a></div>
</div>
<div class="footer hideOnSmallScreen" style="width:12%;">
<span class="footer_optionsHeader"><a href="/life">Life</a></span><br>
<span class="footer_options">
<a href="/food">Food</a>
<a href="/bodymind">Body+Mind</a>
<a href="/travel">Ταξίδια</a>
<a href="/style">Style</a>
<a href="/spiti">Σπίτι</a>
<a href="/family">Family</a>
<a href="/sxeseis">Σχέσεις</a>
</span><br>
</div>
<div class="footer hideOnSmallScreen" style="width:18%;">
<div class="footer_optionsHeader"><a href="/entertainment">Ψυχαγωγία</a></div><br>
<span class="footer_options">
<a href="/agenda">Agenda</a>
<a href="/wintickets">Κερδίστε Προσκλήσεις</a><br>
<a href="/music">Μουσική</a>
<a href="/theater">Θέατρο+Χορός</a>
<a href="/arts">Εικαστικά</a>
<a href="/cinema">Σινεμά</a>
<a href="/nightlife">Νάιτ Λάιφ</a>
<a href="/city">Πόλη</a>
</span>
</div>
<div class="footer fullOnSmallScreen" style="width:32%;">
<div class="footer_optionsHeader">Newsletter</div><br>
<style type="text/css">#mc-embedded-subscribe-form{margin:0;padding:0}#mc_embed_signup{display:table;clear:both;text-align:left;line-height:normal;}#mc_embed_signup .mc-field-group{display:table;margin:10px 0 10px 0;padding:0;width:100%}#mc_embed_signup li{display:block;float:left;margin-top:12px;width:33.333%;padding:0;font-weight:400;font-size:1.25em;}#mc_embed_signup .checkbox{width:22px;height:22px;border:1px solid #e3e3e3;background:#fff;margin:0;padding:0}#mc_embed_signup .checkbox:before{content:"";display:block;width:23px;height:23px;background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 0 0 no-repeat;}#mc_embed_signup .checkbox:checked:before{background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') -32px 0 no-repeat;}#mc_embed_signup label{margin-left:5px;padding-top:-5px;}#mc_embed_signup .email{font-weight:normal;height:45px;width:55%;float:left;display:inline-block;background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 10px -22px no-repeat;color:#000;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;padding:0 0 0 46px;border:1px solid #d9d9d9}#mc_embed_signup .email.focus{color:#000;}#mc_embed_signup .email.placeholder{color:#e3e3e3;}#mc_embed_signup .submit-button{background:#0079ff;text-align:center;float:left;width:30%;height:47px;border:none;font-size:14px;text-transform:uppercase;color:#FFF;letter-spacing:1px;cursor:pointer;display:inline-block;padding-top:6px;margin-left:3px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;}#mc_embed_signup .submit-button:hover{background:#2e78b3;text-decoration:none;}#mc_embed_signup div.mce_inline_error{background-color:#fff;color:#804040;}.clearfix{padding:0;margin:0}</style>
<div id="mc_embed_signup" class="clearfix">
<form action="//nextweb.us10.list-manage.com/subscribe/post?u=037b671af872ebf3eb1ceac4a&amp;id=5ab11026ce" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div class="clearfix">Εγραφείτε στα newsletters του E-Daily.gr για να μαθαίνετε τα καλύτερα στο email σας:</div>
<ul class="mc-field-group input-group">
<li>
<input type="checkbox" class="checkbox" value="1" name="group[1109][1]" id="mce-group[1109]-1109-0" checked="checked">
<label for="mce-group[1109]-1109-0">Ειδήσεις</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="2" name="group[1109][2]" id="mce-group[1109]-1109-1" checked="checked">
<label for="mce-group[1109]-1109-1">Viral</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="4" name="group[1109][4]" id="mce-group[1109]-1109-2" checked="checked">
<label for="mce-group[1109]-1109-2">Life</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="8" name="group[1109][8]" id="mce-group[1109]-1109-3" checked="checked">
<label for="mce-group[1109]-1109-3">E-Agenda</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="16" name="group[1109][16]" id="mce-group[1109]-1109-4" checked="checked">
<label for="mce-group[1109]-1109-4">E-Weekly</label>
</li>
<li></li>
</ul>
<div class="mc-field-group" style="margin-top:25px;">
<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Συμπληρώστε το email σας">
<input type="submit" class="submit-button" value="ΕΓΓΡΑΦΗ" name="subscribe" id="mc-embedded-subscribe">
</div>
</form>
<div id="mce-responses" class="clear">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>
<div style="position: absolute; left: -5000px;"><input type="text" name="b_037b671af872ebf3eb1ceac4a_5ab11026ce" tabindex="-1" value=""></div>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email'; /*
 * Translated default messages for the $ validation plugin.
 * Locale: EL
 */
$.extend($.validator.messages, {
	required: "<span class=asterisk></span> Αυτό το πεδίο είναι υποχρεωτικό.",
	remote: "<span class=asterisk></span> Παρακαλώ διορθώστε αυτό το πεδίο.",
	email: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη διεύθυνση email.",
	url: "<span class=asterisk></span> Παρακαλώ εισάγετε ένα έγκυρο URL.",
	date: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία.",
	dateISO: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία (ISO).",
	number: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό.",
	digits: "<span class=asterisk></span> Παρακαλώ εισάγετε μόνο αριθμητικά ψηφία.",
	creditcard: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό πιστωτικής κάρτας.",
	equalTo: "<span class=asterisk></span> Παρακαλώ εισάγετε την ίδια τιμή ξανά.",
	accept: "<span class=asterisk></span> Παρακαλώ εισάγετε μια τιμή με έγκυρη επέκταση αρχείου.",
	maxlength: $.validator.format("<span class=asterisk></span> Παρακαλώ εισάγετε μέχρι και {0} χαρακτήρες."),
	minlength: $.validator.format("Παρακαλώ εισάγετε τουλάχιστον {0} χαρακτήρες."),
	rangelength: $.validator.format("Παρακαλώ εισάγετε μια τιμή με μήκος μεταξύ {0} και {1} χαρακτήρων."),
	range: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεταξύ {0} και {1}."),
	max: $.validator.format("Παρακαλώ εισάγετε μια τιμή μικρότερη ή ίση του {0}."),
	min: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεγαλύτερη ή ίση του {0}.")
});}(jQuery));var $mcj = jQuery.noConflict(true);</script>
 
</div>
<div class="footer footerRight footer_options fullOnSmallScreen" style="width:14%; text-align: right">
<div class="footer_optionsHeader">Follow us</div><br>
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<span>Facebook</span>
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<span>Twitter</span>
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<span>Google+</span>
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<span>Instagram</span>
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<span>YouTube</span>
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<span>RSS</span>
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</div>
</center>
</div>
<center>
<div id="footerMain">
<div class="footer footer21">
<a href="javascript:openContact();">Επικοινωνία</a>&nbsp;&nbsp;&nbsp;
<a href="javascript:openContact();">Διαφήμιση</a>&nbsp;&nbsp;&nbsp;
<a href="/privacy.asp">Privacy</a>&nbsp;&nbsp;&nbsp;
<a href="/terms.asp">Terms of use</a>
<br>
<a href="http://www.e-radio.gr" target="_blank">E-Radio Hellas </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.e-radio.com.cy" target="_blank">E-Radio Cyprus </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.e-radio.co.uk" target="_blank">E-Radio UK </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.onestreaming.com" target="_blank">One Streaming </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.arionradio.com" target="_blank">Arion Radio </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.athensparty.com" target="_blank">Athens Party </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.akous.gr" target="_blank">Akous </a> &nbsp;&nbsp;&nbsp;
<br>
© 2015 All Right Reserved. "E-DAILY" is a registered trademark
</div>
<div class="footer footer22">
<a href="http://www.nextweb.gr" target="_blank"><img src="/_img/nextweb.png" border="0" alt="Nextweb"></a>
</div>
</div>
</center>
</div>
</div>
<script type='text/javascript'>
$().ready(function() {
	
	if( $('#topHeaderMobile').length ){

		$('#mobilemenu_pad').infinitypush({
			openingspeed: 300,
			closingspeed: 300
		});
		
		/*
		$('#mobilemenu_icon').click(function(){
			
			$(this).toggleClass('open');
			//slideout.toggle();
			
		});
	
		slideout.on('open', function() {
			if ($('#mobilemenu_icon').hasClass('open')==false){$('#mobilemenu_icon').addClass('open')}
		});
		slideout.on('close', function() {
			if ($('#mobilemenu_icon').hasClass('open')==true){$('#mobilemenu_icon').removeClass('open')}
	
		});
		*/
	}

});
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-551bf8914936f63f" async="async"></script>
<script type="text/javascript">

	<!-- Google -->
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-504700-32', 'auto');
  ga('send', 'pageview');

	<!-- Tynt -->
	if(document.location.protocol=='http:'){
	 var Tynt=Tynt||[];Tynt.push('aFq4livo0r4yzYacwqm_6r');Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false};
	 (function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
	}
	
	<!-- Quantcast Tag -->
	var _qevents = _qevents || [];
	
	(function() {
	var elem = document.createElement('script');
	elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
	elem.async = true;
	elem.type = "text/javascript";
	var scpt = document.getElementsByTagName('script')[0];
	scpt.parentNode.insertBefore(elem, scpt);
	})();
	
	_qevents.push({
	qacct:"p-49SuwTaH2w2BM"
	});

</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-49SuwTaH2w2BM.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6032644253807', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6032644253807&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1"/></noscript>
 
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3125699,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('http://s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="http://www.histats.com" target="_blank"><img src="http://sstatic1.histats.com/0.gif?3125699&101" alt="web hit counter" border="0"></a></noscript>
<script type="text/javascript">
( function() {
    var info = getTagInfo();
    var alias = 'edhomeskin_display';
        alias = alias !== '' ? 'alias=' + ( alias.split("_") ? alias.split("_")[ 0 ] : alias ) + '_' + info.alias : '';
    document.write('<scr'+'ipt language="javascript1.1" src="http://' + info.domain + '.adtech.de/addyn/3.0/1370.1/4713596/0/-1/ADTECH;' + alias + ';loc=100;target=_blank;key=;grp=' + info.adgroupid + ';misc=' + new Date().getTime() + '"></scri'+'pt>');
})();</script>
<noscript>Please enable scripting languages in your browser.</noscript>
</body>
</html>
