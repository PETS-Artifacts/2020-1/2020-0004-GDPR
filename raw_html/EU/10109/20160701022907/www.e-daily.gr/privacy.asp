
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>E-Daily.gr Πολιτική Απορρήτου</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="charset" content="utf-8"/>
<meta http-equiv="Refresh" content="900"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta content="Πολιτική Απορρήτου" name="Description"/>
<meta content="" name="keywords"/>
<meta property="og:title" content="E-Daily.gr Πολιτική Απορρήτου"/>
<meta property="og:description" content=""/>
<meta property="og:site_name" content="E-Daily.gr"/>
<meta property="og:type" content="Website"/>
<meta property="og:image" content="http://www.e-daily.gr/_img/fblogo.png"/>
<meta property="fb:app_id" content="1590239487856378"/>
<meta property="og:url" content="http://www.e-daily.gr"/>
<link rel="canonical" href="http://www.e-daily.gr"/>
<link rel="stylesheet" href="/_css/post.css"/>
<link rel="stylesheet" href="/_js/scrollbar/jquery.mCustomScrollbar.css"/>
<meta name="apple-itunes-app" content="app-id=776881885">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="/_ico/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/_ico/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/_ico/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/_ico/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/_ico/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/_ico/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/_ico/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/_ico/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/_ico/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/_ico/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/_ico/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/_ico/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/_ico/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/_ico/manifest.json">
<link rel="mask-icon" href="/_ico/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/_ico/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/_ico/mstile-144x144.png">
<meta name="msapplication-config" content="/_ico/browserconfig.xml">
<meta name="theme-color" content="#ff0004">
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,400italic|Roboto+Condensed:300,400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href="/_css/style.css" rel="stylesheet" type="text/css"/>
<link href="/_css/navigation.css" rel="stylesheet" type="text/css"/>
<link href="/_js/menu/mgmenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/mobilemenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/structure.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/_img/forecastfont/weather.css"/>
<link rel="stylesheet" href="/_img/edailyicons/css/edaily.css">
<link rel="stylesheet" href="/_img/edailyicons/css/animation.css"><!--[if IE 7]><link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->
<link rel="stylesheet" type="text/css" href="/_js/mmenu/jquery.ma.infinitypush.css"/>
<script language="javaScript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script language="javaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/1.0.1/lazysizes.min.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/autocomplete/jquery.autocomplete.min.js"></script>
<script language="javaScript" type="text/javascript" src="http://www.e-radio.gr/cache/mediadata_1.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.popupWindow.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.socialstats.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/menu/mgmenu_plugins.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/menu/mgmenu.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/mmenu/jquery.ma.infinitypush.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/tools_all.js"></script>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
<script type='text/javascript'>
if(document.location.protocol=='http:'){
var Tynt=Tynt||[];Tynt.push('d91U86Bp0r5zpGrkHcnlKl');
Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false,"domain":"e-daily.gr"};
(function(){var h,s=document.createElement('script');s.src='http://cdn.tynt.com/ti.js';
h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();}
</script>
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"http://www.e-daily.gr/privacy.asp","theme":"light-bottom"};
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dt/common/DAC.js"></script>
<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dac/1370.1/w1122524.js"></script>
<script type="text/javascript" id="ean-native-embed-tag" src="//cdn.elasticad.net/native/serve/js/nativeEmbed.gz.js"></script>
<script src="/_js/tabs/jquery.easytabs.js" type="text/javascript"></script>
<script src="/_js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body>
<script type='text/javascript'>
  var nuggprof = '';
  var nuggtg = encodeURIComponent('__CONTENT_TAG__');
  var nuggrid = encodeURIComponent(top.location.href);
  document.write('<scr'+'ipt type="text/javascript" src="//adweb.nuggad.net/rc?nuggn=1230610253&nuggsid=1022102324&nuggtg='+nuggtg+'&nuggrid='+nuggrid+'"></scr'+'ipt>');
</script>
<div id="wrapper">
<nav id="mobilemenu_pad">
<ul class="sections">
<li><a href="/news">Ειδήσεις</a>
<li><a href="/viral">Viral</a>
<li><a href="/quiz">Quiz</a>
<li><a href="/life">Life</a>
<li><a href="/themata">Θέματα</a>
<li><a href="/entertainment">Ψυχαγωγία</a>
<li><a href="/agenda">E-Agenda</a>
</ul>
<ul class="sections badges">
<li><a href="/omg"><img src="/_img/feeds/101.png" border="0"/> OMG</a>
<li><a href="/lol"><img src="/_img/feeds/106.png" border="0"/> LOL</a>
<li><a href="/woman"><img src="/_img/feeds/112.png" border="0"/> Woman</a>
<li><a href="/alist"><img src="/_img/feeds/91.png" border="0"/> A-List</a>
<li><a href="/cute"><img src="/_img/feeds/109.png" border="0"/> Cute</a>
<li><a href="/retro"><img src="/_img/feeds/110.png" border="0"/> Retro</a>
<li><a href="/juicy"><img src="/_img/feeds/104.png" border="0"/> Juicy</a>
<li><a href="/lgbt"><img src="/_img/feeds/55.png" border="0"/> LGBT</a>
<li><a href="/geeky"><img src="/_img/feeds/108.png" border="0"/> Geeky</a>
<li><a href="/win"><img src="/_img/feeds/111.png" border="0"/> Win</a>
<li><a href="/summer"><img src="/_img/feeds/15.png" border="0"/> Summer</a>
<li><a href="/xmas"><img src="/_img/feeds/14.png" border="0"/> Xmas</a>
<li><a href="/summer"><img src="/_img/feeds/12.png" border="0"/> Easter</a>
</ul>
<ul class="sections topics">
<li><a href="/food">Food</a>
<li><a href="/bodymind">Body+Mind</a>
<li><a href="/travel">Ταξίδια</a>
<li><a href="/style">Style</a>
<li><a href="/spiti">Σπίτι</a>
<li><a href="/family">Family</a>
<li><a href="/sxeseis">Σχέσεις</a>
</ul>
<ul class="sections topics">
<li><a href="/agenda">Agenda</a>
<li><a href="/music">Μουσική</a>
<li><a href="/theater">Θέατρο</a>
<li><a href="/arts">Εικαστικά</a>
<li><a href="/cinema">Σινεμά</a>
<li><a href="/nightlife">Νάιτ Λάιφ</a>
<li><a href="/city">Πόλη</a>
</ul>
<ul class="sections topics">
<li><a href="/weather">Καιρός</a>
</ul>
<div class="social">
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</nav>
<div id="main">
<form id="skinform"></form>
<div id="spider"></div>
<div id="topHeaderWarper">
<div id="topHeader">
<div id="topHeaderA">
<a href="/" class="mainlogo"><img src="/_img/edailygr_beta_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία"/></a>
</div>
<div id="topHeaderB" class="feedIcons">
<a href="/win"><img src="/_img/feeds/111.png" border="0" alt="Win Feed"/></a>
<a href="/woman"><img src="/_img/feeds/112.png" border="0" alt="Woman Feed"/></a>
<a href="/omg"><img src="/_img/feeds/101.png" border="0" alt="OMG Feed"/></a>
<a href="/lol"><img src="/_img/feeds/106.png" border="0" alt="LOL Feed"/></a>
<a href="/juicy"><img src="/_img/feeds/104.png" border="0" alt="Juicy Feed"/></a>
<a href="/alist"><img src="/_img/feeds/91.png" border="0" alt="A-List Feed"/></a>
<a href="/cute"><img src="/_img/feeds/109.png" border="0" alt="Cute Feed"/></a>
<a href="/retro"><img src="/_img/feeds/110.png" border="0" alt="Retro Feed"/></a>
<a href="/geeky"><img src="/_img/feeds/108.png" border="0" alt="Geeky Feed"/></a>
<a href="/lgbt"><img src="/_img/feeds/55.png" border="0" alt="LGBT Feed"/></a>
</div>
</div>
</div>
<div id="topHeaderWarper2">
<div id="mainmenu" class="mgmenu_container">
<ul class="mgmenu">
<li class="mgmenu_button"></li> 
<div id="mainmenu_logo"><a href="/"><img src="/_img/edailygr_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία" height="33"/></a></div>
<li><span class="mgmenu_drop"><a href="/news">Ειδήσεις</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/news/60123/fylakish-4-mhnwn-ston-antwnh-remo" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Φυλάκιση 4 μηνών στον Αντώνη Ρέμο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/5269a6d2f40cb9eec47ad8cff6ac0dd1_L.jpg" alt="Φυλάκιση 4 μηνών στον Αντώνη Ρέμο" border="0" class="cropimg lazyload">
</a>
<a href="/news/60049/gennhthhke-ygiestato-koritsaki-apo-waria-poy-eixan-katapsyxthei-18-xronia-prin" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γεννήθηκε υγιέστατο κοριτσάκι από ωάρια που είχαν καταψυχθεί 18 χρόνια πριν!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/koritsakikina2.jpg" alt="Γεννήθηκε υγιέστατο κοριτσάκι από ωάρια που είχαν καταψυχθεί 18 χρόνια πριν!" border="0" class="cropimg lazyload">
</a>
<a href="/news/60062/to-ste-aperripse-ta-asfalistika-metra-toy-mega-gia-tis-thleoptikes-adeies" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>MEDIA</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το ΣτΕ απέρριψε τα ασφαλιστικά μέτρα του Mega για τις τηλεοπτικές άδειες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mega_asfalistika_1.jpg" alt="Το ΣτΕ απέρριψε τα ασφαλιστικά μέτρα του Mega  για τις τηλεοπτικές άδειες" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3">
<div class="str8">
<a href="/news" class="more">ΠΕΡΙΣΣΟΤΕΡΕΣ ΕΙΔΗΣΕΙΣ</a>
<a href="/news/60059/apelyse-ton-gio-toy-apo-to-estiatorio-gia-kati-poy-egrapse-panw-sthn-apodeiksh-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Aπέλυσε τον γιο του από το εστιατόριο για κάτι που έγραψε πάνω στην απόδειξη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/estiatorio_yios_main.jpg" alt="Aπέλυσε τον γιο του από το εστιατόριο για κάτι που έγραψε πάνω στην απόδειξη" border="0" class="cropimg lazyload">
</a>
<a href="/news/60047/anodos-ths-thermokrasias-aithrios-o-kairos" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΑΙΡΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Άνοδος της θερμοκρασίας - Αίθριος ο καιρός </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/iliotherapeiailios.jpg" alt="Άνοδος της θερμοκρασίας - Αίθριος ο καιρός " border="0" class="cropimg lazyload">
</a>
<a href="/news/60118/kovoyn-to-kentriko-deltio-eidhsewn-ep-aoriston-sto-mega" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>MEDIA</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Κόβουν το κεντρικό δελτίο ειδήσεων επ` αόριστον στο Mega</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/megagegonota2.jpg" alt="Κόβουν το κεντρικό δελτίο ειδήσεων επ` αόριστον στο Mega" border="0" class="cropimg lazyload">
</a>
<a href="/news/60109/sev-o-peiraias-mporei-na-parei-th-thesh-toy-londinoy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΟΙΚΟΝΟΜΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">ΣΕΒ: O Πειραιάς μπορεί να πάρει τη θέση του Λονδίνου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/peiraias1.jpg" alt="ΣΕΒ: O Πειραιάς μπορεί να πάρει τη θέση του Λονδίνου" border="0" class="cropimg lazyload">
</a>
<a href="/news/60072/sxedon-etoimh-h-ionia-odos-1-wra-kai-40-lepta-antirrio-giannena-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Σχεδόν έτοιμη η Ιόνια Οδός: 1 ώρα και 40 λεπτά Αντίρριο - Γιάννενα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ionia_odos_1.jpg" alt="Σχεδόν έτοιμη η Ιόνια Οδός: 1 ώρα και 40 λεπτά Αντίρριο - Γιάννενα" border="0" class="cropimg lazyload">
</a>
</div>
</div>
<div class="col_12 inlineMenu">
<a href="/news">ΤΕΛΕΥΤΑΙΑ ΝΕΑ</a>
<a href="/ellada">ΕΛΛΑΔΑ</a>
<a href="/kosmos">ΚΟΣΜΟΣ</a>
<a href="/politiki">ΠΟΛΙΤΙΚΗ</a>
<a href="/oikonomia">ΟΙΚΟΝΟΜΙΑ</a>
<a href="/sports">ΑΘΛΗΤΙΣΜΟΣ</a>
<a href="/epistimi">ΕΠΙΣΤΗΜΗ</a>
<a href="/politismos">ΠΟΛΙΤΙΣΜΟΣ</a>
<a href="http://feeds.feedburner.com/eradioblog" target="_blank">RSS FEED</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/themata">Θέματα</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="http://www.e-daily.gr/themata/60015/kalokairi-sthn-athhna" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΑΦΙΕΡΩΜΑΤΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Μεγάλο αφιέρωμα: Καλοκαίρι στην Αθήνα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/003726-01-view-from-plunge-pool1.jpg" alt="Μεγάλο αφιέρωμα: Καλοκαίρι στην Αθήνα" border="0" class="cropimg lazyload">
</a>
<a href="/themata/28049/ta-mpania-toy-laoy-mia-retro-kalokairinh-ellada-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΙΣΤΟΡΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Τα μπάνια του λαού: Μια ρετρό καλοκαιρινή Ελλάδα </div>
<img data-src="http://cdn.e-daily.gr/repository/2014/mpanialaou.jpg" alt="Τα μπάνια του λαού: Μια ρετρό καλοκαιρινή Ελλάδα " border="0" class="cropimg lazyload">
</a>
<a href="/themata/60095/san-shmera-h-toyrkia-apagoreyei-thn-didaskalia-ths-ellhnikhs-glwssas-se-imvro-kai-tenedo-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΑΔΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Σαν σήμερα: Η Τουρκία απαγορεύει την διδασκαλία της ελληνικής γλώσσας σε Ίμβρο και Τένεδο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sxoleio_imvros_1.jpg" alt="Σαν σήμερα: Η Τουρκία απαγορεύει την διδασκαλία της ελληνικής γλώσσας σε Ίμβρο και Τένεδο" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΑ ΘΕΜΑΤΑ</a>
<a href="/themata/60099/spartawar-of-empires-to-pio-ethistiko-dwrean-paixnidi-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΕΧΝΟΛΟΓΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Sparta:War of Empires. Το πιο εθιστικό δωρεάν παιχνίδι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/02-sparta-war-of-empires-wallpaper.jpg" alt="Sparta:War of Empires. Το πιο εθιστικό δωρεάν παιχνίδι" border="0" class="cropimg lazyload">
</a>
<a href="/themata/60087/vazeis-stoixhma-oi-kalyteres-stoixhmatikes-provlepseis-ths-evdomadas" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΑΘΛΗΤΙΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Βάζεις στοίχημα; Οι καλύτερες στοιχηματικές προβλέψεις της εβδομάδας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/edaily_bazeisstixima1.jpg" alt="Βάζεις στοίχημα; Οι καλύτερες στοιχηματικές προβλέψεις της εβδομάδας" border="0" class="cropimg lazyload">
</a>
<a href="/themata/59998/h-76xronh-daskala-polemikwn-texnwn-poy-trelane-to-diadiktyo-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Η 76χρονη δασκάλα πολεμικών τεχνών που τρέλανε το διαδίκτυο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/kalari meenakshi1.jpg" alt="Η 76χρονη δασκάλα πολεμικών τεχνών που τρέλανε το διαδίκτυο" border="0" class="cropimg lazyload">
</a>
<a href="/themata/59867/to-yperpolyteles-katafygio-disekatommyrioyxwn-sthn-germania-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Το υπερπολυτελές καταφύγιο δισεκατομμυριούχων στην Γερμανία</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/299998D700000578-3123296-image-a-12_1434256042642.jpg" alt="Το υπερπολυτελές καταφύγιο δισεκατομμυριούχων στην Γερμανία" border="0" class="cropimg lazyload">
</a>
<a href="/themata/60024/kathysterhse-h-pthsh-sas-deite-ta-dikaiwmata-sas" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΑΦΙΕΡΩΜΑΤΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Καθυστέρησε η πτήση σας; Δείτε τα δικαιώματα σας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/aeroporika.jpg" alt="Καθυστέρησε η πτήση σας; Δείτε τα δικαιώματα σας" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
<a href="/afieromata">ΑΦΙΕΡΩΜΑΤΑ</a>
<a href="/eikones">ΩΡΑΙΕΣ ΕΙΚΟΝΕΣ</a>
<a href="/istorika">ΙΣΤΟΡΙΚΑ</a>
<a href="/gay-lesbian">LGBT</a>
<a href="/tech">ΤΕΧΝΟΛΟΓΙΑ</a>
<a href="/periballon">ΠΕΡΙΒΑΛΛΟΝ</a>
<a href="/architect">ΑΡΧΙΤΕΚΤΟΝΙΚΗ</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/viral">Viral</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/viral/60114/apisteytes-periptwseis-epithesewn-apo-karxaries-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Απίστευτες περιπτώσεις επιθέσεων από καρχαρίες</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/shark-322342.jpg" alt="Απίστευτες περιπτώσεις επιθέσεων από καρχαρίες" border="0" class="cropimg lazyload">
</a>
<a href="/viral/60121/parathse-to-agori-ths-gia-enan-ploysio-agnwsto-deite-to-apisteyto-video-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Παράτησε το αγόρι της για έναν πλούσιο άγνωστο: Δείτε τo απίστευτo video</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/farsa.jpg" alt="Παράτησε το αγόρι της για έναν πλούσιο άγνωστο: Δείτε τo απίστευτo video" border="0" class="cropimg lazyload">
</a>
<a href="/viral/60127/sifnos-asygkrathtoi-sthn-paralia-h-kylie-minogue-kai-o-28xronos-syntrofos-ths-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Σίφνος: Ασυγκράτητοι στην παραλία η Kylie Minogue και ο 28χρονος σύντροφός της</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/minogue_sifnos_0.jpg" alt="Σίφνος: Ασυγκράτητοι στην παραλία η Kylie Minogue και ο 28χρονος σύντροφός της" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΟ VIRAL</a>
<a href="/viral/60093/osoi-ton-akoloythoysan-sto-snapchat-eidan-thn-apisteyth-peripeteia-toy-live-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Όσοι τον ακολουθούσαν στο Snapchat είδαν την απίστευτη περιπέτειά του live</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/unluckysnapchat.jpg" alt="Όσοι τον ακολουθούσαν στο Snapchat είδαν την απίστευτη περιπέτειά του live" border="0" class="cropimg lazyload">
</a>
<a href="/viral/60122/epiasan-ton-giatro-na-to-kanei-mesa-sto-iatreio-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΑΡΑΞΕΝΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Έπιασαν τον γιατρό να το κάνει μέσα στο ιατρείο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/nintchdbpict000247901974.jpg" alt="Έπιασαν τον γιατρό να το κάνει μέσα στο ιατρείο" border="0" class="cropimg lazyload">
</a>
<a href="/viral/60091/an-psaxnete-monimh-doyleia-ena-xwrio-me-10000-theseis-ergasias-exei-th-lysh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΚΟΣΜΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Αν ψάχνετε μόνιμη δουλειά, ένα χωριό με 10000 θέσεις εργασίας έχει τη λύση!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/nz-village-232.jpg" alt="Αν ψάχνετε μόνιμη δουλειά, ένα χωριό με 10000 θέσεις εργασίας έχει τη λύση!" border="0" class="cropimg lazyload">
</a>
<a href="/viral/60117/posa-zeygaria-kanoyn-telika-seks-thn-prwth-nyxta-toy-gamoy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>SEXY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Πόσα ζευγάρια κάνουν τελικά σeξ την πρώτη νύχτα του γάμου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zeugari_proti_nyxta1.jpg" alt="Πόσα ζευγάρια κάνουν τελικά σeξ την πρώτη νύχτα του γάμου" border="0" class="cropimg lazyload">
</a>
<a href="/viral/60112/kwstetsos-to-gymno-forema-ths-foyreira-einai-antigrafh-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>TABLOID</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Κωστέτσος: Το γυμνό φόρεμα της Φουρέιρα είναι αντιγραφή</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/foureira_forema_1.jpg" alt="Κωστέτσος: Το γυμνό φόρεμα της Φουρέιρα είναι αντιγραφή" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
<a href="/omg">OMG</a>
<a href="/lol">LOL</a>
<a href="/tabloid">TABLOID</a>
<a href="/sexy">SEXY</a>
<a href="/zoa">ΖΩΑ</a>
<a href="/media">MEDIA</a>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/quiz">Κουίζ</a></span>
<div class="dropdown_fullwidth menuPosts">
<div class="mgmenu_width">
<div class="col_9 three">
<a href="/quiz/60034/mporeis-na-entopiseis-to-anekshghto-antikeimeno-sth-fwtografia-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείς να εντοπίσεις το ανεξήγητο αντικείμενο στη φωτογραφία;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/d3770de5-8e91-407f-88dc-f50c83a52139.jpg" alt="Μπορείς να εντοπίσεις το ανεξήγητο αντικείμενο στη φωτογραφία;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/59147/to-pio-dyskolo-paixnidi-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Το πιο δύσκολο παιχνίδι! </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/spot_differencegame.jpg" alt="Το πιο δύσκολο παιχνίδι! " border="0" class="cropimg lazyload">
</a>
<a href="/quiz/59416/poio-nhsi-vlepeis-sthn-eikona-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ποιο νησί βλέπεις στην εικόνα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/nisia-sifnos.jpg" alt="Ποιο νησί βλέπεις στην εικόνα;" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_3 str8">
<a href="/viral" class="more">ΠΕΡΙΣΣΟΤΕΡΑ QUIZ</a>
<a href="/quiz/59594/mporeite-na-vreite-ta-akrwnyma-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείτε να βρείτε τα ακρώνυμα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/enhanced-8988-1466588726-1.jpg" alt="Μπορείτε να βρείτε τα ακρώνυμα;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/59591/mporeis-na-vreis-poio-epitrapezio-fainetai-sthn-eikona-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείς να βρεις ποιο επιτραπέζιο φαίνεται στην εικόνα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/stratego-43.jpg" alt="Μπορείς να βρεις ποιο επιτραπέζιο φαίνεται στην εικόνα;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/59870/poso-kala-ksereis-ta-ethnika-nomismata-twn-kratwn-ths-eyrwphs-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Πόσο καλά ξέρεις τα εθνικά νομίσματα των κρατών της Ευρώπης;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/money-coins-770x285.jpg" alt="Πόσο καλά ξέρεις τα εθνικά νομίσματα των κρατών της Ευρώπης;" border="0" class="cropimg lazyload">
</a>
<a href="/quiz/59501/poio-zwdio-einai-to-allo-soy-miso-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ποιο ζώδιο είναι το άλλο σου μισό;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/446612-zodiac-signs-new.jpg" alt="Ποιο ζώδιο είναι το άλλο σου μισό;" border="0" class="cropimg lazyload">
</a>
<a href="http://www.e-daily.gr/quiz/59251/poios-einai-o-xeiroteros-prwthypoyrgos-kata-thn-diarkeia-ths-krishs-vote" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΨΗΦΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Ποιος είναι ο χειρότερος πρωθυπουργός κατά την διάρκεια της κρίσης;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/prothyp_3.jpg" alt="Ποιος είναι ο χειρότερος πρωθυπουργός κατά την διάρκεια της κρίσης;" border="0" class="cropimg lazyload">
</a>
</div>
<div class="col_12 inlineMenu">
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/life">Life</a></span>
<div class="dropdown_fullwidth mgmenu_tabs">
<div class="mgmenu_width">
<ul class="mgmenu_tabs_nav">
<li><a href="/life" rel="section2_0" class="current">ΤΕΛΕΥΤΑΙΑ θΕΜΑΤΑ</a></li>
<li><a href="/food" rel="section2_1">FOOD</a></li>
<li><a href="/body-mind" rel="section2_2">BODY+MIND</a></li>
<li><a href="/spiti" rel="section2_3">ΣΠΙΤΙ</a></li>
<li><a href="/travel" rel="section2_4">ΤΑΞΙΔΙΑ</a></li>
<li><a href="/style" rel="section2_5">STYLE</a></li>
<li><a href="/family" rel="section2_6">FAMILY</a></li>
<li><a href="/sxeseis" rel="section2_7">ΣΧΕΣΕΙΣ</a></li>
<li><a href="/zodia" rel="section2_8">ΖΩΔΙΑ</a></li>
</ul>
<div class="mgmenu_tabs_panels">
<div id="section2_0" class="menuPosts">
<a href="/life/60000/h-kompsothta-synanta-thn-aythentikothta-sth-nea-seira-royxwn-ths-access-pics-kerdiste" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">H κομψότητα συναντά την αυθεντικότητα στη νέα σειρά ρούχων της Access</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/accessfashion.jpg" alt="H κομψότητα συναντά την αυθεντικότητα στη νέα σειρά ρούχων της Access" border="0" class="cropimg lazyload">
</a>
<a href="/life/60057/ta-zwdia-shmera-1-ioylioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 1 Ιουλίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/gorgona24.jpg" alt="Τα ζώδια σήμερα: 1 Ιουλίου" border="0" class="cropimg lazyload">
</a>
<a href="/life/59869/mporeis-na-vreis-to-froyto-me-thn-perissoterh-zaxarh-quiz" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>QUIZ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-doc-text"></span></div>
<div class="menuItem_title">Μπορείς να βρεις το φρούτο με την περισσότερη ζάχαρη;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/fruitmainpic.jpg" alt="Μπορείς να βρεις το φρούτο με την περισσότερη ζάχαρη;" border="0" class="cropimg lazyload">
</a>
<a href="/life/60089/h-anasa-toy-drakoy-einai-to-pio-hype-glyko-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Η... ανάσα του δράκου είναι το πιο hype γλυκό!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/dragonsbreath2.jpg" alt="Η... ανάσα του δράκου είναι το πιο hype γλυκό!" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_1" class="menuPosts mgmenu_tabs_hide">
<a href="/life/42979/syntagh-gia-gemista-opws-tha-ta-etrwge-kai-h-theanw-fwtioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Συνταγή για γεμιστά, όπως θα τα έτρωγε και η Θεανώ Φωτίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2015/gemistatheano.jpg" alt="Συνταγή για γεμιστά, όπως θα τα έτρωγε και η Θεανώ Φωτίου" border="0" class="cropimg lazyload">
</a>
<a href="/life/59803/giati-xtypame-ta-karpoyzia-prin-ta-agorasoyme" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Γιατί χτυπάμε τα καρπούζια πριν τα αγοράσουμε;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/karpouzia45.jpeg" alt="Γιατί χτυπάμε τα καρπούζια πριν τα αγοράσουμε;" border="0" class="cropimg lazyload">
</a>
<a href="/life/59199/ftiakste-pagakia-me-loyloydia-kai-froyta-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Φτιάξτε παγάκια με λουλούδια και φρούτα </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/icecubesflowers2.jpg" alt="Φτιάξτε παγάκια με λουλούδια και φρούτα " border="0" class="cropimg lazyload">
</a>
<a href="/life/59438/ti-prepei-na-trwme-otan-kanei-poly-zesth" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FOOD</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τι πρέπει να τρώμε όταν κάνει πολύ ζέστη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/summereat3.jpg" alt="Τι πρέπει να τρώμε όταν κάνει πολύ ζέστη" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_2" class="menuPosts mgmenu_tabs_hide">
<a href="/life/60026/epiteloys-h-stutikh-dysleitoyrgia-therapeyetai-se-ellhniko-nosokomeio" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Επιτέλους η στuτική δυσλειτουργία θεραπεύεται σε ελληνικό νοσοκομείο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/stitidus.jpg" alt="Επιτέλους η στuτική δυσλειτουργία θεραπεύεται σε ελληνικό νοσοκομείο" border="0" class="cropimg lazyload">
</a>
<a href="/life/57246/ti-symvainei-ston-organismo-sas-otan-stamatate-na-xrhsimopoieite-kallyntika" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τι συμβαίνει στον οργανισμό σας όταν σταματάτε να χρησιμοποιείτε καλλυντικά;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/wearmakeup3.jpg" alt="Τι συμβαίνει στον οργανισμό σας όταν σταματάτε να χρησιμοποιείτε καλλυντικά;" border="0" class="cropimg lazyload">
</a>
<a href="/life/59802/o-kalyteros-tropos-gia-na-vgalete-ta-agkathia-toy-axinoy-apo-to-podi-sas" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ο καλύτερος τρόπος για να βγάλετε τα αγκάθια του αχινού από το πόδι σας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/patimaaxino.jpg" alt="Ο καλύτερος τρόπος για να βγάλετε τα αγκάθια του αχινού από το πόδι σας" border="0" class="cropimg lazyload">
</a>
<a href="/life/59951/agaphste-thn-mpyrokoilia-sas-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>BODY+MIND</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">«Αγαπήστε την μπυροκοιλιά σας» </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mpyrokoilia_1.jpg" alt="«Αγαπήστε την μπυροκοιλιά σας» " border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_3" class="menuPosts mgmenu_tabs_hide">
<a href="/life/59865/ta-9-fyta-poy-diwxnoyn-ta-koynoypia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα 9 φυτά που διώχνουν τα κουνούπια</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/calendula-7214257634_90c7a674de_z1.jpg" alt="Τα 9 φυτά που διώχνουν τα κουνούπια" border="0" class="cropimg lazyload">
</a>
<a href="/life/59530/poia-einai-ta-fyta-poy-diwxnoyn-ta-koynoypia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ποια είναι τα φυτά που διώχνουν τα κουνούπια</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/kounoupia8.jpg" alt="Ποια είναι τα φυτά που διώχνουν τα κουνούπια" border="0" class="cropimg lazyload">
</a>
<a href="/life/59123/ftiakste-oneiropagides-gia-to-kalokairi-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Φτιάξτε ονειροπαγίδες για το καλοκαίρι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/oneiropad7.jpg" alt="Φτιάξτε ονειροπαγίδες για το καλοκαίρι" border="0" class="cropimg lazyload">
</a>
<a href="/life/59536/ta-10-proionta-poy-prepei-na-exoyn-osoi-misoyn-th-zesth-to-kalokairi-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΠΙΤΙ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Τα 10 προϊόντα που πρέπει να έχουν όσοι μισούν τη ζέστη το καλοκαίρι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/feelinghot.jpg" alt="Τα 10 προϊόντα που πρέπει να έχουν όσοι μισούν τη ζέστη το καλοκαίρι" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_4" class="menuPosts mgmenu_tabs_hide">
<a href="/life/59853/vrhkame-ta-fthnotera-aeroporika-gia-4-top-ellhnika-nhsia-list" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-th-list"></span></div>
<div class="menuItem_title">Βρήκαμε τα φτηνότερα αεροπορικά για 4 Top ελληνικά νησιά!</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/Santorinilate.jpg" alt="Βρήκαμε τα φτηνότερα αεροπορικά για 4 Top ελληνικά νησιά!" border="0" class="cropimg lazyload">
</a>
<a href="/life/59676/to-neo-ritz-sto-parisi-einai-pio-entypwsiako-apo-pote-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">To νέο Ritz στο Παρίσι είναι πιο εντυπωσιακό από ποτέ</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ritz-423123.jpg" alt="To νέο Ritz στο Παρίσι είναι πιο εντυπωσιακό από ποτέ" border="0" class="cropimg lazyload">
</a>
<a href="http://www.e-daily.gr/life/59500/vrhkame-tis-kalyteres-prosfores-gia-dihmeres-odikes-apodraseis-kerdiste" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Βρήκαμε τις καλύτερες προσφορές για διήμερες οδικές αποδράσεις</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/apodrasisd.jpg" alt="Βρήκαμε τις καλύτερες προσφορές για διήμερες οδικές αποδράσεις" border="0" class="cropimg lazyload">
</a>
<a href="/life/59503/to-vathytero-faraggi-toy-kosmoy-einai-ellhniko-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΤΑΞΙΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Το βαθύτερο φαράγγι του κόσμου είναι ελληνικό</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/vikos-main.jpg" alt="Το βαθύτερο φαράγγι του κόσμου είναι ελληνικό" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_5" class="menuPosts mgmenu_tabs_hide">
<a href="/life/59365/ti-na-pareis-mazi-ta-aparaithta-royxa-kai-aksesoyar-twn-diakopwn-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Τι να πάρεις μαζί: Τα απαραίτητα ρούχα και αξεσουάρ των διακοπών</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/432553.jpg" alt="Τι να πάρεις μαζί: Τα απαραίτητα ρούχα και αξεσουάρ των διακοπών" border="0" class="cropimg lazyload">
</a>
<a href="/life/59038/neos-diagwnismosoi-dyo-ellhnides-sxediastries-poy-ftiaxnoyn-ta-foremata-toy-kalokairioy-pics-kerdiste" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Νέος διαγωνισμός:Οι δύο Ελληνίδες σχεδιάστριες που φτιάχνουν τα φορέματα του καλοκαιριού</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/akiramushi899.jpg" alt="Νέος διαγωνισμός:Οι δύο Ελληνίδες σχεδιάστριες που φτιάχνουν τα φορέματα του καλοκαιριού" border="0" class="cropimg lazyload">
</a>
<a href="/life/59498/ayto-einai-to-omorfotero-kragion-poy-exete-dei-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>STYLE</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Αυτό είναι το ομορφότερο κραγιόν που έχετε δει</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/beautifulkragion2.jpg" alt="Αυτό είναι το ομορφότερο κραγιόν που έχετε δει" border="0" class="cropimg lazyload">
</a>
<a href="http://www.e-daily.gr/life/59373/neos-diagwnismos-vinge-project-kerdiste-thn-apolyth-boho-tsanta-toy-kalokairioy-pics-kerdiste" target="_blank" class="absolute-middle">
<div class="menuItem_header"><span class=alert>ΚΕΡΔΙΣΤΕ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-ticket"></span></div>
<div class="menuItem_title">Δείτε τους νικητές: Κερδίστε την απόλυτη boho τσάντα του καλοκαιριού</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/vinge3.jpg" alt="Δείτε τους νικητές: Κερδίστε την απόλυτη boho τσάντα του καλοκαιριού" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_6" class="menuPosts mgmenu_tabs_hide">
<a href="/life/59436/o-mpampas-poy-exase-127-kila-gia-ena-monadiko-logo-pics" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-camera-outline"></span></div>
<div class="menuItem_title">Ο μπαμπάς που έχασε 127 κιλά για ένα μοναδικό λόγο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mpampas_diaita_0.jpg" alt="Ο μπαμπάς που έχασε 127 κιλά για ένα μοναδικό λόγο" border="0" class="cropimg lazyload">
</a>
<a href="/life/58373/enas-daskalos-dinei-3-polytimes-symvoyles-gia-ta-paidia-sas" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ένας δάσκαλος δίνει 3 πολύτιμες συμβουλές για τα παιδιά σας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/jonassxoleio23.jpg" alt="Ένας δάσκαλος δίνει 3 πολύτιμες συμβουλές για τα παιδιά σας" border="0" class="cropimg lazyload">
</a>
<a href="/life/26710/xronia-polla-stoys-mpampades-me-moysikh-kai-xioymor-pics-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-picture"></span></div>
<div class="menuItem_title">Χρόνια πολλά στους μπαμπάδες με μουσική και χιούμορ! </div>
<img data-src="http://cdn.e-daily.gr/repository/2014/fathersday7.jpg" alt="Χρόνια πολλά στους μπαμπάδες με μουσική και χιούμορ! " border="0" class="cropimg lazyload">
</a>
<a href="/life/59216/to-sygkinhtiko-vinteo-gia-toys-mpampades-poy-tha-sas-aggiksei-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>FAMILY</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Το συγκινητικό βίντεο για τους μπαμπάδες που θα σας αγγίξει</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mpampasdove2.jpg" alt="Το συγκινητικό βίντεο για τους μπαμπάδες που θα σας αγγίξει" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_7" class="menuPosts mgmenu_tabs_hide">
<a href="/life/59830/ti-leei-enas-antras-kai-ti-ennoei-pragmatika" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Tι λέει ένας άντρας και τι εννοεί πραγματικά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tileneantres.jpg" alt="Tι λέει ένας άντρας και τι εννοεί πραγματικά" border="0" class="cropimg lazyload">
</a>
<a href="/life/59900/h-episthmh-kserei-pes-moy-me-posoys-koimhthhkes-na-soy-pw-an-tha-xwriseis" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η επιστήμη ξέρει: Πες μου με πόσους κοιμήθηκες, να σου πω αν θα χωρίσεις</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/lovemake.jpg" alt="Η επιστήμη ξέρει: Πες μου με πόσους κοιμήθηκες, να σου πω αν θα χωρίσεις" border="0" class="cropimg lazyload">
</a>
<a href="/life/59579/thes-kalo-seks-mesa-sth-sxesh-des-ti-prepei-na-kaneis" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Θες καλό σεξ μέσα στη σχέση; Δες τι πρέπει να κάνεις</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/couple.jpg" alt="Θες καλό σεξ μέσα στη σχέση; Δες τι πρέπει να κάνεις" border="0" class="cropimg lazyload">
</a>
<a href="/life/58780/terma-oi-koiliakoi-fetes-oi-gynaikes-protimoyn-o-andras-na-exei-koilitsa" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΧΕΣΕΙΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τέρμα οι κοιλιακοί φέτες: Οι γυναίκες προτιμούν ο άνδρας να έχει κοιλίτσα</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/menbelly.jpg" alt="Τέρμα οι κοιλιακοί φέτες: Οι γυναίκες προτιμούν ο άνδρας να έχει κοιλίτσα" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section2_8" class="menuPosts mgmenu_tabs_hide">
<a href="/life/59983/ta-zwdia-shmera-30-ioynioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Tα ζώδια σήμερα: 30 Ιουνίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/swim5.jpg" alt="Tα ζώδια σήμερα: 30 Ιουνίου" border="0" class="cropimg lazyload">
</a>
<a href="/life/59902/ta-zwdia-shmera-29-ioynioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 29 Ιουνίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/eyesrings.jpg" alt="Τα ζώδια σήμερα: 29 Ιουνίου" border="0" class="cropimg lazyload">
</a>
<a href="/life/59818/ta-zwdia-shmera-28-ioynioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 28 Ιουνίου </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/womandrinks.jpg" alt="Τα ζώδια σήμερα: 28 Ιουνίου " border="0" class="cropimg lazyload">
</a>
<a href="/life/59672/ta-zwdia-shmera-27-ioynioy" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΖΩΔΙΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τα ζώδια σήμερα: 27 Ιουνίου</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/fridazodia.jpg" alt="Τα ζώδια σήμερα: 27 Ιουνίου" border="0" class="cropimg lazyload">
</a>
</div>
</div>
</div>
</div>
</li> 
<li><span class="mgmenu_drop"><a href="/entertainment">Ψυχαγωγία</a></span>
<div class="dropdown_fullwidth mgmenu_tabs">
<div class="mgmenu_width">
<ul class="mgmenu_tabs_nav">
<li><a href="/agenda" rel="section5_1" class="current">ATZENTA ΣΗΜΕΡΑ</a></li>
<li><a href="/music" rel="section5_2">ΜΟΥΣΙΚΗ</a></li>
<li><a href="/cinema" rel="section5_3">ΣΙΝΕΜΑ</a></li>
<li><a href="/theater" rel="section5_4">ΘΕΑΤΡΟ+ΧΟΡΟΣ</a></li>
<li><a href="/arts" rel="section5_5">ΕΙΚΑΣΤΙΚΑ</a></li>
<li><a href="/city" rel="section5_6">ΠΟΛΗ</a></li>
<li><a href="/nightlife" rel="section5_7">ΝΑΪΤ ΛΑΪΦ</a></li>
</ul>
<div class="mgmenu_tabs_panels">
<div id="section5_1" class="menuPosts">
<a href="/entertainment/60110/oi-tainies-ths-evdomadas-h-megalh-twn-skhnothetwn-sxolh-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Οι ταινίες της εβδομάδας: Η μεγάλη των σκηνοθετών σχολή</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tbfg1.jpg" alt="Οι ταινίες της εβδομάδας: Η μεγάλη των σκηνοθετών σχολή" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/60106/daphne-and-the-fuzz-live" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Daphne and the Fuzz Live</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/daphneandthefuzzrockwave.jpg" alt="Daphne and the Fuzz Live" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/60105/oidipoys-tyrannos" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Οιδίπους Τύραννος</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/rimastouminas.jpg" alt="Οιδίπους Τύραννος" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/60104/creative-economy-fair-and-forum-apo-thn-idialogue" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Creative Economy Fair and Forum από την iDialogue</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/creativeeconomyfair.jpg" alt="Creative Economy Fair and Forum από την iDialogue" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_2" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/60021/8-winds-apo-to-sokratis-sinopoulos-quartet" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">8 Winds από το Sokratis Sinopoulos Quartet</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/8windssinopoulos.jpg" alt="8 Winds από το Sokratis Sinopoulos Quartet" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/60020/saristra-festival-2016" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Saristra Festival 2016</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/saristrafest2016.jpg" alt="Saristra Festival 2016" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/60017/o-giannhs-xaroylhs-gia-mia-synaylia-sta-xania" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΛΛΗΝΙΚΗ ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Ο Γιάννης Χαρούλης για μια συναυλία στα Χανιά </div>
<img data-src="http://cdn.e-daily.gr/repository/2016/xaroulisxania16.jpg" alt="Ο Γιάννης Χαρούλης για μια συναυλία στα Χανιά " border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/59995/festival-athhnwn-moysikh-ston-khpo" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΜΟΥΣΙΚΗ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Φεστιβάλ Αθηνών - Μουσική στον Κήπο</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mousikistonkipo.jpg" alt="Φεστιβάλ Αθηνών - Μουσική στον Κήπο" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_3" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/59574/blast-from-the-past-stis-tainies-ths-evdomadas-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Blast from the past στις ταινίες της εβδομάδας</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/independencedayresurgence.jpg" alt="Blast from the past στις ταινίες της εβδομάδας" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/59230/6o-athens-open-air-film-festival" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">6ο Athens Open Air Film Festival</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/6athensopenair.jpg" alt="6ο Athens Open Air Film Festival" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/59166/ti-tha-doyme-stis-kinhmatografikes-aithoyses-ayth-thn-evdomada-video" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class="icon-play-circled2"></span></div>
<div class="menuItem_title">Τι θα δούμε στις κινηματογραφικές αίθουσες αυτή την εβδομάδα;</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/florencefoster.jpg" alt="Τι θα δούμε στις κινηματογραφικές αίθουσες αυτή την εβδομάδα;" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/59088/now-politistiko-trihmero-sto-dhmotiko-theatro-peiraia" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΣΙΝΕΜΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">NOW: πολιτιστικό τριήμερο στο Δημοτικό Θέατρο Πειραιά</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/nowpireas.jpg" alt="NOW: πολιτιστικό τριήμερο στο Δημοτικό Θέατρο Πειραιά" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_4" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/60103/to-psema-toy-florian-zeller" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Το Ψέμα, του Florian Zeller</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/topsemaatheridiskaridi.jpg" alt="Το Ψέμα, του Florian Zeller" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/60102/hayali-sans-toi-je-tombe-xwris-esena-peftw" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΧΟΡΟΣ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Hayali – Sans toi, je tombe / Χωρίς εσένα, πέφτω…</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/sanstoijetombe.jpg" alt="Hayali – Sans toi, je tombe / Χωρίς εσένα, πέφτω…" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/60101/h-antigonh-toy-sofoklh-apo-ton-stathh-livathino" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Η Αντιγόνη του Σοφοκλή από τον Στάθη Λιβαθινό</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/antigonilivathinos.jpg" alt="Η Αντιγόνη του Σοφοκλή από τον Στάθη Λιβαθινό" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/57248/to-spiti-ths-mpernarnta-almpa-epistrefei-sto-vrysaki" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΘΕΑΤΡΟ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Τo σπίτι της Μπερνάρντα Άλμπα επιστρέφει στο Βρυσάκι</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mpernarntaalmpa2016.jpg" alt="Τo σπίτι της Μπερνάρντα Άλμπα επιστρέφει στο Βρυσάκι" border="0" class="cropimg lazyload">
</a>
</div>
<div id="section5_5" class="mgmenu_tabs_hide menuPosts">
<a href="/entertainment/60083/ekthesh-aptikwn-eikonwn-gia-atoma-me-tyflwsh-kai-anaphria-sthn-orash-sth-thessalonikh" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Έκθεση απτικών εικόνων για άτομα με τύφλωση και αναπηρία στην όραση στη Θεσσαλονίκη</div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ekthesiaptikonikonon.jpg" alt="Έκθεση απτικών εικόνων για άτομα με τύφλωση και αναπηρία στην όραση στη Θεσσαλονίκη" border="0" class="cropimg lazyload">
</a>
<a href="/entertainment/57916/aspro-mayro-omadikh-ekthesh-sthn-dept-art-gallery" target="_parent" class="absolute-middle">
<div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
<div class="menuItem_spider absolute-center"><span class=""></span></div>
<div class="menuItem_title">Άσπρο <=> Μαύρο: ομαδική έκθεση στην Depôt Art Gallery</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/aspromaurodepot.jpg" alt="Άσπρο <=> Μαύρο: ομαδική έκθεση στην Depôt Art Gallery" border="0" class="cropimg lazyload">
</a>

<a href="/entertainment/55650/occupy-atopos-thedreamer" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΕΙΚΑΣΤΙΚΑ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">Οccupy Atopos: #Τhe_Dreamer</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/occupyatoposthedreamer.jpg" alt="Οccupy Atopos: #Τhe_Dreamer" border="0" class="cropimg lazyload">
</a>

<a href="/entertainment/59925/59o-festival-filippwn" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">59ο Φεστιβάλ Φιλίππων</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/59ofestfilippon.png" alt="59ο Φεστιβάλ Φιλίππων" border="0" class="cropimg lazyload">
</a>

                        </div>
                        <div id="section5_6" class="mgmenu_tabs_hide menuPosts">
                        
<a href="/entertainment/60023/taksideyontas-ispania" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">Ταξιδεύοντας… Ισπανία</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/taksideuontasispania1.jpg" alt="Ταξιδεύοντας… Ισπανία" border="0" class="cropimg lazyload">
</a>

<a href="/entertainment/60016/seminario-a-vohtheiwn-sto-romantso" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">Σεμινάριο Α’ Βοηθειών στο Ρομάντσο</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/seminarioprotonvoithionromantso.jpg" alt="Σεμινάριο Α’ Βοηθειών στο Ρομάντσο" border="0" class="cropimg lazyload">
</a>

<a href="/entertainment/59693/video-art-festival-mhden-2016" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΦΕΣΤΙΒΑΛ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">Video Art Festival Μηδέν 2016</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/videoartfestivalmiden.jpg" alt="Video Art Festival Μηδέν 2016" border="0" class="cropimg lazyload">
</a>

<a href="/entertainment/59692/mesa-moyseio-eleytherhs-skepshs-anthrwpwn-apo-to-emst" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">Μ.Ε.Σ.Α.: Μουσείο Ελεύθερης Σκέψης Ανθρώπων από το ΕΜΣΤ</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/mesaemst.jpg" alt="Μ.Ε.Σ.Α.: Μουσείο Ελεύθερης Σκέψης Ανθρώπων από το ΕΜΣΤ" border="0" class="cropimg lazyload">
</a>

                        </div>
                        <div id="section5_7" class="mgmenu_tabs_hide menuPosts">
                        
<a href="/entertainment/59102/you-say-party-we-say-yeah-kris-riot-postriper-w-upsetter" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΞΕΝΗ ΜΟΥΣΙΚΗ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">You Say Party! We Say Yeah!: Kris Riot & Postriper w/ Upsetter</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/yspwsy.jpg" alt="You Say Party! We Say Yeah!: Kris Riot & Postriper w/ Upsetter" border="0" class="cropimg lazyload">
</a>

<a href="/entertainment/58209/cool-britannia-party-sthn-death-disco" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΝΑΙΤ ΛΑΙΦ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">Cool Britannia party στην Death Disco</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/coolbritannia1.jpg" alt="Cool Britannia party στην Death Disco" border="0" class="cropimg lazyload">
</a>

<a href="/entertainment/58120/stella-live-nteibint-single-release-party" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">Σtella Live + Nteibint Single Release Party</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/stellalive2016.jpg" alt="Σtella Live + Nteibint Single Release Party" border="0" class="cropimg lazyload">
</a>

<a href="/entertainment/57900/bananaboys-godrag" target="_parent" class="absolute-middle">
    <div class="menuItem_header"><span class=category>ΠΟΛΗ</span></div>
	<div class="menuItem_spider absolute-center"><span class=""></span></div>
	<div class="menuItem_title">BananaBoys goDRAG!</div>
    <img data-src="http://cdn.e-daily.gr/repository/2016/bananaboysgodrag.jpg" alt="BananaBoys goDRAG!" border="0" class="cropimg lazyload">
</a>

                        </div>
    
				</div>
            </div>
			</div>

		</li><!-- End Item Ψυχαγωγία -->
        
	</ul>
    
    <a href="/agenda" class="mgmenu_left nextwebchannels"><img src="/_img/eagendagr_logo.png" height="20" /></a>
    <a href="http://www.e-radio.gr" target="_blank" class="mgmenu_left nextwebchannels"><img src="/_img/eradiogr_logo.png" height="20" /></a>
    
    
	<div class="right_item">
      <div class="right_item" id="tabSearch" style="padding:12px 10px 14px 8px;"><div class="icon-search"></div></div>
		<div class="right_item noactive" style="padding:0">
        <a class="icon-twitter-bird right_item twitter" href="http://www.twitter.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px;"></a>
        <a class="icon-instagram right_item instagram" href="http://www.instagram.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px;"></a>
        <a class="icon-facebook right_item facebook" href="http://www.facebook.com/edailygr" target="_blank" style="padding:12px 0 14px 10px;"></a>
		</div>

        <div class="right_item noactive feedIcons showOnSmallMenu">
        <a href="/retro"><img alt="Retro" src="/_img/feeds/110.png" border="0" /></a>
        <a href="/omg"><img alt="OMG" src="/_img/feeds/101.png" border="0" /></a>
        <a href="/lol"><img alt="LOL" src="/_img/feeds/106.png" border="0" /></a>
        <a href="/juicy"><img alt="Juicy" src="/_img/feeds/104.png" border="0" /></a>
        <a href="/alist"><img alt="A-List" src="/_img/feeds/91.png" border="0" /></a>
        <a href="/cute"><img alt="Cute" src="/_img/feeds/109.png" border="0" /></a>
        <a href="/geeky"><img alt="Geeky" src="/_img/feeds/108.png" border="0" /></a>
        </div>

	</div>

	
</div>
<div class="mgmenu_fullwidth"></div>

    
</div>



<div id="topHeaderMobile" style="text-align:center;">
	<div class="ma-infinitypush-button"><span></span><span></span><span></span><span></span></div>
	<a href="/"><img src="/_img/mobilelogo.png" width="136" height="48" alt="E-Daily" border="0" /></a>
	<div class="right_item" id="tabSearchMobile" style="padding:12px; font-size: 18px;"><div class="icon-search"></div></div>
</div>
<div id="tabSearchMenu">
	

<form name="bigsearchboxform" id="bigsearchboxform" class="mainArea" autocomplete="off" method="post" action="/search/" onsubmit="javascript:submitsearch()">

	<input type="search" name="q" id="q" placeholder="Αναζητήστε 59174 θέματα, 998 ραδιόφωνα" value="" alt="Αναζητήστε 59174 θέματα, 998 ραδιόφωνα"/>
    <a name="bigsearchbox_Cancel" id="bigsearchbox_Cancel" href="javascript:hidesearch()" class="icon-cancel-circle"></a>

</form>

</div>



<div class="navibar">

</div>

<div id="topHeader3">
<center>

<script type="text/javascript">
( function() {
    var info = getTagInfo();
    var alias = 'edhomebillboard_display';
        alias = alias !== '' ? 'alias=' + ( alias.split("_") ? alias.split("_")[ 0 ] : alias ) + '_' + info.alias : '';
    document.write('<scr'+'ipt language="javascript1.1" src="http://' + info.domain + '.adtech.de/addyn/3.0/1370.1/4713596/0/-1/ADTECH;' + alias + ';loc=100;target=_blank;key=;grp=' + info.adgroupid + ';misc=' + new Date().getTime() + '"></scri'+'pt>');
})();</script>

</center>
</div>

<div class="mainArea">

	<div class="mainAreaA">

        <h1 class="postTitle">Privacy Policy</h1>
        <div class="postBody">
          <p>Welcome to E-Daily. This Privacy Policy is effective for E-Daily.gr, E-Daily Mobile Apps (&ldquo;E-Daily&rdquo;, &ldquo;we&rdquo;, &ldquo;us&rdquo; and/or &ldquo;our&rdquo;) and  is incorporated by reference into the E-Daily Terms of Service and End User  License Agreement. <br />
            The purpose of this Privacy Policy is to inform users of the E-Daily  Service (as defined below) about the information that we collect through the E-Daily  Service, how we use that information and the ways users can control how that  information is used or shared. The entire policy can be found below. For  information about users&rsquo; obligations and use of the E-Daily Service, please see  our Terms of Service and End User License Agreement. </p>
          <p> We will continue to evaluate this Privacy Policy and update it accordingly  when there is a change to our business practices, our users&rsquo; needs, and to  comply with regulatory and legal requirements. Please check this page  periodically for updates. If we make any material changes to this Privacy  Policy, we will post the updated Privacy Policy here and notify you by email or  by means of a notice on the Site (as defined below) and/or the Applications (as  defined below). </p>
          <p><strong>The types of information we collect</strong><br />
            When you interact with the E-Daily.gr, we may collect Personal  Information (as defined below) and other information from you as described  below. <br />
  &ldquo;Personal Information&rdquo; means information that alone or when in connection  with other information may be used to readily identify, contact, or locate you,  such as: name, address, email address or phone number. We do not consider  personally identifiable information to include information that has been  anonymized so that it does not allow a third party to easily identify a  specific individual. </p>
          <p><strong>Information you give us </strong><br />
            To enable you to enjoy certain features of the E-Daily Service, we collect  certain types of information, including Personal Information, during your  interactions with the E-Daily Service. By using the E-Daily Service, you are  authorizing us to gather, parse, and retain data related to the provision of  the E-Daily Service. </p>
          <p> For example, we collect information, including Personal Information, when  you: </p>
          <ul type="disc">
            <li>Register       to use our E-Daily Service;</li>
            <li>Login       with social networking credentials; </li>
            <li>Participate       in polls, contests, surveys or other features of the E-Daily Service, or       respond to offers or advertisements on the E-Daily Service; </li>
            <li>Communicate with us; and</li>
            <li>Sign up       to receive email newsletters.</li>
          </ul>
          <p>When you register to use the E-Daily Service, we ask you to provide certain  information, which includes your name, email address, birth year, gender, zip  code and/or other geographic information, as well as a password for your  account. By voluntarily providing us with Personal Information or other  information, you are consenting to our use of it in accordance with this  Privacy Policy. If you provide any Personal Information or other information,  you acknowledge and agree that such Personal Information may be transferred  from your current location to our offices and servers and the authorized third  parties. </p>
          <p><strong>Automatic Data Collection</strong><br />
            <em>Listening and Usage Activity</em>: We keep track of your listening and usage  activity. We collect information about stations, favorites, artists, channels,  conversations and tracks you have listened to or in which you have expressed an  interest. <br />
            <em>Social connect information</em>: When you choose to connect your social media  account to your account profile, we collect Personal Information from that  social media website. For example, when you connect your Facebook account, we  may collect the Personal Information you have made publicly available in  Facebook, such as your name, profile picture, cover photo, username, gender,  friend networks, age range, locale, friend list, and any other information you  have made public. We may also obtain other non-public information from the  social network website with your permission. </p>
          <p><em>Information about your computer or device</em>: We may also collect  information about the computer, mobile or other devices you use to access the E-Daily  Service. For example, our servers receive and record information about your  computer and browser, including potentially your IP address, device ID,  operating system, browser type, and other software or hardware information. If  you access the E-Daily Service from a mobile or other device, unless you  register, we may assign a unique device identifier for that device. </p>
          <p><em>Location-based information</em>: We do not collect your zip code, postal code,  city or country unless you choose to provide this information to us at  registration. Your general geo-location is collected at regular intervals if  you use the E-Daily Service on your mobile device or through the Site. We  collect this information to provide content and services that are customized  for your geographic area. </p>
          <p><em>Tracking technology</em>: We use what are commonly called  &quot;cookies&quot; on our Site. A cookie is a piece of information that the  computer that hosts our Site gives to your browser when you access the Site.  These cookies help us improve the E-Daily Service, provide additional  functionality to the Site and help us analyze Site usage more accurately. For  instance, our Site may set a cookie on your browser that allows you to access  the Site without needing to remember and then enter a password more than once  during a visit to the Site. We may also be able to capture other data such as searches  conducted and results, date, time, and connection speed. In all cases in which  we use cookies, we will not collect Personal Information except with your  permission. On most web browsers, you will find a &ldquo;help&rdquo; section on the  toolbar. Please refer to this section for information on how to receive  notification when you are receiving a new cookie and how to turn cookies off.  We recommend that you leave cookies turned on because they allow you to take  advantage of some of the Site&rsquo;s features. Third parties, including advertisers  and third-party advertising companies, whose products or services are  accessible or advertised via the E-Daily Service may also use cookies, and we  advise you to check their privacy policies for information about their cookies  and other privacy practices. </p>
          <p><strong>Our use of the information we collect</strong><br />
            We use the information, including Personal Information, that we collect for  the following purposes: </p>
          <ul type="disc">
            <li>To       personalize and customize the advertising and the content you see.</li>
            <li>To       improve and enhance the user experience. We use tracking information to       determine how well each page, station and/or channel performs overall       based on aggregate user demographics and traffic patterns to those pages,       stations and channels. This helps       us build a better service for you.</li>
            <li>To       fulfill your requests for certain products and services, such as sending       out electronic newsletters and enabling you to participate in polls,       contests, and boards.</li>
            <li>To send       you information about topics or content that we think will be of interest       to you based on your preferences, habits or geographic location.</li>
            <li>To send       you information about the latest developments and features on our E-Daily Service.</li>
            <li>To provide technical support.</li>
          </ul>
          <p>We may anonymize and aggregate data collected through the E-Daily Service  and use it for any purpose. </p>
          <p><strong>How the information we collect is shared</strong><br />
            We are not in the business of selling, renting or sharing Personal  Information about you with other people or nonaffiliated companies for their  direct marketing purposes. We consider this information to be a vital part of  our relationship with you. There are, however, certain circumstances in which  we may share your information, including Personal Information, with certain  third parties without further notice to you, as set forth below: </p>
          <ul type="disc">
            <li>Information       you allow us to share when you register with us, or through a subsequent       affirmative election.</li>
            <li>Information       used when we hire or partner with third parties to provide business-related       services on our behalf, such as mailing information, maintaining       databases, processing payments, sweepstakes management and prize       fulfillment, data processing, analytics, personalization and customization       of advertising, customer/support services and other products or services       that we choose to make available to our registered users. When we employ       another company to perform a function of this nature, we only provide them       with the information that they need to perform their specific function.</li>
            <li>We may       share, transfer or sell your information in connection with a merger,       acquisition, financing due diligence, reorganization, bankruptcy,       receivership, sale of company assets, or transition of service to another       provider. We cannot control how such entities may use or disclose such       information.</li>
            <li>We may       share information with our subsidiaries and affiliates for purposes       consistent with this Privacy Policy.</li>
            <li>The E-Daily       Service may allow you to share information, including Personal       Information, with social networking websites, such as Facebook. We do not       share your Personal Information with them unless you direct the E-Daily Service       to share it. Their use of the information will be governed by their       privacy policies, and you may be able to modify your privacy setting on       their websites.</li>
            <li>We may       share your information if required to do so by law or in the good faith       belief that such action is necessary in order to (i) comply with a legal       obligation, (ii) protect or defend our legal rights or property as well as       those of our business partners, employees, agents and contractors       (including enforcement of our agreements); (iii) act in urgent       circumstances to protect the personal safety of users or the public; (iv)       protect against fraud or risk management purposes; or (v) protect against       legal liability.</li>
          </ul>
          <p>When you register and create a profile through the E-Daily Service, other  users of the E-Daily Service may be able to see your profile information,  including your screen name, profile photo and any content that you post to the E-Daily  Service. We recommend that you guard your anonymity and sensitive information.  We are not responsible for the privacy practices of the other users who will  view and use the information you post. </p>
          <p><strong>Non-Identifiable or Aggregated Data</strong><br />
            We also receive and store certain non-personally identifiable information.  Such information, which is collected passively using various technologies,  cannot presently be used to specifically identify you. We may store such  information itself or such information may be included in databases owned and  maintained by our affiliates, agents or service providers. We may use such  information and pool it with other information to track, for example, the total  number of visitors to the E-Daily Service, the number of visitors to each page  of our Site, the domain names of our visitors' Internet service providers, and  how our users use and interact with the E-Daily Service. Also, in an ongoing  effort to better understand and serve the users of the E-Daily Service, we  often conduct research on our customer demographics, interests and behavior  based on the personal information and other information provided to us. This  research may be compiled and analyzed on an aggregate basis. We may share this  non-identifiable and aggregate data with our affiliates, agents and business  partners, but this type of non-identifiable and aggregate information does not  identify you personally. We may also disclose aggregated user statistics in  order to describe our services to current and prospective business partners and  to other third parties for other lawful purposes. <br />
            The use and disclosure of this non-personally identifiable information is  not subject to restrictions under this Privacy Policy. </p>
          <p><strong>Your Choices</strong><br />
            You can visit the Site without providing any personal information. If you  choose not to provide any personal information, you may not be able to use  certain features of our E-Daily Service. </p>
          <p><strong>Exclusions</strong><br />
            This Privacy Policy does not apply to any personal data collected by E-Daily  other than personal data collected through the E-Daily Service. This Privacy  Policy shall not apply to any unsolicited information you provide to us through  the E-Daily Service or through any other means. This includes, but is not limited  to, information posted to any public areas of the E-Daily Service, such as  bulletin boards (collectively, &ldquo;Public Areas&rdquo;), any ideas for new products or  modifications to existing products, and other unsolicited submissions  (collectively, &ldquo;Unsolicited Information&rdquo;). All Unsolicited Information shall be  deemed to be non-confidential and we shall be free to reproduce, use, disclose,  distribute and exploit such Unsolicited Information without limitation or  attribution. </p>
          <p><strong>How we protect your information</strong><br />
            We have taken reasonable steps to protect the information you provide via  the E-Daily Service from loss, misuse, and unauthorized access, disclosure, alteration,  or destruction. However, no Internet, email or other electronic transmission is  ever fully secure or error free, so you should take special care in deciding  what information you send to us in this way. We do not accept liability for  unintentional disclosure. <br />
            By using the E-Daily Service or providing Personal Information to us, you  agree that we may communicate with you electronically regarding security,  privacy, and administrative issues relating to your use of the E-Daily Service.  If we learn of a security system&rsquo;s breach, we may attempt to notify you  electronically by posting a notice on the Site and/or Applications or sending  an email to you. </p>
          <p><strong>Consent to processing</strong><br />
            If you use the E-Daily Service outside of the European Union, you fully  understand and unambiguously consent to the transfer of your information,  including Personal Information, to, and the collection and processing of such  information in Greece and other countries or territories of the European Union.  The laws on holding such information in European Union may vary and be less  stringent than laws of your state or country. </p>
          <p><strong>Children</strong><br />
            We do not knowingly collect Personal Information from children under the  age of 13. If you are under the age of 13, please do not submit any Personal  Information through the E-Daily Service. We encourage parents and legal  guardians to monitor their children&rsquo;s Internet usage and to help enforce our  Privacy Policy by instructing their children never to provide Personal  Information through the E-Daily Service without their permission. If you have  reason to believe that a child under the age of 13 has provided Personal  Information to us through our E-Daily, please contact us, and we will  endeavor to delete that information from our databases. </p>
          <p><strong>Links to Other Web Sites</strong><br />
            This Privacy Policy applies only to the E-Daily Service. The E-Daily Service  may contain links to other web sites not operated or controlled by us (the  &ldquo;Third Party Sites&rdquo;). The policies and procedures we described here do not  apply to the Third Party Sites. The links from the E-Daily Service do not imply  that we endorse or have reviewed the Third Party Sites. We suggest contacting  those sites directly for information on their privacy policies. </p>
          <p><strong>What are my privacy options and how can I change  my settings?</strong><br />
            <em>To modify your registration information:</em> Once you have registered, you  can change your registration information (including your email address and  password). <br />
            <em>Opt out:</em> Each email communication we send you will contain instructions permitting  you to &quot;opt-out&quot; of receiving future marketing communications. In  addition, if at any time you wish not to receive any future communications or  you wish to have your name deleted from our email mailing lists, please contact  us as indicated below. Note however, that certain communications, such as push  notifications, require you to change the settings on your device if at any time  you wish not to receive any future communications. Note that on occasion we  send service-related announcements, such as disruption notifications, and you  may not be able to opt-out of these communications. </p>
          <p><strong>English language version shall control</strong><br />
            The English language version of this Privacy Policy is the version that  governs your use of the E-Daily Service and in the event of any conflict  between the English language version and a translated version, the English  language version will control.</p>
          <p> To print out this Statement, you may visit www.e-daily.gr/privacy.asp</p>
        </div>
    
    </div>
	<div class="mainArea2">
		
<center>
<script language="javascript">
<!--
if (window.adgroupid == undefined) {
                window.adgroupid = Math.round(Math.random() * 1000);
}
document.write('<scr'+'ipt language="javascript1.1" src="http://adserver.adtech.de/addyn/3.0/1370/5799491/0/170/ADTECH;loc=100;target=_blank;key=;grp='+window.adgroupid+';misc='+new Date().getTime()+'"></scri'+'pt>');
//-->
</script><noscript><a href="http://adserver.adtech.de/adlink/3.0/1370/5799491/0/170/ADTECH;loc=300;key=" target="_blank"><img src="http://adserver.adtech.de/adserv/3.0/1370/5799491/0/170/ADTECH;loc=300;key=" border="0" width="300" height="250"></a></noscript>
</center>



		<br>
		<script language="javascript">
$().ready(function() {
    $("#todayTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  updateHash: false
	});
/*	$("#tabs-hl").mCustomScrollbar({
		theme:"dark-thick"
	});
*/
	$("#tabs-recent").mCustomScrollbar({
		theme:"dark-thick"
	});
});

</script>
<div class="postsRedHeader">διαβάστε σήμερα</div>
<div id="todayTabs" class="tab-container">
  <ul class="etabs">
    <li class="tab"><a href="#tabs-hl">ΕΠΙΛΟΓΕΣ</a></li>
    <li class="tab"><a href="#tabs-recent">ΠΡΟΣΦΑΤΑ</a></li>
  </ul>
  <div id="tabs-hl" class="postEntries">
    
	<a href="http://www.e-daily.gr/themata/60015/kalokairi-sthn-athhna" target="_blank">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/003726-01-view-from-plunge-pool1.jpg" alt="Μεγάλο αφιέρωμα: Καλοκαίρι στην Αθήνα" border="0" class="lazyload">
        </div>
        <div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span><span class="when">00:08</span></div>
        <h3>Μεγάλο αφιέρωμα: Καλοκαίρι στην Αθήνα</h3>
        <h4>Ξέμεινες στην πόλη; Απόλαυσε το!</h4>
	</a>

	<a href="/themata/60024/kathysterhse-h-pthsh-sas-deite-ta-dikaiwmata-sas" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/aeroporika.jpg" alt="Καθυστέρησε η πτήση σας; Δείτε τα δικαιώματα σας" border="0" class="lazyload">
        </div>
        <div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span><span class="when">00:08</span></div>
        <h3>Καθυστέρησε η πτήση σας; Δείτε τα δικαιώματα σας</h3>
        <h4>Έχετε δικαιώματα που πιθανόν να μη γνωρίζετε</h4>
	</a>

	<a href="/themata/60095/san-shmera-h-toyrkia-apagoreyei-thn-didaskalia-ths-ellhnikhs-glwssas-se-imvro-kai-tenedo-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/sxoleio_imvros_1.jpg" alt="Σαν σήμερα: Η Τουρκία απαγορεύει την διδασκαλία της ελληνικής γλώσσας σε Ίμβρο και Τένεδο" border="0" class="lazyload">
        </div>
        <div><span class="category">ΕΛΛΑΔΑ</span><span class="when">00:08</span></div>
        <h3>Σαν σήμερα: Η Τουρκία απαγορεύει την διδασκαλία της ελληνικής γλώσσας σε Ίμβρο και Τένεδο</h3>
        <h4>Την 1η Ιουλίου 1964</h4>
	</a>

	<a href="/themata/28049/ta-mpania-toy-laoy-mia-retro-kalokairinh-ellada-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2014/mpanialaou.jpg" alt="Τα μπάνια του λαού: Μια ρετρό καλοκαιρινή Ελλάδα " border="0" class="lazyload">
        </div>
        <div><span class="category">ΙΣΤΟΡΙΚΑ</span><span class="when">00:00</span></div>
        <h3>Τα μπάνια του λαού: Μια ρετρό καλοκαιρινή Ελλάδα </h3>
        <h4>Φωτογραφίες που θα σας θυμίσουν τα παλιά</h4>
	</a>

	<a href="/themata/59867/to-yperpolyteles-katafygio-disekatommyrioyxwn-sthn-germania-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/299998D700000578-3123296-image-a-12_1434256042642.jpg" alt="Το υπερπολυτελές καταφύγιο δισεκατομμυριούχων στην Γερμανία" border="0" class="lazyload">
        </div>
        <div><span class="category">ΚΟΣΜΟΣ</span><span class="when">16:47</span></div>
        <h3>Το υπερπολυτελές καταφύγιο δισεκατομμυριούχων στην Γερμανία</h3>
        <h4>Θεωρείται ως το «απόλυτο καταφύγιο για την Ημέρα της Κρίσεως»</h4>
	</a>

	<a href="/life/60089/h-anasa-toy-drakoy-einai-to-pio-hype-glyko-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/dragonsbreath2.jpg" alt="Η... ανάσα του δράκου είναι το πιο hype γλυκό!" border="0" class="lazyload">
        </div>
        <div><span class="category">FOOD</span><span class="when">00:08</span></div>
        <h3>Η... ανάσα του δράκου είναι το πιο hype γλυκό!</h3>
        <h4>Το επιδόρπιο που τρελαίνει το Λος Άντζελες</h4>
	</a>

	<a href="/life/60000/h-kompsothta-synanta-thn-aythentikothta-sth-nea-seira-royxwn-ths-access-pics-kerdiste" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-ticket"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/accessfashion.jpg" alt="H κομψότητα συναντά την αυθεντικότητα στη νέα σειρά ρούχων της Access" border="0" class="lazyload">
        </div>
        <div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">STYLE</span><span class="when">00:08</span></div>
        <h3>H κομψότητα συναντά την αυθεντικότητα στη νέα σειρά ρούχων της Access</h3>
        <h4>Κερδίστε ένα ξεχωριστό κομμάτι από τη σειρά Access</h4>
	</a>

	<a href="/life/59869/mporeis-na-vreis-to-froyto-me-thn-perissoterh-zaxarh-quiz" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-doc-text"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/fruitmainpic.jpg" alt="Μπορείς να βρεις το φρούτο με την περισσότερη ζάχαρη;" border="0" class="lazyload">
        </div>
        <div><span class=alert>QUIZ</span><span class="category">FOOD</span><span class="when">00:08</span></div>
        <h3>Μπορείς να βρεις το φρούτο με την περισσότερη ζάχαρη;</h3>
        <h4>Για δοκίμασε να δούμε!</h4>
	</a>

  </div>
  <div id="tabs-recent" class="postEntries noimg" style="height:600px; overflow:hidden">
    
	<a href="/viral/60114/apisteytes-periptwseis-epithesewn-apo-karxaries-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/shark-322342.jpg" alt="Απίστευτες περιπτώσεις επιθέσεων από καρχαρίες" border="0" class="lazyload">
        </div>
        <div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">00:08</span></div>
        <h3>Απίστευτες περιπτώσεις επιθέσεων από καρχαρίες</h3>
        <h4>Όταν η πραγματικότητα ξεπερνά την μυθοπλασία</h4>
	</a>

	<a href="/viral/60121/parathse-to-agori-ths-gia-enan-ploysio-agnwsto-deite-to-apisteyto-video-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/farsa.jpg" alt="Παράτησε το αγόρι της για έναν πλούσιο άγνωστο: Δείτε τo απίστευτo video" border="0" class="lazyload">
        </div>
        <div><span class="category">ΚΟΣΜΟΣ</span><span class="when">00:08</span></div>
        <h3>Παράτησε το αγόρι της για έναν πλούσιο άγνωστο: Δείτε τo απίστευτo video</h3>
        <h4>Τα λεφτά δεν φέρνουν την ευτυχία ή μήπως τη φέρνουν;</h4>
	</a>

	<a href="/life/60000/h-kompsothta-synanta-thn-aythentikothta-sth-nea-seira-royxwn-ths-access-pics-kerdiste" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-ticket"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/accessfashion.jpg" alt="H κομψότητα συναντά την αυθεντικότητα στη νέα σειρά ρούχων της Access" border="0" class="lazyload">
        </div>
        <div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">STYLE</span><span class="when">00:08</span></div>
        <h3>H κομψότητα συναντά την αυθεντικότητα στη νέα σειρά ρούχων της Access</h3>
        <h4>Κερδίστε ένα ξεχωριστό κομμάτι από τη σειρά Access</h4>
	</a>

	<a href="/life/60057/ta-zwdia-shmera-1-ioylioy" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/gorgona24.jpg" alt="Τα ζώδια σήμερα: 1 Ιουλίου" border="0" class="lazyload">
        </div>
        <div><span class="category">ΖΩΔΙΑ</span><span class="when">00:08</span></div>
        <h3>Τα ζώδια σήμερα: 1 Ιουλίου</h3>
        <h4>Αστρολογικές προβλέψεις κάθε μέρα από το E-Daily.gr</h4>
	</a>

	<a href="http://www.e-daily.gr/themata/60015/kalokairi-sthn-athhna" target="_blank">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/003726-01-view-from-plunge-pool1.jpg" alt="Μεγάλο αφιέρωμα: Καλοκαίρι στην Αθήνα" border="0" class="lazyload">
        </div>
        <div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span><span class="when">00:08</span></div>
        <h3>Μεγάλο αφιέρωμα: Καλοκαίρι στην Αθήνα</h3>
        <h4>Ξέμεινες στην πόλη; Απόλαυσε το!</h4>
	</a>

	<a href="/themata/60095/san-shmera-h-toyrkia-apagoreyei-thn-didaskalia-ths-ellhnikhs-glwssas-se-imvro-kai-tenedo-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/sxoleio_imvros_1.jpg" alt="Σαν σήμερα: Η Τουρκία απαγορεύει την διδασκαλία της ελληνικής γλώσσας σε Ίμβρο και Τένεδο" border="0" class="lazyload">
        </div>
        <div><span class="category">ΕΛΛΑΔΑ</span><span class="when">00:08</span></div>
        <h3>Σαν σήμερα: Η Τουρκία απαγορεύει την διδασκαλία της ελληνικής γλώσσας σε Ίμβρο και Τένεδο</h3>
        <h4>Την 1η Ιουλίου 1964</h4>
	</a>

	<a href="/themata/60099/spartawar-of-empires-to-pio-ethistiko-dwrean-paixnidi-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/02-sparta-war-of-empires-wallpaper.jpg" alt="Sparta:War of Empires. Το πιο εθιστικό δωρεάν παιχνίδι" border="0" class="lazyload">
        </div>
        <div><span class="category">ΤΕΧΝΟΛΟΓΙΑ</span><span class="when">00:08</span></div>
        <h3>Sparta:War of Empires. Το πιο εθιστικό δωρεάν παιχνίδι</h3>
        <h4> Με πάνω από 60 εκατομμύρια οπαδούς! </h4>
	</a>

	<a href="/life/59869/mporeis-na-vreis-to-froyto-me-thn-perissoterh-zaxarh-quiz" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-doc-text"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/fruitmainpic.jpg" alt="Μπορείς να βρεις το φρούτο με την περισσότερη ζάχαρη;" border="0" class="lazyload">
        </div>
        <div><span class=alert>QUIZ</span><span class="category">FOOD</span><span class="when">00:08</span></div>
        <h3>Μπορείς να βρεις το φρούτο με την περισσότερη ζάχαρη;</h3>
        <h4>Για δοκίμασε να δούμε!</h4>
	</a>

	<a href="/life/60089/h-anasa-toy-drakoy-einai-to-pio-hype-glyko-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/dragonsbreath2.jpg" alt="Η... ανάσα του δράκου είναι το πιο hype γλυκό!" border="0" class="lazyload">
        </div>
        <div><span class="category">FOOD</span><span class="when">00:08</span></div>
        <h3>Η... ανάσα του δράκου είναι το πιο hype γλυκό!</h3>
        <h4>Το επιδόρπιο που τρελαίνει το Λος Άντζελες</h4>
	</a>

	<a href="/life/60026/epiteloys-h-stutikh-dysleitoyrgia-therapeyetai-se-ellhniko-nosokomeio" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/stitidus.jpg" alt="Επιτέλους η στuτική δυσλειτουργία θεραπεύεται σε ελληνικό νοσοκομείο" border="0" class="lazyload">
        </div>
        <div><span class="category">BODY+MIND</span><span class="when">00:08</span></div>
        <h3>Επιτέλους η στuτική δυσλειτουργία θεραπεύεται σε ελληνικό νοσοκομείο</h3>
        <h4>Σε πιο ελληνικό δημόσιο νοσοκομείο εφαρμόζεται η συγκεκριμένη μέθοδος</h4>
	</a>

	<a href="/viral/60093/osoi-ton-akoloythoysan-sto-snapchat-eidan-thn-apisteyth-peripeteia-toy-live-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/unluckysnapchat.jpg" alt="Όσοι τον ακολουθούσαν στο Snapchat είδαν την απίστευτη περιπέτειά του live" border="0" class="lazyload">
        </div>
        <div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">00:08</span></div>
        <h3>Όσοι τον ακολουθούσαν στο Snapchat είδαν την απίστευτη περιπέτειά του live</h3>
        <h4>Και τι δεν είδε!</h4>
	</a>

	<a href="/themata/60024/kathysterhse-h-pthsh-sas-deite-ta-dikaiwmata-sas" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/aeroporika.jpg" alt="Καθυστέρησε η πτήση σας; Δείτε τα δικαιώματα σας" border="0" class="lazyload">
        </div>
        <div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span><span class="when">00:08</span></div>
        <h3>Καθυστέρησε η πτήση σας; Δείτε τα δικαιώματα σας</h3>
        <h4>Έχετε δικαιώματα που πιθανόν να μη γνωρίζετε</h4>
	</a>

	<a href="/entertainment/60106/daphne-and-the-fuzz-live" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/daphneandthefuzzrockwave.jpg" alt="Daphne and the Fuzz Live" border="0" class="lazyload">
        </div>
        <div><span class="category">ΜΟΥΣΙΚΗ</span><span class="when">00:08</span></div>
        <h3>Daphne and the Fuzz Live</h3>
        <h4>Terra Vibe Park - 19/7/2016</h4>
	</a>

	<a href="/entertainment/60105/oidipoys-tyrannos" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/rimastouminas.jpg" alt="Οιδίπους Τύραννος" border="0" class="lazyload">
        </div>
        <div><span class="category">ΘΕΑΤΡΟ</span><span class="when">00:08</span></div>
        <h3>Οιδίπους Τύραννος</h3>
        <h4>Αρχαίο Θέατρο Επιδαύρου - 29/7/2016 - 30/7/2016</h4>
	</a>

	<a href="/entertainment/60104/creative-economy-fair-and-forum-apo-thn-idialogue" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/creativeeconomyfair.jpg" alt="Creative Economy Fair and Forum από την iDialogue" border="0" class="lazyload">
        </div>
        <div><span class="category">ΠΟΛΗ</span><span class="when">00:08</span></div>
        <h3>Creative Economy Fair and Forum από την iDialogue</h3>
        <h4>Μονή Λαζαριστών - 4/7/2016 - 5/7/2016</h4>
	</a>

	<a href="/entertainment/60103/to-psema-toy-florian-zeller" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/topsemaatheridiskaridi.jpg" alt="Το Ψέμα, του Florian Zeller" border="0" class="lazyload">
        </div>
        <div><span class="category">ΘΕΑΤΡΟ</span><span class="when">00:08</span></div>
        <h3>Το Ψέμα, του Florian Zeller</h3>
        <h4>Θέατρο Μικρό Παλλάς - 7/10/2016 - 30/10/2016</h4>
	</a>

	<a href="/entertainment/60102/hayali-sans-toi-je-tombe-xwris-esena-peftw" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/sanstoijetombe.jpg" alt="Hayali – Sans toi, je tombe / Χωρίς εσένα, πέφτω…" border="0" class="lazyload">
        </div>
        <div><span class="category">ΧΟΡΟΣ</span><span class="when">00:08</span></div>
        <h3>Hayali – Sans toi, je tombe / Χωρίς εσένα, πέφτω…</h3>
        <h4>Ίδρυμα Μιχάλης Κακογιάννης - 18/7/2016</h4>
	</a>

	<a href="/entertainment/60101/h-antigonh-toy-sofoklh-apo-ton-stathh-livathino" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/antigonilivathinos.jpg" alt="Η Αντιγόνη του Σοφοκλή από τον Στάθη Λιβαθινό" border="0" class="lazyload">
        </div>
        <div><span class="category">ΘΕΑΤΡΟ</span><span class="when">00:08</span></div>
        <h3>Η Αντιγόνη του Σοφοκλή από τον Στάθη Λιβαθινό</h3>
        <h4>Αρχαίο Θέατρο Επιδαύρου - 15/7/2016 - 16/7/2016</h4>
	</a>

	<a href="/entertainment/60083/ekthesh-aptikwn-eikonwn-gia-atoma-me-tyflwsh-kai-anaphria-sthn-orash-sth-thessalonikh" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/ekthesiaptikonikonon.jpg" alt="Έκθεση απτικών εικόνων για άτομα με τύφλωση και αναπηρία στην όραση στη Θεσσαλονίκη" border="0" class="lazyload">
        </div>
        <div><span class="category">ΕΙΚΑΣΤΙΚΑ</span><span class="when">00:08</span></div>
        <h3>Έκθεση απτικών εικόνων για άτομα με τύφλωση και αναπηρία στην όραση στη Θεσσαλονίκη</h3>
        <h4>4/7/2016 - 15/7/2016</h4>
	</a>

	<a href="/viral/60127/sifnos-asygkrathtoi-sthn-paralia-h-kylie-minogue-kai-o-28xronos-syntrofos-ths-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/minogue_sifnos_0.jpg" alt="Σίφνος: Ασυγκράτητοι στην παραλία η Kylie Minogue και ο 28χρονος σύντροφός της" border="0" class="lazyload">
        </div>
        <div><span class="category">TABLOID</span><span class="when">00:05</span></div>
        <h3>Σίφνος: Ασυγκράτητοι στην παραλία η Kylie Minogue και ο 28χρονος σύντροφός της</h3>
        <h4>Φιλιούνται, αγκαλιάζονται...</h4>
	</a>

  </div>
</div>
		<br>
		<center>
<div id="5799386"></div>
</center>
<script>
ADTECH.config.placements[5799386] = {params: { alias: '', key:'',target: '_blank' }};
ADTECH.enqueueAd(5799386);
ADTECH.executeQueue();
</script>
		<br>
		
<script>
$().ready(function() {
    $("#topTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  defaultTab: "li#ttab-themata",
	  updateHash: false
	});
/*
	$("#tabs-news").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-viral").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-life").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-themata").mCustomScrollbar({
		theme:"dark-thick"
	});
*/	
});

</script>
<div class="postsRedHeader">δημοφιλέστερα</div>
<div id="topTabs" class="tab-container">
  <ul class="etabs">
    <li class="tab" id="ttab-news"><a href="#tabs-news">ΕΙΔΗΣΕΙΣ</a></li>
    <li class="tab" id="ttab-themata"><a href="#tabs-themata">θΕΜΑΤΑ</a></li>
    <li class="tab" id="ttab-life"><a href="#tabs-life">LIFE</a></li>
    <li class="tab" id="ttab-viral"><a href="#tabs-viral">VIRAL</a></li>
  </ul>
  <div id="tabs-news" class="postEntries top">
    
	<a href="/news/60072/sxedon-etoimh-h-ionia-odos-1-wra-kai-40-lepta-antirrio-giannena-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/ionia_odos_1.jpg" alt="Σχεδόν έτοιμη η Ιόνια Οδός: 1 ώρα και 40 λεπτά Αντίρριο - Γιάννενα" border="0" class="lazyload">
        </div>
        <div><span class="category">ΕΛΛΑΔΑ</span></div>
        <h3>Σχεδόν έτοιμη η Ιόνια Οδός: 1 ώρα και 40 λεπτά Αντίρριο - Γιάννενα</h3>
        <h4>Δείτε καρε καρέ την διαδρομή</h4>
	</a>

	<a href="/news/60124/ethnikh-nok-aoyt-me-gastrenteritida-o-ntorsei" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/dorsey-232.jpg" alt="Εθνική: Νοκ άουτ με γαστρεντερίτιδα ο Ντόρσεϊ" border="0" class="lazyload">
        </div>
        <div><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span></div>
        <h3>Εθνική: Νοκ άουτ με γαστρεντερίτιδα ο Ντόρσεϊ</h3>
        <h4>Νέο πρόβλημα για Κατσικάρη</h4>
	</a>

	<a href="/news/60125/proeidopoiei-h-cia-gia-epithesh-toy-isis-stis-hpa" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/cia-9393.jpg" alt="Προειδοποιεί η CIA για επίθεση του ISIS στις ΗΠΑ" border="0" class="lazyload">
        </div>
        <div><span class="category">ΚΟΣΜΟΣ</span></div>
        <h3>Προειδοποιεί η CIA για επίθεση του ISIS στις ΗΠΑ</h3>
        <h4>Ο επικεφαλής της έκρουσε τον κώδωνα του κινδύνου</h4>
	</a>

	<a href="/news/60109/sev-o-peiraias-mporei-na-parei-th-thesh-toy-londinoy" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/peiraias1.jpg" alt="ΣΕΒ: O Πειραιάς μπορεί να πάρει τη θέση του Λονδίνου" border="0" class="lazyload">
        </div>
        <div><span class="category">ΟΙΚΟΝΟΜΙΑ</span></div>
        <h3>ΣΕΒ: O Πειραιάς μπορεί να πάρει τη θέση του Λονδίνου</h3>
        <h4>Χαρακτηρίζει το Brexit ανεπανάληπτη ευκαιρία για την Ελλάδα</h4>
	</a>

	<a href="/news/60096/salos-oxi-ths-uefa-se-enos-leptoy-sigh-gia-ta-thymata-toy-atatoyrk" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/uefa_sigi.jpg" alt="Σάλος: «Οχι» της UEFA σε ενός λεπτού σιγή για τα θύματα του Ατατούρκ" border="0" class="lazyload">
        </div>
        <div><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span></div>
        <h3>Σάλος: «Οχι» της UEFA σε ενός λεπτού σιγή για τα θύματα του Ατατούρκ</h3>
        <h4>Έντονη η κριτική στα social media</h4>
	</a>

	<a href="/news/60123/fylakish-4-mhnwn-ston-antwnh-remo" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/5269a6d2f40cb9eec47ad8cff6ac0dd1_L.jpg" alt="Φυλάκιση 4 μηνών στον Αντώνη Ρέμο" border="0" class="lazyload">
        </div>
        <div><span class="category">ΕΛΛΑΔΑ</span></div>
        <h3>Φυλάκιση 4 μηνών στον Αντώνη Ρέμο</h3>
        <h4>Για μη καταβολή εργοδοτικών εισφορών</h4>
	</a>

	<a href="/news/60126/sthn-gioynaitent-o-zlatan-impramovits-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/diaqxflk.jpg" alt="Στην Γιουνάιτεντ ο Ζλάταν Ιμπραΐμοβιτς " border="0" class="lazyload">
        </div>
        <div><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span></div>
        <h3>Στην Γιουνάιτεντ ο Ζλάταν Ιμπραΐμοβιτς </h3>
        <h4>Αποκάλυψε τη νέα του ομάδα ο Σουηδός</h4>
	</a>

	<a href="/news/59979/agrios-ksylodarmos-sthn-kina-syzygos-egdyse-ki-edeire-thn-epwmenh-toy-andra-ths-pics-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-picture"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/eromeniantraksilo.jpg" alt="Άγριος ξυλοδαρμός στην Κίνα: Σύζυγος έγδυσε κι έδειρε την εpωμένη του άνδρα της" border="0" class="lazyload">
        </div>
        <div><span class="category">ΚΟΣΜΟΣ</span></div>
        <h3>Άγριος ξυλοδαρμός στην Κίνα: Σύζυγος έγδυσε κι έδειρε την εpωμένη του άνδρα της</h3>
        <h4>Στη μέση του δρόμου </h4>
	</a>

  </div>
  <div id="tabs-themata" class="postEntries top">
    
	<a href="http://www.e-daily.gr/quiz/59251/poios-einai-o-xeiroteros-prwthypoyrgos-kata-thn-diarkeia-ths-krishs-vote" target="_blank">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-doc-text"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/prothyp_3.jpg" alt="Ποιος είναι ο χειρότερος πρωθυπουργός κατά την διάρκεια της κρίσης;" border="0" class="lazyload">
        </div>
        <div><span class=alert>ΨΗΦΙΣΤΕ</span><span class="category">ΠΟΛΙΤΙΚΗ</span></div>
        <h3>Ποιος είναι ο χειρότερος πρωθυπουργός κατά την διάρκεια της κρίσης;</h3>
        <h4>Παπανδρέου, Σαμαράς ή Τσίπρας; Ή όλοι;</h4>
	</a>

	<a href="/themata/59871/h-apisteyth-istoria-mias-14xronhs-transeksoyal-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/14xr.jpg" alt="Η απίστευτη ιστορία μιας 14χρονης τρανσέξουαλ" border="0" class="lazyload">
        </div>
        <div><span class="category">ΚΟΣΜΟΣ</span></div>
        <h3>Η απίστευτη ιστορία μιας 14χρονης τρανσέξουαλ</h3>
        <h4>Πως βίωσε την αλλαγή και το ρατσισμό</h4>
	</a>

	<a href="/themata/59767/fwtografies-apo-to-5o-gay-pride-ths-thessalonikhs-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/thesspride.jpg" alt="Φωτογραφίες από το 5o Gay Pride της Θεσσαλονίκης" border="0" class="lazyload">
        </div>
        <div><span class="category">ΕΛΛΑΔΑ</span></div>
        <h3>Φωτογραφίες από το 5o Gay Pride της Θεσσαλονίκης</h3>
        <h4>Με σύνθημα «Αγαπάτε Αλλήλους»</h4>
	</a>

	<a href="/themata/59827/sta-117-ths-xronia-evgale-kainoyrgia-mallia-kai-dontia" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/gyaliskari.jpg" alt="Στα 117 της χρόνια έβγαλε καινούργια μαλλιά και δόντια" border="0" class="lazyload">
        </div>
        <div><span class="category">ΕΛΛΑΔΑ</span></div>
        <h3>Στα 117 της χρόνια έβγαλε καινούργια μαλλιά και δόντια</h3>
        <h4>Μια απίστευτη ιστορία που αξίζει να διαβάσετε</h4>
	</a>

	<a href="/themata/48232/ti-shmainei-h-leksh-zavarakatranemia-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/ksilouris89.jpg" alt="Τι σημαίνει η λέξη «ζαβαρακατρανέμια»;" border="0" class="lazyload">
        </div>
        <div><span class="category">ΙΣΤΟΡΙΚΑ</span></div>
        <h3>Τι σημαίνει η λέξη «ζαβαρακατρανέμια»;</h3>
        <h4>Αναρωτηθήκατε ποτέ;</h4>
	</a>

	<a href="/themata/59918/h-agnwsth-swsias-ths-alikhs-sth-neraida-kai-to-palikari-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/aliki-9989.jpg" alt="Η άγνωστη σωσίας της Αλίκης στη «Nεράιδα και το παλικάρι»" border="0" class="lazyload">
        </div>
        <div><span class="category">ΑΦΙΕΡΩΜΑΤΑ</span></div>
        <h3>Η άγνωστη σωσίας της Αλίκης στη «Nεράιδα και το παλικάρι»</h3>
        <h4>Η Αλίκη τότε ήταν έγκυος στον Γιαννάκη και η κοιλιά της είχε αρχίσει να φαίνεται πολύ</h4>
	</a>

	<a href="/themata/59044/agoraios-erwtas-sto-mpagklantes-mia-polh-oikos-anoxhs-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/mpagkla3.jpg" alt="Αγοραίος έρωτας στο Μπαγκλαντές: Μια πόλη οίκος ανοχής" border="0" class="lazyload">
        </div>
        <div><span class="category">ΚΟΣΜΟΣ</span></div>
        <h3>Αγοραίος έρωτας στο Μπαγκλαντές: Μια πόλη οίκος ανοχής</h3>
        <h4>Φωτογραφίες που σοκάρουν</h4>
	</a>

	<a href="/themata/59652/7-ameses-synepeies-poy-tha-prokalesei-to-brexit" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/brexit_synepeies_1.jpg" alt="7 άμεσες συνέπειες που θα προκαλέσει το Brexit" border="0" class="lazyload">
        </div>
        <div><span class="category">ΚΟΣΜΟΣ</span></div>
        <h3>7 άμεσες συνέπειες που θα προκαλέσει το Brexit</h3>
        <h4>Τι θα αντιμετωπίσουν οι Βρετανοί και Ευρωπαίοι</h4>
	</a>

  </div>
  <div id="tabs-life" class="postEntries top">
    
	<a href="http://www.e-daily.gr/life/59373/neos-diagwnismos-vinge-project-kerdiste-thn-apolyth-boho-tsanta-toy-kalokairioy-pics-kerdiste" target="_blank">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-ticket"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/vinge3.jpg" alt="Δείτε τους νικητές: Κερδίστε την απόλυτη boho τσάντα του καλοκαιριού" border="0" class="lazyload">
        </div>
        <div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">STYLE</span></div>
        <h3>Δείτε τους νικητές: Κερδίστε την απόλυτη boho τσάντα του καλοκαιριού</h3>
        <h4>Αέρινες boho chic δημιουργίες!</h4>
	</a>

	<a href="/life/59579/thes-kalo-seks-mesa-sth-sxesh-des-ti-prepei-na-kaneis" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/couple.jpg" alt="Θες καλό σεξ μέσα στη σχέση; Δες τι πρέπει να κάνεις" border="0" class="lazyload">
        </div>
        <div><span class="category">ΣΧΕΣΕΙΣ</span></div>
        <h3>Θες καλό σεξ μέσα στη σχέση; Δες τι πρέπει να κάνεις</h3>
        <h4>Δεν είναι αυτό που νομίζεις</h4>
	</a>

	<a href="/life/59847/piesotherapeia-stamathste-toys-ponoys-sto-swma-me-th-methodo-twn-kinezwn-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/piesotherapeia.jpg" alt="Πιεσοθεραπεία: Σταματήστε τους πόνους στο σώμα με τη μέθοδο των Κινέζων" border="0" class="lazyload">
        </div>
        <div><span class="category">BODY+MIND</span></div>
        <h3>Πιεσοθεραπεία: Σταματήστε τους πόνους στο σώμα με τη μέθοδο των Κινέζων</h3>
        <h4>Μάθετε περισσότερα</h4>
	</a>

	<a href="/life/59059/den-tha-pisteyeis-poy-xrhsimeyoyn-ta-kotsania-apo-ta-kerasia" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/kotsania.jpg" alt="Δεν θα πιστεύεις πού χρησιμεύουν τα κοτσάνια από τα κεράσια" border="0" class="lazyload">
        </div>
        <div><span class="category">BODY+MIND</span></div>
        <h3>Δεν θα πιστεύεις πού χρησιμεύουν τα κοτσάνια από τα κεράσια</h3>
        <h4>Δείτε μια ιδιότητά τους που δεν ξέρετε</h4>
	</a>

	<a href="/life/59413/poso-kindyneyei-o-skylos-sas-sto-kayswna" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/koutabi.jpg" alt="Πόσο κινδυνεύει ο σκύλος σας στο καύσωνα;" border="0" class="lazyload">
        </div>
        <div><span class="category">ΖΩΑ</span></div>
        <h3>Πόσο κινδυνεύει ο σκύλος σας στο καύσωνα;</h3>
        <h4>Δείτε τι πρέπει να κάνετε για να τον προστατεύσετε</h4>
	</a>

	<a href="/life/59672/ta-zwdia-shmera-27-ioynioy" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/fridazodia.jpg" alt="Τα ζώδια σήμερα: 27 Ιουνίου" border="0" class="lazyload">
        </div>
        <div><span class="category">ΖΩΔΙΑ</span></div>
        <h3>Τα ζώδια σήμερα: 27 Ιουνίου</h3>
        <h4>Τι λένε τα άστρα</h4>
	</a>

	<a href="/life/59803/giati-xtypame-ta-karpoyzia-prin-ta-agorasoyme" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/karpouzia45.jpeg" alt="Γιατί χτυπάμε τα καρπούζια πριν τα αγοράσουμε;" border="0" class="lazyload">
        </div>
        <div><span class="category">FOOD</span></div>
        <h3>Γιατί χτυπάμε τα καρπούζια πριν τα αγοράσουμε;</h3>
        <h4>Τι προδίδει ο ήχος</h4>
	</a>

	<a href="/life/59038/neos-diagwnismosoi-dyo-ellhnides-sxediastries-poy-ftiaxnoyn-ta-foremata-toy-kalokairioy-pics-kerdiste" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-ticket"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/akiramushi899.jpg" alt="Νέος διαγωνισμός:Οι δύο Ελληνίδες σχεδιάστριες που φτιάχνουν τα φορέματα του καλοκαιριού" border="0" class="lazyload">
        </div>
        <div><span class=alert>ΚΕΡΔΙΣΤΕ</span><span class="category">STYLE</span></div>
        <h3>Νέος διαγωνισμός:Οι δύο Ελληνίδες σχεδιάστριες που φτιάχνουν τα φορέματα του καλοκαιριού</h3>
        <h4>Κερδίστε ένα  γυναικείο καλοκαιρινό πουκάμισο Αkira Mushi</h4>
	</a>

  </div>
  <div id="tabs-viral" class="postEntries top">
    
	<a href="/viral/59974/mad-awards-kautes-emfaniseis-kai-sygkinhsh-gia-ton-pantelidh-pics-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-picture"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/madawards2016.jpg" alt="MAD Awards: Καuτές εμφανίσεις και συγκίνηση για τον Παντελίδη" border="0" class="lazyload">
        </div>
        <div><span class="category">MEDIA</span></div>
        <h3>MAD Awards: Καuτές εμφανίσεις και συγκίνηση για τον Παντελίδη</h3>
        <h4>Όσα έγιναν χτες στη γιορτή της μουσικής</h4>
	</a>

	<a href="/viral/59954/diashmoi-poy-sxedon-to-ekanan-dhmosia-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/celeb-pub-323.jpg" alt="Διάσημοι που σχεδόν το έκαναν δημόσια" border="0" class="lazyload">
        </div>
        <div><span class="category">TABLOID</span></div>
        <h3>Διάσημοι που σχεδόν το έκαναν δημόσια</h3>
        <h4>Χωρίς ίχνος ντροπής</h4>
	</a>

	<a href="/viral/60054/drone-tsakwse-zeygari-na-kanei-seks-sto-kampanario-monasthrioy-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/dronekampanario.jpg" alt="Drone «τσάκωσε» ζευγάρι να κάνει σeξ στο καμπαναριό μοναστηριού!" border="0" class="lazyload">
        </div>
        <div><span class="category">SEXY</span></div>
        <h3>Drone «τσάκωσε» ζευγάρι να κάνει σeξ στο καμπαναριό μοναστηριού!</h3>
        <h4>Στη Ρωσία έγινε κι αυτό</h4>
	</a>

	<a href="/viral/59967/h-nwaina-toy-xfactor-edeikse-live-ton-pisino-ths-se-olh-thn-ellada-video" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-play-circled2"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/noaina_kolos1.jpg" alt="H Nωαίνα του XFactor έδειξε live τον πισινό της σε όλη την Ελλάδα" border="0" class="lazyload">
        </div>
        <div><span class="category">MEDIA</span></div>
        <h3>H Nωαίνα του XFactor έδειξε live τον πισινό της σε όλη την Ελλάδα</h3>
        <h4>Το είδαμε και αυτό!</h4>
	</a>

	<a href="/viral/59877/ki-omws-einai-50-etwn-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/asdkafg1.jpg" alt="Κι όμως είναι 50 ετών!" border="0" class="lazyload">
        </div>
        <div><span class="category">SEXY</span></div>
        <h3>Κι όμως είναι 50 ετών!</h3>
        <h4>Σκοπεύει να φοράει μπικίνι μέχρι τα 80 της</h4>
	</a>

	<a href="/viral/59958/fwties-anapse-h-aretoysaristh-fwtografia-ths-sissys-xrhstidoy-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/sissy767.jpg" alt="Φωτιές άναψε η αρετουσάριστη φωτογραφία της Σίσσυς Χρηστίδου" border="0" class="lazyload">
        </div>
        <div><span class="category">TABLOID</span></div>
        <h3>Φωτιές άναψε η αρετουσάριστη φωτογραφία της Σίσσυς Χρηστίδου</h3>
        <h4>Η αντίδραση της παρουσιάστριας στα αρνητικά σχόλια</h4>
	</a>

	<a href="/viral/60112/kwstetsos-to-gymno-forema-ths-foyreira-einai-antigrafh-pics" target="_parent">
        <div class="cropimg absolute-center">
	        <div class="media"><span class="icon-camera-outline"></span></div>
            <img data-src="http://cdn.e-daily.gr/repository/2016/foureira_forema_1.jpg" alt="Κωστέτσος: Το γυμνό φόρεμα της Φουρέιρα είναι αντιγραφή" border="0" class="lazyload">
        </div>
        <div><span class="category">TABLOID</span></div>
        <h3>Κωστέτσος: Το γυμνό φόρεμα της Φουρέιρα είναι αντιγραφή</h3>
        <h4>Αλλο Madonna και άλλο ΚουλάDonna </h4>
	</a>

	<a href="/viral/60004/estiatorio-prosferei-dwrean-geyma-ef-oroy-zwhs-me-ena-antallagma" target="_parent">
        <div class="cropimg absolute-center">
	        
            <img data-src="http://cdn.e-daily.gr/repository/2016/burger1-343.jpg" alt="Eστιατόριο προσφέρει δωρεάν γεύμα εφ’ όρου ζωής με ένα αντάλλαγμα" border="0" class="lazyload">
        </div>
        <div><span class="category">ΚΟΣΜΟΣ</span></div>
        <h3>Eστιατόριο προσφέρει δωρεάν γεύμα εφ’ όρου ζωής με ένα αντάλλαγμα</h3>
        <h4>Μέχρι που θα έφτανες για δωρεάν μπέργκερ κάθε μέρα;</h4>
	</a>

  </div>
</div>
	</div>

</div>

<div id="footerBgWide">
<center>
<div id="footerMain">

	<div class="footer hideOnSmallScreen" style="width:12%;">			
	
		<span class="footer_optionsHeader"><a href="/news">Ειδήσεις</a></span><br>
		<span class="footer_options">
			<a href="/news">Τελευταία νέα</a>
			<a href="/ellada">Ελλάδα</a>
			<a href="/kosmos">Κόσμος</a>
			<a href="/politiki">Πολιτική</a>
			<a href="/oikonomia">Οικονομία</a>
			<a href="/athlitismos">Αθλητισμός</a>
			<a href="/epistimi">Επιστήμη</a>
			<a href="/politismos">Πολιτισμός</a>			
		</span>
							
	</div>
    
	<div class="footer hideOnSmallScreen" style="width:12%;">
		<div class="footer_optionsHeader"><a href="/themata">Θέματα</a></div><br>
		<div class="footer_optionsHeader"><a href="/quiz">Κουίζ</a></div><br>     
		<div class="footer_optionsHeader"><a href="/viral">Viral</a></div><br>
		<div class="footer_optionsHeader"><a href="/lol">LOL</a></div>
		<div class="footer_optionsHeader"><a href="/omg">OMG</a></div>
		<div class="footer_optionsHeader"><a href="/juicy">JUICY</a></div>
		<div class="footer_optionsHeader"><a href="/alist">A-LIST</a></div>
		<div class="footer_optionsHeader"><a href="/geeky">GEEKY</a></div>
		<div class="footer_optionsHeader"><a href="/cute">CUTE</a></div>
		<div class="footer_optionsHeader"><a href="/retro">RETRO</a></div>
        
    </div>
		
	<div class="footer hideOnSmallScreen" style="width:12%;">			

		<span class="footer_optionsHeader"><a href="/life">Life</a></span><br>
		<span class="footer_options">
			<a href="/food">Food</a>
			<a href="/bodymind">Body+Mind</a>
			<a href="/travel">Ταξίδια</a>
			<a href="/style">Style</a>
			<a href="/spiti">Σπίτι</a>
			<a href="/family">Family</a>
			<a href="/sxeseis">Σχέσεις</a>
		</span><br>		
				
	</div>
		
	<div class="footer hideOnSmallScreen" style="width:18%;">
       
		<div class="footer_optionsHeader"><a href="/entertainment">Ψυχαγωγία</a></div><br>	
		<span class="footer_options">
			<a href="/agenda">Agenda</a>
	        <a href="/wintickets">Κερδίστε Προσκλήσεις</a><br>
			<a href="/music">Μουσική</a>
			<a href="/theater">Θέατρο+Χορός</a>
			<a href="/arts">Εικαστικά</a>
			<a href="/cinema">Σινεμά</a>
			<a href="/nightlife">Νάιτ Λάιφ</a>
			<a href="/city">Πόλη</a>
		</span>
       
	</div> 
    
	<div class="footer fullOnSmallScreen" style="width:32%;">
		<div class="footer_optionsHeader">Newsletter</div><br>	
		<style type="text/css">
#mc-embedded-subscribe-form{ margin:0; padding:0 }
#mc_embed_signup{ display:table; clear:both; text-align: left; line-height: normal;}

#mc_embed_signup .mc-field-group{ display:table; margin:10px 0 10px 0; padding:0; width: 100%}
#mc_embed_signup li{ display:block; float:left; margin-top: 12px; width:33.333%; padding:0; font-weight: 400; font-size: 1.25em;}

#mc_embed_signup .checkbox {width:22px; height:22px; border:1px solid #e3e3e3; background:#fff; margin:0; padding:0}
#mc_embed_signup .checkbox:before {content:""; display:block; width:23px; height:23px; background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 0 0 no-repeat;}
#mc_embed_signup .checkbox:checked:before {background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') -32px 0 no-repeat;}
#mc_embed_signup label {margin-left:5px; padding-top: -5px;}

#mc_embed_signup .email{ font-weight:normal; height:45px; width:55%; float:left; display: inline-block;
background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 10px -22px no-repeat; 
color:#000;  -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; padding:0 0 0 46px; border: 1px solid #d9d9d9}
#mc_embed_signup .email.focus {color:#000;}
#mc_embed_signup .email.placeholder{color:#e3e3e3;}

#mc_embed_signup .submit-button { background:#0079ff; text-align:center; float:left; width:30%; height:47px; border:none; font-size:14px; text-transform:uppercase; color:#FFF; letter-spacing:1px; cursor:pointer; display:inline-block; padding-top:6px; margin-left:3px; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; }
#mc_embed_signup .submit-button:hover {background:#2e78b3; text-decoration:none;}

#mc_embed_signup div.mce_inline_error{ background-color:#fff; color: #804040;}

.clearfix{ padding:0; margin:0 }

</style>

<div id="mc_embed_signup" class="clearfix">

		<form action="//nextweb.us10.list-manage.com/subscribe/post?u=037b671af872ebf3eb1ceac4a&amp;id=5ab11026ce" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			<div class="clearfix">Εγραφείτε στα newsletters του E-Daily.gr για να μαθαίνετε τα καλύτερα στο email σας:</div>
			<ul class="mc-field-group input-group">
				<li>
                   	<input type="checkbox" class="checkbox" value="1" name="group[1109][1]" id="mce-group[1109]-1109-0" checked="checked">
                    <label for="mce-group[1109]-1109-0">Ειδήσεις</label>
                </li>
                <li>
					<input type="checkbox" class="checkbox" value="2" name="group[1109][2]" id="mce-group[1109]-1109-1" checked="checked">
                    <label for="mce-group[1109]-1109-1">Viral</label>
                </li>
                <li>
					<input type="checkbox" class="checkbox" value="4" name="group[1109][4]" id="mce-group[1109]-1109-2" checked="checked">
                    <label for="mce-group[1109]-1109-2">Life</label>
                </li>
                <li>
					<input type="checkbox" class="checkbox" value="8" name="group[1109][8]" id="mce-group[1109]-1109-3" checked="checked">
                    <label for="mce-group[1109]-1109-3">E-Agenda</label>
                </li>
                <li>
					<input type="checkbox" class="checkbox" value="16" name="group[1109][16]" id="mce-group[1109]-1109-4" checked="checked">
                    <label for="mce-group[1109]-1109-4">E-Weekly</label>
                </li>
                <li></li>
			</ul>
			<div class="mc-field-group" style="margin-top:25px;">
				<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Συμπληρώστε το email σας">
				<input type="submit" class="submit-button" value="ΕΓΓΡΑΦΗ" name="subscribe" id="mc-embedded-subscribe">
			</div>
		</form>
            
		<div id="mce-responses" class="clear">
			<div class="response" id="mce-error-response" style="display:none"></div>
			<div class="response" id="mce-success-response" style="display:none"></div>
		</div>
		<div style="position: absolute; left: -5000px;"><input type="text" name="b_037b671af872ebf3eb1ceac4a_5ab11026ce" tabindex="-1" value=""></div>
        
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email'; /*
 * Translated default messages for the $ validation plugin.
 * Locale: EL
 */
$.extend($.validator.messages, {
	required: "<span class=asterisk></span> Αυτό το πεδίο είναι υποχρεωτικό.",
	remote: "<span class=asterisk></span> Παρακαλώ διορθώστε αυτό το πεδίο.",
	email: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη διεύθυνση email.",
	url: "<span class=asterisk></span> Παρακαλώ εισάγετε ένα έγκυρο URL.",
	date: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία.",
	dateISO: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία (ISO).",
	number: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό.",
	digits: "<span class=asterisk></span> Παρακαλώ εισάγετε μόνο αριθμητικά ψηφία.",
	creditcard: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό πιστωτικής κάρτας.",
	equalTo: "<span class=asterisk></span> Παρακαλώ εισάγετε την ίδια τιμή ξανά.",
	accept: "<span class=asterisk></span> Παρακαλώ εισάγετε μια τιμή με έγκυρη επέκταση αρχείου.",
	maxlength: $.validator.format("<span class=asterisk></span> Παρακαλώ εισάγετε μέχρι και {0} χαρακτήρες."),
	minlength: $.validator.format("Παρακαλώ εισάγετε τουλάχιστον {0} χαρακτήρες."),
	rangelength: $.validator.format("Παρακαλώ εισάγετε μια τιμή με μήκος μεταξύ {0} και {1} χαρακτήρων."),
	range: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεταξύ {0} και {1}."),
	max: $.validator.format("Παρακαλώ εισάγετε μια τιμή μικρότερη ή ίση του {0}."),
	min: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεγαλύτερη ή ίση του {0}.")
});}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->        
	</div>
    
    <div class="footer footerRight footer_options fullOnSmallScreen" style="width:14%; text-align: right">
    	<div class="footer_optionsHeader">Follow us</div><br>
		<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
        	<span>Facebook</span>
            <i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
        </a>
        <a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
        	<span>Twitter</span>
            <i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
        </a>
        <a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
        	<span>Google+</span>
            <i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
        </a>
        <a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
        	<span>Instagram</span>
            <i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
        </a>
        <a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
        	<span>YouTube</span>
            <i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
        </a>
        <a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
        	<span>RSS</span>
            <i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
        </a>
    </div>	
    
</div>
</center>
</div>
<center>
<div id="footerMain">
		<div class="footer footer21">
        
			<a href="javascript:openContact();">Επικοινωνία</a>&nbsp;&nbsp;&nbsp;
			<a href="javascript:openContact();">Διαφήμιση</a>&nbsp;&nbsp;&nbsp;
			<a href="/rules.asp">Όροι Διαγωνισμών</a>&nbsp;&nbsp;&nbsp;
			<a href="/privacy.asp">Privacy</a>&nbsp;&nbsp;&nbsp;
			<a href="/terms.asp">Terms of use</a>
			<br>
			<a href="http://www.e-radio.gr" target="_blank">E-Radio Hellas </a> &nbsp;&nbsp;&nbsp;
			<a href="http://www.e-radio.com.cy" target="_blank">E-Radio Cyprus </a> &nbsp;&nbsp;&nbsp;
			<a href="http://www.e-radio.co.uk" target="_blank">E-Radio UK </a> &nbsp;&nbsp;&nbsp;
            <a href="http://www.onestreaming.com" target="_blank">One Streaming </a> &nbsp;&nbsp;&nbsp;
			<a href="http://www.arionradio.com" target="_blank">Arion Radio </a> &nbsp;&nbsp;&nbsp;
			<a href="http://www.athensparty.com" target="_blank">Athens Party </a> &nbsp;&nbsp;&nbsp;
			<a href="http://www.akous.gr" target="_blank">Akous </a> &nbsp;&nbsp;&nbsp;
			<br>
			© 2015 All Right Reserved. "E-DAILY" is a registered trademark

		</div>
		<div class="footer footer22">
			<a href="http://www.nextweb.gr" target="_blank"><img src="/_img/nextweb.png" border="0" alt="Nextweb"></a>
		</div>
</div>
</center>

</div>
</div>

<script type='text/javascript'>
$().ready(function() {
	
	if( $('#topHeaderMobile').length ){

		$('#mobilemenu_pad').infinitypush({
			openingspeed: 300,
			closingspeed: 300
		});
		
		/*
		$('#mobilemenu_icon').click(function(){
			
			$(this).toggleClass('open');
			//slideout.toggle();
			
		});
	
		slideout.on('open', function() {
			if ($('#mobilemenu_icon').hasClass('open')==false){$('#mobilemenu_icon').addClass('open')}
		});
		slideout.on('close', function() {
			if ($('#mobilemenu_icon').hasClass('open')==true){$('#mobilemenu_icon').removeClass('open')}
	
		});
		*/
	}

});
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-551bf8914936f63f" async="async"></script>
<script type="text/javascript">

	<!-- Google -->
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-504700-32', 'auto');
  ga('send', 'pageview');

	<!-- Tynt -->
	if(document.location.protocol=='http:'){
	 var Tynt=Tynt||[];Tynt.push('aFq4livo0r4yzYacwqm_6r');Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false};
	 (function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
	}
	
	<!-- Quantcast Tag -->
	var _qevents = _qevents || [];
	
	(function() {
	var elem = document.createElement('script');
	elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
	elem.async = true;
	elem.type = "text/javascript";
	var scpt = document.getElementsByTagName('script')[0];
	scpt.parentNode.insertBefore(elem, scpt);
	})();
	
	_qevents.push({
	qacct:"p-49SuwTaH2w2BM"
	});

</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-49SuwTaH2w2BM.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>

<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6032644253807', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6032644253807&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>

<!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3125699,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('http://s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="http://www.histats.com" target="_blank"><img  src="http://sstatic1.histats.com/0.gif?3125699&101" alt="web hit counter" border="0"></a></noscript>



<script src="//fast.wistia.net/labs/fresh-url/v1.js" async></script>
<script type="text/javascript">
( function() {
    var info = getTagInfo();
    var alias = 'edhomeskin_display';
        alias = alias !== '' ? 'alias=' + ( alias.split("_") ? alias.split("_")[ 0 ] : alias ) + '_' + info.alias : '';
    document.write('<scr'+'ipt language="javascript1.1" src="http://' + info.domain + '.adtech.de/addyn/3.0/1370.1/4713596/0/-1/ADTECH;' + alias + ';loc=100;target=_blank;key=;grp=' + info.adgroupid + ';misc=' + new Date().getTime() + '"></scri'+'pt>');
})();</script>


<noscript>Please enable scripting languages in your browser.</noscript>
</body>
</html>
