
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>E-Daily.gr Πολιτική Απορρήτου</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="charset" content="utf-8"/>
<meta http-equiv="Refresh" content="900"/>
<meta http-equiv="Expires" content="0"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta content="Πολιτική Απορρήτου" name="Description"/>
<meta content="" name="keywords"/>
<meta property="og:title" content="E-Daily.gr Πολιτική Απορρήτου"/>
<meta property="og:description" content=""/>
<meta property="og:site_name" content="E-Daily.gr"/>
<meta property="og:type" content="Website"/>
<meta property="og:image" content="http://www.e-daily.gr/_img/fblogo.png"/>
<meta property="fb:app_id" content="1590239487856378"/>
<meta property="og:url" content="http://www.e-daily.gr"/>
<link rel="canonical" href="http://www.e-daily.gr"/>
<link rel="stylesheet" href="/_css/post.css"/>
<link rel="stylesheet" href="/_js/scrollbar/jquery.mCustomScrollbar.css"/>
<meta name="apple-itunes-app" content="app-id=776881885">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="apple-touch-icon" sizes="57x57" href="/_ico/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/_ico/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/_ico/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/_ico/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/_ico/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/_ico/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/_ico/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/_ico/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/_ico/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/_ico/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/_ico/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/_ico/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/_ico/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/_ico/manifest.json">
<link rel="mask-icon" href="/_ico/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/_ico/favicon.ico">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/_ico/mstile-144x144.png">
<meta name="msapplication-config" content="/_ico/browserconfig.xml">
<meta name="theme-color" content="#ff0004">
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,400italic|Roboto+Condensed:300,400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,700&subset=latin,greek' rel='stylesheet' type='text/css'>
<link href="/_img/edailyfont/stylesheet.css" rel="stylesheet" type="text/css"/>
<link href="/_css/style.css" rel="stylesheet" type="text/css"/>
<link href="/_css/navigation.css" rel="stylesheet" type="text/css"/>
<link href="/_css/mobilemenu.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="/_css/structure.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="/_img/edailyicons/css/edaily.css">
<link rel="stylesheet" href="/_img/edailyicons/css/animation.css"><!--[if IE 7]><link rel="stylesheet" href="css/fontello-ie7.css"><![endif]-->
<link rel="stylesheet" type="text/css" href="/_js/mmenu/jquery.ma.infinitypush.css"/>
<script language="javaScript" type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script language="javaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lazysizes/1.0.1/lazysizes.min.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/jquery.popupWindow.js"/></script>
<script language="javaScript" type="text/javascript" src="/_js/mmenu/jquery.ma.infinitypush.js"></script>
<script language="javaScript" type="text/javascript" src="/_js/tools_v2.js"></script>
<script type="text/javascript" src="/_js/helios.js"></script>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
<script type='text/javascript'>
if(document.location.protocol=='http:'){
var Tynt=Tynt||[];Tynt.push('d91U86Bp0r5zpGrkHcnlKl');
Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false,"domain":"e-daily.gr"};
(function(){var h,s=document.createElement('script');s.src='http://cdn.tynt.com/ti.js';
h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();}
</script>
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"http://www.e-daily.gr/privacy.asp","theme":"light-bottom"};
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dt/common/DAC.js"></script>
<script type="text/javascript" src="http://aka-cdn-ns.adtech.de/dac/1370.1/w1122524.js"></script>
<script type="text/javascript" id="ean-native-embed-tag" src="//cdn.elasticad.net/native/serve/js/nativeEmbed.gz.js"></script>
<script src="/_js/tabs/jquery.easytabs.js" type="text/javascript"></script>
<script src="/_js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
</head>
<body>
<script type='text/javascript'>
  var nuggprof = '';
  var nuggtg = encodeURIComponent('__CONTENT_TAG__');
  var nuggrid = encodeURIComponent(top.location.href);
  document.write('<scr'+'ipt type="text/javascript" src="//adweb.nuggad.net/rc?nuggn=1230610253&nuggsid=1022102324&nuggtg='+nuggtg+'&nuggrid='+nuggrid+'"></scr'+'ipt>');
</script>
<div id="wrapper">
<nav id="mobilemenu_pad">
<ul class="sections">
<li><a href="/news">Ειδήσεις</a>
<li><a href="/viral">Viral</a>
<li><a href="/quiz">Quiz</a>
<li><a href="/life">Life</a>
<li><a href="/themata">Θέματα</a>
<li><a href="/entertainment">Ψυχαγωγία</a>
<li><a href="/agenda">E-Agenda</a>
</ul>
<ul class="sections badges">
<li><a href="/omg"><img src="/_img/feeds/101.png" border="0"/> OMG</a>
<li><a href="/lol"><img src="/_img/feeds/106.png" border="0"/> LOL</a>
<li><a href="/woman"><img src="/_img/feeds/112.png" border="0"/> Woman</a>
<li><a href="/alist"><img src="/_img/feeds/91.png" border="0"/> A-List</a>
<li><a href="/cute"><img src="/_img/feeds/109.png" border="0"/> Cute</a>
<li><a href="/retro"><img src="/_img/feeds/110.png" border="0"/> Retro</a>
<li><a href="/juicy"><img src="/_img/feeds/104.png" border="0"/> Juicy</a>
<li><a href="/lgbt"><img src="/_img/feeds/55.png" border="0"/> LGBT</a>
<li><a href="/geeky"><img src="/_img/feeds/108.png" border="0"/> Geeky</a>
<li><a href="/win"><img src="/_img/feeds/111.png" border="0"/> Win</a>
<li><a href="/summer"><img src="/_img/feeds/15.png" border="0"/> Summer</a>
<li><a href="/xmas"><img src="/_img/feeds/14.png" border="0"/> Xmas</a>
<li><a href="/summer"><img src="/_img/feeds/12.png" border="0"/> Easter</a>
</ul>
<ul class="sections topics">
<li><a href="/food">Food</a>
<li><a href="/bodymind">Body+Mind</a>
<li><a href="/travel">Ταξίδια</a>
<li><a href="/style">Style</a>
<li><a href="/spiti">Σπίτι</a>
<li><a href="/family">Family</a>
<li><a href="/sxeseis">Σχέσεις</a>
</ul>
<ul class="sections topics">
<li><a href="/agenda">Agenda</a>
<li><a href="/music">Μουσική</a>
<li><a href="/theater">Θέατρο</a>
<li><a href="/arts">Εικαστικά</a>
<li><a href="/cinema">Σινεμά</a>
<li><a href="/nightlife">Νάιτ Λάιφ</a>
<li><a href="/city">Πόλη</a>
</ul>
<ul class="sections topics">
<li><a href="/weather">Καιρός</a>
</ul>
<div class="social">
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</nav>
<div id="main">
<form id="skinform"></form>
<div id="spider"></div>
<div id="topHeaderWarper">
<div id="topHeader">
<div id="topHeaderA">
<a href="/" class="mainlogo"><img src="/_img/edailygr_logo.png" border="0" alt="E-Daily Τα Νέα της ημέρας και ότι σου κάνει κλικ!"/></a>
</div>
<div id="topHeaderB" class="feedIcons">
<a href="/win"><img src="/_img/feeds/111.png" border="0" alt="Win Feed"/></a>
<a href="/woman"><img src="/_img/feeds/112.png" border="0" alt="Woman Feed"/></a>
<a href="/omg"><img src="/_img/feeds/101.png" border="0" alt="OMG Feed"/></a>
<a href="/lol"><img src="/_img/feeds/106.png" border="0" alt="LOL Feed"/></a>
<a href="/juicy"><img src="/_img/feeds/104.png" border="0" alt="Juicy Feed"/></a>
<a href="/alist"><img src="/_img/feeds/91.png" border="0" alt="A-List Feed"/></a>
<a href="/retro"><img src="/_img/feeds/110.png" border="0" alt="Retro Feed"/></a>
<a href="/lgbt"><img src="/_img/feeds/55.png" border="0" alt="LGBT Feed"/></a>
</div>
</div>
</div>
<div id="topHeaderWarper2">
<ul id="mainTabs">
<div id="mainmenu_logo"><a href="/"><img src="/_img/edailygr_logo.png" border="0" alt="E-Daily Ενημέρωση και Ψυχαγωγία" height="34"/></a></div>
<li id="tabNews">
<div id="tabNewsMenu" class="menuPanelContainer">
<a href="/news" class="menuTab menuTabWhite2">Ειδήσεις</a>
<div class="menuFrame">
<div class="menuOptions">
<a href="/news">ΤΕΛΕΥΤΑΙΑ ΝΕΑ</a>
<a href="/ellada">ΕΛΛΑΔΑ</a>
<a href="/kosmos">ΚΟΣΜΟΣ</a>
<a href="/politiki">ΠΟΛΙΤΙΚΗ</a>
<a href="/oikonomia">ΟΙΚΟΝΟΜΙΑ</a>
<a href="/sports">ΑΘΛΗΤΙΣΜΟΣ</a>
<a href="/epistimi">ΕΠΙΣΤΗΜΗ</a>
<a href="/politismos">ΠΟΛΙΤΙΣΜΟΣ</a>
<a href="http://feeds.feedburner.com/eradioblog" target="_blank">RSS FEED</a>
</div>
</div>
</div>
<a href="/news" class="menuTab menuTabWhite1 ">Ειδήσεις</a>
</li>
<li id="tabThemata">
<div id="tabThemataMenu" class="menuPanelContainer">
<a href="/themata" class="menuTab menuTabWhite2">Θέματα</a>
<div class="menuFrame">
<div class="menuOptions">
<a href="/themata">ΤΕΛΕΥΤΑΙΑ ΘΕΜΑΤΑ</a>
<a href="/afieromata">ΑΦΙΕΡΩΜΑΤΑ</a>
<a href="/people">ΑΝΘΡΩΠΟΙ</a>
<a href="/eikones">ΩΡΑΙΕΣ ΕΙΚΟΝΕΣ</a>
<a href="/istorika">ΙΣΤΟΡΙΚΑ</a>
<a href="/lgbt">LGBT</a>
<a href="/tech">ΤΕΧΝΟΛΟΓΙΑ</a>
<a href="/periballon">ΠΕΡΙΒΑΛΛΟΝ</a>
<a href="/architect">ΑΡΧΙΤΕΚΤΟΝΙΚΗ</a>
</div>
</div>
</div>
<a href="/themata" class="menuTab menuTabWhite1 ">Θέματα</a>
</li>
<li id="tabViral">
<div id="tabViralMenu" class="menuPanelContainer">
<a href="/viral" class="menuTab menuTabWhite2">Viral</a>
<div class="menuFrame">
<div class="menuOptions">
<a href="/omg">OMG</a>
<a href="/lol">LOL</a>
<a href="/tabloid">TABLOID</a>
<a href="/sexy">SEXY</a>
<a href="/zoa">ΖΩΑ</a>
<a href="/media">MEDIA</a>
</div>
</div>
</div>
<a href="/viral" class="menuTab menuTabWhite1 ">Viral</a>
</li>
<li id="tabQuiz">
<div id="tabQuizMenu" class="menuPanelContainer">
<a href="/quiz" class="menuTab menuTabWhite2">Κουίζ</a>
</div>
<a href="/quiz" class="menuTab menuTabWhite1 ">Κουίζ</a>
</li>
<li id="tabLife">
<div id="tabLifeMenu" class="menuPanelContainer">
<a href="/life" class="menuTab menuTabWhite2">Life</a>
<div class="menuFrame">
<div class="menuOptions">
<a href="/life">ΤΕΛΕΥΤΑΙΑ θΕΜΑΤΑ</a>
<a href="/food">FOOD</a>
<a href="/body-mind">BODY+MIND</a>
<a href="/spiti">ΣΠΙΤΙ</a>
<a href="/travel">ΤΑΞΙΔΙΑ</a>
<a href="/style">STYLE</a>
<a href="/family">FAMILY</a>
<a href="/sxeseis">ΣΧΕΣΕΙΣ</a>
<a href="/zodia">ΖΩΔΙΑ</a>
</div>
</div>
</div>
<a href="/life" class="menuTab menuTabWhite1 ">Life</a>
</li>
<li id="tabEntertaiment">
<div id="tabEntertaimentMenu" class="menuPanelContainer">
<a href="/entertainment" class="menuTab menuTabWhite2">Ψυχαγωγία</a>
<div class="menuFrame">
<div class="menuOptions">
<a href="/agenda" rel="section5_1" class="current">ATZENTA ΣΗΜΕΡΑ</a>
<a href="/music" rel="section5_2">ΜΟΥΣΙΚΗ</a>
<a href="/cinema" rel="section5_3">ΣΙΝΕΜΑ</a>
<a href="/theater" rel="section5_4">ΘΕΑΤΡΟ+ΧΟΡΟΣ</a>
<a href="/arts" rel="section5_5">ΕΙΚΑΣΤΙΚΑ</a>
<a href="/city" rel="section5_6">ΠΟΛΗ</a>
<a href="/nightlife" rel="section5_7">ΝΑΪΤ ΛΑΪΦ</a>
</div>
</div>
</div>
<a href="/entertainment" class="menuTab menuTabWhite1 ">Ψυχαγωγία</a>
</li>
<li id="extras">
<a href="/agenda" class="nextwebchannels"><img src="/_img/eagendagr_logo.png" height="20"/></a>
</li>
<li id="extras">
<a href="http://www.e-radio.gr" target="_blank" class="nextwebchannels"><img src="/_img/eradiogr_logo.png" height="20"/></a>
</li>
</ul>
<div class="right_item">
<div class="right_item" style="padding:0">
<a class="icon-search right_item search" href="/search" style="padding:12px 10px 14px 8px; box-sizing:border-box; height:44px"></a>
<a class="icon-twitter-bird right_item twitter" href="http://www.twitter.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px; box-sizing:border-box; height:44px"></a>
<a class="icon-instagram right_item instagram" href="http://www.instagram.com/edailygr" target="_blank" style="padding:12px 8px 14px 8px; box-sizing:border-box; height:44px"></a>
<a class="icon-facebook right_item facebook" href="http://www.facebook.com/edailygr" target="_blank" style="padding:12px 0 14px 10px; box-sizing:border-box; height:44px"></a>
</div>
</div>
</div>
<div id="topHeaderMobile" style="text-align:center;">
<div class="ma-infinitypush-button"><span></span><span></span><span></span><span></span></div>
<a href="/"><img src="/_img/mobilelogo.png" width="136" height="48" alt="E-Daily" border="0"/></a>
<a href="/search" class="right_item" id="tabSearchMobile" style="padding:12px; font-size: 18px;"><div class="icon-search"></div></a>
</div>
<div id="adsBillboard">
<center>
<div id="5799488"></div>
<script>
ADTECH.config.placements[5799488] = {params: { alias: 'edhomebillboard_display', kv72890:"2sSqXk"+ASPQ_2sSqXk, kv970250:"r61GJ2"+ASPQ_r61GJ2, key:'',target: '_blank' }};
ADTECH.enqueueAd(5799488);
ADTECH.executeQueue();
</script>
</center>
</div>
<div class="mainArea">
<div class="mainAreaA">
<h1 class="postTitle">Privacy Policy</h1>
<div class="postBody">
<p>Welcome to E-Daily. This Privacy Policy is effective for E-Daily.gr, E-Daily Mobile Apps (&ldquo;E-Daily&rdquo;, &ldquo;we&rdquo;, &ldquo;us&rdquo; and/or &ldquo;our&rdquo;) and is incorporated by reference into the E-Daily Terms of Service and End User License Agreement. <br/>
The purpose of this Privacy Policy is to inform users of the E-Daily Service (as defined below) about the information that we collect through the E-Daily Service, how we use that information and the ways users can control how that information is used or shared. The entire policy can be found below. For information about users&rsquo; obligations and use of the E-Daily Service, please see our Terms of Service and End User License Agreement. </p>
<p> We will continue to evaluate this Privacy Policy and update it accordingly when there is a change to our business practices, our users&rsquo; needs, and to comply with regulatory and legal requirements. Please check this page periodically for updates. If we make any material changes to this Privacy Policy, we will post the updated Privacy Policy here and notify you by email or by means of a notice on the Site (as defined below) and/or the Applications (as defined below). </p>
<p><strong>The types of information we collect</strong><br/>
When you interact with the E-Daily.gr, we may collect Personal Information (as defined below) and other information from you as described below. <br/>
&ldquo;Personal Information&rdquo; means information that alone or when in connection with other information may be used to readily identify, contact, or locate you, such as: name, address, email address or phone number. We do not consider personally identifiable information to include information that has been anonymized so that it does not allow a third party to easily identify a specific individual. </p>
<p><strong>Information you give us </strong><br/>
To enable you to enjoy certain features of the E-Daily Service, we collect certain types of information, including Personal Information, during your interactions with the E-Daily Service. By using the E-Daily Service, you are authorizing us to gather, parse, and retain data related to the provision of the E-Daily Service. </p>
<p> For example, we collect information, including Personal Information, when you: </p>
<ul type="disc">
<li>Register to use our E-Daily Service;</li>
<li>Login with social networking credentials; </li>
<li>Participate in polls, contests, surveys or other features of the E-Daily Service, or respond to offers or advertisements on the E-Daily Service; </li>
<li>Communicate with us; and</li>
<li>Sign up to receive email newsletters.</li>
</ul>
<p>When you register to use the E-Daily Service, we ask you to provide certain information, which includes your name, email address, birth year, gender, zip code and/or other geographic information, as well as a password for your account. By voluntarily providing us with Personal Information or other information, you are consenting to our use of it in accordance with this Privacy Policy. If you provide any Personal Information or other information, you acknowledge and agree that such Personal Information may be transferred from your current location to our offices and servers and the authorized third parties. </p>
<p><strong>Automatic Data Collection</strong><br/>
<em>Listening and Usage Activity</em>: We keep track of your listening and usage activity. We collect information about stations, favorites, artists, channels, conversations and tracks you have listened to or in which you have expressed an interest. <br/>
<em>Social connect information</em>: When you choose to connect your social media account to your account profile, we collect Personal Information from that social media website. For example, when you connect your Facebook account, we may collect the Personal Information you have made publicly available in Facebook, such as your name, profile picture, cover photo, username, gender, friend networks, age range, locale, friend list, and any other information you have made public. We may also obtain other non-public information from the social network website with your permission. </p>
<p><em>Information about your computer or device</em>: We may also collect information about the computer, mobile or other devices you use to access the E-Daily Service. For example, our servers receive and record information about your computer and browser, including potentially your IP address, device ID, operating system, browser type, and other software or hardware information. If you access the E-Daily Service from a mobile or other device, unless you register, we may assign a unique device identifier for that device. </p>
<p><em>Location-based information</em>: We do not collect your zip code, postal code, city or country unless you choose to provide this information to us at registration. Your general geo-location is collected at regular intervals if you use the E-Daily Service on your mobile device or through the Site. We collect this information to provide content and services that are customized for your geographic area. </p>
<p><em>Tracking technology</em>: We use what are commonly called &quot;cookies&quot; on our Site. A cookie is a piece of information that the computer that hosts our Site gives to your browser when you access the Site. These cookies help us improve the E-Daily Service, provide additional functionality to the Site and help us analyze Site usage more accurately. For instance, our Site may set a cookie on your browser that allows you to access the Site without needing to remember and then enter a password more than once during a visit to the Site. We may also be able to capture other data such as searches conducted and results, date, time, and connection speed. In all cases in which we use cookies, we will not collect Personal Information except with your permission. On most web browsers, you will find a &ldquo;help&rdquo; section on the toolbar. Please refer to this section for information on how to receive notification when you are receiving a new cookie and how to turn cookies off. We recommend that you leave cookies turned on because they allow you to take advantage of some of the Site&rsquo;s features. Third parties, including advertisers and third-party advertising companies, whose products or services are accessible or advertised via the E-Daily Service may also use cookies, and we advise you to check their privacy policies for information about their cookies and other privacy practices. </p>
<p><strong>Our use of the information we collect</strong><br/>
We use the information, including Personal Information, that we collect for the following purposes: </p>
<ul type="disc">
<li>To personalize and customize the advertising and the content you see.</li>
<li>To improve and enhance the user experience. We use tracking information to determine how well each page, station and/or channel performs overall based on aggregate user demographics and traffic patterns to those pages, stations and channels. This helps us build a better service for you.</li>
<li>To fulfill your requests for certain products and services, such as sending out electronic newsletters and enabling you to participate in polls, contests, and boards.</li>
<li>To send you information about topics or content that we think will be of interest to you based on your preferences, habits or geographic location.</li>
<li>To send you information about the latest developments and features on our E-Daily Service.</li>
<li>To provide technical support.</li>
</ul>
<p>We may anonymize and aggregate data collected through the E-Daily Service and use it for any purpose. </p>
<p><strong>How the information we collect is shared</strong><br/>
We are not in the business of selling, renting or sharing Personal Information about you with other people or nonaffiliated companies for their direct marketing purposes. We consider this information to be a vital part of our relationship with you. There are, however, certain circumstances in which we may share your information, including Personal Information, with certain third parties without further notice to you, as set forth below: </p>
<ul type="disc">
<li>Information you allow us to share when you register with us, or through a subsequent affirmative election.</li>
<li>Information used when we hire or partner with third parties to provide business-related services on our behalf, such as mailing information, maintaining databases, processing payments, sweepstakes management and prize fulfillment, data processing, analytics, personalization and customization of advertising, customer/support services and other products or services that we choose to make available to our registered users. When we employ another company to perform a function of this nature, we only provide them with the information that they need to perform their specific function.</li>
<li>We may share, transfer or sell your information in connection with a merger, acquisition, financing due diligence, reorganization, bankruptcy, receivership, sale of company assets, or transition of service to another provider. We cannot control how such entities may use or disclose such information.</li>
<li>We may share information with our subsidiaries and affiliates for purposes consistent with this Privacy Policy.</li>
<li>The E-Daily Service may allow you to share information, including Personal Information, with social networking websites, such as Facebook. We do not share your Personal Information with them unless you direct the E-Daily Service to share it. Their use of the information will be governed by their privacy policies, and you may be able to modify your privacy setting on their websites.</li>
<li>We may share your information if required to do so by law or in the good faith belief that such action is necessary in order to (i) comply with a legal obligation, (ii) protect or defend our legal rights or property as well as those of our business partners, employees, agents and contractors (including enforcement of our agreements); (iii) act in urgent circumstances to protect the personal safety of users or the public; (iv) protect against fraud or risk management purposes; or (v) protect against legal liability.</li>
</ul>
<p>When you register and create a profile through the E-Daily Service, other users of the E-Daily Service may be able to see your profile information, including your screen name, profile photo and any content that you post to the E-Daily Service. We recommend that you guard your anonymity and sensitive information. We are not responsible for the privacy practices of the other users who will view and use the information you post. </p>
<p><strong>Non-Identifiable or Aggregated Data</strong><br/>
We also receive and store certain non-personally identifiable information. Such information, which is collected passively using various technologies, cannot presently be used to specifically identify you. We may store such information itself or such information may be included in databases owned and maintained by our affiliates, agents or service providers. We may use such information and pool it with other information to track, for example, the total number of visitors to the E-Daily Service, the number of visitors to each page of our Site, the domain names of our visitors' Internet service providers, and how our users use and interact with the E-Daily Service. Also, in an ongoing effort to better understand and serve the users of the E-Daily Service, we often conduct research on our customer demographics, interests and behavior based on the personal information and other information provided to us. This research may be compiled and analyzed on an aggregate basis. We may share this non-identifiable and aggregate data with our affiliates, agents and business partners, but this type of non-identifiable and aggregate information does not identify you personally. We may also disclose aggregated user statistics in order to describe our services to current and prospective business partners and to other third parties for other lawful purposes. <br/>
The use and disclosure of this non-personally identifiable information is not subject to restrictions under this Privacy Policy. </p>
<p><strong>Your Choices</strong><br/>
You can visit the Site without providing any personal information. If you choose not to provide any personal information, you may not be able to use certain features of our E-Daily Service. </p>
<p><strong>Exclusions</strong><br/>
This Privacy Policy does not apply to any personal data collected by E-Daily other than personal data collected through the E-Daily Service. This Privacy Policy shall not apply to any unsolicited information you provide to us through the E-Daily Service or through any other means. This includes, but is not limited to, information posted to any public areas of the E-Daily Service, such as bulletin boards (collectively, &ldquo;Public Areas&rdquo;), any ideas for new products or modifications to existing products, and other unsolicited submissions (collectively, &ldquo;Unsolicited Information&rdquo;). All Unsolicited Information shall be deemed to be non-confidential and we shall be free to reproduce, use, disclose, distribute and exploit such Unsolicited Information without limitation or attribution. </p>
<p><strong>How we protect your information</strong><br/>
We have taken reasonable steps to protect the information you provide via the E-Daily Service from loss, misuse, and unauthorized access, disclosure, alteration, or destruction. However, no Internet, email or other electronic transmission is ever fully secure or error free, so you should take special care in deciding what information you send to us in this way. We do not accept liability for unintentional disclosure. <br/>
By using the E-Daily Service or providing Personal Information to us, you agree that we may communicate with you electronically regarding security, privacy, and administrative issues relating to your use of the E-Daily Service. If we learn of a security system&rsquo;s breach, we may attempt to notify you electronically by posting a notice on the Site and/or Applications or sending an email to you. </p>
<p><strong>Consent to processing</strong><br/>
If you use the E-Daily Service outside of the European Union, you fully understand and unambiguously consent to the transfer of your information, including Personal Information, to, and the collection and processing of such information in Greece and other countries or territories of the European Union. The laws on holding such information in European Union may vary and be less stringent than laws of your state or country. </p>
<p><strong>Children</strong><br/>
We do not knowingly collect Personal Information from children under the age of 13. If you are under the age of 13, please do not submit any Personal Information through the E-Daily Service. We encourage parents and legal guardians to monitor their children&rsquo;s Internet usage and to help enforce our Privacy Policy by instructing their children never to provide Personal Information through the E-Daily Service without their permission. If you have reason to believe that a child under the age of 13 has provided Personal Information to us through our E-Daily, please contact us, and we will endeavor to delete that information from our databases. </p>
<p><strong>Links to Other Web Sites</strong><br/>
This Privacy Policy applies only to the E-Daily Service. The E-Daily Service may contain links to other web sites not operated or controlled by us (the &ldquo;Third Party Sites&rdquo;). The policies and procedures we described here do not apply to the Third Party Sites. The links from the E-Daily Service do not imply that we endorse or have reviewed the Third Party Sites. We suggest contacting those sites directly for information on their privacy policies. </p>
<p><strong>What are my privacy options and how can I change my settings?</strong><br/>
<em>To modify your registration information:</em> Once you have registered, you can change your registration information (including your email address and password). <br/>
<em>Opt out:</em> Each email communication we send you will contain instructions permitting you to &quot;opt-out&quot; of receiving future marketing communications. In addition, if at any time you wish not to receive any future communications or you wish to have your name deleted from our email mailing lists, please contact us as indicated below. Note however, that certain communications, such as push notifications, require you to change the settings on your device if at any time you wish not to receive any future communications. Note that on occasion we send service-related announcements, such as disruption notifications, and you may not be able to opt-out of these communications. </p>
<p><strong>English language version shall control</strong><br/>
The English language version of this Privacy Policy is the version that governs your use of the E-Daily Service and in the event of any conflict between the English language version and a translated version, the English language version will control.</p>
<p> To print out this Statement, you may visit www.e-daily.gr/privacy.asp</p>
</div>
</div>
<div class="mainArea2">
<center>
<div id="5799491" style="margin-bottom: 20px;"></div>
<script>
ADTECH.config.placements[5799491] = {params: { alias: '', kv300250:"KhHQr6"+ASPQ_KhHQr6, kv300600:"fMDwBs"+ASPQ_fMDwBs, key:'',target: '_blank' }};
ADTECH.enqueueAd(5799491);
ADTECH.executeQueue();
</script>
</center>
<br>
<script language="javascript">
$().ready(function() {
    $("#todayTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  updateHash: false
	});
/*	$("#tabs-hl").mCustomScrollbar({
		theme:"dark-thick"
	});
*/
	$("#tabs-recent").mCustomScrollbar({
		theme:"dark-thick"
	});
});

</script>
<div class="postsRedHeader">ΔΙΑΒΑΣΤΕ ΣΗΜΕΡΑ</div>
<div id="todayTabs" class="tab-container">
<ul class="etabs">
<li class="tab"><a href="#tabs-hl">ΕΠΙΛΟΓΕΣ</a></li>
<li class="tab"><a href="#tabs-recent">ΠΡΟΣΦΑΤΑ</a></li>
</ul>
<div id="tabs-hl" class="postEntries">
<a href="/news/69230/triplh-seismikh-donhsh-sthn-astypalaia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/astyp-seismos.jpg" alt="Τριπλή σεισμική δόνηση στην Αστυπάλαια" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">14/11/2016</span></div>
<h3>Τριπλή σεισμική δόνηση στην Αστυπάλαια</h3>
<h4>Διαδοχικοί σεισμοί μέσα σε μόλις 22 λεπτά</h4>
</a>
<a href="/viral/69229/giagia-sto-agrinio-exei-gia-katoikidio-ena-zarkadi-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zarkad-735x459.jpg" alt="Γιαγιά στο Αγρίνιο έχει για κατοικίδιο ένα ζαρκάδι" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΑ</span><span class="when">21:27</span></div>
<h3>Γιαγιά στο Αγρίνιο έχει για κατοικίδιο ένα ζαρκάδι</h3>
<h4>Το ζώο μεγαλώνει στο σπίτι μαζί της</h4>
</a>
<a href="/viral/69226/o-ntemhs-apefyge-ton-tsipra-sto-karaiskakh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/demis-9399434.jpg" alt="Ο Ντέμης απέφυγε τον Τσίπρα στο Καραϊσκάκη" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">20:46</span></div>
<h3>Ο Ντέμης απέφυγε τον Τσίπρα στο Καραϊσκάκη</h3>
<h4>Αρκέστηκε σε μια τυπική χειραψία με τον πρωθυπουργό</h4>
</a>
<a href="/viral/69225/yposxethhke-na-faei-entoma-an-nikhsei-o-tramp-kai-to-ekane" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/Clipboard028.jpg" alt="Υποσχέθηκε να φάει έντομα αν νικήσει ο Τραμπ και το έκανε" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">20:30</span></div>
<h3>Υποσχέθηκε να φάει έντομα αν νικήσει ο Τραμπ και το έκανε</h3>
<h4>Ο εκλογικός αναλυτής Σαμ Γουάνγκ έχασε το στοίχημα και το πλήρωσε</h4>
</a>
<a href="/themata/69223/facebook-xwris-pseytikes-eidhseis-yposxetai-o-zoykermpergk" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/facebook_4_0.jpg" alt="Facebook χωρίς ψεύτικες ειδήσεις υπόσχεται o Ζούκερμπεργκ" border="0" class="lazyload">
</div>
<div><span class="category">ΤΕΧΝΟΛΟΓΙΑ</span><span class="when">19:54</span></div>
<h3>Facebook χωρίς ψεύτικες ειδήσεις υπόσχεται o Ζούκερμπεργκ</h3>
<h4>«Δεν θέλουμε αναληθείς ειδήσεις στο Facebook»</h4>
</a>
<a href="/themata/69219/panselhnos-kai-nea-selhnh-prokaloyn-megaloys-seismoys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/panselinos-099344.jpg" alt="Πανσέληνος και Νέα Σελήνη προκαλούν μεγάλους σεισμούς" border="0" class="lazyload">
</div>
<div><span class="category">ΕΠΙΣΤΗΜΗ</span><span class="when">19:18</span></div>
<h3>Πανσέληνος και Νέα Σελήνη προκαλούν μεγάλους σεισμούς</h3>
<h4>Και δεν έχει καμία σχέση με την αστρολογία</h4>
</a>
<a href="/viral/69214/terastio-fidi-epese-mesa-se-estiatorio-apo-thn-orofh-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/0fgjhsa146hdm8nu8.274195a2.jpg" alt="Τεράστιο φίδι έπεσε μέσα σε εστιατόριο από την οροφή" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">18:22</span></div>
<h3>Τεράστιο φίδι έπεσε μέσα σε εστιατόριο από την οροφή</h3>
<h4>Οι τεχνικοί πήγαν για διαρροή και βρήκαν το ερπετό</h4>
</a>
<a href="/viral/69209/etsi-einai-sto-eswteriko-toy-to-idiwtiko-tzet-toy-tramp-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/trump_jet_main.jpg" alt="Έτσι είναι στο εσωτερικό του το ιδιωτικό τζετ του Τραμπ" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">17:14</span></div>
<h3>Έτσι είναι στο εσωτερικό του το ιδιωτικό τζετ του Τραμπ</h3>
<h4>Έχει χρυσό μέχρι και στον νεροχύτη</h4>
</a>
</div>
<div id="tabs-recent" class="postEntries noimg" style="height:600px; overflow:hidden">
<a href="/viral/69229/giagia-sto-agrinio-exei-gia-katoikidio-ena-zarkadi-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/zarkad-735x459.jpg" alt="Γιαγιά στο Αγρίνιο έχει για κατοικίδιο ένα ζαρκάδι" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΑ</span><span class="when">21:27</span></div>
<h3>Γιαγιά στο Αγρίνιο έχει για κατοικίδιο ένα ζαρκάδι</h3>
<h4>Το ζώο μεγαλώνει στο σπίτι μαζί της</h4>
</a>
<a href="/news/69227/to-iatriko-anakoinwthen-gia-thn-katastash-toy-thanoy-pleyrh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/thanos-plevris-99923.jpg" alt="Το ιατρικό ανακοινωθέν για την κατάσταση του Θάνου Πλεύρη" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">20:55</span></div>
<h3>Το ιατρικό ανακοινωθέν για την κατάσταση του Θάνου Πλεύρη</h3>
<h4>Σε μηχανική υποστήριξη και ιδιαίτερα κρίσιμη κατάσταση</h4>
</a>
<a href="/viral/69226/o-ntemhs-apefyge-ton-tsipra-sto-karaiskakh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/demis-9399434.jpg" alt="Ο Ντέμης απέφυγε τον Τσίπρα στο Καραϊσκάκη" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">20:46</span></div>
<h3>Ο Ντέμης απέφυγε τον Τσίπρα στο Καραϊσκάκη</h3>
<h4>Αρκέστηκε σε μια τυπική χειραψία με τον πρωθυπουργό</h4>
</a>
<a href="/viral/69225/yposxethhke-na-faei-entoma-an-nikhsei-o-tramp-kai-to-ekane" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/Clipboard028.jpg" alt="Υποσχέθηκε να φάει έντομα αν νικήσει ο Τραμπ και το έκανε" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">20:30</span></div>
<h3>Υποσχέθηκε να φάει έντομα αν νικήσει ο Τραμπ και το έκανε</h3>
<h4>Ο εκλογικός αναλυτής Σαμ Γουάνγκ έχασε το στοίχημα και το πλήρωσε</h4>
</a>
<a href="/themata/69223/facebook-xwris-pseytikes-eidhseis-yposxetai-o-zoykermpergk" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/facebook_4_0.jpg" alt="Facebook χωρίς ψεύτικες ειδήσεις υπόσχεται o Ζούκερμπεργκ" border="0" class="lazyload">
</div>
<div><span class="category">ΤΕΧΝΟΛΟΓΙΑ</span><span class="when">19:54</span></div>
<h3>Facebook χωρίς ψεύτικες ειδήσεις υπόσχεται o Ζούκερμπεργκ</h3>
<h4>«Δεν θέλουμε αναληθείς ειδήσεις στο Facebook»</h4>
</a>
<a href="/news/69221/oi-stathmoi-metro-poy-tha-kleisoyn-trith-kai-tetarth" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/syntagma-kleisto-03994423.jpg" alt="Οι σταθμοί μετρό που θα κλείσουν Τρίτη και Τετάρτη" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">19:36</span></div>
<h3>Οι σταθμοί μετρό που θα κλείσουν Τρίτη και Τετάρτη</h3>
<h4>Θα κλέισει και το τραμ εξαιτίας της επίσκεψης Ομπάμα</h4>
</a>
<a href="/themata/69219/panselhnos-kai-nea-selhnh-prokaloyn-megaloys-seismoys" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/panselinos-099344.jpg" alt="Πανσέληνος και Νέα Σελήνη προκαλούν μεγάλους σεισμούς" border="0" class="lazyload">
</div>
<div><span class="category">ΕΠΙΣΤΗΜΗ</span><span class="when">19:18</span></div>
<h3>Πανσέληνος και Νέα Σελήνη προκαλούν μεγάλους σεισμούς</h3>
<h4>Και δεν έχει καμία σχέση με την αστρολογία</h4>
</a>
<a href="/news/69217/mpoynies-sto-oykraniko-koinovoylio-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ukrainian-mps-throw-punches-over-accusations-of-kremlin-linksa-00_00_29_15-still024.jpg" alt="Μπουνιές στο ουκρανικό κοινοβούλιο" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">18:43</span></div>
<h3>Μπουνιές στο ουκρανικό κοινοβούλιο</h3>
<h4>Ξύλο για τον Πούτιν</h4>
</a>
<a href="/news/69215/shmaia-twn-ethnikistwn-tsetnik-ston-agwna-me-thn-vosnia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/162214-735x450.jpg" alt="Σημαία των εθνικιστών Τσέτνικ στον αγώνα με την Βοσνία" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">18:29</span></div>
<h3>Σημαία των εθνικιστών Τσέτνικ στον αγώνα με την Βοσνία</h3>
<h4>Συντονισμένη δράση εθνικιστικών στοιχείων κατά το παιχνίδι της Εθνικής με την Βοσνία</h4>
</a>
<a href="/viral/69214/terastio-fidi-epese-mesa-se-estiatorio-apo-thn-orofh-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/0fgjhsa146hdm8nu8.274195a2.jpg" alt="Τεράστιο φίδι έπεσε μέσα σε εστιατόριο από την οροφή" border="0" class="lazyload">
</div>
<div><span class="category">ΠΑΡΑΞΕΝΑ</span><span class="when">18:22</span></div>
<h3>Τεράστιο φίδι έπεσε μέσα σε εστιατόριο από την οροφή</h3>
<h4>Οι τεχνικοί πήγαν για διαρροή και βρήκαν το ερπετό</h4>
</a>
<a href="/news/69210/h-apanthsh-ths-ethnikhs-gia-to-apotropiastiko-pano-gia-thn-sremprenitsa-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/pano-0394254634.jpg" alt="Η απάντηση της Εθνικής για το αποτροπιαστικό πανό για την Σρεμπρένιτσα" border="0" class="lazyload">
</div>
<div><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span><span class="when">17:17</span></div>
<h3>Η απάντηση της Εθνικής για το αποτροπιαστικό πανό για την Σρεμπρένιτσα</h3>
<h4>Οι ποδοσφαιριστές έδωσαν την δική τους απάντηση στην ενέργεια ενός ανεγκέφαλου </h4>
</a>
<a href="/viral/69209/etsi-einai-sto-eswteriko-toy-to-idiwtiko-tzet-toy-tramp-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/trump_jet_main.jpg" alt="Έτσι είναι στο εσωτερικό του το ιδιωτικό τζετ του Τραμπ" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">17:14</span></div>
<h3>Έτσι είναι στο εσωτερικό του το ιδιωτικό τζετ του Τραμπ</h3>
<h4>Έχει χρυσό μέχρι και στον νεροχύτη</h4>
</a>
<a href="/viral/69207/ti-lene-kalyvatshs-paylidoy-gia-ta-dhmosieymata-peri-sxeshs" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/pavlidou_kalivatsis_1.jpg" alt="Τι λένε Καλυβάτσης-Παυλίδου για τα δημοσιεύματα περί σχέσης" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span><span class="when">16:56</span></div>
<h3>Τι λένε Καλυβάτσης-Παυλίδου για τα δημοσιεύματα περί σχέσης</h3>
<h4>Φαίνεται πως το ειδύλλιο δεν ισχύει</h4>
</a>
<a href="/viral/69206/o-sakhs-tanimanidhs-paradexetai-sthn-kamera-eimai-epwteymenos-mazi-soy-na-to-ksereis-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tanimanidhs_camera_1.jpg" alt="Ο Σάκης Τανιμανίδης παραδέχεται στην κάμερα: «Είμαι εpωτευμένος μαζί σου να το ξέρεις»" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span><span class="when">16:44</span></div>
<h3>Ο Σάκης Τανιμανίδης παραδέχεται στην κάμερα: «Είμαι εpωτευμένος μαζί σου να το ξέρεις»</h3>
<h4>Μέλι στάζει ο παρουσιαστής</h4>
</a>
<a href="/news/69205/mhtera-aytoktonhse-dyo-evdomades-meta-ton-thanato-toy-8xronoy-gioy-ths-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mhtera_gios_main.jpg" alt="Μητέρα αυτοκτόνησε δυο εβδομάδες μετά τον θάνατο του 8χρονου γιου της" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">16:29</span></div>
<h3>Μητέρα αυτοκτόνησε δυο εβδομάδες μετά τον θάνατο του 8χρονου γιου της</h3>
<h4>Ο 8χρονος πέθανε από φωτιά που ξέσπασε στο σπίτι τους</h4>
</a>
<a href="/news/69196/ayto-tha-einai-to-programma-toy-mparak-ompama-sthn-athhna" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/obama_programma_1.jpg" alt="Αυτό θα είναι το πρόγραμμα του Μπαράκ Ομπάμα στην Αθήνα" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">15:57</span></div>
<h3>Αυτό θα είναι το πρόγραμμα του Μπαράκ Ομπάμα στην Αθήνα</h3>
<h4>Οι συναντήσεις θα ξεκινήσουν στις 13:30 από το προεδρικό μέγαρο</h4>
</a>
<a href="/news/69184/sthn-entatikh-o-thanos-pleyrhs" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/plevris_1.jpg" alt="Στην εντατική ο Θάνος Πλεύρης" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span><span class="when">14:49</span></div>
<h3>Στην εντατική ο Θάνος Πλεύρης</h3>
<h4>Η κατάσταση της υγείας του σύμφωνα με οικείους του είναι πολύ σοβαρή</h4>
</a>
<a href="/entertainment/69178/epilegoyme-thn-katallhlh-moysikh-gia-na-deite-to-supermoon" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-headphones"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/SupermoonAthens.jpg" alt="Επιλέγουμε την κατάλληλη μουσική για να δείτε το Supermoon" border="0" class="lazyload">
</div>
<div><span class="category">MUSIC LISTS</span><span class="when">14:19</span></div>
<h3>Επιλέγουμε την κατάλληλη μουσική για να δείτε το Supermoon</h3>
<h4>Μια playlist εμπνευσμένη από νύχτες με φεγγάρι</h4>
</a>
<a href="/viral/69166/h-tonia-swthropoyloy-magepse-to-ntoympai-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tonaida2.jpg" alt="Η Τόνια Σωτηροπούλου μάγεψε το Ντουμπάι" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span><span class="when">12:34</span></div>
<h3>Η Τόνια Σωτηροπούλου μάγεψε το Ντουμπάι</h3>
<h4>Η ελληνίδα καλλονή με υπέρλαμπρη τουαλέτα </h4>
</a>
<a href="/news/69164/proponhtria-se-lykeio-ekane-seks-me-mathhtes-kai-toys-estelne-gumnes-fwtografies-ths-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/proponitr23.jpg" alt="Προπονήτρια σε Λύκειο έκανε σeξ με μαθητές και τους έστελνε γuμνές φωτογραφίες της" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span><span class="when">12:17</span></div>
<h3>Προπονήτρια σε Λύκειο έκανε σeξ με μαθητές και τους έστελνε γuμνές φωτογραφίες της</h3>
<h4>Νέο περιστατικό στις ΗΠΑ</h4>
</a>
</div>
</div>
<br>
<center>
<div id="5799492"></div>
<script>
ADTECH.config.placements[5799492] = {params: { alias: '', kv300250:"KhHQr6"+ASPQ_KhHQr6, kv300600:"fMDwBs"+ASPQ_fMDwBs, key:'',target: '_blank' }};
ADTECH.enqueueAd(5799492);
ADTECH.executeQueue();
</script>
</center>
<br>
<script>
$().ready(function() {
    $("#topTabs").easytabs({
	  animate: true,
	  animationSpeed: "fast",
	  defaultTab: "li#ttab-themata",
	  updateHash: false
	});
/*
	$("#tabs-news").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-viral").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-life").mCustomScrollbar({
		theme:"dark-thick"
	});
	$("#tabs-themata").mCustomScrollbar({
		theme:"dark-thick"
	});
*/	
});

</script>
<div class="postsRedHeader">ΔΗΜΟΦΙΛΗ</div>
<div id="topTabs" class="tab-container">
<ul class="etabs">
<li class="tab" id="ttab-news"><a href="#tabs-news">ΕΙΔΗΣΕΙΣ</a></li>
<li class="tab" id="ttab-themata"><a href="#tabs-themata">θΕΜΑΤΑ</a></li>
<li class="tab" id="ttab-life"><a href="#tabs-life">LIFE</a></li>
<li class="tab" id="ttab-viral"><a href="#tabs-viral">VIRAL</a></li>
</ul>
<div id="tabs-news" class="postEntries top">
<a href="/news/69146/allazoyn-ta-panta-gia-1-ekat-eleytheroys-epaggelmaties" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/oaee.jpg" alt="Αλλάζουν τα πάντα για 1 εκατ. ελεύθερους επαγγελματίες" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Αλλάζουν τα πάντα για 1 εκατ. ελεύθερους επαγγελματίες</h3>
<h4>Οι 7 ανατροπές στο ασφαλιστικό</h4>
</a>
<a href="/news/69164/proponhtria-se-lykeio-ekane-seks-me-mathhtes-kai-toys-estelne-gumnes-fwtografies-ths-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/proponitr23.jpg" alt="Προπονήτρια σε Λύκειο έκανε σeξ με μαθητές και τους έστελνε γuμνές φωτογραφίες της" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Προπονήτρια σε Λύκειο έκανε σeξ με μαθητές και τους έστελνε γuμνές φωτογραφίες της</h3>
<h4>Νέο περιστατικό στις ΗΠΑ</h4>
</a>
<a href="/news/69141/xrysos-vathmos-gia-thn-ethnikh-me-fwtovolida-toy-tzavella-sth-lhksh-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ellada_vosnia_0..jpg" alt="«Χρυσός» βαθμός για την Εθνική με «φωτοβολίδα» του Τζαβέλλα στη λήξη" border="0" class="lazyload">
</div>
<div><span class="category">ΑΘΛΗΤΙΣΜΟΣ</span></div>
<h3>«Χρυσός» βαθμός για την Εθνική με «φωτοβολίδα» του Τζαβέλλα στη λήξη</h3>
<h4>Σκληρός αγώνας με επεισόδια, ξύλο και δύο κόκκινες</h4>
</a>
<a href="/news/69205/mhtera-aytoktonhse-dyo-evdomades-meta-ton-thanato-toy-8xronoy-gioy-ths-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mhtera_gios_main.jpg" alt="Μητέρα αυτοκτόνησε δυο εβδομάδες μετά τον θάνατο του 8χρονου γιου της" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Μητέρα αυτοκτόνησε δυο εβδομάδες μετά τον θάνατο του 8χρονου γιου της</h3>
<h4>Ο 8χρονος πέθανε από φωτιά που ξέσπασε στο σπίτι τους</h4>
</a>
<a href="/amerikanikes-ekloges/69156/o-ntonalnt-tramp-apokalypse-pws-den-tha-dexthei-ton-mistho-toy-proedroy-twn-hpa" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/trump_salary_1.jpg" alt="Ο Ντόναλντ Τραμπ αποκάλυψε πως δεν θα δεχθεί τον μισθό του προέδρου των ΗΠΑ!" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Ο Ντόναλντ Τραμπ αποκάλυψε πως δεν θα δεχθεί τον μισθό του προέδρου των ΗΠΑ!</h3>
<h4>«Όχι, δεν πρόκειται να λάβω τον μισθό. Δεν τον παίρνω!» απάντησε έντονα σε δημοσιογράφο</h4>
</a>
<a href="/news/69184/sthn-entatikh-o-thanos-pleyrhs" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/plevris_1.jpg" alt="Στην εντατική ο Θάνος Πλεύρης" border="0" class="lazyload">
</div>
<div><span class="category">ΠΟΛΙΤΙΚΗ</span></div>
<h3>Στην εντατική ο Θάνος Πλεύρης</h3>
<h4>Η κατάσταση της υγείας του σύμφωνα με οικείους του είναι πολύ σοβαρή</h4>
</a>
<a href="/news/69140/perimene-23-xronia-gia-th-dikastikh-apofash-kai-twra-zhta-apozhmiwsh" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/dikastirio3.jpg" alt="Περίμενε 23 χρόνια για τη δικαστική απόφαση και τώρα ζητά αποζημίωση" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Περίμενε 23 χρόνια για τη δικαστική απόφαση και τώρα ζητά αποζημίωση</h3>
<h4>Στο ΣτΕ προσέφυγε η κόρη του δικηγόρου Μπάκα που είχε δολοφονηθεί στο Στρατοδικείο</h4>
</a>
<a href="/news/69150/h-nea-zhlandia-meta-ton-seismo-twn-78-rixter-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/new_zealand_main.jpg" alt="Η Νεα Ζηλανδία μετά τον σεισμό των 7,8 Ρίχτερ" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Η Νεα Ζηλανδία μετά τον σεισμό των 7,8 Ρίχτερ</h3>
<h4>Σε 2 δισ. δολάρια εκτιμούνται οι ζημιές</h4>
</a>
</div>
<div id="tabs-themata" class="postEntries top">
<a href="/amerikanikes-ekloges/68742/4-dekaeties-zwhs-toy-ntonalt-tramp-mesa-apo-agnwstes-fwtografies-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tramp9023.jpg" alt="4 δεκαετίες ζωής  του Ντόναλτ Τραμπ μέσα από άγνωστες φωτογραφίες" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>4 δεκαετίες ζωής του Ντόναλτ Τραμπ μέσα από άγνωστες φωτογραφίες</h3>
<h4>Πως ήταν ο νέος 45ος Πρόεδρος των ΗΠΑ</h4>
</a>
<a href="/themata/68761/ta-pio-asteia-eklogika-synthhmata-poy-exoyn-akoystei-sthn-ellada" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/pasokeklog33.jpg" alt="Τα πιο αστεία εκλογικά συνθήματα που έχουν ακουστεί στην Ελλάδα" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Τα πιο αστεία εκλογικά συνθήματα που έχουν ακουστεί στην Ελλάδα</h3>
<h4>Μια κωμωδία μόνα τους</h4>
</a>
<a href="/amerikanikes-ekloges/68753/h-pio-omorfh-prwth-kyria-sthn-istoria-toy-leykoy-oikoy-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/melania988.jpg" alt="Η πιο όμορφη Πρώτη Κυρία στην ιστορία του Λευκού Οίκου" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Η πιο όμορφη Πρώτη Κυρία στην ιστορία του Λευκού Οίκου</h3>
<h4>Το μοντέλο που κέρδισε τον Ντόναλντ Τραμπ</h4>
</a>
<a href="/themata/69011/h-profhteia-toy-nostradamoy-gia-ton-tramp-kai-thn-ellada" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/nostradamus9.jpg" alt="Η «προφητεία» του Νοστράδαμου... για τον Τραμπ και την Ελλάδα!" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Η «προφητεία» του Νοστράδαμου... για τον Τραμπ και την Ελλάδα!</h3>
<h4>Χαμός στο Διαδίκτυο, δημοσιεύματα και στον βρετανικό Τύπο!</h4>
</a>
<a href="/themata/68731/oi-antidraseis-toy-internet-sth-nikh-toy-tramp-pics-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/trumpfailwin245.jpg" alt="Οι αντιδράσεις του internet στη νίκη του Τραμπ" border="0" class="lazyload">
</div>
<div><span class="category">ΧΙΟΥΜΟΡ</span></div>
<h3>Οι αντιδράσεις του internet στη νίκη του Τραμπ</h3>
<h4>Twitter, Facebook και ένας πανικός από posts</h4>
</a>
<a href="/themata/68873/o-leykos-oikos-sto-eswteriko-toy-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/miniaturewhitehouse.jpg" alt="Ο Λευκός Οίκος στο εσωτερικό του" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Ο Λευκός Οίκος στο εσωτερικό του</h3>
<h4>Πως είναι διακοσμημένο το δημοφιλέστερο σπίτι στο κόσμο</h4>
</a>
<a href="/themata/68921/to-syriako-spiti-poy-sokare-toys-pelates-toy-ikea-pics-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/ikea-syr.jpg" alt="Το συριακό σπίτι που σόκαρε τους πελάτες του ΙΚΕΑ" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Το συριακό σπίτι που σόκαρε τους πελάτες του ΙΚΕΑ</h3>
<h4>Μια ισχυρή καμπάνια ευαισθητοποίησης για τις φρικτές συνθήκες διαβίωσης των προσφύγων</h4>
</a>
<a href="/themata/68645/to-nhsi-poy-kathe-6-mhnes-anhkei-alloy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/phaesant2.jpg" alt="Το νησί που κάθε 6 μήνες ανήκει αλλού" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Το νησί που κάθε 6 μήνες ανήκει αλλού</h3>
<h4>Από το 1659 το πιο περίεργο καθεστώς </h4>
</a>
</div>
<div id="tabs-life" class="postEntries top">
<a href="/life/68817/5-kathhmerina-antikeimena-poy-den-gnwrizete-thn-xrhsimothta-toys-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/BIC_Pen.jpg" alt="5 Καθημερινά Αντικείμενα που δεν γνωρίζετε την χρησιμότητα τους" border="0" class="lazyload">
</div>
<div><span class="category">ΤΕΧΝΟΛΟΓΙΑ</span></div>
<h3>5 Καθημερινά Αντικείμενα που δεν γνωρίζετε την χρησιμότητα τους</h3>
<h4>Σε ένα video από το E-Daily.gr</h4>
</a>
<a href="/life/68670/pws-ftiaxnetai-to-spitiko-salepi-rofhma" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/salepitzis673.jpg" alt="Πώς φτιάχνεται το σπιτικό σαλέπι ρόφημα" border="0" class="lazyload">
</div>
<div><span class="category">FOOD</span></div>
<h3>Πώς φτιάχνεται το σπιτικό σαλέπι ρόφημα</h3>
<h4>Σαλεπάκι ζεστό</h4>
</a>
<a href="/life/68678/h-sokolata-toblerone-allazei-sxhma" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/toblerone.jpg" alt="Η σοκολάτα Toblerone αλλάζει σχήμα!" border="0" class="lazyload">
</div>
<div><span class="category">FOOD</span></div>
<h3>Η σοκολάτα Toblerone αλλάζει σχήμα!</h3>
<h4>Η εταιρεία εξήγησε πως το δίλημμα ήταν αλλαγή σχήματος ή αύξηση της τιμής </h4>
</a>
<a href="/life/68938/to-mysthriwdes-toynel-toy-19oy-aiwna-katw-apo-thn-napolh-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/naples_underground_main.jpg" alt="Το μυστηριώδες τούνελ του 19ου αιώνα κάτω από την Νάπολη" border="0" class="lazyload">
</div>
<div><span class="category">ΤΑΞΙΔΙΑ</span></div>
<h3>Το μυστηριώδες τούνελ του 19ου αιώνα κάτω από την Νάπολη</h3>
<h4>Γιατί σήμερα περιέχει vintage αυτοκίνητα και παμπάλαιες μοτοσικλέτες;</h4>
</a>
<a href="/life/69030/to-panemorfo-ksenodoxeio-dasos-sthn-kina-poy-kalyptetai-apo-dentra-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/hotel_china_main.jpg" alt="Το πανέμορφο ξενοδοχείο – δάσος στην Κίνα που καλύπτεται από δέντρα" border="0" class="lazyload">
</div>
<div><span class="category">ΤΑΞΙΔΙΑ</span></div>
<h3>Το πανέμορφο ξενοδοχείο – δάσος στην Κίνα που καλύπτεται από δέντρα</h3>
<h4>Οι πελάτες θα έχουν την αίσθηση ότι κοιμούνται στη φύση χωρίς να χάνουν τις ανέσεις τους</h4>
</a>
<a href="/life/68744/ta-zwdia-shmera-10-noemvrioy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/womanmanlovehook.jpg" alt="Τα ζώδια σήμερα: 10 Νοεμβρίου" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΔΙΑ</span></div>
<h3>Τα ζώδια σήμερα: 10 Νοεμβρίου</h3>
<h4>Αστρολογικές προβλέψεις κάθε μέρα από το E-Daily.gr</h4>
</a>
<a href="/life/68666/an-exeis-xamhlh-aytopepoithhsh-tote-anagnwrizeis-ayta-ta-shmadia" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/aytopepi54.jpg" alt="Αν έχεις χαμηλή αυτοπεποίθηση, τότε αναγνωρίζεις αυτά τα σημάδια" border="0" class="lazyload">
</div>
<div><span class="category">BODY+MIND</span></div>
<h3>Αν έχεις χαμηλή αυτοπεποίθηση, τότε αναγνωρίζεις αυτά τα σημάδια</h3>
<h4>Καντε restart</h4>
</a>
<a href="/life/68964/ta-zwdia-shmera-12-noemvrioy" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/atlascutouts.jpg" alt="Τα ζώδια σήμερα: 12 Νοεμβρίου" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΔΙΑ</span></div>
<h3>Τα ζώδια σήμερα: 12 Νοεμβρίου</h3>
<h4>Αστρολογικές προβλέψεις κάθε μέρα από το E-Daily.gr</h4>
</a>
</div>
<div id="tabs-viral" class="postEntries top">
<a href="/entertainment/69101/krites-diekopsan-diagwnizomenh-giati-den-pisteyan-oti-exei-toso-yperoxh-fwnh-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/aida.jpg" alt="Κριτές διέκοψαν διαγωνιζόμενη γιατί δεν πίστευαν ότι έχει τόσο υπέροχη φωνή" border="0" class="lazyload">
</div>
<div><span class="category">ΜΟΥΣΙΚΗ</span></div>
<h3>Κριτές διέκοψαν διαγωνιζόμενη γιατί δεν πίστευαν ότι έχει τόσο υπέροχη φωνή</h3>
<h4>Νόμιζαν ότι είναι στημένο</h4>
</a>
<a href="/viral/36107/nearh-kanei-stomatiko-se-filh-ths-mesa-se-treno-kai-ginetai-viral-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2015/lesbians-train-pda.jpg" alt="Νεαρή κάνει στοματικό σε φίλη της μέσα σε τρένο και γίνεται viral" border="0" class="lazyload">
</div>
<div><span class="category">SEXY</span></div>
<h3>Νεαρή κάνει στοματικό σε φίλη της μέσα σε τρένο και γίνεται viral</h3>
<h4>Χαλαρές, στη γαλαρία του βαγονιού</h4>
</a>
<a href="/viral/69073/mhtera-egine-aspida-gia-to-2xrono-paidi-ths-otan-toys-epitethhkan-dyo-skylia-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/skylia_mhtera_1.jpg" alt="Μητέρα έγινε ασπίδα για το 2χρονο παιδί της όταν τους επιτέθηκαν δυο σκυλιά" border="0" class="lazyload">
</div>
<div><span class="category">ΚΟΣΜΟΣ</span></div>
<h3>Μητέρα έγινε ασπίδα για το 2χρονο παιδί της όταν τους επιτέθηκαν δυο σκυλιά</h3>
<h4>Δείτε το βίντεο</h4>
</a>
<a href="/viral/69036/apokalyfthhke-onair-to-sththos-ispanidas-thleparoysiastrias-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/161111_TaniaLlasera.jpg" alt="Αποκαλύφθηκε onair το στήθος Ισπανίδας τηλεπαρουσιάστριας" border="0" class="lazyload">
</div>
<div><span class="category">TABLOID</span></div>
<h3>Αποκαλύφθηκε onair το στήθος Ισπανίδας τηλεπαρουσιάστριας</h3>
<h4>Για όλα έφταιγε το... μικρόφωνο</h4>
</a>
<a href="/viral/69151/kai-neo-sklhro-vinteo-apo-to-zwiko-vasileio-meta-to-igkoyana-kai-ta-fidia-video" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-play"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/mikro_katsiki_1.jpg" alt="Και νέο σκληρό βίντεο από το ζωικό βασίλειο, μετά το ιγκουάνα και τα φίδια" border="0" class="lazyload">
</div>
<div><span class="category">ΖΩΑ</span></div>
<h3>Και νέο σκληρό βίντεο από το ζωικό βασίλειο, μετά το ιγκουάνα και τα φίδια</h3>
<h4>Αίγαγρος κάνει σάλτο 10 μέτρων για να γλιτώσει από αλεπού</h4>
</a>
<a href="/viral/69048/to-facebook-molis-skotwse-ena-swro-xrhstes-toy-mazi-kai-ton-zoykermpergk" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/markzuckerberg3.jpg" alt="Το Facebook μόλις... «σκότωσε» ένα σωρό χρήστες του, μαζί και τον Ζούκερμπεργκ!" border="0" class="lazyload">
</div>
<div><span class="category">ΤΕΧΝΟΛΟΓΙΑ</span></div>
<h3>Το Facebook μόλις... «σκότωσε» ένα σωρό χρήστες του, μαζί και τον Ζούκερμπεργκ!</h3>
<h4>Ούτε καν ο CEO του δημοφιλούς ιστότοπου δεν γλίτωσε</h4>
</a>
<a href="/viral/69133/ti-tha-faei-o-ompama-sto-proedriko-megaro" target="_parent">
<div class="cropimg absolute-center">
<img data-src="http://cdn.e-daily.gr/repository/2016/PROEDRIKO_MEGARO-735x459.jpg" alt="Τι θα φάει ο Ομπάμα στο Προεδρικό Μέγαρο" border="0" class="lazyload">
</div>
<div><span class="category">ΕΛΛΑΔΑ</span></div>
<h3>Τι θα φάει ο Ομπάμα στο Προεδρικό Μέγαρο</h3>
<h4>Το δείπνο για τον Αμερικανό πρόεδρο</h4>
</a>
<a href="/viral/66313/ellhnikes-tampeles-poy-prokaloyn-afthono-gelio-pics" target="_parent">
<div class="cropimg absolute-center">
<div class="media"><span class="icon-picture"></span></div>
<img data-src="http://cdn.e-daily.gr/repository/2016/tampeles_ellinikes_main.jpg" alt="Ελληνικές ταμπέλες που προκαλούν άφθονο γέλιο" border="0" class="lazyload">
</div>
<div><span class="category">ΧΙΟΥΜΟΡ</span></div>
<h3>Ελληνικές ταμπέλες που προκαλούν άφθονο γέλιο</h3>
<h4>Όταν προχειρότητα και φαντασία ενώνονται δημιουργούν ένα «ανίκητο» μείγμα</h4>
</a>
</div>
</div>
</div>
</div>
<div id="footerBgWide">
<center>
<div id="footerMain">
<div class="footer hideOnSmallScreen" style="width:12%;">
<span class="footer_optionsHeader"><a href="/news">Ειδήσεις</a></span><br>
<span class="footer_options">
<a href="/news">Τελευταία νέα</a>
<a href="/ellada">Ελλάδα</a>
<a href="/kosmos">Κόσμος</a>
<a href="/politiki">Πολιτική</a>
<a href="/oikonomia">Οικονομία</a>
<a href="/athlitismos">Αθλητισμός</a>
<a href="/epistimi">Επιστήμη</a>
<a href="/politismos">Πολιτισμός</a>
</span>
</div>
<div class="footer hideOnSmallScreen" style="width:12%;">
<div class="footer_optionsHeader"><a href="/themata">Θέματα</a></div><br>
<div class="footer_optionsHeader"><a href="/quiz">Κουίζ</a></div><br>
<div class="footer_optionsHeader"><a href="/viral">Viral</a></div><br>
<div class="footer_optionsHeader"><a href="/lol">LOL</a></div>
<div class="footer_optionsHeader"><a href="/omg">OMG</a></div>
<div class="footer_optionsHeader"><a href="/juicy">JUICY</a></div>
<div class="footer_optionsHeader"><a href="/alist">A-LIST</a></div>
<div class="footer_optionsHeader"><a href="/geeky">GEEKY</a></div>
<div class="footer_optionsHeader"><a href="/cute">CUTE</a></div>
<div class="footer_optionsHeader"><a href="/retro">RETRO</a></div>
</div>
<div class="footer hideOnSmallScreen" style="width:12%;">
<span class="footer_optionsHeader"><a href="/life">Life</a></span><br>
<span class="footer_options">
<a href="/food">Food</a>
<a href="/bodymind">Body+Mind</a>
<a href="/travel">Ταξίδια</a>
<a href="/style">Style</a>
<a href="/spiti">Σπίτι</a>
<a href="/family">Family</a>
<a href="/sxeseis">Σχέσεις</a>
</span><br>
</div>
<div class="footer hideOnSmallScreen" style="width:18%;">
<div class="footer_optionsHeader"><a href="/entertainment">Ψυχαγωγία</a></div><br>
<span class="footer_options">
<a href="/agenda">Agenda</a>
<a href="/wintickets">Κερδίστε Προσκλήσεις</a><br>
<a href="/music">Μουσική</a>
<a href="/theater">Θέατρο+Χορός</a>
<a href="/arts">Εικαστικά</a>
<a href="/cinema">Σινεμά</a>
<a href="/nightlife">Νάιτ Λάιφ</a>
<a href="/city">Πόλη</a>
</span>
</div>
<div class="footer fullOnSmallScreen" style="width:32%;">
<div class="footer_optionsHeader">Newsletter</div><br>
<style type="text/css">#mc-embedded-subscribe-form{margin:0;padding:0}#mc_embed_signup{display:table;clear:both;text-align:left;line-height:normal;}#mc_embed_signup .mc-field-group{display:table;margin:10px 0 10px 0;padding:0;width:100%}#mc_embed_signup li{display:block;float:left;margin-top:12px;width:33.333%;padding:0;font-weight:400;font-size:1.25em;}#mc_embed_signup .checkbox{width:22px;height:22px;border:1px solid #e3e3e3;background:#fff;margin:0;padding:0}#mc_embed_signup .checkbox:before{content:"";display:block;width:23px;height:23px;background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 0 0 no-repeat;}#mc_embed_signup .checkbox:checked:before{background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') -32px 0 no-repeat;}#mc_embed_signup label{margin-left:5px;padding-top:-5px;}#mc_embed_signup .email{font-weight:normal;height:45px;width:55%;float:left;display:inline-block;background:transparent url('http://img2-1.timeinc.net/people/static/i/footer/footer-newsletter-sprite.png') 10px -22px no-repeat;color:#000;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;padding:0 0 0 46px;border:1px solid #d9d9d9}#mc_embed_signup .email.focus{color:#000;}#mc_embed_signup .email.placeholder{color:#e3e3e3;}#mc_embed_signup .submit-button{background:#0079ff;text-align:center;float:left;width:30%;height:47px;border:none;font-size:14px;text-transform:uppercase;color:#FFF;letter-spacing:1px;cursor:pointer;display:inline-block;padding-top:6px;margin-left:3px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;}#mc_embed_signup .submit-button:hover{background:#2e78b3;text-decoration:none;}#mc_embed_signup div.mce_inline_error{background-color:#fff;color:#804040;}.clearfix{padding:0;margin:0}</style>
<div id="mc_embed_signup" class="clearfix">
<form action="//nextweb.us10.list-manage.com/subscribe/post?u=037b671af872ebf3eb1ceac4a&amp;id=5ab11026ce" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div class="clearfix">Εγραφείτε στα newsletters του E-Daily.gr για να μαθαίνετε τα καλύτερα στο email σας:</div>
<ul class="mc-field-group input-group">
<li>
<input type="checkbox" class="checkbox" value="1" name="group[1109][1]" id="mce-group[1109]-1109-0" checked="checked">
<label for="mce-group[1109]-1109-0">Ειδήσεις</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="2" name="group[1109][2]" id="mce-group[1109]-1109-1" checked="checked">
<label for="mce-group[1109]-1109-1">Viral</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="4" name="group[1109][4]" id="mce-group[1109]-1109-2" checked="checked">
<label for="mce-group[1109]-1109-2">Life</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="8" name="group[1109][8]" id="mce-group[1109]-1109-3" checked="checked">
<label for="mce-group[1109]-1109-3">E-Agenda</label>
</li>
<li>
<input type="checkbox" class="checkbox" value="16" name="group[1109][16]" id="mce-group[1109]-1109-4" checked="checked">
<label for="mce-group[1109]-1109-4">E-Weekly</label>
</li>
<li></li>
</ul>
<div class="mc-field-group" style="margin-top:25px;">
<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Συμπληρώστε το email σας">
<input type="submit" class="submit-button" value="ΕΓΓΡΑΦΗ" name="subscribe" id="mc-embedded-subscribe">
</div>
</form>
<div id="mce-responses" class="clear">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>
<div style="position: absolute; left: -5000px;"><input type="text" name="b_037b671af872ebf3eb1ceac4a_5ab11026ce" tabindex="-1" value=""></div>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email'; /*
 * Translated default messages for the $ validation plugin.
 * Locale: EL
 */
$.extend($.validator.messages, {
	required: "<span class=asterisk></span> Αυτό το πεδίο είναι υποχρεωτικό.",
	remote: "<span class=asterisk></span> Παρακαλώ διορθώστε αυτό το πεδίο.",
	email: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη διεύθυνση email.",
	url: "<span class=asterisk></span> Παρακαλώ εισάγετε ένα έγκυρο URL.",
	date: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία.",
	dateISO: "<span class=asterisk></span> Παρακαλώ εισάγετε μια έγκυρη ημερομηνία (ISO).",
	number: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό.",
	digits: "<span class=asterisk></span> Παρακαλώ εισάγετε μόνο αριθμητικά ψηφία.",
	creditcard: "<span class=asterisk></span> Παρακαλώ εισάγετε έναν έγκυρο αριθμό πιστωτικής κάρτας.",
	equalTo: "<span class=asterisk></span> Παρακαλώ εισάγετε την ίδια τιμή ξανά.",
	accept: "<span class=asterisk></span> Παρακαλώ εισάγετε μια τιμή με έγκυρη επέκταση αρχείου.",
	maxlength: $.validator.format("<span class=asterisk></span> Παρακαλώ εισάγετε μέχρι και {0} χαρακτήρες."),
	minlength: $.validator.format("Παρακαλώ εισάγετε τουλάχιστον {0} χαρακτήρες."),
	rangelength: $.validator.format("Παρακαλώ εισάγετε μια τιμή με μήκος μεταξύ {0} και {1} χαρακτήρων."),
	range: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεταξύ {0} και {1}."),
	max: $.validator.format("Παρακαλώ εισάγετε μια τιμή μικρότερη ή ίση του {0}."),
	min: $.validator.format("Παρακαλώ εισάγετε μια τιμή μεγαλύτερη ή ίση του {0}.")
});}(jQuery));var $mcj = jQuery.noConflict(true);</script>
 
</div>
<div class="footer footerRight footer_options fullOnSmallScreen" style="width:14%; text-align: right">
<div class="footer_optionsHeader">Follow us</div><br>
<a href="http://www.facebook.com/edailygr" target="_blank" class="facebook">
<span>Facebook</span>
<i class="icon-facebook-circled" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.twitter.com/edailygr" target="_blank" class="twitter">
<span>Twitter</span>
<i class="icon-twitter-circled" alt="Twitter Official Account" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.google.com/+edailygr" target="_blank" class="gplus">
<span>Google+</span>
<i class="icon-gplus-circled-1" alt="Facebook Official Page" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.instagram.com/edailygr" target="_blank" class="instagram">
<span>Instagram</span>
<i class="icon-instagramm" alt="Instagram" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://www.youtube.com/channel/UCGWrti2JHp5rPBJWM2PCfTw" target="_blank" class="youtube">
<span>YouTube</span>
<i class="icon-youtube-play" alt="YouTube Channel" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
<a href="http://feeds.feedburner.com/edailygr" target="_blank" class="rss">
<span>RSS</span>
<i class="icon-rss" alt="RSS" style="vertical-align: middle; margin-bottom: .5em; padding-left:5px; font-size:2.75em"></i>
</a>
</div>
</div>
</center>
</div>
<center>
<div id="footerMain">
<div class="footer footer21">
<a href="javascript:openContact();">Επικοινωνία</a>&nbsp;&nbsp;&nbsp;
<a href="javascript:openContact();">Διαφήμιση</a>&nbsp;&nbsp;&nbsp;
<a href="/rules.asp">Όροι Διαγωνισμών</a>&nbsp;&nbsp;&nbsp;
<a href="/privacy.asp">Privacy</a>&nbsp;&nbsp;&nbsp;
<a href="/terms.asp">Terms of use</a>
<br>
<a href="http://www.e-radio.gr" target="_blank">E-Radio Hellas </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.e-radio.com.cy" target="_blank">E-Radio Cyprus </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.e-radio.co.uk" target="_blank">E-Radio UK </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.onestreaming.com" target="_blank">One Streaming </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.arionradio.com" target="_blank">Arion Radio </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.athensparty.com" target="_blank">Athens Party </a> &nbsp;&nbsp;&nbsp;
<a href="http://www.akous.gr" target="_blank">Akous </a> &nbsp;&nbsp;&nbsp;
<br>
© 2015 All Right Reserved. "E-DAILY" is a registered trademark
</div>
<div class="footer footer22">
<a href="http://www.nextweb.gr" target="_blank"><img src="/_img/nextweb.png" border="0" alt="Nextweb"></a>
</div>
</div>
</center>
</div>
</div>
<script type='text/javascript'>
$().ready(function() {
	
	if( $('#topHeaderMobile').length ){

		$('#mobilemenu_pad').infinitypush({
			openingspeed: 300,
			closingspeed: 300
		});
		
		/*
		$('#mobilemenu_icon').click(function(){
			
			$(this).toggleClass('open');
			//slideout.toggle();
			
		});
	
		slideout.on('open', function() {
			if ($('#mobilemenu_icon').hasClass('open')==false){$('#mobilemenu_icon').addClass('open')}
		});
		slideout.on('close', function() {
			if ($('#mobilemenu_icon').hasClass('open')==true){$('#mobilemenu_icon').removeClass('open')}
	
		});
		*/
	}

});
</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-551bf8914936f63f" async="async"></script>
<script type="text/javascript">

	<!-- Google -->
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-504700-32', 'auto');
  ga('send', 'pageview');

	<!-- Tynt -->
	if(document.location.protocol=='http:'){
	 var Tynt=Tynt||[];Tynt.push('aFq4livo0r4yzYacwqm_6r');Tynt.i={"ap":"Περισσότερα:","aw":"15","st":true,"su":false};
	 (function(){var s=document.createElement('script');s.async="async";s.type="text/javascript";s.src='http://tcr.tynt.com/ti.js';var h=document.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);})();
	}
	
	<!-- Quantcast Tag -->
	var _qevents = _qevents || [];
	
	(function() {
	var elem = document.createElement('script');
	elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
	elem.async = true;
	elem.type = "text/javascript";
	var scpt = document.getElementsByTagName('script')[0];
	scpt.parentNode.insertBefore(elem, scpt);
	})();
	
	_qevents.push({
	qacct:"p-49SuwTaH2w2BM"
	});

</script>
<noscript>
<div style="display:none;">
<img src="//pixel.quantserve.com/pixel/p-49SuwTaH2w2BM.gif" border="0" height="1" width="1" alt="Quantcast"/>
</div>
</noscript>
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6032644253807', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6032644253807&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1"/></noscript>
 
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3125699,4,0,0,0,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('http://s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="http://www.histats.com" target="_blank"><img src="http://sstatic1.histats.com/0.gif?3125699&101" alt="web hit counter" border="0"></a></noscript>
<script src="//fast.wistia.net/labs/fresh-url/v1.js" async></script>
<div id="5796318"></div>
<script>
ADTECH.config.placements[5796318] = {params: { alias: 'edhomeskin_display', key:'',target: '_blank' }};
ADTECH.enqueueAd(5796318);
ADTECH.executeQueue();
</script>
<noscript>Please enable scripting languages in your browser.</noscript>
</body>
</html>
