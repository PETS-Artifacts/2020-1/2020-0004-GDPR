<!DOCTYPE html>
<html lang="en" class="no-js homepage">
<head>
  <meta charset="utf-8">
  <title data-i18n="">MeWe - The Next-Gen Social Network</title>
  <meta data-i18n="[content]MeWe is the first private collaboration platform that groups your real life communities in one convenient place. Think facebook + dropbox with privacy." content="MeWe is the first private collaboration platform that groups your real life communities in one convenient place. Think facebook + dropbox with privacy." name="description">
  <meta content="MeWe.com" name="author">
  <meta content="width=device-width,initial-scale=1,maximum-scale=1.0" name="viewport">

  <meta data-i18n="[content]MeWe - The Next-Gen Social Network" content="MeWe - The Next-Gen Social Network" property="og:title">
  <meta content="website" property="og:type">
  <meta content="https://d1p499o1pzyid4.cloudfront.net/b/img/logos/mewe-rev-w-250-fc2766d4.jpg" property="og:image">
  <meta data-i18n="[content]Join the Privacy Revolution and share with peace of mind, knowing that we do not track, scrape or sell any of your personal information." content="Join the Privacy Revolution and share with peace of mind, knowing that we do not track, scrape or sell any of your personal information." property="og:description">

  <script src="https://d1p499o1pzyid4.cloudfront.net/b/desktop/vendor/require-jquery-cca7f790.js?v=2"></script>
  <script>var SG = SG || {};SG.readyList = jQuery.Deferred(); SG.isReady = false; SG.ready = function(fn){ SG.readyList.done(fn); return SG.readyList.promise(); }</script>
  <script src="https://d1p499o1pzyid4.cloudfront.net/b/js/modules/i18n/locales-homepage-8cdc0eb8.js"></script>

  <link href="https://d1p499o1pzyid4.cloudfront.net/b/css/styles/mainPage-3448ff81.css" rel="stylesheet">

	
<script>
      SG.controller = 'mainPage';
    </script>

	
</head>
<body>
<div id="login-overlay" class="login-overlay sg-hidden">
  <div class="main-container">

    <p class="logo-wrapper">
      <a data-i18n="[title]Next-Gen Social Network" title="Next-Gen Social Network" class="logo-main" href="/">
        <img height="49" width="151" alt="MeWe" src="https://d1p499o1pzyid4.cloudfront.net/b/img/logos/mewe-logo-151x49-78ded4c6.png">
      </a>

      <a data-i18n="[title]MeWe - Next-Gen Social Network" title="MeWe - Next-Gen Social Network" class="logo-text" href="/">
        <span data-i18n="">Next-Gen Social Network</span>
      </a>
    </p>

    <div id="login-blocked" class="login-blocked error">
      <p data-i18n="" class="text-error">Please try again later.</p>
      <p data-i18n="" class="login-blocked-info">To control abuse, we limit the number of attempted logins per hour</p>
      <a href="http://mewe.kayako.com/index.php?/Knowledgebase/List"><button class="btn-new-1 help-desk">MeWe Help desk</button></a>
    </div>

    <form id="reset-or-try-pwd-form" class="reset-or-try-pwd-form error">
      <p data-i18n="" class="text-error">Login/Password Incorrect.</p>
      <p data-i18n="" class="reset-again-info">You can reset your password or try again</p>
      <button data-i18n="[title]Reset my password;Reset my password" title="Reset my password" class="btn-new-1 reset">Reset my password</button>
      <button data-i18n="[title]Try again;Try again" title="Try again" class="btn-new-1 try-again">Try again</button>
    </form>

    <form id="forgot-pwd-form" class="forgot-pwd-form">
      <p data-i18n="" class="forgot-pwd-info">Enter your email address and we'll email you a link to reset your password</p>

      <div class="not-found hidden">
        <p data-i18n="" class="text-error">We couldn't find your account with that information.</p>
        <p data-i18n="" class="pw-reset-info">Please try your email again or create an account.</p>
      </div>

      <div class="try-later hidden">
        <p data-i18n="" class="text-error">Please try again later.</p>
        <p data-i18n="" class="pw-reset-info">You've exceeded the number of attempts.</p>
        <a href="http://mewe.kayako.com/index.php?/Knowledgebase/List"><button onclick="return false;" class="btn-new-1 help-desk">MeWe Help desk</button></a>
      </div>

      <div class="generic-error hidden">
        <p data-i18n="" class="text-error">We're sorry, something seems to have gone wrong.</p>
        <p data-i18n="" class="pw-reset-info">Please try again soon.</p>
      </div>

      <input id="forgot-email" data-i18n="[title]Email Address;[placeholder]Email" class="sg-input icon-email" required="" type="email" title="Email Address" placeholder="Email" name="forgot-pwd-email">

      <button id="submit-forgot-pwd" data-i18n="[title]Send Email;SEND IT" title="Send Email" class="forgot-btn btn-new-1" type="submit">SEND IT</button>
    </form>

    <form onsubmit="SG.loggedInBeforeInit = true; return false;" class="login-form login-form-top" method="post" action="">

      <p data-i18n="" class="text-error">Login/Password Incorrect. Please try again.</p>

      <input id="email" data-i18n="[placeholder]Email or Phone;[title]Email Address or Phone" class="sg-input icon-email" required="" type="text" title="Email Address or Phone" placeholder="Email or Phone" name="username">
      <input id="password" data-i18n="[placeholder]Password;[title]Password" class="sg-input icon-password" required="" type="password" title="Password" placeholder="Password" name="password">

      <a data-i18n="[title]Forgot Password?;Forgot Password?" onclick="$('.login-overlay').addClass('forgot');$('.login-form-top').hide();$('.forgot-pwd-form').show(); return false;" title="Forgot Password?" href="#" class="forgot-link">
        Forgot Password?
      </a>

      <label class="remember-lbl">
        <input id="remember" name="remember" type="checkbox">
        <span data-i18n="">Remember Me</span>
      </label>

      <button data-i18n="" type="submit" class="submit btn-new-1">LOG IN</button>

    </form>
  </div>
</div>

<div class="main-container subpage general privacy" id="content_index">

  

  <header>

  <div class="inner">

    <img alt="" class="img-highlight" src="https://d1p499o1pzyid4.cloudfront.net/b/img/home/logo-highlight-1e4fd56f.png">

    <p class="logo-wrapper">
      <a data-i18n="[title]Next-Gen Social Network" title="Next-Gen Social Network" class="logo-main" href="/">
        <img height="49" width="151" alt="MeWe" src="https://d1p499o1pzyid4.cloudfront.net/b/img/logos/mewe-logo-151x49-78ded4c6.png">
      </a>

      <a data-i18n="[title]MeWe - Next-Gen Social Network" title="MeWe - Next-Gen Social Network" class="logo-text logo-text-main" href="/">
        <span data-i18n="">Next-Gen Social Network</span>
      </a>

      <button onclick="SG.Dialog.create('video');" title="#Not4Sale" class="btn-reset hashtag">
        <span>#Not4Sale</span>
      </button>
    </p>

    <ul class="locales">
      <li><a data-locale="en" href="/?locale=en">English</a></li>
      <li><a data-locale="ja" href="/?locale=ja">日本語</a></li>
      <li><a data-locale="de" href="/?locale=de">Deutsch</a></li>
      <li><a data-locale="fr" href="/?locale=fr">Français</a></li>
      <li><a data-locale="es" href="/?locale=es">Español</a></li>
      <li><a data-locale="pt-BR" href="/?locale=pt-BR">Português (Brasil)</a></li>
    </ul>

    <div title="Log In Automatically" class="auto-login-wrapper hidden">
      <script>
        if (window.location.hostname === "localhost" || window.location.hostname === "mewe") {
          require(['js/pages/page.test-users'], function(page) {
            new page();
          });

          $(".auto-login-wrapper").removeClass("hidden");
        }
      </script>

      <label id="auto-login-error" class="text-error"></label>
      <select id="auto-login">
        <option value="">Select user to login</option>
      </select>
    </div>

    <ul class="list vertical">
      <li>
        <a data-i18n="About" title="About MeWe" href="/about">
          About
        </a>
      </li>
      <li>
        <a data-i18n="Values" title="Values of MeWe" href="/values">
          Values
        </a>
      </li>
      <li>
        <a data-i18n="Blog" target="_blank" title="Blog" href="https://blog.mewe.com">
          Blog
        </a>
      </li>
      <li>
        <button id="login-fake-btn" data-i18n="Member Log In" onclick="$(&quot;#login-overlay&quot;).removeClass(&quot;forgot sg-hidden&quot;).animate({ top: &quot;0&quot; }, &quot;fast&quot; );if (SG.Login) { SG.Login.hideOverlayBinding(); }" class="login-btn btn-reset" type="button">
          Member Log In
        </button>
      </li>
    </ul>

  </div>
</header>

  <div class="inner">
    <h1 data-i18n="[html]block:::privacy::Privacy Policy" class="header">
      <b>Privacy</b> Policy
    </h1>

    <section class="half first">

      <br>

  		<p>
  		  <strong data-i18n="block:::privacy::Your personal privacy">
          Your personal privacy and the privacy of your data is paramount to us at MeWe.
          We have prepared this Privacy Policy to clearly explain how we protect you and your data at MeWe.
  		  </strong>
  		</p>

    	<h3 data-i18n="block:::privacy::Content Privacy Policy" class="section">
        Content Privacy Policy: You Own Your Content
      </h3>

      <p data-i18n="block:::privacy::You own your content">
        You own your content. We don’t. All your content belongs to you.
        We don’t share your content and we don’t spy on you or your content. Illegal activity is not permitted.
        If we are made aware of illegal activity, we will take appropriate action.
        We ask that you not post any unlawful, harmful, obscene, or pornographic content.
        Please see our Terms of Service for the list of activities that are not permitted at MeWe.
      </p>

      <h3 data-i18n="block:::privacy::Tracking Privacy Policy" class="section">
        Tracking Privacy Policy: We Respect Your Privacy
      </h3>

      <p data-i18n="block:::privacy::We don't track">
        We don’t track you personally and we never sell your personal data.
        Here is what we track: we track how users use our site in general so that we can make it better.
        We are monitoring traffic, usage activity, site performance, and we use general analytic tools so that we can improve your experience.
        We do not associate any of this data with you personally.
        We never sell or share your personally identifiable information unless required to do so by law.
      </p>

      <h3 data-i18n="block:::privacy::Advertising Policy" class="section">
        Advertising Policy: You Are In Control
      </h3>

      <p data-i18n="block:::privacy::We don't provide">
        We don't provide or share with third-party advertisers, search providers, and ad networks (&quot;Advertisers&quot;) any personal information about you.
        You choose your advertising preferences by selecting the product/service categories for ads and coupons you would like to see.
        This is the one of two ways that we know, directly from you, (without ever spying on, profiling, or tracking you), what ads and coupons are most relevant to you.
        Also, when you create a private group, you have the option of choosing the type of group you are creating.
        We use that general information as a guideline for providing ads and coupons to you and your group.
        You also have a “No-Ads” subscription option. We keep your personally self-selected advertising preferences stored so that you can receive the advertisements and ad types you have selected.
        You are in control of this information.
        You can change or delete this information at any time.
        If you click on an ad then we cannot control your privacy once you leave the MeWe site.
      </p>

      <h3 data-i18n="block:::privacy::Message Disappearance" class="section">
        Message Disappearance
      </h3>

      <p data-i18n="block:::privacy::You have the ability">
        You have the ability to send certain types of messages through our services that automatically disappear.
        If you send such disappearing messages through our services they will be automatically deleted once they have been viewed or have expired.
      </p>

      <p data-i18n="block:::privacy::But &#x2014; and this is important">
        But — and this is important — you should understand that users who see your messages can always save them, either by taking a screenshot or by using some other image-capture technology (whether that be software or even something as old-fashioned as a camera to take a photo of your device’s screen).
        The same common sense that applies to the Internet at large applies to MeWe as well: Don’t send messages that you wouldn’t want someone to save or share.
      </p>

      <p data-i18n="block:::privacy::In most cases">
        In most cases, once we detect that all recipients have viewed a message, we automatically delete it from our servers. And again in most cases, the services are programmed to delete the message from the recipients’ device as well. There are, however, exceptions to this general rule:
        </p><ul>
          <li data-i18n="block:::privacy::We can&#x2019;t guarantee that messages">We can’t guarantee that messages will be deleted within a specific timeframe. And even after we’ve deleted message data from our servers, that same data may remain in backup for a limited period of time.</li>
          <li data-i18n="block:::privacy::We may receive requests from law enforcement">We may receive requests from law enforcement requiring us by law to suspend our ordinary server-deletion practices for specific information.</li>
          <li data-i18n="block:::privacy::As with any digital information">As with any digital information, there may be ways to access messages while still in temporary storage on recipients’ devices or, forensically, even after they are deleted.</li>
        </ul>
      <p></p>

      <div class="reg-form only-btn">
        <a data-i18n="[title]Sign Up For Free!" title="Sign Up For Free!" href="/" class="orange-btn new-btn">SIGN UP FOR FREE!</a>
      </div>

  	</section>

  	<section class="half last">

      <h3 data-i18n="block:::privacy::California Disclosures" class="section">
        California Disclosures
      </h3>

      <p data-i18n="block:::privacy::Do Not Track Policy" class="margin-bottom">
        Do Not Track Policy
      </p>
      <p data-i18n="block:::privacy::We do not allow tracking cookies" class="margin-top">
        We do not allow tracking cookies and we do not allow third parties to collect any personal information on our website.
      </p>
      <p data-i18n="block:::privacy::Third Party Cookies" class="margin-bottom">
        Third Party Cookies
      </p>
      <p data-i18n="block:::privacy::We do not allow third parties to use cookies" class="margin-top">
        We do not allow third parties to use cookies to collect information about the use of our website.
      </p>
      <p data-i18n="block:::privacy::California Privacy Rights" class="margin-bottom">
        California Privacy Rights
      </p>
      <p data-i18n="block:::privacy::We do not share any personal information" class="margin-top">
        We do not share any personal information about our customers with any third parties unless required to do so by law.
        If our practices change, we will address the issue in accordance with California Civil Code Section 1798.83
      </p>

  	  <h3 data-i18n="block:::privacy::Your Identity: We Protect" class="section">
        Your Identity: We Protect It To The Extent The Law Allows
      </h3>

      <p data-i18n="block:::privacy::We do our best">
        We do our best to protect your identity at all times and we are respectful of the law.
        MeWe will disclose personally-identifying information when required to do so by law.
      </p>

  	  <h3 data-i18n="block:::privacy::Linking to Third" class="section">
        Linking to Third Party Sites: These Are Your Decisions And Responsibilities
      </h3>

      <p data-i18n="block:::privacy::As an optional">
        As an optional convenience for you, MeWe offers you the opportunity to link to and/or connect to third party websites,
        advertisers, vendor products, services, etc., including and not limited to social network sites like Twitter.
        When you click-through, connect or link to these third party sites, products, services, etc., you are no longer protected by MeWe's Privacy Policy.
        If you share to any third party social site from MeWe, they will not be able to see back into your MeWe account.
        We encourage you to read the privacy policies of these third parties, as they differ from MeWe's privacy practices as set forth in this Privacy Policy.
        MeWe is not liable for wrongful use, loss, or disclosure of your personally-identifying information related to your use of any third party.
      </p>

      <h3 data-i18n="block:::privacy::Security: HTTPS and Encryption" class="section">
        Security: HTTPS and Encryption
      </h3>

      <p data-i18n="block:::privacy::We maintain physical">
        We maintain physical, electronic, and procedural safeguards to protect the confidentiality and security of your personal user data and other information transmitted to us.
        For example, we encrypt your personally identifiable information, and most, if not all requests from you to the server are created over a secure connection (HTTPS).
        However, no data transmission over the Internet or other network can be guaranteed to be 100% secure.
      </p>

      <h3 data-i18n="block:::privacy::Children's Privacy" class="section">
        Children's Privacy
      </h3>

      <p data-i18n="block:::privacy::We do not knowingly">
        We do not knowingly collect personal information from children under the age of 13.
        If we become aware that we have inadvertently received personal information from a child under the age of 13,
        we will delete such information from our records.
      </p>

      <h3 data-i18n="block:::privacy::Notifications: We&#x2019;d Like" class="section">
        Notifications: We’d Like To Keep You Up To Date
      </h3>

      <p data-i18n="block:::privacy::If you are a registered">
        If you are a registered user of the MeWe Website and have supplied your email address,
        MeWe will want to tell you about new features, solicit your feedback, and keep you up to date about the privacy and integrity issues
        we care most about and how our company is working to support them.
        And you have the option to opt-out of these notifications.
      </p>

      <h3 data-i18n="block:::privacy::Privacy Policy Changes" class="section">
        Privacy Policy Changes: Immediate Notification &amp; Easy Opt Out
      </h3>

      <p data-i18n="block:::privacy::If MeWe changes">
        If MeWe changes any aspect of its Privacy Policy, we will email you immediately with a link to the changes so you can review and understand them,
        and provide you with an easy way to opt out of our service and delete your account if you wish.
      </p>

  	</section>
	</div>

  <footer>

  <div class="nav-wrapper clearfix">
    <ul class="list-row nav">
      <li><a data-i18n="[title]FAQ;FAQ" title="FAQ" href="/faq">FAQ</a></li>
      <li><a data-i18n="[title]Press;Press" title="Press" href="/press">Press</a></li>
      <li><a data-i18n="[title]Jobs;Jobs" title="Jobs" href="/jobs">Jobs</a></li>
      <li><a data-i18n="[title]Privacy;Privacy" title="Privacy" href="/privacy">Privacy</a></li>
      <li><a data-i18n="[title]Terms;Terms" title="Terms" href="/terms">Terms</a></li>
    </ul>
    
    <ul class="list-row nav locales">
      <li><a data-locale="en" href="/?locale=en">English</a></li>
      <li><a data-locale="ja" href="/?locale=ja">日本語</a></li>
      <li><a data-locale="de" href="/?locale=de">Deutsch</a></li>
      <li><a data-locale="fr" href="/?locale=fr">Français</a></li>
      <li><a data-locale="es" href="/?locale=es">Español</a></li>
      <li><a data-locale="pt-BR" href="/?locale=pt-BR">Português (Brasil)</a></li>
    </ul>

    <ul class="nav-socials">
      <a data-i18n="[title]Follow Us" title="Follow Us" target="_blank" href="https://twitter.com/MeWe/">
        <span class="sg-icon-mewe sg-icon-mewe-tw"></span>
      </a>
      <a data-i18n="[title]Subscribe Us" title="Subscribe Us" target="_blank" href="https://www.youtube.com/user/Sgrouples">
        <span class="sg-icon-mewe sg-icon-mewe-yt"></span>
      </a>
    </ul>

    <span class="nav-trademark">
      © 2016 Sgrouples Inc.
    </span>
  </div>

</footer>
</div>

<script src="https://d1p499o1pzyid4.cloudfront.net/b/js/RequireMake-build-8335a103.js"></script>


	
<script type="text/javascript">
// <![CDATA[

var lift_page = "F651839453190VOSSIB";
// ]]>
</script><script type="text/javascript">
//<![CDATA[
(function() {
var _analytics_scr = document.createElement('script');
_analytics_scr.type = 'text/javascript'; _analytics_scr.async = true; _analytics_scr.src = '/_Incapsula_Resource?SWJIYLWA=2977d8d74f63d7f8fedbea018b7a1d05&ns=1';
var _analytics_elem = document.getElementsByTagName('script')[0]; _analytics_elem.parentNode.insertBefore(_analytics_scr, _analytics_elem);
})();
// ]]>
</script></body>
</html>  
  