<!DOCTYPE html>
<!--[if IE 7]><html class="ie7 ie no-js"> <![endif]-->
<!--[if IE 8]><html class="ie8 ie no-js"> <![endif]-->
<!--[if IE 9]><html class="ie9 ie no-js"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <title>Privacy Policy | ProtonMail</title>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=Edge"/><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="ProtonMail Privacy Policy">
    <link rel="shortcut icon" href="/images/favicon.ico">
    <link rel="canonical" href="https://protonmail.com/privacy-policy">
    <link rel="alternate" href="https://protonmail.com/privacy-policy" hreflang="en">
    <link rel="alternate" href="https://protonmail.com/fr/privacy-policy" hreflang="fr">
    <link rel="stylesheet" href="/css/static.css">
    <!--[if lt IE 10]>
        <link rel="stylesheet" href="css/ie.css">
     <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/respond.min.js"></script>
    <![endif]-->

</head>
<body>

<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" id="header">
    <div class="top hidden-sm hidden-xs">
        <div class="container text-right form-inline">
            <a target="_blank" href="https://plus.google.com/+protonmail/posts" rel="publisher" onclick="javascript:_paq.push(['trackEvent', 'SOCIAL_GOOGLE', 'CLICK', 'NAV']);">
                <i class="fa fa-google"></i> +Google
            </a>
            <a target="_blank" href="https://facebook.com/ProtonMail" title="Facebook" onclick="javascript:_paq.push(['trackEvent', 'SOCIAL_FACEBOOK', 'CLICK', 'NAV']);">
                <i class="fa fa-facebook"></i> Facebook
            </a>
            <a target="_blank" href="https://twitter.com/ProtonMail" title="Twitter" onclick="javascript:_paq.push(['trackEvent', 'SOCIAL_TWITTER', 'CLICK', 'NAV']);">
                <i class="fa fa-twitter"></i> Twitter
            </a>
            <span class="localization">
                <span>Choose your language</span>:
                <select class="form-control" id="langs" onchange="translateSite()">
                    <option value="en">English</option>
                    <option value="fr">French</option>
                    <option value="it">Italian</option>
                </select>
            </span>
        </div>
    </div>
    <div class="bottom">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="navbar-brand" href="/" onclick="javascript:_paq.push(['trackEvent', 'PAGE_HOME', 'CLICK', 'LOGO']);">
                    <img src="https://protonmail.com/images/logo.png" alt="" height="23" class="pull-left">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="first"><a href="https://protonmail.com/about" title="About">About</a></li>
                    <li><a href="https://protonmail.com/security-details" title="Security">Security</a></li>
                    <li><a href="https://protonmail.com/blog/" title="Blog">Blog</a></li>
                    <li><a href="https://protonmail.com/careers" title="Careers">Careers</a></li>
                    <li><a href="https://protonmail.com/support/" title="Support">Support</a></li>
                    <li><a href="https://protonmail.com/donate" title="Donate">Donate</a></li>
                    <li><a href="https://mail.protonmail.com/login" title="Log In" class="btn btn-ghost btn-short">LOG IN</a></li>
                    <li><a href="https://protonmail.com/signup" title="Sign Up" class="btn btn-default btn-short">SIGN UP</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </div>
</nav>
<div id="header-spacer"></div>


<div class="container content">

    <h1>Privacy Policy</h1>

    <p>In the following policy, ProtonMail refers to the service offered by Proton Technologies AG (the "Company" or "We") through the protonmail.com website (the "Service"). This Privacy Policy explains (i) what information we collect through your access and use of our Service (ii) the use we make of such informatoin; and (iii) the security level we provide for protecting such information.</p>
    <p>By visiting protonmail.com and using the Services provided here, you consent to the terms outlined in this privacy policy.</p>
    <h3>Legal Framework</h3>
    <p>The Company is domiciled solely in Switzerland and all hosting infrastructure is also located solely within Switzerland, and thus governed by the laws and regulations of Switzerland.</p>
    <p>Under Swiss law, the technical means for lawful interceptions of customer communications is governed by the Swiss Federal Act on the Surveillance of Postal and Telecommunications Traffic (SPTT) last amended in 2012. In the SPTT, the obligation to provide the technical means for lawful interception is imposed only on Internet access providers so the Company, as an Internet application provider, is not subject to this obligation and cannot be compelled to build in the technical means to intercept customer communications.</p>
    <h3>Data related to the opening of an account</h3>
    <p>Any emails provided to ProtonMail through either our waiting list, optional email verification, or optional notification/recovery email setting in your account, are considered personal data as defined and protection by the Swiss Federal Data Protection Act (DPA).</p>
    <p>Such data will only be used to contact you with important notifications about ProtonMail, to send you an invitation link to create your ProtonMail account, to verify your ProtonMail account, or to send you password recovery links if you enable the option.</p>
    <h3>Data Collection</h3>
    <p>Our company’s overriding policy is to collect as little user information as possible to ensure a completely private and anonymous user experience when using the Service. We also have no technical means to access your encrypted message contents.</p>
    <p>Service's user data collection is limited to the following:</p>
    <ul>
    	<li>Visiting our website: We employ a local installation of Piwik, an open source analytics tool, on the home page only. Piwik is not employed on any of the internal pages. It is not possible to link Piwik analytics data (such as IP addresses) to ProtonMail accounts.</li>
    	<li>Account creation: We do not require ANY personal information to create an account but you may provide an external email address for notification or password recovery purposes. Should you choose to provide it, we do associate another email address with your account (for password recovery, or notifications).</li>
    	<li>Account activity: Due to limitations of the SMTP protocol, we have access to the following email metadata: sender and recipient email addresses, the ip address incoming messages originated from, message subject, and message sent and received times. We do NOT have access to encrypted message content but unencrypted messages sent from external providers to ProtonMail are scanned for Spam and Viruses for the protection of our users. We also have access to the following records of account activity: number of messages sent, amount of storage space used, total number of messages, last login time.</li>
    	<li>Communicating with ProtonMail: Your communications with  the Company, such as support requests, bug reports, or feature requests may be saved by our staff.</li>
    	<li>IP Logging: ProtonMail does not log the IP addresses used to access our Service unless this feature is specifically enabled by the user (it is disabled by default).</li>
    	<li>Payment Information: The Company relies on third parties to process credit card, PayPal, Bitcoin,and Dash transactions so the Company necessarily must share payment information with third parties. Anonymous cash or bitcoin payments and donations are accepted however.</li>
    </ul>
    <h3>Data Use</h3>
    <p>We do not have any advertising on our site. Any data that we do have will never be shared except under the circumstances described below in Data Disclosure. We do NOT do any analysis on the limited data we do possess with two exceptions:</p>
    <ul>
    	<li>Emails sent unencrypted to ProtonMail accounts (e.g. Gmail to ProtonMail) are scanned automatically for spam so we can block IPs which are sending a lot of spam to ProtonMail users and place spam messages in a spam directory. Inbound message are scanned for spam in memory, and then encrypted and written to disk. We do not possess the technical ability to scan messages after they have been encrypted.</li>
    	<li>Emails sent by ProtonMail users to outside (e.g. Gmail) users with encryption disabled are scanned automatically for spam in the same manner as incoming email. This is to ensure a ProtonMail account which is being used for spamming purposes can be detected and locked so email deliverability for legitimate users is not degraded.</li>
    </ul>
    <p></p>
    <h3>Data Storage</h3>
    <p>All servers used in connection with the provisioning of the Service are located in Switzerland and wholly owned and operated by the Company. Only employees of the Company have physical or other access to the servers.
    Data is ALWAYS stored in encrypted format on our servers. Offline backups may be stored periodically, but these are also encrypted. We do not possess the ability to access any user encrypted message content on either the live servers or in the backups.</p>
    <h3>Data Retention</h3>
    <p>When a ProtonMail account is closed, data is immediately deleted from production servers. Active accounts will have data retained indefinitely. Deleted emails are also instantly deleted from production servers. Deleted data may be retained in our backups for up to 14 days.</p>
    <h3>Data Disclosure</h3>
    <p>We will only disclose the limited user data we possess if we receive an enforceable court order from either the Cantonal Courts of Geneva or the Swiss Federal Supreme Court. If a request is made for encrypted message content that ProtonMail does not possess the ability to decrypt, the fully encrypted message content may be turned over. If permitted by law, ProtonMail will always contact a user first before any data disclosure. Under Swiss law, it is obligatory to notify the target of a data request, although such notification may come from the authorities and not from the Company.</p>
    <h3>Modifications to Privacy Policy</h3>
    <p>ProtonMail reserves the right to periodically review and change this policy from time to time and we will notify users who have enabled the notification preference about changes to our Privacy Policy. Continued use of the Service will be deemed as acceptance of such changes.</p>
    <h3>Applicable Law</h3>
    <p>This Agreement shall be governed in all respects by the substantive laws of Switzerland. Any controversy, claim, or dispute arising out of or relating to the Agreement shall be subject to the jurisdiction of the competent courts of the Canton of Geneva, the jurisdiction of the Swiss Federal Court being expressly reserved.</p>

    <p>&nbsp;</p>

</div><!--/#content-->

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h4>ProtonMail</h4>
                <ul>
                    <li><a href="https://protonmail.com/pricing">Pricing</a></li>
                    <li><a href="https://protonmail.com/security-details">Security</a></li>
                    <li><a href="https://protonmail.com/blog/protonmail-threat-model/">Threat Model</a></li>
                    <li><a href="https://protonmail.com/press">Press</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Legal</h4>
                <ul>
                    <li><a href="https://protonmail.com/imprint">Imprint</a></li>
                    <li><a href="https://protonmail.com/privacy-policy">Privacy Policy</a></li>
                    <li><a href="https://protonmail.com/terms-and-conditions">Terms &amp; Conditions</a></li>
                    <li><a href="https://protonmail.com/blog/transparency-report/">Transparency Report</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Company</h4>
                <ul>
                    <li><a href="https://protonmail.com/blog/">Blog</a></li>
                    <li><a href="https://protonmail.com/about">Team</a></li>
                    <li><a href="https://protonmail.com/careers">Careers</a></li>
                    <li><a href="https://protonmail.com/support/">Support</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h4>Social</h4>
                <ul>
                </ul>
                <ul>
                    <li class="fb"><a href="https://facebook.com/ProtonMail"><i class="fa fa-facebook"></i> Facebook</a></li>
                    <li class="tw"><a href="https://twitter.com/ProtonMail"><i class="fa fa-twitter"></i> Twitter</a></li>
                    <li class="gplus"><a href="https://plus.google.com/+protonmail/posts" rel="publisher"><i class="fa fa-google"></i> Find us on Google+</a></li>
                    <li class="fb-forum"><a target="_blank" href="https://protonmail.uservoice.com/"><i class="fa fa-comments"></i> Feedback Forum</a></li>
                </ul>
            </div>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p class="text-center">&copy; 2016 Proton Technologies AG. All Rights Reserved.</p>
    </div>
</div>



<script src="js/site.js"></script>

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//stats.protonmail.ch/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 5]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//stats.protonmail.ch/piwik.php?idsite=5" style="border:0;" alt=""></p></noscript>
<!-- End Piwik Code -->

</body>
</html>
