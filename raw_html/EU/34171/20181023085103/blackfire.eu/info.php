<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="utf-8" />
<title>Blackfire.eu - Your European Distributor for Hobby Games and Collectibles</title>
<link href="image/favicon.png" rel="icon" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Blackfire.eu - Online-Shop">
<meta name="author" content="KMB">
  <link rel="stylesheet" type="text/css" href="css/stylesheet_v2.css" />
  <link rel="stylesheet" type="text/css" href="css/slideshow.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/flexslider.css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/carousel.css" media="screen" />
  <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.flexslider.js"></script>
  <script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
  <script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
  <script type="text/javascript" src="js/tabs.js"></script>
  <script type="text/javascript" src="js/jquery.dcjqaccordion.js"></script>
  <script type="text/javascript" src="js/custom.js"></script>
  <script type="text/javascript" src="js/html5.js"></script>
  
  
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700&subset=latin-ext,latin' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="wrapper-box">
  <div class="main-wrapper">
    <header id="header">
      <div class="htop">
        <div class="links">
|  <a href="account.php?act=login">Login</a> <a href="account.php?act=register"><strong>Register</strong></a></div>
        <div id="language"><a href="switch_language.php?to=cz" title="če&scaron;tina"><img src="image/flags/cz.png" alt="če&scaron;tina" /></a>
            <a href="switch_language.php?to=de" title="deutsch"><img src="image/flags/de.png" alt="deutsch" /></a> <a href="switch_language.php?to=en" title="english"><img src="image/flags/en.png" alt="english" /></a>  <a href="switch_language.php?to=es" title="espanol"><img src="image/flags/es.png" alt="espanol" /></a> <a href="switch_language.php?to=fr" title="français"><img src="image/flags/fr.png" alt="français" /></a> <a href="switch_language.php?to=it" title="italiano"><img src="image/flags/it.png" alt="italiano" /></a>
        </div>
      </div>
      <section class="hsecond">
        <div id="logo"><a href="index.php"><img src="image/logo.gif" title="Blackfire.eu Logo" alt="Blackfire.eu Logo" /></a></div>
        <div id="search">
           <form method="get" action="list.php" id="header-search">
            <div class="button-search" onclick="document.getElementById('header-search').submit();"></div>
            <input type="text" name="query" placeholder="Search" value="" />
          </form>
        </div>
        <section id="cart">
          <div class="heading">
            <h4><img width="32" height="32" alt="" src="image/cart-bg.png"></h4>
            <a href="cart.php" style="display: inline-block; text-align: center;"><span id="cart-total" >0 Product(s) / 0 Item(s)</span></a>
          </div>
        </section>
        <div class="clear"></div>
      </section>
      <nav id="menu">
        <ul>
          <li class="home"><a title="Home" href="index.php"><span>Home</span></a></li>
          <li style="display: block;"><a>Categories</a>
            <div id="ctabs_wrap">
              <ul id="ctabs">
              <li><a href="#ctabs_c_8">Action Figures</a></li><li><a href="#ctabs_c_13">Blackfire</a></li><li><a href="#ctabs_c_5">Board Games</a></li><li><a href="#ctabs_c_2">Card &amp; Game Supplies</a></li><li><a href="#ctabs_c_9">Card / Dice Games</a></li><li><a href="#ctabs_c_12">Funko</a></li><li><a href="#ctabs_c_10">Merchandising</a></li><li><a href="#ctabs_c_11">Role Playing Games</a></li><li><a href="#ctabs_c_4">Tabletop Miniatures</a></li><li><a href="#ctabs_c_1">Trading Card Games</a></li>              </ul>
              <div id="ctabs_c_8" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=196">Diamond Select Toys</a></li><li><a href="list.php?subcategory=333">Gaya Entertainment</a></li><li><a href="list.php?subcategory=83">Gentle Giant</a></li><li><a href="list.php?subcategory=229">Jakks Pacific</a></li><li><a href="list.php?subcategory=73">Kotobukiya</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=87">McFarlane Toys</a></li><li><a href="list.php?subcategory=61">NECA</a></li><li><a href="list.php?subcategory=88">Royal Bobbles</a></li><li><a href="list.php?subcategory=63">Sideshow Collectibles</a></li><li><a href="list.php?subcategory=233">Underground Toys</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=97">Various</a></li><li><a href="list.php?category=8&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=8&amp;type=preorders"> &middot; Preorders</a></li><li><a href="list.php?category=8&amp;type=closeout"> &middot; Close Out</a></li></ul></div></div><div id="ctabs_c_13" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=384">Blackfire Albums</a></li><li><a href="list.php?subcategory=382">Blackfire Bags</a></li><li><a href="list.php?subcategory=109">Blackfire Board Games</a></li><li><a href="list.php?subcategory=285">Blackfire Card Games</a></li><li><a href="list.php?subcategory=381">Blackfire Card Stands</a></li><li><a href="list.php?subcategory=426">Blackfire Deck Boxes</a></li><li><a href="list.php?subcategory=295">Blackfire Dice</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=347">Blackfire Dice Towers</a></li><li><a href="list.php?subcategory=401">Blackfire Dice Trays</a></li><li><a href="list.php?subcategory=403">Blackfire Life Counters</a></li><li><a href="list.php?subcategory=360">Blackfire Organizers</a></li><li><a href="list.php?subcategory=433">Blackfire Pages</a></li><li><a href="list.php?subcategory=372">Blackfire Playmats</a></li><li><a href="list.php?subcategory=377">Blackfire Sleeves</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=345">Blackfire Specials</a></li><li><a href="list.php?subcategory=353">Blackfire Storage Boxes</a></li><li><a href="list.php?subcategory=361">Blackfire Trophy</a></li><li><a href="list.php?category=13&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=13&amp;type=preorders"> &middot; Preorders</a></li></ul></div></div><div id="ctabs_c_5" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=343">2Geeks</a></li><li><a href="list.php?subcategory=173">Abacus Spiele</a></li><li><a href="list.php?subcategory=258">Academy Games</a></li><li><a href="list.php?subcategory=151">Agricola</a></li><li><a href="list.php?subcategory=128">Alderac Entertainment</a></li><li><a href="list.php?subcategory=405">Alien vs Predator DE</a></li><li><a href="list.php?subcategory=404">Alien vs Predator EN</a></li><li><a href="list.php?subcategory=341">Arcane Wonders</a></li><li><a href="list.php?subcategory=252">Argentum Verlag</a></li><li><a href="list.php?subcategory=101">Arkham Horror</a></li><li><a href="list.php?subcategory=230">Asmodee</a></li><li><a href="list.php?subcategory=24">Avalon Hill</a></li><li><a href="list.php?subcategory=22">Axis &amp; Allies</a></li><li><a href="list.php?subcategory=102">Battlestar Galactica</a></li><li><a href="list.php?subcategory=259">Bezier Games Inc.</a></li><li><a href="list.php?subcategory=41">Board Game Sleeves</a></li><li><a href="list.php?subcategory=448">Breaking Games</a></li><li><a href="list.php?subcategory=188">Calliope Games</a></li><li><a href="list.php?subcategory=150">Carcassonne</a></li><li><a href="list.php?subcategory=257">Catalyst Game Labs</a></li><li><a href="list.php?subcategory=301">Catan</a></li><li><a href="list.php?subcategory=443">Cephalofair Games</a></li><li><a href="list.php?subcategory=324">Cheapass Games</a></li><li><a href="list.php?subcategory=427">Compass Games</a></li><li><a href="list.php?subcategory=138">Conflict of Heroes</a></li><li><a href="list.php?subcategory=190">Cool Mini or Not</a></li><li><a href="list.php?subcategory=103">Cosmic Encounter</a></li><li><a href="list.php?subcategory=113">Cryptozoic</a></li><li><a href="list.php?subcategory=180">Czech Board Games</a></li><li><a href="list.php?subcategory=371">Daily Magic Games</a></li><li><a href="list.php?subcategory=50">Days of Wonder</a></li><li><a href="list.php?subcategory=92">Descent</a></li><li><a href="list.php?subcategory=452">Dragon Dawn Productions</a></li><li><a href="list.php?subcategory=25">Dungeons &amp; Dragons</a></li><li><a href="list.php?subcategory=326">Dyskami Publishing</a></li><li><a href="list.php?subcategory=468">Eagle-Gryphon Games</a></li><li><a href="list.php?subcategory=456">Facade Games</a></li><li><a href="list.php?subcategory=76">Fantasy Flight Games</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=339">Feuerland Spiele</a></li><li><a href="list.php?subcategory=91">Fireside Games</a></li><li><a href="list.php?subcategory=313">Frosted Games</a></li><li><a href="list.php?subcategory=209">Funforge</a></li><li><a href="list.php?subcategory=171">G3 Games</a></li><li><a href="list.php?subcategory=441">Galakta Games</a></li><li><a href="list.php?subcategory=67">Gale Force Nine</a></li><li><a href="list.php?subcategory=363">Game Brewer</a></li><li><a href="list.php?subcategory=430">Gamelyn Games</a></li><li><a href="list.php?subcategory=291">GDM Games</a></li><li><a href="list.php?subcategory=99">Giochix</a></li><li><a href="list.php?subcategory=278">GreenBrier Games</a></li><li><a href="list.php?subcategory=327">Grey Fox Games</a></li><li><a href="list.php?subcategory=312">Grimlord Games</a></li><li><a href="list.php?subcategory=93">Hasbro</a></li><li><a href="list.php?subcategory=95">Huch! &amp; Friends</a></li><li><a href="list.php?subcategory=136">IDW Games</a></li><li><a href="list.php?subcategory=165">Iello</a></li><li><a href="list.php?subcategory=59">Indie Boards and Cards</a></li><li><a href="list.php?subcategory=260">Jasco Games</a></li><li><a href="list.php?subcategory=199">Jolly Roger Games</a></li><li><a href="list.php?subcategory=417">Kosmos</a></li><li><a href="list.php?subcategory=72">Krosmaster</a></li><li><a href="list.php?subcategory=246">Libellud</a></li><li><a href="list.php?subcategory=316">LudiCreations</a></li><li><a href="list.php?subcategory=325">Ludonaute</a></li><li><a href="list.php?subcategory=446">Ludus Magnus Studio</a></li><li><a href="list.php?subcategory=105">Mage Company</a></li><li><a href="list.php?subcategory=74">Mage Wars</a></li><li><a href="list.php?subcategory=331">Magellan Games</a></li><li><a href="list.php?subcategory=183">Mantic Games</a></li><li><a href="list.php?subcategory=75">Mayfair Games</a></li><li><a href="list.php?subcategory=261">Mega Man</a></li><li><a href="list.php?subcategory=52">Memoir &#039;44</a></li><li><a href="list.php?subcategory=317">Monolith</a></li><li><a href="list.php?subcategory=154">Neuroshima Hex</a></li><li><a href="list.php?subcategory=445">Ninja Division Publishing</a></li><li><a href="list.php?subcategory=153">NSKN Games</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=305">Osprey Games</a></li><li><a href="list.php?subcategory=351">PD-Verlag</a></li><li><a href="list.php?subcategory=328">Pearl Games</a></li><li><a href="list.php?subcategory=147">Plaid Hat Games</a></li><li><a href="list.php?subcategory=239">Polish Publishing League</a></li><li><a href="list.php?subcategory=206">Portal Games</a></li><li><a href="list.php?subcategory=205">Portal Games</a></li><li><a href="list.php?subcategory=323">PSC Games</a></li><li><a href="list.php?subcategory=297">Queen Games</a></li><li><a href="list.php?subcategory=464">Quined Games</a></li><li><a href="list.php?subcategory=253">Ravensburger</a></li><li><a href="list.php?subcategory=234">Renegade Game Studios</a></li><li><a href="list.php?subcategory=251">Repos Production</a></li><li><a href="list.php?subcategory=112">Rio Grande Games</a></li><li><a href="list.php?subcategory=85">Risk</a></li><li><a href="list.php?subcategory=197">Rum and Bones</a></li><li><a href="list.php?subcategory=54">Shadows over Camelot</a></li><li><a href="list.php?subcategory=53">Small World</a></li><li><a href="list.php?subcategory=262">Space Cowboys</a></li><li><a href="list.php?subcategory=110">Spielworxx</a></li><li><a href="list.php?subcategory=458">Starling Games</a></li><li><a href="list.php?subcategory=100">Steve Jackson Games</a></li><li><a href="list.php?subcategory=346">Stonemaier Games</a></li><li><a href="list.php?subcategory=90">Stronghold Games</a></li><li><a href="list.php?subcategory=213">Tasty Minstrel Games</a></li><li><a href="list.php?subcategory=461">Tau Leader Games</a></li><li><a href="list.php?subcategory=51">Ticket to Ride</a></li><li><a href="list.php?subcategory=364">Ultra Pro</a></li><li><a href="list.php?subcategory=362">Upper Deck</a></li><li><a href="list.php?subcategory=437">USAopoly</a></li><li><a href="list.php?subcategory=108">Various</a></li><li><a href="list.php?subcategory=450">Victory Point Games</a></li><li><a href="list.php?subcategory=49">WizKids/NECA</a></li><li><a href="list.php?subcategory=94">Z-Man Games</a></li><li><a href="list.php?subcategory=84">Zombicide</a></li><li><a href="list.php?subcategory=111">Zombies!!!</a></li><li><a href="list.php?category=5&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=5&amp;type=preorders"> &middot; Preorders</a></li><li><a href="list.php?category=5&amp;type=closeout"> &middot; Close Out</a></li></ul></div></div><div id="ctabs_c_2" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=11">Albums &amp; Pages</a></li><li><a href="list.php?subcategory=17">Bushiroad Small Sleeves</a></li><li><a href="list.php?subcategory=12">Bushiroad Standard Sleeves</a></li><li><a href="list.php?subcategory=32">Bushiroad Supplies</a></li><li><a href="list.php?subcategory=16">Comic Bags</a></li><li><a href="list.php?subcategory=211">Cryptozoic</a></li><li><a href="list.php?subcategory=178">Dice</a></li><li><a href="list.php?subcategory=29">Dragon Shield Small Sleeves</a></li><li><a href="list.php?subcategory=70">Dragon Shield Standard Sleeves</a></li><li><a href="list.php?subcategory=7">Fantasy Flight Games</a></li><li><a href="list.php?subcategory=194">HCD Supplies - Sleeves</a></li><li><a href="list.php?subcategory=387">KakapopoTCG Cases</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=386">KakapopoTCG Deck Boxes</a></li><li><a href="list.php?subcategory=28">KMC Small Sleeves</a></li><li><a href="list.php?subcategory=27">KMC Standard Sleeves</a></li><li><a href="list.php?subcategory=191">Legion Deck Boxes</a></li><li><a href="list.php?subcategory=192">Legion Portfolios</a></li><li><a href="list.php?subcategory=419">Legion Small Sleeves</a></li><li><a href="list.php?subcategory=193">Legion Standard Sleeves</a></li><li><a href="list.php?subcategory=232">Legion Tins</a></li><li><a href="list.php?subcategory=18">Play Mats</a></li><li><a href="list.php?subcategory=201">Pokemon Supplies</a></li><li><a href="list.php?subcategory=9">Portfolios</a></li><li><a href="list.php?subcategory=30">Storage Boxes</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=31">Token / Counter</a></li><li><a href="list.php?subcategory=8">Ultra Pro Deck Boxes</a></li><li><a href="list.php?subcategory=19">Ultra Pro Pro-Binder</a></li><li><a href="list.php?subcategory=69">Ultra Pro Small Sleeves</a></li><li><a href="list.php?subcategory=68">Ultra Pro Standard Sleeves</a></li><li><a href="list.php?subcategory=355">Ultra Pro Toploaders</a></li><li><a href="list.php?subcategory=13">Various</a></li><li><a href="list.php?subcategory=36">Yu-Gi-Oh! Supplies</a></li><li><a href="list.php?category=2&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=2&amp;type=preorders"> &middot; Preorders</a></li><li><a href="list.php?category=2&amp;type=closeout"> &middot; Close Out</a></li></ul></div></div><div id="ctabs_c_9" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=121">A Game of Thrones LCG</a></li><li><a href="list.php?subcategory=176">Abacus Spiele</a></li><li><a href="list.php?subcategory=208">Action Phase Games</a></li><li><a href="list.php?subcategory=141">Alderac Entertainment Group</a></li><li><a href="list.php?subcategory=122">Android Netrunner LCG</a></li><li><a href="list.php?subcategory=340">Arcane Wonders</a></li><li><a href="list.php?subcategory=349">Artipia Games</a></li><li><a href="list.php?subcategory=231">Asmodee</a></li><li><a href="list.php?subcategory=424">Bandai</a></li><li><a href="list.php?subcategory=276">Bezier Games Inc.</a></li><li><a href="list.php?subcategory=175">Boss Monster</a></li><li><a href="list.php?subcategory=449">Breaking Games</a></li><li><a href="list.php?subcategory=271">Bushiroad</a></li><li><a href="list.php?subcategory=270">Calliope Games</a></li><li><a href="list.php?subcategory=256">Catalyst Game Labs</a></li><li><a href="list.php?subcategory=202">Cheapass Games</a></li><li><a href="list.php?subcategory=245">Cool Mini Or Not</a></li><li><a href="list.php?subcategory=126">Cryptozoic</a></li><li><a href="list.php?subcategory=314">Cubicle 7</a></li><li><a href="list.php?subcategory=392">Daily Magic Games</a></li><li><a href="list.php?subcategory=120">Dice Masters</a></li><li><a href="list.php?subcategory=337">District Games</a></li><li><a href="list.php?subcategory=268">Doomtown</a></li><li><a href="list.php?subcategory=453">Dragon Dawn Productions</a></li><li><a href="list.php?subcategory=469">Eagle-Gryphon Games</a></li><li><a href="list.php?subcategory=457">Erika Svanoe Games</a></li><li><a href="list.php?subcategory=455">Facade Games</a></li><li><a href="list.php?subcategory=158">Fantasy Flight Games</a></li><li><a href="list.php?subcategory=380">FFG Organised Play</a></li><li><a href="list.php?subcategory=166">Fireside Games</a></li><li><a href="list.php?subcategory=330">FloodGate Games</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=393">Frosted Games</a></li><li><a href="list.php?subcategory=442">Galakta Games</a></li><li><a href="list.php?subcategory=265">Gale Force Nine</a></li><li><a href="list.php?subcategory=413">Game Brewer</a></li><li><a href="list.php?subcategory=304">GDM Games</a></li><li><a href="list.php?subcategory=322">GreenBrier Games</a></li><li><a href="list.php?subcategory=307">HUCH! &amp; friends</a></li><li><a href="list.php?subcategory=189">IDW Games</a></li><li><a href="list.php?subcategory=263">Iello</a></li><li><a href="list.php?subcategory=144">Indie Boards and Cards</a></li><li><a href="list.php?subcategory=204">Japanime Games</a></li><li><a href="list.php?subcategory=200">Jolly Rogers Games</a></li><li><a href="list.php?subcategory=454">Jon Sudbury Games</a></li><li><a href="list.php?subcategory=416">Kosmos</a></li><li><a href="list.php?subcategory=466">Leder Games</a></li><li><a href="list.php?subcategory=247">Libellud</a></li><li><a href="list.php?subcategory=272">Lookout Games</a></li><li><a href="list.php?subcategory=242">Looney Labs</a></li><li><a href="list.php?subcategory=123">Lord of the Rings LCG</a></li><li><a href="list.php?subcategory=267">Love Letter</a></li><li><a href="list.php?subcategory=274">Mage Wars</a></li><li><a href="list.php?subcategory=332">Magellan Games</a></li><li><a href="list.php?subcategory=279">Mayfair Games</a></li><li><a href="list.php?subcategory=145">Mercury Games</a></li><li><a href="list.php?subcategory=77">Munchkin</a></li><li><a href="list.php?subcategory=465">New Experience Workshop</a></li><li><a href="list.php?subcategory=444">Ninja Division Publishing</a></li><li><a href="list.php?subcategory=350">NSKN Games</a></li><li><a href="list.php?subcategory=306">Osprey Games</a></li><li><a href="list.php?subcategory=321">Overworld Games</a></li><li><a href="list.php?subcategory=163">Pathfinder Adventure Card Game</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=329">Pearl Games</a></li><li><a href="list.php?subcategory=244">Plaid Hat Games</a></li><li><a href="list.php?subcategory=277">Polish Publishing League</a></li><li><a href="list.php?subcategory=205">Portal Games</a></li><li><a href="list.php?subcategory=296">Queen Games</a></li><li><a href="list.php?subcategory=463">Quined Games</a></li><li><a href="list.php?subcategory=243">Red Raven Games</a></li><li><a href="list.php?subcategory=235">Renegade Game Studios</a></li><li><a href="list.php?subcategory=250">Repos Production</a></li><li><a href="list.php?subcategory=342">Shadowrun</a></li><li><a href="list.php?subcategory=266">Smash Up</a></li><li><a href="list.php?subcategory=264">Space Cowboys</a></li><li><a href="list.php?subcategory=124">Star Wars LCG</a></li><li><a href="list.php?subcategory=459">Starling Games</a></li><li><a href="list.php?subcategory=148">Steve Jackson Games</a></li><li><a href="list.php?subcategory=318">Stone Blade Entertainment</a></li><li><a href="list.php?subcategory=167">Stronghold Games</a></li><li><a href="list.php?subcategory=149">Tasty Minstrel Games</a></li><li><a href="list.php?subcategory=357">Ultra Pro</a></li><li><a href="list.php?subcategory=179">Upper Deck</a></li><li><a href="list.php?subcategory=438">USAopoly</a></li><li><a href="list.php?subcategory=169">Various</a></li><li><a href="list.php?subcategory=395">Vice Games</a></li><li><a href="list.php?subcategory=125">Warhammer</a></li><li><a href="list.php?subcategory=157">White Wizard Games</a></li><li><a href="list.php?subcategory=174">WizKids/NECA</a></li><li><a href="list.php?subcategory=168">Z-Man Games</a></li><li><a href="list.php?category=9&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=9&amp;type=preorders"> &middot; Preorders</a></li><li><a href="list.php?category=9&amp;type=closeout"> &middot; Close Out</a></li></ul></div></div><div id="ctabs_c_12" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=425">Funko 5 Star</a></li><li><a href="list.php?subcategory=379">Funko 8-Bit POP!</a></li><li><a href="list.php?subcategory=385">Funko Action Figures</a></li><li><a href="list.php?subcategory=214">Funko Dorbz</a></li><li><a href="list.php?subcategory=78">Funko Fabrikations</a></li><li><a href="list.php?subcategory=240">Funko Hikari Sofubi</a></li><li><a href="list.php?subcategory=115">Funko Keychains</a></li><li><a href="list.php?subcategory=82">Funko Legacy Collection</a></li><li><a href="list.php?subcategory=215">Funko Mopeez</a></li><li><a href="list.php?subcategory=81">Funko Mystery Minis</a></li><li><a href="list.php?subcategory=275">Funko Non-Retro</a></li><li><a href="list.php?subcategory=319">Funko Plush</a></li><li><a href="list.php?subcategory=184">Funko Pocket POP!</a></li><li><a href="list.php?subcategory=217">Funko POP! - Animation</a></li><li><a href="list.php?subcategory=397">Funko POP! - Comics</a></li><li><a href="list.php?subcategory=218">Funko POP! - Disney</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=308">Funko POP! - Football</a></li><li><a href="list.php?subcategory=227">Funko POP! - Game of Thrones</a></li><li><a href="list.php?subcategory=219">Funko POP! - Games</a></li><li><a href="list.php?subcategory=220">Funko POP! - Heroes</a></li><li><a href="list.php?subcategory=310">Funko POP! - Hockey</a></li><li><a href="list.php?subcategory=238">Funko POP! - Homewares</a></li><li><a href="list.php?subcategory=221">Funko POP! - Marvel</a></li><li><a href="list.php?subcategory=222">Funko POP! - Movies</a></li><li><a href="list.php?subcategory=320">Funko POP! - Pets</a></li><li><a href="list.php?subcategory=241">Funko POP! - Pins</a></li><li><a href="list.php?subcategory=224">Funko POP! - Rides</a></li><li><a href="list.php?subcategory=223">Funko POP! - Rocks</a></li><li><a href="list.php?subcategory=429">Funko POP! - Royal</a></li><li><a href="list.php?subcategory=390">Funko POP! - Sports</a></li><li><a href="list.php?subcategory=225">Funko POP! - Star Wars</a></li><li><a href="list.php?subcategory=226">Funko POP! - Television</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=62">Funko POP! - Various</a></li><li><a href="list.php?subcategory=228">Funko POP! - WWE</a></li><li><a href="list.php?subcategory=185">Funko POP! Oversized</a></li><li><a href="list.php?subcategory=236">Funko POP! Tees</a></li><li><a href="list.php?subcategory=80">Funko ReAction Series</a></li><li><a href="list.php?subcategory=237">Funko Rock Candy</a></li><li><a href="list.php?subcategory=216">Funko Vinyl Collectible</a></li><li><a href="list.php?subcategory=156">Funko Vinyl Idolz</a></li><li><a href="list.php?subcategory=181">Funko Vinyl Vixens</a></li><li><a href="list.php?subcategory=79">Funko Vinyl&sup3; Cubed</a></li><li><a href="list.php?subcategory=359">Funko Vynl</a></li><li><a href="list.php?subcategory=58">Funko Wacky Wobbler</a></li><li><a href="list.php?subcategory=64">Funko Wisecracks</a></li><li><a href="list.php?subcategory=358">Pint Size Heroes</a></li><li><a href="list.php?category=12&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=12&amp;type=preorders"> &middot; Preorders</a></li><li><a href="list.php?category=12&amp;type=closeout"> &middot; Close Out</a></li></ul></div></div><div id="ctabs_c_10" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=117">Badges / Buttons</a></li><li><a href="list.php?subcategory=336">Bags</a></li><li><a href="list.php?subcategory=311">Books</a></li><li><a href="list.php?subcategory=374">Bunch O Balloons</a></li><li><a href="list.php?subcategory=119">Chopsticks</a></li><li><a href="list.php?subcategory=335">Dog Tags</a></li><li><a href="list.php?subcategory=367">Fidget Cubes</a></li><li><a href="list.php?subcategory=369">Fidget Spinners</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=409">Headgear</a></li><li><a href="list.php?subcategory=299">Keychains</a></li><li><a href="list.php?subcategory=334">Masks</a></li><li><a href="list.php?subcategory=210">Mugs</a></li><li><a href="list.php?subcategory=375">Plush</a></li><li><a href="list.php?subcategory=118">Poster / Wallpaper</a></li><li><a href="list.php?subcategory=116">Puzzles</a></li><li><a href="list.php?subcategory=309">Sakami Merchandise</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=170">Silicone / Ice Trays</a></li><li><a href="list.php?subcategory=187">T-Shirts</a></li><li><a href="list.php?subcategory=131">Twilight</a></li><li><a href="list.php?subcategory=298">Various</a></li><li><a href="list.php?category=10&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=10&amp;type=preorders"> &middot; Preorders</a></li><li><a href="list.php?category=10&amp;type=closeout"> &middot; Close Out</a></li></ul></div></div><div id="ctabs_c_11" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=139">Bezier Games Inc.</a></li><li><a href="list.php?subcategory=132">Dungeons &amp; Dragons</a></li><li><a href="list.php?subcategory=365">Dungeons &amp; Dragons DE</a></li><li><a href="list.php?subcategory=134">Fantasy Flight Games</a></li><li><a href="list.php?subcategory=378">Genesys RPG</a></li><li><a href="list.php?subcategory=406">Mutants &amp; Masterminds RPG</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=164">Pathfinder RPG</a></li><li><a href="list.php?subcategory=388">Prometheus Games</a></li><li><a href="list.php?subcategory=408">Renegade Game Studios</a></li><li><a href="list.php?subcategory=133">Star Wars RPG</a></li><li><a href="list.php?subcategory=396">Starfinder RPG</a></li><li><a href="list.php?subcategory=269">Steve Jackson Games</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=422">Vampire: The Masquerade</a></li><li><a href="list.php?subcategory=423">Various</a></li><li><a href="list.php?subcategory=418">Warhammer</a></li><li><a href="list.php?category=11&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=11&amp;type=preorders"> &middot; Preorders</a></li></ul></div></div><div id="ctabs_c_4" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=48">Attack Wing</a></li><li><a href="list.php?subcategory=286">Battlezones</a></li><li><a href="list.php?subcategory=394">Blood Red Skies</a></li><li><a href="list.php?subcategory=366">Brushes</a></li><li><a href="list.php?subcategory=402">Conquest</a></li><li><a href="list.php?subcategory=280">Deadzone</a></li><li><a href="list.php?subcategory=106">Dice Masters</a></li><li><a href="list.php?subcategory=370">Doctor Who</a></li><li><a href="list.php?subcategory=282">Dungeon Saga</a></li><li><a href="list.php?subcategory=21">Dungeons &amp; Dragons</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=290">Gale Force Nine</a></li><li><a href="list.php?subcategory=47">HeroClix</a></li><li><a href="list.php?subcategory=281">Kings of War</a></li><li><a href="list.php?subcategory=368">Modelling Material</a></li><li><a href="list.php?subcategory=411">Modiphius Entertainment</a></li><li><a href="list.php?subcategory=383">Pathfinder</a></li><li><a href="list.php?subcategory=399">Star Saga</a></li><li><a href="list.php?subcategory=415">Star Trek</a></li><li><a href="list.php?subcategory=162">Star Wars</a></li><li><a href="list.php?subcategory=400">TerrainCrate</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=414">Test of Honour</a></li><li><a href="list.php?subcategory=289">The Walking Dead</a></li><li><a href="list.php?subcategory=421">Various</a></li><li><a href="list.php?subcategory=436">Warcradle Studios</a></li><li><a href="list.php?subcategory=391">Wardlings</a></li><li><a href="list.php?subcategory=356">Warpath</a></li><li><a href="list.php?subcategory=142">WizKids</a></li><li><a href="list.php?category=4&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=4&amp;type=preorders"> &middot; Preorders</a></li><li><a href="list.php?category=4&amp;type=closeout"> &middot; Close Out</a></li></ul></div></div><div id="ctabs_c_1" class="tab_content"><div class="column">
                                <ul><li><a href="list.php?subcategory=43">Adrenalyn XL</a></li><li><a href="list.php?subcategory=39">Cardfight!! Vanguard</a></li><li><a href="list.php?subcategory=354">Dragoborne TCG</a></li><li><a href="list.php?subcategory=56">Force Attax</a></li><li><a href="list.php?subcategory=40">Future Card Buddyfight</a></li><li><a href="list.php?subcategory=249">Legend of Zelda Trading Cards</a></li><li><a href="list.php?subcategory=412">Lego Ninjago</a></li><li><a href="list.php?subcategory=207">Luck &amp; Logic TCG</a></li><li><a href="list.php?subcategory=2">Magic: The Gathering - DE</a></li><li><a href="list.php?subcategory=1">Magic: The Gathering - EN</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=4">Magic: The Gathering - FR</a></li><li><a href="list.php?subcategory=3">Magic: The Gathering - IT</a></li><li><a href="list.php?subcategory=6">Magic: The Gathering - PT</a></li><li><a href="list.php?subcategory=5">Magic: The Gathering - SP</a></li><li><a href="list.php?subcategory=55">Match Attax</a></li><li><a href="list.php?subcategory=45">My Little Pony CCG</a></li><li><a href="list.php?subcategory=71">Panini</a></li><li><a href="list.php?subcategory=34">Pokemon - DE</a></li><li><a href="list.php?subcategory=20">Pokemon - EN</a></li><li><a href="list.php?subcategory=37">The Spoils</a></li></ul></div><div class="column">
                                <ul><li><a href="list.php?subcategory=57">Transformers TCG</a></li><li><a href="list.php?subcategory=182">Universal Fighting System</a></li><li><a href="list.php?subcategory=137">Various</a></li><li><a href="list.php?subcategory=38">Wei&szlig; / Schwarz</a></li><li><a href="list.php?subcategory=33">Yu-Gi-Oh! - DE</a></li><li><a href="list.php?subcategory=14">Yu-Gi-Oh! - EN</a></li><li><a href="list.php?category=1&amp;type=new"> &middot; New Items</a></li><li><a href="list.php?category=1&amp;type=preorders"> &middot; Preorders</a></li><li><a href="list.php?category=1&amp;type=closeout"> &middot; Close Out</a></li></ul></div></div>            </div>
                    <script type="text/javascript">
            (function() {
            $(document).ready(function() {
                $("#ctabs_wrap .tab_content").addClass("deactive");
                $("#ctabs_wrap .tab_content:first").removeClass("deactive");
                $("ul#ctabs li:first").addClass("active").show();
                $("ul#ctabs li").click(function() {
                    $("ul#ctabs li").removeClass("active");
                    $(this).addClass("active");
                    $("#ctabs_wrap .tab_content").hide();
                    var activeTab = $(this).find("a").attr("href");
                    $(activeTab).fadeIn();
                    return false;
                });
            });}());
            </script>
          </li>
          <li><a>New Products</a>
            <div>
              <ul>
                <li><a href="list.php?category=8&amp;type=new">Action Figures</a></li><li><a href="list.php?category=13&amp;type=new">Blackfire</a></li><li><a href="list.php?category=5&amp;type=new">Board Games</a></li><li><a href="list.php?category=2&amp;type=new">Card &amp; Game Supplies</a></li><li><a href="list.php?category=9&amp;type=new">Card / Dice Games</a></li><li><a href="list.php?category=12&amp;type=new">Funko</a></li><li><a href="list.php?category=10&amp;type=new">Merchandising</a></li><li><a href="list.php?category=11&amp;type=new">Role Playing Games</a></li><li><a href="list.php?category=4&amp;type=new">Tabletop Miniatures</a></li><li><a href="list.php?category=1&amp;type=new">Trading Card Games</a></li>              </ul>
            </div>
          </li>
          <li><a>Preorders</a>
            <div>
              <ul>
                <li><a href="list.php?category=8&amp;type=preorders">Action Figures</a></li><li><a href="list.php?category=13&amp;type=preorders">Blackfire</a></li><li><a href="list.php?category=5&amp;type=preorders">Board Games</a></li><li><a href="list.php?category=2&amp;type=preorders">Card &amp; Game Supplies</a></li><li><a href="list.php?category=9&amp;type=preorders">Card / Dice Games</a></li><li><a href="list.php?category=12&amp;type=preorders">Funko</a></li><li><a href="list.php?category=10&amp;type=preorders">Merchandising</a></li><li><a href="list.php?category=7&amp;type=preorders">Puzzles</a></li><li><a href="list.php?category=11&amp;type=preorders">Role Playing Games</a></li><li><a href="list.php?category=4&amp;type=preorders">Tabletop Miniatures</a></li><li><a href="list.php?category=1&amp;type=preorders">Trading Card Games</a></li>              </ul>
            </div>
          </li>
          <li><a>Close Out</a>
            <div>
            <ul>
              <li><a href="list.php?category=8&amp;type=closeout">Action Figures</a></li><li><a href="list.php?category=5&amp;type=closeout">Board Games</a></li><li><a href="list.php?category=2&amp;type=closeout">Card &amp; Game Supplies</a></li><li><a href="list.php?category=9&amp;type=closeout">Card / Dice Games</a></li><li><a href="list.php?category=12&amp;type=closeout">Funko</a></li><li><a href="list.php?category=10&amp;type=closeout">Merchandising</a></li><li><a href="list.php?category=4&amp;type=closeout">Tabletop Miniatures</a></li><li><a href="list.php?category=1&amp;type=closeout">Trading Card Games</a></li>            </ul>
            </div>
          </li>
          <li><a>Brands&nbsp;/&nbsp;Licenses</a>
            <div>
              <form method="get" action="list.php">
                <select name="brand" onchange="this.form.submit();">
                  <option value="">Select a Brand</option>
                  <option value="352">25th Century Games</option><option value="205">2Geeks</option><option value="13">4D Cityscape</option><option value="277">4Dados</option><option value="209">A-Games</option><option value="79">Abacus Spiele</option><option value="263">Abba Games</option><option value="57">Academy Games</option><option value="119">Action Phase Games</option><option value="312">Adam&#039;s Apple Games</option><option value="63">ADC Blackfire Entertainment GmbH</option><option value="33">Alderac Entertainment Group</option><option value="376">All Or None Games</option><option value="246">Antler Games</option><option value="231">Antsy Labs</option><option value="309">APE Games</option><option value="8">Arcane Tinmen</option><option value="26">Arcane Wonders</option><option value="147">Argentum Verlag</option><option value="213">Artipia Games</option><option value="150">Artistic Justice Games</option><option value="130">Asmodee</option><option value="326">Atheris Games</option><option value="233">Atomo Games</option><option value="245">Awaken Realms</option><option value="287">Backspindle Games</option><option value="252">Baksha Games</option><option value="303">Bandai</option><option value="219">Bellwether Games</option><option value="58">Bezier Games Inc.</option><option value="274">Blue Ocean</option><option value="251">Blue Orange</option><option value="335">Board to Death</option><option value="145">Bombyx</option><option value="355">Borangame</option><option value="160">Brain Games</option><option value="374">Breaking Games</option><option value="47">Brotherwise Games</option><option value="302">Burnt Island Games</option><option value="7">Bushiroad</option><option value="85">Calliope Games</option><option value="293">Capital Gains Studio</option><option value="317">Captain Macaque</option><option value="11">Cartonboxes</option><option value="103">Catalyst Game Labs</option><option value="235">Catan Studio</option><option value="216">Cephalofair Games</option><option value="97">Cheapass Games</option><option value="286">Cogitate Games</option><option value="156">Comic Studio</option><option value="318">Compass Games</option><option value="51">Cool Mini Or Not</option><option value="196">Cosmangacraft.com</option><option value="339">Crit Games</option><option value="10">Cryptozoic</option><option value="141">Cubicle 7</option><option value="81">Czech Board Games</option><option value="237">Daily Magic Games</option><option value="267">Dann Kriss Games</option><option value="17">Days of Wonder</option><option value="212">DeckTutor</option><option value="299">Deep Water Games</option><option value="48">Devil Pig Games</option><option value="143">Disney</option><option value="199">District Games</option><option value="203">Distridigital</option><option value="164">Dizemo</option><option value="358">Don&rsquo;t Panic Games</option><option value="367">Double-Edge Games</option><option value="340">DPH Games</option><option value="166">Draco Ideas</option><option value="136">Dragon Dawn Productions</option><option value="356">DTDA Games</option><option value="92">Dyskami Publishing</option><option value="385">Eagle-Gryphon Games</option><option value="238">Edge Entertainment</option><option value="163">Elege</option><option value="14">EnterPLAY</option><option value="378">Erika Svanoe Games</option><option value="228">Euler&rsquo;s Game</option><option value="343">Fabricate</option><option value="348">Facade Games</option><option value="28">Fantasy Flight Games</option><option value="315">Far Future Enterprises</option><option value="253">Feuerland Extras</option><option value="200">Feuerland Spiele</option><option value="327">Fidget Creative</option><option value="284">Final Frontier Games</option><option value="72">Fireside Games</option><option value="325">First Fish Games</option><option value="120">FloodGate Games</option><option value="333">Flying Leap Games</option><option value="282">Formal Ferret Games</option><option value="116">Frosted Games</option><option value="206">Fully Baked Ideas</option><option value="86">Funforge</option><option value="18">Funko</option><option value="77">G3 Games</option><option value="360">Galakta Games</option><option value="22">Gale Force Nine</option><option value="225">Game Brewer</option><option value="320">Gamelyn Games</option><option value="204">Games 4 Gamers</option><option value="362">Gamestart Edizioni</option><option value="234">GameWorks</option><option value="198">Gamia Games</option><option value="174">Gaya Entertainment</option><option value="169">GDM Games</option><option value="24">Gentle Giant</option><option value="39">Giochix</option><option value="313">Giortech</option><option value="359">Goblivion Games</option><option value="381">God Hates Games</option><option value="323">Gotha Games</option><option value="220">Grail Games</option><option value="310">Green Couch Games</option><option value="106">Green Ronin Publishing</option><option value="93">GreenBrier Games</option><option value="111">Grey Fox Games</option><option value="170">Grimlord Games</option><option value="94">HCD Game Supplies</option><option value="373">Hellion Cat</option><option value="319">Hexy Studio</option><option value="329">HGN Games</option><option value="25">Hot Toys</option><option value="35">HUCH! &amp; friends</option><option value="259">Hush Hush Projects</option><option value="53">IDW Games</option><option value="71">Iello</option><option value="30">Imperial Distribution</option><option value="60">Indie Boards and Cards</option><option value="268">Jack Dire Studios</option><option value="129">Jakks Pacific</option><option value="5">Japanime Games</option><option value="82">Jasco Games</option><option value="298">Jellybean Games</option><option value="49">Jolly Roger Games</option><option value="377">Jon Sudbury Games</option><option value="250">KakapopoTCG</option><option value="157">Kamikaze Games</option><option value="369">Keep Exploring Games</option><option value="300">Keymaster Games</option><option value="232">Kick Off Games SL</option><option value="239">Kids Table Board Gaming</option><option value="371">Killer Robot Games</option><option value="6">KMC</option><option value="3">Konami</option><option value="80">Koplow Games</option><option value="296">Korona Games</option><option value="279">Kosmos</option><option value="23">Kotobukiya</option><option value="227">Lark &amp; Clam</option><option value="314">Leder Games</option><option value="68">Legion Supplies</option><option value="332">Letiman Games</option><option value="217">Level 99 Games</option><option value="124">Libellud</option><option value="152">Lookout Games</option><option value="95">Looney Labs</option><option value="202">Looping Games</option><option value="351">Lucky Duck Games</option><option value="375">Lucky Loser</option><option value="134">Ludically</option><option value="146">LudiCreations</option><option value="126">Ludonaute</option><option value="365">Ludus Magnus Studio</option><option value="372">Lui-m&ecirc;me</option><option value="334">Madara</option><option value="40">Mage Company</option><option value="195">Magellan Games</option><option value="83">Mantic Games</option><option value="338">Mariucci J. Designs</option><option value="247">Marrow Productions</option><option value="155">Matagot</option><option value="29">Mayfair Games</option><option value="88">McFarlane Toys</option><option value="270">Medieval Lords</option><option value="61">Mercury Games</option><option value="138">Meridiano 6</option><option value="297">Micro Art Studio</option><option value="192">Mighty Boards</option><option value="190">Mindclash Games</option><option value="222">Minion Games</option><option value="273">Modiphius Entertainment</option><option value="185">Monolith</option><option value="168">Mont Taber Edicions</option><option value="104">Monte Cook Games</option><option value="294">Morning</option><option value="260">Moroz Publishing</option><option value="20">NECA</option><option value="383">New Experience Workshop</option><option value="347">Nifty Games</option><option value="304">Ninja Division Publishing</option><option value="4">Nintendo</option><option value="269">Ninth Level Games</option><option value="105">Nocturnal Media</option><option value="59">NSKN Games</option><option value="289">ODD Bird Games</option><option value="305">Orange Machine Games</option><option value="165">Osprey Games</option><option value="188">Overworld Games</option><option value="70">Paizo Publishing</option><option value="256">Pandasaurus Games</option><option value="12">Panini</option><option value="261">Para Bellum Wargames</option><option value="89">Passport Game Studios</option><option value="215">PD-Verlag</option><option value="123">Pearl Games</option><option value="290">Pencil First Games</option><option value="91">Pendragon</option><option value="262">Petersen Games</option><option value="211">Phalanx</option><option value="194">PieceKeeper Games</option><option value="226">Pink Monkey Games</option><option value="45">Plaid Hat Games</option><option value="210">Plan B Games</option><option value="349">Play Nation</option><option value="316">Playford Games</option><option value="135">Playmore Games Inc.</option><option value="346">Pleasant Company Games</option><option value="140">Polish Publishing League</option><option value="285">POPbuddies</option><option value="98">Portal Games</option><option value="201">Prodos Games</option><option value="248">Prometheus Games</option><option value="182">PSC Games</option><option value="171">Queen Games</option><option value="288">Quined Games</option><option value="275">Quixotic Games</option><option value="67">Ravensburger</option><option value="153">Rebel.pl</option><option value="56">Red Raven Games</option><option value="370">Redimp Games</option><option value="115">Redshift Games</option><option value="137">Renegade Game Studios</option><option value="131">Repos Production</option><option value="249">Restoration Games</option><option value="42">Rio Grande Games</option><option value="336">Road to Infamy Games</option><option value="243">Rock Manor Games</option><option value="366">Room 17 Games</option><option value="230">Roubloff</option><option value="75">Roxley Games</option><option value="34">Royal Bobbles</option><option value="283">Rudy Games</option><option value="176">Sakami Merchandise</option><option value="341">Shawnsolo Games</option><option value="328">Shoot Again Games</option><option value="19">Sideshow Collectibles</option><option value="189">Signal Fire Games</option><option value="331">Silverclutch Games</option><option value="224">Sinister Fish Games</option><option value="342">Sirlin Games</option><option value="223">SlugFest Games</option><option value="221">Smirk &amp; Dagger Games</option><option value="357">Soaring Rhino</option><option value="125">Space Cowboys</option><option value="295">Spaceballoon Games</option><option value="337">Sparkworks</option><option value="382">Sphere Games</option><option value="46">Spielworxx</option><option value="280">Splotter Spellen</option><option value="321">Starling Games</option><option value="43">Steve Jackson Games</option><option value="178">Stone Blade Entertainment</option><option value="112">Stonemaier Games</option><option value="214">Strawberry Studio</option><option value="73">Stronghold Games</option><option value="306">Studio 9 Games</option><option value="87">StuntKite Publishing</option><option value="345">Talon Strikes Studios</option><option value="62">Tasty Minstrel Games</option><option value="380">Tau Leader Games</option><option value="255">Tea Time Productions</option><option value="301">The Hut Group</option><option value="100">The Spoils TCG</option><option value="218">Thunderworks</option><option value="117">Tiltfactor</option><option value="241">Titan Forge Games</option><option value="76">TokensForMTG.com</option><option value="9">Topps</option><option value="292">Toylaxy Studio</option><option value="44">Twilight Creations, Inc.</option><option value="361">Txarli Factory</option><option value="344">Tyto Games</option><option value="2">Ultra Pro</option><option value="133">Underground Toys</option><option value="55">Upper Deck</option><option value="127">USAopoly</option><option value="122">Vampire Squid Cards</option><option value="307">Van Ryder Games</option><option value="207">Vedra Games</option><option value="183">Vesuvius Media</option><option value="257">Vice Games</option><option value="322">Victory Point Games</option><option value="276">Vile Genius Games</option><option value="264">Viravi Edicions</option><option value="354">Warcradle Studios</option><option value="236">Warlord Games</option><option value="114">WeaselPants Productions</option><option value="308">Weird City Games</option><option value="266">Weta Workshop</option><option value="65">White Wizard Games</option><option value="330">Wild East Game Company</option><option value="142">WildFire LLC</option><option value="32">Winning Moves</option><option value="1">Wizards of the Coast</option><option value="16">WizKids/NECA</option><option value="197">Wolff Designa</option><option value="353">Wonderdice</option><option value="368">Wyvern Games</option><option value="324">Xtronaut Enterprises</option><option value="132">Ystari Games</option><option value="64">Z-Man Games</option><option value="271">Zafty Games</option><option value="167">ZAS Play</option><option value="244">Zuru</option>                </select>
              </form>
              <br /><br />
              <form method="get" action="list.php">
                <select name="license" onchange="this.form.submit();">
                  <option value="">Select a License</option>
                  <option value="191">1A Games</option><option value="171">Abacus Spiele</option><option value="151">Academy Games</option><option value="174">ADC Blackfire Entertainment GmbH</option><option value="28">Adrenalyn XL</option><option value="71">Adventure Time</option><option value="283">Agricola</option><option value="152">Alderac Entertainment</option><option value="57">Aliens</option><option value="294">Antsy Labs</option><option value="18">Arcane Tinmen</option><option value="277">Arcane Wonders</option><option value="213">Argentum Verlag</option><option value="286">Artipia Games</option><option value="251">Ascension</option><option value="203">Asmodee</option><option value="82">Assassin&#039;s Creed</option><option value="88">Attack On Titan</option><option value="34">Attack Wing</option><option value="10">Avalon Hill</option><option value="7">Axis &amp; Allies</option><option value="147">Back To The Future</option><option value="278">BattleTech</option><option value="27">BCW Supplies</option><option value="77">Big Lebowski</option><option value="89">Bioshock</option><option value="106">Bishoujo</option><option value="298">Blade Runner</option><option value="300">Bloodborne</option><option value="329">Borangame</option><option value="182">Borderlands</option><option value="143">Boss Monster</option><option value="117">Breaking Bad</option><option value="341">Breaking Games</option><option value="19">Bushiroad</option><option value="220">Calliope Games</option><option value="9">Cardfight!! Vanguard</option><option value="192">Catalyst Game Labs</option><option value="333">Cephalofair Games</option><option value="261">Cheapass Games</option><option value="47">Cityscape</option><option value="263">Colt Express</option><option value="322">Compass Games</option><option value="250">Conan</option><option value="208">Cool Mini Or Not</option><option value="238">Counter Strike</option><option value="144">Cryptozoic</option><option value="178">Czech Board Games</option><option value="309">Daily Magic Games</option><option value="234">Darksiders</option><option value="38">Days of Wonder</option><option value="51">DC Comics</option><option value="339">Destiny</option><option value="140">Diablo</option><option value="253">Dishonored</option><option value="50">Disney</option><option value="188">Doctor Who</option><option value="232">Doom</option><option value="296">Dragoborne</option><option value="295">Dragon Age</option><option value="138">Dragon Ball</option><option value="343">Dragon Dawn Productions</option><option value="17">Dragon Shield</option><option value="8">Dungeons &amp; Dragons</option><option value="243">Dying Light</option><option value="264">Dyskami Publishing</option><option value="125">E.T.</option><option value="353">Eagle-Gryphon Games</option><option value="270">Elder Scrolls</option><option value="346">Erika Svanoe Games</option><option value="59">Evil Dead</option><option value="168">Evolve</option><option value="345">Facade Games</option><option value="231">Fallout</option><option value="181">Family Guy</option><option value="100">Fantasy Flight Games</option><option value="274">Feuerland Spiele</option><option value="98">Films &amp; Music</option><option value="64">Firefly</option><option value="161">Fireside Games</option><option value="246">Flames of War</option><option value="268">FloodGate Games</option><option value="30">Force of Will</option><option value="74">Friday The 13th</option><option value="201">Funforge</option><option value="46">Funko</option><option value="173">Futurama</option><option value="26">Future Card Buddyfight</option><option value="169">G3 Games</option><option value="331">Galakta Games</option><option value="237">Gale Force Nine</option><option value="316">Game Brewer</option><option value="29">Game of Thrones</option><option value="323">Gamelyn Games</option><option value="332">Gamestart Edizioni</option><option value="290">Gaya Entertainment</option><option value="239">GDM Games</option><option value="116">Gentle Giant</option><option value="91">Ghostbusters</option><option value="249">Giochix</option><option value="146">Godzilla</option><option value="195">Green Ronin Publishing</option><option value="257">GreenBrier Games</option><option value="72">Gremlins</option><option value="265">Grey Fox Games</option><option value="31">Guardians of the Galaxy</option><option value="75">Halloween</option><option value="121">Halo</option><option value="103">Hanna Barbera</option><option value="66">Harry Potter</option><option value="186">HCD Game Supplies</option><option value="272">Hero Realms</option><option value="233">Hitman</option><option value="53">Hobby World</option><option value="301">Horizon Zero Dawn</option><option value="127">Hutter</option><option value="149">IDW Games</option><option value="167">Iello</option><option value="60">Iron Maiden</option><option value="69">Iron Man</option><option value="222">Japanime Games</option><option value="124">Jeff Dunham</option><option value="105">John Avon Art</option><option value="189">Jolly Roger Games</option><option value="344">Jon Sudbury Games</option><option value="299">KakapopoTCG</option><option value="337">Keep Exploring Games</option><option value="108">Kick Ass</option><option value="16">KMC</option><option value="176">Koplow Games</option><option value="318">Kosmos</option><option value="96">Kotobukiya</option><option value="65">Krosmaster</option><option value="351">Leder Games</option><option value="210">Legend of Zelda</option><option value="260">Legendary Encounters</option><option value="185">Legion Supplies</option><option value="315">Lego</option><option value="209">Libellud</option><option value="302">Little Big Planet</option><option value="221">Lookout Games</option><option value="206">Looney Labs</option><option value="95">Lord of the Rings</option><option value="200">Luck &amp; Logic</option><option value="255">Ludicreations</option><option value="262">Ludonaute</option><option value="336">Ludus Magnus Studio</option><option value="242">Machi Koro</option><option value="245">Mafia</option><option value="184">Mage Knight</option><option value="52">Mage Wars</option><option value="269">Magellan Games</option><option value="2">Magic: The Gathering</option><option value="180">Mantic Games</option><option value="33">Marvel</option><option value="84">Mass Effect</option><option value="23">Match Attax</option><option value="101">Mayfair Games</option><option value="154">Megaman</option><option value="40">Memoir &#039;44</option><option value="324">Mickey Mouse</option><option value="326">Modiphius Entertainment</option><option value="193">Monte Cook Games</option><option value="102">Munchkin</option><option value="13">My Little Pony</option><option value="61">NECA</option><option value="67">NECA Cult Classics</option><option value="350">New Experience Workshop</option><option value="79">Nightmare Before Christmas</option><option value="78">Nightmare on Elm Street</option><option value="335">Ninja Division Publishing</option><option value="202">NSKN Games</option><option value="271">Ori and the Blind Forest</option><option value="240">Osprey Games</option><option value="225">Overwatch</option><option value="256">Overworld Games</option><option value="90">Pacific Rim</option><option value="319">Panini</option><option value="308">Paradox Interactive</option><option value="15">Pathfinder</option><option value="227">Payday</option><option value="287">PD-Verlag</option><option value="267">Pearl Games</option><option value="207">Plaid Hat Games</option><option value="3">Pokemon</option><option value="216">Polish Publishing League</option><option value="236">Portal</option><option value="199">Portal Games</option><option value="44">Predator</option><option value="258">PSC Games</option><option value="92">Pulp Fiction</option><option value="172">Quarriors</option><option value="224">Queen Games</option><option value="349">Quined Games</option><option value="214">Ravensburger</option><option value="150">Red Raven Games</option><option value="338">Redimp Games</option><option value="334">Renegade Game Studios</option><option value="211">Repos Production</option><option value="132">Resident Evil</option><option value="73">Robocop</option><option value="126">Rocky</option><option value="293">Roubloff</option><option value="123">Royal Bobbles</option><option value="204">Runebound</option><option value="48">Scarface</option><option value="212">Scooby Doo</option><option value="183">Sesame Street</option><option value="42">Shadows over Camelot</option><option value="120">Sideshow</option><option value="297">Silent Hill</option><option value="230">Skyrim</option><option value="41">Small World</option><option value="141">Sons Of Anarchy</option><option value="218">Space Cowboys</option><option value="54">Spartacus</option><option value="142">Spielworxx</option><option value="158">Star Realms</option><option value="36">Star Trek</option><option value="58">Star Wars</option><option value="285">Starbound</option><option value="291">Starcraft</option><option value="284">Stardew Valley</option><option value="312">Starfinder</option><option value="325">Starling Games</option><option value="166">Steve Jackson Games</option><option value="340">Stonemaier Games</option><option value="134">Street Fighter</option><option value="162">Stronghold Games</option><option value="281">Super Mario</option><option value="131">Superman</option><option value="219">Tanks</option><option value="198">Tanto Cuore</option><option value="165">Tasty Minstrel Games</option><option value="348">Tau Leader Games</option><option value="177">Teenage Mutant Ninja Turtles</option><option value="63">Terminator</option><option value="317">Test of Honour</option><option value="76">Texas Chainsaw Massacre</option><option value="217">The Army Painter</option><option value="81">The Hobbit</option><option value="25">The Spoils</option><option value="244">The Thalos Principle</option><option value="49">The Walking Dead</option><option value="39">Ticket to Ride</option><option value="163">TokensForMTG.com</option><option value="20">Topps</option><option value="43">Transformers</option><option value="118">True Blood</option><option value="115">Twilight</option><option value="5">Ultra Pro</option><option value="226">Uncharted</option><option value="179">Universal Fighting System</option><option value="292">Upper Deck</option><option value="330">USAopoly</option><option value="321">Vampire: The Masquerade</option><option value="130">Various</option><option value="311">Vice Games</option><option value="342">Victory Point Games</option><option value="259">VS System</option><option value="113">Walking Dead</option><option value="273">Warage</option><option value="328">Warcradle Studios</option><option value="139">Warhammer</option><option value="310">Warlord Games</option><option value="279">Watch Dogs</option><option value="22">Wei&szlig; / Schwarz</option><option value="320">Wizards of the Coast</option><option value="289">WizKids/NECA</option><option value="12">World of Warcraft</option><option value="282">WWE</option><option value="4">Yu-Gi-Oh!</option><option value="156">Z-Man Games</option>                </select>
              </form>
            </div>
          </li>
          <li style="background: #555;"><a href="#">Our Games!</a>
            <div>
              <ul>
                
        <li><a href="games.php?game=calimala">Calimala (DE/EN)</a></li>
        <li><a href="games.php?game=crystaltwister">Crystal Twister Dice Tower</a></li>
        <li><a href="games.php?game=cthulhurealms">Cthulhu Realms (DE)</a></li>
        <li><a href="games.php?game=euphoria">Euphoria (DE)</a></li>				<li><a href="games.php?game=herorealms">Hero Realms (DE)</a></li>
        <li><a href="games.php?game=kraftwagen">Kraftwagen (DE/EN)</a></li>				<li><a href="games.php?game=no-siesta">No Siesta (DE/EN)</a></li>
        <li><a href="games.php?game=portal">Portal (DE/CZ)</a></li>
        <li><a href="games.php?game=shuffleheroes">Shuffle Heroes (DE)</a></li>
        <li><a href="games.php?game=solafide">Sola Fide (DE)</a></li>
        <li><a href="games.php?game=starrealms">Star Realms (DE)</a></li>
        <li><a href="games.php?game=kingswill">The King's Will (DE/EN)</a></li>
        <li><a href="games.php?game=vikings">Vikings on Board (DE/CZ)</a></li>        		<li><a href="games.php?game=villageattacks">Village Attacks (DE/EN)</a></li>				<li><a href="games.php?game=kriegderknoepfe">War of the Buttons (DE/EN/FR)</a></li>
        <li><a href="games.php?game=westofafrica">West of Africa (DE/EN)</a></li>              </ul>
            </div>
          </li>
          <li><a href="account.php">My Account</a>
            <div>
              <ul>
                
            <li><a href="account.php?act=register"><strong>Register now!</strong></a></li>
            <li><a href="account.php?act=login">Login</a></li>
            <li><a href="account.php?act=resetpw">Reset Password</a></li>              </ul>
            </div>
          </li>
          <li><a href="info.php">Information</a>
            <div>
              <ul>
                
        <li><a href="info.php?txt=contact">Contact Us</a></li>
        <li><a href="info.php?txt=shipping">Shipping Conditions</a></li>
        <li><a href="info.php?txt=payment">Payment Conditions</a></li>
        <li><a href="info.php?txt=organized_play">Organized Play</a></li>
        <li><a href="info.php?txt=jobs">Jobs</a></li>
        <li><a href="info.php?txt=faq">Frequently Asked Questions</a></li>
        <li><a href="info.php?txt=privacy">Privacy Policy</a></li>
        <li><a href="info.php?txt=terms">Terms & Conditions</a></li>
        <li><a href="info.php?txt=sitemap">Site Map</a></li>
        <li><a href="info.php?txt=rss">Subscribe to our RSS-Feed</a></li>              </ul>
            </div>
          </li>
        </ul>
      </nav>

    </header>

  <div id="container">
    <!--Left Part-->
    <div id="column-left">
      <div class="box">
        <div class="box-heading">Information</div>
        <div class="box-content">
          <ul class="list-item">
            
        <li><a href="info.php?txt=contact">Contact Us</a></li>
        <li><a href="info.php?txt=shipping">Shipping Conditions</a></li>
        <li><a href="info.php?txt=payment">Payment Conditions</a></li>
        <li><a href="info.php?txt=organized_play">Organized Play</a></li>
        <li><a href="info.php?txt=jobs">Jobs</a></li>
        <li><a href="info.php?txt=faq">Frequently Asked Questions</a></li>
        <li><a href="info.php?txt=privacy">Privacy Policy</a></li>
        <li><a href="info.php?txt=terms">Terms & Conditions</a></li>
        <li><a href="info.php?txt=sitemap">Site Map</a></li>
        <li><a href="info.php?txt=rss">Subscribe to our RSS-Feed</a></li>          </ul>
        </div>
      </div>
    </div>
    <!--Left End-->
    <!--Middle Part Start-->

    <div id="content">
      <h1>Data Protection Information</h1>
      <h1>ADC Blackfire Entertainment GmbH</h1>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b style="mso-bidi-font-weight:normal">&nbsp;</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>The privacy of your data&nbsp;is very important to us!</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We would like to inform you below about the processing of personal data within the context of the use of our Internet pages. This data protection information informs you about the type, scope and purpose of the processing of personal data in conjunction with our online services and the associated websites, functions and content, as well as external online sites such as our social media profiles.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">With regard to the terms used, such as &quot;processing&quot; or &quot;Data Controller&quot;, we refer to the definitions in Art. 4 of the General Data Protection Regulation (GDPR).</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Data Controller </b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">The <b>Data Controller</b> - according to Art. 4, para. 7 of the EU General Data Protection Regulation GDPR - <b>for these Internet pages is:</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">ADC Blackfire Entertainment GmbH</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Harkortstrasse 34</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">40880 Ratingen, Germany</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Further information about our company can be found in our Publishing Information.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Link to the publishing information:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b><a href="http://www.blackfire.eu/info.php?txt=contact">http://www.blackfire.eu/info.php?txt=contact</a></b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Which data are processed?</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>General</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">When you visit our website, personal data are also processed.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">In order for the pages to be displayed in your browser, the IP address of the terminal device you are using needs to be processed. Further information about the browser of your terminal device is also required.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We are also obliged under data protection law to guarantee the confidentiality and integrity of the personal data which are processed with our IT systems.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>The purposes for which we collect data (purpose of the processing) and the legal basis for this</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We process the above-mentioned personal data in accordance with the provisions of the General Data Protection Regulation (&quot;GDPR&quot;) and the Federal Data Protection Act (&quot;BDSG&quot;).</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">a. For the fulfilment of contractual obligations (Art. 6 para. 1b GDPR)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">This includes, for example, the provision of insurance services or the implementation of pre-contractual measures, in particular risk assessment. The submission and processing of claims upon the occurrence of an insured event and the review of the preconditions of the insured event, as well as for the charging and settlement of your brokerage fee claims, must also be listed here.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">b. On the basis of legal requirements (Art. 6 para. 1c GDPR) or in the public interest (Art. 6 para. 1e GDPR)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">As an underwriting agent, we are subject to various legal obligations (e.g. insurance contract law, tax laws etc.) as well as regulatory requirements (e.g. trade supervision, the Chamber of Commerce and Industry and the Federal Financial Supervisory Authority). The purposes of the processing include, for example, the obligation to keep records, the prevention of fraud and terrorism, as well as the review of sanctions.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">c. Within the framework of the balancing of interests (Art. 6, para. 1f GDPR)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We partly process data which are not necessary for the fulfilment of the contract in order to protect the legitimate interests of ourselves or third parties. This includes the assertion of legal claims and defense in the case of legal disputes, as well as the prevention of criminal offences.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">d. On the basis of consent (Art. 6, para. 1a GDPR)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">The processing of your personal data is also lawful if you have given your consent to this (e.g. site inspection, forwarding of data for the risk assessment etc.). You can revoke this consent at any time. However, this revocation then only applies to the future - the previous processing is not affected by it.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Information about the collection of personal data </b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We provide information below on the <b>collection of personal data</b> when you use our website. Personal data means individual pieces of information regarding personal or factual conditions of a specific or identifiable natural person (data subject), e.g. name, address, E-mail addresses, user behavior).</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>When we are contacted by you via E-mail</b> or via a contact form, the data you provide (your E-mail address and - if applicable - your name and telephone number) will be stored by us in order to answer your questions. We will delete the data collected within this context after their storage is no longer required, or otherwise limit their processing if retention obligations exist according to the law.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b style="mso-bidi-font-weight:normal">&nbsp;</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Your Rights</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">You have the following rights toward us with regard to the personal data relating to you:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; right to information,</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; right to correction or deletion,</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; right to the restriction of processing,</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; right to object to their processing,</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; right to withdraw consent,</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; right to data portability.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">You also have the right to complain to a data protection supervisory authority about the processing of your personal data by us.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Personal data collection when visiting our website</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">If you only wish to use our website <b>for information purposes</b>, i.e. if you do not register or otherwise provide us with information, only the personal data transmitted by your browser to our server will be collected. If you wish to view our website, we collect the following data, which are technically necessary for us to display our website to you and guarantee its stability and security (the legal basis is Art. 6, para. 1, sentence 1 (f) of the General Data Protection Regulation (GDPR):</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; IP address</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; date and time of the query</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; time zone difference compared to Greenwich Mean Time (GMT)&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; contents of the request (specific page)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; access status/HTTP status code</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; amount of data transmitted in each case</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; the website from which the request comes</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; browser</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; operating system and its interface</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&mdash; language and version of the browser software.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">In addition to the aforementioned data, cookies are stored on your computer when you use our website. Cookies are small text files that are stored on your hard disk and associated with the browser you are using and by means of which the party which sets the cookie (in this case us) receives certain information. Cookies cannot run programs or transmit viruses to your computer. They serve to make our site more user-friendly and effective.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(3)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Use of cookies:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">a) This website uses the following types of cookies, the scope and functions of which are explained below:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Duty to provide information when collecting personal data</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Transient cookies (see b)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Persistent cookies (see c).</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">b) Transient cookies are automatically deleted when you close the browser. These include session cookies in particular. These store a so-called session ID, with which various requests from your browser can be assigned to the joint session. This allows your computer to be recognised when you return to the site. Session cookies are deleted when you log out or close the browser.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">c) Persistent cookies are automatically deleted after a specified period, which may differ depending on the cookie. You can delete cookies at any time in the security settings of your browser.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">d) You can configure your browser settings in accordance with your wishes and refuse to accept third-party cookies or any cookies, for example. However, please note that if you do so, you may not be able to use all the functions of this website.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">[(e) We use cookies in order to be able to identify you for subsequent visits if you have an account with us. Otherwise you would have to log in again for each visit.]</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">[(f) The Flash cookies used are not recorded by your browser, but by your Flash plug-in. We also use HTML5 storage objects, which are stored on the end device. These objects store the required data regardless of the browser you are using and do not have an automatic expiration date. If you do not want the Flash cookies to be processed, you must install a corresponding add-on, for example &quot;Better Privacy&quot; for Mozilla Firefox (<a href="https://addons.mozilla.org/de/firefox/addon/betterprivacy/">https://addons.mozilla.org/de/firefox/addon/betterprivacy/</a>) or the AdobeFlash Killer Cookie for Google Chrome. You can prevent the use of HTML5 storage objects by using private mode in your browser. In addition, we recommend that you regularly delete your cookies and browser history manually].</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Further functions and offers of our website</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">In addition to the purely informational use of our website, we offer various services that you can use if interested. For this purpose, you must provide further personal data which we use to provide the respective service and to which the aforementioned data processing principles apply.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">In some cases, we use external service providers to process your data. These have been carefully selected and commissioned by us, are bound by our instructions and are regularly checked.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(3)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">If our service providers or partners are located in a country outside the European Economic Area (EEA), we will inform you of the consequences of this circumstance in the description of the offer.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Objection to or revocation of the processing of your data</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>If you have given your consent to the processing of your data, you can revoke this at any time.</b> Such revocation affects the admissibility of the processing of your personal data after you have submitted it to us.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">You have the right to object to the processing, insofar as we base the processing of your personal data on the balancing of interests. This is the case if the processing is not necessary in particular for the fulfilment of a contract with you, as stated by us in each case with the following description of the functions. Should you submit an objection, we ask you to explain the reasons why we should not process your personal data in the manner that we have. In the case of your justified objection, we will examine the circumstances and either discontinue or adjust the data processing or inform you of our compelling reasons which are worthy of protection and on the basis of which we are continuing the processing; if the processing does not serve the purpose of asserting, exercising or defending against legal claims.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(3)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">You can of course object to the processing of your personal data for advertising and data analysis purposes at any time.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">You can inform us of your objection to advertising at the following address: <a href="mailto:info@blackfire.eu"><b>info@blackfire.eu</b></a><b> </b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Business-related processing</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">In addition, we process</p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:14.2pt;line-height:normal;tab-stops:1.0cm">-&nbsp;&nbsp;&nbsp;&nbsp; contract data (for example, subject matter of the contract, validity period, customer category)</p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:6.0pt;
margin-left:14.2pt;line-height:normal;tab-stops:1.0cm">-&nbsp;&nbsp;&nbsp;&nbsp; payment data (for example, bank details, payment history)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">of our customers, potential customers and business partners for the purpose of providing contractual and other services, customer care, marketing, advertising and market research.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Special forms of use of websites</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Use of our online shop</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">If you want to place an order in our web shop, we need personal data to process the order. The required mandatory fields are marked, but all other fields are optional. We use the data provided by you to process your order. For this purpose, we can pass your payment data on to our principal bank. The legal basis for this is Art. 6 Para. 1 sentence 1(b) GDPR.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">A customer account is created for you, with which we store the data you provide for subsequent purchases. These data are stored with revocable effect when the account is opened and can be deleted at any time if you ask us to do so.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We may also process the information you provide in order to inform you about other interesting products from our portfolio or send you technical information by E-mail.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We are obliged by commercial and tax law to store your address, payment, and order data for a period of ten years.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(3)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">In order to prevent unauthorized access by third parties to your personal data, in particular financial data, the ordering process is encrypted using TLS technology.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Establishment of contacts</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">When you contact us (e.g. via a contact form or E-mail), we process the data provided by you in order to handle the inquiry or in the event that follow-up queries are submitted. If the data processing is carried out in conjunction with contractual or pre-contractual relationships, the legal basis for this data processing is Art. 6, para. 1, clause 1 b of the GDPR. We will only process further personal data with your consent (Art. 6, para. 1, clause 1 a) of the GDPR) or if we have a legitimate interest in the processing of your data (Art. 6, para. 1, clause 1 f) of the GDPR). A legitimate interest would be, for example, to respond to your inquiry.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Newsletter</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We send newsletters, E-mails and other electronic notifications with advertising information only with the consent of the recipient. The declaration of consent lists the goods and services advertised.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">For the dispatch of the newsletter it is only necessary to provide your E-mail address. All other information is voluntary and will be used to personalize the newsletter. After your registration we also store your E-mail address for the purpose of sending you the newsletter. The legal basis for this is Art. 6 para. 1 as of the GDPR.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(3)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">You can revoke your consent to the dispatch of the newsletter at any time and cancel the newsletter. You can declare your revocation by sending an E-mail to <a href="mailto:info@blackfire.eu"><b>info@blackfire.eu</b></a> or a message to the contact address stated in the publishing information.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Registration function</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Users can optionally create a user account. During the registration process, the necessary mandatory information is provided to the users. The data entered as part of the registration process is used for the purpose of taking advantage of the offers available. Users may be informed by E-mail of information which is relevant to the offer or registration, such as changes to the scope of the offer or technical circumstances. If users have terminated their user account, their data will be deleted with respect to the active use of the user account, subject to the proviso that their retention is required under commercial or fiscal law pursuant to <i>Art. 6, para. 1 c) of the GDPR</i>. It is the responsibility of the users to secure their data before the end of the contract in the case of termination. We are entitled to irretrievably delete all user data stored during the term of the contract.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We store the IP address and the time of the respective user activity when the user avails himself/herself of our registration and login functions and during the use of the user account. The storage referred to above is based on our legitimate interests, as well as the user&#39;s interests in being protected against abuse and other unauthorized use. These data will not be passed on to third parties under any circumstances unless this is necessary to pursue our claims or there is a legal obligation to do so in accordance with<i> Art. 6, para. 1 f of the GDPR.</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">The IP addresses are anonymized or deleted after 7 days at the latest.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b style="mso-bidi-font-weight:normal">&nbsp;</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Data protection for applications and in the application process</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">The Data Controller collects and processes the personal data of applicants for the purpose of processing the application procedure. Processing may also be carried out electronically. This is particularly the case if an applicant sends corresponding application documents to the Data Controller by electronic means, for example by E-mail or via a web form on the website. If the Data Controller concludes an employment contract with an applicant, the data transferred will be stored for the purposes of processing the employment relationship in compliance with the statutory provisions. If the Data Controller does not conclude an employment contract with the applicant, the application documents are automatically deleted within a period of one month after notification of the decision of refusal, provided that no other legitimate interests of the Data Controller stand in the way of such erasure. Another legitimate interest in this sense is, for example, a burden of proof in proceedings under the German General Equal Treatment Act (AGG).</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Hosting</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">The hosting services used by us serve the purpose of providing the following services: infrastructure and platform services, computing capacity, storage space and database services, security services and technical maintenance services which we use for the purpose of operating this online service.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">For the above, we and our hosting provider process inventory data, contact data, content data, contract data, usage data, meta- and communication data of customers, interested parties and visitors to this online content on the basis of our legitimate interests in the efficient and reliable provision of this online content <i>according to Art. 6 para. 1 f of the GDPR in conjunction with Art. 28 GDPR (conclusion of an order processing agreement).</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Collection of access data and log files</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We, or our hosting provider, collect data on each access to the server on which this service is located (so-called server log files) on the basis of our legitimate interests within the meaning of <i>Art. 6, para. 1 f. of the GDPR</i>. The access data includes the name of the website accessed, the file, date and time of access, the amount of data transferred, notification of successful access, the browser type and version, the user&#39;s operating system, the referrer URL (the previously visited page), the IP address and the requesting provider.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Log file information is stored for security reasons (e.g. for the clarification of abuse or fraudulent activities) for the maximum duration of 7 days and then deleted. Data whose further storage is required for evidentiary purposes is excluded from the deletion process until the respective incident has been finally clarified.&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b style="mso-bidi-font-weight:normal">&nbsp;</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Performance of contractual services</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We process inventory data (e.g. the names and addresses as well as contact data of users), contract data (e.g. the services used, the names of contact persons, payment information) for the purpose of fulfilling our contractual obligations and services <i>in accordance with Art. 6 para. 1 b GDPR.</i> The entries marked as mandatory in online forms are required for the conclusion of the contract.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">When our online services are used, we store the IP address and the time of the respective user action. The storage referred to above is based on our legitimate interests, as well as the user&#39;s interests in being protected against abuse and other unauthorized use. These data will not be passed on to third parties unless this is necessary to pursue our claims or there is a legal obligation to do so <i>in accordance with Art. 6 para. 1 c GDPR.</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">The data are deleted after the expiry of legal warranty and comparable obligations and the necessity of data storage has been checked; in the case of legal archiving obligations, the deletion is carried out after these obligations have expired. Information contained in any customer account is retained until its deletion.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Use of social media plug-ins</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We use the following social media plug-ins on our site:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Facebook, Twitter, Instagram</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We use the two-click solution for this. When you visit our site, no personal data are initially passed on to the plug-in providers. Only if you click on the corresponding button of the provider on our website will the information that you have called up the corresponding website of our online service be transmitted to the provider.&nbsp; In addition, the data referred to in Art. 3 is transmitted to the provider. In the case of Facebook and Xing, your IP address is anonymized immediately after collection in accordance with the details submitted by the respective provider in Germany. When the respective button of the provider is clicked, personal data are therefore transmitted to the provider and stored there. We advise you to delete all your cookies before clicking on the button, as the plug-in provider collects the data mainly via cookies.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We cannot influence the data processing procedures or the data that are collected. We are not aware of the full scope of the data collection, the purposes of its processing or the storage periods. We also have no information on the deletion of the data collected by the plug-in provider.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(3)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">The data collected about you are stored by the plug-in provider as usage profiles. These are used for the purposes of advertising, market research and/or the needs-based design of its website. You have the right to object to the creation of these user profiles; in order to do so you have to contact the provider of the respective plug-in. The purpose of the plug-ins is to improve our offering and your user experience by enabling you to interact with the social networks and other users through the plug-ins. The legal basis for the use of the plug-ins is Art. 6, para. 1, clause 1 f of the GDPR.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(4)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">The data are passed on regardless of whether you have an account with the plug-in provider and are logged in there. If you are logged in, the data collected by us are directly assigned to your account with the plug-in provider. We therefore recommend that you log out regularly after using social networks, especially before activating the button, in order to prevent the direct assignment of your profile to the plug-in provider.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(5)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">For further information about the purpose and extent of the data collection and their processing by the plug-in provider, please see the data protection declarations of these providers which are referred to below. There you will also find further information on your rights in this respect and settings options for protecting your privacy.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(6)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Addresses of the respective plug-in providers and URL with their data protection information:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">a) [Facebook Inc., 1601 California Ave, Palo Alto, California 94304, USA; <a href="http://www.facebook.com/policy.php">http://www.facebook.com/policy.php</a> ; further information on data collection: <a href="http://www.facebook.com/help/186325668085084">http://www.facebook.com/help/186325668085084</a> , <a href="http://www.facebook.com/about/privacy/your-info-on-other#applications">http://www.facebook.com/about/privacy/your-info-on-other#applications</a>&nbsp; and <a href="http://www.facebook.com/about/privacy/your-info#everyoneinfo">http://www.facebook.com/about/privacy/your-info#everyoneinfo</a>. Facebook has committed itself to the EU-US Privacy Shield, <a href="https://www.privacyshield.gov/EU-US-Framework">https://www.privacyshield.gov/EU-US-Framework</a>.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">b) Twitter Inc., 1355 Market St, Suite 900, San Francisco, California 94103, USA; <a href="https://www.twitter.com/privacy">https://www.twitter.com/privacy</a>. Twitter has committed itself to the EU-US Privacy Shield, <a href="https://www.privacyshield.gov/EU-US-Framework">https://www.privacyshield.gov/EU-US-Framework</a>.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">c) Instagram Inc., 200 Jefferson Dr, Menlo Park, CA 94025, USA;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><a href="https://help.instagram.com/519522125107875">https://help.instagram.com/519522125107875</a>. Instagram has committed itself to the EU-US Privacy Shield, <a href="https://www.privacyshield.gov/EU-US-Framework">https://www.privacyshield.gov/EU-US-Framework</a>.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>&nbsp;</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Integration of YouTube Videos</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(1)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We have integrated YouTube videos into our website which are stored at http://www.YouTube.com and can be played directly from our website. [These are all integrated in &quot;Extended Privacy Mode&quot;, i.e. no data about you as a user are transferred to YouTube if you do not play the videos. Only when you play the videos are the data referred to in paragraph 2 transmitted. We have no influence on this data transmission.]</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">(2)</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">When you visit our website, the information that you have visited the corresponding page is transmitted to YouTube. In addition, the data referred to in Art. 3 of this declaration are transmitted. It does not matter whether you are logged in to a user account provided by YouTube or not. If you are logged in to Google, these data will be assigned directly to your account. If you want to prevent this assignment, you must log out before clicking on the button. YouTube creates a user profile with your data and uses them for advertising purposes, market research and/or for the need-based design of its website. You have the right to object to the creation of these user profiles, although in order to do so you have to contact YouTube directly.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">For more information on the purpose and scope of the data collection and their processing by YouTube, please refer to the privacy policy. There you will also find further information on your corresponding rights and settings options for protecting your privacy: <a href="https://www.google.de/intl/de/policies/privacy">https://www.google.de/intl/de/policies/privacy</a>.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Google also processes your personal data in the USA and has committed itself to the EU-US Privacy Shield, <a href="https://www.privacyshield.gov/EU-US-Famework">https://www.privacyshield.gov/EU-US-Famework</a>.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Collection of access data and log files</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We, or our hosting provider, collect data on each access to the server on which this service is located (so-called server log files) on the basis of our legitimate interests within the meaning of Art. 6, para. 1 f. of the GDPR. The access data include the name of the website accessed, the file, date and time of access, the amount of data transferred, notification of successful access, the browser type and version, the user&#39;s operating system, the referrer URL (the previously visited page), the IP address and the requesting provider.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Log file information is stored for security reasons (e.g. for the clarification of abuse or fraudulent activities) for the maximum duration of 7 days and then deleted. Data whose further storage is required for evidentiary purposes are excluded from the deletion process until the respective incident has been finally clarified.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Google Fonts</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">We integrate the fonts (&quot;Google Fonts&quot;) of the provider Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA. Privacy Policy: <a href="https://www.google.com/policies/privacy/">https://www.google.com/policies/privacy/</a>, opt-out: <a href="https://adssettings.google.com/authenticated">https://adssettings.google.com/authenticated</a>.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Google is certified under the Privacy Shield Agreement and thereby offers a guarantee to comply with European data protection law (https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&amp;status=Active (<a href="https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&amp;amp;amp;status=Active">https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&amp;status=Active</a>).</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Integration of third-party services and content</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Within the framework of our online offering &ndash; based on our legitimate interests (i.e. interest in the analysis, optimization and economic operation of our online offering within the meaning of <i>Art. 6, para. 1 f of the GDPR) &ndash;</i> we make use of the content or service offerings of third parties so that we can incorporate their content and services, such as videos or fonts (hereafter uniformly referred to as &quot;content&quot;).</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">This always assumes that the third-party providers of this content can see the IP address of the users, as without the IP address they would not be able to send the content to their browsers. The IP address is therefore required for the display of this content. We make every effort to use only such content whose respective providers use the IP address only for the delivery of the content. Third-party providers can also use so-called pixel tags (invisible graphics, also known as &quot;web beacons&quot;) for statistical or marketing purposes. Through these &quot;Pixel-Tags&quot;, information such as visitor traffic on the pages of this website can be evaluated. The pseudonymous information may also be stored in cookies on the user&#39;s device and may include &ndash; among other things &ndash; technical information about the browser and operating system, the referring websites, the visiting time and other information about the use of our online offering. It may also be linked to such information from other sources.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Consent </b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">If we process your personal data on the basis of consent that has been submitted, you have the right to revoke the consent at any time without affecting the legality of the processing carried out on the basis of the consent up to the time of revocation in accordance with <i>Art. 7, para. 3 of the GDPR.</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Right to lodge a complaint with a supervisory authority</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">According to <i>Art. 77 GDPR</i>, you have the right to lodge a complaint with the competent supervisory authority.</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">You can submit this, for example, to the following supervisory authority:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b><i>Bayrisches Landesamt f&uuml;r Datenschutzaufsicht (Bavarian State Office for Data Protection Supervision)</i></b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i>Promenade 27 (Schloss), 91522 Ansbach, Germany</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i>Website with further contact data: </i><a href="http://www.ida.bayern.de/"><i>http://www.ida.bayern.de</i></a></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i style="mso-bidi-font-style:normal">&nbsp;</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Der Hessische Datenschutzbeauftragte (The Hessian Data Protection Commissioner)</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Gustav-Stresemann-Ring 1, 65189 Wiesbaden, Germany</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i>Website with further contact data: </i><a href="http://www.datenschutz.hessen.de/"><i>http://www.datenschutz.hessen.de</i></a></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i style="mso-bidi-font-style:normal">&nbsp;</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b><i>Die Landesbeauftragte f&uuml;r den Datenschutz Niedersachsen (The State Commissioner for Data Protection in Lower Saxony)</i></b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i>Landschaftstrasse 5, 30159 Hannover, Germany</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i>Website with further contact details: </i><a href="http://www.lfd.niedersachsen.de/"><i>http://www.lfd.niedersachsen.de</i></a></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i style="mso-bidi-font-style:normal">&nbsp;</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Landesbeauftragte f&uuml;r Datenschutz und Informationsfreiheit Nordrhein-Westfalen (State Commissioner for Data Protection and Freedom of Information in North Rhine-Westphalia)</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Kavalleriestr. 2-4, 40213 D&uuml;sseldorf, Germany</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i>Website with further contact details: </i><a href="http://www.ldi.nrw.de/"><i>http://www.ldi.nrw.de</i></a></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i>&nbsp;</i></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>State Commissioner for Data Protection in Saxony-Anhalt (Landesbeauftragte f&uuml;r Datenschutz Sachsen-Anhalt)</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Leiterstr. 9, 39194 Magdeburg, Germany</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><i>Website with further contact details: </i><a href="https://datenschutz.sachsen-anhalt.de/">https://datenschutz.sachsen-anhalt.de</a></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">You can find a list of all supervisory authorities under the following link:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><a href="https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html">https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html</a></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal"><b>Data Protection Assignment</b></p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal"><b style="mso-bidi-font-weight:normal">&nbsp;</b></p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal"><b>We have a Data Protection Officer.</b></p>

<p class="MsoNormal" style="margin-bottom:3.0pt;text-indent:35.4pt;line-height:
normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal">We have set up an E-mail address for questions which are relevant to data protection and any reports of data protection infringements:</p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal"><b><a href="mailto:datenschutz@blackfire.eu">datenschutz@blackfire.eu</a></b></p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal"><b style="mso-bidi-font-weight:normal">&nbsp;</b></p>

<p class="MsoNormal" style="margin-bottom:3.0pt;text-indent:35.4pt;line-height:
normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal">or by mail to the address stated under the contact data of the Data Controller:</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><br style="mso-special-character:
line-break" />
&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">ADC Blackfire Entertainment GmbH</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b>Abteilung</b> <b>Datenschutz (Data Protection Department)</b></p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">Harkortstrasse 34</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal">40880 Ratingen, Germany</p>

<p class="MsoNormal" style="margin-bottom:6.0pt;line-height:normal"><b style="mso-bidi-font-weight:normal">&nbsp;</b></p>

<p class="MsoNormal" style="margin-top:0cm;margin-right:0cm;margin-bottom:3.0pt;
margin-left:35.4pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:3.0pt;line-height:normal"><b>Date: September 26, 2018</b></p>

<p>&nbsp;</p>    </div>
    <!--Middle Part End-->
    <div class="clear"></div>
  </div>

  </div>
  <!--Footer Part Start-->
    <footer id="footer" class="style-3">
      <div class="fpart-inner">
        <!-- Links Start-->
        <div class="box3_1">
          <div class="column">
            <h3>Information</h3>
            <ul>
              
        <li><a href="info.php?txt=contact">Contact Us</a></li>
        <li><a href="info.php?txt=shipping">Shipping Conditions</a></li>
        <li><a href="info.php?txt=payment">Payment Conditions</a></li>
        <li><a href="info.php?txt=organized_play">Organized Play</a></li>
        <li><a href="info.php?txt=jobs">Jobs</a></li>
        <li><a href="info.php?txt=faq">Frequently Asked Questions</a></li>
        <li><a href="info.php?txt=privacy">Privacy Policy</a></li>
        <li><a href="info.php?txt=terms">Terms & Conditions</a></li>
        <li><a href="info.php?txt=sitemap">Site Map</a></li>
        <li><a href="info.php?txt=rss">Subscribe to our RSS-Feed</a></li>            </ul>
          </div>
          <div class="column">
            <h3>My Account</h3>
            <ul>
              
            <li><a href="account.php?act=register"><strong>Register now!</strong></a></li>
            <li><a href="account.php?act=login">Login</a></li>
            <li><a href="account.php?act=resetpw">Reset Password</a></li>            </ul>
          </div>
        </div>
        <!-- Links End-->
        <!-- About Us Part Start-->
        <div class="box3" id="twitter_footer">
          
              <h3>About Us</h3>
              <p><strong>ADC Blackfire Entertainment GmbH (&quot;Blackfire.eu&quot;)</strong> is an European distributor selling popular Fantasy- and Trading Card Games (TCGs) like Magic: The Gathering, Yu-Gi-Oh!, Cardfight!! Vanguard, or Pokemon. We also offer Accessories from well-known producers like KMC Sleeves, Ultra Pro, or Dragon Shields.</p>

<p>Our target audience are resellers, who are searching for a supreme service, competitive prices, and a high quality standard. Benefit from our first-class service, our unrivalled price quotes, and the quick and trouble-free deliverance.</p>        </div>
        <div class="box3" id="facebook">
          
              <h3>Retailers Only</h3>
              <p>Please note that we can only sell to retailers or other distributors. We are not allowed to sell directly to end customers and are therefore strictly Business to Business (&quot;B2B&quot;).</p>

<p>If you are a retailer looking to expand your product-portfolio please register and after we have confirmed your commercial status we will grant you access to our prices and stock levels.</p>        </div>
        <div class="clear"></div>
        <div class="contact contact_icon">
          <h3>Contact Us</h3>
          <ul>
            <li class="address">ADC Blackfire Entertainment GmbH<br />Harkortstra&szlig;e 34<br />40880 / Ratingen || Germany</li>
            <li class="mobile"><a href="http://www.blackfire.eu/info.php?txt=contact" style="font-size: larger;">Contact Us</a></li>
            <li class="email"><a href="mailto:info@blackfire.eu" style="font-size: larger;">info@blackfire.eu</a></li>
          </ul>
        </div>
        <div class="back-to-top" id="back-top"><a title="Back to Top" href="javascript:void(0)" class="backtotop">Top</a></div>
      </div>
    </footer>
    <!--Footer Part End-->
  </div>
  </body>
  </html>
