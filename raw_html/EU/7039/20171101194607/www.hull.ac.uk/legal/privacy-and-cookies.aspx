<!DOCTYPE html>

  <!--[if lt IE 7]>      <html lang="en-gb" class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie6" lang="en-gb" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"> <![endif]-->
  <!--[if IE 7]>         <html lang="en-gb" class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-gb" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"> <![endif]-->
  <!--[if IE 8]>         <html lang="en-gb" class="no-js lt-ie9 lt-ie8" lang="en-gb" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"> <![endif]-->
  <!--[if IE 9]>         <html lang="en-gb" class="no-js lt-ie9" lang="en-gb" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"> <![endif]-->
  <!--[if gt IE 9]><!--> <html lang="en-gb" class="no-js" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"><!--<![endif]-->

  <head><meta http-equiv="X-UA-Compatible" content="IE=Edge" /><meta charset="utf-8"><title>
	Privacy and cookies | University of Hull
</title><meta name="theme-color" content="#00bcb5" />
    
    
    
    <meta name="GENERATOR" content="Contensis CMS Version 8.3" />
<meta name="description" content="Privacy Policy and cookie information at the University of Hull" />
<meta name="Keywords" content="privacy, policy, cookies, university, hull" />
<meta name="viewport" content="width=device-width, initial-scale=1" />




    <!-- Favicons -->
    <link rel="shortcut icon" href="/site-elements/img/favicon/favicon.ico" /><link rel="apple-touch-icon" href="/site-elements/img/favicon/favicon-57.png" /><link rel="apple-touch-icon" sizes="72x72" href="/site-elements/img/favicon/favicon-72.png" /><link rel="apple-touch-icon" sizes="114x114" href="/site-elements/img/favicon/favicon-114.png" /><link rel="apple-touch-icon" sizes="144x144" href="/site-elements/img/favicon/favicon-144.png" /><link rel="apple-touch-icon" sizes="180x180" href="/site-elements/img/favicon/favicon-180.png" /><link rel="apple-touch-icon" sizes="310x310" href="/site-elements/img/favicon/favicon-310.png" /><link href="/site-elements/css/style.css?version=19154" rel="stylesheet" type="text/css" />
<!--
ControlID:Ctrl951b06548a20439fa58034d9a07773a4 of type CMS_API.WebUI.WebControls.RazorView has set the maximum duration to 600 seconds
ControlID:__Page of type ASP.legal_privacy_and_cookies_aspx has set the maximum duration to 3600 seconds
Cache Enabled using rule ControlID:Ctrl951b06548a20439fa58034d9a07773a4 of type CMS_API.WebUI.WebControls.RazorView has set the maximum duration to 600 seconds
Cache Page Render Time 01/11/2017 18:33:17
Cache Host WIN-H72RHPIOHQS

-->
    
    <!--[if lt IE 8]>
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome-ie7.min.css" />
    <![endif]-->
    
    <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P978LR');</script>
<!-- End Google Tag Manager --> 
    <script src="/site-elements/js/vendor/modernizr-custom.min.js"></script>
	<script src="/site-elements/js/bugsnag.js" data-apikey="e3d25e0574216113ced2f17468ebb467"></script>

  <meta name="twitter:card" content="summary" /><meta name="twitter:site" content="UniOfHull" /><meta name="twitter:title" content="Privacy and cookies" /><meta name="twitter:description" content="Privacy Policy and cookie information at the University of Hull" /><meta name="twitter:image" /><meta name="og:title" content="Privacy and cookies" /><meta name="og:url" content="http://www.hull.ac.uk/Legal/Privacy-and-cookies.aspx" /><meta name="og:type" content="website" /><meta name="og:description" content="Privacy Policy and cookie information at the University of Hull" /><meta name="og:image" /></head>

  <body>    
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P978LR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --><!--[if lt IE 9]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a class="button button--green" href="http://browsehappy.com/" target="_blank" rel="noopener">upgrade your browser</a> to improve your experience and security.</p>   
<![endif]--><form method="post" action="/Legal/Privacy-and-cookies.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTI5NDMwMDcyOGRkEa/bOBtWWUGbu3QEYpH+R2Koexk=" />
</div>



      <main class="wrap" id="top">

        <div>
          <a href="#main-content" class="visuallyhidden focusable">Skip to content</a>
        </div>

        
    <header>
<section class="main-navigation">
<div class="content">
<div class="logo"><a class="image-link" title="University homepage" href="/"><img class="black" alt="The University of Hull" height="0" width="0" src="/image-library/imported-images/UoH-logo.svg" /></a></div>
<nav>
<ul class="desktop-only">
<li class="study"><a class="image-link">Study</a></li>
<li class="research"><a class="image-link">Research</a></li>
<li class="choose"><a class="image-link">Choose Hull</a></li>
<li class="work"><a class="image-link">Work with us</a></li>
<li class="search"><a class="image-link"><i class="fa fa-search">&nbsp;</i></a></li>
</ul>
<div class="ham-menu mobile-only"><button class="hamburger hamburger--htx" title="Main menu"><span>toggle menu</span></button></div>
</nav></div>
</section>
<section class="menu-items search">
<div class="content">
<div id="fb-queryform"><input id="search" accesskey="q" class="ui-autocomplete-input search-box-input" title="Search the website" type="search" autocomplete="off" placeholder="Search website" size="15" name="query" /><input type="hidden" value="hull-website-meta" name="collection" /><input id="Template_gobutton" title="Search our website" type="button" name="Template$gobutton" /></div>
</div>
</section>
<section class="navigation">
<div class="content">
<div class="primary mobile-only">
<div class="magic-box">
<ul>
<li class="study"><a class="image-link">Study</a></li>
<li class="research"><a class="image-link">Research</a></li>
<li class="choose"><a class="image-link">Choose Hull</a></li>
<li class="work"><a class="image-link">Work with us</a></li>
</ul>
</div>
</div>
<div class="secondary">
<div class="menu-items study"><strong>Choose your subject</strong>

	<ul>
		<li><a href="/Faculties/subjects/accounting-and-finance.aspx">Accounting and Finance</a></li>
		<li><a href="/Faculties/subjects/american-studies.aspx">American Studies</a></li>
		<li><a href="/Faculties/subjects/biological-and-environmental-science.aspx">Biological and Environmental Science</a></li>
		<li><a href="/Faculties/subjects/Biomedical-Sciences.aspx">Biomedical Sciences</a></li>
		<li><a href="/Faculties/subjects/business-management.aspx">Business and Management</a></li>
		<li><a href="/Faculties/subjects/chemical-engineering.aspx">Chemical Engineering</a></li>
		<li><a href="/Faculties/subjects/chemistry.aspx">Chemistry</a></li>
		<li><a href="/Faculties/subjects/computer-science.aspx">Computer Science</a></li>
		<li><a href="/Faculties/subjects/criminology.aspx">Criminology</a></li>
		<li><a href="/Faculties/subjects/drama.aspx">Drama</a></li>
		<li><a href="/Faculties/subjects/economics.aspx">Economics and Business Economics</a></li>
		<li><a href="/Faculties/subjects/education-teaching-childhood.aspx">Education, Teaching and Childhood Studies</a></li>
		<li><a href="/Faculties/subjects/electrical-and-electronic-engineering.aspx">Electrical and Electronic Engineering</a></li>
		<li><a href="/Faculties/subjects/english.aspx">English</a></li>
		<li><a href="/Faculties/subjects/film-digital-media.aspx">Film and Digital Media</a></li>
		<li><a href="/Faculties/subjects/geography-and-geology.aspx">Geography and Geology</a></li>
		<li><a href="/Faculties/subjects/Health-Nursing-and-Midwifery.aspx">Health, Nursing and Midwifery</a></li>
		<li><a href="/Faculties/subjects/history.aspx">History</a></li>
		<li><a href="/Faculties/subjects/law.aspx">Law</a></li>
		<li><a href="/Faculties/subjects/supply-chain-management.aspx">Logistics and Supply Chain Management</a></li>
		<li><a href="/Faculties/subjects/marketing.aspx">Marketing</a></li>
		<li><a href="/Faculties/subjects/mathematics.aspx">Mathematics</a></li>
		<li><a href="/Faculties/subjects/mechanical-engineering.aspx">Mechanical Engineering</a></li>
		<li><a href="/Faculties/subjects/medical-and-biomedical-engineering.aspx">Medical and Biomedical Engineering</a></li>
		<li><a href="/Faculties/subjects/Medicine.aspx">Medicine</a></li>
		<li><a href="/Faculties/subjects/languages.aspx">Modern Languages and Cultures</a></li>
		<li><a href="/Faculties/subjects/music.aspx">Music</a></li>
		<li><a href="/Faculties/subjects/philosophy.aspx">Philosophy</a></li>
		<li><a href="/Faculties/subjects/physics-and-astrophysics.aspx">Physics and Astrophysics</a></li>
		<li><a href="/Faculties/subjects/politics-security-international-studies.aspx">Politics and International Relations</a></li>
		<li><a href="/Faculties/subjects/Psychology.aspx">Psychology</a></li>
		<li><a href="/Faculties/subjects/youth-work-community-development.aspx">Social Work, Youth and Community Development</a></li>
		<li><a href="/Faculties/subjects/sociology-social-sciences.aspx">Sociology and Social Sciences</a></li>
		<li><a href="/Faculties/subjects/Sport-Health-and-Exercise-Science.aspx">Sport, Health and Exercise Science</a></li>
	</ul>

<div class="more-courses"><span>View all our courses:</span><a title="View all our undergraduate courses" class="sys_0 sys_t82" href="/Study/UG/All-UG.aspx">Undergraduate</a> <a title="View all our postgraduate taught courses" class="sys_0 sys_t82" href="/Study/PGT/All-PGT.aspx">Postgraduate Taught</a> <a title="View all our postgraduate research courses" class="sys_0 sys_t82" href="/Study/PGR/All-PGR.aspx">Postgraduate Research</a><a title="View all our PhD Scholarships" class="sys_0 sys_t82" href="/Study/PGR/PhD/PhD.aspx">PhD Scholarships</a><a class="orderpros" title="Order a prospectus" href="/Study/Prospectuses-and-brochures/prospectuses-and-brochures.aspx">Order a prospectus</a></div>
</div>
<div class="menu-items research">
<div class="menu-item-containers"><strong>Research</strong>



<ul>
  <li><a href="/Work-with-us/Research/Research.aspx">Research</a></li>
  <li><a href="/Work-with-us/Research/Research-areas/Discover-our-research.aspx">Discover our research</a></li>
  <li><a href="/Work-with-us/Research/Collaborate/Collaborate.aspx">Collaborate</a></li>
  <li><a href="/Work-with-us/Research/Facilities/Access-our-facilities.aspx">Access Our Facilities</a></li>
  <li><a href="/Work-with-us/Research/Governance/Research-governance.aspx">Research governance</a></li>
</ul>
</div>
</div>
<div class="menu-items choose">
<div class="menu-item-containers"><strong>University and region</strong>



<ul>
  <li><a href="/Choose-Hull/University-and-region/About-us/About-the-University-of-Hull.aspx">About the University of Hull</a></li>
  <li><a href="/Choose-Hull/University-and-region/About-us/Key-documents/Key-documents.aspx">Key documents</a></li>
  <li><a href="/Choose-Hull/University-and-region/City-of-Culture-2017/UK-City-of-Culture.aspx">UK City of Culture</a></li>
  <li><a href="/Choose-Hull/University-and-region/The-city-of-Hull/About-Hull-and-the-Region.aspx">Hull and the Region</a></li>
  <li><a href="/Choose-Hull/University-and-region/Sustainability/Sustainability.aspx">Sustainability</a></li>
</ul>
</div>
<div class="menu-item-containers"><strong>Study at Hull</strong>



<ul>
  <li><a href="/Choose-Hull/Study-at-Hull/Graduate-School/Graduate-School.aspx">Graduate School</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Student-Contract/Student-Contract.aspx">Student Contract</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Welcome/Welcome.aspx">Welcome</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/International/International.aspx">International</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Our-campus/Our-campus.aspx">Our campus</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Open-days/Visit-us.aspx">Visit us</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Admissions/Applying-to-Hull.aspx">Applying to Hull</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Money/Money.aspx">Money</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Library/index.aspx">University Library</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Careers-and-employability/Careers-and-employability.aspx">Careers and employability</a></li>
  <li><a href="/Choose-Hull/Study-at-Hull/Scholarships-and-bursaries/Scholarships-and-bursaries.aspx">Scholarships and bursaries</a></li>
</ul>
</div>
<div class="menu-item-containers"><strong>Student life</strong>



<ul>
  <li><a href="/Choose-Hull/Student-life/Why-choose-hull/Why-choose-Hull.aspx">Choose Hull</a></li>
  <li><a href="/Choose-Hull/Student-life/Accommodation/accommodation.aspx">Accommodation</a></li>
  <li><a href="/Choose-Hull/Student-life/Students-union/Students-union.aspx">Students&#39; union</a></li>
  <li><a href="/Choose-Hull/Student-life/Clubs-and-societies/Clubs-and-societies.aspx">Clubs and societies</a></li>
  <li><a href="/Choose-Hull/Student-life/Sport-and-leisure/Sports-and-leisure.aspx">Sports and leisure</a></li>
  <li><a href="/Choose-Hull/Student-life/Student-support/Student-support.aspx">Student support</a></li>
</ul>
</div>
</div>
<div class="menu-items work">
<div class="menu-item-containers"><strong>Business services</strong>



<ul>
  <li><a href="/Work-with-us/Business-services/Apprenticeships/Apprenticeships.aspx">Apprenticeships</a></li>
  <li><a href="/Work-with-us/Business-services/Working-with-students/Working-with-students.aspx">Working with students</a></li>
  <li><a href="/Work-with-us/Business-services/Supporting-enterprise/Supporting-enterprise.aspx">Supporting enterprise</a></li>
</ul>
</div>
<div class="menu-item-containers"><strong>More</strong>
  


<ul>
  <li><a href="/Work-with-us/More/Jobs/Work-with-us.aspx">Work with us</a></li>
  <li><a href="/Work-with-us/More/Media-centre/news/news.aspx">News</a></li>
  <li><a href="/Work-with-us/More/Schools-and-colleges/Schools-and-colleges-liaison.aspx">Schools and colleges</a></li>
  <li><a href="/Work-with-us/More/Alumni/Alumni.aspx">Hull University Alumni</a></li>
  <li><a href="/Work-with-us/More/Health-Safety-Services/Health-Safety-Services.aspx">Health &amp; Safety Services</a></li>
  <li><a href="/Work-with-us/More/Supplying-Our-University/Procurement.aspx">Procurement</a></li>
</ul>
</div>
</div>
</div>
</div>
</section>
</header>
<article itemprop="mainEntity" itemtype="http://schema.org/CreativeWork" id="main-content" role="main">


<section class="in-page-navigation cfm">
	<div class="grid">
		<div class="unit whole">
			<ul class="parent-menu-items">
 					<li class=""><a href="/Legal/Freedom-of-information.aspx" title="Visit the Freedom of information page"><i class="fa fa-home" aria-hidden="true"></i> Freedom of information</a></li>
					<li class=""><a href="/Legal/Accessibility.aspx" title="Visit the Accessibility page"> Accessibility</a></li>
					<li class=""><a href="/Legal/Model-Publication-Scheme.aspx" title="Visit the Model Publication Scheme page"> Model Publication Scheme</a></li>
					<li class=""><a href="/Legal/modern-slavery-act.aspx" title="Visit the Modern Slavery Act 2015 page"> Modern Slavery Act 2015</a></li>
					<li class="cur"><a href="/Legal/Privacy-and-cookies.aspx" title="Visit the Privacy and cookies page"> Privacy and cookies</a></li>
					<li class=""><a href="/Legal/Terms-and-conditions.aspx" title="Visit the Terms and conditions page"> Terms and conditions</a></li>
			</ul>
		</div>
	</div>
</section>


  
  <section class="page-intro half">
    <div class="overlay"></div>
    	<img src="/editor-assets/images/Business/Bus-Management-ITC.jpg" alt="Privacy and cookies" class="align40" />
    
        <div class="grid">
            <div class="unit whole">
                <div class="title">
                   <h1>Privacy and cookies</h1>
                </div>
            </div>
        </div>
              
	</section>



<section>
<div class="grid">
<div class="unit whole">

<h3>Privacy Policy</h3>
<h4>Website Disclaimer</h4>
<p>The information contained in this website is for general information purposes only. The information is provided by the University of Hull and whilst we do our best to keep the information up-to-date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>
<p>Without limiting the effect of the previous paragraph, we reserve the right to introduce changes to the information given on our Course Finder pages, including the addition, withdrawal, re-location or restructuring of courses.</p>
<p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of or in connection with the use of this website.</p>
<p>Through this website you are able to link to other websites which are not under the control of the University of Hull. We have no control over the nature, content and availability of those sites and the inclusion of any links does not necessarily imply a recommendation</p>
<h4>Online privacy practices</h4>
<p>Your right to privacy is important to us.</p>
<p>We are keen to strike a fair balance between your personal privacy and ensuring you obtain full value from the internet and other products and services we may be able to market to you.</p>
<p>We are fully registered under the Data Protection Act 1998 and ensure we comply with all protection the Act affords to you. We use the data protection padlock symbol below to show when we gather personal data from you. Look out for it on the site.</p>
<p>Reproduced courtesy of the Information Commissioner's Office and the National Consumer Council.</p>
<h4>Cookies</h4>
<p><b>What are cookies?</b></p>
<p>Cookies are small pieces of information that are stored on your hard drive to allow the University of Hull to recognise you when you visit. They can remember your preferences by gathering and storing information. They do not identify you as an individual user, just the computer used. Cookies cannot be used to run programs or create viruses on your computer. It does not give the University access to your computer.</p>
<p><b>Why do we use them?</b></p>
<p>We may use cookies to improve or personalise your online experience of our websites. For example we use Google Analytics to enable us to identify which pages you find useful and which you do not. This allows us to improve our navigation so you can find things easier in future.</p>
<p><b>If you don&rsquo;t want to receive cookies</b></p>
<p>If you would prefer not to receive cookies while browsing our site, you can set your browser so that it will not download cookies onto your computer. Doing so will still allow you to navigate through the majority of our site but possibly not all of it. If you wish to register on a password protected area of our website you will need to allow &ldquo;per-session&rdquo; cookies. These are temporarily used while you are visiting the site but deleted when you close your browser or log out.</p>
<p>To find out more information about cookies, visit:&nbsp;<a class="sys_16" onclick="void(window.open('http://www.allaboutcookies.org/',''));return false;" onkeypress="void(window.open('http://www.allaboutcookies.org/',''));return false;" href="http://www.allaboutcookies.org/">www.allaboutcookies.org</a></p>

</div>
</div>
</section>



<section>
<div class="grid">
<div class="unit whole">

<h3>How we use Cookies</h3>
<p>We may use cookies to store information about your membership to parts of our site, or to enable you to log in to online resources. This might include blogs, portals, forums, or protected content. The information is stored to enable you to use these resources, and to remember your log in details between sessions. If you clear your cookies, you may need to login to these parts of the website each time you visit.</p>
<p><b>Hobsons</b></p>
<p>The University has a relationship with Hobsons Plc (Hobsons), any URLs beginning with</p>
<p><a class="sys_16" onclick="void(window.open('https://hull.hobsons.co.uk',''));return false;" onkeypress="void(window.open('https://hull.hobsons.co.uk',''));return false;" href="https://hull.hobsons.co.uk">hull.hobsons.co.uk</a>&nbsp;are hosted by Hobsons as part of this relationship:</p>
<p><a title="Hobsons privacy policy" class="sys_16" onclick="void(window.open('https://www.hobsons.com/emea/privacy-policy',''));return false;" onkeypress="void(window.open('https://www.hobsons.com/emea/privacy-policy',''));return false;" href="https://www.hobsons.com/emea/privacy-policy">A list of the Cookies used by Hobsons</a>&nbsp;for the processing of information on these pages can be found here.</p>
<p><b>Google Analytics</b></p>
<p>This website uses Google Analytics, a web analytics service provided by Google, Inc.. Google Analytics uses &lsquo;cookies&rsquo;, which are text files placed on your computer. Google Analytics allows the university to look at how visitors are using the site with the aim of improving visitor&rsquo;s experience.</p>
<h4>Subscription</h4>
<p>In some areas of our website, we ask you to provide information that will enable us to enhance your site visit or reply to you after your visit. This would include where you subscribe to our online newsletters or provide feedback to us through contact form or when you complete any online survey we may offer from time to time. When you do, so we ask you to give us your name, email address and other personal information that will be needed to supply the services to you. In relation to online newsletters you can &lsquo;unsubscribe' at any time. We may use your personal data for future email mailings, support, notification of new products or new versions, general correspondence regarding the products and correspondence which may relate to you. If you would rather not receive future marketing emails from us please inform us by&nbsp;<a class="email" title="Remove me from marketing emails" href="mailto:marketing@hull.ac.uk">email</a>. Such use is strictly in accordance with the Data Protection Act 1998 and the Privacy and Electronic Communications (EC Directive) Regulations 2003. Every email from us comes with an easy to use unsubscribe instruction and you can be removed from our lists at any time, although this can take up to 30 days to effect. Your details will never be sent to any third party or group company of ours and we will only market to you the same kind of goods and services you purchased from us or made an enquiry to us about.</p>
<p>The information you provide will be kept confidential and will be used only to support your relationship with us. We do not disclose or sell your information outside the University except if we sell the whole or part of our business.</p>
<h4>Further information on data protection and personal privacy</h4>
<p>For further information from us on data protection and privacy contact:</p>
<p>The Data Protection Officer,<br />The University of Hull,<br />Cottingham Road,<br />Hull,<br />HU6 7RX</p>
<p><a class="email" title="Email the data protection officer" href="mailto:data.protection@hull.ac.uk">Data.Protection@hull.ac.uk</a></p>
<p>Use of this Site is subject to our <a title="Terms and conditions" class="sys_0 sys_t82" href="/Legal/Terms-and-conditions.aspx">terms and conditions</a>.</p>
<h4>Warning</h4>
<p>Our Site provides links to other sites. We are not responsible for their data protection policies. You should check their privacy policies if you are concerned.</p>

</div>
</div>
</section>


</article>
<footer itemscope="." itemtype="http://schema.org/Organization">
<section class="soc-net-footer">
<div class="grid">
<div class="whole">
<div class="soc-net-container">
<ul>
<li class="iHull"><a class="image-link" title="Download the iHull app" target="_blank" rel="noopener" href="https://i.hull.ac.uk/get"><img alt="iHull desktop app" src="/site-elements/img/layout/footer/soc-torch.svg" /><span class="sr-only">iHull App</span></a></li>
<li class="facebook"><a class="image-link" title="Visit us on Facebook" target="_blank" rel="noopener" href="https://www.facebook.com/UniversityOfHull"><i class="fa fa-facebook">&nbsp;</i><span class="sr-only">Facebook</span></a></li>
<li class="twitter"><a class="image-link" title="Visit us on Twitter" target="_blank" rel="noopener" href="https://twitter.com/UniOfHull"><i class="fa fa-twitter">&nbsp;</i><span class="sr-only">Twitter</span></a></li>
<li class="snapchat"><a class="image-link" title="Visit us on Snapchat" target="_blank" rel="noopener" href="https://www.snapchat.com/add/uniofhull"><i class="fa fa-snapchat-ghost">&nbsp;</i><span class="sr-only">Snapchat</span></a></li>
<li class="youtube"><a class="image-link" title="Visit our YouTube channel" target="_blank" rel="noopener" href="https://www.youtube.com/user/marcomshull"><i class="fa fa-youtube">&nbsp;</i><span class="sr-only">YouTube</span></a></li>
<li class="flickr"><a class="image-link" title="Visit our Flickr feed" target="_blank" rel="noopener" href="https://www.flickr.com/photos/universityofhull"><i class="fa fa-flickr">&nbsp;</i><span class="sr-only">Flickr</span></a></li>
<li class="linkedin"><a class="image-link" title="Visit us on LinkedIn" target="_blank" rel="noopener" href="https://www.linkedin.com/edu/the-university-of-hull-12668"><i class="fa fa-linkedin">&nbsp;</i><span class="sr-only">LinkedIn</span></a></li>
<li class="instagram"><a class="image-link" title="View our Instagram page" target="_blank" rel="noopener" href="https://www.instagram.com/universityofhull/"><i class="fa fa-instagram">&nbsp;</i><span class="sr-only">Instagram</span></a></li>
</ul>
</div>
</div>
</div>
</section>
<section class="grid main-foot">
<div itemprop="address" itemscope="." itemtype="http://schema.org/PostalAddress" class="four-fifths">
<div class="grid">
<div class="one-third">
<h5 class="heading-style">Current students and staff</h5>
<ul class="staff-students-menu">
<li><a title="Schools and Faculties" class="sys_0 sys_t5022" href="/Faculties/Faculties.aspx">Schools and Faculties</a></li>
<li><a class="sys_16" title="Check your email" href="https://mail.hull.ac.uk">Mail</a></li>
<li><a class="sys_16" title="Library Services" href="http://libguides.hull.ac.uk/lib">Library</a></li>
<li><a class="sys_16" title="MyAdmin" href="https://myadmin.hull.ac.uk">MyAdmin</a></li>
<li><a class="sys_16" title="Canvas" href="https://canvas.hull.ac.uk">Canvas</a></li>
<li><a title="Student services centre" class="sys_0 sys_t5999" href="/Choose-Hull/Student-life/Student-support/Where-to-get-help/AskHU.aspx">AskHU</a></li>
<li><a class="sys_16" title="Your Timetable" href="https://timetables.hull.ac.uk">Timetables</a></li>
<li><a class="sys_16" title="SharePoint" href="https://share.hull.ac.uk">SharePoint</a></li>
<li><a class="sys_16" title="Information and Communication Technology Department" href="/ICT/">ICTD</a></li>
<li><a class="sys_16" title="University Shop" href="http://shop.hull.ac.uk">Online Store</a></li>
</ul>
</div>
<div class="one-third">
<h5 class="heading-style">Contact</h5>
<ul>
<li itemprop="telephone"><a class="telephone" title="For general enquiries, call us on +44 (0)1482 346311" href="tel:+441482346311">+44 (0)1482 346311</a></li>
<li><a title="Let us know how we are doing" class="sys_0 sys_t82" href="/website-assets/website-feedback.aspx"><i class="fa fa-commenting-o">&nbsp;</i>Let us know how we are doing</a></li>
</ul>
</div>
<div class="one-third">
<h5 class="heading-style">Find</h5>
<h6 itemprop="name">University of Hull</h6>
<address>
<ul>
<li><span itemprop="addressLocality">Hull</span>, <span itemprop="addressCountry">UK</span></li>
<li itemprop="postalCode">HU6 7RX</li>
</ul>
<ul>
<li><a title="About our campus" class="sys_0 sys_t82" href="/Choose-Hull/Study-at-Hull/Our-campus/Our-campus.aspx">Getting here</a></li>
<li><a title="Download the campus map" class="sys_21" href="/editor-assets/docs/Campus-Map.pdf">Campus map</a></li>
</ul>
</address></div>
</div>
</div>
</section>
<section class="accred-footer">
<div class="accred-inner">
<ul>
<li><img alt="Athena Swan Bronze Award" height="85" width="165" src="/site-elements/img/layout/footer/Athena-Swan-Bronze-Award.jpg" /></li>
<li><img alt="Queens Anniversary Prizes 2015" height="170" width="140" src="/site-elements/img/layout/footer/Queens-Anniversary-Prizes-2015.jpg" /></li>
<li><img alt="Teaching Excellence Framework Silver Award" height="170" width="89" src="/site-elements/img/layout/footer/Teaching-Excellence-Framework-Silver-Award.jpg" /></li>
<li><img alt="QAA UK Quality Assured" height="100" width="170" src="/site-elements/img/layout/footer/QAA-UK-Quality-Assured.jpg" /></li>
</ul>
</div>
</section>
<section class="colophon">
<div class="col-inner">
<div class="copy">University of Hull &copy; 2017 </div>
<nav class="pull-right">



<ul>
  <li><a href="/Legal/Freedom-of-information.aspx" rel="noopener">Freedom of information</a></li>
  <li><a href="/Legal/Accessibility.aspx" rel="noopener">Accessibility</a></li>
  <li><a href="/Legal/Model-Publication-Scheme.aspx" rel="noopener">Model Publication Scheme</a></li>
  <li><a href="/Legal/modern-slavery-act.aspx" rel="noopener">Modern Slavery</a></li>
  <li><a href="/Legal/Privacy-and-cookies.aspx" rel="noopener">Privacy and cookies</a></li>
  <li><a href="/Legal/Terms-and-conditions.aspx" rel="noopener">Website terms and conditions</a></li>
</ul>
</nav></div>
</section>
</footer>



      </main>
      <a href="#top" class="cd-top">Top</a>

      <!--[if (gte IE 6)&(lte IE 8)]>
        <script type="text/javascript" src="/site-elements/js/vendor/selectivizr.min.js"></script><noscript><p>Browser does not support script.</p></noscript>
      <![endif]-->

    
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="335FF928" />
</div>
		<script  type="text/javascript" src="/site-elements/js/pages/generic.js?version=18000&amp;build=831596"></script><noscript><p>Browser does not support script.</p></noscript>
</form>
    
  </body>
  </html>