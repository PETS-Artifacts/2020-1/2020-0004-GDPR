<!DOCTYPE html>

	<!--[if lt IE 7]><html lang="en-GB" class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie6" lang="en-gb" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"> <![endif]-->
	<!--[if IE 7]>   <html lang="en-GB" class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-gb" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"> <![endif]-->
	<!--[if IE 8]>   <html lang="en-GB" class="no-js lt-ie9 lt-ie8" lang="en-gb" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"> <![endif]-->
	<!--[if IE 9]>   <html lang="en-GB" class="no-js lt-ie9" lang="en-gb" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage"> <![endif]-->
	<!--[if (gt IE 9)]><!-->
	<html lang="en-GB" class="no-js" dir="ltr" itemscope="" itemtype="http://schema.org/WebPage">
	<!--<![endif]-->

	<head><meta http-equiv="X-UA-Compatible" content="IE=Edge" /><meta charset="utf-8"><title>
	Privacy and cookies | University of Hull
</title><meta name="theme-color" content="#991E66" />
    
    
    
    

    <meta name="GENERATOR" content="Contensis CMS Version 11.3" />
<meta name="description" content="Privacy Policy and cookie information at the University of Hull" />
<meta name="Keywords" content="privacy, policy, cookies, university, hull" />
<meta name="viewport" content="width=device-width, initial-scale=1" />




		<!-- Favicons -->
		<link rel="shortcut icon" href="/site-elements/img/favicon/favicon.ico" /><link rel="apple-touch-icon" href="/site-elements/img/favicon/favicon-57.png" /><link rel="apple-touch-icon" sizes="72x72" href="/site-elements/img/favicon/favicon-72.png" /><link rel="apple-touch-icon" sizes="114x114" href="/site-elements/img/favicon/favicon-114.png" /><link rel="apple-touch-icon" sizes="144x144" href="/site-elements/img/favicon/favicon-144.png" /><link rel="apple-touch-icon" sizes="180x180" href="/site-elements/img/favicon/favicon-180.png" /><link rel="apple-touch-icon" sizes="310x310" href="/site-elements/img/favicon/favicon-310.png" /><link href="/assets/developer/css/layout.css" rel="stylesheet" type="text/css" /><link href="/site-elements/css/site.css?version=34271" rel="stylesheet" type="text/css" />
<link href="/site-elements/css/pages/generic.css?version=37134" rel="stylesheet" type="text/css" />
<!--
ControlID:Ctrlf42f793193e049adbc0b3bbb8ac737ef of type CMS_API.WebUI.WebControls.RazorView has set the maximum duration to 600 seconds
ControlID:__Page of type ASP.legal_privacy_and_cookies_aspx has set the maximum duration to 3600 seconds
Cache Enabled using rule ControlID:Ctrlf42f793193e049adbc0b3bbb8ac737ef of type CMS_API.WebUI.WebControls.RazorView has set the maximum duration to 600 seconds
Cache Page Render Time 26/03/2019 05:23:46

--><link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous" />

		<!--[if lt IE 8]>
    	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome-ie7.min.css" />
    <![endif]-->

		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P978LR');</script>
<!-- End Google Tag Manager -->

		<script src="/site-elements/js/vendor/modernizr-custom.min.js">

		</script>
		<script src="//d2wy8f7a9ursnm.cloudfront.net/v5.0.0/bugsnag.min.js">

		</script>
		<script>
			window.bugsnagClient = bugsnag('4d11eadb6ee3b68d1a41909b07714bad')
		</script>

	<meta name="twitter:card" content="summary" /><meta name="twitter:site" content="@UniOfHull" /><meta name="twitter:title" content="Privacy and cookies" /><meta name="twitter:description" content="Privacy Policy and cookie information at the University of Hull" /><meta name="twitter:image" /><meta name="og:title" content="Privacy and cookies" /><meta name="og:url" content="https://www.hull.ac.uk/legal/privacy-and-cookies.aspx" /><meta name="og:type" content="website" /><meta name="og:description" content="Privacy Policy and cookie information at the University of Hull" /><meta name="og:image" /><link rel="canonical" href="https://www.hull.ac.uk/legal/privacy-and-cookies.aspx" /></head>

	<body>
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P978LR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --><!--[if lt IE 9]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a class="button button--green" href="http://browsehappy.com/" target="_blank" rel="noopener">upgrade your browser</a> to improve your experience and security.</p>   
<![endif]--><form method="post" action="/legal/privacy-and-cookies.aspx" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="adNzXa3ESBZP2aP6TCwdxEJLkwB2ZBgWBrKVNb2deoVSSE4kE7EE4m1rII0sDei5lH9r48E6v4djHXUuXHlwPpcfJAk=" />
</div>



			<main class="wrap" id="top">

				<div>
					<a href="#main-content" class="visuallyhidden focusable">Skip to content</a>
				</div>

				
    <header>
<section class="main-navigation">
<div class="content">
<div class="logo"><a class="image-link" title="University homepage" href="/"><img class="black" alt="The University of Hull" src="//www.hull.ac.uk/site-elements/img/layout/header/logo/UoH-logo.svg" /></a></div>
<nav>
<ul class="desktop-only">
<li class="study"><a class="image-link">Study</a></li>
<li class="choose"><a class="image-link">Choose Hull</a></li>
<li class="research"><a class="image-link">Research</a></li>
<li class="business"><a class="image-link">Business</a></li>
<li class="work"><a class="image-link">Work with us</a></li>
<li class="search"><a class="image-link"><i class="fa fa-search">&nbsp;</i></a></li>
</ul>
<div class="ham-menu mobile-only"><button class="hamburger hamburger--htx" title="Main menu"><span>toggle menu</span></button></div>
</nav></div>
</section>
<section class="menu-items search">
<div class="content">
<div id="fb-queryform"><input id="search" accesskey="q" class="ui-autocomplete-input search-box-input" title="Search the website" type="search" autocomplete="off" placeholder="Search website" size="15" name="query" /><input type="hidden" value="hull-website-meta" name="collection" /><input id="Template_gobutton" title="Search our website" type="button" name="Template$gobutton" /></div>
</div>
</section>
<section class="navigation">
<div class="content">
<div class="primary mobile-only">
<div class="magic-box">
<ul>
<li class="study"><a class="image-link">Study</a></li>
<li class="choose"><a class="image-link">Choose Hull</a></li>
<li class="research"><a class="image-link">Research</a></li>
<li class="business"><a class="image-link">Business</a></li>
<li class="work"><a class="image-link">Work with us</a></li>
</ul>
</div>
</div>
<div class="secondary">
<div class="menu-items study"><b>Choose your subject</b>

	<ul>
		<li><a href="/faculties/subjects/accounting-and-finance.aspx">Accounting and Finance</a></li>
		<li><a href="/faculties/subjects/american-studies.aspx">American Studies</a></li>
		<li><a href="/faculties/subjects/biochemistry.aspx">Biochemistry</a></li>
		<li><a href="/faculties/subjects/biological-science.aspx">Biological Science</a></li>
		<li><a href="/faculties/subjects/biomedical-sciences.aspx">Biomedical and Forensic Science</a></li>
		<li><a href="/faculties/subjects/business-management.aspx">Business and Management</a></li>
		<li><a href="/faculties/subjects/chemical-engineering.aspx">Chemical Engineering</a></li>
		<li><a href="/faculties/subjects/chemistry.aspx">Chemistry</a></li>
		<li><a href="/faculties/subjects/computer-science.aspx">Computer Science</a></li>
		<li><a href="/faculties/subjects/criminology.aspx">Criminology</a></li>
		<li><a href="/faculties/subjects/drama.aspx">Drama</a></li>
		<li><a href="/faculties/subjects/economics.aspx">Economics</a></li>
		<li><a href="/faculties/subjects/education-teaching-childhood.aspx">Education, Teaching and Childhood Studies</a></li>
		<li><a href="/faculties/subjects/electrical-and-electronic-engineering.aspx">Electrical and Electronic Engineering</a></li>
		<li><a href="/faculties/subjects/english.aspx">English</a></li>
		<li><a href="/faculties/subjects/environmental-science.aspx">Environmental Science</a></li>
		<li><a href="/faculties/subjects/film-digital-media.aspx">Film and Digital Media</a></li>
		<li><a href="/faculties/subjects/geography.aspx">Geography</a></li>
		<li><a href="/faculties/subjects/geology.aspx">Geology</a></li>
		<li><a href="/faculties/subjects/health-nursing-and-midwifery.aspx">Health, Nursing and Midwifery</a></li>
		<li><a href="/faculties/subjects/history.aspx">History</a></li>
		<li><a href="/faculties/subjects/law.aspx">Law</a></li>
		<li><a href="/faculties/subjects/supply-chain-management.aspx">Logistics and Supply Chain Management</a></li>
		<li><a href="/faculties/subjects/marketing.aspx">Marketing</a></li>
		<li><a href="/faculties/subjects/mathematics.aspx">Mathematics</a></li>
		<li><a href="/faculties/subjects/mechanical-engineering.aspx">Mechanical Engineering</a></li>
		<li><a href="/faculties/subjects/mechatronics-and-robotics.aspx">Mechatronics and Robotics</a></li>
		<li><a href="/faculties/subjects/medical-and-biomedical-engineering.aspx">Medical and Biomedical Engineering</a></li>
		<li><a href="/faculties/subjects/medicine.aspx">Medicine</a></li>
		<li><a href="/faculties/subjects/modern-languages-and-cultures.aspx">Modern Languages and Cultures</a></li>
		<li><a href="/faculties/subjects/music.aspx">Music</a></li>
		<li><a href="/faculties/subjects/philosophy.aspx">Philosophy</a></li>
		<li><a href="/faculties/subjects/physics-and-astrophysics.aspx">Physics and Astrophysics</a></li>
		<li><a href="/faculties/subjects/politics-security-international-studies.aspx">Politics and International Studies</a></li>
		<li><a href="/faculties/subjects/psychology.aspx">Psychology</a></li>
		<li><a href="/faculties/subjects/social-work-welfare.aspx">Social Work and Welfare</a></li>
		<li><a href="/faculties/subjects/sociology-social-sciences.aspx">Sociology and Social Sciences</a></li>
		<li><a href="/faculties/subjects/sport-health-and-exercise-science.aspx">Sport, Health and Exercise Science</a></li>
	</ul>

<div class="more-courses"><span>View all our courses:</span> <a title="View all our undergraduate courses" class="sys_0 sys_t0" href="/study/ug/all-ug.aspx">Undergraduate</a> <a title="View all our postgraduate taught courses" class="sys_0 sys_t0" href="/study/pgt/all-pgt.aspx">Postgraduate Taught</a> <a title="View all our postgraduate research courses" class="sys_0 sys_t0" href="/study/pgr/index.aspx">Postgraduate Research</a> <a title="View all our PhD Scholarships" class="sys_0 sys_t0" href="/study/pgr/phd/phd-scholarships.aspx">PhD Scholarships</a> <a title="View all our Online courses" target="_blank" class="sys_16" href="https://online.hull.ac.uk">Online</a> <a class="orderpros" title="Order a prospectus" href="/study/prospectuses-and-brochures/prospectuses-and-brochures.aspx">Order a prospectus</a></div>
</div>
<div class="menu-items choose">
<div class="menu-item-containers"><b>University and region</b>



<ul>
  <li><a href="/choose-hull/university-and-region/about-us/about-the-university-of-hull.aspx">About the University of Hull</a></li>
  <li><a href="/choose-hull/university-and-region/city-of-culture-2017/uk-city-of-culture.aspx">UK City of Culture</a></li>
  <li><a href="/choose-hull/university-and-region/the-city-of-hull/about-hull-and-the-region.aspx">Hull and the Region</a></li>
  <li><a href="/choose-hull/university-and-region/sustainability/sustainability.aspx">Sustainability</a></li>
  <li><a href="/choose-hull/university-and-region/key-documents/key-documents.aspx">Key documents</a></li>
</ul>
</div>
<div class="menu-item-containers"><b>Study at Hull</b>



<ul>
  <li><a href="/choose-hull/study-at-hull/visit-us/visit-us.aspx">Visit us</a></li>
  <li><a href="/choose-hull/study-at-hull/graduate-school/graduate-school.aspx">Graduate School</a></li>
  <li><a href="/choose-hull/study-at-hull/student-contract/student-contract.aspx">Student Contract</a></li>
  <li><a href="/choose-hull/study-at-hull/welcome/welcome-week.aspx">Welcome Week</a></li>
  <li><a href="/choose-hull/study-at-hull/international/international.aspx">International</a></li>
  <li><a href="/choose-hull/study-at-hull/admissions/applying-to-hull.aspx">Applying to Hull</a></li>
  <li><a href="/choose-hull/study-at-hull/money/money.aspx">Money</a></li>
  <li><a href="/choose-hull/study-at-hull/library/index.aspx">University Library</a></li>
  <li><a href="/choose-hull/study-at-hull/careers-and-employability/careers-and-employability.aspx">Careers and employability</a></li>
  <li><a href="/choose-hull/study-at-hull/scholarships-and-bursaries/scholarships-and-bursaries.aspx">Scholarships and bursaries</a></li>
</ul>
</div>
<div class="menu-item-containers"><b>Student life</b>



<ul>
  <li><a href="/choose-hull/student-life/student-support/student-support.aspx">Student support</a></li>
  <li><a href="/choose-hull/student-life/virtual-tour/virtual-tour.aspx">Virtual tour</a></li>
  <li><a href="/choose-hull/student-life/why-choose-hull/why-choose-hull.aspx">Choose Hull</a></li>
  <li><a href="/choose-hull/student-life/accommodation/accommodation.aspx">Accommodation</a></li>
  <li><a href="/choose-hull/student-life/students-union/students-union.aspx">Students&#39; union</a></li>
  <li><a href="/choose-hull/student-life/clubs-and-societies/clubs-and-societies.aspx">Clubs and societies</a></li>
  <li><a href="/choose-hull/student-life/sport-and-leisure/sports-and-leisure.aspx">Sports and leisure</a></li>
  <li><a href="/choose-hull/student-life/graduation-ceremonies/graduation-ceremonies.aspx">Graduation ceremonies</a></li>
</ul>
</div>
</div>
<div class="menu-items research">
<div class="heading"><a title="Our research" class="sys_0 sys_t0" href="/work-with-us/research/research.aspx"> Visit Research home </a></div>
<div class="main-links">
<div><strong>Our research</strong>
<ul>
<li><a title="Hot topics" class="sys_0 sys_t0" href="/work-with-us/research/hot-topics.aspx">Hot topics</a></li>
<li><a title="Research Institutes" class="sys_0 sys_t0" href="/work-with-us/research/research-institutes.aspx">Research institutes</a></li>
<li><a title="Faculty research" class="sys_0 sys_t0" href="/work-with-us/research/faculty-research.aspx">Faculty research</a></li>
<li><a title="Research directory" class="sys_0 sys_t0" href="/work-with-us/research/research-directory.aspx">Research directory</a></li>
</ul>
</div>
<div><strong>Our people</strong>
<ul>
<li><a title="Research - Our people" class="sys_0 sys_t0" href="/work-with-us/research/our-people.aspx">Search our experts</a></li>
<li><a title="Research partnerships" class="sys_0 sys_t0" href="/work-with-us/research/research-partnerships.aspx">Collaborate with us</a></li>
</ul>
</div>
<div><strong>Research with us</strong>
<ul>
<li><a title="Why join us" class="sys_0 sys_t0" href="/work-with-us/research/why-join-us.aspx">Supporting our researchers</a></li>
<li><a title="Postgraduate research" class="sys_0 sys_t0" href="/study/pgr/index.aspx">PG research</a></li>
<li><a title="Graduate School" class="sys_0 sys_t0" href="/choose-hull/study-at-hull/graduate-school/graduate-school.aspx">Graduate School</a></li>
</ul>
</div>
<div><strong>Research environment</strong>
<ul>
<li><a title="Research integrity and governance" class="sys_0 sys_t0" href="/work-with-us/research/governance/research-integrity-and-governance.aspx">Research integrity and governance</a></li>
<li><a title="Research news" class="sys_16" href="/work-with-us/more/media-centre/news/news-archive.aspx?cat=research">Research news</a></li>
</ul>
</div>
</div>
<div class="links"><strong>Quick links</strong> <a class="quick" title="Research directory" href="/work-with-us/research/research-directory.aspx">Search all research</a> <a class="quick" title="Research - Our people" href="/work-with-us/research/our-people.aspx">Find a researcher</a> <a class="quick" href="/work-with-us/research/talk-to-our-team.aspx">Contact us</a></div>
</div>
<div class="menu-items business">
<div class="menu-item-containers"><b>Business</b>



<ul>
  <li><a href="/beta/business/business.aspx">Business</a></li>
  <li><a href="/beta/business/partnerships-and-collaborations.aspx">Partnerships and collaborations</a></li>
  <li><a href="/beta/business/supporting-enterprise.aspx">Supporting enterprise</a></li>
  <li><a href="/beta/business/apprenticeships.aspx">Apprenticeships</a></li>
  <li><a href="/beta/business/working-with-students.aspx">Working with students</a></li>
  <li><a href="/beta/business/contact-us.aspx">Contact us</a></li>
</ul>
</div>
</div>
<div class="menu-items work">
<div class="menu-item-containers"><b>Work with us</b>
  


<ul>
  <li><a href="/work-with-us/more/jobs/work-with-us.aspx">Work with us</a></li>
  <li><a href="/work-with-us/more/media-centre/news/news.aspx">News</a></li>
  <li><a href="/work-with-us/more/conferences-and-venue-hire/conferences-and-venue-hire.aspx">Conferences, events and venue hire</a></li>
  <li><a href="/work-with-us/more/schools-and-colleges/schools-and-colleges-liaison.aspx">Schools and colleges</a></li>
  <li><a href="/work-with-us/more/alumni/alumni.aspx">Hull University Alumni</a></li>
  <li><a href="/work-with-us/more/health-safety-services/health-safety-services.aspx">Health &amp; Safety Services</a></li>
  <li><a href="/work-with-us/more/supplying-our-university/procurement.aspx">Procurement</a></li>
</ul>
</div>
</div>
</div>
</div>
</section>
</header>
<article itemprop="mainEntity" itemtype="http://schema.org/CreativeWork" id="main-content" role="main">


	<section class="in-page-navigation cfm">
		<div class="grid">
			<ul class="parent-menu-items">
				
					<li class=""><a href="/legal/freedom-of-information.aspx" title="Visit the Freedom of information page">Freedom of information</a></li>
					<li class=""><a href="/legal/accessibility.aspx" title="Visit the Accessibility page">Accessibility</a></li>
					<li class=""><a href="/legal/model-publication-scheme.aspx" title="Visit the Model Publication Scheme page">Model Publication Scheme</a></li>
					<li class=""><a href="/legal/modern-slavery-act.aspx" title="Visit the Modern Slavery Act 2015 page">Modern Slavery Act 2015</a></li>
					<li class="cur"><a href="/legal/privacy-and-cookies.aspx" title="Visit the Privacy and cookies page">Privacy and cookies</a></li>
					<li class=""><a href="/legal/terms-and-conditions.aspx" title="Visit the Terms and conditions page">Terms and conditions</a></li>
					<li class=""><a href="/choose-hull/university-and-region/key-documents/data-protection.aspx" title="Visit the Data Protection page">Data Protection</a></li>

				<li class="more hidden" data-width="100">
					<a href="#0">More</a>
					<ul></ul>
				</li>
			</ul>
		</div>
	</section>


    <header class="hero standard shard sea ">
		<div class="overlay"></div>
		<img src="/editor-assets/images/business/bus-management-itc.jpg" alt="Bus_Management_ITC" />
		<div class="text-container">
			<div class="text-content">
				
					<h1>Privacy and cookies</h1>
			</div>
		</div>
	</header>



<section class="cyan-title">
<div class="grid">

<h2>Privacy Policy</h2>
<h3>Website Disclaimer</h3>
<p>The information contained in this website is for general information purposes only. The information is provided by the University of Hull and whilst we do our best to keep the information up-to-date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>
<p>Without limiting the effect of the previous paragraph, we reserve the right to introduce changes to the information given on our Course Finder pages, including the addition, withdrawal, re-location or restructuring of courses.</p>
<p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of or in connection with the use of this website.</p>
<p>Through this website you are able to link to other websites which are not under the control of the University of Hull. We have no control over the nature, content and availability of those sites and the inclusion of any links does not necessarily imply a recommendation</p>
<h3>Online privacy practices</h3>
<p>Your right to privacy is important to us.</p>
<p>We are keen to strike a fair balance between your personal privacy and ensuring you obtain full value from the internet and other products and services we may be able to market to you.</p>
<p>We are fully registered under the Data Protection Act 1998 and ensure we comply with all protection the Act affords to you. We use the data protection padlock symbol below to show when we gather personal data from you. Look out for it on the site.</p>
<p>Reproduced courtesy of the Information Commissioner's Office and the National Consumer Council.</p>
<h3>Cookies</h3>
<p><strong>What are cookies?</strong></p>
<p>Cookies are small pieces of information that are stored on your hard drive to allow the University of Hull to recognise you when you visit. They can remember your preferences by gathering and storing information. They do not identify you as an individual user, just the computer used. Cookies cannot be used to run programs or create viruses on your computer. It does not give the University access to your computer.</p>
<p><strong>Why do we use them?</strong></p>
<p>We may use cookies to improve or personalise your online experience of our websites. For example we use Google Analytics to enable us to identify which pages you find useful and which you do not. This allows us to improve our navigation so you can find things easier in future.</p>
<p><strong>If you don&rsquo;t want to receive cookies</strong></p>
<p>If you would prefer not to receive cookies while browsing our site, you can set your browser so that it will not download cookies onto your computer. Doing so will still allow you to navigate through the majority of our site but possibly not all of it. If you wish to register on a password protected area of our website you will need to allow &ldquo;per-session&rdquo; cookies. These are temporarily used while you are visiting the site but deleted when you close your browser or log out.</p>
<p>To find out more information about cookies, visit:&nbsp;<a class="sys_16" onclick="void(window.open('http://www.allaboutcookies.org/',''));return false;" onkeypress="void(window.open('http://www.allaboutcookies.org/',''));return false;" href="http://www.allaboutcookies.org/">www.allaboutcookies.org</a></p>

</div>
</section>



<section class="cyan-title">
<div class="grid">

<h2>How we use Cookies</h2>
<p>We may use cookies to store information about your membership to parts of our site, or to enable you to log in to online resources. This might include blogs, portals, forums, or protected content. The information is stored to enable you to use these resources, and to remember your log in details between sessions. If you clear your cookies, you may need to login to these parts of the website each time you visit.</p>
<p><strong>Hobsons</strong></p>
<p>The University has a relationship with Hobsons Plc (Hobsons), any URLs beginning with</p>
<p><a class="sys_16" onclick="void(window.open('https://hull.hobsons.co.uk',''));return false;" onkeypress="void(window.open('https://hull.hobsons.co.uk',''));return false;" href="https://hull.hobsons.co.uk">hull.hobsons.co.uk</a>&nbsp;are hosted by Hobsons as part of this relationship:</p>
<p><a title="Hobsons privacy policy" class="sys_16" onclick="void(window.open('https://www.hobsons.com/emea/privacy-policy',''));return false;" onkeypress="void(window.open('https://www.hobsons.com/emea/privacy-policy',''));return false;" href="https://www.hobsons.com/emea/privacy-policy">A list of the Cookies used by Hobsons</a>&nbsp;for the processing of information on these pages can be found here.</p>
<p><strong>Google Analytics</strong></p>
<p>This website uses Google Analytics, a web analytics service provided by Google, Inc.. Google Analytics uses &lsquo;cookies&rsquo;, which are text files placed on your computer. Google Analytics allows the university to look at how visitors are using the site with the aim of improving visitor&rsquo;s experience.</p>
<h3>Subscription</h3>
<p>In some areas of our website, we ask you to provide information that will enable us to enhance your site visit or reply to you after your visit. This would include where you subscribe to our online newsletters or provide feedback to us through contact form or when you complete any online survey we may offer from time to time. When you do, so we ask you to give us your name, email address and other personal information that will be needed to supply the services to you. In relation to online newsletters you can &lsquo;unsubscribe' at any time. We may use your personal data for future email mailings, support, notification of new products or new versions, general correspondence regarding the products and correspondence which may relate to you. If you would rather not receive future marketing emails from us please inform us by&nbsp; <a title="Remove me from marketing emails" class="sys_16" href="mailto:marketing@hull.ac.uk">email</a>. Such use is strictly in accordance with the Data Protection Act 1998 and the Privacy and Electronic Communications (EC Directive) Regulations 2003. Every email from us comes with an easy to use unsubscribe instruction and you can be removed from our lists at any time, although this can take up to 30 days to effect. Your details will never be sent to any third party or group company of ours and we will only market to you the same kind of goods and services you purchased from us or made an enquiry to us about.</p>
<p>The information you provide will be kept confidential and will be used only to support your relationship with us. We do not disclose or sell your information outside the University except if we sell the whole or part of our business.</p>
<h3>Further information on data protection and personal privacy</h3>
<p>For further information from us on data protection and privacy contact:</p>
<p>The Data Protection Officer,<br />The University of Hull,<br />Cottingham Road,<br />Hull,<br />HU6 7RX</p>
<p><a title="Email the data protection officer" class="sys_16" href="mailto:dataprotection@hull.ac.uk">dataprotection@hull.ac.uk</a></p>
<p>Use of this Site is subject to our <a title="Terms and conditions" class="sys_0 sys_t0" href="/legal/terms-and-conditions.aspx">terms and conditions</a>.</p>
<h3>Warning</h3>
<p>Our Site provides links to other sites. We are not responsible for their data protection policies. You should check their privacy policies if you are concerned.</p>

</div>
</section>


</article>
<footer>
<div class="container">
<div class="staff">
<h5>Staff and students</h5>
<ul>
<li><a title="Schools and Faculties" class="sys_0 sys_t0" href="/faculties/faculties.aspx">Faculties</a></li>
<li><a title="Check your email" class="sys_16" href="https://mail.hull.ac.uk">Mail</a></li>
<li><a title="Library Services" class="sys_16" href="/choose-hull/study-at-hull/library/index.aspx">Library</a></li>
<li><a title="MyAdmin" class="sys_16" href="https://myadmin.hull.ac.uk">MyAdmin</a></li>
<li><a title="Canvas" class="sys_16" href="https://canvas.hull.ac.uk">Canvas</a></li>
<li><a title="Student services centre" class="sys_0 sys_t0" href="/choose-hull/student-life/student-support/where-to-get-help/central-hub.aspx">Central Hub</a></li>
<li><a title="Your Timetable" class="sys_16" href="https://timetables.hull.ac.uk">Timetables</a></li>
<li><a title="SharePoint" class="sys_16" href="https://share.hull.ac.uk">SharePoint</a></li>
<li><a title="Information and Communication Technology Department" class="sys_16" href="/ict/">ICTD</a></li>
<li><a title="Online Order and Payment Portal" class="sys_16" href="https://shop.hull.ac.uk">Online Order and Payment Portal</a></li>
</ul>
</div>
<div class="contact">
<h5>Contact</h5>
<ul>
<li><a title="For general enquiries, call us on +44 (0)1482 346311" class="sys_16" href="tel:+441482346311">+44 (0)1482 346311</a></li>
</ul>
</div>
<div class="find">
<h5>Find us</h5>
<address itemprop="address" itemtype="http://schema.org/PostalAddress">
<p>University of Hull</p>
<ul>
<li><span itemprop="addressLocality">Hull</span>, <span itemprop="addressCountry">UK</span></li>
<li itemprop="postalCode">HU6 7RX</li>
</ul>
</address>
<ul>
<li><a title="How to find us" class="sys_0 sys_t0" href="/choose-hull/study-at-hull/visit-us/how-to-find-us.aspx">Getting here</a></li>
<li><a title="Download the campus map" class="sys_21" href="/editor-assets/docs/campus-map.pdf">Campus map</a></li>
</ul>
</div>
<div class="map"></div>
<div class="social">
<ul>
<li><a title="Visit us on Facebook" target="_blank" rel="noopener" class="sys_16" href="https://i.hull.ac.uk/get">


<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="18 18 50 50">
    <defs>
        <style>.cls-1{fill:#fff;}.cls-2{fill:none;}</style>
    </defs>
    <title>University of Hull</title>
    <path class="cls-1" d="M33.16,53.55a2,2,0,1,1,0-4.06H51.91a2,2,0,1,1,0,4.06H33.16m3.1,6.71H48.81a2,2,0,1,0,0-4.06H36.26a2,2,0,1,0,0,4.06M39.34,67h6.4a2,2,0,1,0,0-4.06h-6.4a2,2,0,1,0,0,4.06M31.87,46.83s-2.92-2.45-.48-8.2a9.89,9.89,0,0,0,6,3.86s1.6-6.12,5.7-6.92c0,0-1.47,3.77,2.44,7,0,0,3.4-4.81,7.68-4.76,0,0-2.36,2.45-1,4.71,0,0,1.27,2.65-.24,4.29a8,8,0,0,0,4.67-1.7s2.94-1.7,1.56-8.06c0,0-.7-4.34,6.13-5.9A17.1,17.1,0,0,0,48.6,33.4s-3.78-5.56,4-10.09c0,0-13-1.13-18.72,11.36,0,0-4.8-2.36-1.65-8.35A13.33,13.33,0,0,0,24.09,40.1s.2,6.73,7.78,6.73" transform="translate(-1.36)"/>
</svg>
</a></li>
<li><a title="Visit us on Facebook" target="_blank" rel="noopener" class="sys_16" href="https://www.facebook.com/UniversityOfHull"><i class="fab fa-facebook-f">&nbsp;</i></a></li>
<li><a title="Visit us on Twitter" target="_blank" rel="noopener" class="sys_16" href="https://twitter.com/UniOfHull"><i class="fab fa-twitter">&nbsp;</i></a></li>
<li><a title="Visit us on Snapchat" target="_blank" rel="noopener" class="sys_16" href="https://www.snapchat.com/add/uniofhull"><i class="fab fa-snapchat-ghost">&nbsp;</i></a></li>
<li><a title="Visit our YouTube channel" target="_blank" rel="noopener" class="sys_16" href="https://www.youtube.com/user/marcomshull"><i class="fab fa-youtube">&nbsp;</i></a></li>
<li><a title="Visit our Flickr feed" target="_blank" rel="noopener" class="sys_16" href="https://www.flickr.com/photos/universityofhull"><i class="fab fa-flickr">&nbsp;</i></a></li>
<li><a title="Visit us on LinkedIn" target="_blank" rel="noopener" class="sys_16" href="https://www.linkedin.com/edu/the-university-of-hull-12668"><i class="fab fa-linkedin-in">&nbsp;</i></a></li>
<li><a title="View our Instagram page" target="_blank" rel="noopener" class="sys_16" href="https://www.instagram.com/universityofhull/"><i class="fab fa-instagram">&nbsp;</i></a></li>
</ul>
</div>
<div class="copy"><span>&copy;</span> University of Hull, 2019</div>
<div class="legal">
<ul>
<li><a title="accessibility" class="sys_0 sys_t0" href="/legal/accessibility.aspx">Accessibility</a></li>
<li><a title="freedom-of-information" class="sys_0 sys_t0" href="/legal/freedom-of-information.aspx">Freedom of information</a></li>
<li><a title="model-publication-scheme" class="sys_0 sys_t0" href="/legal/model-publication-scheme.aspx">Model Publication Scheme</a></li>
<li><a title="modern-slavery-act" class="sys_0 sys_t0" href="/legal/modern-slavery-act.aspx">Modern Slavery</a></li>
<li><a title="privacy-and-cookies" class="sys_0 sys_t0" href="/legal/privacy-and-cookies.aspx">Privacy and cookies</a></li>
<li><a title="terms-and-conditions" class="sys_0 sys_t0" href="/legal/terms-and-conditions.aspx">Website terms and conditions</a></li>
</ul>
</div>
</div>
</footer>



			</main>
			<a href="#top" class="cd-top">Top</a>

			<script src="/site-elements/js/site.js">

			</script><noscript><p>Browser does not support script.</p></noscript>

		
<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="335FF928" />
	<input type="hidden" name="__VIEWSTATEENCRYPTED" id="__VIEWSTATEENCRYPTED" value="" />
</div>
		<script  type="text/javascript" src="/site-elements/js/pages/generic.js?version=37320&amp;build=113133"></script><noscript><p>Browser does not support script.</p></noscript>
</form>

	</body>

	</html>