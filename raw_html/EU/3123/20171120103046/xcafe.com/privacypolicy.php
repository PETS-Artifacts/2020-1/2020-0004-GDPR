<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Privacy Policy | xCafe.com</title>
        	<meta name="RATING" content="RTA-5042-1996-1400-1577-RTA" />
    <meta content="origin" id="mref" name="referrer">
    <link rel="icon" type="image/png" href="/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">

<script>
  var _gaq = _gaq || [];
 if (window.location.href.search('promoid=') != -1) {
  var id = /promoid=([^&]*)/.exec(window.location.href)[1];
  _gaq.push(['_setCustomVar', 1, 'promoid', id, 2]);
 }
  _gaq.push(['_setCustomVar', 2, 'type', 'vids', 2]);
  _gaq.push(['_setVisitorCookieTimeout', 86400000]);
  _gaq.push(['_setCampaignCookieTimeout', 86400000]);

  _gaq.push(['_setAccount', 'UA-52478545-1']);
  _gaq.push(['_setDomainName', 'xcafe.com']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

    window.addEventListener("error", function (e, url) {
        var eleArray = ["IMG", "SCRIPT", "LINK"];
        var ele = e.target;
        if(eleArray.indexOf(ele.tagName) != -1){
            var url = ele.tagName == "LINK" ? ele.href: ele.src;
            _gaq.push(['_trackEvent', 'LoadError', url]);
        }
    }, true);
</script>
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="https://xcafe.com/styles/main.min.css" rel="stylesheet" type="text/css"/>
    <script src="/js/jquery.1.11.0.min.js"></script>
    		<link href="https://xcafe.com/rss/" rel="alternate" type="application/rss+xml"/>
		        
	<script type="text/javascript" src="https://xcafe.com/js/KernelTeamVideoSharingSystem.js?v=3.9.0"></script>
    
    <script type="text/javascript">
var SmartclickConfig = SmartclickConfig || {ads:[]};
if($.browser.mobile) {
    SmartclickConfig.ads.push({
        smartclick_key:"f135797adb519f9fac012b8401173b66",
        smartclick_channel:"",
        smartclick_code_format:"ads-async.js",
        smartclick_ads_host:"//c.smartclick.net",
        smartclick_click:"",
        smartclick_custom_params:{},
        smartclick_target_id:"smartclick-f135797adb519f9fac012b8401173b66-750x300"
    });
    SmartclickConfig.ads.push({
        smartclick_key:"553310ce6695e9630bae343292476c1c",
        smartclick_channel:"",
        smartclick_code_format:"ads",
        smartclick_ads_host:"//c.smartclick.net",
        smartclick_click:"",
        smartclick_custom_params:{},
        smartclick_width:"300",
        smartclick_height:"100",
        smartclick_target_id:"smartclick-553310ce6695e9630bae343292476c1c-300x100"
    });
    SmartclickConfig.ads.push({
        smartclick_key:"5918a344951ea57c4928a27692cad541",
        smartclick_channel:"",
        smartclick_code_format:"ads",
        smartclick_ads_host:"//c.smartclick.net",
        smartclick_click:"",
        smartclick_custom_params:{},
        smartclick_width:"300",
        smartclick_height:"250",
        smartclick_target_id:"smartclick-5918a344951ea57c4928a27692cad541-300x250"
    });
    SmartclickConfig.ads.push({
        smartclick_key:"bb4b8d85da91f00726a3a074b737eafb",
        smartclick_channel:"",
        smartclick_code_format:"ads",
        smartclick_ads_host:"//c.smartclick.net",
        smartclick_click:"",
        smartclick_custom_params:{},
        smartclick_width:"300",
        smartclick_height:"100",
        smartclick_target_id:"smartclick-bb4b8d85da91f00726a3a074b737eafb-300x100"
    });
} else {
    SmartclickConfig.ads.push({
      smartclick_key:"3820740aeacb526f7f0061d2c181fba9",
      smartclick_channel: "",
      smartclick_code_format:"ads-async.js",
      smartclick_click:"",
      smartclick_custom_params:{},
      smartclick_target_id:"smartclick-3820740aeacb526f7f0061d2c181fba9-750x300"
    });
}
(function () {
  var sc = document.createElement("script");
  sc.type = "text/javascript";
  sc.async = true;
  sc.src = (location.protocol == "https:" ? "https:" : "http:") + "//c.smartclick.net\/js\/show_ads_smartclick.js";
  var s = document.getElementsByTagName("script")[0];
  s.parentNode.insertBefore(sc, s);
})();
</script>
<script src="/js/html5shiv.min.js"></script>
</head>
<body>
    <div id="smartclick-f135797adb519f9fac012b8401173b66-750x300"></div><div id="smartclick-3820740aeacb526f7f0061d2c181fba9-750x300"></div>
	<div class="wrapper">
        <div class="fake_header"></div>
        <header class="container header cf no_pop">
            <div class="topmenu">
                <div class="container">
                    <ul class="line-menu">
                    	                        <li><a href="https://xcafe.com/signup.php">Sign Up</a></li>
                        <li><a href="https://xcafe.com/login.php">Login</a></li>
                                            </ul>
                </div>
            </div>
            <div class="container inheader">
                <div class="main-menu pull-right">
                    <ul class="icon-menu">
                                                <li class="invisible half_block"><a href="https://xcafe.com/signup.php"><i class="fa fa-fw fa-key"></i> Sign Up</a></li>
                        <li class="invisible half_block"><a href="https://xcafe.com/login.php"><i class="fa fa-fw fa-sign-in"></i>Login</a></li>
                                                <li class="invisible divider"></li>
                        <li class="item7 has-sub-menu"><a href="https://xcafe.com/videos/"><i class="fa fa-fw fa-video-camera"></i> Porn Videos <i class="fa fa-caret-down"></i></a>
                            <ul class="sub-menu">
                                <li><a href="/latest-updates/"><i class="fa fa-fire fa-fw"></i> NEWEST PORN</a></li>
                                <li><a href="/most-popular/"><i class="fa fa-signal fa-fw"></i> POPULAR VIDEOS</a></li>
                                <li><a href="/top-rated/"><i class="fa fa-thumbs-up fa-fw"></i> TOP RATED VIDEOS</a></li>
                            </ul>
                        </li>
                        <li class="item4"><a href="https://xcafe.com/categories/"><i class="fa fa-fw fa-heart"></i> Categories</a></li>
                        <li class="item5"><a href="https://xcafe.com/models/"><i class="fa fa-fw fa-female"></i> Models</a></li>
                        <li class="item6"><a href="https://xcafe.com/channels/"><i class="fa fa-fw fa-archive"></i> Channels</a></li>
                    </ul>
                </div>
                <div class="logo pull-left">
                    <a href="/"><img src="/images/logo.png" alt="xCafe.com"></a>
                </div>
                <div class="search">
                    <form action="https://xcafe.com/videos/">
                        <input type="text" class="s_text" name="q" autocomplete="off">
                        <input type="submit" value="OK" class="s_submit">
                        <i data-toggle-class=".search" class="fa fa-times invisible"></i>
                    </form>
                    <div class="search_error"></div>
                </div>
                <div class="search_holder invisible" data-toggle-class=".search"><i class="fa fa-search"></i></div>
                <div class="menu_holder invisible" data-toggle-class=".main-menu"><i class="fa fa-list"></i></div>
            </div>
        </header>
        <div class="mobile-version ad topad"><div class='adinner'><div id='smartclick-553310ce6695e9630bae343292476c1c-300x100'></div></div></div>
        <div class="content container"><div class="halfpage">
	<h1 class="block_title clear-left">Privacy Policy</h1>
	<div class="text_content">
<p><b>Updated Dec 17, 2012</b></p>

<p>Pursuant to our <a href="https://xcafe.com/terms.php">Terms of Use</a>, this document describes how we treat personal information related to your use of xCafe.com (the “Website”), including information you provide when using it.</p>

<p>We expressly and strictly limit use of the Website to adults over 18 years of age or the age of majority in the individual’s jurisdiction, whichever is greater. Anyone under this age is strictly forbidden from using the Website. We do not knowingly seek or collect any personal information or data from persons who have not attained this age.</p>

<p><b>Data Collected</b></p>

<p><i>Browsing the Website.</i> You can watch videos and access other parts of the Website without having an account, in which case only your IP address, country of origin and other non-personal information about your computer or device (such as web requests, browser type, browser language, referring URL, operating system and date and time of requests) will be recorded for log file information, aggregated traffic information and in the event that there is any misappropriation of information and/or content.</p>

<p><i>Registering on the Website.</i> Registration of an account is required for uploading videos and accessing other features on the Website. We require the following personal information at the time of registration: your username and password (which you generate) and e-mail address.  You may also voluntarily provide your age, relationship status and gender. Your IP address is recorded automatically. Your username, age, relationship status and gender (if provided) will become publicly accessible on the Website.</p>

<p><i>Usage Information.</i> We may record information about your usage of the Website such as your video subscriptions, which users you communicate with, the videos you watch, the frequency and size of data transfers and other statistics. If you have registered and are logged in, we may associate that information with your account.</p>

<p><i>Uploaded Content.</i> Any personal information or content you voluntarily disclose online, including videos, comments and your profile page, becomes publicly available and may be collected and used by others.</p>

<p><i>Correspondences.</i> We may keep a record of any correspondence between you and us.</p>

<p><i>Cookies.</i> When you use the Website, we may send cookies to your computer to uniquely identify your browser session. We may use both session cookies and persistent cookies.</p>

<p><b>Data Usage</b></p>

<p>We may use your information to provide you with certain features and to create a personalized experience on the Website. We may also use that information to operate, maintain and improve features and functionality of the Website.</p>

<p>We will not use your e-mail address or other personal information to send commercial or marketing messages without your consent. We may use your e-mail without further consent for non-marketing or administrative purposes.</p>

<p>We use cookies, web beacons and other information to store information so that you will not have to re-enter it on future visits, provide personalized content and information, monitor the effectiveness of the Website and monitor aggregate metrics such as the number of visitors and page views (including for use in monitoring visitors from affiliates). They may also be used to provide targeted advertising based on your country of origin and other personal information.</p>

<p>We may aggregate your personal information with personal information of other members and users, and disclose such information to advertisers and other third-parties for marketing and promotional purposes.</p>

<p>Your username may be displayed to other users alongside content you upload, including videos, comments and messages, and content you otherwise interact with, including “favoriting.”</p>

<p>Any videos that you submit may be redistributed through the internet and other media channels and may be viewed by the general public.</p>

<p><b>Disclosures of Information</b></p>

<p>We may be required to release certain data to comply with legal obligations or in order to enforce our Terms of Use and other agreements. We may also release certain data to protect the rights, property or safety of us, our users and others. This includes providing information to other companies or organizations like the police or governmental authorities for the purposes of protection against or prosecution of any illegal activity, whether or not it is identified in the Terms of Use.</p>

<p>If you upload any illegal material to the Website, we may forward all available information to relevant authorities, including respective copyright owners, without any notice to you.</p>

<p><b>Miscellaneous</b></p>

<p>If you have an account on the Website and have a password giving you access to certain parts of the Website, you are responsible for keeping the password confidential. Anyone else with your password may access your account and other personal information.</p>

<p>While we use commercially reasonable physical, managerial and technical safeguards to secure your information, the transmission of information via the internet is not completely secure and we cannot ensure or warrant the security of any information or content you transmit to us. Any information or content you transmit to us is done at your own risk.</p>

<p>You are entitled to access, correct or delete your information on the Website and may request us to do so by contacting us at <a href="mailto:mail@xCafe.com">mail@xCafe.com</a></p>




</div>
</div>


</div>
<div class="footer-block"><div class='ad mobile-version'><div class='adinner'><div id='smartclick-5918a344951ea57c4928a27692cad541-300x250'></div></div></div><div class="desktop-bnr desktop-version"></div></div>
<footer class="container footer">
    <ul class="footer-menu">
        <li><a href="https://xcafe.com/support.php">Support</a></li>
        <li><a href="https://xcafe.com/dmca.php">DMCA</a></li>
        <li><a href="https://xcafe.com/terms.php">Terms</a></li>
        <li><a href="https://xcafe.com/privacypolicy.php">Privacy Policy</a></li>
        <li><a href="https://xcafe.com/2257.php">18 U.S.C. § 2257 STATEMENT</a></li>
        <li><a href="http://anycash.com" target="_blank" rel="nofollow">Webmasters</a></li>
    </ul>
    <div class="container">
        <div class="footer-logo"><a href="/"><img src="/images/logo_footer.png" alt="Quality Porn Videos"></a></div>
        <div class="footer-right">
            <p>2017 xcafe.COM ALL RIGHTS RESERVED</p>
            <p>Users are prohibited from posting any material depicting individuals under the age of 18</p>
        </div>
    </div>
</footer>
</div>
<script src="/js/masonry.min.js"></script>
<script src="/js/jquery.qtip2.min.js"></script>
<script src="/js/jquery.smartModal.min.js"></script>
<script src="/js/scripts.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<script>
document.write('<scr'+'ipt src="ht'+'tps://fed736bc9342.yourlustmedia.com/i/advertisement.php?r='+(new Date().getTime() + 1)+'"></'+'scr'+'ipt>');
</script>
<script type="text/javascript">
var mobile = $.browser.mobile ? 2 : 1;
typeof yl !== "undefined" || typeof iiiiiiii !== "undefined" || document.write('<scr'+'ipt src="ht'+'tps://fed736bc9342.yourlustmedia.com/i/s_.php?t=xc&m='+mobile+'"></'+'scr'+'ipt>');
</script>

<!-- LQXSM9ZZU8GiQoZw -->
</body>
</html>