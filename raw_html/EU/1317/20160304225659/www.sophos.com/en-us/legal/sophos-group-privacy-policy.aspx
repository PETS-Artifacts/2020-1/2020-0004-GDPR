
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="headTag"><!-- Global meta tags -->
<!-- End global meta tags --><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><meta name="viewport" content="width=1060" />
<link href="/fairfax/sophosfavicon.ico" rel="shortcut icon" type="image/x-icon" />
<!-- Global Script Tags -->
<script src="/scripts/bundles/global.js?v=1" type="text/javascript"></script>
<!-- End Global Script Tags -->

<!--  Global Styles Box -->
<link href="/en-us/medialibrary/Styles/uitotop.css" type="text/css" rel="stylesheet"></link>
<!-- End Global Styles --><script type="text/javascript">if (typeof readCampaignAndWriteToCookie== 'function') {   readCampaignAndWriteToCookie(); }function GetCookie(k){return(document.cookie.match('(^|; )'+k+'=([^;]*)')||0)[2]}var campaignId = GetCookie('CampaignID');var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-737537-1'],['_setDomainName', '.sophos.com'],['_setAllowLinker', true],['_setAllowHash', false],['_setCustomVar', 4, 'CampaignID', campaignId, 3],['_trackPageview'],['o._setAccount', 'UA-737537-18'],['o._setDomainName', '.sophos.com'],['o._setAllowLinker', true],['o._setAllowHash', false],['o._setCustomVar', 4, 'CampaignID', campaignId, 3],['o._trackPageview']);(function() {var ga = document.createElement('script');ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();</script><script type="text/javascript" src="/en-us/medialibrary/scripts/tracking/nonhtmltracking.js"></script><!-- Start Visual Website Optimizer Code --><script type='text/javascript'>var _vis_opt_account_id = 25349;var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');document.write('<s' + 'cript src="' + _vis_opt_protocol + 'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a='+_vis_opt_account_id+'&url='+encodeURIComponent(document.URL)+'&random='+Math.random()+'" type="text/javascript">' + '<\/s' + 'cript>');</script><script type='text/javascript'>if(typeof(_vis_opt_settings_loaded) == "boolean") { document.write('<s' + 'cript src="' + _vis_opt_protocol + 'd5phz18u4wuww.cloudfront.net/vis_opt.js" type="text/javascript">' + '<\/s' + 'cript>'); }</script><script type='text/javascript'>if(typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {        _vis_opt_top_initialize(); vwo_$(document).ready(function() { _vis_opt_bottom_initialize(); });}</script><!-- End Visual Website Optimizer Code --><script src="/en-us/medialibrary/Scripts/Tracking/gainjectmin.js?v=1" type="text/javascript"></script><script type="text/javascript">/* Instantiate and execute */$(function () {    window.sophosGaGuidStore.processGaToken({        useGoogleVid: true,        gaScope: 1,        gaNamespace: 'o',        gaAccount: 'UA-737537-18'    });});</script>
    <!-- Head scripts Start -->
    
    <!-- Head scripts End -->
    <title>
	Sophos Group Privacy Policy - Legal
</title><link href="/en-us/medialibrary/SophosNext/Styles/aoverride.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/buttons.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/colorbox.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/footer.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/footer2015.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/forms.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/header.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/ie.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/jquery-ui.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/livechat.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/normalize.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/retina.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/sophos.css?la=en" type="text/css" rel="stylesheet" /><link href="/en-us/medialibrary/SophosNext/Styles/typography.css?la=en" type="text/css" rel="stylesheet" /><!-- current language iso code for locale specific js --><script type="text/javascript">function getSitecoreCurrentLocale() { var locale = 'en-US'; if (locale == '') { return 'en'; } return locale; }</script></head>
<body id="Body">
    
    <form name="form1" method="post" action="/en-us/legal/sophos-group-privacy-policy.aspx" id="form1">
<div>
<input type="hidden" name="ToolkitScriptManager1_HiddenField" id="ToolkitScriptManager1_HiddenField" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__LASTFOCUS" id="__LASTFOCUS" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTQwMTQ3MzEyMA8WAh4TVmFsaWRhdGVSZXF1ZXN0TW9kZQIBFgQCAQ9kFgpmDxYCHgRUZXh0BTc8IS0tIEdsb2JhbCBtZXRhIHRhZ3MgLS0+CjwhLS0gRW5kIGdsb2JhbCBtZXRhIHRhZ3MgLS0+ZAIHDxYCHwEFngI8IS0tIEdsb2JhbCBTY3JpcHQgVGFncyAtLT4KPHNjcmlwdCBzcmM9Ii9zY3JpcHRzL2J1bmRsZXMvZ2xvYmFsLmpzP3Y9MSIgdHlwZT0idGV4dC9qYXZhc2NyaXB0Ij48L3NjcmlwdD4KPCEtLSBFbmQgR2xvYmFsIFNjcmlwdCBUYWdzIC0tPgoKPCEtLSAgR2xvYmFsIFN0eWxlcyBCb3ggLS0+CjxsaW5rIGhyZWY9Ii9lbi11cy9tZWRpYWxpYnJhcnkvU3R5bGVzL3VpdG90b3AuY3NzIiB0eXBlPSJ0ZXh0L2NzcyIgcmVsPSJzdHlsZXNoZWV0Ij48L2xpbms+CjwhLS0gRW5kIEdsb2JhbCBTdHlsZXMgLS0+ZAIJDxYCHwFlZAIKDxYCHwFlZAIQDxYCHgdWaXNpYmxlaGQCAw9kFggCAQ8WAh8BZWQCAxBkZBYGAg0PZBYCZg9kFhACAQ8UKwACDxYEHgtfIURhdGFCb3VuZGceC18hSXRlbUNvdW50AgVkZBYCZg9kFgoCAQ9kFgRmDxUBAGQCAQ8PFgQeCENzc0NsYXNzBQxuYXYtcHJvZHVjdHMeBF8hU0ICAmQWAmYQZGQWAmYQFgQeBUZpZWxkBQRUZXh0HglTSXRlbUd1aWQoKVhTeXN0ZW0uR3VpZCwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5JGI2Nzk2ZGVmLTU2YTYtNDVmNi1hNTNhLThmZGVkMTE5ZGRmYmRkAgIPZBYEZg8VAQBkAgEPDxYEHwUFCG5hdi1sYWJzHwYCAmQWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDgxZjRjOGYxLTQ5N2ItNDI4Yi1hOTRlLTk1ZGI0NDk5NGU2YmRkAgMPZBYEZg8VAQBkAgEPDxYEHwUFDG5hdi1wYXJ0bmVycx8GAgJkFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQwNGY4MDZhYi0xNzYwLTRhYzEtYWNkZC01Mjk1ZmFmNDI3ZjlkZAIED2QWBGYPFQEAZAIBDw8WBB8FBQtuYXYtY29tcGFueR8GAgJkFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCRhNTRkZjExMi05YTFhLTQ3MzMtYmMyNC1iOWRlN2RjNThkZDBkZAIFD2QWBGYPFQEAZAIBDw8WBB8FBQtuYXYtc3VwcG9ydB8GAgJkFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCRkNTRhMTAyMC03ZTRjLTQ3NjUtYTQ5Ni03ZTgzMDMyMjhhMmRkZAICD2QWAmYPDxYCHgtOYXZpZ2F0ZVVybAUaL2VuLXVzL215c29waG9zL2xvZ2luLmFzcHgWAh4FY2xhc3MFBnNpZ25pbhYCZhBkZBYCZhAWBB8HBQRUZXJtHwgoKwQkMTBkOTZiMjAtNWUwYy00MDZjLWFiYjUtZTQ2MjFhNTgyZmVjZGQCAw9kFgICARBkZBYCZhAWBB8HBQRUZXJtHwgoKwQkNTBkZDc0ZGMtODhlZC00ODA0LTkxMTgtOWFmZDViMDdiODUyZGQCBA9kFgICARBkZBYCZhAWBB8HBQRUZXJtHwgoKwQkNDQ0ODQ4NWMtYzVhOC00YzM3LTk0NTUtZTRjMjlhNTU5YTViZGQCBQ8UKwACDxYEHwNnHwQCBWRkFgJmD2QWCgIBD2QWBAIBDxQrAAIPFgQfA2cfBAIDZGQWAmYPZBYGAgEPZBYCAgEQZGQWAmYQFgQfBwUFVGl0bGUfCCgrBCQ3NmY1NDM2NS1hOGM0LTRhZmEtOWM1Mi05YWEyNGY2YmEzYjJkZAICD2QWAgIBEGRkFgJmEBYEHwcFBVRpdGxlHwgoKwQkYTAxZDY3ODktZDkwZC00NjEyLTg2OWItZTg3ZDJmODVmZWMzZGQCAw9kFgICARBkZBYCZhAWBB8HBQVUaXRsZR8IKCsEJGYwZDAyZTUxLWI0YzEtNDA4ZC04MGExLTc3ZWYxMjhmZjNlOWRkAgMPFCsAAg8WBB8DZx8EAgNkZBYCZg9kFgYCAQ9kFgICAQ8UKwACDxYEHwNnHwQCBWRkFgJmD2QWCgIBD2QWBGYPFQFDL2VuLXVzL21lZGlhbGlicmFyeS9JbWFnZXMvUHJvZHVjdHMvSWNvbnMvaWNvbi1uZ2Z3LXNtYWxsLnBuZz9sYT1lbmQCAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQ4YWU2MDU1NC01M2Q3LTQ2NTEtYTE2NS1mYWZkM2E4OWMyMWNkZAICD2QWBGYPFQFCL2VuLXVzL21lZGlhbGlicmFyeS9JbWFnZXMvUHJvZHVjdHMvSWNvbnMvaWNvbi11dG0tc21hbGwucG5nP2xhPWVuZAIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDAyMjBiMjU1LTdjYmEtNGZjNi05MjY3LTQ0NGMzOGJhNThkOWRkAgMPZBYEZg8VAUkvZW4tdXMvbWVkaWFsaWJyYXJ5L0ltYWdlcy9Qcm9kdWN0cy9JY29ucy9pY29uLXNlY3VyZXdpZmktc21hbGwucG5nP2xhPWVuZAIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDRiNzNjYjQ2LWJkNWQtNDQyNS1iZTBkLTQwN2QxZTVlNzA0N2RkAgQPZBYEZg8VAUgvZW4tdXMvbWVkaWFsaWJyYXJ5L0ltYWdlcy9Qcm9kdWN0cy9JY29ucy9pY29uLXNlY3VyZXdlYi1zbWFsbC5wbmc/bGE9ZW5kAgEPZBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkNzgyMjQ1Y2MtODI5My00YTg1LWEyMGMtOGYxMWQxMzYxYWJiZGQCBQ9kFgRmDxUBSi9lbi11cy9tZWRpYWxpYnJhcnkvSW1hZ2VzL1Byb2R1Y3RzL0ljb25zL2ljb24tc2VjdXJlZW1haWwtc21hbGwucG5nP2xhPWVuZAIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGExMjE3NTg2LTE2YjktNGQwZS1hMjg0LWIyOTM1NDc3MDIyZGRkAgIPZBYCAgEPFCsAAg8WBB8DZx8EAgVkZBYCZg9kFgoCAQ9kFgRmDxUBQi9lbi11cy9tZWRpYWxpYnJhcnkvSW1hZ2VzL1Byb2R1Y3RzL0ljb25zL2ljb24tZXVwLXNtYWxsLnBuZz9sYT1lbmQCAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQzZWYxMjNkYi1kNGNkLTQyOGEtYTFlNC04YjBiMDY5MjFhMDFkZAICD2QWBGYPFQFML2VuLXVzL21lZGlhbGlicmFyeS9JbWFnZXMvUHJvZHVjdHMvSWNvbnMvRW5kcG9pbnRfQVZfdGh1bWJuYWlsXzA0LnBuZz9sYT1lbmQCAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQyYTkyODAzMy1lYWE2LTQzNTgtYmRkNS04NmFhN2JhYmIzY2ZkZAIDD2QWBGYPFQE4L2VuLXVzL21lZGlhbGlicmFyeS9JbWFnZXMvTmF2aWdhdGlvbi9DbG91ZC0wMi5wbmc/bGE9ZW5kAgEPZBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkZGFiMTc3MTAtNWQ1YS00NWZhLTk4MmQtZGQxM2M0ZGNkOTM1ZGQCBA9kFgRmDxUBTC9lbi11cy9tZWRpYWxpYnJhcnkvSW1hZ2VzL1Byb2R1Y3RzL0ljb25zL2ljb24tbW9iaWxlY29udHJvbC1zbWFsbC5wbmc/bGE9ZW5kAgEPZBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkYjY2ZjI4ZWMtY2Y3ZC00NmUzLWIyOTUtNDEyZjYwZmNkZjRlZGQCBQ9kFgRmDxUBSS9lbi11cy9tZWRpYWxpYnJhcnkvSW1hZ2VzL1Byb2R1Y3RzL0ljb25zL2ljb24tZW5jcnlwdGlvbi1zbWFsbC5wbmc/bGE9ZW5kAgEPZBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkMjEyNWFhOTUtYTQyMi00YTVlLWI4ZWEtMTMyYzU4ZmJjZjZmZGQCAw9kFgICAQ8UKwACDxYEHwNnHwQCBWRkFgJmD2QWCgIBD2QWBGYPFQFGL2VuLXVzL21lZGlhbGlicmFyeS9JbWFnZXMvUHJvZHVjdHMvSWNvbnMvdlNoaWVsZC1pY29uLXNtYWxsLnBuZz9sYT1lbmQCAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQxYzU0MGY2Yi1mYTZmLTRhN2QtOGQ2Mi0yZGU2ZTcwYWI3MDNkZAICD2QWBGYPFQFOL2VuLXVzL21lZGlhbGlicmFyeS9JbWFnZXMvUHJvZHVjdHMvSWNvbnMvaWNvbi1zZXJ2ZXJhbnRpdmlydXMtc21hbGwucG5nP2xhPWVuZAIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGRkMTEwZDEwLTIzNDYtNGE2MC1iYTJhLTI4YjYxMDY1NjY5MGRkAgMPZBYEZg8VAVYvZW4tdXMvbWVkaWFsaWJyYXJ5L1NvcGhvc05leHQvSW1hZ2VzL1Byb2R1Y3RzL1NoYXJlcG9pbnQvU2hhcmVwb2ludC1JY29uLXNtLnBuZz9sYT1lbmQCAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCRmNjIwNzRlMi04NTA5LTQ0NTYtOTU3MS1lYjJiMmI5MDZlYmZkZAIED2QWBGYPFQFNL2VuLXVzL21lZGlhbGlicmFyeS9JbWFnZXMvUHJvZHVjdHMvSWNvbnMvaWNvbi1uZXR3b3Jrc3RvcmFnZS1zbWFsbC5wbmc/bGE9ZW5kAgEPZBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkMjkzNjhhZjEtZjgxNy00ZTk5LWFjMGUtYjk5NWYwNjdjNjg2ZGQCBQ9kFgRmDxUBSi9lbi11cy9tZWRpYWxpYnJhcnkvSW1hZ2VzL1Byb2R1Y3RzL0ljb25zL2ljb24tcHVyZW1lc3NhZ2Utc21hbGwucG5nP2xhPWVuZAIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDg0MTZmY2E2LWIzNGItNDc1ZC05Y2I1LWFkZTc1ZjlmODZjYWRkAgIPZBYEAgEPFCsAAg8WBB8DZx8EAv////8PZGRkAgMPFCsAAg8WBB8DZx8EAv////8PZGRkAgMPZBYEAgEPFCsAAg8WBB8DZx8EAv////8PZGRkAgMPFCsAAg8WBB8DZx8EAv////8PZGRkAgQPZBYEAgEPFCsAAg8WBB8DZx8EAv////8PZGRkAgMPFCsAAg8WBB8DZx8EAv////8PZGRkAgUPZBYEAgEPFCsAAg8WBB8DZx8EAv////8PZGRkAgMPFCsAAg8WBB8DZx8EAv////8PZGRkAgYPFCsAAg8WBB8DZx8EAgJkZBYCZg9kFgoCARBkZBYCZhAWBB8HBQRUZXJtHwgoKwQkOWIwNDhmYzctZjI3Ni00NzMyLWI2MmItMzFiN2FhNzk5NmY0ZGQCAxBkZBYCZhAWBB8HBQRUZXJtHwgoKwQkZThkMjkwMmItZDEyYS00NTQyLTg1OTgtMmMwOGJmZjU2Zjc3ZGQCBRBkZBYCZhAWBB8HBQRUZXJtHwgoKwQkNDM5ZjRiZDQtNzM5My00OTc0LTk5NGMtYTc0MWIyNmJmM2ZmZGQCBw9kFgICAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQ5MjFlOWVhOS00ZDEwLTQ1Y2UtOWM3My1lNDFhYTQzMzQ4MjdkZAIID2QWAgIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGJjN2Q1MzZhLTA4YjgtNGIwZi1iMTc5LTdhZmM0NmEzOGIxNWRkAgcPFCsAAg8WBB8DZx8EAglkZBYCZg9kFhYCARBkZBYCZhAWBB8HBQRUZXJtHwgoKwQkNTBkZDc0ZGMtODhlZC00ODA0LTkxMTgtOWFmZDViMDdiODUyZGQCAxBkZBYCZhAWBB8HBQRUZXJtHwgoKwQkZmNlNzdkMzctMzUxMi00NzgxLWIzY2EtMGI0NjJjZjBlZDA5ZGQCBQ9kFgICAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCRjYmY2MWU3OS0yNjM2LTQwMmQtYmZkNi0wODAyZjQ4ODgzYjZkZAIGD2QWAgIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDIyNDFmYWU0LTgzMWUtNGMxMC04YmZkLTE3YTE5YmRhYjA5NmRkAgcPZBYCAgEPZBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkZmU1ZTNjZWEtNjliMy00YmZkLTk0MzItNWMxYzMzZGJiOTQ0ZGQCCA9kFgICAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQ3ZTAxYjFiMC02ZTcxLTQ1MDEtOTg2Yy03NjZhYzZmMGIyYmFkZAIJD2QWAgIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDg4ZmE5YTIwLWM5ZTEtNDQ5MC1hZGVjLWE0NTY5MjdiMjljNmRkAgoPZBYCAgEPZBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkZGVkNDc0ZDItZjc3NS00MDhjLTgzYjAtNzU5OWZhZWFmODIxZGQCCw9kFgICAQ9kFgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQ5Y2FlNzc4Ni1kYzE5LTRhZjktYjJhNC1hYTA3M2EyODgxZmNkZAIMD2QWAgIBD2QWAmYQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGRjMTNjMzBjLTRiMGItNDczNy04OGY2LWU0ZjU3ODcxNmViZWRkAg0PZBYCAgEPZBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkODE5N2M3OWYtYmZlMy00NTA1LTk1ZTktMzJiOWFjYzhjYzY5ZGQCCBAPFgQfCCgrBCQxZGVkODU1My0xYjNjLTQ2NGUtOGJjNy05Mzc5YmNhY2NhYTYfAmdkZBYCZg8UKwACDxYEHwNnHwQCAmRkFgJmD2QWBAIBD2QWBGYPFQMIc2VsZWN0ZWQAAGQCAQ8PFgYeBlRhcmdldGUfBWUfBgICFgIeBGhyZWYFES9lbi11cy9sZWdhbC5hc3B4FgJmEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQyNmQ3OTE3Ny04N2I5LTQyNjktOGQxZi0wMjAzOTA1NDBmMzRkZAICD2QWBGYPFQMAIiBzZWNvbmRhcnktbmF2LWljb24gc2VjdGlvbi1ib3JkZXJHL2VuLXVzL21lZGlhbGlicmFyeS9Tb3Bob3NOZXh0L0ltYWdlcy9OYXZpZ2F0aW9uL2ljb24tY29udGFjdC5wbmc/bGE9ZW5kAgEPDxYGHwtlHwVlHwYCAhYCHwwFLS9lbi11cy9jb21wYW55L2NvbnRhY3Qvbm9ydGhzb3V0aGFtZXJpY2EuYXNweBYCZhBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkYzM2MGEyYzItODEzMy00OWVjLTkwYWQtYWZmNmVjNjk5OTM4ZGQCDw9kFgJmD2QWAmYPZBYEAgEPZBYCZg9kFgRmDxYCHwJoZAICDxYCHwJoFgJmEA8WAh8CaGRkZAIDD2QWAmYPZBYCZg9kFgYCARBkZBYCZhAWBB8HBQZIZWFkZXIfCCgrBCQ2MzYyYjhlNC1jMDc4LTQ5MjUtYTUyMi1hMmRkM2QyMGE4ZTlkZAIDEGRkFgJmEBYEHwcFBEJvZHkfCCgrBCQ2MzYyYjhlNC1jMDc4LTQ5MjUtYTUyMi1hMmRkM2QyMGE4ZTlkZAIFD2QWBmYPZBYCZg9kFgJmD2QWAmYPZBYGAgEPFgIfAQWwATxzdHlsZT4KCmRpdiNub3RpY2V7CiAgYmFja2dyb3VuZDojRjFGMUYxOwogIGJvcmRlcjoxcHggc29saWQgI0RERERERDsKICBib3JkZXItcmFkaXVzOjVweDsKICBmb250LXNpemU6MTNweDsKICBsaW5lLWhlaWdodDoxLjQ7CiAgcGFkZGluZzoyMHB4OwptYXJnaW4tYm90dG9tOiAyMHB4Owp9Cjwvc3R5bGU+ZAIDDxYCHwFlZAIFEGRkFgJmEBYEHwcFBEhUTUwfCCgrBCRiYTgyNTMzMi1mYzY5LTQyOGQtOTBlMS1lMmE1YmNjOTQyYjVkZAIBD2QWAmYPZBYGAgEQZGQWAmYQFgQfBwUEVGVybR8IKCsEJDYwYTUxYWIyLTBmNGUtNGUwYy04NDBmLThjNTNjNjgyNmI0MWRkAgMQZGQWAmYQFgQfBwUEVGVybR8IKCsEJDEyN2E4OGZiLWUxMzUtNGJkOS05NmM1LWIxZGNhNjRiZTIzMmRkAgUPEGQPFhNmAgECAgIDAgQCBQIGAgcCCAIJAgoCCwIMAg0CDgIPAhACEQISFhMQBRlDb21wZXRpdGlvbiBSdWxlcyBHZW5lcmFsBSZ7QTg4RUU0MUMtQzJGNS00OUMwLUFFOUQtRjA2NkQxRjA5NjVEfWcQBSFTb3Bob3MgRW5kIFVzZXIgTGljZW5zZSBBZ3JlZW1lbnQFJns4M0FDMUVBNy05RDdCLTQzQTItQTlBNy0wNzAxQ0M3MUNGMUF9ZxAFGEhhcmR3YXJlIFdhcnJhbnR5IFBvbGljeQUme0FGOTRGQjk2LTZGRUUtNDI0NC04ODI1LTMxNkJFOTlCNTdFN31nEAUUTGljZW5zaW5nIEd1aWRlbGluZXMFJns5RTBBMjJCQy1GRUNELTQ1NDYtOUQ5Qy1GRjE5NzlGMjI5RTh9ZxAFKVNvcGhvcyBNYW5hZ2VkIFNlcnZpY2UgUHJvdmlkZXIgQWdyZWVtZW50BSZ7MkY3RTg3REQtMzY1Mi00QkE4LUEyREItQjVCQjc5OEMxQTRFfWcQBTNQYXJ0bmVyIChSZXNlbGxlcikgQXBwbGljYXRpb24gVGVybXMgYW5kIENvbmRpdGlvbnMFJntCNzZERTg4QS1FNEE0LTQ3MzEtOEY2Mi1GMTVFOEZGOEUzN0R9ZxAFIlNvcGhvcyBQYXJ0bmVyIEFncmVlbWVudCBSZXNlbGxlcnMFJnswRURDN0Q0OS04QzNGLTRCQjEtOEM0NS00MkU4MDQ0MkJFQTN9ZxAFJSBTb3Bob3MgVHJhaW5pbmcgVGVybXMgYW5kIENvbmRpdGlvbnMFJns1QTE1NUY3OC1FRDYxLTQyMzItQUVFMy1CNTAzQUEwMTI4MjV9ZxAFG1NvcGhvcyBDcmltZWEgR3VpZGFuY2UgTm90ZQUmezA5NDc2ODY3LTlGNUYtNDYzRi04NDAyLUQ2QUQxNzFFODQ1M31nEAUfU29waG9zIEx0ZCBFbmQgVXNlciBVbmRlcnRha2luZwUmezlGRjI2QURDLUNEOTItNEEwOS05NTJFLUVCMDZBMjA4NDhFNn1nEAUbU29waG9zIFdlYnNpdGUgVGVybXMgb2YgVXNlBSZ7QzE3NDg4RkUtMEU0MS00MUNCLTgzMjQtMTg3ODQwRUE3MDM5fWcQBTFTb3Bob3MgUHJvZmVzc2lvbmFsIFNlcnZpY2VzIFRlcm1zIGFuZCBDb25kaXRpb25zBSZ7OTVEQTBBMkItRTY1OS00QjVELUJBNDQtOTEzNjM2MDFBMjk2fWcQBRlTb3Bob3MgQ29va2llIEluZm9ybWF0aW9uBSZ7NzAzN0VDM0EtOENDNS00ODVBLThDMUItRjE5RkRBMDc3RDE1fWcQBSVFeHBvcnQgSW5mb3JtYXRpb24gb24gU29waG9zIFByb2R1Y3RzBSZ7RDdGQUIxNTItM0QzNS00REEzLTkxM0UtOTUyMUI2QkE0NTU3fWcQBRtTb3Bob3MgR3JvdXAgUHJpdmFjeSBQb2xpY3kFJns2MzYyQjhFNC1DMDc4LTQ5MjUtQTUyMi1BMkREM0QyMEE4RTl9ZxAFM1NvcGhvcyBHcm91cCBwbGMgQW50aS1jb3JydXB0aW9uIGFuZCBCcmliZXJ5IFBvbGljeQUme0UwQzBCOTVGLUQ1NUUtNEE1RS1BRUMxLUZDMjYwMzE2QUI3OH1nEAUmU29waG9zTGFicyBJbmZvcm1hdGlvbiBTZWN1cml0eSBQb2xpY3kFJnswQUI4RDYyQi0wMTQ3LTQ2MTAtQTI1RS1DMjlDRDA1N0Q4RTJ9ZxAFHEVtcGxveWVlIFBlcnNvbmFsIFVzZSBQb2xpY3kFJntFNkU4MjEwQy00ODVDLTQ1N0YtQTFFRi05MzhDNTUyRTU3NjZ9ZxAFHlNvcGhvcyBQcm9kdWN0IENsYXNzaWZpY2F0aW9ucwUmezA5RTQwRDFBLTY0NkMtNDE1MC1CMEM2LUI2RUI5NEM5MzA1OH1nFgFmZAICD2QWAmYPZBYCZhBkZBYCZhAWBB8HBQRIVE1MHwgoKwQkM2Q5YjZhMDgtZWM3OC00NmM0LTgxZjMtMTQ2ZGIwNmI3NjE4ZGQCEw9kFgJmD2QWDmYPFCsAAg8WBB8DZx8EAgZkZBYCZg9kFgwCAQ9kFgQCARBkZBYCZhAWBh8HBQVXaWR0aB8IKCsEJGVlOWJjYTMxLWMwNTktNDZkNi1iZmY1LWRkYzA2ZmZjZmE4NB4VRmllbGROb3RGb3VuZEludGVybmFsZ2RkAgUPFCsAAg8WBB8DZx8EAgNkZBYCZg9kFgYCAQ9kFgRmDxUBRC9lbi11cy9tZWRpYWxpYnJhcnkvU29waG9zTmV4dC9JbWFnZXMvU29jaWFsL2ZhY2Vib29rLWljb24ucG5nP2xhPWVuZAIBD2QWAgIBEGRkFgJmEBYEHwcFBFRleHQfCCgrBCRiZmEyMDFjNC1mMzE4LTRlMjMtYTk1OS03MTAzMDc5OTM4NzlkZAICD2QWBGYPFQFDL2VuLXVzL21lZGlhbGlicmFyeS9Tb3Bob3NOZXh0L0ltYWdlcy9Tb2NpYWwvdHdpdHRlci1pY29uLnBuZz9sYT1lbmQCAQ9kFgICARBkZBYCZhAWBB8HBQRUZXh0HwgoKwQkN2JiNTQzOTUtZDFkNy00ZjdiLThhYjYtNDE1MDQ2MWRlYzg5ZGQCAw9kFgRmDxUBQy9lbi11cy9tZWRpYWxpYnJhcnkvU29waG9zTmV4dC9JbWFnZXMvU29jaWFsL3lvdXR1YmUtaWNvbi5wbmc/bGE9ZW5kAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGMyYzdkZWRkLWUxZTUtNGQ4Ny05NWU4LTc0MThjOGU1ZjVmY2RkAgIPZBYEAgEQZGQWAmYQFgYfBwUFV2lkdGgfCCgrBCRhNzMzZTZhYi0xMDgxLTQxMmYtOWUwZC0wNzFmMzI5NTFhYzcfDWdkZAIFDxQrAAIPFgQfA2cfBAIGZGQWAmYPZBYMAgEPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGFhNTVhZGNkLTQwZjctNDQxMy05OTJjLTc2YWY2MDRjZDlkNmRkAgIPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDIwNTM5MmZiLWRmNzAtNGI0OC04MGNmLWRiNGFmMDg2YmUwMmRkAgMPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDQ3ODFlYTU3LWM1MzYtNDAyNi1hYTdkLTVlYTk4NGFkZWUxNmRkAgQPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDhhMTQ2NDdmLWE4NDMtNGY4My05N2I1LTViMzYyOTgyYzE1Y2RkAgUPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDA4NjM3MzY0LWQ4YmUtNGE1Zi04NmU5LTQ5NTRjM2U3NmMwY2RkAgYPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDBhMmVjODJmLTMzNWMtNDUxNS04MGMzLWUyMDExMzhhYjFmOGRkAgMPZBYEAgEQZGQWAmYQFgYfBwUFV2lkdGgfCCgrBCRlOTkyYjllNi01YTI4LTRkZjktYjNkOC0wZGE1MzExNThlN2IfDWdkZAIFDxQrAAIPFgQfA2cfBAIFZGQWAmYPZBYKAgEPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDJjZGRhNzY3LTkzNWUtNGNlOS1hMmQ2LTBjOTlkOGEyOWQ2MmRkAgIPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDA3ZWVkMGEzLTAwZWItNDQzYS1hZjRiLWMzYWVlZDViMjY1M2RkAgMPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGMxMjkyNzliLTM3ZTEtNDdiNC04YTYzLTllZTZiMGJmMDllOWRkAgQPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGVjZDQwZjA1LTA0NzQtNDc1Mi04OTRiLWQ4MzZlYTM2MGY3NGRkAgUPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDE0NzFiZmExLTJkOWEtNGUwMy1hYWM1LTQ2OTJkYWZiZWVjOWRkAgQPZBYEAgEQZGQWAmYQFgYfBwUFV2lkdGgfCCgrBCQzYTUzZjE4MC0yMDU2LTQwMDMtYjhlZS02MWZiZWIwMDFiYTcfDWdkZAIFDxQrAAIPFgQfA2cfBAIFZGQWAmYPZBYKAgEPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDRiY2MxODAyLTQyMDItNGQwZS1iZGNjLTNiYWMwMzZlYzI5M2RkAgIPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDE0YjFlNjM2LWIxNTktNDg3ZS05Y2Y3LTQ0MzczOWYxOGRjYmRkAgMPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDAwNTEzZGY5LTRlZTUtNDMwNS1hODRjLTE3NTQxMGNmY2FiNWRkAgQPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDYxNWExY2U4LWEyZjEtNDVmNi1hZWIwLTNiNmYwYmI5OTRlMWRkAgUPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDUxMTA2MmRlLTI5MTEtNDI4Ni05NjAwLWRiZDlhODkyMWMxOWRkAgUPZBYEAgEQZGQWAmYQFgYfBwUFV2lkdGgfCCgrBCQ1MDI3NWU1Yi0xNzI4LTQwNDgtOTQ5Ni0zODA2ZDhlOGExNDgfDWdkZAIFDxQrAAIPFgQfA2cfBAIFZGQWAmYPZBYKAgEPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGIwMDcyNTQyLTNmZGUtNDlhYi04NTZiLWMxY2JkZGRhZjZhYmRkAgIPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGQxNGRlMzkxLTllZTUtNDg0Ny1iZWFkLTc4M2YyNWZkZThmMGRkAgMPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDJkZjI1Nzg0LWQyNjUtNGVhMS1hNDBmLWM3NTViYTRkYzZjN2RkAgQPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDAzMzg5MDU5LTdkMjEtNDAwNy1hMmQ2LWQ0ZDNhY2E1NzhkYWRkAgUPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGY0Mzc1OWRkLWU4YzktNGY0OC1iMjJhLWIwYWU5MmJjNjVhZmRkAgYPZBYEAgEQZGQWAmYQFgYfBwUFV2lkdGgfCCgrBCRhM2Y5NmEwNC1jOTcwLTQ0NzgtYWNhNy1lOTRjOWZiODRiMDkfDWdkZAIFDxQrAAIPFgQfA2cfBAIGZGQWAmYPZBYMAgEPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDdkNDc1ZTlhLTdiZjEtNDEzMi05ZTkyLWIyYzNlZjg5ODU3ZWRkAgIPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDJmODA2ZDE0LTNlMTQtNDRkMi04ZjNiLWM4ZDFhNjJkZjhjN2RkAgMPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDNmOThlOGRiLTRiYjktNGQ5OS1iNWU5LWFmOGVlNDMxYzNiZmRkAgQPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDExMGE4ZGVmLWQzMzAtNDRjMS04YzM4LTY3N2Y5ZWRjZDE5Y2RkAgUPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGE5Mjc3YmZmLWFiNWYtNGMwNC05ZjUyLTlkMDZmNTg1NmFhY2RkAgYPZBYEZg8VAQBkAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDU2YjkwNmM5LTdlZTgtNDdiYS04NDc0LWZhNmNjMThjYzZjMGRkAgEPZBYCAgEPZBYCZhAWBB8IKCsEJDE5NjdlOTFkLTFjMGUtNDg0Ni1iNmMzLWVlYjBiOGU5ZGM0OB8HBQRMb2dvZGQCAhBkZBYCZhAWBB8HBRRDb3B5cmlnaHQgU3RhcnQgWWVhch8IKCsEJDE5NjdlOTFkLTFjMGUtNDg0Ni1iNmMzLWVlYjBiOGU5ZGM0OGRkAgMQZGQWAmYQFgQfBwUOQ29weXJpZ2h0IFRleHQfCCgrBCQxOTY3ZTkxZC0xYzBlLTQ4NDYtYjZjMy1lZWIwYjhlOWRjNDhkZAIEEGRkFgJmEBYEHwcFBFRleHQfCCgrBCQ3OGMyMTBiNC0yNWI0LTQwMmEtOWQxNC0xMDg5ZGJhNWY3MTZkZAIFDxQrAAIPFgQfA2cfBAIJZGQWAmYPZBYSAgEPZBYCAgMPDxYEHwEFDOeugOS9k+S4reaWhx8JBS0vemgtY24vbGVnYWwvc29waG9zLWdyb3VwLXByaXZhY3ktcG9saWN5LmFzcHhkZAICD2QWBAIBDxYCHwJnZAIDDw8WBB8BBQdFbmdsaXNoHwkFLS9lbi11cy9sZWdhbC9zb3Bob3MtZ3JvdXAtcHJpdmFjeS1wb2xpY3kuYXNweGRkAgMPZBYCAgMPDxYEHwEFCUZyYW7Dp2Fpcx8JBS0vZnItZnIvbGVnYWwvc29waG9zLWdyb3VwLXByaXZhY3ktcG9saWN5LmFzcHhkZAIED2QWAgIDDw8WBB8BBQdEZXV0c2NoHwkFLS9kZS1kZS9sZWdhbC9zb3Bob3MtZ3JvdXAtcHJpdmFjeS1wb2xpY3kuYXNweGRkAgUPZBYCAgMPDxYEHwEFCUl0YWxpYW5vIB8JBS0vaXQtaXQvbGVnYWwvc29waG9zLWdyb3VwLXByaXZhY3ktcG9saWN5LmFzcHhkZAIGD2QWAgIDDw8WBB8BBQnml6XmnKzoqp4fCQUtL2phLWpwL2xlZ2FsL3NvcGhvcy1ncm91cC1wcml2YWN5LXBvbGljeS5hc3B4ZGQCBw9kFgICAw8PFgQfAQUJ7ZWc6rWt7J2YHwkFFWh0dHA6Ly9rci5zb3Bob3MuY29tL2RkAggPZBYCAgMPDxYEHwEFCEVzcGHDsW9sHwkFLS9lcy1lcy9sZWdhbC9zb3Bob3MtZ3JvdXAtcHJpdmFjeS1wb2xpY3kuYXNweGRkAgkPZBYCAgMPDxYEHwEFDOe5gemrlOS4reaWhx8JBRVodHRwOi8vdHcuc29waG9zLmNvbS9kZAIGDxQrAAIPFgQfA2cfBAIDZGQWAmYPZBYGAgEPZBYCAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJDk2ZWY4NmEyLTcxYmYtNGQwZi1iMDgyLTA0MmZkM2UzMzlkMmRkAgIPZBYCAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGM1NTI5MzdkLTJiODQtNGY3OS05Y2MyLWI4ZjFlZWExYmIxZmRkAgMPZBYCAgEPZBYCAgEQZGQWAmYQFgQfBwUEVGV4dB8IKCsEJGQ5ZGM5OTUzLTA0MTMtNGY2NS1hZWU0LTQ4NzE2YzIyOTA2YmRkAgUPFgIfAWVkAgcPFgIfAQWAGjwhLS0gR29vZ2xlIFRhZyBNYW5hZ2VyIC0tPgo8bm9zY3JpcHQ+PGlmcmFtZSBzcmM9Ii8vd3d3Lmdvb2dsZXRhZ21hbmFnZXIuY29tL25zLmh0bWw/aWQ9R1RNLVRTSjNSNyIKaGVpZ2h0PSIwIiB3aWR0aD0iMCIgc3R5bGU9ImRpc3BsYXk6bm9uZTt2aXNpYmlsaXR5OmhpZGRlbiI+PC9pZnJhbWU+PC9ub3NjcmlwdD4KPHNjcmlwdD4oZnVuY3Rpb24odyxkLHMsbCxpKXt3W2xdPXdbbF18fFtdO3dbbF0ucHVzaCh7J2d0bS5zdGFydCc6Cm5ldyBEYXRlKCkuZ2V0VGltZSgpLGV2ZW50OidndG0uanMnfSk7dmFyIGY9ZC5nZXRFbGVtZW50c0J5VGFnTmFtZShzKVswXSwKaj1kLmNyZWF0ZUVsZW1lbnQocyksZGw9bCE9J2RhdGFMYXllcic/JyZsPScrbDonJztqLmFzeW5jPXRydWU7ai5zcmM9CicvL3d3dy5nb29nbGV0YWdtYW5hZ2VyLmNvbS9ndG0uanM/aWQ9JytpK2RsO2YucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoaixmKTsKfSkod2luZG93LGRvY3VtZW50LCdzY3JpcHQnLCdkYXRhTGF5ZXInLCdHVE0tVFNKM1I3Jyk7PC9zY3JpcHQ+CjwhLS0gRW5kIEdvb2dsZSBUYWcgTWFuYWdlciAtLT4KCjwhLS0gR29vZ2xlIENvZGUgZm9yIFJlbWFya2V0aW5nIHRhZyAtLT4KPCEtLSBSZW1hcmtldGluZyB0YWdzIG1heSBub3QgYmUgYXNzb2NpYXRlZCB3aXRoIHBlcnNvbmFsbHkgaWRlbnRpZmlhYmxlIGluZm9ybWF0aW9uIG9yIHBsYWNlZCBvbiBwYWdlcyByZWxhdGVkIHRvIHNlbnNpdGl2ZSBjYXRlZ29yaWVzLiBGb3IgaW5zdHJ1Y3Rpb25zIG9uIGFkZGluZyB0aGlzIHRhZyBhbmQgbW9yZSBpbmZvcm1hdGlvbiBvbiB0aGUgYWJvdmUgcmVxdWlyZW1lbnRzLCByZWFkIHRoZSBzZXR1cCBndWlkZTogZ29vZ2xlLmNvbS9hZHMvcmVtYXJrZXRpbmdzZXR1cCAtLT4KPHNjcmlwdCB0eXBlPSJ0ZXh0L2phdmFzY3JpcHQiPgovKiA8IVtDREFUQVsgKi8KdmFyIGdvb2dsZV9jb252ZXJzaW9uX2lkID0gOTg3NDMzMTExOwp2YXIgZ29vZ2xlX2NvbnZlcnNpb25fbGFiZWwgPSAiNFBKaUNKbnVoZ1VRbDVIczFnTSI7CnZhciBnb29nbGVfY3VzdG9tX3BhcmFtcyA9IHdpbmRvdy5nb29nbGVfdGFnX3BhcmFtczsKdmFyIGdvb2dsZV9yZW1hcmtldGluZ19vbmx5ID0gdHJ1ZTsKLyogXV0+ICovCjwvc2NyaXB0Pgo8c2NyaXB0IHR5cGU9InRleHQvamF2YXNjcmlwdCIgc3JjPSIvL3d3dy5nb29nbGVhZHNlcnZpY2VzLmNvbS9wYWdlYWQvY29udmVyc2lvbi5qcyI+Cjwvc2NyaXB0Pgo8bm9zY3JpcHQ+CjxkaXYgc3R5bGU9ImRpc3BsYXk6aW5saW5lOyI+CjxpbWcgaGVpZ2h0PSIxIiB3aWR0aD0iMSIgc3R5bGU9ImJvcmRlci1zdHlsZTpub25lOyIgYWx0PSIiIHNyYz0iLy9nb29nbGVhZHMuZy5kb3VibGVjbGljay5uZXQvcGFnZWFkL3ZpZXd0aHJvdWdoY29udmVyc2lvbi85ODc0MzMxMTEvP3ZhbHVlPTEuMDAmYW1wO2N1cnJlbmN5X2NvZGU9VVNEJmFtcDtsYWJlbD00UEppQ0pudWhnVVFsNUhzMWdNJmFtcDtndWlkPU9OJmFtcDtzY3JpcHQ9MCIvPgo8L2Rpdj4KPC9ub3NjcmlwdD4KCjwhLS0gRGVtYW5kYmFzZSBwbHVnLWluIGZvciBHb29nbGUgQW5hbHl0aWNzIC0tPgo8c2NyaXB0PgooZnVuY3Rpb24oZCxiLGEscyxlKXsgdmFyIHQgPSBiLmNyZWF0ZUVsZW1lbnQoYSksCiAgICBmcyA9IGIuZ2V0RWxlbWVudHNCeVRhZ05hbWUoYSlbMF07IHQuYXN5bmM9MTsgdC5pZD1lOwogICAgdC5zcmM9KCdodHRwczonPT1kb2N1bWVudC5sb2NhdGlvbi5wcm90b2NvbCA/ICdodHRwczovLycgOiAnaHR0cDovLycpICsgczsKICAgIGZzLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKHQsIGZzKTsgfSkKKHdpbmRvdyxkb2N1bWVudCwnc2NyaXB0Jywnc2NyaXB0cy5kZW1hbmRiYXNlLmNvbS9XdUVGTmdsei5taW4uanMnLCdkZW1hbmRiYXNlX2pzX2xpYicpOwo8L3NjcmlwdD4KPCEtLSBFbmQgRGVtYW5kYmFzZSBwbHVnLWluIGZvciBHb29nbGUgQW5hbHl0aWNzIC0tPgoKCjwhLS0gIFF1YW50Y2FzdCBUYWcgLS0+CjxzY3JpcHQ+CiAgICAgICAoZnVuY3Rpb24oKXsKICAgICAgIHZhciBlbGVtID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7CiAgICAgICBlbGVtLnNyYyA9IChkb2N1bWVudC5sb2NhdGlvbi5wcm90b2NvbCA9PSAiaHR0cHM6IiA/ICJodHRwczovL3NlY3VyZSIgOiAiaHR0cDovL3BpeGVsIikgKyAiLnF1YW50c2VydmUuY29tL2FxdWFudC5qcz9hPXAtTG5ybkJhbjFyd1pwSCI7CiAgICAgICBlbGVtLmFzeW5jID0gdHJ1ZTsKICAgICAgIGVsZW0udHlwZSA9ICJ0ZXh0L2phdmFzY3JpcHQiOwogICAgICAgdmFyIHNjcHQgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnc2NyaXB0JylbMF07CiAgICAgICBzY3B0LnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGVsZW0sc2NwdCk7CiAgICAgfSgpKTsKCgogICB2YXIgc21hcnR0YWdkYXRhPXtxYWNjdDogJ3AtTG5ybkJhbjFyd1pwSCcsCiAgICAgICAgICAgICAgICAgICAgICAgIG9yZGVyaWQ6ICcnLAogICAgICAgICAgICAgICAgICAgICAgICByZXZlbnVlOiAnJywKICAgICAgICAgICAgICAgICAgICAgICAgfTsKPC9zY3JpcHQ+CiAgPG5vc2NyaXB0PgogICAgPGltZyBzcmM9Ii8vcGl4ZWwucXVhbnRzZXJ2ZS5jb20vcGl4ZWwvcC1MbnJuQmFuMXJ3WnBIXC5naWY/bGFiZWxzPV9mcC5ldmVudC5EZWZhdWx0OyIgc3R5bGU9ImRpc3BsYXk6IG5vbmU7IiBib3JkZXI9IjAiIGhlaWdodD0iMSIgd2lkdGg9IjEiIGFsdD0iUXVhbnRjYXN0Ii8+CiAgPC9ub3NjcmlwdD4KICAKICAKICA8c2NyaXB0PgpzbWFydHRhZ2RhdGEub3JkZXJpZD0nSU5TRVJUK09SREVSSUQnOwpzbWFydHRhZ2RhdGEucmV2ZW51ZT0nSU5TRVJUK1JFVkVOVUUnOwo8L3NjcmlwdD4KCjwhLS0gRW5kIFF1YW50Y2FzdCBUYWcgLS0+CgoKPCEtLSBDaGFuZ28gUGl4ZWwgVGFnIC0tPgo8c2NyaXB0IHR5cGU9InRleHQvamF2YXNjcmlwdCI+CiAgICB2YXIgX19jaG9fXyA9IHsicGlkIjo1NTMyfTsKICAgIChmdW5jdGlvbigpIHsKICAgICAgICB2YXIgYyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpOwogICAgICAgIGMudHlwZSA9ICd0ZXh0L2phdmFzY3JpcHQnOwogICAgICAgIGMuYXN5bmMgPSB0cnVlOwogICAgICAgIGMuc3JjID0gZG9jdW1lbnQubG9jYXRpb24ucHJvdG9jb2wgKyAnLy9jYy5jaGFuZ28uY29tL3N0YXRpYy9vLmpzJzsKICAgICAgICB2YXIgcyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdzY3JpcHQnKVswXTsKICAgICAgICBzLnBhcmVudE5vZGUuaW5zZXJ0QmVmb3JlKGMsIHMpOwogICAgfSkoKTsKPC9zY3JpcHQ+CjwhLS0gRW5kIG9mIENoYW5nbyBQaXhlbCBUYWcgLS0+CgpkGBwFLGN0bDA1JGxzdHZIb3Zlck1lbnUkY3RybDQkbHN0dkhvdmVyTWVudUl0ZW1zD2dkBTNjdGwwNiRsc3R2UHJpbWFyeU5hdmlnYXRpb24kY3RybDMkbHN0dlN1YlF1aWNrTGlua3MPFCsADmRkZGRkZGQ8KwAFAAIFZGRkZgL/////D2QFHmN0bDA2JGxzdHZHbG9iYWxMYW5ndWFnZUZvb3Rlcg8UKwAOZGRkZGRkZDwrAAkAAglkZGRmAv////8PZAUzY3RsMDYkbHN0dlByaW1hcnlOYXZpZ2F0aW9uJGN0cmw0JGxzdHZTdWJRdWlja0xpbmtzDxQrAA5kZGRkZGRkPCsABQACBWRkZGYC/////w9kBSxjdGwwNSRsc3R2SG92ZXJNZW51JGN0cmwzJGxzdHZIb3Zlck1lbnVJdGVtcw9nZAUmY3RsMDUkbHN0dkhvdmVyTWVudSRjdHJsNCRsc3R2SGVhZGluZ3MPZ2QFJmN0bDA1JGxzdHZIb3Zlck1lbnUkY3RybDAkbHN0dkhlYWRpbmdzDxQrAA5kZGRkZGRkFCsAA2RkZAIDZGRkZgL/////D2QFM2N0bDA2JGxzdHZQcmltYXJ5TmF2aWdhdGlvbiRjdHJsNSRsc3R2U3ViUXVpY2tMaW5rcw8UKwAOZGRkZGRkZDwrAAYAAgZkZGRmAv////8PZAUzY3RsMDYkbHN0dlByaW1hcnlOYXZpZ2F0aW9uJGN0cmwwJGxzdHZTdWJRdWlja0xpbmtzDxQrAA5kZGRkZGRkFCsAA2RkZAIDZGRkZgL/////D2QFG2N0bDA2JGxzdHZQcmltYXJ5TmF2aWdhdGlvbg8UKwAOZGRkZGRkZDwrAAYAAgZkZGRmAv////8PZAVBY3RsMDUkbHN0dkhvdmVyTWVudSRjdHJsMCRsc3R2SGVhZGluZ3MkY3RybDIkbHN0dkhlYWRpbmdzU3ViSXRlbXMPFCsADmRkZGRkZGQ8KwAFAAIFZGRkZgL/////D2QFG2N0bDA1JGxzdHZQcmltYXJ5TmF2aWdhdGlvbg8UKwAOZGRkZGRkZDwrAAUAAgVkZGRmAv////8PZAUsY3RsMDUkbHN0dkhvdmVyTWVudSRjdHJsMSRsc3R2SG92ZXJNZW51SXRlbXMPZ2QFMWN0bDA1JFNlY29uZGFyeU5hdmlnYXRpb24kbHN0dlNlY29uZGFyeU5hdmlnYXRpb24PFCsADmRkZGRkZGQUKwACZGQCAmRkZGYC/////w9kBSxjdGwwNSRsc3R2SG92ZXJNZW51JGN0cmwwJGxzdHZIb3Zlck1lbnVJdGVtcw8UKwAOZGRkZGRkZBQrAANkZGQCA2RkZGYC/////w9kBRNjdGwwNSRsc3R2SG92ZXJNZW51DxQrAA5kZGRkZGRkPCsABQACBWRkZGYC/////w9kBSxjdGwwNSRsc3R2SG92ZXJNZW51JGN0cmwyJGxzdHZIb3Zlck1lbnVJdGVtcw9nZAUzY3RsMDYkbHN0dlByaW1hcnlOYXZpZ2F0aW9uJGN0cmwyJGxzdHZTdWJRdWlja0xpbmtzDxQrAA5kZGRkZGRkPCsABQACBWRkZGYC/////w9kBRVjdGwwNSRsc3R2R2xvYmFsUGFnZXMPFCsADmRkZGRkZGQ8KwAJAAIJZGRkZgL/////D2QFQWN0bDA1JGxzdHZIb3Zlck1lbnUkY3RybDAkbHN0dkhlYWRpbmdzJGN0cmwxJGxzdHZIZWFkaW5nc1N1Ykl0ZW1zDxQrAA5kZGRkZGRkPCsABQACBWRkZGYC/////w9kBRtjdGwwNiRsc3R2VXRpbGl0eU5hdmlnYXRpb24PFCsADmRkZGRkZGQUKwADZGRkAgNkZGRmAv////8PZAVBY3RsMDUkbHN0dkhvdmVyTWVudSRjdHJsMCRsc3R2SGVhZGluZ3MkY3RybDAkbHN0dkhlYWRpbmdzU3ViSXRlbXMPFCsADmRkZGRkZGQ8KwAFAAIFZGRkZgL/////D2QFWHNvcGhvc25leHRfY29udGVudF8wJHNvcGhvc25leHRfY29udGVudGJvZHlfMCRzb3Bob3NuZXh0X2NvbnRlbnRyYWlsXzAkbXVsdHZ3UGFnZUVkaXRpbmcPD2RmZAUmY3RsMDUkbHN0dkhvdmVyTWVudSRjdHJsMSRsc3R2SGVhZGluZ3MPZ2QFFWN0bDA1JGxzdHZTaWduSW5QYWdlcw8UKwAOZGRkZGRkZBQrAAJkZAICZGRkZgL/////D2QFM2N0bDA2JGxzdHZQcmltYXJ5TmF2aWdhdGlvbiRjdHJsMSRsc3R2U3ViUXVpY2tMaW5rcw8UKwAOZGRkZGRkZDwrAAYAAgZkZGRmAv////8PZAUmY3RsMDUkbHN0dkhvdmVyTWVudSRjdHJsMiRsc3R2SGVhZGluZ3MPZ2QFJmN0bDA1JGxzdHZIb3Zlck1lbnUkY3RybDMkbHN0dkhlYWRpbmdzD2dkFL4iCBmtRzqSNcAXOJoLge5AYow=" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=KC9CXtv0LwWT0F6Rbo3BL6_32xgVMXTPfsqGApNPDVBRpQ1CvxPg7UipW14gvs0GTP2-6SM0M69GzoiVP3p0CfkGja41&amp;t=635802997220000000" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
//]]>
</script>

<script src="/ScriptResource.axd?d=BsN5d_eZ5SHqi__qUQyQywo4bNoPpZHh_R-4xrZlJpJWktbkQUA5EGJZ8V4mQi2So-q0MJIoFQYFQPttbhyaPiXCb2BV3jg-Twq0ObprWeOZQh8YT-eITRksCNPGH_NT-zqbWszOkseBEC9qKjCySbzHtY41&amp;t=5f9d5645" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=2kAk_BJXVLMXcSd-VyxUdYLQqlyykROKPVUP1uJRizlkURSoGrF_1emjbZdBwwGRx7ds05zJSSLFh__cYRQCycyU38nd6pjPE_QAEBzf2pVTvxjHSRXovWb4n_bY_lzExvJoJiCfsaWu_DgC5lhCX4-yCFGM8-lzB7TPHKBvJm_e2P130&amp;t=5f9d5645" type="text/javascript"></script>
<div>

	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdABW/+Q7BTxTArSvQ3Qv9Ik17IWkdGBUYMQrDlYSypOB/0PO24rJQPUq7ClMIU7002CtXuh/c9eveeydeDhCIY6iZaPgE1qfiVAVWWejzgaGFqr1B4OYCCBY+Duzg4zDfBMo56mtlzKb1FJ5wmQCQsjo8GtGEGemvKZ34w5iVK5LWH90uC49LGZq/qwods6batRIwvw2oKD856ARaGNzcduf+g3Kd+HOTP91UWpiItVu+ayY28OG3duTqJrZVrbSLKE4dPSF1z0UBeEEm9TCsBloElgND+DLqHRKhANxhK+uTdxCIo/IXm4SxDZWUx3sukfvx0DEeXPMU1jKaNwW42ru9VgkF67de1ZlJMZ9FOR+wT9JwgE8GgXIEHf7sdMhvAWMjR0kv1oqLLiQ2ZoCqSMCXwuL9dn0ZQW3W04IUuKWDBBx2XZXfvaAjLyRdZZVuHCPhptGUDwOarOqM9Rlih/YMOi/xXw==" />
</div>
    
    
    
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ToolkitScriptManager1', 'form1', [], [], [], 90, '');
//]]>
</script>

    
    
    
    

<style type="text/css">  
    #header #popup-signin ul {
        list-style-type: none;
        margin: 0;
        padding: 0;
        float: none;
    }
    #header #popup-signin li {
        float: none;
        margin: 0;
        padding: 0;
        height: auto;
        border-top: 1px solid #d6d6d6;
    }
    #header #popup-signin li a {
        text-transform: none;
        font-family: 'Flama-Basic';
        padding: 12px 20px;
        letter-spacing: 0px;
        display:block;
    }
    #header #popup-signin li a:hover {
        background: #ffffff;
    }
    .signed-in {
        background-position: left bottom !important;
    }
</style>

<script type="text/javascript">
    $jq(document).ready(function () {
        if ($jq('#secondary').length !== 0) {
            $jq('#nav').addClass('secondary');
        }
    });
</script>

<div id="nav">
    
    <div id="header">        
        <div class="container">
            <h2 id="logo">
                <a href="/en-us.aspx"><img src="/en-us/medialibrary/Images/Common/logo-header.png?la=en" alt="Sophos" width="102" height="19" /></a>
            </h2>
            <ul>
                
                        
                        <li >
                            <a class="nav-products" href="/en-us/products.aspx">Products</a>
                        </li>   
                    
                        <li >
                            <a class="nav-labs" href="/en-us/threat-center/threat-analyses.aspx">Labs</a>
                        </li>   
                    
                        <li >
                            <a class="nav-partners" href="/en-us/partners.aspx">Partners</a>
                        </li>   
                    
                        <li >
                            <a class="nav-company" href="/en-us/company.aspx">Company</a>
                        </li>   
                    
                        <li >
                            <a class="nav-support" href="/en-us/support.aspx">Support</a>
                        </li>   
                    
                    
                <li id="ctl05_signInLink" class="icon"><a id="ctl05_hypMySophos" class="signin" href="/en-us/mysophos/login.aspx">Sign In</a> </li>
                <li id="ctl05_globalSitesLink" class="icon"><a href="#globalsites" class="global">Global Sites</a></li>
                <li id="ctl05_searchLink" class="icon"><a href="#search" class="search">Search</a></li>
            </ul>

            
                    
                     <div id="popup-products" class="popup">
                      <span></span>
                      <div class="popup-wrap">
                          
                        
                               <ol class="clear">
                                    
                                <li class="network-heading"><h3>Network Protection</h3></li>
                            
                                <li class="network-heading"><h3>Enduser Protection</h3></li>
                            
                                <li class="network-heading"><h3>Server Protection</h3></li>
                            
                                </ol>
                            

                        
                                <div class="popup-content clear">
                                    
                                
                                        <ul>
                                            
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-ngfw-small.png?la=en"><a class="icon-utm" href="/en-us/products/next-gen-firewall.aspx">XG Firewall</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-utm-small.png?la=en"><a class="icon-utm" href="/en-us/products/utm-9.aspx">SG UTM</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-securewifi-small.png?la=en"><a class="icon-utm" href="/en-us/products/secure-wifi.aspx">Secure Wi-Fi</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-secureweb-small.png?la=en"><a class="icon-utm" href="/en-us/lp/choose-swg.aspx">Secure Web Gateway</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-secureemail-small.png?la=en"><a class="icon-utm" href="/en-us/products/secure-email-gateway.aspx">Secure Email Gateway</a></li>
                                    
                                        </ul><!-- /popup-content -->
                                    
                            
                                
                                        <ul>
                                            
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-eup-small.png?la=en"><a class="icon-utm" href="/en-us/products/enduser-protection-suites.aspx">Enduser Protection Bundles</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/Endpoint_AV_thumbnail_04.png?la=en"><a class="icon-utm" href="/en-us/products/endpoint-antivirus.aspx">Endpoint Antivirus</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Navigation/Cloud-02.png?la=en"><a class="icon-utm" href="/en-us/products/cloud.aspx">Sophos Cloud</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-mobilecontrol-small.png?la=en"><a class="icon-utm" href="/en-us/products/mobile-control.aspx">Mobile Control</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-encryption-small.png?la=en"><a class="icon-utm" href="/en-us/products/safeguard-encryption.aspx">SafeGuard Encryption</a></li>
                                    
                                        </ul><!-- /popup-content -->
                                    
                            
                                
                                        <ul>
                                            
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/vShield-icon-small.png?la=en"><a class="icon-utm" href="/en-us/products/virtualization-security.aspx">Virtualization Security</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-serverantivirus-small.png?la=en"><a class="icon-utm" href="/en-us/products/server-security.aspx">Server Security</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/SophosNext/Images/Products/Sharepoint/Sharepoint-Icon-sm.png?la=en"><a class="icon-utm" href="/en-us/products/sharepoint-security.aspx">SharePoint Security</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-networkstorage-small.png?la=en"><a class="icon-utm" href="/en-us/products/network-storage-antivirus.aspx">Network Storage Antivirus</a></li>
                                    
                                        <li iconurl="/en-us/medialibrary/Images/Products/Icons/icon-puremessage-small.png?la=en"><a class="icon-utm" href="/en-us/products/puremessage.aspx">PureMessage</a></li>
                                    
                                        </ul><!-- /popup-content -->
                                    
                            
                                </div><!-- /popup-content -->
                            
                             
                      </div><!-- /popup-wrap -->
                    </div>
                
                     <div id="popup-products" class="popup">
                      <span></span>
                      <div class="popup-wrap">
                          
                        

                        
                             
                      </div><!-- /popup-wrap -->
                    </div>
                
                     <div id="popup-products" class="popup">
                      <span></span>
                      <div class="popup-wrap">
                          
                        

                        
                             
                      </div><!-- /popup-wrap -->
                    </div>
                
                     <div id="popup-products" class="popup">
                      <span></span>
                      <div class="popup-wrap">
                          
                        

                        
                             
                      </div><!-- /popup-wrap -->
                    </div>
                
                     <div id="popup-products" class="popup">
                      <span></span>
                      <div class="popup-wrap">
                          
                        

                        
                             
                      </div><!-- /popup-wrap -->
                    </div>
                
                

            <!--LoginPopup-->
            
                    <div id="DONOTSHOW" class="popup" style="display: none;">
                        <span></span>
                        <div class="popup-wrap">
                            <h3>
                                <span class="icon-secure">Secure Login</span>
                                Sign In
                                <small>Pick a destination and sign in. </small>
                            </h3>
                            <div class="popup-content">
                                <ul>
                                    
                    <li>
                        <a href="/en-us/mysophos/login.aspx">My Sophos</a>
                    </li>   
                
                    <li>
                        <a href="/en-us/mysophos/logout.aspx">Log Out</a>
                    </li>   
                
                                </ul>
                            </div><!-- /popup-content -->
                        </div><!-- /popup-wrap -->
                    </div>
                
            <!-- /Login Popup -->

            <!-- Global Sites Popup -->
            
                    <div id="popup-global" class="popup">
                        <span></span>
                        <div class="popup-wrap">
                            <h3>
                                Global Sites
                                <small>Pick your region or language.</small>
                            </h3>
                            <div class="popup-content">
                                <ul>
                                    
                    <li>
                        <a href="/en-us.aspx">English</a>
                    </li>   
                
                    <li>
                        <a href="http://www.sophos.com/de-de/">Deutsch</a>
                    </li>   
                
                    <li>
                        <a href="http://www.sophos.com/es-es/">Español</a>
                    </li>   
                
                    <li>
                        <a href="http://www.sophos.com/fr-fr/">Français</a>
                    </li>   
                
                    <li>
                        <a href="http://www.sophos.com/it-it/">Italiano </a>
                    </li>   
                
                    <li>
                        <a href="http://www.sophos.com/ja-jp/">日本語</a>
                    </li>   
                
                    <li>
                        <a href="http://www.sophos.com/zh-cn">简体中文</a>
                    </li>   
                
                    <li>
                        <a href="http://kr.sophos.com/">한국의</a>
                    </li>   
                
                    <li>
                        <a href="http://tw.sophos.com/">繁體中文</a>
                    </li>   
                
                                </ul>
                            </div><!-- /popup-content -->
                        </div><!-- /popup-wrap -->
                    </div>
                
            <!-- /Global Sites Popup -->

            <!-- Search Popup -->
            <div id="popup-search" class="popup">
                <span></span>
                <div class="popup-wrap">
                    <h3 class="no-results">
                        <input type="text" placeholder="Search sophos.com..." id="search-field" data-gsa-autosuggest="true" SearchResultsUrl="/en-us/search-results.aspx" >
                    </h3>
                </div><!-- /popup-wrap -->
            </div>
            <!-- /Search Popup -->

        </div><!-- /container -->
    </div><!-- /header -->  
    
    
        <div id="secondary">
            <div class="container clear SecondaryNav">
                <ul>
                    
        <li class='selected ' IconUrl="">
            <a href="/en-us/legal.aspx">Overview</a>
        </li>
    
        <li class='  secondary-nav-icon section-border' IconUrl="/en-us/medialibrary/SophosNext/Images/Navigation/icon-contact.png?la=en">
            <a href="/en-us/company/contact/northsouthamerica.aspx">Contact</a>
        </li>
    
                </ul>
            </div>
        </div>
    

    
</div>
    



 <div class="container page">
   


   

<div id="privacy-policy" style="padding-top:40px">
    <div class="row clear">
      <div class="col two-thirds">
        <h1 class="page-heading">Sophos Group Privacy Policy</h1>

        <p>This document was last updated on 25 November 2015.</p>
<h3>General</h3>
<p>This is the privacy policy of Sophos Limited and its subsidiaries. </p>
<p>We are committed to safeguarding the privacy of your personal data. Please read the following privacy policy to understand how we collect and use your personal data, for example when you contact us, visit one of our websites (each a &ldquo;Site&rdquo;), apply for a job, or use our products and services. </p>
<p>Whenever you give us personal data, you are consenting to its collection and use in accordance with this privacy policy. </p>
<h3>What personal data do we collect?</h3>
<p>We may collect personal data such as your name, company position, address, telephone number, mobile number, fax number, email address, credit card details, age, IP address, and account usernames. </p>
<h3>How do you use my personal data?</h3>
<p>If you provide personal data to us, we will collect that information and use it for the purposes for which you have provided it and in accordance with this privacy policy.</p>
<h3>Browsing our Site</h3>
<p>Every time you connect to the Site, we store a log of your visit that shows the unique number your machine uses when it is connected to the Internet - its IP address. This tells us what your machine has looked at, whether the page request was successful or not and which browser your machine used to view the pages. This data is used for statistical purposes as well as to help customize the user experience as you browse the Site and subsequently interact with Sophos and our partners. This helps us to understand which areas of the Site are of particular interest, which pages are not being requested, and how many people are visiting the Site in total. It also helps us and our partners to determine which products and services may be of specific interest to you. We may attempt to contact you through these details if necessary, including, without limitation, when you are using the wrong paths to access the Site or are breaching restrictions on the use of the Site. We may also use this information to block IP addresses where there is a breach of the Terms and Conditions for use of the Site.</p>
<h3>Cookies</h3>
<p>A cookie is a piece of text that gets entered into the memory of your browser by a website, allowing the website to store information on your machine and later retrieve it. Some of our pages use cookies so that we can distinguish you from other users and better serve you when you return to the Site. Cookies also enable us to track and target the interests of our users to enhance the onsite experience. For information about the cookies that we use, please refer to the <a href="https://www.sophos.com/en-us/legal/cookie-information.aspx">Cookie Information</a> page on the relevant Site.</p>
<h3>Job applicants</h3>
<p>If you are making a job application or inquiry, you may provide us with a copy of your CV or other relevant information. We may use this information for the purpose of considering your application or inquiry. Except when you explicitly request otherwise, we may keep this information on file for future reference.</p>
<h3>Partner portal</h3>
<p>Our resellers and distributors may visit our partner portal Site. We may use the customer and prospect information provided on that Site in order to provide the products and services.</p>
<h3>Account management</h3>
<p>If you obtain products or services from us, we may use your contact details and (where applicable) payment information for the purposes of (i) providing training, customer support and account management, (ii) order processing and billing, (iii) verifying your usage of the products and services in accordance with the terms and conditions of your agreement with us, (iv) carrying out end user compliance checks for export control purposes; (v) issuing license expiry, renewal and other related notices, and (vi) maintaining our company accounts and records. </p>
<h3>Product and service related data</h3>
<p>If you purchase or use our products or services, we may collect the following types of information: (i) product type, product version, product features and operating systems being used; (ii) processing times; (iii) customer identification code and company name, and (iv) IP address of the machine  which returns the above listed information.</p>
<p>We may use such information for purposes which include but are not limited to:</p>
<ul class="bulletList">
  <li>verifying your credentials and compliance with any usage restrictions, </li>
    <li>carrying out end user compliance checks for export control purposes, </li>
    <li>providing the products/services and any associated maintenance and technical support, </li>
    <li>providing virus, incident and other alerts, and information about product upgrades, updates, renewals and product lifecycle changes,</li>
    <li>providing maintenance and technical support, </li>
    <li>providing information about product upgrades, updates and renewals, </li>
    <li>generating logs, statistics and reports on service usage, service performance and malware infection, </li>
    <li>evaluating, developing and enhancing products, services, and our infrastructure, </li>
    <li>planning development roadmaps and product lifecycle strategies. </li>
</ul>
<p>Some products and services also collect or generate an ID code for each machine which reports back to us. This ID code is only used to enable us to distinguish between unique machines so that (i) we do not duplicate reports from the same source; and (ii) we can determine the number of unique machines that are using the products and services. If this ID code is collected together with other information which could identify an individual when combined, we anonymize the ID code to prevent this from occurring. </p>
<p>Certain products and services may include features that collect additional personal data for other purposes, as described below. For detailed information, please also refer to the applicable product or service description.</p>

<h3>Sophos Home Products</h3>

<p>We may directly and remotely communicate with your protected devices for the purposes of, without limitation (i) applying policy and configuration changes to such devices; and (ii) extracting usage information, service performance information, and infection logs.</p>
 
<p>In order to continuously improve the protection levels in the Sophos Home products, it may be necessary for us to collect and process certain information relating to you and to users connected to your account. You acknowledge and agree that the information we collect may include confidential and/or personal data, including without limitation (i) names and email addresses; (ii) account usernames; (iii) IP addresses; (iv) usage information; (v) details of changes or attempted changes to executable files, pathnames and scripts, (vi) infection logs; and (vii) files suspected of being infected with malware. We are committed to safeguarding the privacy of your personal data and will never share this outside Sophos.</p>
 
<p>You warrant that you have obtained all necessary permissions and provided the necessary notifications to share the above information with us for the purposes described from all users connected to your account.</p>

<h3>Sophos Mobile Security </h3>
<p>When an application is downloaded on a device or the user initiates a check of all installed applications on an Android device, Sophos Mobile Security sends queries to our cloud infrastructure in order to validate the reputation of the applications. Each query contains a fingerprint generated from the Android application (the APK file) under investigation.</p>
<p>A unique device identifier is also generated locally on each mobile device during installation of Sophos Mobile Security. We do not associate this identifier with any personal data. Periodically the product sends statistical feedback packets to us, including the unique device identifier and service performance information.</p>


<h3>Sophos Firewall Products</h3>
<p>You acknowledge and agree that the Sophos Firewall and Firewall Manager Products may provide us with the below information, which will be used for the purpose of improving product stability, prioritizing feature refinements and enhancing protection. </p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;(a). Configuration and Usage Data, including without limitation (i) device model, firmware and license information, such as model, hardware version, vendor, firmware version, and country; (ii) aggregated product usage information, such as product features in use (on/off, count), amount of configured objects, policies, managed devices, groups, templates (iii) CPU, memory, and disk usage information; (iv) product errors; and </p>
  <p>&nbsp;&nbsp;&nbsp;&nbsp;(b). Application Usage and Threat Data, including without limitation (i) IPS alerts; (ii) virus detected and the URL where the virus was found; (iii) spam; (iv) ATP threats; and (v) applications used and unclassified applications.</p>
<p>Information about unclassified applications is used to improve and enlarge network visibility and the application control library. </p>

  <p>&nbsp;&nbsp;&nbsp;&nbsp;(c).  Monitoring Threshold Data, includes (i) monitoring threshold values per model; and (ii) alert threshold criteria and values per model.</p> 
<p>Monitoring Threshold data is used to improve the default threshold settings and alert criteria included within the product across models.</p>


<p>Configuration and Usage Data does not include user-specific information or personal data and cannot be disabled.  Application Usage and Threat Data, and Monitoring Threshold Data collection is enabled by default, but you may disable collection of such data within the product at any time. </p>



<h3>Sophos Mobile Control </h3>
<p>When Sophos Mobile Control is installed or updated, you may receive Apple push notifications, Google cloud to device messaging for Android, SMS text messages, and other remote communications. </p>
<p>Sophos Mobile Control will store a list of users and mobile devices, and will record any applications downloaded or modifications made to such devices. Your administrator can also configure Sophos Mobile Control to track the geographic location of mobile devices and to lock or wipe a mobile device that has been lost or stolen.</p><h3>Sophos Cloud Products</h3>
<p>We may directly and remotely communicate with your protected devices for the purposes of, without limitation (i) applying policy and configuration changes to such devices; and (ii) extracting usage information, service performance information, and infection logs. Such communications may include but not be limited to SMS text messages and other push notifications.</p>
<p>You acknowledge and agree that it may be necessary for us to collect and process certain information relating to individuals in order to provide the Cloud products, and that such information may include proprietary, confidential and/or personal data, including without limitation (i) names, email addresses, telephone numbers and other contact details; (ii) account usernames; (iii) IP addresses; (iv) usage information; (v) lists of all software, files, paths and applications installed on the device, (vi) details of changes or attempted changes to executable files, pathnames and scripts, (vii) logs of websites visited; (viii) infection logs; and (ix) files suspected of being infected with malware.</p>
<p>Certain Cloud products may also (at your sole option) enable you to configure the product to (i) track and log the geographic location of devices; (ii) block access to devices; (iii) delete the content of devices; (iv) store text and email messages that were sent and/or received by devices. Such information may also be stored on the device itself and accordingly we recommend that you encrypt your devices.</p>
<p>You warrant that you have obtained all necessary permissions and provided the necessary notifications to share the above information with us for the purposes described. You also acknowledge and agree that it may be necessary under applicable law to inform and/or obtain consent from individuals before you intercept, access, monitor, log, store, transfer, export, block access to, and/or delete their communications. You are solely responsible for compliance with such laws.</p>
<h3>Sophos Cloud Portal</h3>
<p>If you select &ldquo;Enable Partner Access&rdquo; in the Settings tab of your Sophos Cloud portal, your designated third party partner or service provider will be able to access and administer your Sophos Cloud services on your behalf. If you do not enable such access, your designated third party partner or service provider will only see high-level reporting information such as Sophos Cloud services purchased and current usage information. You may revoke such access at any time by changing the permissions in the Settings tab.</p>
<h3>Market research</h3>
<p>If you participate in surveys, we may use your personal data to carry out market research. This research is conducted for our internal business and training purposes and will improve our understanding of our users&rsquo; demographics, interests and behaviour. This research is compiled and analysed on an aggregated basis and therefore does not individually identify any user.</p>
<h3>Marketing and promotions</h3>
<p>We (or our resellers or other selected third parties acting on our behalf) may contact you from time to time in order to provide you with information about products and services that may be of interest to you. All marketing emails that we send to you will follow the email guidelines described below. You have the right to ask us not to process your personal data for marketing purposes, but if you do so, we may need to share your contact information with third parties for the limited purpose of ensuring that you do not receive marketing communications from them on our behalf.</p>
<h3>Email communications</h3>
<p>We adhere to the following guidelines in relation to our email communications:</p>
<ul class="bulletList">
    <li>emails will clearly identify us as the sender, </li>
    <li>emails will include our physical postal address, </li>
    <li>emails sent to you for marketing purposes will include an option to unsubscribe from future email messages, </li>
    <li>you may unsubscribe from all mailing lists, with the exception of any emails regarding legal notices, invoicing, product updates, upgrades or license renewals, </li>
    <li>any third parties who send emails on our behalf will be required to comply with legislative requirements on unsolicited emails and the use of personal data.</li>
</ul>
<p>We send emails from a number of different domains in both plain text and HTML email formats. Emails are usually sent using sender email addresses at:<br />
@sophos.com<br />
@email.sophos.com<br />
@sophos.de<br />
@sophos.fr<br />
@sophos.co.jp<br />
@sophos.it<br />
@sophos.au<br />
@sophos.com.au</p>
<p>Emails offering software downloads or free product trials will usually link to web pages on www.sophos.com or www.web.sophos.com. If you receive an email which claims to come from us but does not use these domains, or if you are suspicious that an email may not be approved by us, then please send a copy of the email to customerservice@sophos.com so we can investigate.</p>
<p>We have published best practice guidelines to help internet users learn how to avoid phishing emails at http://www.sophos.com/security/best-practice/phishing.html.</p>
<h3>With whom might we share your personal data?</h3>
<p>As a global company, we have international sites and users all over the world. When you give us personal data, that data may be used, processed or stored anywhere in the world, including countries outside the EEA. </p>
<p>We may also pass your personal data to suppliers, service providers, subcontractors, agents, distributors, resellers and other partners, some of whom may be located outside the EEA, in order to provide you with the information, products and services that you requested or otherwise for the purposes described in this privacy policy.</p>
<p>In the event that we receive requests from government departments, agencies or other official bodies, we will only disclose your information if and to the extent that we believe we are legally required to do so (for example upon receipt of a court order, warrant, subpoena or equivalent).</p>
<p>Except as set out above, we will not disclose your personal data save where we need to do so in order to enforce our rights.</p>
<p>Whenever we share personal data, we take all reasonable steps to ensure that it is treated securely and in accordance with this privacy policy.</p>
<h3>Links</h3>
<p>This privacy policy applies to personal data collected by us. If an email or Site contains links to a third party site, please be aware that we are not responsible for the content or privacy practices of such site. We encourage our users to be aware when they leave our Site, and to read the privacy policy of other sites that collect personal data. </p>
<h3>Security</h3>
<p>We endeavour to hold all personal data securely in accordance with our internal security procedures and applicable law.</p>
<p>Unfortunately, no data transmission over the Internet or any other network can be guaranteed as 100% secure. As a result, while we strive to protect your personal data, we cannot ensure and do not warrant the security of any information you transmit to us, and this information is transmitted at your own risk.</p>
<p>If you have been given log-in details to provide you with access to certain parts of our Site (for example our partner portal), you are responsible for keeping those details confidential.</p>
<h3>Contact</h3>
<p>This is the website of Sophos Limited a company registered in England and Wales under company number 2096520 whose registered office is at The Pentagon, Abingdon Science Park, Abingdon, Oxon, OX14 3YP, United Kingdom and whose VAT registration number is 991 2418 08.</p>
<p>If you want to request any information about your personal data or believe that we are holding incorrect personal data on you, please contact <a href="mailto:customerservice@sophos.com">customerservice@sophos.com</a>. It is possible to obtain a copy of the information that we hold on you. A nominal charge of &pound;10 may be made to cover administrative costs involved.</p>
<h3>Notification of changes</h3>
<p>This privacy policy was last updated on 25 November 2015. We reserve the right to amend or vary this policy at any time and the revised policy will apply from the date posted on the Site. You accept that by doing this, we have provided you with sufficient notice of the amendment or variation.</p>
<p>&nbsp;</p>

      </div>
      <div class="col one-third sidebar">
            
           <style>

div#notice{
  background:#F1F1F1;
  border:1px solid #DDDDDD;
  border-radius:5px;
  font-size:13px;
  line-height:1.4;
  padding:20px;
margin-bottom: 20px;
}
</style>
        
       
<h3>Safe Harbor Notice: </h3>
<div id="notice">
<p>For our Sophos Cloud product, all companies can choose where their data is stored (likely Dublin or Frankfurt for an EU company) and it will never “fail over” to the US. With regard to other data held, following the decision in October 2015 by the European Court of Justice to invalidate the US/EU Safe Harbor framework, Sophos is ensuring that all our intra-group data transfers to the US are based upon the EU model clauses and/or consent. These are template agreements drafted by the EU commission and are a commonly used and accepted way of transferring data outside of the EU. The EU model clauses and/or consent have also been our preferred approach for transfers to third parties, where this is applicable. Like many companies, we are assessing our relationships with third parties so that future transfers will continue to be in accordance with applicable legislation and provide protection for the personal data of our employees, customers and partners.</p>

</div>

    


<h3>Browse Documents</h3>
<div class="well center">
    <p>Select a license agreement or other legal document from the menu below.</p>
    <select name="sophosnext_content_0$sophosnext_contentbody_0$sophosnext_contentrail_1$ddlDocuments" onchange="javascript:setTimeout(&#39;__doPostBack(\&#39;sophosnext_content_0$sophosnext_contentbody_0$sophosnext_contentrail_1$ddlDocuments\&#39;,\&#39;\&#39;)&#39;, 0)" id="sophosnext_content_0_sophosnext_contentbody_0_sophosnext_contentrail_1_ddlDocuments">
	<option selected="selected" value="{A88EE41C-C2F5-49C0-AE9D-F066D1F0965D}">Competition Rules General</option>
	<option value="{83AC1EA7-9D7B-43A2-A9A7-0701CC71CF1A}">Sophos End User License Agreement</option>
	<option value="{AF94FB96-6FEE-4244-8825-316BE99B57E7}">Hardware Warranty Policy</option>
	<option value="{9E0A22BC-FECD-4546-9D9C-FF1979F229E8}">Licensing Guidelines</option>
	<option value="{2F7E87DD-3652-4BA8-A2DB-B5BB798C1A4E}">Sophos Managed Service Provider Agreement</option>
	<option value="{B76DE88A-E4A4-4731-8F62-F15E8FF8E37D}">Partner (Reseller) Application Terms and Conditions</option>
	<option value="{0EDC7D49-8C3F-4BB1-8C45-42E80442BEA3}">Sophos Partner Agreement Resellers</option>
	<option value="{5A155F78-ED61-4232-AEE3-B503AA012825}"> Sophos Training Terms and Conditions</option>
	<option value="{09476867-9F5F-463F-8402-D6AD171E8453}">Sophos Crimea Guidance Note</option>
	<option value="{9FF26ADC-CD92-4A09-952E-EB06A20848E6}">Sophos Ltd End User Undertaking</option>
	<option value="{C17488FE-0E41-41CB-8324-187840EA7039}">Sophos Website Terms of Use</option>
	<option value="{95DA0A2B-E659-4B5D-BA44-91363601A296}">Sophos Professional Services Terms and Conditions</option>
	<option value="{7037EC3A-8CC5-485A-8C1B-F19FDA077D15}">Sophos Cookie Information</option>
	<option value="{D7FAB152-3D35-4DA3-913E-9521B6BA4557}">Export Information on Sophos Products</option>
	<option value="{6362B8E4-C078-4925-A522-A2DD3D20A8E9}">Sophos Group Privacy Policy</option>
	<option value="{E0C0B95F-D55E-4A5E-AEC1-FC260316AB78}">Sophos Group plc Anti-corruption and Bribery Policy</option>
	<option value="{0AB8D62B-0147-4610-A25E-C29CD057D8E2}">SophosLabs Information Security Policy</option>
	<option value="{E6E8210C-485C-457F-A1EF-938C552E5766}">Employee Personal Use Policy</option>
	<option value="{09E40D1A-646C-4150-B0C6-B6EB94C93058}">Sophos Product Classifications</option>

</select>
</div>
<h3><i class="icon-sophos-shield"></i> Contact Us</h3>
        <div class="well">
            <p style="text-align: center;">The Data Controller for the personal data that we collect is:<br />
Sophos Limited<br />
The Pentagon, Abingdon Science Park,<br />
Abingdon, Oxfordshire, OX14 3YP<br />
United Kingdom<br />
Incorporated in England and Wales  number 2096520<br />
VAT number: GB 991 2418 08</p>
<p>If you wish to unsubscribe from promotional emails, please email: <a href="mailto:unsubscribe@sophos.com">unsubscribe@sophos.com</a>.</p>
<p>If you wish to request information about the personal data we hold about you, or to correct/update your personal data, please email <a href="mailto:customerservice@sophos.com">customerservice@sophos.com</a>. A nominal charge of &pound;10 may be made to cover administrative costs involved.</p>
<p>
If you are in Singapore and would like to contact the Singapore Data Protection Officer, please email <a href="mailto:SingaporeDPO@Sophos.com">SingaporeDPO@Sophos.com</a>.</p>
        </div>
      </div>
    </div>
  </div>
</div>
    <!-- START Main Content Area -->
    
    <!-- END Main Content Area -->
    

<script type="text/javascript">
    $jq(document).ready(function () {
        var menuCloseDelay;
        function registerFooterPopupEvents($navitem, $menu) {
            //Stop contextual clicks from going up to the body, allowing these to stay open
            $navitem.click(function (event) { event.stopPropagation(); });
            $menu.click(function (event) { event.stopPropagation(); });

            // Navigation item mouse enter
            $navitem.mouseenter(function () {
                //console.log('mouseenter', $navitem);
                clearTimeout(menuCloseDelay); // clear any timeout
                // if this menu is not visible
                if (!$menu.is(':visible')) {
                    $('.popup').hide(); // hide all popups
                    $menu.fadeIn(100); // show this popup
                }
            });
            // Popup mouse enter
            $menu.mouseenter(function () {
                //console.log('mouseenter', $menu);
                clearTimeout(menuCloseDelay);
            });
            // Navigation item mouse leave
            $navitem.mouseleave(function () {
                //console.log('mouseleave', $menu);
                menuCloseDelay = setTimeout(function () {
                    $menu.fadeOut(100);
                }, 1500);
            });
            // Popup mouse leave
            $menu.mouseleave(function () {
                //console.log('mouseleave', $menu);
                menuCloseDelay = setTimeout(function () {
                    $menu.fadeOut(100);
                }, 1500);
            });
        }

        registerFooterPopupEvents($('.global-footer'), $('#popup-global-footer'));
    });
</script>


        <div id="footer" class="alt-footer">
            <div class="container clear">
                
        <div style='width:'>
            <h5></h5>
            
                    <ul>
                        
                    <li iconurl="/en-us/medialibrary/SophosNext/Images/Social/facebook-icon.png?la=en">
						<a href="http://www.facebook.com/securitybysophos">

							Facebook
						</a>
                    </li>
                
                    <li iconurl="/en-us/medialibrary/SophosNext/Images/Social/twitter-icon.png?la=en">
						<a href="https://twitter.com/Sophos_News">

							Twitter
						</a>
                    </li>
                
                    <li iconurl="/en-us/medialibrary/SophosNext/Images/Social/youtube-icon.png?la=en">
						<a href="http://www.youtube.com/sophoslabs">

							Youtube
						</a>
                    </li>
                
                    </ul>
                
        </div>           
    
        <div style='width:'>
            <h5>Popular</h5>
            
                    <ul>
                        
                    <li iconurl="">
						<a href="/en-us/products/free-trials.aspx">

							Free Trials
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/products/free-tools.aspx">

							Free Tools
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/security-news-trends/whitepapers.aspx">

							Whitepapers
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/why-sophos/our-people/technical-papers.aspx">

							Technical Papers
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/products/buy-now/buy-sophos-online.aspx">

							Buy Online
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="https://shop.sophos.com/" target="_blank">

							Sophos Brand Store
						</a>
                    </li>
                
                    </ul>
                
        </div>           
    
        <div style='width:'>
            <h5>Community</h5>
            
                    <ul>
                        
                    <li iconurl="">
						<a href="http://blogs.sophos.com/">

							Sophos Blog
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/company/community.aspx">

							Social Networks
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="http://nakedsecurity.sophos.com">

							Naked Security News
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/company/podcasts.aspx">

							Podcasts
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/about-us/rss-feeds.aspx">

							RSS
						</a>
                    </li>
                
                    </ul>
                
        </div>           
    
        <div style='width:'>
            <h5>Work With Us</h5>
            
                    <ul>
                        
                    <li iconurl="">
						<a href="/en-us/partners/partner-application.aspx">

							Become a Partner
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="http://partnerportal.sophos.com/">

							Partner Portal (login)
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/partners/resellers.aspx">

							Resellers
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/partners.aspx">

							Tech Partners
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/partners/oem-and-technology.aspx">

							OEM
						</a>
                    </li>
                
                    </ul>
                
        </div>           
    
        <div style='width:'>
            <h5>About Sophos</h5>
            
                    <ul>
                        
                    <li iconurl="">
						<a href="/en-us/company/careers.aspx">

							Jobs/Careers
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/products.aspx">

							Products
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/about-us/site-feedback.aspx">

							Feedback
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/company/contact.aspx">

							Contact Us
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/company/press.aspx">

							Press
						</a>
                    </li>
                
                    </ul>
                
        </div>           
    
        <div style='width:'>
            <h5>Support</h5>
            
                    <ul>
                        
                    <li iconurl="">
						<a href="/en-us/support/technical-support.aspx">

							Extended Warranties
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/support/knowledgebase.aspx">

							Knowledgebase
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="http://www.sophos.com/en-us/support/downloads.aspx">

							Downloads & Updates
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/support/documentation.aspx">

							Documentation
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/support/professional-services.aspx">

							Professional Services
						</a>
                    </li>
                
                    <li iconurl="">
						<a href="/en-us/about-us/training.aspx">

							Training
						</a>
                    </li>
                
                    </ul>
                
        </div>           
    
          </div>
        </div>
    



<!-- Logo and legal -->
<div id="legal" class="alt-footer">
    <div class="container clear">
        <div id="footer-logo">
            <a href="/en-us/">
                <img src="/en-us/medialibrary/SophosNext/Images/Callouts/sophos-logo-footer.png?la=en" alt="Sophos" width="73" height="15" />
            </a>
            <p>
                &copy;
                1997
                - 2016
                Sophos Ltd. All rights reserved.
            </p>
        </div>

        <ul>
            <li id="globalLanguageLink" class="global-footer">
                    <img src="/SophosNext/Assets/Images/footer-globe.png" class="img-footer-globe"/>
                
                <a href="#globalsitesfooter">
                    English
                </a>
                    <img src="/SophosNext/Assets/Images/footer-arrow.png" class="img-footer-arrow"/>

                <!-- Global Sites Popup -->
                
                        <div id="popup-global-footer" class="popup">
                            <div class="popup-wrap">
                                <div class="popup-content">
                                    <ul>
                                        
                        <li>
                            
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl0_LanguageHyperlink" href="/zh-cn/legal/sophos-group-privacy-policy.aspx">简体中文</a>
                        </li>
                    
                        <li>
                            <img src="/SophosNext/Assets/Images/footer-check.png" id="ctl06_lstvGlobalLanguageFooter_ctrl1_LanguageSelectedTick" class="selected" />
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl1_LanguageHyperlink" href="/en-us/legal/sophos-group-privacy-policy.aspx">English</a>
                        </li>
                    
                        <li>
                            
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl2_LanguageHyperlink" href="/fr-fr/legal/sophos-group-privacy-policy.aspx">Français</a>
                        </li>
                    
                        <li>
                            
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl3_LanguageHyperlink" href="/de-de/legal/sophos-group-privacy-policy.aspx">Deutsch</a>
                        </li>
                    
                        <li>
                            
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl4_LanguageHyperlink" href="/it-it/legal/sophos-group-privacy-policy.aspx">Italiano </a>
                        </li>
                    
                        <li>
                            
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl5_LanguageHyperlink" href="/ja-jp/legal/sophos-group-privacy-policy.aspx">日本語</a>
                        </li>
                    
                        <li>
                            
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl6_LanguageHyperlink" href="http://kr.sophos.com/">한국의</a>
                        </li>
                    
                        <li>
                            
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl7_LanguageHyperlink" href="/es-es/legal/sophos-group-privacy-policy.aspx">Español</a>
                        </li>
                    
                        <li>
                            
                            <a id="ctl06_lstvGlobalLanguageFooter_ctrl8_LanguageHyperlink" href="http://tw.sophos.com/">繁體中文</a>
                        </li>
                    
                                    </ul>
                                </div>
                                <!-- /popup-content -->
                            </div>
                            <!-- /popup-wrap -->
                            <span class="arrow"></span>
                        </div>
                    
                <!-- /Global Sites Popup -->
            </li>

            
                    
                    <li>
                        <a href="/en-us/legal.aspx">
                            Legal
                        </a>
                    </li>
                
                    <li>
                        <a href="/en-us/legal/sophos-group-privacy-policy.aspx">
                            Privacy
                        </a>
                    </li>
                
                    <li>
                        <a href="/en-us/legal/cookie-information.aspx">
                            Cookie Information
                        </a>
                    </li>
                
                

        </ul>


    </div>
</div>





    <div id="cboxOverlay" style="opacity: 1; cursor: auto; visibility: visible; display: none;">
    </div>
    <div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none; visibility: visible;
        top: 378px; left: 248px; position: absolute; width: 842px; height: 670px; opacity: 1;
        cursor: auto;">
        <div id="cboxWrapper" style="height: 670px; width: 842px;">
            <div>
                <div id="cboxTopLeft" style="float: left;">
                </div>
                <div id="cboxTopCenter" style="float: left; width: 800px;">
                </div>
                <div id="cboxTopRight" style="float: left;">
                </div>
            </div>
            <div style="clear: left;">
                <div id="cboxMiddleLeft" style="float: left; height: 628px;">
                </div>
                <div id="cboxContent" style="float: left; width: 800px; height: 628px;">
                    <div id="cboxTitle" style="float: left; display: block;">
                    </div>
                    <div id="cboxCurrent" style="float: left; display: block;">
                        1 of 5</div>
                    <button type="button" id="cboxPrevious" style="display: block;">
                        previous</button>
                    <button type="button" id="cboxNext" style="display: block;">
                            next</button>
                    <button id="cboxSlideshow" style="display: none;">
                            </button>
                    <div id="cboxLoadingOverlay" style="float: left; display: none;">
                    </div>
                    <div id="cboxLoadingGraphic" style="float: left; display: none;">
                    </div>
                    <button type="button" id="cboxClose" style="display: inline-block;">
                        close</button></div>
                <div id="cboxMiddleRight" style="float: left; height: 628px;">
                </div>
            </div>
            <div style="clear: left;">
                <div id="cboxBottomLeft" style="float: left;">
                </div>
                <div id="cboxBottomCenter" style="float: left; width: 800px;">
                </div>
                <div id="cboxBottomRight" style="float: left;">
                </div>
            </div>
        </div>
        <div style="position: absolute; width: 9999px; visibility: hidden; display: none;">
        </div>
    </div>
    

<script type="text/javascript">
//<![CDATA[
//]]>
</script>
</form>
    
    <!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TSJ3R7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TSJ3R7');</script>
<!-- End Google Tag Manager -->

<!-- Google Code for Remarketing tag -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 987433111;
var google_conversion_label = "4PJiCJnuhgUQl5Hs1gM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/987433111/?value=1.00&amp;currency_code=USD&amp;label=4PJiCJnuhgUQl5Hs1gM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Demandbase plug-in for Google Analytics -->
<script>
(function(d,b,a,s,e){ var t = b.createElement(a),
    fs = b.getElementsByTagName(a)[0]; t.async=1; t.id=e;
    t.src=('https:'==document.location.protocol ? 'https://' : 'http://') + s;
    fs.parentNode.insertBefore(t, fs); })
(window,document,'script','scripts.demandbase.com/WuEFNglz.min.js','demandbase_js_lib');
</script>
<!-- End Demandbase plug-in for Google Analytics -->


<!--  Quantcast Tag -->
<script>
       (function(){
       var elem = document.createElement('script');
       elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://pixel") + ".quantserve.com/aquant.js?a=p-LnrnBan1rwZpH";
       elem.async = true;
       elem.type = "text/javascript";
       var scpt = document.getElementsByTagName('script')[0];
       scpt.parentNode.insertBefore(elem,scpt);
     }());


   var smarttagdata={qacct: 'p-LnrnBan1rwZpH',
                        orderid: '',
                        revenue: '',
                        };
</script>
  <noscript>
    <img src="//pixel.quantserve.com/pixel/p-LnrnBan1rwZpH\.gif?labels=_fp.event.Default;" style="display: none;" border="0" height="1" width="1" alt="Quantcast"/>
  </noscript>
  
  
  <script>
smarttagdata.orderid='INSERT+ORDERID';
smarttagdata.revenue='INSERT+REVENUE';
</script>

<!-- End Quantcast Tag -->


<!-- Chango Pixel Tag -->
<script type="text/javascript">
    var __cho__ = {"pid":5532};
    (function() {
        var c = document.createElement('script');
        c.type = 'text/javascript';
        c.async = true;
        c.src = document.location.protocol + '//cc.chango.com/static/o.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(c, s);
    })();
</script>
<!-- End of Chango Pixel Tag -->


    <!-- Footer scripts Start -->
    
    <!-- Footer scripts End -->
</body>
</html>
