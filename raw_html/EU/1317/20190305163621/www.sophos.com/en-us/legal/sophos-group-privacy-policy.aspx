

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="headTag"><meta http-equiv="Content-Type" content="text/html;charset=utf-8" /><meta id="ViewportMetaTag" name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="/fairfax/sophosfavicon.ico" rel="shortcut icon" type="image/x-icon" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-556N8KS');</script>
<!-- End Google Tag Manager -->

<!-- Global Script Tags -->
<script src="/scripts/bundles/global.js?v=1" type="text/javascript"></script>
<!-- End Global Script Tags -->

<!--  Global Styles Box -->
<link href="/Styles/Bundles/Global.css?v=1" type="text/css" rel="stylesheet"></link>
<!-- End Global Styles -->

<!-- START of Eloqua tracking script -->
<script type="text/javascript">
    var _elqQ = _elqQ || [];
    _elqQ.push(['elqSetSiteId', '1777052651']);
    _elqQ.push(['elqTrackPageView']);
       
   (function async_load() {
    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
    s.src = '//img03.en25.com/i/elqCfg.min.js';
    var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
  })();

</script>
<!-- END of Eloqua tracking script -->


<!-- Start Visual Website Optimizer Synchronous Code -->
<script type='text/javascript'>
var _vis_opt_account_id = 25349;
var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
document.write('<s' + 'cript src="' + _vis_opt_protocol +
'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a='+_vis_opt_account_id+'&url='
+encodeURIComponent(document.URL)+'&random='+Math.random()+'" type="text/javascript">' + '<\/s' + 'cript>');
</script>

<script type='text/javascript'>
if(typeof(_vis_opt_settings_loaded) == "boolean") { document.write('<s' + 'cript src="' + _vis_opt_protocol +
'd5phz18u4wuww.cloudfront.net/vis_opt.js" type="text/javascript">' + '<\/s' + 'cript>'); }
/* if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above */
</script>

<script type='text/javascript'>
if(typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
        _vis_opt_top_initialize(); vwo_$(document).ready(function() { _vis_opt_bottom_initialize(); });
}
</script>
<!-- End Visual Website Optimizer Synchronous Code --><script type="text/javascript">if (typeof readCampaignAndWriteToCookie== 'function') {   readCampaignAndWriteToCookie(); }function GetCookie(k){return(document.cookie.match('(^|; )'+k+'=([^;]*)')||0)[2]}var campaignId = GetCookie('CampaignID');var _gaq = _gaq || []; _gaq.push(['_setAccount', 'UA-737537-1'],['_setDomainName', '.sophos.com'],['_setAllowLinker', true],['_setAllowHash', false],['_setCustomVar', 4, 'CampaignID', campaignId, 3],['_trackPageview'],['o._setAccount', 'UA-737537-18'],['o._setDomainName', '.sophos.com'],['o._setAllowLinker', true],['o._setAllowHash', false],['o._setCustomVar', 4, 'CampaignID', campaignId, 3],['o._trackPageview']);(function() {var ga = document.createElement('script');ga.type = 'text/javascript'; ga.async = true;ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);})();</script><script type="text/javascript" src="/en-us/medialibrary/scripts/tracking/nonhtmltracking.js"></script><script src="/en-us/medialibrary/Scripts/Tracking/gainjectmin.js?v=1" type="text/javascript"></script><script type="text/javascript">/* Instantiate and execute */$(function () {    window.sophosGaGuidStore.processGaToken({        useGoogleVid: true,        gaScope: 1,        gaNamespace: 'o',        gaAccount: 'UA-737537-18'    });});</script><style>
ul#productPrivacy-tabs {	background: none;}
.responsive-legal-detail ul#productPrivacy-tabs li.tabs-title {    background: no-repeat;    padding-left: 0;}
.responsive-legal-detail h1 {    margin-bottom: .3em;}
.responsive-legal-detail h2 {    opacity: 1;    margin-bottom: .5em;}
ul#productPrivacy-tabs li a { padding: 0; margin-bottom: 15px; font-size: 13px; color: #9b9b9b; font-weight: bold; line-height: normal; padding-right: 25px; margin-right: 15px;}
ul#productPrivacy-tabs .tabs-title>a:focus, ul#productPrivacy-tabs .tabs-title>a[aria-selected='true'] {	background: url(medialibrary/SophosNext/Images/BrandGuidelines/lnav-selected.png) no-repeat 95% center;	color: #0090dd; outline:none; }
.tabs-content {	background: none;	border-bottom: none;	border-right: none;}
@media print, screen and (max-width: 40em){
.tabs-content{ border-left: none;}
.tabs-panel.padding-left-2 { padding-left: 0!important;}}
</style>


<script type="text/javascript">
$(document).ready(function(){
//$('.tabs-panel a[href="#Contact"]').click(function(){	$('#tab13-label').click();	return false;});
//$('.tabs-panel a[href="#MarketingAndPromotions"]').click(function(){	$('#tab2-label').click(); return false;});

$( ".tabs-panel a[href*='#']" ).click(function(){ 	
	var anchorId = $(this).attr('href'); 
	if(anchorId == '#Contact'){$('#tab13-label').click();}
	if(anchorId == '#MarketingAndPromotions'){$('#tab2-label').click();}
	$('html,body').animate({  scrollTop: $(anchorId).offset().top - 60 }, 500); 	return false; 		
});

});	
</script>

    <!-- Head scripts Start -->
    
    <!-- Head scripts End -->
    <title>
	Sophos Group Privacy Policy - Legal
</title><!-- current language iso code for locale specific js --><script type="text/javascript">function getSitecoreCurrentLocale() { var locale = 'en-US'; if (locale == '') { return 'en'; } return locale; }</script></head>
<body id="Body">
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-556N8KS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <form name="form1" method="post" action="/en-us/legal/sophos-group-privacy-policy.aspx" id="form1">
<div>
<input type="hidden" name="ToolkitScriptManager1_HiddenField" id="ToolkitScriptManager1_HiddenField" value="" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="0YTvHifUQ45A3S6elWmR7zPdlBAfzTxckAtYsJK39VNsuNqxAnn5B7g6fUZmO+YuFfHzF2Kp/pjZjxEliCF1bKHsJSFLzegQk8/MvFPl1hJi8HCZggloPrV/UVc04e01AW2bIN/11G4lDd/thlDs84LDG28sGtB+HEH61pmxEHezIozCVzcQ5K8m76KnHM748U9L9h5ulfhJSDcMpPmHzGc8JQ/FniUHs5RBVhUe7sCky2lrs2MxSfvVXt1206aUMTWmIp7D2vsTrkuuBlauYoWlMQNV3bwpTIzei+mEngSCkvoaVrRwCROHRlh+qmUfohx5MxNJFJPU8AceVMjci20gWuIqg5PpMCCzi0Hq5WglcQIoe2kaSZPOcPxRoTfGlkFOU0yIPfOD6CmFFWn2BMRkaz4T6v/sIMeRcrkL3V//q38Yq1PXjMCj6wlPvMmbv2gOVZwzbeAkcVKNZuOpRGIQsJKorawVdzuLt2jrS41/mci9lAoO7Lxk6D02hwhjQXJ+hoG57p3k3v/d5k6LEG++b0/EGhxGsEF3opQeSl0jrEqE/+7d0hGZ4r0UaLhBlYLM261+AoSEJ+ZcFuVjp3vs2AVfFRyk54fwpq6NR03rwkUToRS9RGfjMGV54jEFqXeobmGECJpxGcHv/tnP1sfaJSYB0RP9tOeIQGC8orufMg/MTZdH8LmFEVSm+GScQakrdL3KV5dTln+t8kXmK6XuVB+7Z+Fax9OfWZEAX9nUfxlSrUuz2h/FbdSy/YRuJ0fq5tOCWbazKHeeC+gyksk/51JJjkJ+xch2EM+9P3abLS4ksY+/VDqOE8b3aXfwcwnmRckZKF/O2u5XIx9QdxztLNrInEBmn46KDfh7AWvkRgOskuycHIIpDQKwkeoyiE2KY+M81FfPTcy8Mv4z9isUMqTDDrYjRV5i+Od6C4DLzrYH25WKfHFWAVBLYhIss8oTowju50yxXjZr2lFu01KKGvOWxHjKz/wuX5ldQuouQKivEe6bqkj8TTO1czEVEK/wqEN98XasAk2OR1CmCVlKxQ5j/Moii8lvo3x7hKLmEs2yr6aHI/vyqMAjCdVj8MRx4rfLgYB7XcbvVkIDYbomP4sYgo/dMuwh8p4xnFIc2Xpb" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=pynGkmcFUV13He1Qd6_TZDAws9IRN2gR7AF64Cjfq8LDKHNlqj_VIjlMq0-0h8pfehDW3w2&amp;t=636831699776808893" type="text/javascript"></script>


<script src="/WebResource.axd?d=3WTd45ClInYAUa1dBtg1D8xRhifDeL2nFti_2uQG5LOMEvNSoOatk53fEy_qWlBLQo47OUP9Yi3EApOWovEcfOS6fSPwotrAGxu3gIcyIdB-B-n1hLkacFCKbz9bsADPr2C-ABqfXiQSuxYH03JlGnq8hxWEDItgSbs3j1sUmWZpeRLM6y5gfkj7YfpkVfM27ps4yw2&amp;t=636857369880000000" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
//]]>
</script>

<script src="/ScriptResource.axd?d=uHIkleVeDJf4xS50Krz-yDXrSRMg40IweECFTLo08u4cIRYT5Kcxwcsfv2_OR5mkDMypSWAwVLXPFTp4mrHUt24eT2Xv9MFDmxIyb1r5MK_nqSJ_hWCksO_YsXKSn5xRdLdcoNiPf9pL8jDUFYfSPWYAZYQ1&amp;t=ffffffffcd368728" type="text/javascript"></script>
<script src="/ScriptResource.axd?d=Jw6tUGWnA15YEa3ai3FadLgsjHJldOXLROzbu3RNn1U7TufT5ds_3aWHAMZQPVKeL4RDee_0efPfrc2gQWiIJzKrDEFU5BqfoTPFxTPu-pLEFlPFNdGN9NGkb4obcmm92hPcwB6OTlYmi8fbj3RCrmIs8Hw1&amp;t=ffffffffcd368728" type="text/javascript"></script>
<div>

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="6959D167" />
</div>
    
    
    
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ToolkitScriptManager1', 'form1', [], [], [], 90, '');
//]]>
</script>

    
    
    
    
<link rel="stylesheet" href="https://static.cloud.coveo.com/searchui/v2.3679/css/CoveoFullSearch.css" />
<script src="https://static.cloud.coveo.com/searchui/v2.3679/js/CoveoJsSearch.min.js"></script>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        Coveo.SearchEndpoint.configureCloudV2Endpoint("", "xxfb8501be-7f6a-4979-a506-cfdf258e265d");

        initialiseCoveoSearch(".search-wrapper");
        initialiseCoveoSearch(".responsive-search");
        initialiseCoveoSearch(".support-search-widget-wrapper", "Support");
    });

    function initialiseCoveoSearch(parentContainerClass, tabName) {
        if ($(parentContainerClass + " .CoveoSearchbox").children().length === 0) {
            var root = Coveo.$$(document).find(parentContainerClass + " .CoveoSearchInterface");
            var url = "https://search.sophos.com";
            if (tabName !== undefined) {
                url = url + "#t=" + tabName;
            }

            if (root == null) {
                return;
            }

            Coveo.initSearchbox(root, url);
        }
    }
</script>

<div class="nav" id="navigation">
    <div class="nav-wrapper responsive">               
        <script type='text/javascript'>window.__lc = window.__lc || {};

// choose correct license: 
// live license: 2462101; test license: 7472921;
window.__lc.license = 2462101;

(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();

var LC_API = LC_API || {};
LC_API.on_chat_window_opened = function()
{
    $(".icon-panel-close").trigger("click");
};

var fairfax = fairfax || {};

fairfax.openLiveChatFromFlyoutLink = function() {
    LC_API.open_chat_window();
};

fairfax.minimizeLiveChatWindow = function() {
    LC_API.minimize_chat_window();
};

$(document).ready(function() {
    $('.flyout-live-chat-link').click(function(){
        // assign ga tracking to the two links as on line below:
        // _gaq.push(['_trackPageview', '/products/live-chat/start.aspx']);
    });
});
</script>
<style>/*sitecore css for livechat in products nav*/
.secondary-navigation-livechat-link {
    display: inline-block;
}

.products-livechat-link i {
    display: inline-block;
    vertical-align: middle;
    margin-right: 2px;
}

.products-livechat-link i img{
    max-width: 25px;
    max-height: 25px;
    width: auto;
    height: auto;
}
    
.products-livechat-link .link-text {
    display: inline-block;
    vertical-align: middle;
    color: #58A4D5;
}

.secondary-navigation .products-livechat-link {
    border: 1px solid #6FB1DB;
    border-radius: 5px;
    padding: 1px 8px;
    margin-top: 13px;
    margin-left: 15px;
    line-height: 24px;
    display: block;
}

.secondary-navigation .products-livechat-link:hover {
    border: 1px solid #6FB1DB;
}
</style>

<div class="responsive-navigation-header">

    <div class="row small-collapse responsive-navigation-header-row">

        <h2 class="header-navigation-logo small-10 large-2 column">
            <a href="https://www.sophos.com/en-us.aspx">
                    <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/sophos-logo.svg?la=en&amp;hash=2EAC311AFB216B0944B8684B1ADFB2F62D613889" alt="" />
                </a>
        </h2>

        <div class="header-navigation-menu-reveal-wrapper small-1 column">
            <div class="header-navigation-menu-reveal">
                <img src="/en-us/medialibrary/SophosNext/Images/SophosHomePage/navigation-accordian-img2.svg?h=34&amp;&amp;w=34&amp;la=en&amp;hash=E623017F8FCE993A0D0FFC9C136127E691F490ED" class="header-navigation-menu-toggle open-menu" alt="" width="34" height="34" />
                <img src="/en-us/medialibrary/SophosNext/Images/SophosHomePage/nav-close-icon.png?h=24&amp;&amp;w=24&amp;la=en&amp;hash=06D8BC98ABE49E670EE31FFF118D5BEDEB6505FE" class="header-navigation-menu-toggle close-menu" alt="" width="24" height="24" />
            </div>
        </div>

        <div class="column header-navigation-list-container">

            <ul class="row small-collapse header-navigation-list-row">
                

                        
                            <li class='header-navigation-tab column'
                                data-index="0">
                                <a href="#" class="header-navigation-tab-link">
                                    Products<img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow.svg?la=en&amp;hash=BB88B95FC37993CA56DB77DE6631CAAFE543D8F7" class="icon-responsive-navigation-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-hover.svg?la=en&amp;hash=6D0F054F15C0A2E1B290349BE3117552E6DE770D" class="icon-responsive-navigation-header-arrow-active desktop-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-current.svg?la=en&amp;hash=D0AFFF3A66D0D362BA11EA142EA03A35A4CF1FE8" class="icon-responsive-navigation-header-arrow-current desktop-header-arrow" alt="" />
                                </a>
                            </li>
                            <div class="header-navigation-panel-container" data-index="0">
                                

<div class="header-navigation-panel">
    

        <div class="row collapse header-navigation-panel-row">

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_HeaderNavigationPanelLinkContainer" class="header-navigation-panel-link-container column large-9">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinkContainerPanel" class="header-navigation-link-container">
		
    <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationFeaturedLinks_HeaderNavigationFeaturedLinksPanel" class="header-navigation-featured-links row">
			

    

            <div class="column large-6 header-navigation-featured-link-wrapper">

                <a class="header-navigation-featured-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/next-gen-firewall.aspx">
                    
                        
                                <i class="header-navigation-featured-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/Featured-Links/xg-white.png?h=68&amp;&amp;w=116&amp;la=en&amp;hash=28AF39A7187E7257BF7C639FF23B13D5FE9C4F3E" class="header-navigation-featured-link-image" alt="" width="116" height="68" />
                                </i>
                            
                        
                        
                                <div class="header-navigation-featured-link-name">
                            XG Firewall
                                </div>
                            
                        
                        
                                <div class="header-navigation-featured-link-description">
                            Next-Gen Firewall
                                </div>
                            

                    </a>

            </div>

        

            <div class="column large-6 header-navigation-featured-link-wrapper">

                <a class="header-navigation-featured-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/intercept-x.aspx">
                    
                        
                                <i class="header-navigation-featured-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/Featured-Links/intercept-x-white.png?h=75&amp;&amp;w=78&amp;la=en&amp;hash=167AC0F4B3823CEDA39025376904E465E3BA9CA8" class="header-navigation-featured-link-image" alt="" width="78" height="75" />
                                </i>
                            
                        
                        
                                <div class="header-navigation-featured-link-name">
                            Intercept X
                                </div>
                            
                        
                        
                                <div class="header-navigation-featured-link-description">
                            Next-Gen Endpoint
                                </div>
                            

                    </a>

            </div>

        


		</div>
    <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksPanel" class="header-navigation-links row">
			

    

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl00_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/sophos-central.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/icon-central-promo.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=63B7A59ED8576E3183DFA2C5F432D0FD911F27DE" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Sophos Central
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl01_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/mobile-control.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/product-icon-sophos-mobile.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=CF5A91889EC28319F8BAA2AC1AC3BAA2686EEB94" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Sophos Mobile
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl02_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/server-security.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/product-icon-server-protection.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=3737BBF2286AC2EEF6288530766C548DE2AB218F" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Intercept X for Server
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl03_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/secure-wifi.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/product-icon-secure-wifi.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=DD7B00FAF94AB783DA1FACC19265F4E90FAF3AE5" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Sophos Wireless
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl04_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/phish-threat.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/product-icon-phish-threat.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=0066CEF9854AF61547709967D1CFFFA6FE6A5407" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Phish Threat
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl05_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/safeguard-encryption.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/product-icon-safeguard-encryption.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=2886255472CDC2A4BDCB304150CF8AED858A72CA" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            SafeGuard Encryption
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl06_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/sophos-email.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/product-icon-secure-email-gateway.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=6AFC8955495664CB8E5C067F76C060FC5CD4706A" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Sophos Email
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl07_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/choose-firewall.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/product-icon-sg-utm.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=AA9954EE564039E7959C679CF92F339604CCDAF5" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            UTM
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl08_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-4">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/secure-web-gateway.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Products/Icons/product-icon-secure-web-gateway.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=72401AF42E4794D37AFA92EC98F6DD0D9C69BCC0" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Secure Web Gateway
                                </div>
                            

                        

                    </a>

            
			</div>

        


		</div>

	</div>
            
</div><div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_HeaderNavigationPanelCTAContainer" class="header-navigation-panel-cta-container column large-3">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_CTAContainer_HeaderNavigationCTAContainerPanel" class="header-navigation-cta-container">
		

    
            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_CTAContainer_HeaderNavigationCTARepeater_ctl00_HeaderNavigationCTAOuterPanelInner_HeaderNavigationCTAInnerPanel" class="header-navigation-cta-inner header-navigation-cta-large-font column large-12">
			


    <a rel="external" href="https://home.sophos.com">

            
                    <i class="header-navigation-cta-icon">
                
                    </i>
                

        </a>

    <a rel="external" href="https://home.sophos.com">

            
                    <div class="header-navigation-cta-title">
                For Home Users
                    </div>
                

        </a>

    <a rel="external" href="https://home.sophos.com">


            
                    <div class="header-navigation-cta-description">
                Sophos Home protects every Mac and PC in your home
                    </div>
                

        </a>

    <a class="header-navigation-cta-container-button" rel="external" href="https://home.sophos.com">

            Learn More

        </a>


		</div>

        


	</div>
            
</div>

        </div>

        <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_HeaderNavigationLinkBar_HeaderNavigationLinkBarPanel" class="header-navigation-link-bar row">
	

    

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_HeaderNavigationLinkBar_HeaderNavigationLinkBarRepeater_ctl00_HeaderNavigationLinkBarLinkPanel" class="header-navigation-link-bar-link-wrapper column large-3">
		

                <a class="header-navigation-link-bar-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/free-tools.aspx">

                        
                                <i class="header-navigation-link-bar-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/Link-Bar/icon-free-security-tools.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=B296AE5533D67AA5E63F8FBD353DC5308B675F81" class="header-navigation-link-bar-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-bar-link-name">
                            Free Security Tools
                                </div>
                            

                        

                    </a>

            
	</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_HeaderNavigationLinkBar_HeaderNavigationLinkBarRepeater_ctl01_HeaderNavigationLinkBarLinkPanel" class="header-navigation-link-bar-link-wrapper column large-3">
		

                <a class="header-navigation-link-bar-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/products/free-trials.aspx">

                        
                                <i class="header-navigation-link-bar-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/Link-Bar/icon-free-trials.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=163D104CDCCC071B2A7742F0E3AD0FC38197D77E" class="header-navigation-link-bar-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-bar-link-name">
                            Free Trials
                                </div>
                            

                        

                    </a>

            
	</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_HeaderNavigationLinkBar_HeaderNavigationLinkBarRepeater_ctl02_HeaderNavigationLinkBarLinkPanel" class="header-navigation-link-bar-link-wrapper column large-3">
		

                <a class="header-navigation-link-bar-link" HideIfEmpty="true" href="https://secure2.sophos.com/en-us/products/demos.aspx">

                        
                                <i class="header-navigation-link-bar-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/Link-Bar/icon-product-demos.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=EA75421C28376B6F83524BA489280D6B72FA9399" class="header-navigation-link-bar-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-bar-link-name">
                            Product Demos
                                </div>
                            

                        

                    </a>

            
	</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl00_ctl02_HeaderNavigationLinkBar_HeaderNavigationLinkBarRepeater_ctl03_HeaderNavigationLinkBarLinkPanel" class="header-navigation-link-bar-link-wrapper column large-3">
		

                <a class="header-navigation-link-bar-link" HideIfEmpty="true" href="javascript:fairfax.openLiveChatFromFlyoutLink();">

                        
                                <i class="header-navigation-link-bar-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/Link-Bar/icon-chat.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=901E7AAAC54B472119A9E268B3EE71D8AC91E6DD" class="header-navigation-link-bar-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-bar-link-name">
                            Live Sales Chat
                                </div>
                            

                        

                    </a>

            
	</div>

        


</div>
</div>
                            </div>
                        

                    

                        
                            <li class='header-navigation-tab column'
                                data-index="1">
                                <a href="#" class="header-navigation-tab-link">
                                    Solutions<img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow.svg?la=en&amp;hash=BB88B95FC37993CA56DB77DE6631CAAFE543D8F7" class="icon-responsive-navigation-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-hover.svg?la=en&amp;hash=6D0F054F15C0A2E1B290349BE3117552E6DE770D" class="icon-responsive-navigation-header-arrow-active desktop-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-current.svg?la=en&amp;hash=D0AFFF3A66D0D362BA11EA142EA03A35A4CF1FE8" class="icon-responsive-navigation-header-arrow-current desktop-header-arrow" alt="" />
                                </a>
                            </li>
                            <div class="header-navigation-panel-container" data-index="1">
                                

<div class="header-navigation-panel">
    

        <div class="row collapse header-navigation-panel-row">

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_HeaderNavigationPanelLinkContainer" class="header-navigation-panel-link-container column large-8">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_LinkContainer_HeaderNavigationLinkContainerPanel" class="header-navigation-link-container">
		
    
    <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksPanel" class="header-navigation-links row">
			

    

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl00_HeaderNavigationLinkPanel" class="header-navigation-link-with-description-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/solutions/industries.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/industries-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=90CF12BBDE8031EBB8CEAADD4F92EA9A5FD2C8BB" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Industries
                                </div>
                            

                        
                                <div class="header-navigation-link-description">
                            Your industry. Our expertise.
                                </div>
                            

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl01_HeaderNavigationLinkPanel" class="header-navigation-link-with-description-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/solutions/oem-solutions.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/oem-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=B0940AC6E95061CEB8A0CA212CF58B252E7603DA" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            OEM Solutions
                                </div>
                            

                        
                                <div class="header-navigation-link-description">
                            Trusted by world-leading brands.
                                </div>
                            

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl02_HeaderNavigationLinkPanel" class="header-navigation-link-with-description-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/solutions/initiatives.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/it-initiaties.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=EE0A64F38DC50C21230CD48B3D3D23F22FB05A70" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            IT Initiatives
                                </div>
                            

                        
                                <div class="header-navigation-link-description">
                            Embrace IT initiatives with confidence.
                                </div>
                            

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl03_HeaderNavigationLinkPanel" class="header-navigation-link-with-description-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/support/professional-services.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/professional-services-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=086B218B8D462F7F1DF5389D37720036ABDE8A37" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Professional Services
                                </div>
                            

                        
                                <div class="header-navigation-link-description">
                            Our experience. Your peace of mind.
                                </div>
                            

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl04_HeaderNavigationLinkPanel" class="header-navigation-link-with-description-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/solutions/compliance.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/compliance-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=8CBFC486E7F8328C9D498FED68019BCB41DEA609" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Compliance
                                </div>
                            

                        
                                <div class="header-navigation-link-description">
                            Helping you to stay regulatory compliant.
                                </div>
                            

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl05_HeaderNavigationLinkPanel" class="header-navigation-link-with-description-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/solutions/public-cloud.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/public-cloud-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=32311DBF6388135E3B718B7384FE34EB3B46086B" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Public Cloud
                                </div>
                            

                        
                                <div class="header-navigation-link-description">
                            Stronger, simpler cloud security.
                                </div>
                            

                    </a>

            
			</div>

        


		</div>

	</div>
            
</div><div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_HeaderNavigationPanelCTAContainer" class="header-navigation-panel-cta-container column large-4">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_CTAContainer_HeaderNavigationCTAContainerPanel" class="header-navigation-cta-container header-navigation-wider-cta-container">
		

    
            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl01_ctl02_CTAContainer_HeaderNavigationCTARepeater_ctl00_HeaderNavigationCTAOuterPanelInner_HeaderNavigationCTAInnerPanel" class="header-navigation-cta-inner column large-12">
			


    <a href="https://www.sophos.com/en-us/labs.aspx">

            
                    <i class="header-navigation-cta-icon">
                <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/sophos-labs-logo.svg?la=en&amp;hash=0466861D4218CE8964C5B0BE4EB21695CA9D02AC" alt="" />
                    </i>
                

        </a>

    <a href="https://www.sophos.com/en-us/labs.aspx">

            

        </a>

    <a href="https://www.sophos.com/en-us/labs.aspx">


            
                    <div class="header-navigation-cta-description">
                Behind the scenes of our 24/7 security.
                    </div>
                

        </a>

    <a class="header-navigation-cta-container-button" href="https://www.sophos.com/en-us/labs.aspx">

            Read Our Research

        </a>


		</div>

        


	</div>
            
</div>

        </div>

        
</div>
                            </div>
                        

                    

                        
                            <li class='header-navigation-tab column'
                                data-index="2">
                                <a href="#" class="header-navigation-tab-link">
                                    Partners<img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow.svg?la=en&amp;hash=BB88B95FC37993CA56DB77DE6631CAAFE543D8F7" class="icon-responsive-navigation-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-hover.svg?la=en&amp;hash=6D0F054F15C0A2E1B290349BE3117552E6DE770D" class="icon-responsive-navigation-header-arrow-active desktop-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-current.svg?la=en&amp;hash=D0AFFF3A66D0D362BA11EA142EA03A35A4CF1FE8" class="icon-responsive-navigation-header-arrow-current desktop-header-arrow" alt="" />
                                </a>
                            </li>
                            <div class="header-navigation-panel-container" data-index="2">
                                

<div class="header-navigation-panel">
    

        <div class="row collapse header-navigation-panel-row">

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_HeaderNavigationPanelLinkContainer" class="header-navigation-panel-link-container column large-6">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_LinkContainer_HeaderNavigationLinkContainerPanel" class="header-navigation-link-container header-navigation-link-container-alternate-colours">
		
    
    <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksPanel" class="header-navigation-links row">
			

    

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl00_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/partners.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/partner-program-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=EBF52E5DB8515F2405D1ADF39C5EBA7FE2A5B4BD" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Partner Program
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl01_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/partners/managed-service-providers.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/msp-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=96893C037B261B88CAF398287C6D8DD46F5AEFAC" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            MSP
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl02_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/partners/resellers.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/resellers-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=D01D2C211A7D4608A8099875AAEFA5FBED17DFE5" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Resellers
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl03_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/partners/cloud-security-provider.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/cloud-security-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=E3E85508DA6DA5A80AAA17BCD3D2F8AA422648EC" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Cloud Security
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl04_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/solutions/oem-solutions.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/oem-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=B0940AC6E95061CEB8A0CA212CF58B252E7603DA" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            OEM
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl05_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/about-us/training/partner-certification.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/partner-training-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=33CA82C27AF07BB61F0B99194A588076D47FD92E" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Partner Training
                                </div>
                            

                        

                    </a>

            
			</div>

        


		</div>

	</div>
            
</div><div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_HeaderNavigationPanelCTAContainer" class="header-navigation-panel-cta-container column large-6">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_CTAContainer_HeaderNavigationCTAContainerPanel" class="header-navigation-cta-container header-navigation-cta-container-alternate-colours">
		

    
            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_CTAContainer_HeaderNavigationCTARepeater_ctl00_HeaderNavigationCTAOuterPanelInner_HeaderNavigationCTAInnerPanel" class="header-navigation-cta-inner column large-6">
			


    <a href="https://www.sophos.com/en-us/partners/partner-portal.aspx">

            
                    <i class="header-navigation-cta-icon">
                <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/partner-portal.svg?la=en&amp;hash=7BBDBACF753C24E34723C465016683A941550075" alt="" />
                    </i>
                

        </a>

    <a href="https://www.sophos.com/en-us/partners/partner-portal.aspx">

            
                    <div class="header-navigation-cta-title">
                Partner Portal
                    </div>
                

        </a>

    <a href="https://www.sophos.com/en-us/partners/partner-portal.aspx">


            
                    <div class="header-navigation-cta-description">
                Maximize your Sophos revenue, <br>all in one place.
                    </div>
                

        </a>

    


		</div>

        
            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl02_ctl02_CTAContainer_HeaderNavigationCTARepeater_ctl01_HeaderNavigationCTAOuterPanelInner_HeaderNavigationCTAInnerPanel" class="header-navigation-cta-inner column large-6">
			


    <a href="https://www.sophos.com/en-us/partners/partner-locator.aspx">

            
                    <i class="header-navigation-cta-icon">
                <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/partner-locator.svg?la=en&amp;hash=D04DEFA70AD4187FCF2795900FDD8CD83F1DBF85" alt="" />
                    </i>
                

        </a>

    <a href="https://www.sophos.com/en-us/partners/partner-locator.aspx">

            
                    <div class="header-navigation-cta-title">
                Partner Locator
                    </div>
                

        </a>

    <a href="https://www.sophos.com/en-us/partners/partner-locator.aspx">


            
                    <div class="header-navigation-cta-description">
                Find Sophos partners worldwide.
                    </div>
                

        </a>

    


		</div>

        


	</div>
            
</div>

        </div>

        
</div>
                            </div>
                        

                    

                        
                            <li class='header-navigation-tab column'
                                data-index="3">
                                <a href="#" class="header-navigation-tab-link">
                                    Support<img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow.svg?la=en&amp;hash=BB88B95FC37993CA56DB77DE6631CAAFE543D8F7" class="icon-responsive-navigation-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-hover.svg?la=en&amp;hash=6D0F054F15C0A2E1B290349BE3117552E6DE770D" class="icon-responsive-navigation-header-arrow-active desktop-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-current.svg?la=en&amp;hash=D0AFFF3A66D0D362BA11EA142EA03A35A4CF1FE8" class="icon-responsive-navigation-header-arrow-current desktop-header-arrow" alt="" />
                                </a>
                            </li>
                            <div class="header-navigation-panel-container" data-index="3">
                                

<div class="header-navigation-panel">
    

        <div class="row collapse header-navigation-panel-row">

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_HeaderNavigationPanelLinkContainer" class="header-navigation-panel-link-container column large-8">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_LinkContainer_HeaderNavigationLinkContainerPanel" class="header-navigation-link-container header-navigation-link-container-alternate-colours">
		
    
    <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksPanel" class="header-navigation-links row">
			

    

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl00_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://secure2.sophos.com/en-us/support.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/support-overview.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=2ADCD39566F96106D15EB2536136FAF6FF07BD1B" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Support Overview
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl01_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://secure2.sophos.com/en-us/support/documentation.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/documentation.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=83F7313034A7551F7905AD70F64CD97E17F9F9EF" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Documentation
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl02_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" rel="external" href="https://community.sophos.com/">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/community-forums.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=E6BD262B65E8D6B50BCF66B547BEDD8686719C23" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Ask the Community
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl03_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/support/technical-support.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/support-plans.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=9D0E49C37341D1638DB2E62CD83FFF58B5180813" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Support Packages
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl04_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/about-us/training.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/training.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=E80B67435FF8587AAA81F21B26C06F760DB76818" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Training
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl05_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="/en-us/sophos/support/downloads.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/downloads-updates.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=043C55070922E16591D78F00A2D3B43CD20D585E" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Downloads and Updates
                                </div>
                            

                        

                    </a>

            
			</div>

        


		</div>

	</div>
            
</div><div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_HeaderNavigationPanelCTAContainer" class="header-navigation-panel-cta-container column large-4">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_CTAContainer_HeaderNavigationCTAContainerPanel" class="header-navigation-cta-container header-navigation-cta-container-alternate-colours header-navigation-wider-cta-container">
		

    
            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl03_ctl02_CTAContainer_HeaderNavigationCTARepeater_ctl00_HeaderNavigationCTAOuterPanelInner_HeaderNavigationCTAInnerPanel" class="header-navigation-cta-inner column large-12">
			


    <a href="https://secure2.sophos.com/en-us/support/open-a-support-case.aspx">

            
                    <i class="header-navigation-cta-icon">
                <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/icon-support.svg?la=en&amp;hash=6FA9D3182176BD515265259657892AABD3745AB5" alt="" />
                    </i>
                

        </a>

    <a href="https://secure2.sophos.com/en-us/support/open-a-support-case.aspx">

            
                    <div class="header-navigation-cta-title">
                Technical Assistance
                    </div>
                

        </a>

    <a href="https://secure2.sophos.com/en-us/support/open-a-support-case.aspx">


            

        </a>

    <a class="header-navigation-cta-container-button" href="https://secure2.sophos.com/en-us/support/open-a-support-case.aspx">

            Open a Support Case

        </a>


		</div>

        


	</div>
            
</div>

        </div>

        
</div>
                            </div>
                        

                    

                        
                            <li class='header-navigation-tab column'
                                data-index="4">
                                <a href="#" class="header-navigation-tab-link">
                                    Company<img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow.svg?la=en&amp;hash=BB88B95FC37993CA56DB77DE6631CAAFE543D8F7" class="icon-responsive-navigation-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-hover.svg?la=en&amp;hash=6D0F054F15C0A2E1B290349BE3117552E6DE770D" class="icon-responsive-navigation-header-arrow-active desktop-header-arrow" alt="" /><img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-current.svg?la=en&amp;hash=D0AFFF3A66D0D362BA11EA142EA03A35A4CF1FE8" class="icon-responsive-navigation-header-arrow-current desktop-header-arrow" alt="" />
                                </a>
                            </li>
                            <div class="header-navigation-panel-container" data-index="4">
                                

<div class="header-navigation-panel">
    

        <div class="row collapse header-navigation-panel-row">

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_HeaderNavigationPanelLinkContainer" class="header-navigation-panel-link-container column large-8">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinkContainerPanel" class="header-navigation-link-container">
		
    
    <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksPanel" class="header-navigation-links row">
			

    

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl00_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/company.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/about-sophos-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=6D0364D6EA3B1FBF265917B6D2347BF42BABEEF5" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            About Sophos
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl01_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/company/social-media.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/social-media-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=34691F52DF5EE140CC2B73B979563D0EF6366069" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Social Media
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl02_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://investors.sophos.com/en-us.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/investors-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=5BEB4EA2A917609350B31BCF55D93BAC7FD7F04C" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Investors
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl03_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/company/careers.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/careers-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=58AF49B96F6BB844253741AE4C2087BA6463A185" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Careers
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl04_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/company/press.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/press-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=4FC602BF7CEE8064601FF84BB03A8C211F7C45D2" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Press
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl05_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/company/contact.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/contact-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=3041405BBC86C8AE37B35808FFDFB1C1680CF557" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Contact
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl06_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://www.sophos.com/en-us/company/events.aspx">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/upcoming-events-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=AC722584C8FB865DC44C0467A885F76B1524AFE0" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Events
                                </div>
                            

                        

                    </a>

            
			</div>

        

            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_LinkContainer_HeaderNavigationLinks_HeaderNavigationLinksRepeater_ctl07_HeaderNavigationLinkPanel" class="header-navigation-link-wrapper column large-6">
				

                <a class="header-navigation-link" HideIfEmpty="true" href="https://blogs.sophos.com/">

                        
                                <i class="header-navigation-link-icon">
                            <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/sophos-news-icon.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=BB31C2287877CAD5B7DFFA93DAAFA591DAABD016" class="header-navigation-link-image" alt="" />
                                </i>
                            

                        
                                <div class="header-navigation-link-name">
                            Sophos News
                                </div>
                            

                        

                    </a>

            
			</div>

        


		</div>

	</div>
            
</div><div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_HeaderNavigationPanelCTAContainer" class="header-navigation-panel-cta-container column large-4">
	
                <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_CTAContainer_HeaderNavigationCTAContainerPanel" class="header-navigation-cta-container header-navigation-wider-cta-container">
		

    
            <div id="ctl04_HeaderNavigation_HeaderNavigationTabsRepeater_ctl04_ctl02_CTAContainer_HeaderNavigationCTARepeater_ctl00_HeaderNavigationCTAOuterPanelInner_HeaderNavigationCTAInnerPanel" class="header-navigation-cta-inner column large-12">
			


    <a href="https://investors.sophos.com/en-us.aspx">

            
                    <i class="header-navigation-cta-icon">
                <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Main/investors-icon-large.svg?la=en&amp;hash=D0244F21D8CF057F438A1A93E96406832D5DCF0B" alt="" />
                    </i>
                

        </a>

    <a href="https://investors.sophos.com/en-us.aspx">

            
                    <div class="header-navigation-cta-title">
                Investor Information
                    </div>
                

        </a>

    <a href="https://investors.sophos.com/en-us.aspx">


            

        </a>

    <a class="header-navigation-cta-container-button" href="https://investors.sophos.com/en-us.aspx">

            Read our Financial Updates

        </a>


		</div>

        


	</div>
            
</div>

        </div>

        
</div>
                            </div>
                        

                    

                <li id="ctl04_HeaderNavigation_searchListItem" class="header-navigation-search small-12 column">
                    <a href="#search" class="header-navigation-search-link" id="searchLink">
                        <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/Search/header-search-icon.svg?la=en&amp;hash=C13CD0CCEF08DEB5AD396E5996A78C8A89554DE8" class="header-navigation-search-icon" alt="" />
                        <img src="/en-us/medialibrary/SophosNext/Images/Header-Navigation/Search/header-search-icon-hover.svg?la=en&amp;hash=B35DEF5AD6BAEEDA613BB7D87F3BD7584C9676EB" class="header-navigation-search-icon-hover" alt="" />
                    </a>
                </li>

                <li id="ctl04_HeaderNavigation_SignInListItemNav" class="header-navigation-tab header-navigation-sign-in-tab column" data-index="5">
                    <a href="#" class="header-navigation-tab-link header-navigation-sign-in-link">
                        <span id="ctl04_HeaderNavigation_SignInSpanNav">Sign in</span>
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow.svg?la=en&amp;hash=BB88B95FC37993CA56DB77DE6631CAAFE543D8F7" class="icon-responsive-navigation-header-arrow" alt="" />
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-hover.svg?la=en&amp;hash=6D0F054F15C0A2E1B290349BE3117552E6DE770D" class="icon-responsive-navigation-header-arrow-active desktop-header-arrow" alt="" />
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Menu-Arrows/header-arrow-current.svg?la=en&amp;hash=D0AFFF3A66D0D362BA11EA142EA03A35A4CF1FE8" class="icon-responsive-navigation-header-arrow-current desktop-header-arrow" alt="" />
                    </a>
                </li>

                <div class="header-navigation-panel-container" data-index="5">
                    

<div class="header-navigation-panel header-navigation-sign-in-panel">
    
    <div class="large-offset-9 large-3 header-navigation-link-container header-navigation-sign-in-links-container">
        
        <div class="header-navigation-links row">

            
                    <div class="header-navigation-link-wrapper column">
                        <div id="ctl04_HeaderNavigation_HeaderNavigationSignInDropDown_LinksRepeater_ctl00_TextAboveContainer" class="show-for-large">
                            
                                        <h3 class="header-navigation-sign-in-title">
                                APPS
                                        </h3>
                                
                        </div>

                        <a class="header-navigation-link" href="https://central.sophos.com">
                                
                                        <i class="header-navigation-link-icon">
                                    <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Sign-In/icon-central-promo.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=35E6174DBD8E4C48DE204B22151405C082F22789" class="header-navigation-link-image" alt="" />
                                        </i>
                                    
                                
                                
                                        <div class="header-navigation-link-name">
                                    Sophos Central
                                        </div>
                                    
                            </a>
                    </div>
                
                    <div class="header-navigation-link-wrapper column">
                        

                        <a class="header-navigation-link" href="https://home.sophos.com">
                                
                                        <i class="header-navigation-link-icon">
                                    <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Sign-In/icon-sophos-home.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=876D73315E24D8E3A59AC39DBC27295E5E52896D" class="header-navigation-link-image" alt="" />
                                        </i>
                                    
                                
                                
                                        <div class="header-navigation-link-name">
                                    Sophos Home
                                        </div>
                                    
                            </a>
                    </div>
                
                    <div class="header-navigation-link-wrapper column">
                        <div id="ctl04_HeaderNavigation_HeaderNavigationSignInDropDown_LinksRepeater_ctl02_TextAboveContainer" class="show-for-large">
                            
                                        <h3 class="header-navigation-sign-in-title">
                                SERVICES
                                        </h3>
                                
                        </div>

                        <a class="header-navigation-link" href="https://community.sophos.com">
                                
                                        <i class="header-navigation-link-icon">
                                    <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Sign-In/icon-support.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=97DBF58C1EDD1977FBBFE2EEA88074E26DF30B02" class="header-navigation-link-image" alt="" />
                                        </i>
                                    
                                
                                
                                        <div class="header-navigation-link-name">
                                    Support Community
                                        </div>
                                    
                            </a>
                    </div>
                
                    <div class="header-navigation-link-wrapper column">
                        

                        <a class="header-navigation-link" href="https://partnerportal.sophos.com">
                                
                                        <i class="header-navigation-link-icon">
                                    <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Sign-In/icon-partner-portal.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=E8FDE5B97BFBC17EC79E29CDCB9E995385298635" class="header-navigation-link-image" alt="" />
                                        </i>
                                    
                                
                                
                                        <div class="header-navigation-link-name">
                                    Partner Portal
                                        </div>
                                    
                            </a>
                    </div>
                
                    <div class="header-navigation-link-wrapper column">
                        

                        <a class="header-navigation-link" href="https://secure2.sophos.com/en-us/login.aspx">
                                
                                        <i class="header-navigation-link-icon">
                                    <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Sign-In/icon-licensing-portal.svg?&amp;mh=28&amp;mw=28&amp;la=en&amp;hash=FD7BAFE6FF961FD25A3C7CA5A191A64438C0E4DA" class="header-navigation-link-image" alt="" />
                                        </i>
                                    
                                
                                
                                        <div class="header-navigation-link-name">
                                    Licensing Portal<br />(MySophos)
                                        </div>
                                    
                            </a>
                    </div>
                

        </div>
        
        <div class="header-navigation-sign-in-footer-links-container row">
            
                    <div class="header-navigation-link-wrapper column">
                        <a class="header-navigation-link" rel="external" href="https://id.sophos.com/web/profile/">
                                
                                        <div class="header-navigation-link-name">
                                    Profile
                                        </div>
                                    
                            </a>
                    </div>
                
        </div>

        
    </div>

</div>

                </div>

                <li class="responsive-search header-navigation-drop-search small-12 column">
                    
                    

<div class="header-search CoveoSearchInterface" id="dropSearch">
    <div class="CoveoAnalytics"></div>
    <div class="wrapper">
        <div class="search-field-wrapper">
            <div id="searchbox">
                <div class="coveo-search-section">
                    <div class="CoveoSearchbox" data-placeholder="Search Sophos"></div>
                </div>
            </div>
        </div>
    </div>
</div>
 
                </li>

            </ul>

        </div>

    </div>

</div>

        
            
        <div id="ctl04_headerDropSearchCoveoSearchWrapper" class="search-wrapper headerSearchWrapperCoveo">
            

<div class="header-search CoveoSearchInterface" id="dropSearch">
    <div class="CoveoAnalytics"></div>
    <div class="wrapper">
        <div class="search-field-wrapper">
            <div id="searchbox">
                <div class="coveo-search-section">
                    <div class="CoveoSearchbox" data-placeholder="Search Sophos"></div>
                </div>
            </div>
        </div>
    </div>
</div>
 
        </div>    
    </div>
</div> 




<script type="text/javascript">
    var fairfaxQTipSettings = fairfaxQTipSettings || {};
    fairfaxQTipSettings.tertiary = { qtipElements: 'li.has-tertiary-dropdown', contentText: '.qtip-dropdown', adjustY: -11 }
</script>

<div class="responsive-secondary-navigation responsive">
    <div class="row large-collapse">
        <div class="small-12 large-2 column secondary-nav-wrapper">
            
            <span class="responsive-secondary-navigation-text">
                
            </span>
            <div class="responsive-page-name">
                <span></span>
                <i class="icon-responsive-secondary-nav-arrow"></i>
            </div>
        </div>
        <div class="large-10 column secondary-navigation-links-wrapper">
            <ul class="secondary-navigation-links">
                
                
        
            <li  class='link_fdb04fe1a91f4ab78cb1e913b0ef9561 '>
                <a class="selected" href="https://www.sophos.com/en-us/legal.aspx" style="">Overview</a>
            </li>
        
                
    
        
            <li  class='link_9daa6d8dd03c455cacb1bd666fc5ff54 '>
                <a href="https://www.sophos.com/en-us/company/contact.aspx" style="">Contact</a>
            </li>
        
                
    

                
                <div class="secondary-navigation-close">
                    <img src="/medialibrary/SophosNext/Images/Navigation/secondary-nav-close-icon.png" class="secondary-navigation-close-image" alt="" width="32" height="32">
                </div>
            </ul>
        </div>
    </div>
</div>
    <style>  .static-content-row-292dab0f-3f0a-461e-9468-00de3fc2e3a9 {
    background-image: none;
  }
@media print, screen and (min-width: 40em) {
  .static-content-row-292dab0f-3f0a-461e-9468-00de3fc2e3a9 {
    background-image: none;
  }
}
@media print, screen and (min-width: 64em) {
  .static-content-row-292dab0f-3f0a-461e-9468-00de3fc2e3a9 {
    background-image: none;
  }
}
</style>

<div class="static-content-row static-content-row-292dab0f-3f0a-461e-9468-00de3fc2e3a9 responsive " >
    

    <div class="row">

        
                <style>  .content-block-6d93a21e-4784-4dbf-98aa-7df2d8c0bdd8 {
    text-align: center;
  }
@media print, screen and (min-width: 40em) {
  .content-block-6d93a21e-4784-4dbf-98aa-7df2d8c0bdd8 {
    text-align: center;
  }
}
@media print, screen and (min-width: 64em) {
  .content-block-6d93a21e-4784-4dbf-98aa-7df2d8c0bdd8 {
    text-align: center;
  }
}
</style>




    <div class="column content-block content-block-6d93a21e-4784-4dbf-98aa-7df2d8c0bdd8 ">
        <div class="content-block-inner">

            
                    
                    
                    
                    
                    
					
                    
                    
                    <style>  .spacer-content-component-9eeca0ae-7581-4b2f-977a-de1d94443a34 {
    height: 40px;
    display: block;
  }
@media print, screen and (min-width: 40em) {
  .spacer-content-component-9eeca0ae-7581-4b2f-977a-de1d94443a34 {
    height: 40px;
    display: block;
  }
}
@media print, screen and (min-width: 64em) {
  .spacer-content-component-9eeca0ae-7581-4b2f-977a-de1d94443a34 {
    height: 40px;
    display: block;
  }
}
</style>

<span class="spacer-content-component spacer-content-component-9eeca0ae-7581-4b2f-977a-de1d94443a34 "></span>

                

        </div>
    </div>


            

    </div>
</div>


<div class="responsive">
    <div class="row collapse responsive-two-column-layout with-padding">
        <div class="small-12 medium-12 large-8 column">
            
        </div>
        <div class="small-12 medium-12 large-4 column">
            
        </div>
    </div>
</div>


<div class="responsive-legal-detail">
    <div class="responsive-legal-detail-body">
        <div id="sophos-privacy-policy" class="responsive">
<div class="row clear">
<div class="column margin-bottom-2">
<h1 id="General">Sophos Group Privacy Policy</h1>
<h2>General</h2>
<p>This is the privacy policy of Sophos Limited and its subsidiaries. </p>
<p>This document was last updated on 6 February 2018.</p>
<p>We are committed to safeguarding the privacy of your personal data. Please read the following privacy policy to understand how we collect and use your personal data, for example when you contact us, visit or use one of our websites, mobile applications, portals, or other parts of our network (each a &ldquo;Site&rdquo;), apply for a job, or use our products and services, regardless of how you access them. This privacy policy also explains the rights available to you in respect of your personal data.</p>
</div>
<div id="tabs" class="padding-top-2">
<div class="column large-3 medium-4">
<ul class="vertical tabs padding-0 marging-0 no-border" data-tabs id="productPrivacy-tabs">
    <li class="tabs-title is-active"><a class="active" aria-selected="true" href="#tab1">What personal data do we collect, how do we collect it, and why?</a> </li>
    <li class="tabs-title"><a href="#tab2">Other specific ways we collect and use your personal data</a> </li>
    <li class="tabs-title"><a href="#tab3">Legal basis for processing personal data</a> </li>
    <li class="tabs-title"><a href="#tab4">With whom might we share your personal data</a> </li>
    <li class="tabs-title"><a href="#tab5">International transfers of data</a> </li>
    <li class="tabs-title"><a href="#tab6">Data retention</a> </li>
    <li class="tabs-title"><a href="#tab7">Use by children</a> </li>
    <li class="tabs-title"><a href="#tab8">Automated decision-making</a> </li>
    <li class="tabs-title"><a href="#tab9">Your data protection rights</a> </li>
    <li class="tabs-title"><a href="#tab10">Links</a> </li>
    <li class="tabs-title"><a href="#tab11">Security</a> </li>
    <li class="tabs-title"><a href="#tab12">California privacy rights</a> </li>
    <li class="tabs-title"><a href="#tab13">Contact</a> </li>
    <li class="tabs-title"><a href="#tab14">Notification of changes</a> </li>
</ul>
</div>
<div class="column large-9 medium-8 tabs-content" data-tabs-content="productPrivacy-tabs">
<div id="tab1" class="tabs-panel padding-top-0 padding-left-2 is-active">
<h2 class="margin-top-0">What personal data do we collect, how do we collect it, and why?</h2>
<strong>Data that you provide voluntarily to us</strong><br />
<p>When you use our Site, products or services, or you otherwise communicate with us, we may ask you to provide certain personal data voluntarily, including but not limited to your name, company position, postal address, telephone number, mobile number, fax number, email address, credit card or other payment details, age or date of birth, account usernames, passwords, or gender.  For example, we may ask you to provide some or all of this personal data when you register an account with us, subscribe to our marketing communications, purchase products or services, and/or submit enquiries to us.  We use this information to create and administer your account, send you marketing communications, provide you with the products and services you request, and to respond to your enquiries. In general, the personal data that you are asked to provide, and the reasons why you are asked to provide it, will be made clear to you at the point we ask you to provide your personal data.</p>
<strong>Data collected automatically</strong><br />
<p>When you use our Site, products, or services, we may collect certain data automatically from your computers or devices (including mobile devices).  The data we collect automatically may include your IP address (explained further below), device type, operating system details, unique device identification numbers (including mobile advertising identifiers), browser-type, browser language, operating system, geographic location (as explained further under the heading &ldquo;<a href="#Locationinformation">Location information</a>&rdquo;) and other technical information.  We may also collect data about how your device has interacted with our Site, products or services, including the pages or features accessed and links clicked, the amount of time spent on particular pages, mouse hovers, the date and time of the interaction, error logs, referring and exit pages and URLs, and similar information.  Collecting this data enables us to better understand the visitors who use our Site, products, and services, including where they come from and what features are of interest to them.  We use this information for our internal analytics purposes, and to improve the quality, relevance, and security of our Site, products and services.</p>
<p>For example, every time you connect to the Site, we store a log of your visit that shows the unique number your machine uses when it is connected to the Internet - its IP address. This log tells us what your machine has looked at, whether the page request was successful or not, and which browser your machine used to view the pages. This data is used for statistical purposes as well as to help customize the user experience as you browse the Site and subsequently interact with Sophos. This helps us to understand which areas of the Site are of particular interest, which pages are not being requested, and how many people are visiting the Site in total. It also helps us to determine which products and services may be of specific interest to you. We may also use this information to block IP addresses where there is a breach of the terms and conditions for use of the Site.</p>
<p>Some of the data may be collected automatically using tracking technologies, as explained further under the heading &ldquo;<a href="#CookiesAndSimilarTechnology">Cookies and similar tracking technology</a>&rdquo;.</p>
<strong>Data that we obtain from third party sources</strong><br />
<p>From time to time, we may receive personal data about you from third party sources (including without limitation recruitment agencies, credit check agencies, agencies providing compliance checks, lead generation providers, resellers, and other partners who sell our products and services to you), but only where such third parties have confirmed that they have your consent or are otherwise legally permitted or required to disclose your personal data to us. </p>
<p>The types of information we collect from third parties include contact details, CVs, credit history, and order information, and we use the information we receive from these third parties to assess your suitability for a job position, carry out compliance checks required under applicable law (such as anti-bribery and corruption checks), make credit decisions, maintain and improve the accuracy of the records we hold about you, and market our products and services to you.</p>
<p>We also receive information from other members of the industry that forms part of or otherwise helps us to develop, test, and enhance our own product offering (for example spam lists, malicious URL lists, and sample viruses), some of which could contain personal data (where permitted by applicable law).</p>
<p>We may combine information that we collect from you with information about you that we obtain from such third parties.</p>
<strong>Data collected through our products and services</strong><br />
<p>We use data that we collect from products and services for the purposes for which you provided it, usage and audience counts, monitoring the performance and effectiveness of the products/services, monitoring compliance with our terms and conditions, enabling compatibility with third party operating systems/products/services, planning future roadmap strategy, planning product/service/feature lifecycles and retirements, conducting spam, threat and other scientific research, developing new products and services, enhancing existing products and services, troubleshooting product issues, generating statistics, reporting, and trend analysis. This may include incidental personal data (for example usernames, machine IDs, domain names, IP addresses, file names, and file paths).</p>
<p>Our products and services may collect further additional personal data about you beyond the data described in this privacy policy, or use your personal data in ways that are different to or in addition to those described in this privacy policy.  We provide a <a href="https://www.sophos.com/en-us/legal/product-privacy-info.aspx">Product Privacy Information page</a> that explains how our products and services collect and use personal data.  Please review the relevant section of the Product Privacy Information page for the product or service you are using to ensure that you are fully informed.</p>
<div id="CookiesAndSimilarTechnology">
<strong>Cookies and similar tracking technology</strong><br />
<p>A cookie is a piece of text that gets entered into the memory of your browser by a website, allowing the website to store information on your machine and later retrieve it. </p>
<p>Our Site, products, and services may use cookies, unique device identifiers (like Apple ID For Advertisers on iOS devices, and Android Advertising ID on Android devices), and other tracking technologies (collectively, "<strong>Cookies</strong>") to distinguish you from other users and better serve you when you return to the Site, product, or service, and to monitor and analyse the usage of the Site, product, or service. Cookies also enable us and our third party partners to track and target the interests of our users to enhance the onsite or in-product experience through content, features, and advertisements. </p>
<p>We, along with our service providers, may also use other Internet technologies, such as Flash technologies, Web beacons or pixel tags, and other similar technologies, to deliver or communicate with cookies and track your use of the Site, product, or service, as well as serve you ads and personalize/customize your experience when you are using our Site, product, or service and/or when you are on other websites where those cookies can be accessed. We may also include Web beacons in email messages, newsletters, and other electronic communications to determine whether the message has been opened and for other analytics, personalization, and advertising. As we adopt additional technologies, we may also gather additional information through other methods. This practice is explained further under the heading &ldquo;<a href="#MarketingAndPromotions">Marketing and promotions</a>&rdquo;. </p>
<p>As explained above, we occasionally share information you have provided to us with service providers, who will de-identify the information and associate it with cookies that enable us to reach you. We may also help these service providers place their own cookies, by deploying a cookie that is associated with a 'hashed' value associated with interest-based or demographic data, to permit advertising to be directed to you on other websites, applications or services.</p>
<p>Most browsers automatically accept cookies, but you can modify your browser setting to decline cookies by visiting the Help portion of your browser's toolbar. If you choose to decline cookies, please note that your ability to sign in, customize, or use some of the interactive features of our Site, product, or service may be impeded, and the advertisements you see may not be as relevant to you. </p>
<p>For more information about the cookies that we use, please refer to our <a href="https://www.sophos.com/en-us/legal/cookie-information.aspx">Cookie Information</a> page.</p>
</div>
<div id="Locationinformation"><strong>Location information</strong><br />
<p>We may collect different types of information about your location, including general information (for example IP address or ZIP code) and more specific information (for example GPS-based functionality on mobile devices when used to access a Site, product, or service). This information may be used to customize the services provided to you, such as location-based information, advertising, and features. In order to do this, your location information may be shared with our agents, vendors, or advertisers. If you access the Services through a mobile device and you do not want your device to provide us with location-tracking information, you can disable the GPS or other location-tracking functions on your device, provided your device allows you to do this. See your device manufacturer's instructions for further details.</p>
</div>
</div>
<div id="tab2" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Other specific ways we collect and use your personal data</h2>
<strong>Job applications</strong><br />
<p>If you are making a job application or inquiry, you may provide us with a copy of your CV or other relevant information. We may use this information for the purpose of assessing your application or inquiry. We may also keep this information on file to contact you about future opportunities, unless you ask us not to do this.</p>
<strong>Partner portal </strong><br />
<p>Our resellers, distributors, and other partners may visit our partner portal Site. We may use the information provided on that Site for partner relationship management, billing, forecasting, trend analysis, renewal management, marketing, and in order to sell and provide the products and services.</p>
<strong>Account management</strong><br />
<p>If you obtain products or services from us, we may use your contact details and (where applicable) payment information for the purposes of (i) providing training, customer support, and account management, (ii) order processing and billing, (iii) verifying your usage of the products and services in accordance with the terms and conditions of your agreement with us, (iv) carrying out checks for export control, anti-bribery, anti-corruption, the prevention of modern slavery, and other compliance purposes in accordance with requirements under applicable law; (v) contacting you (including by email communication) regarding license expiry, renewal, and other related notices, and (vi) maintaining our company accounts and records. </p>
<strong>Market research and surveys</strong><br />
<p>If you participate in surveys, we may use your personal data for our internal business analysis and training purposes in order to improve our understanding of our users&rsquo; demographics, interests and behaviour, to measure and increase customer satisfaction, and to improve our products and services.</p>
<strong>Competitions, contests, promotions</strong><br />
<p>If you participate in a competition, contest, or promotion conducted by us or on our behalf, we may use your personal data in order to administer such competition, contest, or promotion. We may also use your personal data as explained further under the heading &ldquo;<a href="#MarketingAndPromotions">Marketing and promotions</a>&rdquo;. </p>
<strong>Chat rooms</strong><br />
<p>Please be careful and responsible whenever you are online. Should you choose to voluntarily disclose information to open areas of our Site, such as via the Sophos Community, online help, or other chat rooms, that information can be viewed publicly and may be collected and used by third parties without our knowledge or consent, and may result in unsolicited messages from other individuals or third parties.</p>
<div id="MarketingAndPromotions"><strong>Marketing and promotions</strong><br />
<p>We (or our resellers or other selected third parties acting on our behalf) may contact you from time to time in order to provide you with information about products and services that may be of interest to you. Such communications may contain tracking technology that tells us whether you opened the communication and whether you followed the hyperlinks within the communication, in order to help us analyse the effectiveness of, monitor, and improve our marketing campaigns. All marketing communications that we send to you will respect any marketing preferences you have expressed to us and any consent obligations required under applicable privacy and anti-spam rules. You have the right to ask us not to process your personal data for certain or all marketing purposes, but if you do so, we may need to share your contact information with third parties for the limited purpose of ensuring that you do not receive marketing communications from them on our behalf.</p>
</div>
<strong>Network monitoring</strong><br />
<p>We may collect logs and other data about access to and traffic passing through our network and equipment for the purposes of availability and performance monitoring, maintenance, security monitoring and investigations, conducting spam, threat and other scientific research, new product and service development, the enhancement of existing products and services, generating statistics, reporting, and trend analysis.</p>
<strong>Sample submissions</strong><br />
<p>We collect information about suspected spam, suspected malicious files, and files that may be unwanted or undesirable for our customers (for example file names, URLs, file paths, hashes, and file samples) that are (i) received by our own network and equipment, and (ii) voluntarily submitted via our products and services or our Site submission pages. We use this information for spam, threat and other scientific research, new product and service development, the enhancement of existing products and services, generating statistics, reporting, and trend analysis. </p>
</div>
<div id="tab3" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Legal basis for processing personal data </h2>
<p>Our legal basis for collecting and using personal data will depend on the personal data concerned and the specific context in which we collect it. </p>
<p>However, we will normally collect personal data from you only where we need the personal data to perform a contract with you, or where the processing is in our legitimate interests and not overridden by your data protection interests or fundamental rights and freedoms, or where we have your consent to do so.  In some cases, we may also have a legal obligation to collect personal data from you or may otherwise need the personal data to protect your vital interests or those of another person.</p>
<p>If we collect and use your personal data in reliance on our legitimate interests (or those of any third party) other than as described in this privacy policy, we will make clear to you at the relevant time what those legitimate interests are.</p>
<p>If you have questions about or need further information concerning the legal basis on which we collect and use your personal data, please contact us using the contact details provided under the &ldquo;<a href="#Contact">Contact</a>&rdquo; heading.</p>
</div>
<div id="tab4" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">With whom might we share your personal data</h2>
<p>We may transfer or disclose your personal data to the following categories of recipients:</p>
<ul class="bulletList">
    <li>to <a href="https://www.sophos.com/en-us/company/contact.aspx">our group companies</a>, third party services providers, suppliers, agents, and other organisations who provide data processing services to us (for example, to support the delivery of, provide functionality on, or help to enhance the security of our Site, products, or services), or who otherwise process personal data on our behalf for purposes that are described in this privacy policy or notified to you when we collect your personal data (such as advertising, sweepstakes, analytics, research, customer support, fraud prevention, and legal services);; </li>
    <li>to our authorised resellers, distributors, and other channel partners in order to process your order or sales enquiry, manage your subscription, provide technical or customer support, advise of upcoming product or service subscription expiry and renewal dates, or as otherwise notified to you when we  collect your personal data;</li>
    <li>a subset of our threat intelligence data to selected reputable members of the IT security industry for the purpose of anti-spam and security threat research;</li>
    <li>to any government department, agency, court or other official bodies where we believe disclosure is necessary (i) as a matter of applicable law or regulation (such as in response to a subpoena, warrant, court order, or other legal process), (ii) to exercise, establish, participate in, or defend our legal rights, or limit the damages we sustain in litigation or other legal dispute, or (iii) to protect your vital interests, privacy, or safety, or those of our customers or any other person;</li>
    <li>to a potential or actual buyer or transferee (and its agents and advisers) in connection with any proposed or actual transfer of control, purchase, merger, reorganisation, consolidation, or acquisition of any part of our business, provided that we inform the buyer or transferee it must use your personal data only for the purposes disclosed in this privacy policy;</li>
    <li>to any other person with your consent to the disclosure.</li>
</ul>
<p>Except as set out above, we will not disclose your personal data save where we need to do so in order to enforce this privacy policy, our End User License Agreement, our rights generally, or where required or permitted by applicable local or foreign law.</p>
<p>Whenever we share personal data, we take all reasonable steps to ensure that it is treated securely and in accordance with this privacy policy. This may include without limitation aggregating or de-identifying information so that it is not intended to be used by the third party to identify you.</p>
</div>
<div id="tab5" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">International transfers of data</h2>
<p>As a global company, we and our service providers operate, and our Site, products, and services are accessed from, all over the world. When you give us personal data, that data may be used, processed, or stored anywhere in the world, including in countries that have data protection laws that are different to the country in which you reside. </p>
<p>However, we have taken appropriate safeguards to require that your personal data will remain protected in accordance with this privacy policy. For example, these include implementing the European Commission&rsquo;s Standard Contractual Clauses for transfers of personal data between our group companies, which require all group companies to protect personal data they process from the European Economic Area in accordance with European Union data protection law. </p>
<p>Our Standard Contractual Clauses can be provided on request.  We have implemented similar appropriate safeguards with our third party service providers and partners, and further details can be provided upon request by contacting us using the contact details provided under the &ldquo;<a href="#Contact">Contact</a>&rdquo; heading.</p>
</div>
<div id="tab6" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Data retention</h2>
<p>We retain personal data we collect from you for as long as necessary for the purposes for which the personal data was collected or where we have an ongoing legitimate business need to do so (for example, to provide you with a product or service you have requested, to ensure that transactions can be processed, settled, refunded, charged back, or to identify fraud), or to comply with applicable legal, tax, or regulatory requirements. Even if you close your account, we will retain certain information in order to meet our obligations. </p>
<p>When we have no ongoing legitimate business need to process your personal data, we will either securely destroy, erase, delete or anonymise it, or if this is not possible (for example, because your personal data has been stored in backup archives), then we will securely store your personal data and isolate it from any further processing until deletion is possible. </p>
</div>
<div id="tab7" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Use by children</h2>
<p>The Site, the products, and the services are not intended for persons under the age of 16. By using the Site, product, or service, you hereby represent that you are at least 16 years old. </p>
</div>
<div id="tab8" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Automated decision-making</h2>
<p>In some instances, our use of your personal data may result in automated decisions being taken that legally affect you or similarly significantly affect you. </p>
<p>Automated decisions mean that a decision concerning you is made automatically on the basis of a computer determination (using software algorithms), without our human review.  For example, our products and services use automated decisions to determine whether a domain, URL, or IP address is sending spam or malicious content in order to protect our customers from unwanted or undesirable content.  We have implemented measures to safeguard the rights and interests of individuals whose personal data is subject to automated decision-making, including controlled product releases and regular quality assessments.</p>
<p>When we make an automated decision about you (for example if we block a domain, URL, or IP address used by you), you have the right to contest the decision, to express your point of view, and to require a human review of the decision.  You can exercise this right by contact us using the contact details provided under the &ldquo;<a href="#Contact">Contact</a>&rdquo; heading.</p>
</div>
<div id="tab9" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Your data protection rights</h2>
<p>You have the following data protection rights:</p>
<ul class="bulletList">
    <li>You can <strong>access, delete or request portability</strong> of your personal data by completing <a href="https://app-de.onetrust.com/app/#/webform/5ae85dff-ba17-4244-8f64-15a850fc1130" target="_blank">this form</a>.</li>
    <li>You may also ask us to <strong>correct or update</strong> your personal data, <strong>object to processing</strong> of your personal data, or ask us to <strong>restrict processing</strong> of your personal data using the contact details provided under the &ldquo;<a href="#Contact">Contact</a>&rdquo; heading</li>
    <li>You have the right to <strong>opt-out of marketing communications</strong> we send you at any time.  You can usually exercise this right by clicking on the &ldquo;unsubscribe&rdquo; or &ldquo;opt-out&rdquo; link in the marketing e-mails we send you.  Alternatively, or to opt-out of other forms of marketing (such as postal marketing or telemarketing), then please contact us using the contact details provided under the &ldquo;<a href="#Contact">Contact</a>&rdquo; heading.</li>
    <li>If we have collected and process your personal data with your consent, then you can <strong>withdraw your consent</strong> at any time by contacting us using the contact details provided under the &ldquo;<a href="#Contact">Contact</a>&rdquo; heading.  Withdrawing your consent will not affect the lawfulness of any processing we conducted prior to your withdrawal, nor will it affect processing of your personal data conducted in reliance on lawful processing grounds other than consent. </li>
    <li>You have the <strong>right to complain</strong> to a data protection authority about our collection and use of your personal data.  For more information, please contact your local data protection authority. </li>
</ul>
<p>We respond to all requests we receive from individuals wishing to exercise their data protection rights in accordance with applicable data protection laws.</p>
</div>
<div id="tab10" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Links</h2>
<p>This privacy policy applies to personal data collected by us. If we provide a link to a third party site (whether through our Site, a product or service, or in an e-mail we send you), please be aware that we are not responsible for the content or privacy practices of such third party site. We encourage our users to be aware when they leave our Site, and to read the privacy policy of other sites that collect personal data. We are not liable for any disputes, loss, or damage that may arise from or in connection with your use of such third party sites.</p>
</div>
<div id="tab11" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Security</h2>
<p>While we strive to protect your personal data, no data transmission or storage can be guaranteed as 100% secure. We endeavour to protect all personal data using reasonable and appropriate physical, administrative, technical, and organisational measures, and in accordance with our internal security procedures and applicable law. These safeguards vary based on the sensitivity of the information that we collect, process, and store, and the current state of technology.</p>
<p>If you have been given or have created log-in details to provide you with access to certain parts of our Site (for example our partner portal), you are responsible for keeping those details confidential in order to prevent unauthorised access to your accounts. </p>
</div>
<div id="tab12" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">California privacy rights</h2>
<p>California Online Privacy Protection Act Notice Concerning Do Not Track Signals</p>
<p>Do Not Track (&ldquo;DNT&rdquo;) is a privacy preference that users can set in certain web browsers. DNT is a way for users to inform websites and services that they do not want certain information about their webpage visits collected over time and across websites or online services. We do not recognize or respond to browser-initiated DNT signals, as the Internet industry is currently still working toward defining exactly what DNT means, what it means to comply with DNT, and a common approach to responding to DNT. To learn more about Do Not Track, you can do so <a href="http://allaboutdnt.com/" target="_blank">here</a>.</p>
<p>Your California privacy rights</p>
<p>California law permits users who are California residents to request and obtain from us once a year, free of charge, a list of the third parties to whom we have disclosed their 'personal information' (if any, and as defined under applicable California law) for their direct marketing purposes in the prior calendar year, as well as the type of personal information disclosed to those parties. If you are a California resident and would like to request this information, please submit your request using the contact details provided under the &ldquo;<a href="#Contact">Contact</a>&rdquo; heading.</p>
</div>
<div id="tab13" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Contact</h2>
<p>This is the website of Sophos Limited, a company registered in England and Wales under company number 2096520, whose registered office is at The Pentagon, Abingdon Science Park, Abingdon, Oxon, OX14 3YP, United Kingdom, and whose VAT registration number is 991 2418 08.  Sophos Limited is the controller of personal data collected under this privacy policy (unless we indicate otherwise). </p>
<p>We have appointed individuals who are responsible for the protection and security of your personal data. If you have any questions, comments, concerns, grievances, or complaints about this privacy policy or the manner in which we treat your personal data, or if you want to request any information about your personal data or believe that we are holding incorrect personal data on you, please contact our Data Protection and Privacy team at <a href="mailto:dataprotection@sophos.com">dataprotection@sophos.com</a>. </p>
<p>If you wish to unsubscribe from marketing communications, please email <a href="mailto:unsubscribe@sophos.com">unsubscribe@sophos.com</a>. </p>
</div>
<div id="tab14" class="tabs-panel padding-top-0 padding-left-2">
<h2 class="margin-top-0">Notification of changes</h2>
<p>We reserve the right to amend or vary this policy from time to time to reflect evolving legal, regulatory, or business practices.  When we update our privacy policy, we will take appropriate measures to inform you, consistent with the significance of the changes we make (which, for minor changes, may include posting the revised privacy policy to our Site with immediate effect).  Please check this page periodically for changes. We will obtain your consent to any material privacy policy changes if and where this is required by applicable data protection laws.</p>
<p>You can see when this privacy policy was last updated by checking the &ldquo;last updated&rdquo; date displayed at the top of this privacy policy under the &ldquo;<a href="#General">General</a>&rdquo; heading. </p>
</div>
</div>
</div>
</div>
</div>
    </div>
</div>
    <!-- START Main Content Area -->
    
    <!-- END Main Content Area -->
    

<div class="responsive-footer-wrapper responsive footer-themed footer-dark">
    

<div class="footer-cta no-background-image" style="background-image: url()">
    <div class="row">
        <div class="column small-12 large-9">
            <p class="footer-cta-text">
                Start a Sophos demo in less than a minute. See exactly how our solutions work in a full environment without a commitment. 
            </p>
        </div>
        <div class="column small-12 large-3 footer-cta-button">
            <a class="footer-cta-link" href="https://secure2.sophos.com/en-us/products/demos.aspx">
                    Learn More
                </a>
        </div>
    </div>
    <hr/>
</div>


<div class="footer-social-section-links">
    <div class="row">
        

<div class="footer-social-links column medium-4 large-3">
    <h3>Stay Connected</h3>
    
            <ul>
        
            <li>
                <a rel="external" href="https://www.facebook.com/securitybysophos">
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Footer/facebook-icon.svg?la=en&amp;hash=1CA0A5A0E9C9999E7E079A5D32C86E9BF9C7AD1E" alt="" />
                    </a>
            </li>
        
            <li>
                <a rel="external" href="https://plus.google.com/+sophos">
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Footer/googleplus-icon.svg?la=en&amp;hash=5A36B90788D12316616A8EA992CA92B0509EBE3B" alt="" />
                    </a>
            </li>
        
            <li>
                <a rel="external" href="https://www.instagram.com/sophossecurity/">
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Footer/instagram-icon.svg?la=en&amp;hash=2B50C2B67C3023D9E7D4E1C3A16401EAD8F10F76" alt="" />
                    </a>
            </li>
        
            <li>
                <a rel="external" href="https://www.linkedin.com/company/sophos">
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Footer/linkedin-icon.svg?la=en&amp;hash=B6D5378C1E1A16F239A4C862EAE3499B8EC1F3F9" alt="" />
                    </a>
            </li>
        
            <li>
                <a href="https://www.sophos.com/en-us/company/rss-feeds.aspx">
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Footer/rss-icon.svg?la=en&amp;hash=76C4DAFD331DC675814191A66794A778D8C6DCF7" alt="" />
                    </a>
            </li>
        
            <li>
                <a rel="external" href="https://twitter.com/Sophos">
                        <img src="/en-us/medialibrary/SophosNext/Images/Navigation/Footer/twitter-icon.svg?la=en&amp;hash=9A90D9F99ABE6454741BBAAF9C3FB6966624C92D" alt="" />
                    </a>
            </li>
        
            </ul>
        
</div>
        

<div class="footer-section-links column medium-8 large-9">
    
            <ul>
        
            <li>
                <a href="https://www.sophos.com/en-us/company/careers.aspx">
                        Careers
                    </a>
            </li>
        
            <li>
                <a href="https://www.sophos.com/en-us/partners/partner-locator.aspx">
                        Find a Partner
                    </a>
            </li>
        
            <li>
                <a href="https://secure2.sophos.com/en-us/support.aspx">
                        Support
                    </a>
            </li>
        
            <li>
                <a href="https://www.sophos.com/en-us/threat-center/technical-papers.aspx">
                        Technical Papers
                    </a>
            </li>
        
            <li>
                <a href="https://www.sophos.com/en-us/security-news-trends/whitepapers.aspx">
                        Whitepapers
                    </a>
            </li>
        
            </ul>
        
</div>
    </div>
    <hr/>
</div>
    

    <div class="responsive-legal">
        <div class="row collapse">
            <div class="responsive-legal-copyright column small-12 large-4">
                <p>
                    &copy;
                    1997
                    - 2019
                    Sophos Ltd. All rights reserved.
                </p>
            </div>

            <ul class="responsive-legal-links column small-12 large-8">
                <li class="responsive-legal-language">                
                    <a href="#globalsitesfooter">
                        English
                    </a>

                    <img id="ctl05_FooterArrowImage" src="/SophosNext/Assets/Images/footer-arrow.png" style="border-width:0px;" />

                    
                            <div class="responsive-legal-language-popup">
                                <ul>
                                    
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl0_LanguageHyperlink" href="/zh-cn/legal/sophos-group-privacy-policy.aspx">简体中文</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl1_LanguageHyperlink" href="/cs-cz/legal/sophos-group-privacy-policy.aspx">český</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl2_LanguageHyperlink" href="/en-us/legal/sophos-group-privacy-policy.aspx">English</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl3_LanguageHyperlink" href="/fr-fr/legal/sophos-group-privacy-policy.aspx">Français</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl4_LanguageHyperlink" href="/de-de/legal/sophos-group-privacy-policy.aspx">Deutsch</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl5_LanguageHyperlink" href="/hu-hu/legal/sophos-group-privacy-policy.aspx">Magyar</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl6_LanguageHyperlink" href="/it-it/legal/sophos-group-privacy-policy.aspx">Italiano </a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl7_LanguageHyperlink" href="/ja-jp/legal/sophos-group-privacy-policy.aspx">日本語</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl8_LanguageHyperlink" href="/ko-kr/legal/sophos-group-privacy-policy.aspx">한국의</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl9_LanguageHyperlink" href="/pl-pl/legal/sophos-group-privacy-policy.aspx">Polski</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl10_LanguageHyperlink" href="/pt-br/legal/sophos-group-privacy-policy.aspx">Portuguese</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl11_LanguageHyperlink" href="/es-es/legal/sophos-group-privacy-policy.aspx">Español</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl12_LanguageHyperlink" href="/zh-tw/legal/sophos-group-privacy-policy.aspx">繁體中文</a>
                            </li>
                        
                            <li>
                                <a id="ctl05_lstvGlobalLanguageFooter_ctrl13_LanguageHyperlink" href="/tr-tr/legal/sophos-group-privacy-policy.aspx">Turkish</a>
                            </li>
                        
                                </ul>
                                <span class="arrow"></span>
                            </div>
                        
                </li>

                
                        
                        <li>
                            <a href="/en-us/legal.aspx">
                                Legal
                            </a>
                        </li>
                    
                        <li>
                            <a href="/en-us/legal/sophos-group-privacy-policy.aspx">
                                Privacy
                            </a>
                        </li>
                    
                        <li>
                            <a href="/en-us/legal/cookie-information.aspx">
                                Cookie Information
                            </a>
                        </li>
                    
                    
            </ul>
        </div>
    </div>
</div>
    
<script type="text/javascript">
    var sophosCookiePolicyCompliantCountries = ["AT","BG","CZ","DK","FI","FR","GR","HU","IE","LV","LT","LU","SK","SE","GB","IN"];
</script>

<div id="cookie-policy-wrapper" class="cookie-policy-container responsive">
    <div class="cookie-policy-short row">
        <div class="cookie-policy-short-left small-12 column">
            This site uses cookies to improve site functionality, for advertising purposes, and for website analytics.  By continuing to use the site you are agreeing to our use of cookies. <a class="button-content-component blue-arrow-btn" href="https://www.sophos.com/en-us/legal/cookie-information.aspx">Learn More</a>

            <a href="#" class="cookie-policy-accept" id="cookie-accept">
                Continue
            </a>
        </div>
    </div>
</div>

    

<div id="cboxOverlay" style="opacity: 1; cursor: auto; visibility: visible; display: none;">
</div>
<div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none; visibility: visible;
    top: 378px; left: 248px; position: absolute; width: 842px; height: 670px; opacity: 1;
    cursor: auto;">
    <div id="cboxWrapper" style="height: 670px; width: 842px;">
        <div>
            <div id="cboxTopLeft" style="float: left;">
            </div>
            <div id="cboxTopCenter" style="float: left; width: 800px;">
            </div>
            <div id="cboxTopRight" style="float: left;">
            </div>
        </div>
        <div style="clear: left;">
            <div id="cboxMiddleLeft" style="float: left; height: 628px;">
            </div>
            <div id="cboxContent" style="float: left; width: 800px; height: 628px;">
                <div id="cboxTitle" style="float: left; display: block;">
                </div>
                <div id="cboxCurrent" style="float: left; display: block;">
                    1 of 5</div>
                <button type="button" id="cboxPrevious" style="display: block;">
                    previous</button>
                <button type="button" id="cboxNext" style="display: block;">
                        next</button>
                <button id="cboxSlideshow" style="display: none;">
                        </button>
                <div id="cboxLoadingOverlay" style="float: left; display: none;">
                </div>
                <div id="cboxLoadingGraphic" style="float: left; display: none;">
                </div>
                <button type="button" id="cboxClose" style="display: inline-block;">
                    close</button></div>
            <div id="cboxMiddleRight" style="float: left; height: 628px;">
            </div>
        </div>
        <div style="clear: left;">
            <div id="cboxBottomLeft" style="float: left;">
            </div>
            <div id="cboxBottomCenter" style="float: left; width: 800px;">
            </div>
            <div id="cboxBottomRight" style="float: left;">
            </div>
        </div>
    </div>
    <div style="position: absolute; width: 9999px; visibility: hidden; display: none;">
    </div>
</div>
    

<script type="text/javascript">
//<![CDATA[
externalLinks();//]]>
</script>
</form>
    
    



    <!-- Footer scripts Start -->
    
    <!-- Footer scripts End -->
</body>
</html>
