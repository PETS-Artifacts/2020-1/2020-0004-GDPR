<!DOCTYPE html >
<html lang="en" dir="ltr">
    <head id="ctl00_Head"><meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta name="viewport" content="width=device-width, initial-scale=1" /><meta name="GENERATOR" content="Microsoft SharePoint" />                
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':
        new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
    'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-52TVM8');</script>
        <!-- End Google Tag Manager -->
        <title>
	
     Data Protection

</title>
        <link id="CssRegistration2" rel="stylesheet" type="text/css" href="/_layouts/15/1033/styles/Themable/corev15.css?rev=Pt4excuqEO8l7Cxin%2F2rDQ%3D%3D"/>
<link id="CssRegistration3" rel="stylesheet" type="text/css" href="/_layouts/15/DCCAE/css/bootstrap.css?r=20170228"/>
<link id="CssRegistration4" rel="stylesheet" type="text/css" href="/_layouts/15/DCCAE/css/ie10-viewport-bug-workaround.css"/>
<link id="CssRegistration5" rel="stylesheet" type="text/css" href="/_layouts/15/DCCAE/css/dccae.css?r=20170228"/>
<link id="CssRegistration6" rel="stylesheet" type="text/css" href="/_layouts/15/DCCAE/css/dccae-overrides.css?r=20170228"/>

        
        <script type="text/javascript" src="/_layouts/15/init.js?rev=RIl4brNTuKjQszVd%2Bc1CnQ%3D%3D"></script>
<script src="/_layouts/15/dccae/js/jquery.1.12.4.min.js"></script>
<script src="/_layouts/15/dccae/js/bootstrap.min.js"></script>
<script src="/_layouts/15/dccae/js/quicklaunchaccordion.js"></script>
<script type="text/javascript" src="/ScriptResource.axd?d=9YQCN8gbv_QOkSs4pcKtdxgLAKqDaii2gNkCU5L4S59VSxjcW84eeAq7gDI71MRMxfC55lEcHJKK3PCL2IAZYA5hamfMyOjc7i9NwRYApcNft6w9W6NKvgE9HdayXpPpXPRul2T1LJbh3HsdKxc0axjECeFrlTqBrG1DpDd6jYdEOCcKLbwtS6gcHklbgSh30&amp;t=348b0da"></script>
<script type="text/javascript" src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D"></script>
<script type="text/javascript" src="/ScriptResource.axd?d=qgsZSd--_hklM-XDkf3aXbmYfSM69QzMwLW5IFRQEfEIrdlp1XxuO2hMSzvzbksk_vz3D6Wa9_1nNb6knI3wm2qlnnrxr_4WwnDwJCD56_fWGT_Vq_woEXU2ZmDytJNTr5KhaCaFsycnDCSEA730e3MpABZFnIE6BFpuUJkkzTsZY1taDC7J-xMFRHtlgE1u0&amp;t=348b0da"></script>
<script type="text/javascript">RegisterSod("initstrings.js", "\u002f_layouts\u002f15\u002f1033\u002finitstrings.js?rev=rvK\u00252BOBkjPeQAF3rjImlgDQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("strings.js", "\u002f_layouts\u002f15\u002f1033\u002fstrings.js?rev=41YNvY8G\u00252Fyqpi\u00252BXooKRREw\u00253D\u00253D");RegisterSodDep("strings.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sp.init.js", "\u002f_layouts\u002f15\u002fsp.init.js?rev=jvJC3Kl5gbORaLtf7kxULQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.res.resx", "\u002f_layouts\u002f15\u002fScriptResx.ashx?culture=en\u00252Dus\u0026name=SP\u00252ERes\u0026rev=yNk\u00252FhRzgBn40LJVP\u00252BqfgdQ\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("sp.ui.dialog.js", "\u002f_layouts\u002f15\u002fsp.ui.dialog.js?rev=3Oh2QbaaiXSb7ldu2zd6QQ\u00253D\u00253D");RegisterSodDep("sp.ui.dialog.js", "sp.init.js");RegisterSodDep("sp.ui.dialog.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("core.js", "\u002f_layouts\u002f15\u002fcore.js?rev=rjZicIy\u00252BXBea34HbdjHJhw\u00253D\u00253D");RegisterSodDep("core.js", "strings.js");</script>
<script type="text/javascript">RegisterSod("menu.js", "\u002f_layouts\u002f15\u002fmenu.js?rev=cXv35JACAh0ZCqUwKU592w\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("mQuery.js", "\u002f_layouts\u002f15\u002fmquery.js?rev=VYAJYBo5H8I3gVSL3MzD6A\u00253D\u00253D");</script>
<script type="text/javascript">RegisterSod("callout.js", "\u002f_layouts\u002f15\u002fcallout.js?rev=ryx2n4ePkYj1\u00252FALmcsXZfA\u00253D\u00253D");RegisterSodDep("callout.js", "strings.js");RegisterSodDep("callout.js", "mQuery.js");RegisterSodDep("callout.js", "core.js");</script>
<script type="text/javascript">RegisterSod("clienttemplates.js", "\u002f_layouts\u002f15\u002fclienttemplates.js?rev=wFTHeoDrS3YJ52E27prOkg\u00253D\u00253D");RegisterSodDep("clienttemplates.js", "initstrings.js");</script>
<script type="text/javascript">RegisterSod("sharing.js", "\u002f_layouts\u002f15\u002fsharing.js?rev=XxxHIxIIc8BsW9ikVc6dgA\u00253D\u00253D");RegisterSodDep("sharing.js", "strings.js");RegisterSodDep("sharing.js", "mQuery.js");RegisterSodDep("sharing.js", "clienttemplates.js");RegisterSodDep("sharing.js", "core.js");</script>
<script type="text/javascript">RegisterSod("suitelinks.js", "\u002f_layouts\u002f15\u002fsuitelinks.js?rev=REwVU5jSsadDdOZlCx4wpA\u00253D\u00253D");RegisterSodDep("suitelinks.js", "strings.js");RegisterSodDep("suitelinks.js", "core.js");</script>
<script type="text/javascript">RegisterSod("browserScript", "\u002f_layouts\u002f15\u002fnon_ie.js?rev=W2q45TO627Zi6ztdktTOtA\u00253D\u00253D");RegisterSodDep("browserScript", "strings.js");</script>
<script type="text/javascript">RegisterSod("sp.runtime.js", "\u002f_layouts\u002f15\u002fsp.runtime.js?rev=5f2WkYJoaxlIRdwUeg4WEg\u00253D\u00253D");RegisterSodDep("sp.runtime.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("sp.js", "\u002f_layouts\u002f15\u002fsp.js?rev=yHHzNc0Ngyb9ksRsrUasMQ\u00253D\u00253D");RegisterSodDep("sp.js", "sp.runtime.js");RegisterSodDep("sp.js", "sp.ui.dialog.js");RegisterSodDep("sp.js", "sp.res.resx");</script>
<script type="text/javascript">RegisterSod("inplview", "\u002f_layouts\u002f15\u002finplview.js?rev=gj\u00252BiqPqpNPU131h9hI01nA\u00253D\u00253D");RegisterSodDep("inplview", "strings.js");RegisterSodDep("inplview", "core.js");RegisterSodDep("inplview", "sp.js");</script>
<script type="text/javascript">RegisterSod("sp.core.js", "\u002f_layouts\u002f15\u002fsp.core.js?rev=tZDGLPOvY1bRw\u00252BsgzXpxTg\u00253D\u00253D");RegisterSodDep("sp.core.js", "strings.js");RegisterSodDep("sp.core.js", "sp.init.js");RegisterSodDep("sp.core.js", "core.js");</script>
<script type="text/javascript">RegisterSod("dragdrop.js", "\u002f_layouts\u002f15\u002fdragdrop.js?rev=xyQu9SPqkjO4R2\u00252BS3IwO8Q\u00253D\u00253D");RegisterSodDep("dragdrop.js", "strings.js");</script>
<script type="text/javascript">RegisterSod("quicklaunch.js", "\u002f_layouts\u002f15\u002fquicklaunch.js?rev=\u00252BHeX6ARcp\u00252F9LpMq6FqQYyA\u00253D\u00253D");RegisterSodDep("quicklaunch.js", "strings.js");RegisterSodDep("quicklaunch.js", "dragdrop.js");</script>
<link type="text/xml" rel="alternate" href="/en-ie/about-us/compliance/data-protection/_vti_bin/spsdisco.aspx" />        
        <!-- jQuery 1.12.4 (minified) -->
        
        <!-- Bootstrap JS (v3.3.7 / minified) -->
        
        <!-- Left Navigation behavioural js -->
        
        <!-- Bootstrap CSS (v3.3.7) -->
        
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        
        <!-- Custom CSS -->
                
        <!-- Google Web Font -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic" rel="stylesheet" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
           <script src="/_layouts/15/DCCAE/js/html5shiv.min.js">//<![CDATA[//]]></script>
           <script src="/_layouts/15/DCCAE/js/respond.min.js">//<![CDATA[//]]></script>
        <![endif]-->
        <link rel="SHORTCUT ICON" href="/_layouts/15/DCCAE/images/favicon.ico" type="image/x-icon" /> 
        <!-- Internet explorer CSS Override. -->
         <script type="text/javascript">
             var uAgent = window.navigator.userAgent;
             var isMsie = uAgent.indexOf("MSIE ");
             var isMsieVersion = parseInt(uAgent.substring(isMsie + 5, uAgent.indexOf(".", isMsie)));
             //HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries
             if (isMsieVersion <= 11) {
                 jQuery('head').append('<link href="/_layouts/15/DCCAE/css/dccae-IE.css?r=20161027" rel="stylesheet" type="text/css" />');
             }
             jQuery('#apptile-appadd').css("display", "none");

        </script>     
        
            	
<style type="text/css">
    	    .ms-long {
    	        width: 100% !important;
    	    }
    	</style>  

            <link rel="canonical" href="https://www.dccae.gov.ie:443/en-ie/about-us/compliance/data-protection/Pages/default.aspx" />           
        </head>
    <body onhashchange="if (typeof(_spBodyOnHashChange) != 'undefined') _spBodyOnHashChange();">
        
        
        
         <!-- Google Tag Manager -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-52TVM8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

        <!-- End Google Tag Manager -->
        <form method="post" action="default.aspx" onsubmit="javascript:return WebForm_OnSubmit();" id="aspnetForm">
<div class="aspNetHidden">
<input type="hidden" name="_wpcmWpid" id="_wpcmWpid" value="" />
<input type="hidden" name="wpcmVal" id="wpcmVal" value="" />
<input type="hidden" name="MSOWebPartPage_PostbackSource" id="MSOWebPartPage_PostbackSource" value="" />
<input type="hidden" name="MSOTlPn_SelectedWpId" id="MSOTlPn_SelectedWpId" value="" />
<input type="hidden" name="MSOTlPn_View" id="MSOTlPn_View" value="0" />
<input type="hidden" name="MSOTlPn_ShowSettings" id="MSOTlPn_ShowSettings" value="False" />
<input type="hidden" name="MSOGallery_SelectedLibrary" id="MSOGallery_SelectedLibrary" value="" />
<input type="hidden" name="MSOGallery_FilterString" id="MSOGallery_FilterString" value="" />
<input type="hidden" name="MSOTlPn_Button" id="MSOTlPn_Button" value="none" />
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
<input type="hidden" name="__REQUESTDIGEST" id="__REQUESTDIGEST" value="0xE5888B4720505477E5E20B1D076CAD1046D33E6335376C9ED487609180377C002623B38B1D248F5DB644358362FE8307798183ADD985B006B7894B73DA46622A,30 May 2018 16:34:33 -0000" />
<input type="hidden" name="MSOSPWebPartManager_DisplayModeName" id="MSOSPWebPartManager_DisplayModeName" value="Browse" />
<input type="hidden" name="MSOSPWebPartManager_ExitingDesignMode" id="MSOSPWebPartManager_ExitingDesignMode" value="false" />
<input type="hidden" name="MSOWebPartPage_Shared" id="MSOWebPartPage_Shared" value="" />
<input type="hidden" name="MSOLayout_LayoutChanges" id="MSOLayout_LayoutChanges" value="" />
<input type="hidden" name="MSOLayout_InDesignMode" id="MSOLayout_InDesignMode" value="" />
<input type="hidden" name="_wpSelected" id="_wpSelected" value="" />
<input type="hidden" name="_wzSelected" id="_wzSelected" value="" />
<input type="hidden" name="MSOSPWebPartManager_OldDisplayModeName" id="MSOSPWebPartManager_OldDisplayModeName" value="Browse" />
<input type="hidden" name="MSOSPWebPartManager_StartWebPartEditingName" id="MSOSPWebPartManager_StartWebPartEditingName" value="false" />
<input type="hidden" name="MSOSPWebPartManager_EndWebPartEditing" id="MSOSPWebPartManager_EndWebPartEditing" value="false" />
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUBMA9kFgJmD2QWAgIBD2QWBAIBD2QWAgIGD2QWAmYPZBYCAgEPFgIeE1ByZXZpb3VzQ29udHJvbE1vZGULKYgBTWljcm9zb2Z0LlNoYXJlUG9pbnQuV2ViQ29udHJvbHMuU1BDb250cm9sTW9kZSwgTWljcm9zb2Z0LlNoYXJlUG9pbnQsIFZlcnNpb249MTUuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49NzFlOWJjZTExMWU5NDI5YwFkAgkPZBYOAgMPZBYCAgIPZBYCAgUPZBYCAgMPFgIeB1Zpc2libGVoFgJmD2QWBAICD2QWBgIBDxYCHwFoZAIDDxYCHwFoZAIFDxYCHwFoZAIDDw8WAh4JQWNjZXNzS2V5BQEvZGQCBQ9kFgJmDxYCHgRUZXh0BZsDPHVsIGNsYXNzPSduYXYgbmF2YmFyLW5hdiBuYXZiYXItbWFpbiBub2luZGV4Jz48bGk+PGEgaHJlZj0nL2VuLWllL2NvbW11bmljYXRpb25zJyBjbGFzcz0nbm9pbmRleCc+Q29tbXVuaWNhdGlvbnM8L2E+PC9saT48bGk+PGEgaHJlZj0nL2VuLWllL2NsaW1hdGUtYWN0aW9uJyBjbGFzcz0nbm9pbmRleCc+Q2xpbWF0ZSBBY3Rpb248L2E+PC9saT48bGk+PGEgaHJlZj0nL2VuLWllL2Vudmlyb25tZW50JyBjbGFzcz0nbm9pbmRleCc+RW52aXJvbm1lbnQ8L2E+PC9saT48bGk+PGEgaHJlZj0nL2VuLWllL2VuZXJneScgY2xhc3M9J25vaW5kZXgnPkVuZXJneTwvYT48L2xpPjxsaT48YSBocmVmPScvZW4taWUvbmF0dXJhbC1yZXNvdXJjZXMnIGNsYXNzPSdub2luZGV4Jz5OYXR1cmFsIFJlc291cmNlczwvYT48L2xpPjwvdWw+ZAIHD2QWBAIBDxYCHwMFmwI8bGk+PGEgaHJlZj0iL2VuLWllL25ld3MtYW5kLW1lZGlhLyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPk5ld3MgJiBNZWRpYTwvYT48L2xpPjxsaSBjbGFzcz0nYWN0aXZlIG5vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy8iIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5BYm91dCBVczwvYT48L2xpPjxsaT48YSBocmVmPSIvZW4taWUvYWJvdXQtdXMvY29udGFjdC11cy8iIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db250YWN0IFVzPC9hPjwvbGk+ZAIFDxYCHwMFXDxsaT48YSBocmVmPSdodHRwczovL3R3aXR0ZXIuY29tL0RlcHRfQ0NBRScgY2xhc3M9J3R3aXR0ZXInIHRhcmdldD0nX2JsYW5rJz5Ud2l0dGVyPC9hPjwvbGk+ZAILD2QWAmYPFgIfAwWTAjxvbCBjbGFzcz0nYnJlYWRjcnVtYiBub2luZGV4Jz48bGk+PGEgaHJlZj0nL2VuLWllJyBjbGFzcz0nbm9pbmRleCcgPkhvbWU8L2E+PC9saT48bGk+PGEgaHJlZj0nL2VuLWllL2Fib3V0LXVzJyBjbGFzcz0nbm9pbmRleCcgPkFib3V0IFVzPC9hPjwvbGk+PGxpPjxhIGhyZWY9Jy9lbi1pZS9hYm91dC11cy9jb21wbGlhbmNlJyBjbGFzcz0nbm9pbmRleCcgPkNvbXBsaWFuY2U8L2E+PC9saT48bGkgY2xhc3M9J2FjdGl2ZSBub2luZGV4Jz5EYXRhIFByb3RlY3Rpb248L2xpPjwvb2w+ZAIND2QWAgICD2QWcgICD2QWAgIBDxYCHwALKwQBZAIGD2QWAgIBDxYCHwALKwQBZAIIDxYCHwALKwQBZAIKDxYCHwALKwQBZAIODxYCHwALKwQBZAIQDxYCHwALKwQBZAISDxYCHwALKwQBZAIWDxYCHwALKwQBZAIYDxYCHwALKwQBZAIaDxYCHwALKwQBZAIeDxYCHwALKwQBZAIgDxYCHwALKwQBZAIiDxYCHwALKwQBZAImDxYCHwALKwQBZAIoDxYCHwALKwQBZAIqDxYCHwALKwQBZAIuDxYCHwALKwQBZAIwDxYCHwALKwQBZAIyDxYCHwALKwQBZAI2DxYCHwALKwQBZAI4DxYCHwALKwQBZAI6DxYCHwALKwQBZAI+DxYCHwALKwQBZAJADxYCHwALKwQBZAJCDxYCHwALKwQBZAJGDxYCHwALKwQBZAJIDxYCHwALKwQBZAJKDxYCHwALKwQBZAJODxYCHwALKwQBZAJQDxYCHwALKwQBZAJSDxYCHwALKwQBZAJWDxYCHwALKwQBZAJYDxYCHwALKwQBZAJaDxYCHwALKwQBZAJeDxYCHwALKwQBZAJgDxYCHwALKwQBZAJiDxYCHwALKwQBZAJmDxYCHwALKwQBZAJoDxYCHwALKwQBZAJqDxYCHwALKwQBZAJuDxYCHwALKwQBZAJwDxYCHwALKwQBZAJyDxYCHwALKwQBZAJ2DxYCHwALKwQBZAJ4DxYCHwALKwQBZAJ6DxYCHwALKwQBZAJ+DxYCHwALKwQBZAKAAQ8WAh8ACysEAWQCggEPFgIfAAsrBAFkAoYBDxYCHwALKwQBZAKIAQ8WAh8ACysEAWQCigEPFgIfAAsrBAFkAo4BDxYCHwALKwQBZAKQAQ8WAh8ACysEAWQCkgEPFgIfAAsrBAFkApYBDxYCHwALKwQBZAKYAQ8WAh8ACysEAWQCDw9kFgICAQ9kFgICAQ9kFgICDQ8PFgIfAWhkFgICAw9kFgICAw9kFgICAQ88KwAJAQAPFgIeDU5ldmVyRXhwYW5kZWRnZGQCEQ9kFgZmDxYCHwMF/zo8dWwgY2xhc3M9J2NsZWFyZml4IG5vaW5kZXgnPjxsaSBjbGFzcz0nbm9pbmRleCc+PGgzIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvY29tbXVuaWNhdGlvbnMiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db21tdW5pY2F0aW9uczwvYT48L2gzPjx1bCBjbGFzcz0nbm9pbmRleCc+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvY29tbXVuaWNhdGlvbnMvdG9waWNzIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+VG9waWNzPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvY29tbXVuaWNhdGlvbnMvcHVibGljYXRpb25zLyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPlB1YmxpY2F0aW9uczwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2NvbW11bmljYXRpb25zL3Byb2dyYW1tZXMtYW5kLXNjaGVtZXMiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Qcm9ncmFtbWVzICYgU2NoZW1lczwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2NvbW11bmljYXRpb25zL2NvbnN1bHRhdGlvbnMiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db25zdWx0YXRpb25zPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvY29tbXVuaWNhdGlvbnMvbGVnaXNsYXRpb24iIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5MZWdpc2xhdGlvbjwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25ld3MtYW5kLW1lZGlhL3ByZXNzLXJlbGVhc2VzL1BhZ2VzL2RlZmF1bHQuYXNweCNEZWZhdWx0PSU3QiUyMmslMjIlM0ElMjIlMjIlMkMlMjJyJTIyJTNBJTVCJTdCJTIybiUyMiUzQSUyMkRDQ0FFU2VjdGlvbnNNTVNNdWx0aVRhZyUyMiUyQyUyMnQlMjIlM0ElNUIlMjIlNUMlMjIlQzclODIlQzclODI0MzZmNmQ2ZDc1NmU2OTYzNjE3NDY5NmY2ZTczJTVDJTIyJTIyJTVEJTJDJTIybyUyMiUzQSUyMmFuZCUyMiUyQyUyMmslMjIlM0FmYWxzZSUyQyUyMm0lMjIlM0FudWxsJTdEJTVEJTdEIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+UHJlc3MgUmVsZWFzZXM8L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb250YWN0LXVzL1BhZ2VzL0NvbW11bmljYXRpb25zLmFzcHgiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db250YWN0IFVzPC9hPjwvbGk+PC91bD48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGgzIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvY2xpbWF0ZS1hY3Rpb24vIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+Q2xpbWF0ZSBBY3Rpb248L2E+PC9oMz48dWwgY2xhc3M9J25vaW5kZXgnPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2NsaW1hdGUtYWN0aW9uL3RvcGljcyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPlRvcGljczwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2NsaW1hdGUtYWN0aW9uL3B1YmxpY2F0aW9ucyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPlB1YmxpY2F0aW9uczwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2NsaW1hdGUtYWN0aW9uL2NvbnN1bHRhdGlvbnMiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db25zdWx0YXRpb25zPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvY2xpbWF0ZS1hY3Rpb24vbGVnaXNsYXRpb24iIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5MZWdpc2xhdGlvbjwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25ld3MtYW5kLW1lZGlhL3ByZXNzLXJlbGVhc2VzL1BhZ2VzL2RlZmF1bHQuYXNweCNEZWZhdWx0PSU3QiUyMmslMjIlM0ElMjIlMjIlMkMlMjJyJTIyJTNBJTVCJTdCJTIybiUyMiUzQSUyMkRDQ0FFU2VjdGlvbnNNTVNNdWx0aVRhZyUyMiUyQyUyMnQlMjIlM0ElNUIlMjIlNUMlMjIlQzclODIlQzclODI0MzZjNjk2ZDYxNzQ2NTIwNDE2Mzc0Njk2ZjZlJTVDJTIyJTIyJTVEJTJDJTIybyUyMiUzQSUyMmFuZCUyMiUyQyUyMmslMjIlM0FmYWxzZSUyQyUyMm0lMjIlM0FudWxsJTdEJTVEJTdEIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+UHJlc3MgUmVsZWFzZXM8L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb250YWN0LXVzIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+Q29udGFjdCBVczwvYT48L2xpPjwvdWw+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxoMyBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2Vudmlyb25tZW50IiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+RW52aXJvbm1lbnQ8L2E+PC9oMz48dWwgY2xhc3M9J25vaW5kZXgnPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2Vudmlyb25tZW50L3RvcGljcyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPlRvcGljczwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2Vudmlyb25tZW50L3B1YmxpY2F0aW9ucyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPlB1YmxpY2F0aW9uczwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2Vudmlyb25tZW50L2NvbnN1bHRhdGlvbnMiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db25zdWx0YXRpb25zPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvZW52aXJvbm1lbnQvbGVnaXNsYXRpb24iIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5MZWdpc2xhdGlvbjwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25ld3MtYW5kLW1lZGlhL3ByZXNzLXJlbGVhc2VzL1BhZ2VzL2RlZmF1bHQuYXNweCNEZWZhdWx0PSU3QiUyMmslMjIlM0ElMjIlMjIlMkMlMjJyJTIyJTNBJTVCJTdCJTIybiUyMiUzQSUyMkRDQ0FFU2VjdGlvbnNNTVNNdWx0aVRhZyUyMiUyQyUyMnQlMjIlM0ElNUIlMjIlNUMlMjIlQzclODIlQzclODI0NTZlNzY2OTcyNmY2ZTZkNjU2ZTc0JTVDJTIyJTIyJTVEJTJDJTIybyUyMiUzQSUyMmFuZCUyMiUyQyUyMmslMjIlM0FmYWxzZSUyQyUyMm0lMjIlM0FudWxsJTdEJTVEJTdEIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+UHJlc3MgUmVsZWFzZXM8L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb250YWN0LXVzLyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPkNvbnRhY3QgVXM8L2E+PC9saT48L3VsPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48aDMgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9lbmVyZ3kiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5FbmVyZ3k8L2E+PC9oMz48dWwgY2xhc3M9J25vaW5kZXgnPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2VuZXJneS90b3BpY3MiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Ub3BpY3M8L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9lbmVyZ3kvcHVibGljYXRpb25zIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+UHVibGljYXRpb25zPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvZW5lcmd5L0dyYW50cy1hbmQtU3Vic2lkaWVzIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+R3JhbnRzICYgU3Vic2lkaWVzPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvZW5lcmd5L2NvbnN1bHRhdGlvbnMiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db25zdWx0YXRpb25zPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvZW5lcmd5L2xlZ2lzbGF0aW9uIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+TGVnaXNsYXRpb248L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9uZXdzLWFuZC1tZWRpYS9wcmVzcy1yZWxlYXNlcy9QYWdlcy9kZWZhdWx0LmFzcHgjRGVmYXVsdD0lN0IlMjJrJTIyJTNBJTIyJTIyJTJDJTIyciUyMiUzQSU1QiU3QiUyMm4lMjIlM0ElMjJEQ0NBRVNlY3Rpb25zTU1TTXVsdGlUYWclMjIlMkMlMjJ0JTIyJTNBJTVCJTIyJTVDJTIyJUM3JTgyJUM3JTgyNDU2ZTY1NzI2Nzc5JTVDJTIyJTIyJTVEJTJDJTIybyUyMiUzQSUyMmFuZCUyMiUyQyUyMmslMjIlM0FmYWxzZSUyQyUyMm0lMjIlM0FudWxsJTdEJTVEJTdEIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+UHJlc3MgUmVsZWFzZXM8L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb250YWN0LXVzIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+Q29udGFjdCBVczwvYT48L2xpPjwvdWw+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxoMyBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25hdHVyYWwtcmVzb3VyY2VzIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+TmF0dXJhbCBSZXNvdXJjZXM8L2E+PC9oMz48dWwgY2xhc3M9J25vaW5kZXgnPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25hdHVyYWwtcmVzb3VyY2VzL3RvcGljcyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPlRvcGljczwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25hdHVyYWwtcmVzb3VyY2VzL3B1YmxpY2F0aW9ucyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPlB1YmxpY2F0aW9uczwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25hdHVyYWwtcmVzb3VyY2VzL2xpY2Vuc2luZyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPkxpY2Vuc2luZzwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25hdHVyYWwtcmVzb3VyY2VzL21hcHMtYW5kLWRhdGEiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5NYXBzICYgRGF0YTwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25hdHVyYWwtcmVzb3VyY2VzL2NvbnN1bHRhdGlvbnMiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db25zdWx0YXRpb25zPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvbmF0dXJhbC1yZXNvdXJjZXMvbGVnaXNsYXRpb24iIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5MZWdpc2xhdGlvbjwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL25ld3MtYW5kLW1lZGlhL3ByZXNzLXJlbGVhc2VzL1BhZ2VzL2RlZmF1bHQuYXNweCNEZWZhdWx0PSU3QiUyMmslMjIlM0ElMjIlMjIlMkMlMjJyJTIyJTNBJTVCJTdCJTIybiUyMiUzQSUyMkRDQ0FFU2VjdGlvbnNNTVNNdWx0aVRhZyUyMiUyQyUyMnQlMjIlM0ElNUIlMjIlNUMlMjIlQzclODIlQzclODI0ZTYxNzQ3NTcyNjE2YzIwNTI2NTczNmY3NTcyNjM2NTczJTVDJTIyJTIyJTVEJTJDJTIybyUyMiUzQSUyMmFuZCUyMiUyQyUyMmslMjIlM0FmYWxzZSUyQyUyMm0lMjIlM0FudWxsJTdEJTVEJTdEIiB0YXJnZXQ9Il9zZWxmIiBjbGFzcz0ibm9pbmRleCI+UHJlc3MgUmVsZWFzZXM8L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb250YWN0LXVzL1BhZ2VzL05hdHVyYWwtUmVzb3VyY2VzLmFzcHgiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5Db250YWN0IFVzPC9hPjwvbGk+PC91bD48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGgzIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvYWJvdXQtdXMvY29tcGxpYW5jZSIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPkNvbXBsaWFuY2U8L2E+PC9oMz48dWwgY2xhc3M9J25vaW5kZXgnPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2Fib3V0LXVzL2NvbXBsaWFuY2Uvb2ZmaWNpYWwtbGFuZ3VhZ2VzLWFjdC9QYWdlcy9PZmZpY2lhbC1MYW5ndWFnZXMtQWN0LmFzcHgiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5PZmZpY2lhbCBMYW5ndWFnZXMgQWN0PC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvYWJvdXQtdXMvY29tcGxpYW5jZS9mcmVlZG9tLW9mLWluZm9ybWF0aW9uLShmb2kpL1BhZ2VzL0ZyZWVkb20tb2YtSW5mb3JtYXRpb24tKEZPSSkuYXNweCIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPkZyZWVkb20gb2YgSW5mb3JtYXRpb248L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb21wbGlhbmNlL3JlLXVzZS1vZi1wdWJsaWMtc2VjdG9yLWluZm9ybWF0aW9uL1BhZ2VzL1JlLVVzZS1vZi1QdWJsaWMtU2VjdG9yLUluZm9ybWF0aW9uLmFzcHgiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5SZS1Vc2Ugb2YgSW5mb3JtYXRpb248L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb21wbGlhbmNlL2FjY2Vzcy10by1pbmZvcm1hdGlvbi1vbi10aGUtZW52aXJvbm1lbnQtKGFpZSkvUGFnZXMvQWNjZXNzLXRvLUluZm9ybWF0aW9uLW9uLXRoZS1FbnZpcm9ubWVudCUyMChBSUUpLmFzcHgiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5BY2Nlc3MgdG8gSW5mb3JtYXRpb24gb24gdGhlIEVudmlyb25tZW50PC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvYWJvdXQtdXMvY29tcGxpYW5jZS9hY2Nlc3NpYmlsaXR5LXN0YXRlbWVudC9QYWdlcy9BY2Nlc3NpYmlsaXR5LmFzcHgiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5BY2Nlc3NpYmlsaXR5IFN0YXRlbWVudDwvYT48L2xpPjxsaSBjbGFzcz0nbm9pbmRleCc+PGEgaHJlZj0iL2VuLWllL2Fib3V0LXVzL2NvbXBsaWFuY2UvZGlzY2xhaW1lci9QYWdlcy9kZWZhdWx0LmFzcHgiIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5EaXNjbGFpbWVyPC9hPjwvbGk+PGxpIGNsYXNzPSdub2luZGV4Jz48YSBocmVmPSIvZW4taWUvYWJvdXQtdXMvY29tcGxpYW5jZS9wcml2YWN5LWFuZC1jb29raWUtcG9saWN5L1BhZ2VzL1ByaXZhY3ktYW5kLUNvb2tpZS1Qb2xpY3kuYXNweCIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPlByaXZhY3kgYW5kIENvb2tpZSBQb2xpY3k8L2E+PC9saT48bGkgY2xhc3M9J25vaW5kZXgnPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb21wbGlhbmNlL2RhdGEtcHJvdGVjdGlvbi8iIHRhcmdldD0iX3NlbGYiIGNsYXNzPSJub2luZGV4Ij5EYXRhIFByb3RlY3Rpb248L2E+PC9saT48L3VsPjwvbGk+PC91bD5kAgEPFgIfAwWUBDxhIGhyZWY9Ii9lbi1pZS9uZXdzLWFuZC1tZWRpYSIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPk5ld3MgYW5kIE1lZGlhPC9hPjxzcGFuIGNsYXNzPSdub2luZGV4Jz58PC9zcGFuPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPkFib3V0IFVzPC9hPjxzcGFuIGNsYXNzPSdub2luZGV4Jz58PC9zcGFuPjxhIGhyZWY9Ii9lbi1pZS9hYm91dC11cy9jb250YWN0LXVzLyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPkNvbnRhY3QgVXM8L2E+PHNwYW4gY2xhc3M9J25vaW5kZXgnPnw8L3NwYW4+PGEgaHJlZj0iL2VuLWllL2Fib3V0LXVzL2N1c3RvbWVyLXNlcnZpY2VzLyIgdGFyZ2V0PSJfc2VsZiIgY2xhc3M9Im5vaW5kZXgiPkN1c3RvbWVyIFNlcnZpY2U8L2E+PHNwYW4gY2xhc3M9J25vaW5kZXgnPnw8L3NwYW4+PGEgaHJlZj0iaHR0cHM6Ly90d2l0dGVyLmNvbS9EZXB0X0NDQUUiIHRhcmdldD0iX2JsYW5rIiBjbGFzcz0ibm9pbmRleCI+VHdpdHRlcjwvYT5kAgIPFgIfAwXGAjxhIGhyZWY9Imh0dHA6Ly93aG9kb2Vzd2hhdC5nb3YuaWUvcm9vdC9jb21tcy8iIHRhcmdldD0iX2JsYW5rIiBjbGFzcz0ibm9pbmRleCI+V2hvIGRvZXMgV2hhdDwvYT48c3BhbiBjbGFzcz0nbm9pbmRleCc+fDwvc3Bhbj48YSBocmVmPSJodHRwczovL3d3dy5sb2JieWluZy5pZS8iIHRhcmdldD0iX2JsYW5rIiBjbGFzcz0ibm9pbmRleCI+TG9iYnlpbmcuaWU8L2E+PHNwYW4gY2xhc3M9J25vaW5kZXgnPnw8L3NwYW4+PGEgaHJlZj0iaHR0cHM6Ly9maW5kZXIuZWlyY29kZS5pZS8iIHRhcmdldD0iX2JsYW5rIiBjbGFzcz0ibm9pbmRleCI+RWlyY29kZSBGaW5kZXI8L2E+ZBgBBUNjdGwwMCRQbGFjZUhvbGRlclNpZGVOYXZCb3gkUGxhY2VIb2xkZXJMZWZ0TmF2QmFyJFY0UXVpY2tMYXVuY2hNZW51Dw9kBRpDb21wbGlhbmNlXERhdGEgUHJvdGVjdGlvbmSsw4Ach39Uq1UwfRi4GRvJ7oRyM+fk+AG0MUFZtSDurA==" />
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['aspnetForm'];
if (!theForm) {
    theForm = document.aspnetForm;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script src="/WebResource.axd?d=J1otHXdp1P0G4Eq_NuXywRco72NipGaZ0ebxLQWcm9t3L1pUfOnTJdSBvRV-Za28iJRuHQhY8eiWpoqchmY7p3du_lgBg_GLNmSEKpq-xw81&amp;t=635201010537823876" type="text/javascript"></script>


<script type="text/javascript">
//<![CDATA[
var MSOWebPartPageFormName = 'aspnetForm';
var g_presenceEnabled = true;
var g_wsaEnabled = false;
var g_wsaQoSEnabled = false;
var g_wsaQoSDataPoints = [];
var g_wsaLCID = 1033;
var g_wsaListTemplateId = 850;
var g_wsaSiteTemplateId = 'CMSPUBLISHING#0';
var _fV4UI=true;var _spPageContextInfo = {webServerRelativeUrl: "\u002fen-ie\u002fabout-us\u002fcompliance\u002fdata-protection", webAbsoluteUrl: "https:\u002f\u002fwww.dccae.gov.ie\u002fen-ie\u002fabout-us\u002fcompliance\u002fdata-protection", siteAbsoluteUrl: "https:\u002f\u002fwww.dccae.gov.ie", serverRequestPath: "\u002fen-ie\u002fabout-us\u002fcompliance\u002fdata-protection\u002fPages\u002fdefault.aspx", layoutsUrl: "_layouts\u002f15", webTitle: "Data Protection", webTemplate: "39", tenantAppVersion: "0", isAppWeb: false, webLogoUrl: "\u002fSiteAssets\u002fLogo.png", webLanguage: 1033, currentLanguage: 1033, currentUICultureName: "en-US", currentCultureName: "en-IE", clientServerTimeDelta: new Date("2018-05-30T16:34:33.3422270Z") - new Date(), siteClientTag: "32660$$15.0.4841.1000", crossDomainPhotosEnabled:false, webUIVersion:15, webPermMasks:{High:16,Low:196673},pageListId:"{2eb5ff90-9678-4acd-9722-ab9c39b070b3}",pageItemId:1, pagePersonalizationScope:1, alertsEnabled:true, siteServerRelativeUrl: "\u002f", allowSilverlightPrompt:'True'};document.onreadystatechange=fnRemoveAllStatus; function fnRemoveAllStatus(){removeAllStatus(true)};
function _spNavigateHierarchy(nodeDiv, dataSourceId, dataPath, url, listInContext, type) {

    CoreInvoke('ProcessDefaultNavigateHierarchy', nodeDiv, dataSourceId, dataPath, url, listInContext, type, document.forms.aspnetForm, "", "\u002fen-ie\u002fabout-us\u002fcompliance\u002fdata-protection\u002fPages\u002fdefault.aspx");

}
//]]>
</script>

<script src="https://www.dccae.gov.ie/_layouts/15/DCCAE/js/cookiePolicy.js?r201834300533" type="text/javascript"></script>
<script src="/_layouts/15/blank.js?rev=ZaOXZEobVwykPO9g8hq%2F8A%3D%3D" type="text/javascript"></script>
<script type="text/javascript">
//<![CDATA[
if (typeof(DeferWebFormInitCallback) == 'function') DeferWebFormInitCallback();function WebForm_OnSubmit() {
UpdateFormDigest('\u002fen-ie\u002fabout-us\u002fcompliance\u002fdata-protection', 1440000);if (typeof(_spFormOnSubmitWrapper) != 'undefined') {return _spFormOnSubmitWrapper();} else {return true;};
return true;
}
//]]>
</script>

            <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ctl00$ScriptManager', 'aspnetForm', [], [], [], 90, 'ctl00');
//]]>
</script>

                       
            <div id="ms-designer-ribbon">
                <div>
	
	<div id="s4-ribbonrow" style="visibility:hidden;display:none"></div>

</div>



            </div>
            

            <div id="s4-workspace">
                <div id="s4-bodyContainer">
                    <a href="#content" class="sr-only sr-only-focusable noindex" aria-label="Skip to main content">
                    </a>
                    <!-- HEADER -->
                    <header>
                        <a name="top" id="top"></a>
                        <div class="container noindex">
                            <nav class="navbar navbar-default noindex" role="navigation">
                                <div class="navbar-header noindex">
                                    <button type="button" value="Expand Navigation" style="font-size:0px; line-height:0px;" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                        <span class="sr-only noindex" aria-label="Toggle navigation"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        Expand Navigation
                                    </button>
                                    <a class="navbar-brand noindex" id="logolink" href="#" aria-label="Department of Communications, Climate Action and Environment">
                                    </a>
                                </div>
                                <div id="navbar" class="navbar-collapse collapse noindex">
                                    <ul class='nav navbar-nav navbar-main noindex'><li><a href='/en-ie/communications' class='noindex'>Communications</a></li><li><a href='/en-ie/climate-action' class='noindex'>Climate Action</a></li><li><a href='/en-ie/environment' class='noindex'>Environment</a></li><li><a href='/en-ie/energy' class='noindex'>Energy</a></li><li><a href='/en-ie/natural-resources' class='noindex'>Natural Resources</a></li></ul>

                                    <!-- Top Bar Links -->
                                    

<ul class="nav navbar-nav navbar-top noindex">
    <li><a href="/en-ie/news-and-media/" target="_self" class="noindex">News & Media</a></li><li class='active noindex'><a href="/en-ie/about-us/" target="_self" class="noindex">About Us</a></li><li><a href="/en-ie/about-us/contact-us/" target="_self" class="noindex">Contact Us</a></li>    
    
    <li><a href='https://twitter.com/Dept_CCAE' class='twitter' target='_blank'>Twitter</a></li>
</ul>

                                    <!--end topBar -->
                                    <!-- SEARCH -->
                                    
<div class="navbar-form" role="search">
    <div class="form-group">
        <label class="sr-only" for="sitesearch">Search</label>
        <input type="text" placeholder="Search" value="" maxlength="2048" accesskey="S" title="Search" id="sitesearch" class="form-control" data-pageurl="https://www.dccae.gov.ie/en-ie/pages/search.aspx" />
    </div>
    <button id="SearchTextBoxLink" type="submit"><span class="glyphicon glyphicon-search"></span><span class="sr-only">Submit Search</span></button>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#sitesearch").keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                var $this = jQuery(this);
                url = $this.data("pageurl");
                if (url) {
                    document.location = url + "?k=" + $this.val();
                }
                return false;
            }
        });
        jQuery("#SearchTextBoxLink").click(function (e) {
            e.preventDefault();
            var $this = jQuery("#sitesearch");
            url = $this.data("pageurl");
            if (url) {
                document.location = url + "?k=" + $this.val();
            }
        });
    });
</script>

                                    
                                    <script type="text/javascript">
                                        jQuery(document).ready(function () {

                                            var breadcrumbNode = jQuery('.breadcrumb li:nth-child(2)').html();
                                            var leftNavNode = '';
                                            if (breadcrumbNode != undefined && breadcrumbNode.indexOf("</a>") != -1)
                                                leftNavNode = breadcrumbNode;
                                            else {
                                                //We are at the Lvl1 sites, cant get the url from here, so we build it.
                                                leftNavNode = "<a href='" + _spPageContextInfo.webServerRelativeUrl + "'>" + _spPageContextInfo.webTitle + "</a>";
                                            }
                                            jQuery('<h3><strong>' + leftNavNode + '</strong></h3>').insertBefore(jQuery('.nav-left').find('ul').first())
                                            // Left sidebar menu
                                            // - ensure selected class is propagated to parents
                                            jQuery('#sideNav .ms-core-listMenu-root li.selected').parents('li.static.has-submenu').addClass('selected');
                                            // - ensure selected li is expanded
                                            jQuery('#sideNav .ms-core-listMenu-root li.selected.has-submenu:not(.expanded) span.ql-toggle-button').click();
                                            // - ensure only selected li are expanded (retract the others)
                                            jQuery('#sideNav .ms-core-listMenu-root li.has-submenu.expanded:not(.selected) span.ql-toggle-button').click();

                                            jQuery('h2.ms-webpart-titleText').each(function () { jQuery(this).remove() })
                                        });
                                    </script>
                                    <!-- END SEARCH -->
                                </div>
                            </nav>
                        </div>
                    </header>
                    <!-- /.header -->
                    <div id="roleMain" role="main">
                        <!-- BREACRUMB -->
                        <div class="container noindex">
                            <p class="sr-only">
                                You are here:
                            </p>
                            <ol class='breadcrumb noindex'><li><a href='/en-ie' class='noindex' >Home</a></li><li><a href='/en-ie/about-us' class='noindex' >About Us</a></li><li><a href='/en-ie/about-us/compliance' class='noindex' >Compliance</a></li><li class='active noindex'>Data Protection</li></ol>

                        </div>
                        <!-- /.breadcrumb -->
                        <!-- CONTENT -->
                    <div id="content" tabindex="-1">
                        <!-- PAGE CONTENT COLUMNS -->
                        <div class="container" >
                            <div class="row">
                                <!-- Right Column -->
                                <div class="col-md-9 col-md-push-3">
                                    <!-- MAIN PAGE CONTENT HERE -->
                                    <div data-name="ContentPlaceHolderMain">
                                        <div id="DeltaPlaceHolderMain">
	
                                            
	<div class="content">        
        <div id="ctl00_PlaceHolderMain_EditModePanel3">
		<h1 id="H1">
				Data Protection
			</h1>
	    
	</div><div class="row">
            <div class="col-sm-12">
            
		    <div class="contentimg">
			    <div class="image">
				    <div id="ctl00_PlaceHolderMain_EditModePanel7">
				 
					    
				    
	</div> 
			    </div>
		    </div>
                <div id="ctl00_PlaceHolderMain_Page_x0020_Content_label" style='display:none'>Page Content</div><div id="ctl00_PlaceHolderMain_Page_x0020_Content__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_Page_x0020_Content_label"><p>​​The Department is committed to protecting the rights and privacy of individuals in accordance with both European Union and Irish data protection legislation. The Department needs to lawfully &amp; fairly process personal data about employees, customers, suppliers and other individuals in order to achieve its mission and functions. The data protection legislation confers rights on individuals as well as responsibilities on those persons processing personal data. </p><p>Below are&#160;our current Data Privacy Notice and Statement&#58;&#160;</p><table width="100%" class="dcenr-rteTable-default" cellspacing="0" style="height&#58;186px;"><tbody><tr class="dcenr-rteTableEvenRow-default"><td class="dcenr-rteTableEvenCol-default" style="width&#58;50%;"><p></p><h4 class="dcenr-rteElement-H4">​<a href="/documents/DCCAE%20Privacy%20Statement.pdf" target="_blank">Data Privacy Statement - English </a></h4><p></p></td><td class="dcenr-rteTableOddCol-default" style="width&#58;50%;"><p></p><h4 class="dcenr-rteElement-H4"><a href="/documents/DCCAE%20Privacy%20Statement%20GA.pdf" target="_blank">​Data Privacy Statement - Irish</a></h4><p></p></td></tr><tr class="dcenr-rteTableOddRow-default"><td class="dcenr-rteTableEvenCol-default"></td><td class="dcenr-rteTableOddCol-default"></td></tr><tr class="dcenr-rteTableEvenRow-default"><td class="dcenr-rteTableEvenCol-default" rowspan="1"><p></p><h4 class="dcenr-rteElement-H4">​<a href="/documents/DCCAE%20Privacy%20Notice_full.pdf" target="_blank">Data Privacy Notice - English </a></h4><p></p></td><td class="dcenr-rteTableOddCol-default" rowspan="1"><p></p><h4 class="dcenr-rteElement-H4">​<a href="/documents/DCCAE%20Privacy%20NoticeGA_full.pdf">Data Privacy Notice - Irish</a></h4><p></p></td></tr></tbody></table><p><br>&#160;Further information on how we mange Data Protection,&#160;contact our Dtata Protection Officer at <a href="mailto&#58;dataprotection@dccae.gov.ie">dataprotection@dccae.gov.ie</a> </p><p><br>&#160;</p></div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField2_label" style='display:none'>Tile 1 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField2__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField2_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField1_label" style='display:none'>Tile 2 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField1__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField1_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField3_label" style='display:none'>Tile 3 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField3__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField3_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField4_label" style='display:none'>Tile 4 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField4__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField4_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField5_label" style='display:none'>Tile 5 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField5__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField5_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField6_label" style='display:none'>Tile 6 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField6__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField6_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField7_label" style='display:none'>Tile 7 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField7__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField7_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField8_label" style='display:none'>Tile 8 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField8__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField8_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField9_label" style='display:none'>Tile 9 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField9__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField9_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField10_label" style='display:none'>Tile 10 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField10__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField10_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField11_label" style='display:none'>Tile 11 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField11__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField11_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
               <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField12_label" style='display:none'>Tile 12 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField12__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField12_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField113_label" style='display:none'>Tile 13 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField113__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField113_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
               <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField14_label" style='display:none'>Tile 14 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField14__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField14_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField15_label" style='display:none'>Tile 15 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField15__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField15_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
               <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField16_label" style='display:none'>Tile 16 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField16__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField16_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-6 box" style="display:none">
                <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField17_label" style='display:none'>Tile 17 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField17__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField17_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
            <div class="col-sm-6 box" style="display:none">
               <h2>
				</h2>
                
        
    
				<div id="ctl00_PlaceHolderMain_RichHtmlField18_label" style='display:none'>Tile 18 Description</div><div id="ctl00_PlaceHolderMain_RichHtmlField18__ControlWrapper_RichHtmlField" class="ms-rtestate-field" style="display:inline" aria-labelledby="ctl00_PlaceHolderMain_RichHtmlField18_label"></div>
                <h6><a href="#">
				</a></h6>
            </div> 
        </div>
    </div>
     <script type="text/javascript">
         jQuery(function () {
             jQuery("body").addClass("home");
             jQuery("#main").removeClass("container");
         });

         jQuery(document).ready(function () {
             jQuery('.box img').each(function () {
                 jQuery(this).attr('class', 'pull-left');
                 var url = jQuery(this).attr('src');
                 urlArr = url.toLowerCase().split('?');
                 url = urlArr[0] + '?height=90&width=90'
                 jQuery(this).attr('src', url);
             });

             //Hide featured links if not populated
             jQuery("h6").each(function (index) {
                 //Set Featured link = image link
                 var h6Link = jQuery(this).find('a');
                 h6Link.attr('href', jQuery(this).siblings('a').attr('href'));
                 h6Link.attr('target', jQuery(this).siblings('a').attr('target'));
                 if (h6Link.attr('href') == '#' || h6Link.text().trim() == '') {
                     h6Link.css('display', 'none');
                 }
             });

             //hide titles if not popoulated
             jQuery('.box h2').each(function () {
                 if (jQuery(this).text().trim() != '') {
                     jQuery(this).parent('div').css('display', 'block');
                 }
				  else {
                     jQuery(this).parent('div').remove();
                 }
             });


         });
    </script>
	<style>
	.nav-left {
    margin: 48px 0 16px !important;
}
</style>
<div style='display:none' id='hidZone'><menu class="ms-hide">
		<ie:menuitem id="MSOMenu_Help" iconsrc="/_layouts/15/images/HelpIcon.gif" onmenuclick="MSOWebPartPage_SetNewWindowLocation(MenuWebPart.getAttribute('helpLink'), MenuWebPart.getAttribute('helpMode'))" text="Help" type="option" style="display:none">

		</ie:menuitem>
	</menu></div>
                                        
</div>

                                    </div>
                                </div>
                                <!-- /.right columns -->
                                <!-- Left Column (side menu)-->
                                <div class="col-md-3 col-md-pull-9">                                    
                                    
										<div id="sideNav" style="display:none;" class="nav-left">
                                            <div id="DeltaPlaceHolderLeftNavBar" class="ms-core-navigation" role="directory">
	
                                                
													<a id="startNavigation" name="startNavigation" tabindex="-1"></a>
                                                    
                                                    
                                                    
                                                    
                                                    
                                                    <div class="ms-core-sideNavBox-removeLeftMargin" id="sideNavContents">
                                                        <div id="ctl00_PlaceHolderSideNavBox_PlaceHolderLeftNavBar_QuickLaunchNavigationManager">
		
                                                            
															<div id="zz1_V4QuickLaunchMenu" class=" noindex ms-core-listMenu-verticalBox">
			<ul id="zz2_RootAspMenu" class="root ms-core-listMenu-root static">
				<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-department/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Our Department</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-department/communications/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Communications</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-department/climate-action/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Climate Action</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-department/environment/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Environment</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-department/energy/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Energy</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-department/natural-resources/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Natural Resources</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-ministers/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Our Ministers</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-ministers/minister/Pages/Minister-Denis-Naughten.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Minister Denis Naughten</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-ministers/minister-of-state/Pages/Minister-of-State.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Minister of State</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Our Structure</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/management-board/Pages/Management-Board.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Management Board</span></span></a><ul class="static">
						<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/management-board/secretary-general/Pages/Secretary-General---Mark-Griffin-.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Secretary General</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/management-board/Pages/Assistant-Secretary-Communications.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Assistant Secretary Communications</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/management-board/assistant-secretary-energy/Pages/Michael-Manley---Assistant-Secretary-.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Assistant Secretary Energy</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/management-board/assistant-secretary-corporate-services/Pages/Patricia-Cronin---Assistant-Secretary-.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Assistant Secretary Corporate Services</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/management-board/assistant-secretary-environment-and-climate-action/Pages/Brian Carroll---Assistant-Secretary-.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Assistant Secretary Environment &amp; Climate Action</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/management-board/assistant-secretary-natural-resources-and-waste-policy/Pages/Matthew-Collins-Assistant-Secretary.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Assistant Secretary Natural Resources and Waste Policy</span></span></a></li>
					</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/organisation-chart/Pages/Organisation-Chart.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Organisation Chart</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-structure/state-bodies/Pages/State Bodies.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">State Bodies</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/our-strategy/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Our Strategy</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/customer-services/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Customer Service</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/customer-services/Pages/Complaints-and-Appeals-Process.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Complaints and Appeals</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/customer-services/Pages/Contacts-for-Planning---SEAEPA.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Contacts for Planning SEA EPA</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Compliance</span></span></a><ul class="static">
					<li class="static selected"><a class="static selected menu-item ms-core-listMenu-item ms-displayInline ms-bold ms-core-listMenu-selected ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/data-protection/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Data Protection</span><span class="ms-hidden">Currently selected</span></span></a><ul class="static">
						<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/data-protection/Pages/Subject-Access-Requests.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Subject Access Requests</span></span></a></li>
					</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/freedom-of-information-(foi)/Pages/Freedom-of-Information-(FOI).aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Freedom of Information (FOI)</span></span></a><ul class="static">
						<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/freedom-of-information-(foi)/making-a-request/Pages/Making-a-Request.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Making a Request</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/freedom-of-information-(foi)/legistlation/Pages/legislation.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Legislation</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/freedom-of-information-(foi)/published-foi-requests/Pages/Published-FOI-Requests.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Published FOI Requests</span></span></a><ul class="static">
							<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" title="Disclosure Log 2018" href="/en-ie/about-us/compliance/freedom-of-information-(foi)/published-foi-requests/Pages/Disclosure-Log-2018.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">FOI Disclosure Log 2018</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/freedom-of-information-(foi)/published-foi-requests/published-foi-requests/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">FOI Requests Since 2007</span></span></a></li>
						</ul></li>
					</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/foi-section-8-publication-scheme/Pages/FOI-Section-8-Publication-Scheme.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">FOI Section 8 Publication Scheme</span></span></a><ul class="static">
						<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/foi-section-8-publication-scheme/about-us-and-what-we-do/Pages/About-us-and-what-we-do.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">About us and what we do</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/foi-section-8-publication-scheme/our-decision-making-processes/Pages/Our-decision-making-processes.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Our decision making processes</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/foi-section-8-publication-scheme/our-functions-and-services/Pages/Our-functions-and-services.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Our functions and services</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/foi-section-8-publication-scheme/financial-information/Pages/Financial-information.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Financial Information</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/foi-section-8-publication-scheme/procurement-information/Pages/Procurement-information.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Procurement Information</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/foi-section-8-publication-scheme/other-relevant-information/Pages/Other-relevant-information.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Other Relevant Information</span></span></a></li>
					</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/re-use-of-public-sector-information/Pages/Re-Use-of-Public-Sector-Information.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Re-Use of Public Sector Information</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/access-to-information-on-the-environment-(aie)/Pages/Access-to-Information-on-the-Environment (AIE).aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Access to Information on the Environment (AIE)</span></span></a><ul class="static">
						<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/access-to-information-on-the-environment-(aie)/aie-legislation/Pages/AIE-Legislation.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">AIE Legislation</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/access-to-information-on-the-environment-(aie)/aie-statistics/Pages/AIE-Statistics.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">AIE Statistics</span></span></a><ul class="static">
							<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/access-to-information-on-the-environment-(aie)/aie-statistics/departmental-aie-statistics-/Pages/Department-AIE-Statistics.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Departmental AIE Statistics</span></span></a><ul class="static">
								<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" title="Disclosure Log 2017" href="/en-ie/about-us/compliance/access-to-information-on-the-environment-(aie)/aie-statistics/departmental-aie-statistics-/Pages/Disclosure-Log-2017.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">AIE Disclosure Log 2018</span></span></a></li>
							</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/access-to-information-on-the-environment-(aie)/aie-statistics/Pages/National-AIE-Statistics.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">National AIE Statistics</span></span></a></li>
						</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/access-to-information-on-the-environment-(aie)/making-a-request/Pages/making-a-request.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Making a Request</span></span></a></li>
					</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/accessibility-statement/Pages/Accessibility.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Accessibility Statement</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/privacy-and-cookie-policy/Pages/Privacy-and-Cookie-Policy.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Privacy and Cookie Policy</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/disclaimer/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Disclaimer</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/regulation-of-lobbying-act-2015/Pages/Regulation-of-Lobbying-Act-2015.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Regulation of Lobbying Act 2015</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/protected-disclosures-act/Pages/Protected-Disclosures-Act.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Protected Disclosures Act</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/official-languages-act/Pages/Official-Languages-Act.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Irish Language</span></span></a><ul class="static">
						<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/official-languages-act/language-scheme/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Language Scheme</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/official-languages-act/official-languages-act/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Official Languages Act</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/official-languages-act/20-year-strategy-for-the-irish-language/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">20 Year Strategy for the Irish Language</span></span></a></li>
					</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/compliance/children-first-act-2015/Pages/Children-First-Act-2015.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Children First Act 2015</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Corporate Information</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/accounts-and-payments/Pages/Accounts-and-Payments.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Accounts and Payments</span></span></a><ul class="static">
						<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/accounts-and-payments/Pages/Payments-over-€20,000.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Payments over €20,000</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/accounts-and-payments/Pages/State-Agency-Prompt-Payment-Returns.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">State Agency Prompt Payment Returns</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/accounts-and-payments/Pages/Prompt-Payment-Returns.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Prompt Payment Returns</span></span></a></li>
					</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/audits/Pages/Audits.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Audits</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/appointments-to-state-boards/Pages/Appointments.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Appointments to State Boards</span></span></a><ul class="static">
						<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/appointments-to-state-boards/Pages/State-Boards.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">State Bodies</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/appointments-to-state-boards/Pages/eGovernance.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">eGovernance</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/corporate-information/appointments-to-state-boards/Pages/Current-Vacancies.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Current Vacancies</span></span></a></li>
					</ul></li>
				</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/working-with-us/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Working With Us</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/working-with-us/Pages/Procurement-Opportunities.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Procurement Opportunities</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/working-with-us/Pages/Working-with-us.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Career Opportunities</span></span></a></li>
				</ul></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/default.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Contact Us</span></span></a><ul class="static">
					<li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Communications.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Communications </span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Climate-Action.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Climate Action</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Environment.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Environment</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Energy.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Energy </span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Natural-Resources.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Natural Resources </span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Customer-Services.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Customer Service</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Press-Office.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Press Office</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Minister&#39;s-Office.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Minister&#39;s Office</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Social-Media.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Social Media</span></span></a></li><li class="static"><a class="static menu-item ms-core-listMenu-item ms-displayInline ms-navedit-linkNode" tabindex="0" href="/en-ie/about-us/contact-us/Pages/Our-Locations.aspx"><span class="additional-background ms-navedit-flyoutArrow"><span class="menu-item-text">Our Location</span></span></a></li>
				</ul></li>
			</ul>
		</div>
                                                        
	</div>
                                                        
                                                        
															<!--<div class="ms-core-listMenu-verticalBox">
                                        
                                    </div>-->
                                                        
                                                    </div>
                                                
                                            
</div>
                                        </div>
                                    
                                </div>
                                <!-- /.left columns -->
                            </div>
                        </div>
                        <!-- /.page content columns -->
                    </div>
                    </div>

                    <!-- /.content -->
                    <!-- FOOTER -->
                    <footer>
                        <div id="footer" class="container ms-dialogHidden">
                            
<p class="backtotop noindex"><a href="#top" class="noidex" title="Back to top">Back to top of page</a></p>
<div class="footertop noindex">
    <ul class='clearfix noindex'><li class='noindex'><h3 class='noindex'><a href="/en-ie/communications" target="_self" class="noindex">Communications</a></h3><ul class='noindex'><li class='noindex'><a href="/en-ie/communications/topics" target="_self" class="noindex">Topics</a></li><li class='noindex'><a href="/en-ie/communications/publications/" target="_self" class="noindex">Publications</a></li><li class='noindex'><a href="/en-ie/communications/programmes-and-schemes" target="_self" class="noindex">Programmes & Schemes</a></li><li class='noindex'><a href="/en-ie/communications/consultations" target="_self" class="noindex">Consultations</a></li><li class='noindex'><a href="/en-ie/communications/legislation" target="_self" class="noindex">Legislation</a></li><li class='noindex'><a href="/en-ie/news-and-media/press-releases/Pages/default.aspx#Default=%7B%22k%22%3A%22%22%2C%22r%22%3A%5B%7B%22n%22%3A%22DCCAESectionsMMSMultiTag%22%2C%22t%22%3A%5B%22%5C%22%C7%82%C7%82436f6d6d756e69636174696f6e73%5C%22%22%5D%2C%22o%22%3A%22and%22%2C%22k%22%3Afalse%2C%22m%22%3Anull%7D%5D%7D" target="_self" class="noindex">Press Releases</a></li><li class='noindex'><a href="/en-ie/about-us/contact-us/Pages/Communications.aspx" target="_self" class="noindex">Contact Us</a></li></ul></li><li class='noindex'><h3 class='noindex'><a href="/en-ie/climate-action/" target="_self" class="noindex">Climate Action</a></h3><ul class='noindex'><li class='noindex'><a href="/en-ie/climate-action/topics" target="_self" class="noindex">Topics</a></li><li class='noindex'><a href="/en-ie/climate-action/publications" target="_self" class="noindex">Publications</a></li><li class='noindex'><a href="/en-ie/climate-action/consultations" target="_self" class="noindex">Consultations</a></li><li class='noindex'><a href="/en-ie/climate-action/legislation" target="_self" class="noindex">Legislation</a></li><li class='noindex'><a href="/en-ie/news-and-media/press-releases/Pages/default.aspx#Default=%7B%22k%22%3A%22%22%2C%22r%22%3A%5B%7B%22n%22%3A%22DCCAESectionsMMSMultiTag%22%2C%22t%22%3A%5B%22%5C%22%C7%82%C7%82436c696d61746520416374696f6e%5C%22%22%5D%2C%22o%22%3A%22and%22%2C%22k%22%3Afalse%2C%22m%22%3Anull%7D%5D%7D" target="_self" class="noindex">Press Releases</a></li><li class='noindex'><a href="/en-ie/about-us/contact-us" target="_self" class="noindex">Contact Us</a></li></ul></li><li class='noindex'><h3 class='noindex'><a href="/en-ie/environment" target="_self" class="noindex">Environment</a></h3><ul class='noindex'><li class='noindex'><a href="/en-ie/environment/topics" target="_self" class="noindex">Topics</a></li><li class='noindex'><a href="/en-ie/environment/publications" target="_self" class="noindex">Publications</a></li><li class='noindex'><a href="/en-ie/environment/consultations" target="_self" class="noindex">Consultations</a></li><li class='noindex'><a href="/en-ie/environment/legislation" target="_self" class="noindex">Legislation</a></li><li class='noindex'><a href="/en-ie/news-and-media/press-releases/Pages/default.aspx#Default=%7B%22k%22%3A%22%22%2C%22r%22%3A%5B%7B%22n%22%3A%22DCCAESectionsMMSMultiTag%22%2C%22t%22%3A%5B%22%5C%22%C7%82%C7%82456e7669726f6e6d656e74%5C%22%22%5D%2C%22o%22%3A%22and%22%2C%22k%22%3Afalse%2C%22m%22%3Anull%7D%5D%7D" target="_self" class="noindex">Press Releases</a></li><li class='noindex'><a href="/en-ie/about-us/contact-us/" target="_self" class="noindex">Contact Us</a></li></ul></li><li class='noindex'><h3 class='noindex'><a href="/en-ie/energy" target="_self" class="noindex">Energy</a></h3><ul class='noindex'><li class='noindex'><a href="/en-ie/energy/topics" target="_self" class="noindex">Topics</a></li><li class='noindex'><a href="/en-ie/energy/publications" target="_self" class="noindex">Publications</a></li><li class='noindex'><a href="/en-ie/energy/Grants-and-Subsidies" target="_self" class="noindex">Grants & Subsidies</a></li><li class='noindex'><a href="/en-ie/energy/consultations" target="_self" class="noindex">Consultations</a></li><li class='noindex'><a href="/en-ie/energy/legislation" target="_self" class="noindex">Legislation</a></li><li class='noindex'><a href="/en-ie/news-and-media/press-releases/Pages/default.aspx#Default=%7B%22k%22%3A%22%22%2C%22r%22%3A%5B%7B%22n%22%3A%22DCCAESectionsMMSMultiTag%22%2C%22t%22%3A%5B%22%5C%22%C7%82%C7%82456e65726779%5C%22%22%5D%2C%22o%22%3A%22and%22%2C%22k%22%3Afalse%2C%22m%22%3Anull%7D%5D%7D" target="_self" class="noindex">Press Releases</a></li><li class='noindex'><a href="/en-ie/about-us/contact-us" target="_self" class="noindex">Contact Us</a></li></ul></li><li class='noindex'><h3 class='noindex'><a href="/en-ie/natural-resources" target="_self" class="noindex">Natural Resources</a></h3><ul class='noindex'><li class='noindex'><a href="/en-ie/natural-resources/topics" target="_self" class="noindex">Topics</a></li><li class='noindex'><a href="/en-ie/natural-resources/publications" target="_self" class="noindex">Publications</a></li><li class='noindex'><a href="/en-ie/natural-resources/licensing" target="_self" class="noindex">Licensing</a></li><li class='noindex'><a href="/en-ie/natural-resources/maps-and-data" target="_self" class="noindex">Maps & Data</a></li><li class='noindex'><a href="/en-ie/natural-resources/consultations" target="_self" class="noindex">Consultations</a></li><li class='noindex'><a href="/en-ie/natural-resources/legislation" target="_self" class="noindex">Legislation</a></li><li class='noindex'><a href="/en-ie/news-and-media/press-releases/Pages/default.aspx#Default=%7B%22k%22%3A%22%22%2C%22r%22%3A%5B%7B%22n%22%3A%22DCCAESectionsMMSMultiTag%22%2C%22t%22%3A%5B%22%5C%22%C7%82%C7%824e61747572616c205265736f7572636573%5C%22%22%5D%2C%22o%22%3A%22and%22%2C%22k%22%3Afalse%2C%22m%22%3Anull%7D%5D%7D" target="_self" class="noindex">Press Releases</a></li><li class='noindex'><a href="/en-ie/about-us/contact-us/Pages/Natural-Resources.aspx" target="_self" class="noindex">Contact Us</a></li></ul></li><li class='noindex'><h3 class='noindex'><a href="/en-ie/about-us/compliance" target="_self" class="noindex">Compliance</a></h3><ul class='noindex'><li class='noindex'><a href="/en-ie/about-us/compliance/official-languages-act/Pages/Official-Languages-Act.aspx" target="_self" class="noindex">Official Languages Act</a></li><li class='noindex'><a href="/en-ie/about-us/compliance/freedom-of-information-(foi)/Pages/Freedom-of-Information-(FOI).aspx" target="_self" class="noindex">Freedom of Information</a></li><li class='noindex'><a href="/en-ie/about-us/compliance/re-use-of-public-sector-information/Pages/Re-Use-of-Public-Sector-Information.aspx" target="_self" class="noindex">Re-Use of Information</a></li><li class='noindex'><a href="/en-ie/about-us/compliance/access-to-information-on-the-environment-(aie)/Pages/Access-to-Information-on-the-Environment%20(AIE).aspx" target="_self" class="noindex">Access to Information on the Environment</a></li><li class='noindex'><a href="/en-ie/about-us/compliance/accessibility-statement/Pages/Accessibility.aspx" target="_self" class="noindex">Accessibility Statement</a></li><li class='noindex'><a href="/en-ie/about-us/compliance/disclaimer/Pages/default.aspx" target="_self" class="noindex">Disclaimer</a></li><li class='noindex'><a href="/en-ie/about-us/compliance/privacy-and-cookie-policy/Pages/Privacy-and-Cookie-Policy.aspx" target="_self" class="noindex">Privacy and Cookie Policy</a></li><li class='noindex'><a href="/en-ie/about-us/compliance/data-protection/" target="_self" class="noindex">Data Protection</a></li></ul></li></ul>
</div>
<div class="footermid noindex">
    <p class='noindex'>
        <a href="/en-ie/news-and-media" target="_self" class="noindex">News and Media</a><span class='noindex'>|</span><a href="/en-ie/about-us" target="_self" class="noindex">About Us</a><span class='noindex'>|</span><a href="/en-ie/about-us/contact-us/" target="_self" class="noindex">Contact Us</a><span class='noindex'>|</span><a href="/en-ie/about-us/customer-services/" target="_self" class="noindex">Customer Service</a><span class='noindex'>|</span><a href="https://twitter.com/Dept_CCAE" target="_blank" class="noindex">Twitter</a>
        
    </p>
</div>
<div class="footerbtm noindex">
    <p class='noindex'>
        <a href="http://whodoeswhat.gov.ie/root/comms/" target="_blank" class="noindex">Who does What</a><span class='noindex'>|</span><a href="https://www.lobbying.ie/" target="_blank" class="noindex">Lobbying.ie</a><span class='noindex'>|</span><a href="https://finder.eircode.ie/" target="_blank" class="noindex">Eircode Finder</a>
        
    </p>
    <p class='noindex'>Department of Communications, Climate Action and Environment, &copy; 2018</p>
</div>

                        </div>
                    </footer>
                    <!-- /.footer -->
                    <!-- jQuery 1.12.4 (minified) -->
                    
                    <!-- Bootstrap JS (v3.3.7 / minified) -->
                    
                    
                    <script>                        //<![CDATA[
                        var g_pageLoadAnimationParams = { elementSlideIn: "sideNavBox", elementSlideInPhase2: "contentBox" };
                        jQuery(document).ready(function () {

                            if (_spPageContextInfo.siteServerRelativeUrl != "/")
                                jQuery("#logolink").attr("href", _spPageContextInfo.siteAbsoluteUrl.replace(_spPageContextInfo.siteServerRelativeUrl, "") + '/' + _spPageContextInfo.currentCultureName.toLowerCase());
                            else
                                jQuery("#logolink").attr("href", _spPageContextInfo.siteAbsoluteUrl + '/' + _spPageContextInfo.currentCultureName.toLowerCase());

                            if (_spPageContextInfo.currentCultureName.toLowerCase() == "en-us")
                                jQuery("#logolink").attr("href", _spPageContextInfo.siteAbsoluteUrl.replace(_spPageContextInfo.siteServerRelativeUrl, ""));
                            var params = window.location.search.substring(1);
                            if (params != undefined && params != '') {
                                if (jQuery('#translationLink') != undefined) {
                                    var translationLink = jQuery('#translationLink').attr('href');
                                    translationLink = translationLink + '?' + params;
                                    jQuery('#translationLink').attr('href', translationLink);
                                }
                            }
                        });
                        //]]></script>
                    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
                    <script src="/_layouts/15/DCCAE/js/ie10-viewport-bug-workaround.js">//<![CDATA[
                //]]></script>

                </div>
            </div>
              
            <div id="DeltaFormDigest">
	
                
					<script type="text/javascript">//<![CDATA[
        var formDigestElement = document.getElementsByName('__REQUESTDIGEST')[0];
        if (!((formDigestElement == null) || (formDigestElement.tagName.toLowerCase() != 'input') || (formDigestElement.type.toLowerCase() != 'hidden') ||
            (formDigestElement.value == null) || (formDigestElement.value.length <= 0)))
        {
            formDigestElement.value = '0xE5888B4720505477E5E20B1D076CAD1046D33E6335376C9ED487609180377C002623B38B1D248F5DB644358362FE8307798183ADD985B006B7894B73DA46622A,30 May 2018 16:34:33 -0000';
            g_updateFormDigestPageLoaded = new Date();
        }
        //]]>
        </script>
                
            
</div>
        

<script type="text/javascript">
//<![CDATA[
var _spFormDigestRefreshInterval = 1440000;window.g_updateFormDigestPageLoaded = new Date(); window.g_updateFormDigestPageLoaded.setDate(window.g_updateFormDigestPageLoaded.getDate() -5);var _fV4UI = true;
function _RegisterWebPartPageCUI()
{
    var initInfo = {editable: false,isEditMode: false,allowWebPartAdder: false,listId: "{2eb5ff90-9678-4acd-9722-ab9c39b070b3}",itemId: 1,recycleBinEnabled: true,enableMinorVersioning: true,enableModeration: true,forceCheckout: true,rootFolderUrl: "\u002fen-ie\u002fabout-us\u002fcompliance\u002fdata-protection\u002fPages",itemPermissions:{High:16,Low:196673}};
    SP.Ribbon.WebPartComponent.registerWithPageManager(initInfo);
    var wpcomp = SP.Ribbon.WebPartComponent.get_instance();
    var hid;
    hid = document.getElementById("_wpSelected");
    if (hid != null)
    {
        var wpid = hid.value;
        if (wpid.length > 0)
        {
            var zc = document.getElementById(wpid);
            if (zc != null)
                wpcomp.selectWebPart(zc, false);
        }
    }
    hid = document.getElementById("_wzSelected");
    if (hid != null)
    {
        var wzid = hid.value;
        if (wzid.length > 0)
        {
            wpcomp.selectWebPartZone(null, wzid);
        }
    }
};
function __RegisterWebPartPageCUI() {
ExecuteOrDelayUntilScriptLoaded(_RegisterWebPartPageCUI, "sp.ribbon.js");}
_spBodyOnLoadFunctionNames.push("__RegisterWebPartPageCUI");var __wpmExportWarning='This Web Part Page has been personalized. As a result, one or more Web Part properties may contain confidential information. Make sure the properties contain information that is safe for others to read. After exporting this Web Part, view properties in the Web Part description file (.WebPart) by using a text editor such as Microsoft Notepad.';var __wpmCloseProviderWarning='You are about to close this Web Part.  It is currently providing data to other Web Parts, and these connections will be deleted if this Web Part is closed.  To close this Web Part, click OK.  To keep this Web Part, click Cancel.';var __wpmDeleteWarning='You are about to permanently delete this Web Part.  Are you sure you want to do this?  To delete this Web Part, click OK.  To keep this Web Part, click Cancel.';var g_clientIdDeltaPlaceHolderMain = "DeltaPlaceHolderMain";
var g_clientIdDeltaPlaceHolderUtilityContent = "DeltaPlaceHolderUtilityContent";
g_QuickLaunchControlIds.push("zz1_V4QuickLaunchMenu");_spBodyOnLoadFunctionNames.push('QuickLaunchInitDroppable'); var g_zz1_V4QuickLaunchMenu = null; function init_zz1_V4QuickLaunchMenu() { if (g_zz1_V4QuickLaunchMenu == null) g_zz1_V4QuickLaunchMenu = $create(SP.UI.AspMenu, null, null, null, $get('zz1_V4QuickLaunchMenu')); } ExecuteOrDelayUntilScriptLoaded(init_zz1_V4QuickLaunchMenu, 'SP.Core.js');
//]]>
</script>
</form>
        <span id="DeltaPlaceHolderUtilityContent">
            
        </span>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
                
        
        


<div id="CookiePolicyDiv" class="cookiePolicy noindex">
    <div class="cookiePolicyWrapper noindex">
        <div class="cookiePolicyMessage noindex">Important information regarding cookies and the Department of Communications, Climate Action and Environment website. By using this website, you consent to the use of cookies in accordance with our <a href="/en-ie/about-us/compliance/privacy-and-cookie-policy/Pages/Privacy-and-Cookie-Policy.aspx">Cookie Policy</a>.</div>
        <div class="cookiePolicyAcceptButton noindex">
            <a href="javascript:void(0);" onclick="javascript:jQuery('.cookiePolicy').acceptCookiePolicy();return false;">
                <Div class='cookiePolicyClose'>Close</div>
            </a>           
        </div>
    </div>
</div>
     
    </body>
</html>