










<!doctype html>
<html lang="en">
<head>
   <meta charset="utf-8">
   <title>Privacy Policy | Virgin Money UK</title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   


<script>document.createElement("picture");</script>
<script type="text/javascript" src="/virgin/assets/js/combined-top.min.js"></script>
<script type="text/javascript" src="/virgin/assets/js/mmcore.js"></script>
<link rel="stylesheet" type="text/css" href="/virgin/assets/css/styles.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css"/>
<!--[if IE 9]>
<link rel="stylesheet" type="text/css" href="/virgin/assets/css/ie9styles.css"/>
<![endif]-->


<meta property="og:image" content="http://uk.virginmoney.com/virgin/Images/virgin-money-logo-sharing_tcm23-25136.gif" />

<link rel="canonical" href="http://uk.virginmoney.com/virgin/security.jsp" /> <meta property="og:type" content="website" />
<meta property="og:site_name" content="Virgin Money UK" />
<meta property="og:url" content="http://uk.virginmoney.com/virgin/security.jsp" />

</head>
<body class="home site">
   <div id="e-privacy-message">
   <div id="e-privacy-message-script" class="vm cookie-msg" style="display:none">
      <div class="cookie-msg__container" id="cookie-msg">
         <p class="smallest">Our website uses cookies. They help us understand how customers use our website so we can give you the best experience possible and also keep our online adverts relevant. By continuing to browse this site or choosing to close this message, you give consent for cookies to be used.
            <br/><a href="/virgin/cookies/">Read more about our cookies</a>.
         </p>
         <a href="#" class="button--primary button--small" id="btn-dismiss-cookies">Don't show me this again</a>
      </div>
      <div class="cookie-msg__container" id="cookie-msg-nocookies">
         <p class="smallest">We can see from your browser settings that you're not accepting cookies. Our online application uses cookies to help it to work. These cookies last only for the duration of your visit and are deleted when you close your browser. No personally identifiable information is collected.
            <br/>If you want to apply online please change your browser settings to accept cookies. If you'd rather apply or service your account over the phone, please visit our 'Contact us' section for more details.
            <br/><a href="/virgin/cookies/turning-cookies-on-or-off.jsp">How to turn cookies on</a>.
         </p>
         <a href="#" class="button--primary button--small" id="btn-dismiss-nocookies">Hide this message</a>
      </div>
   </div>
   <div id="e-privacy-message-noscript" class="vm cookie-msg">
      <div class="cookie-msg__container">
         <p class="smallest">Our website uses cookies. They help us understand how customers use our website so we can give you the best experience possible and also keep our online adverts relevant. By continuing to browse this site or choosing to close this message, you give consent for cookies to be used.
            <br/><a href="/virgin/cookies/">Read more about our cookies</a>.
         </p>
      </div>
   </div>
</div>

   <!--[if lt IE 9]>
      <div class="cookie-msg">
        <div class="cookie-msg__container">
          <p class="smallest">We've noticed you're visiting us on IE8. Without the most up-to-date version of your browser, you may have problems viewing content and interacting with the site as we intended.</p>
        </div>
      </div>
   <![endif]-->
   <div class="site-container">
      <header class="site-head">
   <div class="site-head__container" id="top">
      <div class="site-head__lockoff"></div>
      <a href="/virgin" class="site-head__logo site-head__item">
         <img src="/virgin/assets/img/vm-positive-retina.png" class="site-head__img site-head--small" alt="Virgin Money" />
         <img src="/virgin/assets/img/vm-negative-retina.png" class="site-head__img site-head--mplus" alt="Virgin Money" />
      </a>
      
      <a href="#primary-nav" id="#primary-nav-btn" class="menu-btn site-head__item js-menu-btn">Menu</a>
      <a href="#signin-nav" id="#signin-nav-btn" class="topnav-icon topnav-icon--signin site-head__item js-menu-btn">
           <span class="topnav-icon__text">Sign in</span>
      </a>
      <a href="/store-finder/" class="topnav-icon topnav-icon--findastore site-head__item">
         <span class="topnav-icon__text">Find a store</span>
      </a>
      <a href="/virgin/contact/" class="topnav-icon topnav-icon--contactus site-head__item">
         <span class="topnav-icon__text">Contact us</span>
      </a>
      
      <div class="site-head__nav-placeholder"></div>
   </div>
</header>

      
      <div class="grid">
         <div class="grid__row">
            <div class="grid__span8--large grid__span8--medium grid__span4--small grid__end--medium grid__end--small">
               <div class="alpha-a--large alpha-b--medium alpha-a--small omega-b--medium omega-a--small">
                  <section class="section__content-wrapper section-margin--standard"> 
                     <a class="secondary-nav-jump js-nav-button" href="#section-nav">
<span class="js-replace-text tondo secondary-nav__link" data-replacetext="In this section">In this section</span>
<span class="glyph glyph--chevron-down glyph--secondary-menu"></span>
</a>

                     <h1>Virgin Money Privacy Policy</h1>

                     <section>
<header>
<h2>Who we are</h2>
</header>
<p>This notice applies to the following Virgin Money Group companies:</p><ul><li>Virgin Money plc;</li><li>Virgin Money Personal Financial Service Ltd;</li><li>Virgin Money Unit Trust Managers Ltd; and</li><li>Virgin Money Management Services Ltd.</li></ul><p>All Virgin Money Group companies are registered on the public register of data controllers maintained by the Information Commissioner.</p>
</section><section>
<header>
<h2>How we collect your information</h2>
</header>
<p>The information we hold about you comes from the way you engage with us such as in Store, online banking, via the post or over the telephone.</p><p>We may also obtain information about you from our partner companies as well as credit reference agencies and fraud prevention agencies.</p>
</section><section>
<header>
<h2>The type of information we hold</h2>
</header>
<p>The information we hold may be about yourself (such as name, address and contact details) and financial information (including details of the accounts and products you hold and related payments and transactions).</p><p>Some information may sensitive information such as health and criminal offences. This information will only be used to provide you with the service you have requested.</p><p>Where you provide information about other people (such as joint applicants) you must ensure that you have their consent or are otherwise entitled to provide their information to us.</p>
</section><section>
<header>
<h2>How do we use your information?</h2>
</header>
<p>When we ask for information from you we will tell you how it might be used for example, to:</p><ul><li>To assess and process applications;</li><li>To verify your identity;</li><li>To prevent fraud and money laundering;</li><li>To manage your account(s) and communicate with you; and</li><li>For audit purposes, research and statistical analysis.</li></ul><p>For further information about how your information might be used by credit reference agencies, fraud prevention agencies and ourselves <a href="http://uk.virginmoney.com/virgin/downloads/using-your-personal-information.pdf" target="_blank">click here<span class="visually-hidden">&nbsp; further information on how your information may be used. Link opens in a new window</span><span class="glyph glyph--popup">&nbsp;</span></a></p>
</section><section>
<header>
<h2>Contacting you</h2>
</header>
<p>We will use your contact details to communicate with you to help you manage your account, to fulfil our regulatory obligations (for example notify you of changes to terms and conditions), to remind you about the features and benefits of products you already hold and, with your consent, tell you about other products and services which may be of interest to you &#8211; these are called marketing messages and you can change your mind about these communications at any time.</p>
</section><section>
<header>
<h2>Who we might share your information with?</h2>
</header>
<p>From time to time we may share your information with other people or organisations (who are also bound to keep it safe and secure) if we have a duty to disclose it, if it is required for the management of your account/s, or law or regulation allows or requires us to do so, for legitimate business purpose. For example we may share it with:</p><ul><li>Other companies within the Virgin and Virgin Money Group of companies</li><li>Our service providers and agents; in some cases, this might include those outside the European Economic Area.</li><li>Credit reference agencies;</li><li>Fraud prevention agencies; and</li><li>Regulators and authorities.</li></ul>
</section><section>
<header>
<h2>How can you access your information?</h2>
</header>
<p>For a small administration fee you can access the information we hold about you. You can also ask us to change or delete any personal information previously provided. If we can we will, but sometimes we have to maintain records for legal reasons. If you would like a copy of your information, you can write to the Data Protection Officer at the address below.</p>
</section><section>
<header>
<h2>How long will we keep your information?</h2>
</header>
<p>We will not retain your personal information for longer than is necessary for the maintenance of your account, or to meet other legal or regulatory requirements.</p>
</section><section>
<header>
<h2>Use of cookies</h2>
</header>
<p>We may use cookies and similar technologies on our website. Cookies are small text files that may be stored on your computer or mobile device when you visit our website.</p><p>Cookies do many different things, such as letting you navigate between web pages efficiently and remembering your preferences. For information about cookies and how we use them on this website, <a href="/virgin/cookies/" target="_blank">visit our cookies page<span class="visually-hidden">&nbsp;link opens in a new window</span><span class="glyph glyph--popup">&nbsp;</span></a></p>
</section><section>
<header>
<h2>How do we tell you about future changes to this policy?</h2>
</header>
<p>Any changes we make to our policy will be put on our website. Please check for updates from time to time, so you are always fully aware of what information is collected and how it is used.</p>
</section><section>
<header>
<h2>How to contact us?</h2>
</header>
<p>If you have any questions or concerns about our use of your personal information or would like a copy of the information we hold about you, please write to:</p><p>The Data Protection Officer, Virgin Money plc, Jubilee House, Gosforth, Newcastle upon Tyne, NE3 4PL</p>
</section>
                  </section> 
               </div>
            </div>
            <div class="grid__span3--large grid__push1--large grid__span8--medium grid__span4--small grid__end--large">
               <div class="alpha-b--medium alpha-a--small omega-a--large omega-b--medium omega-a--small section-margin--secondary-nav">
                  <nav id="section-nav" class="section-nav">
<p class="navigation__title navigation__title--secondary">In this section</p>
<ul class="navigation">
<li class="navigation__item"><a href="/virgin/service/offers.jsp">Offers</a>
</li>
<li class="navigation__item"><a href="/virgin/service/staying-safe-online/">Staying safe online</a>
</li>
<li class="navigation__item"><a href="/virgin/contact/">Contact us</a>
</li>
<li class="navigation__item is-current"><a href="/virgin/security.jsp">Privacy Policy</a>
</li>
<li class="navigation__item"><a href="/virgin/service/">My Virgin Money Home</a>
</li>
</ul></nav>
   
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- Global Nav -->
<nav class="nav nav--primary" id="primary-nav">
   <ul class="nav__list">
      <li id="nav-home" class="nav__item"><a class="nav__link" href="/virgin">Home</a></li>
      <li id="nav-current-accounts" class="nav__item">
<a class="nav__link js-show-content" href="/virgin/current-account/">Current accounts</a>
<nav class="subnav js-hidden-content">
<ul class="cf">
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon" href="/virgin/current-account/">Current accounts Home</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-2">
<a class="subnav__link" href="/virgin/current-account/essential-current-account.jsp">Essential Current Account
<span class="subnav__strap">Designed to deliver the essentials of banking brilliantly</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-8">
<a class="subnav__link" href="/virgin/current-account/current-account-interest-form.jsp-SSL">Other current accounts
<span class="subnav__strap">We are working on a range of other current accounts</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-9 subnav__item--full">
<a class="subnav__link" href="/virgin/service/current-account/-SSL">Existing customers
<span class="subnav__strap">Access your terms & conditions and useful forms</span>
</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=current-account&amp;dd=0">Current accounts sign in</a>
</li>
</ul>
</nav>
</li>
<li id="nav-savings" class="nav__item">
<a class="nav__link js-show-content" href="/virgin/savings/">Savings</a>
<nav class="subnav js-hidden-content">
<ul class="cf">
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon" href="/virgin/savings/">Savings Home</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-44">
<a class="subnav__link" href="/virgin/isa/">Virgin ISAs
<span class="subnav__strap">Everything you need to know about your ISA allowance</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-2">
<a class="subnav__link" href="/savings/find/results/all/">Find a savings account
<span class="subnav__strap">Search our entire range of savings accounts and Cash ISAs</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-5">
<a class="subnav__link" href="/savings/find/results/isas/">Cash ISAs
<span class="subnav__strap">A range of tax-free Virgin Cash ISAs - easy access or fixed rate</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-13">
<a class="subnav__link" href="/savings/find/help_to_buy_isa/overview/">Help to Buy: ISA
<span class="subnav__strap">Get up to a £3,000 tax-free government bonus on savings towards your first home.</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-6">
<a class="subnav__link" href="/savings/find/results/fixed-term/">Fixed Rate Bonds
<span class="subnav__strap">Benefit from a higher rate of interest by saving your money over a fixed period</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-7">
<a class="subnav__link" href="/virgin/savings/learn/">Learn about savings
<span class="subnav__strap">Everything you need to know about our savings accounts</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-10 subnav__item--full">
<a class="subnav__link" href="/virgin/savings/helpful-information/">Existing customers
<span class="subnav__strap">Find your interest rate, download forms, online guides and tools</span>
</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=savings&amp;dd=0">Savings sign in</a>
</li>
</ul>
</nav>
</li>
<li id="nav-currency" class="nav__item">
<a class="nav__link js-show-content" href="/virgin/travel-money/">Currency</a>
<nav class="subnav js-hidden-content">
<ul class="cf">
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon" href="/virgin/travel-money/">Currency Home</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-40">
<a class="subnav__link" href="/virgin/travel-money/">Virgin Money Travel Money
<span class="subnav__strap">Commission-free currency with great exchange rates</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-39">
<a class="subnav__link" href="/virgin/international-money-transfer/">International Money Transfer
<span class="subnav__strap">Fee-free overseas transfers</span>
</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=international-money-transfer&amp;dd=0">International Money Transfer sign in</a>
</li>
</ul>
</nav>
</li>
<li id="nav-investments" class="nav__item">
<a class="nav__link js-show-content" href="/virgin/splash/investments.jsp">Investments</a>
<nav class="subnav js-hidden-content">
<ul class="cf">
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon" href="/virgin/splash/investments.jsp">Investments Home</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-8">
<a class="subnav__link" href="/virgin/isa/stocks-and-shares/">Stocks and Shares ISAs
<span class="subnav__strap">Why pay tax on your investments?</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-9">
<a class="subnav__link" href="/virgin/unit-trusts/">Investment Funds
<span class="subnav__strap">Used up your ISA allowance? Invest more in a unit trust</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-11 subnav__item--full">
<a class="subnav__link" href="/virgin/financial-planning/">Financial planning
<span class="subnav__strap">Book a financial review in one of our Virgin Money Stores</span>
</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=investments&amp;dd=0">Investments sign in</a>
</li>
</ul>
</nav>
</li>
<li id="nav-mortgages" class="nav__item">
<a class="nav__link js-show-content" href="/virgin/mortgages/">Mortgages</a>
<nav class="subnav js-hidden-content">
<ul class="cf">
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon" href="/virgin/mortgages/">Mortgages Home</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-10">
<a class="subnav__link" href="/mortgages/find/Find-A-Mortgage/">Find a mortgage
<span class="subnav__strap">Search our entire range of mortgages</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-32">
<a class="subnav__link" href="/virgin/mortgages/first-time-buyer/">First time buyer
<span class="subnav__strap">Mortgages and helpful information to get you on the property ladder</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-35">
<a class="subnav__link" href="/virgin/mortgages/buy-to-let/">Buy-to-let
<span class="subnav__strap">Take your next step in property investment with our award winning mortgages</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-34">
<a class="subnav__link" href="/virgin/mortgages/remortgage/">Remortgaging
<span class="subnav__strap">Remortgage with us and get a better deal</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-33">
<a class="subnav__link" href="/virgin/mortgages/moving-home/">Moving home
<span class="subnav__strap">We can help you every step of the way</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-36">
<a class="subnav__link" href="/virgin/mortgages/existing-customers/">Existing customers
<span class="subnav__strap">Everything you need if you already have a Virgin Mortgage</span>
</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=mortgages&amp;dd=0">Mortgages sign in</a>
</li>
</ul>
</nav>
</li>
<li id="nav-cards" class="nav__item">
<a class="nav__link js-show-content" href="/virgin/credit-cards/">Cards</a>
<nav class="subnav js-hidden-content">
<ul class="cf">
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon" href="/virgin/credit-cards/">Cards Home</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-10">
<a class="subnav__link" href="/virgin/credit-cards/balance-transfer-cards/">Balance Transfer Credit Cards
<span class="subnav__strap">Move existing credit card debt to one of our balance transfer cards</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-19">
<a class="subnav__link" href="/virgin/credit-cards/money-transfer-cards/">Money Transfer Credit Cards
<span class="subnav__strap">Pay money straight into your current account with a money transfer card</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-20">
<a class="subnav__link" href="/virgin/credit-cards/purchase-cards/">Purchase Credit Cards
<span class="subnav__strap">Need a card for a big purchase or everyday spending?</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-3">
<a class="subnav__link" href="/virgin/credit-cards/all-round-cards/">All Round Credit Cards
<span class="subnav__strap">Balance to transfer plus new purchases coming up?</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-21">
<a class="subnav__link" href="/virgin/cards/vaa.jsp">Virgin Atlantic Credit Cards
<span class="subnav__strap">Earn Flying Club miles every time you use your card to pay</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-22">
<a class="subnav__link" href="/virgin/prepaid-card/  ">Prepaid MasterCard
<span class="subnav__strap">Our award-winning prepaid card</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-23 subnav__item--full">
<a class="subnav__link" href="/virgin/travel-prepaid-card/">Prepaid Travel Money MasterCard
<span class="subnav__strap">The smart way to spend abroad</span>
</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=credit-cards&amp;dd=0">Cards sign in</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=prepaid-cards&amp;dd=0">Prepaid cards sign in</a>
</li>
</ul>
</nav>
</li>
<li id="nav-insurance" class="nav__item">
<a class="nav__link js-show-content" href="/virgin/splash/insurance.jsp">Insurance</a>
<nav class="subnav js-hidden-content">
<ul class="cf">
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon" href="/virgin/splash/insurance.jsp">Insurance Home</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-41">
<a class="subnav__link" href="/virgin/home-insurance/">Home Insurance
<span class="subnav__strap">Award-winning insurance that won't make you want to break things</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-24">
<a class="subnav__link" href="/virgin/travel-insurance/">Travel Insurance
<span class="subnav__strap">Leave your worries behind with our comprehensive travel insurance</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-37">
<a class="subnav__link" href="/virgin/life-insurance/">Life Insurance
<span class="subnav__strap">Life and critical illness insurance that protects the ones you love</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-27">
<a class="subnav__link" href="/virgin/car-insurance/">Car Insurance
<span class="subnav__strap">Cover that won't drive you crazy</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-26 subnav__item--full">
<a class="subnav__link" href="/virgin/pet-insurance/">Pet Insurance
<span class="subnav__strap">Protect yourself from the worry of vet's bills</span>
</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=insurance&amp;dd=0">Insurance sign in</a>
</li>
</ul>
</nav>
</li>
<li id="nav-pensions" class="nav__item">
<a class="nav__link js-show-content" href="/virgin/splash/pensions.jsp">Pensions</a>
<nav class="subnav js-hidden-content">
<ul class="cf">
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon" href="/virgin/splash/pensions.jsp">Pensions Home</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-28">
<a class="subnav__link" href="/virgin/pension/personal/">Personal pensions
<span class="subnav__strap">Straightforward, straight-talking pensions</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-29">
<a class="subnav__link" href="/virgin/pension/kids/">Children's pensions
<span class="subnav__strap">It's never too early to start saving for their retirement</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-30">
<a class="subnav__link" href="/virgin/pension/workplace/existing-schemes.jsp">Workplace pensions
<span class="subnav__strap">Find out your options</span>
</a>
</li>
<li class="subnav__item subnav__img subnav__img--pos-31">
<a class="subnav__link" href="/virgin/pensions-and-retirement/">Pensions &amp; Retirement
<span class="subnav__strap">Secure your future, wherever you are on the road to retirement</span>
</a>
</li>
<li class="subnav__item subnav--shift">
<a class="subnav__link subnav__icon subnav__icon--signin" href="/virgin/service/?product=personal-pension&amp;dd=0">Pensions sign in</a>
</li>
</ul>
</nav>
</li>

      <li class="nav__item is-hidden-mplus"><a class="nav__link" href="/virgin/service/"> Sign In <span class="glyph menu-btn--signin-white"></span></a></li>
   </ul>
</nav>
<nav class="nav nav--secondary" id="signin-nav">
   <ul class="nav__list">
      <li class="nav__item"><a class="nav__link header" href="/virgin/service/">Choose the product you want to service:</a></li>
      <li class="nav__item"><a class="nav__link" href="/virgin/service/?product=credit-cards&dd=0">Credit cards <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item"><a class="nav__link" href="/virgin/service/?product=prepaid-cards&dd=0">Prepaid cards <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item"><a class="nav__link" href="/virgin/service/?product=savings&dd=0">Savings <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item"><a class="nav__link" href="/virgin/service/?product=investments&dd=0">Investments <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item"><a class="nav__link" href="/virgin/service/?product=mortgages&dd=0">Mortgages <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item"><a class="nav__link" href="/virgin/service/?product=current-account&dd=0">Current accounts <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item"><a class="nav__link nav-new-label" href="/virgin/service/?product=international-money-transfer&dd=0">International money transfer <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item"><a class="nav__link" href="/virgin/service/?product=personal-pension&dd=0">Personal pensions <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item"><a class="nav__link" href="/virgin/service/?product=insurance&dd=0">Insurance <span class="visually-hidden">&nbsp;sign in</span></a></li>
      <li class="nav__item darker"><a class="nav__link" href="/virgin/fraud-information-centre-NOSSL"><span class="visually-hidden">Find out more about&#160;</span> Staying safe online</a></li>
      <li class="nav__item darker"><a class="nav__link" href="http://uk.virginmoneygiving.com">Virgin Money Giving</a></li>
   </ul>
</nav>

<footer class="site-footer global-footer margin-top-48" id="site-footer">
   <div class="site-footer__linkbox">
      <div class="grid">
         <div class="grid__row">
              <div class="grid__span8--medium grid__span12--large grid__end--large grid__end--medium"> <!-- Generic "box" class shared with header/boxout etc? -->
                <div class="alpha-a--large omega-a--large alpha-a--medium omega-a--medium alpha-a--small omega-a--small">
                  <div class="grid__span4--small grid__span4--medium grid__span4--large">
                    <ul class="site-footer__links site-footer__links--first">
                        <li><a class="site-footer__item" href="/virgin/contact/">Contact us</a></li>
<li><a class="site-footer__item" href="/virgin/about/">About us</a></li>
<li><a class="site-footer__item" href="/store-finder/">Store finder</a></li>
<li><a class="site-footer__item ext-no-ssl" href="https://ask.virginmoney.com/">Ask a question</a></li>

                     </ul>
                  </div>
                  <div class="grid__span4--small grid__span4--medium grid__end--medium grid__span4--large">
                    <ul class="site-footer__links site-footer__links--second">
                        <li><a href="#footer-2-0" class="js-show-content site-footer__item">Welcome to Virgin Money <span class="glyph glyph--plus"></span></a>
<ul class="site-footer__links js-hidden-content" id="footer-2-0">
<li><a class="site-footer__item ext-no-ssl" href="http://careers.virginmoney.com/">Careers</a></li>
<li><a class="site-footer__item" href="/virgin/news-centre/">Media centre</a></li>
<li><a class="site-footer__item" href="/virgin/accessibility.jsp">Accessibility</a></li>
</ul></li>
<li><a href="#footer-2-1" class="js-show-content site-footer__item">Helpful info for savers <span class="glyph glyph--plus"></span></a>
<ul class="site-footer__links js-hidden-content" id="footer-2-1">
<li><a class="site-footer__item" href="/savings/find/results/all/">View savings range</a></li>
<li><a class="site-footer__item" href="/savings/helpful-information/find-your-interest-rate/">Interest rate finder</a></li>
<li><a class="site-footer__item" href="/virgin/savings/helpful-information/useful-downloads/">Useful downloads</a></li>
</ul></li>
<li><a href="#footer-2-2" class="js-show-content site-footer__item">Helpful mortgage info <span class="glyph glyph--plus"></span></a>
<ul class="site-footer__links js-hidden-content" id="footer-2-2">
<li><a class="site-footer__item" href="/mortgages/find/Find-A-Mortgage">View mortgage range</a></li>
<li><a class="site-footer__item" href="/mortgages/tools/mortgage-calculator/">How much can I borrow?</a></li>
<li><a class="site-footer__item" href="/mortgages/find/Find-A-Mortgage/">Find a mortgage</a></li>
</ul></li>
<li><a href="#footer-2-3" class="js-show-content site-footer__item">Helpful Credit Card info <span class="glyph glyph--plus"></span></a>
<ul class="site-footer__links js-hidden-content" id="footer-2-3">
<li><a class="site-footer__item" href="/virgin/credit-cards/">View credit card range</a></li>
<li><a class="site-footer__item" href="/virgin/credit-cards/credit-card-guide.jsp">Our guide to credit cards</a></li>
</ul></li>
<li><a class="site-footer__item" href="/virgin/site-map/">See our full sitemap</a></li>
<li><a href="#footer-2-5" class="js-show-content site-footer__item">Already a customer? <span class="glyph glyph--plus"></span></a>
<ul class="site-footer__links js-hidden-content" id="footer-2-5">
<li><a class="site-footer__item" href="/virgin/service/">My Virgin Money (Customer log in)</a></li>
<li><a class="site-footer__item" href="/virgin/about-lounges/">Virgin Money Lounges</a></li>
</ul></li>
<li><a href="#footer-2-6" class="js-show-content site-footer__item">Intermediaries <span class="glyph glyph--plus"></span></a>
<ul class="site-footer__links js-hidden-content" id="footer-2-6">
<li><a class="site-footer__item ext-no-ssl" href="http://intermediaries.virginmoney.com/virgin/">Intermediaries <span class="visually-hidden">&nbsp;website</span></a></li>
<li><a class="site-footer__item" href="/virgin/ifa/">Mortgage centre</a></li>
<li><a class="site-footer__item" href="/virgin/ifa/home.jsp">IFA contact information</a></li>
</ul></li>
<li><a class="site-footer__item" href="/virgin/investor-relations/">Investor relations</a></li>
<li><a href="#footer-2-8" class="js-show-content site-footer__item">More from Virgin <span class="glyph glyph--plus"></span></a>
<ul class="site-footer__links js-hidden-content" id="footer-2-8">
<li><a class="site-footer__item ext-no-ssl" href="http://uk.virginmoneygiving.com/" target="_blank">Virgin Money Giving <span class="visually-hidden">&nbsp;Link opens in a new window</span><span class="glyph glyph--popup">&nbsp;</span></a></li>
<li><a class="site-footer__item ext-no-ssl" href="http://www.virgin.com/" target="_blank">Virgin.com <span class="visually-hidden">&nbsp;Link opens in a new window</span><span class="glyph glyph--popup">&nbsp;</span></a></li>
<li><a class="site-footer__item ext-no-ssl" href="http://www.virginmoneylondonmarathon.com/" target="_blank">Virgin Money London Marathon <span class="visually-hidden">&nbsp;Link opens in a new window</span><span class="glyph glyph--popup">&nbsp;</span></a></li>
<li><a class="site-footer__item ext-no-ssl" href="http://virginmoney.com.au/" target="_blank">Virgin Money - Australia <span class="visually-hidden">&nbsp;Link opens in a new window</span><span class="glyph glyph--popup">&nbsp;</span></a></li>
<li><a class="site-footer__item ext-no-ssl" href="http://www.virginmoney.co.za/" target="_blank">Virgin Money - South Africa <span class="visually-hidden">&nbsp;Link opens in a new window</span><span class="glyph glyph--popup">&nbsp;</span></a></li>
</ul></li>

                     </ul>
                  </div>
                  <div class="grid__span4--small grid__span4--medium grid__span4--large grid__end--large">
                     <ul class="site-footer__links site-footer__links--third">
                        <li><a class="site-footer__item" href="/virgin/legals/">Legals</a></li>
<li><a class="site-footer__item" href="/virgin/security.jsp">Privacy policy</a></li>
<li><a class="site-footer__item" href="/virgin/cookies/">How we use cookies</a></li>
<li><a class="site-footer__item" href="/feedback/">Feedback</a></li>
<li><a id="desktop-return" class="site-footer__item" href="/virgin/">Go to desktop site</a></li>

                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <img class="vm-lockoff-logo" src="/virgin/assets/img/footer-logo.png" alt="Virgin Money - Welcome to Better Banking" />
   <div class="site-footer__social">
      <div class="grid">
         <p class="vm-text-grey gamma">Connect with us</p>
         <a href="http://www.facebook.com/VirginMoneyUK" class="social-button social-button--facebook ext-no-ssl" target="_blank"><span class="visually-hidden">Facebook Link opens in a new window</span></a>
         <a href="http://twitter.com/virginmoney" class="social-button social-button--twitter ext-no-ssl" target="_blank"><span class="visually-hidden">Twitter Link opens in a new window</span></a>
      </div>
   </div>
</footer>


   <div class="grid__span12--large grid__span8--medium grid__span4--small grid__end--medium grid__end--large">
      <div class="alpha-a--large alpha-a--medium alpha-a--small omega-a--medium omega-a--small margin-top-30 margin-bottom-30">
         
      </div>
   </div>
   <script type="text/javascript" src="/virgin/assets/js/ePrivacyParams.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.2/jquery-ui.min.js"></script>
<script type="text/javascript" src="/virgin/assets/js/script.js"></script>
<script type="text/javascript" src="/virgin/assets/js/combinedscript.min.js"></script>
<script type="text/javascript" src="/virgin/assets/js/decibel.js"></script>
<script type="text/javascript" src="/virgin/assets/js/app-outage.js"></script>


<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3685418-1']);
  _gaq.push(['_setDomainName', 'virginmoney.com']);
  _gaq.push(['_setAllowLinker', true]);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1009590742;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1009590742/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- DoubleClick DART Natural Search Standard Tracking Tag for DS3
DFA Network - MVI - DFA EMEA (4378) : DS Agency - Media Vest - Manchester (20200000000000281) : DS Advertiser - Virgin Money Natural Search (21700000000009441)
Please do not remove
Tag should be placed at the top of the page, after opening body tag and before DoubleClick Spotlight or Floodlight tag(s)
Creation Date: 06-25-2012 -->
<script type='text/javascript'>
// DoubleClick DART Natural Search Tracking Code
if (document.referrer){
var ord = Math.random()*1000000000000;
var dcHost = (("https:" == document.location.protocol) ? "https://" : "http://");
document.write('<SCRIPT language="JavaScript1.1" SRC='+ dcHost +'ad.doubleclick.net/adj/N4378.N10463.nso.codesrv/B6754960;dcadv=3725590;sz=1x2;ord=' + ord + '?"><\/SCRIPT>');
}
</script>

  
</body>
</html>